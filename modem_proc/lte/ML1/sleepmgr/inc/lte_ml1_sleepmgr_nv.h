/*!
  @file
  lte_ml1_sleepmgr_nv.h

  @brief
  header file for efs client related operations for this module.

  @detail
*/

/*===========================================================================

  Copyright (c) 2009 - 2014 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/ML1/sleepmgr/inc/lte_ml1_sleepmgr_nv.h#1 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
24/10/13    ap      Initial version
===========================================================================*/

#ifndef LTE_ML1_SLEEPMGR_NV_H
#define LTE_ML1_SLEEPMGR_NV_H
/*===========================================================================
		
								   INCLUDE FILES
		
===========================================================================*/

/*===========================================================================
		
						  MODULE DEFINITIONS AND TYPES
		
===========================================================================*/
#define LTE_ML1_SLEEPMGR_EFS_MAX_FILENAME 100
#define LTE_ML1_SLEEPMGR_EFS_STR_LEN 25
#define LTE_ML1_SLEEPMGR_ML1_BASENAME "/nv/item_files/modem/lte/ML1"
#define LTE_ML1_SLEEPMGR_EFS_PERMISSIONS 0777

#include <comdef.h>
/*! Power strobe values */
typedef struct
{
  /*! Power strobe enable */
  boolean     pwr_strobe_enable;
  /* GPIO for power strobe */
  uint16      pwr_strobe_gpio;
} lte_ml1_nv_cfg_sleep_pwr_strobe_cfg_s;


/*===========================================================================
	 
	FUNCTION:  lte_ml1_sleepmgr_nv_sleep_tcxo_vote_assert_size
	 
===========================================================================*/
/*!
	@brief
	This function returns the size of EFS variable in bytes. 
	This size information is used by EFS framework for memory 
	allocation and storing the EFS value.
			
	@details
	 
	@return size in bytes
*/
/*=========================================================================*/

  uint8 lte_ml1_sleepmgr_nv_sleep_tcxo_vote_assert_size(void);
 
/*===========================================================================
	 
	FUNCTION:  lte_ml1_sleepmgr_nv_sleep_tcxo_vote_assert_defaultvalue
	 
 ===========================================================================*/
/*!
	@brief
	This function provides the default value for the efs variable. 
	This data will be stored with the EFS framework.
	 
	@details
    EFS framework sends the pointer to memory(which framework owns), 
    to fill in the default value.
	 
	@return
*/
 /*=========================================================================*/

  void lte_ml1_sleepmgr_nv_sleep_tcxo_vote_assert_defaultvalue(void *ptr);
 
/*===========================================================================
	 
	FUNCTION:  lte_ml1_sleepmgr_nv_sleep_pwr_strobe_cfg_size
	 
===========================================================================*/
/*!
	@brief
	This function returns the size of EFS variable in bytes. 
	This size information is used by EFS framework for memory 
	allocation and storing the EFS value.
			
	@details
	 
	@return size in bytes
*/
/*=========================================================================*/

  uint8 lte_ml1_sleepmgr_nv_sleep_pwr_strobe_cfg_size(void);
 
/*===========================================================================
	 
	FUNCTION:  lte_ml1_sleepmgr_nv_sleep_pwr_strobe_cfg_defaultvalue
	 
 ===========================================================================*/
/*!
	@brief
	This function provides the default value for the efs variable. 
	This data will be stored with the EFS framework.
	 
	@details
    EFS framework sends the pointer to memory(which framework owns),
    to fill in the default value.
	 
	@return
*/
 /*=========================================================================*/

  void lte_ml1_sleepmgr_nv_sleep_pwr_strobe_cfg_defaultvalue(void *ptr);

/*===========================================================================
		  
	FUNCTION:  lte_ml1_sleepmgr_sleep_config_idle_fdd_pre_efsread
		  
===========================================================================*/
/*!
	@brief
	This function will be called pre efsread by framework, 
	to perform client defined pre efsread functionality.
		  
	@details
	.
		  
	@return
*/
/*=========================================================================*/
  
 void lte_ml1_sleepmgr_sleep_config_idle_fdd_pre_efsread(char* efsfilepath);

/*===========================================================================
		  
	FUNCTION:  lte_ml1_sleepmgr_sleep_config_conn_fdd_pre_efsread
		  
===========================================================================*/
/*!
	@brief
	This function will be called pre efsread by framework,
	to perform client defined pre efsread functionality.
		  
	@details
	.
		  
	@return
*/
/*=========================================================================*/

  void lte_ml1_sleepmgr_sleep_config_conn_fdd_pre_efsread(char* efsfilepath);
 
/*===========================================================================
		  
	FUNCTION:  lte_ml1_sleepmgr_sleep_config_idle_tdd_pre_efsread
		  
===========================================================================*/
/*!
	@brief
	This function will be called pre efsread by framework, 
	to perform client defined pre efsread functionality.
		  
	@details
	.
		  
	@return
*/
/*=========================================================================*/

  void lte_ml1_sleepmgr_sleep_config_idle_tdd_pre_efsread(char* efsfilepath);
 
/*===========================================================================
		  
	FUNCTION:  lte_ml1_sleepmgr_sleep_config_conn_tdd_pre_efsread
		  
===========================================================================*/
/*!
	@brief
	This function will be called pre efsread by framework, 
	to perform client defined pre efsread functionality.
		  
	@details
	.
		  
	@return
*/
/*=========================================================================*/

  void lte_ml1_sleepmgr_sleep_config_conn_tdd_pre_efsread(char* efsfilepath);
 
/*===========================================================================
		  
	FUNCTION:  lte_ml1_sleepmgr_sleep_setup_idle_fdd_pre_efsread
		  
===========================================================================*/
/*!
	@brief
	This function will be called pre efsread by framework, 
	to perform client defined pre efsread functionality.
		  
	@details
	.
		  
	@return
*/
/*=========================================================================*/

  void lte_ml1_sleepmgr_sleep_setup_idle_fdd_pre_efsread(char* efsfilepath);
 
/*===========================================================================
		  
	FUNCTION:  lte_ml1_sleepmgr_sleep_setup_conn_fdd_pre_efsread
		  
===========================================================================*/
/*!
	@brief
	This function will be called pre efsread by framework, 
	to perform client defined pre efsread functionality.
		  
	@details
	.
		  
	@return
*/
/*=========================================================================*/

  void lte_ml1_sleepmgr_sleep_setup_conn_fdd_pre_efsread(char* efsfilepath);
 
/*===========================================================================
		  
	FUNCTION:  lte_ml1_sleepmgr_sleep_setup_idle_tdd_pre_efsread
		  
===========================================================================*/
/*!
	@brief
	This function will be called pre efsread by framework, 
	to perform client defined pre efsread functionality.
		  
	@details
	.
		  
	@return
*/
/*=========================================================================*/

  void lte_ml1_sleepmgr_sleep_setup_idle_tdd_pre_efsread(char* efsfilepath);
  
/*===========================================================================
		  
	FUNCTION:  lte_ml1_sleepmgr_sleep_setup_conn_tdd_pre_efsread
		  
===========================================================================*/
/*!
	@brief
	This function will be called pre efsread by framework, 
	to perform client defined pre efsread functionality.
		  
	@details
	.
		  
	@return
*/
/*=========================================================================*/

  void lte_ml1_sleepmgr_sleep_setup_conn_tdd_pre_efsread(char* efsfilepath);
 
/*===========================================================================
		  
	FUNCTION:  lte_ml1_sleepmgr_sleep_dyn_drx_idle_fdd_pre_efsread
		  
===========================================================================*/
/*!
	@brief
	This function will be called pre efsread by framework, 
	to perform client defined pre efsread functionality.
		  
	@details
	.
		  
	@return
*/
/*=========================================================================*/

  void lte_ml1_sleepmgr_sleep_dyn_drx_idle_fdd_pre_efsread(char* efsfilepath);
 
/*===========================================================================
		  
	FUNCTION:  lte_ml1_sleepmgr_sleep_dyn_drx_conn_fdd_pre_efsread
		  
===========================================================================*/
/*!
	@brief
	This function will be called pre efsread by framework,
	to perform client defined pre efsread functionality.
		  
	@details
	.
		  
	@return
*/
/*=========================================================================*/
  
   void lte_ml1_sleepmgr_sleep_dyn_drx_conn_fdd_pre_efsread(char* efsfilepath);
 
/*===========================================================================
		  
	FUNCTION:  lte_ml1_sleepmgr_sleep_dyn_drx_idle_tdd_pre_efsread
		  
===========================================================================*/
/*!
	@brief
	This function will be called pre efsread by framework,
	to perform client defined pre efsread functionality.
		  
	@details
	.
	  
	@return
*/
/*=========================================================================*/

  void lte_ml1_sleepmgr_sleep_dyn_drx_idle_tdd_pre_efsread(char* efsfilepath);
 
/*===========================================================================
		  
	FUNCTION:  lte_ml1_sleepmgr_sleep_dyn_drx_conn_tdd_pre_efsread
		  
===========================================================================*/
/*!
	@brief
	This function will be called pre efsread by framework, 
	to perform client defined pre efsread functionality.
		  
	@details
	.
		  
	@return
*/
/*=========================================================================*/

  void lte_ml1_sleepmgr_sleep_dyn_drx_conn_tdd_pre_efsread(char* efsfilepath);
 
/*===========================================================================
		  
	FUNCTION:  lte_ml1_sleepmgr_rf_warmup_usec_idle_fdd_pre_efsread
		  
===========================================================================*/
/*!
	@brief
	This function will be called pre efsread by framework, 
	to perform client defined pre efsread functionality.
		  
	@details
	.
		  
	@return
*/
/*=========================================================================*/

  void lte_ml1_sleepmgr_rf_warmup_usec_idle_fdd_pre_efsread(char* efsfilepath);

/*===========================================================================
		  
	FUNCTION:  lte_ml1_sleepmgr_rf_warmup_usec_conn_fdd_pre_efsread
		  
===========================================================================*/
/*!
	@brief
	This function will be called pre efsread by framework, 
	to perform client defined pre efsread functionality.
		  
	@details
	.
		  
	@return
*/
/*=========================================================================*/

  void lte_ml1_sleepmgr_rf_warmup_usec_conn_fdd_pre_efsread(char* efsfilepath);
 
/*===========================================================================
		  
	FUNCTION:  lte_ml1_sleepmgr_rf_warmup_usec_idle_tdd_pre_efsread
		  
===========================================================================*/
/*!
	@brief
	This function will be called pre efsread by framework, 
	to perform client defined pre efsread functionality.
		  
	@details
	.
		  
	@return
*/
/*=========================================================================*/

  void lte_ml1_sleepmgr_rf_warmup_usec_idle_tdd_pre_efsread(char* efsfilepath);

/*===========================================================================
		  
	FUNCTION:  lte_ml1_sleepmgr_rf_warmup_usec_conn_tdd_pre_efsread
		  
===========================================================================*/
/*!
	@brief
	This function will be called pre efsread by framework, 
	to perform client defined pre efsread functionality.
		  
	@details
	.
		  
	@return
*/
/*=========================================================================*/

  void lte_ml1_sleepmgr_rf_warmup_usec_conn_tdd_pre_efsread(char* efsfilepath);

#endif

