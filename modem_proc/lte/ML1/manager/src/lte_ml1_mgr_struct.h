/*!
  @file
  lte_ml1_mgr_struct.h

  @brief
  Contains all Manager module globals structure
*/

/*===========================================================================

  Copyright (c) 2013 QUALCOMM Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/ML1/manager/src/lte_ml1_mgr_struct.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------

===========================================================================*/

#ifndef LTE_ML1_MGR_STRUCT_H
#define LTE_ML1_MGR_STRUCT_H

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/
typedef struct lte_ml1_manager_task_data_s lte_ml1_manager_task_data_s;
typedef struct lte_ml1_manager_data_s lte_ml1_manager_data_s;

#include "lte_ml1_mgr_task.h"
#include "lte_ml1_mgr_stm.h"
#include "lte_ml1_mem.h"
#include "lte_ml1_mgr_tam.h"

/*===========================================================================

                   EXTERNAL DEFINITIONS AND TYPES

===========================================================================*/
/*===========================================================================
*******************************MGR MANAGER DATA******************************
===========================================================================*/
struct lte_ml1_manager_task_data_s
{
  //thread id 
  pthread_t                     thread_id;
  
  //task data
  lte_ml1_mgr_client_data_s     *client_ptr;
};

struct lte_ml1_manager_data_s
{
  //instance number 
  lte_mem_instance_type         inst_num;
  
  //Manager static data
  /* Do not clear the information in this structure at one time!
   Data inside are not related to each other! */
  lte_mgr_static_data_s         *mgr_static_data_ptr;
  
  //Manager stm procedure data
  l1m_stm_data_s                *stm_ptr;
  
  //Manager stm test data (Conditional allocation TBD)
  lte_ml1_mgr_test_data_s       *test_data_ptr;
  
#ifdef FEATURE_LTE_AS_RESET
  //Manager stm recovery data (Conditional allocation TBD)
  lte_ml1_mgr_recovery_data_s   *recovery_data_ptr;
#endif
  
  //Manager stm acq phase data (Conditional allocation TBD)
  lte_ml1_mgr_acq_phase_data_s  *acq_phase_ptr;
  
  //Manager stm idle state data (Conditional allocation TBD)
  lte_ml1_mgr_idle_state_data_s *idle_state_ptr;

  // TAM DB pointer
  lte_ml1_mgr_tam_db_s   *tam_db_ptr;

};

/*===========================================================================

  FUNCTION:  lte_ml1_mgr_get_manager_data

===========================================================================*/
/*!
    @brief
    Get L1M data pointer
    
    @return
    L1M data pointer
*/
/*=========================================================================*/
lte_ml1_manager_data_s *lte_ml1_mgr_get_manager_data
(
  lte_mem_instance_type inst_num
);


#endif /* LTE_ML1_MGR_STRUCT_H */
