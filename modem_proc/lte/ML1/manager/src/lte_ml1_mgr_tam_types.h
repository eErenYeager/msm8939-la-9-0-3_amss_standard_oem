/*!
  @file
  lte_ml1_mgr_tam_types.h

  @brief
  Tune Away Manager types header
*/

/*===========================================================================

  Copyright (c) 2013 QUALCOMM Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/ML1/manager/src/lte_ml1_mgr_tam_types.h#1 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
10/09/14   vc      moved upper_lyr_msg_in_susp_mask to tam_db to retian that data 
                   after INACTIVE state. 
05/02/14    vc     Remove qta_tick and unlock cancel flag handling.
04/30/12    vc     Changes to sync tune away type for rf available/unavailable.
08/30/12   anm     Initial Version

===========================================================================*/

#ifndef LTE_ML1_MGR_TAM_TYPES_H
#define LTE_ML1_MGR_TAM_TYPES_H

#include "lte_ml1_comdef.h"
#include "lte_variation.h"
#include "lte_cphy_msg.h"
#include "lte_ml1_rfmgr_msg.h"


/* Masks used to determine if  CPHY_ABORT/CPHY_STOP/CPHY_SUSPEND 
  requests are received during or after TAM suspend  */
#define CPHY_ABORT_BIT     0x01
#define CPHY_STOP_BIT      0x02
#define CPHY_SUSPEND_BIT   0x04

typedef struct lte_ml1_mgr_tam_db_s lte_ml1_mgr_tam_db_s;


/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

/*===========================================================================

                   INTERNAL DEFINITIONS AND TYPES

===========================================================================*/

/*! @brief TAM Suspend/Resume Type Enum

*/
typedef enum{

  LTE_ML1_MGR_TAM_SCHEDULED_TUNE_AWAY,      
  LTE_ML1_MGR_TAM_RUDE_TUNE_AWAY,
  LTE_ML1_MGR_TAM_SLEEP_TUNE_AWAY,
  LTE_ML1_MGR_TAM_LONG_TUNE_AWAY,   

}lte_ml1_mgr_tam_tune_away_type_e;

/*! @brief TAM State Enum
 *  Do NOT change order, code currently depends on it!
*/
typedef enum{
// todo: add comments 
  LTE_ML1_MGR_TAM_IDLE_STATE,      
  LTE_ML1_MGR_TAM_ALLOC_STATE,
  LTE_ML1_MGR_TAM_SUSPEND_TRIGGER_STATE,
  LTE_ML1_MGR_TAM_TRM_WAIT_STATE,
  LTE_ML1_MGR_TAM_RESUME_TRIGGER_STATE,
  LTE_ML1_MGR_TAM_SPV_TIMER_EXPIRED_STATE,
  LTE_ML1_MGR_TAM_QTA_STATE,

}lte_ml1_mgr_tam_state_e;

/*! @brief Tuneaway type
*/
typedef enum{
  LTE_ML1_MGR_TAM_TUNEAWAY_NONE,
  LTE_ML1_MGR_TAM_TUNEAWAY_LTA,
  LTE_ML1_MGR_TAM_TUNEAWAY_QTA,
  LTE_ML1_MGR_TAM_TUNEAWAY_DTA
}lte_ml1_mgr_tam_tuneaway_type_e;

/*! @brief TAM DTA State Enum
*/
typedef enum{
  LTE_ML1_MGR_TAM_SLTE_IDLE,
  LTE_ML1_MGR_TAM_SLTE_ENTER_SETUP_FW,
  LTE_ML1_MGR_TAM_SLTE_ENTER_SETUP_RF,
  LTE_ML1_MGR_TAM_SLTE_DTA,
  LTE_ML1_MGR_TAM_SLTE_EXIT_SETUP_RF,
  LTE_ML1_MGR_TAM_SLTE_EXIT_SETUP_FW
}lte_ml1_mgr_tam_slte_state_e;

/*! @brief RLF/OOS reason 

*/
typedef enum{

  SPV_TIMER_EXPIRY,      
  MGR_RESUME_FAILURE,
}lte_ml1_tam_rlf_oos_reason_e;

#ifdef FEATURE_DTA_FW_RF_DEBUG
/*! @brief id of parameters in DTA simulation efs   

*/
typedef enum{

  SKIP_TRM_REQ,
  SKIP_RFM_REQ,
  SKIP_FW_REQ,
  FAKE_DTA_TIMER_VAL,
}lte_ml1_tam_dta_sim_nv_mask_e;
#endif

/*! @brief TAM module suspend/resume data used in TAM suspend/resume
    processing
*/
typedef struct
{
  /*! RTC Cnt at which Next TAO Start time */
  uint64                                tao_start_rtc;

  /*! Subfn Cnt at which Next TRM reserve is requested*/
  uint32                                trm_reserve_at_subfn_cnt;

  /*! Suspend/resume status */
  errno_enum_type                       status;

  /*! Tune Away type */
  lte_ml1_mgr_tam_tune_away_type_e      tune_away_type;

   /*! TAO priority if QTA/LTA */
  lte_ml1_schdlr_priority_e             tao_prio;

  /* True if slte is enabled, conditioned on there being a TAO object */
  boolean                               is_slte_enabled;

  /* Mask for CCs involved in DTA */
  uint8                                 slte_cc_mask;

  /* SLTE state */
  lte_ml1_mgr_tam_slte_state_e          slte_state;

  /* G/1x - currently used only if is_slte_enabled is true */
  trm_client_enum_t                     target_rat;

  /*!Prev TAO priority if QTA/LTA */
  lte_ml1_schdlr_priority_e             prev_tao_prio;

  /*! Time before tune away at which  TAO should become active*/
  lte_ml1_time_t                        obj_time_before_ta;

  /*! If supervisory timer expired  */
  boolean                               spv_timer_started;
 
  /*! If LTE_LL1_SYS_CONFLICT_CHECK_IND was received*/
  boolean                               conflict_ind_recvd;

  /*! If Chain Grant IND received */
  boolean                               chain_grnt_rcvd;

  /*! If LTE_LL1_SYS_CONFLICT_CHECK_CNF was received */
  boolean                               conflict_cnf_recvd;


/*! If NO RF indication was received from Sleep*/
  boolean                               no_rf_ind_recvd;

    /*! Whether QTA->LTA switch indication was received */
  boolean                               rlf_qta_lta;

  /*! TRM winning client  */
  trm_client_enum_t                     trm_winning_client;

  /*! QTA target rat ASID  */
  sys_modem_as_id_e_type                qta_winning_client_asid;

  /*! Actual unlock_by_time in sclk - zero if immediate was requested or unset */
  uint32                                unlock_by_time;

  /*! Mask to determine the upper layer msgs received during TAM suspend*/
  uint8                                 upper_lyr_msg_in_susp_mask;  

  /*! If TAO IS scheduled as ASAP or subf type*/
  lte_ml1_schdlr_start_time_type_e      tao_start_time_type;

}lte_ml1_tam_suspend_resume_data_s;


/*! @brief TAM module suspend/resume data used in TAM suspend/resume
    processing
*/
typedef struct
{
  /*! If Unlock_ind is buffered */
  boolean                               is_buffered;

  /*! Cfg Ptr for unlock_ind  */
  lte_ml1_rfmgr_msg_trm_unlock_ind_s     cfg;

}lte_ml1_tam_unlock_ind_buffer_s;


/*! @brief ML1 Mgr TAM DB

*/
typedef struct {

  /*! Modules suspend/resume data */
  lte_ml1_tam_suspend_resume_data_s     suspend_resume_data;

  /*! TAM state */
  lte_ml1_mgr_tam_state_e               tam_state;

  /*! Previous TAM state */
  lte_ml1_mgr_tam_state_e               prev_tam_state;

 /*! Buffer to carry Unlock Ind when received before resume is done  */
  lte_ml1_tam_unlock_ind_buffer_s       unlock_ind_buffer;

  /*! RLF/OOS reason*/
  lte_ml1_tam_rlf_oos_reason_e          rlf_oos_reason;
  /* Conflict check cnf db from FW  */
  #ifndef FEATURE_BOLT_MODEM
  lte_LL1_sys_conflict_check_cnf_struct conflict_check_cnf_db;
  #endif

  /*! If trm req and notify called from TAM  */
  boolean                               trm_req_notify_sent;

} lte_ml1_mgr_tam_data_s;


/*! @brief TAM PQ action Enum

*/
typedef enum{

  PROCESS_MSG,      
  BUFFER_MSG,
  IGNORE_MSG,
 
}tam_pq_action_e;


typedef enum
{
   RLF,
}lte_ml1_mgr_tam_qta_lta_switch_cause_e;


struct lte_ml1_mgr_tam_db_s
{
  /*! MGR TAM database */
  lte_ml1_mgr_tam_data_s tam_data;

  /*! SPV timer */
  lte_ml1_common_timer_s lte_ml1_spv_timer;

  /*! SPV timer value - based on t310 */
  uint32                 spv_timer_val;

  /*! RF less wakeup request sent to sleepmgr or not*/
  boolean                rf_less_wakeup_sent;

  /*! If HO failure happened  due to search failure on Target cell*/
  boolean                ho_failed;

  /*! If RF_AVAILABLE was sent to upper layers*/
  boolean                rf_unavailable_sent;

  /* Tune away type indicated to upper layers along with rf unavailable ind*/
  lte_cphy_tuneaway_t    tune_away_type_indicated;
  
#ifdef FEATURE_DTA_FW_RF_DEBUG
    /*! Fake DTA  timer */
  lte_ml1_common_timer_s lte_ml1_fake_dta_timer;
#endif  
};


#endif /* LTE_ML1_MGR_TAM_TYPES_H */

