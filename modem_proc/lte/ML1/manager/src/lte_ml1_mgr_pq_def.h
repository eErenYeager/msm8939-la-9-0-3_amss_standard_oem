/*!
  @file
  lte_ml1_mgr_pq_def.h

  @brief
  Pending queue struct definition header file.
*/

/*===========================================================================

  Copyright (c) 2013 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/ML1/manager/src/lte_ml1_mgr_pq_def.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------

===========================================================================*/

#ifndef LTE_ML1_MGR_PQ_DEF_H
#define LTE_ML1_MGR_PQ_DEF_H

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/
typedef struct pq_msg_qnode_s pq_msg_qnode_s;
typedef struct lte_ml1_mgr_pq_msg_mem_s lte_ml1_mgr_pq_msg_mem_s;
typedef struct simple_pendingq_s simple_pendingq_s;
typedef struct lte_ml1_mgr_pq_data_s lte_ml1_mgr_pq_data_s;

#include <lte_ml1_mgr_qlist.h>
#include "lte_ml1_mgr_msg_req.h"

/*===========================================================================

                   EXTERNAL DEFINITIONS AND TYPES

===========================================================================*/
#define PQ_MSGS_MAX 10

/*! @brief Memory current usage state structure
*/
typedef struct
{
  /*! Memory usage index map */
  uint32 free_map[PQ_MSGS_MAX];
  /*! Current count of used memory array index (used to quickly determine if 
      memory is available) 
  */
  uint32 usage_cnt;  
} lte_ml1_mgr_pq_mem_usage_state_s;

/*! @brief PQ special storage for reqs with pointers in the req
*/
typedef struct
{
  /*! Storage for conn_meas_cfg_info in LTE_CPHY_CONN_MEAS_CFG_REQ.
    Connection measurement cfg req is passed with one parameter 
    "lte_cphy_conn_meas_cfg_info_s" as pointer. So, if we need to buffer this 
    req in pending queue, we store meas_cfg_info to this static location
    (No deep memcpy required or no extra queue node space wastage for common
    cases) 
  */
  lte_cphy_conn_meas_cfg_info_s    conn_meas_cfg_info;
  lte_cphy_conn_meas_cfg_req_s     conn_meas_cfg_req;

  //! @todo Add idle meas cfg if req members change to pointers
 
}lte_ml1_mgr_pq_special_storage_s;

/*! @brief 
    Pending queue message node structure
*/
struct pq_msg_qnode_s
{
  /*! Q List Node */
  qnode n;  
  /*! Pointer to message buffer with MSGR header and payload */
  void*  msg_ptr;  
  /*! Message length */
  uint32  msg_len;  
};


/*! @brief Message memory element structure
*/
struct lte_ml1_mgr_pq_msg_mem_s
{
  /*! Message received by L1M */
  lte_ml1_mgr_msg_u  msg;  
  /*! Message memory array index alloted (used while freeing) */
  uint32  allocated_index;  
};

/*! @brief Internal PQ memory manager structure
*/
typedef struct
{
  /*! Array for PQ upper lyr msg memory allocation sent to ML1 */
  lte_ml1_mgr_pq_msg_mem_s msg_mem[PQ_MSGS_MAX];
  
  /*! Array for PQ Qnode memory allocation for upper lyr msgs sent to ML1 */
  pq_msg_qnode_s node_mem[PQ_MSGS_MAX];
  
  /*! PQ memory allocation usage data */
  lte_ml1_mgr_pq_mem_usage_state_s mem_state;
}lte_ml1_mgr_pq_mem_s;

/*! @brief Defines the data structure representing a simple PendingQ*/
struct simple_pendingq_s
{
  /*! Pending Queue list */
  qlist pending_msgq; 
  /*! Flag indicates whether to append upper layer requests or not in PQ 
        - Set if some request under processing stage in L1 
        - RESET only when both RRC and MAC PQ is empty
  */
   boolean enqueue_mode_is_set;
};

struct lte_ml1_mgr_pq_data_s
{
  /*! MAC requests pending queue */
  simple_pendingq_s                     mac_lyr_pq;
  /*! RRC requests pending queue */
  simple_pendingq_s                     rrc_lyr_pq;
  /*! @todo delete */
  uint32                                num_alloc;
  uint32                                num_free;
  /*! @brief Internal PQ memory manager structure */
  lte_ml1_mgr_pq_mem_s                  lte_ml1_mgr_pq_mem;
};

#endif /* LTE_ML1_MGR_PQ_DEF_H */
