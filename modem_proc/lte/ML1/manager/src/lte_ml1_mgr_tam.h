/*!
  @file
  lte_ml1_mgr_tam.h

  @brief
  Tune Away Manager header
*/

/*===========================================================================

  Copyright (c) 2013 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/ML1/manager/src/lte_ml1_mgr_tam.h#1 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
10/15/14   au      Delay TAO by 20 ms instead of 10, in case of HO failure, so that ABORT can kick in07/15/14   vc      Added back deschedule of TAO obj before reschedule.
07/30/14   vc      Added new obj for DTA handling.
05/02/14   vc      Remove qta_tick and unlock cancel flag handling.
04/11/14   vc      Ignore unlock immediate received within 6ms of TAO 
08/30/12   anm     Initial Version 
 
===========================================================================*/
#include "lte_ml1_comdef.h"
#include "lte_variation.h"
#include "lte_ml1_mgr_msg.h"
#include "lte_ml1_rfmgr_types.h"
#include "lte_ml1_rfmgr_msg.h"
#include <lte_cphy_msg.h>
#include "lte_ml1_mdb.h"
#include "lte_ml1_mgr_struct.h"
#include "lte_ml1_rfmgr_trm.h"

#ifndef LTE_ML1_MGR_TAM_H
#define LTE_ML1_MGR_TAM_H

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

/*===========================================================================

                   INTERNAL DEFINITIONS AND TYPES

===========================================================================*/


extern lte_ml1_mgr_tam_data_s mgr_tam_data;


#define LTE_ML1_TAM_UNLOCK_BY_TIME_TO_CURR_TIME_DELAY_MS 2 
/* Suspend Time worst case 13ms + Margin 2ms */
#define LTE_ML1_TAM_SUSPEND_DURATION_MS                  15// Tsus
#define LTE_ML1_TAM_SUSPEND_WINDOW_MS                    LTE_ML1_SCHDLR_NO_WIN
#define LTE_ML1_SPV_TIMER_DFLT                           10000
#ifdef FEATURE_DTA_FW_RF_DEBUG
#define LTE_ML1_FAKE_DTA_TIMER_DFLT                      20
#endif
#define MIN_DUR_TRM_REQUEST_MS                           50  
#define LTE_ML1_TAM_INVALID_TUNE_AWAY_TIME               LTE_ML1_SCHDLR_SF_TIME_INVALID
#define LTE_ML1_TAM_IRAT_MAXTIME                         30
#define LTE_ML1_TAM_G_WARMUP_TIME_MS                     0// G is already taking care of the warm up time
#define LTE_ML1_QTA_START_TAO_DIFF                       15
#define MIN_DUR_TRM_REQUEST_TIMEOUT_MS                   500
#define LTE_ML1_QTA_TAO_BEFORE_TA_TIME_MS                3
#define LTE_ML1_TAM_MAX_SLTE_ENTER_DURATION_MS           10// FW 4ms (has 1 ms buffer), RF worst case 6 ms (can reduce later)
#define LTE_ML1_LIMIT_TO_HANDLE_UNLOCK_IMMED_BEFORE_TA   6  //Tms
#define LTE_ML1_TAM_TAO_POSTPONE_MS                      10
#define LTE_ML1_TAM_TAO_POSTPONE_HO_FAIL_EXTRA_MS        10
#define LTE_ML1_TAM_SWRP_MARGIN                          2
#define LTE_ML1_TAM_UNLOCK_TAO_CB_START_DELTA             1
#define LTE_ML1_TAM_UNLOCK_TAO_CB_END_DELTA               4

//PBCH cleanup time 20ms + 2ms( buffer)
#define LTE_ML1_TAM_PBCH_CLEANUP_TIME                    22  
//LTE is tuned away for long and hence, may need to search/PBCH on old cell
#define MIN_DUR_TRM_REQUEST_LTA_RESUME_MS                90

/* Limiation set to handle back-to-back G reservations */
/* ~28ms is the MIN gap b/w G page and extended page reservation. */
#define LTE_ML1_MIN_DUR_QTA_TRM_REQUEST_MS                           27
/* Worst case 20ms resume + 20ms suspend */
#define LTE_ML1_MIN_DUR_LTA_TRM_REQUEST_MS                           50
/* Max Acceptable time difference between TAO start Cb and TAO start time in ms
   when TAO is scheduled as ASAP*/
#define LTE_ML1_MAX_ASAP_THRESH                                      200

#define LTE_ML1_TAO_START_CB_WIN                              ((3* LTE_NUM_RTC_TICKS_PER_MS)/2)
/*===========================================================================

  FUNCTION:  lte_ml1_mgr_tam_db_init

===========================================================================*/
/*!
  @brief
  TAM database init
 
  @return
  
*/
/*=========================================================================*/
void lte_ml1_mgr_tam_db_init( void );


/*===========================================================================

  FUNCTION:  lte_ml1_mgr_tam_handle_trm_unlock_ind

===========================================================================*/
/*!
  @brief
  Handle LTE_ML1_RFMGR_TRM_UNLOCK_IND from RF Mgr
 
  @return
  
*/
/*=========================================================================*/
void lte_ml1_mgr_tam_handle_trm_unlock_ind
(
  lte_ml1_rfmgr_msg_trm_unlock_ind_s  *cfg_ptr,
  /*! Pointer to right instance */
  lte_ml1_manager_data_s              *mgr_data_inst
);

/*===========================================================================

  FUNCTION:  lte_ml1_mgr_tam_handle_trm_chain_grant_ind

===========================================================================*/
/*!
  @brief
  Handle LTE_ML1_RFMGR_TRM_CHAIN_GRANT_IND from RF Mgr
 
  @return
  
*/
/*=========================================================================*/
void lte_ml1_mgr_tam_handle_trm_chain_grant_ind
(
  /*! Configuration pointer */ 
  lte_ml1_rfmgr_msg_trm_chain_grant_ind_s  *cfg_ptr,
  /*! Pointer to right instance */
  lte_ml1_manager_data_s                   *mgr_data_inst
);

/*===========================================================================

  FUNCTION:  lte_ml1_mgr_get_tam_state

===========================================================================*/
/*!
  @brief
  Used by other modules to get the TAM state
  @return
  The current TAM state
*/
/*=========================================================================*/

lte_ml1_mgr_tam_state_e  lte_ml1_mgr_get_tam_state(lte_ml1_manager_data_s  *mgr_data_inst);
/*===========================================================================

  FUNCTION:  lte_ml1_mgr_tam_handle_msgs

===========================================================================*/
/*!
  @brief
  Used for reception of messages by TAM  
  It is expected that the L1 message redirector directs messages to this
  callback for processing by the downlink manager.

  @return
  None
*/
/*=========================================================================*/
void lte_ml1_mgr_tam_handle_msgs
(
  uint32      length,
  /**< Pointer to to the TAM msg message */
  void const *tam_msg, 
  /*! Pointer to right instance */
  lte_ml1_manager_data_s  *mgr_data_inst
);


/*===========================================================================

  FUNCTION:  lte_ml1_mgr_tam_send_rf_status_ind

===========================================================================*/
/*!
  @brief
  Send RF Status Indication to Upper layers  
 
  @return
  
*/
/*=========================================================================*/
void lte_ml1_mgr_tam_send_rf_status_ind
(
  /*! Pointer to right instance */
  lte_ml1_manager_data_s  *mgr_data_inst,
  /*! RF Status */ 
  lte_ml1_tam_rf_status_e  rf_status
);

/*===========================================================================

  FUNCTION:  lte_ml1_mgr_pq_action_in_tam_suspend

===========================================================================*/
/*!
    @brief
    Handling of new msgs during the TAM suspend, before doing the pending
    queue processing
 
    @return
    TAM Pending queue action . Values can be
    PROCESS_MSG,
    BUFFER_MSG,
    IGNORE_MSG
 
*/
/*=========================================================================*/
tam_pq_action_e  lte_ml1_mgr_pq_action_in_tam_suspend
(
  /*! Pointer to right instance */
  lte_ml1_manager_data_s *mgr_data_inst,
  /*! CPHY msg umid*/
  msgr_umid_type         umid,
  /*! Message */
  void                   *msg
);

/*===========================================================================

  FUNCTION:  lte_ml1_mgr_next_action_in_tam_suspend

===========================================================================*/
/*!
    @brief
    Perform the next action for the new messages received during TAM suspend.
    For e.g. sending fake and immediate cnfs and sending pending queue indications
    to dequeue msgs(for e.g. in case of ABORT/STOP)
 
    @return
    snone
 
*/
/*=========================================================================*/
void  lte_ml1_mgr_next_action_in_tam_suspend
(
  /*! Pointer to right instance */
  lte_ml1_manager_data_s *mgr_data_inst,
  /*! CPHY msg umid*/
  msgr_umid_type         umid
);

#ifdef FEATURE_DTA_FW_RF_DEBUG
/*===========================================================================

  FUNCTION:  lte_ml1_mgr_create_fake_dta_timer

===========================================================================*/
/*!
  @brief
  Create Fake DTA Timer 
 
  @return
  
*/
/*=========================================================================*/
void lte_ml1_mgr_create_fake_dta_timer( lte_ml1_manager_data_s  *mgr_data_inst );
#endif
/*===========================================================================

  FUNCTION:  lte_ml1_mgr_create_spv_timer

===========================================================================*/
/*!
  @brief
  Create SPV Timer 
 
  @return
  
*/
/*=========================================================================*/
void lte_ml1_mgr_create_spv_timer( lte_ml1_manager_data_s  *mgr_data_inst );
/*===========================================================================

  FUNCTION:  lte_ml1_mgr_tam_register_objs

===========================================================================*/
/*!
    @brief
    Register ML1 mgr TAM schdlr objects

    @return
    None
*/
/*=========================================================================*/
void lte_ml1_mgr_tam_register_objs( lte_ml1_manager_data_s  *mgr_data_inst );
/*===========================================================================

  FUNCTION:  lte_ml1_mgr_tam_deregister_objs

===========================================================================*/
/*!
    @brief
    De-register ML1 mgr  TAM schdlr objects

    @return
    None
*/
/*=========================================================================*/
void lte_ml1_mgr_tam_deregister_objs( void );

/*===========================================================================

  FUNCTION:  lte_ml1_mgr_tam_handle_trm_unlock_ind

===========================================================================*/
/*!
  @brief
  Handle LTE_ML1_RFMGR_TRM_UNLOCK_CANCEL_IND from RF Mgr
   
  @return
  None
*/
/*=========================================================================*/
void lte_ml1_mgr_tam_handle_trm_unlock_cancel_ind
(
  lte_ml1_rfmgr_msg_trm_unlock_cancel_ind_s  *cfg_ptr,
     /*! Pointer to right instance */
  lte_ml1_manager_data_s              *mgr_data_inst
);
/*===========================================================================

  FUNCTION:  lte_ml1_mgr_tam_actions_after_suspend

===========================================================================*/
/*!
  @brief
  TAM actions required after Suspend is done in case of CPHY_SUSPEND/
  ABORT/STOP
   
  @return
  None
*/
/*=========================================================================*/
void lte_ml1_mgr_tam_actions_after_suspend
(
     /*! Pointer to right instance */
  lte_ml1_manager_data_s              *mgr_data_inst
);
/*===========================================================================

  FUNCTION:  lte_ml1_mgr_process_tam_msg

===========================================================================*/
/*!
    @brief
    Function to process TAM msgs

    @detail
    
    @return

*/
/*=========================================================================*/
boolean lte_ml1_mgr_process_tam_msg
(
  /*! Pointer to right instance */
  lte_ml1_manager_data_s *mgr_data_inst,
  lte_ml1_id_type msg_id,
  uint8* msg,
  uint32 len
);
/*===========================================================================
  FUNCTION:  lte_ml1_mgr_tam_schdl_obj

===========================================================================*/
/*!
    @brief
    Schdl Tune Away Object 

    @detail
    Currently, use to schedule cfg obj with ASAP/TNXT type on current cell

    @return
    None
*/
/*=========================================================================*/
void lte_ml1_mgr_tam_schdl_obj
( 
  /*! Pointer to right instance */
  lte_ml1_manager_data_s             *mgr_data_inst,
  /*! Start time sf */
  lte_ml1_schdlr_sf_time_t           start_time_sf,
  /*! Obj priority */
  lte_ml1_schdlr_priority_e          priority,
  /*! The start_time type */
  lte_ml1_schdlr_start_time_type_e   start_time_type,
    /* Object id */
  lte_ml1_schdlr_obj_id_e            obj_id,
  /* Deschedule previous obj before new schedule if this is true*/
  boolean                            desched_before_resched
);

/*===========================================================================

  FUNCTION:  lte_ml1_mgr_tam_data_init

===========================================================================*/
/*!
    @brief
    Init TAM data

    @return
    None
*/
/*=========================================================================*/
void lte_ml1_mgr_tam_data_init
(
  lte_ml1_manager_data_s  *mgr_data_inst
);
/*===========================================================================

  FUNCTION:  lte_ml1_mgr_tam_suspend_time

===========================================================================*/
/*!
    @brief
    Calculates the time at which TRM neeeds to be released

    @return
    None
*/
/*=========================================================================*/
uint32 lte_ml1_mgr_tam_suspend_time
(
  lte_ml1_manager_data_s  *mgr_data_inst
);
/*===========================================================================

  FUNCTION:  lte_ml1_mgr_tam_clear_db

===========================================================================*/
/*!
    @brief
    Clear TAM DB and deregister TAO 

    @return
    None
*/
/*=========================================================================*/
void lte_ml1_mgr_tam_clear_db
(
  lte_ml1_manager_data_s  *mgr_data_inst
);
/*===========================================================================

  FUNCTION:  lte_ml1_mgr_tam_collision_detected

===========================================================================*/
/*!
    @brief
    To check if collision detected with TAO 

    @return
    True if detected, False if not 
*/
/*=========================================================================*/
boolean lte_ml1_mgr_tam_collision_detected
( 
  uint16                           min_time_reqd,
      /*! Pointer to right instance */
  lte_ml1_manager_data_s            *mgr_data_inst
);
/*==============================================================================

  FUNCTION:  lte_ml1_tam_mgr_cfg_obj_start_cb

==============================================================================*/
/*!
  @brief
  ML1 TAM cfg obj start cb

  @return
  None

*/
/*============================================================================*/
void lte_ml1_tam_mgr_cfg_obj_start_cb
(
  void                   *user_data_ptr
);
   
/*===========================================================================

  FUNCTION:  lte_ml1_mgr_tam_handle_ll_conflict_check_cnf

===========================================================================*/
/*!
  @brief
  Handle LTE_LL1_SYS_CONFLICT_CHECK_CNF 
   
  @return
  None
*/
/*=========================================================================*/
#ifndef FEATURE_BOLT_MODEM
void lte_ml1_mgr_tam_handle_ll_conflict_check_cnf
(
   lte_LL1_sys_conflict_check_cnf_struct       *conflict_check_cnf,
      /*! Pointer to right instance */
   lte_ml1_manager_data_s                       *mgr_data_inst
);
#endif
/*===========================================================================

  FUNCTION:  lte_ml1_mgr_tam_send_ll_conflict_check_req

===========================================================================*/
/*!
    @brief
    Send Conflict Check Request to FW for QTA

    @return
    None
*/
/*=========================================================================*/
#ifndef FEATURE_BOLT_MODEM
void lte_ml1_mgr_tam_send_ll_conflict_check_req
( 
  /*! Pointer to right instance */
  lte_ml1_manager_data_s            *mgr_data_inst,
  boolean                           conflict_check_stop
);
#endif
/*===========================================================================

  FUNCTION:  lte_ml1_mgr_tam_get_tao_prio

===========================================================================*/
/*!
    @brief
    To return TAO priority if QTA/LTA

    @return
    TAO priority
*/
/*=========================================================================*/
lte_ml1_schdlr_priority_e lte_ml1_mgr_tam_get_tao_prio
( 
  /*! Pointer to right instance */
  lte_ml1_manager_data_s            *mgr_data_inst
);

/*===========================================================================

  FUNCTION:  lte_ml1_mgr_tam_handle_qta_switch_ind

===========================================================================*/
/*!
  @brief
  Handle LTE_ML1_MGR_QTA_TO_LTA_SWITCH_IND 
   
  @return
  None
*/
/*=========================================================================*/
void lte_ml1_mgr_tam_handle_qta_switch_ind
(
   lte_ml1_mgr_qta_to_lta_switch_ind_s    *qta_start_ind,
      /*! Pointer to right instance */
   lte_ml1_manager_data_s                       *mgr_data_inst
);

/*===========================================================================

  FUNCTION:  lte_ml1_mgr_tam_qta_cleanup

===========================================================================*/
/*!
    @brief
    QTA/DTA related cleaning
    1. Enable Sleep 2. FW cleanup 3. G cleanup 

    @return
    None
*/
/*=========================================================================*/
void lte_ml1_mgr_tam_qta_cleanup
( 
  /*! Pointer to right instance */
  lte_ml1_manager_data_s                *mgr_data_inst
);

/*===========================================================================

  FUNCTION:  lte_ml1_mgr_tam_handle_ll_conflict_check_ind

===========================================================================*/
/*!
  @brief
  Handle LTE_LL1_SYS_CONFLICT_CHECK_IND 
   
  @return
  None
*/
/*=========================================================================*/
void lte_ml1_mgr_tam_handle_ll_conflict_check_ind
(
   lte_LL1_sys_conflict_check_ind_struct       *conflict_check_cnf,
      /*! Pointer to right instance */
   lte_ml1_manager_data_s                       *mgr_data_inst
);
/*===========================================================================

  FUNCTION:  lte_ml1_mgr_tam_if_no_rf_ind_recvd

===========================================================================*/
/*!
  @brief
  Check if No RF ind received
 
  @return
  TRUE or FALSE depending on whether NO RF indication received or not.
*/
/*=========================================================================*/
boolean lte_ml1_mgr_tam_if_no_rf_ind_recvd
(
   lte_ml1_manager_data_s                       *mgr_data_inst
);

/*===========================================================================

  FUNCTION:  lte_ml1_mgr_tam_send_rfless_wakeup

===========================================================================*/
/*!
  @brief
  Send RFless wakeup indication to sleepmgr
 
  @return
  None
*/
/*=========================================================================*/
void lte_ml1_mgr_tam_send_rfless_wakeup
(
   lte_ml1_manager_data_s                       *mgr_data_inst
);
/*===========================================================================

  FUNCTION:  lte_ml1_mgr_tam_reset_rlf_qta_lta

===========================================================================*/
/*!
  @brief
  Reset rlf_lta_qta flag
 
  @return
  None
*/
/*=========================================================================*/
void lte_ml1_mgr_tam_reset_rlf_qta_lta
(
   lte_ml1_manager_data_s                       *mgr_data_inst
);

/*===========================================================================

  FUNCTION:  lte_ml1_mgr_tam_print_tam_data

===========================================================================*/
/*!
    @brief
    Print debug data for TAM

    @return
    None
*/
/*=========================================================================*/
void lte_ml1_mgr_tam_print_tam_data
(
  /*! Pointer to right instance */
  lte_ml1_manager_data_s                *mgr_data_inst
);

/*===========================================================================

  FUNCTION:  lte_ml1_mgr_tam_set_ho_failure

===========================================================================*/
/*!
    @brief
    Set HO failure flag in TAM.

    @return
    None
*/
/*=========================================================================*/
void lte_ml1_mgr_tam_set_reset_ho_failure
(
  /*! Pointer to right instance */
  lte_ml1_manager_data_s                *mgr_data_inst,

  boolean                               ho_failed
);

/*===========================================================================

  FUNCTION:  lte_ml1_mgr_store_tuneaway_data

===========================================================================*/
/*!
    @brief
    Store Tuneaway data  in Tam db

    @return
    None
*/
/*=========================================================================*/
void lte_ml1_mgr_store_tuneaway_data
(
  lte_mem_instance_type         inst_num,
  
  uint32                        ta_subfn_cnt,

  boolean 			            is_suspend
);
/*===========================================================================

  FUNCTION:  lte_ml1_mgr_tam_reset_rf_less_wakeup_sent

===========================================================================*/
/*!
    @brief
    Reset rf_less_wakeup_sent flag if set
    
    @return
    None
*/
/*=========================================================================*/
void lte_ml1_mgr_tam_reset_rf_less_wakeup_sent
(
    /*! Pointer to right instance */
  lte_ml1_manager_data_s                *mgr_data_inst
);
  

/*===========================================================================

  FUNCTION:  lte_ml1_mgr_tam_postpone_tao_start

===========================================================================*/
/*!
    @brief
    Delay TAO start point
    
    @details
    TAO is delayed due to current conflicts

    @return
    None
*/
/*=========================================================================*/
void lte_ml1_mgr_tam_postpone_tao_start
(
    /*! Pointer to right instance */
  lte_ml1_manager_data_s                *mgr_data_inst,
  
  /*! Delay TAO start (ms) from current time */
  lte_ml1_schdlr_sf_time_t              delay_from_now
);

/*===========================================================================

  FUNCTION:  lte_ml1_mgr_tam_get_current_systime_info

===========================================================================*/
/*!
  @brief
    Get current system time
   
  @return
  None
*/
/*=========================================================================*/
void lte_ml1_mgr_tam_get_current_systime_info
(
      /*! Pointer to right instance */
  lte_ml1_schdlr_systime_info_s *curr_sys_time_ptr
);  

/*===========================================================================

  FUNCTION:  lte_ml1_mgr_tam_handle_buffered_trm_unlock_ind

===========================================================================*/
/*!
  @brief
  Handle the Buffered LTE_ML1_RFMGR_TRM_UNLOCK_IND from RF Mgr 
   
  @return
  None
*/
/*=========================================================================*/
void lte_ml1_mgr_tam_handle_buffered_trm_unlock_ind
(
  lte_mem_instance_type  instance 
);


#endif /* LTE_ML1_MGR_TAM_H */
