/*!
  @file
  lte_ml1_mgr_tam_log.h

  @brief
  Tune Away Event log Code
*/

/*===========================================================================

  Copyright (c) 2013 QUALCOMM Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/ML1/manager/src/lte_ml1_mgr_tam_log.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------  
02/18/14   avi     Initial Version
===========================================================================*/

#ifndef LTE_ML1_MGR_TAM_LOG_H
#define LTE_ML1_MGR_TAM_LOG_H

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

#include "lte_ml1_mgr_struct.h"
#include "lte_ml1_mgr_stm.h"
#include "lte_ml1_comdef.h"
#include "lte_ml1_mgr_if.h"
#include <lte_ml1_log.h>
#include <lte_ml1_rfmgr_trm.h>
#include <lte_log_codes.h>


/*===========================================================================

                   EXTERNAL DEFINITIONS AND TYPES

===========================================================================*/



#define LTE_ML1_MGR_TA_RECORD_MAX_CNT 1

/* Tune away event name */
typedef enum
{
  UNLOCK_BY = 0,
  UNLOCK_IMMEDIATE,
  UNLOCK_CANCEL,
  CHAIN_GRANT,
  QTA_TO_LTA,
  TUNEAWAY_START,
  TUNEAWAY_END,
  TRM_RELEASE,
  L_WAKEUP_TRM_DENIAL,
  L_WAKEUP_WITH_RF,
  SPV_TMR_EXPIRY
}lte_ml1_mgr_tam_log_ta_event_e;

/* suspend resume cause */
typedef enum
{
  LTE_ML1_TAM_IRAT_1X_RESEL,                     /*!<  L->1X [CDMA1XRTT] resel */
  LTE_ML1_TAM_IRAT_DO_RESEL,          			 /*!<  L->DO [CDMAHRPD] resel */
  LTE_ML1_TAM_IRAT_WCDMA_RESEL,      			 /*!<  L->WCDMA [UTRAN] resel */
  LTE_ML1_TAM_IRAT_TDSCDMA_RESEL,                /*!<  L->TDSCDMA [UTRAN] resel */
  LTE_ML1_TAM_IRAT_GERAN_RESEL,                  /*!<  L->GERAN [GSM] resel */
  LTE_ML1_TAM_IRAT_1X_HANDOVER,                  /*!<  L->1X [CDMA1XRTT] handover */
  LTE_ML1_TAM_IRAT_DO_HANDOVER,                  /*!<  L->DO [CDMAHRPD] handover */
  LTE_ML1_TAM_IRAT_WCDMA_HANDOVER,               /*!<  L->WCDMA [UTRAN] handover */
  LTE_ML1_TAM_IRAT_GERAN_HANDOVER,               /*!<  L->GERAN [GSM] handover */
  LTE_ML1_TAM_IRAT_BPLMN,                        /*!<  L->other RAT BPLMN */
  LTE_ML1_TAM_IRAT_CDMA_REVERSE_TIME_TRANSFER,   /*!<  L->CDMA reverse time transfer */
  LTE_ML1_TAM_IRAT_GERAN_CCO_NACC,               /*!<  L->G RAT cell change order recvd in LTE conncted mode  */
  LTE_ML1_TAM_IRAT_CGI,                          /*!<  L->other RAT CGI */
  LTE_ML1_TAM_IRAT_TDSCDMA_HANDOVER,             /*!<  L->WCDMA [UTRAN] handover */
  LTE_ML1_TAM_LTA,								 /* Long Tune Away */
  LTE_ML1_TAM_QTA,				 				 /* Quick Tune Away */	
  LTE_ML1_TAM_DTA,                               /* Diversity Tune Away */
  LTE_ML1_TAM_CPHY_UNDEFINED                     /* undefined cphy cause  */
}lte_ml1_mgr_tam_log_l_suspend_cause_e;



/*Tune away event type */ 
typedef enum
{
  LTE_ML1_L_SUSPEND = 0,                  /* For all External Cphy suspend resume event */
  LTE_ML1_TA_LTA,
  LTE_ML1_TA_QTA,
  LTE_ML1_TA_DTA  
}lte_ml1_mgr_tam_log_ta_type_e;

/* Event log time  */
typedef struct
{
  uint32   sfn:10;						 ///<radio frame 0-1023
  uint32   sub_fn:4;					 ///< 0~9
  uint32   reserved_0:18;                ///Reserved.  Used for bitfield padding to nearest 32-bit boundary

  uint32   sclk_time;
}lte_ml1_mgr_tam_log_time_s;

  
typedef struct
{ 
  
  uint32     tao_start_sfn:10;						   ///<radio frame 0-1023
  uint32	 tao_start_sub_fn:4;					   ///< 0~9
  uint32     winning_client:8;                         ///TRM winning Client 
  uint32	 reserved_0:10; 			               ///Reserved.  Used for bitfield padding to nearest 32-bit boundary

  uint32     unlock_schdlr_sclk;                       ///Unlock Schdlr sclk 
  uint32     ta_relative_ms;                           ///TRM reserve relative time from now in ms       

}lte_ml1_mgr_log_unlock_info_s;




typedef struct
{
  lte_ml1_mgr_tam_log_time_s  event_time;                   //Event time
  
  uint32                   	  ta_event:6;                   //TA event name
  uint32                      ta_type:4;                    //TA type     
  uint32                      lte_state:4;                  //Current  MGR state   
  uint32                      suspend_cause:8;              
  uint32                      reserved_0:10;                ///Reserved.  Used for bitfield padding to nearest 32-bit boundary
  
  uint32                      chain_mask:16;                //Currently Chains granted to LTE   
  uint32                      spv_timer_value:16;           // max value 10000
   
  uint32                      trm_release_sclk;             //Valid for trm_release , 0 otherwise

  /*Valid for Tuneaway start */
  uint32                      ta_transition_time_gap_start;  //Time taken to suspend LTE


  /* Valid for Tuneaway end */	
  uint32                      ta_transition_time_gap_end;     //Time taken to Resume LTE
  lte_ml1_mgr_tam_log_time_s  ta_start;                      //Tune away start time
  lte_ml1_mgr_tam_log_time_s  ta_end;                        //Tune away end time  
  uint32                      ta_duration_ms;                // Total tuneaway duration in ms
         


  /*Valid for UNLOCK_BY UNLOCK_IMM QTA-LTA */ 
  lte_ml1_mgr_log_unlock_info_s    unlock_info;              //Unlock event information (tao sfn,sub_fn, unlock schdl sclk, 
  															 //	trm relative time in ms)
}lte_ml1_mgr_tam_log_ta_record_s;  



typedef struct
{
  
	
  uint32     version:8;                                     //Version  
  uint32     num_records:8;                                 // num_records 

  uint32     reserved_0:16;
  	
 /* TA event records */
  lte_ml1_mgr_tam_log_ta_record_s record[LTE_ML1_MGR_TA_RECORD_MAX_CNT];
}lte_ml1_mgr_tam_log_ta_records_s;

/*===========================================================================

                    EXTERNAL FUNCTION PROTOTYPES

===========================================================================*/

/*===========================================================================
	 
	   FUNCTION:  lte_ml1_mgr_log_tam_unlock_event
	   
 ===========================================================================*/
 /*!
	 @brief
	 Tuneaway  Unlock event logging function
 
	 @return
	 None
 */
 /*=========================================================================*/
 void lte_ml1_mgr_log_tam_unlock_event
 (
   /*! Pointer to right instance */
   lte_ml1_manager_data_s						  *mgr_data_inst,
   /*! Tuneaway Event name	   */
   lte_ml1_mgr_tam_log_ta_event_e				   tam_event,
   /*! Unlock by Cfg pointer */
   lte_ml1_rfmgr_msg_trm_unlock_ind_s             *unlock_cfg_ptr	  
 );


/*===========================================================================
	
	  FUNCTION:  lte_ml1_mgr_log_tam_event
	  
===========================================================================*/
/*!
	@brief
	Tuneaway event logging function
	
	@return
	None
*/
/*=========================================================================*/
void lte_ml1_mgr_log_tam_event
(
  /*! Pointer to right instance */
  lte_ml1_manager_data_s						 *mgr_data_inst,
  lte_ml1_mgr_tam_log_ta_event_e 				  tam_event
);

#endif /* LTE_ML1_MGR_TAM_LOG_H */

