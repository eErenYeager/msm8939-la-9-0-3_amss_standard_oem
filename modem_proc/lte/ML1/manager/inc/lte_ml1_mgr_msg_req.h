/*!
  @file
  lte_ml1_mgr_msg_req.h

  @brief
  Contains all ML1 internal and external msgs that ML1 MGR can receive
*/

/*===========================================================================

  Copyright (c) 2008 QUALCOMM Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/ML1/manager/inc/lte_ml1_mgr_msg_req.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------

===========================================================================*/

#ifndef LTE_ML1_MGR_MSG_REQ_H
#define LTE_ML1_MGR_MSG_REQ_H

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/
#include "lte_cphy_msg.h"
#include "lte_cphy_rssi_msg.h"
#include "intf_async_msg.h"
#include "lte_ml1_gm_msg.h"
#include "lte_ml1_mgr_msg.h"
#include "lte_cxm_msg.h"
#include "lte_ml1_coex_msg.h"
#include "wcn_coex_mgr.h"
#include "wwan_coex_mgr.h"
#include <intf_dl.h>
#include <intf_srch.h>
#include <intf_sys.h>
#include <intf_ul.h>

/*===========================================================================

                   EXTERNAL DEFINITIONS AND TYPES

===========================================================================*/

/*===========================================================================
******************************MGR MSG DEFINITION*****************************
===========================================================================*/
/*! @brief Union of all ML1 internal msgs that ML1 MGR can receive
*/
typedef union
{
  /*! PQ process Indication */
  msgr_hdr_struct_type    lte_ml1_mgr_process_pq_ind;
  //! @todo Add as new internal msgs added
} lte_ml1_mgr_int_msg_u;

/*! @brief Union of all external msgs that ML1 MGR can receive
*/
typedef union
{
  /* Upper layer requests */
  lte_cphy_start_req_s                       lte_cphy_start_req;
  lte_cphy_start_cnf_s                       lte_cphy_start_cnf;
  lte_cphy_stop_req_s                        lte_cphy_stop_req;
  lte_cphy_stop_cnf_s                        lte_cphy_stop_cnf;
  lte_cphy_common_cfg_req_s                  lte_cphy_common_cfg_req;
  lte_cphy_common_cfg_cnf_s                  lte_cphy_common_cfg_cnf;
  lte_cphy_dedicated_cfg_req_s               lte_cphy_dedicated_cfg_req;
  lte_cphy_dedicated_cfg_cnf_s               lte_cphy_dedicated_cfg_cnf;
  lte_cphy_handover_req_s                    lte_cphy_handover_req;
  lte_cphy_handover_cnf_s                    lte_cphy_handover_cnf;
  lte_cphy_abort_req_s                       lte_cphy_abort_req;
  lte_cphy_abort_cnf_s                       lte_cphy_abort_cnf;
  lte_cphy_con_release_req_s                 lte_cphy_con_release_req;
  lte_cphy_con_release_cnf_s                 lte_cphy_con_release_cnf;
  lte_cphy_acq_req_s                         lte_cphy_acq_req;
  lte_cphy_acq_cnf_s                         lte_cphy_acq_cnf;
  lte_cphy_system_scan_req_s                 lte_cphy_system_scan_req;
  lte_cphy_system_scan_cnf_s                 lte_cphy_system_scan_cnf;
  lte_cphy_band_scan_req_s                   lte_cphy_band_scan_req;
  lte_cphy_band_scan_cnf_s                   lte_cphy_band_scan_cnf;
  lte_cphy_cell_select_req_s                 lte_cphy_cell_select_req;
  lte_cphy_cell_select_cnf_s                 lte_cphy_cell_select_cnf;
  lte_cphy_start_rach_req_s                  lte_cphy_start_rach_req;
  lte_cphy_start_rach_cnf_s                  lte_cphy_start_rach_cnf;
  lte_cphy_rach_rc_req_s                     lte_cphy_rach_rc_req;
  lte_cphy_rach_rc_cnf_s                     lte_cphy_rach_rc_cnf;
  lte_cphy_rar_params_req_s                  lte_cphy_rar_params_req;
  lte_cphy_rach_abort_req_s                  lte_cphy_rach_abort_req;
  lte_cphy_rach_abort_cnf_s                  lte_cphy_rach_abort_cnf;
  lte_cphy_rach_cfg_req_s                    lte_cphy_rach_cfg_req;
  lte_cphy_rach_cfg_cnf_s                    lte_cphy_rach_cfg_cnf;
  lte_mac_contention_result_ind_msg_s        lte_mac_contention_result_ind_msg;
  lte_cphy_out_of_sync_req_s                 lte_cphy_out_of_sync_req;
  lte_cphy_apply_ta_params_req_s             lte_cphy_apply_ta_params_req;
  lte_cphy_sib_sched_req_s                   lte_cphy_sib_sched_req;
  lte_cphy_cell_bar_req_s                    lte_cphy_cell_bar_req;
  lte_cphy_blacklisted_csg_pci_range_ind_s   lte_cphy_blacklisted_csg_pci_range_ind;
  lte_cphy_idle_drx_cfg_req_s                lte_cphy_idle_drx_cfg_req;
  lte_cphy_cell_resel_cancel_req_s           lte_cphy_cell_resel_cancel_req;
  lte_cphy_idle_meas_cfg_req_s               lte_cphy_idle_meas_cfg_req;
  lte_cphy_idle_meas_cfg_cnf_s               lte_cphy_idle_meas_cfg_cnf;
  lte_cphy_irat_cdma_system_time_chg_req_s   lte_cphy_irat_cdma_system_time_chg_req;
  lte_cphy_conn_meas_cfg_req_s               lte_cphy_conn_meas_cfg_req;
  lte_cphy_cancel_conn_req_s                 lte_cphy_cancel_conn_req;
  lte_cphy_suspend_req_s                     lte_cphy_suspend_req;
  lte_cphy_suspend_cnf_s                     lte_cphy_suspend_cnf;
  lte_cphy_resume_req_s                      lte_cphy_resume_req;
  lte_cphy_resume_cnf_s                      lte_cphy_resume_cnf;
  lte_cphy_tdd_cfg_req_s                     lte_cphy_tdd_cfg_req;
  lte_cphy_tdd_cfg_cnf_s                     lte_cphy_tdd_cfg_cnf;
  lte_cphy_signal_report_cfg_ind_s           lte_cphy_signal_report_cfg_ind;

  lte_cphy_msg_bplmn_start_req_s             lte_cphy_msg_bplmn_start_req;
  lte_cphy_msg_bplmn_stop_req_s              lte_cphy_msg_bplmn_stop_req;
  lte_cphy_msg_bplmn_stop_cnf_s              lte_cphy_msg_bplmn_stop_cnf;
  lte_cphy_msg_bplmn_cell_req_s              lte_cphy_msg_bplmn_cell_req;
  lte_cphy_msg_bplmn_cell_cnf_s              lte_cphy_msg_bplmn_cell_cnf;
  lte_cphy_msg_bplmn_skip_req_s              lte_cphy_msg_bplmn_skip_req;
  lte_cphy_ra_one_off_ind_s                  lte_cphy_ra_one_off_ind;
  lte_cphy_ifreq_otdoa_ind_s                 lte_cphy_ifreq_otdoa_ind;
  lte_cphy_prs_update_cells_req_s            lte_cphy_prs_update_cells_req;

  /* Supervisor Requests */
  msgr_spr_loopback_s          msgr_spr_loopback;
  msgr_spr_loopback_reply_s    msgr_spr_loopback_reply;

  /* LL1 messages received by ML1 */
  lte_LL1_dl_ch_config_cnf_msg_struct                lte_LL1_dl_ch_config_cnf_msg;
  lte_LL1_dl_ch_state_change_cnf_msg_struct          lte_LL1_dl_ch_state_change_cnf_msg;
  lte_LL1_dl_pdcch_rnti_change_cnf_msg_struct        lte_LL1_dl_pdcch_rnti_change_cnf_msg;
  lte_LL1_dl_pbch_state_change_cnf_msg_struct        lte_LL1_dl_pbch_state_change_cnf_msg;
  lte_LL1_dl_enable_disable_cnf_msg_struct           lte_LL1_dl_enable_disable_cnf_msg;

  lte_LL1_meas_scell_cnf_msg_struct                  lte_LL1_meas_scell_cnf_msg;
  lte_LL1_meas_tracking_ncell_cnf_msg_struct         lte_LL1_meas_tracking_ncell_cnf_msg;
  lte_LL1_meas_tracking_ncell_abort_cnf_msg_struct   lte_LL1_meas_tracking_ncell_abort_cnf_msg;
  lte_LL1_meas_ttl_ftl_ncell_cnf_msg_struct          lte_LL1_meas_ttl_ftl_ncell_cnf_msg;
  lte_LL1_dl_si_start_collect_cnf_msg_struct         lte_LL1_dl_si_start_collect_cnf_msg;
  lte_LL1_dl_si_stop_collect_cnf_msg_struct          lte_LL1_dl_si_stop_collect_cnf_msg;

  //Begin odyssey only
  lte_LL1_dl_pdsch_config_cnf_msg_struct             lte_LL1_dl_pdsch_config_cnf_msg;
  //end

  lte_LL1_dl_pdcch_phich_ind_msg_struct              lte_LL1_dl_pdcch_phich_ind_msg;
  lte_LL1_dl_pdsch_stat_ind_msg_struct               lte_LL1_dl_pdsch_stat_ind_msg;
  lte_LL1_dl_pdsch_test_cnf_msg_struct               lte_LL1_dl_pdsch_test_cnf_msg;

  lte_LL1_sys_config_serving_cell_cnf_msg_struct     lte_LL1_sys_config_serving_cell_cnf_msg;
  lte_LL1_sys_slam_timing_cnf_msg_struct             lte_LL1_sys_slam_timing_cnf_msg;
  lte_LL1_sys_adj_timing_cnf_msg_struct              lte_LL1_sys_adj_timing_cnf_msg;
  lte_LL1_sys_ta_adj_cnf_msg_struct                  lte_LL1_sys_ta_adj_cnf_msg;
  lte_LL1_sys_param_config_cnf_msg_struct            lte_LL1_sys_param_config_cnf_msg;
  lte_LL1_sys_subframe_ind_msg_struct                lte_LL1_sys_subframe_ind_msg;
  lte_LL1_sys_stop_sys_time_cnf_msg_struct           lte_LL1_sys_stop_sys_time_cnf_msg;

  lte_LL1_log_srch_pss_results_ind_msg_struct
    lte_LL1_sys_log_lte_ll1_pss_result_log_c_ind_msg;

  lte_LL1_srch_front_end_config_cnf_msg_struct       lte_LL1_srch_front_end_config_cnf_msg;
  lte_LL1_srch_init_acq_cnf_msg_struct               lte_LL1_srch_init_acq_cnf_msg;
  lte_LL1_srch_serving_cell_srch_cnf_msg_struct      lte_LL1_srch_serving_cell_srch_cnf_msg;
  lte_LL1_srch_neighbor_cell_srch_cnf_msg_struct     lte_LL1_srch_neighbor_cell_srch_cnf_msg;
  lte_LL1_dl_pbch_decode_cnf_msg_struct              lte_LL1_srch_pbch_decode_cnf_msg;
  lte_LL1_srch_init_acq_abort_cnf_msg_struct         lte_LL1_srch_init_acq_abort_cnf_msg;
  lte_LL1_srch_black_list_cnf_msg_struct             lte_LL1_srch_black_list_cnf_msg;

  lte_LL1_async_read_hw_mem_cnf_msg_struct           lte_LL1_async_read_hw_mem_cnf_msg;
  lte_LL1_async_write_hw_mem_cnf_msg_struct          lte_LL1_async_write_hw_mem_cnf_msg;
  lte_LL1_async_reset_cnf_msg_struct                 lte_LL1_async_reset_cnf_msg;

  lte_LL1_ul_phychan_config_cnf_msg_struct           lte_LL1_ul_phychan_config_cnf_msg;
  lte_LL1_ul_phychan_deconfig_cnf_msg_struct         lte_LL1_ul_phychan_deconfig_cnf_msg;
  lte_LL1_ul_ifft_scaling_change_cnf_msg_struct      lte_LL1_ul_ifft_scaling_change_cnf_msg;
  lte_LL1_ul_tx_window_length_change_cnf_msg_struct  lte_LL1_ul_tx_window_length_change_cnf_msg;

  lte_LL1_dl_csf_config_cnf_msg_struct               lte_LL1_dl_csf_config_cnf_msg;
  lte_LL1_dl_csf_deconfig_cnf_msg_struct             lte_LL1_dl_csf_deconfig_cnf_msg;
  lte_LL1_sys_config_ue_cnf_msg_struct               lte_LL1_sys_config_ue_cnf_msg; 

  /* Grant Manager messages */
  lte_ml1_gm_common_cfg_cnf_s                        lte_ml1_gm_common_cfg_cnf;
  lte_ml1_gm_dedicated_cfg_cnf_s                     lte_ml1_gm_dedicated_cfg_cnf;
#ifndef FEATURE_LTE_3GPP_REL8_MAR09
  lte_ml1_gm_ra_rnti_decoded_ind_s                   lte_ml1_gm_ra_rnti_decoded_ind;
#endif
  lte_ml1_mgr_update_pmic_ind_s                      lte_ml1_mgr_update_pmic_ind;

  /* Scheduler messages */
  lte_ml1_mgr_schdlr_deactivate_cnf_s                lte_ml1_mgr_schdlr_deactivate_cnf;
  lte_ml1_schdlr_msg_cswitch_done_ind_s              lte_ml1_schdlr_msg_cswitch_done_ind;

  lte_cphy_test_gm_skip_rach_req_s                   lte_cphy_test_gm_skip_rach_req;
  lte_cphy_test_ml1_skip_rach_req_s                  lte_cphy_test_ml1_skip_rach_req;
  lte_cphy_test_skip_dbch_pre_idle_req_s             lte_cphy_test_skip_dbch_pre_idle_req;

#ifdef FEATURE_LTE_PLT
  /* PLT test messages */
  lte_cphy_test_common_cfg_mbsfn_req_s               lte_cphy_test_common_cfg_mbsfn_req;
  lte_cphy_test_ul_tti_cfg_req_s                     lte_cphy_test_ul_tti_cfg_req;
  lte_cphy_test_cfg_req_s                            lte_cphy_test_cfg_req;
  lte_cphy_test_skip_acq_req_s                       lte_cphy_test_skip_acq_req;
  lte_cphy_test_search_cfg_req_s                     lte_cphy_test_search_cfg_req;
  lte_cphy_test_dl_tti_cfg_req_s                     lte_cphy_test_dl_tti_cfg_req;
  lte_cphy_test_dl_cfg_req_s                         lte_cphy_test_dl_cfg_req;
  lte_cphy_test_skip_ulpc_req_s                      lte_cphy_test_skip_ulpc_req;
  lte_cphy_test_ody_plt_l1m_cfg_req_s                lte_cphy_test_ody_plt_l1m_cfg_req;
#endif

#ifdef FEATURE_LTE_ML1_COEX
  lte_ml1_cxm_state_update_req_s                     lte_ml1_coex_state_update_req;
  lte_ml1_coex_state_update_and_notify_req_s         lte_ml1_coex_state_update_and_notify_req;
  lte_ml1_cxm_activation_req_s                       lte_ml1_coex_activation_req;
  lte_ml1_cxm_deactivation_req_s                     lte_ml1_coex_deactivation_req;
  lte_ml1_cxm_reading_count_req_s                    lte_ml1_coex_reading_count_req;
  lte_ml1_cxm_rc_complete_ind_s                      lte_ml1_coex_reading_count_complete_ind;
  lte_ml1_cxm_phr_less_backoff_req_s                 lte_ml1_coex_phr_less_backoff_req;
  lte_ml1_cxm_phr_backoff_req_s                      lte_ml1_coex_phr_backoff_req;
  lte_ml1_coex_gm_backoff_ind_s                      lte_ml1_coex_phr_backoff_ind;
  lte_ml1_sleepmgr_sleep_start_ind_s                 lte_ml1_sleepmgr_sleep_start_ind;
  lte_ml1_sleepmgr_wakeup_isr_ind_s                  lte_ml1_sleepmgr_wakeup_isr_ind;
  lte_ml1_sleepmgr_stmr_on_ind_s                     lte_ml1_sleepmgr_stmr_on_ind;
  cxm_coex_boot_params_v01_ind_s                     lte_ml1_coex_boot_ind;             
  cxm_coex_active_policy_lte_ind_s                   lte_ml1_coex_active_ind;
  cxm_coex_generic_req_s                             lte_ml1_coex_boot_req;
  cxm_freqid_info_ind_s                              lte_ml1_coex_freqid_list;
  lte_ml1_coex_send_power_ind_s                      lte_ml1_coex_send_power_ind;
#endif

  lte_cphy_cdma_systime_req_s                        lte_cphy_cdma_systime_req;
  lte_cphy_utc_time_update_req_s                     lte_cphy_utc_time_update_req;
  lte_cphy_utc_time_update_cnf_s                     lte_cphy_utc_time_update_cnf;
  //! @todo Add as new external msgs added
} lte_ml1_mgr_ext_msg_u;

/*! @brief Union of all msgs that ML1 MGR can receive
*/
typedef union
{
  lte_ml1_mgr_int_msg_u int_msg;
  lte_ml1_mgr_ext_msg_u ext_msg;
}lte_ml1_mgr_msg_u;

#endif /* LTE_ML1_MGR_MSG_REQ_H */
