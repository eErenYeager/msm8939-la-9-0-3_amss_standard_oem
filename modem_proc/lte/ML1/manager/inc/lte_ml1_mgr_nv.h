/*!
  @file
  lte_ml1_mgr_nv.h

  @brief
  header file for efs client related operations for this module.

  @detail
*/

/*===========================================================================

  Copyright (c) 2009 - 2014 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/ML1/manager/inc/lte_ml1_mgr_nv.h#1 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
24/10/13    ap      Initial version
===========================================================================*/

#ifndef LTE_ML1_MGR_NV_H
#define LTE_ML1_MGR_NV_H

/*===========================================================================
	
							   INCLUDE FILES
	
===========================================================================*/
	
#include <comdef.h>
#include "lte_ml1_comdef.h"	
	
/*===========================================================================
	
					  MODULE DEFINITIONS AND TYPES
	
===========================================================================*/

/* LTE mode info */
#define LTE_ML1_DEFAULT_MODE            0x01
#define LTE_ML1_FTM_CAL_MODE            0x02
#define LTE_ML1_FTM_NS_MODE             0x03
#define LTE_ML1_MVT_MODE                0x04


/*===========================================================================
 
   FUNCTION:  lte_ml1_mgr_nv_sps_tti_cfg_size
 
 ===========================================================================*/
 /*!
	 @brief
        This function returns the size of EFS variable in bytes. 
		This size information is used by EFS framework for memory 
        allocation and storing the EFS value.
        
	 @details
 
	 @return size in bytes
 */
 /*=========================================================================*/

  uint8 lte_ml1_mgr_nv_sps_tti_cfg_size(void);


/*===========================================================================
 
   FUNCTION:  lte_ml1_mgr_nv_sps_tti_cfg_defaultvalue
 
 ===========================================================================*/
 /*!
	 @brief
	 This function provides the default value for the efs variable. 
	 This data will be stored with the EFS framework.
 
	 @details
	 EFS framework sends the pointer to memory(which framework owns), 
	 to fill in the default value.
 
	 @return
 */
 /*=========================================================================*/


  void lte_ml1_mgr_nv_sps_tti_cfg_defaultvalue(void *ptr);

/*===========================================================================
  
	FUNCTION:  lte_ml1_mgr_nv_fw_recovery_size
  
 ===========================================================================*/
/*!
	@brief
	This function returns the size of EFS variable in bytes. 
	This size information is used by EFS framework for memory 
    allocation and storing the EFS value.
		 
	@details
  
	@return size in bytes
*/
/*=========================================================================*/
 
  uint8 lte_ml1_mgr_nv_fw_recovery_size(void);
 
/*===========================================================================
  
	FUNCTION:  lte_ml1_mgr_nv_fw_recovery_defaultvalue
  
===========================================================================*/
/*!
	  @brief
	  This function provides the default value for the efs variable. 
	  This data will be stored with the EFS framework.
  
	  @details
	  EFS framework sends the pointer to memory(which framework owns),
	  to fill in the default value.
  
	  @return
*/
/*=========================================================================*/
 
  void lte_ml1_mgr_nv_fw_recovery_defaultvalue(void *ptr);

/*===========================================================================
  
	FUNCTION:  lte_ml1_mgr_nv_lte_mode_size
  
 ===========================================================================*/
/*!
	@brief
	This function returns the size of EFS variable in bytes. 
	This size information is used by EFS framework for memory 
    allocation and storing the EFS value.
		 
	@details
  
	@return size in bytes
*/
/*=========================================================================*/

  uint8 lte_ml1_mgr_nv_lte_mode_size(void);
 

/*===========================================================================
  
	FUNCTION:  lte_ml1_mgr_nv_lte_mode_defaultvalue
  
===========================================================================*/
/*!
	  @brief
	  This function provides the default value for the efs variable. 
	  This data will be stored with the EFS framework.
  
	  @details
	  EFS framework sends the pointer to memory(which framework owns), 
	  to fill in the default value.
  
	  @return
*/
/*=========================================================================*/

  void lte_ml1_mgr_nv_lte_mode_defaultvalue(void *ptr);
 


#endif

