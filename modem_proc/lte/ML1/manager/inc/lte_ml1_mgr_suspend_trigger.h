/*!
  @file
  lte_ml1_mgr_suspend_trigger.h

  @brief
  Internal suspend trigger header
*/

/*===========================================================================

  Copyright (c) 20012 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/ML1/manager/inc/lte_ml1_mgr_suspend_trigger.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------

===========================================================================*/

#ifndef LTE_ML1_MGR_SUSPEND_TRIGGER_H
#define LTE_ML1_MGR_SUSPEND_TRIGGER_H

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

#include "lte_ml1_comdef.h"
#include "lte_ml1_common_timer.h"
#include "stm2.h"
#include "lte_ml1_mgr_struct.h"

/*===========================================================================

                   EXTERNAL DEFINITIONS AND TYPES

===========================================================================*/
/*! @brief Enum for suspend/resume profiling
*/
//typedef enum 
//{
//  LTE_ML1_SUSPEND_START = 0,
//  LTE_ML1_SUSPEND_END,
//  LTE_ML1_RESUME_START,
//  LTE_ML1_RESUME_END
//} lte_ml1_suspend_resume_profile_e;

/*===========================================================================

                    EXTERNAL FUNCTION PROTOTYPES

===========================================================================*/
/*===========================================================================

  FUNCTION:  lte_ml1_mgr_suspend_resume_profile

===========================================================================*/
/*!
    @brief
    Suspend/resume profiling related slow clock time capture

    @return
    None
*/
/*=========================================================================*/
//void lte_ml1_mgr_suspend_resume_profile( lte_ml1_suspend_resume_profile_e profile_type);

/*===========================================================================

  FUNCTION:  lte_ml1_mgr_suspend_trigger

===========================================================================*/
/*!
    @brief
    Suspend trigger

    @return
    None
*/
/*=========================================================================*/
void lte_ml1_mgr_suspend_trigger
(
  /*! Pointer to right instance */
  lte_ml1_manager_data_s  *mgr_data_inst,
  /*! Previous state */
  stm_state_t             prev_state,
  /*! Current state */
  stm_state_t             curr_state
);

/*===========================================================================

  FUNCTION:  lte_ml1_mgr_suspend_trigger_is_skip

===========================================================================*/
/*!
    @brief
    check if skip mask has been set

    @return
    TRUE is skip mask is set, else FALSE
*/
/*=========================================================================*/
boolean lte_ml1_mgr_suspend_trigger_is_skip
(
  lte_mem_instance_type instance,
  uint16 skip_mask_check
);

/*===========================================================================

  FUNCTION:  lte_ml1_mgr_suspend_trigger_is_qta_supported

===========================================================================*/
/*!
    @brief
    check if QTA is supported

    @return
    TRUE if supported, else FALSE
*/
/*=========================================================================*/
boolean lte_ml1_mgr_suspend_trigger_is_qta_supported
(
  lte_mem_instance_type instance
);

#endif /* LTE_ML1_MGR_SUSPEND_TRIGGER_H */
