/*!
  @file
  lte_ml1_mgr_suspend_timing.h

  @brief
  Suspend resume timing related functions
*/

/*===========================================================================

  Copyright (c) 2013 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/ML1/manager/inc/lte_ml1_mgr_suspend_timing.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/24/13   anm     Initial version

===========================================================================*/

#ifndef LTE_ML1_MGR_SUSPEND_TIMING_H
#define LTE_ML1_MGR_SUSPEND_TIMING_H

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

#include "lte_ml1_comdef.h"
#include "lte_ml1_mgr_struct.h"

/*===========================================================================

                   EXTERNAL DEFINITIONS AND TYPES

===========================================================================*/

/*===========================================================================

                    EXTERNAL FUNCTION PROTOTYPES

===========================================================================*/

/*===========================================================================

  FUNCTION:  lte_ml1_mgr_suspend_resume_event_update

===========================================================================*/
/*!
    @brief
    Suspend/resume event update

    @return
    None
*/
/*=========================================================================*/
void lte_ml1_mgr_suspend_resume_event_update
( 
  /*! Pointer to right instance */
  lte_ml1_manager_data_s                *mgr_data_inst,

   lte_ml1_suspend_resume_event_e       event_type
);

/*===========================================================================

  FUNCTION:  lte_ml1_mgr_suspend_timing_if_skip_init_acq

===========================================================================*/
/*!
    @brief
    Determines if need to skip acq and time slam on resume
    
    @return
    TRUE = Skip init acq/PBCH/time slam
    otherwise FALSE
*/
/*=========================================================================*/
boolean lte_ml1_mgr_suspend_timing_if_skip_init_acq(lte_mem_instance_type instance);

/*===========================================================================

  FUNCTION:  lte_ml1_mgr_get_suspend_duration

===========================================================================*/
/*!
    @brief
    Returns suspend duration in ms.
    
    @details
    Resume trigger time - Suspend trigger time
    
    @return
    last suspend duration in ms
*/
/*=========================================================================*/
uint32 lte_ml1_mgr_get_suspend_duration(lte_mem_instance_type instance);

#endif /* LTE_ML1_MGR_SUSPEND_TIMING_H */
