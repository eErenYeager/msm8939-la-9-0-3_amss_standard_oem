/*=============================================================================

    __lte_ml1_mgr_stm_int.h

Description:
  This file contains the machine generated header file for the state machine
  specified in the file:
  lte_ml1_mgr_stm.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


#ifndef __LTE_ML1_MGR_STM_INT_H
#define __LTE_ML1_MGR_STM_INT_H

#ifdef __cplusplus
/* If compiled into a C++ file, ensure symbols names are not mangled */
extern "C"
{
#endif

/* Include external state machine header */
#include "__lte_ml1_mgr_stm.h"

/* Begin machine generated internal header for state machine array: LTE_ML1_MGR_SM[] */

/* Suppress Lint suggestions to const-ify state machine and payload ptrs */
/*lint -esym(818,sm,payload) */

/* Define a macro for the number of SM instances */
#define LTE_ML1_MGR_SM_NUM_INSTANCES 1

/* Define a macro for the number of SM states */
#define LTE_ML1_MGR_SM_NUM_STATES 11

/* Define a macro for the number of SM inputs */
#define LTE_ML1_MGR_SM_NUM_INPUTS 165

#ifndef STM_DATA_STRUCTURES_ONLY
/* State Machine entry/exit function prototypes */
void lte_ml1_mgr_stm_entry(stm_state_machine_t *sm,void *payload);
void lte_ml1_mgr_stm_exit(stm_state_machine_t *sm,void *payload);


/* State entry/exit function prototypes */
void lte_ml1_mgr_entry_stop_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_mgr_exit_stop_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_mgr_entry_irat_meas_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_mgr_exit_irat_meas_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_mgr_entry_inactive_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_mgr_exit_inactive_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_mgr_entry_aborting_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_mgr_exit_aborting_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_mgr_entry_band_sys_scan_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_mgr_exit_band_sys_scan_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_mgr_entry_acq_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_mgr_exit_acq_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_mgr_entry_dbch_decode_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_mgr_exit_dbch_decode_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_mgr_entry_idle_drx_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_mgr_exit_idle_drx_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_mgr_entry_suspend_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_mgr_exit_suspend_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_mgr_entry_connected_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_mgr_exit_connected_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_mgr_entry_recovery_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_mgr_exit_recovery_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);


/* Transition function prototypes */
stm_state_t lte_ml1_mgr_stm_start_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_rfm_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_rx_cfg_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_irat_meas_init_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_other_state_suspend_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_plmn_stop_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_plmn_stop_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_plmn_suspend_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_plmn_cphy_suspend_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_no_op(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_defer_handler_no_op(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_forward_msg_to_imeas_stm(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_cfg_start_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_defer_handler_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_stop_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_rx_cfg_done_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_init_acq_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_inactive_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_bs_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_ss_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_test_ody_plt_l1m_cfg_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_plmn_start_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_plmn_cell_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_plmn_cell_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_plmn_skip_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_ignore_rach_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_dog_timer_expiry_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_recovery_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_ho_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_inactive_suspend_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_aborting_state_schdlr_suspend_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_sm_abort_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_ulm_abort_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_dlm_abort_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_gm_abort_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_pos_prs_abort_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_aborting_state_schdlr_deactivate_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_aborting_state_suspend_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_aborting_state_rfm_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_acq_phase_implicit_abort(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_acq_phase_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_sm_bs_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_sm_ss_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_suspend_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_resume_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_tao_sched_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_sm_suspend_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_acq_stage1_complete(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_acq_stage2_complete(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_acq_stage1_complete_ody_phase1_plt(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_sib_sched_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_test_skip_dbch_pre_idle_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_implicit_abort_due_to_stop_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_initial_cell_select_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_initial_cell_select_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_tdd_cfg_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_cfg_done_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_test_ml1_skip_rach_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_rach_cfg_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_start_rach_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_ulm_start_rach_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_rar_params_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_mac_contention_result_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_rach_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_ulm_rach_abort_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_cancel_conn_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_comn_cfg_req_in_idle(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_ded_cfg_req_in_idle(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_paging_drx_cfg_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_idle_cell_select_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_idle_cell_select_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_cell_resel_trigger_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_cell_resel_cancel_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_dlm_resel_cancel_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_idle_resel_comp_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_idle_meas_cfg_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_ul_out_of_sync_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_sm_ho_pbch_done_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_dlm_ho_sfn_update_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_gm_sfn_updated_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_plmn_suspend_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_trm_status_update_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_susp_state_susp_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_trm_acq_done_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_ulm_suspend_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_dlm_suspend_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_gm_suspend_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_pos_prs_suspend_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_suspend_state_rfm_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_sm_resume_acq_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_schdlr_resume_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_dlm_sfn_update_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_resume_proc_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_resume_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_gm_resume_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_ttl_setup_done_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_suspend_ded_cfg_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_suspend_pend_mac_reqs(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_scc_act_deact_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_resource_rel_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_resource_acq_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_ca_proc_trigger_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_idle2_conn_trigger_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_sm_idle_exit_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_comn_cfg_req_in_conn(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_ded_cfg_req_in_conn(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_x2l_ho_prep_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_schdlr_ho_suspend_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_dlm_ho_suspend_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_gm_ho_suspend_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_ulm_ho_suspend_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_schdlr_ho_resume_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_sm_ho_acq_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_ho_rx_tune_done_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_ho_tx_tune_done_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_ho_cswitch_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_dlm_ho_cfg_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_ulm_ho_cfg_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_ho_cfg_done_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_pos_prs_ho_suspend_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_gm_ho_resume_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_ho_comn_cfg_done_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_ho_complete_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_conn_meas_cfg_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_conn_release_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_conn_release_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_con_rel_done_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_scc_int_deact_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_vrlf_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_recovery_clean_up_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_dummy_cnf_handler(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_rec_state_tam_resume_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_mgr_stm_ignore(stm_state_machine_t *sm, void *payload);


/* State enumeration */
enum
{
  LTE_ML1_MGR_STOP_STATE,
  LTE_ML1_MGR_IRAT_MEAS_STATE,
  LTE_ML1_MGR_INACTIVE_STATE,
  LTE_ML1_MGR_ABORTING_STATE,
  LTE_ML1_MGR_BAND_SYS_SCAN_STATE,
  LTE_ML1_MGR_ACQ_STATE,
  LTE_ML1_MGR_DBCH_DECODE_STATE,
  LTE_ML1_MGR_IDLE_DRX_STATE,
  LTE_ML1_MGR_SUSPEND_STATE,
  LTE_ML1_MGR_CONNECTED_STATE,
  LTE_ML1_MGR_RECOVERY_STATE,
};

#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated internal header for state machine array: LTE_ML1_MGR_SM[] */


#ifdef __cplusplus
} /* extern "C" {...} */
#endif

#endif /* ! __LTE_ML1_MGR_STM_INT_H */
