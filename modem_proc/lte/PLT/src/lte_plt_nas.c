/*!
  @file
  lte_plt_nas.c

  @brief
  NAS simulator module of the LTE PLT.
  
  @detail
  
*/

/*===========================================================================

  Copyright (c) 2011 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/PLT/src/lte_plt_nas.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/04/14    ml     Initial creation of the file
===========================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/
#include <msgr.h>
#include <dsm_item.h>
#include <fs_lib.h>
#include <lte_cphy_msg.h>
#include <lte_cphy_test_msg.h>
#include <lte_rrc_ext_msg.h>
#include "lte_plt_driver.h"
#include "lte_plt_nas.h"

/*===========================================================================

                   EXTERNAL DEFINITIONS AND TYPES

===========================================================================*/

/*===========================================================================

                    INTERNAL FUNCTION PROTOTYPES

===========================================================================*/

static void lte_plt_nas_send_rrc_system_update_req ( void );
static void lte_plt_nas_send_rrc_service_req ( void );
static void lte_plt_nas_send_rrc_conn_est_req ( void );
static void lte_plt_nas_send_rrc_ul_data_req ( void);

/*===========================================================================

                   INTERNAL DEFINITIONS AND TYPES

===========================================================================*/

/*! ************************************************************************
   Tables
    ************************************************************************
*/
msgr_umid_type  lte_plt_nas_scenario_steps[] =
{
    //LTE_RRC_SYSTEM_UPDATE_REQ,
    LTE_RRC_SERVICE_REQ,
    LTE_RRC_SERVICE_IND,
    LTE_RRC_CONN_EST_REQ, // contains attach request
    LTE_RRC_DL_DATA_IND, // ESM info request
    LTE_RRC_UL_DATA_REQ, // ESM info response
    LTE_RRC_DL_DATA_IND, // attach accept
    LTE_RRC_UL_DATA_REQ, // attach complete
  LTE_RRC_DL_DATA_IND, // activate bearer context
  LTE_RRC_UL_DATA_REQ, // attach complete
    LTE_PLT_NAS_TEST_END // End a scenario this way
};

lte_plt_nas_msg_info_s     lte_plt_nas_msg_handler_table[] =
{
  { LTE_RRC_SYSTEM_UPDATE_REQ, lte_plt_nas_send_rrc_system_update_req },
  { LTE_RRC_SERVICE_REQ, lte_plt_nas_send_rrc_service_req },
  { LTE_RRC_SERVICE_IND, NULL },
  { LTE_RRC_CONN_EST_REQ, lte_plt_nas_send_rrc_conn_est_req },
  { LTE_RRC_DL_DATA_IND, NULL },
  { LTE_RRC_UL_DATA_REQ, lte_plt_nas_send_rrc_ul_data_req },
  /* Should always be last */
  { LTE_PLT_NAS_TEST_END, NULL },
};

lte_plt_nas_ota_info_s     lte_plt_nas_ota_msg_table[] =
{
  { LTE_PLT_NAS_ATTACH_REQUEST_MSG, LTE_ML1_PLT_NAS_ATTACH_REQ_FILE, 70 },
  { LTE_PLT_NAS_ESM_INFORMATION_RESPONSE_MSG1, LTE_ML1_PLT_NAS_ESM_INFORMATION_RESPONSE_MSG_FILE, 17 },
  { LTE_PLT_NAS_ESM_INFORMATION_RESPONSE_MSG2, LTE_ML1_PLT_NAS_ESM_INFORMATION_RESPONSE_MSG_FILE, 17 },
  { LTE_PLT_NAS_ATTACH_COMPLETE_MSG, LTE_ML1_PLT_NAS_ATTACH_COMPLETE_MSG_FILE, 7 },
  /* Should always be last */
  { LTE_PLT_NAS_INVALID_OTA_STATE, "" },
};

/*! ************************************************************************
   Data structures 
    ************************************************************************
*/

/* Current scenario. Initialize to init acq */
//lte_plt_nas_test_scenario_e    lte_plt_nas_test_scenario = LTE_PLT_NAS_TEST;

/* All LTE PLT NAS message data is here */
lte_plt_nas_msg_data_s         lte_plt_nas_msg_data;

/*===========================================================================

                    INTERNAL MESSAGE HANDLING FUNCTIONS

===========================================================================*/
/*===========================================================================

FUNCTION PLT_SEND_RRC_SERVICE_REQ

DESCRIPTION
  This function sends RRC SERVICE REQ to RRC

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
static void lte_plt_nas_init_rrc_service_req
(
		void
)
{
	lte_rrc_service_req_s *rrc_service_req_ptr = &lte_plt_nas_msg_data.rrc_service_req;
	lte_rrc_plmn_s req_plmn = { {0, 0, 1}, 2, {0, 1, 15} };
	sys_rat_pri_list_info_s_type rat_pri_list =
	{ 1, 0, {
			{(sys_band_mask_e_type)0x120030938DF, SYS_SYS_MODE_LTE, 0, 1, (sys_band_mask_e_type)0x120030938DF},
			{0, SYS_SYS_MODE_NO_SRV, 0, 0, 0},
			{0, SYS_SYS_MODE_NO_SRV, 0, 0, 0},
			{0, SYS_SYS_MODE_NO_SRV, 0, 0, 0},
			{0, SYS_SYS_MODE_NO_SRV, 0, 0, 0},
			{0, SYS_SYS_MODE_NO_SRV, 0, 0, 0},
			{0, SYS_SYS_MODE_NO_SRV, 0, 0, 0},
			{0, SYS_SYS_MODE_NO_SRV, 0, 0, 0},
			{0, SYS_SYS_MODE_NO_SRV, 0, 0, 0},
			{0, SYS_SYS_MODE_NO_SRV, 0, 0, 0}
	},
			{0, 0}
	};

	rrc_service_req_ptr->trans_id = 16973825;

	rrc_service_req_ptr->network_select_mode = SYS_NETWORK_SELECTION_MODE_AUTOMATIC;

	rrc_service_req_ptr->req_plmn_info_is_valid = 1 ;

	memcpy(&rrc_service_req_ptr->req_plmn, &req_plmn, sizeof(req_plmn)) ;
	memcpy(&rrc_service_req_ptr->rat_pri_list, &rat_pri_list, sizeof(rat_pri_list));

	/*MNK: Copy the RAT PRI LIST to the MM global variable
    NAS_ENTER_CRIT_SECT(mm_rat_prioritylist_crit_sec);
    mm_rat_pri_list_info = *rat_pri_list;
    NAS_EXIT_CRIT_SECT(mm_rat_prioritylist_crit_sec);*/

	/* RPLMN */
	rrc_service_req_ptr->rplmn_info_is_valid = 0;

	/* Populate EHPLMN list or HPLMN */
	rrc_service_req_ptr->hplmn_info_is_valid = 0;
	rrc_service_req_ptr->ehplmn_list.num_plmns = 0;

	/* Forbidden TAI lists for roaming and manual selection */
	rrc_service_req_ptr->forbidden_ta_list.num_entries = 0;
	rrc_service_req_ptr->forbidden_manual_ta_list.num_entries = 0;

	/* Report eplmn list to RRC */
	rrc_service_req_ptr->eplmn_list.num_plmns = 0;

	rrc_service_req_ptr->scan_is_new = 1 ;

	rrc_service_req_ptr->use_timer = 0 ;
	rrc_service_req_ptr->lte_scan_time = 0 ;
	rrc_service_req_ptr->csg_id = (sys_csg_id_type)4294967295;

	rrc_service_req_ptr->req_type = LTE_RRC_SERVICE_REQ_TYPE_NORMAL;
	rrc_service_req_ptr->scan_scope = SYS_SCAN_SCOPE_FULL_BAND;

	rrc_service_req_ptr->emc_srv_pending = FALSE;

	msgr_init_hdr((msgr_hdr_s *)&lte_plt_nas_msg_data.rrc_service_req, MSGR_LTE_PLT, LTE_RRC_SERVICE_REQ);

} /* end of lte_plt_nas_init_rrc_service_req() */

static void lte_plt_nas_init_rrc_system_update_req
(
	void
)
{
	lte_rrc_system_update_req_s *rrc_system_update_req_ptr = &lte_plt_nas_msg_data.rrc_system_update_req;

//TODO:  fill in the structure based on NAS emm_rrc_if.c

  //uint32 plmn_index = 0;

/* Populate EHPLMN list or HPLMN */
  rrc_system_update_req_ptr->hplmn_info_is_valid = 0;
  rrc_system_update_req_ptr->ehplmn_list.num_plmns = 0;

  /* RPLMN */

  msgr_init_hdr((msgr_hdr_s *)&lte_plt_nas_msg_data.rrc_system_update_req, MSGR_LTE_PLT, LTE_RRC_SYSTEM_UPDATE_REQ);


} /* end of lte_plt_nas_init_rrc_system_update_req() */


static void lte_plt_nas_init_rrc_conn_est_req
(
		void
)
{
	lte_rrc_conn_est_req_s *rrc_conn_est_req_ptr = &lte_plt_nas_msg_data.rrc_conn_est_req;

  lte_plt_nas_msg_data.ota_message_state = LTE_PLT_NAS_ATTACH_REQUEST_MSG;

  //reset the memory
  memset((void *)rrc_conn_est_req_ptr, 0x0, sizeof(lte_rrc_conn_est_req_s));
  memset((void *)lte_plt_nas_msg_data.emm_ota_message,0x0,
                                 sizeof(byte)*EMM_MAX_MESSAGE_LENGTH);

  lte_plt_nas_msg_data.ota_msg_size = efs_get(lte_plt_nas_ota_msg_table[lte_plt_nas_msg_data.ota_message_state].efs_file_name,
                                              lte_plt_nas_msg_data.emm_ota_message, 
                                              lte_plt_nas_ota_msg_table[lte_plt_nas_msg_data.ota_message_state].efs_file_size);

  LTE_PLT_ASSERT(lte_plt_nas_msg_data.ota_msg_size > 0);
  
  msgr_init_hdr_attach((msgr_hdr_s *)&rrc_conn_est_req_ptr->msg_hdr,
                       MSGR_LTE_PLT, LTE_RRC_CONN_EST_REQ, 0, 1) ;
  
  // loop back the service indication
	dsm_item_type                 *dsm_ptr   = NULL;
	msgr_attach_struct_type       *att_ptr;   // Message attachment pointer

	//boolean                               ac_barring_info_valid = FALSE;
	lte_rrc_ac_barring_info_for_special_s ac_barring_info;

	uint8 access_barring_prob[] = {40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55};

	MSG_HIGH("PLT: Establishing RRC connection, set nas_secure_exchange to FALSE",0,0,0);

	// Init structures
	//memset((void *)&ul_nas_security_info, 0x0,
	//       sizeof(lte_nas_ul_security_info_type));
	memset((void *)&ac_barring_info, 0x0, sizeof(lte_rrc_ac_barring_info_for_special_s));

	rrc_conn_est_req_ptr->est_cause = LTE_RRC_EST_CAUSE_MO_SIGNALING ;
	rrc_conn_est_req_ptr->transaction_id = 16973825 ;
	rrc_conn_est_req_ptr->is_high_priority = 0;

	//Initialize the Access Barring Prob list
	memscpy((void*)&ac_barring_info.access_barring_prob[0],
			sizeof(ac_barring_info.access_barring_prob),
			(void*)&access_barring_prob[0],
			sizeof(access_barring_prob));

	ac_barring_info.access_barring_time = 20; // Timer value = 20 msec

	rrc_conn_est_req_ptr->ac_barring_info_for_special_valid = 0;
	rrc_conn_est_req_ptr->ac_barring_info_for_special = ac_barring_info;

	rrc_conn_est_req_ptr->include_s_tmsi = 0;

	rrc_conn_est_req_ptr->registered_mme_is_present = TRUE;

	/* GUMMEI = MCC + MNC + MMEGI + MMEC */
	rrc_conn_est_req_ptr->registered_mme.plmn_identity_is_present = TRUE ;
	rrc_conn_est_req_ptr->registered_mme.plmn_identity = lte_plt_nas_msg_data.rrc_service_ind.camped_svc_info.selected_plmn;

	rrc_conn_est_req_ptr->registered_mme.mmegi = 0;
	rrc_conn_est_req_ptr->registered_mme.mmec = 0;
	rrc_conn_est_req_ptr->registered_mme.gummei_type = LTE_RRC_GUMMEI_TYPE_NATIVE;

	uint16 dsm_packet_length = 0;

	/*if we do not have enough small items use large item pool*/
	LTE_PLT_ASSERT(lte_plt_nas_msg_data.ota_msg_size <= DSM_DS_SMALL_ITEM_SIZ);
	LTE_PLT_ASSERT(lte_plt_nas_msg_data.ota_msg_size <= NAS_REM_SMALL_ITEM_POOL_SIZE);

	/* Pushdown DSM */
	dsm_packet_length = dsm_pushdown(&dsm_ptr,
			lte_plt_nas_msg_data.emm_ota_message,
			lte_plt_nas_msg_data.ota_msg_size,
			DSM_DS_SMALL_ITEM_POOL);

	LTE_PLT_ASSERT(lte_plt_nas_msg_data.ota_msg_size == dsm_packet_length);

	// Set DSM attach
	att_ptr = msgr_get_attach(&rrc_conn_est_req_ptr->msg_hdr, 0);
	ASSERT(att_ptr != NULL);
	msgr_set_dsm_attach(att_ptr, dsm_ptr);


	/* context_ptr = emm_get_context_by_state(emm_ctrl_data_ptr,
	                                               CONTEXT_STATE_CURRENT);

	    if( context_ptr != NULL )
	    {
	      if(emm_message_ptr->emm_outgoing_msg.service_req.security_hdr == SERV_REQ)
	      {
	        MSG_HIGH("EMM: Send SIM_UPDATE_REQ to RRC ",0,0,0);
	        emm_send_sim_update(emm_ctrl_data_ptr);
	      } // SERV_REQ
	      else
	      {
	        switch (emm_message_ptr->nas_hdr.msg_id)
	        {
	          case ATTACH_REQUEST:
	          case EXT_SERVICE_REQUEST:
	            MSG_HIGH("EMM: Send SIM_UPDATE_REQ to RRC ",0,0,0);
	            emm_send_sim_update(emm_ctrl_data_ptr);
	          break;
	          case TRACKING_AREA_UPADTE_REQUEST:
	              MSG_HIGH("EMM: Send SIM_UPDATE_REQ to RRC ",0,0,0);
	              emm_send_sim_update(emm_ctrl_data_ptr);
	          break;
	          default:
	            MSG_MED("EMM: Default, Do not SIM_UPDATE_REQ to RRC for msg_id = %d ",emm_message_ptr->nas_hdr.msg_id,0,0);
	          break;
	        }// end switch
	      } // else
	    }
	    else  // No Current context
	    {
	      MSG_HIGH("EMM: Security not valid, Do NOT Send SIM_UPDATE_REQ to RRC ",0,0,0);
	    }
	  } */
} /* end of lte_plt_nas_init_rrc_conn_est_req() */


static void lte_plt_nas_init_rrc_ul_data_req
(
		void
)
{
  static uint32 trans_id = 1234567;

	lte_rrc_ul_data_req_s *rrc_ul_data_req_ptr = &lte_plt_nas_msg_data.rrc_ul_data_req;

  
  //reset the memory
  memset((void *)rrc_ul_data_req_ptr, 0x0, sizeof(lte_rrc_ul_data_req_s));
  memset((void *)lte_plt_nas_msg_data.emm_ota_message,0x0,
                                 sizeof(byte)*EMM_MAX_MESSAGE_LENGTH);

  lte_plt_nas_msg_data.ota_msg_size = efs_get(lte_plt_nas_ota_msg_table[lte_plt_nas_msg_data.ota_message_state].efs_file_name,
                                              lte_plt_nas_msg_data.emm_ota_message, 
                                              lte_plt_nas_ota_msg_table[lte_plt_nas_msg_data.ota_message_state].efs_file_size);

  LTE_PLT_ASSERT(lte_plt_nas_msg_data.ota_msg_size == 
                 lte_plt_nas_ota_msg_table[lte_plt_nas_msg_data.ota_message_state].efs_file_size);
  
  msgr_init_hdr_attach((msgr_hdr_s *)&rrc_ul_data_req_ptr->msg_hdr,
                       MSGR_LTE_PLT, LTE_RRC_UL_DATA_REQ, 0, 1) ;
  
  rrc_ul_data_req_ptr->trans_id = trans_id++; /*!< Transaction ID */
  rrc_ul_data_req_ptr->priority = LTE_RRC_UL_NAS_MSG_PRIORITY_HIGH; /*!< Priority of the message */
  rrc_ul_data_req_ptr->cnf_is_reqd = FALSE; /*!< Is confirmation required? */
  rrc_ul_data_req_ptr->cnf_is_reqd_during_conn_rel = FALSE; /*!< Is confirmation required 
                                 during RRC connection release */

  dsm_item_type                 *dsm_ptr   = NULL;
	msgr_attach_struct_type       *att_ptr;   // Message attachment pointer
	uint16 dsm_packet_length = 0;

	/*if we do not have enough small items use large item pool*/
	LTE_PLT_ASSERT(lte_plt_nas_msg_data.ota_msg_size <= DSM_DS_SMALL_ITEM_SIZ);
	LTE_PLT_ASSERT(lte_plt_nas_msg_data.ota_msg_size <= NAS_REM_SMALL_ITEM_POOL_SIZE);

	/* Pushdown DSM */
	dsm_packet_length = dsm_pushdown(&dsm_ptr,
			lte_plt_nas_msg_data.emm_ota_message,
			lte_plt_nas_msg_data.ota_msg_size,
			DSM_DS_SMALL_ITEM_POOL);

	LTE_PLT_ASSERT(lte_plt_nas_msg_data.ota_msg_size == dsm_packet_length);

	// Set DSM attach
	att_ptr = msgr_get_attach(&rrc_ul_data_req_ptr->msg_hdr, 0);
	ASSERT(att_ptr != NULL);
	msgr_set_dsm_attach(att_ptr, dsm_ptr);
} /* end of lte_plt_nas_init_rrc_ul_data_req() */


static void lte_plt_nas_send_rrc_service_req
(
  void
)
{
  lte_plt_nas_init_rrc_service_req();

  LTE_PLT_ASSERT ( msgr_send((msgr_hdr_struct_type *)&lte_plt_nas_msg_data.rrc_service_req, sizeof(lte_rrc_service_req_s)) ==
       E_SUCCESS );

  lte_plt_nas_msg_data.wait_umid = LTE_RRC_SERVICE_IND;

}

static void lte_plt_nas_send_rrc_system_update_req
(
  void
)
{
  lte_plt_nas_init_rrc_system_update_req();

  LTE_PLT_ASSERT ( msgr_send((msgr_hdr_struct_type *)&lte_plt_nas_msg_data.rrc_system_update_req, sizeof(lte_rrc_system_update_req_s)) ==
       E_SUCCESS );


}

static void lte_plt_nas_send_rrc_conn_est_req
(
  void
)
{
	lte_plt_nas_init_rrc_conn_est_req();

  LTE_PLT_ASSERT ( msgr_send((msgr_hdr_struct_type *)&lte_plt_nas_msg_data.rrc_conn_est_req, sizeof(lte_rrc_conn_est_req_s)) ==
       E_SUCCESS );

  lte_plt_nas_msg_data.wait_umid = LTE_RRC_DL_DATA_IND;

}

static void lte_plt_nas_send_rrc_ul_data_req
(
  void
)
{
	lte_plt_nas_init_rrc_ul_data_req();

  LTE_PLT_ASSERT ( msgr_send((msgr_hdr_struct_type *)&lte_plt_nas_msg_data.rrc_ul_data_req, sizeof(lte_rrc_ul_data_req_s)) ==
       E_SUCCESS );

  lte_plt_nas_msg_data.wait_umid = LTE_RRC_DL_DATA_IND;

}

/*===========================================================================

   FUNCTION: lte_plt_nas_execute_msg_handler

===========================================================================*/
/*!
   @brief
   Based on UMID, and scenario execute the next action 

   @detail
   
   @return
   
*/
/*=========================================================================*/
static void lte_plt_nas_execute_scenario_step
(
  uint32 index
)
{
  //< Variable declarations go here >
  uint32    action_index = 0;

  /* Find what action it maps to in the msg_handler_table */
  while ( lte_plt_nas_msg_handler_table[action_index].umid != LTE_PLT_NAS_TEST_END )
  {
    if ( lte_plt_nas_msg_handler_table[action_index].umid == lte_plt_nas_scenario_steps[index] )
    {
      // execute the handler if we have defined one
      if (lte_plt_nas_msg_handler_table[action_index].msg_handler != NULL) {
        lte_plt_nas_msg_handler_table[action_index].msg_handler();
      }
      // increment whether or not there is a handler, and proceed to next step
      lte_plt_nas_msg_data.next_step_in_scenario++;
      return;
    }
    else
    {
      action_index++;
    }
  }
  // No matching entry in handler tabler.  This is pathological
  LTE_PLT_ASSERT(0);
}

void lte_plt_nas_update_ota_state
(
   void
)
{
  uint32 dsm_packet_length;
  dsm_item_type           *dsm_ptr = NULL;  /* DSM pointer */
  msgr_attach_struct_type *att_ptr;         /* Message attachment pointer */

  memset(lte_plt_nas_msg_data.incoming_emm_ota_message, 0, EMM_MAX_MESSAGE_LENGTH);
  
  /* See if NAS has sent the DT message as a DSM item by checking for attachments */
  LTE_PLT_ASSERT(msgr_get_num_attach((msgr_hdr_struct_type *)&lte_plt_nas_msg_data.rrc_dl_data_ind)==1);

    att_ptr = msgr_get_attach((msgr_hdr_struct_type *)&lte_plt_nas_msg_data.rrc_dl_data_ind, 0);
    msgr_get_dsm_attach(att_ptr, &dsm_ptr);
    ASSERT(dsm_ptr != NULL);

    dsm_packet_length = dsm_length_packet(dsm_ptr);
    MSG_HIGH("EMM: DSM DL Data found! item len %d", dsm_packet_length,0,0);
    
    LTE_PLT_ASSERT( dsm_packet_length <= EMM_MAX_MESSAGE_LENGTH )
    
      /* store the RRC message payload, for SMC processing */
      lte_plt_nas_msg_data.incoming_ota_msg_size = (word)dsm_packet_length;

      lte_plt_nas_msg_data.incoming_ota_msg_size = dsm_pullup(&dsm_ptr, 
                                                   lte_plt_nas_msg_data.incoming_emm_ota_message, 
                                                   lte_plt_nas_msg_data.incoming_ota_msg_size);
    /* Received dsm should have been freed */
    LTE_PLT_ASSERT(dsm_ptr == NULL);

    /* Now update the state to send the next set of bytes from EFS */
    lte_plt_nas_msg_data.ota_message_state++;

}

/*===========================================================================

   FUNCTION: lte_plt_nas_handle_message

===========================================================================*/
/*!
   @brief
   Handles messages meant for NAS.

   @detail
   
   @return
   
*/
/*=========================================================================*/
void lte_plt_nas_handle_message
(
   msgr_hdr_struct_type *msg_hdr_ptr, 
   uint32 mail_msg_len 
)
{
  // Handle incoming messages in this function
  // Return if this isn't the one we are waiting for
  if (msg_hdr_ptr->id != lte_plt_nas_msg_data.wait_umid) {
    return;
  }

  // clear the flag to break out of the wait loop
  lte_plt_nas_msg_data.wait_umid = 0;

  switch ( msg_hdr_ptr->id )
  {
  case 	LTE_RRC_SERVICE_IND:
    // store the received indication for debug
    memcpy ( &lte_plt_nas_msg_data.rrc_service_ind, msg_hdr_ptr,
                     sizeof ( lte_rrc_service_ind_s ) );
    // something is wrong unless we can make calls
    LTE_PLT_ASSERT(lte_plt_nas_msg_data.rrc_service_ind.camped_svc_info.cell_access_status == LTE_RRC_CELL_ACCESS_ALL_CALLS);
    break;
  case LTE_RRC_DL_DATA_IND:
    // store the received indication for debug
    memcpy ( &lte_plt_nas_msg_data.rrc_dl_data_ind, msg_hdr_ptr,
                     sizeof ( lte_rrc_dl_data_ind_s ) );
    // set up the next OTA message
    lte_plt_nas_update_ota_state();
    break;

  default:
    /* Do nothing, ignore unhandled messages */
    break;
  }

} /* lte_plt_nas_handle_message */


/*===========================================================================

                                FUNCTIONS

===========================================================================*/

/*===========================================================================

   FUNCTION: lte_plt_nas_run

===========================================================================*/
/*!
   @brief
   Runs the NAS emulation state machine.

   @detail

   @return

*/
/*=========================================================================*/
void lte_plt_nas_run
(
   msgr_client_t *plt_msgr_client
)
{
  /*-----------------------------------------------------------------------*/

  uint32 mail_msg_len;
  msgr_hdr_struct_type *msg_hdr_ptr;
  errno_enum_type  eresult = E_SUCCESS;
  static uint64 plt_msgr_buf[1024];

  /* start by executing the first handler -- assume this will kick off the process */
  lte_plt_nas_msg_data.next_step_in_scenario = 0;
  lte_plt_nas_msg_data.wait_umid = 0;
  lte_plt_nas_msg_data.ota_message_state = LTE_PLT_NAS_INVALID_OTA_STATE;

  while (lte_plt_nas_scenario_steps[lte_plt_nas_msg_data.next_step_in_scenario] != LTE_PLT_NAS_TEST_END)
  {
    while (lte_plt_nas_msg_data.wait_umid != 0)
    {
      /* Blocking call which receives a message */
      eresult = msgr_receive(plt_msgr_client, (void*)&plt_msgr_buf[0],
                                               sizeof(plt_msgr_buf), &mail_msg_len);
      LTE_PLT_ASSERT(eresult == E_SUCCESS);

      msg_hdr_ptr = (msgr_hdr_struct_type *)&plt_msgr_buf[0];

      MSG_2(MSG_SSID_DIAG, MSG_LEGACY_HIGH,
            "LTE PLT-NAS: Received 0x%x [state %d]",msg_hdr_ptr->id, lte_plt_nas_msg_data.next_step_in_scenario );

      // message handler does whatever preliminary processing is required based on received message UMID
      // any new message to be sent should be done through execute_scenario
      lte_plt_nas_handle_message ( ( void * ) &plt_msgr_buf[0], mail_msg_len );

      // clear wait flag inside the handler, as we may have received an unexpected message
    }

    lte_plt_nas_execute_scenario_step(lte_plt_nas_msg_data.next_step_in_scenario);
  }
}


