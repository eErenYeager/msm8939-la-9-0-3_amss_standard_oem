/*!
   @file
   lte_plt_nas.h

   @brief
   Interface to the LTE PLT RRC sim module.

   @detail
   None
*/

/*===========================================================================

  Copyright (c) 2011 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/PLT/src/lte_plt_nas.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
02/25/11    vp     File creation

===========================================================================*/

#ifndef LTE_PLT_NAS_H
#define LTE_PLT_NAS_H

#include <com_dtypes.h>
#include <dsm_init.h>
#include <fs_sys_types.h>
#include <msgr_types.h>

/*===========================================================================

                    Types

===========================================================================*/
/*! ************************************************************************
   Defines
    ************************************************************************
*/
#define  LTE_PLT_NAS_MAX_MSGS_IN_SCENARIO   25
#define  LTE_PLT_NAS_INVALID_ACTION_INDEX   0xFFFFFFFF
#define  LTE_PLT_NAS_TEST_END               0xFFFFFFFF

/*! ************************************************************************
   Types
    ************************************************************************
*/

/* 
   Message handler type 
*/
typedef void ( *lte_plt_nas_msg_handler_t ) ( void );


/*!
   Type for message handling 
*/
typedef struct
{
  /*! UMID to handle */
  msgr_umid_type               umid;
  /*! The message handling function */
  lte_plt_nas_msg_handler_t    msg_handler;

} lte_plt_nas_msg_info_s;

#define MAX_EFS_FILE_NAME_LENTH 60
#define LTE_ML1_PLT_NAS_ATTACH_REQ_FILE "/nv/item_files/modem/lte/ML1/plt_nas_attach_req"
#define LTE_ML1_PLT_NAS_IDENTITY_RESPONSE_MSG_FILE "/nv/item_files/modem/lte/ML1/plt_nas_identity_rsp_msg"
#define LTE_ML1_PLT_NAS_AUTHENICATION_RESPONSE_MSG_FILE "/nv/item_files/modem/lte/ML1/plt_nas_auth_rsp_msg"
#define LTE_ML1_PLT_NAS_SECURITY_MODE_COMPLETE_MSG_FILE "/nv/item_files/modem/lte/ML1/plt_nas_sec_mode_cmplt_msg"
#define LTE_ML1_PLT_NAS_ATTACH_COMPLETE_MSG_FILE "/nv/item_files/modem/lte/ML1/plt_nas_attach_cmplt_msg"

#define LTE_ML1_PLT_NAS_ESM_INFORMATION_RESPONSE_MSG_FILE "/nv/item_files/modem/lte/ML1/plt_nas_esm_info_rsp_msg"

typedef enum
{
  LTE_PLT_NAS_ATTACH_REQUEST_MSG,
  LTE_PLT_NAS_ESM_INFORMATION_RESPONSE_MSG1,
  LTE_PLT_NAS_ESM_INFORMATION_RESPONSE_MSG2,
  LTE_PLT_NAS_ATTACH_COMPLETE_MSG,
  LTE_PLT_NAS_INVALID_OTA_STATE
} lte_plt_nas_ota_message_state_type;


typedef struct
{
  /*! state to handle */
  lte_plt_nas_ota_message_state_type state;
  /*! The name of the EFS file containing the message bytes */
  char  efs_file_name[MAX_EFS_FILE_NAME_LENTH];
  fs_size_t efs_file_size;
} lte_plt_nas_ota_info_s;

/*!
   Structure which contains content of all messages
*/
#define EMM_MAX_MESSAGE_LENGTH         2048
/* gets the free bytes available in small item pool */
#define NAS_REM_SMALL_ITEM_POOL_SIZE (DSM_DS_SMALL_ITEM_SIZ * DSM_POOL_FREE_CNT(DSM_DS_SMALL_ITEM_POOL))

typedef struct 
{
	lte_rrc_system_update_req_s rrc_system_update_req;
	lte_rrc_service_req_s       rrc_service_req;
	lte_rrc_service_ind_s       rrc_service_ind;
    lte_rrc_conn_est_req_s      rrc_conn_est_req;
    lte_rrc_dl_data_ind_s       rrc_dl_data_ind;
    lte_rrc_ul_data_req_s       rrc_ul_data_req;

    msgr_umid_type                      wait_umid;
    uint32                              next_step_in_scenario;
    lte_plt_nas_ota_message_state_type  ota_message_state;

	/* lte nas ota message structures */
    byte                      emm_ota_message[EMM_MAX_MESSAGE_LENGTH];
	word                      ota_msg_size;

    byte                      incoming_emm_ota_message[EMM_MAX_MESSAGE_LENGTH];
    word                      incoming_ota_msg_size;
}lte_plt_nas_msg_data_s;


/*===========================================================================

                    Function Prototypes

===========================================================================*/


/*===========================================================================

   FUNCTION: lte_plt_nas_run

===========================================================================*/
/*!
   @brief
   Runs the NAS emulation state machine

   @detail
   
   @return
   
*/
/*=========================================================================*/
void lte_plt_nas_run(
   msgr_client_t *plt_msgr_client
);
#endif /* LTE_PLT_NAS_H */
