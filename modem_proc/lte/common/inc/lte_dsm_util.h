#ifndef LTE_DSM_UTIL_H
#define LTE_DSM_UTIL_H

/*!
  @file
  lte_dsm_util.h

  @brief
  This file includes DSM utility functions/apis tailored for LTE usage.
  These functions are expected to perform better than legacy DSM apis.
*/

/*==============================================================================

  Copyright (c) 2011 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/common/inc/lte_dsm_util.h#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------------
01/04/11   ax      Separate out what is needed by FTM to API dir
02/16/10   ar      included dsm_lock.h as dsm_queue.h no longer exports 
                   dsm_lock() api
02/15/09   bqiu    add lte_dsm_queue_init
01/29/09   bqiu    add lte_dsm_check, lte_dsm_next
11/18/08   ar      Added new DSM API lte_dsm_get_wm_size() to get the wm size
11/05/08   sm      initial version
==============================================================================*/

/*==============================================================================

                           INCLUDE FILES

==============================================================================*/

/* Target-independent Include files */
#include <comdef.h>
#include <customer.h>
#include <lte_assert.h>
#include <lte_lockless_q.h>
#include <dsm_item.h>
#include <dsm_lock.h>
#include <dsm_queue.h>
#include <msg.h>
#include "lte_dsm_util_ext_api.h"


/*==============================================================================

                   MACRO DEFINITIONS

==============================================================================*/

/*!
    @brief
    Internal MACRO for calling passed call back function pointer(if configured)
*/
#define LTE_DSMI_WM_CB(CB_FPTR,WM_PTR,CB_DATA) do {\
  if(CB_FPTR != NULL) (CB_FPTR)(WM_PTR,CB_DATA);}while(0)

/*!
    @brief
    Locks the given watermark.
*/
#define LTE_DSM_LOCK_WM(wm_ptr) DSM_LOCK(&((wm_ptr)->lock))

/*!
    @brief
    Unlocks the given watermark.
*/
#define LTE_DSM_UNLOCK_WM(wm_ptr) DSM_UNLOCK(&((wm_ptr)->lock))

/*!
    @brief
    Create lock for the given watermark.
*/
#define LTE_DSM_LOCK_WM_CREATE(wm_ptr) \
  DSM_LOCK_CREATE( &((wm_ptr)->lock) )

/*!
    @brief
    destroy lock for the given watermark.
*/
#define LTE_DSM_LOCK_WM_DESTROY(wm_ptr) \
  DSM_LOCK_DESTROY( &((wm_ptr)->lock) )

/*==============================================================================

                   static inline FUNCTIONS

==============================================================================*/

/*==============================================================================

  FUNCTION:  lte_dsm_get_wm_item_count()

==============================================================================*/
/*!
    @brief
    This function returns the number of dsm packet chains enqueued to a
    given watermark

    @note
    parameter 'wm_ptr' must NOT be NULL.

    @return
    Number of packets enqueued to the dsm watermark.

*/
/*============================================================================*/
static inline uint16 lte_dsm_get_wm_item_count
(
  /*! Pointer to watermark item to get item from */
  dsm_watermark_type *wm_ptr
)
{
  return(lte_lockless_q_cnt(wm_ptr->q_ptr));
}

/*==============================================================================

  FUNCTION:  lte_dsm_check()

==============================================================================*/
/*!
    @brief
    This function return the first dsm item in the water mark
    @note

    @return
    dsm pointer to the first item in the water mark
*/
/*============================================================================*/
static inline dsm_item_type * lte_dsm_check
(
  /*! Pointer to Watermark item to check */
  dsm_watermark_type *wm_ptr
)
{
  return ((dsm_item_type *) lte_lockless_q_check(wm_ptr->q_ptr));
} /* lte_dsm_check */

/*==============================================================================

  FUNCTION:  lte_dsm_next()

==============================================================================*/
/*!
    @brief
    This function return the next dsm item in the water mark
    @note

    @return
    dsm pointer next to the cur_dsm_ptr
*/
/*============================================================================*/
static inline dsm_item_type * lte_dsm_next
(
  /*! Pointer to Watermark item to check */
  dsm_watermark_type *wm_ptr,
  /*! current dsm item */
  dsm_item_type      *cur_dsm_ptr
)
{
  dsm_item_type *temp_ptr = NULL;

  LTE_DSM_LOCK_WM(wm_ptr);

  temp_ptr = (dsm_item_type *) lte_lockless_q_next(wm_ptr->q_ptr, 
                           cur_dsm_ptr? &(cur_dsm_ptr->link): NULL);

  LTE_DSM_UNLOCK_WM(wm_ptr);
  
  return(temp_ptr);
} /* lte_dsm_next */


/*==============================================================================

  FUNCTION:  lte_dsm_get_wm_size()

==============================================================================*/
/*!
    @brief
    This function returns the total size (in bytes) of the dsm items enqueued to
    the given watermark

    @note
    parameter 'wm_ptr' must NOT be NULL.

    @return
    Number of bytes enqueued to the given watermark.

*/
/*============================================================================*/
static inline uint32 lte_dsm_get_wm_size
(
  /*! Pointer to watermark item */
  dsm_watermark_type *wm_ptr
)
{
  return(wm_ptr->current_cnt);
}


#if DSM_TRACE_LEN > 0

/*==============================================================================

  FUNCTION:  lte_dsmi_touch_item()

==============================================================================*/
/*!
    @brief
    This function sets the file and line fields of the header.

    @return
    void

    @retval value
    void
*/
/*============================================================================*/
static inline void lte_dsmi_touch_item
(
  /*! A pointer to an item */
  dsm_item_type * item_ptr,
  /*! the file name to use */
  const char * file,
  /*! the line number to use */
  int line
)
{

#if (DSM_TRACE_LEN > 1)
  uint16 i;
  for(i=DSM_TRACE_LEN-1;i>0;i--)
  {
    item_ptr->trace[i] = item_ptr->trace[i-1];
  }
#endif

  item_ptr->trace[0].file = file;
  item_ptr->trace[0].line = line;

} /* lte_dsmi_touch_item() */

#endif /*  DSM_TRACE_LEN > 0 */


/*==============================================================================

  FUNCTION:  lte_dsm_touch_item()

==============================================================================*/
/*!
    @brief
    This function sets the file and line fields in dsm header for tracing
    purpose.

    @return
    void

    @retval value
    void
*/
/*============================================================================*/
#if DSM_TRACE_LEN > 0
#define lte_dsm_touch_item(item_ptr) \
  lte_dsmi_touch_item((item_ptr), __FILE__, __LINE__)
#else
#define lte_dsm_touch_item(item_ptr) \
  (void)item_ptr
#endif

/*==============================================================================

  FUNCTION:  lte_dsm_enqueue()

==============================================================================*/
/*!
    @brief
    This function enqueues the DSM item to the passed watermark queue, then
    check for and trigger relevant 'put' events.

    This function doesnt check for the priority and will NOT enqueue an item
    to the front of the queue.

    This function updates the watermark count with the "item_length" indicated
    as part of the argument.

    Argument "item_length" will be used to update the watermark count.

    @note
    If enqueue fails then this API will release the passed DSM item and set the
    passed *pkt_ptr to NULL.
    Receiver entity shall make use of "lte_dsmi_dequeue()" API to dequeue an
    item from the watermark.

    If enqueue fails then this API will release the passed DSM item.

    @return
    TRUE if enqueue is succeeds; FALSE if enqueue fails.
    This function will set the passed *pkt_ptr to NULL.
*/
/*============================================================================*/
#define lte_dsm_enqueue(wm_ptr, pkt_head_ptr, item_length) \
  lte_dsmi_enqueue((wm_ptr), (pkt_head_ptr), (item_length), __FILE__, __LINE__)

boolean lte_dsmi_enqueue
(
  /*! Pointer to Watermark item to put to */
  dsm_watermark_type *wm_ptr,
  /*! Pointer to pointer to item to add to queue */
  dsm_item_type      **pkt_head_ptr,
  /*! length of the DSM item chain (may include length which may be part of
      the meta info associated with the dsm ptr to be enqueued). */
  uint32             item_length,
  /*! file pointer from where this function is called */
  const char         *file,
  /*! line number from where this function is called */
  uint32             line
);


/*==============================================================================

  FUNCTION:  lte_dsm_dequeue()

==============================================================================*/
/*!
    @brief
    This function dequeues and return a pointer to the next item on the
    watermark queue associated with the passed watermark item.

    This function updates the 'current_cnt' of the watermark using
    dsm_packet_length() on the dequeued dsm packet.

    It also checks for and perform any relevent 'get' events.

    @note
    The parameter must NOT be NULL.

    @return
    A pointer to a 'dsm_item_type' or NULL if no item_array available.
*/
/*============================================================================*/
#define lte_dsm_dequeue(wm_ptr) \
  lte_dsmi_dequeue((wm_ptr), __FILE__, __LINE__)

dsm_item_type *lte_dsmi_dequeue
(
  /*! Pointer to watermark item to get item from */
  dsm_watermark_type *wm_ptr,
  /*! file pointer from where this function is called */
  const char        *file,
  /*! line number from where this function is called */
  uint32            line
);


/*==============================================================================

  FUNCTION:  lte_dsmi_delete_next()

==============================================================================*/
/*!
    @brief
    This function delete the next dsm item in the water mark
    @note

    @return
    deleted dsm pointer
*/
/*============================================================================*/
#define lte_dsm_delete_next(wm_ptr, cur_dsm_ptr) \
  lte_dsmi_delete_next((wm_ptr), (cur_dsm_ptr), __FILE__, __LINE__)

dsm_item_type * lte_dsmi_delete_next
(
  /*! Pointer to Watermark item to check */
  dsm_watermark_type *wm_ptr,
  /*! current dsm item */
  dsm_item_type      *cur_dsm_ptr,
  /*! file pointer from where this function is called */
  const char         *file,
  /*! line number from where this function is called */
  uint32             line
);

/*==============================================================================

  FUNCTION:  lte_dsmi_delete_next_with_length_in_app_field()

==============================================================================*/
/*!
    @brief
    This function delete the next dsm item in the water mark, this water mark 
    dsm app field has the pkt length

    @note

    @return
    deleted dsm pointer
*/
/*============================================================================*/
#define lte_dsm_delete_next_with_length_in_app_field(wm_ptr, cur_dsm_ptr) \
  lte_dsmi_delete_next_with_length_in_app_field((wm_ptr), (cur_dsm_ptr), __FILE__, __LINE__)

dsm_item_type * lte_dsmi_delete_next_with_length_in_app_field
(
  /*! Pointer to Watermark item to check */
  dsm_watermark_type *wm_ptr,
  /*! current dsm item */
  dsm_item_type      *cur_dsm_ptr,
  /*! file pointer from where this function is called */
  const char         *file,
  /*! line number from where this function is called */
  uint32             line
);

/*==============================================================================

  FUNCTION:  lte_dsmi_extract_1byte()

==============================================================================*/
/*!
    @brief
    This function extracts/copies one byte from the 'offset' inside the DSM
    packet pointer and stores the same to the address 'buf' passed to this
    function.

    @return
    number of bytes extracted
*/
/*============================================================================*/
#define lte_dsm_extract_1byte(packet_ptr, offset, buf) \
  lte_dsmi_extract_1byte((packet_ptr), (offset), (buf))

uint16 lte_dsmi_extract_1byte
(
  /*! dsm pointer of the packet from which a byte needs to be extracted */
  dsm_item_type *packet_ptr,
  /*! offset inside the dsm packet from which a byte needs to be extracted */
  uint16 offset,
  /*! address where extracted byte needs to be stored */
  void *buf
);

/*==============================================================================

  FUNCTION:  lte_dsmi_extract_2byte()

==============================================================================*/
/*!
    @brief
    This function extracts/copies two bytes from the 'offset' inside the DSM
    packet pointer and stores the same to the address 'buf' passed to this
    function.

    @return
    number of bytes extracted
*/
/*============================================================================*/
#define lte_dsm_extract_2byte(packet_ptr, offset, buf) \
  lte_dsmi_extract_2byte((packet_ptr), (offset), (buf))

uint16 lte_dsmi_extract_2byte
(
  /*! dsm pointer of the packet from which 2 bytes need to be extracted */
  dsm_item_type *packet_ptr,
  /*! offset inside the dsm packet from which 2 bytes need to be extracted */
  uint16 offset,
  /*! address where extracted bytes needs to be stored */
  void *buf
);

/*==============================================================================

  FUNCTION:  lte_dsmi_extract_3byte()

==============================================================================*/
/*!
    @brief
    This function extracts/copies three bytes from the 'offset' inside the DSM
    packet pointer and stores the same to the address 'buf' passed to this
    function.

    @return
    number of bytes extracted
*/
/*============================================================================*/
#define lte_dsm_extract_3byte(dsm_packet_ptr, offset, buf) \
  lte_dsmi_extract_3byte((dsm_packet_ptr), (offset), (buf))


uint16 lte_dsmi_extract_3byte
(
  /*! dsm pointer of the packet from which 3 bytes need to be extracted */
  dsm_item_type *packet_ptr,
  /*! offset inside the dsm packet from which 3 bytes need to be extracted */
  uint16 offset,
  /*! address where extracted bytes needs to be stored */
  void *buf
);


/*==============================================================================

  FUNCTION:  lte_dsm_extract_4byte()

==============================================================================*/
/*!
    @brief
    This function extracts/copies four bytes from the 'offset' inside the DSM
    packet pointer and stores the same to the address 'buf' passed to this
    function.

    @return
    number of bytes extracted
*/
/*============================================================================*/
#define lte_dsm_extract_4byte(dsm_packet_ptr, offset, buf) \
  lte_dsmi_extract_4byte((dsm_packet_ptr), (offset), (buf))

uint16 lte_dsmi_extract_4byte
(
  /*! dsm pointer of the packet from which four bytes need to be extracted */
  dsm_item_type *packet_ptr,
  /*! offset inside the dsm packet from which four bytes need to be extracted */
  uint16 offset,
  /*! address where extracted bytes needs to be stored */
  void *buf
);

/*==============================================================================

  FUNCTION:  lte_dsm_pullup_1byte()

==============================================================================*/
/*!
    @brief
    Pull single character from dsm_item_type

    @note
    When the last byte is pulled from a buffer item the item is returned to
    its free queue.

    @return
    returns -1 if nothing can be pulled from passed data item else returns the
    byte pulled.
*/
/*============================================================================*/
#define lte_dsm_pullup_1byte(dsm_packet_ptr) \
  dsmi_pull8((dsm_packet_ptr), __FILE__, __LINE__)


/*==============================================================================

  FUNCTION:  lte_dsm_pullup_2byte()

==============================================================================*/
/*!
    @brief
    Pull a 16-bit integer in host order from buffer in network byte order.

    @note
    When the last byte is pulled from a buffer item the item is returned to
    its free queue.

    @return
    returns -1 if nothing.  Otherwise the a uint16 that is in HOST byte order.
*/
/*============================================================================*/
#define lte_dsm_pullup_2byte(dsm_packet_ptr) \
  lte_dsmi_pullup_2byte((dsm_packet_ptr), __FILE__, __LINE__)

int32 lte_dsmi_pullup_2byte
(
  /*! Pointer to item pull from */
  dsm_item_type **pkt_head_ptr,
  const char * file,
  uint32 line
);

/*===========================================================================
FUNCTION LTE_DSM_QUEUE_INIT()

DESCRIPTION
   This function initializes a watermark queue.  Setting all the callbacks and 
   callback data to NULL, watermark levels to 0, and initializing the queue 
   that this will use.  

DEPENDENCIES
   None

PARAMETERS
   wm_ptr - Pointer to the watermark to initialize
   dne - Do not exceed level for this watermark
   queue - Pointer to the queue header that this water mark should use

RETURN VALUE
   None

SIDE EFFECTS
   Queue is initialized
===========================================================================*/
void lte_dsm_queue_init
(
  dsm_watermark_type *wm_ptr,
  int dne,
  q_type * queue
);

/*===========================================================================
FUNCTION lte_dsm_queue_destroy()

DESCRIPTION
   This function tears down a watermark queue.
DEPENDENCIES
   None

PARAMETERS
   wm_ptr - Pointer to the watermark to initialize

RETURN VALUE
   None

SIDE EFFECTS
   Locks might be destroyed.
   Packets might be freed.
   WM pointer will be non-initialized.
===========================================================================*/
void lte_dsm_queue_destroy
(
  dsm_watermark_type *wm_ptr
);

#endif /* LTE_DSM_UTIL_H */

