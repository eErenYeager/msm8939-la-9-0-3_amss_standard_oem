/*!
  @file
  lte_startup.h

  @brief
  This file contains definitions for the features that LTE Access Stratum(AS)
  may need.

*/

/*=============================================================================

  Copyright (c) 2011 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/common/inc/lte_startup.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
8/16/12   ypathak  initial version
=============================================================================*/

#ifndef LTE_STARTUP_H
#define LTE_STARTUP_H

/*=============================================================================

                   INCLUDE FILES

=============================================================================*/

#include "lte_variation.h"

/*=============================================================================

                   EXTERNAL CONSTANT/MACRO DEFINITIONS

=============================================================================*/

/*! Checking if PLT mode is enabled */
boolean lte_get_plt_mode_enabled (void);

#endif /* LTE_STARTUP_H */
