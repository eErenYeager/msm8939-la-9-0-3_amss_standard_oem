#===============================================================================
#
# LTE RRC Scons
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2010 by Qualcomm Technologies, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: $
#
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 11/21/10    ae     Initial version.
#===============================================================================
Import('env')

# Enable warnings -> errors for all, except LLVM toolchain (6.x.x) during migration
import os
if not os.environ.get('HEXAGON_RTOS_RELEASE').startswith('6'):
	env = env.Clone(HEXAGONCC_WARN = "${HEXAGONCC_WARN} -Werror")
	env = env.Clone(HEXAGONCXX_WARN = "${HEXAGONCCXX_WARN} -Werror")

from glob import glob
from os.path import join, basename


env.LoadSoftwareUnits()
#-------------------------------------------------------------------------------
# Setup source PATH
#-------------------------------------------------------------------------------
SRCPATH = "../src"

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

#-------------------------------------------------------------------------------
# Set MSG_BT_SSID_DFLT for legacy MSG macros
#-------------------------------------------------------------------------------
env.Append(CPPDEFINES = [
   "MSG_BT_SSID_DFLT=MSG_SSID_LTE_RRC",
])

#-------------------------------------------------------------------------------
# Necessary Public and Restricted API's
#-------------------------------------------------------------------------------
env.RequirePublicApi([
    'HWENGINES',
    'DEBUGTOOLS',
    'SERVICES',
    'SYSTEMDRIVERS',
    'DAL',
    'POWER',
    'BUSES',
    'MPROC',
    'KERNEL',                             # needs to be last 
    ], area='core')

env.RequireExternalApi(['BREW'])

# Need to get access to Modem Public headers
env.RequirePublicApi([
    'ONEX',
    'GPS',      
    'HDR',      
    'MCS',
    'MMODE',    
    'RFA',      
    'UIM',      
    'UTILS',
	'WCDMA',
    ])

# Need get access to Modem Restricted headers
env.RequireRestrictedApi([
    'MMODE',    
    'NAS',      
    'RFA',      
    'MDSP',     
    'MDSPSVC',  
    'GERAN',    
    'GPS',      
    'WCDMA',
    'TDSCDMA',
    'ONEX',     
    'HDR',      
    'MCS',
    'UTILS',
    'LTE',
    'QTF',
    ])

# Need to get access to LTE protected headers
env.RequireProtectedApi([
    'LTE',
    ])

# Allow us to see headers in src dir.
env.PublishPrivateApi('RRC', '${INC_ROOT}/modem/lte/RRC/src')
if 'USES_MOB' in env:
    env.PublishPrivateApi('RRC', '${INC_ROOT}/lte/RRC/ASN1/off_target')
else:
    env.PublishPrivateApi('RRC', '${INC_ROOT}/lte/RRC/ASN1/on_target')

env.RequirePublicApi('QTF', area='mob') 
#-------------------------------------------------------------------------------
# Generate the library and add to an image
#-------------------------------------------------------------------------------

# Construct the list of source files by looking for *.c
LTE_RRC_SOURCES = ['${BUILDPATH}/' + basename(fname)
                   for fname in glob(join(env.subst(SRCPATH), '*.c'))]

import re

# STM Files
for stmname in glob(join(env.subst(SRCPATH), '*.stm')):
    match = re.search('([^\\\/]*)\.stm', stmname)
    match.group(1)
    env.STM2('${BUILDPATH}/__' + match.group(1) + '_int.c',
             '${BUILDPATH}/' + match.group(1) + '.stm')
    LTE_RRC_SOURCES.append('${BUILDPATH}/__' + match.group(1) + '_int.c')

env.Append(CPPPATH = '${BUILDPATH}')
env.Append(CPPPATH = '${LTE_ROOT}/RRC/build/${BUILDPATH}')

# Compile the LTE RRC source and convert to a binary library
env.AddBinaryLibrary(['MODEM_MODEM', 'MOB_LTE'], '${BUILDPATH}/lte_rrc', LTE_RRC_SOURCES, pack_exception=['USES_COMPILE_LTE_L3_L2_PROTECTED_LIBS', 'USES_CUSTOMER_GENERATE_LIBS', 'USES_FEATURE_LTE_PACKBUILD_GENERATE_PROTECTED_LIBS'])


#-------------------------------------------------------------------------------
# avoid run-time errors when trying to send MSGR messages 
#-------------------------------------------------------------------------------

if 'USES_MSGR' in env:
	env.AddUMID('${BUILDPATH}/lte_rrc.umid', ['../src/lte_rrc_int_msg.h'])



