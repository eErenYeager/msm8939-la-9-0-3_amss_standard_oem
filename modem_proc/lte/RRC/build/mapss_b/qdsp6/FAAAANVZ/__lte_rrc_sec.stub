/*!
  @file
  __lte_rrc_sec.stub

  @brief
  This module contains the entry, exit, and transition functions
  necessary to implement the following state machines:

  @detail
  LTE_RRC_SEC_SM ( 1 instance/s )


  OPTIONAL further detailed description of state machines
  - DELETE this section if unused.

*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
===========================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

/* Include STM external API */
#include <stm2.h>

//! @todo Include necessary files here


/*===========================================================================

         STM COMPILER GENERATED PROTOTYPES AND DATA STRUCTURES

===========================================================================*/

/* Include STM compiler generated internal data structure file */
#include "__lte_rrc_sec_int.h"

/*===========================================================================

                         LOCAL VARIABLES

===========================================================================*/


/*! @brief Structure for state-machine per-instance local variables
*/
typedef struct
{
  int   internal_var;  /*!< My internal variable */
  void *internal_ptr;  /*!< My internal pointer */
  //! @todo SM per-instance variables go here
} __lte_rrc_sec_type;


/*! @brief Variables internal to module __lte_rrc_sec.stub
*/
STATIC __lte_rrc_sec_type __lte_rrc_sec;



/*===========================================================================

                 STATE MACHINE: LTE_RRC_SEC_SM

===========================================================================*/

/*===========================================================================

  STATE MACHINE ENTRY FUNCTION:  lte_rrc_sec_sm_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_RRC_SEC_SM

    @detail
    Called upon activation of this state machine, with optional
    user-passed payload pointer parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_rrc_sec_sm_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_rrc_sec_sm_entry() */


/*===========================================================================

     (State Machine: LTE_RRC_SEC_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: INITIAL

===========================================================================*/

/*===========================================================================

  STATE ENTRY FUNCTION:  lte_rrc_sec_sm_initial_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_RRC_SEC_SM,
    state INITIAL

    @detail
    Called upon entry into this state of the state machine, with optional
    user-passed payload pointer parameter.  The prior state of the state
    machine is also passed as the prev_state parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_rrc_sec_sm_initial_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         prev_state,  /*!< Previous state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( prev_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_rrc_sec_sm_initial_entry() */


/*===========================================================================

  STATE EXIT FUNCTION:  lte_rrc_sec_sm_initial_exit

===========================================================================*/
/*!
    @brief
    Exit function for state machine LTE_RRC_SEC_SM,
    state INITIAL

    @detail
    Called upon exit of this state of the state machine, with optional
    user-passed payload pointer parameter.  The impending state of the state
    machine is also passed as the next_state parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_rrc_sec_sm_initial_exit
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         next_state,  /*!< Next state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( next_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_rrc_sec_sm_initial_exit() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_sec_sm_stopped_indi

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_SEC_SM,
    state INITIAL,
    upon receiving input LTE_RRC_STOPPED_INDI

    @detail
    Called upon receipt of input LTE_RRC_STOPPED_INDI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_sec_sm_stopped_indi
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_sec_sm_stopped_indi() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_sec_sm_security_mode_command_dlm

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_SEC_SM,
    state INITIAL,
    upon receiving input LTE_RRC_SECURITY_MODE_COMMAND_DLM

    @detail
    Called upon receipt of input LTE_RRC_SECURITY_MODE_COMMAND_DLM, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_sec_sm_security_mode_command_dlm
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_sec_sm_security_mode_command_dlm() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_sec_sm_sim_update_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_SEC_SM,
    state INITIAL,
    upon receiving input LTE_RRC_SIM_UPDATE_REQ

    @detail
    Called upon receipt of input LTE_RRC_SIM_UPDATE_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_sec_sm_sim_update_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_sec_sm_sim_update_req() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_sec_sm_security_mode_failure_cnfi

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_SEC_SM,
    state INITIAL,
    upon receiving input LTE_RRC_SECURITY_MODE_FAILURE_CNFI

    @detail
    Called upon receipt of input LTE_RRC_SECURITY_MODE_FAILURE_CNFI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_sec_sm_security_mode_failure_cnfi
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_sec_sm_security_mode_failure_cnfi() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_sec_sm_nas_lte_key_rsp

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_SEC_SM,
    state INITIAL,
    upon receiving input LTE_RRC_NAS_LTE_KEY_RSP

    @detail
    Called upon receipt of input LTE_RRC_NAS_LTE_KEY_RSP, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_sec_sm_nas_lte_key_rsp
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_sec_sm_nas_lte_key_rsp() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_sec_sm_handover_completed_ind

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_SEC_SM,
    state INITIAL,
    upon receiving input LTE_RRC_HANDOVER_COMPLETED_IND

    @detail
    Called upon receipt of input LTE_RRC_HANDOVER_COMPLETED_IND, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_sec_sm_handover_completed_ind
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_sec_sm_handover_completed_ind() */


/*===========================================================================

     (State Machine: LTE_RRC_SEC_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: WT_CFG_CNF

===========================================================================*/

/*===========================================================================

  STATE ENTRY FUNCTION:  lte_rrc_sec_sm_wt_cfg_cnf_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_RRC_SEC_SM,
    state WT_CFG_CNF

    @detail
    Called upon entry into this state of the state machine, with optional
    user-passed payload pointer parameter.  The prior state of the state
    machine is also passed as the prev_state parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_rrc_sec_sm_wt_cfg_cnf_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         prev_state,  /*!< Previous state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( prev_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_rrc_sec_sm_wt_cfg_cnf_entry() */


/*===========================================================================

  STATE EXIT FUNCTION:  lte_rrc_sec_sm_wt_cfg_cnf_exit

===========================================================================*/
/*!
    @brief
    Exit function for state machine LTE_RRC_SEC_SM,
    state WT_CFG_CNF

    @detail
    Called upon exit of this state of the state machine, with optional
    user-passed payload pointer parameter.  The impending state of the state
    machine is also passed as the next_state parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_rrc_sec_sm_wt_cfg_cnf_exit
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         next_state,  /*!< Next state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( next_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_rrc_sec_sm_wt_cfg_cnf_exit() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_sec_sm_cfg_cnfi

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_SEC_SM,
    state WT_CFG_CNF,
    upon receiving input LTE_RRC_CFG_CNFI

    @detail
    Called upon receipt of input LTE_RRC_CFG_CNFI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_sec_sm_cfg_cnfi
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_sec_sm_cfg_cnfi() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_sec_sm_conn_released_indi

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_SEC_SM,
    state WT_CFG_CNF,
    upon receiving input LTE_RRC_CONN_RELEASED_INDI

    @detail
    Called upon receipt of input LTE_RRC_CONN_RELEASED_INDI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_sec_sm_conn_released_indi
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_sec_sm_conn_released_indi() */


/*===========================================================================

     (State Machine: LTE_RRC_SEC_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: SMC_FAIL

===========================================================================*/

/*===========================================================================

  STATE ENTRY FUNCTION:  lte_rrc_sec_sm_smc_fail_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_RRC_SEC_SM,
    state SMC_FAIL

    @detail
    Called upon entry into this state of the state machine, with optional
    user-passed payload pointer parameter.  The prior state of the state
    machine is also passed as the prev_state parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_rrc_sec_sm_smc_fail_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         prev_state,  /*!< Previous state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( prev_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_rrc_sec_sm_smc_fail_entry() */


/*===========================================================================

  STATE EXIT FUNCTION:  lte_rrc_sec_sm_smc_fail_exit

===========================================================================*/
/*!
    @brief
    Exit function for state machine LTE_RRC_SEC_SM,
    state SMC_FAIL

    @detail
    Called upon exit of this state of the state machine, with optional
    user-passed payload pointer parameter.  The impending state of the state
    machine is also passed as the next_state parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_rrc_sec_sm_smc_fail_exit
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         next_state,  /*!< Next state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( next_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_rrc_sec_sm_smc_fail_exit() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_sec_sm_smc_fail_tmri

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_SEC_SM,
    state SMC_FAIL,
    upon receiving input LTE_RRC_SMC_FAIL_TMRI

    @detail
    Called upon receipt of input LTE_RRC_SMC_FAIL_TMRI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_sec_sm_smc_fail_tmri
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_sec_sm_smc_fail_tmri() */


/*===========================================================================

     (State Machine: LTE_RRC_SEC_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: SECURE

===========================================================================*/

/*===========================================================================

  STATE ENTRY FUNCTION:  lte_rrc_sec_sm_secure_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_RRC_SEC_SM,
    state SECURE

    @detail
    Called upon entry into this state of the state machine, with optional
    user-passed payload pointer parameter.  The prior state of the state
    machine is also passed as the prev_state parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_rrc_sec_sm_secure_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         prev_state,  /*!< Previous state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( prev_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_rrc_sec_sm_secure_entry() */


/*===========================================================================

  STATE EXIT FUNCTION:  lte_rrc_sec_sm_secure_exit

===========================================================================*/
/*!
    @brief
    Exit function for state machine LTE_RRC_SEC_SM,
    state SECURE

    @detail
    Called upon exit of this state of the state machine, with optional
    user-passed payload pointer parameter.  The impending state of the state
    machine is also passed as the next_state parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_rrc_sec_sm_secure_exit
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         next_state,  /*!< Next state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( next_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_rrc_sec_sm_secure_exit() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_sec_sm_security_mode_complete_cnfi

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_SEC_SM,
    state SECURE,
    upon receiving input LTE_RRC_SECURITY_MODE_COMPLETE_CNFI

    @detail
    Called upon receipt of input LTE_RRC_SECURITY_MODE_COMPLETE_CNFI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_sec_sm_security_mode_complete_cnfi
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_sec_sm_security_mode_complete_cnfi() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_sec_sm_counter_check_dlm

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_SEC_SM,
    state SECURE,
    upon receiving input LTE_RRC_COUNTER_CHECK_DLM

    @detail
    Called upon receipt of input LTE_RRC_COUNTER_CHECK_DLM, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_sec_sm_counter_check_dlm
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_sec_sm_counter_check_dlm() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_sec_sm_pdcpul_counter_rsp

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_SEC_SM,
    state SECURE,
    upon receiving input LTE_PDCPUL_COUNTER_CNF

    @detail
    Called upon receipt of input LTE_PDCPUL_COUNTER_CNF, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_sec_sm_pdcpul_counter_rsp
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_sec_sm_pdcpul_counter_rsp() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_sec_sm_pdcpdl_counter_rsp

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_SEC_SM,
    state SECURE,
    upon receiving input LTE_PDCPDL_COUNTER_CNF

    @detail
    Called upon receipt of input LTE_PDCPDL_COUNTER_CNF, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_sec_sm_pdcpdl_counter_rsp
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_sec_sm_pdcpdl_counter_rsp() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_sec_sm_cre_completed_indi

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_SEC_SM,
    state SECURE,
    upon receiving input LTE_RRC_CRE_COMPLETED_INDI

    @detail
    Called upon receipt of input LTE_RRC_CRE_COMPLETED_INDI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_sec_sm_cre_completed_indi
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_sec_sm_cre_completed_indi() */




