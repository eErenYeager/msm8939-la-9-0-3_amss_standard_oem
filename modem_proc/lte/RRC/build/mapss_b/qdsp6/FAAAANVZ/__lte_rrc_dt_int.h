/*=============================================================================

    __lte_rrc_dt_int.h

Description:
  This file contains the machine generated header file for the state machine
  specified in the file:
  lte_rrc_dt.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


#ifndef __LTE_RRC_DT_INT_H
#define __LTE_RRC_DT_INT_H

#ifdef __cplusplus
/* If compiled into a C++ file, ensure symbols names are not mangled */
extern "C"
{
#endif

/* Include external state machine header */
#include "__lte_rrc_dt.h"

/* Begin machine generated internal header for state machine array: LTE_RRC_DT_SM[] */

/* Suppress Lint suggestions to const-ify state machine and payload ptrs */
/*lint -esym(818,sm,payload) */

/* Define a macro for the number of SM instances */
#define LTE_RRC_DT_SM_NUM_INSTANCES 1

/* Define a macro for the number of SM states */
#define LTE_RRC_DT_SM_NUM_STATES 2

/* Define a macro for the number of SM inputs */
#define LTE_RRC_DT_SM_NUM_INPUTS 16

#ifndef STM_DATA_STRUCTURES_ONLY
/* State Machine entry/exit function prototypes */
void lte_rrc_dt_sm_entry(stm_state_machine_t *sm,void *payload);


/* State entry/exit function prototypes */


/* Transition function prototypes */
stm_state_t lte_rrc_dt_sm_dl_information_transfer_dlm(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_dt_sm_cdma2000_csfb_parameters_response_dlm(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_dt_sm_ho_from_eutra_prep_req_dlm(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_dt_sm_ready_ul_data_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_dt_sm_ready_irat_tunnel_ul_msg_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_dt_sm_ready_irat_hdr_tunnel_ul_msg_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_dt_sm_ul_information_transfer_cnfi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_dt_sm_handover_rlf_started(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_dt_sm_conn_released_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_dt_sm_stopped(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_dt_sm_blocked_ul_data_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_dt_sm_blocked_irat_tunnel_ul_msg_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_dt_sm_blocked_irat_hdr_tunnel_ul_msg_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_dt_sm_handover_rlf_recovery_success(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_dt_sm_handover_rlf_recovery_failure(stm_state_machine_t *sm, void *payload);


/* State enumeration */
enum
{
  READY,
  BLOCKED,
};

#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated internal header for state machine array: LTE_RRC_DT_SM[] */


#ifdef __cplusplus
} /* extern "C" {...} */
#endif

#endif /* ! __LTE_RRC_DT_INT_H */
