/*!
  @file
  __lte_rrc_misc.stub

  @brief
  This module contains the entry, exit, and transition functions
  necessary to implement the following state machines:

  @detail
  LTE_RRC_MISC_SM ( 1 instance/s )


  OPTIONAL further detailed description of state machines
  - DELETE this section if unused.

*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
===========================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

/* Include STM external API */
#include <stm2.h>

//! @todo Include necessary files here


/*===========================================================================

         STM COMPILER GENERATED PROTOTYPES AND DATA STRUCTURES

===========================================================================*/

/* Include STM compiler generated internal data structure file */
#include "__lte_rrc_misc_int.h"

/*===========================================================================

                         LOCAL VARIABLES

===========================================================================*/


/*! @brief Structure for state-machine per-instance local variables
*/
typedef struct
{
  int   internal_var;  /*!< My internal variable */
  void *internal_ptr;  /*!< My internal pointer */
  //! @todo SM per-instance variables go here
} __lte_rrc_misc_type;


/*! @brief Variables internal to module __lte_rrc_misc.stub
*/
STATIC __lte_rrc_misc_type __lte_rrc_misc;



/*===========================================================================

                 STATE MACHINE: LTE_RRC_MISC_SM

===========================================================================*/

/*===========================================================================

  STATE MACHINE ENTRY FUNCTION:  lte_rrc_misc_sm_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_RRC_MISC_SM

    @detail
    Called upon activation of this state machine, with optional
    user-passed payload pointer parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_rrc_misc_sm_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_rrc_misc_sm_entry() */


/*===========================================================================

     (State Machine: LTE_RRC_MISC_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: INITIAL

===========================================================================*/

/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_misc_sm_handle_procedure_start

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_MISC_SM,
    state INITIAL,
    upon receiving input LTE_RRC_CONN_ESTABLISHMENT_STARTED_INDI

    @detail
    Called upon receipt of input LTE_RRC_CONN_ESTABLISHMENT_STARTED_INDI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_misc_sm_handle_procedure_start
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_misc_sm_handle_procedure_start() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_misc_sm_handle_procedure_stop

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_MISC_SM,
    state INITIAL,
    upon receiving input LTE_RRC_CAMPED_INDI

    @detail
    Called upon receipt of input LTE_RRC_CAMPED_INDI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_misc_sm_handle_procedure_stop
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_misc_sm_handle_procedure_stop() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_sm_process_procedure_status_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_MISC_SM,
    state INITIAL,
    upon receiving input LTE_RRC_PROCEDURE_STATUS_REQ

    @detail
    Called upon receipt of input LTE_RRC_PROCEDURE_STATUS_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_sm_process_procedure_status_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_sm_process_procedure_status_req() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_misc_sm_csfb_call_status_ind

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_MISC_SM,
    state INITIAL,
    upon receiving input LTE_RRC_CSFB_CALL_STATUS_IND

    @detail
    Called upon receipt of input LTE_RRC_CSFB_CALL_STATUS_IND, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_misc_sm_csfb_call_status_ind
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_misc_sm_csfb_call_status_ind() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_misc_sm_handle_ue_mode_ind

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_MISC_SM,
    state INITIAL,
    upon receiving input LTE_RRC_UE_MODE_IND

    @detail
    Called upon receipt of input LTE_RRC_UE_MODE_IND, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_misc_sm_handle_ue_mode_ind
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_misc_sm_handle_ue_mode_ind() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_misc_sm_handle_trm_high_priority_tmri

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_MISC_SM,
    state INITIAL,
    upon receiving input LTE_RRC_TRM_HIGH_PRIORITY_TMRI

    @detail
    Called upon receipt of input LTE_RRC_TRM_HIGH_PRIORITY_TMRI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_misc_sm_handle_trm_high_priority_tmri
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_misc_sm_handle_trm_high_priority_tmri() */




