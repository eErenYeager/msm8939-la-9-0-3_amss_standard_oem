/*!
  @file
  __lte_rrc_config.stub

  @brief
  This module contains the entry, exit, and transition functions
  necessary to implement the following state machines:

  @detail
  LTE_RRC_CONFIG_SM ( 1 instance/s )


  OPTIONAL further detailed description of state machines
  - DELETE this section if unused.

*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
===========================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

/* Include STM external API */
#include <stm2.h>

//! @todo Include necessary files here


/*===========================================================================

         STM COMPILER GENERATED PROTOTYPES AND DATA STRUCTURES

===========================================================================*/

/* Include STM compiler generated internal data structure file */
#include "__lte_rrc_config_int.h"

/*===========================================================================

                         LOCAL VARIABLES

===========================================================================*/


/*! @brief Structure for state-machine per-instance local variables
*/
typedef struct
{
  int   internal_var;  /*!< My internal variable */
  void *internal_ptr;  /*!< My internal pointer */
  //! @todo SM per-instance variables go here
} __lte_rrc_config_type;


/*! @brief Variables internal to module __lte_rrc_config.stub
*/
STATIC __lte_rrc_config_type __lte_rrc_config;



/*===========================================================================

                 STATE MACHINE: LTE_RRC_CONFIG_SM

===========================================================================*/

/*===========================================================================

  STATE MACHINE ENTRY FUNCTION:  lte_rrc_config_sm_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_RRC_CONFIG_SM

    @detail
    Called upon activation of this state machine, with optional
    user-passed payload pointer parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_rrc_config_sm_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_rrc_config_sm_entry() */


/*===========================================================================

     (State Machine: LTE_RRC_CONFIG_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: INITIAL

===========================================================================*/

/*===========================================================================

  STATE ENTRY FUNCTION:  lte_rrc_config_initial_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_RRC_CONFIG_SM,
    state INITIAL

    @detail
    Called upon entry into this state of the state machine, with optional
    user-passed payload pointer parameter.  The prior state of the state
    machine is also passed as the prev_state parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_rrc_config_initial_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         prev_state,  /*!< Previous state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( prev_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_rrc_config_initial_entry() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_reconfig_dlm_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CONFIG_SM,
    state INITIAL,
    upon receiving input LTE_RRC_RRC_CONNECTION_RECONFIGURATION_DLM

    @detail
    Called upon receipt of input LTE_RRC_RRC_CONNECTION_RECONFIGURATION_DLM, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_reconfig_dlm_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_reconfig_dlm_handler() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_mobility_from_eutra_dlm_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CONFIG_SM,
    state INITIAL,
    upon receiving input LTE_RRC_MOBILITY_FROM_EUTRA_COMMAND_DLM

    @detail
    Called upon receipt of input LTE_RRC_MOBILITY_FROM_EUTRA_COMMAND_DLM, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_mobility_from_eutra_dlm_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_mobility_from_eutra_dlm_handler() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_config_cre_complete_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CONFIG_SM,
    state INITIAL,
    upon receiving input LTE_RRC_CRE_COMPLETED_INDI

    @detail
    Called upon receipt of input LTE_RRC_CRE_COMPLETED_INDI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_config_cre_complete_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_config_cre_complete_handler() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_config_psho_started_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CONFIG_SM,
    state INITIAL,
    upon receiving input LTE_RRC_IRAT_TO_LTE_PSHO_STARTED_INDI

    @detail
    Called upon receipt of input LTE_RRC_IRAT_TO_LTE_PSHO_STARTED_INDI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_config_psho_started_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_config_psho_started_handler() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_config_ho_to_eutra_reqi_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CONFIG_SM,
    state INITIAL,
    upon receiving input LTE_RRC_IRAT_TO_LTE_PSHO_REQI

    @detail
    Called upon receipt of input LTE_RRC_IRAT_TO_LTE_PSHO_REQI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_config_ho_to_eutra_reqi_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_config_ho_to_eutra_reqi_handler() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_config_reconfig_wait_tmri_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CONFIG_SM,
    state INITIAL,
    upon receiving input LTE_RRC_RECONFIG_WAIT_TMRI

    @detail
    Called upon receipt of input LTE_RRC_RECONFIG_WAIT_TMRI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_config_reconfig_wait_tmri_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_config_reconfig_wait_tmri_handler() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_config_conn_rel_lte_stopped_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CONFIG_SM,
    state INITIAL,
    upon receiving input LTE_RRC_CONN_REL_STARTED_INDI

    @detail
    Called upon receipt of input LTE_RRC_CONN_REL_STARTED_INDI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_config_conn_rel_lte_stopped_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_config_conn_rel_lte_stopped_handler() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_config_idle_transition_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CONFIG_SM,
    state INITIAL,
    upon receiving input LTE_RRC_CONN_RELEASED_INDI

    @detail
    Called upon receipt of input LTE_RRC_CONN_RELEASED_INDI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_config_idle_transition_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_config_idle_transition_handler() */


/*===========================================================================

     (State Machine: LTE_RRC_CONFIG_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: WAIT_FOR_CFG_CNFI

===========================================================================*/

/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_config_cfg_cnfi_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CONFIG_SM,
    state WAIT_FOR_CFG_CNFI,
    upon receiving input LTE_RRC_CFG_CNFI

    @detail
    Called upon receipt of input LTE_RRC_CFG_CNFI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_config_cfg_cnfi_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_config_cfg_cnfi_handler() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_config_failure_during_rach_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CONFIG_SM,
    state WAIT_FOR_CFG_CNFI,
    upon receiving input LTE_RRC_T304_TMRI

    @detail
    Called upon receipt of input LTE_RRC_T304_TMRI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_config_failure_during_rach_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_config_failure_during_rach_handler() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_config_ho_to_eutra_abort_reqi_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CONFIG_SM,
    state WAIT_FOR_CFG_CNFI,
    upon receiving input LTE_RRC_IRAT_TO_LTE_PSHO_ABORT_REQI

    @detail
    Called upon receipt of input LTE_RRC_IRAT_TO_LTE_PSHO_ABORT_REQI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_config_ho_to_eutra_abort_reqi_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_config_ho_to_eutra_abort_reqi_handler() */


/*===========================================================================

     (State Machine: LTE_RRC_CONFIG_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: WAIT_FOR_MEAS_CFG_CNFI

===========================================================================*/

/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_config_meas_cfg_cnfi_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CONFIG_SM,
    state WAIT_FOR_MEAS_CFG_CNFI,
    upon receiving input LTE_RRC_MEAS_CFG_CNFI

    @detail
    Called upon receipt of input LTE_RRC_MEAS_CFG_CNFI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_config_meas_cfg_cnfi_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_config_meas_cfg_cnfi_handler() */


/*===========================================================================

     (State Machine: LTE_RRC_CONFIG_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: WAIT_FOR_MAC_ACCESS_CNF

===========================================================================*/

/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_config_mac_access_cnf_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CONFIG_SM,
    state WAIT_FOR_MAC_ACCESS_CNF,
    upon receiving input LTE_MAC_ACCESS_CNF

    @detail
    Called upon receipt of input LTE_MAC_ACCESS_CNF, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_config_mac_access_cnf_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_config_mac_access_cnf_handler() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_config_mac_access_abort_cnf_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CONFIG_SM,
    state WAIT_FOR_MAC_ACCESS_CNF,
    upon receiving input LTE_MAC_ACCESS_ABORT_CNF

    @detail
    Called upon receipt of input LTE_MAC_ACCESS_ABORT_CNF, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_config_mac_access_abort_cnf_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_config_mac_access_abort_cnf_handler() */


/*===========================================================================

     (State Machine: LTE_RRC_CONFIG_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: WAIT_FOR_PS_HO_TO_COMPLETE

===========================================================================*/

/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_config_psho_complete_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CONFIG_SM,
    state WAIT_FOR_PS_HO_TO_COMPLETE,
    upon receiving input LTE_RRC_IRAT_TO_LTE_PSHO_COMPLETED_INDI

    @detail
    Called upon receipt of input LTE_RRC_IRAT_TO_LTE_PSHO_COMPLETED_INDI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_config_psho_complete_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_config_psho_complete_handler() */




