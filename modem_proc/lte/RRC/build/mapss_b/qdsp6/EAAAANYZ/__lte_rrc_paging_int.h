/*=============================================================================

    __lte_rrc_paging_int.h

Description:
  This file contains the machine generated header file for the state machine
  specified in the file:
  lte_rrc_paging.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


#ifndef __LTE_RRC_PAGING_INT_H
#define __LTE_RRC_PAGING_INT_H

#ifdef __cplusplus
/* If compiled into a C++ file, ensure symbols names are not mangled */
extern "C"
{
#endif

/* Include external state machine header */
#include "__lte_rrc_paging.h"

/* Begin machine generated internal header for state machine array: LTE_RRC_PAGING_SM[] */

/* Suppress Lint suggestions to const-ify state machine and payload ptrs */
/*lint -esym(818,sm,payload) */

/* Define a macro for the number of SM instances */
#define LTE_RRC_PAGING_SM_NUM_INSTANCES 1

/* Define a macro for the number of SM states */
#define LTE_RRC_PAGING_SM_NUM_STATES 3

/* Define a macro for the number of SM inputs */
#define LTE_RRC_PAGING_SM_NUM_INPUTS 9

#ifndef STM_DATA_STRUCTURES_ONLY
/* State Machine entry/exit function prototypes */
void lte_rrc_paging_sm_entry(stm_state_machine_t *sm,void *payload);
void lte_rrc_paging_sm_exit(stm_state_machine_t *sm,void *payload);


/* State entry/exit function prototypes */
void lte_rrc_paging_sm_initial_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_paging_sm_initial_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_paging_sm_idle_camped_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_paging_sm_idle_camped_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_paging_sm_connected_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_paging_sm_connected_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);


/* Transition function prototypes */
stm_state_t lte_rrc_paging_sm_camped_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_paging_sm_drx_info_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_paging_sm_sim_update_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_paging_sm_service_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_paging_sm_paging_dlm(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_paging_sm_connected_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_paging_sm_stopped_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_paging_sm_not_camped_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_paging_sm_sib_updated_indi(stm_state_machine_t *sm, void *payload);


/* State enumeration */
enum
{
  INITIAL,
  IDLE_CAMPED,
  CONNECTED,
};

#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated internal header for state machine array: LTE_RRC_PAGING_SM[] */


#ifdef __cplusplus
} /* extern "C" {...} */
#endif

#endif /* ! __LTE_RRC_PAGING_INT_H */
