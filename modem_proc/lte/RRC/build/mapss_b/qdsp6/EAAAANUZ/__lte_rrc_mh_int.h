/*=============================================================================

    __lte_rrc_mh_int.h

Description:
  This file contains the machine generated header file for the state machine
  specified in the file:
  lte_rrc_mh.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


#ifndef __LTE_RRC_MH_INT_H
#define __LTE_RRC_MH_INT_H

#ifdef __cplusplus
/* If compiled into a C++ file, ensure symbols names are not mangled */
extern "C"
{
#endif

/* Include external state machine header */
#include "__lte_rrc_mh.h"

/* Begin machine generated internal header for state machine array: LTE_RRC_MH_SM[] */

/* Suppress Lint suggestions to const-ify state machine and payload ptrs */
/*lint -esym(818,sm,payload) */

/* Define a macro for the number of SM instances */
#define LTE_RRC_MH_SM_NUM_INSTANCES 1

/* Define a macro for the number of SM states */
#define LTE_RRC_MH_SM_NUM_STATES 2

/* Define a macro for the number of SM inputs */
#define LTE_RRC_MH_SM_NUM_INPUTS 14

#ifndef STM_DATA_STRUCTURES_ONLY
/* State Machine entry/exit function prototypes */
void lte_rrc_mh_sm_entry(stm_state_machine_t *sm,void *payload);


/* State entry/exit function prototypes */


/* Transition function prototypes */
stm_state_t lte_rrc_mh_sm_send_ul_msg_reqi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_mh_sm_go_to_ho(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_mh_sm_go_to_initial(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_mh_sm_dlm_processed_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_mh_sm_camped_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_mh_sm_ps_ho_started_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_mh_sm_dl_data_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_mh_sm_pdcpul_sdu_cnf(stm_state_machine_t *sm, void *payload);


/* State enumeration */
enum
{
  INITIAL,
  HANDOVER,
};

#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated internal header for state machine array: LTE_RRC_MH_SM[] */


#ifdef __cplusplus
} /* extern "C" {...} */
#endif

#endif /* ! __LTE_RRC_MH_INT_H */
