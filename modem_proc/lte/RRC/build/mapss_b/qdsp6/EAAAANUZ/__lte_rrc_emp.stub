/*!
  @file
  __lte_rrc_emp.stub

  @brief
  This module contains the entry, exit, and transition functions
  necessary to implement the following state machines:

  @detail
  LTE_RRC_EMP_SM ( 1 instance/s )


  OPTIONAL further detailed description of state machines
  - DELETE this section if unused.

*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
===========================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

/* Include STM external API */
#include <stm2.h>

//! @todo Include necessary files here


/*===========================================================================

         STM COMPILER GENERATED PROTOTYPES AND DATA STRUCTURES

===========================================================================*/

/* Include STM compiler generated internal data structure file */
#include "__lte_rrc_emp_int.h"

/*===========================================================================

                         LOCAL VARIABLES

===========================================================================*/


/*! @brief Structure for state-machine per-instance local variables
*/
typedef struct
{
  int   internal_var;  /*!< My internal variable */
  void *internal_ptr;  /*!< My internal pointer */
  //! @todo SM per-instance variables go here
} __lte_rrc_emp_type;


/*! @brief Variables internal to module __lte_rrc_emp.stub
*/
STATIC __lte_rrc_emp_type __lte_rrc_emp;



/*===========================================================================

                 STATE MACHINE: LTE_RRC_EMP_SM

===========================================================================*/

/*===========================================================================

  STATE MACHINE ENTRY FUNCTION:  lte_rrc_emp_sm_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_RRC_EMP_SM

    @detail
    Called upon activation of this state machine, with optional
    user-passed payload pointer parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_rrc_emp_sm_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_rrc_emp_sm_entry() */


/*===========================================================================

     (State Machine: LTE_RRC_EMP_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: INACTIVE

===========================================================================*/

/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_emp_sm_act_tmgi_reqi_init

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_EMP_SM,
    state INACTIVE,
    upon receiving input LTE_RRC_ESMGR_ACT_TMGI_REQI

    @detail
    Called upon receipt of input LTE_RRC_ESMGR_ACT_TMGI_REQI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_emp_sm_act_tmgi_reqi_init
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_emp_sm_act_tmgi_reqi_init() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_emp_sm_deact_tmgi_reqi

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_EMP_SM,
    state INACTIVE,
    upon receiving input LTE_RRC_ESMGR_DEACT_TMGI_REQI

    @detail
    Called upon receipt of input LTE_RRC_ESMGR_DEACT_TMGI_REQI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_emp_sm_deact_tmgi_reqi
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_emp_sm_deact_tmgi_reqi() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_emp_sm_avail_tmgi_list_reqi

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_EMP_SM,
    state INACTIVE,
    upon receiving input LTE_RRC_ESMGR_AVAIL_TMGI_LIST_REQI

    @detail
    Called upon receipt of input LTE_RRC_ESMGR_AVAIL_TMGI_LIST_REQI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_emp_sm_avail_tmgi_list_reqi
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_emp_sm_avail_tmgi_list_reqi() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_emp_sm_stopped_indi

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_EMP_SM,
    state INACTIVE,
    upon receiving input LTE_RRC_STOPPED_INDI

    @detail
    Called upon receipt of input LTE_RRC_STOPPED_INDI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_emp_sm_stopped_indi
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_emp_sm_stopped_indi() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_emp_sm_rlcdl_mcch_pdu_ind

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_EMP_SM,
    state INACTIVE,
    upon receiving input LTE_RLCDL_MCCH_PDU_IND

    @detail
    Called upon receipt of input LTE_RLCDL_MCCH_PDU_IND, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_emp_sm_rlcdl_mcch_pdu_ind
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_emp_sm_rlcdl_mcch_pdu_ind() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_emp_sm_mcch_change_notification

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_EMP_SM,
    state INACTIVE,
    upon receiving input LTE_CPHY_MCCH_CHANGE_NOTIFICATION_IND

    @detail
    Called upon receipt of input LTE_CPHY_MCCH_CHANGE_NOTIFICATION_IND, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_emp_sm_mcch_change_notification
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_emp_sm_mcch_change_notification() */


/*===========================================================================

     (State Machine: LTE_RRC_EMP_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: ACTIVE

===========================================================================*/

/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_emp_sm_act_tmgi_reqi

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_EMP_SM,
    state ACTIVE,
    upon receiving input LTE_RRC_ESMGR_ACT_TMGI_REQI

    @detail
    Called upon receipt of input LTE_RRC_ESMGR_ACT_TMGI_REQI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_emp_sm_act_tmgi_reqi
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_emp_sm_act_tmgi_reqi() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_emp_sm_sib_updated_indi

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_EMP_SM,
    state ACTIVE,
    upon receiving input LTE_RRC_SIB_UPDATED_INDI

    @detail
    Called upon receipt of input LTE_RRC_SIB_UPDATED_INDI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_emp_sm_sib_updated_indi
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_emp_sm_sib_updated_indi() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_emp_sm_cfg_cnfi

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_EMP_SM,
    state ACTIVE,
    upon receiving input LTE_RRC_CFG_CNFI

    @detail
    Called upon receipt of input LTE_RRC_CFG_CNFI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_emp_sm_cfg_cnfi
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_emp_sm_cfg_cnfi() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_emp_sm_mcch_wt_tmri

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_EMP_SM,
    state ACTIVE,
    upon receiving input LTE_RRC_EMP_MCCH0_WT_TMRI

    @detail
    Called upon receipt of input LTE_RRC_EMP_MCCH0_WT_TMRI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_emp_sm_mcch_wt_tmri
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_emp_sm_mcch_wt_tmri() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_emp_sm_service_ind

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_EMP_SM,
    state ACTIVE,
    upon receiving input LTE_RRC_SERVICE_IND

    @detail
    Called upon receipt of input LTE_RRC_SERVICE_IND, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_emp_sm_service_ind
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_emp_sm_service_ind() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_emp_sm_oos_tmri

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_EMP_SM,
    state ACTIVE,
    upon receiving input LTE_RRC_EMP_OOS_TMRI

    @detail
    Called upon receipt of input LTE_RRC_EMP_OOS_TMRI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_emp_sm_oos_tmri
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_emp_sm_oos_tmri() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_emp_sm_act_deact_tmgi_reqi

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_EMP_SM,
    state ACTIVE,
    upon receiving input LTE_RRC_ESMGR_ACT_DEACT_TMGI_REQI

    @detail
    Called upon receipt of input LTE_RRC_ESMGR_ACT_DEACT_TMGI_REQI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_emp_sm_act_deact_tmgi_reqi
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_emp_sm_act_deact_tmgi_reqi() */


/*===========================================================================

     (State Machine: LTE_RRC_EMP_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: WAIT_FOR_CNF

===========================================================================*/

/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_emp_sm_act_tmgi_reqi_pend

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_EMP_SM,
    state WAIT_FOR_CNF,
    upon receiving input LTE_RRC_ESMGR_ACT_TMGI_REQI

    @detail
    Called upon receipt of input LTE_RRC_ESMGR_ACT_TMGI_REQI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_emp_sm_act_tmgi_reqi_pend
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_emp_sm_act_tmgi_reqi_pend() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_emp_sm_deact_tmgi_reqi_pend

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_EMP_SM,
    state WAIT_FOR_CNF,
    upon receiving input LTE_RRC_ESMGR_DEACT_TMGI_REQI

    @detail
    Called upon receipt of input LTE_RRC_ESMGR_DEACT_TMGI_REQI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_emp_sm_deact_tmgi_reqi_pend
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_emp_sm_deact_tmgi_reqi_pend() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_emp_sm_sib_updated_indi_pend

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_EMP_SM,
    state WAIT_FOR_CNF,
    upon receiving input LTE_RRC_SIB_UPDATED_INDI

    @detail
    Called upon receipt of input LTE_RRC_SIB_UPDATED_INDI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_emp_sm_sib_updated_indi_pend
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_emp_sm_sib_updated_indi_pend() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_emp_sm_rlcdl_mcch_pdu_ind_pend

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_EMP_SM,
    state WAIT_FOR_CNF,
    upon receiving input LTE_RLCDL_MCCH_PDU_IND

    @detail
    Called upon receipt of input LTE_RLCDL_MCCH_PDU_IND, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_emp_sm_rlcdl_mcch_pdu_ind_pend
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_emp_sm_rlcdl_mcch_pdu_ind_pend() */




