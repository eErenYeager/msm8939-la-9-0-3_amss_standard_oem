/*=============================================================================

    __lte_rrc_sib_int.h

Description:
  This file contains the machine generated header file for the state machine
  specified in the file:
  lte_rrc_sib.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


#ifndef __LTE_RRC_SIB_INT_H
#define __LTE_RRC_SIB_INT_H

#ifdef __cplusplus
/* If compiled into a C++ file, ensure symbols names are not mangled */
extern "C"
{
#endif

/* Include external state machine header */
#include "__lte_rrc_sib.h"

/* Begin machine generated internal header for state machine array: LTE_RRC_SIB_SM[] */

/* Suppress Lint suggestions to const-ify state machine and payload ptrs */
/*lint -esym(818,sm,payload) */

/* Define a macro for the number of SM instances */
#define LTE_RRC_SIB_SM_NUM_INSTANCES 1

/* Define a macro for the number of SM states */
#define LTE_RRC_SIB_SM_NUM_STATES 2

/* Define a macro for the number of SM inputs */
#define LTE_RRC_SIB_SM_NUM_INPUTS 22

#ifndef STM_DATA_STRUCTURES_ONLY
/* State Machine entry/exit function prototypes */
void lte_rrc_sib_sm_entry(stm_state_machine_t *sm,void *payload);


/* State entry/exit function prototypes */


/* Transition function prototypes */
stm_state_t lte_rrc_sib_sm_get_sibs_reqi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_sib_sm_stopped_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_sib_sm_sib_3hr_clock_tick_tmri(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_sib_sm_periodic_sib8_tmri(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_sib_sm_dl_data_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_sib_sm_tdd_cfg_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_sib_sm_msg_id_list_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_sib_sm_periodic_sib16_tmri(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_sib_sm_utc_time_update_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_sib_sm_deact_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_sib_sm_rf_unavail_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_sib_sm_rf_avail_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_sib_sm_resel_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_sib_sm_serv_cell_sib_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_sib_sm_sib_abort_reqi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_sib_sm_camped_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_sib_sm_connected_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_sib_sm_sib_wt_tmri(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_sib_sm_ho_started_indi(stm_state_machine_t *sm, void *payload);


/* State enumeration */
enum
{
  INACTIVE,
  ACTIVE,
};

#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated internal header for state machine array: LTE_RRC_SIB_SM[] */


#ifdef __cplusplus
} /* extern "C" {...} */
#endif

#endif /* ! __LTE_RRC_SIB_INT_H */
