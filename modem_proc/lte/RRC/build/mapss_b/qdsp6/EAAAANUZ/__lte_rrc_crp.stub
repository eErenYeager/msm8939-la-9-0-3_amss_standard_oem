/*!
  @file
  __lte_rrc_crp.stub

  @brief
  This module contains the entry, exit, and transition functions
  necessary to implement the following state machines:

  @detail
  LTE_RRC_CRP_SM ( 1 instance/s )


  OPTIONAL further detailed description of state machines
  - DELETE this section if unused.

*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
===========================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

/* Include STM external API */
#include <stm2.h>

//! @todo Include necessary files here


/*===========================================================================

         STM COMPILER GENERATED PROTOTYPES AND DATA STRUCTURES

===========================================================================*/

/* Include STM compiler generated internal data structure file */
#include "__lte_rrc_crp_int.h"

/*===========================================================================

                         LOCAL VARIABLES

===========================================================================*/


/*! @brief Structure for state-machine per-instance local variables
*/
typedef struct
{
  int   internal_var;  /*!< My internal variable */
  void *internal_ptr;  /*!< My internal pointer */
  //! @todo SM per-instance variables go here
} __lte_rrc_crp_type;


/*! @brief Variables internal to module __lte_rrc_crp.stub
*/
STATIC __lte_rrc_crp_type __lte_rrc_crp;



/*===========================================================================

                 STATE MACHINE: LTE_RRC_CRP_SM

===========================================================================*/

/*===========================================================================

  STATE MACHINE ENTRY FUNCTION:  lte_rrc_crp_sm_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_RRC_CRP_SM

    @detail
    Called upon activation of this state machine, with optional
    user-passed payload pointer parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_rrc_crp_sm_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_rrc_crp_sm_entry() */


/*===========================================================================

     (State Machine: LTE_RRC_CRP_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: INACTIVE

===========================================================================*/

/*===========================================================================

  STATE ENTRY FUNCTION:  lte_rrc_crp_sm_inactive_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_RRC_CRP_SM,
    state INACTIVE

    @detail
    Called upon entry into this state of the state machine, with optional
    user-passed payload pointer parameter.  The prior state of the state
    machine is also passed as the prev_state parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_rrc_crp_sm_inactive_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         prev_state,  /*!< Previous state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( prev_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_rrc_crp_sm_inactive_entry() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_crp_sm_conn_rel_dlm

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CRP_SM,
    state INACTIVE,
    upon receiving input LTE_RRC_RRC_CONNECTION_RELEASE_DLM

    @detail
    Called upon receipt of input LTE_RRC_RRC_CONNECTION_RELEASE_DLM, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_crp_sm_conn_rel_dlm
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_crp_sm_conn_rel_dlm() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_crp_sm_conn_abort_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CRP_SM,
    state INACTIVE,
    upon receiving input LTE_RRC_CONN_ABORT_REQ

    @detail
    Called upon receipt of input LTE_RRC_CONN_ABORT_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_crp_sm_conn_abort_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_crp_sm_conn_abort_req() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_crp_initiate_conn_rel_indi

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CRP_SM,
    state INACTIVE,
    upon receiving input LTE_RRC_INITIATE_CONN_REL_INDI

    @detail
    Called upon receipt of input LTE_RRC_INITIATE_CONN_REL_INDI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_crp_initiate_conn_rel_indi
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_crp_initiate_conn_rel_indi() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_crp_sm_stopped_indi

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CRP_SM,
    state INACTIVE,
    upon receiving input LTE_RRC_STOPPED_INDI

    @detail
    Called upon receipt of input LTE_RRC_STOPPED_INDI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_crp_sm_stopped_indi
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_crp_sm_stopped_indi() */


/*===========================================================================

     (State Machine: LTE_RRC_CRP_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: WAIT_FOR_TIMER

===========================================================================*/

/*===========================================================================

  STATE ENTRY FUNCTION:  lte_rrc_crp_sm_wait_for_timer_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_RRC_CRP_SM,
    state WAIT_FOR_TIMER

    @detail
    Called upon entry into this state of the state machine, with optional
    user-passed payload pointer parameter.  The prior state of the state
    machine is also passed as the prev_state parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_rrc_crp_sm_wait_for_timer_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         prev_state,  /*!< Previous state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( prev_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_rrc_crp_sm_wait_for_timer_entry() */


/*===========================================================================

  STATE EXIT FUNCTION:  lte_rrc_crp_sm_wait_for_timer_exit

===========================================================================*/
/*!
    @brief
    Exit function for state machine LTE_RRC_CRP_SM,
    state WAIT_FOR_TIMER

    @detail
    Called upon exit of this state of the state machine, with optional
    user-passed payload pointer parameter.  The impending state of the state
    machine is also passed as the next_state parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_rrc_crp_sm_wait_for_timer_exit
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         next_state,  /*!< Next state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( next_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_rrc_crp_sm_wait_for_timer_exit() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_crp_sm_conn_rel_tmri

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CRP_SM,
    state WAIT_FOR_TIMER,
    upon receiving input LTE_RRC_CONN_REL_TMRI

    @detail
    Called upon receipt of input LTE_RRC_CONN_REL_TMRI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_crp_sm_conn_rel_tmri
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_crp_sm_conn_rel_tmri() */


/*===========================================================================

     (State Machine: LTE_RRC_CRP_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: WAIT_FOR_LLC_CONFIRM

===========================================================================*/

/*===========================================================================

  STATE ENTRY FUNCTION:  lte_rrc_crp_sm_wait_for_llc_confirm_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_RRC_CRP_SM,
    state WAIT_FOR_LLC_CONFIRM

    @detail
    Called upon entry into this state of the state machine, with optional
    user-passed payload pointer parameter.  The prior state of the state
    machine is also passed as the prev_state parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_rrc_crp_sm_wait_for_llc_confirm_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         prev_state,  /*!< Previous state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( prev_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_rrc_crp_sm_wait_for_llc_confirm_entry() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_crp_sm_cfg_cnfi

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CRP_SM,
    state WAIT_FOR_LLC_CONFIRM,
    upon receiving input LTE_RRC_CFG_CNFI

    @detail
    Called upon receipt of input LTE_RRC_CFG_CNFI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_crp_sm_cfg_cnfi
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_crp_sm_cfg_cnfi() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_crp_sm_conn_rel_cnf

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CRP_SM,
    state WAIT_FOR_LLC_CONFIRM,
    upon receiving input LTE_CPHY_CON_RELEASE_CNF

    @detail
    Called upon receipt of input LTE_CPHY_CON_RELEASE_CNF, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_crp_sm_conn_rel_cnf
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_crp_sm_conn_rel_cnf() */


/*===========================================================================

     (State Machine: LTE_RRC_CRP_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: WAIT_FOR_IRAT_ABORT_CONFIRM

===========================================================================*/

/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_crp_sm_irat_abort_cnfi

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CRP_SM,
    state WAIT_FOR_IRAT_ABORT_CONFIRM,
    upon receiving input LTE_RRC_IRAT_FROM_LTE_ABORT_CNFI

    @detail
    Called upon receipt of input LTE_RRC_IRAT_FROM_LTE_ABORT_CNFI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_crp_sm_irat_abort_cnfi
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_crp_sm_irat_abort_cnfi() */




