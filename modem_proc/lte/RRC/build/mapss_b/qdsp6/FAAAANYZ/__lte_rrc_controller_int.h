/*=============================================================================

    __lte_rrc_controller_int.h

Description:
  This file contains the machine generated header file for the state machine
  specified in the file:
  lte_rrc_controller.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


#ifndef __LTE_RRC_CONTROLLER_INT_H
#define __LTE_RRC_CONTROLLER_INT_H

#ifdef __cplusplus
/* If compiled into a C++ file, ensure symbols names are not mangled */
extern "C"
{
#endif

/* Include external state machine header */
#include "__lte_rrc_controller.h"

/* Begin machine generated internal header for state machine array: LTE_RRC_CONTROLLER_SM[] */

/* Suppress Lint suggestions to const-ify state machine and payload ptrs */
/*lint -esym(818,sm,payload) */

/* Define a macro for the number of SM instances */
#define LTE_RRC_CONTROLLER_SM_NUM_INSTANCES 1

/* Define a macro for the number of SM states */
#define LTE_RRC_CONTROLLER_SM_NUM_STATES 8

/* Define a macro for the number of SM inputs */
#define LTE_RRC_CONTROLLER_SM_NUM_INPUTS 28

#ifndef STM_DATA_STRUCTURES_ONLY
/* State Machine entry/exit function prototypes */
void lte_rrc_ctlr_sm_entry(stm_state_machine_t *sm,void *payload);


/* State entry/exit function prototypes */
void lte_rrc_ctlr_sm_inactive_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_ctlr_sm_idle_not_camped_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_ctlr_sm_idle_camped_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_ctlr_sm_connecting_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_ctlr_sm_connected_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_ctlr_sm_suspended_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_ctlr_sm_irat_to_lte_started_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_ctlr_sm_closing_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);


/* Transition function prototypes */
stm_state_t lte_rrc_ctlr_sm_mode_change_reqi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_ctlr_sm_cfg_cnfi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_ctlr_sm_start_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_ctlr_sm_stop_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_ctlr_sm_sim_update_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_ctlr_sm_mac_start_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_ctlr_sm_mac_stop_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_ctlr_sm_rlcdl_start_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_ctlr_sm_rlcdl_stop_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_ctlr_sm_rlcul_start_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_ctlr_sm_rlcul_stop_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_ctlr_sm_pdcpdl_start_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_ctlr_sm_pdcpdl_stop_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_ctlr_sm_pdcpul_start_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_ctlr_sm_pdcpul_stop_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_ctlr_sm_deadlock_tmri(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_ctlr_sm_trm_priority_change_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_ctlr_sm_rf_available_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_ctlr_sm_rf_unavailable_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_ctlr_sm_ds_status_change_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_ctlr_sm_trm_priority_change_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_ctlr_sm_camped_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_ctlr_sm_not_camped_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_ctlr_sm_conn_est_started_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_ctlr_sm_connected_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_ctlr_sm_conn_rel_started_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_ctlr_sm_connected_camped_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_ctlr_sm_suspend_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_ctlr_sm_resume_cnf(stm_state_machine_t *sm, void *payload);


/* State enumeration */
enum
{
  INACTIVE,
  IDLE_NOT_CAMPED,
  IDLE_CAMPED,
  CONNECTING,
  CONNECTED,
  SUSPENDED,
  IRAT_TO_LTE_STARTED,
  CLOSING,
};

#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated internal header for state machine array: LTE_RRC_CONTROLLER_SM[] */


#ifdef __cplusplus
} /* extern "C" {...} */
#endif

#endif /* ! __LTE_RRC_CONTROLLER_INT_H */
