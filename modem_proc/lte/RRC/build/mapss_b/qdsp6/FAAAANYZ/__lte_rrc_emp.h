/*=============================================================================

    __lte_rrc_emp.h

Description:
  This file contains the machine generated header file for the state machine
  specified in the file:
  lte_rrc_emp.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


#ifndef __LTE_RRC_EMP_H
#define __LTE_RRC_EMP_H

#ifdef __cplusplus
/* If compiled into a C++ file, ensure symbols names are not mangled */
extern "C"
{
#endif

/* Include STM framework header */
#include <stm2.h>

/* Begin machine generated code for state machine array: LTE_RRC_EMP_SM[] */

/* Define a macro for the number of SM instances */
#define LTE_RRC_EMP_SM_NUM_INSTANCES 1

/* External reference to state machine structure */
extern stm_state_machine_t LTE_RRC_EMP_SM[ LTE_RRC_EMP_SM_NUM_INSTANCES ];

/* External enumeration representing state machine's states */
enum
{
  LTE_RRC_EMP_SM__INACTIVE,
  LTE_RRC_EMP_SM__ACTIVE,
  LTE_RRC_EMP_SM__WAIT_FOR_CNF,
};

#ifndef STM_DATA_STRUCTURES_ONLY
/* User called 'reset' routine.  Should never be needed, but can be used to
   effect a complete reset of all a given state machine's instances. */
extern void LTE_RRC_EMP_SM_reset(void);
#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated code for state machine array: LTE_RRC_EMP_SM[] */


#ifdef __cplusplus
} /* extern "C" {...} */
#endif

#endif /* __LTE_RRC_EMP_H */
