/*=============================================================================

    __lte_rrc_esmgr_int.h

Description:
  This file contains the machine generated header file for the state machine
  specified in the file:
  lte_rrc_esmgr.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


#ifndef __LTE_RRC_ESMGR_INT_H
#define __LTE_RRC_ESMGR_INT_H

#ifdef __cplusplus
/* If compiled into a C++ file, ensure symbols names are not mangled */
extern "C"
{
#endif

/* Include external state machine header */
#include "__lte_rrc_esmgr.h"

/* Begin machine generated internal header for state machine array: LTE_RRC_ESMGR_SM[] */

/* Suppress Lint suggestions to const-ify state machine and payload ptrs */
/*lint -esym(818,sm,payload) */

/* Define a macro for the number of SM instances */
#define LTE_RRC_ESMGR_SM_NUM_INSTANCES 1

/* Define a macro for the number of SM states */
#define LTE_RRC_ESMGR_SM_NUM_STATES 3

/* Define a macro for the number of SM inputs */
#define LTE_RRC_ESMGR_SM_NUM_INPUTS 39

#ifndef STM_DATA_STRUCTURES_ONLY
/* State Machine entry/exit function prototypes */
void lte_rrc_esmgr_sm_entry(stm_state_machine_t *sm,void *payload);


/* State entry/exit function prototypes */


/* Transition function prototypes */
stm_state_t lte_rrc_esmgr_sm_enable_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_esmgr_sm_disable_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_esmgr_sm_act_tmgi_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_esmgr_sm_deact_tmgi_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_esmgr_sm_act_deact_tmgi_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_esmgr_sm_avail_tmgi_list_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_esmgr_sm_tlb_cfg_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_esmgr_sm_sig_strength_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_esmgr_sm_sig_strength_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_esmgr_sm_avail_sai_list_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_esmgr_sm_cmapi_embms_cov_status(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_esmgr_sm_cmapi_embms_datamcs_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_esmgr_sm_lte_stopped(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_esmgr_sm_cm_emer_mode_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_esmgr_sm_trm_prio_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_esmgr_sm_avail_tmgi_list_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_esmgr_sm_cm_voice_call_status_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_esmgr_sm_wms_sms_status_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_esmgr_sm_act_tmgi_cnfi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_esmgr_sm_deact_tmgi_cnfi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_esmgr_sm_act_deact_tmgi_cnfi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_esmgr_sm_service_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_esmgr_sm_oos_warn_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_esmgr_sm_sib_updated_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_esmgr_sm_act_tmgi_guard_tmri(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_esmgr_sm_connected_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_esmgr_sm_best_neigh_freq_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_esmgr_sm_mbms_interest_ind_tmri(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_esmgr_sm_handover_cre_started_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_esmgr_sm_handover_cre_completed_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_esmgr_sm_get_sibs_cnfi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_esmgr_sm_camped_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_esmgr_sm_sib_not_rcvd_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_esmgr_sm_clear_cfl_wait_tmri(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_esmgr_sm_rf_unavail_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_esmgr_sm_rf_avail_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_esmgr_sm_pend_cmd(stm_state_machine_t *sm, void *payload);


/* State enumeration */
enum
{
  DISABLED,
  ENABLED,
  WAIT_FOR_CNF,
};

#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated internal header for state machine array: LTE_RRC_ESMGR_SM[] */


#ifdef __cplusplus
} /* extern "C" {...} */
#endif

#endif /* ! __LTE_RRC_ESMGR_INT_H */
