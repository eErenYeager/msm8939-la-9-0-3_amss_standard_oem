/*!
  @file
  __lte_rrc_csg_asf.stub

  @brief
  This module contains the entry, exit, and transition functions
  necessary to implement the following state machines:

  @detail
  LTE_RRC_CSG_ASF_SM ( 1 instance/s )


  OPTIONAL further detailed description of state machines
  - DELETE this section if unused.

*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
===========================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

/* Include STM external API */
#include <stm2.h>

//! @todo Include necessary files here


/*===========================================================================

         STM COMPILER GENERATED PROTOTYPES AND DATA STRUCTURES

===========================================================================*/

/* Include STM compiler generated internal data structure file */
#include "__lte_rrc_csg_asf_int.h"

/*===========================================================================

                         LOCAL VARIABLES

===========================================================================*/


/*! @brief Structure for state-machine per-instance local variables
*/
typedef struct
{
  int   internal_var;  /*!< My internal variable */
  void *internal_ptr;  /*!< My internal pointer */
  //! @todo SM per-instance variables go here
} __lte_rrc_csg_asf_type;


/*! @brief Variables internal to module __lte_rrc_csg_asf.stub
*/
STATIC __lte_rrc_csg_asf_type __lte_rrc_csg_asf;



/*===========================================================================

                 STATE MACHINE: LTE_RRC_CSG_ASF_SM

===========================================================================*/

/*===========================================================================

  STATE MACHINE ENTRY FUNCTION:  lte_rrc_csg_asf_sm_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_RRC_CSG_ASF_SM

    @detail
    Called upon activation of this state machine, with optional
    user-passed payload pointer parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_rrc_csg_asf_sm_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_rrc_csg_asf_sm_entry() */


/*===========================================================================

     (State Machine: LTE_RRC_CSG_ASF_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: INITIAL

===========================================================================*/

/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_csg_asf_sm_service_ind

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CSG_ASF_SM,
    state INITIAL,
    upon receiving input LTE_RRC_SERVICE_IND

    @detail
    Called upon receipt of input LTE_RRC_SERVICE_IND, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_csg_asf_sm_service_ind
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_csg_asf_sm_service_ind() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_csg_asf_sm_system_update_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CSG_ASF_SM,
    state INITIAL,
    upon receiving input LTE_RRC_SYSTEM_UPDATE_REQ

    @detail
    Called upon receipt of input LTE_RRC_SYSTEM_UPDATE_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_csg_asf_sm_system_update_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_csg_asf_sm_system_update_req() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_csg_asf_sm_irat_asf_needed_ind

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CSG_ASF_SM,
    state INITIAL,
    upon receiving input LTE_CPHY_IRAT_ASF_NEEDED_IND

    @detail
    Called upon receipt of input LTE_CPHY_IRAT_ASF_NEEDED_IND, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_csg_asf_sm_irat_asf_needed_ind
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_csg_asf_sm_irat_asf_needed_ind() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_csg_asf_sm_plmn_search_cnfi

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CSG_ASF_SM,
    state INITIAL,
    upon receiving input LTE_RRC_PLMN_SEARCH_CNFI

    @detail
    Called upon receipt of input LTE_RRC_PLMN_SEARCH_CNFI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_csg_asf_sm_plmn_search_cnfi
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_csg_asf_sm_plmn_search_cnfi() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_csg_asf_sm_timer_expiry_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CSG_ASF_SM,
    state INITIAL,
    upon receiving input LTE_RRC_CSG_ASF_TMRI

    @detail
    Called upon receipt of input LTE_RRC_CSG_ASF_TMRI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_csg_asf_sm_timer_expiry_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_csg_asf_sm_timer_expiry_handler() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_csg_asf_sm_prox_cfg_indi

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CSG_ASF_SM,
    state INITIAL,
    upon receiving input LTE_RRC_PROX_CFG_INDI

    @detail
    Called upon receipt of input LTE_RRC_PROX_CFG_INDI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_csg_asf_sm_prox_cfg_indi
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_csg_asf_sm_prox_cfg_indi() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_csg_asf_sm_cgi_success_indi

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CSG_ASF_SM,
    state INITIAL,
    upon receiving input LTE_RRC_CGI_SUCCESS_INDI

    @detail
    Called upon receipt of input LTE_RRC_CGI_SUCCESS_INDI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_csg_asf_sm_cgi_success_indi
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_csg_asf_sm_cgi_success_indi() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_csg_asf_sm_sib_updated_indi

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CSG_ASF_SM,
    state INITIAL,
    upon receiving input LTE_RRC_SIB_UPDATED_INDI

    @detail
    Called upon receipt of input LTE_RRC_SIB_UPDATED_INDI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_csg_asf_sm_sib_updated_indi
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_csg_asf_sm_sib_updated_indi() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_csg_asf_sm_idle_cell_change_start

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CSG_ASF_SM,
    state INITIAL,
    upon receiving input LTE_RRC_CELL_RESEL_STARTED_INDI

    @detail
    Called upon receipt of input LTE_RRC_CELL_RESEL_STARTED_INDI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_csg_asf_sm_idle_cell_change_start
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_csg_asf_sm_idle_cell_change_start() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_csg_asf_sm_connected_cell_change_started

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CSG_ASF_SM,
    state INITIAL,
    upon receiving input LTE_RRC_HANDOVER_STARTED_INDI

    @detail
    Called upon receipt of input LTE_RRC_HANDOVER_STARTED_INDI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_csg_asf_sm_connected_cell_change_started
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_csg_asf_sm_connected_cell_change_started() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_csg_asf_sm_stopped_indi

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CSG_ASF_SM,
    state INITIAL,
    upon receiving input LTE_RRC_STOPPED_INDI

    @detail
    Called upon receipt of input LTE_RRC_STOPPED_INDI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_csg_asf_sm_stopped_indi
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_csg_asf_sm_stopped_indi() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_csg_asf_sm_deactivate_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CSG_ASF_SM,
    state INITIAL,
    upon receiving input LTE_RRC_DEACTIVATE_REQ

    @detail
    Called upon receipt of input LTE_RRC_DEACTIVATE_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_csg_asf_sm_deactivate_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_csg_asf_sm_deactivate_req() */


/*===========================================================================

     (State Machine: LTE_RRC_CSG_ASF_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: WT_CSG_SEARCH_CNF

===========================================================================*/

/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_csg_asf_sm_search_cnfi

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CSG_ASF_SM,
    state WT_CSG_SEARCH_CNF,
    upon receiving input LTE_RRC_CSG_ASF_SEARCH_CNFI

    @detail
    Called upon receipt of input LTE_RRC_CSG_ASF_SEARCH_CNFI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_csg_asf_sm_search_cnfi
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_csg_asf_sm_search_cnfi() */


/*===========================================================================

     (State Machine: LTE_RRC_CSG_ASF_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: WT_CELL_CHANGE

===========================================================================*/

/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_csg_asf_sm_idle_cell_change_success

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CSG_ASF_SM,
    state WT_CELL_CHANGE,
    upon receiving input LTE_RRC_CAMPED_INDI

    @detail
    Called upon receipt of input LTE_RRC_CAMPED_INDI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_csg_asf_sm_idle_cell_change_success
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_csg_asf_sm_idle_cell_change_success() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_csg_asf_sm_idle_cell_change_failure

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CSG_ASF_SM,
    state WT_CELL_CHANGE,
    upon receiving input LTE_RRC_IRAT_FROM_LTE_RESEL_FAILED_INDI

    @detail
    Called upon receipt of input LTE_RRC_IRAT_FROM_LTE_RESEL_FAILED_INDI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_csg_asf_sm_idle_cell_change_failure
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_csg_asf_sm_idle_cell_change_failure() */


/*===========================================================================

     (State Machine: LTE_RRC_CSG_ASF_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: WT_CONNECTED_CELL_CHANGE

===========================================================================*/

/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_csg_asf_sm_connected_cell_change_success

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CSG_ASF_SM,
    state WT_CONNECTED_CELL_CHANGE,
    upon receiving input LTE_RRC_SERVICE_IND

    @detail
    Called upon receipt of input LTE_RRC_SERVICE_IND, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_csg_asf_sm_connected_cell_change_success
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_csg_asf_sm_connected_cell_change_success() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_csg_asf_sm_connected_cell_change_failure

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CSG_ASF_SM,
    state WT_CONNECTED_CELL_CHANGE,
    upon receiving input LTE_RRC_CONN_REL_STARTED_INDI

    @detail
    Called upon receipt of input LTE_RRC_CONN_REL_STARTED_INDI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_csg_asf_sm_connected_cell_change_failure
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_csg_asf_sm_connected_cell_change_failure() */




