/*=============================================================================

    __lte_rrc_plmn_search_int.h

Description:
  This file contains the machine generated header file for the state machine
  specified in the file:
  lte_rrc_plmn_search.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


#ifndef __LTE_RRC_PLMN_SEARCH_INT_H
#define __LTE_RRC_PLMN_SEARCH_INT_H

#ifdef __cplusplus
/* If compiled into a C++ file, ensure symbols names are not mangled */
extern "C"
{
#endif

/* Include external state machine header */
#include "__lte_rrc_plmn_search.h"

/* Begin machine generated internal header for state machine array: LTE_RRC_PLMN_SEARCH_SM[] */

/* Suppress Lint suggestions to const-ify state machine and payload ptrs */
/*lint -esym(818,sm,payload) */

/* Define a macro for the number of SM instances */
#define LTE_RRC_PLMN_SEARCH_SM_NUM_INSTANCES 1

/* Define a macro for the number of SM states */
#define LTE_RRC_PLMN_SEARCH_SM_NUM_STATES 8

/* Define a macro for the number of SM inputs */
#define LTE_RRC_PLMN_SEARCH_SM_NUM_INPUTS 36

#ifndef STM_DATA_STRUCTURES_ONLY
/* State Machine entry/exit function prototypes */
void lte_rrc_plmn_search_sm_entry(stm_state_machine_t *sm,void *payload);


/* State entry/exit function prototypes */
void lte_rrc_plmn_search_sm_wt_sib1s_and_cell_select_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_plmn_search_sm_wt_search_stop_cnf_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_plmn_search_sm_wt_time_avail_ind_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_plmn_search_sm_wt_irat_search_cnf_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_plmn_search_sm_wt_irat_search_abort_cnf_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_plmn_search_sm_wt_irat_search_abort_cnf_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);


/* Transition function prototypes */
stm_state_t lte_rrc_plmn_search_sm_search_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_plmn_search_sm_csp_cell_select_abort_cnfi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_plmn_search_sm_stopped_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_plmn_search_sm_system_update_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_plmn_search_sm_ml1_revoke_cnfi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_plmn_search_sm_irat_to_lte_search_reqi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_plmn_search_sm_irat_to_lte_search_abort_reqi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_plmn_search_sm_irat_to_lte_search_suspend_reqi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_plmn_search_sm_initial_irat_to_lte_get_plmn_prtl_results_reqi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_plmn_search_sm_mode_change_cnfi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_plmn_search_sm_search_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_plmn_search_sm_search_abort_reqi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_plmn_search_sm_guard_tmri(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_plmn_search_sm_firstp_tmri(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_plmn_search_sm_morep_tmri(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_plmn_search_sm_irat_to_lte_get_plmn_prtl_results_reqi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_plmn_search_sm_bplmn_cell_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_plmn_search_sm_unsolicited_sib_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_plmn_search_sm_bplmn_complete_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_plmn_search_sm_cphy_abort_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_plmn_search_sm_irat_from_lte_search_cnfi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_plmn_search_sm_bplmn_stop_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_plmn_search_sm_bplmn_suspend_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_plmn_search_sm_bplmn_time_avail_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_plmn_search_sm_irat_cphy_abort_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_plmn_search_sm_ml1_revoke_reqi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_plmn_search_sm_irat_from_lte_search_abort_cnfi(stm_state_machine_t *sm, void *payload);


/* State enumeration */
enum
{
  INITIAL,
  WT_MODE_CHANGE_CNF,
  WT_SIB1S_AND_CELL_SELECT,
  WT_SEARCH_STOP_CNF,
  WT_SEARCH_SUSPEND_CNF,
  WT_TIME_AVAIL_IND,
  WT_IRAT_SEARCH_CNF,
  WT_IRAT_SEARCH_ABORT_CNF,
};

#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated internal header for state machine array: LTE_RRC_PLMN_SEARCH_SM[] */


#ifdef __cplusplus
} /* extern "C" {...} */
#endif

#endif /* ! __LTE_RRC_PLMN_SEARCH_INT_H */
