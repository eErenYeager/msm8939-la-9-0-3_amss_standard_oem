/*=============================================================================

    __lte_rrc_misc_int.h

Description:
  This file contains the machine generated header file for the state machine
  specified in the file:
  lte_rrc_misc.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


#ifndef __LTE_RRC_MISC_INT_H
#define __LTE_RRC_MISC_INT_H

#ifdef __cplusplus
/* If compiled into a C++ file, ensure symbols names are not mangled */
extern "C"
{
#endif

/* Include external state machine header */
#include "__lte_rrc_misc.h"

/* Begin machine generated internal header for state machine array: LTE_RRC_MISC_SM[] */

/* Suppress Lint suggestions to const-ify state machine and payload ptrs */
/*lint -esym(818,sm,payload) */

/* Define a macro for the number of SM instances */
#define LTE_RRC_MISC_SM_NUM_INSTANCES 1

/* Define a macro for the number of SM states */
#define LTE_RRC_MISC_SM_NUM_STATES 1

/* Define a macro for the number of SM inputs */
#define LTE_RRC_MISC_SM_NUM_INPUTS 12

#ifndef STM_DATA_STRUCTURES_ONLY
/* State Machine entry/exit function prototypes */
void lte_rrc_misc_sm_entry(stm_state_machine_t *sm,void *payload);


/* State entry/exit function prototypes */


/* Transition function prototypes */
stm_state_t lte_rrc_misc_sm_handle_procedure_start(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_misc_sm_handle_procedure_stop(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_sm_process_procedure_status_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_misc_sm_csfb_call_status_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_misc_sm_handle_ue_mode_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_misc_sm_handle_trm_high_priority_tmri(stm_state_machine_t *sm, void *payload);


/* State enumeration */
enum
{
  INITIAL,
};

#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated internal header for state machine array: LTE_RRC_MISC_SM[] */


#ifdef __cplusplus
} /* extern "C" {...} */
#endif

#endif /* ! __LTE_RRC_MISC_INT_H */
