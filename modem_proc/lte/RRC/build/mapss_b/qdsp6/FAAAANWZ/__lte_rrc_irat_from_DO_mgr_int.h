/*=============================================================================

    __lte_rrc_irat_from_DO_mgr_int.h

Description:
  This file contains the machine generated header file for the state machine
  specified in the file:
  lte_rrc_irat_from_DO_mgr.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


#ifndef __LTE_RRC_IRAT_FROM_DO_MGR_INT_H
#define __LTE_RRC_IRAT_FROM_DO_MGR_INT_H

#ifdef __cplusplus
/* If compiled into a C++ file, ensure symbols names are not mangled */
extern "C"
{
#endif

/* Include external state machine header */
#include "__lte_rrc_irat_from_DO_mgr.h"

/* Begin machine generated internal header for state machine array: LTE_RRC_IRAT_FROM_DO_MGR_SM[] */

/* Suppress Lint suggestions to const-ify state machine and payload ptrs */
/*lint -esym(818,sm,payload) */

/* Define a macro for the number of SM instances */
#define LTE_RRC_IRAT_FROM_DO_MGR_SM_NUM_INSTANCES 1

/* Define a macro for the number of SM states */
#define LTE_RRC_IRAT_FROM_DO_MGR_SM_NUM_STATES 7

/* Define a macro for the number of SM inputs */
#define LTE_RRC_IRAT_FROM_DO_MGR_SM_NUM_INPUTS 7

#ifndef STM_DATA_STRUCTURES_ONLY
/* State Machine entry/exit function prototypes */
void lte_rrc_irat_from_DO_sm_entry(stm_state_machine_t *sm,void *payload);
void lte_rrc_irat_from_DO_sm_exit(stm_state_machine_t *sm,void *payload);


/* State entry/exit function prototypes */
void lte_rrc_irat_from_DO_sm_initial_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_irat_from_DO_sm_initial_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_irat_from_DO_sm_wt_for_activate_cnf_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_irat_from_DO_sm_wt_for_activate_cnf_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_irat_from_DO_sm_wt_to_abort_during_activate_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_irat_from_DO_sm_wt_to_abort_during_activate_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_irat_from_DO_sm_wt_for_deactivate_cnf_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_irat_from_DO_sm_wt_for_deactivate_cnf_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_irat_from_DO_sm_wt_for_lte_to_complete_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_irat_from_DO_sm_wt_for_lte_to_complete_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_irat_from_DO_sm_wt_for_lte_rrc_abort_cnf_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_irat_from_DO_sm_wt_for_lte_rrc_abort_cnf_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_irat_from_DO_sm_wt_for_activation_rsp_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_irat_from_DO_sm_wt_for_activation_rsp_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);


/* Transition function prototypes */
stm_state_t lte_rrc_irat_from_DO_sm_resel_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_irat_from_DO_sm_stopped_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_irat_from_DO_sm_abort_resel_during_initial(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_irat_from_DO_sm_activate_cnfi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_irat_from_DO_sm_handle_abort_resel_req_during_activate(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_irat_from_DO_sm_start_abort_proc_after_activation(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_irat_from_DO_sm_deactivate_cnfi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_irat_from_DO_sm_handle_abort_resel_req_during_deactivate(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_irat_from_DO_sm_handle_abort_resel_req_during_lte_acq(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_irat_from_DO_sm_resel_completed_cnfi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_irat_from_DO_sm_resel_abort_cnfi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_irat_from_DO_sm_process_activation_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_irat_from_DO_sm_handle_abort_resel_req_while_wt_for_act_rsp(stm_state_machine_t *sm, void *payload);


/* State enumeration */
enum
{
  INITIAL,
  WT_FOR_ACTIVATE_CNF,
  WT_TO_ABORT_DURING_ACTIVATE,
  WT_FOR_DEACTIVATE_CNF,
  WT_FOR_LTE_TO_COMPLETE,
  WT_FOR_LTE_RRC_ABORT_CNF,
  WT_FOR_ACTIVATION_RSP,
};

#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated internal header for state machine array: LTE_RRC_IRAT_FROM_DO_MGR_SM[] */


#ifdef __cplusplus
} /* extern "C" {...} */
#endif

#endif /* ! __LTE_RRC_IRAT_FROM_DO_MGR_INT_H */
