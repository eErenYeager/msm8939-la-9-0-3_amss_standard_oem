/*=============================================================================

    __lte_rrc_meas_int.h

Description:
  This file contains the machine generated header file for the state machine
  specified in the file:
  lte_rrc_meas.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


#ifndef __LTE_RRC_MEAS_INT_H
#define __LTE_RRC_MEAS_INT_H

#ifdef __cplusplus
/* If compiled into a C++ file, ensure symbols names are not mangled */
extern "C"
{
#endif

/* Include external state machine header */
#include "__lte_rrc_meas.h"

/* Begin machine generated internal header for state machine array: LTE_RRC_MEAS_SM[] */

/* Suppress Lint suggestions to const-ify state machine and payload ptrs */
/*lint -esym(818,sm,payload) */

/* Define a macro for the number of SM instances */
#define LTE_RRC_MEAS_SM_NUM_INSTANCES 1

/* Define a macro for the number of SM states */
#define LTE_RRC_MEAS_SM_NUM_STATES 5

/* Define a macro for the number of SM inputs */
#define LTE_RRC_MEAS_SM_NUM_INPUTS 43

#ifndef STM_DATA_STRUCTURES_ONLY
/* State Machine entry/exit function prototypes */
void lte_rrc_meas_sm_entry(stm_state_machine_t *sm,void *payload);


/* State entry/exit function prototypes */
void lte_rrc_meas_sm_idle_config_cnf_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_meas_sm_connected_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);


/* Transition function prototypes */
stm_state_t lte_rrc_meas_sm_camped_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_meas_ded_priority_list_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_meas_sm_t320_tmri(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_meas_sm_resel_wait_time_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_meas_sm_tds_resel_wait_time_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_meas_sm_irat_w_resel_tmri(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_meas_sm_irat_tds_resel_tmri(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_meas_sm_nmr_info_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_meas_sm_dedicated_priority_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_meas_sm_clear_dedicated_priority_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_meas_sm_connected_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_meas_sm_t325_tmri(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_meas_sm_cphy_nmr_info_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_meas_sm_clear_depri_freq_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_meas_sm_not_camped_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_meas_sm_sib_updated_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_meas_sm_rat_priority_update_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_meas_sm_csg_cfg_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_meas_sm_stopped_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_meas_sm_depri_freq_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_meas_sm_resel_prio_change_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_meas_sm_sys_update_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_meas_sm_idle_meas_cfg_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_meas_sm_pend_sib_updated_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_meas_sm_meas_cfg_reqi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_meas_sm_report_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_meas_sm_sib_updated_indi_connected(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_meas_sm_interfreq_rstd_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_meas_sm_conn_released_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_meas_sm_unsolicited_sib_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_meas_sm_t321_tmri(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_meas_sm_abort_irat_cgi_handler(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_meas_sm_handle_serv_cell_meas_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_meas_sm_cre_started_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_meas_sm_irat_cgi_start_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_meas_sm_w_get_cgi_cnfi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_meas_sm_g_get_cgi_cnfi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_meas_sm_do_get_cgi_cnfi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_meas_sm_1x_get_cgi_cnfi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_meas_sm_abort_cgi_cnfi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_meas_sm_sec_active_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_meas_sm_conn_meas_cfg_cnf(stm_state_machine_t *sm, void *payload);


/* State enumeration */
enum
{
  INACTIVE,
  IDLE_CAMPED,
  WAIT_FOR_IDLE_CONFIG_CNF,
  CONNECTED,
  WAIT_FOR_CONNECTED_CONFIG_CNF,
};

#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated internal header for state machine array: LTE_RRC_MEAS_SM[] */


#ifdef __cplusplus
} /* extern "C" {...} */
#endif

#endif /* ! __LTE_RRC_MEAS_INT_H */
