/*=============================================================================

    __lte_rrc_emp_int.h

Description:
  This file contains the machine generated header file for the state machine
  specified in the file:
  lte_rrc_emp.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


#ifndef __LTE_RRC_EMP_INT_H
#define __LTE_RRC_EMP_INT_H

#ifdef __cplusplus
/* If compiled into a C++ file, ensure symbols names are not mangled */
extern "C"
{
#endif

/* Include external state machine header */
#include "__lte_rrc_emp.h"

/* Begin machine generated internal header for state machine array: LTE_RRC_EMP_SM[] */

/* Suppress Lint suggestions to const-ify state machine and payload ptrs */
/*lint -esym(818,sm,payload) */

/* Define a macro for the number of SM instances */
#define LTE_RRC_EMP_SM_NUM_INSTANCES 1

/* Define a macro for the number of SM states */
#define LTE_RRC_EMP_SM_NUM_STATES 3

/* Define a macro for the number of SM inputs */
#define LTE_RRC_EMP_SM_NUM_INPUTS 21

#ifndef STM_DATA_STRUCTURES_ONLY
/* State Machine entry/exit function prototypes */
void lte_rrc_emp_sm_entry(stm_state_machine_t *sm,void *payload);


/* State entry/exit function prototypes */


/* Transition function prototypes */
stm_state_t lte_rrc_emp_sm_act_tmgi_reqi_init(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_emp_sm_deact_tmgi_reqi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_emp_sm_avail_tmgi_list_reqi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_emp_sm_stopped_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_emp_sm_rlcdl_mcch_pdu_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_emp_sm_mcch_change_notification(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_emp_sm_act_tmgi_reqi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_emp_sm_sib_updated_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_emp_sm_cfg_cnfi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_emp_sm_mcch_wt_tmri(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_emp_sm_service_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_emp_sm_oos_tmri(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_emp_sm_act_deact_tmgi_reqi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_emp_sm_act_tmgi_reqi_pend(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_emp_sm_deact_tmgi_reqi_pend(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_emp_sm_sib_updated_indi_pend(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_emp_sm_rlcdl_mcch_pdu_ind_pend(stm_state_machine_t *sm, void *payload);


/* State enumeration */
enum
{
  INACTIVE,
  ACTIVE,
  WAIT_FOR_CNF,
};

#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated internal header for state machine array: LTE_RRC_EMP_SM[] */


#ifdef __cplusplus
} /* extern "C" {...} */
#endif

#endif /* ! __LTE_RRC_EMP_INT_H */
