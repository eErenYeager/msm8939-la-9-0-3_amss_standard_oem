/*!
  @file
  __lte_rrc_mh.stub

  @brief
  This module contains the entry, exit, and transition functions
  necessary to implement the following state machines:

  @detail
  LTE_RRC_MH_SM ( 1 instance/s )


  OPTIONAL further detailed description of state machines
  - DELETE this section if unused.

*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
===========================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

/* Include STM external API */
#include <stm2.h>

//! @todo Include necessary files here


/*===========================================================================

         STM COMPILER GENERATED PROTOTYPES AND DATA STRUCTURES

===========================================================================*/

/* Include STM compiler generated internal data structure file */
#include "__lte_rrc_mh_int.h"

/*===========================================================================

                         LOCAL VARIABLES

===========================================================================*/


/*! @brief Structure for state-machine per-instance local variables
*/
typedef struct
{
  int   internal_var;  /*!< My internal variable */
  void *internal_ptr;  /*!< My internal pointer */
  //! @todo SM per-instance variables go here
} __lte_rrc_mh_type;


/*! @brief Variables internal to module __lte_rrc_mh.stub
*/
STATIC __lte_rrc_mh_type __lte_rrc_mh;



/*===========================================================================

                 STATE MACHINE: LTE_RRC_MH_SM

===========================================================================*/

/*===========================================================================

  STATE MACHINE ENTRY FUNCTION:  lte_rrc_mh_sm_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_RRC_MH_SM

    @detail
    Called upon activation of this state machine, with optional
    user-passed payload pointer parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_rrc_mh_sm_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_rrc_mh_sm_entry() */


/*===========================================================================

     (State Machine: LTE_RRC_MH_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: INITIAL

===========================================================================*/

/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_mh_sm_send_ul_msg_reqi

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_MH_SM,
    state INITIAL,
    upon receiving input LTE_RRC_SEND_UL_MSG_REQI

    @detail
    Called upon receipt of input LTE_RRC_SEND_UL_MSG_REQI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_mh_sm_send_ul_msg_reqi
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_mh_sm_send_ul_msg_reqi() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_mh_sm_go_to_ho

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_MH_SM,
    state INITIAL,
    upon receiving input LTE_RRC_HANDOVER_STARTED_INDI

    @detail
    Called upon receipt of input LTE_RRC_HANDOVER_STARTED_INDI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_mh_sm_go_to_ho
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_mh_sm_go_to_ho() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_mh_sm_go_to_initial

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_MH_SM,
    state INITIAL,
    upon receiving input LTE_RRC_CONN_RELEASED_INDI

    @detail
    Called upon receipt of input LTE_RRC_CONN_RELEASED_INDI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_mh_sm_go_to_initial
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_mh_sm_go_to_initial() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_mh_sm_dlm_processed_indi

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_MH_SM,
    state INITIAL,
    upon receiving input LTE_RRC_DLM_PROCESSED_INDI

    @detail
    Called upon receipt of input LTE_RRC_DLM_PROCESSED_INDI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_mh_sm_dlm_processed_indi
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_mh_sm_dlm_processed_indi() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_mh_sm_camped_indi

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_MH_SM,
    state INITIAL,
    upon receiving input LTE_RRC_CAMPED_INDI

    @detail
    Called upon receipt of input LTE_RRC_CAMPED_INDI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_mh_sm_camped_indi
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_mh_sm_camped_indi() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_mh_sm_ps_ho_started_indi

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_MH_SM,
    state INITIAL,
    upon receiving input LTE_RRC_IRAT_TO_LTE_PSHO_STARTED_INDI

    @detail
    Called upon receipt of input LTE_RRC_IRAT_TO_LTE_PSHO_STARTED_INDI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_mh_sm_ps_ho_started_indi
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_mh_sm_ps_ho_started_indi() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_mh_sm_dl_data_ind

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_MH_SM,
    state INITIAL,
    upon receiving input LTE_PDCPDL_SDU_IND

    @detail
    Called upon receipt of input LTE_PDCPDL_SDU_IND, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_mh_sm_dl_data_ind
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_mh_sm_dl_data_ind() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_mh_sm_pdcpul_sdu_cnf

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_MH_SM,
    state INITIAL,
    upon receiving input LTE_PDCPUL_SDU_CNF

    @detail
    Called upon receipt of input LTE_PDCPUL_SDU_CNF, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_mh_sm_pdcpul_sdu_cnf
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_mh_sm_pdcpul_sdu_cnf() */


/*===========================================================================

     (State Machine: LTE_RRC_MH_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: HANDOVER

===========================================================================*/



