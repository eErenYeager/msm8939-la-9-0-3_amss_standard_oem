/*=============================================================================

    __lte_rrc_cep_int.h

Description:
  This file contains the machine generated header file for the state machine
  specified in the file:
  lte_rrc_cep.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


#ifndef __LTE_RRC_CEP_INT_H
#define __LTE_RRC_CEP_INT_H

#ifdef __cplusplus
/* If compiled into a C++ file, ensure symbols names are not mangled */
extern "C"
{
#endif

/* Include external state machine header */
#include "__lte_rrc_cep.h"

/* Begin machine generated internal header for state machine array: LTE_RRC_CEP_SM[] */

/* Suppress Lint suggestions to const-ify state machine and payload ptrs */
/*lint -esym(818,sm,payload) */

/* Define a macro for the number of SM instances */
#define LTE_RRC_CEP_SM_NUM_INSTANCES 1

/* Define a macro for the number of SM states */
#define LTE_RRC_CEP_SM_NUM_STATES 5

/* Define a macro for the number of SM inputs */
#define LTE_RRC_CEP_SM_NUM_INPUTS 27

#ifndef STM_DATA_STRUCTURES_ONLY
/* State Machine entry/exit function prototypes */
void lte_rrc_cep_entry(stm_state_machine_t *sm,void *payload);


/* State entry/exit function prototypes */
void lte_rrc_cep_sm_inactive_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_cep_sm_pending_retry_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_cep_sm_pending_retry_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_cep_sm_access_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_cep_sm_access_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_cep_sm_csrb1_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_cep_sm_csrb1_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_cep_sm_connected_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_cep_sm_connected_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);


/* Transition function prototypes */
stm_state_t lte_rrc_cep_sm_conn_est_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_cep_sm_t302_tmri(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_cep_sm_t303_tmri(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_cep_sm_t305_tmri(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_cep_sm_stopped_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_cep_sm_camped_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_cep_sm_conn_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_cep_sm_irat_to_lte_psho_success_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_cep_sm_rf_available_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_cep_sm_rf_unavailable_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_cep_sm_cell_resel_started_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_cep_sm_cell_resel_canceled_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_cep_sm_not_camped_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_cep_sm_est_stopped_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_cep_sm_resel_tmri(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_cep_sm_connection_reject_dlm(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_cep_sm_access_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_cep_sm_random_access_problem_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_cep_ml1_revoke_cnfi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_cep_sm_new_cell_info_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_cep_sm_rf_unavailable_tmri(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_cep_sm_acb_delay_rach_tmri(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_cep_sm_connection_setup_dlm(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_cep_sm_t300_tmri(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_cep_sm_mac_access_abort_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_cep_sm_cfg_cnfi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_cep_sm_conn_released_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_cep_sm_connection_setup_complete_cnfi(stm_state_machine_t *sm, void *payload);


/* State enumeration */
enum
{
  INACTIVE,
  PENDING_RETRY,
  ACCESS,
  CONFIGURING_SRB1,
  CONNECTED,
};

#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated internal header for state machine array: LTE_RRC_CEP_SM[] */


#ifdef __cplusplus
} /* extern "C" {...} */
#endif

#endif /* ! __LTE_RRC_CEP_INT_H */
