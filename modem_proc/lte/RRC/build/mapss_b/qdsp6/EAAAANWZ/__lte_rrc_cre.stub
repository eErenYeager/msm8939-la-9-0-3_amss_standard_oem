/*!
  @file
  __lte_rrc_cre.stub

  @brief
  This module contains the entry, exit, and transition functions
  necessary to implement the following state machines:

  @detail
  LTE_RRC_CRE_SM ( 1 instance/s )


  OPTIONAL further detailed description of state machines
  - DELETE this section if unused.

*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
===========================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

/* Include STM external API */
#include <stm2.h>

//! @todo Include necessary files here


/*===========================================================================

         STM COMPILER GENERATED PROTOTYPES AND DATA STRUCTURES

===========================================================================*/

/* Include STM compiler generated internal data structure file */
#include "__lte_rrc_cre_int.h"

/*===========================================================================

                         LOCAL VARIABLES

===========================================================================*/


/*! @brief Structure for state-machine per-instance local variables
*/
typedef struct
{
  int   internal_var;  /*!< My internal variable */
  void *internal_ptr;  /*!< My internal pointer */
  //! @todo SM per-instance variables go here
} __lte_rrc_cre_type;


/*! @brief Variables internal to module __lte_rrc_cre.stub
*/
STATIC __lte_rrc_cre_type __lte_rrc_cre;



/*===========================================================================

                 STATE MACHINE: LTE_RRC_CRE_SM

===========================================================================*/

/*===========================================================================

  STATE MACHINE ENTRY FUNCTION:  lte_rrc_cre_sm_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_RRC_CRE_SM

    @detail
    Called upon activation of this state machine, with optional
    user-passed payload pointer parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_rrc_cre_sm_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_rrc_cre_sm_entry() */


/*===========================================================================

     (State Machine: LTE_RRC_CRE_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: INITIAL

===========================================================================*/

/*===========================================================================

  STATE ENTRY FUNCTION:  lte_rrc_cre_initial_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_RRC_CRE_SM,
    state INITIAL

    @detail
    Called upon entry into this state of the state machine, with optional
    user-passed payload pointer parameter.  The prior state of the state
    machine is also passed as the prev_state parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_rrc_cre_initial_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         prev_state,  /*!< Previous state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( prev_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_rrc_cre_initial_entry() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_cre_trigger_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CRE_SM,
    state INITIAL,
    upon receiving input LTE_CPHY_RL_FAILURE_IND

    @detail
    Called upon receipt of input LTE_CPHY_RL_FAILURE_IND, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_cre_trigger_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_cre_trigger_handler() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_cre_conn_est_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CRE_SM,
    state INITIAL,
    upon receiving input LTE_RRC_CONN_ESTABLISHMENT_STARTED_INDI

    @detail
    Called upon receipt of input LTE_RRC_CONN_ESTABLISHMENT_STARTED_INDI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_cre_conn_est_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_cre_conn_est_handler() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_cre_idle_transition_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CRE_SM,
    state INITIAL,
    upon receiving input LTE_RRC_STOPPED_INDI

    @detail
    Called upon receipt of input LTE_RRC_STOPPED_INDI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_cre_idle_transition_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_cre_idle_transition_handler() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_cre_sm_deactivate_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CRE_SM,
    state INITIAL,
    upon receiving input LTE_RRC_DEACTIVATE_REQ

    @detail
    Called upon receipt of input LTE_RRC_DEACTIVATE_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_cre_sm_deactivate_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_cre_sm_deactivate_req() */


/*===========================================================================

     (State Machine: LTE_RRC_CRE_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: WAIT_FOR_CFG_CNFI

===========================================================================*/

/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_cre_cfg_cnfi_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CRE_SM,
    state WAIT_FOR_CFG_CNFI,
    upon receiving input LTE_RRC_CFG_CNFI

    @detail
    Called upon receipt of input LTE_RRC_CFG_CNFI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_cre_cfg_cnfi_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_cre_cfg_cnfi_handler() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_cre_mac_access_cnf_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CRE_SM,
    state WAIT_FOR_CFG_CNFI,
    upon receiving input LTE_MAC_ACCESS_CNF

    @detail
    Called upon receipt of input LTE_MAC_ACCESS_CNF, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_cre_mac_access_cnf_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_cre_mac_access_cnf_handler() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_cre_pend_rl_failure_ind

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CRE_SM,
    state WAIT_FOR_CFG_CNFI,
    upon receiving input LTE_CPHY_RL_FAILURE_IND

    @detail
    Called upon receipt of input LTE_CPHY_RL_FAILURE_IND, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_cre_pend_rl_failure_ind
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_cre_pend_rl_failure_ind() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_cre_pend_cmd

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CRE_SM,
    state WAIT_FOR_CFG_CNFI,
    upon receiving input LTE_FC_SHUTDOWN_IND

    @detail
    Called upon receipt of input LTE_FC_SHUTDOWN_IND, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_cre_pend_cmd
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_cre_pend_cmd() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_cre_abort_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CRE_SM,
    state WAIT_FOR_CFG_CNFI,
    upon receiving input LTE_CPHY_OOS_IND

    @detail
    Called upon receipt of input LTE_CPHY_OOS_IND, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_cre_abort_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_cre_abort_handler() */


/*===========================================================================

     (State Machine: LTE_RRC_CRE_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: WAIT_FOR_UE_TO_CAMP

===========================================================================*/

/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_cre_camped_indi_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CRE_SM,
    state WAIT_FOR_UE_TO_CAMP,
    upon receiving input LTE_RRC_CAMPED_INDI

    @detail
    Called upon receipt of input LTE_RRC_CAMPED_INDI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_cre_camped_indi_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_cre_camped_indi_handler() */


/*===========================================================================

     (State Machine: LTE_RRC_CRE_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: WAIT_FOR_CONN_RE

===========================================================================*/

/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_cre_dlm_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CRE_SM,
    state WAIT_FOR_CONN_RE,
    upon receiving input LTE_RRC_RRC_CONNECTION_REESTABLISHMENT_DLM

    @detail
    Called upon receipt of input LTE_RRC_RRC_CONNECTION_REESTABLISHMENT_DLM, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_cre_dlm_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_cre_dlm_handler() */




