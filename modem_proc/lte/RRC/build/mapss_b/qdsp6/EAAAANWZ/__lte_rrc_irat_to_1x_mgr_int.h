/*=============================================================================

    __lte_rrc_irat_to_1x_mgr_int.h

Description:
  This file contains the machine generated header file for the state machine
  specified in the file:
  lte_rrc_irat_to_1x_mgr.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


#ifndef __LTE_RRC_IRAT_TO_1X_MGR_INT_H
#define __LTE_RRC_IRAT_TO_1X_MGR_INT_H

#ifdef __cplusplus
/* If compiled into a C++ file, ensure symbols names are not mangled */
extern "C"
{
#endif

/* Include external state machine header */
#include "__lte_rrc_irat_to_1x_mgr.h"

/* Begin machine generated internal header for state machine array: LTE_RRC_IRAT_TO_1X_MGR_SM[] */

/* Suppress Lint suggestions to const-ify state machine and payload ptrs */
/*lint -esym(818,sm,payload) */

/* Define a macro for the number of SM instances */
#define LTE_RRC_IRAT_TO_1X_MGR_SM_NUM_INSTANCES 1

/* Define a macro for the number of SM states */
#define LTE_RRC_IRAT_TO_1X_MGR_SM_NUM_STATES 6

/* Define a macro for the number of SM inputs */
#define LTE_RRC_IRAT_TO_1X_MGR_SM_NUM_INPUTS 19

#ifndef STM_DATA_STRUCTURES_ONLY
/* State Machine entry/exit function prototypes */
void lte_rrc_irat_to_1x_sm_entry(stm_state_machine_t *sm,void *payload);
void lte_rrc_irat_to_1x_sm_exit(stm_state_machine_t *sm,void *payload);


/* State entry/exit function prototypes */
void lte_rrc_irat_to_1x_sm_initial_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_irat_to_1x_sm_wt_for_suspend_cnf_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_irat_to_1x_sm_wt_to_abort_during_suspend_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_irat_to_1x_sm_wt_for_resume_cnf_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_irat_to_1x_sm_irat_to_1x_in_progress_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_rrc_irat_to_1x_sm_wt_for_1x_abort_rsp_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);


/* Transition function prototypes */
stm_state_t lte_rrc_irat_to_1x_sm_redir_reqi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_irat_to_1x_sm_resel_reqi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_irat_to_1x_sm_sib_updated_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_irat_to_1x_sm_ho_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_irat_to_1x_sm_csfb_call_rel_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_irat_to_1x_sm_lte_depri_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_irat_to_1x_sm_get_cgi_reqi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_irat_to_1x_sm_suspended(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_irat_to_1x_sm_abort_during_suspend(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_irat_to_1x_sm_not_camped_indi_suspended(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_irat_to_1x_sm_start_abort(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_irat_to_1x_sm_resumed(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_irat_to_1x_sm_abort_during_resume(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_irat_to_1x_sm_stopped(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_irat_to_1x_sm_redir_failed(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_irat_to_1x_sm_resel_failed(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_irat_to_1x_sm_ho_failed(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_irat_to_1x_sm_abort_irat(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_irat_to_1x_sm_get_cgi_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_irat_to_1x_sm_redir_abort_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_irat_to_1x_sm_resel_abort_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_irat_to_1x_sm_csfb_ho_abort_rsp(stm_state_machine_t *sm, void *payload);


/* State enumeration */
enum
{
  INITIAL,
  WT_FOR_SUSPEND_CNF,
  WT_TO_ABORT_DURING_SUSPEND,
  WT_FOR_RESUME_CNF,
  IRAT_TO_1X_IN_PROGRESS,
  WT_FOR_1X_ABORT_RSP,
};

#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated internal header for state machine array: LTE_RRC_IRAT_TO_1X_MGR_SM[] */


#ifdef __cplusplus
} /* extern "C" {...} */
#endif

#endif /* ! __LTE_RRC_IRAT_TO_1X_MGR_INT_H */
