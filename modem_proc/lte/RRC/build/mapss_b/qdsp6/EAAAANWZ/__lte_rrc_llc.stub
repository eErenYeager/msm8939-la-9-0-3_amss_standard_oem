/*!
  @file
  __lte_rrc_llc.stub

  @brief
  This module contains the entry, exit, and transition functions
  necessary to implement the following state machines:

  @detail
  LTE_RRC_LLC_SM ( 1 instance/s )


  OPTIONAL further detailed description of state machines
  - DELETE this section if unused.

*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
===========================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

/* Include STM external API */
#include <stm2.h>

//! @todo Include necessary files here


/*===========================================================================

         STM COMPILER GENERATED PROTOTYPES AND DATA STRUCTURES

===========================================================================*/

/* Include STM compiler generated internal data structure file */
#include "__lte_rrc_llc_int.h"

/*===========================================================================

                         LOCAL VARIABLES

===========================================================================*/


/*! @brief Structure for state-machine per-instance local variables
*/
typedef struct
{
  int   internal_var;  /*!< My internal variable */
  void *internal_ptr;  /*!< My internal pointer */
  //! @todo SM per-instance variables go here
} __lte_rrc_llc_type;


/*! @brief Variables internal to module __lte_rrc_llc.stub
*/
STATIC __lte_rrc_llc_type __lte_rrc_llc;



/*===========================================================================

                 STATE MACHINE: LTE_RRC_LLC_SM

===========================================================================*/

/*===========================================================================

  STATE MACHINE ENTRY FUNCTION:  lte_rrc_llc_sm_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_RRC_LLC_SM

    @detail
    Called upon activation of this state machine, with optional
    user-passed payload pointer parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_rrc_llc_sm_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_rrc_llc_sm_entry() */


/*===========================================================================

     (State Machine: LTE_RRC_LLC_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: INITIAL

===========================================================================*/

/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_llc_cfg_reqi_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_LLC_SM,
    state INITIAL,
    upon receiving input LTE_RRC_CFG_REQI

    @detail
    Called upon receipt of input LTE_RRC_CFG_REQI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_llc_cfg_reqi_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_llc_cfg_reqi_handler() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_llc_mac_release_resources_ind_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_LLC_SM,
    state INITIAL,
    upon receiving input LTE_MAC_RELEASE_RESOURCES_IND

    @detail
    Called upon receipt of input LTE_MAC_RELEASE_RESOURCES_IND, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_llc_mac_release_resources_ind_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_llc_mac_release_resources_ind_handler() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_llc_ho_completed_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_LLC_SM,
    state INITIAL,
    upon receiving input LTE_RRC_HANDOVER_COMPLETED_IND

    @detail
    Called upon receipt of input LTE_RRC_HANDOVER_COMPLETED_IND, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_llc_ho_completed_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_llc_ho_completed_handler() */


/*===========================================================================

     (State Machine: LTE_RRC_LLC_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: WAIT_FOR_CPHY_CFG_CNF

===========================================================================*/

/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_llc_cphy_common_cfg_cnf_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_LLC_SM,
    state WAIT_FOR_CPHY_CFG_CNF,
    upon receiving input LTE_CPHY_COMMON_CFG_CNF

    @detail
    Called upon receipt of input LTE_CPHY_COMMON_CFG_CNF, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_llc_cphy_common_cfg_cnf_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_llc_cphy_common_cfg_cnf_handler() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_llc_cphy_dedicated_cfg_cnf_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_LLC_SM,
    state WAIT_FOR_CPHY_CFG_CNF,
    upon receiving input LTE_CPHY_DEDICATED_CFG_CNF

    @detail
    Called upon receipt of input LTE_CPHY_DEDICATED_CFG_CNF, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_llc_cphy_dedicated_cfg_cnf_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_llc_cphy_dedicated_cfg_cnf_handler() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_llc_cphy_handover_cnf_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_LLC_SM,
    state WAIT_FOR_CPHY_CFG_CNF,
    upon receiving input LTE_CPHY_HANDOVER_CNF

    @detail
    Called upon receipt of input LTE_CPHY_HANDOVER_CNF, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_llc_cphy_handover_cnf_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_llc_cphy_handover_cnf_handler() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_llc_cphy_con_release_cnf_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_LLC_SM,
    state WAIT_FOR_CPHY_CFG_CNF,
    upon receiving input LTE_CPHY_CON_RELEASE_CNF

    @detail
    Called upon receipt of input LTE_CPHY_CON_RELEASE_CNF, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_llc_cphy_con_release_cnf_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_llc_cphy_con_release_cnf_handler() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_llc_cphy_abort_cnf_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_LLC_SM,
    state WAIT_FOR_CPHY_CFG_CNF,
    upon receiving input LTE_CPHY_ABORT_CNF

    @detail
    Called upon receipt of input LTE_CPHY_ABORT_CNF, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_llc_cphy_abort_cnf_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_llc_cphy_abort_cnf_handler() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_llc_deadlock_tmri_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_LLC_SM,
    state WAIT_FOR_CPHY_CFG_CNF,
    upon receiving input LTE_RRC_DEADLOCK_TMRI

    @detail
    Called upon receipt of input LTE_RRC_DEADLOCK_TMRI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_llc_deadlock_tmri_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_llc_deadlock_tmri_handler() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_llc_pend_cmd_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_LLC_SM,
    state WAIT_FOR_CPHY_CFG_CNF,
    upon receiving input LTE_RRC_CFG_REQI

    @detail
    Called upon receipt of input LTE_RRC_CFG_REQI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_llc_pend_cmd_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_llc_pend_cmd_handler() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_llc_rf_available_ind_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_LLC_SM,
    state WAIT_FOR_CPHY_CFG_CNF,
    upon receiving input LTE_CPHY_RF_AVAILABLE_IND

    @detail
    Called upon receipt of input LTE_CPHY_RF_AVAILABLE_IND, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_llc_rf_available_ind_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_llc_rf_available_ind_handler() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_llc_rf_unavailable_ind_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_LLC_SM,
    state WAIT_FOR_CPHY_CFG_CNF,
    upon receiving input LTE_CPHY_RF_UNAVAILABLE_IND

    @detail
    Called upon receipt of input LTE_CPHY_RF_UNAVAILABLE_IND, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_llc_rf_unavailable_ind_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_llc_rf_unavailable_ind_handler() */


/*===========================================================================

     (State Machine: LTE_RRC_LLC_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: WAIT_FOR_MAC_CFG_CNF

===========================================================================*/

/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_llc_mac_cfg_cnf_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_LLC_SM,
    state WAIT_FOR_MAC_CFG_CNF,
    upon receiving input LTE_MAC_CFG_CNF

    @detail
    Called upon receipt of input LTE_MAC_CFG_CNF, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_llc_mac_cfg_cnf_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_llc_mac_cfg_cnf_handler() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_llc_mac_abort_cnf_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_LLC_SM,
    state WAIT_FOR_MAC_CFG_CNF,
    upon receiving input LTE_MAC_ACCESS_ABORT_CNF

    @detail
    Called upon receipt of input LTE_MAC_ACCESS_ABORT_CNF, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_llc_mac_abort_cnf_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_llc_mac_abort_cnf_handler() */


/*===========================================================================

     (State Machine: LTE_RRC_LLC_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: WAIT_FOR_RLC_CFG_CNF

===========================================================================*/

/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_llc_rlcdl_cfg_cnf_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_LLC_SM,
    state WAIT_FOR_RLC_CFG_CNF,
    upon receiving input LTE_RLCDL_CFG_CNF

    @detail
    Called upon receipt of input LTE_RLCDL_CFG_CNF, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_llc_rlcdl_cfg_cnf_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_llc_rlcdl_cfg_cnf_handler() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_llc_rlcul_cfg_cnf_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_LLC_SM,
    state WAIT_FOR_RLC_CFG_CNF,
    upon receiving input LTE_RLCUL_CFG_CNF

    @detail
    Called upon receipt of input LTE_RLCUL_CFG_CNF, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_llc_rlcul_cfg_cnf_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_llc_rlcul_cfg_cnf_handler() */


/*===========================================================================

     (State Machine: LTE_RRC_LLC_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: WAIT_FOR_PDCP_CFG_CNF

===========================================================================*/

/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_llc_pdcpdl_cfg_cnf_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_LLC_SM,
    state WAIT_FOR_PDCP_CFG_CNF,
    upon receiving input LTE_PDCPDL_CFG_CNF

    @detail
    Called upon receipt of input LTE_PDCPDL_CFG_CNF, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_llc_pdcpdl_cfg_cnf_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_llc_pdcpdl_cfg_cnf_handler() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_llc_pdcpul_cfg_cnf_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_LLC_SM,
    state WAIT_FOR_PDCP_CFG_CNF,
    upon receiving input LTE_PDCPUL_CFG_CNF

    @detail
    Called upon receipt of input LTE_PDCPUL_CFG_CNF, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_llc_pdcpul_cfg_cnf_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_llc_pdcpul_cfg_cnf_handler() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_llc_pdcpul_recfg_prep_cnf_handler

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_LLC_SM,
    state WAIT_FOR_PDCP_CFG_CNF,
    upon receiving input LTE_PDCPUL_RECFG_PREP_CNF

    @detail
    Called upon receipt of input LTE_PDCPUL_RECFG_PREP_CNF, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_llc_pdcpul_recfg_prep_cnf_handler
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_llc_pdcpul_recfg_prep_cnf_handler() */




