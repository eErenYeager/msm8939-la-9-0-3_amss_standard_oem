/*!
  @file
  __lte_rrc_dt.stub

  @brief
  This module contains the entry, exit, and transition functions
  necessary to implement the following state machines:

  @detail
  LTE_RRC_DT_SM ( 1 instance/s )


  OPTIONAL further detailed description of state machines
  - DELETE this section if unused.

*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
===========================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

/* Include STM external API */
#include <stm2.h>

//! @todo Include necessary files here


/*===========================================================================

         STM COMPILER GENERATED PROTOTYPES AND DATA STRUCTURES

===========================================================================*/

/* Include STM compiler generated internal data structure file */
#include "__lte_rrc_dt_int.h"

/*===========================================================================

                         LOCAL VARIABLES

===========================================================================*/


/*! @brief Structure for state-machine per-instance local variables
*/
typedef struct
{
  int   internal_var;  /*!< My internal variable */
  void *internal_ptr;  /*!< My internal pointer */
  //! @todo SM per-instance variables go here
} __lte_rrc_dt_type;


/*! @brief Variables internal to module __lte_rrc_dt.stub
*/
STATIC __lte_rrc_dt_type __lte_rrc_dt;



/*===========================================================================

                 STATE MACHINE: LTE_RRC_DT_SM

===========================================================================*/

/*===========================================================================

  STATE MACHINE ENTRY FUNCTION:  lte_rrc_dt_sm_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_RRC_DT_SM

    @detail
    Called upon activation of this state machine, with optional
    user-passed payload pointer parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_rrc_dt_sm_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_rrc_dt_sm_entry() */


/*===========================================================================

     (State Machine: LTE_RRC_DT_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: READY

===========================================================================*/

/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_dt_sm_dl_information_transfer_dlm

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_DT_SM,
    state READY,
    upon receiving input LTE_RRC_DL_INFORMATION_TRANSFER_DLM

    @detail
    Called upon receipt of input LTE_RRC_DL_INFORMATION_TRANSFER_DLM, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_dt_sm_dl_information_transfer_dlm
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_dt_sm_dl_information_transfer_dlm() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_dt_sm_cdma2000_csfb_parameters_response_dlm

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_DT_SM,
    state READY,
    upon receiving input LTE_RRC_CSFB_PARAMETERS_RESPONSE_CDMA2000_DLM

    @detail
    Called upon receipt of input LTE_RRC_CSFB_PARAMETERS_RESPONSE_CDMA2000_DLM, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_dt_sm_cdma2000_csfb_parameters_response_dlm
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_dt_sm_cdma2000_csfb_parameters_response_dlm() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_dt_sm_ho_from_eutra_prep_req_dlm

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_DT_SM,
    state READY,
    upon receiving input LTE_RRC_HANDOVER_FROM_EUTRA_PREPARATION_REQUEST_DLM

    @detail
    Called upon receipt of input LTE_RRC_HANDOVER_FROM_EUTRA_PREPARATION_REQUEST_DLM, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_dt_sm_ho_from_eutra_prep_req_dlm
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_dt_sm_ho_from_eutra_prep_req_dlm() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_dt_sm_ready_ul_data_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_DT_SM,
    state READY,
    upon receiving input LTE_RRC_UL_DATA_REQ

    @detail
    Called upon receipt of input LTE_RRC_UL_DATA_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_dt_sm_ready_ul_data_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_dt_sm_ready_ul_data_req() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_dt_sm_ready_irat_tunnel_ul_msg_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_DT_SM,
    state READY,
    upon receiving input LTE_RRC_IRAT_TUNNEL_UL_MSG_REQ

    @detail
    Called upon receipt of input LTE_RRC_IRAT_TUNNEL_UL_MSG_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_dt_sm_ready_irat_tunnel_ul_msg_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_dt_sm_ready_irat_tunnel_ul_msg_req() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_dt_sm_ready_irat_hdr_tunnel_ul_msg_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_DT_SM,
    state READY,
    upon receiving input LTE_RRC_IRAT_HDR_UL_TUNNEL_MSG_REQ

    @detail
    Called upon receipt of input LTE_RRC_IRAT_HDR_UL_TUNNEL_MSG_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_dt_sm_ready_irat_hdr_tunnel_ul_msg_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_dt_sm_ready_irat_hdr_tunnel_ul_msg_req() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_dt_sm_ul_information_transfer_cnfi

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_DT_SM,
    state READY,
    upon receiving input LTE_RRC_UL_INFORMATION_TRANSFER_CNFI

    @detail
    Called upon receipt of input LTE_RRC_UL_INFORMATION_TRANSFER_CNFI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_dt_sm_ul_information_transfer_cnfi
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_dt_sm_ul_information_transfer_cnfi() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_dt_sm_handover_rlf_started

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_DT_SM,
    state READY,
    upon receiving input LTE_RRC_HANDOVER_STARTED_INDI

    @detail
    Called upon receipt of input LTE_RRC_HANDOVER_STARTED_INDI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_dt_sm_handover_rlf_started
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_dt_sm_handover_rlf_started() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_dt_sm_conn_released_indi

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_DT_SM,
    state READY,
    upon receiving input LTE_RRC_CONN_RELEASED_INDI

    @detail
    Called upon receipt of input LTE_RRC_CONN_RELEASED_INDI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_dt_sm_conn_released_indi
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_dt_sm_conn_released_indi() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_dt_sm_stopped

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_DT_SM,
    state READY,
    upon receiving input LTE_RRC_STOPPED_INDI

    @detail
    Called upon receipt of input LTE_RRC_STOPPED_INDI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_dt_sm_stopped
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_dt_sm_stopped() */


/*===========================================================================

     (State Machine: LTE_RRC_DT_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: BLOCKED

===========================================================================*/

/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_dt_sm_blocked_ul_data_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_DT_SM,
    state BLOCKED,
    upon receiving input LTE_RRC_UL_DATA_REQ

    @detail
    Called upon receipt of input LTE_RRC_UL_DATA_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_dt_sm_blocked_ul_data_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_dt_sm_blocked_ul_data_req() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_dt_sm_blocked_irat_tunnel_ul_msg_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_DT_SM,
    state BLOCKED,
    upon receiving input LTE_RRC_IRAT_TUNNEL_UL_MSG_REQ

    @detail
    Called upon receipt of input LTE_RRC_IRAT_TUNNEL_UL_MSG_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_dt_sm_blocked_irat_tunnel_ul_msg_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_dt_sm_blocked_irat_tunnel_ul_msg_req() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_dt_sm_blocked_irat_hdr_tunnel_ul_msg_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_DT_SM,
    state BLOCKED,
    upon receiving input LTE_RRC_IRAT_HDR_UL_TUNNEL_MSG_REQ

    @detail
    Called upon receipt of input LTE_RRC_IRAT_HDR_UL_TUNNEL_MSG_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_dt_sm_blocked_irat_hdr_tunnel_ul_msg_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_dt_sm_blocked_irat_hdr_tunnel_ul_msg_req() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_dt_sm_handover_rlf_recovery_success

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_DT_SM,
    state BLOCKED,
    upon receiving input LTE_RRC_SERVICE_IND

    @detail
    Called upon receipt of input LTE_RRC_SERVICE_IND, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_dt_sm_handover_rlf_recovery_success
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_dt_sm_handover_rlf_recovery_success() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_dt_sm_handover_rlf_recovery_failure

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_DT_SM,
    state BLOCKED,
    upon receiving input LTE_RRC_CONN_RELEASED_INDI

    @detail
    Called upon receipt of input LTE_RRC_CONN_RELEASED_INDI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_dt_sm_handover_rlf_recovery_failure
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_dt_sm_handover_rlf_recovery_failure() */




