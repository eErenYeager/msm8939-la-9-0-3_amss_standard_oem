/*!
  @file
  __lte_rrc_capabilities.stub

  @brief
  This module contains the entry, exit, and transition functions
  necessary to implement the following state machines:

  @detail
  LTE_RRC_CAPABILITIES_SM ( 1 instance/s )


  OPTIONAL further detailed description of state machines
  - DELETE this section if unused.

*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
===========================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

/* Include STM external API */
#include <stm2.h>

//! @todo Include necessary files here


/*===========================================================================

         STM COMPILER GENERATED PROTOTYPES AND DATA STRUCTURES

===========================================================================*/

/* Include STM compiler generated internal data structure file */
#include "__lte_rrc_capabilities_int.h"

/*===========================================================================

                         LOCAL VARIABLES

===========================================================================*/


/*! @brief Structure for state-machine per-instance local variables
*/
typedef struct
{
  int   internal_var;  /*!< My internal variable */
  void *internal_ptr;  /*!< My internal pointer */
  //! @todo SM per-instance variables go here
} __lte_rrc_capabilities_type;


/*! @brief Variables internal to module __lte_rrc_capabilities.stub
*/
STATIC __lte_rrc_capabilities_type __lte_rrc_capabilities;



/*===========================================================================

                 STATE MACHINE: LTE_RRC_CAPABILITIES_SM

===========================================================================*/

/*===========================================================================

  STATE MACHINE ENTRY FUNCTION:  lte_rrc_cap_sm_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_RRC_CAPABILITIES_SM

    @detail
    Called upon activation of this state machine, with optional
    user-passed payload pointer parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_rrc_cap_sm_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_rrc_cap_sm_entry() */


/*===========================================================================

     (State Machine: LTE_RRC_CAPABILITIES_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: INITIAL

===========================================================================*/

/*===========================================================================

  STATE ENTRY FUNCTION:  lte_rrc_cap_sm_initial_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_RRC_CAPABILITIES_SM,
    state INITIAL

    @detail
    Called upon entry into this state of the state machine, with optional
    user-passed payload pointer parameter.  The prior state of the state
    machine is also passed as the prev_state parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_rrc_cap_sm_initial_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         prev_state,  /*!< Previous state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( prev_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_rrc_cap_sm_initial_entry() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_cap_sm_stopped_indi

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CAPABILITIES_SM,
    state INITIAL,
    upon receiving input LTE_RRC_STOPPED_INDI

    @detail
    Called upon receipt of input LTE_RRC_STOPPED_INDI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_cap_sm_stopped_indi
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_cap_sm_stopped_indi() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_cap_sm_ue_capability_enquiry_dlm

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CAPABILITIES_SM,
    state INITIAL,
    upon receiving input LTE_RRC_UE_CAPABILITY_ENQUIRY_DLM

    @detail
    Called upon receipt of input LTE_RRC_UE_CAPABILITY_ENQUIRY_DLM, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_cap_sm_ue_capability_enquiry_dlm
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_cap_sm_ue_capability_enquiry_dlm() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_cap_sm_ue_capability_information_cnfi

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CAPABILITIES_SM,
    state INITIAL,
    upon receiving input LTE_RRC_UE_CAPABILITY_INFORMATION_CNFI

    @detail
    Called upon receipt of input LTE_RRC_UE_CAPABILITY_INFORMATION_CNFI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_cap_sm_ue_capability_information_cnfi
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_cap_sm_ue_capability_information_cnfi() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_cap_sm_eutra_capabilities_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CAPABILITIES_SM,
    state INITIAL,
    upon receiving input LTE_RRC_EUTRA_CAPABILITIES_REQ

    @detail
    Called upon receipt of input LTE_RRC_EUTRA_CAPABILITIES_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_cap_sm_eutra_capabilities_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_cap_sm_eutra_capabilities_req() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_cap_check_and_change_fgi_on_the_fly

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CAPABILITIES_SM,
    state INITIAL,
    upon receiving input LTE_RRC_CAP_FGI_CHANGE_REQ

    @detail
    Called upon receipt of input LTE_RRC_CAP_FGI_CHANGE_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_cap_check_and_change_fgi_on_the_fly
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_cap_check_and_change_fgi_on_the_fly() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_cap_sm_deactivate_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CAPABILITIES_SM,
    state INITIAL,
    upon receiving input LTE_RRC_DEACTIVATE_REQ

    @detail
    Called upon receipt of input LTE_RRC_DEACTIVATE_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_cap_sm_deactivate_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_cap_sm_deactivate_req() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_cap_sm_sim_update_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CAPABILITIES_SM,
    state INITIAL,
    upon receiving input LTE_RRC_SIM_UPDATE_REQ

    @detail
    Called upon receipt of input LTE_RRC_SIM_UPDATE_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_cap_sm_sim_update_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_cap_sm_sim_update_req() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_cap_sm_camped_indi

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CAPABILITIES_SM,
    state INITIAL,
    upon receiving input LTE_RRC_CAMPED_INDI

    @detail
    Called upon receipt of input LTE_RRC_CAMPED_INDI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_cap_sm_camped_indi
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_cap_sm_camped_indi() */


/*===========================================================================

     (State Machine: LTE_RRC_CAPABILITIES_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: WT_RAT_CAP_CNF

===========================================================================*/

/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_cap_sm_conn_released_indi

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CAPABILITIES_SM,
    state WT_RAT_CAP_CNF,
    upon receiving input LTE_RRC_CONN_RELEASED_INDI

    @detail
    Called upon receipt of input LTE_RRC_CONN_RELEASED_INDI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_cap_sm_conn_released_indi
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_cap_sm_conn_released_indi() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_cap_sm_irat_do_capabilites_cnfi

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CAPABILITIES_SM,
    state WT_RAT_CAP_CNF,
    upon receiving input LTE_RRC_IRAT_FROM_DO_TO_LTE_CAPABILITIES_CNFI

    @detail
    Called upon receipt of input LTE_RRC_IRAT_FROM_DO_TO_LTE_CAPABILITIES_CNFI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_cap_sm_irat_do_capabilites_cnfi
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_cap_sm_irat_do_capabilites_cnfi() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_cap_sm_irat_w_capabilities_cnfi

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CAPABILITIES_SM,
    state WT_RAT_CAP_CNF,
    upon receiving input LTE_RRC_IRAT_FROM_W_TO_LTE_CAPABILITIES_CNFI

    @detail
    Called upon receipt of input LTE_RRC_IRAT_FROM_W_TO_LTE_CAPABILITIES_CNFI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_cap_sm_irat_w_capabilities_cnfi
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_cap_sm_irat_w_capabilities_cnfi() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_cap_sm_irat_tds_capabilities_cnfi

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CAPABILITIES_SM,
    state WT_RAT_CAP_CNF,
    upon receiving input LTE_RRC_IRAT_FROM_TDS_TO_LTE_CAPABILITIES_CNFI

    @detail
    Called upon receipt of input LTE_RRC_IRAT_FROM_TDS_TO_LTE_CAPABILITIES_CNFI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_cap_sm_irat_tds_capabilities_cnfi
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_cap_sm_irat_tds_capabilities_cnfi() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_cap_sm_irat_g_cs_capabilities_cnfi

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CAPABILITIES_SM,
    state WT_RAT_CAP_CNF,
    upon receiving input LTE_RRC_IRAT_FROM_G_CS_TO_LTE_CAPABILITIES_CNFI

    @detail
    Called upon receipt of input LTE_RRC_IRAT_FROM_G_CS_TO_LTE_CAPABILITIES_CNFI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_cap_sm_irat_g_cs_capabilities_cnfi
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_cap_sm_irat_g_cs_capabilities_cnfi() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_cap_sm_irat_g_ps_capabilities_cnfi

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CAPABILITIES_SM,
    state WT_RAT_CAP_CNF,
    upon receiving input LTE_RRC_IRAT_FROM_G_PS_TO_LTE_CAPABILITIES_CNFI

    @detail
    Called upon receipt of input LTE_RRC_IRAT_FROM_G_PS_TO_LTE_CAPABILITIES_CNFI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_cap_sm_irat_g_ps_capabilities_cnfi
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_cap_sm_irat_g_ps_capabilities_cnfi() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_rrc_cap_sm_connected_indi

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_RRC_CAPABILITIES_SM,
    state WT_RAT_CAP_CNF,
    upon receiving input LTE_RRC_CONNECTED_INDI

    @detail
    Called upon receipt of input LTE_RRC_CONNECTED_INDI, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_rrc_cap_sm_connected_indi
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_rrc_cap_sm_connected_indi() */




