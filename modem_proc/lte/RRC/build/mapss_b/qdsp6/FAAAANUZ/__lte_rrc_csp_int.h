/*=============================================================================

    __lte_rrc_csp_int.h

Description:
  This file contains the machine generated header file for the state machine
  specified in the file:
  lte_rrc_csp.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


#ifndef __LTE_RRC_CSP_INT_H
#define __LTE_RRC_CSP_INT_H

#ifdef __cplusplus
/* If compiled into a C++ file, ensure symbols names are not mangled */
extern "C"
{
#endif

/* Include external state machine header */
#include "__lte_rrc_csp.h"

/* Begin machine generated internal header for state machine array: LTE_RRC_CSP_SM[] */

/* Suppress Lint suggestions to const-ify state machine and payload ptrs */
/*lint -esym(818,sm,payload) */

/* Define a macro for the number of SM instances */
#define LTE_RRC_CSP_SM_NUM_INSTANCES 1

/* Define a macro for the number of SM states */
#define LTE_RRC_CSP_SM_NUM_STATES 9

/* Define a macro for the number of SM inputs */
#define LTE_RRC_CSP_SM_NUM_INPUTS 54

#ifndef STM_DATA_STRUCTURES_ONLY
/* State Machine entry/exit function prototypes */
void lte_rrc_csp_sm_entry(stm_state_machine_t *sm,void *payload);


/* State entry/exit function prototypes */
void lte_rrc_csp_sm_camped_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);


/* Transition function prototypes */
stm_state_t lte_rrc_csp_sm_service_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_deactivate_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_system_update_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_conn_released_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_irat_from_lte_redir_failed_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_initial_ml1_revoke_reqi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_irat_to_lte_resel_reqi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_stopped_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_nw_sel_mode_reset_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_irat_to_lte_redir_list_reqi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_irat_to_lte_redir_full_reqi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_irat_to_lte_resel_completed_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_irat_to_lte_redir_completed_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_im3_backoff_applied_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_start_internal_cell_selection_reqi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_irat_to_lte_psho_started_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_serving_cell_changed_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_irat_to_lte_psho_success_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_interfreq_list_update_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_cell_bar_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_forbidden_ta_list_reset_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_band_pri_change_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_rlf_system_scan_tmri(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_system_scan_prior_t311_expiry_tmri(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_get_band_pri_list_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_set_pci_lock_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_set_earfcn_lock_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_ims_emerg_supp_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_mode_change_cnfi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_oos_tmri(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_cell_select_abort_reqi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_cfg_cnfi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_dl_weak_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_oos_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_sib_read_error_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_irat_to_lte_resel_abort_reqi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_irat_to_lte_redir_abort_reqi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_resel_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_cphy_system_scan_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_cphy_band_scan_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_cphy_acq_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_get_sibs_cnfi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_int_oos_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_cphy_cell_select_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_camped_conn_released_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_camped_system_update_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_sib_updated_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_conn_est_fail_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_camped_dl_weak_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_camped_sib_read_error_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_camped_oos_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_handover_completed_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_camped_get_sibs_cnfi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_camped_cfg_cnfi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_proceed_with_resel_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_initiate_cell_sel_reqi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_srb2_resumed_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_irat_from_lte_resel_failed_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_ml1_revoke_reqi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_irat_from_lte_plmn_srch_resume_failed_indi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_avoidance_channel_list_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_rrc_csp_sm_conn_rel_started_ind(stm_state_machine_t *sm, void *payload);


/* State enumeration */
enum
{
  INITIAL,
  WT_MODE_CHANGE_CNF,
  WT_CFG_CNF,
  WT_SYSTEM_SCAN_CNF,
  WT_BAND_SCAN_CNF,
  WT_ACQ_CNF,
  WT_SIB_CNF,
  WT_CELL_SELECT_CNF,
  CAMPED,
};

#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated internal header for state machine array: LTE_RRC_CSP_SM[] */


#ifdef __cplusplus
} /* extern "C" {...} */
#endif

#endif /* ! __LTE_RRC_CSP_INT_H */
