/*!
  @file
  lte_pdcp_ul_protocol.h

  @brief
  This file contains defines and functions interfaces for PDCP UL protocol stack.

*/

/*===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/L2/pdcp/src/lte_pdcp_ul_protocol.h#2 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/17/15   sb      CR748751: For RLF do not enable UL flow after recompression
10/15/14   sb      CR733993: Do not re-enqueue packet after RAB is dereg
04/11/14   mg      CR647594: QSH Phase 1
07/24/14   mg      CR699600: Revert CR669056
07/14/14   mg      CR665352: PDCP UL statistics API
07/03/14   mg      CR669056: PDCP discard timer algorithm
06/10/14   sb      CR672057: Revert timings of PDCP re-establishment to original and 
                   increase Num of IRs to huge number
06/12/14   sb      CR656696: KW fixes
05/08/14   mg      CR 500110: Klockwork Fixes
05/08/14  zb       CR660943: Fix bug introduced because of CR642185
04/25/14  sb       CR642185 :Delay PDCP re-establishment until RRC notifies for 
                   re-establishment complete
11/19/13   sb      CR535830: PDCP/RLC Re-arch and support of REL-11 features
10/02/13   mg      CR489345: Get num of PDCP PDU bytes transmitted for an EPSID
06/06/13   sb      CR495372: Remove LTE_PDCPUL_CRYPTO_DONE_IND
                   message and handle integrity checked SRB SDU synchronously
06/19/13   sb      CR458155: Set the reason for RoHC Reset (HO/Discard) so that
                   OFFLOAD calls the correct RoHC IOCTL
05/24/13   sb      CR491559: Free metainfo for packets sent for Re-RoHC and reallocate
                   them on recompression of packets
04/05/13   sb      CR457099: PDCP UL logging timer optimization
03/20/13   sb      CR464450: When EPSID and RBID mapping changes during 
                   RoHC ON/OFF testing, make sure to have PS watermark pointed 
                   correctly by OFFLOAD or PDCUL.
11/20/12   ax      CR383618: L2 modem heap use reduction phase I  
06/16/12   sb      CR365196 and CR365004: Fix to handle scenarios 
                   where RB switches back and forth with RoHC enabled and 
                   disabled. Also to reduce the delay in UL after HO
10/06/11   ax      make use of non_empty callback for PS-PDCP watermark
09/06/11   ax      add support for piggyback RoHC feedback
06/15/11   ax      RoHC support
03/06/11   ax      add support for release 9 PDCP full configuration
12/02/10   ax      fixed CR266146 cipher with old key during RLF or HO.
08/25/10   ar      save cfg for RECFG and HO cases and clean it after
                   LTE_RRC_CONFIG_COMPLETED_IND
07/08/10   ax      fixed flow state issue
05/19/10   ar      added PDCP ciphering log packet support
02/10/09   ar      added PS->PDCPUL wm enqueue ind optimization
11/09/09   ax      relocated num_rst in statistics
09/10/09   bq      RLF reestablish fix
08/27/09   bq      Security Corner case fix
08/03/09   bq      RLF implementation
02/22/09   bq      add flow suspend state during HO/RB release
02/17/09   bq      fix cfg validation
02/02/09   bq      eNodeB generate status report handling
01/28/09   bq      add peer ctrl ind handler
07/18/08   bq      Initial version
===========================================================================*/

#ifndef LTE_PDCP_UL_PROTOCOL_H
#define LTE_PDCP_UL_PROTOCOL_H

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/
#include <comdef.h>
#include "ps_rohc.h"
#include "lte_l2_comdef.h"
#include "mutils_circ_q.h"
#include "lte_pdcp_msg.h"
#include "lte_pdcp_offload_msg.h"
#include "lte_pdcp_offload_protocol.h"
#include "lte_pdcp_ul_if.h"
#include "lte_pdcpi.h"
#include "lte_l2_common.h"
#include "qsh.h"

/*===========================================================================

                   EXTERNAL DEFINITIONS AND TYPES

===========================================================================*/
/*! @brief PDCP RB UL state
*/
typedef enum
{
  LTE_PDCPUL_IDLE = 0,
  LTE_PDCPUL_ACTIVE = 1
} lte_pdcpul_state_e;

/*! @brief bit mask (first LSB) that marks the watermark state:
     0 for enable; 1 for disable
*/
#define LTE_PDCPUL_FLOW_MASK_DISABLE                            1

/*! @brief bit mask (second LSB) that marks the flow state:
     1 for suspend; 0 for resume
*/
#define LTE_PDCPUL_FLOW_MASK_SUSPEND                            2

/*! @brief PDCP RB UL flow state
*/
typedef enum
{
  LTE_PDCPUL_FLOW_ENABLE = 0,  /* enabled */
  LTE_PDCPUL_FLOW_DISABLE = LTE_PDCPUL_FLOW_MASK_DISABLE, /* cross high wm */
  LTE_PDCPUL_FLOW_SUSPEND = LTE_PDCPUL_FLOW_MASK_SUSPEND, /* during HO or
                                                   RB release or RLF suspend */
  LTE_PDCPUL_FLOW_DISABLE_SUSPEND = LTE_PDCPUL_FLOW_MASK_SUSPEND |
                                    LTE_PDCPUL_FLOW_MASK_DISABLE /* during HO
                                    or RB release or RLF suspend */
} lte_pdcpul_flow_e;

typedef enum
{
  LTE_PDCPUL_PKT_TYPE_NEW = 1,
  LTE_PDCPUL_PKT_TYPE_RETX,
  LTE_PDCPUL_PKT_TYPE_CONTROL,
  LTE_PDCPUL_PKT_TYPE_ROHC
}lte_pdcpul_pkt_type_e;

/*! @brief PDCP RB UL cfg parameter control block
*/
typedef lte_pdcpul_info_s lte_pdcpul_cfg_s;

/*! @brief invalid TX queue index
*/
#define LTE_PDCPUL_INVALID_TX_QUEUE_INDEX 0xFFFF

/* MAX PDCP status report buffer */
#define LTE_PDCPUL_STATUS_REPORT_BYTE_SIZE 256

/* Discard Timer Tick in msec */
#define LTE_PDCPUL_DISCARD_TIMER_TICK_IN_MSEC  10

/* Discard Threshold when ROHC is config */
#define LTE_PDCPUL_DISCARD_THRESHOLD_WITH_ROHC  12

/*! @brief saved configuration info from RRC to PDCP for add/delete RB during HO
 *  this structure is used to reapply the add/delete config later during HO
 *  when HO is completed or RLF is happening
*/
typedef struct
{
  /*!< number of released RB */
  uint8                               num_released_rb;
  /*!< released RB cfg index */
  lte_rb_cfg_idx_t                    released_rb_cfg_idx[LTE_MAX_ACTIVE_RB];
  /*!< number of add RB */
  uint8                               num_add_rb;
  /*!< add RB Cfg index */
  lte_rb_cfg_idx_t                    add_rb_cfg_idx[LTE_MAX_ACTIVE_RB];
  /*!< UL RB config data base ptr */
  lte_pdcpul_info_s                   *rb_cfg_base_ptr;

} lte_pdcpul_saved_cfg_req_s;

/*! @brief PDCP RB UL state control block
*/
typedef struct
{
  lte_pdcpul_state_e state;   /*!< PDCP state */
  lte_pdcpul_flow_e  flow;    /*!< PDCP flow mask */

  uint32             tx_hfn;  /*!< PDCP TX HFN */
  uint32             tx_seq;  /*!< PDCP TX seq */

  uint32             seq_modulus; /*!< PDCP seq modulus */
  uint32             seq_mask; /*!< PDCP seq mask */

  dsm_watermark_type  *ps_to_pdcp_wm_ptr;   /*!< PS to PDCP watermark */

  dsm_item_type      *dl_tx_status_report_ptr; /*!< PDCP DL generate status report */

  /*For Debug info*/

  dsm_item_type      *last_reenqueued_head_ptr; /*Head of  re-enqueued DSM chains to PS WM
                                                                                     during HO*/
                                                                                     	
  dsm_item_type      *dequeued_high_dsm_ptr; /*Head of high prio DSM chains to discarded
                                                                                  on calling DSM API*/
                                                                                     	
  dsm_item_type      *dequeued_normal_dsm_ptr; /*Head of normal prio DSM chains to discarded
                                                                                  on calling DSM API*/

  const dsm_item_type      *last_proc_item_disc; /*Last DSM item processed by DSM callback  
                                                                           lte_pdcpul_traverse_wm_to_discard_pkts
                                                                           during discard timer expiry*/
                                                                           
  dsm_wm_iterator_req_enum_type last_iter_req; /*Last iter req processed by DSM callback  
                                                                           lte_pdcpul_traverse_wm_to_discard_pkts
                                                                           during discard timer expiry*/

  /* Discard related state */
  /*!< Number of consecutive discard */
  uint16             num_consecutive_discard;
  /*!< Number of consecutive discard threshold*/
  uint16             num_consecutive_discard_threshold;
  /*!< PDCP discard timer unit based on
       LTE_PDCPUL_DISCARD_TIMER_TICK_IN_MSEC */
  uint16             discard_timer_tick;

  /*Boolean to indicate RoHC was disabled/enabled back and forth on the same RB*/
  boolean rohc_switch_on_off;

} lte_pdcpul_state_s;

/*! @brief PDCP statistics of the RB
*/
typedef struct
{
  uint32  num_rst;     /*!< number of reestablishments since activated */
  lte_pdcpul_stats_set_s  curr_stats;  /*!< statistics of the RB since last reestablishment/activation*/
  lte_pdcpul_stats_set_s  stats_upto_rst; /*!< accumulation upto the last est*/
} lte_pdcpul_stats_s;

/*! @brief PDCP RB UL control block
*/
typedef struct
{
  lte_pdcpul_cfg_s                  cfg;      /*!< config */
  lte_pdcpul_state_s                state;    /*!< state  */
  lte_pdcpul_stats_s                stats;    /*!< stats  */
} lte_pdcpul_cb_s;

/*! @brief PS to PDCP watermark related information
*/
typedef struct
{
  dsm_watermark_type*  ps_pdcp_wm_ptr;  /*!< PS to PDCP watermark pointer */
  boolean rb_map_rohc_enabled;   /*!<If RB mapped to this EPSID is rohc enabled*/
  lte_rb_cfg_idx_t rb_cfg_idx;  /*Corresponding RB_CF_IDX for EPS_ID*/
} lte_pdcpul_eps_info_s;

/*! @brief PDCP UL protocol stack control block
*/
typedef struct
{
  lte_pdcpul_cb_s*    rb_cb[LTE_MAX_RB_CFG_IDX];    /*!< RB control block array */

  lte_pdcpul_cb_s     *discard_cfg_rb_cb[LTE_MAX_ACTIVE_RB];
  uint8               num_discard_cfg_rb;

  /*!< security config */
  lte_pdcp_security_cfg_s security_cfg;

  /* Saved config request during HO. */
  lte_pdcpul_saved_cfg_req_s save_cfg_req;

  uint8                   num_active_rb; /*!<Num of Active RBs*/

  boolean       no_srb_cipher; /*if RRC asks an SDU not to be ciphered*/

} lte_pdcpul_protocol_cb_s;

/*! @brief Callback user data for DSM iterator function
*/
typedef struct
{
  uint32 seq_index;
  lte_rb_cfg_idx_t rb_cfg_idx;
}lte_pdcpul_protocol_dsm_iterator_cb_s;


/*! @brief PDCP UL protocol static data structure
*/
typedef struct
{
  lte_pdcpul_eps_info_s   eps_info[LTE_MAX_EPS]; /*!< EPS related information */
  dsm_watermark_type        lte_pdcpul_dummy_wm;
  q_type                    lte_pdcpul_dummy_q;

  /*when RB is not activated/added, this dummy Ctrl Blk to be pointed to for
  better performance: no check of NULL pointer is needed*/
  lte_pdcpul_cb_s           lte_pdcp_dummy_rb_cb;
  lte_pdcpul_rlc_wm_s  pdcp_rlc_wm[LTE_MAX_ACTIVE_SRB]; /*!< PDCP RLC WM */
  uint16 sys_discard_tick;
  int lte_pdcpul_thread_id;
} lte_pdcpul_static_s;

/*===========================================================================

                    EXTERNAL FUNCTION PROTOTYPES

===========================================================================*/


/*===========================================================================

  FUNCTION:  lte_pdcpul_process_re_est_status_ind_ind

===========================================================================*/
/*!
    @brief
    This function is called to process the reest status ind from RLC UL for
    AM SRB during HO

    @detail

    @return

    @note

    @see
*/
/*=========================================================================*/
extern void lte_pdcpul_process_re_est_status_ind
(
  lte_rb_cfg_idx_t  rb_cfg_idx,
  dsm_item_type *ack_pdu_ptr,
  dsm_item_type *nack_pdu_ptr,
  dsm_item_type *maybe_pdu_ptr
);

EXTERN void lte_pdcpul_sdu_ind_handler
(
  lte_rb_cfg_idx_t  rb_cfg_idx,
  dsm_item_type *ack_pdu_ptr,
  dsm_item_type *nack_pdu_ptr,
  dsm_item_type *maybe_pdu_ptr
);

/*===========================================================================

  FUNCTION:  lte_pdcpul_process_recfg_req

===========================================================================*/
/*!
    @brief
    This function is called to process RB reconfig request for add/modified/delete.

    @detail

    @return
    TRUE for success, FALSE for fail

    @note

    @see

*/
/*=========================================================================*/
extern boolean lte_pdcpul_process_recfg_req
(
  lte_l2_cfg_reason_e cfg_reason,
  lte_pdcp_cfg_act_e  cfg_act,
  lte_rb_cfg_idx_t  rb_cfg_idx,
  lte_pdcpul_info_s* cfg_info_ptr
);

/*===========================================================================

  FUNCTION:  lte_pdcpul_process_rab_register_req

===========================================================================*/
/*!
    @brief
    This function is called to process RAB register request from PS.

    @detail

    @return
    TRUE for success, FALSE for fail

    @note

    @see

*/
/*=========================================================================*/
extern boolean lte_pdcpul_process_rab_register_req
(
  lte_eps_id_t  eps_id,      /*!< EPS ID */
  dsm_watermark_type* ps_to_pdcp_wm_ptr /*!< PS to PDCP WM ptr */
);

/*===========================================================================

  FUNCTION:  lte_pdcpul_process_rab_deregister_req

===========================================================================*/
/*!
    @brief
    This function is called to process RAB Deregister request from PS.

    @detail

    @return
    TRUE for success, FALSE for fail

    @note

    @see

*/
/*=========================================================================*/
extern boolean lte_pdcpul_process_rab_deregister_req
(
  lte_eps_id_t  eps_id      /*!< EPS ID */
);

/*===========================================================================

  FUNCTION:  lte_pdcpul_process_discard_timer_tick_ind

===========================================================================*/
/*!
    @brief
    This function is called to process discard timer tick ind.

    @detail

    @return

    @note

    @see

*/
/*=========================================================================*/
extern void lte_pdcpul_process_discard_timer_tick_ind
(
  void
);

/*===========================================================================

  FUNCTION:  lte_pdcpul_process_connection_release

===========================================================================*/
/*!
    @brief
    This function is called to process connection release.

    @detail

    @return

    @note

    @see

*/
/*=========================================================================*/
extern void lte_pdcpul_process_connection_release
(
  void
);

/*===========================================================================

  FUNCTION:  lte_pdcpul_process_sdu_req

===========================================================================*/
/*!
    @brief
    This function is called to process SDU request from RRC.

    @detail

    @return
    TRUE for success

    @note

    @see

*/
/*=========================================================================*/
extern boolean lte_pdcpul_process_sdu_req
(
  lte_rb_cfg_idx_t  rb_cfg_idx,
  uint8             mu_id,
  dsm_item_type*    pkt_ptr,
  boolean           integrity_protect,  /*! need integrity protection. */
  boolean           cipher /*! need cipher. */
);

/*===========================================================================

  FUNCTION:  lte_pdcpul_process_recfg_prep_req

===========================================================================*/
/*!
    @brief
    This function is called to process RB reconfig prep request.

    @detail

    @return

    @note

    @see

*/
/*=========================================================================*/
extern void lte_pdcpul_process_recfg_prep_req
(
  lte_rb_cfg_idx_t  rb_cfg_idx
);

/*===========================================================================

  FUNCTION:  lte_pdcpul_process_ho_recfg_prep_req

===========================================================================*/
/*!
    @brief
    This function is called to process RB reconfig prep request for HO.

    @detail

    @return

    @note

    @see

*/
/*=========================================================================*/
extern void lte_pdcpul_process_ho_recfg_prep_req
(
  void
);

/*===========================================================================

  FUNCTION:  lte_pdcpul_process_rlf_recfg_prep_req

===========================================================================*/
/*!
    @brief
    This function is called to process RB reconfig prep request for RLF.

    @detail

    @return

    @note

    @see

*/
/*=========================================================================*/
extern void lte_pdcpul_process_rlf_recfg_prep_req
(
  void
);

/*===========================================================================

  FUNCTION:  lte_pdcpul_send_control_pdu

===========================================================================*/
/*!
    @brief
    This function is called to send PDCP control msg generated by PDCP DL.

    @detail

    @return

    @note

    @see

*/
/*=========================================================================*/
extern void lte_pdcpul_send_control_pdu
(
  lte_rb_cfg_idx_t  rb_cfg_idx,
  dsm_item_type*    pkt_ptr,
  boolean           status_report
);

/*===========================================================================

  FUNCTION:  lte_pdcpul_process_peer_status_ind

===========================================================================*/
/*!
    @brief
    This function is called to process the PDCP status report via PDCPDL from
    EnodeB.

    @detail

    @return

    @note

    @see

*/
/*=========================================================================*/
extern void lte_pdcpul_process_peer_status_ind
(
  lte_rb_cfg_idx_t  rb_cfg_idx,
  dsm_item_type*    pkt_ptr
);


extern uint16 lte_pdcpul_encap_pdcp_hdr
(
  lte_rb_cfg_idx_t rb_cfg_idx,
  dsm_item_type* pkt_ptr
);

/*==============================================================================

  FUNCTION:  lte_pdcpul_update_data_stats

==============================================================================*/
/*!
    @brief
    This function update PDCP UL stats based on PDCP SDU sent OTA
    
    @detail
    This API will be called by RLC when PDCP SDU is dequeued from WM to 
    build RLC PDU
    
    @return
    none
    
    @note

    @see

*/
/*============================================================================*/
EXTERN void lte_pdcpul_update_data_stats
(
  lte_rb_cfg_idx_t rb_cfg_idx,
  uint32 size
);

/*==============================================================================

  FUNCTION:  lte_pdcpul_get_pdcp_hdr_cipher_info

==============================================================================*/
/*!
    @brief
 This API returns PDCP header info and ciphering params per
 packet

    @detail
This will be called by RLC while writing RLC PDU to A2 

    @return

*/
/*============================================================================*/
EXTERN uint8 lte_pdcpul_get_pdcp_hdr_cipher_info
(
  lte_rb_cfg_idx_t rb_cfg_idx,
  dsm_item_type* sdu_dsm_ptr,
  uint8* pdcp_hdr,
  uint8* cipher_algorithm,
  uint8* cipher_key_index,
  uint32* count
);

/*==============================================================================

  FUNCTION:  lte_pdcpul_calc_pdcp_hdr_size

==============================================================================*/
/*!
    @brief
  Calculate PDCP header size based on RB and PDU type

  @return

*/
/*============================================================================*/
EXTERN uint16 lte_pdcpul_calc_pdcp_hdr_size
(
  lte_rb_cfg_idx_t  rb_cfg_idx,
  dsm_item_type* pkt_ptr
);

/*==============================================================================

  FUNCTION:  lte_pdcpul_reg_non_empty_cb_func

==============================================================================*/
/*!
    @brief
    This API will register Non-empty callback function to PS-PDCP WM for DRB OR
    PDCP-RLC WM for SRB

    @return
    None
*/
/*============================================================================*/

EXTERN void lte_pdcpul_reg_non_empty_cb_func
(
  dsm_watermark_type* wm_ptr,
  lte_rb_cfg_idx_t rb_cfg_idx
);

/*===========================================================================

  FUNCTION:  lte_pdcpul_protocol_static_init

===========================================================================*/
/*!
  @brief
  one time Init PDCP UL protocol stack

  @return

*/
/*=========================================================================*/
extern void lte_pdcpul_protocol_static_init
(
  void
);

/*===========================================================================

  FUNCTION:  lte_pdcpul_protocol_dynamic_init

===========================================================================*/
/*!
  @brief
  dynamic Init PDCP UL protocol stack at start REQ to allocate memory resrouces

  @return

*/
/*=========================================================================*/
extern void lte_pdcpul_protocol_dynamic_init
(
  void
);

/*===========================================================================

  FUNCTION:  lte_pdcpul_protocol_dynamic_init

===========================================================================*/
/*!
  @brief
  PDCP UL protocol stack deinit at stop REQ to deallocate memory resrouces

  @return

*/
/*=========================================================================*/
extern void lte_pdcpul_protocol_deinit
(
  void
);
/*===========================================================================

  FUNCTION:  lte_pdcpul_process_reestablish_req

===========================================================================*/
/*!
    @brief
    This function is called to perform reestablish for active RBs
    the cfg reason can be RLF or HO

    @detail

    @return

    @note

    @see

*/
/*=========================================================================*/
extern void lte_pdcpul_process_reestablish_req
(
  lte_l2_cfg_reason_e cfg_reason
);

/*===========================================================================

  FUNCTION:  lte_pdcpul_process_crypto_done_ind

===========================================================================*/
/*!
    @brief
    This function is called to process the crypto done ind

    @detail

    @return

    @note

    @see
*/
/*=========================================================================*/
extern void lte_pdcpul_process_crypto_done_ind
(
  lte_rb_cfg_idx_t    rb_cfg_idx,
  dsm_item_type*      dsm_ptr,
  uint8               mac_i[4]
);

/*===========================================================================

  FUNCTION:  lte_pdcpul_process_cfg_completed_ind

===========================================================================*/
/*!
    @brief
    This function is called to process the config completed ind from RRC

    @detail

    @return

    @note

    @see
*/
/*=========================================================================*/
extern void lte_pdcpul_process_cfg_completed_ind
(
  void
);

/*===========================================================================

  FUNCTION:  lte_pdcpul_process_counter_check_req

===========================================================================*/
/*!
    @brief
    This function is called to process the counter check request from RRC

    @detail

    @return
    Number of active drbs

    @note

    @see
*/
/*=========================================================================*/
extern uint8 lte_pdcpul_process_counter_check_req
(
  lte_pdcp_count_info_s *drb_count_info_ptr
);

/*===========================================================================

  FUNCTION:  lte_pdcpul_process_security_cfg

===========================================================================*/
/*!
    @brief
    This function is called to process security cfg from RRC.

    @detail

    @return
    TRUE if any parameter has changed.

    @note

    @see

*/
/*=========================================================================*/
extern boolean lte_pdcpul_process_security_cfg
(
  lte_pdcp_security_cfg_s *security_cfg_ptr
);


/*===========================================================================

  FUNCTION:  lte_pdcpul_get_cb_size

===========================================================================*/
/*!
    @brief
    This function is called to return the total control block size for PDCP UL.

    @detail

    @return
    PDCP UL protocol stack control block size

    @note

    @see

*/
/*=========================================================================*/
extern uint32 lte_pdcpul_get_cb_size
(
  void
);

/*===========================================================================

  FUNCTION:  lte_pdcpul_get_cb

===========================================================================*/
/*!
    @brief
    This function is called to return the control block ptr for PDCP UL.

    @detail

    @return
    PDCP UL protocol stack control block ptr

    @note

    @see

*/
/*=========================================================================*/
extern lte_pdcpul_protocol_cb_s* lte_pdcpul_get_cb
(
  void
);

/*==============================================================================

  FUNCTION:  lte_pdcpul_static_data_init

==============================================================================*/
/*!
    @brief
    This function is called to initialize the static structure lte_pdcpul_static_s.

    @detail
    This function sets the eps_info.rb_cfg_idx to LTE_INVALID_RB_CFG_IDX

    @return
    void

    @note

    @see

*/
/*============================================================================*/
extern void lte_pdcpul_static_data_init
(
  void
);

/*===========================================================================

  FUNCTION:  lte_pdcpul_get_rb_cb

===========================================================================*/
/*!
    @brief
    This function is called to return the RB control block ptr for PDCP UL.

    @detail

    @return
    PDCP UL RB block ptr

    @note

    @see

*/
/*=========================================================================*/
extern lte_pdcpul_cb_s* lte_pdcpul_get_rb_cb
(
  lte_rb_cfg_idx_t  rb_cfg_idx
);

/*===========================================================================

  FUNCTION:  lte_pdcpul_valid_recfg_req

===========================================================================*/
/*!
    @brief
    This function is called to valid RB reconfig request for add/modified/delete.

    @detail

    @return
    TRUE for success, FALSE for fail

    @note

    @see

*/
/*=========================================================================*/
extern boolean lte_pdcpul_valid_recfg_req
(
  lte_l2_cfg_reason_e cfg_reason,
  uint8   num_released_rb, /*!< number of released RB */
  lte_rb_cfg_idx_t released_rb_cfg_idx[], /*!< released RB cfg index */
  uint8   num_add_modified_rb, /*!< number of add/modified RB */
  lte_pdcp_cfg_act_s add_modified_rb[] /*!< cfg action for add/modified RB ID */
);

/*===========================================================================

  FUNCTION:  lte_pdcpul_save_recfg_req

===========================================================================*/
/*!
    @brief
    This function is called to save RB reconfig request for add/delete

    @detail

    @return

    @note

    @see

*/
/*=========================================================================*/
extern void lte_pdcpul_save_recfg_req
(
  uint8   num_released_rb, /*!< number of released RB */
  lte_rb_cfg_idx_t released_rb_cfg_idx[], /*!< released RB cfg index */
  uint8   num_add_modified_rb, /*!< number of add/modified RB */
  lte_pdcp_cfg_act_s add_modified_rb[], /*!< cfg action for add/modified RB ID */
  lte_pdcpul_info_s* rb_cfg_base_ptr /*!< RB config base ptr */
);

/*===========================================================================

  FUNCTION:  lte_pdcpul_reset_saved_recfg_req

===========================================================================*/
/*!
    @brief
    This function is called to reset saved recfg req

    @detail

    @return

    @note

    @see

*/
/*=========================================================================*/
extern void lte_pdcpul_reset_saved_recfg_req
(
  void
);

/*==============================================================================

  FUNCTION:  lte_pdcpul_get_cb

==============================================================================*/
/*!
  @brief
    This function is called to return the Stattic data pointer PDCP UL.

    @detail

  @return
    PDCP UL protocol static data structure

    @note

    @see

*/
/*============================================================================*/
extern lte_pdcpul_static_s* lte_pdcpul_get_static_data_ptr
(
  void
);

/*===========================================================================

  FUNCTION:  lte_pdcpul_get_security_cfg

===========================================================================*/
/*!
    @brief
    This function is called to return the security cfg for PDCP UL.

    @detail

    @return
    Security Cfg Ptr

    @note

    @see

*/
/*=========================================================================*/
extern lte_pdcp_security_cfg_s* lte_pdcpul_get_security_cfg
(
  void
);

/*===========================================================================

  FUNCTION:  lte_pdcpul_flush_log_stats

===========================================================================*/
/*!
    @brief
    This function is called to flush the statistics log for PDCP UL

    @detail

    @return

    @note

    @see
*/
/*=========================================================================*/
extern void lte_pdcpul_flush_log_stats
(
  void
);

extern boolean lte_pdcpul_flow_enabled
(
  lte_pdcpul_flow_e      flow
);

extern boolean lte_pdcpul_flow_suspended
(
  lte_pdcpul_flow_e      flow
);

extern void lte_pdcpul_suspend_flow
(
  lte_pdcpul_cb_s*      cb_ptr
);

extern void lte_pdcpul_resume_flow
(
  lte_pdcpul_cb_s*      cb_ptr
);

extern void lte_pdcpul_update_eps_2_cb_mapping( void );

extern dsm_watermark_type* lte_pdcp_offload_get_ps_pdcp_offload_wm
(
  lte_rb_cfg_idx_t  rb_cfg_idx
);

/*==============================================================================

  FUNCTION:  lte_pdcpul_process_rb_stats_req

==============================================================================*/
/*!
    @brief
    Returns the number of active DRB statistics copied into the statistics
    pointer passed by the calling function.

    @detail

    @return

*/
/*============================================================================*/
extern uint8 lte_pdcpul_process_rb_stats_req
(
  lte_pdcpul_rb_stats_req_msg_s* msg_ptr
);

/*===========================================================================

  FUNCTION:  lte_pdcpul_start_log_timer

===========================================================================*/
/*!
    @brief
    This function will start the Log Flush timer.

    @detail

    @return
    None

    @note

    @see

*/
/*=========================================================================*/
EXTERN void lte_pdcpul_start_log_timer (void);

/*===========================================================================

  FUNCTION:  lte_pdcpul_stop_log_timer

===========================================================================*/
/*!
    @brief
    This function will Stop the Log Flush timer.

    @detail

    @return
    None

    @note

    @see

*/
/*=========================================================================*/
EXTERN void lte_pdcpul_stop_log_timer (void);

EXTERN void lte_pdcpul_process_free_pdu_ind
(
  dsm_item_type *pdu_ptr
);

EXTERN void lte_pdcpul_process_free_pkt_ind
(
  dsm_item_type *pdu_ptr
);

EXTERN void lte_pdcpul_qsh_analysis_routine
(
  qsh_action_e action,
  uint32 category_mask
);
EXTERN void lte_pdcpul_protocol_rohc_comp_done_ind
(
  lte_rb_cfg_idx_t rb_cfg_idx,
  lte_l2_cfg_reason_e cfg_reason
);
#endif /* LTE_PDCP_UL_PROTOCOL_H */

