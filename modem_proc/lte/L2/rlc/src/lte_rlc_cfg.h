/*!
  @file
  lte_rlc_cfg.h

  @brief
  Compile time configuration for RLC.

  @detail
  RLC can be configured at compile time to turn on certain features.  
  Compile time flags defined in this file shall meet these requirements:
  -# All flags shall start with FEATURE_LTE_RLC_.
  -# A Flag shall not cause another flag to be defined or undefined. 
  -# Flags used only in test code shall not be defined in this file.

  @author
  axiao

*/

/*==============================================================================

  Copyright (c) 2013 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/L2/rlc/src/lte_rlc_cfg.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------- 
05/27/14   mm      CR 670482: Changes for new QSH API
04/11/14   mg      CR647594: QSH Phase 1
04/03/14   mg      CR643306: DSDS RLC Fast NAK after receiving RF_AVAILABLE_IND 
01/23/14   mg      CR599494: RLC behavior to alleviate the low DSM count problem.
11/19/13   sb      CR535830: PDCP/RLC Re-arch and support of REL-11 features 
04/08/13   md      Reduced queue depth
08/22/12   ax      CR383618: L2 modem heap use reduction phase I  
12/16/11   ax      feature clean up 
10/25/11   ax      make RLC-A2 eMBMS watermark levels configruable
07/21/11   ax      fixed CR#297113: detect and release huge SDU 
04/05/10   ax      add #define for task mailbox size
03/26/10   ax      remvoed #define FEATURE_LTE_RLC_PROFILE
09/17/09   ax      moved rlcdl logging related timer value here
09/04/09   ax      moved UL logging related timer values from lte_rlcul_log.c
08/25/09   ax      merge lte_rlc_cfg.h files into a single file
06/08/09   ax      include lte_l2_profile.h so that it does not depend on tests
06/02/09   ax      center of RLC featurization
==============================================================================*/

#ifndef LTE_RLC_CFG_H
#define LTE_RLC_CFG_H

/*==============================================================================

                           INCLUDE FILES

==============================================================================*/

#include <comdef.h>
#include "lte_l2_profile.h"
#include "lte_as.h"

/*==============================================================================

                   EXTERNAL DEFINITIONS AND TYPES

==============================================================================*/

/*==============================================================================

                   FEATURIZATION

==============================================================================*/

/*==============================================================================

                   RLCDL thresholds

==============================================================================*/

/*! @brief largest PDCP SDU size is 8188 per spec, assuming PDCP control PDU 
     will not be anywhere near this limit
*/
#define LTE_RLC_CFG_MAX_RLC_SDU_SZ                                    8200

/*==============================================================================

                   RLCDL memory related EXTERNAL DEFINITIONS AND TYPES

==============================================================================*/

/*! @brief maximum pending message router messages allowed
*/
#define LTE_RLC_CFG_DL_TASK_MAX_MSG                                          50

/*! @brief number of RLCDL NAK elements dynamically allocated at one time
*/
#define LTE_RLC_CFG_DL_NUM_NAK_ELEMENTS                                      512 

/*! @brief pdu log buffer size for received PDUs
*/
#define LTE_RLC_CFG_DL_LOG_PDU_BUF_SZ                                       2048

/*! @brief RLCDL->PDCPDL circular queue size (number of elements)
     10ms*1 entry per TTI per bearer*total number of active bearers
*/
#define LTE_RLC_CFG_DL_SDU_CIRC_Q_SIZE                    (LTE_MAX_ACTIVE_RB*10)

/*! @brief pdu log buffer size for transmitted PDUs
*/
#define LTE_RLC_CFG_UL_LOG_PDU_BUF_SIZE                                     2048

/*! @brief RLCDL task scratch pad size: 
    The scratch pad is used for the following, it is critical that the size is 
    big enough for the maximum required size for all the intended usage.
    1. extract the extended header (LI's)
*/
#define LTE_RLC_CFG_DL_SCRATCH_PAD_SIZE                                      512

/*! @brief RLC-A2 eMBMS watermark low
*/
#define LTE_RLC_CFG_EMBMS_WM_LO                                     60000

/*! @brief RLC-A2 eMBMS watermark high
*/
#define LTE_RLC_CFG_EMBMS_WM_HI                                     75000

/*! @brief RLC-A2 eMBMS watermark dne
*/
#define LTE_RLC_CFG_EMBMS_WM_DNE                                    90000

/*==============================================================================

                   RLCUL memory related EXTERNAL DEFINITIONS AND TYPES

==============================================================================*/

/*! @brief maximum pending message router messages allowed
*/
#define LTE_RLC_CFG_UL_TASK_MAX_MSG                                          100

/*! @brief Macro for maximum number of NACKs per status pdu. This number is a
 * restriction from the UE side. If there are any more NACKs present in the
 * status pdu, than this number, UE will ignore them.
*/
#define LTE_RLC_CFG_UL_MAX_NACKS_PER_STATUS_PDU                              512

/*! @brief Macro for maximum supported size (in bytes) of the RLCDL status pdu
*/
#define LTE_RLC_CFG_UL_AM_STATUS_PDU_MAX_SIZE                               2000

/*==============================================================================

                   RLCUL timing related EXTERNAL DEFINITIONS AND TYPES

==============================================================================*/

/*! @brief Log flush timer periodicity in ms
*/
#define LTE_RLC_CFG_UL_FLUSH_TIMER_PERIODICITY                                45

/*! @brief Log statistics timer periodicity in ms: 1.44 second
*/
#define LTE_RLC_CFG_UL_STATS_TIMER_PERIODICITY                 \
(LTE_RLC_CFG_UL_FLUSH_TIMER_PERIODICITY << 5)

/*! @brief Log status update timer periodicity in ms
*/
#define LTE_RLC_CFG_UL_LOG_STATUS_TIMER_PERIODICITY                          100

/*==============================================================================

                   RLCDL timing related EXTERNAL DEFINITIONS AND TYPES

==============================================================================*/
/*! @brief flush every 40(8*5) ms (periodicity of timer )
*/
#define LTE_RLC_CFG_DL_PDU_LOG_FLUSH(counter)     (((counter) & 0x7) == 0)

/*! @brief update log status every 160(32*5) ms (periodicity of timer )
*/
#define LTE_RLC_CFG_DL_LOG_UPDATE(counter)    (((counter) & 0x1f) == 0)

/*! @brief update log status every 1280(256*5) ms (periodicity of timer )
*/
#define LTE_RLC_CFG_DL_LOG_STATISTICS(counter)    (((counter) & 0xff) == 0)

/*==============================================================================

                   RLCDL flow control related EXTERNAL DEFINITIONS AND TYPES

==============================================================================*/
/*! @brief mask when rlc low dsm count behavior is not active
*/
#define LTE_RLC_CFG_DL_FC_DISABLE_MASK                                       0

/*! @brief mask when rlc low dsm count behavior is active
*/
#define LTE_RLC_CFG_DL_FC_LOW_MEMORY_MASK                                    0x00000001

/*! @brief mask when rlc low dsm count behavior is active
*/
#define LTE_RLC_CFG_DL_DSDS_RF_AVAILABLE_MASK                                0x00000001 << 1

/*! @brief AM default reordering timer when rlc low dsm count behavior is active
*/
#define LTE_RLC_CFG_DL_FC_DEFAULT_AM_T_REORDERING                            10

/*! @brief AM default status prohibit timer when rlc low dsm count behavior is active
*/
#define LTE_RLC_CFG_DL_FC_DEFAULT_AM_T_STATUS_PROHIBIT                       15

/*! @brief UM default reordering timer when rlc low dsm count behavior is active
*/
#define LTE_RLC_CFG_DL_FC_DEFAULT_UM_T_REORDERING                            20

/*==============================================================================

                   RLC QSH related EXTERNAL DEFINITIONS AND TYPES

==============================================================================*/
#define LTE_RLC_CFG_QSH_MAJOR_VER                                            1
#define LTE_RLC_CFG_QSH_MINOR_VER                                            1
#define LTE_RLC_CFG_DL_QSH_HIGH_T_REORDERING                                 50
#define LTE_RLC_CFG_DL_QSH_HIGH_T_STATUS_PROHIBIT                            80
#define LTE_RLC_CFG_DL_QSH_HIGH_SEQ_DIFF                                     100


#define LTE_RLC_CFG_UL_QSH_HIGH_T_POLL                                       80
#define LTE_RLC_CFG_UL_QSH_HIGH_SEQ_DIFF                                     100

/*! @brief Time in milliseconds for which the DSDS fast nak behavior is active
*/
#define LTE_RLC_CFG_DL_DSDS_FAST_NAK_TIMER                                   45
/*==============================================================================

                    EXTERNAL FUNCTION PROTOTYPES

==============================================================================*/


#endif /* LTE_RLC_CFG_H */
