/*!
  @file
  lte_rlcdl_nak.h

  @brief
  Definition file for RLC DL NAK related procedures and functions.

  @detail
  OPTIONAL detailed description of this C header file.
  - DELETE this section if unused.

*/

/*==============================================================================

  Copyright (c) 2008 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/L2/rlc/src/lte_rlcdl_nak.h#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------------
08/19/2008 ax      initial version
==============================================================================*/

#ifndef LTE_RLCDL_NAK_H
#define LTE_RLCDL_NAK_H

/*==============================================================================

                           INCLUDE FILES

==============================================================================*/

#include <comdef.h>
#include <queue.h>
/*==============================================================================

                   EXTERNAL DEFINITIONS AND TYPES

==============================================================================*/
/*-------------------------------------------------------------------------*/
/*! @brief nak hole element definition
*/
typedef struct
{
  q_link_type          link;         /*!<  q link needed for queue operation */
  uint16               sn;           /*!<  sequence number */
  uint16               so_start;     /*!<  start offset of the hole */
  uint16               so_end;       /*!<  end offset of the hole   */
} lte_rlcdl_nak_element_s;

/*==============================================================================

                    EXTERNAL FUNCTION PROTOTYPES

==============================================================================*/

extern void lte_rlcdl_nak_init( void );

extern void lte_rlcdl_nak_deinit(void);

extern boolean lte_rlcdl_nak_resource_freed( void );

extern lte_rlcdl_nak_element_s* lte_rlcdl_nak_get_elem( void );

extern void lte_rlcdl_nak_free_elem
( 
  lte_rlcdl_nak_element_s*     nak_elem_ptr
);

extern void lte_rlcdl_nak_append_pdu_to_chain
(
  uint16                       sn
);

extern void lte_rlcdl_nak_append_pdu_seg_to_chain
(
  uint16                       sn,
  uint16                       so_start,
  uint16                       so_end
);

extern void lte_rlcdl_nak_append_chain_to_nak_q
(
  lte_rlcdli_ctrl_blk_ptr      ctrl_blk_ptr,
  uint16                       new_vr_ms
);

extern void lte_rlcdl_nak_trim
(
  lte_rlcdli_ctrl_blk_ptr      ctrl_blk_ptr,
  const dsm_item_type*         dsm_ptr,
  uint16                       sn
);

extern uint32 lte_rlcdl_nak_build_status
(
  const lte_rlcdli_ctrl_blk_ptr      ctrl_blk_ptr,
  uint8*                             status_pdu_ptr,
  uint32                             allotment
);

extern void lte_rlcdl_nak_q_flush
(
  lte_rlcdli_ctrl_blk_ptr      ctrl_blk_ptr
);

#endif /* LTE_RLCDL_NAK_H */
