/*!
  @file
  lte_rlc_dbg.h

  @brief
  RLC debug related utilities api.

  @author
  axiao

*/

/*==============================================================================

  Copyright (c) 2012 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/L2/rlc/src/lte_rlc_dbg.h#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------------
01/23/14   mg      CR599494: RLC behavior to alleviate the low DSM count problem.
01/09/11   ax      CR326734: remove efs_get() from lte_pdcpul_log_init(), which 
                   is part of start procedure due to unpredictalbe delays
10/17/11   ax      make RLC-A2 eMBMS watermark level runtime configurable
06/26/10   ax      make AM receive window size configurable in flow control
03/26/10   ax      added lte_rlc_dbg_get_max_pending_rlcdl_sdu() api
03/18/10   ax      make profile log pkt frequency configurable
07/08/09   ax      make starting segment offset configurable for DEC08 spec
06/08/09   ax      added PDCPUL log pkt no aggregate support
06/02/09   ax      RLC debugging related utilities
==============================================================================*/

#ifndef LTE_RLC_DBG_H
#define LTE_RLC_DBG_H

/*==============================================================================

                           INCLUDE FILES

==============================================================================*/

#include <comdef.h>



/*==============================================================================

                   EXTERNAL DEFINITIONS AND TYPES

==============================================================================*/

#define LTE_RLC_DBG_FC_SHUTDOWN_ASSERT_MASK                               0x1

typedef struct
{
  uint32   wm_low;       /*!< watermark low */
  uint32   wm_hi;        /*!< watermark high */
  uint32   wm_dne;       /*!< watermark DNE */
} lte_rlc_dbg_wm_cfg_s;

/*! @brief structure that contains all debug related variables
*/
typedef struct
{
  boolean  rlcdl_log_pkt_no_aggregate; /*!< instant commit for every DL PDU*/
  boolean  rlcul_log_pkt_no_aggregate; /*!< instant commit for every UL PDU*/
  boolean  pdcpul_log_pkt_no_aggregate;/*!<instant commit for PDCPUL PDU*/
  uint8    rlc_min_so;                 /*!<RLC minimum segment offset   */
  uint32   profile_log_freq_mask;      /*!<profile logging freq mask   */
  uint32   max_pending_rlcdl_sdu;      /*!<maximum pending rlcdl sdus allowed*/
  uint16   seq_modulus_at_fc;          /*!< sequence modulus during flow control
                                            1024 for standard window size 512 */
  uint32   assert_mask;         /*!<control what condition assert is needed   */
  lte_rlc_dbg_wm_cfg_s  embms_wm_cfg;       /*!<RLC-A2 watermark configuration*/
  uint16   pdcpul_sdu_bytes_to_log;    /*!< num pdcp sdu to be logged */
  /*AM reordering timer value under low DSM count behavior*/
  uint16   fc_am_t_reordering;
  /*AM status timer value under low DSM count behavior*/
  uint16   fc_am_t_status_prohibit;
  /*UM reordering timer value under low DSM count behavior*/
  uint16   fc_um_t_reordering;  
} lte_rlc_dbg_s;

/*==============================================================================

                    EXTERNAL FUNCTION PROTOTYPES

==============================================================================*/

extern void lte_rlc_dbg_init( void );

extern boolean lte_rlc_dbg_dl_log_pkt_no_aggregate( void );

extern boolean lte_rlc_dbg_ul_log_pkt_no_aggregate( void );

extern boolean lte_rlc_dbg_pdcpul_log_pkt_no_aggregate( void );

extern uint8 lte_rlc_dbg_get_min_so( void );

extern uint32 lte_rlc_dbg_get_profile_log_freq( void );

extern uint32 lte_rlc_dbg_get_max_pending_rlcdl_sdu( void );

extern uint16 lte_rlc_dbg_get_seq_mod_at_fc( void );

extern boolean lte_rlc_dbg_assert
(
  uint32       trigger_mask
);

extern lte_rlc_dbg_s* lte_rlc_dbg_get( void );

extern uint16 lte_rlc_dbg_get_pdcpul_log_sdu_bytes(void);

#endif /* LTE_RLC_DBG_H */
