/*!
  @file
  lte_rlcdl_embms.h

  @brief
  REQUIRED brief one-sentence description of this C header file.

  @detail
  OPTIONAL detailed description of this C header file.
  - DELETE this section if unused.

  @author
  gxiao

*/

/*==============================================================================

  Copyright (c) 2013 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/L2/rlc/src/lte_rlcdl_embms.h#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------------
05/08/13   sb      CR466846: Remove assert and free the SDU if LTE_RLCDL_MCCH_PDU_IND
                   message sending fails to RRC
04/23/12   ax      CR333829: capability to deal with area_id > 8, etc
04/17/12   ru      CR 342859: Added eMBMS logging changes  
10/17/11   ax      initial version
==============================================================================*/

#ifndef LTE_RLCDL_EMBMS_H
#define LTE_RLCDL_EMBMS_H

/*==============================================================================

                           INCLUDE FILES

==============================================================================*/

#include "comdef.h"
#include "lte.h"
#include "lte_rlc_rrc.h"
#include "lte_mac_rlc.h"
#include "dsm_queue.h"
#include "lte_rlcdl_hdr.h"

/*==============================================================================

                   EXTERNAL DEFINITIONS AND TYPES

==============================================================================*/
/*! @brief maximum active MTCH + maximum active MCCH (one per area)
*/
#define LTE_RLCDL_EMBMS_MAX_MRB  (LTE_MAX_ACTIVE_MRB + LTE_EMBMS_MAX_MBSFN_AREA)

/*! @brief invalid control block index
*/
#define LTE_RLCDL_EMBMS_INVALID_CB_IDX                                 0xFF

/*! @brief invalid lc_id 
*/
#define LTE_RLCDL_EMBMS_INVALID_LC_ID                                  0XFF

/*! @brief RLC sequence length
*/
#define LTE_RLCDL_EMBMS_MRB_SN_LEN                                        5

/*! @brief total number of valid area id [0, 255]
*/
#define LTE_RLCDL_EMBMS_NUM_AREA_ID                                   256

/*! @brief MRB statistics
*/
typedef struct
{
  uint32  num_data_pdu;       /*!< number of RLC data pdu received */
  uint32  data_pdu_bytes;     /*!< RLC data PDU in bytes received */
  uint32  num_invalid_pdu;    /*!< number of invalid PDUs */
  uint32  invalid_pdu_bytes;  /*!< invalid PDU in bytes */
  uint32  num_sdu;            /*!< number of RLC sdu reassembled */
  uint32  sdu_bytes;          /*!< RLC SDUs in bytes reassembled */
  uint32  num_holes;          /*!< number of times holes are encountered */
  uint32  num_holes_discard;  /*!< number of times holes that resulted in seg discard */
  uint32  sdu_bytes_discard;  /*!< SDU bytes discarded: reassembly failure */
} lte_rlcdl_embms_stats_s;

/*! @brief eMBMS control block structure
*/
typedef struct
{
  lte_rlcdli_state_e          rb_state; /*!< state of the RB */
  lte_rlcdl_mrb_info_s        cfg;      /*!< configuration */
  uint16                      vr_r;              /*!< VR(R) or VR(UR) */
  dsm_item_type*              sdu_fragment_ptr;   /*!< residual sdu fragment */
  uint32                      sdu_fragment_sz;    /*!< size of the fragment */
  lte_rlcdl_embms_stats_s     stats;    /*!< statistics of the RB */
} lte_rlcdl_embms_cb_s;

/*! @brief eMBMS static data structure
*/
typedef struct
{
  mutils_circ_q_s*      mac_rlc_buf_ptr;  /*!< MAC-RLC circular buffer pointer */
  dsm_watermark_type    rlc_a2_wm;        /*!< RLC-A2 watermark */
  q_type                rlc_a2_q;         /*!< RLC-A2 queue */
  lte_rlcdl_embms_cb_s  mcch_cb[LTE_EMBMS_MAX_MBSFN_AREA]; /*!< MCCH control block */
  boolean               notify_a2;        /*!< whether A2 needs to be notified */
  lte_rlcdl_mcch_pdu_ind_s rrc_ind;       /*!< Indicating MCCH msg for RRC */
  uint8                 num_active_mrb;   /*!< number of active mrb */
  uint32                mcch_cb_idx;      /*!< avaiable mcch control block index */
  uint32                num_embms_sdu_drop;  /*!<num of times LTE_RLCDL_MCCH_PDU_IND msg 
                                             sending fails */
} lte_rlcdl_embms_static_s;

/*! @brief eMBMS dynamic data structure
*/
typedef struct
{
  lte_rlcdl_embms_cb_s mtch_cb[LTE_MAX_ACTIVE_MRB]; /*!< MTCH control block */
} lte_rlcdl_embms_dynamic_s;
/*==============================================================================

                    EXTERNAL FUNCTION PROTOTYPES

==============================================================================*/
extern void lte_rlcdl_embms_init( void );

extern lte_errno_e lte_rlcdl_embms_proc_cfg
(
  const lte_rlcdl_cfg_req_s*   msg_ptr  
);

extern void lte_rlcdl_embms_proc_release( void );

extern void lte_rlcdl_embms_proc_pdus( void );

extern dsm_watermark_type* lte_rlcdl_embms_get_a2_wm( void );

extern lte_rlcdl_embms_dynamic_s* lte_rlcdl_embms_get_embms_data
(
  void
);

extern uint8 lte_rlcdl_embms_get_num_active_mrb( void );

#endif /* LTE_RLCDL_EMBMS_H */
