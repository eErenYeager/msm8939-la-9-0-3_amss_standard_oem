/*!
  @file
  lte_l1_ftm.h

  Interface for FTM to use L1 to tune

*/

/*===========================================================================

  Copyright (c) 2008 - 2011 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: 

when       who     what, where, why
--------   ---     ----------------------------------------------------------

===========================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

#include "lte_cphy_irat_meas_msg.h"

#ifndef _LTE_ML1_COMMON_FTM_H_
#define _LTE_ML1_COMMON_FTM_H_

/*===========================================================================

                   INTERNAL DEFINITIONS AND TYPES

===========================================================================*/

/*===========================================================================

                         LOCAL VARIABLES

===========================================================================*/

/*===========================================================================

                    INTERNAL FUNCTION PROTOTYPES

===========================================================================*/

/*===========================================================================

                             INTERNAL FUNCTIONS

===========================================================================*/

/*===========================================================================

  FUNCTION:  lte_l1_ftm_tune
===========================================================================*/
/*!
    @brief 
    LTE L1 will tune, using the FW via scripts.

    The L1 FTM module must be initialized via lte_l1_ftm_init() prior to 
    calling this function.  FW will capture samples during this process
    which can be dumped and extracted for sanity purposes.

    All other RATs must release the FW resources required for IRAT
    prior to calling this function.  The RF and clocks will be in LTE
    mode following the execution of this function.

    The caller of this function must have the scripts prepared in 
    the RF tune buffer.

    Multiple lte_l1_ftm_tune() calls can happen between init and deinit.
    
*/
/*=========================================================================*/
void lte_l1_ftm_tune
(
  /*! LTE Downlink center frequency to tune to */
  lte_earfcn_t     earfcn,
  /*! RF tune script index */
  uint8            rf_tune_script_index,
  /*! Rxlm buffer for antenna 0, ignored for non-rxlm targets */
  uint8            rxlm_buf_ant0,
  /*! Rxlm buffer for antenna 1, ignored for non-rxlm targets */
  uint8            rxlm_buf_ant1
);

/*===========================================================================

  FUNCTION:  lte_l1_ftm_init
===========================================================================*/
/*!
    @brief 
    Initializes the L1 FTM module.

    Init must be called prior to tuning via lte_l1_ftm_tune().  
    lte_l1_ftm_deinit() must be called prior to calling lte_l1_ftm_init()
    again.  
    
    On genesis init will configure some LTE-only clocks, these should not
    be modified prior to lte_l1_ftm_deinit() being called
*/
/*=========================================================================*/
void lte_l1_ftm_init( void );

/*===========================================================================

  FUNCTION:  lte_l1_ftm_deinit
===========================================================================*/
/*!
    @brief 
    Deinitializes the L1 FTM module.

    The LTE app will be disabled after this function is called and 
    all LTE resource will be freed.
*/
/*=========================================================================*/
void lte_l1_deinit_req( void );

#endif

