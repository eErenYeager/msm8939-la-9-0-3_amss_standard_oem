/*!
  @file
  lte_dsm_util_ext_api.h

  @brief
  REQUIRED brief one-sentence description of this C header file.

  @detail
  OPTIONAL detailed description of this C header file.
  - DELETE this section if unused.

*/

/*==============================================================================

  Copyright (c) 2011 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/api/lte_dsm_util_ext_api.h#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------------
01/04/11   ax      Separate out what is needed by FTM to API dir
==============================================================================*/

#ifndef LTE_DSM_UTIL_EXT_API_H
#define LTE_DSM_UTIL_EXT_API_H

/*==============================================================================

                           INCLUDE FILES

==============================================================================*/

#include <comdef.h>
#include <dsm_item.h>
#include <dsm_queue.h>

/*==============================================================================

                   EXTERNAL DEFINITIONS AND TYPES

==============================================================================*/

/*==============================================================================

                    EXTERNAL FUNCTION PROTOTYPES

==============================================================================*/
/*==============================================================================

  FUNCTION:  lte_dsmi_enqueue_with_length_in_app_field()

==============================================================================*/
/*!
    @brief
    This function enqueues the DSM item to the passed watermark queue., then
    check for and trigger relevant 'put' events.

    This function doesnt check for the priority and will NOT enqueue an item
    to the front of the queue.

    @note
    Caller of this function is expected to make sure that app_field is not
    used as part of the watermark interface because this function will over-
    write the same with 'item_length'.

    'item_length' can possibly be set to user defined length which may or may
    not be equal to the enqueued dsm packet length.

    If enqueue fails then this API will release the passed DSM item and set the
    passed *pkt_ptr to NULL.

    @see related_function()
    lte_dsmi_dequeue_with_length_in_app_field() - "app_field" of the first
    dsm item of the dequeued packet will be used for updating the watermark
    count whenever an item is dequeued from the watermark

    @return
    TRUE or FALSE based on enqueue status
    This function also sets the passed *pkt_ptr to NULL.
*/
/*============================================================================*/
#define lte_dsm_enqueue_with_length_in_app_field(wm_ptr, pkt_head_ptr, \
  item_length) lte_dsmi_enqueue_with_length_in_app_field((wm_ptr), \
  (pkt_head_ptr), (item_length), __FILE__, __LINE__)

boolean lte_dsmi_enqueue_with_length_in_app_field
(
  /*! Pointer to Watermark item to put to */
  dsm_watermark_type *wm_ptr,
  /*! Pointer to pointer to item to add to queue */
  dsm_item_type      **pkt_head_ptr,
  /*! length of the DSM item chain (may include length which may be part of
      the meta info associated with the dsm ptr to be enqueued). */
  uint32             item_length,
  /*! file pointer from where this function is called */
  const char         *file,
  /*! line number from where this function is called */
  uint32             line
);

/*==============================================================================

  FUNCTION:  lte_dsm_dequeue_with_length_in_app_field()

==============================================================================*/
/*!
    @brief
    This function dequeues and return a pointer to the next item on the
    watermark queue associated with the passed watermark item.

    This function will also update the 'current_cnt' field using "app_field" of
    the first item of the dequeued dsm packet.

    After updating the count, it checks for and perform any relevent 'get'
    events.

    @note
    The parameter must NOT be NULL.

    @return
    A pointer to a 'dsm_item_type' or NULL if no item_array available.
*/
/*============================================================================*/
#define lte_dsm_dequeue_with_length_in_app_field(wm_ptr) \
  lte_dsmi_dequeue_with_length_in_app_field((wm_ptr), __FILE__, __LINE__)

dsm_item_type *lte_dsmi_dequeue_with_length_in_app_field
(
  /*! Pointer to watermark item to get item from */
  dsm_watermark_type *wm_ptr,
  /*! file pointer from where this function is called */
  const char        *file,
  /*! line number from where this function is called */
  uint32            line
);


#endif /* LTE_DSM_UTIL_EXT_API_H */
