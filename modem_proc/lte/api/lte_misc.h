/*!
  @file lte_misc.c
  

  @brief
  REQUIRED This file has all Miscellaneous routine used by lte modules 

  @detail
  OPTIONAL detailed description of this C module.
  - DELETE this section if unused.

*/

/*=============================================================================

  Copyright (c) 2012 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/api/lte_misc.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
10/31/11   kp      Added code to get UE category from API lte\common
10/31/11   kp      Initial Version.
=============================================================================*/

#ifndef LTE_MISC_H
#define LTE_MISC_H

/*=============================================================================

                   INCLUDE FILES

=============================================================================*/

#include <comdef.h>
#include <IxErrno.h>

/*=============================================================================

                   EXTERNAL CONSTANT/MACRO DEFINITIONS

=============================================================================*/


  /*8x30 JTAG_ID_1 0x0728 :- MM LTE Cat2, HSPA+, DOrB, UMTS*/
  #define LTE_RRC_CAP_8x30_CAT2_JTAG_ID_1 0x0728
  /*8x30 JTAG_ID_2 0x0729 :- MM LTE Cat2, HSPA+, DOrB, UMTS, HDCP*/
  #define LTE_RRC_CAP_8x30_CAT2_JTAG_ID_2 0x0729
  /*8x30 JTAG_ID_3 0x072A :- 3GPP LTE Cat2, HSPA+, TD-SCDMA, UMTS*/
  #define LTE_RRC_CAP_8x30_CAT2_JTAG_ID_3 0x072A
  /*8x30 JTAG_ID_4 0x072B :- 3GPP LTE Cat2, HSPA+, TD-SCDMA, UMTS, HDCP*/
  #define LTE_RRC_CAP_8x30_CAT2_JTAG_ID_4 0x072B
  /*! @brief UE Category -2
  */
  #define LTE_UE_CATEGORY2   2


/*=============================================================================

                   EXTERNAL ENUMERATION TYPES

=============================================================================*/

/*=============================================================================

                   EXTERNAL STRUCTURE/UNION TYPES

=============================================================================*/



/*=============================================================================

                   DATA TYPES

=============================================================================*/

/*! @brief REQUIRED brief description of this structure typedef
  This type is used for returning ue_category
*/
typedef struct
{
  /*Jtag I.D used for debugging only*/
  uint32 debug_pnId;
  
  boolean use_cat_2;   /*!< UE category */
} lte_ue_category_info_s;

/*===========================================================================

                    EXTERNAL FUNCTION PROTOTYPES

===========================================================================*/
/*===========================================================================

  FUNCTION:  lte_get_ue_category

===========================================================================*/
/*!
  @brief
   this function will return ue_category
  
  @return
  ue_category - 2 or 3

*/
/*=========================================================================*/
uint8 lte_get_ue_category
(
  void
);
/*===========================================================================

  FUNCTION:  lte_get_current_time

===========================================================================*/
/*!
    @brief
    Get current time in ms since power up

    @details
    Get current time in ms since power up
    This API will be removed once all SW is migrated to use one of the two
    below APIs

    @return
    None
*/
/*=========================================================================*/
uint64 lte_get_current_time(void);

/*===========================================================================

  FUNCTION:  lte_get_current_time_since_power_up_ms

===========================================================================*/
/*!
    @brief
    Get current time in ms since power up

    @details
    Get current time in ms since power up

    @return
    None
*/
/*=========================================================================*/
/* Current time since power up in ms */
uint64 
lte_get_current_time_since_power_up_ms(void);

/*===========================================================================

  FUNCTION:  lte_get_current_time_since_power_up_secs

===========================================================================*/
/*!
    @brief
    Get current time in secs since power up

    @details
    Get current time in secs since power up

    @return
    None
*/
/*=========================================================================*/
/* Current time since power up in secs */
uint32 
lte_get_current_time_since_power_up_secs(void);

/*===========================================================================

  FUNCTION:  lte_set_current_time

===========================================================================*/
/*!
    @brief
    Set current time in ms since power up

    @details
    Set current time in ms since power up

    @return
    None
*/
/*=========================================================================*/
void lte_set_current_time(uint64 cur_time);

/*===========================================================================

  FUNCTION:  lte_is_ca_feature_enabled

===========================================================================*/
/*!
    @brief  
    if DALCHIPINFO_ID_MDM9225_1
      then CA is disabled
    else
      CA is Enabled  

    @return
      boolean  
*/
/*=========================================================================*/
extern boolean lte_is_ca_feature_enabled 
(
  void
);

#endif /* LTE_MISC_H */

