#ifndef __VOICEAPP_API_H__
#define __VOICEAPP_API_H__

/*
   Copyright (C) 2014 QUALCOMM Technologies, Inc.
   All Rights Reserved.
   Qualcomm Technologies, Inc. Confidential and Proprietary.

   $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/avs/vsd/inc/protected/voiceapp.h#1 $
   $Author: mplp4svc $
*/

#include "apr_comdef.h"
#include "apr_timer.h"
#include "drv_api.h"
#include "tdsl2external.h"
#include "wcdmamvsif.h"
#include "wcdma_ext_api.h"

/****************************************************************************
 * DEFINES                                                                  *
 ****************************************************************************/

/* Size of Voiceapp Heap in bytes, making it 16 byte aligned.
 * Sizeof of Heap is reserved to support 8 voicapp_objects.   
 */
#define VOICEAPP_HEAP_SIZE_V ( 768 )
#define VOICEAPP_HANDLE_TOTAL_BITS_V ( 16 )
#define VOICEAPP_HANDLE_INDEX_BITS_V ( 3 ) 

/* Keeping maximum supported Voiceapp object to 8.
 */
#define VOICEAPP_MAX_OBJECTS_V ( 1 << VOICEAPP_HANDLE_INDEX_BITS_V )

/* Size is in bytes, size is of AMR Core Frame( speech or comfort noice data), which 
 * includes size of class A, class B and class C bits. 
 * Note: Maximum AMR frame size is 31 bytes, but padded it to 34 bytes for legacy reasons. 
 * Specs : 3GPP TS 26.101 and 3GPP TS 26.201.
 */
#define VOICEAPP_AMR_FRAME_SIZE_V ( 34 )
#define VOICEAPP_AMRWB_FRAME_SIZE_V ( 60 )

/****************************************************************************
 * DEFINITIONS                                                              *
 ****************************************************************************/
/**
 * Defined media types.
 */
 
/* Adaptive Multi-Rate. 
 * This Guid is same as of CVD media_id  Guid */
#define VOICEAPP_MEDIA_ID_AMR ( 0x00010FC6 ) 
/* Adaptive Multi-Rate. 
 * This Guid is same as of CVD media_id Guid. */
#define VOICEAPP_MEDIA_ID_AMRWB ( 0x00010FC7 )
/* Enhanced Adaptive Multi-Rate. 
 * This Guid is same as of CVD media_id Guid. */
#define VOICEAPP_MEDIA_ID_EAMR ( 0x00010FD4 )


/* Network types in Voiceapp.
 */
typedef enum {
   VOICEAPP_NETWORK_WCDMA = 0,
   VOICEAPP_NETWORK_TDSCDMA = 1   
} voiceapp_network_type_t;
  
/**
 * Defined AMR-NB frame type. 
 */
typedef enum voiceapp_amr_frame_type_t
{
  VOICEAPP_AMR_FRAME_TYPE_SPEECH_GOOD = 0,
      /**< Good speech frame. */
  VOICEAPP_AMR_FRAME_TYPE_SPEECH_DEGRADED = 1,
      /**< Degraded speech frame. */
  VOICEAPP_AMR_FRAME_TYPE_ONSET = 2,
      /**<
        * ONSET - Announces the beginning of speech burst.
        * Does not contain information bits.
        */
  VOICEAPP_AMR_FRAME_TYPE_SPEECH_BAD = 3,
      /**< Corrupt speech frame (bad CRC). */
  VOICEAPP_AMR_FRAME_TYPE_SID_FIRST = 4,
      /**<
        * Marks the beginning of a comfort noise period (silence).
        * Does not contain information bits. /
        */
  VOICEAPP_AMR_FRAME_TYPE_SID_UPDATE = 5,
      /**<
        * Comfort noise frame.
        * It does contain information bits.
        */
  VOICEAPP_AMR_FRAME_TYPE_SID_BAD = 6,
      /**< Comfort noise frame (bad crc).    */
  VOICEAPP_AMR_FRAME_TYPE_NO_DATA = 7
      /**<
        *  No useful information.
        *  Nothing to transmit.
        */
}
  voiceapp_amr_frame_type_t;

/**
 * Defined AMR-WB frame type. 
 */
typedef enum voiceapp_amrwb_frame_type_t
{
  VOICEAPP_AMRWB_FRAME_TYPE_SPEECH_GOOD = 0,
      /**< Good speech frame. */
  VOICEAPP_AMRWB_FRAME_TYPE_SPEECH_DEGRADED = 1,
      /**< Degraded speech frame. */
  VOICEAPP_AMRWB_FRAME_TYPE_ONSET = 2,
      /**<
        * ONSET - Announces the beginning of speech burst.
        * Does not contain information bits.
        */
  VOICEAPP_AMRWB_FRAME_TYPE_SPEECH_BAD = 3,
      /**< Corrupt speech frame (bad CRC). */
  VOICEAPP_AMRWB_FRAME_TYPE_SID_FIRST = 4,
      /**<
        * Marks the beginning of a comfort noise period (silence).
        * Does not contain information bits. /
        */
  VOICEAPP_AMRWB_FRAME_TYPE_SID_UPDATE = 5,
      /**<
        * Comfort noise frame.
        * It does contain information bits.
        */
  VOICEAPP_AMRWB_FRAME_TYPE_SID_BAD = 6,
      /**< Comfort noise frame (bad crc).    */
  VOICEAPP_AMRWB_FRAME_TYPE_NO_DATA = 7
      /**<
        *  No useful information.
        *  Nothing to transmit.
        */
}
  voiceapp_amrwb_frame_type_t;

/* Supported AMR rates.
 */
typedef enum 
{
  VOICEAPP_AMR_CODEC_MODE_0475 = 0,    /* AMR-NB 4.75 kbit /s */
  VOICEAPP_AMR_CODEC_MODE_0515 = 1,    /* AMR-NB 5.15 kbit /s */
  VOICEAPP_AMR_CODEC_MODE_0590 = 2,    /* AMR-NB 5.90 kbit /s */
  VOICEAPP_AMR_CODEC_MODE_0670 = 3,    /* AMR-NB 6.70 kbit /s */
  VOICEAPP_AMR_CODEC_MODE_0740 = 4,    /* AMR-NB 7.40 kbit /s */
  VOICEAPP_AMR_CODEC_MODE_0795 = 5,    /* AMR-NB 7.95 kbit /s */
  VOICEAPP_AMR_CODEC_MODE_1020 = 6,    /* AMR-NB 10.2 kbit /s */
  VOICEAPP_AMR_CODEC_MODE_1220 = 7     /* AMR-NB 12.2 kbit /s */
} 
  voiceapp_amr_codec_mode_type_t;

/* Supported AMR-WB rates.
 */
typedef enum 
{
  VOICEAPP_AMRWB_CODEC_MODE_0660 = 0,    /* AMR-WB 6.60 kbps */
  VOICEAPP_AMRWB_CODEC_MODE_0885 = 1,    /* AMR-WB 8.85 kbps */
  VOICEAPP_AMRWB_CODEC_MODE_1265 = 2,    /* AMR-WB 12.65 kbps */
  VOICEAPP_AMRWB_CODEC_MODE_1425 = 3,    /* AMR-WB 14.25 kbps */
  VOICEAPP_AMRWB_CODEC_MODE_1585 = 4,    /* AMR-WB 15.85 kbps */
  VOICEAPP_AMRWB_CODEC_MODE_1825 = 5,    /* AMR-WB 18.25 kbps */
  VOICEAPP_AMRWB_CODEC_MODE_1985 = 6,    /* AMR-WB 19.85 kbps */
  VOICEAPP_AMRWB_CODEC_MODE_2305 = 7,    /* AMR-WB 23.05 kbps */
  VOICEAPP_AMRWB_CODEC_MODE_2385 = 8     /* AMR-WB 23.85 kbps */
} 
  voiceapp_amrwb_codec_mode_type_t;


/* This callback indicates the event call back from Voiceapp to modem
 * voice clients which have registered for modem timings.
 */
typedef void ( *voiceapp_modem_timing_callback_fn_t ) ( void* client_data ); 

/* This function will be registered to voiceapp by client of voiceapp, 
 * voiceapp will be call this function with vsid and client should then call 
 * function voiceapp_process_command in its own context. 
 */
typedef uint32_t ( *voiceapp_queue_cmd_to_client_fn_t ) ( uint32_t vsid );

/* WCDMA timing information, connection frame number and stmr_tick_count. 
 * See comments on voiceapp_register_wcdma_timing_reference.
 */
typedef struct voiceapp_wcdma_timing_param_t voiceapp_wcdma_timing_param_t;
struct voiceapp_wcdma_timing_param_t
{
  uint8_t cfn;
  uint8_t stmr_tick_count;
};

/* TDSCDMA timing information, see comments  for
 * voiceapp_register_tdscma_timing_reference.
 */
typedef struct voiceapp_tdscdma_timing_param_t voiceapp_tdscdma_timing_param_t;
struct voiceapp_tdscdma_timing_param_t
{
  uint32_t tds_sys_time;
};

/* Voiceapp object will hold timing information corrosponding to each network. 
 * At present only supported networks are TDSCDMA and WCDMA. 
 */
typedef union voiceapp_timing_param_t voiceapp_timing_param_t;
union voiceapp_timing_param_t
{
  voiceapp_wcdma_timing_param_t wcdma;
  voiceapp_tdscdma_timing_param_t tdscdma;  
};

/* Information specific to AMR Media ID.
 */ 
typedef struct voiceapp_amr_frame_info_t voiceapp_amr_frame_info_t;
struct voiceapp_amr_frame_info_t
{
  /* rates for AMR */
  voiceapp_amr_codec_mode_type_t codec_mode;
  /* frame type for AMR */
  voiceapp_amr_frame_type_t frame_type; 
};

/* Information specific to AMR-WB Media ID. 
 */ 
typedef struct voiceapp_amrwb_frame_info_t voiceapp_amrwb_frame_info_t;
struct voiceapp_amrwb_frame_info_t
{
  /* rates for AMR-WB. */
  voiceapp_amrwb_codec_mode_type_t codec_mode;
  /* frame type for AMR-WB. */
  voiceapp_amrwb_frame_type_t frame_type; 
};

/* Header of voiceapp object contains vsid and handle. */
typedef struct voiceapp_object_header_t voiceapp_object_header_t;
struct voiceapp_object_header_t
{
  uint32_t vsid;
  /* Voice Sub-system ID. */
  uint32_t handle;
  /* The handle to the associated apr_objmgr_object_t instance. */
};

/* voiceapp object structure */
typedef struct voiceapp_object_t voiceapp_object_t;
struct voiceapp_object_t
{
  /* Header of Voiceapp object. */
  voiceapp_object_header_t header;
  /* Client of voiceapp would rerister this function, voiceapp would call this function with the vsid
   * and client should then call voiceapp_process_command, in its own context with the same vsid. 
   */
  voiceapp_queue_cmd_to_client_fn_t queue_cmd_to_client_fn;
  /* Network supported by Voiceapp. */
  voiceapp_network_type_t network;
  /* Timing parameters corrosponding to network types. */
  voiceapp_timing_param_t timing_param;
  /* time in nsec */
  uint64_t rx_timing_offset;
  /* time in nsec */
  uint64_t tx_timing_offset;
  /* rx_timing_cb will be called with rx_timing_offset */
  voiceapp_modem_timing_callback_fn_t rx_timing_cb;
  /* tx_timing_cb will be called with tx_timing_offset */
  voiceapp_modem_timing_callback_fn_t tx_timing_cb;
  /* timer for rx callback */ 
  apr_timer_t rx_timer;
  /* timer for tx callback */
  apr_timer_t tx_timer;
};

/****************************************************************************
 * FORWARD PROTOTYPES                                                       *
 ****************************************************************************/

/**
  This function is used by modem client to register their Rx and Tx callback functions with Voiceapp.
  Voiceapp will trigger the rx_timing_cb function with rx_timing_offset offset from VFR and
  tx_timing_cb with tx_timing_offset offset from VFR. 
  
  Follwoing is the way to interpret information provided by WCDMA and set our timing offset. 
  a) WCDMA send CFN ( connection frame number ) and stmr_tick_value,
     WCDMA picks packet from RLC DSM queue on even CFN.
  b) CFN range from 0-255. 
  c) 150 stmr_time_tick equals 1 cfn. 
  d) WCDMA L1 picks up vocoder packets from DSM queue at 30th slot. 
  e) Ideally stmr_time_tick value should be 0, when calling this function. But, due to the modem latencies
     and delay in reading from stmr block these are not 0. 
  f) So we should program our timer with offset  ( 30 - stmr_tick_value - buffer_offset )*(1/15)msec.
  g) buffer_offset is needed, to account for any delay in putting the vocoder packet to DSM queue.  

  Voiceapp allows only one registration per VSID. 

  @par Payload
  @param[in] vsid : Voice sub-system id. Must register with unique VSID. 
  @param[in] queue_cmd_to_client_fn : voiceapp would call this function to move processing to client context.
  @param[in] rx_timing_cb : rx_timing_cb to be trigerred at rx_timing_offset from VFR. 
  @param[in] tx_timing_cb : tx_timing_cb to be trigerred at tx_timing_offset from VFR.

  @return 
  APR_EOK on success.

  @dependencies
  none.

*/
APR_INTERNAL uint32_t voiceapp_register_wcdma_timing_reference
(
  uint32_t vsid,
  /* Voiceapp would use this function to do processing in client context. It will call this function
   * with the vsid, and then client should call voiceapp_process_command in its own context.
   */
  voiceapp_queue_cmd_to_client_fn_t queue_cmd_to_client_fn,
  /* rx_timing_cb will be called with rx_timing_offset */
  voiceapp_modem_timing_callback_fn_t rx_timing_cb,
  /* tx_timing_cb will be called with tx_timing_offset */
  voiceapp_modem_timing_callback_fn_t tx_timing_cb
);

/**
  This function is used by modem client to register their Rx and Tx callback functions with Voiceapp.
  Voiceapp will trigger the rx_timing_cb function with offset of rx_timing_offset from VFR and
  tx_timing_cb with tx_timing_offset offset from VFR. 
  Following is the way to interpret TDSCDMA timing information and calculate our timer offset. 

   1 radio frame of TDSCDMA is 10ms.
   1 radio frame consists of 2 sub-frames of 5ms each.
   TDSCDMA give us the dump of STMR timer, it is a 32-bit number. 
   We can interpret it as follows : 

   a) Bits - 28:16
      These bits represents sub-frame count
      Range is from 0 to 8191 (SFN*2 = sub-frame count; SFN range is from 0 to 4095)
      Sub-frame count value is divisible by 4 (mod 4) whenever TDSCDMA invokes the 
      timing callback function. We get callback at every 20 msec. 

   b) Bits � 15:0
      Each 5ms sub-frame equals 6400 chips. However, chip count is represented here in terms of x8,
      That means 1 chip count is represented as 8. 6400 will be represented as 6400*8 = 51200.
      So, the range is from 0 to 51199. Based off this information, you can see 1 us = 10.24 chips.
      or 5000 us (5 ms) = 5000*10.24 = 51200
 
  TDSCDMA picks UL vocoder packet at 1 msec from DSM queue. We will program our timers accordingly
  cosidering VFR latencies.

  Voiceapp allows only one registration per VSID. 
 
  @par Payload
  @param[in] vsid : Voice sub-system id. Must register with unique VSID. 
  @param[in] queue_cmd_to_client_fn : voiceapp would call this function to move processing to client context.
  @param[in] rx_timing_cb : rx_timing_cb to be trigerred at rx_timing_offset from VFR. 
  @param[in] tx_timing_cb : tx_timing_cb to be trigerred at tx_timing_offset from VFR.
 
  @return 
  APR_EOK on success.

  @dependencies
  none.

*/
APR_INTERNAL uint32_t voiceapp_register_tdscdma_timing_reference
(
  uint32_t vsid,
  /* Voiceapp would use this function to do processing in client context. It will call this function
   * with the vsid, and then client should call voiceapp_process_command in its own context.
   */
  voiceapp_queue_cmd_to_client_fn_t queue_cmd_to_client_fn,
  /* rx_timing_cb will be called with rx_timing_offset */
  voiceapp_modem_timing_callback_fn_t rx_timing_cb,
  /* tx_timing_cb will be called with tx_timing_offset */
  voiceapp_modem_timing_callback_fn_t tx_timing_cb
);

/**
  Functions takes media_id and meta-data of media_id and copies the corresponding 
  homing vector to frame location. 
 
  @par Payload

  INPUT PARAM:
  @param[in] media_id : Supported media id is VOICEAPP_MEDIA_ID_AMR and VOICEAPP_MEDIA_ID_AMRWB. 
  @param[in] frame_info: media_id determines the frame_info, it contains meta-data for media_id.
  @param[in,out] frame : Location at which ( header + homing frame ) has to be copied. 
  @param[in,out] frame_size: Size of ( header + homing frame ) that was copied to location frame. 
  
  @return 
  APR_EOK on success.

  @dependencies
  none

*/
APR_INTERNAL uint32_t voiceapp_get_homing_frame 
(
  uint32_t media_id,
  void* frame_info,
  uint8_t* frame,
  uint32_t* frame_size
);


/* This function is called in client context( mvs/vs ), voiceapp should have internal 
 * states corrosponding to vsid and should do all its processing in client context. 
 */
APR_INTERNAL uint32_t voiceapp_process_command
(
  uint32_t vsid
);

/**
  
  Issues a command to VOICEAPP.
 
  @param[in] cmd_id       Command ID.
  @param[in,out] params   Command payload.
  @param[in] size         Size of the command payload.
   
  Only supported cmd_id is DRV_CMDID_INIT and DRV_CMDID_DEINIT.
  
  @return
  APR_EOK when successful.
 
  @dependencies
  None.

  @comments
  None.


*/
APR_INTERNAL uint32_t voiceapp_call
(
  uint32_t cmd_id,
  void* params,
  uint32_t size
);

#endif  /* __VOICEAPP_API_H__ */
