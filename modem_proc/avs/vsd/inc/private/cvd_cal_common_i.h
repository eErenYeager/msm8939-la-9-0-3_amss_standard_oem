#ifndef __CVD_CAL_COMMON_I_H__
#define __CVD_CAL_COMMON_I_H__

/*
   Copyright (C) 2012 Qualcomm Technologies Incorporated.
   All rights reserved.
   QUALCOMM Proprietary/GTDR.

   $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/avs/vsd/inc/private/cvd_cal_common_i.h#1 $
   $Author: mplp4svc $
*/

/****************************************************************************
 * INCLUDE HEADER FILES                                                     *
 ****************************************************************************/

#include "apr_comdef.h"
#include "apr_memmgr.h"

/****************************************************************************
 * DEFINES                                                                  *
 ****************************************************************************/

#define CVD_CAL_QUERY_MATCH ( 0 )
#define CVD_CAL_QUERY_NOT_MATCH ( 1 )

/****************************************************************************
 * COMMON UTILITIES                                                         *
 ****************************************************************************/

/* Get width in bytes of a data type. */
APR_INTERNAL uint32_t cvd_cal_get_type_width (
  uint32_t data_type
);

/* Get the size/data pair information from an entry in table,
   return the total size of this entry. */
APR_INTERNAL uint32_t cvd_cal_get_entry_info (
  cvd_cal_table_handle_t* table_handle,
  uint8_t* cur_ptr,
  cvd_cal_entry_t* entry_item
);

/* Get the meta size (size excluding the actual data and data size)
   of a cal entry in table. */
APR_INTERNAL int32_t cvd_cal_get_entry_meta_size ( 
  cvd_cal_column_descriptor_t* column_descriptor,
  uint32_t num_columns,
  uint32_t* ret_size 
);

/* Get number of cal entries in from a cal table. */
APR_INTERNAL int32_t cvd_cal_get_num_cal_entries (
  cvd_cal_table_descriptor_t* cal_table_descriptor,
  uint32_t* ret_num_cal_entries
);

/* Form a new key by re-ordering a query key. 
   Columns in new key has the same order as in calibration table. 
   N/A values are filled into new key if the original query 
   key does not have all the columns present. */
APR_INTERNAL int32_t cvd_cal_reorder_key (
  cvd_cal_table_descriptor_t* table_descriptor,
  cvd_cal_key_t* orig_key,
  cvd_cal_key_t* ret_key,
  uint8_t* column_buf
 );

#endif /* __CVD_CAL_COMMON_I_H__ */
