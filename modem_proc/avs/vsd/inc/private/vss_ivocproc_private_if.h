#ifndef __VSS_IVOCPROC_PRIVATE_IF_H__
#define __VSS_IVOCPROC_PRIVATE_IF_H__

/*
   Copyright (C) 2010-2012 Qualcomm Technologies Incorporated.
   All rights reserved.
   QUALCOMM Proprietary/GTDR.

   $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/avs/vsd/inc/private/vss_ivocproc_private_if.h#1 $
   $Author: mplp4svc $
*/


#include "apr_comdef.h"
#include "vss_public_if.h"


/****************************************************************************
 * CORE VOCPROC APIS                                                        *
 ****************************************************************************/

#define VSS_IVOCPROC_CMD_MVM_ENABLE ( 0x00011187 )
  /**< No payload. Wait for APRV2_IBASIC_RSP_RESULT response. */


#define VSS_IVOCPROC_CMD_MVM_DISABLE ( 0x00011188 )
  /**< No payload. Wait for APRV2_IBASIC_RSP_RESULT response. */


#define VSS_IVOCPROC_CMD_ATTACH_STREAM ( 0x000110F0 )
   /**<
    * Attach a stream to the vocproc.
    * If command is successful the reply will be
    * VSS_IVOCPROC_ATTACH_STREAM_RSP with VPM handle. If command fails
    * the reply will be APRV2_IBASIC_RSP_RESULT with failure code.
    */

typedef struct vss_ivocproc_cmd_attach_stream_t vss_ivocproc_cmd_attach_stream_t;

#include "apr_pack_begin.h"
struct vss_ivocproc_cmd_attach_stream_t
{
   uint16_t direction;
      /**<
       * Stream direction.
       *
       * 0 : TX only
       * 1 : RX only
       * 2 : TX and RX
       */
}
#include "apr_pack_end.h"
;

#define VSS_IVOCPROC_RSP_ATTACH_STREAM ( 0x000110F1 )
   /**< Response to VSS_IVOCPROC_CMD_ATTACH_STREAM. */

typedef struct vss_ivocproc_rsp_attach_stream_t vss_ivocproc_rsp_attach_stream_t;

#include "apr_pack_begin.h"
struct vss_ivocproc_rsp_attach_stream_t
{
   uint16_t vdsp_session_handle;
      /**< Handle to the VDSP session. */
   uint16_t direction;
      /**<
       * Vocproc direction.
       *
       * 0 : TX only
       * 1 : RX only
       * 2 : TX and RX
       */
}
#include "apr_pack_end.h"
;


#define VSS_IVOCPROC_CMD_DETACH_STREAM ( 0x000110F2 )
   /**< No payload. Wait for APRV2_IBASIC_RSP_RESULT response. */


/****************************************************************************
 * CALIBRATION DATA APIS                                                    *
 ****************************************************************************/

/**
  Evaluates the amount of memory required by the vocproc from the client for
  using a faster-than-linear search algorithm for looking up entries in the
  client-registered calibration data tables.

  @par Payload
  #vss_ivocproc_cmd_eval_cal_indexing_mem_size_t

  @return
  #VSS_IVOCPROC_RSP_EVAL_CAL_INDEXING_MEM_SIZE -- In case of success.\n
  #APRV2_IBASIC_RSP_RESULT -- In case of failure.

  @dependencies
  The calibration data table memory provided here must first be mapped with CVD
  by following @latexonly \hyperref[shmem_guideline]{Shared Memory Usage
  Guideline} @endlatexonly.

  @comments
  None.
*/
#define VSS_IVOCPROC_CMD_EVAL_CAL_INDEXING_MEM_SIZE ( 0x00011356 )

/** @brief Type definition for #vss_ivocproc_cmd_eval_cal_indexing_mem_size_t.
*/
typedef struct vss_ivocproc_cmd_eval_cal_indexing_mem_size_t vss_ivocproc_cmd_eval_cal_indexing_mem_size_t;

#include "apr_pack_begin.h"

/** @brief Payload structure for the
    #VSS_IVOCPROC_CMD_EVAL_CAL_INDEXING_MEM_SIZE command.

    Expected buffer format: Must match the format specified in the
    #VSS_IVOCPROC_CMD_REGISTER_CALIBRATION_DATA_V3 command or the
    #VSS_IVOCPROC_CMD_REGISTER_VOLUME_CALIBRATION_DATA_V2 command depending on
    the type of calibration data table being evaluated.
*/
struct vss_ivocproc_cmd_eval_cal_indexing_mem_size_t
{
  uint32_t mem_handle;
    /**< Handle to the shared memory that holds the calibration data.
         See @latexonly \hyperref[shmem_guideline]{Shared Memory Usage
         Guideline} @endlatexonly. */
  uint64_t mem_address;
    /**< Location of calibration data. See @latexonly \hyperref
         [shmem_guideline]{Shared Memory Usage Guideline} @endlatexonly on how
         the address is intrepreted. */
  uint32_t mem_size;
    /**< Size of the calibration data in bytes. The amount of memory allocated
         at mem_address must meet the requirement as specified in @latexonly
         \hyperref[shmem_guideline]{Shared Memory Usage Guideline} 
         @endlatexonly. */
  uint32_t num_columns;
    /**< Number of columns used to form the key for looking up a calibration data
         entry. */
#if __STDC_VERSION__ >= 199901L
  vss_ivocproc_cal_column_t columns[];
#endif /* __STDC_VERSION__ >= 199901L */
    /**< Array of columns of size num_columns. The order in which the colums are
         provided here must match the order in which they exist in the calibration
         table provided. */
}
#include "apr_pack_end.h"
;


/**
  Response to the #VSS_IVOCPROC_CMD_EVAL_CAL_INDEXING_MEM_SIZE
  command.

  @par Payload
  #vss_ivocproc_rsp_eval_cal_indexing_mem_size_t

  @return
  None.

  @dependencies
  None.
*/
#define VSS_IVOCPROC_RSP_EVAL_CAL_INDEXING_MEM_SIZE ( 0x00011357 )

/** @brief Type definition for #vss_ivocproc_rsp_eval_cal_indexing_mem_size_t.
*/
typedef struct vss_ivocproc_rsp_eval_cal_indexing_mem_size_t vss_ivocproc_rsp_eval_cal_indexing_mem_size_t;

#include "apr_pack_begin.h"

/** @brief Payload structure for the
    #VSS_IVOCPROC_RSP_EVAL_CAL_INDEXING_MEM_SIZE command response.
*/
struct vss_ivocproc_rsp_eval_cal_indexing_mem_size_t
{
  uint32_t mem_size;
    /**< Size in bytes of the memory required by the stream from the client for
         using a faster-than-linear search algorithm for looking up entries
         in the client-registered calibration data table. */
}
#include "apr_pack_end.h"
;


/**
  Registers the memory that contains common calibration data table with
  the vocproc.

  As part of this registration, in addition to the address, size and handle
  of the memory containing the calibration table, the client also porvides
  the columns and associated column types which the vocproc uses to form a
  key that is used to index into the calibration table and look-up a matching
  entry. The columns/types must be provided in this command in the same order
  that they exist in the calibration table. If no matching entry is found,
  calibration data is not applied.

  Calibration data is applied when the vocproc transitions to the run state.

  @par Payload
  #vss_ivocproc_cmd_register_calibration_data_v3_t

  @return
  #APRV2_IBASIC_RSP_RESULT

  @dependencies
  Any memory registered here must first be mapped with CVD by following
  @latexonly \hyperref[shmem_guideline]{Shared Memory Usage Guideline}
  @endlatexonly. 

  For using a faster-than-linear search algorithm when looking up entries in
  the registered calibration data table, the client must provide cal-indexing
  memory to the vocproc. The size of the memory that the client must provide
  is queried via #VSS_IVOCPROC_CMD_EVAL_CAL_INDEXING_MEM_SIZE, and the memory
  must be readable and writable by CVD. The calibration data table used for
  evaluation must be the same as the one registered here. If the client does
  not provide cal-indexing memory to the vocproc, then linear search is 
  performed whenever calibration data must be looked up to be applied to the
  vocproc.

  @comments
  The client is not allowed to modify the calibration data memory while it
  is registered and the vocproc does not modify this memory while registered.
  The calibration data at the registered memory is valid until unregistered. 
  Registering a memory region for calibration data while another region is
  still registered causes the command to fail.

  The client is not allowed to modify the cal-indexing memory while
  it is registered with the vocproc. The vocproc modifies this memory for its
  internal purposes while it is registered.
*/
#define VSS_IVOCPROC_CMD_REGISTER_CALIBRATION_DATA_V3 ( 0x00011387 )

/** @brief Type definition for
    #vss_ivocproc_cmd_register_calibration_data_v3_t.
*/
typedef struct vss_ivocproc_cmd_register_calibration_data_v3_t vss_ivocproc_cmd_register_calibration_data_v3_t;

#include "apr_pack_begin.h"

/** @brief Payload structure for the
    #VSS_IVOCPROC_CMD_REGISTER_CALIBRATION_DATA_V3 command.

    Expected buffer format:

    @verbatim
                                          <-------- 32 bits -------->     
      -------------------------
     | column_1                | --> eg. network_id
      -------------------------
     | column_2                | --> eg. rx_sampling_rate
      -------------------------
     |  .                      |
     |	.                      |
      -------------------------
     | column_n                |
      -------------------------
     | data_size               | --> byte length of data block that follows
      -------------------------
     | data                    | -------->  -------------------------
     |                         |           | mod_id                  |
     |                         |            -------------------------
     | ...                     |           | param_id                |
      -------------------------             -------------------------
     | column_1                |           | param_size | reserved   |
      -------------------------	            -------------------------
     | column_2                |           | param_data              |
      -------------------------	           |                         |
     |  .                      |           | ...                     |
     |  .                      |            -------------------------
      -------------------------            | mod_id                  |
     | column_n                |           .                         .
      -------------------------            .                         .
     | data_size               |           .                         .
      -------------------------
     | data                    |
     |                         |
     |                         |
     | ...                     |
      -------------------------
     |                         |
     .                         .
     .                         .

    @endverbatim                                               

*/
struct vss_ivocproc_cmd_register_calibration_data_v3_t
{
  uint32_t cal_indexing_mem_handle;
    /**< Handle to the shared memory that is used by the vocproc for performing 
         faster-than-linear look-up of calibration data table entries. See 
         @latexonly \hyperref[shmem_guideline]{Shared Memory Usage Guideline} 
         @endlatexonly. Set this field to 0 if the memory is not provided. */
  uint64_t cal_indexing_mem_address;
    /**< Location of the shared memory that is used by the vocproc for performing 
         faster-than-linear look-up of calibration data table entries. See
         @latexonly \hyperref[shmem_guideline]{Shared Memory Usage Guideline}
         @endlatexonly on how the address is intrepreted. This field is ignored
         if the cal_indexing_mem_handle is 0. */
  uint32_t cal_indexing_mem_size;
    /**< Size of the shared memory that is used by the vocproc for performing 
         faster-than-linear look-up of calibration data table entries. The amount
         of memory allocated at cal_indexing_mem_handle must meet the requirement
         as specified in @latexonly \hyperref [shmem_guideline]{Shared Memory Usage
         Guideline} @endlatexonly. This field is ignored if the cal_indexing_mem_handle
         is 0. */
  uint32_t cal_mem_handle;
    /**< Handle to the shared memory that holds the per-network calibration
         data. See @latexonly \hyperref[shmem_guideline]{Shared Memory Usage
         Guideline} @endlatexonly. */
  uint64_t cal_mem_address;
    /**< Location of calibration data. See @latexonly \hyperref
         [shmem_guideline]{Shared Memory Usage Guideline} @endlatexonly on how
         the address is intrepreted. */
  uint32_t cal_mem_size;
    /**< Size of the calibration data in bytes. The amount of memory allocated
         at mem_address must meet the requirement as specified in @latexonly
         \hyperref[shmem_guideline]{Shared Memory Usage Guideline} 
         @endlatexonly. */
  uint32_t num_columns;
    /**< Number of columns used to form the key for looking up a calibration data
         entry. */
#if __STDC_VERSION__ >= 199901L
  vss_ivocproc_cal_column_t columns[];
#endif /* __STDC_VERSION__ >= 199901L */
    /**< Array of columns of size num_columns. The order in which the colums are
         provided here must match the order in which they exist in the calibration
         table provided. */
}
#include "apr_pack_end.h"
;


/**
  Registers the memory that contains the volume calibration data table with
  the vocproc.

  As part of this registration, in addition to the address, size and handle
  of the memory containing the calibration table, the client also porvides
  the columns and associated column types which the vocproc uses to form a
  key that is used to index into the calibration table and look-up a matching
  entry. The columns/types must be provided in this command in the same order
  that they exist in the calibration table. If no matching entry is found,
  calibration data is not applied.

  Calibration data is applied when the vocproc transisions to the run state
  as well as when the volume step changes during the run state.
  
  @par Payload
  #vss_ivocproc_cmd_register_volume_calibration_data_v2_t

  @return
  #APRV2_IBASIC_RSP_RESULT

  @dependencies
  Any memory registered here must first be mapped with CVD by following
  @latexonly \hyperref[shmem_guideline]{Shared Memory Usage Guideline}
  @endlatexonly. 

  For using a faster-than-linear search algorithm when looking up entries in
  the registered volume calibration table, the client must provide cal-indexing
  memory to the vocproc. The size of the memory that the client must provide
  is queried via #VSS_IVOCPROC_CMD_EVAL_CAL_INDEXING_MEM_SIZE, and the memory
  must be readable and writable by CVD. The volume calibration table used for
  evaluation must be the same as the one registered here. If the client does
  not provide cal-indexing memory to the vocproc, then linear search is 
  performed whenever volume calibration data must be looked up to be applied
  to the vocproc.

  @comments
  The client is not allowed to modify the calibration data memory while it
  is registered and the vocproc does not modify this memory while registered.
  The calibration data at the registered memory is valid until unregistered. 
  Registering a memory region for calibration data while another region is
  still registered causes the command to fail.

  The client is not allowed to modify the cal-indexing memory while
  it is registered with the vocproc. The vocproc modifies this memory for its
  internal purposes while it is registered.
*/
#define VSS_IVOCPROC_CMD_REGISTER_VOLUME_CALIBRATION_DATA_V2 ( 0x00011388 )

/** @brief Type definition for
    #vss_ivocproc_cmd_register_volume_calibration_data_v2_t.
*/
typedef struct vss_ivocproc_cmd_register_volume_calibration_data_v2_t vss_ivocproc_cmd_register_volume_calibration_data_v2_t;

#include "apr_pack_begin.h"

/** @brief Payload structure for the
    #VSS_IVOCPROC_CMD_REGISTER_VOLUME_CALIBRATION_DATA_V2 command.

    Expected buffer format.

    @verbatim
                                          <-------- 32 bits -------->     
      -------------------------
     | column_1                | --> eg. network_id
      -------------------------
     | column_2                | --> eg. rx_sampling_rate
      -------------------------
     |  .                      |
     |  .                      |
      -------------------------
     | column_n                | --> eg. volume_index
      -------------------------
     | data_size               | --> byte length of data block that follows
      -------------------------
     | data                    | -------->  -------------------------
     |                         |           | mod_id                  |
     |                         |            -------------------------
     | ...                     |           | param_id                |
      -------------------------             -------------------------
     | column_1                |           | param_size | reserved   |
      -------------------------	            -------------------------
     | column_2                |           | param_data              |
      -------------------------	           |                         |
     |  .                      |           | ...                     |
     |  .                      |            -------------------------
      -------------------------            | mod_id                  |
     | column_n                |           .                         .
      -------------------------            .                         .
     | data_size               |           .                         .
      -------------------------
     | data                    |
     |                         |
     |                         |
     | ...                     |
      -------------------------
     |                         |
     .                         .
     .                         .

   @endverbatim
*/
struct vss_ivocproc_cmd_register_volume_calibration_data_v2_t
{
  uint32_t cal_indexing_mem_handle;
    /**< Handle to the shared memory that is used by the vocproc for performing 
         faster-than-linear look-up of volume calibration table entries. See 
         @latexonly \hyperref[shmem_guideline]{Shared Memory Usage Guideline} 
         @endlatexonly. Set this field to 0 if the memory is not provided. */
  uint64_t cal_indexing_mem_address;
    /**< Location of the shared memory that is used by the vocproc for performing 
         faster-than-linear look-up of volume calibration table entries. See
         @latexonly \hyperref[shmem_guideline]{Shared Memory Usage Guideline}
         @endlatexonly on how the address is intrepreted. This field is ignored
         if the cal_indexing_mem_handle is 0. */
  uint32_t cal_indexing_mem_size;
    /**< Size of the shared memory that is used by the vocproc for performing 
         faster-than-linear look-up of volume calibrations table entries. The amount
         of memory allocated at cal_indexing_mem_handle must meet the requirement
         as specified in @latexonly \hyperref [shmem_guideline]{Shared Memory Usage
         Guideline} @endlatexonly. This field is ignored if the cal_indexing_mem_handle
         is 0. */
  uint32_t cal_mem_handle;
    /**< Handle to the shared memory that holds the volume calibration
         data. See @latexonly \hyperref[shmem_guideline]{Shared Memory Usage
         Guideline} @endlatexonly. */
  uint64_t cal_mem_address;
    /**< Location of volume calibration data. See @latexonly \hyperref
         [shmem_guideline]{Shared Memory Usage Guideline} @endlatexonly on how
         the address is intrepreted. */
  uint32_t cal_mem_size;
    /**< Size of the volume calibration data in bytes. The amount of memory allocated
         at mem_address must meet the requirement as specified in @latexonly
         \hyperref[shmem_guideline]{Shared Memory Usage Guideline} 
         @endlatexonly. */
  uint32_t num_columns;
    /**< Number of columns used to form the key for looking up a calibration data
         entry. */
#if __STDC_VERSION__ >= 199901L
  vss_ivocproc_cal_column_t columns[];
#endif /* __STDC_VERSION__ >= 199901L */
    /**< Array of columns of size num_columns. The order in which the colums are
         provided here must match the order in which they exist in the calibration
         table provided. */
}
#include "apr_pack_end.h"
;

/****************************************************************************
 * NOTIFICATIONS ISSUED BY THE VOCPROC TO ITS CLIENTS                       *
 ****************************************************************************/

#define VSS_IVOCPROC_EVT_READY ( 0x000110F3 )
   /**<
    * No payload.
    * Issued to all attached streams when vocproc transitions to run state.
    */


#define VSS_IVOCPROC_EVT_NOT_READY ( 0x000110F4 )
   /**<
    * No payload.
    * Issued to all attached streams when vocproc transitions to stop state.
    */


#define VSS_IVOCPROC_EVT_GOING_AWAY ( 0x000110F5 )
   /**<
    * No payload.
    * Issued to all attached streams when vocproc is destroyed. vocproc will
    * wait (with a timeout) for VSS_IVOCPROC_RSP_GO_AWAY response from each
    * each attached stream before destroying.
    */


#define VSS_IVOCPROC_RSP_GO_AWAY ( 0x000110F6 )
   /**<
    * No payload.
    * Issued by attached stream to this vocproc in response to
    * VSS_IVOCPROC_EVT_GOING_AWAY.
    */


#endif /* __VSS_IVOCPROC_PRIVATE_IF_H__ */

