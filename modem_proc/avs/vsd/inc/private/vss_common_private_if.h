#ifndef __VSS_COMMON_PRIVATE_IF_H__
#define __VSS_COMMON_PRIVATE_IF_H__

/*
   Copyright (C) 2010-2012 Qualcomm Technologies Incorporated.
   All rights reserved.
   QUALCOMM Proprietary/GTDR.

   $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/avs/vsd/inc/private/vss_common_private_if.h#1 $
   $Author: mplp4svc $
*/

#include "apr_comdef.h"


/****************************************************************************
 * VOICE SYSTEM CONFIG APIS                                                 *
 ****************************************************************************/

/**
  Command issued by the MVM to attached streams and vocprocs, to propagate
  the current system configuration.

  @par Payload
  #vss_icommon_cmd_set_system_config_t

  @return
  #APRV2_IBASIC_RSP_RESULT

  @dependencies
  None.
*/
#define VSS_ICOMMON_CMD_SET_SYSTEM_CONFIG ( 0x00012A0F )

/** @brief Type definition for #vss_icommon_cmd_set_system_config_t.
*/
typedef struct vss_icommon_cmd_set_system_config_t vss_icommon_cmd_set_system_config_t;

#include "apr_pack_begin.h"

/** @brief Payload structure for the #VSS_ICOMMON_CMD_SET_SYSTEM_COPNFIG command.
*/
struct vss_icommon_cmd_set_system_config_t
{
  uint32_t network_id;
    /** Network ID. Supported values: \n
        see @latexonly \hyperref[cal_network_ID]{Calibration Network IDs} @endlatexonly. */
  uint32_t voc_class;
    /** Vocoder class. Supported values: Supported values: \n
        see @latexonly \hyperref[cal_voc_class]{Calibration Vocoder Classes} @endlatexonly. */
  uint32_t dec_sr;
    /** Decoder sampling rate. */
  uint32_t enc_sr;
    /** Encoder sampling rate. */
  uint32_t rx_pp_sr;
    /** RX post-processing blocks sampling rate. */
  uint32_t tx_pp_sr;
    /** TX pre-processing blocks sampling rate. */
  uint16_t vfr_mode;
    /**< Vocoder frame synchronization mode. Possible values:\n
         - 0 -- No frame synchronization. \n
         - 1 -- Hard Vocoder Frame Reference (VFR). A 20 ms VFR interrupt. */
  uint16_t enc_offset;
    /**< Offset in microseconds from the VFR to deliver a Tx vocoder
        packet. The offset is to be less than 20000 us. */
  uint16_t dec_req_offset;
    /**< The offset in microseconds from the VFR to request for an Rx vocoder
         packet. The offset is to be less than 20000 us. */
  uint16_t dec_offset;
    /**< Offset in microseconds from the VFR to indicate the deadline to
         receive an Rx vocoder packet. The offset is to be less than 20000 us.
         Rx vocoder packets received after this deadline are not guaranteed to
         be processed.  */
}
#include "apr_pack_end.h"
;


#endif /* __VSS_COMMON_PRIVATE_IF_H__ */

