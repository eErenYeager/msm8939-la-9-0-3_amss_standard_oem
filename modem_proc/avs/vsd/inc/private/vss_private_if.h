#ifndef __VSS_PRIVATE_IF_H__
#define __VSS_PRIVATE_IF_H__

/*
   Copyright (C) 2009-2010, 2011, 2014 Qualcomm Technologies Incorporated.
   All rights reserved.
   QUALCOMM Proprietary/GTDR.

   $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/avs/vsd/inc/private/vss_private_if.h#1 $
   $Author: mplp4svc $
*/

#include "cvd_mem_mapper_i.h"
#include "vss_istream_private_if.h"
#include "vss_ivocproc_private_if.h"
#include "vss_imvm_private_if.h"
#include "vss_common_private_if.h"
#include "cvs_api.h"
#include "cvp_api.h"
#include "mvm_api.h"
#include "mvs_module_api.h"
#include "cvd_cal_i.h"

#endif /* __VSS_PRIVATE_IF_H__ */

