#ifndef __VSS_IMVM_PRIVATE_IF_H__
#define __VSS_IMVM_PRIVATE_IF_H__

/*
   Copyright (C) 2009-2010, 2011 Qualcomm Technologies Incorporated.
   All rights reserved.
   QUALCOMM Proprietary/GTDR.

   $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/avs/vsd/inc/private/vss_imvm_private_if.h#1 $
   $Author: mplp4svc $
*/

#include "apr_comdef.h"

/*****************************************************************************
   CVD Voice Manager Core APIs
*****************************************************************************/

/**
  Allows modem to the clear circuit-switched sessions that were previously
  allocated. This will clear all full control "default modem voice" sessions.
 
  @par Payload
  None.
 
  @return
  #APRV2_IBASIC_RSP_RESULT

  @dependencies
  None.
*/
#define VSS_IMVM_CMD_MODEM_CLEAR_SESSIONS ( 0x000112B8 )

/**
  Allows CVS to register the circuit-switched full control modem stream
  with MVM so that it gets closed during clean up.
 
  @par Payload
  None.
 
  @return
  #APRV2_IBASIC_RSP_RESULT

  @dependencies
  None.
*/
#define VSS_IMVM_CMD_REGISTER_MODEM_STREAM ( 0x000112B9 )

/**
  Allows CVS to unregister the circuit-switched full control modem stream 
  with MVM 
 
  @par Payload
  None.
 
  @return
  #APRV2_IBASIC_RSP_RESULT

  @dependencies
  None.
*/
#define VSS_IMVM_CMD_UNREGISTER_MODEM_STREAM ( 0x000112BA )

#endif /* __VSS_IMVM_PRIVATE_IF_H__ */

