/*
  Copyright (C) 2009 - 2014 QUALCOMM Technologies, Inc.
  All Rights Reserved.
  Qualcomm Technologies, Inc. Confidential and Proprietary.
  
  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/avs/vsd/common/mvs/src/mvs_module.c#2 $
  $Author: shrakuma $
*/

/* <EJECT> */
/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
08/23/14   aim     Added debug code to monitor mvs processing time.
08/05/14   sre     Added support for enabling eAMR using voice configuration 
                   framework. 
06/11/14  shr     Removed the state_handover from the MVS state machine.
                   Sending MVM stop command during handovers instead on MVM standby.
05/26/14   aim     Modified timing params for G, W and TD.
03/03/13   js      Added support for generating silence frame on tx side for WCDMA
                   and TDSCDMA.
02/10/14   aim     Modified thread priority.
12/09/13   js/sre  Added support for clearing the CVD sessions, after the end of 
                   each call. Code clean up, with added error check for accquire,
                   enable, standby and release. 
11/8/13    sre     Renamed the GUID VOICECFG_CMDID_GET_BOOL_ITEM to
                   VOICECFG_CMD_GET_BOOL_ITEM for consistency with VS commands. 
10/31/13   sre     Fixed vocoder packet size calculation in 
                   mvs_process_dl_fr_packet function.
09/20/13   sre     Fixed the banned QPSI memcpy and memmove APIs.
09/17/13   aim     Modified timing parameters for G,W & 1X to save on RTD.
06/29/13    sg     Fixed the banned QPSI memcpy APIs.
06/28/13    hl     Fixed the error handling code for handling APR communication
                   errors, in order to prevent crashes in MVS due to APR 
                   communication errors.
06/13/13   rajatm  commented the usedbytes message as apr_memmgr uses memheap2.
06/12/13   sud     Merged the missing SSR phase3 changes.
06/10/13    sg     Merged the missing SSR phase3 changes.
06/01/13   sud     Fixed compiler warnings.
05/21/13    sg     Send sample rate notification ( 8 KHz ) to NAS/GHDI for AMR
05/21/13    sg     Added eAMR mode change detection 
05/16/13   sud     Fixed compiler warnings. 
07/10/12    ak     Added SSR phase 3 support.
04/30/12    qc     Removed the usage of MVS_IST_PRIO_ORDER.
04/24/12    nrc    Event call_back functions are not released properly during
                   call tear down.
04/11/12    hl     Added eAMR support.
03/13/12    at     Merged in TD-SCDMA support.
02/09/12    sb     Remove MVS timing parameters change and add RELEASE fix for SSR
11/02/11    ji     Make MVS timing parameters per network per vocoder type.
08/11/11    nrc    Fixes for Klocwork and compiler warnings.
07/11/11    hl     Added SSR phase 2+ support.
31/3/10     hl     Added SO73 support. Updated set timewarp for wideband vocoders.
10/10/10    jk     Added Featurized timing analysis code
03/11/10    sr     Sending MVS_MODE_NONE on release()
03/03/10    sr     Fixed a bug in MVS release();
02/25/10    jk     fixed CDMA only compilation issue
02/10/10    jk     Changed VOIP client to vfr mode none (need for FW changes)
02/03/10    sr     Support for MVS_MODE_NONE and Re-sync ctm bug fix.
01/27/10    jk     Added g722 support
11/18/09    jk     Fixed dsm activate sequencing problem

==========================================================================
*/

/****************************************************************************
 * INCLUDE HEADER FILES                                                     *
 ****************************************************************************/

#include <stddef.h>
#include "err.h"
#include "mvs.h"
#include "mvsi.h"
#include "mvs_module.h"
#include "mvs_voc_def.h"
#include "mvsdsm.h"
#include "mvsamr.h"
#include "mvs_umts_chan_setup.h"
#include "mvs_log_packet.h"
#include "apr_event.h"
#include "apr_thread.h"
#include "apr_misc.h"
#include "vss_public_if.h"
#include "vss_private_if.h"
#include "vocsvc_nv_api.h"
#include "voicecfg_api.h"
#include "voicecfg_items_i.h"
#include "log.h"
#include "log_codes_umts.h"
#include "mmstd.h"
#include "voiceapp.h"
#include "timetick.h"

#define USE_WCDMAMVSIF_CMI_NONCOMPLIANCE_HACK
#ifdef USE_WCDMAMVSIF_CMI_NONCOMPLIANCE_HACK
  #include "queue.h"
#endif /* USE_WCDMAMVSIF_CMI_NONCOMPLIANCE_HACK */
#include "wcdmamvsif.h"

#ifndef WINSIM
#include "sys_m_messages.h"
#include "rcecb.h"
#endif /* !WINSIM */

/****************************************************************************
 * DEFINES                                                                  *
 ****************************************************************************/

#define MVS_PANIC_ON_ERROR( rc ) \
  { if ( rc ) { ERR_FATAL( "Error[%d]", rc, 0, 0 ); } }

#define MVS_PORT ( 0x1010 )

/* MVS internal opcodes. */
#define VSS_IMVS_CMD_INTERNAL ( 0x00010041 )
#define VSS_IMVS_CMD_SHUTDOWN ( 0x000112D9 )

/* Time in ns for 10ms release delay. */
#define MVS_MVM_POLL_TIMER_NS ( 10000000 )

#define MVS_IST_NAME ( "MVS_IST" )
#define MVS_IST_PRIORITY ( 255 - 170 ) 
  /**< SIM task priority is arbitrary. */


#define MVS_MAX_JOBS_V ( 32 ) 
  /**< Maximum number of jobs which can be allocated at a time. */

  /**< Tracking count of unavailability of packet from ADSP, incrementing mvs_pkt_miss_count variable
       on every call_back from voiceapp, and clearing the variable on packet availability from ADSP.
       We wait till mvs_pkt_miss_count < MVS_PACKET_MISS_THRESHOLD_FOR_PUMPING_SILENCE_FRAME, before
       pumping the silence frame on TX side. 
   */ 
#define MVS_PACKET_MISS_THRESHOLD_FOR_PUMPING_SILENCE_FRAME ( 2 )

/* Threshold in the order of micro seconds. */
#define MVS_PROCESSING_TIME_THRESHOLD_US 1500

/* timetick_get() API reads qtimer running at 19.2MHz and converts it to 32.768 KHz 
 * and return the time tick. So each time tick converts to (1/32768) seconds, 
 * i.e 30517578125 femto seconds. Femto seconds is considered as the unit to  
 * avoid floating point operations.  */
#define MVS_NUM_OF_FS_PER_TIMETICK ( 30517578125 ) 

/* VSID for MVS */
#define MVS_VSID_CS_VOICE_MULTIMODE ( 0x10C01000 )
/*****************************************************************************
 * DEFINITIONS                                                               *
 ****************************************************************************/

/* Timing paramters. */
typedef struct
{
  uint16_t enc_offset;
  uint16_t dec_req_offset;
  uint16_t dec_offset;
} 
  mvs_timing_param_type;
  
/* Re-aligned timing offsets which are close to VFR to account for the jitter. */
/* GSM Timing Parameter */
static mvs_timing_param_type mvs_timing_param_gsm = { 10000, 1000 , 5000 }; 
/* WCDMA Timing Parameter */
static mvs_timing_param_type mvs_timing_param_wcdma = { 19000, 3300, 6300 };
/* CDMA Timing Parameter */
static mvs_timing_param_type mvs_timing_param_cdma = { 18000, 3750, 6750 };
/* TDSCDMA Timing Parameter */
static mvs_timing_param_type mvs_timing_param_tdscdma = { 19000, 3300, 6300 };
/* Default Timing Parameter */
static mvs_timing_param_type mvs_timing_param_default = { 8000, 3300, 8300 };

typedef enum mvs_thread_state_enum_t
{
  MVS_THREAD_STATE_ENUM_INIT,
  MVS_THREAD_STATE_ENUM_READY,
  MVS_THREAD_STATE_ENUM_EXIT
}
  mvs_thread_state_enum_t;

#ifdef USE_MVS_PACKET_TIMING_ANALYSIS
/* Logging struct. */
typedef struct 
{
   timetick_type ul_deliver_delay_max;
   timetick_type ul_processing_delay_max;
   uint32_t      ul_missed_slow_count;
   uint32_t      ul_missed_fast_count;
   timetick_type dl_deliver_delay_max;
   timetick_type dl_processing_delay_max;
   uint32_t      dl_missed_slow_count;
   uint32_t      dl_missed_fast_count;
} mvs_timing_info_type;

static mvs_timing_info_type  mvs_timing_info;   
static timetick_type mvs_ul_prev = 0;
static timetick_type mvs_dl_prev = 0;
static bool_t   mvs_debug_timing = 1;
static uint32_t mvs_timing_start_cnt_ul = 0;
static uint32_t mvs_timing_start_cnt_dl = 0;

static uint32_t mvs_timing_start_delay = 200;

typedef struct 
{
  /* This flag will inidcate if MVS proc time check is enabled
   * via VOICECFG_CFG_IS_MVS_PROC_TIME_CHECK_ENABLED voicecfg item.
   * It will be updated only during init. */
  bool_t is_enabled;
  unsigned long long max_time;
  unsigned long long min_time;
} mvs_processing_time_info_type;

static mvs_processing_time_info_type mvs_processing_time_info;

/*used for WCDMA timing */
/*uint32_t mvs_curr_stmr_time_cx8;*/
/*STMR_STATUS_DUMP(STMR_STATUS_DUMP_CMD_IMMEDIATE);
mvs_curr_stmr_time_cx8 = STMR_GET_TX_FRAME_COUNT_STATUS();*/
#endif /* USE_MVS_PACKET_TIMING_ANALYSIS */

/* The generic item is used in MVS to track job objects and free them in 
 * in case of an enexpected error, such as SSR. */
typedef struct mvs_generic_item_t
{
  apr_list_node_t link;

  uint32_t handle;
    /**< Any custom handle. */
}
  mvs_generic_item_t;

/****************************************************************************
 * GLOBALS                                                                  *
 ****************************************************************************/

/* Name of CVS session created by MVS. */
static char_t mvs_default_stream_name[] = "default modem voice";

/* Flag that prevents mvs from running before basic initializtion has been done */
static bool_t mvs_is_initialized = FALSE;

/* Flag that indicates whether MVM on the ADSP is up. */
static bool_t mvs_is_mvm_up = TRUE;

/* Flag to indicate that we need to preform cleanup because the ADSP has crashed. */
static bool_t mvs_adsp_crash = FALSE;

/* Timer used for polling MVM after SSR to determine weather it's up. */
static apr_timer_t mvs_mvm_poll_timer; 

/* Token used to ensure that the received poll result is from the latest poll. */
static mvs_simple_job_object_t *mvs_mvm_latest_poll;

/* Flag that indicates whether all the allocated objects are freed upon ADSP
 * SSR. 
 */
static bool_t mvs_is_allocated_objs_freed_on_ssr = TRUE;

/* Flag that indicates whether this is the first voice call setup. 
 * The MVM and CVS sessions are only created during the first 
 * voice call setup and are reused for subsequent calls.
 */
static bool_t mvs_is_first_call = TRUE;

static uint8 mvs_MaskArray[] = { 1, 2, 4, 8, 16, 32, 64, 128 };

/* Tracks the count of packets missed from adsp. This variables works in sync with
 * MVS_PACKET_MISS_THRESHOLD_FOR_PUMPING_SILENCE_FRAME.
 * Also see comment for MVS_PACKET_MISS_THRESHOLD_FOR_PUMPING_SILENCE_FRAME 
 */
static uint32_t mvs_pkt_miss_count = 0;

static bool_t mvs_is_registered_with_voiceapp = FALSE;

/* MVM and CVS sessions creted by MVS. */
static uint16_t mvs_mvm_port = APR_NULL_V;
static uint16_t mvs_cvs_port = APR_NULL_V;

static mvs_client_state_type mvs_client_table[MVS_CLIENT_MAX];
static mvs_client_type mvs_cur_client = MVS_CLIENT_NONE;

static apr_memmgr_type mvs_heapmgr;
static uint8_t mvs_heap_pool[ MVS_HEAP_SIZE ];

static mvs_work_item_t mvs_cmd_pool[ MVS_NUM_COMMANDS ];
static apr_list_t mvs_free_cmd_q;
static apr_list_t mvs_incoming_cmd_q;
static apr_list_t mvs_pending_cmd_q;

static apr_list_t mvs_allocated_item_q; 
static mvs_generic_item_t mvs_allocated_object_pool[ MVS_MAX_JOBS_V ];

static apr_lock_t mvs_int_lock;
static apr_lock_t mvs_thread_lock;

/* IST Management */
static apr_event_t mvs_ist_event;
static apr_thread_t mvs_ist_handle;
static mvs_thread_state_enum_t mvs_ist_state = MVS_THREAD_STATE_ENUM_INIT;

/* APR Resource */
static uint32_t mvs_my_apr_handle;

/* Cached MVS vocoder properties. */
typedef struct{
  mvs_voc_rate_type max_voc_rate;
  mvs_voc_rate_type min_voc_rate;
  mvs_dtx_mode_type dtx_mode;
  uint16_t          rate_factor;
  mvs_amr_mode_type amr_ul_mode;
  mvs_amr_mode_type awb_ul_mode;
} mvs_vocoder_properties;

static mvs_vocoder_properties mvs_curr_props, mvs_new_props;

static uint32_t mvs_cached_set = 0;

/* Session Management */
static mvs_pending_control_t mvs_pending_ctrl;
static apr_objmgr_object_t mvs_object_table[ MVS_MAX_OBJECTS ];
static apr_objmgr_t mvs_objmgr;

#ifndef WINSIM
/* WCDMA Channels */
extern mvssup_umts_chan_state_type mvs_ul_umts_chan_state;
extern mvssup_umts_chan_state_type mvs_dl_umts_chan_state;
#endif /* !WINSIM */

/* Release Handling */
static apr_timer_t mvs_timer;

/* G711 Variables */
static mvs_g711_mode_type mvs_current_g711_mode;
/* g722 Variable */
static mvs_g722_mode_type mvs_current_g722_mode;

/* eAMR config */
static vocsvc_nv_eamr_config_t mvs_eamr_config;

/* eAMR config with voice configuration framework */
typedef struct{
  bool_t enable_2g;
  bool_t enable_3g;
  bool_t enable_tdscdma;
} vocicecfg_eamr_config_t;

static vocicecfg_eamr_config_t mvs_voicecfg_eamr_config;

/* APR Service Addresses */
static char_t mvs_my_dns[] = "qcom.audio.mvs";
static uint16_t mvs_my_addr;
  /* MVS address is set at initialization */
static char_t mvs_mvm_dns[] = "qcom.audio.mvm";
static uint16_t mvs_mvm_addr;
  /* MVM address is set at initialization */
static char_t mvs_cvs_dns[] = "qcom.audio.cvs";
static uint16_t mvs_cvs_addr;
  /* CVD address is set at initialization */

static bool_t mvs_is_listen_for_emar_mode_change = FALSE;

/* Array elements are the number of bytes required for the packet
 ** for EVRC
 */
static uint32 mvslegacy_evrc_packet_size[] = {
   0,                                      /* Blank        */
   2,                                      /* Eighth rate  */
   0,                                      /* Quarter rate */
   10,                                     /* Half rate    */
   22,                                     /* Full rate    */
   0                                       /* Erasure      */
};

/* Array elements are the number of bytes required for the packet
** for 13K
*/
static uint32 mvslegacy_13k_packet_size[] = {
   0,                                      /* Blank        */
   3,                                      /* Eighth rate  */
   7,                                      /* Quarter rate */
   16,                                     /* Half rate    */
   34,                                     /* Full rate    */
   0                                       /* Erasure      */
};

/* Array elements are the number of bytes required for the packet
** for 4GV
*/
static uint32 mvslegacy_4gv_packet_size[] = {
   0,                                      /* Blank        */
   2,                                      /* Eighth rate  */
   5,                                      /* Quarter rate */
   10,                                     /* Half rate    */
   22,                                     /* Full rate    */
   0                                       /* Erasure      */
};

/* Array elements are the number of bytes required for the packet
** for 4GV-WB
*/
static uint32 mvslegacy_4gv_WB_packet_size[] = {
   0,                                      /* Blank        */
   2,                                      /* Eighth rate  */
   5,                                      /* Quarter rate */
   10,                                     /* Half rate    */
   22,                                     /* Full rate    */
   0                                       /* Erasure      */
};

/* Array elements are the number of bytes required for the packet
** for 4GV-NW
*/
static uint32 mvslegacy_4gv_NW_packet_size[] = {
   0,                                      /* Blank        */
   2,                                      /* Eighth rate  */
   5,                                      /* Quarter rate */
   10,                                     /* Half rate    */
   22,                                     /* Full rate    */
   0,                                      /* Erasure      */
   2                                       /* Non critical eighth rate */
};

/* Frame type mapping for AMR-NB vocoder over the downlink path,
 * from MVS to ADSP.
 */
cvd_amr_frame_type mvs_to_cvd_amr_frame_table[ MVS_AMR_FRAME_TYPE_MAX ] = 
{
 
 CVD_AMR_SPEECH_GOOD,     /* MVS_AMR_SPEECH_GOOD */
 CVD_AMR_SPEECH_DEGRADED, /* MVS_AMR_SPEECH_DEGRADED */
 CVD_AMR_ONSET,           /* MVS_AMR_ONSET */
 CVD_AMR_SPEECH_BAD,      /* MVS_AMR_SPEECH_BAD */
 CVD_AMR_SID_FIRST,       /* MVS_AMR_SID_FIRST */
 CVD_AMR_SID_UPDATE,      /* MVS_AMR_SID_UPDATE */
 CVD_AMR_SID_BAD,         /* MVS_AMR_SID_BAD */
 CVD_AMR_NO_DATA,         /* MVS_AMR_NO_DATA */
 CVD_AMR_NO_DATA          /* MVS_AMR_SPEECH_LOST */ 
};

/* Frame type mapping for AMR-NB vocoder over the uplink path,
 * from ADSP to MVS.
 */
mvs_amr_frame_type cvd_to_mvs_amr_frame_type[ CVD_AMR_FRAME_TYPE_MAX ] =
{
  MVS_AMR_SPEECH_GOOD,     /* CVD_AMR_SPEECH_GOOD */
  MVS_AMR_SPEECH_DEGRADED, /* CVD_AMR_SPEECH_DEGRADED */
  MVS_AMR_ONSET,           /* CVD_AMR_ONSET */
  MVS_AMR_SPEECH_BAD,      /* CVD_AMR_SPEECH_BAD */
  MVS_AMR_SID_FIRST,       /* CVD_AMR_SID_FIRST */
  MVS_AMR_SID_UPDATE,      /* CVD_AMR_SID_UPDATE */
  MVS_AMR_SID_BAD,         /* CVD_AMR_SID_BAD */
  MVS_AMR_NO_DATA          /* CVD_AMR_NO_DATA */
};

/* Frame type mapping for AMR-WB vocoder over the downlink path,
 * from MVS to ADSP.
 */
cvd_amrwb_frame_type mvs_to_cvd_armwb_frame_table[ MVS_AMR_FRAME_TYPE_MAX ] =
{
  CVD_AMRWB_SPEECH_GOOD,              /* MVS_AMR_SPEECH_GOOD */
  CVD_AMRWB_SPEECH_PROBABLY_DEGRADED, /* MVS_AMR_SPEECH_DEGRADED */
  CVD_AMRWB_NO_DATA,                  /* MVS_AMR_ONSET */
  CVD_AMRWB_SPEECH_BAD,               /* MVS_AMR_SPEECH_BAD */
  CVD_AMRWB_SID_FIRST,                /* MVS_AMR_SID_FIRST */
  CVD_AMRWB_SID_UPDATE,               /* MVS_AMR_SID_UPDATE */
  CVD_AMRWB_SID_BAD,                  /* MVS_AMR_SID_BAD */
  CVD_AMRWB_NO_DATA,                  /* MVS_AMR_NO_DATA */
  CVD_AMRWB_SPEECH_LOST               /* MVS_AMR_SPEECH_LOST */
};

/* Frame type mapping for AMR-WB vocoder over the uplink path,
 * from ADSP to MVS.
 */
mvs_amr_frame_type cvd_to_mvs_amrwb_frame_type[ CVD_AMRWB_FRAME_TYPE_MAX ] = 
{
  MVS_AMR_SPEECH_GOOD,     /* CVD_AMRWB_SPEECH_GOOD */
  MVS_AMR_SPEECH_DEGRADED, /* CVD_AMRWB_SPEECH_PROBABLY_DEGRADED */
  MVS_AMR_SPEECH_LOST,     /* CVD_AMR_SPEECH_LOST */
  MVS_AMR_SPEECH_BAD,      /* CVD_AMR_SPEECH_BAD */
  MVS_AMR_SID_FIRST,       /* CVD_AMR_SID_FIRST */
  MVS_AMR_SID_UPDATE,      /* CVD_AMR_SID_UPDATE */
  MVS_AMR_SID_BAD,         /* CVD_AMR_SID_BAD */
  MVS_AMR_NO_DATA          /* CVD_AMR_NO_DATA */
};

/****************************************************************************
 * FORWARD PROTOTYPES                                                       *
 ****************************************************************************/

static int32_t mvs_free_object (
  mvs_object_t* object
);

/* Silence Frame APIs */

/* This function checks if Silence Frame Generation is supported on mvs_cur_client.
 */
static bool_t mvs_is_silence_frame_supported_for_mvs_cur_client ( void );

/* MVS uses this function to register for modem timing from voiceapp, Silence Frame will be pumped on Tx, 
 * if packets are unavailable from ADSP. 
 */
static uint32_t mvs_register_modem_timing ( void );

/* MVS uses this function to De register itself for modem timing. 
 */
static uint32_t mvs_deregister_modem_timing ( void );

/* MVS registers this function with voiceapp, this Function will be triggered at enc_offset 
 * from VFR timing, and would implement the logic of pumping silence frame on TX. 
 * Please also refer to comment of MVS_PACKET_MISS_THRESHOLD_FOR_PUMPING_SILENCE_FRAME.
 */ 
static void mvs_tx_modem_timing_cb
(
  void* client_data
);

/* Delivers Silence Frame on Tx.
 */
static uint32_t mvs_deliver_tx_silence_frame
( 
  uint8_t* vocoder_packet,
  uint32_t* vocoder_packet_size 
);

/****************************************************************************
 * COMMON INTERNAL ROUTINES                                                 *
 ****************************************************************************/

static void mvs_check_processing_time ( unsigned long long qtimer_start_tick )
{
  unsigned long long qtimer_end_tick = 0, tcount = 0, max_64bit_value = -1;
  
  qtimer_end_tick = timetick_get();
  tcount = ( qtimer_end_tick - qtimer_start_tick + max_64bit_value ) % max_64bit_value; 
  tcount *= MVS_NUM_OF_FS_PER_TIMETICK;

  /* Converting the proc time info to micro second units. */
  tcount = tcount / 1000000000;

  /* Caching the max and min processing times. */
  mvs_processing_time_info.max_time = ( tcount > mvs_processing_time_info.max_time )? 
        tcount : mvs_processing_time_info.max_time;
  mvs_processing_time_info.min_time = ( tcount < mvs_processing_time_info.min_time )? 
        tcount : mvs_processing_time_info.min_time;
  
  if ( tcount > MVS_PROCESSING_TIME_THRESHOLD_US )
  {
    ERR_FATAL( "MVS processing time (%d)us has exceeded the processing_time threshold",tcount, 0, 0 );
  }
}

static void mvs_is_processing_time_check_enabled ( void )
{
  uint32_t _rc;
  voicecfg_cmd_get_bool_item_t item;
  item.id = VOICECFG_CFG_IS_MVS_PROC_TIME_CHECK_ENABLED;
  item.ret_value = FALSE;
  _rc = voicecfg_call( VOICECFG_CMD_GET_BOOL_ITEM, &item, sizeof (item));
  if ( !_rc && ( TRUE == item.ret_value ) )
  {  
     mvs_processing_time_info.is_enabled = TRUE;
  }
  else
  {
    mvs_processing_time_info.is_enabled = FALSE;
  }
}

static void mvs_int_lock_fn ( void )
{
  ( void ) apr_lock_enter( mvs_int_lock );
}

static void mvs_int_unlock_fn ( void )
{
  ( void ) apr_lock_leave( mvs_int_lock );
}

static void mvs_thread_lock_fn ( void )
{
  ( void ) apr_lock_enter( mvs_thread_lock );
}

static void mvs_thread_unlock_fn ( void )
{
  ( void ) apr_lock_leave( mvs_thread_lock );
}

static void mvs_signal_run ( void )
{
#ifdef USE_SINGLE_AUDSVC_THREAD
  APR_INTERNAL void vocsvc_signal_task ( void );

  vocsvc_signal_task( );
#else
  apr_event_signal( mvs_ist_event );
#endif /* USE_SINGLE_AUDSVC_THREAD */
}

static int32_t mvs_get_object (
  uint32_t handle,
  mvs_object_t** ret_obj
)
{
  int32_t rc;
  apr_objmgr_object_t* objmgr_obj;

  if ( ret_obj == NULL )
  {
    return APR_EBADPARAM;
  }

  rc = apr_objmgr_find_object( &mvs_objmgr, handle, &objmgr_obj );
  if ( rc )
  {
    return APR_EFAILED;
  }

  *ret_obj = ( ( mvs_object_t* ) objmgr_obj->any.ptr );

  return APR_EOK;
}

static int32_t mvs_typecast_object (
  apr_objmgr_object_t* store,
  mvs_object_type_enum_t type,
  mvs_object_t** ret_obj
)
{
  mvs_object_t* obj;

  if ( ( store == NULL ) || ( ret_obj == NULL ) )
  {
    return APR_EBADPARAM;
  }

  obj = ( ( mvs_object_t* ) store->any.ptr );

  if ( obj->header.type != type )
  {
    return APR_EFAILED;
  }

  *ret_obj = obj;

  return APR_EOK;
}

static int32_t mvs_get_typed_object (
  uint32_t handle,
  mvs_object_type_enum_t type,
  mvs_object_t** ret_obj
)
{
  int32_t rc;
  apr_objmgr_object_t* store;

  if ( handle == 0 )
  {
    return APR_EHANDLE;
  }

  rc = apr_objmgr_find_object( &mvs_objmgr, handle, &store );
  if ( rc ) return rc;

  rc = mvs_typecast_object( store, type, ret_obj );
  if ( rc ) return rc;

  return APR_EOK;
}

/****************************************************************************
 * MVS COMMAND AND RESPONSE PROCESSING ROUTINES                             *
 ****************************************************************************/

static void mvs_default_event_handler_fn (
  aprv2_packet_t* packet
)
{
  /* The default event handler just drops the packet. A specific event
   * handler routine should be written to something more useful.
   */
  ( void ) __aprv2_cmd_free( mvs_my_apr_handle, packet );
}

static void mvs_set_default_response_table (
  mvs_response_fn_table_t table
)
{
  int i;

  if ( table == NULL )
  {
    return;
  }

  /* Initialize the state response handler function table. */
  for ( i = 0; i < MVS_RESPONSE_FN_ENUM_MAX; ++i )
  {
    table[ i ] = mvs_default_event_handler_fn;
  }
}

static void mvs_simple_result_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  mvs_simple_job_object_t* obj;

  rc = mvs_get_typed_object( packet->token, MVS_OBJECT_TYPE_ENUM_SIMPLE_JOB, ( ( mvs_object_t** ) &obj ) );
  if ( rc == APR_EOK )
  {
    obj->is_completed = TRUE;
    obj->status = APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet )->status;
  }

  rc = __aprv2_cmd_free( mvs_my_apr_handle, packet );
  MVS_PANIC_ON_ERROR(rc);

  //MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_heapmgr->usedbytes = %d",
  //        mvs_heapmgr.used_bytes );
}

static void mvs_mvm_poll_result_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  mvs_simple_job_object_t* obj;
  int32_t status;

  rc = mvs_get_typed_object( packet->token, MVS_OBJECT_TYPE_ENUM_SIMPLE_JOB, ( ( mvs_object_t** ) &obj ) );
  if ( ( rc == APR_EOK ) && 
       ( obj == mvs_mvm_latest_poll ) )
  {
    status = APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet )->status;
    if ( status == APR_EUNSUPPORTED )
    {
      /* This is a temporary workaround to detect that MVM as well as CVD is up
       * after SSR. MVM is the last service in CVD that get initialized. 
       * Therefore after MVM is up, it means the CVS and CVP is also up. 
       * MVM does not support the command APR_CMDID_GET_VERSION. And it will
       * return APR_EUNSUPPORTED. Once we get this response from MVM, we know
       * that MVM is up after SSR. 
       */
      mvs_is_mvm_up = TRUE;
      MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_ssr_debug: detected ADSP is " \
          "up again." );
    }
  }

  rc = __aprv2_cmd_free( mvs_my_apr_handle, packet );
  MVS_PANIC_ON_ERROR(rc);

  //MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_heapmgr->usedbytes = %d",
  //        mvs_heapmgr.used_bytes );
}

/****************************************************************************
 * MVM/CVS SESSION CREATION RESPONSE FUNCTIONS                              *
 ****************************************************************************/

static void mvs_create_mvm_session_rsp_result_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  mvs_simple_job_object_t* obj;

  rc = mvs_get_typed_object( packet->token, MVS_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( mvs_object_t** ) &obj ) );
  if ( rc == APR_EOK )
  {
    obj->is_completed = TRUE;
    obj->status = APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet )->status;
  }

  if ( obj->status == APR_EOK )
  {
    mvs_mvm_port = packet->src_port;
  }

  rc = __aprv2_cmd_free( mvs_my_apr_handle, packet );
  MVS_PANIC_ON_ERROR(rc);

  //MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_heapmgr->usedbytes = %d",
  //        mvs_heapmgr.used_bytes );
}

static void mvs_create_cvs_session_rsp_result_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  mvs_simple_job_object_t* obj;

  rc = mvs_get_typed_object( packet->token, MVS_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( mvs_object_t** ) &obj ) );
  if ( rc == APR_EOK )
  {
    obj->is_completed = TRUE;
    obj->status = APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet )->status;
  }

  if ( obj->status == APR_EOK )
  {
    mvs_cvs_port = packet->src_port;
  }

  rc = __aprv2_cmd_free( mvs_my_apr_handle, packet );
  MVS_PANIC_ON_ERROR(rc);

  //MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_heapmgr->usedbytes = %d",
  //        mvs_heapmgr.used_bytes );
}

/****************************************************************************
 * MVS OBJECT CREATION AND DESTRUCTION ROUTINES                             *
 ****************************************************************************/

/* This function assumes the input parameters are good. */
static int32_t mvs_find_object (
  apr_list_t* list,
  uint32_t handle,
  mvs_generic_item_t** ret_generic_item
)
{
  int32_t rc;
  mvs_generic_item_t* generic_item;

  generic_item = ( ( mvs_generic_item_t* ) &list->dummy );
  for ( ;; )
  {
    rc = apr_list_get_next( list,
                            ( ( apr_list_node_t* ) generic_item ),
                            ( ( apr_list_node_t** ) &generic_item ) );
    if ( rc )
    {
      return APR_EFAILED;
    }

    if ( generic_item->handle == handle )
    {
      break;
    }
  }

  *ret_generic_item = generic_item;

  return APR_EOK;
}

static int32_t mvs_track_allocated_object (
  mvs_generic_item_t* tracked_item
)
{
  int32_t rc;
  mvs_generic_item_t *generic_item = NULL;

  /* Find the first NULL item and populate it with
  *  handle to track. 
  */
  rc = mvs_find_object ( 
         &mvs_allocated_item_q, 
         APR_NULL_V, 
         &generic_item );
  if ( rc )
  {
    MVS_PANIC_ON_ERROR( rc );
  }
  generic_item->handle = ( ( int32_t ) tracked_item );

  return APR_EOK;
}

static int32_t mvs_untrack_allocated_object (
  mvs_generic_item_t* tracked_item
)
{
  int32_t rc;
  mvs_generic_item_t *generic_item = NULL;
  rc = mvs_find_object ( 
         &mvs_allocated_item_q, 
         ( ( uint32_t ) tracked_item ), 
         &generic_item );

  if ( rc )
  {
    MVS_PANIC_ON_ERROR( rc );
  }
  generic_item->handle = APR_NULL_V;

  return APR_EOK;
}

static int32_t mvs_free_all_allocated_objects ( void )
{
  int32_t rc;
  mvs_generic_item_t* generic_item;

  generic_item = ( ( mvs_generic_item_t* ) &mvs_allocated_item_q.dummy );
  for ( ;; )
  {
    rc = apr_list_get_next( &mvs_allocated_item_q,
                            ( ( apr_list_node_t* ) generic_item ),
                            ( ( apr_list_node_t** ) &generic_item ) );
    if ( ( rc ) || 
         ( generic_item->handle == APR_NULL_V ) )
    {
      break;
    }

    {
      rc = mvs_free_object ( ( mvs_object_t* ) generic_item->handle );
      MVS_PANIC_ON_ERROR( rc );
    }
  }

  return APR_EOK;
}

int32_t mvs_mem_alloc_object (
  uint32_t size,
  mvs_object_t** ret_object
)
{
  int32_t rc;
  mvs_object_t* mvs_obj;
  apr_objmgr_object_t* objmgr_obj;

  if ( ret_object == NULL )
  {
    return APR_EBADPARAM;
  }

  { /* Allocate memory for the MVS object. */
    mvs_obj = apr_memmgr_malloc( &mvs_heapmgr, size );
    if ( mvs_obj == NULL )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_mem_alloc_object(): Out " \
              "of memory, requested size (%d)",
              size);
      return APR_ENORESOURCE;
    }

    /* Allocate a new handle for the MVS object. */
    rc = apr_objmgr_alloc_object( &mvs_objmgr, &objmgr_obj );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_mem_alloc_object(): Out " \
              "of objects, result: (0x%08X)",
              rc);
      apr_memmgr_free( &mvs_heapmgr, mvs_obj );
      return APR_ENORESOURCE;
    }

    /* Use the custom object type. */
    objmgr_obj->any.ptr = mvs_obj;

    /* Initialize the base MVS object header. */
    mvs_obj->header.handle = objmgr_obj->handle;
    mvs_obj->header.type = MVS_OBJECT_TYPE_ENUM_UNINITIALIZED;
  }

  *ret_object = mvs_obj;

  return APR_EOK;
}

static int32_t mvs_mem_free_object (
  mvs_object_t* object
)
{
  if ( object == NULL )
  {
    return APR_EBADPARAM;
  }

  /* Free the object memory and object handle. */
  ( void ) apr_objmgr_free_object( &mvs_objmgr, object->header.handle );
  apr_memmgr_free( &mvs_heapmgr, object );

  return APR_EOK;
}

static void mvs_simple_self_destruct_result_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  mvs_token_object_t* obj;

  rc = mvs_get_typed_object( packet->token, MVS_OBJECT_TYPE_ENUM_HANDLE,
                             ( ( mvs_object_t** ) &obj ) );
  if ( rc == APR_EOK )
  {
    rc = mvs_free_object( ( mvs_object_t* ) obj );
    MVS_PANIC_ON_ERROR( rc );
  }

  rc = __aprv2_cmd_free( mvs_my_apr_handle, packet );
  MVS_PANIC_ON_ERROR(rc);

  //MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_heapmgr->usedbytes = %d",
   //       mvs_heapmgr.used_bytes );
}

int32_t mvs_create_autofree_token_object (
  mvs_token_object_t** ret_obj
)
{
  int32_t rc;
  mvs_token_object_t* mvs_obj;

  if ( ret_obj == NULL )
  {
    return APR_EBADPARAM;
  }

  rc = mvs_mem_alloc_object( sizeof( mvs_token_object_t ), ( ( mvs_object_t** ) &mvs_obj ) );
  if ( rc )
  {
    return APR_ENORESOURCE;
  }

  {  /* Initialize the simple job object. */
    mvs_obj->header.type = MVS_OBJECT_TYPE_ENUM_HANDLE;
    mvs_set_default_response_table( mvs_obj->fn_table );
    mvs_obj->fn_table[ MVS_RESPONSE_FN_ENUM_RESULT ] = mvs_simple_self_destruct_result_rsp_fn;
  }

  { /* Track the allocated object to free in case of error */
    rc = mvs_track_allocated_object( ( mvs_generic_item_t* )mvs_obj );
  }

  *ret_obj = mvs_obj;

  return APR_EOK;
}

static int32_t mvs_create_simple_job_object (
  uint32_t parentjob_handle,
  mvs_simple_job_object_t** ret_job_obj
)
{
  int32_t rc;
  mvs_simple_job_object_t* mvs_obj;

  if ( ret_job_obj == NULL )
  {
    return APR_EBADPARAM;
  }

  rc = mvs_mem_alloc_object( sizeof( mvs_simple_job_object_t ), ( ( mvs_object_t** ) &mvs_obj ) );
  if ( rc )
  {
    return APR_ENORESOURCE;
  }

  { /* Initialize the simple job object. */
    mvs_obj->header.type = MVS_OBJECT_TYPE_ENUM_SIMPLE_JOB;
    mvs_obj->parentjob_handle = parentjob_handle;
    mvs_set_default_response_table( mvs_obj->fn_table );
    mvs_obj->fn_table[ MVS_RESPONSE_FN_ENUM_RESULT ] = mvs_simple_result_rsp_fn;
    mvs_obj->is_accepted = 0;
    mvs_obj->is_completed = 0;
  }

  { /* Track the allocated object to free in case of error */
    rc = mvs_track_allocated_object( ( mvs_generic_item_t* )mvs_obj );
  }

  *ret_job_obj = mvs_obj;

  return APR_EOK;
}

static int32_t mvs_create_sequencer_job_object (
  mvs_sequencer_job_object_t** ret_job_obj
)
{
  int32_t rc;
  mvs_sequencer_job_object_t* job_obj;

  if ( ret_job_obj == NULL )
  {
    return APR_EBADPARAM;
  }

  rc = mvs_mem_alloc_object( sizeof( mvs_sequencer_job_object_t ),
                             ( ( mvs_object_t** ) &job_obj ) );
  if ( rc )
  {
    return APR_ENORESOURCE;
  }

  { /* Initialize the pending job object. */
    job_obj->header.type = MVS_OBJECT_TYPE_ENUM_SEQUENCER_JOB;

    job_obj->state = APR_NULL_V;
    job_obj->subjob_obj = NULL;
    job_obj->status = APR_UNDEFINED_ID_V;
  }

  { /* Track the allocated object to free in case of error */
    rc = mvs_track_allocated_object( ( mvs_generic_item_t* )job_obj );
  }

  *ret_job_obj = job_obj;

  return APR_EOK;
}

static int32_t mvs_free_object (
  mvs_object_t* object
)
{
  if ( object == NULL )
  {
    return APR_EBADPARAM;
  }

  /* Perform object-specific clean-up necessary. */
  switch ( object->header.type )
  {
  case MVS_OBJECT_TYPE_ENUM_UNINITIALIZED:
    break;

  case MVS_OBJECT_TYPE_ENUM_HANDLE:
    mvs_untrack_allocated_object( (mvs_generic_item_t* ) object );
    break;
    
  case MVS_OBJECT_TYPE_ENUM_SIMPLE_JOB:
    mvs_untrack_allocated_object( (mvs_generic_item_t* ) object );
    break;

  case MVS_OBJECT_TYPE_ENUM_SEQUENCER_JOB:
    mvs_untrack_allocated_object( (mvs_generic_item_t* ) object );
    break;

  case MVS_OBJECT_TYPE_ENUM_INVALID:
    break;

  default:
    break;
  }

  /* Free the object memory and object handle. */
  ( void ) mvs_mem_free_object( object );

  return APR_EOK;
}

/****************************************************************************
 * MVS WORK QUEUE ROUTINES                                                  *
 ****************************************************************************/

static int32_t mvs_send_mode_event (
  mvs_client_type client,
  mvs_mode_status_type status,
  mvs_mode_type voc_mode
)
{
  mvs_event_type event_data;

  if ( client >= MVS_CLIENT_MAX )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_send_mode_event(): Invalid " \
            "client (%d)",
            client );
    return APR_EBADPARAM;
  }

  if ( mvs_client_table[client].event_cb != NULL )
  {
    event_data.mode.hdr.event = MVS_EV_MODE;
    event_data.mode.hdr.client = client;
    event_data.mode.mode_status = status;
    event_data.mode.mode = voc_mode;
    mvs_client_table[client].event_cb( &event_data );
  }
  else
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_send_mode_event(), " \
            "client=(%d), event_cb=NULL",
            client );
    return APR_EFAILED;
  }

  return APR_EOK;
}

static int32_t mvs_send_cmd_event (
  mvs_client_type client,
  mvs_cmd_status_type status
)
{
  mvs_event_type event_data;

  if ( client >= MVS_CLIENT_MAX )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "Invalid client = (%d)",
            client );
    return APR_EBADPARAM;
  }

  if ( mvs_client_table[client].event_cb != NULL )
  {
    event_data.cmd.hdr.event = MVS_EV_COMMAND;
    event_data.cmd.hdr.client = client;
    event_data.cmd.cmd_status = status;
    mvs_client_table[client].event_cb( &event_data );
  }
  else
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_send_cmd_event(), " \
            "client=(%d), event_cb=NULL",
            client );
    return APR_EFAILED ;
  }

  return APR_EOK ;
}

static uint32_t mvs_eamr_mode_call ( void )
{
  voicecfg_cmd_get_bool_item_t item; 
  uint32_t rc;
  
  /* Vaidate eAMR on 2G */
  item.id = VOICECFG_CFG_IS_2G_EAMR_ENABLED;
  item.ret_value = FALSE; 

  rc =  voicecfg_call( VOICECFG_CMD_GET_BOOL_ITEM, &item, sizeof( item ) );

  if ( ! rc  && item.ret_value == TRUE )
  { 
    mvs_voicecfg_eamr_config.enable_2g = TRUE;
  }
  else
  {
    mvs_voicecfg_eamr_config.enable_2g = FALSE;	
  }

  /* Vaidate eAMR on 3G */
  item.id = VOICECFG_CFG_IS_3G_EAMR_ENABLED;
  item.ret_value = FALSE; 

  rc =  voicecfg_call( VOICECFG_CMD_GET_BOOL_ITEM, &item, sizeof( item ) );

  if ( ! rc  && item.ret_value == TRUE )
  { 
    mvs_voicecfg_eamr_config.enable_3g = TRUE;
  }
  else
  {
    mvs_voicecfg_eamr_config.enable_3g = FALSE;	
  }

  /* Vaidate eAMR on TDSCDMA */
  item.id = VOICECFG_CFG_IS_TDS_EAMR_ENABLED;
  item.ret_value = FALSE; 

  rc =  voicecfg_call( VOICECFG_CMD_GET_BOOL_ITEM, &item, sizeof( item ) );

  if ( ! rc  && item.ret_value == TRUE )
  { 
    mvs_voicecfg_eamr_config.enable_tdscdma = TRUE;
  }
  else
  {
    mvs_voicecfg_eamr_config.enable_tdscdma = FALSE;	
  }

  MSG_3(MSG_SSID_DFLT, MSG_LEGACY_HIGH, "mvs_eamr_mode_call(): eAMR configuration for "
                                        "GSM is : (%d), WCDMA is : (%d) & TDSCDMA is : (%d)",
                                         mvs_voicecfg_eamr_config.enable_2g,
                                         mvs_voicecfg_eamr_config.enable_3g,
                                         mvs_voicecfg_eamr_config.enable_tdscdma );

  return APR_EOK;

}

/* Return True if eAMR is enabled. */
bool_t mvs_is_eamr_enabled (
  mvs_client_type client 
)
{
  boolean ret_val = FALSE;
  
  switch ( client )
  {
  case MVS_CLIENT_GSM:
    {
      if ( ( mvs_eamr_config.enable_2g == TRUE ) ||
           ( mvs_voicecfg_eamr_config.enable_2g == TRUE ) )
      {
         ret_val = TRUE;
      }
    }
    break;
    
  case MVS_CLIENT_WCDMA:
    {
      if ( ( mvs_eamr_config.enable_3g == TRUE ) ||
           ( mvs_voicecfg_eamr_config.enable_3g == TRUE ) )
      {
         ret_val = TRUE;
      }
    }
    break;

  case MVS_CLIENT_TDSCDMA:
    {
      if ( ( mvs_eamr_config.enable_tdscdma == TRUE ) ||
           ( mvs_voicecfg_eamr_config.enable_tdscdma == TRUE ) )
      {
         ret_val = TRUE;
      }
    }
    break;

  case MVS_CLIENT_TEST:
    {
      if ( ( mvs_eamr_config.enable_2g == TRUE ) ||
           ( mvs_eamr_config.enable_3g == TRUE ) ||
           ( mvs_eamr_config.enable_tdscdma == TRUE ) ||
           ( mvs_voicecfg_eamr_config.enable_2g == TRUE ) ||
           ( mvs_voicecfg_eamr_config.enable_3g == TRUE ) ||
           ( mvs_voicecfg_eamr_config.enable_tdscdma == TRUE ) )
      {
         ret_val = TRUE;
      }
    }
    break;

  default:
    {
      ret_val = FALSE;
    }
    break;
  }

  return ret_val;
}

static int32_t mvs_send_eamr_sr_event (
  mvs_client_type client,
  mvs_eamr_sr_type sr
)
{

  mvs_event_type event_data;

  if ( client >= MVS_CLIENT_MAX )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_send_eamr_sr_event(): " \
            "Invalid client = (%d)",
            client );
    return APR_EBADPARAM;
  }

  if ( mvs_client_table[client].event_cb != NULL )
  {
    event_data.eamr.hdr.event = MVS_EV_EAMR_SR;
    event_data.eamr.hdr.client = client;
    event_data.eamr.sr = sr;
    mvs_client_table[client].event_cb( &event_data );
  }
  else
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_send_eamr_sr_event(), " \
            "client = (%d), event_cb = NULL",
            client );
    return APR_EFAILED;
  }

  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,  "mvs_send_eamr_sr_event(), client = " \
        "(%d), sample_rate = (%d)",
        client, sr);
  return APR_EOK;

}

static void mvs_queue_pending_packet (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  mvs_work_item_t* work_item;

  MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_queue_pending_packet" );

  for ( ;; )
  {
    if ( mvs_is_initialized == FALSE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "MVS recieved a command before " \
            "it was properly initialized" );
      __aprv2_cmd_end_command( 0xDEAD, packet, APR_ENOTREADY );
      break;
    }

    /* TODO: CMDRSP should use its own free_cmd_q. It would be more robust to
     *       have client side free command queues and server side free
     *       commands queues. The MVS clients can run out of free commands,
     *       but this should not prevent MVS from receiving messages from its
     *       servers. We can check the opcode is VSS_IMVM_CMD_INTERNAL to
     *       know to use a different free command queue.
     */

    { /* Get a free command structure. */
      rc = apr_list_remove_head( &mvs_free_cmd_q, ( ( apr_list_node_t** ) &work_item ) );
      if ( rc )
      { /* No free command structure is available. */
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "MVS ran out of free pending " \
              "command queues" );
        __aprv2_cmd_free( mvs_my_apr_handle, packet );
        break;
      }
    }
    { /* Queue the incoming command to the pending command queue. We don't
       * need to signal do work because after the incoming command
       * handler is done the pending command handler routine will be
       * called.
       */
      work_item->packet = packet;
      ( void ) apr_list_add_tail( &mvs_pending_cmd_q, &work_item->link );
      mvs_signal_run( );
    }

    return;
  }

  /* Not reporting the error. */
}

/* Returns TRUE if successful and FALSE if unsuccessful. */
static bool_t mvs_queue_incoming_packet (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  mvs_work_item_t* work_item;

  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_queue_incoming_packet() - " \
        "opcode=(0x%08X), token=(0x%08X)",
        packet->opcode, packet->token );

  //TODO: Fix INIT in mvs_enable()
  //TODO: READY/NOT_READY should check the mvs_cur_client and state, no need to check anything else

  for ( ;; )
  {
    if ( mvs_is_initialized == FALSE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "MVS recieved a command before " \
            "it was properly initialized" );
      __aprv2_cmd_end_command( 0xDEAD, packet, APR_ENOTREADY );
      break;
    }

    /* TODO: CMDRSP should use its own free_cmd_q. It would be more robust to
     *       have client side free command queues and server side free
     *       commands queues. The MVS clients can run out of free commands,
     *       but this should not prevent MVS from receiving messages from its
     *       servers. We can check the opcode is VSS_IMVM_CMD_INTERNAL to
     *       know to use a different free command queue.
     */

    { /* Get a free command structure. */
      rc = apr_list_remove_head( &mvs_free_cmd_q, ( ( apr_list_node_t** ) &work_item ) );
      if ( rc )
      { /* No free command structure is available. */
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "MVS ran out of free incoming " \
              "command queues" );
        __aprv2_cmd_free( mvs_my_apr_handle, packet );
        break;
      }
    }
    { /* Report command acceptance when requested. */
      rc = __aprv2_cmd_accept_command( mvs_my_apr_handle, packet );
      if ( rc )
      { /* Can't report so abort the command. */
        ( void ) apr_list_add_tail( &mvs_free_cmd_q, &work_item->link );
        break;
      }
    }
    { /* Queue the new command to the incoming command queue and signal do
       * work.
       */
      work_item->packet = packet;
      ( void ) apr_list_add_tail( &mvs_incoming_cmd_q, &work_item->link );
      mvs_signal_run( );
    }

    return TRUE;
  }

  return FALSE;
}

static int32_t mvs_make_cmd_packet (
  mvs_packet_type* mvs_cmd,
  aprv2_packet_t** packet
)
{
  int32_t rc;
  void* payload;
  uint32_t payload_size;

  rc = __aprv2_cmd_alloc_ext(
         mvs_my_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
         mvs_my_addr, APR_NULL_V,
         mvs_my_addr, APR_NULL_V,
         mvs_cmd->hdr.cmd, VSS_IMVS_CMD_INTERNAL,
         sizeof( mvs_packet_type ), packet );
  if ( rc )
  {
    return APR_ENORESOURCE;
  }

  /* TODO: Make sure the payload copy is cache/uncache safe. */
  payload = APRV2_PKT_GET_PAYLOAD( void, *packet );
  payload_size = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( (*packet)->header );
  mmstd_memcpy( payload, payload_size, ( ( void* ) mvs_cmd ), sizeof( *mvs_cmd ) );

  return APR_EOK;
}

/****************************************************************************
 * MVS CORE IMPLEMENTATION                                                  *
 ****************************************************************************/

static uint32_t map_to_cvd_awb_mode (
  mvs_amr_mode_type awb_mode
)
{
  uint32_t mode;

  switch ( awb_mode )
  {
  case MVS_AMR_MODE_0660:
    mode = 0x00000000;
    break;

  case MVS_AMR_MODE_0885:
    mode = 0x00000001;
    break;

  case MVS_AMR_MODE_1265:
    mode = 0x00000002;
    break;

  case MVS_AMR_MODE_1425:
    mode = 0x00000003;
    break;

  case MVS_AMR_MODE_1585:
    mode = 0x00000004;
    break;

  case MVS_AMR_MODE_1825:
    mode = 0x00000005;
    break;

  case MVS_AMR_MODE_1985:
    mode = 0x00000006;
    break;

  case MVS_AMR_MODE_2305:
    mode = 0x00000007;
    break;

  case MVS_AMR_MODE_2385:
    mode = 0x00000008;
    break;

  default:
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "received wrong mode, set to " \
          "default MVS_AMR_MODE_2385" );
    mode = 0x00000008;
    break;
  }

  return mode;
}

static uint32_t map_to_cvd_mode (
  mvs_mode_type voc_mode
)
{
  uint32_t media_id;

  switch ( voc_mode )
  {
  default:
  case MVS_MODE_NONE:
    media_id = VSS_MEDIA_ID_NONE;
    break;

  case MVS_MODE_LINEAR_PCM:
    media_id = VSS_MEDIA_ID_PCM_8_KHZ;
    break;

  case MVS_MODE_IS733:
    media_id = VSS_MEDIA_ID_13K_MODEM;
    break;

  case MVS_MODE_IS127:
    media_id = VSS_MEDIA_ID_EVRC_MODEM;
    break;

  case MVS_MODE_4GV_NB:
    media_id = VSS_MEDIA_ID_4GV_NB_MODEM;
    break;

  case MVS_MODE_4GV_WB:
    media_id = VSS_MEDIA_ID_4GV_WB_MODEM;
    break;

  case MVS_MODE_4GV_NW:
    media_id = VSS_MEDIA_ID_4GV_NW_MODEM;
    break;

  case MVS_MODE_AMR:
    if ( mvs_is_eamr_enabled ( mvs_cur_client ) == TRUE ) 
    {
      media_id = VSS_MEDIA_ID_EAMR;
    }
    else
    {
      media_id = VSS_MEDIA_ID_AMR_NB_MODEM;
    }
    break;

  case MVS_MODE_AMR_WB:
    media_id = VSS_MEDIA_ID_AMR_WB_MODEM;
    break;

  case MVS_MODE_EFR:
    media_id = VSS_MEDIA_ID_EFR_MODEM;
    break;

  case MVS_MODE_FR:
    media_id = VSS_MEDIA_ID_FR_MODEM;
    break;

  case MVS_MODE_HR:
    media_id = VSS_MEDIA_ID_HR_MODEM;
    break;

  case MVS_MODE_G711:
  case MVS_MODE_G711A:
    media_id = ( ( mvs_current_g711_mode == MVS_G711_MODE_MULAW ) ?
                 VSS_MEDIA_ID_G711_MULAW :
                 VSS_MEDIA_ID_G711_ALAW );
    break;

  case MVS_MODE_G722:
    media_id = VSS_MEDIA_ID_G722;
    break;

  case MVS_MODE_G729A:
    media_id = VSS_MEDIA_ID_G729;
    break;
  }

  return media_id;
}

static uint32_t map_to_cvd_network (
  mvs_client_type client
)
{
  uint32_t network_id;

  switch ( client )
  {
  default:
  case MVS_CLIENT_CDMA:
    network_id = VSS_ICOMMON_CAL_NETWORK_ID_CDMA;
    break;

  case MVS_CLIENT_WCDMA:
  case MVS_CLIENT_TDSCDMA:
    network_id = VSS_ICOMMON_CAL_NETWORK_ID_WCDMA;
    break;

  case MVS_CLIENT_GSM:
    network_id = VSS_ICOMMON_CAL_NETWORK_ID_GSM;
    break;
  }

  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_ CLIENT (%d) NEW NETWORK  (%x)",
        client, network_id );

  return network_id;
}

boolean mvs_mod_mode_supported (
  mvs_mode_type mode
)
{
  switch ( mode )
  {
  case MVS_MODE_LINEAR_PCM:
  case MVS_MODE_IS733:
  case MVS_MODE_IS127:
  case MVS_MODE_4GV_NB:
  case MVS_MODE_4GV_WB:
  case MVS_MODE_4GV_NW:
  case MVS_MODE_AMR:
  case MVS_MODE_AMR_WB:
  case MVS_MODE_EFR:
  case MVS_MODE_FR:
  case MVS_MODE_HR:
  case MVS_MODE_G711:
  case MVS_MODE_G711A:
  case MVS_MODE_G722:
  case MVS_MODE_G729A:
    return TRUE;

  default:
    return FALSE;
  }
}

mvs_amr_mode_type mvs_get_ul_amr_mode ( void )
{
  return mvs_curr_props.amr_ul_mode;
}

mvs_amr_mode_type mvs_get_ul_awb_mode ( void )
{
  return mvs_curr_props.awb_ul_mode;
}

int32_t mvs_mod_voc_set_frame_rate (
  mvs_voc_rate_type max_rate,
  mvs_voc_rate_type min_rate
)
{
  int32_t rc;
  mvs_packet_type mvs_cmd;
  aprv2_packet_t* packet;

  mvs_cmd.set_cdma_minmax_rate.hdr.cmd = MVS_CMD_SET_CDMA_MINMAX_RATE;
  mvs_cmd.set_cdma_minmax_rate.max_rate = max_rate;
  mvs_cmd.set_cdma_minmax_rate.min_rate = min_rate;

  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_mod_voc_set_frame_rate(): " \
        "max_rate (%d), min_rate (%d)",
        max_rate, min_rate );

  mvs_new_props.max_voc_rate = max_rate;
  mvs_new_props.min_voc_rate = min_rate;

  if ( mvs_cvs_port != APR_NULL_V  )
  {
    rc = mvs_make_cmd_packet( &mvs_cmd, &packet );
    if ( rc )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "make pkt failed!!" );
      return APR_ENORESOURCE;
    }

    ( void ) mvs_queue_incoming_packet( packet );
  }

  return APR_EOK;
}

int32_t mvs_mod_voc_set_frame_rate_proc (
  mvs_voc_rate_type max_rate,
  mvs_voc_rate_type min_rate,
  boolean force_write_through
)
{
  int32_t rc = APR_EOK;
  mvs_token_object_t* obj;
  vss_istream_cmd_cdma_set_enc_minmax_rate_t minmax;

  if ( max_rate == MVS_VOC_0_RATE )
  {
    /* Modem sending invalid rate */
    max_rate = MVS_VOC_1_RATE;
  }

  if ( min_rate == MVS_VOC_0_RATE )
  {
    /* Modem sending invalid rate */
    min_rate = MVS_VOC_8_RATE;
  }

  if ( ( max_rate == mvs_curr_props.max_voc_rate ) && ( min_rate == mvs_curr_props.min_voc_rate ) &&
       ( mvs_cached_set == 0 ) )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,  "mvs_voc_set_frame_rate(): " \
          "Ignoring same values max "
          "rate:(%d) min rate:(%d)", max_rate, min_rate );
    return APR_EOK;
  }

  mvs_curr_props.max_voc_rate = max_rate;
  mvs_curr_props.min_voc_rate = min_rate;

  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,  "mvs_voc_set_frame_rate(): max_rate " \
        "(%d) , min_rate (%d)",
        max_rate, min_rate );

  if ( mvs_cur_client != MVS_CLIENT_NONE )
  {
    if ( mvs_client_table[mvs_cur_client].config_mode == MVS_MODE_NONE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "mvs_voc_set_frame_rate(): MVS " \
           "has no config mode" );
      return APR_EOK;
    }
  }
  else
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "mvs_voc_set_frame_rate(): There is " \
         "no current client" );
    return APR_EOK;
  }

  if ( mvs_client_table[mvs_cur_client].state == MVS_STATE_VOCODER ||
       force_write_through )
  {
    rc = mvs_create_autofree_token_object(&obj);
    MVS_PANIC_ON_ERROR( rc );

    minmax.max_rate = ( ( uint16_t ) max_rate );
    minmax.min_rate = ( ( uint16_t ) min_rate );

    rc = __aprv2_cmd_alloc_send(
           mvs_my_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
           mvs_my_addr, MVS_PORT,
           mvs_cvs_addr, mvs_cvs_port,
           obj->header.handle,
           VSS_ISTREAM_CMD_CDMA_SET_ENC_MINMAX_RATE,
           &minmax, sizeof( minmax ) );
    // MVS_PANIC_ON_ERROR( rc );
    if ( rc )
    {
      /* Temporary workaround to handle SSR phase 3. Any APR IPC communication 
       * failure will cause the current command to fail instead of panicing on 
       * failure.
       */
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_voc_set_frame_rate(): " \
              "Failed to communicate with CVS, rc = (0x%08X)",
              rc );

      rc = mvs_free_object( ( mvs_object_t* ) obj );
      MVS_PANIC_ON_ERROR( rc );
    }
  }
  else
  {
    /* Caching in place */
    MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_voc_set_frame_rate(): Caching " \
        "values" );
    return APR_EOK;
  }

  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_voc_set_frame_rate(): complete " \
        "- max (%d) min (%d)",
        max_rate, min_rate );

  return rc;
}

int32_t mvs_mod_voc_tx_rate_limit (
  uint16 rate_factor
)
{
  int32_t rc;
  mvs_packet_type mvs_cmd;
  aprv2_packet_t* packet;

  mvs_cmd.set_cdma_rate_factor.hdr.cmd = MVS_CMD_SET_CDMA_RATE_FACTOR;
  mvs_cmd.set_cdma_rate_factor.rate_factor = rate_factor;

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_mod_tx_rate_limit(): " \
        "rate_factor (%d)",
        rate_factor );

  mvs_new_props.rate_factor = rate_factor;

  if ( mvs_cvs_port != APR_NULL_V  )
  {
    rc = mvs_make_cmd_packet( &mvs_cmd, &packet );

    if ( rc )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "make pkt failed!!" );
      return APR_ENORESOURCE;
    }

    ( void ) mvs_queue_incoming_packet( packet );
  }

  return APR_EOK;
}

int32_t mvs_mod_voc_tx_rate_limit_proc (
  uint16 rate_factor,
  boolean force_write_through
)
{
  int32_t rc = APR_EOK;
  mvs_token_object_t* obj;
  uint16_t rate;
  vss_istream_cmd_voc_qcelp13k_set_rate_t qcelp13k_rate;
  vss_istream_cmd_voc_4gvnb_set_rate_t fgvnb_rate;
  vss_istream_cmd_voc_4gvwb_set_rate_t fgvwb_rate;
  vss_istream_cmd_voc_4gvnw_set_rate_t fgvnw_rate;
  vss_istream_cmd_cdma_set_enc_rate_modulation_t rate_mod;

  mvs_curr_props.rate_factor = rate_factor;

  if ( mvs_cur_client != MVS_CLIENT_NONE )
  {
    if ( MVS_MODE_NONE == mvs_client_table[mvs_cur_client].config_mode )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "mvs_mod_voc_tx_rate_limit(): MVS " \
           "has no config mode" );
      return APR_EOK;
    }
  }
  else
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "mvs_mod_voc_tx_rate_limit(): There " \
         "is no current client" );
    return APR_EOK;
  }

  if ( mvs_client_table[mvs_cur_client].state == MVS_STATE_VOCODER ||
       force_write_through )
  {
    { /* Historical notes:
       *
       *    MCCTC.C shifts the rate reduced mode field up a bit. This works
       *    for IS-96(A). TR-45 (13kbps) needs a value of 0..4 for its' rate
       *    reduced mode, so we compensate for the shift here.
       *
       * Translation:
       *
       *    1X modem code passes us the rate reduced level left-shifted by 2.
       */
      rate = ( ( rate_factor >> 1 ) & 7 );
        /* The mode is stored in b3..b1 of the service option control message
         * field.
         */
    }

    /* Send reduce rate level (average bit-rate) to the vocoder. */
    switch ( mvs_client_table[mvs_cur_client].config_mode )
    {
    case MVS_MODE_IS733:
      switch ( rate )
      {
      case 0: /* 14.4 kbps */
      case 1: /* 12.2 kbps */
      case 2: /* 11.2 kbps */
      case 3: /*  9.0 kbps */
      case 4: /*  7.2 kbps */
        qcelp13k_rate.rate = rate;
        break;

      default:
        return APR_EBADPARAM;
      }

      rc = mvs_create_autofree_token_object( &obj );
      MVS_PANIC_ON_ERROR( rc );

      rc = __aprv2_cmd_alloc_send(
             mvs_my_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             mvs_my_addr, MVS_PORT,
             mvs_cvs_addr, mvs_cvs_port,
             obj->header.handle, VSS_ISTREAM_CMD_VOC_QCELP13K_SET_RATE,
             &qcelp13k_rate, sizeof( qcelp13k_rate ) );
      // MVS_PANIC_ON_ERROR( rc );
      if ( rc )
      {
        /* Temporary workaround to handle SSR phase 3. Any APR IPC communication 
         * failure will cause the current command to fail instead of panicing on 
         * failure.
         */
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                "mvs_mod_voc_tx_rate_limit(): Failed to communicate with CVS, " \
                "rc = (0x%08X)",
                rc );

        rc = mvs_free_object( ( mvs_object_t* ) obj );
        MVS_PANIC_ON_ERROR( rc );
      }
      break;

    case MVS_MODE_4GV_NB:
      switch ( rate )
      {
      case 0: /* 10.0 kbps */
      case 1: /*  8.5 kbps */
      case 2: /*  7.5 kbps */
      case 3: /*  7.0 kbps */
      case 4: /*  6.6 kbps */
      case 5: /*  6.2 kbps */
      case 6: /*  5.8 kbps */
      case 7: /*  4.8 kbps */
        fgvnb_rate.rate = rate;
        break;

      default:
        return APR_EBADPARAM;
      }

      rc = mvs_create_autofree_token_object( &obj );
      MVS_PANIC_ON_ERROR( rc );

      rc = __aprv2_cmd_alloc_send(
             mvs_my_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             mvs_my_addr, MVS_PORT,
             mvs_cvs_addr, mvs_cvs_port,
             obj->header.handle, VSS_ISTREAM_CMD_VOC_4GVNB_SET_RATE,
             &fgvnb_rate, sizeof( fgvnb_rate ) );
      // MVS_PANIC_ON_ERROR( rc );
      if ( rc )
      {
        /* Temporary workaround to handle SSR phase 3. Any APR IPC communication 
         * failure will cause the current command to fail instead of panicing on 
         * failure.
         */
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                "mvs_mod_voc_tx_rate_limit(): Failed to communicate with CVS, " \
                "rc = (0x%08X)",
                rc );

        rc = mvs_free_object( ( mvs_object_t* ) obj );
        MVS_PANIC_ON_ERROR( rc );
      }
      break;

    case MVS_MODE_4GV_WB:
      switch ( rate )
      {
      case 0: /*  8.5 kbps */
      case 4: /* 10.0 kbps */
      case 7: /*  4.8 kbps */
        fgvwb_rate.rate = rate;
        break;

      default:
        return APR_EBADPARAM;
      }

      rc = mvs_create_autofree_token_object( &obj );
      MVS_PANIC_ON_ERROR( rc );

      rc = __aprv2_cmd_alloc_send(
             mvs_my_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             mvs_my_addr, MVS_PORT,
             mvs_cvs_addr, mvs_cvs_port,
             obj->header.handle, VSS_ISTREAM_CMD_VOC_4GVWB_SET_RATE,
             &fgvwb_rate, sizeof( fgvwb_rate ) );
      // MVS_PANIC_ON_ERROR( rc );
      if ( rc )
      {
        /* Temporary workaround to handle SSR phase 3. Any APR IPC communication 
         * failure will cause the current command to fail instead of panicing on 
         * failure.
         */
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                "mvs_mod_voc_tx_rate_limit(): Failed to communicate with CVS, " \
                "rc = (0x%08X)",
                rc );

        rc = mvs_free_object( ( mvs_object_t* ) obj );
        MVS_PANIC_ON_ERROR( rc );
      }
      break;

    case MVS_MODE_4GV_NW:
      switch ( rate )
      {
      case 0: /*  8.5 kbps */
      case 1: /*  9.3 kbps */
      case 2: /*  7.5 kbps */
      case 3: /*  7.0 kbps */
      case 4: /*  6.6 kbps */
      case 5: /*  6.2 kbps */
      case 6: /*  5.8 kbps */
      case 7: /*  4.8 kbps */
        fgvnw_rate.rate = rate;
        break;

      default:
        return APR_EBADPARAM;
      }

      rc = mvs_create_autofree_token_object( &obj );
      MVS_PANIC_ON_ERROR( rc );

      rc = __aprv2_cmd_alloc_send(
             mvs_my_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             mvs_my_addr, MVS_PORT,
             mvs_cvs_addr, mvs_cvs_port,
             obj->header.handle, VSS_ISTREAM_CMD_VOC_4GVNW_SET_RATE,
             &fgvnw_rate, sizeof( fgvnw_rate ) );
      // MVS_PANIC_ON_ERROR( rc );
      if ( rc )
      {
        /* Temporary workaround to handle SSR phase 3. Any APR IPC communication 
         * failure will cause the current command to fail instead of panicing on 
         * failure.
         */
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                "mvs_mod_voc_tx_rate_limit(): Failed to communicate with CVS, " \
                "rc = (0x%08X)",
                rc );

        rc = mvs_free_object( ( mvs_object_t* ) obj );
        MVS_PANIC_ON_ERROR( rc );
      }
      break;

    default:
      break;
    }

    /* Send rate modulation to the vocoder.
     *
     * The vocoder rate modulation equation is:
     *
     *              2X+1
     *    ratio = --------
     *             2(X+1)
     *
     * where X=S when in select S mode and X=1/S when in select 1/S mode. S is
     * the rate limit factor. S is an integer which causes the ratio, when
     * multiplied by the 14.4 kbps (full rate), to be the desired average
     * bitrate.
     *
     * Refer to `rateModulationCmd' in the "6550 VDSP Firmware Interface
     * Specification" for further documentation.
     *
     * The mode is the `rateModulationCmd'. The mode's bit structure is:
     *
     *        b0 = Vocoder rate modulation enabled when 1 and disabled when 0.
     *        b1 = Select S when 1 and select 1/S when 0.
     *    b9..b2 = Rate limit factor is the value of S.
     */
    switch ( mvs_client_table[mvs_cur_client].config_mode )
    {
    case MVS_MODE_IS127:
      switch ( rate )
      {
      case 0x0000: /* Target 14.4 kbps (8/8 rate) on the average. */
        rate_mod.mode = 0x0000;
        /* Bitmasks:
         *    0x0000 = Vocoder rate modulation disabled.
         */
        break;

      case 0x0001: /* Target 12.2 kbps (7/8 rate) on the average. */
        rate_mod.mode = 0x000F;
        /* Bitmasks:
         *    0x0001 = Vocoder rate modulation enabled.
         *    0x0002 = Select S.
         *    0x000C = Rate limit factor S=3.
         */
        break;

      case 0x0002: /* Target 11.2 kbps (6/8 rate) on the average. */
        rate_mod.mode = 0x0007;
        /* Bitmasks:
         *    0x0001 = Vocoder rate modulation enabled.
         *    0x0002 = Select S.
         *    0x0004 = Rate limit factor S=1.
         */
        break;

      case 0x0003: /* Target 9.0 kbps (5/8 rate) on the average. */
        rate_mod.mode = 0x0005;
        /* Bitmasks:
         *    0x0001 = Vocoder rate modulation enabled.
         *    0x0000 = Select 1/S.
         *    0x0004 = Rate limit factor S=3.
         */
        break;

      case 0x0004: /* Target 7.2 kbps (4/8 rate) on the average. */
        rate_mod.mode = 0x0000;
        /* Bitmasks:
         *    0x0000 = Vocoder rate modulation disabled. (Not supported).
         */
        break;

      default:
        return APR_EBADPARAM;
      }

      rc = mvs_create_autofree_token_object( &obj);
      MVS_PANIC_ON_ERROR( rc );

      rc = __aprv2_cmd_alloc_send(
             mvs_my_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             mvs_my_addr, MVS_PORT,
             mvs_cvs_addr, mvs_cvs_port,
             obj->header.handle, VSS_ISTREAM_CMD_CDMA_SET_ENC_RATE_MODULATION,
             &rate_mod, sizeof( rate_mod ) );
      // MVS_PANIC_ON_ERROR( rc );
      if ( rc )
      {
        /* Temporary workaround to handle SSR phase 3. Any APR IPC communication 
         * failure will cause the current command to fail instead of panicing on 
         * failure.
         */
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                "mvs_mod_voc_tx_rate_limit(): Failed to communicate with CVS, " \
                "rc = (0x%08X)",
                rc );

        rc = mvs_free_object( ( mvs_object_t* ) obj );
        MVS_PANIC_ON_ERROR( rc );
      }
      break;

    default:
      break;
    }
  }
  else
  {
    /* Caching in place */
    MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_mod_voc_tx_rate_limit(): " \
        "Caching values" );
    return APR_EOK;
  } /* if ( mvs_client_table[mvs_cur_client].state == MVS_STATE_VOCODER ||
            force_write_through ) */

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_voc_tx_rate_limit(): complete - " \
        "rate-factor (%d)",
        rate_factor );

  return rc;
}

int32_t mvs_mod_set_dtx_mode (
  mvs_dtx_mode_type dtx_mode
)
{
  int32_t rc;
  mvs_packet_type mvs_cmd;
  aprv2_packet_t* packet = NULL;

  mvs_cmd.set_dtx_mode.hdr.cmd = MVS_CMD_SET_DTX_MODE;
  mvs_cmd.set_dtx_mode.dtx_mode = dtx_mode;

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_mode_set_dtx_mode(): dtx_mode " \
        "(%d)",
        dtx_mode );

  mvs_new_props.dtx_mode = dtx_mode;

  if ( mvs_cvs_port != APR_NULL_V  )
  {
    rc = mvs_make_cmd_packet( &mvs_cmd, &packet );
    if ( rc )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "make pkt failed!!" );
      return APR_ENORESOURCE;
    }

    ( void ) mvs_queue_incoming_packet( packet );
  }

  return APR_EOK;
}

int32_t mvs_mod_set_dtx_mode_proc (
  mvs_dtx_mode_type dtx_mode,
  boolean force_write_through
)
{
  int32_t rc = APR_EOK;
  mvs_token_object_t* obj;
  vss_istream_cmd_set_enc_dtx_mode_t set_dtx_mode;

  set_dtx_mode.enable = ( dtx_mode ? 1 : 0 );

  if ((mvs_curr_props.dtx_mode == dtx_mode) && (mvs_cached_set == 0) )
  {
    /* Caching in place */
    MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "mvs_mod_set_dtx_mode(): No change " \
        "in value, not sending to vocoder" );
    return APR_EOK;
  }

  mvs_curr_props.dtx_mode = dtx_mode;

  if ( mvs_cur_client != MVS_CLIENT_NONE )
  {
    if ( MVS_MODE_NONE == mvs_client_table[mvs_cur_client].config_mode )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "mvs_mod_set_dtx_mode(): MVS has " \
           "no config mode" );
      return APR_EOK;
    }
  }
  else
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "mvs_mod_set_dtx_mode(): There is " \
         "no current client" );
    return APR_EOK;
  }

  if ( mvs_client_table[mvs_cur_client].state == MVS_STATE_VOCODER ||
       force_write_through )
  {
    rc = mvs_create_autofree_token_object( &obj);
    MVS_PANIC_ON_ERROR( rc );

    rc = __aprv2_cmd_alloc_send(
           mvs_my_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
           mvs_my_addr, MVS_PORT,
           mvs_cvs_addr, mvs_cvs_port,
           obj->header.handle, VSS_ISTREAM_CMD_SET_ENC_DTX_MODE,
           &set_dtx_mode, sizeof( set_dtx_mode ) );
    // MVS_PANIC_ON_ERROR( rc );
    if ( rc )
    {
      /* Temporary workaround to handle SSR phase 3. Any APR IPC communication 
       * failure will cause the current command to fail instead of panicing on 
       * failure.
       */
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_mod_set_dtx_mode(): " \
              "Failed to communicate with CVS, rc = (0x%08X)",
              rc );

      rc = mvs_free_object( ( mvs_object_t* ) obj );
      MVS_PANIC_ON_ERROR( rc );
    }
  }
  else
  {
    /* Caching in place */
    MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_mod_set_dtx_mode(): Caching " \
        "values" );
    return APR_EOK;
  }

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_gsm_set_dtx_mode() complete - " \
        "dtx_mode (%d)",
        dtx_mode );

  return rc;
}

int32_t mvs_mod_amr_set_scr_mode (
  mvs_scr_mode_type scr_mode
)
{
  int32_t rc;

  rc = mvs_mod_set_dtx_mode( (mvs_dtx_mode_type)scr_mode );
  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "mvs_amr_set_scr_mode(): " \
       "scr_mode=(%d), rc=(%d)",
       scr_mode, rc);
  return rc;
}

int32_t mvs_mod_amr_set_awb_mode (
  mvs_amr_mode_type awb_ul_mode
)
{
  int32_t rc;
  mvs_packet_type mvs_cmd;
  aprv2_packet_t* packet = NULL;

  mvs_cmd.set_amr_mode.hdr.cmd = MVS_CMD_SET_AWB_MODE;
  mvs_cmd.set_amr_mode.amr_mode = awb_ul_mode;

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_mod_amr_set_awb_mode(): " \
        "awb_ul_mode (%d)",
        awb_ul_mode );

  mvs_new_props.awb_ul_mode = awb_ul_mode;

  if ( mvs_cvs_port != APR_NULL_V  )
  {
    rc = mvs_make_cmd_packet( &mvs_cmd, &packet );
    if ( rc )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "make pkt failed!!" );
      return APR_ENORESOURCE;
    }

    ( void ) mvs_queue_incoming_packet( packet );
  }

  return APR_EOK;
}

int32_t mvs_mod_amr_set_awb_mode_proc (
  mvs_amr_mode_type awb_ul_mode,
  boolean force_write_through
)
{
  int32_t rc = APR_EOK;
  mvs_token_object_t* obj;
  vss_istream_cmd_voc_amrwb_set_enc_rate_t set_awb_mode;

  if ( MVS_AMR_MODE_MAX == awb_ul_mode ||
       MVS_AMR_MODE_0660 > awb_ul_mode )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "mvs_mod_amr_set_awb_mode(): " \
           "AMR_WB mode has not been set on MVS. Setting (%d)",
           MVS_AMR_MODE_2385 );
    awb_ul_mode =  MVS_AMR_MODE_2385;
  }

  if ((mvs_curr_props.awb_ul_mode == awb_ul_mode) && (mvs_cached_set == 0) )
  {
    /* Caching in place */
    MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "mvs_mod_amr_set_awb_mode(): No " \
        "change in value, not sending to vocoder" );
    return APR_EOK;
  }

  mvs_curr_props.awb_ul_mode = awb_ul_mode;

  if ( mvs_cur_client != MVS_CLIENT_NONE )
  {
    if ( MVS_MODE_NONE == mvs_client_table[mvs_cur_client].config_mode )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "mvs_mod_amr_set_awb_mode(): MVS " \
           "has no config mode" );
      return APR_EOK;
    }
  }
  else
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "mvs_mod_amr_set_awb_mode(): There " \
         "is no current client" );
    return APR_EOK;
  }

  if ( mvs_client_table[mvs_cur_client].state == MVS_STATE_VOCODER ||
       force_write_through )
  {
    rc = mvs_create_autofree_token_object( &obj );
    MVS_PANIC_ON_ERROR( rc );

    set_awb_mode.mode = map_to_cvd_awb_mode( awb_ul_mode );

    rc = __aprv2_cmd_alloc_send(
           mvs_my_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
           mvs_my_addr, MVS_PORT,
           mvs_cvs_addr, mvs_cvs_port,
           obj->header.handle, VSS_ISTREAM_CMD_VOC_AMRWB_SET_ENC_RATE,
           &set_awb_mode, sizeof( set_awb_mode ) );
    // MVS_PANIC_ON_ERROR( rc );
    if ( rc )
    {
      /* Temporary workaround to handle SSR phase 3. Any APR IPC communication 
       * failure will cause the current command to fail instead of panicing on 
       * failure.
       */
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_mod_amr_set_awb_mode(): " \
              "Failed to communicate with CVS, rc = (0x%08X)",
              rc );

      rc = mvs_free_object( ( mvs_object_t* ) obj );
      MVS_PANIC_ON_ERROR( rc );
    }

    amrsup_set_default_dl_amr_mode( (amrsup_mode_type) mvs_curr_props.awb_ul_mode );
  }
  else
  {
    /* Caching in place */
    MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_mod_amr_set_awb_mode(): Caching " \
        "values" );
    return APR_EOK;
  }

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_mod_amr_set_awb_mode(): " \
        "complete awb_ul_mode=(%d)",
        awb_ul_mode );

  return rc;
}

int32_t mvs_mod_amr_set_amr_mode (
  mvs_amr_mode_type amr_ul_mode
)
{
  int32_t rc;
  mvs_packet_type mvs_cmd;
  aprv2_packet_t* packet;

  mvs_cmd.set_amr_mode.hdr.cmd = MVS_CMD_SET_AMR_MODE;
  mvs_cmd.set_amr_mode.amr_mode = amr_ul_mode;

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_mod_amr_set_amr_mode(): " \
        "amr_ul_mode (%d)",
        amr_ul_mode );

  mvs_new_props.amr_ul_mode = amr_ul_mode;

  if ( mvs_cvs_port != APR_NULL_V  )
  {
    rc = mvs_make_cmd_packet( &mvs_cmd, &packet );
    if ( rc )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "make pkt failed!!" );
      return APR_ENORESOURCE;
    }

    ( void ) mvs_queue_incoming_packet( packet );
  }

  return APR_EOK;
}

int32_t mvs_mod_amr_set_amr_mode_proc (
  mvs_amr_mode_type amr_ul_mode,
  boolean force_write_through
)
{
  int32_t rc = APR_EOK;
  mvs_token_object_t* obj;
  vss_istream_cmd_voc_amr_set_enc_rate_t set_amr_mode;

  if ( MVS_AMR_MODE_MAX == amr_ul_mode ||
       MVS_AMR_MODE_1220 < amr_ul_mode )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "mvs_mod_gsm_set_amr_mode(): AMR " \
           "mode has not been set on MVS. Setting (%d)",
           MVS_AMR_MODE_1220 );
    amr_ul_mode =  MVS_AMR_MODE_1220;
  }

  if ((mvs_curr_props.amr_ul_mode == amr_ul_mode) && (mvs_cached_set == 0) )
  {
     MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "mvs_mod_gsm_set_amr_mode(): No " \
         "change in value, not sending to vocoder");
     return APR_EOK;
  }

  mvs_curr_props.amr_ul_mode = amr_ul_mode;

  if ( mvs_cur_client != MVS_CLIENT_NONE )
  {
    if ( MVS_MODE_NONE == mvs_client_table[mvs_cur_client].config_mode )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "mvs_mod_gsm_set_amr_mode(): MVS " \
           "has no config mode" );
      return APR_EOK;
    }
  }
  else
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "mvs_mod_gsm_set_amr_mode(): There " \
         "is no current client" );
    return APR_EOK;
  }

  if ( mvs_client_table[mvs_cur_client].state == MVS_STATE_VOCODER ||
       force_write_through )
  {
    rc = mvs_create_autofree_token_object( &obj );
    MVS_PANIC_ON_ERROR( rc );

    set_amr_mode.mode = ( ( uint32_t ) amr_ul_mode );

    rc = __aprv2_cmd_alloc_send(
           mvs_my_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
           mvs_my_addr, MVS_PORT,
           mvs_cvs_addr, mvs_cvs_port,
           obj->header.handle, VSS_ISTREAM_CMD_VOC_AMR_SET_ENC_RATE,
           &set_amr_mode, sizeof( set_amr_mode ) );
    // MVS_PANIC_ON_ERROR( rc );
    if ( rc )
    {
      /* Temporary workaround to handle SSR phase 3. Any APR IPC communication 
       * failure will cause the current command to fail instead of panicing on 
       * failure.
       */
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_mod_gsm_set_amr_mode(): " \
              "Failed to communicate with CVS, rc = (0x%08X)",
              rc );

      rc = mvs_free_object( ( mvs_object_t* ) obj );
      MVS_PANIC_ON_ERROR( rc );
    }

    amrsup_set_default_dl_amr_mode( (amrsup_mode_type) mvs_curr_props.amr_ul_mode );
  }
  else
  {
    /* Caching in place */
    MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_mod_gsm_set_amr_mode(): Caching " \
        "values" );
    return APR_EOK;
  }

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_amr_set_amr_mode(): complete - " \
        "amr_mode=(%d)",
        amr_ul_mode );

  return rc;
}

void mvs_mod_set_g711_mode (
  mvs_g711_mode_type mode
)
{
  mvs_current_g711_mode = mode;
}

mvs_g711_mode_type mvs_mod_get_g711_mode ( void )
{
  return mvs_current_g711_mode;
}
/* not implemented in FW not functional */
mvs_g722_mode_type mvs_mod_get_g722_mode ( void )
{
  return mvs_current_g722_mode;
}

void mvs_mod_set_g722_mode (
  mvs_g722_mode_type mode
)
{
  /* not suppoprted alway set max */
  mvs_current_g722_mode = MVS_G722_MODE_MAX;
}



static int32_t mvs_mod_set_voice_timing_proc ( void )
{
  int32_t rc = APR_EOK;
  vss_icommon_cmd_set_voice_timing_t timing_param;
  mvs_token_object_t* obj;

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_mod_set_voice_timing_proc(): " \
        "client (%d)",
        mvs_cur_client );
  
  /* Set timing parameters. */
  switch ( mvs_cur_client )
  {
  case MVS_CLIENT_GSM:
    {
      timing_param.enc_offset     = mvs_timing_param_gsm.enc_offset;
      timing_param.dec_req_offset = mvs_timing_param_gsm.dec_req_offset;
      timing_param.dec_offset     = mvs_timing_param_gsm.dec_offset;
    }
    break;
    
  case MVS_CLIENT_WCDMA:
    {
      timing_param.enc_offset     = mvs_timing_param_wcdma.enc_offset;
      timing_param.dec_req_offset = mvs_timing_param_wcdma.dec_req_offset;
      timing_param.dec_offset     = mvs_timing_param_wcdma.dec_offset;
    }
    break;

  case MVS_CLIENT_CDMA:
    {
      timing_param.enc_offset     = mvs_timing_param_cdma.enc_offset;
      timing_param.dec_req_offset = mvs_timing_param_cdma.dec_req_offset;
      timing_param.dec_offset     = mvs_timing_param_cdma.dec_offset;
    }
    break;
  case MVS_CLIENT_TDSCDMA:
    {
      timing_param.enc_offset     = mvs_timing_param_tdscdma.enc_offset;
      timing_param.dec_req_offset = mvs_timing_param_tdscdma.dec_req_offset;
      timing_param.dec_offset     = mvs_timing_param_tdscdma.dec_offset;
    }
    break;

  default:
    {
      timing_param.enc_offset     = mvs_timing_param_default.enc_offset;
      timing_param.dec_req_offset = mvs_timing_param_default.dec_req_offset;
      timing_param.dec_offset     = mvs_timing_param_default.dec_offset;
    }
    break;
  }

  /* Set frame sync mode */
  switch ( mvs_cur_client )
  {
  case MVS_CLIENT_CDMA:
  case MVS_CLIENT_WCDMA:
  case MVS_CLIENT_TDSCDMA:
  case MVS_CLIENT_GSM:
  case MVS_CLIENT_QVP:
    timing_param.mode = 1; /* Hard VFR */
    break;

  default:
    timing_param.mode = 0; /* No frame sync */
    break;
  }

  rc = mvs_create_autofree_token_object( &obj);
  MVS_PANIC_ON_ERROR( rc );

  rc = __aprv2_cmd_alloc_send(
         mvs_my_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         mvs_my_addr, MVS_PORT,
         mvs_mvm_addr, mvs_mvm_port,
         obj->header.handle,
         VSS_ICOMMON_CMD_SET_VOICE_TIMING,
         &timing_param, sizeof( timing_param ) );
  // MVS_PANIC_ON_ERROR( rc );
  if ( rc )
  {
    /* Temporary workaround to handle SSR phase 3. Any APR IPC communication 
     * failure will cause the current command to fail instead of panicing on 
     * failure.
     */
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
            "mvs_mod_set_voice_timing_proc(): Failed to communicate with MVM, " \
            "rc = (0x%08X)",
            rc );

    rc = mvs_free_object( ( mvs_object_t* ) obj );
    MVS_PANIC_ON_ERROR( rc );
  }

  return APR_EOK;
}

static int32_t mvs_mod_set_cal_network_proc ( void )
{
  int32_t rc;
  vss_imvm_cmd_set_cal_network_t set_network;
  mvs_token_object_t* obj;

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_mod_set_cal_network_proc(): " \
        "client (%d)",
        mvs_cur_client );

  set_network.network_id = map_to_cvd_network( mvs_cur_client );

  rc = mvs_create_autofree_token_object( &obj );
  MVS_PANIC_ON_ERROR( rc );

  rc = __aprv2_cmd_alloc_send(
         mvs_my_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         mvs_my_addr, MVS_PORT,
         mvs_mvm_addr, mvs_mvm_port,
         obj->header.handle, VSS_IMVM_CMD_SET_CAL_NETWORK,
         &set_network, sizeof( set_network ) );
  // MVS_PANIC_ON_ERROR( rc );
  if ( rc )
  {
    /* Temporary workaround to handle SSR phase 3. Any APR IPC communication 
     * failure will cause the current command to fail instead of panicing on 
     * failure.
     */
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_mod_set_cal_network_proc(): " \
            "Failed to communicate with MVM, rc = (0x%08X)",
            rc );

    rc = mvs_free_object( ( mvs_object_t* ) obj );
    MVS_PANIC_ON_ERROR( rc );
  }

  return APR_EOK;
}

static int32_t mvs_mod_set_cal_media_type_proc ( void )
{
  int32_t rc;
  vss_imvm_cmd_set_cal_media_type_t set_media_type;
  mvs_token_object_t* obj;

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_mod_set_cal_media_type_proc(): " \
        "client (%d)",
        mvs_cur_client );

  set_media_type.media_id = map_to_cvd_mode( 
                              mvs_client_table[ mvs_cur_client ].config_mode );

  rc = mvs_create_autofree_token_object( &obj );
  MVS_PANIC_ON_ERROR( rc );

  rc = __aprv2_cmd_alloc_send(
         mvs_my_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         mvs_my_addr, MVS_PORT,
         mvs_mvm_addr, mvs_mvm_port,
         obj->header.handle, VSS_IMVM_CMD_SET_CAL_MEDIA_TYPE,
         &set_media_type, sizeof( set_media_type ) );
  // MVS_PANIC_ON_ERROR( rc );
  if ( rc )
  {
    /* Temporary workaround to handle SSR phase 3. Any APR IPC communication 
     * failure will cause the current command to fail instead of panicing on 
     * failure.
     */
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
            "mvs_mod_set_cal_media_type_proc(): Failed to communicate with " \
            "MVM, rc = (0x%08X)",
            rc );

    rc = mvs_free_object( ( mvs_object_t* ) obj );
    MVS_PANIC_ON_ERROR( rc );
  }

  return APR_EOK;
}

/* This function is called in modem ISR context by voiceapp, This function takes VSID as an input
 * and moves the processing to MVS context, It would call voiceapp_process_command function
 * with same vsid in MVS context. 
 */
static uint32_t mvs_process_voiceapp_fn
(
  uint32_t vsid
)
{
  uint32_t rc = APR_EOK;
  mvs_packet_type mvs_cmd;
  aprv2_packet_t* packet = NULL;

  for ( ;; )
  {
    /* Wait till mvs_pkt_miss_count reaches threshold, once it reaches threshold start pumping
     * silence frame. 
     */
    if ( mvs_pkt_miss_count < MVS_PACKET_MISS_THRESHOLD_FOR_PUMPING_SILENCE_FRAME  )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "mvs_process_voiceapp_fn():mvs_pkt_miss_count: (0x%08X) is less than threshold: (0x%08X)", 
             mvs_pkt_miss_count, MVS_PACKET_MISS_THRESHOLD_FOR_PUMPING_SILENCE_FRAME );

      mvs_pkt_miss_count++;
      rc = APR_EOK;
      break;
    }

    mvs_pkt_miss_count++;
    
    if ( mvs_is_initialized )
    {
      mvs_cmd.process_voiceapp.hdr.cmd = MVS_CMD_PROCESS_VOICEAPP;
      mvs_cmd.process_voiceapp.vsid = vsid;

      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,
             "mvs_process_voiceapp_fn():vsid = (0x%08X)", vsid );


      rc = mvs_make_cmd_packet( &mvs_cmd, &packet );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "mvs_process_voiceapp_fn():make pkt failed!, rc = 0x%08X", rc );
        break;
      }

      if ( mvs_queue_incoming_packet( packet ) == FALSE )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "mvs_process_voiceapp_fn():mvs_queue_incoming_packet failed" ); 
        rc = APR_EFAILED;
        break;
      }
      
      break;
    }
    else
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, 
           "mvs_process_voiceapp_fn(): MVS is not initialized" );
      rc = APR_EFAILED;
      break;
    }
  }

  return rc;
}

static int32_t mvs_set_cached_values ( void )
{
  mvs_cached_set = 1;

  switch ( mvs_client_table[mvs_cur_client].config_mode )
  {
  case MVS_MODE_AMR:
    //set amr_mode
    ( void ) mvs_mod_amr_set_amr_mode_proc( mvs_new_props.amr_ul_mode, TRUE );
  case MVS_MODE_EFR:
  case MVS_MODE_HR:
  case MVS_MODE_FR:
    //set dtx_mode
    ( void ) mvs_mod_set_dtx_mode_proc( mvs_new_props.dtx_mode, TRUE ) ;
    break;
  case MVS_MODE_IS127:
  case MVS_MODE_IS733:
    //set rate limit
    ( void ) mvs_mod_voc_tx_rate_limit_proc( mvs_new_props.rate_factor, TRUE );
    ( void ) mvs_mod_voc_set_frame_rate_proc( mvs_new_props.max_voc_rate, mvs_new_props.min_voc_rate, TRUE );
    break;
  case MVS_MODE_4GV_NB:
  case MVS_MODE_4GV_WB:
  case MVS_MODE_4GV_NW:
    //set rate limit
    ( void ) mvs_mod_voc_tx_rate_limit_proc( mvs_new_props.rate_factor, TRUE );
    ( void ) mvs_mod_voc_set_frame_rate_proc( mvs_new_props.max_voc_rate, mvs_new_props.min_voc_rate, TRUE );
    //set dtx_mode
    ( void ) mvs_mod_set_dtx_mode_proc( mvs_new_props.dtx_mode, TRUE ) ;
    break;
  case MVS_MODE_AMR_WB:
    //set amr_wb
    ( void ) mvs_mod_amr_set_awb_mode_proc( mvs_new_props.awb_ul_mode, TRUE );
    //set dtx_mode
    ( void ) mvs_mod_set_dtx_mode_proc( mvs_new_props.dtx_mode, TRUE ) ;
    break;
  default:
    /* Do nothing. */
    break;
  }

  mvs_cached_set = 0;

  return APR_EOK;
}

#ifndef WINSIM

static int32_t mvs_extract_channels_from_if1 (
  uint8 *AMRIF1Packet,
  amrsup_core_speech_type *speech_data
)
{
  int nResult = APR_EOK;
  int bitsReadForClass ;
  uint8* pbyDest  ;
  int32 CurrbitRead ;
  int32 CurrBitWrite ;
  int32 CurrByteRead ;
  int32 CurrByteWrite ;
  uint8* pbySrc ;
  uint8 pbyDesC ;
  uint8 CurrByte;
  uint8 CurrBit;
  int writeBitPos;

  MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_extract_channels_from_if1()" );

  if ( AMRIF1Packet == NULL )
  {
    return APR_EBADPARAM;
  }

  //uint32 ClassABitsOffset = AmrFrameInfo.AMRCoreDataOffset ;
  memset((void*)(speech_data->data_a), 0, AMRSUP_CLASS_A_BYTES);
  memset((void*)(speech_data->data_b), 0, AMRSUP_CLASS_B_BYTES);
  memset((void*)(speech_data->data_c), 0, AMRSUP_CLASS_C_BYTES);

  bitsReadForClass = 0 ;
  pbyDest = speech_data->data_a ;
  CurrbitRead = 0 ;
  CurrBitWrite = 0 ;
  CurrByteRead = 0 ;
  CurrByteWrite = 0 ;
  pbySrc = NULL;
  pbySrc = AMRIF1Packet ;
  pbyDesC = 0x0;

  while ( bitsReadForClass < speech_data->num_bits_a )
  {
    CurrbitRead = 0 ;
    CurrBitWrite = 0 ;

    CurrByte = *(pbySrc + CurrByteRead) ;
    //munch 8 bits or if u have reached your quota for this Class of bits
    //change
    while ( (CurrbitRead < 8) && (bitsReadForClass < speech_data->num_bits_a) )
    {
      CurrBit = mvs_MaskArray[7-CurrbitRead] & CurrByte ; // got a bit here
      pbyDesC = pbyDesC | CurrBit ;            // put it together
      CurrbitRead++ ;
      CurrBitWrite++ ;
      bitsReadForClass++ ;
      if ( CurrBitWrite == 8 )
      {
        *(pbyDest + CurrByteWrite) = pbyDesC ;
        CurrBitWrite = 0 ;
        CurrByteWrite++ ;
        pbyDesC  = 0x0 ;
      }

    }

    if ( CurrBitWrite == 8 )
    {
      *(pbyDest + CurrByteWrite) = pbyDesC ;
      CurrBitWrite = 0 ;
      CurrByteWrite++ ;
      pbyDesC = 0x0;
    }
    if ( CurrbitRead==8 )
    {
      CurrByteRead++ ;
      CurrbitRead = 0 ;
    }


  }

  bitsReadForClass = 0 ;
  *(pbyDest + CurrByteWrite) = pbyDesC ;
  pbyDesC = 0x0 ;
  CurrByteWrite = 0 ; // destination is changed
  CurrBitWrite = 0 ;
  pbyDest = speech_data->data_b ;
  //uint32 pRemain = CurrbitRead ;
  //CurrByteRead is not moved ahead

  while ( bitsReadForClass < speech_data->num_bits_b )
  {

    CurrByte = *(pbySrc + CurrByteRead) ;
    //munch 8 bits or if u have reached your quota for this Class of bits
    while ( (CurrbitRead < 8) && (bitsReadForClass < speech_data->num_bits_b) )
    {
      CurrBit = mvs_MaskArray[7-CurrbitRead] & CurrByte ;
      writeBitPos = CurrbitRead-CurrBitWrite ;
      if ( writeBitPos>0 )
      {
        CurrBit = CurrBit << (CurrbitRead-CurrBitWrite) ;
      }
      else
      {
        CurrBit = CurrBit >> (CurrBitWrite-CurrbitRead) ;
      }

      pbyDesC = pbyDesC | CurrBit ;
      CurrbitRead++ ;
      CurrBitWrite++ ;
      bitsReadForClass++ ;
      if ( CurrBitWrite == 8 )
      {
        *(pbyDest + CurrByteWrite) = pbyDesC ;
        CurrBitWrite = 0 ;
        CurrByteWrite++ ;
        pbyDesC  = 0x0 ;
      }

    }

    if ( CurrbitRead == 8 )
    {
      CurrbitRead = 0 ;
      CurrByteRead++ ;
    }

    if ( CurrBitWrite == 8 )
    {
      *(pbyDest + CurrByteWrite) = pbyDesC ;
      CurrBitWrite = 0 ;
      CurrByteWrite++ ;
      pbyDesC  = 0x0 ;
    }


  }
  bitsReadForClass = 0 ;
  *(pbyDest + CurrByteWrite) = pbyDesC ;
  pbyDesC = 0x0 ;
  CurrByteWrite = 0 ; // destination is changed
  CurrBitWrite = 0 ;
  pbyDest = speech_data->data_c ;
  //CurrByteRead is not moved ahead

  while ( bitsReadForClass < speech_data->num_bits_c )
  {
    CurrByte = *(pbySrc + CurrByteRead) ;
    //munch 8 bits or if u have reached your quota for this Class of bits
    while ( (CurrbitRead < 8) && (bitsReadForClass < speech_data->num_bits_c) )
    {
      CurrBit = mvs_MaskArray[7-CurrbitRead] & CurrByte ;
      writeBitPos = CurrbitRead-CurrBitWrite ;
      if ( writeBitPos>0 )
      {
        CurrBit = CurrBit << (CurrbitRead-CurrBitWrite) ;
      }
      else
      {
        CurrBit = CurrBit >> (CurrBitWrite-CurrbitRead) ;
      }

      pbyDesC = pbyDesC | CurrBit ;
      CurrbitRead++ ;
      CurrBitWrite++ ;
      bitsReadForClass++ ;
      if ( CurrBitWrite == 8 )
      {
        *(pbyDest + CurrByteWrite) = pbyDesC ;
        CurrBitWrite = 0 ;
        CurrByteWrite++ ;
        pbyDesC  = 0x0 ;
      }
    }

    if ( CurrbitRead == 8 )
    {
      CurrbitRead = 0 ;
      CurrByteRead++ ;
    }

    if ( CurrBitWrite == 8 )
    {
      *(pbyDest + CurrByteWrite) = pbyDesC ;
      CurrBitWrite = 0 ;
      CurrByteWrite++ ;
      pbyDesC  = 0x0 ;
    }

  }

  *(pbyDest + CurrByteWrite) = pbyDesC ;

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_extract_channels_from_if1(): " \
        "complete (%d)",
        nResult );

  return nResult ;

}

static int32_t mvs_make_if1_from_channels (
  uint8 *AMRIF1Packet,
  amrsup_core_speech_type *speech_data,
  int nIF1PacketLength,
  int *nIF1PacketLengthFormed
)
{
  int32_t nResult = APR_EOK;
  int32 CurrByteWrite  = 0 ;
  int32 CurrBitWrite = 0 ;
  uint8 *pbySrc = NULL;
  uint32 bitsReadForClass = 0 ;
  int32 CurrbitRead = 0 ;
  int32 CurrByteRead = 0 ;
  uint8 pbyDesC =0x0;
  uint8* pbyDest = NULL;
  uint8 CurrByte;
  uint8 CurrBit;
  int writeBitPos;


  MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_make_if1_from_channels()" );

  if ( AMRIF1Packet == NULL )
    return APR_EBADPARAM;

  //Initialize vocoder packet to zero
  memset((void*)(AMRIF1Packet), 0, nIF1PacketLength);

  //Check for NO_DATA frame
  if ( speech_data->num_bits_a == 0 &&
       speech_data->num_bits_b == 0 &&
       speech_data->num_bits_c == 0 )
  {
    if ( nIF1PacketLengthFormed != NULL ) *nIF1PacketLengthFormed = 0;
    return APR_EOK;
  }

  CurrByteWrite  = 0 ;
  CurrBitWrite = 0 ;
  pbySrc = NULL;
  bitsReadForClass = 0 ;
  CurrbitRead = 0 ;
  CurrByteRead = 0 ;
  pbyDesC =0x0;
  pbyDest = NULL;

  if ( speech_data->num_bits_a )
  {
    pbySrc = speech_data->data_a ;

    /* pbyDest = (AMRIF1Packet+3)+CurrByteWrite ; */
    pbyDest = (AMRIF1Packet)+CurrByteWrite ;

    while ( bitsReadForClass < speech_data->num_bits_a )
    {
      CurrByte = *(pbySrc + CurrByteRead) ;
      while ( (CurrbitRead<8)&&(bitsReadForClass < speech_data->num_bits_a) )
      {
        CurrBit = mvs_MaskArray[7-CurrbitRead] & CurrByte ;

#if 0 /* TODO: Need to re-enable? */
        int writeBitPos = CurrbitRead-CurrBitWrite ;
        if (writeBitPos>0)
        {
          CurrBit = CurrBit >> (CurrbitRead-CurrBitWrite) ;
        }
        else
        {
          CurrBit = CurrBit << (CurrBitWrite-CurrbitRead) ;
        }
#endif /* 0 */

        pbyDesC = pbyDesC | CurrBit ;
        CurrbitRead++ ;
        CurrBitWrite++ ;
        bitsReadForClass++ ;
        if ( CurrBitWrite == 8 )
        {
          *(pbyDest + CurrByteWrite) = pbyDesC ;
          CurrBitWrite = 0 ;
          CurrByteWrite++ ;
          pbyDesC  = 0x0 ;
        }
      }//process 8 bits

      if ( CurrbitRead == 8 )
      {
        CurrbitRead = 0 ;
        CurrByteRead++ ;
      }

      if ( CurrBitWrite == 8 )
      {
        *(pbyDest + CurrByteWrite) = pbyDesC ;
        CurrBitWrite = 0 ;
        CurrByteWrite++ ;
        pbyDesC  = 0x0 ;
      }
    }//for all A bits

  }//processed all A channel bits
  bitsReadForClass = 0 ;

  //    *(pbyDest + CurrByteWrite) = pbyDesC ;
  CurrbitRead = 0 ;
  CurrByteRead = 0 ;
  if ( speech_data->num_bits_b )
  {
    pbySrc = speech_data->data_b ;
    if( speech_data->num_bits_a == 0 )
    {
      pbyDest = AMRIF1Packet;
    }
    //uint8* pbyDest = (AMRIF1Packet+4)+CurrByteWrite ;
    while ( bitsReadForClass < speech_data->num_bits_b )
    {
      CurrByte = *(pbySrc + CurrByteRead) ;
      while ( (CurrbitRead<8)&&(bitsReadForClass < speech_data->num_bits_b) )
      {
        CurrBit = mvs_MaskArray[7-CurrbitRead] & CurrByte ;
        writeBitPos = CurrBitWrite - CurrbitRead ;
        if ( writeBitPos>0 )
        {
          CurrBit = CurrBit >> (CurrBitWrite-CurrbitRead) ;
        }
        else
        {
          CurrBit = CurrBit << (CurrbitRead - CurrBitWrite ) ;
        }

        //CurrBit = CurrBit << CurrBitWrite ;

        pbyDesC = pbyDesC | CurrBit ;
        CurrbitRead++ ;
        CurrBitWrite++ ;
        bitsReadForClass++ ;
        if ( CurrBitWrite == 8 )
        {
          *(pbyDest + CurrByteWrite) = pbyDesC ;
          CurrBitWrite = 0 ;
          CurrByteWrite++ ;
          pbyDesC  = 0x0 ;
        }
      }//process 8 bits

      if ( CurrbitRead == 8 )
      {
        CurrbitRead = 0 ;
        CurrByteRead++ ;
      }

      if ( CurrBitWrite == 8 )
      {
        *(pbyDest + CurrByteWrite) = pbyDesC ;
        CurrBitWrite = 0 ;
        CurrByteWrite++ ;
        pbyDesC  = 0x0 ;
      }
    }//for all B bits

  }//processed all B channel bits
  bitsReadForClass = 0 ;
  CurrbitRead = 0 ;
  CurrByteRead = 0 ;
  if ( speech_data->num_bits_c )
  {
    if( speech_data->num_bits_a == 0 && speech_data->num_bits_b == 0 )
    {
      pbyDest = AMRIF1Packet;
    }
    pbySrc = speech_data->data_c ;
    //uint8* pbyDest = (AMRIF1Packet+4)+CurrByteWrite ;
    while ( bitsReadForClass < speech_data->num_bits_c )
    {
      CurrByte = *(pbySrc + CurrByteRead) ;
      while ( (CurrbitRead<8)&&(bitsReadForClass < speech_data->num_bits_c) )
      {
        CurrBit = mvs_MaskArray[7-CurrbitRead] & CurrByte ;
        writeBitPos = CurrBitWrite - CurrbitRead ;
        if ( writeBitPos>0 )
        {
          CurrBit = CurrBit >> (CurrBitWrite - CurrbitRead) ;
        }
        else
        {
          CurrBit = CurrBit << (CurrbitRead - CurrBitWrite ) ;
        }

        //CurrBit = CurrBit << CurrBitWrite ;

        pbyDesC = pbyDesC | CurrBit ;
        CurrbitRead++ ;
        CurrBitWrite++ ;
        bitsReadForClass++ ;
        if ( CurrBitWrite == 8 )
        {
          *(pbyDest + CurrByteWrite) = pbyDesC ;
          CurrBitWrite = 0 ;
          CurrByteWrite++ ;
          pbyDesC  = 0x0 ;
        }
      }//process 8 bits

      if ( CurrbitRead == 8 )
      {
        CurrbitRead = 0 ;
        CurrByteRead++ ;
      }

      if ( CurrBitWrite == 8 )
      {
        *(pbyDest + CurrByteWrite) = pbyDesC ;
        CurrBitWrite = 0 ;
        CurrByteWrite++ ;
        pbyDesC  = 0x0 ;
      }
    }//for all c bits

  }//processed all c channel bits

  *(pbyDest + CurrByteWrite) = pbyDesC ;
  if ( nIF1PacketLengthFormed != NULL ) *nIF1PacketLengthFormed = CurrByteWrite + 1;

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_make_if1_from_channels(): " \
        "complete (%d)",
        nResult );

  return nResult ;
}

int32_t mvs_umts_dl_processing (
  uint8 *vocoder_packet,
  mvs_frame_info_type *frame_info
)
{
  int32_t nResult = APR_EOK;
  amrsup_core_speech_type amr_speech;
  mvs_amr_speech_buffer_type speech_data;
  mvs_amr_frame_type predicted_frame;
  boolean is_bad_frame;

  MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_umts_dl_processing()" );

  if ( mvs_cur_client != MVS_CLIENT_WCDMA &&
       mvs_cur_client != MVS_CLIENT_TDSCDMA )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_umts_dl_processing(): current " \
          "client is not a UMTS client" );
    return APR_EFAILED;
  }

  if ( vocoder_packet == NULL || frame_info == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_umts_dl_processing(): Invalid " \
          "parameters" );
    return APR_EBADPARAM;
  }

  /* Initialize vocoder packet to zero. */
  memset((void*)(vocoder_packet), 0, VOICE_PACKET_LENGTH);

  mvs_amr_init_core_speech(&amr_speech, &speech_data);

  /* Get per channel speech data from modem via the DSM queues. */
  mvs_dsm_amr_dl_proc(&amr_speech, &predicted_frame, &is_bad_frame);

  /* Determine frame type and mode. */
  frame_info->amr_rate.dl_info.frame = (mvs_amr_frame_type)(amrsup_get_frame_type(&amr_speech));
  frame_info->amr_rate.dl_info.mode = (mvs_amr_mode_type)(amrsup_get_frame_mode(&amr_speech,
                                                                                (amrsup_frame_type)(frame_info->amr_rate.dl_info.frame)));

  /* TODO: Need to make use of the is_bad_frame in mvs_make_if1_from_channels()
   *       to generate SPEECH_BAD or SID_BAD frames.
   */

  /* Create vocoder packet from data in individual channels. */
  nResult = mvs_make_if1_from_channels(vocoder_packet, &amr_speech, VOICE_PACKET_LENGTH, NULL);

  if ( is_bad_frame==TRUE )
  {
    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "Bad frame prediction: (%d) (%d) " \
          "(%d)",
          predicted_frame,
          frame_info->amr_rate.dl_info.frame,
          frame_info->amr_rate.dl_info.mode);
  }

  if ( predicted_frame != frame_info->amr_rate.dl_info.frame )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "Wrong prediction: (%d) (%d)",
            predicted_frame, frame_info->amr_rate.dl_info.frame);
  }

  MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_umts_dl_processing(): complete" );

  return nResult ;
}

int32_t mvs_umts_ul_processing (
  uint8 *vocoder_packet,
  mvs_frame_info_type *frame_info
)
{
  int32_t nResult = APR_EOK;
  amrsup_core_speech_type amr_speech;
  mvs_amr_speech_buffer_type speech_data;
  mvs_amr_chan_state_type chan_state;

  MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_umts_ul_processing()" );

  if ( mvs_cur_client != MVS_CLIENT_WCDMA &&
       mvs_cur_client != MVS_CLIENT_TDSCDMA )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_umts_ul_processing(): current " \
          "client is not a UMTS client" );
    return APR_EFAILED;
  }
  if ( vocoder_packet == NULL || frame_info == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_umts_ul_processing(): Invalid " \
          "parameters" );
    return APR_EBADPARAM;
  }

  mvs_amr_init_core_speech(&amr_speech, &speech_data);

  /* Determine number of bits in each class. */
  amr_speech.num_bits_a = mvs_amr_class_A_bits(frame_info->amr_rate.ul_info.frame, frame_info->amr_rate.ul_info.mode);
  amr_speech.num_bits_b = mvs_amr_class_B_bits(frame_info->amr_rate.ul_info.frame, frame_info->amr_rate.ul_info.mode);
  amr_speech.num_bits_c = mvs_amr_class_C_bits(frame_info->amr_rate.ul_info.frame, frame_info->amr_rate.ul_info.mode);

  chan_state.has_chan_a = (amr_speech.num_bits_a > 0) ? TRUE : FALSE;
  chan_state.has_chan_b = (amr_speech.num_bits_b > 0) ? TRUE : FALSE;
  chan_state.has_chan_c = (amr_speech.num_bits_c > 0) ? TRUE : FALSE;

  if ( mvs_amr_is_subset_chan_state(&chan_state, &(mvs_ul_umts_chan_state.chan_state)) != TRUE )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "ERROR: inconsistent channel " \
            "setup for AMR mode (%d)",
            mvs_client_table[mvs_cur_client].config_mode );
    return APR_EFAILED;
  }

  /* Extract individual channel data from vocoder packet in order to put in separate DSM queues. */
  nResult = mvs_extract_channels_from_if1(vocoder_packet, &amr_speech);
  if ( nResult != APR_EOK )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_umts_ul_processing(): " \
            "FAILED to extract individual channel data from vocoder packet " \
            "(%d)",
            nResult );
    return APR_EBADPARAM;
  }

  /* Transport individual AMR channels to modem via DSM queues. */
  mvs_dsm_amr_ul_proc(&amr_speech, chan_state);

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_umts_ul_processing(): complete " \
        "(%d)",
        nResult );

  return nResult;
}

#endif /* !WINSIM */

static int32_t mvs_change_state_reset (
  mvs_client_type client
)
{
  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "mvs_change_state_reset(): " \
         "client=(%d)",
         client );

  switch ( mvs_client_table[client].pending_cmd.cur_cmd )
  {
  case MVS_CMD_SSR:
    break;
    
  case MVS_CMD_ACQUIRE:
    mvs_client_table[client].state = MVS_STATE_IDLE;
    break;

  default:
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "Invalid command (%d)",
            mvs_client_table[client].pending_cmd.cur_cmd );
    break;
  }

  return APR_EOK;
}

static int32_t mvs_change_state_idle (
  mvs_client_type client
)
{
  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "mvs_change_state_idle(): " \
         "client=(%d), cur_cmd=(%d)",
         client, mvs_client_table[client].pending_cmd.cur_cmd );

  switch ( mvs_client_table[client].pending_cmd.cur_cmd )
  {
  case MVS_CMD_RELEASE:
    mvs_client_table[client].state = MVS_STATE_RESET;
    break;

  case MVS_CMD_ENABLE:
    if ( mvs_client_table[client].config_mode != MVS_MODE_NONE )
    {
      mvs_client_table[client].state = MVS_STATE_VOCODER;
    }
    break;

  case MVS_CMD_STANDBY:
      mvs_client_table[client].state = MVS_STATE_IDLE;
    break;

  case MVS_CMD_SSR:
    break;

  default:
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "Invalid command (%d)",
            mvs_client_table[client].pending_cmd.cur_cmd );
    break;
  }

  return APR_EOK;
}

static int32_t mvs_change_state_vocoder (
  mvs_client_type client
)
{
  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "mvs_change_state_vocoder(): " \
         "client=(%d)",
         client );

  switch ( mvs_client_table[client].pending_cmd.cur_cmd )
  {
  case MVS_CMD_ACQUIRE:
  case MVS_CMD_SSR:
    mvs_client_table[client].state = MVS_STATE_IDLE;
    break;

  case MVS_CMD_STANDBY:
      mvs_client_table[client].state = MVS_STATE_IDLE;
    break;

  case MVS_CMD_ENABLE:
    if ( mvs_client_table[client].config_mode == MVS_MODE_NONE )
    {
      mvs_client_table[client].state = MVS_STATE_IDLE;
    }
    break;

  case MVS_CMD_RELEASE:
    mvs_client_table[client].state = MVS_STATE_RESET;
    break;

  default:
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "Invalid command (%d)",
            mvs_client_table[client].pending_cmd.cur_cmd );
    break;
  }

  return APR_EOK;
}

/* NOTE: This routine updates the MVS state machine transition to the next
 *       state once an MVS control command (mvs_enable(), mvs_standby(),
 *       mvs_release()) has completed.
 */
static int32_t mvs_update_state (
  mvs_client_type client
)
{
  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_update_state(): client=(%d)",
        client );

  if ( client >= MVS_CLIENT_MAX )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_update_state(): invalid " \
            "client (%d)",
            client );
    return APR_EBADPARAM;
  }

  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "mvs_update_state(): client=(%d), " \
         "cur_state=(%d)",
         client, mvs_client_table[client].state );

  /* TODO: Need to clean this up; it's not obvious what this logic really
   *       means by looking at the variable names. Basically the cmd_status
   *       set to TRUE means there's an MVS control command that's currently
   *       being processed (e.g. mvs_acquire(), mvs_standby(), mvs_enable(),
   *       mvs_release()). The cur_cmd is the command that's running. The
   *       cmd_status is redundant when cur_cmd is already sufficient. The
   *       cur_cmd is reset to MVS_CMD_INVALID when cmd_status if FALSE. Why
   *       would this be necessary?
   */
  if ( mvs_client_table[client].pending_cmd.is_done )
  {
    mvs_client_table[client].pending_cmd.cur_cmd = MVS_CMD_INVALID;
    return APR_EOK;
  }

  switch ( mvs_client_table[client].state )
  {
  case MVS_STATE_RESET:
    ( void ) mvs_change_state_reset( client );
    break;

  case  MVS_STATE_IDLE:
    ( void ) mvs_change_state_idle( client );
    break;

  case MVS_STATE_VOCODER:
    ( void ) mvs_change_state_vocoder( client );
    break;

  default:
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_update_state(): bad state " \
            "(%d)",
            mvs_client_table[client].state );
    break;
  }

  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "mvs_update_state(): client=(%d), " \
         "new_state=(%d)",
         client, mvs_client_table[client].state );

  /* TODO: Need to clean this up; consuming the currently pending mvs
   *       command shouldn't be done by this function because it's very tough
   *       to find where commands are consumed outside of the top-level
   *       processing routine.
   */
  mvs_client_table[client].pending_cmd.cur_cmd = MVS_CMD_INVALID;
  mvs_client_table[client].pending_cmd.is_done = TRUE;

  return APR_EOK;
}

static void mvs_mvm_poll_timer_cb ( void* client_token )
{
  int32_t rc;
  mvs_simple_job_object_t *job_obj;

  if ( mvs_is_allocated_objs_freed_on_ssr == FALSE )
  { 
    /* Avoids creating a job object for polling MVM until all previously  
     * allocated objects are freed upon ADSP SSR. 
     */
    apr_timer_start( mvs_mvm_poll_timer, MVS_MVM_POLL_TIMER_NS );
    return;
  }

  if ( mvs_mvm_latest_poll != APR_NULL_V )
  {
    mvs_free_object ( ( mvs_object_t* ) mvs_mvm_latest_poll );
  }
  if ( mvs_is_mvm_up )
  {
    rc = __aprv2_cmd_alloc_send(
                    mvs_my_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                    mvs_my_addr, MVS_PORT,
                    mvs_my_addr, MVS_PORT,
                    MVS_CMD_SSR, 
                    VSS_IMVS_CMD_INTERNAL, NULL, 0 );
    return;
  }

  rc = mvs_create_simple_job_object( APR_NULL_V, &job_obj );
  job_obj->fn_table[ MVS_RESPONSE_FN_ENUM_RESULT ] = mvs_mvm_poll_result_rsp_fn;
  MVS_PANIC_ON_ERROR( rc );

  mvs_mvm_latest_poll = job_obj;

  rc = __aprv2_cmd_alloc_send(
                 mvs_my_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                 mvs_my_addr, MVS_PORT,
                 mvs_mvm_addr, mvs_mvm_port,
                 job_obj->header.handle,
                 APR_CMDID_GET_VERSION, NULL, 0 );

  /* Restart the timer. */
  apr_timer_start( mvs_mvm_poll_timer, MVS_MVM_POLL_TIMER_NS );

}

void mvs_ssr_adsp_after_powerup ( void )
{
  MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "mvs_ssr_adsp_after_powerup()" );
  /* Start poll timer */
  apr_timer_start( mvs_mvm_poll_timer, MVS_MVM_POLL_TIMER_NS );
  mvs_mvm_latest_poll = APR_NULL_V; 
}

void mvs_ssr_adsp_before_shutdown ( void )
{
  MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "mvs_ssr_adsp_before_shutdown()" );
  mvs_is_mvm_up = FALSE;
  mvs_adsp_crash = TRUE;
  mvs_is_allocated_objs_freed_on_ssr = FALSE;
  mvs_mvm_port = APR_NULL_V;
  mvs_cvs_port = APR_NULL_V;
  mvs_mvm_latest_poll = APR_NULL_V;

  /* Signal MVS to run */ 
  mvs_signal_run( );

}


void mvs_state_control_init ( void )
{
  int i = 0;

  for ( i=0; i< MVS_CLIENT_MAX; i++ )
  {
    mvs_client_table[i].state = MVS_STATE_RESET;
    mvs_client_table[i].config_mode = MVS_MODE_NONE;
    mvs_client_table[i].dl_cb = NULL;
    mvs_client_table[i].ul_cb = NULL;
    mvs_client_table[i].event_cb = NULL;
    mvs_client_table[i].pending_cmd.cur_cmd = MVS_CMD_INVALID;
    mvs_client_table[i].pending_cmd.is_done = TRUE;
    mvs_client_table[i].pending_cmd.job_token = 0xf0f0;
  }

  mvs_curr_props.awb_ul_mode = MVS_AMR_MODE_MAX;
  mvs_curr_props.amr_ul_mode = MVS_AMR_MODE_MAX;
  mvs_curr_props.dtx_mode = MVS_DTX_OFF;
  mvs_curr_props.max_voc_rate = MVS_VOC_0_RATE;
  mvs_curr_props.min_voc_rate = MVS_VOC_0_RATE;
  mvs_curr_props.rate_factor = 0;
  mvs_new_props.awb_ul_mode = MVS_AMR_MODE_MAX;
  mvs_new_props.amr_ul_mode = MVS_AMR_MODE_MAX;
  mvs_new_props.dtx_mode = MVS_DTX_OFF;
  mvs_new_props.max_voc_rate = MVS_VOC_0_RATE;
  mvs_new_props.min_voc_rate = MVS_VOC_0_RATE;
  mvs_new_props.rate_factor = 0;
}

int32_t mvs_mod_acquire (
  mvs_client_type client,
  mvs_event_cb_type cb_func
)
{
  int32_t rc;
  mvs_packet_type mvs_cmd;
  aprv2_packet_t* packet = NULL;
  bool_t is_ok = FALSE;

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_mod_acquire(): client = (%d)",
        client );

  if ( client >= MVS_CLIENT_MAX )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "Invalid client = (%d)",
            client );
    return APR_EBADPARAM;
  }

  if ( mvs_is_initialized )
  {
    mvs_cmd.acquire.hdr.cmd = MVS_CMD_ACQUIRE;
    mvs_cmd.acquire.client = client;
    mvs_cmd.acquire.cb_func = cb_func;

    rc = mvs_make_cmd_packet( &mvs_cmd, &packet );
    if ( rc )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "make pkt failed!!" );
      is_ok = FALSE;
    }
    else 
    {
      is_ok = mvs_queue_incoming_packet( packet );
    }
  }

  if ( ( is_ok == FALSE ) && ( cb_func != NULL ) )
  {
    mvs_event_type event_data;

    event_data.cmd.hdr.event = MVS_EV_COMMAND;
    event_data.cmd.hdr.client = client;
    event_data.cmd.cmd_status = MVS_CMD_FAILURE;
    cb_func( &event_data );
  }

  return APR_EOK;
}

static int32_t mvs_mod_acquire_proc (
  mvs_pending_control_t *ctrl
)
{
  int32_t rc;
  mvs_packet_type* acquire_cmd;
  mvs_client_type client;
  mvs_event_cb_type cb_func;

  acquire_cmd = APRV2_PKT_GET_PAYLOAD( mvs_packet_type, ctrl->packet );

  if ( ctrl->state == MVS_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_mod_acquire_proc(): client " \
          "(%d)",
          acquire_cmd->acquire.client );

    client =  acquire_cmd->acquire.client;
    cb_func = acquire_cmd->acquire.cb_func;

    if ( client >= MVS_CLIENT_MAX )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "Invalid client = (%d)",
              client );
      return APR_EBADPARAM;
    }

    if ( cb_func == NULL )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_mod_acquire_proc(): cb_func " \
            "is NULL" );
      return APR_EBADPARAM;
    }

    mvs_client_table[client].pending_cmd.cur_cmd = MVS_CMD_ACQUIRE;
    mvs_client_table[client].pending_cmd.is_done = FALSE;
    mvs_client_table[client].event_cb = cb_func;

    switch ( mvs_client_table[client].state )
    {
    case MVS_STATE_RESET:
      break;

    case MVS_STATE_IDLE:
    case MVS_STATE_VOCODER:
    default:
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_mod_acquire_proc(): " \
              "already acquired. Tell client CMD_SUCCESS. (%d)",
              mvs_client_table[client].state );

        mvs_client_table[client].pending_cmd.is_done = TRUE;
        rc = __aprv2_cmd_free( mvs_my_apr_handle, ctrl->packet );
        MVS_PANIC_ON_ERROR(rc);
        mvs_send_cmd_event( client, MVS_CMD_SUCCESS );
      }
      return APR_EOK;
    }

    /* MVS & CVS sessions are created at init time. Therefore in mvs_acquire()
     * we only update state. */
    rc = __aprv2_cmd_free( mvs_my_apr_handle, ctrl->packet );
    MVS_PANIC_ON_ERROR(rc);
    mvs_send_cmd_event( client, MVS_CMD_SUCCESS );
    mvs_update_state( client );

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,
           "mvs_mod_acquire_proc(): complete client (%d)", client );


    return rc;
  }
  else
  {
    MVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
  }

  return rc;
}

enum {
  MVS_ENABLE_SEQUENCER_ENUM_UNINITIALIZED, 
  MVS_ENABLE_SEQUENCER_ENUM_STOP_VOICE, /* Entry. */
  MVS_ENABLE_SEQUENCER_ENUM_STOP_VOICE_WAIT, /* Exit. */
  MVS_ENABLE_SEQUENCER_ENUM_CREATE_MVM, /* Entry. */
  MVS_ENABLE_SEQUENCER_ENUM_CREATE_MVM_WAIT,
  MVS_ENABLE_SEQUENCER_ENUM_CREATE_CVS,
  MVS_ENABLE_SEQUENCER_ENUM_CREATE_CVS_WAIT,
  MVS_ENABLE_SEQUENCER_ENUM_ATTACH_STREAM,
  MVS_ENABLE_SEQUENCER_ENUM_ATTACH_STREAM_WAIT,
  MVS_ENABLE_SEQUENCER_ENUM_LISTEN_EAMR,
  MVS_ENABLE_SEQUENCER_ENUM_STANDBY_VOICE, /* Entry. */
  MVS_ENABLE_SEQUENCER_ENUM_STANDBY_VOICE_WAIT,
  MVS_ENABLE_SEQUENCER_ENUM_SET_MEDIA_TYPE,
  MVS_ENABLE_SEQUENCER_ENUM_SET_MEDIA_TYPE_WAIT,
  MVS_ENABLE_SEQUENCER_ENUM_RECONFIG,
  MVS_ENABLE_SEQUENCER_ENUM_SET_CACHED_VALUES,
  MVS_ENABLE_SEQUENCER_ENUM_RESYNC_CTM,
  MVS_ENABLE_SEQUENCER_ENUM_START_VOICE,
  MVS_ENABLE_SEQUENCER_ENUM_START_VOICE_WAIT, /* Exit. */
  MVS_ENABLE_SEQUENCER_ENUM_COMPLETE,
  MVS_ENABLE_SEQUENCER_ENUM_INVALID
};

static int32_t mvs_mod_enable_sequencer (
  mvs_pending_control_t* ctrl,
  bool_t is_standby
)
{
  int32_t rc;
  uint32_t _rc;
  mvs_packet_type* cmd;  
  mvs_client_type client;
  mvs_mode_type mode;
  mvs_sequencer_job_object_t* seqjob_obj;
  mvs_simple_job_object_t* subjob_obj;
  mvs_token_object_t* token_obj;
  vss_istream_cmd_create_full_control_session_t* full_control;
  char_t* dst_stream_name;
  aprv2_packet_t* new_packet;
  vss_istream_cmd_set_media_type_t set_media_type;

  cmd = APRV2_PKT_GET_PAYLOAD( mvs_packet_type, ctrl->packet );
  if ( is_standby )
  {
    client = cmd->standby.client;
    mode = cmd->standby.mode;
  }
  else
  {
    client = cmd->enable.client;
    mode = cmd->enable.mode;
  }

  seqjob_obj = &ctrl->rootjob_obj->sequencer_job;

  for ( ;; )
  {
    switch ( seqjob_obj->state )
    {
    case MVS_ENABLE_SEQUENCER_ENUM_STOP_VOICE:
      {
        if ( ( mvs_is_silence_frame_supported_for_mvs_cur_client( ) == TRUE ) && 
             ( mvs_is_registered_with_voiceapp == TRUE ) )
        {
          rc = mvs_deregister_modem_timing( );
          if ( rc == APR_EOK )
          {
            mvs_is_registered_with_voiceapp = FALSE;
          }
        }
       
        /* STOP_VOICE on MVM. */
        rc = mvs_create_simple_job_object( APR_NULL_V, &subjob_obj );
        MVS_PANIC_ON_ERROR( rc );
        seqjob_obj->subjob_obj = ( ( mvs_object_t* ) subjob_obj );

        rc = __aprv2_cmd_alloc_send(
               mvs_my_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               mvs_my_addr, MVS_PORT,
               mvs_mvm_addr, mvs_mvm_port,
               subjob_obj->header.handle,
               VSS_IMVM_CMD_MODEM_STOP_VOICE, NULL, 0 );
        // MVS_PANIC_ON_ERROR( rc );
        if ( rc )
        {
          /* Temporary workaround to handle SSR phase 3. Any APR IPC communication 
           * failure will cause the current command to fail instead of panicing on 
           * failure.
           */
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                  "mvs_mod_enable_sequencer(): Failed to communicate with " \
                  "MVM, rc = (0x%08X)",
                  rc );

          seqjob_obj->status = rc;
          seqjob_obj->state = MVS_ENABLE_SEQUENCER_ENUM_COMPLETE;

          rc = mvs_free_object( ( mvs_object_t* ) subjob_obj );
          MVS_PANIC_ON_ERROR( rc );
        }
        else
        {
          seqjob_obj->state = MVS_ENABLE_SEQUENCER_ENUM_STOP_VOICE_WAIT;
        }
      }
      continue;

    case MVS_ENABLE_SEQUENCER_ENUM_STOP_VOICE_WAIT:
      {
        /* Wait for MVM to STOP_VOICE. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        seqjob_obj->status = subjob_obj->status;
        seqjob_obj->state = MVS_ENABLE_SEQUENCER_ENUM_COMPLETE;

        rc = mvs_free_object( ( mvs_object_t* ) subjob_obj );
        MVS_PANIC_ON_ERROR( rc );
      }
      continue;

    case MVS_ENABLE_SEQUENCER_ENUM_CREATE_MVM:
      {
        /* Create the MVM session. */
        rc = mvs_create_simple_job_object( APR_NULL_V, &subjob_obj );
        MVS_PANIC_ON_ERROR( rc );
        subjob_obj->fn_table[ MVS_RESPONSE_FN_ENUM_RESULT ] = mvs_create_mvm_session_rsp_result_fn;
        seqjob_obj->subjob_obj = ( ( mvs_object_t* ) subjob_obj );

        rc = __aprv2_cmd_alloc_send(
               mvs_my_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               mvs_my_addr, MVS_PORT,
               mvs_mvm_addr, APR_NULL_V,
               subjob_obj->header.handle, VSS_IMVM_CMD_CREATE_FULL_CONTROL_SESSION,
               mvs_default_stream_name, sizeof( mvs_default_stream_name ) );
        // MVS_PANIC_ON_ERROR( rc );
        if ( rc )
        {
          /* Temporary workaround to handle SSR phase 3. Any APR IPC communication 
           * failure will cause the current command to fail instead of panicing on 
           * failure.
           */
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                  "mvs_mod_enable_sequencer(): Failed to communicate with " \
                  "MVM, rc = (0x%08X)",
                  rc );

          seqjob_obj->status = rc;
          seqjob_obj->state = MVS_ENABLE_SEQUENCER_ENUM_COMPLETE;

          rc = mvs_free_object( ( mvs_object_t* ) subjob_obj );
          MVS_PANIC_ON_ERROR( rc );
        }
        else
        {
          seqjob_obj->state = MVS_ENABLE_SEQUENCER_ENUM_CREATE_MVM_WAIT;
        }
      }
      continue;

    case MVS_ENABLE_SEQUENCER_ENUM_CREATE_MVM_WAIT:
      {
        /* Wait for the MVM session to be created. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        /* Tell MVM that we want to enable the MODEM state machine control. */
        {
          vss_imvm_cmd_set_modem_voice_control_t ctrl_args;
          mvs_token_object_t* token_obj;

          rc = mvs_create_autofree_token_object( &token_obj );
          MVS_PANIC_ON_ERROR( rc );
           /* Waiting for a response is not needed as commands sent later in
            * the sequencer will guarantee that this command completes.
            */

          ctrl_args.enable_flag = TRUE;

          rc = __aprv2_cmd_alloc_send(
                 mvs_my_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                 mvs_my_addr, MVS_PORT,
                 mvs_mvm_addr, mvs_mvm_port,
                 token_obj->header.handle, VSS_IMVM_CMD_SET_MODEM_VOICE_CONTROL,
                 &ctrl_args, sizeof( ctrl_args ) );
          // MVS_PANIC_ON_ERROR( rc );
          if ( rc )
          {
            /* Temporary workaround to handle SSR phase 3. Any APR IPC communication 
             * failure will cause the current command to fail instead of panicing on 
             * failure.
             */
            MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                    "mvs_mod_enable_sequencer(): Failed to communicate with " \
                    "MVM, rc = (0x%08X)",
                    rc );

            seqjob_obj->status = rc;
            seqjob_obj->state = MVS_ENABLE_SEQUENCER_ENUM_COMPLETE;

            rc = mvs_free_object( ( mvs_object_t* ) token_obj );
            MVS_PANIC_ON_ERROR( rc );

            rc = mvs_free_object( ( mvs_object_t* ) subjob_obj );
            MVS_PANIC_ON_ERROR( rc );

            continue;
          }
        }

        /* Move to the next sequencer state. */
        seqjob_obj->status = subjob_obj->status;
        seqjob_obj->state = MVS_ENABLE_SEQUENCER_ENUM_CREATE_CVS;

        rc = mvs_free_object( ( mvs_object_t* ) subjob_obj );
        MVS_PANIC_ON_ERROR( rc );
      }
      continue;

    case MVS_ENABLE_SEQUENCER_ENUM_CREATE_CVS:
      {
        /* Create the CVS session. */
        rc = mvs_create_simple_job_object( APR_NULL_V, &subjob_obj );
        MVS_PANIC_ON_ERROR( rc );
        subjob_obj->fn_table[ MVS_RESPONSE_FN_ENUM_RESULT ] = mvs_create_cvs_session_rsp_result_fn;
        seqjob_obj->subjob_obj = ( ( mvs_object_t* ) subjob_obj );

        rc = __aprv2_cmd_alloc_ext(
               mvs_my_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               mvs_my_addr, MVS_PORT,
               mvs_cvs_addr, APR_NULL_V,
               subjob_obj->header.handle,
               VSS_ISTREAM_CMD_CREATE_FULL_CONTROL_SESSION,
               ( sizeof( vss_istream_cmd_create_full_control_session_t ) +
               sizeof( mvs_default_stream_name ) ),
               &new_packet );
        MVS_PANIC_ON_ERROR( rc );

        full_control = APRV2_PKT_GET_PAYLOAD( vss_istream_cmd_create_full_control_session_t,
                                              new_packet );
        full_control->direction = 2;
        full_control->enc_media_type = VSS_MEDIA_ID_NONE;
        full_control->dec_media_type = VSS_MEDIA_ID_NONE;
        full_control->network_id = VSS_NETWORK_ID_DEFAULT;

        dst_stream_name = ( ( char_t* )
                            APR_PTR_END_OF(
                            full_control,
                            sizeof( vss_istream_cmd_create_full_control_session_t ) ) );

        mmstd_memcpy( dst_stream_name, sizeof( mvs_default_stream_name ), 
                      mvs_default_stream_name, sizeof( mvs_default_stream_name ) );

        rc = __aprv2_cmd_forward( mvs_my_apr_handle, new_packet );
        // MVS_PANIC_ON_ERROR( rc );
        if ( rc )
        {
          /* Temporary workaround to handle SSR phase 3. Any APR IPC communication 
           * failure will cause the current command to fail instead of panicing on 
           * failure.
           */
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                  "mvs_mod_enable_sequencer(): Failed to communicate with " \
                  "CVS, rc = (0x%08X)",
                  rc );

          seqjob_obj->status = rc;
          seqjob_obj->state = MVS_ENABLE_SEQUENCER_ENUM_COMPLETE;

          rc = mvs_free_object( ( mvs_object_t* ) subjob_obj );
          MVS_PANIC_ON_ERROR( rc );
        }
        else
        {
          seqjob_obj->state = MVS_ENABLE_SEQUENCER_ENUM_CREATE_CVS_WAIT;
        }
      }
      continue;

    case MVS_ENABLE_SEQUENCER_ENUM_CREATE_CVS_WAIT:
      {
        /* Wait for the CVS session to be created. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        seqjob_obj->status = subjob_obj->status;
        seqjob_obj->state = MVS_ENABLE_SEQUENCER_ENUM_ATTACH_STREAM;

        rc = mvs_free_object( ( mvs_object_t* ) subjob_obj );
        MVS_PANIC_ON_ERROR( rc );
      }
      continue;

    case MVS_ENABLE_SEQUENCER_ENUM_ATTACH_STREAM:
      {
        /* Attach the CVS session to the MVM. */
        rc = mvs_create_simple_job_object( APR_NULL_V, &subjob_obj );
        MVS_PANIC_ON_ERROR( rc );
        seqjob_obj->subjob_obj = ( ( mvs_object_t* ) subjob_obj );

        rc = __aprv2_cmd_alloc_send(
               mvs_my_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               mvs_my_addr, MVS_PORT,
               mvs_mvm_addr, mvs_mvm_port,
               subjob_obj->header.handle, VSS_IMVM_CMD_ATTACH_STREAM,
               &mvs_cvs_port, sizeof( mvs_cvs_port ) );
        // MVS_PANIC_ON_ERROR( rc );
        if ( rc )
        {
          /* Temporary workaround to handle SSR phase 3. Any APR IPC communication 
           * failure will cause the current command to fail instead of panicing on 
           * failure.
           */
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                  "mvs_mod_enable_sequencer(): Failed to communicate with " \
                  "MVM, rc = (0x%08X)",
                  rc );

          seqjob_obj->status = rc;
          seqjob_obj->state = MVS_ENABLE_SEQUENCER_ENUM_COMPLETE;

          rc = mvs_free_object( ( mvs_object_t* ) subjob_obj );
          MVS_PANIC_ON_ERROR( rc );
        }
        else
        {
          seqjob_obj->state = MVS_ENABLE_SEQUENCER_ENUM_ATTACH_STREAM_WAIT;
        }
      }
      continue;

    case MVS_ENABLE_SEQUENCER_ENUM_ATTACH_STREAM_WAIT:
      {
        /* Wait for the MVM session to be created. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        seqjob_obj->status = subjob_obj->status;
        seqjob_obj->state = MVS_ENABLE_SEQUENCER_ENUM_LISTEN_EAMR;
       
        rc = mvs_free_object( ( mvs_object_t* ) subjob_obj );
        MVS_PANIC_ON_ERROR( rc );
      }
      continue;

    case MVS_ENABLE_SEQUENCER_ENUM_LISTEN_EAMR:
      {
        vss_inotify_cmd_listen_for_event_class_t ctrl_args;
        voicecfg_cmd_get_bool_item_t item;

        item.id = VOICECFG_CFG_IS_EAMR_REPORT_MODE_CHANGE_ENABLED;
        item.ret_value = FALSE;
        _rc = voicecfg_call( VOICECFG_CMD_GET_BOOL_ITEM, &item, sizeof ( item ) );

        if ( !_rc && ( item.ret_value == TRUE ) )
        {
          rc = mvs_create_autofree_token_object( &token_obj );
          MVS_PANIC_ON_ERROR( rc );

          ctrl_args.class_id = VSS_ISTREAM_EVENT_CLASS_EAMR_MODE_CHANGE;
          rc = __aprv2_cmd_alloc_send(
                 mvs_my_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                 mvs_my_addr, MVS_PORT,
                 mvs_cvs_addr, mvs_cvs_port,
                 token_obj->header.handle, VSS_INOTIFY_CMD_LISTEN_FOR_EVENT_CLASS,
                 &ctrl_args, sizeof( ctrl_args ) );
          // MVS_PANIC_ON_ERROR( rc );
          if ( rc )
          {
            /* Temporary workaround to handle SSR phase 3. Any APR IPC communication 
             * failure will cause the current command to fail instead of panicing on 
             * failure.
             */
            MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                    "mvs_mod_enable_sequencer(): Failed to communicate with " \
                    "CVS, rc = (0x%08X)",
                    rc );

            seqjob_obj->status = rc;
            seqjob_obj->state = MVS_ENABLE_SEQUENCER_ENUM_COMPLETE;

            rc = mvs_free_object( ( mvs_object_t* ) token_obj );
            MVS_PANIC_ON_ERROR( rc );

            continue;
          }
          mvs_is_listen_for_emar_mode_change = TRUE;
        }
        seqjob_obj->state = MVS_ENABLE_SEQUENCER_ENUM_STANDBY_VOICE;
     }
     continue;

    case MVS_ENABLE_SEQUENCER_ENUM_STANDBY_VOICE:
      {

        if ( ( mvs_is_silence_frame_supported_for_mvs_cur_client( ) == TRUE ) && 
            ( mvs_is_registered_with_voiceapp == TRUE ) )
        {
          rc = mvs_deregister_modem_timing( );
          if ( rc == APR_EOK )
          {
            mvs_is_registered_with_voiceapp = FALSE;
          }
        }

        /* STANDBY_VOICE on MVM. */
        rc = mvs_create_simple_job_object( APR_NULL_V, &subjob_obj );
        MVS_PANIC_ON_ERROR( rc );
        seqjob_obj->subjob_obj = ( ( mvs_object_t* ) subjob_obj );

        rc = __aprv2_cmd_alloc_send(
               mvs_my_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               mvs_my_addr, MVS_PORT,
               mvs_mvm_addr, mvs_mvm_port,
               subjob_obj->header.handle,
               VSS_IMVM_CMD_MODEM_STANDBY_VOICE, NULL, 0 );
        // MVS_PANIC_ON_ERROR( rc );
        if ( rc )
        {
          /* Temporary workaround to handle SSR phase 3. Any APR IPC communication 
           * failure will cause the current command to fail instead of panicing on 
           * failure.
           */
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                  "mvs_mod_enable_sequencer(): Failed to communicate with " \
                  "MVM, rc = (0x%08X)",
                  rc );

          seqjob_obj->status = rc;
          seqjob_obj->state = MVS_ENABLE_SEQUENCER_ENUM_COMPLETE;

          rc = mvs_free_object( ( mvs_object_t* ) subjob_obj );
          MVS_PANIC_ON_ERROR( rc );
        }
        else
        {
          seqjob_obj->state = MVS_ENABLE_SEQUENCER_ENUM_STANDBY_VOICE_WAIT;
        }
      }
      continue;

    case MVS_ENABLE_SEQUENCER_ENUM_STANDBY_VOICE_WAIT:
      {
        /* Wait for MVM to STOP_VOICE. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        seqjob_obj->status = subjob_obj->status;
        seqjob_obj->state = MVS_ENABLE_SEQUENCER_ENUM_SET_MEDIA_TYPE;

        rc = mvs_free_object( ( mvs_object_t* ) subjob_obj );
        MVS_PANIC_ON_ERROR( rc );
      }
      continue;

    case MVS_ENABLE_SEQUENCER_ENUM_SET_MEDIA_TYPE:
      {
        /* Set media type on CVS to indicate the vocoders used. */
        rc = mvs_create_simple_job_object( APR_NULL_V, &subjob_obj );
        MVS_PANIC_ON_ERROR( rc );
        seqjob_obj->subjob_obj = ( ( mvs_object_t* ) subjob_obj );

        set_media_type.rx_media_id =
          map_to_cvd_mode( mvs_client_table[ mvs_cur_client ].config_mode );
        set_media_type.tx_media_id = 
          map_to_cvd_mode( mvs_client_table[ mvs_cur_client ].config_mode );
     
        rc = __aprv2_cmd_alloc_send(
               mvs_my_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               mvs_my_addr, MVS_PORT,
               mvs_cvs_addr, mvs_cvs_port,
               subjob_obj->header.handle, VSS_ISTREAM_CMD_SET_MEDIA_TYPE,
               &set_media_type, sizeof( set_media_type) );
        // MVS_PANIC_ON_ERROR( rc );
        if ( rc )
        {
          /* Temporary workaround to handle SSR phase 3. Any APR IPC communication 
           * failure will cause the current command to fail instead of panicing on 
           * failure.
           */
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                  "mvs_mod_enable_sequencer(): Failed to communicate with " \
                  "CVS, rc = (0x%08X)",
                  rc );

          seqjob_obj->status = rc;
          seqjob_obj->state = MVS_ENABLE_SEQUENCER_ENUM_COMPLETE;

          rc = mvs_free_object( ( mvs_object_t* ) subjob_obj );
          MVS_PANIC_ON_ERROR( rc );
        }
        else
        {
          seqjob_obj->state = MVS_ENABLE_SEQUENCER_ENUM_SET_MEDIA_TYPE_WAIT;        
        }
      }
      continue;

    case MVS_ENABLE_SEQUENCER_ENUM_SET_MEDIA_TYPE_WAIT:
      {
        /* Wait for CVS to SET_MEDIA_TYPE. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
        return APR_EPENDING;
        }

        seqjob_obj->status = subjob_obj->status;
        seqjob_obj->state = MVS_ENABLE_SEQUENCER_ENUM_RECONFIG;

        rc = mvs_free_object( ( mvs_object_t* ) subjob_obj );
        MVS_PANIC_ON_ERROR( rc );
      }
      continue;
      
    case MVS_ENABLE_SEQUENCER_ENUM_RECONFIG:
      {
        /* Set network on MVM to be used for calibrating the attached
         * stream/vocproc. 
         */
        ( void ) mvs_mod_set_cal_network_proc();

        /* Set media type on MVM to be used for calibrating the attached
         * stream/vocproc.
         */
        ( void ) mvs_mod_set_cal_media_type_proc();

        /* Set frame sync on MVM. */
        ( void ) mvs_mod_set_voice_timing_proc();

        seqjob_obj->state = MVS_ENABLE_SEQUENCER_ENUM_SET_CACHED_VALUES;
      }
      continue;

    case MVS_ENABLE_SEQUENCER_ENUM_SET_CACHED_VALUES:
      {
        /* Send data commands to stream. */
        rc = mvs_set_cached_values();

        if ( is_standby )
        {
          seqjob_obj->state = MVS_ENABLE_SEQUENCER_ENUM_COMPLETE;
        }
        else
        { 
          seqjob_obj->state = MVS_ENABLE_SEQUENCER_ENUM_RESYNC_CTM;
        }
      }
      continue;

    case MVS_ENABLE_SEQUENCER_ENUM_RESYNC_CTM:
      {

          /* Send RESYNC_CTM to CVS. */
          rc = mvs_create_autofree_token_object( &token_obj );
          MVS_PANIC_ON_ERROR( rc );

          rc = __aprv2_cmd_alloc_send(
                 mvs_my_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                 mvs_my_addr, MVS_PORT,
                 mvs_cvs_addr, mvs_cvs_port,
                 token_obj->header.handle,
                 VSS_ISTREAM_CMD_RESYNC_CTM, NULL, 0 );
          // MVS_PANIC_ON_ERROR( rc );
          if ( rc )
          {
            /* Temporary workaround to handle SSR phase 3. Any APR IPC communication 
             * failure will cause the current command to fail instead of panicing on 
             * failure.
             */
            MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                    "mvs_mod_enable_sequencer(): Failed to communicate with " \
                    "CVS, rc = (0x%08X)",
                    rc );

            seqjob_obj->status = rc;
            seqjob_obj->state = MVS_ENABLE_SEQUENCER_ENUM_COMPLETE;

            rc = mvs_free_object( ( mvs_object_t* ) token_obj );
            MVS_PANIC_ON_ERROR( rc );

            continue;
          }

        seqjob_obj->state = MVS_ENABLE_SEQUENCER_ENUM_START_VOICE;
      }
      continue;

    case MVS_ENABLE_SEQUENCER_ENUM_START_VOICE:
      {
        if ( mvs_is_silence_frame_supported_for_mvs_cur_client( ) )
        {
          rc = mvs_register_modem_timing( );
          if ( rc == APR_EOK )
          {
            mvs_is_registered_with_voiceapp = TRUE;
          }
        }

        /* START_VOICE on MVM. */
        rc = mvs_create_simple_job_object( APR_NULL_V, &subjob_obj );
        MVS_PANIC_ON_ERROR( rc );
        seqjob_obj->subjob_obj = ( ( mvs_object_t* ) subjob_obj );

        rc = __aprv2_cmd_alloc_send(
               mvs_my_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               mvs_my_addr, MVS_PORT,
               mvs_mvm_addr, mvs_mvm_port,
               subjob_obj->header.handle,
               VSS_IMVM_CMD_MODEM_START_VOICE, NULL, 0 );
        // MVS_PANIC_ON_ERROR( rc );
        if ( rc )
        {
          /* Temporary workaround to handle SSR phase 3. Any APR IPC communication 
           * failure will cause the current command to fail instead of panicing on 
           * failure.
           */
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                  "mvs_mod_enable_sequencer(): Failed to communicate with " \
                  "MVM, rc = (0x%08X)",
                  rc );

          seqjob_obj->status = rc;
          seqjob_obj->state = MVS_ENABLE_SEQUENCER_ENUM_COMPLETE;

          rc = mvs_free_object( ( mvs_object_t* ) subjob_obj );
          MVS_PANIC_ON_ERROR( rc );
        }
        else
        {
          seqjob_obj->state = MVS_ENABLE_SEQUENCER_ENUM_START_VOICE_WAIT;
        }
      }
      continue;

    case MVS_ENABLE_SEQUENCER_ENUM_START_VOICE_WAIT:
      {
        /* Wait for MVM to START_VOICE. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        seqjob_obj->status = subjob_obj->status;
        seqjob_obj->state = MVS_ENABLE_SEQUENCER_ENUM_COMPLETE;

        rc = mvs_free_object( ( mvs_object_t* ) subjob_obj );
        MVS_PANIC_ON_ERROR( rc );
      }
      continue;

    case MVS_ENABLE_SEQUENCER_ENUM_COMPLETE:
      {
        voicecfg_cmd_get_bool_item_t item;

        item.id = VOICECFG_CFG_IS_MVS_EVENT_SEQUENCE_ENABLED;
        item.ret_value = FALSE;

        /* End the sequencer. */
        if ( seqjob_obj->status == APR_EOK )
        {
           _rc = voicecfg_call( VOICECFG_CMD_GET_BOOL_ITEM, &item, sizeof ( item ) );

           if ( !_rc && ( item.ret_value == TRUE ) )       
           {
             mvs_send_mode_event( client, MVS_MODE_INIT, mode ) ;
             if ( ( is_standby == FALSE ) || ( is_standby && mode == MVS_MODE_NONE ) )
             {
               mvs_send_mode_event( client, MVS_MODE_READY, mode) ;
             }
             mvs_send_cmd_event( client, MVS_CMD_SUCCESS );
           }
           else
           {
              mvs_send_cmd_event( client, MVS_CMD_SUCCESS );
              mvs_send_mode_event( client, MVS_MODE_INIT, mode );
              if ( ( is_standby == FALSE ) || ( is_standby && mode == MVS_MODE_NONE ) )
              {
                 mvs_send_mode_event( client, MVS_MODE_READY, mode);
              }
           }

          if ( ( ( client == MVS_CLIENT_WCDMA ) || ( client == MVS_CLIENT_TDSCDMA ) ) && 
               ( is_standby == FALSE ) )
          {
#ifndef WINSIM
            mvs_dsm_amr_activate( TRUE );
            MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "DSM activate done" );
#endif /* !WINSIM */
          }
          /* Send sample rate notification (8 KHz) to NAS/GHDI whenever the
           * vocoder configured is AMR. This is in addition to the sample rate
           * notification that is sent by Modem during call set-up and
           * handovers. 
           *  
           * This is to avoid incorrect sample rate notification to AP during 
           * inter-RAT handovers, when eAMR is not enabled for the current 
           * client and eAMR is enabled for other clients. 
           */
          if ( ( mvs_client_table[mvs_cur_client].config_mode == MVS_MODE_AMR ) &&
               ( is_standby == FALSE ) )
          {
            if ( mvs_is_eamr_enabled ( mvs_cur_client ) == FALSE ) 
            {
              ( void ) mvs_send_eamr_sr_event( mvs_cur_client, MVS_EAMR_SR_8000 );
              MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,  "MVS sends AMR sample " \
                    "rate notification event, "
                    "client = (%d), sample rate = 8000 Hz", mvs_cur_client );
            }
          }
          mvs_update_state( client );
        }
        else
        {
          mvs_send_cmd_event( client, MVS_CMD_FAILURE );
        }

        rc = mvs_free_object( ( mvs_object_t* ) seqjob_obj );
        MVS_PANIC_ON_ERROR( rc );
        ctrl->rootjob_obj = NULL;

        rc = __aprv2_cmd_free( mvs_my_apr_handle, ctrl->packet );
        MVS_PANIC_ON_ERROR( rc );

        if ( is_standby )
        {
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_LOW, 
                 "mvs_mod_standby_proc(): complete - current client=(%d) and mode=(%d)",
                 mvs_cur_client, mvs_client_table[mvs_cur_client].config_mode );
        }
        else
        {
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_LOW,
                 "mvs_mod_enable_proc(): complete - current client=(%d) and mode=(%d)",
                 mvs_cur_client, mvs_client_table[mvs_cur_client].config_mode );
        }
      }
      return APR_EOK;

    default:
      MVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
      break;
    }
  }

  return APR_EOK;
}

int32_t mvs_mod_enable (
  mvs_client_type client,
  mvs_mode_type voc_mode,
  mvs_packet_ul_fn_type ul_func,
  mvs_packet_dl_fn_type dl_func,
  mvs_pkt_context_type context
)
{
  int32_t rc;
  mvs_packet_type mvs_cmd;
  aprv2_packet_t* packet = NULL;
  bool_t is_ok = FALSE;

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_mod_enable client = (%d)",
        client );

  if ( client >= MVS_CLIENT_MAX )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "Invalid client = (%d)",
            client );
    return APR_EBADPARAM;
  }

  if ( mvs_is_initialized )
  {
    mvs_cmd.enable.hdr.cmd = MVS_CMD_ENABLE;
    mvs_cmd.enable.client = client;
    mvs_cmd.enable.mode = voc_mode;
    mvs_cmd.enable.ul_func = ul_func;
    mvs_cmd.enable.dl_func = dl_func;
    mvs_cmd.enable.context = context;

    rc = mvs_make_cmd_packet( &mvs_cmd, &packet );
    if ( rc )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "make pkt failed!!" );
      is_ok = FALSE;
    }
    else
    {
      is_ok = mvs_queue_incoming_packet( packet );
    }
  }

  if ( is_ok == FALSE )
  {
    /* TODO: Need a cleaner solution. If MVS is not initialized, then it
     *       would not be possible to access the mvs_client_table[] required
     *       by mvs_send_cmd_event() anyway.
     */
    mvs_send_cmd_event( client, MVS_CMD_FAILURE );
  }

  return APR_EOK;
}

static int32_t mvs_mod_enable_proc (
  mvs_pending_control_t* ctrl
)
{
  int32_t rc;
  mvs_packet_type* enable_cmd;
  mvs_client_type client;
  mvs_sequencer_job_object_t* seqjob_obj;

  enable_cmd = APRV2_PKT_GET_PAYLOAD( mvs_packet_type, ctrl->packet );

  client = enable_cmd->enable.client;

  if ( client >= MVS_CLIENT_MAX )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "Invalid client = (%d)",
            client );
    return APR_EBADPARAM;
  }

  if ( ctrl->state == MVS_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_mod_enable_proc(): client = " \
          "(%d) mode = (%d)",
          enable_cmd->enable.client, enable_cmd->enable.mode );

    if ( ( mvs_cur_client == MVS_CLIENT_NONE ) || ( mvs_cur_client == client ) )
    {
      mvs_client_table[client].pending_cmd.cur_cmd = MVS_CMD_ENABLE; 
      mvs_client_table[client].pending_cmd.is_done = FALSE;
      mvs_cur_client = client;
    }
    else
    {
      /* MVS is still not released by other client, report back CMD_FAILURE.
       */
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_mod_enable_proc(): MVS " \
              "was not released by mvs_cur_client = (%d),"
              "when client = (%d) called enable_proc",
              mvs_cur_client, client );
      
      mvs_send_mode_event( client, MVS_MODE_NOT_AVAIL, MVS_MODE_NONE );
      mvs_send_cmd_event( client, MVS_CMD_FAILURE );

      mvs_client_table[client].pending_cmd.is_done = TRUE;

      rc = __aprv2_cmd_free( mvs_my_apr_handle, ctrl->packet );
      MVS_PANIC_ON_ERROR( rc );

      return APR_EOK;
    }

    rc = mvs_create_sequencer_job_object(
           ( mvs_sequencer_job_object_t** ) &ctrl->rootjob_obj );
    MVS_PANIC_ON_ERROR( rc );
    seqjob_obj = &ctrl->rootjob_obj->sequencer_job;

    switch ( mvs_client_table[client].state )
    {
    case MVS_STATE_IDLE:
      {
        if ( mvs_is_mvm_up )
        {
          /* Store the new packet exchange callbacks. */
          mvs_client_table[client].config_mode = enable_cmd->enable.mode;
          mvs_client_table[client].ul_cb = enable_cmd->enable.ul_func;
          mvs_client_table[client].dl_cb = enable_cmd->enable.dl_func;

          if ( enable_cmd->enable.mode != MVS_MODE_NONE )
          {
            /* mvs_enable(mode != NONE) in IDLE state: go through 
             * the whole START_VOICE sequence beginning with: 
             * CLEAN_STALE_CS_SESSIONS if this is the first call,  
             * STANDBY_VOICE if this is not the first call.
             */
            if ( mvs_is_first_call == TRUE )
            {
              mvs_is_first_call = FALSE;
              ctrl->rootjob_obj->sequencer_job.state = MVS_ENABLE_SEQUENCER_ENUM_CREATE_MVM;
            }
            else
            {
              ctrl->rootjob_obj->sequencer_job.state = MVS_ENABLE_SEQUENCER_ENUM_STANDBY_VOICE;
            }

          }
          else /* MVS_MODE_NONE */
          {
            /* mvs_enable(mode = NONE) in IDLE state: nothing to. Reply
             * CMD_SUCCESS to client.
             */
            mvs_send_cmd_event( client, MVS_CMD_SUCCESS );

            mvs_client_table[client].pending_cmd.is_done = TRUE;

            rc = mvs_free_object( ( mvs_object_t* ) seqjob_obj );
            MVS_PANIC_ON_ERROR( rc );
            ctrl->rootjob_obj = NULL;

            rc = __aprv2_cmd_free( mvs_my_apr_handle, ctrl->packet );
            MVS_PANIC_ON_ERROR( rc );

            return APR_EOK;
          }
        }
        else
        {
          /* MVS is not ready, report back CMD_FAILURE.
           */
          MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "mvs_mod_enable_proc(): MVS " \
               "not available" );
          mvs_send_mode_event( mvs_cur_client, MVS_MODE_NOT_AVAIL, MVS_MODE_NONE );
          mvs_send_cmd_event(client, MVS_CMD_FAILURE);

          mvs_client_table[client].pending_cmd.is_done = TRUE;

          rc = mvs_free_object( ( mvs_object_t* ) seqjob_obj );
          MVS_PANIC_ON_ERROR( rc );
          ctrl->rootjob_obj = NULL;

          rc = __aprv2_cmd_free( mvs_my_apr_handle, ctrl->packet );
          MVS_PANIC_ON_ERROR( rc );

          return APR_EOK;
        }
      }
      break;

    case MVS_STATE_VOCODER:
      {
        if ( mvs_is_mvm_up )
        {
          /* Store the new packet exchange callbacks. */
          mvs_client_table[client].config_mode = enable_cmd->enable.mode;
          mvs_client_table[client].ul_cb = enable_cmd->enable.ul_func;
          mvs_client_table[client].dl_cb = enable_cmd->enable.dl_func;

          if ( enable_cmd->enable.mode != MVS_MODE_NONE )
          {
            /* mvs_enable(mode != NONE) while running: go through the
             * whole START_VOICE sequence.
             */
            ctrl->rootjob_obj->sequencer_job.state = MVS_ENABLE_SEQUENCER_ENUM_STANDBY_VOICE;

          }
          else /* MVS_MODE_NONE */
          {
            /* mvs_enable(mode = NONE) while running: STOP_VOICE. */
            ctrl->rootjob_obj->sequencer_job.state = MVS_ENABLE_SEQUENCER_ENUM_STOP_VOICE;

          }
        }
        else
        {
          /* MVS is not ready, report back CMD_FAILURE.
           */
          MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "mvs_mod_enable_proc(): MVS " \
               "not available" );
          mvs_send_mode_event( mvs_cur_client, MVS_MODE_NOT_AVAIL, MVS_MODE_NONE );
          mvs_send_cmd_event(client, MVS_CMD_FAILURE);

          mvs_client_table[client].pending_cmd.is_done = TRUE;

          rc = mvs_free_object( ( mvs_object_t* ) seqjob_obj );
          MVS_PANIC_ON_ERROR( rc );
          ctrl->rootjob_obj = NULL;

          rc = __aprv2_cmd_free( mvs_my_apr_handle, ctrl->packet );
          MVS_PANIC_ON_ERROR( rc );

          return APR_EOK;
        }
      }
      break;

    case MVS_STATE_RESET:
      {
        /* Client sent mvs_enable() without having acquired first. Can't do
         * that. Report back CMD_FAILURE.
         */
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "mvs_mod_enable_proc(): " \
               "cannot enable in state (%d)",
               mvs_client_table[client].state );
        mvs_send_cmd_event( client, MVS_CMD_FAILURE );

        mvs_client_table[client].pending_cmd.is_done = TRUE;

        rc = mvs_free_object( ( mvs_object_t* ) seqjob_obj );
        MVS_PANIC_ON_ERROR( rc );
        ctrl->rootjob_obj = NULL;

        rc = __aprv2_cmd_free( mvs_my_apr_handle, ctrl->packet );
        MVS_PANIC_ON_ERROR( rc );

        return APR_EOK;
      }

    default:
      MVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
      break;
    }
  } /* if ( ctrl->state == MVS_PENDING_CMD_STATE_ENUM_EXECUTE ) */

  rc = mvs_mod_enable_sequencer( ctrl, FALSE );

  return rc;
}

int32_t mvs_mod_standby (
  mvs_client_type client,
  mvs_mode_type voc_mode
)
{
  int32_t rc;
  mvs_packet_type mvs_cmd;
  aprv2_packet_t* packet = NULL;
  bool_t is_ok = FALSE;

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_mod_standby client = (%d)",
        client );

  if ( client >= MVS_CLIENT_MAX )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "Invalid client = (%d)",
            client );
    return APR_EBADPARAM;
  }

  if ( mvs_is_initialized )
  {
    mvs_cmd.standby.hdr.cmd = MVS_CMD_STANDBY;
    mvs_cmd.standby.client = client;
    mvs_cmd.standby.mode = voc_mode;

    rc = mvs_make_cmd_packet( &mvs_cmd, &packet );
    if ( rc )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "make pkt failed!!" );
      is_ok = FALSE;
    }
    else
    {
      is_ok = mvs_queue_incoming_packet( packet );
    }
  }

  if ( is_ok == FALSE )
  {
    /* TODO: Need a cleaner solution. If MVS is not initialized, then it
     *       would not be possible to access the mvs_client_table[] required
     *       by mvs_send_cmd_event() anyway.
     */
    mvs_send_cmd_event( client, MVS_CMD_FAILURE );
  }

  return APR_EOK;
}

static int32_t mvs_mod_standby_proc (
  mvs_pending_control_t* ctrl
)
{
  int32_t rc;
  mvs_packet_type* standby_cmd;
  mvs_client_type client;
  mvs_sequencer_job_object_t* seqjob_obj;

  standby_cmd = APRV2_PKT_GET_PAYLOAD( mvs_packet_type, ctrl->packet );

  client = standby_cmd->standby.client;

  if ( client >= MVS_CLIENT_MAX )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "Invalid client = (%d)",
            client );
    return APR_EBADPARAM;
  }

  if ( ctrl->state == MVS_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    if ( ( mvs_cur_client == MVS_CLIENT_NONE ) || ( mvs_cur_client == client ) )
    {
      mvs_client_table[client].pending_cmd.cur_cmd = MVS_CMD_STANDBY;
      mvs_client_table[client].pending_cmd.is_done = FALSE;
      mvs_cur_client = client;
    }
    else
    {
      /* MVS is still not released by other client, report back CMD_FAILURE.
       */
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_mod_standby_proc(): MVS " \
              "was not released by mvs_cur_client = (%d),"
              "when client = (%d) called standby_proc",
              mvs_cur_client, client );
      
      mvs_send_mode_event( client, MVS_MODE_NOT_AVAIL, MVS_MODE_NONE );
      mvs_send_cmd_event( client, MVS_CMD_FAILURE );

      mvs_client_table[client].pending_cmd.is_done = TRUE;

      rc = __aprv2_cmd_free( mvs_my_apr_handle, ctrl->packet );
      MVS_PANIC_ON_ERROR( rc );

      return APR_EOK;
    }

    rc = mvs_create_sequencer_job_object(
           ( mvs_sequencer_job_object_t** ) &ctrl->rootjob_obj );
    MVS_PANIC_ON_ERROR( rc );
    seqjob_obj = &ctrl->rootjob_obj->sequencer_job;

    switch ( mvs_client_table[client].state )
    {
    case MVS_STATE_IDLE:
      {
        mvs_client_table[client].config_mode = standby_cmd->standby.mode;

        if ( mvs_is_mvm_up )
        {        
            /* mvs_is_first_call would be always true at the time
             * of call start in 8916. CREATE_MVM if this is during call start,
             * STOP_VOICE if this is not during call establishment
             * irrespective of the mode
             */
            if ( mvs_is_first_call == TRUE )
            {
              mvs_is_first_call = FALSE;
              ctrl->rootjob_obj->sequencer_job.state = MVS_ENABLE_SEQUENCER_ENUM_CREATE_MVM;
            }
            else
            {
              ctrl->rootjob_obj->sequencer_job.state = MVS_ENABLE_SEQUENCER_ENUM_STOP_VOICE;
          }
        }
        else
        {
          /* MVS is not ready, report back CMD_FAILURE. */
          MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "mvs_mod_standby_proc(): MVS " \
               "not available" );
          mvs_send_mode_event( mvs_cur_client, MVS_MODE_NOT_AVAIL, MVS_MODE_NONE );
          mvs_send_cmd_event(client, MVS_CMD_FAILURE);

          mvs_client_table[client].pending_cmd.is_done = TRUE;

          rc = mvs_free_object( ( mvs_object_t* ) seqjob_obj );
          MVS_PANIC_ON_ERROR( rc );
          ctrl->rootjob_obj = NULL;

          rc = __aprv2_cmd_free( mvs_my_apr_handle, ctrl->packet );
          MVS_PANIC_ON_ERROR( rc );

          return APR_EOK;
        }
      }
      break;

    case MVS_STATE_VOCODER:
      {
        mvs_client_table[client].config_mode = standby_cmd->standby.mode;
        mvs_client_table[client].pending_cmd.is_done = FALSE;

        if ( mvs_is_mvm_up )
        {
            /* mvs_standby, while running: go to the STOP_VOICE sequence.
             */
            ctrl->rootjob_obj->sequencer_job.state = MVS_ENABLE_SEQUENCER_ENUM_STOP_VOICE;
          }
        else
        {
          /* MVS is not ready, report back CMD_FAILURE.
           */
          MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "mvs_mod_standby_proc(): MVS " \
               "not available" );
          mvs_send_mode_event( mvs_cur_client, MVS_MODE_NOT_AVAIL, MVS_MODE_NONE );
          mvs_send_cmd_event(client, MVS_CMD_FAILURE);

          mvs_client_table[client].pending_cmd.is_done = TRUE;

          rc = mvs_free_object( ( mvs_object_t* ) seqjob_obj );
          MVS_PANIC_ON_ERROR( rc );
          ctrl->rootjob_obj = NULL;

          rc = __aprv2_cmd_free( mvs_my_apr_handle, ctrl->packet );
          MVS_PANIC_ON_ERROR( rc );

          return APR_EOK;
        }
      }
      break;

    case MVS_STATE_RESET:
      {
        /* Client sent mvs_standby() without having acquired first. Can't do
         * that. Report back CMD_FAILURE.
         */
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "mvs_mod_standby_proc(): " \
               "cannot enable in state (%d)",
               mvs_client_table[client].state );
        mvs_send_cmd_event(client, MVS_CMD_FAILURE);

        mvs_client_table[client].pending_cmd.is_done = TRUE;

        rc = mvs_free_object( ( mvs_object_t* ) seqjob_obj );
        MVS_PANIC_ON_ERROR( rc );
        ctrl->rootjob_obj = NULL;

        rc = __aprv2_cmd_free( mvs_my_apr_handle, ctrl->packet );
        MVS_PANIC_ON_ERROR( rc );

        return APR_EOK;
      }

    default:
      MVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
      break;
    }
  } /* if ( ctrl->state == MVS_PENDING_CMD_STATE_ENUM_EXECUTE ) */

  rc = mvs_mod_enable_sequencer( ctrl, TRUE );

  return rc;
}

int32_t mvs_mod_release (
  mvs_client_type client
)
{
  int32_t rc;
  mvs_packet_type mvs_cmd;
  aprv2_packet_t* packet = NULL;
  bool_t is_ok = FALSE;

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_mod_release client = (%d)",
        client );

  if ( client >= MVS_CLIENT_MAX )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "Invalid client = (%d)",
            client );
    return APR_EBADPARAM;
  }

  if ( mvs_is_initialized )
  {
    mvs_cmd.release.hdr.cmd = MVS_CMD_RELEASE;
    mvs_cmd.release.client = client;

    rc = mvs_make_cmd_packet( &mvs_cmd, &packet );
    if ( rc )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "make pkt failed!!" );
      is_ok = FALSE;
    }
    else
    {
      is_ok = mvs_queue_incoming_packet( packet );
    }
  }

  if ( is_ok == FALSE )
  {
    /* TODO: Need a cleaner solution. If MVS is not initialized, then it
     *       would not be possible to access the mvs_client_table[] required
     *       by mvs_send_cmd_event() anyway.
     */
    mvs_send_cmd_event( client, MVS_CMD_FAILURE );
  }

  return APR_EOK;
}

static int32_t mvs_helper_destroy_proc (
  mvs_sequencer_job_object_t* seqjob_obj
)
{
  int32_t rc;
  mvs_simple_job_object_t* subjob_obj;

  enum{
    MVS_SEQUENCER_ENUM_DETACH_STREAM, 
    MVS_SEQUENCER_ENUM_DETACH_STREAM_WAIT,
    MVS_SEQUENCER_ENUM_DESTROY_CVS,
    MVS_SEQUENCER_ENUM_DESTROY_CVS_WAIT,
    MVS_SEQUENCER_ENUM_DESTROY_MVM,
    MVS_SEQUENCER_ENUM_DESTROY_MVM_WAIT,
    MVS_SEQUENCER_ENUM_COMPLETE
  };

  if( seqjob_obj->helper_state == APR_UNDEFINED_ID_V )
  {
    seqjob_obj->helper_state = MVS_SEQUENCER_ENUM_DETACH_STREAM;
  }
  
  for ( ;; )
  {
    switch ( seqjob_obj->helper_state )
    {
    case MVS_SEQUENCER_ENUM_DETACH_STREAM:
      {
        if ( mvs_cvs_port == APR_NULL_V )
        {
          seqjob_obj->helper_state = MVS_SEQUENCER_ENUM_DESTROY_MVM;
        }
        else
        {
          rc = mvs_create_simple_job_object( APR_NULL_V, &subjob_obj );
          MVS_PANIC_ON_ERROR( rc );
          seqjob_obj->subjob_obj = ( ( mvs_object_t* ) subjob_obj );

          rc = __aprv2_cmd_alloc_send(
                 mvs_my_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                 mvs_my_addr, MVS_PORT,
                 mvs_mvm_addr, mvs_mvm_port,
                 subjob_obj->header.handle, VSS_IMVM_CMD_DETACH_STREAM,
                 &mvs_cvs_port, sizeof( mvs_cvs_port ) );

          if ( rc )
          {
            /* Temporary workaround to handle SSR phase 3. Any APR IPC communication 
             * failure will cause the current command to fail instead of panicing on 
             * failure.
             */
            MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                    "mvs_helper_destroy_proc(): Failed to communicate with " \
                    "MVM, rc = (0x%08X)",
                    rc );

            seqjob_obj->status = rc;
            seqjob_obj->helper_state = MVS_SEQUENCER_ENUM_COMPLETE;

            rc = mvs_free_object( ( mvs_object_t* ) subjob_obj );
            MVS_PANIC_ON_ERROR( rc );
          }
          else
          {
            seqjob_obj->helper_state = MVS_SEQUENCER_ENUM_DETACH_STREAM_WAIT;
          }
        }
      }
      continue;

    case MVS_SEQUENCER_ENUM_DETACH_STREAM_WAIT:
      {
        /* Wait for CVS to destroy. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        seqjob_obj->status = subjob_obj->status;

        /* Cancel listen for eAMR mode change event.*/
        if( mvs_cvs_port != APR_NULL_V &&
            mvs_is_listen_for_emar_mode_change == TRUE )
        {
          vss_inotify_cmd_cancel_event_class_t ctrl_args;

          ctrl_args.class_id = VSS_ISTREAM_EVENT_CLASS_EAMR_MODE_CHANGE;
          rc = __aprv2_cmd_alloc_send(
                      mvs_my_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                      mvs_my_addr, MVS_PORT,
                      mvs_cvs_addr, mvs_cvs_port,
                      0, VSS_INOTIFY_CMD_CANCEL_EVENT_CLASS,
                      &ctrl_args, sizeof( ctrl_args ) );

          if ( rc )
          {
            /* Temporary workaround to handle SSR phase 3. Any APR IPC communication 
             * failure will cause the current command to fail instead of panicing on 
             * failure.
             */
            MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                    "mvs_helper_destroy_proc(): Failed to communicate with "
                    "CVS, rc = (0x%08X)", rc );

            seqjob_obj->status = rc;
            seqjob_obj->helper_state = MVS_SEQUENCER_ENUM_COMPLETE;

            rc = mvs_free_object( ( mvs_object_t* ) subjob_obj );
            MVS_PANIC_ON_ERROR( rc );

            continue;
          }

          mvs_is_listen_for_emar_mode_change = FALSE;
        }

        seqjob_obj->helper_state = MVS_SEQUENCER_ENUM_DESTROY_CVS;

        rc = mvs_free_object( ( mvs_object_t* ) subjob_obj );
        MVS_PANIC_ON_ERROR( rc );
      }
      continue;

    case MVS_SEQUENCER_ENUM_DESTROY_CVS:
      {
        if ( mvs_cvs_port == APR_NULL_V )
        {
          seqjob_obj->helper_state = MVS_SEQUENCER_ENUM_DESTROY_MVM;
        }
        else
        {
          rc = mvs_create_simple_job_object( APR_NULL_V, &subjob_obj );
          MVS_PANIC_ON_ERROR( rc );
          seqjob_obj->subjob_obj = ( ( mvs_object_t* ) subjob_obj );

          rc = __aprv2_cmd_alloc_send(
                 mvs_my_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                 mvs_my_addr, MVS_PORT,
                 mvs_cvs_addr, mvs_cvs_port,
                 subjob_obj->header.handle, APRV2_IBASIC_CMD_DESTROY_SESSION,
                 NULL, 0 );

          if ( rc )
          {
            /* Temporary workaround to handle SSR phase 3. Any APR IPC communication 
             * failure will cause the current command to fail instead of panicing on 
             * failure.
             */
            MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                    "mvs_helper_destroy_proc(): Failed to communicate with " \
                    "CVS, rc = (0x%08X)",
                    rc );

            seqjob_obj->status = rc;
            seqjob_obj->helper_state = MVS_SEQUENCER_ENUM_COMPLETE;

            rc = mvs_free_object( ( mvs_object_t* ) subjob_obj );
            MVS_PANIC_ON_ERROR( rc );
          }
          else
          {
            seqjob_obj->helper_state = MVS_SEQUENCER_ENUM_DESTROY_CVS_WAIT;
          }
        }
      }
      continue;

    case MVS_SEQUENCER_ENUM_DESTROY_CVS_WAIT:
      {
        /* Wait for CVS to destroy. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        mvs_cvs_port = APR_NULL_V;
          /**< Clear out the CVS port. */

        seqjob_obj->status = subjob_obj->status;
        seqjob_obj->helper_state = MVS_SEQUENCER_ENUM_DESTROY_MVM;

        rc = mvs_free_object( ( mvs_object_t* ) subjob_obj );
        MVS_PANIC_ON_ERROR( rc );
      }
      continue;

    case MVS_SEQUENCER_ENUM_DESTROY_MVM:
      {
        if ( mvs_mvm_port == APR_NULL_V )
        {
          seqjob_obj->helper_state = MVS_SEQUENCER_ENUM_COMPLETE;
        }
        else
        {
          rc = mvs_create_simple_job_object( APR_NULL_V, &subjob_obj );
          MVS_PANIC_ON_ERROR( rc );
          seqjob_obj->subjob_obj = ( ( mvs_object_t* ) subjob_obj );

          rc = __aprv2_cmd_alloc_send(
                 mvs_my_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                 mvs_my_addr, MVS_PORT,
                 mvs_mvm_addr, mvs_mvm_port,
                 subjob_obj->header.handle, APRV2_IBASIC_CMD_DESTROY_SESSION,
                 NULL, 0 );

          if ( rc )
          {
            /* Temporary workaround to handle SSR phase 3. Any APR IPC communication 
             * failure will cause the current command to fail instead of panicing on 
             * failure.
             */
            MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                    "mvs_helper_destroy_proc(): Failed to communicate with " \
                    "MVM, rc = (0x%08X)",
                    rc );

            seqjob_obj->status = rc;
            seqjob_obj->helper_state = MVS_SEQUENCER_ENUM_COMPLETE;

            rc = mvs_free_object( ( mvs_object_t* ) subjob_obj );
            MVS_PANIC_ON_ERROR( rc );
          }
          else
          {
            seqjob_obj->helper_state = MVS_SEQUENCER_ENUM_DESTROY_MVM_WAIT;
          }
        }
      }
      continue;

    case MVS_SEQUENCER_ENUM_DESTROY_MVM_WAIT:
      { /* Wait for the MVM state machine to finish. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        mvs_mvm_port = APR_NULL_V;
          /**< Clear out the MVM port. */

        seqjob_obj->status = APR_EOK;
        seqjob_obj->helper_state = MVS_SEQUENCER_ENUM_COMPLETE;

        ( void ) mvs_free_object( ( mvs_object_t* ) subjob_obj );
      }
      continue;

    case MVS_SEQUENCER_ENUM_COMPLETE:
         rc = APR_EOK; 
         break;
 
    default:
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_helper_destroy_proc (): " \
              "unexpected state" );
        MVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
      }
      break;
 
     }  /* end of switch ( seqjob_obj->state ) */
     break;
  }
  return rc;
}

static int32_t mvs_mod_release_proc (
  mvs_pending_control_t* ctrl
)
{
  int32_t rc;
  uint32_t _rc;
  mvs_client_type client;
  mvs_packet_type* release_cmd;
  mvs_sequencer_job_object_t* seqjob_obj;
  mvs_simple_job_object_t* subjob_obj;
  voicecfg_cmd_get_bool_item_t item;
  
  enum {
    MVS_SEQUENCER_ENUM_UNINITIALIZED,
    MVS_SEQUENCER_ENUM_STOP_VOICE,
    MVS_SEQUENCER_ENUM_STOP_VOICE_WAIT,
    MVS_SEQUENCER_ENUM_DESTROY_WAIT,
    MVS_SEQUENCER_ENUM_COMPLETE,
    MVS_SEQUENCER_ENUM_INVALID
  };

  release_cmd = APRV2_PKT_GET_PAYLOAD( mvs_packet_type, ctrl->packet );

  client = release_cmd->release.client;

  if ( client >= MVS_CLIENT_MAX )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "Invalid client = (%d)",
            client );
    return APR_EBADPARAM;
  }

  if ( ctrl->state == MVS_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    mvs_client_table[client].pending_cmd.cur_cmd = MVS_CMD_RELEASE;
    mvs_client_table[client].pending_cmd.is_done = FALSE;

    rc = mvs_create_sequencer_job_object(
           ( mvs_sequencer_job_object_t** ) &ctrl->rootjob_obj );
    MVS_PANIC_ON_ERROR( rc );
    seqjob_obj = &ctrl->rootjob_obj->sequencer_job;

    switch ( mvs_client_table[client].state )
    {
    case MVS_STATE_IDLE:
    case MVS_STATE_VOCODER:
      {
      
        /* Release process for the current client. */
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "mvs_mod_release_proc(): " \
               "releasing in state (%d), client (%d)",
               mvs_client_table[client].state, client );

        mvs_send_mode_event( client, MVS_MODE_READY, MVS_MODE_NONE );
        mvs_send_cmd_event( client, MVS_CMD_SUCCESS );

        if ( client == mvs_cur_client )
        {
          if ( ( mvs_is_silence_frame_supported_for_mvs_cur_client( ) == TRUE ) && 
              ( mvs_is_registered_with_voiceapp == TRUE ) )
          {
            rc = mvs_deregister_modem_timing( );
            if ( rc == APR_EOK )
            {
              mvs_is_registered_with_voiceapp = FALSE;
            }
          }
         
          ctrl->rootjob_obj->sequencer_job.state = MVS_SEQUENCER_ENUM_STOP_VOICE;
          mvs_cur_client = MVS_CLIENT_NONE;
        }
        else
        {
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "mvs_mod_release_proc(): " \
                 "mvs_cur_client = (%d) is not same as client = (%d)",
                 mvs_cur_client, client );
          ctrl->rootjob_obj->sequencer_job.state = MVS_SEQUENCER_ENUM_COMPLETE;
        }

        { /* Deregistering WM channels and clearing event callback. */
#ifndef WINSIM
          if ( mvs_client_table[client].config_mode == MVS_MODE_AMR ||
               mvs_client_table[client].config_mode == MVS_MODE_AMR_WB )
          {
            if ( client == MVS_CLIENT_WCDMA )
            { /* Deregistering WM channels for WCDMA client. */
              MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,   \
                     "mvs_mod_release_proc(): Deregistering WM channels for " \
                     "client (%d)",
                     client );

              /* Do WCDMA DSM clean-up. */
              mvs_wcdma_set_ul_channel( 0, 0, 0, MVSSUP_UMTS_CHAN_CLASS_NONE );
              mvs_wcdma_set_dl_channel( 0, 0, 0, MVSSUP_UMTS_CHAN_CLASS_NONE, NULL );
              mvs_dsm_amr_activate( FALSE );
            }
            else if ( client == MVS_CLIENT_TDSCDMA )
            { /* Deregistering WM channels for TDSCDMA client. */
              MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,   \
                     "mvs_mod_release_proc(): Deregistering WM channels for " \
                     "client (%d)",
                     client );
 
              /* Do TD-SCDMA DSM clean-up. */
              mvs_tdscdma_set_ul_channel( 0, 0, 0, MVSSUP_UMTS_CHAN_CLASS_NONE );
              mvs_tdscdma_set_dl_channel( 0, 0, 0, MVSSUP_UMTS_CHAN_CLASS_NONE, NULL );
              mvs_dsm_amr_activate( FALSE );
            }
          }
#endif /* !WINSIM */

          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "mvs_mod_release_proc(): " \
                 "Clearing event callbacks for client (%d)",
                 client );

          mvs_client_table[client].dl_cb = NULL;
          mvs_client_table[client].ul_cb = NULL;
          mvs_client_table[client].event_cb = NULL;
          mvs_client_table[client].config_mode = MVS_MODE_NONE;
          mvs_client_table[client].pending_cmd.cur_cmd = MVS_CMD_RELEASE;
          mvs_client_table[client].pending_cmd.is_done = FALSE;
        }		
      }
      break;

    case MVS_STATE_RESET:
      {
        /* Client sent mvs_release() without having acquired. */
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "mvs_mod_release_proc(): " \
               "releasing in state (%d)",
               mvs_client_table[client].state );

        mvs_send_mode_event( client, MVS_MODE_READY, MVS_MODE_NONE );
        mvs_send_cmd_event( client, MVS_CMD_SUCCESS );

        mvs_client_table[client].event_cb = NULL;
        mvs_client_table[client].pending_cmd.is_done = TRUE;

        rc = mvs_free_object( ( mvs_object_t* ) seqjob_obj );
        MVS_PANIC_ON_ERROR( rc );
        ctrl->rootjob_obj = NULL;

        rc = __aprv2_cmd_free( mvs_my_apr_handle, ctrl->packet );
        MVS_PANIC_ON_ERROR( rc );

        return APR_EOK;
      }

    default:
      MVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
      break;

    }
  } /* end of if ( ctrl->state == MVS_PENDING_CMD_STATE_ENUM_EXECUTE ) */

  seqjob_obj = &ctrl->rootjob_obj->sequencer_job;

  /* Ending the current session. */
    for ( ;; )
    {
      switch ( seqjob_obj->state )
      {
      case MVS_SEQUENCER_ENUM_STOP_VOICE:
        {
          /* If there is no valid MVM handle, complete the sequence. */
          if ( mvs_mvm_port == APR_NULL_V )
          {
            MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "mvs_mod_release_proc(): " \
                 "mvm session handle is NULL, complete sequence" );
            seqjob_obj->state = MVS_SEQUENCER_ENUM_COMPLETE;
            continue;
          }

          /* STOP_VOICE on MVM. */
          rc = mvs_create_simple_job_object( APR_NULL_V, &subjob_obj );
          MVS_PANIC_ON_ERROR( rc );
          seqjob_obj->subjob_obj = ( ( mvs_object_t* ) subjob_obj );

          rc = __aprv2_cmd_alloc_send(
                 mvs_my_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                 mvs_my_addr, MVS_PORT,
                 mvs_mvm_addr, mvs_mvm_port,
                 subjob_obj->header.handle,
                 VSS_IMVM_CMD_MODEM_STOP_VOICE, NULL, 0 );

          if ( rc )
          {
            /* Temporary workaround to handle SSR phase 3. APR IPC communication
             * failure while processing the release call will result the release
             * complete without stop voice on MVM. 
             */
            MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_mod_release_proc(): " \
                    "Failed to communicate with MVM, rc = (0x%08X)",
                    rc );
            MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_mod_release_proc(): " \
                  "Not able to stop voice on MVM. Allowing the release " \
                  "sequence to complete." );
            seqjob_obj->status = APR_EOK;
            seqjob_obj->state = MVS_SEQUENCER_ENUM_COMPLETE;
            rc = mvs_free_object( ( mvs_object_t* ) subjob_obj );
            MVS_PANIC_ON_ERROR( rc );
            continue;
          }

          seqjob_obj->state = MVS_SEQUENCER_ENUM_STOP_VOICE_WAIT;
        }
        continue;

      case MVS_SEQUENCER_ENUM_STOP_VOICE_WAIT:
        {
          /* Wait for MVM to STOP_VOICE. */
          subjob_obj = &seqjob_obj->subjob_obj->simple_job;

          {          
            /* Temporary workaround to handle SSR phase 3. If ADSP SSR happened,
             * the mvs_mvm_port will be set to APR_NULL_V. Allow the release to
             * complete in this case.
             */
            if ( mvs_mvm_port == APR_NULL_V )
            {
              MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "mvs_mod_release_proc(): " \
                   "mvm session handle become NULL due to ADSP SSR, complete " \
                   "sequence" );
              seqjob_obj->status = APR_EOK;
              seqjob_obj->state = MVS_SEQUENCER_ENUM_COMPLETE;
              rc = mvs_free_object( ( mvs_object_t* ) subjob_obj );
              MVS_PANIC_ON_ERROR( rc );
              continue;
            }
          }

          if ( subjob_obj->is_completed == FALSE )
          {
            return APR_EPENDING;
          }

          item.id = VOICECFG_CFG_IS_RETAIN_CVD_AFTER_RELEASE_OPTIMIZATION_DISABLED;
          item.ret_value = FALSE;
          _rc = voicecfg_call( VOICECFG_CMD_GET_BOOL_ITEM, &item, sizeof ( item ) );

          if ( !_rc && ( item.ret_value == TRUE ) )
          {
            seqjob_obj->status = APR_EOK;
            seqjob_obj->state = MVS_SEQUENCER_ENUM_COMPLETE;
          }
          else
          {
            mvs_is_first_call = TRUE;
            seqjob_obj->state = MVS_SEQUENCER_ENUM_DESTROY_WAIT;
            seqjob_obj->helper_state = APR_UNDEFINED_ID_V;
          }

          rc = mvs_free_object( ( mvs_object_t* ) subjob_obj );
          MVS_PANIC_ON_ERROR( rc );
        }
        continue;

      case MVS_SEQUENCER_ENUM_DESTROY_WAIT: 
        {
          rc = mvs_helper_destroy_proc( seqjob_obj );
          if( rc )
          { 
            return rc; 
          }
          else
          {
            seqjob_obj->state = MVS_SEQUENCER_ENUM_COMPLETE;
            continue;
          }
        }
      
      case MVS_SEQUENCER_ENUM_COMPLETE:
        {
          /* End the sequencer. */
          mvs_update_state( client );

          rc = mvs_free_object( ( mvs_object_t* ) seqjob_obj );
          MVS_PANIC_ON_ERROR( rc );
          ctrl->rootjob_obj = NULL;

          rc = __aprv2_cmd_free( mvs_my_apr_handle, ctrl->packet );
          MVS_PANIC_ON_ERROR( rc );

          MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,
               "mvs_mod_release_proc() - complete" );
          if ( mvs_processing_time_info.is_enabled )
          {
            MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "Processing time info in micro secs:" \
                   "max_time (%d), "
                   "min_time (%d)",
                   mvs_processing_time_info.max_time,
                   mvs_processing_time_info.min_time );
          }

#ifdef USE_MVS_PACKET_TIMING_ANALYSIS
          /* Print out time info at end of call. */
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "TIMING UL info: ul max " \
                 "deliver delay (%d), "
                 "ul max processing delay (%d)",
                 mvs_timing_info.ul_deliver_delay_max,
                 mvs_timing_info.ul_processing_delay_max );
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "ul slow misses count (%d), " \
                 "ul fast misses count (%d)",
                 mvs_timing_info.ul_missed_slow_count,
                 mvs_timing_info.ul_missed_fast_count );
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "TIMING DL info: dl max " \
                 "deliver delay (%d), "
                 "dl max processing delay (%d)",
                 mvs_timing_info.dl_deliver_delay_max,
                 mvs_timing_info.dl_processing_delay_max );
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "dl slow misses count (%d), " \
                 "dl fast misses count (%d)",
                 mvs_timing_info.dl_missed_slow_count,
                 mvs_timing_info.dl_missed_fast_count );

          /* Reset timing data. */  
          memset( ( ( void* ) &mvs_timing_info ), 0, sizeof( mvs_timing_info ) );
          mvs_ul_prev = 0;
          mvs_dl_prev = 0;
          mvs_timing_start_cnt_ul = 0;
          mvs_timing_start_cnt_dl = 0;
#endif /* USE_MVS_PACKET_TIMING_ANALYSIS */

          return APR_EOK;
        }

      default:
        MVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
        return APR_EFAILED;
      }

      break;
    }

  return APR_EPENDING;
}

static int32_t mvs_mod_shutdown_proc (
  mvs_pending_control_t* ctrl
)
{
  int32_t rc;
  mvs_sequencer_job_object_t* seqjob_obj;

  enum {
    MVS_SEQUENCER_ENUM_UNINITIALIZED,
    MVS_SEQUENCER_ENUM_DESTROY_WAIT,
    MVS_SEQUENCER_ENUM_COMPLETE,
    MVS_SEQUENCER_ENUM_INVALID
  };

  if ( ctrl->state == MVS_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    rc = mvs_create_sequencer_job_object(
           ( mvs_sequencer_job_object_t** ) &ctrl->rootjob_obj );
    MVS_PANIC_ON_ERROR( rc );

    ctrl->rootjob_obj->sequencer_job.state = MVS_SEQUENCER_ENUM_DESTROY_WAIT;
    ctrl->rootjob_obj->sequencer_job.helper_state = APR_UNDEFINED_ID_V;
  }

  seqjob_obj = &ctrl->rootjob_obj->sequencer_job;

  for ( ;; )
  {
    switch ( seqjob_obj->state )
    {
    case MVS_SEQUENCER_ENUM_DESTROY_WAIT:
        {
        rc = mvs_helper_destroy_proc( seqjob_obj );
        if( rc )
        {
          return rc;
        }
        else
        {
          seqjob_obj->state = MVS_SEQUENCER_ENUM_COMPLETE;
          continue;
        }
      }

    case MVS_SEQUENCER_ENUM_COMPLETE:
      { /* End the sequencer. */
        rc = __aprv2_cmd_end_command( mvs_my_apr_handle, ctrl->packet,
                                      seqjob_obj->status );
        MVS_PANIC_ON_ERROR( rc );
        ( void ) mvs_free_object( ( mvs_object_t* ) seqjob_obj );
        ctrl->rootjob_obj = NULL;

        mvs_is_initialized = FALSE;
          /**<
            * NOTE: This variable prevents MVS from processing any commands. It is
            *       possible for APR packet leaks during de-initialization because
            *       MVS is currently not waiting for all the enqueued commands to
            *       complete processing.
            */
      }
      return APR_EOK;

    default:
      MVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
      break;
    }

    break;
  }

  return APR_EPENDING;
}


static int32_t mvs_mod_adsp_reset_proc (
  mvs_pending_control_t *ctrl
)
{
  int32_t rc;

  if ( ctrl->state == MVS_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_mod_adsp_reset_proc(): " \
        "entering" );
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_ssr_debug: Enter, current " \
          "state is (%d)",
          mvs_client_table[mvs_cur_client].state );

    if ( mvs_is_allocated_objs_freed_on_ssr == FALSE )
    { /* Free all allocated objects. */ 
      ( void ) mvs_free_all_allocated_objects( );
      mvs_is_allocated_objs_freed_on_ssr = TRUE;
      MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_ssr_debug: freed all " \
          "allocated objects" );
    }

    switch ( mvs_client_table[mvs_cur_client].state )
    {
    case MVS_STATE_IDLE:
    case MVS_STATE_RESET:
      {
        if ( mvs_is_mvm_up )
        {
          rc = __aprv2_cmd_local_dns_lookup(
                 mvs_mvm_dns, sizeof( mvs_mvm_dns ), &mvs_mvm_addr );

          rc = __aprv2_cmd_local_dns_lookup(
                 mvs_cvs_dns, sizeof( mvs_cvs_dns ), &mvs_cvs_addr );

          if ( mvs_cur_client != MVS_CLIENT_NONE )
          {
            /* If we have a current client, and we've received this command it means that
             * the ADSP has just come up. We should tell the client that MVS is now READY. */
            mvs_send_mode_event( mvs_cur_client, MVS_MODE_READY, MVS_MODE_NONE );
          }
        }
        break;
      }

    case MVS_STATE_VOCODER:
      {
        if ( mvs_is_mvm_up == FALSE ) 
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_mod_adsp_reset_proc(): " \
              "adsp down, report NOT_AVAIL." );

          mvs_send_mode_event( mvs_cur_client, MVS_MODE_NOT_AVAIL,
                               mvs_client_table[mvs_cur_client].config_mode );
        }
      }
    }
    
    if ( mvs_cur_client != MVS_CLIENT_NONE )
    {
      mvs_client_table[mvs_cur_client].pending_cmd.is_done = FALSE;
      mvs_client_table[mvs_cur_client].pending_cmd.cur_cmd = MVS_CMD_SSR;

      mvs_update_state( mvs_cur_client );
    }

    rc = __aprv2_cmd_free( mvs_my_apr_handle, ctrl->packet );
    MVS_PANIC_ON_ERROR(rc);

    return rc;
  }
  else
  {
    MVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
  }

  return rc;
}

static uint32_t mvs_process_dl_amr_packet (
  uint8 *vocoder_packet,
  mvs_amr_frame_info_type *amr_rate,
  uint32 *vocoder_packet_size
)
{
  uint32_t nResult = APR_EOK;
  uint8 dest_voc_pkt[VOICE_PACKET_LENGTH];
  
  int mode;
  int frameType;

  MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_dl_amr_packet()" );

  if ( vocoder_packet == NULL || amr_rate == NULL || vocoder_packet_size == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_dl_amr_packet(): "
         "Invalid parameters." );
    return APR_EBADPARAM;
  }

  mode = (int)(amr_rate->dl_info.mode);
  frameType = (int)(amr_rate->dl_info.frame);

  //Validate mode and type
  if ( mode < MVS_AMR_MODE_0475 || mode > MVS_AMR_MODE_1220 ||
       frameType < MVS_AMR_SPEECH_GOOD || frameType > MVS_AMR_NO_DATA )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_dl_amr_packet(): "
           "Invalid frame_info: mode (%d), type (%d)", mode, frameType );
    *vocoder_packet_size = 0;
    return APR_EBADPARAM;
  }

  frameType = mvs_to_cvd_amr_frame_table[ frameType ];

  //Populate the packet header
  *dest_voc_pkt = ((frameType & 0x0F) << 4) | (mode & 0x0F);
  //keep room for packet header at 1st byte
  mmstd_memcpy(dest_voc_pkt +VOICE_PACKET_HDR_LENGTH,VOICE_PACKET_LENGTH 
               - VOICE_PACKET_HDR_LENGTH,vocoder_packet, VOICE_PACKET_LENGTH
               - VOICE_PACKET_HDR_LENGTH);  
  //Determine length of newly formed packet
  *vocoder_packet_size = mvs_amr_frame_len(amr_rate->dl_info.frame, amr_rate->dl_info.mode)
                                           + VOICE_PACKET_HDR_LENGTH;

  mmstd_memcpy(vocoder_packet,VOICE_PACKET_LENGTH,dest_voc_pkt, *vocoder_packet_size);

  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_dl_amr_packet(): " \
        "complete (%d)  mode = (%d)",
        nResult, mode );

  return nResult;
}


static uint32_t mvs_process_ul_amr_packet (
  uint8 *vocoder_packet,
  mvs_amr_frame_info_type *amr_rate,
  uint8 **speechData,
  uint32 *vocoder_packet_size
)
{
  uint32_t nResult = APR_EOK;
  int mode;
  int frameType;

  MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_ul_amr_packet()" );

  if ( vocoder_packet == NULL || amr_rate == NULL || speechData == NULL || vocoder_packet_size == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_ul_amr_packet(): " \
          "Invalid parameters" );
    return APR_EBADPARAM;
  }

  //Parse the mode and frame type
  frameType = (int)( ((*vocoder_packet) & 0xF0) >> 4 );
  mode = (int)( (*vocoder_packet) & 0x0F );

  frameType = cvd_to_mvs_amr_frame_type[ frameType ];

  //Validate mode and type
  if ( mode < MVS_AMR_MODE_0475 || mode > MVS_AMR_MODE_1220 ||
       frameType < MVS_AMR_SPEECH_GOOD || frameType > MVS_AMR_NO_DATA )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_ul_amr_packet(): " \
            "Invalid packet header: mode (%d), type (%d)",
            mode, frameType );
    *vocoder_packet_size = 0;
    return APR_EBADPARAM;
  }

  amr_rate->ul_info.mode = (mvs_amr_mode_type)(mode);
  amr_rate->ul_info.frame = (mvs_amr_frame_type)(frameType);

  //Get pointer to the beginning of speech data
  *speechData = vocoder_packet + VOICE_PACKET_HDR_LENGTH;

  //Determine the packet length
  *vocoder_packet_size = mvs_amr_frame_len(amr_rate->dl_info.frame, amr_rate->dl_info.mode);

  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_ul_amr_packet(): " \
        "complete (%d) mode = (%d)",
        nResult, mode );

  return nResult;
}

static uint32_t mvs_process_dl_amr_wb_packet (
  uint8 *vocoder_packet,
  mvs_amr_frame_info_type *amr_rate,
  uint32 *vocoder_packet_size
)
{
  uint32_t nResult = APR_EOK;
  int mode;
  int frameType;
  uint8 dest_voc_pkt[VOICE_PACKET_LENGTH];

  MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_dl_amr_wb_packet()" );

  if ( vocoder_packet == NULL || amr_rate == NULL || vocoder_packet_size == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_dl_amr_wb_packet(): " \
          "Invalid parameters." );
    return APR_EBADPARAM;
  }

  mode = (int)(amr_rate->dl_info.mode);
  frameType = (int)(amr_rate->dl_info.frame);

  //Validate mode and type
  if ( mode < MVS_AMR_MODE_0660 || mode > MVS_AMR_MODE_2385 ||
       frameType < MVS_AMR_SPEECH_GOOD || frameType > MVS_AMR_SPEECH_LOST )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_dl_amr_wb_packet(): " \
            "Invalid frame_info: mode (%d), type (%d)",
            mode, frameType );
    *vocoder_packet_size = 0;
    return APR_EBADPARAM;
  }

  frameType = mvs_to_cvd_armwb_frame_table[ frameType ];

  //Populate the packet header
  mode -= (int)(MVS_AMR_MODE_0660);
  *dest_voc_pkt = ((frameType & 0x0F) << 4) | (mode & 0x0F);

  mmstd_memcpy(dest_voc_pkt +VOICE_PACKET_HDR_LENGTH,VOICE_PACKET_LENGTH 
               - VOICE_PACKET_HDR_LENGTH ,vocoder_packet, VOICE_PACKET_LENGTH 
               - VOICE_PACKET_HDR_LENGTH);
  //Determine length of newly formed packet
  *vocoder_packet_size = mvs_amr_frame_len(amr_rate->dl_info.frame, amr_rate->dl_info.mode)
                                           + VOICE_PACKET_HDR_LENGTH;
  mmstd_memcpy(vocoder_packet,VOICE_PACKET_LENGTH,dest_voc_pkt,
               *vocoder_packet_size);
  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_dl_amr_wb_packet(): " \
        "complete (%d)",
        nResult );

  return nResult;
}

static uint32_t mvs_process_ul_amr_wb_packet (
  uint8 *vocoder_packet,
  mvs_amr_frame_info_type *amr_rate,
  uint8 **speechData,
  uint32 *vocoder_packet_size
)
{
  uint32_t nResult = APR_EOK;
  int mode;
  int frameType;

  MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_ul_amr_wb_packet()" );

  if ( vocoder_packet == NULL || amr_rate == NULL || speechData == NULL || vocoder_packet_size == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_ul_amr_wb_packet(): " \
          "Invalid parameters." );
    return APR_EBADPARAM;
  }

  //Parse the mode and frame type
  frameType = (int)( ((*vocoder_packet) & 0xF0) >> 4 );
  mode = (int)( (*vocoder_packet) & 0x0F );

  mode += (int)(MVS_AMR_MODE_0660);
  frameType = cvd_to_mvs_amrwb_frame_type[ frameType ];

  //Validate mode and type
  if ( mode < MVS_AMR_MODE_0660 || mode > MVS_AMR_MODE_2385 ||
       frameType < MVS_AMR_SPEECH_GOOD || frameType > MVS_AMR_SPEECH_LOST )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_ul_amr_wb_packet(): " \
            "Invalid packet header: mode (%d), type (%d)",
            mode, frameType );
    *vocoder_packet_size = 0;
    return APR_EBADPARAM;
  }

  amr_rate->ul_info.mode = (mvs_amr_mode_type)(mode);
  amr_rate->ul_info.frame = (mvs_amr_frame_type)(frameType);

  //Get pointer to the beginning of speech data
  *speechData = vocoder_packet + VOICE_PACKET_HDR_LENGTH;

  //Determine the packet length
  *vocoder_packet_size = mvs_amr_frame_len(amr_rate->dl_info.frame, amr_rate->dl_info.mode);

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_ul_amr_wb_packet(): " \
        "complete (%d)",
        nResult );

  return nResult;
}


static uint32_t mvs_process_dl_efr_packet (
  uint8 *vocoder_packet,
  mvs_gsm_frame_info_type *gsm_rate,
  uint32 *vocoder_packet_size
)
{
  uint32_t nResult = APR_EOK;
  int bfi;
  int sid;
  int taf;
  uint8 dest_voc_pkt[VOICE_PACKET_LENGTH];

  MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_dl_efr_packet()" );

  if ( vocoder_packet == NULL || gsm_rate == NULL || vocoder_packet_size == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_dl_efr_packet(): " \
          "Invalid parameters." );
    return APR_EBADPARAM;
  }

  taf = (int)(gsm_rate->dl_info.taf);

  switch ( gsm_rate->dl_info.frame )
  {
  case MVS_GSM_SID:
    sid = 2; //Valid SID frame
    bfi = 0;
    break;

  case MVS_GSM_SPEECH_GOOD:
    sid = 0; //speech frame
    bfi = 0;
    break;

  case MVS_GSM_BFI:
    sid = 0; //speech frame
    bfi = 1;
    break;

  default:
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_dl_efr_packet(): " \
            "Invalid frame: (%d)",
            gsm_rate->dl_info.frame );
    *vocoder_packet_size = 0;
    return APR_EBADPARAM;
  }

  //Populate packet header
  *dest_voc_pkt = ( (( 0  & 0x0F) << 4) | //BIT 7-4 : Reserved
                     ((taf & 0x01) << 3) | //BIT 3   : Time Allignment Flag (TAF)
                     ((sid & 0x03) << 1) | //BIT 2-1 : SID Frame Indication
                     ((bfi & 0x01)) );     //BIT 0   : Bad Frame Indicator (BFI)

  mmstd_memcpy(dest_voc_pkt + VOICE_PACKET_HDR_LENGTH,VOICE_PACKET_LENGTH 
               - VOICE_PACKET_HDR_LENGTH,vocoder_packet, VOICE_PACKET_LENGTH 
               - VOICE_PACKET_HDR_LENGTH);

  *vocoder_packet_size = EFR_MAX_PACKET_DATA_LENGTH + VOICE_PACKET_HDR_LENGTH;

  mmstd_memcpy(vocoder_packet,VOICE_PACKET_LENGTH,dest_voc_pkt, *vocoder_packet_size);
  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_dl_efr_packet(): " \
        "complete (%d)",
        nResult );

  return nResult;
}

static uint32_t mvs_process_ul_efr_packet (
  uint8 *vocoder_packet,
  mvs_gsm_frame_info_type *gsm_rate,
  uint8 **speechData,
  uint32 *vocoder_packet_size
)
{
  uint32_t nResult = APR_EOK;
  int sid;

  MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_ul_efr_packet()" );

  if ( vocoder_packet == NULL || gsm_rate == NULL || speechData == NULL || vocoder_packet_size == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_ul_efr_packet(): " \
          "Invalid parameters." );
    return APR_EBADPARAM;
  }

  //Parse the packet header
  sid = (int)( (*vocoder_packet & 0x06) >> 1 );

  gsm_rate->ul_info.rate = MVS_GSM_RATE_EFR;

  switch ( sid )
  {
  case 0: //speech frame
    gsm_rate->ul_info.frame = MVS_GSM_SPEECH_GOOD;
    break;

  case 1: //Invalid SID frame
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_ul_efr_packet(): " \
          "Received "
          "Invalid SID frame from the encoder. "
          "This should never happen" );
    *vocoder_packet_size = 0;
    return APR_EBADPARAM;

  case 2: //Valid SID frame
    gsm_rate->ul_info.frame = MVS_GSM_SID;
    break;

  default:
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvs_process_ul_efr_packet(): " \
           "Invalid packet header: sid (%d),",
           sid );
    *vocoder_packet_size = 0;
    return APR_EBADPARAM;
  }

  //Get pointer to the beginning of speech data
  *speechData = vocoder_packet + VOICE_PACKET_HDR_LENGTH;

  *vocoder_packet_size = EFR_MAX_PACKET_DATA_LENGTH;

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_ul_efr_packet(): " \
        "complete (%d)",
        nResult );

  return nResult;
}


static uint32_t mvs_process_dl_fr_packet (
  uint8 *vocoder_packet,
  mvs_gsm_frame_info_type *gsm_rate,
  uint32 *vocoder_packet_size
)
{
  uint32_t nResult = APR_EOK;
  int bfi;
  int sid;
  int taf;
  uint8 dest_voc_pkt[VOICE_PACKET_LENGTH];

  MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_dl_fr_packet()" );

  if ( vocoder_packet == NULL || gsm_rate == NULL || vocoder_packet_size == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_dl_fr_packet(): " \
          "Invalid parameters." );
    return APR_EBADPARAM;
  }

  taf = (int)(gsm_rate->dl_info.taf);

  switch ( gsm_rate->dl_info.frame )
  {
  case MVS_GSM_SID:
    sid = 2; //Valid SID frame
    bfi = 0;
    break;

  case MVS_GSM_SPEECH_GOOD:
    sid = 0; //speech frame
    bfi = 0;
    break;

  case MVS_GSM_BFI:
    sid = 0; //speech frame
    bfi = 1;
    break;

  default:
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_dl_fr_packet(): " \
            "Invalid frame: (%d)",
            gsm_rate->dl_info.frame );
    *vocoder_packet_size = 0;
    return APR_EBADPARAM;
  }

  //Populate packet header
  *dest_voc_pkt = ( (( 0  & 0x0F) << 4) | //BIT 7-4 : Reserved
                     ((taf & 0x01) << 3) | //BIT 3   : Time Allignment Flag (TAF)
                     ((sid & 0x03) << 1) | //BIT 2-1 : SID Frame Indication
                     ((bfi & 0x01)) );     //BIT 0   : Bad Frame Indicator (BFI)

  mmstd_memcpy(dest_voc_pkt + VOICE_PACKET_HDR_LENGTH,VOICE_PACKET_LENGTH 
               - VOICE_PACKET_HDR_LENGTH,vocoder_packet, VOICE_PACKET_LENGTH 
               - VOICE_PACKET_HDR_LENGTH);

  *vocoder_packet_size = FR_MAX_PACKET_DATA_LENGTH + VOICE_PACKET_HDR_LENGTH;

  mmstd_memcpy(vocoder_packet,VOICE_PACKET_LENGTH,dest_voc_pkt, *vocoder_packet_size);
  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_dl_fr_packet(): " \
        "complete (%d)",
        nResult );

  return nResult;
}


static uint32_t mvs_process_ul_fr_packet (
  uint8 *vocoder_packet,
  mvs_gsm_frame_info_type *gsm_rate,
  uint8 **speechData,
  uint32 *vocoder_packet_size
)
{
  uint32_t nResult = APR_EOK;

  MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_ul_fr_packet()" );

  //FR uplink packet processing is currently identical to EFR packet processing, except...
  nResult = mvs_process_ul_efr_packet(vocoder_packet, gsm_rate, speechData, vocoder_packet_size);

  //...for gsm_rate->ul_info.rate setting and packet length which we override here
  gsm_rate->ul_info.rate = MVS_GSM_RATE_FR;
  *vocoder_packet_size = FR_MAX_PACKET_DATA_LENGTH;

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_ul_fr_packet(): " \
        "complete (%d)",
        nResult );

  return nResult;
}


static uint32_t mvs_process_dl_hr_packet (
  uint8 *vocoder_packet,
  mvs_hr_frame_info_type *hr_rate,
  uint32 *vocoder_packet_size
)
{
  uint32_t nResult = APR_EOK;
  int bfi;
  int sid;
  int taf;
  int ufi;
  uint8 dest_voc_pkt[VOICE_PACKET_LENGTH];

  MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_dl_hr_packet()" );

  if ( vocoder_packet == NULL || hr_rate == NULL || vocoder_packet_size == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_dl_hr_packet(): " \
          "Invalid parameters." );
    return APR_EBADPARAM;
  }

  bfi = (int)(hr_rate->dl_info.bfi);
  taf = (int)(hr_rate->dl_info.taf);
  ufi = (int)(hr_rate->dl_info.ufi);

  switch ( hr_rate->dl_info.frame )
  {
  case MVS_HR_SID:
    sid = 2; //Valid SID frame
    break;

  case MVS_HR_INVALID_SID:
    sid = 1; //Invalid SID frame
    break;

  case MVS_HR_SPEECH_GOOD:
  case MVS_HR_SPEECH_BAD:
    sid = 0; //speech frame
    break;

  default:
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_dl_hr_packet(): " \
            "Invalid frame: (%d)",
            hr_rate->dl_info.frame );
    *vocoder_packet_size = 0;
    return APR_EBADPARAM;
  }

  //Populate packet header
  *dest_voc_pkt = ( (( 0  & 0x07) << 5) | //BIT 7-3 : Reserved
                     ((ufi & 0x01) << 4) | //BIT 4   : Unreliable Frame Indicator (UFI)
                     ((taf & 0x01) << 3) | //BIT 3   : Time Allignment Flag (TAF)
                     ((sid & 0x03) << 1) | //BIT 2-1 : SID Frame Indication
                     ((bfi & 0x01)) );     //BIT 0   : Bad Frame Indicator (BFI)

  //Make room for packet header
  mmstd_memcpy(dest_voc_pkt+VOICE_PACKET_HDR_LENGTH,VOICE_PACKET_LENGTH 
               - VOICE_PACKET_HDR_LENGTH ,vocoder_packet, VOICE_PACKET_LENGTH 
               - VOICE_PACKET_HDR_LENGTH);
  *vocoder_packet_size = HR_MAX_PACKET_DATA_LENGTH + VOICE_PACKET_HDR_LENGTH;
  mmstd_memcpy(vocoder_packet,VOICE_PACKET_LENGTH,dest_voc_pkt,
               *vocoder_packet_size );
  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_dl_hr_packet(): " \
        "complete (%d)",
        nResult );

  return nResult;
}


static uint32_t mvs_process_ul_hr_packet (
  uint8 *vocoder_packet,
  mvs_hr_frame_info_type *hr_rate,
  uint8 **speechData,
  uint32 *vocoder_packet_size
)
{
  uint32_t nResult = APR_EOK;
  int sid;

  MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_ul_hr_packet()" );

  if ( vocoder_packet == NULL || hr_rate == NULL || speechData == NULL || vocoder_packet_size == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_ul_hr_packet(): " \
          "Invalid parameters." );
    return APR_EBADPARAM;
  }

  //Parse the packet header
  sid = (int)( (*vocoder_packet & 0x06) >> 1 );

  hr_rate->ul_info.enc_err_flag = 0;

  switch ( sid )
  {
  case 0: //speech frame
    hr_rate->ul_info.frame = MVS_HR_SPEECH_GOOD;
    break;

  case 1: //Invalid SID frame
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_ul_hr_packet(): " \
          "Received Invalid SID frame from "
          "the encoder. This should never happen" );
    *vocoder_packet_size = 0;
    return APR_EBADPARAM;

  case 2: //Valid SID frame
    hr_rate->ul_info.frame = MVS_HR_SID;
    break;

  default:
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_ul_hr_packet(): " \
            "Invalid packet header: sid (%d),",
            sid );
    *vocoder_packet_size = 0;
    return APR_EBADPARAM;
  }

  //Get pointer to the beginning of speech data
  *speechData = vocoder_packet + VOICE_PACKET_HDR_LENGTH;

  *vocoder_packet_size = HR_MAX_PACKET_DATA_LENGTH;

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_ul_hr_packet(): " \
        "complete (%d)",
        nResult );

  return nResult;
}

static uint32_t mvs_process_dl_evrc_packet (
  uint8 *vocoder_packet,
  mvs_voc_frame_info_type *voc_rate,
  uint32 *vocoder_packet_size
)
{
  uint32_t nResult = APR_EOK;
  uint8  pkt_rate = 0;
  uint8 dest_voc_pkt[VOICE_PACKET_LENGTH];

  MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_dl_evrc_packet()" );

  if ( vocoder_packet == NULL || voc_rate == NULL || vocoder_packet_size == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_dl_evrc_packet(): " \
          "Invalid parameters." );
    return APR_EBADPARAM;
  }

  switch ( voc_rate->rx_info.rate )
  {
  case MVS_VOC_1_RATE:
    pkt_rate = 4;
    break;
  case MVS_VOC_2_RATE:
    pkt_rate = 3;
    break;
  case MVS_VOC_8_RATE:
    pkt_rate = 1;
    break;
  case MVS_VOC_0_RATE:
    pkt_rate = 0;
    break;
  case MVS_VOC_ERASURE:
    pkt_rate = 14;
    break;
  default:
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_dl_evrc_packet(): " \
            "Invalid voc_rate : (%d),",
            voc_rate->rx_info.rate );
    return APR_EBADPARAM;
  }

  //Populate packet header
  *dest_voc_pkt = ( 0x0F & pkt_rate );            //BIT (0-3): Rate
                                                   //BIT (4-7):Reserved - 0's
  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_dl_evrc_packet() " \
        "voc_rate->rx_info.rate (%d)",
        voc_rate->rx_info.rate );

  mmstd_memcpy(dest_voc_pkt+VOICE_PACKET_HDR_LENGTH,VOICE_PACKET_LENGTH 
    - VOICE_PACKET_HDR_LENGTH,vocoder_packet, EVRC_MAX_PACKET_DATA_LENGTH);
  *vocoder_packet_size = mvslegacy_evrc_packet_size[voc_rate->rx_info.rate]
                         + VOICE_PACKET_HDR_LENGTH;
  mmstd_memcpy(vocoder_packet,VOICE_PACKET_LENGTH,dest_voc_pkt, *vocoder_packet_size);
  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_dl_evrc_packet(): " \
        "complete (%d)",
        nResult );

  return nResult;
}

static uint32_t mvs_process_ul_evrc_packet (
  uint8 *vocoder_packet,
  mvs_voc_frame_info_type *voc_rate,
  uint8 **speechData,
  uint32 *vocoder_packet_size
)
{
  uint32_t nResult = APR_EOK;
  uint8  pkt_hdr = 0;

  MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_ul_evrc_packet()" );

  if ( vocoder_packet == NULL || voc_rate == NULL || speechData == NULL || vocoder_packet_size == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_ul_evrc_packet(): " \
          "Invalid parameters." );
    return APR_EBADPARAM;
  }

  pkt_hdr = *vocoder_packet;

  pkt_hdr = (pkt_hdr & 0x0F);

  switch ( pkt_hdr )
  {
  case 0:
    voc_rate->tx_info.rate = (mvs_voc_rate_type)MVS_VOC_0_RATE;
    break;
  case 1:
    voc_rate->tx_info.rate = (mvs_voc_rate_type)MVS_VOC_8_RATE;
    break;
  case 3:
    voc_rate->tx_info.rate = (mvs_voc_rate_type)MVS_VOC_2_RATE;
    break;
  case 4:
    voc_rate->tx_info.rate = (mvs_voc_rate_type)MVS_VOC_1_RATE;
    break;
  case 14:
    voc_rate->tx_info.rate = (mvs_voc_rate_type)MVS_VOC_ERASURE;
    break;
  default:
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_ul_evrc_packet(): " \
            "Invalid pkt_hdr : (%d),",
            pkt_hdr );
    return APR_EBADPARAM;
  }

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_ul_evrc_packet() " \
        "voc_rate->tx_info.rate (%d)",
        voc_rate->tx_info.rate );

  //Get pointer to the beginning of speech data
  *speechData = (uint8*)(vocoder_packet + VOICE_PACKET_HDR_LENGTH);

  *vocoder_packet_size = mvslegacy_evrc_packet_size[voc_rate->tx_info.rate];
  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_ul_evrc_packet(): " \
        "complete (%d)",
        nResult );

  return nResult;
}

static uint32_t mvs_process_dl_13k_packet (
  uint8 *vocoder_packet,
  mvs_voc_frame_info_type *voc_rate,
  uint32 *vocoder_packet_size
)
{
  uint32_t nResult = APR_EOK;
  uint8  pkt_rate = 0;
  uint8 dest_voc_pkt[VOICE_PACKET_LENGTH];

  MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_dl_13k_packet()" );

  if ( vocoder_packet == NULL || voc_rate == NULL || vocoder_packet_size == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_dl_13k_packet(): " \
          "Invalid parameters." );
    return APR_EBADPARAM;
  }

  switch ( voc_rate->rx_info.rate )
  {
  case MVS_VOC_1_RATE:
    pkt_rate = 4;
    break;
  case MVS_VOC_2_RATE:
    pkt_rate = 3;
    break;
  case MVS_VOC_8_RATE:
    pkt_rate = 1;
    break;
  case MVS_VOC_4_RATE:
    pkt_rate = 2;
    break;
  case MVS_VOC_0_RATE:
    pkt_rate = 0;
    break;
  case MVS_VOC_ERASURE:
    pkt_rate = 14;
    break;
  default:
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_dl_13k_packet(): " \
            "Invalid voc_rate : (%d),",
            voc_rate->rx_info.rate );
    return APR_EBADPARAM;
  }

  //Populate packet header
  *dest_voc_pkt = ( 0x0F & pkt_rate );      //BIT (0-3): Rate
  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_dl_13k_packet() " \
        "voc_rate->rx_info.rate (%d)",
        voc_rate->rx_info.rate );

  mmstd_memcpy(dest_voc_pkt + VOICE_PACKET_HDR_LENGTH,VOICE_PACKET_LENGTH 
    - VOICE_PACKET_HDR_LENGTH,vocoder_packet, IS733_MAX_PACKET_DATA_LENGTH);
  *vocoder_packet_size = mvslegacy_13k_packet_size[voc_rate->rx_info.rate]
                         + VOICE_PACKET_HDR_LENGTH;
  mmstd_memcpy(vocoder_packet,VOICE_PACKET_LENGTH,dest_voc_pkt,
               *vocoder_packet_size);
  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_dl_13k_packet(): " \
        "complete (%d)",
        nResult );

  return nResult;
}

static uint32_t mvs_process_ul_13k_packet (
  uint8 *vocoder_packet,
  mvs_voc_frame_info_type *voc_rate,
  uint8 **speechData,
  uint32 *vocoder_packet_size
)
{
  uint32_t nResult = APR_EOK;
  uint8  pkt_hdr = 0;

  MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_ul_13k_packet()" );

  if ( vocoder_packet == NULL || voc_rate == NULL || speechData == NULL || vocoder_packet_size == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_ul_13k_packet(): " \
          "Invalid parameters." );
    return APR_EBADPARAM;
  }

  pkt_hdr = *vocoder_packet;
  pkt_hdr = (pkt_hdr & 0x0F);

  switch ( pkt_hdr )
  {
  case 0 :
    voc_rate->tx_info.rate = (mvs_voc_rate_type)MVS_VOC_0_RATE;
    break;
  case 1:
    voc_rate->tx_info.rate = (mvs_voc_rate_type)MVS_VOC_8_RATE;
    break;
  case 2:
    voc_rate->tx_info.rate = (mvs_voc_rate_type)MVS_VOC_4_RATE;
    break;
  case 3:
    voc_rate->tx_info.rate =(mvs_voc_rate_type) MVS_VOC_2_RATE;
    break;
  case 4:
    voc_rate->tx_info.rate =(mvs_voc_rate_type) MVS_VOC_1_RATE;
    break;
  case 14:
    voc_rate->tx_info.rate = (mvs_voc_rate_type)MVS_VOC_ERASURE;
    break;
  default:
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_ul_13k_packet(): " \
            "Invalid pkt_hdr : (%d),",
            pkt_hdr );
    return APR_EBADPARAM;
  }
  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_ul_13k_packet() " \
        "voc_rate->tx_info.rate (%d)",
        voc_rate->tx_info.rate );

  //Get pointer to the beginning of speech data
  *speechData = (uint8*)(vocoder_packet + VOICE_PACKET_HDR_LENGTH);

  *vocoder_packet_size = mvslegacy_13k_packet_size[voc_rate->tx_info.rate];

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_ul_13k_packet(): " \
        "complete (%d)",
        nResult );

  return nResult;
}

static uint32_t mvs_process_dl_4gv_packet (
  uint8 *vocoder_packet,
  mvs_voc_frame_info_type *voc_rate,
  uint32 *vocoder_packet_size
)
{
  uint32_t nResult = APR_EOK;
  uint8  pkt_rate = 0;
  uint8 dest_voc_pkt[VOICE_PACKET_LENGTH];

  MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_dl_4gv_packet()" );

  if ( vocoder_packet == NULL || voc_rate == NULL || vocoder_packet_size == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_dl_4gv_packet(): " \
          "Invalid parameters." );
    return APR_EBADPARAM;
  }

  switch ( voc_rate->rx_info.rate )
  {
  case MVS_VOC_1_RATE:
    pkt_rate = 4;
    break;
  case MVS_VOC_2_RATE:
    pkt_rate = 3;
    break;
  case MVS_VOC_8_RATE:
    pkt_rate = 1;
    break;
  case MVS_VOC_4_RATE:
    pkt_rate = 2;
    break;
  case MVS_VOC_0_RATE:
    pkt_rate = 0;
    break;
  case MVS_VOC_ERASURE:
    pkt_rate = 14;
    break;
  default:
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_dl_4gv_packet(): " \
            "Invalid voc_rate : (%d),",
            voc_rate->rx_info.rate );
    return APR_EBADPARAM;
  }

  //Populate packet header
  *dest_voc_pkt = ( 0x0F & pkt_rate );      //BIT (0-3): Rate
  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_dl_4gv_packet() " \
        "voc_rate->rx_info.rate (%d)",
        voc_rate->rx_info.rate );

  mmstd_memcpy(dest_voc_pkt +VOICE_PACKET_HDR_LENGTH,VOICE_PACKET_LENGTH
               -VOICE_PACKET_HDR_LENGTH, vocoder_packet,
               FOURGV_MAX_PACKET_DATA_LENGTH);

  *vocoder_packet_size = mvslegacy_4gv_packet_size[voc_rate->rx_info.rate]
                         + VOICE_PACKET_HDR_LENGTH;
  mmstd_memcpy(vocoder_packet,VOICE_PACKET_LENGTH,dest_voc_pkt,
               *vocoder_packet_size);
  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_dl_4gv_packet(): " \
        "complete (%d)",
        nResult );

  return nResult;
}

static uint32_t mvs_process_ul_4gv_packet (
  uint8 *vocoder_packet,
  mvs_voc_frame_info_type *voc_rate,
  uint8 **speechData,
  uint32 *vocoder_packet_size
)
{
  uint32_t nResult = APR_EOK;
  uint8  pkt_hdr = 0;

  MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_ul_4gv_packet()" );

  if ( vocoder_packet == NULL || voc_rate == NULL || speechData == NULL || vocoder_packet_size == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_ul_4gv_packet(): " \
          "Invalid parameters." );
    return APR_EBADPARAM;
  }

  pkt_hdr = *vocoder_packet;
  pkt_hdr = (pkt_hdr & 0x0F);

  switch ( pkt_hdr )
  {
  case 0 :
    voc_rate->tx_info.rate = (mvs_voc_rate_type)MVS_VOC_0_RATE;
    break;
  case 1:
    voc_rate->tx_info.rate = (mvs_voc_rate_type)MVS_VOC_8_RATE;
    break;
  case 2:
    voc_rate->tx_info.rate = (mvs_voc_rate_type)MVS_VOC_4_RATE;
    break;
  case 3:
    voc_rate->tx_info.rate =(mvs_voc_rate_type) MVS_VOC_2_RATE;
    break;
  case 4:
    voc_rate->tx_info.rate =(mvs_voc_rate_type) MVS_VOC_1_RATE;
    break;
  case 14:
    voc_rate->tx_info.rate = (mvs_voc_rate_type)MVS_VOC_ERASURE;
    break;
  default:
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_ul_4gv_packet(): " \
            "Invalid pkt_hdr : (%d),",
            pkt_hdr );
    return APR_EBADPARAM;
  }
  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_ul_4gv_packet() " \
        "voc_rate->tx_info.rate (%d)",
        voc_rate->tx_info.rate );

  //Get pointer to the beginning of speech data
  *speechData = (uint8*)(vocoder_packet + VOICE_PACKET_HDR_LENGTH);

  *vocoder_packet_size = mvslegacy_4gv_packet_size[voc_rate->tx_info.rate];

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_ul_4gv_packet(): " \
        "complete (%d)",
        nResult );

  return nResult;
}

static uint32_t mvs_process_dl_4gv_wb_packet (
  uint8 *vocoder_packet,
  mvs_voc_frame_info_type *voc_rate,
  uint32 *vocoder_packet_size
)
{
  uint32_t nResult = APR_EOK;
  uint8  pkt_rate = 0;
  uint8 dest_voc_pkt[VOICE_PACKET_LENGTH];

  MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_dl_4gv_wb_packet()" );

  if ( vocoder_packet == NULL || voc_rate == NULL || vocoder_packet_size == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_dl_4gv_wb_packet(): " \
          "Invalid parameters." );
    return APR_EBADPARAM;
  }

  switch ( voc_rate->rx_info.rate )
  {
  case MVS_VOC_1_RATE:
    pkt_rate = 4;
    break;
  case MVS_VOC_2_RATE:
    pkt_rate = 3;
    break;
  case MVS_VOC_8_RATE:
    pkt_rate = 1;
    break;
  case MVS_VOC_4_RATE:
    pkt_rate = 2;
    break;
  case MVS_VOC_0_RATE:
    pkt_rate = 0;
    break;
  case MVS_VOC_ERASURE:
    pkt_rate = 14;
    break;
  default:
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_dl_4gv_wb_packet(): " \
            "Invalid voc_rate : (%d),",
            voc_rate->rx_info.rate );
    return APR_EBADPARAM;
  }

  //Populate packet header
  *dest_voc_pkt = ( 0x0F & pkt_rate );      //BIT (0-3): Rate

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_dl_4gv_wb_packet() " \
        "voc_rate->rx_info.rate (%d)",
        voc_rate->rx_info.rate );

  mmstd_memcpy(dest_voc_pkt + VOICE_PACKET_HDR_LENGTH,VOICE_PACKET_LENGTH 
               - VOICE_PACKET_HDR_LENGTH,vocoder_packet,
               FOURGV_MAX_PACKET_DATA_LENGTH);
  *vocoder_packet_size = mvslegacy_4gv_WB_packet_size[voc_rate->rx_info.rate]
                         + VOICE_PACKET_HDR_LENGTH;

  mmstd_memcpy(vocoder_packet,VOICE_PACKET_LENGTH,dest_voc_pkt, *vocoder_packet_size);
  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_dl_4gv_wb_packet(): " \
        "complete (%d)",
        nResult );

  return nResult;
}

static uint32_t mvs_process_ul_4gv_wb_packet (
  uint8 *vocoder_packet,
  mvs_voc_frame_info_type *voc_rate,
  uint8 **speechData,
  uint32 *vocoder_packet_size
)
{
  uint32_t nResult = APR_EOK;
  uint8  pkt_hdr = 0;

  MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_ul_4gv_wb_packet()" );

  if ( vocoder_packet == NULL || voc_rate == NULL || speechData == NULL || vocoder_packet_size == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_ul_4gv_wb_packet(): " \
          "Invalid parameters." );
    return APR_EBADPARAM;
  }

  pkt_hdr = *vocoder_packet;
  pkt_hdr = (pkt_hdr & 0x0F);

  switch ( pkt_hdr )
  {
  case 0 :
    voc_rate->tx_info.rate = (mvs_voc_rate_type)MVS_VOC_0_RATE;
    break;
  case 1:
    voc_rate->tx_info.rate = (mvs_voc_rate_type)MVS_VOC_8_RATE;
    break;
  case 2:
    voc_rate->tx_info.rate = (mvs_voc_rate_type)MVS_VOC_4_RATE;
    break;
  case 3:
    voc_rate->tx_info.rate =(mvs_voc_rate_type) MVS_VOC_2_RATE;
    break;
  case 4:
    voc_rate->tx_info.rate =(mvs_voc_rate_type) MVS_VOC_1_RATE;
    break;
  case 14:
    voc_rate->tx_info.rate = (mvs_voc_rate_type)MVS_VOC_ERASURE;
    break;
  default:
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_ul_4gv_wb_packet(): " \
            "Invalid pkt_hdr : (%d),",
            pkt_hdr );
    return APR_EBADPARAM;
  }
  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_ul_4gv_wb_packet() " \
        "voc_rate->tx_info.rate (%d)",
        voc_rate->tx_info.rate );

  //Get pointer to the beginning of speech data
  *speechData = (uint8*)(vocoder_packet + VOICE_PACKET_HDR_LENGTH);

  *vocoder_packet_size = mvslegacy_4gv_WB_packet_size[voc_rate->tx_info.rate];

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_ul_4gv_wb_packet(): " \
        "complete (%d)",
        nResult );

  return nResult;
}

static uint32_t mvs_process_dl_4gv_nw_packet (
  uint8 *vocoder_packet,
  mvs_voc_frame_info_type *voc_rate,
  uint32 *vocoder_packet_size
)
{
  uint32_t nResult = APR_EOK;
  uint8  pkt_rate = 0;
  uint8 dest_voc_pkt[VOICE_PACKET_LENGTH];

  MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_dl_4gv_nw_packet()" );

  if ( vocoder_packet == NULL || voc_rate == NULL || vocoder_packet_size == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_dl_4gv_nw_packet(): " \
          "Invalid parameters." );
    return APR_EBADPARAM;
  }

  switch ( voc_rate->rx_info.rate )
  {
  case MVS_VOC_1_RATE:
    pkt_rate = 4;
    break;
  case MVS_VOC_2_RATE:
    pkt_rate = 3;
    break;
  case MVS_VOC_8_RATE:
    pkt_rate = 1;
    break;
  case MVS_VOC_4_RATE:
    pkt_rate = 2;
    break;
  case MVS_VOC_0_RATE:
    pkt_rate = 0;
    break;
  case MVS_VOC_ERASURE:
    pkt_rate = 14;
    break;
  default:
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_dl_4gv_nw_packet(): " \
            "Invalid voc_rate : (%d),",
            voc_rate->rx_info.rate );
    return APR_EBADPARAM;
  }

  //Populate packet header
  *dest_voc_pkt = ( 0x0F & pkt_rate );      //BIT (0-3): Rate
  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_dl_4gv_nw_packet() " \
        "voc_rate->rx_info.rate (%d)",
        voc_rate->rx_info.rate );
  mmstd_memcpy(dest_voc_pkt + VOICE_PACKET_HDR_LENGTH,VOICE_PACKET_LENGTH 
               - VOICE_PACKET_HDR_LENGTH,vocoder_packet,
               FOURGV_MAX_PACKET_DATA_LENGTH);

  *vocoder_packet_size = mvslegacy_4gv_NW_packet_size[voc_rate->rx_info.rate]
                         + VOICE_PACKET_HDR_LENGTH;
  mmstd_memcpy(vocoder_packet,VOICE_PACKET_LENGTH,dest_voc_pkt, 
               *vocoder_packet_size);
  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_dl_4gv_nw_packet(): " \
        "complete (%d)",
        nResult );

  return nResult;
}

static uint32_t mvs_process_ul_4gv_nw_packet (
  uint8 *vocoder_packet,
  mvs_voc_frame_info_type *voc_rate,
  uint8 **speechData,
  uint32 *vocoder_packet_size
)
{
  uint32_t nResult = APR_EOK;
  uint8  pkt_hdr = 0;

  MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_ul_4gv_nw_packet()" );

  if ( vocoder_packet == NULL || voc_rate == NULL || speechData == NULL || vocoder_packet_size == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_ul_4gv_nw_packet(): " \
          "Invalid parameters." );
    return APR_EBADPARAM;
  }

  pkt_hdr = *vocoder_packet;
  pkt_hdr = (pkt_hdr & 0xFF);

  switch ( pkt_hdr )
  {
  case 0 :
    voc_rate->tx_info.rate = (mvs_voc_rate_type)MVS_VOC_0_RATE;
    break;
  case 1:
    voc_rate->tx_info.rate = (mvs_voc_rate_type)MVS_VOC_8_RATE;
    break;
  case 2:
    voc_rate->tx_info.rate = (mvs_voc_rate_type)MVS_VOC_4_RATE;
    break;
  case 3:
    voc_rate->tx_info.rate =(mvs_voc_rate_type) MVS_VOC_2_RATE;
    break;
  case 4:
    voc_rate->tx_info.rate =(mvs_voc_rate_type) MVS_VOC_1_RATE;
    break;
  case 14:
    voc_rate->tx_info.rate = (mvs_voc_rate_type)MVS_VOC_ERASURE;
    break;
  case 255:
    voc_rate->tx_info.rate = (mvs_voc_rate_type)MVS_VOC_8_RATE_NONCRITICAL;
    break;
  default:
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_ul_4gv_nw_packet(): " \
            "Invalid pkt_hdr : (%d),",
            pkt_hdr );
    return APR_EBADPARAM;
  }
  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_ul_4gv_nw_packet() " \
        "voc_rate->tx_info.rate (%d)",
        voc_rate->tx_info.rate );

  //Get pointer to the beginning of speech data
  *speechData = (uint8*)(vocoder_packet + VOICE_PACKET_HDR_LENGTH);

  *vocoder_packet_size = mvslegacy_4gv_NW_packet_size[voc_rate->tx_info.rate];

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_ul_4gv_nw_packet(): " \
        "complete (%d)",
        nResult );

  return nResult;
}

static uint32_t mvs_process_dl_linear_pcm_packet (
  uint8 *vocoder_packet,
  uint32 *vocoder_packet_size
)
{
  uint32_t nResult = APR_EOK;
  uint8 dest_voc_pkt[VOICE_PACKET_LENGTH];

  MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_dl_linear_pcm_packet()" );

  if ( vocoder_packet == NULL || vocoder_packet_size == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
          "mvs_process_dl_linear_pcm_packet(): Invalid parameters." );
    return APR_EBADPARAM;
  }

  //Make room for packet header
  mmstd_memcpy(dest_voc_pkt ,VOICE_PACKET_LENGTH,vocoder_packet,
               LINEAR_PCM_MAX_PACKET_DATA_LENGTH);

  *vocoder_packet_size = LINEAR_PCM_MAX_PACKET_DATA_LENGTH;

  mmstd_memcpy(vocoder_packet + VOICE_PACKET_HDR_LENGTH, VOICE_PACKET_LENGTH 
               -VOICE_PACKET_HDR_LENGTH, dest_voc_pkt, *vocoder_packet_size);


  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_dl_linear_pcm_packet(): " \
        "complete (%d)",
        nResult );

  return nResult;

}

static uint32_t mvs_process_ul_linear_pcm_packet (
  uint8 *vocoder_packet,
  uint8 **speechData,
  uint32 *vocoder_packet_size
)
{
  uint32_t nResult = APR_EOK;

  MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_ul_linear_pcm_packet()" );

  if ( vocoder_packet == NULL || speechData == NULL || vocoder_packet_size == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
          "mvs_process_ul_linear_pcm_packet(): Invalid parameters." );
    return APR_EBADPARAM;
  }

  //Get pointer to the beginning of speech data
  *speechData = (uint8*)(vocoder_packet);

  *vocoder_packet_size = LINEAR_PCM_MAX_PACKET_DATA_LENGTH;

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_ul_linear_pcm_packet(): " \
        "complete (%d)",
        nResult );

  return nResult;
}

static uint32_t mvs_process_dl_g711_packet (
  uint8 *vocoder_packet,
  mvs_g711_frame_info_type *g711_mode,
  uint32 *vocoder_packet_size
)
{
  uint32_t nResult = APR_EOK;
  int mode;
  int frame2Loc;
  uint8 dest_voc_pkt[VOICE_PACKET_LENGTH];

  MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_dl_g711_packet()" );

  if ( vocoder_packet == NULL || vocoder_packet_size == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_dl_g711_packet(): " \
          "Invalid parameters." );
    return APR_EBADPARAM;
  }

  mode = (int)g711_mode->dl_info.g711_mode;
  //validate mode type
  if ( mode > MVS_G711_MODE_ALAW || mode < MVS_G711_MODE_MULAW )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvs_process_dl_g711_packet(): " \
           "Invalid frame_info: mode (%d)",
           mode );
    *vocoder_packet_size = 0;
    return APR_EBADPARAM;
  }


  //Populate the packet header
  *dest_voc_pkt =  (mode << 2 & 0x0F);

  mmstd_memcpy(dest_voc_pkt  + VOICE_PACKET_HDR_LENGTH,VOICE_PACKET_LENGTH 
               -VOICE_PACKET_HDR_LENGTH,vocoder_packet, VOICE_PACKET_LENGTH - VOICE_PACKET_HDR_LENGTH);
  //Make room for Second packet header
  frame2Loc = VOICE_PACKET_HDR_LENGTH + G711_MAX_PACKET_DATA_LENGTH ;

  mmstd_memcpy(dest_voc_pkt + frame2Loc + VOICE_PACKET_HDR_LENGTH,
               VOICE_PACKET_LENGTH - frame2Loc- VOICE_PACKET_HDR_LENGTH,
               &vocoder_packet[G711_MAX_SG_DATA_LENGTH + VOICE_PACKET_HDR_LENGTH],
               G711_MAX_SG_DATA_LENGTH);

  //Populate the second frame header
  *(dest_voc_pkt + frame2Loc) = (mode << 2 & 0x0F);

  *vocoder_packet_size = 2*G711_MAX_PACKET_DATA_LENGTH  + 2*VOICE_PACKET_HDR_LENGTH;

  mmstd_memcpy(vocoder_packet ,VOICE_PACKET_LENGTH,dest_voc_pkt,*vocoder_packet_size);
  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_dl_g711_packet(): " \
        "complete (%d)",
        nResult );

  return nResult;
}

static uint32_t mvs_process_ul_g711_packet (
  uint8 *vocoder_packet,
  mvs_g711_frame_info_type *g711_mode,
  uint8 **speechData,
  uint32 *vocoder_packet_size
)
{
  uint32_t nResult = APR_EOK;
  uint8 dest_voc_pkt[VOICE_PACKET_LENGTH];
  int mode;
  int frame2Loc;

  MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_ul_g711_packet()" );

  if ( vocoder_packet == NULL || speechData == NULL || vocoder_packet_size == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_ul_g711_packet(): " \
          "Invalid parameters." );
    return APR_EBADPARAM;
  }

  //Parse the mode
  mode = (int)(( (*vocoder_packet) & 0x0F) >> 2 );
  if ( mode > MVS_G711_MODE_ALAW || mode < MVS_G711_MODE_MULAW )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvs_process_ul_g711_packet(): " \
           "Invalid frame_info: mode (%d)",
           mode);
    *vocoder_packet_size = 0;
    return APR_EBADPARAM;
  }

  g711_mode->ul_info.g711_mode = (mvs_g711_mode_type)mode;

  //Get pointer to the beginning of speech data
  *speechData = (vocoder_packet + VOICE_PACKET_HDR_LENGTH);

  //remove second header
  frame2Loc = VOICE_PACKET_HDR_LENGTH + G711_MAX_PACKET_DATA_LENGTH ;

  mmstd_memcpy(dest_voc_pkt,VOICE_PACKET_LENGTH,
               &vocoder_packet[frame2Loc + VOICE_PACKET_HDR_LENGTH] ,
               G711_MAX_SG_DATA_LENGTH);

  *vocoder_packet_size = G711_MAX_PACKET_DATA_LENGTH;

  mmstd_memcpy(vocoder_packet,VOICE_PACKET_LENGTH,dest_voc_pkt,*vocoder_packet_size);

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_ul_g711_packet(): " \
        "complete (%d)",
        nResult );

  return nResult;
}

static uint32_t mvs_process_dl_g711a_packet (
  uint8 *vocoder_packet,
  mvs_g711a_frame_info_type *g711a_mode,
  uint32 *vocoder_packet_size
)
{
  uint32_t nResult = APR_EOK;
  int mode;
  int mode2;
  int frameType1;
  int frameType2;
  int frame2Loc;
  uint8 pkt_buf[VOICE_PACKET_LENGTH];
  uint8 dest_voc_pkt[VOICE_PACKET_LENGTH];
  mvs_frame_info_type mvs_frame_info;
  mvs_pkt_status_type mvs_pkt_status;


  MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_dl_g711a_packet()" );

  if ( vocoder_packet == NULL || vocoder_packet_size == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_dl_g711a_packet(): " \
          "Invalid parameters." );
    return APR_EBADPARAM;
  }

  mode = (int)g711a_mode->dl_info.g711a_mode;
  frameType1 = (int)g711a_mode->dl_info.frame;
  //validate mode type
  if ( mode > MVS_G711A_MODE_ALAW || mode < MVS_G711A_MODE_MULAW ||
       frameType1 > MVS_G711A_NO_DATA || frameType1 < MVS_G711A_SPEECH_GOOD )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_dl_g711a_packet(): " \
            "Invalid frame_info: mode (%d)  frameType (%d)",
            mode, frameType1 );
    *vocoder_packet_size = 0;
    return APR_EBADPARAM;
  }

  //Populate the packet header
  *dest_voc_pkt = ((mode << 2 & 0x0F) | (frameType1 & 0x03));
  
  //Make room for packet header
  mmstd_memcpy( dest_voc_pkt + VOICE_PACKET_HDR_LENGTH,VOICE_PACKET_LENGTH
                - VOICE_PACKET_HDR_LENGTH, vocoder_packet, VOICE_PACKET_LENGTH
                - VOICE_PACKET_HDR_LENGTH);

  //get second packet

  //Obtain downlink vocoder packet from modem via callback function provided by the client
  if ( mvs_client_table[mvs_cur_client].dl_cb )
  {
    mvs_client_table[mvs_cur_client].dl_cb((uint8*)pkt_buf, &mvs_frame_info, &mvs_pkt_status);

    //get location of second packet
    frame2Loc = G711_MAX_PACKET_DATA_LENGTH  + VOICE_PACKET_HDR_LENGTH;

    //copy second packet into correct location
    mmstd_memcpy(dest_voc_pkt + frame2Loc + VOICE_PACKET_HDR_LENGTH,
                 VOICE_PACKET_LENGTH - frame2Loc- VOICE_PACKET_HDR_LENGTH,
                 (void*)(pkt_buf), G711_MAX_SG_DATA_LENGTH);
  }
  else
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_get_voc_packet(): DL callback " \
          "function is not set" );
    *vocoder_packet_size = 0;
    return APR_EFAILED;
  }

  //insert second packets header
  mode2 = (int)mvs_frame_info.g711a_rate.dl_info.g711a_mode;
  frameType2 = (int)mvs_frame_info.g711a_rate.dl_info.frame;

  //if second frame packet is slow insert erasure frame
  if ( mvs_pkt_status == MVS_PKT_STATUS_SLOW )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_get_voc_packet(): Modem did " \
          "not give second frame. "
          "Sending an erasure as second frame to DSP" );
    frameType2 = (int)MVS_G711A_ERASURE;
  }

  //validate mode type
  if ( mode2 > MVS_G711A_MODE_ALAW || mode2 < MVS_G711A_MODE_MULAW ||
       frameType2 > MVS_G711A_ERASURE || frameType2 < MVS_G711A_SPEECH_GOOD )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_dl_g711a_packet(): " \
            "Invalid frame_info: mode (%d)  frameType (%d)",
            mode2, frameType2 );
    *vocoder_packet_size = 0;
    return APR_EBADPARAM;
  }

  *(dest_voc_pkt +frame2Loc) = ((mode2 << 2 & 0x0F) | (frameType2 & 0x03));

  //determine packet length
  //length of second frame
  *vocoder_packet_size = G711_MAX_PACKET_DATA_LENGTH *2 +  VOICE_PACKET_HDR_LENGTH*2;
  mmstd_memcpy(vocoder_packet ,VOICE_PACKET_LENGTH,dest_voc_pkt,*vocoder_packet_size);

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_dl_g711a_packet(): " \
        "complete (%d)",
        nResult );

  return nResult;

}
static uint32_t mvs_process_ul_g711a_packet (
  uint8 *vocoder_packet,
  mvs_g711a_frame_info_type *g711a_mode,
  uint8 **speechData,
  uint32 *vocoder_packet_size
)
{
  uint32_t nResult = APR_EOK;
  int mode;
  int mode2;
  int frameType;
  int frameType2;
  int frame2Loc;
  uint8 g711a_packet[G711_MAX_SG_DATA_LENGTH];
  mvs_pkt_status_type mvs_pkt_status;
  mvs_frame_info_type info_type;

  MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_dl_g711a_packet()" );

  if ( vocoder_packet == NULL || speechData == NULL || vocoder_packet_size == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_dl_g711a_packet(): " \
          "Invalid parameters." );
    return APR_EBADPARAM;
  }

  //Parse the mode
  mode = (int)(( (*vocoder_packet) & 0x0F) >> 2 );
  frameType = (int)((*vocoder_packet) & 0x03) ;
  if ( mode > MVS_G711A_MODE_ALAW || mode < MVS_G711A_MODE_MULAW ||
       frameType > MVS_G711A_NO_DATA || frameType < MVS_G711A_SPEECH_GOOD )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_dl_g711a_packet(): " \
            "Invalid frame_info: mode (%d) frameType (%d)",
            mode, frameType );
    *vocoder_packet_size = 0;
    return APR_EBADPARAM;
  }


  info_type.g711a_rate.ul_info.g711a_mode = (mvs_g711a_mode_type)mode;
  info_type.g711a_rate.ul_info.frame = (mvs_g711a_frame_type)frameType;

  //send first frame
  //Get pointer to the beginning of speech data
  *speechData = (vocoder_packet + VOICE_PACKET_HDR_LENGTH);
  memset(g711a_packet, 0, G711_MAX_SG_DATA_LENGTH);
  mmstd_memcpy((uint8 *)&g711a_packet[0], G711_MAX_SG_DATA_LENGTH,
               *speechData, G711_MAX_SG_DATA_LENGTH);
  info_type.hdr.mode = mvs_client_table[mvs_cur_client].config_mode;
  //Give vocoder packet to modem via callback funtion provided by the client
  if ( mvs_client_table[mvs_cur_client].ul_cb )
  {
    mvs_client_table[mvs_cur_client].ul_cb(g711a_packet, &info_type, G711_MAX_SG_DATA_LENGTH , &mvs_pkt_status);
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_deliver_voc_packet(): Packet " \
          "status: (%d)",
          mvs_pkt_status );
  }
  else
  {
    return APR_EFAILED;
  }

  //get second frame
  frame2Loc = G711_MAX_PACKET_DATA_LENGTH  + VOICE_PACKET_HDR_LENGTH;
  //Parse the mode and Frame
  mode2 = (int)((vocoder_packet[frame2Loc] & 0x0F) >> 2 );
  frameType2 = (int)(vocoder_packet[frame2Loc] & 0x03) ;
  if ( mode2 > MVS_G711A_MODE_ALAW || mode2 < MVS_G711A_MODE_MULAW ||
       frameType2 > MVS_G711A_NO_DATA || frameType2 < MVS_G711A_SPEECH_GOOD )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_dl_g711a_packet(): " \
            "Invalid frame_info: mode (%d) frameType (%d)",
            mode2, frameType2 );
    *vocoder_packet_size = 0;
    return APR_EBADPARAM;
  }

  g711a_mode->ul_info.g711a_mode = (mvs_g711a_mode_type)mode2;
  g711a_mode->ul_info.frame = (mvs_g711a_frame_type)frameType2;


  *speechData = (vocoder_packet + VOICE_PACKET_HDR_LENGTH + frame2Loc);

  *vocoder_packet_size = G711_MAX_SG_DATA_LENGTH;

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_dl_g711a_packet(): " \
        "complete (%d)",
        nResult );

  return nResult;
}

static uint32_t mvs_process_dl_g729a_packet (
  uint8 *vocoder_packet,
  mvs_g729a_frame_info_type *g729a_mode,
  uint32 *vocoder_packet_size
)
{
  uint32_t nResult = APR_EOK;
  int frameType1;
  int frameType2;
  int frame2Loc;
  uint8 pkt_buf[VOICE_PACKET_LENGTH];
  uint8 dest_voc_pkt[VOICE_PACKET_LENGTH];
  mvs_frame_info_type mvs_frame_info;
  mvs_pkt_status_type mvs_pkt_status;


  MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_dl_g729a_packet()" );

  if ( vocoder_packet == NULL || vocoder_packet_size == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_dl_g729a_packet(): " \
          "Invalid parameters." );
    return APR_EBADPARAM;
  }

  frameType1 = (int)g729a_mode->dl_info.frame;
  //validate mode type
  if ( frameType1 >  MVS_G729A_SID  || frameType1 < MVS_G729A_NO_DATA )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvs_process_dl_g729a_packet(): " \
           "Invalid frame_info: frameType (%d)",
           frameType1 );
    *vocoder_packet_size = 0;
    return APR_EBADPARAM;
  }

  //Populate the packet header
  *dest_voc_pkt = (frameType1 & 0x03);

  mmstd_memcpy(dest_voc_pkt + VOICE_PACKET_HDR_LENGTH,VOICE_PACKET_LENGTH
               - VOICE_PACKET_HDR_LENGTH, vocoder_packet, VOICE_PACKET_LENGTH - VOICE_PACKET_HDR_LENGTH);

  //get second packet

  //Obtain downlink vocoder packet from modem via callback function provided by the client
  if ( mvs_client_table[mvs_cur_client].dl_cb )
  {
    mvs_client_table[mvs_cur_client].dl_cb((uint8*)pkt_buf, &mvs_frame_info, &mvs_pkt_status);

    //get location of second packet
    frame2Loc = G729A_MAX_FRAME_DATA_LENGTH  + VOICE_PACKET_HDR_LENGTH;

    //copy second packet into correct location
    mmstd_memcpy(dest_voc_pkt + frame2Loc + VOICE_PACKET_HDR_LENGTH,
                 VOICE_PACKET_LENGTH - frame2Loc- VOICE_PACKET_HDR_LENGTH,
            (void*)(pkt_buf), G729A_MAX_FRAME_DATA_LENGTH );
  
  }
  else
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_dl_g729a_packet(): DL " \
          "callback function is not set" );
    *vocoder_packet_size = 0;
    return APR_EFAILED;
  }

  //insert second packets header
  frameType2 = (int)mvs_frame_info.g729a_rate.dl_info.frame;

  //if second frame packet is slow insert erasure frame
  if ( mvs_pkt_status == MVS_PKT_STATUS_SLOW )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_get_voc_packet(): Modem did " \
          "not give second frame. "
          "Sending an erasure as second frame to DSP" );
    frameType2 = (int)MVS_G729A_ERASURE;
  }

  //validate mode type
  if ( frameType2 > MVS_G729A_ERASURE || frameType2 < MVS_G729A_NO_DATA )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_dl_g729a_packet: " \
            "Invalid frame_info: frameType (%d)",
            frameType2 );
    *vocoder_packet_size = 0;
    return APR_EBADPARAM;
  }

  *(dest_voc_pkt + frame2Loc) = (frameType2 & 0x03);

  //determine packet length
  //length of second frame
  *vocoder_packet_size = G729A_MAX_PACKET_DATA_LENGTH + VOICE_PACKET_HDR_LENGTH*2;
  mmstd_memcpy(vocoder_packet ,VOICE_PACKET_LENGTH,dest_voc_pkt,
               *vocoder_packet_size);

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_dl_g729a_packet(): " \
        "complete (%d)",
        nResult );

  return nResult;

}

static uint32_t mvs_process_ul_g729a_packet (
  uint8 *vocoder_packet,
  mvs_g729a_frame_info_type *g729a_mode,
  uint8 **speechData,
  uint32 *vocoder_packet_size)
{
  uint32_t nResult = APR_EOK;
  int frameType;
  int frameType2;
  int frame2Loc;
  uint8 g729a_packet[G729A_MAX_FRAME_DATA_LENGTH];
  mvs_pkt_status_type mvs_pkt_status;
  mvs_frame_info_type info_type;

  MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_ul_g729a_packet()" );

  if ( vocoder_packet == NULL || speechData == NULL || vocoder_packet_size == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_ul_g729a_packet(): " \
          "Invalid parameters." );
    return APR_EBADPARAM;
  }

  //Parse the frameType
  frameType = (int)((*vocoder_packet) & 0x03) ;
  if ( frameType > MVS_G729A_SID || frameType < MVS_G729A_NO_DATA )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_ul_g729a_packet(): " \
            "Invalid frame_info: frameType (%d)",
            frameType );
    *vocoder_packet_size = 0;
    return APR_EBADPARAM;
  }

  info_type.g729a_rate.ul_info.frame = (mvs_g729a_frame_type)frameType;

  //send first frame
  //Get pointer to the beginning of speech data
  *speechData = (vocoder_packet + VOICE_PACKET_HDR_LENGTH);
  memset(g729a_packet, 0, G729A_MAX_FRAME_DATA_LENGTH);
  mmstd_memcpy((uint8 *)&g729a_packet[0], G729A_MAX_FRAME_DATA_LENGTH,
               *speechData, G729A_MAX_FRAME_DATA_LENGTH);
  info_type.hdr.mode = mvs_client_table[mvs_cur_client].config_mode;
  //Give vocoder packet to modem via callback funtion provided by the client
  if ( mvs_client_table[mvs_cur_client].ul_cb )
  {
    mvs_client_table[mvs_cur_client].ul_cb(g729a_packet, &info_type, G729A_MAX_FRAME_DATA_LENGTH , &mvs_pkt_status);
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_deliver_voc_packet(): Packet " \
          "status: (%d)",
          mvs_pkt_status );
  }
  else
  {
    return APR_EFAILED;
  }

  //get second frame
  frame2Loc = G729A_MAX_FRAME_DATA_LENGTH + VOICE_PACKET_HDR_LENGTH;
  //Parse the FrameType
  frameType2 = (int)(vocoder_packet[frame2Loc] & 0x03) ;
  if ( frameType2 > MVS_G729A_SID || frameType2 < MVS_G729A_NO_DATA )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_process_ul_g729a_packet(): " \
            "Invalid frame_info:  frameType (%d)",
            frameType2 );
    *vocoder_packet_size = 0;
    return APR_EBADPARAM;
  }

  g729a_mode->ul_info.frame = (mvs_g729a_frame_type)frameType2;


  *speechData = (vocoder_packet + VOICE_PACKET_HDR_LENGTH + frame2Loc);

  *vocoder_packet_size = G729A_MAX_FRAME_DATA_LENGTH ;

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_process_ul_g729a_packet(): " \
        "complete (%d)",
        nResult );

  return nResult;
}

static uint32_t mvs_process_dl_g722_packet (
  uint8 *vocoder_packet,
  mvs_g722_frame_info_type *g722_mode,
  uint32 *vocoder_packet_size)
{


  uint32_t nResult = APR_EOK;
  int mode;
  uint8 dest_voc_pkt[VOICE_PACKET_LENGTH];

  MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,   \
      "mvs_packet_xchg::mvs_process_dl_g722_packet()" );

  if ( vocoder_packet == NULL || vocoder_packet_size == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
          "mvs_packet_xchg::mvs_process_dl_g722_packet(): Invalid " \
          "parameters." );
    return APR_EBADPARAM;
  }

   mmstd_memcpy(dest_voc_pkt + VOICE_PACKET_HDR_LENGTH,VOICE_PACKET_LENGTH
                - VOICE_PACKET_HDR_LENGTH, vocoder_packet, VOICE_PACKET_LENGTH
                - VOICE_PACKET_HDR_LENGTH);
  mode = (int)(g722_mode->dl_info.g722_mode);
  //Validate mode and type
  if ( mode < MVS_G722_MODE_01 || mode > MVS_G722_MODE_03 )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
            "mvs_packet_xchg::mvs_process_dl_g722_packet(): Invalid " \
            "frame_info: mode (%d),",
            mode );
    *vocoder_packet_size = 0;
    return APR_EBADPARAM;
  }

  //Populate the packet header
  *dest_voc_pkt = (mode & 0x0F);

  //Determine length of newly formed packet
  *vocoder_packet_size =  G722_MAX_PACKET_DATA_LENGTH + VOICE_PACKET_HDR_LENGTH;

  mmstd_memcpy(vocoder_packet ,VOICE_PACKET_LENGTH,dest_voc_pkt,*vocoder_packet_size);
  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_LOW,   \
        "mvs_packet_xchg::mvs_process_dl_g722_packet(): complete (%d)  mode = " \
        "(%d)",
        nResult, mode );

  return nResult;
}


static uint32_t mvs_process_ul_g722_packet (
  uint8 *vocoder_packet,
  mvs_g722_frame_info_type *g722_mode,
  uint8 **speechData, uint32 *vocoder_packet_size)
{
  uint32_t nResult = APR_EOK;
  int mode;

  MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,   \
      "mvs_packet_xchg::mvs_process_ul_g722_packet()" );

  if ( vocoder_packet == NULL || speechData == NULL || vocoder_packet_size == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
          "mvs_packet_xchg::mvs_process_ul_g722_packet(): Invalid " \
          "parameters." );
    return APR_EBADPARAM;
  }

  //Parse the mode
  mode = (int)( (*vocoder_packet) & 0x0F );

  //Validate mode and type
  if ( mode < MVS_G722_MODE_01|| mode > MVS_G722_MODE_03 )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
            "mvs_packet_xchg::mvs_process_ul_g722_packet(): Invalid packet " \
            "header: mode (%d)",
            mode );
    *vocoder_packet_size = 0;
    return APR_EBADPARAM;
  }


  *speechData = (vocoder_packet + VOICE_PACKET_HDR_LENGTH);

  *vocoder_packet_size = G722_MAX_PACKET_DATA_LENGTH ;

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,   \
        "mvs_packet_xchg::mvs_process_ul_g722_packet(): complete (%d)",
        nResult );

  return nResult;

}

static int32_t mvs_deliver_voc_packet (
  void *vocoder_packet,
  uint32 vocoder_packet_size
)
{
  mvs_frame_info_type mvs_frame_info;
  mvs_pkt_status_type mvs_pkt_status;
  uint32 newPacketLength = 0;
  uint8* speechData = NULL;
  uint32_t nRes;
  int32_t logresult = APR_EOK;
#ifdef USE_MVS_PACKET_TIMING_ANALYSIS
  timetick_type timetick;
#endif /* USE_MVS_PACKET_TIMING_ANALYSIS */

  /* Verify parameters. */
  if ( vocoder_packet == NULL || vocoder_packet_size == 0 )
  {
    return APR_EBADPARAM;
  }

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_deliver_voc_packet(): packet "
         "length (%d)", vocoder_packet_size );

  //Convert packet to format expected by modem (strip off the header)
  switch ( mvs_client_table[mvs_cur_client].config_mode )
  {
  case MVS_MODE_AMR:
    nRes = mvs_process_ul_amr_packet((uint8*)(vocoder_packet), &(mvs_frame_info.amr_rate), &speechData, &newPacketLength);
    mvs_frame_info.hdr.frame_mode = MVS_FRAME_MODE_AMR_UL;
    break;

  case MVS_MODE_AMR_WB:
    nRes = mvs_process_ul_amr_wb_packet((uint8*)(vocoder_packet), &(mvs_frame_info.amr_rate), &speechData, &newPacketLength);
    mvs_frame_info.hdr.frame_mode = MVS_FRAME_MODE_AMR_UL;
    break;

  case MVS_MODE_EFR:
    nRes = mvs_process_ul_efr_packet((uint8*)(vocoder_packet), &(mvs_frame_info.gsm_rate), &speechData, &newPacketLength);
    mvs_frame_info.hdr.frame_mode = MVS_FRAME_MODE_GSM_UL;
    break;

  case MVS_MODE_FR:
    nRes = mvs_process_ul_fr_packet((uint8*)(vocoder_packet), &(mvs_frame_info.gsm_rate), &speechData, &newPacketLength);
    mvs_frame_info.hdr.frame_mode = MVS_FRAME_MODE_GSM_UL;
    break;

  case MVS_MODE_HR:
    nRes = mvs_process_ul_hr_packet((uint8*)(vocoder_packet), &(mvs_frame_info.hr_rate), &speechData, &newPacketLength);
    mvs_frame_info.hdr.frame_mode = MVS_FRAME_MODE_HR_UL;
    break;

  case MVS_MODE_IS127:
    nRes = mvs_process_ul_evrc_packet((uint8*)(vocoder_packet), &(mvs_frame_info.voc_rate), &speechData, &newPacketLength);
    mvs_frame_info.hdr.frame_mode = MVS_FRAME_MODE_VOC_TX;
    break;

  case MVS_MODE_IS733:
    nRes = mvs_process_ul_13k_packet((uint8*)(vocoder_packet), &(mvs_frame_info.voc_rate), &speechData, &newPacketLength);
    mvs_frame_info.hdr.frame_mode = MVS_FRAME_MODE_VOC_TX;
    break;

  case MVS_MODE_4GV_NB:
    nRes = mvs_process_ul_4gv_packet((uint8*)(vocoder_packet), &(mvs_frame_info.voc_rate), &speechData, &newPacketLength);
    mvs_frame_info.hdr.frame_mode = MVS_FRAME_MODE_VOC_TX;
    break;

  case MVS_MODE_4GV_WB:
    nRes = mvs_process_ul_4gv_wb_packet((uint8*)(vocoder_packet), &(mvs_frame_info.voc_rate), &speechData, &newPacketLength);
    mvs_frame_info.hdr.frame_mode = MVS_FRAME_MODE_VOC_TX;
    break;

  case MVS_MODE_4GV_NW:
    nRes = mvs_process_ul_4gv_nw_packet((uint8*)(vocoder_packet), &(mvs_frame_info.voc_rate), &speechData, &newPacketLength);
    mvs_frame_info.hdr.frame_mode = MVS_FRAME_MODE_VOC_TX;
    break;

  case MVS_MODE_LINEAR_PCM:
    nRes = mvs_process_ul_linear_pcm_packet((uint8*)(vocoder_packet), &speechData, &newPacketLength);
    mvs_frame_info.hdr.frame_mode =  MVS_FRAME_MODE_PCM_UL;
    break;

  case MVS_MODE_G711:
    nRes = mvs_process_ul_g711_packet((uint8*)(vocoder_packet), &(mvs_frame_info.g711_rate), &speechData, &newPacketLength);
    mvs_frame_info.hdr.frame_mode = MVS_FRAME_MODE_G711_UL;
    break;

  case MVS_MODE_G711A:
    nRes = mvs_process_ul_g711a_packet((uint8*)(vocoder_packet), &(mvs_frame_info.g711a_rate), &speechData, &newPacketLength);
    mvs_frame_info.hdr.frame_mode = MVS_FRAME_MODE_G711_UL;
    break;

  case MVS_MODE_G729A:
    nRes = mvs_process_ul_g729a_packet((uint8*)(vocoder_packet), &(mvs_frame_info.g729a_rate), &speechData, &newPacketLength);
    mvs_frame_info.hdr.frame_mode =  MVS_FRAME_MODE_G729A_UL;
    break;

  case MVS_MODE_G722:
    nRes = mvs_process_ul_g722_packet((uint8*)(vocoder_packet), &(mvs_frame_info.g722_rate), &speechData, &newPacketLength);
    mvs_frame_info.hdr.frame_mode =  MVS_FRAME_MODE_G722_UL;
    break;

  default:
    mvs_frame_info.hdr.frame_mode = MVS_FRAME_MODE_NONE;
    return APR_EUNSUPPORTED;
  }

  mvs_frame_info.hdr.mode = mvs_client_table[mvs_cur_client].config_mode;

  if ( nRes != APR_EOK )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_deliver_voc_packet(): " \
            "Failed to process uplink vocoder packet with result (%d)",
            nRes );
    return APR_EFAILED;
  }

  /* Give vocoder packet to modem via callback funtion provided by the client. */
  if ( mvs_client_table[mvs_cur_client].ul_cb )
  {
#ifdef USE_MVS_PACKET_TIMING_ANALYSIS
    timetick = timetick_get( );
    /* Check to see if we have largest processing delay in uplink. */
    if ( ( mvs_timing_start_cnt_ul > mvs_timing_start_delay ) && 
         ( timetick - mvs_ul_prev ) > mvs_timing_info.ul_processing_delay_max )
    {  
      mvs_timing_info.ul_processing_delay_max = timetick - mvs_ul_prev;
    }
    if ( mvs_debug_timing )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "timetick of UL packet before " \
             "sending to client (%d)",
             timetick );
    }
#endif /* USE_MVS_PACKET_TIMING_ANALYSIS */

    mvs_client_table[mvs_cur_client].ul_cb(
      speechData, &mvs_frame_info, ( ( uint16_t ) newPacketLength ),
      &mvs_pkt_status );

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "mvs_deliver_voc_packet(): Packet " \
           "status: (%d)",
           mvs_pkt_status );

#ifdef USE_MVS_PACKET_TIMING_ANALYSIS
    if ( mvs_pkt_status == MVS_PKT_STATUS_FAST )
    {
      if ( mvs_timing_start_cnt_ul > mvs_timing_start_delay )
      {
        ++mvs_timing_info.ul_missed_fast_count;
      }
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "UL packet to modem was too " \
            "fast" );
    }
    if ( mvs_pkt_status == MVS_PKT_STATUS_SLOW )
    {
      if ( mvs_timing_start_cnt_ul > mvs_timing_start_delay )
      {
        ++mvs_timing_info.ul_missed_slow_count;
      }
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "UL packet to modem was too " \
            "slow" );
    }
#endif /* USE_MVS_PACKET_TIMING_ANALYSIS */
  }
  else
  {
    return APR_EFAILED;
  }

  switch ( mvs_client_table[mvs_cur_client].config_mode )
  {
  case MVS_MODE_IS127:
  case MVS_MODE_IS733:
  case MVS_MODE_4GV_NB:
  case MVS_MODE_4GV_WB:
  case MVS_MODE_4GV_NW:
    if ( log_status(LOG_VOC_REV_C) )
    {
      logresult = LogCDMATxPacket(mvs_client_table[mvs_cur_client].config_mode,
                                  mvs_frame_info.voc_rate.tx_info.rate,(void*)vocoder_packet);
    }
    break;
  case MVS_MODE_AMR:
  case MVS_MODE_AMR_WB:
  case MVS_MODE_EFR:
  case MVS_MODE_FR:
  case MVS_MODE_HR:
    if ( log_status(LOG_UMTS_TX_VOCODER_PACKET_DSP_C) )
    {
      logresult = LogUMTSTxPacket(mvs_client_table[mvs_cur_client].config_mode,mvs_frame_info,
                                  (void*)vocoder_packet);
    }
    break;
  default:
    logresult = APR_EBADPARAM;
    break;
  }
  if ( APR_EOK != logresult )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "Packet was not logged result " \
          "(%d)",
          logresult);
  }

  return APR_EOK ;
}

static int32_t mvs_get_voc_packet (
  void *vocoder_packet,
  uint32 *vocoder_packet_size
)
{
  uint8 pkt_buf[ VOICE_PACKET_LENGTH ];
  uint16 pkt_size;
  mvs_frame_info_type mvs_frame_info;
  mvs_pkt_status_type mvs_pkt_status;
  uint32_t nRes;
  mvs_token_object_t* obj_tw;
  vss_istream_cmd_set_dec_timewarp_t cvd_cmd_timewarp;
  int32_t logresult = APR_EOK;
  uint16 timewarp_factor;
#ifdef USE_MVS_PACKET_TIMING_ANALYSIS
  timetick_type timetick;
#endif /* USE_MVS_PACKET_TIMING_ANALYSIS */

  /* Verify parameters. */
  if ( vocoder_packet == NULL || vocoder_packet_size == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "Bad parameters." );
    return APR_EBADPARAM;
  }

  MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_get_voc_packet():" );

  /* Initialize vocoder_packet_size to 0 in case going into the failure path. */
  *vocoder_packet_size = 0;

  /* Initializing mvs frame info header info */
  mvs_frame_info.hdr.mode = mvs_client_table[mvs_cur_client].config_mode;

  switch ( mvs_client_table[mvs_cur_client].config_mode )
  {
  case MVS_MODE_AMR:
  case MVS_MODE_AMR_WB:
    mvs_frame_info.hdr.frame_mode = MVS_FRAME_MODE_AMR_DL;
    break;

  case MVS_MODE_EFR:
    mvs_frame_info.hdr.frame_mode = MVS_FRAME_MODE_GSM_DL;
    break;

  case MVS_MODE_FR:
    mvs_frame_info.hdr.frame_mode = MVS_FRAME_MODE_GSM_DL;
    break;

  case MVS_MODE_HR:
    mvs_frame_info.hdr.frame_mode = MVS_FRAME_MODE_HR_DL;
    break;

  case MVS_MODE_IS127:
    mvs_frame_info.hdr.frame_mode = MVS_FRAME_MODE_VOC_RX;
    break;

  case MVS_MODE_IS733:
    mvs_frame_info.hdr.frame_mode = MVS_FRAME_MODE_VOC_RX;
    break;

  case MVS_MODE_4GV_NB:
    mvs_frame_info.hdr.frame_mode = MVS_FRAME_MODE_VOC_RX;
    break;

  case MVS_MODE_4GV_WB:
    mvs_frame_info.hdr.frame_mode = MVS_FRAME_MODE_VOC_RX;
    break;

  case MVS_MODE_4GV_NW:
    mvs_frame_info.hdr.frame_mode = MVS_FRAME_MODE_VOC_RX;
    break;

  case MVS_MODE_LINEAR_PCM:
    mvs_frame_info.hdr.frame_mode = MVS_FRAME_MODE_PCM_DL;
    break;

  case MVS_MODE_G711:
    mvs_frame_info.hdr.frame_mode = MVS_FRAME_MODE_G711_DL;
    break;

  case MVS_MODE_G711A:
    mvs_frame_info.hdr.frame_mode = MVS_FRAME_MODE_G711A_DL;
    break;

  case MVS_MODE_G729A:
    mvs_frame_info.hdr.frame_mode = MVS_FRAME_MODE_G729A_DL;
    break;

  case MVS_MODE_G722:
    mvs_frame_info.hdr.frame_mode = MVS_FRAME_MODE_G722_DL;
    break;

  default:
    mvs_frame_info.hdr.frame_mode = MVS_FRAME_MODE_NONE;
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_get_voc_packet(): Invalid " \
          "mode" );
    return APR_EUNSUPPORTED;
  }

  /* Obtain downlink vocoder packet from modem via callback function provided by the client. */
  if ( mvs_client_table[mvs_cur_client].dl_cb )
  {
    mvs_client_table[mvs_cur_client].dl_cb((uint8*)pkt_buf, &mvs_frame_info, &mvs_pkt_status);

#ifdef USE_MVS_PACKET_TIMING_ANALYSIS
    timetick = timetick_get( );

    /* Check to see if we have largest delay in uplink. */
    if ( ( mvs_timing_start_cnt_dl > mvs_timing_start_delay ) &&
         ( mvs_dl_prev != 0 ) &&
         ( ( timetick - mvs_dl_prev ) > mvs_timing_info.dl_deliver_delay_max ) )
    {  
      mvs_timing_info.dl_deliver_delay_max = timetick - mvs_dl_prev;
    }

    if ( mvs_cur_client == MVS_CLIENT_WCDMA )
    {
      if ( mvs_debug_timing )
      {
       /*STMR_STATUS_DUMP(STMR_STATUS_DUMP_CMD_IMMEDIATE);
        mvs_curr_stmr_time_cx8 = STMR_GET_TX_FRAME_COUNT_STATUS();
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "DL WCDMA  DPCCH Chipx8 (%d)",
               mvs_curr_stmr_time_cx8  );*/
      }
    }

    /* Set prev to current time tick. */
    ++mvs_timing_start_cnt_dl;
    mvs_dl_prev = timetick;

    if ( mvs_debug_timing )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "timetick of DL packet recieved " \
             "sending from client (%d)",
             timetick );
    }
#endif /* USE_MVS_PACKET_TIMING_ANALYSIS */

    /* Biggest size Q6 allowed is only VOICE_PACKET_LENGTH. */
    pkt_size = VOICE_PACKET_LENGTH;
    mmstd_memcpy((void*)(vocoder_packet), pkt_size, (void*)(pkt_buf), pkt_size);

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "mvs_get_voc_packet(): Packet " \
           "status: (%d)",
           mvs_pkt_status );

    if ( mvs_pkt_status == MVS_PKT_STATUS_SLOW )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_get_voc_packet(): Modem did " \
            "not give any packet. "
            "Nothing to give DSP" );

#ifdef USE_MVS_PACKET_TIMING_ANALYSIS
      if ( mvs_timing_start_cnt_dl > mvs_timing_start_delay )
      {
        ++mvs_timing_info.dl_missed_slow_count;
      }
#endif /* USE_MVS_PACKET_TIMING_ANALYSIS */

      *vocoder_packet_size = 0;
      return APR_EOK;
    }

#ifdef USE_MVS_PACKET_TIMING_ANALYSIS
    if ( mvs_pkt_status == MVS_PKT_STATUS_FAST)
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "DL packet to modem was too " \
            "fast" );

      if ( mvs_timing_start_cnt_dl > mvs_timing_start_delay )
      {
        mvs_timing_info.dl_missed_fast_count++;
      }
    }
#endif /* USE_MVS_PACKET_TIMING_ANALYSIS */
  }
  else
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_get_voc_packet(): DL callback " \
          "function is not set" );
    *vocoder_packet_size = 0;
    return APR_EFAILED;
  }

  //Convert packet to format expected by decoder (add the header)
  switch ( mvs_client_table[mvs_cur_client].config_mode )
  {
  case MVS_MODE_AMR:
    nRes = mvs_process_dl_amr_packet((uint8*)(vocoder_packet), &(mvs_frame_info.amr_rate), vocoder_packet_size);
    break;

  case MVS_MODE_AMR_WB:
    nRes = mvs_process_dl_amr_wb_packet((uint8*)(vocoder_packet), &(mvs_frame_info.amr_rate), vocoder_packet_size);
    break;

  case MVS_MODE_EFR:
    nRes = mvs_process_dl_efr_packet((uint8*)(vocoder_packet), &(mvs_frame_info.gsm_rate), vocoder_packet_size);
    break;

  case MVS_MODE_FR:
    nRes = mvs_process_dl_fr_packet((uint8*)(vocoder_packet), &(mvs_frame_info.gsm_rate), vocoder_packet_size);
    break;

  case MVS_MODE_HR:
    nRes = mvs_process_dl_hr_packet((uint8*)(vocoder_packet), &(mvs_frame_info.hr_rate), vocoder_packet_size);
    break;

  case MVS_MODE_IS127:
    nRes = mvs_process_dl_evrc_packet((uint8*)(vocoder_packet), &(mvs_frame_info.voc_rate), vocoder_packet_size);
    break;

  case MVS_MODE_IS733:
    nRes = mvs_process_dl_13k_packet((uint8*)(vocoder_packet), &(mvs_frame_info.voc_rate), vocoder_packet_size);
    break;

  case MVS_MODE_4GV_NB:
    nRes = mvs_process_dl_4gv_packet((uint8*)(vocoder_packet), &(mvs_frame_info.voc_rate), vocoder_packet_size);
    break;

  case MVS_MODE_4GV_WB:
    nRes = mvs_process_dl_4gv_wb_packet((uint8*)(vocoder_packet), &(mvs_frame_info.voc_rate), vocoder_packet_size);
    break;

  case MVS_MODE_4GV_NW:
    nRes = mvs_process_dl_4gv_nw_packet((uint8*)(vocoder_packet), &(mvs_frame_info.voc_rate), vocoder_packet_size);
    break;

  case MVS_MODE_LINEAR_PCM:
    nRes = mvs_process_dl_linear_pcm_packet((uint8*)(vocoder_packet), vocoder_packet_size);
    break;

  case MVS_MODE_G711:
    nRes = mvs_process_dl_g711_packet((uint8*)(vocoder_packet), &(mvs_frame_info.g711_rate), vocoder_packet_size);
    break;

  case MVS_MODE_G711A:
    nRes = mvs_process_dl_g711a_packet((uint8*)(vocoder_packet), &(mvs_frame_info.g711a_rate), vocoder_packet_size);
    break;

  case MVS_MODE_G729A:
    nRes = mvs_process_dl_g729a_packet((uint8*)(vocoder_packet), &(mvs_frame_info.g729a_rate), vocoder_packet_size);
    break;

  case MVS_MODE_G722:
    nRes = mvs_process_dl_g722_packet((uint8*)(vocoder_packet), &(mvs_frame_info.g722_rate), vocoder_packet_size);
    break;

  default:
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_get_voc_packet(): Invalid " \
          "mode" );
    return APR_EUNSUPPORTED;
  }

  if ( nRes != APR_EOK )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_get_voc_packet(): Failed to " \
            "process downlink vocoder packet with result: (%d)",
            nRes );
    *vocoder_packet_size = 0;
    return APR_EOK;
  }

  /*send time warping. */
  /*for future vocoder that supports timewarp, update the below code if required. */
  if ( mvs_cur_client == MVS_CLIENT_VOIP )
  {
    cvd_cmd_timewarp.enable_time_warp = mvs_frame_info.voc_rate.rx_info.timewarp.enable;
    timewarp_factor = mvs_frame_info.voc_rate.rx_info.timewarp.factor;
    if ( timewarp_factor < 50 ) /* VCP does not accept valus less than 1/2 of the non-compressed/non-expanded PCM frame length */
    {
      timewarp_factor = 50;
    }
    if ( MVS_MODE_4GV_WB == mvs_client_table[mvs_cur_client].config_mode || MVS_MODE_4GV_NW == mvs_client_table[mvs_cur_client].config_mode )
    {
      cvd_cmd_timewarp.exp_length = (TIMEWARP_PCM_FRAME_LENGTH_WB * timewarp_factor)/100;
    }
    else
    {
      cvd_cmd_timewarp.exp_length = (TIMEWARP_PCM_FRAME_LENGTH_NB * timewarp_factor)/100;
    }
    cvd_cmd_timewarp.enable_phase_match = mvs_frame_info.voc_rate.rx_info.timewarp.phase_enable;
    cvd_cmd_timewarp.run_length = mvs_frame_info.voc_rate.rx_info.timewarp.run_length;
    cvd_cmd_timewarp.phase_offset = mvs_frame_info.voc_rate.rx_info.timewarp.phase_offset;

    nRes = mvs_create_autofree_token_object( &obj_tw );
    MVS_PANIC_ON_ERROR( nRes );
    nRes = __aprv2_cmd_alloc_send(
             mvs_my_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             mvs_my_addr, MVS_PORT,
             mvs_mvm_addr, mvs_mvm_port,
             obj_tw->header.handle, VSS_ISTREAM_CMD_SET_DEC_TIMEWARP,
             &cvd_cmd_timewarp, sizeof( cvd_cmd_timewarp ) );
    // MVS_PANIC_ON_ERROR( nRes );
    if ( nRes )
    {
      /* Temporary workaround to handle SSR phase 3. Any APR IPC communication 
       * failure will cause the current command to fail instead of panicing on 
       * failure.
       */
       MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "mvs_get_voc_packet(): Failed " \
               "to communicate with MVM, nRes = (0x%08X)",
               nRes );

       nRes = mvs_free_object( ( mvs_object_t* ) obj_tw );
       MVS_PANIC_ON_ERROR( nRes );
    }
  }

  switch ( mvs_client_table[mvs_cur_client].config_mode )
  {
  case MVS_MODE_IS127:
  case MVS_MODE_IS733:
  case MVS_MODE_4GV_NB:
  case MVS_MODE_4GV_WB:
  case MVS_MODE_4GV_NW:
    if ( log_status(LOG_VOC_FOR_C) )
    {
      logresult =  LogCDMARxPacket(mvs_client_table[mvs_cur_client].config_mode,
                                   mvs_frame_info.voc_rate.rx_info.rate ,vocoder_packet);
    }
    break;
  case MVS_MODE_AMR:
  case MVS_MODE_AMR_WB:
  case MVS_MODE_EFR:
  case MVS_MODE_FR:
  case MVS_MODE_HR:
    if ( log_status(LOG_UMTS_RX_VOCODER_PACKET_DSP_C) )
    {
      logresult = LogUMTSRxPacket(mvs_client_table[mvs_cur_client].config_mode,
                                  mvs_frame_info,vocoder_packet);
    }
    break;
  default:
    logresult = APR_EBADPARAM;
    break;
  }
  if ( APR_EOK != logresult )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "Packet was not logged result " \
          "(%d)",
          logresult);
  }

#ifdef USE_MVS_PACKET_TIMING_ANALYSIS
  timetick = timetick_get( );

  /* Check to see if we have largest delay in uplink. */
  if ( ( mvs_timing_start_cnt_dl > mvs_timing_start_delay ) && 
       ( ( timetick - mvs_dl_prev ) > mvs_timing_info.dl_processing_delay_max ) )
  {
    mvs_timing_info.dl_processing_delay_max = timetick - mvs_dl_prev;
  }

  if ( mvs_debug_timing )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "timetick of DL packet before " \
           "sending to DSP (%d)",
           timetick );
  }
#endif /* USE_MVS_PACKET_TIMING_ANALYSIS */

  return APR_EOK;
}

int32_t mvs_send_dec_pkt (
  void* buffer,
  uint32 buf_size,
  aprv2_packet_t* in_packet
)
{
  int32_t rc;
  aprv2_packet_t* packet;
  vss_istream_evt_send_dec_buffer_t* dec_buffer;
  uint8_t* content;
  uint32_t dest_buf_size;

  rc = __aprv2_cmd_alloc_ext(
         mvs_my_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
         in_packet->dst_addr, in_packet->dst_port,
         in_packet->src_addr, in_packet->src_port,
         in_packet->token, VSS_ISTREAM_EVT_SEND_DEC_BUFFER,
         ( sizeof( vss_istream_evt_send_dec_buffer_t ) + buf_size ),
         &packet );
  MVS_PANIC_ON_ERROR( rc );

  dec_buffer = APRV2_PKT_GET_PAYLOAD( vss_istream_evt_send_dec_buffer_t, packet );
  dest_buf_size = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE(packet->header)
                      - sizeof( vss_istream_evt_send_dec_buffer_t );
  dec_buffer->media_id = map_to_cvd_mode(mvs_client_table[mvs_cur_client].config_mode);
  content = APR_PTR_END_OF( dec_buffer, sizeof( vss_istream_evt_send_dec_buffer_t ) );
  mmstd_memcpy( content, dest_buf_size , buffer, buf_size );

  ( void ) __aprv2_cmd_forward( mvs_my_apr_handle, packet );

  return APR_EOK;
}

/****************************************************************************
 * SILENCE FRAME GENERATION                                                 *
 ****************************************************************************/

/* This functions check if Silence Frame Generation is supported on mvs_cur_client.
 */
static bool_t mvs_is_silence_frame_supported_for_mvs_cur_client ( void )
{
  switch( mvs_cur_client )
  {
  case MVS_CLIENT_WCDMA:
  case MVS_CLIENT_TDSCDMA: 
    {
      return TRUE;
    }
  default:
    break;
  }
  return FALSE;
}

/* This funtion will be called in timer context by voiceapp, it will queue an internal command 
 * MVS_CMD_SEND_SILENCE_FRAME for delivering silence frame in mvs context. 
 */
uint32_t mvs_mod_send_silence_frame ( void* vsid )
{
  uint32_t rc = APR_EOK;
  mvs_packet_type mvs_cmd;
  aprv2_packet_t* packet;

  for ( ;; )
  {
    if ( mvs_is_initialized )
    {
      mvs_cmd.process_voiceapp.hdr.cmd = MVS_CMD_SEND_SILENCE_FRAME;
      mvs_cmd.process_voiceapp.vsid = ( ( uint32_t )vsid );

      MSG ( MSG_SSID_DFLT, MSG_LEGACY_LOW,
            "mvs_mod_send_silence_frame(): " );

      rc = mvs_make_cmd_packet( &mvs_cmd, &packet );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "mvs_mod_send_silence_frame(): make pkt failed! rc = (0x%08X)", rc );
        break;
      }

      if ( mvs_queue_incoming_packet( packet ) == FALSE )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "mvs_mod_send_silence_frame(): mvs_queue_incoming_packet failed" );
        rc = APR_EFAILED;
        break;
      }

      break;
    }
    else
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "mvs_mod_send_silence_frame():MVS is not initialized" );
      rc = APR_EFAILED;
      break;
    }
  }

  return rc;
}

/* MVS registers this function with voiceapp, this Function will be triggered at enc_offset 
 * from VFR timing, and will would implement the logic of pumping silence frame on TX. 
 * Please also refer to comment of MVS_PACKET_MISS_THRESHOLD_FOR_PUMPING_SILENCE_FRAME.
 */
static void mvs_tx_modem_timing_cb (
  void* client_data
)
{
  uint32_t rc = APR_EOK;
  /* Wait till mvs_pkt_miss_count reaches threshold, once it reaches threshold start pumping
   * silence frame. 
   */
  if ( mvs_pkt_miss_count >= MVS_PACKET_MISS_THRESHOLD_FOR_PUMPING_SILENCE_FRAME  )
  {
    rc = mvs_mod_send_silence_frame( client_data );
    if( rc ) 
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, 
             "mvs_tx_modem_timing_cb(): mvs_mod_send_silence_frame failed rc = 0x%08X", rc );
    }
  }
}

/* MVS uses this function to register for modem timing from voiceapp, Silence Frame will be pumped on Tx, 
 * if packets are unavailable from ADSP. 
 */
static uint32_t mvs_register_modem_timing ( void )
{
  uint32_t rc = APR_EOK;
  
  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
         "mvs_deregister_modem_timing(): mvs_cur_client = (0x%08X)", mvs_cur_client );

  switch ( mvs_cur_client )
  {
  case MVS_CLIENT_WCDMA:
    {
      rc = voiceapp_register_wcdma_timing_reference( 
             MVS_VSID_CS_VOICE_MULTIMODE, mvs_process_voiceapp_fn, NULL, mvs_tx_modem_timing_cb );
    }
    break;
 
  case MVS_CLIENT_TDSCDMA: 
    {
      rc = voiceapp_register_tdscdma_timing_reference(
             MVS_VSID_CS_VOICE_MULTIMODE, mvs_process_voiceapp_fn, NULL, mvs_tx_modem_timing_cb );
    }
    break;

  default: 
    { 
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "mvs_register_modem_timing(): Silence Frame not supported for mvs client = (0x%08X)",
             mvs_cur_client );
      rc = APR_EUNSUPPORTED;
    }
    break;
  }

  return rc;  
}

/* MVS uses this function to De register itself for modem timing from voiceapp. 
 */
static uint32_t mvs_deregister_modem_timing ( void )
{
  uint32_t rc = APR_EOK;

  switch ( mvs_cur_client )
  {
  case MVS_CLIENT_WCDMA:
    {
      rc = voiceapp_register_wcdma_timing_reference( 
             MVS_VSID_CS_VOICE_MULTIMODE, NULL, NULL, NULL );
    }
    break;
 
  case MVS_CLIENT_TDSCDMA: 
    {
      rc = voiceapp_register_tdscdma_timing_reference( 
             MVS_VSID_CS_VOICE_MULTIMODE, NULL, NULL, NULL );
    }
    break;

  default: 
    { 
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "mvs_deregister_modem_timing(): Silence Frame not supported for mvs client = (0x%08X)",
               mvs_cur_client );

      rc = APR_EUNSUPPORTED;
    }
    break;
  }

  if ( rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "mvs_deregister_modem_timing(): mvs_cur_client = (0x%08X) rc = (0x%08X)",
            mvs_cur_client, rc );
  }

  return rc;
}

/* Delivers Silence Frame on Tx.
 */
static uint32_t mvs_deliver_tx_silence_frame
( 
  uint8_t* vocoder_packet,
  uint32_t* vocoder_packet_size
)
{
  uint32_t rc = APR_EOK;
  voiceapp_amr_frame_info_t amr_info;
  voiceapp_amrwb_frame_info_t amrwb_info;

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
         "mvs_tx_modem_timing_cb(): pkt_miss_cnt: (0x%08X)", mvs_pkt_miss_count );
  
  switch ( mvs_client_table[mvs_cur_client].config_mode )
  {
  case MVS_MODE_AMR:
    {
      amr_info.codec_mode = mvs_new_props.amr_ul_mode;
      amr_info.frame_type = VOICEAPP_AMR_FRAME_TYPE_SPEECH_GOOD;

      /* vocoder packet returned will have header( containing mode and frame info )
       * and core amr structre( or speech data ), which has only class A, B, C bits.
       * Vocoder packet contructed by voiceapp will be of same format as given by vsm
       * refer to comments in voiceapp on voiceapp_amr_if1_homing_frame.  
       */
      rc = voiceapp_get_homing_frame( map_to_cvd_mode( MVS_MODE_AMR ), &amr_info,
                                      vocoder_packet, vocoder_packet_size );
    }  
    break;

  case MVS_MODE_AMR_WB:
    {
      amrwb_info.codec_mode = ( mvs_new_props.awb_ul_mode - MVS_AMR_MODE_0660 );
      amrwb_info.frame_type = VOICEAPP_AMRWB_FRAME_TYPE_SPEECH_GOOD;

      /* vocoder packet returned will have header( containing mode and frame info )
       * and core amrwb structre( or speech data ), which has only class A, B, C bits.
       * Vocoder packet contructed by voiceapp will be of same format as given by vsm
       * refer to comments in voiceapp on voiceapp_amrwb_if1_homing_frame.  
       */
      rc = voiceapp_get_homing_frame( map_to_cvd_mode( MVS_MODE_AMR_WB ), &amrwb_info, 
                                      vocoder_packet, vocoder_packet_size );
    }
    break;

  default: 
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "mvs_deliver_tx_silence_frame(): mvs mode = (0x%08X) not supported for Silence Frame",
             mvs_client_table[mvs_cur_client].config_mode );
      rc = APR_EUNSUPPORTED;
    }
    break;
  }

  if( rc == APR_EOK )
  {
    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "mvs_deliver_tx_silence_frame(): mvs_cur_client = (0x%08X), vocoder_pkt_size = (%d), "
           "vocoder_packet[0] = (0x%02X)", mvs_cur_client, *vocoder_packet_size, vocoder_packet[0] );
  
    mvs_deliver_voc_packet( ( ( void*) vocoder_packet ), *vocoder_packet_size );
  }
  else
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "mvs_deliver_tx_silence_frame():failed with rc = (0x%08X)", rc );
  }

  return rc;
}

/****************************************************************************
 * DISPATCHING ROUTINES                                                     *
 ****************************************************************************/

/* Dispatch the incoming packet in-context (assume in ISR). */
static void mvs_dispatch_packet (
  aprv2_packet_t* packet
)
{
  ( void ) mvs_queue_incoming_packet( packet );
}

static int32_t mvs_isr_dispatch_fn (
  aprv2_packet_t* packet,
  void* dispatch_data
)
{
  mvs_dispatch_packet( packet );
  return APR_EOK;
}

static void mvs_response_fn_trampoline (
  uint32_t fn_index,
  aprv2_packet_t* packet
)
{
  int32_t rc;
  mvs_object_t* object;
  uint32_t msg_type;

  msg_type = APR_GET_FIELD( APRV2_PKT_MSGTYPE, packet->header );

  if ( msg_type == APRV2_PKT_MSGTYPE_EVENT_V )
  {
    /* The dst_port in MVS is hard coded -  cannot be looked up.
     * ALWAYS drop events
     */
    rc = APR_EFAILED;
  }
  else
  {
    rc = mvs_get_object( packet->token, &object );
  }

  if ( rc == APR_EOK )
  {
    switch ( object->header.type )
    {
    case MVS_OBJECT_TYPE_ENUM_HANDLE:
      object->handle.fn_table[ fn_index ]( packet );
      return;

    case MVS_OBJECT_TYPE_ENUM_SIMPLE_JOB:
      object->simple_job.fn_table[ fn_index ]( packet );
      return;

    default:
      break;
    }
  }

  ( void ) __aprv2_cmd_free( mvs_my_apr_handle, packet );
}

/* Process high priority events, responses, and non-gating commands in task context. */
static void mvs_task_process_events_and_nongating_commands ( void )
{
  int32_t rc = APR_EOK;
  mvs_work_item_t* work_item;
  aprv2_packet_t* packet;
  mvs_packet_type* mvs_cmd;
  bool_t queue_to_pending_flag;
  uint8 vocoder_packet[ VOICE_PACKET_LENGTH ];
  uint32 vocoder_packet_size;
  uint8_t* payload_content;
#ifdef USE_MVS_PACKET_TIMING_ANALYSIS
  timetick_type timetick;
#endif /* USE_MVS_PACKET_TIMING_ANALYSIS */

  while ( apr_list_remove_head( &mvs_incoming_cmd_q,
                                ( ( apr_list_node_t** ) &work_item ) ) == APR_EOK )
  {
    packet = ( ( aprv2_packet_t* ) work_item->packet );
    ( void ) apr_list_add_tail( &mvs_free_cmd_q, &work_item->link );
    queue_to_pending_flag = FALSE;

    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "Processing the non-gating queue - " \
          "opcode=(0x%08X), token=(0x%08X)",
          packet->opcode, packet->token );

    switch ( packet->opcode )
    {
    case APRV2_IBASIC_EVT_ACCEPTED:
      mvs_response_fn_trampoline( MVS_RESPONSE_FN_ENUM_ACCEPTED, packet );
      break;

    case APRV2_IBASIC_RSP_RESULT:
      mvs_response_fn_trampoline( MVS_RESPONSE_FN_ENUM_RESULT, packet );
      break;

    case VSS_ISTREAM_EVT_READY:
      {
        if ( ( mvs_cur_client != MVS_CLIENT_NONE )
             && ( mvs_client_table[mvs_cur_client].state == MVS_STATE_VOCODER ) )
        {
          mvs_send_mode_event( mvs_cur_client, MVS_MODE_READY,
                               mvs_client_table[mvs_cur_client].config_mode );
            /**< Indicate to the client that the vocoder is ready. */
        }

        ( void ) __aprv2_cmd_free( mvs_my_apr_handle, packet );

/*
FIXME:
  What if the READY signal was sent before mvs_enable() completes? The VSS_ISTREAM_EVT_READY
  would be shunted from actually sending MVS_MODE_READY to the MVS client. The way out of
  this problem is store the event ready into the client variable and having a function call
  to check if this is the first time notification has been sent since the last time the
  conditions were right for notification. This routine would be called by both
  VSS_ISTREAM_EVT_READY handler and the mvs_enable() and mvs_standby() functions.
  
  Sending the mode event based on VSS_ISTREAM_EVT_READY alone would not work because:
  1) We would be sending an event before the command completed which may or may not be
     expected by the client.
  2) We would ignore the mvs_client_table[mvs_cur_client].state because starting CVS may
     cause us to receive VSS_ISTREAM_EVT_READY but the rest of the mvs_enable() or
     mvs_standby() processing has not completed.
     
*/

      }
      break;

    case VSS_ISTREAM_EVT_NOT_READY:
      {
        if ( ( mvs_cur_client != MVS_CLIENT_NONE )
             && ( mvs_client_table[mvs_cur_client].state == MVS_STATE_VOCODER ) )
        {
          mvs_send_mode_event( mvs_cur_client, MVS_MODE_NOT_AVAIL,
                               mvs_client_table[mvs_cur_client].config_mode );
            /**< Indicate to the client that the vocoder is not available. */
        }

        ( void ) __aprv2_cmd_free( mvs_my_apr_handle, packet );
      }
      break;

    case VSS_ISTREAM_EVT_SEND_ENC_BUFFER:
      {
        if ( ( mvs_cur_client != MVS_CLIENT_NONE )
             && ( mvs_client_table[mvs_cur_client].state == MVS_STATE_VOCODER )
             && ( mvs_client_table[mvs_cur_client].pending_cmd.is_done == TRUE ) )
        {
#ifdef USE_MVS_PACKET_TIMING_ANALYSIS
          /* Make sure ul cb exists before logging timing. */
          if ( mvs_client_table[mvs_cur_client].ul_cb != NULL )
          {
            /* Record timing info. */
            timetick = timetick_get( );

            /* Check to see if we have largest delay in uplink. */
            if ( ( mvs_timing_start_cnt_ul > mvs_timing_start_delay ) && 
                 ( mvs_ul_prev != 0 ) &&
                 ( ( timetick - mvs_ul_prev ) > mvs_timing_info.ul_deliver_delay_max ) )
            {
              mvs_timing_info.ul_deliver_delay_max = timetick - mvs_ul_prev;
            }

            if ( mvs_cur_client == MVS_CLIENT_WCDMA )
            {
              if ( mvs_debug_timing )
              {
                /* STMR_STATUS_DUMP( STMR_STATUS_DUMP_CMD_IMMEDIATE );
                mvs_curr_stmr_time_cx8 = STMR_GET_TX_FRAME_COUNT_STATUS( );
                MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "UL WCDMA DPCCH " \
                       "Chipx8 (%d)",
                       mvs_curr_stmr_time_cx8  ); */
              }
            }

            /* Set prev to current time tick. */
            ++mvs_timing_start_cnt_ul;
            mvs_ul_prev = timetick;

            if ( mvs_debug_timing )
            {
              MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "timetick of UL packet " \
                     "recieved from DSP (%d)",
                     timetick );
            }      
          }
#endif /* USE_MVS_PACKET_TIMING_ANALYSIS */

          vocoder_packet_size = ( APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( packet->header ) -
                                  sizeof( vss_istream_evt_send_enc_buffer_t ) );
          payload_content = APR_PTR_END_OF( APRV2_PKT_GET_PAYLOAD( void, packet ),
                                            sizeof( vss_istream_evt_send_enc_buffer_t ) );
          if ( vocoder_packet_size > sizeof( vocoder_packet ) )
          {
            MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                    "mvs_task_process_events_and_nongating_commands(): " \
                    "Received unexpected packet length (%d)",
                    vocoder_packet_size );
            MVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
          }
       
          {
            if( mvs_pkt_miss_count > MVS_PACKET_MISS_THRESHOLD_FOR_PUMPING_SILENCE_FRAME ) 
            {
              mvs_clear_ul_dsm_amr_queue( );
            }
            /* Clearing the packet miss count, since packet is available from ADSP. */
            mvs_pkt_miss_count = 0;
          }
          mmstd_memcpy( vocoder_packet, VOICE_PACKET_LENGTH, payload_content, vocoder_packet_size );
          mvs_deliver_voc_packet( ( ( void* ) &vocoder_packet ), vocoder_packet_size );         
        }
        ( void ) __aprv2_cmd_free( mvs_my_apr_handle, packet );
      }
      break;

    case VSS_ISTREAM_EVT_REQUEST_DEC_BUFFER:
      {
        if ( ( mvs_cur_client != MVS_CLIENT_NONE )
             && ( mvs_client_table[mvs_cur_client].state == MVS_STATE_VOCODER )
             && ( mvs_client_table[mvs_cur_client].pending_cmd.is_done == TRUE ) )
        {
          mvs_get_voc_packet( ( ( void* ) &vocoder_packet ), &vocoder_packet_size );
          /* Loop back the received near-end Tx packet back to Rx. */
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_get_voc_packet (%d)",
                 vocoder_packet_size );
          rc = mvs_send_dec_pkt( ( ( void* ) vocoder_packet ), vocoder_packet_size, packet );
          MVS_PANIC_ON_ERROR( rc );
        }
        ( void ) __aprv2_cmd_free( mvs_my_apr_handle, packet );
      }
      break;

    case VSS_ISTREAM_EVT_EAMR_MODE_CHANGED:
      {
        vss_istream_evt_eamr_mode_changed_t* mode_changed;
        uint32_t sr = MVS_EAMR_SR_8000;
        bool_t is_sr_valid = TRUE;

        mode_changed = APRV2_PKT_GET_PAYLOAD( vss_istream_evt_eamr_mode_changed_t, packet );
        if ( mode_changed->mode == VSS_ISTREAM_EAMR_MODE_NARROWBAND )
        {
          sr = MVS_EAMR_SR_8000;
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,  "eAMR mode changed, sampling " \
                "rate = (%d)",
                8000 );
        }
        else if ( mode_changed->mode == VSS_ISTREAM_EAMR_MODE_WIDEBAND )
        {
          sr = MVS_EAMR_SR_16000;
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,  "eAMR mode changed, sampling " \
                "rate = (%d)",
                16000 );
        }
        else
        { 
          is_sr_valid = FALSE;
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "Unsupported eAMR mode " \
                  "event, mode = (%d)",
                  mode_changed->mode );
        }

        if ( ( mvs_cur_client != MVS_CLIENT_NONE )
             && ( mvs_client_table[mvs_cur_client].state == MVS_STATE_VOCODER )
             && ( is_sr_valid == TRUE ) )
        {
          ( void ) mvs_send_eamr_sr_event( mvs_cur_client, sr );
        }
        
      }
      ( void ) __aprv2_cmd_free( mvs_my_apr_handle, packet );
      break;

    case VSS_IMVS_CMD_INTERNAL:
      {
        mvs_cmd = APRV2_PKT_GET_PAYLOAD( mvs_packet_type, packet );

        switch ( packet->token )
        {
        case MVS_CMD_SEND_SILENCE_FRAME:
          rc = mvs_deliver_tx_silence_frame( vocoder_packet, &vocoder_packet_size );
          break;

        case MVS_CMD_PROCESS_VOICEAPP:
          rc = voiceapp_process_command( mvs_cmd->process_voiceapp.vsid );
          break;
   
        case MVS_CMD_SET_CDMA_MINMAX_RATE:
          rc = mvs_mod_voc_set_frame_rate_proc(
                 mvs_cmd->set_cdma_minmax_rate.max_rate,
                 mvs_cmd->set_cdma_minmax_rate.min_rate,
                 FALSE );
          break;

        case MVS_CMD_SET_CDMA_RATE_FACTOR:
          rc = mvs_mod_voc_tx_rate_limit_proc(
                 mvs_cmd->set_cdma_rate_factor.rate_factor,
                 FALSE );
          break;

        case MVS_CMD_SET_DTX_MODE:
          rc = mvs_mod_set_dtx_mode_proc(
                 mvs_cmd->set_dtx_mode.dtx_mode,
                 FALSE );
          break;

        case MVS_CMD_SET_AMR_MODE:
          rc = mvs_mod_amr_set_amr_mode_proc(
                 mvs_cmd->set_amr_mode.amr_mode,
                 FALSE );
          break;

        case MVS_CMD_SET_AWB_MODE:
          rc = mvs_mod_amr_set_awb_mode_proc(
                 mvs_cmd->set_amr_mode.amr_mode,
                 FALSE );
          break;

        default:
          queue_to_pending_flag = TRUE;
          break;
        }

        if ( queue_to_pending_flag == FALSE )
        {
          ( void ) __aprv2_cmd_end_command( mvs_my_apr_handle, packet, rc );
        }
      }
      break;

    default:
      queue_to_pending_flag = TRUE;
      break;
    }

    if ( queue_to_pending_flag == TRUE )
    {
      mvs_queue_pending_packet( packet );
    }
  }
}

/* Process normal priority sequentially executed pending commands. */
static void mvs_task_process_pending_commands ( void )
{
  int32_t rc;
  aprv2_packet_t* packet;
  mvs_work_item_t* work_item;
  mvs_work_item_t* work_item_release_cmd;
  bool_t is_unsupported;

  if ( mvs_adsp_crash )
  {
    /* Cancel the current command, if it is not release command. */ 
    if ( ( mvs_pending_ctrl.state != MVS_PENDING_CMD_STATE_ENUM_FETCH ) &&
         ( mvs_client_table[ mvs_cur_client ].pending_cmd.cur_cmd != MVS_CMD_RELEASE ) )
    {
      /* Free all job objects. */ 
      ( void ) mvs_free_all_allocated_objects( );
      mvs_is_allocated_objs_freed_on_ssr = TRUE;
      MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_ssr_debug: freed all " \
          "allocated objects" );

      /* Send failure. */
      mvs_send_cmd_event( mvs_cur_client, MVS_CMD_FAILURE );

      /* Free the command. */ 
      rc = __aprv2_cmd_free( mvs_my_apr_handle, mvs_pending_ctrl.packet );
      MVS_PANIC_ON_ERROR( rc );

      /* Fetch next command. */
      mvs_pending_ctrl.state = MVS_PENDING_CMD_STATE_ENUM_FETCH;
    }

    { /* Queue the SSR packet. */
      rc = __aprv2_cmd_alloc_ext(
             mvs_my_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
             mvs_my_addr, APR_NULL_V,
             mvs_my_addr, APR_NULL_V,
             MVS_CMD_SSR, VSS_IMVS_CMD_INTERNAL,
             0, &packet );

      if ( ( mvs_pending_ctrl.state != MVS_PENDING_CMD_STATE_ENUM_FETCH ) &&
           ( mvs_client_table[ mvs_cur_client ].pending_cmd.cur_cmd == MVS_CMD_RELEASE ) )
      { 
        /* Temporary workaround for handling SSR phase 3. Queue the SSR packet
         * after the release command if a release command is pending.
         */

        ( void ) apr_list_remove_head( 
                   &mvs_pending_cmd_q, 
                   ( ( apr_list_node_t** ) &work_item_release_cmd ) );

        MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "MVS Enqueuing SSR packet" );

        rc = apr_list_remove_head( &mvs_free_cmd_q, ( ( apr_list_node_t** ) &work_item ) );
        if ( rc )
        { /* No free command structure is available. */
          __aprv2_cmd_free( mvs_my_apr_handle, packet );
          MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "MVS cannot enqueue SSR " \
                "packet, panicking" );
          MVS_PANIC_ON_ERROR(rc);
        }

        work_item->packet = packet;

        ( void ) apr_list_add_head( &mvs_pending_cmd_q, &work_item->link );
        ( void ) apr_list_add_head( &mvs_pending_cmd_q, &work_item_release_cmd->link );
      }
      else
      { /* Enqueue packet in first pending queue to enter not ready state. */
        MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "MVS Enqueuing SSR packet" );

        rc = apr_list_remove_head( &mvs_free_cmd_q, ( ( apr_list_node_t** ) &work_item ) );
        if ( rc )
        { /* No free command structure is available. */
          __aprv2_cmd_free( mvs_my_apr_handle, packet );
          MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "MVS cannot enqueue SSR " \
                "packet, panicking" );
          MVS_PANIC_ON_ERROR(rc);
        }

        work_item->packet = packet;
        ( void ) apr_list_add_head( &mvs_pending_cmd_q, &work_item->link );
      }
    }

    mvs_adsp_crash = FALSE;
    mvs_is_first_call = TRUE;
  }

  for ( ;; )
  {
    switch ( mvs_pending_ctrl.state )
    {
    case MVS_PENDING_CMD_STATE_ENUM_FETCH:
      {
        { /* Fetch the next pending command to execute. */
          rc = apr_list_remove_head( &mvs_pending_cmd_q,
                                     ( ( apr_list_node_t** ) &work_item ) );
          if ( rc )
          { /* Return when the pending command queue is empty. */
            return;
          }
          mvs_pending_ctrl.packet = work_item->packet;
          ( void ) apr_list_add_tail( &mvs_free_cmd_q, &work_item->link );

          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "Fetched a gating command - " \
                "opcode=(0x%08X), token=(0x%08X)",
                mvs_pending_ctrl.packet->opcode,
                mvs_pending_ctrl.packet->token );
        }
        mvs_pending_ctrl.state = MVS_PENDING_CMD_STATE_ENUM_EXECUTE;
      }
      break;

    case MVS_PENDING_CMD_STATE_ENUM_EXECUTE:
    case MVS_PENDING_CMD_STATE_ENUM_CONTINUE:
      {
        is_unsupported = FALSE;
        packet = mvs_pending_ctrl.packet;

        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "Running the gating command - " \
              "opcode=(0x%08X), token=(0x%08X)",
              packet->opcode, packet->token );

        switch ( packet->opcode )
        {
        case VSS_IMVS_CMD_INTERNAL:
          {
            switch ( mvs_pending_ctrl.packet->token )
            {
            case MVS_CMD_ACQUIRE:
              rc = mvs_mod_acquire_proc( &mvs_pending_ctrl );
              break;

            case MVS_CMD_ENABLE:
              rc = mvs_mod_enable_proc( &mvs_pending_ctrl );
              break;

            case MVS_CMD_STANDBY:
              rc = mvs_mod_standby_proc( &mvs_pending_ctrl );
              break;

            case MVS_CMD_RELEASE:
              rc = mvs_mod_release_proc( &mvs_pending_ctrl );
              break;

            case MVS_CMD_SSR:
              rc = mvs_mod_adsp_reset_proc( &mvs_pending_ctrl );
              break;

            default:
              is_unsupported = TRUE;
              break;
            }
          }
          break;

        case VSS_IMVS_CMD_SHUTDOWN:
          rc = mvs_mod_shutdown_proc( &mvs_pending_ctrl );
          break;

        default:
          is_unsupported = TRUE;
          break;
        }

        if ( is_unsupported == TRUE )
        {
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "Received an unsupported " \
                  "command ((0x%08X), (0x%08X))",
                  packet->opcode, packet->token );

          ( void ) __aprv2_cmd_end_command(
                     mvs_my_apr_handle, mvs_pending_ctrl.packet,
                     APR_EUNSUPPORTED );

          /* Go process the next command. */
          rc = APR_EOK;
        }

        if ( rc == APR_EOK )
        { /* The current command finished so fetch the next command. */
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "Finished the gating command " \
                "- opcode=(0x%08X), token=(0x%08X)",
                packet->opcode, packet->token );

          mvs_pending_ctrl.state = MVS_PENDING_CMD_STATE_ENUM_FETCH;
        }
        else
        {/* Assuming the current pending command control routine returns
           * APR_EPENDING the overall progress stalls until one or more
           * external events or responses are received.
           */
          mvs_pending_ctrl.state = MVS_PENDING_CMD_STATE_ENUM_CONTINUE;
          return;
        }
      }
      break;

    default:
      /* Assert when the pending state doesn't exist. */
      return;
    }
  }
}

/****************************************************************************
 * PENDING COMMAND ROUTINES                                                 *
 ****************************************************************************/

static int32_t mvs_pending_control_init (
  mvs_pending_control_t* ctrl
)
{
  int32_t rc;

  if ( ctrl == NULL )
  {
    return APR_EBADPARAM;
  }

  rc = apr_list_init( &ctrl->cmd_q, mvs_thread_lock_fn, mvs_thread_unlock_fn );
  if ( rc )
  {
    return APR_EFAILED;
  }

  ctrl->state = MVS_PENDING_CMD_STATE_ENUM_FETCH;
  ctrl->packet = NULL;
  ctrl->rootjob_obj = NULL;

  return APR_EOK;
}

static int32_t mvs_pending_control_destroy (
  mvs_pending_control_t* ctrl
)
{
#if 0 /* There's nothing to destroy if everything is in a good state. */
  int32_t rc;
#endif /* 0 */

  if ( ctrl == NULL )
  {
    return APR_EBADPARAM;
  }

#if 0 /* There's nothing to destroy if everything is in a good state. */
  rc = apr_list_destroy( &ctrl->cmd_q );
  if ( rc )
  {
    return APR_EFAILED;
  }

  ctrl->state = MVS_PENDING_CMD_STATE_ENUM_UNINITIALIZED;
  ctrl->packet = NULL;
  ctrl->rootjob_obj = NULL;
#endif /* 0 */

  return APR_EOK;
}

/****************************************************************************
 * IST ROUTINES                                                             *
 ****************************************************************************/

static int32_t mvs_mod_run ( void )
{
  unsigned long long qtimer_start_tick = 0;

  if ( mvs_processing_time_info.is_enabled )
  {
    qtimer_start_tick = timetick_get();
  }

  mvs_task_process_events_and_nongating_commands( );
  mvs_task_process_pending_commands( );

  if ( mvs_processing_time_info.is_enabled )
  {
    mvs_check_processing_time( qtimer_start_tick );
  }

  return APR_EOK;
}

static int32_t mvs_ist ( void* param )
{
  int32_t rc;

  rc = apr_event_create( &mvs_ist_event );
  MVS_PANIC_ON_ERROR( rc );

  mvs_ist_state = MVS_THREAD_STATE_ENUM_READY;

  do
  {
    rc = apr_event_wait( mvs_ist_event );
#ifndef USE_SINGLE_AUDSVC_THREAD
   ( void ) mvs_mod_run( );
#endif /* !USE_SINGLE_AUDSVC_THREAD */
  }
  while ( rc == APR_EOK );

  rc = apr_event_destroy( mvs_ist_event );
  MVS_PANIC_ON_ERROR( rc );

  mvs_ist_state = MVS_THREAD_STATE_ENUM_EXIT;

  return APR_EOK;
}

/****************************************************************************
 * EXTERNAL API ROUTINES                                                    *
 ****************************************************************************/

static int32_t mvs_mod_init ( void )
{
  int32_t rc;
  uint32_t i;

#ifndef WINSIM
  RCECB_HANDLE ssr_handle;
#endif /* !WINSIM */

  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_mod_init() Date %s Time %s",
        __DATE__, __TIME__ );

  {  /* Initialize the locks. */
    rc = apr_lock_create( APR_LOCK_TYPE_INTERRUPT, &mvs_int_lock );
    rc = apr_lock_create( APR_LOCK_TYPE_MUTEX, &mvs_thread_lock );
  }
  { /* Initialize the custom heap. */
    apr_memmgr_init_heap( &mvs_heapmgr, ( ( void* ) &mvs_heap_pool ), sizeof( mvs_heap_pool ), NULL, NULL );
  }
  { /* Initialize the object manager. */
    apr_objmgr_setup_params_t params;

    params.table = mvs_object_table;
    params.total_bits = MVS_HANDLE_TOTAL_BITS;
    params.index_bits = MVS_HANDLE_INDEX_BITS;
    params.lock_fn = mvs_int_lock_fn;
    params.unlock_fn = mvs_int_unlock_fn;
    rc = apr_objmgr_construct( &mvs_objmgr, &params );
    /* TODO: When the ISR and task context contentions becomes a problem
     *       the objmgr could be split into ISR/task and task-only stores
     *       to reduce the bottleneck.
     */
  }
  { /* Initialize the session management. */
    rc = mvs_pending_control_init( &mvs_pending_ctrl );
  }
  { /* Populate free command structures */
    rc = apr_list_init( &mvs_free_cmd_q, mvs_int_lock_fn, mvs_int_unlock_fn );
    for ( i = 0; i < MVS_NUM_COMMANDS; ++i )
    {
      rc = apr_list_add_tail( &mvs_free_cmd_q, ( ( apr_list_node_t* ) &mvs_cmd_pool[i] ) );
    }
  }
  {
    rc = apr_list_init( &mvs_incoming_cmd_q, mvs_int_lock_fn, mvs_int_unlock_fn );
    rc = apr_list_init( &mvs_pending_cmd_q, mvs_int_lock_fn, mvs_int_unlock_fn );
  }
#ifndef WINSIM
  {
    mvs_dsm_amr_init();
    MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_mod_init(): DSM init done" );
  }
#endif /* !WINSIM */
  {
    mvs_state_control_init();

    mvs_current_g711_mode = MVS_G711_MODE_MULAW; //default to mulaw
    mvs_current_g722_mode = MVS_G722_MODE_MAX; //default g722 mode
  }
  {
#ifndef WINSIM
    /* Get eAMR config from vocsvc_nv service. */
    rc = vocsvc_nv_call( VOCSVC_NV_CALLINDEX_ENUM_GET_EAMR_CONFIG, 
                         &mvs_eamr_config, sizeof( mvs_eamr_config ) );
#else
    rc = APR_EFAILED;
#endif /* !WINSIM */
    if ( rc )
    { /* Not able to get eAMR config from vocsvc_nv, e.g. NV item file doesn't
       * exist or there are file read errors. eAMR will be disabled.
       */
      MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,  "mvs_mod_init(): Not able to get " \
          "eAMR config from voice NV,"
          "disabling eAMR by default." );
      mvs_eamr_config.enable_2g = FALSE;
      mvs_eamr_config.enable_3g = FALSE;
      mvs_eamr_config.enable_tdscdma = FALSE;
    }
  }
  
  {
#ifndef WINSIM
    /* Get eAMR config from voicecfg framework. */
    rc = mvs_eamr_mode_call( );
#else
    rc = APR_EFAILED;
#endif /* !WINSIM */
    if ( rc )
    { /* Not able to get eAMR config from voicecfg, e.g. Voice config item file 
       * doesn't exist or there are file read errors. eAMR will be disabled.
       */
      mvs_voicecfg_eamr_config.enable_2g = FALSE;	
      mvs_voicecfg_eamr_config.enable_3g = FALSE;	
      mvs_voicecfg_eamr_config.enable_tdscdma = FALSE;	
    }
  }
  
  { /* Create the MVS IST. */
    rc = apr_thread_create( &mvs_ist_handle, MVS_IST_NAME, MVS_IST_PRIORITY,
                            NULL, 0, mvs_ist, NULL );
    MVS_PANIC_ON_ERROR( rc );

    while ( mvs_ist_state != MVS_THREAD_STATE_ENUM_READY )
    {
      rc = apr_misc_sleep( 1000000 ); /* Sleep for 1ms. */
    }
  }

  {  /* Initialize the APR resource. */
    rc = __aprv2_cmd_local_dns_lookup(
           mvs_mvm_dns, sizeof( mvs_mvm_dns ), &mvs_mvm_addr );

    rc = __aprv2_cmd_local_dns_lookup(
           mvs_cvs_dns, sizeof( mvs_cvs_dns ), &mvs_cvs_addr );

    rc = __aprv2_cmd_register2(
           &mvs_my_apr_handle, mvs_my_dns, sizeof( mvs_my_dns ), 0,
           mvs_isr_dispatch_fn, NULL, &mvs_my_addr );
  }

#ifdef USE_MVS_PACKET_TIMING_ANALYSIS
  /* Intialize timing struct. */
  memset( &mvs_timing_info, 0, sizeof( mvs_timing_info ) );
#endif /* USE_MVS_PACKET_TIMING_ANALYSIS */

#ifndef WINSIM
  {  /* Register for system manager SSR callbacks. */
    ssr_handle = rcecb_register_context_name( SYS_M_SSR_ADSP_BEFORE_SHUTDOWN, (void *)mvs_ssr_adsp_before_shutdown );
    if ( ssr_handle == NULL ) MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, \
                                     "mvs_init: SSR Registration failed" );
  
    ssr_handle = rcecb_register_context_name( SYS_M_SSR_ADSP_AFTER_POWERUP, (void *)mvs_ssr_adsp_after_powerup );
    if ( ssr_handle == NULL ) MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  \
                                     "mvs_init: SSR Registration failed" );
  }
#endif /* !WINSIM */
  {  /* Create timer to be used for polling MVM after reset. */
    rc = apr_timer_create( &mvs_mvm_poll_timer, mvs_mvm_poll_timer_cb, NULL );
  }

  /* Initialize allocated objects queue. */
  ( void ) apr_list_init( &mvs_allocated_item_q, NULL, NULL );
  for ( i = 0; i < MVS_MAX_JOBS_V; ++i )
  {
    mvs_allocated_object_pool[i].handle = APR_NULL_V;
    ( void ) apr_list_add_tail(
                &mvs_allocated_item_q,
                ( ( apr_list_node_t* ) &mvs_allocated_object_pool[i] ) );
  }
  
  mvs_processing_time_info.is_enabled = FALSE;
  mvs_processing_time_info.max_time = 0;
  mvs_processing_time_info.min_time = -1;

  mvs_is_processing_time_check_enabled();

  mvs_is_first_call = TRUE;
  mvs_is_mvm_up = TRUE;
  mvs_adsp_crash = FALSE;
  mvs_is_allocated_objs_freed_on_ssr = TRUE;
  mvs_is_initialized = TRUE;
  mvs_cur_client = MVS_CLIENT_NONE;

  return APR_EOK;
}

static int32_t mvs_mod_post_init( void )
{
  return APR_EOK;
}

static int32_t mvs_mod_pre_deinit( void )
{
  int32_t rc;

  /* TODO: Ensure no race conditions on deregister. */

  /* TODO: The following must be moved into MVS thread context to guarantee
   *       that the responses come back before MVS shunts its incoming queue.
   *       MVS must shunt otherwise a memory leak occurs when MVS queues the
   *       incoming packet but does not process it. When in shunting state,
   *       MVS should fail all incoming commands coming from protocol
   *       clients.
   */

  rc = __aprv2_cmd_alloc_send(
         mvs_my_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         mvs_my_addr, MVS_PORT,
         mvs_my_addr, MVS_PORT,
         0,
         VSS_IMVS_CMD_SHUTDOWN, NULL, 0 );
  MVS_PANIC_ON_ERROR( rc );

  while ( mvs_is_initialized == TRUE )
  {
    rc = apr_misc_sleep( 1000000 ); /* Sleep for 1ms. */
  }

  return APR_EOK;
}

static int32_t mvs_mod_deinit ( void )
{
  int32_t rc;

  /* TODO: Ensure no race conditions on deregister. */

  { /* Release the APR resource. */
    rc = __aprv2_cmd_deregister( mvs_my_apr_handle );
  }
  { /* Destroy the MVS IST. */
    rc = apr_event_signal_abortall( mvs_ist_event );

    while ( mvs_ist_state != MVS_THREAD_STATE_ENUM_EXIT )
    {
      rc = apr_misc_sleep( 1000000 ); /* Sleep for 1ms. */
    }

    rc = apr_thread_destroy( mvs_ist_handle );
  }
  {
    rc = apr_timer_destroy( mvs_timer );
  }
  { /* Release the command queue management. */
    rc = apr_list_destroy( &mvs_pending_cmd_q );
    rc = apr_list_destroy( &mvs_incoming_cmd_q );
    rc = apr_list_destroy( &mvs_free_cmd_q );
  }
  { /* Release the session management. */
    rc = mvs_pending_control_destroy( &mvs_pending_ctrl );
  }
  { /* Release the object management. */
    rc = apr_objmgr_destruct( &mvs_objmgr );
  }
  { /* Release the locks. */
    rc = apr_lock_destroy( mvs_thread_lock );
    rc = apr_lock_destroy( mvs_int_lock );
  }

  { /* Print out debug info. on object sizes. */
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "apr_objmgr_object_t size = (%d)",
          sizeof( apr_objmgr_object_t ) );
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_object_t size = (%d)",
          sizeof( mvs_object_t ) );
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "mvs_mod_deinit rc = (%d)", rc );
  }

  return APR_EOK;
}

mvs_voc_rate_type mvs_mod_get_voc_max_rate ( void )
{
  return mvs_curr_props.max_voc_rate;
}

mvs_voc_rate_type mvs_mod_get_voc_min_rate ( void )
{
  return mvs_curr_props.min_voc_rate;
}

APR_EXTERNAL int32_t mvs_call (
  mvs_callindex_enum_t index,
  void* params,
  uint32_t size
)
{
  int32_t rc;

  switch ( index )
  {
  case MVS_CALLINDEX_ENUM_INIT:
    rc = mvs_mod_init( );
    break;

  case MVS_CALLINDEX_ENUM_POSTINIT:
    rc = mvs_mod_post_init( );
    break;

  case MVS_CALLINDEX_ENUM_PREDEINIT:
    rc = mvs_mod_pre_deinit( );
    break;

  case MVS_CALLINDEX_ENUM_DEINIT:
    rc = mvs_mod_deinit( );
    break;

  case MVS_CALLINDEX_ENUM_RUN:
    rc = mvs_mod_run( );
    break;

  default:
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "Unsupported callindex (%d)",
            index );
    rc = APR_EUNSUPPORTED;
    break;
  }

  return rc;
}

