#ifndef __MVSI_H__
#define __MVSI_H__

/*===========================================================================

                  M U L T I M O D E   V O I C E   S E R V I C E S

DESCRIPTION
  This header file defines function interfaces and corresponding data
  structures for invoking multi-mode voice services.

REFERENCES
  None.

Copyright (C) 2009 - 2014 QUALCOMM Technologies, Inc.
All rights reserved.
Qualcomm Technologies, Inc. Confidential and Proprietary.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/avs/vsd/common/mvs/inc/mvsi.h#1 $
$Author: mplp4svc $

===========================================================================*/

/*===========================================================================

                      INCLUDE FILES FOR MODULE

===========================================================================*/
//#include "queue.h"
#include "mvs.h"

#ifdef __cplusplus
extern "C" {
#endif

/*===========================================================================

                    D A T A   D E F I N I T I O N S

===========================================================================*/

/* MVS Command Queue Available Packets.
*/
typedef enum {
  MVS_CMD_ACQUIRE,
  MVS_CMD_ENABLE,
  MVS_CMD_STANDBY,
  MVS_CMD_LISTEN,
  MVS_CMD_RELEASE,
  MVS_CMD_MODE,
  MVS_CMD_UPDATE,
  MVS_CMD_TX,
  MVS_CMD_RX,
  MVS_CMD_SET_AMR_MODE,
  MVS_CMD_SET_AWB_MODE,
  MVS_CMD_SET_DTX_MODE,
  MVS_CMD_SET_CDMA_MINMAX_RATE,
  MVS_CMD_SET_CDMA_RATE_FACTOR,
  MVS_CMD_SSR,
  MVS_CMD_PROCESS_VOICEAPP,
  MVS_CMD_SEND_SILENCE_FRAME,
  MVS_CMD_INVALID
} mvs_cmd_type;

/* Header for MVS commands
*/
typedef struct
{
#if 0
  q_link_type            link;         /* Command queue link               */
#endif /* 0 */
  mvs_cmd_type           cmd;          /* MVS command to perform           */
} mvs_hdr_type;

/* Base packet type for MVS commands that use standard CB function and have
** no parameters.
*/
typedef struct
{
  mvs_hdr_type           hdr;          /* Command Header            */
  mvs_event_cb_type      cb_func;      /* Callback function pointer */
} mvs_base_packet_type;

typedef struct
{
  mvs_hdr_type           hdr;          /* Command Header            */
  mvs_event_cb_type      cb_func;      /* Callback function pointer */

  mvs_client_type        client;
} mvs_acquire_packet_type;

typedef struct
{
  mvs_hdr_type           hdr;          /* Command Header            */
  mvs_event_cb_type      cb_func;      /* Callback function pointer */

  mvs_client_type        client;
  mvs_mode_type          mode;
  mvs_packet_ul_fn_type  ul_func;
  mvs_packet_dl_fn_type  dl_func;
  mvs_pkt_context_type   context;
} mvs_enable_packet_type;

typedef struct
{
  mvs_hdr_type           hdr;          /* Command Header            */
  mvs_event_cb_type      cb_func;      /* Callback function pointer */

  mvs_client_type        client;
  mvs_mode_type          mode;
} mvs_standby_packet_type;

typedef struct
{
  mvs_hdr_type           hdr;          /* Command Header            */
  mvs_event_cb_type      cb_func;      /* Callback function pointer */

  mvs_client_type        client;
  mvs_listen_fn_type     ul_func;
  mvs_listen_fn_type     dl_func;
} mvs_listen_packet_type;

typedef struct
{
  mvs_hdr_type           hdr;          /* Command Header            */
  mvs_event_cb_type      cb_func;      /* Callback function pointer */

  mvs_client_type        client;
} mvs_release_packet_type;

typedef struct
{
  mvs_hdr_type           hdr;          /* Command Header            */
  mvs_event_cb_type      cb_func;      /* Callback function pointer */

  mvs_mode_type          mode;
} mvs_mode_packet_type;

typedef struct
{
  mvs_hdr_type           hdr;          /* Command Header            */
  mvs_event_cb_type      cb_func;      /* Callback function pointer */

  mvs_client_type        client;
  mvs_frame_q_type       *ul_qptr;     /* uplink voice frame buffer */
  mvs_packet_ul_fn_type  ul_func;      /* uplink callback function */
} mvs_tx_packet_type;

typedef struct
{
  mvs_hdr_type           hdr;          /* Command Header            */
  mvs_event_cb_type      cb_func;      /* Callback function pointer */
  mvs_client_type        client;
  mvs_frame_q_type       *dl_qptr;     /* downlink voice frame buffer */
  mvs_packet_dl_fn_type  dl_func;      /* downlink callback function */
} mvs_rx_packet_type;

typedef struct
{
  mvs_hdr_type           hdr;          /* Command Header            */
  mvs_event_cb_type      cb_func;      /* Callback function pointer */

  mvs_amr_mode_type      amr_mode;
} mvs_set_amr_mode_packet_type;

typedef struct
{
  mvs_hdr_type           hdr;          /* Command Header            */
  mvs_event_cb_type      cb_func;      /* Callback function pointer */

  mvs_dtx_mode_type      dtx_mode;
} mvs_set_dtx_mode_packet_type;

typedef struct
{
  mvs_hdr_type           hdr;          /* Command Header            */
  mvs_event_cb_type      cb_func;      /* Callback function pointer */

  mvs_voc_rate_type      max_rate;
  mvs_voc_rate_type      min_rate;
} mvs_set_cdma_minmax_rate_packet_type;

typedef struct
{
  mvs_hdr_type           hdr;          /* Command Header            */
  mvs_event_cb_type      cb_func;      /* Callback function pointer */

  uint16                 rate_factor;
} mvs_set_cdma_rate_factor_type;

typedef struct
{
  mvs_hdr_type hdr;
  uint32_t vsid;
} mvs_process_voiceapp;

typedef union {
  mvs_hdr_type hdr;
  mvs_base_packet_type base;      /* Base MVS packet type */
  mvs_acquire_packet_type acquire;
  mvs_enable_packet_type enable;
  mvs_standby_packet_type standby;
  mvs_listen_packet_type listen;
  mvs_release_packet_type release;
  mvs_mode_packet_type mode;
  mvs_tx_packet_type tx;
  mvs_rx_packet_type rx;
  mvs_set_amr_mode_packet_type set_amr_mode;
  mvs_set_dtx_mode_packet_type set_dtx_mode;
  mvs_set_cdma_minmax_rate_packet_type set_cdma_minmax_rate;
  mvs_set_cdma_rate_factor_type set_cdma_rate_factor;
  mvs_process_voiceapp process_voiceapp;
} mvs_packet_type;

/*===========================================================================

F U N C T I O N   D E F I N I T I O N S

===========================================================================*/

/*===========================================================================

FUNCTION mvs_init

DESCRIPTION
  This procedure initializes the MVS command queues.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  mvs_cmd_q
  mvs_free_q
  mvs_free_packet

===========================================================================*/
extern void mvs_init ( void );

#ifdef __cplusplus
}
#endif

#endif  /* __MVSI_H__ */

