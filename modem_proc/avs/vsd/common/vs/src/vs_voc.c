/**
  @file vs_voc.c
  @brief This file contains the definitions of the internal functions of VS.
   
*/

/*
   Copyright (C) 2013-2015 Qualcomm Technologies, Inc.
   All Rights Reserved.
   Confidential and Proprietary - Qualcomm Technologies, Inc.

   $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/avs/vsd/common/vs/src/vs_voc.c#3 $
   $Author: shrakuma $
 
  when      who   what, where, why
  --------  ---   ------------------------------------------------------------
  08/05/14  sre   Added support for enabing eAMR over VS.
  06/04/14  shr   Sending MVM stop command during handovers instead on MVM standby.
  12/09/13  js    Added support for clearing the CVD sessions, after the end of 
                  each call.
  10/18/13  rm    Replaced apr_list_init() with apr_list_init_v2().
                  Added vs_voc_cmd_thread_lock to make vs_voc_cmd_open thread
                  safe.  
  09/20/13  sre   Fixed the banned QPSI memcpy APIs.
  08/09/13  sud   Renamed macro PC_ENV_SET to WINSIM.
  07/30/13  sud   Updated software to report MSG_FATAL when fatal error 
                  conditions occurs in client context.
  07/10/13  sg    Fixed the KW errors.
  06/29/13  sg    Fixed the banned QPSI memcpy APIs.  
  06/26/13  sud   Updated debug messages and rearranged some of the functions
  06/16/13  sud   Updated VS to handle failed CVD command and to process 
                  subsequent commands.
  06/13/13  hl    Added support for handling ADSP SSR.
  06/12/13  sud   Updated VS to handle errors and status propagation to client.
                  Updated VS to handle memory leak and memory corruption
                  issues. 
                  Updated VS to handle command priorities and proper sequencing.
                  Updated VS to add proper debug messages
  06/01/13  sud   Fixed compiler warnings.
  05/16/13  sg    Fixed bug in close cmd
  04/16/13  sud   VS refactored code and fixed bugs. 
  01/12/13  sg    Initial revision

*/

/****************************************************************************
 * HEADER FILES                                                     *
 ****************************************************************************/

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#include "err.h"
#include "msg.h"

#ifndef WINSIM
#include "sys_m_messages.h"
#include "rcecb.h"
#endif /* !WINSIM */

#include "mmstd.h"

#include "apr_event.h"
#include "apr_thread.h"
#include "apr_timer.h"
#include "vss_public_if.h"
#include "vss_private_if.h"
#include "voicecfg_api.h"
#include "voicecfg_items_i.h"
#include "vs_i.h"


/****************************************************************************
 * DEFINES                                                                  *
 ****************************************************************************/
/* Time in ns for 10ms for periodic polling of CVD (MVM)'s readiness after 
 * ADSP SSR. 
 */
#define VS_VOC_SSR_CVD_POLL_TIMER_NS ( 10000000 )

/* Name of CVD session created by VS for multimode voice system.
 * CVD session name is not changed to maintain backwards 
 * compatability. 
 */
static char_t vs_default_stream_name[] = "default modem voice"; 

/* Name of CVD session created by VS based off VSID */

#if 0
/* Multimode voice system - not used as of now */
static char_t vs_vsid_cs_voice_mm_stream_name[] = "10C01000";
#endif 
  /* GSM only voice system (GSM2) */
static char_t vs_vsid_cs_voice_gsm2_stream_name[] = "10DC1000";

/* Timer used for polling CVD (MVM) after ADSP SSR for its readiness. */
static apr_timer_t vs_voc_ssr_cvd_poll_timer;

/* Used to track the job object created for polling CVD (MVM) for its readiness
 * after ADSP SSR. 
 */
static vs_simple_job_object_t* vs_voc_ssr_cvd_poll_job_obj;

static bool_t vs_voc_is_cvd_ready;
static bool_t vs_voc_is_ssr_cleanup_done;

/* Flag used to abort polling CVD (MVM) for its readiness. */
static bool_t vs_voc_is_abort_cvd_poll;

/* Flag to indicate if eAMR vocoder can be enabled or not. */
bool_t vs_voicecfg_is_eamr_enabled = FALSE;

/* The following are used to track the VS objects with type 
 * VS_OBJECT_TYPE_ENUM_SIMPLE_JOB and VS_OBJECT_TYPE_ENUM_HANDLE, in order to 
 * free them in case of ADSP SSR. 
 */
static apr_list_t vs_voc_ssr_obj_tracking_free_q;
static apr_list_t vs_voc_ssr_obj_tracking_used_q;
static vs_generic_item_t vs_voc_ssr_obj_tracking_pool[ VS_MAX_OBJECTS_V ];

/****************************************************************************
 * EXTERN DECLARATIONS                                                      *
 ****************************************************************************/
extern uint16_t vs_my_addr;
extern uint16_t vs_mvm_addr;
extern uint16_t vs_cvs_addr;

extern apr_lock_t vs_voc_cmd_thread_lock;

extern apr_list_t vs_free_session_q;
extern apr_list_t vs_used_session_q;

extern apr_list_t vs_priority1_packet_q;
extern apr_list_t vs_priority2_packet_q;
extern apr_list_t vs_priority3_packet_q;

extern bool_t vs_is_initialized;
extern bool_t vs_is_adsp_up_curr; /* TODO: clean this up. */

extern uint32_t vs_apr_handle;
extern apr_objmgr_t vs_objmgr;

extern apr_memmgr_type vs_heapmgr;


/****************************************************************************
 * VS COMMAND AND RESPONSE PROCESSING ROUTINES                             *
 ****************************************************************************/
static void vs_voc_create_mvm_session_result_rsp_fn (
  aprv2_packet_t* p_packet
)
{

  uint32_t rc = VS_EOK;
  vs_simple_job_object_t* p_job_obj = NULL;
  vs_session_object_t* p_session_obj = NULL;
  
  if ( p_packet != APR_NULL_V )
  {
    rc = vs_get_typed_object( p_packet->token, VS_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                               ( ( vs_object_t** ) &p_job_obj ) );
    VS_PANIC_ON_ERROR( rc );
    p_job_obj->is_completed = TRUE;
    p_job_obj->status = 
      APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, p_packet )->status;

    rc = vs_get_typed_object( p_job_obj->context_handle, 
                              VS_OBJECT_TYPE_ENUM_SESSION,
                              ( ( vs_object_t** ) &p_session_obj ) );
    VS_PANIC_ON_ERROR( rc );
    p_session_obj->session_ctrl.status = p_job_obj->status;

    if ( p_session_obj->session_ctrl.status == VS_EOK )
    {
      p_session_obj->mvm_port = p_packet->src_port;
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,  "MVM_PORT (%d)  set for " \
            "session_handle=(0x%08x)",
            p_session_obj->mvm_port, p_session_obj->header.handle );
    }
    else
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
              "vs_create_mvm_session_rsp_result_fn(): "
              "command (0x%08x) failed with rc=(0x%08x)",
            APRV2_PKT_GET_PAYLOAD(aprv2_ibasic_rsp_result_t,p_packet)->opcode,
              p_job_obj->status  );
    }

    rc = __aprv2_cmd_free( vs_apr_handle, p_packet );
    VS_PANIC_ON_ERROR( rc );
    p_packet = APR_NULL_V;
  }
  else
  {
    VS_PANIC_ON_ERROR( VS_EUNEXPECTED );
  }

  vs_signal_run();

}  /* end of vs_voc_create_mvm_session_result_rsp_fn () */

static void vs_voc_create_cvs_session_result_rsp_fn (
  aprv2_packet_t* p_packet
)
{

  uint32_t rc = VS_EOK;
  vs_simple_job_object_t* p_job_obj = NULL;
  vs_session_object_t* p_session_obj = NULL;

  if ( p_packet != APR_NULL_V )
  {
    rc = vs_get_typed_object( p_packet->token, 
                              VS_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                              ( ( vs_object_t** ) &p_job_obj ) );
    VS_PANIC_ON_ERROR( rc );
    p_job_obj->is_completed = TRUE;
    p_job_obj->status = 
      APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, p_packet )->status;

    rc = vs_get_typed_object( p_job_obj->context_handle, 
                              VS_OBJECT_TYPE_ENUM_SESSION,
                              ( ( vs_object_t** ) &p_session_obj ) );
    VS_PANIC_ON_ERROR( rc );
    p_session_obj->session_ctrl.status = p_job_obj->status;

    if ( p_session_obj->session_ctrl.status == VS_EOK )
    {
      p_session_obj->cvs_port = p_packet->src_port;
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "CVS_PORT (%d)  set for " \
             "session_handle=(0x%08x)",
             p_session_obj->cvs_port, p_session_obj->header.handle );
    }
    else
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
              "vs_create_cvs_session_rsp_result_fn(): "
              "command (0x%08x) failed with rc=(0x%08x)",
            APRV2_PKT_GET_PAYLOAD(aprv2_ibasic_rsp_result_t,p_packet)->opcode,
              p_job_obj->status );
    }
    rc = __aprv2_cmd_free( vs_apr_handle, p_packet );
    VS_PANIC_ON_ERROR( rc );
    p_packet = APR_NULL_V;
  }
  else
  {
    VS_PANIC_ON_ERROR( VS_EUNEXPECTED );
  }
  vs_signal_run();

}  /* end of vs_voc_create_cvs_session_result_rsp_fn () */

static void vs_voc_ssr_cvd_poll_result_rsp_fn (
  aprv2_packet_t* packet
)
{

  int32_t rc;
  int32_t status;

  status = APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet )->status;
  if ( status == APR_EUNSUPPORTED )
  {
    /* This is a temporary workaround to detect that CVD (MVM) is ready after
     * ADSP SSR. MVM is the last service in CVD that get initialized. Therefore
     * after MVM is ready, it means that CVS and CVP are also ready.
     * MVM does not support the command APR_CMDID_GET_VERSION. And it will
     * return APR_EUNSUPPORTED. Once we get this response from MVM, we know
     * that MVM is ready after ADSP SSR.
     * In the future, we need to query APR for individual service's readiness
     * once APR has such support.
     */
    vs_voc_is_cvd_ready = TRUE;
    MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,   \
        "vs_voc_ssr_cvd_poll_result_rsp_fn(): "
        "Detected CVD is ready." );
  }

  rc = __aprv2_cmd_free( vs_apr_handle, packet );
  VS_PANIC_ON_ERROR( rc );

  vs_signal_run();

}
/****************************************************************************
 * VS COMMAND AND RESPONSE PROCESSING ROUTINES... END                       *
 ****************************************************************************/

static uint32_t vs_voc_create_session_object (
  vs_session_object_t** p_ret_session_obj
)
{

  uint32_t rc = VS_EOK;
  uint32_t checkpoint = 0;
  vs_session_object_t* p_session_obj = NULL;

  for ( ;; )
  {

    if ( p_ret_session_obj == NULL )
    {
      rc = VS_EBADPARAM;
      break;
    }

    rc = vs_mem_alloc_object( sizeof( vs_session_object_t ),
                               ( ( vs_object_t** ) &p_session_obj ) );
    if ( rc ) 
    {
      VS_REPORT_FATAL_ON_ERROR( rc );
      break;
    }
    checkpoint = 1;

    /* Initialize the session object. */
    { 
      p_session_obj->vsid = VS_VSID_UNDEFINED_V;
      p_session_obj->client_id = VS_VOC_CLIENT_UNDEFINED_V;
      p_session_obj->media_id = VS_COMMON_MEDIA_ID_UNDEFINED_V;
      p_session_obj->event_cb = NULL;
      p_session_obj->session_context = NULL;
      p_session_obj->pending_cmd = VS_VOC_CMD_UNDEFINED_V;
      p_session_obj->is_configured = FALSE;

      rc = apr_list_init_v2( &p_session_obj->alloc_buf_q, vs_int_lock_fn, 
                           vs_int_unlock_fn );

      rc = apr_list_init_v2( &p_session_obj->read_buf_q, vs_int_lock_fn, 
                           vs_int_unlock_fn );

      rc = apr_list_init_v2( &p_session_obj->write_buf_q, vs_int_lock_fn, 
                           vs_int_unlock_fn );

      ( void ) mmstd_memset( (void*)(&p_session_obj->cvd_session_name), 0,
                           sizeof( p_session_obj->cvd_session_name ) );
      
      p_session_obj->state= VS_VOC_STATE_ENUM_RESET;  

      /* Initialize voice timing settings */
      p_session_obj->voice_timing.vfr_mode = VS_VOC_VFR_MODE_SOFT_V; 
      p_session_obj->voice_timing.vsid = VS_VSID_UNDEFINED_V;
      p_session_obj->voice_timing.enc_offset = VS_VOC_TIMING_GSM_ENC_OFFSET_V;
      p_session_obj->voice_timing.dec_req_offset = 
                                         VS_VOC_TIMING_GSM_DEC_REQ_OFFSET_V;
      p_session_obj->voice_timing.dec_offset = VS_VOC_TIMING_GSM_DEC_OFFSET_V;

      /* Initialize vocoder properties - current */
      p_session_obj->current_voc.amr.codec_mode = 
                                              VS_VOCAMR_CODEC_MODE_UNDEFINED_V;
      p_session_obj->current_voc.amr.dtx_enable = VS_VOC_DTX_MODE_UNDEFINED_V;

      p_session_obj->current_voc.amrwb.codec_mode = 
                                            VS_VOCAMRWB_CODEC_MODE_UNDEFINED_V;
      p_session_obj->current_voc.amrwb.dtx_enable = VS_VOC_DTX_MODE_UNDEFINED_V;

      p_session_obj->current_voc.efr.dtx_enable = VS_VOC_DTX_MODE_UNDEFINED_V;
      p_session_obj->current_voc.fr.dtx_enable = VS_VOC_DTX_MODE_UNDEFINED_V;
      p_session_obj->current_voc.hr.dtx_enable = VS_VOC_DTX_MODE_UNDEFINED_V;

      /* Initialize vocoder properties - pending */
      p_session_obj->pending_voc.amr.codec_mode = 
                                           VS_VOCAMR_CODEC_MODE_UNDEFINED_V;
      p_session_obj->pending_voc.amr.dtx_enable = VS_VOC_DTX_MODE_UNDEFINED_V;

      p_session_obj->pending_voc.amrwb.codec_mode = 
                                           VS_VOCAMRWB_CODEC_MODE_UNDEFINED_V;
      p_session_obj->pending_voc.amrwb.dtx_enable = VS_VOC_DTX_MODE_UNDEFINED_V;

      p_session_obj->pending_voc.efr.dtx_enable = VS_VOC_DTX_MODE_UNDEFINED_V;
      p_session_obj->pending_voc.fr.dtx_enable = VS_VOC_DTX_MODE_UNDEFINED_V;
      p_session_obj->pending_voc.hr.dtx_enable = VS_VOC_DTX_MODE_UNDEFINED_V;

      p_session_obj->session_ctrl.transition_job_handle = APR_NULL_V;
      p_session_obj->session_ctrl.pendjob_handle = APR_NULL_V;
      p_session_obj->session_ctrl.pendjob_seq_ptr = NULL;
      p_session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;
    
      p_session_obj->stream_ready = FALSE;
      p_session_obj->stream_standby = FALSE;
      p_session_obj->is_first_call = TRUE;
    }

    /* Complete initialization. */
    p_session_obj->header.type = VS_OBJECT_TYPE_ENUM_SESSION;
      /* Mark the actual object type here to indicate that the object has been
       * fully initialized. Otherwise, on an error the destructor called at
       * the checkpoint handler would inadvertently try to free resources in
       * the object that have not been allocated yet or have already been
       * freed by the clean up sequence.
       */

    *p_ret_session_obj = p_session_obj;

    return VS_EOK;
  }  /* end of for ( ;; ) */

  switch ( checkpoint )
  {
  case 1:
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_create_session_object(): " \
            "failed, checkpoint=(%d)",
            checkpoint );
    rc = vs_mem_free_object( ( vs_object_t* ) p_session_obj );
    VS_REPORT_FATAL_ON_ERROR( rc );
    p_session_obj = NULL;
    /*-fallthru */
  default:
    break;
  }

  return rc;

}  /* end of vs_voc_create_session_object() */


/****************************************************************************
 * VS CORE IMPLEMENTATION ,Interaction with CVD                             
 ****************************************************************************/
static uint32_t vs_voc_map_to_cvd_mode (
  uint32_t vs_media_id
)
{

  uint32_t media_id = VSS_MEDIA_ID_NONE; 
  
  switch ( vs_media_id )
  {
  case VS_COMMON_MEDIA_ID_UNDEFINED_V: 
    {
      media_id = VSS_MEDIA_ID_NONE;
    }
    break;

  case VS_COMMON_MEDIA_ID_AMR:
    {
      if ( vs_voicecfg_is_eamr_enabled == TRUE )
      {
        media_id = VSS_MEDIA_ID_EAMR;
      }
      else
      {
        media_id = VSS_MEDIA_ID_AMR_NB_MODEM;
      }
    }
    break;

  case VS_COMMON_MEDIA_ID_AMRWB:
    {
      media_id = VSS_MEDIA_ID_AMR_WB_MODEM;
    }
    break;

  case VS_COMMON_MEDIA_ID_EFR:
    {
      media_id = VSS_MEDIA_ID_EFR_MODEM;
    }
    break;

  case VS_COMMON_MEDIA_ID_FR:
    {
      media_id = VSS_MEDIA_ID_FR_MODEM;
    }
    break;

  case VS_COMMON_MEDIA_ID_HR:
    {
      media_id = VSS_MEDIA_ID_HR_MODEM;
    }
    break;

  default:
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "Received vs_media_id not " \
          "supported currently..." );
    media_id = VSS_MEDIA_ID_NONE;
    break;

  }

  return media_id;

}  /* end of vs_voc_map_to_cvd_mode () */

static uint32_t vs_voc_map_to_cvd_network (
  uint32_t client
)
{

  uint32_t network_id;

  switch ( client )
  {
  case VS_VOC_CLIENT_GSM:
    {
        network_id = VSS_ICOMMON_CAL_NETWORK_ID_GSM;
    }
    break;

  default:
    network_id = VSS_ICOMMON_CAL_NETWORK_ID_NONE;
    break;
  }

  return network_id;

}  /* end of vs_map_to_cvd_network () */

static uint32_t vs_voc_set_voice_timing_proc (
  uint32_t  handle
)
{

  uint32_t rc = VS_EOK;
  vss_icommon_cmd_set_voice_timing_v2_t timing_param_v2;
  vs_token_object_t* p_token_obj = NULL;  
  vs_session_object_t* p_session_obj = NULL;

   /*get the session object based on handle*/
  rc = vs_get_typed_object ( handle, VS_OBJECT_TYPE_ENUM_SESSION, 
                             ( ( vs_object_t** ) &p_session_obj ) );
  VS_PANIC_ON_ERROR( rc );

  timing_param_v2.mode = p_session_obj->voice_timing.vfr_mode;
  timing_param_v2.vsid = p_session_obj->voice_timing.vsid; 
  timing_param_v2.enc_offset = p_session_obj->voice_timing.enc_offset;
  timing_param_v2.dec_req_offset = p_session_obj->voice_timing.dec_req_offset;
  timing_param_v2.dec_offset = p_session_obj->voice_timing.dec_offset;;

  MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED,  "vs_voc_set_voice_timing_proc()-  "
        "handle=(0x%08x), vsid=(0x%08x), vfr_mode=(%d)",
        handle, timing_param_v2.vsid, timing_param_v2.mode );
  MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED,  "vs_voc_set_voice_timing_proc()-  "
        "enc_off=(0x%08x), dec_req_off=(0x%08x), dec_off=(0x%08x)",
        timing_param_v2.enc_offset,
        timing_param_v2.dec_req_offset,
        timing_param_v2.dec_offset );

  rc = vs_create_autofree_token_object( &p_token_obj );
  VS_PANIC_ON_ERROR( rc );

  rc = __aprv2_cmd_alloc_send(
         vs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         vs_my_addr, ( (uint16_t) p_session_obj->header.handle ),
         vs_mvm_addr, p_session_obj->mvm_port,
         p_token_obj->header.handle,
         VSS_ICOMMON_CMD_SET_VOICE_TIMING_V2,
         &timing_param_v2, sizeof( timing_param_v2 ) );
  if ( rc )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_voc_set_voice_timing_proc(): " \
            "Faield to communicate with"
            " MVM with rc=(0x%08x)", rc );

    rc = vs_mem_free_object( ( vs_object_t* ) p_token_obj );
    VS_PANIC_ON_ERROR( rc );
  }

  return VS_EOK;

}  /* end of vs_voc_set_voice_timing_proc() */

/* Set network on MVM to be used for calibrating the attached
 * stream/vocproc.
 */
static uint32_t vs_voc_set_mvm_network_proc ( 
  uint32_t handle
)
{

  uint32_t rc = VS_EOK;
  vss_imvm_cmd_set_cal_network_t set_network;
  vs_token_object_t* p_token_obj = NULL;
  vs_session_object_t* p_session_obj = NULL;
  
  /*get the session object based on handle*/
  rc = vs_get_typed_object( handle,VS_OBJECT_TYPE_ENUM_SESSION, 
                           ( ( vs_object_t** ) &p_session_obj ) );
  VS_PANIC_ON_ERROR( rc );

  set_network.network_id = vs_voc_map_to_cvd_network( p_session_obj->client_id );

  MSG_3( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "vs_voc_set_mvm_network_proc() - "
        "handle=(0x%08x), client=(0x%08x), network_id=(0x%08x)",
        handle, p_session_obj->client_id, set_network.network_id );

  rc = vs_create_autofree_token_object( &p_token_obj );
  VS_PANIC_ON_ERROR( rc );

  rc = __aprv2_cmd_alloc_send(
         vs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         vs_my_addr, ( (uint16_t) p_session_obj->header.handle ),
         vs_mvm_addr, p_session_obj->mvm_port,
         p_token_obj->header.handle, VSS_IMVM_CMD_SET_CAL_NETWORK,
         &set_network, sizeof( set_network ) );
  if ( rc )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_voc_set_mvm_network_proc(): " \
            "Faield to communicate with"
            " MVM with rc=(0x%08x)", rc );

    rc = vs_mem_free_object( ( vs_object_t* ) p_token_obj );
    VS_PANIC_ON_ERROR( rc );
  }

  return VS_EOK;

}  /* end of vs_voc_set_mvm_network_proc() */

static uint32_t vs_voc_set_mvm_media_type_proc (
  uint32_t handle
)
{

  uint32_t rc = VS_EOK;
  vss_imvm_cmd_set_cal_media_type_t set_media_type;
  vs_token_object_t* p_token_obj = NULL;
  vs_session_object_t* p_session_obj = NULL;

    /*get the session object based on handle*/
  rc = vs_get_typed_object( handle,VS_OBJECT_TYPE_ENUM_SESSION, 
                           ( ( vs_object_t** ) &p_session_obj ) );
  VS_PANIC_ON_ERROR( rc );

  set_media_type.media_id = vs_voc_map_to_cvd_mode( p_session_obj->media_id );

  MSG_3( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "vs_voc_set_mvm_media_type_proc() - "
        "handle=(0x%08x), client=(0x%08x), media_id=(0x%08x)",
        handle, p_session_obj->client_id, set_media_type.media_id );

  rc = vs_create_autofree_token_object( &p_token_obj );
  VS_PANIC_ON_ERROR( rc );

  rc = __aprv2_cmd_alloc_send(
         vs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         vs_my_addr, ( (uint16_t) p_session_obj->header.handle ),
         vs_mvm_addr, p_session_obj->mvm_port,
         p_token_obj->header.handle, VSS_IMVM_CMD_SET_CAL_MEDIA_TYPE,
         &set_media_type, sizeof( set_media_type ) );
  if ( rc )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
            "vs_voc_set_mvm_media_type_proc(): Faield to communicate with "
            "MVM with rc=(0x%08x)", rc );

    rc = vs_mem_free_object( ( vs_object_t* ) p_token_obj );
    VS_PANIC_ON_ERROR( rc );
  }

  return VS_EOK;

}  /* end of vs_voc_set_mvm_media_type_proc() */


/****************************************************************************
 * VS state machine functions                              
 ****************************************************************************/

static uint32_t vs_voc_state_reset (
  vs_session_object_t* p_session_obj
)
{

  if ( p_session_obj->pending_cmd == VS_VOC_CMD_CLOSE )
  {
    p_session_obj->state = VS_VOC_STATE_ENUM_RESET;
    MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "VS_VOC_STATE_ENUM_RESET" );
    return VS_ECONTINUE;
  }
  else if ( vs_is_adsp_up_curr )
  {
    p_session_obj->state = VS_VOC_STATE_ENUM_IDLE;
    MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "VS_VOC_STATE: RESET to IDLE " \
        "transition" );
    return VS_EIMMEDIATE;
  }
  else
  {
    p_session_obj->state = VS_VOC_STATE_ENUM_RESET;
    MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "VS_VOC_STATE_ENUM_RESET");
    return VS_ECONTINUE;
  }

}  /* end of vs_voc_state_reset() */

static uint32_t vs_voc_state_idle (
  vs_session_object_t* p_session_obj
)
{

  if ( p_session_obj->pending_cmd == VS_VOC_CMD_CLOSE )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "VS_VOC_STATE: IDLE to RESET " \
        "transition" );
    p_session_obj->state = VS_VOC_STATE_ENUM_RESET;
    return VS_EIMMEDIATE;
  }
  else if ( p_session_obj->stream_ready )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "VS_VOC_STATE: IDLE to RUN " \
        "transition" );
    p_session_obj->state = VS_VOC_STATE_ENUM_RUN;
    return VS_EIMMEDIATE;
  }
  else
  { 
    return VS_ECONTINUE;
  }

}  /* end of vs_voc_state_idle() */

static uint32_t vs_voc_state_run (
  vs_session_object_t* p_session_obj
)
{

  if ( !( p_session_obj->stream_ready ) || 
       !( vs_is_adsp_up_curr ) ||
        ( p_session_obj->pending_cmd == VS_VOC_CMD_CLOSE ) )
  {
    if ( p_session_obj->stream_ready )
    {
      p_session_obj->stream_ready = FALSE;
    }

    if ( p_session_obj->pending_cmd == VS_VOC_CMD_CLOSE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "VS_VOC_STATE_ENUM_RUN - " \
            "incorrect API sequence."
            "CLOSE called before vocoder resources are released" );
      /* Client does not follow correct API sequence.
       * DISABLE the vocoder, free all the allocated buffers and then
       * performs actions for CLOSE 
       * <TBD> 
       */ 
    }
    MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "VS_VOC_STATE: RUN to IDLE " \
        "transition");
    p_session_obj->state = VS_VOC_STATE_ENUM_IDLE;
    return VS_EIMMEDIATE;
  }
  else
  {
    p_session_obj->state = VS_VOC_STATE_ENUM_RUN;
    MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "VS_VOC_STATE_ENUM_RUN");
    return VS_ECONTINUE;
  }
  
}  /* end of vs_voc_state_run() */

VS_INTERNAL uint32_t vs_voc_state_control (
  vs_session_object_t* p_session_obj
)
{
  uint32_t rc = VS_EOK;

  if ( p_session_obj == NULL )
  {
    VS_PANIC_ON_ERROR( VS_EUNEXPECTED );
  }

  do
  {
    switch ( p_session_obj->state )
    {
    case VS_VOC_STATE_ENUM_RESET:
      rc = vs_voc_state_reset( p_session_obj );
      break;

    case VS_VOC_STATE_ENUM_IDLE:
      rc = vs_voc_state_idle( p_session_obj );
      break;

    case VS_VOC_STATE_ENUM_RUN:
      rc = vs_voc_state_run( p_session_obj );
      break;

    default:
      rc = VS_EUNEXPECTED;
      break;
    }
  }
  while ( rc == VS_EIMMEDIATE );

  return rc;

}  /* end of vs_voc_state_control() */

VS_INTERNAL uint32_t vs_voc_run_state_control (
  void
)
{

  uint32_t rc = VS_EOK;
  vs_session_object_t* p_session_obj = NULL;
  vs_generic_item_t* p_generic_item = NULL;

  p_generic_item = ( ( vs_generic_item_t* ) &vs_used_session_q.dummy );

  for ( ;; )
  {
    rc = apr_list_get_next( &vs_used_session_q,
                            ( ( apr_list_node_t* ) p_generic_item ),
                            ( ( apr_list_node_t** ) &p_generic_item ) );
    if ( rc ) 
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR ,  "SSR:session_q empty or " \
               "iteration complete, rc=(0x%08x)",
               rc );
      break;
    }

    rc = vs_get_typed_object( p_generic_item->handle,
                              VS_OBJECT_TYPE_ENUM_SESSION,
                              ( ( vs_object_t** ) &p_session_obj ) );
    VS_PANIC_ON_ERROR( rc );

    vs_voc_state_control( p_session_obj );

  }  /* for( ;; )*/

  return rc;

}  /*  end of vs_voc_run_state_control() */
/****************************************************************************
 * VS state machine functions...END                                         *
 ****************************************************************************/


/****************************************************************************
 * VS Asynchronous non-blocking API functions...START                       *
 ****************************************************************************/
VS_INTERNAL uint32_t vs_voc_cmd_enable (
  vs_voc_cmd_enable_t* params
)
{

  uint32_t rc = VS_EOK;
  vs_cmd_packet_t* p_enable_cmd_pkt = NULL;
  vs_voc_cmd_enable_t* p_args = NULL;
  vs_session_object_t* p_session_obj = NULL;
  uint32_t checkpoint = 0;

  if ( params == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_voc_cmd_enable() - NULL " \
          "params" );
    rc = VS_EUNEXPECTED;
    return rc;
  }

  /* validate the session handle */ 
  rc = vs_get_typed_object( params->handle, VS_OBJECT_TYPE_ENUM_SESSION,
                            ( ( vs_object_t** ) &p_session_obj ) ); 
  if ( rc != VS_EOK )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_voc_cmd_enable() - Invalid " \
            "handle=(0x%08x)",
            params->handle );
    rc = VS_EBADPARAM;
    return rc;
  }

  for ( ;; )
  {
    p_args = ( vs_voc_cmd_enable_t* ) apr_memmgr_malloc( 
                                      &vs_heapmgr,
                                      sizeof( vs_voc_cmd_enable_t ) );
    if( p_args == NULL )
    { 
      rc = VS_ENORESOURCE;
      VS_REPORT_FATAL_ON_ERROR( rc );
      break;
    }
    p_args->handle = params->handle;
    p_args->media_id = params->media_id;
    p_args->client_context = params->client_context;
    checkpoint = 1;

    /* Prepare command packet for VS_VOC_CMD_ENABLE. */
    p_enable_cmd_pkt = ( vs_cmd_packet_t* ) apr_memmgr_malloc( 
                                      &vs_heapmgr,
                                      sizeof( vs_cmd_packet_t ) );
    if( p_enable_cmd_pkt == NULL )
    { 
      rc = VS_ENORESOURCE;
      VS_REPORT_FATAL_ON_ERROR( rc );
      break;
    }
    p_enable_cmd_pkt->cmd_id = VS_VOC_CMD_ENABLE;
    p_enable_cmd_pkt->params = ( void* )p_args;
    checkpoint = 2;

    /* Queue the command packet for processing */
    rc = vs_queue_incoming_vs_command_packet( p_enable_cmd_pkt );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_voc_cmd_enable() - cmd pkt " \
              "queuing failed. "
              "rc=(0x%08x)", rc );
    }
    else
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,  "vs_voc_cmd_enable() cmd pkt " \
            "queued: "
            "handle=(0x%08x), media_id=(0x%08x)",
            params->handle, params->media_id );
      checkpoint = 3;
    }
    break;

  } /* end of for ( ;; ) */

  switch ( checkpoint )
  {
  case 3:
    break;

  case 2:
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_voc_cmd_enable() - failed, " \
              "checkpoint=(%d)",
              checkpoint );
      if ( p_enable_cmd_pkt != NULL )
      {
        apr_memmgr_free( &vs_heapmgr, p_enable_cmd_pkt );
        p_enable_cmd_pkt = NULL;
      }
    }
    /*-fallthru */

  case 1:
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_voc_cmd_enable(): failed, " \
              "checkpoint=(%d)",
              checkpoint );
      if ( p_args != NULL )
      {
        apr_memmgr_free( &vs_heapmgr, p_args );
        p_args = NULL;
      }
    }
    /*-fallthru */

  case 0:
  default:
    break;

  }  /* end of switch ( checkpoint ) */

  return rc;

}  /* end of vs_voc_cmd_enable () */

VS_INTERNAL uint32_t vs_voc_cmd_disable (
  vs_voc_cmd_disable_t* params
)
{

  uint32_t rc = VS_EOK;
  vs_cmd_packet_t* p_disable_cmd_pkt = NULL;
  vs_voc_cmd_disable_t* p_args = NULL;
  vs_session_object_t* p_session_obj = NULL;
  uint32_t checkpoint = 0;
 
  if ( params == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_voc_cmd_disable() - NULL " \
          "params" );
    rc = VS_EUNEXPECTED;
    return rc;
  }

  /* validate the session handle */ 
  rc = vs_get_typed_object( params->handle, VS_OBJECT_TYPE_ENUM_SESSION,
                            ( ( vs_object_t** ) &p_session_obj ) ); 
  if ( rc != VS_EOK )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_voc_cmd_disable() - Invalid " \
            "handle=(0x%08x)",
            params->handle );
    rc = VS_EBADPARAM;
    return rc;
  }

  for ( ;; )
  {
    p_args = ( vs_voc_cmd_disable_t* ) apr_memmgr_malloc( 
                                              &vs_heapmgr,
                                              sizeof( vs_voc_cmd_disable_t ) );
    if ( p_args == NULL )
    { 
      rc = VS_ENORESOURCE;
      VS_REPORT_FATAL_ON_ERROR( rc );
      break;
    }
    p_args->handle = params->handle;
    p_args->client_context = params->client_context;
    checkpoint = 1;

    /* Prepare command packet for VS_VOC_CMD_DISABLE. */
    p_disable_cmd_pkt = ( vs_cmd_packet_t* ) apr_memmgr_malloc( 
                                              &vs_heapmgr,
                                              sizeof( vs_cmd_packet_t ) );
    if ( p_disable_cmd_pkt == NULL )
    { 
      rc = VS_ENORESOURCE;
      VS_REPORT_FATAL_ON_ERROR( rc );
      break;
    }
    p_disable_cmd_pkt->cmd_id = VS_VOC_CMD_DISABLE;
    p_disable_cmd_pkt->params = ( void* )p_args;
    checkpoint = 2;

    /* queue the command packet for processing */
    rc = vs_queue_incoming_vs_command_packet( p_disable_cmd_pkt );
    if ( rc )
    {
      VS_REPORT_FATAL_ON_ERROR( rc );
    }
    else
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,  "vs_voc_cmd_disable() queued: " \
            "handle=(0x%08x)",
            params->handle  );
      checkpoint = 3;
    }
    break;

  }  /* end of for ( ;; ) */

  switch ( checkpoint )
  {
  case 3:
    break;

  case 2:
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_voc_cmd_disable(): failed, " \
              "checkpoint=(%d)",
              checkpoint );
      if ( p_disable_cmd_pkt != NULL )
      {
        apr_memmgr_free( &vs_heapmgr, p_disable_cmd_pkt );
        p_disable_cmd_pkt = NULL;
      }
    }
    /*-fallthru */

  case 1:
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_voc_cmd_disable(): failed, " \
              "checkpoint=(%d)",
              checkpoint );
      if ( p_args != NULL )
      {
        apr_memmgr_free( &vs_heapmgr, p_args );
        p_args = NULL;
      }
    }
    /*-fallthru */

  case 0:
  default:
    break;

  }  /* end of switch ( checkpoint ) */

  return rc;

}  /* end of vs_voc_cmd_disable () */

VS_INTERNAL uint32_t vs_voc_cmd_standby (
  vs_voc_cmd_standby_t* params
)
{

  uint32_t rc = VS_EOK;
  vs_cmd_packet_t* p_standby_cmd_pkt = NULL;
  vs_voc_cmd_standby_t* p_args = NULL;
  vs_session_object_t* p_session_obj = NULL;
  uint32_t checkpoint = 0;
  
  if ( params == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_voc_cmd_standby() - NULL " \
          "params" );
    rc = VS_EUNEXPECTED;
    return rc;
  }
  
  /* validate the session handle */ 
  rc = vs_get_typed_object( params->handle, VS_OBJECT_TYPE_ENUM_SESSION,
                            ( ( vs_object_t** ) &p_session_obj ) ); 
  if ( rc != VS_EOK )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_voc_cmd_standby(): Invalid " \
            "handle=(0x%08x)",
            params->handle );
    rc = VS_EBADPARAM;
    return rc;
  }

  for ( ;; )
  {
    p_args = ( vs_voc_cmd_standby_t* ) apr_memmgr_malloc( 
                                             &vs_heapmgr,
                                             sizeof( vs_voc_cmd_standby_t ) );
    if( p_args == NULL )
    { 
      rc = VS_ENORESOURCE;
      VS_REPORT_FATAL_ON_ERROR( rc );
      break;
    }
    p_args->handle = params->handle;
    p_args->media_id = params->media_id;
    p_args->client_context = params->client_context;
    checkpoint = 1;

    /* Prepare command packet for VS_VOC_CMD_STANDBY. */
    p_standby_cmd_pkt = ( vs_cmd_packet_t* ) apr_memmgr_malloc( 
                                             &vs_heapmgr,
                                             sizeof( vs_cmd_packet_t ) );
    if( p_standby_cmd_pkt == NULL )
    { 
      rc = VS_ENORESOURCE;
      VS_REPORT_FATAL_ON_ERROR( rc );
      break;
    }
    p_standby_cmd_pkt->cmd_id = VS_VOC_CMD_STANDBY;
    p_standby_cmd_pkt->params = ( void* )p_args;
    checkpoint = 2;

    /* queue the command packet for processing */
    rc = vs_queue_incoming_vs_command_packet( p_standby_cmd_pkt );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_voc_cmd_standby() - cmd " \
              "pkt queuing failed. rc=(0x%08x)",
              rc );
    }
    else
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,  "vs_voc_cmd_standby() cmd pkt " \
            "queued: "
            "handle=(0x%08x), media_id=(0x%08x)",
            params->handle, params->media_id );
      checkpoint = 3;
    }
    break;

  } /* end of for ( ;; ) */

  switch ( checkpoint )
  {
  case 3:
    break;

  case 2:
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_voc_cmd_standby() - " \
              "failed, checkpoint=(%d)",
              checkpoint );
      if ( p_standby_cmd_pkt != NULL )
      {
        apr_memmgr_free( &vs_heapmgr, p_standby_cmd_pkt );
        p_standby_cmd_pkt = NULL;
      }
    }
    /*-fallthru */

  case 1:
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_voc_cmd_standby() - " \
              "failed, checkpoint=(%d)",
              checkpoint );
      if ( p_args != NULL )
      {
        apr_memmgr_free( &vs_heapmgr, p_args );
        p_args = NULL;
      }
    }
    /*-fallthru */

  case 0:
  default:
    break;

  }  /* end of switch ( checkpoint ) */

  return rc;

}  /* end of vs_voc_cmd_standby () */

VS_INTERNAL uint32_t vs_voc_cmd_flush_buffers (
  vs_voc_cmd_flush_buffers_t* params
)
{

  uint32_t rc = VS_EOK;
  vs_cmd_packet_t* p_flush_buffers_cmd_pkt = NULL;
  vs_voc_cmd_flush_buffers_t* p_args = NULL;
  vs_session_object_t* p_session_obj = NULL;
  uint32_t checkpoint = 0;

  if ( params == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "vs_voc_cmd_flush_buffers - NULL params" );
    rc = VS_EUNEXPECTED;
    return rc;
  }
  
  /* Get the session object with respect to the handle */ 
  rc = vs_get_typed_object ( params->handle, VS_OBJECT_TYPE_ENUM_SESSION, 
                             ( ( vs_object_t** ) &p_session_obj ) ); 
  if ( rc )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "vs_voc_cmd_flush_buffers(): Invalid handle=(0x%08x)",
           params->handle );
    rc = VS_EBADPARAM;
    return rc;
  }

  for ( ;; )
  {
    p_args =
       ( vs_voc_cmd_flush_buffers_t* ) apr_memmgr_malloc( &vs_heapmgr,
                                         sizeof( vs_voc_cmd_flush_buffers_t ) );
    if( p_args == NULL )
    { 
      rc = VS_ENORESOURCE;
      VS_REPORT_FATAL_ON_ERROR( rc );
      break;
    }
    p_args->handle = params->handle;
    p_args->client_context = params->client_context;
    checkpoint = 1;

    /* Prepare command packet for VS_VOC_CMD_FLUSH_BUFFERS. */
    p_flush_buffers_cmd_pkt = ( vs_cmd_packet_t* ) apr_memmgr_malloc( &vs_heapmgr,
                                               sizeof( vs_cmd_packet_t ) );
    if( p_flush_buffers_cmd_pkt == NULL )
    { 
      rc = VS_ENORESOURCE;
      VS_REPORT_FATAL_ON_ERROR( rc );
      break;
    }
    p_flush_buffers_cmd_pkt->cmd_id = VS_VOC_CMD_FLUSH_BUFFERS;
    p_flush_buffers_cmd_pkt->params = ( void* )p_args;
    checkpoint = 2;

    /* queue the command packet for processing */
    rc = vs_queue_incoming_vs_command_packet( p_flush_buffers_cmd_pkt );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "vs_voc_cmd_flush_buffers() - cmd pkt queuing failed. "
             "rc =(0x%08x)", rc );
    }
    else
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "vs_voc_cmd_flush_buffers() cmd pkt queued: handle=(0x%08x)",
             params->handle );
      checkpoint = 3;
    }
    break;
  }  /* end of for ( ;; ) */

  switch ( checkpoint )
  {
  case 3:
    break;

  case 2:
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "vs_voc_cmd_flush_buffers() - failed, checkpoint=(%d)",
             checkpoint );
      if ( p_flush_buffers_cmd_pkt != NULL )
      {
        apr_memmgr_free( &vs_heapmgr, p_flush_buffers_cmd_pkt );
        p_flush_buffers_cmd_pkt = NULL;
      }
    }
    /*-fallthru */

  case 1:
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "vs_voc_cmd_flush_buffers() - failed, checkpoint=(%d)",
             checkpoint );
      if ( p_args != NULL )
      {
        apr_memmgr_free( &vs_heapmgr, p_args );
        p_args = NULL;
      }
    }
    /*-fallthru */

  case 0:
  default:
    break;

  }  /* end of switch ( checkpoint ) */

  return rc;

}  /* end of vs_voc_cmd_flush_buffers () */

VS_INTERNAL uint32_t vs_voc_cmd_close (
  vs_voc_cmd_close_t* params
)
{

  uint32_t rc = VS_EOK;
  vs_cmd_packet_t* p_close_cmd_pkt = NULL;
  vs_voc_cmd_close_t* p_args = NULL;
  vs_session_object_t* p_session_obj = NULL;
  uint32_t checkpoint = 0;

  if ( params == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_voc_cmd_close() - NULL params"  \
          );
    rc = VS_EUNEXPECTED;
    return rc;
  }
  
  /* validate the session handle */ 
  rc = vs_get_typed_object( params->handle, VS_OBJECT_TYPE_ENUM_SESSION,
                            ( ( vs_object_t** ) &p_session_obj ) );
  if ( rc )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_voc_cmd_close(): Invalid " \
            "handle=(0x%08x)",
            params->handle );
    rc = VS_EBADPARAM;
    return rc;
  }
  
  for ( ;; )
  {
    p_args = ( vs_voc_cmd_close_t* ) apr_memmgr_malloc( 
                                        &vs_heapmgr,
                                        sizeof( vs_voc_cmd_close_t ) );
    if( p_args == NULL )
    { 
      rc = VS_ENORESOURCE;
      VS_REPORT_FATAL_ON_ERROR( rc );
      break;
    }
    p_args->handle = params->handle;
    p_args->client_context = params->client_context;
    checkpoint = 1;

    /* Prepare command packet for VS_VOC_CMD_CLOSE. */
    p_close_cmd_pkt = ( vs_cmd_packet_t* ) apr_memmgr_malloc( 
                                        &vs_heapmgr,
                                        sizeof( vs_cmd_packet_t ) );
    if( p_close_cmd_pkt == NULL )
    { 
      rc = VS_ENORESOURCE;
      VS_REPORT_FATAL_ON_ERROR( rc );
      break;
    }
    p_close_cmd_pkt->cmd_id = VS_VOC_CMD_CLOSE;
    p_close_cmd_pkt->params = ( void* )p_args;
    checkpoint = 2;

    /* queue the command packet for processing */
    rc = vs_queue_incoming_vs_command_packet( p_close_cmd_pkt );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_voc_cmd_close() - cmd pkt " \
              "queuing failed. rc=(0x%08x)",
              rc );
    }
    else
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,  "vs_voc_cmd_close() cmd pkt " \
            "queued: handle=(0x%08x)",
            params->handle );
      checkpoint = 3;
    }
    break;
  }  /* end of for ( ;; ) */

  switch ( checkpoint )
  {
  case 3:
    break;

  case 2:
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_voc_cmd_close() - failed, " \
              "checkpoint=(%d)",
              checkpoint );
      if ( p_close_cmd_pkt != NULL )
      {
        apr_memmgr_free( &vs_heapmgr, p_close_cmd_pkt );
        p_close_cmd_pkt = NULL;
      }
    }
    /*-fallthru */

  case 1:
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_voc_cmd_close() - failed, " \
              "checkpoint=(%d)",
              checkpoint );
      if ( p_args != NULL )
      {
        apr_memmgr_free( &vs_heapmgr, p_args );
        p_args = NULL;
      }
    }
    /*-fallthru */

  case 0:
  default:
    break;
  }  /* end of switch ( checkpoint ) */

  return rc;

}  /* end of vs_voc_cmd_close () */
/****************************************************************************
 * VS Asynchronous non-blocking API functions...END                         *
 ****************************************************************************/


/****************************************************************************
 * VS Synchronous blocking API functions...START                            *
 ****************************************************************************/
VS_INTERNAL uint32_t vs_voc_cmd_open (
  vs_voc_cmd_open_t* params
)
{

  uint32_t rc = VS_EOK;
  vs_session_object_t* p_session_obj = NULL;
  vs_generic_item_t* p_generic_item = NULL;
  uint32_t dst_len = 0;
 
  if ( params == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_voc_cmd_open() - NULL params"  \
          );
    return VS_EUNEXPECTED;
  }
 
  /* first verify if the VSID passed by client is valid and supported by the
   * system.
   */
  switch ( params->vsid )
  {
  case VS_VSID_CS_VOICE_MULTIMODE:
  case VS_VSID_CS_VOICE_GSM2:
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,  "vs_voc_cmd_open() - " \
            "VSID=(0x%08x), client=(0x%08x)",
            params->vsid, params->client_id );
    }
    break;

  default:
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_voc_cmd_open() - " \
              "Unsupported VSID=(0x%08x), "
              "client=(0x%08x)", params->vsid, params->client_id );
      rc = VS_EBADPARAM;
      return rc;
    }
  }  /* end of switch ( params->vsid ) */
  
  /* Make vs_voc_cmd_open() thread safe, during init process for  
   * muliple VS_CLIENTS.
   */
  ( void )apr_lock_enter( vs_voc_cmd_thread_lock );  
  for ( ;; )
  {
    /* verify if a session is already opened for a VSID/client_id combination. 
     * if already opened, fail the API call. 
     * Parse the session objects and compare VSID/client_id combination. 
     */
    p_generic_item = ( ( vs_generic_item_t* ) &vs_used_session_q.dummy );
    for ( ;; )
    {
      rc = apr_list_get_next( &vs_used_session_q,
                              ( ( apr_list_node_t* ) p_generic_item ),
                              ( ( apr_list_node_t** ) &p_generic_item ) );
      if ( rc ) 
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW ,  "vs_voc_cmd_open() - no " \
               "opened session, rc=(0x%08x)",
               rc );
        break;
      }

      rc = vs_get_typed_object( p_generic_item->handle,
                                VS_OBJECT_TYPE_ENUM_SESSION,
                                ( ( vs_object_t** ) &p_session_obj ) );
      if ( rc )
      { 
        VS_REPORT_FATAL_ON_ERROR( rc );
        break;
      }

      if ( ( p_session_obj->vsid == params->vsid ) && 
           ( p_session_obj->client_id == params->client_id ) )
      {
        /* fail API if already a session opened for VSID/client */
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_voc_cmd_open() - session " \
                "already created, "
                "VSID=(0x%08x), client=(0x%08x)",
                params->vsid, params->client_id );
        rc = VS_EUNSUPPORTED;
        break;
      }
      else
      {
        /* Keep looking. */
        p_session_obj = NULL;
      }
    }  /* end of for ( ;; ) */

    /* We did not find existing session object. Go ahead and create a new
     * session object for VSID/Client.
     */
    if ( p_session_obj == NULL )
    {
      /* verify if we have empty session objects. */
      if ( vs_used_session_q.size == VS_MAX_NUM_SESSIONS_V )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_voc_cmd_open() - ran out " \
                "of objects"
                "VSID=(0x%08x), client=(0x%08x)",
                params->vsid, params->client_id );
        rc = VS_ENORESOURCE;
        break;
      }

      /* create a session object for a VSID/Client combination. */
      rc = vs_voc_create_session_object( &p_session_obj );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR ,  "vs_voc_cmd_open() - create " \
                 "session object failed,"
                 " rc=(0x%08x)", rc );
        break;
      }
      else
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED ,  "vs_voc_cmd_open()- new " \
               "session object created. "
               "handle = (0x%08x)", p_session_obj->header.handle );
      }

      p_session_obj->client_id = params->client_id;
      p_session_obj->event_cb = params->event_cb;
      p_session_obj->session_context = params->session_context;
      p_session_obj->vsid = params->vsid;

      /* Update voice timing parameters based off VSID and client_id */
      if ( p_session_obj->client_id == VS_VOC_CLIENT_GSM )
      {
        /* VFR mode is hardcoded now. Need to come up with a solution to
         * derive this information from voice system and client. 
         * <TBD> 
         */
        p_session_obj->voice_timing.vfr_mode = VS_VOC_VFR_MODE_HARD_V;
        p_session_obj->voice_timing.vsid = params->vsid;
        p_session_obj->voice_timing.enc_offset = 
                                       VS_VOC_TIMING_GSM_ENC_OFFSET_V;
        p_session_obj->voice_timing.dec_req_offset = 
                                       VS_VOC_TIMING_GSM_DEC_REQ_OFFSET_V;
        p_session_obj->voice_timing.dec_offset = 
                                       VS_VOC_TIMING_GSM_DEC_OFFSET_V;
      }
      else
      {
        /* VFR mode is hardcoded to SOFT for all client other than GSM.
         * Need to come up with a solution to derive this information 
         * from voice system and client. <TBD> 
         */
        p_session_obj->voice_timing.vfr_mode = VS_VOC_VFR_MODE_SOFT_V;
        p_session_obj->voice_timing.vsid = params->vsid;
        p_session_obj->voice_timing.enc_offset = VS_VOC_TIMING_ENC_OFFSET_V;
        p_session_obj->voice_timing.dec_req_offset = 
                                               VS_VOC_TIMING_DEC_REQ_OFFSET_V;
        p_session_obj->voice_timing.dec_offset = VS_VOC_TIMING_DEC_OFFSET_V;
      }

      /* Update CVD session name based off VSID */
      switch ( p_session_obj->vsid )
      {
      case VS_VSID_CS_VOICE_MULTIMODE:
        dst_len = MMSTD_MIN( sizeof( vs_default_stream_name ),
                              sizeof( p_session_obj->cvd_session_name ) );
        mmstd_strlcpy( p_session_obj->cvd_session_name, vs_default_stream_name,
                      dst_len );
        break;

      case VS_VSID_CS_VOICE_GSM2:
        dst_len = MMSTD_MIN( sizeof( vs_vsid_cs_voice_gsm2_stream_name ),
                              sizeof( p_session_obj->cvd_session_name ) );
        mmstd_strlcpy( p_session_obj->cvd_session_name, 
                       vs_vsid_cs_voice_gsm2_stream_name, dst_len );
        break;
      }
      
      /* Copy the handle to return to client */
      *params->ret_handle = p_session_obj->header.handle;

      /* Add the new session to the session tracking list. */
      rc = apr_list_remove_head( &vs_free_session_q,
                                 ( ( apr_list_node_t** ) &p_generic_item ) );
      if ( rc )
      {
        VS_REPORT_FATAL_ON_ERROR( rc ); 
        break;
      }

      p_generic_item->handle = p_session_obj->header.handle;
      ( void ) apr_list_add_tail( &vs_used_session_q, &p_generic_item->link );

      /* Run VS state machine now */
      ( void ) vs_voc_state_control( p_session_obj );

      /* Unlock mutex after success case scenarios. */
      ( void )apr_lock_leave( vs_voc_cmd_thread_lock );
      
      return rc;
    }  
    else
    {
      /* fail OPEN if already a session opened for VSID/client */
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_voc_cmd_open() failed: " \
              "session exist already for "
              "VSID=(0x%08x), client=(0x%08x)",
              params->vsid, params->client_id );
      break;
    }  /* end of if (p_session_obj == NULL) */

  }  /* end of for ( ;; ) */

  /* Unlock mutex to handle error case  scenarios. */ 
  ( void )apr_lock_leave( vs_voc_cmd_thread_lock ); 
  
  /* errors occured */
  return rc;

}  /* end of vs_voc_cmd_open () */

VS_INTERNAL uint32_t vs_voc_cmd_alloc_buffer (
  vs_voc_cmd_alloc_buffer_t* params
)
{

  vs_session_object_t* p_session_obj = NULL; 
  vs_voc_buffer_t* p_voc_buf = NULL;
  vs_voc_buffer_item_t* p_voc_buf_node = NULL;
  void* p_frame_info = NULL;
  uint8_t* p_frame = NULL;
  uint32_t rc = VS_EOK;
  uint32_t checkpoint = 0;

  if ( params == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "vs_voc_cmd_alloc_buffer() - NULL params" );
    return VS_EUNEXPECTED;
  }
  
  for ( ;; )
  {
    rc = vs_get_typed_object( params->handle, VS_OBJECT_TYPE_ENUM_SESSION, 
                              ( ( vs_object_t** ) &p_session_obj ) ); 
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "vs_voc_cmd_alloc_buffer() - Invalid handle=(0x%08x)",
             params->handle );
      return VS_EBADPARAM;
    }
    else
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_LOW,
             "vs_voc_cmd_alloc_buffer() - handle=(0x%08x), buffer_size=(%d)",
             params->handle, params->req_max_frame_size );
    }

    {
      /* Allocate memory for vs_voc_buffer_t structure */
      p_voc_buf = ( vs_voc_buffer_t* ) apr_memmgr_malloc( &vs_heapmgr,
                                                 sizeof( vs_voc_buffer_t ) );
      if ( p_voc_buf == NULL )
      {
        rc = VS_ENORESOURCE;
        VS_REPORT_FATAL_ON_ERROR( rc ); 
        break;
      }
      else
      {
        mmstd_memset( ( void* )p_voc_buf, 0, sizeof( vs_voc_buffer_t ) );
      }
    }
    checkpoint = 1;

    {
      /* Allocate memory for vs_voc_buffer_t.frame_info.
       * since the exact size depends on vs_voc_buffer_t.media_id, allocate size 
       * of the largest frame_info structure - vs_vocamrwb_frame_info_t . 
       */
      p_frame_info =
         ( vs_vocamrwb_frame_info_t* ) apr_memmgr_malloc( &vs_heapmgr, 
                                          sizeof( vs_vocamrwb_frame_info_t ) );
      if ( p_frame_info == NULL )
      {
        rc = VS_ENORESOURCE;
        VS_REPORT_FATAL_ON_ERROR( rc ); 
        break;
      }
      else
      {
        mmstd_memset( ( void* )p_frame_info, 0, sizeof( vs_vocamrwb_frame_info_t ) );
        p_voc_buf->frame_info = ( void* )p_frame_info;
      }
    }
    checkpoint = 2;

    {
      /* Allocate memory for frame "vocoder packet" */
      p_frame =
         (uint8_t*) apr_memmgr_malloc( &vs_heapmgr, params->req_max_frame_size );
      if ( p_frame == NULL )
      {
        rc = VS_ENORESOURCE;
        VS_REPORT_FATAL_ON_ERROR( rc ); 
        break;
      }
      else
      {
        mmstd_memset( ( void* )p_frame, 0, params->req_max_frame_size );
        p_voc_buf->max_size = params->req_max_frame_size;
        p_voc_buf->frame = p_frame;
        *params->ret_buffer = p_voc_buf;
      }
    }
    checkpoint = 3;

    {
      /* Allocate memory for node vs_voc_buffer_item_t.
       * The vocoder buffer added to this node and used to track allocated 
       * vocoder buffer during packet exchange across alloc_buf_q, write_buf_q 
       * and read_buf_q.
       */
      p_voc_buf_node =
         ( vs_voc_buffer_item_t* ) apr_memmgr_malloc( &vs_heapmgr,
                                              sizeof( vs_voc_buffer_item_t ) );
      if ( p_voc_buf_node == NULL )
      {
        rc = VS_ENORESOURCE;
        VS_REPORT_FATAL_ON_ERROR( rc ); 
        break;
      }
      else
      {
        p_voc_buf_node->buf = p_voc_buf;
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,
               "vs_voc_cmd_alloc_buffer() - buffer allocated=(0x%08x)", 
               p_voc_buf_node->buf );
        p_voc_buf_node->buf_type = VS_VOC_BUFFER_TYPE_NONE;
        ( void ) apr_list_init_node( ( apr_list_node_t* ) &p_voc_buf_node->link );
        ( void ) apr_list_add_tail( &p_session_obj->alloc_buf_q, 
                                    (apr_list_node_t*) &p_voc_buf_node->link );
      }
    }
    checkpoint = 4;

    break;
  }  /* end of for ( ;; ) */

  switch ( checkpoint )
  {
  case 4:
    break;

  case 3:
    {
      if ( p_voc_buf->frame != NULL )
      {
        apr_memmgr_free( &vs_heapmgr, (void*) p_voc_buf->frame );
        p_voc_buf->frame = NULL;
      }
    }
    /*-fallthru */

  case 2:
    {
      if ( p_voc_buf->frame_info != NULL )
      {
        apr_memmgr_free( &vs_heapmgr, (void*) p_voc_buf->frame_info );
        p_voc_buf->frame_info = NULL;
      }
    }
    /*-fallthru */

  case 1:
    {
      if ( p_voc_buf != NULL )
      {
        apr_memmgr_free( &vs_heapmgr, (void*) p_voc_buf );
        p_voc_buf = NULL;
      }
    }
    /*-fallthru */

  default:
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "vs_voc_cmd_alloc_buffer()-  failed, checkpoint=(%d)", checkpoint );
    }
    break;
  }  /* end of switch ( checkpoint ) */

  return rc;

}  /* end of vs_voc_cmd_alloc_buffer () */

VS_INTERNAL uint32_t vs_voc_cmd_free_buffer (
  vs_voc_cmd_free_buffer_t* params
)
{

  uint32_t rc = VS_EOK;
  bool_t is_buf_in_read_q = FALSE;
  bool_t is_buf_in_write_q = FALSE;
  bool_t is_buf_in_alloc_q = FALSE;
  vs_session_object_t* p_session_obj = NULL;
  vs_voc_buffer_item_t* p_voc_buf_node = NULL;

  /* Validate the params provided by client. */
  if ( params == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "vs_voc_cmd_free_buffer() - NULL params" );
    return VS_EUNEXPECTED;
  }
  
  /* Get the session object */ 
  rc = vs_get_typed_object( params->handle, VS_OBJECT_TYPE_ENUM_SESSION, 
                            ( ( vs_object_t** ) &p_session_obj ) ); 
  if ( rc )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "vs_voc_cmd_free_buffer() - Invalid handle=(0x%08x)",
           params->handle );
    return VS_EBADPARAM;
  }
  else
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_LOW,
           "vs_voc_cmd_free_buffer() - handle=(0x%08x), buffer=(0x%08x)",
           params->handle, params->buffer );
  }


  /* Allocated buffer should be present in one of session object queues
   * alloc_buf_q, write_buf_q or read_buf_q.
   */
  for ( ;; )
  {
    /* Traverse read_buf_q to find the vocoder buffer. */
    p_voc_buf_node =
       ( ( vs_voc_buffer_item_t* ) &p_session_obj->read_buf_q.dummy );
    for ( ;;)
    {
      rc = apr_list_get_next( &p_session_obj->read_buf_q,
                              ( ( apr_list_node_t* ) p_voc_buf_node ),
                              ( ( apr_list_node_t** ) &p_voc_buf_node ) );
      if ( rc ) 
      {
        is_buf_in_read_q = FALSE;
        rc = VS_EFAILED;
        break;
      }
      else if( params->buffer == p_voc_buf_node->buf )
      {
        is_buf_in_read_q = TRUE;
        break;
      }
    }
    if ( is_buf_in_read_q == TRUE )
    {
      /* Delete the buffer node from read_buf_q list */
      apr_list_delete( &p_session_obj->read_buf_q,
                       ( ( apr_list_node_t* ) p_voc_buf_node ) );
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "vs_voc_cmd_free_buffer() - buffer found in read_buf_q  " );
      break;
    }

    /* Traverse write_buf_q to find the vocoder buffer. */
    p_voc_buf_node =
       ( ( vs_voc_buffer_item_t* ) &p_session_obj->write_buf_q.dummy );
    for ( ;;)
    {
      rc = apr_list_get_next( &p_session_obj->write_buf_q,
                              ( ( apr_list_node_t* ) p_voc_buf_node ),
                              ( ( apr_list_node_t** ) &p_voc_buf_node ) );
      if ( rc ) 
      {
        is_buf_in_write_q = FALSE;
        rc = VS_EFAILED;
        break;
      }
      else if( params->buffer == p_voc_buf_node->buf )
      {
        is_buf_in_write_q = TRUE;
        break;
      }
    }
    if ( is_buf_in_write_q == TRUE )
    {
      /* Delete the buffer node from read_buf_q list */
      apr_list_delete( &p_session_obj->write_buf_q,
                       ( ( apr_list_node_t* ) p_voc_buf_node ) );
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "vs_voc_cmd_free_buffer() - buffer found in write_buf_q  " );
      break;
    }

    /* Traverse alloc_buf_q to find the vocoder buffer. */
  p_voc_buf_node = 
            ( ( vs_voc_buffer_item_t* ) &p_session_obj->alloc_buf_q.dummy );
  for ( ;; )
  {
    rc = apr_list_get_next( &p_session_obj->alloc_buf_q,
                            ( ( apr_list_node_t* ) p_voc_buf_node ),
                            ( ( apr_list_node_t** ) &p_voc_buf_node ) );
    if ( rc )
    { 
        is_buf_in_alloc_q = FALSE;
      rc = VS_EFAILED;
      break;
    }
    else if( params->buffer == p_voc_buf_node->buf )
    {
        is_buf_in_alloc_q = TRUE;
      break;
    }
    }
    if ( is_buf_in_alloc_q == TRUE )
  {
      /* Delete the buffer node from alloc_buf_q list */
    apr_list_delete( &p_session_obj->alloc_buf_q, 
                     ( ( apr_list_node_t* ) p_voc_buf_node ) );
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "vs_voc_cmd_free_buffer() - buffer found in alloc_buf_q  " );
       break;
    }

    break;
  }

  if ( is_buf_in_write_q || is_buf_in_read_q || is_buf_in_alloc_q )
  {
    /* free the buffer requested by client */
    if ( p_voc_buf_node != NULL )
    {
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "vs_voc_cmd_free_buffer(): buffer freed buf=(0x%08x), "
             "buf->frame_info=(0x%08x), buf->frame=(0x%08x)", p_voc_buf_node->buf,
             p_voc_buf_node->buf->frame_info, p_voc_buf_node->buf->frame );

      if ( p_voc_buf_node->buf->frame_info != NULL )
      { /* Delete the vocoder buffer frame info memory. */
        apr_memmgr_free( &vs_heapmgr, p_voc_buf_node->buf->frame_info );
        p_voc_buf_node->buf->frame_info = NULL;
      }

      if ( p_voc_buf_node->buf->frame != NULL )
      { /* Delete the vocoder buffer frame memory. */
        apr_memmgr_free( &vs_heapmgr, p_voc_buf_node->buf->frame );
        p_voc_buf_node->buf->frame = NULL;
      }

      { /* Delete the vocoder buffer memory. */
        apr_memmgr_free( &vs_heapmgr, p_voc_buf_node->buf );
        p_voc_buf_node->buf = NULL;
      }

      { /* Delete the buffer node memory. */
        apr_memmgr_free( &vs_heapmgr, p_voc_buf_node );
        p_voc_buf_node = NULL;
      }
    }
    else
    {
      VS_REPORT_FATAL_ON_ERROR( VS_EUNEXPECTED ); 
    }
  }
  else
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "vs_voc_cmd_free_buffer() - invalid buffer=(0x%08x)",
           p_voc_buf_node->buf  );
  }  /* end of if ( is_found ) */
  
  return rc;

}  /* end of vs_voc_cmd_free_buffer () */
/****************************************************************************
 * VS Synchronous blocking API functions...END                              *
 ****************************************************************************/


/****************************************************************************
 * VS Synchronous non-blocking API functions...START                        *
 ****************************************************************************/
VS_INTERNAL uint32_t vs_voc_cmd_prime_read_buffer (
  vs_voc_cmd_prime_read_buffer_t* params
)
{

  uint32_t rc = VS_EOK;
  vs_session_object_t* p_session_obj = NULL; 
  vs_voc_buffer_item_t* p_voc_buf_node = NULL;
  bool_t is_found = FALSE;

  if ( params == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "vs_voc_cmd_prime_read_buffer() - NULL params" );
    return VS_EUNEXPECTED;
  }
  
  /* Get the session object with respect to the handle */ 
  rc = vs_get_typed_object( params->handle, VS_OBJECT_TYPE_ENUM_SESSION, 
                            ( (vs_object_t** ) &p_session_obj ) ); 
  if ( rc )
  {
    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "vs_voc_cmd_prime_read_buffer(): Invalid handle=(0x%08x), "
           "buffer=(0x%08x), rc=(0x%08x)", params->handle, params->buffer, rc );
    return VS_EBADPARAM;
  }

  /* Check whether the given buffer is in the alloc_buf_q. */
  p_voc_buf_node = 
         ( vs_voc_buffer_item_t* )( &p_session_obj->alloc_buf_q.dummy );
  for ( ;; )
  {
    rc = apr_list_get_next( &p_session_obj->alloc_buf_q,
                            ( ( apr_list_node_t* ) p_voc_buf_node ),
                            ( ( apr_list_node_t** ) &p_voc_buf_node ) );
    if ( rc )
    {
      is_found = FALSE;
      break;     
    }
    else if ( params->buffer == p_voc_buf_node->buf )
    {
      is_found = TRUE;
      break;
    }
  } /* end of for ( ;; ) */

  if( is_found )
  {

    if ( p_voc_buf_node->buf_type == VS_VOC_BUFFER_TYPE_PRIMED )
    {
      /* Flag Error: if node containing params->buffer is already primed. */
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "vs_voc_cmd_prime_read_buffer(): buffer=(0x%08x) primed",
             params->buffer );
      }
      else
      {
    /* Mark this node containing params->buffer primed for TX path. */
      p_voc_buf_node->buf_type = VS_VOC_BUFFER_TYPE_PRIMED;
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,
             "vs_voc_cmd_prime_read_buffer(): buffer=(0x%08x) primed",
             params->buffer );
    }
  }
  else
  {
    rc = VS_ENOTEXIST;
    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "vs_voc_cmd_prime_read_buffer(): Primed buffer invalid=(0x%08x) "
           "rc=(0x%08x), handle=(0x%08x)", params->buffer, rc, params->handle );
  } /* end of if( found ) */
   
  return rc;

}  /* end of vs_voc_cmd_prime_read_buffer () */

VS_INTERNAL uint32_t vs_voc_cmd_read_buffer (
  vs_voc_cmd_read_buffer_t* params
)
{

  uint32_t rc = VS_EOK;
  vs_session_object_t* p_session_obj; 
  vs_voc_buffer_item_t* p_voc_buf_node;
   
  if ( params == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "vs_voc_cmd_read_buffer() - NULL  params" );
    return VS_EUNEXPECTED;
  }

  /* Get the session object with respect to the handle */ 
  rc = vs_get_typed_object( params->handle, VS_OBJECT_TYPE_ENUM_SESSION, 
                            ( ( vs_object_t** ) &p_session_obj ) ); 
  if ( rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "vs_voc_cmd_read_buffer(): Invalid handle=(0x%08x), buffer = (0x%08x)",
           params->handle, (*params->ret_buffer) );
    return VS_EBADPARAM;
  }
  
  /* Extract the encoder packet from read_buf_q if available form ADSP,
   * unprime the vocoder node by marking it UNSET and moving it to 
   * alloc_buf_q. 
   */
  rc = apr_list_remove_head( &p_session_obj->read_buf_q, 
                             ( ( apr_list_node_t** ) &p_voc_buf_node ) );
  if ( rc )
  {
    rc = VS_ENOTEXIST;
    *params->ret_buffer = NULL;
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "tx_packet_status = vocoder packet NOT available from ADSP."  );
  }
  else
  {
    if ( p_voc_buf_node != NULL)
    {
      /* Return the read/TX buffer to client. */
      *params->ret_buffer = p_voc_buf_node->buf;

      /* Move the node back to alloc_buf_q since its read successfully. */
      p_voc_buf_node->buf_type = VS_VOC_BUFFER_TYPE_NONE; 
      ( void ) apr_list_add_tail( &p_session_obj->alloc_buf_q,
                                  &p_voc_buf_node->link );
      p_voc_buf_node = NULL;
    }
  }  /* end of if ( rc ) */

  return rc;

}  /* end of vs_voc_cmd_read_buffer () */

VS_INTERNAL uint32_t vs_voc_cmd_write_buffer (
  vs_voc_cmd_write_buffer_t* params
)
{ 

  uint32_t rc = VS_EOK;
  vs_session_object_t* p_session_obj; 
  vs_voc_buffer_item_t* p_voc_buf_node;
  bool_t is_buf_in_alloc_q = FALSE;
  bool_t is_buf_in_write_q = FALSE;

  if ( params == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "vs_voc_cmd_write_buffer() - NULL params" );
    return VS_EUNEXPECTED;
  }
  
  /* Get the session object with respect to the handle */ 
  rc = vs_get_typed_object( params->handle, VS_OBJECT_TYPE_ENUM_SESSION, 
                            ( (vs_object_t** ) &p_session_obj ) ); 
  if ( rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "vs_voc_cmd_write_buffer(): Invalid handle=(0x%08x) buffer=(0x%08x)",
           params->handle, params->buffer );
    return VS_EBADPARAM;
  }
  
  for ( ;;)
  {
  /* Check whether the given buffer is in the alloc buffer queue*/
  p_voc_buf_node = 
       ( vs_voc_buffer_item_t* )( &p_session_obj->alloc_buf_q.dummy );
  for ( ;; )
  {
    rc = apr_list_get_next( &p_session_obj->alloc_buf_q,
                             ( ( apr_list_node_t* ) p_voc_buf_node ),
                             ( ( apr_list_node_t** ) &p_voc_buf_node ) );
    if ( rc )
    { 
      is_buf_in_alloc_q = FALSE;
      break;
    }
    else if ( params->buffer == p_voc_buf_node->buf )
    {
      is_buf_in_alloc_q = TRUE;
      break;
    }
  }  /* end of for ( ;; ) */
  if ( is_buf_in_alloc_q ==  TRUE )
  {
    /* Delete the buffer node from alloc_buf_q list */
    apr_list_delete( &p_session_obj->alloc_buf_q,
                     ( ( apr_list_node_t* ) p_voc_buf_node ) );

      if ( p_voc_buf_node->buf_type == VS_VOC_BUFFER_TYPE_PRIMED )
  {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "vs_voc_cmd_write_buffer(): buffer=(0x%08x) is already primed "
               "for read Overridding the buffer status as per current request",
               params->handle, params->buffer );
      }
    /* Mark this node primed for RX path. */
      p_voc_buf_node->buf_type = VS_VOC_BUFFER_TYPE_WRITE;

    /* Move this node to write_buf_q for forwarding it to ADSP. */
    ( void ) apr_list_add_tail( &p_session_obj->write_buf_q,
                                &p_voc_buf_node->link );
      break;
  }

  /* If not found in alloc_buf_q, check whether the given buffer is
   * already written to write_buf_q.
     */
    p_voc_buf_node = 
         ( vs_voc_buffer_item_t* )( &p_session_obj->write_buf_q.dummy );
    for ( ;; )
    {
      rc = apr_list_get_next( &p_session_obj->write_buf_q,
                               ( ( apr_list_node_t* ) p_voc_buf_node ),
                               ( ( apr_list_node_t** ) &p_voc_buf_node ) );
      if ( rc )
      { 
      is_buf_in_write_q = FALSE;
        break;
      }
      else if ( params->buffer == p_voc_buf_node->buf )
      {
      is_buf_in_write_q = TRUE;
        break;
      }
    }  /* end of for ( ;; ) */
  if ( is_buf_in_write_q ==  TRUE )
    {
    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "vs_voc_cmd_write_buffer(): buffer already written=(0x%08x) "
           "rc=(0x%08x), handle=(0x%08x)", params->buffer, rc, params->handle );
    break;
      }

  break;
      }

  if ( ( is_buf_in_alloc_q == FALSE ) && ( is_buf_in_write_q == FALSE ) )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "vs_voc_cmd_write_buffer(): Invalid write buffer = (0x%08x) session "
           "handle= (0x%08x)", params->buffer, params->handle );
    rc = VS_EBADPARAM;
  }

  return rc;
}  /* end of vs_voc_cmd_write_buffer () */
/****************************************************************************
 * VS Synchronous non-blocking API functions...END                          *
 ****************************************************************************/


VS_INTERNAL uint32_t vs_voc_send_dec_packet (
  uint32_t session_handle,
  vs_voc_buffer_t* p_wbuffer,  
  aprv2_packet_t* p_packet
)
{

  vs_session_object_t* p_session_obj = NULL; 
  uint32_t rc = VS_EOK;
  aprv2_packet_t* p_packet1 = NULL;
  vss_istream_evt_send_dec_buffer_t* p_dec_buffer = NULL;
  uint8_t* p_voc_packet;
  vs_vocamr_frame_info_t* p_amr = NULL;
  vs_vocamr_frame_type_t amr_frame_type;
  vs_vocamr_codec_mode_t amr_codec_mode;
  vs_vocamrwb_frame_info_t* p_amrwb = NULL;
  vs_vocamrwb_frame_type_t amrwb_frame_type;
  vs_vocamrwb_codec_mode_t amrwb_codec_mode;
  uint32_t packet_payload_size;
  /* 1 byte header + vocoder data */
  uint32_t frame_length = 0;

  if ( p_wbuffer == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "vs_voc_send_dec_packet(): NULL p_wbuffer" );
    return VS_EBADPARAM;
  }

  for ( ;; )
  {
    /* Get the session object with respect to the handle */ 
    rc = vs_get_typed_object( session_handle, VS_OBJECT_TYPE_ENUM_SESSION, 
                              ( ( vs_object_t** ) &p_session_obj ) ); 
    VS_PANIC_ON_ERROR( rc );

    /* check media_id is valid or not */
    /* since gl1 is not sending correct media_id, commented this block
     * for test purpose. 
     */
    #if 0
    if ( p_wbuffer->media_id != p_session_obj->media_id )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "vs_voc_send_dec_packet(): Incorrect media_id received = (0x%08x), "
             "actual configured media_id = (0x%08x)",
             p_wbuffer->media_id, p_session_obj->media_id);
      rc = VS_EBADPARAM;
      break;
    }
    #endif

    /* this line should be removed as well - <TBD>*/
    p_wbuffer->media_id = p_session_obj->media_id;

    /* check vocoder packet size */
    if ( p_wbuffer->size > p_wbuffer->max_size )
    {
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "vs_voc_send_dec_packet(): Incorrect packet_size=(%d), "
             "maximum size=(%d), session_handle=(0x%08x)",
             p_wbuffer->size, p_wbuffer->max_size, session_handle );
      rc = VS_EBADPARAM;
      break;
    }

    switch ( p_wbuffer->media_id )
    {
    case VS_COMMON_MEDIA_ID_AMR:
      {
        p_amr = ( vs_vocamr_frame_info_t* )( p_wbuffer->frame_info );

        /* validate amr frame_type  */
        amr_frame_type  = p_amr->frame_type;

        if ( amr_frame_type < VS_VOCAMR_FRAME_TYPE_SPEECH_GOOD ||
             amr_frame_type > VS_VOCAMR_FRAME_TYPE_NO_DATA )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                 "vs_voc_send_dec_packet(): Incorrect AMR frame_type=(%d)",
                 amr_frame_type );
          rc = VS_EBADPARAM;
          break;
        }

        /* validate amr codec_mode */
        amr_codec_mode = p_amr->codec_mode;

        if ( amr_codec_mode < VS_VOCAMR_CODEC_MODE_0475 ||
             amr_codec_mode > VS_VOCAMR_CODEC_MODE_1220 )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                 "vs_voc_send_dec_packet(): Incorrect AMR codec_mode=(%d)",
                 amr_codec_mode );
          rc = VS_EBADPARAM;
          break;
        }
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_LOW,
               "vs_voc_send_dec_packet(): AMR codec_mode=(%d), frame_type=(%d), "
               "session_handle=(0x%08x)",
               amr_codec_mode, amr_frame_type, session_handle );

        frame_length = vs_vocamr_get_frame_len( amr_frame_type, 
                                                amr_codec_mode );

        rc = __aprv2_cmd_alloc_ext(
               vs_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
               p_packet->dst_addr, p_packet->dst_port,
               p_packet->src_addr, p_packet->src_port,
               p_packet->token, VSS_ISTREAM_EVT_SEND_DEC_BUFFER,
               ( sizeof( vss_istream_evt_send_dec_buffer_t ) + 
                 ( frame_length + 1 ) ), &p_packet1 );
        VS_PANIC_ON_ERROR( rc );

        if ( p_packet1 == NULL )
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "vs_voc_send_dec_packet(): __aprv2_cmd_alloc_ext() Failed!!" );
          rc = VS_ENORESOURCE;
          break;
        }
        p_dec_buffer = APRV2_PKT_GET_PAYLOAD( vss_istream_evt_send_dec_buffer_t,
                                              p_packet1 );
        p_dec_buffer->media_id = vs_voc_map_to_cvd_mode( p_wbuffer->media_id );
        p_voc_packet = APR_PTR_END_OF( p_dec_buffer,
                                       sizeof( vss_istream_evt_send_dec_buffer_t ) );

        /* populate header and copy vocoder packet data */
        *p_voc_packet = ( uint8_t ) ( ( ( amr_frame_type & 0x0F ) << 4 ) | 
                                      ( amr_codec_mode & 0x0F ) );

        packet_payload_size = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( p_packet1->header )
                              - sizeof( vss_istream_evt_send_dec_buffer_t )
                              - VS_VOC_PACKET_HDR_LENGTH_V;

        mmstd_memcpy( ( p_voc_packet + VS_VOC_PACKET_HDR_LENGTH_V ), packet_payload_size,
                        p_wbuffer->frame, frame_length );

        /* send vocoder packet to CVS */
        ( void ) __aprv2_cmd_forward( vs_apr_handle, p_packet1 );
      }
      break;

    case VS_COMMON_MEDIA_ID_AMRWB:
      {
        p_amrwb = ( vs_vocamrwb_frame_info_t* )( p_wbuffer->frame_info );

        /* validate frame_type  */
        amrwb_frame_type  = p_amrwb->frame_type;
                 
        if ( amrwb_frame_type < VS_VOCAMRWB_FRAME_TYPE_SPEECH_GOOD ||
             amrwb_frame_type > VS_VOCAMRWB_FRAME_TYPE_NO_DATA )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                 "vs_voc_send_dec_packet(): Invalid AMRWB frame_type=(%d)",
                 amrwb_frame_type );
          rc = VS_EBADPARAM;
          break;
        }

        /* validate codec_mode */
        amrwb_codec_mode = p_amrwb->codec_mode;
                
        if ( amrwb_codec_mode < VS_VOCAMRWB_CODEC_MODE_0660 ||
             amrwb_codec_mode > VS_VOCAMRWB_CODEC_MODE_2385 )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                 "vs_voc_send_dec_packet(): Incorrect AMRWB codec_mode=(%d)",
                 amrwb_codec_mode );
          rc = VS_EBADPARAM;
          break;
        }

        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_LOW,
               "vs_voc_send_dec_packet(): AMRWB codec_mode=(%d), frame_type=(%d), "
               "session_handle=(0x%08x)",
               amrwb_codec_mode, amrwb_frame_type, session_handle );

        frame_length = vs_vocamrwb_get_frame_len( amrwb_frame_type, 
                                                  amrwb_codec_mode );

        rc = __aprv2_cmd_alloc_ext(
               vs_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
               p_packet->dst_addr, p_packet->dst_port,
               p_packet->src_addr, p_packet->src_port,
               p_packet->token, VSS_ISTREAM_EVT_SEND_DEC_BUFFER,
               ( sizeof( vss_istream_evt_send_dec_buffer_t ) + 
                 ( frame_length + 1 ) ), &p_packet1 );
        VS_PANIC_ON_ERROR( rc );

        if ( p_packet1 == NULL )
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "vs_voc_send_dec_packet(): __aprv2_cmd_alloc_ext() Failed. " );
          rc = VS_ENORESOURCE;
          break;
        }
        p_dec_buffer = APRV2_PKT_GET_PAYLOAD( vss_istream_evt_send_dec_buffer_t,
                                              p_packet1 );
        p_dec_buffer->media_id = vs_voc_map_to_cvd_mode( p_wbuffer->media_id );
        p_voc_packet = APR_PTR_END_OF( p_dec_buffer,
                                       sizeof( vss_istream_evt_send_dec_buffer_t ) );

        /* populate header and copy vocoder packet */
        *p_voc_packet = ( ( amrwb_frame_type & 0x0F ) << 4 ) | 
                        ( amrwb_codec_mode & 0x0F );

        packet_payload_size = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( p_packet1->header )
                              - sizeof( vss_istream_evt_send_dec_buffer_t )
                              - VS_VOC_PACKET_HDR_LENGTH_V;

        mmstd_memcpy( ( p_voc_packet + VS_VOC_PACKET_HDR_LENGTH_V ), packet_payload_size,
                        p_wbuffer->frame, frame_length );

        /* send vocoder packet to CVS */
        ( void ) __aprv2_cmd_forward( vs_apr_handle, p_packet1 );

      }
      break;

    case VS_COMMON_MEDIA_ID_EFR:
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_LOW,
               "vs_voc_send_dec_packet(): EFR frame_info=(%d), session_handle = (0x%08x)",
               (*( uint8_t* )( p_wbuffer->frame_info ) ), session_handle );

        rc = __aprv2_cmd_alloc_ext(
               vs_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
               p_packet->dst_addr, p_packet->dst_port,
               p_packet->src_addr, p_packet->src_port,
               p_packet->token, VSS_ISTREAM_EVT_SEND_DEC_BUFFER,
               ( sizeof( vss_istream_evt_send_dec_buffer_t ) + 
                 ( VS_VOCEFR_MAX_PACKET_DATA_LENGTH + 1 ) ), &p_packet1 );
        VS_PANIC_ON_ERROR( rc );

        if ( p_packet1 == NULL )
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "vs_voc_send_dec_packet(): __aprv2_cmd_alloc_ext() Failed!!" );
          rc = VS_ENORESOURCE;
          break;
        }
        p_dec_buffer = APRV2_PKT_GET_PAYLOAD( vss_istream_evt_send_dec_buffer_t,
                                              p_packet1 );
        p_dec_buffer->media_id = vs_voc_map_to_cvd_mode( p_wbuffer->media_id );
        p_voc_packet = APR_PTR_END_OF( p_dec_buffer,
                                       sizeof( vss_istream_evt_send_dec_buffer_t ) );

        /* populate header and copy vocoder packet */
        *p_voc_packet = (*( uint8_t* )( p_wbuffer->frame_info ) );

        packet_payload_size = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( p_packet1->header )
                              - sizeof( vss_istream_evt_send_dec_buffer_t )
                              - VS_VOC_PACKET_HDR_LENGTH_V;
		
        mmstd_memcpy( ( p_voc_packet + VS_VOC_PACKET_HDR_LENGTH_V ), packet_payload_size,
                      p_wbuffer->frame, VS_VOCEFR_MAX_PACKET_DATA_LENGTH );

        /* send vocoder packet to CVS */
        ( void ) __aprv2_cmd_forward( vs_apr_handle, p_packet1 );
      }
      break;

    case VS_COMMON_MEDIA_ID_FR:
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_LOW,
               "vs_voc_send_dec_packet(): FR frame_info=(%d), session_handle = (0x%08x)",
               (*( uint8_t* )( p_wbuffer->frame_info ) ), session_handle );

        rc = __aprv2_cmd_alloc_ext(
               vs_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
               p_packet->dst_addr, p_packet->dst_port,
               p_packet->src_addr, p_packet->src_port,
               p_packet->token, VSS_ISTREAM_EVT_SEND_DEC_BUFFER,
               ( sizeof( vss_istream_evt_send_dec_buffer_t ) + 
                 ( VS_VOCFR_MAX_PACKET_DATA_LENGTH + 1 ) ), &p_packet1 );
        VS_PANIC_ON_ERROR( rc );

        if ( p_packet1 == NULL )
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "vs_voc_send_dec_packet(): __aprv2_cmd_alloc_ext() Failed!!" );
          rc = VS_ENORESOURCE;
          break;
        }
        p_dec_buffer = APRV2_PKT_GET_PAYLOAD( vss_istream_evt_send_dec_buffer_t,
                                              p_packet1 );
        p_dec_buffer->media_id = vs_voc_map_to_cvd_mode( p_wbuffer->media_id );
        p_voc_packet = APR_PTR_END_OF( p_dec_buffer,
                                       sizeof( vss_istream_evt_send_dec_buffer_t ) );

        /* populate header and copy vocoder packet */
        *p_voc_packet = (*( uint8_t* )( p_wbuffer->frame_info ) );     

        packet_payload_size = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( p_packet1->header )
                              - sizeof( vss_istream_evt_send_dec_buffer_t )
                              - VS_VOC_PACKET_HDR_LENGTH_V;
		
        mmstd_memcpy( ( p_voc_packet + VS_VOC_PACKET_HDR_LENGTH_V ), packet_payload_size, 
                        p_wbuffer->frame, VS_VOCFR_MAX_PACKET_DATA_LENGTH );

        /* send vocoder packet to CVS */
        ( void ) __aprv2_cmd_forward( vs_apr_handle, p_packet1 );
      }
      break;

    case VS_COMMON_MEDIA_ID_HR:
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_LOW,
               "vs_voc_send_dec_packet(): HR frame_info=(%d), session_handle = (0x%08x)",
               (*( uint8_t* )( p_wbuffer->frame_info ) ), session_handle );

        rc = __aprv2_cmd_alloc_ext(
               vs_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
               p_packet->dst_addr, p_packet->dst_port,
               p_packet->src_addr, p_packet->src_port,
               p_packet->token, VSS_ISTREAM_EVT_SEND_DEC_BUFFER,
               ( sizeof( vss_istream_evt_send_dec_buffer_t ) + 
                 ( VS_VOCHR_MAX_PACKET_DATA_LENGTH + 1 ) ), &p_packet1 );
        VS_PANIC_ON_ERROR( rc );

        if ( p_packet1 == NULL )
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "vs_voc_send_dec_packet(): __aprv2_cmd_alloc_ext() Failed!!" );
          rc = VS_ENORESOURCE;
          break;
        }
        p_dec_buffer = APRV2_PKT_GET_PAYLOAD( vss_istream_evt_send_dec_buffer_t,
                                              p_packet1 );
        p_dec_buffer->media_id = vs_voc_map_to_cvd_mode( p_wbuffer->media_id );
        p_voc_packet = APR_PTR_END_OF( p_dec_buffer, 
                                       sizeof( vss_istream_evt_send_dec_buffer_t ) );

        /* populate header and copy vocoder packet */
        *p_voc_packet = (*( uint8_t* )( p_wbuffer->frame_info ) );     

        packet_payload_size = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( p_packet1->header )
                              - sizeof( vss_istream_evt_send_dec_buffer_t )
                              - VS_VOC_PACKET_HDR_LENGTH_V;

        mmstd_memcpy( ( p_voc_packet + VS_VOC_PACKET_HDR_LENGTH_V ), packet_payload_size,
                        p_wbuffer->frame, VS_VOCHR_MAX_PACKET_DATA_LENGTH );

        /* send vocoder packet to CVS */
        ( void ) __aprv2_cmd_forward( vs_apr_handle, p_packet1 );
      }
      break;

    default:
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "vs_voc_send_dec_packet(): Invalid media id" );
      rc = VS_EUNSUPPORTED;
      break;
    }  /* end of switch ( p_wbuffer->media_id ) */

    /* break always */
    break;

  }  /* end of for ( ;; ) */

  return rc;

}  /* end of vs_voc_send_dec_packet() */


/****************************************************************************
 * GATING COMMAND PROCESSING FUNCTIONS                                      *
 ****************************************************************************/
enum { 
  VS_STANDBY_SEQUENCER_ENUM_UNINITIALIZED, 
  VS_STANDBY_SEQUENCER_ENUM_CREATE_MVM, /* Entry. */ 
  VS_STANDBY_SEQUENCER_ENUM_CREATE_MVM_WAIT, 
  VS_STANDBY_SEQUENCER_ENUM_SET_MODEM_VOICE_CONTROL,
  VS_STANDBY_SEQUENCER_ENUM_CREATE_CVS, 
  VS_STANDBY_SEQUENCER_ENUM_CREATE_CVS_WAIT, 
  VS_STANDBY_SEQUENCER_ENUM_ATTACH_STREAM, 
  VS_STANDBY_SEQUENCER_ENUM_ATTACH_STREAM_WAIT, 
  VS_STANDBY_SEQUENCER_ENUM_STANDBY_VOICE,
  VS_STANDBY_SEQUENCER_ENUM_STANDBY_VOICE_WAIT, 
  VS_STANDBY_SEQUENCER_ENUM_SET_MEDIA_TYPE, 
  VS_STANDBY_SEQUENCER_ENUM_SET_MEDIA_TYPE_WAIT, 
  VS_STANDBY_SEQUENCER_ENUM_RECONFIG, 
  VS_STANDBY_SEQUENCER_ENUM_SET_CACHED_VALUES, 
  VS_STANDBY_SEQUENCER_ENUM_STOP_VOICE, /* Entry. */ 
  VS_STANDBY_SEQUENCER_ENUM_STOP_VOICE_WAIT, 
  VS_STANDBY_SEQUENCER_ENUM_COMPLETE, /* Exit. */
  VS_STANDBY_SEQUENCER_ENUM_INVALID 
};

static uint32_t vs_voc_standby_sequencer ( 
  vs_gating_control_t* ctrl
) 
{

  uint32_t rc = VS_EOK; 
  vs_session_object_t* p_session_obj = NULL; 
  vs_sequencer_job_object_t* p_seqjob_obj = NULL; 
  vs_simple_job_object_t* p_simple_job_obj = NULL; 
  vs_token_object_t* p_token_obj = NULL; 
  aprv2_packet_t* p_packet = NULL; 
  
  char_t* p_dst_stream_name = NULL;
  vs_cmd_packet_t* p_standby_cmd_pkt = NULL; 
  vs_voc_cmd_standby_t* p_standby_cmd_params = NULL; 
  
  vss_imvm_cmd_set_modem_voice_control_t modem_voice_ctrl_args;
  vss_istream_cmd_create_full_control_session_t* p_full_control = NULL; 
  vss_istream_cmd_set_media_type_t set_media_type;

  /* get the handle */
  p_standby_cmd_pkt = ctrl->packet;
  p_standby_cmd_params = ( vs_voc_cmd_standby_t* )p_standby_cmd_pkt->params;

  /* Get the session object with respect to the handle */ 
  rc = vs_get_typed_object( p_standby_cmd_params->handle,
                            VS_OBJECT_TYPE_ENUM_SESSION, 
                            ( ( vs_object_t** ) &p_session_obj ) );
  VS_PANIC_ON_ERROR( rc );

  p_seqjob_obj = p_session_obj->session_ctrl.pendjob_seq_ptr;

  if ( ( vs_voc_is_cvd_ready == FALSE ) || 
       ( vs_voc_is_ssr_cleanup_done == FALSE ) )
  {
    p_seqjob_obj->status = VS_ENOTREADY;
    p_seqjob_obj->state = VS_STANDBY_SEQUENCER_ENUM_COMPLETE;
    p_session_obj->is_first_call = TRUE;
  }

  for ( ;; ) 
  { 
    switch ( p_seqjob_obj->state ) 
    {
    case VS_STANDBY_SEQUENCER_ENUM_CREATE_MVM:
      {
        /* Create the MVM session. */
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,   \
              "VS_STANDBY_SEQUENCER_ENUM_CREATE_MVM  handle=(0x%08x)",
              p_session_obj->header.handle );
        rc = vs_create_simple_job_object( p_session_obj->header.handle, 
                                          &p_simple_job_obj );
        VS_PANIC_ON_ERROR( rc );

        p_seqjob_obj->subjob_obj = ( ( vs_object_t* ) p_simple_job_obj );
        p_simple_job_obj->fn_table[ VS_RESPONSE_FN_ENUM_RESULT ] = 
                                      vs_voc_create_mvm_session_result_rsp_fn;

        rc = __aprv2_cmd_alloc_send(
              vs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
              vs_my_addr, ( (uint16_t) p_session_obj->header.handle ),
              vs_mvm_addr, APR_NULL_V,
              p_simple_job_obj->header.handle, 
              VSS_IMVM_CMD_CREATE_FULL_CONTROL_SESSION, 
              p_session_obj->cvd_session_name,
              sizeof( p_session_obj->cvd_session_name ) );
        if ( rc )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                  "vs_voc_standby_sequencer(): Faield to communicate with "
                  "MVM with rc=(0x%08x)", rc );
          p_seqjob_obj->status = rc;

          rc = vs_mem_free_object( ( vs_object_t* ) p_simple_job_obj );
          VS_PANIC_ON_ERROR( rc );

          p_seqjob_obj->state = VS_STANDBY_SEQUENCER_ENUM_COMPLETE;
        }
        else
        {
          p_seqjob_obj->state = VS_STANDBY_SEQUENCER_ENUM_CREATE_MVM_WAIT;
        }
      }
      continue;

    case VS_STANDBY_SEQUENCER_ENUM_CREATE_MVM_WAIT:
      {
        /* Wait for the MVM session to be created. */
        p_simple_job_obj = &p_seqjob_obj->subjob_obj->simple_job;

        if ( p_simple_job_obj->is_completed == FALSE )
        {
          return VS_EPENDING;
        }

        p_seqjob_obj->status = p_session_obj->session_ctrl.status;

        rc = vs_mem_free_object( ( vs_object_t* ) p_simple_job_obj );
        VS_PANIC_ON_ERROR( rc );
        p_simple_job_obj = NULL;

        if ( p_seqjob_obj->status == VS_EOK ) 
        {
          p_seqjob_obj->state = 
                    VS_STANDBY_SEQUENCER_ENUM_SET_MODEM_VOICE_CONTROL;
        }
        else
        {
          p_seqjob_obj->state = VS_STANDBY_SEQUENCER_ENUM_COMPLETE;
        }
      }
      continue;

    case VS_STANDBY_SEQUENCER_ENUM_SET_MODEM_VOICE_CONTROL:
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,   \
              "VS_STANDBY_SEQUENCER_ENUM_SET_MODEM_VOICE_CONTROL  "
              "handle=(0x%08x)", p_session_obj->header.handle );
        /* Set Modem voice control to TRUE. */
        rc = vs_create_autofree_token_object( &p_token_obj );
        VS_PANIC_ON_ERROR( rc );

        modem_voice_ctrl_args.enable_flag = TRUE;

        rc = __aprv2_cmd_alloc_send(
            vs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
            vs_my_addr, ( (uint16_t) p_session_obj->header.handle ),
            vs_mvm_addr, p_session_obj->mvm_port,
            p_token_obj->header.handle, 
            VSS_IMVM_CMD_SET_MODEM_VOICE_CONTROL,
            &modem_voice_ctrl_args, sizeof( modem_voice_ctrl_args ) );
        if ( rc )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                  "vs_voc_standby_sequencer(): Faield to communicate with "
                  "MVM with rc=(0x%08x)", rc );
          p_seqjob_obj->status = rc;

          rc = vs_mem_free_object( ( vs_object_t* ) p_token_obj );
          VS_PANIC_ON_ERROR( rc );

          p_seqjob_obj->state = VS_STANDBY_SEQUENCER_ENUM_COMPLETE;
        }
        else
        { /* Move to the next sequencer state. */
          p_seqjob_obj->state = VS_STANDBY_SEQUENCER_ENUM_CREATE_CVS;
        }
      }
      continue;

    case VS_STANDBY_SEQUENCER_ENUM_CREATE_CVS:
      {
        /* Create the CVS session. */
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,   \
              "VS_STANDBY_SEQUENCER_ENUM_CREATE_CVS  handle=(0x%08x)",
              p_session_obj->header.handle );
        rc = vs_create_simple_job_object( p_session_obj->header.handle, 
                                          &p_simple_job_obj );
        VS_PANIC_ON_ERROR( rc );
        p_simple_job_obj->fn_table[ VS_RESPONSE_FN_ENUM_RESULT ] = 
                                         vs_voc_create_cvs_session_result_rsp_fn;
        p_seqjob_obj->subjob_obj = ( ( vs_object_t* ) p_simple_job_obj );

        rc = __aprv2_cmd_alloc_ext(
              vs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
              vs_my_addr, ( (uint16_t) p_session_obj->header.handle ),
              vs_cvs_addr, APR_NULL_V,
              p_simple_job_obj->header.handle,
              VSS_ISTREAM_CMD_CREATE_FULL_CONTROL_SESSION,
              ( sizeof( vss_istream_cmd_create_full_control_session_t ) +
              sizeof( p_session_obj->cvd_session_name ) ),
              &p_packet );
        VS_PANIC_ON_ERROR( rc );

        p_full_control = APRV2_PKT_GET_PAYLOAD( 
                                 vss_istream_cmd_create_full_control_session_t,
                                 p_packet );
        p_full_control->direction = 2;
        p_full_control->enc_media_type = VSS_MEDIA_ID_NONE;
        p_full_control->dec_media_type = VSS_MEDIA_ID_NONE;
        p_full_control->network_id = VSS_NETWORK_ID_DEFAULT;

        p_dst_stream_name = ( ( char_t* )
                  APR_PTR_END_OF(
                  p_full_control,
                  sizeof( vss_istream_cmd_create_full_control_session_t ) ) );

        mmstd_memcpy( p_dst_stream_name, sizeof( p_session_obj->cvd_session_name ),
                      p_session_obj->cvd_session_name, sizeof( p_session_obj->cvd_session_name ) );

        rc = __aprv2_cmd_forward( vs_apr_handle, p_packet );
        if ( rc )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                  "vs_voc_standby_sequencer(): Faield to communicate with "
                  "CVS with rc=(0x%08x)", rc );
          p_seqjob_obj->status = rc;

          rc = vs_mem_free_object( ( vs_object_t* ) p_simple_job_obj );
          VS_PANIC_ON_ERROR( rc );

          p_seqjob_obj->state = VS_STANDBY_SEQUENCER_ENUM_COMPLETE;
        }
        else
        {
          p_seqjob_obj->state = VS_STANDBY_SEQUENCER_ENUM_CREATE_CVS_WAIT;
        }
      }
      continue;

    case VS_STANDBY_SEQUENCER_ENUM_CREATE_CVS_WAIT:
      {
        /* Wait for the CVS session to be created. */
        p_simple_job_obj = &p_seqjob_obj->subjob_obj->simple_job;

        if ( p_simple_job_obj->is_completed == FALSE )
        {
          return VS_EPENDING;
        }

        p_seqjob_obj->status = p_session_obj->session_ctrl.status;

        rc = vs_mem_free_object( ( vs_object_t* ) p_simple_job_obj );
        VS_PANIC_ON_ERROR( rc );
        p_simple_job_obj = NULL;

        if ( p_seqjob_obj->status == VS_EOK ) 
        {
          p_seqjob_obj->state = VS_STANDBY_SEQUENCER_ENUM_ATTACH_STREAM;
        }
        else
        {
          p_seqjob_obj->state = VS_STANDBY_SEQUENCER_ENUM_COMPLETE;
        }

      }
      continue;

    case VS_STANDBY_SEQUENCER_ENUM_ATTACH_STREAM:
      {
        /* Attach the CVS session to the MVM. */
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,   \
              "VS_STANDBY_SEQUENCER_ENUM_ATTACH_STREAM  handle=(0x%08x)",
              p_session_obj->header.handle );
        rc = vs_create_simple_job_object( p_session_obj->header.handle, 
                                          &p_simple_job_obj );
        VS_PANIC_ON_ERROR( rc );
        p_simple_job_obj->fn_table[ VS_RESPONSE_FN_ENUM_RESULT ] = 
                                         vs_simple_transition_result_rsp_fn;
        p_seqjob_obj->subjob_obj = ( ( vs_object_t* ) p_simple_job_obj );

        rc = __aprv2_cmd_alloc_send(
              vs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
              vs_my_addr, ( (uint16_t) p_session_obj->header.handle ),
              vs_mvm_addr, p_session_obj->mvm_port,
              p_simple_job_obj->header.handle, VSS_IMVM_CMD_ATTACH_STREAM,
              &p_session_obj->cvs_port, sizeof( p_session_obj->cvs_port ) );
        if ( rc )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                  "vs_voc_standby_sequencer(): Faield to communicate with "
                  "MVM with rc=(0x%08x)", rc );
          p_seqjob_obj->status = rc;

          rc = vs_mem_free_object( ( vs_object_t* ) p_simple_job_obj );
          VS_PANIC_ON_ERROR( rc );

          p_seqjob_obj->state = VS_STANDBY_SEQUENCER_ENUM_COMPLETE;
        }
        else
        {
          p_seqjob_obj->state = VS_STANDBY_SEQUENCER_ENUM_ATTACH_STREAM_WAIT;
        }
      }
      continue;

    case VS_STANDBY_SEQUENCER_ENUM_ATTACH_STREAM_WAIT:
      {
        /* Wait for the MVM session to be created. */
        p_simple_job_obj = &p_seqjob_obj->subjob_obj->simple_job;

        if ( p_simple_job_obj->is_completed == FALSE )
        {
          return VS_EPENDING;
        }

        p_seqjob_obj->status = p_session_obj->session_ctrl.status;

        rc = vs_mem_free_object( ( vs_object_t* ) p_simple_job_obj );
        VS_PANIC_ON_ERROR( rc );
        p_simple_job_obj = NULL;

        if ( p_seqjob_obj->status == VS_EOK ) 
        {
          p_seqjob_obj->state = VS_STANDBY_SEQUENCER_ENUM_STANDBY_VOICE;
        }
        else
        {
          p_seqjob_obj->state = VS_STANDBY_SEQUENCER_ENUM_COMPLETE;
        }
      }
      continue;

    case VS_STANDBY_SEQUENCER_ENUM_STANDBY_VOICE:
      {
        /* STANDBY_VOICE on MVM. */
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,   \
              "VS_STANDBY_SEQUENCER_ENUM_STANDBY_VOICE  handle=(0x%08x)",
              p_session_obj->header.handle );
        rc = vs_create_simple_job_object( p_session_obj->header.handle, 
                                          &p_simple_job_obj );
        VS_PANIC_ON_ERROR( rc );

        p_seqjob_obj->subjob_obj = ( ( vs_object_t* ) p_simple_job_obj );
        p_simple_job_obj->fn_table[ VS_RESPONSE_FN_ENUM_RESULT ] = 
                                          vs_simple_transition_result_rsp_fn;
        rc = __aprv2_cmd_alloc_send(
              vs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
              vs_my_addr, ( (uint16_t) p_session_obj->header.handle ),
              vs_mvm_addr, p_session_obj->mvm_port,
              p_simple_job_obj->header.handle,
              VSS_IMVM_CMD_MODEM_STANDBY_VOICE, NULL, 0 );
        if ( rc )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                  "vs_voc_standby_sequencer(): Faield to communicate with "
                  "MVM with rc=(0x%08x)", rc );
          p_seqjob_obj->status = rc;

          rc = vs_mem_free_object( ( vs_object_t* ) p_simple_job_obj );
          VS_PANIC_ON_ERROR( rc );

          p_seqjob_obj->state = VS_STANDBY_SEQUENCER_ENUM_COMPLETE;
        }
        else
        {
          p_seqjob_obj->state = VS_STANDBY_SEQUENCER_ENUM_STANDBY_VOICE_WAIT;
        }
      }
      continue;

    case VS_STANDBY_SEQUENCER_ENUM_STANDBY_VOICE_WAIT:
      {
        /* Wait for MVM to STOP_VOICE. */
        p_simple_job_obj = &p_seqjob_obj->subjob_obj->simple_job;

        if ( p_simple_job_obj->is_completed == FALSE )
        {
          return VS_EPENDING;
        }
        
        p_seqjob_obj->status = p_session_obj->session_ctrl.status;

        rc = vs_mem_free_object( ( vs_object_t* ) p_simple_job_obj );
        VS_PANIC_ON_ERROR( rc );
        p_simple_job_obj = NULL;

        if ( p_seqjob_obj->status == VS_EOK ) 
        {
          p_seqjob_obj->state = VS_STANDBY_SEQUENCER_ENUM_SET_MEDIA_TYPE;
        }
        else
        {
          p_seqjob_obj->state = VS_STANDBY_SEQUENCER_ENUM_COMPLETE;
        }
      }
      continue;

    case VS_STANDBY_SEQUENCER_ENUM_SET_MEDIA_TYPE:
      {
        /* Set media type on CVS to indicate the vocoders used. */
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,   \
              "VS_STANDBY_SEQUENCER_ENUM_SET_MEDIA_TYPE  handle=(0x%08x)",
              p_session_obj->header.handle );

        rc = vs_create_simple_job_object( p_session_obj->header.handle, 
                                          &p_simple_job_obj );
        VS_PANIC_ON_ERROR( rc );

        p_seqjob_obj->subjob_obj = ( ( vs_object_t* ) p_simple_job_obj );
        p_simple_job_obj->fn_table[ VS_RESPONSE_FN_ENUM_RESULT ] = 
                                      vs_simple_transition_result_rsp_fn;

        set_media_type.rx_media_id =
             vs_voc_map_to_cvd_mode( p_session_obj->media_id );
        set_media_type.tx_media_id =
             vs_voc_map_to_cvd_mode( p_session_obj->media_id );

        rc = __aprv2_cmd_alloc_send(
               vs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               vs_my_addr, ( (uint16_t) p_session_obj->header.handle ),
               vs_cvs_addr, p_session_obj->cvs_port,
               p_simple_job_obj->header.handle, VSS_ISTREAM_CMD_SET_MEDIA_TYPE,
               &set_media_type, sizeof( set_media_type) );
        if ( rc )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                  "vs_voc_standby_sequencer(): Faield to communicate with "
                  "CVS with rc=(0x%08x)", rc );
          p_seqjob_obj->status = rc;

          rc = vs_mem_free_object( ( vs_object_t* ) p_simple_job_obj );
          VS_PANIC_ON_ERROR( rc );

          p_seqjob_obj->state = VS_STANDBY_SEQUENCER_ENUM_COMPLETE;
        }
        else
        {
          p_seqjob_obj->state = VS_STANDBY_SEQUENCER_ENUM_SET_MEDIA_TYPE_WAIT;
        }
      }
      continue;

    case VS_STANDBY_SEQUENCER_ENUM_SET_MEDIA_TYPE_WAIT:
      {
        /* Wait for set media type to complete */
        p_simple_job_obj = &p_seqjob_obj->subjob_obj->simple_job;

        if ( p_simple_job_obj->is_completed == FALSE )
        {
          return VS_EPENDING;
        }

        p_seqjob_obj->status = p_session_obj->session_ctrl.status;

        rc = vs_mem_free_object( ( vs_object_t* ) p_simple_job_obj );
        VS_PANIC_ON_ERROR( rc );
        p_simple_job_obj = NULL;

        if ( p_seqjob_obj->status == VS_EOK ) 
        {
          p_seqjob_obj->state = VS_STANDBY_SEQUENCER_ENUM_RECONFIG;
        }
        else
        {
          p_seqjob_obj->state = VS_STANDBY_SEQUENCER_ENUM_COMPLETE;
        }
      }
      continue;

    case VS_STANDBY_SEQUENCER_ENUM_RECONFIG:
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,   \
              "VS_STANDBY_SEQUENCER_ENUM_RECONFIG  handle=(0x%08x)",
              p_session_obj->header.handle );
        /* Set network on MVM to be used for calibrating the attached
         * stream/vocproc. 
         */
        vs_voc_set_mvm_network_proc( p_standby_cmd_params->handle );

        /* Set media type on MVM to be used for calibrating the attached
         * stream/vocproc.
         */
        vs_voc_set_mvm_media_type_proc( p_standby_cmd_params->handle );

        /* Set voice timing parameters. */
        vs_voc_set_voice_timing_proc( p_standby_cmd_params->handle );

        p_seqjob_obj->state = VS_STANDBY_SEQUENCER_ENUM_SET_CACHED_VALUES;
      }
      continue;

    case VS_STANDBY_SEQUENCER_ENUM_SET_CACHED_VALUES:
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,   \
              "VS_STANDBY_SEQUENCER_ENUM_SET_CACHED_VALUES"
              "  handle=(0x%08x)", p_session_obj->header.handle );
        /* Send data commands to stream. */
        switch( p_session_obj->media_id )
        { 
        case VS_COMMON_MEDIA_ID_FR: 
          rc = 
            vs_vocfr_set_cached_voc_property( p_standby_cmd_params->handle );
          break;

        case VS_COMMON_MEDIA_ID_HR: 
          rc = 
            vs_vochr_set_cached_voc_property( p_standby_cmd_params->handle );
          break;

        case VS_COMMON_MEDIA_ID_EFR:   
          rc = 
            vs_vocefr_set_cached_voc_property( p_standby_cmd_params->handle );
          break;

        case VS_COMMON_MEDIA_ID_AMR: 
          rc = 
            vs_vocamr_set_cached_voc_property( p_standby_cmd_params->handle );
          break;

        case VS_COMMON_MEDIA_ID_AMRWB: 
          rc = 
            vs_vocamrwb_set_cached_voc_property( p_standby_cmd_params->handle );
          break;
        }
        
        /* Update the session object configured state to FALSE */
        p_session_obj->is_configured = FALSE;
        p_seqjob_obj->state = VS_STANDBY_SEQUENCER_ENUM_COMPLETE;
      }
      continue;

    case VS_STANDBY_SEQUENCER_ENUM_STOP_VOICE:
      {
        /* STANDBY_VOICE on MVM. */
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,   \
              "VS_STANDBY_SEQUENCER_ENUM_STOP_VOICE  handle=(0x%08x)",
              p_session_obj->header.handle );
        rc = vs_create_simple_job_object( p_session_obj->header.handle, 
                                          &p_simple_job_obj );
        VS_PANIC_ON_ERROR( rc );

        p_seqjob_obj->subjob_obj = ( ( vs_object_t* ) p_simple_job_obj );
        p_simple_job_obj->fn_table[ VS_RESPONSE_FN_ENUM_RESULT ] = 
                                          vs_simple_transition_result_rsp_fn;
        rc = __aprv2_cmd_alloc_send(
              vs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
              vs_my_addr, ( (uint16_t) p_session_obj->header.handle ),
              vs_mvm_addr, p_session_obj->mvm_port,
              p_simple_job_obj->header.handle,
              VSS_IMVM_CMD_MODEM_STOP_VOICE, NULL, 0 );
        if ( rc )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                  "vs_voc_standby_sequencer(): Faield to communicate with "
                  "MVM with rc=(0x%08x)", rc );
          p_seqjob_obj->status = rc;

          rc = vs_mem_free_object( ( vs_object_t* ) p_simple_job_obj );
          VS_PANIC_ON_ERROR( rc );

          p_seqjob_obj->state = VS_STANDBY_SEQUENCER_ENUM_COMPLETE;
        }
        else
        {
          p_seqjob_obj->state = VS_STANDBY_SEQUENCER_ENUM_STOP_VOICE_WAIT;
        }
      }
      continue;

    case VS_STANDBY_SEQUENCER_ENUM_STOP_VOICE_WAIT:
      {
        /* Wait for MVM to STOP_VOICE. */
        p_simple_job_obj = &p_seqjob_obj->subjob_obj->simple_job;

        if ( p_simple_job_obj->is_completed == FALSE )
        {
          return VS_EPENDING;
        }

        p_seqjob_obj->status = p_session_obj->session_ctrl.status;

        rc = vs_mem_free_object( ( vs_object_t* ) p_simple_job_obj );
        VS_PANIC_ON_ERROR( rc );
        p_simple_job_obj = NULL;

        p_seqjob_obj->state = VS_STANDBY_SEQUENCER_ENUM_COMPLETE;
      }
      continue;

    case VS_STANDBY_SEQUENCER_ENUM_COMPLETE:
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,   \
              "VS_STANDBY_SEQUENCER_ENUM_COMPLETE  handle=(0x%08x)",
              p_session_obj->header.handle );
        if ( p_seqjob_obj->status == VS_EOK )
        {
          #ifdef WINSIM
          if( ( phase_num == 1 ) || ( phase_num == 4 ) || ( phase_num == 3 ) )
          {
            if ( p_session_obj->stream_ready == TRUE )
            {
              vs_common_send_event( p_session_obj->header.handle, NULL, 
                                    VS_COMMON_EVENT_NOT_READY, VS_EOK ) ; 
              p_session_obj->stream_ready = FALSE;
            }
            vs_voc_state_control( p_session_obj );
          }
          #endif
        }

        if ( p_seqjob_obj->status == VS_EOK )
        {
          p_session_obj->stream_standby = TRUE;
        }

        vs_common_send_event( p_session_obj->header.handle, p_standby_cmd_pkt,
                              VS_COMMON_EVENT_CMD_RESPONSE, 
                              p_seqjob_obj->status );
        
        /* Free the memory for sequencer object. */
        rc = vs_mem_free_object( ( vs_object_t* ) p_seqjob_obj );
        VS_PANIC_ON_ERROR( rc );
        p_seqjob_obj = NULL;
        
        /* free the p_standby_cmd_params memory
         * allocated in vs_voc_cmd_standby
         */
        if ( p_standby_cmd_params != NULL )
        {
          /* Free the memeory - p_standby_cmd_params. */
          apr_memmgr_free( &vs_heapmgr, p_standby_cmd_params );
          p_standby_cmd_params = NULL;
        }

        /* free the p_standby_cmd_pkt memory
         * allocated in vs_voc_cmd_standby
         */
        if ( p_standby_cmd_pkt != NULL )
        {
          /* Free the memeory - p_standby_cmd_pkt. */
          apr_memmgr_free( &vs_heapmgr, p_standby_cmd_pkt );
          p_standby_cmd_pkt = NULL;
        }

      } 
      return VS_EOK;

    default:
      VS_PANIC_ON_ERROR( VS_EUNEXPECTED );
      break;
    }  /* end of switch ( p_seqjob_obj->state )  */
  }  /* end of for ( ;; )  */

  return VS_EOK;

}  /* end of vs_voc_standby_sequencer() */

enum { 
  VS_ENABLE_SEQUENCER_ENUM_UNINITIALIZED, 
  VS_ENABLE_SEQUENCER_ENUM_CREATE_MVM, /* Entry. */ 
  VS_ENABLE_SEQUENCER_ENUM_CREATE_MVM_WAIT, 
  VS_ENABLE_SEQUENCER_ENUM_SET_MODEM_VOICE_CONTROL,
  VS_ENABLE_SEQUENCER_ENUM_CREATE_CVS, 
  VS_ENABLE_SEQUENCER_ENUM_CREATE_CVS_WAIT, 
  VS_ENABLE_SEQUENCER_ENUM_ATTACH_STREAM, 
  VS_ENABLE_SEQUENCER_ENUM_ATTACH_STREAM_WAIT, 
  VS_ENABLE_SEQUENCER_ENUM_STANDBY_VOICE, /* Entry. */ 
  VS_ENABLE_SEQUENCER_ENUM_STANDBY_VOICE_WAIT, 
  VS_ENABLE_SEQUENCER_ENUM_SET_MEDIA_TYPE, 
  VS_ENABLE_SEQUENCER_ENUM_SET_MEDIA_TYPE_WAIT, 
  VS_ENABLE_SEQUENCER_ENUM_RECONFIG, 
  VS_ENABLE_SEQUENCER_ENUM_SET_CACHED_VALUES, 
  VS_ENABLE_SEQUENCER_ENUM_RESYNC_CTM, 
  VS_ENABLE_SEQUENCER_ENUM_START_VOICE, 
  VS_ENABLE_SEQUENCER_ENUM_START_VOICE_WAIT, 
  VS_ENABLE_SEQUENCER_ENUM_COMPLETE, /* Exit. */ 
  VS_ENABLE_SEQUENCER_ENUM_INVALID 
};

static uint32_t vs_voc_enable_sequencer ( 
  vs_gating_control_t* ctrl
) 
{

  uint32_t rc = VS_EOK; 
  vs_session_object_t* p_session_obj = NULL; 
  vs_sequencer_job_object_t* p_seqjob_obj = NULL; 
  vs_simple_job_object_t* p_simple_job_obj = NULL; 
  vs_token_object_t* p_token_obj = NULL; 
  aprv2_packet_t* p_packet = NULL; 
  char_t* p_dst_stream_name = NULL;
  vs_cmd_packet_t* p_enable_cmd_pkt = NULL; 
  vs_voc_cmd_enable_t* p_enable_cmd_params = NULL; 

  vss_istream_cmd_create_full_control_session_t* p_full_control = NULL; 
  vss_istream_cmd_set_media_type_t set_media_type;
  vss_imvm_cmd_set_modem_voice_control_t modem_voice_ctrl_args;

  /* get the handle */
  p_enable_cmd_pkt = ctrl->packet; 
  p_enable_cmd_params = ( vs_voc_cmd_enable_t* )p_enable_cmd_pkt->params; 


  /* Get the session object with respect to the handle */ 
  rc = vs_get_typed_object( p_enable_cmd_params->handle,
                            VS_OBJECT_TYPE_ENUM_SESSION, 
                            ( ( vs_object_t** ) &p_session_obj ) );
  VS_PANIC_ON_ERROR( rc );

  p_seqjob_obj = p_session_obj->session_ctrl.pendjob_seq_ptr;

  if ( ( vs_voc_is_cvd_ready == FALSE ) || 
       ( vs_voc_is_ssr_cleanup_done == FALSE ) )
  {
    p_seqjob_obj->status = VS_ENOTREADY;
    p_seqjob_obj->state = VS_ENABLE_SEQUENCER_ENUM_COMPLETE;
    p_session_obj->is_first_call = TRUE;
  }

  for ( ;; ) 
  { 
    switch ( p_seqjob_obj->state ) 
    {
    case VS_ENABLE_SEQUENCER_ENUM_CREATE_MVM:
      {
        /* Create the MVM session. */
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,   \
              "VS_ENABLE_SEQUENCER_ENUM_CREATE_MVM  handle=(0x%08x)",
              p_session_obj->header.handle );
        rc = vs_create_simple_job_object( p_session_obj->header.handle, 
                                          &p_simple_job_obj );
        VS_PANIC_ON_ERROR( rc );

        p_seqjob_obj->subjob_obj = ( ( vs_object_t* ) p_simple_job_obj );
        p_simple_job_obj->fn_table[ VS_RESPONSE_FN_ENUM_RESULT ] = 
                                      vs_voc_create_mvm_session_result_rsp_fn;

        rc = __aprv2_cmd_alloc_send(
              vs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
              vs_my_addr, ( (uint16_t) p_session_obj->header.handle ),
              vs_mvm_addr, APR_NULL_V,
              p_simple_job_obj->header.handle, 
              VSS_IMVM_CMD_CREATE_FULL_CONTROL_SESSION, 
              p_session_obj->cvd_session_name,
              sizeof( p_session_obj->cvd_session_name ) );
        if ( rc )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                  "vs_voc_enable_sequencer(): Faield to communicate with "
                  "MVM with rc=(0x%08x)", rc );
          p_seqjob_obj->status = rc;

          rc = vs_mem_free_object( ( vs_object_t* ) p_simple_job_obj );
          VS_PANIC_ON_ERROR( rc );

          p_seqjob_obj->state = VS_ENABLE_SEQUENCER_ENUM_COMPLETE;
        }
        else
        {
          p_seqjob_obj->state = VS_ENABLE_SEQUENCER_ENUM_CREATE_MVM_WAIT;
        }
      }
      continue;

    case VS_ENABLE_SEQUENCER_ENUM_CREATE_MVM_WAIT:
      {
        /* Wait for the MVM session to be created. */
        p_simple_job_obj = &p_seqjob_obj->subjob_obj->simple_job;

        if ( p_simple_job_obj->is_completed == FALSE )
        {
          return VS_EPENDING;
        }

        p_seqjob_obj->status = p_session_obj->session_ctrl.status;
        
        rc = vs_mem_free_object( ( vs_object_t* ) p_simple_job_obj );
        VS_PANIC_ON_ERROR( rc );
        p_simple_job_obj = NULL;

        if ( p_seqjob_obj->status == VS_EOK )
        {
          p_seqjob_obj->state = 
                  VS_ENABLE_SEQUENCER_ENUM_SET_MODEM_VOICE_CONTROL;
        }
        else
        {
          p_seqjob_obj->state = VS_ENABLE_SEQUENCER_ENUM_COMPLETE;
        }
        
      }
      continue;

    case VS_ENABLE_SEQUENCER_ENUM_SET_MODEM_VOICE_CONTROL:
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,   \
              "VS_ENABLE_SEQUENCER_ENUM_SET_MODEM_VOICE_CONTROL  "
              "handle=(0x%08x)", p_session_obj->header.handle );
        /* Set Modem voice control to TRUE. */
        rc = vs_create_autofree_token_object( &p_token_obj );
        VS_PANIC_ON_ERROR( rc );

        modem_voice_ctrl_args.enable_flag = TRUE;

        rc = __aprv2_cmd_alloc_send(
            vs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
            vs_my_addr, ( (uint16_t) p_session_obj->header.handle ),
            vs_mvm_addr, p_session_obj->mvm_port,
            p_token_obj->header.handle, 
            VSS_IMVM_CMD_SET_MODEM_VOICE_CONTROL,
            &modem_voice_ctrl_args, sizeof( modem_voice_ctrl_args ) );
        if ( rc )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                  "vs_voc_enable_sequencer(): Faield to communicate with "
                  "MVM with rc=(0x%08x)", rc );
          p_seqjob_obj->status = rc;

          rc = vs_mem_free_object( ( vs_object_t* ) p_token_obj );
          VS_PANIC_ON_ERROR( rc );

          p_seqjob_obj->state = VS_ENABLE_SEQUENCER_ENUM_COMPLETE;
        }
        else
        { /* Move to the next sequencer state. */
          p_seqjob_obj->state = VS_ENABLE_SEQUENCER_ENUM_CREATE_CVS;
        }
      }
      continue;

    case VS_ENABLE_SEQUENCER_ENUM_CREATE_CVS:
      {
        /* Create the CVS session. */
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,   \
              "VS_ENABLE_SEQUENCER_ENUM_CREATE_CVS  handle=(0x%08x)",
              p_session_obj->header.handle );
        rc = vs_create_simple_job_object( p_session_obj->header.handle, 
                                          &p_simple_job_obj );
        VS_PANIC_ON_ERROR( rc );

        p_simple_job_obj->fn_table[ VS_RESPONSE_FN_ENUM_RESULT ] = 
                                         vs_voc_create_cvs_session_result_rsp_fn;
        p_seqjob_obj->subjob_obj = ( ( vs_object_t* ) p_simple_job_obj );

        rc = __aprv2_cmd_alloc_ext(
              vs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
              vs_my_addr, ( (uint16_t) p_session_obj->header.handle ),
              vs_cvs_addr, APR_NULL_V,
              p_simple_job_obj->header.handle,
              VSS_ISTREAM_CMD_CREATE_FULL_CONTROL_SESSION,
              ( sizeof( vss_istream_cmd_create_full_control_session_t ) +
              sizeof( p_session_obj->cvd_session_name ) ),
              &p_packet );
        VS_PANIC_ON_ERROR( rc );

        p_full_control = APRV2_PKT_GET_PAYLOAD( 
                                   vss_istream_cmd_create_full_control_session_t,
                                   p_packet );
        p_full_control->direction = 2;
        p_full_control->enc_media_type = VSS_MEDIA_ID_NONE;
        p_full_control->dec_media_type = VSS_MEDIA_ID_NONE;
        p_full_control->network_id = VSS_NETWORK_ID_DEFAULT;

        p_dst_stream_name = ( ( char_t* )
                  APR_PTR_END_OF(
                  p_full_control,
                  sizeof( vss_istream_cmd_create_full_control_session_t ) ) );

        mmstd_memcpy( p_dst_stream_name, sizeof( p_session_obj->cvd_session_name ),
                      p_session_obj->cvd_session_name, sizeof( p_session_obj->cvd_session_name ) );

        rc = __aprv2_cmd_forward( vs_apr_handle, p_packet );
        if ( rc )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                  "vs_voc_enable_sequencer(): Faield to communicate with "
                  "CVS with rc=(0x%08x)", rc );
          p_seqjob_obj->status = rc;

          rc = vs_mem_free_object( ( vs_object_t* ) p_simple_job_obj );
          VS_PANIC_ON_ERROR( rc );

          p_seqjob_obj->state = VS_ENABLE_SEQUENCER_ENUM_COMPLETE;
        }
        else
        {
          p_seqjob_obj->state = VS_ENABLE_SEQUENCER_ENUM_CREATE_CVS_WAIT;
        }
      }
      continue;

    case VS_ENABLE_SEQUENCER_ENUM_CREATE_CVS_WAIT:
      {
        /* Wait for the CVS session to be created. */
        p_simple_job_obj = &p_seqjob_obj->subjob_obj->simple_job;

        if ( p_simple_job_obj->is_completed == FALSE )
        {
          return VS_EPENDING;
        }

        p_seqjob_obj->status = p_session_obj->session_ctrl.status;

        rc = vs_mem_free_object( ( vs_object_t* ) p_simple_job_obj );
        VS_PANIC_ON_ERROR( rc );
        p_simple_job_obj = NULL;

        if ( p_seqjob_obj->status == VS_EOK )
        {
          p_seqjob_obj->state = VS_ENABLE_SEQUENCER_ENUM_ATTACH_STREAM;
        }
        else
        {
          p_seqjob_obj->state = VS_ENABLE_SEQUENCER_ENUM_COMPLETE;
        }
      }
      continue;

    case VS_ENABLE_SEQUENCER_ENUM_ATTACH_STREAM:
      {
        /* Attach the CVS session to the MVM. */
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,   \
              "VS_ENABLE_SEQUENCER_ENUM_ATTACH_STREAM  handle=(0x%08x)",
              p_session_obj->header.handle );
        rc = vs_create_simple_job_object( p_session_obj->header.handle, 
                                          &p_simple_job_obj );
        VS_PANIC_ON_ERROR( rc );
        p_simple_job_obj->fn_table[ VS_RESPONSE_FN_ENUM_RESULT ] = 
                                         vs_simple_transition_result_rsp_fn;
        p_seqjob_obj->subjob_obj = ( ( vs_object_t* ) p_simple_job_obj );

        rc = __aprv2_cmd_alloc_send(
              vs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
              vs_my_addr, ( (uint16_t) p_session_obj->header.handle ),
              vs_mvm_addr, p_session_obj->mvm_port,
              p_simple_job_obj->header.handle, VSS_IMVM_CMD_ATTACH_STREAM,
              &p_session_obj->cvs_port, sizeof( p_session_obj->cvs_port ) );
        if ( rc )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                  "vs_voc_enable_sequencer(): Faield to communicate with "
                  "MVM with rc=(0x%08x)", rc );
          p_seqjob_obj->status = rc;

          rc = vs_mem_free_object( ( vs_object_t* ) p_simple_job_obj );
          VS_PANIC_ON_ERROR( rc );

          p_seqjob_obj->state = VS_ENABLE_SEQUENCER_ENUM_COMPLETE;
        }
        else
        {
          p_seqjob_obj->state = VS_ENABLE_SEQUENCER_ENUM_ATTACH_STREAM_WAIT;
        }
      }
      continue;

    case VS_ENABLE_SEQUENCER_ENUM_ATTACH_STREAM_WAIT:
      {
        /* Wait for the MVM session to be created. */
        p_simple_job_obj = &p_seqjob_obj->subjob_obj->simple_job;

        if ( p_simple_job_obj->is_completed == FALSE )
        {
          return VS_EPENDING;
        }

        p_seqjob_obj->status = p_session_obj->session_ctrl.status;

        rc = vs_mem_free_object( ( vs_object_t* ) p_simple_job_obj );
        VS_PANIC_ON_ERROR( rc );
        p_simple_job_obj = NULL;

        if ( p_seqjob_obj->status == VS_EOK )
        {
          p_seqjob_obj->state = VS_ENABLE_SEQUENCER_ENUM_STANDBY_VOICE;
        }
        else
        {
          p_seqjob_obj->state = VS_ENABLE_SEQUENCER_ENUM_COMPLETE;
        }
      }
      continue;

    case VS_ENABLE_SEQUENCER_ENUM_STANDBY_VOICE:
      {
        /* STANDBY_VOICE on MVM. */
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,   \
              "VS_ENABLE_SEQUENCER_ENUM_STANDBY_VOICE  handle=(0x%08x)",
              p_session_obj->header.handle );
        rc = vs_create_simple_job_object( p_session_obj->header.handle, 
                                          &p_simple_job_obj );
        VS_PANIC_ON_ERROR( rc );

        p_seqjob_obj->subjob_obj = ( ( vs_object_t* ) p_simple_job_obj );
        p_simple_job_obj->fn_table[ VS_RESPONSE_FN_ENUM_RESULT ] = 
                                          vs_simple_transition_result_rsp_fn;
        rc = __aprv2_cmd_alloc_send(
              vs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
              vs_my_addr, ( (uint16_t) p_session_obj->header.handle ),
              vs_mvm_addr, p_session_obj->mvm_port,
              p_simple_job_obj->header.handle,
              VSS_IMVM_CMD_MODEM_STANDBY_VOICE, NULL, 0 );
        if ( rc )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                  "vs_voc_enable_sequencer(): Faield to communicate with "
                  "MVM with rc=(0x%08x)", rc );
          p_seqjob_obj->status = rc;

          rc = vs_mem_free_object( ( vs_object_t* ) p_simple_job_obj );
          VS_PANIC_ON_ERROR( rc );

          p_seqjob_obj->state = VS_ENABLE_SEQUENCER_ENUM_COMPLETE;
        }
        else
        {
          p_seqjob_obj->state = VS_ENABLE_SEQUENCER_ENUM_STANDBY_VOICE_WAIT;
        }
      }
      continue;

    case VS_ENABLE_SEQUENCER_ENUM_STANDBY_VOICE_WAIT:
      {
        /* Wait for MVM to STOP_VOICE. */
        p_simple_job_obj = &p_seqjob_obj->subjob_obj->simple_job;

        if ( p_simple_job_obj->is_completed == FALSE )
        {
          return VS_EPENDING;
        }

        p_seqjob_obj->status = p_session_obj->session_ctrl.status;

        rc = vs_mem_free_object( ( vs_object_t* ) p_simple_job_obj );
        VS_PANIC_ON_ERROR( rc );
        p_simple_job_obj = NULL;

        if ( p_seqjob_obj->status == VS_EOK )
        {
          p_seqjob_obj->state = VS_ENABLE_SEQUENCER_ENUM_SET_MEDIA_TYPE;
        }
        else
        {
          p_seqjob_obj->state = VS_ENABLE_SEQUENCER_ENUM_COMPLETE;
        }
      }
      continue;

    case VS_ENABLE_SEQUENCER_ENUM_SET_MEDIA_TYPE:
      {
        /* Set media type on CVS to indicate the vocoders used. */
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,   \
              "VS_ENABLE_SEQUENCER_ENUM_SET_MEDIA_TYPE  handle=(0x%08x)",
              p_session_obj->header.handle );

        rc = vs_create_simple_job_object( p_session_obj->header.handle, 
                                          &p_simple_job_obj );
        VS_PANIC_ON_ERROR( rc );

        p_seqjob_obj->subjob_obj = ( ( vs_object_t* ) p_simple_job_obj );
        p_simple_job_obj->fn_table[ VS_RESPONSE_FN_ENUM_RESULT ] = 
                                      vs_simple_transition_result_rsp_fn;

        set_media_type.rx_media_id =
             vs_voc_map_to_cvd_mode( p_session_obj->media_id );
        set_media_type.tx_media_id =
             vs_voc_map_to_cvd_mode( p_session_obj->media_id );
     
        rc = __aprv2_cmd_alloc_send(
               vs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               vs_my_addr, ( (uint16_t) p_session_obj->header.handle ),
               vs_cvs_addr, p_session_obj->cvs_port,
               p_simple_job_obj->header.handle, VSS_ISTREAM_CMD_SET_MEDIA_TYPE,
               &set_media_type, sizeof( set_media_type) );
        if ( rc )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                  "vs_voc_enable_sequencer(): Faield to communicate with "
                  "CVS with rc=(0x%08x)", rc );
          p_seqjob_obj->status = rc;

          rc = vs_mem_free_object( ( vs_object_t* ) p_simple_job_obj );
          VS_PANIC_ON_ERROR( rc );

          p_seqjob_obj->state = VS_ENABLE_SEQUENCER_ENUM_COMPLETE;
        }
        else
        {
          p_seqjob_obj->state = VS_ENABLE_SEQUENCER_ENUM_SET_MEDIA_TYPE_WAIT;
        }
      }
      continue;

    case VS_ENABLE_SEQUENCER_ENUM_SET_MEDIA_TYPE_WAIT:
      {
        /* Wait for set media type to complete */
        p_simple_job_obj = &p_seqjob_obj->subjob_obj->simple_job;

        if ( p_simple_job_obj->is_completed == FALSE )
        {
          return VS_EPENDING;
        }

        p_seqjob_obj->status = p_session_obj->session_ctrl.status;

        rc = vs_mem_free_object( ( vs_object_t* ) p_simple_job_obj );
        VS_PANIC_ON_ERROR( rc );
        p_simple_job_obj = NULL;

        if ( p_seqjob_obj->status == VS_EOK )
        {
          p_seqjob_obj->state = VS_ENABLE_SEQUENCER_ENUM_RECONFIG;
        }
        else
        {
          p_seqjob_obj->state = VS_ENABLE_SEQUENCER_ENUM_COMPLETE;
        }
      }
      continue;

    case VS_ENABLE_SEQUENCER_ENUM_RECONFIG:
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,   \
              "VS_ENABLE_SEQUENCER_ENUM_RECONFIG  handle=(0x%08x)",
              p_session_obj->header.handle );
        /* Set network on MVM to be used for calibrating the attached
         * stream/vocproc. 
         */
        vs_voc_set_mvm_network_proc( p_enable_cmd_params->handle );

        /* Set media type on MVM to be used for calibrating the attached
         * stream/vocproc.
         */
        vs_voc_set_mvm_media_type_proc( p_enable_cmd_params->handle );

        /* Set voice timing parameters. */
        vs_voc_set_voice_timing_proc( p_enable_cmd_params->handle );

        p_seqjob_obj->state = VS_ENABLE_SEQUENCER_ENUM_SET_CACHED_VALUES;
      }
      continue;

    case VS_ENABLE_SEQUENCER_ENUM_SET_CACHED_VALUES:
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,   \
              "VS_ENABLE_SEQUENCER_ENUM_SET_CACHED_VALUES  handle= (0x%08x)",
              p_session_obj->header.handle );
        /* Send data commands to stream. */
        switch( p_session_obj->media_id )
        { 
        case VS_COMMON_MEDIA_ID_FR: 
          rc = 
            vs_vocfr_set_cached_voc_property( p_enable_cmd_params->handle );
          break;

        case VS_COMMON_MEDIA_ID_HR: 
          rc = 
            vs_vochr_set_cached_voc_property( p_enable_cmd_params->handle );
          break;

        case VS_COMMON_MEDIA_ID_EFR:   
          rc = 
            vs_vocefr_set_cached_voc_property( p_enable_cmd_params->handle );
          break;

        case VS_COMMON_MEDIA_ID_AMR: 
          rc =
            vs_vocamr_set_cached_voc_property( p_enable_cmd_params->handle );
          break;

        case VS_COMMON_MEDIA_ID_AMRWB: 
          rc = 
            vs_vocamrwb_set_cached_voc_property( p_enable_cmd_params->handle );
          break;
         
        default:
          break;
        }  /* end of switch(session_obj->media_id) */

        p_seqjob_obj->state = VS_ENABLE_SEQUENCER_ENUM_RESYNC_CTM;
      }
      continue;

    case VS_ENABLE_SEQUENCER_ENUM_RESYNC_CTM:
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,   \
              "VS_ENABLE_SEQUENCER_ENUM_RESYNC_CTM  handle=(0x%08x)",
              p_session_obj->header.handle );

          // Send RESYNC_CTM to CVS. 
          rc = vs_create_autofree_token_object( &p_token_obj );
          VS_PANIC_ON_ERROR( rc );

          rc = __aprv2_cmd_alloc_send(
                 vs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                 vs_my_addr, ( (uint16_t) p_session_obj->header.handle ),
                 vs_cvs_addr, p_session_obj->cvs_port,
                 p_token_obj->header.handle,
                 VSS_ISTREAM_CMD_RESYNC_CTM, NULL, 0 );
          if ( rc )
          {
            MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                    "vs_voc_enable_sequencer(): Faield to communicate with "
                    "CVS with rc=(0x%08x)", rc );
            p_seqjob_obj->status = rc;

            rc = vs_mem_free_object( ( vs_object_t* ) p_token_obj );
            VS_PANIC_ON_ERROR( rc );

            p_seqjob_obj->state = VS_ENABLE_SEQUENCER_ENUM_COMPLETE;

            continue;
          }


        p_seqjob_obj->state = VS_ENABLE_SEQUENCER_ENUM_START_VOICE;
      }
      continue;

    case VS_ENABLE_SEQUENCER_ENUM_START_VOICE:
      {
        /* START_VOICE on MVM. */
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,   \
               "VS_ENABLE_SEQUENCER_ENUM_START_VOICE  handle=(0x%08x)",
               p_session_obj->header.handle );
        rc = vs_create_simple_job_object( p_session_obj->header.handle, 
                                          &p_simple_job_obj );
        VS_PANIC_ON_ERROR( rc );

        p_seqjob_obj->subjob_obj = ( ( vs_object_t* ) p_simple_job_obj );
        p_simple_job_obj->fn_table[ VS_RESPONSE_FN_ENUM_RESULT ] = 
                                  vs_simple_transition_result_rsp_fn;

        rc = __aprv2_cmd_alloc_send(
               vs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               vs_my_addr, ( (uint16_t) p_session_obj->header.handle ),
               vs_mvm_addr, p_session_obj->mvm_port,
               p_simple_job_obj->header.handle,
               VSS_IMVM_CMD_MODEM_START_VOICE, NULL, 0 );
        if ( rc )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                  "vs_voc_enable_sequencer(): Faield to communicate with "
                  "MVM with rc=(0x%08x)", rc );
          p_seqjob_obj->status = rc;

          rc = vs_mem_free_object( ( vs_object_t* ) p_simple_job_obj );
          VS_PANIC_ON_ERROR( rc );

          p_seqjob_obj->state = VS_ENABLE_SEQUENCER_ENUM_COMPLETE;
        }
        else
        {
          p_seqjob_obj->state = VS_ENABLE_SEQUENCER_ENUM_START_VOICE_WAIT;
        }
      }
      continue;

    case VS_ENABLE_SEQUENCER_ENUM_START_VOICE_WAIT:
      {
        /* Wait for MVM to START_VOICE. */
        p_simple_job_obj = &p_seqjob_obj->subjob_obj->simple_job;

       if ( p_simple_job_obj->is_completed == FALSE )
        {
          return VS_EPENDING;
        }

        p_seqjob_obj->status = p_session_obj->session_ctrl.status;
        /* Update the session object configured state to TRUE */
        p_session_obj->is_configured = TRUE;

        rc = vs_mem_free_object( ( vs_object_t* ) p_simple_job_obj );
        VS_PANIC_ON_ERROR( rc );
        p_simple_job_obj = NULL;

        p_seqjob_obj->state = VS_ENABLE_SEQUENCER_ENUM_COMPLETE;
      }
      continue;

    case VS_ENABLE_SEQUENCER_ENUM_COMPLETE:
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,   \
              "VS_ENABLE_SEQUENCER_ENUM_COMPLETE  handle=(0x%08x)",
              p_session_obj->header.handle );
        if ( p_seqjob_obj->status == VS_EOK )
        {
          #ifdef WINSIM
          if( (phase_num == 1) || (phase_num == 4)|| (phase_num == 3) )
          {
            vs_common_send_event( p_session_obj->header.handle, NULL, 
                                  VS_COMMON_EVENT_READY, VS_EOK ) ; 
            p_session_obj->stream_ready = TRUE;
            vs_voc_state_control( p_session_obj );
          }
          #endif
        }

        p_session_obj->stream_standby = FALSE;
        vs_common_send_event( p_session_obj->header.handle, p_enable_cmd_pkt,
                              VS_COMMON_EVENT_CMD_RESPONSE,
                              p_seqjob_obj->status );
        
        /* Free the memory for sequencer object. */
        rc = vs_mem_free_object( ( vs_object_t* ) p_seqjob_obj );
        VS_PANIC_ON_ERROR( rc );
        p_seqjob_obj = NULL;
        
        /* free the p_enable_cmd_params memory
         * allocated in vs_voc_cmd_enable
         */
        if ( p_enable_cmd_params != NULL )
        {
          /* Free the memeory - enable_cmd_pkt params. */
          apr_memmgr_free( &vs_heapmgr, p_enable_cmd_params );
          p_enable_cmd_params = NULL;
        }

        /* free the p_enable_cmd_pkt memory
         * allocated in vs_voc_cmd_enable
         */
        if ( p_enable_cmd_pkt != NULL )
        {
          /* Free the memeory - p_enable_cmd_pkt. */
          apr_memmgr_free( &vs_heapmgr, p_enable_cmd_pkt );
          p_enable_cmd_pkt = NULL;
        }
      }  
      return VS_EOK;

    default:
      VS_PANIC_ON_ERROR( VS_EUNEXPECTED );
      break;

    }  /* end of switch ( p_seqjob_obj->state )  */
  }  /* end of for ( ;; )  */

  return VS_EOK;

}  /* end of vs_voc_enable_sequencer() */

VS_INTERNAL uint32_t vs_voc_cmd_enable_proc ( 
  vs_gating_control_t* ctrl 
) 
{

  uint32_t rc = VS_EOK; 
  vs_cmd_packet_t* p_enable_cmd_pkt = NULL;
  vs_voc_cmd_enable_t* p_enable_cmd_params = NULL;
  vs_session_object_t* p_session_obj = NULL;
  vs_sequencer_job_object_t* p_seqjob_obj = NULL;
 
  p_enable_cmd_pkt = ctrl->packet;
  if( p_enable_cmd_pkt == NULL )
  {
    VS_PANIC_ON_ERROR( VS_EUNEXPECTED );
  }

  p_enable_cmd_params = ( vs_voc_cmd_enable_t * )p_enable_cmd_pkt->params; 
 
  if ( p_enable_cmd_params != NULL )
  {
    /* validate the session handle */ 
    rc = vs_get_typed_object( p_enable_cmd_params->handle, 
                              VS_OBJECT_TYPE_ENUM_SESSION,
                              ( ( vs_object_t** ) &p_session_obj ) ); 
    VS_PANIC_ON_ERROR( rc );

    if ( rc == VS_EOK )
    {
      /* validate the media_id */
      switch ( p_enable_cmd_params->media_id )
      {
      case VS_COMMON_MEDIA_ID_AMR:
      case VS_COMMON_MEDIA_ID_AMRWB:
      case VS_COMMON_MEDIA_ID_EFR:
      case VS_COMMON_MEDIA_ID_FR:
      case VS_COMMON_MEDIA_ID_HR:
        p_session_obj->media_id = p_enable_cmd_params->media_id; 
        break;

      default:
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_voc_cmd_enable_proc(): " \
                "Unsupported Media_ID=(0x%08x)",
                p_enable_cmd_params->media_id );
        rc = VS_EBADPARAM;
        break;
      }  /* end of switch ( p_enable_cmd_params->media_id ) */
    }
  }
  else
  {
    VS_PANIC_ON_ERROR( VS_EUNEXPECTED );
  }

  if ( rc == VS_EOK )
  {
    if( ctrl->state == VS_GATING_CMD_STATE_ENUM_EXECUTE ) 
    {
      p_session_obj->pending_cmd = VS_VOC_CMD_ENABLE;

      rc = vs_create_sequencer_job_object( 
                           ( vs_sequencer_job_object_t** ) &ctrl->rootjob_obj );
      VS_PANIC_ON_ERROR( rc ); 
      
      p_seqjob_obj = &ctrl->rootjob_obj->sequencer_job;
      p_session_obj->session_ctrl.pendjob_seq_ptr = p_seqjob_obj;

      switch( p_session_obj->state ) 
      {
      case VS_VOC_STATE_ENUM_RESET:
        {
          /* have to handle this case - <TBD> */
          MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "ADSP is not booted up yet; " \
                "VS_VOC_CMD_ENABLE is cached");
          rc = VS_ENOTREADY;
        }
        break;
        
      case VS_VOC_STATE_ENUM_IDLE: 
        {
          /* go through the whole ENABLE sequence beginning with
           * CREATE_MVM if this is the first call,  
           * STANDBY_VOICE if this is not the first call.
           */
            if ( p_session_obj->is_first_call == TRUE )
            {
              p_session_obj->is_first_call = FALSE;
              ctrl->rootjob_obj->sequencer_job.state = 
                                VS_ENABLE_SEQUENCER_ENUM_CREATE_MVM;
            }
            else
            {
              ctrl->rootjob_obj->sequencer_job.state = 
                                VS_ENABLE_SEQUENCER_ENUM_SET_MEDIA_TYPE;
            }
        }
        break;
       
      case VS_VOC_STATE_ENUM_RUN: 
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "Calling VS_VOC_CMD_ENABLE " \
                "more than once is not permitted" );
          MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "Client must first call " \
                "VS_VOC_CMD_DISABLE before calling"
                "VS_VOC_CMD_ENABLE" );
          rc = VS_EFAILED;
        }
        break; 
        
      default: 
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_voc_cmd_enable(): " \
                "unexpected state" );
          VS_PANIC_ON_ERROR( VS_EUNEXPECTED ); 
        }
        break;
      }  /* end of switch( session_obj->state ) */

    }  /* end of if( ctrl->state == VS_PENDING_CMD_STATE_ENUM_EXECUTE ) */
  }  /* end of if ( rc == VS_EOK ) */

  switch ( rc )
  {
  case VS_EOK:
    {
      rc = vs_voc_enable_sequencer( ctrl ); 
    }
    break;

  case VS_EFAILED:
  case VS_ENOTREADY:
    {
      rc = vs_mem_free_object( ( vs_object_t* )p_seqjob_obj ); 
      VS_PANIC_ON_ERROR( rc ); 
      p_seqjob_obj = NULL; 
      ctrl->rootjob_obj = NULL; 
    }
    /*-fallthru */

  case VS_EBADPARAM:
    {
      if ( p_enable_cmd_params != NULL )
      {
        /* Free the memeory - enable_cmd_params. */
        apr_memmgr_free( &vs_heapmgr, p_enable_cmd_params );
        p_enable_cmd_params = NULL;
      }

      /* Inform command status to client */
      vs_common_send_event( p_session_obj->header.handle, p_enable_cmd_pkt,
                            VS_COMMON_EVENT_CMD_RESPONSE, rc );

      if ( p_enable_cmd_pkt != NULL )
      {
        /* Free the memeory - p_enable_cmd_pkt. */
        apr_memmgr_free( &vs_heapmgr, p_enable_cmd_pkt );
        p_enable_cmd_pkt = NULL;
      }
    }
    break;

  default:
    break;
  }  /* end of switch ( rc ) */

  return rc;
  
}  /* end of vs_voc_cmd_enable_proc() */

VS_INTERNAL uint32_t vs_voc_helper_destroy_proc( 
  vs_sequencer_job_object_t* p_seqjob_obj,
  vs_session_object_t* p_session_obj 
)
{

 uint32_t rc = VS_EOK;
 vs_simple_job_object_t* p_simple_job_obj = NULL; 

  enum {
    VS_DESTROY_SEQUENCER_ENUM_DETACH_STREAM,/* Entry */  
    VS_DESTROY_SEQUENCER_ENUM_DETACH_STREAM_WAIT,
    VS_DESTROY_SEQUENCER_ENUM_DESTROY_CVS,
    VS_DESTROY_SEQUENCER_ENUM_DESTROY_CVS_WAIT,
    VS_DESTROY_SEQUENCER_ENUM_DESTROY_MVM,
    VS_DESTROY_SEQUENCER_ENUM_DESTROY_MVM_WAIT,
    VS_DESTROY_SEQUENCER_ENUM_COMPLETE /* Exit */
  };

  if( p_seqjob_obj->helper_state == APR_UNDEFINED_ID_V )
  {
    p_seqjob_obj->helper_state = VS_DESTROY_SEQUENCER_ENUM_DETACH_STREAM;
  }

  for( ; ; )
  {
    switch ( p_seqjob_obj->helper_state )
    {
    case VS_DESTROY_SEQUENCER_ENUM_DETACH_STREAM:
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,   \
              "VS_DESTROY_SEQUENCER_ENUM_DETACH_STREAM  handle=(0x%08x)",
              p_session_obj->header.handle );
        if ( p_session_obj->cvs_port == APR_NULL_V )
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                "vs_voc_helper_destroy_proc(): cvs port is NULL, "
                "skipping detach stream" );
          p_seqjob_obj->helper_state = VS_DESTROY_SEQUENCER_ENUM_DESTROY_MVM;
        }
        else
        {
          rc = vs_create_simple_job_object( p_session_obj->header.handle, 
                                            &p_simple_job_obj );
          VS_PANIC_ON_ERROR( rc );
 
          p_seqjob_obj->subjob_obj = ( ( vs_object_t* ) p_simple_job_obj );
          p_simple_job_obj->fn_table[ VS_RESPONSE_FN_ENUM_RESULT ] = 
                                      vs_simple_transition_result_rsp_fn;
 
          rc = __aprv2_cmd_alloc_send(
                 vs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                 vs_my_addr, ( (uint16_t) p_session_obj->header.handle ),
                 vs_mvm_addr, p_session_obj->mvm_port,
                 p_simple_job_obj->header.handle, VSS_IMVM_CMD_DETACH_STREAM,
                 &p_session_obj->cvs_port, sizeof( p_session_obj->cvs_port ) );
          if ( rc )
          {
            MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                    "vs_voc_helper_destroy_proc(): Faield to communicate with " \
                    "MVM with rc=(0x%08x)", rc );

            rc = vs_mem_free_object( ( vs_object_t* ) p_simple_job_obj );
            VS_PANIC_ON_ERROR( rc );
 
            p_seqjob_obj->helper_state = VS_DESTROY_SEQUENCER_ENUM_COMPLETE;
          }
          else
          {
            p_seqjob_obj->helper_state = VS_DESTROY_SEQUENCER_ENUM_DETACH_STREAM_WAIT;
          }
        }
      }
      continue;
 
    case VS_DESTROY_SEQUENCER_ENUM_DETACH_STREAM_WAIT:
      {
        /* Wait for CVS to destroy. */
        p_simple_job_obj = &p_seqjob_obj->subjob_obj->simple_job;
 
        if ( p_simple_job_obj->is_completed == FALSE )
        {
          rc = VS_EPENDING;
          break;
        }
 
        p_seqjob_obj->status = p_session_obj->session_ctrl.status;
        rc = vs_mem_free_object( ( vs_object_t* ) p_simple_job_obj );
        VS_PANIC_ON_ERROR( rc );
        p_simple_job_obj = NULL;
 
        if ( p_seqjob_obj->status == VS_EOK )
        {
          p_seqjob_obj->helper_state = VS_DESTROY_SEQUENCER_ENUM_DESTROY_CVS;
        }
        else
        {
          p_seqjob_obj->helper_state = VS_DESTROY_SEQUENCER_ENUM_COMPLETE;
        }
      }
      continue;
 
    case VS_DESTROY_SEQUENCER_ENUM_DESTROY_CVS:
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,   \
              "VS_DESTROY_SEQUENCER_ENUM_DESTROY_CVS  handle=(0x%08x)",
              p_session_obj->header.handle );
        if ( p_session_obj->cvs_port == APR_NULL_V )
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                "vs_voc_helper_destroy_proc(): cvs port is NULL, "
                "skipping destroy stream" );
          p_seqjob_obj->helper_state = VS_DESTROY_SEQUENCER_ENUM_DESTROY_MVM;
        }
        else
        {
          rc = vs_create_simple_job_object( p_session_obj->header.handle,
                                            &p_simple_job_obj );
          VS_PANIC_ON_ERROR( rc );
 
          p_seqjob_obj->subjob_obj = ( ( vs_object_t* ) p_simple_job_obj );
          p_simple_job_obj->fn_table[ VS_RESPONSE_FN_ENUM_RESULT ] = 
                                        vs_simple_transition_result_rsp_fn;
 
          rc = __aprv2_cmd_alloc_send(
                 vs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                 vs_my_addr, ( (uint16_t) p_session_obj->header.handle ),
                 vs_cvs_addr, p_session_obj->cvs_port,
                 p_simple_job_obj->header.handle,
                 APRV2_IBASIC_CMD_DESTROY_SESSION,
                 NULL, 0 );
          if ( rc )
          {
            MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                    "vs_voc_helper_destroy_proc(): Faield to communicate with " \
                    "CVS with rc=(0x%08x)", rc );
 
            rc = vs_mem_free_object( ( vs_object_t* ) p_simple_job_obj );
            VS_PANIC_ON_ERROR( rc );
 
            p_seqjob_obj->helper_state = VS_DESTROY_SEQUENCER_ENUM_COMPLETE;
          }
          else
          {
             p_seqjob_obj->helper_state = VS_DESTROY_SEQUENCER_ENUM_DESTROY_CVS_WAIT;
          }
        }
      }
      continue;
 
    case VS_DESTROY_SEQUENCER_ENUM_DESTROY_CVS_WAIT:
      {
        /* Wait for CVS to destroy. */
        p_simple_job_obj = &p_seqjob_obj->subjob_obj->simple_job;
 
        if ( p_simple_job_obj->is_completed == FALSE )
        {
          rc = VS_EPENDING;
          break;
        }
 
        p_session_obj->cvs_port = APR_NULL_V;
        p_seqjob_obj->status = p_session_obj->session_ctrl.status;
        rc = vs_mem_free_object( ( vs_object_t* ) p_simple_job_obj );
        VS_PANIC_ON_ERROR( rc );
        p_simple_job_obj = NULL;
 
        if ( p_seqjob_obj->status == VS_EOK )
        {
          p_seqjob_obj->helper_state = VS_DESTROY_SEQUENCER_ENUM_DESTROY_MVM;
        }
        else
        {
          p_seqjob_obj->helper_state = VS_DESTROY_SEQUENCER_ENUM_COMPLETE;
        }
      }
      continue;
 
     case VS_DESTROY_SEQUENCER_ENUM_DESTROY_MVM:
       {
         MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,   \
               "VS_DESTROY_SEQUENCER_ENUM_DESTROY_MVM  handle=(0x%08x)",
               p_session_obj->header.handle );
         if ( p_session_obj->mvm_port == APR_NULL_V )
         {
           MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                 "vs_voc_helper_destroy_proc(): mvm port is NULL, "
                 "skipping destroy mvm_port`" );
           p_seqjob_obj->helper_state = VS_DESTROY_SEQUENCER_ENUM_COMPLETE;
         }
         else
         {
           rc = vs_create_simple_job_object( p_session_obj->header.handle,
                                             &p_simple_job_obj );
           VS_PANIC_ON_ERROR( rc );
 
           p_seqjob_obj->subjob_obj = ( ( vs_object_t* ) p_simple_job_obj );
           p_simple_job_obj->fn_table[ VS_RESPONSE_FN_ENUM_RESULT ] = 
                                        vs_simple_transition_result_rsp_fn;
 
           rc = __aprv2_cmd_alloc_send(
                  vs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                  vs_my_addr, ( (uint16_t) p_session_obj->header.handle ),
                  vs_mvm_addr, p_session_obj->mvm_port,
                  p_simple_job_obj->header.handle, APRV2_IBASIC_CMD_DESTROY_SESSION,
                  NULL, 0 );
           if ( rc )
           {
             MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                     "vs_voc_helper_destroy_proc(): Faield to communicate " \
                     "with MVM with rc=(0x%08x)", rc );
 
             rc = vs_mem_free_object( ( vs_object_t* ) p_simple_job_obj );
             VS_PANIC_ON_ERROR( rc );
 
             p_seqjob_obj->helper_state = VS_DESTROY_SEQUENCER_ENUM_COMPLETE;
           }
           else
           {
             p_seqjob_obj->helper_state = VS_DESTROY_SEQUENCER_ENUM_DESTROY_MVM_WAIT;
           }
         }
       }
       continue;
 
    case VS_DESTROY_SEQUENCER_ENUM_DESTROY_MVM_WAIT:
      { /* Wait for the MVM state machine to finish. */
        p_simple_job_obj = &p_seqjob_obj->subjob_obj->simple_job;
 
        if ( p_simple_job_obj->is_completed == FALSE )
        {
          rc = VS_EPENDING;
          break;
        }
 
        p_session_obj->mvm_port = APR_NULL_V;
        p_seqjob_obj->status = p_session_obj->session_ctrl.status;
        rc = vs_mem_free_object( ( vs_object_t* ) p_simple_job_obj );
        VS_PANIC_ON_ERROR( rc );
        p_simple_job_obj = NULL;
 
        p_seqjob_obj->helper_state = VS_DESTROY_SEQUENCER_ENUM_COMPLETE;
      }
      continue;
 
    case VS_DESTROY_SEQUENCER_ENUM_COMPLETE:
         rc = APR_EOK; 
         break;
 
    default:
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_voc_helper_destroy_proc(): " \
              "unexpected state" );
        VS_PANIC_ON_ERROR( VS_EUNEXPECTED );
      }
      break;
 
    }  /* end of switch ( p_seqjob_obj->state ) */
    break;
  }

  return rc;
}

VS_INTERNAL uint32_t vs_voc_cmd_disable_proc ( 
  vs_gating_control_t* ctrl 
)
{ 

  uint32_t rc = VS_EOK; 
  uint32_t _rc = VS_EOK;
  vs_cmd_packet_t* p_disable_cmd_pkt = NULL;
  vs_voc_cmd_disable_t* p_disable_cmd_params = NULL;
  vs_sequencer_job_object_t* p_seqjob_obj = NULL;
  vs_simple_job_object_t* p_simple_job_obj = NULL;
  vs_session_object_t* p_session_obj = NULL; 
  voicecfg_cmd_get_bool_item_t item;

  enum {
    VS_DISABLE_SEQUENCER_ENUM_UNINITIALIZED,
    VS_DISABLE_SEQUENCER_ENUM_STOP_VOICE,/* Entry. */
    VS_DISABLE_SEQUENCER_ENUM_STOP_VOICE_WAIT,
    VS_DISABLE_SEQUENCER_ENUM_DESTROY_WAIT,
    VS_DISABLE_SEQUENCER_ENUM_COMPLETE, /* Exit. */
    VS_DISABLE_SEQUENCER_ENUM_INVALID
  };

  /* get the disable command params */ 
  p_disable_cmd_pkt = ctrl->packet;
  if ( p_disable_cmd_pkt == NULL )
  {
    VS_PANIC_ON_ERROR( VS_EUNEXPECTED );
  }

  p_disable_cmd_params = ( ( vs_voc_cmd_disable_t* )p_disable_cmd_pkt->params ); 
  if ( p_disable_cmd_params != NULL )
  {
    /* validate the session handle */ 
    rc = vs_get_typed_object( p_disable_cmd_params->handle, 
                              VS_OBJECT_TYPE_ENUM_SESSION,
                              ( ( vs_object_t** ) &p_session_obj ) ); 
    VS_PANIC_ON_ERROR( rc );

    if ( rc == VS_EOK )
    {
      /* Check in what state is the control */ 
      if ( ctrl->state == VS_GATING_CMD_STATE_ENUM_EXECUTE )
      {
        p_session_obj->pending_cmd = VS_VOC_CMD_DISABLE;

        rc = vs_create_sequencer_job_object(
               ( vs_sequencer_job_object_t** ) &ctrl->rootjob_obj );
        VS_PANIC_ON_ERROR( rc );

        ctrl->rootjob_obj->sequencer_job.state = 
                              VS_DISABLE_SEQUENCER_ENUM_STOP_VOICE;
      } 

      p_seqjob_obj = &ctrl->rootjob_obj->sequencer_job;
      p_session_obj->session_ctrl.pendjob_seq_ptr = p_seqjob_obj;

      switch ( p_session_obj->state )
      {
      case VS_VOC_STATE_ENUM_RESET:
        {
          /* have to handle this case - <TBD> */
          MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "ADSP is not booted up yet; " \
                "VS_VOC_CMD_DISABLE is cached");
          rc = VS_ENOTREADY;
        }
        break;
      case VS_VOC_STATE_ENUM_IDLE:
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,  "VS_VOC_CMD_DISABLE in IDLE " \
              "state; call MVM_STOP_VOICE" );
        }
        break;

      case VS_VOC_STATE_ENUM_RUN:
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,  "VS_VOC_CMD_DISABLE in RUN " \
              "state; call MVM_STOP_VOICE" );
        }
        break;

      default:
        break;

      }  /* end of switch ( session_obj->state ) */
    }  /* end of if ( rc == VS_EOK ) */
  }
  else
  {
    VS_PANIC_ON_ERROR( VS_EUNEXPECTED );
  }  /* end of if ( p_disable_cmd_params != NULL ) */

  if ( rc == VS_EOK )
  {
    if ( ( vs_voc_is_cvd_ready == FALSE ) || 
         ( vs_voc_is_ssr_cleanup_done == FALSE ) )
    {
      p_session_obj->session_ctrl.status = VS_EOK;
      p_seqjob_obj->state = VS_DISABLE_SEQUENCER_ENUM_COMPLETE;
    }

    for ( ;; )
    {
      switch ( p_seqjob_obj->state )
      {
      case VS_DISABLE_SEQUENCER_ENUM_STOP_VOICE:
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,   \
                "VS_DISABLE_SEQUENCER_ENUM_STOP_VOICE  handle=(0x%08x)",
                p_session_obj->header.handle );
          /* If there is no valid MVM handle, complete the sequence. */
          if ( p_session_obj->mvm_port == APR_NULL_V )
          {
            MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                  "vs_voc_cmd_disable_proc(): mvm session handle is NULL, "
                  "complete disable sequence" );
            p_session_obj->session_ctrl.status = VS_EOK;
            p_seqjob_obj->state = VS_DISABLE_SEQUENCER_ENUM_COMPLETE;
          }
          else
          {
            /* STOP_VOICE on MVM. */
            rc = vs_create_simple_job_object( p_session_obj->header.handle, 
                                              &p_simple_job_obj );
            VS_PANIC_ON_ERROR( rc );

            p_seqjob_obj->subjob_obj = ( ( vs_object_t* ) p_simple_job_obj );
            p_simple_job_obj->fn_table[ VS_RESPONSE_FN_ENUM_RESULT ] = 
                                         vs_simple_transition_result_rsp_fn;

            rc = __aprv2_cmd_alloc_send(
                       vs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                       vs_my_addr, ( (uint16_t) p_session_obj->header.handle ),
                       vs_mvm_addr, p_session_obj->mvm_port,
                       p_simple_job_obj->header.handle,
                       VSS_IMVM_CMD_MODEM_STOP_VOICE, NULL, 0 );
            if ( rc )
            {
              MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                      "vs_voc_cmd_disable_proc(): Faield to communicate with "
                      "MVM with rc=(0x%08x)", rc );

              rc = vs_mem_free_object( ( vs_object_t* ) p_simple_job_obj );
              VS_PANIC_ON_ERROR( rc );

              p_session_obj->session_ctrl.status = VS_EOK;
              p_seqjob_obj->state = VS_DISABLE_SEQUENCER_ENUM_COMPLETE;
            }
            else
            {
              p_seqjob_obj->state = VS_DISABLE_SEQUENCER_ENUM_STOP_VOICE_WAIT;
            }
          }
        }
        continue;

      case VS_DISABLE_SEQUENCER_ENUM_STOP_VOICE_WAIT:
        {
          /* Wait for MVM to STOP_VOICE. */
          p_simple_job_obj = &p_seqjob_obj->subjob_obj->simple_job;

          if ( p_simple_job_obj->is_completed == FALSE )
          {
            rc = VS_EPENDING;
            break;
          }

          item.id = VOICECFG_CFG_IS_RETAIN_CVD_AFTER_RELEASE_OPTIMIZATION_DISABLED;
          item.ret_value = FALSE;
          _rc = voicecfg_call( VOICECFG_CMD_GET_BOOL_ITEM, &item, sizeof ( item ) );

          if ( !_rc && ( item.ret_value == TRUE ) )
          {
            p_seqjob_obj->status = p_session_obj->session_ctrl.status;
            p_seqjob_obj->state = VS_DISABLE_SEQUENCER_ENUM_COMPLETE;
          }
          else
          {
            p_session_obj->is_first_call = TRUE;
            p_seqjob_obj->state = VS_DISABLE_SEQUENCER_ENUM_DESTROY_WAIT;
            p_seqjob_obj->helper_state = APR_UNDEFINED_ID_V;
          }

          /* Free the memory for simple job object.*/
          rc = vs_mem_free_object( ( vs_object_t* ) p_simple_job_obj );
          VS_PANIC_ON_ERROR( rc );
          p_simple_job_obj = NULL;
        }
        continue;
     
      case VS_DISABLE_SEQUENCER_ENUM_DESTROY_WAIT: 
        {
          rc = vs_voc_helper_destroy_proc( p_seqjob_obj, p_session_obj);
          if( rc )
          { 
            break;
          }
          else
          {
            p_seqjob_obj->state = VS_DISABLE_SEQUENCER_ENUM_COMPLETE;
            continue;
          }
        }
      case VS_DISABLE_SEQUENCER_ENUM_COMPLETE:
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,   \
                "VS_DISABLE_SEQUENCER_ENUM_COMPLETE  handle=(0x%08x)",
                p_session_obj->header.handle );
          /* Free the memory for sequencer job object. */
          rc = vs_mem_free_object( ( vs_object_t* ) p_seqjob_obj );
          VS_PANIC_ON_ERROR( rc );
          p_seqjob_obj = NULL;

          #ifdef WINSIM
          if ( (phase_num == 1) )
          {
            p_session_obj->stream_ready = FALSE;
            vs_voc_state_control( p_session_obj );
          }
          #endif 

          p_session_obj->stream_standby = FALSE;
          rc = p_session_obj->session_ctrl.status;

          vs_common_send_event( p_session_obj->header.handle, 
                                p_disable_cmd_pkt, 
                                VS_COMMON_EVENT_CMD_RESPONSE, rc );

          /* Update the session object configured state to FALSE */
          p_session_obj->is_configured = FALSE;

          /* Reset the vocoder properties */
          p_session_obj->current_voc.amr.codec_mode = 
                          ( vs_vocamr_codec_mode_t )VS_VOCAMR_CODEC_MODE_NONE_V;
          p_session_obj->current_voc.amr.dtx_enable = VS_VOC_DTX_MODE_UNDEFINED_V;
          p_session_obj->current_voc.amrwb.codec_mode = 
                       ( vs_vocamrwb_codec_mode_t )VS_VOCAMRWB_CODEC_MODE_NONE_V;
          p_session_obj->current_voc.amrwb.dtx_enable = VS_VOC_DTX_MODE_UNDEFINED_V;
          p_session_obj->current_voc.efr.dtx_enable = VS_VOC_DTX_MODE_UNDEFINED_V;
          p_session_obj->current_voc.fr.dtx_enable = VS_VOC_DTX_MODE_UNDEFINED_V;
          p_session_obj->current_voc.hr.dtx_enable = VS_VOC_DTX_MODE_UNDEFINED_V;
          p_session_obj->pending_voc.amr.codec_mode =
                          ( vs_vocamr_codec_mode_t )VS_VOCAMR_CODEC_MODE_NONE_V;
          p_session_obj->pending_voc.amr.dtx_enable = VS_VOC_DTX_MODE_UNDEFINED_V;
          p_session_obj->pending_voc.amrwb.codec_mode =
                       ( vs_vocamrwb_codec_mode_t )VS_VOCAMRWB_CODEC_MODE_NONE_V;
          p_session_obj->pending_voc.amrwb.dtx_enable = VS_VOC_DTX_MODE_UNDEFINED_V;
          p_session_obj->pending_voc.efr.dtx_enable = VS_VOC_DTX_MODE_UNDEFINED_V;
          p_session_obj->pending_voc.fr.dtx_enable = VS_VOC_DTX_MODE_UNDEFINED_V;
          p_session_obj->pending_voc.hr.dtx_enable = VS_VOC_DTX_MODE_UNDEFINED_V;

          rc = VS_EOK;
          {
            /* free the p_disable_cmd_params memory
             * allocated in vs_voc_cmd_disable
             */
            if ( p_disable_cmd_params != NULL )
            {
              /* Free the memeory - p_disable_cmd_params. */
              apr_memmgr_free( &vs_heapmgr, p_disable_cmd_params );
              p_disable_cmd_params = NULL;
            }

            /* free the p_disable_cmd_pkt memory
             * allocated in vs_voc_cmd_disable
             */
            if ( p_disable_cmd_pkt != NULL )
            {
              /* Free the memeory - p_disable_cmd_pkt. */
              apr_memmgr_free( &vs_heapmgr, p_disable_cmd_pkt );
              p_disable_cmd_pkt = NULL;
            }
          }
        }
        break;

      default:
        VS_PANIC_ON_ERROR( VS_EUNEXPECTED );
        break;

      }  /* end of switch ( p_seqjob_obj->state ) */
      break;
    }  /* end of for ( ;; ) */
  }  /* end of if ( rc == VS_EOK ) */

  switch ( rc )
  {
  case VS_ENOTREADY:
    {
      /* Free the memory for sequencer job object. */
      rc = vs_mem_free_object( ( vs_object_t* ) p_seqjob_obj );
      VS_PANIC_ON_ERROR( rc );
      p_seqjob_obj = NULL;
    }
    /*-fallthru */

  case VS_EBADPARAM:
    {
      /* free the p_disable_cmd_params memory
       * allocated in vs_voc_cmd_disable
       */
      if ( p_disable_cmd_params != NULL )
      {
        /* Free the memeory - p_disable_cmd_params. */
        apr_memmgr_free( &vs_heapmgr, p_disable_cmd_params );
        p_disable_cmd_params = NULL;
      }

      /* Inform command status to client */
      vs_common_send_event( p_session_obj->header.handle, p_disable_cmd_pkt, 
                            VS_COMMON_EVENT_CMD_RESPONSE, rc );

      /* free the p_disable_cmd_pkt memory
       * allocated in vs_voc_cmd_disable
       */
      if ( p_disable_cmd_pkt != NULL )
      {
        /* Free the memeory - p_disable_cmd_pkt. */
        apr_memmgr_free( &vs_heapmgr, p_disable_cmd_pkt );
        p_disable_cmd_pkt = NULL;
      }
    }
    break;

  case VS_EPENDING:
  default:
    break;
  }  /* end of switch ( rc ) */

  return rc;

}  /*  end of vs_voc_cmd_disable_proc () */

VS_INTERNAL uint32_t vs_voc_cmd_standby_proc (
  vs_gating_control_t* ctrl
)
{

  uint32_t rc = VS_EOK;
  vs_cmd_packet_t* p_standby_cmd_pkt = NULL;
  vs_voc_cmd_standby_t* p_standby_cmd_params = NULL;
  vs_session_object_t* p_session_obj = NULL; 
  vs_sequencer_job_object_t* p_seqjob_obj = NULL; 

  /* get the standby command params */ 
  p_standby_cmd_pkt = ctrl->packet; 
  if ( p_standby_cmd_pkt == NULL )
  {
    VS_PANIC_ON_ERROR( VS_EUNEXPECTED );
  }

  p_standby_cmd_params = ((vs_voc_cmd_standby_t *)p_standby_cmd_pkt->params);

  if ( p_standby_cmd_params != NULL )
  {
    /* validate the session handle */ 
    rc = vs_get_typed_object( p_standby_cmd_params->handle, 
                              VS_OBJECT_TYPE_ENUM_SESSION,
                              ( ( vs_object_t** ) &p_session_obj ) ); 
    VS_PANIC_ON_ERROR( rc );  

    if ( rc == VS_EOK )
    {
      /* validate the media_id */
      switch ( p_standby_cmd_params->media_id )
      {
      case VS_COMMON_MEDIA_ID_AMR:
      case VS_COMMON_MEDIA_ID_AMRWB:
      case VS_COMMON_MEDIA_ID_EFR:
      case VS_COMMON_MEDIA_ID_FR:
      case VS_COMMON_MEDIA_ID_HR:
        p_session_obj->media_id = p_standby_cmd_params->media_id; 
        break;

      default:
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_voc_cmd_standby_proc(): " \
                "Unsupported Media_ID=(0x%08x)",
                p_standby_cmd_params->media_id );
        rc = VS_EBADPARAM;
        break;
      }  /* end of switch ( p_standby_cmd_params->media_id ) */
    }
  }
  else
  {
    VS_PANIC_ON_ERROR( VS_EUNEXPECTED );
  }

  if ( rc == VS_EOK )
  {
    /* Check in what state is the control */ 
    if ( ctrl->state == VS_GATING_CMD_STATE_ENUM_EXECUTE )
    {
      p_session_obj->pending_cmd = VS_VOC_CMD_STANDBY;

      rc = vs_create_sequencer_job_object(
             ( vs_sequencer_job_object_t** ) &ctrl->rootjob_obj );
      VS_PANIC_ON_ERROR( rc );

      p_seqjob_obj = &ctrl->rootjob_obj->sequencer_job;
      p_session_obj->session_ctrl.pendjob_seq_ptr = p_seqjob_obj;

      switch ( p_session_obj->state )
      {

      case VS_VOC_STATE_ENUM_RESET: 
        {
          /* have to handle this case - <TBD> */
          MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "ADSP is not booted up yet; " \
                "VS_VOC_CMD_STANDBY is cached");
          rc = VS_ENOTREADY;
        }
        break;

      case VS_VOC_STATE_ENUM_IDLE:
        {
          /* go through the whole STANBY_VOICE sequence beginning with
           * CREATE_MVM if this is the first call,  
           * STOP_VOICE if this is not the first call.
           */
            if ( p_session_obj->is_first_call == TRUE )
            {
              p_session_obj->is_first_call = FALSE;
              ctrl->rootjob_obj->sequencer_job.state = 
                                  VS_STANDBY_SEQUENCER_ENUM_CREATE_MVM;
            }
            else
            {
              ctrl->rootjob_obj->sequencer_job.state = 
                                  VS_STANDBY_SEQUENCER_ENUM_STOP_VOICE;
            }
          }
        break;

      case VS_VOC_STATE_ENUM_RUN:
        {
          ctrl->rootjob_obj->sequencer_job.state = 
                                          VS_STANDBY_SEQUENCER_ENUM_STOP_VOICE;
        }
        break;

      default:
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_voc_cmd_standby (): " \
                "unexpected state" );
          VS_PANIC_ON_ERROR( VS_EUNEXPECTED );
        }
        break;
      }  /* end of switch ( session_obj->state ) */

    }   /* end of if ( ctrl->state == VS_PENDING_CMD_STATE_ENUM_EXECUTE ) */
  }  /* end of if ( rc == VS_EOK ) */

  switch ( rc )
  {
  case VS_EOK:
    {
      rc = vs_voc_standby_sequencer( ctrl ); 
    }
    break;

  case VS_EFAILED:
  case VS_ENOTREADY:
    {
      rc = vs_mem_free_object( ( vs_object_t* ) p_seqjob_obj ); 
      VS_PANIC_ON_ERROR( rc ); 
      p_seqjob_obj = NULL;
      ctrl->rootjob_obj = NULL; 
    }
    /*-fallthru */

  case VS_EBADPARAM:
    {
      if ( p_standby_cmd_params != NULL )
      {
        /* Free the memeory - p_standby_cmd_params. */
        apr_memmgr_free( &vs_heapmgr, p_standby_cmd_params );
        p_standby_cmd_params = NULL;
      }

      /* Inform command status to client. */
      vs_common_send_event( p_session_obj->header.handle, p_standby_cmd_pkt,
                            VS_COMMON_EVENT_CMD_RESPONSE, rc );

      if ( p_standby_cmd_pkt != NULL )
      {
        /* Free the memeory - p_standby_cmd_pkt. */
        apr_memmgr_free( &vs_heapmgr, p_standby_cmd_pkt );
        p_standby_cmd_pkt = NULL;
      }
    }
    break;

  default:
    break;
  }  /* end of switch ( rc ) */

  return rc;

}  /* end of vs_voc_cmd_standby_proc() */

VS_INTERNAL uint32_t vs_voc_cmd_close_proc (
  vs_gating_control_t* ctrl
)
{

  uint32_t rc = VS_EOK;
  vs_cmd_packet_t* p_close_cmd_pkt = NULL;
  vs_voc_cmd_close_t* p_close_cmd_params = NULL;
  vs_session_object_t* p_session_obj = NULL;
  vs_generic_item_t* p_generic_item = NULL;
  vs_sequencer_job_object_t* p_seqjob_obj = NULL;

  enum {
    VS_CLOSE_ENUM_UNINITIALIZED,
    VS_CLOSE_ENUM_DESTROY_WAIT, /*Entry*/
    VS_CLOSE_SEQUENCER_ENUM_COMPLETE, /* Exit. */
    VS_CLOSE_SEQUENCER_ENUM_INVALID
  };
  
  /* get the close command params */ 
  p_close_cmd_pkt = ctrl->packet;

  if( p_close_cmd_pkt == NULL )
  {
    VS_PANIC_ON_ERROR( VS_EUNEXPECTED );
  }

  p_close_cmd_params = ( vs_voc_cmd_close_t* )p_close_cmd_pkt->params;
  if ( p_close_cmd_params != NULL )
  {
    /* validate the session handle */ 
    rc = vs_get_typed_object( p_close_cmd_params->handle, 
                              VS_OBJECT_TYPE_ENUM_SESSION,
                              ( ( vs_object_t** ) &p_session_obj ) );
    VS_PANIC_ON_ERROR( rc );

    if ( rc  == VS_EOK )
    {
      /* Check in what state is the control */ 
      if ( ctrl->state == VS_GATING_CMD_STATE_ENUM_EXECUTE )
      {
        p_session_obj->pending_cmd = VS_VOC_CMD_CLOSE;

        rc = vs_create_sequencer_job_object(
                      ( vs_sequencer_job_object_t** ) &ctrl->rootjob_obj );
        VS_PANIC_ON_ERROR( rc );

        ctrl->rootjob_obj->sequencer_job.state = 
                                    VS_CLOSE_ENUM_DESTROY_WAIT;
        ctrl->rootjob_obj->sequencer_job.helper_state = 
        	                           APR_UNDEFINED_ID_V; 
      }

      p_seqjob_obj = &ctrl->rootjob_obj->sequencer_job;
      p_session_obj->session_ctrl.pendjob_seq_ptr = p_seqjob_obj;

      switch ( p_session_obj->state )
      {
      case VS_VOC_STATE_ENUM_RESET:
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "ADSP is not booted up yet. "
               "delete session object if already created");
        }
        break;
      case VS_VOC_STATE_ENUM_IDLE:
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,  "VS_VOC_CMD_CLOSE in IDLE " \
              "state. destroy CVS and MVM if already created, "
              "delete session object" );
        }
        break;

      case VS_VOC_STATE_ENUM_RUN:
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "VS_VOC_CMD_CLOSE in RUN " \
                "state. ERROR" );
          rc = VS_EABORTED;
        }
        break;

      default:
        break;
      }  /* end of switch ( p_session_obj->state ) */
    }  /* end of if ( rc  == VS_EOK ) */
  }
  else
  {
    VS_PANIC_ON_ERROR( VS_EUNEXPECTED );
  }  /* end of if ( p_close_cmd_params != NULL ) */

  if ( rc == VS_EOK )
  {
    if ( ( vs_voc_is_cvd_ready == FALSE ) || 
         ( vs_voc_is_ssr_cleanup_done == FALSE ) )
    {
      p_seqjob_obj->state = VS_CLOSE_SEQUENCER_ENUM_COMPLETE;
    }

    for ( ;; )
    {
      switch ( p_seqjob_obj->state )
      {
      case VS_CLOSE_ENUM_DESTROY_WAIT:
        {
          rc = vs_voc_helper_destroy_proc( p_seqjob_obj, p_session_obj);
          if( rc )
          { 
            break;
          }
          else
          {
            p_seqjob_obj->state = VS_CLOSE_SEQUENCER_ENUM_COMPLETE;
            continue;
          }
        }

      case VS_CLOSE_SEQUENCER_ENUM_COMPLETE:
        { 
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,   \
                "VS_CLOSE_SEQUENCER_ENUM_COMPLETE  handle=(0x%08x)",
                p_session_obj->header.handle );
          p_generic_item = ( ( vs_generic_item_t* ) &vs_used_session_q.dummy );
          for ( ;; )
          {
            rc = apr_list_get_next( &vs_used_session_q,
                                    ( ( apr_list_node_t* ) p_generic_item ),
                                    ( ( apr_list_node_t** ) &p_generic_item ) );
            if ( rc )
            {
              break;
            }

            if ( p_generic_item->handle == p_session_obj->header.handle )
            {
              apr_list_delete (&vs_used_session_q, (apr_list_node_t*) p_generic_item );
              apr_list_add_tail (&vs_free_session_q, &p_generic_item->link );
              break;
            }
          }  /* end of for ( ;; ) */

          rc = apr_list_destroy( &p_session_obj->alloc_buf_q );
          rc = apr_list_destroy( &p_session_obj->write_buf_q );
          rc = apr_list_destroy( &p_session_obj->read_buf_q );

          vs_common_send_event( p_session_obj->header.handle, p_close_cmd_pkt,
                                VS_COMMON_EVENT_CMD_RESPONSE, rc );

          /* run state machine now to udpate state */
          vs_voc_state_control( p_session_obj );

          rc = VS_EOK;
          {
            rc = vs_mem_free_object( (vs_object_t*) p_session_obj);
            VS_PANIC_ON_ERROR( rc );
            p_session_obj = NULL;

            rc = vs_mem_free_object( ( vs_object_t* ) p_seqjob_obj );
            VS_PANIC_ON_ERROR( rc );
            p_seqjob_obj = NULL;

            /* free the p_disable_cmd_params memory
             * allocated in vs_voc_cmd_disable
             */
            if ( p_close_cmd_params != NULL )
            {
              /* Free the memeory - p_close_cmd_params. */
              apr_memmgr_free( &vs_heapmgr, p_close_cmd_params );
              p_close_cmd_params = NULL;
            }

            /* free the p_close_cmd_pkt memory
             * allocated in vs_voc_cmd_disable
             */
            if ( p_close_cmd_pkt != NULL )
            {
              /* Free the memeory - p_close_cmd_pkt. */
              apr_memmgr_free( &vs_heapmgr, p_close_cmd_pkt );
              p_close_cmd_pkt = NULL;
            }
          }
        } 
        break;

      default:
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_voc_cmd_close_proc (): " \
                "unexpected state" );
          VS_PANIC_ON_ERROR( VS_EUNEXPECTED );
        }
        break;

      }  /* end of switch ( p_seqjob_obj->state ) */
      break;
    }  /* end of for ( ;; ) */
  }  /* end of if ( rc == VS_EOK ) */

  switch ( rc )
  {
  case VS_EABORTED:
    {
      rc = vs_mem_free_object( ( vs_object_t* ) p_seqjob_obj );
      VS_PANIC_ON_ERROR( rc );
      p_seqjob_obj = NULL;
    }
    /*-fallthru */

  case VS_EBADPARAM:
    {
      /* free the p_disable_cmd_params memory
       * allocated in vs_voc_cmd_disable
       */
      if ( p_close_cmd_params != NULL )
      {
        /* Free the memeory - p_close_cmd_params. */
        apr_memmgr_free( &vs_heapmgr, p_close_cmd_params );
        p_close_cmd_params = NULL;
      }

      /* Inform command status to client. */
      vs_common_send_event( p_session_obj->header.handle, p_close_cmd_pkt,
                            VS_COMMON_EVENT_CMD_RESPONSE, rc );

      /* free the p_close_cmd_pkt memory
       * allocated in vs_voc_cmd_disable
       */
      if ( p_close_cmd_pkt != NULL )
      {
        /* Free the memeory - p_close_cmd_pkt. */
        apr_memmgr_free( &vs_heapmgr, p_close_cmd_pkt );
        p_close_cmd_pkt = NULL;
      }
    }
    /*-fallthru */

  case VS_EPENDING:
  default:
    break;
  }  /* end of switch ( rc ) */

  return rc;

}  /* end of vs_voc_cmd_close_proc() */

VS_INTERNAL uint32_t vs_voc_cmd_flush_buffers_proc (
  vs_gating_control_t* ctrl
)
{

  uint32_t rc = VS_EOK;
  vs_cmd_packet_t* p_flush_cmd_pkt;
  vs_voc_cmd_flush_buffers_t* p_flush_cmd_params;
  vs_voc_buffer_item_t* p_voc_buf_node;
  vs_session_object_t* p_session_obj;
  vs_voc_buffer_t* p_voc_buffer;

  /* Get the cmd packet. */
    p_flush_cmd_pkt = ctrl->packet;
    if ( p_flush_cmd_pkt == NULL )
    {
      VS_PANIC_ON_ERROR( VS_EUNEXPECTED );
    }
    p_flush_cmd_params = ( vs_voc_cmd_flush_buffers_t* )p_flush_cmd_pkt->params;

  if ( p_flush_cmd_params == NULL )
  {
    VS_PANIC_ON_ERROR( VS_EUNEXPECTED );
  }

  for ( ;; )
  {
    /* Get the session object with respect to the handle */ 
    rc = vs_get_typed_object ( p_flush_cmd_params->handle, 
                               VS_OBJECT_TYPE_ENUM_SESSION, 
                               ( ( vs_object_t** ) &p_session_obj ) ); 
    VS_PANIC_ON_ERROR( rc );

    /* Make all node in alloc_buf_q as VS_VOC_BUFFER_TYPE_NONE.
     * None of the buffer available contians valid vocder frame 
     * data.
     */
    p_voc_buf_node = 
       ( vs_voc_buffer_item_t* )( &p_session_obj->alloc_buf_q.dummy );

    for( ;; )
    {
      rc = apr_list_get_next( &p_session_obj->alloc_buf_q,
                              ( ( apr_list_node_t* ) p_voc_buf_node ),
                              ( ( apr_list_node_t** ) &p_voc_buf_node ) );
      if ( rc )
      {
        rc = VS_EOK;
        break;
      }
      p_voc_buf_node->buf_type = VS_VOC_BUFFER_TYPE_NONE;
    }  /* end of for( ;; ) */
       
    /* Only buffer marked as VS_VOC_BUFFER_TYPE_READ contains valid ADSP
     * encoder frame data. These are available only in read_buf_q. 
     *
     * Send only one read available event irrespective of number of
     * buffers available in read_buf_q.
     *  
     * Client should call VS_VOC_CMD_READ_BUFFER continuosuly until 
     * it gets VS_NOTEXIST from VS. 
     */
    if ( p_session_obj->read_buf_q.size > 0 )
    {
      p_session_obj->event_cb( VS_VOC_EVENT_READ_AVAILABLE, NULL, 0,
                               p_session_obj->session_context );
    }

    /* Remove elems from Write buffer queue if any */
    if( p_session_obj->write_buf_q.size > 0 )
    {
      for( ;; )
      {
        rc = apr_list_remove_head( &p_session_obj->write_buf_q,
                                   ( ( apr_list_node_t** ) &p_voc_buf_node ) );
        if ( rc )
        {
          rc = VS_EOK;
          break;
        }
        else
        {
          if( p_voc_buf_node != NULL )
          {
            p_voc_buffer = ( ( vs_voc_buffer_t* ) p_voc_buf_node->buf );

            /* Send write buffer return event(s)
             * Number of EVENT_WRITE_BUFFER_RETURNED sent will be equal to 
             * number of write buffers available in the queue. 
             */
            MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
                   "vs_voc_cmd_flush_buffers (): VS_VOC_EVENT_WRITE_BUFFER_RETURNED "
                   "sent, buf=(0x%08x)", p_voc_buffer );
            p_session_obj->event_cb( VS_VOC_EVENT_WRITE_BUFFER_RETURNED, p_voc_buffer,
                                     sizeof( vs_voc_event_write_buffer_returned_t ), 
                                     p_session_obj->session_context);
              
            p_voc_buf_node->buf_type = VS_VOC_BUFFER_TYPE_NONE;
            /* Move this node back to alloc_buf_q. */
            ( void ) apr_list_add_tail( &p_session_obj->alloc_buf_q,
                                        &p_voc_buf_node->link );
            p_voc_buf_node = NULL;
          }
          else
          {
            VS_PANIC_ON_ERROR( VS_EUNEXPECTED );
          }
        }  /* end of if ( rc ) */
      }  /* end of for( ;; ) */
    }
    else
    {
      /* Send one dummy write buffer return event to client if the write_buf_q
       * is empty.
       */
      p_session_obj->event_cb( VS_VOC_EVENT_WRITE_BUFFER_RETURNED, NULL,
                               0, p_session_obj->session_context );
    }  /* end of if( session_obj->write_buf_q.size > 0 ) */

    break;
  }  /* end of for ( ;; ) */

  /* Now send the command response */
  vs_common_send_event( p_flush_cmd_params->handle, p_flush_cmd_pkt, 
                        VS_COMMON_EVENT_CMD_RESPONSE, rc );
  switch ( rc )
  {
  case VS_EOK:
    {
      /* free the p_flush_cmd_params
       * allocated in vs_voc_cmd_flush_buffers
       */
      if ( p_flush_cmd_params != NULL )
      {
        /* Free the memeory - p_flush_cmd_params. */
        apr_memmgr_free( &vs_heapmgr, p_flush_cmd_params );
        p_flush_cmd_params = NULL;
      }

      /* free the p_flush_cmd_pkt memory
       * allocated in vs_voc_cmd_flush_buffers
       */
      if ( p_flush_cmd_pkt != NULL )
      {
        /* Free the memeory - p_flush_cmd_pkt. */
        apr_memmgr_free( &vs_heapmgr, p_flush_cmd_pkt );
        p_flush_cmd_pkt = NULL;
      }
    }
    break;

  default:
    break;

  }  /* end of switch ( rc ) */

  return rc;

}  /* end of vs_voc_cmd_flush_buffers_proc() */

VS_INTERNAL uint32_t vs_voc_cmd_cleanup_for_adsp_ssr_proc ( 
  vs_gating_control_t* ctrl
)
{

  uint32_t rc;
  vs_cmd_packet_t* ssr_cleanup_cmd_pkt;
  vs_session_object_t* session_obj;
  vs_generic_item_t* generic_item;

  ssr_cleanup_cmd_pkt = ctrl->packet;

  { /* Clean up the stale ADSP handles and all tracked objects. */
    generic_item = ( ( vs_generic_item_t* ) &vs_used_session_q.dummy );

    for ( ;; )
    {
      rc = apr_list_get_next( &vs_used_session_q,
                              ( ( apr_list_node_t* ) generic_item ),
                              ( ( apr_list_node_t** ) &generic_item ) );
      if ( rc ) break;

      rc = vs_get_typed_object( generic_item->handle,
                                VS_OBJECT_TYPE_ENUM_SESSION,
                                ( ( vs_object_t** ) &session_obj ) );
      VS_PANIC_ON_ERROR( rc );

      session_obj->cvs_port = APR_NULL_V;
      session_obj->mvm_port = APR_NULL_V;
      session_obj->is_first_call = TRUE;
      session_obj->stream_standby = FALSE;
      if ( session_obj->stream_ready == TRUE )
      {
        ( void ) vs_common_send_event( session_obj->header.handle, NULL,
                                       VS_COMMON_EVENT_NOT_READY, VS_EOK );
        session_obj->stream_ready = FALSE;
        ( void ) vs_voc_state_control( session_obj );
      }
    }

    rc = vs_voc_ssr_free_all_tracked_objects( );
  }

  vs_voc_is_cvd_ready = FALSE;

  if ( vs_voc_ssr_cvd_poll_job_obj != NULL )
  {
    /* VS is still in the process of polling CVD. This can only happen if
     * ADSP SSR happened while we are polling CVD. 
     */
    vs_voc_ssr_cvd_poll_job_obj = NULL;
    vs_voc_is_abort_cvd_poll = TRUE;
  }

  if ( ssr_cleanup_cmd_pkt != NULL )
  {
    apr_memmgr_free( &vs_heapmgr, ssr_cleanup_cmd_pkt );
  }

  vs_voc_is_ssr_cleanup_done = TRUE;

  return APR_EOK;
}

VS_INTERNAL uint32_t vs_voc_cmd_poll_cvd_proc ( 
  vs_gating_control_t* ctrl
)
{

  uint32_t rc;
  vs_cmd_packet_t* cvd_poll_cmd_pkt;
  vs_simple_job_object_t* job_obj;

  cvd_poll_cmd_pkt = ctrl->packet;

  for ( ;; )
  {
    if ( vs_voc_is_abort_cvd_poll == TRUE )
    {
      vs_voc_is_abort_cvd_poll = FALSE;
      break;
    }

    if ( vs_voc_ssr_cvd_poll_job_obj != NULL )
    {
      rc = vs_mem_free_object( ( vs_object_t* ) vs_voc_ssr_cvd_poll_job_obj );
      VS_PANIC_ON_ERROR( rc );

      vs_voc_ssr_cvd_poll_job_obj = NULL;
    }

    if ( vs_voc_is_cvd_ready == TRUE )
    {
      break;
    }

    rc = vs_create_simple_job_object( APR_NULL_V, &job_obj );
    VS_PANIC_ON_ERROR( rc );
    job_obj->fn_table[ VS_RESPONSE_FN_ENUM_RESULT ] = vs_voc_ssr_cvd_poll_result_rsp_fn;

    vs_voc_ssr_cvd_poll_job_obj = job_obj;

    /* This is a temporary workaround to detect that CVD (MVM) is ready after
     * ADSP SSR. VS will send APR_CMDID_GET_VERSION to MVM, if MVM is ready, MVM 
     * will return APR_EUNSUPPORTED since MVM doesn't support this command. Once
     * VS receives the response from MVM for APR_EUNSUPPORTED, it means MVM is 
     * is ready. MVM is the last service in CVD that get initialized. Therefore
     * after MVM is ready, it means that CVS and CVP are also ready.
     * In the future, we need to query APR for individual service's readiness
     * once APR has such support.
     */
     ( void ) __aprv2_cmd_alloc_send(
                vs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                vs_my_addr, APR_NULL_V,
                vs_mvm_addr, APR_NULL_V,
                job_obj->header.handle,
                APR_CMDID_GET_VERSION, NULL, 0 );

     /* Start cvd poll timer, which will trigger the VS thread to RUN again. */
     ( void ) apr_timer_start( 
                vs_voc_ssr_cvd_poll_timer, VS_VOC_SSR_CVD_POLL_TIMER_NS );
  
    break;
  }

  if ( cvd_poll_cmd_pkt != NULL )
  {
    apr_memmgr_free( &vs_heapmgr, cvd_poll_cmd_pkt );
  }

  return VS_EOK;

}
/****************************************************************************
 * END OF GATING COMMAND PROCESSING FUNCTIONS                               *
 ****************************************************************************/

/****************************************************************************
 * VS ADSP SSR RELATED HANDLING FUNCTIONS                                   *
 ****************************************************************************/
static void vs_voc_ssr_adsp_before_shutdown_handler ( 
  void
)
{

  uint32_t rc;
  vs_cmd_packet_t* ssr_cleanup_cmd_pkt = NULL;

  MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,   \
       "vs_voc_ssr_adsp_before_shutdown_handler is called." );

  vs_voc_is_ssr_cleanup_done = FALSE;

  ssr_cleanup_cmd_pkt = ( ( vs_cmd_packet_t* ) 
                          apr_memmgr_malloc( 
                            &vs_heapmgr, sizeof( vs_cmd_packet_t ) ) );
  if ( ssr_cleanup_cmd_pkt == NULL )
  {
    VS_PANIC_ON_ERROR( VS_ENORESOURCE );
  }

  ssr_cleanup_cmd_pkt->cmd_id = VS_VOC_CMD_CLEANUP_FOR_ADSP_SSR;
  ssr_cleanup_cmd_pkt->params = NULL;

  /* Queue the command packet for processing. */
  rc = vs_queue_incoming_vs_command_packet( ssr_cleanup_cmd_pkt );
  VS_PANIC_ON_ERROR( rc );

}

static void vs_voc_ssr_trigger_cvd_polling (
  void
)
{

  uint32_t rc;
  vs_cmd_packet_t* cvd_poll_cmd_pkt = NULL;

  cvd_poll_cmd_pkt = ( ( vs_cmd_packet_t* )
                       apr_memmgr_malloc( 
                         &vs_heapmgr, sizeof( vs_cmd_packet_t ) ) );
  if ( cvd_poll_cmd_pkt == NULL )
  {
    VS_PANIC_ON_ERROR( VS_ENORESOURCE );
  }

  cvd_poll_cmd_pkt->cmd_id = VS_VOC_CMD_POLL_CVD;
  cvd_poll_cmd_pkt->params = NULL;

  /* Queue the command packet for processing. */
  rc = vs_queue_incoming_vs_command_packet( cvd_poll_cmd_pkt );
  VS_PANIC_ON_ERROR( rc );

}

static void vs_voc_ssr_adsp_after_powerup_handler (
  void
)
{

  MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,   \
       "vs_voc_ssr_adsp_after_powerup_handler is called." );

  vs_voc_ssr_trigger_cvd_polling( );

}

static void vs_voc_ssr_cvd_poll_timer_cb ( 
  void* client_token 
)
{

  MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "vs_voc_ssr_cvd_poll_timer_cb is " \
       "called." );
  vs_voc_ssr_trigger_cvd_polling( );

}

/* Track the allocated VS objects with type VS_OBJECT_TYPE_ENUM_SIMPLE_JOB and
 * VS_OBJECT_TYPE_ENUM_HANDLE, in order to free them in case of ADSP SSR. VS
 * allocates job object with one of the above types when communicating with the
 * ADSP and frees the object once a response is received from the ADSP. If VS 
 * has issued commands to the ADSP and ADSP SSR occurs, VS will not get 
 * responses from the ADSP and thus the objects will not be freed. Therefore
 * VS needs to track these types of allocated objects and free them upon ADSP
 * SSR.
 */
VS_INTERNAL uint32_t vs_voc_ssr_track_object (
  uint32_t obj_handle
)
{

  int32_t rc;
  vs_generic_item_t* generic_item;

  rc = apr_list_remove_head( &vs_voc_ssr_obj_tracking_free_q,
                             ( ( apr_list_node_t** ) &generic_item ) );
  VS_PANIC_ON_ERROR( rc );

  generic_item->handle = obj_handle;

  ( void ) apr_list_add_tail( &vs_voc_ssr_obj_tracking_used_q,
                              &generic_item->link );

  return VS_EOK;

}

/* Untrack the VS objects with type VS_OBJECT_TYPE_ENUM_SIMPLE_JOB and
 * VS_OBJECT_TYPE_ENUM_HANDLE.
 */
VS_INTERNAL uint32_t vs_voc_ssr_untrack_object (
  uint32_t obj_handle
)
{

  int32_t rc;
  vs_generic_item_t* generic_item;

  generic_item = 
    ( ( vs_generic_item_t* ) 
      &vs_voc_ssr_obj_tracking_used_q.dummy );

  for ( ;; )
  {
    rc = apr_list_get_next( 
           &vs_voc_ssr_obj_tracking_used_q,
           ( ( apr_list_node_t* ) generic_item ),
           ( ( apr_list_node_t** ) &generic_item ) );
    VS_PANIC_ON_ERROR( rc );

    if ( generic_item->handle == obj_handle )
    {
      ( void ) apr_list_delete( 
                 &vs_voc_ssr_obj_tracking_used_q,
                 &generic_item->link );
      ( void ) apr_list_add_tail( 
                 &vs_voc_ssr_obj_tracking_free_q,
                 &generic_item->link );
      break;
    }
  }

  return VS_EOK;

}

/* Free all of the tracked VS objects. */
VS_INTERNAL uint32_t vs_voc_ssr_free_all_tracked_objects ( 
  void 
)
{

  int32_t apr_rc;
  uint32_t vs_rc;
  vs_generic_item_t* generic_item;
  vs_object_t* obj;

  for ( ;; )
  {
    apr_rc = apr_list_peak_head( &vs_voc_ssr_obj_tracking_used_q,
                                 ( ( apr_list_node_t** ) &generic_item ) );
    if ( apr_rc ) break;

    vs_rc = vs_get_object( generic_item->handle, &obj );
    VS_PANIC_ON_ERROR( vs_rc );

    /* Note that vs_mem_free_object will untrack the object. */
    vs_rc = vs_mem_free_object ( obj );
    VS_PANIC_ON_ERROR( vs_rc );
  }

  return VS_EOK;

}

VS_INTERNAL uint32_t vs_voc_ssr_init (
  void 
)
{

  int32_t rc;
  uint32_t index;
#ifndef WINSIM
  RCECB_HANDLE ssr_handle;
#endif /* !WINSIM */

  vs_voc_ssr_cvd_poll_job_obj = NULL;
  vs_voc_is_cvd_ready = TRUE;
  vs_voc_is_ssr_cleanup_done = TRUE;
  vs_voc_is_abort_cvd_poll = FALSE;

  { /* Create timer used for polling CVD (MVM)'s readiness after ADSP SSR. */
    rc = apr_timer_create( 
           &vs_voc_ssr_cvd_poll_timer, vs_voc_ssr_cvd_poll_timer_cb, NULL );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR ,  "vs_voc_ssr_init():Failed to " \
               "create vs_voc_ssr_cvd_poll_timer"
               " with rc=(0x%08x)", rc );
    }
  }

  { /* Initialize the object tracking list. */
    rc = apr_list_init_v2( &vs_voc_ssr_obj_tracking_free_q, NULL, NULL );
    for ( index = 0; index < VS_MAX_OBJECTS_V; ++index )
    {
      ( void )apr_list_init_node( 
                ( apr_list_node_t* ) &vs_voc_ssr_obj_tracking_pool[ index ] );
      rc = apr_list_add_tail(
             &vs_voc_ssr_obj_tracking_free_q,
             ( ( apr_list_node_t* ) &vs_voc_ssr_obj_tracking_pool[ index ] ) );
    }

    rc = apr_list_init_v2( &vs_voc_ssr_obj_tracking_used_q, NULL, NULL );
  }

#ifndef WINSIM
  {  /* Register for system monitor SSR callbacks. */
    ssr_handle = rcecb_register_context_name(
                   SYS_M_SSR_ADSP_BEFORE_SHUTDOWN,
                   vs_voc_ssr_adsp_before_shutdown_handler );
    if ( ssr_handle == NULL )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR ,  "vs_voc_ssr_init(): Failed to " \
             "register for SYS_M_SSR_ADSP_BEFORE_SHUTDOWN callback." );
    }
  
    ssr_handle = rcecb_register_context_name( 
                   SYS_M_SSR_ADSP_AFTER_POWERUP, 
                   vs_voc_ssr_adsp_after_powerup_handler );
    if ( ssr_handle == NULL )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR ,  "vs_voc_ssr_init(): Failed to " \
             "register for SYS_M_SSR_ADSP_AFTER_POWERUP callback." );
    }
  }
#endif /* !WINSIM */

  return VS_EOK;

}

VS_INTERNAL uint32_t vs_voc_ssr_deinit ( 
  void 
)
{

#ifndef WINSIM
  { /* Unregister for system monitor SSR callbacks. */
    ( void ) rcecb_unregister_context_name(
               SYS_M_SSR_ADSP_AFTER_POWERUP,
               vs_voc_ssr_adsp_after_powerup_handler );

    ( void ) rcecb_unregister_context_name( 
               SYS_M_SSR_ADSP_BEFORE_SHUTDOWN, 
               vs_voc_ssr_adsp_before_shutdown_handler );
  }
#endif /* !WINSIM */

  ( void ) apr_list_destroy( &vs_voc_ssr_obj_tracking_used_q );
  ( void ) apr_list_destroy( &vs_voc_ssr_obj_tracking_free_q );

  ( void ) apr_timer_destroy( vs_voc_ssr_cvd_poll_timer );

  return VS_EOK;

}

/* Apply cached properties to make sure latest properties are appiled */
VS_INTERNAL uint32_t  vs_voc_action_set_cached_stream_properties (
  vs_session_object_t* p_session_obj
)
{

  uint32_t rc = VS_EOK;

  if ( p_session_obj == NULL )
  {
    VS_PANIC_ON_ERROR( VS_EUNEXPECTED );
  }

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,   \
         "vs_voc_action_set_cached_stream_properties:  handle = (0x%08x)",
         p_session_obj->header.handle );

  /* Send data commands to stream. */
  switch( p_session_obj->media_id )
  {
        case VS_COMMON_MEDIA_ID_FR: 
          rc = 
            vs_vocfr_set_cached_voc_property( p_session_obj->header.handle );
          break;

        case VS_COMMON_MEDIA_ID_HR: 
          rc = 
            vs_vochr_set_cached_voc_property( p_session_obj->header.handle );
          break;

        case VS_COMMON_MEDIA_ID_EFR:   
          rc = 
            vs_vocefr_set_cached_voc_property( p_session_obj->header.handle );
          break;

        case VS_COMMON_MEDIA_ID_AMR: 
          rc =
            vs_vocamr_set_cached_voc_property( p_session_obj->header.handle );
          break;

        case VS_COMMON_MEDIA_ID_AMRWB: 
          rc = 
            vs_vocamrwb_set_cached_voc_property( p_session_obj->header.handle );
          break;
         
        default:
          break;
   }  /* end of switch(session_obj->media_id) */

   return rc;
}