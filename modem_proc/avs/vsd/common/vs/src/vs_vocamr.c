/**
  @file vs_vocamr.c
  @brief This file contains the definitions of the interfaces for AMR vocoder.
   
*/

/*
  ============================================================================

   Copyright (C) 2013 QUALCOMM Technologies Incorporated. All Rights Reserved.
   QUALCOMM Proprietary and Confidential

  ============================================================================

                             Edit History

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/avs/vsd/common/vs/src/vs_vocamr.c#1 $
  $Author: mplp4svc $

  when      who   what, where, why
  --------  ---   ------------------------------------------------------------
  07/30/13  sud   Updated software to report MSG_FATAL when fatal error 
                  conditions occurs in client context.
  06/26/13  sud   Updated debug messages and rearranged some of the functions
                  from vs_voc.c
  06/16/13  sud   Added debug messages.
  06/13/13  hl    Added support for handling ADSP SSR.
  06/12/13  sud   Updated VS to handle errors and status propagation to client.
                  Updated VS to handle memory leak and memory corruption
                  issues. 
                  Updated VS to handle command priorities and proper sequencing.
                  Updated VS to add proper debug messages
  04/16/13  sud   VS refactored code and fixed bugs. 
  01/13/13  sg    Added the funtions for AMR vocoder
  01/02/13  sud   Initial revision

  ============================================================================
*/

/****************************************************************************
 * INCLUDE HEADER FILES                                                     *
 ****************************************************************************/
#include "stdlib.h"
#include "msg.h"
#include "vs.h"
#include "vs_i.h"  /*internal data structures*/
#include "vs_vocamr.h"
#include "vss_public_if.h"
#include "amrsup.h"

/****************************************************************************
 * EXTERN DECLARATIONS                                                      *
 ****************************************************************************/
extern uint16_t vs_my_addr;
extern uint16_t vs_cvs_addr;
extern uint32_t vs_apr_handle;
extern apr_memmgr_type vs_heapmgr;

/****************************************************************************
 * FUNCTIONS                                                                *
 ****************************************************************************/
/**
 * Sets the codec mode of the AMR vocoder.
 *
 * VS_EOK (0) when successful. 
 */
VS_INTERNAL uint32_t vs_vocamr_cmd_set_codec_mode (
  vs_vocamr_cmd_set_codec_mode_t* params
)
{

  uint32_t rc = VS_EOK;
  vs_cmd_packet_t* p_amrmode_cmd_pkt = NULL;
  vs_vocamr_cmd_set_codec_mode_t* p_args = NULL;
  vs_session_object_t* p_session_obj = NULL;
  uint32_t checkpoint = 0;

  if ( params == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_vocamr_cmd_set_codec_mode() - " \
          "NULL params" );
    return VS_EBADPARAM;
  }

  /* get the session object based on handle*/
  rc = vs_get_typed_object( params->handle, VS_OBJECT_TYPE_ENUM_SESSION,
                            ( ( vs_object_t** ) &p_session_obj ) );
  if ( rc != VS_EOK )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_vocamr_cmd_set_codec_mode() " \
            "- Invalid handle=0x%08x",
            params->handle );
    rc = VS_EBADPARAM;
    return rc;
  }

  for ( ;; )
  {
    p_args = ( vs_vocamr_cmd_set_codec_mode_t* ) apr_memmgr_malloc( 
                                    &vs_heapmgr,
                                    sizeof( vs_vocamr_cmd_set_codec_mode_t ) );
    if( p_args == NULL )
    { 
      rc = VS_ENORESOURCE;
      VS_REPORT_FATAL_ON_ERROR( rc );
      break;
    }
    p_args->handle = params->handle;
    p_args->codec_mode = params->codec_mode;
    checkpoint = 1;

    /* Prepare command packet for VS_VOCAMR_CMD_SET_CODEC_MODE. */
    p_amrmode_cmd_pkt = ( vs_cmd_packet_t* ) apr_memmgr_malloc( 
                                      &vs_heapmgr,
                                      sizeof( vs_cmd_packet_t ) );
    if( p_amrmode_cmd_pkt == NULL )
    { 
      rc = VS_ENORESOURCE;
      VS_REPORT_FATAL_ON_ERROR( rc );
      break;
    }
    p_amrmode_cmd_pkt->cmd_id = VS_VOCAMR_CMD_SET_CODEC_MODE;
    p_amrmode_cmd_pkt->params = p_args;
    checkpoint = 2;

    /* queue the command packet for processing */
    rc = vs_queue_incoming_vs_command_packet( p_amrmode_cmd_pkt );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
              "vs_vocamr_cmd_set_codec_mode() - cmd pkt queuing failed."
              "rc=0x%08x", rc );
    }
    else
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "vs_vocamr_cmd_set_codec_mode() " \
            "cmd pkt queued: "
            "handle=0x%08x, codec_mode=0x%08x",
            params->handle, params->codec_mode );
      checkpoint = 3;
    }
    break;
  }  /* end of for ( ;; ) */
  
  switch ( checkpoint )
  {
  case 3:
    break;

  case 2:
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
              "vs_vocamr_cmd_set_codec_mode() - failed, checkpoint=%d",
              checkpoint );
      if ( p_amrmode_cmd_pkt != NULL )
      {
        apr_memmgr_free( &vs_heapmgr, p_amrmode_cmd_pkt );
        p_amrmode_cmd_pkt = NULL;
      }
    }
    /*-fallthru */

  case 1:
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
              "vs_vocamr_cmd_set_codec_mode(): failed, checkpoint=%d",
              checkpoint );
      if ( p_args != NULL )
      {
        apr_memmgr_free( &vs_heapmgr, p_args );
        p_args = NULL;
      }
    }
    /*-fallthru */

  case 0:
  default:
    break;

  }  /* end of switch ( checkpoint ) */

  return rc;

}  /* end of vs_vocamr_cmd_set_codec_mode () */

VS_INTERNAL uint32_t vs_vocamr_cmd_set_codec_mode_proc (
  uint32_t handle,
  vs_vocamr_codec_mode_t codec_mode
)
{

  uint32_t rc = VS_EOK;
  vss_istream_cmd_voc_amr_set_enc_rate_t amr_mode;
  vs_session_object_t* p_session_obj;
 
  for ( ;; )
  {
    /* get the session object based on handle*/
    rc = vs_get_typed_object( handle, VS_OBJECT_TYPE_ENUM_SESSION,
                              ( ( vs_object_t** ) &p_session_obj ) );
    VS_PANIC_ON_ERROR( rc );

    if ( codec_mode > VS_VOCAMR_CODEC_MODE_1220 )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
              "vs_vocamr_cmd_set_codec_mode(): "
              "Invalid AMR codec_mode %d input, setting mode to 04.75",
              codec_mode );

      codec_mode = VS_VOCAMR_CODEC_MODE_0475;
    }

    /* send to CVD only if the codec mode requested is different from the
     * current codec mode
     */
    if ( p_session_obj->current_voc.amr.codec_mode == ( uint32_t )codec_mode )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,   \
            "vs_vocamr_cmd_set_codec_mode_proc(): "
            "no change in codec_mode %d", codec_mode );
      break;
    }

    /* Cache and send to CVD immediately when in RUN state and media_id is
     * VS_COMMON_MEDIA_ID_AMR.
     * If both conditions are NOT true, cache the value and apply while                    .
     * enabling the stream. 
     */  
    if ( ( p_session_obj->state == VS_VOC_STATE_ENUM_RUN ) &&
         ( p_session_obj->media_id == VS_COMMON_MEDIA_ID_AMR ) )
    {

      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,   \
            "vs_vocamr_cmd_set_codec_mode_proc()-  codec_mode %d set, "
            "previous mode %d",
            codec_mode, p_session_obj->current_voc.amr.codec_mode );

      /* update pending_voc and current_voc */
      p_session_obj->pending_voc.amr.codec_mode = VS_VOCAMR_CODEC_MODE_NONE_V;
      p_session_obj->current_voc.amr.codec_mode = ( uint32_t )codec_mode;

      /* apply the codec_mode */
      amr_mode.mode = p_session_obj->current_voc.amr.codec_mode;

      rc = __aprv2_cmd_alloc_send(
             vs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             vs_my_addr, ( (uint16_t) p_session_obj->header.handle ),
             vs_cvs_addr, p_session_obj->cvs_port,
             p_session_obj->header.handle, VSS_ISTREAM_CMD_VOC_AMR_SET_ENC_RATE,
             &amr_mode, sizeof( amr_mode ) );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                "vs_vocamr_cmd_set_codec_mode_proc(): "
                "Failed to communicate with CVS with rc=0x%08x", rc );
      }

      break;
    }
    else
    {
      /* Cache the codec_mode now. 
       * Apply the codec_mode while enabling the stream 
       */
      p_session_obj->pending_voc.amr.codec_mode = ( uint32_t )codec_mode;
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,   \
            "vs_vocamr_cmd_set_codec_mode_proc() - codec_mode saved=%d"
            "current codec_mode=%d",
            codec_mode, p_session_obj->current_voc.amr.codec_mode );
      break;
    }  /* end of if ( session_obj->state == VS_VOC_STATE_ENUM_RUN ) */

  }  /* end of for ( ;; ) */

  return rc;

}  /* end of vs_vocamr_cmd_set_codec_mode_proc () */

/**
 * Sets the DTX mode of the AMR vocoder.
 *
 * VS_EOK (0) when successful. 
 */
VS_INTERNAL uint32_t vs_vocamr_cmd_set_dtx_mode (
  vs_vocamr_cmd_set_dtx_mode_t* params
)
{

  uint32_t rc = VS_EOK;
  vs_cmd_packet_t* p_amrdtx_cmd_pkt = NULL;
  vs_vocamr_cmd_set_dtx_mode_t* p_args = NULL;
  vs_session_object_t* p_session_obj = NULL;
  uint32_t checkpoint = 0;

  if ( params == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_vocamr_cmd_set_dtx_mode() - " \
          "NULL params" );
    return VS_EBADPARAM;
  }
    
  /* get the session object based on handle*/
  rc = vs_get_typed_object( params->handle, VS_OBJECT_TYPE_ENUM_SESSION,
                            ( ( vs_object_t** ) &p_session_obj ) );
  if ( rc != VS_EOK )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_vocamr_cmd_set_dtx_mode() - " \
            "Invalid handle=0x%08x",
            params->handle );
    rc = VS_EBADPARAM;
    return rc;
  }
      
  for ( ;; )
  {
    p_args = ( vs_vocamr_cmd_set_dtx_mode_t* ) apr_memmgr_malloc( 
                                          &vs_heapmgr,
                                          sizeof( vs_vocamr_cmd_set_dtx_mode_t ) );
    if( p_args == NULL )
    { 
      rc = VS_ENORESOURCE;
      VS_REPORT_FATAL_ON_ERROR( rc );
      break;
    }
    p_args->handle = params->handle;
    p_args->enable_flag = params->enable_flag;
    checkpoint = 1;

    /* Prepare command packet for VS_VOCAMR_CMD_SET_DTX_MODE. */
    p_amrdtx_cmd_pkt = ( vs_cmd_packet_t* ) apr_memmgr_malloc( 
                                      &vs_heapmgr,
                                      sizeof( vs_cmd_packet_t ) );
    if( p_amrdtx_cmd_pkt == NULL )
    { 
      rc = VS_ENORESOURCE;
      VS_REPORT_FATAL_ON_ERROR( rc );
      break;
    }
    p_amrdtx_cmd_pkt->cmd_id = VS_VOCAMR_CMD_SET_DTX_MODE;
    p_amrdtx_cmd_pkt->params = p_args;
    checkpoint = 2;

    /* queue the command packet for processing */
    rc = vs_queue_incoming_vs_command_packet( p_amrdtx_cmd_pkt );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_vocamr_cmd_set_dtx_mode() " \
              "- cmd pkt queuing failed."
              "rc=0x%08x", rc );
    }
    else
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "vs_vocamr_cmd_set_dtx_mode() " \
            "cmd pkt queued: handle=0x%08x, dtx_enable=%d",
            params->handle, params->enable_flag );
      checkpoint = 3;
    }
    break;
  }  /* end of for ( ;; ) */

  switch ( checkpoint )
  {
  case 3:
    break;

  case 2:
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_vocamr_cmd_set_dtx_mode() " \
              "- failed, checkpoint=%d",
              checkpoint );
      if ( p_amrdtx_cmd_pkt != NULL )
      {
        apr_memmgr_free( &vs_heapmgr, p_amrdtx_cmd_pkt );
        p_amrdtx_cmd_pkt = NULL;
      }
    }
    /*-fallthru */

  case 1:
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_vocamr_cmd_set_dtx_mode(): " \
              "failed, checkpoint=%d",
              checkpoint );
      if ( p_args != NULL )
      {
        apr_memmgr_free( &vs_heapmgr, p_args );
        p_args = NULL;
      }
    }
    /*-fallthru */

  case 0:
  default:
    break;
  }  /* end of switch ( checkpoint ) */

  return rc;

}  /* end of vs_vocamr_cmd_set_dtx_mode () */

VS_INTERNAL uint32_t vs_vocamr_cmd_set_dtx_mode_proc (
  uint32_t handle,
  bool_t dtx_enable
)
{

  uint32_t rc = VS_EOK;
  vss_istream_cmd_set_enc_dtx_mode_t amr_dtx;
  vs_session_object_t* p_session_obj;

  for ( ;; )
  {
    /*get the session object based on handle*/
    rc = vs_get_typed_object( handle, VS_OBJECT_TYPE_ENUM_SESSION,
                              ( ( vs_object_t** ) &p_session_obj ) );
    VS_PANIC_ON_ERROR( rc );

    /* send to CVD only if the dtx mode requested is different from the
     * current dtx mode
     */
    if ( p_session_obj->current_voc.amr.dtx_enable == ( uint32_t ) dtx_enable ) 
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,   \
            "vs_vocamr_cmd_set_dtx_mode_proc(): no change in dtx_mode %d",
            dtx_enable );
      break;
    }

    /* Cache and send to CVD immediately when in RUN state and media_id is
     * VS_COMMON_MEDIA_ID_AMR.
     * If both conditions are NOT true, cache the value and apply while                    .
     * enabling the stream. 
     */  
    if ( ( p_session_obj->state == VS_VOC_STATE_ENUM_RUN ) &&
         ( p_session_obj->media_id == VS_COMMON_MEDIA_ID_AMR ) )
    {

      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,   \
            "vs_vocamr_cmd_set_dtx_mode_proc()-  dtx_mode %d set, "
            "previous mode %d",
            dtx_enable, p_session_obj->current_voc.amr.dtx_enable );

      /* update pending_voc and current_voc */
      p_session_obj->pending_voc.amr.dtx_enable = VS_VOC_DTX_MODE_UNDEFINED_V;
      p_session_obj->current_voc.amr.dtx_enable = ( uint32_t )dtx_enable;

      amr_dtx.enable = p_session_obj->current_voc.amr.dtx_enable;

      /* apply the dtx_mode */
      rc = __aprv2_cmd_alloc_send(
             vs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             vs_my_addr, ( (uint16_t) p_session_obj->header.handle ),
             vs_cvs_addr, p_session_obj->cvs_port,
             p_session_obj->header.handle, VSS_ISTREAM_CMD_SET_ENC_DTX_MODE,
             &amr_dtx, sizeof( amr_dtx ) );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                "vs_vocamr_cmd_set_dtx_mode_proc(): Failed to communicate "
                "with CVS with rc=0x%08x", rc );
      }

      break;
    }
    else
    {
      /* Cache the dtx_mode now. 
       * Apply the dtx_mode while enabling the stream 
       */
      p_session_obj->pending_voc.amr.dtx_enable = ( uint32_t )dtx_enable;
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,   \
            "vs_vocamr_cmd_set_dtx_mode_proc(): dtx_mode saved=%d,"
            "current dtx_mode=%d",
            dtx_enable, p_session_obj->current_voc.amr.dtx_enable );
      break;
    }  /* end of if ( session_obj->state == VS_VOC_STATE_ENUM_RUN ) */

  }  /* end of for ( ;; ) */

  return rc;

}  /* end of vs_vocamr_cmd_set_dtx_mode_proc () */

VS_INTERNAL uint32_t vs_vocamr_set_cached_voc_property (
  uint32_t handle
)
{

  uint32_t rc = VS_EOK;
  vs_session_object_t* p_session_obj = NULL;
  vss_istream_cmd_voc_amr_set_enc_rate_t amr_mode;
  vss_istream_cmd_set_enc_dtx_mode_t amr_dtx;
    
  /* set cached amr codec_mode */
  for ( ;; )
  {

    /*get the session object based on handle*/
    rc = vs_get_typed_object( handle, VS_OBJECT_TYPE_ENUM_SESSION,
                              ( ( vs_object_t** ) &p_session_obj ) );
    VS_PANIC_ON_ERROR( rc );


    /* if client hasn't set codec_mode yet, then do not update CVD */
    if ( ( p_session_obj->pending_voc.amr.codec_mode == 
                                    VS_VOCAMR_CODEC_MODE_NONE_V ) &&
         ( p_session_obj->current_voc.amr.codec_mode == 
                                    VS_VOCAMR_CODEC_MODE_NONE_V ) )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "AMR codec_mode NOT yet set by " \
           "client - ADSP default value would be used");
      break;
    }
    else
    {
      if ( p_session_obj->pending_voc.amr.codec_mode != 
                                       VS_VOCAMR_CODEC_MODE_NONE_V )
      {
        p_session_obj->current_voc.amr.codec_mode = 
                                  p_session_obj->pending_voc.amr.codec_mode;
        p_session_obj->pending_voc.amr.codec_mode = VS_VOCAMR_CODEC_MODE_NONE_V;
      }
      else
      {
        /* we reach here if current_voc.amr.codec_mode is valid (may be
         * after ADSP SSR)
         * in this case, just send current_voc.amr.codec_mode to ADSP */
      }
    }

    /* apply the codec_mode */
    amr_mode.mode = p_session_obj->current_voc.amr.codec_mode;

    rc = __aprv2_cmd_alloc_send(
           vs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
           vs_my_addr, ( (uint16_t) p_session_obj->header.handle ),
           vs_cvs_addr, p_session_obj->cvs_port,
           p_session_obj->header.handle, VSS_ISTREAM_CMD_VOC_AMR_SET_ENC_RATE,
           &amr_mode, sizeof( amr_mode ) );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
              "vs_vocamr_set_cached_voc_property(): Failed to communicate "
              "with CVS with rc=0x%08x", rc );
      break;
    }

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,   \
          "vs_vocamr_set_cached_voc_property(): codec_mode applied %d",
          amr_mode.mode );
    break;
  }  /* end of for ( ;; ) */

  /* set cached dtx_mode */
  for ( ;; )
  {

    /*get the session object based on handle*/
    rc = vs_get_typed_object( handle, VS_OBJECT_TYPE_ENUM_SESSION,
                              ( ( vs_object_t** ) &p_session_obj ) );
    VS_PANIC_ON_ERROR( rc );

    /* if client hasn't set dtx_mode yet, then do not update CVD.
     * if we are NOT here because of ADSP SSR, then do not update CVD.
     */
    if ( ( p_session_obj->pending_voc.amr.dtx_enable == 
                                               VS_VOC_DTX_MODE_UNDEFINED_V ) &&
         ( p_session_obj->current_voc.amr.dtx_enable == 
                                               VS_VOC_DTX_MODE_UNDEFINED_V ) )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "AMR dtx_mode is not yet set by " \
           "client - ADSP default value would be used");
      break;
    }
    else
    {
      if ( p_session_obj->pending_voc.amr.dtx_enable != 
                                            VS_VOC_DTX_MODE_UNDEFINED_V )
      {
        p_session_obj->current_voc.amr.dtx_enable = 
                                    p_session_obj->pending_voc.amr.dtx_enable;
        p_session_obj->pending_voc.amr.dtx_enable = 
                                    VS_VOC_DTX_MODE_UNDEFINED_V;
      }
      else
      {
        /* we reach here if current_voc.amr.codec_mode is valid (may be
         * after ADSP SSR)
         * in this case, just send current_voc.amr.codec_mode to ADSP */
      }
    }

    /* apply the codec_mode */
    amr_dtx.enable = p_session_obj->current_voc.amr.dtx_enable;

    rc = __aprv2_cmd_alloc_send(
           vs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
           vs_my_addr, ( (uint16_t) p_session_obj->header.handle ),
           vs_cvs_addr, p_session_obj->cvs_port,
           p_session_obj->header.handle, VSS_ISTREAM_CMD_SET_ENC_DTX_MODE,
           &amr_dtx, sizeof( amr_dtx ) );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
              "vs_vocamr_set_cached_voc_property(): Failed to communicate "
              "with CVS with rc=0x%08x", rc );
      break;
    }

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,   \
          "vs_vocamr_set_cached_voc_property(): dtx_mode applied %d",
          amr_dtx.enable );
    break;
  }  /* end of for ( ;; ) */
  
  return rc;

}  /* end of vs_vocamr_set_cached_values () */

/**
 * Gets the codec mode of the AMR vocoder.
 *
 * VS_EOK (0) when successful. 
 */
VS_INTERNAL uint32_t vs_vocamr_cmd_get_codec_mode (
  vs_vocamr_cmd_get_codec_mode_t* params
)
{

  uint32_t rc = VS_EOK;
  vs_session_object_t* p_session_obj = NULL;

  if (params == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_vocamr_cmd_get_codec_mode() - " \
          "NULL params" );
    return VS_EBADPARAM;
  }

  /* get the session object based on handle*/
  rc = vs_get_typed_object( params->handle, VS_OBJECT_TYPE_ENUM_SESSION,
                            ( ( vs_object_t** ) &p_session_obj ) );
  if ( rc != VS_EOK )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_vocamr_cmd_get_codec_mode() " \
            "- Invalid handle=0x%08x",
            params->handle );
    rc = VS_EBADPARAM;
    return rc;
  }

  /* read the current amr codec mode and return */
  *( params->ret_codec_mode ) =
          ( vs_vocamr_codec_mode_t )p_session_obj->current_voc.amr.codec_mode;

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,  "vs_vocamr_cmd_get_codec_mode() - " \
        "codec_mode=%d",
        *( params->ret_codec_mode ) );

  return VS_EOK;

}  /* end of vs_vocamr_cmd_get_codec_mode () */

/**
 * Gets the DTX mode of the AMR vocoder.
 *
 * VS_EOK (0) when successful. 
 */
VS_INTERNAL uint32_t vs_vocamr_cmd_get_dtx_mode (
  vs_vocamr_cmd_get_dtx_mode_t* params
)
{

  uint32_t rc = VS_EOK;
  vs_session_object_t* p_session_obj = NULL;
  
  if ( params == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_vocamr_cmd_get_dtx_mode() - " \
          "NULL params" );
    return VS_EBADPARAM;
  }

  /* get the session object based on handle*/
  rc = vs_get_typed_object( params->handle, VS_OBJECT_TYPE_ENUM_SESSION,
                            ( ( vs_object_t** ) &p_session_obj ) );
  if ( rc != VS_EOK )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "vs_vocamr_cmd_get_dtx_mode() - " \
            "Invalid handle=0x%08x",
            params->handle );
    rc = VS_EBADPARAM;
    return rc;
  }

  /* read the current amr dtx mode and return */
  *( params->ret_enable_flag ) =  
            ( bool_t )p_session_obj->current_voc.amr.dtx_enable;
  
  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,  "vs_vocamr_cmd_get_dtx_mode() - " \
        "dtx_enable=%d",
        *( params->ret_enable_flag ) );

  return VS_EOK;

}  /* end of vs_vocamr_cmd_get_dtx_mode () */

/* Conversion tables & macros from VS_AMR to AMRSUP & VS_AMR-WB to AMRSUP*/

amrsup_frame_type vs_amr_to_amrsup_frame_table[8] =
{
  AMRSUP_SPEECH_GOOD,     /* VS_AMR_SPEECH_GOOD */
  AMRSUP_SPEECH_DEGRADED, /* VS_AMR_SPEECH_DEGRADED */
  AMRSUP_ONSET,           /* VS_AMR_ONSET */
  AMRSUP_SPEECH_BAD,      /* VS_AMR_SPEECH_BAD */
  AMRSUP_SID_FIRST,       /* VS_AMR_SID_FIRST */
  AMRSUP_SID_UPDATE,      /* VS_AMR_SID_UPDATE */
  AMRSUP_SID_BAD,         /* VS_AMR_SID_BAD */
  AMRSUP_NO_DATA,         /* VS_AMR_NO_DATA */
};

amrsup_mode_type vs_amr_to_amrsup_mode_table[8] =
{
  AMRSUP_MODE_0475,        /* VS_AMR_MODE_0475 */
  AMRSUP_MODE_0515,        /* VS_AMR_MODE_0515 */
  AMRSUP_MODE_0590,        /* VS_AMR_MODE_0590 */
  AMRSUP_MODE_0670,        /* VS_AMR_MODE_0670 */
  AMRSUP_MODE_0740,        /* VS_AMR_MODE_0740 */
  AMRSUP_MODE_0795,        /* VS_AMR_MODE_0795 */
  AMRSUP_MODE_1020,        /* VS_AMR_MODE_1020 */
  AMRSUP_MODE_1220,        /* VS_AMR_MODE_1220 */
};

#define VS_AMR_TO_AMRSUP_FRAME(x) vs_amr_to_amrsup_frame_table[(x)]
#define VS_AMR_TO_AMRSUP_MODE(x) vs_amr_to_amrsup_mode_table[(x)]

/**
 * This function will determine number of bytes of AMR vocoder frame length
 * based on the frame type and frame rate.
 *
 * returns "number of bytes" when successful. 
 */
VS_INTERNAL uint32_t vs_vocamr_get_frame_len (
  vs_vocamr_frame_type_t frame_type,
  vs_vocamr_codec_mode_t amr_mode
)
{

  if ( ( amr_mode > VS_VOCAMR_CODEC_MODE_1220 ) ||
       ( frame_type > VS_VOCAMR_FRAME_TYPE_NO_DATA ) )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "vs_vocamr_get_frame_len(): wrong " \
           "inputs. amr_mode=%d ,frame_type=%d", amr_mode, frame_type );
    return 0;
  }

  return ( uint32_t )( amrsup_frame_len( VS_AMR_TO_AMRSUP_FRAME( frame_type ),
                     VS_AMR_TO_AMRSUP_MODE( amr_mode ) ) );

}  /* end of vs_vocamr_get_frame_len () */

