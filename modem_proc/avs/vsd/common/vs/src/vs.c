/**
  @file vs.c
  @brief This file contains the definitions of the public interfaces of VS.
  This file also includes some common function defintions.

*/

/*
   Copyright (C) 2013-2015 Qualcomm Technologies, Inc.
   All Rights Reserved.
   Confidential and Proprietary - Qualcomm Technologies, Inc.

   $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/avs/vsd/common/vs/src/vs.c#2 $
   $Author: shrakuma $

  when      who   what, where, why
  --------  ---   ------------------------------------------------------------
  08/05/14  sre   Added support for enabing eAMR over VS.
  10/18/13  rm    Replaced apr_list_init() with apr_list_init_v2().
                  Added vs_voc_cmd_thread_lock to make vs_voc_cmd_open thread
                  safe.
  08/09/13  sud   Renamed macro PC_ENV_SET to WINSIM.
                  Added session object cleanup in vs_deinit.
  07/30/13  sud   Updated software to report MSG_FATAL when fatal error
                  conditions occurs in client context.
  07/10/13  sg    Fixed the KW errors.
  06/29/13  sg    Fixed the banned QPSI memcpy APIs.
  06/26/13  sud   Updated debug messages and rearranged some of the functions
                  from vs_voc.c
  06/16/13  sud   Fixed some of the function defintions.
  06/13/13  hl    Added support for handling ADSP SSR.
  06/12/13  sud   Updated VS to handle errors and status propagation to client.
                  Updated VS to handle memory leak and memory corruption
                  issues.
                  Updated VS to handle command priorities and proper sequencing.
                  Updated VS to add proper debug messages
  06/01/13  sud   Fixed compiler warnings.
  04/16/13  sud   VS refactored code and fixed bugs.
  01/12/13  sud   Initial revision

*/

/******************************************************************************
 * INCLUDE HEADER FILES                                                     *
 ****************************************************************************/
#include <stdlib.h>

#include "msg.h"
#ifdef WINSIM
#include "drv_api.h"
#endif
#include "mmstd.h"
#include "voicecfg_api.h"
#include "voicecfg_items_i.h"
#include "vs.h"
#include "vs_i.h"
#include "vs_api.h"
#include "vss_public_if.h"
#include "vss_private_if.h"


/****************************************************************************
 * GLOBALS                                                                  *
 ****************************************************************************/

char_t vs_my_dns[] = "qcom.audio.vs";
char_t vs_cvs_dns[] = "qcom.audio.cvs";
char_t vs_mvm_dns[] = "qcom.audio.mvm";

/* VS address is set at initialization. */
uint16_t vs_my_addr;
/* MVM address is set at initialization. */
uint16_t vs_mvm_addr;
/* CVS address is set at initialization. */
uint16_t vs_cvs_addr;

/* flag that indicates the state of vs task. */
bool_t vs_is_initialized = FALSE;

bool_t vs_is_adsp_up_curr = TRUE; /* TODO: clean this up. */
bool_t vs_is_adsp_up_prev = TRUE; /* TODO: clean this up. */

/* Flag to indicate if eAMR vocoder can be enabled or not. */
extern bool_t vs_voicecfg_is_eamr_enabled ;

/****************************************************************************
 * VARIABLE DECLARATIONS                                                    *
 ****************************************************************************/
/* Lock Management */
apr_lock_t vs_int_lock;
apr_lock_t vs_thread_lock;
apr_lock_t vs_voc_cmd_thread_lock;

/* Heap Management */
apr_memmgr_type vs_heapmgr;
uint8_t vs_heap_pool[ VS_HEAP_SIZE_V ];

/* Object Management */
apr_objmgr_object_t vs_object_table[ VS_MAX_OBJECTS_V ];
apr_objmgr_t vs_objmgr;

/* Command Queue Management */
vs_cmd_item_t vs_cmd_pool[ VS_NUM_COMMANDS_V ];
apr_list_t vs_free_cmd_q;

/* APR Packet Queue management */
vs_work_item_t vs_apr_pkt_pool[ VS_NUM_APR_PKTS_V ];
apr_list_t vs_free_apr_pkt_q;

apr_list_t vs_apr_packet_p1_q;

/* commands in vs_apr_packet_p2_q and vs_cmd_packet_p2_q
 * are treated with same priority "medium - P2" - next to
 * vs_gating_cmd_packet_p1_q which is priority "high - P1"
 */
apr_list_t vs_apr_packet_p2_q;
apr_list_t vs_cmd_packet_p2_q;

/* gating command management.
 * vs_gating_cmd_packet_p1_q is the highest priority.
 * vs_gating_cmd_packet_p3_q is the lowest priority next to
 * vs_apr_packet_p2_q and vs_non_gating_cmd_packet_p2_q.
 */
vs_gating_control_t vs_gating_cmd_packet_p1_q;
vs_gating_control_t vs_gating_cmd_packet_p3_q;

/* IST Management */
apr_event_t vs_ist_event;
apr_event_t vs_thread_event;
apr_thread_t vs_ist_handle;
vs_thread_state_enum_t vs_ist_state = VS_THREAD_STATE_ENUM_INIT;

/* VS APR handle */
uint32_t vs_apr_handle;

/* pool of linked list node data structure for sessions */
vs_generic_item_t vs_session_list_pool[ VS_MAX_NUM_SESSIONS_V ];
apr_list_t vs_free_session_q;  /* session tracking - free nodes */
apr_list_t vs_used_session_q;  /* session tracking - used nodes */

/****************************************************************************
 * COMMON INTERNAL ROUTINES                                                 *
 ****************************************************************************/
VS_INTERNAL void vs_int_lock_fn (
  void
)
{

  ( void )apr_lock_enter( vs_int_lock );

}

VS_INTERNAL void vs_int_unlock_fn (
  void
)
{
   ( void )apr_lock_leave( vs_int_lock );

}

static void vs_thread_lock_fn (
  void
)
{

  ( void )apr_lock_enter( vs_thread_lock );

}

static void vs_thread_unlock_fn (
  void
)
{

  ( void )apr_lock_leave( vs_thread_lock );

}

VS_INTERNAL uint32_t vs_eamr_mode_call ( void )
{
  voicecfg_cmd_get_bool_item_t item;
  uint32_t rc;

  /* Vaidate eAMR on 2G */
  item.id = VOICECFG_CFG_IS_2G_SUB2_EAMR_ENABLED;
  item.ret_value = FALSE;

  rc =  voicecfg_call( VOICECFG_CMD_GET_BOOL_ITEM, &item, sizeof( item ) );

  if ( ! rc  && item.ret_value == TRUE )
  {
    vs_voicecfg_is_eamr_enabled = TRUE;
  }
  else
  {
    vs_voicecfg_is_eamr_enabled = FALSE;
  }
 
  MSG_1(MSG_SSID_DFLT, MSG_LEGACY_HIGH, "vs_eamr_mode_call(): eAMR configuration for "
                                        "GSM is : ", vs_voicecfg_is_eamr_enabled );
  
  return rc;
}  /* end of vs_eamr_mode_call () */
/****************************************************************************
 * END of COMMON INTERNAL ROUTINES                                                 *
 ****************************************************************************/


VS_INTERNAL void vs_signal_run (
  void
)
{

  ( void )apr_event_signal( vs_ist_event );

}


/****************************************************************************
 * VS COMMAND and APR packet queuing functions
 ****************************************************************************/
static uint32_t vs_queue_incoming_apr_packet (
  aprv2_packet_t* p_packet
)
{

  uint32_t rc = VS_EOK;
  vs_work_item_t* p_work_item = NULL;

  for ( ;; )
  {
    /* Get a free command structure. */
    rc = apr_list_remove_head( &vs_free_apr_pkt_q,
                               ( ( apr_list_node_t** ) &p_work_item ) );
    if ( rc )
    {
      rc = VS_ENORESOURCE;
      /* No free APR packet structure is available. */
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_FATAL, \
                 "VS ran out of APR packets, rc=0x%08x, vs_state=%d",
                  rc, vs_is_initialized );
      break;
    }

    /* Report command acceptance when requested. */
    rc = __aprv2_cmd_accept_command( vs_apr_handle, p_packet );
    if ( rc )
    { /* Can't report so abort the command. */
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "VS cannot report command " \
            "acceptance, rc=0x%08x" );
      ( void ) apr_list_add_tail( &vs_free_apr_pkt_q, &p_work_item->link );
      rc = VS_EFAILED;
      break;
    }

    p_work_item->packet = p_packet;

    switch ( p_work_item->packet->opcode )
    {
    case VSS_ISTREAM_EVT_READY:
    case VSS_ISTREAM_EVT_NOT_READY:
    case VSS_ISTREAM_EVT_SEND_ENC_BUFFER:
    case VSS_ISTREAM_EVT_REQUEST_DEC_BUFFER:
      {
        /* Add to priority2 queue - 2nd highest priority */
        rc = apr_list_add_tail( &vs_apr_packet_p2_q, &p_work_item->link );
        if ( rc )
        {
          rc = VS_EFAILED;
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                  "vs_queue_incoming_apr_packet() - ERROR: opcode=0x%08x,"
                  "rc=0x%08x", p_work_item->packet->opcode, rc );
          /* Add back to vs_free_apr_pkt_q */
          ( void ) apr_list_add_tail( &vs_free_apr_pkt_q, &p_work_item->link );
        }
        else
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,   \
                "vs_queue_incoming_apr_packet() - priority2_packet queued,"
                " opcode = 0x%08x", p_work_item->packet->opcode );
        }
      }
      break;

    case APRV2_IBASIC_EVT_ACCEPTED:
    case APRV2_IBASIC_RSP_RESULT:
      {
        /* Add to priority1 queue - highest priority */
        rc = apr_list_add_tail( &vs_apr_packet_p1_q, &p_work_item->link );
        if ( rc )
        {
          rc = VS_EFAILED;
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                  "vs_queue_incoming_apr_packet() - ERROR: opcode=0x%08x,"
                  "rc=0x%08x", p_work_item->packet->opcode, rc );
          /* Add back to vs_free_apr_pkt_q */
          ( void ) apr_list_add_tail( &vs_free_apr_pkt_q, &p_work_item->link );
        }
        else
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,   \
                "vs_queue_incoming_apr_packet() - priority1_packet queued,"
                " opcode=0x%08x", p_work_item->packet->opcode );
        }
      }
      break;

    default:
      break;

    }  /* end of switch ( p_work_item->packet->opcode ) */

    if ( rc  == VS_EOK )
    {
      vs_signal_run( );
    }
    else
    { /* free the resources */
      if ( p_packet != APR_NULL_V )
      {
        rc = __aprv2_cmd_free( vs_apr_handle, p_packet );
        VS_REPORT_FATAL_ON_ERROR( rc );
        p_packet = APR_NULL_V;
      }
    }  /* end of if ( rc  == VS_EOK ) */

    break;

  }  /* end of for ( ;; )*/

  return rc;

}  /* end of vs_queue_incoming_apr_packet () */

VS_INTERNAL uint32_t vs_queue_incoming_vs_command_packet (
  vs_cmd_packet_t* p_packet
)
{

  uint32_t rc = VS_EOK;
  vs_cmd_item_t* p_work_item = NULL;

  for ( ;; )
  {
    /* Get a free command structure. */
    rc = apr_list_remove_head( &vs_free_cmd_q,
                               ( ( apr_list_node_t** ) &p_work_item ) );
    if ( rc )
    {
      rc = VS_ENORESOURCE;
      /* No free command structure is available. */
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_FATAL, \
                 "VS ran out of command packets, rc=0x%08x, vs_state=%d",
                  rc, vs_is_initialized );
      break;
    }

    p_work_item->packet = p_packet;
    switch ( p_work_item->packet->cmd_id )
    {
    case VS_VOC_CMD_DISABLE:
      {
        /* Add to priority1 gating queue - highest priority */
        rc = apr_list_add_tail( &vs_gating_cmd_packet_p1_q.cmd_q,
                                &p_work_item->link );
        if ( rc )
        {
          rc = VS_EFAILED;
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                  "vs_queue_incoming_vs_command_packet()- ERROR: "
                  "cmd_id=0x%08x, rc=0x%08x",
                  p_work_item->packet->cmd_id, rc );
          /* Add back to vs_free_cmd_q */
          ( void ) apr_list_add_tail( &vs_free_cmd_q, &p_work_item->link );
        }
        else
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,   \
                "vs_queue_incoming_vs_command_packet() "
                "priority1_packet queued. cmd_id=0x%08x",
                p_work_item->packet->cmd_id );
        }
      }
      break;

    case VS_VOCAMR_CMD_SET_CODEC_MODE:
    case VS_VOCAMR_CMD_GET_CODEC_MODE:
    case VS_VOCAMR_CMD_SET_DTX_MODE:
    case VS_VOCAMR_CMD_GET_DTX_MODE:
    case VS_VOCAMRWB_CMD_SET_CODEC_MODE:
    case VS_VOCAMRWB_CMD_GET_CODEC_MODE:
    case VS_VOCAMRWB_CMD_SET_DTX_MODE:
    case VS_VOCAMRWB_CMD_GET_DTX_MODE:
    case VS_VOCFR_CMD_SET_DTX_MODE:
    case VS_VOCFR_CMD_GET_DTX_MODE:
    case VS_VOCEFR_CMD_SET_DTX_MODE:
    case VS_VOCEFR_CMD_GET_DTX_MODE:
    case VS_VOCHR_CMD_SET_DTX_MODE:
    case VS_VOCHR_CMD_GET_DTX_MODE:
      {
        /* Add to non-gating command priority2 queue - 2nd highest priority */
        rc = apr_list_add_tail( &vs_cmd_packet_p2_q,
                                &p_work_item->link );
        if ( rc )
        {
          rc = VS_EFAILED;
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                  "vs_queue_incoming_vs_command_packet() - ERROR: "
                  "cmd_id=0x%08x, rc=0x%08x",
                  p_work_item->packet->cmd_id, rc );
          /* Add back to vs_free_cmd_q */
          ( void ) apr_list_add_tail( &vs_free_cmd_q, &p_work_item->link );
        }
        else
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,   \
                "vs_queue_incoming_vs_command_packet() "
                "priority2_packet queued. cmd_id=0x%08x",
                p_work_item->packet->cmd_id );
        }
      }
      break;

    case VS_VOC_CMD_ENABLE:
    case VS_VOC_CMD_STANDBY:
    case VS_VOC_CMD_CLOSE:
    case VS_VOC_CMD_FLUSH_BUFFERS:
    case VS_VOC_CMD_CLEANUP_FOR_ADSP_SSR:
    case VS_VOC_CMD_POLL_CVD:
    default:
      {
        /* Add to priority3 gating queue - lowest priority */
        rc = apr_list_add_tail( &vs_gating_cmd_packet_p3_q.cmd_q,
                                &p_work_item->link );
        if ( rc )
        {
          rc = VS_EFAILED;
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
                  "vs_queue_incoming_vs_command_packet() - ERROR: "
                  "cmd_id=0x%08x, rc=0x%08x",
                  p_work_item->packet->cmd_id, rc );
          /* Add back to vs_free_cmd_q */
          ( void ) apr_list_add_tail( &vs_free_cmd_q, &p_work_item->link );
        }
        else
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,   \
                "vs_queue_incoming_vs_command_packet() "
                "priority3_packet queued. cmd_id=0x%08x",
                p_work_item->packet->cmd_id );
        }
      }
      break;
    }  /* end of switch ( p_packet->cmd_id ) */

    if ( rc == VS_EOK )
    {
      vs_signal_run( );
    }
    else
    {  /* free the resources */
      if ( p_packet != NULL )
      {
        if ( p_packet->params != NULL )
        {
          apr_memmgr_free(  &vs_heapmgr, p_packet->params);
          p_packet->params = NULL;
        }
        apr_memmgr_free(  &vs_heapmgr, p_packet);
        p_packet = NULL;
      }
      else
      {
        VS_REPORT_FATAL_ON_ERROR( VS_EUNEXPECTED );
      }
    }  /* end of if ( rc = VS_EOK ) */

    break;
  }  /* end of for ( ;; )*/

  return rc;

}  /* end of vs_queue_incoming_vs_command_packet () */
/****************************************************************************
 * END of VS COMMAND and APR packet queuing functions
 ****************************************************************************/


/****************************************************************************
 * VS event callback functions
 ****************************************************************************/
VS_INTERNAL uint32_t vs_common_send_event (
  uint32_t handle,
  vs_cmd_packet_t* p_cmd,
  uint32_t event_id,
  uint32_t status
)
{

  uint32_t rc = VS_EOK;
  vs_session_object_t* p_session_obj = NULL;
  vs_common_event_cmd_response_t cmd_resp;

  /* Get the session object with respect to the handle */
  rc = vs_get_typed_object( handle, VS_OBJECT_TYPE_ENUM_SESSION,
                           ( ( vs_object_t** ) &p_session_obj ) );
  VS_PANIC_ON_ERROR ( rc );

  switch ( event_id )
  {
  case VS_COMMON_EVENT_READY:
  case VS_COMMON_EVENT_NOT_READY:
    {
    p_session_obj->event_cb( event_id, NULL, 0, p_session_obj->session_context );

      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED,  "vs_common_send_event(): " \
            "event_id=0x%08x status=0x%08x handle=0x%08x",
            event_id, status, handle );
    break;
    }

  case VS_COMMON_EVENT_CMD_RESPONSE:
    {
      cmd_resp.cmd_id = p_cmd->cmd_id;
      cmd_resp.status_id = status;

      switch ( p_cmd->cmd_id )
      {
        case VS_VOC_CMD_ENABLE:
          cmd_resp.client_context =
                ( ( vs_voc_cmd_enable_t* )p_cmd->params )->client_context;
          break;

        case VS_VOC_CMD_DISABLE:
          cmd_resp.client_context =
                ( ( vs_voc_cmd_disable_t* )p_cmd->params )->client_context;
          break;

        case VS_VOC_CMD_STANDBY:
          cmd_resp.client_context =
                ( ( vs_voc_cmd_standby_t* )p_cmd->params )->client_context;
          break;

        case VS_VOC_CMD_FLUSH_BUFFERS:
          cmd_resp.client_context =
                ( ( vs_voc_cmd_flush_buffers_t* )p_cmd->params )->client_context;
          break;

        case VS_VOC_CMD_CLOSE:
          cmd_resp.client_context =
                ( ( vs_voc_cmd_close_t* )p_cmd->params )->client_context;
          break;

        default:
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "Unexpected cmd_id=0x%08x ", \
                  p_cmd->cmd_id );
          break;
      }  /* end of switch ( cmd->cmd_id ) */

      p_session_obj->event_cb( event_id, &cmd_resp,
                             sizeof( vs_common_event_cmd_response_t ),
                             p_session_obj->session_context );

      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED,  "vs_common_send_event(): " \
            "cmd_id=0x%08x status=0x%08x handle=0x%08x",
            p_cmd->cmd_id, status, handle );
    }
    break;

  default:
    break;

  }  /* end of switch ( event_id ) */

  return rc;

}  /* end of vs_common_send_event () */


/****************************************************************************
 * DISPATCHING ROUTINES                                                     *
 ****************************************************************************/
static void vs_dispatch_apr_packet (
  aprv2_packet_t* p_packet
)
{

  ( void )vs_queue_incoming_apr_packet( p_packet );

}  /* end of vs_dispatch_packet () */

static int32_t vs_isr_dispatch_fn (
  aprv2_packet_t* p_packet,
  void* dispatch_data
)
{

  vs_dispatch_apr_packet( p_packet );
  return ( int32_t )VS_EOK;

}  /* end of vs_isr_dispatch_fn() */


/****************************************************************************
 * GATING COMMAND ROUTINES                                                 *
 ****************************************************************************/
static uint32_t vs_gating_control_init (
  vs_gating_control_t* p_ctrl
)
{

  uint32_t rc = VS_EOK;

  if ( p_ctrl == NULL )
  {
    return VS_EBADPARAM;
  }

  rc = apr_list_init_v2( &p_ctrl->cmd_q, vs_thread_lock_fn, vs_thread_unlock_fn );
  if ( rc )
  {
    return VS_EFAILED;
  }

  p_ctrl->state = VS_GATING_CMD_STATE_ENUM_FETCH;
  p_ctrl->packet = NULL;
  p_ctrl->rootjob_obj = NULL;

  return VS_EOK;

}  /* end of vs_gating_control_init () */

static uint32_t vs_gating_control_destroy (
  vs_gating_control_t* p_ctrl
)
{

  if ( p_ctrl == NULL )
  {
    return VS_EBADPARAM;
  }

  ( void ) apr_list_destroy( &p_ctrl->cmd_q );

  return VS_EOK;

}  /* end of vs_gating_control_destroy () */

static void vs_response_fn_trampoline (
  uint32_t fn_index,
  aprv2_packet_t* p_packet
)
{

  uint32_t rc = VS_EOK;
  vs_object_t* p_object = NULL;
  uint32_t msg_type;

  if ( p_packet != APR_NULL_V )
  {
    msg_type = APR_GET_FIELD( APRV2_PKT_MSGTYPE, p_packet->header );

    if ( msg_type == APRV2_PKT_MSGTYPE_EVENT_V )
    {
      rc = vs_get_object( p_packet->dst_port, &p_object );
    }
    else
    {
      rc = vs_get_object( p_packet->token, &p_object );
    }

    if ( rc == VS_EOK )
    {
      switch ( p_object->header.type )
      {
      case VS_OBJECT_TYPE_ENUM_HANDLE:
        p_object->handle.fn_table[ fn_index ]( p_packet );
        return;

      case VS_OBJECT_TYPE_ENUM_SIMPLE_JOB:
        p_object->simple_job.fn_table[ fn_index ]( p_packet );
        return;

      default:
        break;
      }
    }

    rc =  __aprv2_cmd_free( vs_apr_handle, p_packet );
    VS_PANIC_ON_ERROR( rc );
    p_packet = APR_NULL_V;
  }
  else
  {
     VS_PANIC_ON_ERROR( VS_EUNEXPECTED );
  }
}  /* end of vs_response_fn_trampoline () */


/****************************************************************************
 * Command and Events processing functions
 ****************************************************************************/
static void vs_process_prioirty1_commands_and_events (
  void
)
{

  vs_cmd_item_t* p_cmd_item = NULL;
  vs_work_item_t* p_work_item = NULL;
  vs_gating_control_t* p_ctrl_p1 = NULL;
  vs_gating_control_t* p_ctrl_p3 = NULL;
  aprv2_packet_t* p_packet = APR_NULL_V;
  bool_t is_gating_queue_empty = FALSE;
  uint32_t rc = VS_EOK;

  /* Process non-gating priority1 commands/events.
  */
  while( apr_list_remove_head( &vs_apr_packet_p1_q,
                           ( ( apr_list_node_t** ) &p_work_item ) )== VS_EOK )
  {
    p_packet = p_work_item->packet;
    ( void ) apr_list_add_tail( &vs_free_apr_pkt_q, &p_work_item->link );

    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_LOW,   \
          "vs_process_prioirty1_commands_and_events(): "
          "opcode=0x%08x, token=0x%08x", p_packet->opcode, p_packet->token );

    switch ( p_packet->opcode )
    {
    case APRV2_IBASIC_EVT_ACCEPTED:
      {
        vs_response_fn_trampoline( VS_RESPONSE_FN_ENUM_ACCEPTED, p_packet );
      }
      break;

    case APRV2_IBASIC_RSP_RESULT:
      {
        vs_response_fn_trampoline( VS_RESPONSE_FN_ENUM_RESULT, p_packet );
      }
      break;

    default:
      break;

    }  /* end of switch ( p_packet->opcode ) */
  }  /* end of while () */

  /* If P3 gating command is being processed, skip P1 gating commands
   * processing. P1 command processing will start after current P3 gating
   * command is completed.
   */
  p_ctrl_p3 = &vs_gating_cmd_packet_p3_q;
  if ( p_ctrl_p3->state != VS_GATING_CMD_STATE_ENUM_FETCH )
  {
    /* Debug message to be removed */
    MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,  "P3 gating command in processing, " \
        "skipping P1 process" );
    return;
  }

  /* Process gating priority1 commands/events */
  p_ctrl_p1 = &vs_gating_cmd_packet_p1_q;
  do
  {
    switch ( p_ctrl_p1->state )
    {
    case VS_GATING_CMD_STATE_ENUM_FETCH:
      {
        /* Fetch the next gating command to execute. */
        rc = apr_list_remove_head( &p_ctrl_p1->cmd_q,
                                   ( ( apr_list_node_t** ) &p_cmd_item ) );
        if ( rc )
        {
          is_gating_queue_empty = TRUE;
          break;
        }

        p_ctrl_p1->packet = p_cmd_item->packet;
        p_ctrl_p1->state = VS_GATING_CMD_STATE_ENUM_EXECUTE;
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,   \
              "vs_process_prioirty1_commands_and_events(): cmd_id=0x%08x",
              p_ctrl_p1->packet->cmd_id );
        ( void ) apr_list_add_tail( &vs_free_cmd_q, &p_cmd_item->link );

      }
      break;

    case VS_GATING_CMD_STATE_ENUM_EXECUTE:
    case VS_GATING_CMD_STATE_ENUM_CONTINUE:
      {
        if ( p_ctrl_p1->packet != NULL )
        {
          switch ( p_ctrl_p1->packet->cmd_id )
          {
          case VS_VOC_CMD_DISABLE:
            {
              rc = vs_voc_cmd_disable_proc( p_ctrl_p1 );
            }
            break;

          default:
            {
              MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "Unsupported prioirty1 " \
                      "VS command, cmd_id=0x%08x"
                      "fetching next command", p_ctrl_p1->packet->cmd_id );

              /* free the p_ctrl_p1->packet->params memory. */
              if ( p_ctrl_p1->packet->params != NULL )
              {
                apr_memmgr_free( &vs_heapmgr, p_ctrl_p1->packet->params );
                p_ctrl_p1->packet->params = NULL;
              }

              /* free the p_ctrl_p1->packet memory. */
                apr_memmgr_free( &vs_heapmgr, p_ctrl_p1->packet );
                p_ctrl_p1->packet = NULL;

              /* set to VS_EOK to fetch the next command in queue. */
              rc = VS_EOK;

            }
            break;

          }  /* end of switch ( p_ctrl_p1->packet->cmd_id ) */
        }  /* end of  if (p_ctrl_p1->packet != NULL )*/
        else
        {
          VS_PANIC_ON_ERROR( VS_EUNEXPECTED );
        }

        switch ( rc )
        {
        case VS_EOK:
          {
            /* The current command finished so fetch the next command. */
            if( p_ctrl_p1->packet != NULL )
            {
              MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "Finished the priority1 " \
                    "gating command, cmd_id=0x%08x",
                    p_ctrl_p1->packet->cmd_id );
            }
            p_ctrl_p1->state = VS_GATING_CMD_STATE_ENUM_FETCH;

          }
          break;

        case VS_EBADPARAM:
          {
            /* The current command failed because of bad handle. you cannot
             * do anything. So just fetch the next command in the queue
             * for processing.
             */
            MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "FAILED processing the " \
                    "priority1 gating command - "
                    "invalid handle, cmd_id=0x%08x",
                    p_ctrl_p1->packet->cmd_id );
            p_ctrl_p1->state = VS_GATING_CMD_STATE_ENUM_FETCH;
          }
          break;

        case VS_ENOTREADY:
          {
            /* ADSP is not booted up yet. <TBD>
             */
            MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,   \
                 "vs_process_prioirty1_commands_and_events(): "
                 "ADSP is not booted up yet" );
          }
          break;

        case VS_EPENDING:
        default:
          {
            /* VS_EPENDING the overall progress stalls until one or more
             * external events or responses are received.
             */
            p_ctrl_p1->state = VS_GATING_CMD_STATE_ENUM_CONTINUE;
          }
          return;

        }  /* end of switch ( rc ) */
      }
      break;

    default:
      return;

    }  /* end of switch ( p1_ctrl->state ) */
  }
  while ( is_gating_queue_empty == FALSE );

}  /* end of vs_process_prioirty1_commands_and_events () */

static void vs_process_prioirty2_commands_and_events (
  void
)
{

  vs_work_item_t* p_work_item = NULL;
  aprv2_packet_t* p_packet = APR_NULL_V;
  vs_cmd_item_t* p_cmd_item = NULL;
  vs_cmd_item_t* p_cmd_item_dummy = NULL;
  vs_cmd_item_t* p_work_item_dummy = NULL;
  vs_cmd_packet_t* p_vs_packet = NULL;
  uint8_t* p_payload = NULL;
  vs_session_object_t* p_session_obj = NULL;
  vs_gating_control_t* p_ctrl_p1 = NULL;

  uint32_t rc = VS_EOK;
  uint32_t voc_packet_size;

  vs_voc_buffer_item_t* p_wbuffer_node = NULL;
  vs_voc_buffer_item_t* p_rbuffer_node = NULL;
  vs_voc_buffer_t* p_wbuffer = NULL;
  vs_voc_buffer_t* p_rbuffer = NULL;

  vs_vocamr_codec_mode_t amr_codec_mode;
  vs_vocamr_frame_type_t amr_frame_type;
  vs_vocamrwb_codec_mode_t amrwb_codec_mode;
  vs_vocamrwb_frame_type_t amrwb_frame_type;

  /* If P1 gating command is being processed, skip P2 commands
   * processing. P2 command processing will start after all P1 gating
   * commands are completed.
   */
  p_ctrl_p1 = &vs_gating_cmd_packet_p1_q;
  if ( p_ctrl_p1->state != VS_GATING_CMD_STATE_ENUM_FETCH )
  {
    /* Debug message to be removed */
    MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,  "P1 gating command in processing, " \
        "skipping P2 process" );
    return;
  }

  /* Process gating priority2 commands.
   * There are no commands/events in this category as of today.
   */

  /* Process non-gating priority2 apr events. */
  while( apr_list_remove_head( &vs_apr_packet_p2_q,
                           ( ( apr_list_node_t** ) &p_work_item ) )== VS_EOK )
  {
    p_packet = p_work_item->packet;
    ( void ) apr_list_add_tail( &vs_free_apr_pkt_q, &p_work_item->link );

    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_LOW,   \
          "vs_process_prioirty2_commands_and_events(): "
          "opcode=0x%08x, token=0x%08x", p_packet->opcode, p_packet->token );

    switch ( p_packet->opcode )
    {
    case VSS_ISTREAM_EVT_READY:
      {
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "VSS_ISTREAM_EVT_READY " \
               "received. src_port=0x%08x, "
               "dst_port=0x%08x, token=0x%08x",
               p_packet->src_port, p_packet->dst_port, p_packet->token );
        /* Get the session object with respect to the handle */
        rc = vs_get_typed_object( p_packet->dst_port, VS_OBJECT_TYPE_ENUM_SESSION,
                                 ( ( vs_object_t** ) &p_session_obj ) );
        VS_PANIC_ON_ERROR( rc );

        vs_common_send_event( p_session_obj->header.handle, NULL,
                              VS_COMMON_EVENT_READY, VS_EOK ) ;
        p_session_obj->stream_ready = TRUE;
        vs_voc_state_control( p_session_obj );

	
        /* Apply cached properties to make sure latest properties are appiled */
        vs_voc_action_set_cached_stream_properties( p_session_obj );
	
        if ( p_packet != APR_NULL_V )
        {
          rc = __aprv2_cmd_free( vs_apr_handle, p_packet );
          VS_PANIC_ON_ERROR( rc );
          p_packet = APR_NULL_V;
        }
      }
      break;

    case VSS_ISTREAM_EVT_NOT_READY:
      {
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "VSS_ISTREAM_EVT_NOT_READY " \
               "received. src_port=0x%08x, "
               "dst_port=0x%08x, token=0x%08x",
               p_packet->src_port, p_packet->dst_port, p_packet->token );
        /* Get the session object with respect to the handle */
        rc = vs_get_typed_object( p_packet->dst_port, VS_OBJECT_TYPE_ENUM_SESSION,
                                 ( ( vs_object_t** ) &p_session_obj ) );
        VS_PANIC_ON_ERROR( rc );

        if ( p_session_obj->stream_ready == TRUE )
        {
          vs_common_send_event( p_session_obj->header.handle, NULL,
                                VS_COMMON_EVENT_NOT_READY, VS_EOK ) ;
          p_session_obj->stream_ready = FALSE;
          vs_voc_state_control( p_session_obj );
        }

        if ( p_packet != APR_NULL_V )
        {
          rc = __aprv2_cmd_free( vs_apr_handle, p_packet );
          VS_PANIC_ON_ERROR( rc );
          p_packet = APR_NULL_V;
        }
      }
      break;

    case VSS_ISTREAM_EVT_SEND_ENC_BUFFER:
      {
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_LOW,
               "VSS_ISTREAM_EVT_SEND_ENC_BUFFER received. src_port = 0x%08x, "
               "dst_port=0x%08x, token=0x%08x", p_packet->src_port,
               p_packet->dst_port, p_packet->token );

        rc = vs_get_typed_object( p_packet->dst_port, VS_OBJECT_TYPE_ENUM_SESSION,
                                  ( (vs_object_t** ) &p_session_obj ) );
        VS_PANIC_ON_ERROR( rc );

        if ( p_session_obj->state == VS_VOC_STATE_ENUM_RUN )
        {
          voc_packet_size = ( APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( p_packet->header ) -
                                 sizeof( vss_istream_evt_send_enc_buffer_t ) );
          p_payload = APR_PTR_END_OF( APRV2_PKT_GET_PAYLOAD( void, p_packet ),
                                 sizeof( vss_istream_evt_send_enc_buffer_t ) );

          if ( voc_packet_size > VS_VOC_MAX_PACKET_LENGTH_V )
          {
            MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                   "VSS_ISTREAM_EVT_SEND_ENC_BUFFER: unexpected vocoder pkt "
                   "length=%d, discarding packet",voc_packet_size );
            if ( p_packet != APR_NULL_V )
            {
              rc = __aprv2_cmd_free( vs_apr_handle, p_packet );
              VS_PANIC_ON_ERROR( rc );
              p_packet = APR_NULL_V;
            }
            break;
          }

          /* Detach the node from alloc_buf_q if it is Primed. */
          p_rbuffer_node = 
             ( vs_voc_buffer_item_t* )( &p_session_obj->alloc_buf_q.dummy );
          for ( ;; )
          {
            rc = apr_list_get_next( &p_session_obj->alloc_buf_q,
                                    ( ( apr_list_node_t* ) p_rbuffer_node ),
                                   ( ( apr_list_node_t** ) &p_rbuffer_node ) );

            /* If alloc_buf_q list is ended. */
            if ( rc ) break;

            /* Got read primed buffer from client. */
            if ( p_rbuffer_node->buf_type == VS_VOC_BUFFER_TYPE_PRIMED )
            {
              ( void ) apr_list_delete( &p_session_obj->alloc_buf_q,
                                        ( (apr_list_node_t* ) p_rbuffer_node ) );
              MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,
                     "Read Primed buffer available. = 0x%8X",  p_rbuffer_node->buf );
              break;
            }
          }
          
          if( rc == VS_EOK )
          {
            p_rbuffer = p_rbuffer_node->buf;

            /* Fail if voc_packet_size is larger than the client allocated
             * buffer
             */
            if ( ( voc_packet_size - VS_VOC_PACKET_HDR_LENGTH_V ) >
                                                       p_rbuffer->max_size )
            {
              MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                     "CVS delivered encoder packet size (%d) is larger than the "
                     "allocated buffer size (%d).",
                     ( voc_packet_size - VS_VOC_PACKET_HDR_LENGTH_V ),
                     p_rbuffer->max_size );

              /* Put the buffer back to alloc_buf_q */
              ( void ) apr_list_add_tail( &p_session_obj->alloc_buf_q,
                                          &( p_rbuffer_node->link ) );
            }
            else
            {
              p_rbuffer->media_id = p_session_obj->media_id;
              p_rbuffer_node->buf_type = VS_VOC_BUFFER_TYPE_READ;

              switch( p_rbuffer->media_id )
              {
              case VS_COMMON_MEDIA_ID_FR:
              case VS_COMMON_MEDIA_ID_HR:
              case VS_COMMON_MEDIA_ID_EFR:
                {
                  p_rbuffer->version = VS_VOC_BUFFER_MINOR_VERSION_V;
                  p_rbuffer->flags = VS_VOC_BUFFER_VALID_SPEECH_FRAME;

                  /* Update SID here. BFI, TAF and UFI (HR only) bits are
                   * applicable only on downlink.
                   */
                  ( ( vs_vocfr_frame_info_t* )p_rbuffer->frame_info )->sid =
                                               ( ( *p_payload ) & 0x06 ) >> 1 ;

                  p_rbuffer->size = voc_packet_size - VS_VOC_PACKET_HDR_LENGTH_V ;
                  mmstd_memcpy( p_rbuffer->frame,p_rbuffer->max_size,
                          ( p_payload + VS_VOC_PACKET_HDR_LENGTH_V ),
                          p_rbuffer->size );

                  p_rbuffer->timestamp = VS_VOC_BUFFER_TIMESTAMP_NOT_SUPPORTED_V;

                  ( void ) apr_list_add_tail( &p_session_obj->read_buf_q,
                                                &( p_rbuffer_node->link ) );
                  p_session_obj->event_cb( VS_VOC_EVENT_READ_AVAILABLE, NULL, 0,
                                           p_session_obj->session_context );
                }
                break;

              case VS_COMMON_MEDIA_ID_AMR:
                {
                  p_rbuffer->version = VS_VOC_BUFFER_MINOR_VERSION_V;
                  p_rbuffer->flags = VS_VOC_BUFFER_VALID_SPEECH_FRAME;

                  /* extract amr codec mode and frame type */
                  amr_codec_mode = ( vs_vocamr_codec_mode_t )
                                     ( ( *p_payload ) & 0x0F ) ;
                  amr_frame_type = ( vs_vocamr_frame_type_t )
                                     ( ( ( *p_payload ) & 0xF0 ) >> 4 );

                  /* validate amr codec mode and frame type */
                  if ( amr_codec_mode < VS_VOCAMR_CODEC_MODE_0475 ||
                       amr_codec_mode > VS_VOCAMR_CODEC_MODE_1220 ||
                       amr_frame_type < VS_VOCAMR_FRAME_TYPE_SPEECH_GOOD ||
                       amr_frame_type > VS_VOCAMR_FRAME_TYPE_NO_DATA )
                  {
                    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                           "VSS_ISTREAM_EVT_SEND_ENC_BUFFER: Invalid AMR packet "
                           "header: codec_mode=%d, frame_type=%d", amr_codec_mode,
                           amr_frame_type );
                  }
                  else
                  {
                    ( ( vs_vocamr_frame_info_t* )p_rbuffer->frame_info )->codec_mode =
                                                 amr_codec_mode;
                    ( ( vs_vocamr_frame_info_t* )p_rbuffer->frame_info )->frame_type =
                                                 amr_frame_type;

                    p_rbuffer->size = voc_packet_size - VS_VOC_PACKET_HDR_LENGTH_V ;
                    mmstd_memcpy( p_rbuffer->frame,p_rbuffer->max_size,
                            ( p_payload + VS_VOC_PACKET_HDR_LENGTH_V ),
                            p_rbuffer->size );

                    p_rbuffer->timestamp = VS_VOC_BUFFER_TIMESTAMP_NOT_SUPPORTED_V;

                    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_LOW,
                           "VSS_ISTREAM_EVT_SEND_ENC_BUFFER (AMR): size=%d, "
                           "codec_mode=%d, frame_type=%d", p_rbuffer->size, 
                           amr_codec_mode, amr_frame_type );

                    ( void ) apr_list_add_tail( &p_session_obj->read_buf_q,
                                                &p_rbuffer_node->link);
                    p_session_obj->event_cb( VS_VOC_EVENT_READ_AVAILABLE, NULL, 0,
                                           p_session_obj->session_context );
                  }  /* end of if ( mode < VS_VOCAMR_CODEC_MODE_0475 ||  */
                }
                break;

              case VS_COMMON_MEDIA_ID_AMRWB:
                {
                  p_rbuffer->version = VS_VOC_BUFFER_MINOR_VERSION_V;
                  p_rbuffer->flags = VS_VOC_BUFFER_VALID_SPEECH_FRAME;

                  /* extract mode and frame type */
                  amrwb_codec_mode = ( vs_vocamrwb_codec_mode_t )
                                                ( ( *p_payload ) & 0x0F );
                  amrwb_frame_type = ( vs_vocamrwb_frame_type_t )
                                       ( ( ( *p_payload ) & 0xF0 ) >> 4 );

                  /* validate mode and frame type */
                  if ( amrwb_codec_mode < VS_VOCAMRWB_CODEC_MODE_0660 ||
                       amrwb_codec_mode > VS_VOCAMRWB_CODEC_MODE_2385 ||
                       amrwb_frame_type < VS_VOCAMRWB_FRAME_TYPE_SPEECH_GOOD ||
                       amrwb_frame_type > VS_VOCAMRWB_FRAME_TYPE_NO_DATA )
                  {
                    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                           "VSS_ISTREAM_EVT_SEND_ENC_BUFFER: Invalid AMRWB packet "
                           "header: codec_mode=%d, frame_type=%d", amrwb_codec_mode,
                           amrwb_frame_type );
                  }
                  else
                  {
                    ( ( vs_vocamrwb_frame_info_t* )p_rbuffer->frame_info )->codec_mode =
                                                   amrwb_codec_mode;
                    ( ( vs_vocamrwb_frame_info_t* )p_rbuffer->frame_info )->frame_type =
                                                   amrwb_frame_type;

                    p_rbuffer->size = voc_packet_size - VS_VOC_PACKET_HDR_LENGTH_V ;
                    mmstd_memcpy( p_rbuffer->frame,p_rbuffer->max_size,
                            ( p_payload + VS_VOC_PACKET_HDR_LENGTH_V ),
                             p_rbuffer->size );

                    p_rbuffer->timestamp = VS_VOC_BUFFER_TIMESTAMP_NOT_SUPPORTED_V;

                    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_LOW,
                           "VSS_ISTREAM_EVT_SEND_ENC_BUFFER(AMR_WB):size=%d, "
                           "codec_mode=%d, frame_type=%d",p_rbuffer->size, 
                           amrwb_codec_mode, amrwb_frame_type );

                    ( void ) apr_list_add_tail( &p_session_obj->read_buf_q,
                                                &p_rbuffer_node->link);
                    p_session_obj->event_cb( VS_VOC_EVENT_READ_AVAILABLE, NULL, 0,
                                           p_session_obj->session_context );
                  }  /* end of if ( mode < VS_VOCAMRWB_CODEC_MODE_0660 ||  */
                }
                break;

              default:
                MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                       "Media ID did not match any available one. media_id=0x%08x",
                       p_rbuffer->media_id );
                break;

              }  /* end of switch( p_rbuffer->media_id ) */
            }  /* end of if ( ( voc_packet_size - VS_VOC_PACKET_HDR_LENGTH_V ) > */
          }
          else
          {
            MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                 "tx_packet_status = NO client buffer to copy vocoder packet "
                 "from ADSP." );
          } /* end of if( rc == VS_EOK ) */
        }
        else
        {
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                  "VSS_ISTREAM_EVT_SEND_ENC_BUFFER in incorrect state"
                 "session_state=%d, client=0x%08x", p_session_obj->state,
                 p_session_obj->client_id );
        }  /* end of if ( session_obj->state == VS_VOC_STATE_ENUM_RUN ) */

        if ( p_packet != APR_NULL_V)
        {
          rc = __aprv2_cmd_free( vs_apr_handle, p_packet );
          VS_PANIC_ON_ERROR( rc );
          p_packet = APR_NULL_V;
        }
      }
      break;

    case VSS_ISTREAM_EVT_REQUEST_DEC_BUFFER:
      {
        rc = vs_get_typed_object( p_packet->dst_port,
                                  VS_OBJECT_TYPE_ENUM_SESSION,
                                  ( ( vs_object_t** ) &p_session_obj ) );
        VS_PANIC_ON_ERROR( rc );

        if ( p_session_obj->state == VS_VOC_STATE_ENUM_RUN )
        {
          if( apr_list_remove_head( &p_session_obj->write_buf_q,
                                    ( ( apr_list_node_t** ) &p_wbuffer_node ) )
                                     == VS_EOK )
          {

            /* Reset vocoder buffer type to VS_VOC_BUFFER_TYPE_NONE.
             * Add node back to alloc_buf_q.
             */
            if( p_wbuffer_node != NULL )
            {
              p_wbuffer = ( ( vs_voc_buffer_t* ) p_wbuffer_node->buf );

              p_wbuffer_node->buf_type = VS_VOC_BUFFER_TYPE_NONE;
              ( void ) apr_list_add_tail( &p_session_obj->alloc_buf_q,
                                          &p_wbuffer_node->link );
              p_wbuffer_node = NULL;
            }

            /* Get decoder packet from modem (VS client).
             * Add packet header.
             * Send decoder packet to cvs.
             */
            rc = vs_voc_send_dec_packet( p_session_obj->header.handle, p_wbuffer,
                                         p_packet );
            if ( rc )
            {
              MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                     "vs_voc_send_dec_packet() retuned ERROR. rc=0x%08x", rc );
            }

            p_session_obj->event_cb( VS_VOC_EVENT_WRITE_BUFFER_RETURNED, p_wbuffer,
                                   sizeof( vs_voc_event_write_buffer_returned_t ),
                                   p_session_obj->session_context );
          }
          else
          {
            MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                 "rx_packet_status = NO vocoder packet to give to ADSP."  );
          }
        }
        else
        {
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                  "VSS_ISTREAM_EVT_REQUEST_DEC_BUFFER in incorrect state"
                 "session_state=%d, client=0x%08x", p_session_obj->state,
                 p_session_obj->client_id );
        }  /* end of if ( session_obj->state == VS_VOC_STATE_ENUM_RUN ) */

        if ( p_packet != APR_NULL_V )
        {
          rc = __aprv2_cmd_free( vs_apr_handle, p_packet );
          VS_PANIC_ON_ERROR( rc );
          p_packet = APR_NULL_V;
        }
      }
      break;

    default:
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "vs_process_prioirty2_commands_and_events(): Unsupported APR "
               "event. opcode=0x%08x, token=0x%08x", p_packet->opcode,
               p_packet->token );

        if ( p_packet != APR_NULL_V )
        {
          rc = __aprv2_cmd_free( vs_apr_handle, p_packet );
          VS_PANIC_ON_ERROR( rc );
          p_packet = APR_NULL_V;
        }
      }
      break;

    }  /* end of switch ( p_packet->opcode ) */

    /* If there are any packets in priority1 amd/packet queue, go process
     * those first and come back here to process remaining packets in
     * priority2 queue.
     */
    p_ctrl_p1 = &vs_gating_cmd_packet_p1_q;

    if ( ( ( apr_list_peak_head( &p_ctrl_p1->cmd_q,
             ( ( apr_list_node_t** ) &p_cmd_item_dummy ) )== VS_EOK ) ) ||
           ( ( apr_list_peak_head( &vs_apr_packet_p1_q,
             ( ( apr_list_node_t** ) &p_work_item_dummy ) )== VS_EOK ) ) )
    {
      vs_process_prioirty1_commands_and_events( );
      p_ctrl_p1 = &vs_gating_cmd_packet_p1_q;
      if ( p_ctrl_p1->state != VS_GATING_CMD_STATE_ENUM_FETCH )
      {
        /* Debug message to be removed */
        MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "P1 pending command in processing, skipping rest of P2 processing" );
        return;
      }
    }  /* end of if ( ( ( apr_list_peak_head( &p_ctrl_p1->cmd_q, */

  }  /* end of while( apr_list_remove_head( &vs_priority2_packet_q, */

  /* Process non-gating prioirty2 VS commands */
  while( apr_list_remove_head( &vs_cmd_packet_p2_q,
                           ( ( apr_list_node_t** ) &p_cmd_item ) )== VS_EOK )
  {
    p_vs_packet = p_cmd_item->packet;
    ( void ) apr_list_add_tail( &vs_free_cmd_q, &p_cmd_item->link );

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,
           "vs_process_prioirty2_commands_and_events(): cmd_id=0x%08x",
           p_vs_packet->cmd_id );

    switch ( p_vs_packet->cmd_id )
    {
    case VS_VOCAMR_CMD_SET_CODEC_MODE:
      rc = vs_vocamr_cmd_set_codec_mode_proc(
           ( ( vs_vocamr_cmd_set_codec_mode_t* )p_vs_packet->params )->handle,
           ( ( vs_vocamr_cmd_set_codec_mode_t* )p_vs_packet->params )->codec_mode );
      break;

    case VS_VOCAMRWB_CMD_SET_CODEC_MODE:
      rc = vs_vocamrwb_cmd_set_codec_mode_proc(
           ( ( vs_vocamrwb_cmd_set_codec_mode_t* )p_vs_packet->params )->handle,
           ( ( vs_vocamrwb_cmd_set_codec_mode_t* )p_vs_packet ->params )->codec_mode );
      break;

    case VS_VOCAMR_CMD_SET_DTX_MODE:
      rc = vs_vocamr_cmd_set_dtx_mode_proc(
            ( ( vs_vocamr_cmd_set_dtx_mode_t* )p_vs_packet->params )->handle,
            ( ( vs_vocamr_cmd_set_dtx_mode_t* )p_vs_packet->params )->enable_flag );
      break;

    case VS_VOCAMRWB_CMD_SET_DTX_MODE:
      rc = vs_vocamrwb_cmd_set_dtx_mode_proc(
           ( ( vs_vocamrwb_cmd_set_dtx_mode_t* )p_vs_packet->params )->handle,
           ( ( vs_vocamrwb_cmd_set_dtx_mode_t* )p_vs_packet->params )->enable_flag );
      break;

    case VS_VOCEFR_CMD_SET_DTX_MODE:
      rc = vs_vocefr_cmd_set_dtx_mode_proc(
           ( ( vs_vocefr_cmd_set_dtx_mode_t* )p_vs_packet->params )->handle,
           ( ( vs_vocefr_cmd_set_dtx_mode_t* )p_vs_packet->params )->enable_flag );
      break;

    case VS_VOCFR_CMD_SET_DTX_MODE:
      rc = vs_vocfr_cmd_set_dtx_mode_proc(
           ( ( vs_vocfr_cmd_set_dtx_mode_t* )p_vs_packet->params )->handle,
           ( ( vs_vocfr_cmd_set_dtx_mode_t* )p_vs_packet->params )->enable_flag );
      break;

    case VS_VOCHR_CMD_SET_DTX_MODE:
      rc = vs_vochr_cmd_set_dtx_mode_proc(
           ( ( vs_vochr_cmd_set_dtx_mode_t* )p_vs_packet->params )->handle,
           ( ( vs_vochr_cmd_set_dtx_mode_t* )p_vs_packet->params )->enable_flag );
      break;

    default:
      break;

    }  /* end of switch ( p_vs_packet->cmd_id ) */

    if( p_vs_packet->params != NULL )
    {
      apr_memmgr_free( &vs_heapmgr, p_vs_packet->params );
      p_vs_packet->params = NULL;
    }

    if( p_vs_packet != NULL )
    {
      apr_memmgr_free( &vs_heapmgr, p_vs_packet );
      p_vs_packet = NULL;
    }

    /* If there are any packets in priority1 cmd/packet queue, go process
     * those first and come back here to process remaining packets in
     * priority2 queue.
     */
    p_ctrl_p1 = &vs_gating_cmd_packet_p1_q;
    if ( ( ( apr_list_peak_head( &p_ctrl_p1->cmd_q,
             ( ( apr_list_node_t** ) &p_cmd_item_dummy ) )== VS_EOK ) ) ||
           ( ( apr_list_peak_head( &vs_apr_packet_p1_q,
             ( ( apr_list_node_t** ) &p_work_item_dummy ) )== VS_EOK ) ) )
    {
      vs_process_prioirty1_commands_and_events( );
      p_ctrl_p1 = &vs_gating_cmd_packet_p1_q;
      if ( p_ctrl_p1->state != VS_GATING_CMD_STATE_ENUM_FETCH )
      {
        /* Debug message to be removed */
        MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,  "P1 pending command in " \
            "processing, skipping rest of "
            "P2 processing" );
        return;
      }
    }  /* end of if ( ( ( apr_list_peak_head( &p_ctrl_p1->cmd_q, */

  }  /* end of while( apr_list_remove_head( &vs_non_gating_cmd_packet_p2_q, */

}  /* end of vs_process_prioirty2_commands_and_events () */

static void vs_process_prioirty3_commands_and_events (
  void
)
{

  vs_cmd_item_t* p_cmd_item_dummy = NULL;
  vs_cmd_item_t* p_work_item_dummy = NULL;
  vs_gating_control_t* p_ctrl_p3 = NULL;
  vs_gating_control_t* p_ctrl_p1 = NULL;
  bool_t is_gating_queue_empty = FALSE;
  uint32_t rc = VS_EOK;
  vs_cmd_item_t* p_cmd_item = NULL;

  /* If P1 gating command is being processed, skip P3 commands`
   * processing. P3 command processing will start after all P1 gating
   * commands are completed.
   */
  p_ctrl_p1 = &vs_gating_cmd_packet_p1_q;
  if ( p_ctrl_p1->state != VS_GATING_CMD_STATE_ENUM_FETCH )
  {
    /* Debug message to be removed */
    MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,  "P1 pending command in processing, " \
        "skipping P3 process" );
    return;
  }

  /* Process gating priority3 commands */
  p_ctrl_p3 = &vs_gating_cmd_packet_p3_q;
  do
  {
    switch ( p_ctrl_p3->state )
    {
    case VS_GATING_CMD_STATE_ENUM_FETCH:
      {
        /* Fetch the next gating command to execute. */
        rc = apr_list_remove_head( &p_ctrl_p3->cmd_q,
                                   ( ( apr_list_node_t** ) &p_cmd_item ) );
        if ( rc )
        {
          is_gating_queue_empty = TRUE;
          break;
        }

        p_ctrl_p3->packet = p_cmd_item->packet;
        p_ctrl_p3->state = VS_GATING_CMD_STATE_ENUM_EXECUTE;
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,   \
              "vs_process_prioirty3_commands_and_events(): cmd_id=0x%08x",
              p_ctrl_p3->packet->cmd_id );
        ( void ) apr_list_add_tail( &vs_free_cmd_q, &p_cmd_item->link );
      }
      break;

    case VS_GATING_CMD_STATE_ENUM_EXECUTE:
    case VS_GATING_CMD_STATE_ENUM_CONTINUE:
      {
        if ( p_ctrl_p3->packet != NULL )
        {
          switch ( p_ctrl_p3->packet->cmd_id )
          {
          case VS_VOC_CMD_ENABLE:
            rc = vs_voc_cmd_enable_proc( p_ctrl_p3 );
            break;

          case VS_VOC_CMD_STANDBY:
            rc = vs_voc_cmd_standby_proc( p_ctrl_p3 );
            break;

          case VS_VOC_CMD_CLOSE:
            rc = vs_voc_cmd_close_proc( p_ctrl_p3 );
            break;

          case VS_VOC_CMD_FLUSH_BUFFERS:
            rc = vs_voc_cmd_flush_buffers_proc( p_ctrl_p3 );
            break;

          case VS_VOC_CMD_CLEANUP_FOR_ADSP_SSR:
            rc = vs_voc_cmd_cleanup_for_adsp_ssr_proc( p_ctrl_p3 );
            break;

          case VS_VOC_CMD_POLL_CVD:
            rc = vs_voc_cmd_poll_cvd_proc( p_ctrl_p3 );
            break;

          default:
            {
              MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "Unsupported prioirty3 " \
                      "VS command. cmd_id=0x%08x ,"
                      "fetching next command", p_ctrl_p3->packet->cmd_id );

              /* free the p_ctrl_p3->packet->params memory. */
              if ( p_ctrl_p3->packet->params != NULL )
              {
                apr_memmgr_free( &vs_heapmgr, p_ctrl_p3->packet->params );
                p_ctrl_p3->packet->params = NULL;
              }

              /* free the p_ctrl_p3->packet memory. */
              apr_memmgr_free( &vs_heapmgr, p_ctrl_p3->packet );
              p_ctrl_p3->packet = NULL;
              /* set to VS_EOK to fetch the next command in queue. */
              rc = VS_EOK;

            }
            break;
          }  /* end of switch ( p_ctrl_p3->packet->cmd_id ) */
        }  /*end of if ( p_ctrl_p3->packet != NULL )*/
        else
        {
          VS_PANIC_ON_ERROR( VS_EUNEXPECTED );
        }

        if ( rc == VS_EOK )
        {
          /* The current command finished so fetch the next command. */
          if( p_ctrl_p3->packet != NULL )
          {
            MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "Finished " \
                  "vs_process_prioirty3_commands_and_events()"
                  " - cmd_id=0x%08x", p_ctrl_p3->packet->cmd_id );
          }
          p_ctrl_p3->state = VS_GATING_CMD_STATE_ENUM_FETCH;
        }
        else
        {
          if ( rc == VS_ENOTREADY )
          {
            /* ADSP is not booted up yet. push this command to the end of
               the queue. We will process this command later when external
               events or responses are received. <TBD> */
            MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,   \
                 "vs_process_prioirty3_commands_and_events(): "
                 "ADSP is not booted up yet" );
          }
          else
          {
            /* Assuming the current gating command control routine returns
             * VS_EPENDING the overall progress stalls until one or more
             * external events or responses are received.
             */
            p_ctrl_p3->state = VS_GATING_CMD_STATE_ENUM_CONTINUE;
          }
        }  /* if ( rc == VS_EOK ) */
      }

      break;

      default:
        return;
    }  /* end of switch ( p_ctrl_p3->state ) */

    if ( ( p_ctrl_p3->state == VS_GATING_CMD_STATE_ENUM_FETCH ) ||
         ( ( apr_list_peak_head( &vs_apr_packet_p1_q,
           ( ( apr_list_node_t** ) &p_work_item_dummy ) )== VS_EOK ) )
       )
    {

      /* If there are any packets in priority1 packet queues,
       * go process those first and come back here.
       */
      p_ctrl_p1 = &vs_gating_cmd_packet_p1_q;
      if ( ( ( apr_list_peak_head( &p_ctrl_p1->cmd_q,
               ( ( apr_list_node_t** ) &p_cmd_item_dummy ) )== VS_EOK ) ) ||
             ( ( apr_list_peak_head( &vs_apr_packet_p1_q,
               ( ( apr_list_node_t** ) &p_work_item_dummy ) )== VS_EOK ) )
         )
      {
        vs_process_prioirty1_commands_and_events( );
        p_ctrl_p1 = &vs_gating_cmd_packet_p1_q;
        if ( p_ctrl_p1->state != VS_GATING_CMD_STATE_ENUM_FETCH )
        {
          /* Debug message to be removed */
          MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,  "P1 pending command in " \
              "processing, skipping rest of "
              "P3 processing" );
          break;
        }
      }
    }  /* end of if ( ( p_ctrl_p3->state == VS_GATING_CMD_STATE_ENUM_FETCH ) */

    if ( p_ctrl_p3->state == VS_GATING_CMD_STATE_ENUM_FETCH )
    {
      /* If there are any packets in priority2 packet queues, go process those
       * first and come back here.
       */
      if( ( ( apr_list_peak_head( &vs_apr_packet_p2_q,
            ( ( apr_list_node_t** ) &p_work_item_dummy ) )== VS_EOK ) ) ||
          ( ( apr_list_peak_head( &vs_cmd_packet_p2_q,
            ( ( apr_list_node_t** ) &p_cmd_item_dummy ) )== VS_EOK ) ) )
      {
        vs_process_prioirty2_commands_and_events( );
      }
    }  /* end of if ( p_ctrl_p3->state == VS_GATING_CMD_STATE_ENUM_FETCH ) */

   /*  P3 processing stalls until one or more external events or responses
    *  are received.
    */
   if ( p_ctrl_p3->state == VS_GATING_CMD_STATE_ENUM_CONTINUE )
   {
     break;
   }

  }
  while ( is_gating_queue_empty == FALSE );

  /* Process non-gating priority1 commands
   * There are no commands/events in this category as of today.
   */

}  /* end of vs_process_prioirty3_commands_and_events () */


/****************************************************************************
 * VS OBJECT ROUTINES...START                                               *
 ****************************************************************************/
static uint32_t vs_typecast_object (
  apr_objmgr_object_t* p_store,
  vs_object_type_enum_t type,
  vs_object_t** p_ret_obj
)
{

  vs_object_t* p_obj = NULL;

  if ( ( p_store == NULL ) || ( p_ret_obj == NULL ) )
  {
    return VS_EBADPARAM;
  }

  p_obj = ( ( vs_object_t* ) p_store->any.ptr );

  if ( p_obj->header.type != type )
  {
    return VS_EFAILED;
  }

  *p_ret_obj = p_obj;

  return VS_EOK;

}  /* end of vs_typecast_object () */

VS_INTERNAL uint32_t vs_get_object (
  uint32_t handle,
  vs_object_t** ret_obj
)
{

  uint32_t rc = VS_EOK;
  apr_objmgr_object_t* p_objmgr_obj = NULL;

  for ( ;; )
  {
    if ( ret_obj == NULL )
    {
      rc = VS_EBADPARAM;
      break;
    }

    rc = apr_objmgr_find_object( &vs_objmgr, handle, &p_objmgr_obj );
    if ( rc )
    {
      rc = VS_EFAILED;
      break;
    }

    *ret_obj = ( ( vs_object_t* ) p_objmgr_obj->any.ptr );
    break;
  }  /* end of for ( ;; ) */

  return rc;

}  /* end of vs_get_object () */

VS_INTERNAL uint32_t vs_get_typed_object (
  uint32_t handle,
  vs_object_type_enum_t type,
  vs_object_t** p_ret_obj
)
{

  uint32_t rc = VS_EOK;
  apr_objmgr_object_t* p_store = NULL;

  if ( handle == 0 )
  {
    return VS_EHANDLE;
  }

  rc = apr_objmgr_find_object( &vs_objmgr, handle, &p_store );
  if ( rc )
  {
    return ( uint32_t )rc;
  }

  rc = vs_typecast_object( p_store, type, p_ret_obj );
  if ( rc )
  {
    return ( uint32_t )rc;
  }

  return VS_EOK;

} /* end of vs_get_typed_object () */
/****************************************************************************
 * VS OBJECT ROUTINES...END                                                 *
 ****************************************************************************/


/****************************************************************************
 * VS OBJECT CREATION AND DESTRUCTION ROUTINES...START                      *
 ****************************************************************************/
VS_INTERNAL uint32_t vs_mem_alloc_object (
  uint32_t size,
  vs_object_t** p_ret_object
)
{

  uint32_t rc = VS_EOK;
  vs_object_t* p_vs_obj = NULL;
  apr_objmgr_object_t* p_objmgr_obj = NULL;

  if ( p_ret_object == NULL )
  {
    return VS_EBADPARAM;
  }

  { /* Allocate memory for the VS object. */
    p_vs_obj = apr_memmgr_malloc( &vs_heapmgr, size );
    if ( p_vs_obj == NULL )
    {
      rc = VS_ENORESOURCE;
      return rc;
    }

    /* Allocate a new handle for the VS object. */
    rc = apr_objmgr_alloc_object( &vs_objmgr, &p_objmgr_obj );
    if ( rc )
    {
      rc = VS_ENORESOURCE;
      return rc;
    }

    /* Link the VS object to the handle. */
    p_objmgr_obj->any.ptr = p_vs_obj;

    /* Initialize the base VS object header. */
    p_vs_obj->header.handle = p_objmgr_obj->handle;
    p_vs_obj->header.type = VS_OBJECT_TYPE_ENUM_UNINITIALIZED;
  }

  *p_ret_object = p_vs_obj;

  return VS_EOK;

}  /* end of vs_mem_alloc_object () */

VS_INTERNAL uint32_t vs_mem_free_object (
  vs_object_t* p_object
)
{

  if ( p_object == NULL )
  {
    return VS_EBADPARAM;
  }

  if ( ( p_object->header.type == VS_OBJECT_TYPE_ENUM_SIMPLE_JOB ) ||
       ( p_object->header.type == VS_OBJECT_TYPE_ENUM_HANDLE ) )
  {
    ( void ) vs_voc_ssr_untrack_object( p_object->header.handle );
  }

  /* Free the object memory and object handle. */
  ( void ) apr_objmgr_free_object( &vs_objmgr, p_object->header.handle );

  apr_memmgr_free( &vs_heapmgr, p_object );

  return VS_EOK;

}  /* end of vs_mem_free_object () */

static void vs_default_event_handler_fn (
  aprv2_packet_t* p_packet
)
{

  uint32_t rc = VS_EOK;

  /* The default event handler just drops the packet. A specific event
   * handler routine should be written to something more useful.
   */
  if ( p_packet != APR_NULL_V )
  {
    rc =  __aprv2_cmd_free( vs_apr_handle, p_packet );
    VS_PANIC_ON_ERROR( rc );
    p_packet = APR_NULL_V;
  }

}  /* end of vs_default_event_handler_fn () */

static void vs_set_default_response_table (
  vs_response_fn_table_t table
)
{

  int i;

  if ( table == NULL )
  {
    return;
  }

  /* Initialize the state response handler function table. */
  for ( i = 0; i < VS_RESPONSE_FN_ENUM_MAX; ++i )
  {
    table[ i ] = vs_default_event_handler_fn;
  }

}  /* end of vs_set_default_response_table () */

static void vs_simple_self_destruct_result_rsp_fn (
  aprv2_packet_t* p_packet
)
{

  uint32_t rc = VS_EOK;
  vs_token_object_t* p_obj = NULL;

  if ( p_packet != APR_NULL_V )
  {
    rc = vs_get_typed_object( p_packet->token, VS_OBJECT_TYPE_ENUM_HANDLE,
                               ( ( vs_object_t** ) &p_obj ) );
    if ( rc == VS_EOK )
    {
      rc = vs_mem_free_object( ( vs_object_t* ) p_obj );
      VS_PANIC_ON_ERROR( rc );
      p_obj = NULL;
    }

    rc = __aprv2_cmd_free( vs_apr_handle, p_packet );
    VS_PANIC_ON_ERROR( rc );
    p_packet = APR_NULL_V;
  }
  else
  {
    VS_PANIC_ON_ERROR( VS_EUNEXPECTED );
  }

  vs_signal_run();

}  /* end of vs_simple_self_destruct_result_rsp_fn () */

VS_INTERNAL void vs_simple_result_rsp_fn (
  aprv2_packet_t* p_packet
)
{

  uint32_t rc = VS_EOK;
  vs_simple_job_object_t* p_obj = NULL;

  if ( p_packet != APR_NULL_V )
  {
    rc = vs_get_typed_object( p_packet->token, VS_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                              ( ( vs_object_t** ) &p_obj ) );
    VS_PANIC_ON_ERROR( rc );
    p_obj->is_completed = TRUE;
    p_obj->status =
        APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, p_packet )->status;

    rc = __aprv2_cmd_free( vs_apr_handle, p_packet );
    VS_PANIC_ON_ERROR( rc );
    p_packet = APR_NULL_V;
  }
  else
  {
    VS_PANIC_ON_ERROR( VS_EUNEXPECTED );
  }
  vs_signal_run();

}  /* end of vs_simple_result_rsp_fn ()  */

VS_INTERNAL void vs_simple_transition_result_rsp_fn (
  aprv2_packet_t* p_packet
)
{

  uint32_t rc = VS_EOK;
  vs_simple_job_object_t* p_job_obj = NULL;
  vs_session_object_t* p_session_obj = NULL;

  if ( p_packet != APR_NULL_V )
  {
    rc = vs_get_typed_object( p_packet->token, VS_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                               ( ( vs_object_t** ) &p_job_obj ) );
    VS_PANIC_ON_ERROR( rc );
    p_job_obj->is_completed = TRUE;
    p_job_obj->status =
      APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, p_packet )->status;

    rc = vs_get_typed_object( p_job_obj->context_handle,
                              VS_OBJECT_TYPE_ENUM_SESSION,
                              ( ( vs_object_t** ) &p_session_obj ) );
    VS_PANIC_ON_ERROR( rc );
    p_session_obj->session_ctrl.status = p_job_obj->status;

    /* If completed action failed, log the error. */
    if ( p_session_obj->session_ctrl.status != VS_EOK )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,   \
           "vs_simple_transition_result_rsp_fn(): "
           "Command 0x%08x failed with rc=0x%08x",
           APRV2_PKT_GET_PAYLOAD(aprv2_ibasic_rsp_result_t, p_packet)->opcode,
              p_session_obj->session_ctrl.status );
    }

    rc = __aprv2_cmd_free( vs_apr_handle, p_packet );
    VS_PANIC_ON_ERROR( rc );
    p_packet = APR_NULL_V;
  }
  else
  {
    VS_PANIC_ON_ERROR( VS_EUNEXPECTED );
  }

  vs_signal_run();

}  /* end of vs_simple_transition_result_rsp_fn () */

VS_INTERNAL uint32_t vs_create_autofree_token_object (
  vs_token_object_t** p_ret_obj
)
{

  uint32_t rc = VS_EOK;
  vs_token_object_t* p_vs_obj = NULL;

  if ( p_ret_obj == NULL )
  {
    return VS_EBADPARAM;
  }

  rc = vs_mem_alloc_object( sizeof( vs_token_object_t ),
                            ( ( vs_object_t** ) &p_vs_obj ) );
  VS_PANIC_ON_ERROR( rc );

  {  /* Initialize the handle object. */
    p_vs_obj->header.type = VS_OBJECT_TYPE_ENUM_HANDLE;
    vs_set_default_response_table( p_vs_obj->fn_table );
    p_vs_obj->fn_table[ VS_RESPONSE_FN_ENUM_RESULT ] =
                        vs_simple_self_destruct_result_rsp_fn;
  }

  { /* Track the allocated object in order to free in case of ADSP SSR. */
    rc = vs_voc_ssr_track_object( p_vs_obj->header.handle );
  }

  *p_ret_obj = p_vs_obj;

  return VS_EOK;

}  /* end of vs_create_autofree_token_object () */

VS_INTERNAL uint32_t vs_create_simple_job_object (
  uint32_t context_handle,
  vs_simple_job_object_t** p_ret_job_obj
)
{

  uint32_t rc = VS_EOK;
  vs_simple_job_object_t* p_vs_obj = NULL;

  if ( p_ret_job_obj == NULL )
  {
    return VS_EBADPARAM;
  }

  rc = vs_mem_alloc_object( sizeof( vs_simple_job_object_t ),
                           ( ( vs_object_t** ) &p_vs_obj ) );
  VS_PANIC_ON_ERROR( rc );

  { /* Initialize the simple job object. */
    p_vs_obj->header.type = VS_OBJECT_TYPE_ENUM_SIMPLE_JOB;
    p_vs_obj->context_handle = context_handle;
    vs_set_default_response_table( p_vs_obj->fn_table );
    p_vs_obj->fn_table[ VS_RESPONSE_FN_ENUM_RESULT ] = vs_simple_result_rsp_fn;
    p_vs_obj->is_accepted = FALSE;
    p_vs_obj->is_completed = FALSE;
  }

  { /* Track the allocated object in order to free in case of ADSP SSR. */
    rc = vs_voc_ssr_track_object( p_vs_obj->header.handle );
  }

  *p_ret_job_obj = p_vs_obj;

  return VS_EOK;

}  /* end of vs_create_simple_job_object () */

VS_INTERNAL uint32_t vs_create_sequencer_job_object (
  vs_sequencer_job_object_t** p_ret_job_obj
)
{

  uint32_t rc = VS_EOK;
  vs_sequencer_job_object_t* p_job_obj = NULL;

  if ( p_ret_job_obj == NULL )
  {
    return VS_EBADPARAM;
  }

  rc = vs_mem_alloc_object( sizeof( vs_sequencer_job_object_t ),
                             ( ( vs_object_t** ) &p_job_obj ) );
  VS_PANIC_ON_ERROR( rc );

  { /* Initialize the pending job object. */
    p_job_obj->header.type = VS_OBJECT_TYPE_ENUM_SEQUENCER_JOB;

    p_job_obj->state = APR_NULL_V;
    p_job_obj->subjob_obj = NULL;
    p_job_obj->status = APR_UNDEFINED_ID_V;
  }

  *p_ret_job_obj = p_job_obj;

  return VS_EOK;

}  /* end of vs_create_sequencer_job_object () */


/****************************************************************************
 * IST ROUTINES                                                             *
 ****************************************************************************/
static uint32_t vs_run (
  void
)
{

  /* Run the state machine for all session objects if the ADSP SSR
   * event is received and that caused ADSP state change.
   */
  if ( vs_is_adsp_up_curr != vs_is_adsp_up_prev )
  {
    vs_is_adsp_up_prev = vs_is_adsp_up_curr;
    vs_voc_run_state_control( );
  }

  /* Process highest prioirty commands and events.
   * VS_VOC_CMD_DISABLE is the only one in this category now.
   */
  vs_process_prioirty1_commands_and_events( );

  /* Process vocoder packet exchange commands/events and vocoder
   * properties commands.
   */
  vs_process_prioirty2_commands_and_events( );

  /* Process control commands except VS_VOC_CMD_DISABLE */
  vs_process_prioirty3_commands_and_events( );

  return VS_EOK;

}  /* end of vs_run () */

static int32_t vs_ist (
  void* param
)
{

  uint32_t rc = VS_EOK;

  rc = ( uint32_t ) apr_event_create( &vs_ist_event );
  VS_PANIC_ON_ERROR( rc );

  vs_ist_state = VS_THREAD_STATE_ENUM_READY;
  apr_event_signal( vs_thread_event );

  do
  {
    rc = ( uint32_t )apr_event_wait( vs_ist_event );
    #ifndef USE_SINGLE_AUDSVC_THREAD
    ( void ) vs_run( );
    #endif /* !USE_SINGLE_AUDSVC_THREAD */
  }
  while ( rc == VS_EOK );

  rc = ( uint32_t )apr_event_destroy( vs_ist_event );
  VS_PANIC_ON_ERROR( rc );

  vs_ist_state = VS_THREAD_STATE_ENUM_EXIT;

  return ( int32_t )VS_EOK;

}  /* end of vs_ist () */


/****************************************************************************
 * EXTERNAL API ROUTINES                                                    *
 ****************************************************************************/
static uint32_t vs_init (
  void
)
{

  uint32_t rc = VS_EOK;
  uint32_t i;
  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "vs_init() Date %s Time %s",
        __DATE__, __TIME__ );

  /* Lets create three locks, one for simple resource synchronization,
   * second one non pre-emptebile and the third to make synchornous
   * blocking calls thread safe.
   */
  {
    rc = apr_lock_create( APR_LOCK_TYPE_INTERRUPT, &vs_int_lock );
    rc = apr_lock_create( APR_LOCK_TYPE_MUTEX, &vs_thread_lock );
    rc = apr_lock_create( APR_LOCK_TYPE_MUTEX, &vs_voc_cmd_thread_lock );
  }

  /* Initialize the custom heap. */
  {
    apr_memmgr_init_heap( &vs_heapmgr, ( ( void* ) &vs_heap_pool ),
                           sizeof( vs_heap_pool ), NULL, NULL );
    /* memheap mustn't be called from interrupt context. No locking is
     * required in task context because all commands are serialized.
     */
  }

  /* Initialize the object manager. */
  {
    apr_objmgr_setup_params_t params;
    params.table = vs_object_table;
    params.total_bits = VS_HANDLE_TOTAL_BITS_V;
    params.index_bits = VS_HANDLE_INDEX_BITS_V;
    params.lock_fn = vs_int_lock_fn;
    params.unlock_fn = vs_int_unlock_fn;
    rc = apr_objmgr_construct( &vs_objmgr, &params );
  }

  /* Initialize gating controls structures. */
  {
    rc = vs_gating_control_init( &vs_gating_cmd_packet_p1_q );
    rc = vs_gating_control_init( &vs_gating_cmd_packet_p3_q );
  }

  /* Initialize the command queue management. */
  {
    /* Populate free command structures */
    {
      rc = apr_list_init_v2( &vs_free_cmd_q, vs_int_lock_fn, vs_int_unlock_fn );
      for ( i = 0; i < VS_NUM_COMMANDS_V; ++i )
      {
        ( void ) apr_list_init_node( ( apr_list_node_t* ) &vs_cmd_pool[i] );
        rc = apr_list_add_tail( &vs_free_cmd_q,
                                ( ( apr_list_node_t* ) &vs_cmd_pool[i] ) );
      }
    }

    /* Populate free command structures */
    {
      rc = apr_list_init_v2( &vs_free_apr_pkt_q, vs_int_lock_fn, vs_int_unlock_fn );
      for ( i = 0; i < VS_NUM_APR_PKTS_V; ++i )
      {
        ( void ) apr_list_init_node( ( apr_list_node_t* ) &vs_apr_pkt_pool[i] );
        rc = apr_list_add_tail( &vs_free_apr_pkt_q,
                                ( ( apr_list_node_t* ) &vs_apr_pkt_pool[i] ) );
      }
    }
  }

  {
    rc = apr_list_init_v2( &vs_apr_packet_p1_q, vs_int_lock_fn,
                         vs_int_unlock_fn );
    rc = apr_list_init_v2( &vs_apr_packet_p2_q, vs_int_lock_fn,
                         vs_int_unlock_fn );
    rc = apr_list_init_v2( &vs_cmd_packet_p2_q, vs_int_lock_fn,
                         vs_int_unlock_fn );
  }

  /* Initialize the VS session tracking list. */
  {
    {
      rc = apr_list_init_v2( &vs_free_session_q, NULL, NULL );
      /* Populate the free session list. */
      for ( i = 0; i < VS_MAX_NUM_SESSIONS_V; ++i )
      {
        ( void ) apr_list_init_node( ( apr_list_node_t* ) &vs_session_list_pool[i] );
        rc = apr_list_add_tail(
               &vs_free_session_q,
               ( ( apr_list_node_t* ) &vs_session_list_pool[ i ] ) );
      }
    }
    rc = apr_list_init_v2( &vs_used_session_q, NULL, NULL );
  }

  {
#ifndef WINSIM
    /* Get eAMR config from voicecfg framework. */
    rc = vs_eamr_mode_call( );
#else
    rc = APR_EFAILED;
#endif /* !WINSIM */
    if ( rc )
    { /* Not able to get eAMR config from voicecfg, e.g. Voice config item file
       * doesn't exist or there are file read errors. eAMR will be disabled.
       */
      vs_voicecfg_is_eamr_enabled = FALSE;  
    }
  }
  
  { 
    rc = apr_thread_create( &vs_ist_handle, VS_IST_NAME, VS_IST_PRIORITY,
                            NULL, 0, vs_ist, NULL );
    VS_PANIC_ON_ERROR( rc );

    while ( vs_ist_state != VS_THREAD_STATE_ENUM_READY )
    {
      rc = apr_misc_sleep( 1000000 ); /* Sleep for 1ms. */
    }
  }

  /* Initialize VS VOC SSR management. */
  rc = vs_voc_ssr_init( );

  /* Initialize the APR resource. */
  {
    rc = __aprv2_cmd_local_dns_lookup(
           vs_mvm_dns, sizeof( vs_mvm_dns ), &vs_mvm_addr );

    rc = __aprv2_cmd_local_dns_lookup(
           vs_cvs_dns, sizeof( vs_cvs_dns ), &vs_cvs_addr );

    rc = __aprv2_cmd_register2(
           &vs_apr_handle, vs_my_dns, sizeof( vs_my_dns ), 0,
           vs_isr_dispatch_fn, NULL, &vs_my_addr );
  }

  vs_is_initialized = TRUE;

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "vs_init(), rc=0x%08x", rc );
  return rc;

}  /* end of vs_init () */

static uint32_t vs_post_init (
  void
)
{

  return VS_EOK;

}  /* end of vs_post_init () */

static uint32_t vs_deinit (
  void
)
{

  vs_session_object_t* p_session_obj = NULL;
  vs_generic_item_t* p_generic_item = NULL;
  uint32_t rc = VS_EOK;

  vs_is_initialized = FALSE;

  /* Release the APR resource. */
  ( void ) __aprv2_cmd_deregister( vs_apr_handle );

  ( void ) vs_voc_ssr_deinit( );

  /* Destroy VS_IST */
  ( void ) apr_event_signal_abortall( vs_ist_event );

  while ( vs_ist_state != VS_THREAD_STATE_ENUM_EXIT )
  {
    ( void ) apr_misc_sleep( 1000000 ); /* Sleep for 1ms. */
  }

  ( void ) apr_thread_destroy( vs_ist_handle );

  /* Destroy session objects */
  p_generic_item = ( ( vs_generic_item_t* ) &vs_used_session_q.dummy );
  for ( ;; )
  {
    rc = apr_list_get_next( &vs_used_session_q,
                            ( ( apr_list_node_t* ) p_generic_item ),
                            ( ( apr_list_node_t** ) &p_generic_item ) );
    if ( rc )
    {
      break;
    }

    rc = vs_get_typed_object( p_generic_item->handle,
                              VS_OBJECT_TYPE_ENUM_SESSION,
                              ( ( vs_object_t** ) &p_session_obj ) );

    rc = apr_list_destroy( &p_session_obj->alloc_buf_q );
    rc = apr_list_destroy( &p_session_obj->write_buf_q );
    rc = apr_list_destroy( &p_session_obj->read_buf_q );
    rc = vs_mem_free_object( (vs_object_t*) p_session_obj);

  }  /* end of for ( ;; ) */

  /* Release session queue structures. */
  ( void ) apr_list_destroy( &vs_used_session_q );
  ( void ) apr_list_destroy( &vs_free_session_q );

  /* Release the command queue management. */
  ( void ) apr_list_destroy( &vs_cmd_packet_p2_q);
  ( void ) apr_list_destroy( &vs_apr_packet_p2_q );
  ( void ) apr_list_destroy( &vs_apr_packet_p1_q );

  ( void ) apr_list_destroy( &vs_free_apr_pkt_q );
  ( void ) apr_list_destroy( &vs_free_cmd_q );

  /* Release gating control structures */
  ( void ) vs_gating_control_destroy( &vs_gating_cmd_packet_p3_q );
  ( void ) vs_gating_control_destroy( &vs_gating_cmd_packet_p1_q );

  /* Release the object management. */
  ( void ) apr_objmgr_destruct( &vs_objmgr );

  /* Release the locks. */
  ( void ) apr_lock_destroy( vs_thread_lock );
  ( void ) apr_lock_destroy( vs_int_lock );
  ( void ) apr_lock_destroy( vs_voc_cmd_thread_lock );

  MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,  "vs_deinit(), rc=VS_EOK" );
  return VS_EOK;

}  /* end of vs_deinit () */

static uint32_t vs_pre_deinit (
  void
)
{

  return VS_EOK;

} /* end of vs_pre_deinit () */

/**
 * Provides a single API entry-point into VS.
 *
 * \param[in] cmd_id The command identifier to execute.
 * \param[in] params The command parameters.
 * \param[out] size The size of the command parameters in bytes.
 *
 * \return VS_EOK (0) when successful.
 */
VS_EXTERNAL uint32_t vs_call (
  uint32_t cmd_id,
  void* params,
  uint32_t size
)
{

  uint32_t rc = VS_EOK;

  switch ( cmd_id )
  {
  #if defined WINSIM
  case DRV_CMDID_INIT:
  #else
  case VS_CALLINDEX_ENUM_INIT:
  #endif
    {
      rc = vs_init( );
    }
    break;

  #if defined WINSIM
  case DRV_CMDID_POSTINIT:
  #else
  case VS_CALLINDEX_ENUM_POSTINIT:
  #endif
    {
      rc = vs_post_init( );
    }
    break;

  #if defined WINSIM
  case DRV_CMDID_PREDEINIT:
  #else
  case VS_CALLINDEX_ENUM_PREDEINIT:
  #endif
    {
      rc = vs_pre_deinit( );
    }
    break;

  #if defined WINSIM
  case DRV_CMDID_DEINIT:
  #else
  case VS_CALLINDEX_ENUM_DEINIT:
  #endif
    {
      rc = vs_deinit( );
    }
    break;

  case VS_CALLINDEX_ENUM_RUN:
    {
      rc = vs_run( );
    }
    break;

  case VS_VOC_CMD_OPEN:
    {
    rc = vs_voc_cmd_open( ( vs_voc_cmd_open_t* ) params );
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,  "vs_call(): VS_VOC_CMD_OPEN " \
          "cmd_id=0x%08x, rc=0x%08x",
          cmd_id, rc );
    }
    break;

  case VS_VOC_CMD_CLOSE:
    {
    rc = vs_voc_cmd_close( ( vs_voc_cmd_close_t* ) params );
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,  "vs_call(): VS_VOC_CMD_CLOSE " \
          "cmd_id=0x%08x, rc=0x%08x",
          cmd_id, rc );
    }
    break;

  case VS_VOC_CMD_ENABLE:
    {
    rc = vs_voc_cmd_enable( ( vs_voc_cmd_enable_t* ) params );
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,  "vs_call(): VS_VOC_CMD_ENABLE " \
          "cmd_id=0x%08x, rc=0x%08x",
          cmd_id, rc );
    }
    break;

  case VS_VOC_CMD_DISABLE:
    {
    rc = vs_voc_cmd_disable( ( vs_voc_cmd_disable_t* ) params );
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,  "vs_call(): VS_VOC_CMD_DISABLE " \
          "cmd_id=0x%08x, rc=0x%08x",
          cmd_id, rc );
    }
    break;

  case VS_VOC_CMD_STANDBY:
    {
    rc = vs_voc_cmd_standby( ( vs_voc_cmd_standby_t* ) params );
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,  "vs_call(): VS_VOC_CMD_STANDBY " \
          "cmd_id=0x%08x, rc=0x%08x",
          cmd_id, rc );
    }
    break;

  case VS_VOC_CMD_ALLOC_BUFFER:
    {
    rc = vs_voc_cmd_alloc_buffer( ( vs_voc_cmd_alloc_buffer_t* ) params );
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,  "vs_call(): " \
          "VS_VOC_CMD_ALLOC_BUFFER cmd_id=0x%08x, rc=0x%08x",
          cmd_id, rc );
    }
    break;

  case VS_VOC_CMD_FREE_BUFFER:
    {
    rc = vs_voc_cmd_free_buffer( ( vs_voc_cmd_free_buffer_t* ) params );
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,  "vs_call(): VS_VOC_CMD_FREE_BUFFER " \
          "cmd_id=0x%08x, rc=0x%08x",
          cmd_id, rc );
    }
    break;

  case VS_VOC_CMD_PRIME_READ_BUFFER:
    {
    rc = vs_voc_cmd_prime_read_buffer(
               ( vs_voc_cmd_prime_read_buffer_t* ) params );
    }
    break;

  case VS_VOC_CMD_READ_BUFFER:
    {
    rc = vs_voc_cmd_read_buffer( ( vs_voc_cmd_read_buffer_t* ) params );
    }
    break;

  case VS_VOC_CMD_WRITE_BUFFER:
    {
    rc = vs_voc_cmd_write_buffer( ( vs_voc_cmd_write_buffer_t* ) params );
    }
    break;

  case VS_VOC_CMD_FLUSH_BUFFERS:
    {
    rc = vs_voc_cmd_flush_buffers( ( vs_voc_cmd_flush_buffers_t* ) params );
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,  "vs_call(): " \
          "VS_VOC_CMD_FLUSH_BUFFERS cmd_id=0x%08x, "
          "rc=0x%08x", cmd_id, rc );
    }
    break;

  case VS_VOCAMR_CMD_SET_CODEC_MODE:
    {
    rc = vs_vocamr_cmd_set_codec_mode(
               ( vs_vocamr_cmd_set_codec_mode_t* ) params );
    }
    break;

  case VS_VOCAMR_CMD_GET_CODEC_MODE:
    {
    rc = vs_vocamr_cmd_get_codec_mode(
               ( vs_vocamr_cmd_get_codec_mode_t* ) params );
    }
    break;

  case VS_VOCAMR_CMD_SET_DTX_MODE:
    {
    rc = vs_vocamr_cmd_set_dtx_mode(
               ( vs_vocamr_cmd_set_dtx_mode_t* ) params );
    }
    break;

  case VS_VOCAMR_CMD_GET_DTX_MODE:
    {
    rc = vs_vocamr_cmd_get_dtx_mode(
               ( vs_vocamr_cmd_get_dtx_mode_t* ) params );
    }
    break;

  case VS_VOCAMRWB_CMD_SET_CODEC_MODE:
    {
    rc = vs_vocamrwb_cmd_set_codec_mode(
               ( vs_vocamrwb_cmd_set_codec_mode_t* ) params );
    }
    break;

  case VS_VOCAMRWB_CMD_GET_CODEC_MODE:
    {
    rc = vs_vocamrwb_cmd_get_codec_mode(
               ( vs_vocamrwb_cmd_get_codec_mode_t* ) params );
    }
    break;

  case VS_VOCAMRWB_CMD_SET_DTX_MODE:
    {
    rc = vs_vocamrwb_cmd_set_dtx_mode(
               ( vs_vocamrwb_cmd_set_dtx_mode_t* ) params );
    }
    break;

  case VS_VOCAMRWB_CMD_GET_DTX_MODE:
    {
    rc = vs_vocamrwb_cmd_get_dtx_mode(
               ( vs_vocamrwb_cmd_get_dtx_mode_t* ) params );
    }
    break;

  case VS_VOCEFR_CMD_SET_DTX_MODE:
    {
    rc = vs_vocefr_cmd_set_dtx_mode(
               ( vs_vocefr_cmd_set_dtx_mode_t* ) params );
    }
    break;

  case VS_VOCEFR_CMD_GET_DTX_MODE:
    {
    rc = vs_vocefr_cmd_get_dtx_mode(
               ( vs_vocefr_cmd_get_dtx_mode_t* ) params );
    }
    break;

  case VS_VOCFR_CMD_SET_DTX_MODE:
    {
    rc = vs_vocfr_cmd_set_dtx_mode(
              ( vs_vocfr_cmd_set_dtx_mode_t* ) params );
    }
    break;

  case VS_VOCFR_CMD_GET_DTX_MODE:
    {
    rc = vs_vocfr_cmd_get_dtx_mode(
              ( vs_vocfr_cmd_get_dtx_mode_t* ) params );
    }
    break;

  case VS_VOCHR_CMD_SET_DTX_MODE:
    {
    rc = vs_vochr_cmd_set_dtx_mode(
              ( vs_vochr_cmd_set_dtx_mode_t* ) params );
    }
    break;

  case VS_VOCHR_CMD_GET_DTX_MODE:
    {
    rc = vs_vochr_cmd_get_dtx_mode(
              ( vs_vochr_cmd_get_dtx_mode_t* ) params );
    }
    break;

  default:
    {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,  "Unsupported callindex (%x)",
            cmd_id ); /* index vs cmd_id - TBD */
    rc = VS_EUNSUPPORTED;
    }
    break;
  }  /* end of switch ( cmd_id )*/

  return rc;

}  /* end of vs_call () */
