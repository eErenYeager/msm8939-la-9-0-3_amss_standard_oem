#ifndef __VS_I_H__
#define __VS_I_H__

/**
  @file  vs_i.h
  @brief This file contains internal definitions of VS  
*/

/*
  ============================================================================

   Copyright (C) 2013-2015 Qualcomm Technologies, Inc.
   All Rights Reserved.
   Confidential and Proprietary - Qualcomm Technologies, Inc.

  ============================================================================

                             Edit History

  $Header: //source/qcom/qct/multimedia2/api/audio/2.x/vs/main/latest/inc/vs_i.h
  $Author: shrakuma $

  when      who   what, where, why
  --------  ---   ------------------------------------------------------------
  05/26/14  aim   Re-aligned timing offsets which are close to VFR 
	              to account for the jitter.
  02/10/14  aim   Modified thread priority.
  08/09/13  sud   Renamed macro PC_ENV_SET to WINSIM.
  07/30/13  sud   Added a macro VS_REPORT_FATAL_ON_ERROR to report MSG_FATAL
                  when fatal error conditions occurs in client context.
  06/26/13  sud   Moved some of the function declarations from vs.c and 
                  vs_voc.c
  06/16/13  sud   Removed some of the function declarations.
  06/13/13  hl    Added support for handling ADSP SSR.
  06/12/13  sud   VS refactored code and fixed bugs.
  04/16/13  sud   VS refactored code and fixed bugs. 
  03/14/13  sud   Initial revision

  ============================================================================
*/

/****************************************************************************
  Include files for Module
****************************************************************************/
#include <stddef.h>
#include <string.h>
#include "err.h"
#include "mmdefs.h"
#include "vs.h"
#include "aprv2_packet.h"
#include "apr_lock.h"
#include "apr_memmgr.h"
#include "apr_thread.h"
#include "apr_event.h"
#include "apr_objmgr.h"
#include "apr_memmgr.h"
#include "apr_list.h"
#include "apr_misc.h"
#include "aprv2_api_inline.h"

/****************************************************************************
  VS DEFINES
****************************************************************************/

#ifdef WINSIM
#include "testclient_vs.h"
#endif

/* Defined 10K for now. Expected it to go up to 25 K in future  <TBD>*/
#define VS_HEAP_SIZE_V ( 10000 )

/* Size of VS command queue. */
#define VS_NUM_COMMANDS_V ( 40 )

/* Size of APR packet queue. */
#define VS_NUM_APR_PKTS_V ( 40 )

#define VS_HANDLE_TOTAL_BITS_V ( 16 )
#define VS_HANDLE_INDEX_BITS_V ( 7 ) /**< 7 bits = 128 handles. */
#define VS_MAX_OBJECTS_V ( 1 << VS_HANDLE_INDEX_BITS_V )
#define VS_MAX_GENERIC_OBJECTS_PER_SESSION_V (32)  

/* This value is decided considering that 10 session/client per voice
 * system. 10 * 2 voice system = 20
 */
#define VS_MAX_NUM_SESSIONS_V ( 20 ) 
/* This value is bounded by CVS max session name size (31) */
#define VS_MAX_CVD_SESSION_NAME_SIZE_V ( 31 ) 

/* Definitions for error checking */
#define VS_COMMON_MEDIA_ID_UNDEFINED_V (0xFFFFFFFF)
#define VS_VSID_UNDEFINED_V (0xFFFFFFFF)

#define VS_IST_NAME ( "VS_IST" )
#define VS_IST_PRIORITY ( 255 - 171 ) 

/* This should be a GUID. Change it. <TBD> */
#define VS_CMD_PACKET_TYPE_V ( 0xCCCCCCCC )
#define VS_APR_PACKET_TYPE_V ( 0xAAAAAAAA )

/* Length of EFR/FR/HR packet core speech data in bytes */
#define VS_VOCEFR_MAX_PACKET_DATA_LENGTH ( 31 )
#define VS_VOCFR_MAX_PACKET_DATA_LENGTH ( 33 )
#define VS_VOCHR_MAX_PACKET_DATA_LENGTH ( 14 )

/* Maximum vocoder packet length - This number is decided based on the
 * vocoders supported by VS. As of today 4/2013, the biggest of all
 * the vocoder supported is AMR-WB. Size of AMR-WB is 61 + 1 byte header.
 */
#define VS_VOC_MAX_PACKET_LENGTH_V ( 62 )

// Length of voice packet header in bytes
#define VS_VOC_PACKET_HDR_LENGTH_V ( 1 )

/* Definitions for error checking */
#define VS_VOC_CLIENT_UNDEFINED_V (0xFFFFFFFF)
#define VS_VOC_CMD_UNDEFINED_V (0xFFFFFFFF)

#define VS_VOCAMR_CODEC_MODE_UNDEFINED_V  (0xFFFFFFFF)
#define VS_VOCAMR_CODEC_MODE_NONE_V VS_VOCAMR_CODEC_MODE_UNDEFINED_V

#define VS_VOCAMRWB_CODEC_MODE_UNDEFINED_V  (0xFFFFFFFF)
#define VS_VOCAMRWB_CODEC_MODE_NONE_V VS_VOCAMRWB_CODEC_MODE_UNDEFINED_V

#define VS_VOC_DTX_MODE_DISABLE_V (0x0)
#define VS_VOC_DTX_MODE_ENABLE_V (0x1)
#define VS_VOC_DTX_MODE_UNDEFINED_V (0x2)

/* Default timing parameters. Applicable to any network that doesn't have 
 * its own timing parameters. 
 */
#define VS_VOC_TIMING_ENC_OFFSET_V ( 8000 )
  /**< DSP sends encoder packet 8 msec afer VFR. */
#define VS_VOC_TIMING_DEC_REQ_OFFSET_V ( 3300 )
  /**< DSP sends decoder request 3.3 msec afer VFR. */
#define VS_VOC_TIMING_DEC_OFFSET_V ( 8300 )
  /**< DSP deadline to receive decoder packet - 8.3 msec afer VFR. */

/* GSM specific timing parameters. */
#define VS_VOC_TIMING_GSM_ENC_OFFSET_V ( 10000 )
  /**< DSP sends encoder packet 10 msec afer VFR. */
#define VS_VOC_TIMING_GSM_DEC_REQ_OFFSET_V ( 1000 )
  /**< DSP sends decoder request 1 msec afer VFR. */
#define VS_VOC_TIMING_GSM_DEC_OFFSET_V ( 4300 )
  /**< DSP deadline to receive decoder packet - 4.3 msec afer VFR. */

/* Vocode frame synchronization modes */
#define VS_VOC_VFR_MODE_HARD_V (1)  /* Hard VFR (20 ms VFR interrupt) */
#define VS_VOC_VFR_MODE_SOFT_V (0)  /* No VFR */


#define VS_VOC_BUFFER_VALID_SPEECH_FRAME ( 1 ) /* Valid encoder speech frame */
#define VS_VOC_BUFFER_NO_SPEECH_FRAME_V ( 0 ) /* NO encoder speech frame */
#define VS_VOC_BUFFER_MINOR_VERSION_V ( 0x0001 ) /* minor versoion - 0001 */
#define VS_VOC_BUFFER_TIMESTAMP_NOT_SUPPORTED_V ( 0 ) /* timestamp not supported */

/* Command to request VS to do cleanup due to ADSP SSR. */
#define VS_VOC_CMD_CLEANUP_FOR_ADSP_SSR ( 0x00012ED9 )

/* Command to request VS to poll CVD (MVM) for its readness. */
#define VS_VOC_CMD_POLL_CVD ( 0x00012EDA )

#define VS_PANIC_ON_ERROR( rc ) \
  { if ( rc ) { ERR_FATAL( "Error[0x%08x], vs_state=%d", \
                            rc, vs_is_initialized, 0 ); } }

#define VS_REPORT_FATAL_ON_ERROR( rc ) \
  { if ( rc ) { MSG_2( MSG_SSID_DFLT, MSG_LEGACY_FATAL,\
                      "Error[0x%08x], vs_state=%d", \
                            rc, vs_is_initialized ); } }

/* flag that indicates the state of vs task. */
extern bool_t vs_is_initialized;

/****************************************************************************
                    D A T A   D E F I N I T I O N S
****************************************************************************/
/* The following macros are here for temporary purposes.
 * Once the voicecfg is preset, we do not need this. Instead 
 */

typedef enum vs_vsid_enum_t
{
  VS_VSID_CS_VOICE_MULTIMODE = 0x10C01000,
  VS_VSID_CS_VOICE_GSM2 = 0x10DC1000
}
  vs_vsid_enum_t;


/****************************************************************************
  VS_VOC packet status type
****************************************************************************/
typedef enum vs_voc_pkt_status_enum_t
{
  VS_VOC_PKT_STATUS_NORMAL,
  VS_VOC_PKT_STATUS_FAST,    /* packets were supplied too fast */
  VS_VOC_PKT_STATUS_SLOW     /* packets were supplied too slowly */
} 
  vs_voc_pkt_status_enum_t;

/****************************************************************************
  VS_VOC state machine STATES 
****************************************************************************/
typedef enum vs_voc_state_enum_t
{
  VS_VOC_STATE_ENUM_RESET,
  VS_VOC_STATE_ENUM_IDLE,
  VS_VOC_STATE_ENUM_RUN
} 
  vs_voc_state_enum_t;

/****************************************************************************
  VS vocoder properties definitions 
****************************************************************************/
/* AMR vocoder property structure. */
typedef struct vs_vocamr_property_t vs_vocamr_property_t;
struct vs_vocamr_property_t
{
  uint32_t dtx_enable; 
      /**< Toggle DTX on or off. Possible values:\n
           - VS_VOC_DTX_MODE_DISABLE_V -- Disables DTX \n
           - VS_VOC_DTX_MODE_ENABLE_V -- Enables DTX \n 
           - VS_VOC_DTX_MODE_UNDEFINED_V -- Not configured */ 
  uint32_t codec_mode;
};

/* AMR-WB vocoder property structure. */
typedef struct vs_vocamrwb_property_t vs_vocamrwb_property_t;
struct vs_vocamrwb_property_t
{
  uint32_t dtx_enable; 
  /**< Toggle DTX on or off. Possible values:\n
       - VS_VOC_DTX_MODE_DISABLE_V -- Disables DTX \n
       - VS_VOC_DTX_MODE_ENABLE_V -- Enables DTX \n 
       - VS_VOC_DTX_MODE_UNDEFINED_V -- Not configured */ 
  uint32_t codec_mode;
};

/* EFR vocoder property structure. */
typedef struct vs_vocefr_property_t vs_vocefr_property_t;
struct vs_vocefr_property_t
{
  uint32_t dtx_enable; 
  /**< Toggle DTX on or off. Possible values:\n
       - VS_VOC_DTX_MODE_DISABLE_V -- Disables DTX \n
       - VS_VOC_DTX_MODE_ENABLE_V -- Enables DTX \n 
       - VS_VOC_DTX_MODE_UNDEFINED_V -- Not configured */ 
};

/* FR vocoder property structure. */
typedef struct vs_vocfr_property_t vs_vocfr_property_t;
struct vs_vocfr_property_t
{
  uint32_t dtx_enable; 
  /**< Toggle DTX on or off. Possible values:\n
       - VS_VOC_DTX_MODE_DISABLE_V -- Disables DTX \n
       - VS_VOC_DTX_MODE_ENABLE_V -- Enables DTX \n 
       - VS_VOC_DTX_MODE_UNDEFINED_V -- Not configured */ 
};

/* HR vocoder property structure. */
typedef struct vs_vochr_property_t vs_vochr_property_t;
struct vs_vochr_property_t
{
  uint32_t dtx_enable; 
  /**< Toggle DTX on or off. Possible values:\n
       - VS_VOC_DTX_MODE_DISABLE_V -- Disables DTX \n
       - VS_VOC_DTX_MODE_ENABLE_V -- Enables DTX \n 
       - VS_VOC_DTX_MODE_UNDEFINED_V -- Not configured */ 
};

typedef struct vs_voc_property_t vs_voc_property_t;
struct vs_voc_property_t
{
  vs_vocamr_property_t    amr;
  vs_vocamrwb_property_t  amrwb ;
  vs_vocefr_property_t    efr;
  vs_vocfr_property_t     fr;
  vs_vochr_property_t     hr;
};
/****************************************************************************
  VS vocoder properties definitions...END
****************************************************************************/


typedef struct vs_cmd_packet_t vs_cmd_packet_t;
struct vs_cmd_packet_t
{
  uint32_t cmd_id; /**< Command id issued from client. */
  void* params; /**< Structure associated to each cmd_id. */
};

/* This identifies the state of a client allocated buffer at an
 * instant of time.
 */
typedef enum vs_voc_buffer_type_t
{
  VS_VOC_BUFFER_TYPE_NONE,
  VS_VOC_BUFFER_TYPE_READ,
  VS_VOC_BUFFER_TYPE_WRITE,
  VS_VOC_BUFFER_TYPE_PRIMED
}
  vs_voc_buffer_type_t;

/* The generic item is used in VS to track read and write buffers and
 * free them at appropriate time.
 */
typedef struct vs_voc_buffer_item_t vs_voc_buffer_item_t;
struct vs_voc_buffer_item_t
{
  apr_list_node_t link;
  vs_voc_buffer_type_t buf_type;
  vs_voc_buffer_t* buf;
};

/****************************************************************************
  VS thread states
****************************************************************************/
typedef enum vs_thread_state_enum_t
{
  VS_THREAD_STATE_ENUM_INIT,
  VS_THREAD_STATE_ENUM_READY,
  VS_THREAD_STATE_ENUM_EXIT
}
  vs_thread_state_enum_t;

/****************************************************************************
  VS WORK QUEUE DEFINITIONS
****************************************************************************/
typedef struct vs_cmd_item_t vs_cmd_item_t;
struct vs_cmd_item_t 
{
  apr_list_node_t link;
  vs_cmd_packet_t* packet;
};

typedef struct vs_work_item_t vs_work_item_t;
struct vs_work_item_t 
{
  apr_list_node_t link;
  aprv2_packet_t* packet;
};

/****************************************************************************
   VS OBJECT DEFINITIONS
****************************************************************************/
typedef struct vs_object_header_t vs_object_header_t;
typedef struct vs_token_object_t vs_token_object_t;
typedef struct vs_simple_job_object_t vs_simple_job_object_t;
typedef struct vs_sequencer_job_object_t vs_sequencer_job_object_t;
typedef struct vs_session_object_t vs_session_object_t;
typedef union vs_object_t vs_object_t;
typedef struct vs_generic_item_t vs_generic_item_t;

/****************************************************************************
  COMMAND RESPONSE FUNCTION TABLE
****************************************************************************/
typedef void ( *vs_event_handler_fn_t ) ( aprv2_packet_t* packet );

typedef enum vs_response_fn_enum_t
{
  VS_RESPONSE_FN_ENUM_ACCEPTED,
  VS_RESPONSE_FN_ENUM_RESULT,
  VS_RESPONSE_FN_ENUM_INVALID,
  VS_RESPONSE_FN_ENUM_MAX = VS_RESPONSE_FN_ENUM_INVALID
}
  vs_response_fn_enum_t;

/*
 * Pending commands may load different sets of response and event handlers to
 * complete each job. The response function table is equivalent to the state
 * design pattern. The state context is stored in the gating command control.
 * Pending commands can be as simple or as complex as required.
 */
typedef vs_event_handler_fn_t vs_response_fn_table_t[ VS_RESPONSE_FN_ENUM_MAX ];

typedef enum vs_gating_cmd_state_enum_t
{
  VS_GATING_CMD_STATE_ENUM_FETCH,
  VS_GATING_CMD_STATE_ENUM_EXECUTE,
  VS_GATING_CMD_STATE_ENUM_CONTINUE
}
  vs_gating_cmd_state_enum_t;
   
typedef struct vs_gating_control_t vs_gating_control_t;
struct vs_gating_control_t
{
  apr_list_t cmd_q;
  /**< The gating (vs_work_item_t) command queue. */
  vs_gating_cmd_state_enum_t state;
  /**<
   * The current state of the gating command control.
   *
   * This variable is managed by the gating command processor. The
   * individual gating command controls indicates to the gating command
   * processor to complete or to delay the completion of the current
   * gating command.
   */
  vs_cmd_packet_t* packet;
  /**<
   * The current (command) packet being processed.
   */
  vs_object_t* rootjob_obj;
  /**<
   * The rootjob_obj is a temporary storage for the current gating
   * command.
   */
};

/****************************************************************************
   THE COMMON OBJECT DEFINITIONS                                           
****************************************************************************/
typedef enum vs_object_type_enum_t
{
   VS_OBJECT_TYPE_ENUM_UNINITIALIZED,
   VS_OBJECT_TYPE_ENUM_HANDLE,
   VS_OBJECT_TYPE_ENUM_SIMPLE_JOB,
   VS_OBJECT_TYPE_ENUM_SEQUENCER_JOB,
   VS_OBJECT_TYPE_ENUM_SESSION,
   VS_OBJECT_TYPE_ENUM_INVALID
}
  vs_object_type_enum_t;

struct vs_object_header_t
{
  uint32_t handle;
   /**< The handle to the associated apr_objmgr_object_t instance. */
  vs_object_type_enum_t type;
   /**<
    * The object type defines the actual derived object.
    *
    * The derived object can be any custom object type. A session or a
    * command are two such custom object types. A free object entry is set
    * to VS_OBJECT_TYPE_ENUM_FREE.
    */
};

struct vs_generic_item_t  {
  apr_list_node_t link;

  uint32_t handle;
   /**< Any custom handle. */
};

typedef struct vs_voc_voice_timing_t vs_voc_voice_timing_t;
struct vs_voc_voice_timing_t
{           
  uint16_t vfr_mode;
  /**< Vocoder frame synchronization mode. Possible values:\n 
        - 0 -- No frame synchronization. \n
        - 1 -- Hard Vocoder Frame Reference (VFR). A 20 ms VFR interrupt. */
  uint32_t vsid;
  /**< Voice System ID as defined by DCN 80-NF711-1 */
  uint16_t enc_offset;
  /**< Offset in microseconds from the VFR to deliver a Tx vocoder
      packet. The offset is to be less than 20000 us. */
  uint16_t dec_req_offset;
  /**< The offset in microseconds from the VFR to request for an Rx vocoder
        packet. The offset is to be less than 20000 us. */
  uint16_t dec_offset;
  /**< Offset in microseconds from the VFR to indicate the deadline to
        receive an Rx vocoder packet. The offset is to be less than 20000 us.
        Rx vocoder packets received after this deadline are not guaranteed to
        be processed.  */
};

/****************************************************************************
  THE HANDLE OBJECT                                                       
****************************************************************************/
struct vs_token_object_t
{
  vs_object_header_t header;
  vs_response_fn_table_t fn_table;
   /**<
    * This is the response function v-table. The response table can store
    * custom response routines for all possible responses directed to this
    * specific job.
    */
};

/****************************************************************************
  THE SIMPLE JOB OBJECT                                                   
****************************************************************************/
struct vs_simple_job_object_t
{
  vs_object_header_t header;
  uint32_t context_handle;
   /**<
    * The parent-job handle indicates this sub-job is part of a batch-job.
    *
    * Set this value to -1 when it is unused or when there is no parent.
    */
  vs_response_fn_table_t fn_table;
   /**<
    * This is the response function v-table. The response table can store
    * custom response routines for all possible responses directed to this
    * specific job.
    */
  bool_t is_accepted;
   /**< The command accepted response flag. 0 is false and 1 is true. */
  bool_t is_completed;
   /**< The command completed response flag. 0 is false and 1 is true. */
  uint32_t status;
   /**< The status returned by the command completion. */
};

/****************************************************************************
  THE SEQUENCER JOB OBJECT                                                
****************************************************************************/
struct vs_sequencer_job_object_t
{
  vs_object_header_t header;

  uint32_t state;
   /**< The generic state variable. */
  uint32_t helper_state; 
  /**< Helper function state variable. */
  union vs_object_t* subjob_obj;
   /**< The current sub-job object. */
  uint32_t status;
   /**< A status value. */
};

typedef struct vs_control_t vs_control_t;
struct vs_control_t
{
  uint32_t transition_job_handle;
  uint32_t pendjob_handle;
  vs_sequencer_job_object_t* pendjob_seq_ptr;
  uint32_t status;
};
/****************************************************************************
  THE SESSION OBJECT                                                
****************************************************************************/
struct vs_session_object_t
{
  vs_object_header_t header;

  uint32_t vsid;

  uint32_t client_id;

  uint32_t media_id;  

  vs_common_event_callback_fn_t event_cb;  

  void* session_context;

  uint32_t pending_cmd;

  bool_t is_configured;

  apr_list_t alloc_buf_q;

  apr_list_t read_buf_q;

  apr_list_t write_buf_q;

  char_t cvd_session_name[ VS_MAX_CVD_SESSION_NAME_SIZE_V ];  

  vs_voc_state_enum_t state;  

  vs_voc_voice_timing_t voice_timing;

  vs_voc_property_t current_voc;

  vs_voc_property_t pending_voc;

  vs_control_t session_ctrl;

  bool_t stream_ready;

  bool_t stream_standby;

  bool_t is_first_call;
  /* CVS port is set at CVS session creation. */
  uint16_t cvs_port;
  /* MVM port is set at MVM session creation. */
  uint16_t mvm_port;

};

/****************************************************************************
  THE GENERIC CVD OBJECT                                                  
****************************************************************************/
union vs_object_t
{
  vs_object_header_t header;
  vs_token_object_t handle;
  vs_simple_job_object_t simple_job;
  vs_sequencer_job_object_t sequencer_job;
  vs_session_object_t session;
};

VS_INTERNAL uint32_t vs_queue_incoming_vs_command_packet (
  vs_cmd_packet_t* p_packet
);

VS_INTERNAL uint32_t vs_get_typed_object (
  uint32_t handle,
  vs_object_type_enum_t type,
  vs_object_t** ret_obj
);

VS_INTERNAL void vs_int_lock_fn (
  void
);

VS_INTERNAL void vs_int_unlock_fn (
  void
);

VS_INTERNAL void vs_signal_run (
  void
);

VS_INTERNAL uint32_t vs_common_send_event ( 
  uint32_t handle, 
  vs_cmd_packet_t* cmd, 
  uint32_t event_id,
  uint32_t status 
);

VS_INTERNAL uint32_t vs_get_object (
  uint32_t handle,
  vs_object_t** ret_obj
);


/****************************************************************************
    SYNCHRONOUS BLOCKING API DEFINITIONS
****************************************************************************/
VS_INTERNAL uint32_t vs_voc_cmd_open (
  vs_voc_cmd_open_t* params
);

VS_INTERNAL uint32_t vs_voc_cmd_alloc_buffer (
  vs_voc_cmd_alloc_buffer_t* params
);

VS_INTERNAL uint32_t vs_voc_cmd_free_buffer (
  vs_voc_cmd_free_buffer_t* params
);

/****************************************************************************
    SYNCHRONOUS NON-BLOCKING API DEFINITIONS
****************************************************************************/
VS_INTERNAL uint32_t vs_voc_cmd_prime_read_buffer (
  vs_voc_cmd_prime_read_buffer_t* params
);

VS_INTERNAL uint32_t vs_voc_cmd_read_buffer (
  vs_voc_cmd_read_buffer_t* params
);

VS_INTERNAL uint32_t vs_voc_cmd_write_buffer (
  vs_voc_cmd_write_buffer_t* params
);


/****************************************************************************
    ASYNCHRONOUS NON-BLOCKING API DEFINITIONS
****************************************************************************/
VS_INTERNAL uint32_t vs_voc_cmd_enable (
  vs_voc_cmd_enable_t* params
);

VS_INTERNAL uint32_t vs_voc_cmd_disable (
  vs_voc_cmd_disable_t* params
);

VS_INTERNAL uint32_t vs_voc_cmd_standby (
  vs_voc_cmd_standby_t* params
);

VS_INTERNAL uint32_t vs_voc_cmd_flush_buffers (
  vs_voc_cmd_flush_buffers_t* params
);

VS_INTERNAL uint32_t vs_voc_cmd_close (
  vs_voc_cmd_close_t* params
);


/****************************************************************************
 * VOCODER PROPERTY FUNCTIONS                                      *
 ****************************************************************************/
/* AMR */
VS_INTERNAL uint32_t vs_vocamr_cmd_set_codec_mode (
  vs_vocamr_cmd_set_codec_mode_t* params
);

VS_INTERNAL uint32_t vs_vocamr_cmd_set_codec_mode_proc (
  uint32_t handle,
  vs_vocamr_codec_mode_t codec_mode
);

VS_INTERNAL uint32_t vs_vocamr_cmd_get_codec_mode (
  vs_vocamr_cmd_get_codec_mode_t* params
);

VS_INTERNAL uint32_t vs_vocamr_cmd_set_dtx_mode (
  vs_vocamr_cmd_set_dtx_mode_t* params
);

VS_INTERNAL uint32_t vs_vocamr_cmd_set_dtx_mode_proc (
  uint32_t handle,
  bool_t dtx_enable
);

VS_INTERNAL uint32_t vs_vocamr_cmd_get_dtx_mode (
  vs_vocamr_cmd_get_dtx_mode_t* params
);

VS_INTERNAL uint32_t vs_vocamr_set_cached_voc_property (
  uint32_t handle
);

/* AMR-WB */
VS_INTERNAL uint32_t vs_vocamrwb_cmd_set_codec_mode (
  vs_vocamrwb_cmd_set_codec_mode_t* params
);

VS_INTERNAL uint32_t vs_vocamrwb_cmd_set_codec_mode_proc (
  uint32_t handle,
  vs_vocamrwb_codec_mode_t codec_mode
);

VS_INTERNAL uint32_t vs_vocamrwb_cmd_set_dtx_mode (
  vs_vocamrwb_cmd_set_dtx_mode_t* params
);

VS_INTERNAL uint32_t vs_vocamrwb_cmd_set_dtx_mode_proc (
  uint32_t handle,
  bool_t dtx_enable
);

VS_INTERNAL uint32_t vs_vocamrwb_cmd_get_codec_mode (
  vs_vocamrwb_cmd_get_codec_mode_t* params
);

VS_INTERNAL uint32_t vs_vocamrwb_cmd_get_dtx_mode (
  vs_vocamrwb_cmd_get_dtx_mode_t* params
);

VS_INTERNAL uint32_t vs_vocamrwb_set_cached_voc_property (
  uint32_t handle
);

/* EFR */
VS_INTERNAL uint32_t vs_vocefr_cmd_set_dtx_mode (
  vs_vocefr_cmd_set_dtx_mode_t* params
);

VS_INTERNAL uint32_t vs_vocefr_cmd_set_dtx_mode_proc (
  uint32_t handle,
  bool_t dtx_enable
);

VS_INTERNAL uint32_t vs_vocefr_cmd_get_dtx_mode (
  vs_vocefr_cmd_get_dtx_mode_t* params
);

VS_INTERNAL uint32_t vs_vocefr_set_cached_voc_property (
  uint32_t handle
);

/* FR */
VS_INTERNAL uint32_t vs_vocfr_cmd_set_dtx_mode (
  vs_vocfr_cmd_set_dtx_mode_t* params
);

VS_INTERNAL uint32_t vs_vocfr_cmd_set_dtx_mode_proc (
  uint32_t handle,
  bool_t dtx_enable
);

VS_INTERNAL uint32_t vs_vocfr_cmd_get_dtx_mode (
  vs_vocfr_cmd_get_dtx_mode_t* params
);

VS_INTERNAL uint32_t vs_vocfr_set_cached_voc_property (
  uint32_t handle
);

/* HR */
VS_INTERNAL uint32_t vs_vochr_cmd_set_dtx_mode (
  vs_vochr_cmd_set_dtx_mode_t* params
);

VS_INTERNAL uint32_t vs_vochr_cmd_set_dtx_mode_proc (
  uint32_t handle,
  bool_t dtx_enable
);

VS_INTERNAL uint32_t vs_vochr_cmd_get_dtx_mode (
  vs_vochr_cmd_get_dtx_mode_t* params 
);

VS_INTERNAL uint32_t vs_vochr_set_cached_voc_property (
  uint32_t handle 
);

/****************************************************************************
 * GATING COMMAND PROCESSING FUNCTIONS                                      *
 ****************************************************************************/
VS_INTERNAL uint32_t vs_voc_cmd_enable_proc ( 
  vs_gating_control_t* ctrl 
);

VS_INTERNAL uint32_t vs_voc_cmd_disable_proc (
  vs_gating_control_t* ctrl
);

VS_INTERNAL uint32_t vs_voc_cmd_standby_proc (
  vs_gating_control_t* ctrl
);

VS_INTERNAL uint32_t vs_voc_cmd_close_proc (
  vs_gating_control_t* ctrl
);

VS_INTERNAL uint32_t vs_voc_cmd_flush_buffers_proc (
  vs_gating_control_t* ctrl
);


/****************************************************************************
 * VS STATE MACHINE FUNCTIONS                                               *
 ****************************************************************************/
VS_INTERNAL uint32_t vs_voc_run_state_control (
  void
);

VS_INTERNAL uint32_t vs_voc_state_control (
  vs_session_object_t* session_obj
);

/****************************************************************************
 * VS SSR FUNCTIONS                                                         *
 ****************************************************************************/

VS_INTERNAL uint32_t vs_voc_ssr_init (
  void
);

VS_INTERNAL uint32_t vs_voc_ssr_deinit (
  void
);

VS_INTERNAL uint32_t vs_voc_cmd_cleanup_for_adsp_ssr_proc (
  vs_gating_control_t* ctrl
);

VS_INTERNAL uint32_t vs_voc_cmd_poll_cvd_proc (
  vs_gating_control_t* ctrl
);

/****************************************************************************
 * OTHER FUNCTIONS                                                          *
 ****************************************************************************/
/* this function gets decoder packet from modem and adds packet header */
VS_INTERNAL uint32_t vs_voc_send_dec_packet (
  uint32_t session_handle,
  vs_voc_buffer_t* p_wbuffer,  
  aprv2_packet_t* p_packet
);

/**
 * This function will determine number of bytes of AMR vocoder frame length
 * based on the frame type and frame rate.
 *
 * returns "number of bytes" when successful. 
 */
VS_INTERNAL uint32_t vs_vocamr_get_frame_len (
  vs_vocamr_frame_type_t frame_type,
  vs_vocamr_codec_mode_t amr_mode
);

/**
 * This function will determine number of bytes of AMR-WB vocoder frame length
 * based on the frame type and frame rate.
 *
 * returns "number of bytes" when successful. 
 */
VS_INTERNAL uint32_t vs_vocamrwb_get_frame_len (
  vs_vocamrwb_frame_type_t frame_type,
  vs_vocamrwb_codec_mode_t amrwb_mode
);


VS_INTERNAL uint32_t vs_create_autofree_token_object (
  vs_token_object_t** p_ret_obj
);

VS_INTERNAL uint32_t vs_create_simple_job_object (
  uint32_t context_handle,
  vs_simple_job_object_t** p_ret_job_obj
);

VS_INTERNAL uint32_t vs_create_sequencer_job_object (
  vs_sequencer_job_object_t** p_ret_job_obj
);

VS_INTERNAL void vs_simple_result_rsp_fn (
  aprv2_packet_t* p_packet
);

VS_INTERNAL void vs_simple_transition_result_rsp_fn (
  aprv2_packet_t* p_packet
);

VS_INTERNAL uint32_t vs_mem_alloc_object (
  uint32_t size,
  vs_object_t** p_ret_object
);

VS_INTERNAL uint32_t vs_mem_free_object (
  vs_object_t* p_object
);

/* Track the allocated VS objects with type VS_OBJECT_TYPE_ENUM_SIMPLE_JOB and
 * VS_OBJECT_TYPE_ENUM_HANDLE, in order to free them in case of ADSP SSR. VS
 * allocates job object with one of the above types when communicating with the
 * ADSP and free the object once a response is received from the ADSP. If VS 
 * has issued commands to the ADSP and ADSP SSR occurs, VS will not get 
 * responses from the ADSP and thus the objects will not be freed. Therefore
 * VS need to track these types of allocated objects and free them upon ADSP 
 * SSR.
 */
VS_INTERNAL uint32_t vs_voc_ssr_track_object (
  uint32_t obj_handle
);

/* Untrack the VS objects with type VS_OBJECT_TYPE_ENUM_SIMPLE_JOB and
 * VS_OBJECT_TYPE_ENUM_HANDLE.
 */
VS_INTERNAL uint32_t vs_voc_ssr_untrack_object (
  uint32_t obj_handle
);

/* Free all of the tracked VS objects. */
VS_INTERNAL uint32_t vs_voc_ssr_free_all_tracked_objects ( 
  void
);

/* Apply cached properties */
VS_INTERNAL uint32_t vs_voc_action_set_cached_stream_properties ( 
  vs_session_object_t* session_obj
);

#endif  /* __VS_I_H__ */

