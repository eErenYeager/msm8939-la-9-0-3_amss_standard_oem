/*
   Copyright (C) 2014 QUALCOMM Technologies, Inc.
   All Rights Reserved.
   Qualcomm Technologies, Inc. Confidential and Proprietary.

   $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/avs/vsd/voiceapp/src/voiceapp.c#1 $
   $Author: mplp4svc $
*/

/****************************************************************************
 * INCLUDE HEADER FILES                                                     *
 ****************************************************************************/

#include "msg.h"
#include "mmstd.h"
#include "apr_comdef.h"
#include "apr_errcodes.h"
#include "apr_lock.h"
#include "apr_memmgr.h"
#include "apr_objmgr.h"
#include "apr_timer.h"
#include "voiceapp.h"

/*****************************************************************************
 * DEFINES                                                                   *
 *****************************************************************************/

#define VOICEAPP_AMR_MAX_NUM_CODEC_MODE_V ( 8 )
#define VOICEAPP_AMRWB_MAX_NUM_CODEC_MODE_V ( 9 )

/* Stmr tick count at which WCDMA reads DSM queue */
#define VOICEAPP_WCDMA_READS_UL_PACKET_AT_STMR_TICK_COUNT_V ( 30 )

/* TDSCDMA picks UL packet from DSM queue at 1000 micro sec from VFR. 
 */
#define VOICEAPP_TDSCDMA_READS_UL_PKT_FRM_DSM_AT_TIME_V ( 1000 )

/* Buffer time needed to put vocoder packet in DSM Queue.
 * Time in micro second. 
 */
#define VOICEAPP_BUF_TIME_FOR_PUTTING_UL_PACKET_IN_DSM_V ( 1000 )

#define VOICEAPP_DFLT_TIMER_OFFSET_V ( 0 )

/* The frame size in microseconds that each voice processing threads 
 * (vptx, vprx, encoder, decoder, decoder pp) operates on.
 */
#define VOICEAPP_VOICE_FRAME_SIZE_US_V ( 20000 ) 

/****************************************************************************
 * GLOBALS                                                                  *
 ****************************************************************************/

/* Lock to be used by voiceapp  */
static apr_lock_t voiceapp_int_lock;

/* Lock to be used specifically by APR memmgr for maintenance of voiceapp Heap
 * and voiceapp Objects*/
static apr_lock_t voiceapp_objmgr_lock;
  
/* Heap manager and Heap Pool. Memory for all Voiceapp object would be allocated from 
 * voiceapp_heap_pool */
static apr_memmgr_type voiceapp_heapmgr;
static uint8_t voiceapp_heap_pool[ VOICEAPP_HEAP_SIZE_V ];

/* voiceapp object manager and voiceapp_object_table, all voiceapp objects handles would be 
 * stored in voiceapp_object_table.
 */
static apr_objmgr_object_t voiceapp_object_table[ VOICEAPP_MAX_OBJECTS_V ];
static apr_objmgr_t voiceapp_objmgr;

/* Homing frame is in if1 format. Frames have only AMR Core Frame( speech or comfort noise data ), which 
 * includes class A, class B and class C bits. Note we have to append header( frame type and codec rate ). 
 * to make its format similar to the packets received from VSM. 
 * Maximum number of class A, class B, class C bits in AMR will be 244 bits, which equals 31 bytes. 
 * However, there is padding of 3 bytes for frame, for legacy reasons. 
 * Referred docs: Specs : 3GPP TS 26.101. and "Vocoder media types" document - 80-N8828-1.
 */
 static uint8_t voiceapp_amr_if1_homing_frame[VOICEAPP_AMR_MAX_NUM_CODEC_MODE_V][VOICEAPP_AMR_FRAME_SIZE_V] = 
 {
   { 0xf8, 0x9d, 0x67, 0xf1, 0x16, 0x09, 0x35, 0xbd, 0xe1, 0x99, 0x68, 0x40, 0x00, 0x00, 0x00, 0x00, 
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
     0x00, 0x00 }, 
   { 0x1f, 0xb9, 0x67, 0xf7, 0xf1, 0xfd, 0xf5, 0x47, 0xbf, 0x2e, 0x61, 0xc0, 0x60, 0x00, 0x00, 0x00, 
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
     0x00, 0x00 }, 
   { 0xe9, 0x59, 0xf3, 0x5f, 0xdf, 0xe5, 0xe9, 0x66, 0x7f, 0xfb, 0xc0, 0x88, 0x81, 0x80, 0x88, 0x00, 
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
     0x00, 0x00 }, 
   { 0xf0, 0xb9, 0xe6, 0xbf, 0x68, 0x00, 0x72, 0x09, 0xc3, 0xd3, 0xcb, 0xc8, 0x08, 0x48, 0xa1, 0x55,
     0x8c, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
     0x00, 0x00 }, 
   { 0xf8, 0x71, 0xcf, 0x48, 0x80, 0x1e, 0xc4, 0x27, 0xf0, 0xfc, 0x3f, 0x73, 0x18, 0x89, 0x86, 0x22,
     0x06, 0x22, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
     0x00, 0x00 },
   { 0x42, 0xb3, 0x32, 0x08, 0x18, 0x13, 0xd7, 0xe9, 0x16, 0xe7, 0xaa, 0x5e, 0x80, 0xd7, 0xfd, 0xe8,
     0x12, 0xb8, 0xc0, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
     0x00, 0x00 },
   { 0x1f, 0xc7, 0x22, 0xc7, 0x88, 0x03, 0x28, 0xa9, 0xc2, 0x80, 0x03, 0x0b, 0xc9, 0x75, 0x5c, 0x3e,
     0xf5, 0x19, 0xf8, 0x00, 0x00, 0x29, 0x53, 0x23, 0xe0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
     0x00, 0x00 }, 
   { 0x08, 0x55, 0x6d, 0x94, 0x4c, 0x71, 0xa1, 0xa0, 0x81, 0xe7, 0xea, 0xd2, 0x04, 0x24, 0x44, 0x80,
     0x00, 0x0e, 0xcd, 0x82, 0xb8, 0x11, 0x18, 0x00, 0x00, 0x97, 0xc4, 0x79, 0x4e, 0x77, 0x40, 0x00,
     0x00, 0x00 }
 };
 
/* Homing frame is in if1 format. Frames have only AMRWB Core Frame( speech or comfort noise data ), which 
 * includes class A, class B and class C bits. Note we have to append header( frame type and codec rate ) 
 * to make its format similar to the packets received from VSM. 
 * Maximum number of class A, class B, class C bits in AMRWB will be 477 bits, which equals 60 bytes. 
 * Referred docs: Specs : 3GPP TS 26.201. and "Vocoder media types" document - 80-N8828-1 
 */
static uint8_t voiceapp_awb_if1_homing_frame[VOICEAPP_AMRWB_MAX_NUM_CODEC_MODE_V][VOICEAPP_AMRWB_FRAME_SIZE_V] = 
{
  { 0x00, 0x31, 0x00, 0x38, 0x9c, 0x10, 0x30, 0x01, 0xf2, 0x07, 0x22, 0xfa, 0x89, 0xeb, 0xdb, 0x8a,
    0xd0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }, 
  { 0x44, 0x00, 0x0f, 0x00, 0x0a, 0x55, 0xf1, 0x5d, 0x22, 0x0f, 0x94, 0xd7, 0x01, 0xfa, 0xa4, 0x85,
    0xa7, 0x44, 0x46, 0xc5, 0xe6, 0xe5, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }, 
  { 0x50, 0x46, 0x00, 0x77, 0xff, 0xde, 0x05, 0xf1, 0x5b, 0x67, 0x8f, 0x8c, 0xf7, 0x70, 0x07, 0xda,
    0x82, 0xca, 0xd1, 0x5a, 0x42, 0xde, 0x5a, 0xc0, 0x44, 0xee, 0xd3, 0x5a, 0xe6, 0x44, 0xd1, 0xd8,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }, 
  { 0x50, 0x46, 0x00, 0x33, 0xbe, 0xce, 0x05, 0xf1, 0x59, 0x63, 0x9f, 0x84, 0xc7, 0x50, 0x29, 0x13,
    0x91, 0x9a, 0x9a, 0x64, 0xf2, 0x3a, 0x0c, 0xd6, 0xa5, 0x85, 0x5e, 0xc5, 0x74, 0x44, 0xc5, 0x8d,
    0x5c, 0xc7, 0xed, 0x58, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }, 
  { 0x50, 0x46, 0x00, 0x33, 0xb6, 0xce, 0x45, 0xf1, 0x5b, 0xe7, 0x9f, 0x84, 0xf7, 0x53, 0x91, 0x99,
    0x31, 0x10, 0x29, 0x9e, 0x4c, 0x76, 0x0c, 0x59, 0xfc, 0x25, 0x42, 0x06, 0x0c, 0x6a, 0x86, 0x3d,
    0xf3, 0xdc, 0x04, 0x42, 0x46, 0x3d, 0xf1, 0x58, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }, 
  { 0x50, 0x46, 0x00, 0x22, 0xa6, 0x8a, 0x05, 0xf1, 0x59, 0x23, 0x0f, 0x84, 0xc7, 0x77, 0x9f, 0x36,
    0xc7, 0xf9, 0xe6, 0x82, 0x48, 0x89, 0xb9, 0x13, 0x83, 0xee, 0xcd, 0x22, 0x06, 0xd5, 0x64, 0x08,
    0xfe, 0x16, 0x46, 0x12, 0xbe, 0x01, 0x1e, 0xc0, 0x8a, 0xd5, 0xa4, 0x87, 0x27, 0x88, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }, 
  { 0x50, 0x46, 0x00, 0x22, 0xa6, 0x8a, 0x05, 0xf1, 0x59, 0xe3, 0x1f, 0x8c, 0xe7, 0x70, 0x99, 0xab,
    0xbf, 0x10, 0x6e, 0x38, 0x11, 0x7b, 0x69, 0x89, 0x42, 0x1b, 0x22, 0x71, 0x82, 0x41, 0xb9, 0x46,
    0x88, 0x62, 0xc0, 0xb8, 0xfb, 0xb5, 0x6a, 0x5b, 0x6a, 0xa2, 0x32, 0x83, 0x20, 0xa7, 0xb7, 0xb3,
    0x87, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }, 
  { 0x50, 0x46, 0x10, 0x33, 0xb6, 0xce, 0x55, 0xf1, 0x5b, 0xe3, 0x17, 0x84, 0xf7, 0x7b, 0xf9, 0x5e,
    0x65, 0x50, 0x75, 0x07, 0xec, 0x48, 0x01, 0x79, 0x92, 0x12, 0x1a, 0xd5, 0xb8, 0x2d, 0xd9, 0xf5,
    0x70, 0x95, 0xe0, 0x21, 0x40, 0xe3, 0x24, 0x58, 0x54, 0x22, 0x5b, 0x34, 0xa6, 0xbe, 0x22, 0x31,
    0x64, 0xd7, 0x9d, 0x9d, 0xfa, 0xcf, 0xac, 0x4b, 0x97, 0xe8, 0x00, 0x00 },
  { 0x50, 0x46, 0x00, 0x33, 0xba, 0xce, 0x25, 0xf1, 0x59, 0x20, 0x00, 0xa3, 0x1f, 0x84, 0xd7, 0x77,
    0xf3, 0x6a, 0x13, 0x13, 0xa0, 0x93, 0xe5, 0x0c, 0x29, 0x21, 0x72, 0xf1, 0xc3, 0x83, 0xec, 0x66,
    0x35, 0x89, 0x3e, 0x80, 0xe8, 0x89, 0x52, 0xc0, 0x25, 0xb5, 0x1c, 0x6d, 0xe6, 0x3c, 0x07, 0xbc,
    0x33, 0x55, 0xe9, 0xc0, 0x6d, 0x2c, 0xaf, 0xb3, 0xaf, 0x43, 0x31, 0xd0 }
};

/****************************************************************************
 * VOICEAPP CORE IMPLEMENTATION                                             *
 ****************************************************************************/

static void voiceapp_int_lock_fn ( void )
{
  ( void ) apr_lock_enter( voiceapp_int_lock );
}

static void voiceapp_int_unlock_fn ( void )
{
  ( void ) apr_lock_leave( voiceapp_int_lock );
}

static void voiceapp_objmgr_lock_fn ( void )
{
  ( void ) apr_lock_enter( voiceapp_objmgr_lock );
}

static void voiceapp_objmgr_unlock_fn ( void )
{
  ( void ) apr_lock_leave( voiceapp_objmgr_lock );
}

/* Called at Init sequence, create lock, set-up heap and object manager. 
 */
static uint32_t voiceapp_init( void )
{
  uint32_t rc = APR_EOK;
  apr_objmgr_setup_params_t params;

  { /* create lock */
    rc = apr_lock_create( APR_LOCK_TYPE_INTERRUPT, &voiceapp_int_lock );
    rc = apr_lock_create( APR_LOCK_TYPE_MUTEX, &voiceapp_objmgr_lock );
  } 
  { /* Initialize heap manager. */
    apr_memmgr_init_heap( &voiceapp_heapmgr, ( ( void* ) &voiceapp_heap_pool ),
                          sizeof( voiceapp_heap_pool ), NULL, NULL );
  }
  { /* Initialize the object manager. */
    params.table = voiceapp_object_table;
    params.total_bits = VOICEAPP_HANDLE_TOTAL_BITS_V;
    params.index_bits = VOICEAPP_HANDLE_INDEX_BITS_V;
    params.lock_fn = voiceapp_objmgr_lock_fn;
    params.unlock_fn = voiceapp_objmgr_unlock_fn;
    rc = apr_objmgr_construct( &voiceapp_objmgr, &params );    
  }
  
  return rc;
}

/* Called at Deinit sequence, destroy all locks and object manager 
 */
static uint32_t voiceapp_deinit ( void )
{
  uint32_t rc = APR_EOK;
  
  { /* Release the object management resources */
    rc = apr_objmgr_destruct( &voiceapp_objmgr );
  }
  { /* Destroy Locks */
    rc = apr_lock_destroy( voiceapp_int_lock ); 
    rc = apr_lock_destroy( voiceapp_objmgr_lock );
  }

  rc = APR_EOK;
  return rc;
}

/****************************************************************************
 * VOICEAPP CORE IMPLEMENTATION, memory management for voiceapp             *
 ****************************************************************************/

/* Allocates memory for voiceapp object. */
static uint32_t voiceapp_mem_alloc_object
(
  uint32_t size,
  voiceapp_object_t** ret_object
)
{
  uint32_t rc = APR_EOK;
  voiceapp_object_t* obj = NULL;
  apr_objmgr_object_t* objmgr_obj = NULL;

  for ( ;; )
  {
    if ( ret_object == NULL )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "voiceapp_mem_alloc_object(): ret_object is NULL" ); 
      rc = APR_EBADPARAM;
      break;
    }

    { /* Allocate memory for the Voice APP object. */
      obj = apr_memmgr_malloc( &voiceapp_heapmgr, size );
      if ( obj == NULL )
      {
        rc = APR_ENORESOURCE;
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "voiceapp_mem_alloc_object(): apr_memmgr_malloc failed, rc = (0x%08X)", rc );
        break;
      }

      /* Allocate a new handle for the Voice APP object. */
      rc = apr_objmgr_alloc_object( &voiceapp_objmgr, &objmgr_obj );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "voiceapp_mem_alloc_object(): apr_objmgr_alloc_object failed, rc = (0x%08X)", rc );
        apr_memmgr_free( &voiceapp_heapmgr, obj );
        break;
      }

      /* Link the Voice APP object to the handle. */
      objmgr_obj->any.ptr = obj;

      /* Initialize the base Voice APP object header. */
      obj->header.handle = objmgr_obj->handle;

      *ret_object = obj;
      break;
    }
  }

  return rc;
}

/* Free the memory for voiceapp object. */
static uint32_t voiceapp_mem_free_object
(
  voiceapp_object_t* object
)
{
  uint32_t rc = APR_EOK;

  for( ;; )
  {
    if ( object == NULL )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "voiceapp_mem_free_object(): object to be freed is NULL" ); 
      rc = APR_EBADPARAM;
      break;
    }

    /* Free the object memory and object handle. */
    rc = apr_objmgr_free_object( &voiceapp_objmgr, object->header.handle );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "voiceapp_mem_free_object(): failed, rc = (0x%08X)", rc ); 
      break;
    }

    apr_memmgr_free( &voiceapp_heapmgr, object );
    break;
  }

  return rc;
}  

/* Get the voiceapp object based on handle. */
static uint32_t voiceapp_get_object
(
  uint32_t handle,
  voiceapp_object_t** ret_obj
)
{
  uint32_t rc = APR_EOK;
  apr_objmgr_object_t* objmgr_obj = NULL;

  for ( ;; )
  {
    if ( ret_obj == NULL )
    {
      rc = APR_EBADPARAM;
      break;
    }

    rc = apr_objmgr_find_object( &voiceapp_objmgr, handle, &objmgr_obj );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "voiceapp_get_object(): could not find object with handle = (0x%08X), rc = (0x%08X)",
             handle, rc );
      break;
    }

    *ret_obj = ( ( voiceapp_object_t* )( objmgr_obj->any.ptr ) );
    break;
  }
  
  return rc;
}

/* Search voiceapp object based on vsid, traverse the whole
 * voiceapp_object_table for matching vsid.
 */
static uint32_t voiceapp_get_object_from_vsid
(
  uint32_t vsid,
  voiceapp_object_t** ret_obj
)
{
  uint32_t it = 0;
  voiceapp_object_t* voiceapp_object = NULL;
  
  for ( it = 0; it < VOICEAPP_MAX_OBJECTS_V; it++ )
  {
    voiceapp_object = ( ( voiceapp_object_t* )( voiceapp_object_table[it].any.ptr ) );
    if ( ( voiceapp_object != NULL ) && ( voiceapp_object->header.vsid == vsid ) )
    {
      *ret_obj = voiceapp_object;
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "voiceapp_get_object_from_vsid(): Found object with vsid = (0x%08X)", vsid );
      return APR_EOK;
    }
  }

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
         "voiceapp_get_object_from_vsid(): No object with vsid = (0x%08X)", vsid );
  return APR_EHANDLE;
}


/****************************************************************************
 * VOICEAPP CORE IMPLEMENTATION, interaction with ( W/TD ) clients           *
 ****************************************************************************/

/* A helper function, to trigger timers of voiceapp object */
static void voiceapp_helper_timing_reference_fn
(
  voiceapp_object_t* voiceapp_object
)
{
  uint32_t rc = APR_EOK;
  
  if ( voiceapp_object == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "voiceapp_helper_timing_reference_fn(): voiceapp object is NULL" );
    return;
  }

  if ( voiceapp_object->rx_timing_cb != NULL )
  {
    rc = apr_timer_start( voiceapp_object->rx_timer,
                          voiceapp_object->rx_timing_offset );
    if( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "voiceapp_helper_timing_reference_fn(): apr_timer_start failed for RX, rc = (0x%08X)",
             rc ); 
    }
    else
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "voiceapp_helper_timing_reference_fn(): started RX timer with offset = (0x%016X)",
             voiceapp_object->rx_timing_offset ); 
    }
  }
  
  if ( voiceapp_object->tx_timing_cb != NULL )
  {
    rc = apr_timer_start( voiceapp_object->tx_timer, 
                          voiceapp_object->tx_timing_offset );
    if( rc )
    {
      MSG_1 ( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
              "voiceapp_helper_timing_reference_fn(): apr_timer_start failed for TX, rc = (0x%08X)",
              rc ); 
    }
    else
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "voiceapp_helper_timing_reference_fn(): started TX timer with offset = (0x%016X)",
             voiceapp_object->tx_timing_offset ); 
    }
  }
}

/* A helper function to destroy timer of voiceapp object */
static void voiceapp_helper_destroy_timer
(
  voiceapp_object_t* voiceapp_object
)
{
  uint32_t rc = APR_EOK;
  
  if ( voiceapp_object == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "voiceapp_helper_destroy_timer(): Voiceapp Object is null" );

    return;
  }

  if ( voiceapp_object->rx_timing_cb != NULL )
  {
    rc = apr_timer_stop( voiceapp_object->rx_timer );
    if( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "voiceapp_helper_destroy_timer(): RX apr_timer_stop Failed!, rc = (0x%08X)",
             rc ); 
    }
    
    rc  = apr_timer_destroy( voiceapp_object->rx_timer );
    if( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "voiceapp_helper_destroy_timer(): RX apr_timer_destroy Failed!, rc = (0x%08X)",
              rc );
    }
    voiceapp_object->rx_timing_cb = NULL;
  }
  
  if ( voiceapp_object->tx_timing_cb != NULL )
  {
    rc = apr_timer_stop( voiceapp_object->tx_timer );
    if( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "voiceapp_helper_destroy_timer(): TX apr_timer_stop Failed!, rc = (0x%08X)",
             rc ); 
    }

    rc = apr_timer_destroy( voiceapp_object->tx_timer );
    if( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "voiceapp_helper_destroy_timer(): TX apr_timer_destroy Failed!, rc = (0x%08X)",
             rc );
    }
    voiceapp_object->tx_timing_cb = NULL;    
  }

  return;
}

/* Function registered with TDS-CDMA, will be called at the VFR tick.
 * We would be using vfr_info.tds_sys_time, which is TDS Stmr value, to derive Tx and Rx offset 
 * to facilitate modem driven packet exchange.
 * vfr_info.vfr_state denotes the state of VFR timing. If state is TDS_VFR_STATE_NOT_READY, then
 * there should not be any packet exchange. This Function will queue the command to voiceapp client
 * (mvs/vs) and calcualte the timer offset and trigger the timers in thier context. Function 
 * voiceapp_process_tdscdma_timing_param( ) will be called in voiceapp client context. 
 */

static void voiceapp_tdscdma_timing_reference_cb
(
  tds_vfr_info_type vfr_info,
  void* client_data
)
{
  uint32_t rc = APR_EOK;
  voiceapp_object_header_t* header;
  voiceapp_object_t* voiceapp_object = NULL;
 
  header = ( ( voiceapp_object_header_t* )client_data );

  for ( ;; )
  {
    if ( header == NULL )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "voiceapp_tdscdma_timing_reference_cb(): Failed because header is NULL" );
      break;
    }
  
    if ( vfr_info.vfr_state != TDS_VFR_STATE_READY )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "voiceapp_tdscdma_timing_reference_cb(): vfr state is unstable" );
      break;
    }
  
    rc = voiceapp_get_object( header->handle, &voiceapp_object );
    if ( rc )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "voiceapp_tdscdma_timing_reference_cb(): Could not find the voiceapp object" );
      break;
    }

    if ( voiceapp_object->network != VOICEAPP_NETWORK_TDSCDMA )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "voiceapp_tdscdma_timing_reference_cb(): Registration not done with TDSCDMA" );
      break;
    }

    voiceapp_object->timing_param.tdscdma.tds_sys_time = vfr_info.tds_sys_time;

    rc = voiceapp_object->queue_cmd_to_client_fn( header->vsid );
    if ( rc )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "voiceapp_tdscdma_timing_reference_cb(): Could not queue command to client" );
      break;
    }
    
    break;
  }
  
  return;
} 

/*
 * Function registered with WCDMA, will be called at every 10ms that is aligned with 
 * DPDCH frame boundary. Callback with even cfn coincide with vfr event. It will be used
 * for our purpose of knowing the vfr tick and subsequently triggering our timer based on CFN 
 * ( connection frame number ) and stmr tick count. This Function will queue the command to voiceapp 
 * client (mvs/vs) and calcualte the timer offset and trigger the timers in thier context. Function 
 * voiceapp_process_wcdma_timing_param( ) will be called in voiceapp client context. 
 */
  
static void voiceapp_wcdma_timing_reference_cb
(
  bool_t steady_state,
  uint8_t cfn,
  uint8_t stmr_tick_count,
  void* client_data
)
{
  uint32_t rc = APR_EOK;
  voiceapp_object_header_t* header;
  voiceapp_object_t* voiceapp_object = NULL;

  header = ( ( voiceapp_object_header_t* )client_data );

  for ( ;; )
  {
    if ( ( header == NULL ) || ( steady_state == FALSE ) )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "voiceapp_wcdma_timing_reference_cb(): header = (0x%08X), "
             "vfr_state = (0x%01X)(0:unstable, 1:stable)", header, steady_state );
      break;
    }
  
    /* return if cfn ( connection frame number ) is odd */ 
    if ( ( cfn & 1 ) )
    {
      break;
    }
  
    rc = voiceapp_get_object( header->handle, &voiceapp_object );
    if ( rc )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "voiceapp_wcdma_timing_reference_cb(): Could not find the voiceapp object" );
      break;
    }

    if ( voiceapp_object->network != VOICEAPP_NETWORK_WCDMA )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "voiceapp_wcdma_timing_reference_cb(): Registration not done with WCDMA" );
      break;
    }

    voiceapp_object->timing_param.wcdma.cfn = cfn;
    voiceapp_object->timing_param.wcdma.stmr_tick_count = stmr_tick_count;

    rc =  voiceapp_object->queue_cmd_to_client_fn( header->vsid );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "voiceapp_wcdma_timing_reference_cb(): could not queue command to client, rc = (0x%08X)",
             rc );
      break;
    }

    break;
  }
  
  return;
}

/**
  This function is used by modem client to register their Rx and Tx callback functions with Voiceapp.
  Voiceapp will trigger the rx_timing_cb function with offset of rx_timing_offset from VFR and
  tx_timing_cb with tx_timing_offset offset from VFR. 
  Following is the way to interpret TDSCDMA timing information and calculate our timer offset. 

   1 radio frame of TDSCDMA is 10ms.
   1 radio frame consists of 2 sub-frames of 5ms each.
   TDSCDMA give us the dump of STMR timer, it is a 32-bit number. 
   We can interpret it as follows : 

   a) Bits - 28:16
      These bits represents sub-frame count
      Range is from 0 to 8191 (SFN*2 = sub-frame count; SFN range is from 0 to 4095)
      Sub-frame count value is divisible by 4 (mod 4) whenever TDSCDMA invokes the 
      timing callback function. We get callback at every 20 msec. 

   b) Bits � 15:0
      Each 5ms sub-frame equals 6400 chips. However, chip count is represented here in terms of x8,
      That means 1 chip count is represented as 8. 6400 will be represented as 6400*8 = 51200.
      So, the range is from 0 to 51199. Based off this information, you can see 1 us = 10.24 chips.
      or 5000 us (5 ms) = 5000*10.24 = 51200
 
  TDSCDMA picks UL vocoder packet at 1 msec from DSM queue. We will program our timers accordingly
  cosidering VFR latencies.

  Voiceapp allows only one registration per VSID. 
 
  @par Payload
  @param[in] vsid : Voice sub-system id. Must register with unique VSID. 
  @param[in] queue_cmd_to_client_fn : voiceapp would call this function to move processing to client context.
  @param[in] rx_timing_cb : rx_timing_cb to be trigerred at rx_timing_offset from VFR. 
  @param[in] tx_timing_cb : tx_timing_cb to be trigerred at tx_timing_offset from VFR.
 
  @return 
  APR_EOK on success.

  @dependencies
  none.

*/

APR_INTERNAL uint32_t voiceapp_register_tdscdma_timing_reference
(
  uint32_t vsid,
  voiceapp_queue_cmd_to_client_fn_t queue_cmd_to_client_fn, 
  voiceapp_modem_timing_callback_fn_t rx_timing_cb,
  voiceapp_modem_timing_callback_fn_t tx_timing_cb
)
{
  uint32_t rc = APR_EOK;
  tdscdma_register_vfr_notification_t tds_register_param;
  voiceapp_object_t* voiceapp_object = NULL;

  /* lock for avoiding race condition between two client
  */
  voiceapp_int_lock_fn( );

  for ( ;; )
  {
    rc = voiceapp_get_object_from_vsid( vsid, &voiceapp_object );

    /* De register for TDSCDMA */
    if ( rx_timing_cb == NULL && tx_timing_cb == NULL ) 
    {
      /* We didn't find a voiceapp object, corresponding to the VSID, that means
       * client has already de registered itself.
       */
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
               "voiceapp_register_tdscdma_timing_reference(): Client with vsid = (0x%08X) already "
               "de-registered for TDSCDMA", vsid );
        rc = APR_EOK;
        break;
      } 
      else
      { 
        if ( voiceapp_object->network != VOICEAPP_NETWORK_TDSCDMA )
        {
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                "voiceapp_register_tdscdma_timing_reference(): Cur Network is (0x%01X), "
                "Deresigration attempted with (0x%01X)", voiceapp_object->network,
                VOICEAPP_NETWORK_TDSCDMA );
          break;
        }
        /* For now, we have agreed upon to send ( NULL, NULL ) for de-registration 
         * We will have to change it to ( NULL, vsid ), if they support multiple (de)registration
         * based on VSID.
         */

        tds_register_param.service = NULL;
        tds_register_param.client_dt = NULL;
        rc = tdscdma_l2_api_call( TDSL2_CMD_VFR_NOTIFICATION_REGISTER, 
                                  &tds_register_param, sizeof( tds_register_param ) );
        if ( rc != TDS_EOK )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                 "voiceapp_register_tdscdma_timing_reference(): " 
                 "Deregistration Failed with rc = (0x%08X)", rc );
          rc = APR_EFAILED;
        }
        else
        {
          rc = APR_EOK;
        }

        voiceapp_helper_destroy_timer( voiceapp_object );
        rc = voiceapp_mem_free_object( voiceapp_object );

        MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "voiceapp_register_tdscdma_timing_reference(): De-registering for TDSCDMA" );
        break;
      }
    } /* Else part is for registration. */
    else
    {
      /* We were able to find a client with the vsid, so fail the API, 
       * client must first de register itself. 
       */
      if ( rc == APR_EOK ) 
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "voiceapp_register_tdscdma_timing_reference(): client already registered with"
               " vsid = (0x%08X)", vsid );
        rc =  APR_EFAILED;
        break;
      }
      else
      {
        rc = voiceapp_mem_alloc_object( sizeof( voiceapp_object_t ), &voiceapp_object );
        if ( rc )
        {
           MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                  "voiceapp_register_tdscdma_timing_reference(): allocation for voiceapp object"
                  " failed rc = (0x%08X)", rc );
          break; 
        }

        if ( queue_cmd_to_client_fn == NULL )
        {
          MSG ( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                "voiceapp_register_tdscdma_timing_reference(): queue_cmd_to_client_fn is NULL" );
          rc = APR_EFAILED;
          break; 
        }
        
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
               "voiceapp_register_tdscdma_timing_reference(): Registering for TDSCDMA with "
               "vsid = (0x%08X)", vsid );
        
        voiceapp_object->header.vsid = vsid;
        voiceapp_object->queue_cmd_to_client_fn = queue_cmd_to_client_fn; 
        voiceapp_object->rx_timing_offset = VOICEAPP_DFLT_TIMER_OFFSET_V;
        voiceapp_object->rx_timing_cb = rx_timing_cb;
        voiceapp_object->tx_timing_offset = VOICEAPP_DFLT_TIMER_OFFSET_V;
        voiceapp_object->tx_timing_cb = tx_timing_cb;
        voiceapp_object->network = VOICEAPP_NETWORK_TDSCDMA;

        if ( rx_timing_cb != NULL )
        {
          rc = apr_timer_create( &voiceapp_object->rx_timer, rx_timing_cb, ( void* )vsid );
          if ( rc )
          {
            MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                  "voiceapp_register_tdscdma_timing_reference():RX Timer create failed"
                  "with rc = (0x%08X)", rc );
            voiceapp_object->rx_timing_cb = NULL;
          }
        }
        if ( tx_timing_cb != NULL )
        {
          rc = apr_timer_create( &voiceapp_object->tx_timer, tx_timing_cb, ( void* )vsid );
          if ( rc )
          {
            MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                  "voiceapp_register_tdscdma_timing_reference(): TX Timer create failed with "
                  "rc = (0x%08X)", rc );
            voiceapp_object->tx_timing_cb = NULL;
          }
        }
        
       /* Resigster with TDSCDMA, voiceapp_tdscdma_timing_reference_cb function will be called by 
        * TDSCDMA at VFR tick, header will be having 4 bytes of vsid, followed by 4 bytes handle.
        * TDSCDMA is required to pass back header, when calling function voiceapp_tdscdma_timing_reference_cb. 
        * In case modem client decides to use vsid for supporting multiple registration and 
        * deregistration, they can typecast first four bytes for their purpose.
        */

        tds_register_param.service = voiceapp_tdscdma_timing_reference_cb;
        tds_register_param.client_dt = ( ( void * )&voiceapp_object->header );
        rc = tdscdma_l2_api_call( TDSL2_CMD_VFR_NOTIFICATION_REGISTER, 
                                  &tds_register_param, sizeof( tds_register_param ) );
        if( rc != TDS_EOK )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                 "voiceapp_register_tdscdma_timing_reference(): "
                 "Registration Failed with rc = (0x%08X)",
                 rc );

          /* Clean up the voiceapp object and timers */
          voiceapp_helper_destroy_timer( voiceapp_object );
          rc = voiceapp_mem_free_object( voiceapp_object );
          rc = APR_EFAILED;
          break;
        }
        else
        {
          rc = APR_EOK;
        }
        break;
      }
    }
  }

  voiceapp_int_unlock_fn( );
  
  return rc; 
}


/**
  This function is used by modem client to register their Rx and Tx callback functions with Voiceapp.
  Voiceapp will trigger the rx_timing_cb function with rx_timing_offset offset from VFR and
  tx_timing_cb with tx_timing_offset offset from VFR. 
  
  Follwoing is the way to interpret information provided by WCDMA and set our timing offset. 
  a) WCDMA send CFN ( connection frame number ) and stmr_tick_value,
     WCDMA picks packet from RLC DSM queue on even CFN.
  b) CFN range from 0-255. 
  c) 150 stmr_time_tick equals 1 cfn. 
  d) WCDMA L1 picks up vocoder packets from DSM queue at 30th slot. 
  e) Ideally stmr_time_tick value should be 0, when calling this function. But, due to the modem latencies
     and delay in reading from stmr block these are not 0. 
  f) So we should program our timer with offset  ( 30 - stmr_tick_value - buffer_offset )*(1/15)msec.
  g) buffer_offset is needed, to account for any delay in putting the vocoder packet to DSM queue.  

  Voiceapp allows only one registration per VSID. 

  @par Payload
  @param[in] vsid : Voice sub-system id. Must register with unique VSID. 
  @param[in] queue_cmd_to_client_fn : voiceapp would call this function to move processing to client context.
  @param[in] rx_timing_cb : rx_timing_cb to be trigerred at rx_timing_offset from VFR. 
  @param[in] tx_timing_cb : tx_timing_cb to be trigerred at tx_timing_offset from VFR.

  @return 
  APR_EOK on success.

  @dependencies
  none.

*/

APR_INTERNAL uint32_t voiceapp_register_wcdma_timing_reference
(
  uint32_t vsid,
  voiceapp_queue_cmd_to_client_fn_t queue_cmd_to_client_fn,
  voiceapp_modem_timing_callback_fn_t rx_timing_cb,
  voiceapp_modem_timing_callback_fn_t tx_timing_cb
)
{
  uint32_t rc = APR_EOK;
  voiceapp_object_t* voiceapp_object;
  wl1_modem_mvs_client_type wl1_vfr_registration;

  /* lock for avoiding race condition between two client
  */
  voiceapp_int_lock_fn( );
 
  for ( ;; )
  {
    rc = voiceapp_get_object_from_vsid( vsid, &voiceapp_object );
 
    /* De register for WCDMA */
    if( rx_timing_cb == NULL && tx_timing_cb == NULL ) 
    {
      /* We didn't find a voiceapp object, corresponding to the VSID, that means
       * client has already de registered itself
       */
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
               "voiceapp_register_wcdma_timing_reference(): Client with vsid = (0x%08X) "
               "already de registered for WCDMA", vsid );
        rc = APR_EOK;
        break;
      }
      else
      {
        if ( voiceapp_object->network != VOICEAPP_NETWORK_WCDMA )
        {
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                 "voiceapp_register_wcdma_timing_reference(): Cur Network is (0x%01X), "
                 "Deresigration attempted with (0x%01X)", voiceapp_object->network,
                 VOICEAPP_NETWORK_WCDMA );
          break;
        }
        /* For now, we have agreed upon to send ( NULL, NULL ) for  de registration. 
         * We will have to change it to ( NULL, vsid ), if they support multiple (de)registration
         * based on vsid.
         */

        wl1_vfr_registration.wl1_modem_mvs_timing_cb_ptr = NULL;
        wl1_vfr_registration.client_data_mvs = NULL;

        rc  = wcdma_ext_api( WCDMA_MVS_REGISTER_FOR_MODEM_TIMING, &wl1_vfr_registration,
                           sizeof( wl1_vfr_registration ) );
        if( rc != WCDMA_STATUS_GOOD )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                 "voiceapp_register_wcdma_timing_reference(): "
                 "Deregistration Failed with rc = (0x%08X)",
                 rc );
          rc = APR_EFAILED;
        }
        else
        {
          rc = APR_EOK;
        }
 
        voiceapp_helper_destroy_timer( voiceapp_object );
        rc = voiceapp_mem_free_object( voiceapp_object );

        MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "voiceapp_register_wcdma_timing_reference(): De registering for WCDMA" );
        break;
      }
    }
    else
    {
      /* We were able to find a client with the vsid, so fail the API, 
       * client must first de register itself. 
       */
      if ( rc == APR_EOK ) 
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "voiceapp_register_wcdma_timing_reference(): Client already registered with " 
               "vsid = (0x%08X)", vsid );
        rc =  APR_EFAILED;
        break; 
      }
      else
      {
        rc = voiceapp_mem_alloc_object( sizeof( voiceapp_object_t ), &voiceapp_object );
        if ( rc )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                 "voiceapp_register_wcdma_timing_reference(): Creation of voiceapp object"
                 "failed with rc = (0x%08X)", rc );
          break; 
        }

        if ( queue_cmd_to_client_fn == NULL )
        {
          MSG ( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                "voiceapp_register_wcdma_timing_reference(): queue_cmd_to_client_fn is NULL" );
          rc = APR_EFAILED;
          break; 
        }
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
               "voiceapp_register_wcdma_timing_reference(): Registering for WCDMA with "
               "vsid = (0x%08X)", vsid );
        voiceapp_object->header.vsid = vsid;
        voiceapp_object->queue_cmd_to_client_fn = queue_cmd_to_client_fn;
        voiceapp_object->rx_timing_offset = VOICEAPP_DFLT_TIMER_OFFSET_V;
        voiceapp_object->rx_timing_cb = rx_timing_cb;
        voiceapp_object->tx_timing_offset = VOICEAPP_DFLT_TIMER_OFFSET_V;
        voiceapp_object->tx_timing_cb = tx_timing_cb;
        voiceapp_object->network = VOICEAPP_NETWORK_WCDMA;
 
        if ( rx_timing_cb != NULL )
        {
          rc = apr_timer_create( &voiceapp_object->rx_timer, rx_timing_cb, ( void* )vsid );  
          if ( rc )
          {
            MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                   "voiceapp_register_wcdma_timing_reference(): RX Timer create failed with"
                   "rc = (0x%08X)", rc );
            voiceapp_object->rx_timing_cb = NULL;
          }
        }
        if ( tx_timing_cb != NULL )
        {
          rc = apr_timer_create( &voiceapp_object->tx_timer, tx_timing_cb, ( void* )vsid ); 
          if ( rc )
          {
            MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                   "voiceapp_register_wcdma_timing_reference(): TX Timer create failed with "
                   "rc = (0x%08X)", rc );
            voiceapp_object->tx_timing_cb = NULL;
          }
        }

        /* Resigster with WCDMA, voiceapp_wcdma_timing_reference_cb function will be called by 
         * WCDMA at every 10ms that is aligned with DPDCH frame boundary.
         * Callback with even cfn coincide with vfr event. It will be used for our purpose.  
         * WCDMA is required to pass back header when calling function voiceapp_wcdma_timing_reference_cb.
         * In case modem client decides to use vsid for supporting multiple registration and 
         * deregistration, they can typecast first four bytes for their purpose.
         */

        wl1_vfr_registration.wl1_modem_mvs_timing_cb_ptr = voiceapp_wcdma_timing_reference_cb;
        wl1_vfr_registration.client_data_mvs = ( ( void* )&voiceapp_object->header );

        rc  = wcdma_ext_api( WCDMA_MVS_REGISTER_FOR_MODEM_TIMING, &wl1_vfr_registration,
                           sizeof( wl1_vfr_registration ) );
        if( rc != WCDMA_STATUS_GOOD )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                 "voiceapp_register_wcdma_timing_reference(): "
                 "Deregistration Failed with rc = (0x%08X)",
                 rc );

          /* Clean up the voiceapp object and timers */
          voiceapp_helper_destroy_timer( voiceapp_object );
          rc = voiceapp_mem_free_object( voiceapp_object );
          rc = APR_EFAILED;
          break;
        }
        else
        {
          rc = APR_EOK;
        }
        break;
      }
    }
  }
 
  voiceapp_int_unlock_fn( );
  
  return rc; 
}

/**
  Functions take media_id and meta-data of media_id and copies the corresponding 
  homing vector to frame location. 
 
  @par Payload

  INPUT PARAM:
  @param[in] media_id : Supported media id is VOICEAPP_MEDIA_ID_AMR and VOICEAPP_MEDIA_ID_AMRWB. 
  @param[in] frame_info: media_id determines the frame_info, it contains meta-data for media_id.
  @param[in,out] frame : Location at which ( header + homing frame ) has to be copied. 
  @param[in,out] frame_size: Size of ( header + homing frame ) that was copied to location frame. 
  
  @return 
  APR_EOK on success.

  @dependencies
  none

*/
APR_INTERNAL uint32_t voiceapp_get_homing_frame 
(
  uint32_t media_id,
  void* frame_info,
  uint8_t* frame,
  uint32_t* frame_size
)
{
  uint32_t rc = APR_EOK;
  voiceapp_amr_frame_info_t* amr_info;
  voiceapp_amrwb_frame_info_t* amrwb_info;
  voiceapp_amr_codec_mode_type_t amr_codec_mode;
  voiceapp_amr_frame_type_t amr_frame_type;
  voiceapp_amrwb_codec_mode_type_t amrwb_codec_mode;
  voiceapp_amrwb_frame_type_t amrwb_frame_type;
  
  switch( media_id )
  {
  case VOICEAPP_MEDIA_ID_EAMR:
  case VOICEAPP_MEDIA_ID_AMR:
    {  
      amr_info = ( ( voiceapp_amr_frame_info_t* )frame_info );
      if ( amr_info == NULL )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "voiceapp_get_homing_frame(): amr_info is NULL" );
        return APR_EBADPARAM;
      }
      
      amr_codec_mode = amr_info->codec_mode;
      amr_frame_type = amr_info->frame_type;
      if( ( amr_codec_mode < VOICEAPP_AMR_CODEC_MODE_0475 ) || 
          ( amr_codec_mode > VOICEAPP_AMR_CODEC_MODE_1220 ) )
      {
        *frame_size = 0; 
        return APR_EFAILED; 
      }

      /* copy the homing sequence vector, corresponding to a given rate to the frame location. */
      mmstd_memcpy( &frame[1], VOICEAPP_AMR_FRAME_SIZE_V,
                    voiceapp_amr_if1_homing_frame[amr_codec_mode], VOICEAPP_AMR_FRAME_SIZE_V );

      /* Append the frame_type and codec_mode, format is one byte of header followed by frame.
       * Header has 8 bits, bits 0-3 indicate codec_mode, bits 4-7 indicate frame_type.
       * Note: Packet format is similar to the packets received from VSM. That is 
       * ( header + speech_data( class A,B,C bits ) ).
       */ 
      frame[0] = ( ( ( amr_frame_type & 0x0F ) << 4 ) | ( amr_codec_mode & 0x0F ) );
      *frame_size = ( VOICEAPP_AMR_FRAME_SIZE_V + 1 );
      rc = APR_EOK;
    }
    break;
  
  case VOICEAPP_MEDIA_ID_AMRWB: 
    {
      amrwb_info = ( ( voiceapp_amrwb_frame_info_t* )frame_info );
      if ( amrwb_info == NULL )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "voiceapp_get_homing_frame(): amrwb_info is NULL" );
        return APR_EBADPARAM;
      }
      
      amrwb_codec_mode = amrwb_info->codec_mode;
      amrwb_frame_type = amrwb_info->frame_type;

      if( ( amrwb_codec_mode < VOICEAPP_AMRWB_CODEC_MODE_0660 ) ||
          ( amrwb_codec_mode > VOICEAPP_AMRWB_CODEC_MODE_2385 ) )
      {
        *frame_size = 0; 
        return APR_EFAILED;
      }

      /* copy the homing sequence vector, corresponding to a given rate to the frame location. */
      mmstd_memcpy( &frame[1], VOICEAPP_AMRWB_FRAME_SIZE_V,
                    voiceapp_awb_if1_homing_frame[amrwb_codec_mode], VOICEAPP_AMRWB_FRAME_SIZE_V );

      /* Append the frame_type and codec_mode, format is one byte of header followed by frame.
       * Header has 8 bits, bits 0-3 indicate codec_mode, bits 4-7 indicate frame_type. 
       * Note: Packet format is similar to the packets received from VSM. That is 
       * ( header + speech_data( class A,B,C bits ) ).
       */ 
      frame[0] = ( ( ( amrwb_frame_type & 0x0F ) << 4 ) | ( amrwb_codec_mode & 0x0F ) );
      *frame_size = ( VOICEAPP_AMRWB_FRAME_SIZE_V + 1 );
      rc = APR_EOK;
    }
    break;
  
  default: 
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "voiceapp_get_homing_frame(): Media Id = (0x%08X) is not supported", media_id );
      rc  = APR_EUNSUPPORTED;
    }
    break;
  }
  
  return rc;
}

/* Function derives the WCDMA timing information and triggers the timers accordingly.
 * Following is the information. 
 * a) WCDMA send CFN ( connection frame number ) and stmr_tick_value,
 *    WCDMA picks packet from RLC DSM queue on even CFN.
 * b) CFN range from 0-255. 
 * c) 150 stmr_time_tick equals 1 cfn. 
 * d) WCDMA L1 picks up vocoder packets from DSM queue at 30th slot. 
 * e) Ideally stmr_time_tick value should be 0, when calling this function. But, due to the modem latencies
      and delay in reading from stmr block these are not 0. 
 * f) So we should program our timer with offset  ( 30 - stmr_tick_value - buffer_offset )*(1/15)msec.
 * g) Buffer_offset is needed to have enough margin between modem client writing the packet and 
 *    RLC reading the packet from DSM queue
 */

void voiceapp_process_wcdma_timing_param
(
  voiceapp_object_t* voiceapp_object
)
{
  int64_t tx_timing_offset_in_micro_sec = 0;  

  if ( voiceapp_object == NULL ) 
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "voiceapp_process_wcdma_timing_param(): Failed because voiceapp_object is NULL" );
    return;
  }
  
  /* return if cfn ( connection frame number ) is odd */ 
  if ( ( voiceapp_object->timing_param.wcdma.cfn & 1 ) )
  {
    return;
  }


  /*Calculate the timing offset in stmr timer tick count.*/
  tx_timing_offset_in_micro_sec = ( int64_t )( ( int64_t )( VOICEAPP_WCDMA_READS_UL_PACKET_AT_STMR_TICK_COUNT_V ) - 
                                               ( int64_t )( voiceapp_object->timing_param.wcdma.stmr_tick_count ) );
 
 /* Converting ticks to microsec. 150 ticks equals 10 milli-sec, or 150 ticks equals 10000 micro-sec
  * or 1 tick equals 1000/15 or 200/3 microsec or 66.666 microsec .
  */
  tx_timing_offset_in_micro_sec = ( int64_t )( ( 1.0 * tx_timing_offset_in_micro_sec ) * 66.66 );

  tx_timing_offset_in_micro_sec = ( tx_timing_offset_in_micro_sec - 
                                    ( int64_t )( VOICEAPP_BUF_TIME_FOR_PUTTING_UL_PACKET_IN_DSM_V ) );

  if ( tx_timing_offset_in_micro_sec <= 0 )
  {
    if ( ( ( -1 ) * tx_timing_offset_in_micro_sec ) >= ( int64_t )( VOICEAPP_VOICE_FRAME_SIZE_US_V ) )
    {
       MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
              "voiceapp_process_wcdma_timing_param(): voiceapp_object->timing_param.wcdma.cfn "
              " = (0x%08X) is not valid ", voiceapp_object->timing_param.wcdma.cfn );
       return;
    }
    tx_timing_offset_in_micro_sec = ( tx_timing_offset_in_micro_sec + 
                                      ( int64_t )( VOICEAPP_VOICE_FRAME_SIZE_US_V ) );
  }

  MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
         "voiceapp_wcdma_timing_reference_cb(): "
         "cfn = (0x%02X), stmr tick count = (0x%02X), timing_offset_in_micro_sec = (0x%016X)",
         voiceapp_object->timing_param.wcdma.cfn,
         voiceapp_object->timing_param.wcdma.stmr_tick_count,
         tx_timing_offset_in_micro_sec );

  /* Convert time in nano-sec, apr_timer expects time in nano-sec. */
  voiceapp_object->tx_timing_offset = ( tx_timing_offset_in_micro_sec * 1000 );
  voiceapp_helper_timing_reference_fn( voiceapp_object );
}

/* Function derives the TDSCDMA timing information and triggers the timers accordingly.
 * 1 radio frame of TDSCDMA is 10ms.
 * 1 radio frame consists of 2 sub-frames of 5ms each.
 * TDSCDMA give us the dump of STMR timer, it is a 32-bit number. 
 * We can interpret it as follows : 

 * a) Bits - 28:16
 *    These bits represents sub-frame count
 *    Range is from 0 to 8191 (SFN*2 = sub-frame count; SFN range is from 0 to 4095)
 *    Sub-frame count value is divisible by 4 (mod 4) whenever TDSCDMA invokes the 
 *    timing callback function. We get callback at every 20 msec. 

 * b) Bits � 15:0
 *    Each 5ms sub-frame equals 6400 chips. However, chip count is represented here in terms of x8,
 *    That means 1 chip count is represented as 8. 6400 will be represented as 6400*8 = 51200.
 *    So, the range is from 0 to 51199. Based off this information, you can see 1 us = 10.24 chips.
 *    or 5000 us (5 ms) = 5000*10.24 = 51200
 * 
 * TDSCDMA picks UL vocoder packet at 1 msec from DSM queue. We will program our timers accordingly
 * cosidering VFR latencies.
 */

void voiceapp_process_tdscdma_timing_param
(
  voiceapp_object_t* voiceapp_object
)
{
  int64_t tx_timing_offset_in_micro_sec = 0;
  uint32_t tds_sub_frame_count = 0;
  uint32_t tds_stmr_tick_count = 0;
  uint32_t tds_vfr_latency_in_micro_sec = 0;
 
  if ( voiceapp_object == NULL ) 
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "voiceapp_process_tdscdma_timing_param(): Failed because voiceapp_object is NULL" );
    return;
  }

  /* bits [28:16] represents the sub frame count from TDS. */
  tds_sub_frame_count = ( ( voiceapp_object->timing_param.tdscdma.tds_sys_time & 0x1FFF0000 ) >> 16 );

  /* bits [15:0] represents the system tick for current TDS sub frame.
   * (512) ticks are equivalent to (50) usec.
   */
  tds_stmr_tick_count = ( voiceapp_object->timing_param.tdscdma.tds_sys_time & 0x0000FFFF );
  tds_vfr_latency_in_micro_sec = ( uint32_t )( ( 1.0 * tds_stmr_tick_count ) / 10.24 ); 

  MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
         "voiceapp_tdscdma_timing_reference_cb(): tds_sub_frame_count = (0x%08X),"
         "tds_stmr_tick_count = (0x%08X), tds_vfr_latency_in_micro_sec = (0x%08X)",
         tds_sub_frame_count, tds_stmr_tick_count, tds_vfr_latency_in_micro_sec );

  tx_timing_offset_in_micro_sec = ( int64_t )( ( int64_t )( VOICEAPP_TDSCDMA_READS_UL_PKT_FRM_DSM_AT_TIME_V ) - 
                                               ( int64_t )( tds_vfr_latency_in_micro_sec) );
  
  tx_timing_offset_in_micro_sec = ( int64_t )( ( int64_t )( tx_timing_offset_in_micro_sec ) - 
                                               ( int64_t )( VOICEAPP_BUF_TIME_FOR_PUTTING_UL_PACKET_IN_DSM_V ) );

  if ( tx_timing_offset_in_micro_sec <= 0 )
  {
    if ( ( ( -1 ) * tx_timing_offset_in_micro_sec ) >= ( int64_t )( VOICEAPP_VOICE_FRAME_SIZE_US_V ) )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "voiceapp_process_tdscdma_timing_param(): tds_vfr_latency_in_micro_sec = (0x%08X) is invalid",
             tds_vfr_latency_in_micro_sec );
      return;
    }
    tx_timing_offset_in_micro_sec  = ( int64_t )( ( int64_t )( tx_timing_offset_in_micro_sec ) +
                                                  ( int64_t )( VOICEAPP_VOICE_FRAME_SIZE_US_V ) );
  }

  /* APR-Timer expects time in nano-second.
   */
  voiceapp_object->tx_timing_offset = ( tx_timing_offset_in_micro_sec * 1000 );
  voiceapp_helper_timing_reference_fn( voiceapp_object );
}

/* This function is called in client context, voiceapp should have internal states corrosponding to 
 * vsid and should do all its processing in client context. 
 */
APR_INTERNAL uint32_t voiceapp_process_command
(
  uint32_t vsid
)
{
  uint32_t rc = APR_EOK;
  voiceapp_object_t* voiceapp_object = NULL;

  rc = voiceapp_get_object_from_vsid( vsid, &voiceapp_object );
  if ( rc )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "voiceapp_processing_context(): No voiceapp object with vsid = (0x%08X)", vsid );
    return APR_EFAILED;
  }

  switch ( voiceapp_object->network )
  {
  case VOICEAPP_NETWORK_TDSCDMA: 
    {
      voiceapp_process_tdscdma_timing_param( voiceapp_object );
    }
    break;
    
  case VOICEAPP_NETWORK_WCDMA:
    {
      voiceapp_process_wcdma_timing_param( voiceapp_object );
    }
    break;
    
  default:
    return APR_EFAILED;
  }

  return APR_EOK;
}

/**
  Issues a command to VOICEAPP.
 
  @param[in] cmd_id       Command ID.
  @param[in,out] params   Command payload.
  @param[in] size         Size of the command payload.
   
  Only supported cmd_id is DRV_CMDID_INIT and DRV_CMDID_DEINIT.
  
  @return
  APR_EOK when successful.
 
  @dependencies
  None.

  @comments
  None
*/

APR_INTERNAL uint32_t voiceapp_call
(
  uint32_t cmd_id,
  void* params,
  uint32_t size
)
{
  uint32_t rc = APR_EOK;

  switch ( cmd_id )
  {
  case DRV_CMDID_INIT:
    {
      rc = voiceapp_init( );
    }
    break;

  case DRV_CMDID_DEINIT:
    {
      rc = voiceapp_deinit( );
    }
    break;
   
  default:
    break;
  }
  
  if ( rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "voiceapp_call(): Failed cmd_id = (0x%08X), rc = (0x%08X)", cmd_id, rc );
  }
  
  return rc;
}
