#ifndef __VSS_ISTREAM_COMMON_PUBLIC_IF_H__
#define __VSS_ISTREAM_COMMON_PUBLIC_IF_H__

/**
  @file vss_istream_public_if.h
  @brief This file contains the APR API definitions for Core Voice Stream (CVS)
  module.
*/

/*===========================================================================
NOTE: The @brief description and any detailed descriptions above do not appear
      in the PDF.

      The CVD_mainpage.dox file contains all file/group descriptions
      that are in the output PDF generated using Doxygen and Latex. To edit or
      update any of the file/group text in the PDF, edit the
      CVD_mainpage.dox file or contact Tech Pubs.

      The above description for this file is part of the vss_istream_public_if
      group description in the CVD_mainpage.dox file.
=============================================================================*/
/*===========================================================================
Copyright (C) 2011-2014 QUALCOMM Technologies Incorporated.
All rights reserved.
Qualcomm Confidential and Proprietary
===========================================================================*/

/*===========================================================================

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/avs/api/cvd/vss_istream_common_public_if.h#1 $
  $Author: mplp4svc $

===========================================================================*/


#include "apr_comdef.h"


/** @addtogroup vss_istream_common_public_if
@{ */


/****************************************************************************
 * CALIBRATION DATA APIS                                                    *
 ****************************************************************************/

/** @brief Type definition for #vss_istream_cal_na_value_t.
*/
typedef union vss_istream_cal_na_value_t vss_istream_cal_na_value_t;

#include "apr_pack_begin.h"

/** @brief Union for specifying a calibration column N/A value.
*/
union vss_istream_cal_na_value_t
{
  uint8_t uint8_val;
    /**< UINT8 N/A value. */
  uint16_t uint16_val;
    /**< UINT16 N/A value. */
  uint32_t uint32_val;
    /**< UINT32 N/A value. */
  uint64_t uint64_val;
    /**< UINT64 N/A value. */
}
#include "apr_pack_end.h"
;


/** @brief Type definition for #vss_istream_cal_column_t.
*/
typedef struct vss_istream_cal_column_t vss_istream_cal_column_t;

#include "apr_pack_begin.h"

/** @brief Structure for specifying a calibration column.
*/
struct vss_istream_cal_column_t
{
  uint32_t id;
    /**< Column ID. Supported values: \n
         see @latexonly \hyperref[calibration_column]{Calibration Columns}
         @endlatexonly. */
  uint32_t type;
    /**< Column type. Supported values: \n
         see @latexonly \hyperref[calibration_column_type]{Calibration Column Types}
         @endlatexonly. */
  vss_istream_cal_na_value_t na_value;
    /**< The N/A column value associated with this column ID and type.
         
         The N/A value is a reseved value indicating that the calibration
         data entry corresponding to it in the table is independent of this
         column (and therefore the data is applied irrespective of what
         the current system configuration is for this column).

         The size of na_value is dependent on the column type. */
}
#include "apr_pack_end.h"
;
/** @} */ /* end_addtogroup vss_istream_common_public_if */

#endif /* __VSS_ISTREAM_COMMON_PUBLIC_IF_H__ */

