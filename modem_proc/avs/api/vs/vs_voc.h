#ifndef __VS_VOC_H__
#define __VS_VOC_H__

/**
  @file  vs_voc.h
  @brief This file provides a single API entry-point into VS
         infrastructure

*/

/*
  ============================================================================

   Copyright (C) 2012 QUALCOMM Technologies Incorporated. All Rights Reserved.
   QUALCOMM Proprietary and Confidential

  ============================================================================

                             Edit History

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/avs/api/vs/vs_voc.h#1 $
  $Author: mplp4svc $

  when      who   what, where, why
  --------  ---   ------------------------------------------------------------
  12/05/12  sud   Initial revision

  ============================================================================
*/

/****************************************************************************
 * INCLUDE HEADER FILES                                                     *
 ****************************************************************************/

#include "vs_errcodes.h"
#include "vs_common.h"

/****************************************************************************
 * TYPE DEFINITIONS                                                         *
 ****************************************************************************/

/**
 * Vocoder client definitions.
 */
#define VS_VOC_CLIENT_GSM ( 0x00012E26 ) /* GSM Modem Subsystem. */

/*----------------------------------------------------------------------------
  Start of Structures
----------------------------------------------------------------------------*/

typedef struct vs_voc_buffer_t vs_voc_buffer_t;
struct vs_voc_buffer_t
{
  uint16_t version;
    /**< The minor version. Any changes are backward compatible. */

  uint32_t flags;
    /**<
      * Bit 0 set : Buffer is filled with a valid encoder speech frame.
      *
      *   Indicates the buffer is filled with a valid speech frame when true.
      *   The voice driver sets this when delivering encoder buffers for the read
      *   This flag is ignored for buffers given from client to the voice driver
      *
      * Bit 1 set : The timestamp value is valid. Currently not supported.
      */

  uint32_t media_id;
    /** See VS_COMMON_MEDIA_ID_XXX. */
  void* frame_info;
    /**<
      * Frame info payload.
      *
      * The media_id determines the actual frame info data type.
      *
      * See vs_vocxxx_frame_info_t for the respective data structures.
      */
  uint8_t* frame;
    /**< This is the pointer to the vocoder packet data. */
  uint32_t size;
    /**<
      * The actual frame size of the vocoder packet data in bytes.
      *
      * If used in the context of writing buffers, then the actual frame
      * size is the size of what the client wants to send to the vocoder
      * decoder. The client must set the value of this size. This size must be
      * equal or less than the max_size field.
      *
      * If used in the context of reading buffers, then the actual
      * frame size is the size of what the vocoder encoder is sending.
      * This value is set by the vocoder driver.
      */
  uint32_t max_size;
    /**<
      * This is a read only field. The client must not change its value!
      *
      * This is the maximum buffer size allocated to store the vocoder frame.
      */

  uint64_t timestamp;
    /**< In microsecond units. Currently not supported. */

  uint32_t tag;
	/**< 
	  * This field is filled by client to tag a vocoder session. Client 
	  * should write a unique value to this field for each vocoder session. 
	  *  
	  * This can be used by client to flush out stale vocoder packet data 
	  * that it may probably get during handover or when the session is 
	  * disabled and then enabled back to back within a very short time. 
	  */
};

/*----------------------------------------------------------------------------
  VS Control functions
-----------------------------------------------------------------------------*/

/* [Synchronous blocking API]
 *
 * This is the open command. Use this command to acquire a handle to a new
 * vocoder session.
 *
 * The vocoder resource is granted to the client upon the client receiving a
 * VS_COMMON_EVENT_READY event. Clients must assume that they do not have
 * the vocoder resource until the VS_COMMON_EVENT_READY event is received.
 *
 * The vocoder resource may be taken back by the system upon the client
 * receiving a VS_COMMON_EVENT_NOT_READY event.
 *
 * The client is required to use the returned handle (ret_handle) provided by
 * the open command to execute any subsequent commands on the vocoder session.
 *
 * The client is permitted to send any vocoder data commands to the vocoder
 * session after successfully calling the open command. The client is
 * permitted to call vocoder data commands before enabling the vocoder
 * session. Updates to the vocoder resource will only be applied after the
 * client is granted the resource.
 *
 * Each voice system controls at most one vocoder resource. At most only one
 * client per voice system will be granted the vocoder resource. The
 * capability to run multiple vocoder resources across multiple voice systems
 * is a product specific implementation and may not be the same across
 * products.
 */
#define VS_VOC_CMD_OPEN ( 0x00012E27 )

typedef struct vs_voc_cmd_open_t vs_voc_cmd_open_t;
struct vs_voc_cmd_open_t
{
  uint32_t* ret_handle;
    /**<
      * Returns the handle that the client must use when making subsequent
      * commands.
      */
  uint32_t vsid;
    /**< System level published/documented Voice System ID. */
  uint32_t client_id;
    /**< See VS_VOC_CLIENT_XXX. */
  void* session_context;
    /**<
      * The client should store its session context pointer here in order to
      * retrieve its session control data structure to queue and signal events
      * into its worker thread.
      *
      * The session_context is returned to the client each time the event
      * callback is triggered.
      */
  vs_common_event_callback_fn_t event_cb;
    /**<
      * This is the central event callback function which receives
      * asynchronous events from the voice driver.
      *
      * Operational contract:
      *
      * The client shall only queue the incoming event and signal a worker
      * thread to process the event. The client must not perform any other
      * processing in the callback context.
      *
      * The client shall not call any APIs on the voice driver in the
      * callback context. This will cause synchronation issues for the
      * driver and may lead to a system failure or deadlock.
      *
      * The client shall not perform any blocking operations or acquire any
      * locks in the callback context that leads to a system deadlock.
      *
      * The client shall spend no more than 5us while in the callback
      * context.
      *
      * It is strongly recommended to use atomic operations for
      * synchronization needs.
      *
      * Failure to meet the operational contract may lead to an undefined
      * state, system stability, and performance issues. This is the only
      * warning that you will receive.
      */
};

/* [Asynchronous non-blocking API]
 *
 * This command closes the opened session.
 *
 * The client shall not use the open handle after receiving a successful
 * close indication. A VS_COMMON_EVENT_CMD_RESPONSE event will be sent to
 * the client with the status_id set to VS_EOK upon success.
 *
 * The VS_COMMON_EVENT_CMD_RESPONSE event is sent to the client to indicate
 * success or failure.
 */
#define VS_VOC_CMD_CLOSE ( 0x00012E28 )

typedef struct vs_voc_cmd_close_t vs_voc_cmd_close_t;
struct vs_voc_cmd_close_t
{
  uint32_t handle; /**< The open handle. */
  void* client_context; /**< The client provided context for client's use. */
};

/* [Asynchronous non-blocking API]
 *
 * This command requests to enable a vocoder resource.
 *
 * The vocoder does not actually begin until the client receives an
 * VS_COMMON_EVENT_READY event.
 *
 * The vocoder can be taken back by the voice driver after receiving an
 * VS_COMMON_EVENT_NOT_READY event. This can occur at any time. Some reasons
 * for losing the vocoder resource can be due to but not limited to a DSP
 * crash, preemptions, calls to VS_VOC_CMD_STANDBY or VS_VOC_CMD_DISABLE
 * command, device switch, and others.
 *
 * Calling VS_VOC_CMD_ENABLE more than once is not permitted and will not
 * succeed.
 *
 * The client must first call VS_VOC_CMD_DISABLE before calling
 * VS_VOC_CMD_ENABLE again to request for a different vocoder resource.
 *
 * The VS_COMMON_EVENT_CMD_RESPONSE event is sent to the client to indicate
 * success or failure.
 */
#define VS_VOC_CMD_ENABLE ( 0x00012E29 )

typedef struct vs_voc_cmd_enable_t vs_voc_cmd_enable_t;
struct vs_voc_cmd_enable_t
{
  uint32_t handle; /**< The open handle. */
  uint32_t media_id; /**< This is the media type to request enabling. See VS_COMMON_MEDIA_ID_XXX. */
  void* client_context; /**< The client provided context for client's use. */
};

/* [Asynchronous non-blocking API]
 *
 * Used by the client to request for a vocoder resource to be brought up but
 * kept paused in order to reduce the enabling time when actually required for
 * optimization purposes.
 *
 * The client supplied media type is used to predict the vocoder that would be
 * enabled later through the VS_VOC_CMD_ENABLE command.
 *
 * The VS_COMMON_EVENT_NOT_READY event will be sent to the client only if the
 * client has previously received a VS_COMMON_EVENT_READY event.
 *
 * The VS_COMMON_EVENT_CMD_RESPONSE event is sent to the client to indicate
 * success or failure.
 */
#define VS_VOC_CMD_STANDBY ( 0x00012E2A )

typedef struct vs_voc_cmd_standby_t vs_voc_cmd_standby_t;
struct vs_voc_cmd_standby_t
{
  uint32_t handle; /**< The open handle. */
  uint32_t media_id; /**< This is the media type to request getting ready. See VS_COMMON_MEDIA_ID_XXX. */
  void* client_context; /**< The client provided context for client's use. */
};

/* [Asynchronous non-blocking API]
 *
 * This command disables the vocoder resource.
 *
 * The VS_COMMON_EVENT_NOT_READY event will be sent to the client only if the
 * client has previously received a VS_COMMON_EVENT_READY event.
 *
 * The VS_COMMON_EVENT_CMD_RESPONSE event is sent to the client to indicate
 * success or failure.
 */

#define VS_VOC_CMD_DISABLE ( 0x00012E2B )

typedef struct vs_voc_cmd_disable_t vs_voc_cmd_disable_t;
struct vs_voc_cmd_disable_t
{
  uint32_t handle; /**< The open handle. */
  void* client_context; /**< The client provided context for client's use. */
};

/*----------------------------------------------------------------------------
  Data functions
-----------------------------------------------------------------------------*/

/* [Synchronous blocking API]
 *
 * This command allocates one buffer from the voice driver for asynchronously
 * sending and receiving vocoder packets.
 *
 * The client is recommended to allocate the buffers during power on or during
 * call setup. Allocations of buffers during steady state may add undesirable
 * delays.
 *
 * This command must not be called from ISR or IST context.
 *
 * The buffers must be returned to the voice driver by calling
 * VS_VOC_CMD_FREE_BUFFER before the VS_VOC_CMD_CLOSE command is called.
 */

#define VS_VOC_CMD_ALLOC_BUFFER ( 0x00012E2C )

typedef struct vs_voc_cmd_alloc_buffer_t vs_voc_cmd_alloc_buffer_t;
struct vs_voc_cmd_alloc_buffer_t
{
  uint32_t handle; /**< The open handle. */
  vs_voc_buffer_t** ret_buffer;
    /**<
      * Returns a buffer structure that the client can use for asynchronously
      * sending and receiving vocoder packets.
      *
      * The buffer structure contains a vocoder packet header, a speech frame,
      * and additional auxillary information.
      */
  uint32_t req_max_frame_size;
    /**<
      * This is the requested maximum vocoder speech frame size desired by the
      * client.
      */
};

/* [Synchronous blocking API]
 *
 * This command frees a buffer back to the voice driver.
 *
 * The client must not access the buffer once the buffer is freed.
 *
 * This command must not be called from ISR or IST context.
 */

#define VS_VOC_CMD_FREE_BUFFER ( 0x00012E2D )

typedef struct vs_voc_cmd_free_buffer_t vs_voc_cmd_free_buffer_t;
struct vs_voc_cmd_free_buffer_t
{
  uint32_t handle; /**< The open handle. */
  vs_voc_buffer_t* buffer; /**< The buffer to return. */
};

/* [Synchronous non-blocking API]
 *
 * This command pushes a buffer into the voice driver in order to
 * asynchronously receive an encoder packet from the voice driver.
 *
 * The client should call the VS_VOC_CMD_READ_BUFFER command to retrieve one
 * or more encoder buffers upon receiving the VS_VOC_EVENT_READ_AVAILABLE
 * event.
 */

#define VS_VOC_CMD_PRIME_READ_BUFFER ( 0x00012E2E )

typedef struct vs_voc_cmd_prime_read_buffer_t vs_voc_cmd_prime_read_buffer_t;
struct vs_voc_cmd_prime_read_buffer_t
{
  uint32_t handle; /**< The open handle. */
  vs_voc_buffer_t* buffer; /**< The buffer to prime into the voice driver. */
};

/* [Asynchronous event]
 *
 * Notifies the client that one encoder buffer is available to read.
 *
 * There is no parameter for this event.
 */
#define VS_VOC_EVENT_READ_AVAILABLE ( 0x00012E2F )

/* [Synchronous non-blocking API]
 *
 * This command reads a buffer from the encoder. The client should call the
 * read command to retrieve a buffer for processing after receiving
 * a VS_VOC_EVENT_READ_AVAILABLE event.
 *
 * The buffers are read in the sequence of encoding. The buffer structures are
 * not necessarily returned in the same order when they were primed by the
 * client.
 *
 * The read command is the only way to retrieve buffers for the encoder from
 * the voice driver.
 *
 * To handle the flush command, after a VS_VOC_CMD_FLUSH_BUFFERS command
 * completes, the client needs to call the read command to retrieve the
 * flushed buffers.
 *
 * Not all buffers contain speech frames. The client should check the buffer
 * to see if there is a valid speech frame.
 *
 * The client may continue reading for buffers until the command returns
 * VS_ENOTEXIST to indicate that there is no more buffers available to read.
 *
 * Returns VS_EOK if buffer is available.
 * Returns VS_ENOTEXIST when no buffers are available.
 */

#define VS_VOC_CMD_READ_BUFFER ( 0x00012E30 )

typedef struct vs_voc_cmd_read_buffer_t vs_voc_cmd_read_buffer_t;
struct vs_voc_cmd_read_buffer_t
{
  uint32_t handle; /**< The open handle. */
  vs_voc_buffer_t** ret_buffer; /**< The buffer structure that is returned back to the client. */
};

/* [Synchronous non-blocking API]
 *
 * This command writes a buffer to the decoder.
 *
 * The buffer does not return until the decoder consumes the buffer or due to
 * the VS_VOC_CMD_FLUSH_BUFFERS command. In all cases, a buffer is
 * returned to the client after the client receives the
 * VS_VOC_EVENT_WRITE_BUFFER_RETURNED event that notifies the specific buffer
 * that is returned.
 */

#define VS_VOC_CMD_WRITE_BUFFER ( 0x00012E31 )

typedef struct vs_voc_cmd_write_buffer_t vs_voc_cmd_write_buffer_t;
struct vs_voc_cmd_write_buffer_t
{
  uint32_t handle; /**< The open handle. */
  vs_voc_buffer_t* buffer; /**< The buffer to send to the decoder. */
};

/* [Asynchronous event]
 *
 * Notifies the client that a write buffer is returned.
 */
#define VS_VOC_EVENT_WRITE_BUFFER_RETURNED ( 0x00012E32 )

typedef struct vs_voc_event_write_buffer_returned_t vs_voc_event_write_buffer_returned_t;
struct vs_voc_event_write_buffer_returned_t
{
  vs_voc_buffer_t* buffer; /**< The buffer structure that is returned back to the client. */
};

/* [Asynchronous non-blocking API]
 *
 * This command flushes both the read and write buffers from the voice driver.
 *
 * The write buffers will be returned through the
 * VS_VOC_EVENT_WRITE_BUFFER_RETURNED events _before_ the flush command
 * completes.
 *
 * The read buffers will be returned through the VS_VOC_CMD_READ_BUFFER
 * command upon receiving the VS_VOC_EVENT_READ_AVAILABLE events _before_ the
 * flush command completes. The client can optionally repeatedly call the read
 * command after the flush command completes to retrieve all the flushed read
 * buffers at once.
 *
 * The VS_COMMON_EVENT_CMD_RESPONSE event is sent to the client after the
 * flush has completed.
 */
#define VS_VOC_CMD_FLUSH_BUFFERS ( 0x00012E33 )

typedef struct vs_voc_cmd_flush_buffers_t vs_voc_cmd_flush_buffers_t;
struct vs_voc_cmd_flush_buffers_t
{
  uint32_t handle; /**< The open handle. */
  void* client_context; /**< The client provided data pointer for client's use. */
};

#endif /* __VS_VOC_H__ */

