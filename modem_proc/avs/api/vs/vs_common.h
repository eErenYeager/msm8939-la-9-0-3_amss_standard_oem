#ifndef __VS_COMMON_H__
#define __VS_COMMON_H__

/**
  @file  vs_common.h
  @brief This file contains Inline function definitions of VS
         API
*/

/*
  ============================================================================

   Copyright (C) 2012 QUALCOMM Technologies Incorporated. All Rights Reserved.
   QUALCOMM Proprietary and Confidential

  ============================================================================

                             Edit History

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/avs/api/vs/vs_common.h#1 $
  $Author: mplp4svc $

  when      who   what, where, why
  --------  ---   ------------------------------------------------------------
  12/05/12  sud   Initial revision

  ============================================================================
*/

/*----------------------------------------------------------------------------
  Include files for Module
----------------------------------------------------------------------------*/

#include "mmdefs.h"

#include "vs_errcodes.h"

/*----------------------------------------------------------------------------
  Export Macros
----------------------------------------------------------------------------*/

#ifdef __cplusplus
  #define VS_EXTERNAL extern "C"
#else
  #define VS_EXTERNAL extern
#endif /* __cplusplus */
  /**< This marks an external API that is intended for public use. */

#ifdef __cplusplus
  #define VS_INTERNAL extern "C"
#else
  #define VS_INTERNAL extern
#endif /* __cplusplus */
  /**< This marks an internal API that is intended for internal use. */

/****************************************************************************
 * TYPE DEFINITIONS                                                         *
 ****************************************************************************/

/**
 * Defined media types.
 */
#define VS_COMMON_MEDIA_ID_FR ( 0x00012E1C ) /**< GSM Full Rate. */
#define VS_COMMON_MEDIA_ID_HR ( 0x00012E1D ) /**< GSM Half Rate. */
#define VS_COMMON_MEDIA_ID_EFR ( 0x00012E1E ) /**< GSM Enhanced Full Rate. */
#define VS_COMMON_MEDIA_ID_AMR ( 0x00012E1F ) /**< Adaptive Multi-Rate. */
#define VS_COMMON_MEDIA_ID_AMRWB ( 0x00012E20 ) /**< Adaptive Multi-Rate Wideband. */

/*----------------------------------------------------------------------------
  Start of Structures
----------------------------------------------------------------------------*/

/**
 * The command response event structure.
 */
#define VS_COMMON_EVENT_CMD_RESPONSE ( 0x00012E23 )

typedef struct vs_common_event_cmd_response_t vs_common_event_cmd_response_t;
struct vs_common_event_cmd_response_t
{
  uint32_t cmd_id;
    /**< The command that completed. */
  uint32_t status_id;
    /**< The status code returned. Refer to vs_errcodes.h. */
  void* client_context;
    /**< The client context pointer returned from the asynchronous command call. */
};

/**
 * The feature is ready event structure.
 *
 * There is no parameter for this event.
 */
#define VS_COMMON_EVENT_READY ( 0x00012E24 )

/**
 * The feature is not ready event structure.
 *
 * There is no parameter for this event.
 */
#define VS_COMMON_EVENT_NOT_READY ( 0x00012E25 )

/*----------------------------------------------------------------------------
  Event Callback Function
----------------------------------------------------------------------------*/

/**
 * Event callback provided by a client of the voice driver.
 *
 * The client returns VS_EOK from the callback when the client was succesful
 * in queuing the event to its own thread context for processing.
 *
 * All other error codes (see vs_errcodes.h) indicate a problem.
 *
 * The event parameter pointer is valid only within the callback context.
 */
typedef uint32_t ( *vs_common_event_callback_fn_t ) (
  uint32_t event_id,
    /**< See VS_XXX_EVENT_YYY. */
  void* params,
    /**< A pointer to an associated event data structure. */
  uint32_t size,
    /**< The size of the event data structure. */
  void* session_context
    /**< The client provided session context pointer from the open command. */
);

/*----------------------------------------------------------------------------
  VS Call function
----------------------------------------------------------------------------*/

/**
 * Provides a single API entry-point into the voice driver.
 *
 * \param[in] cmd_id The command identifier to execute.
 * \param[in] params The command parameters.
 * \param[out] size The size of the command parameters in bytes.
 *
 * \return VS_EOK (0) when successful.
 */
VS_EXTERNAL uint32_t vs_call
(
  uint32_t cmd_id,
  void* params,
  uint32_t size
);

#endif /* __VS_COMMON_H__ */

