#ifndef __VS_VOCAMR_H__
#define __VS_VOCAMR_H__

/**
  @file vs_vocamr.h
  @brief This file contains the definitions of the constants,
         data structures, and interfaces for AMR vocoder
*/

/*
  ============================================================================

   Copyright (C) 2012 QUALCOMM Technologies Incorporated. All Rights Reserved.
   QUALCOMM Proprietary and Confidential

  ============================================================================

                             Edit History

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/avs/api/vs/vs_vocamr.h#1 $
  $Author: mplp4svc $

  when      who   what, where, why
  --------  ---   ------------------------------------------------------------
  12/06/12  sud   Initial revision

  ============================================================================
*/

/*----------------------------------------------------------------------------
  Include files for Module
----------------------------------------------------------------------------*/

#include "mmdefs.h"
#include "vs_errcodes.h"
#include "vs_common.h"


/*----------------------------------------------------------------------------
   Start of Enumerations
----------------------------------------------------------------------------*/

/**
 * AMR Vocoder Frame types. This shall be used to specify the contents of the
 * AMR Vocoder frame
 *
 * These Frame types are applicable to both Uplink and Downlink unless
 * explicitly stated otherwise.
 */
typedef enum vs_vocamr_frame_type_t
{
  VS_VOCAMR_FRAME_TYPE_SPEECH_GOOD = 0,
      /**< Good speech frame. */
  VS_VOCAMR_FRAME_TYPE_SPEECH_DEGRADED = 1,
      /**< Degraded speech frame. */
  VS_VOCAMR_FRAME_TYPE_ONSET = 2,
      /**<
        * ONSET - Announces the beginning of speech burst.
        * Does not contain information bits.
        */
  VS_VOCAMR_FRAME_TYPE_SPEECH_BAD = 3,
      /**< Corrupt speech frame (bad CRC). */
  VS_VOCAMR_FRAME_TYPE_SID_FIRST = 4,
      /**<
        * Marks the beginning of a comfort noise period (silence).
        * Does not contain information bits. /
        */
  VS_VOCAMR_FRAME_TYPE_SID_UPDATE = 5,
      /**<
        * Comfort noise frame.
        * It does contain inforamtion bits.
        */
  VS_VOCAMR_FRAME_TYPE_SID_BAD = 6,
      /**< Comfort noise frame (bad crc).    */
  VS_VOCAMR_FRAME_TYPE_NO_DATA = 7,
      /**<
        *  No useful information.
        *  Nothing to transmit.
        */
}
  vs_vocamr_frame_type_t;

/**
 * AMR codec mode. This denotes all the AMR codec bit-rates.
 */
typedef enum vs_vocamr_codec_mode_t
{
  VS_VOCAMR_CODEC_MODE_0475 = 0, /**< 4.75 kbps. */
  VS_VOCAMR_CODEC_MODE_0515 = 1, /**< 5.15 kbps. */
  VS_VOCAMR_CODEC_MODE_0590 = 2, /**< 5.90 kbps. */
  VS_VOCAMR_CODEC_MODE_0670 = 3, /**< 6.70 kbps. */
  VS_VOCAMR_CODEC_MODE_0740 = 4, /**< 7.40 kbps. */
  VS_VOCAMR_CODEC_MODE_0795 = 5, /**< 7.95 kbps. */
  VS_VOCAMR_CODEC_MODE_1020 = 6, /**< 10.2 kbps. */
  VS_VOCAMR_CODEC_MODE_1220 = 7, /**< 12.2 kbps. */
}
  vs_vocamr_codec_mode_t;

/*  End of Enumerations */


/*----------------------------------------------------------------------------
   Start of Structures
----------------------------------------------------------------------------*/

/**
 * AMR frame info structure
 */
typedef struct vs_vocamr_frame_info_t vs_vocamr_frame_info_t;
struct vs_vocamr_frame_info_t
{
  vs_vocamr_frame_type_t frame_type;
      /**< AMR frame type. */
  vs_vocamr_codec_mode_t codec_mode;
      /**< AMR codec mode. */
};

/**
 * Sets the codec mode of the AMR vocoder.
 */
#define VS_VOCAMR_CMD_SET_CODEC_MODE ( 0x00012E34 )

typedef struct vs_vocamr_cmd_set_codec_mode_t vs_vocamr_cmd_set_codec_mode_t;
struct vs_vocamr_cmd_set_codec_mode_t
{
  uint32_t handle;
      /**< The handle from the open command. */
  vs_vocamr_codec_mode_t codec_mode;
      /**< AMR codec mode. */
};

/**
 * Gets the codec mode of the AMR vocoder.
 */
#define VS_VOCAMR_CMD_GET_CODEC_MODE ( 0x00012E35 )

typedef struct vs_vocamr_cmd_get_codec_mode_t vs_vocamr_cmd_get_codec_mode_t;
struct vs_vocamr_cmd_get_codec_mode_t
{
  uint32_t handle;
      /**< The handle from the open command. */
  vs_vocamr_codec_mode_t* ret_codec_mode;
      /**< AMR codec mode returned back to the client. */
};

/**
 * Sets the DTX mode of the AMR vocoder.
 */
#define VS_VOCAMR_CMD_SET_DTX_MODE ( 0x00012E36 )

typedef struct vs_vocamr_cmd_set_dtx_mode_t vs_vocamr_cmd_set_dtx_mode_t;
struct vs_vocamr_cmd_set_dtx_mode_t
{
  uint32_t handle;
      /**< The handle from the open command. */
  bool_t enable_flag;
      /**<
        *  Set TRUE, when you want to enable DTX.
        *  Set FALSE, when you want to disable DTX.
        */
};

/**
 * Gets the DTX mode of the AMR vocoder.
 */
#define VS_VOCAMR_CMD_GET_DTX_MODE ( 0x00012E37 )

typedef struct vs_vocamr_cmd_get_dtx_mode_t vs_vocamr_cmd_get_dtx_mode_t;
struct vs_vocamr_cmd_get_dtx_mode_t
{
  uint32_t handle;
      /**< The handle from the open command. */
  bool_t* ret_enable_flag;
      /**<
        * TRUE, when DTX is enabled.
        * FALSE, when DTX is disabled.
        */
};

/*  End of Structures */

#endif /* __VS_VOCAMR_H__ */

