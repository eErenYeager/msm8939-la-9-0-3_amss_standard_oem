#ifndef __VS_VOCFR_H__
#define __VS_VOCFR_H__

/**
  @file vs_vocfr.h
  @brief This file contains the definitions of the constants,
    	 data structures, and interfaces for GSM Full Rate (FR)
    	 vocoder
*/

/*
  ============================================================================

   Copyright (C) 2012 QUALCOMM Technologies Incorporated. All Rights Reserved.
   QUALCOMM Proprietary and Confidential

  ============================================================================

                             Edit History

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/avs/api/vs/vs_vocfr.h#1 $
  $Author: mplp4svc $

  when      who   what, where, why
  --------  ---   ------------------------------------------------------------
  12/06/12  sud   Initial revision

  ============================================================================
*/

/*----------------------------------------------------------------------------
  Include files for Module
----------------------------------------------------------------------------*/

#include "mmdefs.h"
#include "vs_errcodes.h"
#include "vs_common.h"

/*----------------------------------------------------------------------------
  Start of Structures
----------------------------------------------------------------------------*/

/**
 * GSM Full Rate Vocoder frame info structure.
 */
typedef struct vs_vocfr_frame_info_t vs_vocfr_frame_info_t;
struct vs_vocfr_frame_info_t
{
  uint8_t bfi : 1;
      /**<
        * BIT(0): Bad Frame Indication; used for error concealment.
        * The BFI applies to downlink only. Unused for uplink.
        */
  uint8_t sid : 2;
      /**<
        * Applies to both uplink and downlink
        * BIT(1-2): SID frame indication:
        *      3  : reserved
        *      2  : valid SID frame
        *      1  : invalid SID frame
        *      0  : speech frame
        */
  uint8_t taf : 1;
      /**<
        * BIT(3): Time Alignment Flag (TAF):
        * 1: marks the position of the SID frame within the SACCH multiframe
        * 0: frame is not aligned
        * The TAF applies to downlink only. Unused for uplink.
        */
  uint8_t reserved_bit_4 : 1;
  uint8_t reserved_bit_5 : 1;
  uint8_t reserved_bit_6 : 1;
  uint8_t reserved_bit_7 : 1;
};

/**
 * Sets the DTX mode of the GSM FR Vocoder.
 */ 
#define VS_VOCFR_CMD_SET_DTX_MODE ( 0x00012E3E )

typedef struct vs_vocfr_cmd_set_dtx_mode_t vs_vocfr_cmd_set_dtx_mode_t;
struct vs_vocfr_cmd_set_dtx_mode_t
{
  uint32_t handle;
      /**< The handle from the open command. */
  bool_t enable_flag;
  /**<
   *  Set TRUE, when you want to enable DTX.
   *  Set FALSE, when you want to disable DTX.
   */
};

/**
 * Gets the DTX mode of the GSM FR Vocoder.
 */
#define VS_VOCFR_CMD_GET_DTX_MODE ( 0x00012E3F )

typedef struct vs_vocfr_cmd_get_dtx_mode_t vs_vocfr_cmd_get_dtx_mode_t;
struct vs_vocfr_cmd_get_dtx_mode_t
{
  uint32_t handle;
      /**< The handle from the open command. */
  bool_t* ret_enable_flag;
      /**<
       * TRUE, when DTX is enabled.
       * FALSE, when DTX is disabled.
       */
};

/*  End of Structures */


#endif /* __VS_VOCFR_H__ */


