#ifndef __VS_VOCAMRWB_H__
#define __VS_VOCAMRWB_H__

/**
  @file vs_vocamrwb.h
  @brief This file contains the definitions of the constants,
         data structures, and interfaces for AMR-WB vocoder
*/

/*
  ============================================================================

   Copyright (C) 2012 QUALCOMM Technologies Incorporated. All Rights Reserved.
   QUALCOMM Proprietary and Confidential

  ============================================================================

                             Edit History

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/avs/api/vs/vs_vocamrwb.h#1 $
  $Author: mplp4svc $

  when      who   what, where, why
  --------  ---   ------------------------------------------------------------
  12/06/12  sud   Initial revision

  ============================================================================
*/

/*----------------------------------------------------------------------------
  Include files for Module
----------------------------------------------------------------------------*/

#include "mmdefs.h"
#include "vs_errcodes.h"
#include "vs_common.h"

/*----------------------------------------------------------------------------
   Start of Enumerations
----------------------------------------------------------------------------*/

/**
 * AMR-WB Vocoder Frame types. This shall be used to specify the contents of the                                                                                          .
 * AMR-WB Vocoder frame.                                                                                                                                                  .
 *                                                                                                                                                                     .
 * These Frame types are applicable to both Uplink and Downlink unless                                                                    .
 * explicitly stated otherwise.                                                                                                                                        .
 */
typedef enum vs_vocamrwb_frame_type_t
{
  VS_VOCAMRWB_FRAME_TYPE_SPEECH_GOOD = 0,
      /**< Good speech frame. */
  VS_VOCAMRWB_FRAME_TYPE_SPEECH_PROBABLY_DEGRADED = 1,
      /**<
        * Speech frame with CRC OK, but there might.
        * be errors in class2 bits.
        */
  VS_VOCAMRWB_FRAME_TYPE_SPEECH_LOST = 2,
      /**< No frame recieved. */
  VS_VOCAMRWB_FRAME_TYPE_SPEECH_BAD = 3,
      /**< Corrupt speech frame (bad CRC). */
  VS_VOCAMRWB_FRAME_TYPE_SID_FIRST = 4,
  /**<
    * Marks the beginning of a comfort noise period (silence).
    * Does not contain information bits. /
    */
  VS_VOCAMRWB_FRAME_TYPE_SID_UPDATE = 5,
  /**<
    * Comfort noise frame.
    *  It does contain inforamtion bits.
    */
  VS_VOCAMRWB_FRAME_TYPE_SID_BAD = 6,
      /**< Comfort noise frame (bad CRC).    */
  VS_VOCAMRWB_FRAME_TYPE_NO_DATA = 7
  /**<
    * No useful information.
    *  Nothing to transmit.
    */
}
  vs_vocamrwb_frame_type_t;

/**
 * AMR-WB codec mode. This denotes all the AMR-WB codec
 * bit-rates.
 */
typedef enum vs_vocamrwb_codec_mode_t
{
  VS_VOCAMRWB_CODEC_MODE_0660 = 0, /**< AMR-WB 6.60 kbps. */
  VS_VOCAMRWB_CODEC_MODE_0885 = 1, /**< AMR-WB 8.85 kbps. */
  VS_VOCAMRWB_CODEC_MODE_1265 = 2, /**< AMR-WB 12.65 kbps. */
  VS_VOCAMRWB_CODEC_MODE_1425 = 3, /**< AMR-WB 14.25 kbps. */
  VS_VOCAMRWB_CODEC_MODE_1585 = 4, /**< AMR-WB 15.85 kbps. */
  VS_VOCAMRWB_CODEC_MODE_1825 = 5, /**< AMR-WB 18.25 kbps. */
  VS_VOCAMRWB_CODEC_MODE_1985 = 6, /**< AMR-WB 19.85 kbps. */
  VS_VOCAMRWB_CODEC_MODE_2305 = 7, /**< AMR-WB 23.05 kbps. */
  VS_VOCAMRWB_CODEC_MODE_2385 = 8  /**< AMR-WB 23.85 kbps. */
}
  vs_vocamrwb_codec_mode_t;

/*  End of Enumerations */

/*----------------------------------------------------------------------------
   Start of Structures
----------------------------------------------------------------------------*/

/**
 *  AMR-WB frame info structure.
 */

typedef struct vs_vocamrwb_frame_info_t vs_vocamrwb_frame_info_t;
struct vs_vocamrwb_frame_info_t
{
  vs_vocamrwb_frame_type_t frame_type;
      /**< AMR-WB frame type. */
  vs_vocamrwb_codec_mode_t codec_mode;
      /**< AMR-WB codec mode. */
};

/**
 * Sets the codec mode of the AMR-WB vocoder.
 */
#define VS_VOCAMRWB_CMD_SET_CODEC_MODE ( 0x00012E38 )

typedef struct vs_vocamrwb_cmd_set_codec_mode_t vs_vocamrwb_cmd_set_codec_mode_t;
struct vs_vocamrwb_cmd_set_codec_mode_t
{
  uint32_t handle;
      /**< The handle from the open command. */
  vs_vocamrwb_codec_mode_t codec_mode;
      /**< AMR-WB codec mode. */
};

/**
 * Gets the codec mode of the AMR-WB vocoder.
 */
#define VS_VOCAMRWB_CMD_GET_CODEC_MODE ( 0x00012E39 )

typedef struct vs_vocamrwb_cmd_get_codec_mode_t vs_vocamrwb_cmd_get_codec_mode_t;
struct vs_vocamrwb_cmd_get_codec_mode_t
{
  uint32_t handle;
      /**< The handle from the open command. */
  vs_vocamrwb_codec_mode_t* ret_codec_mode;
      /**< AMR-WB codec mode returned back to the client. */
};

/**
 * Sets the DTX mode of the AMR-WB vocoder.
 */
#define VS_VOCAMRWB_CMD_SET_DTX_MODE ( 0x00012E3A )

typedef struct vs_vocamrwb_cmd_set_dtx_mode_t vs_vocamrwb_cmd_set_dtx_mode_t;
struct vs_vocamrwb_cmd_set_dtx_mode_t
{
  uint32_t handle;
      /**< The handle from the open command. */
  bool_t enable_flag;
      /**<
       *  Set TRUE, when you want to enable DTX.
       *  Set FALSE, when you want to disable DTX.
       */
};

/**
 * Gets the DTX mode of the AMR-WB Vocoder.
 */
#define VS_VOCAMRWB_CMD_GET_DTX_MODE ( 0x00012E3B )

typedef struct vs_vocamrwb_cmd_get_dtx_mode_t vs_vocamrwb_cmd_get_dtx_mode_t;
struct vs_vocamrwb_cmd_get_dtx_mode_t
{
  uint32_t handle;
      /**< The handle from the open command. */
  bool_t* ret_enable_flag;
      /**<
       * TRUE, when DTX is enabled.
       * FALSE, when DTX is disabled.
       */
};

/*  End of Structures */

#endif /* __VS_VOCAMRWB_H__ */

