#ifndef __VS_H__
#define __VS_H__

/**
  @file vs.h
  @brief This is the public header file that Clients of Voice 
    	 Services (VS) should include. This file includes all
    	 other VS public header files.
*/

/*
  ============================================================================

   Copyright (C) 2012 QUALCOMM Technologies Incorporated. All Rights Reserved.
   QUALCOMM Proprietary and Confidential

  ============================================================================

                             Edit History

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/avs/api/vs/vs.h#1 $
  $Author: mplp4svc $

  when      who   what, where, why
  --------  ---   ------------------------------------------------------------
  12/05/12  sud   Initial revision

  ============================================================================
*/

/*----------------------------------------------------------------------------
 * VS VERSION
 *--------------------------------------------------------------------------*/

#define VS_VERSION_MAJOR ( 1 )
#define VS_VERSION_MINOR ( 0 )

/*----------------------------------------------------------------------------
 * Include files for Module
 *--------------------------------------------------------------------------*/

#include "vs_common.h"
#include "vs_voc.h"
#include "vs_vocamr.h"
#include "vs_vocamrwb.h"
#include "vs_vocfr.h"
#include "vs_vocefr.h"
#include "vs_vochr.h"

#endif /* __VS_H__ */

