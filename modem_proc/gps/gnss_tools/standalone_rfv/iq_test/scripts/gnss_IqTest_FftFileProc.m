% 
%$Header: 
%*****************************************************************************
%  Copyright (C) 2003 Qualcomm, Inc.
%
%                  Qualcomm, Inc.
%                  675 Campbell Technology Parkway
%                  Campbell, CA  95008
%
% This program is confidential and a trade secret of SnapTrack, Inc.  The
% receipt or possession of this program does not convey any rights to reproduce 
% or disclose its contents or to manufacture, use or sell anything that this 
% program describes in whole or in part, without the express written consent of
% SnapTrack, Inc.  The recipient and/or possessor of this program shall not 
% reproduce or adapt or disclose or use this program except as expressly 
% allowed by a written authorization from SnapTrack, Inc.
%
% *****************************************************************************
% Description:
%
% Version 1.00

% 
%*****************************************************************************
%*/

%
% Usage : cgps_IqTest_FftFileProc( 'FileName' )
%

function fftplot = cgps_fft(FileName);

% enum Enum1 
% {
%   COLLECT_MODE_WBIQ_BP1_BP2_2MHZ,
%   COLLECT_MODE_WBIQ_BP1_2MHZ,
%   COLLECT_MODE_WBIQ_BP2_2MHZ,
%   COLLECT_MODE_WBIQ_BP3_1MHZ,
%   COLLECT_MODE_FBIQ_BP4_20MHZ,
%   COLLECT_MODE_NBIQ_BP1_AUTO_1KHZ,
%   COLLECT_MODE_NBIQ_BP2_AUTO_1KHZ,
%   COLLECT_MODE_NBIQ_BP3_AUTO_1KHZ,
%   COLLECT_MODE_NBIQ_BP1_MANUAL_1KHZ,
%   COLLECT_MODE_NBIQ_BP2_MANUAL_1KHZ,
%   COLLECT_MODE_NBIQ_BP3_MANUAL_1KHZ,
%   COLLECT_MODE_NBIQ_BP4_MANUAL_1KHZ,
%   COLLECT_MODE_ADC1_16_POINT_8_MHZ,
%   COLLECT_MODE_ADC2_16_POINT_8_MHZ,
%   COLLECT_MODE_ADC1_ADC2_21_MHZ,
%   COLLECT_MODE_MAX
% } TEnum1;

% Default filename cgps_fft_collect.txt
if nargin < 1, FileName = 'gnss_fft_collect.bin'; end;    

fid = fopen(FileName,'r');

% Status Flag
% Peak FFT Power
% FFT Center Freq (Hz)
% Collect mode (0~4=>WBIQ (BP1&BP2, BP1, BP2, BP3, BP4)
%              (5~7=>NBIQ Autocenter (BP1, BP2, BP3)
%              (8-11=>NBIQ Manual (BP1, BP2, BP3, BP4)
%              (12-14)=>ADC1 Only, ADC1 Only, ADC1 & ADC2
% GLO R1 Freq (k value)
% GLO HW Chan (1 of 12)
% Number of 1k (1024) Samples 
% Total requested integrations

Config = fread(fid, 14, 'int32');
BinFileVersion = Config(1);
Flag = Config(2);
PeakFFTPower = Config(3);
FFTCenterFreq = Config(4);
CollectMode = Config(5);
GloR1FreqK = Config(6);
GloHWChan = Config(7);
SamplesPerInteg = Config(8);
SamplesPerInteg = SamplesPerInteg * 1024;
NumCollections = Config(9);
NBCenterFreqHz = Config(10);
BpOffsetFreqHz = Config(11);
TotalRelativeGain = Config(12);
FBIqMode = Config(13);
Reserved = Config(14);

Samples=fread(fid,inf, 'uint16');
fclose(fid);
N=length(Samples);

% Identify whether this is 1, 2, 20 MHz or 1 kHz sample collect
CollectIs1Mhz = 0;
CollectIs2Mhz = 0;
CollectIs20Mhz = 0;
CollectIs1Khz = 0;
CollectIsWb = 0;
CollectIsNb = 0;

switch CollectMode
    case {0, 1, 2}
        CollectIs2Mhz = 1;
        CollectIsWb = 1;
        FSample = 2046000;  % Hz
        FScale = 1/1000;
        FUnits = 'kHz';
    case 3
        CollectIs1Mhz = 1;
        CollectIsWb = 1;
        FSample = 1022000;  % Hz
        FScale = 1/1000;
        FUnits = 'kHz';
    case 4
        CollectIs20Mhz = 1;
        CollectIsWb = 1;
        FSample = 20460000;  % Hz
        FScale = 1/1000;
        FUnits = 'kHz';
    case {5, 6, 7 , 8, 9, 10, 11}
        CollectIs1Khz = 1;
        CollectIsNb = 1;
        FSample = 1000;     % Hz 
        FScale = 1;
        FUnits = 'Hz';
    otherwise
        fprintf(1, 'Invalid Collect Mode %u\n', CollectMode);
        return;
end

  % Get the Center Frequency
  CenterFreq = NBCenterFreqHz;

  % Need to <ESC> underscores with a '\' if we want to avoid incorrectly
  % subscripting these in the title.
  FileNameTitle = strrep( FileName, '_', '\_' );

  f = FSample / N * ([0:N-1] - N/2);
  % Plot the results
  fftplot = Samples * (-0.002);
  figure(4); plot( FScale * f, fftplot );
  title( sprintf( 'Power Spectrum - File: %s', FileNameTitle ) );
  grid on;zoom on;

  xlabel( sprintf( 'Frequency [%s]', FUnits ) );
  ylabel( 'Level [dB]' );
  axis([-FScale * FSample/2, FScale * FSample/2, -100, 0]);

  [maxval, maxindex] = max( fftplot );
  axistext = axis;
  xtext = 0.05 * (axistext(2) - axistext(1)) + axistext(1);
  ytext = 0.1 * (axistext(4) - axistext(3)) + axistext(3);
  hndl3 = text( xtext, ytext, sprintf( 'Peak Energy Frequency Offset = %.3f %s\nCenter Frequency = %.3f %s\n', ...
                                              FScale * f( maxindex ), FUnits, FScale * CenterFreq, FUnits ) );
  % Save figure for test documentation
  saveas(hndl3,'IqTestFig4.bmp','bmp');
  hold off;
