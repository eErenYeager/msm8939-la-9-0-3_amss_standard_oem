/*=============================================================================

                        Session Manager API Header File

GENERAL DESCRIPTION
  gps_types.h contains the type definitions for the data types used within 
  the GPS software. This file may require modification as part of a host
  plaform / compiler change to support variable length changes. 
 
 
EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS
  

Copyright (c) 2005, 2006 by Qualcomm Technologies INCORPORATED. All Rights Reserved.

Export of this technology or software is regulated by the U.S. Government.
Diversion contrary to U.S. law prohibited.

=============================================================================*/


/*=============================================================================

                           EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/gps/gnss/common/inc/gps_types.h#1 $ 
  $DateTime: 2015/01/27 06:42:19 $ 
  $Author: mplp4svc $ 


when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/13/07    ld     Lint support
05/03/06    ld     Initial version.

=============================================================================*/

#ifndef GPS_TYPES_H
#define GPS_TYPES_H

/*=============================================================================

                                INCLUDE FILES

=============================================================================*/

#include "customer.h"
#include "comdef.h"

typedef float  FLT;
typedef double DBL;

typedef	unsigned char	 U8;
typedef unsigned short   U16;
typedef unsigned long	 U32;
typedef unsigned long long U64;

typedef	signed char    S8;
typedef signed short   S16;
typedef signed long    S32;
typedef signed long long S64;

#define CGPS_MAX_UINT8  0xFF
#define CGPS_MAX_UINT16 0xFFFF
#define CGPS_MAX_UINT32 0xFFFFFFFF
#define CGPS_MAX_INT16 0x7FFF
#define CGPS_MIN_INT16 ( (int16) 0x8000 )
#define CGPS_MAX_INT32 0x7FFFFFFF
#define CGPS_MIN_INT32 ( (int32) 0x80000000 )
#define CGPS_MAX_INT64 ( 0x7FFFFFFFFFFFFFFFLL )
#define CGPS_MIN_INT64 ( 0x8000000000000000LL )

typedef void (* VFP) (void);

/* Structure used to process 32 bit complex quantities */
typedef struct
{
  int32 l_I;
  int32 l_Q;
} CplxS32;

/* Structure used to process 16 bit complex quantities */
typedef struct
{
  int16 x_I;
  int16 x_Q;
} CplxS16;

/* Structure used to process 8 bit complex quantities */
typedef struct
{
  int8 b_I;
  int8 b_Q;
} CplxS8;

/* AMSS defines TRUE/FALSE as integers (1 and 0).
   Lint complains if a boolean variable is assigned with TRUE or FALSE, because
   it expect "true" and "false" to be used for boolean values. This not 
   supported by the ADS ARM Compiler. So the best solution to avoid lint
   warnings is to typecast boolean to an unsigned char (as far as lint is concerned).
*/
#ifdef _lint
  typedef unsigned char boolean;
#endif

/* Structure used to handle watchdog reports */
typedef struct
{
  uint32 q_DogRptMsgId;          /* Dog report ID */
  uint32 q_DogRptTimePeriodMs;   /* Report Period in Msec */
} gps_DogReportDataType;

#endif /* GPS_TYPES_H */

