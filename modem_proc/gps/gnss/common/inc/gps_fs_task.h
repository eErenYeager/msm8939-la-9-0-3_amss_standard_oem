/* 
 * $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/gps/gnss/common/inc/gps_fs_task.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $ 
 ******************************************************************************
 *  Copyright (C) 1999-2012 Qualcomm Atheros, Inc.
 *
 *     This program is confidential and a trade secret of Qualcomm Atheros, Inc.
 * The receipt or possession of this program does not convey any rights to 
 * reproduce or disclose its contents or to manufacture, use or sell anything 
 * that this program describes in whole or in part, without the express written
 * consent of Qualcomm Atheros, Inc. The recipient and/or possessor of this 
 * program shall not reproduce or adapt or disclose or use this program except 
 * as expressly allowed by a written authorization from Qualcomm Atheros, Inc.
 *
 ******************************************************************************
 * Description:
 *	This file contains the prototype for the task declaration
 *
 ******************************************************************************
*/
#ifndef GPS_FS_TASK_H
#define GPS_FS_TASK_H


/*
 * Include files
*/
#include "fs_public.h"

/*
 * Constant definitions
*/

#define GPS_FS_TASK_EFS_PUT_SIZE       5000  /* IF the File size is greate use efs_put */

/* Message source task */
enum
{
  GPS_FS_TASK_DIAG_MESSAGE = 0,
  GPS_FS_TASK_MGP_MESSAGE,
  GPS_FS_TASK_MAX_MESSAGE
};


/*
 * Structure definitions
*/

/*
 * Local data declarations.
*/


/*
 * Local function prototypes
*/

/*===========================================================================
FUNCTION gps_NvInitDefaultWrites

DESCRIPTION
  This function is used to read/write default NV items.

RETURN VALUE
  None
  
DEPENDENCIES
  None
===========================================================================*/
void gps_NvInitDefaultWrites (void);

/*===========================================================================
FUNCTION gps_NvWriteDone

DESCRIPTION
  This function is used to handle NV write callback status. If there are any
  clients who issued write requests, they will be notified and any pending
  write requests will be subsequently handled.

RETURN VALUE
  None

DEPENDENCIES
  None
===========================================================================*/
void gps_NvWriteDone (void);

#endif /* GPS_FS_TASK_H */
