/*==============================================================================

                      G P S NV Item  R E G I S T R Y

  DESCRIPTION
    The GPS NV item Registry is a central location for configuration item storage.
    It serves as an interface to potentially various data sources. Currently its
    main data source is nonvolatile memory, via the EFS item interface.

  Copyright (c) 2010 - 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
==============================================================================*/

/*==============================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/gps/gnss/common/src/gps_nv_efs.c#1 $
  $DateTime: 2015/01/27 06:42:19 $
  $Author: mplp4svc $


when         who     what, where, why
----------   ---     -----------------------------------------------------------
05/03/10      gk      initial checkin
==============================================================================*/

#include "gps_variation.h"
#include "customer.h"
#include "comdef.h"
#include "aries_os_api.h"

#ifdef FEATURE_CGPS

  #include <math.h>
  #include <float.h>

  #include "fs_public.h"
  #include "gps_nv_efs.h"
  #include "msg.h"

/*===========================================================================
=============================================================================

   Private data structures and declarations
   
=============================================================================
===========================================================================*/
typedef struct
{
  cgps_nv_efs_reg_item_type item_start;
  cgps_nv_efs_reg_item_type item_end;
  char                      sz_pathname_prefix[10];
  const cgps_nv_efs_reg_item_struct* table_ptr;
} gps_nv_efs_subsys_data_type;

static gps_nv_efs_subsys_data_type gps_nv_efs_subsys_table[CGPS_NV_EFS_SUBSYS_MAX];

static const char* cgps_nv_efs_file_name[] =
{
  "cgps_sm.conf",
  "cgps_me.conf",
  "cgps_pe.conf",
  "cgps_pdcomms.conf",
  "cgps_tle_tlm.conf",
  "cgps_tle_xtm.conf",
  "cgps_sdp.conf",
  /* add new names above this line */
  "cgps_invalid.conf"
};


/*IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII

                            Private Functions
   
IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII*/
/*==============================================================================
  FUNCTION
    gps_nv_efs_find_subsys_entry

  DESCRIPTION
    Helper function to find the subsystem table entry for the given NV item.
 
  RETURNS
    index into the table
  
==============================================================================*/
static const gps_nv_efs_subsys_data_type* gps_nv_efs_find_subsys_entry(
  cgps_nv_efs_reg_item_type item_id
)
{
  const gps_nv_efs_subsys_data_type* entry_ptr = NULL;
  uint8 table_idx;
  for (table_idx = 0; 
       (table_idx < ARR_SIZE(gps_nv_efs_subsys_table)) && (entry_ptr == NULL);
       table_idx++)
  {
    if ((item_id >= gps_nv_efs_subsys_table[table_idx].item_start) &&
        (item_id <= gps_nv_efs_subsys_table[table_idx].item_end) &&
        (NULL != gps_nv_efs_subsys_table[table_idx].table_ptr))
    {
      /* found it */
      entry_ptr = &gps_nv_efs_subsys_table[table_idx];
      break;
    }
  }
  return entry_ptr;
}

/*==============================================================================
  FUNCTION
    cgps_nv_efs_reg_get_item_fname

  DESCRIPTION
    This is a local function
    Constructs the full pathname of item file name and returns the length of
    the filename char array and file_name_size. file_name_size value will be 0 if error.
    No bound checks for the array are done here. Make sure the checks are done before
    passing it to this function. function is intended to be a used within this file. 
 
  RETURNS
  
==============================================================================*/
static uint8 cgps_nv_efs_reg_get_item_fname(
  char* file_name, 
  uint8 file_name_buf_size, 
  boolean newline,
  const cgps_nv_efs_reg_item_struct *reg_items_ptr,
  const char* psz_pathname_prefix
)
{
  uint8 file_name_size = 0;
  /* item is already range checked in the calling function */
  if (reg_items_ptr->data_src == CGPS_NV_EFS_REG_ST_ITEM_EXTERNAL)
  {
    file_name_size = (uint8)GNSS_SNPRINTF( file_name, 
                                            file_name_buf_size, 
                                            "/nv/item_files/gps/%s/%s", 
                                            psz_pathname_prefix,
                                            reg_items_ptr->file_name );  
  }
  else
  {
    file_name_size = (uint8)GNSS_SNPRINTF( file_name, 
                                            file_name_buf_size, 
                                            "/cgps/nv/item_files/%s", 
                                            reg_items_ptr->file_name );
  }
  if (newline)
  {
    /* needed only between entries of config file */
    file_name_size = (uint8)GNSS_STRLCAT(file_name, "\n", file_name_buf_size);
    if (file_name_size >= file_name_buf_size)
    {
      MSG_MED("CGPS_NV_EFS_REG: could not add new line to file name file_size %d", 
              file_name_size, 0, 0);
      file_name_size = 0;
    }
  }
  return file_name_size;
}

/*==============================================================================
  FUNCTION
    cgps_nv_efs_init_internal

  DESCRIPTION
    Helper function for initialization required for NV items in the EFS.
    A config file is created and will hold the EFS file names of all NV items
    in the registry for the given subsystem.
 
  RETURN VALUE
    TRUE  - Success
    FALSE - Error during initialization
==============================================================================*/
static boolean cgps_nv_efs_init_internal(
  cgps_nv_efs_subsys_type type, 
  cgps_nv_efs_reg_item_type table_start,
  cgps_nv_efs_reg_item_type table_end,
  const cgps_nv_efs_reg_item_struct* cgps_nv_efs_reg_items,
  const char* psz_pathname_prefix
)
{
  int table_idx;
  int bytes_written = 0;
  int bytes2write = 0;
  uint8 file_size = 0;
  int file_handle;
  boolean ret_val = TRUE;
  char conf_dir_name1[]="/nv/";
  char conf_dir_name2[]="/nv/item_files/";  
  char conf_dir_name[]="/nv/item_files/conf/";
  const char* conf_file_name = NULL;
  char file_name[CGPS_MAX_NV_EFS_FILEPATH_SIZE];
  int result;
  struct fs_stat  temp_stat;

  MSG_MED("CGPS_NV_EFS: Registry Initialization", 0, 0, 0); 

  if (cgps_nv_efs_reg_items == NULL)
  {
    MSG_MED("CGPS_NV_EFS_REG: Null table pointer.", 0, 0, 0);
    ret_val = FALSE;
  } /* if null table*/
  else if (table_start > table_end)
  {
    MSG_MED("CGPS_NV_EFS_REG: invalid range: %d, %d", table_start, table_end, 0);
    ret_val = FALSE;
  }
  else
  {
    /* create conf directory if it doesn't exist */
    result = efs_mkdir(conf_dir_name1, ACCESSPERMS);  
    if (result == -1)
    {
      MSG_MED("CGPS_NV_EFS: efs_mkdir /nv returned -1", 0, 0, 0); 
    }
    result = efs_mkdir(conf_dir_name2, ACCESSPERMS);  
    if (result == -1)
    {
      MSG_MED("CGPS_NV_EFS: efs_mkdir /nv/item_files returned -1", 0, 0, 0); 
    }
    result = efs_mkdir(conf_dir_name, ACCESSPERMS);  
    if (result == -1)
    {
      MSG_MED("CGPS_NV_EFS: efs_mkdir /nv/item_files/conf returned -1", 0, 0, 0); 
    }

    switch (type)
    {
      case CGPS_NV_EFS_SM:
      case CGPS_NV_EFS_ME:
      case CGPS_NV_EFS_PE:
      case CGPS_NV_EFS_PDCOMM:
      case CGPS_NV_EFS_TLE_TLM:
      case CGPS_NV_EFS_TLE_XTM:
      case CGPS_NV_EFS_SDP:
        conf_file_name = cgps_nv_efs_file_name[type];
        break;
      default:
        MSG_ERROR("CGPS_NV_EFS: No config file available for this type %d", type, 0, 0); 
        ret_val = FALSE;
        break;
    }
    if (conf_file_name != NULL) 
    {
      (void)GNSS_SNPRINTF(file_name, sizeof(file_name), "%s%s", 
                           conf_dir_name, conf_file_name);      

      /* Check if conf file already exists and it has got data in it.
         If conf file does not exist or if it exists and has 0 bytes
         then open the conf file and write the absolute path of NV items in it.
         Conf file is required to access NV items via QXDM/QPST interface.
         If just EFS item file is required conf file need not be updated
         with absolute path. */
      if ( efs_stat(file_name, &temp_stat) < 0 || temp_stat.st_size == 0 )
      {
     
        /* create new conf file. */
        file_handle = efs_open(file_name, O_CREAT|O_WRONLY|O_TRUNC, ACCESSPERMS);
  
        if (file_handle == -1)
        {
          MSG_ERROR("CGPS_NV_EFS: Error %d creating config file", efs_errno, 0, 0);
          ret_val = FALSE;
        }
        else
        {
          /* for convenience, traverse the table backward */
          table_idx = table_end - table_start;
          while ((table_idx >= 0) && (ret_val != FALSE))
          {
            /* Generate the absolute path for the file entries. */
            file_size = cgps_nv_efs_reg_get_item_fname(
                          file_name, sizeof(file_name), TRUE,
                          &cgps_nv_efs_reg_items[table_idx],
                          psz_pathname_prefix);
            bytes2write = file_size;
  
            /* Since "It is permitted for efs_write to write fewer bytes than
               were requested, even if space is available" , the following while
               loop is designed to expect data not to be written all in one go. */
            while ((bytes2write > 0) && (ret_val != FALSE))
            {
              if( (sizeof(file_name) <= file_size-bytes2write) ||
                  ((file_size-bytes2write) < 0))
              {
                 MSG_ERROR("CGPS_NV_EFS:Incorrect File Name length. Limit is :%d, Current size:%d", 
                            sizeof(file_name), file_size-bytes2write, 0);
                 ret_val = FALSE;
              }
              else
              {
              
                bytes_written = efs_write(file_handle, 
                                         &file_name[file_size-bytes2write],
                                         bytes2write);
              }
              if (bytes_written < 0)
              {
                MSG_ERROR("CGPS_NV_EFS: Error %d writing to config file", 
                          efs_errno, 0, 0);
                ret_val = FALSE;
              }
              else
              {
                bytes2write -= bytes_written;
              }
            }/* while not done writing */
            table_idx--; /* next entry up */
          } /* while table index is valid */
  
          result = efs_close(file_handle);
          if (result == -1)
          {
            /* Error while closing */
            MSG_ERROR("CGPS_NV_EFS: Error %d closing config file", efs_errno, 0, 0);
            ret_val = FALSE;
          } /* efs_close check */
        } /* efs_open check */
      }
    } /* ret_val = TRUE*/

    if (ret_val != FALSE)
    {
      /* bookkeeping */
      gps_nv_efs_subsys_table[type].item_start = table_start;
      gps_nv_efs_subsys_table[type].item_end = table_end;
      gps_nv_efs_subsys_table[type].table_ptr = cgps_nv_efs_reg_items;
      GNSS_STRLCPY(gps_nv_efs_subsys_table[type].sz_pathname_prefix,
                  psz_pathname_prefix, 
                  sizeof(gps_nv_efs_subsys_table[type].sz_pathname_prefix));
    }
  } /* if cgps_nv_efs_reg_items != NULL */

  return  ret_val;
}

/*==============================================================================
  FUNCTION
    cgps_nv_efs_item_read_internal
 
  DESCRIPTION
    Read the specified item from EFS2 special items file. If the
    item is not found, a default value is returned. The function logs the read
    write activity.
 
  RETURN VALUE
    CGPS_NV_EFS_REG_RW_STAT_OK         - Success
    CGPS_NV_EFS_REG_RW_STAT_RD_DEFAULT - Returning default value
    CGPS_NV_EFS_REG_RW_STAT_RD_ERR     - Error during read operation
    CGPS_NV_EFS_REG_RW_STAT_INVALID    - Invalid paramters
==============================================================================*/
static cgps_nv_efs_reg_rw_stat_enum_type cgps_nv_efs_item_read_internal(
  cgps_nv_efs_reg_item_type item,
  uint32 data_size,
  void *data_ptr
)
{
  char file_name[CGPS_MAX_NV_EFS_FILEPATH_SIZE]; /* file name with absolute address */
  cgps_nv_efs_reg_rw_stat_enum_type status = CGPS_NV_EFS_REG_RW_STAT_INVALID;    
  struct fs_stat temp_buf; /* Temp buff required for efs_stat() */
  const gps_nv_efs_subsys_data_type* entry_ptr = NULL;
  const cgps_nv_efs_reg_item_struct* item_reg_ptr;

  if ((entry_ptr = gps_nv_efs_find_subsys_entry(item)) == NULL)
  {
    MSG_ERROR("CGPS_NV_EFS_READ: subsystem not initialized for item:%d", item, 0, 0); 
  }
  else
  {
    item_reg_ptr = &entry_ptr->table_ptr[(int)(item - entry_ptr->item_start)];
    if (data_size != item_reg_ptr->size)
    {
      MSG_ERROR("CGPS_NV_EFS_READ: Destination buffer size incorrect", 0, 0, 0);
    }
    else
    {
      /* Generate the absolute path for the file */
      cgps_nv_efs_reg_get_item_fname(file_name, sizeof(file_name), FALSE, 
                                     item_reg_ptr, entry_ptr->sz_pathname_prefix);
  
      /* Check if the file is present or not. */
      status = CGPS_NV_EFS_REG_RW_STAT_OK;
      if (efs_stat(file_name,&temp_buf) == -1)
      {
        MSG_MED("CGPS_NV_EFS_READ: Item file does not exist", 0, 0, 0); 
        status = CGPS_NV_EFS_REG_RW_STAT_RD_ERR;
      }

      else if (efs_get(file_name, data_ptr, data_size) == -1)
      {
        MSG_ERROR("CGPS_NV_EFS_READ: EFS read error. Returning error.", 0, 0, 0);
        status = CGPS_NV_EFS_REG_RW_STAT_RD_ERR;
      }
    }
  } /* if cgps_nv_efs_reg_items != NULL */
  return status;
}

/*==============================================================================
  FUNCTION
    cgps_nv_efs_reg_item_write_internal
 
  DESCRIPTION
    Write the specified item value to EFS2 special items file. The function
    logs the write activity.
 
  RETURN VALUE
    CGPS_NV_EFS_REG_RW_STAT_OK     - Success
    CGPS_NV_EFS_REG_RW_STAT_WR_ERR - Error during write operation
    CGPS_NV_EFS_REG_RW_STAT_INVALID - Invalid paramters 
==============================================================================*/
static cgps_nv_efs_reg_rw_stat_enum_type cgps_nv_efs_reg_item_write_internal(
  cgps_nv_efs_reg_item_type item, 
  uint32 data_size,
  const void *data_ptr
)
{
  char file_name[CGPS_MAX_NV_EFS_FILEPATH_SIZE]; /* file name with absolute address */
  cgps_nv_efs_reg_rw_stat_enum_type status = CGPS_NV_EFS_REG_RW_STAT_INVALID;    
  struct fs_stat temp_buf; /* Temp buff required for efs_stat() */
  const gps_nv_efs_subsys_data_type* entry_ptr = NULL;
  const cgps_nv_efs_reg_item_struct* item_reg_ptr;

  if ((entry_ptr = gps_nv_efs_find_subsys_entry(item)) == NULL)
  {
    MSG_ERROR("CGPS_NV_EFS_WRITE: subsystem not initialized for item:%d", item, 0, 0); 
  }
  else
  {
    item_reg_ptr = &entry_ptr->table_ptr[(int)(item - entry_ptr->item_start)];
    if (data_size != item_reg_ptr->size)
    {
      MSG_ERROR("CGPS_NV_EFS_WRITE: Destination buffer size incorrect", 0, 0, 0);
    }
    else
    {
      /* Generate the absolute path for the file */
      cgps_nv_efs_reg_get_item_fname(file_name, sizeof(file_name), FALSE, 
                                     item_reg_ptr, entry_ptr->sz_pathname_prefix);

      /* Check if the file is present or not. */
      status = CGPS_NV_EFS_REG_RW_STAT_OK;
      if (efs_stat(file_name,&temp_buf) != -1)
      {
        MSG_MED("CGPS_NV_EFS_WRITE: Item file exists", 0, 0, 0); 
      }

      if (efs_put(file_name, (void*) data_ptr, data_size, 
                  O_CREAT|O_AUTODIR|O_TRUNC, ACCESSPERMS) == -1)
      {
        MSG_ERROR("CGPS_NV_EFS_WRITE: EFS write error.", 0, 0, 0);
        status = CGPS_NV_EFS_REG_RW_STAT_WR_ERR;
      }
    }
  } /* if cgps_nv_efs_reg_items != NULL */
  return status;
}



/*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

                            Public Functions
   
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*/

/*==============================================================================
  FUNCTION
    cgps_nv_efs_init

  DESCRIPTION
    Performs initialization required for CGPS NV items in the EFS.
    Currently this means generating the config file for diag tools 
 
    Requires a table of the NV items that need to be input to the config file
 
  RETURN VALUE
    TRUE  - Success
    FALSE - Error during initialization
==============================================================================*/
boolean cgps_nv_efs_init(cgps_nv_efs_subsys_type type, 
                         cgps_nv_efs_reg_item_type table_start,
                         cgps_nv_efs_reg_item_type table_end,
                         const cgps_nv_efs_reg_item_struct* cgps_nv_efs_reg_items) 
{
  boolean b_init;

  MSG_MED("CGPS_NV_EFS: Registry Initialization", 0, 0, 0); 
  b_init = cgps_nv_efs_init_internal(type, table_start, table_end,
                                     cgps_nv_efs_reg_items, "cgps");

  return  b_init;
}  /* end of fucntion cgps_nv_efs_init*/

/*==============================================================================
  FUNCTION
    cgps_nv_efs_reg_item_read
 
  DESCRIPTION
    Read the specified item from EFS2 special items file. If the
    item is not found, a default value is returned. The function logs the read
    write activity.
 
  RETURN VALUE
    CGPS_NV_EFS_REG_RW_STAT_OK         - Success
    CGPS_NV_EFS_REG_RW_STAT_RD_DEFAULT - Returning default value
    CGPS_NV_EFS_REG_RW_STAT_RD_ERR     - Error during read operation
    CGPS_NV_EFS_REG_RW_STAT_INVALID    - Invalid paramters
==============================================================================*/
cgps_nv_efs_reg_rw_stat_enum_type cgps_nv_efs_reg_item_read(cgps_nv_efs_reg_item_type const item, 
                                                            uint32 data_size,
                                                            void *data_ptr,
                                                            cgps_nv_efs_reg_item_type table_start,
                                                            cgps_nv_efs_reg_item_type table_end,
                                                            const cgps_nv_efs_reg_item_struct  *cgps_nv_efs_reg_items
                                                           )
{
  cgps_nv_efs_reg_rw_stat_enum_type status = CGPS_NV_EFS_REG_RW_STAT_INVALID;    

  MSG_MED("CGPS_NV_EFS_REG: Registry Item Read Started for item:%d", item, 0, 0); 

  if (cgps_nv_efs_reg_items == NULL)
  {
    MSG_ERROR("CGPS_NV_EFS_READ: NULL Registry pointer for item:%d", item, 0, 0); 
  }
  else if ((item < table_start) || (item > table_end))
  {
    MSG_ERROR("CGPS_NV_EFS_REG: Invalid item index, start:%d, end:%d, item:%d",
              (int) table_start, (int) table_end, (int) item);
  }
  else
  {
    status = cgps_nv_efs_item_read_internal(item, data_size, data_ptr);
  }
  return status;
}

/*==============================================================================
  FUNCTION
    cgps_nv_efs_reg_item_write
 
  DESCRIPTION
    Write the specified item value to EFS2 special items file. The function
    logs the write activity.
 
  RETURN VALUE
    CGPS_NV_EFS_REG_RW_STAT_OK     - Success
    CGPS_NV_EFS_REG_RW_STAT_WR_ERR - Error during write operation
    CGPS_NV_EFS_REG_RW_STAT_INVALID - Invalid paramters 
==============================================================================*/
cgps_nv_efs_reg_rw_stat_enum_type cgps_nv_efs_reg_item_write(cgps_nv_efs_reg_item_type const item, 
                                                             uint32 data_size,
                                                             const void *data_ptr,
                                                             cgps_nv_efs_reg_item_type table_start,
                                                             cgps_nv_efs_reg_item_type table_end,
                                                             const cgps_nv_efs_reg_item_struct  *cgps_nv_efs_reg_items)
{
  cgps_nv_efs_reg_rw_stat_enum_type status = CGPS_NV_EFS_REG_RW_STAT_INVALID;    

  MSG_MED("CGPS_NV_EFS_REG: Registry Item Write Started for item:%d", item, 0, 0); 

  if (cgps_nv_efs_reg_items == NULL)
  {
    MSG_MED("CGPS_NV_EFS_WRITE: Registry Item null Table for Itemm:%d", item, 0, 0); 
  } /* null table*/
  else if ((item < table_start) || (item > table_end))
  {
    MSG_ERROR("CGPS_NV_EFS_WRITE: Invalid item index, start:%d, end:%d, item:%d",
              (int) table_start, (int) table_end, (int) item);
  }
  else
  {
    status = cgps_nv_efs_reg_item_write_internal(item, data_size, data_ptr);
  }
  return status;
} /* end of write function */

/*==============================================================================
  FUNCTION
    gps_nv_efs_init

  DESCRIPTION
    Performs initialization required for NV items in the EFS.
    Currently this means generating the config file for diag tools.
    Requires a table of the NV items that need to be input to the config file.
 
    Unlike cgps_nv_efs_init() which uses the hardcoded name "cgps" in the NV
    item's pathnames, the input parameter psz_pathname_prefix in this function
    allows caller to specify the name to be used as the pathname prefix.

  RETURN VALUE
    TRUE  - Success
    FALSE - Error during initialization
==============================================================================*/
boolean gps_nv_efs_init (cgps_nv_efs_subsys_type subsys, 
                         cgps_nv_efs_reg_item_type table_start,
                         cgps_nv_efs_reg_item_type table_end,
                         const cgps_nv_efs_reg_item_struct* cgps_nv_efs_reg_items,
                         const char* psz_pathname_prefix)
{
  MSG_MED("GPS_NV_EFS: Registry Init Started for subsystem:%d", subsys, 0, 0); 
  return (cgps_nv_efs_init_internal(subsys, table_start, table_end,
                                    cgps_nv_efs_reg_items, 
                                    psz_pathname_prefix));
}

/*==============================================================================
  FUNCTION
    gps_nv_efs_reg_item_read

  DESCRIPTION
    Reads the specified item from EFS2 special items file.
 
    The subsystem that the given item belongs to must have already been
    initialized, or this function will return CGPS_NV_EFS_REG_RW_STAT_INVALID.
 
    If the item EFS file is not found, the default value for the item is returned.
 
  RETURN VALUE
    CGPS_NV_EFS_REG_RW_STAT_OK         - Success
    CGPS_NV_EFS_REG_RW_STAT_RD_ERR     - Error during read operation. if file
                                         does not exist, returns an error
    CGPS_NV_EFS_REG_RW_STAT_INVALID    - Invalid paramters
==============================================================================*/
cgps_nv_efs_reg_rw_stat_enum_type gps_nv_efs_reg_item_read(
  cgps_nv_efs_reg_item_type const item, 
  uint32 data_size,
  void *data_ptr
)
{
  MSG_MED("GPS_NV_EFS: Registry Item Write Started for item:%d", item, 0, 0); 
  return (cgps_nv_efs_item_read_internal(item, data_size, data_ptr));
}

/*==============================================================================
  FUNCTION
    gps_nv_efs_reg_item_write

  DESCRIPTION
    Write the specified item value to EFS2 special items file.
 
    The subsystem that the given item belongs to must have already been
    initialized, or this function will return CGPS_NV_EFS_REG_RW_STAT_INVALID.
 
  RETURN VALUE
    CGPS_NV_EFS_REG_RW_STAT_OK     - Success
    CGPS_NV_EFS_REG_RW_STAT_WR_ERR - Error during write operation
    CGPS_NV_EFS_REG_RW_STAT_INVALID - Invalid paramters
==============================================================================*/
cgps_nv_efs_reg_rw_stat_enum_type gps_nv_efs_reg_item_write(
  cgps_nv_efs_reg_item_type const item, 
  uint32 data_size,
  const void *data_ptr
)
{
  MSG_MED("GPS_NV_EFS: Registry Item Read Started for item:%d", item, 0, 0); 
  return (cgps_nv_efs_reg_item_write_internal(item, data_size, data_ptr));
}

#endif /* FEATURE_CGPS */
