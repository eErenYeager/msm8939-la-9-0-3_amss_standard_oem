/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                             GPS Rom Almanac Module

GENERAL DESCRIPTION
  This module contains Default Rom Almanac helper function that is used 
  within the GPS sub-system.

EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS
  

  Copyright (c) 2005-2012 by Qualcomm Technologies INCORPORATED. All Rights Reserved.
  Copyright (c) 2013-2016 Qualcomm Technologies, Inc. 
  All Rights Reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.


Export of this technology or software is regulated by the U.S. Government.
Diversion contrary to U.S. law prohibited.

Version Control

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/gps/gnss/common/src/gps_rom_almanac.c#2 $ 
  $DateTime: 2016/10/06 06:11:01 $ 
  $Author: c_prashk $ 

when       who     what, where, why
--------   ---     ----------------------------------------------------------
2016-08-23          From: xtra3grc_2016gi.txt
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*
 * Include files
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "customer.h"
#include "comdef.h"
#include "gnss_common.h"
#include "err.h"
#include "msg.h"

/*
 * Constant definitions
*/

/*
 * Local function prototypes
*/


/*
 ******************************************************************************
 * gps_GetGpsRomAlmanac
 *
 * Function description:
 *
 * 	gps_GetGpsRomAlmanac is a helper function used to retreive the default GPS
 *	ROM almanac.
 *
 * Parameters: 
 *
 *  pz_GpsRomAlm - Pointer to the GPS ROM almanac structure
 *
 * Return value: 
 *
 *  void
 *
 ******************************************************************************
*/

void gps_GetGpsRomAlmanac( gps_AlmStructType **pz_RomAlm )
{

  if ( (NULL == pz_RomAlm) || (NULL == *pz_RomAlm) )
  {    
    MSG(MSG_SSID_MGPPE,MSG_LEGACY_ERROR, 
        "gps_GetGpsRomAlmanac: NULL input pointer");
    return;
  }
  pz_RomAlm[0]->u_Sv = 1 ;
  pz_RomAlm[0]->u_Health = 0 ;
  pz_RomAlm[0]->u_Toa = 99 ;
  pz_RomAlm[0]->w_E = 12260 ;
  pz_RomAlm[0]->w_DeltaI = 3800 ;
  pz_RomAlm[0]->w_OmegaDot = 64851 ;
  pz_RomAlm[0]->q_SqrtA = 10554547ul ;
  pz_RomAlm[0]->q_Omega0 = 6660017ul ;
  pz_RomAlm[0]->q_Omega = 1414225ul ;
  pz_RomAlm[0]->q_M0 = 3630065ul ;
  pz_RomAlm[0]->w_Af0 = 33 ;
  pz_RomAlm[0]->w_Af1 = 0 ;
  pz_RomAlm[0]->w_GpsWeek = 1911 ;

  pz_RomAlm[1]->u_Sv = 2 ;
  pz_RomAlm[1]->u_Health = 0 ;
  pz_RomAlm[1]->u_Toa = 99 ;
  pz_RomAlm[1]->w_E = 33312 ;
  pz_RomAlm[1]->w_DeltaI = 258 ;
  pz_RomAlm[1]->w_OmegaDot = 64832 ;
  pz_RomAlm[1]->q_SqrtA = 10554506ul ;
  pz_RomAlm[1]->q_Omega0 = 6533296ul ;
  pz_RomAlm[1]->q_Omega = 11194019ul ;
  pz_RomAlm[1]->q_M0 = 4771502ul ;
  pz_RomAlm[1]->w_Af0 = 588 ;
  pz_RomAlm[1]->w_Af1 = 2047 ;
  pz_RomAlm[1]->w_GpsWeek = 1911 ;

  pz_RomAlm[2]->u_Sv = 3 ;
  pz_RomAlm[2]->u_Health = 0 ;
  pz_RomAlm[2]->u_Toa = 99 ;
  pz_RomAlm[2]->w_E = 291 ;
  pz_RomAlm[2]->w_DeltaI = 2875 ;
  pz_RomAlm[2]->w_OmegaDot = 64839 ;
  pz_RomAlm[2]->q_SqrtA = 10554527ul ;
  pz_RomAlm[2]->q_Omega0 = 9440526ul ;
  pz_RomAlm[2]->q_Omega = 15877877ul ;
  pz_RomAlm[2]->q_M0 = 2888317ul ;
  pz_RomAlm[2]->w_Af0 = 1961 ;
  pz_RomAlm[2]->w_Af1 = 2047 ;
  pz_RomAlm[2]->w_GpsWeek = 1911 ;

  /* No almanac for PRN 4 */
  pz_RomAlm[3]->u_Sv = C_GPS_SV_ALM_INVALID ;

  pz_RomAlm[4]->u_Sv = 5 ;
  pz_RomAlm[4]->u_Health = 0 ;
  pz_RomAlm[4]->u_Toa = 99 ;
  pz_RomAlm[4]->w_E = 9835 ;
  pz_RomAlm[4]->w_DeltaI = 713 ;
  pz_RomAlm[4]->w_OmegaDot = 64827 ;
  pz_RomAlm[4]->q_SqrtA = 10554413ul ;
  pz_RomAlm[4]->q_Omega0 = 9405518ul ;
  pz_RomAlm[4]->q_Omega = 1299939ul ;
  pz_RomAlm[4]->q_M0 = 11411615ul ;
  pz_RomAlm[4]->w_Af0 = 1948 ;
  pz_RomAlm[4]->w_Af1 = 1 ;
  pz_RomAlm[4]->w_GpsWeek = 1911 ;

  pz_RomAlm[5]->u_Sv = 6 ;
  pz_RomAlm[5]->u_Health = 0 ;
  pz_RomAlm[5]->u_Toa = 99 ;
  pz_RomAlm[5]->w_E = 497 ;
  pz_RomAlm[5]->w_DeltaI = 3769 ;
  pz_RomAlm[5]->w_OmegaDot = 64849 ;
  pz_RomAlm[5]->q_SqrtA = 10554784ul ;
  pz_RomAlm[5]->q_Omega0 = 6637562ul ;
  pz_RomAlm[5]->q_Omega = 13171350ul ;
  pz_RomAlm[5]->q_M0 = 4328404ul ;
  pz_RomAlm[5]->w_Af0 = 249 ;
  pz_RomAlm[5]->w_Af1 = 1 ;
  pz_RomAlm[5]->w_GpsWeek = 1911 ;

  pz_RomAlm[6]->u_Sv = 7 ;
  pz_RomAlm[6]->u_Health = 0 ;
  pz_RomAlm[6]->u_Toa = 99 ;
  pz_RomAlm[6]->w_E = 20895 ;
  pz_RomAlm[6]->w_DeltaI = 3822 ;
  pz_RomAlm[6]->w_OmegaDot = 64843 ;
  pz_RomAlm[6]->q_SqrtA = 10554543ul ;
  pz_RomAlm[6]->q_Omega0 = 15094131ul ;
  pz_RomAlm[6]->q_Omega = 9721715ul ;
  pz_RomAlm[6]->q_M0 = 1777389ul ;
  pz_RomAlm[6]->w_Af0 = 453 ;
  pz_RomAlm[6]->w_Af1 = 2047 ;
  pz_RomAlm[6]->w_GpsWeek = 1911 ;

  pz_RomAlm[7]->u_Sv = 8 ;
  pz_RomAlm[7]->u_Health = 0 ;
  pz_RomAlm[7]->u_Toa = 99 ;
  pz_RomAlm[7]->w_E = 3548 ;
  pz_RomAlm[7]->w_DeltaI = 3575 ;
  pz_RomAlm[7]->w_OmegaDot = 64839 ;
  pz_RomAlm[7]->q_SqrtA = 10554762ul ;
  pz_RomAlm[7]->q_Omega0 = 3825595ul ;
  pz_RomAlm[7]->q_Omega = 14051778ul ;
  pz_RomAlm[7]->q_M0 = 11447949ul ;
  pz_RomAlm[7]->w_Af0 = 1998 ;
  pz_RomAlm[7]->w_Af1 = 0 ;
  pz_RomAlm[7]->w_GpsWeek = 1911 ;

  pz_RomAlm[8]->u_Sv = 9 ;
  pz_RomAlm[8]->u_Health = 0 ;
  pz_RomAlm[8]->u_Toa = 99 ;
  pz_RomAlm[8]->w_E = 1079 ;
  pz_RomAlm[8]->w_DeltaI = 1973 ;
  pz_RomAlm[8]->w_OmegaDot = 64844 ;
  pz_RomAlm[8]->q_SqrtA = 10554871ul ;
  pz_RomAlm[8]->q_Omega0 = 12213950ul ;
  pz_RomAlm[8]->q_Omega = 5687821ul ;
  pz_RomAlm[8]->q_M0 = 8947322ul ;
  pz_RomAlm[8]->w_Af0 = 203 ;
  pz_RomAlm[8]->w_Af1 = 2 ;
  pz_RomAlm[8]->w_GpsWeek = 1911 ;

  pz_RomAlm[9]->u_Sv = 10 ;
  pz_RomAlm[9]->u_Health = 0 ;
  pz_RomAlm[9]->u_Toa = 99 ;
  pz_RomAlm[9]->w_E = 3538 ;
  pz_RomAlm[9]->w_DeltaI = 2913 ;
  pz_RomAlm[9]->w_OmegaDot = 64840 ;
  pz_RomAlm[9]->q_SqrtA = 10554618ul ;
  pz_RomAlm[9]->q_Omega0 = 9430038ul ;
  pz_RomAlm[9]->q_Omega = 9293154ul ;
  pz_RomAlm[9]->q_M0 = 14482356ul ;
  pz_RomAlm[9]->w_Af0 = 1955 ;
  pz_RomAlm[9]->w_Af1 = 2046 ;
  pz_RomAlm[9]->w_GpsWeek = 1911 ;

  pz_RomAlm[10]->u_Sv = 11 ;
  pz_RomAlm[10]->u_Health = 0 ;
  pz_RomAlm[10]->u_Toa = 99 ;
  pz_RomAlm[10]->w_E = 35190 ;
  pz_RomAlm[10]->w_DeltaI = 58045 ;
  pz_RomAlm[10]->w_OmegaDot = 64796 ;
  pz_RomAlm[10]->q_SqrtA = 10554595ul ;
  pz_RomAlm[10]->q_Omega0 = 5636365ul ;
  pz_RomAlm[10]->q_Omega = 4170387ul ;
  pz_RomAlm[10]->q_M0 = 2068806ul ;
  pz_RomAlm[10]->w_Af0 = 1355 ;
  pz_RomAlm[10]->w_Af1 = 2047 ;
  pz_RomAlm[10]->w_GpsWeek = 1911 ;

  pz_RomAlm[11]->u_Sv = 12 ;
  pz_RomAlm[11]->u_Health = 0 ;
  pz_RomAlm[11]->u_Toa = 99 ;
  pz_RomAlm[11]->w_E = 12687 ;
  pz_RomAlm[11]->w_DeltaI = 7995 ;
  pz_RomAlm[11]->w_OmegaDot = 64847 ;
  pz_RomAlm[11]->q_SqrtA = 10554629ul ;
  pz_RomAlm[11]->q_Omega0 = 1194278ul ;
  pz_RomAlm[11]->q_Omega = 2101738ul ;
  pz_RomAlm[11]->q_M0 = 171454ul ;
  pz_RomAlm[11]->w_Af0 = 407 ;
  pz_RomAlm[11]->w_Af1 = 0 ;
  pz_RomAlm[11]->w_GpsWeek = 1911 ;

  pz_RomAlm[12]->u_Sv = 13 ;
  pz_RomAlm[12]->u_Health = 0 ;
  pz_RomAlm[12]->u_Toa = 99 ;
  pz_RomAlm[12]->w_E = 8499 ;
  pz_RomAlm[12]->w_DeltaI = 4690 ;
  pz_RomAlm[12]->w_OmegaDot = 64858 ;
  pz_RomAlm[12]->q_SqrtA = 10554648ul ;
  pz_RomAlm[12]->q_Omega0 = 12540095ul ;
  pz_RomAlm[12]->q_Omega = 5282404ul ;
  pz_RomAlm[12]->q_M0 = 4334914ul ;
  pz_RomAlm[12]->w_Af0 = 2006 ;
  pz_RomAlm[12]->w_Af1 = 2047 ;
  pz_RomAlm[12]->w_GpsWeek = 1911 ;

  pz_RomAlm[13]->u_Sv = 14 ;
  pz_RomAlm[13]->u_Health = 0 ;
  pz_RomAlm[13]->u_Toa = 99 ;
  pz_RomAlm[13]->w_E = 18755 ;
  pz_RomAlm[13]->w_DeltaI = 3440 ;
  pz_RomAlm[13]->w_OmegaDot = 64854 ;
  pz_RomAlm[13]->q_SqrtA = 10555395ul ;
  pz_RomAlm[13]->q_Omega0 = 12439595ul ;
  pz_RomAlm[13]->q_Omega = 11554600ul ;
  pz_RomAlm[13]->q_M0 = 8179019ul ;
  pz_RomAlm[13]->w_Af0 = 2015 ;
  pz_RomAlm[13]->w_Af1 = 2047 ;
  pz_RomAlm[13]->w_GpsWeek = 1911 ;

  pz_RomAlm[14]->u_Sv = 15 ;
  pz_RomAlm[14]->u_Health = 0 ;
  pz_RomAlm[14]->u_Toa = 99 ;
  pz_RomAlm[14]->w_E = 17503 ;
  pz_RomAlm[14]->w_DeltaI = 63458 ;
  pz_RomAlm[14]->w_OmegaDot = 64821 ;
  pz_RomAlm[14]->q_SqrtA = 10554560ul ;
  pz_RomAlm[14]->q_Omega0 = 12067580ul ;
  pz_RomAlm[14]->q_Omega = 1320395ul ;
  pz_RomAlm[14]->q_M0 = 7262819ul ;
  pz_RomAlm[14]->w_Af0 = 1707 ;
  pz_RomAlm[14]->w_Af1 = 0 ;
  pz_RomAlm[14]->w_GpsWeek = 1911 ;

  pz_RomAlm[15]->u_Sv = 16 ;
  pz_RomAlm[15]->u_Health = 0 ;
  pz_RomAlm[15]->u_Toa = 99 ;
  pz_RomAlm[15]->w_E = 18174 ;
  pz_RomAlm[15]->w_DeltaI = 8077 ;
  pz_RomAlm[15]->w_OmegaDot = 64847 ;
  pz_RomAlm[15]->q_SqrtA = 10554386ul ;
  pz_RomAlm[15]->q_Omega0 = 1244464ul ;
  pz_RomAlm[15]->q_Omega = 1034892ul ;
  pz_RomAlm[15]->q_M0 = 11637188ul ;
  pz_RomAlm[15]->w_Af0 = 3 ;
  pz_RomAlm[15]->w_Af1 = 1 ;
  pz_RomAlm[15]->w_GpsWeek = 1911 ;

  pz_RomAlm[16]->u_Sv = 17 ;
  pz_RomAlm[16]->u_Health = 0 ;
  pz_RomAlm[16]->u_Toa = 99 ;
  pz_RomAlm[16]->w_E = 23266 ;
  pz_RomAlm[16]->w_DeltaI = 5768 ;
  pz_RomAlm[16]->w_OmegaDot = 64855 ;
  pz_RomAlm[16]->q_SqrtA = 10554764ul ;
  pz_RomAlm[16]->q_Omega0 = 3969375ul ;
  pz_RomAlm[16]->q_Omega = 11638071ul ;
  pz_RomAlm[16]->q_M0 = 9255455ul ;
  pz_RomAlm[16]->w_Af0 = 1835 ;
  pz_RomAlm[16]->w_Af1 = 0 ;
  pz_RomAlm[16]->w_GpsWeek = 1911 ;

  pz_RomAlm[17]->u_Sv = 18 ;
  pz_RomAlm[17]->u_Health = 0 ;
  pz_RomAlm[17]->u_Toa = 99 ;
  pz_RomAlm[17]->w_E = 36205 ;
  pz_RomAlm[17]->w_DeltaI = 62693 ;
  pz_RomAlm[17]->w_OmegaDot = 64810 ;
  pz_RomAlm[17]->q_SqrtA = 10554663ul ;
  pz_RomAlm[17]->q_Omega0 = 9343961ul ;
  pz_RomAlm[17]->q_Omega = 11802290ul ;
  pz_RomAlm[17]->q_M0 = 13429137ul ;
  pz_RomAlm[17]->w_Af0 = 578 ;
  pz_RomAlm[17]->w_Af1 = 1 ;
  pz_RomAlm[17]->w_GpsWeek = 1911 ;

  pz_RomAlm[18]->u_Sv = 19 ;
  pz_RomAlm[18]->u_Health = 0 ;
  pz_RomAlm[18]->u_Toa = 99 ;
  pz_RomAlm[18]->w_E = 22281 ;
  pz_RomAlm[18]->w_DeltaI = 5290 ;
  pz_RomAlm[18]->w_OmegaDot = 64852 ;
  pz_RomAlm[18]->q_SqrtA = 10554540ul ;
  pz_RomAlm[18]->q_Omega0 = 4099043ul ;
  pz_RomAlm[18]->q_Omega = 2270761ul ;
  pz_RomAlm[18]->q_M0 = 798020ul ;
  pz_RomAlm[18]->w_Af0 = 1499 ;
  pz_RomAlm[18]->w_Af1 = 0 ;
  pz_RomAlm[18]->w_GpsWeek = 1911 ;

  pz_RomAlm[19]->u_Sv = 20 ;
  pz_RomAlm[19]->u_Health = 0 ;
  pz_RomAlm[19]->u_Toa = 99 ;
  pz_RomAlm[19]->w_E = 9768 ;
  pz_RomAlm[19]->w_DeltaI = 62912 ;
  pz_RomAlm[19]->w_OmegaDot = 64809 ;
  pz_RomAlm[19]->q_SqrtA = 10554819ul ;
  pz_RomAlm[19]->q_Omega0 = 9206020ul ;
  pz_RomAlm[19]->q_Omega = 3804978ul ;
  pz_RomAlm[19]->q_M0 = 6941841ul ;
  pz_RomAlm[19]->w_Af0 = 453 ;
  pz_RomAlm[19]->w_Af1 = 1 ;
  pz_RomAlm[19]->w_GpsWeek = 1911 ;

  pz_RomAlm[20]->u_Sv = 21 ;
  pz_RomAlm[20]->u_Health = 0 ;
  pz_RomAlm[20]->u_Toa = 99 ;
  pz_RomAlm[20]->w_E = 49032 ;
  pz_RomAlm[20]->w_DeltaI = 64670 ;
  pz_RomAlm[20]->w_OmegaDot = 64825 ;
  pz_RomAlm[20]->q_SqrtA = 10554378ul ;
  pz_RomAlm[20]->q_Omega0 = 6563516ul ;
  pz_RomAlm[20]->q_Omega = 12063342ul ;
  pz_RomAlm[20]->q_M0 = 15974498ul ;
  pz_RomAlm[20]->w_Af0 = 1491 ;
  pz_RomAlm[20]->w_Af1 = 0 ;
  pz_RomAlm[20]->w_GpsWeek = 1911 ;

  pz_RomAlm[21]->u_Sv = 22 ;
  pz_RomAlm[21]->u_Health = 0 ;
  pz_RomAlm[21]->u_Toa = 99 ;
  pz_RomAlm[21]->w_E = 15783 ;
  pz_RomAlm[21]->w_DeltaI = 62349 ;
  pz_RomAlm[21]->w_OmegaDot = 64807 ;
  pz_RomAlm[21]->q_SqrtA = 10554731ul ;
  pz_RomAlm[21]->q_Omega0 = 9345180ul ;
  pz_RomAlm[21]->q_Omega = 11774942ul ;
  pz_RomAlm[21]->q_M0 = 7931683ul ;
  pz_RomAlm[21]->w_Af0 = 275 ;
  pz_RomAlm[21]->w_Af1 = 2045 ;
  pz_RomAlm[21]->w_GpsWeek = 1911 ;

  pz_RomAlm[22]->u_Sv = 23 ;
  pz_RomAlm[22]->u_Health = 0 ;
  pz_RomAlm[22]->u_Toa = 99 ;
  pz_RomAlm[22]->w_E = 23405 ;
  pz_RomAlm[22]->w_DeltaI = 457 ;
  pz_RomAlm[22]->w_OmegaDot = 64836 ;
  pz_RomAlm[22]->q_SqrtA = 10554514ul ;
  pz_RomAlm[22]->q_Omega0 = 12225703ul ;
  pz_RomAlm[22]->q_Omega = 10004370ul ;
  pz_RomAlm[22]->q_M0 = 5809135ul ;
  pz_RomAlm[22]->w_Af0 = 1851 ;
  pz_RomAlm[22]->w_Af1 = 0 ;
  pz_RomAlm[22]->w_GpsWeek = 1911 ;

  pz_RomAlm[23]->u_Sv = 24 ;
  pz_RomAlm[23]->u_Health = 0 ;
  pz_RomAlm[23]->u_Toa = 99 ;
  pz_RomAlm[23]->w_E = 9567 ;
  pz_RomAlm[23]->w_DeltaI = 991 ;
  pz_RomAlm[23]->w_OmegaDot = 64828 ;
  pz_RomAlm[23]->q_SqrtA = 10554740ul ;
  pz_RomAlm[23]->q_Omega0 = 14964067ul ;
  pz_RomAlm[23]->q_Omega = 1074597ul ;
  pz_RomAlm[23]->q_M0 = 4390028ul ;
  pz_RomAlm[23]->w_Af0 = 2025 ;
  pz_RomAlm[23]->w_Af1 = 0 ;
  pz_RomAlm[23]->w_GpsWeek = 1911 ;

  pz_RomAlm[24]->u_Sv = 25 ;
  pz_RomAlm[24]->u_Health = 0 ;
  pz_RomAlm[24]->u_Toa = 99 ;
  pz_RomAlm[24]->w_E = 11706 ;
  pz_RomAlm[24]->w_DeltaI = 6020 ;
  pz_RomAlm[24]->w_OmegaDot = 64835 ;
  pz_RomAlm[24]->q_SqrtA = 10554699ul ;
  pz_RomAlm[24]->q_Omega0 = 1055545ul ;
  pz_RomAlm[24]->q_Omega = 2114575ul ;
  pz_RomAlm[24]->q_M0 = 15514661ul ;
  pz_RomAlm[24]->w_Af0 = 1803 ;
  pz_RomAlm[24]->w_Af1 = 2046 ;
  pz_RomAlm[24]->w_GpsWeek = 1911 ;

  pz_RomAlm[25]->u_Sv = 26 ;
  pz_RomAlm[25]->u_Health = 0 ;
  pz_RomAlm[25]->u_Toa = 99 ;
  pz_RomAlm[25]->w_E = 1941 ;
  pz_RomAlm[25]->w_DeltaI = 3003 ;
  pz_RomAlm[25]->w_OmegaDot = 64820 ;
  pz_RomAlm[25]->q_SqrtA = 10554401ul ;
  pz_RomAlm[25]->q_Omega0 = 1027372ul ;
  pz_RomAlm[25]->q_Omega = 16737878ul ;
  pz_RomAlm[25]->q_M0 = 14015295ul ;
  pz_RomAlm[25]->w_Af0 = 1570 ;
  pz_RomAlm[25]->w_Af1 = 2046 ;
  pz_RomAlm[25]->w_GpsWeek = 1911 ;

  pz_RomAlm[26]->u_Sv = 27 ;
  pz_RomAlm[26]->u_Health = 0 ;
  pz_RomAlm[26]->u_Toa = 99 ;
  pz_RomAlm[26]->w_E = 7976 ;
  pz_RomAlm[26]->w_DeltaI = 4927 ;
  pz_RomAlm[26]->w_OmegaDot = 64846 ;
  pz_RomAlm[26]->q_SqrtA = 10554581ul ;
  pz_RomAlm[26]->q_Omega0 = 3834684ul ;
  pz_RomAlm[26]->q_Omega = 929672ul ;
  pz_RomAlm[26]->q_M0 = 9237880ul ;
  pz_RomAlm[26]->w_Af0 = 137 ;
  pz_RomAlm[26]->w_Af1 = 1 ;
  pz_RomAlm[26]->w_GpsWeek = 1911 ;

  pz_RomAlm[27]->u_Sv = 28 ;
  pz_RomAlm[27]->u_Health = 0 ;
  pz_RomAlm[27]->u_Toa = 99 ;
  pz_RomAlm[27]->w_E = 41549 ;
  pz_RomAlm[27]->w_DeltaI = 7864 ;
  pz_RomAlm[27]->w_OmegaDot = 64846 ;
  pz_RomAlm[27]->q_SqrtA = 10554544ul ;
  pz_RomAlm[27]->q_Omega0 = 1257414ul ;
  pz_RomAlm[27]->q_Omega = 12470827ul ;
  pz_RomAlm[27]->q_M0 = 11515730ul ;
  pz_RomAlm[27]->w_Af0 = 574 ;
  pz_RomAlm[27]->w_Af1 = 1 ;
  pz_RomAlm[27]->w_GpsWeek = 1911 ;

  pz_RomAlm[28]->u_Sv = 29 ;
  pz_RomAlm[28]->u_Health = 0 ;
  pz_RomAlm[28]->u_Toa = 99 ;
  pz_RomAlm[28]->w_E = 1308 ;
  pz_RomAlm[28]->w_DeltaI = 5936 ;
  pz_RomAlm[28]->w_OmegaDot = 64855 ;
  pz_RomAlm[28]->q_SqrtA = 10554815ul ;
  pz_RomAlm[28]->q_Omega0 = 3995595ul ;
  pz_RomAlm[28]->q_Omega = 306781ul ;
  pz_RomAlm[28]->q_M0 = 14599358ul ;
  pz_RomAlm[28]->w_Af0 = 685 ;
  pz_RomAlm[28]->w_Af1 = 0 ;
  pz_RomAlm[28]->w_GpsWeek = 1911 ;

  pz_RomAlm[29]->u_Sv = 30 ;
  pz_RomAlm[29]->u_Health = 0 ;
  pz_RomAlm[29]->u_Toa = 99 ;
  pz_RomAlm[29]->w_E = 5035 ;
  pz_RomAlm[29]->w_DeltaI = 1454 ;
  pz_RomAlm[29]->w_OmegaDot = 64828 ;
  pz_RomAlm[29]->q_SqrtA = 10554836ul ;
  pz_RomAlm[29]->q_Omega0 = 15208559ul ;
  pz_RomAlm[29]->q_Omega = 8327826ul ;
  pz_RomAlm[29]->q_M0 = 2004742ul ;
  pz_RomAlm[29]->w_Af0 = 154 ;
  pz_RomAlm[29]->w_Af1 = 1 ;
  pz_RomAlm[29]->w_GpsWeek = 1911 ;

  pz_RomAlm[30]->u_Sv = 31 ;
  pz_RomAlm[30]->u_Health = 0 ;
  pz_RomAlm[30]->u_Toa = 99 ;
  pz_RomAlm[30]->w_E = 17350 ;
  pz_RomAlm[30]->w_DeltaI = 4625 ;
  pz_RomAlm[30]->w_OmegaDot = 64845 ;
  pz_RomAlm[30]->q_SqrtA = 10554349ul ;
  pz_RomAlm[30]->q_Omega0 = 15120172ul ;
  pz_RomAlm[30]->q_Omega = 15748475ul ;
  pz_RomAlm[30]->q_M0 = 1294082ul ;
  pz_RomAlm[30]->w_Af0 = 264 ;
  pz_RomAlm[30]->w_Af1 = 2047 ;
  pz_RomAlm[30]->w_GpsWeek = 1911 ;

  pz_RomAlm[31]->u_Sv = 32 ;
  pz_RomAlm[31]->u_Health = 0 ;
  pz_RomAlm[31]->u_Toa = 99 ;
  pz_RomAlm[31]->w_E = 1372 ;
  pz_RomAlm[31]->w_DeltaI = 2735 ;
  pz_RomAlm[31]->w_OmegaDot = 64850 ;
  pz_RomAlm[31]->q_SqrtA = 10554546ul ;
  pz_RomAlm[31]->q_Omega0 = 12218880ul ;
  pz_RomAlm[31]->q_Omega = 10590773ul ;
  pz_RomAlm[31]->q_M0 = 10289592ul ;
  pz_RomAlm[31]->w_Af0 = 1952 ;
  pz_RomAlm[31]->w_Af1 = 2044 ;
  pz_RomAlm[31]->w_GpsWeek = 1911 ;

}