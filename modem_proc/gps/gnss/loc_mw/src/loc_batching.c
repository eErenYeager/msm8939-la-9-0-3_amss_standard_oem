/*============================================================================
 @file loc_batching.c

 loc MW Batching module implementation

 GENERAL DESCRIPTION

 This file contains the loc middleware Batching module implementation.

 Copyright (c) 2013 QUALCOMM Atheros Incorporated.
 All Rights Reserved. QUALCOMM Atheros Proprietary and Confidential

 Export of this technology or software is regulated by the U.S. Government.
 Diversion contrary to U.S. law prohibited.

 =============================================================================*/

/*============================================================================

 EDIT HISTORY FOR FILE

 This section contains comments describing changes made to the module.

 $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/gps/gnss/loc_mw/src/loc_batching.c#2 $
 $DateTime: 2016/09/22 23:57:31 $
 $Author: c_abahet $

 when       who     what, where, why
 --------   ---     ----------------------------------------------------------
 04/13/16   yh      loc_batching_array  NULL pointer check
 07/25/13   bnk     Initial version

=============================================================================*/

#include "customer.h"
#include "comdef.h"

#include "msg.h"
#include "loc_api_2.h"
#include "loc_batching.h"
#include "location_service_v02.h"
#include "loc_api_internal.h"
#include "loc_qmi_shim.h"
#include "gnss_calendar_util.h"


/* -----------------------------------------------------------------------*//**
@brief
  Utility function to push the position report to LIFO buffer
  
@param[in]   client_ptr            Client Info Handle 
@param[in]   batching_element      Batched position report 


@retval    TRUE           adding position report to LIFO success
@retval    FALSE          adding position report to LIFO fail
*//* ------------------------------------------------------------------------*/
static boolean loc_batching_push_to_lifo(loc_client_info_s_type           *const client_ptr,
                                         qmiLocBatchedReportStructT_v02   *const batching_element);


/* -----------------------------------------------------------------------*//**
@brief
  Utility function to retrieve the position report from LIFO buffer
  
@param[in]   client_ptr            Client Info Handle 
@param[in]   number_of_reads       Number of reports to retrieve
@param[out]   pReadFromBatchInd    Indication to store the reports


@retval    int32           number of reports actually retrieved from buffer
*//* ------------------------------------------------------------------------*/
static int32 loc_batching_pull_from_lifo(loc_client_info_s_type         *const client_ptr,
                                         int32                          number_of_reads,
                                         qmiLocReadFromBatchIndMsgT_v02 *pReadFromBatchInd); 


/* -----------------------------------------------------------------------*//**
@brief
  Utility function to send Get Batch Size Indication to control point. 
  
@param[in]   client_ptr            Client Info Handle 
@param[in]   p_getBatchSizeInd     Get Batch Indication message to control point

@retval    TRUE           locQmiShimSendInd is success
@retval    FALSE          locQmiShimSendInd is failed
                                  
*//* ------------------------------------------------------------------------*/
boolean loc_send_get_batch_size_ind(loc_client_info_s_type           *const client_ptr,
                                    qmiLocGetBatchSizeIndMsgT_v02    *const p_getBatchSizeInd)
{ 
  locQmiShimIndInfoStructT              shimIndInfo;

  /* send indication */
  shimIndInfo.p_Msg = p_getBatchSizeInd;
  shimIndInfo.q_MsgLen = sizeof(*p_getBatchSizeInd);
  shimIndInfo.q_Id = QMI_LOC_GET_BATCH_SIZE_IND_V02;

  LOC_MSG_LOW("loc_send_get_batch_size_ind Ind:Clt:%u",
                 shimIndInfo.q_Id, 0, 0);

  return locQmiShimSendInd(client_ptr->client_handle,
                           &shimIndInfo);
}


/* -----------------------------------------------------------------------*//**
@brief
  Function to convert the parsed position report from LocAPI into Batched report. 
  
@param[in]   pz_ParsedPositionRpt  Pointer to parsed position report from LocAPI
@param[out]  pBatchedPosReport     Pointer to the Batched position report

@retval    TRUE           pz_ParsedPositionRpt is of final fix report type
@retval    FALSE          pz_ParsedPositionRpt is of intermediate fix report type
                                  
*//* ------------------------------------------------------------------------*/
boolean loc_convert_parsed_pos_report_to_batching(const loc_parsed_position_s_type   *const pz_ParsedPositionRpt,
                                                  qmiLocBatchedReportStructT_v02     *const pBatchedPosReport)
{ 
  uint16 w_GpsWeek;
  uint32 q_GpsToWMs;

  if ( pz_ParsedPositionRpt->valid_mask & LOC_POS_VALID_SESSION_STATUS )
  {
    /* Batching only Final position reports */
    if(pz_ParsedPositionRpt->session_status != LOC_SESS_STATUS_SUCCESS)
    {
      return FALSE;
    }
  }
  /* reset the memory */
  memset(pBatchedPosReport, 0, sizeof(*pBatchedPosReport));

  //fix_id is updated in push operation

  if ( pz_ParsedPositionRpt->valid_mask & LOC_POS_VALID_TIMESTAMP_UTC )
  {
    pBatchedPosReport->timestampUtc = pz_ParsedPositionRpt->timestamp_utc;
    pBatchedPosReport->validFields |= QMI_LOC_BATCHED_REPORT_MASK_VALID_TIMESTAMP_UTC_V02;
  }
  
  if ( pz_ParsedPositionRpt->valid_mask & LOC_POS_VALID_TIME_UNC )
  {
    pBatchedPosReport->timeUnc = pz_ParsedPositionRpt->time_unc;
    pBatchedPosReport->validFields |= QMI_LOC_BATCHED_REPORT_MASK_VALID_TIME_UNC_V02;
  }

  if ( pz_ParsedPositionRpt->valid_mask & LOC_POS_VALID_LATITUDE )
  {
    pBatchedPosReport->latitude = pz_ParsedPositionRpt->latitude;
    pBatchedPosReport->validFields |= QMI_LOC_BATCHED_REPORT_MASK_VALID_LATITUDE_V02;
  }

  if ( pz_ParsedPositionRpt->valid_mask & LOC_POS_VALID_LONGITUDE )
  {
    pBatchedPosReport->longitude = pz_ParsedPositionRpt->longitude;
    pBatchedPosReport->validFields |= QMI_LOC_BATCHED_REPORT_MASK_VALID_LONGITUDE_V02;
  }
  
  if ( pz_ParsedPositionRpt->valid_mask &
          LOC_POS_VALID_ALTITUDE_WRT_ELLIPSOID )
  {
    pBatchedPosReport->altitudeWrtEllipsoid = pz_ParsedPositionRpt->altitude_wrt_ellipsoid;
    pBatchedPosReport->validFields |= QMI_LOC_BATCHED_REPORT_MASK_VALID_ALT_WRT_ELP_V02;
  }

  if ( pz_ParsedPositionRpt->valid_mask & LOC_POS_VALID_SPEED_HORIZONTAL )
  {
    pBatchedPosReport->speedHorizontal = pz_ParsedPositionRpt->speed_horizontal;
    pBatchedPosReport->validFields |= QMI_LOC_BATCHED_REPORT_MASK_VALID_SPEED_HOR_V02;
  }

  if ( pz_ParsedPositionRpt->valid_mask & LOC_POS_VALID_SPEED_VERTICAL )
  {
    pBatchedPosReport->speedVertical = pz_ParsedPositionRpt->speed_vertical;
    pBatchedPosReport->validFields |= QMI_LOC_BATCHED_REPORT_MASK_VALID_SPEED_VER_V02;
  }

  if ( pz_ParsedPositionRpt->valid_mask & LOC_POS_VALID_HEADING )
  {
    pBatchedPosReport->heading = pz_ParsedPositionRpt->heading;
    pBatchedPosReport->validFields |= QMI_LOC_BATCHED_REPORT_MASK_VALID_HEADING_V02;
  }
  
  if ( pz_ParsedPositionRpt->valid_mask & LOC_POS_VALID_HOR_UNC_CIRCULAR )
  {
    pBatchedPosReport->horUncCircular = pz_ParsedPositionRpt->hor_unc_circular;
    pBatchedPosReport->validFields |= QMI_LOC_BATCHED_REPORT_MASK_VALID_HOR_CIR_UNC_V02;
  }

  if ( pz_ParsedPositionRpt->valid_mask & LOC_POS_VALID_VERTICAL_UNC )
  {
    pBatchedPosReport->vertUnc = pz_ParsedPositionRpt->vert_unc;
    pBatchedPosReport->validFields |= QMI_LOC_BATCHED_REPORT_MASK_VALID_VERT_UNC_V02;
  }

  if ( pz_ParsedPositionRpt->valid_mask & LOC_POS_VALID_SPEED_UNC )
  {
    pBatchedPosReport->speedUnc = pz_ParsedPositionRpt->speed_unc;
    pBatchedPosReport->validFields |= QMI_LOC_BATCHED_REPORT_MASK_VALID_SPEED_UNC_V02;
  }

  if ( pz_ParsedPositionRpt->valid_mask & LOC_POS_VALID_HEADING_UNC )
  {
    pBatchedPosReport->headingUnc = pz_ParsedPositionRpt->heading_unc;
    pBatchedPosReport->validFields |= QMI_LOC_BATCHED_REPORT_MASK_VALID_HEADING_UNC_V02;
  }

  if ( pz_ParsedPositionRpt->valid_mask & LOC_POS_VALID_MAGNETIC_VARIATION )
  {
    pBatchedPosReport->magneticDeviation = pz_ParsedPositionRpt->magnetic_deviation;
    pBatchedPosReport->validFields |= QMI_LOC_BATCHED_REPORT_MASK_VALID_MAGNETIC_DEV_V02;
  }

  if ( pz_ParsedPositionRpt->valid_mask & LOC_POS_VALID_TECHNOLOGY_MASK )
  {
    pBatchedPosReport->technologyMask = pz_ParsedPositionRpt->technology_mask;
    pBatchedPosReport->validFields |= QMI_LOC_BATCHED_REPORT_MASK_VALID_TECH_MASK_V02;
  }

  if ( pz_ParsedPositionRpt->valid_mask & LOC_POS_VALID_CONFIDENCE_HORIZONTAL )
  {
    pBatchedPosReport->horConfidence = pz_ParsedPositionRpt->confidence_horizontal;
    pBatchedPosReport->validFields |= QMI_LOC_BATCHED_REPORT_MASK_VALID_HOR_CONF_V02;
  }
  
  /* We need to fill in GPS time as well, calculate from UTC and 
   fill in, only if LEAP Seconds is also provided */
  if ( (pz_ParsedPositionRpt->valid_mask & LOC_POS_VALID_LEAP_SECONDS) &&
       (TRUE == gnss_ConvertUtcMstoGpsTime(pz_ParsedPositionRpt->timestamp_utc,
                                           pz_ParsedPositionRpt->leap_seconds,
                                           &w_GpsWeek,
                                           &q_GpsToWMs)) )
  {
    pBatchedPosReport->gpsTime.gpsWeek = w_GpsWeek;
    pBatchedPosReport->gpsTime.gpsTimeOfWeekMs = q_GpsToWMs;
    pBatchedPosReport->validFields |= QMI_LOC_BATCHED_REPORT_MASK_VALID_TIMESTAMP_GPS_V02;
  }

  return TRUE;
}


/* -----------------------------------------------------------------------*//**
@brief
  Utility function to push the position report to LIFO buffer
  
@param[in]   client_ptr            Client Info Handle 
@param[in]   batching_element      Batched position report 


@retval    TRUE           adding position report to LIFO success
@retval    FALSE          adding position report to LIFO fail
*//* ------------------------------------------------------------------------*/
static boolean loc_batching_push_to_lifo(loc_client_info_s_type           *const client_ptr,
                                         qmiLocBatchedReportStructT_v02   *const batching_element)
{
  if((client_ptr == NULL) || (batching_element == NULL))
  {
     LOC_MSG_ERROR("Null pointer!", 0, 0, 0);
     return FALSE;
  }

  /* check clients batching status */
  if ( FALSE == client_ptr->batching_info.is_batching_on )
  {
    return FALSE;
  }
  if( NULL == client_ptr->batching_info.loc_batching_array )
  {
     LOC_MSG_ERROR("Null loc_batching_array!", 0, 0, 0);
     return FALSE;
  }
  /* buffer is anyways empty, reset the write/read pointers */
  if(client_ptr->batching_info.total_occupied <= 0)
  {    
    client_ptr->batching_info.write_index = 0;
    client_ptr->batching_info.read_index = -1;
    LOC_MSG_LOW("PUSH: total_occupied <= 0 hence resetting write (0) & read (-1) indexes", 0, 0, 0);
  }
  
  /* Check if the write index reached the top of buffer and reset. Mark the overwrite flag */
  if ( client_ptr->batching_info.write_index == client_ptr->batching_info.max_array_size )
  {
    client_ptr->batching_info.write_index = 0;
    client_ptr->batching_info.overwrite_flag = TRUE;
    LOC_MSG_LOW("PUSH: write_index reached max (%d) resetting to 0, and overwrite_flag is TRUE",
                client_ptr->batching_info.max_array_size, 0, 0);
  }

  /* update the fix id in the stored report */
  batching_element->fixId = client_ptr->batching_info.fix_id;  // update fixId for live report & batched pos

  /* copy the input into the buffer */
  client_ptr->batching_info.loc_batching_array[client_ptr->batching_info.write_index] = (*batching_element);
  client_ptr->batching_info.fix_id++;  // increment the fix id counter

  LOC_MSG_MED("PUSH: Fixed batched at write_index %d fixid (%d)", client_ptr->batching_info.write_index,
              client_ptr->batching_info.loc_batching_array[client_ptr->batching_info.write_index].fixId, 0);

  /* increment the write index */
  (client_ptr->batching_info.write_index)++;

  /* update the total occupied counter */
  (client_ptr->batching_info.total_occupied)++;

  /* array overwrites, the value still remains MAX-ARRAY */
  if ( client_ptr->batching_info.total_occupied > client_ptr->batching_info.max_array_size )
  {
    /* counter always remains same */
    client_ptr->batching_info.total_occupied = client_ptr->batching_info.max_array_size;
    LOC_MSG_LOW("PUSH: total_occupied peaked (%d), write index (%d)", client_ptr->batching_info.total_occupied, client_ptr->batching_info.write_index, 0);
  }
  return TRUE;
}


/* -----------------------------------------------------------------------*//**
@brief
  Function to handle position report from LocAPI. The function pushes the 
  batched fix into the LIFO buffer and notifies the control point depending
  on the event registration masks.
  
@param[in]   client_ptr            Client Info Handle 
@param[in]   batching_element      Pointer to the Batched position report

@retval    TRUE           push to LIFO buffer is success
@retval    FALSE          push to LIFO buffer is fail
                                  
*//* ------------------------------------------------------------------------*/
boolean loc_batching_handle_position_report(loc_client_info_s_type           *const client_ptr,
                                            qmiLocBatchedReportStructT_v02   *const batching_element)
{ 
  /* push the entry into batching buffer */
  if (FALSE == loc_batching_push_to_lifo(client_ptr, batching_element))
  {
    return FALSE; /* client turned off batching */
  }

  /* handle:: first report live fix, and then report batch-full */

  /* check if client registered for live fixes and report */
  loc_batching_report_live_position_event(client_ptr, batching_element);

  /* check for buffer full */
  if ( client_ptr->batching_info.total_occupied == client_ptr->batching_info.max_array_size )
  {
    /* check if client registered for batch full event and notify */
    loc_batching_report_batch_full_event(client_ptr);
  }

  return TRUE;
}


/* -----------------------------------------------------------------------*//**
@brief
  Utility function to send read from batch Indication to control point.
  
@param[in]   client_ptr            Client Info Handle 
@param[out]  p_readFromBatchInd    Read From Batch Indication message to control point
@param[in]   request_status        Status from lifo buffer pull operation
@param[in]   reqTransactionId      Transaction Id in request message by control point 

@retval    TRUE           locQmiShimSendInd is success
@retval    FALSE          locQmiShimSendInd is failed
                                  
*//* ------------------------------------------------------------------------*/
boolean loc_send_read_from_batch_ind(loc_client_info_s_type               *const client_ptr,
                                     qmiLocReadFromBatchIndMsgT_v02       *const p_readFromBatchInd,
                                     boolean                              request_status,
                                     uint32                               reqTransactionId)
{
  locQmiShimIndInfoStructT              shimIndInfo;

  /* invalid read size, return failure */
  if(request_status == FALSE)
  {
    p_readFromBatchInd->status = eQMI_LOC_INVALID_PARAMETER_V02;
  }
  else
  {
    p_readFromBatchInd->status = eQMI_LOC_SUCCESS_V02;
  }

  /* update transaction id in indication - mandatory field */
  p_readFromBatchInd->transactionId = reqTransactionId; 

  shimIndInfo.p_Msg = p_readFromBatchInd;
  shimIndInfo.q_MsgLen = sizeof(*p_readFromBatchInd);
  shimIndInfo.q_Id = QMI_LOC_READ_FROM_BATCH_IND_V02;

  LOC_MSG_MED("loc_send_read_from_batch_ind: calling locQmiShimSendInd", 0, 0, 0);

  /* send indication */
  return locQmiShimSendInd(client_ptr->client_handle,
                           &shimIndInfo);
}


/* -----------------------------------------------------------------------*//**
@brief
  Utility function to send release batch Indication to control point.
  
@param[in]   client_ptr            Client Info Handle 
@param[out]  p_releaseBatchInd     Release Batch Indication message to control point
@param[in]   request_status        Status from buffer free operation
@param[in]   reqTransactionId      Transaction Id in request message by control point 


@retval    TRUE           locQmiShimSendInd is success
@retval    FALSE          locQmiShimSendInd is failed
                                  
*//* ------------------------------------------------------------------------*/
boolean loc_send_release_batch_ind(loc_client_info_s_type           *const client_ptr,
                                   qmiLocReleaseBatchIndMsgT_v02    *const p_releaseBatchInd,
                                   boolean                          request_status,
                                   uint32                           reqTransactionId)
{
  locQmiShimIndInfoStructT              shimIndInfo;

  /* invalid read size, return failure */
  if(request_status == FALSE)
  {
    p_releaseBatchInd->status = eQMI_LOC_GENERAL_FAILURE_V02;
  }
  else
  {
    p_releaseBatchInd->status = eQMI_LOC_SUCCESS_V02;
  }

  /* update transaction id in indication - mandatory field */
  p_releaseBatchInd->transactionId = reqTransactionId;

  shimIndInfo.p_Msg = p_releaseBatchInd; 
  shimIndInfo.q_MsgLen = sizeof(*p_releaseBatchInd);
  shimIndInfo.q_Id = QMI_LOC_RELEASE_BATCH_IND_V02;

  LOC_MSG_MED("loc_send_release_batch_ind: calling locQmiShimSendInd", 0, 0, 0);

  /* send indication */
  return locQmiShimSendInd(client_ptr->client_handle,
                           &shimIndInfo);
}


/* -----------------------------------------------------------------------*//**
@brief
  Utility function to send stop batch Indication to control point.
  
@param[in]   client_ptr            Client Info Handle 
@param[out]  p_StopBatchingInd     Stop Batch Indication message to control point
@param[in]   request_status        Status from stop session request to LocAPI
@param[in]   reqTransactionId      Transaction Id in request message by control point 


@retval    TRUE           locQmiShimSendInd is success
@retval    FALSE          locQmiShimSendInd is failed
                                  
*//* ------------------------------------------------------------------------*/
boolean loc_send_stop_batching_ind(loc_client_info_s_type           *const client_ptr,
                                   qmiLocStopBatchingIndMsgT_v02    *const p_StopBatchingInd,
                                   boolean                          request_status,
                                   uint32                           reqTransactionId)
{ 
  locQmiShimIndInfoStructT              shimIndInfo;

  /* return failure */
  if(request_status == FALSE)
  {
    p_StopBatchingInd->status = eQMI_LOC_GENERAL_FAILURE_V02;
  }
  else
  {
    p_StopBatchingInd->status = eQMI_LOC_SUCCESS_V02;
  }

  /* update transaction id in indication - mandatory field */
  p_StopBatchingInd->transactionId = reqTransactionId; 

  shimIndInfo.p_Msg = p_StopBatchingInd;
  shimIndInfo.q_MsgLen = sizeof(*p_StopBatchingInd);
  shimIndInfo.q_Id = QMI_LOC_STOP_BATCHING_IND_V02;
  
  LOC_MSG_MED("loc_send_stop_batching_ind: calling locQmiShimSendInd", 0, 0, 0);

  /* send indication */
  return locQmiShimSendInd(client_ptr->client_handle,
                           &shimIndInfo);
}


/* -----------------------------------------------------------------------*//**
@brief
  Function to free the allocated buffer memory and reset the client's batching
   related fields. 
  
@param[in]   client_ptr            Client Info Handle 

@retval    TRUE           Buffer memory release success
@retval    FALSE          Buffer memory release failed
                                  
*//* ------------------------------------------------------------------------*/
boolean loc_batching_free_and_reset(loc_client_info_s_type     *const client_ptr)
{
   if(NULL == client_ptr)
   {
      LOC_MSG_MED("loc_batching_free_and_reset NULL client_ptr", 0, 0, 0);
      return FALSE;
   }

   /* free the buffer */
   if ( NULL != client_ptr->batching_info.loc_batching_array )
   {
      os_MemFree((void **)&client_ptr->batching_info.loc_batching_array);
      LOC_MSG_MED("loc_batching_free_and_reset free loc batching array", 0, 0, 0);
   }

   /* reset all fields */
   memset(&(client_ptr->batching_info),0,sizeof(client_ptr->batching_info));
   client_ptr->batching_info.read_index = -1;
   client_ptr->batching_info.write_index = -1;

   LOC_MSG_MED("loc_batching_free_and_reset for batching client SUCCESS", 0, 0, 0);
   return TRUE;
}

/* -----------------------------------------------------------------------*//**
@brief
  Function to allocate the buffer memory based on availability and control
   points request.
  
@param[in]   client_ptr            Client Info Handle 
@param[out]  p_GetBatchSizeInd     Get Batch Indication message to control point
@param[in]   client_batch_size_req Batch size requested by control point 
@param[in]   reqTransactionId      Transaction Id in request message by control point 

@retval    TRUE           Buffer memory alloc success
@retval    FALSE          Buffer memory alloc failed
                                  
*//* ------------------------------------------------------------------------*/
boolean locGetBatchSizeHandleAllocate(loc_client_info_s_type         *const client_ptr,
                                      qmiLocGetBatchSizeIndMsgT_v02  *const p_GetBatchSizeInd,
                                      uint32                         client_batch_size_req,
                                      uint32                         reqTransactionId)
{
  qmiLocStatusEnumT_v02 request_status = eQMI_LOC_SUCCESS_V02;
  uint32 actual_batch_size = 0;
  uint32 malloc_retry_reduce = 0;
  void* buffer_ptr = NULL;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /* Not doing check of inputs as this is called from within the module,
     and this will be called with proper parameters */
  

  /* check if batch size requested is valid */
  if ( client_batch_size_req <= 0 )
  {
    client_batch_size_req = 0; /* return 0 to client in indication */
    request_status = eQMI_LOC_INVALID_PARAMETER_V02; /* return failure to client in indication */
  }
  else
  {
    actual_batch_size = client_batch_size_req + LOC_BATCHING_ADDITIONAL_SLOTS; /* add extra slots for buffer manipulation */

    LOC_MSG_MED ("locGetBatchSizeHandleAllocate: client handle = %d, client_batch_size_req = %d, actual_batch_size = %d with reserved",
                 client_ptr->client_handle, client_batch_size_req, actual_batch_size);

    /* malloc retry in 4 parts */
    malloc_retry_reduce = actual_batch_size / 4;

    /* avoid the possible (32bit) integer overflow when the total bytes size is computed. */
    if ( actual_batch_size < (MAX_UINT32 / sizeof(qmiLocBatchedReportStructT_v02)) )   //MAX_UINT32 = 0xFFFFFFFF
    {
      /* actual_batch_size is in limits to avoid buffer overflow */
      LOC_MSG_MED("locGetBatchSizeHandleAllocate: client handle = %d, actual_batch_size is in limits to avoid buffer overflow",
                  client_ptr->client_handle, 0, 0);

      do
      {
        buffer_ptr = (void *)os_MemAlloc(actual_batch_size * sizeof(qmiLocBatchedReportStructT_v02), OS_MEM_SCOPE_TASK);

        if ( NULL == buffer_ptr )
        {
          /* if malloc fails, reduce the size of the batch and retry */
          actual_batch_size = actual_batch_size - malloc_retry_reduce;
        }
        else
        {
          /* malloc success */
          LOC_MSG_MED("locGetBatchSizeHandleAllocate: Sucess Batching malloc - client handle = %d, total batch size = %d, size of each fix = %d",
                      client_ptr->client_handle, actual_batch_size, sizeof(qmiLocBatchedReportStructT_v02));

          break;
        }
      }
      while ( actual_batch_size > LOC_BATCHING_ADDITIONAL_SLOTS );

    }
    else
    {
      LOC_MSG_MED("locGetBatchSizeHandleAllocate: client handle = %d, actual_batch_size is not in limits to avoid buffer overflow ",
                  client_ptr->client_handle, 0, 0);
    }

    if ( buffer_ptr == NULL )
    {
      /* insufficient memory - return failure IND */
      client_ptr->batching_info.is_batching_client = FALSE;
      client_ptr->batching_info.client_batch_size = 0;
      client_ptr->batching_info.total_batch_size = 0;
      client_ptr->batching_info.loc_batching_array = NULL;

      client_batch_size_req = 0; /* return 0 to client in indication */
      request_status = eQMI_LOC_INSUFFICIENT_MEMORY_V02;

      LOC_MSG_MED ("locGetBatchSizeHandleAllocate: Batching client handle = %d, allocation failed",
                   client_ptr->client_handle, 0, 0);
    }
    else
    {
      client_ptr->batching_info.is_batching_client = TRUE;
      client_ptr->batching_info.client_batch_size = actual_batch_size - LOC_BATCHING_ADDITIONAL_SLOTS;
      client_ptr->batching_info.total_batch_size = actual_batch_size;
      client_ptr->batching_info.loc_batching_array = (qmiLocBatchedReportStructT_v02 *)buffer_ptr;

      client_batch_size_req = client_ptr->batching_info.client_batch_size; /* return size to client in indication */
      request_status = eQMI_LOC_SUCCESS_V02;

      LOC_MSG_MED ("locGetBatchSizeHandleAllocate: Batching client handle = %d, batch size = %d",
                   client_ptr->client_handle, client_ptr->batching_info.client_batch_size, 0);

      /* prepare the batching buffer and other variables */
      //loc_batching_variables_set.loc_batching_array = (loc_batched_position_s_type *)buffer_ptr;
      client_ptr->batching_info.write_index = 0;
      client_ptr->batching_info.read_index = -1;
      client_ptr->batching_info.total_occupied = 0;
      client_ptr->batching_info.overwrite_flag = FALSE;
      client_ptr->batching_info.max_array_size = client_ptr->batching_info.client_batch_size;
      client_ptr->batching_info.is_batching_on = FALSE;
    }
  }

  /* update the status field in indication */
  p_GetBatchSizeInd->status = request_status;

  /* update batch size in indication - mandatory field */
  p_GetBatchSizeInd->batchSize = client_batch_size_req;

  /* update transaction id in indication - mandatory field */
  p_GetBatchSizeInd->transactionId = reqTransactionId; 
  
  return TRUE;
}


/* -----------------------------------------------------------------------*//**
@brief
  Utility function to retrieve the position report from LIFO buffer
  
@param[in]   client_ptr            Client Info Handle 
@param[in]   number_of_reads       Number of reports to retrieve
@param[out]   pReadFromBatchInd    Indication to store the reports


@retval    int32           number of reports actually retrieved from buffer
*//* ------------------------------------------------------------------------*/
static int32 loc_batching_pull_from_lifo(loc_client_info_s_type         *const client_ptr,
                                         int32                          number_of_reads,
                                         qmiLocReadFromBatchIndMsgT_v02 *pReadFromBatchInd)
{
  int i = -1;

  /* pReadFromBatchInd is already memset'ed at caller, so no need to initialize each field */
  if((client_ptr == NULL) || (pReadFromBatchInd == NULL))
  {
     LOC_MSG_ERROR("loc_batching_pull_from_lifo Null pointer!", 0, 0, 0);
     return 0;
  }

  if( NULL == client_ptr->batching_info.loc_batching_array )
  {
     LOC_MSG_ERROR("loc_batching_pull_from_lifo Null loc_batching_array!", 0, 0, 0);
    return 0;
  }


  /* always write = (read - 1) */
  client_ptr->batching_info.read_index = client_ptr->batching_info.write_index - 1;

  for(i = 0; i < number_of_reads; i++)  
  {
    if(client_ptr->batching_info.read_index < 0) 
    {
      if(client_ptr->batching_info.overwrite_flag == TRUE)
      {
        client_ptr->batching_info.read_index = client_ptr->batching_info.max_array_size - 1;
        client_ptr->batching_info.overwrite_flag = FALSE; /* reset the flag */
        LOC_MSG_LOW("PULL: overwrite_flag resetted to FALSE", 0, 0, 0);
      }
      else
      {
        break;
      }
    }

    if(client_ptr->batching_info.total_occupied <= 0)
    {
      break;
    }
    
    /* update the counter fields in the indication */
    pReadFromBatchInd->numberOfEntries++;
    pReadFromBatchInd->batchedReportList_len++;

    /* already memset'ed the buffer */
    /* fill the position report into the indication */
    pReadFromBatchInd->batchedReportList[i] = client_ptr->batching_info.loc_batching_array[client_ptr->batching_info.read_index];
    
    LOC_MSG_LOW("PULL: Peek fixId %d from Ind at index i %d", pReadFromBatchInd->batchedReportList[i].fixId, i, 0);

    /* adjust the Read/Write indexes */
    client_ptr->batching_info.write_index = client_ptr->batching_info.read_index;
    (client_ptr->batching_info.read_index)--;
    
    /* update the total entries remaining in the buffer */
    (client_ptr->batching_info.total_occupied)--;
    LOC_MSG_MED("PULL: updated read (%d) write (%d) indexes, total_occupied = %d", client_ptr->batching_info.read_index,
                client_ptr->batching_info.write_index, client_ptr->batching_info.total_occupied);
  }

  if ( i > 0 )
  {
    pReadFromBatchInd->numberOfEntries_valid = TRUE;
    pReadFromBatchInd->batchedReportList_valid = TRUE;
  }

  LOC_MSG_MED("loc_batching_pull_from_lifo: number of entries returning %d total occupied %d", i, client_ptr->batching_info.total_occupied, 0);

  return i;
}


/* -----------------------------------------------------------------------*//**
@brief
  Function to handle read operation from the lifo buffer. 
  
@param[in]   client_ptr            Client Info Handle 
@param[in]   number_of_reads       Number of fixes to be read from batching buffer
@param[out]  pReadFromBatchInd     Indication with batched position reports


@retval    TRUE           number of reads field is valid
@retval    FALSE          number of reads field is not valid
                                  
*//* ------------------------------------------------------------------------*/
boolean loc_handle_batching_read(loc_client_info_s_type            *const client_ptr,
                                 int32                             number_of_reads,
                                 qmiLocReadFromBatchIndMsgT_v02    *const pReadFromBatchInd)
{ 
  LOC_MSG_MED("loc_handle_batching_read: number of entries to read %d", number_of_reads, 0, 0);

  if (( number_of_reads <= 0 ) || 
      ( number_of_reads > QMI_LOC_READ_FROM_BATCH_MAX_SIZE_V02 ))
  {
    return FALSE;
  }

  /* pull the entries from batching buffer */
  loc_batching_pull_from_lifo(client_ptr, number_of_reads, pReadFromBatchInd);

  return TRUE;
}


/* -----------------------------------------------------------------------*//**
@brief
  Function to construct the Indication message for start batching request 

@param[in]   pz_IndInfo            Shim info struct containing the Indication
@param[in]   b_IndFailure          Status included in the indication message


@retval    TRUE           indication constructed success
@retval    FALSE          indication constructed failure
*//* ------------------------------------------------------------------------*/
boolean loc_batching_send_start_batching_ind(locQmiShimIndInfoStructT   *const pz_IndInfo,
                                             boolean                    b_IndFailure)
{
  qmiLocStartBatchingIndMsgT_v02 *pz_StartBatchingInd =
     (qmiLocStartBatchingIndMsgT_v02 *)pz_IndInfo->p_Msg;

  if ( TRUE == b_IndFailure )
  {
    pz_StartBatchingInd->status = eQMI_LOC_GENERAL_FAILURE_V02;
  } else
  {
    pz_StartBatchingInd->status = eQMI_LOC_SUCCESS_V02;
  }

  pz_IndInfo->q_Id = QMI_LOC_START_BATCHING_IND_V02;
  pz_IndInfo->q_MsgLen = sizeof(*pz_StartBatchingInd);

  return TRUE;
}


