/******************************************************************************
  @file:  loc_api_internal.h
  @brief: Location Middleware internal API header

  DESCRIPTION
      Declares the internal data types and macros 
      used by loc middleware

  INITIALIZATION AND SEQUENCING REQUIREMENTS

  -----------------------------------------------------------------------------
  Copyright (c) 2010-2013 Qualcomm Technologies Incorporated.
  All Rights Reserved. QUALCOMM Proprietary and Confidential.
  -----------------------------------------------------------------------------
 ******************************************************************************/

/*=====================================================================
                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

when       who      what, where, why
--------   ---      ------------------------------------------------------- 
02/08/13   ssu      Move to the DOG Heartbeat mechanism on DIME and beyond   
04/07/10   ns       Added loc_mw_task_started flag to denote that loc mw
                    task has started
                    
$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/gps/gnss/loc_mw/src/loc_api_internal.h#1 $
$Author: mplp4svc $
$DateTime: 2015/01/27 06:42:19 $
======================================================================*/

#ifndef FEATURE_LOC_API_INTERNAL_H
#define FEATURE_LOC_API_INTERNAL_H

#include "gps_variation.h"
#include "loc_api_2.h"
#include "pdapi.h"
#include "loc_task.h"
#include "loc_conn.h"
#include "loc_xtra.h"
#include "loc_pd.h"
#include "loc_pa.h"
#include "loc_ni.h"
#include "loc_wifi.h"
#include "loc_client.h"
#include "aries_os_globals.h"
#include "aries_os_api.h"

/*Include to get the data-type definitions for QMI-LOC */
#include "location_service_v02.h"


#ifndef MAX_UINT32
#define MAX_UINT32             0xffffffff
#endif // #ifndef MAX_UINT32

#ifndef MAX_INT32
#define MAX_INT32              0x7fffffff
#endif // #ifndef MAX_INT32

#define LOC_HANDLE_REGISTERED_CLIENTS (-2)

#define LOC_MSG_LOW( str, a, b, c )       MSG_3(MSG_SSID_GNSS_LOCMW, MSG_LEGACY_LOW, str, a, b, c )
#define LOC_MSG_MED( str, a, b, c )       MSG_3(MSG_SSID_GNSS_LOCMW, MSG_LEGACY_MED, str, a, b, c )
#define LOC_MSG_HIGH( str, a, b, c )      MSG_3(MSG_SSID_GNSS_LOCMW, MSG_LEGACY_HIGH, str, a, b, c )
#define LOC_MSG_ERROR( str, a, b, c )     MSG_3(MSG_SSID_GNSS_LOCMW, MSG_LEGACY_ERROR, str, a, b, c )
#define LOC_ERR_FATAL( str, a, b, c )     MSG_3(MSG_SSID_GNSS_LOCMW, MSG_LEGACY_FATAL, str, a, b, c )

// MACRO Initialize Critical Section
#define LOC_MW_INIT_CRIT_SECTION( mutex_ctrl_block )                     \
  { os_MutexInit( mutex_ctrl_block, MUTEX_DATA_ONLY_CONTEXT ); }

// MACRO Enter Critical Section
#define LOC_MW_ENTER_CRIT_SECTION( mutex_ctrl_block )                    \
  { os_MutexLock( mutex_ctrl_block ); }

// MACRO Leave Critical Section
#define LOC_MW_LEAVE_CRIT_SECTION( mutex_ctrl_block )                    \
  { os_MutexUnlock( mutex_ctrl_block ); }

typedef struct
{
   // Module initialization is done when the first client registers for the service
   volatile boolean         loc_mw_task_started;
   boolean                  qmi_loc_enabled;
   boolean                  pd_module_initialized;
   boolean                  nmea_module_initialized;
   boolean                  pa_module_initialized;
   boolean                  conn_module_initialized;
   boolean                  xtra_module_initialized;
   boolean                  ni_module_initialized;
   boolean                  geofence_module_initialized;
   boolean                  nmea_module_needed;    // Whether NMEA module is needed or not

   pdsm_client_id_type      pdapi_client_id;       // Used for pdapi events
   pdsm_client_id_type      xtra_client_id;  // Used for xtra events
   pdsm_client_id_type      ni_client_id;    // Used for ni events

   loc_client_module_data_s_type   client_module_data;
   loc_pd_module_data_s_type       pd_module_data;
   loc_xtra_module_data_s_type     xtra_module_data;
   loc_conn_module_data_s_type     conn_module_data;
   loc_pa_module_data_s_type       pa_module_data;
   loc_ni_module_data_s_type       ni_module_data;
   os_MutexBlockType	           crit_sec; // critical section for the module
   os_MutexBlockType               pd_crit_sec; // critical section for position module
#ifndef FEATURE_CGPS_DOG_HEARTBEAT
   os_TimerMsgType*                dog_timer_ptr;
#endif/* !FEATURE_CGPS_DOG_HEARTBEAT */
#ifdef FEATURE_CGPS_QWIP
   boolean                         wifi_module_initialized;
   loc_wifi_module_data_s_type     wifi_module_data;
   pdsm_client_id_type             wifi_client_id;
   os_MutexBlockType               wifi_crit_sec; // critical section for wifi module
#endif /* FEATURE_CGPS_QWIP */

} loc_middleware_data_s_type;

extern loc_middleware_data_s_type loc_middleware_data;


typedef enum
{
  LM_MIDDLEWARE_MSG_ID_PDAPI = C_USR_MSG_ID_FIRST,
  LM_MIDDLEWARE_MSG_ID_QMI_SHIM,

  LM_MIDDLEWARE_MSG_ID_SDP_SET_SPI_STATUS_IND =             6000,
  LM_MIDDLEWARE_MSG_ID_SDP_INJECT_SENSOR_DATA_IND =         6001,
  LM_MIDDLEWARE_MSG_ID_SDP_INJECT_TIME_SYNC_DATA_IND =      6002,
  LM_MIDDLEWARE_MSG_ID_SDP_GET_CRADLE_MOUNT_CONFIG_IND =    6003,
  LM_MIDDLEWARE_MSG_ID_SDP_GET_EXTERNAL_POWER_CONFIG_IND =  6004,
  LM_MIDDLEWARE_MSG_ID_SDP_SET_CRADLE_MOUNT_CONFIG_IND =    6005,
  LM_MIDDLEWARE_MSG_ID_SDP_SET_EXTERNAL_POWER_CONFIG_IND =  6006,
  LM_MIDDLEWARE_MSG_ID_SDP_EVENT_SENSOR_STREAMING_READINESS_IND =   6007,
  LM_MIDDLEWARE_MSG_ID_SDP_EVENT_TIME_SYNC_NEEDED_IND =     6008,
  LM_MIDDLEWARE_MSG_ID_SDP_EVENT_SWITCH_SPI_STREAMING_REPORT_IND =  6009,
  LM_MIDDLEWARE_MSG_ID_SET_SENSOR_CONTROL_CONFIG_IND                     =  6010,
  LM_MIDDLEWARE_MSG_ID_GET_SENSOR_CONTROL_CONFIG_IND                     =  6011,
  LM_MIDDLEWARE_MSG_ID_SET_SENSOR_PROPERTIES_IND                         =  6012,
  LM_MIDDLEWARE_MSG_ID_GET_SENSOR_PROPERTIES_IND                         =  6013,
  LM_MIDDLEWARE_MSG_ID_SET_SENSOR_PERFORMANCE_CONTROL_CONFIGURATION_IND  =  6014,
  LM_MIDDLEWARE_MSG_ID_GET_SENSOR_PERFORMANCE_CONTROL_CONFIGURATION_IND  =  6015,
  LM_MIDDLEWARE_MSG_ID_GEOFENCE_NI_IND     =                6016,
  LM_MIDDLEWARE_MSG_ID_GEOFENCE_GEN_ALERT_IND  =            6017,
  LM_MIDDLEWARE_MSG_ID_GEOFENCE_BREACH_IND =                6018,
  LM_MIDDLEWARE_MSG_ID_GEOFENCE_ADD_IND =                   6019,
  LM_MIDDLEWARE_MSG_ID_GEOFENCE_DELETE_IND =                6020,
  LM_MIDDLEWARE_MSG_ID_GEOFENCE_QUERY_IND  =                6021,
  LM_MIDDLEWARE_MSG_ID_GEOFENCE_EDIT_IND   =                6022,
  LM_MIDDLEWARE_MSG_ID_EVENT_PEDOMETER_CONTROL_IND  =       6023,
  LM_MIDDLEWARE_MSG_ID_EVENT_MOTION_DATA_CONTROL_IND  =     6024,
  LM_MIDDLEWARE_MSG_ID_PEDOMTER_REPORT_IND            =     6025,
  LM_MIDDLEWARE_MSG_ID_INJECT_MOTION_DATA_IND         =     6026,
  LM_MIDDLEWARE_MSG_ID_GEOFENCE_SET_ENGINE_CONFIG_IND =     6027,
  LM_MIDDLEWARE_MSG_ID_SDP_EVENT_VEHICLE_SENSOR_INJECTION_READINESS_IND = 6028,
  LM_MIDDLEWARE_MSG_ID_SDP_INJECT_VEHICLE_SENSOR_DATA_IND         =     6029,
  LM_MIDDLEWARE_MSG_ID_GEOFENCE_BATCH_BREACH_IND      =     6030,
  LM_MIDDLEWARE_MSG_ID_PD_EVENT_WIFI_AP_SCAN_INJECT_REQ_IND              =  6031,
  LM_MIDDLEWARE_MSG_ID_PD_INJECT_WIFI_AP_SCAN_IND     =     6032

} loc_middleware_msg_id_e_type;

#ifndef FEATURE_CGPS_DOG_HEARTBEAT
#define LOC_MIDDLEWARE_TIMER_ID_DOG  (1)
#endif
#define LOC_MIDDLEWARE_TIMER_ID_FIX  (2)
#define LOC_MIDDLEWARE_TIMER_ID_XTRA (3)
#define LOC_MIDDLEWARE_TIMER_ID_PA   (4)


#endif // FEATURE_LOC_API_INTERNAL_H
