/*============================================================================
 @file loc_batching.h

 loc MW Batching module

 GENERAL DESCRIPTION

 This file defines the Batching module in loc middleware.

 EXTERNALIZED FUNCTIONS


 INITIALIZATION AND SEQUENCING REQUIREMENTS

 Copyright (c) 2013 QUALCOMM Atheros Incorporated.
 All Rights Reserved. QUALCOMM Atheros Proprietary and Confidential

 Export of this technology or software is regulated by the U.S. Government.
 Diversion contrary to U.S. law prohibited.

 =============================================================================*/

/*============================================================================

 EDIT HISTORY FOR FILE

 This section contains comments describing changes made to the module.

 $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/gps/gnss/loc_mw/src/loc_batching.h#1 $
 $DateTime: 2015/01/27 06:42:19 $
 $Author: mplp4svc $

 when       who     what, where, why
 --------   ---     ----------------------------------------------------------
 07/25/13   bnk     Initial version

 =============================================================================*/

#ifndef _LOC_BATCHING_H_
#define _LOC_BATCHING_H_

#include "aries_os_api.h"
#include "location_service_v02.h"
#include "loc_client.h"


#define LOC_BATCHING_ADDITIONAL_SLOTS     20 /* additional batching slots for internal manipulation */

/* -----------------------------------------------------------------------*//**
@brief
  Function to free the allocated buffer memory and reset the client's batching
   related fields. 
  
@param[in]   client_ptr            Client Info Handle 

@retval    TRUE           Buffer memory release success
@retval    FALSE          Buffer memory release failed
                                  
*//* ------------------------------------------------------------------------*/
extern boolean loc_batching_free_and_reset(loc_client_info_s_type     *const client_ptr);


/* -----------------------------------------------------------------------*//**
@brief
  Function to allocate the buffer memory based on availability and control
   points request.
  
@param[in]   client_ptr            Client Info Handle 
@param[out]  p_GetBatchSizeInd     Get Batch Indication message to control point
@param[in]   client_batch_size_req Batch size requested by control point 
@param[in]   reqTransactionId      Transaction Id in request message by control point 

@retval    TRUE           Buffer memory alloc success
@retval    FALSE          Buffer memory alloc failed
                                  
*//* ------------------------------------------------------------------------*/
extern boolean locGetBatchSizeHandleAllocate(loc_client_info_s_type         *const client_ptr,
                                             qmiLocGetBatchSizeIndMsgT_v02  *const p_GetBatchSizeInd,
                                             uint32                         client_batch_size_req,
                                             uint32                         reqTransactionId); 


/* -----------------------------------------------------------------------*//**
@brief
  Utility function to send Get Batch Size Indication to control point. 
  
@param[in]   client_ptr            Client Info Handle 
@param[in]   p_getBatchSizeInd     Get Batch Indication message to control point

@retval    TRUE           locQmiShimSendInd is success
@retval    FALSE          locQmiShimSendInd is failed
                                  
*//* ------------------------------------------------------------------------*/
extern boolean loc_send_get_batch_size_ind(loc_client_info_s_type           *const client_ptr,
                                           qmiLocGetBatchSizeIndMsgT_v02    *const p_getBatchSizeInd);


/* -----------------------------------------------------------------------*//**
@brief
  Function to convert the parsed position report from LocAPI into Batched report. 
  
@param[in]   pz_ParsedPositionRpt  Pointer to parsed position report from LocAPI
@param[out]  pBatchedPosReport     Pointer to the Batched position report

@retval    TRUE           pz_ParsedPositionRpt is of final fix report type
@retval    FALSE          pz_ParsedPositionRpt is of intermediate fix report type
                                  
*//* ------------------------------------------------------------------------*/
extern boolean loc_convert_parsed_pos_report_to_batching(const loc_parsed_position_s_type   *const pz_ParsedPositionRpt,
                                                         qmiLocBatchedReportStructT_v02     *const pBatchedPosReport);


/* -----------------------------------------------------------------------*//**
@brief
  Function to handle position report from LocAPI. The function pushes the 
  batched fix into the LIFO buffer and notifies the control point depending
  on the event registration masks.
  
@param[in]   client_ptr            Client Info Handle 
@param[in]   batching_element      Pointer to the Batched position report

@retval    TRUE           push to LIFO buffer is success
@retval    FALSE          push to LIFO buffer is fail
                                  
*//* ------------------------------------------------------------------------*/
extern boolean loc_batching_handle_position_report(loc_client_info_s_type           *const client_ptr,
                                                   qmiLocBatchedReportStructT_v02   *const batching_element);


/* -----------------------------------------------------------------------*//**
@brief
  Utility function to send read from batch Indication to control point.
  
@param[in]   client_ptr            Client Info Handle 
@param[out]  p_readFromBatchInd    Read From Batch Indication message to control point
@param[in]   request_status        Status from lifo buffer pull operation
@param[in]   reqTransactionId      Transaction Id in request message by control point 

@retval    TRUE           locQmiShimSendInd is success
@retval    FALSE          locQmiShimSendInd is failed
                                  
*//* ------------------------------------------------------------------------*/
extern boolean loc_send_read_from_batch_ind(loc_client_info_s_type               *const client_ptr,
                                            qmiLocReadFromBatchIndMsgT_v02       *const p_readFromBatchInd,
                                            boolean                              request_status,
                                            uint32                               reqTransactionId); 


/* -----------------------------------------------------------------------*//**
@brief
  Utility function to send release batch Indication to control point.
  
@param[in]   client_ptr            Client Info Handle 
@param[out]  p_releaseBatchInd     Release Batch Indication message to control point
@param[in]   request_status        Status from buffer free operation
@param[in]   reqTransactionId      Transaction Id in request message by control point 


@retval    TRUE           locQmiShimSendInd is success
@retval    FALSE          locQmiShimSendInd is failed
                                  
*//* ------------------------------------------------------------------------*/
extern boolean loc_send_release_batch_ind(loc_client_info_s_type           *const client_ptr,
                                          qmiLocReleaseBatchIndMsgT_v02    *const p_releaseBatchInd,
                                          boolean                          request_status,
                                          uint32                           reqTransactionId); 


/* -----------------------------------------------------------------------*//**
@brief
  Utility function to send stop batch Indication to control point.
  
@param[in]   client_ptr            Client Info Handle 
@param[out]  p_StopBatchingInd     Stop Batch Indication message to control point
@param[in]   request_status        Status from stop session request to LocAPI
@param[in]   reqTransactionId      Transaction Id in request message by control point 


@retval    TRUE           locQmiShimSendInd is success
@retval    FALSE          locQmiShimSendInd is failed
                                  
*//* ------------------------------------------------------------------------*/
extern boolean loc_send_stop_batching_ind(loc_client_info_s_type           *const client_ptr,
                                          qmiLocStopBatchingIndMsgT_v02    *const p_StopBatchingInd,
                                          boolean                          request_status,
                                          uint32                           reqTransactionId); 


/* -----------------------------------------------------------------------*//**
@brief
  Function to handle read operation from the lifo buffer. 
  
@param[in]   client_ptr            Client Info Handle 
@param[in]   number_of_reads       Number of fixes to be read from batching buffer
@param[out]  pReadFromBatchInd     Indication with batched position reports


@retval    TRUE           number of reads field is valid
@retval    FALSE          number of reads field is not valid
                                  
*//* ------------------------------------------------------------------------*/
extern boolean loc_handle_batching_read(loc_client_info_s_type            *const client_ptr,
                                        int32                             number_of_reads,
                                        qmiLocReadFromBatchIndMsgT_v02    *const pReadFromBatchInd); 


#endif  //_LOC_BATCHING_H_

