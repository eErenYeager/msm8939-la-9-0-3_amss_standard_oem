#ifndef LOCATION_SERVICE_V02IMPL_H
#define LOCATION_SERVICE_V02IMPL_H
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

              L O C A T I O N _ S E R V I C E _ I M P L _ V 0 2  . h

GENERAL DESCRIPTION
  This is the file which defines the loc service Data structures.

  Copyright (c) 2010-2013 Qualcomm Technologies, Inc.
  All rights reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.


  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/gps/gnss/loc_mw/src/location_service_impl_v02.h#1 $
 *====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/* This file was generated with Tool version 6.5
   It was generated on: Sat Nov  2 2013 (Spin 0)
   From IDL File: location_service_v02.idl */

#include "qmi_si.h"
#ifdef __cplusplus
extern "C" {
#endif
const qmi_implemented_messages *loc_get_service_impl_v02 (void);


#ifdef __cplusplus
}
#endif
#endif

