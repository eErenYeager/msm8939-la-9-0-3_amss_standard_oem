/******************************************************************************
  @file: loc_wifi.h
  @brief: Location API WiFi positioning module

  DESCRIPTION
   Qualcomm Location API WiFi Positioning Module

  INITIALIZATION AND SEQUENCING REQUIREMENTS
   N/A

  -----------------------------------------------------------------------------
  Copyright (c) 2009-2011 Qualcomm Technologies Incorporated.
  All Rights Reserved. QUALCOMM Proprietary and Confidential.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/gps/gnss/loc_mw/src/loc_wifi.h#1 $
$DateTime: 2015/01/27 06:42:19 $

******************************************************************************/

/*=====================================================================
                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

when       who      what, where, why
--------   ---      -------------------------------------------------------

07/24/09   dx       Initial version
======================================================================*/

#ifndef LOC_WIFI_H
#define LOC_WIFI_H

#include "loc_api_2.h"

typedef struct
{
   boolean                     wifi_position_report_in_progress;
   loc_client_handle_type      pos_report_client_handle;
   loc_client_handle_type      status_report_client_handle;
} loc_wifi_module_data_s_type;

extern boolean loc_wifi_init(void);
extern int loc_wifi_process_ioctl(loc_client_handle_type  client_handle,
                                  loc_ioctl_e_type ioctl_type,
                                  const loc_ioctl_data_u_type* ioctl_data);

#endif /* LOC_WIFI_H */
