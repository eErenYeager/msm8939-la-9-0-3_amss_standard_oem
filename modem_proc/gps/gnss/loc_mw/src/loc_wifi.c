/******************************************************************************
  @file: loc_wifi.c
  @brief: Location API WiFi positioning module

  DESCRIPTION
   Qualcomm Location API WiFi Positioning Module

  INITIALIZATION AND SEQUENCING REQUIREMENTS
   N/A

  -----------------------------------------------------------------------------
  Copyright (c) 2009-2012 Qualcomm Technologies Incorporated.
  All Rights Reserved. QUALCOMM Proprietary and Confidential.
 
  Copyright (c) 2013-2014 QUALCOMM Atheros, Inc.
  All Rights Reserved.
  QCA Proprietary and Confidential. 
 
$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/gps/gnss/loc_mw/src/loc_wifi.c#1 $
$DateTime: 2015/01/27 06:42:19 $
******************************************************************************/

/*=====================================================================
                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

when       who      what, where, why
--------   ---      -------------------------------------------------------

07/24/09   dx       Initial version
======================================================================*/

#include "gps_variation.h"
#include "comdef.h"
#include "customer.h"
#include "target.h"

#include "msg.h"
#include "queue.h"

#include "aries_os_api.h"
#include "pdapi.h"
#include "pdsm_atl.h"
#include "loc_api_2.h"

#include "loc_task.h"
#include "loc_client.h"
#include "loc_pd.h"
#include "loc_conn.h"
#include "loc_xtra.h"
#include "loc_pa.h"
#include "loc_ni.h"
#include "loc_wifi.h"
#include "loc_api_internal.h"

#ifdef FEATURE_CGPS_QWIP

// Prototypes
static void loc_wifi_reset_module(void);
static void loc_wifi_event_cb (
      void *user_data,
      pdsm_pd_event_type pd_event,
      const pdsm_pd_info_s_type *pd_info_ptr
);

/*===========================================================================
FUNCTION loc_wifi_init

DESCRIPTION
   One time initializations for the WiFi module. This function is to be called 
   only once.

DEPENDENCIES
   N/A

RETURN VALUE
   True: success
   False: failed

SIDE EFFECTS
   N/A
===========================================================================*/
boolean loc_wifi_init()
{
   boolean                       ret_val = FALSE; /* default: not successful */
   pdsm_client_status_e_type     status = PDSM_CLIENT_OK;

   LOC_MSG_MED("loc_wifi_init: entered", 0, 0, 0);

   do { /* one time loop */

      // Check if this is valid when QWiP is updated
      LOC_MSG_HIGH("loc_wifi_init: installing a hook in PD client", 0, 0, 0);
      status = loc_pd_install_event_cb_hook(loc_wifi_event_cb);
      if (status != PDSM_CLIENT_OK)
      {
         LOC_MSG_ERROR ("loc_pd_install_event_cb_hook failed, error code = %d", status, 0, 0);
         break;
      }

      loc_middleware_data.wifi_client_id = pdsm_client_init(PDSM_CLIENT_TYPE_WIPER);
      if (loc_middleware_data.wifi_client_id == -1)
      {
         LOC_MSG_HIGH("loc_wifi_init: pdsm_client_init for WiFi failed", 0, 0, 0);
         // break;
      }


      /* Initialize the Critical Section */
      os_MutexInit( &loc_middleware_data.wifi_crit_sec, MUTEX_DATA_ONLY_CONTEXT );


      LOC_MSG_HIGH("loc_wifi_init: pdsm_client_init for WiFi successful, client id=%d",
            (int) loc_middleware_data.wifi_client_id, 0, 0);

      // Check if WiFi module needs callbacks
      /*
      status = pdsm_client_pd(wifi)_reg(loc_middleware_data.wifi_client_id,
                                    NULL,
                                    loc_wifi_event_cb,
                                    PDSM_CLIENT_EVENT_REG,
                                    PDSM_PD_EVENT_WPS_NEEDED,
                                    NULL);
      if (status != PDSM_CLIENT_OK)
      {
         pdsm_client_release(loc_middleware_data.wifi_client_id);
         LOC_MSG_ERROR ("pdsm_client_pd_reg failed, error code = %d", status, 0, 0);
         break;
      }

      status = pdsm_client_act(loc_middleware_data.wifi_client_id);
      if (status != PDSM_CLIENT_OK)
      {
         pdsm_client_release(loc_middleware_data.wifi_client_id);
         LOC_MSG_ERROR ("pdsm_client_act failed, error code =%d", status, 0, 0);
         break;
      }
      */

      // Clear states
      loc_wifi_reset_module();

      // Reaching here means success
      ret_val = TRUE;
   } while (0); /* one time loop */

   LOC_MSG_MED ("loc_wifi_init: returned %d", ret_val, 0, 0);
   return ret_val;
}

/*===========================================================================
FUNCTION loc_wifi_reset_module

DESCRIPTION
   This function resets the wifi module. It will be called when:
      - wifi cmd callback reports an error

DEPENDENCIES
   N/A

RETURN VALUE
   None

SIDE EFFECTS
   N/A
===========================================================================*/
static void loc_wifi_reset_module(void)
{
   loc_wifi_module_data_s_type*    wifi_module_data_ptr = &loc_middleware_data.wifi_module_data;

   LOC_MSG_HIGH ("loc_wifi_reset_module \n", 0, 0, 0);

   LOC_MW_ENTER_CRIT_SECTION (&loc_middleware_data.wifi_crit_sec);

   wifi_module_data_ptr->wifi_position_report_in_progress = FALSE;
   wifi_module_data_ptr->pos_report_client_handle = LOC_CLIENT_HANDLE_INVALID;

   LOC_MW_LEAVE_CRIT_SECTION (&loc_middleware_data.wifi_crit_sec);
}

/*===========================================================================
FUNCTION loc_wifi_cmd_cb

DESCRIPTION
   This function processes wiper position report injection callback

DEPENDENCIES
   N/A

RETURN VALUE
   None

SIDE EFFECTS
   N/A
===========================================================================*/
static void loc_wifi_cmd_cb (
      void                      *data_block_ptr, /* Pointer to client provided data block */
      pdsm_wiper_cmd_e_type      wiper_cmd,      /* Indicate which command is this error status for */
      pdsm_wiper_cmd_err_e_type  wiper_cmd_err   /* Command error code, indicating whether command
                                                    is rejected and the reason */
)
{
#if 0 /* XXX not processing any callback yet */
   loc_wifi_module_data_s_type*    wifi_module_data_ptr = &loc_middleware_data.wifi_module_data;
   int32                           ioctl_status;

   LOC_MSG_HIGH ("loc_wifi_cmd_cb: cmd=%d, err=%d\n", wiper_cmd, wiper_cmd, 0);

   LOC_MW_ENTER_CRIT_SECTION (&loc_middleware_data.wifi_crit_sec);

   switch (wiper_cmd)
   {
   // Log position report
   case PDSM_WIPER_CMD_LOG_POSITION_REPORT:
      ioctl_status = (wiper_cmd_err == PDSM_WIPER_CMD_ERR_NOERR ?
              LOC_API_SUCCESS : LOC_API_GENERAL_FAILURE);
      loc_client_queue_ioctl_callback(wifi_module_data_ptr->pos_report_client_handle,
            LOC_IOCTL_SEND_WIPER_POSITION_REPORT,
            ioctl_status,
            NULL);
      wifi_module_data_ptr->wifi_position_report_in_progress = FALSE;
      break;
   default:
      break;
   }
   LOC_MW_LEAVE_CRIT_SECTION (&loc_middleware_data.wifi_crit_sec);
#endif
}

/*===========================================================================
FUNCTION loc_wifi_process_wiper_pos_report

DESCRIPTION
   This function processes wiper position report injection

DEPENDENCIES
   N/A

RETURN VALUE
   Loc API return values (e.g., LOC_API_SUCCESS (or 0) for success)

SIDE EFFECTS
   N/A
===========================================================================*/
static int loc_wifi_process_wiper_pos_report
(
      loc_client_handle_type                   client_handle,
      const loc_wiper_position_report_s_type*  wiper_pos
)
{
   int                             i;
   loc_wifi_module_data_s_type*    wifi_module_data_ptr = &loc_middleware_data.wifi_module_data;
   t_wiper_position_report_ex_struct_type pos_report;
   int32                           ioctl_status = LOC_API_SUCCESS;

    memset ( &pos_report, 0, sizeof(pos_report) );

   /***************************
    *  Copying the data       *
    ***************************/

   pos_report.wiper_pos_report.wiper_valid_info_flag = 0;
   if(wiper_pos->wiper_valid_info_flag & LOC_WIPER_LOG_TIME_VALID)
   {
      pos_report.wiper_pos_report.wiper_valid_info_flag |= WIPER_LOG_TIME_VALID;
   }
   if(wiper_pos->wiper_valid_info_flag & LOC_WIPER_LOG_POS_VALID)
   {
      pos_report.wiper_pos_report.wiper_valid_info_flag |= WIPER_LOG_POS_VALID;
   }
   if(wiper_pos->wiper_valid_info_flag & LOC_WIPER_LOG_AP_SET_VALID)
   {
      pos_report.wiper_pos_report.wiper_valid_info_flag |= WIPER_LOG_AP_SET_VALID;
   }
   if(wiper_pos->wiper_valid_info_flag & LOC_WIPER_LOG_H_RELIABILITY_VALID)
   {
      pos_report.e_HoriRelIndicator = wiper_pos->hor_reliability;
   }
   else
   {
      pos_report.e_HoriRelIndicator = PDSM_POSITION_RELIABILITY_NOT_SET;
   }

   pos_report.wiper_pos_report.wiper_fix_time.slow_clock_count = wiper_pos->wiper_fix_time.slow_clock_count;

   pos_report.wiper_pos_report.wiper_fix_position.lat = wiper_pos->wiper_fix_position.lat;
   pos_report.wiper_pos_report.wiper_fix_position.lon = wiper_pos->wiper_fix_position.lon;
   pos_report.wiper_pos_report.wiper_fix_position.HEPE = wiper_pos->wiper_fix_position.HEPE;
   pos_report.wiper_pos_report.wiper_fix_position.num_of_aps_used = wiper_pos->wiper_fix_position.num_of_aps_used;
   pos_report.wiper_pos_report.wiper_fix_position.fix_error_code = wiper_pos->wiper_fix_position.fix_error_code;

   pos_report.wiper_pos_report.wiper_ap_set.num_of_aps = wiper_pos->wiper_ap_set.num_of_aps;
   for (i = 0; i < wiper_pos->wiper_ap_set.num_of_aps &&
               i < LOC_WIPER_MAX_REPORTED_APS_PER_LOG_MSG; i++)
   {
      if (sizeof( pos_report.wiper_pos_report.wiper_ap_set.ap_info[i].mac_addr ) ==
          sizeof( wiper_pos->wiper_ap_set.ap_info[i].mac_addr ))
      {
         memscpy(&pos_report.wiper_pos_report.wiper_ap_set.ap_info[i].mac_addr,
                sizeof( pos_report.wiper_pos_report.wiper_ap_set.ap_info[i].mac_addr),
                &wiper_pos->wiper_ap_set.ap_info[i].mac_addr,
                sizeof(wiper_pos->wiper_ap_set.ap_info[i].mac_addr));
      }
      else {
         LOC_MSG_ERROR("PDAPI and Loc API mismatch: mac_addr size %d!=%d",
               sizeof pos_report.wiper_pos_report.wiper_ap_set.ap_info[i].mac_addr,
               sizeof wiper_pos->wiper_ap_set.ap_info[i].mac_addr,
               0);
      }

      pos_report.wiper_pos_report.wiper_ap_set.ap_info[i].rssi = wiper_pos->wiper_ap_set.ap_info[i].rssi;
      pos_report.wiper_pos_report.wiper_ap_set.ap_info[i].channel = wiper_pos->wiper_ap_set.ap_info[i].channel;
      pos_report.wiper_pos_report.wiper_ap_set.ap_info[i].ap_qualifier = wiper_pos->wiper_ap_set.ap_info[i].ap_qualifier;
   }

   /***************************
    *  Makes the PDSM call    *
    ***************************/
   if (wifi_module_data_ptr->wifi_position_report_in_progress == TRUE)
   {
      ioctl_status = LOC_API_ENGINE_BUSY;
   }
   else {
      LOC_MW_ENTER_CRIT_SECTION(&loc_middleware_data.wifi_crit_sec);
      wifi_module_data_ptr->wifi_position_report_in_progress = TRUE;

      /* FIXME Handle call back to determine when reporting process ends */
      wifi_module_data_ptr->wifi_position_report_in_progress = FALSE;

      // Save the client handle for issuing callback later on
      wifi_module_data_ptr->pos_report_client_handle = client_handle;

      // XXX check if the client_id should be wifi_client_id
      pdsm_send_wiper_position_report_ex(loc_wifi_cmd_cb,
            loc_middleware_data.wifi_client_id,      // Client id of user
            NULL,                                    // Pointer to client data block
            &pos_report
            );

      wifi_module_data_ptr->wifi_position_report_in_progress = FALSE;
      wifi_module_data_ptr->pos_report_client_handle = LOC_CLIENT_HANDLE_INVALID;

      LOC_MW_LEAVE_CRIT_SECTION(&loc_middleware_data.wifi_crit_sec);
      ioctl_status = LOC_API_SUCCESS;
   }

   /* IOCTL Callback */
   loc_client_queue_ioctl_callback(client_handle,
               LOC_IOCTL_SEND_WIPER_POSITION_REPORT,
               ioctl_status,
               NULL);

   return ioctl_status;
}

/*===========================================================================
FUNCTION loc_wifi_process_wiper_status

DESCRIPTION
   This function processes wiper status report

DEPENDENCIES
   N/A

RETURN VALUE
   Loc API return values (e.g., LOC_API_SUCCESS (or 0) for success)

SIDE EFFECTS
   N/A
===========================================================================*/
static int loc_wifi_process_wiper_status
(
      loc_client_handle_type       client_handle,
      loc_wiper_status_e_type      wiper_status
)
{
   int                             ret_val = LOC_API_SUCCESS;
   boolean                         pdsm_wiper_status;
   int32                           ioctl_status;

   /***************************
    *  Makes the PDSM call    *
    ***************************/
   switch (wiper_status) {
   case LOC_WIPER_STATUS_AVAILABLE:
      pdsm_wiper_status = TRUE;
      break;
   case LOC_WIPER_STATUS_UNAVAILABLE:
      pdsm_wiper_status = FALSE;
      break;
   default:
      pdsm_wiper_status = FALSE;
   }

   pdsm_notify_wiper_status(pdsm_wiper_status);

   /* IOCTL Callback */
   ioctl_status = LOC_API_SUCCESS;
   loc_client_queue_ioctl_callback(client_handle,
         LOC_IOCTL_NOTIFY_WIPER_STATUS,
         ioctl_status,
         NULL);

   return ret_val;
}

/*===========================================================================
FUNCTION loc_wifi_process_ioctl

DESCRIPTION
   This function processes IOCTL commands for WiFi Positioning

DEPENDENCIES
   N/A

RETURN VALUE
   Loc API return values (e.g., LOC_API_SUCCESS (or 0) for success)

SIDE EFFECTS
   N/A
===========================================================================*/
int loc_wifi_process_ioctl (loc_client_handle_type       client_handle,
                            loc_ioctl_e_type             ioctl_type,
                            const loc_ioctl_data_u_type* ioctl_data)
{
   int ret_val = LOC_API_GENERAL_FAILURE; /* default */

   switch (ioctl_type)
   {
   case LOC_IOCTL_SEND_WIPER_POSITION_REPORT:
      if(NULL != ioctl_data)
      {
          ret_val = loc_wifi_process_wiper_pos_report(client_handle, &ioctl_data->wiper_pos);
      }
      else
      {
          ret_val = LOC_API_INVALID_PARAMETER;
      }
      break;

   case LOC_IOCTL_NOTIFY_WIPER_STATUS:
      if(NULL != ioctl_data)
      {
          ret_val = loc_wifi_process_wiper_status(client_handle, ioctl_data->wiper_status);
      }
      else
      {
          ret_val = LOC_API_INVALID_PARAMETER;
      }
      break;

   default:
      break;
   }

   return ret_val;
}

/*===========================================================================
FUNCTION loc_translate_qwip_pdsm_to_locapi

DESCRIPTION
   Translates pdsm_pd_qwip_data_s_type to loc_qwip_request_s_type

DEPENDENCIES
   N/A

RETURN VALUE
   TRUE   : successful
   FALSE  : failed

SIDE EFFECTS
   N/A
===========================================================================*/
static boolean loc_translate_qwip_pdsm_to_locapi(loc_qwip_request_s_type *loc_qwip,
      const pdsm_pd_qwip_data_s_type *pd_qwip)
{
   loc_qwip_request_e_type     request_type;

   switch (pd_qwip->request_type)
   {
   case PDSM_PD_QWIP_START_PERIODIC_HI_FREQ_FIXES:
      request_type = LOC_QWIP_START_PERIODIC_HI_FREQ_FIXES;
      break;
   case PDSM_PD_QWIP_START_PERIODIC_KEEP_WARM:
      request_type = LOC_QWIP_START_PERIODIC_KEEP_WARM;
      break;
   case PDSM_PD_QWIP_STOP_PERIODIC_FIXES:
      request_type = LOC_QWIP_STOP_PERIODIC_FIXES;
      break;
   case PDSM_PD_QWIP_SUSPEND:
      request_type = LOC_QWIP_SUSPEND;
      break;
   default:
      LOC_MSG_ERROR("loc_translate_qwip_pdsm_to_locapi failed, unknown request type: %d",
            pd_qwip->request_type, 0, 0);
      return FALSE;
   }

   loc_qwip->request_type = request_type;
   loc_qwip->tbf_ms = (uint16) pd_qwip->tbf_ms;

   return TRUE;
}

/*===========================================================================
FUNCTION loc_wifi_event_cb

DESCRIPTION
   PD event callback, this is where WPS_NEEDED event will come

DEPENDENCIES
   N/A

RETURN VALUE
   None

SIDE EFFECTS
   N/A
===========================================================================*/
static void loc_wifi_event_cb (void                         *user_data,
                             pdsm_pd_event_type             pd_event,
                             const pdsm_pd_info_s_type      *pd_info_ptr)
{
   const pdsm_pd_qwip_data_s_type  *qwip = &pd_info_ptr->pd_info.qwip_data;
   loc_cmd_nty_client_s_type * ptr_nty_client_data = NULL;

   // Enter log
   LOC_MSG_MED("loc_wifi_event_cb entered: psdm client id=%d, pd_event=%d",
         (int) pd_info_ptr->client_id,
         (int) pd_event, 0);

  // WPS needed event
  if (pd_event & PDSM_PD_EVENT_WPS_NEEDED)
  {
    // Send an IPC to LOC_MW task and then call the callback in LOC QMI SHIM 
    // instead of directly calling the callback in LOC QMI SHIM.
    LOC_MSG_MED ("loc_wifi_event_cb forwards PDSM_PD_EVENT_WPS_NEEDED request, type=%d",
                  (int) pd_info_ptr->pd_info.qwip_data.request_type, 0, 0);
    ptr_nty_client_data = (loc_cmd_nty_client_s_type *)loc_calloc(sizeof(loc_cmd_nty_client_s_type));

    if(NULL != ptr_nty_client_data)
    {
      ptr_nty_client_data->event_type = LOC_EVENT_WPS_NEEDED_REQUEST;
      // Fill in payload
      if (loc_translate_qwip_pdsm_to_locapi(&ptr_nty_client_data->event_data.qwip_request, qwip))
      {
        if(FALSE == loc_middleware_queue_ipc(LOC_HANDLE_REGISTERED_CLIENTS, 
                                             LOC_CMD_TYPE_NOTIFY_CLIENT, 
                                             (void*)ptr_nty_client_data))
        {
          LOC_MSG_ERROR("loc_wifi_event_cb: loc_middleware_queue_ipc failed ",
                         0, 0, 0);
          // reclaim resources allocated for the message
          loc_free(ptr_nty_client_data);
          ptr_nty_client_data = NULL;
        }
      }// end of loc_translate_qwip_pdsm_to_locapi
      else 
      {
        LOC_MSG_ERROR("loc_wifi_event_cb failed at processing PDSM_PD_WPS_NEEDED, type=%d",
                      (int) qwip->request_type, 0, 0);
      }
    }// end of if(NULL != ptr_nty_client_data) 
    else
    {
      LOC_MSG_ERROR("loc_wifi_event_cb failed at loc_calloc ",
                    0, 0, 0);
    }
  }// end of if (pd_event & PDSM_PD_EVENT_WPS_NEEDED)
}
#endif /* FEATURE_CGPS_QWIP */
