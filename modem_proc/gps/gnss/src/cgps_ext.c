
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                          C-GPS External Source file

GENERAL DESCRIPTION
  This module contains the interface functions exported outside the library

EXTERNALIZED FUNCTIONS
  cgpd_XoSupported


INITIALIZATION AND SEQUENCING REQUIREMENTS


Copyright (c) 2007-2012 by Qualcomm Technologies INCORPORATED. All Rights Reserved.

Export of this technology or software is regulated by the U.S. Government.
Diversion contrary to U.S. law prohibited.

Version Control

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/gps/gnss/src/cgps_ext.c#1 $
  $DateTime: 2015/01/27 06:42:19 $
  $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/23/09   jd      Use new GNSS RF APIs.
06/17/09   jd      Added FEATURE_TRM_API_V3 - GPS support for new TRM APIs 
                   done for SVDO and PAM
09/20/07   va      Create wrapper for clk_regime_cgps_freq_offset_ppt
09/07/07   va      Initial implementation.

*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*
 * Include files
*/
#include "gps_variation.h"
#include "cgps_api.h"
#include "gnss_api.h"
#ifndef FEATURE_GNSS_NAV_HW_VER5
#include "clkregim.h"
#endif
#ifdef FEATURE_XO_TASK
#include "tcxomgr_task.h"
#endif /* FEATURE_XO_TASK */
#if defined(FEATURE_BATTERY_CHARGER)
#include "charger.h"
#endif

/*
 * Constant definitions
*/

/*
 * Structure definitions
*/

/*
 * Local data declarations.
*/


/*
 * Local function prototypes
*/

/*=============================================================================

       Variables

=============================================================================*/



#ifndef FEATURE_GNSS_DYNAMIC_SP_MEM_ALLOCATION

#ifdef FEATURE_GPSONE_LOMEM
  #ifdef FEATURE_GPSONE_HIMEM
    #error FEATURE_GPSONE_LOMEM and FEATURE_GPSONE_HIMEM both defined.
  #endif
#endif

/* Signal processing memory to be used by CC.
 * This is allocated here since the size depends on FEATURE_GPSONE_HIMEM.
 * The objective is to deliver a single library regardless of the feature
 * definitions.  The buffer size must match meq_GaccGridBufSizeBytes.
*/
#ifdef FEATURE_GPSONE_HIMEM
   #ifdef FEATURE_ALIGNMENT_DIRECTIVE_SYNTAX2
       uint8 meu_SigProcMem[2246040+65652+256+42544+32+32+2880+224] __attribute__ ((aligned (32)));


   #else /* FEATURE_ALIGNMENT_DIRECTIVE_SYNTAX2 */

      ALIGN(32) uint8 meu_SigProcMem[2246040+65652+256+42544+32+32+2880+224];


#endif /* FEATURE_ALIGNMENT_DIRECTIVE_SYNTAX2 */
boolean mcu_HiMemFeature = TRUE;
#else /* FEATURE_GPSONE_HIMEM */
   #ifdef FEATURE_ALIGNMENT_DIRECTIVE_SYNTAX2
/* 1324440 - signal processing memory 
   65652+256+42544+32+32  - CP and GACC command queues (These used to be in
                            cc_task but moved here for relocatable modem
                            feature to allow us to assume that ALL memory
                            needed by CC for DM commands are in one continguous
                            block so that we can use the SAME offset when
                            converting from Virt To Phy address.
  +752                    - GTM can only be accessed at a word (32bit) boundary.
                            As a result fields of data structures being stored in
                            GTM had to be aligned. This increase inmemory reflects
    						the alignment.
  225280                 - Add this to signal processing memory to increase the number
    					   of tasks by 16
   15480                  - Increase the number of 2MS IQ sums buffer (both 1.5 and 40msec buffers)
                            by 5 for BDS D2
  11680+7584+512         - Add this to CP and GACC command buffer to increase number
    						of tasks by 16
  271360                 - Reduce the CP corr buffers from 70msec to 50msec. We are subtracting this number
                           to keep the history of changes.
                            
*/
uint8 meu_SigProcMem[1324440+65652+256+42544+32+32+2880+224+752+225280+11680+7584+512+15480-271360] __attribute__ ((aligned (32)));
#else  /* FEATURE_ALIGNMENT_DIRECTIVE_SYNTAX2 */
/* 1324440 - signal processing memory
   65652+256+42544+32+32  - CP and GACC command queues (These used to be in
                            cc_task but moved here for relocatable modem
                            feature to allow us to assume that ALL memory
                            needed by CC for DM commands are in one continguous
                            block so that we can use the SAME offset when
                            converting from Virt To Phy address.
  +752                    - GTM can only be accessed at a word (32bit) boundary.
                            As a result fields of data structures being stored in
                            GTM had to be aligned. This increase inmemory reflects
                            the alignment.
   225280                 - Add this to signal processing memory to increase the number
    						of tasks by 16
   15480                  - Increase the number of 2MS IQ sums buffer (both 1.5 and 40msec buffers)
                            by 5 for BDS D2
   11680+7584+512         - Add this to CP and GACC command buffer to increase number
                            of tasks by 16
  271360                 - Reduce the CP corr buffers from 70msec to 50msec. We are subtracting this number
                           to keep the history of changes.
*/
ALIGN(32) uint8 meu_SigProcMem[1324440+65652+256+42544+32+32+2880+224+752+225280+11680+7584+512+15480-271360];
#endif /* FEATURE_ALIGNMENT_DIRECTIVE_SYNTAX2 */
boolean mcu_HiMemFeature = FALSE;
#endif /* FEATURE_GPSONE_HIMEM */

#else /* FEATURE_GNSS_DYNAMIC_SP_MEM_ALLOCATION */

   #ifdef FEATURE_ALIGNMENT_DIRECTIVE_SYNTAX2
     uint8 meu_SigProcMem[512] __attribute__ ((aligned (32)));

   #else

   ALIGN(32) uint8 meu_SigProcMem[512];

   #endif

   boolean mcu_HiMemFeature = FALSE;
   

#endif /*FEATURE_STATIC_SP_MEM_ALLOCATION*/


/*****************************************************************************
 * API functions for C-GPS capabilities
 *****************************************************************************
*/

/*
 ******************************************************************************
 * Function cgps_XOSupported
 *
 * Description:
 *
 *  This function is a peek on C-GPS capability for XO Support
 *
 * Parameters:
 *
 *  None.
 *
 * Dependencies:
 *
 *  None
 *
 * Return value:
 *
 *  TRUE if XO is supported
 *  FALSE if XO is not supported and VCTCXO is supported.
 *
 ******************************************************************************
*/
boolean cgps_XoSupported( void )
{
  #ifdef FEATURE_XO
  return TRUE;
  #else
  return FALSE;
  #endif
}

/*
 ******************************************************************************
 * Function cgps_ClkRegimeFreqOffsetPpt
 *
 * Description:
 *
 *  This function calls the appropriate clk fuction for QSC targets,
 *  empty function otherwise.
 *
 * Parameters:
 *
 *  l_Offset - Freq Offset in ppt ( parts per trillion )
 *
 * Dependencies:
 *
 *  None
 *
 * Return value:
 *
 *  None
 *
 ******************************************************************************
*/
void cgps_ClkRegimeFreqOffsetPpt(int32 l_Offset)
{
  #if defined(T_QSC60X5) || defined(T_QSC6270)
  (void)clk_regime_cgps_freq_offset_ppt(l_Offset);
  #endif
}

/*
 ******************************************************************************
 * Function cgps_SigProcMemSize
 *
 * Description:
 *
 * This function performs check on the buffer size of the signal processing
 * memory meu_SigProcMem[].
 *
 *
 * Parameters:
 *
 *  None
 *
 * Dependencies:
 *
 *  None
 *
 * Return value:
 *
 *  Size of the the buffer meu_SigProcMem.
 *
 ******************************************************************************
*/
uint32 cgps_SigProcMemSize( void )
{
  return sizeof( meu_SigProcMem );
}

#ifndef FEATURE_GNSS_TCXOMGR
/*
 ******************************************************************************
 * Function cgps_XoEstimationState
 *
 * Description:
 *
 *  This function starts or stops XO frequency estimation loop
 *
 * Parameters: 
 *
 *  u_Enable : TRUE  : set start frequency estimation sig
 *             FALSE : set stop frequenmcy estimation sig
 *
 * Dependencies:
 * 
 *  None
 *
 * Return value: 
 *
 *  None
 *
 ******************************************************************************
*/
void cgps_XoEstimationState( boolean u_Enable )
{
  #ifdef FEATURE_XO_TASK
  if (cgps_XoSupported())
  {
    tcxomgr_set_sigs(u_Enable ? TCXOMGR_START_UPDATE_TIMER_SIG :
                     TCXOMGR_STOP_UPDATE_TIMER_SIG);
  }
  #endif /* FEATURE_XO_TASK */
}
#endif /* ! FEATURE_GNSS_TCXOMGR */

/*
 ******************************************************************************
 *
 * cgps_SetExternalChargerStatus( void )
 *
 * Function description:
 *  This function sets a static indication of the external charger status.
 *  The Charger status may be used by other functions to enable / disable functions
 *  (for example, the receiver may be configured to disable low power mode of operation
 *  when it is connected to charger to give better accuracy)
 *
 * Parameters: 
 *  e_ExtChargerStatus -- Indicates whether the receiver is connected to a charger
 * 
 * Return value: None
 *  
 *
 ******************************************************************************
*/

/* Static variable to store the external charger status */
static cgps_BatteryChargerStatusEnumType cgpse_ExtChargerStatus = CGPS_BATTERY_CHARGER_STATE_UNKNOWN;

void cgps_SetExternalChargerStatus( cgps_BatteryChargerStatusEnumType e_ExtChargerStatus )
{
  cgpse_ExtChargerStatus = e_ExtChargerStatus;
} /* end cgps_SetExternalChargerStatus() */

/*
 ******************************************************************************
 *
 * cgps_GetExternalChargerStatus( void )
 *
 * Function description:
 *  This function returns the value of external charger status.
 *
 * Parameters: 
 *  e_ExtChargerStatus -- Indicates whether the receiver is connected to a charger
 * 
 * Return value: None
 *  
 *
 ******************************************************************************
*/

static cgps_BatteryChargerStatusEnumType cgps_GetExternalChargerStatus( void  )
{
  return( cgpse_ExtChargerStatus );
} /* end cgps_GetExternalChargerStatus() */

/*
 ******************************************************************************
 *
 * cgps_GetChargerStatus( void )
 *
 * Function description:
 *  This function obtains the Battery Charger status. It checks whether a charger
 *  is connected or not, and if the phone is still charging or not. 
 *  Possible return values are
 *  CGPS_BATTERY_CHARGER_STATE_UNKNOWN 
 *  CGPS_BATTERY_CHARGER_NOT_CONNECTED
 *  CGPS_BATTERY_CHARGER_CONNECTED_NOT_CHARGED
 *  CGPS_BATTERY_CHARGER_CONNECTED_CHARGED
 *
 * Parameters: 
 *  
 * Return value: cgps_BatteryChargerStatus
 *  
 *
 ******************************************************************************
*/
cgps_BatteryChargerStatusEnumType cgps_GetChargerStatus( void )
{
  cgps_BatteryChargerStatusEnumType e_Status = CGPS_BATTERY_CHARGER_STATE_UNKNOWN;

#if defined(FEATURE_BATTERY_CHARGER) || defined(FEATURE_CHG_DAL)
  if( chg_is_charging() )
  {
    /* Batter Charger is connected. Now check it is is charging */
    if( CHG_UI_EVENT__DONE == chg_ui_event_read() ) 
    {
      /* Charger is connected and charging is complete */
      e_Status = CGPS_BATTERY_CHARGER_CONNECTED_CHARGED;
    }
    else
    {
      /* Charger is connected, but not completely charged */
      e_Status = CGPS_BATTERY_CHARGER_CONNECTED_NOT_CHARGED;
    }
  }
  else
  {
    /* Charger is not connected */
    e_Status = CGPS_BATTERY_CHARGER_NOT_CONNECTED;
  }
#endif /* defined(FEATURE_BATTERY_CHARGER) || defined(FEATURE_CHG_DAL) */
  
  /* If internal charger status is unknown, use external charger status
     This status may be provided by application or other custom charging API's 
  */
  if( CGPS_BATTERY_CHARGER_STATE_UNKNOWN == e_Status )
  {
    /* Get the charger status from an external source */
    e_Status = cgps_GetExternalChargerStatus();
  }

  return e_Status;

} /* end cgps_GetChargerStatus() */

/*
******************************************************************************
* gnss_UpdateWwanTxAntennaStatus
*
* Function description:
*
*  gnss_UpdateWwanTxAntennaStatus is used to update the status of which
*  TX Antenna is being used for WWAN transmissions. This antenna selection
*  control is needed to estimate the impact of the WLAN and WWAN Tx on GPS.
*  Different antennas provide different levels of isolation from the GPS band.
*  Based on the antenna used, GNSS receiver selects appropriate thresholds
*  for cross-correlation, or IMD technique to mitigate the impact.
*
*  This function should be called at start of a GPS session, and whenever the 
*  antenna selection is changed. 
*  The function updates a GNSS global variable to set the Tx Antenna status
*  and returns immediately without performing any further processing.
*  Note: If the device has only a single antenna for WWAN Tx, this API should 
*        be called with "WWAN_TX_ANTENNA_MAIN" as the antenna type.
*
* Parameters:
*
*  e_TxAntUsed - Indicates which Tx Antenna is selected.
*
* Return value:
*
*  void
*
******************************************************************************
*/

gnss_TxAntennaEnumType gnsse_TxAntUsed = WWAN_TX_ANTENNA_MAIN;

void gnss_UpdateWwanTxAntennaStatus( gnss_TxAntennaEnumType e_TxAntUsed )
{
  /* Update the Tx Antenna used */
  gnsse_TxAntUsed = e_TxAntUsed;
}

#ifdef FEATURE_NAVRF
/*
 ******************************************************************************
 *
 * gnss_SetExternalCtrlForELNA
 *
 * Function description:
 *  This function provides an API by which customer can modify
 *  ELNA configuration.
 *
 * Parameters: 
 *  e_LnaCfg -- ELNA config that customer opts to use
 * 
 * Return value: None
 *  
 *
 ******************************************************************************
*/

/* Customer needs to change this variable in order to override NAVRF driver ELNA setting */
gnss_ExternalLnaConfigStruct gnssz_ExtElnaCfg =
{ GNSS_RF_DEFAULT_CFG, C_GNSS_RF_ELNA_GPIO_NUM_DEFAULT, NULL }; 

void gnss_SetExternalCtrlForELNA(gnss_ExternalLnaConfigStruct z_LnaCfg)
{
  gnssz_ExtElnaCfg.e_ExtElnaCtrl = z_LnaCfg.e_ExtElnaCtrl;
  gnssz_ExtElnaCfg.u_GpioNumber = z_LnaCfg.u_GpioNumber;
  gnssz_ExtElnaCfg.p_ExternalLnaFunc = z_LnaCfg.p_ExternalLnaFunc;
} 

/*
  ******************************************************************************
  *
  * gnss_GetExternalCtrlForELNA
  *
  * Function description:
  *  This function is used to get ELNA config information. This will provide
  *  an interface by which driver knows if ELNA config information has been
  *  modified externally.
  *
  * Parameters:
  *   p_ElnaCfg
  *  
  * 
  * Return value:
  *   None
  *  
  *
  ******************************************************************************
*/

void gnss_GetExternalCtrlForELNA(gnss_ExternalLnaConfigStruct *p_ElnaCfg)
{
  p_ElnaCfg->e_ExtElnaCtrl = gnssz_ExtElnaCfg.e_ExtElnaCtrl;
  p_ElnaCfg->u_GpioNumber = gnssz_ExtElnaCfg.u_GpioNumber;
  p_ElnaCfg->p_ExternalLnaFunc = gnssz_ExtElnaCfg.p_ExternalLnaFunc;
}
#endif /* FEATURE_NAVRF */

