# -------------------------------------------------------------------------------- #
#                         G N S S _ M G P M E _ N A V R F . S C O N S
#
# DESCRIPTION
#   SCons file for the whole MGP subsystem. Defines the existence of MGP subsystem.
#
#
# INITIALIZATION AND SEQUENCING REQUIREMENTS
#   None.
#
#
# Copyright (c) 2014 Qualcomm Technologies Incorporated.
#
# All Rights Reserved. Qualcomm Confidential and Proprietary
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and the information contained therein are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
# --------------------------------------------------------------------------------- #

# --------------------------------------------------------------------------------- #
#
#                      EDIT HISTORY FOR FILE
#
# This section contains comments describing changes made to this file.
# Notice that changes are listed in reverse chronological order.
#
# $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/gps/gnss/mgp/me/navrf/build/gnss_mgpme_navrf.scons#1 $
# $DateTime: 2015/01/27 06:42:19 $
# 
# when         who     what, where, why
# ---------    ---     ------------------------------------------------------------
# 07/20/12     dmc     Initial Version
#
# --------------------------------------------------------------------------------- #

#--------------------------------------------------------------------------------
# Import the SCons environment
#--------------------------------------------------------------------------------
Import('env')

#--------------------------------------------------------------------------------
# Check USES flags and return if library isn't needed
#--------------------------------------------------------------------------------
if 'USES_CGPS' not in env:
    Return()

#--------------------------------------------------------------------------------
# Setup Debug preferences 
#--------------------------------------------------------------------------------
if ARGUMENTS.get('DEBUG_OFF','no') == 'yes':
    env.Replace(ARM_DBG     = "")
    env.Replace(HEXAGON_DBG = "")
    env.Replace(GCC_DBG     = "")

if ARGUMENTS.get('DEBUG_ON','no') == 'yes':
    env.Replace(ARM_DBG     = "-g --dwarf2") 
    env.Replace(HEXAGON_DBG = "-g")  
    env.Replace(GCC_DBG     = "-g")	
    
#-------------------------------------------------------------------------------
# Publish these private APIs for the rest of Nav RF
#------------------------------------------------------------------------------- 
NAVRF_PRIVATE_API = ['${GPS_ROOT}/gnss/mgp/me/navrf/common/inc',
                   '${GPS_ROOT}/gnss/mgp/me/navrf/chipset/inc',
                   '${GPS_ROOT}/gnss/mgp/me/navrf/device/inc',
                  ]   

if env['CHIPSET'] in ['msm8x10',
                      'msm8x26',
                      'msm8926',
                      'msm8962',
                     ]:
    NAVRF_PRIVATE_API.append('${GPS_ROOT}/gnss/mgp/me/navrf/adc/waverider/inc')
elif env['CHIPSET'] in ['apq8094',
                        'msm8994',
                        'mdm9x35',
                        'mdm9x45',
                       ]:
    NAVRF_PRIVATE_API.append('${GPS_ROOT}/gnss/mgp/me/navrf/adc/sinope/inc')
elif env['CHIPSET'] in ['msm8974',
                        'mdm9x25',
                       ]:
    NAVRF_PRIVATE_API.append('${GPS_ROOT}/gnss/mgp/me/navrf/adc/elara/inc')
elif env['CHIPSET'] in ['msm8916',
                       ]:
    NAVRF_PRIVATE_API.append('${GPS_ROOT}/gnss/mgp/me/navrf/adc/helike/inc')	
    NAVRF_PRIVATE_API.append('${GPS_ROOT}/gnss/mgp/me/navrf/device/euclid8cfr/inc')	
elif env['CHIPSET'] in ['msm8936',
                        'msm8939',
                       ]:
    NAVRF_PRIVATE_API.append('${GPS_ROOT}/gnss/mgp/me/navrf/adc/helike2/inc')
    NAVRF_PRIVATE_API.append('${GPS_ROOT}/gnss/mgp/me/navrf/device/euclid6bAdc/inc')

if 'USES_GNSS_SA' in env:
    NAVRF_PRIVATE_API.append('${GPS_ROOT}/gnss/mgp/me/navrf/stubs/inc')

env.PublishPrivateApi('NAVRF', NAVRF_PRIVATE_API)
env.RequirePrivateApi('NAVRF')

#--------------------------------------------------------------------------------
# Explicitly remove all source files from the NavRF folders
#--------------------------------------------------------------------------------   
env.CleanPack("CLEANPACK_TARGET", env.FindFiles('*.c', '..'))
env.CleanPack("CLEANPACK_TARGET", env.FindFiles('*.h', '..'))

env.LoadSoftwareUnits()
