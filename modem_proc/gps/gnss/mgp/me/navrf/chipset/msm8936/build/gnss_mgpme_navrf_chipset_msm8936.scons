# -------------------------------------------------------------------------------- #
#   G N S S _ M G P M E _ N A V R F _ C H I P S E T _ M S M 8 9 3 6 . S C O N S
#
# DESCRIPTION
#   SCons file for the GNSS MGP ME Nav RF subsystem.
#
#
# INITIALIZATION AND SEQUENCING REQUIREMENTS
#   None.
#
#
# Copyright (c) 2014 Qualcomm Technologies Incorporated.
#
# All Rights Reserved. Qualcomm Confidential and Proprietary
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and the information contained therein are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
# --------------------------------------------------------------------------------- #

# --------------------------------------------------------------------------------- #
#
#                      EDIT HISTORY FOR FILE
#
# This section contains comments describing changes made to this file.
# Notice that changes are listed in reverse chronological order.
#
# $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/gps/gnss/mgp/me/navrf/chipset/msm8936/build/gnss_mgpme_navrf_chipset_msm8936.scons#1 $
# $DateTime: 2015/01/27 06:42:19 $
# $Author: mplp4svc $
# 
# when          who     what, where, why
# ----------    ---     ------------------------------------------------------------
# 2014/04/07    dmc      Initial Version
#
# --------------------------------------------------------------------------------- #

#--------------------------------------------------------------------------------
# Import and clone the SCons environment
#--------------------------------------------------------------------------------
Import('env')
from glob import glob
from os.path import join, basename

#--------------------------------------------------------------------------------
# Check USES flags and return if library isn't needed
#--------------------------------------------------------------------------------
if 'USES_CGPS' not in env:
   Return()

if 'USES_GNSS_SA' in env:
   Return()

#--------------------------------------------------------------------------------
# Only clean this for certain targets
#--------------------------------------------------------------------------------   
if env['CHIPSET'] not in ['msm8916', 'msm8936', 'msm8939']:
   env.CleanPack('CLEANPACK_TARGET', env.FindFiles('*', '..'))

#--------------------------------------------------------------------------------
# Only build this for certain targets
#--------------------------------------------------------------------------------   
if env['CHIPSET'] not in ['msm8936']:
   Return()

#--------------------------------------------------------------------------------
# Setup Debug preferences 
#--------------------------------------------------------------------------------

if ARGUMENTS.get('DEBUG_OFF','no') == 'yes':
    env.Replace(ARM_DBG     = "")
    env.Replace(HEXAGON_DBG = "")
    env.Replace(GCC_DBG     = "")

if ARGUMENTS.get('DEBUG_ON','no') == 'yes':
    env.Replace(ARM_DBG     = "-g --dwarf2") 
    env.Replace(HEXAGON_DBG = "-g")  
    env.Replace(GCC_DBG     = "-g")

#--------------------------------------------------------------------------------
# Setup source PATH
#--------------------------------------------------------------------------------
SRCPATH = '../src'
env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

#--------------------------------------------------------------------------------
# Name of the subsystem to which this unit belongs
#--------------------------------------------------------------------------------
LIB_TARGET = '${BUILDPATH}/' + 'gnss_mgpme_navrf_chipset_msm8936'

#--------------------------------------------------------------------------------
# Set MSG_BT_SSID_DFLT for legacy MSG macros by removing previous version and adding new
# Definition
#--------------------------------------------------------------------------------
env.Replace(CPPDEFINES = [x for x in env['CPPDEFINES'] if not x.startswith("MSG_BT_SSID_DFLT=")] +
                                         ["MSG_BT_SSID_DFLT=MSG_SSID_MGPME"]) 

#--------------------------------------------------------------------------------
# Required Public, Restricted & Protected APIs
#--------------------------------------------------------------------------------
CORE_PUBLIC_APIS = [
    'KERNEL',
]


MODEM_PUBLIC_APIS = [
    'RFA',
    'UTILS',
]

MODEM_RESTRICTED_APIS = [
    'FW',
    'RFA',
	'RFDEVICE_INTERFACE',
    'UTILS',
]

GPS_PROTECTED_APIS = [
    'COMMON',
    'MGP',
	'MGP_ME',
    'NAVRF_CHIPSET',
    'NAVRF_COMMON',
	'NAVRF_DEVICE',
    'OS_API',    
]

#-------------------------------------------------------------------------------
# We need the Core BSP API's
#-------------------------------------------------------------------------------
env.RequirePublicApi(CORE_PUBLIC_APIS, area="CORE")

#-------------------------------------------------------------------------------
# We need MODEM PUBLIC API's
#-------------------------------------------------------------------------------
env.RequirePublicApi(MODEM_PUBLIC_APIS)

#-------------------------------------------------------------------------------
# We need different restricted API's within MODEM
#-------------------------------------------------------------------------------
env.RequireRestrictedApi(MODEM_RESTRICTED_APIS)

#-------------------------------------------------------------------------------
# We need our protected API's within GPS subsystem
#-------------------------------------------------------------------------------
env.RequireProtectedApi(GPS_PROTECTED_APIS)

#--------------------------------------------------------------------------------
# Images that this VU is added
#--------------------------------------------------------------------------------
IMAGES = ['MODEM_MODEM']

#--------------------------------------------------------------------------------
# Generate the library and add to an image
#--------------------------------------------------------------------------------
BINARY_LIB_SOURCES = ['${BUILDPATH}/' + basename(fname)
                      for fname in glob(join(env.subst(SRCPATH), '*.c'))]

#--------------------------------------------------------------------------------
# Ship our code as binary library and add it to the Modem image
#--------------------------------------------------------------------------------
env.AddBinaryLibrary(IMAGES, (LIB_TARGET + '_pvt'), BINARY_LIB_SOURCES)