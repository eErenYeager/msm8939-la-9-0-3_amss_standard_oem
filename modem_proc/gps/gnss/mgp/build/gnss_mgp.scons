# -------------------------------------------------------------------------------- #
#                         G N S S _ M G P . S C O N S
#
# DESCRIPTION
#   SCons file for the whole MGP subsystem. Defines the existence of MGP subsystem.
#
#
# INITIALIZATION AND SEQUENCING REQUIREMENTS
#   None.
#
#
# Copyright (c) 2010 Qualcomm Technologies Incorporated.
#
# All Rights Reserved. Qualcomm Confidential and Proprietary
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and the information contained therein are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
# --------------------------------------------------------------------------------- #

# --------------------------------------------------------------------------------- #
#
#                      EDIT HISTORY FOR FILE
#
# This section contains comments describing changes made to this file.
# Notice that changes are listed in reverse chronological order.
#
# $Header: //source/qcom/qct/modem/wcdma/main/latest/build/qscons/wcdma.scons#1 $
# $DateTime: 2010/04/06 13:31:09 $
# 
# when         who     what, where, why
# ---------    ---     ------------------------------------------------------------
# 10/06/10     ah      Initial Version
#
# --------------------------------------------------------------------------------- #

#--------------------------------------------------------------------------------
# Import the SCons environment
#--------------------------------------------------------------------------------
Import('env')

#--------------------------------------------------------------------------------
# Check USES flags and return if library isn't needed
#--------------------------------------------------------------------------------
if 'USES_CGPS' not in env:
    Return()

#--------------------------------------------------------------------------------
# Setup Debug preferences 
#--------------------------------------------------------------------------------
if ARGUMENTS.get('DEBUG_OFF','no') == 'yes':
    env.Replace(ARM_DBG     = "")
    env.Replace(HEXAGON_DBG = "")
    env.Replace(GCC_DBG     = "")

if ARGUMENTS.get('DEBUG_ON','no') == 'yes':
    env.Replace(ARM_DBG     = "-g --dwarf2") 
    env.Replace(HEXAGON_DBG = "-g")  
    env.Replace(GCC_DBG     = "-g")

#--------------------------------------------------------------------------------
# Add the proper MGP API's depending on the HW generation.   
#--------------------------------------------------------------------------------
MGP_PRIVATE_API = ['${GPS_ROOT}/gnss/mgp/common/inc',
                   '${GPS_ROOT}/gnss/mgp/me/inc',
                   '${GPS_ROOT}/gnss/mgp/pe/inc',
                  ]

# if NavHW Gen8A, do not add any folders 
# If NavHW Gen8B, then add the NavHW Gen8B folder.  
#   All the PLs are not defined yet, so this is the 'else' case.
# If NavHW Gen8C, then add the NavHW Gen8C folders
if env['CHIPSET'] in ['msm8960',
                      'msm8930',
                      'mdm9x15',
                     ]:
    env.PrintInfo("Nav HW Gen8A Target")
elif env['CHIPSET'] in ['apq8094',
                        'mdm9x35',
                        'mdm9x45',
                        'msm8994',
                        'msm8916',
						'msm8936',
						'msm8939',
                       ]:
    env.PrintInfo("Nav HW Gen8C Target")
    MGP_PRIVATE_API.append('${GPS_ROOT}/gnss/mgp/me/navhw/gen8c/inc')
else:
    env.PrintInfo("Nav HW Gen8B Target")
    MGP_PRIVATE_API.append('${GPS_ROOT}/gnss/mgp/me/navhw/gen8b/inc')

env.PublishPrivateApi('MGP', MGP_PRIVATE_API)
env.RequirePrivateApi('MGP')

#--------------------------------------------------------------------------------
# Find all of the .c and .h files in MGP and Clean them from being packed
#--------------------------------------------------------------------------------
GNSS_MGP_SHIP_FILES = ['mgp_nv.c',
                       'loc_wwan_me_api.h',
                       'mgp_api.h',
                       'mgp_nv.h',
                       'mgp_pe_sensors_api.h',
                       'navhw_api.h',
                      ]

# Parse the files to find the shipped and non-shipped files
black_list = env.FindFiles(['*.h', '*.c'], '..')
white_list = env.FindFiles(GNSS_MGP_SHIP_FILES, '..')
  
# This code does "filtered = black_list - white_list"
filtered = [x for x in black_list if x not in white_list]
env.CleanPack("CLEANPACK_TARGET", filtered, pack_exception=['USES_CUSTOMER_GENERATE_LIBS'])

env.LoadSoftwareUnits()
