#ifndef TM_L1_IFACE_H
#define TM_L1_IFACE_H


/*===========================================================================
  
                            TM_L1_IFACE
                   
DESCRIPTION:
  This header file contains TM L1 interface related constants, variables
  and function protocols.

  
Copyright (c) 2006 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/gps/gnss/sm/tm/src/tm_l1_iface.h#1 $

  when        who     what, where, why
  --------    ---     ------------------------------------------------------
  07/17/07    lt      Initial check-in.

===========================================================================*/

#include "gps_variation.h"
#include "comdef.h"

#ifdef FEATURE_USER_PLANE_MRL 


/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
/*                               DEFINES                                   */
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/


/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
/*                              DATA TYPES                                 */
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/


/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
/*                              FUNCTION DECLARATIONS                      */
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

/*===========================================================================

FUNCTION tm_l1_iface_proc_event

DESCRIPTION
  This function is used by TM to process an L1 Event Message.  This function
  runs within the context of the SM_TM task.

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None

===========================================================================*/
extern void tm_l1_iface_proc_event( void *p_msg );


#ifdef FEATURE_CGPS_LTE_CELLDB

/*===========================================================================

FUNCTION tm_lte_l1_iface_proc_event

DESCRIPTION
  This function is used by TM to process an LTE L1 Event Message. This function
  runs within the context of the SM_TM task.

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None

===========================================================================*/
extern void tm_lte_l1_iface_proc_event( void *p_msg );

#endif /* SUPL2 + LTE_CELL_DB */


/*===========================================================================

FUNCTION tm_l1_iface_init

DESCRIPTION
  This function initializes the TM-L1 IFACE module.

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None

===========================================================================*/
extern void tm_l1_iface_init( void );

#endif /* FEATURE_USER_PLANE_MRL  */

#endif /* TM_L1_IFACE_H */

