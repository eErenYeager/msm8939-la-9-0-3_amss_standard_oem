/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*=======*

            TM Periodic PPM Sub-module

General Description
  This file contains implementations for TM Periodic PPM Module

Copyright (c) 2010 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*=======*/

/*==============================================================================

                           Edit History
  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/gps/gnss/sm/tm/src/tm_periodic_ppm.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
==============================================================================*/
#include "gps_variation.h"
#include "comdef.h"
#include "customer.h"

#ifdef FEATURE_GNSS_PERIODIC_PPM
#error code not present
#endif /* FEATURE_GNSS_PERIODIC_PPM */
