#ifndef TM_OSYSINIT_H
#define TM_OSYSINIT_H
/*===========================================================================
                          TM_OSYSINIT.H

DESCRIPTION
  This header file contains function prototypes for the functions in 
  tm_osysinit.c.

Copyright (c) 2011 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/gps/gnss/sm/tm/umts_common/inc/tm_osysinit.h#1 $
  $DateTime: 2015/01/27 06:42:19 $
  $Author: mplp4svc $


===========================================================================*/

/* ==========================================================================
** Includes and Public Data Declarations
** ========================================================================*/

#include "uecomdef.h"
#include "rtxMemory.h"
#include "rtContext.h"
#include "rtxError.h"
#include "rtxPrintStream.h"

/* -----------------------------------------------------------------------
** Include Files
** ----------------------------------------------------------------------- */
#include "tm_asn1_common.h"

/* -----------------------------------------------------------------------
** Constant / Define Declarations
** ----------------------------------------------------------------------- */
#define NUM_PROTOS 5

/*! @brief OSYS PDU Types */
#define   	tm_osys_SUPL_Message_PDU 		1
#define     tm_osys_RRLP_Message_PDU 	2

/* -----------------------------------------------------------------------
** Type Declarations
** ----------------------------------------------------------------------- */

/* -----------------------------------------------------------------------
** Global Constant Data Declarations 
** ----------------------------------------------------------------------- */


/* -----------------------------------------------------------------------
** Global Data Declarations
** ----------------------------------------------------------------------- */


/* =======================================================================
**                          Macro Definitions
** ======================================================================= */


/* =======================================================================
**                        Function Declarations
** ======================================================================= */


void tm_osys_init( void );

#endif /* TM_OSYSINIT_H */
