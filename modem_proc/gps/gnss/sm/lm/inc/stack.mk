#----------------------------------------------------------------------------
#
# Copyright  (c)  2011 Atheros Communications Inc.  All Rights Reserved.
#
#----------------------------------------------------------------------------
#
#   \file        stack.mk
#   \brief       RM subsystem stack definition file
#   \version     $Id$
#   \author      $Author: mikav $
#

## ifeq - else ifeq : Conditions can be used for different configuration to
##                    define the platform specific stack size.
ifeq ("${ORION_ARCH}","xtensa")
	SUBS_LM_MAIN_STACK = 500
else
	SUBS_LM_MAIN_STACK = 8192
endif

## TOTAL_STACK : Add all stack definitions to TOTAL_STACK variable. 
##               This variable is used to calculate the full stack pool size.
TOTAL_STACK += ${SUBS_LM_MAIN_STACK}

