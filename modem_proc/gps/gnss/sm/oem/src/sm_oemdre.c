/*======================================================================

               OEM DRE main file

 GENERAL DESCRIPTION
  This file contains the implementation of OEM DR Enablement.
  This task enables OEMs to access following positioning information
  provided from the SM layer of GPS software (on modem processor).

 EXTERNALIZED FUNCTIONS


 INITIALIZATION AND SEQUENCING REQUIREMENTS


 Copyright (c) 2010,2011,2013 by Qualcomm Technologies INCORPORATED. All Rights Reserved.

 Export of this technology or software is regulated by the U.S. Government.
 Diversion contrary to U.S. law prohibited.
======================================================================*/

/*=====================================================================

 EDIT HISTORY FOR MODULE

 This section contains comments describing changes made to the module.
 Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/gps/gnss/sm/oem/src/sm_oemdre.c#1 $$DateTime: 2015/01/27 06:42:19 $$Author: mplp4svc $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
06/07/13   rh      Removed NO_FIX report handling (now BestAvailPos)  
02/10/10   vp      Initial version.
======================================================================*/

#include "sm_oemdre.h"
#include "sm_log.h"

/****************** Variables ************************************************/

/****************** Function Definitions**************************************/

/*
 ******************************************************************************
 * oemdreData
 *
 * Function description: Gets data from SM and makes it available for OEMs
 *
 * 1.	Position Fixes (Intermediate and Final position fixes coming from PE only)
 * 2.	Position Measurement Reports (1 Hz position measurement reports from ME)
 * 3.	SV Polynomials (from PE, 100 sec interval)
 *
 * Parameters: 
 *
 * Return value: 
 *
 ******************************************************************************
 */
void oemdreData(const oemdre_data* pOemdreData)
{
	if(NULL == pOemdreData)
	{
		return;
	}

	//Populate DM logs
	switch(pOemdreData->oemdreDataType)
	{
		case OEMDRE_MEAS:
		sm_log_oemdre_measurement_report(&(pOemdreData->u.oemdreMeas));
		break;

		case OEMDRE_POS_FIX:
		sm_log_oemdre_position_fix_report(&(pOemdreData->u.oemdrePosFix));
		break;

		case OEMDRE_SVPOLY:
		sm_log_oemdre_sv_poly_report(&(pOemdreData->u.oemdreSvPoly));
		break;

	    default:
	      break;

	}

	// OEM to put code their here

} /*oemdreData() ends*/
