/*===========================================================================

                  GM_MOTION Sensor Wifi Header File

DESCRIPTION
  This header file contains definitions of data structure necessary for
  GM MOTION Sensor Wifi

Copyright (c) 2012 by Qualcomm Technologies INCORPORATED. All Rights Reserved.

Export of this technology or software is regulated by the U.S. Government.
Diversion contrary to U.S. law prohibited.

===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header:

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/31/14   ss    Supporting multiple clients for motion sensing
05/29/13   yg   Initial creation of file.
===========================================================================*/
#ifndef GM_MOTION_SENSOR_WIFI_H
#define GM_MOTION_SENSOR_WIFI_H

#include "gm_motion_sensor.h"


/*Enum of Motion states*/
typedef enum
{
  GM_MOTION_SENSOR_WIFI_STATE_STOP, /*Motion Sensing Not Running*/
  GM_MOTION_SENSOR_WIFI_STATE_STARTING, /*Motion Sensing Starting*/
  GM_MOTION_SENSOR_WIFI_STATE_STARTED, /*Motion Sensing Started*/
} gm_motion_sensor_wifi_state_e_type;

#define GM_MOTION_SENSOR_WIFI_OVERLAP_HALF_WIFI_RANGE 50
#define GM_MOTION_SENSOR_WIFI_OVERLAP_WALK_THRES 25
#define GM_MOTION_SENSOR_WIFI_OVERLAP_DRIVE_THRES 10

#define GM_MOTION_SENSOR_WIFI_MAX_WIFI_RANGE 1000 /*Worst case 1KM assumed*/

#define GM_MOTION_SENSOR_WIFI_LAST_AP_REINSTATE_THRES 30
#define GM_MOTION_SENSOR_WIFI_MIN_AP_FOR_MOTION_DETECTION 5

void gm_motion_sensor_wifi_init(void);
boolean gm_motion_sensor_wifi_start(float distance_to_check);
boolean gm_motion_sensor_wifi_stop(void);
void gm_motion_sensor_wifi_feed_wifi_ind(t_wiper_position_report_ex_struct_type *p_gm_wifi_ind);
boolean gm_motion_sensor_wifi_validate_scan(t_wiper_position_report_struct_type* scan);
void gm_motion_sensor_wifi_handle_distance_timer_expiry(void);
int gm_motion_sensor_wifi_calculate_similarity(t_wiper_ap_set_struct_type *wifi_ap_info_start, 
                                               t_wiper_ap_set_struct_type *wifi_ap_info_injected);
void gm_motion_sensor_wifi_start_distance_timer(float distance);
void gm_motion_sensor_wifi_save_ap_set(t_wiper_ap_set_struct_type* injected_ap_info);
t_wiper_ap_set_struct_type* gm_motion_sensor_wifi_get_last_ap_set(void);
float gm_motion_sensor_wifi_distance_travelled(void);

#endif /*GM_MOTION_SENSOR_WIFI_H*/
