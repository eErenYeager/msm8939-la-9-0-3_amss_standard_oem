/*===========================================================================

                  GM_LOG Header File

DESCRIPTION
  This header file contains definitions of data structure necessary for
  GM Log.

Copyright (c) 2013 by Qualcomm Technologies INCORPORATED. All Rights Reserved.

Export of this technology or software is regulated by the U.S. Government.
Diversion contrary to U.S. law prohibited.

===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header:

when       who     what, where, why
--------   ---     ----------------------------------------------------------
10/04/13   ss   Initial version
===========================================================================*/
#ifndef GM_LOG_H
#define GM_LOG_H

#include "gm_core.h"

/****************** Data Structures*********************************************/
typedef enum
{
  GEOFENCE_MOTION_DETECTOR_NONE = 0,
  GEOFENCE_MOTION_DETECTOR_AMD_RMD_PED = 0x1,
  GEOFENCE_MOTION_DETECTOR_CMC = 0x2,
  GEOFENCE_MOTION_DETECTOR_WIFI = 0x4
} geofence_motion_detector_type;

typedef PACKED struct PACKED_POST
{
  geofence_motion_detector_type detectors_used; /*Motion detector used*/
  uint32 distance_to_check; /*Distance for motion check*/
} geofence_motion_detection_start_rpt;

typedef PACKED struct PACKED_POST
{
  geofence_motion_detector_type motion_detector; /*Motion detector*/
} geofence_motion_detection_stop_rpt;

typedef PACKED struct PACKED_POST
{
  geofence_motion_detector_type motion_detector; /*Motion detector*/
} geofence_motion_detection_distance_met_rpt;

typedef PACKED struct PACKED_POST
{
  geofence_motion_detector_type motion_detector; /*Motion detector*/
} geofence_motion_detection_error_rpt;

typedef PACKED struct PACKED_POST
{
  geofence_motion_detector_type motion_detector; /*Motion detector*/
  uint32 distance_accumulated;   /*Distance accumulated thus far*/
} geofence_motion_detection_distance_accum_rpt;

typedef enum
{
  GEOFENCE_MOTION_DETECTION_START_RPT = 0x1,
  GEOFENCE_MOTION_DETECTION_STOP_RPT,
  GEOFENCE_MOTION_DETECTION_ERROR_RPT,
  GEOFENCE_MOTION_DETECTION_DISTANCE_MET_RPT,
  GEOFENCE_MOTION_DETECTION_DIST_ACCUM_RPT
} geofence_motion_report_type;

typedef PACKED struct PACKED_POST
{
  geofence_motion_report_type report_type;
  union
  {
   geofence_motion_detection_start_rpt start_rpt;   /* Motion Start Report */
   geofence_motion_detection_stop_rpt stop_rpt;   /* Motion Stop Report */
   geofence_motion_detection_error_rpt error_rpt; /* Motion Error Report */
   geofence_motion_detection_distance_met_rpt distance_met_rpt; /* Distance Met Report */
   geofence_motion_detection_distance_accum_rpt dist_accum_rpt; /* Distance Accumulated Report */
  } motion_rpt_type;
} geofence_motion_detection_rpt;

typedef PACKED struct PACKED_POST
{
  log_hdr_type xx_hdr;
  uint8       u_Version;                /* Version number of DM log */
  uint32      q_Fcount;                 /* Local millisecond counter */

  geofence_motion_detection_rpt motion_detection_rpt;
}geofence_motion_detection_log_rpt;


typedef PACKED struct PACKED_POST
{
  log_hdr_type xx_hdr;
  uint8       u_Version;                /* Version number of DM log */
  uint32      q_Fcount;                 /* Local millisecond counter */

  sm_gm_pos_fix_src pos_fix_src;        /*Position Fix src*/
  double            latitude;          /*Latitude in Degrees*/
  double            longitude;         /*Longitude in Degrees*/
  double            altitude;          /*Altitude*/
  double            pos_unc_2d;        /*2D position uncertainity*/
  uint8             pos_unc_2d_conf;   /*2D position uncertainity confidence*/
  float             heading_radians;   /*Heading in Radians*/
  uint16            w_GpsWeek;         /* GPS week number of fix */
  uint32            q_GpsTimeMs;       /* GPS time in week (milliseconds) of fix */
}geofence_position_log_rpt;

/****************** Function Declarations**************************************/
/*===========================================================================
FUNCTION gm_log_motion_sensing

DESCRIPTION
  This functionlogs Geofencing motion sensing events

DEPENDENCIES

RETURN VALUE

SIDE EFFECTS

===========================================================================*/
void gm_log_motion_sensing(geofence_motion_detection_rpt motion_detection_report);

/*===========================================================================
FUNCTION gm_log_position

DESCRIPTION
  This functionlogs logs positions received by Geofencing

DEPENDENCIES

RETURN VALUE

SIDE EFFECTS

===========================================================================*/
void gm_log_position(sm_gm_position_notification_type *gm_position);


#endif /*GM_LOG_H*/



