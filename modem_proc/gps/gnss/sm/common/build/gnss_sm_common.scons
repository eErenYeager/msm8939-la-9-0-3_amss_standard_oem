# -------------------------------------------------------------------------------- #
#                     G N S S _ S M _ C O M M O N . S C O N S
#
# DESCRIPTION
#   SCons file for the GNSS SM Common subsystem.
#
#
# INITIALIZATION AND SEQUENCING REQUIREMENTS
#   None.
#
#
# Copyright (c) 2010-2011 Qualcomm Technologies Incorporated.
#
# All Rights Reserved. Qualcomm Confidential and Proprietary
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and the information contained therein are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
# --------------------------------------------------------------------------------- #

# --------------------------------------------------------------------------------- #
#
#                      EDIT HISTORY FOR FILE
#
# This section contains comments describing changes made to this file.
# Notice that changes are listed in reverse chronological order.
#
# $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/gps/gnss/sm/common/build/gnss_sm_common.scons#1 $
# $DateTime: 2015/01/27 06:42:19 $
# 
# when         who     what, where, why
# ---------    ---     ------------------------------------------------------------
# 10/06/10     ah      Initial Version
#
# --------------------------------------------------------------------------------- #

#--------------------------------------------------------------------------------
# Import and clone the SCons environment
#--------------------------------------------------------------------------------
Import('env')
from glob import glob
from os.path import join, basename

#--------------------------------------------------------------------------------
# Check USES flags and return if library isn't needed
#--------------------------------------------------------------------------------
if 'USES_CGPS' not in env:
    Return()

#--------------------------------------------------------------------------------
# Setup Debug preferences 
#--------------------------------------------------------------------------------
if ARGUMENTS.get('DEBUG_OFF','no') == 'yes':
    env.Replace(ARM_DBG     = "")
    env.Replace(HEXAGON_DBG = "")
    env.Replace(GCC_DBG     = "")

if ARGUMENTS.get('DEBUG_ON','no') == 'yes':
    env.Replace(ARM_DBG     = "-g --dwarf2") 
    env.Replace(HEXAGON_DBG = "-g")  
    env.Replace(GCC_DBG     = "-g")

#--------------------------------------------------------------------------------
# Setup source PATH
#--------------------------------------------------------------------------------
SRCPATH = '../src'
env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

#--------------------------------------------------------------------------------
# Name of the subsystem to which this unit belongs
#--------------------------------------------------------------------------------
LIB_TARGET = '${BUILDPATH}/' + 'gnss_sm_common'

#--------------------------------------------------------------------------------
# Set MSG_BT_SSID_DFLT for legacy MSG macros by removing previous version and adding new
# Definition
#--------------------------------------------------------------------------------
env.Replace(CPPDEFINES = [x for x in env['CPPDEFINES'] if not x.startswith("MSG_BT_SSID_DFLT=")] +
                                         ["MSG_BT_SSID_DFLT=MSG_SSID_GPSSM"]) 

#--------------------------------------------------------------------------------
# Images that this VU is added
#--------------------------------------------------------------------------------
IMAGES = ['MODEM_MODEM']

#--------------------------------------------------------------------------------
# Generate the library and add to an image
#--------------------------------------------------------------------------------
LIB_SOURCES        = ['${BUILDPATH}/' + basename(fname)
                      for fname in glob(join(env.subst(SRCPATH), '*.c'))]
#Compile out msgr_task related file which is not required for SA targets
gnss_msgr_task = '${BUILDPATH}/gnss_msgr_task.c'
if 'USES_GNSS_SA' in env:
  if LIB_SOURCES.count(gnss_msgr_task) > 0:
    LIB_SOURCES.remove(gnss_msgr_task)

#--------------------------------------------------------------------------------
# Add our library to the Modem image
#--------------------------------------------------------------------------------
env.AddLibrary(IMAGES, LIB_TARGET, LIB_SOURCES)

#--------------------------------------------------------------------------------
# GSS uses RCINIT and does not want this task for SA target
#--------------------------------------------------------------------------------
if 'USES_GNSS_SA' in env:
	Return()
#--------------------------------------------------------------------------------
# Build products for RCINIT
# GSS uses RCINIT and does not want this task for SA target
#--------------------------------------------------------------------------------
RCINIT_TASK_FN = {
    'thread_name'         : 'gnss_msgr',
    'stack_size_bytes'    : '16384',
    'sequence_group'      : env.subst('$MODEM_UPPERLAYER'),
    'priority_amss_order' : 'SM_TM_PRI_ORDER',
    'cpu_affinity'        : 'REX_ANY_CPU_AFFINITY_MASK',
    'policy_optin'        : ['default', 'ftm', ],
}
env.AddRCInitTask (IMAGES, RCINIT_TASK_FN)

