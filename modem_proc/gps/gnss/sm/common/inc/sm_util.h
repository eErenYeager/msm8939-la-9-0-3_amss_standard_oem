#ifndef SM_UTIL_H
#define SM_UTIL_H
/*============================================================================
  FILE:         sm_util.h

  OVERVIEW:     


  DEPENDENCIES: If the code in this file has any notable dependencies,
                describe them here.  Any initialization and sequencing
                requirements, or assumptions about the overall state of
                the system belong here.
 
                Copyright (c) 2009 - 2013 Qualcomm Technologies Incorporated.
                All Rights Reserved.
                Qualcomm Confidential and Proprietary
============================================================================*/

/*============================================================================
  EDIT HISTORY FOR MODULE

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/gps/gnss/sm/common/inc/sm_util.h#1 $
  $DateTime: 2015/01/27 06:42:19 $
  $Author: mplp4svc $

  when        who  what, where, why
  ----------  ---  -----------------------------------------------------------

============================================================================*/

/*------------------------------------------------------------------------------
File comment
------------------------------------------------------------------------------*/

/**
* @file sm_util.h
    
* file containing useful functions for SM module, e.g.
* translating between Gen7 and Gen8 structs
 
*/
#include "gps_variation.h"
#include <gnss_common.h>

/* converts the given argument to boolean */
#define SM_UTIL_MAKE_BOOL(arg) (!!(arg))

/**
* @brief  converts GNSS time to gps time
* 
* @return  boolean. false on failure
* @sideeffects None
* @see
* 
*/
boolean 
sm_translate_gnss_time_to_gps_time(
   sm_ReportGpsTimeStructType *p_Dest, 
   const sm_ReportGnssTimeStructType *p_Src);

/**
* @brief  converts GNSS assistance data to internal assistance
*         data type. this function is used to help bring about
*         changes in internal code in phases.
* 
* @return  boolean. false on failure
* @sideeffects None
* @see
* 
*/
boolean 
sm_translate_gnss_assist_data_to_internal_assist_data( sm_InternalAssistDataStatusStructType* p_Dest,
                                                       const sm_GnssAssistDataStatusStructType *p_Src);

boolean
sm_translate_sm_fix_report_to_gnss_nav_solution(
   gnss_NavSolutionStructType* p_dest, 
   const sm_GnssFixRptStructType* p_src,
   boolean b_FinalFix,
   boolean b_backgroundFix);


/**
 * Translates sm's new SVInfo structure into the old array which
 * is defined in gnss nav solution structure so 
 * that it may be used to generate old-style log 
 * @param p_destSVInfo
 * @param p_srcSVInfo 
 * @param q_validBitmask  all bits must be present (if more than 
 *                        one bit)
 * 
 * @return uint32  number of SVs populated in destSVInfo array
 */
uint32
sm_generate_sv_array_from_svinfo_struct(
   uint8 *p_destSV,
   uint32 q_destMaxNum,
   const sm_GnssSvInfoType *p_srcSVInfo,
   uint32 q_srcMaxNum,
   uint32 q_validBitmask);

#endif /* SM_UTIL_H */
