#ifndef LOC_LTE_OTDOA_API_H
#define LOC_LTE_OTDOA_API_H
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                     Location LTE OTDOA API Header File

GENERAL DESCRIPTION
This file contains API definitions between Location LTE OTDOA module and
other modules outside GNSS.

Copyright (c) 2011 by Qualcomm Technologies INCORPORATED. All Rights Reserved.

Export of this technology or software is regulated by the U.S. Government.
Diversion contrary to U.S. law prohibited.

Version Control

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/gps/api/loc_lte_otdoa_api.h#1 $
$DateTime: 2015/01/27 06:42:19 $
$Author: mplp4svc $

*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/


/*--------------------------------------------------------------------------
 * Include Files
 *-----------------------------------------------------------------------*/

#include "comdef.h"
#include "lte_as.h"

/*--------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -----------------------------------------------------------------------*/

/* maximum number of searches possible per PRS occasion */
#define LOC_LTE_OTDOA_MAX_HYPO_SRCH_PER_OCCASION    4

/*--------------------------------------------------------------------------
 * Type Declarations
 *-----------------------------------------------------------------------*/
typedef enum
{
  /* error because GPS told ML1 to stop */
  LOC_LTE_OTDOA_ERROR_GPS_ABORT = 0,

  /* error because ML1 ran out of PRS occassions */
  LOC_LTE_OTDOA_ERROR_NO_PRS_OCCASIONS,

  /* error because ML1 had a configuration error */
  LOC_LTE_OTDOA_ERROR_CONFIG,

  /* error because ML1 cannot detect the reference cell */
  LOC_LTE_OTDOA_ERROR_NO_REF_CELL,

  /* error because ML1 dosen't have a serving cell (no LTE service) */
  LOC_LTE_OTDOA_ERROR_NO_SERV_CELL,

  /* error because reference cell is unsupported (inter-freq) */
  LOC_LTE_OTDOA_ERROR_UNSUPPORTED_REF_CELL,

  /* undefined error when cause is unknown */
  LOC_LTE_OTDOA_ERROR_UNDEFINED
}
loc_lte_otdoa_PrsErrorInfoType;

/* LTE support bandwidths */
typedef enum
{
  /* 1.4MHz bandwidth */
  LLOC_PRS_BW_6 = 0,
  /* 3MHz bandwidth */
  LLOC_PRS_BW_15,
  /* 5MHz bandwidth */
  LLOC_PRS_BW_25,
  /* 10MHz bandwidth */
  LLOC_PRS_BW_50,
  /* 15MHz bandwidth */
  LLOC_PRS_BW_75,
  /* 20MHz bandwidth */
  LLOC_PRS_BW_100,
  /* Unknown Bandwidth */
  LLOC_PRS_BW_UNKNOWN
}
loc_lte_otdoa_PrsBandwidthType;

typedef struct
{
  /* enum of type 'loc_lte_otdoa_PrsErrorInfoType' */
  uint8 e_Error;
}
loc_lte_otdoa_MeasErrorInfoType;

/*
  Supported cyclix prefix modes
  Defined in 36.211 Table 6.12-1 for normal and extended mode.
*/
typedef enum
{
  /* Normal CP */
  LOC_LTE_OTDOA_CP_MODE_NORMAL = 0,

  /* Extended CP */
  LOC_LTE_OTDOA_CP_MODE_EXTENDED,

  /* Long CP for MBSFN */
  LOC_LTE_OTDOA_CP_MODE_EXTENDED_MBSFN,

  LOC_LTE_OTDOA_CP_MODE_MAX
}
loc_lte_otdoa_CpModeType;

typedef enum
{
  /* used by ML1 to provide serving cell info at start of PRS meas. */
  LOC_LTE_OTDOA_SERVING_CELL_AT_SESSION_START = 0,

  /* used by ML1 to indicate that seving cell has changed during PRS occasion */
  LOC_LTE_OTDOA_SERVING_CELL_SWITCH
}
loc_lte_otdoa_ServingCellContextType;

typedef struct
{
  /* Physical cell id */
  uint16 w_PhyCellId;

  /* E-ARFCN */
  lte_earfcn_t w_Earfcn;

  /* PRS bandwidth in RBs */
  uint8 u_Bandwidth;

  /* Cyclic prefix. Enum of type 'loc_lte_otdoa_CpModeType' */
  uint8 e_CpMode;

  /* System Frame Number (SFN) of the last PRS occasion */
  uint16 w_Sfn;

  /* Enum to indicate when this info is passed to OTDOA SW by ML1.
   * Type 'loc_lte_otdoa_ServingCellContextType' */
  uint8 e_Context;

  /* BW of serving cell in enum. Type 'loc_lte_otdoa_PrsBandwidthType' */
  uint8 e_ServingBw;
}
loc_lte_otdoa_ServingCellInfoType;

typedef struct
{
  /* Physical cell id */
  uint16 w_PhyCellId;

  /* number of hypothesis reqd. for this particular cell */
  uint8 u_NumHypPerCell;

  /* Hypothesis sequence number. Range - 1 to 'u_NumHypPerCell' */
  uint8 u_HypSeqNum;

  /* PRS BW used to measure this cell. Type 'loc_lte_otdoa_PrsBandwidthType' */
  uint8 e_PrsBw;
}
loc_lte_otdoa_PrsMeasHypoSrchInfo;

typedef struct
{
  /* Pointer to the shared memory block where the PRS occasion meas. are stored */
  void *p_PrsMeasData;

  /* Sequence number of the data block. Used for sanity chk */
  uint32 q_OccasionSeqNum;

  /* total number of hypothesis searched in this PRS occasion */
  uint8 u_NumHyp;

  /* System Frame Number (SFN) of this PRS occasion */
  uint16 w_Sfn;

  /* Info. on each of the hypothesis in this PRS occasion */
  loc_lte_otdoa_PrsMeasHypoSrchInfo z_HypoInfo[LOC_LTE_OTDOA_MAX_HYPO_SRCH_PER_OCCASION];
}
loc_lte_otdoa_PrsOccasionMeasInfoType;

/*--------------------------------------------------------------------------
 * Function Declarations
 *-----------------------------------------------------------------------*/
/*
 ******************************************************************************
 * Function: loc_lte_otdoa_MeasError
 *
 * Description:
 *  This function informs LSW (loc software) if there are any error during PRS meas.
 *
 * Parameters:
 *  Complete status.
 *
 * Dependencies:
 *  None
 *
 * Return value:
 *  None.
 *
 ******************************************************************************
 */
extern void loc_lte_otdoa_MeasError (loc_lte_otdoa_MeasErrorInfoType *p_MeasErrorInfo);

/*
 ******************************************************************************
 * Function: loc_lte_otdoa_MeasResultsAvail
 *
 * Description:
 *  This function informs LSW (loc software) that measurement results are
 *  available in shared memory
 *
 * Parameters:
 *  Sequence number of the available OTDOA measurements
 *
 * Dependencies:
 *  None
 *
 * Return value:
 *  None.
 *
 ******************************************************************************
 */
extern void loc_lte_otdoa_MeasResultsAvail (loc_lte_otdoa_PrsOccasionMeasInfoType *p_MeasResultsInfo);

/*
 ******************************************************************************
 * Function: loc_lte_otdoa_UpdateServingCell
 *
 * Description:
 *  This function informs LSW (loc software) about the serving cell information. This
 *  will also be used by ML1 for informing about a cell switch (change of serving cell)
 *
 * Parameters:
 *  Downlink system information of the serving cell
 *
 * Dependencies:
 *  None
 *
 * Return value:
 *  None.
 *
 ******************************************************************************
 */
extern void loc_lte_otdoa_UpdateServingCell (loc_lte_otdoa_ServingCellInfoType *p_ServingCellInfo);

#endif /* LOC_LTE_OTDOA_API_H */
