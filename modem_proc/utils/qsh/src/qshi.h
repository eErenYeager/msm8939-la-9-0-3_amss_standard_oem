/*!
  @file
  qshi.h

  @brief
  QSH internal header file.

 
  @author
  vdalmiya

*/

/*==============================================================================

  Copyright (c) 2014 QUALCOMM Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/utils/qsh/src/qshi.h#3 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------- 
09/04/15   pb      Moved qsh_crit_sect_enter() and qsh_crit_sect_leave() to 
                   qsh.c file
09/04/15   pb      Removed qsh_pad_str()
05/23/14   mm      Added standardized analysis summary
03/12/14   vd      CR 630063: initial version                   
==============================================================================*/

#ifndef QSHI_H
#define QSHI_H

/*==============================================================================

                           INCLUDE FILES

==============================================================================*/

#include "utils_variation.h"
#include <assert.h>
#include "qsh_cfg.h"
#ifdef FEATURE_POSIX
#include <pthread.h>
#endif
#include <rex.h>

/*==============================================================================

                   EXTERNAL DEFINITIONS AND TYPES

==============================================================================*/
#ifdef FEATURE_POSIX
  typedef pthread_mutex_t qsh_crit_sect_s;
#else
  typedef rex_crit_sect_type qsh_crit_sect_s;
#endif

/*!
  @brief
  time type 
*/
typedef uint32 qsh_timetick_t;

#define QSH_ASSERT(exp)  \
    /*lint -save -e506 -e774 */ \
    if(QSH_UNLIKELY(!(exp)))  \
    /*lint -restore */ \
    { \
       ERR_FATAL( "QSH Assertion Failed", 0, 0, 0 ); \
    } \

/*!
  @brief
  Struct to represent the checks for each client. Only maintaining total and
  no error so it's convenient to print as a ratio.
*/
typedef struct
{
  /*! number of checks which evaluated to FALSE (no error) */
  uint32  num_no_error;
  /*! total number of checks */
  uint32  num_total;
} qsh_stats_check_s;

/*==============================================================================

MACRO QSH_ASSERT_LOG

DESCRIPTION
  Given a boolean expression, verify that the input expression is TRUE.
  If the input expression is FALSE, flags an error and resets SW stack.
  This is to be used if the developer has determined that this error is 
  critical in nature if assertion fails, No easy recovery mechanism is possible, 
  Only action to be taken is to reset.

DEPENDENCIES
  None.

RETURN VALUE
  None

SIDE EFFECTS
  This WILL call error fatal

==============================================================================*/
#define QSH_ASSERT_LOG(exp, fmt, arg1, arg2, arg3 ) \
    /*lint -save -e506 -e774 */ \
    if(QSH_UNLIKELY(!(exp))) \
    { \
      /*lint --e{527} */ \
      ERR_FATAL( "Assert " #exp " failed: " fmt, arg1, arg2, arg3 ); \
    } \
    /*lint -restore */ \

#define QSH_MAX(x,y) (((x)>(y)) ? (x) : (y))

#ifdef QSH_CFG_DEBUG_MSG
  #define QSH_DEBUG_MSG(xx_fmt)                                      \
    MSG(MSG_SSID_A2, MSG_LEGACY_HIGH, xx_fmt)

  #define QSH_DEBUG_MSG_1(xx_fmt, a)                                   \
    MSG_1(MSG_SSID_A2, MSG_LEGACY_HIGH, xx_fmt, a)

  #define QSH_DEBUG_MSG_2(xx_fmt, a, b)                                \
    MSG_2(MSG_SSID_A2, MSG_LEGACY_HIGH, xx_fmt, a, b)

  #define QSH_DEBUG_MSG_3(xx_fmt, a, b, c)                             \
    MSG_3(MSG_SSID_A2, MSG_LEGACY_HIGH, xx_fmt, a, b, c)

  #define QSH_DEBUG_MSG_4(xx_fmt, a, b, c, d)                          \
    MSG_4(MSG_SSID_A2, MSG_LEGACY_HIGH, xx_fmt, a, b, c, d)

  #define QSH_DEBUG_MSG_5(xx_fmt, a, b, c, d, e)                       \
    MSG_5(MSG_SSID_A2, MSG_LEGACY_HIGH, xx_fmt, a, b, c, d, e)

  #define QSH_DEBUG_MSG_6(xx_fmt, a, b, c, d, e, f)                    \
    MSG_6(MSG_SSID_A2, MSG_LEGACY_HIGH, xx_fmt, a, b, c, d, e, f)
#else
  #define QSH_DEBUG_MSG(xx_fmt)

  #define QSH_DEBUG_MSG_1(xx_fmt, a)

  #define QSH_DEBUG_MSG_2(xx_fmt, a, b)

  #define QSH_DEBUG_MSG_3(xx_fmt, a, b, c)

  #define QSH_DEBUG_MSG_4(xx_fmt, a, b, c, d)

  #define QSH_DEBUG_MSG_5(xx_fmt, a, b, c, d, e)

  #define QSH_DEBUG_MSG_6(xx_fmt, a, b, c, d, e, f)

#endif

#define QSH_MSG(xx_fmt)                                      \
  MSG(MSG_SSID_A2, MSG_LEGACY_HIGH, xx_fmt)

#define QSH_MSG_1(xx_fmt, a)                                   \
  MSG_1(MSG_SSID_A2, MSG_LEGACY_HIGH, xx_fmt, a)

#define QSH_MSG_2(xx_fmt, a, b)                                \
  MSG_2(MSG_SSID_A2, MSG_LEGACY_HIGH, xx_fmt, a, b)

#define QSH_MSG_3(xx_fmt, a, b, c)                             \
  MSG_3(MSG_SSID_A2, MSG_LEGACY_HIGH, xx_fmt, a, b, c)

#define QSH_MSG_4(xx_fmt, a, b, c, d)                          \
  MSG_4(MSG_SSID_A2, MSG_LEGACY_HIGH, xx_fmt, a, b, c, d)

#define QSH_MSG_5(xx_fmt, a, b, c, d, e)                       \
  MSG_5(MSG_SSID_A2, MSG_LEGACY_HIGH, xx_fmt, a, b, c, d, e)

#define QSH_ERR(xx_fmt)                                      \
  MSG(MSG_SSID_A2, MSG_LEGACY_ERROR, xx_fmt)

#define QSH_ERR_1(xx_fmt, a)                                   \
  MSG_1(MSG_SSID_A2, MSG_LEGACY_ERROR, xx_fmt, a)

#define QSH_ERR_2(xx_fmt, a, b)                                \
  MSG_2(MSG_SSID_A2, MSG_LEGACY_ERROR, xx_fmt, a, b)

#define QSH_ERR_3(xx_fmt, a, b, c)                             \
  MSG_3(MSG_SSID_A2, MSG_LEGACY_ERROR, xx_fmt, a, b, c)

#define QSH_ERR_4(xx_fmt, a, b, c, d)                          \
  MSG_4(MSG_SSID_A2, MSG_LEGACY_ERROR, xx_fmt, a, b, c, d)

#define QSH_ERR_5(xx_fmt, a, b, c, d, e)                       \
  MSG_5(MSG_SSID_A2, MSG_LEGACY_ERROR, xx_fmt, a, b, c, d, e)

/*! Macro for subtracting two timestamps with a given modulus */
#define SUB_MOD( a, b, m ) \
      ( ( (a) >= (b) ) ? ( (a) - (b) ) : ( (m) + (a) - (b) ) )

/*==============================================================================

                    EXTERNAL FUNCTION PROTOTYPES

==============================================================================*/
INLINE void qsh_crit_sect_init( qsh_crit_sect_s * lock_ptr )
{
#ifdef FEATURE_POSIX
   pthread_mutexattr_t attr;
   QSH_ASSERT( lock_ptr != NULL );

   if( pthread_mutexattr_init(&attr) !=0 )
   {
     ERR_FATAL("failed on pthread_mutexattr_init",0,0,0);
   }
   if( pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE) !=0 )
   {
     ERR_FATAL("failed on pthread_mutexattr_settype",0,0,0);
   }
   if( pthread_mutex_init(lock_ptr, &attr) != 0 )
   {
     ERR_FATAL("failed on thread_mutex_init",0,0,0);
   }
   if(pthread_mutexattr_destroy(&attr)!=0)
   {
     MSG_HIGH(
      "WARNING: Potential memory leak. Could not destroy pthread_mutex_attr",
      0,0,0);
   }

#else
  rex_init_crit_sect( (rex_crit_sect_type*) lock_ptr );
#endif
}

INLINE void qsh_crit_sect_deinit( qsh_crit_sect_s * lock_ptr )
{
#ifdef FEATURE_POSIX

  QSH_ASSERT( lock_ptr != NULL );

  if( pthread_mutex_destroy(lock_ptr) !=0 )
  {
    ERR_FATAL("Couldn't destroy pthread mutex",0,0,0);
  }

#endif /* FEATURE_POSIX */
}

/*==============================================================================

  FUNCTION:  qsh_timetick_diff

==============================================================================*/
/*!
  @brief
  This is called to return the time difference, this is for 32 bit timetick

*/
/*============================================================================*/
qsh_timetick_t qsh_timetick_diff
(
  qsh_timetick_t first_tick,
  qsh_timetick_t second_tick
);

/*==============================================================================

  FUNCTION:  qsh_timetick_get

==============================================================================*/
/*!
  @brief
  This is called to return the time_tick
  
*/
/*============================================================================*/
qsh_timetick_t qsh_timetick_get
(
  void
);

/*==============================================================================

  FUNCTION:  qsh_print_summary

==============================================================================*/
/*!
  @brief
  Prints a summary of the analysis.
*/
/*============================================================================*/
void qsh_print_summary
(
  void
);

/*==============================================================================

  FUNCTION:  qsh_ulog_write_checks

==============================================================================*/
/*!
  @brief
  Writes the ratio as a string padded to <= 7 chars.
*/
/*============================================================================*/
void qsh_ulog_write_checks
(
  qsh_stats_check_s *   checks_ptr,
  uint32                pad_len
);

#endif /* QSHI_H */
