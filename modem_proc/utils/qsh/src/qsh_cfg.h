/*!
  @file
  qsh_cfg.h

  @brief
  The interface to the Qualcomm Sherlock Holmes configuration implementation file.

  @author
  vdalmiya

*/

/*==============================================================================

  Copyright (c) 2014 QUALCOMM Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/utils/qsh/src/qsh_cfg.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------- 
03/12/14   vd      CR 630063: initial version                  
==============================================================================*/

#ifndef QSH_CFG_H
#define QSH_CFG_H

/*==============================================================================

                           INCLUDE FILES

==============================================================================*/

#include "utils_variation.h"
#include <comdef.h>
#include <customer.h>
#include "qsh.h"

/*==============================================================================

                   EXTERNAL DEFINITIONS AND TYPES

==============================================================================*/
/*! @brief default client enable mask
*/
#define QSH_CFG_ENABLE_ALL_CLT_MASK 0xFFFFFFFF

/*! @brief default client enable mask
*/
#define QSH_CFG_ENABLE_ALL_MSG_TYPE_MASK 0xFFFFFFFF

/*! @brief QSH configuration data structure
*/
typedef struct
{
  uint32  log_size_in_bytes;  /*!< total log size in bytes */
  uint32  client_mask; /*!< mask that enables/disables client */
  uint32  category_mask; /*!< mask that enables/disables category */
  uint32  msg_type_mask; /*!< mask that enables/disables message type */
  uint32  log_size_per_client_in_words; /*!< log size per client in words */
  uint32  debug_mask; /*!< debug mask */
} qsh_cfg_s;

/*==============================================================================

                    EXTERNAL FUNCTION PROTOTYPES

==============================================================================*/
/*==============================================================================

  FUNCTION:  qsh_cfg_init

==============================================================================*/
/*!
    @brief
    initialize qsh_cfg structure

    @return
    pointer to qsh_cfg data structure
*/
/*============================================================================*/
qsh_cfg_s* qsh_cfg_init(void);

/*==============================================================================

  FUNCTION:  qsh_cfg_client_is_enabled

==============================================================================*/
/*!
    @brief
    Return whether or not the client is enabled for analysis.

    @return
    TRUE if client is enabled; FALSE otherwise
*/
/*============================================================================*/
boolean qsh_cfg_client_is_enabled
(
  qsh_clt_e          client  /*!< the client in question */
);

/*==============================================================================

  FUNCTION:  qsh_cfg_get_client_mask

==============================================================================*/
/*!
    @brief
    Returns client mask
 
    @return
    client mask
*/
/*============================================================================*/
uint32 qsh_cfg_get_client_mask
(
  void
);

/*==============================================================================

  FUNCTION:  qsh_cfg_set_client_mask

==============================================================================*/
/*!
    @brief
    Sets client mask 
*/
/*============================================================================*/
void qsh_cfg_set_client_mask
(
  uint32 client_mask
);

/*==============================================================================

  FUNCTION:  qsh_cfg_msg_type_is_enabled

==============================================================================*/
/*!
    @brief
    Return whether or not the message type is enabled for logging.
 
    @return
    true/false
*/
/*============================================================================*/
boolean qsh_cfg_msg_type_is_enabled
(
  qsh_msg_type_e msg_type
);

/*==============================================================================

  FUNCTION:  qsh_cfg_get_msg_type_mask

==============================================================================*/
/*!
    @brief
    Returns message type mask
 
    @return
    message type mask
*/
/*============================================================================*/
uint32 qsh_cfg_get_msg_type_mask
(
  void
);

/*==============================================================================

  FUNCTION:  qsh_cfg_set_msg_type_mask

==============================================================================*/
/*!
    @brief
    Sets message type mask 
*/
/*============================================================================*/
void qsh_cfg_set_msg_type_mask
(
  uint32 msg_type_mask
);

/*==============================================================================

  FUNCTION:  qsh_cfg_category_is_enabled

==============================================================================*/
/*!
    @brief
    Return whether or not the category is enabled for logging.
 
    @return
    true/false
*/
/*============================================================================*/
boolean qsh_cfg_category_is_enabled
(
  qsh_cat_e category
);

/*==============================================================================

  FUNCTION:  qsh_cfg_get_category_mask

==============================================================================*/
/*!
    @brief
    Returns category mask
 
    @return
    category mask
*/
/*============================================================================*/
uint32 qsh_cfg_get_category_mask
(
  void
);

/*==============================================================================

  FUNCTION:  qsh_cfg_set_category_mask

==============================================================================*/
/*!
    @brief
    Sets category mask 
*/
/*============================================================================*/
void qsh_cfg_set_category_mask
(
  uint32 category_mask
);

/*==============================================================================

  FUNCTION:  qsh_cfg_get_log_size_in_bytes

==============================================================================*/
/*!
    @brief
    Returns total log size in bytes
 
    @return
    total log size in bytes
*/
/*============================================================================*/
uint32 qsh_cfg_get_log_size_in_bytes
(
  void
);

/*==============================================================================

  FUNCTION:  qsh_cfg_get_log_size_per_client_in_words

==============================================================================*/
/*!
    @brief
    Returns log size per client in words
 
    @return
    log size per client in words
*/
/*============================================================================*/
uint32 qsh_cfg_get_log_size_per_client_in_words
(
  void
);

/*==============================================================================

  FUNCTION:  qsh_cfg_set_log_size_per_client_in_words

==============================================================================*/
/*!
    @brief
    Sets log size per client in words
 
*/
/*============================================================================*/
void qsh_cfg_set_log_size_per_client_in_words
(
  uint32 log_size_per_client_in_words
);

/*==============================================================================

  FUNCTION:  qsh_cfg_assert_for_invalid_params

==============================================================================*/
/*!
    @brief
    Returns whether to assert for invalid params
 
    @return
    true/false
*/
/*============================================================================*/
boolean qsh_cfg_assert_for_invalid_params
(
  void
);
#endif /* QSH_CFG_H */
