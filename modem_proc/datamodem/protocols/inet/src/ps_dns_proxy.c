/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                P S _ D N S _ P R O X Y . C

GENERAL DESCRIPTION
    This file implements the DNS Proxy. DNS Proxy will receive the DNS
    requests from internal NATed clients and will forward the DNS queries
    to the actual DNS server on the UM interface.


Copyright (c) 2013 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/
/*===========================================================================

                        EDIT HISTORY FOR MODULE

$Id: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/protocols/inet/src/ps_dns_proxy.c#1 $

when        who    what, where, why
--------    ---    ----------------------------------------------------------
09/12/13    sd     Cryptographic vulnerability - replacing ran_next APIs.
07/08/13    cx     Replace memcpy and memmove with memscpy and memsmove
12/12/12    ds     Replacing LOG_MSG_FATAL_ERROR with LOG_MSG_ERROR.
05/20/10    rt     Added module
===========================================================================*/


/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "comdef.h"
#include "customer.h"
#include "dsm.h"
#include "msg.h"
#include "err.h"
#include "dssocket.h"
#include "ps_mem.h"
#include "dssinternal.h"
#include "ps_dns_proxy.h"
#include "ps_iface.h"
#include "ps_iface_ipfltr.h"
#include "ps_in.h"
#include "dserrno.h"
#include "ds_Utils_DebugMsg.h"

#include "ps_utils.h"
#include "ps_svc.h"
#include "ran.h"

#include "ds_Utils_DebugMsg.h"

/*===========================================================================

                DEFINITIONS AND DECLARATIONS FOR MODULE

===========================================================================*/

#define  PS_DNS_PROXY_SERVER_PORT (dss_htons(53))

#define  PS_DNS_PROXY_MIN_MSG_ID  1
#define  PS_DNS_PROXY_MAX_MSG_ID  0x7FFF

static ps_dns_proxy_info_type    dns_proxy_info;

/*---------------------------------------------------------------------------
  Array of Pending DNS requests waiting for response.
---------------------------------------------------------------------------*/
static ps_dns_proxy_query_entry_type 
  dns_proxy_query_array[NUM_DNS_PROXY_ENTRIES];

/*===========================================================================
MACRO PS_DNS_PROXY_PUSHDOWN_BYTES()

DESCRIPTION
 copy 'size' bytes of data from user-provided buffer to dsm item.
PARAMETERS
  dsm_item_ptr: Pointer to dsm item
  buffer: Pointer to source buffer
  size: Number of bytes to copy.
RETURN VALUE
  None
DEPENDENCIES
  None
SIDE EFFECTS
  Copies size number bytes from buffer into dsm item.
===========================================================================*/
#define PS_DNS_PROXY_PUSHDOWN_BYTES( dsm_item_ptr, buffer, size )       \
{                                                                       \
  if (dsm_pushdown_packed( dsm_item_ptr,                                \
                           (byte *)buffer,                              \
                           size,                                        \
                           DSM_DS_LARGE_ITEM_POOL) != size)             \
  {                                                                     \
    LOG_MSG_ERROR("Error in dsm_pushdown ",0,0,0);                                \
    dsm_free_packet(dsm_item_ptr);                                      \
    return DSS_ERROR;                                                   \
  }                                                                     \
}

/*===========================================================================
MACRO PS_DNS_PROXY_EXTRACT_BYTES()

DESCRIPTION
 copy 'size' bytes of data from dsm item to user-provided buffer, starting at
 'offset' bytes from start of dsm item and increment the offset by 'size'.
PARAMETERS
  dsm_item_ptr: Pointer to dsm item
  buffer: Pointer to destination buffer
  size: Number of bytes to copy.
  offset: Offset this number of bytes from packet head
RETURN VALUE
  None
DEPENDENCIES
  None
SIDE EFFECTS
  Copies size number bytes from dsm item into buffer and increments
  'offset' by 'size'
===========================================================================*/
#define PS_DNS_PROXY_EXTRACT_BYTES(dsm_item_ptr, buffer, size, offset)     \
{                                                                          \
  if (dsm_extract( dsm_item_ptr,                                           \
                   offset,                                                 \
                   (byte *) (buffer),                                      \
                   size) != (size))                                        \
  {                                                                        \
    LOG_MSG_ERROR("Error in extracting field ", 0, 0, 0);                            \
    dsm_free_packet(&(dsm_item_ptr));                                      \
    return DSS_ERROR;                                                      \
  }                                                                        \
  offset += size;                                                          \
}

/*===========================================================================
MACRO PS_DNS_PROXY_INSERT_BYTES()

DESCRIPTION
 insert 'size' bytes of data into dsm item from user-provided buffer, 
  starting at 'offset' bytes from start of dsm item.
PARAMETERS
  dsm_item_ptr: Pointer to dsm item
  buffer: Pointer to destination buffer
  size: Number of bytes to insert.
  offset: Offset this number of bytes from packet head
RETURN VALUE
  None
DEPENDENCIES
  None
SIDE EFFECTS
===========================================================================*/
#define PS_DNS_PROXY_INSERT_BYTES(dsm_item_ptr, offset, buffer, size)     \
{                                                                         \
  if (dsm_insert( dsm_item_ptr,                                           \
                  DSM_DS_SMALL_ITEM_POOL,                                 \
                  offset,                                                 \
                  (byte *) (buffer),                                      \
                  size) != (size))                                        \
  {                                                                       \
    LOG_MSG_ERROR_0("Error in inserting field ");                         \
  }                                                                       \
}

/*===========================================================================

                           FORWARD DECLARATIONS

===========================================================================*/
#if 0
static void 
ps_dns_proxy_um_iface_ev_cb
(
  ps_iface_type              *this_iface_ptr,
  ps_iface_event_enum_type    event,
  ps_iface_event_info_u_type  event_info,
  void                 *user_data_ptr
);
#endif

static void 
ps_dns_proxy_iface_ev_cmd_hdlr
(
  ps_cmd_enum_type   ps_cmd,
  void              *data_ptr
);
  
static void
ps_dns_proxy_sock_cb
(
   int16    dss_nethandle,
   int16    sockfd,
   uint32   event_mask,
   void    *user_data_ptr
);

static void 
ps_dns_proxy_cmd_handler
(
  ps_cmd_enum_type             ps_cmd,
  void                        *data_ptr
);

static void 
ps_dns_proxy_rm_process_sock_event
(
  const ps_dns_proxy_sock_event_cmd_info_type   *dns_proxy_sock_ev_info
);

static int32 
ps_dns_proxy_forward_dns_query
(
  dsm_item_type          **dss_packet,
  const struct ps_sockaddr_in   *dss_addr
);

static int32 
ps_dns_proxy_sendmsg
(
  sint15                sock_fd,
  dsm_watermark_type   *write_wm
);

static void 
ps_dns_proxy_um_process_sock_event
(
  const ps_dns_proxy_sock_event_cmd_info_type   *dns_proxy_sock_ev_info
);

static int32 
ps_dns_proxy_forward_dns_response
(
  dsm_item_type       **dss_packet,
  const struct ps_sockaddr_in   *dss_addr
);

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

                           PRIVATE FUNCTIONS

=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/

#if 0
/*===========================================================================
FUNCTION PS_DNS_PROXY_UM_IFACE_EV_CB()

DESCRIPTION
  This function is registered as a callback function UM Configuring Event.

DEPENDENCIES
  None

PARAMETERS
  this_iface_ptr: Pointer to the UM interface.
  event         : The UM iface Configuring event.
  event_info    : The information associated with the event.
  user_data_ptr : Any user data passed during registering the callback.

 RETURN VALUE
  None

 SIDE EFFECTS
  None
===========================================================================*/
static void 
ps_dns_proxy_um_iface_ev_cb
(
  ps_iface_type               *this_iface_ptr,
  ps_iface_event_enum_type     event,
  ps_iface_event_info_u_type   event_info,
  void                        *user_data_ptr
)
{
  ps_dns_proxy_um_iface_ev_cmd_info_type  *iface_event_info;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  (void)event_info;
  (void)user_data_ptr;

  iface_event_info = (ps_dns_proxy_um_iface_ev_cmd_info_type *)
    ps_mem_get_buf(PS_MEM_DNS_PROXY_IFACE_EVENT_INFO_TYPE);

  if(iface_event_info == NULL)
  {
    LOG_MSG_ERROR_0("Out of iface event info memory buffers");
    return;
  }

  iface_event_info->event = event;
  iface_event_info->nat_iface_ptr = this_iface_ptr;

  ps_send_cmd(PS_DNS_PROXY_IFACE_EVENT_CMD, iface_event_info);
}
#endif

/*===========================================================================
FUNCTION PS_DNS_PROXY_IFACE_EV_CMD_HANDLER()

DESCRIPTION
  This function is called when the PS notifies the DNS PROXY
  of IFACE CONFIGURING event on UM iface

PARAMETERS
  ps_cmd     - Should be PS_DNS_PROXY_IFACE_EVENT_CMD
  data_ptr   - Points to the iface pointer and event.

RETURN VALUE
  None

DEPENDENCIES
  None

SIDE EFFECTS
  None
===========================================================================*/
static void 
ps_dns_proxy_iface_ev_cmd_hdlr
(
  ps_cmd_enum_type   ps_cmd,
  void              *data_ptr
)
{
  ps_dns_proxy_um_iface_ev_cmd_info_type  *iface_event_info;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_INFO2_0("Entering ps_dns_proxy_iface_ev_cmd_hdlr");

  if(data_ptr == NULL || ps_cmd != PS_DNS_PROXY_IFACE_EVENT_CMD)
  {
    LOG_MSG_ERROR_0("DNS Proxy EV command handler called with invalid params");
    return;
  }

  iface_event_info = (ps_dns_proxy_um_iface_ev_cmd_info_type *) data_ptr;

  if (!PS_IFACE_IS_VALID(iface_event_info->nat_iface_ptr))
  {
    LOG_MSG_ERROR_0("Invalid iface");
    PS_MEM_FREE(data_ptr);
    return;
  }

  /*-------------------------------------------------------------------------
   NAT iface never goes to configuring state as of now.
   This allows for WiFi Clients <-> Modem Nat iface data transfer going.
  -------------------------------------------------------------------------*/
  //TODO: Check if we want to register for some other event on NAT iface

  PS_MEM_FREE(data_ptr);
  
  LOG_MSG_INFO2_1("Exiting ps_dns_proxy_iface_ev_cmd_hdlr for event %d",
                  iface_event_info->event);
  
  return;

} /* ps_dns_proxy_iface_ev_cmd_hdlr() */

/*===========================================================================
STATIC FUNCTION DNS_PROXY_SOCK_CB

Description:
  This function gets called by the sockets layer when there is an
  event on the socket.  We handle each of the possible events that can
  come in.  Note: we can get more than one event in the callback.

Arguements:
  int16 dss_nethandle - The app id (if any) associated with the socket
  int16 sockfd - The socket descriptor
  uint32 event_mask - The event(s) that occured on the socket
  void * handle - A handle to our info.

Return value:
  void

Dependencies:
  None.
===========================================================================*/
static void
ps_dns_proxy_sock_cb
(
   int16    dss_nethandle,
   int16    sockfd,
   uint32   event_mask,
   void    *user_data_ptr
)
{
  ps_dns_proxy_sock_event_cmd_info_type   *dns_proxy_sock_ev_info;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  (void)dss_nethandle;

  LOG_MSG_INFO2_2("Socket event %d on socket %d occurred for DNS Proxy",
                  event_mask, sockfd);

  dns_proxy_sock_ev_info = (ps_dns_proxy_sock_event_cmd_info_type *)
                        ps_mem_get_buf(PS_MEM_DNS_PROXY_SOCK_EVENT_INFO_TYPE);

  if (dns_proxy_sock_ev_info == NULL)
  {
    LOG_MSG_ERROR_0("Out of DNS Proxy sock event info memory buffers");
    return;
  }

  dns_proxy_sock_ev_info->sockfd = sockfd;
  dns_proxy_sock_ev_info->sock_event_mask = event_mask;
  dns_proxy_sock_ev_info->user_data_ptr = user_data_ptr;

  ps_send_cmd(PS_DNS_PROXY_SOCK_EVENT_CMD, (void*)dns_proxy_sock_ev_info);
}


/*===========================================================================
FUNCTION PS_DNS_PROXY_CMD_HANDLER()

DESCRIPTION
  This function is called when the sockets layer notifies the DNS PROXY
  of events of interest.

PARAMETERS
  ps_cmd        - Should be PS_DNS_PROXY_SOCK_EVENT_CMD
  io_data_ptr   - Points to the dns proxy sock event info.

RETURN VALUE
  None

DEPENDENCIES
  None

SIDE EFFECTS
  None
===========================================================================*/
static void 
ps_dns_proxy_cmd_handler
(
  ps_cmd_enum_type   ps_cmd,
  void              *data_ptr
)
{
  ps_dns_proxy_sock_event_cmd_info_type     *dns_proxy_sock_ev_info;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if(data_ptr == NULL || ps_cmd != PS_DNS_PROXY_SOCK_EVENT_CMD)
  {
    LOG_MSG_ERROR_0("DNS Proxy command handler called with invalid params");
    return;
  }

  dns_proxy_sock_ev_info = (ps_dns_proxy_sock_event_cmd_info_type*)data_ptr;

  LOG_MSG_INFO2_2("Processing DNS Proxy SOCK_EVENT socket %d Ev_Mask %d",
           dns_proxy_sock_ev_info->sockfd, 
                  dns_proxy_sock_ev_info->sock_event_mask);

  if (dns_proxy_sock_ev_info->sockfd == dns_proxy_info.rm_sockfd)
  {
    ps_dns_proxy_rm_process_sock_event(dns_proxy_sock_ev_info);
  }
  else if (dns_proxy_sock_ev_info->sockfd == dns_proxy_info.um_sockfd)
  {
    ps_dns_proxy_um_process_sock_event(dns_proxy_sock_ev_info);
  }
  else
  {
    LOG_MSG_ERROR_1("Sock callback called with unknown sockfd %d",
                    dns_proxy_sock_ev_info->sockfd);
  }

  PS_MEM_FREE(data_ptr);
 
  return;

} /* ps_dns_proxy_cmd_handler() */

/*===========================================================================
FUNCTION  PS_DNS_PROXY_RM_PROCESS_SOCK_EVENT()

DESCRIPTION

PARAMETERS

RETURN VALUE
  None

DEPENDENCIES
  None

SIDE EFFECTS
  None
===========================================================================*/
static void 
ps_dns_proxy_rm_process_sock_event
(
  const ps_dns_proxy_sock_event_cmd_info_type   *dns_proxy_sock_ev_info
)
{
  int32              retval;
  dsm_item_type     *dss_packet = NULL;
  int16              dss_errno;
  uint32             event_mask;
  struct ps_sockaddr_in   dss_addr;
  uint16                  dss_addr_len = sizeof( struct ps_sockaddr_in );
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (NULL == dns_proxy_sock_ev_info)
  {
    LOG_MSG_ERROR_0("NULL dns proxy info");
    return;
  }

  event_mask = dns_proxy_sock_ev_info->sock_event_mask;

  LOG_MSG_INFO2_1("Entering ps_dns_proxy_rm_process_sock_event mask %d on RM socket",
                  event_mask);

  if (DS_READ_EVENT & event_mask)
  {
    retval = dss_recvfrom_dsm_chain( dns_proxy_sock_ev_info->sockfd,
                                     &dss_packet,
                                     0,
                                     (struct ps_sockaddr*)&dss_addr,
                                     &dss_addr_len,
                                     &dss_errno ) ;
    if (retval == DSS_ERROR)
    {
      if (dss_errno != DS_EWOULDBLOCK)
      {
        LOG_MSG_ERROR_1("Read from DSM chain returned error %d", dss_errno);
      }
    }
    else
    {
      /* Forward the incoming DNS request to UM DNS server */
      retval = ps_dns_proxy_forward_dns_query(&dss_packet, &dss_addr);
      if (retval == DSS_ERROR)
      {
        LOG_MSG_ERROR_0("Error in forwarding the DNS Query");
      }
    }
    (void)dss_async_select( dns_proxy_sock_ev_info->sockfd, 
                            DS_READ_EVENT, &dss_errno );
  }

  if (DS_WRITE_EVENT & event_mask)
  {
    (void)ps_dns_proxy_sendmsg(dns_proxy_sock_ev_info->sockfd, 
                               &dns_proxy_info.rm_write_wm);
  }
  
  return;

} /* ps_dns_proxy_rm_process_sock_event() */

/*===========================================================================
FUNCTION  PS_DNS_PROXY_FORWARD_DNS_QUERY()

DESCRIPTION

PARAMETERS

RETURN VALUE
  None

DEPENDENCIES
  None

SIDE EFFECTS
  None
===========================================================================*/
static int32 
ps_dns_proxy_forward_dns_query
(
  dsm_item_type                **dss_packet,
  const struct ps_sockaddr_in   *dss_addr
)
{
  uint16             offset;
  uint16             dns_query_id;
  uint32             temp_time;
  uint8              i;
  uint8              lru_entry_idx;
  uint16             rand_query_id; 
  ip_addr_type       prim_dns_addr;
  ip_addr_type       sec_dns_addr;
  ip_addr_type       um_dns_addr;
  struct ps_sockaddr_in dest_addr;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  prim_dns_addr.type = IPV4_ADDR;
  sec_dns_addr.type  = IPV4_ADDR;
  ps_iface_get_v4_dns_addrs(dns_proxy_info.nat_iface_ptr, 
                            &prim_dns_addr, 
                            &sec_dns_addr);
  
  if (prim_dns_addr.type == IP_ADDR_INVALID)
  {
    LOG_MSG_INFO1_0("Primary DNS server not available");
    if(sec_dns_addr.type == IP_ADDR_INVALID)
    {
      LOG_MSG_ERROR_0("No DNS server available. DNS Proxy dropping the req pkt");
      dsm_free_packet(dss_packet);
      return DSS_ERROR;
    }
    else
    {
      memscpy(&um_dns_addr,
              sizeof(um_dns_addr), 
              &sec_dns_addr, 
              sizeof(ip_addr_type));
    }
  }
  else
  {
    memscpy(&um_dns_addr,
            sizeof(um_dns_addr), 
            &prim_dns_addr, 
            sizeof(ip_addr_type));
  }

  LOG_MSG_INFO2_0("Sending the Query to DNS server address");
  IPV4_ADDR_MSG(um_dns_addr.addr.v4);

  /*-------------------------------------------------------------------------
    Extract the Query ID. Generate a random Query ID that will be used to
    send out the DNS request on UM.
    Store the Src IP, Src Port, Original Query ID, Mapped Query ID.
  -------------------------------------------------------------------------*/

  offset = 0;
  PS_DNS_PROXY_EXTRACT_BYTES (*dss_packet, &dns_query_id, 
                              sizeof(dns_query_id), offset);

  rand_query_id = (uint16) ps_utils_generate_16bit_rand_num_by_range(
                                     PS_DNS_PROXY_MIN_MSG_ID,
                                     PS_DNS_PROXY_MAX_MSG_ID);
  
  /*-------------------------------------------------------------------------
    Find an empty entry in the DNS Proxy pending request array.
  -------------------------------------------------------------------------*/
  temp_time = 0;
  lru_entry_idx = 0;
  for (i = 0; i < NUM_DNS_PROXY_ENTRIES; i++)
  {
    if (dns_proxy_query_array[i].time_stamp == 0)
    {
      lru_entry_idx = i;
      break;
    }
    else
    {
      if (temp_time < dns_proxy_query_array[i].time_stamp)
      {
        lru_entry_idx = i;
        temp_time = dns_proxy_query_array[i].time_stamp;
      }
    }
  }

  dns_proxy_query_array[lru_entry_idx].time_stamp = msclock();
  dns_proxy_query_array[lru_entry_idx].src_addr = dss_addr->ps_sin_addr.ps_s_addr;
  dns_proxy_query_array[lru_entry_idx].src_port = dss_addr->ps_sin_port;
  dns_proxy_query_array[lru_entry_idx].orig_query_id = dns_query_id;
  dns_proxy_query_array[lru_entry_idx].mapped_query_id = rand_query_id;

  PS_DNS_PROXY_INSERT_BYTES(dss_packet,
                            0,
                            &rand_query_id,
                            sizeof(uint16));
  
  /*-------------------------------------------------------------------------
    Send the DNS request on UM socket.
  -------------------------------------------------------------------------*/
  dest_addr.ps_sin_family = DSS_AF_INET;
  dest_addr.ps_sin_port = PS_DNS_PROXY_SERVER_PORT;
  dest_addr.ps_sin_addr.ps_s_addr = um_dns_addr.addr.v4;
  
 /*-------------------------------------------------------------------------
    Add the TO Destination Address to the message
  -------------------------------------------------------------------------*/
  PS_DNS_PROXY_PUSHDOWN_BYTES(dss_packet,
                              &dest_addr,
                              sizeof(struct ps_sockaddr_in));

  dsm_enqueue(&dns_proxy_info.um_write_wm, dss_packet);

  LOG_MSG_INFO2_0("Calling sendmsg from ps_dns_proxy_forward_dns_query");

  return ps_dns_proxy_sendmsg(dns_proxy_info.um_sockfd, 
                              &dns_proxy_info.um_write_wm);
} /* ps_dns_proxy_forward_dns_query() */

/*===========================================================================

FUNCTION PS_DNS_PROXY_SENDMSG

DESCRIPTION
  Dequeue and send message.

DEPENDENCIES
  None.

RETURN VALUE

SIDE EFFECTS

===========================================================================*/
static int32 
ps_dns_proxy_sendmsg
(
  sint15                sock_fd,
  dsm_watermark_type   *write_wm
)
{
  dsm_item_type           *item_ptr = NULL;
  struct ps_sockaddr_in    sockaddr;
  int16                    ret;
  sint15                   dss_errno;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_INFO2_1("Entering ps_dns_proxy_sendmsg for sockfd", sock_fd);
  while ((item_ptr = dsm_dequeue(write_wm)) != NULL)
  {
    /*-----------------------------------------------------------------------
      pull up the destination address
    -----------------------------------------------------------------------*/
    if(dsm_pullup(&item_ptr,
                  &(sockaddr),
                  sizeof(sockaddr)) != sizeof(sockaddr))
    {
      LOG_MSG_ERROR_0("Failed to send MSG");
      dsm_free_packet(&item_ptr);
      return DSS_ERROR;
    }

    ret = dss_sendto_dsm_chain(sock_fd,
                               &item_ptr,
                               0,
                               (struct ps_sockaddr*)&sockaddr,
                               sizeof(struct ps_sockaddr_in),
                               &dss_errno);

    if (ret == DSS_ERROR)
    {
      if( dss_errno == DS_EWOULDBLOCK)
      {
        /*-----------------------------------------------------------------
          Enqueue DSM on to WM first before calling dss_async_select() so
          that if WRITE_EVENT is posted immediately, DNS Proxy can dequeue
          item from WM
        -----------------------------------------------------------------*/
        if (dsm_pushdown_packed(&item_ptr,
                                (byte *) &sockaddr,
                                sizeof(sockaddr),
                                DSM_DS_SMALL_ITEM_POOL) != sizeof(sockaddr))
        {
          LOG_MSG_ERROR_0("Error in dsm_pushdown");
          dsm_free_packet(&item_ptr);
          return DSS_ERROR;
        }

        dsm_enqueue(write_wm, &item_ptr);

        (void) dss_async_select(sock_fd, DS_WRITE_EVENT, &dss_errno);
        break;
      }
      else
      {
        LOG_MSG_ERROR_1("Unrecognized DSS error (%d)", dss_errno);
        dsm_free_packet(&item_ptr);
        return DSS_ERROR;
      }
    }
  }

  return DSS_SUCCESS;
} /* ps_dns_proxy_sendmsg() */

/*===========================================================================
FUNCTION  PS_DNS_PROXY_UM_PROCESS_SOCK_EVENT()

DESCRIPTION

PARAMETERS

RETURN VALUE
  None

DEPENDENCIES
  None

SIDE EFFECTS
  None
===========================================================================*/
static void 
ps_dns_proxy_um_process_sock_event
(
  const ps_dns_proxy_sock_event_cmd_info_type   *dns_proxy_sock_ev_info
)
{
  int32              retval;
  dsm_item_type     *dss_packet = NULL;
  uint16             dss_addr_len = sizeof( struct ps_sockaddr_in );
  int16              dss_errno;
  uint32             event_mask;
  struct ps_sockaddr_in dss_addr;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (NULL == dns_proxy_sock_ev_info)
  {
    LOG_MSG_ERROR_0("NULL dns proxy info");
    return;
  }

  event_mask = dns_proxy_sock_ev_info->sock_event_mask;

  LOG_MSG_INFO2_1("Event mask received %d", event_mask);

  if (DS_READ_EVENT & event_mask)
  {
    retval = dss_recvfrom_dsm_chain( dns_proxy_sock_ev_info->sockfd,
                                     &dss_packet,
                                     0,
                                     (struct ps_sockaddr*)&dss_addr,
                                     &dss_addr_len,
                                     &dss_errno ) ;
    if (retval == DSS_ERROR)
    {
      if (dss_errno != DS_EWOULDBLOCK)
      {
        LOG_MSG_ERROR_1("Read from DSM chain returned error %d", dss_errno);
      }
    }
    else
    {
      /* Send the read DNS response to RM DNS server*/
      retval = ps_dns_proxy_forward_dns_response(&dss_packet, &dss_addr);
      if (retval == DSS_ERROR)
      {
        LOG_MSG_ERROR_0("Error in forwarding the DNS response");
      }
    }
    (void)dss_async_select( dns_proxy_sock_ev_info->sockfd, 
                            DS_READ_EVENT, &dss_errno );
  }

  if (DS_WRITE_EVENT & event_mask)
  {
    (void)ps_dns_proxy_sendmsg(dns_proxy_sock_ev_info->sockfd, 
                         &dns_proxy_info.um_write_wm);
  }

  return;

} /* ps_dns_proxy_um_process_sock_event() */

/*===========================================================================
FUNCTION  PS_DNS_PROXY_FORWARD_DNS_RESPONSE()

DESCRIPTION

PARAMETERS

RETURN VALUE
  None

DEPENDENCIES
  None

SIDE EFFECTS
  None
===========================================================================*/
static int32 
ps_dns_proxy_forward_dns_response
(
  dsm_item_type                **dss_packet,
  const struct ps_sockaddr_in   *dss_addr
)
{
  uint16             offset;
  uint16             dns_query_id;
  uint8              i;
  struct ps_sockaddr_in dest_addr;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  (void)dss_addr;

  /*-------------------------------------------------------------------------
    Extract the Query/Response Identifier.
  -------------------------------------------------------------------------*/

  offset = 0;
  PS_DNS_PROXY_EXTRACT_BYTES (*dss_packet, &dns_query_id, 
                              sizeof(dns_query_id), offset );
  
  /*-------------------------------------------------------------------------
    Find the entry for this dns query id in the DNS Proxy pending request 
    array.
  -------------------------------------------------------------------------*/
  for (i = 0; i < NUM_DNS_PROXY_ENTRIES; i++)
  {
    if (dns_proxy_query_array[i].mapped_query_id == dns_query_id)
    {
      break;
    }
  }

  if (i >= NUM_DNS_PROXY_ENTRIES)
  {
    LOG_MSG_ERROR_0("No matching DNS Query id found. Dropping the Response");
    dsm_free_packet( dss_packet );
    return DSS_ERROR;
  }

  PS_DNS_PROXY_INSERT_BYTES(dss_packet,
                            0,
                            &(dns_proxy_query_array[i].orig_query_id),
                            sizeof(uint16));
  
  dest_addr.ps_sin_family = DSS_AF_INET;
  dest_addr.ps_sin_port = dns_proxy_query_array[i].src_port;
  dest_addr.ps_sin_addr.ps_s_addr = dns_proxy_query_array[i].src_addr;
  
  memset(&dns_proxy_query_array[i], 0, sizeof(ps_dns_proxy_query_entry_type));

 /*-------------------------------------------------------------------------
    Add the TO Destination ADDRESS TO THE message
  -------------------------------------------------------------------------*/
  PS_DNS_PROXY_PUSHDOWN_BYTES(dss_packet,
                              &dest_addr,
                              sizeof(struct ps_sockaddr_in));

  dsm_enqueue(&dns_proxy_info.rm_write_wm, dss_packet);

  (void)ps_dns_proxy_sendmsg(dns_proxy_info.rm_sockfd, 
                       &dns_proxy_info.rm_write_wm);
  return DSS_SUCCESS;
} /* ps_dns_proxy_forward_dns_response() */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

                           EXTERNALIZED FUNCTIONS

=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
/*===========================================================================
FUNCTION DNS_PROXYI_ENABLE()

DESCRIPTION
  This function enables the DNS Proxy. It takes two iface pointers as input.
  The RM iface pointer is the iface where DNS proxy will receive requests.
  The UM iface pointer is the iface where DNS request will be forwarded to
  the actual DNS server that is configured on the UM iface.

  As of now only one DNS proxy can be run i.e. DNS proxy can enabled on
  only on one interface.

DEPENDENCIES

PARAMETERS
  nat_iface_ptr: Pointer to the iface where DNS proxy will be running
                 and getting request on.
  dss_errno:     Error, if any.

RETURN VALUE
  Success  0
  Fail    -1
  

SIDE EFFECTS
  None
===========================================================================*/
int32
dns_proxyi_enable
(
 ps_iface_type               *nat_iface_ptr,
 sint15                      *dss_errno 
)
{
  sint15      dss_ret;
  sint31      ret_val;
  dss_net_policy_info_type    policy;
  struct ps_sockaddr_in          dss_sockaddr;
/*-------------------------------------------------------------------------*/

  if (!PS_IFACE_IS_VALID(nat_iface_ptr))
  {
    LOG_MSG_ERROR_1("Invalid NAT Iface 0x%p", nat_iface_ptr);
    *dss_errno = DS_EFAULT;
    return DSS_ERROR;
  }
  /*-------------------------------------------------------------------------
    Set the command handler for the socket events.
  -------------------------------------------------------------------------*/
  (void)ps_set_cmd_handler(PS_DNS_PROXY_SOCK_EVENT_CMD,
                           ps_dns_proxy_cmd_handler);

  /*-------------------------------------------------------------------------
    Set the command handler for the iface events.
  -------------------------------------------------------------------------*/
  (void)ps_set_cmd_handler(PS_DNS_PROXY_IFACE_EVENT_CMD,
                           ps_dns_proxy_iface_ev_cmd_hdlr);

  /*-------------------------------------------------------------------------
    Initialize the different DNS Proxy data structures
  -------------------------------------------------------------------------*/
  memset(&dns_proxy_info, 0, sizeof(ps_dns_proxy_info_type));

  /* Initialize the write watermark */
  dsm_queue_init(&dns_proxy_info.rm_write_wm,
                 DNS_PROXY_SEND_WM_SZ,
                 &dns_proxy_info.rm_write_wm_q);
  
  dsm_queue_init(&dns_proxy_info.um_write_wm,
                 DNS_PROXY_SEND_WM_SZ,
                 &dns_proxy_info.um_write_wm_q);

  /* Initialize the Pending Query Array */
  memset(dns_proxy_query_array, 0, 
         sizeof(ps_dns_proxy_query_entry_type) * NUM_DNS_PROXY_ENTRIES);

  /* Store the rm and um iface pointer */
  dns_proxy_info.nat_iface_ptr = nat_iface_ptr;

  /*-------------------------------------------------------------------------
    Set up the socket on DNS server port on the NAT iface. It would get 
    incoming DNS requests.
  -------------------------------------------------------------------------*/

  /* Init the policy */
  memset(&policy, 0, sizeof(dss_net_policy_info_type));
  dss_init_net_policy_info(&policy);

  /* Set up the socket. */
  policy.policy_flag    = DSS_IFACE_POLICY_SPECIFIED;
  policy.iface.kind     = DSS_IFACE_ID;
  policy.iface.info.id  = PS_IFACE_GET_ID(nat_iface_ptr);
  policy.family         = DSS_AF_INET;

  dss_ret = dss_socket2(DSS_AF_INET, DSS_SOCK_DGRAM, (uint8)PS_IPPROTO_UDP,
                        ps_dns_proxy_sock_cb, NULL,  
                        &policy, dss_errno);
  if (dss_ret == DSS_ERROR)
  {
    LOG_MSG_ERROR_1( "Unable to open socket Error %d", *dss_errno );
    return DSS_ERROR;
  }
  dns_proxy_info.rm_sockfd = dss_ret;

  LOG_MSG_INFO2_1("Created RM side socket fd %d", dss_ret);

  dss_sockaddr.ps_sin_family      = DSS_AF_INET;
  dss_sockaddr.ps_sin_port        = PS_DNS_PROXY_SERVER_PORT;
  dss_sockaddr.ps_sin_addr.ps_s_addr = 0;

  dss_ret = dss_bind(dns_proxy_info.rm_sockfd,
                     (struct ps_sockaddr*)&dss_sockaddr,
                     sizeof(struct ps_sockaddr_in),
                     dss_errno);
  if (dss_ret == DSS_ERROR)
  {
    LOG_MSG_ERROR_1("Unable to bind to DNS server port %d", *dss_errno);
    (void)dss_close( dns_proxy_info.rm_sockfd, dss_errno );
    return DSS_ERROR;
  }

  ret_val = dss_async_select(dns_proxy_info.rm_sockfd, 
                             DS_READ_EVENT, 
                             dss_errno);
  if (ret_val == DSS_ERROR)
  {
    LOG_MSG_ERROR_2( "Failed async select READ on socket: %d error: %d",
                     dns_proxy_info.rm_sockfd, *dss_errno );
    (void)dss_close( dns_proxy_info.rm_sockfd, dss_errno );
    return DSS_ERROR;
  }

  /*-------------------------------------------------------------------------
    Set up the socket on the NAT iface that would be used to fwd the DNS
    request to the UM side and also receive the DNS response coming from 
    the DNS server.
  -------------------------------------------------------------------------*/
  
  /* Same policy as used for the receiver socket above. */

  dss_ret = dss_socket2(DSS_AF_INET, DSS_SOCK_DGRAM, (uint8)PS_IPPROTO_UDP,
                        ps_dns_proxy_sock_cb, NULL,  
                        &policy, dss_errno);
  if (dss_ret == DSS_ERROR)
  {
    LOG_MSG_ERROR_1( "Unable to open UM socket Error %d", *dss_errno );
    (void)dss_close( dns_proxy_info.rm_sockfd, dss_errno );
    return DSS_ERROR;
  }
  dns_proxy_info.um_sockfd = dss_ret;
  LOG_MSG_INFO2_1("Created UM side socket fd %d", dss_ret);

  dss_sockaddr.ps_sin_family = DSS_AF_INET;
  dss_sockaddr.ps_sin_port   = 0;
  dss_sockaddr.ps_sin_addr.ps_s_addr = 0;

  dss_ret = dss_bind(dns_proxy_info.um_sockfd,
                     (struct ps_sockaddr*)&dss_sockaddr,
                     sizeof(struct ps_sockaddr_in),
                     dss_errno);
  if (dss_ret == DSS_ERROR)
  {
    LOG_MSG_ERROR_1("Unable to bind to DNS server port %d", *dss_errno);
    (void)dss_close( dns_proxy_info.rm_sockfd, dss_errno );
    (void)dss_close( dns_proxy_info.um_sockfd, dss_errno );
    return DSS_ERROR;
  }

  ret_val = dss_async_select(dns_proxy_info.um_sockfd, 
                             DS_READ_EVENT, 
                             dss_errno);
  if (ret_val == DSS_ERROR)
  {
    LOG_MSG_ERROR_2( "Failed async select READ on socket: %d error: %d",
                     dns_proxy_info.um_sockfd, *dss_errno );
    (void)dss_close( dns_proxy_info.rm_sockfd, dss_errno );
    (void)dss_close( dns_proxy_info.um_sockfd, dss_errno );
    return DSS_ERROR;
  }

  return DSS_SUCCESS;
} /* dns_proxyi_enable() */

/*===========================================================================
FUNCTION DNS_PROXYI_DISABLE()

DESCRIPTION
  This function disables the DNS Proxy. Since we support only one DNS proxy
  in the system, the input nat_iface_ptr to this function should be the same
  as used when the DNS Proxy was enabled.

DEPENDENCIES

PARAMETERS
  nat_iface_ptr: The iface on which DNS proxy is running.
  dss_errno: Error number, if any.

RETURN VALUE
  Success  0
  Fail    -1
  

SIDE EFFECTS
  None
===========================================================================*/
int32
dns_proxyi_disable
(
 ps_iface_type               *nat_iface_ptr,
 sint15                      *dss_errno 
)
{
  dsm_item_type  *dss_packet;
/*-------------------------------------------------------------------------*/

  if (!PS_IFACE_IS_VALID(nat_iface_ptr))
  {
    LOG_MSG_ERROR_1("Invalid NAT Iface 0x%p", nat_iface_ptr);
    *dss_errno = DS_EFAULT;
    return DSS_ERROR;
  }

  if (dns_proxy_info.nat_iface_ptr != nat_iface_ptr)
  {
    LOG_MSG_ERROR_0("Disable DNS Proxy called with unknown NAT Iface");
  }

  /* Free anything left on the write WM's */
  while ( NULL != (dss_packet = dsm_dequeue(&dns_proxy_info.rm_write_wm)) )
  {
    dsm_free_packet( &dss_packet );
  }

  while ( NULL != (dss_packet = dsm_dequeue(&dns_proxy_info.um_write_wm)) )
  {
    dsm_free_packet( &dss_packet );
  }

  /* Destroy the Queue*/
  dsm_queue_destroy(&dns_proxy_info.rm_write_wm);
  dsm_queue_destroy(&dns_proxy_info.um_write_wm);

  /*Close the sockets*/
  (void)dss_close(dns_proxy_info.rm_sockfd, dss_errno);
  (void)dss_close(dns_proxy_info.um_sockfd, dss_errno);

  /* Reset the Pending Query Array */
  memset(dns_proxy_query_array, 0, 
         sizeof(ps_dns_proxy_query_entry_type) * NUM_DNS_PROXY_ENTRIES);

  /*Memset the dns proxy global info*/
  memset(&dns_proxy_info, 0, sizeof(ps_dns_proxy_info_type));

  return DSS_SUCCESS;
} /* dns_proxyi_disable() */

