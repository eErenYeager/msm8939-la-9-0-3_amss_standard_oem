
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

     P S  M M G S D I  C L I E N T   F I L E

GENERAL DESCRIPTION
  Acts as a client to MMGSDI and registers for RUIM card events.

Copyright (c) 2008-2013 Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $PVCSPath: L:/src/asw/MM_DATA/vcs/dss_ps.c_v   1.13   24 Feb 2003 18:32:12   akuzhiyi  $
  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/protocols/inet/src/ps_mmgsdi_client.c#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when        who    what, where, why
--------    ---    ----------------------------------------------------------
01/30/13    ds     Replacing LOG_MSG_FATAL_ERROR macro with LOG_MSG_ERROR.
12/14/12    fn     Remove default NV item writes when NV read fails 
08/20/11    ttv    Merged legacy GSDI/MMGSDI APIs removal from PS
01/10/11    ss     Cleaning up of Globals for thread safety in Q6 Free 
                   Floating environment.
03/26/09    pp     CMI De-featurization.
09/04/08    am     Created module.
===========================================================================*/


/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"
#include "amssassert.h"
#include "msg.h"
#include "nv_items.h"
#include "ps_mmgsdi_client.h"
#include "ps_iface_defs.h"
#include "ps_svc.h"
#include "ps_utils.h"
#include "ps_socki_defs.h"
#include "ps_tcp_config.h"

#ifdef FEATURE_MMGSDI
#include "mmgsdilib.h"
#endif /* FEATURE_MMGSDI */

#include "ds_Utils_DebugMsg.h"
#include "ps_system_heap.h"

/*===========================================================================

            REGIONAL DEFINITIONS AND DECLARATIONS FOR MODULE

===========================================================================*/
/*= = = = = = = =  = = = = = = = = = =  = = =  = = = = = = = = = = = = = = =
                    INTERNAL FUNCTION DEFINTIONS
= = = = = = = = = = = = = = =  = = = = =  = = = = = = = = = = = = = = = = =*/
#ifdef FEATURE_MMGSDI
/*===========================================================================
FUNCTION PS_READ_NV()

DESCRIPTION
  This function performs NV read for items featured on the RUIM card.
  Current NVs read:
    RUIM EF name: UIM_CDMA_3GPD_TCPCONFIG
    NV_TCP_GRACEFUL_DORMANT_CLOSE_I
    NV_TCP_KEEPALIVE_IDLE_TIME_I

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
static void
ps_read_nv
(
  void
)
{
  nv_stat_enum_type nv_status;
  nv_item_type     *ps_nv_item_ptr;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------------
    Allocate temporary memory for the NV item
  -------------------------------------------------------------------------*/
  ps_nv_item_ptr = (nv_item_type *)
    ps_system_heap_mem_alloc(sizeof(nv_item_type));
  if( ps_nv_item_ptr == NULL )
  {
    LOG_MSG_ERROR_0("Mem alloc from system heap failed.");
    ASSERT(0);
  }

  /*-------------------------------------------------------------------------
    Retrieve the value of tcp_graceful_dormant_close flag from the NV
  -------------------------------------------------------------------------*/
  nv_status =
    ps_get_nv_item(NV_TCP_GRACEFUL_DORMANT_CLOSE_I, ps_nv_item_ptr);
  if( nv_status != NV_DONE_S )
  {
    LOG_MSG_INFO1_1("TCP graceful dormant close flag unavailable, nv_status %d,"
                    " using default", nv_status);
    /*-----------------------------------------------------------------------
      Set the global variable with default value and then set the NV item to
      default value
   ------------------------------------------------------------------------*/
    sock_config_cb.tcp_graceful_dormant_close = FALSE;
    ps_nv_item_ptr->tcp_graceful_dormant_close = FALSE;
  }
  else
  {
    sock_config_cb.tcp_graceful_dormant_close =
      ps_nv_item_ptr->tcp_graceful_dormant_close;
  }

  /*-------------------------------------------------------------------------
    Retrieve the value of tcp_keepalive_idle_time from the NV
  -------------------------------------------------------------------------*/
  nv_status = ps_get_nv_item(NV_TCP_KEEPALIVE_IDLE_TIME_I, ps_nv_item_ptr);
  if( nv_status != NV_DONE_S )
  {
    LOG_MSG_INFO1_1("TCP keepalive idle time unavailable, nv_status %d,"
                    " using default", nv_status);
    /*-----------------------------------------------------------------------
      set the global variable with default value and then set the NV item
      to default value
     ----------------------------------------------------------------------*/
    sock_config_cb.tcp_keepalive_idle_time = TCP_KEEPALIVE_IDLE_TIME;
    ps_nv_item_ptr->tcp_keepalive_idle_time = TCP_KEEPALIVE_IDLE_TIME/(60 * 1000);
  }
  else
  {
    /*-----------------------------------------------------------------------
     The NV keepalive time is supplied in minutes. Convert it to milliseconds
    -----------------------------------------------------------------------*/
     sock_config_cb.tcp_keepalive_idle_time =
       ((ps_nv_item_ptr->tcp_keepalive_idle_time) * 60 * 1000);
  }

  PS_SYSTEM_HEAP_MEM_FREE(ps_nv_item_ptr);
} /* ps_read_nv() */

/*===========================================================================
FUNCTION PS_MMGSDI_CLIENT_PROCESS_EVENT_CMD()

DESCRIPTION
  Called when an NV item read is to be performed.
  This function processes commands to read NV items placed in the SIM Card's
  elementary file system.

  Function is handled within PS task.

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
static void
ps_mmgsdi_client_process_event_cmd
(
  ps_cmd_enum_type      cmd_id,
  void                  *userdata_ptr
)
{
  mmgsdi_events_enum_type evt_recvd;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/


  if ( cmd_id != PS_SIM_EVENT_HANDLER_CMD ||
       NULL == userdata_ptr)
  {
    LOG_MSG_ERROR_1("Invalid params, cmd_id %d", cmd_id);
    ASSERT(0);
    return;
  }

  evt_recvd = (mmgsdi_events_enum_type)userdata_ptr;

  switch(evt_recvd)
  {
#ifdef FEATURE_MMGSDI_SESSION_LIB
    case MMGSDI_SUBSCRIPTION_READY_EVT:
      ps_read_nv();
      break;
#endif /* FEATURE_MMGSDI_SESSION_LIB */

    default:
      LOG_MSG_ERROR_1("Invalid event %d", evt_recvd);
      break;
  }

} /* ps_mmgsdi_client_process_event_cmd() */

#endif /* FEATURE_MMGSDI */

/*= = = = = = = =  = = = = = = = = = =  = = =  = = = = = = = = = = = = = = =
                    EXTERNAL FUNCTION DEFINTIONS
= = = = = = = = = = = = = = =  = = = = =  = = = = = = = = = = = = = = = = =*/
/*===========================================================================
FUNCTION PS_MMGSDI_CLIENT_REG_INIT()

DESCRIPTION
  This function registers PS as client to the MMGSDI module.
  If MMGSDI is not enabled, NV read is performed directly.

  Called from psi_init() in PS task.

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
void
ps_mmgsdi_client_reg_init
(
  void
)
{
  /*-------------------------------------------------------------------------
    All NVs are read in dssps_init() at init time. RUIM NVs will be re-read later.
  -------------------------------------------------------------------------*/
#ifdef FEATURE_MMGSDI
#if 0 /* This kind of registration is obsolete - DSMgr will act as a client */
  /*-------------------------------------------------------------------------
    Register as a MMGSDI client to read NV items on card power up.
  -------------------------------------------------------------------------*/
  if ( MMGSDI_SUCCESS !=
       mmgsdi_client_id_reg( ps_mmgsdi_client_id_reg_cback, 0) )
  {
    LOG_MSG_ERROR_0("MMGSDI client registration failed");
    return;
  }
#endif /* Obsolete - End */
  /*-------------------------------------------------------------------------
    Register PS cmd callback for concerned MMGSDI events.
  -------------------------------------------------------------------------*/
  (void)ps_set_cmd_handler(PS_SIM_EVENT_HANDLER_CMD,
                           ps_mmgsdi_client_process_event_cmd);

#else
  #error code not present
#endif /* FEATURE_MMGSDI */

  return;

} /* ps_mmgsdi_client_reg_init() */

