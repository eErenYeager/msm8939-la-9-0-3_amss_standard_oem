/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                P S _ D N S _ P R O X Y _ E X T. C

GENERAL DESCRIPTION
    This file implements the external interface of DNS Proxy. DNS Proxy will 
    receive the DNS requests from internal NATed clients and will forward the
    DNS queries to the actual DNS server on the UM interface.


Copyright (c) 2012 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/
/*===========================================================================

                        EDIT HISTORY FOR MODULE

$Id: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/protocols/inet/src/ps_dns_proxy_ext.c#1 $

when        who    what, where, why
--------    ---    ----------------------------------------------------------
12/12/12    ds     Replacing LOG_MSG_FATAL_ERROR with LOG_MSG_ERROR.
05/20/10    rt     Added module
===========================================================================*/


/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "comdef.h"
#include "customer.h"
#include "dserrno.h"
#include "ps_mem.h"
#include "ps_svc.h"
#include "ps_dns_proxy.h"
#include "ps_dns_proxy_ext.h"

#include "ds_Utils_DebugMsg.h"

/*===========================================================================

                DEFINITIONS AND DECLARATIONS FOR MODULE

===========================================================================*/

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

                           PRIVATE FUNCTIONS

=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
/*===========================================================================
FUNCTION DNS_PROXY_ENABLE_CMD_HDLR()

DESCRIPTION
  This function enables the DNS Proxy.

  As of now only one DNS proxy can be run i.e. DNS proxy can enabled on
  only on one interface.

DEPENDENCIES

PARAMETERS

RETURN VALUE
  

SIDE EFFECTS
  None
===========================================================================*/
static void
dns_proxy_enable_cmd_hdlr
( 
  ps_cmd_enum_type   ps_cmd,
  void              *data_ptr
)
{
  ps_dns_proxy_enable_cmd_info_type  *dns_proxy_enable_info;
  sint15                              dss_errno;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (data_ptr == NULL || ps_cmd != PS_DNS_PROXY_ENABLE_CMD)
  {
    LOG_MSG_ERROR_0("DNS Proxy Enable command handler called with invalid params");
    return;
  }

  dns_proxy_enable_info = (ps_dns_proxy_enable_cmd_info_type*)data_ptr;

  LOG_MSG_INFO1_1("Processing DNS Proxy Enable CMD on NAT iface: 0x%p",
                  dns_proxy_enable_info->nat_iface_ptr);
  (void) dns_proxyi_enable(dns_proxy_enable_info->nat_iface_ptr, 
                           &dss_errno);

  PS_MEM_FREE(data_ptr);
}/* dns_proxy_enable_cmd_hdlr() */

/*===========================================================================
FUNCTION DNS_PROXY_DISABLE_CMD_HDLR()

DESCRIPTION
  This function disables the DNS Proxy.

DEPENDENCIES

PARAMETERS

RETURN VALUE
  

SIDE EFFECTS
  None
===========================================================================*/
static void
dns_proxy_disable_cmd_hdlr
( 
  ps_cmd_enum_type   ps_cmd,
  void              *data_ptr
)
{
  ps_dns_proxy_disable_cmd_info_type  *dns_proxy_disable_info;
  sint15                               dss_errno;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (data_ptr == NULL || ps_cmd != PS_DNS_PROXY_DISABLE_CMD)
  {
    LOG_MSG_ERROR_0("DNS Proxy Disable command handler called with invalid params");
    return;
  }

  dns_proxy_disable_info = (ps_dns_proxy_disable_cmd_info_type*)data_ptr;

  LOG_MSG_INFO1_1("Processing DNS Proxy Disable CMD on iface RM: 0x%p",
                  dns_proxy_disable_info->nat_iface_ptr);
  (void) dns_proxyi_disable(dns_proxy_disable_info->nat_iface_ptr, &dss_errno);

  PS_MEM_FREE(data_ptr);
} /* dns_proxy_disable_cmd_hdlr() */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

                           EXTERNALIZED FUNTIONS

=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
/*===========================================================================
FUNCTION DNS_PROXY_ENABLE()

DESCRIPTION
  This function enables the DNS Proxy. It takes two iface pointers as input.
  The RM iface pointer is the iface where DNS proxy will receive requests.
  The UM iface pointer is the iface where DNS request will be forwarded to
  the actual DNS server that is configured on the UM iface.

  As of now only one DNS proxy can be run i.e. DNS proxy can enabled on
  only on one interface.

DEPENDENCIES

PARAMETERS
  nat_iface_ptr: Pointer to the iface where DNS proxy will run
  dss_errno:     Error, if any.

RETURN VALUE
  Success  0
  Fail    -1
  

SIDE EFFECTS
  None
===========================================================================*/
int32
dns_proxy_enable
(
 ps_iface_type               *nat_iface_ptr,
 sint15                      *dss_errno 
)
{
  ps_dns_proxy_enable_cmd_info_type  *dns_proxy_enable_info;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (!PS_IFACE_IS_VALID(nat_iface_ptr))
  {
    LOG_MSG_ERROR_1("Invalid NAT Iface to run DNS Proxy 0x%p", nat_iface_ptr);
    *dss_errno = DS_EFAULT;
    return -1;
  }

  if(dss_errno == NULL)
  {
    LOG_MSG_ERROR_0("NULL dss_errno passed");
    return -1;
  }
  
  LOG_MSG_INFO1_1("Enabling DNS Proxy on NAT iface: 0x%p", nat_iface_ptr);
  
  dns_proxy_enable_info = (ps_dns_proxy_enable_cmd_info_type *)
    ps_mem_get_buf(PS_MEM_DNS_PROXY_ENABLE_INFO_TYPE);

  if (dns_proxy_enable_info == NULL)
  {
    LOG_MSG_ERROR_0("Couldn't create DNS proxy enable info");
    *dss_errno = DS_ENOMEM;
    return -1;
  }

  dns_proxy_enable_info->nat_iface_ptr = nat_iface_ptr;

  (void) ps_set_cmd_handler( PS_DNS_PROXY_ENABLE_CMD, dns_proxy_enable_cmd_hdlr );
  ps_send_cmd(PS_DNS_PROXY_ENABLE_CMD, (void*)dns_proxy_enable_info);

  return 0;
}

/*===========================================================================
FUNCTION DNS_PROXY_DISABLE()

DESCRIPTION
  This function disables the DNS Proxy. Since we support only one DNS proxy
  in the system, the input nat_iface_ptr to this function should be the same
  as used when the DNS Proxy was enabled.

DEPENDENCIES

PARAMETERS
  nat_iface_ptr: The iface on which DNS proxy is running.
  dss_errno: Error number, if any.

RETURN VALUE
  Success  0
  Fail    -1
  

SIDE EFFECTS
  None
===========================================================================*/
int32
dns_proxy_disable
(
 ps_iface_type               *nat_iface_ptr,
 sint15                      *dss_errno 
)
{
  ps_dns_proxy_disable_cmd_info_type  *dns_proxy_disable_info;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (!PS_IFACE_IS_VALID(nat_iface_ptr))
  {
    LOG_MSG_ERROR_1("Invalid NAT Iface 0x%p", nat_iface_ptr);
    *dss_errno = DS_EFAULT;
    return -1;
  }

  if(dss_errno == NULL)
  {
    LOG_MSG_ERROR_0("NULL dss_errno passed");
    return -1;
  }

  LOG_MSG_INFO1_1("Disabling DNS Proxy NAT iface: 0x%p ", nat_iface_ptr);

  dns_proxy_disable_info = (ps_dns_proxy_disable_cmd_info_type *)
    ps_mem_get_buf(PS_MEM_DNS_PROXY_DISABLE_INFO_TYPE);

  if (dns_proxy_disable_info == NULL)
  {
    LOG_MSG_ERROR_0("Couldn't create DNS proxy disable info");
    *dss_errno = DS_ENOMEM;
    return -1;
  }

  dns_proxy_disable_info->nat_iface_ptr = nat_iface_ptr;

  (void) ps_set_cmd_handler( PS_DNS_PROXY_DISABLE_CMD, dns_proxy_disable_cmd_hdlr );
  ps_send_cmd(PS_DNS_PROXY_DISABLE_CMD, (void*)dns_proxy_disable_info);

  return 0;
}


