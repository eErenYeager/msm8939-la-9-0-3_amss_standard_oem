#ifndef PS_MMGSDI_CLIENT_H
#define PS_MMGSDI_CLIENT_H
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

  P S  M M G S D I  C L I E N T  H E A D E R  F I L E

GENERAL DESCRIPTION
  Acts as a client to MMGSDI and registers for RUIM card events.

Copyright (c) 2008 Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/


/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/protocols/inet/inc/ps_mmgsdi_client.h#1 $ 
  $Author: mplp4svc $
  $DateTime: 2015/01/27 06:42:19 $

when        who    what, where, why
--------    ---    ----------------------------------------------------------
09/04/08    am     Created.

===========================================================================*/


/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "comdef.h"
#include "customer.h"

/*= = = = = = = =  = = = = = = = = = =  = = =  = = = = = = = = = = = = = = =
                    EXTERNAL FUNCTION DEFINTIONS
= = = = = = = = = = = = = = =  = = = = =  = = = = = = = = = = = = = = = = =*/
/*===========================================================================
FUNCTION PS_MMGSDI_CLIENT_REG_INIT()

DESCRIPTION
  This function registers PS as client to the MMGSDI module.

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
void ps_mmgsdi_client_reg_init
(
  void
);

#endif /* PS_MMGSDI_CLIENT_H */

