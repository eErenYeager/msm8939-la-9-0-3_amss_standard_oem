#ifndef PS_EAP_CLIENT_H
#define PS_EAP_CLIENT_H


/*===========================================================================
                       E A P  C L I E N T  M O D U L E
                            
                   
DESCRIPTION
  This file contains client configuration processing functions for EAP clients.
     
    
  Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/


/*===========================================================================

                      EDIT HISTORY FOR FILE


  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.


  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/protocols/linklayer/inc/ps_eap_client_config.h#1 $ 
  $DateTime: 2016/04/29 09:27:59 $ 
  $Author: vidhiyal $


when        who    what, where, why
--------    ---    ----------------------------------------------------------
08/10/14    fn      Created module.

===========================================================================*/



/*===========================================================================

                          INCLUDE FILES FOR MODULE

===========================================================================*/

#include "datamodem_variation.h"
#include "customer.h"
#include "comdef.h"

#ifdef FEATURE_DATA_PS_EAP
#include "ps_eap.h"
/*===========================================================================

                    EXTERNAL DEFINITIONS AND DECLARATIONS FOR MODULE

===========================================================================*/
typedef enum
{
  EAP_SUBS_PRIMARY   = 0x01,
  EAP_SUBS_SECONDARY = 0x02,
  EAP_SUBS_TERTIARY  = 0x03,
  EAP_SUBS_MAX
} eap_client_config_subs_e_type;

/*===========================================================================

                     EXTERNAL FUNCTION DECLARATIONS

===========================================================================*/

/*===========================================================================

FUNCTION EAP_CLIENT_CONFIG_GET_SESSION_TYPE

DESCRIPTION
  Get the session type from subscription and algo type. If user does not 
 have a preference eap_sim_aka_algo then EAP_SIM_AKA_ALGO_NONE should be 
 passed in.
 
DEPENDENCIES 
  

RETURN VALUE
  uint32 session type
 
SIDE EFFECTS

===========================================================================*/
uint32 eap_client_config_get_session_type
(
  eap_client_config_subs_e_type  subscription,
  eap_sim_aka_algo_enum_type     eap_sim_aka_algo
);

#endif /* FEATURE_DATA_PS_EAP */
#endif /* PS_EAP_CLIENT_H */
