#ifndef HDLC_DRV_H
#define HDLC_DRV_H

/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                        H D L C    D R I V E R

                        H E A D E R    F I L E

GENERAL DESCRIPTION
  This module contains definitions and declarations for using the
  HDLC ( High-level Data Link Control ) driver.


EXTERNALIZED FUNCTIONS

  HDLC_DRV_WRITE_FRAMER
    Writes data into framer input buffer.

  HDLC_DRV_READ_FRAMER
    Reads data out of framer output buffer.

  HDLC_DRV_WRITE_DEFRAMER
    Writes data into deframer input buffer.

  HDLC_DRV_READ_DEFRAMER
    Reads data out of deframer output buffer.

  HDLC_DRV_DEFRAMER_XLATE_DATA_STATE_HW_TO_SW
    Translates data state from hardware to software.

  HDLC_DRV_DEFRAMER_XLATE_DATA_STATE_SW_TO_HW
    Translates data state from software to hardware.

  INLINED FUNCTIONS IN THIS HEADER FILE
  -------------------------------------
  HDLC_DRV_GET_DEFRAMER_STATE
    This function is used to get the deframing state (the values of CRC and
    data state registers), which can be cached by software for subsequent
    restoral using the set function below.

  HDLC_DRV_SET_DEFRAMER_STATE
    This function is used to set the deframing state (the CRC and data state
    registers), previously cached by software using the get function above.

  HDLC_DRV_DEFRAMER_RESET
    This function is used to perform software reset of the HDLC deframer.

  HDLC_DRV_DEFRAMER_WRITE_DONE
    This function is used to indicate to the hardware that a complete packet
    has been written to the deframer buffer for the deframing operation to
    begin.

  HDLC_DRV_DEFRAMER_NUM_FRAMES
    This function returns the number of deframed packets in the deframer
    buffer.

  HDLC_DRV_DEFRAMER_FRAME_SIZE
    Returns the size of the unframed packet, whether it contains the end of
    the packet, and if the CRC passed.

  HDLC_DRV_DEFRAMER_ERROR
    This function returns TRUE if an error occured during deframing.

  HDLC_DRV_FRAMER_SET_ACCM
    This function is used to set the Asynchronous Control Charater Map (ACCM)
    to be used for framing

  HDLC_DRV_FRAMER_RESET
    This function is used to perform the software reset of the HDLC framer

  HDLC_DRV_FRAMER_WRITE_DONE
    This function is used to indicate to the hardware that a complete packet
    has been written to the framer for the framing operation to begin.

  HDLC_DRV_FRAMER_INSERT_FLAG
    This function is used to force the framer to insert a 0x7E byte into the
    framer buffer

  HDLC_DRV_FRAMER_INSERT_CRC
    This function is used to force the framer to insert the 2 CRC bytes in the
    framer buffer.

  HDLC_DRV_FRAMER_FRAME_SIZE
    This function returns the size of the framed packet in the framer output
    register.

  HDLC_DRV_FRAMER_ERROR
    This function returns TRUE if an error occured during framing.

Copyright (c)2001 - 2009 Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary

*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/


/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$PVCSPath: L:/src/asw/MM_DATA/vcs/hdlc_drv.h_v   1.2   04 Dec 2002 00:09:26   vramaswa  $
$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/protocols/linklayer/src/hdlc_drv.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when        who    what, where, why
--------    ---    ----------------------------------------------------------
08/25/10    sch    moving macro HDLC_DRV_ALIGN_SIZE from hdlc_drv.c to this 
                   file so that it can be used in ps_hdlc_lib.c.
09/14/09    jg     Modify HDLC_DRV_MAX_DEFRAMER_INPUT_SIZE to be 224 to fix HDLC HW
                   checksum error issue for some corner case.
07/28/08    scb    Added changes under FEATURE_DATA_PS_HDLC_DSM_OPTIMIZED
04/03/08    scb    Added HDLC h/w registry mappings.
12/20/07    scb    Added retrieval of the last nine bits (as against 8) of the
                   FRAMER_BYTE_CNT_RB register to take care of edge conditions.
09/27/07    ifk    Added definitions for MIN and MAX H/W framing and deframing
                   buffers.
04/20/06    ssh    Functions to parse and preset values for the deframer
                   data state register.
11/15/05    ssh    Cleanup, added more comments
10/07/05    ssh    Redesign to achieve cleanliness and optimization
09/20/05    ssh    Added hdlc_deframer_state_type, prototypes for new
                   functions to set and get deframer state, and for hw bug in
                   newer cores.
09/06/05    ssh    Added more deframer data state field masks.
09/02/05    ssh    Added get and set macros for deframer data state and CRC
                   registers.
03/21/05    ifk    Fixed reading of PPP header from DEFRAMER_DATA_OUT register.
12/02/02    vr     Reduced HDLC_DATA_BUF_SIZE to prevent the HW
                   framer buffer from overflowing if all the bytes need to
                   be escaped.
10/08/02    vr     Reduced HDLC_MAX_FRAMER_BYTES_SMALL_POOL by 1 to prevent
10/11/02    usb    Merging file to MM_DATA archive from COMMON
10/08/02    vr     Reduced HDLC_MAX_FRAMER_BYTES_SMALL_POOL by 1 to prevent
                   potential memory scribble
08/09/02    mvl    Added FEATURE_HDLC_HW_ACCEL
07/30/02    igt    remove macros HDLC_FRAMER_CLEAR_ERR and
                   HDLC_DEFRAMER_CLEAR_ERR as the hw is now read only
06/05/02    dwp    Modify header inclusion criteria.
05/20/02    vr     Changed interfaces for hdlc_read/write_buf and code review
                   changes
12/31/01    sq     Made deframer/framer commands to be enum type.
08/12/01    sq     Created.
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"
#include "msm.h"

#ifdef FEATURE_HDLC_HW_ACCEL
#include "ps_hdlc_lib.h"
#include "ps_hdlc_libi.h"
/*===========================================================================

                      PUBLIC DATA DECLARATIONS

===========================================================================*/

/*---------------------------------------------------------------------------
  Deframer command definitions
---------------------------------------------------------------------------*/
typedef enum
{
  HDLC_DRV_DEFRAMER_RESET      = 0x0,
  HDLC_DRV_DEFRAMER_WRITE_DONE = 0x1,
  HDLC_DRV_DEFRAMER_CMD_MAX
} hdlc_drv_deframer_cmd_type;

/*---------------------------------------------------------------------------
  Framer command definitions
---------------------------------------------------------------------------*/
typedef enum
{
  HDLC_DRV_FRAMER_RESET        = 0x0,
  HDLC_DRV_FRAMER_WRITE_DONE   = 0x1,
  HDLC_DRV_FRAMER_INSERT_FLAG  = 0x2,
  HDLC_DRV_FRAMER_INSERT_CRC   = 0x3,
  HDLC_DRV_FRAMER_DRAIN        = 0x4,
  HDLC_DRV_FRAMER_CMD_MAX
} hdlc_drv_framer_cmd_type;

/*---------------------------------------------------------------------------
  The hardware expects 4-byte alignment of certain memory addresses
---------------------------------------------------------------------------*/
#define     HDLC_DRV_ALIGN_SIZE              4
#define     HDLC_DRV_ALIGN_SIZE_SHFT         2      /* For bit shifting    */

/*---------------------------------------------------------------------------
  Registry mappings for HDLC framer and deframer
---------------------------------------------------------------------------*/
#ifndef DEFRAMER_HEADER_CNT_RB

#define DEFRAMER_HEADER_CNT_RB      HWIO_DEFRAMER_HEADER_CNT_ADDR
#define DEFRAMER_BYTE_CNT_RB        HWIO_DEFRAMER_BYTE_CNT_ADDR
#define DEFRAMER_ERROR_WB           HWIO_DEFRAMER_ERROR_ADDR
#define DEFRAMER_ERROR_RB           HWIO_DEFRAMER_ERROR_ADDR

#ifdef HWIO_DEFRAMER_DATA_INn_ADDR
#define DEFRAMER_DATA_IN_WW         HWIO_DEFRAMER_DATA_INn_ADDR(0)
#else
#ifdef HWIO_DEFRAMER_DATA_INn_ADDRI
#define DEFRAMER_DATA_IN_WW         HWIO_DEFRAMER_DATA_INn_ADDRI(0)
#endif /*HWIO_DEFRAMER_DATA_INn_ADDRI*/
#endif /*HWIO_DEFRAMER_DATA_INn_ADDR*/

#ifdef HWIO_DEFRAMER_DATA_OUTn_ADDR
#define DEFRAMER_DATA_OUT_RW        HWIO_DEFRAMER_DATA_OUTn_ADDR(0)
#else
#ifdef HWIO_DEFRAMER_DATA_OUTn_ADDRI
#define DEFRAMER_DATA_OUT_RW        HWIO_DEFRAMER_DATA_OUTn_ADDRI(0)
#endif /*HWIO_DEFRAMER_DATA_OUTn_ADDRI*/
#endif /*HWIO_DEFRAMER_DATA_OUTn_ADDR*/

#define DEFRAMER_DATA_STATE_RW      HWIO_DEFRAMER_DATA_STATE_ADDR
#define DEFRAMER_CRC_STATE_RW       HWIO_DEFRAMER_CRC_STATE_ADDR
#define DEFRAMER_CMD_WB             HWIO_DEFRAMER_CMD_ADDR
#define FRAMER_ACCM_VALUE_WW        HWIO_FRAMER_ACCM_VALUE_ADDR
#define FRAMER_BYTE_CNT_RB          HWIO_FRAMER_BYTE_CNT_ADDR
#define FRAMER_ERROR_WB             HWIO_FRAMER_ERROR_ADDR
#define FRAMER_ERROR_RB             HWIO_FRAMER_ERROR_ADDR
#define FRAMER_CMD_WB               HWIO_FRAMER_CMD_ADDR

#ifdef HWIO_FRAMER_DATA_INn_ADDR
#define FRAMER_DATA_IN_WW           HWIO_FRAMER_DATA_INn_ADDR(0)
#else
#ifdef HWIO_FRAMER_DATA_INn_ADDRI
#define FRAMER_DATA_IN_WW           HWIO_FRAMER_DATA_INn_ADDRI(0)
#endif /*HWIO_FRAMER_DATA_INn_ADDRI*/
#endif /*HWIO_FRAMER_DATA_INn_ADDR*/

#ifdef HWIO_FRAMER_DATA_OUTn_ADDR
#define FRAMER_DATA_OUT_RW          HWIO_FRAMER_DATA_OUTn_ADDR(0)
#else
#ifdef HWIO_FRAMER_DATA_OUTn_ADDRI
#define FRAMER_DATA_OUT_RW          HWIO_FRAMER_DATA_OUTn_ADDRI(0)
#endif /* HWIO_FRAMER_DATA_OUTn_ADDRI*/
#endif /*HWIO_FRAMER_DATA_OUTn_ADDR*/

#define FRAMER_CMD_WB               HWIO_FRAMER_CMD_ADDR

#endif/* DEFRAMER_HEADER_CNT_RB */

/*---------------------------------------------------------------------------
  Externalized constants needed for using the HDLC hardware framer/deframer
---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
  Deframer data state field masks
---------------------------------------------------------------------------*/
#define  HDLC_DRV_DEFRAMER_DATA_STATE_ESCAPE_FLAG    0x80000000
#define  HDLC_DRV_DEFRAMER_DATA_STATE_START_FLAG     0x40000000
#define  HDLC_DRV_DEFRAMER_DATA_STATE_START_FOUND    0x20000000

/*---------------------------------------------------------------------------
  Maximum of residual bytes reported by the deframer data state register
---------------------------------------------------------------------------*/
#define MAX_RESIDUAL_BYTES     3

/*---------------------------------------------------------------------------
  Framer/deframer input data buffer sizes
---------------------------------------------------------------------------*/
#define  HDLC_DRV_DEFRAMER_BUF_SIZE  255
#define  HDLC_DRV_FRAMER_BUF_SIZE    255
#define  HDLC_DRV_FRAMER_FRM_SIZE_MASK   0x01FF

/*---------------------------------------------------------------------------
  The data buffer size of the hardware framer is 255 bytes. But in the
  worst case, we may need to escape every byte. So limit the input size to
  the framer to 127 bytes.
---------------------------------------------------------------------------*/
#define HDLC_DRV_MAX_FRAMER_INPUT_SIZE  240
#ifdef FEATURE_DATA_PS_HDLC_DSM_OPTIMIZED
#define HDLC_DRV_INITIAL_FRAMER_INPUT_SIZE  (HDLC_DRV_MAX_FRAMER_INPUT_SIZE-1)
#endif /* FEATURE_DATA_PS_HDLC_DSM_OPTIMIZED */
#define HDLC_DRV_MIN_FRAMER_INPUT_SIZE  \
  (((HDLC_DRV_FRAMER_BUF_SIZE/2) - 4) & ~0x3)

/*---------------------------------------------------------------------------
  The maximum output buffer size for the hardware deframer is 256 bytes.
  However, the output of the deframer contains one 4-byte header for each
  complete PPP packet that is deframed and usually contains one 4-byte
  trailer at the end of the output buffer. In the worst case, none of the
  bytes in the input PPP packets to the deframer could be escaped and the
  total number of the bytes in the deframer output will be equal to

  total_input_bytes + (num_hdrs * 4) - num_7Es_delimiting_PPP_frames

  As the result the maximum number of bytes we can input to the deframer
  in one write is less than 256. The very worst case would be all one byte
  packets to the deframer that would give us total_input_bytes = 256/4 = 64
---------------------------------------------------------------------------*/
#define HDLC_DRV_MAX_DEFRAMER_INPUT_SIZE  224
#define HDLC_DRV_MIN_DEFRAMER_INPUT_SIZE  \
  ((HDLC_DRV_DEFRAMER_BUF_SIZE/4) & ~0x3)


/*===========================================================================

                      PUBLIC FUNCTION DECLARATIONS

===========================================================================*/

/*===========================================================================
FUNCTION HDLC_DRV_WRITE_FRAMER

DESCRIPTION
  Writes data into framer input buffer.
  This function byte-transfers the initial non-aligned bytes (if any),
  dword-transfers the aligned bytes, and finally byte-transfers the trailing
  non-aligned bytes (if any).

  +-------+-------+ -----------------------+-------+-------+
  |  (0-3 bytes)  |  ..   |   ..   |  ..   |  (0-3 bytes)  |
  +-------+-------+ -----------------------+-------+-------+
  <- unaligned  -><- - -   ALIGNED   - - -><-  unaligned  ->

DEPENDENCIES
  None.

PARAMETERS
  (IN) buf is the pointer to the buffer to write from.
  (IN) length is the number of bytes to write from the buffer.

RETURN VALUE
  Number of bytes written to the framer input buffer

SIDE EFFECTS
  None.
===========================================================================*/
uint16 hdlc_drv_write_framer
(
  uint8  * buf,
  uint16   length
);

/*===========================================================================
FUNCTION HDLC_DRV_READ_FRAMER

DESCRIPTION
  Reads data out of framer output buffer.
  The buffer to write to must be 4-byte aligned. The function dword-transfers
  the aligned bytes, and then reads the last dword, processing the contained
  (less than 4) bytes in that dword assuming them to be left-aligned.

  + -----------------------+-------+-------+
  |  ..   |   ..   |  ..   |  (0-3 bytes)  |
  + -----------------------+-------+-------+
  <- - -   ALIGNED   - - -><-  unaligned  ->

DEPENDENCIES
  buf must be 4-byte aligned

PARAMETERS
  (OUT) buf is the buffer to be read into.
  (IN) length is the number of bytes to read

RETURN VALUE
  Number of bytes read out of the framer output buffer.

SIDE EFFECTS
  None.
===========================================================================*/
uint16 hdlc_drv_read_framer
(
  uint8  * buf,
  uint16   length
);

/*===========================================================================
FUNCTION HDLC_DRV_WRITE_DEFRAMER

DESCRIPTION
  Writes data into deframer input buffer.
  This function byte-transfers the initial non-aligned bytes (if any),
  dword-transfers the aligned bytes, and finally byte-transfers the trailing
  non-aligned bytes (if any).

  +-------+-------+ -----------------------+-------+-------+
  |  (0-3 bytes)  |  ..   |   ..   |  ..   |  (0-3 bytes)  |
  +-------+-------+ -----------------------+-------+-------+
  <- unaligned  -><- - -   ALIGNED   - - -><-  unaligned  ->

DEPENDENCIES
  None.

PARAMETERS
  (IN) buf is the buffer to be written.
  (IN) length is the number of bytes to write from buf

RETURN VALUE
  Number of bytes written to the deframer input buffer

SIDE EFFECTS
  None.
===========================================================================*/
uint16 hdlc_drv_write_deframer
(
  uint8  *buf,
  uint16 length
);

/*===========================================================================
FUNCTION HDLC_DRV_READ_DEFRAMER

DESCRIPTION
  Reads data out of deframer output buffer.
  The buffer to write to must be 4-byte aligned. The function dword-transfers
  the aligned bytes, and then reads the last dword, processing the contained
  (less than 4) bytes in that dword assuming them to be left-aligned.

  + -----------------------+-------+-------+
  |  ..   |   ..   |  ..   |  (0-3 bytes)  |
  + -----------------------+-------+-------+
  <- - -   ALIGNED   - - -><-  unaligned  ->

DEPENDENCIES
  buf must be 4-byte aligned

PARAMETERS
  (OUT) buf is the buffer to be read into.
  (IN) length is the number of bytes to read

RETURN VALUE
  Number of bytes read out of the deframer output buffer.

SIDE EFFECTS
  None.
===========================================================================*/
uint16 hdlc_drv_read_deframer
(
  uint8  * buf,
  uint16   length
);

/*===========================================================================
FUNCTION  HDLC_DRV_DEFRAMER_XLATE_DATA_STATE_HW_TO_SW

DESCRIPTION
  Interprets passed 32-bit data_state to extract the information needed
  by the software variant of the HDLC deframer (maintained in
  ps_hdlc_lib.c)

DEPENDENCIES
  None.

PARAMETERS
  (IN) data_state: The 32-bit value to interpret
  (OUT) escape: If escape flag is set, this is set to TRUE, else FALSE.
  (OUT) unframe_state: The Start_Flag and Start_Found bits are examined to
        provide an output value for this.
  (OUT) num_residual_bytes: The number of residual bytes in data_state (0-3)
  (OUT) residual_bytes_array: The residual bytes themselves (0-3)
  (IN)  residual_bytes_array_len: Size of residual_bytes_array passed in,
        should be atleast MAX_RESIDUAL_BYTES.

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
int hdlc_drv_deframer_xlate_data_state_hw_to_sw
(
  uint32     data_state,
  boolean  * escape,
  uint8    * unframe_state,
  uint8    * num_residual_bytes,
  uint8    * residual_bytes_array,
  uint8      residual_bytes_array_len
);

/*===========================================================================
FUNCTION HDLC_DRV_DEFRAMER_XLATE_DATA_STATE_SW_TO_HW

DESCRIPTION
  Sets up the 32-bit data_state output paramters according to the passed
  escape and unframe_state info. This function will likely be followed by a
  hdlc_drv_set_deframer_state() call by the driver client to set the
  deframer data_state register.
  The data_state variable passed in is cleared initially, and then
  appropriate bits set according to the input.

  Interpretation of the START_FLAG and START_FOUND bits in the deframer
  data state register. START_FLAG bit set means that the the last byte sent
  to the deframer in the previous RLP packet was 0x7e. START_FOUND bit set
  means that there was at least one 0x7e somewhere in the previous RLP pkt.

  +------------+-------------+--------------------------------------------+
  | Start_Flag | Start_Found | Equivalent "unframing" state               |
  +------------+-------------+--------------------------------------------+
  |    0       |     0       | looking for 7e( FINDING_7E )               |
  +------------+-------------+--------------------------------------------+
  |    1       |     1       | Start found and was also the last byte of  |
  |            |             | prev packet ( FINDING_SOP, i.e. skip 7Es ) |
  +------------+-------------+--------------------------------------------+
  |    0       |     1       | Start found in previous packet (process    |
  |            |             | data as part of prev packet ( UNFRAMING )  |
  +------------+-------------+--------------------------------------------+
  |    1       |     0       |  Invalid (Disallowed)                      |
  +------------+-------------+--------------------------------------------+

DEPENDENCIES
  None.

PARAMETERS
  (IN) escape: If this is TRUE, the escape flag in data_state is set.
  (IN) unframe_state: The unframing state to use to preset the Start_Flag
       and Start_Found bits in data_state
  (OUT) data_state: The mod'ed data_state

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
int hdlc_drv_deframer_xlate_data_state_sw_to_hw
(
  boolean   escape,
  uint8     unframe_state,
  uint32  * data_state
);

/*===========================================================================
FUNCTION HDLC_DRV_GET_DEFRAMER_STATE

DESCRIPTION
  This function is used to get the values of CRC and data state registers
  (which is essentially the state hardware needs to continue it's previous
  deframing operation on the same stream). The returned values can be cached
  by software, and the hardware state can be restored for the subsequent
  deframing operation by using the companion function
  hdlc_drv_set_deframer_state().

DEPENDENCIES
  None

PARAMETERS
  (OUT) uint16 *crc_state : Address where the value of CRC register should
                            be written.
  (OUT) uint32 *data_state: Address where the value of data state register
                            should be written.

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
INLINE void hdlc_drv_get_deframer_state
(
  uint16  * crc_state,
  uint32  * data_state
)
{
  *crc_state  = inpw( DEFRAMER_CRC_STATE_RW );
  *data_state = inpdw( DEFRAMER_DATA_STATE_RW );
}

/*===========================================================================
FUNCTION HDLC_DRV_SET_DEFRAMER_STATE

DESCRIPTION
  This function is used to set the CRC and data state registers (which is
  essentially the state hardware needs to continue it's previous deframing
  operation on the same stream). This function can be used to restore
  hardware state for the upcoming deframer operation with values previously
  cached by software using the companion function
  hdlc_drv_get_deframer_state()

DEPENDENCIES
  None

PARAMETERS
  (IN) uint16 crc_state: The value to be written to to CRC state register.
  (IN) uint32 data_state: The value to be written to data state register.

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
INLINE void hdlc_drv_set_deframer_state
(
  uint16  crc_state,
  uint32  data_state
)
{
  /* Reset deframer before restoring state */
  outp( DEFRAMER_CMD_WB, HDLC_DRV_DEFRAMER_RESET );

  outpw( DEFRAMER_CRC_STATE_RW, crc_state );
  outpdw( DEFRAMER_DATA_STATE_RW, data_state );
}

/*===========================================================================
FUNCTION HDLC_DRV_DEFRAMER_RESET

DESCRIPTION
  This function is used to perform software reset of the HDLC deframer.

DEPENDENCIES
  None

PARAMETERS
  None

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
INLINE void hdlc_drv_deframer_reset(void)
{
  outp( DEFRAMER_CMD_WB, HDLC_DRV_DEFRAMER_RESET );
}

/*===========================================================================
FUNCTION HDLC_DRV_DEFRAMER_WRITE_DONE

DESCRIPTION
  This function is used to indicate to the hardware that a complete packet
  has been written to the deframer buffer for the deframing operation to
  begin.

DEPENDENCIES
  None

PARAMETERS
  None

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
INLINE void hdlc_drv_deframer_write_done(void)
{
  outp( DEFRAMER_CMD_WB, HDLC_DRV_DEFRAMER_WRITE_DONE );
}

/*===========================================================================
FUNCTION HDLC_DRV_DEFRAMER_NUM_FRAMES

DESCRIPTION
  This function returns the number of deframed packets in the deframer
  buffer.

DEPENDENCIES
  None

PARAMETERS
  None

RETURN VALUE
  Number of unframed packets in the deframer buffer.

SIDE EFFECTS
  None.
===========================================================================*/
INLINE uint8 hdlc_drv_deframer_num_frames(void)
{
  return (inp( DEFRAMER_HEADER_CNT_RB ));
}

/*===========================================================================
FUNCTION HDLC_DRV_DEFRAMER_FRAME_SIZE

DESCRIPTION
  This function returns the size of the unframed packet, as well as if
  this contains the end of the packet(via out param frame_end), and if the
  CRC is valid (via out parameter crc_is_valid).

DEPENDENCIES
  None

PARAMETERS
  (OUT) boolean *frame_end: Indicates if this is the end of the PPP packet
  (OUT) boolean *crc_is_valid: Indicates if the checksum passed for this
        packet. This is relevant only if frame_end returned is TRUE.


RETURN VALUE
  The size of the unframed packet in the deframer output register.

SIDE EFFECTS
  None.
===========================================================================*/
INLINE uint32 hdlc_drv_deframer_frame_size
(
  boolean  * frame_end,
  boolean  * crc_is_valid
)
{
  uint32 frame_hdr;
  const uint32 CRC_PASS_MASK   = 0x80000000;
  const uint32 PPP_END_MASK    = 0x40000000;
  const uint32 PPP_LENGTH_MASK = 0x000000ff;

  frame_hdr     = inpdw( DEFRAMER_DATA_OUT_RW );
  *frame_end    = ((boolean)((frame_hdr & PPP_END_MASK) != 0));
  *crc_is_valid = ((boolean)((frame_hdr & CRC_PASS_MASK) != 0));

  return (frame_hdr & PPP_LENGTH_MASK);
}

/*===========================================================================
FUNCTION HDLC_DRV_DEFRAMER_ERROR

DESCRIPTION
  This function returns TRUE if an error occured during deframing.

DEPENDENCIES
  None

PARAMETERS
  None

RETURN VALUE
  TRUE: An error occured during deframing.
  FALSE: No error occured during deframing.

SIDE EFFECTS
  None.
===========================================================================*/
INLINE boolean hdlc_drv_deframer_error(void)
{
  return ((boolean)((inp( DEFRAMER_ERROR_RB )) != 0));
}

/*===========================================================================
FUNCTION HDLC_DRV_FRAMER_SET_ACCM

DESCRIPTION
  This function is used to set the Asynchronous Control Charater Map (ACCM)
  to be used for framing

DEPENDENCIES
  None

PARAMETERS
  (IN) uint32 accm: The ACCM value to be used for framing.

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
INLINE void hdlc_drv_framer_set_accm(uint32 accm)
{
  outpdw( FRAMER_ACCM_VALUE_WW, accm );
}

/*===========================================================================
FUNCTION HDLC_DRV_FRAMER_RESET

DESCRIPTION
  This function is used to perform the software reset of the HDLC framer

DEPENDENCIES
  None

PARAMETERS
  None

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
INLINE void hdlc_drv_framer_reset(void)
{
  outp( FRAMER_CMD_WB, HDLC_DRV_FRAMER_RESET );
}

/*===========================================================================
FUNCTION HDLC_DRV_FRAMER_WRITE_DONE

DESCRIPTION
  This function is used to indicate to the hardware that a complete packet
  has been written to the framer for the framing operation to begin.

DEPENDENCIES
  None

PARAMETERS
  None

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
INLINE void hdlc_drv_framer_write_done(void)
{
  outp( FRAMER_CMD_WB, HDLC_DRV_FRAMER_DRAIN );
  outp( FRAMER_CMD_WB, HDLC_DRV_FRAMER_WRITE_DONE );
}

/*===========================================================================
FUNCTION HDLC_DRV_FRAMER_INSERT_FLAG

DESCRIPTION
  This function is used to force the framer to insert a 0x7E byte into the
  framer buffer

DEPENDENCIES
  None

PARAMETERS
  None

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
INLINE void hdlc_drv_framer_insert_flag(void)
{
  outp( FRAMER_CMD_WB, HDLC_DRV_FRAMER_INSERT_FLAG );
}

/*===========================================================================
FUNCTION HDLC_DRV_FRAMER_INSERT_CRC

DESCRIPTION
  This function is used to force the framer to insert the 2 CRC bytes in the
  framer buffer.

DEPENDENCIES
  None

PARAMETERS
  None

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
INLINE void hdlc_drv_framer_insert_crc(void)
{
  outp( FRAMER_CMD_WB, HDLC_DRV_FRAMER_INSERT_CRC );
}

/*===========================================================================
FUNCTION HDLC_DRV_FRAMER_FRAME_SIZE

DESCRIPTION
  This function returns the size of the framed packet in the framer output
  register.

DEPENDENCIES
  None

PARAMETERS
  None

RETURN VALUE
  The size of the framed packet in the framer output register.

SIDE EFFECTS
  None.
===========================================================================*/
INLINE int hdlc_drv_framer_frame_size(void)
{
  return ((inpw( FRAMER_BYTE_CNT_RB )) & HDLC_DRV_FRAMER_FRM_SIZE_MASK);
}

/*===========================================================================
FUNCTION HDLC_DRV_FRAMER_ERROR

DESCRIPTION
  This function returns TRUE if an error occured during framing.

DEPENDENCIES
  None

PARAMETERS
  None

RETURN VALUE
  TRUE: An error occured during framing.
  FALSE: No error occured during framing.

SIDE EFFECTS
  None.
===========================================================================*/
INLINE boolean hdlc_drv_framer_error(void)
{
  return ((boolean)((inp( FRAMER_ERROR_RB )) != 0));
}

#endif /* FEATURE_HDLC_HW_ACCEL */
#endif /* FEATURE_DATA_PS */
