/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                  H D L C   H A R D W A R E   D R I V E R

GENERAL DESCRIPTION
  This is the HDLC driver. It provides an interface to the HDLC hardware.

EXTERNALIZED FUNCTIONS
  
  HDLC_DRV_WRITE_FRAMER
    Writes data into framer input buffer.
  
  HDLC_DRV_READ_FRAMER
    Reads data out of framer output buffer.
  
  HDLC_DRV_WRITE_DEFRAMER
    Writes data into deframer input buffer.

  HDLC_DRV_READ_DEFRAMER
    Reads data out of deframer output buffer.

  HDLC_DRV_DEFRAMER_XLATE_DATA_STATE_HW_TO_SW
    Translates data state from hardware to software.
  
  HDLC_DRV_DEFRAMER_XLATE_DATA_STATE_SW_TO_HW
    Translates data state from software to hardware.
  
  Look at the header file for more (inlined) functions.

Copyright (c) 2001, 2002 by Qualcomm Technologies, Inc.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/


/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$PVCSPath: L:/src/asw/MM_DATA/vcs/hdlc_drv.c_v   1.0   11 Oct 2002 09:14:10   ubabbar  $
$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/protocols/linklayer/src/hdlc_drv.c#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $
   
when        who    what, where, why
--------    ---    ----------------------------------------------------------
08/25/10    sch    moving macro HDLC_DRV_ALIGN_SIZE from this file to 
                   hdlc_drv.h so that it can be used in file ps_hdlc_lib.c.
11/10/08    scb    Fixed QTF compilation warnings.
04/20/06    ssh    Added some macros; functions to parse and preset values 
                   for the deframer data state register.
11/15/05    ssh    Cleanup, added more comments
10/27/05    ssh    Using framer/deframer in/out register addresses directly 
10/07/05    ssh    Redesign to achieve cleanliness and optimization
09/20/05    ssh    Added functions to set and get deframer state, and for 
                   hw bug in newer cores (under FEATURE_HDLC_SW_WORKAROUND)
05/18/05    ssh    Fixed lint errors.
10/11/02    usb    Merging file to MM_DATA archive from COMMON
08/09/02    mvl    Added FEATURE_HDLC_HW_ACCEL
05/20/02    vr     Fixed some bugs in hdlc_read_buf and code review changes
12/31/01    sq     Added two new functions: hdlc_deframer_cmd and 
                   hdlc_framer_cmd.
08/20/01    sq     Created.
===========================================================================*/


/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"       /* Customer Specific Features */
#ifdef FEATURE_HDLC_HW_ACCEL
#include "hdlc_drv.h"
#include "miscasm.h"
#include "err.h"
#include "amssassert.h"
#include "ds_Utils_DebugMsg.h"


/*===========================================================================

                LOCAL DEFINITIONS AND DECLARATIONS FOR MODULE

This section contains local definitions for constants, macros, types,
variables and other items needed by this module.
                                
===========================================================================*/
/*---------------------------------------------------------------------------
  Deframer data state field masks - relevant only to newer HDLC core.
  These are needed to report residual bytes' state to software, which can
  use it to continue unframing in the software mode.
---------------------------------------------------------------------------*/
#define     HDLC_DRV_DEFRAMER_RESIDUAL_BYTE_CNT_MASK   0x03000000
#define     HDLC_DRV_DEFRAMER_RESIDUAL_BYTE_0_MASK     0x000000ff
#define     HDLC_DRV_DEFRAMER_RESIDUAL_BYTE_1_MASK     0x0000ff00
#define     HDLC_DRV_DEFRAMER_RESIDUAL_BYTE_2_MASK     0x00ff0000
#define     HDLC_DRV_DEFRAMER_ALL_RESIDUAL_BYTES_MASK                       \
        ( HDLC_DRV_DEFRAMER_RESIDUAL_BYTE_0_MASK |                          \
          HDLC_DRV_DEFRAMER_RESIDUAL_BYTE_1_MASK |                          \
          HDLC_DRV_DEFRAMER_RESIDUAL_BYTE_2_MASK                            \
        )

/*---------------------------------------------------------------------------
  Value to write to error buffer to clear framer/deframer error
---------------------------------------------------------------------------*/
#define     HDLC_DRV_CLR_ERR_CMD            0x1

/*---------------------------------------------------------------------------
  Is the escape flag set in data_state ?
---------------------------------------------------------------------------*/
#define HDLC_DRV_DEFRAMER_LAST_BYTE_WAS_ESC( data_state )                   \
  ( ( data_state & HDLC_DRV_DEFRAMER_DATA_STATE_ESCAPE_FLAG ) == TRUE )

/*---------------------------------------------------------------------------
  Interpretation of the START_FLAG and START_FOUND bits in the deframer
  data state register. START_FLAG bit set means that the the last byte sent 
  to the deframer in the previous RLP packet was 0x7e. START_FOUND bit set 
  means that there was at least one 0x7e somewhere in the previous RLP pkt. 

  +------------+-------------+--------------------------------------------+
  | Start_Flag | Start_Found | Equivalent "unframing" state               |
  +------------+-------------+--------------------------------------------+
  |    0       |     0       | looking for 7e( FINDING_7E )               |
  +------------+-------------+--------------------------------------------+
  |    1       |     1       | Start found and was also the last byte of  |
  |            |             | prev packet ( FINDING_SOP, i.e. skip 7Es   | 
  +------------+-------------+--------------------------------------------+
  |    0       |     1       | Start found in previous packet (process    |
  |            |             | data as part of prev packet ( UNFRAMING )  |
  +------------+-------------+--------------------------------------------+
  |    1       |     0       |  Invalid (Disallowed)                      |
  +------------+-------------+--------------------------------------------+ 
---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
  Is the deframer FINDING_7E ?
---------------------------------------------------------------------------*/
#define HDLC_DRV_DEFRAMER_IS_FINDING_7E( data_state )                       \
   !( data_state & HDLC_DRV_DEFRAMER_DATA_STATE_START_FLAG  ) &&            \
   !( data_state & HDLC_DRV_DEFRAMER_DATA_STATE_START_FOUND )
 
/*---------------------------------------------------------------------------
  Is the deframer FINDING_SOP ?
---------------------------------------------------------------------------*/
#define HDLC_DRV_DEFRAMER_IS_FINDING_SOP( data_state )                      \
   ( data_state & HDLC_DRV_DEFRAMER_DATA_STATE_START_FLAG  ) &&             \
   ( data_state & HDLC_DRV_DEFRAMER_DATA_STATE_START_FOUND )

/*---------------------------------------------------------------------------
  Is the deframer UNFRAMING ?
---------------------------------------------------------------------------*/
#define HDLC_DRV_DEFRAMER_IS_UNFRAMING( data_state )                        \
   !( data_state & HDLC_DRV_DEFRAMER_DATA_STATE_START_FLAG  ) &&            \
    ( data_state & HDLC_DRV_DEFRAMER_DATA_STATE_START_FOUND ) 


/*===========================================================================

                        PUBLIC FUNCTION DECLARATIONS

===========================================================================*/

/*===========================================================================
FUNCTION HDLC_DRV_WRITE_FRAMER                                          

DESCRIPTION
  Writes data into framer input buffer. 
  This function byte-transfers the initial non-aligned bytes (if any),
  dword-transfers the aligned bytes, and finally byte-transfers the trailing 
  non-aligned bytes (if any).
 
  +-------+-------+ -----------------------+-------+-------+ 
  |  (0-3 bytes)  |  ..   |   ..   |  ..   |  (0-3 bytes)  |
  +-------+-------+ -----------------------+-------+-------+ 
  <- unaligned  -><- - -   ALIGNED   - - -><-  unaligned  ->
  
DEPENDENCIES
  None.
  
PARAMETERS
  (IN) buf is the pointer to the buffer to write from. 
  (IN) length is the number of bytes to write from the buffer.

RETURN VALUE
  Number of bytes written to the framer input buffer 

SIDE EFFECTS
  None.
===========================================================================*/
uint16 hdlc_drv_write_framer
( 
  uint8  * buf, 
  uint16   length
)
{
  uint8    unaligned_bytes = 0;
  uint16   write_bytes     = 0;  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  /*-------------------------------------------------------------------------
    Make sure buf is a non-zero address
  -------------------------------------------------------------------------*/
  if( buf == NULL)
  {
    return 0;
  }

  /*-------------------------------------------------------------------------
    Cap the max bytes transferred to the framer buffer size
  -------------------------------------------------------------------------*/
  length = MIN( length, HDLC_DRV_FRAMER_BUF_SIZE );

  /*-------------------------------------------------------------------------
    If length is less than 4 bytes, simply byte transfer them
  -------------------------------------------------------------------------*/
  if(length < HDLC_DRV_ALIGN_SIZE)
  {
    for(unaligned_bytes = 0; unaligned_bytes < length; unaligned_bytes++)
    {
      outp(FRAMER_DATA_IN_WW,*buf);
      buf++;
    }
    
    return length;
  } /* if(length < HDLC_DRV_ALIGN_SIZE) */

  /*-------------------------------------------------------------------------
    Byte-transferring data until we read a 4-byte boundary
  -------------------------------------------------------------------------*/
  unaligned_bytes = (uint8)
    (HDLC_DRV_ALIGN_SIZE - (((uint32)buf) % HDLC_DRV_ALIGN_SIZE));
  write_bytes = length - unaligned_bytes;

  while( unaligned_bytes-- )
  {
    outp(FRAMER_DATA_IN_WW,*buf);
    buf++;
  }

  unaligned_bytes = write_bytes % HDLC_DRV_ALIGN_SIZE;

  /*-------------------------------------------------------------------------
    Burst write data into the framer till we hit the final 4-byte boundary 
    in the input buffer
  -------------------------------------------------------------------------*/
  if(write_bytes >= HDLC_DRV_ALIGN_SIZE) 
  {
    miscasm_write_burst_data((uint32 *) buf,
                             (uint32 *) FRAMER_DATA_IN_WW,
                             (write_bytes >> HDLC_DRV_ALIGN_SIZE_SHFT));
  }

  buf += (write_bytes - unaligned_bytes);

  /*-------------------------------------------------------------------------
    Write the remaining bytes into the framer
  -------------------------------------------------------------------------*/
  while( unaligned_bytes-- )
  {
    outp(FRAMER_DATA_IN_WW,*buf);
    buf++;
  }

  return length;
} /* hdlc_drv_write_framer */


/*===========================================================================
FUNCTION HDLC_DRV_READ_FRAMER                                           

DESCRIPTION
  Reads data out of framer output buffer. 
  The buffer to write to must be 4-byte aligned. The function dword-transfers
  the aligned bytes, and then reads the last dword, processing the contained
  (less than 4) bytes in that dword assuming them to be left-aligned.
  
  + -----------------------+-------+-------+ 
  |  ..   |   ..   |  ..   |  (0-3 bytes)  |
  + -----------------------+-------+-------+ 
  <- - -   ALIGNED   - - -><-  unaligned  ->
  
DEPENDENCIES
  buf must be 4-byte aligned
  
PARAMETERS
  (OUT) buf is the buffer to be read into. 
  (IN) length is the number of bytes to read

RETURN VALUE
  Number of bytes read out of the framer output buffer.

SIDE EFFECTS
  None.
===========================================================================*/
uint16 hdlc_drv_read_framer
( 
  uint8    *buf, 
  uint16 length
)
{
  uint32   tail_bytes      = 0;    /* the last dword read from hardware    */
  uint8    remaining_bytes = 0;    /* number of useful bytes in tail_bytes */
  uint8   *ptr             = NULL; /* temp ptr for bytes in tail_bytes     */ 
  uint8    i               = 0;    /* counter for bytes in tail_bytes      */
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  /*-------------------------------------------------------------------------
    Make sure buf is a non-zero address
  -------------------------------------------------------------------------*/
  if( buf == NULL)
  {
    return 0;
  }

  /*-------------------------------------------------------------------------
    buf must be 4-byte aligned
  -------------------------------------------------------------------------*/
  ASSERT( (((uint32) buf) % HDLC_DRV_ALIGN_SIZE) == 0);

  /*-------------------------------------------------------------------------
    Cap the max bytes to read to the framer buffer size
  -------------------------------------------------------------------------*/
  length = MIN( length, HDLC_DRV_FRAMER_BUF_SIZE );

  /*-------------------------------------------------------------------------
    Burst read the data till less than 4 bytes are remaining to be read
  -------------------------------------------------------------------------*/
  miscasm_read_burst_data( (uint32 *) FRAMER_DATA_OUT_RW,
                          (uint32 *) buf,
                          (length >> HDLC_DRV_ALIGN_SIZE_SHFT));

  /*-------------------------------------------------------------------------
    Read the last dword, and process the contained bytes (less than 4),
    assuming they are left-aligned within the dword read.
  -------------------------------------------------------------------------*/
  remaining_bytes = length % HDLC_DRV_ALIGN_SIZE;
  if(remaining_bytes != 0)
  {
    tail_bytes = inpdw(FRAMER_DATA_OUT_RW);

    /*-----------------------------------------------------------------------
      Write byte-wise starting at offset num into buf, where num is 
      bytes already written ((length / 4) * 4). 
      dwords written = dwds = (length >> HDLC_DRV_ALIGN_SIZE_SHFT)
      bytes written  = num  = (dwds << HDLC_DRV_ALIGN_SIZE_SHFT)
    -----------------------------------------------------------------------*/
    ptr = buf + 
          ((length >> HDLC_DRV_ALIGN_SIZE_SHFT) << HDLC_DRV_ALIGN_SIZE_SHFT);

    while(remaining_bytes--)
    {
      *ptr++  = ((uint8 *) &tail_bytes)[i];
      i++;
    } /* while(remaining_bytes--) */
  } /* if(remaining_bytes != 0) */

  return length;
} /* hdlc_drv_read_framer */


/*===========================================================================
FUNCTION HDLC_DRV_WRITE_DEFRAMER                                          

DESCRIPTION
  Writes data into deframer input buffer. 
  This function byte-transfers the initial non-aligned bytes (if any),
  dword-transfers the aligned bytes, and finally byte-transfers the trailing 
  non-aligned bytes (if any).
 
  +-------+-------+ -----------------------+-------+-------+ 
  |  (0-3 bytes)  |  ..   |   ..   |  ..   |  (0-3 bytes)  |
  +-------+-------+ -----------------------+-------+-------+ 
  <- unaligned  -><- - -   ALIGNED   - - -><-  unaligned  ->
  
DEPENDENCIES
  None.

PARAMETERS
  (IN) buf is the buffer to be written. 
  (IN) length is the number of bytes to write from buf

RETURN VALUE
  Number of bytes written to the deframer input buffer 

SIDE EFFECTS
  None.
===========================================================================*/
uint16 hdlc_drv_write_deframer
( 
  uint8 *buf, 
  uint16 length
)
{
  uint8    unaligned_bytes = 0;
  uint16   write_bytes     = 0;
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  /*-------------------------------------------------------------------------
    Make sure buf is a non-zero address
  -------------------------------------------------------------------------*/
  if( buf == NULL)
  {
    return 0;
  }

  /*-------------------------------------------------------------------------
    Cap the max bytes transferred to the deframer buffer size
  -------------------------------------------------------------------------*/
  length = MIN( length, HDLC_DRV_DEFRAMER_BUF_SIZE );

  /*-------------------------------------------------------------------------
    If length is less than 4 bytes, simply byte transfer them
  -------------------------------------------------------------------------*/
  if(length < HDLC_DRV_ALIGN_SIZE)
  {
    for(unaligned_bytes = 0; unaligned_bytes < length; unaligned_bytes++)
    {
      outp(DEFRAMER_DATA_IN_WW,*buf);
      buf++;
    }
    return length;
  } /* if(length < HDLC_DRV_ALIGN_SIZE) */  

  /*-------------------------------------------------------------------------
    Byte-transferring data until we read a 4-byte boundary
  -------------------------------------------------------------------------*/
  unaligned_bytes = (uint8)
    (HDLC_DRV_ALIGN_SIZE - (((uint32)buf) % HDLC_DRV_ALIGN_SIZE));

  write_bytes = length - unaligned_bytes;

  while( unaligned_bytes-- )
  {
    outp(DEFRAMER_DATA_IN_WW,*buf);
    buf++;
  }

  unaligned_bytes = write_bytes % HDLC_DRV_ALIGN_SIZE;

  /*-------------------------------------------------------------------------
    Burst write data into the deframer till we hit the final 4-byte boundary
    in the input buffer
  -------------------------------------------------------------------------*/
  if(write_bytes >= HDLC_DRV_ALIGN_SIZE) 
  {
    miscasm_write_burst_data((uint32 *) buf,
                              (uint32 *) DEFRAMER_DATA_IN_WW,
                             (write_bytes >> HDLC_DRV_ALIGN_SIZE_SHFT));
  }
  
  buf +=  (write_bytes - unaligned_bytes);

  /*-------------------------------------------------------------------------
    Write the residual bytes into the deframer
  -------------------------------------------------------------------------*/
  while( unaligned_bytes-- )
  {
    outp(DEFRAMER_DATA_IN_WW,*buf);
    buf++;
  }
  
  return length;
} /* hdlc_drv_write_deframer */
  

/*===========================================================================
FUNCTION HDLC_DRV_READ_DEFRAMER                                           

DESCRIPTION
  Reads data out of deframer output buffer.
  The buffer to write to must be 4-byte aligned. The function dword-transfers
  the aligned bytes, and then reads the last dword, processing the contained
  (less than 4) bytes in that dword assuming them to be left-aligned.
  
  + -----------------------+-------+-------+ 
  |  ..   |   ..   |  ..   |  (0-3 bytes)  |
  + -----------------------+-------+-------+ 
  <- - -   ALIGNED   - - -><-  unaligned  ->

DEPENDENCIES
  buf must be 4-byte aligned

PARAMETERS
  (OUT) buf is the buffer to be read into. 
  (IN) length is the number of bytes to read

RETURN VALUE
  Number of bytes read out of the deframer output buffer.

SIDE EFFECTS
  None.
===========================================================================*/
uint16 hdlc_drv_read_deframer
( 
  uint8  * buf, 
  uint16   length
)
{
  uint32   tail_bytes      = 0;    /* the last dword read from hardware    */
  uint8    remaining_bytes = 0;    /* number of useful bytes in tail_bytes */
  uint8   *ptr             = NULL; /* temp ptr for bytes in tail_bytes     */ 
  uint8    i               = 0;    /* counter for bytes in tail_bytes      */
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  /*-------------------------------------------------------------------------
    Make sure buf is a non-zero address
  -------------------------------------------------------------------------*/
  if( buf == NULL)
  {
    return 0;
  }
  
  /*-------------------------------------------------------------------------
    buf must be 4-byte aligned
  -------------------------------------------------------------------------*/
  ASSERT( (((uint32) buf) % HDLC_DRV_ALIGN_SIZE) == 0);
  
  /*-------------------------------------------------------------------------
    Cap the max bytes to read to the framer buffer size
  -------------------------------------------------------------------------*/
  length = MIN( length, HDLC_DRV_DEFRAMER_BUF_SIZE );

  /*-------------------------------------------------------------------------
    Burst read the data till less than 4 bytes are remaining to be read
  -------------------------------------------------------------------------*/
  miscasm_read_burst_data( (uint32 *) DEFRAMER_DATA_OUT_RW,
                          (uint32 *) buf,
                          length >> HDLC_DRV_ALIGN_SIZE_SHFT);

  /*-------------------------------------------------------------------------
    Read the last dword, and process the contained bytes (less than 4),
    assuming they are left-aligned within the dword read.
  -------------------------------------------------------------------------*/
  remaining_bytes = length % HDLC_DRV_ALIGN_SIZE;
  if(remaining_bytes != 0)
  {                                                                         
    tail_bytes = inpdw(DEFRAMER_DATA_OUT_RW);

    /*-----------------------------------------------------------------------
      Write byte-wise starting at offset num into buf, where num is 
      bytes already written ((length / 4) * 4). 
      dwords written = dwds = (length >> HDLC_DRV_ALIGN_SIZE_SHFT)
      bytes written  = num  = (dwds << HDLC_DRV_ALIGN_SIZE_SHFT)
    -----------------------------------------------------------------------*/
    ptr = buf + 
          ((length >> HDLC_DRV_ALIGN_SIZE_SHFT) << HDLC_DRV_ALIGN_SIZE_SHFT);

    while(remaining_bytes--)
    {                                                                         
      *ptr++  = ((uint8 *) &tail_bytes)[i];
      i++;
    } /* while(remaining_bytes--) */
  } /* if(remaining_bytes != 0) */
                                                                            
  return length;
} /* hdlc_drv_read_deframer */


/*===========================================================================
FUNCTION  HDLC_DRV_DEFRAMER_XLATE_DATA_STATE_HW_TO_SW

DESCRIPTION
  Interprets passed 32-bit data_state to extract the information needed
  by the software variant of the HDLC deframer (maintained in
  ps_hdlc_lib.c)
  
DEPENDENCIES
  None.
  
PARAMETERS
  (IN) data_state: The 32-bit value to interpret 
  (OUT) escape: If escape flag is set, this is set to TRUE, else FALSE.
  (OUT) unframe_state: The Start_Flag and Start_Found bits are examined to
        provide an output value for this.
  (OUT) num_residual_bytes: The number of residual bytes in data_state (0-3)
  (OUT) residual_bytes_array: The residual bytes themselves (0-3)
  (IN)  residual_bytes_array_len: Size of residual_bytes_array passed in,
        should be atleast MAX_RESIDUAL_BYTES.

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
int hdlc_drv_deframer_xlate_data_state_hw_to_sw
(
  uint32     data_state,
  boolean  * escape,
  uint8    * unframe_state, 
  uint8    * num_residual_bytes,
  uint8    * residual_bytes_array,
  uint8      residual_bytes_array_len
)
{
  uint8 * ptr = NULL;
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  
  /*-------------------------------------------------------------------------
    Figure out the escape bit in data_state
  -------------------------------------------------------------------------*/
  *escape = 
    ( HDLC_DRV_DEFRAMER_LAST_BYTE_WAS_ESC( data_state ) ? TRUE : FALSE );

  /*-------------------------------------------------------------------------
    Figure out the state hardware deframer is in
  -------------------------------------------------------------------------*/
  if( HDLC_DRV_DEFRAMER_IS_FINDING_7E( data_state ) )
  {
    *unframe_state = (uint8)FINDING_7E;
  }
  else if( HDLC_DRV_DEFRAMER_IS_FINDING_SOP( data_state ) )
  {
    *unframe_state = (uint8)FINDING_SOP;
  }
  else if( HDLC_DRV_DEFRAMER_IS_UNFRAMING( data_state ) )
  {
    *unframe_state = (uint8)UNFRAMING;
  }
  else
  {
    LOG_MSG_ERROR_1("data_state (0x%x) passed in to parse_data_state likely "
                    "malformed", data_state );
    return -1;
  }
  
  /*-------------------------------------------------------------------------
    Figure out the residual byte count
  -------------------------------------------------------------------------*/
  *num_residual_bytes = (uint8)
    ( (data_state & HDLC_DRV_DEFRAMER_RESIDUAL_BYTE_CNT_MASK) >> 24 );

  /*-------------------------------------------------------------------------
    Validate that sufficient memory has been passed to hold
    MAX_RESIDUAL_BYTES
  -------------------------------------------------------------------------*/
  if( residual_bytes_array_len < MAX_RESIDUAL_BYTES )
  {
    LOG_MSG_ERROR_1("Insufficient memory (%d bytes) passed in to hold residual "
                    "bytes ", num_residual_bytes);
    return -1;
  }

  /*-------------------------------------------------------------------------
    Write the residual bytes to residual_bytes_array
  -------------------------------------------------------------------------*/
  ptr = residual_bytes_array;
  *ptr++ =  (uint8)
    ( ( data_state & HDLC_DRV_DEFRAMER_RESIDUAL_BYTE_0_MASK ) );
  *ptr++ = (uint8)
    ( ( data_state & HDLC_DRV_DEFRAMER_RESIDUAL_BYTE_1_MASK ) >> 8);
  *ptr = (uint8)
    ( ( data_state & HDLC_DRV_DEFRAMER_RESIDUAL_BYTE_2_MASK ) >> 16); 

  return 0;
} /* hdlc_drv_deframer_xlate_data_state_hw_to_sw() */


/*===========================================================================
FUNCTION HDLC_DRV_DEFRAMER_XLATE_DATA_STATE_SW_TO_HW

DESCRIPTION
  Sets up the 32-bit data_state output paramters according to the passed 
  escape and unframe_state info. This function will likely be followed by a 
  hdlc_drv_set_deframer_state() call by the driver client to set the 
  deframer data_state register.
  The data_state variable passed in is cleared initially, and then 
  appropriate bits set according to the input.
  
  Interpretation of the START_FLAG and START_FOUND bits in the deframer
  data state register. START_FLAG bit set means that the the last byte sent 
  to the deframer in the previous RLP packet was 0x7e. START_FOUND bit set 
  means that there was at least one 0x7e somewhere in the previous RLP pkt. 

  +------------+-------------+--------------------------------------------+
  | Start_Flag | Start_Found | Equivalent "unframing" state               |
  +------------+-------------+--------------------------------------------+
  |    0       |     0       | looking for 7e( FINDING_7E )               |
  +------------+-------------+--------------------------------------------+
  |    1       |     1       | Start found and was also the last byte of  |
  |            |             | prev packet ( FINDING_SOP, i.e. skip 7Es ) | 
  +------------+-------------+--------------------------------------------+
  |    0       |     1       | Start found in previous packet (process    |
  |            |             | data as part of prev packet ( UNFRAMING )  |
  +------------+-------------+--------------------------------------------+
  |    1       |     0       |  Invalid (Disallowed)                      |
  +------------+-------------+--------------------------------------------+   
  
DEPENDENCIES
  None.
  
PARAMETERS
  (IN) escape: If this is TRUE, the escape flag in data_state is set.
  (IN) unframe_state: The unframing state to use to preset the Start_Flag 
       and Start_Found bits in data_state
  (OUT) data_state: The mod'ed data_state

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
int hdlc_drv_deframer_xlate_data_state_sw_to_hw
( 
  boolean   escape,
  uint8     unframe_state,
  uint32  * data_state
)
{
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  
  *data_state = 0;
  if( escape == TRUE )
  {
    *data_state |= HDLC_DRV_DEFRAMER_DATA_STATE_ESCAPE_FLAG;
  }

  switch( unframe_state )
  {
    case FINDING_7E:
      *data_state &= ~HDLC_DRV_DEFRAMER_DATA_STATE_START_FLAG;
      *data_state &= ~HDLC_DRV_DEFRAMER_DATA_STATE_START_FOUND;
      break;

    case FINDING_SOP:
      *data_state |= HDLC_DRV_DEFRAMER_DATA_STATE_START_FLAG;
      *data_state |= HDLC_DRV_DEFRAMER_DATA_STATE_START_FOUND;
      break;
      
    case UNFRAMING:
      *data_state &= ~HDLC_DRV_DEFRAMER_DATA_STATE_START_FLAG;
      *data_state |= HDLC_DRV_DEFRAMER_DATA_STATE_START_FOUND;
      break;
      
    default:
      LOG_MSG_ERROR_1("Invalid unframe_state 0x%x passed to preset_data_state",
                      unframe_state);
      return -1;
  } /* switch( unframe_state ) */

  return 0;
} /* hdlc_drv_deframer_xlate_data_state_sw_to_hw() */
#endif /* FEATURE_HDLC_HW_ACCEL */
