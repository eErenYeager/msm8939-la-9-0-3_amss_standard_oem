#ifndef PS_IPDC_HB
#define PS_IPDC_HB
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

           P S _ I P D C _ H B . H

GENERAL DESCRIPTION
  This file contains all the Hash Bucket utility functions to
  support IPDC Compressor Engine operation.

  
 
  Copyright (c) 2013 by QUALCOMM Technologies Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary.  
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/



/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

#include "comdef.h"
#include "ps_ipdci.h"



/*===========================================================================

                      PUBLIC FUNCTION DECLARATIONS

===========================================================================*/

/*===========================================================================
FUNCTION ps_ipdc_hb_alloc()

DESCRIPTION
  Allocates the Hash Bucket Memory to be used by the IPDC Instance's 
  Compressor Engine.  The Hash Bucket memory will be zeroed out.

DEPENDENCIES
  IPDC Instance must be valid.

RETURN VALUE
  Pointer to the allocated Hash Bucket memory. 

SIDE EFFECTS
  Allocates memory for HashBuckets resource
===========================================================================*/
HashBuckets * ps_ipdc_hb_alloc
(
 const ipdc_comp_config_options_s_type * const comp_channel_params
);

/*===========================================================================
FUNCTION ps_ipdc_hb_init()

DESCRIPTION
  Initializes the Hash Bucket memory for the IPDC Instance based on the 
  current contents of the Compressor Engine's Compression Mememory.

DEPENDENCIES
  IPDC Instance must be valid and the Compressor Engine's Hash Bucket 
  memory must have been allocated.

RETURN VALUE
  None

SIDE EFFECTS
  Initializes the Hash Bucket memory.
===========================================================================*/
void ps_ipdc_hb_init(ipdc_instance_s_type * instp);

/*===========================================================================
FUNCTION ps_ipdc_hb_free()

DESCRIPTION
  Frees the IPDC Instance Compressor Engine's version 2 Hash Bucket Memory. 

DEPENDENCIES
  IPDC Instance must be valid.

RETURN VALUE
  None

SIDE EFFECTS
  Frees the Hash Bucket Memory. 
===========================================================================*/
void ps_ipdc_hb_free(HashBuckets * hb_ptr);

/*===========================================================================
FUNCTION ps_ipdc_hb_add_hkeys()

DESCRIPTION
  This function adds the requested number of Hash Key entries into the
  Compressor Engine's Hash Bucket for strings of 5 symbols that
  reside in the Compressor Engine's ExtCompMem for a particular IPDC
  Instance.  The 'uint16 cmp_mem_idx' input parameter provides the location 
  within ExtCompMem where the first string of 5 symbols begins.

DEPENDENCIES
  IPDC Instance must be valid and Compressor Engine's Hash Bucket
  must have been initialized.

RETURN VALUE
  None

SIDE EFFECTS
  Adds the requested Hash Key entries into the IPDC Instance's V2
  Compressor Engine's Hash Bucket.
===========================================================================*/
void ps_ipdc_hb_add_hkeys
(
  ipdc_comp_ctl_blk_s_type    *comp_ptr,
  uint16                       cmp_mem_idx,
  uint16                       num_hkeys_to_add
);

/*===========================================================================
FUNCTION ps_ipdc_hb_shrink_hb()

DESCRIPTION
  Shrinks the IPDC Compressor Engine's Hash Bucket by the specified amount.

DEPENDENCIES
  IPDC Instance must be valid and Compressor Engine's Hash Bucket must have
  been initialized.

RETURN VALUE
  None

SIDE EFFECTS
  Shrinks the HashBucket
===========================================================================*/
void ps_ipdc_hb_shrink_hb
(
  ipdc_comp_ctl_blk_s_type    *comp_ptr,
  uint16                       cmp_mem_start_idx_prior_to_shrink,
  uint16                       cmp_mem_end_idx_prior_to_shrink,
  uint16                       cmp_mem_memmove_size
);

#ifdef FEATURE_IPDC_OFFTARGET
/*===========================================================================
FUNCTION ps_ipdc_hb_dump_hkeys_for_hval()

DESCRIPTION
  This utility function logs the contents of the hash key chain for the
  specified Hash Value.

DEPENDENCIES
  IPDC Instance must be valid and Compressor Engine's Hash Bucket must have
  been initialized.

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
void ps_ipdc_hb_dump_hkeys_for_hval(HashBuckets *hb, ExtCompMem *ecm, uint16 hash_val);
#endif  // #ifdef FEATURE_IPDC_OFFTARGET


#endif  // #ifndef PS_IPDC_HB
