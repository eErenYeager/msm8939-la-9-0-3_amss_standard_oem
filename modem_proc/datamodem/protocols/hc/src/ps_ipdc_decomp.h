#ifndef PS_IPDC_DECOMP_H
#define PS_IPDC_DECOMP_H

/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                    P S _ I P D C _ DEC O M P . H

GENERAL DESCRIPTION
  This file contains all the data structures, variables, enums and
  function prototypes needed to support LempelZiv 77 decompression.
 
  LZ77 � is a sliding window based compression algorithm. 
 
  Copyright (c) 2013 by QUALCOMM Technologies Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary.  
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/



/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "comdef.h"
#include "ps_ipdci.h"

/*===========================================================================

                          CONSTANT DECLARATIONS

===========================================================================*/



/*===========================================================================

                      PUBLIC FUNCTION DECLARATIONS

===========================================================================*/
/*===========================================================================
FUNCTION ps_ipdc_decompress_packet()

DESCRIPTION 
  This function invokes the decompression algorithm on the packet.
  It calls the LZ77Decompress() and updates the 'origDsmPtrPtr' . 
  Functions also updates the IPDC stats and metrics 

DEPENDENCIES
  A valid IPDC handle must be passed.

RETURN VALUE
   0   if successful.
  -1   in case of error 

SIDE EFFECTS 
  'origDsmPtrPtr' will be updated with the decompressed packet

===========================================================================*/
int ps_ipdc_decompress_packet
( 
  ipdc_instance_s_type  *instp, 
  dsm_item_type        **pkt
 );

#endif  // PS_IPDC_DECOMP_H
