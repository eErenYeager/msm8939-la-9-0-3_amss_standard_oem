#ifndef PS_IPDCI_H
#define PS_IPDCI_H

/*===========================================================================

                          P S _ I P D C I . H

GENERAL DESCRIPTION
  This file provides internal IP Data Compression (IPDC) Library's data and
  function definitions.

EXTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None

Copyright (c) 2013 QUALCOMM Technologies Incorporated. 
All Rights Reserved.
Qualcomm Confidential and Proprietary
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/data.mpss/1.0/protocols/hc/src/ps_ipdci.h#1
  $DateTime: 2013/01/10 21:41:01
  $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
01/10/13   pc      Initial version

===========================================================================*/


/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "ps_ipdc.h"

#if !defined (FEATURE_IPDC_OFFTARGET) && !defined (BOA_APPS)  // UE
#include "ps_system_heap.h"
#include "ps_in.h"
#include "ps_comp_logging_helper.h"
#include <stringl/stringl.h>  // memsmove() & memscpy()
#else
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#if !defined (TEST_FRAMEWORK)
#include "netInit.h"
#endif // !TEST_FRAMEWORK 
#endif



/*===========================================================================

                              CONSTANTS

===========================================================================*/

#ifdef FEATURE_IPDC_OFFTARGET
#define IPDC_INLINE _inline
#else
#define IPDC_INLINE inline
#endif

#if defined(BOA_APPS) || defined(FEATURE_IPDC_OFFTARGET)
#define FEATURE_IPDC_STATS
#endif


#ifdef FEATURE_IPDC_OFFTARGET
// IPDC Compressor Engine Debug Flags:
//#define DEBUG_HB_HASH_KEY_MGMT
#endif

#if !defined (FEATURE_IPDC_OFFTARGET) && !defined (BOA_APPS)  // UE Target
// Feature Flags for Q6 Optimizations
//#define Q6_OPTIMIZED_HKEY_COMPUTATION
#define Q6_OPTIMIZED_SEARCH
#endif

#define IPDC_HDR_SIZE_IN_BYTES                 2   // Includes IPDC Header (1 Byte) + Number of Matches (1 Byte)
#define IPDC_PTR_META_DATA_BLK_SIZE_IN_BYTES   4

#define IPDC_SUPPORTS_NO_MATCHES_PKT_TYPE


/*---------------------------------------------------------------------------
  Definitions of Masks for the IPDC Header's Fields
     - Packet Type: upper 2 bits in first byte of IPDC Header
     - Checksum:    lower 6 bits in first byte of IPDC Header
---------------------------------------------------------------------------*/
#define PS_IPDC_HDR_PKT_TYPE_MASK                    0xc0
#define PS_IPDC_HDR_CHECKSUM_MASK                    0x3f


/*---------------------------------------------------------------------------
  Definitions for IP Data Compression (IPDC) packet types that are 
  set in the Compressed Packet's IPDC Header (upper 2 bits)
---------------------------------------------------------------------------*/
#define PS_IPDC_ENTIRE_PKT_COMPRESSED_PKT_TYPE       0xc0
#define PS_IPDC_HEADER_ONLY_COMPRESSED_PKT_TYPE      0x80
#define PS_IPDC_HEADER_NO_MATCHES_PKT_TYPE           0x40
#define PS_IPDC_NOTHING_COMPRESSED_PKT_TYPE          0x00




/*===========================================================================

                              DATA TYPES

===========================================================================*/

typedef enum
{
  IPDCI_IPV4_PKT,
  IPDCI_IPV4_TCP_PKT,
  IPDCI_IPV4_UDP_PKT,
  IPDCI_IPV6_PKT,
  IPDCI_IPV6_TCP_PKT,
  IPDCI_IPV6_UDP_PKT,
  IPDCI_UNKNOWN_PKT
} ipdci_pkt_type;

typedef enum
{
  IPDC_HDR_NOTHING_COMPRESSED_PKT_TYPE,
  IPDC_HDR_NO_MATCHES_PKT_TYPE,
  IPDC_HDR_COMPRESSED_HDR_PKT_TYPE,
  IPDC_HDR_COMPRESSED_HDR_PAYLOAD_PKT_TYPE
} ipdci_ipdc_hdr_pkt_type;

typedef uint32 ipdc_ipv6_ipv4_addr_type[4]; 


typedef struct
{
  ipdci_pkt_type                     pkt_type;
  uint16                             pkt_size;
  uint8                              ip_header_length;
  ipdc_ipv6_ipv4_addr_type           src_addr;
  uint16                             src_port;
  ipdc_ipv6_ipv4_addr_type           dest_addr;
  uint16                             dest_port;
}ipdc_pkt_info_type;


/*---------------------------------------------------------------------------
  IPDC Compressor Metrics data structure
 
   This information is collected for a Single Packet 
---------------------------------------------------------------------------*/

typedef struct 
{
  /* Packet Type Info */
  ipdci_pkt_type            type;
  ipdci_ipdc_hdr_pkt_type ipdc_hdr;

  /* Flow Id */
  ipdc_ipv6_ipv4_addr_type  src_addr;                 // If IPv4 packet, address resides in src_addr[0] 
  uint16                    src_port;
  ipdc_ipv6_ipv4_addr_type  dst_addr;                 // If IPv4 packet, address resides in dst_addr[0] 
  uint16                    dst_port;

  /* Metrics */
  uint16                    orig_pkt_len;             /* NOT REPORTED TO QXDM! Total nr of bytes in the original packet */

  uint16                    orig_dtc_len;             /* NOT REPORTED TO QXDM! Needed for Compression Gain computation! Total nr of bytes in the original packet that were treated as data to be compressed (either header + payload or header only */
  uint16                    orig_hdr_len;             /* Total nr of bytes in the original packet that are considered to be header */
  uint16                    orig_payload_len;         /* Total nr of bytes in the original packet that are considered to be payload */

  uint16                    comp_pkt_len;             /* NOT REPORTED TO QXDM! Total nr of bytes that were Tx'd in a compressed Pkt */
  uint16                    comp_dtr_len;             /* NOT REPORTED TO QXDM! Needed for Compression Gain computation! Total nr of header bytes or header + payload bytes that were Tx'd in a compressed Pkt */
  uint16                    comp_pkt_hdr_len;         /* Total nr of header bytes in a compressed Pkt (rough estimation) */
  uint16                    comp_pkt_payload_len;     /* Total nr of payload bytes in a compressed Pkt (rough estimation) */

  uint16                    uncomp_packet_len;        /* Total nr of bytes that were Tx'd in an uncompressed Pkt.   */

#ifdef FEATURE_IPDC_OFFTARGET
  uint16                    found_matches;
#endif  // FEATURE_IPDC_OFFTARGET

  uint16                    hash_key_searches;
  uint16                    hash_collisions;
  uint8                     shrink_performed;

} ipdc_comp_metrics_t;


/*---------------------------------------------------------------------------
  IPDC Compressor Statistics data structure
 
    This information is collected for Multiple Packets / Multiple IP Flows 
---------------------------------------------------------------------------*/

#define IPDC_MAX_COMPRESSOR_FLOWS  16

typedef enum
{
  IPDC_STATE_TESTING_FULL_PACKET_COMPRESSION = 0,
  IPDC_STATE_COMPRESS_FULL_PACKET,
  IPDC_STATE_COMPRESS_HEADER_ONLY
} flow_compression_states_e_type;

/**
 * Flow specific statistics for the compressor.
 */
typedef struct 
{
  /* Flow Identifier Info */
  boolean flow_in_use;
  uint8   ip_version;          // IP Version in use for this flow.  4 - IPv4, 6 � IPv6 (all other values are reserved)
   
  ipdci_pkt_type             flow_type;  // Distinguishes the flow based on the packet type used to initialize the flow

  ipdc_ipv6_ipv4_addr_type   src_addr;  // If IPv4 flow, address resides in src_addr[0] 
  uint16                     src_port;     
  ipdc_ipv6_ipv4_addr_type   dst_addr;  // If IPv4 flow, address resides in dst_addr[0]    
  uint16                     dst_port;

  flow_compression_states_e_type  flow_state;  // Full Pkt Comression, Hdr Only Compression, or Testing Full Pkt Comression
  uint16  filtered_comp_gain;  // Filtered compression gain % for this flow

  uint32 num_pkts_to_stay_in_cur_mode;         // Number of packets that need to be processed before a state change can be attempted
  uint16 num_drops_to_hdr_only;                // Number of times the flow has "fallen" to Header Only compression

  uint16 initial_num_pkts_to_not_test;         // This value is computed at init, and used for backoff computation (this variable is for MIPS savings)
  uint16 max_drops_to_hdr_only;                // This value is computed at init, and used for backoff computation (this variable is for MIPS savings)

  uint64  last_packet_processed_num;           // NEVER RESET. Packet number of the last packet compressed.  Used for LRU detection
  
#ifdef FEATURE_IPDC_OFFTARGET

  // Stats for this flow's packets
  uint64 total_orig_pkts_recvd_for_flow;         /* Total Nr of packets received for this flow.   */
  uint64 total_comp_hdr_payload_pkts_sent;       /* Total Nr of compressed header and payload packets sent for this (compressible) flow.   */
  uint64 total_comp_hdr_only_pkts_sent;          /* Total Nr of compressed header only packets sent for this (compressible) flow.   */
  uint64 total_comp_no_matches_pkts_sent;        /* Total Nr of compressed packets with no matches sent for this (compressible) flow.   */

  /* This Flow�s Original Packet Stats */
  uint64 orig_pkts_hdr_bytes;       /* Total nr of bytes within the original packets that are considered to be header */
  uint64 orig_pkts_payload_bytes;   /* Total nr of bytes within the original packet that are considered to be payload */
  uint64 orig_pkts_dtc_size;        /* NOT REPORTED TO QXDM! Needed for Compression Gain computation! Total nr of bytes within the original packet that are considered to be data-to-compress */

  /* This Flow�s Compressed Packet Stats for Pkt Types 11 & 10 & 01 : stats for Original Packets which were compressed      */
  uint64 comp_pkts_header_bytes;    /* Total nr of header bytes Tx�d in compressed Pkts (rough estimation) */
  uint64 comp_pkts_payload_bytes;   /* Total nr of payload bytes Tx�d in compressed Pkts (rough estimation) */
  uint64 comp_pkts_dtr_size;        /* NOT REPORTED TO QXDM! Needed for Compression Gain computation! Total nr of compressed header bytes or compressed header + payload bytes that were Tx'd in a compressed Pkt */

#else  // UE Target : All Stats counters are reset after being reported unless otherwise stated

  // Stats for this flow's packets
  uint16 total_orig_pkts_recvd_for_flow;         /* Total Nr of packets received for this flow.   */
  uint16 total_comp_hdr_payload_pkts_sent;       /* Total Nr of compressed header and payload packets sent for this (compressible) flow.   */
  uint16 total_comp_hdr_only_pkts_sent;          /* Total Nr of compressed header only packets sent for this (compressible) flow.   */
  uint16 total_comp_no_matches_pkts_sent;        /* Total Nr of compressed packets with no matches sent for this (compressible) flow.   */

  /* This Flow�s Original Packets Stats */
  uint32 orig_pkts_hdr_bytes;       /* Total nr of bytes within the original packets that are considered to be header */
  uint32 orig_pkts_payload_bytes;   /* Total nr of bytes within the original packet that are considered to be payload */
  uint32 orig_pkts_dtc_size;        /* NOT REPORTED TO QXDM! Needed for Compression Gain computation! Total nr of bytes within the original packet that are considered to be data-to-compress */

  /* This Flow�s Compressed Packets Stats for Pkt Types 11 & 10 & 01 : stats for Original Packets which were compressed      */
  uint32 comp_pkts_header_bytes;    /* Total nr of header bytes Tx�d in compressed Pkts (rough estimation) */
  uint32 comp_pkts_payload_bytes;   /* Total nr of payload bytes Tx�d in compressed Pkts (rough estimation) */
  uint32 comp_pkts_dtr_size;        /* NOT REPORTED TO QXDM! Needed for Compression Gain computation! Total nr of header bytes or header + payload bytes that were Tx'd in a compressed Pkt */

#endif

} ipdc_comp_per_flow_stats_t;


typedef struct 
{

#ifdef FEATURE_IPDC_OFFTARGET
  /* General Stats */
  uint64 global_pkt_number;           /* NEVER RESET. keeps track of the total number of packets that have gone thru the Compressor Engine */

  /* The outgoing IPDC Header tags these types of packets */
  uint64 compressed_hdr_payload_pkts; /* Nr of packets sent with header and payload compressed.   */
  uint64 compressed_hdr_only_pkts;    /* Nr of packets sent with only header compressed.   */
  uint64 compressed_no_matches_pkts;  /* Nr of packets sent compressed with no matches.   */
  uint64 uncompressed_pkts;           /* Nr of packets sent uncompressed. */
  uint64 compressed_small_pkts;       /* Nr of packets whose size is less than the configured header length (packets that bypass the incompressible flows state machine as there is no payload compression gain that can be evaluated) for all flows   */
  uint64 compressed_no_matches_small_pkts;       /* Nr of packets whose size is less than the configured header length (packets that bypass the incompressible flows state machine as there is no payload compression gain that can be evaluated) for all flows   */
  uint64 uncompressed_small_pkts;     /* Nr of packets whose size is less than the configured header length (packets that bypass the incompressible flows state machine as there is no payload compression gain that can be evaluated) for all flows   */
  uint64 comp_failed_pkts;            /* Nr of compression failures.      */

  uint64 all_orig_pkts_dtc_size;        /* Needed for Compression Gain computation! Total nr of bytes within all original packets that are considered to be data-to-compress */
  uint64 all_comp_uncomp_pkts_dtr_size; /* Needed for Compression Gain computation! Total nr of compressible bytes that were either TX'd as compressed bytes or TX'd as uncompressed bytes in all IPDC Pkts */
  // Compression Gain is calculated as follows:
  /*
     comp_gain = (((all_orig_pkts_dtc_size - all_comp_uncomp_pkts_dtr_size) * 100)/ all_orig_pkts_dtc_size);
  */

  uint64 small_pkts_orig_header_bytes;       /* Nr of bytes in original small packets (size is less than or equal to the configured header length (packets that bypass the incompressible flows state machine as there is no payload compression gain that can be evaluated) for all flows   */
  uint64 small_pkts_comp_header_bytes;       /* Nr of bytes in compressed small packets (size is less than or equal to the configured header length (packets that bypass the incompressible flows state machine as there is no payload compression gain that can be evaluated) for all flows   */

  uint64 unknown_pkts_incoming_header_bytes;    /* Nr of header bytes in incoming unknown packets on which compression is attempted   */
  uint64 unknown_pkts_incoming_payload_bytes;   /* Nr of payload bytes in incoming unknown packets on which compression is attempted   */
  uint64 unknown_pkts_compressed_header_bytes;  /* Nr of header bytes in outgoing compressed unknown packets  */
  uint64 unknown_pkts_compressed_payload_bytes; /* Nr of payload bytes in outgoing compressed unknown packets   */


  // These stats are used to calculate the overall compression gain
  uint64 orig_pkts_hdr_bytes;        /* Total nr of bytes within all original packets that are considered to be header bytes */
  uint64 orig_pkts_payload_bytes;    /* Total nr of bytes within all original packets that are considered to be payload bytes */
  uint64 uncompressed_incoming_pkts_bytes;    /* Total nr of bytes received in all incoming uncompressed packets */
  uint64 uncompressed_outgoing_pkts_bytes;    /* Total nr of bytes within TX'd in all uncompressed packets */

  //-------------------------------------------------------------------------
  // Stats that are collected only when the configuration is to Compress Headers only (i.e., bypasses incompressible flows state machine)
  //-------------------------------------------------------------------------
  uint64 comp_pkts_hdr_bytes;        /* Total nr of bytes within all compressed packets that are considered to be header bytes (rough estimate)  when Header only compression is configured */
  uint64 comp_pkts_payload_bytes;    /* Total nr of bytes within all compressed packets that are considered to be payload bytes (rough estimate) when Header only compression is configured */
  // end of Stats that are collected only when the configuration is to Compress Headers only (i.e., bypasses incompressible flows state machine)

  //-------------------------------------------------------------------------
  // Compressor Engine Performance Stats - only collected off-target! 
  //-------------------------------------------------------------------------
  uint64 found_matches;
  uint64 hash_key_searches;
  uint64 hash_collisions;
  /* End of Compressor Engine Performance Stats - only collected off-target! */

#else  // UE Target  : All Stats counters are reset after being reported unless otherwise stated! 

  uint64 global_pkt_number;            /* NEVER RESET. keeps track of the total numver of packets that have gone thru the Compressor Engine.*/

  /* The outgoing IPDC Header tags these types of packets */
  uint16 compressed_hdr_payload_pkts; /* Nr of packets sent with header and payload compressed.   */
  uint16 compressed_hdr_only_pkts;    /* Nr of packets sent with only header compressed.   */
  uint16 compressed_no_matches_pkts;  /* Nr of packets sent compressed with no matches.   */
  uint16 uncompressed_pkts;           /* Nr of packets sent uncompressed. */
  uint16 compressed_small_pkts;       /* Nr of packets whose size is less than the configured header length (packets that bypass the incompressible flows state machine as there is no payload compression gain that can be evaluated) for all flows   */
  uint16 compressed_no_matches_small_pkts;       /* Nr of packets whose size is less than the configured header length (packets that bypass the incompressible flows state machine as there is no payload compression gain that can be evaluated) for all flows   */
  uint16 uncompressed_small_pkts;     /* Nr of packets whose size is less than the configured header length (packets that bypass the incompressible flows state machine as there is no payload compression gain that can be evaluated) for all flows   */
  uint16 comp_failed_pkts;            /* Nr of compression failures.      */

  uint32 all_orig_pkts_dtc_size;        /* Needed for Compression Gain computation! Total nr of bytes within all original packets that are considered to be data-to-compress */
  uint32 all_comp_uncomp_pkts_dtr_size; /* Needed for Compression Gain computation! Total nr of compressible bytes that were either TX'd as compressed bytes or TX'd as uncompressed bytes in all IPDC Pkts */
  // Compression Gain is calculated as follows:
  /*
     comp_gain = (((all_orig_pkts_dtc_size - all_comp_uncomp_pkts_dtr_size) * 100)/ all_orig_pkts_dtc_size);
  */

  // Small Packets are defined to be any packet whose size is less than or equal to the configured header length.
  //   Small packets bypass the incompressible flows state machine as there is no payload compression gain that can be evaluated
  uint32 small_pkts_orig_header_bytes;       /* Nr of Header bytes in original small packets for all flows   */
  uint32 small_pkts_comp_header_bytes;       /* Nr of bytes in compressed small packets (size is less than or equal to the configured header length (packets that bypass the incompressible flows state machine as there is no payload compression gain that can be evaluated) for all flows   */

  uint32 unknown_pkts_incoming_header_bytes;  /* Nr of header bytes in incoming unknown packets on which compression is attempted   */
  uint32 unknown_pkts_incoming_payload_bytes; /* Nr of payload bytes in incoming unknown packets on which compression is attempted   */
  uint32 unknown_pkts_compressed_header_bytes;  /* Nr of header bytes in outgoing compressed unknown packets   */
  uint32 unknown_pkts_compressed_payload_bytes; /* Nr of payload bytes in outgoing compressed unknown packets  */

  // These stats are used to calculate the overall compression gain
  uint32 orig_pkts_hdr_bytes;        /* NOT REPORTED TO QXDM! Total nr of bytes within all original packets that are considered to be header bytes */
  uint32 orig_pkts_payload_bytes;    /* NOT REPORTED TO QXDM! Total nr of bytes within all original packets that are considered to be payload bytes */
  uint32 uncompressed_incoming_pkts_bytes;    /* Total nr of bytes received in all incoming uncompressed packets */
  uint32 uncompressed_outgoing_pkts_bytes;    /* Total nr of bytes within TX'd in all uncompressed packets */

  //-------------------------------------------------------------------------
  // Stats that are collected only when the configuration is to Compress Headers only (i.e., bypasses incompressible flows state machine)
  //-------------------------------------------------------------------------
  uint32 comp_pkts_hdr_bytes;        /* Total nr of bytes within all compressed packets that are considered to be header bytes (rough estimate)  when Header only compression is configured */
  uint32 comp_pkts_payload_bytes;    /* Total nr of bytes within all compressed packets that are considered to be payload bytes (rough estimate) when Header only compression is configured */
  // end of Stats that are collected only when the configuration is to Compress Headers only (i.e., bypasses incompressible flows state machine)

#endif  // !FEATURE_IPDC_OFFTARGET

  /* Stats for each Flow */
  ipdc_comp_per_flow_stats_t      flow_stats[IPDC_MAX_COMPRESSOR_FLOWS];

  /* Flow info for uidentifiable flows (those that are non-IP) */
  ipdc_comp_per_flow_stats_t      untracked_flow_stats;

} ipdc_comp_stats_t;



/*---------------------------------------------------------------------------
  IPDC Decompressor Metrics data structure
---------------------------------------------------------------------------*/

typedef struct 
{
  ipdc_pkt_e_type       type;
  uint16                bytes_orig;
  uint16                bytes_decomp;
  uint16                found_matches;
} ipdc_decomp_metrics_t;


/*---------------------------------------------------------------------------
  IPDC Decompressor Statistics data structure
---------------------------------------------------------------------------*/

/**
 * Channel specific statistics for the decompressor.
 * The stats can be accessed via ipdc_decomp_get_stats() function.
 */

typedef struct 
{
  /* General */
  uint32 out_packets;           /* Nr of packets generated by decompressor.      */
#if defined (FEATURE_IPDC_OFFTARGET) || defined (BOA_APPS)
  uint64 orig_pkt_size;         /* Total nr of bytes in original packets         */
  uint64 bytes_decomp;          /* Total nr of bytes after decompression.*/
#else  // UE
  uint32 orig_pkt_size;         /* Total nr of bytes in original packets         */
  uint32 bytes_decomp;          /* Total nr of bytes after decompression.*/
#endif
  /* The incoming IPDC Header tags these types of packets */
  uint32 decompressed;          /* Nr of packets decompressed */
  uint32 out_packets_type_00;   /* Nr of packets with packet type 00 */
  uint32 out_packets_type_11;   /* Nr of packets with packet type 11  */
  uint32 out_packets_type_10;   /* Nr of packets with packet type 10  */
  uint32 out_packets_type_01;   /* Nr of packets with packet type 01  */
  uint32 decomp_failed;         /* Nr of decompression failures  */
  uint32 found_matches;
} ipdc_decomp_stats_t;





/*---------------------------------------------------------------------------
  IPDC decompressor Data structures 
---------------------------------------------------------------------------*/
typedef struct 
{
  byte     *mem;
  uint16    end_idx;
  uint16    capacity;
} DecompMem ;


/*---------------------------------------------------------------------------
  Data structures for the IPDC compressor 
---------------------------------------------------------------------------*/

typedef uint16 (*HashFuncPtrType)(uint16 hashbucket_size,byte a,byte b,byte c,byte d,byte e);

typedef struct 
{
  byte      empty;
  byte     *mem;
  uint16    start_idx;
  uint16    end_idx;
  uint16    cap;
  uint16    max_end_idx;
  uint16    dtc_start_idx;
  uint16    dtc_end_idx;
} ExtCompMem;


typedef struct
{
  uint16                     hashbucket_size;
#ifdef Q6_OPTIMIZED_HKEY_COMPUTATION             // Feature supported on UE Target Only!
  uint64                     hash_mask;          // 64 bit hash mask derived from hash_bucket_size
#endif  // Q6_OPTIMIZED_HKEY_COMPUTATION
  uint16                    *hash_vals_array;
  uint16                    *hash_keys_array;
  uint32                     allocated_hkey_cnt;
  HashFuncPtrType            myHashFunc;
} HashBuckets;

typedef struct 
{
  uint16 distance_to_ptr;
  uint16 lookback_len;
  uint8 match_len;
}PointerMetadata;

typedef struct 
{
  ipdc_comp_config_options_s_type     comp_params;          // Compressor Engine's Configuration Parameters
  ExtCompMem                          cmp_mem;              // Compressor Engine's Extended Compression Memory (ECM) data structure
  HashBuckets                        *hb_ptr;               // Pointer to the Compressor Engine's Hash Buckets data structure
  ipdc_comp_metrics_t                 per_pkt_metrics;      // Compressor Engine's Per Packet Metrics
  ipdc_comp_stats_t                   comp_stats;           // Compressor Engine's Statistics

  uint8                               num_matches;          // current number of Pointer Meta Data blocks that have been added to the Compressed Packet's DSM Item
  PointerMetadata                     pointer_metadata_array[PS_IPDC_MAX_NUM_MATCHES];
  uint16                              prev_match_idx;

  ipdci_pkt_type                      base_pkt_type;
  uint16                              lfs_start;            // Look Forward String (LFS) Start (index into ECM extended region)    
#if !defined (FEATURE_IPDC_OFFTARGET) && !defined (BOA_APPS)  // UE
  ps_comp_logging_handle_type         logging_handle;       // Support for DPL logging of Compressed Packets & Compression Memory
#endif

} ipdc_comp_ctl_blk_s_type;


/*---------------------------------------------------------------------------
  Data structures for the IPDC decompressor  
---------------------------------------------------------------------------*/
typedef struct
{
  ipdc_decomp_config_options_s_type   decomp_params;        // Decompressor Engine's Configuration Parameters
  DecompMem                           decmp_mem;            // Decompressor Engine's Decompression Memory data structure
  ipdc_decomp_metrics_t               per_pkt_metrics;      // Decompressor Engine's Per Packet Metrics
  ipdc_decomp_stats_t                 decomp_stats;         // Decompressor Engine's Statistics
} ipdc_decomp_ctl_blk_s_type;


/*---------------------------------------------------------------------------
  IP Data Compression (IPDC) instance structure
---------------------------------------------------------------------------*/
typedef struct ipdc_instance_s_type
{
  ipdc_comp_ctl_blk_s_type      *comp_ptr;     /* IPDC Compressor Engine Control Block    */
  ipdc_decomp_ctl_blk_s_type    *decomp_ptr;   /* IPDC Decompressor Engine Control Block     */
} ipdc_instance_s_type;




/*===========================================================================
MACRO IPDCI_MEMALLOC

DESCRIPTION
  This macro is a wrapper to PS function which calls the fully re-entrant 
  rex_malloc().
DEPENDENCIES
  None

RETURN VALUE
  pointer to memory allocated
  NULL if failed to allocate memory

SIDE EFFECTS
  Allocates memory of the requested size
===========================================================================*/
#if !defined (FEATURE_IPDC_OFFTARGET) && !defined (BOA_APPS)  // UE
#define ipdci_memalloc ps_system_heap_mem_alloc
#else
#define ipdci_memalloc malloc
#endif

/*===========================================================================
FUNCTION IPDCI_MEMFREE

DESCRIPTION
  This macro is a wrapper to PS function which calls the fully re-entrant 
  rex_free().

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  Frees the passed memory buffer
===========================================================================*/
#if !defined (FEATURE_IPDC_OFFTARGET) && !defined (BOA_APPS)  // UE
#define ipdci_memfree(mem_ptr) \
  if ((mem_ptr) != NULL) \
  { \
    PS_SYSTEM_HEAP_MEM_FREE(mem_ptr); \
  }
#else
#define ipdci_memfree(mem_ptr) \
  if ((mem_ptr) != NULL) \
  { \
    free(mem_ptr); \
  }
#endif

/*===========================================================================
MACROS: IPDCI_NTOHS, IPIPDCI_HTONS, IPDCI_HTONL

DESCRIPTION
  These macros are wrappers for the Host-to-Network and Network-to-Host
  conversion functions
DEPENDENCIES
  None

RETURN VALUE
  The network or host ordered value

SIDE EFFECTS
  None
===========================================================================*/
#if !defined (FEATURE_IPDC_OFFTARGET) && !defined (BOA_APPS)  // UE
#define ipdci_ntohs ps_ntohs
#define ipdci_ntohl ps_ntohl
#define ipdci_htons ps_htons
#define ipdci_htonl ps_htonl
#else
#define ipdci_ntohs NTOHS
#define ipdci_ntohl NTOHL
#define ipdci_htons HTONS
#define ipdci_htonl HTONL
#endif

#endif /* PS_IPDCI_H */
