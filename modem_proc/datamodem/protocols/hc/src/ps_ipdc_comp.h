#ifndef PS_IPDC_COMP_H
#define PS_IPDC_COMP_H

/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                    P S _ I P D C _ C O M P . H

GENERAL DESCRIPTION
  This file contains all the data structures, variables, enums and
  function prototypes needed to support LempelZiv 77 compression using
  the IPDC Compression Algorithm.
 
  LZ77 � is a sliding window based compression algorithm. 
    - Upon encountering a string that was seen previously in the window,
      the duplicate is replaced with a pointer to the original using
      ( length, distance ) pair
    - Scans up to 32kbytes in the past and finds matches up to 260 bytes long
        - For our purpose, scans up to start of current packet 

  
 
  Copyright (c) 2013 by QUALCOMM Technologies Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary.  
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/



/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "comdef.h"
#include "ps_ipdci.h"

/*===========================================================================

                          CONSTANT DECLARATIONS

===========================================================================*/


/*===========================================================================

                      PUBLIC FUNCTION DECLARATIONS

===========================================================================*/

/*===========================================================================
FUNCTION ps_ipdc_comp_packet()

DESCRIPTION 
  This function invokes the compression algorithm on the packet.

DEPENDENCIES
  A valid IPDC handle must be passed.

RETURN VALUE
   0   if successful.
  -1   in case of error 

SIDE EFFECTS 
  May replace the original packet's uncompressed data contents with compressed
  data contents. 

===========================================================================*/
int ps_ipdc_comp_packet
( 
  ipdc_req_pkt_comp_info_type  *comp_pkt_info_ptr
 );


/*===========================================================================
FUNCTION ps_ipdc_comp_get_uncompressed_packet()

DESCRIPTION 
  This function adds an IPDC header indicating nothing was compressed
  to original packet.
  Nothing Compressed packet is returned in orig_pkt argument.

DEPENDENCIES
  A valid IPDC handle must be passed.

RETURN VALUE
   0   if successful.
  -1   in case of error 

SIDE EFFECTS 
  Replaces the original packet's uncompressed data contents with an
  uncompressed IPDC packet (adds IPDC header to original packet).

===========================================================================*/
int ps_ipdc_comp_get_uncompressed_packet
( 
  ipdc_req_pkt_comp_info_type  *comp_pkt_info_ptr,
  boolean                       chain_src_dsm_to_target_dsm
 );


#endif  // PS_IPDC_COMP_H
