#ifndef PS_IPDC_UTILS
#define PS_IPDC_UTILS
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

           P S _ I P D C _ U T I L S . H

GENERAL DESCRIPTION
  Header file for the IP Data Compression protocol utility functions
  
 
  Copyright (c) 2013 by QUALCOMM Technologies Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary.  
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/



/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#ifdef __cplusplus
extern "C" {
#endif

#include "comdef.h"
#include "ps_ipdci.h"



/*===========================================================================

                     DEFINITIONS AND CONSTANTS

===========================================================================*/
#if !defined (FEATURE_IPDC_OFFTARGET) && !defined (BOA_APPS) // UE

#define MSG_IPDC_ERROR(s,x,y,z)    MSG_3(MSG_SSID_DS_UMTS, MSG_LEGACY_ERROR, s,x,y,z)

#ifdef IPDC_DEBUG
#define MSG_IPDC_HIGH(s,x,y,z)     MSG_3(MSG_SSID_DS_UMTS, MSG_LEGACY_HIGH, s,x,y,z)
#define MSG_IPDC_MED(s,x,y,z)      MSG_3(MSG_SSID_DS_UMTS, MSG_LEGACY_MED, s,x,y,z)
#define MSG_IPDC_LOW(s,x,y,z)      MSG_3(MSG_SSID_DS_UMTS, MSG_LEGACY_LOW, s,x,y,z)

#define MSG_IPDC_DEBUG(s,x,y,z)    MSG_IPDC_HIGH(s,x,y,z)

#define MSG_IPDC_BUFFER(s,x,y,z)   MSG_IPDC_MED(s,x,y,z)

#else
#define MSG_IPDC_HIGH(s,x,y,z)     MSG_3(MSG_SSID_DS_UMTS, MSG_LEGACY_HIGH, s,x,y,z)

#define MSG_IPDC_MED(s,x,y,z)      MSG_3(MSG_SSID_DS_UMTS, MSG_LEGACY_MED, s,x,y,z)
#define MSG_IPDC_LOW(s,x,y,z)
#define MSG_IPDC_DEBUG(s,x,y,z)
#define MSG_IPDC_BUFFER(s,x,y,z)

#endif
#endif // !defined (FEATURE_IPDC_OFFTARGET) && !defined (BOA_APPS)

/*===========================================================================

                              DATA TYPES

===========================================================================*/

#ifdef FEATURE_IPDC_OFFTARGET // Following Data types are not supported on the UE Target!
/*
    ****  NOTE *****
  Following data structure is intended for use by the OFF-Target IPDC Tool
  Release SW which is used by External Customers only!
  *****************
*/
// For OFF-Target IPDC External Customer Logs:
typedef struct 
{
  ipdci_ipdc_hdr_pkt_type      pkt_type;
  uint16                       orig_pkt_length;  
  uint16                       orig_header_length;
  uint16                       payload_length_before_compression;
  uint16                       payload_length_after_compression;
  int                          gain;
} ipdcu_ext_log_pkt_metrics;

#endif  // #ifdef FEATURE_IPDC_OFFTARGET -  Data types are not supported on the UE Target!



/*===========================================================================

                               MACROS

===========================================================================*/
/*===========================================================================
MACRO IPDC_DCM_IDX_DEC

DESCRIPTION
  This macro decreases the passed in index number by 1 and modulo with capacity

PARAMETERS
  idx : Current Index
  capacity : modulo value

DEPENDENCIES
  None

RETURN VALUE
  idx.

SIDE EFFECTS
  None
===========================================================================*/
//#define IPDC_DCM_IDX_DEC(idx,capacity)  (((idx)+(capacity-1)) & (capacity-1))


/*===========================================================================
MACRO IPDC_DCM_IDX_INC

DESCRIPTION
  This macro increases the passed in index number by 1 and modulo with capacity

  NOTE: This MACRO only works when the Decomp Memory size is a power of 2!


PARAMETERS
  idx : Current Index
  capacity : modulo value

DEPENDENCIES
  None

RETURN VALUE
  idx.

SIDE EFFECTS
  None
===========================================================================*/
//#define IPDC_DCM_IDX_INC(idx,capacity) (((idx)+1) % (capacity))  // NOTE: Do not use this MACRO!! The MIPS cost is too high!!!
#define IPDC_DCM_IDX_INC(idx,capacity) (((idx)+1) & (capacity-1))

/*===========================================================================
MACRO IPDC_DCM_IDX_ADD

DESCRIPTION
  This macro adds the passed in index number by 'add' and modulo with capacity

  NOTE: This MACRO only works when the DecompMem size is a power of 2!

PARAMETERS
  idx : Current Index
  add : value  to add
  capacity : modulo value

DEPENDENCIES
  None

RETURN VALUE
  idx.

SIDE EFFECTS
  None
===========================================================================*/
//#define IPDC_DCM_IDX_ADD(idx,add,capacity) (((idx)+(add)) % (capacity))//  // NOTE: Do not use this MACRO!! The MIPS cost is too high!!!
#define IPDC_DCM_IDX_ADD(idx,add,capacity) (((idx)+(add)) & (capacity-1))

/*===========================================================================
MACRO IPDC_DCM_IDX_SUB

DESCRIPTION
  This macro decreases the passed in index number by 'sub' and modulo with capacity

PARAMETERS
  idx : Current Index
  sub : Value to subtract
  capacity : modulo value

DEPENDENCIES
  None

RETURN VALUE
  idx.

SIDE EFFECTS
  None
===========================================================================*/
#define IPDC_DCM_IDX_SUB(idx,sub,capacity)  ((((idx)-(sub)+1)+(capacity-1)) & (capacity-1))


/*===========================================================================

                      PUBLIC DATA DECLARATIONS

===========================================================================*/


/*===========================================================================

                      PUBLIC FUNCTION DECLARATIONS

===========================================================================*/

/*===========================================================================
FUNCTION ps_ipdcu_alloc_compressor_mem()

DESCRIPTION
  Allocates and initializes Extended Compression Memory. 
  If the Memory is to be pre-filled, updates contents of the Memory 
  with the Prefill data. 

DEPENDENCIES
  None

RETURN VALUE
  True if Compressor Engine's Extended Compression Memory was Allocated
  & Initialized
  False otherwise

SIDE EFFECTS
  Will load the Compressor Engine's Extended Compression Memory with
  the Prefill data if prefill_ptr != NULL and prefill_size > 0.
===========================================================================*/
boolean ps_ipdcu_alloc_compressor_mem
(
  ExtCompMem                           * ecm_ptr,
  uint16                                 capacity,
  ipdc_hk_ecm_prealloc_size_e_type       prealloc_size,
  const byte                           * const prefill_ptr,
  uint16                                 prefill_size
);

/*===========================================================================
FUNCTION ps_ipdcu_flush_compressor_mem()

DESCRIPTION
  Function to flush the Compressor Engine's Extended Compression Memory.
 
DEPENDENCIES
  None

RETURN VALUE
  return FALSE is failure to flush else return TRUE

SIDE EFFECTS
  Flushes Compressor Engine's Extended Compression Memory and sets 
  contents to all zeroes
===========================================================================*/
boolean ps_ipdcu_flush_compressor_mem
(
  ExtCompMem              * ecm_ptr,
  const byte              * const mem_prefill_ptr,
  uint16                    mem_prefill_size
);

/*===========================================================================
FUNCTION ps_ipdcu_free_compressor_mem()

DESCRIPTION
  Frees the Compressor Engine's allocated Extended Compression Memory.

DEPENDENCIES
  None

RETURN VALUE
  True if  Compressor Engine's allocated Extended Compression Memory was Freed
  False otherwise

SIDE EFFECTS
  None
===========================================================================*/
boolean ps_ipdcu_free_compressor_mem
(
  ExtCompMem *ecm_ptr
);

#ifdef FEATURE_IPDC_OFFTARGET  // Following functions are not supported on the UE Target!
/*===========================================================================
FUNCTION ps_ipdcu_dump_compressor_mem()

DESCRIPTION
  Function to dump the current contents of Compressor Engine's Memory 

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
void ps_ipdcu_dump_compressor_mem
(
  ExtCompMem * ecm_ptr
);

/*===========================================================================
FUNCTION ps_ipdcu_get_compressor_mem_5_char_blk()

DESCRIPTION 
 
  This utility function returns the 5 characters in ExtCompMem starting from
  the provided Compressor Memory index and packs them into a uint64 value.

DEPENDENCIES
  The IPDC Compressor Engine must have been allocated!

RETURN VALUE
  uint64 value - contains the 5 characters in ExtCompMem at the requested
                 index into ExtCompMem

SIDE EFFECTS 
  None 
  
===========================================================================*/
uint64 ps_ipdcu_get_compressor_mem_5_char_blk
(
  ExtCompMem             *ecm, 
  uint16                  ecm_idx
);
#endif  // #ifdef FEATURE_IPDC_OFFTARGET - Following functions are not supported on the UE Target!

/*===========================================================================
FUNCTION ps_ipdcu_alloc_decompressor_mem()

DESCRIPTION
  Allocates and initializes Memory for use by the Decompressor Engine. 
  If the Memory is to be pre-filled, updates contents of the Memory 
  with the Prefill data. 

DEPENDENCIES
  None

RETURN VALUE
  True if Decompressor Engine's Memory was Allocated & Initialized
  False otherwise

SIDE EFFECTS
  Will load the Decompressor Engine's Memory with the Prefill data if 
  prefill_ptr != NULL and prefill_size > 0.
===========================================================================*/
boolean ps_ipdcu_alloc_decompressor_mem
(
  DecompMem   *dcm_ptr,
  int          alloc_memsize,
  const byte * const prefill_ptr,
  uint16       prefill_size
);

/*===========================================================================
FUNCTION ps_ipdcu_flush_decompressor_mem()

DESCRIPTION
  Function to flush the Decompressor Engine's Memory.
 
DEPENDENCIES
  None

RETURN VALUE
  return FALSE is failure to flush else return TRUE

SIDE EFFECTS
  Flushes Decompressor Engine's Memory and sets contents to all zeroes
===========================================================================*/
boolean ps_ipdcu_flush_decompressor_mem
(
  DecompMem               * dcm_ptr,
  const byte              * const mem_prefill_ptr,
  uint16                    mem_prefill_size
);

/*===========================================================================
FUNCTION ps_ipdcu_free_decompressor_mem()

DESCRIPTION
  Frees the Decompressor Engine's allocated Memory.

DEPENDENCIES
  None

RETURN VALUE
  True if Decompressor Engine's Memory was Freed
  False otherwise

SIDE EFFECTS
  None
===========================================================================*/
boolean ps_ipdcu_free_decompressor_mem
(
  DecompMem *dcm_ptr
);

#ifdef FEATURE_IPDC_OFFTARGET  // Following functions are not supported on the UE Target!

/*===========================================================================
FUNCTION ps_ipdcu_dump_decompressor_mem()

DESCRIPTION
  Function to dump the current contents of Decompressor Engine's Memory

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
void ps_ipdcu_dump_decompressor_mem
(
  DecompMem * dcm_ptr
);

/*===========================================================================
FUNCTION ps_ipdcu_dump_dsm()

DESCRIPTION
  Helper function to log the contents of the dsm packet 

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
void ps_ipdcu_dump_dsm
(
  dsm_item_type **dsm,
  int offset,
  int len
);

/*===========================================================================
FUNCTION PS_IPDC_LOGGING_GET_COMP_GAIN_STAT

DESCRIPTION
  Returns the IPDC Compressor Engine's overall compression gain statistic.

DEPENDENCIES
  The IPDC memheap must have been initialized and ipdc_alloc() called prior to
  calling ps_ipdc_logging_log_comp_stats()

PARAMETERS
  ipdc_handle_type *handle - pointer to the ipdc handle

RETURN VALUE
   current compression gain as a percentage

SIDE EFFECTS
  
===========================================================================*/
int ps_ipdcu_get_comp_gain_stat
(
  ipdc_handle_type               handle           /* IPDC instance         */
);

/*===========================================================================
FUNCTION ps_ipdcu_log_comp_eng_hash_key_distribution_stat

DESCRIPTION
  Logs the IPDC Compressor Engine's Hash Key Distribution statistic.

DEPENDENCIES
  The IPDC memheap must have been initialized and ipdc_alloc() called prior to
  calling ps_ipdc_logging_log_comp_stats()

PARAMETERS
  ipdc_handle_type *handle - pointer to the ipdc handle

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
void ps_ipdcu_log_comp_eng_hash_key_distribution_stat
(
  ipdc_handle_type               handle           /* IPDC instance */ 
);

/*===========================================================================
FUNCTION PS_IPDC_GET_COMP_ENG_SRCH_CNT_STAT

DESCRIPTION
  Returns the IPDC Compressor Engine's overall search count statistic.

DEPENDENCIES
  The IPDC memheap must have been initialized and ipdc_alloc() called prior to
  calling ps_ipdc_logging_log_comp_stats()

PARAMETERS
  ipdc_handle_type *handle - pointer to the ipdc handle

RETURN VALUE
   current overall search count statistic

SIDE EFFECTS
  
===========================================================================*/
unsigned long long ps_ipdcu_get_comp_eng_srch_cnt_stat
(
  ipdc_handle_type               handle           /* IPDC instance         */
);

/*===========================================================================
FUNCTION PS_IPDC_GET_COMP_ENG_HASH_COLLISION_CNT

DESCRIPTION
  Returns the IPDC Compressor Engine's hash miss count statistic.

DEPENDENCIES
  The IPDC memheap must have been initialized and ipdc_alloc() called prior to
  calling ps_ipdcu_get_comp_eng_hash_miss_cnt_stat()

PARAMETERS
  ipdc_handle_type *handle - pointer to the ipdc handle

RETURN VALUE
   Number of Hash collisions

SIDE EFFECTS
  
===========================================================================*/
unsigned long long ps_ipdcu_get_comp_eng_hash_collision_cnt
(
  ipdc_handle_type               handle           /* IPDC instance         */
);

/*===========================================================================
FUNCTION ps_ipdcu_log_comp_eng_stats

DESCRIPTION
  Logs IPDC Compressor Engine Stats.

DEPENDENCIES
  The IPDC memheap must have been initialized and ipdc_alloc() called prior to
  calling ps_ipdc_logging_log_comp_eng_stats()

PARAMETERS
  ipdc_handle_type *handle - pointer to the ipdc handle

RETURN VALUE
   none

SIDE EFFECTS
  
===========================================================================*/
void ps_ipdcu_log_comp_eng_stats
(
  ipdc_handle_type               handle           /* IPDC instance         */
);

/*===========================================================================
FUNCTION ps_ipdcu_log_comp_eng_metrics

DESCRIPTION
  Logs IPDC Compressor Engine's Per PDCP SDU Metrics.

DEPENDENCIES
  The IPDC memheap must have been initialized and ipdc_alloc() called prior to
  calling ps_ipdc_logging_log_comp_eng_metrics()

PARAMETERS
  ipdc_handle_type *handle - pointer to the ipdc handle

RETURN VALUE
   none

SIDE EFFECTS
  
===========================================================================*/
void ps_ipdcu_log_comp_eng_metrics
(
  ipdc_handle_type               handle           /* IPDC instance         */
);

/*===========================================================================
FUNCTION PS_IPDCU_GET_IPV4_PKT_SRC_IP_ADDR

DESCRIPTION
  Returns the Source IP Address from the IPv4 Packet.

DEPENDENCIES
  None

PARAMETERS
  dsm_item_type *pkt_ptr - pointer to dsm item containing the IPv4 packet
  uint32 *src_ip_addr_ptr  - pointer to memory in which to store the Src IP Address

RETURN VALUE
  returns TRUE if 'src_ip_addr_ptr' contains a valid Src IP Address
  returns FALSE if 'src_ip_addr_ptr' is invalid

SIDE EFFECTS
  None
===========================================================================*/
boolean ps_ipdcu_get_ipv4_pkt_src_ip_addr
(
  dsm_item_type                 *pkt_ptr,           /* Pointer to IPv4 Packet */
  uint32                        *src_ip_addr_ptr
);

/*===========================================================================
FUNCTION PS_IPDCU_GET_IPV4_PKT_DST_IP_ADDR

DESCRIPTION
  Returns the Destination IP Address from the IPv4 Packet.

DEPENDENCIES
  None

PARAMETERS
  dsm_item_type *pkt_ptr - pointer to dsm item containing the IPv4 packet
  uint32 *dst_ip_addr_ptr  - pointer to memory in which to store the Dst IP Address

RETURN VALUE
  returns TRUE if 'dst_ip_addr_ptr' contains a valid Src IP Address
  returns FALSE if 'dst_ip_addr_ptr' is invalid

SIDE EFFECTS
  None
===========================================================================*/
boolean ps_ipdcu_get_ipv4_pkt_dst_ip_addr
(
  dsm_item_type                 *pkt_ptr,           /* Pointer to IPv4 Packet */
  uint32                        *dst_ip_addr_ptr
);

/*===========================================================================
FUNCTION PS_IPDCU_GET_IPV6_PKT_SRC_IP_ADDR

DESCRIPTION
  Returns the Source IP Address from the IPv6 Packet.

DEPENDENCIES
  None

PARAMETERS
  dsm_item_type *pkt_ptr - pointer to dsm item containing the IPv6 packet
  ipdc_ipv6_ipv4_addr_type *src_ip_addr_ptr  - pointer to memory in which to store the Src IP Address

RETURN VALUE
  returns TRUE if 'src_ip_addr_ptr' contains a valid Src IP Address
  returns FALSE if 'src_ip_addr_ptr' is invalid

SIDE EFFECTS
  None
===========================================================================*/
boolean ps_ipdcu_get_ipv6_pkt_src_ip_addr
(
  dsm_item_type                 *pkt_ptr,           /* Pointer to IPv6 Packet */
  ipdc_ipv6_ipv4_addr_type      *src_ip_addr_ptr
);

/*===========================================================================
FUNCTION PS_IPDCU_GET_IPV6_PKT_DST_IP_ADDR

DESCRIPTION
  Returns the Destination IP Address from the IPv6 Packet.

DEPENDENCIES
  None

PARAMETERS
  dsm_item_type *pkt_ptr - pointer to dsm item containing the IPv6 packet
  uint32 *dst_ip_addr_ptr  - pointer to memory in which to store the Dst IP Address

RETURN VALUE
  returns TRUE if 'dst_ip_addr_ptr' contains a valid Src IP Address
  returns FALSE if 'dst_ip_addr_ptr' is invalid

SIDE EFFECTS
  None
===========================================================================*/
boolean ps_ipdcu_get_ipv6_pkt_dst_ip_addr
(
  dsm_item_type                 *pkt_ptr,           /* Pointer to IPv6 Packet */
  ipdc_ipv6_ipv4_addr_type      *dst_ip_addr_ptr
);

/*===========================================================================
FUNCTION PS_IPDCU_GET_PKT_COMPRESSION_METRICS

DESCRIPTION

  ****  NOTE *****
  Function is intended for use by the OFF-Target IPDC Release SW which is
  used by External Customers only!
  *****************

  Gets packet metrics from the IPDC Compressor Engine for the last packet.  
  This information will then be logged to the external customer log file.

DEPENDENCIES
  The IPDC memheap must have been initialized and ipdc_alloc() called prior to
  calling ps_ipdc_log_ext_cust_pkt_compression_metrics()

PARAMETERS
  ipdc_handle_type *handle - pointer to the ipdc handle

RETURN VALUE
   none

SIDE EFFECTS
   Contents of metrics_ptr's data structure is updated 
===========================================================================*/
void ps_ipdcu_get_pkt_compression_metrics
(
  ipdc_handle_type               handle,           /* IPDC instance         */
  ipdcu_ext_log_pkt_metrics     *metrics_ptr
);

/*===========================================================================
FUNCTION ps_ipdcu_get_pkt_compression_gain

DESCRIPTION
  Computes the compression gain for the packet.

DEPENDENCIES
  The IPDC memheap must have been initialized and ipdc_alloc() called prior to
  calling get_pkt_compression_gain()

PARAMETERS
  ipdc_handle_type *handle - pointer to the ipdc handle

RETURN VALUE
   none

SIDE EFFECTS
  
===========================================================================*/
int  ps_ipdcu_get_pkt_compression_gain
(
  ipdc_handle_type               handle           /* IPDC instance         */
);
#endif  // #ifdef FEATURE_IPDC_OFFTARGET -  Following functions are not supported on the UE Target!

#if defined(FEATURE_IPDC_OFFTARGET) || defined(BOA_APPS) // Following functions are not supported on the UE Target! but are supported in BOA_APPS
/*===========================================================================
FUNCTION ps_ipdcu_log_decomp_eng_stats

DESCRIPTION
  Logs IPDC DeCompressor Engine Stats.

DEPENDENCIES
  The IPDC memheap must have been initialized and ipdc_alloc() called prior to
  calling ps_ipdc_logging_log_decomp_eng_stats()

PARAMETERS
  ipdc_handle_type *handle - pointer to the ipdc handle

RETURN VALUE
   none

SIDE EFFECTS
  
===========================================================================*/
void ps_ipdcu_log_decomp_eng_stats
(
  ipdc_handle_type               handle           /* IPDC instance         */
);

/*===========================================================================
FUNCTION ps_ipdcu_log_decomp_eng_metrics

DESCRIPTION
  Logs IPDC DeCompressor Engine's Per PDCP SDU Metrics.

DEPENDENCIES
  The IPDC memheap must have been initialized and ipdc_alloc() called prior to
  calling ps_ipdc_logging_log_decomp_eng_metrics()

PARAMETERS
  ipdc_handle_type *handle - pointer to the ipdc handle

RETURN VALUE
   none

SIDE EFFECTS
  
===========================================================================*/
void ps_ipdcu_log_decomp_eng_metrics
(
  ipdc_handle_type               handle           /* IPDC instance         */
);
#endif  // #if defined(FEATURE_IPDC_OFFTARGET) || defined(BOA_APPS) -  Following functions are not supported on the UE Target!

/*===========================================================================
FUNCTION ps_ipdcu_get_packet_info

DESCRIPTION
  Gets information for the incoming packet

PARAMETERS
  dsm_item_type - dsm item containing the pdcp sdu

RETURN VALUE
  Fills out the output parameter 'ip_header'
===========================================================================*/
void ps_ipdcu_get_packet_info
(
  dsm_item_type        *p_pdcp_sdu,
  ipdc_pkt_info_type   *ip_header
);


/*===========================================================================
FUNCTION ps_ipdcu_get_compressor_packet_info

DESCRIPTION
  Gets information for the incoming packet

PARAMETERS
  dsm_item_type - dsm item containing the pdcp sdu

RETURN VALUE
  Fills out the output parameter 'ip_header'
===========================================================================*/
void ps_ipdcu_get_compressor_packet_info
(
  dsm_item_type        *p_pdcp_sdu,
  ipdc_pkt_info_type   *ip_header
);

/*===========================================================================
FUNCTION ps_ipdcu_get_actual_compression_mem_size

DESCRIPTION
  This function converts the enumerated compression memory size configuration
  parameter into the actual value in bytes.

DEPENDENCIES
  None

RETURN VALUE
   uint16

SIDE EFFECTS
  None 
===========================================================================*/
uint16 ps_ipdcu_get_actual_compression_mem_size(ipdc_compression_mem_size_e_type cmp_mem_size);

/*===========================================================================
FUNCTION ps_ipdcu_get_actual_hash_bucket_size

DESCRIPTION
  This function converts the enumerated hash bucket configuration
  parameter into the actual value in bytes.

DEPENDENCIES
  None

RETURN VALUE
   uint16

SIDE EFFECTS
  None 
===========================================================================*/
uint16 ps_ipdcu_get_actual_hash_bucket_size(ipdc_hb_size_e_type hb_size);

/*===========================================================================
FUNCTION ps_ipdcu_calculate_ipdc_hdr_checksum

DESCRIPTION
  Calculates the 6 bit IPDC Header checksum for the current packet.

  This function computes the checksum of the given  
  data buffer across PS_IPDC_MIN_MATCH_IN_BYTES bytes

PARAMETERS

RETURN VALUE
  uint8 checksum value (lower 6 bits contain checksum)
===========================================================================*/
uint8 ps_ipdcu_calculate_ipdc_hdr_checksum
(
  uint8                         *buffer
);

/*===========================================================================
FUNCTION ps_ipdcu_validate_ipdc_hdr_checksum

DESCRIPTION
  Validates the current packet's IPDC Header checksum.

PARAMETERS

RETURN VALUE
  if checksum validation passes, return TRUE
  otherwise return FALSE
===========================================================================*/
boolean ps_ipdcu_validate_ipdc_hdr_checksum
(
  uint8                         pkt_hdr_checksum,
  uint8                        *buffer
);


#ifdef __cplusplus
}
#endif

#endif  // #ifndef PS_IPDC_UTILS
