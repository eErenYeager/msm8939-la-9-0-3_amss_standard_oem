#ifndef DSUMTSSUBSMGR_H
#define DSUMTSSUBSMGR_H
/*===========================================================================

                        D S U M T S P S  S U B S  M G R 

                          H E A D E R   F I L E

DESCRIPTION
  This file contains common data declarations and function prototypes specific 
  to DUAL SIM feature. 
  
Copyright (c) 2010 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.
===========================================================================*/


/*===========================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/3gpp/dsprofile/inc/dsumts_subs_mgr.h#2 $ 
  $DateTime: 2015/12/07 00:52:58 $ $Author: teralak $ 

when       who     what, where, why
--------   ---     ---------------------------------------------------------
02/16/11   ttv     Added DSDS changes for consolidated profile family.
11/22/10   ttv     Creation of file.
===========================================================================*/


/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "datamodem_variation.h"
#include "customer.h"
#include "comdef.h"
#include "cm_dualsim.h"
#include "dsumtspdpreg.h"
#include "ds_3gppi_utils.h"
#include "ds_profile.h"

#if defined(FEATURE_DATA_WCDMA_PS) || defined(FEATURE_GSM_GPRS)

//Define random default values for each profile family to start with
//This will be overwritten after user selection
#define DSUMTS_DEF_PROFILE_FOR_EMBEDDED_SUB1 1
#define DSUMTS_DEF_PROFILE_FOR_EMBEDDED_SUB2 2
#define DSUMTS_DEF_PROFILE_FOR_EMBEDDED_SUB3 3

#define DSUMTS_DEF_PROFILE_FOR_TETHERED_SUB1 1
#define DSUMTS_DEF_PROFILE_FOR_TETHERED_SUB2 2
#define DSUMTS_DEF_PROFILE_FOR_TETHERED_SUB3 3


/*-----------------------------------------------------------------------------
  Macro to get the subscription ID.
  If subscription is none, set to default (subscription 1)
-----------------------------------------------------------------------------*/
#define DS_UMTS_PDP_GET_SUBS_ID(data_subs) {\
  data_subs = dsumts_subs_mgr_get_subs_id(ds3g_get_ps_subs_id());\
  if (data_subs == DS_UMTS_PDP_SUBSCRIPTION_NONE) \
  {\
    data_subs = DS_UMTS_PDP_SUBSCRIPTION_1;\
  }\
  DS_3GPP_MSG3_HIGH("Data Subscription is : %d", data_subs, 0, 0);\
}

/*----------------------------------------------------------------------------- 
  Macro to convert subs_id to be used only for dsumtspdpregint.c
  -----------------------------------------------------------------------------*/
#define DS_UMTS_CONVERT_PROFILE_SUBS_ID_TO_PDPREGINT(data_subs) {\
  if(data_subs == DS_PROFILE_ACTIVE_SUBSCRIPTION_1)\
  {\
    data_subs = DS_UMTS_PDP_SUBSCRIPTION_NONE;\
  }\
  else if (data_subs == DS_PROFILE_ACTIVE_SUBSCRIPTION_2)\
  {\
    data_subs = DS_UMTS_PDP_SUBSCRIPTION_1;\
  }\
  else if (data_subs == DS_PROFILE_ACTIVE_SUBSCRIPTION_3)\
  {\
    data_subs = DS_UMTS_PDP_SUBSCRIPTION_2;\
  }\
  else\
  {\
    data_subs = DS_UMTS_PDP_SUBSCRIPTION_NONE;\
  }\
}

/*----------------------------------------------------------------------------- 
  Macro to convert subs_id to be used only for dsumtspdpregint.c
  -----------------------------------------------------------------------------*/
#define DS_UMTS_CONVERT_SUBS_ID_FOR_PDPREGINT(data_subs) {\
  if(data_subs == DS_UMTS_PDP_SUBSCRIPTION_1)\
  {\
    data_subs = DS_UMTS_PDP_SUBSCRIPTION_NONE;\
  }\
  else if (data_subs == DS_UMTS_PDP_SUBSCRIPTION_2)\
  {\
    data_subs = DS_UMTS_PDP_SUBSCRIPTION_1;\
  }\
  else\
  {\
    data_subs = DS_UMTS_PDP_SUBSCRIPTION_NONE;\
  }\
}

/*===========================================================================
                   EXTERNAL FUNCTION PROTOTYPES
===========================================================================*/
/*===========================================================================
FUNCTION DSUMTS_SUBS_MGR_INIT

DESCRIPTION
  This function initializes the DS Subscription manager. It creates the mapping 
  table in EFS with default profile numbers for socket/Rmnet calls
   
PARAMETERS 
  none

DEPENDENCIES 
  Should be called only ONCE during startup.

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS  - initialization was successfull
  DS_UMTS_PDP_FAIL     - initialization failed

SIDE EFFECTS 
  Creates the profiles in EFS if the profiles are not present at startup
===========================================================================*/
ds_umts_pdp_profile_status_etype dsumts_subs_mgr_init(void);

/*===========================================================================

FUNCTION DSUMTS_SUBS_MGR_PROCESS_SUBS_CHANGE_EVENT

DESCRIPTION

  This function processes the events from CM that conveys a change in active
  subscription. Main processing is done in ds3gcmif. This function has the 
  umtsps specific additional processing

PARAMETERS
  as_id   - Active subscription ID from CM

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
void dsumts_subs_mgr_process_subs_change_event
(
  sys_modem_as_id_e_type as_id
);


/*===========================================================================

FUNCTION DSUMTS_SUBS_MGR_GET_SUBS_ID

DESCRIPTION

  This function returns the subscription ID internal to Data services that maps
  to the id from CM.
  CM : DS subscription ID mapping

PARAMETERS
  as_id   - Subscription ID from CM

DEPENDENCIES
  None

RETURN VALUE
  ds_umts_pdp_subs_e_type

SIDE EFFECTS
  None

===========================================================================*/
ds_umts_pdp_subs_e_type dsumts_subs_mgr_get_subs_id
(
  sys_modem_as_id_e_type as_id
);

/*===========================================================================
FUNCTION DSUMTS_SUBS_MGR_GET_SUBS_ID_FROM_DB_SUBS_ID 
 
DESCRIPTION

  This function returns ds_umts_pdp_subs_e_type from 
  ds_profile_db_subs_type


PARAMETERS
  uint32   - ps policy

DEPENDENCIES
  None

RETURN VALUE
 ds_umts_pdp_subs_e_type   -- Internal Subscription ID

SIDE EFFECTS
  None

===========================================================================*/
ds_umts_pdp_subs_e_type dsumts_subs_mgr_get_subs_id_from_policy_subs_id
(
   uint32 subs_id
);

#endif /* FEATURE_DATA_WCDMA_PS || FEATURE_GSM_GPRS */
#endif /* DSUMTSSUBSMGR_H */
