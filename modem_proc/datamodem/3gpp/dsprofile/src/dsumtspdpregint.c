/*===========================================================================

                        P D P   R E G I S T R Y   L I B R A R Y 

                          
GENERAL DESCRIPTION
  This file contains data and function definitions for PDP registry.
  PDP registry provides a library to maintain PDP profiles in 
  Embedded File System (EFS). This allows the profiles to be accessed
  across power cycles. ATCOP / UI and Data services use the API library
  to store and retrieve the PDP profile data
  
INITIALIZATION AND SEQUENCING REQUIREMENTS
  ds_umts_pdp_registry_init_internal() should be called at task / system startup. 
  ds_umts_pdp_registry_init_internal checks for the presence of the profiles in 
  EFS and creates them if they are not found and initializes them.

  *********************  I M P O R T A N T ***************************
  a) PDP profiles are PACKED structures and are stored as such in binary
  files. The offset of the sub structures & parameters within ( auth,
  qos info etc) use the offset location of from the start of the file. 
  Care should be take to ensure this when changes are made to the 
  profile content structure definitions.

  b) The EFS file access requires fully qualified file names to be specified
  e.g., to access "profile1" file under "pdp_profiles" directory, the 
  fully qualified file name "pdp_profiles/profile1" has to be specified.

  c) MACROS are used to perform checks on input/output parameters and also 
  repetetive FILE operations. The macros return from the calling function
  if an error occurs. 

  d) Profile format revisions and identifying changes :
  If there are any changes to the profile formats, the s/w would have a 
  different size/format than the profiles which are already stored in EFS.
  Later API releases added a version field to the profile schema to
  identify unique versions along with a configuration file.  Earlier releases
  use the length of the file contents to see if the profile stored in EFS
  is of a different version than the s/w.  The presence of the configuration
  file determines which mode is used to detect version upgrades.
  
  e) No validation is performed on the parameter sets for their 
  validity or combination with respect to other fields. It is the 
  caller's responsibility to ensure this. However, the "valid_flg"
  field is checked to see if the parameter set is defined.

  f) The PDP registry API uses syncrhonous EFS mechanism to retrieve
  write data from/to the file system. The calling task has to be 
  blocked for this to work correctly and is done by the EFS API
  functions. DO NOT DISABLE CONTEXT SWITCHING by USING TASKLOCK()/TASKFREE()
  when calling the pdp registry API's to ensure the proper functioning
  of the API's. 

  g) The PDP registry API's do not enforce the dependency checks between
  the context definition and other parameter sets for a profile. Parameters
  other than the context definition (auth,qos,dns) are in effect an extension
  of the context definition. It is upto the caller & users of the API's to
  check for the dependency between the different parameter subsets and use 
  then accordingly.

  h) For L4, The PDP registry APIs use critical section to serialize access
 to the PDP registry and the efs file system. So after the first call,
 subsequent calls to the PDP registry will be blocked untill first call
 completes. The PDP Registry uses the rex critical sections to prevent
 multiple processes to access the registry or the efs file system.
  
Copyright (c) 2006-2014 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.
===========================================================================*/


/*===========================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/3gpp/dsprofile/src/dsumtspdpregint.c#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
02/20/13   nd      Skip creation of default emb/tethered profile num file if
                   not present in EFS.
07/27/12   ss      Fix to make embedded/tethered profile_num_file as an item 
                   file and use appropriate efs apis to read/write. 
06/27/12   kv      Support for mode T in Apn bearer field.
05/09/12   nd      Memory reduction effort in 3gpp.
09/27/11   nd      Fixed compiler warnings.
09/13/11   ttv     Changes to add src_stat_desc as profile parameter.
07/27/11   ttv     Added changes required for source statistics descriptor
                   addition.
05/31/11   rr      Added support for transient profiles
05/10/11   ttv     Fixed compilation warning.
04/20/11   ttv     Added changes for setting the default profile per family 
                   using set_default_profile_num_per_subs incase of dualsim.
04/13/11   ttv     Added changes to set pdp_type as ip based on 
                   v6_enabled_flag.
02/28/11   hs      Added changes to support APN class in profile
02/24/11   ttv     Fixed compilation warnings.
02/16/11   ttv     Added DSDS changes for consolidated profile family.
01/17/11   ss      Added support for Iface linger.
11/22/10   ttv     Added support for DSDS.
10/01/10   sa      Set default type to IPV4V6 only if IPV6 is enabled.
08/31/10   sa      Added support for default pdp type as IPv4v6.
02/07/10   ss      Reset string type PDP profile parameters before updating new 
                   value.
06/28/10   ss      Fixed compiler warnings.
06/01/10   ss      Compiler warning fix.
05/24/10   asn     Remove call to efs_sync
05/17/10   hs      Added APN disable and PDN inactivity feature support.
4/28/10    ars     Removed for loop in profile registry init that sets all 
                   profiles as valid upon bootup.
04/04/10   sa      Fixed reset issue for ds_umts_reset_pdp_profile_tft
                   _all_info_internal().
11/03/09   vk      Removed restriction that default sockets profile cannot be 
                   IPv6
11/02/09   asn     Fix bug in Profile number checking
10/24/09   asn     Removed FEATURE_DATA_RM_NET
10/19/09   ars     Replaced STD_SIZEOF as it was not defined for Linux builds
10/12/09   vk      PDP profile changes for IPv4v6 support
09/30/09   asn     Consolidation of Families
09/23/09   vd      Added support for LTE Qos.
09/22/09   vrk     Merged LTE changes.
05/19/09   vk      Defeaturization of PDP registry for CMI
12/11/08   vk      Fixed bug where socks call profile number was not being 
                   written correctly to EFS.
04/04/08   vd      Lint Fixes.
02/20/08   vrk     Fixed a bug for IMS flags by passing pointer to efs_write 
11/15/07   asn     Added support for new feature MT-PDP terminating in TE
06/15/07   aw      Added support for PCSCF Address discovery using DHCP.
05/01/07   aw      Added support for IM CN Flag.
02/15/07   aw      Removed the banned string APIs.
11/06/06   vk      Added function ds_umts_pdp_itoa() to remove dependency on 
                   ATCOP.
08/16/06   aw      Fixed the errors with FEATURE_PROFILE_OTAP_ENHANCEMENTS
                   turned on.
07/26/06   rsl     Changes relating to using EFS2 as supposed to EFS1.
07/17/06   rr      Added a check to make sure that Default profile type cannot 
                   be set as IPV6.
07/17/06   aw      Changed cscf to pcscf.
07/14/06   aw      Fixed a bug in PDP Registry function where PDP Registry
                   is not inidialized if new profile registry is loaded.
06/13/06   aw      Added support for IMS cscf address flag.
01/20/06   aw      Moved all functions from dsumtspdpreg file to this internal
                   file. Now these are called by wrappers in dsumtspdpreg
                   wrapper functions instead of by applications directly.
12/02/05   rr      Added RMNET Profile Family
11/18/05   rr      Featurized context_valid variable in ds_umts_pdp_registry
                   _init to fix lint error when FEATURE_PROFILE_OTAP_
                   ENHANCEMENTS is defined.
11/18/05   rc      Set all the undefined Socket Profiles to Default Value if
                   FEATURE_PROFILE_OTAP_ENHANCEMENTS is not defined.
09/20/05   rc      Fixed bug where profile file was not closed before 
                   returning from a function in case of error.
07/20/05   rc      Fixed bug in ds_umts_set_pdp_read_only_flag() and
                   ds_umts_set_pdp_otap_enabled_flag() where address-of the
                   variable was not passed in memcpy(). 
07/18/05   rr      Always check the validity of file size in  
                   ds_umts_pdp_validate_file_content
05/11/05   rr      Fixed Lint errors/warnings.
05/03/05   rr      Set ds_umts_pdp_registry_init_flag to FALSE if 
                   initialization fails. 
04/20/05   rr      Fixed a typo in calls to ds_umts_set_pdp_profile_info
                   where parameters were swapped.
04/13/05   rr      Fixed string copies to ensure that they are NULL 
                   terminated. 
03/23/05   rr      Replaced arguments of memcpy with correct pointers
03/09/05   rc      Added support for read_only cache. All get*() operations
                   are done from the profile family cache table.
                   Added check to see if read_only_flag for the profile is
                   set before performing set*() operations.
02/10/05   rc      Changed type (to UTF16 format) and length of profile name.
                   Added get/set routines for read_only_flag, otap_enabled,
                   and otap_napid fields of the profile.
                   All changes were made under FEATURE_PROFILE_OTAP_ENHANCEMENTS.
                   Moved version field to be the first field of the structure.
12/06/04   rr      PDP registry initialization will set all the undefined 
                   Sockets profiles to default value.
11/24/04   rc      Added code to set default atcop profile. 
11/22/04   rc      Added support for separation of ATCOP and sockets profiles
06/04/04   rc      Fixed Lint errors/warnings.
03/12/04   vsk     set sockets call profile number to 1 if value at startup
                   is invalid.
                   code cleanup to remove tab chars.
01/27/04   vsk     mainline M-PDP support
08/05/03   ar      Add configuration metadata file.
07/23/03   rc      Included files msg.h and err.h.
07/11/03   ar      Unconditionally truncate EFS file during reset operation. 
06/26/03   ar      Added support for secondary context and traffic flow
                   template paramaters.
05/23/03   rc      Removed Compiler Warnings.
02/14/03   vsk     Removed the dependency between context definition and
                   other parameter sets for a profile.
02/10/03   vsk     Added support to store/retrieve minimum UTMS QOS (AT+CGEQMIN)
                   parameters
02/05/03   vsk     Updated the file header section and added comments about
                   synchronous access & importance of not using TASKLOCKS
01/24/03   vsk     fixed lint warnings/errors
01/17/03   vsk     fixed a return error in the function
                   ds_umts_get_pdp_profile_context_info_pdp_address
01/16/03   vsk     creation of file 
===========================================================================*/


/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

#include "datamodem_variation.h"
#include "customer.h"
#include "comdef.h"

#if defined(FEATURE_DATA_WCDMA_PS) || defined(FEATURE_GSM_GPRS) || defined(FEATURE_DATA_LTE)
#include <string.h>
#include "amssassert.h"
#include "msg.h"
#include "err.h"
#include "fs_public.h"
#include "fs_sys_types.h"
#include "fs_errno.h"
#include "dsumtspdpregint.h"
#include "ds_system_heap.h"

#include <stringl/stringl.h>
#include "dstaski.h"
#include "nv_items.h"
#include "ds_3gppi_utils.h"
#include "ps_sys.h"
#ifdef FEATURE_GOBI_MUXD_PDP_PROFILE_DIR
#include "dsatctab.h"
#endif /* FEATURE_GOBI_MUXD_PDP_PROFILE_DIR */

#ifdef FEATURE_DATA_MODEM_HEAP
#include "modem_mem.h"
#endif /* FEATURE_DATA_MODEM_HEAP */

#ifdef FEATURE_DUAL_SIM
#include "dsumts_subs_mgr.h"
#include "ds3gmgr.h"
#endif /* FEATURE_DUAL_SIM */
#include "rex.h"

/*===========================================================================

                      PUBLIC DATA DECLARATIONS

===========================================================================*/
/*------------------------------------------------------------------
    Macro for acquiring a Tasklock for cache access
-------------------------------------------------------------*/
  #define DS_UMTS_PDP_TASKLOCK_NON_L4() {}

/*------------------------------------------------------------------
  Macro for freeing a tasklock after cache access
------------------------------------------------------------------*/
  #define DS_UMTS_PDP_TASKFREE_NON_L4() {}

/*===========================================================================
            LOCAL DEFINITIONS AND DECLARATIONS FOR MODULE
This section contains local definitions for constants, macros, types,
variables and other items needed by this module.

===========================================================================*/


/*---------------------------------------------------------------------------
 Macros to perform simple checks on input paramaters in the access functions
 Since the checks are performed in each API, these macros are helpfull
 
NOTE : THE MACROS RETURN FROM THE CALLING FUNCTION IF AN ERROR OCCURS
---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
  Macro to check if we are initialized
---------------------------------------------------------------------------*/
#define DS_UMTS_PDP_CHECK_INIT_FLAG() {\
  if( !ds_umts_pdp_registry_init_flag )\
    {\
      DS_3GPP_MSG3_ERROR("PDP registry not initialized :",0,0,0);\
      return DS_UMTS_PDP_NOT_INITIALIZED;\
    }\
  }

 /*---------------------------------------------------------------------------
   Macro for transience_flag reading.
 ---------------------------------------------------------------------------*/
#define DS_UMTS_PDP_PROFILE_IS_PERSISTENT( profile_ptr ) ( \
    (((profile_ptr)->transience_flag) == FALSE) \
 )

/*---------------------------------------------------------------------------
  Macro to check if the TFT filter identifier is valid
---------------------------------------------------------------------------*/
#define  DS_UMTS_PDP_CHECK_FILTER_ID(filter_id) {\
  if( (filter_id == 0) || \
      (filter_id > DS_UMTS_MAX_TFT_PARAM_SETS) )\
    {\
      DS_3GPP_MSG3_ERROR( "Invalid filter identifier:%d ",filter_id,0,0);\
      return DS_UMTS_PDP_INVALID_FILTER_ID;\
    }\
  }

/*---------------------------------------------------------------------------
  Macro to check if output buffer length is valid 
---------------------------------------------------------------------------*/
#define DS_UMTS_PDP_CHECK_OUT_BUFF_LEN(val,max) {\
  if( max > val ) \
    {\
      DS_3GPP_MSG3_ERROR("out buffer parm short :V:%d M:%d",val,max,0);\
      return DS_UMTS_PDP_INSUFFICIENT_OUTPUT_BUFFER;\
    }\
  }

/*---------------------------------------------------------------------------
  Macro to check if input buffer length is valid
---------------------------------------------------------------------------*/
#define DS_UMTS_PDP_CHECK_IN_BUFF_LEN(val,max) {\
  if( val > max ) \
    {\
      DS_3GPP_MSG3_ERROR("Input buff > Max allowed V:%d M :%d",val,max,0);\
      return DS_UMTS_PDP_INPUT_BUFFER_LEN_INVALID;\
    }\
  }



/*---------------------------------------------------------------------------
  Macro to check if the profile's read_only_flag is set.
---------------------------------------------------------------------------*/
#define DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_num,subs_id,read_only_flag) {\
  ds_umts_pdp_profile_status_etype ro_status;\
  ro_status = ds_umts_get_pdp_profile_is_read_only_per_subs(profile_number,\
                                                 subs_id,\
                                                &read_only_flag);\
  if(ro_status != DS_UMTS_PDP_SUCCESS )\
  {\
    return ro_status;\
  }\
  if(read_only_flag )\
  {\
    DS_3GPP_MSG3_HIGH("Read-only Profile:%d.Unable to set.",\
      profile_number, 0, 0);\
    return DS_UMTS_PDP_READ_ONLY_FLAG_SET;\
  }\
}



/*---------------------------------------------------------------------------
  file name and directory name definitions for accessing EFS
  PDP profiles are stored as files in the Embedded File System (EFS)
  under the "pdp_profiles" directory
  profiles directory name          : pdp_profiles
  ATCOP profiles directory name    : atcop_profiles
  Sockets profiles directory name  : sockets_profiles
  profile file names      : profileXY  where XY represents the profile number
  for.e.g, profile1,profile2,profile3,.....profile10,profile11....
  sockets call file name  : socks_call_prof_file_num 
  configuration file name : config 
  
  API uses profile numbers as input parameters. The profile nummbers are 
  mapped to the profile file names e.g., profile number1 is stored in
  the file "profile1" and profile number '12' is stored in "profile12"
---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
  Directory name for PDP profiles Config File in EFS 
---------------------------------------------------------------------------*/
#define DS_UMTS_PDP_PROFILE_DIR_NAME "pdp_profiles"

#define DS_UMTS_SUB1_DIR_NAME "Subs1"
#define DS_UMTS_SUB2_DIR_NAME "Subs2"
#ifdef FEATURE_TRIPLE_SIM
#define DS_UMTS_SUB3_DIR_NAME "Subs3"
#endif
/*---------------------------------------------------------------------------
  Length of the profile directory name "pdp_profiles" + _sub_number + '/'
---------------------------------------------------------------------------*/
#define DS_UMTS_PDP_PROFILE_DIR_NAME_LEN 13

/*---------------------------------------------------------------------------
  Length of sub directory name "subX"
---------------------------------------------------------------------------*/
#define DS_UMTS_SUB_DIR_NAME_LEN 6


#define DS_UMTS_PDP_PROFILE_SUB_DIR_NAME_LEN (DS_UMTS_SUB_DIR_NAME_LEN + \
                                         DS_UMTS_PDP_PROFILE_DIR_NAME_LEN + 1)
/*---------------------------------------------------------------------------
  Max length of the Consolidated profile directory name. This
  is the maximum path length for the directory name. If
  a profile is added whose directory name is longet than this, then this
  needs to be updated.

  "pdp_profiles"  = 12
---------------------------------------------------------------------------*/
#define DS_UMTS_PDP_PROFILE_MAX_DIR_NAME_LEN 50

#ifdef FEATURE_GOBI_MUXD_PDP_PROFILE_DIR

#define UNDP_PDP_PROFILE_CONSL_DIR_NAME_START "pdp_profiles"
#define UNDP_PDP_PROFILE_CONSL_DIR_NAME_INDEX 12
#define UNDP_CONSL_PROFILE_EN_MASK  0xFF00

#define DS_UMTS_PDP_PROFILE_CONSL_DIR_NAME undp_get_muxd_pdp_profile_dirname()
#define DS_UMTS_PDP_PROFILE_CONSL_DIR_NAME_LEN 14

#endif /* FEATURE_GOBI_MUXD_PDP_PROFILE_DIR */

/*---------------------------------------------------------------------------
  File name for PDP profiles in EFS : XY will be replaced with the profile
  number during access
---------------------------------------------------------------------------*/
#define DS_UMTS_PDP_PROFILE_FILE_NAME "profileXY"

/*---------------------------------------------------------------------------
  offset in "profileXY" where the profile number starts( index runs from 0 
  to N-1
  X - index0, Y - index1
---------------------------------------------------------------------------*/
#define DS_UMTS_PDP_PROFILE_FILE_NAME_NUM_INDEX0 7

/*---------------------------------------------------------------------------
  Length of the profile file name "profilexy" where xy will be replaced by 
  1,2, 10,11 ... 
---------------------------------------------------------------------------*/
#define DS_UMTS_PDP_PROFILE_FILE_NAME_LEN  9

/*---------------------------------------------------------------------------
  File name which stores the profile number used for embedded call
---------------------------------------------------------------------------*/
#define DS_UMTS_PDP_PROFILE_NUM_FOR_EMBEDDED_FILE_NAME "embedded_call_prof_num"

/*---------------------------------------------------------------------------
  Length of the embedded call profile number file name "embedded_call_prof_num" 
---------------------------------------------------------------------------*/
#define DS_UMTS_PDP_EMBEDDED_CALL_PROF_NUM_FILE_NAME_LEN  22

/*---------------------------------------------------------------------------
  File name which stores default Profile number used for tethered call
---------------------------------------------------------------------------*/
#define DS_UMTS_PDP_PROFILE_NUM_FOR_TETHERED_FILE_NAME "tethered_call_prof_num"

/*---------------------------------------------------------------------------
  Length of the tethered Profile number file name "tethered_call_prof_num" 
---------------------------------------------------------------------------*/
#define DS_UMTS_PDP_TETHERED_CALL_PROF_NUM_FILE_NAME_LEN  22

/*---------------------------------------------------------------------------
  Macro to allocate memory for local pdp profile.Used for temporary operations
  in PDPREG internal functions.
  ---------------------------------------------------------------------------*/
#define DS_UMTS_PDP_ALLOC_LOCAL_PDP_PROFILE(PTR) {\
  PTR = (ds_umts_pdp_profile_type *)ds_system_heap_mem_alloc(\
                                            sizeof(ds_umts_pdp_profile_type));\
  if(NULL == PTR)\
  {\
    MSG_ERROR("Memory allocation failure for client:%d, size:%d",\
               MODEM_MEM_CLIENT_DATA,\
               sizeof(ds_umts_pdp_profile_type),0);\
    return DS_UMTS_PDP_FAIL;\
  }\
  memset((void*)PTR, 0x0, sizeof(ds_umts_pdp_profile_type));\
}

/*---------------------------------------------------------------------------
  Macro to free local pdp profile memory.Used for temporary operations
  in PDPREG internal functions.
---------------------------------------------------------------------------*/
#define DS_UMTS_PDP_FREE_LOCAL_PDP_PROFILE(PTR) {\
  ds_system_heap_mem_free(PTR);\
}

/*---------------------------------------------------------------------------
  Size of configuration file content
---------------------------------------------------------------------------*/
#define DS_UMTS_PDP_CONFIG_FILE_SIZE  ( sizeof (byte) + sizeof (byte) + \
                                        sizeof(boolean) +\
                                       ( sizeof (ds_umts_pdp_profile_type) - \
                                        DS_UMTS_PDP_PROFILE_CONTEXT_OFFSET))


static char ds_umts_pdp_profile_dir_name[DS_UMTS_PDP_PROFILE_DIR_NAME_LEN+1];

static char ds_umts_pdp_profile_sub_dir_name[DS_UMTS_PDP_PROFILE_DIR_NAME_LEN + 
                                             DS_UMTS_SUB_DIR_NAME_LEN + 1];

static char ds_umts_pdp_profile_sub_dir_name_only[DS_UMTS_PDP_PROFILE_DIR_NAME_LEN + 
                                             DS_UMTS_SUB_DIR_NAME_LEN];

/*---------------------------------------------------------------------------- 
  exact replica of umts_pdp_profile_dir_name minus the backslash
  ----------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
  EFS file name for the profiles "profileXY". Allocate an additional 
static char ds_umts_pdp_profile_sub_dir_name_only[DS_UMTS_PDP_PROFILE_DIR_NAME_LEN];
  character for the ASCIZ terminator
---------------------------------------------------------------------------*/
static char ds_umts_pdp_profile_file_name[DS_UMTS_PDP_PROFILE_FILE_NAME_LEN+1];

/*---------------------------------------------------------------------------
  Fully qualified EFS file name for the storing profile fullly qualified name
  "profileXY" for all profile types

  The fully qualified file name consists of the high level directory 
  pdp_profiles. Under the base directory there are directories for
  consolidated profile family. 
  Under each directory, there are profile files, one for each profile names
  profileXY where XY is the profile number.
  
  e.g., "pdp_profiles/profile3
  Note: This length is dependent on DS_UMTS_PDP_PROFILE_MAX_DIR_NAME_LEN. If
  a longer length is required, the dir name length as defined by above 
  variable needs to be increased accordingly.
---------------------------------------------------------------------------*/
char ds_umts_pdp_fully_qual_profile_file_name[\
              DS_UMTS_PDP_PROFILE_MAX_DIR_NAME_LEN + \
              1 + \
              DS_UMTS_PDP_PROFILE_FILE_NAME_LEN + \
              1 ];

/*---------------------------------------------------------------------------
  Fully qualified EFS file name for the embedded call profile number file
  i.e., "pdp_profiles/embedded_call_prof_num"
---------------------------------------------------------------------------*/
static char ds_umts_pdp_fully_qual_embedded_call_file_name[\
              DS_UMTS_PDP_PROFILE_DIR_NAME_LEN + \
              1 + \
              DS_UMTS_PDP_EMBEDDED_CALL_PROF_NUM_FILE_NAME_LEN + \
              1 ];

/*---------------------------------------------------------------------------
  Fully qualified EFS file name for the tethered call profile number file
  i.e., "pdp_profiles/tethered_call_prof_num"
---------------------------------------------------------------------------*/
static char ds_umts_pdp_fully_qual_tethered_call_file_name[\
              DS_UMTS_PDP_PROFILE_DIR_NAME_LEN + \
              1 + \
              DS_UMTS_PDP_TETHERED_CALL_PROF_NUM_FILE_NAME_LEN + \
              1 ];

/*---------------------------------------------------------------------------
  Flag which stores the status of PDP registry initialization
---------------------------------------------------------------------------*/
static boolean ds_umts_pdp_registry_init_flag = FALSE;

/*-------------------------------------------------------------------------- 
  Enum to track the failure cause for pdp_reg_init failure
  --------------------------------------------------------------------------*/
static ds_umts_pdp_reg_failure_e ds_umts_pdp_reg_init_fail_cause;
/*---------------------------------------------------------------------------
  Scratchpad variable : Default PDP Profile storage
---------------------------------------------------------------------------*/
static ds_umts_pdp_profile_type   ds_umts_pdp_default_profile;

/*---------------------------------------------------------------------------
  Scratchpad variable : Default PDP Profile storage
---------------------------------------------------------------------------*/
static ds_umts_pdp_profile_type   ds_umts_pdp_default_tethered_profile;

/*---------------------------------------------------------------------------
  Scratchpad storage to access profile data
---------------------------------------------------------------------------*/
static ds_umts_pdp_profile_type*  ds_umts_pdp_local_profile_ptr = NULL;

/*---------------------------------------------------------------------------
  Cached value of profile number for embedded calls.  This value is set
  during library initialization and anytime the embedded profile number
  is updated.  Reads for the embedded profile number will return this
  value.
---------------------------------------------------------------------------*/
static byte  cached_embedded_call_profile_number[DS_SUBSCRIPTION_MAX] = {0};

/*---------------------------------------------------------------------------
  Cached value of profile number for tethered calls.  This value is set
  during library initialization and anytime the tethered profile number
  is updated.  Reads for the tethered profile number will return this
  value.
---------------------------------------------------------------------------*/
static byte  cached_tethered_call_profile_number[DS_SUBSCRIPTION_MAX]  = {0};

/*---------------------------------------------------------------------------
  Cache to store profile data Consolidated profiles.
  Profile families are represnted by first subscript and profiles by second
  subscript.

  Note: the profile numbers start from array subscript 1 and not 0. The
  profile 0 does not exist and ds_umts_pdp_profile_cache[][0] is unused.

  The first subscript in the array points to the profile family.
  Family CONSL -> array location 0

  Profiles start at array location 1. 
  profile 1 -> array location 1
  profile 2 -> array location 2
  ..................
  profile DS_UMTS_MAX_PDP_PROFILE_NUM -> array location DS_UMTS_MAX_PDP_PROFILE_NUM

  So ds_umts_pdp_profile_cache[0][2] will refer to profile number 2 in
  Consolidated profile family.
  So ds_umts_pdp_profile_cache[0][0] cannot be used as profile number 0 is
  not defined.
---------------------------------------------------------------------------*/
ds_umts_pdp_profile_type
 *ds_umts_pdp_profile_cache_ptr[DS_SUBSCRIPTION_MAX][DS_UMTS_MAX_PDP_PROFILE_NUM+1] = 
  {NULL};

/*---------------------------------------------------------------------------
 Macro to dereference a profile from the profile cache.
---------------------------------------------------------------------------*/
#define DS_UMTS_PDP_PROFILE_CACHE(s,j) \
        ds_umts_pdp_profile_cache_ptr[s][j]


#define DS_UMTS_PDP_PROFILE_CACHE_SET(s,j,ds_umts_pdp_profile_ptr) \
        ds_umts_pdp_profile_cache_ptr[s][j] = ds_umts_pdp_profile_ptr;

#ifdef FEATURE_GOBI_MUXD_PDP_PROFILE_DIR
static byte  undp_muxd_consl_pdp_profile_dir_name_len = 0;
static char  undp_muxd_consl_pdp_profile_file_dir_name[DS_UMTS_PDP_PROFILE_CONSL_DIR_NAME_LEN+1];
#endif 

/*---------------------------------------------------------------------------
 Number of persistent profiles. NV vonfigurable value.
---------------------------------------------------------------------------*/
static uint8 ds_3gpp_num_persistent_profiles;
/*===========================================================================
                    Forward declarations/prototypes
===========================================================================*/

static byte * ds_umts_pdp_itoa
(
  uint32 v,         /*  Value to convert to ASCII     */
  byte *s,          /*  Pointer to string for result  */
  uint16 r          /*  Conversion radix              */
);


/*===========================================================================
FUNCTION DS_UMTS_PDP_SET_DS_UMTS_PDP_PROFILE_SUB_DIR_NAME

DESCRIPTION
  This function initializes the PDP profile sub dir
PARAMETERS 
  None

DEPENDENCIES 

RETURN VALUE 

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype ds_umts_pdp_profile_set_sub_dir_name
(
   ds_umts_pdp_subs_e_type  subs_id
);

/*===========================================================================
                   INTERNAL FUNCTION DEFINITIONS
===========================================================================*/
/*===========================================================================
FUNCTION DS_UMTS_PDP_EFS_READ

DESCRIPTION
This function invokes efs_read and checks to make sure the number of bytes
read matches the requested number of bytes.

PARAMETERS
fd - file descriptor of the file to be read
*buf - pointer to the bytes of data read
nbyte - number of bytes to be read

DEPENDENCIES
  None

RETURN VALUE
FALSE - Read nbyte mismatch with efs_read return value.
TRUE - SUCCESS

SIDE EFFECTS
  None
===========================================================================*/
static boolean ds_umts_pdp_efs_read
(
  int fd,
  void *buf,
  uint32 nbyte
)
{   
  fs_ssize_t fs_res;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*---------------------------------------------------------------------- 
  efs_read returns number of bytes read if successful, -1 indicates an
  error and 0 means end of file has been reached.
  ----------------------------------------------------------------------*/
  fs_res = efs_read (fd, buf, (fs_size_t)nbyte);
  if (fs_res == -1) 
  {
    DS_3GPP_MSG3_ERROR("efs_read failed for fd=%d errno=%d", fd, efs_errno, 0);
    return FALSE;
  }
  /*lint -e737*/
  else if (fs_res != nbyte)
  {
    DS_3GPP_MSG3_ERROR("efs_read read wrong num of bytes=%d errno=%d", 
              fs_res, efs_errno, 0);
    return FALSE;
  }
  /*lint +e737*/
  return TRUE;
}

/*===========================================================================
FUNCTION DS_UMTS_PDP_EFS_WRITE

DESCRIPTION
This function invokes efs_write and checks to make sure the number of bytes
written matches the requested number of bytes.

PARAMETERS
fd - file descriptor of the file from which to write
*buf - pointer to the bytes of data written
nbyte - number of bytes written

DEPENDENCIES
  None

RETURN VALUE
FALSE - nbyte mismatch with efs_write return value.
TRUE - SUCCESS

SIDE EFFECTS
  None
===========================================================================*/
static boolean ds_umts_pdp_efs_write
(
  int fd,
  const void *buf,
  uint32 nbyte
)
{
  fs_ssize_t fs_res;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /*---------------------------------------------------------------------- 
  efs_write returns number of bytes written if successful, -1 indicates an
  error and 0 means the file system is full.
  ----------------------------------------------------------------------*/
  fs_res = efs_write (fd, buf, (fs_size_t)nbyte);
  if (fs_res == -1) 
  {
    DS_3GPP_MSG3_ERROR("efs_write failed for fd=%d errno=%d", fd, efs_errno, 0);
    return FALSE;
  }
  /*lint -e737*/
  else if (fs_res != nbyte)
  {
    DS_3GPP_MSG3_ERROR("efs_write wrote wrong num of bytes=%d errno=%d", 
              fs_res, efs_errno, 0);
    return FALSE;
  }
  /*lint +e737*/

  return TRUE;
}

/*===========================================================================

FUNCTION DS_UMTS_PDP_ITOA

DESCRIPTION
  This function converts an integer to ASCII, using a specified
  radix. The resulting string is null terminated.

DEPENDENCIES
  The function does not work correctly if r < 2 or r > 36.
  (r is the radix argument.)

  The length of the returned string depends on the value and the
  radix.  The worst case is the value 0xFFFFFFFF and a radix of 2,
  which returns 33 bytes (32 one's and the null terminator.)
  The array pointed to by 's' must be large enough for the returned
  string.

RETURN VALUE
  A pointer to the null terminator is returned.

SIDE EFFECTS
  None
===========================================================================*/
static byte * ds_umts_pdp_itoa
(
  uint32 v,         /*  Value to convert to ASCII     */
  byte *s,        /*  Pointer to string for result  */
  uint16 r          /*  Conversion radix              */
)
{
  byte buf[33], c;
  int n;

  n = sizeof(buf) - 1;

  buf[n] = '\0';


  do
  {
    c = (byte) (v % r);

    if (n <= 0)
    {
      DS_3GPP_MSG3_HIGH ("OVERFLOW", 0, 0, 0);
      break;
    }

    buf[--n] = (byte) ( (c > 9) ? c + 'A' - 10 : c + '0');

  } while ( (v /= r) > 0);

  while ( (*s++ = buf[n++]) != 0)
      ;

  return (s-1);
} /*  ds_umts_pdp_itoa */
  
/*===========================================================================
FUNCTION DS_UMTS_PDP_CHECK_PROFILE_NUMBER

DESCRIPTION
This function checks the validity of profile number. It checks if the profile
number and profile family are in valid range and profile family is defined.
PARAMETERS
  profile_number - profile number passed. This profile number is a two byte
                   number. First byte is the profile family and the second
                   byte is the actual profile number.

DEPENDENCIES
  None

RETURN VALUE
  ds_umts_pdp_profile_status_etype

SIDE EFFECTS
  None
===========================================================================*/
static ds_umts_pdp_profile_status_etype ds_umts_pdp_check_profile_number
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id
)
{
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ds_umts_pdp_profile_type *profile_cache_ptr = NULL;

  if( (profile_number == 0) ||
      (profile_number > DS_UMTS_MAX_PDP_PROFILE_NUM) )
  {
    DS_3GPP_MSG1_ERROR("Invalid profile number:%d", profile_number);
    return DS_UMTS_PDP_INVALID_PROFILE_NUM;
  }

  if( (subs_id < DS_UMTS_PDP_SUBSCRIPTION_NONE) ||
      (subs_id >= DS_UMTS_PDP_SUBSCRIPTION_MAX -1) )
  {
    DS_3GPP_MSG0_ERROR("Subscription ID is invalid" );
    return DS_UMTS_PDP_INVALID_PROFILE_NUM;
  }

  /*------------------------------------------------------------------------ 
    Also check if the cache is allocated
    ------------------------------------------------------------------------*/

  profile_cache_ptr = DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);

  if (profile_cache_ptr == NULL)
  {
    DS_3GPP_MSG0_ERROR("Cache ptr is not allocated");
    return DS_UMTS_PDP_INVALID_PROFILE_NUM;
  }

  return DS_UMTS_PDP_SUCCESS;
}   

#ifdef FEATURE_PROFILE_OTAP_ENHANCEMENTS
/*===========================================================================
FUNCTION DS_UMTS_PDP_STRING_TO_WIDE_STRING

DESCRIPTION
This function converts a single-byte string into a wide string 

PARAMETERS
  in_str  -  Pointer to null terminated string comprised of single-byte 
             characters 
  out_str -  Pointer to destination buffer to receive the wide string  
  nbytes  -  Size (in bytes) of out_str buffer. 
             If this is 0, this function does not do any conversion. 
             It returns out_str as is without any changes

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
static void ds_umts_string_to_wide_string
(
  const byte *in_str, 
  PACKED uint16 *out_str, 
  int nbytes
)
{
  int   i;
  int   nFilled;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if (nbytes >= sizeof(uint16))
  {
    /*---------------------------------------------------------------------- 
      Get to the end of the input string.
    ----------------------------------------------------------------------*/
    for (i = 0; in_str[i]; i++);

    /*---------------------------------------------------------------------- 
      We are expanding out, so make certain we dont over-index...
    ----------------------------------------------------------------------*/
    nFilled = i * sizeof(uint16);

    if (nFilled >= nbytes) {
      i = (nbytes / sizeof(uint16)) - 1;
      out_str[i--] = 0;
      nFilled = nbytes;
    }
    /*---------------------------------------------------------------------- 
      Now, do a byte to uint16 conversion.
    ----------------------------------------------------------------------*/
    while (i >= 0) {
      out_str[i] = (uint16)((unsigned char)in_str[i]);
      i--;
    }
  }
}
#endif

/*===========================================================================
FUNCTION DS_UMTS_PDP_SET_QUALIFIED_PROFILE_FILE_NAME

DESCRIPTION
  This function sets the PDP qualified file name.
  
PARAMETERS
  uint16 profile_number
  
DEPENDENCIES 
  None

RETURN VALUE 
  boolean

SIDE EFFECTS 
  None
===========================================================================*/
boolean ds_umts_pdp_set_qualified_profile_file_name
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id
)
{
  byte                            profile_num_lower_byte;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if(ds_umts_pdp_profile_set_sub_dir_name(subs_id) != DS_UMTS_PDP_SUCCESS )
  {
    return FALSE;
  }
  /*-------------------------------------------------------------------------
     profile number passed in is a combination of profile family and actual
     profile number. The higher byte is profile family and lower byte is
     profile number. So get the actual profile number to append inside the 
     file name by getting the lower byte from the profile number 
  -------------------------------------------------------------------------*/
  profile_num_lower_byte = profile_number;

                                                                      
  /*lint -e534 ignoring return code from function*/                       
  strlcpy(ds_umts_pdp_fully_qual_profile_file_name,                  
              ds_umts_pdp_profile_sub_dir_name,
              sizeof(ds_umts_pdp_fully_qual_profile_file_name) );             

  ds_umts_pdp_itoa(profile_num_lower_byte,                                           
         ((byte*)&ds_umts_pdp_profile_file_name[DS_UMTS_PDP_PROFILE_FILE_NAME_NUM_INDEX0]),
         10);                                                                  

  strlcat(ds_umts_pdp_fully_qual_profile_file_name,
              ds_umts_pdp_profile_file_name,
              sizeof(ds_umts_pdp_fully_qual_profile_file_name));
  /*lint +e534*/                                                          
  return TRUE;
}

/*===========================================================================
FUNCTION DS_UMTS_UPDATE_EMBEDDED_CALL_PROF_NUM

DESCRIPTION
  This function updates the profile number in the "embedded_call_prof_num" file
  with the value passed.  The profile number is cached to support reads when
  EFS read operations are disabled.

PARAMETERS
 profile_number : profile number to be written to the file
  
DEPENDENCIES 
  None

RETURN VALUE 
  TRUE       : profile number updated successfully
  FALSE      : Error in updating profile number

SIDE EFFECTS 
  None
===========================================================================*/
static boolean ds_umts_update_embedded_call_prof_num
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id
)
{
  int                       result;
  byte                      profile_num; /* Holds lower 8 bits of profile number*/
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
   profile_num = (byte) profile_number;

  if(ds_umts_pdp_profile_set_sub_dir_name(subs_id) != DS_UMTS_PDP_SUCCESS )
  {
    return FALSE;
  }
  /*-------------------------------------------------------------------------
    Init the fully qualified profile file name to pdp_profiles directory
  -------------------------------------------------------------------------*/
  /*lint -e534 ignoring return code from function*/
  if(strlcpy( ds_umts_pdp_fully_qual_embedded_call_file_name,
                  ds_umts_pdp_profile_sub_dir_name,
                  sizeof(ds_umts_pdp_fully_qual_embedded_call_file_name) ) >=
     (int) sizeof(ds_umts_pdp_fully_qual_embedded_call_file_name) )
  {
    return FALSE;
  }

  strlcat( ds_umts_pdp_fully_qual_embedded_call_file_name,
               DS_UMTS_PDP_PROFILE_NUM_FOR_EMBEDDED_FILE_NAME,
               sizeof(ds_umts_pdp_fully_qual_embedded_call_file_name) );
  /*lint +e534*/    
  /*-------------------------------------------------------------------------
    Open the file for write 
  -------------------------------------------------------------------------*/
  result = efs_put(ds_umts_pdp_fully_qual_embedded_call_file_name,
                   (void *)(&profile_num),
                   sizeof(byte),
                   O_CREAT | O_TRUNC | O_RDWR,
                   DEFFILEMODE);
  if ( result == -1 )
  {
    DS_3GPP_MSG3_ERROR("efs_put failed fd=%d errno=%d.", result, efs_errno, 0);
    return FALSE;
  }

  /*-------------------------------------------------------------------------
    Set the cached embedded call profile_number for later reads
  -------------------------------------------------------------------------*/
  cached_embedded_call_profile_number[subs_id] = (byte)profile_number;
  return TRUE;  
}

/*===========================================================================
FUNCTION DS_UMTS_UPDATE_DEFAULT_TETHERED_PROF_NUM

DESCRIPTION
  This function updates the profile number in the "tethered_call_prof_num" file
  with the value passed.  The profile number is cached to support reads when
  EFS read operations are disabled.

PARAMETERS
  profile_number : profile number to be written to the file
  
DEPENDENCIES 
  None

RETURN VALUE 
  TRUE       : profile number updated successfully
  FALSE      : Error in updating profile number

SIDE EFFECTS 
  None
===========================================================================*/
static boolean ds_umts_update_default_tethered_prof_num
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id
)
{
  int                       result;
  byte                      profile_num; /* Holds lower 8 bits of profile number*/
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  profile_num = (byte) profile_number;

  if(ds_umts_pdp_profile_set_sub_dir_name(subs_id) != DS_UMTS_PDP_SUCCESS )
  {
    return FALSE;
  }
  /*-------------------------------------------------------------------------
    Init the fully qualified profile file name to pdp_profiles directory
  -------------------------------------------------------------------------*/
  /*lint -e534 ignoring return code from function*/
  if( strlcpy( ds_umts_pdp_fully_qual_tethered_call_file_name,
                   ds_umts_pdp_profile_sub_dir_name,
                   sizeof(ds_umts_pdp_fully_qual_tethered_call_file_name) ) >=
      (int) sizeof(ds_umts_pdp_fully_qual_tethered_call_file_name) )
  {
    return FALSE;
  }

  strlcat( ds_umts_pdp_fully_qual_tethered_call_file_name,
               DS_UMTS_PDP_PROFILE_NUM_FOR_TETHERED_FILE_NAME,
               sizeof(ds_umts_pdp_fully_qual_tethered_call_file_name) );
  /*lint +e534*/    

  /*-------------------------------------------------------------------------
    Open the file for write 
  -------------------------------------------------------------------------*/
  result = efs_put(ds_umts_pdp_fully_qual_tethered_call_file_name,
                   (void *)(&profile_num),
                   sizeof(byte),
                   O_CREAT | O_TRUNC | O_RDWR,
                      DEFFILEMODE);
  if ( result == -1)
  {
    DS_3GPP_MSG3_ERROR("efs_open failed fd=%d errno=%d.", result, efs_errno, 0);
    return FALSE;
  }

  /*-------------------------------------------------------------------------
    Set the cached tethered call profile_number for later reads
  -------------------------------------------------------------------------*/
  cached_tethered_call_profile_number[subs_id] = (byte)profile_number;
  
  return TRUE;  
}

/*===========================================================================
FUNCTION DS_UMTS_PDP_INIT_CACHE_DEFAULT_PROFILE_INFO

DESCRIPTION
  This function updates the cache with default profile information.
  
PARAMETERS
  None

DEPENDENCIES 
  ds_umts_pdp_profile_cache_ptr should be set
  
RETURN VALUE 
  TRUE               : the operation completed successfully
  FALSE              : Fails to initialize cache.
SIDE EFFECTS 
  None
===========================================================================*/
static boolean ds_umts_pdp_init_cache_default_profile_info
(
  uint16                    profile_num,
  ds_umts_pdp_subs_e_type   subs_id
)
{
  ds_umts_pdp_profile_type *profile_data_ptr;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /*-----------------------------------------------------------------------
    Init the fully qualified profile file name to 
    "pdp_profiles" directory
  -----------------------------------------------------------------------*/
  if(ds_umts_pdp_set_qualified_profile_file_name(profile_num,subs_id) == FALSE)
    return FALSE;

  ds_umts_pdp_default_profile.context.pdp_context_number = profile_num;

#ifdef FEATURE_PROFILE_OTAP_ENHANCEMENTS
  ds_umts_string_to_wide_string(
       (const byte *)ds_umts_pdp_profile_file_name,
       (uint16 *)ds_umts_pdp_default_profile.profile_name,
       sizeof(ds_umts_pdp_default_profile.profile_name));
#else
  /*lint -e534 ignoring return code from function*/
  if(strlcpy((char *)ds_umts_pdp_default_profile.profile_name,
         ds_umts_pdp_profile_file_name,
         sizeof(ds_umts_pdp_default_profile.profile_name)) >=
         (int) sizeof(ds_umts_pdp_default_profile.profile_name) )
  {
    DS_3GPP_MSG3_ERROR("Copying profile name(strlcpy) failed ",0,0,0);
    return FALSE;
  }
  /*lint +e534*/
#endif

  /*--------------------------------------------------------------------------
    memcpy default profile data in cache.
  -------------------------------------------------------------------------*/
  profile_data_ptr = 
      DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_num);
  DS_UMTS_PDP_TASKLOCK_NON_L4();
  memscpy(profile_data_ptr, sizeof(ds_umts_pdp_profile_type), 
          &ds_umts_pdp_default_profile,
          sizeof(ds_umts_pdp_profile_type)); 
  DS_UMTS_PDP_TASKFREE_NON_L4();
  return TRUE;
}

/*===========================================================================
FUNCTION DS_UMTS_CACHE_PROFILE_FILE_DATA

DESCRIPTION
  This function updates the profile cache information from EFS
  with the profile file data.
  It directly overwrites the current values stored in the profile cache 
  including the profile name. fs_handle should be pointing to start of the
  file.

PARAMETERS
  profile_number  : profile number
  fs_handle       : file handle

DEPENDENCIES 
  None
  
RETURN VALUE 
  TRUE   :Success
  FALSE  :Failure
SIDE EFFECTS 
  None
===========================================================================*/
static ds_umts_pdp_profile_status_etype  ds_umts_cache_profile_file_data
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  int                       fs_handle 
) 
{
  ds_umts_pdp_profile_type         *profile_data_ptr;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if( ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
      DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return FALSE; 

  profile_data_ptr = 
      DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);

  /*-------------------------------------------------------------------------
    Read the context definition parameters directly in the cache
  -------------------------------------------------------------------------*/
  if (!ds_umts_pdp_efs_read(fs_handle,
                    (void *)profile_data_ptr,
                    sizeof(ds_umts_pdp_profile_type)))
  {
    DS_3GPP_MSG3_ERROR( "efs_read failed for %d errno = %d profile num %d",
                        fs_handle, efs_errno, profile_number );
    return FALSE;
  }
  if(!((profile_data_ptr->context).valid_flg))
  {
    /*-------------------------------------------------------------------------
      Reset profile to default.
    -------------------------------------------------------------------------*/
    if(!ds_umts_pdp_init_cache_default_profile_info(profile_number,subs_id))
    {
      DS_3GPP_MSG3_ERROR("Reset profile %d to default failed", profile_number, 0, 0);
      return FALSE;
    }
  }
  return TRUE;
}

/*===========================================================================
FUNCTION DS_UMTS_PDP_DELETE_FILE

DESCRIPTION
  This function deletes an efs file 
  
PARAMETERS
 None
  
DEPENDENCIES 
  None

RETURN VALUE 
  TRUE       : file deleted successfully
  FALSE      : Error in file deletion / check

SIDE EFFECTS 
  None
===========================================================================*/
static boolean ds_umts_pdp_delete_file
(
  char *file_path
)
{
  int                   result;

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  result = efs_unlink(file_path);

  if ( result == -1)
  {
    DS_3GPP_MSG3_ERROR("efs_unlink failed for fd=%d errno=%d.", result, efs_errno, 0);
    return FALSE;
  }

  return TRUE;
} /* ds_umts_pdp_delete_file */

/*===========================================================================
FUNCTION DS_3GPP_PROFILE_CACHE_DEALLOCATE_PROFILE_CACHE

DESCRIPTION
This function deallocates memory for profile cache 
 
DEPENDENCIES
  pointer to the profile cache structure

RETURN VALUE
  None
 
SIDE EFFECTS
  None
===========================================================================*/
static void ds_3gpp_profile_cache_deallocate_profile_cache
(
   ds_umts_pdp_profile_type *ds_umts_pdp_profile_ptr
)
{
  if (ds_umts_pdp_profile_ptr == NULL)
  {
    DS_3GPP_MSG0_ERROR("ds_umts_pdp_profile_ptr is NULL");
    return;
  }

  modem_mem_free(ds_umts_pdp_profile_ptr, MODEM_MEM_CLIENT_DATA_CRIT);
}   


/*===========================================================================
FUNCTION DS_UMTS_UPDATE_PDP_PROFILE_TO_UNDEFINED

DESCRIPTION
  This function undefines the context and profile parameters for a specific
  profile. The context "valid_flg" is set to FALSE and all parameters 
  are set to subscribed (0x0) values and the "valid_flg" in the related sets 
  are set to FALSE as well. 

PARAMETERS
 profile_number : profile number
  
DEPENDENCIES 
  None

RETURN VALUE 
  TRUE       : profile updated successfully
  FALSE      : Error in updating profile

SIDE EFFECTS 
  None
===========================================================================*/
static boolean ds_umts_update_pdp_profile_to_undefined
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id
)
{
  ds_umts_pdp_profile_type         *profile_data_ptr;

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    
  profile_data_ptr = DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);

  if (profile_data_ptr == NULL)
  {
    DS_3GPP_MSG3_ERROR("Cache Ptr is NULL, Entry does not exist",0,0,0);
    return FALSE;
  }
  /*--------------------------------------------------------------------------
    check valid flag and transient flag. If it is valid and 
    not transient then delete the file
  --------------------------------------------------------------------------*/
  /*-----------------------------------------------------------------------
    Delete from EFS only when file is present which means is 
    valid and persistent.
  -----------------------------------------------------------------------*/
  if ( (profile_data_ptr->context.valid_flg == TRUE) && 
       (profile_data_ptr->transience_flag == FALSE) )
  {
    DS_3GPP_MSG1_HIGH("Profile %d file is present in EFS delete it", 
                      profile_number);

    /*------------------------------------------------------------------------
      Set file name string with the corresponding profile number file which
      needs to be deleted
    ------------------------------------------------------------------------*/
    ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id);

    /*-----------------------------------------------------------------------
      Delete the file from EFS.
    -----------------------------------------------------------------------*/
    ds_umts_pdp_delete_file((char*)ds_umts_pdp_fully_qual_profile_file_name);
  }

  /*--------------------------------------------------------------------------
    memcpy default profile data in cache, Update the file data with default
    values.
  -------------------------------------------------------------------------*/
  ds_umts_pdp_init_cache_default_profile_info(profile_number,subs_id);

  //ds_3gpp_profile_cache_deallocate_profile_cache(profile_data_ptr);

  //DS_UMTS_PDP_PROFILE_CACHE_SET(subs_id, profile_number , NULL);

  return TRUE;
}

/*===========================================================================
FUNCTION DS_UMTS_PDP_CREATE_EMBEDDED_CALL_PROFILE_NUM_FILE

DESCRIPTION
  This function creates a new "embedded_call_profile_num" file and
  initlizes its contents
  
PARAMETERS
 None
  
DEPENDENCIES 
  None

RETURN VALUE 
  TRUE       : file initialized successfully
  FALSE      : Error in file creation / check

SIDE EFFECTS 
  None
===========================================================================*/
static boolean ds_umts_pdp_create_embedded_call_profile_num_file
(
   ds_umts_pdp_subs_e_type subs_id
)
{
  int                   result;
  byte                  prof_num = DS_3GPP_DEF_EMBEDDED_PROFILE;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if(ds_umts_pdp_profile_set_sub_dir_name(subs_id) != DS_UMTS_PDP_SUCCESS)
  {
    return FALSE;
  }
  /*-------------------------------------------------------------------------
    Init the fully qualified profile file name to 
    "pdp_profiles" directory
  -------------------------------------------------------------------------*/
  /*lint -e534 ignoring return code from function*/
  if( strlcpy( ds_umts_pdp_fully_qual_embedded_call_file_name,
                   ds_umts_pdp_profile_sub_dir_name,
                   sizeof(ds_umts_pdp_fully_qual_embedded_call_file_name) ) >=
      (int) sizeof(ds_umts_pdp_fully_qual_embedded_call_file_name) )
  {
    return FALSE;
  }

  strlcat( ds_umts_pdp_fully_qual_embedded_call_file_name,
               DS_UMTS_PDP_PROFILE_NUM_FOR_EMBEDDED_FILE_NAME,
               sizeof(ds_umts_pdp_fully_qual_embedded_call_file_name) );
  /*lint +e534*/
  /*-------------------------------------------------------------------------
    As the size of the contents of this file is only 1 byte it will be treated 
    as item file.
  -------------------------------------------------------------------------*/
  result = efs_put(ds_umts_pdp_fully_qual_embedded_call_file_name,
                            (void *)(&prof_num),
                   sizeof(byte),
                   O_CREAT | O_TRUNC | O_RDWR,
                   DEFFILEMODE);
  if ( result == -1)
  {
    DS_3GPP_MSG3_ERROR("efs_put failed for fd=%d errno=%d.", result, efs_errno, 0);
    return FALSE;
  }

  DS_3GPP_MSG3_HIGH("embedded_call_profile_num created",0,0,0);
  cached_embedded_call_profile_number[subs_id] = prof_num;
  return TRUE;
}

/*===========================================================================
FUNCTION DS_UMTS_PDP_CREATE_TETHERED_CALL_PROFILE_NUM_FILE

DESCRIPTION
  This function creates a new "tethered_call_profile_num" file and
  initlizes its contents
  
PARAMETERS
 None
  
DEPENDENCIES 
  None

RETURN VALUE 
  TRUE       : file initialized successfully
  FALSE      : Error in file creation / check

SIDE EFFECTS 
  None
===========================================================================*/
static boolean ds_umts_pdp_create_tethered_call_profile_num_file
(
   ds_umts_pdp_subs_e_type subs_id
)
{
  int   result;
  byte  prof_num = DS_3GPP_DEF_TETHERED_PROFILE;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if(ds_umts_pdp_profile_set_sub_dir_name(subs_id) != DS_UMTS_PDP_SUCCESS)
  {
    return FALSE;
  }
  /*-------------------------------------------------------------------------
    Init the fully qualified profile file name to 
    pdp_profiles directory
  -------------------------------------------------------------------------*/
  /*lint -e534 ignoring return code from function*/
  if( strlcpy( ds_umts_pdp_fully_qual_tethered_call_file_name,
                   ds_umts_pdp_profile_sub_dir_name,
                   sizeof(ds_umts_pdp_fully_qual_tethered_call_file_name) ) >=
      (int) sizeof(ds_umts_pdp_fully_qual_tethered_call_file_name) )
  {
    return FALSE;
  }

  strlcat( ds_umts_pdp_fully_qual_tethered_call_file_name,
               DS_UMTS_PDP_PROFILE_NUM_FOR_TETHERED_FILE_NAME,
               sizeof(ds_umts_pdp_fully_qual_tethered_call_file_name));
  /*lint +e534*/
  /*-------------------------------------------------------------------------
    As the size of the contents of this file is only 1 byte it will be treated 
    as item file.
  -------------------------------------------------------------------------*/
  result = efs_put(ds_umts_pdp_fully_qual_tethered_call_file_name,
                   (void *)(&prof_num),
                   sizeof(byte),
                   O_CREAT | O_TRUNC | O_RDWR,
                       DEFFILEMODE);
  if ( result == -1)
  {
    DS_3GPP_MSG3_ERROR("efs_open failed for fd=%d errno=%d.", result, efs_errno, 0);
    return FALSE;
  }
  
  DS_3GPP_MSG3_HIGH("tethered_call_profile_num created",0,0,0);
  cached_tethered_call_profile_number[subs_id] = prof_num;
  return TRUE;
  }
  
/*===========================================================================
FUNCTION DS_UMTS_PDP_CREATE_PERSISTENT_PROFILE_FILE

DESCRIPTION
  This function creates a new profile file and initializes its contents
  It is assumed that file profile is persistent.
  It assumes that cache will be updated by caller.
PARAMETERS
 profile_number   : profile_number
 profile_data_ptr : profile data to write in EFS.
  
DEPENDENCIES 
  None

RETURN VALUE 
  TRUE       : file initialized successfully
  FALSE      : Error in file creation / check

SIDE EFFECTS 
  None
===========================================================================*/
static boolean ds_umts_pdp_create_persistent_profile_file
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  ds_umts_pdp_profile_type *profile_data_ptr
)
{
  int        fs_handle;
  int        efs_res;
  struct     fs_stat sbuf;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /*-------------------------------------------------------------------------
    Only persistent profile gets written in EFS.
  -------------------------------------------------------------------------*/   

  /*-------------------------------------------------------------------------
    Init the fully qualified profile file name to 
    "pdp_profiles" directory.
  -------------------------------------------------------------------------*/
  if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
    return FALSE;
  /*-----------------------------------------------------------------------
    Check whether file is present if yes we need to delete the file otherwise
    we need to handle truncate.
  -----------------------------------------------------------------------*/
   efs_res = efs_stat((char*) ds_umts_pdp_fully_qual_profile_file_name, 
                      &sbuf);
  /*-----------------------------------------------------------------------
    Open/Create the file for write 
  -----------------------------------------------------------------------*/
  if ( efs_res == 0 )
  {
    /* File present: delete the file */
    ds_umts_pdp_delete_file((char*)ds_umts_pdp_fully_qual_profile_file_name);
  }
  /*-----------------------------------------------------------------------
    We have make sure that invalid profile is not present in EFS by
    validating the persistent profiles we are using and deleting rest of
    the files.
  -----------------------------------------------------------------------*/
  DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ", 
                          ds_umts_pdp_fully_qual_profile_file_name);
  
  fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                        O_CREAT | O_TRUNC | O_WRONLY,
                        DEFFILEMODE);
  if ( fs_handle == -1)
  {
    DS_3GPP_MSG3_ERROR("efs_open failed for fd=%d errno=%d.", fs_handle, efs_errno, 0);
    return FALSE;
  }
  /*-------------------------------------------------------------------------
    write the profile data in EFS.
  -------------------------------------------------------------------------*/
  if (ds_umts_pdp_efs_write(fs_handle,
                            (void *)profile_data_ptr,
                            sizeof(ds_umts_pdp_profile_type)) == FALSE)
  {
    DS_3GPP_MSG3_ERROR("efs_write failed for fd=%d errno=%d.", fs_handle, efs_errno, 0);
    efs_close (fs_handle);
    return FALSE;
  }

  /*-------------------------------------------------------------------------
    Close the file
  -------------------------------------------------------------------------*/
  if (efs_close (fs_handle) == -1)
  {
    DS_3GPP_MSG3_ERROR("efs_close failed for fd=%d errno=%d", fs_handle, efs_errno, 0);
    return FALSE;
  }

  return TRUE;
} /* ds_umts_pdp_create_persistent_profile_file */

/*===========================================================================
FUNCTION DS_UMTS_PDP_WRITE_PERSISTENT_PROFILE_FILE

DESCRIPTION
  This function writes profile data from cache for an existing profile.
  It is assumed that file profile is persistent and is present in EFS.
  It assumes that cache will be updated by caller.
PARAMETERS
 profile_number   : profile_number
 profile_data_ptr : profile data to write in EFS.
  
DEPENDENCIES 
  None

RETURN VALUE 
  TRUE       : file initialized successfully
  FALSE      : Error in file creation / check

SIDE EFFECTS 
  None
===========================================================================*/
static boolean ds_umts_pdp_write_persistent_profile_file
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  ds_umts_pdp_profile_type *profile_data_ptr
)
{
  int        fs_handle;
  struct     fs_stat sbuf;
  /*-------------------------------------------------------------------------
    Only persistent profile gets written in EFS.
  -------------------------------------------------------------------------*/   
  if(ds_umts_pdp_profile_set_sub_dir_name(subs_id) != DS_UMTS_PDP_SUCCESS)
  {
    return FALSE;
  }
  /*-------------------------------------------------------------------------
    Init the fully qualified profile file name to 
    "pdp_profiles" directory.
  -------------------------------------------------------------------------*/
  if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
    return FALSE;
  /*-----------------------------------------------------------------------
    It is assumed that file is present, we just need to update the data.
    The file is either already validated or created in previous operations.
  -----------------------------------------------------------------------*/
  /*-----------------------------------------------------------------------
    Open/Create the file for write 
  -----------------------------------------------------------------------*/
  /*-----------------------------------------------------------------------
    We have make sure that invalid profile is not present in EFS by
    validating the persistent profiles we are using and deleting rest of
    the files.
  -----------------------------------------------------------------------*/

#ifdef TEST_FRAMEWORK
  #error code not present
#else
  if(efs_stat((char*) ds_umts_pdp_profile_sub_dir_name, 
#endif
              &sbuf) == 0)
  {
    DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, 
                            "pdp_profiles dir %s present", 
                            ds_umts_pdp_profile_sub_dir_name);
  }
  else
  {
    DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_ERROR, 
                            "pdp_profiles dir %s NOT present. Write failed.", 
                            ds_umts_pdp_profile_sub_dir_name);
    return FALSE;
  }

  if(efs_stat(ds_umts_pdp_fully_qual_profile_file_name,&sbuf) == 0)
  {
    DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, 
                            "profile_name: %s present ", 
                            ds_umts_pdp_fully_qual_profile_file_name);
  }
  else
  {
    DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, 
                            "profile_name: %s NOT present ", 
                            ds_umts_pdp_fully_qual_profile_file_name);
  }

  fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name,
                        O_WRONLY);

  /*-------------------------------------------------------------------------
    if the efs file is not present, it will be created before written to
  -------------------------------------------------------------------------*/  
  if (fs_handle == -1 && efs_errno == ENOENT)
  {
    DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, 
                            "profile_name: %s does not exist. Create before writing.", 
                            ds_umts_pdp_fully_qual_profile_file_name);

    fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                          O_CREAT | O_TRUNC | O_WRONLY,
                          DEFFILEMODE);
  }
  
  if ( fs_handle == -1)
  {
    DS_3GPP_MSG3_ERROR("efs_open failed for fd=%d errno=%d.", fs_handle, efs_errno, 0);
    return FALSE;
  }
  /*-------------------------------------------------------------------------
    write the profile data in EFS.
  -------------------------------------------------------------------------*/
  if (ds_umts_pdp_efs_write(fs_handle,
                            (void *)profile_data_ptr,
                            sizeof(ds_umts_pdp_profile_type)) == FALSE)
  {
    DS_3GPP_MSG3_ERROR("efs_write failed for fd=%d errno=%d.", fs_handle, efs_errno, 0);
    efs_close (fs_handle);
    return FALSE;
  }

  /*-------------------------------------------------------------------------
    Close the file
  -------------------------------------------------------------------------*/
  if (efs_close (fs_handle) == -1)
  {
    DS_3GPP_MSG3_ERROR("efs_close failed for fd=%d errno=%d", fs_handle, efs_errno, 0);
    return FALSE;
  }

  DS_3GPP_MSG3_HIGH("PDP Profile File created NO:%d",profile_number, 0, 0 );

  return TRUE;
}


/*===========================================================================
FUNCTION DS_UMTS_PDP_VALIDATE_PROFILE_CONTENT

DESCRIPTION
  This function validates the profile content.

PARAMETERS
  profile_number  : profile number
  file_size       : Size of EFS file, we pass this for saving efs operation.
  version_offset  : 

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  
RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system

SIDE EFFECTS 
  None
===========================================================================*/
static ds_umts_pdp_profile_status_etype ds_umts_pdp_validate_profile_content
(
  uint8                     profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  uint16                    file_size,
  uint16                    version_offset
  /*lint -esym(715,version_offset) */
)
{
  int        fs_handle;
  byte       version;
  ds_umts_pdp_profile_type        *profile_data_ptr;
  /*-----------------------------------------------------------------------
    Init the fully qualified profile file name to 
    pdp_profiles directory
  -----------------------------------------------------------------------*/
  if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
  {
    return DS_UMTS_PDP_FAIL;
  }
  /*-----------------------------------------------------------------------
    Compare size if it matches we are good. Otherwise if version schema is
    is true compare version and based on it do truncate or addition. Else
    reset profile to default.
  -----------------------------------------------------------------------*/
  if ( sizeof (ds_umts_pdp_profile_type) != file_size )
  {
    DS_3GPP_MSG3_HIGH("ds_umts_pdp_validate_profile_content size mismatched"
                      "profile_number %d file_size %d", profile_number, file_size, 0);

    /*-----------------------------------------------------------------------
      Compare version of the profile 
    -----------------------------------------------------------------------*/
    
    /*-----------------------------------------------------------------------
      Open the file for read 
    -----------------------------------------------------------------------*/
    DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ", 
                            ds_umts_pdp_fully_qual_profile_file_name);
    
    fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                          O_RDONLY);
    if ( fs_handle == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_open failed for fd=%d errno=%d.", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    
    if (0 != version_offset)
    {
      /*---------------------------------------------------------------------
        Seek to the specified version offset position
      ---------------------------------------------------------------------*/
      if (efs_lseek(fs_handle,
                    version_offset,
                    SEEK_SET) == -1)
      {
        DS_3GPP_MSG3_ERROR( "efs_lseek failed for %d errno = %d", fs_handle, efs_errno, 0 );
        efs_close (fs_handle);
        return DS_UMTS_PDP_ACCESS_ERROR;
      }
    }
  
    /*-----------------------------------------------------------------------
      Read the version field
    -----------------------------------------------------------------------*/
    if (!ds_umts_pdp_efs_read(fs_handle,
                      (void *)&version,
                      sizeof(version)))
    {
      DS_3GPP_MSG3_ERROR( "efs_read failed for %d errno = %d", fs_handle, efs_errno, 0 );
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-----------------------------------------------------------------------
      Close the file
    -----------------------------------------------------------------------*/
    if (efs_close (fs_handle) == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_close failed for %d errno = %d", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_FAIL;
    }

    /*-----------------------------------------------------------------------
      Compare the file version against the schema version
    -----------------------------------------------------------------------*/
    if (DS_UMTS_PROFILE_VERSION != version)
    {
      DS_3GPP_MSG3_ERROR("Version validation failed: %d/%d",
                DS_UMTS_PROFILE_VERSION,version,0);
      return DS_UMTS_PDP_PROFILE_VERSION_MISMATCH;
    }
    
    /*-----------------------------------------------------------------------
      Size mismatches but version doesn't matches: Some junk data in file,
      we are not going to use this file anyway so delete it.
    -----------------------------------------------------------------------*/
    ds_umts_pdp_delete_file((char*)ds_umts_pdp_fully_qual_profile_file_name);
    
    DS_3GPP_MSG3_ERROR("Delete the file Version Matched size mismatched",
                        0, 0, 0);
    return DS_UMTS_PDP_FAIL;
  }
  DS_3GPP_MSG3_HIGH("Size matched for profile_number %d", profile_number, 0, 0);
  /*-----------------------------------------------------------------------
    Size matched, we can read profile data in cache so that all next
    operation can be done from cache.
  -----------------------------------------------------------------------*/
  /*-----------------------------------------------------------------------
    Open the file for read 
  -----------------------------------------------------------------------*/
  DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ", 
                          ds_umts_pdp_fully_qual_profile_file_name);
  
  fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                          O_RDONLY);
  if ( fs_handle == -1)
  {
    DS_3GPP_MSG3_ERROR("efs_open failed for fd=%d errno=%d.", fs_handle, efs_errno, 0);
    return DS_UMTS_PDP_ACCESS_ERROR;
  }

  profile_data_ptr = DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);

  /*-------------------------------------------------------------------------
    Read the context definition parameters directyly in the cache
  -------------------------------------------------------------------------*/

  if (!ds_umts_pdp_efs_read(fs_handle,
                    (void *)profile_data_ptr,
                    sizeof(ds_umts_pdp_profile_type)))
  {
    efs_close (fs_handle);
    return DS_UMTS_PDP_ACCESS_ERROR;
  }
  /*-----------------------------------------------------------------------
    Only valid profile content is considered. It is assumed that file will
    be persistent as it is valid and is present after bootup.
  -----------------------------------------------------------------------*/
  if(!((profile_data_ptr->context).valid_flg))
  {
    /*-------------------------------------------------------------------------
      Reset profile to default.
    -------------------------------------------------------------------------*/
    if(!ds_umts_pdp_init_cache_default_profile_info(profile_number,subs_id))
    {
      DS_3GPP_MSG3_ERROR("Reset profile %d to default failed", profile_number, 0, 0);
      efs_close (fs_handle);
      return DS_UMTS_PDP_FAIL;
    }
    /*-------------------------------------------------------------------------
      Close the file
    -------------------------------------------------------------------------*/
    if (efs_close (fs_handle) == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_close failed for fd=%d errno=%d", fs_handle, efs_errno, 0);
      efs_close (fs_handle);
      return DS_UMTS_PDP_FAIL;
    }
    ds_umts_pdp_delete_file((char*)ds_umts_pdp_fully_qual_profile_file_name);
    efs_close (fs_handle);
    /* delete the file in EFS */
    return DS_UMTS_PDP_SUCCESS;
  }
  else
  {
    /* Valid profile in EFS must be persistent */
    profile_data_ptr->transience_flag = FALSE;
  }

  /*-----------------------------------------------------------------------
    Close the file
  -----------------------------------------------------------------------*/
  if (efs_close (fs_handle) == -1)
  {
    DS_3GPP_MSG3_ERROR("efs_close failed for %d errno = %d", fs_handle, efs_errno, 0);
    return DS_UMTS_PDP_FAIL;
  }
  /* Fix of another CR overwrite the version if version doesn't match and size matches */

  if (DS_UMTS_PROFILE_VERSION != profile_data_ptr->version)
  {
    DS_3GPP_MSG3_ERROR("Version validation failed: %d/%d",
                        DS_UMTS_PROFILE_VERSION,profile_data_ptr->version,0);
    /*TO DO: overwrite the version field in profile file and
       also update the cache with new version */
  }
  return DS_UMTS_PDP_SUCCESS;

}
/*===========================================================================
FUNCTION DS_UMTS_PDP_VALIDATE_CALL_PROFILE_NUM_FILE

DESCRIPTION
  This function validates whether the emb/teth call profile num file is an
  item file, if not, then reads the existing value in the file, deletes it
  and creates a new item file and update the value with the one read earlier

PARAMETERS
  file_path  : efs file path

DEPENDENCIES 
  None.
  
RETURN VALUE 
  DS_UMTS_PDP_SUCCESS - on success.
  DS_UMTS_PDP_FAIL    - failure.
  DS_UMTS_PDP_ACCESS_ERROR - failure.

SIDE EFFECTS 
  None
===========================================================================*/
static ds_umts_pdp_profile_status_etype ds_umts_pdp_validate_call_profile_num_file
(
  char *file_path,
  ds_umts_pdp_subs_e_type subs_id
)
{
  int    result = 0;
  uint16 profile_num = 0;
  
  if(ds_umts_pdp_profile_set_sub_dir_name(subs_id) != DS_UMTS_PDP_SUCCESS)
  {
    return DS_UMTS_PDP_FAIL;
  }
  /* Embedded call file */
  if( strlcpy( ds_umts_pdp_fully_qual_embedded_call_file_name,
                   ds_umts_pdp_profile_sub_dir_name,
                   sizeof(ds_umts_pdp_fully_qual_embedded_call_file_name) ) >=
      (int) sizeof(ds_umts_pdp_fully_qual_embedded_call_file_name) )
  {
    return DS_UMTS_PDP_FAIL;
  }

  strlcat( ds_umts_pdp_fully_qual_embedded_call_file_name,
               DS_UMTS_PDP_PROFILE_NUM_FOR_EMBEDDED_FILE_NAME,
               sizeof(ds_umts_pdp_fully_qual_embedded_call_file_name) );

  /* Tethered call file */
  if( strlcpy( ds_umts_pdp_fully_qual_tethered_call_file_name,
                   ds_umts_pdp_profile_sub_dir_name,
                   sizeof(ds_umts_pdp_fully_qual_tethered_call_file_name) ) >=
      (int) sizeof(ds_umts_pdp_fully_qual_tethered_call_file_name) )
  {
    return DS_UMTS_PDP_FAIL;
  }

  strlcat( ds_umts_pdp_fully_qual_tethered_call_file_name,
               DS_UMTS_PDP_PROFILE_NUM_FOR_TETHERED_FILE_NAME,
               sizeof(ds_umts_pdp_fully_qual_tethered_call_file_name) );

  result = efs_get(file_path, (void *)(&profile_num), sizeof(byte));

  if ( result == -1)
  {
    DS_3GPP_MSG3_ERROR("efs_get failed for %d errno=%d", result, efs_errno, 0);
    return DS_UMTS_PDP_ACCESS_ERROR;
  }
  else
  {
    if (memcmp(file_path, 
                 ds_umts_pdp_fully_qual_embedded_call_file_name,
                 sizeof(ds_umts_pdp_fully_qual_embedded_call_file_name)) == 0)
    {
      cached_embedded_call_profile_number[subs_id] = profile_num;
      DS_3GPP_MSG3_ERROR("default embeded call profile number read from efs %d", 
                         cached_embedded_call_profile_number[subs_id], 0, 0);
    }
    else if (memcmp(file_path, 
                      ds_umts_pdp_fully_qual_tethered_call_file_name,
                  sizeof(ds_umts_pdp_fully_qual_tethered_call_file_name)) == 0)
    {
      cached_tethered_call_profile_number[subs_id] = profile_num;
      DS_3GPP_MSG3_ERROR("default tethered call profile number read from efs %d", 
                         cached_tethered_call_profile_number[subs_id], 0, 0);
    }
  }

  /*-------------------------------------------------------------------------
    Open the file for write 
  -------------------------------------------------------------------------*/
  result = efs_put(file_path,
                   (void *)(&profile_num),
                   sizeof(byte),
                   O_RDWR,
                   DEFFILEMODE);

  if ( (result == -1) && (efs_errno == ENOENT) )
  {
    DS_3GPP_MSG3_HIGH("File present is not an item file", 0,0,0);

    /*------------------------------------------------------------------------
      Delete this file and create a new item file. 
    ------------------------------------------------------------------------*/
    if (!ds_umts_pdp_delete_file(file_path))
    {
      DS_3GPP_MSG3_ERROR("error in deleting existing file", 0,0,0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    else
    {
      if (memcmp(file_path, 
                 ds_umts_pdp_fully_qual_embedded_call_file_name,
                 sizeof(ds_umts_pdp_fully_qual_embedded_call_file_name)) == 0)
      {
        if (!ds_umts_pdp_create_embedded_call_profile_num_file(subs_id))
        {
          return DS_UMTS_PDP_FAIL;
        }
        cached_embedded_call_profile_number[subs_id] = profile_num;
      }
      else if (memcmp(file_path, 
                      ds_umts_pdp_fully_qual_tethered_call_file_name,
                  sizeof(ds_umts_pdp_fully_qual_tethered_call_file_name)) == 0)
      {
        if (!ds_umts_pdp_create_tethered_call_profile_num_file(subs_id))
        {
          return DS_UMTS_PDP_FAIL; 
        }
        cached_tethered_call_profile_number[subs_id] = profile_num;
      }
      else
      {
        DS_3GPP_MSG3_ERROR("invalid file path", 0,0,0);
        return DS_UMTS_PDP_FAIL;
      }
    }

    /*-------------------------------------------------------------------------
      Open the file for write 
    -------------------------------------------------------------------------*/
    if ( efs_put(file_path,
                 (void *)(&profile_num),
                 sizeof(byte),
                 O_RDWR,
                 DEFFILEMODE) == -1 )
    {
      DS_3GPP_MSG3_ERROR("efs_put() after file creation returned error: %d", 
                          efs_errno, 0, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  }
  else if ( result == -1 )
  {
    DS_3GPP_MSG3_ERROR("efs_put() returned error with errno: %d", 
                        efs_errno, 0, 0);
    return DS_UMTS_PDP_ACCESS_ERROR;
  }
  else
  {
    DS_3GPP_MSG3_HIGH("File present is an item file", 0,0,0);
    return DS_UMTS_PDP_SUCCESS;
  }
  
  return DS_UMTS_PDP_SUCCESS;
}  

/*===========================================================================
FUNCTION DS_UMTS_PDP_CHECK_AND_INIT_EMBEDDED_CALL_NUM_FILE

DESCRIPTION
  This function checks for the presence of the "embedded_call_profile_num"
  file for the length of its contents. 
  If the length does not match, it overwrites it with a new file.
  If the file does not exist, a new file is created
  
PARAMETERS
  None
  
DEPENDENCIES 
  None

RETURN VALUE 
  TRUE       : file initialized successfully
  FALSE      : Error in file creation / check

SIDE EFFECTS 
  None
===========================================================================*/
static boolean ds_umts_pdp_check_and_init_embedded_call_num_file
(
   ds_umts_pdp_subs_e_type subs_id
)
{
  struct fs_stat sbuf;
  int            efs_res;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if(ds_umts_pdp_profile_set_sub_dir_name(subs_id)!= DS_UMTS_PDP_SUCCESS)
  {
    return FALSE;
  }
  /*-------------------------------------------------------------------------
    Init the fully qualified profile file name to pdp_profiles directory
  -------------------------------------------------------------------------*/
  /*lint -e534 ignoring return code from function*/
  if( strlcpy( ds_umts_pdp_fully_qual_embedded_call_file_name,
                   ds_umts_pdp_profile_sub_dir_name,
                   sizeof(ds_umts_pdp_fully_qual_embedded_call_file_name) ) >=
      (int) sizeof(ds_umts_pdp_fully_qual_embedded_call_file_name) )
  {
    return FALSE;
  }

  strlcat( ds_umts_pdp_fully_qual_embedded_call_file_name,
               DS_UMTS_PDP_PROFILE_NUM_FOR_EMBEDDED_FILE_NAME,
               sizeof(ds_umts_pdp_fully_qual_embedded_call_file_name) );
  /*lint +e534*/

  efs_res = efs_stat((char*) ds_umts_pdp_fully_qual_embedded_call_file_name, 
                     &sbuf);
  if ( efs_res == 0 )
  {
    /*-----------------------------------------------------------------------
      File present, Check file contents to see if any data is present
    -----------------------------------------------------------------------*/
     if ( sbuf.st_size != sizeof(byte) )
    {
      DS_3GPP_MSG3_ERROR( "file size incorrect:reset(%d/1)", 
                           sbuf.st_size, 0, 0 );
      
      /*---------------------------------------------------------------------
        File present but size is incompatible. Delete the current file and
        create a new item file with default values.
      ---------------------------------------------------------------------*/
      if( !ds_umts_pdp_delete_file(
                     (char*)ds_umts_pdp_fully_qual_embedded_call_file_name) )
      {
        DS_3GPP_MSG3_ERROR("Failed to delete existing emb_call_prof_num_file",
                            0,0,0);
        return FALSE;
      }
      else
      {
        return ds_umts_pdp_create_embedded_call_profile_num_file(subs_id);
      }
    }
    else
    {
      DS_3GPP_MSG3_HIGH(" embedded call profile number file present",0,0,0);  
      if ( ds_umts_pdp_validate_call_profile_num_file(
                (char*)ds_umts_pdp_fully_qual_embedded_call_file_name, 
                subs_id) 
           != DS_UMTS_PDP_SUCCESS ) 
      {
        return FALSE;
      } 
    }
  }
  else
  {
    /*-----------------------------------------------------------------------
      embedded_call_prof_num not present : initialize cache with default value
    -----------------------------------------------------------------------*/
    cached_embedded_call_profile_number[subs_id] = DS_3GPP_DEF_EMBEDDED_PROFILE;
  }
  
  return TRUE;
}

/*===========================================================================
FUNCTION DS_UMTS_PDP_CHECK_AND_INIT_TETHERED_CALL_NUM_FILE

DESCRIPTION
  This function checks for the presence of the "tethered_call_profile_num"
  file for the length of its contents. 
  If the length does not match, it overwrites it with a new file.
  If the file does not exist, a new file is created
  
PARAMETERS
 None
  
DEPENDENCIES 
  None

RETURN VALUE 
  TRUE       : file initialized successfully
  FALSE      : Error in file creation / check

SIDE EFFECTS 
  None
===========================================================================*/
static boolean ds_umts_pdp_check_and_init_tethered_call_num_file
(
   ds_umts_pdp_subs_e_type subs_id
)
{
  struct fs_stat sbuf;
  int            efs_res;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if(ds_umts_pdp_profile_set_sub_dir_name(subs_id)!= DS_UMTS_PDP_SUCCESS)
  {
    return FALSE;
  }
  /*-------------------------------------------------------------------------
    Init the fully qualified profile file name to pdp_profiles directory
  -------------------------------------------------------------------------*/
  /*lint -e534 ignoring return code from function*/
  if( strlcpy( ds_umts_pdp_fully_qual_tethered_call_file_name,
                   ds_umts_pdp_profile_sub_dir_name,
                   sizeof(ds_umts_pdp_fully_qual_tethered_call_file_name) ) >=
      (int) sizeof(ds_umts_pdp_fully_qual_tethered_call_file_name) )
  {
    return FALSE;
  }

  strlcat( ds_umts_pdp_fully_qual_tethered_call_file_name,
               DS_UMTS_PDP_PROFILE_NUM_FOR_TETHERED_FILE_NAME,
               sizeof(ds_umts_pdp_fully_qual_tethered_call_file_name) );
  /*lint +e534*/

  efs_res = efs_stat((char*) ds_umts_pdp_fully_qual_tethered_call_file_name, 
                     &sbuf);
  if ( efs_res == 0 )
  {
    /*-----------------------------------------------------------------------
      File present, Check file contents to see if any data is present
    -----------------------------------------------------------------------*/
     if ( sbuf.st_size != sizeof(byte) )
    {
      DS_3GPP_MSG3_ERROR( "file size incorrect:reset(%d/1)", 
                           sbuf.st_size, 0, 0 );
      
      /*---------------------------------------------------------------------
        File present but size is incompatible. Delete the current file and
        create a new item file with default values.
      ---------------------------------------------------------------------*/
      if( !ds_umts_pdp_delete_file(
                    (char*) ds_umts_pdp_fully_qual_tethered_call_file_name) )
      {
        DS_3GPP_MSG3_ERROR("Failed to delete existing teth_call_prof_num_file",
                            0,0,0);
        return FALSE;
      }
      else
      {
        return ds_umts_pdp_create_tethered_call_profile_num_file(subs_id);
      }
    }
    else
    {
      DS_3GPP_MSG3_HIGH(" tethered call profile number file present",0,0,0);
      if ( ds_umts_pdp_validate_call_profile_num_file(
                     (char*)ds_umts_pdp_fully_qual_tethered_call_file_name,
                     subs_id)
           != DS_UMTS_PDP_SUCCESS )
      {
        return FALSE;
      }  
    }
  }
  else
  {
    /*-----------------------------------------------------------------------
      tethered_call_prof_num not present : initialize cache with default value 
    -----------------------------------------------------------------------*/
    cached_tethered_call_profile_number[subs_id] = DS_3GPP_DEF_TETHERED_PROFILE;
  }
  
  return TRUE;
}

/*===========================================================================
FUNCTION DS_UMTS_UPDATE_PDP_PROFILE_VERSION_CHANGE

DESCRIPTION
  This function updates the PDP profile contents when the PDP profile version
  has changed.
  If version in the EFS profile has size smaller than that of the new profile, 
  it concatenates new fields from default profile.
  If version in the EFS profile has size larger than that of the new profile,
  it truncates the extra fields.

PARAMETERS
 profile_number : profile number
 size : size of file 
DEPENDENCIES 
  None

RETURN VALUE 
  TRUE       : profile updated successfully
  FALSE      : Error in updating profile

SIDE EFFECTS 
  None
===========================================================================*/
static boolean ds_umts_update_pdp_profile_version_change
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  uint16                    file_size
)
{
  int     fs_handle;
  byte   *buf_ptr = NULL;
  byte    version = DS_UMTS_PROFILE_VERSION;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /*-----------------------------------------------------------------------
    Init the fully qualified profile file name to 
    "pdp_profiles" directory
  -----------------------------------------------------------------------*/
  if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
    return FALSE;

  /*-----------------------------------------------------------------------
    Open the file for write 
  -----------------------------------------------------------------------*/
  DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ", 
                          ds_umts_pdp_fully_qual_profile_file_name);
  
  fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name,
                        O_RDWR);
  if ( fs_handle == -1)
  {
    DS_3GPP_MSG3_ERROR("efs_open failed for %d errno=%d.", fs_handle, efs_errno, 0);
    return FALSE;
  }

  if (0 != DS_UMTS_PDP_PROFILE_VERSION_OFFSET )
  {
    /*---------------------------------------------------------------------
      Seek to the specified version offset position
    ---------------------------------------------------------------------*/
    if (efs_lseek(fs_handle,
                  DS_UMTS_PDP_PROFILE_VERSION_OFFSET,
                  SEEK_SET) == -1)
    {
      DS_3GPP_MSG3_ERROR( "efs_lseek failed for %d errno = %d", fs_handle, efs_errno, 0 );
      efs_close (fs_handle);
      return FALSE;
    }
  }
  /*-----------------------------------------------------------------------
    Overwrite the current version: Since we are handling version mismatch
    by updating the size of the file also update the version.
  -----------------------------------------------------------------------*/
  if (!ds_umts_pdp_efs_write(fs_handle,
                    (void *)(&version),
                    sizeof(version)))
  {
    DS_3GPP_MSG3_ERROR( "efs_write failed for %d errno = %d", fs_handle, efs_errno, 0 );
    efs_close (fs_handle);
    return FALSE;
  }

  /*-------------------------------------------------------------------
    If new profile is larger than existing profile, concatenate the 
    file with extra fields values from default profile.
  -------------------------------------------------------------------*/
  if (sizeof(ds_umts_pdp_profile_type) > file_size)
  {
    if (efs_lseek(fs_handle,
                  0,
                  SEEK_END) == -1)
    {
      DS_3GPP_MSG3_ERROR( "efs_lseek failed for %d errno=%d", fs_handle, efs_errno, 0 );
      efs_close (fs_handle);
      return FALSE;
    }
    buf_ptr = (byte*)(&ds_umts_pdp_default_profile) + file_size;
    if (ds_umts_pdp_efs_write(fs_handle,
                    buf_ptr,
                    (sizeof(ds_umts_pdp_profile_type) - file_size)) == FALSE)
    {
      DS_3GPP_MSG3_ERROR( "efs_write failed for %d", fs_handle, 0, 0 );
      efs_close (fs_handle);
      return FALSE;
    }
  }
  /*-------------------------------------------------------------------
    If new profile is smaller than existing profile, truncate the 
    extra fields from the file.
  -------------------------------------------------------------------*/
  else if (sizeof(ds_umts_pdp_profile_type) < file_size)
  {
    if (efs_ftruncate(fs_handle, sizeof(ds_umts_pdp_profile_type)) == -1)
    {
      DS_3GPP_MSG3_ERROR( "config :fs_truncate failed for fd=%d errno=%d", 
                  fs_handle, efs_errno, 0 );
      efs_close (fs_handle);
      return FALSE;
    }
  }
  /*--------------------------------------------------------------------------
    Update file handle to beginning of the file.
  -------------------------------------------------------------------------*/
  
  if (efs_lseek(fs_handle,
                0,
                SEEK_SET) == -1)
  {
    DS_3GPP_MSG3_ERROR( "efs_lseek failed for %d errno=%d", fs_handle, efs_errno, 0 );
    efs_close (fs_handle);
    return FALSE;
  }
  /*-------------------------------------------------------------------------
    Read the  context definition parameters directly in the cache
  -------------------------------------------------------------------------*/
  //check validity of flag of profile then only can we read in cache.
  if (!ds_umts_cache_profile_file_data(profile_number,subs_id,fs_handle))
  {
    efs_close (fs_handle);
    return FALSE;
  }
  /*-------------------------------------------------------------------------
    Close the file
  -------------------------------------------------------------------------*/
  if (efs_close (fs_handle) == -1)
  {
    DS_3GPP_MSG3_ERROR("efs_close failed for %d errno = %d", fs_handle, efs_errno, 0);
    return FALSE;
  }
  return TRUE;
}

/*===========================================================================
FUNCTION DS_UMTS_PDP_CHECK_AND_INIT_PERSISTENT_PROFILE_FILE

DESCRIPTION
  This function checks for the presence of the profile file and also the 
  length of its contents. It populates the profile data in cache and 
  check for transient flag. If profile is marked as persistent it will
  be saved and initialized.
  
PARAMETERS
  profie_number : profile number
  
DEPENDENCIES 
  None

RETURN VALUE 
  TRUE       : Profile file initialized successfully
  FALSE      : Error in file creation / check

SIDE EFFECTS 
  None
===========================================================================*/
static boolean ds_umts_pdp_check_and_init_persistent_profile_file
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id
)
{
  struct fs_stat sbuf;
  int                                efs_res;
  ds_umts_pdp_profile_status_etype   ret_val;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /*-----------------------------------------------------------------------
    Init the fully qualified profile file name to 
    pdp_profiles directory
  -----------------------------------------------------------------------*/
  if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
  {
    return FALSE;
  }

  efs_res = efs_stat((char*) ds_umts_pdp_fully_qual_profile_file_name, 
                     &sbuf);
  
  if( efs_res == 0 )
  {
    /*---------------------------------------------------------------------
      File present, check file to see if contents any data is present
    ---------------------------------------------------------------------*/
    ret_val = ds_umts_pdp_validate_profile_content
                (
                  profile_number,
                  subs_id,
                  (uint16)sbuf.st_size,
                  DS_UMTS_PDP_PROFILE_VERSION_OFFSET
                );
    if (DS_UMTS_PDP_SUCCESS != ret_val)    
    {
      if(ret_val == DS_UMTS_PDP_PROFILE_VERSION_MISMATCH)
      {
        if( ! ds_umts_update_pdp_profile_version_change(profile_number,subs_id,
                                                       (uint16)sbuf.st_size) )
        {
          return FALSE;
        }
        /* profile will always be persistent if it is present */
      }
    }
    else
    {
      DS_3GPP_MSG3_HIGH(" Profile file check passed for profile num = %d",
               profile_number, 0, 0);
      /* profile will always be persistent if it is present */
    }
  }

  /*-----------------------------------------------------------------
    Triggerring of reset of default embedded is not required now because
    we are not creating any profile file for them if not there already
    and cache already have default data.
  -----------------------------------------------------------------*/
  return TRUE;
}

/*===========================================================================
FUNCTION DS_UMTS_PDP_CREATE_AND_INIT_DEFAULT_PROFILE

DESCRIPTION
  This function creates default profile file in the EFS and also the 
  file which stores the profile number for a embedded data call
  
PARAMETERS
  None
  
DEPENDENCIES 
  None

RETURN VALUE 
  TRUE       : Default profile file have been created successfully
  FALSE      : Error in file creation

SIDE EFFECTS 
  None
===========================================================================*/
static boolean ds_umts_pdp_create_and_init_default_profile
(
   ds_umts_pdp_subs_e_type subs_id
)
{
  struct fs_stat sbuf;
  EFSDIR *dirp = NULL;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  dirp = efs_opendir(DS_UMTS_PDP_PROFILE_DIR_NAME);
  if ((dirp == NULL) || (EFSDIR *)-1 == dirp) 
  {
    if (efs_mkdir(DS_UMTS_PDP_PROFILE_DIR_NAME, S_IREAD|S_IWRITE|S_IEXEC) == -1)
    {
      DS_3GPP_MSG3_ERROR( "pdp_profiles/subs1: fs_mkdir failed errno = %d", efs_errno, 0, 0 );
      return FALSE;
    }
  }


  if (efs_mkdir(ds_umts_pdp_profile_sub_dir_name_only, S_IREAD|S_IWRITE|S_IEXEC) == -1)
  {
    DS_3GPP_MSG3_ERROR( "pdp_profiles/subs1: fs_mkdir failed errno = %d", efs_errno, 0, 0 );
    return FALSE;
  }

  DS_3GPP_MSG3_HIGH("Profiles directory created :",0,0,0);
  /*-----------------------------------------------------------------------
    tethered_call_prof_num and cached_embedded_call_profile_number files
    not present : initialize cache with default value 
  -----------------------------------------------------------------------*/
  cached_tethered_call_profile_number[subs_id] = DS_3GPP_DEF_TETHERED_PROFILE;
  cached_embedded_call_profile_number[subs_id] = DS_3GPP_DEF_EMBEDDED_PROFILE;
  return TRUE;
}

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_ALL_INFO_FROM_CACHE
DESCRIPTION
  This function retrieves the profile information from the cache for a
  specific profile number
  
PARAMETERS
  profile_number  : profile number
  profile_data_ptr    : pointer to store the profile information

DEPENDENCIES 

RETURN VALUE 
  TRUE             : the operation completed successfully
  FALSE            : The operation did not complete successfully.
SIDE EFFECTS 
  None
===========================================================================*/
static boolean ds_umts_get_pdp_profile_all_info_from_cache
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  ds_umts_pdp_profile_type  *profile_data_ptr
)
{
  /*-----------------------------------------------------------------------
    Get the profile information from the profile cache table.
  -----------------------------------------------------------------------*/
  DS_UMTS_PDP_TASKLOCK_NON_L4();
  memscpy( (void*) profile_data_ptr,
  	    sizeof(ds_umts_pdp_profile_type),
            (void *)DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number),
            sizeof(ds_umts_pdp_profile_type) );
  DS_UMTS_PDP_TASKFREE_NON_L4();

  return TRUE;
}

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_CONTEXT_INFO_FROM_CACHE

DESCRIPTION
  This function retrieves the context information from the cache for a
  specific profile number
  
PARAMETERS
  profile_number  : profile number
  pdp_context_data: pointer to store the context information

DEPENDENCIES 

RETURN VALUE 
  TRUE             : the operation completed successfully
  FALSE            : error code returned by file system functions
SIDE EFFECTS 
  None
===========================================================================*/
static boolean ds_umts_get_pdp_profile_context_info_from_cache
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  ds_umts_pdp_context_type  *pdp_context_data 
)
{
  /*-----------------------------------------------------------------------
    Get the profile context information from the cache table.
  -----------------------------------------------------------------------*/

  DS_UMTS_PDP_TASKLOCK_NON_L4();
  memscpy( (void*) pdp_context_data,
  	    sizeof(ds_umts_pdp_context_type),
            (void*)&(DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number)->context),
             sizeof(ds_umts_pdp_context_type) );
  DS_UMTS_PDP_TASKFREE_NON_L4();
  return TRUE;
}

/*===========================================================================
FUNCTION DS_UMTS_UPDATE_PDP_PROFILE_UMTS_QOS_INFO

DESCRIPTION
  This function updates the umts qos information with the values passed.
  It directly overwrites the current values stored in the profile file.
  It also updates the cache table with the new UMTS QOS Info.

PARAMETERS
  profile_number  : profile number
  qos_set         : specifies the qos parameter set ( requested / minimum)
  pdp_umts_qos_data: pointer to the UMTS qos information

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system

SIDE EFFECTS 
  None
===========================================================================*/
static ds_umts_pdp_profile_status_etype  ds_umts_update_pdp_profile_umts_qos_info
(
  uint16                       profile_number,
  ds_umts_pdp_subs_e_type      subs_id,
  ds_umts_qos_set_enum_type    qos_set,
  const ds_umts_umts_qos_params_type *pdp_umts_qos_data
)
{
  int                             fs_handle;
  fs_off_t                        offset_position;
  ds_umts_pdp_profile_type       *ds_umts_pdp_profile_ptr;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------------
    Allow the QOS parameters to be updated independent of the context
    definition.  Check if we are initialized & the profile number is valid
  -------------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();

  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 
  
  /*-------------------------------------------------------------------------
    Initialize the variables for fs_open                   
  -------------------------------------------------------------------------*/

  if(pdp_umts_qos_data->sig_ind == TRUE) 
  {
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
      Sig Ind Flag should only be set if the traffic class is interactive
      and the traffic handling priority is 1 (This is as per spec 
      23.107 Sec 6.4.3.1).

      Signalling Indication (Yes/No)
      Definition: Indicates the signalling nature of the submitted SDUs.
      This attribute is additional to the other QoS attributes and does
      not over-ride them. This attribute is only defined for the
      interactive traffic class. If signalling indication is set to "Yes",
      the UE should set the traffic handling priority to "1".
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

    if(pdp_umts_qos_data->traffic_class != DS_UMTS_QOS_TCLASS_INTR)
    {
      DS_3GPP_MSG3_ERROR("Sig Ind Flag should be used with Intr Traff Class", 0,0,0);
      return DS_UMTS_PDP_SIG_IND_ALLOWED_ONLY_WITH_TCLASS_INTR;
    }

    if(pdp_umts_qos_data->thandle_prio != 0x01)
    {
      DS_3GPP_MSG3_ERROR("Sig Ind Flag should be used with Traff Hand Prio 1", 0,0,0);
      return DS_UMTS_PDP_SIG_IND_ALLOWED_ONLY_WITH_THANDLE_PRI1;
    }
  }

  ds_umts_pdp_profile_ptr = DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);
  if(DS_UMTS_PDP_PROFILE_IS_PERSISTENT(ds_umts_pdp_profile_ptr)) 
  {
  
    /*-----------------------------------------------------------------------
      Init the fully qualified profile file name to 
      pdp_profiles directory
    -----------------------------------------------------------------------*/
    if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
      return DS_UMTS_PDP_FAIL;
  
    /*-----------------------------------------------------------------------
      Open the file for write 
    -----------------------------------------------------------------------*/
    DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ", 
                            ds_umts_pdp_fully_qual_profile_file_name);

    fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                          O_RDWR);
    if ( fs_handle == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_open failed for %d errno = %d.", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------------
      Set the file offset to the start of UMTS QOS info
    -------------------------------------------------------------------------*/
    offset_position = (qos_set == DS_UMTS_QOS_REQ_TYPE ) ?
      DS_UMTS_PDP_PROFILE_UMTS_REQ_QOS_OFFSET : DS_UMTS_PDP_PROFILE_UMTS_MIN_QOS_OFFSET;
  
  
    if (efs_lseek(fs_handle,
                  offset_position,
                  SEEK_SET) == -1)
    {
      DS_3GPP_MSG3_ERROR( "efs_lseek failed for %d errno = %d", fs_handle, efs_errno, 0 );
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    /*-------------------------------------------------------------------------
      write the UMTS QOS info passed in 
    -------------------------------------------------------------------------*/
    if (ds_umts_pdp_efs_write(fs_handle,
                              (void *)pdp_umts_qos_data,
                              sizeof(ds_umts_umts_qos_params_type)) == FALSE)
    {
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    
    /*-------------------------------------------------------------------------
      Close the file
    -------------------------------------------------------------------------*/
    if (efs_close (fs_handle) == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_close failed for %d errno = %d", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  }
  /*-------------------------------------------------------------------------
    Update the cache with the new UMTS QOS Info.
  -------------------------------------------------------------------------*/


  DS_UMTS_PDP_TASKLOCK_NON_L4();
  if (qos_set == DS_UMTS_QOS_REQ_TYPE) 
  {
    memscpy((void *)&ds_umts_pdp_profile_ptr->qos_request_umts,
	    sizeof(ds_umts_umts_qos_params_type),
            (void*)pdp_umts_qos_data,
            sizeof(ds_umts_umts_qos_params_type) );
  }       
  else
  {
    memscpy( (void *)&ds_umts_pdp_profile_ptr->qos_minimum_umts,
              sizeof(ds_umts_umts_qos_params_type),
              (void*)pdp_umts_qos_data,
              sizeof(ds_umts_umts_qos_params_type) );
  } 
  DS_UMTS_PDP_TASKFREE_NON_L4();
  return DS_UMTS_PDP_SUCCESS;
}

/*===========================================================================
FUNCTION DS_UMTS_UPDATE_PDP_PROFILE_GPRS_QOS_INFO

DESCRIPTION
  This function updates the GPRS qos information with the values passed.
  It directly overwrites the current values stored in the profile file.
  It also updates the cache table with the new GPRS QOS info.

PARAMETERS
  profile_number  : profile number
  qos_set         : specifies the qos parameter set ( requested / minimum)
  pdp_gprs_qos_data : pointer to the qos data

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  
RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system

SIDE EFFECTS 
  None
===========================================================================*/
static ds_umts_pdp_profile_status_etype  ds_umts_update_pdp_profile_gprs_qos_info
(   
  uint16                          profile_number,
  ds_umts_pdp_subs_e_type         subs_id,
  ds_umts_gprs_qos_set_enum_type  qos_set,
  const ds_umts_gprs_qos_params_type    *pdp_gprs_qos_data
)
{
  int                             fs_handle;
  fs_off_t                        offset_position;
  ds_umts_pdp_profile_type       *ds_umts_pdp_profile_ptr;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------------
    Allow the QOS parameters to be updated independent of the context
    definition.  Check if we are initialized & the profile number is valid
  -------------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();

  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 

  /*-------------------------------------------------------------------------
    Initialize the variables for fs_open                   
  -------------------------------------------------------------------------*/
  ds_umts_pdp_profile_ptr = DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);

  if(DS_UMTS_PDP_PROFILE_IS_PERSISTENT(ds_umts_pdp_profile_ptr)) 
  {
      /*-----------------------------------------------------------------------
      Init the fully qualified profile file name to 
        pdp_profiles directory
      -----------------------------------------------------------------------*/
    if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
      return DS_UMTS_PDP_FAIL;
  
    /*-----------------------------------------------------------------------
      Open the file for write 
    -----------------------------------------------------------------------*/
    DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ", 
                            ds_umts_pdp_fully_qual_profile_file_name);
    
    fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                          O_RDWR);
    if ( fs_handle == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_open failed for %d errno = %d.", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------------
      Set the file offset to the start of GPRS QOS info
    -------------------------------------------------------------------------*/
    offset_position = (qos_set == DS_UMTS_GPRS_QOS_REQ_TYPE ) ?
      DS_UMTS_PDP_PROFILE_GPRS_REQ_QOS_OFFSET : DS_UMTS_PDP_PROFILE_GPRS_MIN_QOS_OFFSET;
  
    if (efs_lseek(fs_handle,
                  offset_position,
                  SEEK_SET) == -1)
    {
      DS_3GPP_MSG3_ERROR( "efs_lseek failed for %d errno = %d", fs_handle, efs_errno, 0 );
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    /*-------------------------------------------------------------------------
      write the GPRS QOS info passed in 
    -------------------------------------------------------------------------*/
    if (ds_umts_pdp_efs_write(fs_handle,
                              (void *)pdp_gprs_qos_data,
                              sizeof(ds_umts_gprs_qos_params_type)) == FALSE)
    {
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------------
      Close the file
    -------------------------------------------------------------------------*/
    if (efs_close (fs_handle) == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_close failed for %d errno = %d", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  }
  /*-------------------------------------------------------------------------
    Update the cache with the new GPRS QOS Info.
  -------------------------------------------------------------------------*/

  DS_UMTS_PDP_TASKLOCK_NON_L4();
  if (qos_set == DS_UMTS_GPRS_QOS_REQ_TYPE) 
  {
    memscpy( (void *)&ds_umts_pdp_profile_ptr->qos_request_gprs,
            sizeof(ds_umts_gprs_qos_params_type),
            (void*) pdp_gprs_qos_data,
            sizeof(ds_umts_gprs_qos_params_type) );
  }       
  else
  {
    memscpy( (void *)&ds_umts_pdp_profile_ptr->qos_minimum_gprs,
	     sizeof(ds_umts_gprs_qos_params_type),
            (void*) pdp_gprs_qos_data,
            sizeof(ds_umts_gprs_qos_params_type) );
  }
  DS_UMTS_PDP_TASKFREE_NON_L4();

  return DS_UMTS_PDP_SUCCESS;
}

/*===========================================================================
FUNCTION DS_UMTS_UPDATE_PDP_PROFILE_SRC_STAT_DESC

DESCRIPTION
  This function updates the source statistics descriptor information with 
  the values passed. It directly overwrites the current values stored in 
  the profile file. It also updates the cache table with the new Info.

PARAMETERS
  profile_number  : profile number
  qos_set         : specifies the qos parameter set ( requested / minimum)
  src_stat_desc   : value of the src_stat_desc
  offset          : Offset to which the above value to be written in EFS.

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system

SIDE EFFECTS 
  none
===========================================================================*/
static ds_umts_pdp_profile_status_etype  
ds_umts_update_pdp_profile_src_stat_desc_info
(
  uint16                        profile_number,
  ds_umts_pdp_subs_e_type       subs_id,
  ds_umts_qos_set_enum_type     qos_set,
  const uint32                  src_stat_desc,
  uint32                        offset
)
{
  int                             fs_handle;
  ds_umts_pdp_profile_type       *ds_umts_pdp_profile_ptr;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------------
    Allow the QOS parameters to be updated independent of the context
    definition.  Check if we are initialized & the profile number is valid
  -------------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();

  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 
  
  /*-------------------------------------------------------------------------
    Update the cache with the new UMTS QOS Info.
  -------------------------------------------------------------------------*/
  ds_umts_pdp_profile_ptr = DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);

  DS_UMTS_PDP_TASKLOCK_NON_L4();
  if (qos_set == DS_UMTS_QOS_REQ_TYPE) 
  {
    memscpy((void *)&ds_umts_pdp_profile_ptr->src_stat_desc_req,
	    sizeof(uint32),
            (void*)&src_stat_desc,
            sizeof(uint32) );
  }       
  else
  {
    memscpy( (void *)&ds_umts_pdp_profile_ptr->src_stat_desc_min,
	     sizeof(uint32),
             (void*)&src_stat_desc,
             sizeof(uint32) );
  } 
  DS_UMTS_PDP_TASKFREE_NON_L4();
  /*-------------------------------------------------------------------------
    Initialize the variables for fs_open                   
  -------------------------------------------------------------------------*/
  if(DS_UMTS_PDP_PROFILE_IS_PERSISTENT(ds_umts_pdp_profile_ptr)) 
  {
    /*-----------------------------------------------------------------------
      Init the fully qualified profile file name to 
      pdp_profiles/<profile_family>_profiles directory
    -----------------------------------------------------------------------*/
    if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
      return DS_UMTS_PDP_FAIL;

    /*-----------------------------------------------------------------------
      Open the file for write 
    -----------------------------------------------------------------------*/
    DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ", 
                            ds_umts_pdp_fully_qual_profile_file_name);

    
    fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                          O_RDWR);
    if ( fs_handle == -1)
    {
      return DS_UMTS_PDP_ACCESS_ERROR;
    }

    if (efs_lseek(fs_handle,
                  offset,
                  SEEK_SET) == -1)
    {
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    /*-------------------------------------------------------------------------
      write the UMTS QOS info passed in 
    -------------------------------------------------------------------------*/
    if (ds_umts_pdp_efs_write(fs_handle,
                              (void *)&src_stat_desc,
                              sizeof(uint32)) == FALSE)
    {
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------------
      Close the file
    -------------------------------------------------------------------------*/
    if (efs_close (fs_handle) == -1)
    {
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  }
  return DS_UMTS_PDP_SUCCESS;
} /* ds_umts_update_pdp_profile_src_stat_desc_info */

/*===========================================================================
FUNCTION DS_3GPP_UPDATE_PDP_PROFILE_LTE_QOS_INFO

DESCRIPTION
  This function updates the LTE qos information with the values passed.
  It directly overwrites the current values stored in the profile file.
  It also updates the cache table with the new LTE QOS info.

PARAMETERS
  profile_number  : profile number
  pdp_lte_qos_data : pointer to the qos data

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  
RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system

SIDE EFFECTS 
  None
===========================================================================*/
static ds_umts_pdp_profile_status_etype  ds_3gpp_update_pdp_profile_lte_qos_info
(   
  uint16                                profile_number,
  ds_umts_pdp_subs_e_type               subs_id,
  const ds_3gpp_lte_qos_params_type    *pdp_lte_qos_data
)
{
  int                             fs_handle;
  fs_off_t                        offset_position;
  ds_umts_pdp_profile_type       *ds_umts_pdp_profile_ptr;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------------
    Allow the QOS parameters to be updated independent of the context
    definition.  Check if we are initialized & the profile number is valid
  -------------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();

  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 

  /*-------------------------------------------------------------------------
    Update the cache with the new LTE QOS Info.
  -------------------------------------------------------------------------*/

  ds_umts_pdp_profile_ptr = DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);

  DS_UMTS_PDP_TASKLOCK_NON_L4();
  memscpy( (void *)&ds_umts_pdp_profile_ptr->qos_request_lte,
  	    sizeof(ds_3gpp_lte_qos_params_type),
            (void*) pdp_lte_qos_data,
            sizeof(ds_3gpp_lte_qos_params_type) );
         
  DS_UMTS_PDP_TASKFREE_NON_L4();

  /*-------------------------------------------------------------------------
    Initialize the variables for fs_open                   
  -------------------------------------------------------------------------*/
  if(DS_UMTS_PDP_PROFILE_IS_PERSISTENT(ds_umts_pdp_profile_ptr)) 
  {
    /*-----------------------------------------------------------------------
      Init the fully qualified profile file name to 
      pdp_profiles directory
    -----------------------------------------------------------------------*/
    if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
      return DS_UMTS_PDP_FAIL;

    /*-----------------------------------------------------------------------
      Open the file for write 
    -----------------------------------------------------------------------*/
    DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ", 
                            ds_umts_pdp_fully_qual_profile_file_name);
    
    fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                          O_RDWR);
    if ( fs_handle == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_open failed for %d errno = %d.", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }

    /*-------------------------------------------------------------------------
      Set the file offset to the start of LTE QOS info
    -------------------------------------------------------------------------*/
    offset_position = DS_3GPP_PDP_PROFILE_LTE_QOS_OFFSET;

    if (efs_lseek(fs_handle,
                  offset_position,
                  SEEK_SET) == -1)
    {
      DS_3GPP_MSG3_ERROR( "efs_lseek failed for %d errno = %d", fs_handle, efs_errno, 0 );
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    /*-------------------------------------------------------------------------
      write the LTE QOS info passed in 
    -------------------------------------------------------------------------*/
    if (ds_umts_pdp_efs_write(fs_handle,
                              (void *)pdp_lte_qos_data,
                              sizeof(ds_3gpp_lte_qos_params_type)) == FALSE)
    {
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }

    /*-------------------------------------------------------------------------
      Close the file
    -------------------------------------------------------------------------*/
    if (efs_close (fs_handle) == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_close failed for %d errno = %d", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  }
  return DS_UMTS_PDP_SUCCESS;
}

/*===========================================================================
FUNCTION DS_UMTS_PDP_GET_NUM_PERSISTENT_PROFILES

DESCRIPTION
  This function goes through all profiles in cache check for 
  valid flag and then check transient flag returns number of
  persistent profile in cache.
  
PARAMETERS
  None

DEPENDENCIES 
  Profiles should be initialized in cache.
  
RETURN VALUE 
  int     :Number of persistent profile in cache.
SIDE EFFECTS 
  None
===========================================================================*/
static int ds_umts_pdp_get_num_persistent_profiles(subs_id)
{
  uint16 profile_number;
  int counter = 0;
  ds_umts_pdp_profile_type *profile_ptr;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  for(profile_number=1;
      profile_number<=DS_UMTS_MAX_PDP_PROFILE_NUM; profile_number++)
  {
    profile_ptr = DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);
    if(((profile_ptr->context).valid_flg) && 
       DS_UMTS_PDP_PROFILE_IS_PERSISTENT( profile_ptr ))
    {
      counter++;
    }
  }
  return counter;
}

/*===========================================================================
FUNCTION DS_UMTS_PDP_CAN_BE_PERSISTENT_PROFILE

DESCRIPTION
  This function goes through all profiles in cache check for 
  valid flag and then check transient flag. If all persistent
  profile slots are taken and the passed argument profile is
  not among the persistent profiles it returns FALSE, else TRUE.
  
PARAMETERS
  None

DEPENDENCIES 
  Profiles should be initialized in cache.
  
RETURN VALUE 
  int     :profile number for first empty slot for 
           persistent profile. 0 if no slot is available.
SIDE EFFECTS 
  None
===========================================================================*/
static boolean ds_umts_pdp_can_be_persistent_profile
(
   uint16                    profile_number,
   ds_umts_pdp_subs_e_type   subs_id
)
{
  ds_umts_pdp_profile_type *profile_ptr;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  profile_ptr = DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);

  /* If profile is already valid return based on profile content */
  if((profile_ptr->context).valid_flg)
  {
    return !((profile_ptr)->transience_flag);
  }
  if(ds_umts_pdp_get_num_persistent_profiles(subs_id) < ds_3gpp_num_persistent_profiles)
  {
    return TRUE;
  }
  return FALSE;
}


/*===========================================================================
FUNCTION DS_3GPP_PROFILE_CACHE_ALLOCATE_PROFILE_CACHE

DESCRIPTION
This function allocates memory for profile cache 
 
INPUT 
   poitner(OUT) that would be assigned upon succfull allocation of memory
 
DEPENDENCIES
  None

RETURN VALUE
  TRUE if successful
  FALSE ohterwise
 
SIDE EFFECTS
  None
===========================================================================*/
static boolean ds_3gpp_profile_cache_allocate_profile_cache
(
   ds_umts_pdp_profile_type **ds_umts_pdp_profile_ptr
)
{
  if (ds_umts_pdp_profile_ptr == NULL)
  {
    DS_3GPP_MSG0_ERROR("ds_umts_pdp_profile_ptr is NULL");
    return FALSE;
  }

  *ds_umts_pdp_profile_ptr = 
            (ds_umts_pdp_profile_type *)modem_mem_alloc(
            (sizeof(ds_umts_pdp_profile_type)), MODEM_MEM_CLIENT_DATA_CRIT);

  if (*ds_umts_pdp_profile_ptr == NULL)
  {
    DS_3GPP_MSG0_ERROR("modem_mem_alloc failed to allocate memory for new profile");
    return FALSE;
  }

  return TRUE;
  
}  

/*===========================================================================
FUNCTION DS_UMTS_UPDATE_PDP_PROFILE_ALL_DATA

DESCRIPTION
  This function updates the profile information with the values passed.
  It directly overwrites the current values stored in the profile file 
  including the profile name
  It also updates the cache with the new profile information.
 
NOTE
  EFS is not updated if transience_flag is set

PARAMETERS
  profile_number  : profile number
  profile_data_ptr    : pointer to the profile information

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  
RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system
  DS_UMTS_PDP_ERR_OUT_OF_PERSISTENT_PROFILES : All persistent profile slots are taken.
                                               Can't create any more persistent profile.
SIDE EFFECTS 
  None
===========================================================================*/
static ds_umts_pdp_profile_status_etype  ds_umts_update_pdp_profile_all_data
(
  uint16                   profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  ds_umts_pdp_profile_type *profile_data_ptr
)
{
  ds_umts_pdp_profile_type *ds_umts_pdp_profile_ptr;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
 
  /*-------------------------------------------------------------------------
    Embed the current profile structure version.
  -------------------------------------------------------------------------*/
  profile_data_ptr->version = DS_UMTS_PROFILE_VERSION;  
  
  /*-------------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();

  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 

   ds_umts_pdp_profile_ptr = 
     DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);

   if (ds_umts_pdp_profile_ptr == NULL)
   {
     if(ds_3gpp_profile_cache_allocate_profile_cache(&ds_umts_pdp_profile_ptr))
     {
       DS_UMTS_PDP_PROFILE_CACHE_SET(subs_id, profile_number, ds_umts_pdp_profile_ptr);
     }
     else
     {
       DS_3GPP_MSG0_HIGH("Failure creating profile, No memory");
       return  DS_UMTS_PDP_FAIL;
     }
   }

  /* if wanted to create persistent profile then */
  if( !(profile_data_ptr->transience_flag) )
  {
   /*-----------------------------------------------------------------------
   Trying to write a persistant profile check whether persistent slot is available.
   If slot is available then profile file may need to be created before we can write.
   If not available print an error and don't write in efs.
   -----------------------------------------------------------------------*/
    if(ds_umts_pdp_can_be_persistent_profile(profile_number,subs_id))
    {
      /*-------------------------------------------------------------------------
        Update the cache with the new profile information.
      -------------------------------------------------------------------------*/
      DS_UMTS_PDP_TASKLOCK_NON_L4();
      memscpy((void *)ds_umts_pdp_profile_ptr,
	      sizeof(ds_umts_pdp_profile_type),
              (void*) profile_data_ptr,
              sizeof(ds_umts_pdp_profile_type) );
      DS_UMTS_PDP_TASKFREE_NON_L4();
      ds_umts_pdp_profile_ptr->transience_flag = FALSE;
      if (!ds_umts_pdp_create_persistent_profile_file(profile_number,subs_id,profile_data_ptr))
      {
        DS_3GPP_MSG3_ERROR("Write failed profile_number %d", profile_number,0, 0);
        ds_umts_pdp_profile_ptr->transience_flag = TRUE;
        return DS_UMTS_PDP_ACCESS_ERROR;
      }
      return DS_UMTS_PDP_SUCCESS;
    }
    else
    {
      /* no profile is created, cache is also not updated */
      DS_3GPP_MSG3_ERROR(" can not create persistent profile", 0, 0, 0);
      return DS_UMTS_PDP_ERR_OUT_OF_PERSISTENT_PROFILES;
    }
  }
  /*-------------------------------------------------------------------------
    Update the cache with the new profile information.
  -------------------------------------------------------------------------*/
  DS_UMTS_PDP_TASKLOCK_NON_L4();
  memscpy((void *)ds_umts_pdp_profile_ptr,
  	  sizeof(ds_umts_pdp_profile_type),
          (void*) profile_data_ptr,
          sizeof(ds_umts_pdp_profile_type) );
  DS_UMTS_PDP_TASKFREE_NON_L4();
  return DS_UMTS_PDP_SUCCESS;
}


/*===========================================================================
FUNCTION DS_UMTS_PDP_INITIALIZE_CONSL_PROFILE_FILES

DESCRIPTION
  This function initializes the profile files for the tethered profile for 
  each profile family type.

PARAMETERS
  prof_profile_req           : variable to store profile reset request
  
DEPENDENCIES 
  profile directories must be initialized.
  
RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_FAIL                 : In case of any error
  
SIDE EFFECTS 
  None
===========================================================================*/
static ds_umts_pdp_profile_status_etype
  ds_umts_pdp_initialize_consl_profile_files(ds_umts_pdp_subs_e_type subs_id)
{
  uint16 profile_num;
  struct fs_dirent        *dirent;
  EFSDIR                  *dirp;
  struct                  fs_stat;
  boolean                 read_default_embedded_num_file;
  boolean                 read_default_tethered_num_file;
  boolean                 profiles_remaining;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  read_default_embedded_num_file = FALSE;
  read_default_tethered_num_file = FALSE;
  profiles_remaining             = TRUE;

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  DS_3GPP_MSG3_HIGH("Initializing Consl Family profile files",0,0,0);
  dirp = efs_opendir(ds_umts_pdp_profile_sub_dir_name_only);
  if (dirp == NULL)
  {
    DS_3GPP_MSG3_ERROR ("Opendir failed (efs_errno %d).\n",
                         efs_errno, 0, 0);
    return DS_UMTS_PDP_FAIL;
  }
  cached_embedded_call_profile_number[subs_id] = DS_3GPP_DEF_EMBEDDED_PROFILE;
  cached_tethered_call_profile_number[subs_id] = DS_3GPP_DEF_TETHERED_PROFILE;

  do
  {
    efs_errno = 0;
    dirent = efs_readdir (dirp);
    profile_num = 0; /* Initialize with invalid profile number */
    if (dirent != NULL && efs_errno == 0)
    {
      /* Skip over . and .. entries. */
      if (((dirent->d_name[0] == '.') && (dirent->d_name[1] == 0)) ||
          ((dirent->d_name[0] == '.') && (dirent->d_name[1] == '.') &&
           (dirent->d_name[2] == 0)))
        continue;
      if( profiles_remaining && 
          (strncmp(dirent->d_name, DS_UMTS_PDP_PROFILE_FILE_NAME,
                 DS_UMTS_PDP_PROFILE_FILE_NAME_NUM_INDEX0) == 0))
      {
        profile_num = (uint16)atoi((char*)(dirent->d_name + \
                                 DS_UMTS_PDP_PROFILE_FILE_NAME_NUM_INDEX0));
        if( ds_umts_pdp_check_profile_number(profile_num,subs_id) ==
            DS_UMTS_PDP_INVALID_PROFILE_NUM )
        {
          DS_3GPP_MSG3_ERROR("Invalid profile %d",profile_num,0,0);
          continue;
        }
        DS_3GPP_MSG3_HIGH("profile %d present in EFS",profile_num,0,0);
  /*-----------------------------------------------------------------------
    Go through all files if present and valid. Populate the cache with 
    profile info check transient flag if persistent profile decrement 
    persistent count else clear cache to default.
  -----------------------------------------------------------------------*/

    if( FALSE ==
        ds_umts_pdp_check_and_init_persistent_profile_file(
                profile_num,subs_id))
    {
      DS_3GPP_MSG3_ERROR("profile file check/create err :%d",profile_num,0,0);
          efs_closedir (dirp);
      return DS_UMTS_PDP_FAIL;
    }
    /*-----------------------------------------------------------------------
      If number of persistent profile read is equal to ds_3gpp_num_persistent_profiles
      stop reading more files 
    -----------------------------------------------------------------------*/

    if(ds_umts_pdp_get_num_persistent_profiles(subs_id) >= 
       ds_3gpp_num_persistent_profiles)
    {
          DS_3GPP_MSG3_HIGH("Read all persistent profiles ",0,0,0);
          profiles_remaining = FALSE;
        }
    }
      /*-----------------------------------------------------------------------
        Check if the pdp_profiles directory has tethered_call_profile_num file
      -----------------------------------------------------------------------*/
      else if( !read_default_tethered_num_file &&
              (strcmp(dirent->d_name, DS_UMTS_PDP_PROFILE_NUM_FOR_TETHERED_FILE_NAME) == 0))
      {
        DS_3GPP_MSG3_HIGH("Tethered call profile num file",0,0,0);
        if(ds_umts_pdp_check_and_init_tethered_call_num_file(subs_id) == FALSE )
        {
          DS_3GPP_MSG3_ERROR("Tethered call prof num file check/create err",0,0,0);
          efs_closedir (dirp);
          return DS_UMTS_PDP_FAIL;
        }
        read_default_tethered_num_file = TRUE;
      }
      /*-----------------------------------------------------------------------
        Check if the pdp_profiles directory has embedded_call_profile_num file
      -----------------------------------------------------------------------*/
      else if( !read_default_embedded_num_file &&
              (strcmp(dirent->d_name, DS_UMTS_PDP_PROFILE_NUM_FOR_EMBEDDED_FILE_NAME) == 0))
      {
        DS_3GPP_MSG3_HIGH("embedded call profile num file",0,0,0);
        if(ds_umts_pdp_check_and_init_embedded_call_num_file(subs_id) == FALSE )
        {
          DS_3GPP_MSG3_ERROR("embedded call prof num file check/create err",0,0,0);
          efs_closedir (dirp);
          return DS_UMTS_PDP_FAIL;
        }
        read_default_embedded_num_file = TRUE;
      }
      else
      {
        DS_3GPP_MSG3_ERROR("Non data profile file",0,0,0);
    }
      /*-----------------------------------------------------------------------
        If number of persistent profile read is equal to ds_3gpp_num_persistent_profiles
        and default num tethered and default num embedded files are read stop 
        reading more files 
      -----------------------------------------------------------------------*/

      if( (profiles_remaining == FALSE) &&
          (read_default_embedded_num_file) &&
          (read_default_tethered_num_file) )
      {
        DS_3GPP_MSG3_HIGH("Read all files ",0,0,0);
        break;
  }
    }  /* if (dirent != NULL && efs_errno == 0) */
  }while (dirent != NULL);
  efs_closedir (dirp);
  return DS_UMTS_PDP_SUCCESS;
}


/*===========================================================================
FUNCTION DS_UMTS_PDP_INIT_ALL_CACHE_PROFILE_INFO

DESCRIPTION
  This function updates the cache with default profile information.
  
PARAMETERS
  None

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  
RETURN VALUE 
  TRUE              : the operation completed successfully
  FALSE              : Fails to initialize cache.
SIDE EFFECTS 
  None
===========================================================================*/
static boolean ds_umts_pdp_init_all_cache_profile_info
(
   ds_umts_pdp_subs_e_type  subs_id
)
{
  uint16                    profile_num;
  ds_umts_pdp_profile_type *profile_data_ptr;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  for(profile_num=1;
      profile_num<=DS_UMTS_MAX_PDP_PROFILE_NUM; profile_num++)
  {
    /*-----------------------------------------------------------------------
      Init the fully qualified profile file name to 
      "pdp_profiles" directory
    -----------------------------------------------------------------------*/
    if(ds_umts_pdp_set_qualified_profile_file_name(profile_num,subs_id) == FALSE)
    {
      return FALSE;
    }

    /*-----------------------------------------------------------------------
      Store the profile values for the ATCOP family in the atcop cache table.
    -----------------------------------------------------------------------*/
    
    /*-------------------------------------------------------------------------
      Write the default profile values
      assign the profile structure version identifer.
      update the context number in the default profile before writing
      write the profile name  to the default profile . Write the profile
      file name as the default name that we assign during creation
      profile name has been set by the macro 
      DS_UMTS_PDP_SET_QUALIFIED_PROFILE_ATCOP_FILE_NAME , 
      DS_UMTS_PDP_SET_QUALIFIED_PROFILE_RMNET_FILE_NAMEand 
      DS_UMTS_PDP_SET_QUALIFIED_PROFILE_SOCKETS_FILE_NAME
      e.g. profile number 3 would have been set to "profile3"
    -------------------------------------------------------------------------*/
    ds_umts_pdp_default_profile.context.pdp_context_number = profile_num;

#ifdef FEATURE_PROFILE_OTAP_ENHANCEMENTS
    ds_umts_string_to_wide_string(
       (const byte *)ds_umts_pdp_profile_file_name,
        (uint16 *)ds_umts_pdp_default_profile.profile_name,
       sizeof(ds_umts_pdp_default_profile.profile_name));
#else
  if(strlcpy((char *)ds_umts_pdp_default_profile.profile_name,
         ds_umts_pdp_profile_file_name,
         sizeof(ds_umts_pdp_default_profile.profile_name)) >(int) sizeof(ds_umts_pdp_default_profile.profile_name) )
  {
    DS_3GPP_MSG3_ERROR("strlcpy failed ",0,0,0);
    return FALSE;
  }
#endif

  /*--------------------------------------------------------------------------
    memcpy default profile data in cache, read profile name from file update
    cache with the read name. Update the file data with default valuethe default profile values starting from version identifier.
  -------------------------------------------------------------------------*/
   profile_data_ptr = DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_num);

   if (profile_data_ptr == NULL)
   {
     if(ds_3gpp_profile_cache_allocate_profile_cache(&profile_data_ptr))
     {
       DS_UMTS_PDP_PROFILE_CACHE_SET(subs_id, profile_num,profile_data_ptr);
     }
     else
     {
       DS_3GPP_MSG0_HIGH("Failure creating profile, No memory");
       return  DS_UMTS_PDP_FAIL;
     } 
   }

    memscpy(profile_data_ptr, sizeof(ds_umts_pdp_profile_type),
          &ds_umts_pdp_default_profile, 
          sizeof(ds_umts_pdp_profile_type)); 
  }
  return TRUE;
}

/*===========================================================================
FUNCTION DS_UMTS_PDP_SET_DS_UMTS_PDP_PROFILE_SUB_DIR_NAME

DESCRIPTION
  This function initializes the PDP profile sub dir
PARAMETERS 
  None

DEPENDENCIES 

RETURN VALUE 

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype ds_umts_pdp_profile_set_sub_dir_name
(
   ds_umts_pdp_subs_e_type  subs_id
)
{
  /*-----------------------------------------------------------------------*/
    switch (subs_id)
    {
       case DS_UMTS_PDP_SUBSCRIPTION_NONE:
         if(strlcpy(ds_umts_pdp_profile_sub_dir_name,ds_umts_pdp_profile_dir_name,
                    sizeof(ds_umts_pdp_profile_sub_dir_name) ) >=
                    (int) sizeof(ds_umts_pdp_profile_sub_dir_name) )
         {
           ds_umts_pdp_reg_init_fail_cause = DS_PROFILE_COPY_DIR_NAME_FAILURE;
           return DS_UMTS_PDP_FAIL;
         }

         strlcat(ds_umts_pdp_profile_sub_dir_name,DS_UMTS_SUB1_DIR_NAME,
                 DS_UMTS_PDP_PROFILE_SUB_DIR_NAME_LEN);
         break;

       case DS_UMTS_PDP_SUBSCRIPTION_1:

         if(strlcpy(ds_umts_pdp_profile_sub_dir_name,ds_umts_pdp_profile_dir_name,
                    sizeof(ds_umts_pdp_profile_sub_dir_name) ) >=
                    (int) sizeof(ds_umts_pdp_profile_sub_dir_name) )
         {
           ds_umts_pdp_reg_init_fail_cause = DS_PROFILE_COPY_DIR_NAME_FAILURE;
           return DS_UMTS_PDP_FAIL;
         }

         strlcat(ds_umts_pdp_profile_sub_dir_name,DS_UMTS_SUB2_DIR_NAME,
                 DS_UMTS_PDP_PROFILE_SUB_DIR_NAME_LEN);
         break;
#ifdef FEATURE_TRIPLE_SIM
       case DS_UMTS_PDP_SUBSCRIPTION_2:
         if(strlcpy(ds_umts_pdp_profile_sub_dir_name,ds_umts_pdp_profile_dir_name,
                    sizeof(ds_umts_pdp_profile_sub_dir_name) ) >=
                    (int) sizeof(ds_umts_pdp_profile_sub_dir_name) )
         {
           ds_umts_pdp_reg_init_fail_cause = DS_PROFILE_COPY_DIR_NAME_FAILURE;
           return DS_UMTS_PDP_FAIL;
         }

         strlcat(ds_umts_pdp_profile_sub_dir_name,DS_UMTS_SUB3_DIR_NAME,
                 DS_UMTS_PDP_PROFILE_SUB_DIR_NAME_LEN);
         break;
#endif


       default:
         DS_3GPP_MSG0_ERROR("Invalid subscription");
         break;

    }


    ds_umts_pdp_profile_sub_dir_name[DS_UMTS_PDP_PROFILE_SUB_DIR_NAME_LEN - 2] = 
      DS_FS_DIRECTORY_CHAR;

    ds_umts_pdp_profile_sub_dir_name[DS_UMTS_PDP_PROFILE_SUB_DIR_NAME_LEN - 1] = '\0';

    return DS_UMTS_PDP_SUCCESS;
}

/*===========================================================================
                   EXTERNAL FUNCTION DEFINITIONS
===========================================================================*/

/*===========================================================================
FUNCTION DS_UMTS_PDP_PROFILE_REGISTRY_INIT_INTERNAL

DESCRIPTION
  This function initializes the PDP profile registry. It checks the EFS to 
  see if the required profile files are present. If the files are not present,
  it creates the files and initializes them.
  The embedded call profile number is read to initialize the cached value.
  The default embedded call profile is reset after initialization if profile
  data was undefined.
  The default atcop and rmnet call profile is reset after initialization if profile
  data was undefined.
  
PARAMETERS 
  None

DEPENDENCIES 
  Should be called only ONCE during startup.

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS  - initialization was successfull
  DS_UMTS_PDP_FAIL     - initialization failed

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype ds_umts_pdp_registry_init_internal(void)
{
  struct fs_stat sbuf;
  ds_umts_pdp_profile_status_etype status;
  boolean          is_valid_flag;
  ds_umts_pdp_subs_e_type subs_id;

#ifdef FEATURE_DATA_PS_IPV6
  ds_ip_version_enum_type def_ip_version = DS_IP_V4V6;
#else
  ds_ip_version_enum_type def_ip_version = DS_IP_V4;
#endif /* FEATURE_DATA_PS_IPV6 */

  static boolean called_once = FALSE;
  byte  profile_num, def_teth_profile_num, def_emb_profile_num;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if (!called_once) 
  {
    called_once = TRUE;
  }
  else
  {
    /*-------------------------------------------------------------------------
      PDP reg has already been inited. Just return Success
    -------------------------------------------------------------------------*/
    return DS_UMTS_PDP_SUCCESS;
  }


   /*-------------------------------------------------------------------------- 
    Update PDP reg init fail cause  to NONE
  --------------------------------------------------------------------------*/
  ds_umts_pdp_reg_init_fail_cause = DS_PDP_REG_NO_FAILURE;

  /* read number of persistent profile from NV */
  if (ds_3gpp_cfg_get( DS_3GPP_NUM_PERSISTENT_PROFILES, 
                      (uint8 *)&(ds_3gpp_num_persistent_profiles), 
                       sizeof(ds_3gpp_num_persistent_profiles)
                     ) > 0) 
  {
    DS_3GPP_MSG3_HIGH("ds_3gpp_num_persistent_profiles set to %d in EFS", 
                      ds_3gpp_num_persistent_profiles, 0, 0);
  }
  else
  {
    ds_3gpp_num_persistent_profiles = DS_UMTS_MAX_PDP_PROFILE_NUM;/* defaults to 16 */
    DS_3GPP_MSG3_HIGH("ds_3gpp_num_persistent_profiles not set in EFS default value %d", 
                      ds_3gpp_num_persistent_profiles, 0, 0);
  }
    /* Verify the ds_3gpp_num_persistent_profiles value */
  if( ds_3gpp_num_persistent_profiles == 0 ||
      ds_3gpp_num_persistent_profiles > DS_UMTS_MAX_PDP_PROFILE_NUM )
  {
    ds_3gpp_num_persistent_profiles = DS_UMTS_MAX_PDP_PROFILE_NUM;/* defaults to 16 */
    DS_3GPP_MSG3_HIGH("Incorrect value of ds_3gpp_num_persistent_profiles"
                      "setting it to default %d", 
                      ds_3gpp_num_persistent_profiles, 0, 0);

  }

  for (subs_id = DS_UMTS_PDP_SUBSCRIPTION_NONE; subs_id < DS_SUBSCRIPTION_MAX;
        subs_id++)
  {

    /*-------------------------------------------------------------------------
      Set the default values for a profile. All fields are set to 0x0.
      The schema version is set based on header constant.
      APN and profile name being ASCIZ strings are initialized to null strings
      Other parameters have the value '0' as subscribed
      Flags have the value 0x0 indicating that the set is invalid
    -------------------------------------------------------------------------*/
    memset((void*)&ds_umts_pdp_default_profile,
           0x0,sizeof(ds_umts_pdp_profile_type) );
    ds_umts_pdp_default_profile.version = DS_UMTS_PROFILE_VERSION;
    ds_umts_pdp_default_profile.apn_bearer = 
                          DS_UMTS_PDP_PROFILE_APN_BEARER_FLAG_ALL;

    ds_umts_pdp_default_profile.max_pdn_conn_per_blk = 
      DS_UMTS_PDP_DEF_MAX_PDN_CONN_PER_BLK;
    ds_umts_pdp_default_profile.pdn_req_wait_time = 
      DS_UMTS_PDP_DEF_PDN_REQ_WAIT_TIME;
    ds_umts_pdp_default_profile.max_pdn_conn_time = 
      DS_UMTS_PDP_DEF_MAX_PDN_CONN_TIME;
    /*-------------------------------------------------------------------------
      By default all profiles now are created transient.
    -------------------------------------------------------------------------*/
    ds_umts_pdp_default_profile.transience_flag = TRUE;

    /*-------------------------------------------------------------------------
      Set the default values for a profile. All fields are set to 0x0.
      The schema version is set based on header constant.
      APN and profile name being ASCIZ strings are initialized to null strings
      Other parameters have the value '0' as subscribed
      Flags have the value 0x0 indicating that the set is invalid
    -------------------------------------------------------------------------*/
    memset( (void*)&ds_umts_pdp_default_tethered_profile,
            0x0,sizeof(ds_umts_pdp_profile_type) );
    ds_umts_pdp_default_tethered_profile.version = DS_UMTS_PROFILE_VERSION;
    ds_umts_pdp_default_tethered_profile.apn_bearer = 
                                   DS_UMTS_PDP_PROFILE_APN_BEARER_FLAG_ALL;
    /*-------------------------------------------------------------------------
      Check the file system to see if the profiles are present
      - If not create and initialize the files
      - Initialize the Directory Name to the qualified name "pdp_profiles/"
    -------------------------------------------------------------------------*/
    /*lint -e534 ignoring return code from function*/
    if(strlcpy(ds_umts_pdp_profile_dir_name,DS_UMTS_PDP_PROFILE_DIR_NAME,
                   sizeof(ds_umts_pdp_profile_dir_name) ) >=
       (int) sizeof(ds_umts_pdp_profile_dir_name) )
    {
      ds_umts_pdp_reg_init_fail_cause = DS_PROFILE_COPY_DIR_NAME_FAILURE;
      return DS_UMTS_PDP_FAIL;
    }

    ds_umts_pdp_profile_dir_name[DS_UMTS_PDP_PROFILE_DIR_NAME_LEN-1] = 
      DS_FS_DIRECTORY_CHAR;

    ds_umts_pdp_profile_dir_name[DS_UMTS_PDP_PROFILE_DIR_NAME_LEN] = 
      '\0';

   
    if(ds_umts_pdp_profile_set_sub_dir_name(subs_id) != DS_UMTS_PDP_SUCCESS)
    {
      return DS_UMTS_PDP_FAIL;
    }


    if(strlcpy(ds_umts_pdp_profile_sub_dir_name_only,
               ds_umts_pdp_profile_sub_dir_name,
               DS_UMTS_PDP_PROFILE_SUB_DIR_NAME_LEN - 1) >=
               DS_UMTS_PDP_PROFILE_SUB_DIR_NAME_LEN)
    {
      ds_umts_pdp_reg_init_fail_cause = DS_PROFILE_COPY_DIR_NAME_FAILURE;
      return DS_UMTS_PDP_FAIL;
    }

    if( strlcpy(ds_umts_pdp_profile_file_name,
                    DS_UMTS_PDP_PROFILE_FILE_NAME,
                    sizeof(ds_umts_pdp_profile_file_name)) >=
        (int) sizeof(ds_umts_pdp_profile_file_name) )
    {
      DS_3GPP_MSG3_ERROR( "Copying template profile name failed(strlcpy) failed",
                          0, 0, 0 );
      ds_umts_pdp_reg_init_fail_cause = DS_PROFILE_COPY_FILE_NAME_FAILURE;
      return DS_UMTS_PDP_FAIL;
    }
    /*-------------------------------------------------------------------------
      Initialize the cache with default data,
    -------------------------------------------------------------------------*/  
    if(ds_umts_pdp_init_all_cache_profile_info(subs_id) == FALSE)
    {
      DS_3GPP_MSG3_ERROR("ds_umts_pdp_init_all_cache_profile_info failed",0,0,0);
      ds_umts_pdp_reg_init_fail_cause = DS_PROFILE_INIT_CACHE_FAILURE;
      return DS_UMTS_PDP_FAIL;
    }
    /*lint +e534*/
      /*-----------------------------------------------------------------------
        Check the file system to see if the profiles are present
        - If not create and initialize the files
        - Initialize the Directory Name to the qualified name 
          "pdp_profiles"
      -----------------------------------------------------------------------*/
      /*lint -e534 ignoring return code from function*/

    DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_dir_name: %s ", 
                            ds_umts_pdp_profile_sub_dir_name);

  #ifdef TEST_FRAMEWORK
    #error code not present
#else
    if(efs_stat((char*) ds_umts_pdp_profile_sub_dir_name, 
  #endif
                &sbuf) == 0)
    {
      DS_3GPP_MSG3_HIGH( "pdp_profiles dir present", 0, 0, 0 );
        
      /*-----------------------------------------------------------------------
        Initialize ds_3gpp_num_persistent_profiles number of persistent profile
        files. Read through all files for transient flag based on that select 
        persistent profiles.
      -----------------------------------------------------------------------*/
      status = ds_umts_pdp_initialize_consl_profile_files(subs_id);
      if(status != DS_UMTS_PDP_SUCCESS)
      {
        DS_3GPP_MSG3_ERROR("ds_umts_pdp_initialize_consl_profile_files error %d",
                           status,0,0);
        return status;
      }
    } /* pdp profiles dir present */
    else
    {
      /*-----------------------------------------------------------------------
       Create the profile directory and default profile file 
      -----------------------------------------------------------------------*/
      if( ds_umts_pdp_create_and_init_default_profile(subs_id)== FALSE )
      {
        DS_3GPP_MSG3_ERROR("profile file creation / init error",0,0,0);
        ds_umts_pdp_reg_init_fail_cause =
           DS_PROFILE_CREATE_DEFAULT_PROFILE_FAILURE;
        return DS_UMTS_PDP_FAIL;
      }

      /*-----------------------------------------------------------------------
       Default embedded call profile reset
      -----------------------------------------------------------------------*/

      /*-------------------------------------------------------------------------
        Init the fully qualified profile file name to pdp_profiles directory
      -------------------------------------------------------------------------*/
      if(strlcpy( ds_umts_pdp_fully_qual_embedded_call_file_name,
                  ds_umts_pdp_profile_sub_dir_name,
                  sizeof(ds_umts_pdp_fully_qual_embedded_call_file_name) ) >=
         (int) sizeof(ds_umts_pdp_fully_qual_embedded_call_file_name) )
      {
        ds_umts_pdp_registry_init_flag = FALSE;
        ds_umts_pdp_reg_init_fail_cause =
           DS_PROFILE_COPY_DIR_NAME_FAILURE;
        return FALSE;
      }

      strlcat( ds_umts_pdp_fully_qual_embedded_call_file_name,
               DS_UMTS_PDP_PROFILE_NUM_FOR_EMBEDDED_FILE_NAME,
               sizeof(ds_umts_pdp_fully_qual_embedded_call_file_name) );
      /*-----------------------------------------------------------------------
        Reset the default tethered profile to default values.
      -----------------------------------------------------------------------*/
      ds_umts_pdp_registry_init_flag = TRUE;
      if( ds_3gpp_num_persistent_profiles != 1 &&
          DS_3GPP_DEF_TETHERED_PROFILE == DS_3GPP_DEF_EMBEDDED_PROFILE)
      {
        status = ds_umts_set_pdp_profile_all_data_to_default_per_subs(
                    DS_3GPP_DEF_TETHERED_PROFILE, 
                    subs_id,
                    def_ip_version);

        if (status != DS_UMTS_PDP_SUCCESS)
        {
          ds_umts_pdp_reg_init_fail_cause =
           DS_PROFILE_SET_DEFAULT_TETHERED_PROFILE_FAILURE;
          ds_umts_pdp_registry_init_flag = FALSE;
          return DS_UMTS_PDP_FAIL;
        }
      }
      /*-----------------------------------------------------------------------
       Default tethered call profile reset
      -----------------------------------------------------------------------*/
      /*-------------------------------------------------------------------------
        Init the fully qualified profile file name to 
        pdp_profiles directory
      -------------------------------------------------------------------------*/
      if( strlcpy( ds_umts_pdp_fully_qual_tethered_call_file_name,
                   ds_umts_pdp_profile_sub_dir_name,
                   sizeof(ds_umts_pdp_fully_qual_tethered_call_file_name) ) >=
      (int) sizeof(ds_umts_pdp_fully_qual_tethered_call_file_name) )
      {
        ds_umts_pdp_reg_init_fail_cause =
           DS_PROFILE_COPY_DIR_NAME_FAILURE;
        ds_umts_pdp_registry_init_flag = FALSE;
        return FALSE;
      }

      strlcat( ds_umts_pdp_fully_qual_tethered_call_file_name,
               DS_UMTS_PDP_PROFILE_NUM_FOR_TETHERED_FILE_NAME,
               sizeof(ds_umts_pdp_fully_qual_tethered_call_file_name));
      /*-----------------------------------------------------------------------
        Reset the default embedded profile to default values.
      -----------------------------------------------------------------------*/
        status = ds_umts_set_pdp_profile_all_data_to_default_per_subs(
                    DS_3GPP_DEF_EMBEDDED_PROFILE, 
                    subs_id,
                    def_ip_version);
        if (status != DS_UMTS_PDP_SUCCESS)
        {
          ds_umts_pdp_reg_init_fail_cause =
           DS_PROFILE_SET_DEFAULT_EMBEDDED_PROFILE_FAILURE;
          ds_umts_pdp_registry_init_flag = FALSE;
          return DS_UMTS_PDP_FAIL;
        }

    } /* pdp profiles dir not present */
    
    DS_3GPP_MSG3_HIGH("PDP profiles initialized ",0,0,0);
      
    ds_umts_pdp_registry_init_flag = TRUE;
    
    /*-------------------------------------------------------------------------
      Reset the default tethered profile to default values.
      This must be done after the library has been initialized.
    
      If the valid flag for default profile was set to FALSE,
      we need to still reset the profile data to default
    -------------------------------------------------------------------------*/
    if(ds_umts_get_pdp_profile_num_for_tethered_calls_per_subs
       (&def_teth_profile_num,subs_id) != DS_UMTS_PDP_SUCCESS)
    {
      def_teth_profile_num = DS_3GPP_DEF_TETHERED_PROFILE;
    }


    status = ds_umts_get_pdp_profile_context_info_is_valid_per_subs(
                def_teth_profile_num,
                subs_id,
                &is_valid_flag);

    if (status == DS_UMTS_PDP_SUCCESS && is_valid_flag == FALSE)
    {
      /*-----------------------------------------------------------------------
        Reset the default tethered profile to default values.
      -----------------------------------------------------------------------*/
      if( ds_3gpp_num_persistent_profiles != 1 &&
          DS_3GPP_DEF_TETHERED_PROFILE == DS_3GPP_DEF_EMBEDDED_PROFILE)
      {
        status = ds_umts_set_pdp_profile_all_data_to_default_per_subs(
                    DS_3GPP_DEF_TETHERED_PROFILE, 
                    subs_id,
                    def_ip_version);

        if (status != DS_UMTS_PDP_SUCCESS)
        {
          ds_umts_pdp_registry_init_flag = FALSE;
          ds_umts_pdp_reg_init_fail_cause = 
            DS_PROFILE_SET_DEFAULT_TETHERED_PROFILE_FAILURE;
          return DS_UMTS_PDP_FAIL;
        }
      }
      /*-----------------------------------------------------------------------
        Reset tethered call profile number file to point to default.
      -----------------------------------------------------------------------*/
        if( !ds_umts_update_default_tethered_prof_num(DS_3GPP_DEF_TETHERED_PROFILE,
                                                      subs_id) )
        {
          ds_umts_pdp_registry_init_flag = FALSE;
          ds_umts_pdp_reg_init_fail_cause = 
            DS_PROFILE_UPDATE_TETHERED_PROFILE_NUM_FAILURE;
          return DS_UMTS_PDP_FAIL;
        }
    }
    /*-------------------------------------------------------------------------
      Read the tethered call profile number to initialize the cached value
      if still undefined.
    -------------------------------------------------------------------------*/
    else if( 0 == cached_tethered_call_profile_number[subs_id])
    {
      if( DS_UMTS_PDP_SUCCESS !=
          ds_umts_get_pdp_profile_num_for_tethered_calls_per_subs( &profile_num,
                                                                   subs_id ) )
      {
        DS_3GPP_MSG3_ERROR("rmnet call profile number read",0,0,0);
        ds_umts_pdp_registry_init_flag = FALSE;
        ds_umts_pdp_reg_init_fail_cause = 
          DS_PROFILE_GET_TETHERED_PROFILE_NUM_FAILURE;
        return DS_UMTS_PDP_FAIL;
      }
      /*-----------------------------------------------------------------------
        Check the profile number ?. It should be in the range of 1 to 
        DS_UMTS_MAX_PDP_PROFILE_NUM. If not set it to profile #1
      -----------------------------------------------------------------------*/
      if( profile_num < 1 || profile_num > DS_UMTS_MAX_PDP_PROFILE_NUM )
      {
        DS_3GPP_MSG3_HIGH(" Invalid profile number for embedded calls:reset to 1",0,0,0);
        if(DS_UMTS_PDP_SUCCESS !=
           ds_umts_set_pdp_profile_num_for_tethered_calls_per_subs 
           (DS_3GPP_DEF_TETHERED_PROFILE),subs_id )
        {
          ds_umts_pdp_registry_init_flag = FALSE;
          ds_umts_pdp_reg_init_fail_cause = 
            DS_PROFILE_UPDATE_TETHERED_PROFILE_NUM_FAILURE;
          return DS_UMTS_PDP_FAIL;
        }
      }
    }

    /*-------------------------------------------------------------------------
      Reset the default embedded profile to default values.
      This must be done after the library has been initialized.

      If the valid flag for default profile was set to FALSE,
      we need to still reset the profile data to default
    -------------------------------------------------------------------------*/
    if(ds_umts_get_pdp_profile_num_for_embedded_calls_per_subs(&def_emb_profile_num,
                                    subs_id)  != DS_UMTS_PDP_SUCCESS)
    {
      def_emb_profile_num = DS_3GPP_DEF_EMBEDDED_PROFILE;
    }

    status = ds_umts_get_pdp_profile_context_info_is_valid_per_subs(
                                     def_emb_profile_num,
                                     subs_id,
                                     &is_valid_flag);
    if( status == DS_UMTS_PDP_SUCCESS && is_valid_flag == FALSE )
    {
      /*-----------------------------------------------------------------------
        Reset the default embedded profile to default values.
      -----------------------------------------------------------------------*/
        status = ds_umts_set_pdp_profile_all_data_to_default_per_subs(
                    DS_3GPP_DEF_EMBEDDED_PROFILE, 
                    subs_id,
                    def_ip_version);
        if (status != DS_UMTS_PDP_SUCCESS)
        {
          ds_umts_pdp_registry_init_flag = FALSE;
          ds_umts_pdp_reg_init_fail_cause = 
            DS_PROFILE_SET_DEFAULT_EMBEDDED_PROFILE_FAILURE;
          return DS_UMTS_PDP_FAIL;
        }

      /*-----------------------------------------------------------------------
        Reset embedded call profile number file to point to default.
      -----------------------------------------------------------------------*/
        if( !ds_umts_update_embedded_call_prof_num(DS_3GPP_DEF_EMBEDDED_PROFILE,
            subs_id ))
        {
          ds_umts_pdp_registry_init_flag = FALSE;
          ds_umts_pdp_reg_init_fail_cause =
             DS_PROFILE_UPDATE_EMBEDDED_PROFILE_NUM_FAILURE;
          return DS_UMTS_PDP_FAIL;
        }
    }

    /*-------------------------------------------------------------------------
      Read the embedded call profile number to initialize the cached value
      if still undefined.
    -------------------------------------------------------------------------*/
    else if( 0 == cached_embedded_call_profile_number[subs_id] )
    {
      if( DS_UMTS_PDP_SUCCESS !=
          ds_umts_get_pdp_profile_num_for_embedded_calls_per_subs( &profile_num,subs_id ) )
      {
        DS_3GPP_MSG3_ERROR("embedded call profile number read",0,0,0);
        ds_umts_pdp_registry_init_flag = FALSE;
        ds_umts_pdp_reg_init_fail_cause = 
          DS_PROFILE_GET_EMBEDDED_PROFILE_NUM_FAILURE;
        return DS_UMTS_PDP_FAIL;
      }
      /*-----------------------------------------------------------------------
        Check the profile number ?. It should be in the range of 1 to 
        DS_UMTS_MAX_PDP_PROFILE_NUM. If not set it to profile #1
      -----------------------------------------------------------------------*/
      if( profile_num < 1 || profile_num > DS_UMTS_MAX_PDP_PROFILE_NUM )
      {
        DS_3GPP_MSG3_HIGH(" Invalid profile number for embedded calls:reset to 1",0,0,0);
        if(DS_UMTS_PDP_SUCCESS !=
           ds_umts_set_pdp_profile_num_for_embedded_calls_per_subs
           (DS_3GPP_DEF_EMBEDDED_PROFILE,subs_id))
        {
          ds_umts_pdp_registry_init_flag = FALSE;
          ds_umts_pdp_reg_init_fail_cause =
             DS_PROFILE_UPDATE_EMBEDDED_PROFILE_NUM_FAILURE;
          return DS_UMTS_PDP_FAIL;
        }
      }
    }
  }

  return DS_UMTS_PDP_SUCCESS;
}   

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_ALL_DATA_INTERNAL

DESCRIPTION
  This function retrieves all the parameters associated with a profile number. 
  
  NOTE:
  No check is done to see if a valid context is defined. The caller has to 
  check if the context and associated parameter set is defined and use it
  accordingly.

PARAMETERS
  profile_number  : profile number
  profile_data_ptr    : pointer to store the profile information

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype  ds_umts_get_pdp_profile_all_data_internal
(
  uint16                    profile_number, 
  ds_umts_pdp_subs_e_type   subs_id,
  ds_umts_pdp_profile_type *profile_data_ptr
)
{
  boolean  status;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();

  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 

  /*-------------------------------------------------------------------------
    Read the profile from the cache
  -------------------------------------------------------------------------*/
  status = ds_umts_get_pdp_profile_all_info_from_cache(
            profile_number,
            subs_id,
            profile_data_ptr);

  if( status != TRUE )
  {
    DS_3GPP_MSG1_ERROR("PDP profile access error:%d",status);
    return DS_UMTS_PDP_ACCESS_ERROR;
  }
  
  /*-------------------------------------------------------------------------
    Pass the profile as is to the caller. Caller has to check
    if the context is defined explicitly (check the valid_flg) and 
    also for the validity for other parameter sets
  -------------------------------------------------------------------------*/
  return DS_UMTS_PDP_SUCCESS;
}
/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_NAME_UTF16_INTERNAL

DESCRIPTION
  This function retrieves the name of the specified profile number that is
  stored in UTF16 format. 
  Note : This information can be retrieved even if the context (profile) has
  not been defined

PARAMETERS
  profile_number  : profile number
  profile_name_buf    : pointer to store the name string (UTF16 string)
  profile_name_buf_len: length of the profile_name_buf passed (UTF16 words)

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system
  DS_UMTS_PDP_INSUFFICIENT_OUTPUT_BUFFER : length of profile_name_buf is shorter
                                      than required

SIDE EFFECTS 
  None
===========================================================================*/
/*ARGSUSED*/
ds_umts_pdp_profile_status_etype  ds_umts_get_pdp_profile_name_utf16_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  uint16                   *profile_name_buf,
  byte                      profile_name_buf_len
)
{
#ifndef FEATURE_PROFILE_OTAP_ENHANCEMENTS
  DS_3GPP_MSG3_ERROR("Format not supported", 0, 0, 0);
  return DS_UMTS_PDP_FORMAT_NOT_SUPPORTED;
#else
  boolean  status;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 

  /*-------------------------------------------------------------------------
    Check if the buffer passed in is atleast the length required
  -------------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_OUT_BUFF_LEN(profile_name_buf_len,
        DS_UMTS_MAX_PROFILE_NAME_LEN);
  DS_UMTS_PDP_ALLOC_LOCAL_PDP_PROFILE( ds_umts_pdp_local_profile_ptr );

  /*-------------------------------------------------------------------------
    Read the profile from the cache.
  -------------------------------------------------------------------------*/
  status = ds_umts_get_pdp_profile_all_info_from_cache(
                                            profile_number,
                                            subs_id,
                                            ds_umts_pdp_local_profile_ptr);
  if( status != TRUE )
  {
    DS_UMTS_PDP_FREE_LOCAL_PDP_PROFILE(ds_umts_pdp_local_profile_ptr);
    DS_3GPP_MSG3_ERROR("PDP profile access error:%d",status,0,0);
    return DS_UMTS_PDP_ACCESS_ERROR;
  }
  
  /*-------------------------------------------------------------------------
    Profile name is not dependent on having a valid profile. Just
    return the string currently in the profile
  -------------------------------------------------------------------------*/
  memscpy( (void*)profile_name_buf,
           sizeof(uint16)*profile_name_buf_len,
           (void*) ds_umts_pdp_local_profile_ptr->profile_name,
           sizeof(uint16)*profile_name_buf_len );
  DS_UMTS_PDP_FREE_LOCAL_PDP_PROFILE(ds_umts_pdp_local_profile_ptr);

  return DS_UMTS_PDP_SUCCESS;
#endif
}

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_NAME_INTERNAL

DESCRIPTION
  This function retrieves the name of the specified profile number . 
  Note : This information can be retrieved even if the context (profile) has
  not been defined

PARAMETERS
  profile_number  : profile number
  profile_name_buf    : pointer to store the name string
  profile_name_buf_len: length of the profile_name_buf passed

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system
  DS_UMTS_PDP_INSUFFICIENT_OUTPUT_BUFFER : length of profile_name_buf is shorter
                                      than required

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype  ds_umts_get_pdp_profile_name_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  byte                      *profile_name_buf,
  byte                      profile_name_buf_len
)
{
#ifdef FEATURE_PROFILE_OTAP_ENHANCEMENTS
  DS_3GPP_MSG3_ERROR("Format not supported", 0, 0, 0);
  return DS_UMTS_PDP_FORMAT_NOT_SUPPORTED;
#else
  boolean  status;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 

  /*-------------------------------------------------------------------
    Check if the buffer passed in is atleast the length required
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_OUT_BUFF_LEN(profile_name_buf_len,
        DS_UMTS_MAX_PROFILE_NAME_LEN);
  DS_UMTS_PDP_ALLOC_LOCAL_PDP_PROFILE( ds_umts_pdp_local_profile_ptr );

  /*-------------------------------------------------------------------
    Read the profile from the cache
  -------------------------------------------------------------------*/
  status = ds_umts_get_pdp_profile_all_info_from_cache(
                                            profile_number,
                                            subs_id,
                                            ds_umts_pdp_local_profile_ptr);
  if( status != TRUE )
  {
    DS_UMTS_PDP_FREE_LOCAL_PDP_PROFILE(ds_umts_pdp_local_profile_ptr);
    DS_3GPP_MSG3_ERROR("PDP profile access error:%d",status,0,0);
    return DS_UMTS_PDP_ACCESS_ERROR;
  }
  
  /*-------------------------------------------------------------------
    Profile name is not dependent on having a valid profile. Just
    return the string currently in the profile
  -------------------------------------------------------------------*/
  /*lint -e534 ignoring return code from function*/
  strlcpy( (char*)profile_name_buf,
          (char*)ds_umts_pdp_local_profile_ptr->profile_name,
          profile_name_buf_len);
  /*lint +e534*/

  DS_UMTS_PDP_FREE_LOCAL_PDP_PROFILE(ds_umts_pdp_local_profile_ptr);

  return DS_UMTS_PDP_SUCCESS;
#endif /* FEATURE_PROFILE_OTAP_ENHANCEMENTS */
}

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_REQUEST_PCSCF_ADDRESS_FLAG_INTERNAL

DESCRIPTION
  This function retrieves the "request_pcscf_address" flag specified for a 
  profile. 
  The "request_pcscf_address" indicates if the pcscf address should be
  requested for this profile or not.
  Note : This information can be retrieved even if the context (profile) has
  not been defined

PARAMETERS
  profile_number  : profile number
  request_pcscf_address_flag : pointer to store request_pcscf_address_flag


DEPENDENCIES
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system

SIDE EFFECTS
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype 
  ds_umts_get_pdp_profile_request_pcscf_address_flag_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  boolean                   *request_pcscf_address_flag
)
{

  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 


  /*-------------------------------------------------------------------
    Read the request_pcscf_address_flag info from the cache table.
  -------------------------------------------------------------------*/

  DS_UMTS_PDP_TASKLOCK_NON_L4();  

  *request_pcscf_address_flag = 
      (DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number))->request_pcscf_address_flag;

  DS_UMTS_PDP_TASKFREE_NON_L4();

  return DS_UMTS_PDP_SUCCESS;
}

/*===========================================================================
FUNCTION 
  DS_UMTS_GET_PDP_PROFILE_REQUEST_PCSCF_ADDRESS_USING_DHCP_FLAG_INTERNAL

DESCRIPTION
  This function retrieves the "request_pcscf_address_using_dhcp_flag" flag
  specified for a profile. 
  The "request_pcscf_address_using_dhcp_flag" indicates if the pcscf address
  should be requested for this profile using DHCP or not.
  Note : This information can be retrieved even if the context (profile) has
  not been defined

PARAMETERS
  profile_number  : profile number
  request_pcscf_address_using_dhcp_flag : pointer to store
                                        request_pcscf_address_using_dhcp_flag


DEPENDENCIES
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system

SIDE EFFECTS
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype 
  ds_umts_get_pdp_profile_request_pcscf_address_using_dhcp_flag_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  boolean                   *request_pcscf_address_using_dhcp_flag
)
{
  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 
  
   /*-------------------------------------------------------------------
    Read the otap_enabled_flag info from the cache table.
  -------------------------------------------------------------------*/

  DS_UMTS_PDP_TASKLOCK_NON_L4();
  *request_pcscf_address_using_dhcp_flag = 
      DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number)->
      request_pcscf_address_using_dhcp_flag;

  DS_UMTS_PDP_TASKFREE_NON_L4();
  
  return DS_UMTS_PDP_SUCCESS;  
}

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_IM_CN_FLAG_INTERNAL

DESCRIPTION
  This function retrieves the "im_cn" flag specified for a 
  profile.
  The "im_cn" indicates if the dedicated context should be
  requested for this profile or not.
  Note : This information can be retrieved even if the context (profile) has
  not been defined

PARAMETERS
  profile_number  : profile number
  im_cn_flag : pointer to store im_cn_flag


DEPENDENCIES
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system

SIDE EFFECTS
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype 
  ds_umts_get_pdp_profile_im_cn_flag_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  boolean                   *im_cn_flag
)
{

  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 

  /*-------------------------------------------------------------------
    Read the im_cn_flag info from the cache table.
  -------------------------------------------------------------------*/

  DS_UMTS_PDP_TASKLOCK_NON_L4();  
  *im_cn_flag = 
      DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number)->
      im_cn_flag;

  DS_UMTS_PDP_TASKFREE_NON_L4();

  return DS_UMTS_PDP_SUCCESS;
}

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_APN_DISABLE_FLAG_INTERNAL

DESCRIPTION
  This function calls the registry internal function to retrieve the 
  "apn_diable_flag" mechanism specified in the context information for a 
  profile number. This parameter is retrieved only if a valid context is 
  defined for the profile specified.

PARAMETERS
  profile_number   : profile number
  apn_disable_flag : pointer to the boolean flag

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  A valid context must have been defined for the profile number

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system
  DS_UMTS_PDP_CONTEXT_NOT_DEFINED  : valid context not defined for profile

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype 
ds_umts_get_pdp_profile_apn_disable_flag_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  boolean                  *apn_disable_flag
)
{

  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 

  /*-------------------------------------------------------------------
    Read the im_cn_flag info from the cache table.
  -------------------------------------------------------------------*/

  DS_UMTS_PDP_TASKLOCK_NON_L4();  
  *apn_disable_flag = 
      DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number)->apn_disable_flag;

  DS_UMTS_PDP_TASKFREE_NON_L4();

  return DS_UMTS_PDP_SUCCESS;
} /* ds_umts_get_pdp_profile_context_info_apn_disable_flag_internal */

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_ROAMING_DISALLOWED_FLAG_INTERNAL

DESCRIPTION
  This function calls the registry internal function to retrieve the 
  "roaming_disallowed_flag" mechanism specified in the context information for a 
  profile number. This parameter is retrieved only if a valid context is 
  defined for the profile specified.

PARAMETERS
  profile_number          : profile number
  roaming_disallowed      : pointer to the boolean flag

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  A valid context must have been defined for the profile number

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system
  DS_UMTS_PDP_CONTEXT_NOT_DEFINED  : valid context not defined for profile

SIDE EFFECTS 
  none
===========================================================================*/
ds_umts_pdp_profile_status_etype 
  ds_umts_get_pdp_profile_roaming_disallowed_flag_internal
(
  uint16   profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  boolean  *roaming_disallowed
)
{
  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 

  /*-------------------------------------------------------------------
    Read the im_cn_flag info from the cache table.
  -------------------------------------------------------------------*/

  DS_UMTS_PDP_TASKLOCK_NON_L4();  
  *roaming_disallowed = 
      DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number)->roaming_disallowed;

  DS_UMTS_PDP_TASKFREE_NON_L4();

  return DS_UMTS_PDP_SUCCESS;
}

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_PDN_DISCON_WAIT_TIME_INTERNAL

DESCRIPTION
  This function calls the registry internal function to retrieve the 
  "pdn_discon_wait_time" mechanism specified in the context information for a 
  profile number. This parameter is retrieved only if a valid context is 
  defined for the profile specified.

PARAMETERS
  profile_number          : profile number
  pdn_discon_wait_time    : pointer to the pdn_discon_wait_time

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  A valid context must have been defined for the profile number

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system
  DS_UMTS_PDP_CONTEXT_NOT_DEFINED  : valid context not defined for profile

SIDE EFFECTS 
  none
===========================================================================*/
ds_umts_pdp_profile_status_etype 
  ds_umts_get_pdp_profile_pdn_discon_wait_time_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  uint8                     *pdn_discon_wait_time
)
{
  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 

  /*-------------------------------------------------------------------
    Read the im_cn_flag info from the cache table.
  -------------------------------------------------------------------*/

  DS_UMTS_PDP_TASKLOCK_NON_L4();  
  *pdn_discon_wait_time = 
      DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number)->pdn_discon_wait_time;

  DS_UMTS_PDP_TASKFREE_NON_L4();

  return DS_UMTS_PDP_SUCCESS;
}

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_INACTIVITY_TIMER_VAL_INTERNAL

DESCRIPTION
  This function calls the registry internal function to retrieve the 
  "inactivity_timer_val" value specified in the context information for a 
  profile number. This parameter is retrieved only if a valid context is 
  defined for the profile specified.

PARAMETERS
  profile_number       : profile number
  inactivity_timer_val : pointer to the inactivity_timer_val

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  A valid context must have been defined for the profile number

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system
  DS_UMTS_PDP_CONTEXT_NOT_DEFINED  : valid context not defined for profile

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype 
ds_umts_get_pdp_profile_inactivity_timer_val_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  uint32                    *inactivity_timer_val
)
{

  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 

  /*-------------------------------------------------------------------
    Read the im_cn_flag info from the cache table.
  -------------------------------------------------------------------*/

  DS_UMTS_PDP_TASKLOCK_NON_L4();  
  *inactivity_timer_val = 
      DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number)->
      inactivity_timer_val;

  DS_UMTS_PDP_TASKFREE_NON_L4();

  return DS_UMTS_PDP_SUCCESS;
} /* ds_umts_get_pdp_profile_inactivity_timer_val_internal */

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_APN_CLASS_INTERNAL

DESCRIPTION
  This function calls the registry internal function to retrieve the 
  "apn_class" for a specified profile number. 
  This parameter is retrieved only if a valid context is 
  defined for the profile specified.

PARAMETERS
  profile_number   : profile number
  apn_class        : pointer to the uint8 (to hold APN class)

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  A valid context must have been defined for the profile number

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system
  DS_UMTS_PDP_CONTEXT_NOT_DEFINED  : valid context not defined for profile

SIDE EFFECTS 
  none
===========================================================================*/
ds_umts_pdp_profile_status_etype 
ds_umts_get_pdp_profile_apn_class_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  uint8                     *apn_class
)
{

  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 


  /*-------------------------------------------------------------------
    Read the APN class info from the cache table.
  -------------------------------------------------------------------*/

  DS_UMTS_PDP_TASKLOCK_NON_L4();  
  *apn_class = 
      DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number)->apn_class;

  DS_UMTS_PDP_TASKFREE_NON_L4();

  return DS_UMTS_PDP_SUCCESS;
} /* ds_umts_get_pdp_profile_apn_class_internal */


/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_APN_BEARER_INTERNAL

DESCRIPTION
  This function calls the registry internal function to retrieve the 
  "apn_bearer" for a specified profile number. 
  This parameter is retrieved only if a valid context is 
  defined for the profile specified.

PARAMETERS
  profile_number   : profile number
  apn_bearer       : pointer to the uint8 (to hold APN class)

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  A valid context must have been defined for the profile number

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system
  DS_UMTS_PDP_CONTEXT_NOT_DEFINED  : valid context not defined for profile

SIDE EFFECTS 
  none
===========================================================================*/
ds_umts_pdp_profile_status_etype 
ds_umts_get_pdp_profile_apn_bearer_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  uint8                    *apn_bearer
)
{
  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 

  /*-------------------------------------------------------------------
    Read the APN class info from the cache table.
  -------------------------------------------------------------------*/

  DS_UMTS_PDP_TASKLOCK_NON_L4();  
  *apn_bearer = DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number)->apn_bearer;

  DS_UMTS_PDP_TASKFREE_NON_L4();

  return DS_UMTS_PDP_SUCCESS;
} /* ds_umts_get_pdp_profile_apn_bearer_internal */


/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_MCC_INTERNAL

DESCRIPTION
  This function calls the registry internal function to retrieve the 
  "MCC" for a specified profile number. 
  This parameter is retrieved only if a valid context is 
  defined for the profile specified.

PARAMETERS
  profile_number   : profile number
  mcc              : pointer to the uint16 (to hold MCC)

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  A valid context must have been defined for the profile number

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  
SIDE EFFECTS 
  none
===========================================================================*/
ds_umts_pdp_profile_status_etype 
ds_umts_get_pdp_profile_mcc_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  uint16                    *mcc
)
{
  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 

  /*-------------------------------------------------------------------
    Read the mcc info from the cache table.
  -------------------------------------------------------------------*/

  DS_UMTS_PDP_TASKLOCK_NON_L4();  
  *mcc = DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number)->mcc;
  DS_UMTS_PDP_TASKFREE_NON_L4();

  return DS_UMTS_PDP_SUCCESS;
} /* ds_umts_get_pdp_profile_mcc_internal */

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_MNC_INTERNAL

DESCRIPTION
  This function calls the registry internal function to retrieve the 
  "MNC" for a specified profile number. 
  This parameter is retrieved only if a valid context is 
  defined for the profile specified.

PARAMETERS
  profile_number   : profile number
  mnc              : pointer to the struct (to hold MNC)

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  A valid context must have been defined for the profile number

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  
SIDE EFFECTS 
  none
===========================================================================*/
ds_umts_pdp_profile_status_etype 
ds_umts_get_pdp_profile_mnc_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  ds_umts_3gpp_mnc_type    *mnc
)
{
  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 

  /*-------------------------------------------------------------------
    Read the mnc class info from the cache table.
  -------------------------------------------------------------------*/

  DS_UMTS_PDP_TASKLOCK_NON_L4();  
  *mnc = DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number)->mnc;
  DS_UMTS_PDP_TASKFREE_NON_L4();

  return DS_UMTS_PDP_SUCCESS;
} /* ds_umts_get_pdp_profile_mnc_internal */

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_OPERATOR_RESERVED_PCO_INTERNAL

DESCRIPTION
  This function calls the registry internal function to retrieve the 
  "MNC" for a specified profile number. 
  This parameter is retrieved only if a valid context is 
  defined for the profile specified.

PARAMETERS
  profile_number   : profile number
  mnc              : pointer to the uint16 (to hold operator reserved PCO)

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  A valid context must have been defined for the profile number

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  
SIDE EFFECTS 
  none
===========================================================================*/
ds_umts_pdp_profile_status_etype 
ds_umts_get_pdp_profile_operator_reserved_pco_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  uint16                    *pco
)
{
    /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 

  /*-------------------------------------------------------------------
    Read the operator_reserved_pco class info from the cache table.
  -------------------------------------------------------------------*/

  DS_UMTS_PDP_TASKLOCK_NON_L4();  
  *pco = DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number)->
    operator_reserved_pco;
  DS_UMTS_PDP_TASKFREE_NON_L4();

  return DS_UMTS_PDP_SUCCESS;
} /* ds_umts_get_pdp_profile_operator_reserved_pco_internal */

#ifdef FEATURE_DATA_LTE

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_MAX_PDN_CONN_PER_BLK_INTERNAL

DESCRIPTION
  This function calls the registry internal function to retrieve the 
  Max PDN Conn Per Blk for a specified profile number. 
  This parameter is retrieved only if a valid context is 
  defined for the profile specified.

PARAMETERS
  profile_number   : profile number
  max_pdn_conn_per_blk: pointer to the uint16 (to hold max_pdn_conn_per_blk)

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  A valid context must have been defined for the profile number

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  
SIDE EFFECTS 
  none
===========================================================================*/
ds_umts_pdp_profile_status_etype 
ds_umts_get_pdp_profile_max_pdn_conn_per_blk_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  uint16                   *max_pdn_conn_per_blk
)
{
  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 

  /*-------------------------------------------------------------------
    Read the mnc class info from the cache table.
  -------------------------------------------------------------------*/

  DS_UMTS_PDP_TASKLOCK_NON_L4();  
  *max_pdn_conn_per_blk = DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number)->max_pdn_conn_per_blk;
  DS_UMTS_PDP_TASKFREE_NON_L4();

  DS_3GPP_MSG3_MED("max_pdn_conn_per_blk read is %d", *max_pdn_conn_per_blk, 0, 0);

  return DS_UMTS_PDP_SUCCESS;
}

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_PDN_REQ_WAIT_TIME_INTERNAL

DESCRIPTION
  This function calls the registry internal function to retrieve the 
  PDN Req Wait Time for a specified profile number. 
  This parameter is retrieved only if a valid context is 
  defined for the profile specified.

PARAMETERS
  profile_number   : profile number
  pdn_req_wait_time: pointer to the uint16 (to hold pdn_req_wait_time)

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  A valid context must have been defined for the profile number

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  
SIDE EFFECTS 
  none
===========================================================================*/
ds_umts_pdp_profile_status_etype
ds_umts_get_pdp_profile_pdn_req_wait_time_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  uint16                    *pdn_req_wait_time
)
{
  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 

  /*-------------------------------------------------------------------
    Read the mnc class info from the cache table.
  -------------------------------------------------------------------*/

  DS_UMTS_PDP_TASKLOCK_NON_L4();  
  *pdn_req_wait_time = DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number)->pdn_req_wait_time;
  DS_UMTS_PDP_TASKFREE_NON_L4();

  DS_3GPP_MSG3_MED("pdn_req_wait_time read is %d", *pdn_req_wait_time, 0, 0);
  return DS_UMTS_PDP_SUCCESS;
}

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_MAX_PDN_CONN_TIME_INTERNAL

DESCRIPTION
  This function calls the registry internal function to retrieve the 
  Max PDN Conn Time for a specified profile number. 
  This parameter is retrieved only if a valid context is 
  defined for the profile specified.

PARAMETERS
  profile_number   : profile number
  max_pdn_conn_time: pointer to the uint16 (to hold max_pdn_conn_time)

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  A valid context must have been defined for the profile number

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  
SIDE EFFECTS 
  none
===========================================================================*/
ds_umts_pdp_profile_status_etype 
ds_umts_get_pdp_profile_max_pdn_conn_time_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  uint16                    *max_pdn_conn_time
)
{
  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 

  /*-------------------------------------------------------------------
    Read the mnc class info from the cache table.
  -------------------------------------------------------------------*/

  DS_UMTS_PDP_TASKLOCK_NON_L4();  
  *max_pdn_conn_time = DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number)->max_pdn_conn_time;
  DS_UMTS_PDP_TASKFREE_NON_L4();

  DS_3GPP_MSG3_MED("max_pdn_conn_time read is %d", *max_pdn_conn_time, 0, 0);
  return DS_UMTS_PDP_SUCCESS;
} /* ds_umts_get_pdp_profile_max_pdn_conn_time_internal */

#endif /*FEATURE_DATA_LTE*/
/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_USER_APP_DATA_INTERNAL
===========================================================================*/
/** 
  
  This function gets the value of parameter
  "user_app_data" for a specified profile number. 

  @profile_number            : profile number
  @user_app_data             : uint32 value being passed in for the
                               user_app_data

  DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  A valid context must have been defined for the profile number

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  
SIDE EFFECTS 
  none

*/
ds_umts_pdp_profile_status_etype 
ds_umts_get_pdp_profile_user_app_data_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  uint32                   *user_app_data
)
{
  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 

  /*-------------------------------------------------------------------
    Read the mnc class info from the cache table.
  -------------------------------------------------------------------*/

  DS_UMTS_PDP_TASKLOCK_NON_L4();  
  *user_app_data = DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number)->user_app_data;
  DS_UMTS_PDP_TASKFREE_NON_L4();

  DS_3GPP_MSG3_MED("user_app_data read is %d", *user_app_data, 0, 0);
  return DS_UMTS_PDP_SUCCESS;
}

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_TRANSIENCE_FLAG

DESCRIPTION
  This function calls the registry internal function to retrieve the 
  "transience_flag" for a specified profile number. 
  This parameter is retrieved only if a valid context is 
  defined for the profile specified.

PARAMETERS
  profile_number         : profile number
  transience_flag        : pointer to boolean (to hold transience_flag)

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  A valid context must have been defined for the profile number

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system
  DS_UMTS_PDP_CONTEXT_NOT_DEFINED  : valid context not defined for profile

SIDE EFFECTS 
  none
===========================================================================*/
ds_umts_pdp_profile_status_etype 
ds_umts_get_pdp_profile_transience_flag
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  boolean                   *transience_flag
)
{

  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 

  /*-------------------------------------------------------------------
    Read the transience flag from the cache table.
  -------------------------------------------------------------------*/

  DS_UMTS_PDP_TASKLOCK_NON_L4();  
  *transience_flag = 
      DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number)->transience_flag;

  DS_UMTS_PDP_TASKFREE_NON_L4();

  return DS_UMTS_PDP_SUCCESS;
  
} /* ds_umts_get_pdp_profile_transience_flag */

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_OTAP_NAPID_INTERNAL

DESCRIPTION
  This function retrieves the NAPID of the specified profile number . 
  Note : This information can be retrieved even if the context (profile) has
  not been defined

PARAMETERS
  profile_number          : profile number
  otap_napid_buf          : pointer to store the napid string (UTF8 string)
  otap_napid_buf_len      : length of the otap_napid_buf passed (UTF8 bytes)

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system
  DS_UMTS_PDP_INSUFFICIENT_OUTPUT_BUFFER : length of otap_napid_buf is shorter
                                      than required

SIDE EFFECTS 
  None
===========================================================================*/
/*ARGSUSED*/
ds_umts_pdp_profile_status_etype  ds_umts_get_pdp_otap_napid_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  byte                     *otap_napid_buf,
  byte                      otap_napid_buf_len
)
{
#ifndef FEATURE_PROFILE_OTAP_ENHANCEMENTS
  DS_3GPP_MSG3_ERROR("PDP profile API not supported", 0, 0, 0);
  return DS_UMTS_PDP_API_NOT_SUPPORTED;
#else
  boolean  status;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM;

  /*-------------------------------------------------------------------
    Check if the buffer passed in is atleast the length required
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_OUT_BUFF_LEN(otap_napid_buf_len,
        DS_UMTS_MAX_OTAP_NAPID_LEN);
  DS_UMTS_PDP_ALLOC_LOCAL_PDP_PROFILE( ds_umts_pdp_local_profile_ptr );

  /*-------------------------------------------------------------------
    Read the profile from the cache
  -------------------------------------------------------------------*/
  status = ds_umts_get_pdp_profile_all_info_from_cache(
                                            profile_number,
                                            subs_id,
                                            ds_umts_pdp_local_profile_ptr);
  if( status != TRUE )
  {
    DS_UMTS_PDP_FREE_LOCAL_PDP_PROFILE(ds_umts_pdp_local_profile_ptr);
    DS_3GPP_MSG3_ERROR("PDP profile access error:%d",status,0,0);
    return DS_UMTS_PDP_ACCESS_ERROR;
  }
  
  /*-------------------------------------------------------------------
   Return the napid currently in the profile.
  -------------------------------------------------------------------*/
  memscpy( (void*)otap_napid_buf,
           otap_napid_buf_len,
           (void*)ds_umts_pdp_local_profile_ptr->otap_napid,
           otap_napid_buf_len );
  DS_UMTS_PDP_FREE_LOCAL_PDP_PROFILE(ds_umts_pdp_local_profile_ptr);

  return DS_UMTS_PDP_SUCCESS;
#endif
}

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_IS_READ_ONLY_INTERNAL

DESCRIPTION
  This function retrieves the "read_only_flag" specified for a profile. 
  The "read_only_flag" indicates if the profile is read-only (TRUE) or 
  not (FALSE)
  Note : This information can be retrieved even if the context (profile) has
  not been defined

PARAMETERS
  profile_number  : profile number
  read_only_flag  : pointer to store the read_only_flag

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype  ds_umts_get_pdp_profile_is_read_only_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  boolean                  *read_only_flag
)
{
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 

  /*-------------------------------------------------------------------
    Read the read_only_flag info from the cache table.
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_TASKLOCK_NON_L4();
  *read_only_flag = 
    DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number)->read_only_flag;
  DS_UMTS_PDP_TASKFREE_NON_L4();
  return DS_UMTS_PDP_SUCCESS;
}

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_IS_OTAP_ENABLED_INTERNAL

DESCRIPTION
  This function retrieves the "otap_enabled_flag" specified for a profile. 
  The "otap_enabled_flag" indicates if the context is otap_enabled (TRUE) or 
  not (FALSE)
  Note : This information can be retrieved even if the context (profile) has
  not been defined

PARAMETERS
  profile_number     : profile number
  otap_enabled_flag  : pointer to store the otap_enabled_flag

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype 
 ds_umts_get_pdp_profile_is_otap_enabled_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  boolean                  *otap_enabled_flag
)
{

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 

  /*-------------------------------------------------------------------
    Read the otap_enabled_flag info from the cache table.
  -------------------------------------------------------------------*/

  DS_UMTS_PDP_TASKLOCK_NON_L4();  
  *otap_enabled_flag = 
      DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number)->otap_enabled_flag;
  DS_UMTS_PDP_TASKFREE_NON_L4();
  return DS_UMTS_PDP_SUCCESS;
}

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_CONTEXT_INFO_INTERNAL

DESCRIPTION
  This function retrieves the context information for a profile number. 
  This information is retrieved only if the context definition is valid
  (valid_flg for context definition is TRUE)

PARAMETERS
  profile_number  : profile number
  pdp_context_data: pointer to store the context information

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system
  DS_UMTS_PDP_CONTEXT_NOT_DEFINED  : valid context not defined for profile

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype 
  ds_umts_get_pdp_profile_context_info_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  ds_umts_pdp_context_type  *pdp_context_data
)
{
  boolean status;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 

  /*-------------------------------------------------------------------
    Read the context info from the cache. 
  -------------------------------------------------------------------*/
  status = ds_umts_get_pdp_profile_context_info_from_cache(
                                                profile_number,
                                                subs_id,
                                                pdp_context_data );
  if( status != TRUE )
  {
    DS_3GPP_MSG3_ERROR("PDP profile access error %d",status,0,0);
    return DS_UMTS_PDP_ACCESS_ERROR;
  }
  
  if( !pdp_context_data->valid_flg )
  {
    DS_3GPP_MSG3_HIGH("Context not defined %d", profile_number, 0, 0);
    return DS_UMTS_PDP_CONTEXT_NOT_DEFINED;
  }

  return DS_UMTS_PDP_SUCCESS;
}

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_CONTEXT_INFO_IS_VALID_INTERNAL

DESCRIPTION
  This function retrieves the "valid_flg" specified in the context 
  information for a profile number. The "valid_flg" indicates if the 
  context has been defined (TRUE) or not (FALSE)

PARAMETERS
  profile_number  : profile number
  valid_flg      : pointer to store the valid_flg

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_get_pdp_profile_context_info_is_valid_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  boolean                  *valid_flg
)
{

  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 

  /*-------------------------------------------------------------------
    Read the valid_flg info from the cache table.
  -------------------------------------------------------------------*/

  DS_UMTS_PDP_TASKLOCK_NON_L4();
  *valid_flg = 
        DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number)->context.valid_flg;
  DS_UMTS_PDP_TASKFREE_NON_L4();
  return DS_UMTS_PDP_SUCCESS;
}

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_CONTEXT_INFO_PDP_ADDRESS_INTERNAL

DESCRIPTION
  This function retrieves the "pdp address" specified in the context 
  information for a profile number. This parameter is retrieved only 
  if a valid context is defined for the profile specified.

PARAMETERS
  profile_number  : profile number
  pdp_address     : pointer to store the pdp address

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  A valid context must have been defined for the profile number

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system
  DS_UMTS_PDP_CONTEXT_NOT_DEFINED  : valid context not defined for profile

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_get_pdp_profile_context_info_pdp_address_internal
(  
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  ds_umts_pdp_addr_type     *pdp_address 
)
{
  ds_umts_pdp_profile_status_etype status;
  ds_umts_pdp_context_type    pdp_context_data;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------
    read the context info from the cache. 
    ds_umts_pdp_get_profile_context_info_from_cache
    performs err check on profile number & init flag & returns
    a context only if it is valid
  -------------------------------------------------------------------*/
  status = ds_umts_get_pdp_profile_context_info(
            profile_number,
            &pdp_context_data);

  if( status != DS_UMTS_PDP_SUCCESS )
  {
    DS_3GPP_MSG3_ERROR("PDP profile access error:%d",status,0,0);
  }
  else
  {
    /*-------------------------------------------------------------------
      Valid context , retrieve the address
    -------------------------------------------------------------------*/
    *pdp_address = pdp_context_data.pdp_addr;
  }

  return status;
}

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_CONTEXT_INFO_APN_INTERNAL

DESCRIPTION
  This function retrieves the "apn" specified in the context information 
  for a profile number. This parameter is retrieved only if a valid 
  context is defined for the profile specified.

PARAMETERS
  profile_number  : profile number
  apn_buf         : pointer to store the apn string
  apn_buf_len     : length of the buffer pointed by apn_buf

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  A valid context must have been defined for the profile number

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system
  DS_UMTS_PDP_CONTEXT_NOT_DEFINED  : valid context not defined for profile
  DS_UMTS_PDP_INSUFFICIENT_OUTPUT_BUFFER : length of apn_buf is shorter than 
                                      required

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_get_pdp_profile_context_info_apn_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  byte                     *apn_buf,
  byte                      apn_buf_len
)
{
  ds_umts_pdp_profile_status_etype status;
  ds_umts_pdp_context_type    pdp_context_data;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-----------------------------------------------------------------------
    Check if we have been initialized & the in/out paramaters are correct
    ds_umts_pdp_get_profile_context_info_from_cache
    performs err check on profile number & init flag & returns
    a context only if it is valid .so just check for the output buffer len
  ------------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_OUT_BUFF_LEN(apn_buf_len,DS_UMTS_MAX_APN_STRING_LEN);

  /*-------------------------------------------------------------------
    read the context info from the file. An error could also indicate 
    that the context has not been defined
  -------------------------------------------------------------------*/
  status = ds_umts_get_pdp_profile_context_info_per_subs(
                                           profile_number,
										   subs_id,
                                           &pdp_context_data);

  if( status != DS_UMTS_PDP_SUCCESS )
  {
    DS_3GPP_MSG3_ERROR("PDP profile access error:%d",status,0,0);
  }
  else
  {
    /*-------------------------------------------------------------------
      Valid context , retrieve the apn
    -------------------------------------------------------------------*/
    /*lint -e534 ignoring return code from function*/
    strlcpy((char*)apn_buf,(char*)pdp_context_data.apn, apn_buf_len);
    /*lint +e534*/
  }

  return status;
}

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_CONTEXT_INFO_IPV4_ADDR_ALLOC_INTERNAL

DESCRIPTION
  This function calls the registry internal function to retrieve the "ipv4
  addr alloc" mechanism specified in the context information for a profile 
  number. This parameter is retrieved only if a valid context is defined for 
  the profile specified.

PARAMETERS
  profile_number  : profile number
  ipv4_addr_alloc : pointer to store the ipv4 addr alloc

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  A valid context must have been defined for the profile number

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system
  DS_UMTS_PDP_CONTEXT_NOT_DEFINED  : valid context not defined for profile

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype 
ds_umts_get_pdp_profile_context_info_ipv4_addr_alloc_internal
(
  uint16                              profile_number,
  ds_umts_pdp_subs_e_type             subs_id,
  ds_umts_pdp_ipv4_addr_alloc_e_type *ipv4_addr_alloc
)
{
  ds_umts_pdp_profile_status_etype status;
  ds_umts_pdp_context_type    pdp_context_data;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------
    read the context info from the cache. 
    ds_umts_pdp_get_profile_context_info_from_cache
    performs err check on profile number & init flag & returns
    a context only if it is valid
  -------------------------------------------------------------------*/
  status = ds_umts_get_pdp_profile_context_info_per_subs(
            profile_number,subs_id,
            &pdp_context_data);

  if( status != DS_UMTS_PDP_SUCCESS )
  {
    DS_3GPP_MSG3_ERROR("PDP profile access error:%d",status,0,0);
  }
  else
  {
    /*-------------------------------------------------------------------
      Valid context , retrieve the address
    -------------------------------------------------------------------*/
    *ipv4_addr_alloc = pdp_context_data.ipv4_addr_alloc;
  }

  return status;
}

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_AUTH_INFO_INTERNAL

DESCRIPTION
  This function retrieves the authentication information for a profile number.

  The auth info is returned ir-respective of the state of the context definition
  for the profile.

PARAMETERS
  profile_number  : profile number
  pdp_auth_data   : pointer to store the authentication information

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype  ds_umts_get_pdp_profile_auth_info_internal
(
  uint16                     profile_number,
  ds_umts_pdp_subs_e_type    subs_id,
  ds_umts_pdp_auth_type     *pdp_auth_data
)
{
  ds_umts_pdp_profile_status_etype status;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  DS_UMTS_PDP_ALLOC_LOCAL_PDP_PROFILE( ds_umts_pdp_local_profile_ptr );

  /*-------------------------------------------------------------------
    Get the complete profile using ds_umts_get_pdp_profile_all_data
    It performs the err check on profile number & init flag
    The profile is returned ONLY if a valid context is defined.
  -------------------------------------------------------------------*/
  status = ds_umts_get_pdp_profile_all_data_internal(
                                       profile_number,
                                       subs_id,
                                       ds_umts_pdp_local_profile_ptr);

  if( status == DS_UMTS_PDP_SUCCESS )
  {
    /*-------------------------------------------------------------------
      Get the auth info 
    -------------------------------------------------------------------*/
    memscpy( (void*) pdp_auth_data,
             sizeof(ds_umts_pdp_auth_type),
             (void*) &(ds_umts_pdp_local_profile_ptr->auth),
             sizeof(ds_umts_pdp_auth_type) );
  }

  DS_UMTS_PDP_FREE_LOCAL_PDP_PROFILE(ds_umts_pdp_local_profile_ptr);

  return status;  
}

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_AUTH_INFO_TYPE_INTERNAL

DESCRIPTION
  This function retrieves the "authentication type" specified in the
  authentication information for a profile number . This parameter is 
  retrieved ir-respective of the state of the context definition of
  the profile

PARAMETERS
  profile_number  : profile number
  auth_type       : pointer to store the authentication type

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype  ds_umts_get_pdp_profile_auth_info_type_internal
(
  uint16                     profile_number,
  ds_umts_pdp_subs_e_type    subs_id,
  ds_umts_pdp_auth_enum_type *auth_type
)
{
  ds_umts_pdp_profile_status_etype status;
  ds_umts_pdp_auth_type  pdp_auth_data;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------
    Get the auth info completely and then just return the auth type
    ds_umts_get_pdp_profile_auth_info performs checks on init flag and
    profile number. 
  -------------------------------------------------------------------*/
  status = ds_umts_get_pdp_profile_auth_info(
                                        profile_number,
                                        &pdp_auth_data);

  if( status == DS_UMTS_PDP_SUCCESS )
  {
    /*-------------------------------------------------------------------
      Get the auth type 
    -------------------------------------------------------------------*/
    *auth_type = pdp_auth_data.auth_type;
  }
  return status;
}

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_AUTH_INFO_USERNAME_INTERNAL

DESCRIPTION
  This function retrieves the "username" specified in the authentication 
  information for a profile number . This parameter is retrieved 
  ir-respective of the state of the context definition of the profile

PARAMETERS
  profile_number  : profile number
  username_buf    : pointer to store the username string
  username_buf_len: length of the username_buf passed

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system
  DS_UMTS_PDP_INSUFFICIENT_OUTPUT_BUFFER : length of username_buf is shorter than 
                                      required

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_get_pdp_profile_auth_info_username_internal
(
  uint16                     profile_number,
  ds_umts_pdp_subs_e_type    subs_id,
  byte                       *username_buf,
  byte                       username_buf_len
)
{
  ds_umts_pdp_profile_status_etype status;
  ds_umts_pdp_auth_type  pdp_auth_data;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
 
  /*-------------------------------------------------------------------
    Get the auth info completely and then just return the username
    ds_umts_get_pdp_profile_auth_info performs checks on init flag and
    profile number. 
  
    Check the length of the output buffer here.
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_OUT_BUFF_LEN(username_buf_len,DS_UMTS_MAX_QCPDP_STRING_LEN);

  status = ds_umts_get_pdp_profile_auth_info(
                                        profile_number,
                                        &pdp_auth_data);

  if( status == DS_UMTS_PDP_SUCCESS )
  {
    /*-------------------------------------------------------------------
      Get the username which is a ASCIZ string
    -------------------------------------------------------------------*/
    /*lint -e534 ignoring return code from function*/
    strlcpy((char*)username_buf,(char*)pdp_auth_data.username,
                username_buf_len);
    /*lint +e534*/
  }
  return status;
}

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_AUTH_INFO_PASSWORD_INTERNAL

DESCRIPTION
  This function retrieves the password specified in the authentication 
  information for a profile number . This parameter is retrieved 
  ir-respective of the state of the context definition of the profile

PARAMETERS
  profile_number  : profile number
  password_buf    : pointer to store the password string
  password_buf_len: length of the password_buf passed

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system
  DS_UMTS_PDP_INSUFFICIENT_OUTPUT_BUFFER : length of password_buf is shorter than 
                                      required

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_get_pdp_profile_auth_info_password_internal
(
  uint16                     profile_number,
  ds_umts_pdp_subs_e_type    subs_id,
  byte                       *password_buf,
  byte                       password_buf_len
)
{
  ds_umts_pdp_profile_status_etype status;
  ds_umts_pdp_auth_type  pdp_auth_data;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
 
  /*-------------------------------------------------------------------
    Get the auth info completely and then just return the password
    ds_umts_get_pdp_profile_auth_info performs checks on init flag and
    profile number. 
  
    Check the length of the output buffer here.
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_OUT_BUFF_LEN(password_buf_len,DS_UMTS_MAX_QCPDP_STRING_LEN);

  status = ds_umts_get_pdp_profile_auth_info(
                                        profile_number,
                                        &pdp_auth_data);

  if( status == DS_UMTS_PDP_SUCCESS )
  {
    /*-------------------------------------------------------------------
      Get the password which is a ASCIZ string
    -------------------------------------------------------------------*/
    /*lint -e534 ignoring return code from function*/
    strlcpy((char*) password_buf,(char *) pdp_auth_data.password,
                password_buf_len);
    /*lint +e534*/
  }
  return status;
}

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_UMTS_QOS_REQ_INFO_INTERNAL

DESCRIPTION
  This function retrieves the UMTS qos parameters (requested) specified for a
  profile number . No check is done to see if a valid context is defined. 
  The caller has to check if the context and associated parameter set is 
  defined (valid_flg of QOS is set) and use it accordingly.

PARAMETERS
  profile_number  : profile number
  pdp_umts_qos_data   : pointer to store the QOS parameters

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_get_pdp_profile_umts_qos_req_info_internal
(
  uint16                       profile_number,
  ds_umts_pdp_subs_e_type      subs_id,
  ds_umts_umts_qos_params_type *pdp_umts_qos_data
)
{
  ds_umts_pdp_profile_status_etype status;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  DS_UMTS_PDP_ALLOC_LOCAL_PDP_PROFILE( ds_umts_pdp_local_profile_ptr );
 
 /*-------------------------------------------------------------------
    Get the complete profile using ds_umts_get_pdp_profile_all_data
    It performs the err check on profile number & init flag
    Note: the UMTS QOS parameters might be invalid & not defined
    The caller has to handle this scenario by checking the "valid_flg"
    of the UMTS parameters returned
  -------------------------------------------------------------------*/
  status = ds_umts_get_pdp_profile_all_data_internal(
                                       profile_number,
                                       subs_id,
                                       ds_umts_pdp_local_profile_ptr);

  if( status == DS_UMTS_PDP_SUCCESS )
  {
    /*-------------------------------------------------------------------
      Get the UMTS QOS info.
    -------------------------------------------------------------------*/
    memscpy( (void*) pdp_umts_qos_data,
             sizeof(ds_umts_umts_qos_params_type),
             (void *) (&(ds_umts_pdp_local_profile_ptr->qos_request_umts)),
             sizeof(ds_umts_umts_qos_params_type) );
  }

  DS_UMTS_PDP_FREE_LOCAL_PDP_PROFILE(ds_umts_pdp_local_profile_ptr);

  return status;  
}


/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_UMTS_QOS_MIN_INFO_INTERNAL

DESCRIPTION
  This function retrieves the UMTS qos parameters (minimum) specified for a
  profile number . No check is done to see if a valid context is defined. 
  The caller has to check if the context and associated parameter set is 
  defined (valid_flg of QOS is set) and use it accordingly.

PARAMETERS
  profile_number  : profile number
  pdp_umts_qos_data   : pointer to store the QOS parameters

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_get_pdp_profile_umts_qos_min_info_internal
(
  uint16                       profile_number,
  ds_umts_pdp_subs_e_type      subs_id,
  ds_umts_umts_qos_params_type *pdp_umts_qos_data
)
{
  ds_umts_pdp_profile_status_etype status;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  DS_UMTS_PDP_ALLOC_LOCAL_PDP_PROFILE( ds_umts_pdp_local_profile_ptr );

  /*-------------------------------------------------------------------
    Get the complete profile using ds_umts_get_pdp_profile_all_data
    It performs the err check on profile number & init flag
    Note: the UMTS QOS parameters might be invalid & not defined
    The caller has to handle this scenario by checking the "valid_flg"
    of the UMTS parameters returned
  -------------------------------------------------------------------*/
  status = ds_umts_get_pdp_profile_all_data_internal(
                                       profile_number,
                                       subs_id,
                                       ds_umts_pdp_local_profile_ptr);

  if( status == DS_UMTS_PDP_SUCCESS )
  {
    /*-------------------------------------------------------------------
      Get the UMTS QOS info.
    -------------------------------------------------------------------*/
    memscpy( (void*) pdp_umts_qos_data,
             sizeof(ds_umts_umts_qos_params_type),
             (void *) (&(ds_umts_pdp_local_profile_ptr->qos_minimum_umts)),
             sizeof(ds_umts_umts_qos_params_type) );
  }

  DS_UMTS_PDP_FREE_LOCAL_PDP_PROFILE(ds_umts_pdp_local_profile_ptr);

  return status;  
}

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_SRC_STAT_DESC_REQ_INFO_INTERNAL

DESCRIPTION
  This function retrieves the src_stat_desc value specified for a
  profile number .

PARAMETERS
  profile_number  : profile number
  src_stat_desc   : pointer to store the src_stat_desc

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system

SIDE EFFECTS 
  none
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_get_pdp_profile_src_stat_desc_req_info_internal
(
  uint16                       profile_number,
  ds_umts_pdp_subs_e_type      subs_id,
  uint32                       *src_stat_desc
)
{
  ds_umts_pdp_profile_status_etype status;
  
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  DS_UMTS_PDP_ALLOC_LOCAL_PDP_PROFILE( ds_umts_pdp_local_profile_ptr );
 
 /*-------------------------------------------------------------------
    Get the complete profile using ds_umts_get_pdp_profile_all_data
    It performs the err check on profile number & init flag
    Note: the UMTS QOS parameters might be invalid & not defined
    The caller has to handle this scenario by checking the "valid_flg"
    of the UMTS parameters returned
  -------------------------------------------------------------------*/
  status = ds_umts_get_pdp_profile_all_data_internal(
                                       profile_number,
                                       subs_id,
                                       ds_umts_pdp_local_profile_ptr);

  if( status == DS_UMTS_PDP_SUCCESS )
  {
    /*-------------------------------------------------------------------
      Get the Req Source Statistiocs Descriptor info.
    -------------------------------------------------------------------*/
    memscpy( (void*) src_stat_desc,
             sizeof(uint32),
             (void *) (&(ds_umts_pdp_local_profile_ptr->src_stat_desc_req)),
             sizeof(uint32) );
  }

  DS_UMTS_PDP_FREE_LOCAL_PDP_PROFILE(ds_umts_pdp_local_profile_ptr);

  return status;  
}

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_SRC_STAT_DESC_MIN_INFO_INTERNAL

DESCRIPTION
  This function retrieves the src_stat_desc value specified for a
  profile number .

PARAMETERS
  profile_number  : profile number
  src_stat_desc   : pointer to store the src_stat_desc

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system

SIDE EFFECTS 
  none
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_get_pdp_profile_src_stat_desc_min_info_internal
(
  uint16                       profile_number,
  ds_umts_pdp_subs_e_type      subs_id,
  uint32                       *src_stat_desc
)
{
  ds_umts_pdp_profile_status_etype status;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  DS_UMTS_PDP_ALLOC_LOCAL_PDP_PROFILE( ds_umts_pdp_local_profile_ptr );
 
 /*-------------------------------------------------------------------
    Get the complete profile using ds_umts_get_pdp_profile_all_data
    It performs the err check on profile number & init flag
    Note: the UMTS QOS parameters might be invalid & not defined
    The caller has to handle this scenario by checking the "valid_flg"
    of the UMTS parameters returned
  -------------------------------------------------------------------*/
  status = ds_umts_get_pdp_profile_all_data_internal(
                                       profile_number,
                                       subs_id,
                                       ds_umts_pdp_local_profile_ptr);

  if( status == DS_UMTS_PDP_SUCCESS )
  {
    /*-------------------------------------------------------------------
      Get the Min Source Statistics Descriptor info
    -------------------------------------------------------------------*/
    memscpy( (void*) src_stat_desc,
             sizeof(uint32),
             (void *) (&(ds_umts_pdp_local_profile_ptr->src_stat_desc_min)),
             sizeof(uint32) );
  }

  DS_UMTS_PDP_FREE_LOCAL_PDP_PROFILE(ds_umts_pdp_local_profile_ptr);

  return status;  
}

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_GPRS_QOS_REQ_INFO_INTERNAL

DESCRIPTION
  This function retrieves the GPRS "requested" qos parameters specified for a
  profile number . No check is done to see if a valid context is defined. 
  The caller has to check if the context and associated parameter set is 
  defined (valid_flg of QOS is set) and use it accordingly.

PARAMETERS
  profile_number  : profile number
  pdp_gprs_qos_req_data   : pointer to store the QOS parameters

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_get_pdp_profile_gprs_qos_req_info_internal
(
  uint16                       profile_number,
  ds_umts_pdp_subs_e_type      subs_id,
  ds_umts_gprs_qos_params_type *pdp_gprs_qos_req_data
)
{
  ds_umts_pdp_profile_status_etype status;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  DS_UMTS_PDP_ALLOC_LOCAL_PDP_PROFILE( ds_umts_pdp_local_profile_ptr );

 /*-------------------------------------------------------------------
    Get the complete profile using ds_umts_get_pdp_profile_all_data
    It performs the err check on profile number & init flag
    Note: the GPRS QOS parameters might be invalid & not defined
    The caller has to handle this scenario by checking the "valid_flg"
    of the GPRS parameters returned
  -------------------------------------------------------------------*/
  status = ds_umts_get_pdp_profile_all_data_internal(
                                       profile_number,
                                       subs_id,
                                       ds_umts_pdp_local_profile_ptr);

  if( status == DS_UMTS_PDP_SUCCESS )
  {
    /*-------------------------------------------------------------------
      Get the GPRS requested QOS info.
    -------------------------------------------------------------------*/
    memscpy( (void *) pdp_gprs_qos_req_data,
             sizeof(ds_umts_gprs_qos_params_type),
            (void *) &(ds_umts_pdp_local_profile_ptr->qos_request_gprs),
            sizeof(ds_umts_gprs_qos_params_type) );
  }
  DS_UMTS_PDP_FREE_LOCAL_PDP_PROFILE(ds_umts_pdp_local_profile_ptr);

  return status;  
}

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_GPRS_QOS_MIN_INFO_INTERNAL

DESCRIPTION
  This function retrieves the GPRS "minimum" qos parameters specified for a
  profile number . No check is done to see if a valid context is defined. 
  The caller has to check if the context and associated parameter set is 
  defined (valid_flg of QOS is set) and use it accordingly.

PARAMETERS
  profile_number  : profile number
  pdp_gprs_qos_min_data   : pointer to store the QOS parameters

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_get_pdp_profile_gprs_qos_min_info_internal
(
  uint16                       profile_number,
  ds_umts_pdp_subs_e_type      subs_id,
  ds_umts_gprs_qos_params_type *pdp_gprs_qos_min_data
)
{
  ds_umts_pdp_profile_status_etype status;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  DS_UMTS_PDP_ALLOC_LOCAL_PDP_PROFILE( ds_umts_pdp_local_profile_ptr );

  /*-------------------------------------------------------------------
    Get the complete profile using ds_umts_get_pdp_profile_all_data
    It performs the err check on profile number & init flag
    Note: the GPRS QOS parameters might be invalid & not defined
    The caller has to handle this scenario by checking the "valid_flg"
    of the GPRS parameters returned
  -------------------------------------------------------------------*/
  status = ds_umts_get_pdp_profile_all_data_internal(
                                       profile_number,
                                       subs_id,
                                       ds_umts_pdp_local_profile_ptr);

  if( status == DS_UMTS_PDP_SUCCESS )
  {
    /*-------------------------------------------------------------------
      Get the GPRS requested QOS info.
    -------------------------------------------------------------------*/
    memscpy( (void*) pdp_gprs_qos_min_data,
             sizeof(ds_umts_gprs_qos_params_type),
            (void*) &(ds_umts_pdp_local_profile_ptr->qos_minimum_gprs),
            sizeof(ds_umts_gprs_qos_params_type) );
  }
  DS_UMTS_PDP_FREE_LOCAL_PDP_PROFILE(ds_umts_pdp_local_profile_ptr);

  return status;  
}

/*===========================================================================
FUNCTION DS_3GPP_GET_PDP_PROFILE_LTE_QOS_INFO_INTERNAL

DESCRIPTION
  This function retrieves the LTE qos parameters specified for a
  profile number . No check is done to see if a valid context is defined. 
  The caller has to check if the context and associated parameter set is 
  defined (valid_flg of QOS is set) and use it accordingly.

PARAMETERS
  profile_number  : profile number
  pdp_lte_qos_data   : pointer to store the QOS parameters

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_3gpp_get_pdp_profile_lte_qos_info_internal
(
  uint16                       profile_number,
  ds_umts_pdp_subs_e_type      subs_id,
  ds_3gpp_lte_qos_params_type *pdp_lte_qos_data
)
{
  ds_umts_pdp_profile_status_etype status;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  DS_UMTS_PDP_ALLOC_LOCAL_PDP_PROFILE( ds_umts_pdp_local_profile_ptr );

  /*-------------------------------------------------------------------
    Get the complete profile using ds_umts_get_pdp_profile_all_data
    It performs the err check on profile number & init flag
    Note: the GPRS QOS parameters might be invalid & not defined
    The caller has to handle this scenario by checking the "valid_flg"
    of the GPRS parameters returned
  -------------------------------------------------------------------*/
  status = ds_umts_get_pdp_profile_all_data_internal(
                                       profile_number,
                                       subs_id,
                                       ds_umts_pdp_local_profile_ptr);

  if( status == DS_UMTS_PDP_SUCCESS )
  {
    /*-------------------------------------------------------------------
      Get the LTE requested QOS info.
    -------------------------------------------------------------------*/
    memscpy( (void*) pdp_lte_qos_data,
             sizeof(ds_3gpp_lte_qos_params_type),
            (void*) &(ds_umts_pdp_local_profile_ptr->qos_request_lte),
            sizeof(ds_3gpp_lte_qos_params_type) );
  }
  DS_UMTS_PDP_FREE_LOCAL_PDP_PROFILE(ds_umts_pdp_local_profile_ptr);

  return status;  
}

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_DNS_ADDRESS_INFO_INTERNAL

DESCRIPTION
  This function retrieves the DNS addresses specified for a
  profile number . No check is done to see if a valid context is defined. 
  The caller has to check if the context and associated parameter set is 
  defined ( non zero dns address values ) and use it accordingly.

PARAMETERS
  profile_number  : profile number
  pdp_dns_addr    : pointer to store the dns addresses

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_get_pdp_profile_dns_address_info_internal
(
  uint16                          profile_number,
  ds_umts_pdp_subs_e_type         subs_id,
  ds_umts_pdp_dns_addresses_type *pdp_dns_addr
)
{
  ds_umts_pdp_profile_status_etype status;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  DS_UMTS_PDP_ALLOC_LOCAL_PDP_PROFILE( ds_umts_pdp_local_profile_ptr );
 
  /*-------------------------------------------------------------------
    Get the complete profile using ds_umts_get_pdp_profile_all_data
    It performs the err check on profile number & init flag
  -------------------------------------------------------------------*/
  status = ds_umts_get_pdp_profile_all_data_internal(
                                       profile_number,
                                       subs_id,
                                       ds_umts_pdp_local_profile_ptr);

  if( status == DS_UMTS_PDP_SUCCESS )
  {
    /*-------------------------------------------------------------------
      Get the DNS addresses
    -------------------------------------------------------------------*/
    memscpy( (void*) pdp_dns_addr,
             sizeof(ds_umts_pdp_dns_addresses_type),
            (void*) &(ds_umts_pdp_local_profile_ptr->dns_addr),
            sizeof(ds_umts_pdp_dns_addresses_type) );
  }
  DS_UMTS_PDP_FREE_LOCAL_PDP_PROFILE(ds_umts_pdp_local_profile_ptr);

  return status;  
}

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_DNS_ADDRESS_INFO_PRIMARY_INTERNAL

DESCRIPTION
  This function retrieves the primary DNS address specified for a
  profile number . No check is done to see if a valid context is defined. 
  The caller has to check if the context and associated parameter set is 
  defined (non zero dns address ) and use it accordingly.

PARAMETERS
  profile_number  : profile number
  primary_dns     : pointer to store the dns address

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_get_pdp_profile_dns_address_info_primary_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  ds_umts_pdp_addr_type    *primary_dns
)
{
  ds_umts_pdp_profile_status_etype status;
  ds_umts_pdp_dns_addresses_type pdp_dns_info;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------
    Get the DNS infor by using using ds_umts_get_pdp_profile_dns_address_info
  -------------------------------------------------------------------*/
  status = ds_umts_get_pdp_profile_dns_address_info(
                                       profile_number,
                                       &pdp_dns_info);

  if( status == DS_UMTS_PDP_SUCCESS )
  {
    /*-------------------------------------------------------------------
      Get the DNS addresses
    -------------------------------------------------------------------*/
    *primary_dns = pdp_dns_info.primary_dns_addr;
  }

  return status;  
}

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_DNS_ADDRESS_INFO_SECONDARY_INTERNAL

DESCRIPTION
  This function retrieves the secondary DNS address specified for a
  profile number . No check is done to see if a valid context is defined. 
  The caller has to check if the context and associated parameter set is 
  defined (non zero dns address ) and use it accordingly.

PARAMETERS
  profile_number  : profile number
  secondary_dns   : pointer to store the dns address

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_get_pdp_profile_dns_address_info_secondary_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  ds_umts_pdp_addr_type    *secondary_dns
)
{
  ds_umts_pdp_profile_status_etype status;
  ds_umts_pdp_dns_addresses_type pdp_dns_info;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
 
  /*-------------------------------------------------------------------
    Get the DNS infor by using using ds_umts_get_pdp_profile_dns_address_info
  -------------------------------------------------------------------*/
  status = ds_umts_get_pdp_profile_dns_address_info(
                                       profile_number,
                                       &pdp_dns_info);

  if( status == DS_UMTS_PDP_SUCCESS )
  {
    /*-------------------------------------------------------------------
      Get the DNS addresses
    -------------------------------------------------------------------*/
    *secondary_dns = pdp_dns_info.secondary_dns_addr;
  }

  return status;  
}

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_TFT_INFO_INTERNAL

DESCRIPTION
  This function retrieves the Traffic Flow Template (TFT) parameters
  specified for a profile number and filter identifer. No check is
  done to see if a valid context is defined.  The caller has to check
  if the context and associated parameter set is defined (valid_flg of
  TFT is set) and use it accordingly.

PARAMETERS
  profile_number : profile number
  filter_id      : filter identifier 
  pdp_tft_data   : pointer to store the TFT parameters

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_INVALID_FILTER_ID    : Invalid filter identifier
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype  ds_umts_get_pdp_profile_tft_info_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  byte                      filter_id,
  ds_umts_tft_params_type  *pdp_tft_data
)
{
  ds_umts_pdp_profile_status_etype status;
  byte index = filter_id - 1;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------
    Check if the input parameters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_FILTER_ID(filter_id);
  
  DS_UMTS_PDP_ALLOC_LOCAL_PDP_PROFILE( ds_umts_pdp_local_profile_ptr );

 /*-------------------------------------------------------------------
    Get the complete profile using ds_umts_get_pdp_profile_all_data
    It performs the err check on profile number & init flag
    The profile is returned ONLY if a valid context is defined.
  -------------------------------------------------------------------*/
  status = ds_umts_get_pdp_profile_all_data_internal(
                                       profile_number,
                                       subs_id,
                                       ds_umts_pdp_local_profile_ptr);

  if( status == DS_UMTS_PDP_SUCCESS )
  {
      /*-------------------------------------------------------------------
        Get the TFT info 
      -------------------------------------------------------------------*/
      memscpy( (void*) pdp_tft_data,
               sizeof(ds_umts_tft_params_type),
              (void*) ( ((byte*)ds_umts_pdp_local_profile_ptr) +
                DS_UMTS_PDP_PROFILE_TFT_FILTER_OFFSET(index) ),
              sizeof(ds_umts_tft_params_type) );
  }

  DS_UMTS_PDP_FREE_LOCAL_PDP_PROFILE(ds_umts_pdp_local_profile_ptr);

  return status;  
}

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_TFT_ALL_INFO_INTERNAL

DESCRIPTION

  This function retrieves ALL the Traffic Flow Template (TFT)
  parameters specified for a profile number. No check is done to see
  if a valid context is defined.  The caller is responsible for
  allocating sufficent storage for the returned structure.  The caller
  has to check if the context and associated parameter set is defined
  (valid_flg of TFT is set) and use it accordingly.

PARAMETERS
  profile_number  : profile number
  pdp_tft_data   : pointer to store the TFT parameters

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_get_pdp_profile_tft_all_info_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  ds_umts_tft_params_type  *pdp_tft_data
)
{
  ds_umts_pdp_profile_status_etype status;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  DS_UMTS_PDP_ALLOC_LOCAL_PDP_PROFILE( ds_umts_pdp_local_profile_ptr );

  /*-------------------------------------------------------------------
    Get the complete profile using ds_umts_get_pdp_profile_all_data
    It performs the err check on profile number & init flag
    The profile is returned ONLY if a valid context is defined.
  -------------------------------------------------------------------*/
  status = ds_umts_get_pdp_profile_all_data_internal(
                       profile_number,
                       subs_id,
                       ds_umts_pdp_local_profile_ptr);

  if( status == DS_UMTS_PDP_SUCCESS )
  {
    /*-------------------------------------------------------------------
      Get the TFT info 
    -------------------------------------------------------------------*/
    memscpy( (void*) pdp_tft_data,
             (DS_UMTS_MAX_TFT_PARAM_SETS * sizeof(ds_umts_tft_params_type)),
            (void*) ds_umts_pdp_local_profile_ptr->tft,
            (DS_UMTS_MAX_TFT_PARAM_SETS * sizeof(ds_umts_tft_params_type))
          );
  }
  DS_UMTS_PDP_FREE_LOCAL_PDP_PROFILE(ds_umts_pdp_local_profile_ptr);

  return status;  
}

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_LINGER_PARAMS_INFO_INTERNAL

DESCRIPTION
  This function retrieves the linger parameters from the profile.

PARAMETERS
  profile_number     : profile number
  linger_params_data : pointer to store the linger parameters

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system

SIDE EFFECTS
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype 
   ds_umts_get_pdp_profile_linger_params_info_internal
(
  uint16                     profile_number,
  ds_umts_pdp_subs_e_type    subs_id,
  ds_umts_linger_params_type *linger_params_data
)
{
  ds_umts_pdp_profile_status_etype status;

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  DS_UMTS_PDP_ALLOC_LOCAL_PDP_PROFILE( ds_umts_pdp_local_profile_ptr );

  /*-------------------------------------------------------------------
    Get the complete profile using ds_umts_get_pdp_profile_all_data
    It performs the err check on profile number & init flag
    The profile is returned ONLY if a valid context is defined.
  -------------------------------------------------------------------*/
  status = ds_umts_get_pdp_profile_all_data_internal(
                                       profile_number,
                                       subs_id,
                                       ds_umts_pdp_local_profile_ptr);

  if( status == DS_UMTS_PDP_SUCCESS )
  {
    /*-------------------------------------------------------------------
      Get the Linger params info 
    -------------------------------------------------------------------*/
    memscpy( (void*) linger_params_data,
            sizeof(ds_umts_linger_params_type),
            (void*) ( &(ds_umts_pdp_local_profile_ptr->iface_linger_params )),
            sizeof(ds_umts_linger_params_type) );
  }
  DS_UMTS_PDP_FREE_LOCAL_PDP_PROFILE(ds_umts_pdp_local_profile_ptr);

  return status;
}

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_NUM_FOR_EMBEDDED_CALLS_INTERNAL

DESCRIPTION
  This function retrieves the profile number specififed for embedded
  data calls ( embedded calls ).  The cached value is return if available;
  otherwise, the read is done from the EFS file.

PARAMETERS
  profile_number  : pointer to store profile number

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype
   ds_umts_get_pdp_profile_num_for_embedded_calls_internal
( 
  byte *profile_number,
  ds_umts_pdp_subs_e_type   subs_id
)
{
  int        result;

  if(ds_umts_pdp_profile_set_sub_dir_name(subs_id) != DS_UMTS_PDP_SUCCESS)
  {
    return DS_UMTS_PDP_FAIL;
  }
  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  /*-------------------------------------------------------------------
    Check if cached embedded call profile number is available
  -------------------------------------------------------------------*/
  if( 0 != cached_embedded_call_profile_number[subs_id])
  {
    *profile_number = cached_embedded_call_profile_number[subs_id];
  }
  else
  {
    /*-------------------------------------------------------------------
      Init the fully qualified embedded call file name 
    -------------------------------------------------------------------*/
    /*lint -e534 ignoring return code from function*/
    strlcpy( ds_umts_pdp_fully_qual_embedded_call_file_name,
                 ds_umts_pdp_profile_sub_dir_name,
                 sizeof(ds_umts_pdp_fully_qual_embedded_call_file_name) );
    strlcat( ds_umts_pdp_fully_qual_embedded_call_file_name,
                 DS_UMTS_PDP_PROFILE_NUM_FOR_EMBEDDED_FILE_NAME,
                 sizeof(ds_umts_pdp_fully_qual_embedded_call_file_name) );
    /*lint +e534*/
    /*-------------------------------------------------------------------
      read the contents of the file : only one field 
      a one byte context number is stored.
      -------------------------------------------------------------------*/
    result = efs_get( ds_umts_pdp_fully_qual_embedded_call_file_name,
                              (void *)profile_number,
                      sizeof(byte));
    if ( result == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_get failed for %d errno=%d.", result, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }

    /*-------------------------------------------------------------------
      Set the cached embedded call profile_number for later reads
      -------------------------------------------------------------------*/
    cached_embedded_call_profile_number[subs_id] = *profile_number;
  }

  return DS_UMTS_PDP_SUCCESS;
}

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_NUM_FOR_TETHERED_CALLS_INTERNAL

DESCRIPTION
  This function retrieves the profile number specififed for embedded
  data calls ( tethered calls ).  The cached value is return if available;
  otherwise, the read is done from the EFS file.

PARAMETERS
  profile_number  : pointer to store profile number

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_get_pdp_profile_num_for_tethered_calls_internal
( 
  byte *profile_number,
  ds_umts_pdp_subs_e_type   subs_id
)
{
  int        result;

  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(ds_umts_pdp_profile_set_sub_dir_name(subs_id) != DS_UMTS_PDP_SUCCESS)
  {
    return DS_UMTS_PDP_FAIL;
  }

  /*-------------------------------------------------------------------
    Check if cached tethered call profile number is available
  -------------------------------------------------------------------*/
  if( 0 != cached_tethered_call_profile_number[subs_id])
  {
    *profile_number = cached_tethered_call_profile_number[subs_id];
  }
  else
  {
    /*-------------------------------------------------------------------
      Init the fully qualified tethered call file name 
    -------------------------------------------------------------------*/
    /*lint -e534 ignoring return code from function*/
    strlcpy( ds_umts_pdp_fully_qual_tethered_call_file_name,
                 ds_umts_pdp_profile_sub_dir_name,
                 sizeof(ds_umts_pdp_fully_qual_tethered_call_file_name) );
    strlcat( ds_umts_pdp_fully_qual_tethered_call_file_name,
                 DS_UMTS_PDP_PROFILE_NUM_FOR_TETHERED_FILE_NAME,
                 sizeof(ds_umts_pdp_fully_qual_tethered_call_file_name) );
    /*lint +e534*/
      

    /*-------------------------------------------------------------------
      read the contents of the file : only one field 
      a one byte context number is stored.
      -------------------------------------------------------------------*/
    result = efs_get( ds_umts_pdp_fully_qual_tethered_call_file_name,
                      (void *)profile_number,
                         sizeof(byte));
    if ( result == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_open failed for %d errno=%d.", result, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      Set the cached tethered call profile_number for later reads
      -------------------------------------------------------------------*/
    cached_tethered_call_profile_number[subs_id] = *profile_number;
  }

  return DS_UMTS_PDP_SUCCESS;
}

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_EMERGENCY_CALLS_ARE_SUPPORTED_INTERNAL

DESCRIPTION
  This function retrieves the "emergency_calls_are_supported" flag specified for a
  profile. 
  This flag indicates if IMS emergency can be made uising this profile.
  Note : This information can be retrieved even if the context (profile) has
  not been defined

PARAMETERS
  profile_number  : profile number
  emergency_calls_are_supported   : pointer to store the emergency_calls_are_supported flag.


DEPENDENCIES
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system

SIDE EFFECTS
  none
===========================================================================*/
ds_umts_pdp_profile_status_etype 
  ds_umts_get_pdp_profile_emergency_calls_are_supported_flag_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  boolean                  *emergency_calls_are_supported
)
{
#ifdef FEATURE_EMERGENCY_PDN
  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 

  /*-------------------------------------------------------------------
    Read the emergency_calls_are_supported info from the cache table.
  -------------------------------------------------------------------*/

  DS_UMTS_PDP_TASKLOCK_NON_L4();  
  *emergency_calls_are_supported = 
      DS_UMTS_PDP_PROFILE_CACHE( subs_id, profile_number )->
      emergency_calls_are_supported;

  DS_UMTS_PDP_TASKFREE_NON_L4();

  return DS_UMTS_PDP_SUCCESS;
#endif /* FEATURE_EMERGENCY_PDN */
  return DS_UMTS_PDP_FEATURE_NOT_SUPPORTED;
}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_NAME_UTF16_INTERNAL

DESCRIPTION
  This function updates the name of a profile with the value passed.
  NOTE: The name of the profile can be updated at any time ir-respecitive
  of the valididity of the context associated with the profile
  It also updates the profile name in the cache table.

PARAMETERS
  profile_number  : profile number
  profile_name_buf : pointer to the name string (UTF16 string)
  profile_name_len : length of the name string (UTF16 words)

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file
  DS_UMTS_PDP_INPUT_BUFFER_LEN_INVALID : length of profile name is too long

SIDE EFFECTS 
  None
===========================================================================*/
/*ARGSUSED*/
ds_umts_pdp_profile_status_etype  ds_umts_set_pdp_profile_name_utf16_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  const uint16              *profile_name_buf,
  byte                      profile_name_len
)
{
#ifndef FEATURE_PROFILE_OTAP_ENHANCEMENTS

  DS_3GPP_MSG3_ERROR("Format not supported", 0, 0, 0);
  return DS_UMTS_PDP_FORMAT_NOT_SUPPORTED;

#else

  int        fs_handle;
  uint16                asciz = 0x0;
  boolean               read_only_flag;
  ds_umts_pdp_profile_type *ds_umts_pdp_profile_ptr;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
 
  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
    A valid profile need not be defined at this time to set the name    
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 

  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  DS_UMTS_PDP_CHECK_IN_BUFF_LEN(profile_name_len,DS_UMTS_MAX_PROFILE_NAME_LEN);

  /*-------------------------------------------------------------------
    Initialize the variables for fs_open                   
  -------------------------------------------------------------------*/
  ds_umts_pdp_profile_ptr = DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);

  if(DS_UMTS_PDP_PROFILE_IS_PERSISTENT(ds_umts_pdp_profile_ptr)) 
  {
      /*-------------------------------------------------------------------
        Init the fully qualified profile file name to pdp_profiles directory
      -------------------------------------------------------------------*/
    if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
      return DS_UMTS_PDP_FAIL;
  
    /*-------------------------------------------------------------------
      Open the file for read / write
    -------------------------------------------------------------------*/
    DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ", 
                            ds_umts_pdp_fully_qual_profile_file_name);
      
    fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                          O_RDWR);
    if ( fs_handle == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_open failed for %d errno=%d.", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      write the  profile name
    -------------------------------------------------------------------*/
    if (efs_lseek(fs_handle,
                  DS_UMTS_PDP_PROFILE_NAME_OFFSET,
                  SEEK_SET) == -1)
    {
      DS_3GPP_MSG3_ERROR( "efs_lseek failed for %d errno=%d", fs_handle, efs_errno, 0 );
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    
    if (ds_umts_pdp_efs_write(fs_handle,
                              (void *)profile_name_buf,
                              sizeof(uint16)*profile_name_len) == FALSE)
    {
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    
    /*----------------------------------------------------------------------
      Terminate the string wigh ASCIZ .. so that reading is easy with 
      strlcpy
    -----------------------------------------------------------------------*/
    if (ds_umts_pdp_efs_write(fs_handle,
                              (void *)&asciz,
                              sizeof(uint16)) == FALSE)
    {
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
     
    /*-------------------------------------------------------------------
      Close the file
    -------------------------------------------------------------------*/
    if (efs_close (fs_handle) == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_close failed for %d errno = %d", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_FAIL;
    }
  }
  /*-------------------------------------------------------------------
    Update the profile name in the cache table.
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_TASKLOCK_NON_L4();
  memscpy( (void*)ds_umts_pdp_profile_ptr->profile_name,
          sizeof(uint16)*profile_name_len
          (void*)profile_name_buf,
          sizeof(uint16)*profile_name_len );
  ds_umts_pdp_profile_ptr->profile_name[profile_name_len] = 
                                                   (uint16)'\0';  
  DS_UMTS_PDP_TASKFREE_NON_L4();

  
 return DS_UMTS_PDP_SUCCESS;  
#endif
}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_NAME_INTERNAL

DESCRIPTION
  This function updates the name of a profile with the value passed.
  NOTE: The name of the profile can be updated at any time ir-respecitive
  of the valididity of the context associated with the profile
  It also updates the profile name in the cache table.

PARAMETERS
  profile_number  : profile number
  profile_name_buf : pointer to the name string
  profile_name_len : length of the name string

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file
  DS_UMTS_PDP_INPUT_BUFFER_LEN_INVALID : length of profile name is too long

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype  ds_umts_set_pdp_profile_name_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  const byte                *profile_name_buf,
  byte                      profile_name_len
)
{
#ifdef FEATURE_PROFILE_OTAP_ENHANCEMENTS
  DS_3GPP_MSG3_ERROR("Format not supported", 0, 0, 0);
  return DS_UMTS_PDP_FORMAT_NOT_SUPPORTED;
#else
  int        fs_handle;
  char                  asciz = 0x0;
  ds_umts_pdp_profile_type *ds_umts_pdp_profile_ptr;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
 
  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
    A valid profile need not be defined at this time to set the name    
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 

  /*-------------------------------------------------------------------
    Check of the length of the name is valid and within range
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_IN_BUFF_LEN(profile_name_len,DS_UMTS_MAX_PROFILE_NAME_LEN);

  /*-------------------------------------------------------------------
    Initialize the variables for fs_open                   
  -------------------------------------------------------------------*/

  ds_umts_pdp_profile_ptr = DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);

  if(DS_UMTS_PDP_PROFILE_IS_PERSISTENT(ds_umts_pdp_profile_ptr)) 
  {
      /*-------------------------------------------------------------------
        Init the fully qualified profile file name to pdp_profiles directory
      -------------------------------------------------------------------*/
    if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
      return DS_UMTS_PDP_FAIL;
  
    /*-------------------------------------------------------------------
      Open the file for read / write
    -------------------------------------------------------------------*/
    DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ", 
                            ds_umts_pdp_fully_qual_profile_file_name);
    
    fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                          O_RDWR);
    if ( fs_handle == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_open failed for %d errno=%d.", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      write the  profile name
    -------------------------------------------------------------------*/
    if (efs_lseek(fs_handle,
                  DS_UMTS_PDP_PROFILE_NAME_OFFSET,
                  SEEK_SET) == -1)
    {
      DS_3GPP_MSG3_ERROR( "efs_lseek failed for %d errno=%d", fs_handle, efs_errno, 0 );
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    if (ds_umts_pdp_efs_write(fs_handle,
                              (void *)profile_name_buf,
                              profile_name_len) == FALSE)
    {
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    
    /*----------------------------------------------------------------------
      Terminate the string wigh ASCIZ .. so that reading is easy with 
      strlcpy
    -----------------------------------------------------------------------*/
    if (ds_umts_pdp_efs_write(fs_handle,
                              (void *)&asciz,
                              sizeof(char)) == FALSE)
    {
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
     
    /*-------------------------------------------------------------------
      Close the file
    -------------------------------------------------------------------*/
    if (efs_close (fs_handle) == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_close failed for %d errno = %d", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  }

  /*-------------------------------------------------------------------
    Update the profile name in the cache table.
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_TASKLOCK_NON_L4();

  /*-------------------------------------------------------------------
    Reset the previous profile name before updating new profile name.
  -------------------------------------------------------------------*/
  memset((void *)ds_umts_pdp_profile_ptr->profile_name, 
         0, 
         DS_UMTS_MAX_PROFILE_NAME_LEN+1);
  
  /*lint -e534 ignoring return code from function*/
  strlcpy( (char*)ds_umts_pdp_profile_ptr->profile_name,
          (char*)profile_name_buf,
      sizeof(ds_umts_pdp_profile_ptr->profile_name));
  /*lint +e534*/
  DS_UMTS_PDP_TASKFREE_NON_L4();
  
  return DS_UMTS_PDP_SUCCESS;  
#endif
}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_OTAP_NAPID_INTERNAL

DESCRIPTION
  This function updates the napid of a profile with the value passed.
  No check is done to see if a valid context is defined.
  It also updates the cache table with the new value.

PARAMETERS
  profile_number : profile number
  otap_napid_buf : pointer to the napid string (UTF8 string)
  otap_napid_len : length of the napid string  (variable length UTF8 bytes)

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file
  DS_UMTS_PDP_INPUT_BUFFER_LEN_INVALID : length of otap napid is too long

SIDE EFFECTS 
  None
===========================================================================*/
/*ARGSUSED*/
ds_umts_pdp_profile_status_etype  ds_umts_set_pdp_otap_napid_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  byte                      *otap_napid_buf,
  byte                      otap_napid_len
)
{
#ifndef FEATURE_PROFILE_OTAP_ENHANCEMENTS
  DS_3GPP_MSG3_ERROR("API not supported", 0, 0, 0);
  return DS_UMTS_PDP_API_NOT_SUPPORTED;
#else
  int                  fs_handle;
  char                            asciz = 0x0;
  boolean                         read_only_flag;
  ds_umts_pdp_profile_type        *ds_umts_pdp_profile_ptr;

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
 
  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
    A valid profile need not be defined at this time to set the name    
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 
  
  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  /*-------------------------------------------------------------------
    Check of the length of the name is valid and within range
  -------------------------------------------------------------------*/

  ds_umts_pdp_profile_ptr = 
    DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);
 if(DS_UMTS_PDP_PROFILE_IS_PERSISTENT(ds_umts_pdp_profile_ptr)) 
  {
    /*-------------------------------------------------------------------
        Init the fully qualified profile file name to pdp_profiles directory
    -------------------------------------------------------------------*/
    if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
      return DS_UMTS_PDP_FAIL;
  
    /*-------------------------------------------------------------------
      Open the file for read / write
    -------------------------------------------------------------------*/
    DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ", 
                            ds_umts_pdp_fully_qual_profile_file_name);
    
    fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                          O_RDWR);
    if ( fs_handle == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_open failed for %d errno=%d.", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      Set the file offset to the start of context information
    -------------------------------------------------------------------*/
    if (efs_lseek(fs_handle,
                  DS_UMTS_PDP_PROFILE_OTAP_NAPID_OFFSET,
                  SEEK_SET) == -1)
    {
      DS_3GPP_MSG3_ERROR( "efs_lseek failed for %d errno=%d", fs_handle, efs_errno, 0 );
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      write the  profile napid
    -------------------------------------------------------------------*/
    if (ds_umts_pdp_efs_write(fs_handle,
                              (void *)otap_napid_buf,
                              otap_napid_len) == FALSE)
    {
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    
    /*----------------------------------------------------------------------
      Terminate the string wigh ASCIZ .. so that reading is easy with 
      strlcpy
    -----------------------------------------------------------------------*/
    if (ds_umts_pdp_efs_write(fs_handle,
                              (void *)&asciz,
                              sizeof(char)) == FALSE)
    {
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
     
    /*-------------------------------------------------------------------
      Close the file
    -------------------------------------------------------------------*/
    if (efs_close (fs_handle) == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_close failed for %d errno = %d", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_FAIL;
    }
 }
  /*-------------------------------------------------------------------
    Update the otap_napid field in the cache table.
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_TASKLOCK_NON_L4();
  memscpy( (void*)ds_umts_pdp_profile_ptr->otap_napid,
           sizeof(ds_umts_pdp_profile_ptr->otap_napid),
           (void*)otap_napid_buf,
           otap_napid_len );
  ds_umts_pdp_profile_ptr->otap_napid[otap_napid_len] = (uint16)'\0';
  DS_UMTS_PDP_TASKFREE_NON_L4();

  return DS_UMTS_PDP_SUCCESS;  
#endif
}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_READ_ONLY_FLAG_INTERNAL

DESCRIPTION
  This function updates the read_only_flag of a profile with the value passed.
  No check is done to see if a valid context is defined.
  It also updates the cache table with the new value.
  
PARAMETERS
  profile_number : profile number
  read_only_flag : profile is read-only or not

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype  ds_umts_set_pdp_read_only_flag_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  boolean                   read_only_flag
)
{
  int                  fs_handle;
  ds_umts_pdp_profile_type        *ds_umts_pdp_profile_ptr;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
 
  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
    A valid profile need not be defined at this time to set the name    
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 

  ds_umts_pdp_profile_ptr = 
    DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);

  if(DS_UMTS_PDP_PROFILE_IS_PERSISTENT(ds_umts_pdp_profile_ptr)) 
  {
    
    /*-------------------------------------------------------------------
        Init the fully qualified profile file name to pdp_profiles directory
    -------------------------------------------------------------------*/
    if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
      return DS_UMTS_PDP_FAIL;
  
    /*-------------------------------------------------------------------
      Open the file for read / write
    -------------------------------------------------------------------*/
    DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ", 
                            ds_umts_pdp_fully_qual_profile_file_name);
      
    fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                          O_RDWR);
    if ( fs_handle == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_open failed for %d errno=%d.", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      Set the file offset to the start of context information
    -------------------------------------------------------------------*/
    if (efs_lseek(fs_handle,
                  DS_UMTS_PDP_PROFILE_READ_ONLY_FLAG_OFFSET,
                  SEEK_SET) == -1)
    {
      DS_3GPP_MSG3_ERROR( "efs_lseek failed for %d errno=%d", fs_handle, efs_errno, 0 );
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      write the  profile napid
    -------------------------------------------------------------------*/
    if (ds_umts_pdp_efs_write(fs_handle,
                              (void *)(&read_only_flag),
                              sizeof(boolean)) == FALSE)
    {
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    
    /*-------------------------------------------------------------------
      Close the file
    -------------------------------------------------------------------*/
    if (efs_close (fs_handle) == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_close failed for %d errno = %d", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_FAIL;
    }
  }
  /*-------------------------------------------------------------------
    Update the cache table with the new read_only_flag value.
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_TASKLOCK_NON_L4();
  memscpy( (void*)&ds_umts_pdp_profile_ptr->read_only_flag,
  	   sizeof(boolean),
          (void*)&read_only_flag,
          sizeof(boolean) );
  DS_UMTS_PDP_TASKFREE_NON_L4();
  
  return DS_UMTS_PDP_SUCCESS;  
}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_OTAP_ENABLED_FLAG_INTERNAL

DESCRIPTION
  This function updates the otap_enabled_flag of a profile with the value passed.
  No check is done to see if a valid context is defined.
  It also updates the cache table with the new otap_enabled_flag value.
  
PARAMETERS
  profile_number : profile number
  otap_enabled_flag : profile is otap_enabled or not

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype  ds_umts_set_pdp_otap_enabled_flag_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  boolean                   otap_enabled_flag
)
{
  int        fs_handle;
  boolean               read_only_flag;
  ds_umts_pdp_profile_type *ds_umts_pdp_profile_ptr;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
 
  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
    A valid profile need not be defined at this time to set the name    
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 
  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  ds_umts_pdp_profile_ptr = 
    DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);

  if(DS_UMTS_PDP_PROFILE_IS_PERSISTENT(ds_umts_pdp_profile_ptr)) 
  {
    /*-------------------------------------------------------------------
        Init the fully qualified profile file name to pdp_profiles directory
    -------------------------------------------------------------------*/
    if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
      return DS_UMTS_PDP_FAIL;
  
    /*-------------------------------------------------------------------
      Open the file for read / write
    -------------------------------------------------------------------*/
    DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ", 
                            ds_umts_pdp_fully_qual_profile_file_name);
    
    fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                          O_RDWR);
    if ( fs_handle == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_open failed for %d errno=%d.", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      Set the file offset to the start of context information
    -------------------------------------------------------------------*/
    if (efs_lseek(fs_handle,
                  DS_UMTS_PDP_PROFILE_OTAP_ENABLED_FLAG_OFFSET,
                  SEEK_SET) == -1)
    {
      DS_3GPP_MSG3_ERROR( "efs_lseek failed for %d errno=%d", fs_handle, efs_errno, 0 );
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      write the  profile napid
    -------------------------------------------------------------------*/
    if (ds_umts_pdp_efs_write(fs_handle,
                              (void *)(&otap_enabled_flag),
                              sizeof(boolean)) == FALSE)
    {
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    
    /*-------------------------------------------------------------------
      Close the file
    -------------------------------------------------------------------*/
    if (efs_close (fs_handle) == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_close failed for %d errno = %d", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_FAIL;
    }
  }
  /*-------------------------------------------------------------------
    Update the cache table with the new otap_enabled_flag value.
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_TASKLOCK_NON_L4();
  memscpy( (void*)&ds_umts_pdp_profile_ptr->otap_enabled_flag,
  	  sizeof(boolean),
          (void*)&otap_enabled_flag,
          sizeof(boolean) );
  DS_UMTS_PDP_TASKFREE_NON_L4();

  return DS_UMTS_PDP_SUCCESS;  
}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_REQUEST_PCSCF_ADDRESS_FLAG_INTERNAL

DESCRIPTION
  This function sets the "request_pcscf_address_flag" flag specified for a 
  profile. 
  The "request_pcscf_address_flag" indicates if the pcscf address should be
  requested for this profile using PCO or not.

PARAMETERS
  profile_number  : profile number
  request_pcscf_address_flag   : pointer to store the request_pcscf_address_flag


DEPENDENCIES
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system

SIDE EFFECTS
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype 
  ds_umts_set_pdp_profile_request_pcscf_address_flag_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  boolean                   request_pcscf_address_flag
)
{
  int                  fs_handle;
  ds_umts_pdp_profile_type        *ds_umts_pdp_profile_ptr;
  boolean read_only_flag;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
 
  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
    A valid profile need not be defined at this time to set the name    
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 
  
  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  ds_umts_pdp_profile_ptr = 
    DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);

  /* Update EFS only if profile is persistent */
  if( DS_UMTS_PDP_PROFILE_IS_PERSISTENT(ds_umts_pdp_profile_ptr)) 
  {
    /*-------------------------------------------------------------------
      Init the fully qualified profile file name to pdp_profiles directory
    -------------------------------------------------------------------*/
    if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
      return DS_UMTS_PDP_FAIL;
  
    /*-------------------------------------------------------------------
      Open the file for read / write
    -------------------------------------------------------------------*/
    DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ", 
                            ds_umts_pdp_fully_qual_profile_file_name);
      
    fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                          O_RDWR);
    if ( fs_handle == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_open failed for %d errno=%d.", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      Set the file offset to the start of context information
    -------------------------------------------------------------------*/
    if (efs_lseek(fs_handle,
                  DS_UMTS_PDP_PROFILE_REQUEST_PCSCF_ADDRESS_FLAG_OFFSET,
                  SEEK_SET) == -1)
    {
      DS_3GPP_MSG3_ERROR( "efs_lseek failed for %d errno=%d", fs_handle, efs_errno, 0 );
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      write the profile pcscf address
    -------------------------------------------------------------------*/
    if (ds_umts_pdp_efs_write(fs_handle,
                              (void *)(&request_pcscf_address_flag),
                              sizeof(boolean)) == FALSE) 
    {
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    
    /*-------------------------------------------------------------------
      Close the file
    -------------------------------------------------------------------*/
    if (efs_close (fs_handle) == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_close failed for %d errno = %d", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  }
  
  /*-------------------------------------------------------------------
    Update the cache table with the new read_only_flag value.
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_TASKLOCK_NON_L4();
  memscpy( (void*)&ds_umts_pdp_profile_ptr->request_pcscf_address_flag,
          sizeof(boolean),
          (void*)&request_pcscf_address_flag,
          sizeof(boolean) );
  DS_UMTS_PDP_TASKFREE_NON_L4();
  
  return DS_UMTS_PDP_SUCCESS;  
}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_REQUEST_PCSCF_ADDRESS_USING_DHCP_FLAG_INTERNAL

DESCRIPTION
  This function sets the "request_pcscf_address_using_dhcp_flag" flag
  specified for a profile. 
  The "request_pcscf_address_using_dhcp_flag" indicates if the pcscf address
  should be requested for this profile using DHCP or not.

PARAMETERS
  profile_number  : profile number
  request_pcscf_address_using_dhcp_flag   : pointer to store the 
                  request_pcscf_address_using_dhcp_flag


DEPENDENCIES
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system

SIDE EFFECTS
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype 
  ds_umts_set_pdp_profile_request_pcscf_address_using_dhcp_flag_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  boolean                   request_pcscf_address_using_dhcp_flag
)
{
  int                  fs_handle;
  ds_umts_pdp_profile_type        *ds_umts_pdp_profile_ptr;
  boolean read_only_flag;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
 
  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
    A valid profile need not be defined at this time to set the name    
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 
  
  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  /*-------------------------------------------------------------------
    Initialize the variables for fs_open                   
  -------------------------------------------------------------------*/

  ds_umts_pdp_profile_ptr = 
    DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);

  
  /* Update EFS only if profile is persistent */
  if(DS_UMTS_PDP_PROFILE_IS_PERSISTENT(ds_umts_pdp_profile_ptr)) 
  {
    /*-------------------------------------------------------------------
      Init the fully qualified profile file name to pdp_profiles directory
    -------------------------------------------------------------------*/
    if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
      return DS_UMTS_PDP_FAIL;
  
    /*-------------------------------------------------------------------
      Open the file for read / write
    -------------------------------------------------------------------*/
    DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ", 
                            ds_umts_pdp_fully_qual_profile_file_name);
      
    fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                          O_RDWR);
    if ( fs_handle == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_open failed for %d errno=%d.", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      Set the file offset to the start of context information
    -------------------------------------------------------------------*/
    if (efs_lseek(fs_handle,
          DS_UMTS_PDP_PROFILE_REQUEST_PCSCF_ADDRESS_USING_DHCP_FLAG_OFFSET,
          SEEK_SET) == -1)
    {
      DS_3GPP_MSG3_ERROR( "efs_lseek failed for %d errno=%d", fs_handle, efs_errno, 0 );
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      write the profile pcscf address
    -------------------------------------------------------------------*/
    if (ds_umts_pdp_efs_write(fs_handle,
                              (void *)(&request_pcscf_address_using_dhcp_flag),
                              sizeof(boolean)) == FALSE) 
    {
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    
    /*-------------------------------------------------------------------
      Close the file
    -------------------------------------------------------------------*/
    if (efs_close (fs_handle) == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_close failed for %d errno = %d", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  }

  /*-------------------------------------------------------------------
    Update the cache table with the new read_only_flag value.
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_TASKLOCK_NON_L4();
  memscpy( (void*)&ds_umts_pdp_profile_ptr->
      request_pcscf_address_using_dhcp_flag,
          sizeof(boolean),
          (void*)&request_pcscf_address_using_dhcp_flag,
          sizeof(boolean) );
  DS_UMTS_PDP_TASKFREE_NON_L4();
  
  return DS_UMTS_PDP_SUCCESS;  
}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_IM_CN_FLAG_INTERNAL

DESCRIPTION
  This function sets the "im_cn" flag specified for a 
  profile. 
  The "im_cn" indicates if the im_cn flag should be
  requested for this profile or not.

PARAMETERS
  profile_number  : profile number
  im_cn_flag   : pointer to store the im_cn_flag


DEPENDENCIES
  ds_umts_pdp_registry_init_flag should be set(we should have been
  initialized)

RETURN VALUE
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system

SIDE EFFECTS
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype 
  ds_umts_set_pdp_profile_im_cn_flag_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  boolean                   im_cn_flag
)
{
  int                             fs_handle;   
  ds_umts_pdp_profile_type        *ds_umts_pdp_profile_ptr;
  boolean read_only_flag;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
 
  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
    A valid profile need not be defined at this time to set the name    
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 
  
  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  /*-------------------------------------------------------------------
    Initialize the variables for fs_open                   
  -------------------------------------------------------------------*/

  ds_umts_pdp_profile_ptr = 
    DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);

  /* Update EFS only if profile is persistent */
  if(DS_UMTS_PDP_PROFILE_IS_PERSISTENT(ds_umts_pdp_profile_ptr)) 
  {
    /*-------------------------------------------------------------------
      Init the fully qualified profile file name to pdp_profiles directory
    -------------------------------------------------------------------*/
    if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
      return DS_UMTS_PDP_FAIL;
  
    /*-------------------------------------------------------------------
      Open the file for read / write
    -------------------------------------------------------------------*/
    DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ", 
                            ds_umts_pdp_fully_qual_profile_file_name);
      
    fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                          O_RDWR);
    if ( fs_handle == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_open failed for %d errno=%d.", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      Set the file offset to the start of context information
    -------------------------------------------------------------------*/
    if (efs_lseek(fs_handle,
                  DS_UMTS_PDP_PROFILE_IM_CN_FLAG_OFFSET,
                  SEEK_SET) == -1)
    {
      DS_3GPP_MSG3_ERROR( "efs_lseek failed for %d errno=%d", fs_handle, efs_errno, 0 );
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      write the profile im_cn_flag
    -------------------------------------------------------------------*/
    if (ds_umts_pdp_efs_write(fs_handle,
                              (void *)(&im_cn_flag),
                              sizeof(boolean)) == FALSE) 
    {
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    
    /*-------------------------------------------------------------------
      Close the file
    -------------------------------------------------------------------*/
    if (efs_close (fs_handle) == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_close failed for %d errno = %d", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  }

  /*-------------------------------------------------------------------
    Update the cache table with the new read_only_flag value.
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_TASKLOCK_NON_L4();
  memscpy( (void*)&ds_umts_pdp_profile_ptr->im_cn_flag,
  	   sizeof(boolean),
          (void*)&im_cn_flag,
          sizeof(boolean) );
  DS_UMTS_PDP_TASKFREE_NON_L4();
  
  return DS_UMTS_PDP_SUCCESS;  
}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_APN_DISABLE_FLAG_INTERNAL

DESCRIPTION
  This function call the internal registry function to update the apn diable
  flag setting for the profile specified with the value passed. The update 
  can be done ONLY for a valid context as it is part of the context 
  definition. It also updates the cache table with the new value.

PARAMETERS
  profile_number   : profile number
  apn_disable_flag : boolean value to set in the registry

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  A valid context must have been defined for the profile number

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system
  DS_UMTS_PDP_CONTEXT_NOT_DEFINED  : valid context not defined for profile

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype 
ds_umts_set_pdp_profile_apn_disable_flag_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  boolean                   apn_disable_flag
)
{
  int                             fs_handle;   
  ds_umts_pdp_profile_type        *ds_umts_pdp_profile_ptr;
  boolean read_only_flag;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
 
  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
    A valid profile need not be defined at this time to set the name    
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 
  
  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  /*-------------------------------------------------------------------
    Initialize the variables for fs_open                   
  -------------------------------------------------------------------*/

  ds_umts_pdp_profile_ptr = 
    DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);
  /* Update EFS only if profile is persistent */
  if(DS_UMTS_PDP_PROFILE_IS_PERSISTENT(ds_umts_pdp_profile_ptr)) 
  {
    /*-------------------------------------------------------------------
      Init the fully qualified profile file name to pdp_profiles directory
    -------------------------------------------------------------------*/
    if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
      return DS_UMTS_PDP_FAIL;
  
    /*-------------------------------------------------------------------
      Open the file for read / write
    -------------------------------------------------------------------*/
    DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ", 
                            ds_umts_pdp_fully_qual_profile_file_name);
      
    fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                          O_RDWR);
    if ( fs_handle == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_open failed for %d errno=%d.", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      Set the file offset to the start of context information
    -------------------------------------------------------------------*/
    if (efs_lseek(fs_handle,
                  DS_UMTS_PDP_APN_DISABLE_FLAG_OFFSET,
                  SEEK_SET) == -1)
    {
      DS_3GPP_MSG3_ERROR( "efs_lseek failed for %d errno=%d", fs_handle, efs_errno, 0 );
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      write the profile apn_disable_flag
    -------------------------------------------------------------------*/
    if (ds_umts_pdp_efs_write(fs_handle,
                              (void *)(&apn_disable_flag),
                              sizeof(boolean)) == FALSE) 
    {
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    
    /*-------------------------------------------------------------------
      Close the file
    -------------------------------------------------------------------*/
    if (efs_close (fs_handle) == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_close failed for %d errno = %d", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  }

  /*-------------------------------------------------------------------
    Update the cache table with the new read_only_flag value.
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_TASKLOCK_NON_L4();
  memscpy( (void*)&ds_umts_pdp_profile_ptr->apn_disable_flag,
  	   sizeof(boolean),
          (void*)&apn_disable_flag,
          sizeof(boolean) );
  DS_UMTS_PDP_TASKFREE_NON_L4();
  
  return DS_UMTS_PDP_SUCCESS;
} /* ds_umts_set_pdp_profile_apn_disable_flag_internal */

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_ROAMING_DISALLOWED_FLAG_INTERNAL

DESCRIPTION
  This function call the internal registry function to update the roaming
  disallowed flag setting for the profile specified with the value passed. 
  The update can be done ONLY for a valid context as it is part of the context 
  definition. It also updates the cache table with the new value.

PARAMETERS
  profile_number     : profile number
  roaming_disallowed : boolean value to set in the registry

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  A valid context must have been defined for the profile number

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system
  DS_UMTS_PDP_CONTEXT_NOT_DEFINED  : valid context not defined for profile

SIDE EFFECTS 
  none
===========================================================================*/
ds_umts_pdp_profile_status_etype 
  ds_umts_set_pdp_profile_roaming_disallowed_flag_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  boolean                   roaming_disallowed
)
{
  int                             fs_handle;   
  ds_umts_pdp_profile_type        *ds_umts_pdp_profile_ptr;
  boolean read_only_flag;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
 
  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
    A valid profile need not be defined at this time to set the name    
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 
  
  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  /*-------------------------------------------------------------------
    Initialize the variables for fs_open                   
  -------------------------------------------------------------------*/

  ds_umts_pdp_profile_ptr = 
    DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);
  /* Update EFS only if profile is persistent */
  if(DS_UMTS_PDP_PROFILE_IS_PERSISTENT(ds_umts_pdp_profile_ptr)) 
  {
    /*-------------------------------------------------------------------
      Init the fully qualified profile file name to pdp_profiles directory
    -------------------------------------------------------------------*/
    if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,
                                                   subs_id) == FALSE)
      return DS_UMTS_PDP_FAIL;
  
    /*-------------------------------------------------------------------
      Open the file for read / write
    -------------------------------------------------------------------*/
    DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ", 
                            ds_umts_pdp_fully_qual_profile_file_name);
      
    fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                          O_RDWR);
    if ( fs_handle == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_open failed for %d errno=%d.", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      Set the file offset to the start of context information
    -------------------------------------------------------------------*/
    if (efs_lseek(fs_handle,
                  DS_UMTS_PDP_PROFILE_ROAMING_DISALLOWED_FLAG_OFFSET,
                  SEEK_SET) == -1)
    {
      DS_3GPP_MSG3_ERROR( "efs_lseek failed for %d errno=%d", fs_handle, efs_errno, 0 );
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      write the profile roaming_disallowed_flag
    -------------------------------------------------------------------*/
    if (ds_umts_pdp_efs_write(fs_handle,
                              (void *)(&roaming_disallowed),
                              sizeof(boolean)) == FALSE) 
    {
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    
    /*-------------------------------------------------------------------
      Close the file
    -------------------------------------------------------------------*/
    if (efs_close (fs_handle) == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_close failed for %d errno = %d", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  }

  /*-------------------------------------------------------------------
    Update the cache table with the new read_only_flag value.
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_TASKLOCK_NON_L4();
  ds_umts_pdp_profile_ptr->roaming_disallowed = roaming_disallowed;
  DS_UMTS_PDP_TASKFREE_NON_L4();
  
  return DS_UMTS_PDP_SUCCESS;
}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_PDN_DISCON_WAIT_TIME_INTERNAL

DESCRIPTION
  This function call the internal registry function to update the PDN Disconnect
  Wait Time setting for the profile specified with the value passed. 
  The update can be done ONLY for a valid context as it is part of the context 
  definition. It also updates the cache table with the new value.

PARAMETERS
  profile_number       : profile number
  pdn_discon_wait_time : uint8 for PDN Disconnext wait time in seconds

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  A valid context must have been defined for the profile number

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system
  DS_UMTS_PDP_CONTEXT_NOT_DEFINED  : valid context not defined for profile

SIDE EFFECTS 
  none
===========================================================================*/
ds_umts_pdp_profile_status_etype 
  ds_umts_set_pdp_profile_pdn_discon_wait_time_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  uint8                     pdn_discon_wait_time
)
{
  int                             fs_handle;   
  ds_umts_pdp_profile_type        *ds_umts_pdp_profile_ptr;
  boolean read_only_flag;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
 
  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
    A valid profile need not be defined at this time to set the name    
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 
  
  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  /*-------------------------------------------------------------------
    Initialize the variables for fs_open                   
  -------------------------------------------------------------------*/

  ds_umts_pdp_profile_ptr = 
    DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);
  /* Update EFS only if profile is persistent */
  if(DS_UMTS_PDP_PROFILE_IS_PERSISTENT(ds_umts_pdp_profile_ptr)) 
  {
    /*-------------------------------------------------------------------
      Init the fully qualified profile file name to pdp_profiles directory
    -------------------------------------------------------------------*/
    if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
      return DS_UMTS_PDP_FAIL;
  
    /*-------------------------------------------------------------------
      Open the file for read / write
    -------------------------------------------------------------------*/
    DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ", 
                            ds_umts_pdp_fully_qual_profile_file_name);
      
    fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                          O_RDWR);
    if ( fs_handle == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_open failed for %d errno=%d.", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      Set the file offset to the start of context information
    -------------------------------------------------------------------*/
    if (efs_lseek(fs_handle,
                  DS_UMTS_PDP_PROFILE_PDN_DISCON_WAIT_TIME_OFFSET,
                  SEEK_SET) == -1)
    {
      DS_3GPP_MSG3_ERROR( "efs_lseek failed for %d errno=%d", fs_handle, efs_errno, 0 );
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      write the profile roaming_disallowed_flag
    -------------------------------------------------------------------*/
    if (ds_umts_pdp_efs_write(fs_handle,
                              (void *)(&pdn_discon_wait_time),
                              sizeof(uint8)) == FALSE) 
    {
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    
    /*-------------------------------------------------------------------
      Close the file
    -------------------------------------------------------------------*/
    if (efs_close (fs_handle) == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_close failed for %d errno = %d", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  }

  /*-------------------------------------------------------------------
    Update the cache table with the new read_only_flag value.
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_TASKLOCK_NON_L4();
  ds_umts_pdp_profile_ptr->pdn_discon_wait_time = pdn_discon_wait_time;
  DS_UMTS_PDP_TASKFREE_NON_L4();
  
  return DS_UMTS_PDP_SUCCESS;
}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_INACTIVITY_TIMER_VAL_INTERNAL

DESCRIPTION
  This function call the internal registry function to update the inactivity
  timer setting for the profile specified with the value passed. The update 
  can be done ONLY for a valid context as it is part of the context 
  definition. It also updates the cache table with the new value.

PARAMETERS
  profile_number       : profile number
  inactivity_timer_val : boolean value to set in the registry

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  A valid context must have been defined for the profile number

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system
  DS_UMTS_PDP_CONTEXT_NOT_DEFINED  : valid context not defined for profile

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype 
ds_umts_set_pdp_profile_inactivity_timer_val_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  uint32                    inactivity_timer_val
)
{
  int                             fs_handle;   
  ds_umts_pdp_profile_type        *ds_umts_pdp_profile_ptr;
  boolean read_only_flag;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
 
  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
    A valid profile need not be defined at this time to set the name    
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 
  
  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  /*-------------------------------------------------------------------
    Initialize the variables for fs_open                   
  -------------------------------------------------------------------*/

  ds_umts_pdp_profile_ptr = 
    DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);
  /* Update EFS only if profile is persistent */

  if(DS_UMTS_PDP_PROFILE_IS_PERSISTENT(ds_umts_pdp_profile_ptr)) 
  {
    /*-------------------------------------------------------------------
      Init the fully qualified profile file name to pdp_profiles directory
    -------------------------------------------------------------------*/
    if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
      return DS_UMTS_PDP_FAIL;
  
    /*-------------------------------------------------------------------
      Open the file for read / write
    -------------------------------------------------------------------*/
    DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ", 
                            ds_umts_pdp_fully_qual_profile_file_name);
      
    fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                          O_RDWR);
    if ( fs_handle == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_open failed for %d errno=%d.", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      Set the file offset to the start of context information
    -------------------------------------------------------------------*/
    if (efs_lseek(fs_handle,
                  DS_UMTS_PDP_INACTIVITY_TIMER_VAL_OFFSET,
                  SEEK_SET) == -1)
    {
      DS_3GPP_MSG3_ERROR( "efs_lseek failed for %d errno=%d", fs_handle, efs_errno, 0 );
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      write the profile im_cn_flag
    -------------------------------------------------------------------*/
    if (ds_umts_pdp_efs_write(fs_handle,
                              (void *)(&inactivity_timer_val),
                              sizeof(uint32)) == FALSE) 
    {
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    
    /*-------------------------------------------------------------------
      Close the file
    -------------------------------------------------------------------*/
    if (efs_close (fs_handle) == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_close failed for %d errno = %d", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  }

  /*-------------------------------------------------------------------
    Update the cache table with the new read_only_flag value.
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_TASKLOCK_NON_L4();
  memscpy( (void*)&ds_umts_pdp_profile_ptr->inactivity_timer_val,
  	  sizeof(uint32),
          (void*)&inactivity_timer_val,
          sizeof(uint32) );
  DS_UMTS_PDP_TASKFREE_NON_L4();
  
  return DS_UMTS_PDP_SUCCESS;
} /* ds_umts_set_pdp_profile_inactivity_timer_val_internal */

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_APN_CLASS_INTERNAL

DESCRIPTION
  This function calls the registry internal function to set the 
  "apn_class" for a specified profile number. 
  This parameter is retrieved only if a valid context is 
  defined for the profile specified.

PARAMETERS
  profile_number   : profile number
  apn_class        : uint8 value being passed in for the APN class

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  A valid context must have been defined for the profile number

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system
  DS_UMTS_PDP_CONTEXT_NOT_DEFINED  : valid context not defined for profile

SIDE EFFECTS 
  none
===========================================================================*/
ds_umts_pdp_profile_status_etype 
ds_umts_set_pdp_profile_apn_class_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  uint8                     apn_class
)
{
  int                             fs_handle;   
  ds_umts_pdp_profile_type        *ds_umts_pdp_profile_ptr;

  boolean read_only_flag;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
 
  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
    A valid profile need not be defined at this time to set the name    
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 
  
  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  /*-------------------------------------------------------------------
    Initialize the variables for fs_open                   
  -------------------------------------------------------------------*/

  ds_umts_pdp_profile_ptr = 
    DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);

  if(DS_UMTS_PDP_PROFILE_IS_PERSISTENT(ds_umts_pdp_profile_ptr)) 
  {
    /*-------------------------------------------------------------------
      Init the fully qualified profile file name to pdp_profiles directory
    -------------------------------------------------------------------*/
    if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
      return DS_UMTS_PDP_FAIL;
  
    /*-------------------------------------------------------------------
      Open the file for read / write
    -------------------------------------------------------------------*/
    DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ", 
                            ds_umts_pdp_fully_qual_profile_file_name);
      
    fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                          O_RDWR);
    if ( fs_handle == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_open failed for %d errno=%d.", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      Set the file offset to the start of context information
    -------------------------------------------------------------------*/
    if (efs_lseek(fs_handle,
                  DS_UMTS_PDP_APN_CLASS_OFFSET,
                  SEEK_SET) == -1)
    {
      ERR( "efs_lseek failed for %d errno=%d", fs_handle, efs_errno, 0 );
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      write the profile apn_class
    -------------------------------------------------------------------*/
    if (ds_umts_pdp_efs_write(fs_handle,
                              (void *)(&apn_class),
                              sizeof(uint8)) == FALSE) 
    {
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    
    /*-------------------------------------------------------------------
      Close the file
    -------------------------------------------------------------------*/
    if (efs_close (fs_handle) == -1)
    {
      ERR("efs_close failed for %d errno = %d", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  }

  /*-------------------------------------------------------------------
    Update the cache table with the new read_only_flag value.
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_TASKLOCK_NON_L4();
  memscpy( (void*)&ds_umts_pdp_profile_ptr->apn_class,
  	  sizeof(uint8),
          (void*)&apn_class,
          sizeof(uint8) );
  DS_UMTS_PDP_TASKFREE_NON_L4();
  
  return DS_UMTS_PDP_SUCCESS;
} /* ds_umts_set_pdp_profile_apn_class_internal */


/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_APN_BEARER_INTERNAL

DESCRIPTION
  This function calls the registry internal function to set the 
  "apn_bearer" for a specified profile number. 
  This parameter is retrieved only if a valid context is 
  defined for the profile specified.

PARAMETERS
  profile_number   : profile number
  apn_bearer        : uint8 value being passed in for the APN class

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  A valid context must have been defined for the profile number

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system
  DS_UMTS_PDP_CONTEXT_NOT_DEFINED  : valid context not defined for profile

SIDE EFFECTS 
  none
===========================================================================*/
ds_umts_pdp_profile_status_etype 
ds_umts_set_pdp_profile_apn_bearer_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  uint8                     apn_bearer
)
{
  int                             fs_handle;   
  ds_umts_pdp_profile_type        *ds_umts_pdp_profile_ptr;

  boolean read_only_flag;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
 
  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
    A valid profile need not be defined at this time to set the name    
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 
  
  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  /*-------------------------------------------------------------------
    Initialize the variables for fs_open                   
  -------------------------------------------------------------------*/

  if(apn_bearer != DS_UMTS_PDP_PROFILE_APN_BEARER_FLAG_ALL
     && (apn_bearer & ~(DS_UMTS_PDP_PROFILE_APN_BEARER_FLAG_G
#ifdef FEATURE_TDSCDMA
                   | DS_UMTS_PDP_PROFILE_APN_BEARER_FLAG_T
#endif
                   | DS_UMTS_PDP_PROFILE_APN_BEARER_FLAG_W
                   | DS_UMTS_PDP_PROFILE_APN_BEARER_FLAG_L)) != 0)
  {
    DS_3GPP_MSG3_ERROR("ds_umts_set_pdp_profile_apn_bearer_internal:"
                       "invalid arg apn_bearer %d", apn_bearer, 0, 0);
    return DS_UMTS_PDP_FAIL;
  }

  ds_umts_pdp_profile_ptr = 
    DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);

  if(DS_UMTS_PDP_PROFILE_IS_PERSISTENT(ds_umts_pdp_profile_ptr)) 
  {
    /*-------------------------------------------------------------------
      Init the fully qualified profile file name to pdp_profiles directory
    -------------------------------------------------------------------*/
    if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
      return DS_UMTS_PDP_FAIL;
  
    /*-------------------------------------------------------------------
      Open the file for read / write
    -------------------------------------------------------------------*/
    DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ", 
                            ds_umts_pdp_fully_qual_profile_file_name);
      
    fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                          O_RDWR);
    if ( fs_handle == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_open failed for %d errno=%d.", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      Set the file offset to the start of context information
    -------------------------------------------------------------------*/
    if (efs_lseek(fs_handle,
                  DS_UMTS_PDP_PROFILE_APN_BEARER_OFFSET,
                  SEEK_SET) == -1)
    {
      ERR( "efs_lseek failed for %d errno=%d", fs_handle, efs_errno, 0 );
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      write the profile apn_class
    -------------------------------------------------------------------*/
    if (ds_umts_pdp_efs_write(fs_handle,
                              (void *)(&apn_bearer),
                              sizeof(uint8)) == FALSE) 
    {
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    
    /*-------------------------------------------------------------------
      Close the file
    -------------------------------------------------------------------*/
    if (efs_close (fs_handle) == -1)
    {
      ERR("efs_close failed for %d errno = %d", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  }

  /*-------------------------------------------------------------------
    Update the cache table with the new read_only_flag value.
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_TASKLOCK_NON_L4();
  memscpy( (void*)&ds_umts_pdp_profile_ptr->apn_bearer,
  	  sizeof(uint8),
          (void*)&apn_bearer,
          sizeof(uint8) );
  DS_UMTS_PDP_TASKFREE_NON_L4();
  
  return DS_UMTS_PDP_SUCCESS;
} /* ds_umts_set_pdp_profile_apn_class_internal */

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_MCC_INTERNAL

DESCRIPTION
  This function calls the registry internal function to set the 
  "MCC" for a specified profile number. 
  This parameter is retrieved only if a valid context is 
  defined for the profile specified.

PARAMETERS
  profile_number   : profile number
  mcc              : MCC of the carrier

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  A valid context must have been defined for the profile number

RETURN VALUE 
  

SIDE EFFECTS 
  none
===========================================================================*/

ds_umts_pdp_profile_status_etype 
ds_umts_set_pdp_profile_mcc_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  uint16                    mcc
)
{
   int                             fs_handle;   
  ds_umts_pdp_profile_type        *ds_umts_pdp_profile_ptr;
  boolean read_only_flag;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /*-------------------------------------------------------------------
      Check if we have been initialized & the input paramaters are correct
      A valid profile need not be defined at this time to set the name    
    -------------------------------------------------------------------*/
  
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 
  
  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  /*-------------------------------------------------------------------
    Initialize the variables for fs_open                   
  -------------------------------------------------------------------*/
  ds_umts_pdp_profile_ptr = 
    DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);

  if(DS_UMTS_PDP_PROFILE_IS_PERSISTENT(ds_umts_pdp_profile_ptr)) 
  {
    /*-------------------------------------------------------------------
      Init the fully qualified profile file name to pdp_profiles directory
    -------------------------------------------------------------------*/
    if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
      return DS_UMTS_PDP_FAIL;

    /*-------------------------------------------------------------------
      Open the file for read / write
    -------------------------------------------------------------------*/
    DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ", 
                            ds_umts_pdp_fully_qual_profile_file_name);
      
    fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                          O_RDWR);
    if ( fs_handle == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_open failed for %d errno=%d.", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }


    /*-------------------------------------------------------------------
      Set the file offset to the start of MCC information
    -------------------------------------------------------------------*/
    if (efs_lseek(fs_handle,
                  DS_UMTS_PDP_PROFILE_MCC_OFFSET,
                  SEEK_SET) == -1)
    {
      ERR( "efs_lseek failed for %d errno=%d", fs_handle, efs_errno, 0 );
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    

    /*-------------------------------------------------------------------
      write the profile for mcc
    -------------------------------------------------------------------*/
    if (ds_umts_pdp_efs_write(fs_handle,
                              (void *)&mcc,
                              sizeof(uint16)) == FALSE) 
    {
      return DS_UMTS_PDP_ACCESS_ERROR;
    }


     /*---------------------------------------------------------------
      Close the file
    -------------------------------------------------------------------*/
    if (efs_close (fs_handle) == -1)
    {
      ERR("efs_close failed for %d errno = %d", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  }

  /*-------------------------------------------------------------------
    Update the cache table with the new read_only_flag value.
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_TASKLOCK_NON_L4();
  memscpy( (void*)&ds_umts_pdp_profile_ptr->mcc,
  	  sizeof(uint16),
          (void*)&mcc,
          sizeof(uint16) );
  DS_UMTS_PDP_TASKFREE_NON_L4();
 
  
  return DS_UMTS_PDP_SUCCESS;

}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_MNC_INTERNAL

DESCRIPTION
  This function calls the registry internal function to set the 
  "MNC" for a specified profile number. 
  This parameter is retrieved only if a valid context is 
  defined for the profile specified.

PARAMETERS
  profile_number   : profile number
  mnc              : MNC for the carrier 

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  A valid context must have been defined for the profile number

RETURN VALUE 
  

SIDE EFFECTS 
  none
===========================================================================*/

ds_umts_pdp_profile_status_etype 
ds_umts_set_pdp_profile_mnc_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  ds_umts_3gpp_mnc_type     mnc
)
{
    int                             fs_handle;   
    ds_umts_pdp_profile_type        *ds_umts_pdp_profile_ptr;
    boolean read_only_flag;
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

    /*-------------------------------------------------------------------
      Check if we have been initialized & the input paramaters are correct
      A valid profile need not be defined at this time to set the name    
    -------------------------------------------------------------------*/
    DS_UMTS_PDP_CHECK_INIT_FLAG();
    if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
       DS_UMTS_PDP_INVALID_PROFILE_NUM )
      return DS_UMTS_PDP_INVALID_PROFILE_NUM; 

    DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

    /*-------------------------------------------------------------------
      Initialize the variables for fs_open                   
    -------------------------------------------------------------------*/

    ds_umts_pdp_profile_ptr = 
      DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);

    if(DS_UMTS_PDP_PROFILE_IS_PERSISTENT(ds_umts_pdp_profile_ptr)) 
    {
      /*-------------------------------------------------------------------
        Init the fully qualified profile file name to pdp_profiles directory
      -------------------------------------------------------------------*/
      if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
        return DS_UMTS_PDP_FAIL;

      /*-------------------------------------------------------------------
        Open the file for read / write
      -------------------------------------------------------------------*/
      DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ", 
                              ds_umts_pdp_fully_qual_profile_file_name);
        
      fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                            O_RDWR);
      if ( fs_handle == -1)
      {
        DS_3GPP_MSG3_ERROR("efs_open failed for %d errno=%d.", fs_handle, efs_errno, 0);
        return DS_UMTS_PDP_ACCESS_ERROR;
      }

      /*-------------------------------------------------------------------
        Set the file offset to the start of MNC information
      -------------------------------------------------------------------*/
      if (efs_lseek(fs_handle,
                    DS_UMTS_PDP_PROFILE_MNC_OFFSET,
                    SEEK_SET) == -1)
      {
        ERR( "efs_lseek failed for %d errno=%d", fs_handle, efs_errno, 0 );
        return DS_UMTS_PDP_ACCESS_ERROR;
      }


      /*-------------------------------------------------------------------
        write the profile for mnc
      -------------------------------------------------------------------*/
      if (ds_umts_pdp_efs_write(fs_handle,
                                (void *)&mnc,
                                sizeof(ds_umts_3gpp_mnc_type)) == FALSE) 
      {
        return DS_UMTS_PDP_ACCESS_ERROR;
      }


       /*---------------------------------------------------------------
        Close the file
      -------------------------------------------------------------------*/
      if (efs_close (fs_handle) == -1)
      {
        ERR("efs_close failed for %d errno = %d", fs_handle, efs_errno, 0);
        return DS_UMTS_PDP_ACCESS_ERROR;
      }
    }

    /*-------------------------------------------------------------------
      Update the cache table with the new read_only_flag value.
    -------------------------------------------------------------------*/
    DS_UMTS_PDP_TASKLOCK_NON_L4();
    memscpy( (void*)&ds_umts_pdp_profile_ptr->mnc,
            sizeof(ds_umts_3gpp_mnc_type),
            (void*)&mnc,
            sizeof(ds_umts_3gpp_mnc_type) );
    DS_UMTS_PDP_TASKFREE_NON_L4();


    return DS_UMTS_PDP_SUCCESS;

}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_OPERATOR_RESERVED_PCO_INTERNAL

DESCRIPTION
  This function calls the registry internal function to set the operator reserved 
  "PCO" for a specified profile number. 
  This parameter is retrieved only if a valid context is 
  defined for the profile specified.

PARAMETERS
  profile_number   : profile number
  pco              : operator range

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  A valid context must have been defined for the profile number

RETURN VALUE 
  

SIDE EFFECTS 
  none
===========================================================================*/

ds_umts_pdp_profile_status_etype 
ds_umts_set_pdp_profile_operator_reserved_pco_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  uint16                    pco
)
{
    int                             fs_handle;   
    ds_umts_pdp_profile_type        *ds_umts_pdp_profile_ptr;
    boolean read_only_flag;
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

    /*-------------------------------------------------------------------
      Check if we have been initialized & the input paramaters are correct
      A valid profile need not be defined at this time to set the name    
    -------------------------------------------------------------------*/
    DS_UMTS_PDP_CHECK_INIT_FLAG();
    if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
       DS_UMTS_PDP_INVALID_PROFILE_NUM )
      return DS_UMTS_PDP_INVALID_PROFILE_NUM; 

    DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

    /*-------------------------------------------------------------------
      Initialize the variables for fs_open                   
    -------------------------------------------------------------------*/
    ds_umts_pdp_profile_ptr = 
      DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);

    if(DS_UMTS_PDP_PROFILE_IS_PERSISTENT(ds_umts_pdp_profile_ptr)) 
    {
      /*-------------------------------------------------------------------
        Init the fully qualified profile file name to pdp_profiles directory
      -------------------------------------------------------------------*/
      if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
        return DS_UMTS_PDP_FAIL;

      /*-------------------------------------------------------------------
        Open the file for read / write
      -------------------------------------------------------------------*/
      DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ", 
                              ds_umts_pdp_fully_qual_profile_file_name);
        
      fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                            O_RDWR);
      if ( fs_handle == -1)
      {
        DS_3GPP_MSG3_ERROR("efs_open failed for %d errno=%d.", fs_handle, efs_errno, 0);
        return DS_UMTS_PDP_ACCESS_ERROR;
      }


      /*-------------------------------------------------------------------
        Set the file offset to the start of context information
      -------------------------------------------------------------------*/
      if (efs_lseek(fs_handle,
                    DS_UMTS_PDP_PROFILE_OPERATOR_RESERVED_PCO_OFFSET,
                    SEEK_SET) == -1)
      {
        ERR( "efs_lseek failed for %d errno=%d", fs_handle, efs_errno, 0 );
        return DS_UMTS_PDP_ACCESS_ERROR;
      }


      /*-------------------------------------------------------------------
        write the profile for operator reserved pco
      -------------------------------------------------------------------*/
      if (ds_umts_pdp_efs_write(fs_handle,
                                (void *)&pco,
                                sizeof(uint16)) == FALSE) 
      {
        return DS_UMTS_PDP_ACCESS_ERROR;
      }


       /*---------------------------------------------------------------
        Close the file
      -------------------------------------------------------------------*/
      if (efs_close (fs_handle) == -1)
      {
        ERR("efs_close failed for %d errno = %d", fs_handle, efs_errno, 0);
        return DS_UMTS_PDP_ACCESS_ERROR;
      }
    }

    /*-------------------------------------------------------------------
      Update the cache table with the new read_only_flag value.
    -------------------------------------------------------------------*/
    DS_UMTS_PDP_TASKLOCK_NON_L4();
    memscpy( (void*)&ds_umts_pdp_profile_ptr->operator_reserved_pco,
	     sizeof(uint16),
             (void*)&pco,
             sizeof(uint16) );
    DS_UMTS_PDP_TASKFREE_NON_L4();


    return DS_UMTS_PDP_SUCCESS;


}
/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_TRANSIENCE_FLAGS_INTERNAL

DESCRIPTION
  This function calls the registry internal function to set the 
  "transience_flag" for a specified profile number. 
  This parameter is retrieved only if a valid context is 
  defined for the profile specified.
 
PARAMETERS
  profile_number   : profile number
  transience_flag  : boolean value being passed in for the APN class

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  A valid context must have been defined for the profile number

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system
  DS_UMTS_PDP_CONTEXT_NOT_DEFINED  : valid context not defined for profile

SIDE EFFECTS 
  none
===========================================================================*/
ds_umts_pdp_profile_status_etype 
ds_umts_set_pdp_profile_transience_flag_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  boolean                   transience_flag
)
{
  int                             fs_handle;   
  ds_umts_pdp_profile_type        *ds_umts_pdp_profile_ptr;

  boolean read_only_flag;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
 
  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
    A valid profile need not be defined at this time to set the name    
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 
  
  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  /*-------------------------------------------------------------------
    Initialize the variables for fs_open                   
  -------------------------------------------------------------------*/

  ds_umts_pdp_profile_ptr = 
    DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);

  if(DS_UMTS_PDP_PROFILE_IS_PERSISTENT(ds_umts_pdp_profile_ptr)) 
  {
    /*-------------------------------------------------------------------
      Init the fully qualified profile file name to pdp_profiles directory
    -------------------------------------------------------------------*/
    if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
      return DS_UMTS_PDP_FAIL;
  
    /*-------------------------------------------------------------------
      Open the file for read / write
    -------------------------------------------------------------------*/
    DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ", 
                            ds_umts_pdp_fully_qual_profile_file_name);
      
    fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                          O_RDWR);
    if ( fs_handle == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_open failed for %d errno=%d.", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      Set the file offset to the start of context information
    -------------------------------------------------------------------*/
    if (efs_lseek(fs_handle,
                  DS_UMTS_PDP_TRANSIENCE_FLAG_OFFSET,
                  SEEK_SET) == -1)
    {
      ERR( "efs_lseek failed for %d errno=%d", fs_handle, efs_errno, 0 );
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      write the profile apn_class
    -------------------------------------------------------------------*/
    if (ds_umts_pdp_efs_write(fs_handle,
                              (void *)(&transience_flag),
                              sizeof(boolean)) == FALSE) 
    {
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    
    /*-------------------------------------------------------------------
      Close the file
    -------------------------------------------------------------------*/
    if (efs_close (fs_handle) == -1)
    {
      ERR("efs_close failed for %d errno = %d", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  }

  /*-------------------------------------------------------------------
    Update the cache table with the transience_flag value.
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_TASKLOCK_NON_L4();
  memscpy( (void*)&ds_umts_pdp_profile_ptr->transience_flag,
            sizeof(boolean),
            (void*)&transience_flag,
            sizeof(boolean) );
  DS_UMTS_PDP_TASKFREE_NON_L4();
  
  return DS_UMTS_PDP_SUCCESS;
} /* ds_umts_set_pdp_profile_apn_class_internal */
/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_CONTEXT_INFO_INTERNAL

DESCRIPTION
  This function updates the context definition  parameters with the
  values passed.
  It also updates the cache table with the new context information.

PARAMETERS
  profile_number  : profile number
  pdp_context_data  : pointer to the context definition 

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file
  DS_UMTS_PDP_PARMS_VALID_FLAG_NOT_SET : the valid_flg for the context is not 
                                    set to TRUE

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_set_pdp_profile_context_info_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  const ds_umts_pdp_context_type  *pdp_context_data
)
{
  boolean               read_only_flag;
  ds_umts_pdp_profile_type *ds_umts_pdp_profile_ptr;

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  /* initialize the profile on demand basis */

  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 
  
  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  /*-------------------------------------------------------------------------
    Check if the context defn is set to true : only then we allow it 
    to be set. to reset a context to undefined, the reset funs should be used
  -------------------------------------------------------------------------*/
  if( !(pdp_context_data->valid_flg) )
  {
    DS_3GPP_MSG3_ERROR( "context :%d passed is not enabled", profile_number, 0, 0 );
    return  DS_UMTS_PDP_PARMS_VALID_FLAG_NOT_SET ;
  }

  ds_umts_pdp_profile_ptr = DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);

  /*-------------------------------------------------------------------
    Update the cache table with the new context information.
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_TASKLOCK_NON_L4();
  memscpy( (void*)&ds_umts_pdp_profile_ptr->context,
           sizeof(ds_umts_pdp_context_type),
          (void*)pdp_context_data,
          sizeof(ds_umts_pdp_context_type) );
  DS_UMTS_PDP_TASKFREE_NON_L4();
  /*-------------------------------------------------------------------
    Profile may or may not be valid before we are just updating the
    context data here. We will not bother about persistence of the profile.
  -------------------------------------------------------------------*/
 if(DS_UMTS_PDP_PROFILE_IS_PERSISTENT(ds_umts_pdp_profile_ptr)) 
 {
    /*-------------------------------------------------------------------
      The profile is already a persistent profile as transient flag is false
      in cache and also it is valid in cache. It must be present in EFS.
    -------------------------------------------------------------------*/    
    /*-------------------------------------------------------------------
      write everything from cache
    -------------------------------------------------------------------*/
    ds_umts_pdp_profile_ptr->transience_flag = FALSE;
    if (!ds_umts_pdp_write_persistent_profile_file(profile_number,subs_id,ds_umts_pdp_profile_ptr))
    {
      DS_3GPP_MSG3_ERROR("Write failed profile_number %d", profile_number,0, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }

  }
    return DS_UMTS_PDP_SUCCESS;
}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_CONTEXT_INFO_EX_INTERNAL

DESCRIPTION
  This function updates the context definition  parameters with the values
  passed. This is special API called only from ATCOP which will always try to
  create persistent profile. It also updates the cache table with the new
  context information.

PARAMETERS
  profile_number  : profile number
  pdp_context_data  : pointer to the context definition 

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file
  DS_UMTS_PDP_PARMS_VALID_FLAG_NOT_SET : the valid_flg for the context is not 
                                    set to TRUE
  DS_UMTS_PDP_ERR_OUT_OF_PERSISTENT_PROFILES : All persistent profile slots are taken.
                                               Can't create any more persistent profile.

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_set_pdp_profile_context_info_ex_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  const ds_umts_pdp_context_type  *pdp_context_data
)
{
  boolean               read_only_flag;
  ds_umts_pdp_profile_type *ds_umts_pdp_profile_ptr;

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();

  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 
  
  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  /*-------------------------------------------------------------------------
    Check if the context defn is set to true : only then we allow it 
    to be set. to reset a context to undefined, the reset funs should be used
  -------------------------------------------------------------------------*/
  if( !(pdp_context_data->valid_flg) )
  {
    DS_3GPP_MSG3_ERROR( "context :%d passed is not enabled", profile_number, 0, 0 );
    return  DS_UMTS_PDP_PARMS_VALID_FLAG_NOT_SET ;
  }

  ds_umts_pdp_profile_ptr = DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);


  /*-------------------------------------------------------------------
      For Atcop we always try to create persistent profile
  -------------------------------------------------------------------*/

  /*-------------------------------------------------------------------
    Check whether we can allow creation of one more persistent profile. It may
    also be the case that we are just updating a profile which is already created.
  -------------------------------------------------------------------*/
    /* If profile is already valid return based on profile content */
  /*-------------------------------------------------------------------
    Atcop profiles are already marked valid via 
    ds_umts_set_pdp_profile_context_info_to_default so we just need to check
    whether it is already persistent if yes then create else check we can create
    one persistent profile.
  -------------------------------------------------------------------*/

  if((DS_UMTS_PDP_PROFILE_IS_PERSISTENT(ds_umts_pdp_profile_ptr))||
     (ds_umts_pdp_get_num_persistent_profiles(subs_id) < ds_3gpp_num_persistent_profiles))
  {
    /*-------------------------------------------------------------------
      Update the cache table with the new context information.
    -------------------------------------------------------------------*/
    DS_UMTS_PDP_TASKLOCK_NON_L4();
    memscpy( (void*)&ds_umts_pdp_profile_ptr->context,
             sizeof(ds_umts_pdp_context_type),
             (void*)pdp_context_data,
             sizeof(ds_umts_pdp_context_type) );
    DS_UMTS_PDP_TASKFREE_NON_L4();
    ds_umts_pdp_profile_ptr->transience_flag = FALSE;
    /*-------------------------------------------------------------------
      write everything from cache
    -------------------------------------------------------------------*/
    if (!ds_umts_pdp_create_persistent_profile_file(profile_number,subs_id,ds_umts_pdp_profile_ptr))
    {
      DS_3GPP_MSG3_ERROR("Write failed profile_number %d", profile_number,0, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    return DS_UMTS_PDP_SUCCESS;
  }
  else
  {
    DS_3GPP_MSG3_ERROR(" can not create persistent profile", 0, 0, 0);
    /*-------------------------------------------------------------------
      We need to mark the profile invalid here as valid flag for this profile
      will be already set. Also default values are populated in this profile.
    -------------------------------------------------------------------*/
    (ds_umts_pdp_profile_ptr->context).valid_flg = FALSE;
    return DS_UMTS_PDP_ERR_OUT_OF_PERSISTENT_PROFILES;
  }
    /*-------------------------------------------------------------------
      Update the cache table with the new context information.
    -------------------------------------------------------------------*/
  DS_UMTS_PDP_TASKLOCK_NON_L4();
  memscpy( (void*)&ds_umts_pdp_profile_ptr->context,
  	   sizeof(ds_umts_pdp_context_type),
          (void*)pdp_context_data,
           sizeof(ds_umts_pdp_context_type) );
  DS_UMTS_PDP_TASKFREE_NON_L4();
  return DS_UMTS_PDP_SUCCESS;

}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_CONTEXT_INFO_APN_INTERNAL

DESCRIPTION
  This function updates the APN for the profile specified with the
  value passed. The APN  can be updated ONLY for a valid context as it
  is part of the context definition
  It also updates the cache table with the new APN string.

PARAMETERS
  profile_number  : profile number
  apn_buf         : pointer to the APN string
  apn_buf_len     : length of the APN string

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  A valid context must have been defined for the profile number already

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file
  DS_UMTS_PDP_CONTEXT_NOT_DEFINED  : valid context not defined for profile
  DS_UMTS_PDP_INPUT_BUFFER_LEN_INVALID : length of APN string is too long

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_set_pdp_profile_context_info_apn_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  const byte                *apn_buf,
  byte                      apn_buf_len
)
{
  int        fs_handle;
  boolean               context_valid;
  ds_umts_pdp_profile_status_etype status;
  char                  asciz=0x0;
  ds_umts_pdp_profile_type *ds_umts_pdp_profile_ptr;
  boolean                read_only_flag;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  /*-------------------------------------------------------------------------
    Check if we have a valid context defined first. The init flag and profile
    numbers are checked in ds_umts_get_pdp_profile_context_info_is_valid
  --------------------------------------------------------------------------*/
  status = ds_umts_get_pdp_profile_context_info_is_valid_per_subs(
              profile_number,
              subs_id,
              &context_valid);

  if(status != DS_UMTS_PDP_SUCCESS )
  {
    return status;
  }
  
  if(!context_valid )
  {
    DS_3GPP_MSG3_HIGH( "Ctx not def PR:%d APN update fail ",
      profile_number,0,0);
    return DS_UMTS_PDP_CONTEXT_NOT_DEFINED;
  }

  /*-------------------------------------------------------------------
    Check of the length of the name is valid and within range
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_IN_BUFF_LEN(apn_buf_len,DS_UMTS_MAX_APN_STRING_LEN);

  ds_umts_pdp_profile_ptr = 
    DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);

 if(DS_UMTS_PDP_PROFILE_IS_PERSISTENT(ds_umts_pdp_profile_ptr)) 
  {
    /*-------------------------------------------------------------------
        Init the fully qualified profile file name to pdp_profiles directory
    -------------------------------------------------------------------*/
    if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
      return DS_UMTS_PDP_FAIL;
  
    /*-------------------------------------------------------------------
      Open the file for write 
    -------------------------------------------------------------------*/
    DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ", 
                            ds_umts_pdp_fully_qual_profile_file_name);
    
    fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                          O_RDWR);
    if ( fs_handle == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_open failed for %d errno=%d.", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      Set the file offset to the start of APN
    -------------------------------------------------------------------*/
    if (efs_lseek(fs_handle,
                  DS_UMTS_PDP_CONTEXT_APN_OFFSET,
                  SEEK_SET) == -1)
    {
      DS_3GPP_MSG3_ERROR( "efs_lseek failed for %d errno=%d", fs_handle, efs_errno, 0 );
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      write the APN 
    -------------------------------------------------------------------*/
    if (ds_umts_pdp_efs_write(fs_handle,
                              (void *)apn_buf,
                              apn_buf_len) == FALSE)
    {
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    
    /*----------------------------------------------------------------------
      Terminate the string wigh ASCIZ .. so that reading is easy with 
      strlcpy
    -----------------------------------------------------------------------*/
    if (ds_umts_pdp_efs_write(fs_handle,
                              (void *)&asciz,
                              sizeof(char)) == FALSE)
    {
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    
    /*-------------------------------------------------------------------
      Close the file
    -------------------------------------------------------------------*/
    if (efs_close (fs_handle) == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_close failed for %d errno = %d", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
 }
  /*-------------------------------------------------------------------
    Update the cache table with the new apn.
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_TASKLOCK_NON_L4();

  /*-------------------------------------------------------------------
    Reset the previous APN name before updating new APN name.
  -------------------------------------------------------------------*/
  memset((void *)ds_umts_pdp_profile_ptr->context.apn, 
         0, 
         DS_UMTS_MAX_APN_STRING_LEN+1);
    
  /*lint -e534 ignoring return code from function*/
  strlcpy( (char*)ds_umts_pdp_profile_ptr->context.apn,
          (char*)apn_buf,
          sizeof(ds_umts_pdp_profile_ptr->context.apn));
  /*lint +e534*/
  DS_UMTS_PDP_TASKFREE_NON_L4();
  
  return DS_UMTS_PDP_SUCCESS;
}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_CONTEXT_INFO_ADDRESS_INTERNAL

DESCRIPTION
  This function updates the pdp address for the profile specified with the
  value passed. The address can be updated ONLY for a valid context as it 
  is part of the context definition
  It also updates the cache table with the new pdp address value.

PARAMETERS
  profile_number  : profile number
  pdp_address     : ip address

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  A valid context must have been defined for the profile number already

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file
  DS_UMTS_PDP_CONTEXT_NOT_DEFINED  : valid context not defined for profile

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_set_pdp_profile_context_info_address_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  ds_umts_pdp_addr_type     pdp_address
)
{
  int        fs_handle;
  boolean               context_valid;
  ds_umts_pdp_profile_status_etype status;
  ds_umts_pdp_profile_type *ds_umts_pdp_profile_ptr;
  boolean               read_only_flag;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  /*-------------------------------------------------------------------
    Check if we have a valid context defined first. THe init flag and
    profile numbers are checked in ds_umts_get_pdp_profile_context_info_is_valid
  -------------------------------------------------------------------*/
  status = ds_umts_get_pdp_profile_context_info_is_valid_per_subs(
              profile_number,
              subs_id,
              &context_valid);

  if(status != DS_UMTS_PDP_SUCCESS )
  {
    return status;
  }
  
  if(!context_valid )
  {
    DS_3GPP_MSG3_HIGH( "Ctx not def.PR :%d Addr upd fail",
              profile_number,0,0);
    return DS_UMTS_PDP_CONTEXT_NOT_DEFINED;
  }

  ds_umts_pdp_profile_ptr = 
    DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);

 if(DS_UMTS_PDP_PROFILE_IS_PERSISTENT(ds_umts_pdp_profile_ptr)) 
  {
    /*-------------------------------------------------------------------
        Init the fully qualified profile file name to pdp_profiles directory
    -------------------------------------------------------------------*/
    if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
      return DS_UMTS_PDP_FAIL;
  
    /*-------------------------------------------------------------------
      Open the file for write 
    -------------------------------------------------------------------*/
    DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ", 
                            ds_umts_pdp_fully_qual_profile_file_name);
    
    fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                          O_RDWR);
    if ( fs_handle == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_open failed for %d errno=%d.", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      Set the file offset to the start of PDP address
    -------------------------------------------------------------------*/
    if (efs_lseek(fs_handle,
                  DS_UMTS_PDP_CONTEXT_ADDR_OFFSET,
                  SEEK_SET) == -1)
    {
      DS_3GPP_MSG3_ERROR( "efs_lseek failed for %d errno=%d", fs_handle, efs_errno, 0 );
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    /*-------------------------------------------------------------------
      write the pdp address
    -------------------------------------------------------------------*/
    if (ds_umts_pdp_efs_write(fs_handle,
                              (void *)&pdp_address,
                              sizeof(ds_umts_pdp_addr_type)) == FALSE)
    {
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    /*-------------------------------------------------------------------
      Close the file
    -------------------------------------------------------------------*/
    if (efs_close (fs_handle) == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_close failed for %d errno = %d", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
 }
  /*-------------------------------------------------------------------
    Update the cache table with the new pdp address value.
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_TASKLOCK_NON_L4();

  memscpy( (void*)&ds_umts_pdp_profile_ptr->context.pdp_addr,
  	   sizeof(ds_umts_pdp_addr_type),
          (void*)&pdp_address,
          sizeof(ds_umts_pdp_addr_type) );
  DS_UMTS_PDP_TASKFREE_NON_L4();
  
  return DS_UMTS_PDP_SUCCESS;
}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_CONTEXT_INFO_IPV4_ADDR_ALLOC_INTERNAL

DESCRIPTION
  This function call the internal registry function to update the ipv4 addr
  alloc setting for the profile specified with the value passed. The update 
  can be done ONLY for a valid context as it is part of the context 
  definition. It also updates the cache table with the new value.

PARAMETERS
  profile_number  : profile number
  ipv4_addr_alloc : ipv4 address allocation (mechanism)

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  A valid context must have been defined for the profile number already

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file
  DS_UMTS_PDP_CONTEXT_NOT_DEFINED  : valid context not defined for profile

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype 
ds_umts_set_pdp_profile_context_info_ipv4_addr_alloc_internal
(
  uint16                              profile_number,
  ds_umts_pdp_subs_e_type             subs_id,
  ds_umts_pdp_ipv4_addr_alloc_e_type  ipv4_addr_alloc
)
{
  ds_umts_pdp_profile_type *ds_umts_pdp_profile_ptr;
  int        fs_handle;
  boolean               context_valid;
  ds_umts_pdp_profile_status_etype status;
  boolean               read_only_flag;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  /*-------------------------------------------------------------------
    Check if we have a valid context defined first. THe init flag and
    profile numbers are checked in ds_umts_get_pdp_profile_context_info_is_valid
  -------------------------------------------------------------------*/
  status = ds_umts_get_pdp_profile_context_info_is_valid_per_subs(
              profile_number,
              subs_id,
              &context_valid);

  if(status != DS_UMTS_PDP_SUCCESS )
  {
    return status;
  }

  if(!context_valid )
  {
    DS_3GPP_MSG3_HIGH( "Ctx not def.PR :%d Ipv4 Addr Alloc upd fail",
              profile_number,0,0);
    return DS_UMTS_PDP_CONTEXT_NOT_DEFINED;
  }

  ds_umts_pdp_profile_ptr = 
    DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);

 if(DS_UMTS_PDP_PROFILE_IS_PERSISTENT(ds_umts_pdp_profile_ptr)) 
  {
    /*-------------------------------------------------------------------
        Init the fully qualified profile file name to pdp_profiles directory
    -------------------------------------------------------------------*/
    if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
      return DS_UMTS_PDP_FAIL;
  
    /*-------------------------------------------------------------------
      Open the file for write 
    -------------------------------------------------------------------*/
    DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ", 
                            ds_umts_pdp_fully_qual_profile_file_name);
    
    fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                          O_RDWR);
    if ( fs_handle == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_open failed for %d errno=%d.", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      Set the file offset to the start of IPv4 addr alloc
    -------------------------------------------------------------------*/
    if (efs_lseek(fs_handle,
                  DS_UMTS_PDP_CONTEXT_IPV4_ADDR_ALLOC_OFFSET,
                  SEEK_SET) == -1)
    {
      DS_3GPP_MSG3_ERROR( "efs_lseek failed for %d errno=%d", fs_handle, efs_errno, 0 );
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    /*-------------------------------------------------------------------
      write the ipv4 addr alloc info
    -------------------------------------------------------------------*/
    if (ds_umts_pdp_efs_write(fs_handle,
                              (void *)&ipv4_addr_alloc,
                              sizeof(ds_umts_pdp_ipv4_addr_alloc_e_type))
         == FALSE)
    {
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      Close the file
    -------------------------------------------------------------------*/
    if (efs_close (fs_handle) == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_close failed for %d errno = %d", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
 }
  /*-------------------------------------------------------------------
    Update the cache table with the new ipv4 addr alloc value.
  -------------------------------------------------------------------*/
  ds_umts_pdp_profile_ptr->context.ipv4_addr_alloc = 
      ipv4_addr_alloc;

  return DS_UMTS_PDP_SUCCESS;
}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_CONTEXT_INFO_TO_DEFAULT_INTERNAL

DESCRIPTION
  This function sets the context information to be defined for the 
  profile number specified and the following values are set for the context
  pdp type : IP
  IP address :0.0.0.0
  APN        : Null string
  data & header compression : OFF

  This function can be called by UI to define a context & hence a profile
  to default and then allow the user to override other fields of the 
  context
  If valid flag is not set (profile being created), transience_flag is set
  to FALSE (default value)
   
PARAMETERS
  profile_number  : profile number

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_set_pdp_profile_context_info_to_default_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  ds_ip_version_enum_type   ip_vsn
)
{
  ds_umts_pdp_context_type  pdp_context_data;
  ds_umts_pdp_profile_status_etype status;
  boolean  read_only_flag;
  boolean  valid_flag;
  boolean ds_umts_is_ipv6_enabled = FALSE;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  /* Save transience_flag if profile context is valid */
  status = ds_umts_get_pdp_profile_context_info_is_valid_per_subs(profile_number,
                                                                  subs_id,
                                                           &valid_flag);
  if (status != DS_UMTS_PDP_SUCCESS)
    return status;

  /*-------------------------------------------------------------------------
    If first time profile creation write default info to profile.
  -------------------------------------------------------------------------*/
  if(!valid_flag)
  {
    /*-------------------------------------------------------------------------
      Reset the profile name to ProfileXY. XY is profile number.
      Also set the number to profile_number. The function also copies
      ds_umts_pdp_default_profile to profile_data_ptr in cache. So resetting
      version, apn_bearer, transience_flag (TRUE), valid_flg (FALSE).
      ds_umts_pdp_local_profile_ptr->context.valid_flg          = FALSE;
      ds_umts_pdp_local_profile_ptr->transience_flag            = TRUE; 
      ds_umts_pdp_local_profile_ptr->context.pdp_context_number = profile_number;
      ds_umts_pdp_default_profile.version = DS_UMTS_PROFILE_VERSION;
      ds_umts_pdp_default_profile.apn_bearer = 
                          DS_UMTS_PDP_PROFILE_APN_BEARER_FLAG_ALL;
    -------------------------------------------------------------------------*/
    ds_umts_set_pdp_profile_all_data_to_default_internal(profile_number,subs_id,ip_vsn);
  }
  /*-------------------------------------------------------------------------
    Constuct default context definition 
    Header & data compression : off 
    Address : 0.0.0.0
    APN : NULL
  -------------------------------------------------------------------------*/

  memset((void*)&pdp_context_data,0x0,sizeof(ds_umts_pdp_context_type));
  pdp_context_data.valid_flg = TRUE;

#ifdef FEATURE_DATA_PS_IPV6
  ds_umts_is_ipv6_enabled = TRUE;
#endif /* FEATURE_DATA_PS_IPV6 */

  if(ip_vsn == DS_IP_V4)
  {
    /*-----------------------------------------------------------------------
      Construct default context definition of type PDP-IP
    -----------------------------------------------------------------------*/
    pdp_context_data.pdp_type = DS_UMTS_PDP_IPV4;
  }
  else if( (ip_vsn == DS_IP_V6)&& (ds_umts_is_ipv6_enabled == TRUE) )
  {
    /*-----------------------------------------------------------------------
      Construct default context definition of type PDP-IPV6
    -----------------------------------------------------------------------*/
    pdp_context_data.pdp_type = DS_UMTS_PDP_IPV6;
  }
  else if( (ip_vsn == DS_IP_V4V6) && (ds_umts_is_ipv6_enabled == TRUE) )
  {
    /*-----------------------------------------------------------------------
      Construct default context definition of type PDP-IPV4V6
    -----------------------------------------------------------------------*/
    pdp_context_data.pdp_type = DS_UMTS_PDP_IPV4V6;
  }
  else
  {
    DS_3GPP_MSG3_ERROR("Wrong IP version:%d",ip_vsn,0,0);
    return DS_UMTS_PDP_FAIL;
  }

  pdp_context_data.pdp_addr.ip_vsn = ip_vsn;
  pdp_context_data.pdp_context_number = profile_number;

  /*-------------------------------------------------------------------
    Call the routine to set this context 
  -------------------------------------------------------------------*/
  status = ds_umts_set_pdp_profile_context_info(
                                              profile_number,
                                              &pdp_context_data);

  return status;
}

/*===========================================================================
FUNCTION DS_UMTS_RESET_PDP_PROFILE_CONTEXT_INFO_INTERNAL

DESCRIPTION
  This function resets the context information to be undefined for the 
  profile number specified. It does not over-write the values of
  other parameter sets (qos, auth etc).
  It also resets the context information to be undefined in the cache table.

PARAMETERS
  profile_number  : profile number

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file

SIDE EFFECTS 
 The context definition is reset, but all other parameter sets are left
 as is. Auth, qos, dns address values are not changed.
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_reset_pdp_profile_context_info_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id
)
{
  ds_umts_pdp_context_type  pdp_context_data;
  ds_umts_pdp_profile_type *ds_umts_pdp_profile_ptr;
  boolean               read_only_flag;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 
  
  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  /*-------------------------------------------------------------------------
    Construct a context definition with valid flag set to FALSE
    Header & data compression : off 
    Address : 0.0.0.0
    APN : NULL
  -------------------------------------------------------------------------*/
  memset((void*)&pdp_context_data,0x0,sizeof(ds_umts_pdp_context_type));
  pdp_context_data.valid_flg = FALSE;

#ifdef FEATURE_DATA_PS_IPV6
  pdp_context_data.pdp_type  = DS_UMTS_PDP_IPV4V6;
#else
  pdp_context_data.pdp_type  = DS_UMTS_PDP_IPV4;
#endif /* FEATURE_DATA_PS_IPV6 */

  pdp_context_data.pdp_context_number = profile_number;

  ds_umts_pdp_profile_ptr = DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);

  /* check whether it is persistant profile  */
  if(DS_UMTS_PDP_PROFILE_IS_PERSISTENT(ds_umts_pdp_profile_ptr)) 
  {

    /*-------------------------------------------------------------------
        Init the fully qualified profile file name to pdp_profiles directory
    -------------------------------------------------------------------*/
    if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
      return DS_UMTS_PDP_FAIL;
      
    /*-----------------------------------------------------------------------
      Delete from EFS. since profile is persistent it must be present in efs.
    -----------------------------------------------------------------------*/
    DS_3GPP_MSG3_HIGH("Profile %d file is present in EFS delete it", profile_number, 0, 0);
    /*-----------------------------------------------------------------------
      Delete the file from EFS.
    -----------------------------------------------------------------------*/
    ds_umts_pdp_delete_file((char*)ds_umts_pdp_fully_qual_profile_file_name);	  
  }
  /*-------------------------------------------------------------------
   Reset the context information in the cache table
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_TASKLOCK_NON_L4();
  ds_umts_pdp_profile_ptr->transience_flag = TRUE;
  memscpy( (void*)&ds_umts_pdp_profile_ptr->context,
  	   sizeof(ds_umts_pdp_context_type),
          (void*)&pdp_context_data,
          sizeof(ds_umts_pdp_context_type) );
  DS_UMTS_PDP_TASKFREE_NON_L4();
  return DS_UMTS_PDP_SUCCESS;

}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_AUTH_INFO_INTERNAL

DESCRIPTION
  This function updates the authentication information with the values passed.
  The auth parameters are updated with the values passed even if a valid 
  context defintion is not present. 
  It also updates the cache table with the passed authentication information.

PARAMETERS
  profile_number  : profile number
  pdp_auth_data   : pointer to authentication information struct

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype  ds_umts_set_pdp_profile_auth_info_internal
(
  uint16                        profile_number,
  ds_umts_pdp_subs_e_type       subs_id,
  const ds_umts_pdp_auth_type  *pdp_auth_data
)
{
  int        fs_handle;
  ds_umts_pdp_profile_type *ds_umts_pdp_profile_ptr;
  boolean               read_only_flag;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 
  
  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  ds_umts_pdp_profile_ptr = 
    DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);

  
 if(DS_UMTS_PDP_PROFILE_IS_PERSISTENT(ds_umts_pdp_profile_ptr)) 
  {
    /*-------------------------------------------------------------------
        Init the fully qualified profile file name to pdp_profiles directory
    -------------------------------------------------------------------*/
    if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
      return DS_UMTS_PDP_FAIL;
        
    /*-------------------------------------------------------------------
      Open the file for write 
    -------------------------------------------------------------------*/
    DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ", 
                            ds_umts_pdp_fully_qual_profile_file_name);
    
    fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                          O_RDWR);
    if ( fs_handle == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_open failed for %d errno=%d.", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      Set the file offset to the start of auth info
    -------------------------------------------------------------------*/
    if (efs_lseek(fs_handle,
                  DS_UMTS_PDP_PROFILE_AUTH_OFFSET,
                  SEEK_SET) == -1)
    {
      DS_3GPP_MSG3_ERROR( "efs_lseek failed for %d errno=%d", fs_handle, efs_errno, 0 );
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    /*-------------------------------------------------------------------
      write the auth info
    -------------------------------------------------------------------*/
    if (ds_umts_pdp_efs_write(fs_handle,
                              (void *)pdp_auth_data,
                              sizeof(ds_umts_pdp_auth_type)) == FALSE)
    {
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    
    /*-------------------------------------------------------------------
      Close the file
    -------------------------------------------------------------------*/
    if (efs_close (fs_handle) == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_close failed for %d errno = %d", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
 }
  /*-------------------------------------------------------------------
    Update the cache table with the authentication information.
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_TASKLOCK_NON_L4();
  memscpy( (void*)&ds_umts_pdp_profile_ptr->auth,
  	   sizeof(ds_umts_pdp_auth_type),
           (void*)pdp_auth_data,
           sizeof(ds_umts_pdp_auth_type) );
  DS_UMTS_PDP_TASKFREE_NON_L4();
  return DS_UMTS_PDP_SUCCESS;

}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_AUTH_INFO_TYPE_INTERNAL

DESCRIPTION
  This function updates the authentication type field of authentication 
  information with the value passed.
  The auth parameters are updated with the values passed even if a valid 
  context defintion is not present. 
  It also updates the cache table with the authentication type value.

PARAMETERS
  profile_number  : profile number
  auth_type       : Authentication type

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_set_pdp_profile_auth_info_type_internal
(
  uint16                     profile_number,
  ds_umts_pdp_subs_e_type    subs_id,
  ds_umts_pdp_auth_enum_type auth_type
)
{
  int        fs_handle;
  ds_umts_pdp_profile_type *ds_umts_pdp_profile_ptr;
  boolean               read_only_flag;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 
  
  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  ds_umts_pdp_profile_ptr = 
    DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);

  if(DS_UMTS_PDP_PROFILE_IS_PERSISTENT(ds_umts_pdp_profile_ptr)) 
  {
    /*-------------------------------------------------------------------
        Init the fully qualified profile file name to pdp_profiles directory
    -------------------------------------------------------------------*/
    if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
      return DS_UMTS_PDP_FAIL;
  
    /*-------------------------------------------------------------------
      Open the file for write 
    -------------------------------------------------------------------*/
    DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ", 
                            ds_umts_pdp_fully_qual_profile_file_name);
    
    fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                          O_RDWR);
    if ( fs_handle == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_open failed for %d errno=%d.", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      Set the file offset to the start of auth info
    -------------------------------------------------------------------*/
    if (efs_lseek(fs_handle,
                  DS_UMTS_PDP_AUTH_TYPE_OFFSET,
                  SEEK_SET) == -1)
    {
      DS_3GPP_MSG3_ERROR( "efs_lseek failed for %d errno=%d", fs_handle, efs_errno, 0 );
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    /*-------------------------------------------------------------------
      write the auth type info
    -------------------------------------------------------------------*/
    if (ds_umts_pdp_efs_write(fs_handle,
                              (void *)&auth_type,
                              sizeof(ds_umts_pdp_auth_enum_type)) == FALSE)
    {
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    
    /*-------------------------------------------------------------------
      Close the file
    -------------------------------------------------------------------*/
    if (efs_close (fs_handle) == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_close failed for %d errno = %d", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  }
  /*-------------------------------------------------------------------
    Update the cache table with the authentication type value passed.
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_TASKLOCK_NON_L4();
  ds_umts_pdp_profile_ptr->auth.auth_type = auth_type;
  DS_UMTS_PDP_TASKFREE_NON_L4();
  
  return DS_UMTS_PDP_SUCCESS;
}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_AUTH_INFO_USERNAME_INTERNAL

DESCRIPTION
  This function updates the username field of authentication information 
  with the value passed.
  The auth parameters are updated with the values passed even if a valid 
  context defintion is not present. 
  It also updates the cache table with the value passed.

PARAMETERS
  profile_number  : profile number
  usename_buf     : pointer to the username string
  username_buf_len: length of the username string

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file
  DS_UMTS_PDP_INPUT_BUFFER_LEN_INVALID : The length of username string is too long

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_set_pdp_profile_auth_info_username_internal
(
  uint16                     profile_number,
  ds_umts_pdp_subs_e_type    subs_id,
  const byte                 *usernme_buf,
  byte                       username_buf_len
)
{
  int        fs_handle;
  char                  asciz=0x0;
  ds_umts_pdp_profile_type *ds_umts_pdp_profile_ptr;
  boolean               read_only_flag;

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 
  
  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  /*-------------------------------------------------------------------
    Check if the input buffer (username) is of valid length
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_IN_BUFF_LEN(username_buf_len,DS_UMTS_MAX_QCPDP_STRING_LEN);

  ds_umts_pdp_profile_ptr = 
    DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);

 if(DS_UMTS_PDP_PROFILE_IS_PERSISTENT(ds_umts_pdp_profile_ptr)) 
  {
    /*-------------------------------------------------------------------
        Init the fully qualified profile file name to pdp_profiles directory
    -------------------------------------------------------------------*/
    if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
      return DS_UMTS_PDP_FAIL;
  
    /*-------------------------------------------------------------------
      Open the file for write 
    -------------------------------------------------------------------*/
    DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ", 
                            ds_umts_pdp_fully_qual_profile_file_name);
    
    fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                          O_RDWR);
    if ( fs_handle == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_open failed for %d errno=%d.", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      Set the file offset to the start of auth info
    -------------------------------------------------------------------*/
    if (efs_lseek(fs_handle,
                  DS_UMTS_PDP_AUTH_USERNAME_OFFSET,
                  SEEK_SET) == -1)
    {
      DS_3GPP_MSG3_ERROR( "efs_lseek failed for %d errno=%d", fs_handle, efs_errno, 0 );
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    /*-------------------------------------------------------------------
      write the username 
    -------------------------------------------------------------------*/
    if (ds_umts_pdp_efs_write(fs_handle,
                              (void *)usernme_buf,
                              username_buf_len) == FALSE)
    {
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*----------------------------------------------------------------------
      Terminate the string wigh ASCIZ .. so that reading is easy with 
      strlcpy.
    -----------------------------------------------------------------------*/
    if (ds_umts_pdp_efs_write(fs_handle,
                              (void *)&asciz,
                              sizeof(char)) == FALSE)
    {
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    
  
    /*-------------------------------------------------------------------
      Close the file
    -------------------------------------------------------------------*/
    if (efs_close (fs_handle) == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_close failed for %d errno = %d", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  }
  /*-------------------------------------------------------------------
    Update the cache table with the username information passed.
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_TASKLOCK_NON_L4();

  /*-------------------------------------------------------------------
    Reset the previous username before updating new username.
  -------------------------------------------------------------------*/
  memset((void *)ds_umts_pdp_profile_ptr->auth.username, 
         0, 
         DS_UMTS_MAX_QCPDP_STRING_LEN+1);

  /*lint -e534 ignoring return code from function*/
  strlcpy( (char *)ds_umts_pdp_profile_ptr->auth.username,
          (char *) usernme_buf,
          sizeof(ds_umts_pdp_profile_ptr->auth.username));
  /*lint +e534*/
  DS_UMTS_PDP_TASKFREE_NON_L4();
  
  return DS_UMTS_PDP_SUCCESS;

}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_AUTH_INFO_PASSWORD_INTERNAL

DESCRIPTION
  This function updates the password field of authentication information 
  with the value passed.
  The auth parameters are updated with the values passed even if a valid 
  context defintion is not present. 
  It also updates the cache table with the value passed.

PARAMETERS
  profile_number  : profile number
  password_buf    : pointer to the password string
  password_buf_len: length of the password string

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file
  DS_UMTS_PDP_INPUT_BUFFER_LEN_INVALID : The length of password string is too long

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_set_pdp_profile_auth_info_password_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  const byte               *password_buf,
  byte                      password_buf_len
)
{
  int        fs_handle;
  char                  asciz=0x0;
  ds_umts_pdp_profile_type *ds_umts_pdp_profile_ptr;
  boolean               read_only_flag;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 
  
  
  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  /*-------------------------------------------------------------------
    Check if the input buffer (username) is of valid length
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_IN_BUFF_LEN(password_buf_len,DS_UMTS_MAX_QCPDP_STRING_LEN);

  ds_umts_pdp_profile_ptr = 
    DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);

  
  if(DS_UMTS_PDP_PROFILE_IS_PERSISTENT(ds_umts_pdp_profile_ptr)) 
  {
    /*-------------------------------------------------------------------
        Init the fully qualified profile file name to pdp_profiles directory
    -------------------------------------------------------------------*/
    if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
      return DS_UMTS_PDP_FAIL;
        
    /*-------------------------------------------------------------------
      Open the file for write 
    -------------------------------------------------------------------*/
    DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ",
                            ds_umts_pdp_fully_qual_profile_file_name);
    
    fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                          O_RDWR);
    if ( fs_handle == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_open failed for %d errno=%d.", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      Set the file offset to the start of auth info
    -------------------------------------------------------------------*/
    if (efs_lseek(fs_handle,
                  DS_UMTS_PDP_AUTH_PASSWORD_OFFSET,
                  SEEK_SET) == -1)
    {
      DS_3GPP_MSG3_ERROR( "efs_lseek failed for %d errno=%d", fs_handle, efs_errno, 0 );
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    /*-------------------------------------------------------------------
      write the password 
    -------------------------------------------------------------------*/
    if (ds_umts_pdp_efs_write(fs_handle,
                              (void *)password_buf,
                              password_buf_len) == FALSE)
    {
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    
    /*----------------------------------------------------------------------
      Terminate the string wigh ASCIZ .. so that reading is easy with 
      strlcpy.
    -----------------------------------------------------------------------*/
    if (ds_umts_pdp_efs_write(fs_handle,
                              (void *)&asciz,
                              sizeof(char)) == FALSE)
    {
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      Close the file
    -------------------------------------------------------------------*/
    if (efs_close (fs_handle) == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_close failed for %d errno = %d", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  }
  /*-------------------------------------------------------------------
    Update the cache table with the password passed.
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_TASKLOCK_NON_L4();

  /*-------------------------------------------------------------------
    Reset the previous password before updating new password.
  -------------------------------------------------------------------*/
  memset((void *)ds_umts_pdp_profile_ptr->auth.password, 
         0, 
         DS_UMTS_MAX_QCPDP_STRING_LEN+1);

  /*lint -e534 ignoring return code from function*/
  strlcpy( (char *) ds_umts_pdp_profile_ptr->auth.password,
          (char *) password_buf,
      sizeof(ds_umts_pdp_profile_ptr->auth.password));
  /*lint +e534*/
  DS_UMTS_PDP_TASKFREE_NON_L4();
  
  return DS_UMTS_PDP_SUCCESS;
}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_AUTH_INFO_TO_DEFAULT_INTERNAL

DESCRIPTION
  This function resets the authentication information for the profile 
  specified. Authentication is set to "None" 
  The auth parameters are updated even if a valid context defintion 
  is not present. 

PARAMETERS
  profile_number  : profile number

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_set_pdp_profile_auth_info_to_default_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id
)
{
  ds_umts_pdp_auth_type pdp_auth_info;
  boolean               read_only_flag;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  /*-------------------------------------------------------------------
    Construct the default Auth info structure
    Auth type : None
    username,password : NULL
    Memset to 0x0 would set the above values 
  -------------------------------------------------------------------*/
  memset((void*) &pdp_auth_info,0x0,sizeof(ds_umts_pdp_auth_type) );

  /*-------------------------------------------------------------------
    Call the routine ds_umts_set_pdp_profile_auth_info to set this
   auth information.  
  -------------------------------------------------------------------*/
  return ds_umts_set_pdp_profile_auth_info( 
                                           profile_number,
                                           &pdp_auth_info);
}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_UMTS_QOS_REQ_INFO_INTERNAL

DESCRIPTION
  This function updates the UMTS "requested" qos parameters with the values
  passed. 
  The qos parameters are updated with the values passed even if a valid 
  context defintion is not present. 

PARAMETERS
  profile_number  : profile number
  pdp_umts_qos_data : the values to be updated with 

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  The "valid_flg" in the parameter passed SHOULD be set to TRUE

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file
  DS_UMTS_PDP_PARMS_VALID_FLAG_NOT_SET : Valid flag for the parameters is not TRUE

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_set_pdp_profile_umts_qos_req_info_internal
(
  uint16                       profile_number,
  ds_umts_pdp_subs_e_type      subs_id,
  const ds_umts_umts_qos_params_type *pdp_umts_qos_data
)
{
  boolean               read_only_flag;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  /*-------------------------------------------------------------------
    Check if the valid flag is set in the input parameters
  -------------------------------------------------------------------*/
  if( ! pdp_umts_qos_data->valid_flg )
  {
    DS_3GPP_MSG3_HIGH("Valid flag for UMTS QOS not set PR:%d",
             profile_number,0,0);
    return DS_UMTS_PDP_PARMS_VALID_FLAG_NOT_SET;
  }

  /*-------------------------------------------------------------------
    Call the local routine to update the QOS params. It performs a 
    check on the init flag and the profile number
  -------------------------------------------------------------------*/
  return ds_umts_update_pdp_profile_umts_qos_info(
            profile_number,
            subs_id,
            DS_UMTS_QOS_REQ_TYPE,
            pdp_umts_qos_data);
}

/*===========================================================================
FUNCTION DS_UMTS_RESET_PDP_PROFILE_UMTS_QOS_REQ_INFO_INTERNAL

DESCRIPTION
  This function updates the UMTS "requested" qos parameters for the 
  profile indicated. The parameters are updated to be subscribed values
  and the "valid_flg" is set to FALSE indicating that the values are not
  valid.
  The qos parameters are updated even if a valid context defintion 
  is not present. 

PARAMETERS 
  profile_number  : profile number

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file

SIDE EFFECTS 
  sets the umts qos (requested) information to be undefined and all parameters
  are set to subscribed values
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_reset_pdp_profile_umts_qos_req_info_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id
)
{
  ds_umts_umts_qos_params_type pdp_umts_qos_info;
  boolean               read_only_flag;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  /*-------------------------------------------------------------------------
    Constuct the UMTS QOS info : All fields set to subscribed & valid_flg
    set to FALSE
    writing 0x0 to the entire structure sets the above
  -------------------------------------------------------------------------*/
  memset((void*)&pdp_umts_qos_info,0x0,sizeof(ds_umts_umts_qos_params_type) );

  /*-------------------------------------------------------------------------
    Call the local routine to update the QOS params.It performs a 
    check on the init flag and the profile number
  -------------------------------------------------------------------------*/
  return ds_umts_update_pdp_profile_umts_qos_info(
            profile_number,
            subs_id,
            DS_UMTS_QOS_REQ_TYPE,
            &pdp_umts_qos_info);

}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_UMTS_QOS_MIN_INFO_INTERNAL

DESCRIPTION
  This function updates the UMTS "minimum" qos parameters with the values
  passed. 
  The qos parameters are updated with the values passed even if a valid 
  context defintion is not present. 

PARAMETERS
  profile_number  : profile number
  pdp_umts_qos_data : the values to be updated with 

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  The "valid_flg" in the parameter passed SHOULD be set to TRUE

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file
  DS_UMTS_PDP_PARMS_VALID_FLAG_NOT_SET : Valid flag for the parameters is not TRUE

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_set_pdp_profile_umts_qos_min_info_internal
(
  uint16                              profile_number,
  ds_umts_pdp_subs_e_type             subs_id,
  const ds_umts_umts_qos_params_type *pdp_umts_qos_data
)
{
  boolean               read_only_flag;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  /*-------------------------------------------------------------------
    Check if the valid flag is set in the input parameters
  -------------------------------------------------------------------*/
  if( ! pdp_umts_qos_data->valid_flg )
  {
    DS_3GPP_MSG3_HIGH("Valid flag for UMTS QOS not set PR:%d",
      profile_number,0,0);
    return DS_UMTS_PDP_PARMS_VALID_FLAG_NOT_SET;
  }

  /*-------------------------------------------------------------------
    Call the local routine to update the QOS params. It performs a 
    check on the init flag and the profile number
  -------------------------------------------------------------------*/
  return ds_umts_update_pdp_profile_umts_qos_info(
                                         profile_number,
                                         subs_id,
                                         DS_UMTS_QOS_MIN_TYPE,
                                         pdp_umts_qos_data);
}

/*===========================================================================
FUNCTION DS_UMTS_RESET_PDP_PROFILE_UMTS_QOS_MIN_INFO_INTERNAL
DESCRIPTION
  This function updates the UMTS "minimum" qos parameters for the 
  profile indicated. The parameters are updated to be subscribed values
  and the "valid_flg" is set to FALSE indicating that the values are not
  valid.
  The qos parameters are updated even if a valid context defintion 
  is not present. 

PARAMETERS 
  profile_number  : profile number

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file

SIDE EFFECTS 
  sets the umts qos (minimum) information to be undefined and all parameters
  are set to subscribed values
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_reset_pdp_profile_umts_qos_min_info_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id
)
{
  ds_umts_umts_qos_params_type pdp_umts_qos_info;
  boolean               read_only_flag;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  /*-------------------------------------------------------------------------
    Constuct the UMTS QOS info : All fields set to subscribed & valid_flg
    set to FALSE
    writing 0x0 to the entire structure sets the above
  -------------------------------------------------------------------------*/
  memset((void*)&pdp_umts_qos_info,0x0,sizeof(ds_umts_umts_qos_params_type) );

  /*-------------------------------------------------------------------------
    Call the local routine to update the QOS params.It performs a 
    check on the init flag and the profile number
  -------------------------------------------------------------------------*/
  return ds_umts_update_pdp_profile_umts_qos_info(
                                         profile_number,
                                         subs_id,
                                         DS_UMTS_QOS_MIN_TYPE,
                                         &pdp_umts_qos_info);

}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_SRC_STAT_DESC_REQ_INFO_INTERNAL

DESCRIPTION
  This function calls the internal registry function to update the UMTS
  "requested" src_stat_desc info with the values passed. 

PARAMETERS
  profile_number  : profile number
  src_stat_desc   : the values to be updated with 

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file
  DS_UMTS_PDP_PARMS_VALID_FLAG_NOT_SET : Valid flag for the parameters is not TRUE

SIDE EFFECTS 
  none
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_set_pdp_profile_src_stat_desc_req_info_internal
(
  uint16                       profile_number,
  ds_umts_pdp_subs_e_type      subs_id,
  const uint32                 src_stat_desc
)
{
  boolean               read_only_flag;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  /*-------------------------------------------------------------------
    Call the local routine to update the QOS params. It performs a 
    check on the init flag and the profile number
  -------------------------------------------------------------------*/
  return ds_umts_update_pdp_profile_src_stat_desc_info(
            profile_number,
            subs_id,
            DS_UMTS_QOS_REQ_TYPE,
            src_stat_desc,
            (uint32)DS_UMTS_PDP_PROFILE_SRC_STAT_DESC_REQ_OFFSET);
}

/*===========================================================================
FUNCTION DS_UMTS_RESET_PDP_PROFILE_SRC_STAT_DESC_REQ_INFO_INTERNAL

DESCRIPTION
  This function calls the internal registry function to update the UMTS
  "requested" src_stat_desc info with the values passed. 

PARAMETERS
  profile_number  : profile number

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file
  DS_UMTS_PDP_PARMS_VALID_FLAG_NOT_SET : Valid flag for the parameters is not TRUE

SIDE EFFECTS 
  sets the umts qos (requested) information to be undefined and all parameters
  are set to subscribed values
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_reset_pdp_profile_src_stat_desc_req_info_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id
)
{
  const uint32          src_stat_desc = 0;
  boolean               read_only_flag;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  /*-------------------------------------------------------------------------
    Call the local routine to update the QOS params.It performs a 
    check on the init flag and the profile number
  -------------------------------------------------------------------------*/
  return ds_umts_update_pdp_profile_src_stat_desc_info(
            profile_number,
            subs_id,
            DS_UMTS_QOS_REQ_TYPE,
            src_stat_desc,
            (uint32)DS_UMTS_PDP_PROFILE_SRC_STAT_DESC_REQ_OFFSET);
}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_SRC_STAT_DESC_MIN_INFO_INTERNAL

DESCRIPTION
  This function calls the internal registry function to update the UMTS
  "requested" src_stat_desc info with the values passed. 

PARAMETERS
  profile_number  : profile number
  src_stat_desc   : the values to be updated with 

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file
  DS_UMTS_PDP_PARMS_VALID_FLAG_NOT_SET : Valid flag for the parameters is not TRUE

SIDE EFFECTS 
  none

===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_set_pdp_profile_src_stat_desc_min_info_internal
(
  uint16                       profile_number,
  ds_umts_pdp_subs_e_type      subs_id,
  const uint32                 src_stat_desc
)
{
  boolean               read_only_flag;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  /*-------------------------------------------------------------------
    Call the local routine to update the QOS params. It performs a 
    check on the init flag and the profile number
  -------------------------------------------------------------------*/
  return ds_umts_update_pdp_profile_src_stat_desc_info(
            profile_number,
            subs_id,
            DS_UMTS_QOS_MIN_TYPE,
            src_stat_desc,
            (uint32)DS_UMTS_PDP_PROFILE_SRC_STAT_DESC_MIN_OFFSET);
}

/*===========================================================================
FUNCTION DS_UMTS_RESET_PDP_PROFILE_SRC_STAT_DESC_MIN_INFO_INTERNAL

DESCRIPTION
  This function calls the internal registry function to update the UMTS
  "requested" src_stat_desc info with the values passed. 

PARAMETERS
  profile_number  : profile number

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file
  DS_UMTS_PDP_PARMS_VALID_FLAG_NOT_SET : Valid flag for the parameters is not TRUE

SIDE EFFECTS 
  sets the umts qos (requested) information to be undefined and all parameters
  are set to subscribed values
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_reset_pdp_profile_src_stat_desc_min_info_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id
)
{
  const uint32          src_stat_desc = 0;
  boolean               read_only_flag;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  /*-------------------------------------------------------------------------
    Call the local routine to update the QOS params.It performs a 
    check on the init flag and the profile number
  -------------------------------------------------------------------------*/
    return ds_umts_update_pdp_profile_src_stat_desc_info(
              profile_number,
              subs_id,
              DS_UMTS_QOS_MIN_TYPE,
              src_stat_desc,
              (uint32)DS_UMTS_PDP_PROFILE_SRC_STAT_DESC_MIN_OFFSET);
}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_GPRS_QOS_REQ_INFO_INTERNAL

DESCRIPTION
  This function updates the GPRS "requested" qos parameters with the values
  passed. 
  The qos parameters are updated with the values passed even if a valid 
  context defintion is not present. 

PARAMETERS
  profile_number  : profile number
  pdp_gprs_qos_req_data : the values to be updated with 

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  The "valid_flg" in the parameter passed SHOULD be set to TRUE

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file
  DS_UMTS_PDP_PARMS_VALID_FLAG_NOT_SET : Valid flag for the parameters is not TRUE

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_set_pdp_profile_gprs_qos_req_info_internal
(
  uint16                       profile_number,
  ds_umts_pdp_subs_e_type      subs_id,
  const ds_umts_gprs_qos_params_type *pdp_gprs_qos_req_data
)
{
  boolean               read_only_flag;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  /*-------------------------------------------------------------------
    Check if the valid flag is set in the input parameters
  -------------------------------------------------------------------*/
  if( ! pdp_gprs_qos_req_data->valid_flg )
  {
    DS_3GPP_MSG3_HIGH("Valid flg -GPRS REQ QOS not set :PR:%d ",
      profile_number,0,0);
    return DS_UMTS_PDP_PARMS_VALID_FLAG_NOT_SET;
  }

  /*-------------------------------------------------------------------
    Call the local routine to update the QOS params.It performs a 
    check on the init flag and the profile number
  -------------------------------------------------------------------*/
  return ds_umts_update_pdp_profile_gprs_qos_info(
                                         profile_number,
                                         subs_id,
                                         DS_UMTS_GPRS_QOS_REQ_TYPE,
                                         pdp_gprs_qos_req_data);
}

/*===========================================================================
FUNCTION DS_UMTS_RESET_PDP_PROFILE_GPRS_QOS_REQ_INFO_INTERNAL
DESCRIPTION
  This function updates the GPRS "requested" qos parameters for the 
  profile indicated. The parameters are updated to be subscribed values
  and the "valid_flg" is set to FALSE indicating that the values are not
  valid.
  The qos parameters are updated even if a valid context defintion 
  is not present. 

PARAMETERS 
  profile_number  : profile number

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file

SIDE EFFECTS 
  sets the gprs requested qos information to be undefined and all parameters 
  are set to subscribed values
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_reset_pdp_profile_gprs_qos_req_info_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id
)
{
  ds_umts_gprs_qos_params_type pdp_gprs_qos_info;
  boolean               read_only_flag;
  ds_umts_pdp_profile_status_etype status;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  /*-------------------------------------------------------------------------
    Constuct the GPRS QOS info : All fields set to subscribed & valid_flg
    set to FALSE
    writing 0x0 to the entire structure sets the above
  -------------------------------------------------------------------------*/
  memset((void*)&pdp_gprs_qos_info,0x0,sizeof(ds_umts_gprs_qos_params_type) );

  /*-------------------------------------------------------------------------
    Call the local routine to update the QOS params.It performs a 
    check on the init flag and the profile number
  -------------------------------------------------------------------------*/
  status = ds_umts_update_pdp_profile_gprs_qos_info(
                                         profile_number,
                                         subs_id,
                                         DS_UMTS_GPRS_QOS_REQ_TYPE,
                                         &pdp_gprs_qos_info);

  return status;
}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_GPRS_QOS_MIN_INFO_INTERNAL

DESCRIPTION
  This function updates the GPRS "minimum" qos parameters with the values
  passed. 
  The qos parameters are updated with the values passed even if a valid 
  context defintion is not present. 

PARAMETERS
  profile_number  : profile number
  pdp_gprs_qos_min_data : the values to be updated with 

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  The "valid_flg" in the parameter passed SHOULD be set to TRUE

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file
  DS_UMTS_PDP_PARMS_VALID_FLAG_NOT_SET : Valid flag for the parameters is not TRUE

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_set_pdp_profile_gprs_qos_min_info_internal
( 
  uint16                              profile_number,
  ds_umts_pdp_subs_e_type             subs_id,
  const ds_umts_gprs_qos_params_type *pdp_gprs_qos_min_data
)
{
  boolean               read_only_flag;
  ds_umts_pdp_profile_status_etype status;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  /*-------------------------------------------------------------------
    Check if the valid flag is set in the input parameters
  -------------------------------------------------------------------*/
  if( ! pdp_gprs_qos_min_data->valid_flg )
  {
    DS_3GPP_MSG3_HIGH("Valid flag GPRS MIN QOS not set:PR:%d",profile_number,0,0);
    return DS_UMTS_PDP_PARMS_VALID_FLAG_NOT_SET;
  }

  /*-------------------------------------------------------------------
    Call the local routine to update the QOS params.It performs a 
    check on the init flag and the profile number
  -------------------------------------------------------------------*/
  status = ds_umts_update_pdp_profile_gprs_qos_info(
                                         profile_number,
                                         subs_id,
                                         DS_UMTS_GPRS_QOS_MIN_TYPE,
                                         pdp_gprs_qos_min_data);

    return status;
}

/*===========================================================================
FUNCTION DS_UMTS_RESET_PDP_PROFILE_GPRS_QOS_MIN_INFO_INTERNAL

DESCRIPTION
  This function updates the GPRS "minimum" qos required parameters for the 
  profile indicated. The parameters are updated to be subscribed values
  and the "valid_flg" is set to FALSE indicating that the values are not
  valid.
  The qos parameters are updated even if a valid context defintion 
  is not present. 

PARAMETERS 
  profile_number  : profile number

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file

SIDE EFFECTS 
  sets the gprs minimum qos information to be undefined and all parameters 
  are set to subscribed values
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_reset_pdp_profile_gprs_qos_min_info_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id
)
{
  ds_umts_gprs_qos_params_type pdp_gprs_qos_info;
  ds_umts_pdp_profile_status_etype  status;
  boolean               read_only_flag;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  /*-------------------------------------------------------------------------
    Constuct the GPRS QOS info : All fields set to subscribed & valid_flg
    set to FALSE
    writing 0x0 to the entire structure sets the above
  -------------------------------------------------------------------------*/
  memset((void*) &pdp_gprs_qos_info,0x0,sizeof(ds_umts_gprs_qos_params_type) );

  /*-------------------------------------------------------------------------
    Call the local routine to update the QOS params.It performs a 
    check on the init flag and the profile number
  -------------------------------------------------------------------------*/
  status = ds_umts_update_pdp_profile_gprs_qos_info(
                                         profile_number,
                                         subs_id,
                                         DS_UMTS_GPRS_QOS_MIN_TYPE,
                                         &pdp_gprs_qos_info);

  return status;
}

/*===========================================================================
FUNCTION DS_3GPP_SET_PDP_PROFILE_LTE_QOS_INFO_INTERNAL

DESCRIPTION
  This function updates the LTE qos parameters with the values passed. 
  The qos parameters are updated with the values passed even if a valid 
  context defintion is not present. 

PARAMETERS
  profile_number  : profile number
  pdp_lte_qos_data : the values to be updated with 

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  The "valid_flg" in the parameter passed SHOULD be set to TRUE

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file
  DS_UMTS_PDP_PARMS_VALID_FLAG_NOT_SET : Valid flag for the parameters is not TRUE

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_3gpp_set_pdp_profile_lte_qos_info_internal
( 
  uint16                       profile_number,
  ds_umts_pdp_subs_e_type      subs_id,
  const ds_3gpp_lte_qos_params_type *pdp_lte_qos_data
)
{
#ifdef FEATURE_PROFILE_OTAP_ENHANCEMENTS
  boolean               read_only_flag;
#endif
  ds_umts_pdp_profile_status_etype status;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

#ifdef FEATURE_PROFILE_OTAP_ENHANCEMENTS
  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);
#endif

  /*-------------------------------------------------------------------
    Check if the valid flag is set in the input parameters
  -------------------------------------------------------------------*/
  if( ! pdp_lte_qos_data->valid_flg )
  {
    DS_3GPP_MSG3_HIGH("Valid flag LTE QOS not set:PR:%d",profile_number,0,0);
    return DS_UMTS_PDP_PARMS_VALID_FLAG_NOT_SET;
  }

  /*-------------------------------------------------------------------
    Call the local routine to update the QOS params.It performs a 
    check on the init flag and the profile number
  -------------------------------------------------------------------*/
  status = ds_3gpp_update_pdp_profile_lte_qos_info(
                                         profile_number,
                                         subs_id,
                                         pdp_lte_qos_data);

  return status;
}


/*===========================================================================
FUNCTION DS_3GPP_RESET_PDP_PROFILE_LTE_QOS_INFO_INTERNAL

DESCRIPTION
  This function updates the LTE qos required parameters for the 
  profile indicated. The parameters are updated to be subscribed values
  and the "valid_flg" is set to FALSE indicating that the values are not
  valid.
  The qos parameters are updated even if a valid context defintion 
  is not present. 

PARAMETERS 
  profile_number  : profile number

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file

SIDE EFFECTS 
  sets the lte qos information to be undefined and all parameters 
  are set to subscribed values
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_3gpp_reset_pdp_profile_lte_qos_info_internal
(
  uint16                              profile_number,
  ds_umts_pdp_subs_e_type             subs_id
)
{
  ds_3gpp_lte_qos_params_type pdp_lte_qos_info;
  ds_umts_pdp_profile_status_etype  status;
#ifdef FEATURE_PROFILE_OTAP_ENHANCEMENTS
  boolean               read_only_flag;
#endif
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

#ifdef FEATURE_PROFILE_OTAP_ENHANCEMENTS
  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);
#endif

  /*-------------------------------------------------------------------------
    Constuct the LTE QOS info : All fields set to subscribed & valid_flg
    set to FALSE
    writing 0x0 to the entire structure sets the above
  -------------------------------------------------------------------------*/
  memset((void*) &pdp_lte_qos_info,0x0,sizeof(ds_3gpp_lte_qos_params_type) );

  /*-------------------------------------------------------------------------
    Call the local routine to update the QOS params.It performs a 
    check on the init flag and the profile number
  -------------------------------------------------------------------------*/
  status = ds_3gpp_update_pdp_profile_lte_qos_info(
                                         profile_number,subs_id,
                                         &pdp_lte_qos_info);

  return status;
}

/*===========================================================================
FUNCTION DS_UMTS_UPDATE_PDP_PROFILE_TFT_INFO_INTERNAL

DESCRIPTION
  This function updates the TFT information with the values passed.
  It directly overwrites the current values stored in the profile file.

PARAMETERS
  profile_number  : profile number
  write_offset    : Byte offset within file to start write
  write_size      : Byte count to write
  pdp_tft_data    : pointer to the TFT data

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  
RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system

SIDE EFFECTS 
  None
===========================================================================*/
static ds_umts_pdp_profile_status_etype
  ds_umts_update_pdp_profile_tft_info_internal
(
  uint16                         profile_number,
  ds_umts_pdp_subs_e_type        subs_id,
  uint16                         write_offset,
  uint16                         write_size,
  byte                           index,
  const ds_umts_tft_params_type  *pdp_tft_data
)
{
  int        fs_handle;
  ds_umts_pdp_profile_type *ds_umts_pdp_profile_ptr;
  boolean               read_only_flag;

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 
  
  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  ds_umts_pdp_profile_ptr = 
    DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);

  if(DS_UMTS_PDP_PROFILE_IS_PERSISTENT(ds_umts_pdp_profile_ptr)) 
  {
      
    /*-------------------------------------------------------------------
        Init the fully qualified profile file name to pdp_profiles directory
    -------------------------------------------------------------------*/
    if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
      return DS_UMTS_PDP_FAIL;
        
    /*-------------------------------------------------------------------
      Open the file for write 
    -------------------------------------------------------------------*/
    DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ",
                            ds_umts_pdp_fully_qual_profile_file_name);
    
    fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                          O_RDWR);
    if ( fs_handle == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_open failed for %d errno=%d.", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      Set the file offset to the start of TFT info
    -------------------------------------------------------------------*/
    if (efs_lseek(fs_handle,
                  write_offset,
                  SEEK_SET) == -1)
    {
      DS_3GPP_MSG3_ERROR( "efs_lseek failed for %d errno=%d", fs_handle, efs_errno, 0 );
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    /*-------------------------------------------------------------------
      write the TFT info
    -------------------------------------------------------------------*/
    if (ds_umts_pdp_efs_write(fs_handle,
                              (void *)pdp_tft_data,
                              write_size) == FALSE)
    {
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    
    /*-------------------------------------------------------------------
      Close the file
    -------------------------------------------------------------------*/
    if (efs_close (fs_handle) == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_close failed for %d errno = %d", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  }
  /*-------------------------------------------------------------------
    Update the cache table with the tft information passed.
  -------------------------------------------------------------------*/
  if(index >= 2)
  {
    DS_3GPP_MSG3_ERROR("invalid index value index:%d",index,0,0);
    return DS_UMTS_PDP_FAIL;  
  }
  DS_UMTS_PDP_TASKLOCK_NON_L4();
  memscpy( (void *)(ds_umts_pdp_profile_ptr->tft + index),
  	    sizeof(ds_umts_tft_params_type),
            (void *) pdp_tft_data,
            write_size );
  DS_UMTS_PDP_TASKFREE_NON_L4();

  return DS_UMTS_PDP_SUCCESS;
}


/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_TFT_INFO_INTERNAL

DESCRIPTION
  This function updates the Traffic Flow Template (TFT) parameters with
  the values passed for the given filter identifier. 
  The TFT parameters are updated with the values passed even if a valid 
  context defintion is not present. 

PARAMETERS
  profile_number  : profile number
  filter_id       : filter identifer
  pdp_tft_data    : the values to be updated with 

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  The "valid_flg" in the parameter passed SHOULD be set to TRUE

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_INVALID_FILTER_ID    : Invalid fulter identifier
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file
  DS_UMTS_PDP_PARMS_VALID_FLAG_NOT_SET : Valid flag for the parameters is not TRUE

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype  ds_umts_set_pdp_profile_tft_info_internal
(
  uint16                      profile_number,
  ds_umts_pdp_subs_e_type     subs_id,
  byte                        filter_id,
  const ds_umts_tft_params_type     *pdp_tft_data
)
{
  byte index = filter_id - 1;
  ds_umts_pdp_profile_status_etype status;
  boolean               read_only_flag;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  /*-------------------------------------------------------------------
    Check if the input parameters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_FILTER_ID(filter_id);
  
  /*-------------------------------------------------------------------
    Call the local routine to update the TFT params. It performs a 
    check on the init flag and the profile number
  -------------------------------------------------------------------*/
  status =  ds_umts_update_pdp_profile_tft_info_internal (
                            profile_number,
                            subs_id,
                            DS_UMTS_PDP_PROFILE_TFT_FILTER_OFFSET(index),
                            sizeof(ds_umts_tft_params_type),
                            index,
                            pdp_tft_data);

  return status;
}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_TFT_ALL_INFO_INTERNAL

DESCRIPTION
  This function updates the Traffic Flow Template (TFT) parameters
  with the values passed. The data pointer is assumed to be the
  address of an array of size DS_UMTS_MAX_TFT_PARAM_SETS.
  The TFT parameters are updated with the values passed even if a valid 
  context defintion is not present. 

PARAMETERS
  profile_number  : profile number
  pdp_tft_data    : the values to be updated with;
                    array[DS_UMTS_MAX_TFT_PARAM_SETS] 

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  The "valid_flg" in the parameter passed SHOULD be set to TRUE

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file
  DS_UMTS_PDP_PARMS_VALID_FLAG_NOT_SET : Valid flag for the parameters is not TRUE

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_set_pdp_profile_tft_all_info_internal
(
  uint16                             profile_number,
  ds_umts_pdp_subs_e_type            subs_id,
  const ds_umts_tft_params_type     *pdp_tft_data
)
{
  ds_umts_pdp_profile_status_etype status;
  boolean               read_only_flag;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  /*-------------------------------------------------------------------
    Call the local routine to update the TFT params. It performs a 
    check on the init flag and the profile number
  -------------------------------------------------------------------*/
  status = ds_umts_update_pdp_profile_tft_info_internal(
            profile_number,
            subs_id,
            DS_UMTS_PDP_PROFILE_TFT_OFFSET,
            (DS_UMTS_MAX_TFT_PARAM_SETS * sizeof(ds_umts_tft_params_type)),
            0,
            pdp_tft_data);

  return status;
}

/*===========================================================================
FUNCTION DS_UMTS_RESET_PDP_PROFILE_TFT_ALL_INFO_INTERNAL

DESCRIPTION
  This function updates the Traffice Flow Template (TFT) required
  parameters for the profile indicated. The parameters are updated to
  zero values and the "valid_flg" is set to FALSE indicating that the
  values are not valid.

  The TFT parameters are updated even if a valid context defintion 
  is not present. 

PARAMETERS 
  profile_number  : profile number

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file

SIDE EFFECTS 
  sets the TFT information to be undefined and all parameters 
  are set to zero values
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_reset_pdp_profile_tft_all_info_internal
(
  uint16                              profile_number,
  ds_umts_pdp_subs_e_type             subs_id
)
{
  ds_umts_pdp_profile_status_etype result = DS_UMTS_PDP_SUCCESS;
  ds_umts_tft_params_type pdp_tft_info[DS_UMTS_MAX_TFT_PARAM_SETS];
  uint8 i;
  boolean               read_only_flag;
  ds_umts_pdp_type_enum_type pdp_type;
  ds_ip_version_enum_type  ip_version_type = DS_IP_V4;
  boolean ds_umts_is_ipv6_enabled = FALSE;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  /*-------------------------------------------------------------------------
    Constuct the TFT info : All fields set to zero & valid_flg set to FALSE
    writing 0x0 to the entire structure sets the above
  -------------------------------------------------------------------------*/
  memset((void*)&pdp_tft_info[0], 0x0, 
        ( DS_UMTS_MAX_TFT_PARAM_SETS *sizeof(ds_umts_tft_params_type)));

  /*-------------------------------------------------------------------
    Set the ip_vsn of src address appropriately
  -------------------------------------------------------------------*/
  pdp_type = DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number)->context.
             pdp_type;

#ifdef FEATURE_DATA_PS_IPV6
  ds_umts_is_ipv6_enabled = TRUE;
#endif /* FEATURE_DATA_PS_IPV6 */

  if(pdp_type == DS_UMTS_PDP_IPV4)
  {
    ip_version_type = DS_IP_V4;
  }
  else if( (pdp_type == DS_UMTS_PDP_IPV6) && (ds_umts_is_ipv6_enabled == TRUE) )
  {
    ip_version_type = DS_IP_V6;
  }
  else if( (pdp_type == DS_UMTS_PDP_IPV4V6) && (ds_umts_is_ipv6_enabled == TRUE) )
  {
    ip_version_type = DS_IP_V4V6;
  }
  else
  {
    DS_3GPP_MSG3_ERROR("Cannot set tft to default values for profile:%d, pdp_type:%d",
               profile_number,pdp_type, 0);
    return DS_UMTS_PDP_FAIL;
  }
  for (i=0; i < DS_UMTS_MAX_TFT_PARAM_SETS; i++)
  {
    pdp_tft_info[i].src_addr_mask.address.ip_vsn = ip_version_type;
  }
  /*-------------------------------------------------------------------------
    Call the local routine to update the TFT params. It performs a 
    check on the init flag and the profile number.
  -------------------------------------------------------------------------*/
  result =  ds_umts_set_pdp_profile_tft_all_info(
                                                 profile_number,
                                                 &pdp_tft_info[0]);
  return result;
}

/*===========================================================================
FUNCTION DS_UMTS_UPDATE_PDP_PROFILE_PARAMS_INFO_INTERNAL

DESCRIPTION
  This function updates the PDP profile parameters.

PARAMETERS
  profile_number        : profile number
  write_offset          : Byte offset within file to start write
  write_size            : Byte count to write
  data_ptr              : pointer to the profile params data

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  
RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system
  DS_UMTS_PDP_READ_ONLY_FLAG_SET   : Read Only flag set
SIDE EFFECTS 
  none
===========================================================================*/
static ds_umts_pdp_profile_status_etype
  ds_umts_update_pdp_profile_params_info_internal
(
  uint16                              profile_number,
  ds_umts_pdp_subs_e_type             subs_id,
  uint16                              write_offset,
  uint16                              write_size,
  const void                         *data_ptr
)
{
  int                              fs_handle;
  ds_umts_pdp_profile_status_etype status;
  boolean                          read_only_flag;
  boolean                          transience_flag;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 
  
  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);
 
  status = ds_umts_get_pdp_profile_transience_flag(profile_number,subs_id,
                                                   &transience_flag);
  if(status != DS_UMTS_PDP_SUCCESS) 
  {
    return status;
  }
  
 if(transience_flag == FALSE) 
  {
    /*-------------------------------------------------------------------
        Init the fully qualified profile file name to pdp_profiles directory
    -------------------------------------------------------------------*/
    if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
      return DS_UMTS_PDP_FAIL;
        
    /*-------------------------------------------------------------------
      Open the file for write 
    -------------------------------------------------------------------*/
    DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ", 
                            ds_umts_pdp_fully_qual_profile_file_name);
    
    fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                          O_RDWR);
    if ( fs_handle == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_open failed for %d errno=%d.", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      Set the file offset to the start of Linger Param info
    -------------------------------------------------------------------*/
    if (efs_lseek(fs_handle,
                  write_offset,
                  SEEK_SET) == -1)
    {
      ERR( "efs_lseek failed for %d errno=%d", fs_handle, efs_errno, 0 );
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    /*-------------------------------------------------------------------
      write the Linger Param info
    -------------------------------------------------------------------*/
    if (ds_umts_pdp_efs_write(fs_handle,
                              (void *)data_ptr,
                              write_size) == FALSE)
    {
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    
    /*-------------------------------------------------------------------
      Close the file
    -------------------------------------------------------------------*/
    if (efs_close (fs_handle) == -1)
    {
      ERR("efs_close failed for %d errno = %d", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  }
  return DS_UMTS_PDP_SUCCESS;

}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_LINGER_PARAMS_INFO_INTERNAL

DESCRIPTION
  This function updates the linger parameters in the profile

PARAMETERS
  profile_number     : profile number
  linger_params_data : pointer to the linger parameter data

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)
  The "valid_flg" in the parameter passed SHOULD be set to TRUE

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file
  DS_UMTS_PDP_PARMS_VALID_FLAG_NOT_SET : Valid flag for the parameters is not TRUE

SIDE EFFECTS 
  none
===========================================================================*/
ds_umts_pdp_profile_status_etype  
  ds_umts_set_pdp_profile_linger_params_info_internal
(
  uint16                           profile_number,
  ds_umts_pdp_subs_e_type          subs_id,
  const ds_umts_linger_params_type *linger_params_data
)
{
  ds_umts_pdp_profile_status_etype status;
  ds_umts_pdp_profile_type         *ds_umts_pdp_profile_ptr;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------------
    Check if the context defn is set to true : only then we allow it 
    to be set. to reset a context to undefined, the reset funs should be used
  -------------------------------------------------------------------------*/
  if( !(linger_params_data->valid_flag) )
  {
    DS_3GPP_MSG3_ERROR( "Linger paramters for profile :%d passed is not enabled", 
               profile_number, 0, 0 );
    return  DS_UMTS_PDP_PARMS_VALID_FLAG_NOT_SET ;
  }

  ds_umts_pdp_profile_ptr = 
    DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);

  /*-------------------------------------------------------------------
    Call the local routine to update the linger params. It performs a 
    check on the init flag and the profile number
  -------------------------------------------------------------------*/
  status =  ds_umts_update_pdp_profile_params_info_internal (
                            profile_number,
                            subs_id,
                            DS_UMTS_PDP_PROFILE_LINGER_PARAMS_OFFSET,
                            sizeof(ds_umts_linger_params_type),
                            (void *)linger_params_data);

  /*-------------------------------------------------------------------
    Update the cache table with the Linger param information passed.
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_TASKLOCK_NON_L4();
  memscpy( (void *)&(ds_umts_pdp_profile_ptr->iface_linger_params),
  	   sizeof(ds_umts_linger_params_type),
           (void *) linger_params_data,
           sizeof(ds_umts_linger_params_type) );
  DS_UMTS_PDP_TASKFREE_NON_L4();

  return status;
}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_DNS_ADDRESS_INFO_INTERNAL

DESCRIPTION
  This function updates the dns addresses for the profile specified with 
  the values passed
  The dns parameters are updated with the values passed even if a valid 
  context defintion is not present. 
  It also updates the cache table with the dns addresses passed.

PARAMETERS 
  profile_number  : profile number
  pds_dns_addr    : pointer to the dns addresses

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_set_pdp_profile_dns_address_info_internal
(
  uint16                                profile_number,
  ds_umts_pdp_subs_e_type               subs_id,
  const ds_umts_pdp_dns_addresses_type *pdp_dns_addr
)
{
  int        fs_handle;
  ds_umts_pdp_profile_type *ds_umts_pdp_profile_ptr;
  boolean               read_only_flag;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 
  
  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  ds_umts_pdp_profile_ptr = 
    DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);

  if(DS_UMTS_PDP_PROFILE_IS_PERSISTENT(ds_umts_pdp_profile_ptr)) 
  {
    /*-------------------------------------------------------------------
      Init the fully qualified profile file name to pdp_profiles directory
    -------------------------------------------------------------------*/
    if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
      return DS_UMTS_PDP_FAIL;
  
    /*-------------------------------------------------------------------
      Open the file for write 
    -------------------------------------------------------------------*/
    DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ", 
                            ds_umts_pdp_fully_qual_profile_file_name);
    
    fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                          O_RDWR);
    if ( fs_handle == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_open failed for %d errno=%d.", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      Set the file offset to the start of auth info
    -------------------------------------------------------------------*/
    if (efs_lseek(fs_handle,
                  DS_UMTS_PDP_PROFILE_DNS_ADDR_OFFSET,
                  SEEK_SET) == -1)
    {
      DS_3GPP_MSG3_ERROR( "efs_lseek failed for %d errno=%d", fs_handle, efs_errno, 0 );
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    /*-------------------------------------------------------------------
      write the auth info
    -------------------------------------------------------------------*/
    if (ds_umts_pdp_efs_write(fs_handle,
                              (void *)pdp_dns_addr,
                              sizeof(ds_umts_pdp_dns_addresses_type)) == FALSE)
    {
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    
    /*-------------------------------------------------------------------
      Close the file
    -------------------------------------------------------------------*/
    if (efs_close (fs_handle) == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_close failed for %d errno = %d", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  }
  /*-------------------------------------------------------------------
    Update the cache table with the dns addresses passed.
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_TASKLOCK_NON_L4();
  memscpy( (void *)&ds_umts_pdp_profile_ptr->dns_addr,
  	   sizeof(ds_umts_pdp_dns_addresses_type),
          (void *) pdp_dns_addr,
          sizeof(ds_umts_pdp_dns_addresses_type) );
  DS_UMTS_PDP_TASKFREE_NON_L4();
    return DS_UMTS_PDP_SUCCESS;
}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_DNS_ADDRESS_PRIMARY_INTERNAL

DESCRIPTION
  This function sets the primary dns address for a pdp profile
  The dns parameters are updated with the values passed even if a valid 
  context defintion is not present. 
  It also updates the cache table with the primary dns address value passed.

PARAMETERS 
  profile_number  : profile number
  primary_dns_addr: dns address

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_set_pdp_profile_dns_address_primary_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  ds_umts_pdp_addr_type     primary_dns_addr
)
{
  int        fs_handle;
  ds_umts_pdp_profile_type *ds_umts_pdp_profile_ptr;
  boolean               read_only_flag;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 
  
  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  ds_umts_pdp_profile_ptr = 
    DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);
  if(DS_UMTS_PDP_PROFILE_IS_PERSISTENT(ds_umts_pdp_profile_ptr)) 
  {
    /*-------------------------------------------------------------------
      Init the fully qualified profile file name to pdp_profiles directory
    -------------------------------------------------------------------*/
    if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
      return DS_UMTS_PDP_FAIL;
        
    /*-------------------------------------------------------------------
      Open the file for write 
    -------------------------------------------------------------------*/
    DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ", 
                            ds_umts_pdp_fully_qual_profile_file_name);
    
    fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                          O_RDWR);
    if ( fs_handle == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_open failed for %d errno=%d.", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      Set the file offset to the start of auth info
    -------------------------------------------------------------------*/
    if (efs_lseek(fs_handle,
                  DS_UMTS_PDP_DNS_PRIMARY_OFFSET,
                  SEEK_SET) == -1)
    {
      DS_3GPP_MSG3_ERROR( "efs_lseek failed for %d errno=%d", fs_handle, efs_errno, 0 );
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    /*-------------------------------------------------------------------
      write the auth info
    -------------------------------------------------------------------*/
    if (ds_umts_pdp_efs_write(fs_handle,
                              (void *)&primary_dns_addr,
                              sizeof(ds_umts_pdp_addr_type)) == FALSE)
    {
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      Close the file
    -------------------------------------------------------------------*/
    if (efs_close (fs_handle) == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_close failed for %d errno = %d", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  }
  /*-------------------------------------------------------------------
    Update the cache table with the primary dns address value passed.
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_TASKLOCK_NON_L4();
  memscpy( (void*)&ds_umts_pdp_profile_ptr->dns_addr.primary_dns_addr,
  	    sizeof(ds_umts_pdp_addr_type),
           (void*)&primary_dns_addr,
           sizeof(ds_umts_pdp_addr_type) );
  DS_UMTS_PDP_TASKFREE_NON_L4();

  return DS_UMTS_PDP_SUCCESS;
}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_DNS_ADDRESS_SECONDARY_INTERNAL

DESCRIPTION
  This function sets the secondary dns address for a pdp profile
  The dns parameters are updated with the values passed even if a valid 
  context defintion is not present. 
  It also updates the cache table with the secondary dns address value passed.

PARAMETERS 
  profile_number  : profile number
  secondary_dns_addr: dns address

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_set_pdp_profile_dns_address_secondary_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id,
  ds_umts_pdp_addr_type     secondary_dns_addr
)
{
  int        fs_handle;
  ds_umts_pdp_profile_type *ds_umts_pdp_profile_ptr;
  boolean               read_only_flag;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 
  
  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  ds_umts_pdp_profile_ptr = 
    DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);

  if(DS_UMTS_PDP_PROFILE_IS_PERSISTENT(ds_umts_pdp_profile_ptr)) 
  {
    /*-------------------------------------------------------------------
      Init the fully qualified profile file name to pdp_profiles directory
    -------------------------------------------------------------------*/
    if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
      return DS_UMTS_PDP_FAIL;
        
    /*-------------------------------------------------------------------
      Open the file for write 
    -------------------------------------------------------------------*/
    DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ", 
                            ds_umts_pdp_fully_qual_profile_file_name);
    
    fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                          O_RDWR);
    if ( fs_handle == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_open failed for %d errno=%d.", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      Set the file offset to the start of auth info
    -------------------------------------------------------------------*/
    if (efs_lseek(fs_handle,
                  DS_UMTS_PDP_DNS_SECONDARY_OFFSET,
                  SEEK_SET) == -1) 
    {
      DS_3GPP_MSG3_ERROR( "efs_lseek failed for %d errno=%d", fs_handle, efs_errno, 0 );
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    /*-------------------------------------------------------------------
      write the auth info
    -------------------------------------------------------------------*/
    if (ds_umts_pdp_efs_write(fs_handle,
                              (void *)&secondary_dns_addr,
                              sizeof(ds_umts_pdp_addr_type)) == FALSE)
    {
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    
    /*-------------------------------------------------------------------
      Close the file
    -------------------------------------------------------------------*/
    if (efs_close (fs_handle) == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_close failed for %d errno = %d", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  }
  /*-------------------------------------------------------------------
    Update the cache table with the secondary dns address value passed.
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_TASKLOCK_NON_L4();
  memscpy( (void*)&ds_umts_pdp_profile_ptr->dns_addr.secondary_dns_addr,
  	    sizeof(ds_umts_pdp_addr_type),
           (void*)&secondary_dns_addr,
           sizeof(ds_umts_pdp_addr_type) );
  DS_UMTS_PDP_TASKFREE_NON_L4();

  return DS_UMTS_PDP_SUCCESS;
}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_DNS_ADDRESS_INFO_TO_DEFAULT_INTERNAL

DESCRIPTION
  This function sets the dns addresses of a pdp profile to default values
  primary, secondary DNS addresses are set to 0.0.0.0
  The dns parameters are updated with the values passed even if a valid 
  context defintion is not present. 

PARAMETERS 
  profile_number  : profile number

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_set_pdp_profile_dns_address_info_to_default_internal
(
  uint16                              profile_number,
  ds_umts_pdp_subs_e_type             subs_id
)
{
  ds_umts_pdp_dns_addresses_type pdp_dns_info;
  ds_umts_pdp_profile_status_etype status;
  boolean               read_only_flag;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  /*-------------------------------------------------------------------------
    Constuct the DNS address information 
    Both primary & secondary addresses is set to 0.0.0.0
    writing 0x0 to the entire structure sets the above
  -------------------------------------------------------------------------*/
  memset((void*) &pdp_dns_info,0x0,sizeof(ds_umts_pdp_dns_addresses_type) );

  /*-------------------------------------------------------------------------
    Call ds_umts_set_pdp_profile_dns_address_info to set the values.
  -------------------------------------------------------------------------*/
  status = ds_umts_set_pdp_profile_dns_address_info(
                                                  profile_number,
                                                  &pdp_dns_info);

  return status;
}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_ALL_DATA_INTERNAL

DESCRIPTION
  This function is used to set all the parameters associated with a profile
  The profile_data_ptr is passed in. The profile_data_ptr should atleast have a
  valid context for this function to update the profile parameters 
  
  NOTE : The name of the profile is overwritten with the value passed in 
  the profile
         The profile is not modified if read_only_flag is set
         The transience_flag cannot be modified.
 
PARAMETERS 
  profile_number  : profile number
  profile_data_ptr    : pointer to the profile data parameters
 
 
DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file
  DS_UMTS_PDP_CONTEXT_NOT_DEFINED  : The parameter did not contain a valid context
  DS_UMTS_PDP_FAIL                 : Everything else...
SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_set_pdp_profile_all_data_internal
(
  uint16                    profile_number,
  ds_umts_pdp_subs_e_type   subs_id, 
  ds_umts_pdp_profile_type *profile_data_ptr 
)
{
  ds_umts_pdp_profile_status_etype status;
  boolean               read_only_flag;
  boolean               transience_flag;
  boolean               valid_flag;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if(profile_data_ptr == NULL ) 
  {
    return DS_UMTS_PDP_FAIL;
  }

  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);
  /*-------------------------------------------------------------------------
    Check if the context defn is set to true : only then we allow it 
    to be set. to reset a context to undefined, the reset funs should be used
  -------------------------------------------------------------------------*/
  if( !(profile_data_ptr->context.valid_flg) )
  {
    DS_3GPP_MSG3_ERROR( "profile :%d does not have a valid context",profile_number, 0, 0 );
    return  DS_UMTS_PDP_CONTEXT_NOT_DEFINED ;
  }

  if( ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
                              DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 

  
  if((status = ds_umts_get_pdp_profile_context_info_is_valid_per_subs(profile_number,
                                                                      subs_id,
                                                             &valid_flag))
                                                      != DS_UMTS_PDP_SUCCESS)
  {
    return status;
  }

  if(valid_flag) 
  {
    status = ds_umts_get_pdp_profile_transience_flag(profile_number, 
                                                     subs_id,
                                                       &transience_flag);
    if( status != DS_UMTS_PDP_SUCCESS)
      return status;

    if (transience_flag != profile_data_ptr->transience_flag) 
    {
      DS_3GPP_MSG3_ERROR("profile: %d cannot update transience_flag", 
                                                   profile_number, 0, 0);
      return DS_UMTS_PDP_FAIL;
    }
  }

  /*-------------------------------------------------------------------
    Call the local fn ds_umts_update_pdp_profile_all_data to set the 
    profile init flag and profile number are checked in 
    ds_umts_update_pdp_profile_all_data
  -------------------------------------------------------------------*/  
  status = ds_umts_update_pdp_profile_all_data(
                                             profile_number,
                                             subs_id,
                                             profile_data_ptr);

  return status;
}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_ALL_DATA_TO_DEFAULT_INTERNAL

DESCRIPTION
  This function sets the profile information to default values for a given
  profile number. It defines a valid context for the profile (pdp-ip)or 
  pdp-ipv6  as per ip_vsn and sets all other parameters to the default values
  context : no compression, ip address :0.0.0.0, APN : Null
  Auth    : no auth
  QOS     : no qos params defined ( umts and gprs)
  dns     : primary and secondary set to 0.0.0.0
  TFT     : no tft

  NOTE : The name of the profile is left as-is.
         If the profile was already created (valid flag true),
         the transience_flag is left as-is
 
PARAMETERS 
  profile_number  : profile number

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file

SIDE EFFECTS 
  All the profile parameters are overwritten.
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_set_pdp_profile_all_data_to_default_internal
(
  uint16                              profile_number,
  ds_umts_pdp_subs_e_type             subs_id,
  ds_ip_version_enum_type             ip_vsn
)
{
  uint16                           profile_name[DS_UMTS_MAX_PROFILE_NAME_LEN+1];
  ds_umts_pdp_profile_status_etype status;
  int                              loop;
  boolean                          valid;
  boolean                          read_only_flag;    
  boolean                          transience_flag = FALSE;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  /*-------------------------------------------------------------------------
    Retain the name of the profile and set everything else to default values
  -------------------------------------------------------------------------*/
#ifndef FEATURE_PROFILE_OTAP_ENHANCEMENTS
  status = ds_umts_get_pdp_profile_name_per_subs(profile_number,subs_id,
                                        (byte *)profile_name,(DS_UMTS_MAX_PROFILE_NAME_LEN+1));
#else
  status = ds_umts_get_pdp_profile_name_utf16_per_subs(profile_number,subs_id
                                        profile_name,(DS_UMTS_MAX_PROFILE_NAME_LEN+1));
#endif
  if(status != DS_UMTS_PDP_SUCCESS)
  { 
    DS_3GPP_MSG3_HIGH("profile name could not be retrieved:%d",
             profile_number, 0, 0);
    
    return status;
  }
  /*-----------------------------------------------------
     If profile valid (exists), save transience flag
   ----------------------------------------------------*/
  ds_umts_get_pdp_profile_context_info_is_valid_per_subs( profile_number,
                                                          subs_id,
                                                          &valid);

  if(status != DS_UMTS_PDP_SUCCESS) 
    return status;
  if( valid ) 
  {
     status = ds_umts_get_pdp_profile_transience_flag(profile_number, 
                                                      subs_id,
                                                       &transience_flag);
  }

  DS_UMTS_PDP_ALLOC_LOCAL_PDP_PROFILE( ds_umts_pdp_local_profile_ptr );

  /*-------------------------------------------------------------------------
    Construct the default profile : 
    Context: valid ( no compression) 
    ip-address :0.0.0.0 for PDP-IP, ::0 for PDP-IPV6
    APN :NULL)
    Auth   : None 
    UMTS QOS : not defined (valid_flg = FALSE )
    GPRS (req & min) QOS : not defined (valid_flg = FALSE )
    DNS : primary,seconary addresses :0.0.0.0 for PDP-IP, ::0 for PDP-IPV6 
    TFT   : None 
    Leave the name unchanged
  -------------------------------------------------------------------------*/
  ds_umts_pdp_local_profile_ptr->context.valid_flg          = TRUE;
  ds_umts_pdp_local_profile_ptr->transience_flag            = FALSE; 
  ds_umts_pdp_local_profile_ptr->context.pdp_context_number = profile_number;
  switch( ip_vsn )
  {
    case DS_IP_V4:
      /*-------------------------------------------------------------------------
        Set to PDP-IP context
      -------------------------------------------------------------------------*/
      ds_umts_pdp_local_profile_ptr->context.pdp_type = DS_UMTS_PDP_IP;    
      break;

    case DS_IP_V6:
#ifdef FEATURE_DATA_PS_IPV6
      /*-------------------------------------------------------------------------
        Set to PDP-IPV6 context
      -------------------------------------------------------------------------*/
      ds_umts_pdp_local_profile_ptr->context.pdp_type = DS_UMTS_PDP_IPV6;
#else
      DS_UMTS_PDP_FREE_LOCAL_PDP_PROFILE(ds_umts_pdp_local_profile_ptr);
      return DS_UMTS_PDP_FAIL;
#endif /* FEATURE_DATA_PS_IPV6 */
      break;

    case DS_IP_V4V6:

#ifdef FEATURE_DATA_PS_IPV6
      /*-------------------------------------------------------------------------
        Set to PDP-IPV4V6 context
      -------------------------------------------------------------------------*/
      ds_umts_pdp_local_profile_ptr->context.pdp_type = DS_UMTS_PDP_IPV4V6;    

#else
      /*-------------------------------------------------------------------------
        Set to PDP-IP context
      -------------------------------------------------------------------------*/
      ds_umts_pdp_local_profile_ptr->context.pdp_type = DS_UMTS_PDP_IP;
#endif /* FEATURE_DATA_PS_IPV6 */
      break;

    default:
      DS_3GPP_MSG3_ERROR("Wrong IP version:%d",ip_vsn,0,0);
      DS_UMTS_PDP_FREE_LOCAL_PDP_PROFILE(ds_umts_pdp_local_profile_ptr);
      return DS_UMTS_PDP_FAIL;
  } 
    
  ds_umts_pdp_local_profile_ptr->context.pdp_addr.ip_vsn            = ip_vsn;
  ds_umts_pdp_local_profile_ptr->dns_addr.ip_vsn                    = ip_vsn;
  ds_umts_pdp_local_profile_ptr->dns_addr.primary_dns_addr.ip_vsn   = ip_vsn;
  ds_umts_pdp_local_profile_ptr->dns_addr.secondary_dns_addr.ip_vsn = ip_vsn;
  ds_umts_pdp_local_profile_ptr->apn_bearer = DS_UMTS_PDP_PROFILE_APN_BEARER_FLAG_ALL;
  for(loop = 0; loop < DS_UMTS_MAX_TFT_PARAM_SETS ; loop++)
  {  
    ds_umts_pdp_local_profile_ptr->tft[loop].src_addr_mask.address.ip_vsn = ip_vsn;
  }
  
/* Gobi specific Customization*/
#ifdef FEATURE_GOBI

/* Set requested UMTS QoS parm, traffic class to Interactive for Gobi2 UMTS builds - CR 155084 */
  ds_umts_pdp_local_profile_ptr->qos_request_umts.valid_flg = TRUE;    
  ds_umts_pdp_local_profile_ptr->qos_request_umts.traffic_class = DS_UMTS_QOS_TCLASS_INTR;    

#endif

  ds_umts_pdp_local_profile_ptr->max_pdn_conn_per_blk = 
    DS_UMTS_PDP_DEF_MAX_PDN_CONN_PER_BLK;
  ds_umts_pdp_local_profile_ptr->pdn_req_wait_time = 
    DS_UMTS_PDP_DEF_PDN_REQ_WAIT_TIME;
  ds_umts_pdp_local_profile_ptr->max_pdn_conn_time = 
    DS_UMTS_PDP_DEF_MAX_PDN_CONN_TIME;

  memscpy( (void*) ds_umts_pdp_local_profile_ptr->profile_name,
  	   sizeof(ds_umts_pdp_local_profile_ptr->profile_name),
           (void*) profile_name,
           sizeof(profile_name) );

  /*---------------------------------------------------------------
   If profile was valid before this call, restore transience flag
  ---------------------------------------------------------------*/
  if( valid ) 
  {
    ds_umts_pdp_local_profile_ptr->transience_flag = transience_flag;
  }

  /*-------------------------------------------------------------------
    Call the local fn ds_umts_update_pdp_profile_all_data to set the 
    profile init flag and profile number are checked in 
    ds_umts_update_pdp_profile_all_data
  -------------------------------------------------------------------*/
  status = ds_umts_update_pdp_profile_all_data( profile_number,subs_id,
                                                ds_umts_pdp_local_profile_ptr);
  DS_UMTS_PDP_FREE_LOCAL_PDP_PROFILE(ds_umts_pdp_local_profile_ptr);
  
  return status;
}   

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_ALL_DATA_TO_DEFAULT_EX_INTERNAL

DESCRIPTION 
  This function is similar to DS_UMTS_SET_PDP_PROFILE_ALL_DATA_TO_DEFAULT_INTERNAL.
  It takes an extra parameter, transience_flag - allowing DS_PROFILE_CREATE_EX to
  create a profile in a single EFS update.
 
  This function sets the profile information to default values for a given
  profile number. It defines a valid context for the profile (pdp-ip)or 
  pdp-ipv6  as per ip_vsn and sets all other parameters to the default values
  context : no compression, ip address :0.0.0.0, APN : Null
  Auth    : no auth
  QOS     : no qos params defined ( umts and gprs)
  dns     : primary and secondary set to 0.0.0.0
  TFT     : no tft

  NOTE : The name of the profile is left as-is.
         If the profile was already created (valid flag true),
         the transience_flag is left as-is, otherwise, transience_flag
         is set.
 
 
PARAMETERS 
  profile_number  : profile number

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file

SIDE EFFECTS 
  All the profile parameters are overwritten.
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_set_pdp_profile_all_data_to_default_ex_internal
(
  uint16                  profile_number,
  ds_umts_pdp_subs_e_type subs_id,
  ds_ip_version_enum_type ip_vsn,
  boolean                 transience_flag
)
{
  uint16                           profile_name[DS_UMTS_MAX_PROFILE_NAME_LEN+1];
  ds_umts_pdp_profile_status_etype status;
  int                              loop;
  boolean                          valid;
  boolean                          transience_flag_read = FALSE;    
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------------
    Retain the name of the profile and set everything else to default values
  -------------------------------------------------------------------------*/
#ifndef FEATURE_PROFILE_OTAP_ENHANCEMENTS
  status = ds_umts_get_pdp_profile_name(profile_number,
                                        (byte *)profile_name,(DS_UMTS_MAX_PROFILE_NAME_LEN+1));
#else
  status = ds_umts_get_pdp_profile_name_utf16(profile_number,
                                        profile_name,(DS_UMTS_MAX_PROFILE_NAME_LEN+1));
#endif

  if(status != DS_UMTS_PDP_SUCCESS)
  { 
    DS_3GPP_MSG3_HIGH("profile name could not be retrieved:%d",
             profile_number, 0, 0);
    
    return status;
  }
  /*-----------------------------------------------------
     If profile valid (exists), save transience flag
   ----------------------------------------------------*/
  ds_umts_get_pdp_profile_context_info_is_valid_per_subs( profile_number,subs_id,
                                                          &valid);
  
  if( valid ) 
  {
     status = ds_umts_get_pdp_profile_transience_flag(profile_number,subs_id,
                                                       &transience_flag_read);
  }

  DS_UMTS_PDP_ALLOC_LOCAL_PDP_PROFILE( ds_umts_pdp_local_profile_ptr );

  /*-------------------------------------------------------------------------
    Construct the default profile : 
    Context: valid ( no compression) 
    ip-address :0.0.0.0 for PDP-IP, ::0 for PDP-IPV6
    APN :NULL)
    Auth   : None 
    UMTS QOS : not defined (valid_flg = FALSE )
    GPRS (req & min) QOS : not defined (valid_flg = FALSE )
    DNS : primary,seconary addresses :0.0.0.0 for PDP-IP, ::0 for PDP-IPV6 
    TFT   : None 
    Leave the name unchanged
  -------------------------------------------------------------------------*/
  ds_umts_pdp_local_profile_ptr->context.valid_flg          = TRUE;
  ds_umts_pdp_local_profile_ptr->transience_flag            = transience_flag; 
  ds_umts_pdp_local_profile_ptr->context.pdp_context_number = profile_number;

  switch( ip_vsn )
  {
    case DS_IP_V4:
      /*-------------------------------------------------------------------------
        Set to PDP-IP context
      -------------------------------------------------------------------------*/
      ds_umts_pdp_local_profile_ptr->context.pdp_type = DS_UMTS_PDP_IP;    
      break;

    case DS_IP_V6:
#ifdef FEATURE_DATA_PS_IPV6 
      /*-------------------------------------------------------------------------
        Set to PDP-IPV6 context
      -------------------------------------------------------------------------*/
      ds_umts_pdp_local_profile_ptr->context.pdp_type = DS_UMTS_PDP_IPV6;    
#else
      DS_UMTS_PDP_FREE_LOCAL_PDP_PROFILE(ds_umts_pdp_local_profile_ptr);
      return DS_UMTS_PDP_FAIL;
#endif /* FEATURE_DATA_PS_IPV6 */
      break;

    case DS_IP_V4V6:

#ifdef FEATURE_DATA_PS_IPV6 
      /*-------------------------------------------------------------------------
        Set to PDP-IPV4V6 context
      -------------------------------------------------------------------------*/
      ds_umts_pdp_local_profile_ptr->context.pdp_type = DS_UMTS_PDP_IPV4V6;    
      
#else
      /*-------------------------------------------------------------------------
        Set to PDP-IP context
      -------------------------------------------------------------------------*/
      ds_umts_pdp_local_profile_ptr->context.pdp_type = DS_UMTS_PDP_IP;
#endif /* FEATURE_DATA_PS_IPV6 */
      break;

    default:
      DS_UMTS_PDP_FREE_LOCAL_PDP_PROFILE(ds_umts_pdp_local_profile_ptr);
      DS_3GPP_MSG3_ERROR("Wrong IP version:%d",ip_vsn,0,0);
      return DS_UMTS_PDP_FAIL;
  } 
    
  ds_umts_pdp_local_profile_ptr->context.pdp_addr.ip_vsn            = ip_vsn;
  ds_umts_pdp_local_profile_ptr->dns_addr.ip_vsn                    = ip_vsn;
  ds_umts_pdp_local_profile_ptr->dns_addr.primary_dns_addr.ip_vsn   = ip_vsn;
  ds_umts_pdp_local_profile_ptr->dns_addr.secondary_dns_addr.ip_vsn = ip_vsn;
  ds_umts_pdp_local_profile_ptr->apn_bearer = DS_UMTS_PDP_PROFILE_APN_BEARER_FLAG_ALL;
  for(loop = 0; loop < DS_UMTS_MAX_TFT_PARAM_SETS ; loop++)
  {  
    ds_umts_pdp_local_profile_ptr->tft[loop].src_addr_mask.address.ip_vsn = ip_vsn;
  }
  
/* Gobi specific Customization*/
#ifdef FEATURE_GOBI

/* Set requested UMTS QoS parm, traffic class to Interactive for Gobi2 UMTS builds - CR 155084 */
  ds_umts_pdp_local_profile_ptr->qos_request_umts.valid_flg     = TRUE;    
  ds_umts_pdp_local_profile_ptr->qos_request_umts.traffic_class = DS_UMTS_QOS_TCLASS_INTR;    

#endif

  ds_umts_pdp_local_profile_ptr->max_pdn_conn_per_blk = 
    DS_UMTS_PDP_DEF_MAX_PDN_CONN_PER_BLK;
  ds_umts_pdp_local_profile_ptr->pdn_req_wait_time = 
    DS_UMTS_PDP_DEF_PDN_REQ_WAIT_TIME;
  ds_umts_pdp_local_profile_ptr->max_pdn_conn_time = 
    DS_UMTS_PDP_DEF_MAX_PDN_CONN_TIME;

  memscpy( (void*) ds_umts_pdp_local_profile_ptr->profile_name,
  	   sizeof(ds_umts_pdp_local_profile_ptr->profile_name),
           (void*) profile_name,
           sizeof(profile_name) );

    /*---------------------------------------------------------------
   If profile was valid before this call, restore transience flag
  ---------------------------------------------------------------*/
  if( valid ) 
  {
    ds_umts_pdp_local_profile_ptr->transience_flag = transience_flag_read;
  }

  /*-------------------------------------------------------------------
    Call the local fn ds_umts_update_pdp_profile_all_data to set the 
    profile init flag and profile number are checked in 
    ds_umts_update_pdp_profile_all_data
  -------------------------------------------------------------------*/
  status = ds_umts_update_pdp_profile_all_data( profile_number,subs_id,
                                                ds_umts_pdp_local_profile_ptr);
  DS_UMTS_PDP_FREE_LOCAL_PDP_PROFILE(ds_umts_pdp_local_profile_ptr);
  
  return status;
}   

/*===========================================================================
FUNCTION DS_UMTS_RESET_PDP_PROFILE_TO_UNDEFINED_INTERNAL

DESCRIPTION
  This function resets the profile specified to undefined values. 
  An undefined profile has the foll : 
  context - not defined
  auth information : None
  QOS params ( umts and gprs ) : not defined
  dns addresses : all addresses are set to 0.0.0.0
  TFT information : None

  NOTE : The name of the profile is left as-is.

PARAMETERS 
  profile_number  : profile number to be set to undefined 

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file

SIDE EFFECTS 
  All the profile parameters are overwritten
===========================================================================*/
ds_umts_pdp_profile_status_etype
  ds_umts_reset_pdp_profile_to_undefined_internal
(
  uint16                              profile_number,
  ds_umts_pdp_subs_e_type             subs_id
)
{
  boolean               read_only_flag;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();


    if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
       DS_UMTS_PDP_INVALID_PROFILE_NUM )
    {
      return DS_UMTS_PDP_INVALID_PROFILE_NUM; 
    }

  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  /*-------------------------------------------------------------------------
    Call the local function to reset the profile : undefined values are
    context : not defined
    auth    : None
    umts qos : not defined
    GPRS qos(req &min) :not defined
    dns addresses : primary & secondary - 0.0.0.0
    TFT :not defined
  -------------------------------------------------------------------------*/
  if(! ds_umts_update_pdp_profile_to_undefined(profile_number,subs_id) )
  {
    DS_3GPP_MSG3_HIGH("error in resetting context/profile:%d",
      profile_number, 0, 0);
    return DS_UMTS_PDP_ACCESS_ERROR;
  }

  return DS_UMTS_PDP_SUCCESS;
}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_NUM_FOR_EMBEDDED_CALLS_INTERNAL

DESCRIPTION
  This function sets the profile number specified to be used for embedded 
  calls. It writes the value specified to the file the embedded_call_prof_num
  file

PARAMETERS 
  profile_number  : profile number 

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype
   ds_umts_set_pdp_profile_num_for_embedded_calls_internal
(
  uint16                              profile_number,
  ds_umts_pdp_subs_e_type             subs_id
)
{
  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
       DS_UMTS_PDP_INVALID_PROFILE_NUM )
  {
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 
  }

  /*---------------------------------------------------------------------
    Call the local routine to update the file with the new profile number
  ---------------------------------------------------------------------*/ 
  if( ! ds_umts_update_embedded_call_prof_num(profile_number,subs_id) )
  {
    return  DS_UMTS_PDP_ACCESS_ERROR;
  }

  return DS_UMTS_PDP_SUCCESS;
}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_NUM_FOR_TETHERED_CALLS_INTERNAL

DESCRIPTION
  This function sets the profile number specified to be used for tethered 
  calls. It writes the value specified to the file the tethered_call_prof_num file

PARAMETERS 
  profile_number  : profile number 

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype
   ds_umts_set_pdp_profile_num_for_tethered_calls_internal
(
  uint16                              profile_number,
  ds_umts_pdp_subs_e_type             subs_id
)
{
  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if( ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
      DS_UMTS_PDP_INVALID_PROFILE_NUM )
  {
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 
  }

  /*---------------------------------------------------------------------
    Call the local routine to update the file with the new profile number
  ---------------------------------------------------------------------*/ 
  if( ! ds_umts_update_default_tethered_prof_num(profile_number,subs_id) )
  {
    return  DS_UMTS_PDP_ACCESS_ERROR;
  }

  return DS_UMTS_PDP_SUCCESS;
}

/*===========================================================================
FUNCTION DS_UMTS_GET_PDP_PROFILE_ACCESS_CTRL_FLAG_INTERNAL

DESCRIPTION
  This function retrieves the "access_ctrl_flag" specified for a profile. 
 
  Note : This information can be retrieved even if the context (profile) has
  not been defined

PARAMETERS
  profile_number  : profile number
  access_ctrl_flag  : pointer to store the access_ctrl_flag

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system

SIDE EFFECTS 
  None
===========================================================================*/
/*ARGSUSED*/
ds_umts_pdp_profile_status_etype  ds_umts_get_pdp_profile_access_ctrl_flag_internal
(
  uint16                          profile_number,
  ds_umts_pdp_subs_e_type         subs_id,
  dsat_pdp_access_control_e_type  *access_ctrl_flag
)
{
#ifndef FEATURE_DATA_TE_MT_PDP
  DS_3GPP_MSG3_ERROR("PDP registry API not supported", 0, 0, 0);
  return DS_UMTS_PDP_API_NOT_SUPPORTED;
#else
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 

  /*-------------------------------------------------------------------
    Read the access_ctrl_flag from the cache table.
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_TASKLOCK_NON_L4();
  *access_ctrl_flag = 
    DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number)->context.access_ctrl_flag;
  DS_UMTS_PDP_TASKFREE_NON_L4();
  return DS_UMTS_PDP_SUCCESS;
#endif
}

#ifdef FEATURE_GOBI_MUXD_PDP_PROFILE_DIR

/*===========================================================================
FUNCTION UNDP_GET_MUXD_PDP_PROFILE_DIRNAME

DESCRIPTION
  Return Consolidated Profile Directory Name

PARAMETERS 
  None

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  Consolidated Profile Directory Name

SIDE EFFECTS 
  None
===========================================================================*/

static char* undp_get_muxd_pdp_profile_dirname(void)
{
  nv_stat_enum_type  nv_status;
  uint8              gobi_consl_profle_dir_index;

  /* Get the index number from NV if not done so already */
  if ( !undp_muxd_consl_pdp_profile_dir_name_len )
  {
    nv_status = dsi_get_nv_item(NV_IRDA_PNP_VERSION_I, &ds_nv_item);
    gobi_consl_profle_dir_index = (uint8)((ds_nv_item.irda_pnp_version & UNDP_CONSL_PROFILE_EN_MASK) >> 8);

    /* Consolidated AtCOP & RmNET profiles */
    strlcpy( undp_muxd_consl_pdp_profile_file_dir_name,                
                 UNDP_PDP_PROFILE_CONSL_DIR_NAME_START,
                 sizeof(undp_muxd_consl_pdp_profile_file_dir_name) );     

    if((nv_status == NV_DONE_S) && (gobi_consl_profle_dir_index !=0 ))
    {
        ds_umts_pdp_itoa( gobi_consl_profle_dir_index, 
                      ((byte*)&undp_muxd_consl_pdp_profile_file_dir_name[UNDP_PDP_PROFILE_CONSL_DIR_NAME_INDEX]), 
                      10 );
        undp_muxd_consl_pdp_profile_dir_name_len = DS_UMTS_PDP_PROFILE_CONSL_DIR_NAME_LEN; 
    }
    else
    {
        /* NV Item Inactive or set to 0 */
        undp_muxd_consl_pdp_profile_dir_name_len = UNDP_PDP_PROFILE_CONSL_DIR_NAME_INDEX; 
    }
  }

  return undp_muxd_consl_pdp_profile_file_dir_name;
}
#endif /* FEATURE_GOBI_MUXD_PDP_PROFILE_DIR */

/*===========================================================================
FUNCTION DS_UMTS_SET_DEFAULT_PROFILE_NUMBER_PER_SUBS_INTERNAL

DESCRIPTION
  This function sets the profile number for tethered or embedded family for 
  the specified subscription. The value is written to either 
  embedded_call_prof_nums_dsds or tethered_call_prof_nums_dsds file.

PARAMETERS 
  family           : Denotes embedded or tetherd profile
  subs             : Denotes one of two active subscriptions
  profile_number   : Profile number

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : The operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file

SIDE EFFECTS 
  None
===========================================================================*/

ds_umts_pdp_profile_status_etype
ds_umts_set_default_profile_number_per_subs_internal
(
  ds_umts_profile_family_e_type        family,
  ds_umts_pdp_subs_e_type              subs_id,
  uint16                               profile_number
)
{
  ds_umts_pdp_profile_status_etype status = DS_UMTS_PDP_FAIL;

  /*---------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  ---------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
                                     DS_UMTS_PDP_INVALID_PROFILE_NUM )
  {
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 
  }
  
  /*---------------------------------------------------------------------
    Check if the subscription id is valid
  ---------------------------------------------------------------------*/
  if((subs_id < DS_UMTS_PDP_SUBSCRIPTION_NONE) ||
     (subs_id >= DS_UMTS_PDP_SUBSCRIPTION_MAX -1 ))
  {
    return DS_UMTS_PDP_INVALID_SUBS_ID;
  }

  /*---------------------------------------------------------------------
    Call the local routine based on family to update the EFS file with
    the new profile number
  ---------------------------------------------------------------------*/ 
  switch( family )
  {
    case DS_UMTS_EMBEDDED_PROFILE_FAMILY:
      status = ds_umts_set_pdp_profile_num_for_embedded_calls_internal(
                                           profile_number,subs_id);
      break;
      
    case DS_UMTS_TETHERED_PROFILE_FAMILY:
      status = ds_umts_set_pdp_profile_num_for_tethered_calls_internal(
                                           profile_number,subs_id);
      break;

    default:
      status = DS_UMTS_PDP_FAIL; 
  }
  return status;
}

/*===========================================================================
FUNCTION DS_UMTS_GET_DEFAULT_PROFILE_NUMBER_PER_SUBS_INTERNAL

DESCRIPTION
  This function gets the profile number for the specifies subscription.
  It gets the value specified to the embedded_call_prof_nums_dsds file or 
  tethered_call_prof_nums_dsds file.

PARAMETERS 
  family           : Denotes embedded or tethered profile
  subs             : Denotes one of two active subscriptions
  profile_number   : Profile number

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : The operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file

SIDE EFFECTS 
  None
===========================================================================*/

ds_umts_pdp_profile_status_etype
ds_umts_get_default_profile_number_per_subs_internal
(
  ds_umts_profile_family_e_type        family,
  ds_umts_pdp_subs_e_type              subs_id,
  uint16                              *profile_number
)
{
  ds_umts_pdp_profile_status_etype status = DS_UMTS_PDP_FAIL;

  /*---------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
  ---------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();

/*---------------------------------------------------------------------
    Check if the subscription id is valid
  ---------------------------------------------------------------------*/
  if((subs_id < DS_UMTS_PDP_SUBSCRIPTION_NONE) ||
     (subs_id >= DS_UMTS_PDP_SUBSCRIPTION_MAX -1 ))
  {
    return DS_UMTS_PDP_INVALID_SUBS_ID;
  }

  /*---------------------------------------------------------------------
    Call the local routine based on family to get the default 
    profile number
  ---------------------------------------------------------------------*/ 
  switch( family )
  {
    case DS_UMTS_EMBEDDED_PROFILE_FAMILY:
      status = ds_umts_get_pdp_profile_num_for_embedded_calls_internal(
                                           (byte *)profile_number,subs_id);
      break;

    case DS_UMTS_TETHERED_PROFILE_FAMILY:
      status = ds_umts_get_pdp_profile_num_for_tethered_calls_internal(
                                           (byte*)profile_number,subs_id);
      break;

    default:
      status = DS_UMTS_PDP_FAIL;
  }
  return status;
}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_EMERGENCY_CALLS_ARE_SUPPORTED_FLAG_INTERNAL

DESCRIPTION
  This function calls the internal registry function to set the
  "emergency_calls_are_supported" flag specified for a profile. 
  This flag indicates if IMS emergency calls can be made using this profile.
  Note : This information can be retrieved even if the context (profile) has
  not been defined

PARAMETERS
  profile_number  : profile number
  emergency_calls_are_supported   :value to store in the emergency_calls_are_supported
                                   flag.

DEPENDENCIES
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system

SIDE EFFECTS
  none
===========================================================================*/
ds_umts_pdp_profile_status_etype 
  ds_umts_set_pdp_profile_emergency_calls_are_supported_flag_internal
(
  uint16                              profile_number,
  ds_umts_pdp_subs_e_type             subs_id,
  boolean                             emergency_calls_are_supported
)
{
#ifdef FEATURE_EMERGENCY_PDN
  int                             fs_handle;   
  ds_umts_pdp_profile_type        *ds_umts_pdp_profile_ptr;
  boolean read_only_flag;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
 
  /*-------------------------------------------------------------------
    Check if we have been initialized & the input paramaters are correct
    A valid profile need not be defined at this time to set the name    
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_CHECK_INIT_FLAG();
  if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
     DS_UMTS_PDP_INVALID_PROFILE_NUM )
    return DS_UMTS_PDP_INVALID_PROFILE_NUM; 
  
  DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

  /*-------------------------------------------------------------------
    Initialize the variables for fs_open                   
  -------------------------------------------------------------------*/

  ds_umts_pdp_profile_ptr = 
    DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);

  /* Update EFS only if profile is persistent */
  if(DS_UMTS_PDP_PROFILE_IS_PERSISTENT(ds_umts_pdp_profile_ptr)) 
  {
    /*-------------------------------------------------------------------
      Init the fully qualified profile file name to pdp_profiles directory
    -------------------------------------------------------------------*/
    if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
      return DS_UMTS_PDP_FAIL;
  
    /*-------------------------------------------------------------------
      Open the file for read / write
    -------------------------------------------------------------------*/
    DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ", 
                            ds_umts_pdp_fully_qual_profile_file_name);
    
    fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                          O_RDWR);
    if ( fs_handle == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_open failed for %d errno=%d.", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      Set the file offset to the start of context information
    -------------------------------------------------------------------*/
    if (efs_lseek(fs_handle,
                  DS_UMTS_PDP_PROFILE_EMERGENCY_CALLS_ARE_SUPPORTED_OFFSET,
                  SEEK_SET) == -1)
    {
      DS_3GPP_MSG3_ERROR( "efs_lseek failed for %d errno=%d", fs_handle, efs_errno, 0 );
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  
    /*-------------------------------------------------------------------
      write the profile im_cn_flag
    -------------------------------------------------------------------*/
    if (ds_umts_pdp_efs_write(fs_handle,
                              (void *)(&emergency_calls_are_supported),
                              sizeof(boolean)) == FALSE) 
    {
      efs_close (fs_handle);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
    
    /*-------------------------------------------------------------------
      Close the file
    -------------------------------------------------------------------*/
    if (efs_close (fs_handle) == -1)
    {
      DS_3GPP_MSG3_ERROR("efs_close failed for %d errno = %d", fs_handle, efs_errno, 0);
      return DS_UMTS_PDP_ACCESS_ERROR;
    }
  }

  /*-------------------------------------------------------------------
    Update the cache table with the new read_only_flag value.
  -------------------------------------------------------------------*/
  DS_UMTS_PDP_TASKLOCK_NON_L4();
  memscpy( (void*)&ds_umts_pdp_profile_ptr->emergency_calls_are_supported,
  	   sizeof(boolean),
          (void*)&emergency_calls_are_supported,
          sizeof(boolean) );
  DS_UMTS_PDP_TASKFREE_NON_L4();
  
  return DS_UMTS_PDP_SUCCESS;  
#endif /* FEATURE_EMERGENCY_PDN */
  return DS_UMTS_PDP_FEATURE_NOT_SUPPORTED;
}

#ifdef FEATURE_DATA_LTE
/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_MAX_PDN_CONN_PER_BLK_INTERNAL

DESCRIPTION
  This function sets the Max PDN Conn Per Block field in the profile to
  the specified value.

PARAMETERS
  profile_number       : profile number
  max_pdn_conn_per_blk : Max PDN Connections Per Blk

DEPENDENCIES
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system

SIDE EFFECTS
  none
===========================================================================*/
ds_umts_pdp_profile_status_etype 
ds_umts_set_pdp_profile_max_pdn_conn_per_blk_internal
(
  uint16                              profile_number,
  ds_umts_pdp_subs_e_type             subs_id,
  uint16                              max_pdn_conn_per_blk
)
{
    int                             fs_handle;   
    ds_umts_pdp_profile_type        *ds_umts_pdp_profile_ptr;
    boolean read_only_flag;
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

    /*-------------------------------------------------------------------
      Check if we have been initialized & the input paramaters are correct
      A valid profile need not be defined at this time to set the name    
    -------------------------------------------------------------------*/
    DS_UMTS_PDP_CHECK_INIT_FLAG();
    if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
       DS_UMTS_PDP_INVALID_PROFILE_NUM )
    {
      return DS_UMTS_PDP_INVALID_PROFILE_NUM; 
    }

    DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

    /*-------------------------------------------------------------------
      Initialize the variables for fs_open                   
    -------------------------------------------------------------------*/

    ds_umts_pdp_profile_ptr = 
      DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);

    if(DS_UMTS_PDP_PROFILE_IS_PERSISTENT(ds_umts_pdp_profile_ptr)) 
    {
      /*-------------------------------------------------------------------
        Init the fully qualified profile file name to pdp_profiles directory
      -------------------------------------------------------------------*/
      if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
      {
        return DS_UMTS_PDP_FAIL;
      }

      /*-------------------------------------------------------------------
        Open the file for read / write
      -------------------------------------------------------------------*/
      DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ", 
                              ds_umts_pdp_fully_qual_profile_file_name);
    
      fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                            O_RDWR);
      if ( fs_handle == -1)
      {
        DS_3GPP_MSG3_ERROR("efs_open failed for %d errno=%d.", fs_handle, efs_errno, 0);
        return DS_UMTS_PDP_ACCESS_ERROR;
      }

      /*-------------------------------------------------------------------
        Set the file offset to the start of Max PDN Conn Per Blk information
      -------------------------------------------------------------------*/
      if (efs_lseek(fs_handle,
                    DS_UMTS_PDP_PROFILE_MAX_PDN_CONN_PER_BLK_OFFSET,
                    SEEK_SET) == -1)
      {
        ERR( "efs_lseek failed for %d errno=%d", fs_handle, efs_errno, 0 );
        return DS_UMTS_PDP_ACCESS_ERROR;
      }


      /*-------------------------------------------------------------------
        write the profile for Max PDN Conn Per Blk
      -------------------------------------------------------------------*/
      if (ds_umts_pdp_efs_write(fs_handle,
                                (void *)&max_pdn_conn_per_blk,
                                sizeof(uint16)) == FALSE) 
      {
        return DS_UMTS_PDP_ACCESS_ERROR;
      }

       /*---------------------------------------------------------------
        Close the file
      -------------------------------------------------------------------*/
      if (efs_close (fs_handle) == -1)
      {
        ERR("efs_close failed for %d errno = %d", fs_handle, efs_errno, 0);
        return DS_UMTS_PDP_ACCESS_ERROR;
      }
    }

    DS_3GPP_MSG3_ERROR("max_pdn_conn_per_blk written is %d", max_pdn_conn_per_blk, 0, 0);
    /*-------------------------------------------------------------------
      Update the cache table with the new read_only_flag value.
    -------------------------------------------------------------------*/
    DS_UMTS_PDP_TASKLOCK_NON_L4();
    memscpy( (void*)&ds_umts_pdp_profile_ptr->max_pdn_conn_per_blk,
             sizeof(uint16),
            (void*)&max_pdn_conn_per_blk,
            sizeof(uint16) );
    DS_UMTS_PDP_TASKFREE_NON_L4();

    return DS_UMTS_PDP_SUCCESS;
}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_PDN_REQ_WAIT_TIME_INTERNAL

DESCRIPTION
  This function sets the PDN Req Wait Time field in the profile to
  the specified value.

PARAMETERS
  profile_number       : profile number
  pdn_req_wait_time    : PDN Req Wait Time

DEPENDENCIES
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system

SIDE EFFECTS
  none
===========================================================================*/
ds_umts_pdp_profile_status_etype 
ds_umts_set_pdp_profile_pdn_req_wait_time_internal
(
  uint16                              profile_number,
  ds_umts_pdp_subs_e_type             subs_id,
  uint16                              pdn_req_wait_time
)
{
    int                             fs_handle;   
    ds_umts_pdp_profile_type        *ds_umts_pdp_profile_ptr;
    boolean read_only_flag;
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

    /*-------------------------------------------------------------------
      Check if we have been initialized & the input paramaters are correct
      A valid profile need not be defined at this time to set the name    
    -------------------------------------------------------------------*/
    DS_UMTS_PDP_CHECK_INIT_FLAG();
    if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
       DS_UMTS_PDP_INVALID_PROFILE_NUM )
      return DS_UMTS_PDP_INVALID_PROFILE_NUM; 

    DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

    /*-------------------------------------------------------------------
      Initialize the variables for fs_open                   
    -------------------------------------------------------------------*/

    ds_umts_pdp_profile_ptr = 
      DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);

    if(DS_UMTS_PDP_PROFILE_IS_PERSISTENT(ds_umts_pdp_profile_ptr)) 
    {
      /*-------------------------------------------------------------------
        Init the fully qualified profile file name to pdp_profiles directory
      -------------------------------------------------------------------*/
      if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
        return DS_UMTS_PDP_FAIL;

      /*-------------------------------------------------------------------
        Open the file for read / write
      -------------------------------------------------------------------*/
      DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ", 
                              ds_umts_pdp_fully_qual_profile_file_name);
    
      fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                            O_RDWR);
      if ( fs_handle == -1)
      {
        DS_3GPP_MSG3_ERROR("efs_open failed for %d errno=%d.", fs_handle, efs_errno, 0);
        return DS_UMTS_PDP_ACCESS_ERROR;
      }

      /*-------------------------------------------------------------------
        Set the file offset to the start of PDN Req Wait Time information
      -------------------------------------------------------------------*/
      if (efs_lseek(fs_handle,
                    DS_UMTS_PDP_PROFILE_PDN_REQ_WAIT_TIME_OFFSET,
                    SEEK_SET) == -1)
      {
        ERR( "efs_lseek failed for %d errno=%d", fs_handle, efs_errno, 0 );
        return DS_UMTS_PDP_ACCESS_ERROR;
      }

      DS_3GPP_MSG3_ERROR("pdn_req_wait_time written is %d", pdn_req_wait_time, 0, 0);

      /*-------------------------------------------------------------------
        write the profile for PDN Req Wait Time
      -------------------------------------------------------------------*/
      if (ds_umts_pdp_efs_write(fs_handle,
                                (void *)&pdn_req_wait_time,
                                sizeof(uint16)) == FALSE) 
      {
        return DS_UMTS_PDP_ACCESS_ERROR;
      }

       /*---------------------------------------------------------------
        Close the file
      -------------------------------------------------------------------*/
      if (efs_close (fs_handle) == -1)
      {
        ERR("efs_close failed for %d errno = %d", fs_handle, efs_errno, 0);
        return DS_UMTS_PDP_ACCESS_ERROR;
      }
    }

    /*-------------------------------------------------------------------
      Update the cache table with the new read_only_flag value.
    -------------------------------------------------------------------*/
    DS_UMTS_PDP_TASKLOCK_NON_L4();
    memscpy( (void*)&ds_umts_pdp_profile_ptr->pdn_req_wait_time,
	     sizeof(uint16),
            (void*)&pdn_req_wait_time,
            sizeof(uint16) );
    DS_UMTS_PDP_TASKFREE_NON_L4();

    return DS_UMTS_PDP_SUCCESS;
}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_MAX_PDN_CONN_TIME_INTERNAL

DESCRIPTION
  This function sets the Max PDN Conn TIme field in the profile to
  the specified value.

PARAMETERS
  profile_number       : profile number
  pdn_req_wait_time    : Max PDN Conn Time

DEPENDENCIES
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system

SIDE EFFECTS
  none
===========================================================================*/
ds_umts_pdp_profile_status_etype 
ds_umts_set_pdp_profile_max_pdn_conn_time_internal
(
  uint16                              profile_number,
  ds_umts_pdp_subs_e_type             subs_id,
  uint16                              max_pdn_conn_time
)
{
    int                             fs_handle;   
    ds_umts_pdp_profile_type        *ds_umts_pdp_profile_ptr;
    boolean read_only_flag;
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

    /*-------------------------------------------------------------------
      Check if we have been initialized & the input paramaters are correct
      A valid profile need not be defined at this time to set the name    
    -------------------------------------------------------------------*/
    DS_UMTS_PDP_CHECK_INIT_FLAG();
    if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
       DS_UMTS_PDP_INVALID_PROFILE_NUM )
      return DS_UMTS_PDP_INVALID_PROFILE_NUM; 

    DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

    /*-------------------------------------------------------------------
      Initialize the variables for fs_open                   
    -------------------------------------------------------------------*/

    ds_umts_pdp_profile_ptr = 
      DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);

    if(DS_UMTS_PDP_PROFILE_IS_PERSISTENT(ds_umts_pdp_profile_ptr)) 
    {
      /*-------------------------------------------------------------------
        Init the fully qualified profile file name to pdp_profiles directory
      -------------------------------------------------------------------*/
      if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
        return DS_UMTS_PDP_FAIL;

      /*-------------------------------------------------------------------
        Open the file for read / write
      -------------------------------------------------------------------*/
      DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ", 
                              ds_umts_pdp_fully_qual_profile_file_name);
    
      fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                            O_RDWR);
      if ( fs_handle == -1)
      {
        DS_3GPP_MSG3_ERROR("efs_open failed for %d errno=%d.", fs_handle, efs_errno, 0);
        return DS_UMTS_PDP_ACCESS_ERROR;
      }

      /*-------------------------------------------------------------------
        Set the file offset to the start of Max PDN Conn Time information
      -------------------------------------------------------------------*/
      if (efs_lseek(fs_handle,
                    DS_UMTS_PDP_PROFILE_MAX_PDN_CONN_TIME_OFFSET,
                    SEEK_SET) == -1)
      {
        ERR( "efs_lseek failed for %d errno=%d", fs_handle, efs_errno, 0 );
        return DS_UMTS_PDP_ACCESS_ERROR;
      }

      DS_3GPP_MSG3_ERROR("max_pdn_conn_time written is %d", max_pdn_conn_time, 0, 0);
      /*-------------------------------------------------------------------
        write the profile for mnc
      -------------------------------------------------------------------*/
      if (ds_umts_pdp_efs_write(fs_handle,
                                (void *)&max_pdn_conn_time,
                                sizeof(uint16)) == FALSE) 
      {
        return DS_UMTS_PDP_ACCESS_ERROR;
      }

       /*---------------------------------------------------------------
        Close the file
      -------------------------------------------------------------------*/
      if (efs_close (fs_handle) == -1)
      {
        ERR("efs_close failed for %d errno = %d", fs_handle, efs_errno, 0);
        return DS_UMTS_PDP_ACCESS_ERROR;
      }
    }

    /*-------------------------------------------------------------------
      Update the cache table with the new read_only_flag value.
    -------------------------------------------------------------------*/
    DS_UMTS_PDP_TASKLOCK_NON_L4();
    memscpy( (void*)&ds_umts_pdp_profile_ptr->max_pdn_conn_time,
	     sizeof(uint16),
            (void*)&max_pdn_conn_time,
            sizeof(uint16) );
    DS_UMTS_PDP_TASKFREE_NON_L4();

    return DS_UMTS_PDP_SUCCESS;
}

#endif /*FEATURE_DATA_LTE*/
/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_USER_APP_DATA_INTERNAL
===========================================================================*/
/** 
  This function sets the "user_app_data" field in the profile to
  the specified value.

  @profile_number            : profile number
  @user_app_data             : uint32 value being passed in for the
                               user_app_data

DEPENDENCIES
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system

*/
ds_umts_pdp_profile_status_etype 
ds_umts_set_pdp_profile_user_app_data_internal
(
  uint16                              profile_number,
  ds_umts_pdp_subs_e_type             subs_id,
  uint32                              user_app_data
)
{
    int                             fs_handle;   
    ds_umts_pdp_profile_type        *ds_umts_pdp_profile_ptr;
    boolean read_only_flag;
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

    /*-------------------------------------------------------------------
      Check if we have been initialized & the input paramaters are correct
      A valid profile need not be defined at this time to set the name    
    -------------------------------------------------------------------*/
    DS_UMTS_PDP_CHECK_INIT_FLAG();
    if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
       DS_UMTS_PDP_INVALID_PROFILE_NUM )
      return DS_UMTS_PDP_INVALID_PROFILE_NUM; 

    DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

    /*-------------------------------------------------------------------
      Initialize the variables for fs_open                   
    -------------------------------------------------------------------*/

    ds_umts_pdp_profile_ptr = 
      DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);

    if(DS_UMTS_PDP_PROFILE_IS_PERSISTENT(ds_umts_pdp_profile_ptr)) 
    {
      /*-------------------------------------------------------------------
        Init the fully qualified profile file name to pdp_profiles directory
      -------------------------------------------------------------------*/
      if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
        return DS_UMTS_PDP_FAIL;

      /*-------------------------------------------------------------------
        Open the file for read / write
      -------------------------------------------------------------------*/
      DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ", 
                              ds_umts_pdp_fully_qual_profile_file_name);
    
      fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                            O_RDWR);
      if ( fs_handle == -1)
      {
        DS_3GPP_MSG3_ERROR("efs_open failed for %d errno=%d.", fs_handle, efs_errno, 0);
        return DS_UMTS_PDP_ACCESS_ERROR;
      }

      /*-------------------------------------------------------------------
        Set the file offset to the start of DS_UMTS_PDP_PROFILE_USER_APP_DATA_OFFSET information
      -------------------------------------------------------------------*/
      if (efs_lseek(fs_handle,
                    DS_UMTS_PDP_PROFILE_USER_APP_DATA_OFFSET,
                    SEEK_SET) == -1)
      {
        ERR( "efs_lseek failed for %d errno=%d", fs_handle, efs_errno, 0 );
        return DS_UMTS_PDP_ACCESS_ERROR;
      }

      DS_3GPP_MSG3_ERROR("user_app_data written is %d", user_app_data, 0, 0);
      /*-------------------------------------------------------------------
        write the profile for mnc
      -------------------------------------------------------------------*/
      if (ds_umts_pdp_efs_write(fs_handle,
                                (void *)&user_app_data,
                                sizeof(uint32)) == FALSE) 
      {
        return DS_UMTS_PDP_ACCESS_ERROR;
      }

       /*---------------------------------------------------------------
        Close the file
      -------------------------------------------------------------------*/
      if (efs_close (fs_handle) == -1)
      {
        ERR("efs_close failed for %d errno = %d", fs_handle, efs_errno, 0);
        return DS_UMTS_PDP_ACCESS_ERROR;
      }
    }

    /*-------------------------------------------------------------------
      Update the cache table with the new read_only_flag value.
    -------------------------------------------------------------------*/
    DS_UMTS_PDP_TASKLOCK_NON_L4();
    memscpy( (void*)&ds_umts_pdp_profile_ptr->user_app_data,
	     sizeof(uint32),
            (void*)&user_app_data,
            sizeof(uint32) );
    DS_UMTS_PDP_TASKFREE_NON_L4();

    return DS_UMTS_PDP_SUCCESS;
}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_SUBSCRIPTION_ID_INTERNAL
===========================================================================*/
/** 
  This function sets the "subscription id" field in the profile to
  the specified value.

  @profile_number            : profile number
  @subs_id                   : ds_umts_pdp_subs_e_type value being passed in for the
                               subs_id

DEPENDENCIES
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in accessing the file system

*/
ds_umts_pdp_profile_status_etype 
ds_umts_set_pdp_profile_subscription_id_internal
(
  uint16   profile_number,
  ds_umts_pdp_subs_e_type   subs_id
)
{
    int                             fs_handle;   
    ds_umts_pdp_profile_type        *ds_umts_pdp_profile_ptr;
    boolean read_only_flag;
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

    /*-------------------------------------------------------------------
      Check if we have been initialized & the input paramaters are correct
      A valid profile need not be defined at this time to set the name    
    -------------------------------------------------------------------*/
    DS_UMTS_PDP_CHECK_INIT_FLAG();
    if(  ds_umts_pdp_check_profile_number(profile_number,subs_id) == 
       DS_UMTS_PDP_INVALID_PROFILE_NUM )
      return DS_UMTS_PDP_INVALID_PROFILE_NUM; 

    DS_UMTS_PDP_CHECK_READ_ONLY_FLAG(profile_number,subs_id,read_only_flag);

    /*-------------------------------------------------------------------
      Initialize the variables for fs_open                   
    -------------------------------------------------------------------*/

    ds_umts_pdp_profile_ptr = 
      DS_UMTS_PDP_PROFILE_CACHE(subs_id,profile_number);

    if(DS_UMTS_PDP_PROFILE_IS_PERSISTENT(ds_umts_pdp_profile_ptr)) 
    {
      /*-------------------------------------------------------------------
        Init the fully qualified profile file name to pdp_profiles directory
      -------------------------------------------------------------------*/
      if(ds_umts_pdp_set_qualified_profile_file_name(profile_number,subs_id) == FALSE)
        return DS_UMTS_PDP_FAIL;

      /*-------------------------------------------------------------------
        Open the file for read / write
      -------------------------------------------------------------------*/
      DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "profile_name: %s ", 
                              ds_umts_pdp_fully_qual_profile_file_name);
    
      fs_handle = efs_open( ds_umts_pdp_fully_qual_profile_file_name, 
                            O_RDWR);
      if ( fs_handle == -1)
      {
        DS_3GPP_MSG3_ERROR("efs_open failed for %d errno=%d.", fs_handle, efs_errno, 0);
        return DS_UMTS_PDP_ACCESS_ERROR;
      }

      /*-------------------------------------------------------------------
        Set the file offset to the start of DS_UMTS_PDP_PROFILE_USER_APP_DATA_OFFSET information
      -------------------------------------------------------------------*/
      if (efs_lseek(fs_handle,
                    DS_PROFILE_3GPP_PROFILE_PARAM_SUBS_ID_OFFSET,
                    SEEK_SET) == -1)
      {
        ERR( "efs_lseek failed for %d errno=%d", fs_handle, efs_errno, 0 );
        return DS_UMTS_PDP_ACCESS_ERROR;
      }

      DS_3GPP_MSG3_ERROR("subs_id written is %d", subs_id, 0, 0);
      /*-------------------------------------------------------------------
        write the profile for mnc
      -------------------------------------------------------------------*/
      if (ds_umts_pdp_efs_write(fs_handle,
                                (void *)&subs_id,
                                sizeof(ds_umts_pdp_subs_e_type)) == FALSE) 
      {
        return DS_UMTS_PDP_ACCESS_ERROR;
      }

       /*---------------------------------------------------------------
        Close the file
      -------------------------------------------------------------------*/
      if (efs_close (fs_handle) == -1)
      {
        ERR("efs_close failed for %d errno = %d", fs_handle, efs_errno, 0);
        return DS_UMTS_PDP_ACCESS_ERROR;
      }
    }

    return DS_UMTS_PDP_SUCCESS;
}

/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_ROAMING_DISALLOWED_FLAG
===========================================================================*/
/**
  This function call the internal registry function to update the roaming
  disallowed flag setting for the profile specified with the value passed. 
  The update can be done ONLY for a valid context as it is part of the context 
  definition. It also updates the cache table with the new value.

  @param profile_number          [in] Profile number.
  @param roaming_disallowed_flag [in] Boolean value to set in the registry.

  @dependencies 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized).
  A valid context must have been defined for the profile number.

  @return
  DS_UMTS_PDP_SUCCESS -- Operation completed successfully. \n
  DS_UMTS_PDP_NOT_INITIALIZED -- Registry API library not initialized. \n
  DS_UMTS_PDP_INVALID_PROFILE_NUM -- Invalid profile number. \n
  DS_UMTS_PDP_ACCESS_ERROR -- Error in accessing the file system. \n
  DS_UMTS_PDP_CONTEXT_NOT_DEFINED -- Valid context not defined for profile. \n

*/ 
ds_umts_pdp_profile_status_etype 
  ds_umts_set_pdp_profile_roaming_disallowed_flag_per_subs
(
  uint16   profile_number,
  ds_umts_pdp_subs_e_type          subs_id,
  boolean  roaming_disallowed_flag
);
/*===========================================================================
FUNCTION DS_UMTS_SET_PDP_PROFILE_ALL_DATA

DESCRIPTION
  This function is used to calls the internal registry function to set all
  the parameters associated with a profile. The profile_data_ptr is passed
  in. The profile_data_ptr should atleast have a valid context for this
  function to update the profile parameters.
  
  NOTE : The name of the profile is overwritten with the value passed in 
  the profile

PARAMETERS 
  profile_number  : profile number
  profile_data_ptr    : pointer to the profile data parameters

DEPENDENCIES 
  ds_umts_pdp_registry_init_flag should be set(we should have been initialized)

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS              : the operation completed successfully
  DS_UMTS_PDP_NOT_INITIALIZED      : Registry API library not initialized
  DS_UMTS_PDP_INVALID_PROFILE_NUM  : Invalid profile number
  DS_UMTS_PDP_ACCESS_ERROR         : Error in updating the file
  DS_UMTS_PDP_CONTEXT_NOT_DEFINED  : The parameter did not contain a valid context

SIDE EFFECTS 
  none
===========================================================================*/
ds_umts_pdp_profile_status_etype  ds_umts_set_pdp_profile_all_data_per_subs
(
  uint16                   profile_number, 
  ds_umts_pdp_subs_e_type          subs_id,
  ds_umts_pdp_profile_type *profile_data_ptr 
);

#endif /*  (FEATURE_DATA_WCDMA_PS) || defined(FEATURE_GSM_GPRS) || defined(FEATURE_DATA_LTE)) */
