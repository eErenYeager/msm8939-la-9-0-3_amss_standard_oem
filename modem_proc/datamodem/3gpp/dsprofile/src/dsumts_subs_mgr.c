
/*===========================================================================

                        D S U M T S  S U B S  M G R 

                          
GENERAL DESCRIPTION
  This file contains data and function definitions specific to Dual Slot
  Dual Standby Feature. This modeule is included in PDP Profile registry 
  if the Dual SIM feature is enabled
  
INITIALIZATION AND SEQUENCING REQUIREMENTS
  dsumts_subs_mgr_init() should be called at task / system startup. 

 
Copyright (c) 2014 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.
===========================================================================*/


/*===========================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/3gpp/dsprofile/src/dsumts_subs_mgr.c#2 $ 
  $DateTime: 2015/12/07 00:52:58 $ $Author: teralak $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/29/11   ttv     Merged the fix for crash due to invalid EFS contents.
02/16/11   ttv     Added DSDS changes for consolidated profile family.
11/22/10   ttv     Creation of file.
===========================================================================*/


/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "datamodem_variation.h"
#include "customer.h"
#include "comdef.h"

#include <stringl/stringl.h>

#if defined(FEATURE_DATA_WCDMA_PS) || defined(FEATURE_GSM_GPRS)
#include "msg.h"
#include "fs_public.h"
#include "fs_sys_types.h"
#include "cm.h"
#include "dsumts_subs_mgr.h"
#include "dsumtspdpreg.h"
#include "dsumtspdpregint.h"
#include "ds_3gppi_utils.h"
#include "amssassert.h"
#include "modem_mem.h"

/*===========================================================================

                      PUBLIC DATA DECLARATIONS

===========================================================================*/



/*===========================================================================
            LOCAL DEFINITIONS AND DECLARATIONS FOR MODULE
This section contains local definitions for constants, macros, types,
variables and other items needed by this module.

===========================================================================*/
/*---------------------------------------------------------------------------
  Maximum number of families.
---------------------------------------------------------------------------*/
/*===========================================================================
                    Local Function Definitions
===========================================================================*/
/*===========================================================================
                    Forward Function Definitions
===========================================================================*/

/*===========================================================================
FUNCTION DSUMTS_SUBS_MGR_INIT

DESCRIPTION
  This function initializes the DS Subscription manager. It creates the mapping 
  table in EFS with default profile numbers for embedded/tethered calls.
   
PARAMETERS 
  none

DEPENDENCIES 
  Should be called only ONCE during startup.

RETURN VALUE 
  DS_UMTS_PDP_SUCCESS  - initialization was successfull
  DS_UMTS_PDP_FAIL     - initialization failed

SIDE EFFECTS 
  None
===========================================================================*/
ds_umts_pdp_profile_status_etype dsumts_subs_mgr_init(void)
{
 return DS_UMTS_PDP_SUCCESS;
}/* dsumts_subs_mgr_init */

/*===========================================================================

FUNCTION DSUMTS_SUBS_MGR_GET_SUBS_ID

DESCRIPTION

  This function returns the subscription ID internal to Data services that maps
  to the id from CM.
  CM : DS subscription ID mapping
  SYS_MODEM_AS_ID_1 : DS_UMTS_PDP_SUBSCRIPTION_1
  SYS_MODEM_AS_ID_2 : DS_UMTS_PDP_SUBSCRIPTION_2
  other             : DS_UMTS_PDP_SUBSCRIPTION_NONE


PARAMETERS
  as_id   - Subscription ID from CM

DEPENDENCIES
  None

RETURN VALUE
 pdp subscription type

SIDE EFFECTS
  None

===========================================================================*/
ds_umts_pdp_subs_e_type dsumts_subs_mgr_get_subs_id
(
  sys_modem_as_id_e_type as_id
)
{
  switch(as_id) {
    case SYS_MODEM_AS_ID_1:
      return DS_UMTS_PDP_SUBSCRIPTION_1;

    case SYS_MODEM_AS_ID_2:
      return DS_UMTS_PDP_SUBSCRIPTION_2;

#ifdef FEATURE_TRIPLE_SIM
    case SYS_MODEM_AS_ID_3:
     return DS_UMTS_PDP_SUBSCRIPTION_3; 
#endif /* FEATURE_TRIPLE_SIM */

    default:
      return DS_UMTS_PDP_SUBSCRIPTION_NONE;
  }
}/* dsumts_subs_mgr_get_subs_id */

/*===========================================================================

FUNCTION DSUMTS_SUBS_MGR_PROCESS_SUBS_CHANGE_EVENT

DESCRIPTION

  This function processes the events from CM that conveys a change in active
  subscription. Main processing is done in DSMGR. This function has the 
  umtsps specific additional processing

PARAMETERS
  as_id   - Active subscription ID from CM

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
void dsumts_subs_mgr_process_subs_change_event
(
  sys_modem_as_id_e_type as_id
)
{
  return;
}/* dsumts_subs_mgr_process_subs_change_event */

/*===========================================================================
FUNCTION DSUMTS_SUBS_MGR_GET_SUBS_ID_FROM_DB_SUBS_ID 
 
DESCRIPTION

  This function returns ds_umts_pdp_subs_e_type from 
  ds_profile_db_subs_type


PARAMETERS
  uint32   - ps policy

DEPENDENCIES
  None

RETURN VALUE
 ds_umts_pdp_subs_e_type   -- Internal Subscription ID

SIDE EFFECTS
  None

===========================================================================*/
ds_umts_pdp_subs_e_type dsumts_subs_mgr_get_subs_id_from_policy_subs_id
(
   uint32 subs_id
)
{
   return (ds_umts_pdp_subs_e_type)(subs_id - 1);
}/*  dsumts_subs_mgr_get_subs_id_from_policy_subs_id */


#endif /*  (FEATURE_DATA_WCDMA_PS) || defined(FEATURE_GSM_GPRS) */
