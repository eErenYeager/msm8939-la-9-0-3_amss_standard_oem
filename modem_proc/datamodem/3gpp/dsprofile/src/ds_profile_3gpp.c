/******************************************************************************
  @file    ds_profile_3gpp.c
  @brief   

  DESCRIPTION
  Tech specific implementation of 3GPP Profile Management  

  INITIALIZATION AND SEQUENCING REQUIREMENTS
  N/A

  ---------------------------------------------------------------------------
  Copyright (C) 2010-2014 QUALCOMM technologies Incorporated.
  All Rights Reserved. QUALCOMM Proprietary and Confidential.
  ---------------------------------------------------------------------------
******************************************************************************/
/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/3gpp/dsprofile/src/ds_profile_3gpp.c#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
11/27/12   vs      Removed ds_profile ENTRY and EXIT log messages
01/27/12   nd      Cleaning redundant param validation in set functions.
09/13/11   ttv     Changes to add src_stat_desc as profile parameter.
05/17/11   ss      Added profile parameter validation for pdp_type.
02/28/11   hs      Added changes to support APN class in profile
02/24/11   ttv     Fixed compilation warnings.
01/26/11   ars     Added support for apn_disabled_flag and inactivity_timer_val
01/17/11   ss      Added support for Iface linger.
09/28/10   ss      NULL terminating variable length strings in Get APIs.
09/03/10   ss      Fixed issue in length field of variable length parameters 
                   updated wrongly in ds_profile_3gpp get APIs.
08/24/10   sa      Added support for zero length param.
02/07/10   ss      Reset string type PDP profile parameters before updating new 
                   value.
06/28/10   ss      Fixed compiler warnings.
05/17/10   hs      Added APN disable and PDN inactivity feature support.
04/30/10   ars     Made modem_mem_alloc/free and str fns platform independent
10/15/09   vk      Added support for LTE params
09/30/09   mg      Created the module. First version of the file.
===========================================================================*/
#include "datamodem_variation.h"
#include "ds_profile_3gppi.h"
#include "ds_profile_os.h"
#include "dsumtspdpreg.h"
#include "dsumtspsqos.h"

#include "ds_3gpp_qos.h"
#include "ds_3gpp_pdn_context.h"
#include "dstask_v.h"

/*---------------------------------------------------------------------------
                       UTILITY MACROS
---------------------------------------------------------------------------*/

/*lint -save -e641*/

/* Macro to check info (for get_param function)
  (info->buf not NULL and validate info->len) */
#define GET_INFO_IS_VALID( info, max_len ) \
  ( (info != NULL) && (info->buf != NULL) && (info->len >= max_len) )

/* Macro to check info (for set_param function)
  (info->buf not NULL and validate info->len) */
#define SET_INFO_IS_VALID( info, max_len ) \
  ( (info->buf != NULL) && ( (info->len > 0) && (info->len <= max_len) ) )

/* 3GPP QoS Parameters */
#define DS_PROFILE_3GPP_QOS_PARAM_MAX_UL_BITRATE   QOS_MAX_UL_KBIT_RATE
#define DS_PROFILE_3GPP_QOS_PARAM_MAX_DL_BITRATE   QOS_MAX_DL_KBIT_RATE

#define DS_PROFILE_3GPP_QOS_PARAM_MAX_GTD_UL_BITRATE  QOS_MAX_UL_KBIT_RATE
#define DS_PROFILE_3GPP_QOS_PARAM_MAX_GTD_DL_BITRATE  QOS_MAX_DL_KBIT_RATE

#define DS_PROFILE_3GPP_QOS_PARAM_MAX_SDU_SIZE     QOS_PARAM_MAX_SDU_SIZE

#define DS_PROFILE_3GPP_QOS_PARAM_MAX_TRANS_DELAY  QOS_PARAM_MAX_TRANS_DELAY

#define DS_PROFILE_3GPP_QOS_PARAM_MAX_THANDLE_PRIORITY  \
                                               QOS_PARAM_MAX_THANDLE_PRIORITY

#define DS_PROFILE_3GPP_GPRS_QOS_PARAM_MAX_RELIABILITY_CLASS  \
                                               QOS_PARAM_MAX_RELIABILITY_CLASS

#define DS_PROFILE_3GPP_GPRS_QOS_PARAM_MAX_DELAY_CLASS        \
                                               QOS_PARAM_MAX_DELAY_CLASS

#define DS_PROFILE_3GPP_GPRS_QOS_PARAM_MAX_PRECEDENCE_CLASS   \
                                               QOS_PARAM_MAX_PRECEDENCE_CLASS

#define DS_PROFILE_3GPP_GPRS_QOS_PARAM_MAX_PEAK_THROUGHPUT_CLASS   \
                                            QOS_PARAM_MAX_PEAK_THROUGHPUT_CLASS

#define DS_PROFILE_3GPP_GPRS_QOS_PARAM_MAX_MEAN_THROUGHPUT_CLASS   \
                                            QOS_PARAM_MAX_MEAN_THROUGHPUT_CLASS

/* LTE QoS parameters */
#define DS_PROFILE_3GPP_LTE_QOS_PARAM_MAX_UL_BITRATE   QOS_LTE_MAX_UL_KBIT_RATE
#define DS_PROFILE_3GPP_LTE_QOS_PARAM_MAX_DL_BITRATE   QOS_LTE_MAX_DL_KBIT_RATE

#define DS_PROFILE_3GPP_LTE_QOS_PARAM_MAX_GTD_UL_BITRATE  QOS_LTE_MAX_UL_KBIT_RATE
#define DS_PROFILE_3GPP_LTE_QOS_PARAM_MAX_GTD_DL_BITRATE  QOS_LTE_MAX_DL_KBIT_RATE

/*-----------------------------------------------------------------------------
         LOCAL definitions and declarations for module
-----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
          3GPP PARAMS Table internal to DSI_PROFILE_3GPP module
-----------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------
  List of callback function tables
---------------------------------------------------------------------------*/
list_type ds_profile_3gpp_callback_table_list;


/*----------------------------------------------------------------------------- 
  Table for parameter and the size of the parameter required in get/set
  functions 
-----------------------------------------------------------------------------*/
static dsi_profile_params_desc_type ds_profile_3gpp_profile_params_desc_tbl[] = {
  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_INVALID, 
   0},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_PROFILE_NAME, 
   (DS_PROFILE_3GPP_MAX_PROFILE_NAME_LEN+1)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_PDP_CONTEXT_PDP_TYPE, 
   sizeof(ds_profile_3gpp_pdp_type_enum_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_PDP_CONTEXT_H_COMP, 
   sizeof(ds_profile_3gpp_pdp_header_comp_e_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_PDP_CONTEXT_D_COMP, 
   sizeof(ds_profile_3gpp_pdp_data_comp_e_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_PDP_CONTEXT_APN, 
   (DS_PROFILE_3GPP_MAX_APN_STRING_LEN+1)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_DNS_ADDR_V4_PRIMARY, 
   sizeof(ds_profile_3gpp_pdp_addr_type_ipv4)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_DNS_ADDR_V4_SECONDARY, 
   sizeof(ds_profile_3gpp_pdp_addr_type_ipv4)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_UMTS_REQ_QOS, 
   sizeof(ds_profile_3gpp_3gpp_qos_params_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_UMTS_MIN_QOS, 
   sizeof(ds_profile_3gpp_3gpp_qos_params_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_GPRS_REQ_QOS, 
   sizeof(ds_profile_3gpp_gprs_qos_params_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_GPRS_MIN_QOS, 
   sizeof(ds_profile_3gpp_gprs_qos_params_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_AUTH_USERNAME, 
   (DS_PROFILE_3GPP_MAX_QCPDP_STRING_LEN+1)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_AUTH_PASSWORD, 
   (DS_PROFILE_3GPP_MAX_QCPDP_STRING_LEN+1)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_AUTH_TYPE, 
   sizeof(ds_profile_3gpp_auth_pref_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_PDP_CONTEXT_PDP_ADDR_V4, 
   sizeof(ds_profile_3gpp_pdp_addr_type_ipv4)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_PCSCF_REQ_FLAG, 
   sizeof(ds_profile_3gpp_request_pcscf_address_flag_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_PDP_CONTEXT_TE_MT_ACCESS_CTRL_FLAG, 
   sizeof(ds_profile_3gpp_pdp_access_control_e_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_PCSCF_DHCP_REQ_FLAG, 
   sizeof(ds_profile_3gpp_request_pcscf_address_using_dhcp_flag_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_IM_CN_FLAG, 
   sizeof(ds_profile_3gpp_im_cn_flag_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_TFT_FILTER_ID1, 
   (sizeof(ds_profile_3gpp_tft_params_type))},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_TFT_FILTER_ID2, 
   (sizeof(ds_profile_3gpp_tft_params_type))},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_PDP_CONTEXT_NUMBER, 
   sizeof(ds_profile_3gpp_pdp_context_number_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_PDP_CONTEXT_SECONDARY_FLAG, 
   sizeof(ds_profile_3gpp_pdp_context_secondary_flag_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_PDP_CONTEXT_PRIMARY_ID, 
   sizeof(ds_profile_3gpp_pdp_context_primary_id_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_PDP_CONTEXT_PDP_ADDR_V6, 
   sizeof(ds_profile_3gpp_pdp_addr_type_ipv6)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_UMTS_REQ_QOS_EXTENDED, 
   (sizeof(ds_profile_3gpp_3gpp_qos_params_type) - sizeof(boolean))},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_UMTS_MIN_QOS_EXTENDED, 
   (sizeof(ds_profile_3gpp_3gpp_qos_params_type) - sizeof(boolean))},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_DNS_ADDR_V6_PRIMARY, 
   sizeof(ds_profile_3gpp_pdp_addr_type_ipv6)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_DNS_ADDR_V6_SECONDARY, 
   sizeof(ds_profile_3gpp_pdp_addr_type_ipv6)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_IPV4_ADDR_ALLOC, 
   sizeof(ds_profile_3gpp_pdp_ipv4_addr_alloc_e_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_LTE_REQ_QOS, 
   sizeof(ds_profile_3gpp_lte_qos_params_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_LINGER_PARAMS, 
   sizeof(ds_profile_3gpp_linger_params_type)},
  
  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_APN_DISABLE_FLAG, 
   sizeof(ds_profile_3gpp_apn_disable_flag_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_INACTIVITY_TIMER_VAL, 
   sizeof(ds_profile_3gpp_inactivity_timer_val_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_APN_CLASS, 
   sizeof(ds_profile_3gpp_apn_class_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_SRC_STAT_DESC_REQ, 
   sizeof(uint32)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_SRC_STAT_DESC_MIN, 
   sizeof(uint32)},

  {(ds_profile_identifier_type)
    DS_PROFILE_3GPP_PROFILE_PARAM_APN_BEARER,
    sizeof(uint8)},

   {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_EMERGENCY_CALLS_SUPPORTED, 
   sizeof(ds_profile_3gpp_emergency_calls_are_supported)},

   {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_OPERATOR_RESERVED_PCO, 
   sizeof(uint16)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_MCC, 
   sizeof(uint16)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_MNC, 
   sizeof(ds_profile_3gpp_mnc_type)},

   {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_MAX_PDN_CONN_PER_BLOCK, 
   sizeof(uint16)},

    {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_MAX_PDN_CONN_TIMER, 
   sizeof(uint16)},

    {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PDN_REQ_WAIT_TIMER, 
   sizeof(uint16)},

    {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_USER_APP_DATA, 
   sizeof(uint32)},

    {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_ROAMING_DISALLOWED, 
   sizeof(boolean)
    },

   {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_PDN_DISCON_WAIT_TIME, 
   sizeof(uint8)
    }
};

/*---------------------------------------------------------------------------
              3GPP PARAMS ACCCESSOR/MUTATOR ROUTINES & UTILS
---------------------------------------------------------------------------*/

/*===========================================================================
FUNCTION dsi_profile_set_ group of functions

DESCRIPTION
  This set of accessor functions are used to set corresponding Profile 
  parameter

PARAMETERS
  profile : ptr to 3GPP Profile blob 
  mask    : mask to identify Profile parameter
  info    : ptr to mem containing data to be written to Profile blob
  
DEPENDENCIES 
  
RETURN VALUE 
  
SIDE EFFECTS 
===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp_set_profile_name(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  uint8 index = 0;

  index = dsi_profile_3gpp_get_index_from_ident( 
                                   DS_PROFILE_3GPP_PROFILE_PARAM_PROFILE_NAME );

  /* clear the entire destination buffer before memcpy of requested data */
  memset((void*)((dsi_profile_3gpp_type *)profile)->prf->profile_name, 0, 
        ds_profile_3gpp_profile_params_desc_tbl[index].len ); 

  memscpy((void*)((dsi_profile_3gpp_type *)profile)->prf->profile_name,
                  sizeof(((dsi_profile_3gpp_type *)profile)->prf->profile_name),
                  info->buf, info->len ); 
                 ((dsi_profile_3gpp_type *)profile)->mask |= mask;

  DS_PROFILE_LOGD("set profile_name:%s ", 
                  (char*)(((dsi_profile_3gpp_type *)profile)->prf->profile_name));
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_pdp_context_pdp_type(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  ds_profile_3gpp_pdp_type_enum_type  pdp_type_input = DS_PROFILE_3GPP_PDP_IP;

  pdp_type_input 
    = (ds_profile_3gpp_pdp_type_enum_type)( *((ds_profile_3gpp_pdp_type_enum_type *)(info->buf)));


  if ( pdp_type_input > DS_PROFILE_3GPP_PDP_IPV4V6 )
  {
    DS_PROFILE_LOGE("Invalid pdp_type %d specified", pdp_type_input);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->context.pdp_type, 
  	      sizeof(((dsi_profile_3gpp_type *)profile)->prf->context.pdp_type),
              info->buf, info->len ); 
              ((dsi_profile_3gpp_type *)profile)->mask |= mask;

  DS_PROFILE_LOGD("set pdp_context_pdp_type:%d ",
                  ((dsi_profile_3gpp_type *) profile)->prf->context.pdp_type);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_pdp_context_h_comp(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  ds_profile_3gpp_pdp_header_comp_e_type  h_comp 
                                         = DS_PROFILE_3GPP_PDP_HEADER_COMP_OFF;
  h_comp = (ds_profile_3gpp_pdp_header_comp_e_type)(* ((ds_profile_3gpp_pdp_header_comp_e_type *)(info->buf)));

  if (h_comp > DS_PROFILE_3GPP_PDP_HEADER_COMP_RFC3095)
  {
    DS_PROFILE_LOGE("Invalid h_comp value: %d passed as input", h_comp);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }
  
  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->context.h_comp, 
  	      sizeof(((dsi_profile_3gpp_type *)profile)->prf->context.h_comp),
              info->buf, info->len ); 
              ((dsi_profile_3gpp_type *)profile)->mask |= mask;

  DS_PROFILE_LOGD("set pdp_context h_comp:%d ",
  	              ((dsi_profile_3gpp_type *)profile)->prf->context.h_comp);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_pdp_context_d_comp(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  ds_profile_3gpp_pdp_data_comp_e_type   d_comp 
                                         = DS_PROFILE_3GPP_PDP_DATA_COMP_OFF;

  d_comp = (ds_profile_3gpp_pdp_data_comp_e_type)(* ((ds_profile_3gpp_pdp_data_comp_e_type *)(info->buf)));

  if (d_comp > DS_PROFILE_3GPP_PDP_DATA_COMP_V44)
  {
    DS_PROFILE_LOGE("Invalid d_comp value: %d passed as input", d_comp);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->context.d_comp, 
  	    sizeof(((dsi_profile_3gpp_type *)profile)->prf->context.d_comp),
            info->buf, info->len ); 
            ((dsi_profile_3gpp_type *)profile)->mask |= mask;

  DS_PROFILE_LOGD("set pdp_context d_comp:%d ", 
  	              ((dsi_profile_3gpp_type *)profile)->prf->context.d_comp);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_pdp_context_apn(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  uint8 index = 0;

  /* clear the entire destination buffer before memcpy of requested data */
  index = dsi_profile_3gpp_get_index_from_ident( 
                                DS_PROFILE_3GPP_PROFILE_PARAM_PDP_CONTEXT_APN );

  if ( (info->len != 0) &&
      !(SET_INFO_IS_VALID(info,ds_profile_3gpp_profile_params_desc_tbl[index].len)))
  {
    DS_PROFILE_LOGE( "_3gpp_set_pdp_context_apn: Invalid len", 0 );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }
  memset((void*)((dsi_profile_3gpp_type *)profile)->prf->context.apn, 0, 
        ds_profile_3gpp_profile_params_desc_tbl[index].len ); 

  if ( info->len != 0 )
  {
    memscpy((void*)((dsi_profile_3gpp_type *)profile)->prf->context.apn, 
  		    sizeof(((dsi_profile_3gpp_type *)profile)->prf->context.apn),
                    info->buf, info->len ); 
    DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH,
                                "set pdp_context apn:%s",
	                      (char*)(((dsi_profile_3gpp_type *)profile)->prf->context.apn));
  }
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_dns_addr_v4_primary(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  ds_profile_3gpp_ip_version_enum_type * ip_vsn_p;

  ip_vsn_p = &(((dsi_profile_3gpp_type *)profile)->prf->dns_addr_primary_ip_vsn);

  if ( (*ip_vsn_p == DS_PROFILE_3GPP_IP_V6) ||
       (*ip_vsn_p == DS_PROFILE_3GPP_IP_V4V6) ) 
  {
    *ip_vsn_p = DS_PROFILE_3GPP_IP_V4V6;
  }
  else if (*ip_vsn_p == DS_PROFILE_3GPP_IP_V4)
  {
    *ip_vsn_p = DS_PROFILE_3GPP_IP_V4;
  }
  else
  {
    DS_PROFILE_LOGE("Invalid IP version: %d", *ip_vsn_p);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->dns_addr.primary_dns_addr.ds_profile_3gpp_pdp_addr_ipv4, 
  	    sizeof(((dsi_profile_3gpp_type *)profile)->prf->dns_addr.primary_dns_addr.ds_profile_3gpp_pdp_addr_ipv4),
            info->buf, info->len ); 
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;

  DS_PROFILE_LOGD("set primary_dns_addrIPV4:%d ", 
                   ((dsi_profile_3gpp_type *)profile)->prf->dns_addr.primary_dns_addr.ds_profile_3gpp_pdp_addr_ipv4);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_dns_addr_v6_primary(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  ds_profile_3gpp_ip_version_enum_type * ip_vsn_p;

  ip_vsn_p = &(((dsi_profile_3gpp_type *)profile)->prf->dns_addr_primary_ip_vsn);

  if ( (*ip_vsn_p == DS_PROFILE_3GPP_IP_V4) ||
       (*ip_vsn_p == DS_PROFILE_3GPP_IP_V4V6) ) 
  {
    *ip_vsn_p = DS_PROFILE_3GPP_IP_V4V6;
  }
  else if (*ip_vsn_p == DS_PROFILE_3GPP_IP_V6)
  {
    *ip_vsn_p = DS_PROFILE_3GPP_IP_V6;
  }
  else
  {
    DS_PROFILE_LOGE("Invalid IP version: %d", *ip_vsn_p);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->dns_addr.primary_dns_addr.ds_profile_3gpp_pdp_addr_ipv6,
  	  sizeof(((dsi_profile_3gpp_type *)profile)->prf->dns_addr.primary_dns_addr.ds_profile_3gpp_pdp_addr_ipv6),
          info->buf, info->len ); 
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;

  DS_PROFILE_LOGD("set primary_dns_addr IPV6:successful", 0);                    
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_dns_addr_v4_secondary(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  ds_profile_3gpp_ip_version_enum_type * ip_vsn_p;

  ip_vsn_p = &(((dsi_profile_3gpp_type *)profile)->prf->dns_addr_primary_ip_vsn);

  if ( (*ip_vsn_p == DS_PROFILE_3GPP_IP_V6) ||
       (*ip_vsn_p == DS_PROFILE_3GPP_IP_V4V6) ) 
  {
    *ip_vsn_p = DS_PROFILE_3GPP_IP_V4V6;
  }
  else if (*ip_vsn_p == DS_PROFILE_3GPP_IP_V4)
  {
    *ip_vsn_p = DS_PROFILE_3GPP_IP_V4;
  }
  else
  {
    DS_PROFILE_LOGE("Invalid IP version: %d", *ip_vsn_p);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->dns_addr.secondary_dns_addr.ds_profile_3gpp_pdp_addr_ipv4, 
  	      sizeof(((dsi_profile_3gpp_type *)profile)->prf->dns_addr.secondary_dns_addr.ds_profile_3gpp_pdp_addr_ipv4),
              info->buf, info->len ); 
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;

 DS_PROFILE_LOGD("set secondary_dns_addr IPV4: %d", 
                    ((dsi_profile_3gpp_type *)profile)->prf->dns_addr.secondary_dns_addr.ds_profile_3gpp_pdp_addr_ipv4);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_dns_addr_v6_secondary(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  ds_profile_3gpp_ip_version_enum_type * ip_vsn_p;

  ip_vsn_p = &(((dsi_profile_3gpp_type *)profile)->prf->dns_addr_secondary_ip_vsn);

  if ( (*ip_vsn_p == DS_PROFILE_3GPP_IP_V4) ||
       (*ip_vsn_p == DS_PROFILE_3GPP_IP_V4V6) ) 
  {
    *ip_vsn_p = DS_PROFILE_3GPP_IP_V4V6;
  }
  else if (*ip_vsn_p == DS_PROFILE_3GPP_IP_V6)
  {
    *ip_vsn_p = DS_PROFILE_3GPP_IP_V6;
  }
  else
  {
    DS_PROFILE_LOGE("Invalid IP version: %d", *ip_vsn_p);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->dns_addr.secondary_dns_addr.ds_profile_3gpp_pdp_addr_ipv6, 
  	  sizeof(((dsi_profile_3gpp_type *)profile)->prf->dns_addr.secondary_dns_addr.ds_profile_3gpp_pdp_addr_ipv6),
          info->buf, info->len ); 
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;

  DS_PROFILE_LOGD("set secondary_dns_addr IPV6: successful", 0);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_umts_req_qos(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  ds_profile_3gpp_3gpp_qos_params_type    qos_request_3gpp;

  memset(&qos_request_3gpp, 0, sizeof(qos_request_3gpp));

  memscpy((void*)(&qos_request_3gpp),
  	              sizeof(qos_request_3gpp),
  	              info->buf, info->len);

  if (qos_request_3gpp.traffic_class > DS_PROFILE_3GPP_TC_BACKGROUND)
  {
    DS_PROFILE_LOGE("Invalid traffic class: %d in Req QoS parameters",
                     qos_request_3gpp.traffic_class);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  if ( (qos_request_3gpp.max_ul_bitrate > DS_PROFILE_3GPP_QOS_PARAM_MAX_UL_BITRATE) ||
       (qos_request_3gpp.max_dl_bitrate > DS_PROFILE_3GPP_QOS_PARAM_MAX_DL_BITRATE) ||
       (qos_request_3gpp.gtd_ul_bitrate > DS_PROFILE_3GPP_QOS_PARAM_MAX_GTD_UL_BITRATE) ||
       (qos_request_3gpp.gtd_dl_bitrate > DS_PROFILE_3GPP_QOS_PARAM_MAX_GTD_DL_BITRATE) )
  {
    DS_PROFILE_LOGE("Bitrate in Req QoS is greater than max allowable bitrate",
                     0);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  if (qos_request_3gpp.dlvry_order > DS_PROFILE_3GPP_DO_OFF)
  {
    DS_PROFILE_LOGE("Invalid Delivery ordr: %d in Req QoS",
                     qos_request_3gpp.dlvry_order);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  if (qos_request_3gpp.max_sdu_size > DS_PROFILE_3GPP_QOS_PARAM_MAX_SDU_SIZE)
  {
    DS_PROFILE_LOGE("Invalid Max SDU size: %d in Req QoS",
                     qos_request_3gpp.max_sdu_size);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  if (qos_request_3gpp.sdu_err > DS_PROFILE_3GPP_SDU_ERR_RATIO_MAX)
  {
    DS_PROFILE_LOGE("Invalid SDU err ratio: %d in Req QoS",
                     qos_request_3gpp.sdu_err);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  if (qos_request_3gpp.res_biterr > DS_PROFILE_3GPP_RESIDUAL_BER_6ENEG8)
  {
    DS_PROFILE_LOGE("Invalid Residual BER ratio: %d in Req QoS",
                     qos_request_3gpp.res_biterr);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  if (qos_request_3gpp.dlvr_err_sdu > DS_PROFILE_3GPP_DELIVER_ERR_SDU_NO_DELIVER)
  {
    DS_PROFILE_LOGE("Invalid Delivery err SDU: %d in Req QoS",
                     qos_request_3gpp.dlvr_err_sdu);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  if(qos_request_3gpp.trans_delay > DS_PROFILE_3GPP_QOS_PARAM_MAX_TRANS_DELAY)
  {
    DS_PROFILE_LOGE("Invalid trans delay: %d in Req QoS",
                     qos_request_3gpp.trans_delay);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  if(qos_request_3gpp.thandle_prio > DS_PROFILE_3GPP_QOS_PARAM_MAX_THANDLE_PRIORITY)
  {
    DS_PROFILE_LOGE("Invalid traffic handling priority: %d in Req QoS",
                     qos_request_3gpp.thandle_prio);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->qos_request_3gpp,
  	      sizeof(((dsi_profile_3gpp_type *)profile)->prf->qos_request_3gpp),
              info->buf, info->len ); 
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;

    DS_PROFILE_LOGD("set umts_req_qos:successful ",0);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_umts_min_qos(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  ds_profile_3gpp_3gpp_qos_params_type    qos_min_3gpp;

  memset(&qos_min_3gpp, 0, sizeof(qos_min_3gpp));

  memscpy((void*)(&qos_min_3gpp),
          sizeof(qos_min_3gpp),
          info->buf, info->len);

  if (qos_min_3gpp.traffic_class > DS_PROFILE_3GPP_TC_BACKGROUND)
  {
    DS_PROFILE_LOGE("Invalid traffic class: %d in Min QoS parameters",
                     qos_min_3gpp.traffic_class);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  if ( (qos_min_3gpp.max_ul_bitrate > DS_PROFILE_3GPP_QOS_PARAM_MAX_UL_BITRATE) ||
       (qos_min_3gpp.max_dl_bitrate > DS_PROFILE_3GPP_QOS_PARAM_MAX_DL_BITRATE) ||
       (qos_min_3gpp.gtd_ul_bitrate > DS_PROFILE_3GPP_QOS_PARAM_MAX_GTD_UL_BITRATE) ||
       (qos_min_3gpp.gtd_dl_bitrate > DS_PROFILE_3GPP_QOS_PARAM_MAX_GTD_DL_BITRATE) )
  {
    DS_PROFILE_LOGE("Bitrate in Min QoS is greater than max allowable bitrate",
                     0);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  if (qos_min_3gpp.dlvry_order > DS_PROFILE_3GPP_DO_OFF)
  {
    DS_PROFILE_LOGE("Invalid Delivery ordr: %d in Min QoS",
                     qos_min_3gpp.dlvry_order);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  if (qos_min_3gpp.max_sdu_size > DS_PROFILE_3GPP_QOS_PARAM_MAX_SDU_SIZE)
  {
    DS_PROFILE_LOGE("Invalid Max SDU size: %d in Min QoS",
                     qos_min_3gpp.max_sdu_size);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  if (qos_min_3gpp.sdu_err > DS_PROFILE_3GPP_SDU_ERR_RATIO_MAX)
  {
    DS_PROFILE_LOGE("Invalid SDU err ratio: %d in Min QoS",
                     qos_min_3gpp.sdu_err);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  if (qos_min_3gpp.res_biterr > DS_PROFILE_3GPP_RESIDUAL_BER_6ENEG8)
  {
    DS_PROFILE_LOGE("Invalid Residual BER ratio: %d in Min QoS",
                     qos_min_3gpp.res_biterr);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  if (qos_min_3gpp.dlvr_err_sdu > DS_PROFILE_3GPP_DELIVER_ERR_SDU_NO_DELIVER)
  {
    DS_PROFILE_LOGE("Invalid Delivery err SDU: %d in Min QoS",
                     qos_min_3gpp.dlvr_err_sdu);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  if(qos_min_3gpp.trans_delay > DS_PROFILE_3GPP_QOS_PARAM_MAX_TRANS_DELAY)
  {
    DS_PROFILE_LOGE("Invalid trans delay: %d in Min QoS",
                     qos_min_3gpp.trans_delay);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  if(qos_min_3gpp.thandle_prio > DS_PROFILE_3GPP_QOS_PARAM_MAX_THANDLE_PRIORITY)
  {
    DS_PROFILE_LOGE("Invalid traffic handling priority: %d in Min QoS",
                     qos_min_3gpp.thandle_prio);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->qos_minimum_3gpp, 
  	      sizeof(((dsi_profile_3gpp_type *)profile)->prf->qos_minimum_3gpp),  
              info->buf, info->len ); 
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;

    DS_PROFILE_LOGD("set umts_min_qos: successful ",0);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_gprs_req_qos(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  ds_profile_3gpp_gprs_qos_params_type    qos_request_gprs;
  
  memset(&qos_request_gprs, 0, sizeof(qos_request_gprs));

  memscpy((void*)(&qos_request_gprs),sizeof(qos_request_gprs), info->buf, info->len);

  if (qos_request_gprs.precedence > 
       DS_PROFILE_3GPP_GPRS_QOS_PARAM_MAX_PRECEDENCE_CLASS)
  {
    DS_PROFILE_LOGE("Invalid Precedence class: %d in gprs req qos",
                     qos_request_gprs.precedence);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  if (qos_request_gprs.delay > DS_PROFILE_3GPP_GPRS_QOS_PARAM_MAX_DELAY_CLASS)
  {
    DS_PROFILE_LOGE("Invalid Delay class: %d in gprs req qos",
                     qos_request_gprs.delay);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  if (qos_request_gprs.reliability > 
        DS_PROFILE_3GPP_GPRS_QOS_PARAM_MAX_RELIABILITY_CLASS)
  {
    DS_PROFILE_LOGE("Invalid Reliablilty class: %d in gprs req qos",
                     qos_request_gprs.reliability);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  if (qos_request_gprs.peak > 
       DS_PROFILE_3GPP_GPRS_QOS_PARAM_MAX_PEAK_THROUGHPUT_CLASS)
  {
    DS_PROFILE_LOGE("Invalid Peak throughput class: %d in gprs req qos",
                     qos_request_gprs.peak);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  if (qos_request_gprs.mean > 
       DS_PROFILE_3GPP_GPRS_QOS_PARAM_MAX_MEAN_THROUGHPUT_CLASS)
  {
    DS_PROFILE_LOGE("Invalid Mean throughput class: %d in gprs req qos",
                     qos_request_gprs.mean);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->qos_request_gprs, 
  	      sizeof(((dsi_profile_3gpp_type *)profile)->prf->qos_request_gprs),
              info->buf, info->len ); 
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;

    DS_PROFILE_LOGD("set gprs_req_qos:successful", 0);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_gprs_min_qos(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  ds_profile_3gpp_gprs_qos_params_type    qos_min_gprs;
  
  memset(&qos_min_gprs, 0, sizeof(qos_min_gprs));

  memscpy((void*)(&qos_min_gprs),sizeof(qos_min_gprs), info->buf, info->len);

  if (qos_min_gprs.precedence > 
       DS_PROFILE_3GPP_GPRS_QOS_PARAM_MAX_PRECEDENCE_CLASS)
  {
    DS_PROFILE_LOGE("Invalid Precedence class: %d in gprs min qos",
                     qos_min_gprs.precedence);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  if (qos_min_gprs.delay > DS_PROFILE_3GPP_GPRS_QOS_PARAM_MAX_DELAY_CLASS)
  {
    DS_PROFILE_LOGE("Invalid Delay class: %d in gprs min qos",
                     qos_min_gprs.delay);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  if (qos_min_gprs.reliability > 
        DS_PROFILE_3GPP_GPRS_QOS_PARAM_MAX_RELIABILITY_CLASS)
  {
    DS_PROFILE_LOGE("Invalid Reliablilty class: %d in gprs min qos",
                     qos_min_gprs.reliability);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  if (qos_min_gprs.peak > 
       DS_PROFILE_3GPP_GPRS_QOS_PARAM_MAX_PEAK_THROUGHPUT_CLASS)
  {
    DS_PROFILE_LOGE("Invalid Peak throughput class: %d in gprs min qos",
                     qos_min_gprs.peak);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  if (qos_min_gprs.mean > 
       DS_PROFILE_3GPP_GPRS_QOS_PARAM_MAX_MEAN_THROUGHPUT_CLASS)
  {
    DS_PROFILE_LOGE("Invalid Mean throughput class: %d in gprs min qos",
                     qos_min_gprs.mean);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->qos_minimum_gprs, 
  	      sizeof(((dsi_profile_3gpp_type *)profile)->prf->qos_minimum_gprs),
              info->buf, info->len ); 
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;

    DS_PROFILE_LOGD("set gprs_min_qos:successful", 0);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_auth_username(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  uint8 index = 0;

  /* clear the entire destination buffer before memcpy of requested data */
  index = dsi_profile_3gpp_get_index_from_ident( 
                                  DS_PROFILE_3GPP_PROFILE_PARAM_AUTH_USERNAME );

  if (( info->len != 0 ) &&
      !(SET_INFO_IS_VALID(info,ds_profile_3gpp_profile_params_desc_tbl[index].len)))
  {
    DS_PROFILE_LOGE( "_3gpp_set_auth_username: Invalid len", 0 );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }
  memset((void*)((dsi_profile_3gpp_type *)profile)->prf->auth.username, 0, 
        ds_profile_3gpp_profile_params_desc_tbl[index].len ); 

  if ( info->len !=0 )
  {
    memscpy((void*)((dsi_profile_3gpp_type *)profile)->prf->auth.username,
		    sizeof(((dsi_profile_3gpp_type *)profile)->prf->auth.username),
                    info->buf, info->len ); 
    DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH,
                                "set auth_username:%s",
	                      (char*)((dsi_profile_3gpp_type *)profile)->prf->auth.username);
  }
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_auth_password(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  uint8 index = 0;

  /* clear the entire destination buffer before memcpy of requested data */
  index = dsi_profile_3gpp_get_index_from_ident( 
                                  DS_PROFILE_3GPP_PROFILE_PARAM_AUTH_PASSWORD );

  if (( info->len != 0 ) &&
      !(SET_INFO_IS_VALID(info,ds_profile_3gpp_profile_params_desc_tbl[index].len)))
  {
    DS_PROFILE_LOGE( "_3gpp_set_auth_password: Invalid len", 0 );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }
  memset((void*)((dsi_profile_3gpp_type *)profile)->prf->auth.password, 0, 
        ds_profile_3gpp_profile_params_desc_tbl[index].len ); 

  if ( info->len !=0 )
  {
    memscpy((void*)((dsi_profile_3gpp_type *)profile)->prf->auth.password,
		    sizeof(((dsi_profile_3gpp_type *)profile)->prf->auth.password),
                    info->buf, info->len );
  }
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;
  DS_PROFILE_LOGD( "_3gpp_set_auth_password successfully", 0 );
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_auth_type(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  ds_profile_3gpp_auth_pref_type  auth_type 
                              = DS_PROFILE_3GPP_AUTH_PREF_PAP_CHAP_NOT_ALLOWED;

  auth_type = (ds_profile_3gpp_auth_pref_type)\
                          (*((ds_profile_3gpp_auth_pref_type *)(info->buf)));

  if (auth_type > DS_PROFILE_3GPP_AUTH_PREF_PAP_CHAP_ALLOWED)
  {
    DS_PROFILE_LOGE("Invalid auth_type: %d", auth_type);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->auth.auth_type, 
  	     sizeof(((dsi_profile_3gpp_type *)profile)->prf->auth.auth_type),
             info->buf, info->len ); 
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;

  DS_PROFILE_LOGD( "_3gpp_set_auth_type %d", 
                   ((dsi_profile_3gpp_type *)profile)->prf->auth.auth_type );
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_pdp_context_pdp_addr_v4(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  ds_profile_3gpp_ip_version_enum_type * ip_vsn_p;

  ip_vsn_p = &(((dsi_profile_3gpp_type *)profile)->prf->pdp_addr_ip_vsn);

  if ( (*ip_vsn_p == DS_PROFILE_3GPP_IP_V6) ||
       (*ip_vsn_p == DS_PROFILE_3GPP_IP_V4V6) ) 
  {
    *ip_vsn_p = DS_PROFILE_3GPP_IP_V4V6;
  }
  else if (*ip_vsn_p == DS_PROFILE_3GPP_IP_V4)
  {
    *ip_vsn_p = DS_PROFILE_3GPP_IP_V4;
  }
  else
  {
    DS_PROFILE_LOGE("Invalid ip_version: %d", *ip_vsn_p);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->context.pdp_addr.ds_profile_3gpp_pdp_addr_ipv4, 
  	      sizeof(((dsi_profile_3gpp_type *)profile)->prf->context.pdp_addr.ds_profile_3gpp_pdp_addr_ipv4),
              info->buf, info->len ); 
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;

    DS_PROFILE_LOGD("set ds_profile_3gpp_pdp_addr_ipv4: %d", 
                     ((dsi_profile_3gpp_type *)profile)->prf->context.pdp_addr.ds_profile_3gpp_pdp_addr_ipv4);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_pdp_context_pdp_addr_v6(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  ds_profile_3gpp_ip_version_enum_type * ip_vsn_p;

  ip_vsn_p = &(((dsi_profile_3gpp_type *)profile)->prf->pdp_addr_ip_vsn);

  if ( (*ip_vsn_p == DS_PROFILE_3GPP_IP_V4) ||
       (*ip_vsn_p == DS_PROFILE_3GPP_IP_V4V6) ) 
  {
    *ip_vsn_p = DS_PROFILE_3GPP_IP_V4V6;
  }
  else if (*ip_vsn_p == DS_PROFILE_3GPP_IP_V6)
  {
    *ip_vsn_p = DS_PROFILE_3GPP_IP_V6;
  }
  else
  {
    DS_PROFILE_LOGE("Invalid ip_version: %d", *ip_vsn_p);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->context.pdp_addr.ds_profile_3gpp_pdp_addr_ipv6,
  	      sizeof(((dsi_profile_3gpp_type *)profile)->prf->context.pdp_addr.ds_profile_3gpp_pdp_addr_ipv6),
              info->buf, info->len ); 
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;

  DS_PROFILE_LOGD("set ds_profile_3gpp_pdp_addr_ipv6: successful",0);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_pcscf_req_flag(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->request_pcscf_address_flag, 
        sizeof(((dsi_profile_3gpp_type *)profile)->prf->request_pcscf_address_flag),
        info->buf, info->len ); 
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;

  DS_PROFILE_LOGD("set request_pcscf_address_flag: %d", 
                     ((dsi_profile_3gpp_type *)profile)->prf->request_pcscf_address_flag);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_pdp_context_te_mt_access_ctrl_flag(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  ds_profile_3gpp_pdp_access_control_e_type   access_ctrl;
  access_ctrl = (*((ds_profile_3gpp_pdp_access_control_e_type *)(info->buf)));

  if (access_ctrl > DS_PROFILE_3GPP_PDP_ACCESS_CONTROL_PERMISSION)
  {
    DS_PROFILE_LOGE("Invalid access_Ctrl_flag: %d", access_ctrl);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->context.access_ctrl_flag, 
  	   sizeof(((dsi_profile_3gpp_type *)profile)->prf->context.access_ctrl_flag),
            info->buf, info->len ); 
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;

  DS_PROFILE_LOGD("set access_ctrl_flag: %d", 
                   ((dsi_profile_3gpp_type *)profile)->prf->context.access_ctrl_flag);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_pcscf_dhcp_req_flag(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->request_pcscf_address_using_dhcp_flag, 
  	sizeof(((dsi_profile_3gpp_type *)profile)->prf->request_pcscf_address_using_dhcp_flag),
        info->buf, info->len ); 
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;

  DS_PROFILE_LOGD("set request_pcscf_address_using_dhcp_flag: %d", 
                   ((dsi_profile_3gpp_type *)profile)->prf->request_pcscf_address_using_dhcp_flag);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_im_cn_flag(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->im_cn_flag,
  	   sizeof(((dsi_profile_3gpp_type *)profile)->prf->im_cn_flag),
           info->buf, info->len ); 
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;

  DS_PROFILE_LOGD("set im_cn_flag: %d", 
                   ((dsi_profile_3gpp_type *)profile)->prf->im_cn_flag);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_param_tft_filter_id1(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  ds_profile_3gpp_ip_version_enum_type   ip_version;

  ip_version = ((ds_profile_3gpp_tft_params_type *)(info->buf))->ip_version;

  if( (ip_version != DS_PROFILE_3GPP_IP_V4) &&
      (ip_version != DS_PROFILE_3GPP_IP_V6) )
  {
    DS_PROFILE_LOGE("Invalid ip_version: %d in TFT params", ip_version);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->tft[DS_PROFILE_3GPP_TFT_FILTER_ID1], 
  	  sizeof(((dsi_profile_3gpp_type *)profile)->prf->tft[DS_PROFILE_3GPP_TFT_FILTER_ID1]),
          info->buf, info->len ); 
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;

  DS_PROFILE_LOGD("set param_tft_filter_id1 sucessful",0);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_param_tft_filter_id2(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  ds_profile_3gpp_ip_version_enum_type   ip_version;

  ip_version = ((ds_profile_3gpp_tft_params_type *)(info->buf))->ip_version;

  if( (ip_version != DS_PROFILE_3GPP_IP_V4) &&
      (ip_version != DS_PROFILE_3GPP_IP_V6) )
  {
    DS_PROFILE_LOGE("Invalid ip_version: %d in TFT params", ip_version);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->tft[DS_PROFILE_3GPP_TFT_FILTER_ID2],
           sizeof(((dsi_profile_3gpp_type *)profile)->prf->tft[DS_PROFILE_3GPP_TFT_FILTER_ID2]),
           info->buf, info->len ); 
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;

    DS_PROFILE_LOGD("set param_tft_filter_id1 sucessful",0);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

ds_profile_status_etype dsi_profile_3gpp_set_linger_params(
  void                  *profile,
  uint64                 mask, 
  const ds_profile_info_type  *info
)
{
  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->iface_linger_params, 
  	  sizeof(((dsi_profile_3gpp_type *)profile)->prf->iface_linger_params),
          info->buf, info->len ); 
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;

    DS_PROFILE_LOGD("set linger_params: successful ",0);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_pdp_context_number(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  ds_profile_3gpp_pdp_context_number_type  pdp_context_num = 0;

  pdp_context_num = (*((ds_profile_3gpp_pdp_context_number_type *)(info->buf)));

  if ((pdp_context_num > DS_UMTS_MAX_PDP_PROFILE_NUM) ||
      (pdp_context_num != 
          ((dsi_profile_3gpp_type *)profile)->prf->context.pdp_context_number))
  {
    DS_PROFILE_LOGE("Invalid pdp_context_num : %d", pdp_context_num);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->context.pdp_context_number,
  	    sizeof(((dsi_profile_3gpp_type *)profile)->prf->context.pdp_context_number),
            info->buf, info->len ); 
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;

   DS_PROFILE_LOGD("set pdp_context_number: %d", 
                   ((dsi_profile_3gpp_type *)profile)->prf->context.pdp_context_number);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_pdp_context_secondary_flag(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{

  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->context.secondary_flag,
  	   sizeof(((dsi_profile_3gpp_type *)profile)->prf->context.secondary_flag),
           info->buf, info->len ); 
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;

  DS_PROFILE_LOGD("set pdp_context_secondary_flag: %d", 
                  ((dsi_profile_3gpp_type *)profile)->prf->context.secondary_flag);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_pdp_context_primary_id(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  ds_profile_3gpp_pdp_context_number_type pdp_context_number = 0;
  ds_profile_3gpp_pdp_context_primary_id_type primary_id     = 0;

  if(!((dsi_profile_3gpp_type *)profile)->prf->context.secondary_flag)
  {
    DS_PROFILE_LOGD( "_3gpp_set_pdp_context_primary_id primary profile: EXIT with SUCCESS", 0 );
    return DS_PROFILE_REG_RESULT_SUCCESS;
  }
  pdp_context_number = 
   (((dsi_profile_3gpp_type *)profile)->prf->context.pdp_context_number);

  primary_id = (*((ds_profile_3gpp_pdp_context_primary_id_type *)(info->buf)));

  if ( (primary_id > DS_UMTS_MAX_PDP_PROFILE_NUM) || 
       (primary_id == pdp_context_number) )
  {
    DS_PROFILE_LOGE("Invalid primary_id: %d", primary_id);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }
  
  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->context.primary_id,
  	  sizeof(((dsi_profile_3gpp_type *)profile)->prf->context.primary_id),
          info->buf, info->len ); 
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;

  DS_PROFILE_LOGD("set pdp_context_primary_id: %d", 
                    ((dsi_profile_3gpp_type *)profile)->prf->context.primary_id);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_ipv4_addr_alloc(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  ds_profile_3gpp_pdp_ipv4_addr_alloc_e_type ipv4_addr_alloc = 
                                        DS_PROFILE_3GPP_PDP_IPV4_ADDR_ALLOC_NAS;

  ipv4_addr_alloc
               = (*((ds_profile_3gpp_pdp_ipv4_addr_alloc_e_type *)(info->buf)));

  if (ipv4_addr_alloc > DS_PROFILE_3GPP_PDP_IPV4_ADDR_ALLOC_DHCPV4)
  {
    DS_PROFILE_LOGE("Invalid ipv4_addr_alloc : %d", ipv4_addr_alloc);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->context.ipv4_addr_alloc,
  	   sizeof(((dsi_profile_3gpp_type *)profile)->prf->context.ipv4_addr_alloc),
           info->buf, info->len ); 
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;

   DS_PROFILE_LOGD("set ipv4_addr_alloc: %d", 
                    ((dsi_profile_3gpp_type *)profile)->prf->context.ipv4_addr_alloc);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_apn_disable_flag(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->apn_disable_flag, 
  	  sizeof(((dsi_profile_3gpp_type *)profile)->prf->apn_disable_flag),
          info->buf, info->len ); 
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;

   DS_PROFILE_LOGD("set apn_disable_flag: %d", 
                     ((dsi_profile_3gpp_type *)profile)->prf->apn_disable_flag);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_inactivity_timer_val(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->inactivity_timer_val,
  	   sizeof(((dsi_profile_3gpp_type *)profile)->prf->inactivity_timer_val),
           info->buf, info->len ); 
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;

   DS_PROFILE_LOGD("set inactivity_timer_val: %d", 
                     ((dsi_profile_3gpp_type *)profile)->prf->inactivity_timer_val);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_apn_class(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->apn_class,
  	  sizeof(((dsi_profile_3gpp_type *)profile)->prf->apn_class),
          info->buf, info->len ); 
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;

   DS_PROFILE_LOGD("set apn_class: %d", ((dsi_profile_3gpp_type *)profile)->prf->apn_class);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_lte_req_qos(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  ds_profile_3gpp_lte_qos_params_type     qos_request_lte;

  memset(&qos_request_lte, 0, sizeof(qos_request_lte));

  memscpy((void*)(&qos_request_lte),sizeof(qos_request_lte), info->buf, info->len);

  if (qos_request_lte.qci >= DS_PROFILE_3GPP_LTE_QCI_INVALID)
  {
    DS_PROFILE_LOGE("Invalid QCI: %d in LTE Req QoS parameters",
                     qos_request_lte.qci);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  if ( (qos_request_lte.max_ul_bit_rate > DS_PROFILE_3GPP_LTE_QOS_PARAM_MAX_UL_BITRATE) ||
       (qos_request_lte.max_dl_bit_rate > DS_PROFILE_3GPP_LTE_QOS_PARAM_MAX_DL_BITRATE) ||
       (qos_request_lte.g_ul_bit_rate > DS_PROFILE_3GPP_LTE_QOS_PARAM_MAX_GTD_UL_BITRATE) ||
       (qos_request_lte.g_dl_bit_rate > DS_PROFILE_3GPP_LTE_QOS_PARAM_MAX_GTD_DL_BITRATE) )
  {
    DS_PROFILE_LOGE("Bitrate in Req QoS is greater than max allowable bitrate",
                     0);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->qos_request_lte,
  	  sizeof(((dsi_profile_3gpp_type *)profile)->prf->qos_request_lte),
          info->buf, info->len ); 
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;

   DS_PROFILE_LOGD("set qos_request_lte:successful ",0);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_src_stat_desc_req(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{ 

  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->src_stat_desc_req,
  	  sizeof(((dsi_profile_3gpp_type *)profile)->prf->src_stat_desc_req),
          info->buf, info->len ); 
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;

  DS_PROFILE_LOGD("set src_stat_desc_req: %d", ((dsi_profile_3gpp_type *)profile)->prf->src_stat_desc_req);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_src_stat_desc_min(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{ 

  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->src_stat_desc_min,
  	  sizeof(((dsi_profile_3gpp_type *)profile)->prf->src_stat_desc_min),
          info->buf, info->len ); 
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;
  DS_PROFILE_LOGD("set src_stat_desc_min: %d", ((dsi_profile_3gpp_type *)profile)->prf->src_stat_desc_min);

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_mcc(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{ 

  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->mcc,
  	  sizeof(((dsi_profile_3gpp_type *)profile)->prf->mcc),
          info->buf, info->len ); 
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;
  DS_PROFILE_LOGD("set mcc: %d", ((dsi_profile_3gpp_type *)profile)->prf->mcc );

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_mnc(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{ 

  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->mnc,
  	   sizeof(((dsi_profile_3gpp_type *)profile)->prf->mnc),
           info->buf, info->len ); 
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;

  DS_PROFILE_LOGD("set mnc: successfully", 0);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_operator_reserved_pco(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{ 

  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->operator_reserved_pco,
  	  sizeof(((dsi_profile_3gpp_type *)profile)->prf->operator_reserved_pco),
          info->buf, info->len ); 
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;

  DS_PROFILE_LOGD("set operator_reserved_pco: %d", 
                  ((dsi_profile_3gpp_type *)profile)->prf->operator_reserved_pco );
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/


static ds_profile_status_etype dsi_profile_3gpp_set_max_pdn_conn_per_block(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{ 
  uint16 max_pdn_conn_per_blk = *((uint16*)(info->buf));

  if (max_pdn_conn_per_blk > DS_UMTS_PDP_MAX_PDN_CONN_PER_BLK_UL)
  {
    DS_PROFILE_LOGE("Invalid max_pdn_conn_per_block: %d. ", max_pdn_conn_per_blk);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->max_pdn_conn_per_blk, 
  	  sizeof(((dsi_profile_3gpp_type *)profile)->prf->max_pdn_conn_per_blk),
          info->buf, info->len ); 
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;

  DS_PROFILE_LOGD("set max_pdn_conn_per_block %d: successfully", ((dsi_profile_3gpp_type *)profile)->prf->max_pdn_conn_per_blk);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_pdn_conn_timer(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{ 
#ifdef FEATURE_DATA_LTE
  uint16 pdn_conn_timer = *((uint16*)(info->buf));

  if (pdn_conn_timer > DS_UMTS_PDP_DEF_MAX_PDN_CONN_TIME_UL)
  {
    DS_PROFILE_LOGE("Invalid pdn_conn_timer: %d. ", pdn_conn_timer);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->max_pdn_conn_time,
  	  sizeof(((dsi_profile_3gpp_type *)profile)->prf->max_pdn_conn_time),
          info->buf, info->len ); 
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;

  DS_PROFILE_LOGD("set pdn_conn_timer %d: successfully", ((dsi_profile_3gpp_type *)profile)->prf->max_pdn_conn_time);
  return DS_PROFILE_REG_RESULT_SUCCESS;
#else
  return DS_PROFILE_REG_RESULT_ERR_INVAL;
#endif /*FEATURE_DATA_LTE*/
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_pdn_req_wait_timer(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{ 
  uint16 pdn_req_wait_time = *((uint16*)(info->buf));

  if (pdn_req_wait_time > DS_UMTS_PDP_DEF_PDN_REQ_WAIT_TIME_UL)
  {
    DS_PROFILE_LOGE("Invalid pdn_req_wait_time: %d. ", pdn_req_wait_time);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->pdn_req_wait_time,
  	  sizeof(((dsi_profile_3gpp_type *)profile)->prf->pdn_req_wait_time),
          info->buf, info->len ); 
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;

  DS_PROFILE_LOGD("set pdn_req_wait_timer %d: successfully", ((dsi_profile_3gpp_type *)profile)->prf->pdn_req_wait_time);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_user_app_data
(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{ 
  uint32 user_app_data = *((uint32*)(info->buf));

  DS_PROFILE_LOGE("Invalid user_app_data: %d. ", user_app_data);

  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->user_app_data, 
          sizeof(((dsi_profile_3gpp_type *)profile)->prf->user_app_data),
        info->buf, info->len ); 
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;

  DS_PROFILE_LOGD("set user_app_data %d: successfully", ((dsi_profile_3gpp_type *)profile)->prf->user_app_data);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_roaming_disallowed_flag
(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{ 
  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->roaming_disallowed, 
  	  sizeof(((dsi_profile_3gpp_type *)profile)->prf->roaming_disallowed),
          info->buf, info->len ); 
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;

   DS_PROFILE_LOGD("set roaming_disallowed flag: %d", 
                     ((dsi_profile_3gpp_type *)profile)->prf->roaming_disallowed);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_pdn_discon_wait_time
(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{ 
  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->pdn_discon_wait_time, 
  	  sizeof(((dsi_profile_3gpp_type *)profile)->prf->pdn_discon_wait_time),
          info->buf, info->len ); 
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;

   DS_PROFILE_LOGD("set pdn_discon_wait_time: %d", 
                     ((dsi_profile_3gpp_type *)profile)->prf->pdn_discon_wait_time);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

static ds_profile_status_etype dsi_profile_3gpp_set_subscription_id
(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{ 
  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->subs_id, 
  	  sizeof(((dsi_profile_3gpp_type *)profile)->prf->subs_id),
          info->buf, info->len ); 
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;

   DS_PROFILE_LOGD("set subs_id: %d", 
                     ((dsi_profile_3gpp_type *)profile)->prf->subs_id);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_apn_bearer(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  uint8  apn_bearer;
  uint8  index = 0;
  
  
  /* Validate input parameters */
  index = dsi_profile_3gpp_get_index_from_ident( 
                                   DS_PROFILE_3GPP_PROFILE_PARAM_APN_BEARER);

  if (!(SET_INFO_IS_VALID(info,ds_profile_3gpp_profile_params_desc_tbl[index].len)))
  {
    DS_PROFILE_LOGE("Invalid info. info->len: %d", info->len);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  apn_bearer = *((uint8 *)(info->buf));
    if(apn_bearer != DS_PROFILE_3GPP_APN_BEARER_FLAG_ALL
     && (apn_bearer & ~(DS_PROFILE_3GPP_APN_BEARER_FLAG_G
                   | DS_PROFILE_3GPP_APN_BEARER_FLAG_W
                   | DS_PROFILE_3GPP_APN_BEARER_FLAG_L)) != 0)
  {
    DS_PROFILE_LOGE("_3gpp_set_apn_bearer:"
                       "invalid arg apn_bearer %d", apn_bearer);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->apn_bearer,
  	   sizeof(((dsi_profile_3gpp_type *)profile)->prf->apn_bearer),
           info->buf, info->len ); 
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;

  DS_PROFILE_LOGD("set apn_bearer: %d", 
                   ((dsi_profile_3gpp_type *)profile)->prf->apn_bearer );
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_emergency_calls_are_supported_flag(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
#ifdef FEATURE_EMERGENCY_PDN
  memscpy((void*)&((dsi_profile_3gpp_type *)profile)->prf->emergency_calls_are_supported,
           sizeof(((dsi_profile_3gpp_type *)profile)->prf->emergency_calls_are_supported),
           info->buf, info->len ); 
  ((dsi_profile_3gpp_type *)profile)->mask |= mask;

   DS_PROFILE_LOGD("set emergency_calls_are_supported: %d", 
                   ((dsi_profile_3gpp_type *)profile)->prf->emergency_calls_are_supported );
  return DS_PROFILE_REG_RESULT_SUCCESS;
#endif /* FEATURE_EMERGENCY_PDN */
  DS_PROFILE_LOGD("set emergency_calls_are_supported_flag function not supported",0);
  return DS_PROFILE_REG_NO_EMERGENCY_PDN_SUPPORT;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_set_param_invalid(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{ 
  DS_PROFILE_LOGE( "set param_invalid: Invalid identifier", 0 );
  return DS_PROFILE_REG_RESULT_ERR_INVAL_IDENT;
}

/*=========================================================================*/


/*---------------------------------------------------------------------------
                   ACCESSOR FUNCTIONS: GET
---------------------------------------------------------------------------*/

/*===========================================================================
FUNCTION dsi_profile_get_ group of functions

DESCRIPTION
  This set of accessor functions are used to get Profile 
  parameter from Profile blob

PARAMETERS
  profile : ptr to 3GPP Profile blob 
  info    : ptr to mem containing data to be written to Profile blob
  
DEPENDENCIES 
  
RETURN VALUE 
  
SIDE EFFECTS 
  none
===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp_get_profile_name(
  const void            *profile,
  ds_profile_info_type  *info
)
{ 
  int result = -1;
  result = strlcpy( (char*)(info->buf), 
           (char*)((dsi_profile_3gpp_type *)profile)->prf->profile_name, 
           info->len );
  if( (result >= info->len) || (result < 0))
  {
    DS_PROFILE_LOGE( "_3gpp_get_profile_name: Invalid buffer len %d or strcpy failed",
                      info->len );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }
  info->len = result;
  DS_PROFILE_LOGD("get profile_name: %s", (char*)(((dsi_profile_3gpp_type *)profile)->prf->profile_name));

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_pdp_context_pdp_type(
  const void            *profile,
  ds_profile_info_type  *info
)
{ 

  if ( !((dsi_profile_3gpp_type *)profile)->prf->ds_profile_3gpp_context_valid_flg )
  {
    DS_PROFILE_LOGD("_3gpp_get_pdp_context_pdp_type: valid flg not set", 0);
    return DS_PROFILE_REG_3GPP_VALID_FLAG_NOT_SET;
  }
  memscpy( (void *)info->buf, info->len,
          (void*)&((dsi_profile_3gpp_type *)profile)->prf->context.pdp_type, 
          info->len );

  DS_PROFILE_LOGD("get pdp_context_pdp_type: %d", ((dsi_profile_3gpp_type *)profile)->prf->context.pdp_type);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_pdp_context_h_comp(
  const void            *profile,
  ds_profile_info_type  *info
)
{ 

  if ( !((dsi_profile_3gpp_type *)profile)->prf->ds_profile_3gpp_context_valid_flg )
  {
    DS_PROFILE_LOGD("_3gpp_get_pdp_context_h_comp: valid flg not set", 0);
    return DS_PROFILE_REG_3GPP_VALID_FLAG_NOT_SET;
  }
  memscpy( (void *)info->buf, info->len,
          (void*)&((dsi_profile_3gpp_type *)profile)->prf->context.h_comp, 
          info->len );

  DS_PROFILE_LOGD("get pdp_context_h_comp: %d",((dsi_profile_3gpp_type *)profile)->prf->context.h_comp);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_pdp_context_d_comp(
  const void            *profile,
  ds_profile_info_type  *info
)
{ 

  if ( !((dsi_profile_3gpp_type *)profile)->prf->ds_profile_3gpp_context_valid_flg )
  {
    DS_PROFILE_LOGD("_3gpp_get_pdp_context_d_comp: valid flg not set", 0);
    return DS_PROFILE_REG_3GPP_VALID_FLAG_NOT_SET;
  }
  memscpy( (void *)info->buf, info->len,
          (void*)&((dsi_profile_3gpp_type *)profile)->prf->context.d_comp, 
          info->len );

  DS_PROFILE_LOGD("get pdp_context_d_comp: %d", ((dsi_profile_3gpp_type *)profile)->prf->context.d_comp);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_pdp_context_apn(
  const void            *profile,
  ds_profile_info_type  *info
)
{ 
  int result = -1;
  if ( !((dsi_profile_3gpp_type *)profile)->prf->ds_profile_3gpp_context_valid_flg )
  {
    DS_PROFILE_LOGD("_3gpp_get_pdp_context_apn: valid flg not set", 0);
    return DS_PROFILE_REG_3GPP_VALID_FLAG_NOT_SET;
  }
  result = strlcpy( (char *)info->buf, 
           (char *)((dsi_profile_3gpp_type *)profile)->prf->context.apn, 
           info->len );
  if( (result >= info->len) || (result < 0))
  {
    DS_PROFILE_LOGE( "_3gpp_get_apn: invalid buffer len %d or strlcpy failed",
                      info->len );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }
  info->len = result;

 DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH,
                                "get pdp_context apn:%s",
	                      (char*)(((dsi_profile_3gpp_type *)profile)->prf->context.apn));
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_dns_addr_v4_primary(
  const void            *profile,
  ds_profile_info_type  *info
)
{ 

  if ( (((dsi_profile_3gpp_type *)profile)->prf->dns_addr_primary_ip_vsn != DS_PROFILE_3GPP_IP_V4) &&
       (((dsi_profile_3gpp_type *)profile)->prf->dns_addr_primary_ip_vsn != DS_PROFILE_3GPP_IP_V4V6) )
    return DS_PROFILE_REG_RESULT_ERR_INVAL;

  memscpy( (void *)info->buf, info->len,
          (void*)&((dsi_profile_3gpp_type *)profile)->prf->dns_addr.primary_dns_addr.ds_profile_3gpp_pdp_addr_ipv4, 
          info->len );

  DS_PROFILE_LOGD("get primary_dns_addr IPv4: %d", 
                   ((dsi_profile_3gpp_type *)profile)->prf->dns_addr.primary_dns_addr.ds_profile_3gpp_pdp_addr_ipv4);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_dns_addr_v6_primary(
  const void            *profile,
  ds_profile_info_type  *info
)
{ 

  if ( (((dsi_profile_3gpp_type *)profile)->prf->dns_addr_primary_ip_vsn != DS_PROFILE_3GPP_IP_V6) &&
       (((dsi_profile_3gpp_type *)profile)->prf->dns_addr_primary_ip_vsn != DS_PROFILE_3GPP_IP_V4V6) )
    return DS_PROFILE_REG_RESULT_ERR_INVAL;

  memscpy( (void *)info->buf, info->len,
          (void*)&((dsi_profile_3gpp_type *)profile)->prf->dns_addr.primary_dns_addr.ds_profile_3gpp_pdp_addr_ipv6, 
          info->len );

  DS_PROFILE_LOGD("get primary_dns_addr IPv6:successful", 0);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_dns_addr_v4_secondary(
  const void            *profile,
  ds_profile_info_type  *info
)
{ 

  if ( (((dsi_profile_3gpp_type *)profile)->prf->dns_addr_secondary_ip_vsn != DS_PROFILE_3GPP_IP_V4) &&
       (((dsi_profile_3gpp_type *)profile)->prf->dns_addr_secondary_ip_vsn != DS_PROFILE_3GPP_IP_V4V6) )
    return DS_PROFILE_REG_RESULT_ERR_INVAL;

  memscpy( (void *)info->buf, info->len,
          (void*)&((dsi_profile_3gpp_type *)profile)->prf->dns_addr.secondary_dns_addr.ds_profile_3gpp_pdp_addr_ipv4, 
          info->len );

  DS_PROFILE_LOGD("get secondary_dns_addr IPv4: %d", 
                  ((dsi_profile_3gpp_type *)profile)->prf->dns_addr.secondary_dns_addr.ds_profile_3gpp_pdp_addr_ipv4);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_dns_addr_v6_secondary(
  const void            *profile,
  ds_profile_info_type  *info
)
{ 

  if ( (((dsi_profile_3gpp_type *)profile)->prf->dns_addr_secondary_ip_vsn != DS_PROFILE_3GPP_IP_V6) &&
       (((dsi_profile_3gpp_type *)profile)->prf->dns_addr_secondary_ip_vsn != DS_PROFILE_3GPP_IP_V4V6) )
    return DS_PROFILE_REG_RESULT_ERR_INVAL;

  memscpy( (void *)info->buf, info->len,
          (void*)&((dsi_profile_3gpp_type *)profile)->prf->dns_addr.secondary_dns_addr.ds_profile_3gpp_pdp_addr_ipv6, 
          info->len );

  DS_PROFILE_LOGD("get secondary_dns_addr IPv6: successful",0);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_umts_req_qos(
  const void            *profile,
  ds_profile_info_type  *info
)
{ 

  if ( !((dsi_profile_3gpp_type *)profile)->prf->ds_profile_3gpp_qos_req_3gpp_valid_flg )
  {
    DS_PROFILE_LOGD("_3gpp_get_3gpp_req_qos: valid flg not set", 0);
    return DS_PROFILE_REG_3GPP_VALID_FLAG_NOT_SET;
  }
  memscpy( (void *)info->buf, info->len,
          (void*)&((dsi_profile_3gpp_type *)profile)->prf->qos_request_3gpp, 
          info->len );
  DS_PROFILE_LOGD("get umts_req_qos: successful",0);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_umts_min_qos(
  const void            *profile,
  ds_profile_info_type  *info
)
{ 

  if ( !((dsi_profile_3gpp_type *)profile)->prf->ds_profile_3gpp_qos_min_3gpp_valid_flg )
  {
    DS_PROFILE_LOGD("_3gpp_get_3gpp_min_qos: valid flg not set", 0);
    return DS_PROFILE_REG_3GPP_VALID_FLAG_NOT_SET;
  }
  memscpy( (void *)info->buf, info->len,
          (void*)&((dsi_profile_3gpp_type *)profile)->prf->qos_minimum_3gpp, 
          info->len );

  DS_PROFILE_LOGD("get umts_min_qos: successful",0);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_gprs_req_qos(
  const void            *profile,
  ds_profile_info_type  *info
)
{ 

  if ( !((dsi_profile_3gpp_type *)profile)->prf->ds_profile_3gpp_qos_req_gprs_valid_flg )
  {
    DS_PROFILE_LOGD("_3gpp_get_gprs_req_qos: valid flg not set", 0);
    return DS_PROFILE_REG_3GPP_VALID_FLAG_NOT_SET;
  }
  memscpy( (void *)info->buf, info->len,
          (void*)&((dsi_profile_3gpp_type *)profile)->prf->qos_request_gprs, 
          info->len );

  DS_PROFILE_LOGD("get qos_request_gprs: successful",0);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_gprs_min_qos(
  const void            *profile,
  ds_profile_info_type  *info
)
{ 

  if ( !((dsi_profile_3gpp_type *)profile)->prf->ds_profile_3gpp_qos_min_gprs_valid_flg )
  {
    DS_PROFILE_LOGD("_3gpp_get_gprs_min_qos: valid flg not set", 0);
    return DS_PROFILE_REG_3GPP_VALID_FLAG_NOT_SET;
  }
  memscpy( (void *)info->buf, info->len,
          (void*)&((dsi_profile_3gpp_type *)profile)->prf->qos_minimum_gprs, 
          info->len );

  DS_PROFILE_LOGD("get qos_minimum_gprs: successful",0); 
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_auth_username(
  const void            *profile,
  ds_profile_info_type  *info
)
{ 
  int result = -1;
  result = strlcpy( (char *)info->buf, 
           (char *)((dsi_profile_3gpp_type *)profile)->prf->auth.username, 
           info->len );
  if( (result >= info->len) || (result < 0) )
  {
    DS_PROFILE_LOGE( "_3gpp_get_profile_name: invalid buffer len %d or strlcpy failed",
                     info->len );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }
  info->len = result;
  DATA_3GPP_MSG_SPRINTF_1(MSG_LEGACY_HIGH,
			 "get auth_username:%s",
			(char*)((dsi_profile_3gpp_type *)profile)->prf->auth.username);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_auth_password(
  const void            *profile,
  ds_profile_info_type  *info
)
{ 
  int result = -1;
  result = strlcpy( (char *)info->buf, 
           (char *)((dsi_profile_3gpp_type *)profile)->prf->auth.password, 
           info->len );
  if( (result >= info->len) || (result < 0) )
  {
    DS_PROFILE_LOGE( "_3gpp_get_profile_name: invalid buffer len %d or strlcpy failed",
                     info->len );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }
  info->len = result;
  DS_PROFILE_LOGD("Retrieved  auth_password successfully ", 0);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_auth_type(
  const void            *profile,
  ds_profile_info_type  *info
)
{ 

  memscpy( (void *)info->buf, info->len,
          (void*)&((dsi_profile_3gpp_type *)profile)->prf->auth.auth_type, 
          info->len );

  DS_PROFILE_LOGD("get auth.auth_type: %d", 
                   ((dsi_profile_3gpp_type *)profile)->prf->auth.auth_type);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_pdp_context_pdp_addr_v4(
  const void            *profile,
  ds_profile_info_type  *info
)
{ 

  if ( !((dsi_profile_3gpp_type *)profile)->prf->ds_profile_3gpp_context_valid_flg )
  {
    DS_PROFILE_LOGD("_3gpp_get_pdp_context_pdp_addr_v4: valid flg not set", 0);
    return DS_PROFILE_REG_3GPP_VALID_FLAG_NOT_SET;
  }
  if ( (((dsi_profile_3gpp_type *)profile)->prf->pdp_addr_ip_vsn != DS_PROFILE_3GPP_IP_V4 ) &&
       (((dsi_profile_3gpp_type *)profile)->prf->pdp_addr_ip_vsn != DS_PROFILE_3GPP_IP_V4V6) )
    return DS_PROFILE_REG_RESULT_ERR_INVAL;

  memscpy( (void *)info->buf, info->len,
          (void*)&((dsi_profile_3gpp_type *)profile)->prf->context.pdp_addr.ds_profile_3gpp_pdp_addr_ipv4, 
          info->len );

  DS_PROFILE_LOGD("get pdp_context_pdp_addr_v4: %d", 
                  ((dsi_profile_3gpp_type *)profile)->prf->context.pdp_addr.ds_profile_3gpp_pdp_addr_ipv4);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_pdp_context_pdp_addr_v6(
  const void            *profile,
  ds_profile_info_type  *info
)
{ 

  if ( !((dsi_profile_3gpp_type *)profile)->prf->ds_profile_3gpp_context_valid_flg )
  {
    DS_PROFILE_LOGD("_3gpp_get_pdp_context_pdp_addr_v6: valid flg not set", 0);
    return DS_PROFILE_REG_3GPP_VALID_FLAG_NOT_SET;
  }
  if ( (((dsi_profile_3gpp_type *)profile)->prf->pdp_addr_ip_vsn != DS_PROFILE_3GPP_IP_V6 ) &&
       (((dsi_profile_3gpp_type *)profile)->prf->pdp_addr_ip_vsn != DS_PROFILE_3GPP_IP_V4V6) )
    return DS_PROFILE_REG_RESULT_ERR_INVAL;

  memscpy( (void *)info->buf, info->len,
          (void*)&((dsi_profile_3gpp_type *)profile)->prf->context.pdp_addr.ds_profile_3gpp_pdp_addr_ipv6, 
          info->len );

  DS_PROFILE_LOGD("get pdp_context_pdp_addr_v6:successful ", 0);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_pcscf_req_flag(
  const void            *profile,
  ds_profile_info_type  *info
)
{ 

  memscpy( (void *)info->buf, info->len, 
          (void*)&((dsi_profile_3gpp_type *)profile)->prf->request_pcscf_address_flag, 
          info->len );

  DS_PROFILE_LOGD("get request_pcscf_address_flag: %d", 
                   ((dsi_profile_3gpp_type *)profile)->prf->request_pcscf_address_flag);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_pdp_context_te_mt_access_ctrl_flag(
  const void            *profile,
  ds_profile_info_type  *info
)
{ 

  if ( !((dsi_profile_3gpp_type *)profile)->prf->ds_profile_3gpp_context_valid_flg )
  {
    DS_PROFILE_LOGD("_3gpp_get_pdp_context_te_mt_access_ctrl_flag: valid flg not set", 0);
    return DS_PROFILE_REG_3GPP_VALID_FLAG_NOT_SET;
  }
  memscpy( (void *)info->buf, info->len,
          (void*)&((dsi_profile_3gpp_type *)profile)->prf->context.access_ctrl_flag, 
          info->len );

  DS_PROFILE_LOGD("get access_ctrl_flag: %d", 
                   ((dsi_profile_3gpp_type *)profile)->prf->context.access_ctrl_flag);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_pcscf_dhcp_req_flag(
  const void            *profile,
  ds_profile_info_type  *info
)
{ 

  memscpy( (void *)info->buf, info->len,
          (void*)&((dsi_profile_3gpp_type *)profile)->prf->request_pcscf_address_using_dhcp_flag, 
          info->len );

  DS_PROFILE_LOGD("get request_pcscf_address_using_dhcp_flag: %d", 
                   ((dsi_profile_3gpp_type *)profile)->prf->request_pcscf_address_using_dhcp_flag);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_im_cn_flag(
  const void            *profile,
  ds_profile_info_type  *info
)
{ 

  memscpy( (void *)info->buf, info->len,
          (void*)&((dsi_profile_3gpp_type *)profile)->prf->im_cn_flag, 
          info->len );

  DS_PROFILE_LOGD("get im_cn_flag: %d", ((dsi_profile_3gpp_type *)profile)->prf->im_cn_flag);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_param_tft_filter_id1(
  const void            *profile,
  ds_profile_info_type  *info
)
{ 
 
  if ( !((dsi_profile_3gpp_type *)profile)->prf->ds_profile_3gpp_tft_valid_flg[DS_PROFILE_3GPP_TFT_FILTER_ID1] )
  {
    DS_PROFILE_LOGD("_3gpp_get_param_tft_filter_id1: valid flg not set", 0);
    return DS_PROFILE_REG_3GPP_VALID_FLAG_NOT_SET;
  }

  memscpy( (void *)info->buf, info->len,
          (void*)&((dsi_profile_3gpp_type *)profile)->prf->tft[DS_PROFILE_3GPP_TFT_FILTER_ID1], 
          info->len );

  DS_PROFILE_LOGD("get param_tft_filter_id1:successful",0);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_param_tft_filter_id2(
  const void            *profile,
  ds_profile_info_type  *info
)
{ 
 
  if ( !((dsi_profile_3gpp_type *)profile)->prf->ds_profile_3gpp_tft_valid_flg[DS_PROFILE_3GPP_TFT_FILTER_ID2] )
  {
    DS_PROFILE_LOGD("_3gpp_get_param_tft_filter_id2: valid flg not set", 0);
    return DS_PROFILE_REG_3GPP_VALID_FLAG_NOT_SET;
  }

  memscpy( (void *)info->buf, info->len,
          (void*)&((dsi_profile_3gpp_type *)profile)->prf->tft[DS_PROFILE_3GPP_TFT_FILTER_ID2], 
          info->len );

  DS_PROFILE_LOGD("get param_tft_filter_id2: successful",0); 
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_linger_params (
  const void            *profile,  
  ds_profile_info_type  *info
)
{
 
  if ( !((dsi_profile_3gpp_type *)profile)->prf->ds_profile_3gpp_linger_params_valid_flag )
  {
    DS_PROFILE_LOGD("_3gpp_get_param_allow_linger_params: valid flg not set", 0);
    return DS_PROFILE_REG_3GPP_VALID_FLAG_NOT_SET;
  }

  memscpy( (void *)info->buf, info->len,
          (void*)&((dsi_profile_3gpp_type *)profile)->prf->iface_linger_params, 
          info->len );

  DS_PROFILE_LOGD("get iface_linger_params: successful",0); 
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_pdp_context_number(
  const void            *profile,
  ds_profile_info_type  *info
)
{ 

  if ( !((dsi_profile_3gpp_type *)profile)->prf->ds_profile_3gpp_context_valid_flg )
  {
    DS_PROFILE_LOGD("_3gpp_get_pdp_context_number: valid flg not set", 0);
    return DS_PROFILE_REG_3GPP_VALID_FLAG_NOT_SET;
  }
  memscpy( (void *)info->buf, info->len, 
          (void*)&((dsi_profile_3gpp_type *)profile)->prf->context.pdp_context_number, 
          info->len );

  DS_PROFILE_LOGD("get pdp_context_number: %d", 
                   ((dsi_profile_3gpp_type *)profile)->prf->context.pdp_context_number);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_pdp_context_secondary_flag(
  const void            *profile,
  ds_profile_info_type  *info
)
{

  if ( !((dsi_profile_3gpp_type *)profile)->prf->ds_profile_3gpp_context_valid_flg )
  {
    DS_PROFILE_LOGD("_3gpp_get_pdp_context_secondary_flag: valid flg not set", 0);
    return DS_PROFILE_REG_3GPP_VALID_FLAG_NOT_SET;
  }
  memscpy( (void *)info->buf, info->len,
          (void*)&((dsi_profile_3gpp_type *)profile)->prf->context.secondary_flag, 
          info->len );

  DS_PROFILE_LOGD("get pdp_context_secondary_flag: %d", 
                   ((dsi_profile_3gpp_type *)profile)->prf->context.secondary_flag);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_pdp_context_primary_id(
  const void            *profile,
  ds_profile_info_type  *info
)
{

  if ( !((dsi_profile_3gpp_type *)profile)->prf->ds_profile_3gpp_context_valid_flg )
  {
    DS_PROFILE_LOGD("_3gpp_get_pdp_context_primary_id: valid flg not set", 0);
    return DS_PROFILE_REG_3GPP_VALID_FLAG_NOT_SET;
  }
  memscpy( (void *)info->buf, info->len,
          (void*)&((dsi_profile_3gpp_type *)profile)->prf->context.primary_id, 
          info->len );

  DS_PROFILE_LOGD("get pdp_context_primary_id: %d", 
                   ((dsi_profile_3gpp_type *)profile)->prf->context.primary_id);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_ipv4_addr_alloc(
  const void            *profile,
  ds_profile_info_type  *info
)
{

  if ( !((dsi_profile_3gpp_type *)profile)->prf->ds_profile_3gpp_context_valid_flg )
  {
    DS_PROFILE_LOGD("_3gpp_get_ipv4_addr_alloc: valid flg not set", 0);
    return DS_PROFILE_REG_3GPP_VALID_FLAG_NOT_SET;
  }
  memscpy( (void *)info->buf, info->len,
          (void*)&((dsi_profile_3gpp_type *)profile)->prf->context.ipv4_addr_alloc, 
          info->len );

  DS_PROFILE_LOGD("get pdp_context_ipv4_addr_alloc: %d", 
                   ((dsi_profile_3gpp_type *)profile)->prf->context.ipv4_addr_alloc);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_apn_disable_flag(
  const void            *profile,
  ds_profile_info_type  *info
)
{

  if ( !((dsi_profile_3gpp_type *)profile)->prf->ds_profile_3gpp_context_valid_flg )
  {
    DS_PROFILE_LOGD("_3gpp_get_apn_disable_flag: valid flg not set", 0);
    return DS_PROFILE_REG_3GPP_VALID_FLAG_NOT_SET;
  }
  memscpy( (void *)info->buf, info->len,
          (void*)&((dsi_profile_3gpp_type *)profile)->prf->apn_disable_flag, 
          info->len );

  DS_PROFILE_LOGD("get apn_disable_flag: %d", 
                   ((dsi_profile_3gpp_type *)profile)->prf->apn_disable_flag);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_inactivity_timer_val(
  const void            *profile,
  ds_profile_info_type  *info
)
{

  if ( !((dsi_profile_3gpp_type *)profile)->prf->ds_profile_3gpp_context_valid_flg )
  {
    DS_PROFILE_LOGD("_3gpp_get_inactivity_timer_val: valid flg not set", 0);
    return DS_PROFILE_REG_3GPP_VALID_FLAG_NOT_SET;
  }
  memscpy( (void *)info->buf, info->len,
          (void*)&((dsi_profile_3gpp_type *)profile)->prf->inactivity_timer_val, 
          info->len );

  DS_PROFILE_LOGD("get inactivity_timer_val: %d", 
                   ((dsi_profile_3gpp_type *)profile)->prf->inactivity_timer_val);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_apn_class(
  const void            *profile,
  ds_profile_info_type  *info
)
{

  if ( !((dsi_profile_3gpp_type *)profile)->prf->ds_profile_3gpp_context_valid_flg )
  {
    DS_PROFILE_LOGD("_3gpp_get_apn_class: valid flg not set", 0);
    return DS_PROFILE_REG_3GPP_VALID_FLAG_NOT_SET;
  }
  memscpy( (void *)info->buf, info->len,
          (void*)&((dsi_profile_3gpp_type *)profile)->prf->apn_class, 
          info->len );

  DS_PROFILE_LOGD("get apn_class: %d", 
                   ((dsi_profile_3gpp_type *)profile)->prf->apn_class);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_lte_req_qos(
  const void            *profile,
  ds_profile_info_type  *info
)
{

  if ( !((dsi_profile_3gpp_type *)profile)->prf->ds_profile_3gpp_qos_req_lte_valid_flg )
  {
    DS_PROFILE_LOGD("_3gpp_get_lte_req_qos: valid flg not set", 0);
    return DS_PROFILE_REG_3GPP_VALID_FLAG_NOT_SET;
  }
  memscpy( (void *)info->buf, info->len,
          (void*)&((dsi_profile_3gpp_type *)profile)->prf->qos_request_lte, 
          info->len );

  DS_PROFILE_LOGD("get qos_request_lte: successful", 0);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_src_stat_desc_req(
  const void            *profile,
  ds_profile_info_type  *info
)
{

  memscpy( (void *)info->buf, info->len,
          (void*)&((dsi_profile_3gpp_type *)profile)->prf->src_stat_desc_req, 
          info->len );

  DS_PROFILE_LOGD("get src_stat_desc_req: %d", 
                   ((dsi_profile_3gpp_type *)profile)->prf->src_stat_desc_req);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_src_stat_desc_min(
  const void            *profile,
  ds_profile_info_type  *info
)
{

  memscpy( (void *)info->buf, info->len,
          (void*)&((dsi_profile_3gpp_type *)profile)->prf->src_stat_desc_min, 
          info->len );

  DS_PROFILE_LOGD("get src_stat_desc_min: %d", 
                   ((dsi_profile_3gpp_type *)profile)->prf->src_stat_desc_min);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_apn_bearer(
  const void            *profile,
  ds_profile_info_type  *info
)
{
  DS_PROFILE_LOGD( "_3gpp_get_apn_bearer: ENTRY", 0 ); 

  memscpy( (void *)info->buf, info->len,
          (void*)&((dsi_profile_3gpp_type *)profile)->prf->apn_bearer, 
          info->len );

  DS_PROFILE_LOGD( "_3gpp_get_apn_bearer: EXIT with SUCCESS", 0 );
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_mcc(
  const void            *profile,
  ds_profile_info_type  *info
)
{
  DS_PROFILE_LOGD( "_3gpp_get_mcc: ENTRY", 0 ); 

  memscpy( (void *)info->buf, info->len,
          (void*)&((dsi_profile_3gpp_type *)profile)->prf->mcc, 
          info->len );

  DS_PROFILE_LOGD( "_3gpp_get_mcc: EXIT with SUCCESS", 0 );
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_mnc(
  const void            *profile,
  ds_profile_info_type  *info
)
{
  DS_PROFILE_LOGD( "_3gpp_get_mnc: ENTRY", 0 ); 

  memscpy( (void *)info->buf, info->len,
          (void*)&((dsi_profile_3gpp_type *)profile)->prf->mnc, 
          info->len );

  DS_PROFILE_LOGD( "_3gpp_get_mnc: EXIT with SUCCESS", 0 );
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_operator_reserved_pco(
  const void            *profile,
  ds_profile_info_type  *info
)
{
  DS_PROFILE_LOGD( "_3gpp_get_operator_reserved_pco: ENTRY", 0 ); 

  memscpy( (void *)info->buf, info->len,
          (void*)&((dsi_profile_3gpp_type *)profile)->prf-> operator_reserved_pco , 
          info->len );

  DS_PROFILE_LOGD( "_3gpp_get_operator_reserved_pco: EXIT with SUCCESS", 0 );
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_max_pdn_conn_per_block(
  const void            *profile,
  ds_profile_info_type  *info
)
{
  uint16 max_pdn_conn_per_blk = 0;

  DS_PROFILE_LOGD( "_3gpp_get_max_pdn_conn_per_block: ENTRY", 0 ); 

  memscpy( (void *)info->buf, info->len,
          (void*)&((dsi_profile_3gpp_type *)profile)->prf->max_pdn_conn_per_blk, 
          info->len );

  max_pdn_conn_per_blk = *((uint16*)(info->buf));

  if (max_pdn_conn_per_blk > DS_UMTS_PDP_MAX_PDN_CONN_PER_BLK_UL)
  {
    DS_PROFILE_LOGE("Invalid max_pdn_conn_per_block: %d. ", max_pdn_conn_per_blk);
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  DS_PROFILE_LOGD( "_3gpp_get_max_pdn_conn_per_block %d: EXIT with SUCCESS", max_pdn_conn_per_blk );
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_pdn_conn_timer(
  const void            *profile,
  ds_profile_info_type  *info
)
{
  uint16 pdn_conn_timer = 0;

  DS_PROFILE_LOGD( "_3gpp_get_pdn_conn_timer: ENTRY", 0 ); 

  memscpy( (void *)info->buf, info->len,
          (void*)&((dsi_profile_3gpp_type *)profile)->prf->max_pdn_conn_time, 
          info->len );

  pdn_conn_timer = *((uint16*)(info->buf));

  if (pdn_conn_timer > DS_UMTS_PDP_DEF_MAX_PDN_CONN_TIME_UL)
  {
	DS_PROFILE_LOGE("Invalid pdn_conn_timer: %d. ", pdn_conn_timer);
	return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  DS_PROFILE_LOGD( "_3gpp_get_pdn_conn_timer %d: EXIT with SUCCESS", pdn_conn_timer );
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_pdn_req_wait_timer(
  const void            *profile,
  ds_profile_info_type  *info
)
{
  uint16 pdn_req_wait_timer = 0;

  DS_PROFILE_LOGD( "_3gpp_get_pdn_req_wait_timer: ENTRY", 0 ); 

  memscpy( (void *)info->buf, info->len,
          (void*)&((dsi_profile_3gpp_type *)profile)->prf->pdn_req_wait_time, 
          info->len );

  pdn_req_wait_timer = *((uint16*)(info->buf));

  if (pdn_req_wait_timer > DS_UMTS_PDP_DEF_PDN_REQ_WAIT_TIME_UL)
  {
	DS_PROFILE_LOGE("Invalid pdn_req_wait_timer: %d. ", pdn_req_wait_timer);
	return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  DS_PROFILE_LOGD( "_3gpp_get_pdn_req_wait_timer %d: EXIT with SUCCESS", pdn_req_wait_timer );
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_user_app_data
(
  const void            *profile,
  ds_profile_info_type  *info
)
{
  uint32 user_app_data = 0;

  DS_PROFILE_LOGD( "_3gpp_get_user_app_data: ENTRY", 0 ); 

  memscpy( (void *)info->buf, info->len, 
          (void*)&((dsi_profile_3gpp_type *)profile)->prf->user_app_data, 
          info->len );

  user_app_data = *((uint32*)(info->buf));

  DS_PROFILE_LOGD( "_3gpp_get_user_app_data %d: EXIT with SUCCESS", user_app_data );
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp_get_roaming_disallowed_flag(
  const void            *profile,
  ds_profile_info_type  *info
)
{

  if ( !((dsi_profile_3gpp_type *)profile)->prf->ds_profile_3gpp_context_valid_flg )
  {
    DS_PROFILE_LOGD("_3gpp_get_roaming_disallowed_flag: valid flg not set", 0);
    return DS_PROFILE_REG_3GPP_VALID_FLAG_NOT_SET;
  }
  memscpy( (void *)info->buf, info->len,
          (void*)&((dsi_profile_3gpp_type *)profile)->prf->roaming_disallowed, 
          info->len );

  DS_PROFILE_LOGD("_3gpp_get_roaming_disallowed_flag: %d", 
                   ((dsi_profile_3gpp_type *)profile)->prf->roaming_disallowed);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp_get_pdn_discon_wait_time(
  const void            *profile,
  ds_profile_info_type  *info
)
{

  if ( !((dsi_profile_3gpp_type *)profile)->prf->ds_profile_3gpp_context_valid_flg )
  {
    DS_PROFILE_LOGD("_3gpp_get_roaming_disallowed_flag: valid flg not set", 0);
    return DS_PROFILE_REG_3GPP_VALID_FLAG_NOT_SET;
  }
  memscpy( (void *)info->buf, info->len,
          (void*)&((dsi_profile_3gpp_type *)profile)->prf->pdn_discon_wait_time, 
          info->len );

  DS_PROFILE_LOGD("_3gpp_get_roaming_disallowed_flag: %d", 
                   ((dsi_profile_3gpp_type *)profile)->prf->pdn_discon_wait_time);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}


/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp_get_subscription_id(
  const void            *profile,
  ds_profile_info_type  *info
)
{

  if ( !((dsi_profile_3gpp_type *)profile)->prf->ds_profile_3gpp_context_valid_flg )
  {
    DS_PROFILE_LOGD("_3gpp_get_subscription_id: valid flg not set", 0);
    return DS_PROFILE_REG_3GPP_VALID_FLAG_NOT_SET;
  }
  memscpy( (void *)info->buf, info->len,
          (void*)&((dsi_profile_3gpp_type *)profile)->prf->subs_id, 
          info->len );

  DS_PROFILE_LOGD("_3gpp_get_subscription_id: %d", 
                   ((dsi_profile_3gpp_type *)profile)->prf->subs_id);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_emergency_calls_are_supported_flag(
  const void            *profile,
  ds_profile_info_type  *info
)
{ 
#ifdef FEATURE_EMERGENCY_PDN
  DS_PROFILE_LOGD("_3gpp_get_emergency_calls_are_supported_flag: ENTRY", 0);

  memscpy( (void *)info->buf, info->len,
          (void*)&((dsi_profile_3gpp_type *)profile)->prf->emergency_calls_are_supported, 
          info->len );

  DS_PROFILE_LOGD("_3gpp_get_emergency_calls_are_supported_flag: EXIT with SUCCESS", 0);
  return DS_PROFILE_REG_RESULT_SUCCESS;
#endif /* FEATURE_EMERGENCY_PDN */
  DS_PROFILE_LOGD("_3gpp_get_emergency_calls_are_supported_flag: EXIT with FAILURE, "
                  "flag not supported",0);
  return DS_PROFILE_REG_NO_EMERGENCY_PDN_SUPPORT;
}

/*===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp_get_param_invalid(
  const void            *profile,
  ds_profile_info_type  *info
)
{ 
  DS_PROFILE_LOGE( "_3gpp_get: Invalid identifier", 0 );
  return DS_PROFILE_REG_RESULT_ERR_INVAL_IDENT;
}

/*=========================================================================*/

/*----------------------------------------------------------------------------- 
  Table for parameter and its get/set functions
-----------------------------------------------------------------------------*/ 
static dsi_profile_acc_mut_fn_type ds_profile_3gpp_acc_mut_fn_tbl[] = {

   /* dummy function, ident not valid*/
  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_INVALID,
  dsi_profile_3gpp_set_param_invalid,
  dsi_profile_3gpp_get_param_invalid},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_PROFILE_NAME,
  dsi_profile_3gpp_set_profile_name,
  dsi_profile_3gpp_get_profile_name},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_PDP_CONTEXT_PDP_TYPE,
  dsi_profile_3gpp_set_pdp_context_pdp_type,
  dsi_profile_3gpp_get_pdp_context_pdp_type},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_PDP_CONTEXT_H_COMP,
  dsi_profile_3gpp_set_pdp_context_h_comp,
  dsi_profile_3gpp_get_pdp_context_h_comp},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_PDP_CONTEXT_D_COMP,
  dsi_profile_3gpp_set_pdp_context_d_comp,
  dsi_profile_3gpp_get_pdp_context_d_comp},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_PDP_CONTEXT_APN,
  dsi_profile_3gpp_set_pdp_context_apn,
  dsi_profile_3gpp_get_pdp_context_apn},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_DNS_ADDR_V4_PRIMARY,
  dsi_profile_3gpp_set_dns_addr_v4_primary,
  dsi_profile_3gpp_get_dns_addr_v4_primary},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_DNS_ADDR_V4_SECONDARY,
  dsi_profile_3gpp_set_dns_addr_v4_secondary,
  dsi_profile_3gpp_get_dns_addr_v4_secondary},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_UMTS_REQ_QOS,
  dsi_profile_3gpp_set_umts_req_qos,
  dsi_profile_3gpp_get_umts_req_qos},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_UMTS_MIN_QOS,
  dsi_profile_3gpp_set_umts_min_qos,
  dsi_profile_3gpp_get_umts_min_qos},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_GPRS_REQ_QOS,
  dsi_profile_3gpp_set_gprs_req_qos,
  dsi_profile_3gpp_get_gprs_req_qos},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_GPRS_MIN_QOS,
  dsi_profile_3gpp_set_gprs_min_qos,
  dsi_profile_3gpp_get_gprs_min_qos},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_AUTH_USERNAME,
  dsi_profile_3gpp_set_auth_username,
  dsi_profile_3gpp_get_auth_username},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_AUTH_PASSWORD,
  dsi_profile_3gpp_set_auth_password,
  dsi_profile_3gpp_get_auth_password},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_AUTH_TYPE,
  dsi_profile_3gpp_set_auth_type,
  dsi_profile_3gpp_get_auth_type},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_PDP_CONTEXT_PDP_ADDR_V4,
  dsi_profile_3gpp_set_pdp_context_pdp_addr_v4,
  dsi_profile_3gpp_get_pdp_context_pdp_addr_v4},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_PCSCF_REQ_FLAG,
  dsi_profile_3gpp_set_pcscf_req_flag,
  dsi_profile_3gpp_get_pcscf_req_flag},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_PDP_CONTEXT_TE_MT_ACCESS_CTRL_FLAG,
  dsi_profile_3gpp_set_pdp_context_te_mt_access_ctrl_flag,
  dsi_profile_3gpp_get_pdp_context_te_mt_access_ctrl_flag},         

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_PCSCF_DHCP_REQ_FLAG,
  dsi_profile_3gpp_set_pcscf_dhcp_req_flag,
  dsi_profile_3gpp_get_pcscf_dhcp_req_flag},                      
                          
  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_IM_CN_FLAG,
  dsi_profile_3gpp_set_im_cn_flag,
  dsi_profile_3gpp_get_im_cn_flag},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_TFT_FILTER_ID1,
  dsi_profile_3gpp_set_param_tft_filter_id1,
  dsi_profile_3gpp_get_param_tft_filter_id1}, 

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_TFT_FILTER_ID2,
  dsi_profile_3gpp_set_param_tft_filter_id2,
  dsi_profile_3gpp_get_param_tft_filter_id2}, 

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_PDP_CONTEXT_NUMBER,
  dsi_profile_3gpp_set_pdp_context_number,
  dsi_profile_3gpp_get_pdp_context_number},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_PDP_CONTEXT_SECONDARY_FLAG,
  dsi_profile_3gpp_set_pdp_context_secondary_flag,
  dsi_profile_3gpp_get_pdp_context_secondary_flag},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_PDP_CONTEXT_PRIMARY_ID,
  dsi_profile_3gpp_set_pdp_context_primary_id,
  dsi_profile_3gpp_get_pdp_context_primary_id},
              
  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_PDP_CONTEXT_PDP_ADDR_V6,
  dsi_profile_3gpp_set_pdp_context_pdp_addr_v6,
  dsi_profile_3gpp_get_pdp_context_pdp_addr_v6},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_UMTS_REQ_QOS_EXTENDED,
  dsi_profile_3gpp_set_umts_req_qos,
  dsi_profile_3gpp_get_umts_req_qos},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_UMTS_MIN_QOS_EXTENDED,
  dsi_profile_3gpp_set_umts_min_qos,
  dsi_profile_3gpp_get_umts_min_qos},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_DNS_ADDR_V6_PRIMARY,
  dsi_profile_3gpp_set_dns_addr_v6_primary,
  dsi_profile_3gpp_get_dns_addr_v6_primary},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_DNS_ADDR_V6_SECONDARY,
  dsi_profile_3gpp_set_dns_addr_v6_secondary,
  dsi_profile_3gpp_get_dns_addr_v6_secondary},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_IPV4_ADDR_ALLOC,
  dsi_profile_3gpp_set_ipv4_addr_alloc,
  dsi_profile_3gpp_get_ipv4_addr_alloc},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_LTE_REQ_QOS,
  dsi_profile_3gpp_set_lte_req_qos,
  dsi_profile_3gpp_get_lte_req_qos},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_LINGER_PARAMS,
  dsi_profile_3gpp_set_linger_params,
  dsi_profile_3gpp_get_linger_params },

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_APN_DISABLE_FLAG,
  dsi_profile_3gpp_set_apn_disable_flag,
  dsi_profile_3gpp_get_apn_disable_flag},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_INACTIVITY_TIMER_VAL,
  dsi_profile_3gpp_set_inactivity_timer_val,
  dsi_profile_3gpp_get_inactivity_timer_val},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_APN_CLASS,
  dsi_profile_3gpp_set_apn_class,
  dsi_profile_3gpp_get_apn_class},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_SRC_STAT_DESC_REQ,
  dsi_profile_3gpp_set_src_stat_desc_req,
  dsi_profile_3gpp_get_src_stat_desc_req},

  {(ds_profile_identifier_type)
       DS_PROFILE_3GPP_PROFILE_PARAM_SRC_STAT_DESC_MIN,
   dsi_profile_3gpp_set_src_stat_desc_min,
   dsi_profile_3gpp_get_src_stat_desc_min},

  {(ds_profile_identifier_type)
       DS_PROFILE_3GPP_PROFILE_PARAM_APN_BEARER,
   dsi_profile_3gpp_set_apn_bearer,
   dsi_profile_3gpp_get_apn_bearer},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP_PROFILE_PARAM_EMERGENCY_CALLS_SUPPORTED,
  dsi_profile_3gpp_set_emergency_calls_are_supported_flag,
  dsi_profile_3gpp_get_emergency_calls_are_supported_flag},

  {(ds_profile_identifier_type)
       DS_PROFILE_3GPP_PROFILE_PARAM_OPERATOR_RESERVED_PCO,
   dsi_profile_3gpp_set_operator_reserved_pco,
   dsi_profile_3gpp_get_operator_reserved_pco},

  {(ds_profile_identifier_type)
       DS_PROFILE_3GPP_PROFILE_PARAM_MCC,
   dsi_profile_3gpp_set_mcc,
   dsi_profile_3gpp_get_mcc},

  {(ds_profile_identifier_type)
       DS_PROFILE_3GPP_PROFILE_PARAM_MNC,
   dsi_profile_3gpp_set_mnc,
   dsi_profile_3gpp_get_mnc},

  {(ds_profile_identifier_type)
       DS_PROFILE_3GPP_PROFILE_MAX_PDN_CONN_PER_BLOCK,
   dsi_profile_3gpp_set_max_pdn_conn_per_block,
   dsi_profile_3gpp_get_max_pdn_conn_per_block},

  {(ds_profile_identifier_type)
       DS_PROFILE_3GPP_PROFILE_MAX_PDN_CONN_TIMER,
   dsi_profile_3gpp_set_pdn_conn_timer,
   dsi_profile_3gpp_get_pdn_conn_timer},

  {(ds_profile_identifier_type)
       DS_PROFILE_3GPP_PROFILE_PDN_REQ_WAIT_TIMER,
   dsi_profile_3gpp_set_pdn_req_wait_timer,
   dsi_profile_3gpp_get_pdn_req_wait_timer},

  {(ds_profile_identifier_type)
       DS_PROFILE_3GPP_PROFILE_USER_APP_DATA,
   dsi_profile_3gpp_set_user_app_data,
   dsi_profile_3gpp_get_user_app_data},

  {(ds_profile_identifier_type)
       DS_PROFILE_3GPP_PROFILE_PARAM_ROAMING_DISALLOWED,
   dsi_profile_3gpp_set_roaming_disallowed_flag,
   dsi_profile_3gpp_get_roaming_disallowed_flag},

  {(ds_profile_identifier_type)
       DS_PROFILE_3GPP_PROFILE_PARAM_PDN_DISCON_WAIT_TIME,
   dsi_profile_3gpp_set_pdn_discon_wait_time,
   dsi_profile_3gpp_get_pdn_discon_wait_time}
};

/*===========================================================================
FUNCTION DSI_PROFILE_3GPP_GET_INDEX_FROM_IDENT

DESCRIPTION
  This function gets index for the identifier to index into the function table.

PARAMETERS 
  ident : identifier for which index is to be returned

DEPENDENCIES 
  
RETURN VALUE 
  returns index
SIDE EFFECTS 
  
===========================================================================*/
/*lint -save -e656*/
uint8 dsi_profile_3gpp_get_index_from_ident( 
  ds_profile_identifier_type ident
)
{
  uint8 index = 0;
  uint8 i     = 0;
  for (i = 0; 
       i <= ((DS_PROFILE_3GPP_PROFILE_PARAM_MAX - DS_PROFILE_3GPP_PROFILE_PARAM_MIN)+1); 
       i++ )
  {
    if (ident == ds_profile_3gpp_profile_params_desc_tbl[i].uid)
    {
      index = i;
      break;
    }
  }
  return index;
}
/*lint -restore Restore lint error 656*/
/*===========================================================================
FUNCTION DSI_PROFILE_3GPP_ALLOC_PROFILE
 
DESCRIPTION
  This function is used to allocate memory which will be used for the local copy
  of the profile

PARAMETERS 
  
DEPENDENCIES 
  
RETURN VALUE 
  pointer to which memory is allocated
  NULL on failure
 
SIDE EFFECTS 
  
===========================================================================*/
static void* dsi_profile_3gpp_alloc_profile(void)
{
  dsi_profile_3gpp_type *tmp_prf = NULL;

  tmp_prf = (dsi_profile_3gpp_type *)DS_PROFILE_MEM_ALLOC(sizeof(dsi_profile_3gpp_type),
                                                     MODEM_MEM_CLIENT_DATA);

  if (tmp_prf == NULL) 
  {
    DS_PROFILE_LOGE("_3gpp_alloc_profile: FAILED DS_PROFILE_MEM_ALLOC", 0 );
    return NULL;
  }

  tmp_prf->prf = (ds_profile_3gpp_profile_info_type *)DS_PROFILE_MEM_ALLOC(
      sizeof(ds_profile_3gpp_profile_info_type),
      MODEM_MEM_CLIENT_DATA);

  if (tmp_prf->prf == NULL) 
  {
    DS_PROFILE_MEM_FREE( (void *)tmp_prf, MODEM_MEM_CLIENT_DATA );
    DS_PROFILE_LOGE("_3gpp_alloc_profile: FAILED DS_PROFILE_MEM_ALLOC", 0 );
    return NULL;
  }

  tmp_prf->self = tmp_prf;
  return tmp_prf;
}

/*===========================================================================
FUNCTION DSI_PROFILE_3GPP_DEALLOC_PROFILE
 
DESCRIPTION
  This function is used to free memory which was allocated for the local copy
  of the profile

PARAMETERS 
  ptr : pointer to local copy of profile

DEPENDENCIES 
  
RETURN VALUE 
  DSI_SUCCESS
  DSI_FAILURE
 
SIDE EFFECTS 
  
===========================================================================*/
static int dsi_profile_3gpp_dealloc_profile(
  void *ptr
)
{

  if ( ptr == NULL || ( ((dsi_profile_3gpp_type *)ptr)->prf == NULL ) ) 
  {
    DS_PROFILE_LOGE( "_3gpp_dealloc_profile: ptr NULL", 0);
    DS_PROFILE_LOGE( "_3gpp_dealloc_profile: EXIT with ERR", 0);
    return DSI_FAILURE;
  }

  DS_PROFILE_MEM_FREE( (void *) ((dsi_profile_3gpp_type *)ptr)->prf,
                  MODEM_MEM_CLIENT_DATA ); 
  DS_PROFILE_MEM_FREE( (void *)ptr,
                  MODEM_MEM_CLIENT_DATA );

  return DSI_SUCCESS;
}

/*===========================================================================
FUNCTION DSI_PROFILE_3GPP_SET_PARAM
 
DESCRIPTION
  This function is used to set 3GPP parameter value in the local copy
  of the profile

PARAMETERS 
  blob : pointer to local copy of profile
  ident : identifier whose value is to be set
  info : pointer to store value of identifier to be modified

DEPENDENCIES 
  
RETURN VALUE 
  DS_PROFILE_REG_RESULT_ERR_INVAL_IDENT
  DS_PROFILE_REG_RESULT_ERR_LEN_INVALID
  DS_PROFILE_REG_RESULT_SUCCESS
 
SIDE EFFECTS 
  
===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp_set_param( 
  void                        *blob,
  ds_profile_identifier_type   ident,
  const ds_profile_info_type  *info
)
{
  uint64  mask = 0;
  uint8 index = 0;

  /* Validate identifier */
  if ( !DS_PROFILE_3GPP_IDENT_IS_VALID( ident ) )
  {
    ident = DS_PROFILE_3GPP_PROFILE_PARAM_INVALID;
    return DS_PROFILE_REG_RESULT_ERR_INVAL_IDENT;
  }
  index = dsi_profile_3gpp_get_index_from_ident( ident );

  if( !( ( ident == DS_PROFILE_3GPP_PROFILE_PARAM_PDP_CONTEXT_APN)||
         ( ident == DS_PROFILE_3GPP_PROFILE_PARAM_AUTH_USERNAME) ||
         ( ident == DS_PROFILE_3GPP_PROFILE_PARAM_AUTH_PASSWORD) ) )
  {
    /* Validate info->buf and info->len  for all other except
      APN, username and password . They can have len as zero 
    */
    if ( !SET_INFO_IS_VALID( info, ds_profile_3gpp_profile_params_desc_tbl[index].len ) ) 
    {
      return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
    }
  }
  DS_PROFILE_LOGD("set 3gpp_set_param:calling ds_profile_3gpp_acc_mut_fn_tbl set_fn",0);
  /* get mask from identifier */
  CONVERT_IDENT_TO_MASK( mask, index );
  return ds_profile_3gpp_acc_mut_fn_tbl[index].set_fn(blob, mask, info);
}
/*===========================================================================
FUNCTION DSI_PROFILE_3GPP_UPDATE_LTE_ATTACH_PDN_PROFILES
 
DESCRIPTION
  This function is triggered when any profile resides in the attach profile
  list changed. Recalculation for attach profile will be called.

PARAMETERS 


DEPENDENCIES 
  
RETURN VALUE 
  DS_PROFILE_REG_RESULT_ERR_INVAL_IDENT
  DS_PROFILE_REG_RESULT_ERR_LEN_INVALID
  DS_PROFILE_REG_RESULT_SUCCESS
 
SIDE EFFECTS 
  
===========================================================================*/
#ifdef FEATURE_DATA_LTE
static ds_profile_status_etype dsi_profile_3gpp_update_lte_attach_pdn_profiles
(
   ds_profile_subs_etype subs_id
)
{
  ds_cmd_type                 *cmd_ptr = NULL;

  /*------------------------------------------------------------------
  send msg to DS task to refresh attach pdn list in cache
  -------------------------------------------------------------------*/
  DS_PROFILE_LOGD("dsi_profile_3gpp_update_lte_attach_pdn_profiles",0);
  cmd_ptr = ds_get_cmd_buf();
  if(NULL == cmd_ptr)
  {
	DS_PROFILE_LOGD("get cmd buf fails for refresh attach pdn list",0);
	return DS_PROFILE_REG_RESULT_FAIL;
  }
  cmd_ptr->hdr.cmd_id = DS_DSD_APM_REFRESH_ATTACH_PDN_LIST;
  ds_put_cmd(cmd_ptr);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}
#endif /*FEATURE_DATA_LTE*/

/*===========================================================================
FUNCTION DSI_PROFILE_3GPP_GET_PARAM
 
DESCRIPTION
  This function is used to get 3GPP parameter value from the local copy
  of the profile

PARAMETERS 
  blob : pointer to local copy of profile
  ident : identifier to get value
  info : pointer to store value of identifier fetched

DEPENDENCIES 
  
RETURN VALUE 
  DS_PROFILE_REG_RESULT_ERR_INVAL_IDENT
  DS_PROFILE_REG_RESULT_ERR_LEN_INVALID
  DS_PROFILE_REG_RESULT_SUCCESS
 
SIDE EFFECTS 
  
===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp_get_param(
  void                        *blob,
  ds_profile_identifier_type   ident,
  ds_profile_info_type        *info
)
{
  uint8 index = 0;
  /* Validate identifier */
  if ( !DS_PROFILE_3GPP_IDENT_IS_VALID( ident ) )
  {
    ident = DS_PROFILE_3GPP_PROFILE_PARAM_INVALID; 
    return DS_PROFILE_REG_RESULT_ERR_INVAL_IDENT;
  }
  index = dsi_profile_3gpp_get_index_from_ident( ident );
  /* Validate info->buf and info->len */
  if ( !GET_INFO_IS_VALID( info, ds_profile_3gpp_profile_params_desc_tbl[index].len ) ) 
  {
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }
  info->len = ds_profile_3gpp_profile_params_desc_tbl[index].len;
  DS_PROFILE_LOGD("get 3gpp_set_param:calling ds_profile_3gpp_acc_mut_fn_tbl set_fn",0);
  return ds_profile_3gpp_acc_mut_fn_tbl[index].get_fn(blob, info);
}

/*===========================================================================
FUNCTION DS_PROFILE_3GPP_GET_CALLBACK_TABLE_LIST
 
DESCRIPTION
  This function is used to get callback table list.

PARAMETERS
  void

DEPENDENCIES 
  
RETURN VALUE 
  Pointer to Callback Table List
 
SIDE EFFECTS 
 
===========================================================================*/
list_type* ds_profile_3gpp_get_callback_table_list
(  
  void
)
{
  return (&ds_profile_3gpp_callback_table_list);
}/* ds_profile_3gpp_get_callback_table_list */

/*===========================================================================

FUNCTION DS_PROFILE_3GPP_REGISTER_CALLBACK_TABLE

DESCRIPTION
  This function registers a callback function table of a client
  with DS profile.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  
===========================================================================*/
void ds_profile_3gpp_register_callback_table
(
  ds_profile_3gpp_cb_func_tbl_type  *cb_tbl_ptr /* Callback function table */
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if( cb_tbl_ptr != NULL )
  {
    /*----------------------------------------------------------------------
      Push the list item into the list
    ----------------------------------------------------------------------*/
    list_push_front(&(ds_profile_3gpp_callback_table_list), &(cb_tbl_ptr->link));
  }
  return;
} /* ds_profile_3gpp_register_callback_table() */

/*===========================================================================
FUNCTION DS_PROFILE_3GPP_INIT

DESCRIPTION
  This function is called on the library init. It initializes the function
  pointers to valid functions for 3gpp

PARAMETERS 
  fntbl : pointer to table of function pointers

DEPENDENCIES 
  
RETURN VALUE 
  returns the mask for 3gpp. (Used later as valid mask which is ORed value
  returned from all techs.
SIDE EFFECTS 
  
===========================================================================*/

uint8 ds_profile_3gpp_init ( tech_fntbl_type *fntbl )
{
  DS_PROFILE_LOGD("ds_profile_3gpp_init:initializing function pointers",0);
  /* Init function pointers */
  fntbl->alloc     = dsi_profile_3gpp_alloc_profile;
  fntbl->dealloc   = dsi_profile_3gpp_dealloc_profile;
  fntbl->set_param = dsi_profile_3gpp_set_param;
  fntbl->get_param = dsi_profile_3gpp_get_param;
#ifdef FEATURE_DATA_LTE
  fntbl->update_lte_attach_pdn_list_profiles = 
	                        dsi_profile_3gpp_update_lte_attach_pdn_profiles;
#endif
  /*----------------------------------------------------------------------------
    Initialize the list to hold handler tables of clients of DS Profile
  ----------------------------------------------------------------------------*/
  list_init(&(ds_profile_3gpp_callback_table_list));
  return (0x01 << DS_PROFILE_TECH_3GPP);
}

/*lint -restore Restore lint error 641*/

