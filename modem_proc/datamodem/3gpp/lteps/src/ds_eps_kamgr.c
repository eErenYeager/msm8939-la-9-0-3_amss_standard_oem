/*===========================================================================

  Copyright (c) 2009-2014 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                          INCLUDE FILES FOR MODULE

===========================================================================*/
#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"
#include "data_msg.h"

#ifdef FEATURE_DATA_LTE

#include "dsutil.h"
#include "ds_eps_kamgr_int.h"
#include "dsumtspdpreg.h"
#include "modem_mem.h"
#include "ps_route_iface.h"
#include "cm.h"
#include "ds_3gpp_pdn_context.h"
#include "ds_eps_pdn_context.h"
#include "ds_3gpp_apn_table.h"
#include "ds_3gppi_utils.h"
#include "ps_crit_sect.h"
#include "ps_iface.h"
#include <stringl/stringl.h>
#ifdef FEATURE_DATA_WLAN_MAPCON
#include "ds_wlan_util.h"
#endif  /* FEATURE_DATA_WLAN_MAPCON */

/*===========================================================================

                          MACRO DECLARATIONS

===========================================================================*/

#define DS_EPS_KAMGR_IS_3GPP_CURRENT_MODE(call_mode)  \
  (                                              \
      (call_mode == SYS_SYS_MODE_WCDMA) ||       \
      (call_mode == SYS_SYS_MODE_GSM)   ||       \
      (call_mode == SYS_SYS_MODE_LTE)   ||       \
      (call_mode == SYS_SYS_MODE_TDS)            \
  )
/*===========================================================================

                          GLOBAL VARIABLES

===========================================================================*/
ds_eps_kamgr_state                     *kamgr;

/*---------------------------------------------------------------------------
  Global to inform whether System status cback registration was successful
  or not
---------------------------------------------------------------------------*/
boolean                              system_status_cback_reg_status = TRUE;

/*-------------------------------------------------------------------------- 
 
----------------------------------------------------------------------------*/
const char                          *tags[2] = {"PROFILE_ID","TIMER_VAL"};

/*===========================================================================

                          FUNCTION DECLARATIONS

===========================================================================*/

/*===========================================================================
FUNCTION  DS_EPS_KAMGR_INIT

DESCRIPTION
  Initialization of all the required structures and variable for Keep alive
  manager
  
PARAMETERS
 None
 
DEPENDENCIES
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
void ds_eps_kamgr_init (void)
{
#ifdef FEATURE_DATA_WLAN_MAPCON
  int16                                          ps_errno = -1;
  int16                                          ps_sys_reg_ret_val = -1;
  int                                            subs_index = PS_SYS_DEFAULT_SUBS;
/*--------------------------------------------------------------------------*/

  DS_LTE_MSG0_MED("ds_eps_kamgr_init: Entered during bootup");

  /*-------------------------------------------------------------------------- 
   Register for System confirmation status event with PS to find out the
   preferred mode for the keep alive PDN, if the registration fails we free
   all the resources allocated so far
  ---------------------------------------------------------------------------*/  

  for (subs_index = PS_SYS_PRIMARY_SUBS; 
        subs_index < PS_SYS_SUBS_MAX; subs_index++)
  {
    ps_sys_reg_ret_val = ps_sys_event_reg_ex(PS_SYS_TECH_ALL,
                                             PS_SYS_EVENT_SYSTEM_STATUS_EX,
                                             subs_index,
                                             ds_eps_kamgr_ps_conf_status_cback,
                                             NULL,
                                             &ps_errno);
    if (ps_sys_reg_ret_val < 0)
    {
      DS_LTE_MSG2_HIGH ("ds_eps_kamgr_init: Error while "
                       "registering for PS_SYS_EVENT_SYSTEM_STATUS_EX event, "
                       "return value %d and error %d, freeing all the resources",
                        ps_sys_reg_ret_val, ps_errno);

      system_status_cback_reg_status = FALSE;
      ds_eps_kamgr_clean_up_resources();
      break;
    }
  }

#else
   DS_LTE_MSG0_HIGH ("ds_eps_kamgr_init: MAPCON feature disabled");
#endif /* FEATURE_DATA_WLAN_MAPCON */
}

/*===========================================================================
FUNCTION  DS_EPS_KAMGR_PS_CONF_STATUS_CBACK

DESCRIPTION
  This callback is invoked by PS when conf status is posted by MPPM.
  Specifically the current service status indication.
  
PARAMETERS
    tech_type:                       Technology for which event was registered
    event_name:                      Event that was registered
    subscription_id:                 As of now Default subscription_id
  * event_info_ptr:                  Event Info
  * user_data_ptr:                   Data passed by the user during registration
 

DEPENDENCIES
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
void ds_eps_kamgr_ps_conf_status_cback
(
  ps_sys_tech_enum_type                    tech_type,
  ps_sys_event_enum_type                   event_name,
  ps_sys_subscription_enum_type            subscription_id,
  void                                   * event_info_ptr,
  void                                   * user_data_ptr
)
{
#ifdef FEATURE_DATA_WLAN_MAPCON
  ds_cmd_type                             * cmd_ptr = NULL;
  ps_sys_system_status_ex_type            * sys_info_ptr = NULL;
  sys_modem_as_id_e_type                   subs_id = ds3g_get_ps_subs_id();
/*---------------------------------------------------------------------------*/

  /*-------------------------------------------------------------------------- 
    Sanity check done for input params
  ----------------------------------------------------------------------------*/

  if(event_info_ptr == NULL)
  {
    DS_LTE_MSG0_MED("ds_eps_kamgr_ps_conf_status_cback: Event info invalid");
    return;
  }

  if (PS_SYS_TECH_ALL != tech_type || 
      PS_SYS_EVENT_SYSTEM_STATUS_EX != event_name)
  {
    DS_LTE_MSG0_MED("ds_eps_kamgr_ps_conf_status_cback: Invalid Tech or Event");
    return;
  }

  if((ps_sys_subscription_enum_type)ds3gsubsmgr_subs_id_cm_to_ds(subs_id) != 
       subscription_id)
  {
    DS_LTE_MSG0_MED("ds_eps_kamgr_ps_conf_status_cback: Invalid Subscription ID");
    return;
  }

  if( (cmd_ptr = ds_get_cmd_buf()) == NULL )
  {
    DS_LTE_MSG0_ERROR("ds_eps_kamgr_ps_conf_status_cback: KAMGR can't get cmd"
                      " buf from DS task");
    return;
  }
   
  memset (cmd_ptr, 0, sizeof(ds_cmd_type));
  sys_info_ptr = (ps_sys_system_status_ex_type *)modem_mem_alloc
                  (sizeof(ps_sys_system_status_ex_type), MODEM_MEM_CLIENT_DATA);

  if (sys_info_ptr == NULL)
  {
    DS_LTE_MSG0_ERROR("ds_eps_kamgr_ps_conf_status_cback: Unable to allocate"
                      " memeory on heap, Exiting !!!!");
    return;
  }
  
  memscpy (sys_info_ptr,sizeof(ps_sys_system_status_ex_type),
           event_info_ptr,sizeof(ps_sys_system_status_ex_type));

  cmd_ptr->hdr.cmd_id = DS_EPS_KAMGR_SYS_INFO_CMD;
  cmd_ptr->cmd.dsumtsps_call_info.info_ptr = (void *)sys_info_ptr;
  ds_put_cmd_ext(cmd_ptr);

#else
  DS_LTE_MSG0_HIGH ("ds_eps_kamgr_ps_conf_status_cback: MAPCON feature "
                    "disabled");
#endif /* FEATURE_DATA_WLAN_MAPCON */
} /* ds_eps_kamgr_ps_conf_status_cback */

/*===========================================================================
FUNCTION  DS_EPS_KAMGR_FREE_IFACE_CBACK_BUF

DESCRIPTION
  This function is responsible for cleaning up Iface event cback buffers
  
PARAMETERS 
  None

DEPENDENCIES
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
void ds_eps_kamgr_free_iface_cback_buf
(
   void
)
{
  if(kamgr != NULL)
  {
    if(kamgr->ps_iface_down_ev_buf_ptr != NULL)
    {
     ps_iface_free_event_cback_buf(kamgr->ps_iface_down_ev_buf_ptr);
     kamgr->ps_iface_down_ev_buf_ptr = NULL;
    }

    if (kamgr->ps_iface_up_ev_buf_ptr != NULL)
    {
     ps_iface_free_event_cback_buf(kamgr->ps_iface_up_ev_buf_ptr);
     kamgr->ps_iface_up_ev_buf_ptr = NULL;
    }
    kamgr->active_ind = kamgr->active_ind & RESET_KAMGR;
    ds_eps_kamgr_retry_after_failure(PS_NET_DOWN_REASON_NOT_SPECIFIED, TRUE);
  }
  else
  {
    DS_LTE_MSG0_ERROR ("Keep ALive Manager Instance not active, No clean-up "
                       "needed");
  }
}

/*===========================================================================
FUNCTION DS_EPS_KAMGR_SETUP_IFACE_EVENT_CBACKS

DESCRIPTION
  This function registers for cbacks with IFACE UP and IFACE DOWN events

PARAMETERS
  None

DEPENDENCIES
  None.

RETURN VALUE  
  None.

SIDE EFFECTS
  None.

===========================================================================*/
void ds_eps_kamgr_setup_iface_event_cbacks
(
  void
)
{
  int16                                ps_iface_reg_result = -1;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ps_iface_reg_result = ps_iface_event_cback_reg (kamgr->ps_iface_ptr,
                                                  IFACE_UP_EV,
                                        kamgr->ps_iface_up_ev_buf_ptr);

  if(ps_iface_reg_result != 0)
  {
    /*---------------------------------------------------------------------
      Since we were not able to register for iface up event, there is no
      point of cback buffer so we free it up, since during next full
      service when we again try bring-up this will not cause a memory leak
    ----------------------------------------------------------------------*/
    DS_LTE_MSG0_MED("ds_eps_kamgr_setup_iface_event_cbacks: Iface up event "
                    "registration failed, Free the cback buffer");
    if (kamgr->ps_iface_up_ev_buf_ptr != NULL)
    {
      ps_iface_free_event_cback_buf(kamgr->ps_iface_up_ev_buf_ptr);
      kamgr->ps_iface_up_ev_buf_ptr = NULL;
    }
    kamgr->active_ind = (kamgr->active_ind & RESET_KAMGR) | (REG_FAILED_SET) ;
  }

  ps_iface_reg_result = ps_iface_event_cback_reg (kamgr->ps_iface_ptr,
                                                        IFACE_DOWN_EV,
                                          kamgr->ps_iface_down_ev_buf_ptr);

  if (ps_iface_reg_result != 0)
  {
    DS_LTE_MSG0_MED("ds_eps_kamgr_setup_iface_event_cbacks: Iface down event "
                    "registration failed, free the cback buffer");
    if(kamgr->ps_iface_down_ev_buf_ptr != NULL)
    {
      ps_iface_free_event_cback_buf(kamgr->ps_iface_down_ev_buf_ptr);
      kamgr->ps_iface_down_ev_buf_ptr = NULL;
    }
    kamgr->active_ind = (kamgr->active_ind & RESET_KAMGR) | (REG_FAILED_SET) ;
  }
}

/*===========================================================================
FUNCTION  DS_EPS_KAMGR_BRING_UP_KEEP_ALIVE_PDN

DESCRIPTION
  This function is responsible for initiating the bring up of the PDN that
  has to be kept alive
  
PARAMETERS 
  None

DEPENDENCIES
  None.

RETURN VALUE
  TRUE: If bring up was successful
  FALSE: If bring up failed

SIDE EFFECTS
  None

===========================================================================*/
boolean ds_eps_kamgr_bring_up_keep_alive_pdn 
(
  void
)
{
#ifdef FEATURE_DATA_WLAN_MAPCON
  boolean                               bring_up_ret_val = FALSE;
  boolean                               is_keep_alive_pdn_up = FALSE;
  ds_pdn_context_s                      *pdn_cntxt_p = NULL;
  sys_modem_as_id_e_type                subs_id = SYS_MODEM_AS_ID_NONE;
  ds3g_apn_name_type                   *apn_name = NULL;
  ds_pkt_sys_mode_e_type                pref_mode = DS_PKT_SYS_MODE_NONE;
/*----------------------------------------------------------------------------*/
  
  DS_LTE_MSG0_MED("ds_eps_kamgr_bring_up_keep_alive_pdn: Entered");

  subs_id = ds3g_get_ps_subs_id();
  do
  {
    if(kamgr == NULL)
    {
      DS_LTE_MSG0_ERROR("ds_eps_kamgr_bring_up_keep_alive_pdn: Keep alive "
                        "manager instance released");
      break;
    }

    /*------------------------------------------------------------------------- 
     1. Used to handle a case where due to APN name change to NULL APN,
     APN reconnect feature will tear down the Keep Alive PDN
     2. KAMGR will initiate retry mechanism, where after timer expiry this
     function will be called for bring-up
     3. Since KAMGR does not have support for NULL APN, we want to reject the
     bring-up
    --------------------------------------------------------------------------*/

    if (kamgr->apn_name.apn_string_len <= 0)
    {
       DS_LTE_MSG1_ERROR ("ds_eps_kamgr_bring_up_keep_alive_pdn: Profile does not"
                          " exist or a case of NULL APN %d; Bring-up failure",
                          (kamgr->apn_name).apn_string_len);
       break;
    }

    /*------------------------------------------------------------------------- 
     Suppose the Retry timer is running and the APN name is changed in the
     internal structure due to cback, then we need to check if LTE is still
     the preferred mode for the new APN
    --------------------------------------------------------------------------*/
    apn_name = &(kamgr->apn_name);
    pref_mode = ds3g_get_apn_pref_pkt_mode_ex(apn_name,subs_id);

    if (pref_mode != DS_PKT_SYS_MODE_LTE)
    {
      DS_LTE_MSG1_ERROR("ds_eps_kamgr_bring_up_keep_alive_pdn: Pref mode is %d;"
                        " Not LTE",pref_mode);
      break;
    }
    /*-------------------------------------------------------------------------- 
     If the cback buffer allocation fails then we ensure that retry is initiated
     after some time interval
    ----------------------------------------------------------------------------*/
    kamgr->ps_iface_ptr = NULL;

    kamgr->ps_iface_up_ev_buf_ptr = ps_iface_alloc_event_cback_buf(
                                          ds_eps_kamgr_iface_up_ev_cback,
                                          NULL);

    if(kamgr->ps_iface_up_ev_buf_ptr == NULL)
    {
      DS_LTE_MSG0_ERROR("ds_eps_kamgr_bring_up_keep_alive_pdn: Unable to "
                        "allocate cback buffers");
      kamgr->active_ind = kamgr->active_ind & RESET_KAMGR;
      ds_eps_kamgr_retry_after_failure(PS_NET_DOWN_REASON_NOT_SPECIFIED, TRUE);
      break;
    }

    kamgr->ps_iface_down_ev_buf_ptr = ps_iface_alloc_event_cback_buf(
                                          ds_eps_kamgr_iface_down_ev_cback,
                                          NULL);

    if(kamgr->ps_iface_down_ev_buf_ptr == NULL)
    {
      DS_LTE_MSG0_ERROR("ds_eps_kamgr_bring_up_keep_alive_pdn: Unable to "
                        "allocate cback buffers");
      kamgr->active_ind = kamgr->active_ind & RESET_KAMGR;
      ps_iface_free_event_cback_buf((void *)kamgr->ps_iface_up_ev_buf_ptr);
      ds_eps_kamgr_retry_after_failure(PS_NET_DOWN_REASON_NOT_SPECIFIED, TRUE);
      break;
    }

    /*-------------------------------------------------------------------------- 
    Performing an active bring-up to bring up the keep alive PDN, 
    In case the PDN is already up we change the status of keep alive flag to 
    true 
    ----------------------------------------------------------------------------*/
    PS_ENTER_CRIT_SECTION(&global_ps_crit_section);
    
    is_keep_alive_pdn_up = ds_pdn_cntxt_is_pdn_associated_for_apn 
                                          ((byte *)(kamgr->apn_name).apn_string,
                                           subs_id);

    if (!is_keep_alive_pdn_up)
    {
       bring_up_ret_val = ds_eps_pdn_cntxt_allocate_pdn_and_bringup_ifaces
                          ((kamgr->config_info).profile_no, 
                           &(kamgr->ps_iface_ptr),
                           subs_id);

       if(bring_up_ret_val == FALSE)
       {
         /*---------------------------------------------------------------------
          Since active bring-up failed we need to free the cback buffers
          allocated. Reset the state of keep alive manager and retry
         ----------------------------------------------------------------------*/
         PS_LEAVE_CRIT_SECTION(&global_ps_crit_section);
         DS_LTE_MSG0_ERROR("ds_eps_kamgr_bring_up_keep_alive_pdn: Active "
                           "bring-up failed, Freeing cback buffers allocated");
         kamgr->ps_iface_ptr = NULL;
         ds_eps_kamgr_free_iface_cback_buf();
         break;
       }
    }
    else
    {
       DS_LTE_MSG0_HIGH("ds_eps_kamgr_bring_up_keep_alive_pdn: Keep Alive PDN"
                      " already up, setting the Keep Alive PDN flag");
       is_keep_alive_pdn_up = ds_pdn_cntxt_get_v4_pdn_context(
                                (byte *)(kamgr->apn_name).apn_string,
                                &pdn_cntxt_p,
                                subs_id);
       if((!is_keep_alive_pdn_up) || 
          (!ds_3gpp_pdn_cntx_validate_pdn_context(pdn_cntxt_p)))
       {
         is_keep_alive_pdn_up = ds_pdn_cntxt_get_v6_pdn_context(
                                  (byte *)(kamgr->apn_name).apn_string,
                                  &pdn_cntxt_p,
                                  subs_id);
       }
       if(!ds_3gpp_pdn_cntx_validate_pdn_context(pdn_cntxt_p))
       {
         PS_LEAVE_CRIT_SECTION (&global_ps_crit_section);
         DS_LTE_MSG0_ERROR("ds_eps_kamgr_bring_up_keep_alive_pdn: PDN "
                           "context validation failed, freeing resources");
         ds_eps_kamgr_free_iface_cback_buf ();
         break;
       }
       pdn_cntxt_p->ds_pdn_context_dyn_p->keep_alive_pdn = TRUE;
       kamgr->ps_iface_ptr = DSPDNCNTXT_GET_V4_PS_IFACE_PTR(pdn_cntxt_p);
       if(kamgr->ps_iface_ptr == NULL)
       {
         kamgr->ps_iface_ptr = DSPDNCNTXT_GET_V6_PS_IFACE_PTR(pdn_cntxt_p);
       }
    }
    PS_LEAVE_CRIT_SECTION (&global_ps_crit_section);

    /*------------------------------------------------------------------------
      Register cbacks for IFACE down and IFACE UP event
    -------------------------------------------------------------------------*/
    if(kamgr->ps_iface_ptr == NULL)
    {
      DS_LTE_MSG0_ERROR("No Iface allocated for Keep Alive Manager, Exit !!!");
      ds_eps_kamgr_free_iface_cback_buf();
      break;
    }

    ds_eps_kamgr_setup_iface_event_cbacks();
    bring_up_ret_val = TRUE;

    if(bring_up_ret_val == TRUE)
    {
      kamgr->companion_iface = ds_3gpp_pdn_get_companion_iface 
                                                      (kamgr->ps_iface_ptr);
    }
  }while (0);

  DS_LTE_MSG1_HIGH("ds_eps_kamgr_bring_up_keep_alive_pdn, Value returned is %d",
                   bring_up_ret_val);

  return bring_up_ret_val;
#else
  DS_LTE_MSG0_HIGH ("ds_eps_kamgr_bring_up_keep_alive_pdn: MAPCON feature disabled");
  return FALSE;
#endif /* FEATURE_DATA_WLAN_MAPCON */
} /* ds_eps_kamgr_bring_up_keep_alive_pdn */


/*===========================================================================
FUNCTION DS_EPS_KAMGR_CHECK_WLAN_STATUS

DESCRIPTION
  This function process the system info given by MPPM to decide whether
  WLAN is available or not

PARAMETERS
  sys_info_ptr:                    System information reported by MPPM

DEPENDENCIES
  None.

RETURN VALUE  
  TRUE: If WLAN is available
  FALSE: Otherwise

SIDE EFFECTS
  None.

===========================================================================*/
boolean ds_eps_kamgr_check_wlan_status
(
  ps_sys_system_status_ex_type             *sys_info_ptr
)
{
  boolean                                   retval = FALSE;
  uint8                                     index = 0;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if(sys_info_ptr == NULL)
  {
    DS_LTE_MSG0_ERROR("Invalid Input Params, Exiting !!!!");
    return retval;
  }

  for(index = 0; index < sys_info_ptr->num_avail_sys; index++)
  {
    if(PS_SYS_NETWORK_WLAN == sys_info_ptr->avail_sys[index].technology)
    {
      retval = TRUE;
      DS_LTE_MSG1_MED("WLAN Available, Entry match at index: %d", index);
    }
  }

  return retval;

}

/*===========================================================================
FUNCTION DS_EPS_KAMGR_DEREG_IFACE_CBACKS_CMD

DESCRIPTION
  This function process the system info given by MPPM to decide whether
  Keep alive PDN can be brought up or not

PARAMETERS
  sys_info_ptr:                    System information reported by MPPM

DEPENDENCIES
  None.

RETURN VALUE  
  None.

SIDE EFFECTS
  None.

===========================================================================*/

void ds_eps_kamgr_process_sys_info
(
  void                                    *sys_info
)
{  
#ifdef FEATURE_DATA_WLAN_MAPCON 
  ps_sys_system_status_ex_type            *sys_info_ptr = NULL;
  uint32                                   num_apns_available = 0;
  uint8                                    index = 0;
  ps_sys_network_enum_type                 tech = PS_SYS_NETWORK_MAX; 
  ps_sys_rat_ex_enum_type                  rat_val = PS_SYS_RAT_MAX;  
  uint64                                   so_mask = PS_SYS_SO_EX_UNSPECIFIED;
  boolean                                  bring_up_ret_val = FALSE;  
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -- - - - - -*/
  /*------------------------------------------------------------------------- 
    Extract the APN name from the Profile ID, we do not have support for NULL
    APN
  --------------------------------------------------------------------------*/
  do
  {
    if(kamgr == NULL)
    {
      DS_LTE_MSG0_ERROR("ds_eps_kamgr_process_sys_info: Keep alive manager "
                        "released or not allocated yet");
      break;
    }
    ds_eps_kamgr_get_keep_alive_apn_from_profile();

    if(kamgr->apn_name.apn_string_len <= 0)
    {
      DS_LTE_MSG1_ERROR ("ds_eps_kamgr_process_sys_info: Profile does not exist"
                         " or a case of NULL APN %d",
                         (kamgr->apn_name).apn_string_len);
      break;
    }

    if (sys_info == NULL)
    {
      DS_LTE_MSG0_ERROR ("ds_eps_kamgr_process_sys_info: Invalid Input Param");
      break;
    }
 
    sys_info_ptr = (ps_sys_system_status_ex_type *)sys_info;
    /*-------------------------------------------------------------------------- 
    Loop through all the available APNs for a match with keep alive APN, then 
    find out the preferred RAT for it 

    If LTE is the preferred RAT we try to bring up the Keep Alive APN, for any 
    other case the bring up is blocked till further cbacks
    ----------------------------------------------------------------------------*/
    num_apns_available = sys_info_ptr->num_avail_apns;

    DS_LTE_MSG1_MED ("ds_eps_kamgr_process_sys_info: Num of APNs passed are"
                     " %d",num_apns_available);

    kamgr->wlan_availability = ds_eps_kamgr_check_wlan_status (sys_info_ptr);

    if(num_apns_available == 0)
    {
      DS_LTE_MSG0_HIGH("ds_eps_kamgr_process_sys_info: Using DSD's preferred"
                       " mode information for bring_up");
      bring_up_ret_val = ds_eps_kamgr_use_dsd_pref_sys_for_bring_up (
                                                                 sys_info_ptr);
    }
    else
    {
      for(index = 0; index < num_apns_available; index++)
      {
        if(strncasecmp((const char *)sys_info_ptr->apn_pref_sys_info[index].apn_name, 
             (const char *)(kamgr->apn_name).apn_string, DS_SYS_MAX_APN_LEN) == 0)
        {
          DS_LTE_MSG1_MED("ds_eps_kamgr_process_sys_info: Match found in the"
                          " system information passed by MPPM at %d",index);
          break;
        }
      }

      if(index < num_apns_available)
      {
        tech = sys_info_ptr->apn_pref_sys_info[index].avail_sys[PREF_RAT_INDEX].
               technology;
        rat_val = sys_info_ptr->apn_pref_sys_info[index].avail_sys[PREF_RAT_INDEX].
                  rat_value;
        so_mask = sys_info_ptr->apn_pref_sys_info[index].avail_sys[PREF_RAT_INDEX].
                  so_mask;

        DS_LTE_MSG3_MED("ds_eps_kamgr_process_sys_info: Technology: %d, "
                        "Rat_Mask:%d, So_Mask:%d",tech,rat_val,so_mask);
        bring_up_ret_val = ds_eps_kamgr_process_system_status_info (
                                                                    tech, 
                                                                    rat_val
                                                                    );
      }
      else
      {
        DATA_MSG_SPRINTF_1(MSG_LEGACY_ERROR,
                           "ds_eps_kamgr_process_sys_info:No sys mode for %s "
                           "Using DSD preferred sys mode for bring-up ",
                           (kamgr->apn_name).apn_string);
        bring_up_ret_val = ds_eps_kamgr_use_dsd_pref_sys_for_bring_up (
                                                                   sys_info_ptr);
      }
    }
  }while (0);
  
  if (sys_info != NULL)
  {
    modem_mem_free(sys_info, MODEM_MEM_CLIENT_DATA);
  }

  DS_LTE_MSG1_MED("ds_eps_kamgr_process_sys_info: Bring-Up status %d",
                  bring_up_ret_val);

#else
  DS_LTE_MSG0_HIGH ("ds_eps_kamgr_process_sys_info: MAPCON feature disabled");
#endif /* FEATURE_DATA_WLAN_MAPCON */
}

/*===========================================================================
FUNCTION  DS_EPS_KAMGR_FORCE_TEAR_DOWN

DESCRIPTION
  This function forces tear down of an iface
  
PARAMETERS 
  this_iface_ptr:   Iface which has to be torn down

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/

void ds_eps_kamgr_force_tear_down 
(
   ps_iface_type                     *iface_ptr
)
{
#ifdef FEATURE_DATA_WLAN_MAPCON
  int16                               ps_errno = 0;
/*--------------------------------------------------------------------------*/
  /*------------------------------------------------------------------------ 
    In case of RAT change from LTE to any other 3GPP RAT, we force an active
    iface tear-down
  --------------------------------------------------------------------------*/
  DS_LTE_MSG0_MED ("ds_eps_kamgr_force_tear_down: Entered");

  if (iface_ptr == NULL)
  {
    DS_LTE_MSG0_ERROR("ds_eps_kamgr_force_tear_down: Illegal Input Parameter");
    return;
  }

  if(ps_iface_active_tear_down_cmd(iface_ptr, &ps_errno, NULL) == 0 
     || ps_errno != DS_EWOULDBLOCK)
  {
    DS_LTE_MSG1_ERROR ("ds_eps_kamgr_force_tear_down: Cannot tear-down the "
                       "iface Error: %d",ps_errno);
  }
  else
  {
    DS_LTE_MSG0_MED("ds_eps_kamgr_force_tear_down: Waiting for IFACE DOWN"
                    " event");
  }
#else
   DS_LTE_MSG0_HIGH ("ds_eps_kamgr_force_tear_down: MAPCON feature disabled");
#endif /* FEATURE_DATA_WLAN_MAPCON */
}

/*===========================================================================
FUNCTION DS_EPS_KAMGR_LOCAL_CLEAN_UP

DESCRIPTION
  This function cleans up resources w.r.t Keep Alive Manager when Keep Alive
  PDN is being used by secondary client and additional PDN is up 

PARAMETERS
  pdn_context_ptr :             PDN context of Keep Alive PDN

DEPENDENCIES
  None.

RETURN VALUE  
  None.

SIDE EFFECTS
  None.

===========================================================================*/
void ds_eps_kamgr_local_clean_up
(
  ds_pdn_context_s                      *pdn_context_ptr
)
{
  if(kamgr == NULL)
  {
    DS_LTE_MSG0_ERROR("ds_eps_kamgr_local_clean_up: Keep alive "
                      "manager instance released");
    return;
  }

  if(!ds_3gpp_pdn_cntx_validate_pdn_context(pdn_context_ptr))
  {
    DS_LTE_MSG0_ERROR("ds_eps_kamgr_local_clean_up: Invalid PDN "
                      "context, clean-up stopped");
    return;
  }

  pdn_context_ptr->ds_pdn_context_dyn_p->keep_alive_pdn = FALSE;

  kamgr->active_ind = kamgr->active_ind & RESET_KAMGR;
  ds_eps_kamgr_dereg_iface_cbacks_and_free_cback_buffers(
                                               kamgr->ps_iface_ptr,
                                               FALSE);
}

/*===========================================================================
FUNCTION DS_EPS_KAMGR_SETUP_IFACE_DATA

DESCRIPTION
  This function sets up the iface resources for cback on IFACE UP and
  IFACE DOWN events

PARAMETERS
  None

DEPENDENCIES
  None.

RETURN VALUE  
  None.

SIDE EFFECTS
  None.

===========================================================================*/
void ds_eps_kamgr_setup_iface_data
(
  ps_iface_type                       *iface_ptr
)
{
  boolean                              setup_status = FALSE;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  do
  {
    kamgr->ps_iface_ptr = NULL;
    kamgr->ps_iface_ptr = iface_ptr;

    /*---------------------------------------------------------------------- 
      ps_iface_ptr is now storing the companion iface so we clean-up companion
      iface ptr
    -----------------------------------------------------------------------*/
    kamgr->companion_iface = NULL;

    kamgr->ps_iface_up_ev_buf_ptr = ps_iface_alloc_event_cback_buf(
                                          ds_eps_kamgr_iface_up_ev_cback,
                                          NULL);
    if(kamgr->ps_iface_up_ev_buf_ptr == NULL)
    {
      DS_LTE_MSG0_ERROR("ds_eps_kamgr_setup_iface_data: Unable to "
                        "allocate cback buffers");
      kamgr->active_ind = kamgr->active_ind & RESET_KAMGR;
      ds_eps_kamgr_retry_after_failure(PS_NET_DOWN_REASON_NOT_SPECIFIED, TRUE);
      break;
    }

    kamgr->ps_iface_down_ev_buf_ptr = ps_iface_alloc_event_cback_buf(
                                          ds_eps_kamgr_iface_down_ev_cback,
                                          NULL);

    if(kamgr->ps_iface_down_ev_buf_ptr == NULL)
    {
      DS_LTE_MSG0_ERROR("ds_eps_kamgr_setup_iface_data: Unable to "
                        "allocate cback buffers");
      kamgr->active_ind = kamgr->active_ind & RESET_KAMGR;
      ps_iface_free_event_cback_buf((void *)kamgr->ps_iface_up_ev_buf_ptr);
      ds_eps_kamgr_retry_after_failure(PS_NET_DOWN_REASON_NOT_SPECIFIED, TRUE);
      break;
    }

    /*------------------------------------------------------------------------
      Register cbacks for IFACE down and IFACE UP change event
    -------------------------------------------------------------------------*/
    ds_eps_kamgr_setup_iface_event_cbacks();

    setup_status = TRUE;
  }while (0);

  if(setup_status)
  {
    kamgr->companion_iface = ds_3gpp_pdn_get_companion_iface 
                                                    (kamgr->ps_iface_ptr);
  }
}

/*===========================================================================
FUNCTION DS_EPS_KAMGR_PROCESS_IFACE_DOWN_EV_CBACK

DESCRIPTION
  This function process the iface down event and decides whether Keep alive
  Manager can re-attempt to bring up the PDN or not
  
PARAMETERS 
  event_info:                    Iface information reported by PS
  user_data:                     User data passed by the function posting cmd

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/

void ds_eps_kamgr_process_iface_down_ev_cback
(
  ps_iface_event_info_u_type               iface_down_event_info,
  void                                   * user_data                          
)
{
#ifdef FEATURE_DATA_WLAN_MAPCON
  ps_sys_ioctl_pdn_throttle_info_type      *pdn_throttle_info = NULL;
  int16                                    ps_errno = 0;
  ds_pkt_sys_mode_e_type                   current_mode = 
                                                      DS_PKT_SYS_MODE_NONE;
  boolean                                  permanent_failure = FALSE;
  ps_iface_type                           *iface_ptr = NULL;
  sys_modem_as_id_e_type                   subs_id = SYS_MODEM_AS_ID_NONE;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if(kamgr == NULL)
  {
    DS_LTE_MSG0_ERROR("ds_eps_kamgr_process_iface_down_ev_cback: Keep alive"
                      " manager released or not allocated yet");
    return;
  }

  iface_ptr = (ps_iface_type *)user_data;

  if(kamgr->ps_iface_ptr != iface_ptr)
  {
    DS_LTE_MSG2_MED("ds_eps_kamgr_process_iface_down_ev_cback: Invoked for wrong"
                    " iface 0x%x instead of 0x%x", iface_ptr, 
                    kamgr->ps_iface_ptr);
    return;
  }

  if(kamgr->companion_iface != NULL &&
     PS_IFACE_IS_UP(kamgr->companion_iface) == TRUE)
  {
    DS_LTE_MSG1_HIGH ("Companion Iface: 0x%x is UP, don't initiate retry "
                      "mechanism", kamgr->companion_iface);
    ds_eps_kamgr_dereg_iface_cbacks_and_free_cback_buffers(
                                                      kamgr->ps_iface_ptr,
                                                      permanent_failure);
    ds_eps_kamgr_setup_iface_data (kamgr->companion_iface);
    return;
  }

  pdn_throttle_info = (ps_sys_ioctl_pdn_throttle_info_type *)modem_mem_alloc
    (sizeof(ps_sys_ioctl_pdn_throttle_info_type), MODEM_MEM_CLIENT_DATA);

  if (pdn_throttle_info == NULL)
  {
    DS_LTE_MSG0_ERROR("ds_eps_kamgr_process_iface_down_ev_cback: Unable to "
                      "allocate memory for throttle information");
    return;
  }
  memset(pdn_throttle_info,0, sizeof(ps_sys_ioctl_pdn_throttle_info_type));
  kamgr->active_ind = kamgr->active_ind & RESET_KAMGR;

  /*-------------------------------------------------------------------------- 
  The net down reason passed by the network decides whether we re-initiate the 
  PDN connectivity for Keep Alive PDN 
   
  Temporary cause code: We start a timer and wait for the timer to expire before 
  sending out a new request. 
  The timer duration changes depending on whether the previous cause code was 
  same as the current one or not. 
  After 3 retries (for same cause code) we start a timer for 10 secs after which 
  we retry 
   
  Throttling based cause code: Here we gather the PDN throttle information using 
  the ps_sys_ioctl and start a timer for remaining throttled time, when the timer
  expires we re-initiate the bring-up. 
  In case we the APN is not throttled, we wait for some time before retrying
   
  Permanent cause code: We just clean up the resources right away  
  ----------------------------------------------------------------------------*/
  subs_id = ds3g_get_ps_subs_id();
  current_mode = ds3g_get_apn_pref_pkt_mode_ex(&(kamgr->apn_name), subs_id);

  DS_LTE_MSG1_MED("ds_eps_kamgr_process_iface_down_ev_cback: Current mode %d",
                  current_mode);

  if(DS_EPS_KAMGR_IS_3GPP_CURRENT_MODE(current_mode) &&
     kamgr->apn_name.apn_string_len > 0)
  {
    /*-------------------------------------------------------------------------- 
    De-register for cbacks for the Um iface since during the next bring up, we 
    will be allocated a new iface 
    ----------------------------------------------------------------------------*/
    switch (iface_down_event_info.iface_down_info.netdown_reason)
    {
    /*-------------------------------------------------------------------------- 
    Temporary Cause codes
    ----------------------------------------------------------------------------*/
      case PS_NET_DOWN_REASON_INSUFFICIENT_RESOURCES:
      case PS_NET_DOWN_REASON_NOT_SPECIFIED:
      case PS_NET_DOWN_REASON_LLC_SNDCP_FAILURE:
      case PS_NET_DOWN_REASON_NSAPI_ALREADY_USED:
      case PS_NET_DOWN_REASON_QOS_NOT_ACCEPTED:
      case PS_NET_DOWN_REASON_NETWORK_FAILURE:
      case PS_NET_DOWN_REASON_UMTS_REATTACH_REQ:
      case PS_NET_DOWN_REASON_FEATURE_NOT_SUPPORTED:
      case PS_NET_DOWN_REASON_ESM_INFO_NOT_RECEIVED:
      case PS_NET_DOWN_REASON_PDN_CONN_DOES_NOT_EXIST:
      case PS_NET_DOWN_REASON_INVALID_TRANSACTION_ID:
      case PS_NET_DOWN_REASON_MULTI_CONN_TO_SAME_PDN_NOT_ALLOWED:
        if (kamgr->retry_counter <= (kamgr->config_info).maximum_retries)
        {
          ds_eps_kamgr_retry_after_failure (iface_down_event_info.iface_down_info.
                                                   netdown_reason, FALSE);
        }
        else
        {
          rex_set_timer(&(kamgr->keep_alive_bring_up_timer),
                        (rex_timer_cnt_type)(kamgr->config_info).
                                                       maximum_timer_val);
        }
        break;
      /*-------------------------------------------------------------------------- 
      Throttling Cause codes
      ----------------------------------------------------------------------------*/
      case PS_NET_DOWN_REASON_OPERATOR_DETERMINED_BARRING:
      case PS_NET_DOWN_REASON_GGSN_REJECT:
      case PS_NET_DOWN_REASON_ACTIVATION_REJECT:
      case PS_NET_DOWN_REASON_OPTION_NOT_SUPPORTED:
      case PS_NET_DOWN_REASON_OPTION_UNSUBSCRIBED:
      case PS_NET_DOWN_REASON_OPTION_TEMP_OOO:
      case PS_NET_DOWN_REASON_IP_V6_ONLY_ALLOWED:
      case PS_NET_DOWN_REASON_APN_TYPE_CONFLICT:
      case PS_NET_DOWN_REASON_UNKNOWN_INFO_ELEMENT:
      case PS_NET_DOWN_REASON_CONDITIONAL_IE_ERROR:
      case PS_NET_DOWN_REASON_UNKNOWN_PDP:
      case PS_NET_DOWN_REASON_INVALID_PCSCF_ADDR:
      case PS_NET_DOWN_REASON_PROTOCOL_ERROR:
      case PS_NET_DOWN_REASON_MESSAGE_TYPE_UNSUPPORTED:
      case PS_NET_DOWN_REASON_MSG_TYPE_NONCOMPATIBLE_STATE:
      case PS_NET_DOWN_REASON_MESSAGE_INCORRECT_SEMANTIC:
      case PS_NET_DOWN_REASON_INVALID_MANDATORY_INFO:
      case PS_NET_DOWN_REASON_MSG_AND_PROTOCOL_STATE_UNCOMPATIBLE:
      case PS_NET_DOWN_REASON_NO_SRV:
        if(pdn_throttle_info != NULL)
        {
          if(0 > ps_sys_ioctl(PS_SYS_TECH_3GPP,
                       PS_SYS_IOCTL_PDN_THROTTLE_INFO,
                       (void *)pdn_throttle_info,
                       &ps_errno))
          {
            DS_LTE_MSG2_HIGH("ps_sys_ioctl %d operation returned %d, retrying"
                             "with temporaray cause code handling logic",
                             PS_SYS_IOCTL_PDN_THROTTLE_INFO, ps_errno);
            ds_eps_kamgr_retry_after_failure(iface_down_event_info.
                                             iface_down_info.netdown_reason, 
                                             FALSE);
          }
          else
          {
            ds_eps_kamgr_process_pdn_throttle_info_for_bring_up (
                                              pdn_throttle_info,
                                              iface_down_event_info.iface_down_info.
                                                     netdown_reason);
          }
        }
        break;
      /*-------------------------------------------------------------------------- 
      Permanent Cause codes
      ---------------------------------------------------------------------------*/
      case PS_NET_DOWN_REASON_UNKNOWN_APN:
      case PS_NET_DOWN_REASON_AUTH_FAILED:
        DS_LTE_MSG1_HIGH("ds_eps_kamgr_process_iface_down_ev_cback, Permanent"
                         " blocking of keep alive manager due to %d",
                         iface_down_event_info.iface_down_info.netdown_reason);
        permanent_failure = TRUE;
        break;

      case PS_NET_DOWN_REASON_REGULAR_DEACTIVATION:
      case PS_NET_DOWN_REASON_EMM_DETACHED:
        DS_LTE_MSG0_MED("ds_eps_kamgr_process_iface_down_ev_cback: Regular "
                        "deactivation cause code");
        rex_set_timer(&(kamgr->keep_alive_bring_up_timer),
                     (rex_timer_cnt_type)(kamgr->config_info).default_timer_val);
        break;

      default:
        DS_LTE_MSG0_MED("ds_eps_kamgr_process_iface_down_ev_cback: Un-handled"
                        " Cause code");
        rex_set_timer(&(kamgr->keep_alive_bring_up_timer),
                     (rex_timer_cnt_type)((kamgr->config_info).default_timer_val*2));
        break;
    }
  }
  else
  {
    DS_LTE_MSG1_HIGH("ds_eps_kamgr_process_iface_down_ev_cback: Current mode"
                     " is not 3gpp, %d Thereby we clean-up iface resources",
                     current_mode);
  }
  ds_eps_kamgr_dereg_iface_cbacks_and_free_cback_buffers(kamgr->ps_iface_ptr,
                                                         permanent_failure);

  modem_mem_free ((void *)pdn_throttle_info, MODEM_MEM_CLIENT_DATA);
  pdn_throttle_info = NULL;
#else
  DS_LTE_MSG0_HIGH ("ds_eps_kamgr_process_iface_down_ev_cback: MAPCON feature"
                    " disabled");
#endif /*FEATURE_DATA_WLAN_MAPCON*/
}

/*===========================================================================
FUNCTION DS_EPS_KAMGR_PROCESS_IFACE_UP_EV_CBACK

DESCRIPTION
  This function process the iface up event and resets the retry counter
  
PARAMETERS 
  event_info:                    Iface information reported by PS
  user_data:                     User data passed by the function posting cmd

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/

void ds_eps_kamgr_process_iface_up_ev_cback
(
  void                                    *user_data                          
)
{
#ifdef FEATURE_DATA_WLAN_MAPCON
  ps_iface_type                           *iface_ptr = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if(kamgr == NULL)
  {
    DS_LTE_MSG0_ERROR("ds_eps_kamgr_process_iface_up_ev_cback: Keep alive"
                      " manager released or not allocated yet");
    return;
  }

  iface_ptr = (ps_iface_type *)user_data;

  if(kamgr->ps_iface_ptr != iface_ptr)
  {
    DS_LTE_MSG2_MED("ds_eps_kamgr_process_iface_up_ev_cback: Invoked for wrong"
                    " iface 0x%x instead of 0x%x",
                    iface_ptr, kamgr->ps_iface_ptr);
    return;
  }

  kamgr->retry_counter = RESET_KAMGR;
  kamgr->prev_down_reason = RESET_KAMGR;
#else
  DS_LTE_MSG0_HIGH ("ds_eps_kamgr_process_iface_up_ev_cback: MAPCON feature"
                     "disabled");
#endif /* FEATURE_DATA_WLAN_MAPCON */
}

/*===========================================================================
FUNCTION DS_EPS_KAMGR_PROCESS_ALL_IFACE_UP_EV_CBACK

DESCRIPTION
  This function process the iface up event cback which is received as a part
  of WLAN disable Indication
  
PARAMETERS 
  user_data:                     User data passed by the function posting cmd

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/

void ds_eps_kamgr_process_all_iface_up_ev_cback
(
  void                                    *user_data                          
)
{
#ifdef FEATURE_DATA_WLAN_MAPCON
  ps_iface_type                           *iface_ptr = NULL;
  ds_pdn_context_s                        *pdn_context_ptr = NULL;
  ds_3gpp_iface_s                         *ds_iface_p = NULL;
  boolean                                  clean_up_needed = FALSE;
  ps_iface_type                           *companion_iface = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  do
  {
    if(kamgr == NULL)
    {
      DS_LTE_MSG0_ERROR("ds_eps_kamgr_process_all_iface_up_ev_cback: Keep alive"
                        " manager released or not allocated yet");
      break;
    }

    iface_ptr = (ps_iface_type *)user_data;

    if((kamgr->active_ind != RESET_KAMGR) &&
       (kamgr->ps_iface_ptr != NULL))
    {
      if(kamgr->ps_iface_ptr != iface_ptr &&
         kamgr->companion_iface != iface_ptr)
      {
        DS_LTE_MSG1_MED("ds_eps_kamgr_process_all_iface_up_ev_cback: Cback receieved"
                        " for IFACE:0x%x", iface_ptr);

        ds_iface_p = (ds_3gpp_iface_s *)
                               ((kamgr->ps_iface_ptr)->client_data_ptr); 

        if (ds_iface_p == NULL)
        {
          DS_LTE_MSG2_ERROR("No DS iface associated with PS iface 0x%x:%d",
                             kamgr->ps_iface_ptr, 
                             (kamgr->ps_iface_ptr)->instance);
          break;
        }
        pdn_context_ptr = (ds_pdn_context_s*)(ds_iface_p->client_data_ptr);

        if(!ds_3gpp_pdn_cntx_validate_pdn_context(pdn_context_ptr))
        {
          DS_LTE_MSG1_ERROR("Invalid PDN context associated with "
                             "PS iface:0x%x",kamgr->ps_iface_ptr);
          break;
        }
        /*------------------------------------------------------------------------- 
          2 cases:
          1. WLAN Available - No action taken, dereg from listener and free cback
          buffer
          2. WLAN Un-available
            2.1 Check if a new PDN is brought up, if it is we check if Keep Alive
            PDN is in use, if not we tear down and clean-up
            2.2 If it is in use then we clean keep alive PDN flag so that when
            client tears down the call MH can clean-up
            2.3 If a new PDN is not up, we clear the keep alive PDN flag since
            MH will make it default PDN by design
        -------------------------------------------------------------------------*/
        if(kamgr->wlan_availability == FALSE)
        {
          if(ds_pdn_cntx_is_last_prevailing_pdn_in_lte(pdn_context_ptr) == FALSE)
          {
            companion_iface = ds_3gpp_pdn_get_companion_iface (kamgr->ps_iface_ptr);

            if(PS_IFACE_IS_IN_USE (kamgr->ps_iface_ptr) == FALSE &&
               PS_IFACE_IS_IN_USE (companion_iface) == FALSE) 
            {
              ds_eps_kamgr_force_tear_down(kamgr->ps_iface_ptr);
            }
            else
            {
              ds_eps_kamgr_local_clean_up(pdn_context_ptr);
            }
            clean_up_needed = TRUE;
          }/* Not Last standing PDN so tear it */
          else
          {
            ds_eps_kamgr_local_clean_up(pdn_context_ptr);
          }
        } /* WLAN availability check */
        else
        {
          clean_up_needed = TRUE;
        }
      } /* IFace check to see new IFACE is brought up */
    } /* Keep Alive PDN still active Check*/
    else
    {
      clean_up_needed = TRUE;
    }
  }while(0);

  if(clean_up_needed == TRUE)
  {
    if(kamgr->ps_iface_up_all_ev_buf_ptr != NULL)
    {
      ps_iface_event_cback_dereg(NULL,
                                 IFACE_UP_EV,
                                 kamgr->ps_iface_up_all_ev_buf_ptr);
      DS_LTE_MSG0_MED("Freeing iface down event buffer");
      ps_iface_free_event_cback_buf(kamgr->ps_iface_up_all_ev_buf_ptr);
      kamgr->ps_iface_up_all_ev_buf_ptr = NULL;
    }
    else
    {
      DS_LTE_MSG0_MED("ds_eps_kamgr_process_all_iface_up_ev_cback: Iface up "
                      "event buffer already released");
    }
  }
  else
  {
    DS_LTE_MSG0_HIGH("ds_eps_kamgr_process_all_iface_up_ev_cback: Additional"
                     " PDN not up on LTE, holding onto Keep Alive PDN");
  }
#else
  DS_LTE_MSG0_HIGH ("ds_eps_kamgr_process_iface_up_ev_cback: MAPCON feature"
                     "disabled");
#endif /* FEATURE_DATA_WLAN_MAPCON */
}

/*===========================================================================
FUNCTION DS_EPS_KAMGR_PROCESS_TEAR_DOWN_TIMER_EXPIRY_CBACK

DESCRIPTION
  This function is responsible for tearing down the Keep Alive PDN once
  tear-down timer expires if needed
  
PARAMETERS 
  None.

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/
void ds_eps_kamgr_process_tear_down_timer_expiry_cback
(
  void
)
{
#ifdef FEATURE_DATA_WLAN_MAPCON
  ds_pdn_context_s                           *pdn_context_ptr = NULL;
  ds_3gpp_iface_s                            *ds_iface_p      = NULL;
  ps_iface_type                              *companion_iface = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  do
  {
    if(kamgr == NULL)
    {
      DS_LTE_MSG0_ERROR("ds_eps_kamgr_process_tear_down_timer_expiry_cback: "
                        "Keep alive manager instance released");
      break;
    }

    /*------------------------------------------------------------------------- 
      Fetch/Validate the PDN context pointer from the iface pointer
    -------------------------------------------------------------------------*/
    ds_iface_p = (ds_3gpp_iface_s *)((kamgr->ps_iface_ptr)->client_data_ptr); 

    if (ds_iface_p == NULL)
    {
      DS_LTE_MSG2_ERROR("No DS iface associated with PS iface 0x%x:%d",
                         kamgr->ps_iface_ptr,
                         (kamgr->ps_iface_ptr)->instance);
      break;
    }
    pdn_context_ptr = (ds_pdn_context_s*)(ds_iface_p->client_data_ptr);

    if(!ds_3gpp_pdn_cntx_validate_pdn_context(pdn_context_ptr))
    {
      DS_LTE_MSG1_ERROR("Invalid PDN context associated with PS iface:0x%x",
                         kamgr->ps_iface_ptr);
      break;
    }
    /*------------------------------------------------------------------------ 
      On Timer expiry we check if WLAN is available or not
      1. Not available, we check if additional PDN is up excluding Keep Alive
      PDN.
      2. If it is we check the state of Keep Alive PDN and if no client is
      bound to it we tear it down else we reset the Keep ALive PDN flag so
      that later on client can tear it down
      3. If No additional PDN is up we wait for it to come up before we do the
      clean-up
    -------------------------------------------------------------------------*/
    if(kamgr->wlan_availability == FALSE)
    {
      if(ds_pdn_cntx_is_last_prevailing_pdn_in_lte(pdn_context_ptr) == FALSE)
      {
        companion_iface = ds_3gpp_pdn_get_companion_iface (kamgr->ps_iface_ptr);

        if(PS_IFACE_IS_IN_USE (kamgr->ps_iface_ptr) == FALSE &&
           PS_IFACE_IS_IN_USE (companion_iface) == FALSE) 
        {
          ds_eps_kamgr_force_tear_down(kamgr->ps_iface_ptr);
        }
        else
        {
          ds_eps_kamgr_local_clean_up (pdn_context_ptr);
        }
      }/* Not Last standing PDN so tear it */
      else
      {
        ds_eps_kamgr_handle_wlan_disable_ind();
      }
    } /* WLAN availability check */
    else
    {
      DS_LTE_MSG0_MED("ds_eps_kamgr_process_tear_down_timer_expiry_cback: WI-FI"
                      " available, tear-down stopped");
    }
  }while(0);

#else
  DS_LTE_MSG0_HIGH ("ds_eps_kamgr_process_tear_down_timer_expiry_cback: "
                    "MAPCON feature disabled");
#endif /*FEATURE_DATA_WLAN_MAPCON*/
}

/*===========================================================================
FUNCTION DS_EPS_KAMGR_PROCESS_TIMER_EXPIRY_CBACK

DESCRIPTION
  This function is responsible for bring-up of keep alive manager after timer
  expiry
  
PARAMETERS 
  None.

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/
void ds_eps_kamgr_process_timer_expiry_cback
(
   void
)
{
#ifdef FEATURE_DATA_WLAN_MAPCON
  boolean                                     bring_up_ret_val = FALSE;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  if(kamgr == NULL)
  {
    DS_LTE_MSG0_ERROR("ds_eps_kamgr_process_timer_expiry_cback: Keep alive"
                      " manager not active; Error");
    return;
  }
  /*------------------------------------------------------------------------ 
    On Timer expiry we check whether LTE is in full service and Keep alive
    manager is not active, if both conditions are satisfied we initiate the
    bring-up again.
  -------------------------------------------------------------------------*/
  if(kamgr->lte_srv_status == TRUE && kamgr->active_ind == RESET_KAMGR &&
     kamgr->wlan_availability == TRUE) 
  {
    kamgr->active_ind = (kamgr->active_ind & RESET_KAMGR) | (SET_KAMGR);
    bring_up_ret_val = ds_eps_kamgr_bring_up_keep_alive_pdn();
  }
  else
  {
    DS_LTE_MSG0_MED("ds_eps_kamgr_process_timer_expiry_cback: LTE/WLAN not in "
                    "full Service or keep alive manager already active, "
                    "Keep Alive PDN bring up not initiated");
  }

  DS_LTE_MSG1_MED("ds_eps_kamgr_process_timer_expiry_cback: Bring Up status"
                  " after keep alive PDN Timer wait %d", bring_up_ret_val);
#else
  DS_LTE_MSG0_HIGH ("ds_eps_kamgr_process_timer_expiry_cback: "
                    "MAPCON feature disabled");
#endif /*FEATURE_DATA_WLAN_MAPCON*/
}

/*===========================================================================
FUNCTION DS_EPS_KAMGR_PROCESS_APN_NAME_CHG

DESCRIPTION
  This function is responsible for update the internal structure with the
  updated APN name
  
PARAMETERS 
  None.

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/
void ds_eps_kamgr_process_apn_name_chg
(
  void                         *user_data
)
{
#ifdef FEATURE_DATA_WLAN_MAPCON
  uint16                       *profile_ptr = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  profile_ptr = (uint16 *)user_data;

  do
  {

    if(kamgr == NULL || profile_ptr == NULL)
    {
      DS_LTE_MSG0_ERROR("ds_eps_kamgr_process_apn_name_chg: Keep alive "
                        "manager instance released");
      break; 
    }

    if (*profile_ptr != kamgr->config_info.profile_no)
    {
      DS_LTE_MSG0_HIGH ("Keep Alive Manager's APN name has not changed: No "
                         "Update reqired");
      break;
    }

    ds_eps_kamgr_get_keep_alive_apn_from_profile();

    if(kamgr->apn_name.apn_string_len <= 0)
    {
      DS_LTE_MSG1_ERROR ("ds_eps_kamgr_process_apn_name_chg: NULL/Invalid APN"
                         " %d",(kamgr->apn_name).apn_string_len);
      break;
    }

    DATA_MSG_SPRINTF_1(MSG_LEGACY_MED,
                     "ds_eps_kamgr_process_apn_name_chg Updated APN name: %s ",
                     (kamgr->apn_name).apn_string);

    /*---------------------------------------------------------------------------
      Consider a scenario:
      1. Keep Alive APN: vzwinternet, Network changes to : vzwapps
      2. Pref mode for vzwapps is WLAN, so Keep Alive PDN torn down
      3. Now Network again changes it back to vzwinternet
      4. Now UE needs to re-initiate the bring-up of Keep Alive PDN
    ----------------------------------------------------------------------------*/

    if (kamgr->lte_srv_status == TRUE && kamgr->active_ind == RESET_KAMGR &&
        (!(rex_timer_cnt_type)rex_get_timer(&(kamgr->keep_alive_bring_up_timer)))
        && (kamgr->wlan_availability == TRUE))
    {
      DS_LTE_MSG0_HIGH ("ds_eps_kamgr_process_apn_name_chg: Inititating bring-up"
                        " of Keep Alive PDN");
      (void)ds_eps_kamgr_bring_up_keep_alive_pdn();
    }

  }while (0);

  if (profile_ptr != NULL)
  {
    modem_mem_free ((void *)profile_ptr, MODEM_MEM_CLIENT_DATA);
  }

#else
  DS_LTE_MSG0_HIGH ("ds_eps_kamgr_process_apn_name_chg: MAPCON feature disabled");
#endif /*FEATURE_DATA_WLAN_MAPCON*/
}

/*===========================================================================
FUNCTION       DS_EPS_KAMGR_PROCESS_CMD

DESCRIPTION    Process DS cmds 
 
PARAMETERS     cmd_ptr :              Command information pointer 

DEPENDENCIES   NONE

RETURN VALUE   NONE

SIDE EFFECTS   NONE
===========================================================================*/
void ds_eps_kamgr_process_cmd
(
  ds_cmd_type                    *cmd_ptr
)
{
#ifdef FEATURE_DATA_WLAN_MAPCON
  if(cmd_ptr == NULL)
  {
    DS_LTE_MSG0_HIGH("ds_eps_kamgr_process_cmd: NULL CMD PTR");
    return;
  }

  DS_LTE_MSG1_HIGH("ds_eps_kamgr_process_cmd: command ID=%d",cmd_ptr->hdr.cmd_id);

  switch(cmd_ptr->hdr.cmd_id)
  {
    case DS_EPS_KAMGR_SYS_INFO_CMD:
      ds_eps_kamgr_process_sys_info (cmd_ptr->cmd.dsumtsps_call_info.info_ptr);
      break;

    case DS_EPS_KAMGR_IFACE_DOWN_EV_CBACK_CMD:
      ds_eps_kamgr_process_iface_down_ev_cback
                                   (cmd_ptr->cmd.dsumtsps_call_info.event_info,
                                    cmd_ptr->cmd.dsumtsps_call_info.user_data);
      break;

    case DS_EPS_KAMGR_IFACE_UP_EV_CBACK_CMD:
      ds_eps_kamgr_process_iface_up_ev_cback
                                   (cmd_ptr->cmd.dsumtsps_call_info.user_data);
      break;

    case DS_EPS_KAMGR_TIMER_EXPIRY_CBACK_CMD:
      ds_eps_kamgr_process_timer_expiry_cback();
      break;

    case DS_EPS_KAMGR_APN_NAME_CHG_CMD:
      ds_eps_kamgr_process_apn_name_chg 
                                    (cmd_ptr->cmd.dsumtsps_call_info.user_data);
      break;

    case DS_EPS_KAMGR_ALL_IFACE_UP_EV_CBACK_CMD:
      ds_eps_kamgr_process_all_iface_up_ev_cback
                                    (cmd_ptr->cmd.dsumtsps_call_info.user_data);
      break;

    case DS_EPS_KAMGR_TEAR_DOWN_TIMER_EXPIRY_CBACK_CMD:
      ds_eps_kamgr_process_tear_down_timer_expiry_cback();
      break;

    default:
      DS_LTE_MSG1_ERROR("ds_eps_kamgr_process_cmd: Command %d: not for KAMGR",
                        cmd_ptr->hdr.cmd_id);
      break;
  }
#else
   DS_LTE_MSG0_HIGH ("ds_eps_kamgr_process_cmd: MAPCON feature disabled");
#endif /* FEATURE_DATA_WLAN_MAPCON */
}

/*===========================================================================
FUNCTION  DS_EPS_KAMGR_IFACE_DOWN_EV_CBACK

DESCRIPTION
  This function gets called when iface down event is posted
  
PARAMETERS 
  this_iface_ptr:   Iface on which down event was posted
  event:            Event that caused the cback to be invoked
  event_info:       Event info posted by PS
  user_data_ptr:    Data to be passed to the cback function

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
void ds_eps_kamgr_iface_down_ev_cback
(
  ps_iface_type                           *this_iface_ptr,
  ps_iface_event_enum_type                 event,
  ps_iface_event_info_u_type               event_info,
  void                                    *user_data_ptr
)
{
#ifdef FEATURE_DATA_WLAN_MAPCON
  ds_cmd_type                             * cmd_ptr = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if(event != IFACE_DOWN_EV )
  {
    DS_LTE_MSG1_ERROR("Cback invoked for a wrong event; %d", event);
    return;
  }

  DS_LTE_MSG2_HIGH("ds_eps_kamgr_iface_down_ev_cback: Received cback for %d "
                   "event with down reason as 0x%x",event,
                   event_info.iface_down_info.netdown_reason);

  
  if( (cmd_ptr = ds_get_cmd_buf()) == NULL )
  {
    DS_LTE_MSG0_ERROR("ds_eps_kamgr_iface_down_ev_cback: KAMGR can't get cmd"
                      " buf from DS task");
    ASSERT(0);
  }
  
  memset (cmd_ptr, 0, sizeof(ds_cmd_type));

  cmd_ptr->hdr.cmd_id = DS_EPS_KAMGR_IFACE_DOWN_EV_CBACK_CMD;
  memscpy (&(cmd_ptr->cmd.dsumtsps_call_info.event_info),
           sizeof(cmd_ptr->cmd.dsumtsps_call_info.event_info),
           &event_info, sizeof(event_info));
  cmd_ptr->cmd.dsumtsps_call_info.user_data = (void *)this_iface_ptr;
  ds_put_cmd_ext(cmd_ptr);

#else
   DS_LTE_MSG0_HIGH ("ds_eps_kamgr_iface_down_ev_cback: MAPCON feature "
                     "disabled");
#endif /* FEATURE_DATA_WLAN_MAPCON */
}

/*===========================================================================
FUNCTION  DS_EPS_KAMGR_KEEP_ALIVE_PDN_TEAR_DOWN_TIMER_CB

DESCRIPTION
  This function gets called when keep alive pdn tear down timer expires,
  we now post a cmd and as a part of that we initiate tear-down
  
PARAMETERS 
  cback_param:                  Parameter to be passed to the cback function

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
void ds_eps_kamgr_keep_alive_pdn_tear_down_timer_cb
(
  unsigned long                               cback_param
)
{
#ifdef FEATURE_DATA_WLAN_MAPCON
  ds_cmd_type                             * cmd_ptr = NULL;
/*--------------------------------------------------------------------------*/
  DS_LTE_MSG0_MED("Keep alive PDN Timer cback : Entered");

  if( (cmd_ptr = ds_get_cmd_buf()) == NULL )
  {
    DS_LTE_MSG0_ERROR("ds_eps_kamgr_keep_alive_pdn_timer_cb: KAMGR can't get cmd"
                      " buf from DS task: Cannot re-initiate bring-up");
    return;
  }

  memset (cmd_ptr, 0, sizeof(ds_cmd_type));

  cmd_ptr->hdr.cmd_id = DS_EPS_KAMGR_TEAR_DOWN_TIMER_EXPIRY_CBACK_CMD;
  ds_put_cmd_ext(cmd_ptr);

#else
   DS_LTE_MSG0_HIGH ("ds_eps_kamgr_keep_alive_pdn_timer_cb: MAPCON feature "
                     "disabled");
#endif /* FEATURE_DATA_WLAN_MAPCON */
}

/*===========================================================================
FUNCTION  DS_EPS_KAMGR_KEEP_ALIVE_PDN_TIMER_CB

DESCRIPTION
  This function gets called when keep alive pdn retry timer expires,
  we now initate a new request for bring up of keep alive PDN
  
PARAMETERS 
  cback_param:                  Parameter to be passed to the cback function

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
void ds_eps_kamgr_keep_alive_pdn_timer_cb 
(
  unsigned long                               cback_param
)
{
#ifdef FEATURE_DATA_WLAN_MAPCON
  ds_cmd_type                             * cmd_ptr = NULL;
/*--------------------------------------------------------------------------*/
  DS_LTE_MSG0_MED("Keep alive PDN Timer cback : Entered");

  if( (cmd_ptr = ds_get_cmd_buf()) == NULL )
  {
    DS_LTE_MSG0_ERROR("ds_eps_kamgr_keep_alive_pdn_timer_cb: KAMGR can't get cmd"
                      " buf from DS task: Cannot re-initiate bring-up");
    return;
  }

  memset (cmd_ptr, 0, sizeof(ds_cmd_type));

  cmd_ptr->hdr.cmd_id = DS_EPS_KAMGR_TIMER_EXPIRY_CBACK_CMD;
  ds_put_cmd_ext(cmd_ptr);

#else
   DS_LTE_MSG0_HIGH ("ds_eps_kamgr_keep_alive_pdn_timer_cb: MAPCON feature "
                     "disabled");
#endif /* FEATURE_DATA_WLAN_MAPCON */
}

/*==============================================================================
FUNCTION  DS_EPS_KAMGR_PROCESS_PDN_THROTTLE_INFO_FOR_BRING_UP

DESCRIPTION
  This function gets called when keep alive pdn timer expires, we now initate
  a new request for bring up of keep alive PDN
  
PARAMETERS 
  pdn_throttle_info:               Structure containing throttle information

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

================================================================================*/
void ds_eps_kamgr_process_pdn_throttle_info_for_bring_up
(
   ps_sys_pdn_throttle_info_type                     *throttle_info,
   ps_iface_net_down_reason_type                      down_reason
)
{
#ifdef FEATURE_DATA_WLAN_MAPCON
  uint16                                              index = 0;
/*-------------------------------------------------------------------------------*/

  DS_LTE_MSG0_MED("ds_eps_kamgr_process_pdn_throttle_info_for_bring_up: "
                  "Entered");

  if(throttle_info == NULL || kamgr == NULL)
  {
    DS_LTE_MSG0_MED("ds_eps_kamgr_process_pdn_throttle_info_for_bring_up: "
                    "Invalid function call made");
    return;
  }

  for(index = 0; index < throttle_info->num_apn; index++)
  {
    if(strncasecmp((const char*)throttle_info->pdn_throttle_info[index].apn_string, 
          (const char*)kamgr->apn_name.apn_string, DS_SYS_MAX_APN_LEN) == 0)
    {
      DS_LTE_MSG1_MED("ds_eps_kamgr_process_pdn_throttle_info_for_bring_up, "
                    "APN match at index %d ",index);
      break;
    }
  }
  /*---------------------------------------------------------------------------
    If
      None of the IP types are throttled then we revert to default timer logic
    Else
      depending on IP type that is throttled for a longer time, we decide the
      back-off timer value
  ----------------------------------------------------------------------------*/
  if (index == throttle_info->num_apn || 
      (throttle_info->pdn_throttle_info[index].is_ipv4_throttled == FALSE 
       && throttle_info->pdn_throttle_info[index].is_ipv6_throttled == FALSE))
  {
    DS_LTE_MSG0_MED("ds_eps_kamgr_process_pdn_throttle_info_for_bring_up, APN"
                   " not throttled initiating bring up after waiting for "
                   "some time");
    ds_eps_kamgr_retry_after_failure(down_reason, FALSE);
  }
  else
  {
   kamgr->bring_up_timer_val = 
     (throttle_info->pdn_throttle_info[index].remaining_ipv4_throttled_time >
     throttle_info->pdn_throttle_info[index].remaining_ipv6_throttled_time) ?
     (throttle_info->pdn_throttle_info[index].remaining_ipv4_throttled_time) :
     (throttle_info->pdn_throttle_info[index].remaining_ipv6_throttled_time);

   rex_set_timer(&(kamgr->keep_alive_bring_up_timer),(rex_timer_cnt_type)
                kamgr->bring_up_timer_val);
  }
#else
   DS_LTE_MSG0_HIGH ("ds_eps_kamgr_process_pdn_throttle_info_for_bring_up: "
                     "MAPCON feature disabled");
#endif /* FEATURE_DATA_WLAN_MAPCON */
}

/*==============================================================================
FUNCTION DS_EPS_KAMGR_DEREG_IFACE_CBACKS

DESCRIPTION
  This function is called to clean up all the cback resources associated with
  an iface
 
PARAMETERS 
  this_iface_ptr:          Iface whose call backs and buffers have to be cleaned
                           up

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

================================================================================*/
void ds_eps_kamgr_dereg_iface_cbacks_and_free_cback_buffers
(
  ps_iface_type                                 *this_iface_ptr,
  boolean                                        clean_up
)
{
  /*---------------------------------------------------------------------------- 
    Since during every active bring up of keep alive manager we will be allocating
    a new iface so we free the iface resources every time the keep alive PDN is
    brought down
      - In case of Permanent cause code we also end up cleaning the only instance
        of Keep alive manager
  -----------------------------------------------------------------------------*/
#ifdef FEATURE_DATA_WLAN_MAPCON
  DS_LTE_MSG0_MED("ds_eps_kamgr_clean_up_iface_cbacks: Entered");

  if(kamgr->ps_iface_down_ev_buf_ptr != NULL)
  {
    ps_iface_event_cback_dereg(this_iface_ptr,
                               IFACE_DOWN_EV,
                               kamgr->ps_iface_down_ev_buf_ptr);
    DS_LTE_MSG0_MED("Freeing iface down event buffer");
    ps_iface_free_event_cback_buf(kamgr->ps_iface_down_ev_buf_ptr);
    kamgr->ps_iface_down_ev_buf_ptr = NULL;
  }
  else
  {
    DS_LTE_MSG0_MED("ds_eps_kamgr_dereg_iface_cbacks: Iface down "
                  "event buffer already released");
  }

  if(kamgr->ps_iface_up_ev_buf_ptr != NULL)
  {
    ps_iface_event_cback_dereg(this_iface_ptr,
                               IFACE_UP_EV,
                               kamgr->ps_iface_up_ev_buf_ptr);
    DS_LTE_MSG0_MED("Freeing iface up event buffer");
    ps_iface_free_event_cback_buf(kamgr->ps_iface_up_ev_buf_ptr);
    kamgr->ps_iface_up_ev_buf_ptr = NULL;
  }
  else
  {
    DS_LTE_MSG0_MED("ds_eps_kamgr_dereg_iface_cbacks: "
                  "iface up event buffer already released");
  }

  if (clean_up == TRUE)
  {
    ds_eps_kamgr_clean_up_resources();
  }
#else
  DS_LTE_MSG0_HIGH ("ds_eps_kamgr_dereg_iface_cbacks_and_free_cback_buffers: "
                  "MAPCON feature disabled");
#endif /* FEATURE_DATA_WLAN_MAPCON */
}

/*==============================================================================
FUNCTION  DS_EPS_KAMGR_CLEAN_UP_RESOURCES

DESCRIPTION
  This function is called to clean up all the resources associated with the keep
  alive manager
  
PARAMETERS 
  this_iface_ptr:                Iface associated with Keep alive manager

DEPENDENCIES
  None

RETURN VALUE
  TRUE:                          Clean up successful
  FALSE:                         Clean up failed

SIDE EFFECTS
  None

================================================================================*/

void ds_eps_kamgr_clean_up_resources 
(
  void
)
{
#ifdef FEATURE_DATA_WLAN_MAPCON
  int16                            ps_errno = -1;
  int16                            ret_val = -1;
  int                              subs_index = PS_SYS_DEFAULT_SUBS;
/*------------------------------------------------------------------------------*/

  /*--------------------------------------------------------------------------   
  At last we de-register from PS for system status notification and then clean 
  up the instance of keep alive manager 
  ----------------------------------------------------------------------------*/

  for (subs_index = PS_SYS_PRIMARY_SUBS; 
        subs_index < PS_SYS_SUBS_MAX; subs_index++)
  {
    ret_val = ps_sys_event_dereg_ex(PS_SYS_TECH_ALL,
                                    PS_SYS_EVENT_SYSTEM_STATUS_EX,
                                    subs_index,
                                    ds_eps_kamgr_ps_conf_status_cback,
                                    &ps_errno);
    if(ret_val < 0)
    {
      DS_LTE_MSG2_HIGH ("ds_eps_kamgr_clean_up_resources, Error while "
                        "de-registering for PS_SYS_EVENT_SYSTEM_STATUS_EX event,"
                        " return value %d and error %d ",ret_val, ps_errno);
    }
  }

  if(kamgr != NULL)
  {
    DS_LTE_MSG0_MED ("ds_eps_kamgr_clean_up_resources: Freeing Keep alive "
                     "manager instance");
    modem_mem_free((void *)kamgr, MODEM_MEM_CLIENT_DATA);
  }
  kamgr = NULL;
#else
  DS_LTE_MSG0_HIGH ("ds_eps_kamgr_clean_up_resources: MAPCON feature disabled");
#endif /* FEATURE_DATA_WLAN_MAPCON */
}

/*===========================================================================
FUNCTION  DS_EPS_KAMGR_RETRY_AFTER_FAILURE

DESCRIPTION
  This function incorporates the retry logic after we temporaray cause code
  as net down reason or we are not able to extract throttling info when we
  receive a throttling based cause code
  
PARAMETERS 
  down_reason:                            down reason received from network

DEPENDENCIES
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/

void ds_eps_kamgr_retry_after_failure 
(
  ps_iface_net_down_reason_type                    down_reason,
  boolean                                          bringup_failure
)
{
#ifdef FEATURE_DATA_WLAN_MAPCON
  if(kamgr == NULL)
  {
    DS_LTE_MSG0_ERROR("ds_eps_kamgr_retry_after_failure: Keep alive"
                      "manager not active");
    return;
  }
  /*----------------------------------------------------------------------- 
   If it is a bring-up failure then we retry after a fixed period of time
   everytime (default timer val)
   In other case we use a concept of incremental timer, the timer increment,
   default timer val, maximum number of retries and maximum timer value
   is all configurable through an efs file
  ------------------------------------------------------------------------*/
  if(bringup_failure == FALSE)
  {
    /*------------------------------------------------------------------- 
      If the previous net down reason is same as current down reason, we
      use an incremental timer
      Else, we reset the counter and timer and start with the default
      timer value
    --------------------------------------------------------------------*/
    if(kamgr->prev_down_reason != down_reason)
    {
      kamgr->retry_counter = RESET_KAMGR;
      kamgr->prev_down_reason = down_reason;
      kamgr->bring_up_timer_val = RESET_KAMGR;
    }
  
    if(kamgr->bring_up_timer_val != RESET_KAMGR)
    {
      kamgr->bring_up_timer_val = kamgr->bring_up_timer_val + 
                                (kamgr->config_info).incremental_timer_val;
    }
    else
    { 
      kamgr->bring_up_timer_val = (kamgr->config_info).default_timer_val;
    }
  }
  else
  {
    kamgr->bring_up_timer_val = (kamgr->config_info).default_timer_val;
  }

  (kamgr->retry_counter)++;
  rex_set_timer(&(kamgr->keep_alive_bring_up_timer),
                  (rex_timer_cnt_type)(kamgr->bring_up_timer_val));
#else
   DS_LTE_MSG0_HIGH ("ds_eps_kamgr_retry_after_failure: MAPCON feature"
                     " disabled");
#endif /* FEATURE_DATA_WLAN_MAPCON */
}


#ifdef TEST_FRAMEWORK
#error code not present
#endif /* TEST_FRAMEWORK*/

/*===========================================================================
FUNCTION  DS_EPS_KAMGR_IFACE_UP_EV_CBACK

DESCRIPTION
  This function gets called when iface up event is posted, called for all
  ifaces
  
PARAMETERS 
  this_iface_ptr:   Iface on which up event was posted
  event:            Event that caused the cback to be invoked
  event_info:       Event info posted by PS
  user_data_ptr:    Data to be passed to the cback function

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
void ds_eps_kamgr_all_iface_up_ev_cback
(
  ps_iface_type                           *this_iface_ptr,
  ps_iface_event_enum_type                 event,
  ps_iface_event_info_u_type               event_info,
  void                                    *user_data_ptr
)
{
#ifdef FEATURE_DATA_WLAN_MAPCON
  ds_cmd_type                            * cmd_ptr = NULL;  
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /*----------------------------------------------------------------------- 
   This cback is used an indication saying bring-up was successful and to
   reset the retry mechanism counters
  ------------------------------------------------------------------------*/
  if(event != IFACE_UP_EV )
  {
    DS_LTE_MSG1_MED("Cback invoked for a wrong event; %d", event);
    return;
  }

  DS_LTE_MSG1_HIGH("ds_eps_kamgr_all_iface_up_ev_cback: Received cback for %d "
                   "event",event);

  if( (cmd_ptr = ds_get_cmd_buf()) == NULL )
  {
    DS_LTE_MSG0_ERROR("ds_eps_kamgr_all_iface_up_ev_cback: KAMGR can't get cmd"
                      " buf from DS task");
    return;
  }

  memset (cmd_ptr, 0, sizeof(ds_cmd_type));

  cmd_ptr->hdr.cmd_id = DS_EPS_KAMGR_ALL_IFACE_UP_EV_CBACK_CMD;
  cmd_ptr->cmd.dsumtsps_call_info.user_data = (void *)this_iface_ptr;
  ds_put_cmd_ext(cmd_ptr);

#else
   DS_LTE_MSG0_HIGH ("ds_eps_kamgr_all_iface_up_ev_cback: MAPCON feature "
                     "disabled");
#endif /* FEATURE_DATA_WLAN_MAPCON */
}

/*===========================================================================
FUNCTION  DS_EPS_KAMGR_HANDLE_WLAN_DISABLE_IND

DESCRIPTION
  This function takes the required actions on WLAN Disable indication; tearing
  down the Keep Alive PDN if needed 
  
PARAMETERS 
  None

DEPENDENCIES
  None

RETURN VALUE
  TRUE: If the required actions were taken successfully
  FALSE: Otherwise

SIDE EFFECTS
  None

===========================================================================*/
boolean ds_eps_kamgr_handle_wlan_disable_ind
(
  void
)
{
  int                                       ps_iface_reg_result = -1;
  ds_pdn_context_s                         *pdn_context_ptr = NULL;
  ds_3gpp_iface_s                          *ds_iface_p      = NULL;
  boolean                                   retval = FALSE;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  do
  {
    if(kamgr == NULL)
    {
      DS_LTE_MSG0_ERROR("ds_eps_kamgr_handle_wlan_disable_ind: Keep alive "
                        "manager instance released");
      break;
    }

    /*------------------------------------------------------------------------- 
      Fetch/Validate the PDN context pointer from the iface pointer
    -------------------------------------------------------------------------*/
    ds_iface_p = (ds_3gpp_iface_s *)((kamgr->ps_iface_ptr)->client_data_ptr); 

    if (ds_iface_p == NULL)
    {
      DS_LTE_MSG2_ERROR("No DS iface associated with PS iface 0x%x:%d",
                         kamgr->ps_iface_ptr,
                         (kamgr->ps_iface_ptr)->instance);
      break;
    }
    pdn_context_ptr = (ds_pdn_context_s*)(ds_iface_p->client_data_ptr);

    if(!ds_3gpp_pdn_cntx_validate_pdn_context(pdn_context_ptr))
    {
      DS_LTE_MSG1_ERROR("Invalid PDN context associated with PS iface:0x%x",
                         kamgr->ps_iface_ptr);
      break;
    }
  /*------------------------------------------------------------------------ 
    Check if Keep Alive PDN is last standing PDN
      If yes, then register for IFACE_UP event for all ifaces
      The moment a new iface comes up indicates an additional PDN is UP
   
      Once additional PDN is UP, check for any clients of Keep Alive PDN
      If no clients start a timer before we tear it down
      Else, if there are clients just clear the keep alive PDN flag 
  --------------------------------------------------------------------------*/

    if(ds_pdn_cntx_is_last_prevailing_pdn_in_lte(pdn_context_ptr))
    {
      kamgr->ps_iface_up_all_ev_buf_ptr = ps_iface_alloc_event_cback_buf(
                                            ds_eps_kamgr_all_iface_up_ev_cback,
                                            NULL);
      if(kamgr->ps_iface_up_all_ev_buf_ptr == NULL)
      {
        DS_LTE_MSG0_ERROR("Unable to allocate buffer for IFACE UP Event "
                          "Listener");
        break;
      }

      ps_iface_reg_result = ps_iface_event_cback_reg(NULL,
                                                     IFACE_UP_EV,
                                           kamgr->ps_iface_up_all_ev_buf_ptr);
      if(ps_iface_reg_result != 0)
      {
        DS_LTE_MSG0_ERROR("Unable to register IFACE UP Event Listener, Freeing "
                          "cback buffer allocated");
        if (kamgr->ps_iface_up_all_ev_buf_ptr != NULL)
        {
          ps_iface_free_event_cback_buf(kamgr->ps_iface_up_all_ev_buf_ptr);
          kamgr->ps_iface_up_all_ev_buf_ptr = NULL;
        }
        break;
      }

      retval = TRUE;
    }
    else
    {
      rex_set_timer(&(kamgr->keep_alive_bring_down_timer),
                    (kamgr->config_info).teardown_backoff_timer);
    }
    retval = TRUE;

  }while(0);

  return retval;
}

/*===========================================================================
FUNCTION DS_EPS_KAMGR_PROCESS_SYSTEM_STATUS_INFO

DESCRIPTION
  This function process the system status information recieved and
  decides whether LTE is preferred system or not, if it is we bring up the
  keep alive PDN
 
PARAMETERS
  tech:                            Technology (3gpp/3gpp2)
  rat_val:                         RAT value
  so_mask:                         Mask
 
DEPENDENCIES
  None.

RETURN VALUE  
  TRUE:                            If bring-up successful
  FALSE:                           If bring-up failed

SIDE EFFECTS

===========================================================================*/

boolean ds_eps_kamgr_process_system_status_info
(
  ps_sys_network_enum_type                 tech,
  ps_sys_rat_ex_enum_type                  rat_val 
)
{
#ifdef FEATURE_DATA_WLAN_MAPCON
  boolean                                  bring_up_ret_val = FALSE;
  ps_iface_type                           *companion_iface = NULL;
/*----------------------------------------------------------------------------*/
  /*------------------------------------------------------------------------- 
    Case 1:
    WLAN available and LTE in full service, we bring up Keep Alive PDN
  --------------------------------------------------------------------------*/
  if(kamgr->wlan_availability == TRUE)
  {
    if(tech == PS_SYS_NETWORK_3GPP && rat_val == PS_SYS_RAT_EX_3GPP_LTE )
    {
      DS_LTE_MSG0_MED("LTE full service reported");
      kamgr->lte_srv_status = TRUE;
      /*------------------------------------------------------------------------ 
        If Keep alive PDN is not up and no back-off timer is in progress, then
        we initiate bring-up of keep alive PDN
      -------------------------------------------------------------------------*/
      if(kamgr->active_ind != SET_KAMGR && 
         (!(rex_timer_cnt_type)rex_get_timer(&(kamgr->keep_alive_bring_up_timer))))
      {
        DS_LTE_MSG0_MED("Keep alive manager not active, Initiating bring up of "
                        "Keep alive PDN");
        kamgr->active_ind = (kamgr->active_ind & RESET_KAMGR) | (SET_KAMGR);
        bring_up_ret_val = ds_eps_kamgr_bring_up_keep_alive_pdn ();
      }
      else
      {
        DS_LTE_MSG0_MED("ds_eps_kamgr_process_conf_status_info: Either Keep alive"
                        " manager is active or Keep alive timer is running");
      }
    }
    else
    {
      DS_LTE_MSG0_MED("LTE not the preferrd mode for Keep Alive APN");
      kamgr->lte_srv_status = FALSE;
    }
  }

  /*------------------------------------------------------------------------- 
    Case 2:
    Non-LTE (UMTS or 3GPP2) in full service, we bring down the Keep Alive PDN
  --------------------------------------------------------------------------*/
  if( (kamgr->active_ind != RESET_KAMGR) && (tech == PS_SYS_NETWORK_3GPP) &&
           (rat_val != PS_SYS_RAT_EX_3GPP_LTE) && (rat_val != PS_SYS_RAT_EX_NULL_BEARER) )
  {
    /*------------------------------------------------------------------------- 
      Sys mode has changed from LTE, so we force a tear-down of Keep alive PDN
      if it does not have any active clients.
      Using an already existing function which checks whether any iface is not
      in use before tearing down the PDN context
    --------------------------------------------------------------------------*/
    DS_LTE_MSG0_HIGH("ds_eps_kamgr_process_system_status_info: RAT is no longer"
                   " LTE,tearing down Keep Alive PDN if iface is not in use");
    companion_iface = ds_3gpp_pdn_get_companion_iface (kamgr->ps_iface_ptr);

    if(PS_IFACE_IS_IN_USE (kamgr->ps_iface_ptr) == FALSE &&
       PS_IFACE_IS_IN_USE (companion_iface) == FALSE) 
    {
      ds_eps_kamgr_force_tear_down(kamgr->ps_iface_ptr);
    }
    kamgr->lte_srv_status = FALSE;
  }
  else if (kamgr->active_ind !=  RESET_KAMGR && tech == PS_SYS_NETWORK_3GPP2)
  {
    companion_iface = ds_3gpp_pdn_get_companion_iface (kamgr->ps_iface_ptr);
    if(PS_IFACE_IS_IN_USE (kamgr->ps_iface_ptr) == FALSE &&
       PS_IFACE_IS_IN_USE (companion_iface) == FALSE) 
    {
      ds_eps_kamgr_dereg_iface_cbacks_and_free_cback_buffers(kamgr->ps_iface_ptr,
                                                             FALSE);
      kamgr->ps_iface_ptr = NULL;
      kamgr->active_ind = kamgr->active_ind & RESET_KAMGR;
    
      DS_LTE_MSG0_HIGH("ds_eps_kamgr_process_system_status_info: RAT changed to"
                       " 3GPP2,tearing down Keep Alive PDN");
    }
    else
    {
      DS_LTE_MSG0_MED ("ds_eps_kamgr_process_system_status_info: Iface still"
                       " in USE, so clean-up not attempted now");
    }
    kamgr->lte_srv_status = FALSE;
  }
  /*------------------------------------------------------------------------- 
    Case 3:
    WLAN is no longer available so we tear down the PDN
  --------------------------------------------------------------------------*/
  else
  {
    if(kamgr->wlan_availability == FALSE && tech == PS_SYS_NETWORK_3GPP &&
       kamgr->active_ind != RESET_KAMGR)
    {
      DS_LTE_MSG0_MED("WLAN no longer available so tearing down Keep Alive PDN");
      bring_up_ret_val = ds_eps_kamgr_handle_wlan_disable_ind ();
    }
  }

  return bring_up_ret_val;
#else
   DS_LTE_MSG0_HIGH ("ds_eps_kamgr_process_system_status_info: MAPCON feature "
                     "disabled");
   return FALSE;
#endif /* FEATURE_DATA_WLAN_MAPCON */
}

/*===========================================================================
FUNCTION DS_EPS_KAMGR_USE_DSD_PREF_SYS_FOR_BRING_UP

DESCRIPTION
  This function uses the DSD provided sys mode information to decide whether
  LTE is available or not. If LTE is available, it attempts to bring-up the
  Keep alive PDN
 
PARAMETERS
  conf_status:                   System information received from PS_SYS event

DEPENDENCIES
  None.

RETURN VALUE  
  TRUE:                           If bring-up successful
  FALSE:                          If bring-up failed

SIDE EFFECTS
  None
===========================================================================*/

boolean ds_eps_kamgr_use_dsd_pref_sys_for_bring_up
(
   ps_sys_system_status_ex_type           *conf_status
)
{
#ifdef FEATURE_DATA_WLAN_MAPCON
  uint32                                   num_avail_sys = 0; 
  ps_sys_network_enum_type                 tech = PS_SYS_NETWORK_MAX; 
  ps_sys_rat_ex_enum_type                  rat_val = PS_SYS_RAT_MAX;  
  uint64                                   so_mask = PS_SYS_SO_EX_UNSPECIFIED;
  boolean                                  bring_up_ret_val = FALSE;
/*---------------------------------------------------------------------------*/
 
  if(conf_status == NULL)
  {
    DS_LTE_MSG0_ERROR("ds_eps_kamgr_use_dsd_pref_sys_for_bring_up: Invalid "
                      "Input Parameter");
  }
  else
  {
    num_avail_sys = conf_status->num_avail_sys;
      
    if(num_avail_sys == 0)
    {
      DS_LTE_MSG0_ERROR("ds_eps_kamgr_use_dsd_pref_sys_for_bring_up: Invalid "
                        "data passed; No preferred system reported via DSD");
    }
    else
    {
      tech = conf_status->avail_sys[PREF_RAT_INDEX].technology;
      rat_val = conf_status->avail_sys[PREF_RAT_INDEX].rat_value;
      so_mask = conf_status->avail_sys[PREF_RAT_INDEX].so_mask;

      DS_LTE_MSG3_MED("ds_eps_kamgr_use_dsd_pref_sys_for_bring_up: Technology: "
                      "%d, Rat_Mask: %d, So_Mask: %d",tech,rat_val,so_mask);

      bring_up_ret_val = ds_eps_kamgr_process_system_status_info (
                                                                  tech, 
                                                                  rat_val
                                                                  );
    }
  }

  return bring_up_ret_val;
#else
   DS_LTE_MSG0_HIGH ("ds_eps_kamgr_use_dsd_pref_sys_for_bring_up: MAPCON "
                     "feature disabled");
   return FALSE;
#endif /* FEATURE_DATA_WLAN_MAPCON */
}

/*===========================================================================
FUNCTION  DS_EPS_KAMGR_IFACE_UP_EV_CBACK

DESCRIPTION
  This function gets called when iface up event is posted
  
PARAMETERS 
  this_iface_ptr:   Iface on which up event was posted
  event:            Event that caused the cback to be invoked
  event_info:       Event info posted by PS
  user_data_ptr:    Data to be passed to the cback function

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/

void ds_eps_kamgr_iface_up_ev_cback
(
  ps_iface_type                           *this_iface_ptr,
  ps_iface_event_enum_type                 event,
  ps_iface_event_info_u_type               event_info,
  void                                    *user_data_ptr
)
{
#ifdef FEATURE_DATA_WLAN_MAPCON
  ds_cmd_type                            * cmd_ptr = NULL;  
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /*----------------------------------------------------------------------- 
   This cback is used an indication saying bring-up was successful and to
   reset the retry mechanism counters
  ------------------------------------------------------------------------*/
  if(event != IFACE_UP_EV )
  {
    DS_LTE_MSG1_MED("Cback invoked for a wrong event; %d", event);
    return;
  }

  DS_LTE_MSG1_HIGH("ds_eps_kamgr_iface_up_ev_cback: Received cback for %d "
                   "event",event);

  if( (cmd_ptr = ds_get_cmd_buf()) == NULL )
  {
    DS_LTE_MSG0_ERROR("ds_eps_kamgr_iface_up_ev_cback: KAMGR can't get cmd"
                      " buf from DS task");
    return;
  }

  memset (cmd_ptr, 0, sizeof(ds_cmd_type));

  cmd_ptr->hdr.cmd_id = DS_EPS_KAMGR_IFACE_UP_EV_CBACK_CMD;
  cmd_ptr->cmd.dsumtsps_call_info.user_data = (void *)this_iface_ptr;
  ds_put_cmd_ext(cmd_ptr);

#else
   DS_LTE_MSG0_HIGH ("ds_eps_kamgr_iface_up_ev_cback: MAPCON feature "
                     "disabled");
#endif /* FEATURE_DATA_WLAN_MAPCON */
}

/*===========================================================================
FUNCTION  DS_EPS_KAMGR_EFS_TOKENIZER

DESCRIPTION
  This function matches the tags, PROFILE_ID and TIMER_VAL with the data
  provided in EFS config file
 
PARAMETERS 
  file_data;                 EFS file data
  start:                     Starting point of comparison
  end:                       End point of comparison
  no_tokens:                 Total number of tags

DEPENDENCIES
  None

RETURN VALUE
  TRUE:                      if parsing was successful
  FALSE:                     parsing failure

SIDE EFFECTS
  None

===========================================================================*/

boolean ds_eps_kamgr_efs_tokenizer 
(
   char                             *file_data,
   uint32                            start,
   uint32                            end,
   uint32                            no_tokens
)
{
#ifdef FEATURE_DATA_WLAN_MAPCON
   uint32                            length = 0;
   boolean                           ret_value = FALSE;
/*-------------------------------------------------------------------------*/
   /*---------------------------------------------------------------------- 
    This function is used for case in-sensitive tag matching
   -----------------------------------------------------------------------*/
   if (no_tokens >= MAX_EFS_TOKENS)
   {
     DS_LTE_MSG0_ERROR("ds_eps_kamgr_efs_tokenizer: Invalid Token Count");
     return ret_value;
   }

   if(end > start)
   {
    length = strlen (tags[no_tokens]);
   }
   else
   {
     DS_LTE_MSG0_ERROR("ds_eps_kamgr_efs_tokenizer: Invalid Params Passed");
     return ret_value;
   }

   if(strncasecmp(tags[no_tokens],(const char*) (file_data+start), 
                  length) == 0)
   {
     DS_LTE_MSG1_MED ("ds_eps_kamgr_efs_tokenizer: Token %d matched",
                    no_tokens+1);
     ret_value = TRUE;
   }

   DS_LTE_MSG1_MED ("ds_eps_kamgr_efs_tokenizer: Tag matching status %d",
                  ret_value);
   return ret_value;
#else
   DS_LTE_MSG0_HIGH ("ds_eps_kamgr_efs_tokenizer: MAPCON feature disabled");
   return FALSE;
#endif /* FEATURE_DATA_WLAN_MAPCON */
}

/*===========================================================================
FUNCTION  DS_EPS_KAMGR_PARSE_EFS_DATA

DESCRIPTION
  This function parses the data read from the configurable efs file to extract
  profile number and timer values 
 
PARAMETERS 
  efs_data_ptr:              Buffer containing data read from efs file
  file_size:                 file size in bytes

DEPENDENCIES
  None

RETURN VALUE
  TRUE:                      if parsing was successful
  FALSE:                     parsing failure

SIDE EFFECTS
  None

===========================================================================*/
boolean ds_eps_kamgr_parse_efs_data 
(
   char                           *efs_data_ptr,
   uint32                          file_size,
   ds_eps_kamgr_efs_info          *efs_info
)
{
#ifdef FEATURE_DATA_WLAN_MAPCON
   uint32                       index = 0;
   uint32                       position = 0;  //Track position of ':'
   uint16                       no_of_chars = 0; //No. of chars read after ':'
   char                        *data = NULL; 
   uint8                        data_element_no = 0;
   boolean                      ret_val = FALSE;
   uint32                       token_start = 0, token_end = 0, no_tokens = 0;
/*---------------------------------------------------------------------------*/

  do
  {
    /*----------------------------------------------------------------------- 
     EFS File Format:
     PROFILE_ID:2;
     TIMER_VAL:500:600:10000:2:500;
     
     The Tags PROFILE_ID and TIMER_VAL are not case sensitive
     
     500: Default Timer Value
     600: Incremental Timer Value, Increments to be added every time
     10000: Maximum Timer value
     2: Maximum Number of Retries
     500: Default PDN teardown backoff timer
    ------------------------------------------------------------------------*/
    if(efs_data_ptr == NULL || file_size == 0 || efs_info == NULL)
    {
      DS_LTE_MSG0_ERROR ("ds_eps_kamgr_parse_efs_data: Invalid Input Parameter");
      break;
    }
    /*--------------------------------------------------------------------------- 
     The data in the buffer is read till we encounter a ':', this indicates start
     of useful data (profile number or timer value)
     We read the data till we encounter a ';' (line delimiter) or a ':' (next info)
    --------------------- --------------------------------------------------------*/
    for(index = 0; index < file_size; index++)
    {
      if(index == 0)
      {
        token_start = index;
      }
      else if (efs_data_ptr[index - 1] == '\n')
      {
        token_start = index;
      }
      if(efs_data_ptr[index] == ':')
      {
        token_end = index - 1;
        if(no_tokens < 2)
        {
         ret_val = ds_eps_kamgr_efs_tokenizer (efs_data_ptr, token_start, 
                                           token_end, no_tokens);
         no_tokens++;
        }
        if(ret_val == FALSE)
        {
         DS_LTE_MSG0_ERROR ("ds_eps_kamgr_parse_efs_data: Tag Matching Failed");
         break;
        }
        token_start = 0;
        token_end = 0;
        no_of_chars = 0;
        index++; position = index;
        while(efs_data_ptr[index] != ';' && efs_data_ptr[index] != ':')
        {
         index++;
         no_of_chars++;
        }
        if(no_of_chars > 0)
        {
         data_element_no++;
         data = (char *)modem_mem_alloc(sizeof(char)*(no_of_chars+1),
                                        MODEM_MEM_CLIENT_DATA);
         if(data == NULL)
         {
           DS_LTE_MSG1_ERROR ("ds_eps_kamgr_parse_efs_data: Unable to allocate %d"
                              " bytes also", (no_of_chars+1));
           ret_val = FALSE;
           break;
         }

         ds_eps_kamgr_get_parsed_data (data_element_no, data, position, 
                                       efs_data_ptr, no_of_chars, efs_info);
         modem_mem_free((void *)data, MODEM_MEM_CLIENT_DATA);
         data = NULL; 
        }
        index--;
        ret_val = TRUE;
      }
    }    
  }while (0);

  return ret_val;
#else
   DS_LTE_MSG0_HIGH ("ds_eps_kamgr_parse_efs_data: MAPCON feature disabled");
   return FALSE;
#endif /* FEATURE_DATA_WLAN_MAPCON */
}

/*===========================================================================
FUNCTION  DS_EPS_KAMGR_GET_PARSED_DATA

DESCRIPTION
  This function stores the parsed data in the keep alive manager instance
  for further use
 
PARAMETERS 
  element_no:                The element to be read
  element_data:              buffer to store the element data
  position:                  Postion in the file buffer to start the reading
  file_data:                 Buffer storing the efs file contents
  no_of_chars:               Number of characters to be read for the element

DEPENDENCIES 
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
void ds_eps_kamgr_get_parsed_data
(
   ds_eps_kamgr_efs_element_enum_type    element_no,
   char                                * element_data,
   uint32                                position,
   char                                * file_data,
   uint16                                no_of_chars,
   ds_eps_kamgr_efs_info               * efs_info
)
{
#ifdef FEATURE_DATA_WLAN_MAPCON
   uint16                            index = 0;
/*-------------------------------------------------------------------------*/
  while(index < no_of_chars)
  {
    element_data[index] = file_data[position+index];
    index++;
  }
  element_data [index] = '\0';

  switch (element_no)
  {
    case DS_EPS_KAMGR_EFS_PROFILE_NO:
      efs_info->profile_no = atoi((const char *)element_data);
      DS_LTE_MSG1_MED ("ds_eps_kamgr_get_parsed_data: Extracted profile "
                     "Number %d",efs_info->profile_no);
      if (efs_info->profile_no <= 0 || 
          efs_info->profile_no > DS_UMTS_MAX_PDP_PROFILE_NUM )
      {
        DS_LTE_MSG0_ERROR ("ds_eps_kamgr_get_parsed_data: Invalid Profile "
                         "passed, Resetting it to zero");
        efs_info->profile_no = 0;
      }
      break;

    case DS_EPS_KAMGR_EFS_DEFAULT_TIMER_VAL:
      efs_info->default_timer_val = atoi((const char *)element_data);
      DS_LTE_MSG1_MED ("ds_eps_kamgr_get_parsed_data: Extracted default timer"
                       " val %d", efs_info->default_timer_val); 
      break;

    case DS_EPS_KAMGR_EFS_INCREMENTAL_TIMER_VAL:
      efs_info->incremental_timer_val = atoi((const char *)element_data);
      DS_LTE_MSG1_MED ("ds_eps_kamgr_get_parsed_data: Extracted timer increment"
                       " val %d", efs_info->incremental_timer_val);
      break;
    
    case DS_EPS_KAMGR_EFS_MAXIMUM_TIMER_VAL:
      efs_info->maximum_timer_val = atoi((const char *)element_data);
      DS_LTE_MSG1_MED ("ds_eps_kamgr_get_parsed_data: Extracted Maximum Timer"
                       " val %d",efs_info->maximum_timer_val);
      break;

    case DS_EPS_KAMGR_EFS_MAXIMUM_RETRIES:
      efs_info->maximum_retries = atoi ((const char*)element_data);
      DS_LTE_MSG1_MED ("ds_eps_kamgr_get_parsed_data: Extracted Maximum retry"
                       " count %d",efs_info->maximum_retries);
      break;

    case DS_EPS_KAMGR_EFS_TEARDOWN_BACKOFF_TIMER:
      efs_info->teardown_backoff_timer = atoi ((const char*)element_data);
      DS_LTE_MSG1_MED ("ds_eps_kamgr_get_parsed_data: Extracted teardown backoff"
                       " timer %d",efs_info->teardown_backoff_timer);
      break;

    default:
      DS_LTE_MSG0_ERROR("ds_eps_kamgr_get_parsed_data: Invalid element number");
      break;
  }
#else
   DS_LTE_MSG0_HIGH ("ds_eps_kamgr_get_parsed_data: MAPCON feature disabled");
#endif /* FEATURE_DATA_WLAN_MAPCON */
}


/*===========================================================================
FUNCTION  DS_EPS_KAMGR_GET_KEEP_ALIVE_APN_FROM_PROFILE

DESCRIPTION
  This function gets the Keep alive APN name from the profile number extracted
  from EFS file
 
PARAMETERS 
 None
 
DEPENDENCIES 
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/

void ds_eps_kamgr_get_keep_alive_apn_from_profile
(
   void
)
{
#ifdef FEATURE_DATA_WLAN_MAPCON
  ds_umts_pdp_profile_type                  *profile_info = NULL;
  ds_umts_pdp_profile_status_etype           result = DS_UMTS_PDP_FAIL;
  sys_modem_as_id_e_type subs_id = SYS_MODEM_AS_ID_NONE;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  subs_id = ds3g_get_ps_subs_id();

  if(subs_id ==  SYS_MODEM_AS_ID_NONE)
  {
    DS_3GPP_MSG0_ERROR("Invalid subs_id");
    return;
  }
/*-----------------------------------------------------------------------*/
  /*--------------------------------------------------------------------- 
    Thisn function is used to extract the APN associated with the profile
    passed as a part of EFS configurable file
  ----------------------------------------------------------------------*/
  do
  {
    profile_info = (ds_umts_pdp_profile_type *)modem_mem_alloc
      (sizeof(ds_umts_pdp_profile_type), MODEM_MEM_CLIENT_DATA);

    if(profile_info == NULL)
    {
      DS_LTE_MSG0_HIGH("ds_eps_kamgr_bring_up_keep_alive_pdn: Unable to "
                       "allocate memory for extracting profile"
                       " information");
      return;
    }

    memset(profile_info, 0, sizeof(ds_umts_pdp_profile_type));
    result = ds_umts_get_pdp_profile_all_data_per_subs((kamgr->config_info).profile_no, 
                                                       subs_id,
                                                profile_info);

    if (result != DS_UMTS_PDP_SUCCESS)
    {
      break;
    }
    else
    {
       (void)strlcpy((char *)(kamgr->apn_name).apn_string, (const char*)
               (profile_info->context).apn, DS_UMTS_MAX_APN_STRING_LEN+1);
       (kamgr->apn_name).apn_string_len = strlen ((const char*)
                                               (profile_info->context).apn); 
    }
  }while (0);

  modem_mem_free((void *)profile_info,MODEM_MEM_CLIENT_DATA);
  profile_info = NULL; 
#else
   DS_LTE_MSG0_HIGH ("ds_eps_kamgr_get_keep_alive_apn_from_profile: MAPCON "
                     "feature disabled");
#endif /* FEATURE_DATA_WLAN_MAPCON */
}

/*===========================================================================
FUNCTION       DS_DSD_APM_APN_CHANGE_CB_HDLR

DESCRIPTION    Callback handler to process apn_change

DEPENDENCIES   NONE

RETURN VALUE   NONE

SIDE EFFECTS   NONE
===========================================================================*/
 void ds_eps_kamgr_apn_change_cb_hdlr
(
  uint16                                   profile_id
)
{
#ifdef FEATURE_DATA_WLAN_MAPCON
  ds_cmd_type                             * cmd_ptr = NULL;
  uint16                                  * profile_ptr = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - --  - - - - - - */
   DS_LTE_MSG1_MED ("Keep ALive PDN's profile %d has its APN changed",
                    profile_id);

   profile_ptr = (uint16 *)modem_mem_alloc (sizeof(uint16), 
                                            MODEM_MEM_CLIENT_DATA);


   if (profile_ptr == NULL || (cmd_ptr = ds_get_cmd_buf()) == NULL)
   {
     DS_LTE_MSG0_ERROR("ds_eps_kamgr_pn_change_cb_hdlr: KAMGR can't get cmd"
                       " buf from DS task: internal state may be inconsistent");
     return;
   }
   
   memset (profile_ptr, 0, sizeof(uint16));
   *profile_ptr = profile_id;

   memset (cmd_ptr, 0, sizeof(ds_cmd_type));
   cmd_ptr->hdr.cmd_id = DS_EPS_KAMGR_APN_NAME_CHG_CMD;
   cmd_ptr->cmd.dsumtsps_call_info.user_data = (void *)profile_ptr;
   ds_put_cmd_ext(cmd_ptr);
#else
   DS_LTE_MSG0_HIGH ("ds_eps_kamgr_apn_change_cb_hdlr: MAPCON feature "
                     "disabled");
#endif /*FEATURE_DATA_WLAN_MAPCON*/
}
/*===========================================================================
FUNCTION  DS_EPS_KAMGR_READ_EFS_INFO

DESCRIPTION
  This function is responsible for reading the EFS file and WLAN offload config
  NV and deciding whether Keep alive manager should be enabled or not
  
PARAMETERS 
  None

DEPENDENCIES
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/

void ds_eps_kamgr_read_efs_info
(
   void
)
{
#ifdef FEATURE_DATA_WLAN_MAPCON
  char                                          *efs_data = NULL;
  boolean                                        parsing_status = FALSE; 
  uint16                                         epdg_nv_value = 0;
  uint32                                         file_size = 0;
  ds_eps_kamgr_efs_info                          config_info;
/*----------------------------------------------------------------------------*/

  if (system_status_cback_reg_status == TRUE)
  {
    /*------------------------------------------------------------------------ 
     Here we use the various EFS items to decide whether keep alive manager
     has to enable or not
     In the configurable EFS file, Profile ID is a must whereas Timer values
     are optional
     There is no support for "NULL APN"
    -------------------------------------------------------------------------*/
    epdg_nv_value = ds_wlan_get_wlan_offload_config_nv ();

    file_size = ds_efs_read_keep_alive_apn_data (&efs_data);

    if(file_size <= 0)
    {
      DS_LTE_MSG1_ERROR("ds_eps_kamgr_read_efs_info: Invalid File Size %d",
                        file_size);
      ds_eps_kamgr_clean_up_resources();
      return;
    }

    memset(&config_info, 0, sizeof(config_info));

    parsing_status = ds_eps_kamgr_parse_efs_data (efs_data,file_size,
                                                  &config_info);

    DS_LTE_MSG1_MED("ds_eps_kamgr_read_efs_info: Parsing Status %d",
                    parsing_status);

    ds_efs_free_kamgr_data_buffer (&efs_data);

    if(config_info.profile_no == 0)
    {
      DS_LTE_MSG0_ERROR ("ds_eps_kamgr_read_efs_info: Parsing Error, Exiting");
      ds_eps_kamgr_clean_up_resources();
      return;
    }

    if (config_info.incremental_timer_val == 0)
    {
      config_info.incremental_timer_val = INCREMENTAL_TIMER_VAL;
      parsing_status =  TRUE;
    }

    if (config_info.default_timer_val == 0)
    {
      config_info.default_timer_val = DEFAULT_TIMER_VAL;
      parsing_status =  TRUE;
    }

    if (config_info.maximum_timer_val == 0)
    {
      config_info.maximum_timer_val = MAX_TIMER_VAL;
      parsing_status =  TRUE;
    }

    if (config_info.maximum_retries == 0)
    {
      config_info.maximum_retries = MAXIMUM_RETRIES;
      parsing_status =  TRUE;
    }

    if(config_info.teardown_backoff_timer == 0)
    {
      config_info.teardown_backoff_timer = DEFAULT_TEARDOWN_BACKOFF_TIMER_VAL;
      parsing_status =  TRUE;
    }

    /*------------------------------------------------------------------------
    Read the NV 70315 and Configurable EFS file to find the Keep Alive PDN,
    If NV70315 is set to 2 (local breakout + IWLAN S2B) and APN name is valid
    we enable the Keep Alive Manager
    -------------------------------------------------------------------------*/
    if(epdg_nv_value == KEEP_ALIVE_EPDG_VALUE &&
       parsing_status == TRUE)
    {
      DS_LTE_MSG0_MED("ds_eps_kamgr_init: Enabling Keep alive manager");

      kamgr = (ds_eps_kamgr_state *)modem_mem_alloc(sizeof(ds_eps_kamgr_state), 
                                                MODEM_MEM_CLIENT_DATA);

      if(kamgr == NULL)
      {
        DS_LTE_MSG0_MED("ds_eps_kamgr_init: unable to allocate memory "
                        "for Keep alive manager");
        return;
      }
      memset(kamgr, 0, sizeof(ds_eps_kamgr_state));
      /*----------------------------------------------------------------------- 
      Define a timer that will be used before re-initiating request to bring up 
      Keep alive PDN 
      ------------------------------------------------------------------------*/
      rex_def_timer_ex(&(kamgr->keep_alive_bring_up_timer),
                       (rex_timer_cb_type)ds_eps_kamgr_keep_alive_pdn_timer_cb,
                       (unsigned long)0);

      rex_def_timer_ex(
        &(kamgr->keep_alive_bring_down_timer),
        (rex_timer_cb_type)ds_eps_kamgr_keep_alive_pdn_tear_down_timer_cb,
        (unsigned long)0);

      (void)memscpy((void *)&(kamgr->config_info), sizeof(kamgr->config_info),
                    (void *)&config_info, sizeof(config_info));
      /*-----------------------------------------------------------------------
       Register for APN change callback
      -----------------------------------------------------------------------*/
      (kamgr->ds_profile_kamgr_cb_func_tbl).apn_disable_flag_change_handler = 
                                                                         NULL;
      (kamgr->ds_profile_kamgr_cb_func_tbl).apn_name_change_handler = 
                                              ds_eps_kamgr_apn_change_cb_hdlr;
      (kamgr->ds_profile_kamgr_cb_func_tbl).link.next_ptr = NULL;
      ds_profile_3gpp_register_callback_table(&kamgr->
                                              ds_profile_kamgr_cb_func_tbl);
    }
  }
#else
  DS_LTE_MSG0_HIGH ("ds_eps_kamgr_read_efs_info: MAPCON feature"
                    " disabled");
#endif /* FEATURE_DATA_WLAN_MAPCON */
}
#endif /* FEATURE_DATA_LTE */