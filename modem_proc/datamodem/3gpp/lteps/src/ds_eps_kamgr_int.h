#ifndef DS_EPS_KAMGR_INT_H
#define DS_EPS_KAMGR_INT_H
/*===========================================================================

  Copyright (c) 2009-2014 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/
/*===========================================================================

                          INCLUDE FILES FOR MODULE

===========================================================================*/
#include "comdef.h"
#include "customer.h"
#include "ds_eps_kamgr_ext.h"
#include "ds_3gpp_pdn_context.h"
#include "ds3gmgr.h"
#include "dstask_v.h"
#include "ds_profile_3gppi.h"

/*===========================================================================

                          DATA DECLARATIONS

===========================================================================*/

typedef struct
{
  uint16                             profile_no;
  uint32                             default_timer_val;
  uint32                             incremental_timer_val;
  uint32                             maximum_timer_val;
  uint8                              maximum_retries;
  uint32                             teardown_backoff_timer;
}ds_eps_kamgr_efs_info;

typedef struct
{
/*---------------------------------------------------------------------------
 Tells whether Keep alive manager is active or not
---------------------------------------------------------------------------*/
  uint8                             active_ind;

/*---------------------------------------------------------------------------
 System status, whether LTE is available as full service or limited service
---------------------------------------------------------------------------*/
  boolean                           lte_srv_status;

/*---------------------------------------------------------------------------
 System status, whether WLAN is available or not
---------------------------------------------------------------------------*/
  boolean                           wlan_availability;

/*---------------------------------------------------------------------------
 Keep Alive APN
---------------------------------------------------------------------------*/
  ds3g_apn_name_type                apn_name;

/*---------------------------------------------------------------------------
 Um iface on which cback has to be registered
---------------------------------------------------------------------------*/
  ps_iface_type                     *ps_iface_ptr;

/*---------------------------------------------------------------------------
 Um iface on which cback has to be registered
---------------------------------------------------------------------------*/
  ps_iface_type                     *companion_iface;

/*---------------------------------------------------------------------------
Um iface down event registration buffer
---------------------------------------------------------------------------*/
  void                             *ps_iface_down_ev_buf_ptr;

/*---------------------------------------------------------------------------
Um iface up event registration buffer
---------------------------------------------------------------------------*/
  void                             *ps_iface_up_ev_buf_ptr;

/*---------------------------------------------------------------------------
Um iface bearer tech change event registration buffer
---------------------------------------------------------------------------*/
  void                             *ps_iface_up_all_ev_buf_ptr;

/*---------------------------------------------------------------------------
Wait timer before re-initiating the bring up of keep alive PDN
---------------------------------------------------------------------------*/
  rex_timer_type                    keep_alive_bring_up_timer;

/*---------------------------------------------------------------------------
Wait timer before re-initiating the bring up of keep alive PDN
---------------------------------------------------------------------------*/
  rex_timer_type                    keep_alive_bring_down_timer;

/*---------------------------------------------------------------------------
Timer value required to wait before re-initiating the bring up
---------------------------------------------------------------------------*/
  uint32                            bring_up_timer_val;

/*---------------------------------------------------------------------------
Counter that keep tracks of the occurrence of same net down reason
---------------------------------------------------------------------------*/
  uint16                            retry_counter;

/*---------------------------------------------------------------------------
Previous net down reason received during iface down event cback
---------------------------------------------------------------------------*/
  ps_iface_net_down_reason_type     prev_down_reason;

/*---------------------------------------------------------------------------
Variable to store the profile number read from EFS
---------------------------------------------------------------------------*/
  ds_eps_kamgr_efs_info                 config_info;

/*-------------------------------------------------------------------------- 
 3GPP DS Profile callback function table
---------------------------------------------------------------------------*/
  ds_profile_3gpp_cb_func_tbl_type      ds_profile_kamgr_cb_func_tbl;

}ds_eps_kamgr_state;

typedef enum
{
  DS_EPS_KAMGR_EFS_PROFILE_NO = 1,            /**< Keep Alive APN profile No */
  DS_EPS_KAMGR_EFS_DEFAULT_TIMER_VAL,         /**< Default wait timer value */
  DS_EPS_KAMGR_EFS_INCREMENTAL_TIMER_VAL,     /**< Increment to the default timer */
  DS_EPS_KAMGR_EFS_MAXIMUM_TIMER_VAL,         /**< Maximum wait timer value */
  DS_EPS_KAMGR_EFS_MAXIMUM_RETRIES,           /**< Maximum number of retries */
  DS_EPS_KAMGR_EFS_TEARDOWN_BACKOFF_TIMER,    /**< PDN Teardown backoff timer */  

  DS_EPS_KAMGR_EFS_MAX = 0xFF,                /**< Invalid Element type*/
}ds_eps_kamgr_efs_element_enum_type;

/*---------------------------------------------------------------------------
 NV 70315 value for Keep alive manager to kick in
---------------------------------------------------------------------------*/
#define                            KEEP_ALIVE_EPDG_VALUE      2

/*---------------------------------------------------------------------------
 Default Wait timer after 1st iface down indication during bring-up
---------------------------------------------------------------------------*/
#define                            DEFAULT_TIMER_VAL          500

/*---------------------------------------------------------------------------
Maximum retries to be done after we receive first iface down indication
---------------------------------------------------------------------------*/
#define                            MAXIMUM_RETRIES            2

/*---------------------------------------------------------------------------
Constant that stores the maximum timer value
---------------------------------------------------------------------------*/
#define                            MAX_TIMER_VAL              10000

/*---------------------------------------------------------------------------
Constant that stores the incremental timer value
---------------------------------------------------------------------------*/
#define                            INCREMENTAL_TIMER_VAL      500

/*---------------------------------------------------------------------------
 Default Wait timer to run before KAMGR PDN is to be torn down
---------------------------------------------------------------------------*/
#define               DEFAULT_TEARDOWN_BACKOFF_TIMER_VAL      250

/*---------------------------------------------------------------------------
Proc id for DUN call
---------------------------------------------------------------------------*/
#define                            PROC_ID_TE_DUN             0x1

/*---------------------------------------------------------------------------
Constant used to reset Keep Alive Manager active indication flag
---------------------------------------------------------------------------*/
#define                            RESET_KAMGR                0x00

/*---------------------------------------------------------------------------
Constant to set Keep Alive Manager active indication flag
---------------------------------------------------------------------------*/
#define                            SET_KAMGR                  0x01

/*---------------------------------------------------------------------------
Constant to indicate Keep Alive Manager is active but iface down event 
registration failed 
---------------------------------------------------------------------------*/
#define                            REG_FAILED_SET             0x02

/*---------------------------------------------------------------------------
Constant to indicate preferred RAT index
---------------------------------------------------------------------------*/
#define                            PREF_RAT_INDEX             0x00

/*---------------------------------------------------------------------------
Constant to indicate number of EFS tokens
---------------------------------------------------------------------------*/
#define                            MAX_EFS_TOKENS             02
/*===========================================================================

                          PUBLIC DATA DECLARATIONS

===========================================================================*/
#if 0
/*===========================================================================
FUNCTION  DS_EPS_KAMGR_ALLOCATE_PDN_AND_BRING_UP_IFACE

DESCRIPTION
  This function performs the active bring-up of the Keep alive PDN and its
  ifaces
 
PARAMETERS 
 None
 
DEPENDENCIES 
  None

RETURN VALUE
  TRUE:            If the bring-up was successful
  FALSE:           If the bring-up failed

SIDE EFFECTS
  None

===========================================================================*/

boolean ds_eps_kamgr_allocate_pdn_and_bring_up_iface 
(
   void
);
#endif

/*===========================================================================
FUNCTION  DS_EPS_KAMGR_PS_CONF_STATUS_CBACK

DESCRIPTION
  This callback is called by PS when PS events are provided. Specifically,
  the current service status indication.
  
PARAMETERS
    tech_type:                   technology for which event cback was invoked
    event_name:                  Event occurred
    subscription_id:             Current Subscription ID
  * event_info_ptr:              Event information
  * user_data_ptr:               Information passed during registration
 

DEPENDENCIES
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
void ds_eps_kamgr_ps_conf_status_cback
(
  ps_sys_tech_enum_type                    tech_type,
  ps_sys_event_enum_type                   event_name,
  ps_sys_subscription_enum_type            subscription_id,
  void                                   * event_info_ptr,
  void                                   * user_data_ptr
);

/*===========================================================================
FUNCTION  DS_EPS_KAMGR_BRING_UP_KEEP_ALIVE_PDN

DESCRIPTION
  This function is responsible for initiating the bring up of the PDN that
  has to be kept alive
  
PARAMETERS 
  None

DEPENDENCIES
  None.

RETURN VALUE
  TRUE: If bring up was successful
  FALSE: If bring up failed

SIDE EFFECTS
  None

===========================================================================*/
boolean ds_eps_kamgr_bring_up_keep_alive_pdn 
(
  void
);


/*===========================================================================
FUNCTION  DS_EPS_KAMGR_IFACE_DOWN_EV_CBACK

DESCRIPTION
  This function gets called when iface down event is posted
  
PARAMETERS 
  this_iface_ptr:   Iface on which down event was posted
  event:            Event that caused the cback to be invoked
  event_info:       Event info posted by PS
  user_data_ptr:    Data to be passed to the cback function

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
void ds_eps_kamgr_iface_down_ev_cback
(
  ps_iface_type                           *this_iface_ptr,
  ps_iface_event_enum_type                 event,
  ps_iface_event_info_u_type               event_info,
  void                                    *user_data_ptr
);


/*===========================================================================
FUNCTION  DS_EPS_KAMGR_KEEP_ALIVE_PDN_TIMER_CB

DESCRIPTION
  This function gets called when keep alive pdn timer expires, we now initate
  a new request for bring up of keep alive PDN
  
PARAMETERS 
  cback_param:                  Parameter to be passed to the cback function

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
void ds_eps_kamgr_keep_alive_pdn_timer_cb 
(
  unsigned long                               cback_param
);

/*==============================================================================
FUNCTION  DS_EPS_KAMGR_PROCESS_PDN_THROTTLE_INFO_FOR_BRING_UP

DESCRIPTION
  This function gets called when keep alive pdn timer expires, we now initate
  a new request for bring up of keep alive PDN
  
PARAMETERS 
  pdn_throttle_info:               Structure containing throttle information

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

================================================================================*/
void ds_eps_kamgr_process_pdn_throttle_info_for_bring_up
(
   ps_sys_pdn_throttle_info_type                     *throttle_info,
   ps_iface_net_down_reason_type                      down_reason
);


/*==============================================================================
FUNCTION DS_EPS_KAMGR_DEREG_IFACE_CBACKS

DESCRIPTION
  This function is called to clean up all the cback resources associated with
  an iface
 
PARAMETERS 
  this_iface_ptr:          Iface whose call backs have to be cleaned up
  clean_up:                Complete clean-up required or not

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

================================================================================*/
void ds_eps_kamgr_dereg_iface_cbacks_and_free_cback_buffers 
(
  ps_iface_type                                 *this_iface_ptr,
  boolean                                        clean_up
);

/*==============================================================================
FUNCTION  DS_EPS_KAMGR_CLEAN_UP_RESOURCES

DESCRIPTION
  This function is called to clean up all the resources associated with the keep
  alive manager
  
PARAMETERS 
  None

DEPENDENCIES
  None

RETURN VALUE
  TRUE:                          Clean up successful
  FALSE:                         Clean up failed

SIDE EFFECTS
  None

================================================================================*/

void ds_eps_kamgr_clean_up_resources 
(
   void
);

/*===========================================================================
FUNCTION  DS_EPS_KAMGR_RETRY_AFTER_FAILURE

DESCRIPTION
  This function incorporates the retry logic after we temporaray cause code
  as net down reason or we are not able to extract throttling info when we
  receive a throttling based cause code
  
PARAMETERS 
  down_reason:                            down reason received from network

DEPENDENCIES
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
void ds_eps_kamgr_retry_after_failure
(
  ps_iface_net_down_reason_type                    down_reason,
  boolean                                          bringup_failure
);

/*===========================================================================
FUNCTION DS_EPS_KAMGR_PROCESS_CONF_STATUS_INFO

DESCRIPTION
  This function process the confirmation status information recieved and
  decides whether LTE is preferred system or not, if it is we bring up the
  keep alive PDN
 
PARAMETERS
  tech:                            Technology (3gpp/3gpp2)
  rat_val:                         RAT Mask

DEPENDENCIES
  None.

RETURN VALUE  
  TRUE:                           If bring-up successful
  FALSE:                          If bring-up failed

SIDE EFFECTS

===========================================================================*/

boolean ds_eps_kamgr_process_system_status_info
(
  ps_sys_network_enum_type                 tech,
  ps_sys_rat_ex_enum_type                  rat_val
);

/*===========================================================================
FUNCTION DS_EPS_KAMGR_USE_DSD_PREF_SYS_FOR_BRING_UP

DESCRIPTION
  This function uses the DSD provided sys mode information to decide whether
  LTE is available or not. If LTE is available, it attempts to bring-up the
  Keep alive PDN
 
PARAMETERS
  conf_status:                   System information received from PS_SYS event

DEPENDENCIES
  None.

RETURN VALUE  
  TRUE:                           If bring-up successful
  FALSE:                          If bring-up failed

SIDE EFFECTS

===========================================================================*/

boolean ds_eps_kamgr_use_dsd_pref_sys_for_bring_up
(
   ps_sys_system_status_ex_type                  *conf_status
);

/*===========================================================================
FUNCTION  DS_EPS_KAMGR_IFACE_UP_EV_CBACK

DESCRIPTION
  This function gets called when iface up event is posted
  
PARAMETERS 
  this_iface_ptr:   Iface on which up event was posted
  event:            Event that caused the cback to be invoked
  event_info:       Event info posted by PS
  user_data_ptr:    Data to be passed to the cback function

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/

void ds_eps_kamgr_iface_up_ev_cback
(
  ps_iface_type                           *this_iface_ptr,
  ps_iface_event_enum_type                 event,
  ps_iface_event_info_u_type               event_info,
  void                                    *user_data_ptr
);

/*===========================================================================
FUNCTION  DS_EPS_KAMGR_GET_PARSED_DATA

DESCRIPTION
  This function stores the parsed data in the keep alive manager instance
  for further use
 
PARAMETERS 
  element_no:                The element to be read
  element_data:              buffer to store the element data
  position:                  Postion in the file buffer to start the reading
  file_data:                 Buffer storing the efs file contents
  no_of_chars:               Number of characters to be read for the element

DEPENDENCIES 
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
void ds_eps_kamgr_get_parsed_data
(
   ds_eps_kamgr_efs_element_enum_type    element_no,
   char                            * element_data,
   uint32                            position,
   char                            * file_data,
   uint16                            no_of_chars,
   ds_eps_kamgr_efs_info               * efs_info
);

/*===========================================================================
FUNCTION  DS_EPS_KAMGR_PARSE_EFS_DATA

DESCRIPTION
  This function parses the data read from the configurable efs file to extract
  profile number and timer values 
 
PARAMETERS 
  efs_data_ptr:              Buffer containing data read from efs file
  file_size:                 file size in bytes

DEPENDENCIES
  None

RETURN VALUE
  TRUE:                      if parsing was successful
  FALSE:                     parsing failure

SIDE EFFECTS
  None

===========================================================================*/
boolean ds_eps_kamgr_parse_efs_data 
(
   char                       *efs_data_ptr,
   uint32                      file_size,
   ds_eps_kamgr_efs_info          *efs_info
);

/*===========================================================================
FUNCTION  DS_EPS_KAMGR_GET_KEEP_ALIVE_APN_FROM_PROFILE

DESCRIPTION
  This function gets the Keep alive APN name from the profile number extracted
  from EFS file
 
PARAMETERS 
 None
 
DEPENDENCIES 
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/

void ds_eps_kamgr_get_keep_alive_apn_from_profile
(
   void
);

/*===========================================================================
FUNCTION  DS_EPS_KAMGR_HANDLE_WLAN_DISABLE_IND

DESCRIPTION
  This function takes the required actions on WLAN Disable indication; tearing
  down the Keep Alive PDN if needed 
  
PARAMETERS 
  None

DEPENDENCIES
  None

RETURN VALUE
  TRUE: If the required actions were taken successfully
  FALSE: Otherwise

SIDE EFFECTS
  None

===========================================================================*/
boolean ds_eps_kamgr_handle_wlan_disable_ind
(
  void
);

#endif /* DS_EPS_KAMGR_INT_H */
