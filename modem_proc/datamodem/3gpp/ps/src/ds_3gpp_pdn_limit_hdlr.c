/*!
  @file
  ds_pdn_limit_hdlr.c

  @brief
  REQUIRED brief one-sentence description of this C module.

  @detail
  OPTIONAL detailed description of this C module.
  - DELETE this section if unused.

*/

/*===========================================================================

  Copyright (c) 2009-2013 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/3gpp/ps/src/ds_3gpp_pdn_limit_hdlr.c#1 $

when         who     what, where, why
--------     ---     --------------------------------------------------------
05/15/13     vs      Initial File
===========================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

#include "datamodem_variation.h"
#include "customer.h"
#include "comdef.h"

#ifdef FEATURE_DATA_LTE
#include "ds_3gpp_pdn_limit_hdlr.h"
#include "modem_mem.h"
#include "ds_3gppi_utils.h"
#include "ds_3gpp_pdn_throttle_sm.h"
#include "ds_eps_pdn_context.h"
#include "ds_3gpp_apn_table.h"
/*===========================================================================

                   INTERNAL DEFINITIONS AND TYPES

===========================================================================*/

/* This structure stores PDN Connection Limit Information for a particular APN.
 
  max_pdn_conn_per_blk: Specifies the maximum number of PDN connections in a
                        time block that the UE is allowed to perform with
                        the network.
 
  max_pdn_conn_time:    Specifies the time duration in seconds during which
                        the UE counts the PDN connections already made.
 
  pdn_req_wait_time:    Specifies the minimum time interval between the new
                        PDN connection request and the last successful UE
                        initiated PDN disconnection.
 
  time_blk_size:        Specifies the time block duration during which at
                        maximum max_pdn_conn_per_blk successful PDN connection
                        requests are allowed to make for a given PDN from the
                        UE.
 
  pdn_conn_cntr:        This counter counts the number of successful PDN
                        connections since the PDNConnTimer is started.
 
  pdn_conn_tmr:         Timer which will be set to max_pdn_conn_time. 
 
*/

typedef struct
{
  byte              apn[DS_UMTS_MAX_APN_STRING_LEN+1]; /* Apn name */

  uint16            max_pdn_conn_per_blk; 
  uint16            max_pdn_conn_time; 
  uint16            pdn_req_wait_time;  
  uint16            time_blk_size;
  uint16            pdn_conn_cntr;
  rex_timer_type    pdn_conn_tmr;
  void*             client_data_ptr;
}ds_3gpp_pdn_limit_dyn_tbl_s;

typedef struct
{
  ds_3gpp_pdn_limit_dyn_tbl_s* pdn_limit_dyn_tbl[DS_3GPP_PDN_LIMIT_TBL_MAX_ENTRIES];
  void*                        client_data_ptr;
} ds_3gpp_pdn_limit_tbl_s; 


/*=========================================================================== 
  This is the main structure which stores PDN Conn Limit information for all
  APNs for which PDN Conn Timer, PDN Wait Timer or PDN Throttling Timer is
  running.
=============================================================================*/
typedef struct
{
  ds_3gpp_pdn_limit_efs_info_s efs_info;
  ds_3gpp_pdn_limit_tbl_s      *pdn_limit_tbl[DS3GSUBSMGR_SUBS_ID_MAX];
}ds_3gpp_pdn_limit_info_s;


#define DEF_PDN_REQ_THROTTLING_TIME 15 /*Minutes*/
/*===========================================================================

                         LOCAL VARIABLES

===========================================================================*/

static ds_3gpp_pdn_limit_info_s *ds_3gpp_pdn_limit_info_p;

/*===========================================================================

                    INTERNAL FUNCTION PROTOTYPES

===========================================================================*/

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_ALLOC_DYN_MEM

  DESCRIPTION
  This function allocates an entry in the PDN Limit Table.
  
  PARAMETERS  
  None.
    
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  Index of the entry into the PDN Limit Table
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
static uint8 ds_3gpp_pdn_limit_alloc_dyn_mem
(
  sys_modem_as_id_e_type subs_id
);

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_FREE_DYN_MEM

  DESCRIPTION
  This function frees an entry in the PDN Limit Table.
  
  PARAMETERS  
  Index of the entry into the PDN Limit Table
    
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  None
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
static void ds_3gpp_pdn_limit_free_dyn_mem
(
  uint8                  index,
  sys_modem_as_id_e_type subs_id
);

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_PDN_CONN_TMR_EXPIRY_CB

  DESCRIPTION
  This function is invoked when the PDN Connection Timer Expires.
  
  PARAMETERS  
  Callback data. This will be the index into the PDN Limit Table.
    
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  None.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
static void ds_3gpp_pdn_limit_pdn_conn_tmr_expiry_cb
(
  unsigned long callback_data
);

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_PROCESS_PDN_CONN_TMR_EXPIRY_CMD

  DESCRIPTION
  This function processes the PDN Connection Timer expiry event.
  
  PARAMETERS  
  Callback data. This will be the index into the PDN Limit Table.
    
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  None.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
static void ds_3gpp_pdn_limit_process_pdn_conn_tmr_expiry_cmd
(
  unsigned long callback_data
);

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_PDN_REQ_THROT_TMR_EXPIRY_CB

  DESCRIPTION
  This function is invoked when the PDN Req Throttling Timer Expires.
  
  PARAMETERS  
  Callback data. This will be the index into the PDN Limit Table.
    
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  None.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
static void ds_3gpp_pdn_limit_pdn_req_throt_tmr_expiry_cb
(
  void* throt_sm_ptr,
  void* callback_data
);

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_PROCESS_PDN_REQ_THROT_TMR_EXPIRY_CMD

  DESCRIPTION
  This function processes the PDN Req Throttle Timer Expiry Cmd.
  
  PARAMETERS  
  Callback data. This will be the index into the PDN Limit Table.
    
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  None.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
static void ds_3gpp_pdn_limit_process_pdn_req_throt_tmr_expiry_cmd
(
  void*                   throt_sm_ptr,
  sys_modem_as_id_e_type  subs_id
);

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_PDN_WAIT_TMR_EXPIRY_CB

  DESCRIPTION
  This function is invoked when the PDN Wait Timer expires.
  
  PARAMETERS  
  Callback data. This will be the index into the PDN Limit Table.
    
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  None.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
static void ds_3gpp_pdn_limit_pdn_wait_tmr_expiry_cb
(
  void* throt_sm_ptr,
  void* callback_data
);

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_PROCESS_PDN_WAIT_TMR_EXPIRY_CMD

  DESCRIPTION
  This function processes the PDN Wait Timer Expiry Cmd.
  
  PARAMETERS  
  Callback data. This will be the index into the PDN Limit Table.
    
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  None.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
static void ds_3gpp_pdn_limit_process_pdn_wait_tmr_expiry_cmd
(
  void*                   throt_sm_ptr,
  sys_modem_as_id_e_type  subs_id
);

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_IS_PDN_REQ_THROTTLING_IN_PROGRESS

  DESCRIPTION
  This function checks whether the PDN Req Throttling Timer is running.
  
  PARAMETERS  
  Index into the PDN Limit Table.
    
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  TRUE or FALSE based on whether PDN is throttled
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
static boolean ds_3gpp_pdn_limit_is_pdn_req_throttling_in_progress
(
  uint8                   index,
  sys_modem_as_id_e_type  subs_id
);

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_TBL_GET_CURRENT_TIME_BLK

  DESCRIPTION
  This function returns the Current time block based on the Current
  PDN Connection Timer Value.
  
  PARAMETERS  
  Index into the PDN Limit Table.
  Time in milli-secs for which the PDN Conn Timer has run.
    
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  Current Timer Block
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
static uint32 ds_3gpp_pdn_limit_tbl_get_current_time_blk
(
  uint8                  index,
  sys_modem_as_id_e_type subs_id,
  uint32                 time_in_msec
);

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_TBL_GET_PDN_WAIT_TMR_VAL

  DESCRIPTION
  This function returns the PDN Wait Timer Value for a given APN.
  
  PARAMETERS  
  callback data - Throttle SM Ptr from which we obtain APN information
    
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  PDN Wait Timer Value in milli-seconds
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
static uint32 ds_3gpp_pdn_limit_get_pdn_wait_tmr_val
(
  void* throt_sm_void_ptr,
  void* callback_data
);

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_TBL_GET_THROTTLING_TMR_VAL

  DESCRIPTION
  This function returns the PDN Req Throttling Timer value.
  
  PARAMETERS  
  callback data 
    
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  PDN Req Throttle Timer Value in milli-seconds
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
static uint32 ds_3gpp_pdn_limit_get_throttling_tmr_val
(
  void *throt_sm_ptr,
  void* cb_data
);

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_TBL_GET_TIME_BLK_SIZE

  DESCRIPTION
  This function fetches the time block size in milli-seconds.
  
  PARAMETERS  
  Index into the PDN Limit Table.
  
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  Time Block Size in millisecs.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
static uint32 ds_3gpp_pdn_limit_tbl_get_time_blk_size
(
  uint8                  index,
  sys_modem_as_id_e_type subs_id
);

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_TBL_GET_MAX_PDN_CONN_PER_BLK

  DESCRIPTION
  This function fetches Max PDN Conn allowed per Block for a Given APN.
  
  PARAMETERS  
  Index into the PDN Limit Table.
  
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  Max PDN Conn allowed per Time Block.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/

static uint16 ds_3gpp_pdn_limit_tbl_get_max_pdn_conn_per_blk
(
  uint8                  index,
  sys_modem_as_id_e_type subs_id
);

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_TBL_GET_MAX_PDN_CONN_ALLOWED

  DESCRIPTION
  This function fetches Max PDN Conn allowed in MAX PDN Conn Time.
  
  PARAMETERS  
  Index into the PDN Limit Table.
  
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  Max PDN Conn Allowed in MAX PDN Conn Time.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
static uint32 ds_3gpp_pdn_limit_tbl_get_max_pdn_conn_allowed
(
  uint8                  index,
  sys_modem_as_id_e_type subs_id
);

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_START_THROTTLING_TMR

  DESCRIPTION
  This function starts the PDN Req Throttling Timer.
  
  PARAMETERS  
  Index into the PDN Limit Table.
  
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  None.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
static void ds_3gpp_pdn_limit_start_throttling_tmr
(
  uint8                   index,
  sys_modem_as_id_e_type  subs_id
);

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_GET_MAX_PDN_CONN_TIME

  DESCRIPTION
  This function gets thE Max PDN Conn Time in milli-seconds.
  
  PARAMETERS  
  Index into the PDN Limit Table.
  
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  Max PDN Conn Time in milli-seconds.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
static uint32 ds_3gpp_pdn_limit_get_max_pdn_conn_time
(
  uint8                   index,
  sys_modem_as_id_e_type  subs_id
);

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_GET_SUBS_ID

  DESCRIPTION
  This function fetches the subs id corresponding to a PDN Limit Dyn Tbl Entry
  
  PARAMETERS  
  PDN Limit Dyn Tbl Entry 
  
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  Subs Id
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/

static sys_modem_as_id_e_type ds_3gpp_pdn_limit_get_subs_id
(
  ds_3gpp_pdn_limit_dyn_tbl_s *pdn_limit_dyn_tbl_entry_ptr 
);

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_GET_TBL_ENTRY_PER_SUBS_ID

  DESCRIPTION
  This function fetches the PDN Limit Table Entry for a Particular subscription
  
  PARAMETERS  
  Subs Id
  
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  PDN Limit Table Entry for a Particular subscription
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
static ds_3gpp_pdn_limit_tbl_s* ds_3gpp_pdn_limit_get_tbl_entry_per_subs_id
(
  sys_modem_as_id_e_type  subs_id
);

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_FREE_TBL_ENTRY_PER_SUBS_ID

  DESCRIPTION
  This function frees the PDN Limit Table Entry for a Particular subscription
  
  PARAMETERS  
  Subs Id
  
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  None.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
static void ds_3gpp_pdn_limit_free_tbl_entry_per_subs_id
(
  sys_modem_as_id_e_type  subs_id
);

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_GET_TBL_ENTRY_PER_APN

  DESCRIPTION
  This function fetches the PDN Limit Table Entry at a particular index and
  for a particular subscription
  
  PARAMETERS
  1. Index
  2. Subs Id
  
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  PDN Limit Table Entry at a particular index and
  for a particular subscription
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
static ds_3gpp_pdn_limit_dyn_tbl_s* ds_3gpp_pdn_limit_get_tbl_entry_per_apn
(
  uint8                   index,
  sys_modem_as_id_e_type  subs_id
);

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_FREE_TBL_ENTRY_PER_APN

  DESCRIPTION
  This function frees the PDN Limit Table Entry at a particular index and
  for a particular subscription
  
  PARAMETERS
  1. Index
  2. Subs Id
  
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  None.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
static void ds_3gpp_pdn_limit_free_tbl_entry_per_apn
(
  uint8                   index,
  sys_modem_as_id_e_type  subs_id
);

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_VALIDATE_CLEAR_ENTRY

  DESCRIPTION
  This function determines whether the entry corresponding to the given
  index and subs_id can be cleared from the PDN limit table.
  
  PARAMETERS
  1. Index into the PDN Limit Table.
  2. Subs id
 
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  None.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
static boolean ds_3gpp_pdn_limit_validate_clear_entry
(
  uint8                   index,
  sys_modem_as_id_e_type  subs_id
);

/*===========================================================================


                               FUNCTIONS

===========================================================================*/

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_GET_CURRENT_PDN_CONN_TMR_VAL

  DESCRIPTION
  This function checks gets the current PDN Connection Timer Value.
  
  PARAMETERS  
  Index into the PDN Limit Table.
  
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  Current PDN Conn Timer Value.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
uint32 ds_3gpp_pdn_limit_get_current_pdn_conn_tmr_val
(
  uint8                  index,
  sys_modem_as_id_e_type subs_id
)
{
  uint32                       current_timer_val;
  ds_3gpp_pdn_limit_dyn_tbl_s* per_apn_entry_ptr = NULL;
  /*-------------------------------------------------------------------------*/

  per_apn_entry_ptr = ds_3gpp_pdn_limit_get_tbl_entry_per_apn(index, subs_id);

  if (per_apn_entry_ptr == NULL)
  {
    DS_3GPP_MSG0_ERROR("per_apn_entry_ptr is NULL");
    return 0;
  }

  current_timer_val = (uint32)rex_get_timer
    (&(per_apn_entry_ptr->pdn_conn_tmr));

  DS_3GPP_MSG2_MED("Current value of PDN Conn Timer at index %d is %d msecs",
                    index,current_timer_val);

  return current_timer_val;
}

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_POWERUP_INIT

  DESCRIPTION
  This function performs Powerup Initialization of the Module
  
  PARAMETERS  
  None. 
    
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  None.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
void ds_3gpp_pdn_limit_powerup_init
(
  void
)
{
  int                          j;
  ds_3gpp_pdn_limit_efs_info_s efs_info;
  /*-------------------------------------------------------------------------*/

  DS_3GPP_MSG0_MED("Initializing PDN Limit Handler");

  efs_info.pdn_limit_is_enabled = FALSE;
  efs_info.pdn_req_throttling_time = DEF_PDN_REQ_THROTTLING_TIME;

  if (ds_3gpp_cfg_get(DS_3GPP_PDN_CONN_LIMIT_HANDLING,
                      (unsigned char *)(&efs_info),
                      sizeof(efs_info)) == -1)
  {
    DS_LTE_MSG0_ERROR("Cannot read EFS Config item"
                      "DS_3GPP_PDN_CONN_LIMIT_HANDLING, using "
                      "default as FALSE");
    return;
  }
  
  if (efs_info.pdn_limit_is_enabled == FALSE)
  {
    DS_LTE_MSG0_HIGH("PDN Conn Limit Handling not enabled");
    return;
  }

  ds_3gpp_pdn_limit_info_p = 
    (ds_3gpp_pdn_limit_info_s*)
      modem_mem_alloc(sizeof(ds_3gpp_pdn_limit_info_s), MODEM_MEM_CLIENT_DATA);

  if (ds_3gpp_pdn_limit_info_p != NULL)
  {
    ds_3gpp_pdn_limit_info_p->efs_info.pdn_limit_is_enabled = 
      efs_info.pdn_limit_is_enabled;
    ds_3gpp_pdn_limit_info_p->efs_info.pdn_req_throttling_time = 
      efs_info.pdn_req_throttling_time;

    for (j=0;j<DS3GSUBSMGR_SUBS_ID_MAX;j++)
    {
      ds_3gpp_pdn_limit_info_p->pdn_limit_tbl[j] = NULL;
    }
  }
  else
  {
    DS_LTE_MSG0_ERROR("Cannot allocate memory !");
  }
}

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_IS_ENABLED

  DESCRIPTION
  This function checks whether PDN Limit handling is enabled.
  
  PARAMETERS  
  None. 
    
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  TRUE if enabled. FALSE otherwise
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
boolean ds_3gpp_pdn_limit_is_enabled
(
  void
)
{
  /*-------------------------------------------------------------------------*/
  if (ds_3gpp_pdn_limit_info_p != NULL)
  {
    DS_3GPP_MSG1_MED("PDN Limit is enabled: %d", 
                     ds_3gpp_pdn_limit_info_p->efs_info.pdn_limit_is_enabled);
    return ds_3gpp_pdn_limit_info_p->efs_info.pdn_limit_is_enabled;
  }
  else
  {
    DS_3GPP_MSG0_MED("PDN Limit is enabled: 0");
    return FALSE;
  }
}

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_ALLOC_DYN_MEM

  DESCRIPTION
  This function allocates an entry in the PDN Limit Table.
  
  PARAMETERS  
  None.
    
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  Index of the entry into the PDN Limit Table
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
static uint8 ds_3gpp_pdn_limit_alloc_dyn_mem
(
  sys_modem_as_id_e_type subs_id
)
{
  uint32                       index = 0;
  ds_3gpp_pdn_limit_tbl_s     *pdn_limit_tbl_entry_ptr = NULL;
  ds_3gpp_pdn_limit_dyn_tbl_s *apn_based_tbl_ptr  = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(ds_3gpp_pdn_limit_info_p->efs_info.pdn_limit_is_enabled == TRUE);

  pdn_limit_tbl_entry_ptr = ds_3gpp_pdn_limit_get_tbl_entry_per_subs_id(subs_id);

  if (pdn_limit_tbl_entry_ptr == NULL)
  {
    pdn_limit_tbl_entry_ptr = ds_3gpp_pdn_limit_info_p->pdn_limit_tbl[subs_id] =
          (ds_3gpp_pdn_limit_tbl_s*)
            modem_mem_alloc(sizeof(ds_3gpp_pdn_limit_tbl_s), 
                            MODEM_MEM_CLIENT_DATA);

    if (pdn_limit_tbl_entry_ptr != NULL)
    {
      DS_3GPP_MSG1_HIGH("PDN Limit table allocated at subs id:%d",
                           subs_id);
      memset(pdn_limit_tbl_entry_ptr, 0, sizeof(ds_3gpp_pdn_limit_tbl_s));
      pdn_limit_tbl_entry_ptr->client_data_ptr = (void*)subs_id;
    }
    else
    {
      DS_3GPP_MSG0_ERROR("Failed to allocate dyn memory");
      ASSERT(0);
      return DS_3GPP_PDN_LIMIT_TBL_INVALID_ENTRY;
    }
  }

  for(index = 0; index < DS_3GPP_PDN_LIMIT_TBL_MAX_ENTRIES; index++)
  {
    if(ds_3gpp_pdn_limit_get_tbl_entry_per_apn(index, subs_id) == NULL)
    {
      apn_based_tbl_ptr = pdn_limit_tbl_entry_ptr->pdn_limit_dyn_tbl[index] =
        (ds_3gpp_pdn_limit_dyn_tbl_s *) modem_mem_alloc
        (sizeof(ds_3gpp_pdn_limit_dyn_tbl_s),MODEM_MEM_CLIENT_DATA);

      if (apn_based_tbl_ptr != NULL)
      {
        DS_3GPP_MSG1_HIGH("PDN Limit table dyn memory allocated at index:%d",
                           index);
        memset(apn_based_tbl_ptr , 0, sizeof(ds_3gpp_pdn_limit_dyn_tbl_s));

        apn_based_tbl_ptr ->client_data_ptr = (void*)index;

      	rex_def_timer_ex
        (
          &(apn_based_tbl_ptr ->pdn_conn_tmr),
          (rex_timer_cb_type) ds_3gpp_pdn_limit_pdn_conn_tmr_expiry_cb,
          (unsigned long)apn_based_tbl_ptr
        );
      }
      else
      {
        DS_3GPP_MSG0_ERROR("Failed to allocate dyn memory!");
        ASSERT(0);
        return DS_3GPP_PDN_LIMIT_TBL_INVALID_ENTRY;
      }
      break;
    }
  }

  if (index == DS_3GPP_PDN_LIMIT_TBL_MAX_ENTRIES)
  {
    DS_3GPP_MSG0_ERROR("Max PDN Limit TBL entries have been allocated");
  }

  return index;
}

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_FREE_DYN_MEM

  DESCRIPTION
  This function frees an entry in the PDN Limit Table.
  
  PARAMETERS  
  Index of the entry into the PDN Limit Table
    
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  None
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
static void ds_3gpp_pdn_limit_free_dyn_mem
(
  uint8                  index,
  sys_modem_as_id_e_type subs_id
)
{
  ds_3gpp_pdn_limit_dyn_tbl_s *per_apn_entry_ptr  = NULL;
  uint8                        i;
  /*-------------------------------------------------------------------------*/
  ds_3gpp_pdn_limit_free_tbl_entry_per_apn(index, subs_id);

  DS_3GPP_MSG1_HIGH("PDN Limit Table dyn memory at index:%d freed",index);

  for (i=0; i< DS_3GPP_PDN_LIMIT_TBL_MAX_ENTRIES; i++)
  {
    per_apn_entry_ptr = ds_3gpp_pdn_limit_get_tbl_entry_per_apn
                          (index, subs_id);

    if (per_apn_entry_ptr != NULL)
    {
      DS_3GPP_MSG0_MED("per_apn_entry_ptr is not NULL, breaking");
      break;
    }
  }

  if (i == DS_3GPP_PDN_LIMIT_TBL_MAX_ENTRIES)
  {
    DS_3GPP_MSG1_HIGH("All indexes are empty,"
                       "deallocate the PDN Limit Table for Subs %d", 
                       subs_id);

    ds_3gpp_pdn_limit_free_tbl_entry_per_subs_id(subs_id);
  }

  return; 
}

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_TBL_GET_MAX_PDN_CONN_PER_BLK

  DESCRIPTION
  This function fetches Max PDN Conn allowed per Block for a Given APN.
  
  PARAMETERS  
  Index into the PDN Limit Table.
  
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  Max PDN Conn allowed per Time Block.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
static uint16 ds_3gpp_pdn_limit_tbl_get_max_pdn_conn_per_blk
(
  uint8                  index,
  sys_modem_as_id_e_type subs_id
)
{
  ds_3gpp_pdn_limit_dyn_tbl_s *per_apn_entry_ptr  = NULL;
  /*-------------------------------------------------------------------------*/

  per_apn_entry_ptr = ds_3gpp_pdn_limit_get_tbl_entry_per_apn(index, subs_id);

  if (per_apn_entry_ptr == NULL)
  {
    DS_3GPP_MSG0_ERROR("per_apn_entry_ptr is NULL");
    return 0;
  }

  DS_3GPP_MSG2_MED("Max PDN Conn Per Blk for index %d is %d",
                    index, 
                    per_apn_entry_ptr->max_pdn_conn_per_blk);

  return per_apn_entry_ptr->max_pdn_conn_per_blk;
}

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_TBL_GET_TIME_BLK_SIZE

  DESCRIPTION
  This function fetches the time block size in mill-seconds.
  
  PARAMETERS  
  Index into the PDN Limit Table.
  
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  Time Block Size in milli-secs.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
static uint32 ds_3gpp_pdn_limit_tbl_get_time_blk_size
(
  uint8                  index,
  sys_modem_as_id_e_type subs_id
)
{
  ds_3gpp_pdn_limit_dyn_tbl_s *per_apn_entry_ptr  = NULL;
  /*-------------------------------------------------------------------------*/

  per_apn_entry_ptr = ds_3gpp_pdn_limit_get_tbl_entry_per_apn(index, subs_id);

  if (per_apn_entry_ptr == NULL)
  {
    DS_3GPP_MSG0_ERROR("per_apn_entry_ptr is NULL");
    return 0;
  }

  DS_3GPP_MSG2_MED("Time Blk size for index %d is %d secs",
                    index, 
                    per_apn_entry_ptr->time_blk_size);

  return per_apn_entry_ptr->time_blk_size * 1000;
}

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_TBL_GET_MAX_PDN_CONN_ALLOWED

  DESCRIPTION
  This function fetches Max PDN Conn allowed in MAX PDN Conn Time.
  
  PARAMETERS  
  Index into the PDN Limit Table.
  
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  Max PDN Conn Allowed in MAX PDN Conn Time.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
static uint32 ds_3gpp_pdn_limit_tbl_get_max_pdn_conn_allowed
(
  uint8                  index,
  sys_modem_as_id_e_type subs_id
)
{
  uint32 max_pdn_conn_allowed = 0;
  /*-------------------------------------------------------------------------*/

  if (index >= DS_3GPP_PDN_LIMIT_TBL_INVALID_ENTRY)
  {
    DS_3GPP_MSG1_ERROR("Invalid index: %d, returning",index);
    return 0;
  }

  max_pdn_conn_allowed = 
    ds_3gpp_pdn_limit_tbl_get_max_pdn_conn_allowed_in_given_time (
     index, subs_id, ds_3gpp_pdn_limit_get_max_pdn_conn_time(index, subs_id));
                          
  DS_3GPP_MSG2_MED("Max PDN Conns allowed for index %d is %d",
                    index, 
                    max_pdn_conn_allowed);
                              
  return max_pdn_conn_allowed;

}

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_TBL_GET_CURRENT_TIME_BLK

  DESCRIPTION
  This function returns the Current time block based on the Current
  PDN Connection Timer Value.
  
  PARAMETERS  
  Index into the PDN Limit Table.
  Time in mill-secs for which the PDN Conn Timer has run.
    
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  Current Timer Block
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
static uint32 ds_3gpp_pdn_limit_tbl_get_current_time_blk
(
  uint8                  index,
  sys_modem_as_id_e_type subs_id,
  uint32                 time_in_msec
)
{
  uint32 current_time_blk = 0;
  uint32 current_time_blk_size = 0;
  /*-------------------------------------------------------------------------*/

  if (index >= DS_3GPP_PDN_LIMIT_TBL_INVALID_ENTRY)
  {
    DS_3GPP_MSG1_ERROR("Invalid index: %d, returning",index);
    return 0;
  }

  // Put comments
  current_time_blk_size = ds_3gpp_pdn_limit_tbl_get_time_blk_size(index, subs_id);
  if (current_time_blk_size != 0)
  {
    current_time_blk = (time_in_msec + current_time_blk_size - 1) 
                         / current_time_blk_size;
  }

  return current_time_blk;
}

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_IS_PDN_REQ_WAIT_TIMER_IN_PROGRESS

  DESCRIPTION
  This function checks whether the PDN Req Wait Timer is running.
  
  PARAMETERS  
  Index into the PDN Limit Table.
    
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  TRUE or FALSE based on whether PDN Wait Timer is running
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
boolean ds_3gpp_pdn_limit_is_pdn_req_wait_timer_in_progress
(
  uint8                  index,
  sys_modem_as_id_e_type subs_id
)
{
  ds_3gpp_pdn_throttle_sm_s   *pdn_throt_sm_ptr = NULL;
  byte                        *apn = NULL;
  ds_3gpp_pdn_limit_dyn_tbl_s *per_apn_entry_ptr  = NULL;
  /*-------------------------------------------------------------------------*/

  per_apn_entry_ptr = ds_3gpp_pdn_limit_get_tbl_entry_per_apn(index, subs_id);

  if (per_apn_entry_ptr == NULL)
  {
    DS_3GPP_MSG0_ERROR("per_apn_entry_ptr is NULL");
    return FALSE;
  }

  apn = per_apn_entry_ptr->apn;

  pdn_throt_sm_ptr = ds_3gpp_pdn_throt_sm_get_throttle_sm
                     (apn, TRUE, FALSE, DS_3GPP_THROTTLE_TYPE_PDN_REQ_WAIT_TIME,
                      subs_id);

  // Could not find PDN Req throttling SM. This means that throttling is not in
  // progress. 
  if (pdn_throt_sm_ptr == NULL)
  {
    DS_3GPP_MSG1_HIGH("PDN Req Wait Timer is not in progress for index %d",
                       index);

    return FALSE;
  }

  DS_3GPP_MSG1_HIGH("PDN Req Wait Timer is in progress for index %d",index);
  return TRUE;
}

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_TBL_GET_TIME_REMAINING_IN_CURRENT_BLK

  DESCRIPTION
  This function fetches the time in milli-seconds remaining before the current
  time block ends.
  
  PARAMETERS  
  Index into the PDN Limit Table.
  Current time in milli-secs of the PDN Conn Timer.
    
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  Time in mill-seconds remaining before the current time block ends.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
uint32 ds_3gpp_pdn_limit_tbl_get_time_remaining_in_current_blk
(
  uint8                  index,
  sys_modem_as_id_e_type subs_id,
  uint32                 time_in_msec
)
{
  uint32 current_time_blk = 0;
  uint32 time_remaining_in_current_blk = 0;
  uint32 current_time_blk_end_time;
  /*-------------------------------------------------------------------------*/

  if (index >= DS_3GPP_PDN_LIMIT_TBL_INVALID_ENTRY)
  {
    DS_3GPP_MSG1_ERROR("Invalid index: %d, returning",index);
    return 0;
  }

  current_time_blk = 
    ds_3gpp_pdn_limit_tbl_get_current_time_blk(index,subs_id,time_in_msec);

  if (current_time_blk != 0)
  {
    current_time_blk_end_time = 
      (current_time_blk * ds_3gpp_pdn_limit_tbl_get_time_blk_size
                            (index, subs_id));

    if (time_in_msec <= current_time_blk_end_time)
    {
      time_remaining_in_current_blk = current_time_blk_end_time - time_in_msec;
    }
  }
  
  return time_remaining_in_current_blk;
}

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_TBL_GET_MAX_PDN_CONN_ALLOWED_IN_GIVEN_TIME

  DESCRIPTION
  This function fetches the Max PDN Connections allowed at any instant of time.
  
  PARAMETERS  
  Index into the PDN Limit Table.
  Current value of PDN Con Timer in mill-secs.
    
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  Max PDN Connections allowed in given time.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
uint32 ds_3gpp_pdn_limit_tbl_get_max_pdn_conn_allowed_in_given_time
(
  uint8                  index,
  sys_modem_as_id_e_type subs_id,
  uint32                 time_in_msec
)
{
  uint32 max_pdn_conn_allowed = 0;
  uint32 current_time_blk = 0;
  /*-------------------------------------------------------------------------*/

  if (index >= DS_3GPP_PDN_LIMIT_TBL_INVALID_ENTRY)
  {
    DS_3GPP_MSG1_ERROR("Invalid index: %d, returning",index);
    return 0;
  }

  current_time_blk = 
    ds_3gpp_pdn_limit_tbl_get_current_time_blk(index, subs_id, time_in_msec);

  max_pdn_conn_allowed =  
    current_time_blk * ds_3gpp_pdn_limit_tbl_get_max_pdn_conn_per_blk
                         (index, subs_id);

  DS_3GPP_MSG3_MED("Max PDN Conns allowed in current time %d "
                   "msecs for index %d is %d", 
                    time_in_msec, index, max_pdn_conn_allowed);


  return max_pdn_conn_allowed;

}

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_TBL_FIND_ENTRY

  DESCRIPTION
  This function finds entry corresponding at a given APN in PDN Limit Table.
  
  PARAMETERS  
  APN Name
    
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  Index into the PDN limit Table.
  Returns DS_3GPP_PDN_LIMIT_TBL_INVALID_ENTRY if cannot find an entry.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
uint8 ds_3gpp_pdn_limit_tbl_find_entry
(
  byte                   *apn,
  sys_modem_as_id_e_type  subs_id
)
{
  uint8                        loop = DS_3GPP_PDN_LIMIT_TBL_INVALID_ENTRY;
  ds_3gpp_pdn_limit_dyn_tbl_s *per_apn_entry_ptr  = NULL;
  /*-------------------------------------------------------------------------*/

  if (ds_3gpp_pdn_limit_is_enabled())
  {
    for( loop = 0; loop < DS_3GPP_PDN_LIMIT_TBL_MAX_ENTRIES; loop++)
    {
      per_apn_entry_ptr = ds_3gpp_pdn_limit_get_tbl_entry_per_apn(loop, subs_id);

      if (per_apn_entry_ptr != NULL)
      {
        if(strncasecmp((char*)per_apn_entry_ptr->apn, 
                       (char *)apn,
                        DS_UMTS_MAX_APN_STRING_LEN+1) == 0)
        {
          DS_3GPP_MSG1_MED("Found entry %d in PDN Limit Tbl", loop);

          break;
        }
      }
    }

    if (loop == DS_3GPP_PDN_LIMIT_TBL_MAX_ENTRIES)
    {
      DS_3GPP_MSG0_MED("Did not find an entry in PDN Limit Tbl");
    }
  }
  return loop;
}

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_TBL_FIND_OR_ADD_ENTRY

  DESCRIPTION
  This function tries to finds an entry corresponding to a given APN in the
  PDN Limit Table. If it cannot find an entry it creates a new entry in the
  PDN Limit Table.
  
  PARAMETERS  
  APN Name
  Max PDN Connections Per Block.
  Max PDN Connection Time.
  PDN Req Wait Time.
    
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  Index into the PDN limit Table.
  Returns DS_3GPP_PDN_LIMIT_TBL_INVALID_ENTRY if it cannot make an entry.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
uint8 ds_3gpp_pdn_limit_tbl_find_or_add_entry
(
  byte                   *apn,
  uint16                  max_pdn_conn_per_blk,
  uint16                  max_pdn_conn_time,
  uint16                  pdn_req_wait_time,
  sys_modem_as_id_e_type  subs_id
)
{
  uint8                        index = DS_3GPP_PDN_LIMIT_TBL_INVALID_ENTRY;
  ds_3gpp_pdn_limit_dyn_tbl_s *per_apn_entry_ptr  = NULL;
  /*-------------------------------------------------------------------------*/

  if (ds_3gpp_pdn_limit_is_enabled() == FALSE)
  {
    return index;
  }

  index = ds_3gpp_pdn_limit_tbl_find_entry(apn, subs_id);

  //Could not find an entry corresponding to the APN in PDN Limit TBL
  if (index == DS_3GPP_PDN_LIMIT_TBL_INVALID_ENTRY )
  {
    //Allocate a new entry in the PDN Limit TBL
    index = ds_3gpp_pdn_limit_alloc_dyn_mem(subs_id);

    //Successfully allocated a new entry in PDN Limit TBL
    if (index != DS_3GPP_PDN_LIMIT_TBL_INVALID_ENTRY)
    {
      per_apn_entry_ptr = ds_3gpp_pdn_limit_get_tbl_entry_per_apn
                            (index, subs_id);

      if (per_apn_entry_ptr != NULL)
      {
        (void) strlcpy((char *) per_apn_entry_ptr->apn, 
                       (char *) apn, 
                       DS_UMTS_MAX_APN_STRING_LEN+1);

        per_apn_entry_ptr->max_pdn_conn_per_blk = max_pdn_conn_per_blk;

        per_apn_entry_ptr->max_pdn_conn_time = max_pdn_conn_time;

        per_apn_entry_ptr->pdn_req_wait_time = pdn_req_wait_time;

        per_apn_entry_ptr->time_blk_size = max_pdn_conn_time;
      }
      else
      {
        DS_3GPP_MSG0_ERROR("per_apn_entry_ptr is NULL");
        return DS_3GPP_PDN_LIMIT_TBL_INVALID_ENTRY;
      }
    }
  }

  return index;
}

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_TBL_UPDATE_APN_NAME

  DESCRIPTION
  This function updates the APN Name in PDN Limit Table.
  
  PARAMETERS  
  Index into the PDN Limit Table.
  New APN name.
    
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  None.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
void ds_3gpp_pdn_limit_tbl_update_apn_name
(
  uint8                  index,
  sys_modem_as_id_e_type subs_id,
  char*                  apn
)
{
  ds_3gpp_pdn_limit_dyn_tbl_s *per_apn_entry_ptr  = NULL;
  /*-------------------------------------------------------------------------*/

  per_apn_entry_ptr = ds_3gpp_pdn_limit_get_tbl_entry_per_apn(index, subs_id);

  if (per_apn_entry_ptr == NULL)
  {
    DS_3GPP_MSG0_ERROR("per_apn_entry_ptr is NULL");
    return;
  }

  DATA_3GPP_MSG_SPRINTF_2(MSG_LEGACY_HIGH,
                          "APN name: %s updated for index %d in PDN Limit Tbl",
                          apn,
                          index);

  (void) strlcpy((char *) per_apn_entry_ptr->apn, apn, 
                   DS_UMTS_MAX_APN_STRING_LEN+1);
}

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_TBL_REMOVE_ENTRY
  DESCRIPTION
  This function removes entry at index 0 from PDN Limit Table.
  
  PARAMETERS  
  Index into the PDN Limit Table.
  
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  None.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
void ds_3gpp_pdn_limit_tbl_remove_entry
(
  uint8                  index,
  sys_modem_as_id_e_type subs_id
)
{
  if (ds_3gpp_pdn_limit_validate_clear_entry(index, subs_id))
  {
    DS_3GPP_MSG1_MED("Removing entry at index %d",index);

    ds_3gpp_pdn_limit_free_dyn_mem(index, subs_id);
  }
}


/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_PDN_CONN_TMR_EXPIRY_CB

  DESCRIPTION
  This function is invoked when the PDN Connection Timer Expires.
  
  PARAMETERS  
  Callback data. This will be the index into the PDN Limit Table.
    
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  None.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
static void ds_3gpp_pdn_limit_pdn_conn_tmr_expiry_cb
(
  unsigned long callback_data
)
{
  ds_cmd_type      *cmd_ptr;                     
  /*------------------------------------------------------------------------*/

  if( (cmd_ptr = ds_get_cmd_buf()) == NULL )
  {
    DS_3GPP_MSG0_ERROR( "DS3GPP can't get cmd buf from DS task");
    return;
  }
  else
  {
    DS_3GPP_MSG0_HIGH("Posting DS_3GPP_PDN_LIMIT_PDN_CONN_TMR_EXP_CMD");

    cmd_ptr->hdr.cmd_id = DS_3GPP_PDN_LIMIT_PDN_CONN_TMR_EXP_CMD;
    cmd_ptr->cmd.client_data_ptr = (void*)callback_data;
    ds_put_cmd_ext(cmd_ptr);
  }
}

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_PROCESS_PDN_CONN_TMR_EXPIRY_CMD

  DESCRIPTION
  This function processes the PDN Connection Timer expiry event.
  
  PARAMETERS  
  Callback data. This will be the index into the PDN Limit Table.
    
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  None.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
static void ds_3gpp_pdn_limit_process_pdn_conn_tmr_expiry_cmd
(
  unsigned long callback_data
)
{
  uint32                       index = 0;
  sys_modem_as_id_e_type       subs_id = SYS_MODEM_AS_ID_NONE;
  ds_3gpp_pdn_limit_dyn_tbl_s *pdn_limit_dyn_tbl_entry_ptr = 
    (ds_3gpp_pdn_limit_dyn_tbl_s*)callback_data;
  /*-------------------------------------------------------------------------*/

  DS_3GPP_MSG1_HIGH("PDN Conn Timer expired at index %d",index);

  subs_id = ds_3gpp_pdn_limit_get_subs_id(pdn_limit_dyn_tbl_entry_ptr);
  index = (uint32)pdn_limit_dyn_tbl_entry_ptr->client_data_ptr;

  ds_3gpp_pdn_limit_reset_pdn_conn_cntr(index, subs_id);
  ds_3gpp_pdn_limit_stop_pdn_wait_tmr(index, subs_id);
  ds_3gpp_pdn_limit_tbl_remove_entry(index, subs_id);
}


/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_TBL_UPDATE_MAX_PDN_CONN_TIME

  DESCRIPTION
  This function updates the Max PDN Conn Time in PDN Limit Table.
  
  PARAMETERS  
  Index into the PDN Limit Table.
  New Max PDN Conn Time
    
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  None.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
void ds_3gpp_pdn_limit_update_max_pdn_conn_time
(
  uint8                  index,
  sys_modem_as_id_e_type subs_id,
  uint16                 max_pdn_conn_time
)
{
  ds_3gpp_pdn_limit_dyn_tbl_s *per_apn_entry_ptr  = NULL;
  /*-------------------------------------------------------------------------*/

  per_apn_entry_ptr = ds_3gpp_pdn_limit_get_tbl_entry_per_apn(index, subs_id);

  if (per_apn_entry_ptr == NULL)
  {
    DS_3GPP_MSG0_ERROR("per_apn_entry_ptr is NULL");
    return;
  }

  per_apn_entry_ptr->max_pdn_conn_time = max_pdn_conn_time;

  per_apn_entry_ptr->time_blk_size = max_pdn_conn_time;
}

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_TBL_UPDATE_MAX_PDN_CONN_PER_BLOCK

  DESCRIPTION
  This function updates the Max PDN Conn Per Block in PDN Limit Table.
  
  PARAMETERS  
  Index into the PDN Limit Table.
  New Max PDN Conn Per Block.
    
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  None.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
void ds_3gpp_pdn_limit_update_max_pdn_conn_per_blk
(
  uint8 index,
  sys_modem_as_id_e_type subs_id,
  uint16 max_pdn_conn_per_blk
)
{
  ds_3gpp_pdn_limit_dyn_tbl_s *per_apn_entry_ptr  = NULL;
  /*-------------------------------------------------------------------------*/

  per_apn_entry_ptr = ds_3gpp_pdn_limit_get_tbl_entry_per_apn(index, subs_id);

  if (per_apn_entry_ptr == NULL)
  {
    DS_3GPP_MSG0_ERROR("per_apn_entry_ptr is NULL");
    return;
  }


  per_apn_entry_ptr->max_pdn_conn_per_blk = max_pdn_conn_per_blk;
}

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_TBL_UPDATE_PDN_REQ_WAIT_TIME

  DESCRIPTION
  This function updates the PDN Req Wait Time in PDN Limit Table.
  
  PARAMETERS  
  Index into the PDN Limit Table.
  New PDN Req Wait Time.
    
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  None.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
void ds_3gpp_pdn_limit_update_pdn_req_wait_time
(
  uint8 index,
  sys_modem_as_id_e_type subs_id,
  uint16 pdn_req_wait_time
)
{
  ds_3gpp_pdn_limit_dyn_tbl_s *per_apn_entry_ptr  = NULL;
  /*-------------------------------------------------------------------------*/

  per_apn_entry_ptr = ds_3gpp_pdn_limit_get_tbl_entry_per_apn(index, subs_id);

  if (per_apn_entry_ptr == NULL)
  {
    DS_3GPP_MSG0_ERROR("per_apn_entry_ptr is NULL");
    return;
  }

  per_apn_entry_ptr->pdn_req_wait_time = pdn_req_wait_time;
}

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_START_PDN_CONN_TMR

  DESCRIPTION
  This function starts the PDN Conn Timer if the PDN throttling timer
  is not running and if the PDN Conn Timer has not yet started.
  
  PARAMETERS  
  Index into the PDN Limit Table.
  
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  None.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
void ds_3gpp_pdn_limit_start_pdn_conn_tmr
(
  uint8                   index,
  sys_modem_as_id_e_type  subs_id 
)
{
  ds_3gpp_pdn_limit_dyn_tbl_s *per_apn_entry_ptr  = NULL;
  /*-------------------------------------------------------------------------*/

  per_apn_entry_ptr = ds_3gpp_pdn_limit_get_tbl_entry_per_apn(index, subs_id);

  if (per_apn_entry_ptr == NULL)
  {
    DS_3GPP_MSG0_ERROR("per_apn_entry_ptr is NULL");
    return;
  }

  DS_3GPP_MSG1_HIGH("Starting PDN Conn Timer at index %d",index);

  if (ds_3gpp_pdn_limit_is_pdn_req_throttling_in_progress(index, subs_id) ==
        FALSE )
  {
    rex_set_timer(&(per_apn_entry_ptr->pdn_conn_tmr),
                  ds_3gpp_pdn_limit_get_max_pdn_conn_time(index, subs_id));
  }
}

/*===========================================================================
FUNCTION  DS_3GPP_PDN_LIMIT_CLR_PDN_CONN_TMR 

DESCRIPTION
  This function clears the PDN connection timer

PARAMETERS
   void            
 
DEPENDENCIES
  None.

RETURN VALUE 
  void 

SIDE EFFECTS
  None.

===========================================================================*/
void ds_3gpp_pdn_limit_clr_pdn_conn_tmr
(
   uint8                             index,
   sys_modem_as_id_e_type            subs_id
)
{
  ds_3gpp_pdn_limit_dyn_tbl_s        *per_apn_entry_ptr  = NULL;
 /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  per_apn_entry_ptr = ds_3gpp_pdn_limit_get_tbl_entry_per_apn(index, subs_id);

  if (per_apn_entry_ptr == NULL)
  {
    DS_3GPP_MSG0_ERROR("per_apn_entry_ptr is NULL");
    return;
  }

  DS_3GPP_MSG0_MED("Clearing pdn conn tmr");

  (void)rex_clr_timer(&(per_apn_entry_ptr->pdn_conn_tmr));

}

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_IS_PDN_REQ_THROTTLING_IN_PROGRESS

  DESCRIPTION
  This function checks whether the PDN Req Throttling Timer is running.
  
  PARAMETERS  
  Index into the PDN Limit Table.
    
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  TRUE or FALSE based on whether PDN is throttled
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
boolean ds_3gpp_pdn_limit_is_pdn_req_throttling_in_progress
(
  uint8                   index,
  sys_modem_as_id_e_type  subs_id
)
{
  ds_3gpp_pdn_throttle_sm_s   *pdn_throt_sm_ptr = NULL;
  byte                        *apn = NULL;
  ds_3gpp_pdn_limit_dyn_tbl_s *per_apn_entry_ptr  = NULL;
  /*-------------------------------------------------------------------------*/

  per_apn_entry_ptr = ds_3gpp_pdn_limit_get_tbl_entry_per_apn(index, subs_id);

  if (per_apn_entry_ptr == NULL)
  {
    DS_3GPP_MSG0_ERROR("per_apn_entry_ptr is NULL");
    return FALSE;
  }

  apn = per_apn_entry_ptr->apn;

  pdn_throt_sm_ptr = ds_3gpp_pdn_throt_sm_get_throttle_sm
                       (apn, TRUE, FALSE, DS_3GPP_THROTTLE_TYPE_PDN_CONN_LIMIT, 
                          subs_id);

  // Could not find PDN Req throttling SM. This means that throttling is not in
  // progress. 
  if (pdn_throt_sm_ptr == NULL)
  {
    DS_3GPP_MSG1_HIGH("PDN Req throttling is not in progress for index %d",
                      index);

    return FALSE;
  }

  DS_3GPP_MSG1_HIGH("PDN Req Throttling is in progress for index %d",index);
  return TRUE;
}

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_PDN_REQ_THROT_TMR_EXPIRY_CB

  DESCRIPTION
  This function is invoked when the PDN Req Throttling Timer Expires.
  
  PARAMETERS  
  Callback data. This will be the index into the PDN Limit Table.
    
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  None.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
void ds_3gpp_pdn_limit_pdn_req_throt_tmr_expiry_cb
(
  void* throt_sm_ptr,
  void* callback_data
)
{
  ds_cmd_type      *cmd_ptr;                     
  /*------------------------------------------------------------------------*/

  if( (cmd_ptr = ds_get_cmd_buf()) == NULL )
  {
    DS_3GPP_MSG0_ERROR( "DS3GPP can't get cmd buf from DS task");
    return;
  }
  else
  {
    DS_3GPP_MSG0_HIGH("Posting DS_3GPP_PDN_LIMIT_PDN_CONN_TMR_EXP_CMD");

    cmd_ptr->hdr.cmd_id = DS_3GPP_PDN_LIMIT_PDN_REQ_THROT_TMR_EXP_CMD;
    cmd_ptr->cmd.throt_info.throt_sm_ptr = throt_sm_ptr;
    cmd_ptr->cmd.throt_info.subs_id = (int32)callback_data;
    ds_put_cmd_ext(cmd_ptr);
  }
}

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_PROCESS_PDN_REQ_THROT_TMR_EXPIRY_CMD

  DESCRIPTION
  This function processes the PDN Req Throttle Timer Expiry Cmd.
  
  PARAMETERS  
  Callback data. This will be the index into the PDN Limit Table.
    
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  None.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
static void ds_3gpp_pdn_limit_process_pdn_req_throt_tmr_expiry_cmd
(
  void*                   throt_sm_ptr,
  sys_modem_as_id_e_type  subs_id
)
{
  ds_3gppi_throttle_sm_s      *ds_3gppi_throt_sm_ptr = NULL;
  uint8                        index = 0;
  ds_3gpp_pdn_limit_dyn_tbl_s *per_apn_entry_ptr  = NULL;

  /*------------------------------------------------------------------------*/

  ds_3gppi_throt_sm_ptr = (ds_3gppi_throttle_sm_s*)throt_sm_ptr;

  if (ds_3gppi_throt_sm_ptr == NULL)
  {
    DS_3GPP_MSG0_ERROR("ds_3gppi_throt_sm_ptr is NULL");
    return;
  }

  if ((index = ds_3gpp_pdn_limit_tbl_find_entry(ds_3gppi_throt_sm_ptr->apn, subs_id))
       != DS_3GPP_PDN_LIMIT_TBL_INVALID_ENTRY)
  {

    DS_3GPP_MSG1_HIGH("PDN Req throttling tmr expired at index %d",index);

    per_apn_entry_ptr = ds_3gpp_pdn_limit_get_tbl_entry_per_apn(index, subs_id);

    if (per_apn_entry_ptr == NULL)
    {
      DS_3GPP_MSG0_ERROR("per_apn_entry_ptr is NULL");
      return;
    }

    ds_3gpp_pdn_throt_clear_throttle_state
      (per_apn_entry_ptr->apn, DS_UMTS_PDP_IPV4V6, subs_id);

    ds_3gpp_pdn_limit_reset_pdn_conn_cntr(index, subs_id);

    ds_3gpp_pdn_limit_start_pdn_conn_tmr(index, subs_id);

  }
}

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_PDN_WAIT_TMR_EXPIRY_CB

  DESCRIPTION
  This function is invoked when the PDN Wait Timer expires.
  
  PARAMETERS  
  Callback data. This will be the index into the PDN Limit Table.
    
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  None.
   
  SIDE EFFECTS 
  None.
===========================================================================*/

static void ds_3gpp_pdn_limit_pdn_wait_tmr_expiry_cb
(
  void* throt_sm_ptr,
  void* callback_data
)
{
  ds_cmd_type      *cmd_ptr;                     
  /*------------------------------------------------------------------------*/

  if( (cmd_ptr = ds_get_cmd_buf()) == NULL )
  {
    DS_3GPP_MSG0_ERROR( "DS3GPP can't get cmd buf from DS task");
    return;
  }
  else
  {
    DS_3GPP_MSG0_HIGH("Posting DS_3GPP_PDN_LIMIT_PDN_CONN_TMR_EXP_CMD");

    cmd_ptr->hdr.cmd_id = DS_3GPP_PDN_LIMIT_PDN_WAIT_TMR_EXP_CMD;
    cmd_ptr->cmd.throt_info.throt_sm_ptr = throt_sm_ptr;
    cmd_ptr->cmd.throt_info.subs_id = (int32)callback_data;
    ds_put_cmd_ext(cmd_ptr);
  }
}

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_PROCESS_PDN_WAIT_TMR_EXPIRY_CMD

  DESCRIPTION
  This function processes the PDN Wait Timer Expiry Cmd.
  
  PARAMETERS  
  Callback data. This will be the index into the PDN Limit Table.
    
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  None.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
static void ds_3gpp_pdn_limit_process_pdn_wait_tmr_expiry_cmd
(
  void*                   throt_sm_ptr,
  sys_modem_as_id_e_type  subs_id
)
{
  ds_3gppi_throttle_sm_s      *ds_throt_sm_ptr = NULL;
  uint8                        index = 0;
  ds_3gpp_pdn_limit_dyn_tbl_s *per_apn_entry_ptr  = NULL;
  /*------------------------------------------------------------------------*/

  ds_throt_sm_ptr = (ds_3gppi_throttle_sm_s*)throt_sm_ptr;

  if (ds_throt_sm_ptr == NULL)
  {
    DS_3GPP_MSG0_ERROR("throt_sm_p is NULL");
    return;
  }

  if ((index = ds_3gpp_pdn_limit_tbl_find_entry(ds_throt_sm_ptr->apn, subs_id))
       != DS_3GPP_PDN_LIMIT_TBL_INVALID_ENTRY)
  {
    DS_3GPP_MSG1_HIGH("PDN Req Wait tmr expired at index %d",index);

    per_apn_entry_ptr = ds_3gpp_pdn_limit_get_tbl_entry_per_apn(index, subs_id);

    if (per_apn_entry_ptr == NULL)
    {
      DS_3GPP_MSG0_ERROR("per_apn_entry_ptr is NULL");
      return;
    }

    ds_3gpp_pdn_throt_clear_throttle_state
      (per_apn_entry_ptr->apn, DS_UMTS_PDP_IPV4V6, subs_id);

    ds_3gpp_pdn_limit_tbl_remove_entry(index, subs_id);
  }
}

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_START_THROTTLING_TMR

  DESCRIPTION
  This function starts the PDN Req Throttling Timer.
  
  PARAMETERS  
  Index into the PDN Limit Table.
  
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  None.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
static void ds_3gpp_pdn_limit_start_throttling_tmr
(
  uint8                   index,
  sys_modem_as_id_e_type  subs_id
)
{
  ds_3gpp_pdn_throttle_sm_s *pdn_throt_sm_ptr = NULL;
  byte                      *apn = NULL;
  /*-------------------------------------------------------------------------*/

  ds_3gpp_pdn_limit_dyn_tbl_s *per_apn_entry_ptr  = NULL;

  per_apn_entry_ptr = ds_3gpp_pdn_limit_get_tbl_entry_per_apn(index, subs_id);

  if (per_apn_entry_ptr == NULL)
  {
    DS_3GPP_MSG0_ERROR("per_apn_entry_ptr is NULL");
    return;
  }

  if (ds_3gpp_pdn_limit_info_p->efs_info.pdn_req_throttling_time !=0)
  {
    DS_3GPP_MSG1_HIGH("Starting PDN Req throttling timer at index %d",index);

    apn = per_apn_entry_ptr->apn;

    pdn_throt_sm_ptr = ds_3gpp_pdn_throt_sm_get_throttle_sm
                       (apn, TRUE, TRUE, DS_3GPP_THROTTLE_TYPE_PDN_CONN_LIMIT, 
                          subs_id);

    if (pdn_throt_sm_ptr == NULL)
    {
      DS_3GPP_MSG0_ERROR("pdn_throt_sm_p is NULL");
      return;
    }

    ds_3gppi_throt_sm_register_timer_expiry_ev_cb(
      pdn_throt_sm_ptr->v4_throttle_sm,
      ds_3gpp_pdn_limit_pdn_req_throt_tmr_expiry_cb,
      (void*)subs_id);

    ds_3gppi_throt_sm_register_get_throttle_timer_val_cb(
      pdn_throt_sm_ptr->v4_throttle_sm,
      ds_3gpp_pdn_limit_get_throttling_tmr_val,
      (void*)subs_id);

    ds_3gpp_pdn_throt_perform_apn_throttling(pdn_throt_sm_ptr,
                                                    apn,
                                                    NULL,
                                                    DS_UMTS_PDP_IPV4V6,
                                                    subs_id,
                                                    TRUE);
  }
  else
  {
    DS_3GPP_MSG1_HIGH("Did not start PDN Req throttling timer at "
                      "index %d as timer val was configured as 0",index);
  }

  return;
}

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_TBL_GET_THROTTLING_TMR_VAL

  DESCRIPTION
  This function returns the PDN Req Throttling Timer value.
  
  PARAMETERS  
  callback data 
    
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  PDN Req Throttle Timer Value in milli-seconds
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
static uint32 ds_3gpp_pdn_limit_get_throttling_tmr_val
(
  void *throt_sm_ptr,
  void* cb_data
)
{
  /*-------------------------------------------------------------------------*/
  DS_3GPP_MSG1_MED("PDN Req throttling timer val is %d min",
                    ds_3gpp_pdn_limit_info_p->efs_info.pdn_req_throttling_time);

  return ds_3gpp_pdn_limit_info_p->efs_info.pdn_req_throttling_time * 60 * 1000;
}

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_START_PDN_WAIT_TMR

  DESCRIPTION
  This function starts the PDN Req Wait Timer.
  
  PARAMETERS  
  Index into the PDN Limit Table.
  
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  None.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
void ds_3gpp_pdn_limit_start_pdn_wait_tmr
(
  uint8                  index,
  sys_modem_as_id_e_type subs_id
)
{
  ds_3gpp_pdn_throttle_sm_s   *pdn_throt_sm_ptr = NULL;
  byte                        *apn = NULL;
  ds_3gpp_pdn_limit_dyn_tbl_s *per_apn_entry_ptr  = NULL;
  /*------------------------------------------------------------------------*/

  per_apn_entry_ptr = ds_3gpp_pdn_limit_get_tbl_entry_per_apn(index, subs_id);

  if (per_apn_entry_ptr == NULL)
  {
    DS_3GPP_MSG0_ERROR("per_apn_entry_ptr is NULL");
    return;
  }

  if (per_apn_entry_ptr->pdn_req_wait_time !=0)
  {
    DS_3GPP_MSG1_HIGH("Starting PDN Wait timer at index %d",index);

    apn = per_apn_entry_ptr->apn;

    pdn_throt_sm_ptr = ds_3gpp_pdn_throt_sm_get_throttle_sm
                       (apn,TRUE,TRUE, DS_3GPP_THROTTLE_TYPE_PDN_REQ_WAIT_TIME,
                         subs_id);

    if (pdn_throt_sm_ptr == NULL)
    {
      DS_3GPP_MSG0_ERROR("pdn_throt_sm_p is NULL");
      return;
    }

    ds_3gppi_throt_sm_register_timer_expiry_ev_cb(
      pdn_throt_sm_ptr->v4_throttle_sm,
      ds_3gpp_pdn_limit_pdn_wait_tmr_expiry_cb,
      (void*)subs_id);

    ds_3gppi_throt_sm_register_get_throttle_timer_val_cb(
      pdn_throt_sm_ptr->v4_throttle_sm,
      ds_3gpp_pdn_limit_get_pdn_wait_tmr_val,
      (void*)subs_id);

    ds_3gpp_pdn_throt_perform_apn_throttling(pdn_throt_sm_ptr,
                                                    apn,
                                                    NULL,
                                                    DS_UMTS_PDP_IPV4V6,
                                                    subs_id,
                                                    TRUE);
  }

  else
  {
    DS_3GPP_MSG1_HIGH("Did not start PDN Req Wait timer at "
                      "index %d as timer val was configured as 0",index);
  }

  return;
}

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_STOP_PDN_WAIT_TMR

  DESCRIPTION
  This function stops the PDN Req Wait Timer.
  
  PARAMETERS  
  Index into the PDN Limit Table.
  
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  None.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
void ds_3gpp_pdn_limit_stop_pdn_wait_tmr
(
  uint8                   index,
  sys_modem_as_id_e_type  subs_id
)
{
  byte                          *apn = NULL;
  ds_3gpp_pdn_throttle_sm_type   sm_type;
  ds_3gpp_pdn_limit_dyn_tbl_s   *per_apn_entry_ptr  = NULL;
  /*-------------------------------------------------------------------------*/

  per_apn_entry_ptr = ds_3gpp_pdn_limit_get_tbl_entry_per_apn(index, subs_id);

  if (per_apn_entry_ptr == NULL)
  {
    DS_3GPP_MSG0_ERROR("per_apn_entry_ptr is NULL");
    return;
  }


  apn = per_apn_entry_ptr->apn;

  if (ds_3gpp_pdn_throt_get_sm_type(apn,subs_id,TRUE,&sm_type) &&
      sm_type == DS_3GPP_THROTTLE_TYPE_PDN_REQ_WAIT_TIME )
  {

    DS_3GPP_MSG1_HIGH("Stopping PDN Wait timer at index %d",index);

    ds_3gpp_pdn_throt_clear_throttle_state
      (per_apn_entry_ptr->apn, DS_UMTS_PDP_IPV4V6, subs_id );    
  }
  else
  {
    DS_3GPP_MSG1_HIGH("PDN Wait timer at index %d was not running. "
                      "No need to stop.", index);
  }

}
/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_TBL_GET_PDN_WAIT_TMR_VAL

  DESCRIPTION
  This function returns the PDN Wait Timer Value for a given APN.
  
  PARAMETERS  
  callback data - Throttle SM Ptr from which we obtain APN information
    
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  PDN Wait Timer Value in milli-seconds
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
uint32 ds_3gpp_pdn_limit_get_pdn_wait_tmr_val
(
  void* throt_sm_void_ptr,
  void* callback_data
)
{
  uint8                          index = 0;
  ds_3gppi_throttle_sm_s        *throt_sm_ptr = 
                                   (ds_3gppi_throttle_sm_s*)throt_sm_void_ptr;
  sys_modem_as_id_e_type         subs_id = 
                                   (sys_modem_as_id_e_type)callback_data;
  ds_3gpp_pdn_limit_dyn_tbl_s   *per_apn_entry_ptr  = NULL;
  /*-------------------------------------------------------------------------*/

  if ( (index = ds_3gpp_pdn_limit_tbl_find_entry(throt_sm_ptr->apn, subs_id)) != 
          DS_3GPP_PDN_LIMIT_TBL_INVALID_ENTRY)
  {
    per_apn_entry_ptr = ds_3gpp_pdn_limit_get_tbl_entry_per_apn(index, subs_id);

    if (per_apn_entry_ptr == NULL)
    {
      DS_3GPP_MSG0_ERROR("per_apn_entry_ptr is NULL");
      return 0;
    }

    DS_3GPP_MSG2_MED("PDN Wait timer value at index %d is %d secs",
                     index,
                     per_apn_entry_ptr->pdn_req_wait_time);

    return per_apn_entry_ptr->pdn_req_wait_time * 1000;
  }

  else
  {
    return 0;
  }
}

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_GET_MAX_PDN_CONN_TIME

  DESCRIPTION
  This function gets thE Max PDN Conn Time in milli-seconds.
  
  PARAMETERS  
  Index into the PDN Limit Table.
  
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  Max PDN Conn Time in milli-seconds.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
static uint32 ds_3gpp_pdn_limit_get_max_pdn_conn_time
(
  uint8                   index,
  sys_modem_as_id_e_type  subs_id
)
{
  ds_3gpp_pdn_limit_dyn_tbl_s *per_apn_entry_ptr  = NULL;
  /*-------------------------------------------------------------------------*/

  per_apn_entry_ptr = ds_3gpp_pdn_limit_get_tbl_entry_per_apn(index, subs_id);

  if (per_apn_entry_ptr == NULL)
  {
    DS_3GPP_MSG0_ERROR("per_apn_entry_ptr is NULL");
    return 0;
  }

  DS_3GPP_MSG2_MED("PDN Conn Timer value at index %d is %d secs",
                   index,
                   per_apn_entry_ptr->max_pdn_conn_time);

  return per_apn_entry_ptr->max_pdn_conn_time * 1000;
}


/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_INCREMENT_PDN_CONN_CNTR

  DESCRIPTION
  This function increments the PDN Connection Counter.
  
  PARAMETERS  
  Index into the PDN Limit Table.
  
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  None.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
void ds_3gpp_pdn_limit_increment_pdn_conn_cntr
(
  uint8                   index,
  sys_modem_as_id_e_type  subs_id
)
{

  ds_3gpp_pdn_limit_dyn_tbl_s *per_apn_entry_ptr  = NULL;
  /*-------------------------------------------------------------------------*/

  per_apn_entry_ptr = ds_3gpp_pdn_limit_get_tbl_entry_per_apn(index, subs_id);

  if (per_apn_entry_ptr == NULL)
  {
    DS_3GPP_MSG0_ERROR("per_apn_entry_ptr is NULL");
    return;
  }

  per_apn_entry_ptr->pdn_conn_cntr++;

  DS_3GPP_MSG2_HIGH("Incrementing PDN Conn Cntr value at index %d. "
                    "New value %d", index,
                     per_apn_entry_ptr->pdn_conn_cntr);

}

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_RESET_PDN_CONN_CNTR

  DESCRIPTION
  This function resets the PDN Connection Counter.
  
  PARAMETERS  
  Index into the PDN Limit Table.
  
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  None.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
void ds_3gpp_pdn_limit_reset_pdn_conn_cntr
(
  uint8                  index,
  sys_modem_as_id_e_type subs_id
)
{
  ds_3gpp_pdn_limit_dyn_tbl_s *per_apn_entry_ptr  = NULL;
  /*-------------------------------------------------------------------------*/

  per_apn_entry_ptr = ds_3gpp_pdn_limit_get_tbl_entry_per_apn(index, subs_id);

  if (per_apn_entry_ptr == NULL)
  {
    DS_3GPP_MSG0_ERROR("per_apn_entry_ptr is NULL");
    return;
  }

  DS_3GPP_MSG1_HIGH("Resetting PDN Conn Ctr at index %d",index);

  per_apn_entry_ptr->pdn_conn_cntr = 0;
}

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_GET_PDN_CONN_CNTR

  DESCRIPTION
  This function fetches the current PDN Connection Counter.
  
  PARAMETERS  
  Index into the PDN Limit Table.
  
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  Current PDN Conn Cntr.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
uint16 ds_3gpp_pdn_limit_get_pdn_conn_cntr
(
  uint8                  index,
  sys_modem_as_id_e_type subs_id
)
{
  ds_3gpp_pdn_limit_dyn_tbl_s *per_apn_entry_ptr  = NULL;
  /*-------------------------------------------------------------------------*/

  per_apn_entry_ptr = ds_3gpp_pdn_limit_get_tbl_entry_per_apn(index, subs_id);

  if (per_apn_entry_ptr == NULL)
  {
    DS_3GPP_MSG0_ERROR("per_apn_entry_ptr is NULL");
    return 0;
  }

  DS_3GPP_MSG2_MED("Current PDN Conn Ctr at index %d is %d",
                    index,per_apn_entry_ptr->pdn_conn_cntr);

  return per_apn_entry_ptr->pdn_conn_cntr;
}

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_CALL_BRINGUP_IS_ALLOWED

  DESCRIPTION
  This function checks whether call bringup is allowed for the given APN.
  
  PARAMETERS  
  Index into the PDN Limit Table.
  
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  TRUE if call is allowed.
  FALSE otherwise.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
boolean ds_3gpp_pdn_limit_call_bringup_is_allowed
(
  uint8                  index,
  sys_modem_as_id_e_type subs_id
)
{
  uint32 current_pdn_conn_tmr_val = 0;
  /*-------------------------------------------------------------------------*/

  DS_3GPP_MSG0_HIGH("Checking whether call bringup is allowed");

  if (ds_3gpp_pdn_limit_get_pdn_conn_cntr(index, subs_id) >=  
        ds_3gpp_pdn_limit_tbl_get_max_pdn_conn_allowed(index, subs_id))
  {
    DS_3GPP_MSG0_ERROR("Max PDN Conn allowed count is reached. "
                       "Rejecting the call and starting the throttling timer"); 
    ds_3gpp_pdn_limit_start_throttling_tmr(index, subs_id);
    return FALSE;  
  }

  current_pdn_conn_tmr_val = 
    ds_3gpp_pdn_limit_get_current_pdn_conn_tmr_val(index, subs_id);

  if (current_pdn_conn_tmr_val == 0)
  {
    DS_3GPP_MSG0_HIGH("PDN Conn Timer not yet started or expired, Allow call."); 
    return TRUE;
  }

  if (ds_3gpp_pdn_limit_get_pdn_conn_cntr(index, subs_id) >=  
      ds_3gpp_pdn_limit_tbl_get_max_pdn_conn_allowed_in_given_time
        (index, subs_id, current_pdn_conn_tmr_val))
  {
    DS_3GPP_MSG0_ERROR("Max PDN Conn allowed count for "
                       "current time slice is reached. Rejecting the call");
    return FALSE;  
  }

  DS_3GPP_MSG0_HIGH("All checks passed. Allow call");

  return TRUE;
}

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_GET_SUBS_ID

  DESCRIPTION
  This function fetches the subs id corresponding to a PDN Limit Dyn Tbl Entry
  
  PARAMETERS  
  PDN Limit Dyn Tbl Entry 
  
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  Subs Id
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
static sys_modem_as_id_e_type ds_3gpp_pdn_limit_get_subs_id
(
  ds_3gpp_pdn_limit_dyn_tbl_s *pdn_limit_dyn_tbl_entry_ptr 
)
{
  uint32 index;
  int    subs_id = SYS_MODEM_AS_ID_NONE;
  /*-------------------------------------------------------------------------*/

  do 
  {
    if (pdn_limit_dyn_tbl_entry_ptr == NULL)
    {
      break;
    }

    index = (uint32)pdn_limit_dyn_tbl_entry_ptr->client_data_ptr;

    if (index >= DS_3GPP_PDN_LIMIT_TBL_INVALID_ENTRY)
    {
      break;
    }

    for (subs_id=0; subs_id < DS3GSUBSMGR_SUBS_ID_MAX; subs_id++ )
    {
      if (ds_3gpp_pdn_limit_get_tbl_entry_per_apn(index, subs_id) ==
            pdn_limit_dyn_tbl_entry_ptr)
      {
        break;
      }
    }
  } while(0);

  return subs_id;
}

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_GET_TBL_ENTRY_PER_SUBS_ID

  DESCRIPTION
  This function fetches the PDN Limit Table Entry for a Particular subscription
  
  PARAMETERS  
  Subs Id
  
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  PDN Limit Table Entry for a Particular subscription
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
static ds_3gpp_pdn_limit_tbl_s* ds_3gpp_pdn_limit_get_tbl_entry_per_subs_id
(
  sys_modem_as_id_e_type  subs_id
)
{
  ds_3gpp_pdn_limit_tbl_s *pdn_limit_tbl_entry_ptr = NULL;
  /*-------------------------------------------------------------------------*/
  DS_3GPP_MSG1_MED("Getting Tbl Entry, Subs Id: %d", subs_id);

  do
  {
    if (ds_3gpp_pdn_limit_info_p == NULL)
    {
      DS_3GPP_MSG0_ERROR("ds_3gpp_pdn_limit_info_p is NULL");
      break;
    }

    if (!ds3gsubsmgr_is_subs_id_valid(subs_id))
    {
      DS_3GPP_MSG0_ERROR("Subs Id is invalid");
      break;
    }  

    pdn_limit_tbl_entry_ptr = ds_3gpp_pdn_limit_info_p->pdn_limit_tbl[subs_id];

    DS_3GPP_MSG2_MED("pdn_limit_tbl_entry_ptr: 0x%x for Subs Id: %d", 
                     pdn_limit_tbl_entry_ptr,subs_id);

  }while(0);

  return pdn_limit_tbl_entry_ptr;
}

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_FREE_TBL_ENTRY_PER_SUBS_ID

  DESCRIPTION
  This function frees the PDN Limit Table Entry for a Particular subscription
  
  PARAMETERS  
  Subs Id
  
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  None.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
static void ds_3gpp_pdn_limit_free_tbl_entry_per_subs_id
(
  sys_modem_as_id_e_type  subs_id
)
{
  /*-------------------------------------------------------------------------*/
  do
  {
    if (ds_3gpp_pdn_limit_info_p == NULL)
    {
      DS_3GPP_MSG0_ERROR("ds_3gpp_pdn_limit_info_p is NULL");
      break;
    }

    if (!ds3gsubsmgr_is_subs_id_valid(subs_id))
    {
      DS_3GPP_MSG0_ERROR("Subs Id is invalid");
      break;
    }

    DS_3GPP_MSG1_MED("Freeing entry for for Subs Id: %d", subs_id);

    modem_mem_free ((void*)ds_3gpp_pdn_limit_info_p->pdn_limit_tbl[subs_id], 
                      MODEM_MEM_CLIENT_DATA);

    ds_3gpp_pdn_limit_info_p->pdn_limit_tbl[subs_id] = NULL;

  }while(0);

  return;
}

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_GET_TBL_ENTRY_PER_APN

  DESCRIPTION
  This function fetches the PDN Limit Table Entry at a particular index and
  for a particular subscription
  
  PARAMETERS
  1. Index
  2. Subs Id
  
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  PDN Limit Table Entry at a particular index and
  for a particular subscription
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
static ds_3gpp_pdn_limit_dyn_tbl_s* ds_3gpp_pdn_limit_get_tbl_entry_per_apn
(
  uint8                   index,
  sys_modem_as_id_e_type  subs_id
)
{
  ds_3gpp_pdn_limit_dyn_tbl_s* pdn_limit_dyn_tbl_ptr = NULL;
  ds_3gpp_pdn_limit_tbl_s*     pdn_limit_tbl_entry_ptr = NULL;
  /*-------------------------------------------------------------------------*/
  DS_3GPP_MSG2_MED("Getting Tbl Entry, Subs Id: %d, Index: %d", subs_id, index);

  do
  {
    pdn_limit_tbl_entry_ptr = ds_3gpp_pdn_limit_get_tbl_entry_per_subs_id(subs_id);

    if (pdn_limit_tbl_entry_ptr == NULL)
    {
      DS_3GPP_MSG0_ERROR("pdn_limit_tbl_entry_ptr is NULL");
      break;
    }

    if (index >= DS_3GPP_PDN_LIMIT_TBL_INVALID_ENTRY)
    {
      DS_3GPP_MSG1_ERROR("Invalid index: %d",index);
      break;
    }

    pdn_limit_dyn_tbl_ptr = pdn_limit_tbl_entry_ptr->pdn_limit_dyn_tbl[index];

  }while (0); 

  return pdn_limit_dyn_tbl_ptr;
}

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_FREE_TBL_ENTRY_PER_APN

  DESCRIPTION
  This function frees the PDN Limit Table Entry at a particular index and
  for a particular subscription
  
  PARAMETERS
  1. Index
  2. Subs Id
  
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  None.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
static void ds_3gpp_pdn_limit_free_tbl_entry_per_apn
(
  uint8                   index,
  sys_modem_as_id_e_type  subs_id
)
{
  ds_3gpp_pdn_limit_tbl_s* pdn_limit_tbl_entry_ptr = NULL;
  /*-------------------------------------------------------------------------*/
  DS_3GPP_MSG2_MED("Freeing Tbl Entry, Subs Id: %d, Index: %d", subs_id, index);

  do
  {
    pdn_limit_tbl_entry_ptr = ds_3gpp_pdn_limit_get_tbl_entry_per_subs_id(subs_id);

    if (pdn_limit_tbl_entry_ptr == NULL)
    {
      DS_3GPP_MSG0_ERROR("pdn_limit_tbl_entry_ptr is NULL");
      break;
    }

    if (index >= DS_3GPP_PDN_LIMIT_TBL_INVALID_ENTRY)
    {
      DS_3GPP_MSG1_ERROR("Invalid index: %d",index);
      break;
    }

    modem_mem_free ((void*)pdn_limit_tbl_entry_ptr->pdn_limit_dyn_tbl[index], 
                      MODEM_MEM_CLIENT_DATA);

    pdn_limit_tbl_entry_ptr->pdn_limit_dyn_tbl[index] = NULL;

  }while (0); 

  return;

}

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_PROCESS_CMDS

  DESCRIPTION
  This function processes any commands that need to be handled by
  PDN Connection Limit Module.
  
  PARAMETERS
  1. Cmd Ptr
  
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  None.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
void ds_3gpp_pdn_limit_process_cmds
(
  const ds_cmd_type      *cmd_ptr
)
{
  switch (cmd_ptr->hdr.cmd_id)
  {
    case DS_3GPP_PDN_LIMIT_PDN_CONN_TMR_EXP_CMD:
    {
      ds_3gpp_pdn_limit_process_pdn_conn_tmr_expiry_cmd
        ((unsigned long)cmd_ptr->cmd.client_data_ptr);
      break;
    }

    case DS_3GPP_PDN_LIMIT_PDN_REQ_THROT_TMR_EXP_CMD:
    {
      ds_3gpp_pdn_limit_process_pdn_req_throt_tmr_expiry_cmd
        (cmd_ptr->cmd.throt_info.throt_sm_ptr,cmd_ptr->cmd.throt_info.subs_id);
      break;
    }
    case DS_3GPP_PDN_LIMIT_PDN_WAIT_TMR_EXP_CMD:
    {
      ds_3gpp_pdn_limit_process_pdn_wait_tmr_expiry_cmd
        (cmd_ptr->cmd.throt_info.throt_sm_ptr,cmd_ptr->cmd.throt_info.subs_id);
      break;
    }
    default:
    {
      DS_3GPP_MSG1_ERROR("Unrecognized cmd: %d, ignoring",cmd_ptr->hdr.cmd_id);
      break;
    }
  }

  return;
}

/*===========================================================================
  FUNCTION DS_3GPP_PDN_LIMIT_VALIDATE_CLEAR_ENTRY

  DESCRIPTION
  This function determines whether the entry corresponding to the given
  index and subs_id can be cleared from the PDN limit table.
  
  PARAMETERS
  1. Index into the PDN Limit Table.
  2. Subs id
 
  DEPENDENCIES 
  None.
  
  RETURN VALUE 
  None.
   
  SIDE EFFECTS 
  None.
  
===========================================================================*/
static boolean ds_3gpp_pdn_limit_validate_clear_entry
(
  uint8                   index,
  sys_modem_as_id_e_type  subs_id
)
{
  ds_3gpp_pdn_limit_dyn_tbl_s* per_apn_entry_ptr = NULL;
  boolean                      clear_entry = TRUE;
  ds_pdn_context_s             *v4_pdn_cntx_p = NULL;
  ds_pdn_context_s             *v6_pdn_cntx_p = NULL;
  /*-------------------------------------------------------------------------*/

  per_apn_entry_ptr = ds_3gpp_pdn_limit_get_tbl_entry_per_apn(index, subs_id);
  do
  {
    /* If no entry is found for the index, subs id passed in,
       we have nothing to clear*/
    if (per_apn_entry_ptr == NULL)
    {
      DS_3GPP_MSG0_ERROR("Did not find any entry in PDN Limit Table");
      clear_entry = FALSE;
      break;
    }

    /* If Throttling is in progress or PDN Connection Timer is running,
       dont clear the entry*/
    if (ds_3gpp_pdn_limit_is_pdn_req_throttling_in_progress(index, subs_id) ||
        ds_3gpp_pdn_limit_is_pdn_req_wait_timer_in_progress(index, subs_id) ||
        ds_3gpp_pdn_limit_get_current_pdn_conn_tmr_val(index, subs_id) != 0)
    {
      DS_3GPP_MSG1_HIGH("Not clearing entry at index %d ",index);
      clear_entry = FALSE;
      break;
    }

    /* If a V4 or a V6 PDN Context exists for the give APN, do not clear the entry.
       This is done to ensure that when the PDN Connection is terminated,
       PDN Wait Timer can be started */
    if (ds_pdn_cntxt_get_v4_pdn_context
          (per_apn_entry_ptr->apn, &v4_pdn_cntx_p, subs_id ) ||
        ds_pdn_cntxt_get_v6_pdn_context
          (per_apn_entry_ptr->apn, &v6_pdn_cntx_p, subs_id ))
    {
      DS_3GPP_MSG2_HIGH("Found a valid pdn context, v4: 0x%x, v6: 0x%x, "
                        "Call still active. Not clearing entry ", 
                         v4_pdn_cntx_p, v6_pdn_cntx_p);
      clear_entry = FALSE;
      break;
    }
    else
    {
      DS_3GPP_MSG0_HIGH("No PDNs active for given APN. Clearing entry ");
      clear_entry = TRUE;
    }
  }
  while (0);

  return clear_entry;
}

/*==============================================================================
FUNCTION DS_3GPP_PDN_CNTXT_VALIDATE_AND_UPDATE_PDN_LIMIT_ENTRY
 
DESCRIPTION
  This function initially checks if pdn limit parameters bring passed are non-
  default , 
    If Yes: Add an entry in PDN limit table
    If NO : Check all other profile for non-default pdn limit parameter
            with same apn
            If Yes : Add an entry in PDN limit table with non-default parameters
            If No  : Add an entry in PDN limit table default parameters
 
  Assumptions made 
  1: Non-default Profile PDN limit parameter will be updated over default Profile
     PDN limit parameters with the same APN
  2. First non-default Profile PDN limit parameters will be chosen over multiple
     non-default Profile parameter given they have same APN

PARAMETERS 
  uint8  Profile_id :  Profile ID being passed
  byte profiel_apn  :  APN name of Profile ID being passed
  subs_id           :  Subscription ID
 
DEPENDENCIES 
  None. 
  
RETURN VALUE 
  boolean DS3G_FAILURE : Failure to update PDN Limit Entry
          DS3G_SUCCESS : Sucessfully update PDN Limit Entry
 
SIDE EFFECTS 
  None 
  
==============================================================================*/
ds_umts_pdp_profile_status_etype ds_3gpp_pdn_cntxt_validate_and_update_pdn_limit_entry
(
 uint8                  profile_id,
 uint8                  *handle_id,
 sys_modem_as_id_e_type subs_id
)
{
  uint8                            profile_nums_arr[DS_UMTS_MAX_PDP_PROFILE_NUM];
  uint8                            profile_count,temp_prof_num;
  boolean                          ret_val = FALSE;
  byte                             profile_apn[DS_UMTS_MAX_APN_STRING_LEN+1];
  uint16                           max_pdn_conn_per_blk ; 
  uint16                           max_pdn_conn_time ; 
  uint16                           pdn_req_wait_time ;  
  ds_umts_pdp_profile_status_etype result = DS3G_FAILURE;
  ds_umts_pdp_profile_type         profile_info; 
  
/*-----------------------------------------------------------------------------*/
  memset(profile_apn, 0, sizeof(DS_UMTS_MAX_APN_STRING_LEN+1));

  if (subs_id == SYS_MODEM_AS_ID_NONE )
  {
    DS_3GPP_MSG0_ERROR("Subs ID is invalid");
    return DS3G_FAILURE;
  }

  result = ds_umts_get_pdp_profile_context_info_apn_per_subs(profile_id,
                                               subs_id,
                                               profile_apn,
                                               DS_UMTS_MAX_APN_STRING_LEN);
  if(result != DS_UMTS_PDP_SUCCESS )
  {
    DS_3GPP_MSG0_ERROR("Invalid Profile num passed , no profile found");
    return DS3G_FAILURE; 
  }

  // Update Parameters with Profile values
  result = ds_umts_get_pdp_profile_max_pdn_conn_time_per_subs(profile_id,subs_id,
                                                              &max_pdn_conn_time);
  if(result != DS_UMTS_PDP_SUCCESS )
  {
    DS_3GPP_MSG0_ERROR("Invalid  max_pdn_conn_time parameter");
    return DS3G_FAILURE; 
  }

  result = ds_umts_get_pdp_profile_max_pdn_conn_per_blk_per_subs(profile_id,
                                                                 subs_id,
                                                        &max_pdn_conn_per_blk);
  if(result != DS_UMTS_PDP_SUCCESS )
  {
    DS_3GPP_MSG0_ERROR("Invalid max_pdn_conn_per_blk parameter");
    return DS3G_FAILURE; 
  }
 
  result = ds_umts_get_pdp_profile_pdn_req_wait_time_per_subs(profile_id,
                                                              subs_id,
                                                     &pdn_req_wait_time);
  if(result != DS_UMTS_PDP_SUCCESS )
  {
    DS_3GPP_MSG0_ERROR("Invalid pdn_req_wait_time parameter");
    return DS3G_FAILURE; 
  }

  // Verify pdn limit parameter for profile being passed is default 
  ret_val = ds_3gpp_pdn_context_is_pdn_limit_param_default(max_pdn_conn_per_blk,
                                                           max_pdn_conn_time,
                                                           pdn_req_wait_time);
  
  if (ret_val) 
  {

    for(temp_prof_num = 1;temp_prof_num <= DS_UMTS_MAX_PDP_PROFILE_NUM;
       temp_prof_num++)
    {
      /*------------------------------------------------------------------- 
        Ignore the existing profile
      ------------------------------------------------------------------*/
      if(profile_id == temp_prof_num)
      {
        continue;
      }

      memset((void*)&profile_info, 0, sizeof(ds_umts_pdp_profile_type));

      result = ds_umts_get_pdp_profile_all_data_per_subs(temp_prof_num,
                                                         subs_id,&profile_info);

      if(result != DS_UMTS_PDP_SUCCESS)
      {
        continue;
      }
 
      if(!profile_info.context.valid_flg)
      {
        continue;
      }
 
      // Compare APN names 
      ret_val = ds_3gpp_compare_apn_names((byte*)profile_info.context.apn,(byte*)profile_apn); 
      
      if((ret_val == TRUE) &&  
         (!ds_3gpp_pdn_context_is_pdn_limit_param_default(profile_info.max_pdn_conn_per_blk,
                                                          profile_info.max_pdn_conn_time,
                                                          profile_info.pdn_req_wait_time)))
      {
        // Found profile with non-default PDN limit param with same APN being passed
        DS_3GPP_MSG1_HIGH("PDN limit parameters updated with "
                          "profile %d info",temp_prof_num);
        max_pdn_conn_per_blk = profile_info.max_pdn_conn_per_blk ;
        max_pdn_conn_time = profile_info.max_pdn_conn_time ;
        pdn_req_wait_time = profile_info.pdn_req_wait_time  ;
        break;
      }
    }
  }
  else
  {
    DS_3GPP_MSG1_HIGH("PDN limit parameters updated with "
                      "profile %d info",profile_id);
  }
  // Add an entry into PDN limit table
  *handle_id = ds_3gpp_pdn_limit_tbl_find_or_add_entry
              ( profile_apn,
                max_pdn_conn_per_blk,
                max_pdn_conn_time,
                pdn_req_wait_time, 
                subs_id             
              );
                                         
  DS_3GPP_MSG3_HIGH("Updating PDN limit parameters "
                     "max_pdn_conn_per_blk %d , max_pdn_conn_time %d ,"
                     "pdn_req_wait_time %d " ,
                      max_pdn_conn_per_blk,
                      max_pdn_conn_time,
                      pdn_req_wait_time);

  return DS3G_SUCCESS;
}/*ds_3gpp_pdn_cntxt_validate_and_update_pdn_limit_entry*/

#endif /* FEATURE_DATA_LTE */
