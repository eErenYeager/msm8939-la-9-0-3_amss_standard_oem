/*!
  @file
  ds_3gpp_throttle_sm.h

  @brief
  Internal utility functions and routines

  @detail
  OPTIONAL detailed description of this C header file.
  - DELETE this section if unused.

*/

/*===========================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/3gpp/ps/inc/ds_3gpp_pdn_throttle_sm.h#2 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/05/11   ss      Changes to support RPM.
08/16/10   hs      Initial version

===========================================================================*/

#ifndef DS_3GPP_PDN_THROTTLE_SM_H
#define DS_3GPP_PDN_THROTTLE_SM_H


/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

#include "datamodem_variation.h"
#include "comdef.h"
#include "rex.h"
#include "ds_3gpp_throttle_sm.h"
#include "ds_3gpp_pdn_context.h"
#include "ps_iface_defs.h"
#include "ps_sys_ioctl.h"
#include "cm.h"
#include "ds_3gpp_apn_table.h"
/*===========================================================================

                   EXTERNAL DEFINITIONS AND TYPES

===========================================================================*/

#define DS_3GPP_PDN_THROT_INFINITE_THROT 0xFFFFFFFF
#define DS_PDN_THROT_LOG_PARAM_NA   0x0F
#define DS_PDN_THROT_MAX_APN_LOG_LENGTH  6
#define DS_PDN_THROT_LOG_LIST_TYPE_MAX (DS_MAX_APN_NUM * 2)

typedef enum 
{
     DS_3GPP_UNTHROTTLED = 0,
     DS_3GPP_THROTTLED_V4,
     DS_3GPP_THROTTLED_V6,
     DS_3GPP_THROTTLED_V4V6
} ds_3gpp_pdn_throttle_state_e;

/*------------------------------------------------------------------------
  Structure to hold information about pdn throttling for logging
-------------------------------------------------------------------------*/
typedef PACKED struct PACKED_POST
{
  uint8         subs_id;
  unsigned int  is_throttled:4;
  uint8         throttle_ip_type:4;
  uint8         profile_id;
  uint8         counter;  
  uint16        duration;
  uint16        mcc;
  uint16        mnc;
  char          apn[6];
  char          reserved[4];
} ds_3gpp_pdn_throttle_event_type;
/*------------------------------------------------------------------------------
  This enumeration will indicate the kind of Throttling being applied. 
------------------------------------------------------------------------------*/
typedef enum
{
  DS_3GPP_THROTTLE_TYPE_NOT_SPECIFIED = 0,
                           /*Throttling type was not specified*/
  DS_3GPP_THROTTLE_TYPE_PDN_CONN_LIMIT,
                           /*Throttling because Max limit of successful 
                             PDN Connections was reached*/
  DS_3GPP_THROTTLE_TYPE_PDN_REQ_WAIT_TIME,
                           /*Throttling because UE initiated PDN Disconnection*/
  DS_3GPP_THROTTLE_TYPE_T3396
                         /*Throttling because of T3396 Timer*/
} ds_3gpp_pdn_throttle_sm_type;

typedef struct
{
  sys_plmn_id_s_type                plmn_id;
  ds_3gppi_throttle_sm_s           *v4_throttle_sm;
  ds_3gppi_throttle_sm_s           *v6_throttle_sm;
  void                             *apn_throt_sm_ptr; //used to get to the per
                                                       // APN throt table
  ds_3gpp_pdn_throttle_sm_type     sm_type;
} ds_3gpp_pdn_throttle_sm_s;

typedef struct 
{
  byte  apn[DS_UMTS_MAX_APN_STRING_LEN+1];
  uint8 len;
  boolean    attach_allowed; 
}ds_3gpp_pdn_throt_apn_list_type;

/*------------------------------------------------------------------------------ 
  This structure stores the profile number only if a particule apn is PDN
  throttled. This is needed to log the throttle information when timer expires
  We will not have access to the throttled profile information otherwise
  ------------------------------------------------------------------------------*/
typedef struct
{
  ds_3gppi_throttle_sm_s *throttle_sm_ptr;
  uint16                  profile_num;
}ds_3gpp_pdn_throt_log_list_type;

typedef struct
{
  boolean                 v4_throttle_status;
  boolean                 v6_throttle_status;
}ds_3gpp_pdn_throt_temp_status_type;

#ifdef FEATURE_DATA_LTE
/*---------------------------------------------------------------------------- 
  This structure is used to update the apn from EFS and on receiving a
  PDN connectivity Reject, a decision is made to set block_pdn to TRUE based
  on the cause code received.
  ----------------------------------------------------------------------------*/
typedef struct
{
  boolean block_pdn;
  byte *apn;
  rex_timer_type  apn_reject_sm_timer;
  uint16  profile_num;
}ds_3gpp_pdn_throt_apn_reject_sm_s;

extern ds_3gpp_pdn_throt_apn_reject_sm_s ds_3gpp_pdn_throt_apn_reject_sm;

#endif
/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_CLEAR_THROTTLE_STATE

DESCRIPTION
  This function clears throttling state for a pdn context and a given ip
  type.

PARAMETERS   :
   ds_pdn_context_s * - pdn context ptr
   ds_umts_pdp_type_enum_type - pdp type
DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None

===========================================================================*/
void ds_3gpp_pdn_throt_clear_throttle_state
(
  byte                       *apn,
  ds_umts_pdp_type_enum_type  pdn_pdp_type,
  sys_modem_as_id_e_type      subs_id
);

/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_SM_PER_APN_POWER_UP_INIT

DESCRIPTION
  This function initializes the data structure needed to perform PDN throt
  on a per APN basis.

  This initializes the APN names to NULL and initializes the pointer array
  of PDN throt SM per APN to NULL

PARAMETERS
   None

DEPENDENCIES
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
void ds_3gpp_pdn_throt_sm_per_apn_power_up_init
(
  sys_modem_as_id_e_type subs_id
);

/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_SM_GET_THROTTLE_INFO_FOR_PLMN

DESCRIPTION
  This function retrieves the pdn throttle SM info for a specific
  plmn_id. It is used by IOCTL handler
  DS_3GPP_PDN_CNTX_GET_THR0TTLE_INFO

PARAMETERS   :
   plmn_id        - plmn_id
   *throttle_info - retrieved throttle info is put here
 
DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
void ds_3gpp_pdn_throt_sm_get_throttle_info_for_plmn
(
  sys_plmn_id_s_type                   *plmn_ptr,
  ps_sys_ioctl_pdn_throttle_info_type  *throttle_info,
  sys_modem_as_id_e_type                subs_id
);

/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_SM_GET_THROTTLE_SM_FROM_PLMN

DESCRIPTION
  This function retrieves the pdn throttle_sm for a specific plmn_id and apn.

PARAMETERS   :
   plmn_id - plmn_id
   *apn    - the apn for which the pdn throttle_sm is to be retrieved.

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
void *ds_3gpp_pdn_throt_sm_get_throttle_sm_from_plmn
(
  sys_plmn_id_s_type     plmn_id,
  byte                   *apn,
  sys_modem_as_id_e_type subs_id
);

/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_SM_GET_THROTTLE_SM

DESCRIPTION
  This function gets a Throttling state machine instance ptr for a apn on the 
  current plmn. If the pointer does not exist ;an entry will be allocated

PARAMETERS   :
  apn              - 
  globally_blocked - Flag indicating if globally blocked throttling state machine
                     is needed
  allow_create     - Flag indicating whether creation is allowed
  sm_type          - Throttling state machine type requested
  subs_id          - Subscription id of the client
DEPENDENCIES
  None.

RETURN VALUE
 Pointer to the Throttling state machine

SIDE EFFECTS
  None

===========================================================================*/
void* ds_3gpp_pdn_throt_sm_get_throttle_sm
(
  byte                          *apn,
  boolean                        globally_blocked,
  boolean                        allow_create,
  ds_3gpp_pdn_throttle_sm_type   sm_type,
  sys_modem_as_id_e_type         subs_id
);

/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_SM_GET_THROTTLE_SM_PLMN_BASED

DESCRIPTION
  This function gets a Throttling state machine instance ptr for a apn, plmn,
  subscription id pair. If the pointer does not exist ;an entry will be allocated

PARAMETERS   :
  apn              - 
  globally_blocked - Flag indicating if globally blocked throttling state machine
                     is needed
  allow_create     - Flag indicating whether creation is allowed
  sm_type          - Throttling state machine type requested
  subs_id          - Subscription id of the client
DEPENDENCIES
  None.

RETURN VALUE
 Pointer to the Throttling state machine

SIDE EFFECTS
  None

===========================================================================*/

void* ds_3gpp_pdn_throt_sm_get_throttle_sm_plmn_based
(
  byte                          *apn,
  boolean                        allow_create,
  ds_3gpp_pdn_throttle_sm_type   sm_type,
  sys_modem_as_id_e_type         subs_id,
  sys_plmn_id_s_type             plmn_id
);


/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_IS_PDN_THROTTLED

DESCRIPTION
  This function clears throttling state for a pdn context and a given ip
  type.

PARAMETERS   :
   byte* - apn name ptr
   ds_umts_pdp_type_enum_type - pdp type
DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None

===========================================================================*/
boolean ds_3gpp_pdn_throt_is_apn_throttled
(
  byte                       *apn,
  ds_pdn_context_type_e       call_type,
  sys_modem_as_id_e_type      subs_id
);

/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_HANDLE_CONN_RESPONSE

DESCRIPTION
  This function handles the PDP/PDN connection response. Delegates the
  response to the RPM handler function for appropriate action.

PARAMETERS   :
  conn_status    - whether network accepted or rejected.
  *pdn_context_p - pdn_context_ptr.
  down_reason    - the ps_iface_cause code for reject.

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
void ds_3gpp_pdn_throt_handle_conn_response
(
  boolean                       conn_status,
  ds_pdn_context_s*             pdn_context_p,
  ps_iface_net_down_reason_type down_reason
);

#ifdef FEATURE_DATA_RPM
#ifdef TEST_FRAMEWORK
#error code not present
#endif /* TEST_FRAMEWORK */

#endif /* FEATURE_DATA_RPM */

/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_GET_PREV_CAUSE_CODE

DESCRIPTION
  This function returns the previous IPV4 and IPV6 cause codes stored in
  the core throttle state machine
 
PARAMETERS   : 
 
  pdn_throt_sm_ptr  -  PDN throttle state machine pointer to fetch the cause
 					   code from the core throttle state machine
  v4_cause_code_ptr -  Failure cause code ptr to populate V4 causecode
  v6_cause_code_ptr -  Failure cause code ptr to populate V6 causecode 

DEPENDENCIES
  None.

RETURN VALUE
  boolean -  TRUE indicating SUCCESS
		     FALSE indicating pdn_throt sm ptr has not been allocated

SIDE EFFECTS
  None.
===========================================================================*/
boolean ds_3gpp_pdn_throt_get_prev_cause_code
(
  void 							    *pdn_throt_sm_ptr,
  void                             **v4_cause_code_ptr,
  void							   **v6_cause_code_ptr
);

/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_GET_SM_TYPE

DESCRIPTION
  This function gets the PDN Throttle SM Type.
 
PARAMETERS   : 
  APN Name
  Whether the APN is globally blocked
  SM Type to be returned
DEPENDENCIES
  None.

RETURN VALUE
  boolean -  TRUE indicating SUCCESS
             FALSE otherwise

SIDE EFFECTS
  None.
===========================================================================*/
boolean ds_3gpp_pdn_throt_get_sm_type
(
  byte                          *apn,
  sys_modem_as_id_e_type         subs_id,
  boolean                        globally_blocked,
  ds_3gpp_pdn_throttle_sm_type  *throt_sm_type_p
);

/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_SET_FAILURE_CAUSE_CODE

DESCRIPTION
  This function sets the failure cause based on the PDP type
 
PARAMETERS   : 
 
  pdn_throt_sm_ptr					 -   PDN throttle state machine pointer to
 									               set the cause code in the core
									               throttle state machine
  cause_code			        	 -   Failure cause code to be set
  pdn_pdp_type                       -   PDP type
									  

DEPENDENCIES
  None.

RETURN VALUE
  boolean -  TRUE indicating SUCCESS
             FALSE indicating pdn_throt sm ptr has not been allocated

SIDE EFFECTS
  None.
============================================================================*/
boolean ds_3gpp_pdn_throt_set_failure_cause_code
(
  void                          *pdn_throt_sm_ptr,
  void                          *cause_code,
  ds_umts_pdp_type_enum_type    pdn_pdp_type
);


/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_HANDLE_PERM_CONN_FAILURE

DESCRIPTION
  This function sets INFINITE THROTTLING for a PDN context.
  Set the is_throttled flag to TRUE, Set the Timer value to 0xFFFFFFFF
  

PARAMETERS   : 
  pdn_throt_sm_ptr					 -   PDN throttle state machine pointer to
 									               set the cause code in the core
									               throttle state machine
  pdn_pdp_type                       -   PDP type
 
DEPENDENCIES
  None.

RETURN VALUE
  None.
 
SIDE EFFECTS
  None.

===========================================================================*/

void ds_3gpp_pdn_throt_handle_perm_conn_failure
(
  void                        *pdn_throt_sm_ptr,
  ds_umts_pdp_type_enum_type   pdn_pdp_type,
  sys_modem_as_id_e_type       subs_id
);

/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_CLEAR_INFINITE_THROTTLING

DESCRIPTION
  This function clears INFINITE THROTTLING for a PDN context.
  The is_throttled flag is set to FALSE and the Timer value is cleared.
  

PARAMETERS: 
  None.
 
DEPENDENCIES
  None.

RETURN VALUE
  None.
 
SIDE EFFECTS
  None.

===========================================================================*/

void ds_3gpp_pdn_throt_clear_infinite_throttling
(
  sys_modem_as_id_e_type subs_id
);

#ifdef FEATURE_DATA_LTE
/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_IS_ANY_APN_THROTTLED_FOR_MAX_FAILURE

DESCRIPTION
  This function is used to check of any APN is throttled for max count 
  

PARAMETERS: 
  subs_id.
 
DEPENDENCIES
  None.

RETURN VALUE
  true - if any APN is throttled and fail count reached maximum
   flase - otherwise.
 
SIDE EFFECTS
  None.

===========================================================================*/

boolean ds_3gpp_pdn_throt_is_any_apn_throttled_for_max_failure
(
  sys_modem_as_id_e_type subs_id
);
#endif /* FEATURE_DATA_LTE */

/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_SM_SET_SM_TYPE

DESCRIPTION
  This function sets the pdn throttling state machine type
  
PARAMETERS   : 
  pdn_throttle_sm_ptr - pdn throttling pointer
  sm_type             - state machine type

DEPENDENCIES
  None.

RETURN VALUE
 
SIDE EFFECTS
  None.

===========================================================================*/
void ds_3gpp_pdn_throt_sm_set_sm_type
(
  ds_3gpp_pdn_throttle_sm_s *pdn_throttle_sm_ptr,
  ds_3gpp_pdn_throttle_sm_type     sm_type
);

/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_RESET_PDN_THROTTLING

DESCRIPTION
  This function clears cause code specific THROTTLING for a PDN context.
  The is_throttled flag is set to FALSE and the Timer value is cleared.
  

PARAMETERS: 
  None.
 
DEPENDENCIES
  None.

RETURN VALUE
  None.
 
SIDE EFFECTS
  None.

===========================================================================*/
void ds_3gpp_pdn_throt_reset_pdn_throttle(sys_modem_as_id_e_type);

/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_PERFORM_REG_THROTTLING

DESCRIPTION
  This function performs regular throttling and sets failure cause code
  

PARAMETERS   : 
  pdn_throt_sm_ptr		-  PDN throttle state machine pointer to
 						   Set the cause code in the core
 						   throttle state machine
  ds_pdn_context_s      -  Rejected PDN cntxt pointer
  cause_code            -  ESM cause code (void *)
  pdn_pdp_type          -  PDP type
 
DEPENDENCIES
  None.

RETURN VALUE
  None.
 
SIDE EFFECTS
  None.

===========================================================================*/

void ds_3gpp_pdn_throt_perform_reg_throttling
(
  void	                         *pdn_throt_sm_ptr,
  ds_pdn_context_s               *pdn_cntxt_ptr,
  void                           *cause_code,
  ds_umts_pdp_type_enum_type 	  pdn_pdp_type
);

/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_VALIDATE_MIN_FAILURE_COUNT

DESCRIPTION
  This function ensures that the throttle failure count is atleast
  equal to the minimum throttle failure count passed
  

PARAMETERS   : 
  ds_pdn_context_s           -  Rejected PDN cntxt pointer
  min_throt_failure_cnt      -  The min failure count of V4/V6 core
                                throttle SM
  pdn_pdp_type               -  PDP type
 
DEPENDENCIES
  None.

RETURN VALUE
  None.
 
SIDE EFFECTS
  None.

===========================================================================*/
void ds_3gpp_pdn_throt_validate_min_failure_count
(
  ds_pdn_context_s               *pdn_cntxt_ptr,
  ds_umts_pdp_type_enum_type      pdn_pdp_type,
  uint8                           min_throt_failure_cnt         
);

/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_PERFORM_APN_THROTTLING

DESCRIPTION
  This function performs PDN throttling based on Release 10 requirements
  

PARAMETERS   : 
  pdn_throt_sm          -  PDN throttle state machine pointer to
 									              set the cause code in the core throttle
                                state machine
  ds_pdn_context_s           -  Rejected PDN cntxt pointer
  cause_code                 -  ESM cause code (void *)
  pdn_pdp_type               -  PDP type
  is_global                  -  Flag indicating if throttling is global
                                or per plmn
 
DEPENDENCIES
  None.

RETURN VALUE
  None.
 
SIDE EFFECTS
  None.

===========================================================================*/

void ds_3gpp_pdn_throt_perform_apn_throttling
(
  void                                *pdn_throt_sm_ptr,
  byte                                *apn,
  void                                *cause_code,
  ds_umts_pdp_type_enum_type  	       pdn_pdp_type,
  sys_modem_as_id_e_type               subs_id,
  boolean                              is_global
);

/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_PERFORM_ROAMING_THROTTLING

DESCRIPTION
  This function performs PDN throttling based on Roaming Requirement.
  

PARAMETERS
  APN name to be throttled
  Subscription Id
 
DEPENDENCIES
  None.

RETURN VALUE
  None.
 
SIDE EFFECTS
  None.
===========================================================================*/
void ds_3gpp_pdn_throt_perform_roaming_throttling
(
  byte                                *apn,
  sys_modem_as_id_e_type               subs_id
);

/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_PERFORM_APN_DISABLED_THROTTLING

DESCRIPTION
  This function performs PDN throttling because APN was disabled.
  

PARAMETERS
  APN name to be throttled
  Subscription Id
 
DEPENDENCIES
  None.

RETURN VALUE
  None.
 
SIDE EFFECTS
  None.
===========================================================================*/
void ds_3gpp_pdn_throt_perform_apn_disabled_throttling
(
  byte                                *apn,
  sys_modem_as_id_e_type               subs_id
);

/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_UNBLOCK_ROAMING_THROTTLING

DESCRIPTION
  This function unblocks roaming throttling for given APN or for all
  APNs blocked due to roaming.
  
PARAMETERS
  1. APN name to be throttled
  2. Subscription Id
  3. Flag to indicate whether only the given APN should be unblocked or
     all APNs blocked due to roaming should be unblocked.
 
DEPENDENCIES
  None.

RETURN VALUE
  None.
 
SIDE EFFECTS
  None.
===========================================================================*/
void ds_3gpp_pdn_throt_unblock_roaming_throttling
(
  byte*                                apn,
  sys_modem_as_id_e_type               subs_id,
  boolean                              unblock_all_apns
);

/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_UNBLOCK_APN_DISABLED_THROTTLING

DESCRIPTION
  This function unblocks PDN throttling because APN was re-enabled.
  
PARAMETERS
  1. APN name
  2. Subscription Id
 
DEPENDENCIES
  None.

RETURN VALUE
  None.
 
SIDE EFFECTS
  None.
===========================================================================*/
void ds_3gpp_pdn_throt_unblock_apn_disabled_throttling
(
  byte*                                apn,
  sys_modem_as_id_e_type               subs_id
);

/*===========================================================================
FUNCTION      DS_3GPP_APN_THROT_SET_T3396_TIMER

DESCRIPTION   This function saves the PDN throttling t3396 timer value 
              corresponding to the APN for which it was received.

PARAMETERS 
apn - apn name for which it was received 
timer_val - t3396 timer value 
plmn_id   - plmn passed by the client
subs_id   - subscription id passed by the client

RETURN VALUE 
void 

SIDE EFFECTS  None
===========================================================================*/
boolean ds_3gpp_apn_throt_set_t3396_timer 
(
  byte                   *apn,
  uint32                 timer_val,
  sys_plmn_id_s_type     plmn_id,
  sys_modem_as_id_e_type subs_id,
  ps_iface_net_down_reason_type down_reason 
);

/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_SM_RESET_REL_10_PERM_PLMN_SM

DESCRIPTION
  This function resets the pdn throttling state machine for a release 10
  based throttling sm. 
  
PARAMETERS   : 
  pdn_throttle_sm_ptr - pdn throttling pointer

DEPENDENCIES
  None.

RETURN VALUE
 
SIDE EFFECTS
  None.

===========================================================================*/

void ds_3gpp_pdn_throt_sm_reset_rel_10_per_plmn_sm
(
  ds_3gpp_pdn_throttle_sm_s *per_plmn_throt_sm_ptr
);
/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_SET_GLOBAL_THROTTLE_FLAG

DESCRIPTION
  This function sets the global throttle flag for the given APN

PARAMETERS   : 
  apn - APN name
 
DEPENDENCIES
  None.

RETURN VALUE
  None.
 
SIDE EFFECTS
  None.

===========================================================================*/
void ds_3gpp_pdn_throt_set_global_throttle_flag
(
  byte                     *apn,
  sys_modem_as_id_e_type    subs_id
);

/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_SET_ROAMING_THROTTLE_FLAG

DESCRIPTION
  This function sets the Roaming throttle flag for the given index

PARAMETERS
  Index ptr - Index to the APN based throttle SM.
  Flag      - Indicates throttle or no throttle
  
DEPENDENCIES
  None.

RETURN VALUE
  None.
 
SIDE EFFECTS
  None.

===========================================================================*/
void ds_3gpp_pdn_throt_set_roaming_throttle_flag
(
  void    *index_ptr,
  boolean  flag
);

/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_SET_APN_DISABLED_FLAG

DESCRIPTION
  This function sets the APN Disabled flag for the given index.

PARAMETERS
  Index ptr - Index to the APN based throttle SM.
  Flag      - Indicates throttle or no throttle
  
DEPENDENCIES
  None.

RETURN VALUE
  None.
 
SIDE EFFECTS
  None.

===========================================================================*/
void ds_3gpp_pdn_throt_set_apn_disabled_flag
(
  void    *index_ptr,
  boolean  flag
);

/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_GET_ROAMING_THROTTLE_FLAG

DESCRIPTION
  This function gets the Roaming throttle flag for the given index

PARAMETERS
  Index ptr - Index to the APN based throttle SM.
  
DEPENDENCIES
  None.

RETURN VALUE
  TRUE, if throttled
  FALSE, otherwise
 
SIDE EFFECTS
  None.

===========================================================================*/
boolean ds_3gpp_pdn_throt_get_roaming_throttle_flag
(
  void    *index_ptr
);

/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_GET_BLOCKED_APN_LIST_ON_CUR_PLMN

DESCRIPTION
  This function gets the currently blocked list of of APNs from the PDN throt
  SM on current PLMN.
  
PARAMETERS   : 
 
  list - list of blocked APNs (output parameter)

DEPENDENCIES
  None.

RETURN VALUE
  None.
 
SIDE EFFECTS
  None.

===========================================================================*/

boolean ds_3gpp_pdn_throt_get_blocked_apn_list_on_cur_plmn
(
  ds_3gpp_pdn_throt_apn_list_type *blocked_apn_list,
  uint8                           *num_blocked_apn,
  sys_modem_as_id_e_type           subs_id
);

/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_ADVERTISE_GLOBALLY_BLOCKED_APNS_to_APM

DESCRIPTION
  This function sends the currently blocked list of of APNs to APM
  
PARAMETERS   : 
  globally_blocked_apn_list
  num_blocked_apns
 
DEPENDENCIES
  None.

RETURN VALUE
  
SIDE EFFECTS
  None.

===========================================================================*/
void ds_3gpp_pdn_throt_advertise_globally_blocked_apns_to_apm
(
  ds_3gpp_pdn_throt_apn_list_type *globally_blocked_apn_list,
  uint8                            num_blocked_apns,
  sys_modem_as_id_e_type           subs_id
);

/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_UNBLOCK_APN_TIMER_EXP_CMD_HDLR

DESCRIPTION
  This function processes cmd to unblock the APN on expiry of the throttling
  timer
  
PARAMETERS   : 
  apn
 
DEPENDENCIES
  None.

RETURN VALUE
  None.
 
SIDE EFFECTS
  None.

===========================================================================*/

void ds_3gpp_pdn_throt_unblock_apn_timer_exp_cmd_hdlr
(
  void                  *throt_sm_p,
  sys_modem_as_id_e_type subs_id
);

/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_UNBLOCK_ALL_APN_GLOBALLY_BLKED_APN

DESCRIPTION
  This function processes cmd to unblock all the globally blocked APN
  
PARAMETERS   : 
  void
 
DEPENDENCIES
  None.

RETURN VALUE
  None.
 
SIDE EFFECTS
  None.

===========================================================================*/
void ds_3gpp_pdn_throt_unblock_all_globally_blked_apn
(
  sys_modem_as_id_e_type subs_id
);

/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_TIMESTAMP_APN_THROT_TMR_CMD_HDLR

DESCRIPTION
  This function timestamps the APN throttle timer event in EFS
  
PARAMETERS   : 
  throt_sm_p      - pointer to core Throt state machine
 
DEPENDENCIES
  None.

RETURN VALUE
  None.
 
SIDE EFFECTS
  None.

===========================================================================*/
void ds_3gpp_pdn_throt_timestamp_apn_throt_tmr_cmd_hdlr
(
  void*                  throt_sm_p,
  sys_modem_as_id_e_type subs_id
);

/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_IMSI_INFO_AVAIL_CMD_HDLR

DESCRIPTION
  This function checks to see if the IMSI info matches the one stored in EFS.
  If yes, powerup global APN throttling is performed. Else, it has already
  been done, so nothing is done in this function.
 
PARAMETERS
  None.

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
void ds_3gpp_pdn_throt_imsi_info_avail_cmd_hdlr
(
  sys_modem_as_id_e_type subs_id
);

/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_IS_APN_GLOBALLY_THROTTLED

DESCRIPTION
  This function checks if given APN is globally throttled across all PLMNs.

PARAMETERS   :
   byte* - apn name ptr
 
DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None

===========================================================================*/
boolean ds_3gpp_pdn_throt_is_apn_globally_throttled
(
  byte                   *apn,
  sys_modem_as_id_e_type  subs_id
);

/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_IS_APN_ROAMING_THROTTLED

DESCRIPTION
  This function checks if given APN is throttled due to roaming.

PARAMETERS   :
   byte* - apn name ptr
   Subscription id
 
DEPENDENCIES
  None.

RETURN VALUE
  TRUE, if throttled
  FALSE, otherwise

SIDE EFFECTS
  None

===========================================================================*/
boolean ds_3gpp_pdn_throt_is_apn_roaming_throttled
(
  byte                   *apn,
  sys_modem_as_id_e_type  subs_id
);

/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_IS_APN_DISABLED

DESCRIPTION
  This function checks if given APN is disabled.

PARAMETERS   :
   byte* - apn name ptr
   Subscription id
 
DEPENDENCIES
  None.

RETURN VALUE
  TRUE, if throttled
  FALSE, otherwise

SIDE EFFECTS
  None

===========================================================================*/
boolean ds_3gpp_pdn_throt_is_apn_disabled
(
  byte                   *apn,
  sys_modem_as_id_e_type  subs_id
);

/*===========================================================================
FUNCTION DS_3GPP_APN_THROTTLE_INIT

DESCRIPTION
  This function does the powerup initialization for the APN throttle
  functionality.

PARAMETERS
  None.

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
void ds_3gpp_apn_throt_init
(
  sys_modem_as_id_e_type subs_id
);

/*===========================================================================
FUNCTION      DS_3GPP_APN_THROT_CLEAR_T3396_TMR_ENTRY

DESCRIPTION   This function resets the t3396 timer entry from local cache and 
              also the EFS file.
 
PARAMETERS 
apn - apn name for which it was received 
plmn_id   - plmn passed by the client
subs_id   - subscription id passed by the client

RETURN VALUE 
void 

SIDE EFFECTS  None
===========================================================================*/
void ds_3gpp_apn_throt_clear_t3396_tmr_entry 
(
  byte                      *apn,
  sys_plmn_id_s_type         plmn_id,
  sys_modem_as_id_e_type     subs_id
);

/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_UMTS_HANLDE_REJECT_WITH_T3396_IE

DESCRIPTION
  This function handles the reject message received with a t3396 IE
  based on Release 10 requirements
  

PARAMETERS   : 
  gw_ps_call_info 					 -  GW PS call info received in reject from CM
  apn                        -  APN name on the PDP context
  down_reason                
 
DEPENDENCIES
  None.

RETURN VALUE
  None.
 
SIDE EFFECTS
  None.

===========================================================================*/
void ds_3gpp_pdn_throt_umts_handle_reject_with_t3396_ie
(
  cm_gw_ps_call_info_s_type    *gw_ps_call_info,
  byte                         *apn,
  ps_iface_net_down_reason_type down_reason,
  sys_modem_as_id_e_type        subs_id
);

/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_LTE_HANLDE_REJECT_WITH_T3396_IE

DESCRIPTION
  This function handles the reject message received with a t3396 IE
  based on Release 10 requirements
  

PARAMETERS   : 
  lte_call_info 				  	 -  LTE call info received in reject from CM
  apn                        -  APN name on the PDN connection
  down_reason
 
DEPENDENCIES
  None.

RETURN VALUE
  None.
 
SIDE EFFECTS
  None.

===========================================================================*/
void ds_3gpp_pdn_throt_lte_handle_reject_with_t3396_ie
(
  cm_lte_call_info_s_type      *lte_call_info,
  byte                         *apn,
  ps_iface_net_down_reason_type down_reason,
  sys_modem_as_id_e_type        subs_id,
  ds_pdn_context_s             *pdn_context_p
);

/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_HANLDE_NW_INITED_UNBLOCK_APN 
 
DESCRIPTION
  This function handles the network initiated to unblock a globally blocked 
  APN based on Release 10 requirements
  

PARAMETERS   : 
  apn                        -  APN name on the PDN connection

DEPENDENCIES
  None.

RETURN VALUE
  None.
 
SIDE EFFECTS
  None.

===========================================================================*/
void ds_3gpp_pdn_throt_handle_nw_inited_unblock_apn
(
  byte                     *apn,
  sys_modem_as_id_e_type    subs_id
);

/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_SAMPLING_TIMER_EXP_CMD_HDLR 
 
DESCRIPTION
  This function handles the sampling timer expiration command.
  
PARAMETERS   : 
  void

DEPENDENCIES
  None.

RETURN VALUE
  None.
 
SIDE EFFECTS
  None.

===========================================================================*/
void ds_3gpp_pdn_throt_sampling_timer_exp_cmd_hdlr
(
  sys_modem_as_id_e_type subs_id
);



#ifdef FEATURE_DATA_LTE
/*===========================================================================
FUNCTION  DS_3GP_PDN_THROT_VALIDATE_APN_REJECT_NAME

DESCRIPTION
  This function checks the EFS item to see if enable APN reject has been set.
  It it has not been set the apn_name will be NULL

PARAMETERS
                     
 
DEPENDENCIES
  None.

RETURN VALUE
   True - If the apn has been set in EFS
   False - Otherwise

SIDE EFFECTS
  None.

===========================================================================*/
boolean ds_3gpp_pdn_throt_validate_apn_reject_name
(
   void
);

/*===========================================================================
FUNCTION  DS_3GPP_PDN_THROT_SET_APN_REJECT_PDN_BLOCKED

DESCRIPTION
  This function sets PDN _BLOCKED in the apn_reject sm

PARAMETERS
   void              
 
DEPENDENCIES
  None.

RETURN VALUE
   True - If SET is successful
   False - Otherwise

SIDE EFFECTS
  None.

===========================================================================*/
boolean ds_3gpp_pdn_throt_set_apn_reject_pdn_blocked
(      
   boolean set_val
);

/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_APN_REJ_SM_INIT

DESCRIPTION
  This function initialize the apn reject sm

PARAMETERS   :

DEPENDENCIES
  None.

RETURN VALUE

SIDE EFFECTS
  None

===========================================================================*/
void ds_3gpp_pdn_throt_apn_rej_sm_init
(
   void
);

/*===========================================================================
FUNCTION  DS_3GPP_PDN_THROT_APN_REJECT_CLR_TIMER

DESCRIPTION
  This function clears the t3402 timer 

PARAMETERS
   void            
 
DEPENDENCIES
  None.

RETURN VALUE 
  void 

SIDE EFFECTS
  None.

===========================================================================*/
void ds_3gpp_pdn_throt_apn_reject_clr_timer
(
   void
);
/*===========================================================================
FUNCTION  DS_3GPP_PDN_THROT_APN_REJECT_T3402_TIMER_START

DESCRIPTION
  This function starts the t3402 timer receiving the start timer cb

PARAMETERS
   void            
 
DEPENDENCIES
  None.

RETURN VALUE 
  void 

SIDE EFFECTS
  None.

===========================================================================*/
void ds_3gpp_pdn_throt_apn_reject_t3402_timer_start
(
   void
);

/*===========================================================================
FUNCTION  DS_3GPP_PDN_THROT_APN_REJECT_T3402_TIMER_EXPIRY_CMD_HDLR

DESCRIPTION
  This function resets the sm on timer expiry

PARAMETERS
   void            
 
DEPENDENCIES
  None.

RETURN VALUE 
  void 

SIDE EFFECTS
  None.

===========================================================================*/

void ds_3gpp_pdn_throt_apn_reject_t3402_timer_expiry_cmd_hdlr
(
   void
);

/*===========================================================================
FUNCTION  DS_3GP_PDN_THROT_VALIDATE_APN_REJECT_NAME

DESCRIPTION
  This function checks the EFS item to see if enable APN reject has been set.
  It it has not been set the apn_name will be NULL

PARAMETERS
                     
 
DEPENDENCIES
  None.

RETURN VALUE
   True - If the apn has been set in EFS
   False - Otherwise

SIDE EFFECTS
  None.

===========================================================================*/
boolean ds_3gpp_pdn_throt_validate_apn_reject_name
(
   void
);

/*===========================================================================
FUNCTION DS_3GPP_PDN_THROT_APN_REJ_SM_INIT

DESCRIPTION
  This function initialize the apn reject sm

PARAMETERS   :

DEPENDENCIES
  None.

RETURN VALUE

SIDE EFFECTS
  None

===========================================================================*/
void ds_3gpp_pdn_throt_apn_rej_sm_init
(
   void
);

/*===========================================================================
FUNCTION  DS_3GPP_PDN_THROT_APN_REJECT_CLR_TIMER

DESCRIPTION
  This function clears the t3402 timer 

PARAMETERS
   void            
 
DEPENDENCIES
  None.

RETURN VALUE 
  void 

SIDE EFFECTS
  None.

===========================================================================*/
void ds_3gpp_pdn_throt_apn_reject_clr_timer
(
   void
);
/*===========================================================================
FUNCTION  DS_3GPP_PDN_THROT_APN_REJECT_T3402_TIMER_START

DESCRIPTION
  This function starts the t3402 timer receiving the start timer cb

PARAMETERS
   void            
 
DEPENDENCIES
  None.

RETURN VALUE 
  void 

SIDE EFFECTS
  None.

===========================================================================*/
void ds_3gpp_pdn_throt_apn_reject_t3402_timer_start
(
   void
);

/*===========================================================================
FUNCTION  DS_3GPP_PDN_THROT_APN_REJECT_T3402_TIMER_EXPIRY_CMD_HDLR

DESCRIPTION
  This function resets the sm on timer expiry

PARAMETERS
   void            
 
DEPENDENCIES
  None.

RETURN VALUE 
  void 

SIDE EFFECTS
  None.

===========================================================================*/

void ds_3gpp_pdn_throt_apn_reject_t3402_timer_expiry_cmd_hdlr
(
   void
);


/*===========================================================================
FUNCTION  DS_3GPP_PDN_THROT_GET_APN_REJECT_PDN_BLOCKED_STATUS

DESCRIPTION
  This function gets the PDN blocked flag from Reject SM

PARAMETERS
  void           
 
DEPENDENCIES
  None.

RETURN VALUE
  TRUE - If the flag is set
  FALSE - Otherwise

SIDE EFFECTS
  None.

===========================================================================*/
boolean ds_3gpp_pdn_throt_get_apn_reject_pdn_blocked_status
(      
  void
);

/*===========================================================================
FUNCTION  DS_3GPP_PDN_THROT_APN_REJECT_GET_T3402_TIMER_VAL

DESCRIPTION
  This function gets the remaining Timer value of the throttle SM

PARAMETERS
   void            
 
DEPENDENCIES
  None.

RETURN VALUE 
  timer_val: Remaining Timer value 

SIDE EFFECTS
  None.

===========================================================================*/

uint32 ds_3gpp_pdn_throt_apn_reject_get_t3402_timer_val
(
  void
);

/*===========================================================================
FUNCTION  DS_3GPP_PDN_THROT_APN_REJECT_GET_APN_NAME

DESCRIPTION
  This function stores the Reject APN in the pointer passed

PARAMETERS
  apn_name: Memory block in which APN name has to be copied            
 
DEPENDENCIES
  None.

RETURN VALUE 
  None 

SIDE EFFECTS
  None.

===========================================================================*/
void ds_3gpp_pdn_throt_apn_reject_get_apn_name
(
   char                            *apn_name
);

/*===========================================================================
FUNCTION  DS_3GPP_PDN_THROT_APN_REJECT_GET_APN_NAME

DESCRIPTION
  This function obtains the list of APNs throttled on
  current PLMN and advertises this list to APM.

PARAMETERS
  Subscription Id            
 
DEPENDENCIES
  None.

RETURN VALUE 
  None 

SIDE EFFECTS
  None.

===========================================================================*/
void ds_3gpp_pdn_throt_advertise_blocked_apns_current_plmn
(
  sys_modem_as_id_e_type    subs_id
);

/*===========================================================================
FUNCTION  DS_3GPP_PDN_THROT_LTE_DISABLE_THROTTLE_TIMER_VALUE

DESCRIPTION
  This function calculates the amount time PLMN needs to be blocked.
  
PARAMETERS
  Subscription Id            
 
DEPENDENCIES
  None.

RETURN VALUE 
  Timer value in seconds

SIDE EFFECTS
  None.

===========================================================================*/
uint32 ds_3gpp_pdn_throt_lte_disable_throttle_timer_value
(
   sys_modem_as_id_e_type    subs_id
);

/*===========================================================================
FUNCTION  DS_3GPP_PDN_THROT_CHECK_IF_BLOCK_PLMN_LIST_NEEDS_UPDATE

DESCRIPTION
  This function checks if the blocked PLMN List can be updated.
 
PARAMETERS
  Subscription Id          
 
DEPENDENCIES
  None.

RETURN VALUE 
  TRUE, if all Attach APNs are  
        1. Globally blocked or
        3. Blocked due to APN being disabled 
  FALSE, otherwise

SIDE EFFECTS
  None.

===========================================================================*/
boolean ds_3gpp_pdn_throt_check_if_block_plmn_list_needs_update
(
  sys_modem_as_id_e_type           subs_id
);
/*===========================================================================

FUNCTION  DS_3GPP_PDN_THROT_SET_APN_REJECT_PRF_NUM      

DESCRIPTION
  This function sets the profile number in the apn_reject sm

PARAMETERS
  uint8 profile_number
 
DEPENDENCIES
  None.

RETURN VALUE
   True - If NV is set
   False - Otherwise

SIDE EFFECTS
  None.

===========================================================================*/
boolean ds_3gpp_pdn_throt_set_apn_reject_prf_num
(      
   uint8 profile_number
);

/*===========================================================================
FUNCTION  DS_3GPP_PDN_THROT_EV_ALLOC_MEM

DESCRIPTION
  This function would allocate memory for pdn_throt_ev_ptr from heap

PARAMETERS
  Void
 
DEPENDENCIES
  None.

RETURN VALUE 
  ds_3gpp_pdn_throttle_event_type ptr
 
SIDE EFFECTS
  None.

===========================================================================*/

ds_3gpp_pdn_throttle_event_type *ds_3gpp_pdn_throt_ev_alloc_mem
(
   void 
);

/*===========================================================================
FUNCTION  DS_3GPP_PDN_THROT_GEN_EVENT

DESCRIPTION
  This function would generate an event when pdn throttling
  happens

PARAMETERS
  ds_3gpp_pdn_throttle_event_type ptr
 
DEPENDENCIES
  The ptr must be populated with appropriate values

RETURN VALUE 
  void
 
SIDE EFFECTS
  None.

===========================================================================*/

void  ds_3gpp_pdn_throt_gen_event
(
   ds_3gpp_pdn_throttle_event_type *pdn_throt_ev_ptr
);

/*===========================================================================
FUNCTION  DS_3GPP_PDN_THROT_HDL_GEN_EVENT

DESCRIPTION
  This function would populate the structure and generate an event
  when pdn throttling happens

PARAMETERS
   uint8                            failure_count
   uint16                           timer_val
   boolean                          is_throttled
   sys_modem_as_id_e_type           subs_id
   sys_plmn_id_s_type               *plmn_id
   uint8                            throttle_ip_type
   char                             *apn
   uint8                            profile_id
 
DEPENDENCIES

RETURN VALUE 
  void
 
SIDE EFFECTS
  None.

===========================================================================*/
void ds_3gpp_pdn_throt_hdl_gen_event
(
   uint8                            failure_count,
   uint32                           timer_val,
   boolean                          is_throttled,
   sys_modem_as_id_e_type           subs_id,
   sys_plmn_id_s_type               *plmn_id,
   uint8                            throttle_ip_type,
   char                             *apn,
   uint8                            profile_id
);
/*===========================================================================
FUNCTION  DS_3GPP_PDN_THROT_LOG_THROT_INFO

DESCRIPTION
  This function would check if the pdn throttling information would needed
  to be logged based on the is_throttled flag

PARAMETERS
   ds_3gppi_throttle_sm_s*     throt_sm_ptr,
   ds_umts_pdp_type_enum_type  pdn_pdp_type
 
DEPENDENCIES
  None

RETURN VALUE 
  void
 
SIDE EFFECTS
  PDN PDP type can only be IPV4 or IPV6, It cannot be V4V6 here.

===========================================================================*/

void ds_3gpp_pdn_throt_log_throt_info
(
   ds_3gppi_throttle_sm_s     *throt_sm_ptr,
   byte                       *apn,
   ds_umts_pdp_type_enum_type  pdn_pdp_type
);

/*===========================================================================
FUNCTION  DS_3GPP_PDN_THROT_CLR_LOG_ENTRY

DESCRIPTION
  This function would delete profile number from pdn_throt_profile_num_list

PARAMETERS
   ds_3gppi_throttle_sm_s*     throt_sm_ptr,
 
DEPENDENCIES
  None

RETURN VALUE 
  void 
 
SIDE EFFECTS

===========================================================================*/

void ds_3gpp_pdn_throt_clr_log_entry
(
   ds_3gppi_throttle_sm_s     *throt_sm_ptr
);


/*===========================================================================
FUNCTION  DS_3GPP_PDN_THROT_GET_PROFILE_NUMBER

DESCRIPTION
  This function would get profile number to pdn_throt_profile_num_list

PARAMETERS
   ds_3gppi_throttle_sm_s*     throt_sm_ptr,
 
DEPENDENCIES
  None

RETURN VALUE 
  uint16                       profile_number
 
SIDE EFFECTS

===========================================================================*/

uint16 ds_3gpp_pdn_throt_get_profile_number
(
   ds_3gppi_throttle_sm_s     *throt_sm_ptr
);

/*===========================================================================
FUNCTION  DS_3GPP_PDN_THROT_ADD_LOG_ENTRY

DESCRIPTION
  This function would add profile number to pdn_throt_profile_num_list

PARAMETERS
   ds_3gppi_throttle_sm_s*     throt_sm_ptr,
   uint16                      profile_num
 
DEPENDENCIES
  None

RETURN VALUE 
  void
 
SIDE EFFECTS

===========================================================================*/

void ds_3gpp_pdn_throt_add_log_entry
(
   ds_3gppi_throttle_sm_s     *throt_sm_ptr,
   uint16                      profile_num  
);

/*===========================================================================
FUNCTION  DS_3GPP_PDN_THROT_UPDATE_LOG_LIST

DESCRIPTION
  This function would update the logging list from apn and ip type

PARAMETERS
   ds_3gppi_throttle_sm_s*     throt_sm_ptr,
  ds_umts_pdp_type_enum_type  pdn_pdp_type   
 
DEPENDENCIES
  None

RETURN VALUE 
  void
 
SIDE EFFECTS/DEPENDENCIES 
PDP type must be IPV4 or IPV6 
 

===========================================================================*/

void ds_3gpp_pdn_throt_update_log_list
(
   ds_3gppi_throttle_sm_s     *throt_sm_ptr,
   ds_umts_pdp_type_enum_type  pdn_pdp_type 
);

#endif /* FEATURE_DATA_LTE */

#endif /* DS_3GPP_PDN_THROTTLE_SM_H */
