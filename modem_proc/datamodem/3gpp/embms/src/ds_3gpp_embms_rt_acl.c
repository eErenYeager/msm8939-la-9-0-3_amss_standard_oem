/*==============================================================================

    E M B M S S P E C I F I C   M O D E   H A N D L E R

        A C C E S S   C O N T R O L   L I S T   D E F I N I T I O N S
 
       F O R   R O U T I N G   V I A   T H E  e M B M S   I N T E R F  A C E

GENERAL DESCRIPTION
  The embms specific routing ACL is used to enable policy and address
  based routing across the embms interface.

EXTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2011 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
==============================================================================*/

/*==============================================================================

                      EDIT HISTORY FOR FILE

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/3gpp/embms/src/ds_3gpp_embms_rt_acl.c#1 $ 

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/26/11    hs     Created file/Initial version.
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"

#ifdef FEATURE_DATA_EMBMS
#include "ps_acl.h"
#include "ps_aclrules.h"
#include "ps_iface.h"

#include "ds_3gpp_embms_rt_acl.h"
/*===========================================================================

                        FORWARD FUNCTION DECLARATIONS

===========================================================================*/

/*===========================================================================

                 ACCESS CONTROL LIST NAME DEFINITIONS

===========================================================================*/

/***  eMBMS ACL ***/
ACL_DEF( ds_3gpp_embms_rt_acl )
ACL_START
  PASS_QUICK( ACL_DEFAULT_CLASS, IS_POL_INFO_NULL() );
  DENY( DIFFERENT_IFACE_NAME_REQUIRED() );
  DENY( DIFFERENT_IFACE_ID_REQUIRED() );
  PASS( ACL_IFNAME_CLASS, REQUIRED_IFACE_TYPE_IS( EMBMS_IFACE ) );
  PASS( ACL_DEFAULT_CLASS, ALL );
ACL_END

#endif /* FEATURE_DATA_EMBMS */
