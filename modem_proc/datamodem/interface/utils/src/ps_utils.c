/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                           P S _ U T I L S . C

GENERAL DESCRIPTION
  Collection of utility functions being used by various modules in PS.
  Most of these functions assume that the caller is in PS task context.

Copyright (c) 1995-2012 Qualcomm Technologies Incorporated. 
All Rights Reserved.
Qualcomm Confidential and Proprietary
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/


/*===========================================================================

                        EDIT HISTORY FOR MODULE

  $PVCSPath: L:/src/asw/MM_DATA/vcs/ps_utils.c_v   1.0   08 Aug 2002 11:19:58   akhare  $
  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/utils/src/ps_utils.c#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when        who    what, where, why
--------    ---    ---------------------------------------------------------- 
10/26/12    bb     Added ps_efs_init and functionality to handle nv item 
                    file paths in protocols 
05/20/11    am     Dyn buffer for extra \n in EFS create.
10/26/10    op     Added functions to use EFS items.
11/21/08    pp     Lint fixes.
09/07/07    scb    Added REX signal ext code under FEATURE_REX_SIGS_EXT
11/02/06    mct    Added 64 bit random number generator function.
02/22/06    msr    Using single critical section
10/31/04   msr/ks  Added memdump().
04/30/04    mct    Fixed some lint errors.
08/05/02    usb    Moved lcsum() from this file to ps_iputil.c
08/05/02    usb    Fixed get/set nv functions, moved them out of FEATURE_MIP
07/31/02    usb    Renamed the file from psmisc.c
06/14/02    usb    Removed byte manipulation functions.  Use dsbyte.h now.
04/17/02    rc     Wrapped code in !FEATURE_DATA_WCDMA_PS
12/21/01    dwp    Wrap get16 in !FEATURE_DATA_PS as it is defined else where
                   in MSM5200 archive.
11/12/01    dwp    Add "|| FEATURE_DATA_PS" to whole module.
05/24/00    hcg    Added TARGET_OS_SOLARIS to compile with Solaris DSPE.
04/21/00    mvl    Fixed a #define so compiles properly under COMET
01/09/99    jjw    Changed to generic Browser interface
10/27/98    ldg    For T_ARM included C version of TCP checksum routine.
06/16/98    na     Converted the routine that calculates the TCP checksum
                   into 186 assembly.
06/25/97    jgr    Added ifdef FEATURE_DS over whole file
07/22/95    jjw    Created Initial Version
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"       /* Customer Specific Features */

#include "msg.h"
#include "err.h"
#include "amssassert.h"
#include "ps_crit_sect.h"
#include "ps.h"
#include "rcinit_rex.h"

#ifndef FEATURE_WINCE
#include "task.h"
#endif /* FEATURE_WINCE */

#include "ps_utils.h"
#include "ps_utils_init.h"

#include "ran.h"
#include "qw.h"
#include "time_svc.h"

#include "ds_Utils_DebugMsg.h"
#include "ps_system_heap.h"
#include "ps_utils_defs.h"

#include <stringl/stringl.h>
#include "secapi.h"

/*===========================================================================
                              MACRO DEFINITIONS
===========================================================================*/
/*---------------------------------------------------------------------------
  Macro to prevent lint warning 818 'Symbol could be declared as pointing to
   const'
---------------------------------------------------------------------------*/
#ifndef PS_ARG_NOT_CONST
#define PS_ARG_NOT_CONST(arg) /*lint -save -e717 -e506 -e774 */ (arg)=(arg);/*lint -restore*/
#endif

/*===========================================================================

                      LOCAL DECLARATIONS FOR MODULE

===========================================================================*/
/*---------------------------------------------------------------------------
  Command item to NV.
---------------------------------------------------------------------------*/
static nv_cmd_type  ps_nv_cmd_buf;

/*===========================================================================

                      DEFINITIONS FOR MODULE

===========================================================================*/
#ifdef FEATURE_MODEM_RCINIT_PHASE2
extern rex_tcb_type * rex_ps_tcb;
#endif
/*===========================================================================

FUNCTION PS_GET_NV_ITEM

DESCRIPTION
  This function retrieves the specified item from NV.

DEPENDENCIES
  The NV task has been started and is running.
  This functions is Non-reentrant.

RETURN VALUE
  Status returned from the NV read request.  An LOG_MSG_ERROR is logged
  if status is other than:
    NV_DONE_S       - request done
    NV_NOTACTIVE_S  - item was not active

SIDE EFFECTS
  While this function is running all other PS task activities are
  suspended except for watchdog kicking, and until the NV item is
  read in.

===========================================================================*/

nv_stat_enum_type ps_get_nv_item
(
  nv_items_enum_type  item_code,       /* Item to get                      */
  nv_item_type        *data_ptr        /* Pointer where to put the item    */
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

#ifdef FEATURE_MODEM_RCINIT_PHASE2
  ASSERT( rex_self() == rex_ps_tcb );
#else
  ASSERT( rex_self() == &ps_tcb );
#endif
  /*-------------------------------------------------------------------------
    Prepare command buffer to get the item from NV.
  -------------------------------------------------------------------------*/
  ps_nv_cmd_buf.cmd        = NV_READ_F;             /* Read request        */
#ifdef FEATURE_MODEM_RCINIT_PHASE2
  ps_nv_cmd_buf.tcb_ptr    = rex_ps_tcb;            /* Notify this task    */
#else
  ps_nv_cmd_buf.tcb_ptr    = &ps_tcb;               /* Notify this task    */
#endif
  ps_nv_cmd_buf.sigs       = 1 << (rex_sigs_type)PS_NV_CMD_SIGNAL; /* With this signal    */
  ps_nv_cmd_buf.done_q_ptr = NULL;             /* Do not enqueue when done */
  ps_nv_cmd_buf.item       = item_code;             /* Item to get         */
  ps_nv_cmd_buf.data_ptr   = data_ptr;              /* Where to return it  */

  /*-------------------------------------------------------------------------
   Clear signal, issue the command, and wait for the response.
  -------------------------------------------------------------------------*/
  PS_CLR_SIGNAL( PS_NV_CMD_SIGNAL );               /* Clear signal for NV  */
  nv_cmd( &ps_nv_cmd_buf );                        /* Issue the request    */
  (void)ps_wait((rex_sigs_type) 1 <<
                       (rex_sigs_type)PS_NV_CMD_SIGNAL ); /* Wait for completion*/

  if( ps_nv_cmd_buf.status != NV_DONE_S &&
      ps_nv_cmd_buf.status != NV_NOTACTIVE_S )
  {
    LOG_MSG_ERROR_2("ps_get_nv_item(): "
                    "NV Read Failed Item %d Code %d",
                    ps_nv_cmd_buf.item, ps_nv_cmd_buf.status );
  }
  return( ps_nv_cmd_buf.status );
} /* ps_get_nv_item() */


/*===========================================================================
FUNCTION PS_PUT_NV_ITEM

DESCRIPTION
  Write an item to NV memory.  Wait until write is completed.

RETURN VALUE
  Status returned from the NV read request.  An LOG_MSG_ERROR is logged 
  if status is other than:
    NV_DONE_S       - request done

DEPENDENCIES
  This function can only be called from PS task.  Also it is not
  reentrant. Shouldn't be a problem, as it doesn't exit till we're done, and
  it's only called from the PS task.

RETURN VALUE
  Status returned from the NV read request.  An LOG_MSG_ERROR is logged 
  if status is other than:
    NV_DONE_S       - request done
    NV_NOTACTIVE_S  - item was not active

SIDE EFFECTS
  While this function is running all other PS task activities are
  suspended except for watchdog kicking, and until the NV item is
  wrote down.

===========================================================================*/
nv_stat_enum_type ps_put_nv_item(
  nv_items_enum_type item_code,                              /* which item */
  nv_item_type *data_ptr                       /* pointer to data for item */
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

#ifdef FEATURE_MODEM_RCINIT_PHASE2
  ASSERT( rex_self() == rex_ps_tcb );
#else
  ASSERT( rex_self() == &ps_tcb );
#endif

  /*-------------------------------------------------------------------------
    Prepare command buffer to write the item to NV.
    TODO:NV team to be notified to change the nv_cmd_buf.sigs to an array
  -------------------------------------------------------------------------*/
  ps_nv_cmd_buf.cmd        = NV_WRITE_F;            /* Write request       */
#ifdef FEATURE_MODEM_RCINIT_PHASE2
  ps_nv_cmd_buf.tcb_ptr    = rex_ps_tcb;            /* Notify this task    */
#else
  ps_nv_cmd_buf.tcb_ptr    = &ps_tcb;               /* Notify this task    */
#endif
  ps_nv_cmd_buf.sigs       = 1 << (rex_sigs_type)PS_NV_CMD_SIGNAL; /* With this signal    */
  ps_nv_cmd_buf.done_q_ptr = NULL;             /* Do not enqueue when done */
  ps_nv_cmd_buf.item       = item_code;             /* Item to put         */
  ps_nv_cmd_buf.data_ptr   = data_ptr;              /* Data to write       */


  /*-------------------------------------------------------------------------
   Clear signal, issue the command, and wait for the response.
  -------------------------------------------------------------------------*/
  PS_CLR_SIGNAL( PS_NV_CMD_SIGNAL );
  nv_cmd( &ps_nv_cmd_buf );
  (void)ps_wait( (rex_sigs_type) 1 <<
                        (rex_sigs_type)PS_NV_CMD_SIGNAL ); /* Wait for completion*/

  if( ps_nv_cmd_buf.status != NV_DONE_S )
  {
    LOG_MSG_ERROR_2("ps_put_nv_item(): "
                    "NV Write Failed Item %d Code %d",
                    ps_nv_cmd_buf.item, ps_nv_cmd_buf.status );
  }

  return( ps_nv_cmd_buf.status );
} /* ps_put_nv_item() */


/*===========================================================================

FUNCTION msclock

DESCRIPTION
  This function will return the time in milliseconds since ....

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

dword msclock( void)
{
  qword qw_time;
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  time_get_ms( qw_time);

  return( qw_time[0]);

} /* msclock() */


/*===========================================================================
FUNCTION MEMDUMP()

DESCRIPTION
  Debug routine to dump memory to DM

PARAMETERS
  data_ptr -> address of memory to dump
  len      -> number of bytes to dump

RETURN VALUE
  None

DEPENDENCIES
  None

SIDE EFFECTS
  Memory access may go up to 7 bytes beyond last dumped byte, which could
  cause a data abort if dumping the last bytes of RAM.

===========================================================================*/
void memdump
(
  void * data_ptr,
  int    len
)
{
  char * data = (char *) data_ptr;
  int    i;                                                /* current byte */
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_INFO2_2 ("memdump(): "
                   "Dumping %d bytes @ %x", len, data);

  for (i=0; i < len; i+= 8)
  {
    MSG_8( MSG_SSID_DFLT,
           MSG_LEGACY_MED,
           "%02x %02x %02x %02x %02x %02x %02x %02x",
           data[i], data[i+1], data[i+2], data[i+3],
           data[i+4], data[i+5], data[i+6], data[i+7]);
  }

} /* memdump() */

void ps_utils_generate_rand_num
(
  void    *rand_num,
  uint16  rand_len
)
{
  secerrno_enum_type  sec_errno = E_FAILURE;
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  sec_errno = secapi_get_random( SECAPI_PKCS_RANDOM,
                                 (uint8 *)rand_num,
                                 rand_len);
  if (sec_errno != E_SUCCESS)
  {
    ERR_FATAL("ps_utils_generate_rand_num(): "
              "secapi_get_random failed error: %d",
               sec_errno, 0, 0 );
  }

  return;
} /* ps_utils_generate_rand_num */

word ps_utils_generate_16bit_rand_num_by_range
(
  word  lo_val,
  word  hi_val
)
{
  dword random_num;
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  /*------------------------------------------------------------------------ 
    Generate a 32 bit random number
  -------------------------------------------------------------------------*/
  ps_utils_generate_rand_num(&random_num, sizeof(uint32));

  return (ran_dist(random_num, lo_val, hi_val));
} /* ps_utils_generate_16bit_rand_num_by_range */

/*===========================================================================
FUNCTION PS_UTILS_GENERATE_RAND_64BIT_NUM()

DESCRIPTION
  This function generates a random 64 bit number.

PARAMETERS
  *random_num - Pointer to the 64 bit number to be returned by this function.

RETURN VALUE
  None

DEPENDENCIES
  ps_utils_generate_rand_num

SIDE EFFECTS
  None
===========================================================================*/
void ps_utils_generate_rand_64bit_num
(
  uint64 *rand_num                 /* Pointer to the 64bit num be returned */
)
{
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  ps_utils_generate_rand_num(rand_num, sizeof(uint64));

} /* ps_utils_generate_rand_64bit_num */
/*===========================================================================
FUNCTION PS_CREATE_EFS_CONFIG_FILE

DESCRIPTION
  Create a config file in EFS which stores the path of EFS item files.

DEPENDENCIES
  None

PARAMETERS
  conf_file_path - File path of config file

RETURN VALUE
  0         - Success
 -1         - Non-EFS related Failures
  efs_errno - EFS related failures. Meaning of this value can be
              found in fs_errno.h

SIDE EFFECTS
  None
===========================================================================*/
int32 ps_create_efs_config_file
(
  const char *conf_file_path
)
{
  int32                 config_fd, result;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if ( NULL == conf_file_path )
  {
    LOG_MSG_ERROR_0("ps_create_efs_config_file(): "
                    "conf_file_path is NULL");
    return -1;
  }
  
  /*-------------------------------------------------------------------------
    Create common directories if needed.
  -------------------------------------------------------------------------*/
  LOG_MSG_INFO2_0("ps_create_efs_config_file(): "
                  "EFS: Creating conf file if necessary");
  result = ps_path_is_directory("/nv");
  if( 0 != result )
  {
    /* Directory doesn't exist yet */
    LOG_MSG_INFO1_0("ps_create_efs_config_file(): "
                    "Create /nv dir in EFS");
    result = efs_mkdir( "/nv", S_IREAD|S_IWRITE|S_IEXEC);
    if ( -1 == result )
    {
      LOG_MSG_ERROR_1("ps_create_efs_config_file(): "
                      "Create EFS Dir Failed: error %d", efs_errno);
      return efs_errno;
    } 
  }

  result = ps_path_is_directory("/nv/item_files");
  if( 0 != result )
  {
    /* Directory doesn't exist yet */
    LOG_MSG_INFO1_0("ps_create_efs_config_file(): "
                    "Create /nv/item_files dir in EFS");
    result = efs_mkdir( "/nv/item_files", S_IREAD|S_IWRITE|S_IEXEC);
    if ( -1 == result )
    {
      LOG_MSG_ERROR_1("ps_create_efs_config_file(): "
                      "Create EFS Dir Failed: error %d", efs_errno);
      return efs_errno;
    }
  }

  result = ps_path_is_directory("/nv/item_files/conf");
  if( 0 != result )
  {
    /* Directory doesn't exist yet */
    LOG_MSG_INFO1_0("ps_create_efs_config_file(): "
                    "Create /nv/item_file/conf dir in EFS");
    result = efs_mkdir( "/nv/item_files/conf", S_IREAD|S_IWRITE|S_IEXEC);
    if ( -1 == result )
    {
      LOG_MSG_ERROR_1("ps_create_efs_config_file(): "
                      "Create EFS Dir Failed: error %d", efs_errno);
      return efs_errno;
    }
  }

  /*-------------------------------------------------------------------------
    Open conf file. Create conf file if does not exist.
    Resulting file is truncated to zero bytes.
  -------------------------------------------------------------------------*/  
  config_fd = efs_open (conf_file_path, O_WRONLY|O_CREAT|O_TRUNC, ALLPERMS);
  if ( 0 > config_fd )
  {
    LOG_MSG_ERROR_1("ps_create_efs_config_file(): "
                    "Error creating config file, error %d", efs_errno);
    return efs_errno;
  }

  result = efs_close (config_fd);
  if ( 0 != result )
  {
    LOG_MSG_ERROR_1("ps_create_efs_config_file(): "
                    "Error closing config file, error %d", efs_errno);
    return efs_errno;
  }
  return 0;

} /* ps_create_efs_config_file() */

/*===========================================================================
FUNCTION PS_CREATE_EFS_ITEM_FILE

DESCRIPTION
  Put(append and add newline) item_file_path into conf_file_path

DEPENDENCIES
  None

PARAMETERS
  conf_file_path - File path to a specific conf file
  item_file_path - File path to item in NV
  item_file_path_size - Size of item file path

RETURN VALUE
  0         - Success
 -1         - Non-EFS related Failures
  efs_errno - EFS related failures. Meaning of this value can be
              found in fs_errno.h

SIDE EFFECTS
  None
===========================================================================*/
int32 ps_create_efs_item_file
(
  const char   *conf_file_path,
  char         *item_file_path, 
  fs_size_t     item_file_path_size
)
{
  int32              config_fd, result;
  char              *file_loc;
  int32              ret_val = 0;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/ 
  if ( NULL == conf_file_path || NULL == item_file_path )
  {
    LOG_MSG_ERROR_0("ps_create_efs_item_file(): "
                    "conf_file_path or item_file_path is NULL");
    return -1;
  }

  /*-------------------------------------------------------------------------
    Open conf file. If it doesn't exist create it and reopen it.
  -------------------------------------------------------------------------*/  
  config_fd = efs_open (conf_file_path, O_RDWR|O_APPEND);
  if ( 0 > config_fd )
  {
    if ( ENOENT == efs_errno ) /* Conf file does not exist */
    {
      LOG_MSG_ERROR_0("ps_create_efs_item_file(): "
                      "EFS: Config file not present");
      LOG_MSG_INFO1_0("ps_create_efs_item_file(): "
                      "EFS: Creating config file");
      result = ps_create_efs_config_file(conf_file_path);
      if ( 0 != result )
      {
        LOG_MSG_ERROR_1("ps_create_efs_item_file(): "
                        "EFS: Error creating conf file, error %d", efs_errno);
        return efs_errno;
      }

      config_fd = efs_open (conf_file_path, O_RDWR|O_APPEND);
      if ( 0 > config_fd )
      {
        LOG_MSG_ERROR_1("ps_create_efs_item_file(): "
                        "EFS: Error opening config file, error %d", efs_errno);
        return efs_errno;
      }
    }
    else /* Could not open conf file for some other reason */
    {
      LOG_MSG_ERROR_1("ps_create_efs_item_file(): "
                      "Error opening config file, error %d", efs_errno);
      return efs_errno;
    }
  }

  file_loc = (char*)ps_system_heap_mem_alloc(item_file_path_size + 1);
  if (NULL == file_loc)
  {
    LOG_MSG_ERROR_0("ps_create_efs_item_file(): "
                    "Out of mem, can't create file");
    ret_val = -1;
    goto bail;
  }

  (void)strlcpy(file_loc, item_file_path, item_file_path_size + 1);
  file_loc[item_file_path_size] = '\n';
  result = efs_write (config_fd, file_loc, (item_file_path_size + 1));
  if ( (item_file_path_size + 1) != result )
  {
    LOG_MSG_ERROR_1("ps_create_efs_item_file(): "
                    "Error writing into config file, error %d", efs_errno);
    ret_val = efs_errno;
    goto bail;
  }

bail:  
  if (file_loc)
  {
    PS_SYSTEM_HEAP_MEM_FREE(file_loc);
  }

  result = efs_close (config_fd);
  if ( 0 != result )
  {
    LOG_MSG_ERROR_1("ps_create_efs_item_file(): "
                    "Error closing config file, error %d", efs_errno);
    return efs_errno;
  }

  return ret_val;
} /* ps_create_efs_item_file() */

/*===========================================================================
FUNCTION PS_READ_EFS_NV

DESCRIPTION
  This function reads the EFS item from the item file

DEPENDENCIES
  None

PARAMETERS
  item_file_path - File path to item in NV
  nv_info - Struct for NV item(s)
  nv_info_size - Size of NV item structure

RETURN VALUE
  0         - Success
 -1         - Non-EFS related Failures
  efs_errno - EFS related failures. Meaning of this value can be
              found in fs_errno.h

SIDE EFFECTS
  None
===========================================================================*/
int32 ps_read_efs_nv
(
  const char   *item_file_path, 
  void         *nv_info_ptr, 
  fs_size_t    nv_info_size
)
{
  int32                 retval = 0;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  LOG_MSG_INFO2_0("ps_read_efs_nv(): "
                  "Reading from EFS");

  if ( NULL == item_file_path || NULL == nv_info_ptr )
  {
    LOG_MSG_ERROR_0("ps_read_efs_nv(): "
                    "item_file_path is NULL or nv_info_ptr is NULL");
    return -1;
  }

  memset (nv_info_ptr, 0, nv_info_size);

  /*-------------------------------------------------------------------------
    Read the item file from EFS into nv_info_ptr
  -------------------------------------------------------------------------*/
  retval = efs_get( item_file_path, 
                    (void *)nv_info_ptr, 
                    nv_info_size); 

  if( 0 >= retval ) /* Error or 0 bytes read */
  {
    LOG_MSG_ERROR_1("ps_read_efs_nv(): "
                    "Unable to read EFS item, error %d ", efs_errno);
    return efs_errno;
  }

  return 0;
} /* ps_read_efs_nv() */

/*===========================================================================
FUNCTION PS_WRITE_EFS_NV

DESCRIPTION
  This function writes the EFS item to the item file

DEPENDENCIES
  None

PARAMETERS
  item_file_path - File path to item in NV
  nv_info - Pointer to NV item
  nv_info_size - Size of NV item

RETURN VALUE
  0         - Success
 -1         - Non-EFS related Failures
  efs_errno - EFS related failures. Meaning of this value can be
              found in fs_errno.h

SIDE EFFECTS
  None
===========================================================================*/
int32 ps_write_efs_nv
(
  const char   *item_file_path, 
  void         *nv_info_ptr, 
  fs_size_t    nv_info_size
)
{
  int32                 result = 0;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  LOG_MSG_INFO2_0("ps_write_efs_nv(): "
                  "Writing to EFS");

  if ( NULL == item_file_path || NULL == nv_info_ptr )
  {
    LOG_MSG_ERROR_0("ps_write_efs_nv(): "
                    "item_file_path is NULL or nv_info_ptr is NULL");
    return -1;
  }
  /*-------------------------------------------------------------------------
    Write the item into the item file. If file or file path does not exist,
    create the file path and file
  -------------------------------------------------------------------------*/
  result = efs_put( item_file_path, 
                    (void *)nv_info_ptr, 
                    nv_info_size,
                    O_CREAT|O_AUTODIR, 
                    ALLPERMS );
  if( 0 != result )
  {
    LOG_MSG_ERROR_1("ps_write_efs_nv(): "
                    "Unable to write EFS item, error %d ", efs_errno);
    return efs_errno;
  }

  return 0;
} /* ps_write_efs_nv() */

/*===========================================================================
FUNCTION PS_PATH_IS_DIRECTORY

DESCRIPTION
  To check if the EFS directory exists

DEPENDENCIES
  None

PARAMETERS
  dirname - Directory path

RETURN VALUE
   0         - success
   efs_errno - EFS error
   -1        - Other error

SIDE EFFECTS
  None
===========================================================================*/
int32 ps_path_is_directory
(
  const char *dirname
)
{
  int                           rsp;
  struct fs_stat                stat_info;
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  rsp = efs_lstat( dirname, &stat_info);
  if( 0 != rsp )
  {
    rsp = efs_errno;
    if( ENOENT != rsp )
    {
      LOG_MSG_ERROR_1("ps_path_is_directory(): "
                      "efs_lstat error %d", rsp);
      return rsp;
    }
  }
  else if( S_ISDIR (stat_info.st_mode))
  {
    return 0;
  }

  return -1;
} /* ps_path_is_directory */

/*===========================================================================
FUNCTION      PS_EFS_FILE_INIT

DESCRIPTION   The function intializes the state machine and 
              also opens the file

DEPENDENCIES  None.

RETURN VALUE   0  : SUCCESS: The file is good, readable, 
                             State Machine Initialized.
              -1 : FAILURE: The file cannot be opened/ readable. 

SIDE EFFECTS  None.
===========================================================================*/
int ps_efs_file_init
( 
  const char *file_path, 
  ps_efs_token_type *sm
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /*-------------------------------------------------------------------------
    Sanity checks on input parameters
  -------------------------------------------------------------------------*/
  if((NULL == file_path) || (NULL == sm))
  {
    LOG_MSG_INFO1_0("ps_efs_file_init(): "
                    "Input parameters are NULL!");
    return -1;
  }
  
  /*-------------------------------------------------------------------------
    Initialize the structure variables and open the file in read mode.
  -------------------------------------------------------------------------*/    
  sm->seperator   = ';';
  sm->curr        = sm->buffer;
  sm->end_pos     = sm->buffer;
  
  sm->eof         = FALSE;
  sm->eol         = FALSE;
  sm->skip_line   = FALSE;
  sm->bol         = TRUE;

  sm->fd = efs_open( file_path, O_RDONLY);
  if(sm->fd != -1) 
  {
    return 0;
  }
  else 
  {
    LOG_MSG_INFO1_0("ps_efs_file_init(): "
                    "Cannot open file");
    return -1;
  }
} /* ps_efs_file_init() */

/*===========================================================================
FUNCTION      PS_EFS_FILE_CLOSE

DESCRIPTION   The function closes file and releases the state machine 

DEPENDENCIES  The file should have opened already.

RETURN VALUE  NONE

SIDE EFFECTS  None.
===========================================================================*/
void ps_efs_file_close
( 
  ps_efs_token_type *sm
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if(NULL == sm)
  {
    LOG_MSG_INFO1_0("ps_efs_file_close(): "
                    "Input parameters are NULL!");
    return;
  }

  /* lint fix */
  PS_ARG_NOT_CONST(sm);
  (void) efs_close( sm->fd );
  return;
}

/*===========================================================================
FUNCTION      PS_EFS_TOKENIZER

DESCRIPTION   The is the function that reads data from the opened file.
              The data read is looked for tokens 
              1. Each token is seperated by ';'
              2. Successive ';' means empty token
              3. Line begining with '#' is comment
              4. '\n' is the end of token and record
              5. Empty line is ignored
              6. Insufficient tokens is a record is considered bad record
              
DEPENDENCIES  File should have already been opened.

RETURN VALUE   
              SUCCESS : Success => Found Token.
                        *begin points to the begining of token.
                        *end points to the end of token.
              EOL     : End of line is reached => record end 
                        => no token extracted
              END     : End of file is reached => file end => feof
                        => no token extracted
              FAILURE : Failed 

SIDE EFFECTS  None.
===========================================================================*/
ps_efs_token_parse_status_enum_type ps_efs_tokenizer
(
  ps_efs_token_type *sm,
  char **begin,
  char **end
)
{
  int bytes_read = 0;
  char *dummy;
  ps_efs_token_parse_status_enum_type retval;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if ((NULL == sm) || (NULL == begin) || (NULL == end))
  {
    LOG_MSG_ERROR_0("ps_efs_tokenizer(): "
                    "Input paramters are NULL");
    return PS_EFS_TOKEN_PARSE_FAILURE;
  }
 
  *begin = 0;
  *end   = 0; 
  /*------------------------------------------------------------x------------
     Traversed to end of file => return 
  ---------------------------------------------------------------------------*/
  if( sm->eof ) 
  {
    return PS_EFS_TOKEN_PARSE_EOF;
  }
   
  /*------------------------------------------------------------------------
     Have some bytes to read from the buffer
  ---------------------------------------------------------------------------*/
  while( sm->curr < sm->end_pos ) 
  {

    /*----------------------------------------------------------------------
      Skip over all carriage return characters (\r) added if file was
      editted using a windows machine
    -----------------------------------------------------------------------*/

    if (*sm->curr == '\r')         
    {
      sm->skip_line = FALSE;
      sm->curr++;
      continue;
    }

    /*-----------------------------------------------------------------------
       Lines begining the record with # are comments. Continue to read 
       until we reach the end of file.
    -----------------------------------------------------------------------*/
    if( sm->bol && *sm->curr ==  '#' ) 
    {
      sm->skip_line = TRUE;
      sm->bol = FALSE;
      sm->curr++;
      continue;
    } 

    if( sm->skip_line )                 /* reading a comment */
    {
      if( *sm->curr == '\n' )           /* End of comment */
      {  
        sm->skip_line = FALSE;
        sm->eol = TRUE;
        sm->bol = TRUE;
      }
      sm->curr++;
      continue;                         /*Continue to read until the end of line */
    }
    
    /*--------------------------------------------------------------------------
      Look for the token. If ';' found at the begining then it is 
      an empty token.
      There could be a  case where we hit '\n' while we are looking for a token
      so skip over all the new lines.
    ----------------------------------------------------------------------------*/
    if( *begin == 0 )                   /* Find the beginning of token */
    {                          
      if( *sm->curr == sm->seperator )  /* an empty token */
      {                             

        if( sm->bol == TRUE ) 
        {
          sm->bol = FALSE;
        }
        
        *begin = sm->curr;
        *end   = sm->curr;
        sm->curr++;
        return PS_EFS_TOKEN_PARSE_SUCCESS;
      }

      if( *sm->curr == '\n' )           /* Possibly an empty token */
      {    
        if( sm->eol )                   /* Skip over any successive new lines */
        {     
          sm->curr++;
          continue;
        }
        *begin  = sm->curr;
        *end    = sm->curr;
        sm->eol = TRUE;
        sm->bol = TRUE;
        sm->curr++;
        return PS_EFS_TOKEN_PARSE_SUCCESS;
      }

      /*-------------------------------------------------------------------------
       Before beginning a new token, return an end of record for previous record. 
      --------------------------------------------------------------------------*/
      if( sm->eol ) 
      {                             
        sm->eol = FALSE;
        return PS_EFS_TOKEN_PARSE_EOL;
      }

      *begin = sm->curr;                /* Initialize to beginning of token */
    }
    else if( *sm->curr == sm->seperator || *sm->curr == '\n' )
    {
      *end = sm->curr++;                /* Found end of token */
      
      /*--------------------------------------------------------------------------
         This is a end of line. Save the state and send 
         end of line event when a next token is requested .
      --------------------------------------------------------------------------*/
      if( **end == '\n' ) 
      {       
        sm->eol = TRUE;
        sm->bol = TRUE;
      }
      return PS_EFS_TOKEN_PARSE_SUCCESS;
    }
    
    sm->curr++;
  }/* while */

  /*-------------------------------------------------------------------------- 
    In the middle of token and we ran out characters in the buffer 
  --------------------------------------------------------------------------*/
  if( *begin ) 
  {      
    
    if( *begin != sm->buffer )
    {
      /*---------------------------------------------------------------------- 
        Move the partial token over to beginning of buffer 
      -----------------------------------------------------------------------*/
      /*lint -e732 */
      memscpy( sm->buffer, PS_EFS_READ_BUFFER_SZ*sizeof(char),
               *begin, (sm->curr - *begin) );
      /*lint +e732 */
      sm->curr = sm->buffer + (sm->curr - *begin);
      *begin = sm->buffer;
    }
    else 
    {
      LOG_MSG_INFO1_0("ps_efs_tokenizer(): "
                      "Token is larger than PS_EFS_READ_BUFFER_SZ");
      return PS_EFS_TOKEN_PARSE_FAILURE;
    }
  }
  else 
  {
    /*--------------------------------------------------------------------
      No token or data exists in the buffer 
    ---------------------------------------------------------------------*/
    sm->curr = sm->buffer;
  }
  
  /*----------------------------------------------------------------------
      Read data from the efs file.
  -----------------------------------------------------------------------*/
  {
    /*lint -e732 */
    bytes_read = efs_read( sm->fd, sm->curr, 
                           PS_EFS_READ_BUFFER_SZ - (sm->curr - sm->buffer));
    /*lint +e732 */
    
    if( bytes_read > 0 ) 
    {
      sm->end_pos = sm->curr + bytes_read;
      sm->eof    = FALSE;
      
      if(*begin != 0)
      {
        retval= ps_efs_tokenizer( sm, &dummy, end ); /* Call the function 
                                              again because you could be in the
                                              middle of reading a token */
      }
      else
      {
        retval = ps_efs_tokenizer( sm, begin, end);
      }

      return retval;
    }
    else 
    {

      /*
        No bytes read => reached the end of file.
      */
      if(*begin == 0) 
      {
        sm->eof = 1;
        return PS_EFS_TOKEN_PARSE_EOL;
      }
      else
      {

        /*------------------------------------------------------------------
          If a token was found return the token and 
          when next token is requested send EOF 
        --------------------------------------------------------------------*/
        *end = sm->curr;
        if(bytes_read == 0)
        {
          /*---------------------------------------------------------------
           If the EOF character is missing in the file and the bytes read
           are zero all the time then we are trying to bail out of this.
           
           NOTE: We might have to revisit this later again if different 
           modules
           
          ----------------------------------------------------------------*/
          sm->eof = 1;
          return PS_EFS_TOKEN_PARSE_EOL;
        }
        return PS_EFS_TOKEN_PARSE_SUCCESS;
      }      
    }
  }/* End of bytes read*/
}/* ps_efs_tokenizer() */

/** ===========================================================================
FUNCTION PS_CFG_GET_FILE

DESCRIPTION
  Returns the file descriptor for the nv file path supplied
  
PARAMETERS  
  config_fd       - pointer to file descriptor
  config_path   -  pointer to buffer to that has the path name of efs file
  flag_new       - pointer to flag storing file status
    
DEPENDENCIES 
  None.
  
RETURN VALUE 
  0 Success
  -1 / non zero on failure
  
SIDE EFFECTS 
  None.
===========================================================================*/
static int32 ps_conf_get_file
(
  int32    * config_fd,
  char     * config_path,
  boolean  * flag_new
)
{
  int32 ret_val = 0;
  /*--------------------------------------------------------------------
    Create a file if file does not exist
   --------------------------------------------------------------------*/
  
  *config_fd = efs_open (config_path, O_RDWR);

  if ( 0 > *config_fd )
  {
    if ( ENOENT == efs_errno ) /* Conf file does not exist */
    {
      LOG_MSG_INFO2_0("ps_cfg_get_file(): EFS: Config file not present");
      ret_val = ps_create_efs_config_file(config_path);
      if ( 0 != ret_val )
      {
        LOG_MSG_INFO2_1("ps_cfg_get_file(): EFS: Error creating config file "
                        "result %d", ret_val);
        return ret_val;
      }
      *flag_new = TRUE;

      *config_fd = efs_open (config_path, O_RDWR);
      if ( 0 > *config_fd )
      {
        LOG_MSG_INFO2_1("ps_cfg_get_file(): EFS: Error opening config file, "
                        "config_fd %d", config_fd);
        ret_val = *config_fd;
        return ret_val;
      }
    }
    else /* Could not open conf file for some other reason */
    {
      LOG_MSG_INFO2_0("ps_cfg_get_file(): Error opening config file, ");
      return -1;
    }
  }
  
  return ret_val;
}


/** =========================================================================
FUNCTION PS_EFS_GET_CONF_STRING

DESCRIPTION
  Returns the file descriptor for the nv file path supplied.
  
PARAMETERS  
  ps_cfg_item_info - array of pointers to nv item file paths.
  file_path_buf    -  pointer to buffer to that will store the
  string.

DEPENDENCIES 
  None.
  
RETURN VALUE 
  int32 - number of bytes in config string
  
SIDE EFFECTS 
  None.
===========================================================================*/

static int32 ps_efs_get_conf_string
(
  const char * ps_nv_conf_item_info[],
  char * file_path_buf,
  int ps_efs_conf_items

)
{
  int32 position = 0;
  int32 index;
  uint32 file_path_buf_len = 0;
  /* - - -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  if( file_path_buf == NULL )
  {
    LOG_MSG_ERROR_1 ("ps_efs_get_conf_string() : Failed to allocate memory",
                     	 0);
    return -1;
  }
  
  file_path_buf[0] = '\0';
  file_path_buf_len = ps_efs_conf_items * PS_NV_CONF_MAX_PATH_SIZE;
  /*---------------------------------------------------------------------
    Update data_config_info.conf with the list of nv item files.
  ----------------------------------------------------------------------*/
  /* Browse through ps_cfg_item_info and copy all items in local
     buffer to be written to efs in one shot */
  for (index = 0; index < ps_efs_conf_items; index ++)
  {
    /* Copy current path to buffer and append \n */
    position += snprintf( file_path_buf + position, 
                          file_path_buf_len - position,
                          "%s\n", 
                          ps_nv_conf_item_info[index] );		
    /* Check for truncation */
    if( position >= file_path_buf_len)
    {
      LOG_MSG_ERROR_1 ("Position %d exceeds max length", file_path_buf_len);
      return -1;
    }							
  }

  return position;
}

/** =========================================================================
  FUNCTION PS_EFS_WRITE_ITEMS
  
  DESCRIPTION
    Puts all the required nv item file paths in the passed file.
    
  PARAMETERS  
    data_config_info_fd: data_config_info file descriptor.
      
  DEPENDENCIES 
    None.
    
  RETURN VALUE 
    0 for success.
    -1 for failure.
     
  SIDE EFFECTS 
    None.
    
===========================================================================*/
static int32 ps_efs_write_items
(
  char * data_conf_path,
  const char * ps_nv_conf_item_info[],
  int ps_efs_conf_items,
  int max_path_size
)
{
  int32  conf_fd, conf_info_size = 0;
  fs_ssize_t bytes_read           = 0;
  boolean flag_new_file           = FALSE;
  int efs_ret                     = 0;
  char *file_read_buf             = NULL;
  char *file_path_buf             = NULL;
  int32 ret_val                   = 0;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  
  if (NULL == ps_nv_conf_item_info)
  {
    LOG_MSG_ERROR_0("ps_efs_write_items(): Failed to get ps_cfg_item_info");
    return -1; 
  }
  
  if (NULL == data_conf_path)
  {
    LOG_MSG_ERROR_0("ps_efs_write_items(): Failed to get file path");
    return -1; 
  }

  do {
    /*------------------------------------------------------------
      Get file descriptor for given nv file path
    -------------------------------------------------------------*/
  
    ret_val = ps_conf_get_file( &conf_fd, data_conf_path, &flag_new_file );
  
    if ( 0 != ret_val )
    {
      LOG_MSG_ERROR_1("ps_efs_write_items(): Failed to get file %d", ret_val);
      ret_val = -1;
      break;
    }

    /* Allocate memory for file_read_buf */
    file_path_buf = (char*)ps_system_heap_mem_alloc( ps_efs_conf_items*max_path_size );

    if( file_path_buf == NULL )
    {
      LOG_MSG_ERROR_0("ps_efs_write_items(): Failed to allocate"
                      "file_path_buf memory");
      ret_val = -1;
      break;
    }
  
    /* Get all cfg data to be written */
    conf_info_size = ps_efs_get_conf_string( ps_nv_conf_item_info, 
                                             file_path_buf, 
                                             ps_efs_conf_items);

    if( 0 == conf_info_size )
    {
      LOG_MSG_ERROR_0("ps_efs_write_items(): Failed to read cfg data");
      ret_val = -1;
      break;
    }

    /* Allocate memory for file_read_buf */
    file_read_buf = (char*)ps_system_heap_mem_alloc( conf_info_size );

    if (file_read_buf == NULL )
    {
      LOG_MSG_ERROR_0("ps_efs_write_items(): Failed to allocate "
                      "file_read_buf memory");
      ret_val = -1;
      break;
    }

    /* Initialize buffers */
    memset(file_read_buf, 0, conf_info_size);

    /* Read file if already existed */

    if (FALSE == flag_new_file)
    {
      efs_ret = efs_read(conf_fd, file_read_buf, conf_info_size);
      bytes_read = efs_ret;

      if( efs_ret < 0 )
      {
        LOG_MSG_ERROR_0("ps_efs_write_items(): Error reading file:"  );
      }  
    }

    /* Write to file if file existed and length is different or its a new file */

    if ((FALSE == flag_new_file && (bytes_read != conf_info_size))
        || TRUE == flag_new_file ) 
    {
      /* Write the local buffer in efs memory */
      if( (fs_size_t)conf_info_size != efs_write(conf_fd,
                                                file_path_buf,
                                                (fs_size_t)conf_info_size))
      {
        LOG_MSG_ERROR_1(
"ps_efs_write_items(): efs_write error, bytes: %d", conf_info_size);
          ret_val = -1;
      }
    }    
  } while (0);

  /* Free memory for file_paths_buf */
  PS_SYSTEM_HEAP_MEM_FREE( file_read_buf );
  PS_SYSTEM_HEAP_MEM_FREE( file_path_buf );
  
  /* EFS Close */
  efs_ret = efs_close (conf_fd);
  if ( 0 != efs_ret )
  {
    LOG_MSG_ERROR_1("ps_efs_write_items(): Error closing config file "
                    "result %d", ret_val);
    ret_val = efs_ret;
  }

  return ret_val;
}

/** ========================================================================
  FUNCTION PS_EFS_INIT
  
  DESCRIPTION
    Initialization function for creating NV item file paths in EFS under
    ps_config_info.conf. The function will put all the required 
    ps nv item file paths in efs. The required ps nv item file paths
    are specified in ps_utils_nv_conf_path_array and reference using the 
    indicies from ps_utils_nv_conf_enum_type
    
  PARAMETERS  
    None
      
  DEPENDENCIES 
    None.
    
  RETURN VALUE 
    None
     
  SIDE EFFECTS 
    None.
    
===========================================================================*/
void ps_efs_init
(
  void
)
{
  int ps_efs_conf_items = 0;
   
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/  

  LOG_MSG_INFO2_0("ps_efs_init(): Initializing EFS config item files");

  ps_efs_conf_items = sizeof(ps_nv_conf_efs_path_array ) / sizeof( *ps_nv_conf_efs_path_array );

  if (0 !=  ps_efs_write_items(PS_NV_CONF_EFS_FILE_NAME, 
                               ps_nv_conf_efs_path_array,
                               ps_efs_conf_items,
                               PS_NV_CONF_MAX_PATH_SIZE))

  {
    LOG_MSG_ERROR_0("ps_efs_init(): Item files write failed.");
  }

}
