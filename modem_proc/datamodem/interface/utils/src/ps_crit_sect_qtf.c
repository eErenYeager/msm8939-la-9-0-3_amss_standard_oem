/*==========================================================================*/
/*!
  @file 
  ps_crit_sect.c

  @brief
  This file provides QTF specific critical section implementation.

  Copyright (c) 2009 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary
*/
/*==========================================================================*/
/*===========================================================================
  EDIT HISTORY FOR MODULE

  Please notice that the changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/utils/src/ps_crit_sect_qtf.c#1 $
  $DateTime: 2015/01/27 06:42:19 $$Author: mplp4svc $

  when       who what, where, why
  ---------- --- ------------------------------------------------------------
  2009-12-03 hm  Created module.

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "datamodem_variation.h"
#include "comdef.h"
#include "ps_crit_sect.h"
#include "ps_system_heap.h"
#include "amssassert.h"

#if defined(TEST_FRAMEWORK) && defined(FEATURE_QUBE)
#error code not present
#endif


/*===========================================================================

                          PUBLIC FUNCTION DEFINITIONS

===========================================================================*/
#if defined(TEST_FRAMEWORK) && defined(FEATURE_QUBE)
#error code not present
#endif /* if defined(TEST_FRAMEWORK) && defined(FEATURE_QUBE) */


