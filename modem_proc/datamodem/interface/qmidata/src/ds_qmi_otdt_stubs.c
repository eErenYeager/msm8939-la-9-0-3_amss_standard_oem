/*===========================================================================

                    DS_QMI_OTDT_STUBS.C

DESCRIPTION
  Stubs for DS QMI ON TARGET DATA TEST, in case that this framework is not 
  enabled.

EXTERNALIZED FUNCTIONS

  
Copyright (c) 2012 Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary
===========================================================================*/
/*===========================================================================

EDIT HISTORY FOR FILE

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/qmidata/src/ds_qmi_otdt_stubs.c#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when        who    what, where, why
--------    ---    ----------------------------------------------------------
09/02/12    aa     Created module
===========================================================================*/

/*===========================================================================

                         INCLUDE FILES FOR MODULE

===========================================================================*/
#include "comdef.h"


/*===========================================================================

                    EXTERNAL FUNCTION DEFINITIONS

===========================================================================*/
int ds_qmi_otdt_init
(
  void
)
{
  return 0;
}

