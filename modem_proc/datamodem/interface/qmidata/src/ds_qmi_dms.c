/*===========================================================================

                         D S _ Q M I _ D M S . C

DESCRIPTION

 The Data Services Qualcomm MSM Interface Device Management Services source
 file.

EXTERNALIZED FUNCTIONS

  qmi_dms_init()
    Register the DMS service with QMUX for all applicable QMI links
    

Copyright (c) 2004-2016 QUALCOMM Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary
===========================================================================*/
/*===========================================================================

                      EDIT HISTORY FOR FILE

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/qmidata/src/ds_qmi_dms.c#4 $ $DateTime: 2017/10/16 23:02:26 $ $Author: sammeeku $

when        who    what, where, why
--------    ---    ----------------------------------------------------------
--------    ---    ----------------------------------------------------------
06/19/14    vrk    Added support for SRLTE configuration
05/01/14    vrk    Added support for current device config through policyman
02/24/14    sk     Added new message - DMSI_CMD_GET_ENCRYPTED_DEVICE_SERIAL_NUMBERS.
03/01/14    vrk    Added error code QMI_ERR_DEVICE_NOT_READY to QMI_DMS_SET_OPERATING_MODE_RESP
02/20/14    sah    Added support for tertiary subscription
01/10/14    sah    Modified nv items to report correct phone self ESN and MEID
                   after UIM card is inserted.
01/10/14    vrk    Modified handling of get_ck_state_q
01/08/14    sah    Fix for MSISDN read when additional data is present
                   in extension file.
12/24/13    sah    Using Sysmon API for poweroff when phone unlock attempts
                   trials exceeded.
12/12/13    sah    Added support for get_operator info message
11/26/13    gk     Added support for get_mac message
10/30/13    sah    Added support for set_ap_sw_version message.
10/17/13    gk     In SGLTE mode we should report CS and PS active
08/30/13    gk     Added SGLTE support to simul_voice_and_data_capability TLV
07/16/13    gk     Fixed double prepending of message header
07/09/13    az     Implemented TDSCDMA GET/SET test config support
06/07/13    rk     modified get msisdn message to retrieve proper values 
01/25/13    ua     Poweroff mapping to OFFLINE first is revereted. 
                   Only Poweroff is sent. 
05/21/13    gk     Added support for device_multisim_capability TLV 
05/10/13    ab     Add TTL markers for boot up profiling 
03/18/13    ab     Increased number of dms clients available to 32 
02/05/13    sn     Fixing invalid async command buffer access on client release.
12/11/12    sn     DMS Memory Optimization, moving to new QMI Framework.
10/31/12    svj    Allow hex digits in ICCID numbers 
10/26/12    gk     Reset and Shutdown operating mode changes calls CM API for clients 
                   to handle any mode change processes before actually invoking 
                   Sys Mon API's.
09/10/12    sb     Len checks for QMI TLVs
08/13/12    svj    Add a TLV to indicate device service capability
08/07/12    ab     Add a TLV to indicate simultaneous voice and data capability
08/06/12    gk     Added support for QMI service to log what messages/TLVs  
                   are supported on this branch.
07/18/12    sn     Fix to use Sysmon API for shutdown. For reset, if AT
                   cmd is not forwarded to AP, Sysmon API for reset is used.
06/11/12    sb     Fix to move to correct UIM state in case of CARD ERROR
                   event.
05/31/12    sj     Support SYS_OPRT_MODE_NET_TEST_GW in get 
12/23/11    ua     Route the Operating mode Reset command through 
                   DMS->AT->QMI-AT->Apps Proc. 
04/27/12    sb     Added additional check in qmi_dmsi_set_operating_mode()
04/27/12    sb     Removed feature Wrap FEATURE_DATA_NIKEL
04/20/12    sn     Added TLV for CSFB voice support in device capability msg.
02/28/12    sj     Add support for TDSCDMA 
02/17/12    sb     Fixed Compiler and KW Warnings.
01/25/12    sj     Fix qmi_dmsi_get_hw_supported_radio_if_list and 
                   qmi_dmsi_get_max_channel_rate to use CM mode_capability
                   which is runtime determined
12/13/11    wc     Fix get_msisdn returing NOT_PROVISINOED on multi-mode build
11/02/11    wc     Add qmi_dms_get_current_prl_info
10/10/11    sj     Add support for LTE band capability TLV  
10/04/11    sa     Q6 free floating changes for QMI.
12/09/10    ua     Added new message for Set SPC, and 
                   additional TLV for GET Device Serial Numbers to have the IMEI SVN.
08/03/11    sa     GSDI cleanup changes.
07/14/11    sj     Expose UIM status and PIN1 status to other modules 
06/22/11    wc     Send PRL init indication when PRL is loaded 
05/11/11    sa     Added new message for SW version info.
03/08/11    ua     Retreiving WCDMA/HSDPA/HSUPA default data rates from RRC.
02/21/11    kk     Fix to prevent insignificant bytes in GET_UIM_IMSI.
02/15/11    kk     Added support for PRL_ONLY TLV in PRL_INFO message.
01/10/11    ss     Cleaning up of Globals for thread safety in Q6 Free 
                   Floating environment.
12/17/10    kk     Added log messages before ASSERTs.
12/01/10    ua     Added support for LTE Data rates. 
11/09/10    ua     Extending support of qmi_dmsi_get_msisdn in LTE Mode. 
08/02/10    ack    Added support for setting user time using QMI
06/29/10    kk     Removing bbver.h dependency for BOOT_BLOCK_VERSION.
06/21/10    kk     Fix to align to new MMGSDI APIs for PERSO requests.
06/21/10    kk     Fix to check UIM status before posting commands to MMGSDI.
06/07/10    kk     Fix to update app_type and uim_state from all possible
                   events.
06/07/10    kk     Fix to include HSDPA and HSUPA channel rates in device
                   capabilities.
06/07/10    kk     Fix to prevent accidental memset of get_ck queue.
04/13/10    kk     Fix for GET_UIM_ICCID to report correct error value.
03/18/10    jee    changed FEATURE_MMGSDI to FEATURE_MMGSDI_SESSION_LIB
11/10/09    jee    Changes to support MMGSDI re-architecture
10/16/09    kk     Windows7 compliance - new messages and ATCoP dependencies.
10/09/09    kk     Segregated the internal and externalized functions.
10/08/09    kk     Windows7 compliance - DMS changes.
02/19/09    am     DS Task De-coupling effort and introduction of DCC task.
10/02/06    ks     Fixed PIN1 and PIN2 status TLV types in event_report_ind.
09/26/06    ks     Updated version of DMS to 1.1. Removed pin_id param
                   from pin_status tlvs.
09/06/06    ks     Featurized pin support code, under FEATURE_MMGSDI.
09/05/06    ks     Added support for PIN1/PIN2 operations using QMI.
09/05/06    ks     Added support to handle async responses to DMS.
09/02/06    jd     In production, don't return WLAN in supported radio list
08/23/06    ks     Changes to support multiple qmi/rmnet instances.
07/11/06    ks     Using dsi_get_nv_item() to read from NV. Other clean up.
07/06/06    ks     Removed qmi_dmsi_get_tl(), using qmi_svc_get_tl() instead
06/07/06    ks     Fixed Power state and Battery level reporting
04/05/06    jd     Changed GET_DEVICE_SERIAL_NUMBERS to only return not 
                   provisioned if no serial number is returned.
03/13/06    ks     Changed parsing of requests to return error for fixed 
                   length tlvs when tlv lengh passed is incorrect.
03/13/06    ks     Changed QMI_ERR_INVALID_ARG to QMI_ERR_MALFORMED_MSG.                    
03/06/06    ks     Return QMI_ERR_INVALID_ARG if a malformed TLV is seen.
10/27/05    ks     Removed bat_lvl_lower_limit < BAT_LVL_MIN(0) check as 
                   bat_lvl_lower_limit is unsigned.
09/08/05    ks     Added support for returning imei, MSISDN for UMTS.
08/15/05    ks     Return error when all optional TLVs are absent for messages
                   which don't have any mandatory TLVs.
05/31/05   jd/ks   Code review updates
05/11/05    ks     Code review comments and clean up.
03/14/05    ks     Removed featurization
11/21/04    jd/ks  Created module
===========================================================================*/

/*===========================================================================

                          INCLUDE FILES FOR MODULE

===========================================================================*/

#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"

#include "ttl_map.h"

#include <stdio.h>
#include <string.h>
#include <stringl/stringl.h>

#include "mobile.h"
#include "dsm.h"
#include "nv.h"
#include "msg.h"
#include "err.h"
#include "amssassert.h"

#include "ps_in.h"
#include "ps_byte.h"
#include "dsatctab.h"
#include "dcc_task_defs.h"
#include "dcc_task_svc.h"

#include "ds_qmi_task.h"
#include "ds_qmux.h"
#include "ds_qmi_if.h"
#include "qmi_framework.h"

#include "ds_qmi_dms.h"
#include "ds_qmi_dms_ext.h"
#include "ds_qmi_defs.h"
#include "ds_qmi_svc.h"
#include "ds_qmi_cflog.h"
#include "secapi.h"

#include "qmi_charger.h"

#ifdef FEATURE_MMGSDI_SESSION_LIB
  #include "mmgsdilib.h"
  #include "mmgsdisessionlib.h"
#endif /* FEATURE_MMGSDI_SESSION_LIB */

#include "DDIChipInfo.h"

#include "time.h"
#include "time_genoff.h"

#ifdef FEATURE_PREFERRED_ROAMING
   #include "prl.h"
#endif /* FEATURE_PREFERRED_ROAMING */

#include "qw.h"

#include <stringl/stringl.h>
#include "ds_Utils_DebugMsg.h"
#include "ds_qmi_nv_util.h"
#include "ps_system_heap.h"

#ifdef FEATURE_DATA_QMI_ADDENDUM
#include "gobi_nv.h"
#include "gobi_im_common.h"
#include "gobi_im_amss.h"
#endif /* FEATURE_DATA_QMI_ADDENDUM */
#include "trm.h"
#include "sys_m_reset.h"

#include "qmi_si.h"
#include "qmi_idl_lib.h"
#include "device_management_service_v01.h"
#include "device_management_service_impl_v01.h"

#include "policyman.h"
#include "policyman_msg.h"
#include "dsmsgr.h"

#ifdef FEATURE_TDSCDMA
#include "tds_rrc_ext_if.h"
#endif

/*===========================================================================

                            CONSTANT DEFINITIONS

===========================================================================*/
#define HANDLE_FAILURE()  dsm_free_packet(response); \
                          errval = QMI_ERR_NO_MEMORY; \
                          goto send_result

/*---------------------------------------------------------------------------
  Service management
---------------------------------------------------------------------------*/
#define DMSI_MAX_CLIDS    (32)
#define DMSI_INVALID_INSTANCE    -1

/*---------------------------------------------------------------------------
  Major and Minor Version Nos for DMS
---------------------------------------------------------------------------*/
#define DMSI_BASE_VER_MAJOR    (1)
#define DMSI_BASE_VER_MINOR    (14)

#define DMSI_ADDENDUM_VER_MAJOR  (0)
#define DMSI_ADDENDUM_VER_MINOR  (0)
/*---------------------------------------------------------------------------
  Device Serial Numbers
---------------------------------------------------------------------------*/
#define DMSI_PRM_TYPE_ESN   (0x10)
#define DMSI_PRM_TYPE_IMEI  (0x11)
#define DMSI_PRM_TYPE_MEID  (0x12)
#define DMSI_PRM_TYPE_IMEISV_SVN (0x13)

/*---------------------------------------------------------------------------
  Get cdma lock mode TLV definitions
---------------------------------------------------------------------------*/
#define DMSI_PRM_TYPE_GET_CDMA_LOCK_MODE      (0x10)

/*---------------------------------------------------------------------------
  bind_subscription TLV definitions
---------------------------------------------------------------------------*/
#define DMSI_PRM_TYPE_BIND_SUBSCRIPTION (0x10)

/*---------------------------------------------------------------------------
  Get MSISDN TLV definitions
---------------------------------------------------------------------------*/
#define DMSI_PRM_TYPE_MIN   (0x10)
#define DMSI_PRM_TYPE_UIM_IMSI (0x11)

#define DMSI_MODEL_ID_SIZ   (256)
#define DMSI_REV_ID_SIZ     (256)
#define DMSI_SERIAL_NO_SIZ  (256)
#define DMSI_HW_REV_ID_SIZ  (256)

#define DMSI_BASE_DECIMAL   (10)
#define DMSI_BASE_HEX       (16)
#define DMSI_UIM_PIN1       (1)
#define DMSI_UIM_PIN2       (2)
#define DMSI_MAX_PINS       (2)

/*---------------------------------------------------------------------------
  Automatic Activation Info
---------------------------------------------------------------------------*/
#define DMSI_ACT_AUTO_CODE_SIZ (CM_MAX_NUMBER_CHARS)

/*---------------------------------------------------------------------------
 subscription  configuration list max length
---------------------------------------------------------------------------*/
#define DMSI_MAX_CONFIG_LIST_LEN   32

/*---------------------------------------------------------------------------
 subscription max length
---------------------------------------------------------------------------*/
#define DMSI_MAX_SUBS_LIST_LEN   6

/*---------------------------------------------------------------------------
  Manual Activation Info
---------------------------------------------------------------------------*/
/* ID reserved for single-request PRL TLV (not size limited)  */
//#define DMSI_PRM_TYPE_PRL   (0x10)
#define DMSI_PRM_TYPE_MN_HA (0x11)
#define DMSI_PRM_TYPE_MN_AAA (0x12)
#define DMSI_PRM_TYPE_PRL   (0x13)

#define DMSI_SPC_SIZ        (6)
#define DMSI_NUMB_PCS_SIZ   (22)
#define DMSI_MN_KEY_SIZ     (16)
#define DMSI_MIN_SIZ        (10)

#define QMI_DMS_POLICYMAN_MAX_SUBS (3)

#define QMI_DMS_DEVICE_CAP_INDICATION (0x1)
#define QMI_DMS_DEVICE_CAP_RESP (0x2)

/*---------------------------------------------------------------------------
  DMS segment length (3k)
---------------------------------------------------------------------------*/
#define DMSI_TLV_SEGMENT_LEN_MAX (1536)

/*---------------------------------------------------------------------------
  Constants for IMSI
---------------------------------------------------------------------------*/
#if defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900)
#define DMSI_IMSI_MCC_0 999        /* 000 */
#define DMSI_IMSI_S1_0  16378855   /* 0000000 */
#define DMSI_IMSI_S2_0  999        /* 000 */
#define DMSI_IMSI_CLASS0_ADDR_NUM 0xFF
#define DMSI_IMSI_11_12_0 99
#define DMSI_FMMIN 0
#define DMSI_CDMAMIN 1
#endif /* defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900) */

/*---------------------------------------------------------------------------
  Event report TLV definitions
---------------------------------------------------------------------------*/
#define DMSI_PRM_TYPE_POWER_STATE         (0x10)
#define DMSI_PRM_TYPE_BAT_LVL_RPT_LIMITS  (0x11)
#define DMSI_PRM_TYPE_PIN_STATUS          (0x12)
#define DMSI_PRM_TYPE_ACTIVATION_STATE    (0x13)
#define DMSI_PRM_TYPE_OPRT_MODE           (0x14)
#define DMSI_PRM_TYPE_UIM_GET_STATE       (0x15)
#define DMSI_PRM_TYPE_WD_STATE            (0x16)
#define DMSI_PRM_TYPE_PRL_INIT            (0x17)
#define DMSI_PRM_TYPE_CDMA_LOCK_MODE      (0x18)
#define DMSI_PRM_TYPE_DEVICE_MODE         (0x19)

#define DMSI_PRM_TYPE_IND_CURRENT_DEVICE_CAPABILITY        (0x1A)
#define DMSI_PRM_TYPE_IND_CURRENT_MSIM_SUBS_CAPABILITY     (0x1B)
#define DMSI_PRM_TYPE_IND_VOICE_DATA_SUBS_CAPABILITY       (0x1C)
#define DMSI_PRM_TYPE_IND_MAX_DATA_SUBS                    (0x1D)

/*---------------------------------------------------------------------------
  Get Pin Status TLV definitions
---------------------------------------------------------------------------*/
#define DMSI_PRM_TYPE_PIN1_STATUS         (0x11)
#define DMSI_PRM_TYPE_PIN2_STATUS         (0x12)

#define DMSI_PRM_TYPE_RETRIES_LEFT        (0x10)

#define DMSI_BAT_LVL_MIN               (0)
#define DMSI_BAT_LVL_MAX               (100)
#define DMSI_BAT_LVL_INACTIVE          (255)
#define DMSI_PWR_STATE_INACTIVE        (0)
#define DMSI_EVENT_MASK_POWER_STATE    (0x01)
#define DMSI_EVENT_MASK_BATTERY_LVL    (0x02)

/*---------------------------------------------------------------------------
  Get/Set Time TLV definitions
---------------------------------------------------------------------------*/
#define DMSI_PRM_TYPE_SYS_TIME      (0x10)
#define DMSI_PRM_TYPE_USR_TIME      (0x11)
#define DMSI_PRM_TYPE_TIME_REF_TYPE (0x10)

/*---------------------------------------------------------------------------
  SIM Lock TLV definitions
---------------------------------------------------------------------------*/
#define DMSI_PRM_TYPE_BLOCKING_OPERATION (0x10)
#define DMSI_PRM_TYPE_CK_RETRY_STATUS    (0x10)
#define DMSI_GET_CK_STATE_BLOCKED        (0x2)

/*---------------------------------------------------------------------------
  LTE Band capability TLV definition
---------------------------------------------------------------------------*/
#define DMSI_PRM_TYPE_LTE_BAND_CAP       (0x10)

/*---------------------------------------------------------------------------
  TDSCDMA Band capability TLV definition
---------------------------------------------------------------------------*/
#define DMSI_PRM_TYPE_TDS_BAND_CAP       (0x11)

/*---------------------------------------------------------------------------
  UIM ICCID length
---------------------------------------------------------------------------*/
#define DMSI_MMGSDI_ICCID_LEN (10)

/*---------------------------------------------------------------------------
  User Lock Code length
---------------------------------------------------------------------------*/
#define DMSI_USER_LOCK_SIZ NV_LOCK_CODE_SIZE

/*---------------------------------------------------------------------------
  UIM IMSI length
---------------------------------------------------------------------------*/
#define DMSI_MMGSDI_IMSI_LEN (15)

/*---------------------------------------------------------------------------
  UIM MSISDN length
---------------------------------------------------------------------------*/
#define DMSI_MMGSDI_MSISDN_LEN (20)

/*---------------------------------------------------------------------------
  New Service Programming Code 
---------------------------------------------------------------------------*/
#define DMSI_PRM_TYPE_REQUIRED_PRM_NEW_SPC   (0x2)

/*---------------------------------------------------------------------------
  User Data file length
---------------------------------------------------------------------------*/
#define DMSI_USER_DATA_FILE_MAX_LEN (1024)

/*---------------------------------------------------------------------------
  ERI (Extended Roaming Indicator) file length
---------------------------------------------------------------------------*/
#define DMSI_ERI_FILE_MAX_LEN (512)

/*---------------------------------------------------------------------------
  Invalid PRL version for RUIM
---------------------------------------------------------------------------*/
#define DMSI_PRL_VERSION_INVALID (0xFFFF)

/*---------------------------------------------------------------------------
  UIM maximum Control Key length
  As per 3GPP spec(TS 22.022) The control keys shall be decimal strings 
  with an appropriate number of digits for the level of personalisation. 
  PCK (SIM category) should be at least 6 digits, and the remaining control 
  keys at least 8 digits in length. The maximum length for any control 
  key is 16 digits
---------------------------------------------------------------------------*/
#define DMSI_MMGSDI_CK_LEN_MAX (16)

#ifdef FEATURE_DATA_QMI_ADDENDUM
/*---------------------------------------------------------------------------
  Get Revision ID TLV definitions
---------------------------------------------------------------------------*/
#define DMSI_PRM_TYPE_BOOT_VER  (0x10)
#define DMSI_PRM_TYPE_PRI_REV   (0x11)

/*---------------------------------------------------------------------------
  HSU Alternate NET Config Definitions
---------------------------------------------------------------------------*/
#define DMSI_ALT_NET_CONFIG_DISABLED 0x00
#define DMSI_ALT_NET_CONFIG_ENABLED  0x01

/*---------------------------------------------------------------------------
  Get Operation Mode TLV definitions
---------------------------------------------------------------------------*/
#define DMSI_PRM_TYPE_OFFLINE_REASON    (0x10)
#define DMSI_PRM_TYPE_HW_RESTRICTED     (0x11)

/*---------------------------------------------------------------------------
  UQCN version masks used for QMI_DMS_RESTORE_FACTORY_DEFAULTS
---------------------------------------------------------------------------*/
#define DMSI_RESTORE_FACT_VALUE   0xFACD0000 
#define DMSI_RESTORE_FACT_VALUE   0xFACD0000 

/*---------------------------------------------------------------------------
  GOBI Image Definitions
---------------------------------------------------------------------------*/
/* Set firmware preference */
#define DMSI_PRM_TYPE_DOWNLOAD_OVERRIDE   (0x10)
#define DMSI_PRM_TYPE_MODEM_STORAGE_INDEX (0x11)

#define DMSI_PRM_TYPE_MAX_BUILD_ID_LEN    (0x10)

/* Shared */
#define DMSI_IMG_TYPE_MODEM (0)
#define DMSI_IMG_TYPE_PRI   (1)

#define DMSI_NUM_SUPPORTED_IMAGE_TYPES   (2)

#define DMSI_RUNNING_IMAGE_INDEX_UNKNOWN (0xFF)
#define DMSI_STORAGE_INDEX_NOT_USED      (0xFF)
#define DMSI_FAILURE_COUNT_NOT_USED      (0xFF)

#define FEATURE_QMI_GOBI_IMG_TBL_DEBUG

/* Get stored build info */
#define DMSI_PRM_TYPE_BOOT_VER    (0x10)
#define DMSI_PRM_TYPE_PRI_VER     (0x11)
#define DMSI_PRM_TYPE_OEM_LOCK_ID (0x12)

#define DMSI_PRI_VER_INFO_LEN (32)

/* Get boot image download mode */
#define DMSI_PRM_TYPE_DOWNLOAD_MODE (0x10)
#endif /* FEATURE_DATA_QMI_ADDENDUM */

#define DMSI_PRM_TYPE_PRL_ONLY (0x10)

#define DMSI_PRM_TYPE_PRL_INFO_PRL_VER  (0x10)
#define DMSI_PRM_TYPE_PRL_INFO_PRL_ONLY (0x11)

/*---------------------------------------------------------------------------
  Get Device Capability TLV definitions
---------------------------------------------------------------------------*/
#define DMSI_PRM_TYPE_DEVICE_SERVICE_CAPABILITY (0x10)
#define DMSI_PRM_TYPE_DEVICE_CAP_VOICE_SUPPORT  (0x11)
#define DMSI_PRM_TYPE_DEVICE_CAP_SIMUL_VOICE_AND_DATA  (0x12)
#define DMSI_PRM_TYPE_MULTI_SIM_CAPABILITY             (0x13)
#define DMSI_PRM_TYPE_CURRENT_DEVICE_CAPABILITY        (0x14)
#define DMSI_PRM_TYPE_CURRENT_MSIM_SUBS_CAPABILITY     (0x15)
#define DMSI_PRM_TYPE_VOICE_DATA_SUBS_CAPABILITY       (0x16)
#define DMSI_PRM_TYPE_SUBS_DEVICE_FEATURE_MODE         (0x17)
#define DMSI_PRM_TYPE_MAX_DATA_SUBS                    (0x18)

/*---------------------------------------------------------------------------
  Macro used in command handlers (common)
---------------------------------------------------------------------------*/
#define CHECK_RETVAL()  if (FALSE == retval) { dsm_free_packet(&response); \
                                               return NULL; }

/*---------------------------------------------------------------------------
  Macro used to determine target support
---------------------------------------------------------------------------*/
#define DMSI_TARGET_SUPPORTS_CDMA(mode_capability)  (mode_capability & SYS_SYS_MODE_MASK_CDMA )
#define DMSI_TARGET_SUPPORTS_HDR(mode_capability)  (mode_capability & SYS_SYS_MODE_MASK_HDR )
#define DMSI_TARGET_SUPPORTS_GSM(mode_capability)  (mode_capability & SYS_SYS_MODE_MASK_GSM )
#define DMSI_TARGET_SUPPORTS_WCDMA(mode_capability)  (mode_capability & SYS_SYS_MODE_MASK_WCDMA )
#define DMSI_TARGET_SUPPORTS_LTE(mode_capability)  (mode_capability & SYS_SYS_MODE_MASK_LTE )
#define DMSI_TARGET_SUPPORTS_TDSCDMA(mode_capability)  (mode_capability & SYS_SYS_MODE_MASK_TDS )


/* invalid SPC used to allow certain service provisioning (defined by carrier)
   without knowing the actual SPC*/
#define SPECIAL_INVALID_SPC "999999"

/*---------------------------------------------------------------------------
  Message-internal TLV length field values
---------------------------------------------------------------------------*/
#define DMSI_PRM_TYPE_OPRT_MODE_TYPE_LEN              (0x01)

#define DMSI_SW_VER_LEN               (32)

/*---------------------------------------------------------------------------
  Defines required for TDSCDMA test config QMI messages
---------------------------------------------------------------------------*/
#define DMSI_PRM_TYPE_TDSCDMA_CONFIG             (0x10)
#define DMSI_PRM_TYPE_GET_TDSCDMA_ACTIVE_CONFIG  (0x10)
#define DMSI_PRM_TYPE_GET_TDSCDMA_DESIRED_CONFIG (0x11)

/*---------------------------------------------------------------------------
  operator version TLV definition
---------------------------------------------------------------------------*/
#define DMSI_PRM_TYPE_OPERATOR                   (0x10)
/*---------------------------------------------------------------------------
  get_mac_address TLV definition
---------------------------------------------------------------------------*/
#define DMSI_PRM_TYPE_MAC_ADDR                   (0x10)

/*---------------------------------------------------------------------------
  mac_address buffer length
---------------------------------------------------------------------------*/
#define DMSI_MAC_ADDR_LEN                        6

/*---------------------------------------------------------------------------
  get sw version max length
---------------------------------------------------------------------------*/
#define DMSI_SW_VER_MAX_LEN                      32

/*===========================================================================

                                DATA TYPES

===========================================================================*/

/*---------------------------------------------------------------------------
  device cap enums
---------------------------------------------------------------------------*/
typedef enum
{
  DMSI_DATA_ONLY      = 0x01, 
  DMSI_VOICE_ONLY     = 0x02,
  DMSI_SIM_VOICE_DATA = 0x03,
  DMSI_VOICE_DATA     = 0x04  // voice or data (non-simultaneous)
} qmi_dmsi_voice_cap_e_type;

typedef enum
{
  DMSI_SIM_NOT_REQ  = 0x01,
  DMSI_SIM_SUPPORT  = 0x02
} qmi_dmsi_sim_support_e_type;

typedef enum
{
  DMSI_MASK_VOICE_SUPPORT_NONE               = 0x0000,  
  DMSI_MASK_VOICE_SUPPORT_GW_CSFB_CAPABLE    = 0x0001,
  DMSI_MASK_VOICE_SUPPORT_1x_CSFB_CAPABLE    = 0x0002,
  DMSI_MASK_VOICE_SUPPORT_VOLTE_CAPABLE      = 0x0004
} qmi_dmsi_voice_support_cap_e_type;

typedef enum
{
  DMSI_MASK_SIMUL_VOICE_AND_DATA_CAP_NONE         = 0x0000,  
  DMSI_MASK_SIMUL_VOICE_AND_DATA_SVLTE_CAPABLE    = 0x0001,
  DMSI_MASK_SIMUL_VOICE_AND_DATA_SVDO_CAPABLE     = 0x0002,
  DMSI_MASK_SIMUL_VOICE_AND_DATA_SGLTE_CAPABLE    = 0x0004
} qmi_dmsi_simul_voice_and_data_cap_e_type;

typedef enum
{
  DMSI_OPRT_MODE_ONLINE   = 0x00,
  DMSI_OPRT_MODE_LPM      = 0x01,
  DMSI_OPRT_MODE_FTM      = 0x02,
  DMSI_OPRT_MODE_OFFLINE  = 0x03,
  DMSI_OPRT_MODE_RESET    = 0x04,
  DMSI_OPRT_MODE_PWROFF   = 0x05,
  DMSI_OPRT_MODE_PLPM     = 0x06,
  DMSI_OPRT_MODE_MOLPM    = 0x07, // Mode only LPM
  DMSI_OPRT_MODE_NET_TEST_GW = 0x08 //Network test for GSM/WCDMA
} qmi_dmsi_oprt_mode_e_type;

/*---------------------------------------------------------------------------
  Set Event Report event bitmask
---------------------------------------------------------------------------*/
typedef enum
{
  DMSI_REPORT_STATUS_EV_POWER_STATE     = 0x01,
  DMSI_REPORT_STATUS_EV_PIN_STATUS      = 0x02,
  DMSI_REPORT_STATUS_EV_ACTIVATION_STATE= 0x04,
  DMSI_REPORT_STATUS_EV_OPRT_MODE       = 0x08,
  DMSI_REPORT_STATUS_EV_UIM_GET_STATE   = 0x10,
  DMSI_REPORT_STATUS_EV_WD_STATE        = 0x20,
  DMSI_REPORT_STATUS_EV_PRL_INIT        = 0x40,
  DMSI_REPORT_STATUS_EV_CDMA_LOCK_MODE  = 0x80,
  DMSI_REPORT_STATUS_EV_DEV_MSIM_CAP    = 0x100
} qmi_dmsi_report_status_ev_e_type;


/*---------------------------------------------------------------------------
  activation state enum
---------------------------------------------------------------------------*/
typedef enum
{
  DMSI_ACTIVATION_STATE_NOT_ACTIVATED = 0x00,
  DMSI_ACTIVATION_STATE_ACTIVATED     = 0x01,

  /* Call Event values */
  DMSI_ACTIVATION_STATE_CONNECTING    = 0x02,
  DMSI_ACTIVATION_STATE_CONNECTED     = 0x03,
  
  /* OTASP_API values */
  DMSI_ACTIVATION_STATE_AUTHENTICATED = 0x04,
  DMSI_ACTIVATION_STATE_NAM_DLOADED   = 0x05,
  DMSI_ACTIVATION_STATE_MDN_DLOADED   = 0x06,
  DMSI_ACTIVATION_STATE_IMSI_DLOADED  = 0x07,
  DMSI_ACTIVATION_STATE_PRL_DLOADED   = 0x08,
  DMSI_ACTIVATION_STATE_SPC_DLOADED   = 0x09,
  DMSI_ACTIVATION_STATE_COMMIT        = 0x0A,
  
  DMSI_ACTIVATION_STATE_MAX,
  DMSI_ACTIVATION_STATE_MAX32         = 0x10000000
} qmi_dmsi_activation_state_e_type;

/*---------------------------------------------------------------------------
  DMS mmgsdi perso featurs state
---------------------------------------------------------------------------*/
typedef enum
{
  DMSI_MMGSDI_PERSO_STATE_MIN         = -1,
  DMSI_MMGSDI_PERSO_STATE_DEACTIVATED,
  DMSI_MMGSDI_PERSO_STATE_ACTIVATED,
  DMSI_MMGSDI_PERSO_STATE_BLOCKED,
  DMSI_MMGSDI_PERSO_STATE_MAX
} qmi_dmsi_mmgsdi_perso_feature_state;

/*---------------------------------------------------------------------------
  DMS User Lock state enum
---------------------------------------------------------------------------*/
typedef enum
{
  DMSI_USER_LOCK_MIN      = -1,
  DMSI_USER_LOCK_UNLOCKED = 0x00,
  DMSI_USER_LOCK_LOCKED   = 0x01,
  DMSI_USER_LOCK_MAX,
  DMSI_USER_LOCK_MAX32    = 0x10000000
} qmi_dmsi_user_lock_state;

/*---------------------------------------------------------------------------
  DMS device_cap multisim rat_mask enum
---------------------------------------------------------------------------*/
typedef enum
{
  DMSI_SUBS_CAPABILITY_NONE      = 0x0000,
  DMSI_SUBS_CAPABILITY_AMPS      = 0x0001,
  DMSI_SUBS_CAPABILITY_CDMA      = 0x0002,
  DMSI_SUBS_CAPABILITY_HDR       = 0x0004,
  DMSI_SUBS_CAPABILITY_GSM       = 0x0008,
  DMSI_SUBS_CAPABILITY_WCDMA     = 0x0010,
  DMSI_SUBS_CAPABILITY_LTE       = 0x0020,
  DMSI_SUBS_CAPABILITY_TDS       = 0x0040  
}qmi_dmsi_subs_capability_mask_type;

typedef enum
{
  DMS_SUBS_VOICE_DATA_CAPABILITY_NORMAL = 0x01,
  DMS_SUBS_VOICE_DATA_CAPABILITY_SGLTE,
  DMS_SUBS_VOICE_DATA_CAPABILITY_CSFB,
  DMS_SUBS_VOICE_DATA_CAPABILITY_SVLTE,
  DMS_SUBS_VOICE_DATA_CAPABILITY_SRLTE
} qmi_dmsi_subs_voice_data_capability_type;

typedef enum {
  DMS_SUBS_FEATURE_MODE_NORMAL,     
  DMS_SUBS_FEATURE_MODE_SGLTE,
  DMS_SUBS_FEATURE_MODE_SVLTE,
  DMS_SUBS_FEATURE_MODE_SRLTE
} qmi_dmsi_subs_device_feature_mode_type;

/*---------------------------------------------------------------------------
  DMS Command enum type - not equal to the actual command values!
  mapping is in qmi_dms_cmd_callbacks table

  DO NOT REORDER THIS ENUM!
---------------------------------------------------------------------------*/
typedef enum
{
  DMSI_CMD_MIN                          = 0,
  DMSI_CMD_RESET                        = DMSI_CMD_MIN,
  DMSI_CMD_SET_EVENT_REPORT,
  DMSI_CMD_GET_DEVICE_CAP,
  DMSI_CMD_GET_DEVICE_MFR,
  DMSI_CMD_GET_DEVICE_MODEL_ID,
  DMSI_CMD_GET_DEVICE_REV_ID,
  DMSI_CMD_GET_MSISDN,
  DMSI_CMD_GET_DEVICE_SERIAL_NUMBERS,
  DMSI_CMD_GET_POWER_STATE,
  DMSI_CMD_SET_UIM_PIN_PROTECTION,
  DMSI_CMD_VERIFY_UIM_PIN,
  DMSI_CMD_UNBLOCK_UIM_PIN,
  DMSI_CMD_CHANGE_UIM_PIN,
  DMSI_CMD_GET_UIM_PIN_STATUS,
  DMSI_CMD_GET_DEVICE_HARDWARE_REV,
  DMSI_CMD_GET_OPERATING_MODE,
  DMSI_CMD_SET_OPERATING_MODE,
  DMSI_CMD_GET_TIME,
  DMSI_CMD_GET_PRL_VER,
  DMSI_CMD_GET_ACTIVATED_STATE,
  DMSI_CMD_ACTIVATE_AUTOMATIC,
  DMSI_CMD_ACTIVATE_MANUAL,
  DMSI_CMD_GET_USER_LOCK_STATE,
  DMSI_CMD_SET_USER_LOCK_STATE,
  DMSI_CMD_SET_USER_LOCK_CODE,
  DMSI_CMD_READ_USER_DATA,
  DMSI_CMD_WRITE_USER_DATA,
  DMSI_CMD_READ_ERI_FILE,
  DMSI_CMD_RESTORE_FACTORY_DEFAULTS,
  DMSI_CMD_VALIDATE_SPC,
  DMSI_CMD_GET_UIM_ICCID,
#ifdef FEATURE_DATA_QMI_ADDENDUM
  DMSI_CMD_GET_HOST_LOCK,
#endif /* FEATURE_DATA_QMI_ADDENDUM */
  DMSI_CMD_UIM_GET_CK_STATUS,
  DMSI_CMD_UIM_SET_CK_PROTECTION,
  DMSI_CMD_UIM_UNBLOCK_CK,
  DMSI_CMD_GET_UIM_IMSI,
  DMSI_CMD_UIM_GET_STATE,
  DMSI_CMD_GET_BAND_CAPABILITY,
  DMSI_CMD_GET_FACTORY_SKU,
  DMSI_CMD_SET_TIME,
  DMSI_CMD_GET_ALT_NET_CONFIG,
  DMSI_CMD_SET_ALT_NET_CONFIG,
#ifdef FEATURE_DATA_QMI_ADDENDUM
  DMSI_CMD_GET_FIRMWARE_PREF,
  DMSI_CMD_SET_FIRMWARE_PREF,
  DMSI_CMD_LIST_STORED_IMAGES,
  DMSI_CMD_DELETE_STORED_IMAGE,
  DMSI_CMD_GET_STORED_IMAGE_INFO,
  DMSI_CMD_GET_BOOT_IMAGE_DOWNLOAD_MODE,
  DMSI_CMD_SET_BOOT_IMAGE_DOWNLOAD_MODE,
#endif /* FEATURE_DATA_QMI_ADDENDUM */
  DMSI_CMD_GET_SW_VERSION,
  DMSI_CMD_SET_SPC, 
  DMSI_CMD_GET_CURRENT_PRL_INFO,
  DMSI_CMD_BIND_SUBSCRIPTION,
  DMSI_CMD_GET_BIND_SUBSCRIPTION, 
  DMSI_CMD_SET_AP_SW_VERSION, 
  DMSI_CMD_GET_CDMA_LOCK_MODE,
  DMSI_CMD_SET_TEST_CONFIG,
  DMSI_CMD_GET_TEST_CONFIG,
  DMSI_CMD_GET_OEM_CHINA_OPERATOR,
  DMSI_CMD_GET_MAC_ADDRESS,
  DMSI_CMD_GET_ENCRYPTED_DEVICE_SERIAL_NUMBERS,
  DMSI_CMD_MAX, 
  DMSI_CMD_WIDTH                    = 0xFFFF                        
} qmi_dmsi_cmd_e_type;

typedef enum
{
  DMSI_CMD_VAL_RESET                      = 0x0000,
  DMSI_CMD_VAL_SET_EVENT_REPORT           = 0x0001,
  DMSI_CMD_VAL_GET_DEVICE_CAP             = 0x0020,
  DMSI_CMD_VAL_GET_DEVICE_MFR             = 0x0021,
  DMSI_CMD_VAL_GET_DEVICE_MODEL_ID        = 0x0022,
  DMSI_CMD_VAL_GET_DEVICE_REV_ID          = 0x0023,
  DMSI_CMD_VAL_GET_MSISDN                 = 0x0024,
  DMSI_CMD_VAL_GET_DEVICE_SERIAL_NUMBERS  = 0x0025,
  DMSI_CMD_VAL_GET_POWER_STATE            = 0x0026,
  DMSI_CMD_VAL_SET_UIM_PIN_PROTECTION     = 0x0027,
  DMSI_CMD_VAL_VERIFY_UIM_PIN             = 0x0028,
  DMSI_CMD_VAL_UNBLOCK_UIM_PIN            = 0x0029,
  DMSI_CMD_VAL_CHANGE_UIM_PIN             = 0x002A,
  DMSI_CMD_VAL_GET_UIM_PIN_STATUS         = 0x002B,
  DMSI_CMD_VAL_GET_DEVICE_HARDWARE_REV    = 0x002C,
  DMSI_CMD_VAL_GET_OPERATING_MODE         = 0x002D,
  DMSI_CMD_VAL_SET_OPERATING_MODE         = 0x002E,
  DMSI_CMD_VAL_GET_TIME                   = 0x002F,
  DMSI_CMD_VAL_GET_PRL_VER                = 0x0030,
  DMSI_CMD_VAL_GET_ACTIVATED_STATE        = 0x0031,
  DMSI_CMD_VAL_ACTIVATE_AUTOMATIC         = 0x0032,
  DMSI_CMD_VAL_ACTIVATE_MANUAL            = 0x0033,
  DMSI_CMD_VAL_GET_USER_LOCK_STATE        = 0x0034,
  DMSI_CMD_VAL_SET_USER_LOCK_STATE        = 0x0035,
  DMSI_CMD_VAL_SET_USER_LOCK_CODE         = 0x0036,
  DMSI_CMD_VAL_READ_USER_DATA             = 0x0037,
  DMSI_CMD_VAL_WRITE_USER_DATA            = 0x0038,
  DMSI_CMD_VAL_READ_ERI_FILE              = 0x0039,
  DMSI_CMD_VAL_RESTORE_FACTORY_DEFAULTS   = 0x003A,
  DMSI_CMD_VAL_VALIDATE_SPC               = 0x003B,
  DMSI_CMD_VAL_GET_UIM_ICCID              = 0x003C,
    /* Reserved 0x003D - 0x003F message IDs */
#ifdef FEATURE_DATA_QMI_ADDENDUM
  DMSI_CMD_VAL_GET_HOST_LOCK              = 0x003F,
#endif /* FEATURE_DATA_QMI_ADDENDUM */
  DMSI_CMD_VAL_UIM_GET_CK_STATUS          = 0x0040,
  DMSI_CMD_VAL_UIM_SET_CK_PROTECTION      = 0x0041,
  DMSI_CMD_VAL_UIM_UNBLOCK_CK             = 0x0042,
  DMSI_CMD_VAL_GET_UIM_IMSI               = 0x0043,
  DMSI_CMD_VAL_UIM_GET_STATE              = 0x0044,
  DMSI_CMD_VAL_GET_BAND_CAPABILITY        = 0x0045,
  DMSI_CMD_VAL_GET_FACTORY_SKU            = 0x0046,
    /* Reserved 0x0047 - 0x004A message IDs */
#ifdef FEATURE_DATA_QMI_ADDENDUM
  DMSI_CMD_VAL_GET_FIRMWARE_PREF          = 0x0047,
  DMSI_CMD_VAL_SET_FIRMWARE_PREF          = 0x0048,
  DMSI_CMD_VAL_LIST_STORED_IMAGES         = 0x0049,
  DMSI_CMD_VAL_DELETE_STORED_IMAGE        = 0x004A,
#endif /* FEATURE_DATA_QMI_ADDENDUM */
  DMSI_CMD_VAL_SET_TIME                   = 0x004B,
    /* Reserved 0x004C message ID */
#ifdef FEATURE_DATA_QMI_ADDENDUM
  DMSI_CMD_VAL_GET_STORED_IMAGE_INFO      = 0x004C,
#endif /* FEATURE_DATA_QMI_ADDENDUM */
  DMSI_CMD_VAL_GET_ALT_NET_CONFIG         = 0x004D,
  DMSI_CMD_VAL_SET_ALT_NET_CONFIG         = 0x004E,
    /* Reserved 0x004F - 0x0050 message IDs */
#ifdef FEATURE_DATA_QMI_ADDENDUM
  DMSI_CMD_VAL_GET_BOOT_IMAGE_DOWNLOAD_MODE = 0x004F,
  DMSI_CMD_VAL_SET_BOOT_IMAGE_DOWNLOAD_MODE = 0x0050,
#endif /* FEATURE_DATA_QMI_ADDENDUM */
  /* Message for NV reads and writes */
  DMSI_CMD_VAL_GET_SW_VERSION               = 0x0051,
  DMSI_CMD_VAL_SET_SPC                    = 0x0052,
  DMSI_CMD_VAL_GET_CURRENT_PRL_INFO       = 0x0053,
  DMSI_CMD_VAL_BIND_SUBSCRIPTION          = 0x0054,
  DMSI_CMD_VAL_GET_BIND_SUBSCRIPTION      = 0x0055,
  DMSI_CMD_VAL_SET_AP_SW_VERSION          = 0x0056,
  DMSI_CMD_VAL_GET_CDMA_LOCK_MODE         = 0x0057,
  DMSI_CMD_VAL_SET_TEST_CONFIG            = 0x0058,
  DMSI_CMD_VAL_GET_TEST_CONFIG            = 0x0059,
  DMSI_CMD_VAL_GET_OEM_CHINA_OPERATOR     = 0x005B,
  DMSI_CMD_VAL_GET_MAC_ADDRESS            = 0x005C,
  DMSI_CMD_VAL_GET_ENCRYPTED_DEVICE_SERIAL_NUMBERS = 0x005D,
/* QC EXTERNAL QMI COMMAND RANGE IS 0x0000 - 0x5555. 
   Add the next external QMI Command here */

/* VENDOR SPECIFIC QMI COMMAND RANGE IS 0x5556 - 0xAAAA.
   IMPORTANT!
   Add the vendor specific QMI Commands within this range only to avoid 
   conflicts with QC QMI commands that would get released in future */

/* RESERVED QC QMI COMMAND RANGE IS 0xAAAB - 0xFFFE 
   Internal QMI Commands must be added in DECREASING ORDER from Below */

  DMSI_CMD_VAL_WIDTH                      = 0xFFFF
} qmi_dmsi_cmd_val_e_type;


#ifdef FEATURE_TDSCDMA
typedef struct
{
  boolean status;
} tdscdma_set_config_res_type;

typedef struct
{
  tds_rrc_config_e_type  active_config;
  tds_rrc_config_e_type  desired_config;
} tdscdma_get_config_res_type;
#endif

typedef struct
{
  uint16 cmd_id;
  union
  {
    struct
    {
      uint16    num_instances;
    } init_cb;

    struct
    {
      qmi_framework_common_msg_hdr_type    common_msg_hdr;
    } clid_cb;

    struct
    {
      qmi_framework_msg_hdr_type    msg_hdr;
      dsm_item_type             *   sdu_in;
    } cmd_hdlr_cb;

    struct
    {
      void           * user_data;
      dsm_item_type  * pkt;
    } dmsi_resp;

    struct
    {
      qmi_dmsi_report_status_ev_e_type         evt_type;
      void                                    *evt_value;
      uint8                                    clid;
    }dms_evt_info;

    struct
    {
      qmi_if_cm_ph_info_type  info;
      cm_ph_event_e_type      event;
    } cm_ph;

    struct
    {
      void *                user_data;
      cm_ph_cmd_err_e_type  err_type;
    } cm_ph_err;

    struct
    {
      void *                 user_data;
      cm_call_cmd_e_type     call_cmd;
      cm_call_cmd_err_e_type err_type;
    } cm_call_err;

    qmi_if_mmgsdi_evt_info_type mmgsdi_evt_info;
	
#ifdef FEATURE_TDSCDMA
    tdscdma_set_config_res_type tdscdma_set_config_res;
    tdscdma_get_config_res_type tdscdma_get_config_res;
#endif
  } data;
} qmi_dmsi_cmd_buf_type;

/*---------------------------------------------------------------------------
  DMS client sunscription preference enum
---------------------------------------------------------------------------*/
typedef enum
{
  DMSI_BIND_SUBS_PRIMARY   = 0x00,
#ifdef FEATURE_DUAL_SIM
  DMSI_BIND_SUBS_SECONDARY,
#endif
#ifdef FEATURE_TRIPLE_SIM
  DMSI_BIND_SUBS_TERTIARY,
#endif
  DMSI_BIND_SUBS_MAX
} qmi_dmsi_client_subs_e_type;

/*---------------------------------------------------------------------------
  QMI Device Management Service (DMS) instance state definition &
  DMS client state definition
---------------------------------------------------------------------------*/
typedef struct qmi_dmsi_client_state_s
{
  qmi_common_client_state_type common; // must be first since we alias
  int16 qmi_inst;
  uint32 cookie;
  struct 
  {  
    boolean  report_power_state; 
    boolean  report_pin_status;
    boolean  report_activation_state;
    boolean  report_operating_mode;
    boolean  report_uim_state;
    boolean  report_wd_state;
    boolean  report_prl_init;
    uint8    bat_lvl_lower_limit;
    uint8    bat_lvl_upper_limit;
    uint8    last_bat_lvl_reported;
    uint8    last_power_state_reported;
    boolean  report_cdma_lock_mode; 
    boolean  report_device_mode;
  } report_status;
  sys_oprt_mode_e_type      pending_oprt_mode;
#if defined(FEATURE_MMGSDI_SESSION_LIB)
#if defined(FEATURE_GSM) || defined(FEATURE_WCDMA) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA)
  mmgsdi_perso_feature_enum_type get_ck_status_pending;
#endif /* defined(FEATURE_GSM) || defined(FEATURE_WCDMA) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA) */
#endif /* defined(FEATURE_MMGSDI_SESSION_LIB) */
  qmi_dmsi_client_subs_e_type subscription_id;
} qmi_dmsi_client_state_type;

/*---------------------------------------------------------------------------
  Watermark queue to hold deferred responses
---------------------------------------------------------------------------*/
typedef qmi_dmsi_client_state_type* qmi_dmsi_client_state_ptr_type; 

typedef struct
{
  qmi_dmsi_client_state_ptr_type  client[DMSI_MAX_CLIDS];
  q_type                          set_oprt_mode_pending_q;
#ifdef FEATURE_TDSCDMA
  q_type                          set_tdscdma_config_pending_q;
  q_type                          get_tdscdma_config_pending_q;
#endif
} qmi_dmsi_state_type;

#ifdef FEATURE_PREFERRED_ROAMING

extern nv_roaming_list_type nv_pr_list;  /* buffer to hold preferred
                                            roaming list table being
                                            sent to or read from NV
                                          */
#endif /* FEATURE_PREFERRED_ROAMING */

#if defined(FEATURE_MMGSDI_SESSION_LIB)
typedef enum
{
  DMSI_SIM_INIT_SUCCESS           = 0x00,
  DMSI_SIM_INIT_FAIL_OR_PROGRESS  = 0x01,
  DMSI_SIM_NOT_INSERTED           = 0x02,
  DMSI_SIM_UNKNOWN                = 0xFF
} qmi_dmsi_uim_state_e_type;

#endif /* defined(FEATURE_MMGSDI_SESSION_LIB) */

#if defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900)
#ifdef FEATURE_PREFERRED_ROAMING
#ifdef FEATURE_EXTENDED_PRL
extern prl_sspr_p_rev_e_type sspr_p_rev; /* PRL protocol revision */
#endif
#endif /* FEATURE_PREFERRED_ROAMING */
#endif /* defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900) */


/* Configuration parameters to be used for TDSCDMA */
typedef enum
{
  DMSI_TEST_CONFIG_TDS_FIELD,
  DMSI_TEST_CONFIG_TDS_LAB,
  DMSI_TEST_CONFIG_TDS_USER,
} qmi_dmsi_tdscdma_config_type;

/*===========================================================================

                               INTERNAL DATA

===========================================================================*/

/*---------------------------------------------------------------------------
  QMI callback definition
---------------------------------------------------------------------------*/
static boolean qmi_dmsi_alloc_clid_cb
(
  qmi_framework_common_msg_hdr_type * svc_common_hdr
);

static void qmi_dmsi_dealloc_clid_cb
(
  qmi_framework_common_msg_hdr_type * svc_common_hdr
); 

static void qmi_dmsi_init_cb
(
  uint16 num_instances
);

static void qmi_dmsi_cmd_hdlr_cb
( 
  qmi_framework_msg_hdr_type* msg_hdr,
  dsm_item_type ** sdu
);


/*===========================================================================

                       FORWARD FUNCTION DECLARATIONS 

===========================================================================*/

/*---------------------------------------------------------------------------
  QMI service command handlers
  forward declarations & cmd handler dispatch table definition
---------------------------------------------------------------------------*/

static dsm_item_type*  qmi_dmsi_reset(void*, void*, void*, dsm_item_type **);
static dsm_item_type * qmi_dmsi_bind_subscription(
                                              void *,
                                              void *,
                                              void *,
                                              dsm_item_type ** );
static dsm_item_type * qmi_dmsi_get_bind_subscription(
                                              void *,
                                              void *,
                                              void *,
                                              dsm_item_type ** );
static dsm_item_type*  qmi_dmsi_set_event_report(
                                              void*, 
                                              void*, 
                                              void*,
                                              dsm_item_type **);
static dsm_item_type*  qmi_dmsi_get_device_cap(
                                              void*, 
                                              void*, 
                                              void*,
                                              dsm_item_type **);
static dsm_item_type*  qmi_dmsi_get_device_mfr(
                                              void*, 
                                              void*, 
                                              void*,
                                              dsm_item_type **);
static dsm_item_type*  qmi_dmsi_get_device_model_id(
                                              void*, 
                                              void*, 
                                              void*,
                                              dsm_item_type **);
static dsm_item_type*  qmi_dmsi_get_device_rev_id(
                                              void*, 
                                              void*, 
                                              void*,
                                              dsm_item_type **);
static dsm_item_type*  qmi_dmsi_get_msisdn(void*, 
                                           void*, 
                                           void*,
                                           dsm_item_type **);
static dsm_item_type*  qmi_dmsi_get_device_serial_numbers(
                                              void*, 
                                              void*, 
                                              void*,
                                              dsm_item_type **);
static dsm_item_type*  qmi_dmsi_get_power_state(
                                              void*, 
                                              void*, 
                                              void*,
                                              dsm_item_type **);
static dsm_item_type*  qmi_dmsi_set_uim_pin_protection(
                                              void*, 
                                              void*, 
                                              void*,
                                              dsm_item_type **);
static dsm_item_type*  qmi_dmsi_verify_uim_pin(
                                              void*, 
                                              void*, 
                                              void*,
                                              dsm_item_type **);
static dsm_item_type*  qmi_dmsi_unblock_uim_pin(
                                              void*, 
                                              void*, 
                                              void*,
                                              dsm_item_type **);
static dsm_item_type*  qmi_dmsi_change_uim_pin(
                                              void*, 
                                              void*, 
                                              void*,
                                              dsm_item_type **);
static dsm_item_type*  qmi_dmsi_get_uim_pin_status(
                                              void*, 
                                              void*, 
                                              void*,
                                              dsm_item_type **);
static dsm_item_type*  qmi_dmsi_get_device_hardware_rev(
                                              void*, 
                                              void*, 
                                              void*,
                                              dsm_item_type **);
static dsm_item_type*  qmi_dmsi_get_operating_mode(
                                              void*, 
                                              void*, 
                                              void*,
                                              dsm_item_type **);
static dsm_item_type*  qmi_dmsi_set_operating_mode(
                                              void*, 
                                              void*, 
                                              void*,
                                              dsm_item_type **);
static dsm_item_type*  qmi_dmsi_get_time(void*, 
                                         void*, 
                                         void*,
                                         dsm_item_type **);
static dsm_item_type*  qmi_dmsi_set_time(void*, 
                                         void*, 
                                         void*,
                                         dsm_item_type **);
static dsm_item_type*  qmi_dmsi_get_prl_ver(void*, 
                                            void*, 
                                            void*,
                                            dsm_item_type **);
static dsm_item_type*  qmi_dmsi_get_current_prl_info(void*, 
                                            void*, 
                                            void*,
                                            dsm_item_type **);
static dsm_item_type*  qmi_dmsi_get_activated_state(
                                              void*, 
                                              void*, 
                                              void*,
                                              dsm_item_type **);
static dsm_item_type*  qmi_dmsi_activate_automatic(
                                              void*, 
                                              void*, 
                                              void*,
                                              dsm_item_type **);
static dsm_item_type*  qmi_dmsi_activate_manual(
                                              void*, 
                                              void*, 
                                              void*,
                                              dsm_item_type **);
static dsm_item_type*  qmi_dmsi_get_user_lock_state(void*, 
                                                    void*, 
                                                    void*,
                                                    dsm_item_type **);
static dsm_item_type*  qmi_dmsi_set_user_lock_state(void*, 
                                                    void*, 
                                                    void*,
                                                    dsm_item_type **);
static dsm_item_type*  qmi_dmsi_set_user_lock_code(void*, 
                                                   void*, 
                                                   void*,
                                                   dsm_item_type **);
static dsm_item_type*  qmi_dmsi_read_user_data(
                                              void*, 
                                              void*, 
                                              void*,
                                              dsm_item_type **);
static dsm_item_type*  qmi_dmsi_write_user_data(
                                              void*, 
                                              void*, 
                                              void*,
                                              dsm_item_type **);
static dsm_item_type*  qmi_dmsi_read_eri_file(
                                              void*, 
                                              void*, 
                                              void*,
                                              dsm_item_type **);
static dsm_item_type*  qmi_dmsi_restore_factory_defaults(
                                              void*, 
                                              void*, 
                                              void*,
                                              dsm_item_type **);
static dsm_item_type*  qmi_dmsi_validate_spc(void*, 
                                             void*, 
                                             void*,
                                             dsm_item_type **);
static dsm_item_type*  qmi_dmsi_get_uim_iccid(
                                              void*, 
                                              void*,
                                              void*,
                                              dsm_item_type **);
#ifdef FEATURE_DATA_QMI_ADDENDUM
static dsm_item_type*  qmi_dmsi_get_host_lock(
                                              void*, 
                                              void*,
                                              void*,
                                              dsm_item_type **);
#endif /* FEATURE_DATA_QMI_ADDENDUM */
static dsm_item_type*  qmi_dmsi_uim_get_ck_status(
                                              void*, 
                                              void*,
                                              void*,
                                              dsm_item_type **);

static dsm_item_type*  qmi_dmsi_uim_unblock_ck(
                                              void*, 
                                              void*,
                                              void*,
                                              dsm_item_type **);

static dsm_item_type*  qmi_dmsi_uim_set_ck_protection(
                                              void*, 
                                              void*,
                                              void*,
                                              dsm_item_type **);

static dsm_item_type*  qmi_dmsi_get_uim_imsi(
                                              void*, 
                                              void*, 
                                              void*,
                                              dsm_item_type **);

static dsm_item_type*  qmi_dmsi_uim_get_state(
                                              void*, 
                                              void*, 
                                              void*,
                                              dsm_item_type **);

static dsm_item_type*  qmi_dmsi_get_band_capability(
                                              void*, 
                                              void*, 
                                              void*,
                                              dsm_item_type **);
static dsm_item_type*  qmi_dmsi_get_factory_sku(
                                              void*, 
                                              void*, 
                                              void*,
                                              dsm_item_type **);
#ifdef FEATURE_DATA_QMI_ADDENDUM
static dsm_item_type*  qmi_dmsi_get_firmware_pref(
                                              void*, 
                                              void*,
                                              void*,
                                              dsm_item_type **);
static dsm_item_type*  qmi_dmsi_set_firmware_pref(
                                              void*, 
                                              void*,
                                              void*,
                                              dsm_item_type **);
static dsm_item_type*  qmi_dmsi_list_stored_images(
                                              void*, 
                                              void*,
                                              void*,
                                              dsm_item_type **);
static dsm_item_type*  qmi_dmsi_delete_stored_image(
                                              void*, 
                                              void*,
                                              void*,
                                              dsm_item_type **);
static dsm_item_type*  qmi_dmsi_get_stored_image_info(
                                              void*, 
                                              void*,
                                              void*,
                                              dsm_item_type **);
#endif /* FEATURE_DATA_QMI_ADDENDUM */
static dsm_item_type*  qmi_dmsi_get_alt_net_config(
                                              void*, 
                                              void*, 
                                              void*,
                                              dsm_item_type **);
static dsm_item_type*  qmi_dmsi_set_alt_net_config(
                                              void*, 
                                              void*, 
                                              void*,
                                              dsm_item_type **);
#ifdef FEATURE_DATA_QMI_ADDENDUM
static dsm_item_type*  qmi_dmsi_get_boot_image_download_mode(
                                              void*, 
                                              void*,
                                              void*,
                                              dsm_item_type **);
static dsm_item_type*  qmi_dmsi_set_boot_image_download_mode(
                                              void*, 
                                              void*,
                                              void*,
                                              dsm_item_type **);
#endif /* FEATURE_DATA_QMI_ADDENDUM */
static dsm_item_type*  qmi_dmsi_get_sw_version(void*, 
                                            void*, 
                                            void*,
                                            dsm_item_type **);
static dsm_item_type*  qmi_dmsi_set_spc(void*, 
                                            void*, 
                                            void*,
                                            dsm_item_type **);
static dsm_item_type*  qmi_dmsi_set_ap_sw_version(
                                              void*, 
                                              void*, 
                                              void*,
                                              dsm_item_type **);
static dsm_item_type*  qmi_dmsi_get_cdma_lock_mode(
                                              void*, 
                                              void*, 
                                              void*,
                                              dsm_item_type **);
static dsm_item_type * qmi_dmsi_set_test_config_req(
                                              void *,
                                              void *,
                                              void *,
                                              dsm_item_type **);
static dsm_item_type * qmi_dmsi_get_test_config_req(
                                              void *,
                                              void *,
                                              void *,
                                              dsm_item_type **);
static dsm_item_type * qmi_dmsi_get_oem_china_operator(
                                              void *,
                                              void *,
                                              void *,
                                              dsm_item_type **);
static dsm_item_type * qmi_dmsi_get_mac_address(
                                              void *,
                                              void *,
                                              void *,
                                              dsm_item_type **);
static dsm_item_type*  qmi_dmsi_get_encrypted_device_serial_numbers(
                                              void*, 
                                              void*, 
                                              void*,
                                              dsm_item_type **);
static dsm_item_type*  qmi_dmsi_get_device_serial_numbers_ext(
                                              void *,
                                              void *,
                                              void *,
                                              dsm_item_type **,
                                              boolean);


#define DMS_HDLR(a,b)  QMI_SVC_HDLR( a, (qmi_svc_hdlr_ftype)b )

static qmi_svc_cmd_hdlr_type  qmi_dmsi_cmd_callbacks[DMSI_CMD_MAX] =
{   
  DMS_HDLR( DMSI_CMD_VAL_RESET,
                qmi_dmsi_reset),
  DMS_HDLR( DMSI_CMD_VAL_SET_EVENT_REPORT,
                qmi_dmsi_set_event_report),
  DMS_HDLR( DMSI_CMD_VAL_GET_DEVICE_CAP,
                qmi_dmsi_get_device_cap),
  DMS_HDLR( DMSI_CMD_VAL_GET_DEVICE_MFR,
                qmi_dmsi_get_device_mfr),
  DMS_HDLR( DMSI_CMD_VAL_GET_DEVICE_MODEL_ID,
                qmi_dmsi_get_device_model_id),
  DMS_HDLR( DMSI_CMD_VAL_GET_DEVICE_REV_ID,
                qmi_dmsi_get_device_rev_id),
  DMS_HDLR( DMSI_CMD_VAL_GET_MSISDN,
                qmi_dmsi_get_msisdn),
  DMS_HDLR( DMSI_CMD_VAL_GET_DEVICE_SERIAL_NUMBERS,
                qmi_dmsi_get_device_serial_numbers),
  DMS_HDLR( DMSI_CMD_VAL_GET_POWER_STATE,
                qmi_dmsi_get_power_state ),
  DMS_HDLR( DMSI_CMD_VAL_SET_UIM_PIN_PROTECTION,
                qmi_dmsi_set_uim_pin_protection ),
  DMS_HDLR( DMSI_CMD_VAL_VERIFY_UIM_PIN,
                qmi_dmsi_verify_uim_pin ),
  DMS_HDLR( DMSI_CMD_VAL_UNBLOCK_UIM_PIN,
                qmi_dmsi_unblock_uim_pin ),
  DMS_HDLR( DMSI_CMD_VAL_CHANGE_UIM_PIN,
                qmi_dmsi_change_uim_pin ),
  DMS_HDLR( DMSI_CMD_VAL_GET_UIM_PIN_STATUS,
                qmi_dmsi_get_uim_pin_status ),
  DMS_HDLR( DMSI_CMD_VAL_GET_DEVICE_HARDWARE_REV,
                qmi_dmsi_get_device_hardware_rev ),
  DMS_HDLR( DMSI_CMD_VAL_GET_OPERATING_MODE,
                qmi_dmsi_get_operating_mode ),
  DMS_HDLR( DMSI_CMD_VAL_SET_OPERATING_MODE,
                qmi_dmsi_set_operating_mode ),
  DMS_HDLR( DMSI_CMD_VAL_GET_TIME,
                qmi_dmsi_get_time ),
  DMS_HDLR( DMSI_CMD_VAL_GET_PRL_VER,
                qmi_dmsi_get_prl_ver ),
  DMS_HDLR( DMSI_CMD_VAL_GET_ACTIVATED_STATE,
                qmi_dmsi_get_activated_state ),
  DMS_HDLR( DMSI_CMD_VAL_ACTIVATE_AUTOMATIC,
                qmi_dmsi_activate_automatic ),
  DMS_HDLR( DMSI_CMD_VAL_ACTIVATE_MANUAL,
                qmi_dmsi_activate_manual ),
  DMS_HDLR( DMSI_CMD_VAL_GET_USER_LOCK_STATE,
                qmi_dmsi_get_user_lock_state ),
  DMS_HDLR( DMSI_CMD_VAL_SET_USER_LOCK_STATE,
                qmi_dmsi_set_user_lock_state ),
  DMS_HDLR( DMSI_CMD_VAL_SET_USER_LOCK_CODE,
                qmi_dmsi_set_user_lock_code ),
  DMS_HDLR( DMSI_CMD_VAL_READ_USER_DATA,
                qmi_dmsi_read_user_data ),
  DMS_HDLR( DMSI_CMD_VAL_WRITE_USER_DATA,
                qmi_dmsi_write_user_data ),
  DMS_HDLR( DMSI_CMD_VAL_READ_ERI_FILE,
                qmi_dmsi_read_eri_file ),
  DMS_HDLR( DMSI_CMD_VAL_RESTORE_FACTORY_DEFAULTS,
                qmi_dmsi_restore_factory_defaults ),
  DMS_HDLR( DMSI_CMD_VAL_VALIDATE_SPC,
                qmi_dmsi_validate_spc ),
  DMS_HDLR( DMSI_CMD_VAL_GET_UIM_ICCID,
                qmi_dmsi_get_uim_iccid ),
#ifdef FEATURE_DATA_QMI_ADDENDUM
  DMS_HDLR( DMSI_CMD_VAL_GET_HOST_LOCK,
                qmi_dmsi_get_host_lock ),
#endif /* FEATURE_DATA_QMI_ADDENDUM */
  DMS_HDLR( DMSI_CMD_VAL_UIM_GET_CK_STATUS,
                qmi_dmsi_uim_get_ck_status ),
  DMS_HDLR( DMSI_CMD_VAL_UIM_SET_CK_PROTECTION,
                qmi_dmsi_uim_set_ck_protection ),
  DMS_HDLR( DMSI_CMD_VAL_UIM_UNBLOCK_CK,
                qmi_dmsi_uim_unblock_ck ),
  DMS_HDLR( DMSI_CMD_VAL_GET_UIM_IMSI,
                qmi_dmsi_get_uim_imsi ),
  DMS_HDLR( DMSI_CMD_VAL_UIM_GET_STATE,
                qmi_dmsi_uim_get_state ),
  DMS_HDLR( DMSI_CMD_VAL_GET_BAND_CAPABILITY,
                qmi_dmsi_get_band_capability ),
  DMS_HDLR( DMSI_CMD_VAL_GET_FACTORY_SKU,
                qmi_dmsi_get_factory_sku ),
#ifdef FEATURE_DATA_QMI_ADDENDUM
  DMS_HDLR( DMSI_CMD_VAL_GET_FIRMWARE_PREF,
                qmi_dmsi_get_firmware_pref ),
  DMS_HDLR( DMSI_CMD_VAL_SET_FIRMWARE_PREF,
                qmi_dmsi_set_firmware_pref ),
  DMS_HDLR( DMSI_CMD_VAL_LIST_STORED_IMAGES,
                qmi_dmsi_list_stored_images ),
  DMS_HDLR( DMSI_CMD_VAL_DELETE_STORED_IMAGE,
                qmi_dmsi_delete_stored_image ),
  DMS_HDLR( DMSI_CMD_VAL_GET_STORED_IMAGE_INFO,
                qmi_dmsi_get_stored_image_info ),
#endif /* FEATURE_DATA_QMI_ADDENDUM */
  DMS_HDLR( DMSI_CMD_VAL_SET_TIME,
                qmi_dmsi_set_time ),
  DMS_HDLR( DMSI_CMD_VAL_GET_ALT_NET_CONFIG,
                qmi_dmsi_get_alt_net_config ),
  DMS_HDLR( DMSI_CMD_VAL_SET_ALT_NET_CONFIG,
                qmi_dmsi_set_alt_net_config )
#ifdef FEATURE_DATA_QMI_ADDENDUM
  ,
  DMS_HDLR( DMSI_CMD_VAL_GET_BOOT_IMAGE_DOWNLOAD_MODE,
                qmi_dmsi_get_boot_image_download_mode ),
  DMS_HDLR( DMSI_CMD_VAL_SET_BOOT_IMAGE_DOWNLOAD_MODE,
                qmi_dmsi_set_boot_image_download_mode )
#endif /* FEATURE_DATA_QMI_ADDENDUM */
  ,
  DMS_HDLR( DMSI_CMD_VAL_GET_SW_VERSION,
                qmi_dmsi_get_sw_version )
 ,
 DMS_HDLR( DMSI_CMD_VAL_SET_SPC,
                qmi_dmsi_set_spc ),
 DMS_HDLR( DMSI_CMD_VAL_GET_CURRENT_PRL_INFO,
                qmi_dmsi_get_current_prl_info ),
 DMS_HDLR( DMSI_CMD_VAL_BIND_SUBSCRIPTION,
                  qmi_dmsi_bind_subscription),
 DMS_HDLR( DMSI_CMD_VAL_GET_BIND_SUBSCRIPTION,
                  qmi_dmsi_get_bind_subscription),
 DMS_HDLR( DMSI_CMD_VAL_SET_AP_SW_VERSION,
                  qmi_dmsi_set_ap_sw_version ),
 DMS_HDLR( DMSI_CMD_VAL_GET_CDMA_LOCK_MODE,
                qmi_dmsi_get_cdma_lock_mode ),
 DMS_HDLR( DMSI_CMD_VAL_SET_TEST_CONFIG,
                qmi_dmsi_set_test_config_req ),
 DMS_HDLR( DMSI_CMD_VAL_GET_TEST_CONFIG,
                qmi_dmsi_get_test_config_req ),
 DMS_HDLR( DMSI_CMD_VAL_GET_OEM_CHINA_OPERATOR,
                qmi_dmsi_get_oem_china_operator ),
 DMS_HDLR( DMSI_CMD_VAL_GET_MAC_ADDRESS,
                qmi_dmsi_get_mac_address ),
 DMS_HDLR( DMSI_CMD_VAL_GET_ENCRYPTED_DEVICE_SERIAL_NUMBERS,
                qmi_dmsi_get_encrypted_device_serial_numbers)
};

static qmi_dmsi_state_type  qmi_dms_state;

/*---------------------------------------------------------------------------
  QMI Service (QMI_DMS) configuration definition
---------------------------------------------------------------------------*/

static ds_qmi_fw_svc_cfg_type  qmi_dmsi_cfg;

/*---------------------------------------------------------------------------
  QMI instance configurations
---------------------------------------------------------------------------*/
static void     qmi_dmsi_reset_client(void *);



typedef enum
{
  QMI_DMSI_ACTIVATION_TYPE_CALL  = 0,
  QMI_DMSI_ACTIVATION_TYPE_OTASP = 1,
  QMI_DMSI_ACTIVATION_TYPE_MAX,
  QMI_DMSI_ACTIVATION_TYPE_MAX32 = 0x10000000
} qmi_dmsi_activation_e_type;

typedef enum {
  DMS_DEVICE_MAC_WLAN,      /**< WLAN device MAC address \n */
  DMS_DEVICE_MAC_BT,        /**< Bluetooth device MAC address \n */
  DMS_DEVICE_MAX
}qmi_dmsi_device_mac_e_type;

typedef enum {
  DMS_OPERATOR_CT,
  DMS_OPERATOR_CU,
  DMS_OPERATOR_CMCC,
  DMS_OPERATOR_MAX
}qmi_dmsi_oem_china_operator_enum;

typedef struct
{
  qmi_dmsi_activation_e_type    act_type;
  union
  {
    cm_call_event_e_type     call_event;
#if (defined FEATURE_OTASP)
    cm_otasp_status_e_type   otasp_state;
#endif /* (defined FEATURE_OTASP) */
  } event;
} qmi_dmsi_activation_evt_info_type;

typedef struct 
{
  /* control point that is currently writing PRL information */
  qmi_common_client_state_type *cl_sp;

  /* total PRL length */
  uint16 len_total;
  /* PRL length received so far */
  uint16 len_received;
  /* next expected sequence number */
  uint8  seq_num;
  /* PRL data received so far */
  uint8  *data;
} qmi_dmsi_prl_segmented_type;

/*---------------------------------------------------------------------------
  QMI_DMS Global state information - initialized in qmi_dms_init()
  Global to all QMI instances
---------------------------------------------------------------------------*/
static struct
{ 
  boolean inited;
  /*-------------------------------------------------------------------------
    Periodic timer to poll battery level - export to OEM_CHARGER module
  -------------------------------------------------------------------------*/
  struct
  {
    uint8  low_limit;
    uint8  high_limit;
    uint8  report_power_state_client_count;
  } pwr_rpt;
#ifdef FEATURE_MMGSDI_SESSION_LIB
  struct
  {
    mmgsdi_slot_id_enum_type  slot;
    uint8  report_pin_status_client_count;
    struct
    {
      uint8                     pin_id;
      qmi_pin_status_enum_type  status; 
      uint8                     num_retries;
      uint8                     num_unblock_retries;
    } pin_info[DMSI_MAX_PINS];
#if defined(FEATURE_GSM) || defined(FEATURE_WCDMA) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA)
    struct
    {
      struct
      {
        uint8   state;
        uint8   verify_retries;
        uint8   unblock_retries;
      } perso_data[MMGSDI_PERSO_SIM+1];
      boolean in_use;
      boolean state_req_pending;
      boolean verify_req_pending;
      boolean unblock_req_pending;
    } perso_scratch;
    q_type  get_ck_state_q;
#endif /* defined(FEATURE_GSM) || defined(FEATURE_WCDMA) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA) */
    struct
    {
      boolean inited;
      uint8   blocked_mask;
    } mmgsdi_perso;
  } sim;
  qmi_dmsi_uim_state_e_type uim_state;
#endif /* FEATURE_MMGSDI_SESSION_LIB */

  cm_call_id_type               voice_call_id;

  qmi_dmsi_activation_evt_info_type act_evt;

  /* Segmented PRL data */
  qmi_dmsi_prl_segmented_type prl_info;
  
  qmi_dmsi_activation_state_e_type activation_state;

  uint32 clients_created;  
  /* used to store NV config values so they only have to be read once 
     at startup */
  boolean nv_allow_invalid_spc;
  boolean nv_restrict_imsi_access;
  boolean nv_use_max_defined_data_rates;
  boolean nv_use_last_otasp_act_state;
  boolean cdma_lock_mode;
} qmi_dmsi_global = {FALSE,};



/*===========================================================================

                       FORWARD FUNCTION DECLARATIONS 

===========================================================================*/
static void qmi_dmsi_process_cmd(void *);

static void qmi_dmsi_process_svc_init
(
  uint16 num_instances
);

static void qmi_dmsi_process_cmd_hdlr
(
  qmi_framework_msg_hdr_type* msg_hdr,
  dsm_item_type * sdu_in 
);

static void qmi_dmsi_process_alloc_clid
(
  qmi_framework_common_msg_hdr_type * common_msg_hdr
);

static void qmi_dmsi_process_dealloc_clid
(
  qmi_framework_common_msg_hdr_type * common_msg_hdr
);

static boolean qmi_dmsi_send_response
(
  qmi_cmd_buf_type *              cmd_buf_p,
  dsm_item_type *                 msg_ptr
);

static void *qmi_dmsi_get_cmd_buf(qmi_cmd_id_e_type);

#define qmi_dmsi_free_cmd_buf(buf_ptr) PS_SYSTEM_HEAP_MEM_FREE(buf_ptr)


static void qmi_dmsi_event_report_ind
(
  void *   sp,
  uint8    clid,
  uint32   event_mask,
  uint8    power_status,  /* bitmask */
  uint8    battery_lvl,
  uint8    pin_index,
  uint16   activation_state,
  uint8    operating_mode,
  boolean  wd_state,
  boolean  cdma_lock_mode 
);
static uint8 qmi_dmsi_get_hw_supported_radio_if_list(uint8 *);
static qmi_error_e_type qmi_dmsi_get_max_channel_rate(uint32 *, uint32 *);
static uint8 qmi_dmsi_get_voice_capability(void);
static uint8 qmi_dmsi_get_sim_capability(void);
static uint64 qmi_dmsi_get_voice_support_capability(void);
static uint64 qmi_dmsi_get_simul_voice_and_data_capability(void);
static void qmi_dmsi_set_battery_level_limits(uint8);
static qmi_error_e_type qmi_dmsi_get_multisim_info(dsm_item_type **, uint8);
static qmi_error_e_type qmi_dmsi_get_device_overall_capability(sys_overall_feature_t*);

#ifdef FEATURE_MMGSDI_SESSION_LIB
  static void qmi_dmsi_generate_pin_operation_resp
  (
    mmgsdi_pin_operation_cnf_type *
  );
  static void qmi_dmsi_generate_pin_status_resp
  (
    mmgsdi_get_all_pin_status_cnf_type *
  );
  static void qmi_dmsi_generate_read_uim_iccid_resp
  (
    mmgsdi_read_cnf_type *
  );
  static void qmi_dmsi_generate_read_uim_imsi_resp
  (
    mmgsdi_read_cnf_type *
  );
  static void qmi_dmsi_generate_read_uim_msisdn_resp
  (
    mmgsdi_read_cnf_type * read_cnf
  );
  static void qmi_dmsi_generate_read_prl_ver_resp
  (
    mmgsdi_session_read_prl_cnf_type * read_prl_cnf
  );
#if defined(FEATURE_GSM) || defined(FEATURE_WCDMA) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA)
  static void qmi_dmsi_generate_get_ck_status_resp
  (
    mmgsdi_session_perso_cnf_type *
  );  
  static void qmi_dmsi_generate_set_ck_protection_resp
  (
    mmgsdi_session_perso_cnf_type *
  );
  static void qmi_dmsi_generate_unblock_ck_resp
  (
    mmgsdi_session_perso_cnf_type *
  );
#endif /* defined(FEATURE_GSM) || defined(FEATURE_WCDMA) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA) */

  static void qmi_dms_generate_uim_state_ind(void);
#endif /* FEATURE_MMGSDI_SESSION_LIB */
static qmi_error_e_type qmi_dmsi_oprt_mode_from_cm
(
  sys_oprt_mode_e_type,
  qmi_dmsi_oprt_mode_e_type *
);

#if defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900)
  static qmi_error_e_type qmi_dmsi_get_mdn_min(char *,int,char *,int);
  static boolean qmi_dmsi_minnum_validate(dword, word);
  static dword qmi_dmsi_generate_default_min(dword);
  static int qmi_dmsi_minnum_pcs_to_text(char *, dword, word);
  static void  qmi_dmsi_mobnum_pcs_to_text(char *, nv_mob_dir_data_type *);
  static boolean qmi_dmsi_mobnum_text_to_pcs(char*, uint8, nv_mob_dir_data_type *);
  static boolean qmi_dmsi_min_text_to_min12(char*, uint8, dword*, word*);
  static boolean qmi_dmsi_reset_imsi_components(byte nam);
  static boolean qmi_dmsi_reset_imsi_t_components(byte nam);
  static void qmi_dmsi_generate_activation_state_ind(void);
  static qmi_dmsi_activation_state_e_type qmi_dmsi_calculate_activated_state(
                                                                          void);
#if (defined FEATURE_OTASP)
  static void qmi_dmsi_otasp_forced_call_release(void);
#endif /* (defined FEATURE_OTASP) */
#endif /* defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900) */

  static void qmi_dmsi_reset_prl_series_info(void);

void qmi_dms_event_report_req_send_ind
(
   qmi_dmsi_report_status_ev_e_type               event_type,
   void                                         * event_info,
   uint8                                          clid
);

#ifdef FEATURE_TDSCDMA
static void qmi_dmsi_tdscdma_config_init(void);
static qmi_error_e_type qmi_dmsi_tdscdma_get_config_val(tds_rrc_config_e_type config, uint32 *val);
static void qmi_dmsi_generate_set_test_config_resp(tdscdma_set_config_res_type *config_resp);
static void qmi_dmsi_generate_get_test_config_resp(tdscdma_get_config_res_type *config_resp);
#endif

static qmi_error_e_type qmi_dmsi_get_device_multisim_current_cap_info
(
  dsm_item_type **,
  uint8
);

static uint32 qmi_dmsi_is_simul_voice_data 
(
   sys_ue_mode_e_type ue_mode 
);

boolean qmi_dmsi_msgr_hdlr 
(
  msgr_umid_type             msgrtype,
  const msgr_hdr_struct_type *dsmsg
);

static void qmi_dmsi_policyman_config_init (void);

static uint64 qmi_dmsi_convert_rat_mask (uint32 rat);

static void qmi_dmsi_generate_current_device_capability_ind( void );

static qmi_error_e_type qmi_dmsi_get_subs_device_feature_mode
(
  dsm_item_type ** response
);

static qmi_error_e_type qmi_dmsi_get_max_active_data_subs
(
  dsm_item_type ** response,
  uint8            cmd_type
);

/*===========================================================================

                             EXTERNAL FUNCTIONS

===========================================================================*/
#ifdef FEATURE_MMGSDI_SESSION_LIB
/*===========================================================================
  FUNCTION QMI_DMS_CHECK_VALID_ASYNC_CMD_BUF()

  DESCRIPTION
    To validate the command buf by checking the client cookie.
    
  PARAMETERS
    async_cmd_buf : pointer to the async command buffer

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean qmi_dms_check_valid_async_cmd_buf
(
  ds_qmi_fw_async_cmd_buf_type *async_cmd_buf
)
{  
  int i;
  qmi_dmsi_client_state_type * cl_sp;
  qmi_dmsi_state_type * dms_sp;
  
  if ((async_cmd_buf != NULL) && (async_cmd_buf->cmd_buf != NULL))
  {
    dms_sp = (qmi_dmsi_state_type*)&qmi_dms_state;
    for(i = 0; i < DMSI_MAX_CLIDS ; i++ )
    {
      cl_sp = dms_sp->client[i];
      if( cl_sp && (cl_sp->cookie == async_cmd_buf->cookie))
      {
        return TRUE;
      }
    }
  }  
  return FALSE;
}

/*===========================================================================
  FUNCTION QMI_DMS_MMGSDI_EVT_CBACK()

  DESCRIPTION
    Callback function called by mmgsdi to report events

  PARAMETERS
    event  : Info about the event generated

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void qmi_dms_mmgsdi_evt_cback
(
  const mmgsdi_event_data_type *  event
)
{
  qmi_dmsi_cmd_buf_type *  cmd_ptr;
  mmgsdi_slot_id_enum_type slot = MMGSDI_SLOT_AUTOMATIC;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  
  /*Received event_cback from MMGSDI , posting cmd to DS */
  
  switch(event->evt)
  {
    case MMGSDI_CARD_INSERTED_EVT:
      slot = event->data.card_inserted.slot;
      break;

    case MMGSDI_CARD_ERROR_EVT:
      slot = event->data.card_error.slot;
      break;

    case MMGSDI_CARD_REMOVED_EVT:
      slot = event->data.card_removed.slot;
      break;

    default:
      break;
  }
  
  switch(event->evt)
  {
    case MMGSDI_CARD_INSERTED_EVT:
    case MMGSDI_CARD_ERROR_EVT:
    case MMGSDI_CARD_REMOVED_EVT:
      cmd_ptr = (qmi_dmsi_cmd_buf_type *)qmi_dmsi_get_cmd_buf(QMI_CMD_PROCESS_UIM_GET_STATE);
      if( cmd_ptr == NULL)
      {
        return;
      }
      cmd_ptr->cmd_id = QMI_CMD_PROCESS_UIM_GET_STATE;
      cmd_ptr->data.mmgsdi_evt_info.event = event->evt;
      cmd_ptr->data.mmgsdi_evt_info.slot = slot;
      cmd_ptr->data.mmgsdi_evt_info.card_err_info = 
                 event->data.card_error.info;
      break;

    default:
      return;
  }

  dcc_send_cmd_ex(DCC_QMI_CMD, cmd_ptr);
} /* qmi_dms_mmgsdi_evt_cback */


/*===========================================================================
  FUNCTION QMI_DMS_PROCESS_MMGSDI_EVENT()

  DESCRIPTION
    To process event received from mmgsdi in DS task context.
    
  PARAMETERS
    event_info : Event info
    qmi_cmd    : Type of qmi cmd

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void qmi_dms_process_mmgsdi_event  
(
  qmi_if_mmgsdi_evt_info_type *  event_info,
  qmi_cmd_id_e_type              qmi_cmd
)
{
  qmi_dmsi_client_state_type *  cl_sp;
  qmi_dmsi_state_type        *  dms_sp;
  uint8                         j;
  uint8                         pin_index;
  boolean                       send_event_report_ind;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  send_event_report_ind = FALSE;
  pin_index = 0;
 
  LOG_MSG_INFO2_3("DMS received qmi cmd %d as a result of mmgsdi event" 
          "event sim slot %d qmi_dmsi_global.sim.slot %d",qmi_cmd, event_info->slot,qmi_dmsi_global.sim.slot);
 
  if(event_info->slot != qmi_dmsi_global.sim.slot)
  {
    /*sim slot is not supported by qmi - Ignoring"*/
    return;
  }

  switch(qmi_cmd)
  {
    case QMI_CMD_PROCESS_PIN_EVT:
      if(event_info->pin.pin_id >= DMSI_MAX_PINS)
      {
        return;
      }
      if((qmi_pin_status_enum_type) event_info->pin.status != 
           qmi_dmsi_global.sim.pin_info[event_info->pin.pin_id].status)
      {
        send_event_report_ind = TRUE;
        qmi_dmsi_global.sim.pin_info[event_info->pin.pin_id].status = 
                                                    (qmi_pin_status_enum_type) event_info->pin.status;
      }
      qmi_dmsi_global.sim.pin_info[event_info->pin.pin_id].num_retries = 
                                               (uint8)event_info->pin.num_retries;
      qmi_dmsi_global.sim.pin_info[event_info->pin.pin_id].num_unblock_retries =
                                     (uint8)event_info->pin.num_unblock_retries; 
      pin_index = event_info->pin.pin_id;
      break;
    
    default :
      break;
  }

  if(send_event_report_ind == FALSE)
  {
    return;
  }
   
  LOG_MSG_INFO2_0("Generating DMS IND for mmgsdi pin state change");
  /*-------------------------------------------------------------------------
    Generate event report ind for clients registered for pin status
  -------------------------------------------------------------------------*/
  dms_sp = (qmi_dmsi_state_type*)&qmi_dms_state;

    for( j = 0; j < DMSI_MAX_CLIDS ; j++ )
    {
      cl_sp = dms_sp->client[j];
      if( cl_sp && (cl_sp->common.clid != QMI_SVC_CLID_UNUSED) && 
          cl_sp->report_status.report_pin_status )
      {
         qmi_dmsi_event_report_ind(dms_sp,
                                   cl_sp->common.clid,
                                   DMSI_REPORT_STATUS_EV_PIN_STATUS,
                                   0,
                                   0,
                                   pin_index,
                                   0,
                                   0,
                                   0,
                                   0);
      }
    }
} /* qmi_dms_process_mmgsdi_event */

/*===========================================================================
  FUNCTION QMI_DMS_PROCESS_MMGSDI_PERSO_EVENT()

  DESCRIPTION
    Handler for MMGSDI_PERSO feature events

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void qmi_dms_process_mmgsdi_perso_event
(
  mmgsdi_perso_feature_enum_type  feature,
  mmgsdi_perso_status_enum_type   status
)
{
  boolean  blocked = TRUE;
  uint8 mask;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  LOG_MSG_INFO2_2("Received perso status %d and perso feature %d",status,feature);
  switch(status)
  {
    case MMGSDI_PERSO_STATUS_DONE:
    case MMGSDI_PERSO_STATUS_UNLOCKED:
    case MMGSDI_PERSO_STATUS_UNBLOCKED:
      blocked = FALSE;
      break;

    case MMGSDI_PERSO_STATUS_LOCKED:
    case MMGSDI_PERSO_STATUS_BLOCKED:
      blocked = TRUE;
      break;

    default:
      return;
  }

  switch(feature)
  {
    case MMGSDI_PERSO_NW:
    case MMGSDI_PERSO_NS:
    case MMGSDI_PERSO_SP:
    case MMGSDI_PERSO_CP:
    case MMGSDI_PERSO_SIM:
      if(blocked)
      {
        mask = (uint8)(1 << feature);
        qmi_dmsi_global.sim.mmgsdi_perso.blocked_mask |= mask;
      }
      else
      {
        mask = (uint8)(~(1 << feature));
        qmi_dmsi_global.sim.mmgsdi_perso.blocked_mask &= mask;
      }
      break;

    default:
      break;
  }
} /* qmi_dms_process_mmgsdi_perso_event */

/*===========================================================================
  FUNCTION QMI_DMS_PROCESS_MMGSDI_OPERATION_COMPLETE()

  DESCRIPTION
    To send a response to the QMI pin operation requested by the client.
    
  PARAMETERS
    cnf_type : Type of MMGSDI PIN confirmation
    cnf_data : Response data

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void qmi_dms_process_mmgsdi_operation_complete
(
  mmgsdi_cnf_enum_type    cnf_type,
  mmgsdi_cnf_type        *cnf_data,
  uint16                  cmd_type
)
{
  qmi_cmd_buf_type    *cmd_buf = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    
  switch(cnf_type)
  {
    case MMGSDI_PIN_OPERATION_CNF:
      qmi_dmsi_generate_pin_operation_resp(&(cnf_data->pin_operation_cnf));
      break;

    case MMGSDI_GET_ALL_PIN_STATUS_CNF:
      qmi_dmsi_generate_pin_status_resp(&(cnf_data->get_all_pin_status_cnf));
      break;

    case MMGSDI_SESSION_READ_PRL_CNF:
      qmi_dmsi_generate_read_prl_ver_resp(&(cnf_data->session_read_prl_cnf));
      break;

    case MMGSDI_READ_CNF:      
      cmd_buf = (qmi_cmd_buf_type*)
                   cnf_data->read_cnf.response_header.client_data;
      if(cmd_buf == NULL)
      {
        return;
      }
      else
      {
        if( cmd_buf->cmd_type == DMSI_CMD_VAL_GET_UIM_ICCID)
          qmi_dmsi_generate_read_uim_iccid_resp(&(cnf_data->read_cnf));
        else if( cmd_buf->cmd_type == DMSI_CMD_VAL_GET_UIM_IMSI)
          qmi_dmsi_generate_read_uim_imsi_resp(&(cnf_data->read_cnf));
        else if( cmd_buf->cmd_type == DMSI_CMD_VAL_GET_MSISDN)
          qmi_dmsi_generate_read_uim_msisdn_resp(&(cnf_data->read_cnf));
        else
          LOG_MSG_INFO1_1("Recd unhandled cmd_type %d from mmgsdi read",
                          cmd_buf->cmd_type);
      }
      break;

    case MMGSDI_SESSION_PERSO_CNF:
      cmd_buf = (qmi_cmd_buf_type*)
                   cnf_data->session_perso_cnf.response_header.client_data;
      if((cmd_buf == NULL) || (cmd_buf->x_p == NULL) || (cmd_buf->x_p->cl_sp == NULL))
      {
        LOG_MSG_INFO2_1("Cmd_buf or Transaction ptr or client state pointer is NULL, most likely client was released for cmd_type:%d",cmd_type);
        if(cmd_type == DMSI_CMD_VAL_UIM_GET_CK_STATUS)
        {
          if((qmi_dmsi_global.sim.get_ck_state_q.cnt > 0) &&
             (q_check(&qmi_dmsi_global.sim.get_ck_state_q) != NULL ))
          {
            /*first pending in q was lost as client released. Send the response for rest of the pending in q*/
            qmi_dmsi_generate_get_ck_status_resp(
                &(cnf_data->session_perso_cnf));
          }
          else if(qmi_dmsi_global.sim.perso_scratch.in_use)
          {  /*client requested got released and No other commands in q. Reset global data*/
              memset( &qmi_dmsi_global.sim.perso_scratch, 0,
                      sizeof(qmi_dmsi_global.sim.perso_scratch) );
          }
        }
        return;
      }
      else
      {
        switch(cmd_buf->cmd_type)
        {
#if defined(FEATURE_GSM) || defined(FEATURE_WCDMA) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA)
          case DMSI_CMD_VAL_UIM_GET_CK_STATUS:
            qmi_dmsi_generate_get_ck_status_resp(
              &(cnf_data->session_perso_cnf));
            break;
          case DMSI_CMD_VAL_UIM_SET_CK_PROTECTION:
            qmi_dmsi_generate_set_ck_protection_resp(
              &(cnf_data->session_perso_cnf));
            break;
          case DMSI_CMD_VAL_UIM_UNBLOCK_CK:
            qmi_dmsi_generate_unblock_ck_resp(
              &(cnf_data->session_perso_cnf));
            break;
#endif /* defined(FEATURE_GSM) || defined(FEATURE_WCDMA) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA) */
          default:
            LOG_MSG_INFO1_1("Recd unhandled cmd_type %d from mmgsdi_perso request",
                            cmd_buf->cmd_type);
            break;
        }
      }
      break;

    default:
      LOG_MSG_INFO1_1("Received unhandled cnf_type %d from mmgsdi", cnf_type);
      break;
  }
} /* qmi_dms_process_mmgsdi_operation_complete */

/*===========================================================================
  FUNCTION QMI_DMS_PROCESS_UIM_GET_STATE()

  DESCRIPTION
    To process event received for SIM status.
    
  PARAMETERS
    event    : MMGSDI event that was generated
    card_err_info: Contains card error code
    session_activated : Contains if session has been activated

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void qmi_dms_process_uim_get_state
(
  mmgsdi_events_enum_type event,
  mmgsdi_card_err_info_enum_type card_err_info,
  boolean session_activated
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

 LOG_MSG_INFO2_2("DMS received mmgsdi event %d for sim status with card_err_info %d ", event,card_err_info);

  switch(event)
  {
    case MMGSDI_CARD_INSERTED_EVT:
      qmi_dmsi_global.uim_state = DMSI_SIM_INIT_FAIL_OR_PROGRESS;
      break;
    /* SessionChanged replaces SelectAid ev, means init in progress */
    case MMGSDI_SESSION_CHANGED_EVT: 
      if ((DMSI_SIM_NOT_INSERTED == qmi_dmsi_global.uim_state) &&
           (FALSE == session_activated))
      {
        LOG_MSG_INFO2_0("Ignoring MMGSDI_SESSION_CHANGED_EVT since it is deactivated and SIM is not inserted ");
        /* Dont generate indication since there is no change in state */
        return;
      }
      else
      {
        qmi_dmsi_global.uim_state = DMSI_SIM_INIT_FAIL_OR_PROGRESS;
      }
      break;

    /* Card removed ev is seen only on targets with some special hardware */
    case MMGSDI_CARD_REMOVED_EVT: 
      qmi_dmsi_global.uim_state = DMSI_SIM_NOT_INSERTED;
      break;

    case MMGSDI_SUBSCRIPTION_READY_EVT:
      qmi_dmsi_global.uim_state = DMSI_SIM_INIT_SUCCESS;
      break;

    case MMGSDI_CARD_ERROR_EVT:
      /* Checking if card is present in the slot or not */
      /* If the hardware supports the mechanism to detect presence of the card, 
         MMGSDI_CARD_ERR_CARD_REMOVED will be sent when card gets removed. Else 
         one of the other causes will be sent for card removal or power up 
         without card
      */
      if((MMGSDI_CARD_ERR_NO_ATR_RCVD_AT_MAX_VOLT == card_err_info) ||
         (MMGSDI_CARD_ERR_CARD_REMOVED == card_err_info) ||
         (MMGSDI_CARD_ERR_NO_ATR_RCVD_AFTER_RESET == card_err_info) ||
         (MMGSDI_CARD_ERR_CORRUPT_ATR_RCVD_MAX_TIMES == card_err_info))
      {
        qmi_dmsi_global.uim_state = DMSI_SIM_NOT_INSERTED ;
      }
      else
      {
        qmi_dmsi_global.uim_state = DMSI_SIM_INIT_FAIL_OR_PROGRESS;
      }
      break;

    default:
      return;
  }

  LOG_MSG_INFO2_0("Generating DMS IND for uim state change");
  /*-------------------------------------------------------------------------
    Generate event report ind for clients registered for pin status
  -------------------------------------------------------------------------*/
  qmi_dms_generate_uim_state_ind();

} /* qmi_dms_process_uim_get_state */


#endif /* FEATURE_MMGSDI_SESSION_LIB */

/*===========================================================================
  FUNCTION QMI_DMS_INIT()

  DESCRIPTION
    Register the DMS service with QMI Framework 

  PARAMETERS
    None

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void qmi_dms_init
(
  void
)
{
  qmi_framework_err_e_type  reg_result = QMI_FRAMEWORK_ERR_NONE;
  qmi_idl_service_object_type   svc_obj;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_INFO1_0("QMI DMS service init");

  /*-------------------------------------------------------------------------
    Setting QMI DMS service command handler with dcc task process
  -------------------------------------------------------------------------*/
  qmi_task_set_svc_cmd_handler(QMUX_SERVICE_DMS, qmi_dmsi_process_cmd);

  /*-------------------------------------------------------------------------
    QMI DMS service configuration setup
  -------------------------------------------------------------------------*/
  qmi_dmsi_cfg.fw_cfg.base_version.major     = DMSI_BASE_VER_MAJOR;
  qmi_dmsi_cfg.fw_cfg.base_version.minor     = DMSI_BASE_VER_MINOR;

  qmi_dmsi_cfg.fw_cfg.addendum_version.major = DMSI_ADDENDUM_VER_MAJOR;
  qmi_dmsi_cfg.fw_cfg.addendum_version.minor = DMSI_ADDENDUM_VER_MINOR;

  qmi_dmsi_cfg.fw_cfg.cbs.alloc_clid         = qmi_dmsi_alloc_clid_cb;
  qmi_dmsi_cfg.fw_cfg.cbs.dealloc_clid       = qmi_dmsi_dealloc_clid_cb;
  qmi_dmsi_cfg.fw_cfg.cbs.init_cback         = qmi_dmsi_init_cb;
  qmi_dmsi_cfg.fw_cfg.cbs.cmd_hdlr           = qmi_dmsi_cmd_hdlr_cb;

  qmi_dmsi_cfg.cmd_hdlr_array                = qmi_dmsi_cmd_callbacks;
  qmi_dmsi_cfg.cmd_num_entries               = DMSI_CMD_MAX;


  /*-------------------------------------------------------------------------
    Calling QMI Framework API to register the service with Framework
  -------------------------------------------------------------------------*/
  reg_result = qmi_framework_reg_service( QMUX_SERVICE_DMS,
                                          &qmi_dmsi_cfg.fw_cfg );

  if (reg_result != QMI_FRAMEWORK_ERR_NONE)
  {
    return;
  }
  svc_obj =  dms_get_service_object_v01();
  (void) qmi_si_register_object ( svc_obj,
                                  0, /* Service Instance */
                                  dms_get_service_impl_v01() );

  qmi_dmsi_cfg.registered  = TRUE;

} /* qmi_dms_init */


/*===========================================================================
  FUNCTION QMI_DMS_CHARGER_EVENT_OCCURED()

  DESCRIPTION
    Call back function called when the battery level ind timer expires.
    Causes report_status_ind to be sent
    
  PARAMETERS
    arg_ptr : user data

  RETURN VALUE
    None
        
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void qmi_dms_charger_event_occured
(
  uint8 event_mask,
  uint8 power_state_mask,
  uint8 battery_level
)
{
  qmi_dmsi_client_state_type *  cl_sp;
  qmi_dmsi_state_type *         dms_sp;
  uint8                         j;
  boolean send_event_report_ind = FALSE;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  dms_sp = (qmi_dmsi_state_type*)&qmi_dms_state;

  qmi_dmsi_global.pwr_rpt.low_limit = DMSI_BAT_LVL_MIN;
  qmi_dmsi_global.pwr_rpt.high_limit = DMSI_BAT_LVL_MAX;

    for( j = 0; j < DMSI_MAX_CLIDS ; j++ )
    {
      cl_sp = dms_sp->client[j];
      if( cl_sp && (cl_sp->common.clid != QMI_SVC_CLID_UNUSED) && 
          cl_sp->report_status.report_power_state )
      {
        /*-------------------------------------------------------------------
          If event mask is 0x01 or 0x03 => power state has changed so 
          generate indicatation without checking anything else 

          If event mask is 0x02 check battery level and decide whether to 
          generate ind or not as below 
        
          ->on lower limit side:
          If the current bat lvl is <= the client's lower limit and the 
          client's lower limit is < previous battery level when this
          function was called then generate the ind

            ---|-----|-------|-----------
               L1    L2      L3
               20    25      35
                   (curr)  (prev level)

         ->on upper limit side: similar to above 
        -------------------------------------------------------------------*/

        /*------------------------------------------------------------------- 
          If the event mask indicates power state change, send event report 
          ind if the current power state is not equal to the last reported
        -------------------------------------------------------------------*/   
        if((event_mask & DMSI_EVENT_MASK_POWER_STATE) && 
           (power_state_mask !=
            cl_sp->report_status.last_power_state_reported)) 
        {
          send_event_report_ind = TRUE;         
        }
          
        //or else event mask is battery level, hence check levels   
        //If first time going under lower limit             
        if((battery_level <= cl_sp->report_status.bat_lvl_lower_limit) && 
             ((cl_sp->report_status.bat_lvl_lower_limit < cl_sp->report_status.last_bat_lvl_reported) ||
              (cl_sp->report_status.last_bat_lvl_reported == DMSI_BAT_LVL_INACTIVE)))
        {
          send_event_report_ind = TRUE;
        }
       
        if((battery_level >= cl_sp->report_status.bat_lvl_upper_limit) && 
             ((cl_sp->report_status.bat_lvl_upper_limit > cl_sp->report_status.last_bat_lvl_reported) ||
              (cl_sp->report_status.last_bat_lvl_reported == DMSI_BAT_LVL_INACTIVE))) 
        {
          send_event_report_ind = TRUE;
        }
       
        if(send_event_report_ind == TRUE)
        {
          LOG_MSG_INFO2_3("Sending DMS IND to client: %d, power state: %d, bat lvl: %d",
                          cl_sp->common.clid, power_state_mask, battery_level);

          qmi_dmsi_event_report_ind(dms_sp,
                                    cl_sp->common.clid,
                                    DMSI_REPORT_STATUS_EV_POWER_STATE,
                                    power_state_mask,
                                    battery_level,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0);

          cl_sp->report_status.last_bat_lvl_reported     = battery_level;
          cl_sp->report_status.last_power_state_reported = power_state_mask;
          send_event_report_ind = FALSE;
        }
      }
    }

  /*-------------------------------------------------------------------------
    Determine the new high and low battery limits and set them in the 
    charger module
  -------------------------------------------------------------------------*/
  qmi_dmsi_set_battery_level_limits(battery_level);            

} /* qmi_dms_charger_event_occured */

/*===========================================================================
  FUNCTION QMI_DMS_GENERATE_OPRT_MODE_IND()

  DESCRIPTION
    Called when CM notifies us that the operating mode has changed.  Sends
    an indication with the new operating mode to clients registered for
    the event.
        
  PARAMETERS
    new_oprt_mode : new operating mode (if this is LPM, QMI DMS will
                    determine if it is persistent LPM on its own)
    
  RETURN VALUE
    None
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void qmi_dms_generate_oprt_mode_ind
(
  sys_oprt_mode_e_type  new_cm_oprt_mode
)
{
  qmi_dmsi_state_type *           dms_sp;
  qmi_dmsi_client_state_type *    cl_sp;
  int                             j;
  qmi_dmsi_oprt_mode_e_type       new_dms_oprt_mode;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  dms_sp = (qmi_dmsi_state_type*)&qmi_dms_state;

  new_dms_oprt_mode = DMSI_OPRT_MODE_ONLINE;

  if (QMI_ERR_NONE != qmi_dmsi_oprt_mode_from_cm(new_cm_oprt_mode,
                                                 &new_dms_oprt_mode))
  {
    return;
  }

    for( j = 0; j < DMSI_MAX_CLIDS ; j++ )
    {
      cl_sp = dms_sp->client[j];
      if( cl_sp && (cl_sp->common.clid != QMI_SVC_CLID_UNUSED) && 
          cl_sp->report_status.report_operating_mode)
      {
        qmi_dmsi_event_report_ind(dms_sp,
                                  cl_sp->common.clid,
                                  DMSI_REPORT_STATUS_EV_OPRT_MODE,
                                  0,
                                  0,
                                  0,
                                  0,
                                  (uint8) new_dms_oprt_mode,
                                  0,
                                  0);
      }
    }

} /* qmi_dms_generate_oprt_mode_ind() */

/*===========================================================================
  FUNCTION QMI_DMS_GENERATE_SET_OPRT_MODE_RESP()

  DESCRIPTION
    Called when cmd_cb() supplied in cm_ph_cmd_set_oprt_mode() is called 
    by CM. Sends response to control point for the corresponding
    Set Operating Mode Reqs
        
  PARAMETERS
    user_data   : user data
    ph_cmd_err  : error type
    
  RETURN VALUE
    None
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void qmi_dms_generate_set_oprt_mode_resp
(
  void *               user_data,
  cm_ph_cmd_err_e_type ph_cmd_err
)
{
  dsm_item_type *        response;
  qmi_cmd_buf_type * cmd_buf_p;
  qmi_dmsi_client_state_type *  cl_sp;
  qmi_error_e_type       errval;
  qmi_result_e_type      result;
  boolean                retval;
  ds_qmi_fw_async_cmd_buf_type *qmi_resp_buf_p;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  errval = QMI_ERR_NONE;
  
  qmi_resp_buf_p = (ds_qmi_fw_async_cmd_buf_type*)(user_data);

  if(qmi_resp_buf_p == NULL )
  {
    LOG_MSG_INFO1_0("Invalid resp_buf_ptr");
    return;
  }
  
  if ( qmi_dms_check_valid_async_cmd_buf(qmi_resp_buf_p) == FALSE )
  {
    LOG_MSG_INFO1_0("Invalid resp_buf_ptr");
    ds_qmi_fw_free_async_cmd_buf(qmi_resp_buf_p);
    return;
  }
  
  cmd_buf_p = (qmi_cmd_buf_type*)(qmi_resp_buf_p->cmd_buf);

  LOG_MSG_INFO1_2("In generate set oprt mode response 0x%x 0x%x",user_data,cmd_buf_p);

  if(NULL == cmd_buf_p || NULL == cmd_buf_p->x_p || NULL == cmd_buf_p->x_p->cl_sp )
  {
    LOG_MSG_INFO1_0("Invalid Transaction ptr or client state ptr, most likely client was released");
    ds_qmi_fw_free_async_cmd_buf(qmi_resp_buf_p);
    return;
  }

  cl_sp = (qmi_dmsi_client_state_type *)cmd_buf_p->x_p->cl_sp;

  /*-------------------------------------------------------------------------
    Send SUCCESS or FAILURE response to control point based on ph_cmd_err
  -------------------------------------------------------------------------*/
  switch (ph_cmd_err)
  {
    case CM_PH_CMD_ERR_NOERR:
      errval = QMI_ERR_NONE;
      break;

    case CM_PH_CMD_ERR_NO_BUF_L:
      errval = QMI_ERR_NO_MEMORY;
      break;

    case CM_PH_CMD_ERR_IN_USE_S:
      errval = QMI_ERR_DEVICE_IN_USE;
      break;

    case CM_PH_CMD_ERR_OPRT_MODE_S:
      errval = QMI_ERR_INVALID_TRANSITION; //error_code
      break;

    case CM_PH_CMD_ERR_RF_NOT_READY:
      errval = QMI_ERR_DEVICE_NOT_READY; //Temporary Error
      break;

    default:
      errval = QMI_ERR_INTERNAL;
      break;
  }
  

  if ( (errval != QMI_ERR_NONE ) || (cl_sp == NULL ))
  {
     LOG_MSG_INFO1_1("Error %d or Invalid Client Pointer ", errval);
     goto send_result;
  }
  
send_result: 
  response = NULL;
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  if ( cl_sp != NULL )
  {
    cl_sp->pending_oprt_mode = SYS_OPRT_MODE_NONE;
  }
  if(FALSE == retval)
  {
    dsm_free_packet(&response);
    ds_qmi_fw_free_cmd_buf( &cmd_buf_p );
    ds_qmi_fw_free_async_cmd_buf(qmi_resp_buf_p);
    return;
  }

  if ( FALSE == qmi_dmsi_send_response( cmd_buf_p, response ) )
  {
    LOG_MSG_INFO2_0 ("Unable to send Set Operating Mode response");
    // verify if free packet and cmd buf needed here
    dsm_free_packet(&response);
    ds_qmi_fw_free_cmd_buf(&cmd_buf_p);
  }
  ds_qmi_fw_free_async_cmd_buf(qmi_resp_buf_p);
} /* qmi_dms_generate_set_oprt_mode_resp() */

#ifdef FEATURE_DATA_QMI_ADDENDUM

/*===========================================================================
  FUNCTION QMI_DMS_GENERATE_WD_STATE_IND()

  DESCRIPTION
    Called when the wireless disable switch has been toggled.  Sends
    an indication with the new switch state to clients registered for
    the event.
        
  PARAMETERS
    new_wd_state : new state of the wireless disable switch
    
  RETURN VALUE
    None
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void qmi_dms_generate_wd_state_ind
(
  boolean  new_wd_state
)
{
  qmi_dmsi_state_type *           dms_sp;
  qmi_dmsi_client_state_type *    cl_sp;
  int                             qmi_inst;
  int                             j;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  dms_sp = (qmi_dmsi_state_type *)&qmi_dms_state;

    for( j = 0; j < DMSI_MAX_CLIDS ; j++ )
    {
      cl_sp = dms_sp->client[j];
      if( cl_sp && (cl_sp->common.clid != QMI_SVC_CLID_UNUSED) && 
          cl_sp->report_status.report_wd_state)
      {
        qmi_dmsi_event_report_ind(dms_sp,
                                  cl_sp->common.clid,
                                  DMSI_REPORT_STATUS_EV_WD_STATE,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  new_wd_state);
      }
    }

} /* qmi_dms_generate_wd_state_ind() */
#endif /* FEATURE_DATA_QMI_ADDENDUM */

/*===========================================================================
  FUNCTION QMI_DMS_GENERATE_CDMA_LOCK_MODE_IND()

  DESCRIPTION
    Called when the cdma lock mode status changes.  Sends
    an indication with the new cdma lock mode state to clients registered for
    the event.
        
  PARAMETERS
    new_cdma_lock_mode : new state of the cdma lock mode
    
  RETURN VALUE
    None
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void qmi_dms_generate_cdma_lock_mode_ind
(
  boolean  new_cdma_lock_mode
)
{
  qmi_dmsi_state_type *           dms_sp;
  qmi_dmsi_client_state_type *    cl_sp;
  int                             itr_clids;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_INFO2_1("qmi_dms_generate_cdma_lock_mode_ind()function entry"
                  "new_cdma_lock_mode %d", new_cdma_lock_mode);

  dms_sp = (qmi_dmsi_state_type *)&qmi_dms_state;

  /* Update the global variable with the new value */
  qmi_dmsi_global.cdma_lock_mode = new_cdma_lock_mode;

  /* Send the indication for all the registered clients */
  for( itr_clids = 0; itr_clids < DMSI_MAX_CLIDS ; itr_clids++ )
  {
    cl_sp = dms_sp->client[itr_clids];
    if( cl_sp && (cl_sp->common.clid != QMI_SVC_CLID_UNUSED) && 
        cl_sp->report_status.report_cdma_lock_mode)
    {
      qmi_dmsi_event_report_ind(dms_sp,
                                cl_sp->common.clid,
                                DMSI_REPORT_STATUS_EV_CDMA_LOCK_MODE,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                new_cdma_lock_mode);
    }
  }
} /* qmi_dms_generate_cdma_lock_mode_ind */

#ifdef FEATURE_TDSCDMA
/*===========================================================================
FUNCTION QMI_DMS_TDSCDMA_SET_CONFIG_RES_CB()

  DESCRIPTION
  Function gets called from Message Router (MSGRT) to post the command with
  the required value.
  
  PARAMETERS
  set_resp : message posted by MSGRT to the QMI

  RETURN VALUE
  None
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void qmi_dms_tdscdma_set_config_res_cb
(
  void *set_resp
)
{
  qmi_dmsi_cmd_buf_type *cmd_ptr;

  tds_rrc_set_config_rsp_type *set_resp_val;
  set_resp_val = (tds_rrc_set_config_rsp_type *)set_resp;

  LOG_MSG_INFO1_0("qmi_dms_tdscdma_set_config_res_cb(): "
                  "dms SET TDSCDMA config res cb");

  cmd_ptr = (qmi_dmsi_cmd_buf_type *)qmi_dmsi_get_cmd_buf(QMI_CMD_TDSCDMA_SET_CONFIG_RES);
  if( cmd_ptr == NULL)
  {
    return;
  }
  
  memset(cmd_ptr, 0, sizeof(qmi_dmsi_cmd_buf_type));
  cmd_ptr->cmd_id = QMI_CMD_TDSCDMA_SET_CONFIG_RES;
  cmd_ptr->data.tdscdma_set_config_res.status = set_resp_val->status;

  dcc_send_cmd_ex(DCC_QMI_CMD, cmd_ptr);

} /* qmi_dms_tdscdma_set_config_res_cb() */

/*===========================================================================
FUNCTION QMI_DMS_TDSCDMA_GET_CONFIG_RES_CB()

  DESCRIPTION
  Function gets called from Message Router (MSGRT) to post the command with
  the required value.
  
  PARAMETERS
  get_resp : message posted by MSGRT

  RETURN VALUE
    None
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void qmi_dms_tdscdma_get_config_res_cb
(
  void *get_resp
)
{
  qmi_dmsi_cmd_buf_type *cmd_ptr;

  tds_rrc_get_config_rsp_type *get_resp_val;
  get_resp_val = (tds_rrc_get_config_rsp_type *)get_resp;

  LOG_MSG_INFO1_0("qmi_dms_tdscdma_get_config_res_cb(): "
                  "dms GET TDSCDMA config res cb");

  cmd_ptr = (qmi_dmsi_cmd_buf_type *)qmi_dmsi_get_cmd_buf(QMI_CMD_TDSCDMA_GET_CONFIG_RES);
  if( cmd_ptr == NULL)
  {
    return;
  }

  memset(cmd_ptr, 0, sizeof(qmi_dmsi_cmd_buf_type));
  cmd_ptr->cmd_id = QMI_CMD_TDSCDMA_GET_CONFIG_RES;
  cmd_ptr->data.tdscdma_get_config_res.active_config = get_resp_val->active_config;
  cmd_ptr->data.tdscdma_get_config_res.desired_config = get_resp_val->desired_config;

  dcc_send_cmd_ex(DCC_QMI_CMD, cmd_ptr);

} /* qmi_dms_tdscdma_get_config_res_cb() */
#endif

/*===========================================================================

                             INTERNAL FUNCTIONS

===========================================================================*/

/*===========================================================================
  FUNCTION QMI_DMSI_RESET()

  DESCRIPTION
    Reset the issuing DMS client's state

  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response or NULL if none.
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_reset
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *              response;
  boolean                      retval;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  /*-------------------------------------------------------------------------
    Reset the client's state
  -------------------------------------------------------------------------*/
  qmi_dmsi_reset_client(cl_sp);

  /*-------------------------------------------------------------------------
    Build the response from back to front
    No error possible in reset, so result is success
  -------------------------------------------------------------------------*/
  response = NULL;

  retval = qmi_svc_put_result_tlv(&response,
                                  QMI_RESULT_SUCCESS,
                                  QMI_ERR_NONE); 
  CHECK_RETVAL();

  return response;

} /* qmi_dmsi_reset() */

/*===========================================================================
FUNCTION  QMI_DMSI_CM_PH_CB

DESCRIPTION
  CM phone command callback function

DEPENDENCIES
  None

RETURNS
  None

SIDE EFFECTS
  Adds command in DS command buffer

===========================================================================*/
/* ARGSUSED */
  static void qmi_dmsi_cm_ph_cb 
(
  void                         *data_ptr,         /* Data block pointer    */
  cm_ph_cmd_e_type              ph_cmd,              /* Command ID            */
  cm_ph_cmd_err_e_type          ph_cmd_err           /* Command error code    */
)
{
  qmi_dmsi_cmd_buf_type *  cmd_ptr;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /*-------------------------------------------------------------------------
    Post a cmd to DCC to handle this in DCC context
  -------------------------------------------------------------------------*/
  cmd_ptr = (qmi_dmsi_cmd_buf_type *)qmi_dmsi_get_cmd_buf(QMI_CMD_SET_OPERATING_MODE_RESP);
  if( cmd_ptr != NULL)
  {
    switch(ph_cmd)
    {
      case CM_PH_CMD_OPRT_MODE:
        cmd_ptr->cmd_id = QMI_CMD_SET_OPERATING_MODE_RESP;
        break;

      default:
        LOG_MSG_INFO2_0("Unexpected ph_cmd received in qmi_dmsi_cm_ph_cb");
    }

    // Fill in the QMI CMD with relevant DMS related params  
    cmd_ptr->data.cm_ph_err.user_data = data_ptr;
    cmd_ptr->data.cm_ph_err.err_type = ph_cmd_err;
    dcc_send_cmd_ex(DCC_QMI_CMD, cmd_ptr);
  }
  return;
} /* qmi_dmsi_cm_ph_cb */

/*===========================================================================
  FUNCTION QMI_DMSI_GET_DEVICE_CAP()

  DESCRIPTION
    Gives the device capabilities
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_get_device_cap
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  struct
  {
    PACKED struct PACKED_POST
    {
      uint32  max_tx_channel_rate;
      uint32  max_rx_channel_rate;
      uint8   voice_capability;
      uint8   sim_capability;
      uint8   radio_if_list_cnt;
    } fixed;
    uint8 radio_if_list[QMI_N_RADIO_IFS]; 
  } v_required;
  
  struct PACKED_POST
  {
    uint64 voice_support_capability;
  } v_opt_voice_support_cap;

  struct PACKED_POST
  {
    uint64 simul_voice_and_data_capability;
  } v_opt_simul_voice_and_data_cap;

  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;
  uint32 max_tx_channel_rate;
  uint32 max_rx_channel_rate;
  uint32 device_service_capability;  
  uint64 sglte_info; 
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);
  
  response = NULL;
  errval = QMI_ERR_NONE;

  /*-------------------------------------------------------------------------
    Get the device multisim info
  -------------------------------------------------------------------------*/
  errval = qmi_dmsi_get_multisim_info(&response, DMSI_PRM_TYPE_MULTI_SIM_CAPABILITY);
  if(errval != QMI_ERR_NONE)
  {
    goto send_result;
  }

   /*do call and fill the 3 tlvs requied for this*/
  errval = qmi_dmsi_get_device_multisim_current_cap_info(&response, QMI_DMS_DEVICE_CAP_RESP);
  if(errval != QMI_ERR_NONE)
  {
    goto send_result;
  }

  /*-------------------------------------------------------------------------
    Get the device feature mode 
  -------------------------------------------------------------------------*/
  errval = qmi_dmsi_get_subs_device_feature_mode(&response);
  if(errval != QMI_ERR_NONE)
  {
    goto send_result;
  }

  /*-------------------------------------------------------------------------
    Get the Max active subs
  -------------------------------------------------------------------------*/
  errval = qmi_dmsi_get_max_active_data_subs(&response, DMSI_PRM_TYPE_MAX_DATA_SUBS);
  if(errval != QMI_ERR_NONE)
  {
    goto send_result;
  }

  /*-------------------------------------------------------------------------
    Get the list of supported radio ifs
  -------------------------------------------------------------------------*/
  v_required.fixed.radio_if_list_cnt = 
    qmi_dmsi_get_hw_supported_radio_if_list( v_required.radio_if_list );
  
  /*-------------------------------------------------------------------------                                           
    Get the max TX and RX channel rates
  -------------------------------------------------------------------------*/
  errval = qmi_dmsi_get_max_channel_rate(&max_rx_channel_rate,
                                         &max_tx_channel_rate);
  if(errval != QMI_ERR_NONE)
  {
    goto send_result;
  }
  v_required.fixed.max_tx_channel_rate = max_tx_channel_rate;
  v_required.fixed.max_rx_channel_rate = max_rx_channel_rate;

  /*-------------------------------------------------------------------------
    Get the voice capability of the device
  -------------------------------------------------------------------------*/
  v_required.fixed.voice_capability = qmi_dmsi_get_voice_capability();

  /*-------------------------------------------------------------------------
    Get the SIM capability of the device
  -------------------------------------------------------------------------*/
  v_required.fixed.sim_capability = qmi_dmsi_get_sim_capability();
  
  /*-------------------------------------------------------------------------
    Get the Simultaneous Voice and Data capability of the device
  -------------------------------------------------------------------------*/
  v_opt_simul_voice_and_data_cap.simul_voice_and_data_capability =
    qmi_dmsi_get_simul_voice_and_data_capability();

  if(FALSE == qmi_svc_put_param_tlv(&response, 
                                    DMSI_PRM_TYPE_DEVICE_CAP_SIMUL_VOICE_AND_DATA,
                                    sizeof(v_opt_simul_voice_and_data_cap),
                                    &v_opt_simul_voice_and_data_cap))
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
  }
  
  /*-------------------------------------------------------------------------
    Get the Voice Support capability of the device
  -------------------------------------------------------------------------*/
  v_opt_voice_support_cap.voice_support_capability =
    qmi_dmsi_get_voice_support_capability();

  if(FALSE == qmi_svc_put_param_tlv(&response, 
                                    DMSI_PRM_TYPE_DEVICE_CAP_VOICE_SUPPORT,
                                    sizeof(v_opt_voice_support_cap),
                                    &v_opt_voice_support_cap))
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
  }

  sglte_info = v_opt_simul_voice_and_data_cap.simul_voice_and_data_capability;
  /*-------------------------------------------------------------------------
    Get the Device Service capability of the device
  -------------------------------------------------------------------------*/
  device_service_capability =
    (sglte_info & DMSI_MASK_SIMUL_VOICE_AND_DATA_SGLTE_CAPABLE)?DMSI_SIM_VOICE_DATA:
     v_required.fixed.voice_capability;

   if(FALSE == qmi_svc_put_param_tlv(&response, 
                                    DMSI_PRM_TYPE_DEVICE_SERVICE_CAPABILITY,
                                    sizeof(device_service_capability),
                                    &device_service_capability))
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
  }

  v_required.fixed.voice_capability = 
    (sglte_info & DMSI_MASK_SIMUL_VOICE_AND_DATA_SGLTE_CAPABLE)?DMSI_SIM_VOICE_DATA:
       v_required.fixed.voice_capability;
  if(FALSE == qmi_svc_put_param_tlv(&response, 
                                    QMI_TYPE_REQUIRED_PARAMETERS,
                                    sizeof (v_required.fixed) + 
                                    v_required.fixed.radio_if_list_cnt,
                                    &v_required))
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
  }

send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);
  retval = qmi_svc_put_result_tlv( &response, result, errval );
  CHECK_RETVAL();
  
  return response;

} /* qmi_dmsi_get_device_cap() */



/*===========================================================================
  FUNCTION QMI_DMSI_GET_DEVICE_MFR()

  DESCRIPTION
    Gives the name of the device manufacturer.
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_get_device_mfr
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  char *             mfr_info;
  int                mfr_info_len;
  uint16             len;
  
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

  nv_stat_enum_type  nv_status;
  nv_item_type      *dcc_nv_item_ptr;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);
  
  response = NULL;
  errval = QMI_ERR_NONE;

  /*-------------------------------------------------------------------------
    Allocate temporary memory for the NV item
  -------------------------------------------------------------------------*/
  dcc_nv_item_ptr = (nv_item_type *)
    qmi_svc_ps_system_heap_mem_alloc(sizeof(nv_item_type),
                      FILE_ID_DS_QMI_DMS,__LINE__);
  if( dcc_nv_item_ptr == NULL )
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
    goto send_result;
  }
  
  /*-------------------------------------------------------------------------
    Query NV for Model Identification
  -------------------------------------------------------------------------*/
  nv_status = dcc_get_nv_item(NV_UNDP_HSU_MFGSTR_I, dcc_nv_item_ptr);
  if ((nv_status == NV_DONE_S) && 
                        strlen((char *)dcc_nv_item_ptr->undp_hsu_mfgstr))
  {
    mfr_info =(char *) dcc_nv_item_ptr->undp_hsu_mfgstr;
  } 
  else
  {
    mfr_info = qmi_device_mfr_str;
  }
  mfr_info_len = strlen(mfr_info);
  len = (uint16)MIN(65535, mfr_info_len);

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  if(FALSE == qmi_svc_put_param_tlv(&response, 
                                    QMI_TYPE_REQUIRED_PARAMETERS,
                                    len,
                                    mfr_info))
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
  }

  /*-------------------------------------------------------------------------
    Free the temporary memory allocated for NV item.
  -------------------------------------------------------------------------*/
  PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_item_ptr);
  
send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
      
  return response;
  
} /* qmi_dmsi_get_device_mfr() */



/*===========================================================================
  FUNCTION QMI_DMSI_GET_DEVICE_MODEL_ID()

  DESCRIPTION
    Gives the model ID of the device in use
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_get_device_model_id
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  char               model_id_info[DMSI_MODEL_ID_SIZ];
  nv_stat_enum_type  nv_status;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;
  nv_item_type      *dcc_nv_item_ptr;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  response = NULL;
  errval = QMI_ERR_NONE;
  memset(model_id_info, 0, sizeof(char)*DMSI_MODEL_ID_SIZ);

  /*-------------------------------------------------------------------------
    Allocate temporary memory for the NV item
  -------------------------------------------------------------------------*/
  dcc_nv_item_ptr = (nv_item_type *)
    qmi_svc_ps_system_heap_mem_alloc(sizeof(nv_item_type),
                    FILE_ID_DS_QMI_DMS,__LINE__);
  if( dcc_nv_item_ptr == NULL )
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
    goto send_result;
  }

  /*-------------------------------------------------------------------------
    Query NV for Model Identification
  -------------------------------------------------------------------------*/
  nv_status = dcc_get_nv_item(NV_UNDP_HSU_PRODSTR_I, dcc_nv_item_ptr);
  if( nv_status == NV_DONE_S )
  {
    (void) strlcpy((char*)model_id_info,
                       (const char*)dcc_nv_item_ptr->undp_hsu_prodstr,
                       DMSI_MODEL_ID_SIZ);
  }
  if ((nv_status != NV_DONE_S) || (0 == strlen(model_id_info)))
  {
    nv_status = dcc_get_nv_item(NV_MOB_MODEL_I, dcc_nv_item_ptr);
    if(nv_status == NV_DONE_S)
    {
      (void) dsatutil_itoa(dcc_nv_item_ptr->mob_model, 
                           (unsigned char*)model_id_info,
                           DMSI_BASE_DECIMAL);
    }
    else
    {
      /*-----------------------------------------------------------------------
        Model not found in NV, hence using the global Model variable
      -----------------------------------------------------------------------*/  
      (void) dsatutil_itoa(mob_model,
                           (unsigned char*)model_id_info, 
                           DMSI_BASE_DECIMAL);
    }
  }

  /*-----------------------------------------------------------------------
    Make sure that the string is NULL terminated so that strlen will work
  -----------------------------------------------------------------------*/  
  model_id_info[DMSI_MODEL_ID_SIZ-1] = '\0';

  if( FALSE == qmi_svc_put_param_tlv(&response, 
                                     QMI_TYPE_REQUIRED_PARAMETERS,
                                     (uint16) strlen(model_id_info),
                                     model_id_info ) )
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
  }

  /*-------------------------------------------------------------------------
    Free the temporary memory allocated for NV item.
  -------------------------------------------------------------------------*/
  PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_item_ptr);

send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  
  return response;
 
} /* qmi_dmsi_get_device_model_id() */



/*===========================================================================
  FUNCTION QMI_DMSI_GET_DEVICE_REV_ID()

  DESCRIPTION
    Gives the revision ID of the device in use
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_get_device_rev_id
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
/* boot_block_version isn't defined for NAND-based (6k and later) targets.
 * We need to send the default NOR boot block version in our response, for
 * backward compatibility. */
#define QMI_BOOT_BLOCK_VERSION 0x1

  dsm_item_type *    response;
  char               dev_rev_id_info[DMSI_REV_ID_SIZ];
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;
  
#ifdef FEATURE_DATA_QMI_ADDENDUM
  uint32                uqcn_ver;
  char                  pri_ver_str[sizeof(uqcn_ver)*2 + 1];
  char                  saved_boot_ver[GOBI_IM_BUILD_ID_LEN];
  gobi_nv_status_e_type gobi_nv_status;
#endif /* FEATURE_DATA_QMI_ADDENDUM */
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);
  
  response = NULL;
  errval = QMI_ERR_NONE;

#ifdef FEATURE_DATA_QMI_ADDENDUM
  /*-------------------------------------------------------------------------
    Fetch the PRI version
  -------------------------------------------------------------------------*/
  uqcn_ver = qmi_dmsi_get_uqcn_version();
  (void) snprintf(pri_ver_str, sizeof(pri_ver_str), "%08x", uqcn_ver);

  if (strlen(pri_ver_str))
  {
    if (FALSE == qmi_svc_put_param_tlv(&response,
                                       DMSI_PRM_TYPE_PRI_REV,
                                       (uint16) strlen(pri_ver_str),
                                       pri_ver_str))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
      goto send_result;
    }
  }

  /*-------------------------------------------------------------------------
    Fetch the boot version
  -------------------------------------------------------------------------*/
  gobi_nv_status = gobi_nv_read(GOBI_NV_ITEM_BOOT_SW_VER, 0, saved_boot_ver, 
                           sizeof(saved_boot_ver));

  if(GOBI_NV_STATUS_OK != gobi_nv_status )
  {
    LOG_MSG_ERROR_1("Boot ver nv read error %d", gobi_nv_status);
  }
  
  if (FALSE == qmi_svc_put_param_tlv(&response,
                                     DMSI_PRM_TYPE_BOOT_VER,
                                     (uint16) strlen(saved_boot_ver),
                                     saved_boot_ver))
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
    goto send_result;
  }
#endif /* FEATURE_DATA_QMI_ADDENDUM */

  memset(dev_rev_id_info, 0, sizeof(dev_rev_id_info));
  /*-------------------------------------------------------------------------
    Populate the revision ID info
  -------------------------------------------------------------------------*/
  (void) snprintf (dev_rev_id_info, DMSI_REV_ID_SIZ,
           "%s  %d  [%s %s]",
           mob_sw_rev,
           QMI_BOOT_BLOCK_VERSION,
           rel_date,
           rel_time);

  dev_rev_id_info[DMSI_REV_ID_SIZ - 1] = '\0'; //  Null terminate at max len-1

  if( FALSE == qmi_svc_put_param_tlv(&response, 
                                     QMI_TYPE_REQUIRED_PARAMETERS,
                                     (uint16) strlen(dev_rev_id_info),
                                     dev_rev_id_info))
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
    goto send_result;
  }

send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);
  retval = qmi_svc_put_result_tlv( &response, result, errval );
  CHECK_RETVAL();
   
  return response;
} /* qmi_dmsi_get_device_rev_id() */


/*===========================================================================
  FUNCTION QMI_DMSI_GET_MSISDN()

  DESCRIPTION
    Gives the msisdn information(MDN) of the device
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_get_msisdn
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *         response;
  qmi_error_e_type        errval;
  qmi_result_e_type       result;
  boolean                 retval;

#if defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900)
  char                    mob_num[NV_DIR_NUMB_PCS_SIZ + 1];
  char                    min_num[NV_DIR_NUMB_PCS_SIZ+1];
#endif /* defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900) */

#ifdef FEATURE_MMGSDI_SESSION_LIB
  mmgsdi_return_enum_type mmgsdi_status;
  mmgsdi_access_type      sim_filename;
  qmi_if_info_type       *qmi_if_info_ptr;
  qmi_dmsi_client_state_type * dms_cl_sp;
  ds_qmi_fw_async_cmd_buf_type  *async_cmd_buf;
  mmgsdi_session_type_enum_type session_type;
  qmi_if_mmgsd_session_e_type session_index;
#endif /* FEATURE_MMGSDI_SESSION_LIB */

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  response = NULL;
  errval = QMI_ERR_NONE;

#ifdef FEATURE_MMGSDI_SESSION_LIB
  dms_cl_sp = (qmi_dmsi_client_state_type*)cl_sp;
  qmi_if_info_ptr = qmi_if_info_get_ptr();
  /* check that returning IMSI is allowed and that the UIM is ready.  if not,
     ignore the IMSI since it is optional. */
  
  session_type = MMGSDI_GW_PROV_PRI_SESSION;
  session_index = QMI_IF_MMGSD_GW_PROV_PRI_SESSION;

#ifdef FEATURE_DUAL_SIM  
  if(dms_cl_sp->subscription_id == DMSI_BIND_SUBS_SECONDARY) 
  {
    session_type = MMGSDI_GW_PROV_SEC_SESSION;
    session_index = QMI_IF_MMGSD_GW_PROV_SEC_SESSION;
  }
#endif
#ifdef FEATURE_TRIPLE_SIM
  else if(dms_cl_sp->subscription_id == DMSI_BIND_SUBS_TERTIARY)
  {
    session_type = MMGSDI_GW_PROV_TER_SESSION;
    session_index = QMI_IF_MMGSD_GW_PROV_TER_SESSION;
  }
#endif
    
  errval = qmi_dms_get_uim_access_status_ext(session_type);
  if (errval != QMI_ERR_NONE)
  {
    if(dms_cl_sp->subscription_id == DMSI_BIND_SUBS_PRIMARY)
    { 
      session_type = MMGSDI_1X_PROV_PRI_SESSION;
      session_index = QMI_IF_MMGSD_1X_PROV_PRI_SESSION;
      if ((errval = qmi_dms_get_uim_access_status_ext(session_type)) != QMI_ERR_NONE)
      {
        LOG_MSG_ERROR_0("Primary Subscription's SIM is not initialized");
        goto send_result;
      }
    }
    else
    {
      goto send_result;
    }
  }
    
 /* Read MDN and MIN if this is 1x primary session */
 if(session_type == MMGSDI_1X_PROV_PRI_SESSION)
 { 

#if defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900)
    memset(mob_num, 0, sizeof(mob_num));
    memset(min_num, 0, sizeof(min_num));
  
    errval = qmi_dmsi_get_mdn_min(mob_num, sizeof(mob_num),
                                  min_num, sizeof(min_num));
    
    /* add the MIN anytime it is present */
    if (errval == QMI_ERR_NONE)
    {
      if (strlen(min_num))
      {
        if (FALSE == qmi_svc_put_param_tlv(&response, 
                                           DMSI_PRM_TYPE_MIN,
                                           (uint16) strlen(min_num),
                                           min_num))
        {
          errval = QMI_ERR_NO_MEMORY;
          dsm_free_packet(&response);
        }
      }
    }  
    /* qmi_dmsi_get_mdn_min() above checks for NOT_PROVISIONED case and sets
       errval correctly, no reason to check it again*/
    if (errval == QMI_ERR_NONE)
    {
      if (FALSE == qmi_svc_put_param_tlv(&response, 
                                        QMI_TYPE_REQUIRED_PARAMETERS,
                                        (uint16) strlen(mob_num),
                                        mob_num))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }
#endif /* defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900) */ 
  }
  else
  {
    sim_filename.access_method = MMGSDI_EF_ENUM_ACCESS;
    sim_filename.file.file_enum = MMGSDI_NO_FILE_ENUM;
    
  /* GET IMSI, Request goes to MMGSDI, then MSISDN is read */
  if (!qmi_dmsi_global.nv_restrict_imsi_access)
  {    
    if (qmi_if_info_ptr->session_info[session_index].app_type == MMGSDI_APP_SIM)
    {
      sim_filename.file.file_enum = MMGSDI_GSM_IMSI;
    }
    else if (qmi_if_info_ptr->session_info[session_index].app_type == MMGSDI_APP_USIM)
    {
      sim_filename.file.file_enum = MMGSDI_USIM_IMSI;
    }
    else
    {
      LOG_MSG_ERROR_0("MMGSDI apps type is not supported");
    }

    if(sim_filename.file.file_enum != MMGSDI_NO_FILE_ENUM)
    {
      async_cmd_buf = ds_qmi_fw_new_async_cmd_buf(cmd_buf_p, dms_cl_sp->cookie);
      if(async_cmd_buf == NULL)
      {
        errval = QMI_ERR_NO_MEMORY;
        goto send_result;
      }
      
      ds_qmi_cflog_amss_call("mmgsdi_session_read_transparent()");
      mmgsdi_status = mmgsdi_session_read_transparent(
                                        qmi_if_info_ptr->session_info[session_index].session_id,
                                        sim_filename,
                                        0,
                                        DMSI_MMGSDI_IMSI_LEN,
                                        qmi_if_mmgsdi_resp_cback,
                                        (uint32) async_cmd_buf);

      if(mmgsdi_status == MMGSDI_SUCCESS)
      {
        /* IMSI will be reported back later from MMGSDI, other items will be 
        processed at that time */
        return QMI_SVC_RESPONSE_PENDING;
      }
      else
      { 
        /* For any reason IMSI cannot be obtained from MMGSDI, it will be
        skipped and the functon will go forward to process other items */
        LOG_MSG_INFO1_1("MMGSDI read IMSI status %d", mmgsdi_status);
        ds_qmi_fw_free_async_cmd_buf(async_cmd_buf);
      }
    }
    else
    {
      LOG_MSG_ERROR_0("MMGSDI filename not found");
    }
  }

  /*going ahead and reading msisdn */
  if (qmi_if_info_ptr->session_info[session_index].app_type == MMGSDI_APP_SIM)
  {
    sim_filename.file.file_enum = MMGSDI_TELECOM_MSISDN;
  }
  else if (qmi_if_info_ptr->session_info[session_index].app_type == MMGSDI_APP_USIM)
  {
    sim_filename.file.file_enum = MMGSDI_USIM_MSISDN;
  }
  else
  {
    errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
    goto send_result;
  }

  async_cmd_buf = ds_qmi_fw_new_async_cmd_buf(cmd_buf_p, dms_cl_sp->cookie);
  if(async_cmd_buf == NULL)
  {
    errval = QMI_ERR_NO_MEMORY;
    goto send_result;
  }
    	
  ds_qmi_cflog_amss_call("mmgsdi_session_read_record()");
  /* Read entire record for EF-MSISDN (x+14), len param = 0 */
  mmgsdi_status = mmgsdi_session_read_record(
                                 qmi_if_info_ptr->session_info[session_index].session_id,
                                 sim_filename,
                                 1,
                                 0,
                                 qmi_if_mmgsdi_resp_cback,
                                 (uint32) async_cmd_buf);

  if(mmgsdi_status == MMGSDI_SUCCESS)
  {
    /* MSISDN will be reported back later from MMGSDI, other items will be
    processed at that time */
    return QMI_SVC_RESPONSE_PENDING;
  }
  else
  {
    /* For any reason MSISDN cannot be obtained from MMGSDI it will be
    skipped and the functon will go forward to process other items */
    LOG_MSG_ERROR_1("MMGSDI read MSISDN status %d", mmgsdi_status);
    ds_qmi_fw_free_async_cmd_buf(async_cmd_buf);
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }
#endif /* FEATURE_MMGSDI_SESSION_LIB */

  /* Explicitly clear any TLV's that may have been added to the response,
     if an error is detected. */
  if (QMI_ERR_NONE != errval)
  {
    dsm_free_packet(&response);
  }
}

#ifdef FEATURE_MMGSDI_SESSION_LIB
send_result:
#endif /* FEATURE_MMGSDI_SESSION_LIB */
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv( &response, result, errval );
  CHECK_RETVAL();
  
  return response;

} /* qmi_dmsi_get_msisdn() */


/*===========================================================================
  FUNCTION QMI_DMSI_GET_DEVICE_SERIAL_NUMBERS()

  DESCRIPTION
    Gives the serial numbers of the device in use
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_get_device_serial_numbers
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
    return qmi_dmsi_get_device_serial_numbers_ext(sp,
		                                  cmd_buf_p,
		                                  cl_sp,
		                                  sdu_in,
		                                  FALSE
		                                  );

} /* qmi_dmsi_get_device_serial_numbers() */

/*===========================================================================
  FUNCTION QMI_DMSI_GET_ENCRYPTED_DEVICE_SERIAL_NUMBERS()

  DESCRIPTION
    Gives the encrypted serial numbers of the device in use
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_get_encrypted_device_serial_numbers
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  return qmi_dmsi_get_device_serial_numbers_ext(sp,
	                                        cmd_buf_p,
	                                        cl_sp,
	                                        sdu_in,
	                                        TRUE
	                                        );

} /* qmi_dmsi_get_encrypted_device_serial_numbers() */


/*===========================================================================
  FUNCTION QMI_DMSI_GET_DEVICE_SERIAL_NUMBERS_EXT()

  DESCRIPTION
    Gives the serial numbers of the device in use
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request
    is_encrypted: request is from encrypted handler or not.

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/

static dsm_item_type* qmi_dmsi_get_device_serial_numbers_ext
(
  void *			  sp,
  void *			  cmd_buf_p,
  void *			  cl_sp,
  dsm_item_type **                sdu_in,
  boolean                         is_encrypted
)
{
  /*Encrypted data would be ~100 bytes more*/
  #define DMSI_ENCRYPTED_SERIAL_NO_SIZ DMSI_SERIAL_NO_SIZ+100
  dsm_item_type *    response;
  uint8              dev_serial_num_info[DMSI_SERIAL_NO_SIZ];
  nv_stat_enum_type  nv_status;
  int                i;

#if defined(FEATURE_MEID_SUPPORT)
  // used for 3GPP2 ESN/MEID
  byte *             end_ptr;
  int                padding;
#endif /* defined(FEATURE_MEID_SUPPORT) */

  // used for 3GPP IMEI
  uint8              imei_ascii[(NV_UE_IMEI_SIZE-1)*2];
  uint8              imei_bcd_len = 0;
  uint8              digit = '\0';

  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;
  nv_item_type      *dcc_nv_item_ptr;
  uint32             encrypted_info_len = 0;
  uint8              dev_serial_num_encrypted_info[DMSI_ENCRYPTED_SERIAL_NO_SIZ];
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);
  
  response = NULL;
  errval = QMI_ERR_NONE;
  nv_status = NV_DONE_S;

  /*-------------------------------------------------------------------------
    Allocate temporary memory for the NV item
  -------------------------------------------------------------------------*/
  dcc_nv_item_ptr = (nv_item_type *)
    qmi_svc_ps_system_heap_mem_alloc(sizeof(nv_item_type),
                          FILE_ID_DS_QMI_DMS,__LINE__);
  if( dcc_nv_item_ptr == NULL )
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
    goto send_result;
  }  

  memset(dev_serial_num_info, 0, sizeof(dev_serial_num_info));
  memset(dev_serial_num_encrypted_info, 0, sizeof(dev_serial_num_encrypted_info));
  nv_status = dcc_get_nv_item_ext( NV_UE_IMEI_I, dcc_nv_item_ptr,(uint16)((qmi_dmsi_client_state_type *)cl_sp)->subscription_id );
  if (nv_status == NV_DONE_S)
  {
    imei_bcd_len = dcc_nv_item_ptr->ue_imei.ue_imei[0];
    if( imei_bcd_len <= (NV_UE_IMEI_SIZE-1) )
    {
      memset(imei_ascii, 0, (NV_UE_IMEI_SIZE-1)*2);
      for( i = 1; i <= imei_bcd_len; i++ )
      {
        digit = QMI_BCD_LOW_DIGIT(dcc_nv_item_ptr->ue_imei.ue_imei[i]);
        if( (digit <= 9) || (i <= 1) )
        {
          imei_ascii[(i-1)*2] = digit + '0';
        }
        else
        {
          imei_ascii[(i-1)*2] = '\0';
          break;
        }
        
        digit = QMI_BCD_HIGH_DIGIT(dcc_nv_item_ptr->ue_imei.ue_imei[i]);
        if( (digit <= 9) || (i <= 1) )
        {
          imei_ascii[((i-1)*2)+1] = digit + '0';
        }
        else
        {
          imei_ascii[((i-1)*2)+1] = '\0';
          break;
        }
      }
      
      /* Skip the first byte because it is just the ID */
      memscpy( (uint8*)dev_serial_num_info, 
               (NV_UE_IMEI_SIZE-1)*2-1,
               imei_ascii + 1, 
               (NV_UE_IMEI_SIZE-1)*2-1 );

      if(is_encrypted == TRUE)
      {
        encrypted_info_len = DMSI_ENCRYPTED_SERIAL_NO_SIZ;
        if (secapi_secure_message(SC_SSID_TZ,     /* TZ as destination end */
                                  SC_CID_DATA_QMI,/* QMI as client ID */
                                  dev_serial_num_info,
                                  (uint32)strlen((const char *)dev_serial_num_info),
                                   dev_serial_num_encrypted_info,
                                  (uint32*)&encrypted_info_len))
       {
         errval = QMI_ERR_ENCODING;
         dsm_free_packet(&response);
         goto send_result;
       }
      }
      if (strlen((const char *)dev_serial_num_info)||strlen((const char *)dev_serial_num_encrypted_info))
      {
        if( FALSE == qmi_svc_put_param_tlv(
                       &response, 
                       DMSI_PRM_TYPE_IMEI,
                       (is_encrypted)?(uint16) encrypted_info_len:(uint16) strlen((const char *)dev_serial_num_info),
                       (is_encrypted)?dev_serial_num_encrypted_info:dev_serial_num_info))
        {
          errval = QMI_ERR_NO_MEMORY;
          dsm_free_packet(&response);
        }
      }
    }
    else
    {
      LOG_MSG_ERROR_0( "Invalid IMEI value from NV" );
    }
  }
  else
  {
    LOG_MSG_ERROR_0( "No IMEI value from NV" );
  }

  /*-------------------------------------------------------------------------
    Report ESN if CDMA is provisioned.
    dsatutil_itoa returns a pointer to the null-terminator in the output
    string.
  -------------------------------------------------------------------------*/
  memset(dev_serial_num_info, 0, sizeof(dev_serial_num_info));
  memset(dev_serial_num_encrypted_info, 0, sizeof(dev_serial_num_encrypted_info));
  
  /*---------------------------------------------------------------------------------
    Here NV_ESN_ME_I is being used instead of NV_ESN_I because value of NV_ESN_I 
    is getting changed based on dfferent UIM cards or when the card is not inserted
    while value of NV_ESN_ME_I remains consistent.
  ----------------------------------------------------------------------------------*/
  nv_status = dcc_get_nv_item_ext (NV_ESN_ME_I, dcc_nv_item_ptr,(uint16)((qmi_dmsi_client_state_type *)cl_sp)->subscription_id);
  if(NV_DONE_S == nv_status)
  {
    (void)dsatutil_itoa(dcc_nv_item_ptr->esn.esn,
                        dev_serial_num_info,
                        DMSI_BASE_HEX);
      if(is_encrypted == TRUE)
      {
        
        encrypted_info_len = DMSI_ENCRYPTED_SERIAL_NO_SIZ;
        if (secapi_secure_message(SC_SSID_TZ,                     /* TZ as destination end */
                                  SC_CID_DATA_QMI,                       /* QMI as client ID */
                                  dev_serial_num_info,
                                  (uint32)strlen((const char *)dev_serial_num_info),
                                  dev_serial_num_encrypted_info,
                                  (uint32*)&encrypted_info_len))
	{
          errval = QMI_ERR_ENCODING;
          dsm_free_packet(&response);
 	  goto send_result;
        }        
      }
      if (strlen((const char *)dev_serial_num_info)||strlen((const char *)dev_serial_num_encrypted_info))
    {
      if(FALSE == qmi_svc_put_param_tlv(
                    &response, 
                    DMSI_PRM_TYPE_ESN,
                       (is_encrypted)?(uint16) encrypted_info_len:(uint16) strlen((const char *)dev_serial_num_info),
                       (is_encrypted)?dev_serial_num_encrypted_info:dev_serial_num_info))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }
  }
  else
  {
    LOG_MSG_INFO2_0("ESN not read from NV");
  }

#if defined(FEATURE_MEID_SUPPORT)
  /*-------------------------------------------------------------------------
    Report MEID if supported
  -------------------------------------------------------------------------*/
  memset(dev_serial_num_info, 0, sizeof(dev_serial_num_info));
  memset(dev_serial_num_encrypted_info, 0, sizeof(dev_serial_num_encrypted_info));

  /*---------------------------------------------------------------------------------
    Here NV_MEID_ME_I is being used instead of NV_MEID_I because value of NV_MEID_I 
    is getting changed based on dfferent UIM cards or when the card is not inserted
    while value of NV_MEID_ME_I remains consistent.
  ----------------------------------------------------------------------------------*/
  nv_status = dcc_get_nv_item_ext (NV_MEID_ME_I, dcc_nv_item_ptr,(uint16)((qmi_dmsi_client_state_type *)cl_sp)->subscription_id);
  if( NV_DONE_S == nv_status )
  {
    /* nv_item_type.meid is a qword, read appropriately and convert to 14 hex
       digits.  6 digits come from the upper long of the qw and the remaining
       8 come from the lower long of the qw */
    /* 6 from the upper long of the qw */
    end_ptr = dsatutil_itoa( qw_hi(dcc_nv_item_ptr->meid),
                             dev_serial_num_info,
                             DMSI_BASE_HEX );
    /* if the string is not 6 char's long, pad the beginning with 0s */
    padding = 6 - strlen((const char *) dev_serial_num_info);
    if (padding > 0)
    {
      for (i = 5; i >= padding; i--)
      {
        dev_serial_num_info[i] = dev_serial_num_info[i-padding];
      }
      for (i = 0; i < padding; i++)
      {
        dev_serial_num_info[i] = '0';
      }
      dev_serial_num_info[6] = '\0';
    }
    else if(padding < 0)
    {
       LOG_MSG_ERROR_1("dev_serial_num_info length is > 6, padding = %d",
                       padding);
    }

    /* get the start of the second half of the string which is where the lower
       long char's start */
    end_ptr = dev_serial_num_info + 6;

    (void) dsatutil_itoa( qw_lo(dcc_nv_item_ptr->meid),
                          end_ptr,
                          DMSI_BASE_HEX );

    /* if the string is not 8 char's long, pad the beginning with 0s */
    padding = 8 - strlen((const char *) end_ptr);
    if (padding)
    {
      for (i = 7; i >= padding; i--)
      {
        end_ptr[i] = end_ptr[i-padding];
      }
      for (i = 0; i < padding; i++)
      {
        end_ptr[i] = '0';
      }
      dev_serial_num_info[14] = '\0';
    }
    if(is_encrypted == TRUE)
    {
      encrypted_info_len = DMSI_ENCRYPTED_SERIAL_NO_SIZ;	  
      if (secapi_secure_message(SC_SSID_TZ,					  /* TZ as destination end */
                         	SC_CID_DATA_QMI, 				  /* QMI as client ID */
                           	dev_serial_num_info,
                           	(uint32)strlen((const char *)dev_serial_num_info),
                           	dev_serial_num_encrypted_info,
                           	(uint32*)&encrypted_info_len))
      {
        errval = QMI_ERR_ENCODING;
        dsm_free_packet(&response);
        goto send_result;
      }
    }
    if (strlen((const char *)dev_serial_num_info) || strlen((const char *)dev_serial_num_encrypted_info))
    {
      if( FALSE == qmi_svc_put_param_tlv(
                     &response, 
                     DMSI_PRM_TYPE_MEID,  
					 (is_encrypted)?(uint16) encrypted_info_len:(uint16) strlen((const char *)dev_serial_num_info),
					 (is_encrypted)?dev_serial_num_encrypted_info:dev_serial_num_info))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }
  }
  else
  {
    LOG_MSG_INFO2_0("MEID not read from NV");
  }
#endif /* defined(FEATURE_MEID_SUPPORT) */
 
  #if defined(FEATURE_WCDMA) || defined(FEATURE_GSM) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA)
  /*-------------------------------------------------------------------------
    Report IMEISV SVN if supported
  -------------------------------------------------------------------------*/
  memset((void *)&dcc_nv_item, 0, sizeof(dcc_nv_item));
  memset(dev_serial_num_info, 0, sizeof(dev_serial_num_info));
  memset(dev_serial_num_encrypted_info, 0, sizeof(dev_serial_num_encrypted_info));
  nv_status = dcc_get_nv_item_ext ( NV_UE_IMEISV_SVN_I , &dcc_nv_item,(uint16)((qmi_dmsi_client_state_type *)cl_sp)->subscription_id);
  if( NV_DONE_S == nv_status )
  {
    (void)dsatutil_itoa(dcc_nv_item.ue_imeisv_svn,
                        dev_serial_num_info,
                        DMSI_BASE_HEX);
     if(is_encrypted == TRUE)
     {
       encrypted_info_len = DMSI_ENCRYPTED_SERIAL_NO_SIZ;
       if (secapi_secure_message(SC_SSID_TZ,					  /* TZ as destination end */
                           	SC_CID_DATA_QMI, 					  /* QMI as client ID */
                           	dev_serial_num_info,
                           	(uint32)strlen((const char *)dev_serial_num_info),
                           	dev_serial_num_encrypted_info,
                           	(uint32*)&encrypted_info_len))
       {
         errval = QMI_ERR_ENCODING;
         dsm_free_packet(&response);
	 goto send_result;
       }
     }
    if(FALSE == qmi_svc_put_param_tlv(
                  &response, 
                  DMSI_PRM_TYPE_IMEISV_SVN,
					 (is_encrypted)?(uint16) encrypted_info_len:(uint16) strlen((const char *)dev_serial_num_info),
					 (is_encrypted)?dev_serial_num_encrypted_info:dev_serial_num_info))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }

  }
  else
  {
    LOG_MSG_ERROR_0("Problem reading IMEISV SVN from NV");
  }
#endif /* defined(FEATURE_WCDMA) || defined(FEATURE_GSM) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA) */
  if( response == NULL )
  {
    LOG_MSG_INFO1_0("No serial numbers assigned to device!");
    errval = QMI_ERR_NOT_PROVISIONED;
  }
  else
  {
    errval = QMI_ERR_NONE;
  }

  /*-------------------------------------------------------------------------
    Free the temporary memory allocated for NV item.
  -------------------------------------------------------------------------*/
  PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_item_ptr);

send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  
  return response;

} /* qmi_dmsi_get_device_serial_numbers_ext() */



/*===========================================================================
  FUNCTION QMI_DMSI_GET_POWER_STATE()

  DESCRIPTION
    Gives the status of the device power including battery strength of the
    device and whether the device is externally powered
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_get_power_state
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *  response;
  PACKED struct PACKED_POST
  {
    uint8 power_status;
    uint8 battery_lvl;
  } v_required;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  response = NULL;
  errval = QMI_ERR_NONE;

  v_required.power_status = 0;
  v_required.battery_lvl = 0;

  qmi_if_get_power_state(&v_required.power_status, 
                            &v_required.battery_lvl);
  
  if( FALSE == qmi_svc_put_param_tlv(&response, 
                                     QMI_TYPE_REQUIRED_PARAMETERS,
                                     sizeof(v_required),
                                     &v_required) )
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
  }

  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);
  
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  
  return response;

} /* qmi_dmsi_get_power_state() */



/*===========================================================================
  FUNCTION QMI_DMSI_SET_EVENT_REPORT()

  DESCRIPTION
    To set state for event reporting 
        
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_set_event_report
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *               response;
  qmi_dmsi_client_state_type *  dms_client_sp;
  qmi_dmsi_cmd_buf_type      *  cmd_ptr;
  
  PACKED struct PACKED_POST
  {
    boolean power_state_ind_mode;
  } v_in1;

  PACKED struct PACKED_POST
  {
    uint8 bat_lvl_lower_limit;
    uint8 bat_lvl_upper_limit;
  } v_in2;

  PACKED struct PACKED_POST
  {
    boolean report_pin_status;
  } v_in3;

  PACKED struct PACKED_POST
  {
    boolean report_activation_state;
  } v_in4;

  PACKED struct PACKED_POST
  {
    boolean report_operating_mode;
  } v_in5;

  PACKED struct PACKED_POST
  {
    boolean report_uim_state;
  } v_in6;

  PACKED struct PACKED_POST
  {
    boolean report_wd_state;
  } v_in7;

  PACKED struct PACKED_POST
  {
    boolean report_prl_init;
  } v_in8;

  PACKED struct PACKED_POST
  {
    boolean report_cdma_lock_mode;
  } v_in9;

  PACKED struct PACKED_POST
  {
    boolean report_device_mode;
  } v_in10;

  boolean  got_power_state_tlv = FALSE;
  boolean  got_bat_lvl_tlv     = FALSE;
  boolean  got_pin_status_tlv  = FALSE; 
  boolean  got_activation_state_tlv = FALSE;
  boolean  got_oprt_mode_tlv   = FALSE;
  boolean  got_uim_state_tlv   = FALSE;
  boolean  got_wd_state_tlv    = FALSE;
  boolean  got_prl_init_tlv    = FALSE;
  boolean  got_cdma_lock_mode  = FALSE;
  boolean  got_device_mode  = FALSE;
  uint8    type;
  uint16   len;
  uint16   expected_len;
  uint8    curr_power_state;
  uint8    curr_bat_lvl;
  void *   value;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  memset(&v_in1, 0, sizeof(v_in1));
  memset(&v_in2, 0, sizeof(v_in2)); 
  memset(&v_in3, 0, sizeof(v_in3));
  memset(&v_in4, 0, sizeof(v_in4));
  memset(&v_in5, 0, sizeof(v_in5));
  memset(&v_in6, 0, sizeof(v_in6));
  memset(&v_in7, 0, sizeof(v_in7));
  memset(&v_in8, 0, sizeof(v_in8));
  memset(&v_in9, 0, sizeof(v_in9));
  memset(&v_in10, 0, sizeof(v_in10));

  curr_bat_lvl = 0;
  response = NULL;
  errval = QMI_ERR_NONE;

  dms_client_sp = (qmi_dmsi_client_state_type *) cl_sp;

  while (*sdu_in)
  {
    if( !qmi_svc_get_tl( sdu_in, &type, &len ) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case DMSI_PRM_TYPE_POWER_STATE:
        expected_len = sizeof(v_in1);
        got_power_state_tlv = TRUE;
        value = &v_in1;
        break;

      case DMSI_PRM_TYPE_BAT_LVL_RPT_LIMITS:
        expected_len = sizeof(v_in2);
        got_bat_lvl_tlv = TRUE; 
        value = &v_in2;
        break;

      case DMSI_PRM_TYPE_PIN_STATUS:
        expected_len = sizeof(v_in3);
        got_pin_status_tlv = TRUE;
        value = &v_in3;
        break;

      case DMSI_PRM_TYPE_ACTIVATION_STATE:
        expected_len = sizeof(v_in4);
        got_activation_state_tlv = TRUE;
        value = &v_in4;
        break;

      case DMSI_PRM_TYPE_OPRT_MODE:
        expected_len = sizeof(v_in5);
        got_oprt_mode_tlv = TRUE;
        value = &v_in5;
        break;

      case DMSI_PRM_TYPE_UIM_GET_STATE:
        expected_len = sizeof(v_in6);
        got_uim_state_tlv = TRUE;
        value = &v_in6;
        break;

      case DMSI_PRM_TYPE_WD_STATE:
        expected_len = sizeof(v_in7);
        got_wd_state_tlv = TRUE;
        value = &v_in7;
        break;

      case DMSI_PRM_TYPE_PRL_INIT:
        expected_len = sizeof(v_in8);
        got_prl_init_tlv = TRUE;
        value = &v_in8;
        break;

      case DMSI_PRM_TYPE_CDMA_LOCK_MODE:
        expected_len = sizeof(v_in9);
        got_cdma_lock_mode = TRUE;
        value = &v_in9;
        break;

      case DMSI_PRM_TYPE_DEVICE_MODE:
        expected_len = sizeof(v_in10);
        got_device_mode = TRUE;
        value = &v_in10;
        break;

      default:
        LOG_MSG_INFO2_1 ("Unrecognized TLV type (%d)", type);
        break;
    }

    if(!qmi_svc_validate_type_length( type,
                                          len,
                                          expected_len,
                                          FILE_ID_DS_QMI_DMS,__LINE__) )
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    /*-----------------------------------------------------------------------
      If type is unrecognized, value will be NULL, and qmi_svc_dsm_pullup will 
      free the unrecognized value, i.e. we skip past unrecognized TLVs
    -----------------------------------------------------------------------*/
    if(len != qmi_svc_dsm_pullup( sdu_in, value, len ,FILE_ID_DS_QMI_DMS,__LINE__  ))
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  /*-------------------------------------------------------------------------
    If ALL optional TLVs are absent return error or else carry on and
    process the request.
  -------------------------------------------------------------------------*/
  if(!(got_power_state_tlv ||
       got_bat_lvl_tlv ||
       got_pin_status_tlv || 
       got_activation_state_tlv ||
       got_oprt_mode_tlv ||
       got_uim_state_tlv ||
       got_wd_state_tlv ||
       got_prl_init_tlv ||
       got_cdma_lock_mode||
       got_device_mode ))
  {
      errval = QMI_ERR_MISSING_ARG;
      goto send_result;
  }

  /*-------------------------------------------------------------------------
    Perform 'supported' error checking on all provided TLV's before processing
  -------------------------------------------------------------------------*/
#ifndef FEATURE_MMGSDI_SESSION_LIB
  if (got_pin_status_tlv || got_uim_state_tlv)
  {
    errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
    goto send_result;
  }
#endif /* FEATURE_MMGSDI_SESSION_LIB */

#ifndef FEATURE_OTASP
  if (got_activation_state_tlv)
  {
    errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
    goto send_result;
  }
#endif /* FEATURE_OTASP */

#ifndef FEATURE_DATA_QMI_ADDENDUM
  if (got_wd_state_tlv)
  {
    errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
    goto send_result;
  }
#endif /* FEATURE_DATA_QMI_ADDENDUM */

  /*-------------------------------------------------------------------------
    change SIM PIN state reporting if tlv included in request
  -------------------------------------------------------------------------*/
#ifdef FEATURE_MMGSDI_SESSION_LIB
  if( got_pin_status_tlv )
  {
    v_in3.report_pin_status = (v_in3.report_pin_status > 0) ? TRUE : FALSE;
    
    /*-----------------------------------------------------------------------
      if pin status report_mode is same as previous do nothing
    -----------------------------------------------------------------------*/
    if(dms_client_sp->report_status.report_pin_status != 
         v_in3.report_pin_status)
    {
      dms_client_sp->report_status.report_pin_status = 
        v_in3.report_pin_status;
      if (v_in3.report_pin_status == FALSE)
      {
        qmi_dmsi_global.sim.report_pin_status_client_count--; 
      }
      else  //v_in3.report_pin_status == TRUE
      {
        qmi_dmsi_global.sim.report_pin_status_client_count++;
      }
    }
  }
#endif /* FEATURE_MMGSDI_SESSION_LIB */

  /*-------------------------------------------------------------------------
    change power state reporting if tlv included in request
  -------------------------------------------------------------------------*/
  if( got_power_state_tlv )
  {
    if(v_in1.power_state_ind_mode > 0)
    { 
      v_in1.power_state_ind_mode = TRUE;
    }
    else
    {
      v_in1.power_state_ind_mode = FALSE;
    }  
    /*-----------------------------------------------------------------------
      if power state report_mode is same as previous do nothing
    -----------------------------------------------------------------------*/
    if(dms_client_sp->report_status.report_power_state !=
       v_in1.power_state_ind_mode)
    {
      dms_client_sp->report_status.report_power_state =
        v_in1.power_state_ind_mode;
      if (v_in1.power_state_ind_mode == FALSE)
      {
        qmi_dmsi_global.pwr_rpt.report_power_state_client_count--; 

        if(qmi_dmsi_global.pwr_rpt.report_power_state_client_count == 0)
        {
          qmi_charger_set_power_state_reporting(FALSE);
        }
      }

      if(v_in1.power_state_ind_mode == TRUE)
      {
        qmi_dmsi_global.pwr_rpt.report_power_state_client_count++;

        if(qmi_dmsi_global.pwr_rpt.report_power_state_client_count == 1)
        {
          qmi_charger_set_power_state_reporting(TRUE);
        }
        else
        { 
          /*-----------------------------------------------------------------
            Since 2nd or more client has registered, reset the previous
            power state mask in Charger, so that it causes a charger event
            and a DMS ind can be sent to this new client
          -----------------------------------------------------------------*/
          qmi_charger_reset_prev_power_state_mask();
        }
      }
    }
  }
  
  /*-------------------------------------------------------------------------
    change power state report thresholds if tlv included in request
  -------------------------------------------------------------------------*/
  if( got_bat_lvl_tlv )  
  {
    /*-----------------------------------------------------------------------
      validate limits.
      - if lower is less than upper, swap them
      - if lower is less than min, set to min
      - if upper is more than max, set to max
    -----------------------------------------------------------------------*/ 
    if (v_in2.bat_lvl_lower_limit > v_in2.bat_lvl_upper_limit)
    {
      type = v_in2.bat_lvl_lower_limit;
      v_in2.bat_lvl_lower_limit = v_in2.bat_lvl_upper_limit;
      v_in2.bat_lvl_upper_limit = type;
    }

    if (v_in2.bat_lvl_upper_limit > DMSI_BAT_LVL_MAX)
    {
      v_in2.bat_lvl_upper_limit = DMSI_BAT_LVL_MAX;
    }

    /*-----------------------------------------------------------------------
      commit limits to client state
    -----------------------------------------------------------------------*/ 
    dms_client_sp->report_status.bat_lvl_lower_limit =
      v_in2.bat_lvl_lower_limit;
    dms_client_sp->report_status.bat_lvl_upper_limit =
      v_in2.bat_lvl_upper_limit;

    /*-----------------------------------------------------------------------
      Change OEM charger report limits if applicable

       Generate event_report ind if current power level is outside
       registered range
    -----------------------------------------------------------------------*/ 
    qmi_if_get_power_state(&curr_power_state, 
                           &curr_bat_lvl);
  }
  /*-------------------------------------------------------------------------
    Update the battery level limits - determine the new high and low battery 
    limits and set them in the charger module. Note - if the new limits in the
    request are such that there is no change in the high, low limits for the 
    charger, it will result in again setting the existing charger limits, 
    but that is alright
  -------------------------------------------------------------------------*/
  if(got_power_state_tlv || got_bat_lvl_tlv)
  {
    qmi_dmsi_set_battery_level_limits(curr_bat_lvl);
  }
                                                 
  if (got_activation_state_tlv)
  {
    if (0 < v_in4.report_activation_state)
    {
      dms_client_sp->report_status.report_activation_state = TRUE;
    }
    else
    {
      dms_client_sp->report_status.report_activation_state = FALSE;
    }
  }
  
  /*-------------------------------------------------------------------------
    Update setting for reporting the operating mode if TLV present
  -------------------------------------------------------------------------*/
  if (got_oprt_mode_tlv)
  {
    dms_client_sp->report_status.report_operating_mode =
      (0 < v_in5.report_operating_mode)? TRUE: FALSE;
  }

  /*-------------------------------------------------------------------------
    Update setting for reporting the uim_state if TLV present
  -------------------------------------------------------------------------*/
#ifdef FEATURE_MMGSDI_SESSION_LIB
  if (got_uim_state_tlv)
  {
    dms_client_sp->report_status.report_uim_state =
      (0 < v_in6.report_uim_state)? TRUE: FALSE;
  }
#endif /* FEATURE_MMGSDI_SESSION_LIB */

  /*-------------------------------------------------------------------------
    Update setting for reporting the wireless disable switch state
  -------------------------------------------------------------------------*/
  if (got_wd_state_tlv)
  {
    if (0 < v_in7.report_wd_state)
    {
      dms_client_sp->report_status.report_wd_state = TRUE;
    }
    else
    {
      dms_client_sp->report_status.report_wd_state = FALSE;
    }
  }

  if (got_prl_init_tlv)
  {
    dms_client_sp->report_status.report_prl_init = 
                     (0 < v_in8.report_prl_init) ? TRUE : FALSE; 
  }

  /*-------------------------------------------------------------------------
    Update setting for reporting cdma lock mode if TLV present
  -------------------------------------------------------------------------*/
  if (got_cdma_lock_mode)
  {
    LOG_MSG_INFO2_0("In cdma lock Event Report indication!");
    dms_client_sp->report_status.report_cdma_lock_mode =
      (0 < v_in9.report_cdma_lock_mode)? TRUE: FALSE;

    /* If a client registers for QMI indication, the indication must be sent right away */
    /* Post a command to dcc task to send the indication */
    if(dms_client_sp->report_status.report_cdma_lock_mode == TRUE)
    {

      cmd_ptr = (qmi_dmsi_cmd_buf_type *)qmi_dmsi_get_cmd_buf(QMI_CMD_DMS_EVT_REPORT_REQ_IND);
      if( cmd_ptr == NULL)
      {
        errval = QMI_ERR_NO_MEMORY;
        goto send_result;
      }
      memset( cmd_ptr, 0x00, sizeof(*cmd_ptr) );

      cmd_ptr->cmd_id= QMI_CMD_DMS_EVT_REPORT_REQ_IND;
      cmd_ptr->data.dms_evt_info.evt_type = DMSI_REPORT_STATUS_EV_CDMA_LOCK_MODE;
      cmd_ptr->data.dms_evt_info.evt_value = &qmi_dmsi_global.cdma_lock_mode;
      cmd_ptr->data.dms_evt_info.clid = dms_client_sp->common.clid;
      dcc_send_cmd_ex(DCC_QMI_CMD, cmd_ptr);
    }
  }

  if (got_device_mode)
  {
    dms_client_sp->report_status.report_device_mode =
      (0 < v_in10.report_device_mode)? TRUE: FALSE;
    if(dms_client_sp->report_status.report_device_mode == TRUE)
    {
       /*Send the current info related to client*/
      cmd_ptr = (qmi_dmsi_cmd_buf_type *)qmi_dmsi_get_cmd_buf(QMI_CMD_DMS_EVT_REPORT_REQ_IND);
      if( cmd_ptr == NULL)
      {
        errval = QMI_ERR_NO_MEMORY;
        goto send_result;
      }
      memset( cmd_ptr, 0x00, sizeof(*cmd_ptr) );

      cmd_ptr->cmd_id= QMI_CMD_DMS_EVT_REPORT_REQ_IND;
      cmd_ptr->data.dms_evt_info.evt_type = DMSI_REPORT_STATUS_EV_DEV_MSIM_CAP;
      cmd_ptr->data.dms_evt_info.evt_value = NULL;
      cmd_ptr->data.dms_evt_info.clid = dms_client_sp->common.clid;
      dcc_send_cmd_ex(DCC_QMI_CMD, cmd_ptr);
    }
  }

  /*-------------------------------------------------------------------------
    Build the response from back to front
    No Failure possible upto here so result is success
  -------------------------------------------------------------------------*/
send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  
  return response;

} /* qmi_dmsi_set_event_report() */



/*===========================================================================
  FUNCTION QMI_DMSI_EVENT_REPORT_IND()

  DESCRIPTION
    Is called when any of the conditions set in set_event_report becomes true
    Sends an indication to the client
        
  PARAMETERS
    sp           : service provided state pointer (user data)
    clid         : clientID
    event_mask   : mask to indicate what caused the indication to be called
    sig_strength : current signal strength
    radio_if     : radio technology in use
    power_status : power status of the mobile
    battery_lvl  : battery level
    
  RETURN VALUE
    void
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_dmsi_event_report_ind
(
  void *   sp,
  uint8    clid,
  uint32   event_mask,
  uint8    power_status,  /* bitmask */
  uint8    battery_lvl,
  uint8    pin_index,
  uint16   activation_state,
  uint8    operating_mode,
  boolean  wd_state,
  boolean  cdma_lock_mode
)
{
  dsm_item_type *  ind;
  qmi_framework_common_msg_hdr_type    common_hdr;
  qmi_error_e_type    errval;
  
  PACKED struct PACKED_POST
  {
    uint8 power_status;
    uint8 battery_lvl;
  } v_out1;

#ifdef FEATURE_MMGSDI_SESSION_LIB
  PACKED struct PACKED_POST
  {
    uint8 status;
    uint8 num_retries;
    uint8 num_unblock_retries;
  } v_out2;
  uint8   param_type;
#endif /* FEATURE_MMGSDI_SESSION_LIB */

  PACKED struct PACKED_POST
  {
    uint16 activation_state;
  } v_out3;

  PACKED struct PACKED_POST
  {
    uint8 operating_mode;
  } v_out4;
  
#ifdef FEATURE_MMGSDI_SESSION_LIB  
  PACKED struct PACKED_POST
  {
    uint8 uim_state;
  } v_out5;
#endif /* FEATURE_MMGSDI_SESSION_LIB */

  PACKED struct PACKED_POST
  {
    uint8 wd_state;
  } v_out6;

  PACKED struct PACKED_POST
  {
    uint8 prl_init;
  } v_out7;
  
  PACKED struct PACKED_POST
  {
    uint32 cdma_lock_mode;
  } v_out8;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp);

  ind = NULL;

  if(event_mask & DMSI_REPORT_STATUS_EV_DEV_MSIM_CAP) 
  {
    do 
    {
     errval = qmi_dmsi_get_multisim_info(&ind, DMSI_PRM_TYPE_DEVICE_MODE);
     if(errval != QMI_ERR_NONE)
     {
        break;
     }

     /*Do call msim info thing to add 3 tlvs and mention it is ind or response*/
     errval =  qmi_dmsi_get_device_multisim_current_cap_info(&ind, QMI_DMS_DEVICE_CAP_INDICATION);
     if(errval != QMI_ERR_NONE)
     {
        break;
      }

      /*get the max_active_data subs for ind or response*/
      errval =  qmi_dmsi_get_max_active_data_subs(&ind, DMSI_PRM_TYPE_IND_MAX_DATA_SUBS);
      if(errval != QMI_ERR_NONE)
      {
        break;
      }
    }while(0);
    if(errval != QMI_ERR_NONE)
    {
      LOG_MSG_INFO2_1("Unable to generate Event Report indication! %d",errval);
       dsm_free_packet(&ind);
       return;
     }
  }

  /*-------------------------------------------------------------------------
    ind was called for power state and battery lvl
  -------------------------------------------------------------------------*/
  if(event_mask & DMSI_REPORT_STATUS_EV_POWER_STATE) 
  {
    v_out1.power_status = power_status;
    v_out1.battery_lvl  = battery_lvl;
    if( FALSE == qmi_svc_put_param_tlv2(&ind, 
                                       DMSI_PRM_TYPE_POWER_STATE,
                                       sizeof (v_out1),
                                       &v_out1,
                                       FILE_ID_DS_QMI_DMS,__LINE__) )
    {
      dsm_free_packet(&ind);
      return;
    }
  }
#ifdef FEATURE_MMGSDI_SESSION_LIB
  if(event_mask & DMSI_REPORT_STATUS_EV_PIN_STATUS) 
  {
    if(pin_index == MMGSDI_PIN1)
    {
      param_type = DMSI_PRM_TYPE_PIN1_STATUS;
    }
    else if(pin_index == MMGSDI_PIN2) 
    {
      param_type = DMSI_PRM_TYPE_PIN2_STATUS;
    }
    else
    {
      LOG_MSG_INFO1_1("Unhandled pin index %d, only PIN1 and PIN2 supported",
                      pin_index);
      dsm_free_packet(&ind);
      return;
    }
    
    v_out2.status = qmi_dmsi_global.sim.pin_info[pin_index].status;
    v_out2.num_retries = qmi_dmsi_global.sim.pin_info[pin_index].num_retries;
    v_out2.num_unblock_retries = 
      qmi_dmsi_global.sim.pin_info[pin_index].num_unblock_retries;

    if( FALSE == qmi_svc_put_param_tlv2(&ind, 
                                       param_type,
                                       sizeof (v_out2),
                                       &v_out2,
                                       FILE_ID_DS_QMI_DMS,__LINE__) )
    {
      dsm_free_packet(&ind);
      return;
    }
  }
#endif /* FEATURE_MMGSDI_SESSION_LIB */

  if(event_mask & DMSI_REPORT_STATUS_EV_ACTIVATION_STATE) 
  {
    v_out3.activation_state = activation_state;
    if( FALSE == qmi_svc_put_param_tlv2(&ind, 
                                       DMSI_PRM_TYPE_ACTIVATION_STATE,
                                       sizeof (v_out3),
                                       &v_out3,
                                       FILE_ID_DS_QMI_DMS,__LINE__) )
    {
      dsm_free_packet(&ind);
      return;
    }
  }

  if (event_mask & DMSI_REPORT_STATUS_EV_OPRT_MODE)
  {
    v_out4.operating_mode = operating_mode;
    if (FALSE == qmi_svc_put_param_tlv2(&ind,
                                       DMSI_PRM_TYPE_OPRT_MODE,
                                       sizeof(v_out4),
                                       &v_out4,
                                       FILE_ID_DS_QMI_DMS,__LINE__))
    {
      dsm_free_packet(&ind);
      return;
    }
  }

#ifdef FEATURE_MMGSDI_SESSION_LIB
  if (event_mask & DMSI_REPORT_STATUS_EV_UIM_GET_STATE)
  {
 
    v_out5.uim_state = (uint8)qmi_dmsi_global.uim_state;

    if (FALSE == qmi_svc_put_param_tlv2(&ind,
                                       DMSI_PRM_TYPE_UIM_GET_STATE,
                                       sizeof(v_out5),
                                       &v_out5,
                                       FILE_ID_DS_QMI_DMS,__LINE__))
    {
      dsm_free_packet(&ind);
      return;
    }
  }
#endif /* FEATURE_MMGSDI_SESSION_LIB */

  if (event_mask & DMSI_REPORT_STATUS_EV_WD_STATE)
  {
    v_out6.wd_state = (wd_state == TRUE) ? 1 : 0;

    if (FALSE == qmi_svc_put_param_tlv2(&ind,
                                       DMSI_PRM_TYPE_WD_STATE,
                                       sizeof(v_out6),
                                       &v_out6,
                                       FILE_ID_DS_QMI_DMS,__LINE__))
    {
      dsm_free_packet(&ind);
      return;
    }
  }

  if (event_mask & DMSI_REPORT_STATUS_EV_PRL_INIT)
  {
    v_out7.prl_init = 1;

    if (FALSE == qmi_svc_put_param_tlv2(&ind,
                                       DMSI_PRM_TYPE_PRL_INIT,
                                       sizeof(v_out7),
                                       &v_out7,
                                       FILE_ID_DS_QMI_DMS,__LINE__))
    {
      dsm_free_packet(&ind);
      return;
    }
  }

  if (event_mask & DMSI_REPORT_STATUS_EV_CDMA_LOCK_MODE)
  {
    v_out8.cdma_lock_mode = (cdma_lock_mode == TRUE) ? 1 : 0;

    if (FALSE == qmi_svc_put_param_tlv2(&ind,
                                       DMSI_PRM_TYPE_CDMA_LOCK_MODE,
                                       sizeof(v_out8),
                                       &v_out8,
                                       FILE_ID_DS_QMI_DMS,__LINE__))
    {
      dsm_free_packet(&ind);
      return;
    }
  }

  /*-----------------------------------------------------------------------
    Fill the QMI Framework message header before sending Indication to
    Framework.
  -----------------------------------------------------------------------*/
  common_hdr.client_id = clid;
  /* if unicast indication, qmi framework map the instance and no need to fill*/
  common_hdr.qmi_instance = DMSI_INVALID_INSTANCE;
  common_hdr.service = QMUX_SERVICE_DMS;
  /* for indication the transaction ID can be ignored */
  common_hdr.transaction_id = 0;
  
  if( FALSE == ds_qmi_fw_send_ind( &common_hdr, 
                                   DMSI_CMD_VAL_SET_EVENT_REPORT, 
                                   ind ) )
  {
    LOG_MSG_ERROR_0("Unable to send dms event report indication!");
    dsm_free_packet(&ind);
  }

} /* qmi_dmsi_event_report_ind() */



/*===========================================================================
  FUNCTION QMI_DMSI_SET_UIM_PIN_PROTECTION()

  DESCRIPTION
    To enable/diable PIN or PIN2.
        
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_set_uim_pin_protection
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *  response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;
#ifdef FEATURE_MMGSDI_SESSION_LIB 
  PACKED struct PACKED_POST
  {
    uint8   pin_id;
    boolean protection_setting;
    uint8   pin_len;
    uint8   pin_value[MMGSDI_PIN_MAX_LEN];  
  } v_in_req;
  boolean  got_v_in_req;
  uint8    type;
  uint16   len;
  void *   value;
  uint16    tlv_len;
  uint16   expected_len;
  mmgsdi_pin_enum_type pin_id;
  mmgsdi_data_type    pin_data;
  mmgsdi_return_enum_type mmgsdi_status;
  qmi_if_info_type  *qmi_if_info_ptr;
  qmi_dmsi_client_state_type * dms_cl_sp;
  ds_qmi_fw_async_cmd_buf_type  *async_cmd_buf;
#endif /* FEATURE_MMGSDI_SESSION_LIB */
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  response = NULL;
  errval = QMI_ERR_NONE;
#ifndef FEATURE_MMGSDI_SESSION_LIB
  errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
  goto send_result;
#else
  qmi_if_info_ptr = qmi_if_info_get_ptr();
  got_v_in_req = FALSE;
  tlv_len = 0;
  memset(&v_in_req, 0, sizeof(v_in_req));
  memset(&pin_data, 0, sizeof(mmgsdi_data_type));
  
  while (*sdu_in)
  {
    if( !qmi_svc_get_tl( sdu_in, &type, &len ) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        if (len <= sizeof(v_in_req))
        {
          got_v_in_req = TRUE;
          value = &v_in_req;
          tlv_len = len;
        }
        else
        {
          errval = QMI_ERR_ARG_TOO_LONG;
          goto send_result;
        }
        break;

     default:
        LOG_MSG_INFO2_1 ("Unrecognized TLV type (%d)", type);
        break;
    }

    if(!qmi_svc_validate_type_length( type,
                                          len,
                                          expected_len,
                                          FILE_ID_DS_QMI_DMS,__LINE__) )
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    /*-----------------------------------------------------------------------
      If type is unrecognized, value will be NULL, and qmi_svc_dsm_pullup will 
      free the unrecognized value, i.e. we skip past unrecognized TLVs
    -----------------------------------------------------------------------*/
    if(len != qmi_svc_dsm_pullup( sdu_in, value, len ,FILE_ID_DS_QMI_DMS,__LINE__ ))
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  if(!got_v_in_req)
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  switch (v_in_req.pin_id) 
  {
    case DMSI_UIM_PIN1:
      pin_id = MMGSDI_PIN1;
      break;

    case DMSI_UIM_PIN2:
      pin_id = MMGSDI_PIN2;
      break;

    default:
      errval = QMI_ERR_INVALID_PINID;
      goto send_result;
  }
  
  if(v_in_req.pin_len > MMGSDI_PIN_MAX_LEN)
  {
    errval = QMI_ERR_ARG_TOO_LONG;
    goto send_result;
  }
  /* Validate the length of the input tlv to make sure it is consistent with
     the pin length specified within the tlv */
  if(tlv_len != (sizeof(v_in_req) - (MMGSDI_PIN_MAX_LEN - v_in_req.pin_len)))
  {
    errval = QMI_ERR_INCORRECT_PIN;
    goto send_result;
  }

  pin_data.data_ptr = v_in_req.pin_value;
  pin_data.data_len = v_in_req.pin_len;

  if ((errval = qmi_dmsi_get_uim_status()) != QMI_ERR_NONE)
  {
    goto send_result;
  }
  
  if (qmi_dmsi_global.sim.pin_info[pin_id].status == QMI_PIN_STATUS_MIN) 
  {
    errval = QMI_ERR_SIM_NOT_INITIALIZED;
    goto send_result;
  }

  dms_cl_sp = (qmi_dmsi_client_state_type*)cl_sp;
  async_cmd_buf = ds_qmi_fw_new_async_cmd_buf(cmd_buf_p, dms_cl_sp->cookie);
  if(async_cmd_buf == NULL)
  {
    errval = QMI_ERR_NO_MEMORY;
    goto send_result;
  }

  if(v_in_req.protection_setting == FALSE) 
  {
    mmgsdi_status = mmgsdi_session_disable_pin(
                                       qmi_if_info_ptr->session_info[QMI_IF_MMGSD_GW_PROV_PRI_SESSION].session_id,
                                       pin_id,
                                       MMGSDI_PIN_NOT_REPLACED_BY_UNIVERSAL,
                                       pin_data,
                                       qmi_if_mmgsdi_resp_cback, 
                                       (uint32) async_cmd_buf);
  }
  else // if > 0, implies TRUE
  {
    mmgsdi_status = mmgsdi_session_enable_pin(
                                      qmi_if_info_ptr->session_info[QMI_IF_MMGSD_GW_PROV_PRI_SESSION].session_id,
                                      pin_id,
                                      pin_data,
                                      qmi_if_mmgsdi_resp_cback, 
                                      (uint32) async_cmd_buf);
  }
  
  if(mmgsdi_status != MMGSDI_SUCCESS)
  { 
    ds_qmi_fw_free_async_cmd_buf(async_cmd_buf);
    errval = qmi_if_map_mmgsdi_status_to_errval(mmgsdi_status,__LINE__);
    goto send_result;
  }
  
  memset(&v_in_req, 0, sizeof(v_in_req));
  memset(&pin_data, 0, sizeof(pin_data));
  return QMI_SVC_RESPONSE_PENDING;
#endif /* FEATURE_MMGSDI_SESSION_LIB */

send_result: 
#ifdef FEATURE_MMGSDI_SESSION_LIB  
  memset(&v_in_req, 0, sizeof(v_in_req));// for security
  memset(&pin_data, 0, sizeof(pin_data));
#endif /* FEATURE_MMGSDI_SESSION_LIB */
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  
  return response;
} /* qmi_dmsi_set_uim_pin_protection() */



/*===========================================================================
  FUNCTION QMI_DMSI_VERIFY_UIM_PIN()

  DESCRIPTION
    To verify PIN or PIN2.
        
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_verify_uim_pin
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *  response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

#ifdef FEATURE_MMGSDI_SESSION_LIB
  PACKED struct PACKED_POST
  {
    uint8   pin_id;
    uint8   pin_len;
    uint8   pin_value[MMGSDI_PIN_MAX_LEN]; 
  } v_in_req;

  boolean  got_v_in_req;
  uint8    type;
  uint16   len;
  void *   value;
  uint16    tlv_len;
  uint16   expected_len;
  mmgsdi_pin_enum_type pin_id;
  mmgsdi_data_type    pin_data;
  mmgsdi_return_enum_type mmgsdi_status;
  qmi_if_info_type  *qmi_if_info_ptr;  
  qmi_dmsi_client_state_type * dms_cl_sp;
  ds_qmi_fw_async_cmd_buf_type  *async_cmd_buf;

#endif /* FEATURE_MMGSDI_SESSION_LIB */
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  response = NULL;
  errval = QMI_ERR_NONE;
#ifndef FEATURE_MMGSDI_SESSION_LIB
  errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
  goto send_result;
#else
  qmi_if_info_ptr = qmi_if_info_get_ptr();

  got_v_in_req = FALSE;
  tlv_len = 0;
  memset(&v_in_req, 0, sizeof(v_in_req));
  memset(&pin_data, 0, sizeof(mmgsdi_data_type));
  
  while (*sdu_in)
  {
    if( !qmi_svc_get_tl( sdu_in, &type, &len ) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        if (len <= sizeof(v_in_req))
        {
          got_v_in_req = TRUE;
          value = &v_in_req;
          tlv_len = len;
        }
        else
        {
          errval = QMI_ERR_ARG_TOO_LONG;
          goto send_result;
        }
        break;

     default:
        LOG_MSG_INFO2_1 ("Unrecognized TLV type (%d)", type);
        break;
    }

    if(!qmi_svc_validate_type_length( type,
                                          len,
                                          expected_len,
                                          FILE_ID_DS_QMI_DMS,__LINE__) )
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    /*-----------------------------------------------------------------------
      If type is unrecognized, value will be NULL, and qmi_svc_dsm_pullup will 
      free the unrecognized value, i.e. we skip past unrecognized TLVs
    -----------------------------------------------------------------------*/
    if(len != qmi_svc_dsm_pullup( sdu_in, value, len ,FILE_ID_DS_QMI_DMS,__LINE__  ))
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  if(!got_v_in_req)
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  if(v_in_req.pin_len > MMGSDI_PIN_MAX_LEN)
  {
    errval = QMI_ERR_ARG_TOO_LONG;
    goto send_result;
  }       

  /* Validate the length of the input tlv to make sure it is consistent with
     the pin length specified within the tlv */
  if(tlv_len != (sizeof(v_in_req) - (MMGSDI_PIN_MAX_LEN - v_in_req.pin_len)))
  {
    errval = QMI_ERR_INCORRECT_PIN;
    goto send_result;
  }

  switch (v_in_req.pin_id) 
  {
    case DMSI_UIM_PIN1:
      pin_id = MMGSDI_PIN1;
      break;

    case DMSI_UIM_PIN2:
      pin_id = MMGSDI_PIN2;
      break;

    default:
      errval = QMI_ERR_INVALID_PINID;
      goto send_result;
  }
  
  pin_data.data_ptr = v_in_req.pin_value;
  pin_data.data_len = v_in_req.pin_len;

  if ((errval = qmi_dmsi_get_uim_status()) != QMI_ERR_NONE)
  {
    goto send_result;
  }

  if (qmi_dmsi_global.sim.pin_info[pin_id].status == QMI_PIN_STATUS_MIN) 
  {
    errval = QMI_ERR_SIM_NOT_INITIALIZED;
    goto send_result;
  }

  dms_cl_sp = (qmi_dmsi_client_state_type*)cl_sp;
  async_cmd_buf = ds_qmi_fw_new_async_cmd_buf(cmd_buf_p, dms_cl_sp->cookie);
   if(async_cmd_buf == NULL)
   {
     errval = QMI_ERR_NO_MEMORY;
     goto send_result;
   }

  mmgsdi_status = mmgsdi_session_verify_pin(
                                    qmi_if_info_ptr->session_info[QMI_IF_MMGSD_GW_PROV_PRI_SESSION].session_id,
                                    pin_id,
                                    pin_data,
                                    qmi_if_mmgsdi_resp_cback, 
                                    (uint32) async_cmd_buf);  
  if(mmgsdi_status != MMGSDI_SUCCESS)
  { 
    ds_qmi_fw_free_async_cmd_buf(async_cmd_buf);	
    errval = qmi_if_map_mmgsdi_status_to_errval(mmgsdi_status,__LINE__);
    goto send_result;
  }
  
  memset(&v_in_req, 0, sizeof(v_in_req));// for security
  memset(&pin_data, 0, sizeof(pin_data));
  return QMI_SVC_RESPONSE_PENDING;
#endif /* FEATURE_MMGSDI_SESSION_LIB */

send_result:  
#ifdef FEATURE_MMGSDI_SESSION_LIB
  memset(&v_in_req, 0, sizeof(v_in_req));// for security
  memset(&pin_data, 0, sizeof(pin_data));
#endif /* FEATURE_MMGSDI_SESSION_LIB */
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;
} /* qmi_dmsi_verify_uim_pin() */



/*===========================================================================
  FUNCTION QMI_DMSI_UNBLOCK_UIM_PIN()

  DESCRIPTION
    To unblock PIN or PIN2.
        
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_unblock_uim_pin
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *  response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;
#ifdef FEATURE_MMGSDI_SESSION_LIB
  PACKED struct PACKED_POST
  {
    uint8  pin_id;
    uint8  puk_len;
    uint8  puk_value[MMGSDI_PIN_MAX_LEN]; 
    uint8  new_pin_len;
    uint8  new_pin_value[MMGSDI_PIN_MAX_LEN]; 
  } v_in_req;

#define DMSI_PRM_TYPE_UNBLOCK_PIN_MAX_SIZE   (3 + 2*MMGSDI_PIN_MAX_LEN)
  uint8    unblock_pin_info[DMSI_PRM_TYPE_UNBLOCK_PIN_MAX_SIZE];
  uint8 *  unblock_pin_info_ptr;
  boolean  got_v_in_req;
  uint8    type;
  uint16   len;
  void *   value;
  uint16    tlv_len;
  uint16             expected_len;
  uint16             expected_tlv_len;
  mmgsdi_pin_enum_type pin_id;
  mmgsdi_data_type    new_pin_data;
  mmgsdi_data_type    puk_data;
  mmgsdi_return_enum_type mmgsdi_status;
  qmi_if_info_type  *qmi_if_info_ptr;  
  qmi_dmsi_client_state_type * dms_cl_sp;
  ds_qmi_fw_async_cmd_buf_type  *async_cmd_buf;
#endif /* FEATURE_MMGSDI_SESSION_LIB */
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  response = NULL;
  errval = QMI_ERR_NONE;
#ifndef FEATURE_MMGSDI_SESSION_LIB
  errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
  goto send_result;
#else
  qmi_if_info_ptr = qmi_if_info_get_ptr();
  got_v_in_req = FALSE;
  tlv_len = 0;
  memset(&v_in_req, 0, sizeof(v_in_req));
  memset(&puk_data, 0, sizeof(mmgsdi_data_type));
  memset(&new_pin_data, 0, sizeof(mmgsdi_data_type));
  
  while (*sdu_in)
  {
    if( !qmi_svc_get_tl( sdu_in, &type, &len ) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        if (len <= sizeof(v_in_req))
        {
          got_v_in_req = TRUE;
          value = unblock_pin_info;
          tlv_len =len;
        }
        else
        {
          errval = QMI_ERR_ARG_TOO_LONG;
          goto send_result;
        }
        break;

     default:
        LOG_MSG_INFO2_1 ("Unrecognized TLV type (%d)", type);
        break;
    }

    if(!qmi_svc_validate_type_length( type,
                                          len,
                                          expected_len,
                                          FILE_ID_DS_QMI_DMS,__LINE__) )
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    /*-----------------------------------------------------------------------
      If type is unrecognized, value will be NULL, and qmi_svc_dsm_pullup will 
      free the unrecognized value, i.e. we skip past unrecognized TLVs
    -----------------------------------------------------------------------*/
    if(len != qmi_svc_dsm_pullup( sdu_in, value, len ,FILE_ID_DS_QMI_DMS,__LINE__  ))
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  if(!got_v_in_req)
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  unblock_pin_info_ptr = unblock_pin_info;
  /*-------------------------------------------------------------------------
    Populate the local structure with the values coming in the request and
    also validate the values
  -------------------------------------------------------------------------*/
  v_in_req.pin_id       = *unblock_pin_info_ptr; //get the pin_id
  unblock_pin_info_ptr += sizeof(v_in_req.pin_id);

  switch (v_in_req.pin_id) 
  {
    case DMSI_UIM_PIN1:
      pin_id = MMGSDI_PIN1;
      break;

    case DMSI_UIM_PIN2:
      pin_id = MMGSDI_PIN2;
      break;

    default:
      errval = QMI_ERR_INVALID_PINID;
      goto send_result;
  }
  
  v_in_req.puk_len      = *unblock_pin_info_ptr; //get the puk length
  unblock_pin_info_ptr += sizeof(v_in_req.puk_len);

  if(v_in_req.puk_len > MMGSDI_PIN_MAX_LEN)
  {
    errval = QMI_ERR_ARG_TOO_LONG;
    goto send_result;
  }
   
  (void) memscpy((char*)v_in_req.puk_value,         //get the puk value
                 v_in_req.puk_len,
                (char*)unblock_pin_info_ptr, 
                v_in_req.puk_len);
  unblock_pin_info_ptr += v_in_req.puk_len;

  puk_data.data_len = v_in_req.puk_len;  
  puk_data.data_ptr = v_in_req.puk_value;
  
  v_in_req.new_pin_len  = *unblock_pin_info_ptr; //get the new pin length
  unblock_pin_info_ptr += sizeof(v_in_req.new_pin_len);

  if(v_in_req.new_pin_len > MMGSDI_PIN_MAX_LEN)
  {
    errval = QMI_ERR_ARG_TOO_LONG;
    goto send_result;
  }

  (void) memscpy((char*)v_in_req.new_pin_value,  //get the new pin value
                 v_in_req.new_pin_len,
                (char*)unblock_pin_info_ptr, 
                v_in_req.new_pin_len);

  /*-------------------------------------------------------------------------
    Validate the length of the input tlv to make sure it is consistent with
    the pin length specified within the tlv
  -------------------------------------------------------------------------*/
  expected_tlv_len = sizeof(v_in_req) - 
                      (MMGSDI_PIN_MAX_LEN - v_in_req.puk_len) - 
                      (MMGSDI_PIN_MAX_LEN - v_in_req.new_pin_len);
  if ( (tlv_len < expected_tlv_len) ||
       (tlv_len > (expected_tlv_len + 1)) )
  {
    errval = QMI_ERR_MALFORMED_MSG;
    goto send_result;
  }

  new_pin_data.data_len = v_in_req.new_pin_len;
  new_pin_data.data_ptr = v_in_req.new_pin_value;

  if ((errval = qmi_dmsi_get_uim_status()) != QMI_ERR_NONE)
  {
    goto send_result;
  }

  if (qmi_dmsi_global.sim.pin_info[pin_id].status == QMI_PIN_STATUS_MIN) 
  {
    errval = QMI_ERR_SIM_NOT_INITIALIZED;
    goto send_result;
  }

  dms_cl_sp = (qmi_dmsi_client_state_type*)cl_sp;
  async_cmd_buf = ds_qmi_fw_new_async_cmd_buf(cmd_buf_p, dms_cl_sp->cookie);
  if(async_cmd_buf == NULL)
  {
    errval = QMI_ERR_NO_MEMORY;
    goto send_result;
  }

  mmgsdi_status = mmgsdi_session_unblock_pin(
                                     qmi_if_info_ptr->session_info[QMI_IF_MMGSD_GW_PROV_PRI_SESSION].session_id,
                                     pin_id,
                                     puk_data,
                                     new_pin_data,
                                     qmi_if_mmgsdi_resp_cback, 
                                     (uint32) async_cmd_buf);
  
  if(mmgsdi_status != MMGSDI_SUCCESS)
  { 
    ds_qmi_fw_free_async_cmd_buf(async_cmd_buf);
    errval = qmi_if_map_mmgsdi_status_to_errval(mmgsdi_status,__LINE__);
    goto send_result;
  }
  memset(&v_in_req, 0, sizeof(v_in_req));
  memset(&new_pin_data, 0, sizeof(new_pin_data));
  memset(&puk_data, 0, sizeof(puk_data));
  return QMI_SVC_RESPONSE_PENDING;
#endif /* FEATURE_MMGSDI_SESSION_LIB */

send_result:  
#ifdef FEATURE_MMGSDI_SESSION_LIB
  memset(&v_in_req, 0, sizeof(v_in_req));
  memset(&new_pin_data, 0, sizeof(new_pin_data));
  memset(&puk_data, 0, sizeof(puk_data));
#endif /* FEATURE_MMGSDI_SESSION_LIB */
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;
} /* qmi_dmsi_unblock_uim_pin() */



/*===========================================================================
  FUNCTION QMI_DMSI_CHANGE_UIM_PIN()

  DESCRIPTION
    To change PIN or PIN2.
        
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_change_uim_pin
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

#ifdef FEATURE_MMGSDI_SESSION_LIB
  struct
  {
    uint8  pin_id;
    uint8  old_pin_len;
    uint8  old_pin_value[MMGSDI_PIN_MAX_LEN];
    uint8  new_pin_len;
    uint8  new_pin_value[MMGSDI_PIN_MAX_LEN]; 
  } v_in_req;

#define DMSI_PRM_TYPE_CHANGE_PIN_MAX_SIZE   (3 + 2*MMGSDI_PIN_MAX_LEN)
  uint8    change_pin_info[DMSI_PRM_TYPE_CHANGE_PIN_MAX_SIZE];
  uint8 *  change_pin_info_ptr;
  boolean  got_v_in_req;
  uint8    type;
  uint16   len;
  void *   value;
  uint16    tlv_len;
  uint16             expected_len;
  uint16             expected_tlv_len;
  mmgsdi_pin_enum_type pin_id;
  mmgsdi_data_type    old_pin_data;
  mmgsdi_data_type    new_pin_data;
  mmgsdi_return_enum_type mmgsdi_status;
  qmi_if_info_type  *qmi_if_info_ptr;
  qmi_dmsi_client_state_type * dms_cl_sp;
  ds_qmi_fw_async_cmd_buf_type  *async_cmd_buf;
#endif /* FEATURE_MMGSDI_SESSION_LIB */
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  response = NULL;
  errval = QMI_ERR_NONE;
#ifndef FEATURE_MMGSDI_SESSION_LIB
  errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
  goto send_result;
#else
  got_v_in_req = FALSE;
  tlv_len = 0;
  memset(&old_pin_data, 0, sizeof(mmgsdi_data_type));
  memset(&new_pin_data, 0, sizeof(mmgsdi_data_type));
  
  while (*sdu_in)
  {
    if( !qmi_svc_get_tl( sdu_in, &type, &len ) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        if (len <= sizeof(v_in_req))
        {
          got_v_in_req = TRUE;
          value = change_pin_info;
          tlv_len =len;
        }
        else
        {
          errval = QMI_ERR_ARG_TOO_LONG;
          goto send_result;
        }
        break;

     default:
        LOG_MSG_INFO2_1 ("Unrecognized TLV type (%d)", type);
        break;
    }

    if(!qmi_svc_validate_type_length( type,
                                      len,
                                      expected_len,
                                      FILE_ID_DS_QMI_DMS,__LINE__) )
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    /*-----------------------------------------------------------------------
      If type is unrecognized, value will be NULL, and qmi_svc_dsm_pullup will 
      free the unrecognized value, i.e. we skip past unrecognized TLVs
    -----------------------------------------------------------------------*/
    if(len != qmi_svc_dsm_pullup( sdu_in, value, len ,FILE_ID_DS_QMI_DMS,__LINE__  ))
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  if(!got_v_in_req)
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  change_pin_info_ptr = change_pin_info;

  /*-------------------------------------------------------------------------
    Populate the local structure with the values coming in the request and
    also validate the values
  -------------------------------------------------------------------------*/
  v_in_req.pin_id      = *change_pin_info_ptr;       // get the pin id
  change_pin_info_ptr += sizeof(v_in_req.pin_id);

  switch (v_in_req.pin_id) 
  {
    case DMSI_UIM_PIN1:
      pin_id = MMGSDI_PIN1;
      break;

    case DMSI_UIM_PIN2:
      pin_id = MMGSDI_PIN2;
      break;

    default:
      errval = QMI_ERR_INVALID_PINID;
      goto send_result;
  }

  v_in_req.old_pin_len = *change_pin_info_ptr;    // get the old pin length   
  change_pin_info_ptr += sizeof(v_in_req.old_pin_len);

  if(v_in_req.old_pin_len > MMGSDI_PIN_MAX_LEN)
  {
    errval = QMI_ERR_ARG_TOO_LONG;
    goto send_result;
  }

  (void) memscpy((char*)v_in_req.old_pin_value,   // get the old pin value 
                 v_in_req.old_pin_len,
                (char*)change_pin_info_ptr, 
                v_in_req.old_pin_len);

  change_pin_info_ptr += v_in_req.old_pin_len;   

  old_pin_data.data_ptr = v_in_req.old_pin_value;
  old_pin_data.data_len = v_in_req.old_pin_len;

  v_in_req.new_pin_len = *change_pin_info_ptr;    // get the new pin length
  change_pin_info_ptr += sizeof(v_in_req.new_pin_len);

  if(v_in_req.new_pin_len > MMGSDI_PIN_MAX_LEN)
  {
    errval = QMI_ERR_ARG_TOO_LONG;
    goto send_result;
  }

  (void) memscpy((char*)v_in_req.new_pin_value,  // get the new pin length
                 v_in_req.new_pin_len,
                (char*)change_pin_info_ptr, 
                v_in_req.new_pin_len);   

  /*-------------------------------------------------------------------------
    Validate the length of the input tlv to make sure it is consistent with
    the pin length specified within the tlv
  -------------------------------------------------------------------------*/
  expected_tlv_len = sizeof(v_in_req) - 
                      (MMGSDI_PIN_MAX_LEN - v_in_req.old_pin_len) - 
                      (MMGSDI_PIN_MAX_LEN - v_in_req.new_pin_len);
  if ( (tlv_len < expected_tlv_len) ||
       (tlv_len > (expected_tlv_len + 1)) )
  {
    errval = QMI_ERR_MALFORMED_MSG;
    goto send_result;
  }

  new_pin_data.data_ptr = v_in_req.new_pin_value;
  new_pin_data.data_len = v_in_req.new_pin_len;

  if ((errval = qmi_dmsi_get_uim_status()) != QMI_ERR_NONE)
  {
    goto send_result;
  }

  if (qmi_dmsi_global.sim.pin_info[pin_id].status == QMI_PIN_STATUS_MIN) 
  {
    errval = QMI_ERR_SIM_NOT_INITIALIZED;
    goto send_result;
  }

  qmi_if_info_ptr = qmi_if_info_get_ptr();
  dms_cl_sp = (qmi_dmsi_client_state_type*)cl_sp;
  async_cmd_buf = ds_qmi_fw_new_async_cmd_buf(cmd_buf_p, dms_cl_sp->cookie);
  if(async_cmd_buf == NULL)
  {
    errval = QMI_ERR_NO_MEMORY;
    goto send_result;
  }
  
  mmgsdi_status = mmgsdi_session_change_pin(
                                    qmi_if_info_ptr->session_info[QMI_IF_MMGSD_GW_PROV_PRI_SESSION].session_id,
                                    pin_id,
                                    old_pin_data,
                                    new_pin_data,
                                    qmi_if_mmgsdi_resp_cback, 
                                    (uint32) async_cmd_buf);  
  if(mmgsdi_status != MMGSDI_SUCCESS)
  { 
    ds_qmi_fw_free_async_cmd_buf(async_cmd_buf);
    errval = qmi_if_map_mmgsdi_status_to_errval(mmgsdi_status,__LINE__);
    goto send_result;
  }
  
  memset(&v_in_req, 0, sizeof(v_in_req));// for security
  memset(&new_pin_data, 0, sizeof(new_pin_data));
  memset(&old_pin_data, 0, sizeof(old_pin_data));
  return QMI_SVC_RESPONSE_PENDING;
#endif /* FEATURE_MMGSDI_SESSION_LIB */

send_result: 
#ifdef FEATURE_MMGSDI_SESSION_LIB 
  memset(&v_in_req, 0, sizeof(v_in_req));// for security
  memset(&new_pin_data, 0, sizeof(new_pin_data));
  memset(&old_pin_data, 0, sizeof(old_pin_data));
#endif /* FEATURE_MMGSDI_SESSION_LIB */
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  
  return response;

} /* qmi_dmsi_change_uim_pin() */



/*===========================================================================
  FUNCTION QMI_DMSI_GET_UIM_PIN_STATUS()

  DESCRIPTION
    To get the status for PIN or PIN2.
        
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_get_uim_pin_status
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;
#ifdef FEATURE_MMGSDI_SESSION_LIB
  mmgsdi_return_enum_type mmgsdi_status;
  qmi_if_info_type  *qmi_if_info_ptr;  
  qmi_dmsi_client_state_type * dms_cl_sp;
  ds_qmi_fw_async_cmd_buf_type  *async_cmd_buf;
#endif /* FEATURE_MMGSDI_SESSION_LIB */
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  response = NULL;
  errval = QMI_ERR_NONE;

#ifndef FEATURE_MMGSDI_SESSION_LIB
  errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
  goto send_result;
#else
  if ((errval = qmi_dmsi_get_uim_status()) != QMI_ERR_NONE)
  {
    goto send_result;
  }

  qmi_if_info_ptr = qmi_if_info_get_ptr();
  dms_cl_sp = (qmi_dmsi_client_state_type*)cl_sp;
  async_cmd_buf = ds_qmi_fw_new_async_cmd_buf(cmd_buf_p, dms_cl_sp->cookie);
  if(async_cmd_buf == NULL)
  {
    errval = QMI_ERR_NO_MEMORY;
    goto send_result;
  }
  
  mmgsdi_status = mmgsdi_session_get_all_pin_status(
                                        qmi_if_info_ptr->session_info[QMI_IF_MMGSD_GW_PROV_PRI_SESSION].session_id,
                                        qmi_if_mmgsdi_resp_cback, 
                                        (uint32) async_cmd_buf);


  if(mmgsdi_status != MMGSDI_SUCCESS)
  { 
    ds_qmi_fw_free_async_cmd_buf(async_cmd_buf);	
    errval = qmi_if_map_mmgsdi_status_to_errval(mmgsdi_status,__LINE__);
    goto send_result;
  }

  return QMI_SVC_RESPONSE_PENDING;
#endif /* FEATURE_MMGSDI_SESSION_LIB */

send_result:  
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  
  return response;
} /* qmi_dmsi_get_uim_pin_status() */



/*===========================================================================
  FUNCTION QMI_DMSI_GET_DEVICE_HARDWARE_REV()

  DESCRIPTION
    Gives the device hardware revision
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_get_device_hardware_rev
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  uint8              dev_hw_rev[DMSI_HW_REV_ID_SIZ];
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

  uint32             ver;
//#if defined(T_MSM6500) || defined(T_MSM6250)
// hw_rev_type        new_ver; /* Buffer for the new MSM version information */
//#endif /* defined(T_MSM6500) || defined(T_MSM6250) */
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  memset(dev_hw_rev, 0, sizeof(dev_hw_rev));
    
  response = NULL;
  errval = QMI_ERR_NONE;

  /*-------------------------------------------------------------------------
    Populate the HW revision
  -------------------------------------------------------------------------*/
  LOG_MSG_INFO1_0("dms query device hardware rev");

  ver = DalChipInfo_ChipVersion();

#if 0
  #if defined(T_MSM6500) || defined(T_MSM6250)
    if (ver == 0xFF00)
    {
      /* 0xFF00 is reserved */
      hw_partnum_version(&new_ver);
      #if defined HW_REV_TYPE_V2
        ver = new_ver.hw_register;
      #else /* defined HW_REV_TYPE_V2 */
        ver =  new_ver.partnum << 4;
        ver |= new_ver.version;
      #endif /* defined HW_REV_TYPE_V2 */
    }
  #endif /* defined(T_MSM6500) || defined(T_MSM6250) */
#endif
  
  (void) dsatutil_itoa(ver, dev_hw_rev, DMSI_BASE_HEX);
  dev_hw_rev[DMSI_HW_REV_ID_SIZ-1] = '\0';

  if (FALSE == qmi_svc_put_param_tlv(&response, 
                                     QMI_TYPE_REQUIRED_PARAMETERS,
                                     (uint16) 
                                           strlen((const char *)dev_hw_rev),
                                     dev_hw_rev))
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
  }

  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);
  retval = qmi_svc_put_result_tlv( &response, result, errval );
  CHECK_RETVAL();
   
  return response;
  
} /* qmi_dmsi_get_device_hardware_rev() */



/*===========================================================================
  FUNCTION QMI_DMSI_SET_OPERATING_MODE()

  DESCRIPTION
    Sets the operating mode of the device
        
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_set_operating_mode
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;
  nv_stat_enum_type  nv_status;
  qmi_dmsi_client_state_type *client_sp;

  PACKED struct PACKED_POST
  {
    uint8  operating_mode;
  } v_in_req;

  boolean  got_v_in_req;
  uint8    type;
  uint16   len;
  void *   value;
  uint16   expected_len;

  sys_oprt_mode_e_type      new_oprt_mode;
  qmi_dmsi_oprt_mode_e_type curr_oprt_mode;
  boolean                   got_curr_oprt_mode = FALSE;

  qmi_if_info_type  *qmi_if_info_ptr;
  nv_item_type               *dcc_nv_item_ptr = NULL;
  ds_qmi_fw_async_cmd_buf_type *qmi_resp_buf_p = NULL;
  qmi_dmsi_cmd_buf_type      *cmd_ptr = NULL;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  client_sp = (qmi_dmsi_client_state_type *)cl_sp;
  response = NULL;
  errval = QMI_ERR_NONE;
  got_v_in_req = FALSE;
  /*
   * it is safe to clear this struct here, for no value is expected.
   * this also satisfies lint warnings.
   */
  memset((void *)&v_in_req, 0, sizeof(v_in_req));
  

  /*-------------------------------------------------------------------------
    Allocate temporary memory for the NV item
  -------------------------------------------------------------------------*/
  dcc_nv_item_ptr = (nv_item_type *)
    qmi_svc_ps_system_heap_mem_alloc(sizeof(nv_item_type),
                       FILE_ID_DS_QMI_DMS,__LINE__);
  if( dcc_nv_item_ptr == NULL )
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
    goto send_result;
  }
  
  while (*sdu_in)
  {
    if( !qmi_svc_get_tl( sdu_in, &type, &len ) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        expected_len = sizeof(v_in_req);
        got_v_in_req = TRUE;
        value = (void *) &v_in_req;
        break;

     default:
        LOG_MSG_INFO2_1 ("Unrecognized TLV type (%d)", type);
        break;
    }

    if(!qmi_svc_validate_type_length( type,
                                      len,
                                      expected_len,
                                      FILE_ID_DS_QMI_DMS,__LINE__) )
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    /*-----------------------------------------------------------------------
      If type is unrecognized, value will be NULL, and qmi_svc_dsm_pullup will 
      free the unrecognized value, i.e. we skip past unrecognized TLVs
    -----------------------------------------------------------------------*/
    if(len != qmi_svc_dsm_pullup( sdu_in, value, len ,FILE_ID_DS_QMI_DMS,__LINE__  ))
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  if(!got_v_in_req)
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  /*-------------------------------------------------------------------------
    Populate the local structure with the values coming in the request and
    also validate the values
  -------------------------------------------------------------------------*/
  LOG_MSG_INFO1_0("dms set operating mode");

  new_oprt_mode = SYS_OPRT_MODE_NONE;

  switch(v_in_req.operating_mode)
  {
    case DMSI_OPRT_MODE_PWROFF:
      new_oprt_mode = SYS_OPRT_MODE_PWROFF;
      break;

    case DMSI_OPRT_MODE_FTM :
      new_oprt_mode = SYS_OPRT_MODE_FTM;
      break;

    case DMSI_OPRT_MODE_OFFLINE :
      new_oprt_mode = SYS_OPRT_MODE_OFFLINE;
      break;

    case DMSI_OPRT_MODE_ONLINE :
      new_oprt_mode = SYS_OPRT_MODE_ONLINE;
      TTLBOOTUP(QMI_DMS_ONLINE);

      break;

    case DMSI_OPRT_MODE_LPM :
    case DMSI_OPRT_MODE_PLPM :
    case DMSI_OPRT_MODE_MOLPM :
      new_oprt_mode = SYS_OPRT_MODE_LPM;
      break;

    case DMSI_OPRT_MODE_RESET :
      new_oprt_mode = SYS_OPRT_MODE_RESET ;
      break;

    default:
      /* Any other value is considered invalid */
      errval = QMI_ERR_INVALID_ARG;
	  goto send_result;
      break;
  }

  qmi_if_info_ptr = qmi_if_info_get_ptr();
  /* Check wireless disable switch and prevent mode change to ONLINE or FTM */
  if ( qmi_if_info_ptr->ph_info.wd_switch_on )
  {
    if ((DMSI_OPRT_MODE_ONLINE == v_in_req.operating_mode) ||
        (DMSI_OPRT_MODE_FTM    == v_in_req.operating_mode))
    {
      errval = QMI_ERR_HARDWARE_RESTRICTED;
      LOG_MSG_INFO1_1("Could not change mode to %d with wireless disable switch on",
                      v_in_req.operating_mode);
      goto send_result;
    }
  }

  /*-------------------------------------------------------------------------
                  Update NV LPM flag if mode changes to LPM or Persistent LPM 
   -------------------------------------------------------------------------*/
  if ((DMSI_OPRT_MODE_PLPM == v_in_req.operating_mode) ||
      (DMSI_OPRT_MODE_LPM == v_in_req.operating_mode))
  {
    /*Get current operating mode*/
    if( QMI_ERR_NONE == qmi_dmsi_oprt_mode_from_cm( qmi_if_info_ptr->ph_info.oprt_mode, 
                                                    &curr_oprt_mode ) )
    {
      got_curr_oprt_mode = TRUE;
    }
    else
    {
      LOG_MSG_INFO1_1("get current oprt mode failed sys oprt mode = %d\n",
                      qmi_if_info_ptr->ph_info.oprt_mode);
      got_curr_oprt_mode = FALSE;
    }

    /* Read the nv for LPM persistence flag */
    nv_status = dcc_get_nv_item( NV_LPM_PERSISTENCE_I, dcc_nv_item_ptr );
    switch (nv_status)
    {
      case NV_DONE_S:
        /* Clear nv for LPM or set it for P_LPM only if the nv doesn't match
           the mode request*/
        if (dcc_nv_item_ptr->lpm_persistence != 
              (DMSI_OPRT_MODE_PLPM == v_in_req.operating_mode))
        {
          dcc_nv_item_ptr->lpm_persistence = 
                                 (DMSI_OPRT_MODE_PLPM == v_in_req.operating_mode);
          if (NV_DONE_S != dcc_put_nv_item(NV_LPM_PERSISTENCE_I, dcc_nv_item_ptr))
          {
            errval = QMI_ERR_INTERNAL;
            LOG_MSG_ERROR_1("Could not set NV_LPM_PERSISTENCE_I to %d",
                            dcc_nv_item_ptr->lpm_persistence);
            goto send_result;
          }
        }
        break;

      case NV_NOTACTIVE_S:
          dcc_nv_item_ptr->lpm_persistence = 
                                 (DMSI_OPRT_MODE_PLPM == v_in_req.operating_mode);
          if (NV_DONE_S != dcc_put_nv_item(NV_LPM_PERSISTENCE_I, dcc_nv_item_ptr))
          {
            errval = QMI_ERR_INTERNAL;
            LOG_MSG_ERROR_1("Could not set NV_LPM_PERSISTENCE_I to %d",
                            dcc_nv_item_ptr->lpm_persistence);
            goto send_result;
          }
          break;

      default:
        errval = QMI_ERR_INTERNAL;
        goto send_result;
        /* no need for break after goto */
    }

    /* if current operating mode is known, check for LPM<->P-LPM changes */
    if( got_curr_oprt_mode )
    {
      if  ( (DMSI_OPRT_MODE_LPM == v_in_req.operating_mode  && 
             DMSI_OPRT_MODE_PLPM == curr_oprt_mode )|| 
            (DMSI_OPRT_MODE_PLPM == v_in_req.operating_mode &&
             DMSI_OPRT_MODE_LPM == curr_oprt_mode) )
      { 
        /* if a change has happened, send an IND */
        qmi_dms_generate_oprt_mode_ind(SYS_OPRT_MODE_LPM);
      }
    }
  }

  /* For shutdown/Reset call sys monitor and send immediate response */
  if ( (DMSI_OPRT_MODE_RESET == v_in_req.operating_mode) || (DMSI_OPRT_MODE_PWROFF == v_in_req.operating_mode) )
  {
    if(!cm_rpm_check_reset_allowed())
    {
      errval = QMI_ERR_INTERNAL;
      LOG_MSG_ERROR_0("CM denied Reset/Shutdown");
      goto send_result;
    }

    ds_qmi_cflog_cm_ph_cmd_oprt_mode( new_oprt_mode );
    LOG_MSG_INFO1_1("Triggered DMS OPRT MODE %d by sys mon APIs",v_in_req.operating_mode);

    if(DMSI_OPRT_MODE_RESET == v_in_req.operating_mode)
    {
      sys_m_initiate_shutdown();
    }
    else
    {
      sys_m_initiate_poweroff();

    }

   /* Post a command to DCC task to send event report indication */

    cmd_ptr = (qmi_dmsi_cmd_buf_type *)qmi_dmsi_get_cmd_buf(QMI_CMD_DMS_RESET_SHUTDOWN);
    if( cmd_ptr != NULL)
    {
      cmd_ptr->cmd_id  = QMI_CMD_DMS_RESET_SHUTDOWN;
      cmd_ptr->data.cm_ph.info.oprt_mode = new_oprt_mode;
      dcc_send_cmd_ex(DCC_QMI_CMD, cmd_ptr);
    }
    else
    {
      LOG_MSG_ERROR_0("Out of cmd buf");
    }

    errval = QMI_ERR_NONE;
    goto send_result;
  }

  client_sp->pending_oprt_mode = new_oprt_mode;

  if (SYS_OPRT_MODE_NONE != new_oprt_mode)
    {
    ds_qmi_cflog_cm_ph_cmd_oprt_mode( new_oprt_mode );
    /* ----------------------------------------------------------
     Allocated temp memory for resp_buf_type 
    -----------------------------------------------------------*/
    qmi_resp_buf_p =  (ds_qmi_fw_async_cmd_buf_type*) ds_qmi_fw_new_async_cmd_buf(cmd_buf_p, client_sp->cookie);

    if( qmi_resp_buf_p == NULL )
    {
      LOG_MSG_ERROR_0("Mem alloc from system heap failed.");
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
      goto send_result;
    }

    if ( !cm_ph_cmd_oprt_mode( qmi_dmsi_cm_ph_cb,
                               qmi_resp_buf_p,
                               qmi_if_info_ptr->cm_client_id,
                               new_oprt_mode ) )
    {
      LOG_MSG_ERROR_1 ("Unable to change operating mode, cm_ph_cmd_oprt_mode"
                       " fails in Set Oper Mode Cmd 0x%x", cmd_buf_p);
      ds_qmi_fw_free_async_cmd_buf(qmi_resp_buf_p);
    }
    else
    {
        LOG_MSG_INFO1_3("Sent oprt mode cmd to CM using cmd_buf_p 0x%x resp_buf 0x%x, cookie %d wait for response",
                      cmd_buf_p, qmi_resp_buf_p,qmi_resp_buf_p->cookie);
    }
        LOG_MSG_INFO2_2("Response pending for Set Operating Mode %d\t sent to cm %d",
                    client_sp->pending_oprt_mode, new_oprt_mode);
    PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_item_ptr);
    return QMI_SVC_RESPONSE_PENDING; 
  }

send_result:
  /*-------------------------------------------------------------------------
    Free the temporary memory allocated for NV item.
  -------------------------------------------------------------------------*/
  PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_item_ptr);
  ds_qmi_fw_free_async_cmd_buf(qmi_resp_buf_p);
  result = (errval == QMI_ERR_NONE)?QMI_RESULT_SUCCESS: QMI_RESULT_FAILURE;
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;

} /* qmi_dmsi_set_operating_mode() */

/*===========================================================================
  FUNCTION QMI_DMSI_GET_OPERATING_MODE()

  DESCRIPTION
    Gives the operating mode of the device
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_get_operating_mode
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

  qmi_dmsi_oprt_mode_e_type  operating_mode;
  qmi_if_info_type  *qmi_if_info_ptr;
  
#ifdef FEATURE_DATA_QMI_ADDENDUM
  uint8                      hw_restricted;
#ifdef FEATURE_UNDP_NOTEBOOK_LOCK_IDS
  uint16                     offline_reason;
  nv_item_type              *dcc_nv_item_ptr;
#endif /*FEATURE_UNDP_NOTEBOOK_LOCK_IDS*/
#endif /* FEATURE_DATA_QMI_ADDENDUM */

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  qmi_if_info_ptr = qmi_if_info_get_ptr();
  response = NULL;
  errval = QMI_ERR_NONE;

  operating_mode = DMSI_OPRT_MODE_ONLINE;

#ifdef FEATURE_DATA_QMI_ADDENDUM
  hw_restricted = FALSE;
#ifdef FEATURE_UNDP_NOTEBOOK_LOCK_IDS
  offline_reason = 0;
  dcc_nv_item_ptr = NULL;
#endif /*FEATURE_UNDP_NOTEBOOK_LOCK_IDS*/
#endif /* FEATURE_DATA_QMI_ADDENDUM */
  
  /*-------------------------------------------------------------------------
    Perform query action
  -------------------------------------------------------------------------*/
  LOG_MSG_INFO1_0("dms query current operating mode");

  errval = qmi_dmsi_oprt_mode_from_cm( qmi_if_info_ptr->ph_info.oprt_mode, &operating_mode );

#ifdef FEATURE_DATA_QMI_ADDENDUM
  if (QMI_ERR_NONE == errval)
  {
    /*-------------------------------------------------------------------------
      Check if current operating mode is due to hardware switch
    -------------------------------------------------------------------------*/
    /* check to see if the device is in Wireless Disable */
    hw_restricted = qmi_if_info_ptr->ph_info.wd_switch_on;
    if (hw_restricted)
    {
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        DMSI_PRM_TYPE_HW_RESTRICTED,
                                        sizeof(hw_restricted),
                                        &hw_restricted))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
        goto send_result;
      }
    }
  }

  /*-------------------------------------------------------------------------
    Check if current operating mode is due to 'Offline Reason'
  -------------------------------------------------------------------------*/
#ifdef FEATURE_UNDP_NOTEBOOK_LOCK_IDS
/* The bitmap values UNDP_AMSS_NBLOCK_IND and UNDP_UQCN_NBLOCK_IND and the respective checks 
 * have been removed from GOBI3K AMSS SW since the notebook lock validation is done in boot SW and
 * Images will not be flashed and therefore not loaded, if the notebook lock validation fails
 */
#define UNDP_AMSS_MSMTCH_IND  0x80000000 /*AMSS Mismatch lock*/
#define UNDP_UQCN_MSMTCH_IND  0x20000000 /*UQCN Compatibility check fail */
#define UNDP_UQCN_EFS_FULL_IND  0x10000000 /*UQCN EFS Full*/

#define OFFLINE_REASON_AMSS_MSMTCH  0x0001 /*Host image mis-configuration*/
#define OFFLINE_REASON_UQCN_MSMTCH  0x0004 /*PRI version incompatible*/
#define OFFLINE_REASON_UQCN_EFS_FULL  0x0008 /*Dev memory full*/
  /*-------------------------------------------------------------------------
  Build the response from back to front, optional TLV first
  -------------------------------------------------------------------------*/
  if(DMSI_OPRT_MODE_OFFLINE == operating_mode)
  { 
    /*-------------------------------------------------------------------------
      Allocate temporary memory for the NV item
    -------------------------------------------------------------------------*/
    dcc_nv_item_ptr = (nv_item_type *)
      qmi_svc_ps_system_heap_mem_alloc(sizeof(nv_item_type),
                               FILE_ID_DS_QMI_DMS,__LINE__);
    if( dcc_nv_item_ptr == NULL )
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
      goto send_result;
    }

    if(NV_DONE_S == dcc_get_nv_item(NV_UNDP_NOTEBOOK_INFO_I,
                                    dcc_nv_item_ptr))
    {
      offline_reason = 0;

      if(dcc_nv_item_ptr->undp_notebook_info.undp_notebook_model_num & UNDP_AMSS_MSMTCH_IND)
        offline_reason = offline_reason | OFFLINE_REASON_AMSS_MSMTCH;

      if(dcc_nv_item_ptr->undp_notebook_info.undp_notebook_model_num & UNDP_UQCN_MSMTCH_IND)
        offline_reason = offline_reason | OFFLINE_REASON_UQCN_MSMTCH;

      if(dcc_nv_item_ptr->undp_notebook_info.undp_notebook_model_num & UNDP_UQCN_EFS_FULL_IND)
        offline_reason = offline_reason | OFFLINE_REASON_UQCN_EFS_FULL;

      if(offline_reason != 0)
      {
        if(FALSE == qmi_svc_put_param_tlv(&response,
                                          DMSI_PRM_TYPE_OFFLINE_REASON,
                                          sizeof(offline_reason),
                                          &offline_reason))
        {
          errval = QMI_ERR_NO_MEMORY;
          dsm_free_packet(&response);
          PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_item_ptr);
          goto send_result;
        }
      }
    }
    /*-------------------------------------------------------------------------
      Free the temporary memory allocated for NV item.
    -------------------------------------------------------------------------*/
    PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_item_ptr);
  }
#endif /*FEATURE_UNDP_NOTEBOOK_LOCK_IDS*/
#endif /* FEATURE_DATA_QMI_ADDENDUM */

  if (QMI_ERR_NONE == errval)
  {
    if( FALSE == qmi_svc_put_param_tlv(&response, 
                                       QMI_TYPE_REQUIRED_PARAMETERS,
                                       DMSI_PRM_TYPE_OPRT_MODE_TYPE_LEN,
                                       &operating_mode))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
      goto send_result;
    }
  }

send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  
  return response;
 
} /* qmi_dmsi_get_operating_mode() */


/*===========================================================================
  FUNCTION QMI_DMSI_GET_PRL_VER()

  DESCRIPTION
    Gives the version of the active PRL (Preferred Roaming List) of the device
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_get_prl_ver
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;

  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;
  qmi_if_info_type  *qmi_if_info_ptr;

  #if defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900)
  PACKED struct PACKED_POST
  {
    uint16  prl_ver;
  } v_out_req;

  PACKED struct PACKED_POST
  {
    uint8   prl_only;
  } v_out_prl_info;

    nv_stat_enum_type  nv_status;
    byte               cur_nam = 0;

#if defined(FEATURE_RUIM) && defined(FEATURE_UIM_RUN_TIME_ENABLE)
#ifdef FEATURE_MMGSDI_SESSION_LIB
  mmgsdi_return_enum_type mmgsdi_status;
  qmi_dmsi_client_state_type * dms_cl_sp;
  ds_qmi_fw_async_cmd_buf_type  *async_cmd_buf;
#endif /* FEATURE_MMGSDI_SESSION_LIB */
#endif /* defined(FEATURE_RUIM) && defined(FEATURE_UIM_RUN_TIME_ENABLE) */
#endif /* defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900) */

  nv_item_type      *dcc_nv_item_ptr;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  response = NULL;
  errval = QMI_ERR_NONE;
  qmi_if_info_ptr = qmi_if_info_get_ptr();
  /*-------------------------------------------------------------------------
    Allocate temporary memory for the NV item
  -------------------------------------------------------------------------*/
  dcc_nv_item_ptr = (nv_item_type *)
    qmi_svc_ps_system_heap_mem_alloc(sizeof(nv_item_type),
                             FILE_ID_DS_QMI_DMS,__LINE__);
  if( dcc_nv_item_ptr == NULL )
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
    goto send_result;
  }

#if (!defined(FEATURE_CDMA_800) && !defined(FEATURE_CDMA_1900))
    errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
    goto send_result;
#else /* (!defined(FEATURE_CDMA_800) && !defined(FEATURE_CDMA_1900)) */
  memset(&v_out_req, 0, sizeof(v_out_req));

#if defined(FEATURE_RUIM) && defined(FEATURE_UIM_RUN_TIME_ENABLE)
#ifdef FEATURE_MMGSDI_SESSION_LIB
  dms_cl_sp = (qmi_dmsi_client_state_type*)cl_sp;
  if(dms_cl_sp->subscription_id != DMSI_BIND_SUBS_PRIMARY)
  {
    errval = QMI_ERR_INFO_UNAVAILABLE;
    goto send_result;
  }
  /*-------------------------------------------------------------------------
    Check the RUIM state before querying PRL version
  -------------------------------------------------------------------------*/
  if (NV_RTRE_CONTROL_USE_RUIM == nv_rtre_control())
  {
    if((qmi_if_info_ptr->session_info[QMI_IF_MMGSD_1X_PROV_PRI_SESSION].app_type != MMGSDI_APP_RUIM) &&
       (qmi_if_info_ptr->session_info[QMI_IF_MMGSD_1X_PROV_PRI_SESSION].app_type != MMGSDI_APP_CSIM) )
    {
      errval = QMI_ERR_INFO_UNAVAILABLE;
      LOG_MSG_INFO1_1("RUIM/CSIM session info unavailable (%d)", errval);
      goto send_result;
    }

    if((errval = qmi_dms_get_uim_access_status_ext(MMGSDI_1X_PROV_PRI_SESSION)) != QMI_ERR_NONE)
    {
      LOG_MSG_INFO1_1("RUIM not initialized (%d)", errval);
      goto send_result;
    }
	
    async_cmd_buf = ds_qmi_fw_new_async_cmd_buf(cmd_buf_p, dms_cl_sp->cookie);
    if(async_cmd_buf == NULL)
    {
      errval = QMI_ERR_NO_MEMORY;
      goto send_result;
    }

	mmgsdi_status = mmgsdi_session_read_prl(
                                          qmi_if_info_ptr->session_info[QMI_IF_MMGSD_1X_PROV_PRI_SESSION].session_id,
                                          qmi_if_mmgsdi_resp_cback, 
                                          (uint32) async_cmd_buf);
  

    if(mmgsdi_status != MMGSDI_SUCCESS)
    { 
      ds_qmi_fw_free_async_cmd_buf(async_cmd_buf);	  
      errval = qmi_if_map_mmgsdi_status_to_errval(mmgsdi_status,__LINE__);
      goto send_result;
    }
    if (dcc_nv_item_ptr != NULL) 
    {
       PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_item_ptr);
    }
    return QMI_SVC_RESPONSE_PENDING;
  }
#else /* FEATURE_MMGSDI_SESSION_LIB */
    errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
    goto send_result;
#endif /* FEATURE_MMGSDI_SESSION_LIB */
#endif /* defined(FEATURE_RUIM) && defined(FEATURE_UIM_RUN_TIME_ENABLE) */

  /*-------------------------------------------------------------------------
    Perform query action
  -------------------------------------------------------------------------*/
  LOG_MSG_INFO1_0("dms query prl ver");
  nv_status = dcc_get_nv_item(NV_CURR_NAM_I, dcc_nv_item_ptr);
  if (nv_status != NV_DONE_S || dcc_nv_item_ptr->curr_nam >= NV_MAX_NAMS)
  {
    cur_nam = (byte) CM_NAM_1;
  }
  else
  {
    cur_nam = dcc_nv_item_ptr->curr_nam;
  }
  v_out_req.prl_ver = nv_prl_version(cur_nam);
  if (NV_PRL_VERSION_INVALID == v_out_req.prl_ver)
  {
    errval = QMI_ERR_INVALID_ARG;
    goto send_result;
  }

  v_out_prl_info.prl_only = qmi_if_info_ptr->ph_info.prl_only;

  if( FALSE == qmi_svc_put_param_tlv(&response, 
                                     DMSI_PRM_TYPE_PRL_ONLY,
                                     sizeof(v_out_prl_info),
                                     &v_out_prl_info ) )
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
  }

  if( FALSE == qmi_svc_put_param_tlv(&response, 
                                     QMI_TYPE_REQUIRED_PARAMETERS,
                                     sizeof(v_out_req),
                                     &v_out_req ) )
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
  }
#endif /*(!defined(FEATURE_CDMA_800) && !defined(FEATURE_CDMA_1900))*/

send_result:
  /*-------------------------------------------------------------------------
    Free the temporary memory allocated for NV item.
  -------------------------------------------------------------------------*/
  PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_item_ptr);

  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  
  return response;
 
} /* qmi_dmsi_get_prl_ver() */


/*===========================================================================
  FUNCTION QMI_DMSI_GET_CURRENT_PRL_INFO()

  DESCRIPTION
    Gives current active PRL (Preferred Roaming List) info of the device
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_get_current_prl_info
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;

  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;
  qmi_if_info_type  *qmi_if_info_ptr;

  uint16  prl_ver;
  uint8   prl_only;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  response = NULL;
  errval = QMI_ERR_NONE;

#if (!defined(FEATURE_CDMA_800) && !defined(FEATURE_CDMA_1900))
  errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
  goto send_result;
#endif /* (!defined(FEATURE_CDMA_800) && !defined(FEATURE_CDMA_1900)) */

  qmi_if_info_ptr = qmi_if_info_get_ptr();
  LOG_MSG_INFO2_1("qmi_dmsi_get_current_prl_info, prl_info_valid = %d",qmi_if_info_ptr->prl_info_valid);
  
  if (qmi_if_info_ptr->prl_info_valid != TRUE)
  {
    errval = QMI_ERR_INFO_UNAVAILABLE;
    goto send_result;
  }

  prl_only = qmi_if_info_ptr->ph_info.prl_only;
  if( FALSE == qmi_svc_put_param_tlv(&response, 
                                   DMSI_PRM_TYPE_PRL_INFO_PRL_ONLY,
                                   sizeof(prl_only),
                                   &prl_only ) )
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
    goto send_result;
  }

  prl_ver = qmi_if_info_ptr->ph_info.prl_id;
  if( FALSE == qmi_svc_put_param_tlv(&response, 
                                   DMSI_PRM_TYPE_PRL_INFO_PRL_VER,
                                   sizeof(prl_ver),
                                   &prl_ver) )
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
    goto send_result;
  }

send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  
  return response;
 
} /* qmi_dmsi_get_current_prl_info() */

/*===========================================================================
  FUNCTION QMI_DMSI_GET_SW_VERSION()

  DESCRIPTION
    Gives the software version of the device.
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type*  qmi_dmsi_get_sw_version
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
) 

{
  dsm_item_type *    response;
  char              *sw_version_info = NULL;
  char               sw_ver2[DMSI_SW_VER_MAX_LEN + 1]; /*+1 for NULL char*/
  uint16             offset = 0;
  uint16             len    = 0;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval; 
  nv_stat_enum_type  nv_status;
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  
  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);
  
  response = NULL;
  errval = QMI_ERR_NONE;

  LOG_MSG_INFO1_0(" Get SW Version Info");

  memset((void *)&dcc_nv_item, 0, sizeof(dcc_nv_item));
  nv_status = dcc_get_nv_item(NV_SW_VERSION_INFO_I, &dcc_nv_item);
  if(nv_status == NV_DONE_S)
  {
    sw_version_info = (char *)dcc_nv_item.sw_version_info;
  } 
  else
  {
   /*mob_sw_rev is a global extern variable*/
    sw_version_info = (char*)mob_sw_rev;
  }

  len = (uint16)strlen(sw_version_info);
  
  if((sw_version_info == NULL) || (len == 0))
  {
    errval = QMI_ERR_NOT_PROVISIONED;
    goto send_result;
  }

  /*Max length of sw_version in IDL is 32 bytes only. 
    if exceeds truncate the string from start*/
  if(len > DMSI_SW_VER_MAX_LEN)
  {
   /* sw_version examples, old: "M9615ACETWTAAM5011116.1" (< 32 bytes)
      new : "MPSS.DI.2.0-01022-M8974AAAAANAZM-1" (>  32 bytes)
      truncate first N characters from start i.e., �MPSS.....� 
      output will be DI.2.0-01022-M8974AAAAANAZM-1 */
     memset(sw_ver2, 0, sizeof(sw_ver2));
     offset  = len - DMSI_SW_VER_MAX_LEN;
     (void) strlcpy ( sw_ver2,
                      (char*)(sw_version_info + offset),
                      (DMSI_SW_VER_MAX_LEN + 1) );  /*+1 for null char*/

     /*Send the truncated string in response*/
     sw_version_info = (char*)sw_ver2;
     len = DMSI_SW_VER_MAX_LEN;
  }
  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  if(FALSE == qmi_svc_put_param_tlv(&response, 
                                    QMI_TYPE_REQUIRED_PARAMETERS,
                                    (uint16) len,
                                    sw_version_info))
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
  }
  
  send_result:
   result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                    : QMI_RESULT_FAILURE);
   retval = qmi_svc_put_result_tlv(&response, result, errval);
   CHECK_RETVAL();
       
   return response;

}/* qmi_dmsi_get_sw_version */
/*===========================================================================
  FUNCTION QMI_DMSI_SET_SPC()

  DESCRIPTION
    Updates (Set) the Service Programming Code. After validating the current 
    SPC code. 
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type*  qmi_dmsi_set_spc
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  PACKED struct PACKED_POST
  {
    uint8     current_spc[NV_SEC_CODE_SIZE];
  } v_in_req1;

  PACKED struct PACKED_POST
  {
    uint8     new_spc[NV_SEC_CODE_SIZE];
  } v_in_req2;

  dsm_item_type *               response;
  void *   value;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  uint16   len;
  uint16   expected_len;
  boolean  curr_spc_tlv = FALSE;
  boolean  new_spc_tlv = FALSE;
  uint8    type;
  boolean            retval;
  qmi_if_spc_result_e_type check_spc_result = QMI_SPC_FAIL;
  qmi_if_info_type       *qmi_if_info_ptr;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  memset(&v_in_req1, 0, sizeof(v_in_req1));
  memset(&v_in_req2, 0, sizeof(v_in_req2));

  response = NULL;
  errval = QMI_ERR_NONE;

  LOG_MSG_INFO1_0("Set Service Programming Code");
  while (*sdu_in)
  {
    if( !qmi_svc_get_tl( sdu_in, &type, &len ) )
    {
      continue;
    }
  

    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        expected_len = sizeof(v_in_req1);
        curr_spc_tlv = TRUE;
        value = &v_in_req1;
        break;

      case DMSI_PRM_TYPE_REQUIRED_PRM_NEW_SPC:
        expected_len = sizeof(v_in_req2);
        new_spc_tlv = TRUE;
        value = &v_in_req2;
        break;
      default:
        MSG_ERROR("Unrecognized TLV type (%d)",type,0,0);
        break;
    }

    if(!qmi_svc_validate_type_length( type,
                                          len,
                                          expected_len,
                                          FILE_ID_DS_QMI_DMS,__LINE__) )
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    /*-----------------------------------------------------------------------
      If type is unrecognized, value will be NULL, and qmi_svc_dsm_pullup will 
      free the unrecognized value, i.e. we skip past unrecognized TLVs
    -----------------------------------------------------------------------*/
    if(len != qmi_svc_dsm_pullup( sdu_in, value, len ,FILE_ID_DS_QMI_DMS,__LINE__  ))
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  if ( (FALSE == curr_spc_tlv) || ( FALSE == new_spc_tlv ))
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

   /*-----------------------------------------------------------------------
     Check the supplied SPC is valid
   -----------------------------------------------------------------------*/
   check_spc_result = qmi_if_check_spc((uint8 *)v_in_req1.current_spc, FALSE); 
   
   if(check_spc_result == QMI_SPC_FAIL)
   {
     errval = QMI_ERR_AUTHENTICATION_FAILED;
     goto send_result;
   }

   if(check_spc_result == QMI_SPC_LOCKED)
   {
     errval = QMI_ERR_AUTHENTICATION_LOCK;
     goto send_result;
   }

   if(check_spc_result == QMI_SPC_ERR_INTERNAL)
   {
     errval = QMI_ERR_INTERNAL;
     goto send_result;
   }

   /* store the SPC lock code into NV.*/
   
    memscpy((void *)dcc_nv_item.sec_code.digits, 
            NV_SEC_CODE_SIZE,
           (char *)v_in_req2.new_spc,
           NV_SEC_CODE_SIZE);
   
    if (NV_DONE_S != dcc_put_nv_item(NV_SEC_CODE_I, &dcc_nv_item))
    {
      errval = QMI_ERR_INTERNAL;
      LOG_MSG_ERROR_0("Could not Update new SPC");
      goto send_result;
    }

    /* Update cached value in QMI IF*/
    qmi_if_info_ptr = qmi_if_info_get_ptr();
    qmi_if_info_ptr->nv_sec_code = dcc_nv_item.sec_code;
    /*-------------------------------------------------------------------------
    Build the response from back to front
    No Failure possible upto here so result is success
  -------------------------------------------------------------------------*/
send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  
  return response;
}/* qmi_dmsi_set_spc */
/*===========================================================================
  FUNCTION QMI_DMSI_GET_ACTIVATED_STATE()

  DESCRIPTION
    Gives the activation state of the device
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_get_activated_state
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;
#if defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900)
  PACKED struct PACKED_POST
  {
    uint16  act_state;
  } v_out_req;
#endif /* defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900) */
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  response = NULL;
  errval = QMI_ERR_NONE;

#if !defined(FEATURE_CDMA_800) && !defined(FEATURE_CDMA_1900)
  errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
#else /* defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900) */
  memset(&v_out_req, 0, sizeof(v_out_req));
  v_out_req.act_state = DMSI_ACTIVATION_STATE_NOT_ACTIVATED;

  /*-------------------------------------------------------------------------
    If an activation is in progress return the intermediate state
  -------------------------------------------------------------------------*/
  if ((DMSI_ACTIVATION_STATE_NOT_ACTIVATED!=qmi_dmsi_global.activation_state) &&
      (DMSI_ACTIVATION_STATE_ACTIVATED    !=qmi_dmsi_global.activation_state) &&
      (DMSI_ACTIVATION_STATE_MAX          !=qmi_dmsi_global.activation_state))
  {
    v_out_req.act_state = (uint16) qmi_dmsi_global.activation_state;
  }
  /*-------------------------------------------------------------------------
    Else re-calculate the current activated state of the device to return
  -------------------------------------------------------------------------*/
  else
  {
    v_out_req.act_state = (uint16) qmi_dmsi_calculate_activated_state();
  }
  
  if( FALSE == qmi_svc_put_param_tlv(&response, 
                                     QMI_TYPE_REQUIRED_PARAMETERS,
                                     sizeof(v_out_req),
                                     &v_out_req ) )
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
  }
#endif /* defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900) */
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();

  return response;
  
} /* qmi_dmsi_get_activated_state() */



/*===========================================================================
  FUNCTION QMI_DMSI_ACTIVATE_AUTOMATIC()

  DESCRIPTION
    Performs an automatic service provisioning update with the OTA network.
        
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_activate_automatic
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;

  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;
  qmi_if_info_type  *qmi_if_info_ptr;

#if defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900)
  PACKED struct PACKED_POST
  {
    uint8  len;
    uint8  code[DMSI_ACT_AUTO_CODE_SIZ];
  } v_in_req;

  boolean  got_v_in_req;
  uint8    type;
  uint16   len;
  void *   value;
  uint16             expected_len;

#if defined(FEATURE_OTASP)
  /* used to make the CM call origination request */
  cm_num_s_type                 dial_num;
  cm_cdma_orig_params_s_type    call_orig_params;
  cm_orig_alpha_s_type          alpha_buf;
#endif /* defined(FEATURE_OTASP) */
#endif /* defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900) */

  nv_item_type      *dcc_nv_item_ptr;
  qmi_dmsi_client_state_type * dms_cl_sp;
  qmi_dmsi_client_subs_e_type  sub_id;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  response = NULL;
  errval = QMI_ERR_NONE;
  qmi_if_info_ptr = qmi_if_info_get_ptr();
  dms_cl_sp = NULL;
  sub_id  = 0;

  /*-------------------------------------------------------------------------
    Allocate temporary memory for the NV item
  -------------------------------------------------------------------------*/
  dcc_nv_item_ptr = (nv_item_type *)
    qmi_svc_ps_system_heap_mem_alloc(sizeof(nv_item_type),
                             FILE_ID_DS_QMI_DMS,__LINE__);
  dms_cl_sp = (qmi_dmsi_client_state_type*)cl_sp;

  sub_id = dms_cl_sp->subscription_id;
  if(sub_id != DMSI_BIND_SUBS_PRIMARY) 
  {
    /*As of now CDMA should be camped only on Primary sub*/
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  if( dcc_nv_item_ptr == NULL )
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
    goto send_result;
  }

#if defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900)
  memset(&v_in_req, 0, sizeof(v_in_req));
  got_v_in_req = FALSE;
    
  while (*sdu_in)
  {
    if( !qmi_svc_get_tl( sdu_in, &type, &len ) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        if (len <= sizeof(v_in_req))
        {
          got_v_in_req = TRUE;
          value = (void *) &v_in_req;
        }
        else
        {
          errval = QMI_ERR_ARG_TOO_LONG;
         goto send_result;
        }
        break;

      default:
        LOG_MSG_INFO2_1 ("Unrecognized TLV type (%d)", type);
        break;
    }

    if(!qmi_svc_validate_type_length( type,
                                      len,
                                      expected_len,
                                      FILE_ID_DS_QMI_DMS,__LINE__) )
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    /*-----------------------------------------------------------------------
      If type is unrecognized, value will be NULL, and qmi_svc_dsm_pullup will 
      free the unrecognized value, i.e. we skip past unrecognized TLVs
    -----------------------------------------------------------------------*/
    if(len != qmi_svc_dsm_pullup( sdu_in, value, len ,FILE_ID_DS_QMI_DMS,__LINE__  ))
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  if(!got_v_in_req)
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  /*-------------------------------------------------------------------------
    Perform automatic activation
  -------------------------------------------------------------------------*/
  LOG_MSG_INFO1_0("dms activate automatic");

  /* OTASP requires an activation code, make sure SOMETHING was specified */
  if ((v_in_req.len == 0) || (v_in_req.code[0] == '\0'))
  {
    errval = QMI_ERR_INVALID_ARG;
    goto send_result;
  }

  if (v_in_req.len > DMSI_ACT_AUTO_CODE_SIZ) 
  {
    errval = QMI_ERR_ARG_TOO_LONG;
    goto send_result;
  }

  /* check that the device has minimally been activated */
  if(NV_DONE_S != dcc_get_nv_item (NV_ESN_I, dcc_nv_item_ptr))
  {
    errval = QMI_ERR_NOT_PROVISIONED;
    goto send_result;
  } 
  if (0 == dcc_nv_item_ptr->esn.esn)
  {
    errval = QMI_ERR_NOT_PROVISIONED;
    goto send_result;
  }

  /* If qmi_if_info has not yet been inited, no CM Serving System events have
     been received which means the device has not yet gone online.
     If qmi_if_info has been inited, check the service status (1x-only check
     is fine since the OTASP call will be voice).
     */
  if ((!(qmi_if_info_ptr->inited)) ||
      (qmi_if_info_ptr->inited && 
      (SYS_SRV_STATUS_SRV != qmi_if_info_ptr->ss_info[sub_id].srv_status)))
  {
    errval = QMI_ERR_NO_NETWORK_FOUND;
    goto send_result;
  }

#if defined(FEATURE_OTASP)
  /* default the CM call origination parameters */
  memset(&dial_num, 0, sizeof(cm_num_s_type));
  memset(&alpha_buf, 0, sizeof(cm_orig_alpha_s_type));
  memset(&call_orig_params, CM_CALL_CMD_PARAM_DEFAULT_VALUE, 
                                                    sizeof(call_orig_params));

  cm_num_fill(&dial_num, 
              (uint8 *) v_in_req.code, 
              v_in_req.len, 
              CM_DIGIT_MODE_8BIT_ASCII);

  ds_qmi_cflog_cm_call_cmd_orig_otasp( &dial_num );

  /* create the CM call origination request */
  if (cm_mm_call_cmd_orig(qmi_dms_cm_call_cmd_cb,
                  (void *) cmd_buf_p,
                  qmi_if_info_ptr->cm_client_id,
                  CM_CALL_TYPE_VOICE,
                  CM_SRV_TYPE_AUTOMATIC,
                  NULL,
                  &dial_num,
                  &alpha_buf,
                  &call_orig_params, /* cdma orig params */
                  NULL, /* GW CS parameters */
                  NULL, /* GW PS parameters */
                  &qmi_dmsi_global.voice_call_id
                  ))
  {
    PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_item_ptr);
    return QMI_SVC_RESPONSE_PENDING; 
  }
  else
  {
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }
#else /* defined(FEATURE_OTASP) */
  errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
#endif /* defined(FEATURE_OTASP) */
#else /* defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900) */
  errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
#endif /* defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900) */
send_result:	 
  /*-------------------------------------------------------------------------
    Free the temporary memory allocated for NV item.
  -------------------------------------------------------------------------*/
  PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_item_ptr);

  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();

  return response;
  
} /* qmi_dmsi_activate_automatic() */



/*===========================================================================
  FUNCTION QMI_DMSI_ACTIVATE_MANUAL()

  DESCRIPTION
    Performs a manual service provisioning with data provided by the user
        
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_activate_manual
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;
  
#if defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900)
  PACKED struct PACKED_POST
  {
    uint8  spc[DMSI_SPC_SIZ];
    uint16 sys_id;
    uint8  mdn_len;
    uint8  mdn_data[DMSI_NUMB_PCS_SIZ];
    uint8  min_len;
    uint8  min_data[DMSI_NUMB_PCS_SIZ];
  } v_in_req;

  PACKED struct PACKED_POST
  {
    uint8  mn_ha_len;
    uint8  mn_ha_data[DMSI_MN_KEY_SIZ];
  } v_in_opt2;

  PACKED struct PACKED_POST
  {
    uint8  mn_aaa_len;
    uint8  mn_aaa_data[DMSI_MN_KEY_SIZ];
  } v_in_opt3;

  PACKED struct PACKED_POST
  {
    uint16 prl_len_total;
    uint16 prl_len_segment;
    uint8  prl_seq_num;
    uint8  prl_data[DMSI_TLV_SEGMENT_LEN_MAX];
  } v_in_opt4;

  boolean  got_v_in_req;
  boolean  got_v_in_opt2;
  boolean  got_v_in_opt3;
  boolean  got_v_in_opt4;
  uint8    type;
  uint16   len;
  void *   value;
  uint16             expected_len;
  int32    temp;
  uint16   temp_len;	

  qmi_if_spc_result_e_type      check_spc_result;
  dword                         qmi_dmsi_manual_min1;
  word                          qmi_dmsi_manual_min2;
  nv_items_enum_type            item_id;
  nv_stat_enum_type             nv_status;
  nv_ds_mip_ss_user_prof_type * ss_profile;
  byte *                        data_ptr;
  char * min_validate_ptr ;
  byte                          curr_nam;
  byte                          curr_index;
  size_t                        copy_len = 0;
  char default_nai[QMI_NV_ITEM_SIZE_MAX];

#ifdef FEATURE_SD20
  word                     prl_version = PRL_DEFAULT_VER;
#endif

  boolean write_all_parameters;
#endif  /* defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900) */

  nv_item_type      *dcc_nv_item_ptr;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  response = NULL;
  errval = QMI_ERR_NONE;

#if (defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900))
  /* init value here since this variable is used after send_result check */
  got_v_in_opt4 = FALSE;
#endif  /* defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900) */

  /*-------------------------------------------------------------------------
    Allocate temporary memory for the NV item
  -------------------------------------------------------------------------*/
  dcc_nv_item_ptr = (nv_item_type *)
    qmi_svc_ps_system_heap_mem_alloc(sizeof(nv_item_type),
                             FILE_ID_DS_QMI_DMS,__LINE__);
  if( dcc_nv_item_ptr == NULL )
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
    goto send_result;
  }

#if (defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900))
  memset(&v_in_req, 0, sizeof(v_in_req));
  memset(&v_in_opt2, 0, sizeof(v_in_opt2));
  memset(&v_in_opt3, 0, sizeof(v_in_opt3));
  memset(&v_in_opt4, 0, sizeof(v_in_opt4));

  got_v_in_req = FALSE;
  got_v_in_opt2 = FALSE;
  got_v_in_opt3 = FALSE;

  memset(default_nai, 0, sizeof(default_nai));

#if defined(FEATURE_RUIM) && defined(FEATURE_UIM_RUN_TIME_ENABLE)
  /*-------------------------------------------------------------------------
    Changing provisioning cannot be done on a RUIM card. Do not change any
    values, only allow the UQCN processing.
  -------------------------------------------------------------------------*/
  if (NV_RTRE_CONTROL_USE_RUIM == nv_rtre_control())
  {
    LOG_MSG_INFO1_0("Cannot change ADM-protected values on RUIM");
    errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
    goto send_result;
  }
#endif /* defined(FEATURE_RUIM) && defined(FEATURE_UIM_RUN_TIME_ENABLE) */
  
  while (*sdu_in)
  {
    if( !qmi_svc_get_tl( sdu_in, &type, &len ) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        if (len <= sizeof(v_in_req))
        {
          got_v_in_req = TRUE;
          value = (void *) &v_in_req;
        }
        else
        {
          errval = QMI_ERR_ARG_TOO_LONG;
          goto send_result;
        }
        break;

      case DMSI_PRM_TYPE_MN_HA:
        if (len <= sizeof(v_in_opt2))
        {
          got_v_in_opt2 = TRUE;
          value = (void *) &v_in_opt2;
        }
        else
        {
          errval = QMI_ERR_ARG_TOO_LONG;
          goto send_result;
        }
        break;

      case DMSI_PRM_TYPE_MN_AAA:
        if (len <= sizeof(v_in_opt3))
        {
          got_v_in_opt3 = TRUE;
          value = (void *) &v_in_opt3;
        }
        else
        {
          errval = QMI_ERR_ARG_TOO_LONG;
          goto send_result;
        }
        break;

      case DMSI_PRM_TYPE_PRL:
        if (len <= sizeof(v_in_opt4))
        {
          got_v_in_opt4 = TRUE;
          value = (void *) &v_in_opt4;
        }
        else
        {
          errval = QMI_ERR_ARG_TOO_LONG;
          goto send_result;
        }
        break;

      default:
        LOG_MSG_INFO2_1 ("Unrecognized TLV type (%d)", type);
        break;
    }

    if(!qmi_svc_validate_type_length( type,
                                      len,
                                      expected_len,
                                      FILE_ID_DS_QMI_DMS,__LINE__) )
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    if (type == QMI_TYPE_REQUIRED_PARAMETERS) 
    {
      /* Parse the mandatory params into v_in_req one by one since it involves var len MDN and MIN */
      //SPC
      temp_len=DMSI_SPC_SIZ;
      if(temp_len != qmi_svc_dsm_pullup( sdu_in, value, temp_len ,
                           FILE_ID_DS_QMI_DMS,__LINE__  ))
      {
        errval = QMI_ERR_MALFORMED_MSG;
        goto send_result;
      }

      // Home SID
      temp = dsm_pull16(sdu_in);
      if (-1 == temp) 
      {
        LOG_MSG_INFO2_0 ("Invalid length in TLV, while parsing mandatory Home SID in qmi_dmsi_activate_manual");
        errval = QMI_ERR_MALFORMED_MSG;
        goto send_result;
      }
      v_in_req.sys_id = ntohs(temp);

      //MDN
      temp = dsm_pull8(sdu_in);
      if (-1 == temp) 
      {
        LOG_MSG_INFO2_0 ("Invalid length in TLV, while parsing mandatory MDN len in qmi_dmsi_activate_manual");
        errval = QMI_ERR_MALFORMED_MSG;
        goto send_result;
      }
      v_in_req.mdn_len = (uint8) temp;
      /* Extract upto next 15 bytes for MDN, if greater than DMSI_NUMB_PCS_SIZ there is an invalid arg returned later!*/ 
      temp_len = MIN(v_in_req.mdn_len, DMSI_NUMB_PCS_SIZ);
      value = &v_in_req.mdn_data[0];
      if(temp_len != qmi_svc_dsm_pullup( sdu_in, value, temp_len ,
                                   FILE_ID_DS_QMI_DMS,__LINE__  ))
      {
        errval = QMI_ERR_MALFORMED_MSG;
        goto send_result;
      }

      //MIN 
      temp = dsm_pull8(sdu_in);
      if (-1 == temp) 
      {
        LOG_MSG_INFO2_0 ("Invalid length in TLV, while parsing mandatory MIN len in qmi_dmsi_activate_manual");
        errval = QMI_ERR_MALFORMED_MSG;
        goto send_result;
      }
      v_in_req.min_len = (uint8) temp;

      /* Extract upto next 10 bytes for MIN, if greater than DMSI_NUMB_PCS_SIZ there is an invalid arg returned later!*/ 
      temp_len = MIN(v_in_req.min_len, DMSI_MIN_SIZ);
      value = &v_in_req.min_data[0];
      if(temp_len != qmi_svc_dsm_pullup( sdu_in, value, temp_len ,
                                   FILE_ID_DS_QMI_DMS,__LINE__  ))
      {
        errval = QMI_ERR_MALFORMED_MSG;
        goto send_result;
      }
    }
    else
    {
      /* For remaining items (optional TLVs) it is ok to use qmi_svc_dsm_pullup */
      /*-----------------------------------------------------------------------
      If type is unrecognized, value will be NULL, and qmi_svc_dsm_pullup will 
      free the unrecognized value, i.e. we skip past unrecognized TLVs
      -----------------------------------------------------------------------*/
      if(len != qmi_svc_dsm_pullup( sdu_in, value, len ,FILE_ID_DS_QMI_DMS,__LINE__  ))
      {
        errval = QMI_ERR_MALFORMED_MSG;
        goto send_result;
      }
    }
  }

  if(!got_v_in_req)
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  /*-------------------------------------------------------------------------
    Populate the local structure with the values coming in the request and
    also validate the values
  -------------------------------------------------------------------------*/
  LOG_MSG_INFO1_1("dms activate manual, nv_allowed_invalid_spc = %d",qmi_dmsi_global.nv_allow_invalid_spc);

  write_all_parameters = TRUE;
  /* check if configured to allow writing the PRL only */
  if (TRUE == qmi_dmsi_global.nv_allow_invalid_spc)
  {
    /* if allowed, check the specified SPC against the 'invalid' one to see if
       client is trying to write only the PRL */
    write_all_parameters = strncmp((char *) v_in_req.spc, 
                                       SPECIAL_INVALID_SPC, 
                                       strlen(SPECIAL_INVALID_SPC));
  }

  /* if the request is to write only the PRL (special SPC given), no other TLVs
     should be specified in the request as they must have the real SPC */
  LOG_MSG_INFO2_1("Value of write_all_parameters is %d",write_all_parameters);
  if (!write_all_parameters)
  {
    if (got_v_in_opt2 || got_v_in_opt3)
    {
      LOG_MSG_INFO2_0("MIP TLVs not allowed with special SPC");
      errval = QMI_ERR_AUTHENTICATION_FAILED;
      goto send_result;
    }
  }
  else
  {
    /* Check SPC first , no need to validate values and give any special err for
       SPC, make it obscure */
    check_spc_result = qmi_if_check_spc((uint8 *)v_in_req.spc, TRUE); 
    LOG_MSG_INFO2_1("Value of check_spc_result is %d",check_spc_result);
  
    if(check_spc_result == QMI_SPC_FAIL)
    {
      errval = QMI_ERR_AUTHENTICATION_FAILED;
      goto send_result;
    }

    if(check_spc_result == QMI_SPC_LOCKED)
    {
      errval = QMI_ERR_AUTHENTICATION_LOCK;
      goto send_result;
    }

    if(check_spc_result == QMI_SPC_ERR_INTERNAL)
    {
      errval = QMI_ERR_INTERNAL;
      goto send_result;
    }
  }

  /* ------------------------------------------------------------------
  Validate PARAMs here first ... PRL, MN-HA and MN-AAA keys, MIN, MDN 
  --------------------------------------------------------------------*/
  if(got_v_in_opt2)/* If MN-HA TLV received validate its length */
  {
    if( (v_in_opt2.mn_ha_len==0) || (v_in_opt2.mn_ha_len > DMSI_MN_KEY_SIZ) )
	  {
	    errval = QMI_ERR_INVALID_ARG;
	    LOG_MSG_ERROR_0("Invalid MN HA len");
	    goto send_result;
	  }
  }

  if(got_v_in_opt3) /* If MN-HA TLV received validate its length */
  {
    if( (v_in_opt3.mn_aaa_len==0) || (v_in_opt3.mn_aaa_len > DMSI_MN_KEY_SIZ) )
    {
      errval = QMI_ERR_INVALID_ARG;
      LOG_MSG_ERROR_0("Invalid MN AAA len");
      goto send_result;
    }
  }

  if (write_all_parameters)
  {
    /* Validate MIN length and contents */
    min_validate_ptr = (char *)&v_in_req.min_data[0];
    if(v_in_req.min_len != DMSI_MIN_SIZ)
    {
      errval = QMI_ERR_INVALID_ARG;
      LOG_MSG_ERROR_2("Invalid MIN length, passed in %d but must be %d",
                      v_in_req.min_len, DMSI_MIN_SIZ);
      goto send_result;
    }
    else
    {
      for (temp=0; temp<v_in_req.min_len; temp++ ) 
      {
        if ( (*(min_validate_ptr+temp) < '0') || 
             (*(min_validate_ptr+temp) > '9') )
        {
          errval = QMI_ERR_INVALID_ARG;
          LOG_MSG_ERROR_0("Invalid MIN contents (0-9)");
          goto send_result;
        }
      }
    }
  }

  curr_nam = (byte) CM_NAM_1;
  nv_status = dcc_get_nv_item(NV_CURR_NAM_I, dcc_nv_item_ptr);
  if (nv_status == NV_DONE_S && dcc_nv_item_ptr->curr_nam < NV_MAX_NAMS)
  {
    curr_nam = (byte) dcc_nv_item_ptr->curr_nam;
  }
  LOG_MSG_INFO3_1("Updating NAM %d", curr_nam);

  curr_index = 0;
  /* read the active MIP profile index and verify it is within the valid range*/
  nv_status = dcc_get_nv_item(NV_DS_MIP_ACTIVE_PROF_I, dcc_nv_item_ptr);
  if ((NV_DONE_S == nv_status) && 
      (NV_DS_MIP_MAX_NUM_PROF > dcc_nv_item_ptr->ds_mip_active_prof))
  {
    curr_index = dcc_nv_item_ptr->ds_mip_active_prof;
  }
  LOG_MSG_INFO3_1("Updating MIP profile %d", curr_index);

  if(got_v_in_opt4) /* If PRL TLV recvd validate it here */
  {
    /* if a transfer series is not in progress */
    if (NULL == qmi_dmsi_global.prl_info.cl_sp)
    {
      /* reset all variables */
      qmi_dmsi_reset_prl_series_info();

      /* set the current control point calling this function */
      qmi_dmsi_global.prl_info.cl_sp = 
                  (qmi_common_client_state_type *)cl_sp;
      
      /* and the total length to that provided */
      qmi_dmsi_global.prl_info.len_total = v_in_opt4.prl_len_total;
    }

    /* check that this is the control point that started the series */
    else if (qmi_dmsi_global.prl_info.cl_sp != 
                  ((qmi_common_client_state_type *)cl_sp))
    {
      errval = QMI_ERR_DEVICE_IN_USE;
      LOG_MSG_ERROR_0("PRL write already in progress");
      goto send_result;
    }

    /* check that the total prl length is not too large */
    if (NV_ROAMING_LIST_MAX_SIZE <= v_in_opt4.prl_len_total)
    {
      errval = QMI_ERR_ARG_TOO_LONG;
      LOG_MSG_ERROR_0("PRL total len");
      goto send_result;
    }
    
    /*  check the total PRL length is not 0 or total PRL size is
        different than previous requests */
    if ((0 == v_in_opt4.prl_len_total) || 
        (qmi_dmsi_global.prl_info.len_total != v_in_opt4.prl_len_total))
    {
      errval = QMI_ERR_INVALID_ARG;
      LOG_MSG_ERROR_0("Invalid PRL total len");
      goto send_result;
    }
    
    /* check that the segment length is not too large */
    if (DMSI_TLV_SEGMENT_LEN_MAX <= v_in_opt4.prl_len_segment)
    {
      errval = QMI_ERR_SEGMENT_TOO_LONG;
      LOG_MSG_ERROR_0("PRL segment len");
      goto send_result;
    }
    
    /*  check the Segment length is not 0 or the total received length
        is not large then total prl length  */
    if ((0 == v_in_opt4.prl_len_segment) || 
        (v_in_opt4.prl_len_total < 
           (qmi_dmsi_global.prl_info.len_received + v_in_opt4.prl_len_segment)))
    {
      errval = QMI_ERR_INVALID_ARG;
      LOG_MSG_ERROR_0("Invalid PRL segment len");
      goto send_result;
    }

    /* check that the segment sequence number is correct */
    if (qmi_dmsi_global.prl_info.seq_num != v_in_opt4.prl_seq_num)
    {
      errval = QMI_ERR_SEGMENT_ORDER;
      LOG_MSG_ERROR_1("PRL segment number (%d)", v_in_opt4.prl_seq_num);
      goto send_result;
    }

    /* If this is the first segment alloc memory for prl data */
    if(qmi_dmsi_global.prl_info.seq_num == 0)
    {
      qmi_dmsi_global.prl_info.data = 
        ps_system_heap_mem_alloc(qmi_dmsi_global.prl_info.len_total);
      if(NULL == qmi_dmsi_global.prl_info.data)
      {
        errval = QMI_ERR_NO_MEMORY;
        LOG_MSG_ERROR_0("No memory to write PRL info");
        goto send_result;
      }
      memset(qmi_dmsi_global.prl_info.data, 0, qmi_dmsi_global.prl_info.len_total);
    }

    /* append the current PRL segment */
    memscpy(&qmi_dmsi_global.prl_info.data[qmi_dmsi_global.prl_info.len_received],
            qmi_dmsi_global.prl_info.len_total - qmi_dmsi_global.prl_info.len_received,
            &v_in_opt4.prl_data[0], 
            v_in_opt4.prl_len_segment);

    /* update the PRL series variables with the information provided */
    qmi_dmsi_global.prl_info.len_received += v_in_opt4.prl_len_segment;
    qmi_dmsi_global.prl_info.seq_num++;

    /* check that all the PRL segments have been written */
    if (qmi_dmsi_global.prl_info.len_total == 
                                          qmi_dmsi_global.prl_info.len_received)
    {
      LOG_MSG_INFO2_0("writing PRL");
#ifdef FEATURE_SD20
#ifdef FEATURE_EXTENDED_PRL
      /* Get PRL protocol revision from NV. Validate PRL revision from nv first
      ** then with all other available protocol revisions next.
      ** Update revision in nv if returned revision does not match that from nv.
      */
  
      nv_status = dcc_get_nv_item(NV_SSPR_P_REV_I, dcc_nv_item_ptr);
      if(nv_status != NV_DONE_S)
      {
        dcc_nv_item_ptr->sspr_p_rev = PRL_SSPR_P_REV_INVALID;
        sspr_p_rev = PRL_SSPR_P_REV_1;
      }
      else
      {
        sspr_p_rev = (prl_sspr_p_rev_e_type)dcc_nv_item_ptr->sspr_p_rev;
      }
#endif /* FEATURE_EXTENDED_PRL */

      /* Validate PRL now */
      if (PRL_VALID != prl_validate( qmi_dmsi_global.prl_info.data,
                                     &prl_version,
                                     &sspr_p_rev,
                                     TRUE))
      {
        errval = QMI_ERR_INVALID_ARG;
        LOG_MSG_ERROR_0("PRL invalid qmi_dmsi_activate_manual");
        goto send_result;
      }
      else
      {
        if(qmi_dmsi_global.prl_info.len_received > NV_ROAMING_LIST_MAX_SIZE)
        {
          errval = QMI_ERR_ARG_TOO_LONG;
          LOG_MSG_ERROR_1("PRL received len > max (%d)",
                          NV_ROAMING_LIST_MAX_SIZE);
          goto send_result;
        }

        memset(&nv_pr_list, 0, sizeof(nv_pr_list));
        memscpy(nv_pr_list.roaming_list, 
                qmi_dmsi_global.prl_info.len_received,
               qmi_dmsi_global.prl_info.data, 
               qmi_dmsi_global.prl_info.len_received);
        nv_pr_list.nam = curr_nam;
        nv_pr_list.prl_version = prl_version;
        nv_pr_list.size = qmi_dmsi_global.prl_info.len_received * 8;
        nv_pr_list.valid = TRUE;

#ifdef FEATURE_IS683A_PRL
        item_id = NV_ROAMING_LIST_683_I;
#else /* FEATURE_IS683A_PRL */
        item_id = NV_ROAMING_LIST_I;
#endif /* FEATURE_IS683A_PRL */
        if (NV_DONE_S != dcc_put_nv_item(item_id, (nv_item_type*)&nv_pr_list))
        {
          errval = QMI_ERR_INTERNAL;
          goto send_result;
        }
      }
      
#ifdef FEATURE_EXTENDED_PRL
      if ( sspr_p_rev != dcc_nv_item_ptr->sspr_p_rev) 
      {
        dcc_nv_item_ptr->sspr_p_rev = sspr_p_rev;
        if (NV_DONE_S != dcc_put_nv_item (NV_SSPR_P_REV_I, dcc_nv_item_ptr))
        {
          errval = QMI_ERR_INTERNAL;
          goto send_result;
        }
      }
#endif /* FEATURE_EXTENDED_PRL */

      /* series finished, free variables so it can be restarted again */
      qmi_dmsi_reset_prl_series_info();
#else /* FEATURE_SD20 */
      errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
      goto send_result;
#endif /* FEATURE_SD20 */
    }
  }/* If PRL TLV recvd validate it here */

  /* ------------------------------------------------------------------
     Commit mandatory items to NV
  --------------------------------------------------------------------*/
  if (write_all_parameters)
  {
    LOG_MSG_INFO2_0("writing manatory TLV");
    /* MDN, this also validates len and content so not done before */
    memset(dcc_nv_item_ptr, 0, sizeof(nv_item_type));
    dcc_nv_item_ptr->mob_dir_number.nam = curr_nam;
    if (!qmi_dmsi_mobnum_text_to_pcs((char *)v_in_req.mdn_data, 
                                   v_in_req.mdn_len, 
                                   &dcc_nv_item_ptr->mob_dir_number))
    {
      errval = QMI_ERR_INVALID_ARG;
      LOG_MSG_ERROR_0("Invalid MDN param len or contents");
      goto send_result;
    }
    if( NV_DONE_S != dcc_put_nv_item( NV_DIR_NUMBER_PCS_I,
                                    dcc_nv_item_ptr ) )
    {
      errval = QMI_ERR_INTERNAL;
      goto send_result;
    }

    /* MIN */
    if(!qmi_dmsi_min_text_to_min12((char *)v_in_req.min_data, 
                                   v_in_req.min_len, 
                                   &qmi_dmsi_manual_min1, 
                                   &qmi_dmsi_manual_min2))
    {
      errval = QMI_ERR_INTERNAL;
      LOG_MSG_ERROR_0("Invalid MIN len or contents");
      goto send_result;
    }

    /* Write MIN1, both CDMA MIN1 and Analog */
    memset(dcc_nv_item_ptr, 0, sizeof(nv_item_type));
    dcc_nv_item_ptr->min1.nam = curr_nam;
    dcc_nv_item_ptr->min1.min1[NV_ANALOG_MIN_INDEX] = qmi_dmsi_manual_min1;
    dcc_nv_item_ptr->min1.min1[NV_CDMA_MIN_INDEX]   = qmi_dmsi_manual_min1;
    if( NV_DONE_S != dcc_put_nv_item( NV_MIN1_I, dcc_nv_item_ptr ) )
    {
    	errval = QMI_ERR_INTERNAL;
    	goto send_result;
    }

    /* Write MIN2, both CDMA and Analog */
    dcc_nv_item_ptr->min2.nam = curr_nam;
    dcc_nv_item_ptr->min2.min2[NV_ANALOG_MIN_INDEX] = qmi_dmsi_manual_min2;
    dcc_nv_item_ptr->min2.min2[NV_CDMA_MIN_INDEX]   = qmi_dmsi_manual_min2;
    if( NV_DONE_S != dcc_put_nv_item( NV_MIN2_I, dcc_nv_item_ptr ) )
    {
      errval = QMI_ERR_INTERNAL;
      goto send_result;
    }

    /* Update ACCOLC with last digit of MIN (ASCII digit to binary) */
    dcc_nv_item_ptr->accolc.nam = curr_nam;  
    dcc_nv_item_ptr->accolc.ACCOLCpClass[NV_CDMA_MIN_INDEX] = 
                         v_in_req.min_data[v_in_req.min_len - 1] - '0';
    if( NV_DONE_S != dcc_put_nv_item( NV_ACCOLC_I,
                                    dcc_nv_item_ptr ) )
    {
      errval = QMI_ERR_INTERNAL;
      goto send_result;
    }

    /* Home Sid , no validation done for home SID */
#ifdef FEATURE_SSPR_ENHANCEMENTS
    /* NV Item #259 */
    dcc_nv_item_ptr->home_sid_nid.nam = curr_nam;
    /* Read this NV Item to check if it is already active */
    nv_status = dcc_get_nv_item( NV_HOME_SID_NID_I, dcc_nv_item_ptr);
    if ((nv_status == NV_DONE_S) || (nv_status == NV_NOTACTIVE_S))                                   
    {
      // Modify only the SID and write it to NV
      dcc_nv_item_ptr->home_sid_nid.pair[curr_nam].sid = v_in_req.sys_id; 
      nv_status = dcc_put_nv_item( NV_HOME_SID_NID_I, dcc_nv_item_ptr);
      if(nv_status != NV_DONE_S)                                  
      {
    	  errval = QMI_ERR_INTERNAL;
    	  goto send_result;
      }
    }
    else
    {
      errval = QMI_ERR_INTERNAL;
      goto send_result;
    }
#else /* FEATURE_SSPR_ENHANCEMENTS */
    /* NV Item #38 */
    dcc_nv_item_ptr->sid_nid.nam = curr_nam;
    /* Read this NV Item to check if it is already active */
    nv_status = dcc_get_nv_item(NV_SID_NID_I, dcc_nv_item_ptr);
    if((nv_status == NV_DONE_S) || (nv_status == NV_NOTACTIVE_S))                                 
    {
      dcc_nv_item_ptr->sid_nid.pair[NV_CDMA_MIN_INDEX][curr_nam].sid = 
                                                                v_in_req.sys_id;
      nv_status = dcc_put_nv_item(NV_SID_NID_I, dcc_nv_item_ptr);
      if(nv_status != NV_DONE_S)                                  
      {
        errval = QMI_ERR_INTERNAL;
        goto send_result;
      }
    }
    else
    {
      errval = QMI_ERR_INTERNAL;
      goto send_result;
    }
#endif /* FEATURE_SSPR_ENHANCEMENTS */

    // Only execute this on the last PRL segment or non PRL request
    if (got_v_in_opt4 && qmi_dmsi_global.prl_info.len_received)  {
       goto send_result;
    }

    /*--------------------------------------------------------------------------
       Update default SIP/MIP NAIs to MDN@CONSTANT as per requirements 
    --------------------------------------------------------------------------*/
    /* Once provisioning is completed need to update the other provisioning
       values that are based on ones that may have changed.  NAI includes the
       MDN so anytime the MDN changes it should be updated.*/
    memset(default_nai, 0, sizeof(default_nai));
    if (QMI_NV_STATUS_OK != qmi_nv_read(QMI_NV_ITEM_DEFAULT_NAI, 0,
                                        (void *)default_nai,
                                        sizeof(default_nai)))
    {
      memset(default_nai, 0, sizeof(default_nai));
    }
    memset((void *)&dcc_nv_item_ptr->ppp_user_id.user_id[0], 0, 
           sizeof(dcc_nv_item_ptr->ppp_user_id.user_id));
    copy_len = memscpy((void *)&dcc_nv_item_ptr->ppp_user_id.user_id[0],
                       NV_MAX_PPP_USER_ID_LENGTH,
                       (void *)v_in_req.mdn_data, 
                       v_in_req.mdn_len);
    if(v_in_req.mdn_len < NV_MAX_PPP_USER_ID_LENGTH )
    {
      copy_len += memscpy((void *)&dcc_nv_item_ptr->ppp_user_id.user_id[v_in_req.mdn_len],
                          NV_MAX_PPP_USER_ID_LENGTH - v_in_req.mdn_len,
                          (void *)default_nai, 
                          strlen(default_nai) + 1);
    }
    dcc_nv_item_ptr->ppp_user_id.user_id_len = copy_len;

    nv_status = dcc_put_nv_item (NV_PPP_USER_ID_I, dcc_nv_item_ptr);
    if (nv_status != NV_DONE_S)
    {
      errval = QMI_ERR_INTERNAL;
      goto send_result;
    }
  
    nv_status = dcc_put_nv_item (NV_HDR_AN_AUTH_USER_ID_LONG_I, dcc_nv_item_ptr);
    if (nv_status != NV_DONE_S)
    {
      errval = QMI_ERR_INTERNAL;
      goto send_result;
    }
  
    dcc_nv_item_ptr->ds_mip_gen_user_prof.index = curr_index;
    nv_status = dcc_get_nv_item (NV_DS_MIP_GEN_USER_PROF_I, dcc_nv_item_ptr);
    if ((nv_status != NV_DONE_S) && (nv_status != NV_NOTACTIVE_S))
    {
      errval = QMI_ERR_INTERNAL;
      goto send_result;
    }
    else
    {
      copy_len = memscpy((void *)&dcc_nv_item_ptr->ds_mip_gen_user_prof.nai[0],
                         NV_MAX_NAI_LENGTH,
                         (void *)v_in_req.mdn_data, 
                         v_in_req.mdn_len);
     if(v_in_req.mdn_len < NV_MAX_NAI_LENGTH)
     {
        copy_len += memscpy((void *)&dcc_nv_item_ptr->ds_mip_gen_user_prof.nai[v_in_req.mdn_len],
                            NV_MAX_NAI_LENGTH - v_in_req.mdn_len,
                            (void *)default_nai, 
                            strlen(default_nai) + 1);
     }
      dcc_nv_item_ptr->ds_mip_gen_user_prof.nai_length = copy_len;
    
      nv_status = dcc_put_nv_item (NV_DS_MIP_GEN_USER_PROF_I, dcc_nv_item_ptr);
      if (nv_status != NV_DONE_S)
      {
        errval = QMI_ERR_INTERNAL;
        goto send_result;
      }
    }
  } /* if (write_all_parameters) */

  /*--------------------------------------------------------------------------
	  Write MN-HA and MN-AAA shared secrets to active MIP profile
  --------------------------------------------------------------------------*/
  if((got_v_in_opt2) || (got_v_in_opt3))
  {
    dcc_nv_item_ptr->ds_mip_ss_user_prof.index = curr_index;
    nv_status = dcc_get_nv_item(NV_DS_MIP_SS_USER_PROF_I, dcc_nv_item_ptr);
    if ((nv_status != NV_DONE_S) && (nv_status != NV_NOTACTIVE_S))
    {
      errval = QMI_ERR_INTERNAL;
      goto send_result;
    }
    else
    {
      LOG_MSG_INFO2_0("writing MIP TLVs");
      ss_profile = &dcc_nv_item_ptr->ds_mip_ss_user_prof;
      if (got_v_in_opt2)
      {
        // mn_ha_len validated before 
	      ss_profile->mn_ha_shared_secret_length = v_in_opt2.mn_ha_len;
	      data_ptr = (byte *) ss_profile->mn_ha_shared_secret;
        memscpy (data_ptr, 
                 v_in_opt2.mn_ha_len, 
                (uint8 *)v_in_opt2.mn_ha_data, 
                 v_in_opt2.mn_ha_len);
	    }

      if (got_v_in_opt3)
      {
	      // mn_aaa_len validated before 
	      ss_profile->mn_aaa_shared_secret_length = v_in_opt3.mn_aaa_len;
	      data_ptr = (byte *) ss_profile->mn_aaa_shared_secret;
        memscpy (data_ptr, 
                 v_in_opt3.mn_aaa_len, 
                 (uint8 *)v_in_opt3.mn_aaa_data, 
                 v_in_opt3.mn_aaa_len);
	    }

      nv_status = dcc_put_nv_item (NV_DS_MIP_SS_USER_PROF_I, dcc_nv_item_ptr);
      if (nv_status != NV_DONE_S)
      {
	      errval = QMI_ERR_INTERNAL;
	      goto send_result;
      }
    }/* else , nv_status != NV_DONE_S for NV_DS_MIP_SS_USER_PROF_I*/

  }/* (got_v_in_opt2) || (got_v_in_opt3) */
#else /* (defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900)) */
  errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
#endif /* (defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900)) */

send_result:
#if (defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900))
  if ((QMI_ERR_NONE != errval) && (got_v_in_opt4))
  {
    /* reset the PRL transmission series */
    qmi_dmsi_reset_prl_series_info();
  }
#endif /* (defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900)) */

  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);

  /*-------------------------------------------------------------------------
    Free the temporary memory allocated for NV item.
  -------------------------------------------------------------------------*/
  PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_item_ptr);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  
  return response;
  
} /* qmi_dmsi_activate_manual() */


/*===========================================================================
  FUNCTION QMI_DMSI_GET_USER_LOCK_STATE()

  DESCRIPTION
    Gives the state of the user lock maintained by the device
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_get_user_lock_state
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;

  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

  PACKED struct PACKED_POST
  {
    uint8  lock_state;
  } v_out_req;

  qmi_dmsi_user_lock_state lock_state;
  nv_item_type            *dcc_nv_item_ptr;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  memset(&v_out_req, 0, sizeof(v_out_req));
  lock_state = DMSI_USER_LOCK_UNLOCKED;

  response = NULL;
  errval = QMI_ERR_NONE;

  /*-------------------------------------------------------------------------
    Allocate temporary memory for the NV item
  -------------------------------------------------------------------------*/
  dcc_nv_item_ptr = (nv_item_type *)
    qmi_svc_ps_system_heap_mem_alloc(sizeof(nv_item_type),
                            FILE_ID_DS_QMI_DMS,__LINE__);
  if( dcc_nv_item_ptr == NULL )
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
    goto send_result;
  }

  /*-------------------------------------------------------------------------
    Qurey the user lock state
  -------------------------------------------------------------------------*/
  LOG_MSG_INFO1_0("dms query user lock state");

  memset(dcc_nv_item_ptr, 0, sizeof(nv_item_type));
  if(NV_DONE_S == dcc_get_nv_item(NV_LOCK_I, dcc_nv_item_ptr))
  {
    /* only in LOCKED state when NV_LOCK_I is active and set to non-zero */
    if (0 != dcc_nv_item_ptr->lock)
    {
      lock_state = DMSI_USER_LOCK_LOCKED;
    }
  }

  if (QMI_ERR_NONE == errval)
  {
    v_out_req.lock_state = (uint8) lock_state;
    if( FALSE == qmi_svc_put_param_tlv(&response, 
                                       QMI_TYPE_REQUIRED_PARAMETERS,
                                       sizeof(v_out_req),
                                       &v_out_req ) )
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  /*-------------------------------------------------------------------------
    Free the temporary memory allocated for NV item.
  -------------------------------------------------------------------------*/
  PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_item_ptr);

send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();

  return response;
  
} /* qmi_dmsi_get_user_lock_state() */


/*===========================================================================
  FUNCTION QMI_DMSI_SET_USER_LOCK_STATE()

  DESCRIPTION
    Sets the state of the user lock maintained by the device
        
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_set_user_lock_state
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

  PACKED struct PACKED_POST
  {
    uint8  lock_state;
    uint8  lock_code[DMSI_USER_LOCK_SIZ];
  } v_in_req;

  boolean  got_v_in_req;
  uint8    type;
  uint16   len;
  void *   value;
  uint16             expected_len;

  nv_stat_enum_type  nv_status;
  nv_item_type      *dcc_nv_item_ptr;
  int                i;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  response = NULL;
  errval = QMI_ERR_NONE;
  got_v_in_req = FALSE;
  
  (void)memset(&v_in_req, 0, sizeof(v_in_req));
  
  /*-------------------------------------------------------------------------
    Allocate temporary memory for the NV item
  -------------------------------------------------------------------------*/
  dcc_nv_item_ptr = (nv_item_type *)
    qmi_svc_ps_system_heap_mem_alloc(sizeof(nv_item_type),
                        FILE_ID_DS_QMI_DMS,__LINE__);
  if( dcc_nv_item_ptr == NULL )
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
    goto send_result;
  }
  
  while (*sdu_in)
  {
    if( !qmi_svc_get_tl( sdu_in, &type, &len ) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        expected_len = sizeof(v_in_req);
        got_v_in_req = TRUE;
        value = (void *) &v_in_req;
        break;

     default:
        LOG_MSG_INFO2_1 ("Unrecognized TLV type (%d)", type);
        break;
    }

    if(!qmi_svc_validate_type_length( type,
                                      len,
                                      expected_len,
                                      FILE_ID_DS_QMI_DMS,__LINE__) )
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    /*-----------------------------------------------------------------------
      If type is unrecognized, value will be NULL, and qmi_svc_dsm_pullup will 
      free the unrecognized value, i.e. we skip past unrecognized TLVs
    -----------------------------------------------------------------------*/
    if(len != qmi_svc_dsm_pullup( sdu_in, value, len ,FILE_ID_DS_QMI_DMS,__LINE__  ))
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  if(!got_v_in_req)
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  /*-------------------------------------------------------------------------
    Populate the local structure with the values coming in the request and
    also validate the values
  -------------------------------------------------------------------------*/
  LOG_MSG_INFO1_0("dms set user lock state");

  if (DMSI_USER_LOCK_MAX > v_in_req.lock_state)
  {
    /* read the current lock code */
    nv_status = dcc_get_nv_item(NV_LOCK_CODE_I, dcc_nv_item_ptr);
    if (NV_DONE_S == nv_status)
    {
      /* if its been set, compare it to the given code */
      for (i = 0; i < DMSI_USER_LOCK_SIZ; i++)
      {
        if (v_in_req.lock_code[i] != dcc_nv_item_ptr->lock_code.digits[i])
        {
          errval = QMI_ERR_AUTHENTICATION_FAILED;
          goto send_result;
        }
      }
    } 
    else if (NV_NOTACTIVE_S == nv_status) 
    {
      /* if it hasn't been set, write it */
      for (i = 0; i < DMSI_USER_LOCK_SIZ; i++)
      {
        dcc_nv_item_ptr->lock_code.digits[i] = v_in_req.lock_code[i];
      }
      if (NV_DONE_S != dcc_put_nv_item(NV_LOCK_CODE_I, dcc_nv_item_ptr))
      {
        errval = QMI_ERR_INVALID_ARG;
        goto send_result;
      }
    } 
    else 
    {
        errval = QMI_ERR_INTERNAL;
        goto send_result;
    }

    /* change the lock state to the specified value */
    memset(dcc_nv_item_ptr, 0, sizeof(nv_item_type));
    dcc_nv_item_ptr->lock = v_in_req.lock_state;
    if (NV_DONE_S != dcc_put_nv_item(NV_LOCK_I, dcc_nv_item_ptr))
    {
      errval = QMI_ERR_INVALID_ARG;
      goto send_result;
    }
  }
  else
  {
    errval = QMI_ERR_INVALID_ARG;
  }

send_result: 
  /*-------------------------------------------------------------------------
    Free the temporary memory allocated for NV item.
  -------------------------------------------------------------------------*/
  PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_item_ptr);
  
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  
  return response;
  
} /* qmi_dmsi_set_user_lock_state() */


/*===========================================================================
  FUNCTION QMI_DMSI_SET_USER_LOCK_CODE()

  DESCRIPTION
    Changes the user lock code maintained by the device
        
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_set_user_lock_code
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

  PACKED struct PACKED_POST
  {
    uint8 code_cur[DMSI_USER_LOCK_SIZ];
    uint8 code_new[DMSI_USER_LOCK_SIZ];
  } v_in_req;

  boolean  got_v_in_req;
  uint8    type;
  uint16   len;
  void *   value;
  uint16             expected_len;

  nv_stat_enum_type  nv_status;
  nv_item_type      *dcc_nv_item_ptr;
  int                i;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  response = NULL;
  errval = QMI_ERR_NONE;
  got_v_in_req = FALSE;
  (void)memset(&v_in_req, 0, sizeof(v_in_req));
  
  /*-------------------------------------------------------------------------
    Allocate temporary memory for the NV item
  -------------------------------------------------------------------------*/
  dcc_nv_item_ptr = (nv_item_type *)
    qmi_svc_ps_system_heap_mem_alloc(sizeof(nv_item_type),
                             FILE_ID_DS_QMI_DMS,__LINE__);
  if( dcc_nv_item_ptr == NULL )
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
    goto send_result;
  }
  
  while (*sdu_in)
  {
    if( !qmi_svc_get_tl( sdu_in, &type, &len ) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        expected_len = sizeof(v_in_req);
        got_v_in_req = TRUE;
        value = (void *) &v_in_req;
        break;

     default:
        LOG_MSG_INFO2_1 ("Unrecognized TLV type (%d)", type);
        break;
    }

    if(!qmi_svc_validate_type_length( type,
                                      len,
                                      expected_len,
                                      FILE_ID_DS_QMI_DMS,__LINE__) )
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    /*-----------------------------------------------------------------------
      If type is unrecognized, value will be NULL, and qmi_svc_dsm_pullup will 
      free the unrecognized value, i.e. we skip past unrecognized TLVs
    -----------------------------------------------------------------------*/
    if(len != qmi_svc_dsm_pullup( sdu_in, value, len ,FILE_ID_DS_QMI_DMS,__LINE__  ))
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  if(!got_v_in_req)
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  /*-------------------------------------------------------------------------
    Populate the local structure with the values coming in the request and
    also validate the values
  -------------------------------------------------------------------------*/
  LOG_MSG_INFO1_0("dms set user lock code");

  /* read the current lock code */
  memset(dcc_nv_item_ptr, 0, sizeof(nv_item_type));
  nv_status = dcc_get_nv_item(NV_LOCK_CODE_I, dcc_nv_item_ptr);
  if(NV_DONE_S == nv_status)
  {
    /* if its been set, compare it to the given code */
    for (i = 0; i < NV_LOCK_CODE_SIZE; i++)
    {
      if (v_in_req.code_cur[i] != dcc_nv_item_ptr->lock_code.digits[i])
      {
        errval = QMI_ERR_AUTHENTICATION_FAILED;
        goto send_result;
      }
    }
  }
  
  /* write the new lock code if it was previously set and the specified code 
     has been verified as a match or has not been set yet */
  memset(dcc_nv_item_ptr, 0, sizeof(nv_item_type));
  for (i = 0; i < NV_LOCK_CODE_SIZE; i++)
  {
    dcc_nv_item_ptr->lock_code.digits[i] = v_in_req.code_new[i];
  }
  if (NV_DONE_S != dcc_put_nv_item(NV_LOCK_CODE_I, dcc_nv_item_ptr))
  {
    errval = QMI_ERR_INVALID_ARG;
    goto send_result;
  }

send_result: 
  /*-------------------------------------------------------------------------
    Free the temporary memory allocated for NV item.
  -------------------------------------------------------------------------*/
  PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_item_ptr);

  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
            : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  
  return response;
  
} /* qmi_dmsi_set_user_lock_code() */

/*===========================================================================
  FUNCTION QMI_DMSI_READ_USER_DATA()

  DESCRIPTION
    Reads the user data maintained by the device
        
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_read_user_data
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

  PACKED struct PACKED_POST
  {
    uint16 data_len;
    uint8  data[DMSI_USER_DATA_FILE_MAX_LEN];
  } v_out_req;

  qmi_nv_status_e_type nv_status;
  fs_size_t            bytes_read;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  response = NULL;
  errval = QMI_ERR_NONE;

  memset(&v_out_req, 0, sizeof(v_out_req));
  bytes_read = 0;

  /*-------------------------------------------------------------------------
    Perform query action
  -------------------------------------------------------------------------*/
  LOG_MSG_INFO1_0("dms read user data file");

  nv_status = qmi_nv_file_read(QMI_NV_FILE_USER_DATA,
                               (void *) v_out_req.data,
                               (fs_size_t)DMSI_USER_DATA_FILE_MAX_LEN,
                               &bytes_read);
  
  if (QMI_NV_STATUS_OK == nv_status)
  {
    v_out_req.data_len = (uint16) bytes_read;
  } 
  else if (QMI_NV_STATUS_NOT_ACTIVE == nv_status) 
  {
    v_out_req.data_len = 0;
  } 
  else 
  {
    errval = QMI_ERR_INTERNAL;
  }
  LOG_MSG_INFO2_3("Read User Data file with bytes %d,status %d and errval %d",bytes_read,nv_status,errval);

  if (QMI_ERR_NONE == errval)
  {
    if( FALSE == qmi_svc_put_param_tlv(&response, 
                                       QMI_TYPE_REQUIRED_PARAMETERS,
                                       (v_out_req.data_len + 
                                               sizeof(v_out_req.data_len)),
                                       &v_out_req))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }
  
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  
  return response;
  
} /* qmi_dmsi_read_user_data() */


/*===========================================================================
  FUNCTION QMI_DMSI_WRITE_USER_DATA()

  DESCRIPTION
    Writes the user data maintained by the device
        
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_write_user_data
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

  PACKED struct PACKED_POST
  {
    uint16 data_len;
    uint8  data[DMSI_USER_DATA_FILE_MAX_LEN];
  } v_in_req;

  uint16 actual_data_len = 0;

  boolean  got_v_in_req;
  uint8    type;
  uint16   len;
  void *   value;
  uint16   expected_len;

  qmi_nv_status_e_type nv_status;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  response = NULL;
  errval = QMI_ERR_NONE;

  got_v_in_req = FALSE;
  memset(&v_in_req, 0, sizeof(v_in_req));  
  while (*sdu_in)
  {
    if( !qmi_svc_get_tl( sdu_in, &type, &len ) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        if (sizeof(v_in_req) < len)
        {
          errval = QMI_ERR_ARG_TOO_LONG;
          goto send_result;
        }
        else if (sizeof(v_in_req.data_len) > len)
        {
          /* must at least give us the full data_len value */
          errval = QMI_ERR_MALFORMED_MSG;
          goto send_result;
        }
        else
        {
          got_v_in_req = TRUE;
          value = (void *)&v_in_req;
          actual_data_len = len;
        }
        break;

     default:
        LOG_MSG_INFO2_1("Unrecognized TLV type (%d)", type);
        break;
    }

    if(!qmi_svc_validate_type_length( type,
                                      len,
                                      expected_len,
                                      FILE_ID_DS_QMI_DMS,__LINE__) )
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    /*-----------------------------------------------------------------------
      If type is unrecognized, value will be NULL, and qmi_svc_dsm_pullup will 
      free the unrecognized value, i.e. we skip past unrecognized TLVs
    -----------------------------------------------------------------------*/
    if(len != qmi_svc_dsm_pullup( sdu_in, value, len ,FILE_ID_DS_QMI_DMS,__LINE__  ))
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  if(!got_v_in_req)
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  /*-------------------------------------------------------------------------
    Populate the local structure with the values coming in the request and
    also validate the values
  -------------------------------------------------------------------------*/

  if (v_in_req.data_len > sizeof(v_in_req.data))
  {
    LOG_MSG_ERROR_2("User data len (%d) greater than max (%d)",
                    v_in_req.data_len, sizeof(v_in_req.data));
    errval = QMI_ERR_INVALID_ARG;
    goto send_result;
  }
  else if (v_in_req.data_len != (actual_data_len - sizeof(v_in_req.data_len)))
  {
    LOG_MSG_ERROR_2("User data len doesn't match amount: len %d, provided %d",
                    v_in_req.data_len,
                    (actual_data_len - sizeof(v_in_req.data_len)));
    errval = QMI_ERR_INVALID_ARG;
    goto send_result;
  }

  nv_status = qmi_nv_file_write(QMI_NV_FILE_USER_DATA,
                                (void *) v_in_req.data,
                                (fs_size_t)v_in_req.data_len);
  if (QMI_NV_STATUS_OK != nv_status)
  {
    LOG_MSG_ERROR_1("cannot write user data file (status=%d)", nv_status);
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }
  
send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  
  return response;
  
} /* qmi_dmsi_write_user_data() */


/*===========================================================================
  FUNCTION QMI_DMSI_READ_ERI_FILE()

  DESCRIPTION
    Reads the contents ERI file maintained by the device
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_read_eri_file
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

  PACKED struct PACKED_POST
  {
    uint16 data_len;
    uint8  data[DMSI_ERI_FILE_MAX_LEN];
  } v_out_req;

  qmi_nv_status_e_type nv_status;
  fs_size_t            bytes_read;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  response = NULL;
  errval = QMI_ERR_NONE;

  memset(&v_out_req, 0, sizeof(v_out_req));
  bytes_read = 0;

  /*-------------------------------------------------------------------------
    Perform query action
  -------------------------------------------------------------------------*/

  nv_status = qmi_nv_file_read(QMI_NV_FILE_ERI,
                               (void *) v_out_req.data,
                               (fs_size_t)DMSI_ERI_FILE_MAX_LEN,
                               &bytes_read);
  
  if (QMI_NV_STATUS_OK == nv_status)
  {
    v_out_req.data_len = (uint16) bytes_read;
  } 
  else if (QMI_NV_STATUS_NOT_ACTIVE == nv_status) 
  {
    v_out_req.data_len = 0;
  } 
  else 
  {
    errval = QMI_ERR_INTERNAL;
  }
  LOG_MSG_INFO2_3("Read ERI file with bytes %d,status %d and errval %d",bytes_read,nv_status,errval);

  if (QMI_ERR_NONE == errval)
  {
    if( FALSE == qmi_svc_put_param_tlv(&response, 
                                       QMI_TYPE_REQUIRED_PARAMETERS,
                                       (v_out_req.data_len + 
                                               sizeof(v_out_req.data_len)),
                                       &v_out_req))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }
  
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  
  return response;
    
} /* qmi_dmsi_read_eri_file() */

/*===========================================================================
  FUNCTION QMI_DMSI_RESTORE_FACTORY_DEFAULTS()

  DESCRIPTION
    Performs the restore factory defaults procedure
        
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_restore_factory_defaults
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response = NULL;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;
  
  PACKED struct PACKED_POST
  {
    uint8  spc[DMSI_SPC_SIZ];
  } v_in_req;

  boolean  got_v_in_req;
  uint8    type;
  uint16   len;
  void *   value;
  uint16             expected_len;

  qmi_if_spc_result_e_type	check_spc_result;
#if defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900)
  uint8 i;
#endif /* defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900)*/
  nv_item_type      *dcc_nv_item_ptr;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  /*-------------------------------------------------------------------------
    Allocate temporary memory for the NV item
  -------------------------------------------------------------------------*/
  dcc_nv_item_ptr = (nv_item_type *)
    qmi_svc_ps_system_heap_mem_alloc(sizeof(nv_item_type),
                             FILE_ID_DS_QMI_DMS,__LINE__);
  if( dcc_nv_item_ptr == NULL )
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
    goto send_result;
  }

  response = NULL;
  errval = QMI_ERR_NONE;
  
  got_v_in_req = FALSE;
  memset(&v_in_req, 0, sizeof(v_in_req));
  
  while (*sdu_in)
  {
    if( !qmi_svc_get_tl( sdu_in, &type, &len ) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        if (len <= sizeof(v_in_req))
        {
          got_v_in_req = TRUE;
          value = (void *) &v_in_req;
        }
        else
        {
          errval = QMI_ERR_ARG_TOO_LONG;
          goto send_result;
        }
        break;

     default:
        LOG_MSG_INFO2_1 ("Unrecognized TLV type (%d)", type);
        break;
    }

    if(!qmi_svc_validate_type_length( type,
                                      len,
                                      expected_len,
                                      FILE_ID_DS_QMI_DMS,__LINE__))
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    if(len != qmi_svc_dsm_pullup( sdu_in, value, len ,FILE_ID_DS_QMI_DMS,__LINE__  ))
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  if(!got_v_in_req)
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  /*-------------------------------------------------------------------------
    Populate the local structure with the values coming in the request and
    also validate the values
  -------------------------------------------------------------------------*/
  LOG_MSG_INFO1_0("dms restore factory defaults");

  /* Check SPC first , no need to validate values and give any special err for
     SPC, make it obscure */
  if ((FALSE == qmi_dmsi_global.nv_allow_invalid_spc) ||
      (0 != strncmp((char *)v_in_req.spc, 
                        SPECIAL_INVALID_SPC, 
                        strlen(SPECIAL_INVALID_SPC))))
  {
    check_spc_result = qmi_if_check_spc((uint8 *)v_in_req.spc, FALSE); 
  
    if(check_spc_result == QMI_SPC_FAIL)
    {
    	errval = QMI_ERR_AUTHENTICATION_FAILED;
    	goto send_result;
    }
  
    if(check_spc_result == QMI_SPC_LOCKED)
    {
    	errval = QMI_ERR_AUTHENTICATION_LOCK;
    	goto send_result;
    }

    if(check_spc_result == QMI_SPC_ERR_INTERNAL)
    {
      errval = QMI_ERR_INTERNAL;
      goto send_result;
    }
  }

  /*-------------------------------------------------------------------------
    Initiate the factory defaults procedure

    For C2K and UMTS:
    
    For C2K, only:
    Set the IMSI Components to 0
    Set the IMSI_T Components to 0
    Set MDN = "0000000000"
    
    For UMTS, only:
    
    Reboot! - this is expected to be intiated by the PC Connection Manager !
   -------------------------------------------------------------------------*/

  /*-------------------------------------------------------------------------
    Reset items for both C2K and UMTS builds.
   -------------------------------------------------------------------------*/

#if defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900)
  /*-------------------------------------------------------------------------
    Reset items for C2K builds.
   -------------------------------------------------------------------------*/
  
#if defined(FEATURE_RUIM) && defined(FEATURE_UIM_RUN_TIME_ENABLE)
  /*-------------------------------------------------------------------------
    Changing provisioning cannot be done on a RUIM card. Do not change any
    values, only allow the UQCN processing.
  -------------------------------------------------------------------------*/
  if (NV_RTRE_CONTROL_USE_RUIM == nv_rtre_control())
  {
    LOG_MSG_INFO1_0("Cannot change ADM-protected values on RUIM");
  }
  else
#endif /* defined(FEATURE_RUIM) && defined(FEATURE_UIM_RUN_TIME_ENABLE) */
  {
    nv_item_type *dcc_nv_mdn_ptr = NULL;

    /*-------------------------------------------------------------------------
      Allocate temporary memory for the NV item
    -------------------------------------------------------------------------*/
    dcc_nv_mdn_ptr = (nv_item_type *)
      qmi_svc_ps_system_heap_mem_alloc(sizeof(nv_item_type),
                                  FILE_ID_DS_QMI_DMS,__LINE__);
    if( dcc_nv_mdn_ptr == NULL )
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
      goto send_result;
    }

    /* setup the MDN NV item (fills with 0 char) once here before loop */
    memset(dcc_nv_mdn_ptr, 0, sizeof(nv_item_type));
    memscpy(&dcc_nv_item_ptr->dir_number.dir_number[0],
            sizeof(dcc_nv_item_ptr->dir_number.dir_number),
            "0000000000", 
            sizeof(dcc_nv_item_ptr->dir_number.dir_number));

    /* clear all NAM's of info */
    for( i=0; i < NV_MAX_NAMS; i++ ) 
    {
      /* Set the IMSI Components to 0 */
      if ( qmi_dmsi_reset_imsi_components(i) == FALSE )
      {
        errval = QMI_ERR_INTERNAL;
        LOG_MSG_ERROR_1("Could not reset IMSI components on NAM %d", i);
        PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_mdn_ptr);
        goto send_result;
      }
  
#ifdef FEATURE_IS95B_TRUE_IMSI
      /* Set the IMSI_T Components to 0 */
      if ( qmi_dmsi_reset_imsi_t_components(i) == FALSE )
      {
        errval = QMI_ERR_INTERNAL;
        LOG_MSG_ERROR_1("Could not reset IMSI_T components on NAM %d", i);
        PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_mdn_ptr);
        goto send_result;
      }
#endif /* FEATURE_IS95B_TRUE_IMSI */

      /* Set MDN = "0000000000" */
      dcc_nv_mdn_ptr->dir_number.nam = i;
      if ( NV_DONE_S != dcc_put_nv_item( NV_DIR_NUMBER_I, dcc_nv_mdn_ptr ) )
      {
        errval = QMI_ERR_INTERNAL;
        PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_mdn_ptr);
        goto send_result;
      }

      /* Set SSD-A to 0 */
      memset(dcc_nv_item_ptr, 0, sizeof(nv_item_type));
      dcc_nv_item_ptr->ssd_a.nam = i;
      if ( NV_DONE_S != dcc_put_nv_item( NV_SSD_A_I, dcc_nv_item_ptr ) )
      {
        errval = QMI_ERR_INTERNAL;
        PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_mdn_ptr);
        goto send_result;
      }

      /* Set SSD-B to 0 */
      memset(dcc_nv_item_ptr, 0, sizeof(nv_item_type));
      dcc_nv_item_ptr->ssd_b.nam = i;
      if ( NV_DONE_S != dcc_put_nv_item( NV_SSD_B_I, dcc_nv_item_ptr ) )
      {
        errval = QMI_ERR_INTERNAL;
        PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_mdn_ptr);
        goto send_result;
      }
    }

    /*-------------------------------------------------------------------------
      Free the temporary memory allocated for NV item.
    -------------------------------------------------------------------------*/
    PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_mdn_ptr);
  }

#endif /* defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900) */

#if defined(FEATURE_WCDMA) || defined(FEATURE_GSM) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA)
  /*-------------------------------------------------------------------------
    Reset items for UMTS builds.
   -------------------------------------------------------------------------*/

#endif /* defined(FEATURE_WCDMA) || defined(FEATURE_GSM) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA) */

#ifdef FEATURE_DATA_QMI_ADDENDUM
  /*-------------------------------------------------------------------------
    Gobi-specific factory defaults procedure

    For C2K and UMTS:
    
    For C2K, only:
    Set UQCN version = 0xFACD0000 to restore the factory default UQCN
    
    For UMTS, only:
    Set UQCN version = 0xFACD0000 to restore the factory default UQCN
  ----------------------------------------------------------------------*/*/

#if defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900)
  /* Set C2K UQCN version = 0xFACD0000 to restore the factory default UQCN */
  dcc_nv_item_ptr->cust_uqcn_c2k_version = DMSI_RESTORE_FACT_VALUE; 
  if( NV_DONE_S != dcc_put_nv_item( NV_CUST_UQCN_C2K_VERSION_I, dcc_nv_item_ptr ) )
  {
  	errval = QMI_ERR_INTERNAL;
  	goto send_result;
  }
#endif /* defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900) */

#if defined(FEATURE_WCDMA) || defined(FEATURE_GSM) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA)
  /* Set UMTS UQCN version = 0xFACD0000 to restore the factory default UQCN */
  dcc_nv_item_ptr->cust_uqcn_umts_version = DMSI_RESTORE_FACT_VALUE; 
  if( NV_DONE_S != dcc_put_nv_item( NV_CUST_UQCN_UMTS_VERSION_I, dcc_nv_item_ptr ) )
  {
  	errval = QMI_ERR_INTERNAL;
  	goto send_result;
  }
#endif /* defined(FEATURE_WCDMA) || defined(FEATURE_GSM) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA) */
#endif /* FEATURE_DATA_QMI_ADDENDUM */
  
send_result: 
  /*-------------------------------------------------------------------------
    Free the temporary memory allocated for NV item.
  -------------------------------------------------------------------------*/
  PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_item_ptr);

  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  
  return response;
  
} /* qmi_dmsi_restore_factory_defaults() */

/*===========================================================================
  FUNCTION QMI_DMSI_VALIDATE_SPC()

  DESCRIPTION
    Validates a provided security code.
        
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_validate_spc
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;
  
  PACKED struct PACKED_POST
  {
    uint8  spc[DMSI_SPC_SIZ];
  } v_in_req;

  boolean  got_v_in_req;
  uint8    type;
  uint16   len;
  void *   value;
  uint16             expected_len;

  qmi_if_spc_result_e_type	check_spc_result;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  response = NULL;
  errval = QMI_ERR_NONE;
  
  got_v_in_req = FALSE;
  memset(&v_in_req, 0, sizeof(v_in_req));
  
  while (*sdu_in)
  {
    if( !qmi_svc_get_tl( sdu_in, &type, &len ) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        if (len <= sizeof(v_in_req))
        {
          got_v_in_req = TRUE;
          value = (void *) &v_in_req;
        }
        else
        {
          errval = QMI_ERR_ARG_TOO_LONG;
          goto send_result;
        }
        break;

     default:
        LOG_MSG_INFO2_1 ("Unrecognized TLV type (%d)", type);
        break;
    }

    if(!qmi_svc_validate_type_length( type,
                                      len,
                                      expected_len,
                                      FILE_ID_DS_QMI_DMS,__LINE__) )
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    if(len != qmi_svc_dsm_pullup( sdu_in, value, len ,FILE_ID_DS_QMI_DMS,__LINE__  ))
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  if(!got_v_in_req)
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  /* Check the SPC */
  check_spc_result = qmi_if_check_spc((uint8 *)v_in_req.spc, TRUE); 

  if(QMI_SPC_FAIL == check_spc_result)
  {
    errval = QMI_ERR_AUTHENTICATION_FAILED;
  }
  else if (QMI_SPC_LOCKED == check_spc_result)
  {
    errval = QMI_ERR_AUTHENTICATION_LOCK;
  }
  else if(QMI_SPC_ERR_INTERNAL == check_spc_result)
  {
    errval = QMI_ERR_INTERNAL;
  }

send_result: 
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  
  return response;
  
} /* qmi_dmsi_validate_spc() */

/*===========================================================================
  FUNCTION QMI_DMSI_GET_TIME()

  DESCRIPTION
    Gives the current time from the device
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_get_time
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

  PACKED struct PACKED_POST
  {
    uint64 time_count;
  } v_out_req;

  time_genoff_args_struct_type genoff_args;
  time_type                    ts_val;
  time_type                    ts_val_usr;
  time_type                    ts_val_sys;
  time_source_enum_type        ts_source;
  uint16 *                     ts_source_ptr;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  memset(&v_out_req, 0, sizeof(v_out_req));
  memset(&genoff_args, 0, sizeof (genoff_args));
  memset(ts_val, 0, sizeof(ts_val));
  memset(ts_val_usr, 0, sizeof(ts_val_usr));
  memset(ts_val_sys, 0, sizeof(ts_val_sys));
  
  response = NULL;
  errval = QMI_ERR_NONE;

  /*-------------------------------------------------------------------------
    Query the current user time
  -------------------------------------------------------------------------*/

  genoff_args.base        = ATS_USER; 
  genoff_args.base_source = TIME_SCLK;
  genoff_args.ts_val      = ts_val_usr;
  genoff_args.unit        = TIME_MSEC;
  genoff_args.operation   = T_GET;
  time_genoff_opr( &genoff_args );

  LOG_MSG_INFO2_2("Dms query User Time: ts_val_usr[0]=%d : ts_val_usr[1]=%d",
                  ts_val_usr[0], ts_val_usr[1]);

  if( FALSE == qmi_svc_put_param_tlv(&response, 
                                     DMSI_PRM_TYPE_USR_TIME,
                                     sizeof(ts_val_usr),
                                     ts_val_usr ) )
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
    goto send_result;
  }

  memset(&genoff_args, 0, sizeof (genoff_args));

  /*-------------------------------------------------------------------------
    Query the current sys time
  -------------------------------------------------------------------------*/
  LOG_MSG_INFO2_0("dms query sys time in ms");

  genoff_args.base        = ATS_TOD; 
  genoff_args.base_source = TIME_SCLK;
  genoff_args.ts_val      = ts_val_sys;
  genoff_args.unit        = TIME_MSEC;
  genoff_args.operation   = T_GET;
  time_genoff_opr( &genoff_args );

  LOG_MSG_INFO2_2("Dms query Sys Time: ts_val_sys[0]=%d : ts_val_sys[1]=%d",
                  ts_val_sys[0], ts_val_sys[1]);

  if( FALSE == qmi_svc_put_param_tlv(&response, 
                                     DMSI_PRM_TYPE_SYS_TIME,
                                     sizeof(ts_val_sys),
                                     ts_val_sys ) )
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
    goto send_result;
  }

  /*-------------------------------------------------------------------------
    Query the current timestamp
  -------------------------------------------------------------------------*/
  /*
                             TIMESTAMP FORMAT

       |<------------ 48 bits --------------->|<----- 16 bits ------->|
       +--------------------------------------+-----------------------+
       |      1.25 ms counter                 |   1/32 chip counter   |
       +--------------------------------------+-----------------------+
             (11K years of dynamic range)          (1.25 ms dynamic
                                                    range. Rolls over
                                                    at count 49152)
  */
  ts_source = time_get(ts_val);
  
  /*-------------------------------------------------------------------------
    Populate the timestamp response
  -------------------------------------------------------------------------*/
  /* copy the timestamp to native C */
  v_out_req.time_count = QW_CVT_Q2N(ts_val);

  /* shift the timestamp to conform with the output alignment */
  v_out_req.time_count = v_out_req.time_count >> 16;
  
  /* Copy the timestamp source */
  /* locate the most sig 2 bytes, which are actually the LSB in small endian 
  0 + ( 16 * 3 ) = 48th bit position                                       */
  ts_source_ptr = ((uint16 *) &v_out_req.time_count) + 3;
  
  /* clear the 1/32 chip counter */
  *ts_source_ptr &= 0x0000;

  /* copy the time source to the last 2 bytes */
  *ts_source_ptr = ts_source;

  if( FALSE == qmi_svc_put_param_tlv(&response, 
                                     QMI_TYPE_REQUIRED_PARAMETERS,
                                     sizeof(v_out_req),
                                     &v_out_req ) )
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
  }

send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  
  return response;
  
} /* qmi_dmsi_get_time() */

/*===========================================================================
  FUNCTION QMI_DMSI_SET_TIME()

  DESCRIPTION
    Sets the current time to the device

  PARAMETERS
    sp           : service state pointer
    cl_sp        : client state pointer
    cmd_buf_p    : the current command buffer
    sdu_in       : dsm_item containing incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type*  qmi_dmsi_set_time
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;
  uint8              type;
  uint16             len;
  void *             value;
  uint16             expected_len;

  PACKED struct PACKED_POST
  {
    time_type ts_val;
  } v_in_req;

  PACKED struct PACKED_POST
  {
    uint32 time_ref_type;
  } v_in1;

  time_genoff_args_struct_type genoff_args;
  boolean  got_v_in_req;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  response = NULL;
  errval = QMI_ERR_NONE;
  got_v_in_req = FALSE;
  memset(&v_in_req,0,sizeof(v_in_req));
  memset(&v_in1,0,sizeof(v_in1));
  memset(&genoff_args,0,sizeof(genoff_args));
  
  while (*sdu_in)
  {
    if( !qmi_svc_get_tl( sdu_in, &type, &len ) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        expected_len = sizeof(v_in_req);
        got_v_in_req = TRUE;
        value = (void *) &v_in_req;
        break;

      case DMSI_PRM_TYPE_TIME_REF_TYPE:
        expected_len = sizeof(v_in1);
        value = (void *) &v_in1;
        break;

      default:
        LOG_MSG_INFO2_1 ("Unrecognized TLV type (%d)", type);
        break;
    }

    if(!qmi_svc_validate_type_length( type,
                                      len,
                                      expected_len,
                                      FILE_ID_DS_QMI_DMS,__LINE__) )
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    /*-----------------------------------------------------------------------
      If type is unrecognized, value will be NULL, and qmi_svc_dsm_pullup will 
      free the unrecognized value, i.e. we skip past unrecognized TLVs
    -----------------------------------------------------------------------*/
    if(len != qmi_svc_dsm_pullup( sdu_in, value, len ,FILE_ID_DS_QMI_DMS,__LINE__  ))
      {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  if(!got_v_in_req)
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  /*-------------------------------------------------------------------------
    We support only User Time Reference Type (0x00000000)
  -------------------------------------------------------------------------*/
  if(v_in1.time_ref_type)
  {
    LOG_MSG_INFO1_1 ("Invalid Time reference type: %d", v_in1.time_ref_type);
    errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
    goto send_result;
  }

  genoff_args.base        = ATS_USER; 
  genoff_args.base_source = TIME_SCLK;
  genoff_args.ts_val      = v_in_req.ts_val;
  genoff_args.unit        = TIME_MSEC;
  genoff_args.operation   = T_SET;
  time_genoff_opr( &genoff_args );

send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  
  return response;

} /* qmi_dmsi_set_time() */

/*===========================================================================
  FUNCTION QMI_DMS_CLIENT_Q_CLEANUP()

  DESCRIPTION
    Deletes all client related elements in given Queue
    
  PARAMETERS
    cl_sp :  client state pointer
    q_ptr : Address of the Q head pointer
    
  RETURN VALUE
    uint16 : number of deleted elements from queue
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static uint16 qmi_dms_client_q_cleanup
(
  qmi_dmsi_client_state_type *cl_sp,
  q_type * q_ptr
)
{
 uint16 prev_cnt = 0;
 uint16 curr_cnt = q_ptr->cnt;
 uint16 del_cnt  = 0;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
/*q_linear_delete_new() deletes only one element matches.Delete all entries related to client*/
 do
 {
  del_cnt++;
  prev_cnt = curr_cnt;
  
 (void) q_linear_delete_new( q_ptr,
                            (q_compare_func_type) qmi_svc_cmd_buf_belongs_to_client,
                             cl_sp,
                             (q_action_func_type) NULL,
                             NULL);
  curr_cnt = q_ptr->cnt;
 }while( prev_cnt > curr_cnt);

 /*as loop is do while, elements deleted will be one less*/
 return (del_cnt-1);
}


/*===========================================================================
  FUNCTION QMI_DMSI_RESET_CLIENT()

  DESCRIPTION
    Resets the state for the given client
    
  PARAMETERS
    cl_sp :  client state pointer
    
  RETURN VALUE
    None
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_dmsi_reset_client
(
  void * cl_sp_in
)
{
  qmi_dmsi_client_state_type * cl_sp;
  int i;
  qmi_transaction_type *  x_p;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  cl_sp = (qmi_dmsi_client_state_type *)cl_sp_in;
  /*-------------------------------------------------------------------------
    Free resources for client
    Remove any pending commmands for this client from the pending q's
    Clear pending msg transactions and partially bundled responses
  -------------------------------------------------------------------------*/
  /* (void) q_linear_delete_new(
           &(qmi_dms_state.set_oprt_mode_pending_q),
           (q_compare_func_type) qmi_svc_cmd_buf_belongs_to_client,
           cl_sp,
           (q_action_func_type) NULL,
           NULL); */
           
  if((qmi_dms_client_q_cleanup(cl_sp,
  	                  &qmi_dmsi_global.sim.get_ck_state_q)) > 0)
  {
    /*reset global variables only if this client used it*/
    memset( &qmi_dmsi_global.sim.perso_scratch, 0,
		    sizeof(qmi_dmsi_global.sim.perso_scratch));
  }

  #ifdef FEATURE_TDSCDMA

  (void) q_linear_delete_new(&qmi_dms_state.get_tdscdma_config_pending_q,
                             (q_compare_func_type) qmi_svc_cmd_buf_belongs_to_client,
                             cl_sp,
                             (q_action_func_type) NULL,
                             NULL);

  (void) q_linear_delete_new(&qmi_dms_state.set_tdscdma_config_pending_q,
                             (q_compare_func_type) qmi_svc_cmd_buf_belongs_to_client,
                             cl_sp,
                             (q_action_func_type) NULL,
                             NULL);

  #endif

  /*-------------------------------------------------------------------------
    q_init() is used here as a way to clear the queue and not as a way to 
    initialize the queue.
  -------------------------------------------------------------------------*/ 
  (void) q_init( &cl_sp->common.x_free_q );
  for( i = 0; i < MAX_PENDING_TRANSACTIONS; i++ )
  {
    x_p = &cl_sp->common.transactions[i];
    ds_qmi_fw_free_transaction( &x_p );
  }


  /*-------------------------------------------------------------------------
    If power state reporting is set for this client then
    - reset it
    - decrement the report power state count by 1
    - If this count becomes 0, turn off the timer for power state reporting
  -------------------------------------------------------------------------*/
  if(cl_sp->report_status.report_power_state == TRUE)
  {
    cl_sp->report_status.report_power_state  = FALSE;
    if (qmi_dmsi_global.pwr_rpt.report_power_state_client_count > 0)
    {
      qmi_dmsi_global.pwr_rpt.report_power_state_client_count--;
    }

    if(qmi_dmsi_global.pwr_rpt.report_power_state_client_count == 0)
    {
      qmi_charger_set_power_state_reporting(FALSE);
    }
  }
  /*-------------------------------------------------------------------------
    clear event reporting then set non-zero default values
  -------------------------------------------------------------------------*/
  memset(&(cl_sp->report_status), 0, sizeof(cl_sp->report_status));
  cl_sp->report_status.bat_lvl_lower_limit = DMSI_BAT_LVL_MIN;
  cl_sp->report_status.bat_lvl_upper_limit = DMSI_BAT_LVL_MAX;
  cl_sp->report_status.last_bat_lvl_reported = DMSI_BAT_LVL_INACTIVE;
  cl_sp->report_status.last_power_state_reported = DMSI_PWR_STATE_INACTIVE;

  if (cl_sp_in == qmi_dmsi_global.prl_info.cl_sp)
  {
    /* end the PRL series because the client that was using it is ending */
    qmi_dmsi_reset_prl_series_info();
  }  
} /* qmi_dmsi_reset_client() */



/*===========================================================================
  FUNCTION QMI_DMSI_GET_MAX_CHANNEL_RATE()

  DESCRIPTION
    Get the maximum theoretically supported RX and TX channel rate

  PARAMETERS
    max_rx_channel_rate (output parameter)
    max_tx_channel_rate (output parameter)

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static qmi_error_e_type qmi_dmsi_get_max_channel_rate
(
  uint32 * max_rx_channel_rate,
  uint32 * max_tx_channel_rate
)
{
  qmi_channel_info_s_type supp_channel_info;
  uint16 mode_capability = 0;
  qmi_if_info_type  *qmi_if_info_ptr;
#if defined(FEATURE_WCDMA) || defined(FEATURE_GSM) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA)
  uint32  tx_rate = 0;
  uint32  rx_rate = 0;
#endif /* FEATURE_WCDMA || FEATURE_GSM || FEATURE_LTE || FEATURE_TDSCDMA */
#if defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900)
  uint32  tx_rate_3gpp2 = 0;
  uint32  rx_rate_3gpp2 = 0;
#endif /* FEATURE_CDMA_800 || FEATURE_CDMA_1900 */
  qmi_error_e_type   errval;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT (max_rx_channel_rate && max_tx_channel_rate);

  *max_tx_channel_rate = 0;
  *max_rx_channel_rate = 0;

  errval = QMI_ERR_NONE;
  memset(&supp_channel_info, 0, sizeof(supp_channel_info));

  qmi_if_info_ptr = qmi_if_info_get_ptr();

  mode_capability = (uint16)qmi_if_info_ptr->ph_info.mode_capability;


  /*-------------------------------------------------------------------------
    Determine max supported channel rate based on software feature support
  -------------------------------------------------------------------------*/
#if defined(FEATURE_WCDMA) || defined(FEATURE_GSM) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA)
  if (qmi_dmsi_global.nv_use_max_defined_data_rates)
  {
    if(DMSI_TARGET_SUPPORTS_LTE(mode_capability))
    {
      supp_channel_info.supp_channel = QMI_CHANNEL_SUPP_LTE;
    }
    else if(DMSI_TARGET_SUPPORTS_WCDMA(mode_capability))
    {  
      supp_channel_info.supp_channel = QMI_CHANNEL_SUPP_WCDMA;
    }
    else if(DMSI_TARGET_SUPPORTS_TDSCDMA(mode_capability))
    {  
      supp_channel_info.supp_channel = QMI_CHANNEL_SUPP_TDSCDMA;
    }
    else if(DMSI_TARGET_SUPPORTS_GSM(mode_capability))
    {
      #if defined(FEATURE_GSM_EGPRS)
        supp_channel_info.supp_channel = QMI_CHANNEL_SUPP_EDGE;
        supp_channel_info.extend.gprs_edge.multislot_class =
                                            QMI_DEFAULT_GPRS_EDGE_MULTISLOT_CLASS;
      #else
        supp_channel_info.supp_channel = QMI_CHANNEL_SUPP_GPRS;
        supp_channel_info.extend.gprs_edge.multislot_class =
                                            QMI_DEFAULT_GPRS_EDGE_MULTISLOT_CLASS;
      #endif
    }
    /* else supp_channel_info.supp_channel stays at QMI_CHANNEL_SUPP_MIN in
       which case qmi_svc_ext_get_default_channel_rates will return rates as 0 */
  } 
  else 
  {
    if(DMSI_TARGET_SUPPORTS_LTE(mode_capability))
    {   
      supp_channel_info.supp_channel = QMI_CHANNEL_SUPP_LTE;
    }
    else if(DMSI_TARGET_SUPPORTS_WCDMA(mode_capability))
    {  
      supp_channel_info.supp_channel = QMI_CHANNEL_SUPP_WCDMA;
    }
    else if(DMSI_TARGET_SUPPORTS_TDSCDMA(mode_capability))
    {  
      supp_channel_info.supp_channel = QMI_CHANNEL_SUPP_TDSCDMA;
    }
    else if(DMSI_TARGET_SUPPORTS_GSM(mode_capability))
    {
      #if defined(FEATURE_GSM_EGPRS)
        supp_channel_info.supp_channel = QMI_CHANNEL_SUPP_EDGE;
        supp_channel_info.extend.gprs_edge.multislot_class =
                                         qmi_if_info_ptr->gw_info.edge_multislot_class;
      #else
        supp_channel_info.supp_channel = QMI_CHANNEL_SUPP_GPRS;
        supp_channel_info.extend.gprs_edge.multislot_class =
                                         qmi_if_info_ptr->gw_info.gprs_multislot_class;
      #endif
    }
    /* else supp_channel_info.supp_channel stays at QMI_CHANNEL_SUPP_MIN in
       which case qmi_svc_ext_get_default_channel_rates will return rates as 0 */
  }
  qmi_svc_ext_get_default_channel_rates( supp_channel_info,
                                         &tx_rate,
                                         &rx_rate );
#endif /* FEATURE_WCDMA || FEATURE_GSM || FEATURE_LTE || FEATURE_TDSCDMA*/

#if defined (FEATURE_CDMA_800) || defined (FEATURE_CDMA_1900)
  if(DMSI_TARGET_SUPPORTS_HDR(mode_capability))
  {  
    #ifdef FEATURE_HDR_REVA_L1
      supp_channel_info.supp_channel = QMI_CHANNEL_SUPP_HDR_REVA;
    #else /* FEATURE_HDR_REVA_L1 */
      supp_channel_info.supp_channel = QMI_CHANNEL_SUPP_HDR_REV0;
    #endif /* FEATURE_HDR_REVA_L1 */
  }
  else if(DMSI_TARGET_SUPPORTS_CDMA(mode_capability))
  {  
    supp_channel_info.supp_channel = QMI_CHANNEL_SUPP_CDMA;
  }
  /* else supp_channel_info.supp_channel stays at QMI_CHANNEL_SUPP_MIN in
     which case qmi_svc_ext_get_default_channel_rates will return rates as 0 */
  qmi_svc_ext_get_default_channel_rates( supp_channel_info,
                                         &tx_rate_3gpp2,
                                         &rx_rate_3gpp2 );
#endif /* defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900) */

#if (( defined(FEATURE_LTE) || defined(FEATURE_WCDMA) || defined(FEATURE_GSM)) || defined(FEATURE_TDSCDMA)) && \
    (defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900))
    *max_tx_channel_rate = MAX(tx_rate, tx_rate_3gpp2);
    *max_rx_channel_rate = MAX(rx_rate, rx_rate_3gpp2);
#elif defined(FEATURE_WCDMA) || defined(FEATURE_GSM)|| defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA)
    *max_tx_channel_rate = tx_rate;
    *max_rx_channel_rate = rx_rate;
#elif defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900)
    *max_tx_channel_rate = tx_rate_3gpp2;
    *max_rx_channel_rate = rx_rate_3gpp2;
#endif /* defined(FEATURE_LTE) || (defined(FEATURE_WCDMA) || defined(FEATURE_GSM)) || defined(FEATURE_TDSCDMA) &&
          (defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900)) */

  return errval;
} /* qmi_dmsi_get_max_channel_rate */

/*===========================================================================
  FUNCTION QMI_DMSI_GET_HW_SUPPORTED_RADIO_IF_LIST()

  DESCRIPTION
    to get the list of supported radio technologies by the hardware
    
  PARAMETERS
    radio_if_list

  RETURN VALUE
    Number of supported radio_ifs
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static uint8 qmi_dmsi_get_hw_supported_radio_if_list
(
  uint8 *  radio_if_list
)
{
  uint8 index;
  qmi_if_info_type  *qmi_if_info_ptr;
  uint16 mode_capability = 0;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  index = 0;   

  qmi_if_info_ptr = qmi_if_info_get_ptr();

  mode_capability = (uint16)qmi_if_info_ptr->ph_info.mode_capability;

  if(DMSI_TARGET_SUPPORTS_CDMA(mode_capability))
  {
      radio_if_list[index++] = QMI_RADIO_IF_CDMA;
  }
   
  if(DMSI_TARGET_SUPPORTS_HDR(mode_capability))
  {  
    radio_if_list[index++] = QMI_RADIO_IF_CDMA_1XEVDO;
  }
      
  if(DMSI_TARGET_SUPPORTS_GSM(mode_capability))
  {  
    radio_if_list[index++] = QMI_RADIO_IF_GSM;
  }

  if(DMSI_TARGET_SUPPORTS_WCDMA(mode_capability))
  {  
    radio_if_list[index++] = QMI_RADIO_IF_UMTS;
  }

  if(DMSI_TARGET_SUPPORTS_LTE(mode_capability))
  {  
    radio_if_list[index++] = QMI_RADIO_IF_LTE;
  }

  if(DMSI_TARGET_SUPPORTS_TDSCDMA(mode_capability))
  {
    radio_if_list[index++] = QMI_RADIO_IF_TDS;
  }
  return index;
                
} /* qmi_dmsi_get_hw_supported_radio_if_list */



/*===========================================================================
  FUNCTION QMI_DMSI_GET_VOICE_CAPABILITY()

  DESCRIPTION
    to get the voice capability of the mobile
    
  PARAMETERS
    None
    
  RETURN VALUE
    voice_capability
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static uint8 qmi_dmsi_get_voice_capability
(
  void
)
{
#define DMSI_INVALID_VICE_CAP 0xFF

  uint8 sv_mask = 0;
  uint8 ret_val = DMSI_INVALID_VICE_CAP;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  sv_mask = trm_get_simult_cap();
  if( (sv_mask & TRM_SVDO_IS_ENABLED) ||
      (sv_mask & TRM_SVLTE_IS_ENABLED) )
  {
    ret_val = DMSI_SIM_VOICE_DATA;
  }
  else 
  {
    if( (MCS_MODEM_CAP_AVAILABLE == mcs_modem_has_capability(MCS_MODEM_CAPABILITY_FEATURE_GSM ))||
        (MCS_MODEM_CAP_AVAILABLE == mcs_modem_has_capability(MCS_MODEM_CAPABILITY_FEATURE_1X)) )
    {
      ret_val = DMSI_VOICE_ONLY;
    }

    if ((MCS_MODEM_CAP_AVAILABLE == mcs_modem_has_capability(MCS_MODEM_CAPABILITY_FEATURE_HSPA ))||
       (MCS_MODEM_CAP_AVAILABLE == mcs_modem_has_capability( MCS_MODEM_CAPABILITY_FEATURE_WCDMA))||
       (MCS_MODEM_CAP_AVAILABLE == mcs_modem_has_capability( MCS_MODEM_CAPABILITY_FEATURE_TDSCDMA)))
    {
      ret_val = DMSI_VOICE_DATA;
    }

    if ( (MCS_MODEM_CAP_AVAILABLE == mcs_modem_has_capability(MCS_MODEM_CAPABILITY_FEATURE_LTE )) ||
         (MCS_MODEM_CAP_AVAILABLE == mcs_modem_has_capability(MCS_MODEM_CAPABILITY_FEATURE_DO )) )
    {
      if((DMSI_VOICE_ONLY == ret_val ) ||
         (DMSI_VOICE_DATA == ret_val ))
      {
        ret_val = DMSI_VOICE_DATA;
      }
      else
      {
        ret_val = DMSI_DATA_ONLY;
      }
    }

  }

  if ( DMSI_INVALID_VICE_CAP == ret_val )
  {
#if defined(FEATURE_IS2000_REL_A_SVD) || defined(FEATURE_UMTS_CONC_SRVC)
    ret_val= DMSI_SIM_VOICE_DATA;
#elif defined(FEATURE_DATA)
    ret_val= DMSI_VOICE_DATA;
#else /* FEATURE_DATA */
    ret_val= DMSI_VOICE_ONLY;
#endif
  }

  return ret_val;
} /* qmi_dmsi_get_voice_capability */



/*===========================================================================
  FUNCTION QMI_DMSI_GET_SIM_CAPABILITY()

  DESCRIPTION
    to get the SIM capability of the mobile
    
  PARAMETERS
    None

  RETURN VALUE
    sim_capability
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static uint8 qmi_dmsi_get_sim_capability
(
  void
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  #if defined(FEATURE_MMGSDI_GSM) || defined(FEATURE_MMGSDI_UMTS) || defined(FEATURE_MMGSDI_CDMA)
    return(DMSI_SIM_SUPPORT);           
  #else 
    return(DMSI_SIM_NOT_REQ);
  #endif

} /* qmi_dmsi_get_sim_capability */

/*===========================================================================
  FUNCTION QMI_DMSI_GET_VOICE_SUPPORT_CAPABILITY()

  DESCRIPTION
    to get the voice support capability of the mobile,
    used to determine the device capability of fallback for voice    
  PARAMETERS
    None
    
  RETURN VALUE
    voice_support_capability mask identifying 3gpp CSFB and 1x CSFB support
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static uint64 qmi_dmsi_get_voice_support_capability
(
  void
)
{
  uint64 voice_support_capability = DMSI_MASK_VOICE_SUPPORT_NONE;

  #if defined(FEATURE_3GPP_CSFB)
    voice_support_capability |= DMSI_MASK_VOICE_SUPPORT_GW_CSFB_CAPABLE;
  #endif

  #if defined(FEATURE_LTE) && defined(FEATURE_LTE_TO_1X)
    voice_support_capability |= DMSI_MASK_VOICE_SUPPORT_1x_CSFB_CAPABLE;
  #endif

  return voice_support_capability;
} /* qmi_dmsi_get_voice_support_capability */

/*===========================================================================
  FUNCTION QMI_DMSI_GET_SIMUL_VOICE_AND_DATA_CAPABILITY()

  DESCRIPTION
    Returns the Simultaneous Voice and Data Capability of the device
 
  PARAMETERS
    None
    
  RETURN VALUE
    simul_voice_and_data_capability mask identifying SVLTE and SVDO support
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static uint64 qmi_dmsi_get_simul_voice_and_data_capability
(
  void
)
{
  uint8 sv_mask = 0;
  uint64 simul_voice_and_data_capability = DMSI_MASK_SIMUL_VOICE_AND_DATA_CAP_NONE;
  sys_overall_feature_t dev_overall_cap;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  sv_mask = trm_get_simult_cap();
  if( sv_mask & TRM_SVLTE_IS_ENABLED)
  {  
    simul_voice_and_data_capability |= DMSI_MASK_SIMUL_VOICE_AND_DATA_SVLTE_CAPABLE;
  }
  if( sv_mask & TRM_SVDO_IS_ENABLED)
  {  
    simul_voice_and_data_capability |= DMSI_MASK_SIMUL_VOICE_AND_DATA_SVDO_CAPABLE;
  }
  if(QMI_ERR_NONE == qmi_dmsi_get_device_overall_capability(&dev_overall_cap))
  {
    if(dev_overall_cap == SYS_OVERALL_FEATURE_MODE_SGLTE)
    {
      simul_voice_and_data_capability |= DMSI_MASK_SIMUL_VOICE_AND_DATA_SGLTE_CAPABLE;
    }
  }
  return simul_voice_and_data_capability;
} /* qmi_dmsi_get_simul_voice_and_data_capability */


/*===========================================================================
  FUNCTION QMI_DMSI_SET_BATTERY_LEVEL_LIMITS()

  DESCRIPTION
    It is used to determine the global high and low battery level limits to
    be set in the charge module.
    
  PARAMETERS
    curr_bat_lvl : The current battery level
    
  RETURN VALUE
    None
    
  DEPENDENCIES
    none

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_dmsi_set_battery_level_limits
(
  uint8 curr_bat_lvl
)
{
  qmi_dmsi_client_state_type *  cl_sp;
  qmi_dmsi_state_type *         dms_sp;
  uint8                         low_limit, high_limit;
  int                           j;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  dms_sp = (qmi_dmsi_state_type *)&qmi_dms_state;
  low_limit = DMSI_BAT_LVL_MIN;
  high_limit = DMSI_BAT_LVL_MAX;
  
  /*-------------------------------------------------------------------------
    traverse through all the DMS clients to determine the new high and low
    battery limits - the new high and low limts for the charger are the 
    client's upper and lower limts which are closest to the current battery
    level

    Note - It is possible that after looping through the high and low limits 
    to be set in the charger remain unchanged and it will result in again 
    setting the existing charger limits, but that is alright
  -------------------------------------------------------------------------*/
    for( j = 0; j < DMSI_MAX_CLIDS; j++ )
    {
      cl_sp = dms_sp->client[j];
      if( cl_sp && (cl_sp->common.clid != QMI_SVC_CLID_UNUSED) && 
          cl_sp->report_status.report_power_state )
      {
        /*-------------------------------------------------------------------
          The new high limit for the charger is the client's upper or lower
          limit closest to the current battery level on the higher side
        -------------------------------------------------------------------*/
        if(cl_sp->report_status.bat_lvl_upper_limit >= curr_bat_lvl)
        {
          high_limit = MIN(cl_sp->report_status.bat_lvl_upper_limit,
                           high_limit);
        }
        if(cl_sp->report_status.bat_lvl_lower_limit >= curr_bat_lvl)
        {
          high_limit = MIN(cl_sp->report_status.bat_lvl_lower_limit,
                           high_limit);
        }

        /*-------------------------------------------------------------------
          The new low limit for the charger is the client's upper or lower
          limit closest to the current battery level on the lower side
        -------------------------------------------------------------------*/
        if(cl_sp->report_status.bat_lvl_lower_limit <= curr_bat_lvl)
        {
          low_limit = MAX(cl_sp->report_status.bat_lvl_lower_limit,
                          low_limit);
        }
        if(cl_sp->report_status.bat_lvl_upper_limit <= curr_bat_lvl)
        {
          low_limit = MAX(cl_sp->report_status.bat_lvl_upper_limit,
                          low_limit);
        }
      }
    }


  /*-------------------------------------------------------------------------
    Set the high and low limits in the charger to the computed global high
    and low limts
  -------------------------------------------------------------------------*/
  qmi_charger_set_battery_level_limits(low_limit, high_limit);

  qmi_dmsi_global.pwr_rpt.low_limit = low_limit;
  qmi_dmsi_global.pwr_rpt.high_limit = high_limit;
} /* dmsi_set_battery_level_limits() */

/*===========================================================================
  FUNCTION QMI_DMSI_GET_MULTISIM_INFO()

  DESCRIPTION
    Get the multisim capabilities of the device from policyman and populate the response

  PARAMETERS
    response (input parameter)
    response (output parameter)

  RETURN VALUE
    errval

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static qmi_error_e_type qmi_dmsi_get_multisim_info
(
  dsm_item_type ** response,
  uint8            cmd_type
)
{  
  policyman_status_t  status;   
  policyman_item_t const *cm_policyman_device_config = NULL;
  policyman_item_id_t ids[] = { POLICYMAN_ITEM_DEVICE_CONFIGURATION };
  uint32             max_subs;
  uint32             config_list_len;
  uint32             max_active;
  uint32             rat; 
  uint64             rat_mask;
  int32              cfg_index,subs_index;
  uint16             total_size, subs_list_size;
  uint8              type;
  qmi_error_e_type   errval = QMI_ERR_NONE;

  struct
  {
    uint8 max_subs;     
    uint8 subs_cfg_list_len;
    uint8 max_active;
    /**< The maximum number of subscriptions listed in this configuration that can be 
         simultaneously active.  If this number is less than max_subscriptions it implies
         that any combination of the subscriptions in this configuration can be active 
         and the remaining can be in standby 
      */
  } v_opt_device_cap;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  memset(&v_opt_device_cap,0x00,sizeof(v_opt_device_cap));

  status = policyman_get_items(
              &ids[0],
              sizeof(ids)/sizeof(policyman_item_id_t),
              &cm_policyman_device_config
              );
  if (POLICYMAN_FAILED(status))
  {
    errval = QMI_ERR_INTERNAL;
    return errval;
  }
  
  status = 
    policyman_device_config_num_sims(cm_policyman_device_config,(size_t*)&max_subs);
  v_opt_device_cap.max_subs = (uint8)max_subs;

  if (POLICYMAN_FAILED(status))
  {
    errval = QMI_ERR_INTERNAL;
    return errval;
  }

  status = policyman_device_config_max_active(cm_policyman_device_config, (size_t*)&max_active);
  v_opt_device_cap.max_active = (uint8)max_active;

  if (POLICYMAN_FAILED(status))
  {
    errval = QMI_ERR_INTERNAL;
    return errval;
  }
      
  status = policyman_device_config_num_cfgs(cm_policyman_device_config, (size_t*)&config_list_len);

  if (POLICYMAN_FAILED(status))
  {
    errval = QMI_ERR_INTERNAL;
    return errval;
  }

  if( (max_subs > DMSI_MAX_SUBS_LIST_LEN) || ( config_list_len > DMSI_MAX_CONFIG_LIST_LEN ) ||
     (max_active > max_subs) )
  {
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }
   
  v_opt_device_cap.subs_cfg_list_len = config_list_len;

  LOG_MSG_INFO1_3("num_sims %d max_active %d num_cfg %d",
                  max_subs, max_active, config_list_len);

  cfg_index  = v_opt_device_cap.subs_cfg_list_len;
  subs_index = v_opt_device_cap.max_subs;
  
  /* The total size of this TLV will be sizeof_max_subs + 1 byte (config_list_len) +
     the size of all subscription_list array. 
     sizeof array = sizeof_max_subs + 1 byte(subs_list_len) + max_subs * rat_mask_size
  */
  subs_list_size = sizeof(uint8) + sizeof(uint8) + (max_subs * sizeof(uint64));
  total_size = sizeof(uint8) + sizeof(uint8) + (subs_list_size * cfg_index);  

  for (cfg_index -= 1 ; cfg_index >= 0 ; cfg_index--)
  {
    for (subs_index -= 1 ; subs_index >= 0 ; subs_index--)
    {
      status = policyman_device_config_get_config(cm_policyman_device_config, cfg_index, subs_index, &rat);
      if (POLICYMAN_FAILED(status))
      {
        errval = QMI_ERR_INTERNAL;
        goto send_result;
      }
      
      rat_mask = qmi_dmsi_convert_rat_mask(rat);

      if(!QMI_SVC_PKT_PUSH(response,&rat_mask,sizeof(uint64)))
      {
        LOG_MSG_ERROR_0( "Could not push 0x13 tlv to message: out of memory");
        HANDLE_FAILURE();
      }
    }
    if(!(QMI_SVC_PKT_PUSH((dsm_item_type **)response,&v_opt_device_cap.max_subs,sizeof(uint8))) ||
       !(QMI_SVC_PKT_PUSH((dsm_item_type **)response,&v_opt_device_cap.max_active,sizeof(uint8))))
    {
        LOG_MSG_ERROR_0( "Could not push 0x13 tlv to message: out of memory");
        HANDLE_FAILURE();
    }
    subs_index = v_opt_device_cap.max_subs;
  }

  if(!(QMI_SVC_PKT_PUSH((dsm_item_type **)response,&v_opt_device_cap.subs_cfg_list_len,sizeof(uint8))) ||
     !(QMI_SVC_PKT_PUSH((dsm_item_type **)response,&v_opt_device_cap.max_subs,sizeof(uint8))))
  {
    LOG_MSG_ERROR_0( "Could not push 0x13 tlv to message: out of memory");
    HANDLE_FAILURE();
  }

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/

  type = cmd_type;
  
  if( !(QMI_SVC_PKT_PUSH((dsm_item_type **)response, &total_size, sizeof(uint16))) ||
      !(QMI_SVC_PKT_PUSH((dsm_item_type **)response, &type, sizeof(uint8))) )
  {
    LOG_MSG_ERROR_0( "Could not push 0x13 tlv to message: out of memory");
    HANDLE_FAILURE();
  }

  send_result:
  policyman_item_release(cm_policyman_device_config);
  return errval;
}

/*===========================================================================
  FUNCTION QMI_DMSI_SET_AP_SW_VERSION()
  
  
  DESCRIPTION
    Sets the AP Software Version
  	  
  PARAMETERS
    sp		: service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp 	: client state pointer
    sdu_in	: incoming request
  
  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None
  
  SIDE EFFECTS
    None
  ===========================================================================*/
static dsm_item_type* qmi_dmsi_set_ap_sw_version
(
  void *			  sp,
  void *			  cmd_buf_p,
  void *			  cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean 		     retval;
  
  sw_version_s_type  v_in_req;
   
  boolean    got_v_in_req;
  uint8	     type;
  uint16     len;
  void *     value;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  
  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);
  
  response = NULL;
  errval = QMI_ERR_NONE;
  got_v_in_req = FALSE;
  /*
   * it is safe to clear this struct here, for no value is expected.
   * this also satisfies lint warnings.
   */
  memset((void *)&v_in_req, 0, sizeof(v_in_req));
  
  while (*sdu_in)
  {
    if( !qmi_svc_get_tl( sdu_in, &type, &len ) )
    {
  	continue;
    }
  
    // special value 0 = variable length or don't care (unrecognzied TLV)
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
  	got_v_in_req = TRUE;
        v_in_req.length = (uint8)len;
        value = (void *) &v_in_req.sw_version;
        break;
  
     default:
       LOG_MSG_INFO2 ("Unrecognized TLV type (%d)",type,0,0);
       break;
    }
   
    /*-----------------------------------------------------------------------
  	If type is unrecognized, value will be NULL, and qmi_svc_dsm_pullup will 
  	free the unrecognized value, i.e. we skip past unrecognized TLVs
    -----------------------------------------------------------------------*/
    if(len != qmi_svc_dsm_pullup( sdu_in, value, len,FILE_ID_DS_QMI_DMS,__LINE__  ))
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }
  
  if(!got_v_in_req)
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }
  
  if(len > DMSI_SW_VER_LEN)
  {
    LOG_MSG_ERROR_0 ("Invalid sw version length");
    errval = QMI_ERR_MALFORMED_MSG;
    goto send_result;    
  }

  v_in_req.sw_version[v_in_req.length] = '\0';

  //Call CM API to Set APSS Sw Version
  cm_set_apss_sw_version (&v_in_req);

  send_result:
	result = (errval == QMI_ERR_NONE)?QMI_RESULT_SUCCESS: QMI_RESULT_FAILURE;
	retval = qmi_svc_put_result_tlv(&response, result, errval);
	CHECK_RETVAL();
	return response;
} /* qmi_dmsi_set_ap_sw_version() */

/*===========================================================================
  FUNCTION QMI_DMSI_GET_DEVICE_OVERALL_CAPABILITY()

  DESCRIPTION
    Get the overall capabilities(h/w + configuration) of the device from policyman and populate the response

  PARAMETERS
    device_overall_cap (input parameter)
    errval (output parameter)

  RETURN VALUE
    errval

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static qmi_error_e_type qmi_dmsi_get_device_overall_capability
(
  sys_overall_feature_t* device_overall_cap
)
{ 
  qmi_error_e_type   errval = QMI_ERR_NONE;

  policyman_status_t  status;   
  policyman_item_t const *cm_policyman_device_config = NULL;
  policyman_item_id_t ids[] = { POLICYMAN_ITEM_DEVICE_CONFIGURATION };
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if(NULL == device_overall_cap)
  {
    errval = QMI_ERR_INTERNAL;
    LOG_MSG_ERROR_0("Invalid input parameter ");
    goto send_result;
  }
  
  status = policyman_get_items(
              &ids[0],
              sizeof(ids)/sizeof(policyman_item_id_t),
              &cm_policyman_device_config
              );
  if (POLICYMAN_FAILED(status))
  {
    errval = QMI_ERR_INTERNAL;
    LOG_MSG_ERROR_0("get_items fail"); 
    goto send_result;

  }
  
  status = 
    policyman_device_config_overall_feature(cm_policyman_device_config,(sys_overall_feature_t*)device_overall_cap);

  if (POLICYMAN_FAILED(status))
  {
    errval = QMI_ERR_INTERNAL;
    LOG_MSG_ERROR_0("device_config_overall_feature fail");
    goto send_result;
  }

  if((sys_overall_feature_t)*device_overall_cap > SYS_OVERALL_FEATURE_MODE_SGLTE)
  {
    errval = QMI_ERR_INTERNAL;
    LOG_MSG_ERROR_1("cap is greater %d", *device_overall_cap);
  }

  send_result:
  if(NULL != cm_policyman_device_config)
  {
    policyman_item_release(cm_policyman_device_config);
  }
  return errval;
}


/*===========================================================================
  FUNCTION QMI_DMSI_GET_SUBS_DEVICE_FEATURE_MODE()

  DESCRIPTION
    Get the device static feature mode of the device from policyman and 
    populate the response

  PARAMETERS
    dms pointer to the response (input parameter)
    errval (output parameter)

  RETURN VALUE
    errval

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static qmi_error_e_type qmi_dmsi_get_subs_device_feature_mode
(
  dsm_item_type ** response
)
{
  qmi_error_e_type   errval = QMI_ERR_NONE;
  uint32   max_subs,subs_index;
  uint16   total_size;
  uint8    type,v_max_subs;
  int i;
  typedef struct
  {
    uint32 sub_feature_mode;
  }dev_feature_mode_type;

  dev_feature_mode_type   v_dev_feature_mode[QMI_DMS_POLICYMAN_MAX_SUBS];
  sys_subs_feature_t      subs_device_feature_mode;
  sys_overall_feature_t   device_overall_cap;
  qmi_dmsi_subs_device_feature_mode_type  dms_sub_feature_mode;

  policyman_status_t      status;
  policyman_item_t const  *cm_policyman_device_config = NULL;
  policyman_item_id_t ids[] = { POLICYMAN_ITEM_DEVICE_CONFIGURATION };
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_INFO3_0("qmi_dmsi_get_subs_device_feature_mode");
  memset(&v_dev_feature_mode,0x0,sizeof(v_dev_feature_mode));

  /*Get the policyman items intially*/
  status = policyman_get_items_block(
              &ids[0],
              sizeof(ids)/sizeof(policyman_item_id_t),
              &cm_policyman_device_config);
  if (POLICYMAN_FAILED(status))
  {
    errval = QMI_ERR_INTERNAL;
    LOG_MSG_ERROR_0("policyman_get_items_block failed");
    goto send_result;
  }

  /*Get the policyman overall feature first and 
    if it returns the MULTISIM then go for per subs feature mode*/
  status = 
    policyman_device_config_overall_feature(cm_policyman_device_config,&device_overall_cap);
  if (POLICYMAN_FAILED(status))
  {
    errval = QMI_ERR_INTERNAL;
    LOG_MSG_ERROR_0("device_config_overall_feature fail");
    goto send_result;
  }

  if ( device_overall_cap != SYS_OVERALL_FEATURE_MODE_MULTISIM )
  {
    /*Single sim case, treat this as the first subs and send info*/
    max_subs = 1;     
    switch(device_overall_cap)
    { 
      case SYS_OVERALL_FEATURE_MODE_NORMAL:
          dms_sub_feature_mode  = DMS_SUBS_FEATURE_MODE_NORMAL;
        break;
      case SYS_OVERALL_FEATURE_MODE_SVLTE:
          dms_sub_feature_mode  = DMS_SUBS_FEATURE_MODE_SVLTE;
        break;
      case SYS_OVERALL_FEATURE_MODE_SGLTE:
          dms_sub_feature_mode  = DMS_SUBS_FEATURE_MODE_SGLTE;
        break;
      case SYS_OVERALL_FEATURE_MODE_SRLTE:
          dms_sub_feature_mode  = DMS_SUBS_FEATURE_MODE_SRLTE;
        break;
      default:
          dms_sub_feature_mode  = DMS_SUBS_FEATURE_MODE_NORMAL;
        break;
    }
    /*Fill the first element of the array*/
    v_dev_feature_mode[0].sub_feature_mode= (uint32)dms_sub_feature_mode;
  }
  else
  {
    /*Multi sim case, get the per subs info and send it*/
    status = 
      policyman_device_config_num_sims(cm_policyman_device_config,(size_t*)&max_subs);
    if (POLICYMAN_FAILED(status) || (max_subs < SYS_MODEM_AS_ID_1) || 
      (max_subs > SYS_MODEM_AS_ID_3))
    {
      errval = QMI_ERR_INTERNAL;
      goto send_result;
    }

    for( subs_index = (uint32)SYS_MODEM_AS_ID_1;
         (subs_index < max_subs) && (subs_index < QMI_DMS_POLICYMAN_MAX_SUBS);
         subs_index++ )
    {
      status = policyman_device_config_get_subs_feature(cm_policyman_device_config,0,
                                                        subs_index,
                                                        &subs_device_feature_mode);
      if (POLICYMAN_FAILED(status))
      {  
       errval = QMI_ERR_INTERNAL;
       goto send_result;
      }

      switch(subs_device_feature_mode)
      { 
        case SYS_SUBS_FEATURE_MODE_NORMAL:
             dms_sub_feature_mode = DMS_SUBS_FEATURE_MODE_NORMAL;
          break;
        case SYS_SUBS_FEATURE_MODE_SVLTE:
          dms_sub_feature_mode    = DMS_SUBS_FEATURE_MODE_SVLTE;
          break;
        case SYS_SUBS_FEATURE_MODE_SGLTE:
            dms_sub_feature_mode  = DMS_SUBS_FEATURE_MODE_SGLTE;
          break;
        case SYS_SUBS_FEATURE_MODE_SRLTE:
            dms_sub_feature_mode  = DMS_SUBS_FEATURE_MODE_SRLTE;
          break;
        default:
            dms_sub_feature_mode  = DMS_SUBS_FEATURE_MODE_NORMAL;
          break;
      }
      v_dev_feature_mode[subs_index].sub_feature_mode = (uint32)dms_sub_feature_mode;
    }
  }

  v_max_subs = (uint8)max_subs;
  type = (uint8)DMSI_PRM_TYPE_SUBS_DEVICE_FEATURE_MODE;
  total_size = (uint16)((max_subs * sizeof(uint32)) + sizeof(uint8));

  for(i = max_subs-1; i>=0; i--)
  { /*push the device feature mode subs wise*/
    if(!(QMI_SVC_PKT_PUSH((dsm_item_type **)response,&v_dev_feature_mode[i].sub_feature_mode,sizeof(uint32))))
    {
      HANDLE_FAILURE();
    }
  }
  /*no of above instances pushed*/
  if(!(QMI_SVC_PKT_PUSH((dsm_item_type **)response,&v_max_subs,sizeof(uint8))))
  {
    HANDLE_FAILURE();
  }
  if( !(QMI_SVC_PKT_PUSH((dsm_item_type **)response, &total_size, sizeof(uint16))) ||
      !(QMI_SVC_PKT_PUSH((dsm_item_type **)response, &type, sizeof(uint8))) )
  {
    HANDLE_FAILURE();
  }

  send_result:
  if(NULL != cm_policyman_device_config)
  {
    policyman_item_release(cm_policyman_device_config);
  }
  return errval;
}


/*===========================================================================
  FUNCTION QMI_DMSI_GET_MAX_ACTIVE_DATA_SUBS()

  DESCRIPTION
    Get the max active data subscriptions on device from policyman 
    and populate the response

  PARAMETERS
    response - dsm pointer to the response (input parameter)
    cmd_type - specifies the tlv id (input parameter)
    errval (output parameter)

  RETURN VALUE
    errval

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static qmi_error_e_type qmi_dmsi_get_max_active_data_subs
(
  dsm_item_type ** response,
  uint8            cmd_type
)
{
  policyman_status_t  status = -1; 
  policyman_item_t const *cm_policyman_device_config = NULL;
  policyman_item_id_t id1[] = { POLICYMAN_ITEM_DEVICE_CONFIGURATION };
  uint8    type = 0;
  uint16   size = 0;
  uint32   max_data_subs = 0;
  qmi_error_e_type errval = QMI_ERR_NONE;
  struct
  {
    uint8 max_active_data;
  } v_dev_max_data;
  
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  
  LOG_MSG_INFO3_0("qmi_dmsi_get_max_active_data_subs");
  
  memset(&v_dev_max_data,0x00,sizeof(v_dev_max_data));
  status = policyman_get_items(
                &id1[0],
                sizeof(id1)/sizeof(policyman_item_id_t),
                &cm_policyman_device_config
                );
  if (POLICYMAN_FAILED(status))
  {
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  status = 
    policyman_device_config_max_active_data(cm_policyman_device_config,(size_t*)&max_data_subs);
  if (POLICYMAN_FAILED(status) ||  (max_data_subs < SYS_MODEM_AS_ID_1) || 
      (max_data_subs > SYS_MODEM_AS_ID_3))
  {
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  v_dev_max_data.max_active_data = (uint8)max_data_subs;
  
  type = cmd_type;
  size = (uint16)sizeof(uint8);
  /*device configuration of subs*/
  if(!(QMI_SVC_PKT_PUSH((dsm_item_type **)response,&v_dev_max_data.max_active_data, sizeof(uint8))))
  {
     HANDLE_FAILURE();
  }
  if( !(QMI_SVC_PKT_PUSH((dsm_item_type **)response, &size, sizeof(uint16))) ||
      !(QMI_SVC_PKT_PUSH((dsm_item_type **)response, &type, sizeof(uint8))) )
  {
     HANDLE_FAILURE();
  }
  send_result:
  if(NULL != cm_policyman_device_config)
  {
    policyman_item_release(cm_policyman_device_config);
  }
  return errval;
}

#if defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900)
/*===========================================================================
  FUNCTION QMI_DMSI_MOBNUM_PCS_TO_TEXT()

  DESCRIPTION
    convert MDN NV format to ascii-text representation
    
  PARAMETERS
    dest : will contain the mobile number in ascii-text format
    src  : mobile number in NV format

  RETURN VALUE
    None
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_dmsi_mobnum_pcs_to_text
(
  char *                  dest, 
  nv_mob_dir_data_type *  src
)
{
  int i;
  int len;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(dest && src);

  len = src->n_digits;

  if(len > NV_DIR_NUMB_PCS_SIZ)
  {
    LOG_MSG_INFO1_0("Ignoring Mobile Number digits 16 and higher");
  }

  /*-------------------------------------------------------------------------
    MDN NV format is sort of like BCD, but 8 bits instead of 4, and 
    '0' is represented as 0x0A.  Stop parsing if a place holder digit (0x0F)
    is found.
  -------------------------------------------------------------------------*/
  for (i=0; (i < MIN(len,NV_DIR_NUMB_PCS_SIZ)) && (0x0F != src->digitn[i]); i++)
  {
    dest[i] = (src->digitn[i]==10) ? (char)'0' : (char)src->digitn[i] + '0';
  }
  dest[i] = '\0';
} /* qmi_dmsi_mobnum_pcs_to_text() */
#endif /* defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900) */

/*===========================================================================
  FUNCTION QMI_DMSI_GET_UIM_ICCID()

  DESCRIPTION
    Gives the iccid information of the SIM card
    
  PARAMETERS
    sp           : service state pointer
    cl_sp        : client state pointer
    cmd_buf_p    : the current command buffer
    sdu_in       : dsm_item containing incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_get_uim_iccid
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;
#ifdef FEATURE_MMGSDI_SESSION_LIB
  mmgsdi_return_enum_type mmgsdi_status;
  mmgsdi_access_type      file_access_type;
  qmi_if_info_type       *qmi_if_info_ptr;
  qmi_dmsi_client_state_type * dms_cl_sp;
  ds_qmi_fw_async_cmd_buf_type  *async_cmd_buf;  
#endif /* FEATURE_MMGSDI_SESSION_LIB */
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  response = NULL;
  errval = QMI_ERR_NONE;

#ifndef FEATURE_MMGSDI_SESSION_LIB
    errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
    goto send_result;
#else
  if ((errval = qmi_dmsi_get_uim_status()) != QMI_ERR_NONE)
  {
    goto send_result;
  }

  qmi_if_info_ptr = qmi_if_info_get_ptr();
  file_access_type.access_method = MMGSDI_EF_ENUM_ACCESS;
  file_access_type.file.file_enum = MMGSDI_ICCID;

  dms_cl_sp = (qmi_dmsi_client_state_type*)cl_sp;
  async_cmd_buf = ds_qmi_fw_new_async_cmd_buf(cmd_buf_p, dms_cl_sp->cookie);
  if(async_cmd_buf == NULL)
  {
    errval = QMI_ERR_NO_MEMORY;
    goto send_result;
  }

  ds_qmi_cflog_amss_call("mmgsdi_session_read_transparent()");
  mmgsdi_status = mmgsdi_session_read_transparent(
                                      qmi_if_info_ptr->session_info[QMI_IF_MMGSD_GW_PROV_PRI_SESSION].session_id,
                                      file_access_type,
                                      0,
                                      DMSI_MMGSDI_ICCID_LEN,
                                      qmi_if_mmgsdi_resp_cback,
                                      (uint32) async_cmd_buf);

  if(mmgsdi_status != MMGSDI_SUCCESS)
  {
    ds_qmi_fw_free_async_cmd_buf(async_cmd_buf);	
    errval = qmi_if_map_mmgsdi_status_to_errval(mmgsdi_status,__LINE__);
    goto send_result;
  }

  return QMI_SVC_RESPONSE_PENDING;
#endif /* FEATURE_MMGSDI_SESSION_LIB */

send_result:
  result = (errval == QMI_ERR_NONE) ? QMI_RESULT_SUCCESS 
                                    : QMI_RESULT_FAILURE;

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();

  return response;
} /* qmi_dmsi_get_uim_iccid() */

#ifdef FEATURE_DATA_QMI_ADDENDUM
/*===========================================================================
  FUNCTION QMI_DMSI_GET_HOST_LOCK()

  DESCRIPTION
    Retrieve the Host Lock Code 
        
  PARAMETERS
    sp           : service state pointer
    cl_sp        : client state pointer
    cmd_buf_p    : the current command buffer
    sdu_in       : dsm_item containing incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type*  qmi_dmsi_get_host_lock
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

#ifdef FEATURE_UNDP_NOTEBOOK_LOCK_IDS
  uint32 v_out_req;
  nv_stat_enum_type  nv_status;
  nv_item_type      *dcc_nv_item_ptr;
#endif /* FEATURE_UNDP_NOTEBOOK_LOCK_IDS */

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  response = NULL;
  errval = QMI_ERR_NONE;

#ifdef FEATURE_UNDP_NOTEBOOK_LOCK_IDS
  v_out_req = 0;

  /*-------------------------------------------------------------------------
    Allocate temporary memory for the NV item
  -------------------------------------------------------------------------*/
  dcc_nv_item_ptr = (nv_item_type *)
    qmi_svc_ps_system_heap_mem_alloc(sizeof(nv_item_type),FILE_ID_DS_QMI_DMS,__LINE__);
  if( dcc_nv_item_ptr == NULL )
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
    goto send_result;
  }
  
  /*-------------------------------------------------------------------------
    Query The Host Lock ID
  -------------------------------------------------------------------------*/
  LOG_MSG_INFO1_0("dms get host lock id");
  
  memset(dcc_nv_item_ptr, 0, sizeof(nv_item_type));
  nv_status = dcc_get_nv_item(NV_UNDP_NOTEBOOK_INFO_I, dcc_nv_item_ptr);
  if(NV_DONE_S == nv_status)
  {
    v_out_req = dcc_nv_item_ptr->undp_notebook_info.undp_notebook_id;
    if (v_out_req == TRAILER_SIGNATURE_NOTEBOOK_LOCK_KEY) 
    {
      errval = QMI_ERR_NOT_PROVISIONED;
    }
  }
  else if (NV_NOTACTIVE_S == nv_status) 
  {
    errval = QMI_ERR_NOT_PROVISIONED;
  } 
  else 
  {
    errval = QMI_ERR_INTERNAL;
  }

  if (QMI_ERR_NONE == errval)
  {
    if( FALSE == qmi_svc_put_param_tlv(&response, 
                                       QMI_TYPE_REQUIRED_PARAMETERS,
                                       sizeof(v_out_req),
                                       &v_out_req ) )
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  /*-------------------------------------------------------------------------
    Free the temporary memory allocated for NV item.
  -------------------------------------------------------------------------*/
  PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_item_ptr);

send_result:
#else /* FEATURE_UNDP_NOTEBOOK_LOCK_IDS */
  errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
#endif /* FEATURE_UNDP_NOTEBOOK_LOCK_IDS */

  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
            : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();

  return response;
} /* qmi_dmsi_get_host_lock() */
#endif /* FEATURE_DATA_QMI_ADDENDUM */

/*===========================================================================
  FUNCTION QMI_DMSI_UIM_GET_CK_STATUS()

  DESCRIPTION
    Retrieve the status of a UIM personalization Control Key

  PARAMETERS
    sp           : service state pointer
    cl_sp        : client state pointer
    cmd_buf_p    : the current command buffer
    sdu_in       : dsm_item containing incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type*  qmi_dmsi_uim_get_ck_status
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

#if defined(FEATURE_MMGSDI_SESSION_LIB)
#if defined(FEATURE_WCDMA) || defined(FEATURE_GSM) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA)
  qmi_dmsi_client_state_type *dms_cl_sp;
  qmi_if_info_type           *qmi_if_info_ptr;
  ds_qmi_fw_async_cmd_buf_type  *async_cmd_buf;

  uint8    type;
  uint16   len;
  void *   value;
  uint16   expected_len;

  uint8    v_in_req = 0;
  boolean  got_v_in_req;
  boolean  dequeue_latest = FALSE;

  mmgsdi_return_enum_type mmgsdi_status = MMGSDI_INCORRECT_PARAMS;
  mmgsdi_perso_feature_enum_type feature;
#endif /* defined(FEATURE_WCDMA) || defined(FEATURE_GSM) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA) */
#endif /* defined(FEATURE_MMGSDI_SESSION_LIB) */

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  response = NULL;
  errval = QMI_ERR_NONE;

#if defined(FEATURE_MMGSDI_SESSION_LIB)
#if defined(FEATURE_WCDMA) || defined(FEATURE_GSM) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA)
  dms_cl_sp = (qmi_dmsi_client_state_type*)cl_sp;

  got_v_in_req = FALSE;
  
  while (*sdu_in)
  {
    if( !qmi_svc_get_tl( sdu_in, &type, &len ) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        expected_len = sizeof(v_in_req);
        got_v_in_req = TRUE;
        v_in_req = 0;
        value = (void *) &v_in_req;
        break;
  
      default:
        LOG_MSG_INFO2_1 ("Unrecognized TLV type (%d)", type);
        break;
    }

    if(!qmi_svc_validate_type_length( type,
                                      len,
                                      expected_len,
                                      FILE_ID_DS_QMI_DMS,__LINE__) )
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    /*-----------------------------------------------------------------------
      If type is unrecognized, value will be NULL, and qmi_svc_dsm_pullup will 
      free the unrecognized value, i.e. we skip past unrecognized TLVs
    -----------------------------------------------------------------------*/
    if(len != qmi_svc_dsm_pullup( sdu_in, value, len ,FILE_ID_DS_QMI_DMS,__LINE__ ))
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  if(!got_v_in_req)
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  feature = (mmgsdi_perso_feature_enum_type) v_in_req;
  if (MMGSDI_PERSO_SIM < feature)
  {
    errval = QMI_ERR_INVALID_ARG;
    goto send_result;
  }

  if ((errval = qmi_dmsi_get_uim_status()) != QMI_ERR_NONE)
  {
    goto send_result;
  }

  q_put(&qmi_dmsi_global.sim.get_ck_state_q,
        &(((qmi_cmd_buf_type*)cmd_buf_p)->link));

  dms_cl_sp->get_ck_status_pending = feature;
  dequeue_latest = TRUE;

  if (qmi_dmsi_global.sim.perso_scratch.in_use)
  {
    LOG_MSG_INFO1_0("GET_CK_STATUS added to pending queue");
    return QMI_SVC_RESPONSE_PENDING;
  }
  else
  {
    memset( &qmi_dmsi_global.sim.perso_scratch, 0,
            sizeof(qmi_dmsi_global.sim.perso_scratch) );
    qmi_dmsi_global.sim.perso_scratch.in_use = TRUE;
  }

  qmi_if_info_ptr = qmi_if_info_get_ptr();
  async_cmd_buf = ds_qmi_fw_new_async_cmd_buf(cmd_buf_p, dms_cl_sp->cookie);
  if(async_cmd_buf == NULL)
  {
    errval = QMI_ERR_NO_MEMORY;
    goto send_result;
  }
  
  mmgsdi_status = mmgsdi_session_perso_get_indicators(
                    qmi_if_info_ptr->session_info[QMI_IF_MMGSD_GW_PROV_PRI_SESSION].session_id,
                    MMGSDI_PERSO_ALL_FEATURE_IND,
                    qmi_if_mmgsdi_resp_cback,
                    (uint32) async_cmd_buf );
  if (MMGSDI_SUCCESS != mmgsdi_status)
  {
    ds_qmi_fw_free_async_cmd_buf(async_cmd_buf);  
    errval = qmi_if_map_mmgsdi_status_to_errval(mmgsdi_status,__LINE__);
    goto send_result;
  }
  qmi_dmsi_global.sim.perso_scratch.state_req_pending = TRUE;

  async_cmd_buf = ds_qmi_fw_new_async_cmd_buf(cmd_buf_p, dms_cl_sp->cookie);
  if(async_cmd_buf == NULL)
  {
    errval = QMI_ERR_NO_MEMORY;
    goto send_result;
  }

  mmgsdi_status = mmgsdi_session_perso_get_dck_retries(
                    qmi_if_info_ptr->session_info[QMI_IF_MMGSD_GW_PROV_PRI_SESSION].session_id,
                    MMGSDI_PERSO_DCK_RETRIES,
                    qmi_if_mmgsdi_resp_cback,
                    (uint32) async_cmd_buf );
  if (MMGSDI_SUCCESS != mmgsdi_status)
  {
    ds_qmi_fw_free_async_cmd_buf(async_cmd_buf);  
    errval = qmi_if_map_mmgsdi_status_to_errval(mmgsdi_status,__LINE__);
    goto send_result;
  }
  qmi_dmsi_global.sim.perso_scratch.verify_req_pending = TRUE;

  async_cmd_buf = ds_qmi_fw_new_async_cmd_buf(cmd_buf_p, dms_cl_sp->cookie);
  if(async_cmd_buf == NULL)
  {
    errval = QMI_ERR_NO_MEMORY;
    goto send_result;
  }
  
  mmgsdi_status = mmgsdi_session_perso_get_dck_retries(
                    qmi_if_info_ptr->session_info[QMI_IF_MMGSD_GW_PROV_PRI_SESSION].session_id,
                    MMGSDI_PERSO_UNBLOCK_DCK_RETRIES,
                    qmi_if_mmgsdi_resp_cback,
                    (uint32) async_cmd_buf );
  if (MMGSDI_SUCCESS != mmgsdi_status)
  {
    ds_qmi_fw_free_async_cmd_buf(async_cmd_buf);  
    errval = qmi_if_map_mmgsdi_status_to_errval(mmgsdi_status,__LINE__);
    goto send_result;
  }
  qmi_dmsi_global.sim.perso_scratch.unblock_req_pending = TRUE;
  return QMI_SVC_RESPONSE_PENDING;
#else
  LOG_MSG_INFO1_0( "Unable to process GET_CK_STATUS request:"
                   " non-GSM non-WCDMA target" );
  errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
  goto send_result;
#endif /* defined(FEATURE_WCDMA) || defined(FEATURE_GSM) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA) */
#else
  LOG_MSG_INFO1_0( "Unable to process GET_CK_STATUS request:"
                   " no MMGSDI support" );
  errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
  goto send_result;
#endif /* defined(FEATURE_MMGSDI_SESSION_LIB) */

send_result:
#if defined(FEATURE_MMGSDI_SESSION_LIB)
#if defined(FEATURE_WCDMA) || defined(FEATURE_GSM) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA)
/*If any error, dequeue the latest item enqueued*/  
if (dequeue_latest && (errval != QMI_ERR_NONE))
{
  (void) q_last_get(&qmi_dmsi_global.sim.get_ck_state_q);
  memset( &qmi_dmsi_global.sim.perso_scratch, 0,
          sizeof(qmi_dmsi_global.sim.perso_scratch) );
  dms_cl_sp->get_ck_status_pending = MMGSDI_MAX_PERSO_FEATURE_ENUM;
}
#endif /* defined(FEATURE_WCDMA) || defined(FEATURE_GSM) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA) */
#endif /* defined(FEATURE_MMGSDI_SESSION_LIB) */

  /* Support for "Operation Blocking" TLV still pending */

  result = (errval == QMI_ERR_NONE)? QMI_RESULT_SUCCESS: QMI_RESULT_FAILURE;

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();

  return response;
} /* qmi_dmsi_uim_get_ck_status() */

/*===========================================================================
  FUNCTION QMI_DMSI_UIM_SET_CK_PROTECTION()

  DESCRIPTION
    Request for a particular service to be protected by enabling the control
    key

  PARAMETERS
    sp           : service state pointer
    cl_sp        : client state pointer
    cmd_buf_p    : the current command buffer
    sdu_in       : dsm_item containing incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type*  qmi_dmsi_uim_set_ck_protection
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

#if defined(FEATURE_MMGSDI_SESSION_LIB)
#if defined(FEATURE_GSM) || defined(FEATURE_WCDMA) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA)
  uint8    type;
  uint16   len;
  void *   value;
  uint16   expected_len;

  PACKED struct PACKED_POST
  {
    uint8  feature;
    uint8  state;
    uint8  ck_len;
    uint8  ck[DMSI_MMGSDI_CK_LEN_MAX];
  } v_in_req;

  boolean  got_v_in_req;
  mmgsdi_return_enum_type mmgsdi_status = MMGSDI_INCORRECT_PARAMS;
  mmgsdi_perso_client_data_type mmgsdi_ck_data;
  qmi_if_info_type  *qmi_if_info_ptr;
  qmi_dmsi_client_state_type * dms_cl_sp;
  ds_qmi_fw_async_cmd_buf_type  *async_cmd_buf; 
#endif /* defined(FEATURE_GSM) || defined(FEATURE_WCDMA) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA) */
#endif /* defined(FEATURE_MMGSDI_SESSION_LIB) */

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  response = NULL;
  errval = QMI_ERR_NONE;

#if defined(FEATURE_MMGSDI_SESSION_LIB)
#if defined(FEATURE_GSM) || defined(FEATURE_WCDMA) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA)
  got_v_in_req = FALSE;
  memset(&v_in_req,0,sizeof(v_in_req));
  
  while (*sdu_in)
  {
    if( !qmi_svc_get_tl( sdu_in, &type, &len ) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        if (len <= sizeof(v_in_req))
        {
          got_v_in_req = TRUE;
          value = (void *) &v_in_req;
        }
        else
        {
          errval = QMI_ERR_ARG_TOO_LONG;
         goto send_result;
        }
        break;
  
      default:
        LOG_MSG_INFO2_1 ("Unrecognized TLV type (%d)", type);
        break;
    }

    if(!qmi_svc_validate_type_length( type,
                                      len,
                                      expected_len,
                                      FILE_ID_DS_QMI_DMS,__LINE__) )
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    /*-----------------------------------------------------------------------
      If type is unrecognized, value will be NULL, and qmi_svc_dsm_pullup will 
      free the unrecognized value, i.e. we skip past unrecognized TLVs
    -----------------------------------------------------------------------*/
    if(len != qmi_svc_dsm_pullup( sdu_in, value, len ,FILE_ID_DS_QMI_DMS,__LINE__  ))
      {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  if(!got_v_in_req)
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  if (MMGSDI_PERSO_SIM < 
         (mmgsdi_perso_feature_enum_type)v_in_req.feature)
  {
    errval = QMI_ERR_INVALID_ARG;
    goto send_result;
  }

  if ((errval = qmi_dmsi_get_uim_status()) != QMI_ERR_NONE)
  {
    goto send_result;
  }

  if (DMSI_MMGSDI_PERSO_STATE_DEACTIVATED != v_in_req.state)
  {
    errval = QMI_ERR_INVALID_ARG;
    goto send_result;
  }

  qmi_if_info_ptr = qmi_if_info_get_ptr();
  dms_cl_sp = (qmi_dmsi_client_state_type*)cl_sp;
  async_cmd_buf = ds_qmi_fw_new_async_cmd_buf(cmd_buf_p, dms_cl_sp->cookie);
  if(async_cmd_buf == NULL)
  {
    errval = QMI_ERR_NO_MEMORY;
    goto send_result;
  }
  
  mmgsdi_ck_data.data_len = v_in_req.ck_len;
  mmgsdi_ck_data.data_ptr = (uint8*)v_in_req.ck;
  mmgsdi_status = mmgsdi_session_perso_deactivate(
                    qmi_if_info_ptr->session_info[QMI_IF_MMGSD_GW_PROV_PRI_SESSION].session_id,
                    (mmgsdi_perso_feature_enum_type)v_in_req.feature,
                    &mmgsdi_ck_data,
                    qmi_if_mmgsdi_resp_cback,
                    (uint32)async_cmd_buf
                  );
  if (mmgsdi_status == MMGSDI_SUCCESS)
  {
    return QMI_SVC_RESPONSE_PENDING;
  }
  else
  {
    ds_qmi_fw_free_async_cmd_buf(async_cmd_buf);  
    errval = qmi_if_map_mmgsdi_status_to_errval(mmgsdi_status,__LINE__);
  }    
#else
  LOG_MSG_INFO1_0( "Unable to process SET_CK_PROTECT request:"
                   " non-GSM non-WCDMA target" );
  errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
  goto send_result;
#endif /* defined(FEATURE_WCDMA) || defined(FEATURE_GSM) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA) */
#else
  LOG_MSG_INFO1_0( "Unable to process SET_CK_PROTECT request:"
                   " no MMGSDI support" );
  errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
  goto send_result;
#endif /* defined(FEATURE_MMGSDI_SESSION_LIB) */

send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
            : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();

  return response;
} /* qmi_dmsi_uim_set_ck_protection() */

/*===========================================================================
  FUNCTION QMI_DMSI_UIM_UNBLOCK_CK()

  DESCRIPTION
    Request for a blocked control key to be unblocked.

  PARAMETERS
    sp           : service state pointer
    cl_sp        : client state pointer
    cmd_buf_p    : the current command buffer
    sdu_in       : dsm_item containing incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type*  qmi_dmsi_uim_unblock_ck
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

#if defined(FEATURE_MMGSDI_SESSION_LIB)
#if defined(FEATURE_WCDMA) || defined(FEATURE_GSM) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA)
  uint8    type;
  uint16   len;
  void *   value;
  uint16   expected_len;

  struct
  {
    uint8  feature;
    uint8  ck_len;
    uint8  ck[DMSI_MMGSDI_CK_LEN_MAX];
  } v_in_req;

  boolean  got_v_in_req;
  mmgsdi_return_enum_type mmgsdi_status = MMGSDI_INCORRECT_PARAMS;
  mmgsdi_perso_client_data_type mmgsdi_ck_data;
  qmi_if_info_type  *qmi_if_info_ptr;
  qmi_dmsi_client_state_type * dms_cl_sp;
  ds_qmi_fw_async_cmd_buf_type  *async_cmd_buf;
#endif /* defined(FEATURE_WCDMA) || defined(FEATURE_GSM) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA) */
#endif /* defined(FEATURE_MMGSDI_SESSION_LIB) */

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  response = NULL;
  errval = QMI_ERR_NONE;

#if defined(FEATURE_MMGSDI_SESSION_LIB)
#if defined(FEATURE_WCDMA) || defined(FEATURE_GSM) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA)
  got_v_in_req = FALSE;
  memset(&v_in_req,0,sizeof(v_in_req));
  
  while (*sdu_in)
  {
    if( !qmi_svc_get_tl( sdu_in, &type, &len ) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        if (len <= sizeof(v_in_req))
        {
          got_v_in_req = TRUE;
          value = (void *) &v_in_req;
        }
        else
        {
          errval = QMI_ERR_ARG_TOO_LONG;
         goto send_result;
        }
        break;
  
      default:
        LOG_MSG_INFO2_1 ("Unrecognized TLV type (%d)", type);
        break;
    }

    if(!qmi_svc_validate_type_length( type,
                                      len,
                                      expected_len,
                                      FILE_ID_DS_QMI_DMS,__LINE__) )
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    /*-----------------------------------------------------------------------
      If type is unrecognized, value will be NULL, and qmi_svc_dsm_pullup will 
      free the unrecognized value, i.e. we skip past unrecognized TLVs
    -----------------------------------------------------------------------*/
    if(len != qmi_svc_dsm_pullup( sdu_in, value, len ,FILE_ID_DS_QMI_DMS,__LINE__  ))
      {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  if(!got_v_in_req)
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  if (MMGSDI_PERSO_SIM < (mmgsdi_perso_feature_enum_type)v_in_req.feature)
  {
    errval = QMI_ERR_INVALID_ARG;
    goto send_result;
  }

  if ((errval = qmi_dmsi_get_uim_status()) != QMI_ERR_NONE)
  {
    goto send_result;
  }

  qmi_if_info_ptr = qmi_if_info_get_ptr();
  dms_cl_sp = (qmi_dmsi_client_state_type*)cl_sp;
  async_cmd_buf = ds_qmi_fw_new_async_cmd_buf(cmd_buf_p, dms_cl_sp->cookie);
  if(async_cmd_buf == NULL)
  {
    errval = QMI_ERR_NO_MEMORY;
    goto send_result;
  }
  
  mmgsdi_ck_data.data_len = v_in_req.ck_len;
  mmgsdi_ck_data.data_ptr = (uint8*)v_in_req.ck;
  mmgsdi_status = mmgsdi_session_perso_unblock(
                    qmi_if_info_ptr->session_info[QMI_IF_MMGSD_GW_PROV_PRI_SESSION].session_id,
                    (mmgsdi_perso_feature_enum_type)v_in_req.feature,
                    &mmgsdi_ck_data,
                    qmi_if_mmgsdi_resp_cback,
                    (uint32) async_cmd_buf );
  if (mmgsdi_status == MMGSDI_SUCCESS)
  {
    return QMI_SVC_RESPONSE_PENDING;
  }
  else
  {
    ds_qmi_fw_free_async_cmd_buf(async_cmd_buf);  
    errval = qmi_if_map_mmgsdi_status_to_errval(mmgsdi_status,__LINE__);
  }
#else
  LOG_MSG_INFO1_0( "Unable to process UNBLOCK_CK request:"
                   " non-GSM non-WCDMA target" );
  errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
  goto send_result;
#endif /* defined(FEATURE_WCDMA) || defined(FEATURE_GSM) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA) */
#else
  LOG_MSG_INFO1_0( "Unable to process UNBLOCK_CK request:"
                   " no MMGSDI support" );
  errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
  goto send_result;
#endif /* defined(FEATURE_MMGSDI_SESSION_LIB) */

send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
            : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();

  return response;
} /* qmi_dmsi_uim_unblock_ck() */

/*===========================================================================
  FUNCTION QMI_DMSI_GET_UIM_IMSI()

  DESCRIPTION
    Gives the imsi information of the SIM card
    
  PARAMETERS
    sp           : service state pointer
    cl_sp        : client state pointer
    cmd_buf_p    : the current command buffer
    sdu_in       : dsm_item containing incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_get_uim_imsi
(
  void *              sp,
  void *              cmd_buf_p,
  void *              cl_sp,
  dsm_item_type **    sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;
#ifdef FEATURE_MMGSDI_SESSION_LIB
  mmgsdi_return_enum_type mmgsdi_status;
  mmgsdi_access_type      sim_filename;
  qmi_if_info_type       *qmi_if_info_ptr;
  qmi_dmsi_client_state_type * dms_cl_sp;
  ds_qmi_fw_async_cmd_buf_type  *async_cmd_buf;  
  uint8 session_type; 
  qmi_if_mmgsd_session_e_type session_index;
#endif /* FEATURE_MMGSDI_SESSION_LIB */
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  response = NULL;
  errval = QMI_ERR_NONE;
#ifndef FEATURE_MMGSDI_SESSION_LIB
  errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
  goto send_result;
#else
  if (qmi_dmsi_global.nv_restrict_imsi_access)
  {
    errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
    goto send_result;
  }

  sim_filename.access_method = MMGSDI_EF_ENUM_ACCESS;
  sim_filename.file.file_enum = MMGSDI_NO_FILE_ENUM;
  session_type = MMGSDI_GW_PROV_PRI_SESSION;
  session_index = QMI_IF_MMGSD_GW_PROV_PRI_SESSION;

#ifdef FEATURE_DUAL_SIM
  if(((qmi_dmsi_client_state_type *)cl_sp)->subscription_id == DMSI_BIND_SUBS_SECONDARY)
  {
    session_type = MMGSDI_GW_PROV_SEC_SESSION;
    session_index = QMI_IF_MMGSD_GW_PROV_SEC_SESSION;
  }
#endif
#ifdef FEATURE_TRIPLE_SIM
  if(((qmi_dmsi_client_state_type *)cl_sp)->subscription_id == DMSI_BIND_SUBS_TERTIARY)
  {
    session_type = MMGSDI_GW_PROV_TER_SESSION;
    session_index = QMI_IF_MMGSD_GW_PROV_TER_SESSION;
  }
#endif
  if ((errval = qmi_dms_get_uim_access_status_ext(session_type)) != QMI_ERR_NONE)
  {
    goto send_result;
  }

  qmi_if_info_ptr = qmi_if_info_get_ptr();
  if (qmi_if_info_ptr->session_info[session_index].app_type == MMGSDI_APP_SIM)
  {
    sim_filename.file.file_enum = MMGSDI_GSM_IMSI;
  }
  else if (qmi_if_info_ptr->session_info[session_index].app_type == MMGSDI_APP_USIM)
  {
    sim_filename.file.file_enum = MMGSDI_USIM_IMSI;
  }
  else
  {
    errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
    goto send_result;
  }

  if(sim_filename.file.file_enum != MMGSDI_NO_FILE_ENUM)
  {
    dms_cl_sp = (qmi_dmsi_client_state_type*)cl_sp;
    async_cmd_buf = ds_qmi_fw_new_async_cmd_buf(cmd_buf_p, dms_cl_sp->cookie);
    if(async_cmd_buf == NULL)
    {
      errval = QMI_ERR_NO_MEMORY;
      goto send_result;
    }
  
    ds_qmi_cflog_amss_call("mmgsdi_session_read_transparent()");
    mmgsdi_status = mmgsdi_session_read_transparent(
                                        qmi_if_info_ptr->session_info[session_index].session_id,
                                        sim_filename,
                                        0,
                                        DMSI_MMGSDI_IMSI_LEN,
                                        qmi_if_mmgsdi_resp_cback,
                                        (uint32) async_cmd_buf);


    if(mmgsdi_status == MMGSDI_SUCCESS)
    {
        /* IMSI will be reported back later from MMGSDI. */
        return QMI_SVC_RESPONSE_PENDING;
    }
    else
    {
      ds_qmi_fw_free_async_cmd_buf(async_cmd_buf);	  
      errval = qmi_if_map_mmgsdi_status_to_errval(mmgsdi_status,__LINE__);
      goto send_result;
    }
  }
#endif /* FEATURE_MMGSDI_SESSION_LIB */

send_result:  
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();

  return response;
} /* qmi_dmsi_get_uim_imsi() */

/*===========================================================================
  FUNCTION QMI_DMSI_RESET_PRL_SERIES_INFO()

  DESCRIPTION
    Resets the stored PRL 'series' information that is used to complet
    QMI_DMS_ACTIVATE_MANUAL requests.    
    
  PARAMETERS
    None

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_dmsi_reset_prl_series_info
(
  void
)
{
  qmi_dmsi_global.prl_info.cl_sp = NULL;
  qmi_dmsi_global.prl_info.seq_num = 0;
  qmi_dmsi_global.prl_info.len_received = 0;
  if(qmi_dmsi_global.prl_info.data != NULL)
  {
    PS_SYSTEM_HEAP_MEM_FREE(qmi_dmsi_global.prl_info.data);
  }
} /* qmi_dmsi_reset_prl_series_info() */

/*===========================================================================
  FUNCTION QMI_DMSI_UIM_GET_STATE()

  DESCRIPTION
    Gives the sim card status 
    
  PARAMETERS
    sp           : service state pointer
    cl_sp        : client state pointer
    cmd_buf_p    : the current command buffer
    sdu_in       : dsm_item containing incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_uim_get_state
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
    dsm_item_type *    response;
    qmi_error_e_type   errval;
    qmi_result_e_type  result;
    boolean            retval;
#ifdef FEATURE_MMGSDI_SESSION_LIB
    uint8              uim_state;
#endif /* FEATURE_MMGSDI_SESSION_LIB */

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

    ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

    response = NULL;
    errval = QMI_ERR_NONE;

#ifndef FEATURE_MMGSDI_SESSION_LIB
    errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
    goto send_result;
#else
  if (qmi_dmsi_global.uim_state == DMSI_SIM_UNKNOWN)
  {
    errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
    goto send_result;
  }

  uim_state = (uint8)qmi_dmsi_global.uim_state;

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  if(FALSE == qmi_svc_put_param_tlv(&response, 
                                    QMI_TYPE_REQUIRED_PARAMETERS,
                                    sizeof(uint8),
                                    &uim_state))
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
  }
#endif /* FEATURE_MMGSDI_SESSION_LIB */

send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();

    return response;

} /* qmi_dmsi_uim_get_state */

/*===========================================================================
  FUNCTION QMI_DMSI_GET_BAND_CAPABILITY()

  DESCRIPTION
    Gives the current band class info
    
  PARAMETERS
    sp           : service state pointer
    cl_sp        : client state pointer
    cmd_buf_p    : the current command buffer
    sdu_in       : dsm_item containing incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_get_band_capability
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response = NULL;
  qmi_error_e_type   errval = QMI_ERR_NONE;
  qmi_result_e_type  result = QMI_RESULT_SUCCESS;
  boolean            retval = FALSE;
  uint64             band_capability = 0;
  qmi_if_info_type  *qmi_if_info_ptr;
  uint64             lte_band_capability = 0, tds_band_capability = 0;

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  qmi_if_info_ptr = qmi_if_info_get_ptr();
  band_capability = (uint64)qmi_if_info_ptr->ph_info.band_capability;

  if(FALSE == qmi_svc_put_param_tlv(&response, 
                                    QMI_TYPE_REQUIRED_PARAMETERS,
                                    sizeof(band_capability),
                                    &band_capability))
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
  }

  lte_band_capability = (uint64)qmi_if_info_ptr->ph_info.lte_band_capability;

  if(FALSE == qmi_svc_put_param_tlv(&response, 
                                    DMSI_PRM_TYPE_LTE_BAND_CAP,
                                    sizeof(lte_band_capability),
                                    &lte_band_capability))
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
  }

  tds_band_capability = (uint64)qmi_if_info_ptr->ph_info.tds_band_capability;

  if(FALSE == qmi_svc_put_param_tlv(&response, 
                                    DMSI_PRM_TYPE_TDS_BAND_CAP,
                                    sizeof(tds_band_capability),
                                    &tds_band_capability))
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
  }

  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();

  return response;

} /* qmi_dmsi_get_band_capability */

/*===========================================================================
  FUNCTION QMI_DMSI_GET_FACTORY_SKU()

  DESCRIPTION
    Gives the factory SKU for the device.
    
  PARAMETERS
    sp           : service state pointer
    cl_sp        : client state pointer
    cmd_buf_p    : the current command buffer
    sdu_in       : dsm_item containing incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_get_factory_sku
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response = NULL;
  qmi_error_e_type   errval = QMI_ERR_NONE;
  qmi_result_e_type  result = QMI_RESULT_SUCCESS;
  boolean            retval = FALSE;
  
  char *             factory_sku;
  int                factory_sku_len;
  uint16             len;
  nv_stat_enum_type  nv_status;
  nv_item_type      *dcc_nv_item_ptr;

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  response = NULL;
  errval = QMI_ERR_NONE;

  /*-------------------------------------------------------------------------
    Allocate temporary memory for the NV item
  -------------------------------------------------------------------------*/
  dcc_nv_item_ptr = (nv_item_type *)
    qmi_svc_ps_system_heap_mem_alloc(sizeof(nv_item_type),FILE_ID_DS_QMI_DMS,__LINE__);
  if( dcc_nv_item_ptr == NULL )
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
    goto send_result;
  }
  
  // Query NV For factory SKU
  memset(dcc_nv_item_ptr, 0, sizeof(nv_item_type));
  nv_status = dcc_get_nv_item(NV_FACTORY_DATA_4_I, dcc_nv_item_ptr);
  if (NV_DONE_S == nv_status)
  {
    factory_sku = (char *) dcc_nv_item_ptr->factory_data_4;
    factory_sku_len = strlen(factory_sku);
    len = (uint16) MIN(65535, factory_sku_len);

    /*-------------------------------------------------------------------------
      Build the response from back to front
    -------------------------------------------------------------------------*/
    if(FALSE == qmi_svc_put_param_tlv(&response, 
                                      QMI_TYPE_REQUIRED_PARAMETERS,
                                      len,
                                      factory_sku))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  } 
  else if (NV_NOTACTIVE_S == nv_status) 
  {
    errval = QMI_ERR_NOT_PROVISIONED;
  } 
  else 
  {
    errval = QMI_ERR_INTERNAL;
  }

  /*-------------------------------------------------------------------------
    Free the temporary memory allocated for NV item.
  -------------------------------------------------------------------------*/
  PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_item_ptr);;
  
send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();

  return response;

} /* qmi_dmsi_get_factory_sku */

#ifdef FEATURE_DATA_QMI_ADDENDUM
/*===========================================================================
  FUNCTION QMI_DMSI_GET_FIRMWARE_PREF()

  DESCRIPTION
    Retrieves the build information from the last call to SET FW PREF.
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_get_firmware_pref
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;

  PACKED struct PACKED_POST img_list_entry {
    uint8 image_type;
    uint8 unique_id[GOBI_IM_UNIQUE_ID_LEN];
    uint8 build_id_len;
    char  build_id[GOBI_IM_BUILD_ID_LEN];
  } *img_list_entry_ptr;

  PACKED struct PACKED_POST {
    uint8 num_images;
    uint8 img_list[sizeof(struct img_list_entry) * DMSI_NUM_SUPPORTED_IMAGE_TYPES];
  } v_required;

  uint32 list_offset;

  char  modem_build_id[GOBI_IM_BUILD_ID_LEN];
  uint8 modem_unique_id[GOBI_IM_UNIQUE_ID_LEN];
  char  pri_build_id[GOBI_IM_BUILD_ID_LEN];
  uint8 pri_unique_id[GOBI_IM_UNIQUE_ID_LEN];

  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  
  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);
  
  response = NULL;
  errval = QMI_ERR_NONE;

  /* Read the image preference out of NV */
  gobi_im_get_image_pref(modem_build_id,
                         modem_unique_id,
                         pri_build_id,
                         pri_unique_id);

  /*-------------------------------------------------------------------------
    Format the response
  -------------------------------------------------------------------------*/
  v_required.num_images = DMSI_NUM_SUPPORTED_IMAGE_TYPES;

  /* modem image info */
  img_list_entry_ptr = (struct img_list_entry *) v_required.img_list;

  img_list_entry_ptr->image_type = DMSI_IMG_TYPE_MODEM;
  img_list_entry_ptr->build_id_len = strlen(modem_build_id);
  memscpy(img_list_entry_ptr->unique_id, 
          GOBI_IM_UNIQUE_ID_LEN, 
          modem_unique_id, 
          GOBI_IM_UNIQUE_ID_LEN);
  strlcpy(img_list_entry_ptr->build_id, modem_build_id, GOBI_IM_BUILD_ID_LEN);

  /* Advance ptr to byte after last byte of modem information */
  list_offset = (sizeof(struct img_list_entry) - GOBI_IM_BUILD_ID_LEN) 
                  + img_list_entry_ptr->build_id_len;
  img_list_entry_ptr = (struct img_list_entry *) &v_required.img_list[list_offset];

  /* PRI image info */
  img_list_entry_ptr->image_type = DMSI_IMG_TYPE_PRI;
  img_list_entry_ptr->build_id_len = strlen(pri_build_id);
  memscpy(img_list_entry_ptr->unique_id, 
          GOBI_IM_UNIQUE_ID_LEN, 
          pri_unique_id, 
          GOBI_IM_UNIQUE_ID_LEN);
  strlcpy(img_list_entry_ptr->build_id, pri_build_id, GOBI_IM_BUILD_ID_LEN);

  /* Update list offset to reflect the total size of the list */
  list_offset += (sizeof(struct img_list_entry) - GOBI_IM_BUILD_ID_LEN) 
                   + img_list_entry_ptr->build_id_len;
  
  if(FALSE == qmi_svc_put_param_tlv(&response, 
                                    QMI_TYPE_REQUIRED_PARAMETERS,
                                    (sizeof(uint8) + list_offset),
                                    &v_required))
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
  }

  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);
  retval = qmi_svc_put_result_tlv( &response, result, errval );
  CHECK_RETVAL();
  
  return response;

} /* qmi_dmsi_get_firmware_pref() */


/*===========================================================================
  FUNCTION QMI_DMSI_SET_FIRMWARE_PREF()

  DESCRIPTION
    Configures the device to use a new firmware preference.
        
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_set_firmware_pref
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *               response;

  PACKED struct PACKED_POST img_list_entry {
    uint8 image_type;
    uint8 unique_id[GOBI_IM_UNIQUE_ID_LEN];
    uint8 build_id_len;
    char  build_id[GOBI_IM_BUILD_ID_LEN];
  } *img_list_entry_ptr;

  PACKED struct PACKED_POST
  {
    uint8 num_entries;
    uint8 img_list[sizeof(struct img_list_entry) * DMSI_NUM_SUPPORTED_IMAGE_TYPES];
  } v_in_req;

  PACKED struct PACKED_POST
  {
    boolean download_override;
  } v_in1;

  PACKED struct PACKED_POST
  {
    uint8   modem_storage_index;
  } v_in2;

  struct
  {
    uint8 num_entries;
    uint8 img_list[DMSI_NUM_SUPPORTED_IMAGE_TYPES];
  } v_out_req;

  PACKED struct PACKED_POST
  {
    uint8 max_build_id_len;
  } v_out1;

  uint16   list_tlv_len;
  uint32   list_offset;
  uint32   new_boot_cfg;

  char     modem_build_id[GOBI_IM_BUILD_ID_LEN];
  uint8 *  modem_unique_id_ptr;
  char     pri_build_id[GOBI_IM_BUILD_ID_LEN];
  uint8 *  pri_unique_id_ptr;

  boolean  got_v_in_req          = FALSE;
  boolean  got_override_tlv      = FALSE;
  boolean  got_storage_index_tlv = FALSE;

  boolean  got_modem_info        = FALSE;
  boolean  got_pri_info          = FALSE;

  uint8    type;
  uint16   len;
  uint16   expected_len;
  void *   value;

  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  memset(&v_in1, 0, sizeof(v_in1));
  memset(&v_in2, 0, sizeof(v_in2));

  new_boot_cfg = 0;
  list_tlv_len = 0;

  modem_unique_id_ptr = NULL;
  pri_unique_id_ptr   = NULL;

  response = NULL;
  errval = QMI_ERR_NONE;

  while (*sdu_in)
  {
    if( !qmi_svc_get_tl( sdu_in, &type, &len ) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        if (len <= sizeof(v_in_req))
        {
          got_v_in_req = TRUE;
          value = &v_in_req;
          list_tlv_len = len;
        }
        else
        {
          errval = QMI_ERR_ARG_TOO_LONG;
          goto send_result;
        }
        break;

      case DMSI_PRM_TYPE_DOWNLOAD_OVERRIDE:
        expected_len = sizeof(v_in1);
        got_override_tlv = TRUE;
        value = &v_in1;
        break;

      case DMSI_PRM_TYPE_MODEM_STORAGE_INDEX:
        expected_len = sizeof(v_in2);
        got_storage_index_tlv = TRUE;
        value = &v_in2;
        break;

      default:
        LOG_MSG_INFO2_1 ("Unrecognized TLV type (%d)", type);
        break;
    }

    if(!qmi_svc_validate_type_length( type,
                                      len,
                                      expected_len,
                                      FILE_ID_DS_QMI_DMS,__LINE__) )
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    /*-----------------------------------------------------------------------
      If type is unrecognized, value will be NULL, and qmi_svc_dsm_pullup will 
      free the unrecognized value, i.e. we skip past unrecognized TLVs
    -----------------------------------------------------------------------*/
    if(len != qmi_svc_dsm_pullup( sdu_in, value, len ,FILE_ID_DS_QMI_DMS,__LINE__  ))
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  /*-------------------------------------------------------------------------
    Parse the image list (mandatory TLV)
  -------------------------------------------------------------------------*/
  LOG_MSG_INFO2_2("got_v_in_req = %d num_entries = %d",
              got_v_in_req, v_in_req.num_entries);
  if (!got_v_in_req ||
      v_in_req.num_entries < DMSI_NUM_SUPPORTED_IMAGE_TYPES)
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }
  else if (v_in_req.num_entries != DMSI_NUM_SUPPORTED_IMAGE_TYPES)
  {
    errval = QMI_ERR_INVALID_ARG;
    goto send_result;
  }

  /* Subtract header length from TLV overall length: now this value gives us
   * the length of only the list portion */
  list_tlv_len -= sizeof(v_out_req) - sizeof(v_out_req.img_list);
  list_offset = 0;

  do
  {
    img_list_entry_ptr = (struct img_list_entry *) &v_in_req.img_list[list_offset];

    if ((list_tlv_len - list_offset) < (sizeof(struct img_list_entry) - GOBI_IM_BUILD_ID_LEN))
    {
      LOG_MSG_ERROR_2("Malformed, remaning bytes %d too small for list entry >= %d",
                      (list_tlv_len-list_offset),
                      (sizeof(struct img_list_entry) - GOBI_IM_BUILD_ID_LEN));
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    switch (img_list_entry_ptr->image_type)
    {
      case DMSI_IMG_TYPE_MODEM:
        if (TRUE == got_modem_info)
        {
          LOG_MSG_ERROR_0("Duplicate modem entry in list");
          errval = QMI_ERR_INVALID_ARG;
          goto send_result;
        }
        if (img_list_entry_ptr->build_id_len >= GOBI_IM_BUILD_ID_LEN)
        {
          LOG_MSG_ERROR_2("Modem build ID too long: len %d max %d",
                          img_list_entry_ptr->build_id_len,
                          (GOBI_IM_BUILD_ID_LEN-1));
          errval = QMI_ERR_ARG_TOO_LONG;
          goto send_result;
        }

        modem_unique_id_ptr = img_list_entry_ptr->unique_id;
        memscpy(modem_build_id, 
                img_list_entry_ptr->build_id_len, 
                img_list_entry_ptr->build_id, 
                img_list_entry_ptr->build_id_len);
        modem_build_id[img_list_entry_ptr->build_id_len] = '\0';
        got_modem_info = TRUE;
        break;

      case DMSI_IMG_TYPE_PRI:
        if (TRUE == got_pri_info)
        {
          LOG_MSG_ERROR_0("Duplicate PRI entry in list");
          errval = QMI_ERR_INVALID_ARG;
          goto send_result;
        }
        if (img_list_entry_ptr->build_id_len >= GOBI_IM_BUILD_ID_LEN)
        {
          LOG_MSG_ERROR_2("PRI build ID too long: len %d max %d",
                          img_list_entry_ptr->build_id_len,
                          (GOBI_IM_BUILD_ID_LEN-1));
          errval = QMI_ERR_ARG_TOO_LONG;
          goto send_result;
        }

        pri_unique_id_ptr = img_list_entry_ptr->unique_id;
        memscpy(pri_build_id, 
                img_list_entry_ptr->build_id_len, 
                img_list_entry_ptr->build_id, 
                img_list_entry_ptr->build_id_len);
        pri_build_id[img_list_entry_ptr->build_id_len] = '\0';
        got_pri_info = TRUE;
        break;

      default:
        LOG_MSG_ERROR_1("Invalid image type %d",
                        img_list_entry_ptr->image_type);
        errval = QMI_ERR_INVALID_OPERATION;
        goto send_result;
    }

    list_offset += (sizeof(struct img_list_entry) - GOBI_IM_BUILD_ID_LEN) 
                     + img_list_entry_ptr->build_id_len;
  } while (list_offset < list_tlv_len);
  
  if (!got_modem_info || !got_pri_info)
  {
    LOG_MSG_ERROR_2("Missing arg, got_modem_info = %d got_pri_info = %d",
                    got_modem_info, got_pri_info);
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  /*-------------------------------------------------------------------------
    Parse optional TLVs: storage index and download override
  -------------------------------------------------------------------------*/
  if (got_storage_index_tlv)
  {
    if (v_in2.modem_storage_index == GOBI_IM_AMSS_PARTI_INDEX_DEFAULT)
    {
      errval = QMI_ERR_ACCESS_DENIED;
      goto send_result;
    }
    if (!GOBI_IM_VALID_CUSTOM_PARTI_INDEX(v_in2.modem_storage_index))
    {
      errval = QMI_ERR_INVALID_INDEX;
      goto send_result;
    }
  }
  else
  {
    v_in2.modem_storage_index = GOBI_IM_AMSS_PARTI_INDEX_ANY;
  }

  /* Force to be either TRUE or FALSE */
  if (got_override_tlv && v_in1.download_override != FALSE)
  {
    v_in1.download_override = TRUE;
  }

  /*-------------------------------------------------------------------------
    Execute the operation
  -------------------------------------------------------------------------*/
  if (!gobi_im_set_next_image(modem_build_id,
                              modem_unique_id_ptr,
                              pri_build_id,
                              pri_unique_id_ptr,
                              v_in2.modem_storage_index,
                              v_in1.download_override,
                              &new_boot_cfg))
  {
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }
  
  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  v_out_req.num_entries = 0;
  LOG_MSG_INFO2_1("New boot config is 0x%x", new_boot_cfg);
  if (new_boot_cfg & GOBI_BOOT_CONFIG_DOWNLOAD_AMSS)
  {
    v_out_req.img_list[v_out_req.num_entries++] = DMSI_IMG_TYPE_MODEM;
  }
  if (new_boot_cfg & GOBI_BOOT_CONFIG_DOWNLOAD_UQCN)
  {
    v_out_req.img_list[v_out_req.num_entries++] = DMSI_IMG_TYPE_PRI;
  }

  if(FALSE == qmi_svc_put_param_tlv(&response, 
                                    QMI_TYPE_REQUIRED_PARAMETERS,
                                    (sizeof(uint8) + v_out_req.num_entries),
                                    &v_out_req))
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
  }

send_result:
  if (errval == QMI_ERR_ARG_TOO_LONG)
  {
    /* Max supported length is BUILD_ID_LEN less 1 for null terminator */
    v_out1.max_build_id_len = GOBI_IM_BUILD_ID_LEN - 1;
    if(FALSE == qmi_svc_put_param_tlv(&response, 
                                      DMSI_PRM_TYPE_MAX_BUILD_ID_LEN,
                                      sizeof(v_out1),
                                      &v_out1))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  
  return response;

} /* qmi_dmsi_set_firmware_pref() */

/*===========================================================================
  FUNCTION QMI_DMSI_LIST_STORED_IMAGES()

  DESCRIPTION
    Gives a list of all firmware images stored on the device plus some
    additional information about the storage.
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_list_stored_images
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;

  PACKED struct PACKED_POST img_list_hdr {
    uint8 image_type;
    uint8 max_images;
    uint8 running_image;
    uint8 sublist_len;
  } *list_hdr_ptr;

  PACKED struct PACKED_POST img_sublist_entry {
    uint8 storage_index;
    uint8 failure_count;
    uint8 unique_id[GOBI_IM_UNIQUE_ID_LEN];
    uint8 build_id_len;
    char  build_id[GOBI_IM_BUILD_ID_LEN];
  } *img_sublist_entry_ptr;

  PACKED struct PACKED_POST {
    uint8 num_types;
    uint8 img_list[sizeof(struct img_list_hdr) + 
                   (sizeof(struct img_sublist_entry) * 
                    (GOBI_IM_NUM_AMSS_PARTITIONS + GOBI_IM_MAX_NUM_UQCN_FILES))];
  } v_required;

  uint32 list_offset;
  uint32 i;
  uint8  list_index;

  gobi_im_table_s_type            modem_table;
  gobi_im_uqcn_iterator_ptr_type  uqcn_iter;

  char  pri_build_id[GOBI_IM_BUILD_ID_LEN];
  uint8 pri_unique_id[GOBI_IM_UNIQUE_ID_LEN];

  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);
  
  response = NULL;
  errval = QMI_ERR_NONE;

  v_required.num_types = DMSI_NUM_SUPPORTED_IMAGE_TYPES;

  /*-------------------------------------------------------------------------
    Format the AMSS image list portion of the response
  -------------------------------------------------------------------------*/
  list_hdr_ptr = (struct img_list_hdr *) v_required.img_list;

  list_hdr_ptr->image_type = DMSI_IMG_TYPE_MODEM;
  list_hdr_ptr->max_images = GOBI_IM_NUM_AMSS_PARTITIONS;
  list_hdr_ptr->sublist_len = 0;
  list_hdr_ptr->running_image = DMSI_RUNNING_IMAGE_INDEX_UNKNOWN;

  list_offset = sizeof(struct img_list_hdr);
  list_index = 0;
  gobi_im_get_table(&modem_table);

  #ifdef FEATURE_QMI_GOBI_IMG_TBL_DEBUG
  #error code not present
#endif /* FEATURE_QMI_GOBI_IMG_TBL_DEBUG */

  for (i = 0; i < GOBI_IM_NUM_AMSS_PARTITIONS; i++)
  {
    img_sublist_entry_ptr = (struct img_sublist_entry *) &v_required.img_list[list_offset];

    if (modem_table.entries[i].status == GOBI_PARTITION_STATUS_GOOD)
    {
      /* Copy in all data for a good image */
      img_sublist_entry_ptr->storage_index = (uint8) i;
      img_sublist_entry_ptr->failure_count = modem_table.entries[i].failure_count;
      img_sublist_entry_ptr->build_id_len = strlen(modem_table.entries[i].build_id);

      memscpy(img_sublist_entry_ptr->unique_id,
              GOBI_IM_UNIQUE_ID_LEN,
             modem_table.entries[i].unique_id,
             GOBI_IM_UNIQUE_ID_LEN);
      
      strlcpy(img_sublist_entry_ptr->build_id,
                  modem_table.entries[i].build_id,
                  GOBI_IM_BUILD_ID_LEN);

      /* Check if this is the active image */
      if (i == gobi_im_get_running_amss_parti_index())
      {
        list_hdr_ptr->running_image = list_index;
      }

      list_offset += (sizeof(struct img_sublist_entry) - GOBI_IM_BUILD_ID_LEN) 
                       + img_sublist_entry_ptr->build_id_len;
      list_hdr_ptr->sublist_len++;
      list_index++;
    }
    else if (modem_table.entries[i].failure_count > 0)
    {
      /* Copy in storage index & failure count only - leave rest blank */
      img_sublist_entry_ptr->storage_index = (uint8) i;
      img_sublist_entry_ptr->failure_count = modem_table.entries[i].failure_count;
      img_sublist_entry_ptr->build_id_len = 0;
      memset(img_sublist_entry_ptr->unique_id, 0, GOBI_IM_UNIQUE_ID_LEN);

      list_offset += sizeof(struct img_sublist_entry) - GOBI_IM_BUILD_ID_LEN;
      list_hdr_ptr->sublist_len++;
      list_index++;
    }

    #ifdef FEATURE_QMI_GOBI_IMG_TBL_DEBUG
    #error code not present
#endif /* FEATURE_QMI_GOBI_IMG_TBL_DEBUG */
  }

  #ifdef FEATURE_QMI_GOBI_IMG_TBL_DEBUG
  #error code not present
#endif /* FEATURE_QMI_GOBI_IMG_TBL_DEBUG */

  /*-------------------------------------------------------------------------
    Format the UQCN image list portion of the response
  -------------------------------------------------------------------------*/
  list_hdr_ptr = (struct img_list_hdr *) &v_required.img_list[list_offset];

  list_hdr_ptr->image_type = DMSI_IMG_TYPE_PRI;
  list_hdr_ptr->max_images = GOBI_IM_MAX_NUM_UQCN_FILES;
  list_hdr_ptr->sublist_len = 0;
  list_hdr_ptr->running_image = DMSI_RUNNING_IMAGE_INDEX_UNKNOWN;
  list_offset += sizeof(struct img_list_hdr);

  list_index = 0;
  uqcn_iter = gobi_im_get_uqcn_iterator();
  if (uqcn_iter != NULL)
  {
    #ifdef FEATURE_QMI_GOBI_IMG_TBL_DEBUG
    #error code not present
#endif /* FEATURE_QMI_GOBI_IMG_TBL_DEBUG */
    while (FALSE != gobi_im_get_next_uqcn(uqcn_iter, pri_build_id, pri_unique_id))
    {
      img_sublist_entry_ptr = (struct img_sublist_entry *) &v_required.img_list[list_offset];

      img_sublist_entry_ptr->storage_index = DMSI_STORAGE_INDEX_NOT_USED;
      img_sublist_entry_ptr->failure_count = DMSI_FAILURE_COUNT_NOT_USED;
      img_sublist_entry_ptr->build_id_len = strlen(pri_build_id);
      memscpy(img_sublist_entry_ptr->unique_id, 
              GOBI_IM_UNIQUE_ID_LEN, 
              pri_unique_id, 
              GOBI_IM_UNIQUE_ID_LEN);
      strlcpy(img_sublist_entry_ptr->build_id, pri_build_id, GOBI_IM_BUILD_ID_LEN);

      if (gobi_im_uqcn_is_loaded(pri_build_id, pri_unique_id))
      {
        list_hdr_ptr->running_image = list_index;
      }

      #ifdef FEATURE_QMI_GOBI_IMG_TBL_DEBUG
      #error code not present
#endif /* FEATURE_QMI_GOBI_IMG_TBL_DEBUG */

      list_offset += (sizeof(struct img_sublist_entry) - GOBI_IM_BUILD_ID_LEN) 
                       + img_sublist_entry_ptr->build_id_len;
      list_hdr_ptr->sublist_len++;
      list_index++;
    }

    #ifdef FEATURE_QMI_GOBI_IMG_TBL_DEBUG
    #error code not present
#endif /* FEATURE_QMI_GOBI_IMG_TBL_DEBUG */

    gobi_im_release_uqcn_iterator(uqcn_iter);
  }
  else
  {
    LOG_MSG_ERROR_0("Couldn't allocate EFS directory iterator!");
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }
  
  if(FALSE == qmi_svc_put_param_tlv(&response, 
                                    QMI_TYPE_REQUIRED_PARAMETERS,
                                    (sizeof(uint8) + list_offset),
                                    &v_required))
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
  }

send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);
  retval = qmi_svc_put_result_tlv( &response, result, errval );
  CHECK_RETVAL();
  
  return response;

} /* qmi_dmsi_list_stored_images() */


/*===========================================================================
  FUNCTION QMI_DMSI_DELETE_STORED_IMAGE()

  DESCRIPTION
    Removes an image from device storage.
        
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_delete_stored_image
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *               response;

  PACKED struct PACKED_POST
  {
    uint8 image_type;
    uint8 unique_id[GOBI_IM_UNIQUE_ID_LEN];
    uint8 build_id_len;
    char  build_id[GOBI_IM_BUILD_ID_LEN];
  } v_in_req;

  boolean  got_v_in_req = FALSE;

  boolean             gobi_im_ret;
  gobi_im_err_e_type  gobi_im_err;

  uint8    type;
  uint16   len;
  uint16   expected_len;
  void *   value;

  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  gobi_im_ret = TRUE;

  response = NULL;
  errval = QMI_ERR_NONE;

  while (*sdu_in)
  {
    if( !qmi_svc_get_tl( sdu_in, &type, &len ) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        if (len <= sizeof(v_in_req))
        {
          got_v_in_req = TRUE;
          value = &v_in_req;
        }
        else
        {
          errval = QMI_ERR_ARG_TOO_LONG;
          goto send_result;
        }
        break;

      default:
        LOG_MSG_INFO2_1 ("Unrecognized TLV type (%d)", type);
        break;
    }

    if(!qmi_svc_validate_type_length( type,
                                      len,
                                      expected_len,
                                      FILE_ID_DS_QMI_DMS,__LINE__) )
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    /*-----------------------------------------------------------------------
      If type is unrecognized, value will be NULL, and qmi_svc_dsm_pullup will 
      free the unrecognized value, i.e. we skip past unrecognized TLVs
    -----------------------------------------------------------------------*/
    if(len != qmi_svc_dsm_pullup( sdu_in, value, len ,FILE_ID_DS_QMI_DMS,__LINE__  ))
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  /*-------------------------------------------------------------------------
    Error checking
  -------------------------------------------------------------------------*/
  if (!got_v_in_req)
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  if (v_in_req.build_id_len >= GOBI_IM_BUILD_ID_LEN)
  {
    LOG_MSG_ERROR_2("Build ID len %d too long (max %d)",
                    v_in_req.build_id_len, (GOBI_IM_BUILD_ID_LEN-1));
    errval = QMI_ERR_INVALID_ARG;
    goto send_result;
  }

  /*-------------------------------------------------------------------------
    Execute the operation
  -------------------------------------------------------------------------*/
  v_in_req.build_id[v_in_req.build_id_len] = '\0';
  if (v_in_req.image_type == DMSI_IMG_TYPE_MODEM)
  {
    gobi_im_ret = gobi_im_delete_amss_entry(v_in_req.build_id,
                                            v_in_req.unique_id,
                                            &gobi_im_err);
  }
  else if (v_in_req.image_type == DMSI_IMG_TYPE_PRI)
  {
    gobi_im_ret = gobi_im_delete_uqcn(v_in_req.build_id,
                                      v_in_req.unique_id,
                                      &gobi_im_err);
  }
  else
  {
    errval = QMI_ERR_INVALID_OPERATION;
    goto send_result;
  }

  if (errval == QMI_ERR_NONE && !gobi_im_ret)
  {
    switch (gobi_im_err)
    {
      case GOBI_IM_ERR_NOT_FOUND:
        errval = QMI_ERR_NO_EFFECT;
        break;

      case GOBI_IM_ERR_READ_ONLY:
        errval = QMI_ERR_ACCESS_DENIED;
        break;

      case GOBI_IM_ERR_EFS:
      case GOBI_IM_ERR_NULL_PTR:
      default:
        LOG_MSG_ERROR_1("Mapped Gobi IM err %d to internal error",
                        gobi_im_err);
        errval = QMI_ERR_INTERNAL;
    }
  }

send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  
  return response;

} /* qmi_dmsi_delete_stored_image() */


/*===========================================================================
  FUNCTION QMI_DMSI_GET_STORED_IMAGE_INFO()

  DESCRIPTION
    Retrieves extended information about an image stored in flash. This 
    information is read from the trailer record. Currently only supports 
    PRI image.
        
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_get_stored_image_info
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *               response;

  PACKED struct PACKED_POST
  {
    uint8 image_type;
    uint8 unique_id[GOBI_IM_UNIQUE_ID_LEN];
    uint8 build_id_len;
    char  build_id[GOBI_IM_BUILD_ID_LEN];
  } v_in_req;

  PACKED struct PACKED_POST
  {
    uint16 maj_ver;
    uint16 min_ver;
  } v_out_boot_ver;

  struct
  {
    uint32 pri_ver_no;
    char   pri_ver_info[DMSI_PRI_VER_INFO_LEN];
  } v_out_pri_ver;

  PACKED struct PACKED_POST
  {
    uint32 oem_lock_id;
  } v_out_oem_lock_id;

  boolean  got_v_in_req = FALSE;

  boolean             gobi_im_ret;
  gobi_im_err_e_type  gobi_im_err;

  uint8    trailer_rec_buf[GOBI_IM_MAX_TRAILER_REC_LEN];

  uint8    type;
  uint16   len;
  uint16   expected_len;
  void *   value;

  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  gobi_im_ret = FALSE;

  response = NULL;
  errval = QMI_ERR_NONE;

  while (*sdu_in)
  {
    if( !qmi_svc_get_tl( sdu_in, &type, &len ) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        if (len <= sizeof(v_in_req))
        {
          got_v_in_req = TRUE;
          value = &v_in_req;
        }
        else
        {
          errval = QMI_ERR_ARG_TOO_LONG;
          goto send_result;
        }
        break;

      default:
        LOG_MSG_INFO2_1 ("Unrecognized TLV type (%d)", type);
        break;
    }

    if(!qmi_svc_validate_type_length( type,
                                      len,
                                      expected_len,
                                      FILE_ID_DS_QMI_DMS,__LINE__) )
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    /*-----------------------------------------------------------------------
      If type is unrecognized, value will be NULL, and qmi_svc_dsm_pullup will 
      free the unrecognized value, i.e. we skip past unrecognized TLVs
    -----------------------------------------------------------------------*/
    if(len != qmi_svc_dsm_pullup( sdu_in, value, len ,FILE_ID_DS_QMI_DMS,__LINE__  ))
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  /*-------------------------------------------------------------------------
    Error checking
  -------------------------------------------------------------------------*/
  if (!got_v_in_req)
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  if (v_in_req.build_id_len >= GOBI_IM_BUILD_ID_LEN)
  {
    LOG_MSG_ERROR_2("Build ID len %d too long (max %d)",
                    v_in_req.build_id_len, (GOBI_IM_BUILD_ID_LEN-1));
    errval = QMI_ERR_INVALID_ARG;
    goto send_result;
  }
  else
  {
    /* Add null termination */
    v_in_req.build_id[v_in_req.build_id_len] = '\0';
  }

  if (v_in_req.image_type == DMSI_IMG_TYPE_MODEM)
  {
    /* Reading modem trailer record not supported */
    errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
    goto send_result;
  }
  else if (v_in_req.image_type != DMSI_IMG_TYPE_PRI)
  {
    /* Input not PRI or MODEM --> unsupported image type */
    errval = QMI_ERR_INVALID_OPERATION;
    goto send_result;
  }

  /*-------------------------------------------------------------------------
    Extract the trailer record 
  -------------------------------------------------------------------------*/

  gobi_im_ret = gobi_im_extract_trailer_rec_uqcn(v_in_req.build_id,
                                                 v_in_req.unique_id,
                                                 trailer_rec_buf,
                                                 sizeof(trailer_rec_buf),
                                                 &gobi_im_err);
  if (!gobi_im_ret)
  {
    LOG_MSG_ERROR_1("extract trailer rec failed with gobi_im_err %d",
                    gobi_im_err);
    switch(gobi_im_err)
    {
      case GOBI_IM_ERR_NOT_FOUND:
        errval = QMI_ERR_NO_ENTRY;
        break;
      default:
        errval = QMI_ERR_INTERNAL;
    }
    goto send_result;
  }

  /*-------------------------------------------------------------------------
    Parse out the trailer record entries into response TLVs, back to front
  -------------------------------------------------------------------------*/

  /* OEM Lock ID */
  if (gobi_im_parse_trailer_record(trailer_rec_buf,
                                   sizeof(trailer_rec_buf),
                                   TRAILER_SIGNATURE_NOTEBOOK_LOCK_KEY,
                                   &v_out_oem_lock_id,
                                   sizeof(v_out_oem_lock_id)))
  {
    if (FALSE == qmi_svc_put_param_tlv(&response, 
                                      DMSI_PRM_TYPE_OEM_LOCK_ID,
                                      sizeof(v_out_oem_lock_id),
                                      &v_out_oem_lock_id))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
      goto send_result;
    }
  }

  /* PRI Version */
  if (gobi_im_parse_trailer_record(trailer_rec_buf,
                                   sizeof(trailer_rec_buf),
                                   TRAILER_SIGNATURE_UQCN_INFO,
                                   &v_out_pri_ver,
                                   sizeof(v_out_pri_ver)))
  {
    if (FALSE == qmi_svc_put_param_tlv(&response, 
                                      DMSI_PRM_TYPE_PRI_VER,
                                      sizeof(v_out_pri_ver),
                                      &v_out_pri_ver))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
      goto send_result;
    }
  }

  /* Boot version */
  if (gobi_im_parse_trailer_record(trailer_rec_buf,
                                   sizeof(trailer_rec_buf),
                                   TRAILER_SIGNATURE_BOOT_FLASH_VERSION,
                                   &v_out_boot_ver,
                                   sizeof(v_out_boot_ver)))
  {
    if (FALSE == qmi_svc_put_param_tlv(&response, 
                                      DMSI_PRM_TYPE_BOOT_VER,
                                      sizeof(v_out_boot_ver),
                                      &v_out_boot_ver))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
      goto send_result;
    }
  }

send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  
  return response;

} /* qmi_dmsi_get_stored_image_info() */
#endif /* FEATURE_DATA_QMI_ADDENDUM */


/*===========================================================================
  FUNCTION QMI_DMSI_GET_ALT_NET_CONFIG()

  DESCRIPTION
    Gives the current setting of hsu alternate NET config of the device
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_get_alt_net_config
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *        response;
  qmi_error_e_type       errval;
  qmi_result_e_type      result;
  boolean                retval;

#ifdef FEATURE_DATA_QMI_ADDENDUM
  gobi_nv_status_e_type  nv_status;
  uint8 alt_net_config;
#endif /* FEATURE_DATA_QMI_ADDENDUM */
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  response = NULL;
  errval = QMI_ERR_NONE;

#ifndef FEATURE_DATA_QMI_ADDENDUM
  errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
#else /* FEATURE_DATA_QMI_ADDENDUM */

  alt_net_config = DMSI_ALT_NET_CONFIG_DISABLED;

  /*-------------------------------------------------------------------------
    Perform query action
  -------------------------------------------------------------------------*/
  LOG_MSG_INFO1_0("dms query current hsu alternate net config");

  nv_status = gobi_nv_read (GOBI_NV_ITEM_HSU_ALT_NET_CONFIG, 0, 
                            &alt_net_config, sizeof(uint8));
  switch (nv_status)
  {
    case NV_DONE_S:
      if( (alt_net_config != DMSI_ALT_NET_CONFIG_DISABLED) &&
          (alt_net_config != DMSI_ALT_NET_CONFIG_ENABLED))
      {
        errval = QMI_ERR_INVALID_ARG;
        LOG_MSG_ERROR_1("Invalid alt_net_config value %d", alt_net_config);
        goto send_result;
      }
      break;

    case NV_NOTACTIVE_S:
      errval = QMI_ERR_NOT_PROVISIONED;
      goto send_result;
      /* no need for break after goto */

    default:
      LOG_MSG_ERROR_0("Could not read HSU ALT NET config NV");
      errval = QMI_ERR_INTERNAL;
      goto send_result;
      /* no need for break after goto */
  }

  if(FALSE == qmi_svc_put_param_tlv(&response,
                                    QMI_TYPE_REQUIRED_PARAMETERS,
                                    sizeof(alt_net_config),
                                    &alt_net_config))
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
  }

send_result:
#endif /* FEATURE_DATA_QMI_ADDENDUM */
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  
  return response;
 
} /* qmi_dmsi_get_alt_net_config() */


/*===========================================================================
  FUNCTION QMI_DMSI_SET_ALT_NET_CONFIG()

  DESCRIPTION
    Sets the hsu alternate NET config of the device
        
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_set_alt_net_config
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *        response;
  qmi_error_e_type       errval;
  qmi_result_e_type      result;
  boolean                retval;

#ifdef FEATURE_DATA_QMI_ADDENDUM
  gobi_nv_status_e_type  nv_status;

  PACKED struct PACKED_POST
  {
    uint8  alternate_net_config;
  } v_in_req;

  boolean  got_v_in_req;
  uint8    type;
  uint16   len;
  void *   value;
  uint16   expected_len;

  uint8 alt_net_config;
  uint8 cur_alt_net_config;
#endif /* FEATURE_DATA_QMI_ADDENDUM */

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  response = NULL;
  errval = QMI_ERR_NONE;

#ifndef FEATURE_DATA_QMI_ADDENDUM
  errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
#else /* FEATURE_DATA_QMI_ADDENDUM */

  got_v_in_req = FALSE;
  alt_net_config     = DMSI_ALT_NET_CONFIG_DISABLED;
  cur_alt_net_config = DMSI_ALT_NET_CONFIG_DISABLED;

  LOG_MSG_INFO1_0("dms set alternate NET config");

  /*
   * it is safe to clear this struct here, for no value is expected.
   * this also satisfies lint warnings.
   */
  memset((void *)&v_in_req, 0, sizeof(v_in_req));
  
  while (*sdu_in)
  {
    if( !qmi_svc_get_tl( sdu_in, &type, &len ) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        expected_len = sizeof(v_in_req);
        got_v_in_req = TRUE;
        value = (void *) &v_in_req;
        break;

     default:
        LOG_MSG_INFO2_1 ("Unrecognized TLV type (%d)", type);
        break;
    }

    if(!qmi_svc_validate_type_length( type,
                                      len,
                                      expected_len,
                                      FILE_ID_DS_QMI_DMS,__LINE__) )
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    /*-----------------------------------------------------------------------
      If type is unrecognized, value will be NULL, and qmi_svc_dsm_pullup will 
      free the unrecognized value, i.e. we skip past unrecognized TLVs
    -----------------------------------------------------------------------*/
    if(len != qmi_svc_dsm_pullup( sdu_in, value, len ,FILE_ID_DS_QMI_DMS,__LINE__  ))
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  if(!got_v_in_req)
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  /*-------------------------------------------------------------------------
    Populate the local structure with the values coming in the request and
    also validate the values
  -------------------------------------------------------------------------*/
  switch(v_in_req.alternate_net_config)
  {
    case DMSI_ALT_NET_CONFIG_DISABLED:
    case DMSI_ALT_NET_CONFIG_ENABLED:
      alt_net_config = v_in_req.alternate_net_config;
      break;

    default:
      /* Any other value is considered invalid */
      LOG_MSG_INFO2_1 ("Invalid value setting = %d",
                       v_in_req.alternate_net_config);
      errval = QMI_ERR_INVALID_ARG;
      goto send_result;
      /* no need for break after goto */
  }

  /*-------------------------------------------------------------------------
    Requests to set the value to the current setting will
    elicit a QMI_ERR_NO_EFFECT error.
  -------------------------------------------------------------------------*/
  nv_status = gobi_nv_read (GOBI_NV_ITEM_HSU_ALT_NET_CONFIG, 0, 
                            &cur_alt_net_config, sizeof(uint8));
  switch (nv_status)
  {
    case NV_DONE_S:
      if(v_in_req.alternate_net_config == cur_alt_net_config)
      {
        LOG_MSG_INFO2_1 ("Ignore to set the same value in current setting = %d",
                         v_in_req.alternate_net_config);
        errval = QMI_ERR_NO_EFFECT;
        goto send_result;
      }
      break;

    /* all other cases let fall through to try the NV write */
    default:
      break;
  }

  /*-------------------------------------------------------------------------
                  Save the setting of alternate net config into NV
   -------------------------------------------------------------------------*/
  nv_status = gobi_nv_write (GOBI_NV_ITEM_HSU_ALT_NET_CONFIG, 0, 
                             &alt_net_config, sizeof(uint8));
  switch (nv_status)
  {
    case NV_DONE_S:
      break;

    default:
      errval = QMI_ERR_INTERNAL;
      LOG_MSG_ERROR_0("Could not write HSU ALT NET config NV");
      goto send_result;
      /* no need for break after goto */
  }

send_result:
#endif /* FEATURE_DATA_QMI_ADDENDUM */
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;

} /* qmi_dmsi_set_alt_net_config() */

#ifdef FEATURE_DATA_QMI_ADDENDUM

/*===========================================================================
  FUNCTION QMI_DMSI_GET_BOOT_IMAGE_DOWNLOAD_MODE()

  DESCRIPTION
    Retrieves the current boot image download mode configuration
        
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_get_boot_image_download_mode
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type * response;

  PACKED struct PACKED_POST {
    uint8 download_mode;
  } v_out_req;

  boolean             gobi_im_ret;
  gobi_im_image_download_mode_e_type gobi_im_mode;

  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  memset(&v_out_req, 0, sizeof(v_out_req));
  gobi_im_ret = FALSE;

  response = NULL;
  errval = QMI_ERR_NONE;

  /*-------------------------------------------------------------------------
    Retrieve the current image download mode from Gobi IM
  -------------------------------------------------------------------------*/
  gobi_im_ret = gobi_im_get_image_download_mode(&gobi_im_mode, NULL);
  if (!gobi_im_ret)
  {
    LOG_MSG_ERROR_0("Couldn't get current boot img dload mode");
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }
  
  v_out_req.download_mode = (uint8) gobi_im_mode;
  if (FALSE == qmi_svc_put_param_tlv(&response, 
                                     DMSI_PRM_TYPE_DOWNLOAD_MODE,
                                     sizeof(v_out_req),
                                     &v_out_req))
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
  }

send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  
  return response;

} /* qmi_dmsi_get_boot_image_download_mode() */


/*===========================================================================
  FUNCTION QMI_DMSI_SET_BOOT_IMAGE_DOWNLOAD_MODE()

  DESCRIPTION
    Requests the device to enter a specific boot image download mode on next 
    boot.
        
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_set_boot_image_download_mode
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type * response;

  PACKED struct PACKED_POST
  {
    uint8 download_mode;
  } v_in_req;

  boolean  got_v_in_req = FALSE;

  boolean             gobi_im_ret;
  gobi_im_err_e_type  gobi_im_err;
  gobi_im_image_download_mode_e_type current_mode;
  gobi_im_image_download_mode_e_type requested_mode;

  uint8    type;
  uint16   len;
  uint16   expected_len;
  void *   value;

  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  gobi_im_ret = FALSE;

  response = NULL;
  errval = QMI_ERR_NONE;

  while (*sdu_in)
  {
    if( !qmi_svc_get_tl( sdu_in, &type, &len ) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        expected_len = sizeof(v_in_req);
        got_v_in_req = TRUE;
        value = (void *) &v_in_req;
        break;

      default:
        LOG_MSG_INFO2_1 ("Unrecognized TLV type (%d)", type);
        break;
    }

    if(!qmi_svc_validate_type_length( type,
                                      len,
                                      expected_len,
                                      FILE_ID_DS_QMI_DMS,__LINE__) )
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    /*-----------------------------------------------------------------------
      If type is unrecognized, value will be NULL, and qmi_svc_dsm_pullup will 
      free the unrecognized value, i.e. we skip past unrecognized TLVs
    -----------------------------------------------------------------------*/
    if(len != qmi_svc_dsm_pullup( sdu_in, value, len ,FILE_ID_DS_QMI_DMS,__LINE__  ))
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  /*-------------------------------------------------------------------------
    Error checking
  -------------------------------------------------------------------------*/
  if (!got_v_in_req)
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  /* See if we are already configured for the requested mode */
  requested_mode = (gobi_im_image_download_mode_e_type) v_in_req.download_mode;
  gobi_im_ret = gobi_im_get_image_download_mode(&current_mode, &gobi_im_err);
  if (!gobi_im_ret)
  {
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }
  else if (requested_mode == current_mode)
  {
    errval = QMI_ERR_NO_EFFECT;
    goto send_result;
  }

  /*-------------------------------------------------------------------------
    Request Gobi IM to set the new mode
  -------------------------------------------------------------------------*/
  gobi_im_ret = gobi_im_set_image_download_mode(requested_mode, &gobi_im_err);
  if (!gobi_im_ret)
  {
    LOG_MSG_ERROR_3("Error %d while changing boot img dload mode from %d to %d",
                    gobi_im_err, current_mode, requested_mode);
    switch (gobi_im_err)
    {
      case GOBI_IM_ERR_INVALID_OP:
        errval = QMI_ERR_INVALID_OPERATION;
        break;

      default:
        errval = QMI_ERR_INTERNAL;
    }
    goto send_result;
  }

send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  
  return response;

} /* qmi_dmsi_set_boot_image_download_mode() */
#endif /* FEATURE_DATA_QMI_ADDENDUM */

#ifdef FEATURE_MMGSDI_SESSION_LIB
/*===========================================================================
  FUNCTION QMI_DMSI_GENERATE_PIN_OPERATION_RESP()

  DESCRIPTION
    To send a response to the QMI pin operation requested by the client.
    
  PARAMETERS
    pin_op_cnf : info regarding the pin operation

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_dmsi_generate_pin_operation_resp
(
  mmgsdi_pin_operation_cnf_type * pin_op_cnf
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response;
  qmi_error_e_type        errval;
  qmi_result_e_type       result;
  boolean                 retval;
  qmi_dmsi_cmd_val_e_type cmd_val;

  PACKED struct PACKED_POST
  {
    uint8 num_retries_left;
    uint8 num_unblock_retries_left;
  } v_out;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  switch(pin_op_cnf->pin_op)
  {
    case MMGSDI_PIN_OP_VERIFY:
      cmd_val = DMSI_CMD_VAL_VERIFY_UIM_PIN;
      break;

    case MMGSDI_PIN_OP_DISABLE:
      cmd_val = DMSI_CMD_VAL_SET_UIM_PIN_PROTECTION;
      break;

    case MMGSDI_PIN_OP_ENABLE:
      cmd_val = DMSI_CMD_VAL_SET_UIM_PIN_PROTECTION;    
      break;

    case MMGSDI_PIN_OP_CHANGE:
      cmd_val = DMSI_CMD_VAL_CHANGE_UIM_PIN;
      break;

    case MMGSDI_PIN_OP_UNBLOCK:
      cmd_val = DMSI_CMD_VAL_UNBLOCK_UIM_PIN;
      break;

    default:
      LOG_MSG_INFO2_1("Unhandled Pin_operation_cnf %d received",
                      pin_op_cnf->pin_op);
      return;
  }

  cmd_buf_p = (qmi_cmd_buf_type *) pin_op_cnf->response_header.client_data;
  
  if(cmd_buf_p == NULL)
  {
    LOG_MSG_INFO1_0("Null cmd_buf ptr in process_pin_operation_complete");
    return;
  }

  LOG_MSG_INFO2_1("Received cmd_buf_ptr (user_data) %x from mggsdi pin op resp",
                  cmd_buf_p);

  if( NULL == cmd_buf_p->x_p || NULL == cmd_buf_p->x_p->cl_sp )
  {
    LOG_MSG_INFO1_0("Transaction ptr or client state pointer is NULL, most likely client was released.");
    return;
  }

  LOG_MSG_INFO2_1("Sending response for pin operation %d", pin_op_cnf->pin_op);
  
  v_out.num_retries_left = (uint8)pin_op_cnf->pin_info.num_retries;
  v_out.num_unblock_retries_left = (uint8)pin_op_cnf->pin_info.num_unblock_retries;

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  response = NULL;
  errval = QMI_ERR_NONE;

  /* Mapping the pin requested state(pin_op_cnf->pin_op) against the received
  state(pin_op_cnf->pin_info.status). if both are same and mmgsdi_status is 
  MMGSDI_ACCESS_DENIED then return QMI_ERR_NO_EFFECT */
  if(pin_op_cnf->response_header.mmgsdi_status == MMGSDI_ACCESS_DENIED ) 
  {
    errval = qmi_if_map_mmgsdi_status_to_errval(
                  pin_op_cnf->response_header.mmgsdi_status,__LINE__);
    LOG_MSG_INFO2_2("PIN Status is %d and PIN requested state is %d", 
                     pin_op_cnf->pin_info.status, pin_op_cnf->pin_op);
    if((pin_op_cnf->pin_info.status == MMGSDI_PIN_ENABLED_NOT_VERIFIED ||
        pin_op_cnf->pin_info.status == MMGSDI_PIN_ENABLED_VERIFIED) &&
        pin_op_cnf->pin_op == MMGSDI_PIN_OP_ENABLE )
    {
      errval = QMI_ERR_NO_EFFECT;
    }

    /* if PIN is disabled and try to verify or change the pin then return
       QMI_ERR_NO_EFFECT */
    if( pin_op_cnf->pin_info.status == MMGSDI_PIN_DISABLED &&
        (pin_op_cnf->pin_op == MMGSDI_PIN_OP_DISABLE ||
        pin_op_cnf->pin_op == MMGSDI_PIN_OP_VERIFY ||
        pin_op_cnf->pin_op == MMGSDI_PIN_OP_CHANGE ))
    {
      errval = QMI_ERR_NO_EFFECT;
    }
  }
  else
  {
    if(pin_op_cnf->response_header.mmgsdi_status != MMGSDI_SUCCESS) 
    {
      errval = qmi_if_map_mmgsdi_status_to_errval(
                pin_op_cnf->response_header.mmgsdi_status,__LINE__);

      /* Take care of case where PIN is permanently blocked */
      if (errval == QMI_ERR_PIN_BLOCKED &&
          pin_op_cnf->pin_info.num_retries == 0 &&
          pin_op_cnf->pin_info.num_unblock_retries == 0)
      {
        LOG_MSG_INFO1_1("Force error code to permanent blocked (%d)",
                        pin_op_cnf->pin_info.status);
        errval = QMI_ERR_PIN_PERM_BLOCKED;
      }
    }
  }

  if(errval != QMI_ERR_NONE && errval != QMI_ERR_SIM_NOT_INITIALIZED)
  {
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      DMSI_PRM_TYPE_RETRIES_LEFT,
                                      sizeof(v_out),
                                      &v_out))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }
  
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  if(FALSE == retval)
  {
    dsm_free_packet(&response);
    ds_qmi_fw_free_cmd_buf( &cmd_buf_p );
    return;
  }

  if(FALSE == qmi_dmsi_send_response(cmd_buf_p, response))
  {
    LOG_MSG_INFO2_1 ("Unable to send response for QMI PIN operation: %d ",
                     cmd_val);
    /*-----------------------------------------------------------------------
      Note - no need to free ind, taken care of in make_message()
      or send()
    -----------------------------------------------------------------------*/
    dsm_free_packet(&response);
    ds_qmi_fw_free_cmd_buf( &cmd_buf_p );
  }
} /* qmi_dmsi_generate_pin_operation_resp */

/*===========================================================================
  FUNCTION QMI_DMSI_GENERATE_PIN_STATUS_RESP()

  DESCRIPTION
    To send a response to the QMI pin status requested by the client.
    
  PARAMETERS
    pin_op_cnf : info regarding the pin operation

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_dmsi_generate_pin_status_resp
(
  mmgsdi_get_all_pin_status_cnf_type * pin_status_cnf
)
{ 
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response;
  qmi_error_e_type        errval;
  qmi_result_e_type       result;
  boolean                 retval;
  
  PACKED struct PACKED_POST
  {
    uint8 status;
    uint8 num_retries_left;
    uint8 num_unblock_retries_left;
  } v_out_pin1;
  PACKED struct PACKED_POST
  {
    uint8 status;
    uint8 num_retries_left;
    uint8 num_unblock_retries_left;
  } v_out_pin2;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  cmd_buf_p = (qmi_cmd_buf_type *) 
                 pin_status_cnf->response_header.client_data;
  if(cmd_buf_p == NULL)
  {
    LOG_MSG_INFO1_0("Null cmd_buf ptr in process_pin_operation_complete");
    return;
  }

  LOG_MSG_INFO2_1("Received cmd_buf_ptr(user_data) %x from mggsdi pin status resp",
                  cmd_buf_p);

  if( NULL == cmd_buf_p->x_p || NULL == cmd_buf_p->x_p->cl_sp )
  {
    LOG_MSG_INFO1_0("Transaction ptr or client state pointer is NULL, most likely client was released.");
    return;
  }

  v_out_pin1.status = pin_status_cnf->pin1.status;
  v_out_pin1.num_retries_left = (uint8)pin_status_cnf->pin1.num_retries;
  v_out_pin1.num_unblock_retries_left = 
    (uint8)pin_status_cnf->pin1.num_unblock_retries;

  v_out_pin2.status = pin_status_cnf->pin2.status;
  v_out_pin2.num_retries_left = (uint8)pin_status_cnf->pin2.num_retries;
  v_out_pin2.num_unblock_retries_left = 
    (uint8)pin_status_cnf->pin2.num_unblock_retries;

  LOG_MSG_INFO2_0("Sending response for get pin status");

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  response = NULL;
  errval = QMI_ERR_NONE;
  
  if(pin_status_cnf->response_header.mmgsdi_status != MMGSDI_SUCCESS) 
  {
    errval = qmi_if_map_mmgsdi_status_to_errval(
               pin_status_cnf->response_header.mmgsdi_status,__LINE__);
    goto send_result;
  }

  if(FALSE == qmi_svc_put_param_tlv(&response,
                                    DMSI_PRM_TYPE_PIN1_STATUS,
                                    sizeof(v_out_pin1),
                                    &v_out_pin1))
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
    goto send_result;
  }
  if(FALSE == qmi_svc_put_param_tlv(&response,
                                    DMSI_PRM_TYPE_PIN2_STATUS,
                                    sizeof(v_out_pin2),
                                    &v_out_pin2))
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
    goto send_result;
  }

send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  if(FALSE == retval)
  {
    dsm_free_packet(&response);
    ds_qmi_fw_free_cmd_buf( &cmd_buf_p );
    return;
  } 
  
  if(FALSE == qmi_dmsi_send_response(cmd_buf_p, response))
  {
    LOG_MSG_INFO2_0 ("Unable to send response for QMI PIN status operation: %d ");
    /*-----------------------------------------------------------------------
      Note - no need to free ind, taken care of in make_message()
      or send()
    -----------------------------------------------------------------------*/
    dsm_free_packet(&response);
    ds_qmi_fw_free_cmd_buf( &cmd_buf_p );
  }
} /* qmi_dmsi_generate_pin_status_resp */

/*===========================================================================
  FUNCTION QMI_DMSI_GENERATE_READ_UIM_ICCID_RESP()

  DESCRIPTION
    To send a response to the QMI read ICCID requested by the client.
    
  PARAMETERS
    read_cnf : info regarding reading ICCID

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_dmsi_generate_read_uim_iccid_resp
(
  mmgsdi_read_cnf_type * read_cnf
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response;
  qmi_error_e_type        errval;
  qmi_result_e_type       result;
  boolean                 retval;
  char                    uim_id[DMSI_MMGSDI_ICCID_LEN * 2 + 1];
  uint8                   current_byte;
  int                     i;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  cmd_buf_p = (qmi_cmd_buf_type*)read_cnf->response_header.client_data; 
  if(cmd_buf_p == NULL)
  {
    LOG_MSG_INFO1_0("Null cmd_buf ptr in qmi_dms_process_mmgsdi_operation_complete");
    return;
  }

  LOG_MSG_INFO2_1("Received cmd_buf_ptr(user_data) %x from mggsdi read ICCID resp",
                  cmd_buf_p);

  if( NULL == cmd_buf_p->x_p || NULL == cmd_buf_p->x_p->cl_sp )
  {
    LOG_MSG_INFO1_0("Transaction ptr or client state pointer is NULL, most likely client was released.");
    return;
  }

  LOG_MSG_INFO2_0("Sending response for read ICCID");

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  response = NULL;
  errval = QMI_ERR_NONE;
  
  if(read_cnf->response_header.mmgsdi_status != MMGSDI_SUCCESS) 
  {
    errval = qmi_if_map_mmgsdi_status_to_errval(
               read_cnf->response_header.mmgsdi_status,__LINE__);
    goto send_result;
  }

  memset(uim_id, 0, DMSI_MMGSDI_ICCID_LEN * 2 + 1);
  /* Decode from BCD to ASCII string, stop when out of range */
  for(i = 0; i < DMSI_MMGSDI_ICCID_LEN; i++)
  {
    current_byte = read_cnf->read_data.data_ptr[i];
    if(QMI_BCD_LOW_DIGIT(current_byte) < 0x0A)
      uim_id[i * 2] = QMI_BCD_LOW_DIGIT(current_byte) + '0';
    else if(QMI_BCD_LOW_DIGIT(current_byte) < 0x0F)
      uim_id[i * 2] = QMI_BCD_LOW_DIGIT(current_byte) + 'A';
    else if(QMI_BCD_LOW_DIGIT(current_byte) == 0x0F)
      break;
    else
    {
      errval = QMI_ERR_INTERNAL;
      dsm_free_packet(&response);
      goto send_result;
    }

    if(QMI_BCD_HIGH_DIGIT(current_byte) < 0x0A)
      uim_id[i * 2 + 1] = QMI_BCD_HIGH_DIGIT(current_byte) + '0';
    else if(QMI_BCD_HIGH_DIGIT(current_byte) < 0x0F)
      uim_id[i * 2 + 1] = QMI_BCD_HIGH_DIGIT(current_byte) + 'A';
    else if(QMI_BCD_HIGH_DIGIT(current_byte) == 0x0F)
      break;
    else
    {
      errval = QMI_ERR_INTERNAL;
      dsm_free_packet(&response);
      goto send_result;
    }
  }

  if(FALSE == qmi_svc_put_param_tlv(&response,
                                    QMI_TYPE_REQUIRED_PARAMETERS,
                                    (uint16) strlen(uim_id),
                                    uim_id))
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
    goto send_result;
  }

send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  if(FALSE == retval)
  {
    dsm_free_packet(&response);
    ds_qmi_fw_free_cmd_buf( &cmd_buf_p );
    return;
  }

  if(FALSE == qmi_dmsi_send_response(cmd_buf_p, response))
  {
    LOG_MSG_INFO2_0 ("Unable to send response for QMI read ICCID operation: %d ");
    /*-----------------------------------------------------------------------
      Note - no need to free ind, taken care of in make_message()
      or send()
    -----------------------------------------------------------------------*/
    dsm_free_packet(&response);
    ds_qmi_fw_free_cmd_buf( &cmd_buf_p );
  }
} /* qmi_dmsi_generate_read_uim_iccid_resp */


/*===========================================================================
  FUNCTION QMI_DMSI_GENERATE_READ_UIM_IMSI_RESP()

  DESCRIPTION
    To send a response to the QMI read IMSI requested by the client.
    
  PARAMETERS
    read_cnf : info regarding reading IMSI

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_dmsi_generate_read_uim_imsi_resp
(
  mmgsdi_read_cnf_type * read_cnf
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response;
  qmi_error_e_type        errval;
  qmi_result_e_type       result;
  boolean                 retval;
  char                    uim_imsi[DMSI_MMGSDI_IMSI_LEN * 2 + 1];
  uint8                   current_byte;
  int                     i;
  int                     num_bytes_to_read;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  cmd_buf_p = (qmi_cmd_buf_type *) 
                 read_cnf->response_header.client_data;
  num_bytes_to_read = MIN( DMSI_MMGSDI_IMSI_LEN, read_cnf->read_data.data_len );

  LOG_MSG_INFO2_1("Received cmd_buf_ptr(user_data) %x from mggsdi read IMSI resp",
                 ((cmd_buf_p != NULL)?cmd_buf_p:0));

  if(cmd_buf_p == NULL)
  {
    return;
  }

  if( NULL == cmd_buf_p->x_p || NULL == cmd_buf_p->x_p->cl_sp )
  {
    LOG_MSG_INFO1_0("Transaction ptr or client state pointer is NULL, most likely client was released.");
    return;
  }

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  response = NULL;
  errval = QMI_ERR_NONE;
  
  if(read_cnf->response_header.mmgsdi_status != MMGSDI_SUCCESS) 
  {
    errval = qmi_if_map_mmgsdi_status_to_errval(
               read_cnf->response_header.mmgsdi_status,__LINE__);
    goto send_result;
  }

  memset(uim_imsi, 0, DMSI_MMGSDI_IMSI_LEN * 2 + 1);

  /* Decode MCC1 from Byte 2 (bit 4-7), should always available */
  current_byte = read_cnf->read_data.data_ptr[1];
  if(QMI_BCD_HIGH_DIGIT(current_byte) >= 0x0A)
  {
      LOG_MSG_ERROR_1("IMSI digit out of range %d",
                      QMI_BCD_HIGH_DIGIT(current_byte));
      errval = QMI_ERR_INTERNAL;
      dsm_free_packet(&response);
      goto send_result;
  }

  uim_imsi[0] = QMI_BCD_HIGH_DIGIT(current_byte) + '0';
  for(i = 2; i < num_bytes_to_read; i++)
  {
    current_byte = read_cnf->read_data.data_ptr[i];
    if(QMI_BCD_LOW_DIGIT(current_byte) < 0x0A)
      uim_imsi[(i - 2) * 2 + 1] = QMI_BCD_LOW_DIGIT(current_byte) + '0';
    else if(QMI_BCD_LOW_DIGIT(current_byte) == 0x0F)
      break;
    else
    {
        LOG_MSG_ERROR_1("IMSI digit out of range %d",
                        QMI_BCD_LOW_DIGIT(current_byte));
        errval = QMI_ERR_INTERNAL;
        dsm_free_packet(&response);
        goto send_result;
    }

    if(QMI_BCD_HIGH_DIGIT(current_byte) < 0x0A)
      uim_imsi[(i - 2) * 2 + 2] = QMI_BCD_HIGH_DIGIT(current_byte) + '0';
    else if(QMI_BCD_HIGH_DIGIT(current_byte) == 0x0F)
      break;
    else
    {
        LOG_MSG_ERROR_1("IMSI digit out of range %d",
                        QMI_BCD_HIGH_DIGIT(current_byte));
        errval = QMI_ERR_INTERNAL;
        dsm_free_packet(&response);
        goto send_result;
    }
  }

  if(FALSE == qmi_svc_put_param_tlv(&response,
                                    QMI_TYPE_REQUIRED_PARAMETERS,
                                    (uint16) strlen(uim_imsi),
                                    uim_imsi))
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
    goto send_result;
  }

send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  if(FALSE == retval)
  {
    dsm_free_packet(&response);
    ds_qmi_fw_free_cmd_buf( &cmd_buf_p );
    return;
  }

  if(FALSE == qmi_dmsi_send_response(cmd_buf_p, response))
  {
    LOG_MSG_INFO2_0 ("Unable to send response for QMI read IMSI operation: %d ");
    /*-----------------------------------------------------------------------
      Note - no need to free ind, taken care of in make_message()
      or send()
    -----------------------------------------------------------------------*/
    dsm_free_packet(&response);
    ds_qmi_fw_free_cmd_buf( &cmd_buf_p );
  }
} /* qmi_dmsi_generate_read_uim_imsi_resp */

/*===========================================================================
  FUNCTION QMI_DMSI_GENERATE_READ_UIM_MSISDN_RESP()

  DESCRIPTION
    To send a response to the QMI read MSISDN requested by the client.
    
  PARAMETERS
    read_cnf : info for IMSI read from UIM

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_dmsi_generate_read_uim_msisdn_resp
(
  mmgsdi_read_cnf_type * read_cnf
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response;
  qmi_error_e_type        errval;
  qmi_result_e_type       result;
  boolean                 retval;
  boolean                 output_msisdn;
#if defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900)
  char                    mob_num[NV_DIR_NUMB_PCS_SIZ + 1];
  char                    min_num[NV_DIR_NUMB_PCS_SIZ+1];
#endif /* defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900) */

  mmgsdi_return_enum_type mmgsdi_status;
  mmgsdi_access_type      sim_filename;
  mmgsdi_session_type_enum_type session_type;
  qmi_if_mmgsd_session_e_type session_index;

  qmi_if_info_type  *qmi_if_info_ptr;
  qmi_dmsi_client_state_type * dms_cl_sp;
  ds_qmi_fw_async_cmd_buf_type *async_cmd_buf;
  
  static char uim_imsi[DMSI_MMGSDI_IMSI_LEN * 2 + 1];
  static char uim_msisdn[DMSI_MMGSDI_MSISDN_LEN * 2 + 1];

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  cmd_buf_p = (qmi_cmd_buf_type *) read_cnf->response_header.client_data;
  mmgsdi_status = read_cnf->response_header.mmgsdi_status;
  qmi_if_info_ptr = qmi_if_info_get_ptr();
    
  LOG_MSG_INFO2_1("Received cmd_buf_ptr(user_data) %x from mmgsdi read MSISDN resp",
                   ((cmd_buf_p != NULL)?cmd_buf_p:0));

  if(cmd_buf_p == NULL)
  {
    return;
  }

  if( NULL == cmd_buf_p->x_p || NULL == cmd_buf_p->x_p->cl_sp )
  {
    LOG_MSG_INFO1_0("Transaction ptr or client state pointer is NULL, most likely client was released.");
    return;
  }
  
  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  response = NULL;
  errval = QMI_ERR_NONE;
  output_msisdn = FALSE;
  dms_cl_sp = (qmi_dmsi_client_state_type*)cmd_buf_p->x_p->cl_sp;

  session_type = MMGSDI_GW_PROV_PRI_SESSION;
  session_index = QMI_IF_MMGSD_GW_PROV_PRI_SESSION;

#ifdef FEATURE_DUAL_SIM
  if(dms_cl_sp->subscription_id == DMSI_BIND_SUBS_SECONDARY) 
  {
    session_type = MMGSDI_GW_PROV_SEC_SESSION;
    session_index = QMI_IF_MMGSD_GW_PROV_SEC_SESSION;
  }
#endif
#ifdef FEATURE_TRIPLE_SIM
  if(dms_cl_sp->subscription_id == DMSI_BIND_SUBS_TERTIARY)
  {
    session_type = MMGSDI_GW_PROV_TER_SESSION;
    session_index = QMI_IF_MMGSD_GW_PROV_TER_SESSION;
  }
#endif  
  if (read_cnf->access.file.file_enum != MMGSDI_TELECOM_MSISDN &&
      read_cnf->access.file.file_enum != MMGSDI_USIM_MSISDN    &&
      read_cnf->access.file.file_enum != MMGSDI_GSM_IMSI       && 
      read_cnf->access.file.file_enum != MMGSDI_USIM_IMSI      &&
      read_cnf->access.file.file_enum != MMGSDI_USIM_EXT5)
  {
    LOG_MSG_INFO2_1("Cannot determine EF to read (%d)",
                    read_cnf->access.file.file_enum);
    return;
  }

  if(read_cnf->access.file.file_enum == MMGSDI_GSM_IMSI ||
     read_cnf->access.file.file_enum == MMGSDI_USIM_IMSI)
  {
    memset(uim_imsi, 0, DMSI_MMGSDI_IMSI_LEN * 2 + 1);

    /* Report IMSI only when it is available through MMGSDI */
    if (MMGSDI_SUCCESS == mmgsdi_status) 
    {
      int i;
      uint8 current_byte;
      boolean bcd_valid = FALSE;

      /* Decode MCC1 from Byte 2 (bit 4-7), should always available */
      current_byte = read_cnf->read_data.data_ptr[1];
      if(QMI_BCD_HIGH_DIGIT(current_byte) < 0x0A)
      {
        bcd_valid = TRUE;
        uim_imsi[0] = QMI_BCD_HIGH_DIGIT(current_byte) + '0';

        /* Decode the rest from BCD to ASCII string, stop when out of range */
        for(i = 2; i < DMSI_MMGSDI_IMSI_LEN; i++)
        {
          current_byte = read_cnf->read_data.data_ptr[i];
          if(QMI_BCD_LOW_DIGIT(current_byte) < 0x0A)
            uim_imsi[(i - 2) * 2 + 1] = QMI_BCD_LOW_DIGIT(current_byte) + '0';
          else if(QMI_BCD_LOW_DIGIT(current_byte) == 0x0F)
            break;
          else
          {
            bcd_valid = FALSE;
            break;
          }
        
          if(QMI_BCD_HIGH_DIGIT(current_byte) < 0x0A)
            uim_imsi[(i - 2) * 2 + 2] = QMI_BCD_HIGH_DIGIT(current_byte) + '0';
          else if(QMI_BCD_HIGH_DIGIT(current_byte) == 0x0F)
            break;
          else
          {
            bcd_valid = FALSE;
            break;
          }
        }
      }

      /* Only send IMSI if it is valid*/
      if(bcd_valid != TRUE)
      {
          memset(uim_imsi, 0, DMSI_MMGSDI_IMSI_LEN);
      }
    }

    /* Read MSISDN request to MMGSDI */
    sim_filename.access_method = MMGSDI_EF_ENUM_ACCESS;
    sim_filename.file.file_enum = MMGSDI_NO_FILE_ENUM;
    
    if (qmi_if_info_ptr->session_info[session_index].app_type == MMGSDI_APP_SIM)
    {
      sim_filename.file.file_enum = MMGSDI_TELECOM_MSISDN;
    }
    else if (qmi_if_info_ptr->session_info[session_index].app_type == MMGSDI_APP_USIM)
    {
      sim_filename.file.file_enum = MMGSDI_USIM_MSISDN;
    }
    else
    {
      errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
      goto send_result;
    }

    async_cmd_buf = ds_qmi_fw_new_async_cmd_buf(cmd_buf_p, dms_cl_sp->cookie);
    if(async_cmd_buf == NULL)
    {
      errval = QMI_ERR_NO_MEMORY;
      goto send_result;
    }

    ds_qmi_cflog_amss_call("mmgsdi_session_read_record()");
    /* Read entire record for EF-MSISDN (x+14), len param = 0 */
    mmgsdi_status = mmgsdi_session_read_record(
                                     qmi_if_info_ptr->session_info[session_index].session_id,
                                     sim_filename,
                                     1,
                                     0,
                                     qmi_if_mmgsdi_resp_cback,
                                     (uint32) async_cmd_buf);

    if(mmgsdi_status == MMGSDI_SUCCESS)
    {
      /* MSISDN will be reported back later from MMGSDI, other items will be
      processed at that time */
      return;
    }
    else
    { 
      /* For any reason MSISDN cannot be obtained from MMGSDI it will be
      skipped and the functon will go forward to process other items */
      LOG_MSG_ERROR_1("MMGSDI read MSISDN status %d", mmgsdi_status);
      ds_qmi_fw_free_async_cmd_buf(async_cmd_buf);
      errval = QMI_ERR_INTERNAL;
      goto send_result;
    }
  
  }
  
  if (read_cnf->access.file.file_enum == MMGSDI_TELECOM_MSISDN ||
      read_cnf->access.file.file_enum == MMGSDI_USIM_MSISDN)
  {
    memset(uim_msisdn, 0, DMSI_MMGSDI_MSISDN_LEN * 2 + 1);

    /* Report MSISDN only when it is available through MMGSDI */
    if (MMGSDI_SUCCESS == mmgsdi_status) 
    {
      uint8 current_byte, i, offset, len;
      boolean bcd_valid = FALSE;
     
      /* Decode MSISDN, start by finding the start of binary MSISDN data */
      #define DMSI_MSISDN_ENCODED_DATA_OFFSET 14

      /* make sure the overall length is valid (greater than MSISDN start) */
      if (read_cnf->read_data.data_len < DMSI_MSISDN_ENCODED_DATA_OFFSET)
      {
        LOG_MSG_INFO2_1("UIM read length invalid (%d)",
                        read_cnf->read_data.data_len);
        errval = QMI_ERR_INTERNAL;
        goto send_result;
      }

      /* calculate the MSISDN start (offset is from the end of the EF) */
      offset = read_cnf->read_data.data_len - DMSI_MSISDN_ENCODED_DATA_OFFSET;

      /* Decode MCC1 from Byte 2 (bit 4-7), should always available */
      len = read_cnf->read_data.data_ptr[offset];
      if( len < DMSI_MSISDN_ENCODED_DATA_OFFSET )
      {
        bcd_valid = TRUE;

        /* Decode the rest from BCD to ASCII string, stop when out of range */
        for(i = 2; i <= len; i++)
        {
          current_byte = read_cnf->read_data.data_ptr[offset + i];
          if (QMI_BCD_LOW_DIGIT(current_byte) < 0x0A)
            uim_msisdn[(i - 2) * 2] = QMI_BCD_LOW_DIGIT(current_byte)+'0';
          else if (QMI_BCD_LOW_DIGIT(current_byte) == 0x0F)
            break;
          else
          {
            bcd_valid = FALSE;
            break;
          }

          if (QMI_BCD_HIGH_DIGIT(current_byte) < 0x0A)
            uim_msisdn[(i - 2) * 2 + 1] = QMI_BCD_HIGH_DIGIT(current_byte)+'0';
          else if (QMI_BCD_HIGH_DIGIT(current_byte) == 0x0F)
            break;
          else
          {
            bcd_valid = FALSE;
            break;
          }
        }
      }

      /* Only send MSISDN if it is valid*/
      if(bcd_valid != TRUE)
      {
        LOG_MSG_ERROR_0("uim_msisdn BCD format invalid, clearing string");
        memset(uim_msisdn, 0, DMSI_MMGSDI_MSISDN_LEN * 2 + 1);
      }
      /* Reading Extension file if additional data is there */
      if(bcd_valid == TRUE && read_cnf->read_data.data_ptr[read_cnf->read_data.data_len-1] != 0xFF)
      {
        mmgsdi_access_type  sim_filename_ext;
        mmgsdi_rec_num_type  ext_rec_num;
        /* extension record number is present in the last byte*/
        ext_rec_num = read_cnf->read_data.data_ptr[read_cnf->read_data.data_len-1];

        sim_filename_ext.access_method = MMGSDI_EF_ENUM_ACCESS;
        sim_filename_ext.file.file_enum = MMGSDI_NO_FILE_ENUM;
    
        if (qmi_if_info_ptr->session_info[session_index].app_type == MMGSDI_APP_USIM)
        {
          sim_filename_ext.file.file_enum = MMGSDI_USIM_EXT5;
        }
        else
        {
          LOG_MSG_ERROR_0("Extension is present in case of UIM only");
          goto send_result;
        }
        async_cmd_buf = ds_qmi_fw_new_async_cmd_buf(cmd_buf_p, dms_cl_sp->cookie);
        if(async_cmd_buf == NULL)
       {
         goto send_result;
       }
        mmgsdi_status = mmgsdi_session_read_record(
                                   qmi_if_info_ptr->session_info[session_index].session_id,
                                   sim_filename_ext,
                                   ext_rec_num,
                                   0,
                                   qmi_if_mmgsdi_resp_cback,
                                   (uint32) async_cmd_buf);
       if (MMGSDI_SUCCESS == mmgsdi_status)
       {
         /* MSISDN EXT will be reported back later from MMGSDI, other items will be
                processed at that time */
         return;
       }
       else
       {
         ds_qmi_fw_free_async_cmd_buf(async_cmd_buf);
         /* For any reason MSISDN cannot be obtained from MMGSDI it will be
               skipped and the functon will go forward to process other items */
         LOG_MSG_ERROR_1("MMGSDI read MSISDN EXT failed with status %d", mmgsdi_status);
       }
      }
    }
    else
    {
      errval = qmi_if_map_mmgsdi_status_to_errval(mmgsdi_status,__LINE__);
    }

    /* MSISDN is last item but continue reading ext file when EXT is present.*/
    goto send_result;
  }

  if(read_cnf->access.file.file_enum == MMGSDI_USIM_EXT5)
  {
    /* Report MSISDN only when it is available through MMGSDI */
    if (MMGSDI_SUCCESS == mmgsdi_status) 
    {
      uint8 current_byte, j, len;
      boolean bcd_valid = FALSE;

      /* Length of Ext5 file  */
      #define DMSI_MSISDN_EXTN_LENGTH 13
      
      /* make sure the overall length is valid (greater than MSISDN start) */
      if (read_cnf->read_data.data_len < read_cnf->read_data.data_ptr[1])
      {
         LOG_MSG_INFO2_1("UIM extension read length invalid (%d)",read_cnf->read_data.data_len);
    goto send_result;
  }

      /* In byte 2, length of additional data is present */
      len = read_cnf->read_data.data_ptr[1];

      #define MSISDN_EXT_ADDITIONAL_DATA_TYPE 0x02
      /* Data type should be additional data*/
      if(read_cnf->read_data.data_ptr[0] == MSISDN_EXT_ADDITIONAL_DATA_TYPE)
      {
         bcd_valid = TRUE;
         /* 10 bytes are already read from msisdn*/
         #define UIM_MSISDN_OFFSET 10

         /* Decode the rest from BCD to ASCII string, stop when out of range */
         for(j=0; j < len; j++)
         {
            current_byte = read_cnf->read_data.data_ptr[j+2];
            if (QMI_BCD_LOW_DIGIT(current_byte) < 0x0A)
              uim_msisdn[(j+UIM_MSISDN_OFFSET) * 2] = QMI_BCD_LOW_DIGIT(current_byte)+'0';
           else if (QMI_BCD_LOW_DIGIT(current_byte) == 0x0F)
              break;
           else
           {
              bcd_valid = FALSE;
              break;
           }

            if (QMI_BCD_HIGH_DIGIT(current_byte) < 0x0A)
              uim_msisdn[(j+UIM_MSISDN_OFFSET) * 2 + 1] = QMI_BCD_HIGH_DIGIT(current_byte)+'0';
            else if (QMI_BCD_HIGH_DIGIT(current_byte) == 0x0F)
               break;
            else
            {
              bcd_valid = FALSE;
              break;
            }
         }
         if(bcd_valid != TRUE)
          {
            LOG_MSG_ERROR_0("uim_msisdn BCD format invalid, clearing string");
            memset(uim_msisdn, 0, DMSI_MMGSDI_MSISDN_LEN * 2 + 1);
          }
       }
     }
     else
     {
       LOG_MSG_ERROR_1("MMGSDI couldn't read MSISDN EXT status %d", mmgsdi_status);
     }
   }

send_result:
  /* add the IMSI anytime it is present */
  if(strlen(uim_imsi))
  {
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                    DMSI_PRM_TYPE_UIM_IMSI,
                                    strlen(uim_imsi),
                                    uim_imsi))
    {
      errval = QMI_ERR_NO_MEMORY;
    }
  }

  /* Read MDN and MIN if this is primary subscription */
  if(dms_cl_sp->subscription_id == DMSI_BIND_SUBS_PRIMARY)
  {
#if defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900)
    memset(mob_num, 0, sizeof(mob_num));
    memset(min_num, 0, sizeof(min_num));
  
    errval = qmi_dmsi_get_mdn_min(mob_num, sizeof(mob_num),
                                  min_num, sizeof(min_num));
  
    /* add the MIN anytime it is present */
    if (errval == QMI_ERR_NONE)
    {
      if (strlen(min_num))
      {
        if (FALSE == qmi_svc_put_param_tlv(&response, 
                                           DMSI_PRM_TYPE_MIN,
                                           (uint16) strlen(min_num),
                                           min_num))
        {
          errval = QMI_ERR_NO_MEMORY;
          dsm_free_packet(&response);
        }
      }
    }
    /* qmi_dmsi_get_mdn_min() above checks for NOT_PROVISIONED case and sets
       errval correctly, no reason to check it again*/
    if (errval == QMI_ERR_NONE)
    {
      if (FALSE == qmi_svc_put_param_tlv(&response, 
                                        QMI_TYPE_REQUIRED_PARAMETERS,
                                        (uint16) strlen(mob_num),
                                        mob_num))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }

    //Getting MDN and MIN Failed, include MSISDN in the response.
    if(errval != QMI_ERR_NONE)
    {
      output_msisdn = TRUE;
      errval = QMI_ERR_NONE;
    }
#else
    /* Return MSISDN as CDMA feature is disabled in primary subscription */
    output_msisdn = TRUE;
#endif /* defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900) */
  }
  else /* Return only the MSISDN read as this is secondary subscription */
  {
     output_msisdn = TRUE;
  }
 
  if(output_msisdn) 
  {
    /* MSISDN */
    if(strlen(uim_msisdn))
    {
      if (FALSE == qmi_svc_put_param_tlv(&response, 
                                         QMI_TYPE_REQUIRED_PARAMETERS,
                                         strlen(uim_msisdn),
                                         uim_msisdn))
      {
        errval = QMI_ERR_NO_MEMORY;
      }
    }
    else if (errval == QMI_ERR_NONE)
    {
      /* No records of MSISDN */
      errval = QMI_ERR_NOT_PROVISIONED;
    }
  }

  /* make sure to explicitly clear any TLV's that may have been added to the
     response if there is an error detected */
  if (QMI_ERR_NONE != errval)
  {
    dsm_free_packet(&response);
  }
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  if(FALSE == retval)
  {
    dsm_free_packet(&response);
    ds_qmi_fw_free_cmd_buf( &cmd_buf_p );
    return;
  }

  if(FALSE == qmi_dmsi_send_response(cmd_buf_p, response))
  {
    LOG_MSG_INFO2_0 ("Unable to send response for QMI read MSISDN operation: %d ");
    /*-----------------------------------------------------------------------
      Note - no need to free ind, taken care of in make_message()
      or send()
    -----------------------------------------------------------------------*/
    dsm_free_packet(&response);
    ds_qmi_fw_free_cmd_buf( &cmd_buf_p );
  }
} /* qmi_dmsi_generate_read_uim_msisdn_resp */
/*===========================================================================
  FUNCTION QMI_DMSI_GENERATE_READ_PRL_VER_RESP()

  DESCRIPTION
    To send a response to the QMI read prl version requested by the client.
    
  PARAMETERS
    read_prl_cnf : info for prl version read from UIM

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_dmsi_generate_read_prl_ver_resp
(
  mmgsdi_session_read_prl_cnf_type * read_prl_cnf
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response;
  qmi_error_e_type        errval;
  qmi_result_e_type       result;
  boolean                 retval;
  PACKED struct PACKED_POST
  {
    uint16  prl_ver;
  } v_out_req;

  PACKED struct PACKED_POST
  {
    uint8   prl_only;
  } v_out_prl_info;

  qmi_if_info_type  *qmi_if_info_ptr;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  cmd_buf_p = (qmi_cmd_buf_type *) read_prl_cnf->response_header.client_data;

  qmi_if_info_ptr = qmi_if_info_get_ptr();

  LOG_MSG_INFO2_1("Received cmd_buf_ptr(user_data) %x from mmgsdi read prl version resp",
                 ((cmd_buf_p != NULL)?cmd_buf_p:0));

  if(cmd_buf_p == NULL)
  {
    return;
  }


  if( NULL == cmd_buf_p->x_p || NULL == cmd_buf_p->x_p->cl_sp )
  {
    LOG_MSG_INFO1_0("Transaction ptr or client state pointer is NULL, most likely client was released.");
    return;
  }

  LOG_MSG_INFO2_0("Sending response for read prl version");

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  response = NULL;
  errval = QMI_ERR_NONE;

  memset(&v_out_req, 0, sizeof(v_out_req));
  memset(&v_out_prl_info, 0, sizeof(v_out_prl_info));

  if(read_prl_cnf->response_header.mmgsdi_status != MMGSDI_SUCCESS) 
  {
    errval = qmi_if_map_mmgsdi_status_to_errval(
               read_prl_cnf->response_header.mmgsdi_status,__LINE__);
    goto send_result;
  }

  v_out_req.prl_ver = (uint16)read_prl_cnf->prl_version;  

  if ((FALSE == read_prl_cnf->valid ) || 
      (DMSI_PRL_VERSION_INVALID == v_out_req.prl_ver))
  {
    errval = QMI_ERR_INVALID_ARG;
    goto send_result;
  }

  v_out_prl_info.prl_only = qmi_if_info_ptr->ph_info.prl_only;
  if( FALSE == qmi_svc_put_param_tlv(&response, 
                                     DMSI_PRM_TYPE_PRL_ONLY,
                                     sizeof(v_out_prl_info),
                                     &v_out_prl_info ) )
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
    goto send_result;
  }

  if( FALSE == qmi_svc_put_param_tlv(&response, 
                                     QMI_TYPE_REQUIRED_PARAMETERS,
                                     sizeof(v_out_req),
                                     &v_out_req ) )
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
    goto send_result;
  }

send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  if(FALSE == retval)
  {
    dsm_free_packet(&response);
    ds_qmi_fw_free_cmd_buf( &cmd_buf_p );
    return;
  }

  if(FALSE == qmi_dmsi_send_response(cmd_buf_p, response))
  {
    LOG_MSG_INFO2_0 ("Unable to send response for QMI read prl version");
    /*-----------------------------------------------------------------------
      Note - no need to free ind, taken care of in make_message()
      or send()
    -----------------------------------------------------------------------*/
    dsm_free_packet(&response);
    ds_qmi_fw_free_cmd_buf( &cmd_buf_p );
  }
} /* qmi_dmsi_generate_read_prl_ver_resp */
#endif /* FEATURE_MMGSDI_SESSION_LIB */


#ifdef FEATURE_DATA_QMI_ADDENDUM
/*===========================================================================
  FUNCTION QMI_DMSI_GET_UQCN_VERSION
  
  DESCRIPTION
    This command reads the appropriate NV item containing the UQCN version 
    information for UMTS / CDMA.
  
  DEPENDENCIES
    NONE
  
  RETURN VALUE
    uint32 - UQCN Version
  
  SIDE EFFECTS
    None
===========================================================================*/
uint32 qmi_dmsi_get_uqcn_version()
{
  static uint32 uqcn_version = 0;
  nv_item_type *dcc_nv_item_ptr;

  /*-------------------------------------------------------------------------
    Allocate temporary memory for the NV item
  -------------------------------------------------------------------------*/
  dcc_nv_item_ptr = (nv_item_type *)
    qmi_svc_ps_system_heap_mem_alloc(sizeof(nv_item_type),
                            FILE_ID_DS_QMI_DMS,__LINE__);
  if( dcc_nv_item_ptr == NULL )
  {
    return 0;
  }

  if (!uqcn_version) 
  {
#if (defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900))
    /* Fetch NV item #4995 */
    if (NV_DONE_S == dcc_get_nv_item(NV_CUST_UQCN_C2K_VERSION_I, 
                                     dcc_nv_item_ptr))
    {
      uqcn_version = dcc_nv_item_ptr->cust_uqcn_c2k_version;
    }
#elif (defined(FEATURE_WCDMA) || defined(FEATURE_GSM)) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA)
    /* Fetch NV item #4996 */
    if (NV_DONE_S == dcc_get_nv_item(NV_CUST_UQCN_UMTS_VERSION_I, 
                                     dcc_nv_item_ptr))
    {
      uqcn_version = dcc_nv_item_ptr->cust_uqcn_umts_version;
    }
#endif
  }

  PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_item_ptr);
  return uqcn_version;
}
#endif /* FEATURE_DATA_QMI_ADDENDUM */

#if defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900)
#if (defined FEATURE_OTASP)
/*===========================================================================
  FUNCTION QMI_DMSI_OTASP_FORCED_CALL_RELEASE()

  DESCRIPTION
    End all calls to allow power down regostration through after OTASP session

  PARAMETERS
   None
    
  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_dmsi_otasp_forced_call_release()
{
  cm_end_params_s_type end_params;

  if ( qmi_dmsi_global.voice_call_id != CM_CALL_ID_INVALID )
  {
     end_params.call_id = (cm_call_id_type) qmi_dmsi_global.voice_call_id;
     end_params.info_type = CM_CALL_MODE_INFO_CDMA;
     end_params.end_params.cdma_end.end_reason_included = FALSE;
     end_params.end_params.cdma_end.end_reason = CAI_REL_NORMAL;
     LOG_MSG_INFO1_1 ("QMI: Adding call_id %d to the end list",
                      end_params.call_id);

     ds_qmi_cflog_cm_call_cmd_end( end_params.call_id, 
                                   end_params.info_type, 
                                   "OTASP forced release" );

     (void) cm_mm_call_cmd_end( NULL, NULL, (cm_client_id_type) -1,
                              1, &end_params );
  }
  else
  {
      LOG_MSG_INFO1_0 ("QMI: Could not locate call_id ");
  }
} /* qmi_dmsi_otasp_forced_call_release */
#endif /* (defined FEATURE_OTASP) */
#endif /* defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900) */


/*===========================================================================
  FUNCTION QMI_DMS_PROCESS_CALL_EVENT()

  DESCRIPTION
    Called when a QMI_IF call event happens.
    
  PARAMETERS
    None

  RETURN VALUE
    None
        
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void qmi_dms_process_call_event
(
  void
)
{
  qmi_if_info_type  *qmi_if_info_ptr;
  qmi_if_asubs_id_type  sub_id = QMI_IF_AS_ID_MIN;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  qmi_if_info_ptr = qmi_if_info_get_ptr();

  switch (qmi_if_info_ptr->call_info.call_event)
  {
    case CM_CALL_EVENT_END:
    case CM_CALL_EVENT_ORIG:
    case CM_CALL_EVENT_CONNECT:
      /* CDMA devices */
#if defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900)
      /* OTASP activation states */
#if defined(FEATURE_OTASP)
      if ((CM_CALL_TYPE_STD_OTASP     == qmi_if_info_ptr->call_info.call_type) ||
          (CM_CALL_TYPE_NON_STD_OTASP == qmi_if_info_ptr->call_info.call_type))
      {
        qmi_dmsi_global.act_evt.act_type = QMI_DMSI_ACTIVATION_TYPE_CALL;
        qmi_dmsi_global.act_evt.event.call_event = 
                                               qmi_if_info_ptr->call_info.call_event;

        sub_id = qmi_if_get_default_sub_id();

        LOG_MSG_INFO2_3("qmi_dms_process_call_event: call_event %d activation_state %d sys_mode %d",
                        qmi_if_info_ptr->call_info.call_event,
                        qmi_dmsi_global.activation_state,
                        qmi_if_info_ptr->ss_info[sub_id].sys_mode);

        qmi_dmsi_generate_activation_state_ind();
      }
#endif /* defined(FEATURE_OTASP) */
#endif /* defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900) */
      break;

    case CM_CALL_EVENT_OTASP_STATUS:
      /* CDMA devices */
#if defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900)
      /* OTASP activation states */
#if defined(FEATURE_OTASP)
      if ((CM_CALL_TYPE_STD_OTASP     == qmi_if_info_ptr->call_info.call_type) ||
          (CM_CALL_TYPE_NON_STD_OTASP == qmi_if_info_ptr->call_info.call_type))
      {
        if (CM_OTASP_STATUS_SPC_RETRIES_EXCEEDED == 
                                              qmi_if_info_ptr->call_info.otasp_state)
        {
          /* Too many failed attempts to unlock phone. power down */
          (void)cm_ph_cmd_oprt_mode(NULL, NULL, CM_CLIENT_ID_ANONYMOUS, 
                                    SYS_OPRT_MODE_OFFLINE_CDMA);
          if(!cm_rpm_check_reset_allowed())
          {
            LOG_MSG_ERROR_1("OTSAP SPC retires %d execeed MAX value. CM denied power off ",qmi_if_info_ptr->call_info.otasp_state);
            break;
          }
          LOG_MSG_INFO1_0("Triggered Sys Initate Poweroff");          
          sys_m_initiate_poweroff();
        } 
        else 
        {
          qmi_dmsi_global.act_evt.act_type = QMI_DMSI_ACTIVATION_TYPE_OTASP;
          qmi_dmsi_global.act_evt.event.otasp_state = 
                                              qmi_if_info_ptr->call_info.otasp_state;
          qmi_dmsi_generate_activation_state_ind();
        }
      }
#endif /* defined(FEATURE_OTASP) */
#endif /* defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900) */
      break;

    default:
      LOG_MSG_INFO1_1("unknown call event type, ignoring (%d)",
                      qmi_if_info_ptr->call_info.call_event);
      break;
  }
} /* qmi_dms_process_call_event */


#if defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900)

/*===========================================================================
  FUNCTION QMI_DMSI_GENERATE_ACTIVATION_STATE_IND()

  DESCRIPTION
    Called when the activation state information changes. Causes the 
    activation_state_ind to be sent to all clients that have requested to be
    notified of such events.
    
  PARAMETERS
    None

  RETURN VALUE
    None
        
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_dmsi_generate_activation_state_ind
(
  void
)
{
  qmi_dmsi_client_state_type *     cl_sp;
  qmi_dmsi_state_type *            dms_sp;
  int                              j;
  qmi_dmsi_activation_state_e_type act_state;
#if (defined FEATURE_OTASP)
  boolean                          otasp_state;
  boolean                          last_otasp_state;
  qmi_nv_status_e_type             nv_status;
#endif /* (defined FEATURE_OTASP) */
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  act_state = DMSI_ACTIVATION_STATE_MAX;

#if (defined FEATURE_OTASP)
  otasp_state = FALSE;
  last_otasp_state = FALSE;
#endif /* (defined FEATURE_OTASP) */

  LOG_MSG_INFO2_1("Generating DMS IND for activation state change (event=%d)",
                  qmi_dmsi_global.act_evt.act_type);

  /*-----------------------------------------------------------------------
    Determine the new activation state based on the current global values
  -----------------------------------------------------------------------*/
  switch (qmi_dmsi_global.act_evt.act_type)
  {

    /* CDMA devices */
#if defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900)
    /* OTASP activation states */
#if (defined FEATURE_OTASP)
    case QMI_DMSI_ACTIVATION_TYPE_CALL:
    {
      LOG_MSG_INFO3_1("Call event=%d",
                      qmi_dmsi_global.act_evt.event.call_event);
      
      switch (qmi_dmsi_global.act_evt.event.call_event)
      {
        case CM_CALL_EVENT_ORIG:
        {
          act_state = DMSI_ACTIVATION_STATE_CONNECTING;
          break;
        }
            
        case CM_CALL_EVENT_CONNECT:
        {
          act_state = DMSI_ACTIVATION_STATE_CONNECTED;
          break;
        }

        /* To determine 'activated' state based on the last OTASP
           success or failure, do so once the call has ended */
        case CM_CALL_EVENT_END:
        {
          /* If the last state was COMMIT then the call was successful */
          if (DMSI_ACTIVATION_STATE_COMMIT == qmi_dmsi_global.activation_state)
          {
            act_state = DMSI_ACTIVATION_STATE_ACTIVATED;
            otasp_state = TRUE;
          }
          /* Else it failed */
          else
          {
            act_state = DMSI_ACTIVATION_STATE_NOT_ACTIVATED;
            otasp_state = FALSE;
          }
          
          /* see if build should store last OTASP state to QMI NV item */
          if (qmi_dmsi_global.nv_use_last_otasp_act_state)
          {
             /* if so, read the 'last' OTASP state */
             nv_status = qmi_nv_read(QMI_NV_ITEM_LAST_OTASP_STATE, 0, 
                                    &last_otasp_state, 
                                    sizeof(last_otasp_state));

            /* check if the current val was read or not.  if it was read, make
               sure the new val is different.  if it wasn't read, try to write
               anyways. */
             if(nv_status != QMI_NV_STATUS_OK)
             {
               LOG_MSG_ERROR_1("Could not read OTASP state (%d)",
                               last_otasp_state);
             }
              /* if the current OTASP state doesn't match last, write new val */
            if (otasp_state != last_otasp_state)
            {
              if (QMI_NV_STATUS_OK != qmi_nv_write(
                                                 QMI_NV_ITEM_LAST_OTASP_STATE, 
                                                 0, 
                                                 &otasp_state, 
                                                 sizeof(otasp_state)))
              {
                LOG_MSG_ERROR_1("Could not write OTASP state (%d)",
                                otasp_state);
              }
            }
          }
          break;
        }

        /* No other CM call events translate into activation states so ignore */
        default:
          break;
      }
      break;
    } /* case QMI_DMSI_ACTIVATION_TYPE_CALL */

    case QMI_DMSI_ACTIVATION_TYPE_OTASP:
    {
      LOG_MSG_INFO3_1("OTASP state=%d",
                      qmi_dmsi_global.act_evt.event.otasp_state);
      
      switch (qmi_dmsi_global.act_evt.event.otasp_state)
      {
        case CM_OTASP_STATUS_SPL_UNLOCKED:
        {
          act_state = DMSI_ACTIVATION_STATE_AUTHENTICATED;
          break;
        }

        case CM_OTASP_STATUS_NAM_DOWNLOADED:
        {
          act_state = DMSI_ACTIVATION_STATE_NAM_DLOADED;
          break;
        }

        case CM_OTASP_STATUS_MDN_DOWNLOADED:
        {
          act_state = DMSI_ACTIVATION_STATE_MDN_DLOADED;
          break;
        }

        case CM_OTASP_STATUS_IMSI_DOWNLOADED:
        {
          act_state = DMSI_ACTIVATION_STATE_IMSI_DLOADED;
          break;
        }

        case CM_OTASP_STATUS_PRL_DOWNLOADED:
        {
          act_state = DMSI_ACTIVATION_STATE_PRL_DLOADED;
          break;
        }

        case CM_OTASP_STATUS_COMMITTED:
        {
          act_state = DMSI_ACTIVATION_STATE_COMMIT;
          break;
        }

        /* No other OTASP events translate into activation states so ignore */
        default:
          break;
      }
      break;
    } /* case QMI_DMSI_ACTIVATION_TYPE_OTASP */

#endif /* (defined FEATURE_OTASP) */
#endif /* (defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900) */
    default:
      LOG_MSG_ERROR_0(" Act type not supported");
  }

  /* If the activation state was not changed then there is no new activation IND
     to send.  Return here ignore the un-matched events. */
  if (DMSI_ACTIVATION_STATE_MAX == act_state)
  {
    return;
  }

  LOG_MSG_INFO2_2("qmi_dmsi_generate_activation_state_ind: old %d, new %d",
                  qmi_dmsi_global.activation_state, act_state);
  if (act_state != qmi_dmsi_global.activation_state)
  {
    /* Update the global state if the 'new' state is valid and has changed */
    qmi_dmsi_global.activation_state = act_state;
    
    /* Generate event report ind for clients registered for authetication state
       changes */
    dms_sp = (qmi_dmsi_state_type*)&qmi_dms_state;
    for( j = 0; j < DMSI_MAX_CLIDS ; j++ )
      {
      cl_sp = dms_sp->client[j];
      if( cl_sp && (cl_sp->common.clid != QMI_SVC_CLID_UNUSED) && 
            cl_sp->report_status.report_activation_state )
        {
          qmi_dmsi_event_report_ind(dms_sp,
                                    cl_sp->common.clid,
                                    DMSI_REPORT_STATUS_EV_ACTIVATION_STATE,
                                    0,
                                    0,
                                    0,
                                    (uint16) qmi_dmsi_global.activation_state,
                                    0,
                                    0,
                                    0);
        }
      }
    }
  else 
  {
    LOG_MSG_INFO2_0("qmi_dmsi_generate_activation_state_ind: IND cached");
  }

#if (defined FEATURE_OTASP)
  if (DMSI_ACTIVATION_STATE_COMMIT == qmi_dmsi_global.activation_state)
  {
    qmi_dmsi_otasp_forced_call_release();
  }
#endif /* (defined FEATURE_OTASP) */
} /* qmi_dmsi_generate_activation_state_ind */
#endif /* defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900) */

/*===========================================================================
FUNCTION  QMI_DMS_CM_CALL_CMD_CB

DESCRIPTION
  CM call command callback function

DEPENDENCIES
  None

RETURNS
  None

SIDE EFFECTS
  Adds command in DS command buffer

===========================================================================*/
void qmi_dms_cm_call_cmd_cb 
(
  void                         *data_ptr,         /* Data block pointer    */
  cm_call_cmd_e_type           call_cmd,          /* Command ID            */
  cm_call_cmd_err_e_type       call_cmd_err       /* Command error code    */
)
{
   qmi_dmsi_cmd_buf_type *  cmd_ptr;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  
  /*-------------------------------------------------------------------------
    Post a cmd to DCC to handle this in DCC context
  -------------------------------------------------------------------------*/

  switch(call_cmd)
  {
    case CM_CALL_CMD_ORIG:
      cmd_ptr = (qmi_dmsi_cmd_buf_type *) qmi_dmsi_get_cmd_buf(QMI_CMD_DMS_CM_CALL_CMD_STATUS);
      if( cmd_ptr != NULL)
      {
        cmd_ptr->cmd_id = QMI_CMD_DMS_CM_CALL_CMD_STATUS;
        cmd_ptr->data.cm_call_err.user_data = data_ptr;
        cmd_ptr->data.cm_call_err.call_cmd  = call_cmd;
        cmd_ptr->data.cm_call_err.err_type  = call_cmd_err;
        dcc_send_cmd_ex(DCC_QMI_CMD, cmd_ptr);
      }
      break;

    default:
      LOG_MSG_INFO2_0("Unexpected ph_cmd received in qmi_dmsi_cm_ph_cb");
  }

  return;
  
} /* qmi_dms_cm_call_cmd_cb */



/*===========================================================================
  FUNCTION QMI_DMS_PROCESS_CM_CALL_ERR()

  DESCRIPTION
    Process the CM call event

  PARAMETERS
    user_data :  user data ptr passed to CM
    call_cmd  :  requested CM call command
    err_type  :  result of request
    
  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void qmi_dms_process_cm_call_err
(
  void *                 user_data,
  cm_call_cmd_e_type     call_cmd,
  cm_call_cmd_err_e_type err_type
)
{
  dsm_item_type *               response;
  qmi_cmd_buf_type *        cmd_buf_p;
  qmi_error_e_type              errval;
  qmi_result_e_type             result;
  boolean                       retval;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  errval = QMI_ERR_NONE;
  cmd_buf_p = (qmi_cmd_buf_type *) user_data;
  if(cmd_buf_p == NULL)
  {
    LOG_MSG_INFO1_0("Null cmd_buf ptr");
    return;
  }

  if( NULL == cmd_buf_p->x_p || NULL == cmd_buf_p->x_p->cl_sp )
  {
    LOG_MSG_INFO1_0("Transaction ptr or client state pointer is NULL, most likely client was released.");
    return;
  }

  /* map the CM request error to the proper QMI error value */
  switch (err_type)
  {
    case CM_CALL_CMD_ERR_NOERR:
      errval = QMI_ERR_NONE;
      break;

    case CM_CALL_CMD_ERR_OFFLINE_S:
      errval = QMI_ERR_NO_NETWORK_FOUND;
      break;

    case CM_CALL_CMD_ERR_IN_USE_S:
      errval = QMI_ERR_DEVICE_IN_USE;
      break;

    default:
      errval = QMI_ERR_INTERNAL;
      break;
  }

  response = NULL;
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  if(FALSE == retval)
  {
    dsm_free_packet(&response);
    ds_qmi_fw_free_cmd_buf( &cmd_buf_p );
    return;
  }
  
  if(FALSE == qmi_dmsi_send_response(cmd_buf_p, response))
  {
    LOG_MSG_INFO2_0 ("Unable to send CM call cmd response");
    // verify if free packet and cmd buf needed here
    dsm_free_packet(&response);
    ds_qmi_fw_free_cmd_buf(&cmd_buf_p);
  }
} /* qmi_dms_process_cm_call_err() */

#if defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900)
/*===========================================================================
  FUNCTION QMI_DMSI_GET_MDN_MIN()

  DESCRIPTION
    a
    
  PARAMETERS
    None

  RETURN VALUE
    None
        
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
qmi_error_e_type qmi_dmsi_get_mdn_min
(
  char *mob_num,
  int   mdn_size,
  char *min_num,
  int   min_size
)
{
  qmi_error_e_type        errval;
  
  dword                   min1;
  word                    min2;
  int                     min_len;

  nv_stat_enum_type       nv_status;
  nv_item_type           *dcc_nv_item_ptr;

  cm_nam_e_type           curr_nam;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(mob_num && min_num);

  errval = QMI_ERR_NONE;
  min1 = min2 = min_len = 0;

  memset(mob_num, 0, mdn_size);
  memset(min_num, 0, min_size);

#if defined(FEATURE_RUIM) && defined(FEATURE_UIM_RUN_TIME_ENABLE)
#ifdef FEATURE_MMGSDI_SESSION_LIB
  /*-------------------------------------------------------------------------
    Check the RUIM state before querying MDN/MIN
  -------------------------------------------------------------------------*/
  if ((NV_RTRE_CONTROL_USE_RUIM == nv_rtre_control()) &&
      ((errval = qmi_dms_get_uim_access_status_ext(MMGSDI_1X_PROV_PRI_SESSION)) != QMI_ERR_NONE))
  {
    LOG_MSG_INFO1_1("RUIM not initialized (%d)", errval);
  }
#else /* FEATURE_MMGSDI_SESSION_LIB */
    errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
#endif /* FEATURE_MMGSDI_SESSION_LIB */
#endif /* defined(FEATURE_RUIM) && defined(FEATURE_UIM_RUN_TIME_ENABLE) */

  if (QMI_ERR_NONE == errval)
  {
    /*-------------------------------------------------------------------------
      Allocate temporary memory for the NV item
    -------------------------------------------------------------------------*/
    dcc_nv_item_ptr = (nv_item_type *)
      qmi_svc_ps_system_heap_mem_alloc(sizeof(nv_item_type),
                               FILE_ID_DS_QMI_DMS,__LINE__);
    if( dcc_nv_item_ptr == NULL )
    {
      return QMI_ERR_NO_MEMORY;
    }
    
    nv_status = dcc_get_nv_item(NV_CURR_NAM_I, dcc_nv_item_ptr);  
    if(nv_status != NV_DONE_S || dcc_nv_item_ptr->curr_nam >= NV_MAX_NAMS)
    {
      /*-----------------------------------------------------------------------
        Default to the first NAM
      -----------------------------------------------------------------------*/
      dcc_nv_item_ptr->curr_nam = (byte) CM_NAM_1;
    }
    curr_nam = (cm_nam_e_type)dcc_nv_item_ptr->curr_nam;
    dcc_nv_item_ptr->mob_dir_number.nam = dcc_nv_item_ptr->curr_nam;

    /*-------------------------------------------------------------------------
      Get the Mobile Directory Number from NV
    -------------------------------------------------------------------------*/
    nv_status = dcc_get_nv_item(NV_DIR_NUMBER_PCS_I, dcc_nv_item_ptr);
    if (NV_DONE_S == nv_status)
    {
      /*-----------------------------------------------------------------------
        Convert the mobile number to text format
      -----------------------------------------------------------------------*/
      qmi_dmsi_mobnum_pcs_to_text(mob_num, &dcc_nv_item_ptr->mob_dir_number);
      dcc_nv_item_ptr->min1.nam = curr_nam;
      if (NV_DONE_S == dcc_get_nv_item(NV_MIN1_I, dcc_nv_item_ptr))
      {
        min1 = dcc_nv_item_ptr->min1.min1[NV_CDMA_MIN_INDEX];
        if (NV_DONE_S == dcc_get_nv_item(NV_MIN2_I, dcc_nv_item_ptr))
        {
          min2 = dcc_nv_item_ptr->min2.min2[NV_CDMA_MIN_INDEX];
          if (qmi_dmsi_minnum_validate(min1, min2))
          {
            min_len = qmi_dmsi_minnum_pcs_to_text(min_num, min1, min2);
            min_num[min_len] = '\0';
          }
        }
      }
    }
    else
    {
      errval = QMI_ERR_NOT_PROVISIONED;  
    }

    /*-------------------------------------------------------------------------
      Free the temporary memory allocated for NV item.
    -------------------------------------------------------------------------*/
    PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_item_ptr);
  }
  
  return errval;
} /* qmi_dmsi_get_mdn_min() */


/*===========================================================================
  FUNCTION QMI_DMSI_GENERATE_DEFAULT_MIN()

  DESCRIPTION
    Generates the default MIN (IMSI_M_S1 / IMSI_T_S1) for the device, based on 
    the ESN, that is set when the UI defaults the provisioning.
    
  PARAMETERS
    esn : dword containing the ESN in binary

  RETURN VALUE
    dword MIN in encoded format returned
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dword qmi_dmsi_generate_default_min
(
  dword  esn
)
{
  word zero;     /* Encoding of three zero digits */
  word fourth;   /* Fourth from last decimal digit of the ESN */
  word last3;    /* Last three decimal digits of the ESN */
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /* Encode digits as per JSTD-008 section 2.3.1.1 */
  zero = 1110 - 111;
  last3 = (esn % 1000);
  last3 += ((last3 / 100) == 0) ? 1000 : 0;
  last3 += (((last3 % 100) / 10) == 0) ? 100 : 0;
  last3 += ((last3 % 10) == 0) ? 10 : 0;
  last3 -= 111;
  fourth = ((esn % 10000) / 1000); /* In range 0-9 */
  if (fourth == 0)
  {
    fourth = 10;
  }

  /* Concatenate results and return 24 bit value for imsi_s1 */
  /* Example: esn = 120406
  **      imsi_s1 = 000  0  406
  **  encodes to -> 999  10 395
  **       in hex = 3e7  a  18b
  **    in binary = 1111100111 1010 0110001011
  */
  return (((dword) zero << 14) | (fourth << 10) | last3);
} /* qmi_dmsi_generate_default_min() */

/*===========================================================================
  FUNCTION QMI_DMSI_MINNUM_VALIDATE()

  DESCRIPTION
    validate MIN NV format to make sure no reserved areas are set and that the 
    MIN values are within the propper range
    
  PARAMETERS
    min1 : lower 24 bits of the MIN binary representation
    min2 : upper 10 bits of the MIN binary representation

  RETURN VALUE
    Boolean whether the specified MIN is valid according to the binary MIN 
    encoding format.
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static boolean qmi_dmsi_minnum_validate
(
  dword  min1,
  word   min2
)
{

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if ( ( (   min1 & 0xFF000000 ) != 0 ) ||
       ( ( ( min1 & 0x00FFC000 ) >> 14 ) > 999 ) ||
       ( ( ( min1 & 0x00003C00 ) >> 10 ) > 10 ) ||
       ( ( ( min1 & 0x00003C00 ) >> 10 ) == 0 ) ||
       ( ( ( min1 & 0x000003FF )         > 999 ) ) 
     ||
       ( ( min2 & 0xFC00 ) != 0) || 
       ( ( min2 & 0x03FF)          > 999 )
     )
  {
    return FALSE;
  }
  
  return TRUE;
} /* qmi_dmsi_minnum_validate() */


/*===========================================================================
  FUNCTION QMI_DMSI_MINNUM_PCS_TO_TEXT()

  DESCRIPTION
    convert MIN NV format to ascii-text representation
    
  PARAMETERS
    dest : will contain the mobile number in ascii-text format
    min1 : lower 24 bits of the MIN binary representation
    min2 : upper 10 bits of the MIN binary representation

  RETURN VALUE
    length of ascii-text string that was created
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static int qmi_dmsi_minnum_pcs_to_text
(
  char *  dest, 
  dword  min1,
  word   min2
)
{
  byte * start;
  byte * end;
  dword   temp;
  boolean digit_one_zero;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(dest);

  start = end = (byte *) dest;

  /*-------------------------------------------------------------------------
    MIN binary format is as per JSTD-008 section 2.3.1.1, convert to BCD per
    spec
  -------------------------------------------------------------------------*/
  /* mask off and convert first three digits from MIN2 */
  temp = (min2 & 0x03FF);
  digit_one_zero = FALSE;
  /* add 100 and check for overflows from the 4th digit */
  temp += 100;
  if (temp > 999)
  {
    temp -= 1000;
    end[0] = (char) '0';
    end++;
    digit_one_zero = TRUE;
  }
  /* add 10 and check for overflows from the 5th digit */
  temp += 10;
  if ((temp % 10) == (temp % 100))
  {
    temp -= 100;
    if (digit_one_zero)
    {
    end[0] = (char) '0';
    end++;
    }
  }
  /* add 1 and check for overflows from the 6th digit */
  temp += 1;
  if (0 == (temp % 10))
  {
    temp -= 10;
  }
  end = dsatutil_itoa( temp, end, DMSI_BASE_DECIMAL);

  /* mask off and convert next three digits from MIN1 */
  temp = (min1 & 0x00FFC000) >> 14;
  digit_one_zero = FALSE;
  /* add 100 and check for overflows from the 4th digit */
  temp += 100;
  if (temp > 999)
  {
    temp -= 1000;
    end[0] = (char) '0';
    end++;
    digit_one_zero = TRUE;
  }
  /* add 10 and check for overflows from the 5th digit */
  temp += 10;
  if ((temp % 10) == (temp % 100))
  {
    temp -= 100;
    if (digit_one_zero)
    {
    end[0] = (char) '0';
    end++;
    }
  }
  /* add 1 and check for overflows from the 6th digit */
  temp += 1;
  if (0 == (temp % 10))
  {
    temp -= 10;
  }
  end = dsatutil_itoa( temp, end, DMSI_BASE_DECIMAL);

  /* mask off and convert next digit from MIN1 */
  temp = (min1 & 0x00003C00) >> 10;
  /* check for overflows from the 7th digit */
  if (temp == 10)
  {
    temp = 0;
  }
  end = dsatutil_itoa( temp, end, DMSI_BASE_DECIMAL);

  /* mask off and convert last three digits from MIN1 */
  temp = (min1 & 0x000003FF);
  digit_one_zero = FALSE;
  /* add 100 and check for overflows from the 4th digit */
  temp += 100;
  if (temp > 999)
  {
    temp -= 1000;
    end[0] = (char) '0';
    end++;
    digit_one_zero = TRUE;
  }
  /* add 10 and check for overflows from the 5th digit */
  temp += 10;
  if ((temp % 10) == (temp % 100))
  {
    temp -= 100;
    if (digit_one_zero)
    {
    end[0] = (char) '0';
    end++;
    }
  }
  /* add 1 and check for overflows from the 6th digit */
  temp += 1;
  if (0 == (temp % 10))
  {
    temp -= 10;
  }
  end = dsatutil_itoa( temp, end, DMSI_BASE_DECIMAL);

  /* return the length (end - start) given in number of bytes */
  return ((end - start) / sizeof(byte));
} /* qmi_dmsi_minnum_pcs_to_text() */

/*===========================================================================
  FUNCTION QMI_DMSI_MOBNUM_PCS_TO_TEXT()

  DESCRIPTION
    convert MDN NV format to ascii-text representation
    
  PARAMETERS
    dest : will contain the mobile number in ascii-text format
    src  : mobile number in NV format

  RETURN VALUE
    None
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static boolean  qmi_dmsi_mobnum_text_to_pcs
(
  char *                  src,
  uint8                   len,
  nv_mob_dir_data_type *  dest
)
{
  int i;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(dest && src);

  if ((len == 0) || (len > NV_DIR_NUMB_PCS_SIZ))
  {
  	LOG_MSG_INFO1_0("Invalid MDN length");
  	return FALSE;
  }
  else
  {
    dest->n_digits = len;
  }

  /*-------------------------------------------------------------------------
    MDN NV format is sort of like BCD, but 8 bits instead of 4, and 
    '0' is represented as 0x0A.
  -------------------------------------------------------------------------*/
  for (i=0; i < len; i++)
  {
    if ('0' == src[i]) 
    {
      dest->digitn[i] = 0x0A;
    } 
    else if ( (src[i] >= '1') || (src[i]  <= '9'))
    {
      dest->digitn[i] = (byte) src[i] - '0';
    } 
    else 
    {
      return FALSE; /* invalid num */
    }
  }

  return TRUE;
} /* qmi_dmsi_mobnum_text_to_pcs() */

/*===========================================================================
  FUNCTION QMI_DMSI_MIN_TEXT_TO_MIN12()

  DESCRIPTION
    convert MDN NV format to ascii-text representation
    
  PARAMETERS
    min_data : (IN)  poitner to text-format MIN
    len      : (IN)  length of text-format MIN
    min1_ptr : (OUT) pointer to binary-format MIN1
    min2_ptr : (OUT) pointer to binary-format MIN2

  RETURN VALUE
    None
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static boolean  qmi_dmsi_min_text_to_min12
(
  char *          min_data,
  uint8           len,
  dword *				  min1_ptr, 
  word *				  min2_ptr
)
{
  int       i;
  uint16    digit;
  char *    min_text;
  dword		  min1;
  word 		  min2;

  if ((NULL == min_data) || (NULL == min1_ptr) || (NULL == min2_ptr))
  {
    LOG_MSG_ERROR_0("NULL ptr passed");
    return FALSE;
  }

  min_text = min_data;  
  /* Validate MIN length and content */
  if(len != DMSI_MIN_SIZ)
  {
    LOG_MSG_ERROR_0("Invalid MIN length");
    return FALSE;
  }
  else
  {
    for (i=0; i<len; i++ ) 
    {
      if ( (*(min_text+i) <  '0') || (*(min_text+i) > '9') ) 
        return FALSE;
    }
  }

  // Encode the first three digits (IS-95A 6.3.1.1)
  min2 = 0;
  for (i = 0; i < 3; i++) 
  {
    digit = *min_text - '0';
    if (0 == digit) 
      digit = 10;
    min_text++;
    min2 = (uint16) (min2 * 10) + digit;
  }

  min2 -= 111;

  // Encode the last seven digits (IS-95A 6.3.1.1)

  // Encode the second three digits into the ten most
  // significant bits (of the 24-bit number)...
  min1 = 0;
  for (i = 0; i < 3; i++) 
  {
    digit = *min_text - (uint16) '0';
    if (0 == digit) 
      digit = 10;
    min_text++;
    min1 = (uint32) (min1 * 10) + digit;
  }

  min1 -= 111;

  min1 <<= 14;
  min1 &= 0x00FFC000;

  // The fourth last digit is mapping as BCD into four bits
  digit = *min_text - '0';
  if (0 == digit) 
    digit = 10;
  min_text++;

  min1 = min1 | (0x00003C00 & (digit << 10));

  // Encode the last three digits into the ten least significant bits
  {
    uint32 tmp = 0;
    for (i = 0; i < 3; i++) 
    {
      digit = *min_text - '0';
      if (0 == digit) 
        digit = 10;
      min_text++;

      tmp = (uint32) (tmp * 10) + digit;
    }

    tmp -= 111;
    tmp &= 0x000003FF;

    min1 |= tmp;
  }

  // Update the min1_ptr and min2_ptr
  *min1_ptr = min1;
  *min2_ptr = min2;
  return TRUE;
} /* qmi_dmsi_min_text_to_min12() */

/*===========================================================================
  FUNCTION QMI_DMSI_RESET_IMSI_COMPONENTS()

  DESCRIPTION
    Resets the IMSI components for restore factory operation
    
  PARAMETERS
    nam

  RETURN VALUE
    TRUE/FALSE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static boolean  qmi_dmsi_reset_imsi_components(byte nam)
{
  nv_item_type      *dcc_nv_item_ptr;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /*-------------------------------------------------------------------------
    Allocate temporary memory for the NV item
  -------------------------------------------------------------------------*/
  dcc_nv_item_ptr = (nv_item_type *)
    qmi_svc_ps_system_heap_mem_alloc(sizeof(nv_item_type),
                             FILE_ID_DS_QMI_DMS,__LINE__);
  if( dcc_nv_item_ptr == NULL )
  {
    return FALSE;
  }
  
  /* NV_IMSI_MCC_I*/
  memset(dcc_nv_item_ptr, 0, sizeof(nv_item_type));
  dcc_nv_item_ptr->imsi_mcc.nam = nam;
  dcc_nv_item_ptr->imsi_mcc.imsi_mcc = DMSI_IMSI_MCC_0;      
  if( NV_DONE_S != dcc_put_nv_item( NV_IMSI_MCC_I, dcc_nv_item_ptr ) )
  {
    PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_item_ptr);;
    return FALSE;
  }

  /* NV_IMSI_11_12_I */
  /* Default MNC is 00 in ASCII and 99 in min */
  memset(dcc_nv_item_ptr, 0, sizeof(nv_item_type));
  dcc_nv_item_ptr->imsi_11_12.nam = nam;
  dcc_nv_item_ptr->imsi_11_12.imsi_11_12 = DMSI_IMSI_11_12_0;  /* default to '00' */
  if( NV_DONE_S != dcc_put_nv_item( NV_IMSI_11_12_I, dcc_nv_item_ptr ) )
  {
    PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_item_ptr);;
    return FALSE;
  }

  /* NV_MIN2_I */
  memset(dcc_nv_item_ptr, 0, sizeof(nv_item_type));
  dcc_nv_item_ptr->min2.nam = nam;
  dcc_nv_item_ptr->min2.min2[DMSI_FMMIN] = DMSI_IMSI_S2_0 ;
  dcc_nv_item_ptr->min2.min2[DMSI_CDMAMIN] = DMSI_IMSI_S2_0 ;
  if( NV_DONE_S != dcc_put_nv_item( NV_MIN2_I, dcc_nv_item_ptr ) )
  {
    PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_item_ptr);;
    return FALSE;
  }

  /* NV_MIN1_I */
  memset(dcc_nv_item_ptr, 0, sizeof(nv_item_type));
  dcc_nv_item_ptr->min1.nam = nam;
  dcc_nv_item_ptr->min1.min1[DMSI_FMMIN] = DMSI_IMSI_S1_0 ;
  dcc_nv_item_ptr->min1.min1[DMSI_CDMAMIN] = DMSI_IMSI_S1_0;
  if( NV_DONE_S != dcc_put_nv_item( NV_MIN1_I, dcc_nv_item_ptr ) )
  {
    PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_item_ptr);;
    return FALSE;
  }

  /*-------------------------------------------------------------------------
    Free the temporary memory allocated for NV item.
  -------------------------------------------------------------------------*/
  PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_item_ptr);;
  
  /* All IMSI components reset as required here, return true */
  return TRUE;

}/* qmi_dmsi_reset_imsi_components() */

#ifdef FEATURE_IS95B_TRUE_IMSI
/*===========================================================================
  FUNCTION QMI_DMSI_RESET_IMSI_T_COMPONENTS()

  DESCRIPTION
    Resets the IMSI_T components for restore factory operation
    
  PARAMETERS
    nam

  RETURN VALUE
    TRUE/FALSE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static boolean  qmi_dmsi_reset_imsi_t_components(byte nam)
{
  nv_item_type      *dcc_nv_item_ptr;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /*-------------------------------------------------------------------------
    Allocate temporary memory for the NV item
  -------------------------------------------------------------------------*/
  dcc_nv_item_ptr = (nv_item_type *)
    qmi_svc_ps_system_heap_mem_alloc(sizeof(nv_item_type),FILE_ID_DS_QMI_DMS,__LINE__);
  if( dcc_nv_item_ptr == NULL )
  {
    return FALSE;
  }
  
  /* NV_IMSI_T_MCC_I */
  memset(dcc_nv_item_ptr, 0, sizeof(nv_item_type));
  /* Default MCC is 000 in ASCII and 999 in min */
  dcc_nv_item_ptr->imsi_t_mcc.nam = nam;
  dcc_nv_item_ptr->imsi_t_mcc.imsi_mcc = DMSI_IMSI_MCC_0;      /* default to '000' */
  if( NV_DONE_S != dcc_put_nv_item( NV_IMSI_T_MCC_I, dcc_nv_item_ptr ) )
  {
    PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_item_ptr);;
    return FALSE;
  }

  /* NV_IMSI_T_11_12_I */
  memset(dcc_nv_item_ptr, 0, sizeof(nv_item_type));
  /* Default MNC is 00 in ASCII and 99 in min */
  dcc_nv_item_ptr->imsi_t_11_12.nam = nam;
  dcc_nv_item_ptr->imsi_t_11_12.imsi_11_12 = DMSI_IMSI_11_12_0;  /* default to '00' */
  if( NV_DONE_S != dcc_put_nv_item( NV_IMSI_T_11_12_I, dcc_nv_item_ptr ) )
  {
    PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_item_ptr);;
    return FALSE;
  }

  /* NV_IMSI_T_S2_I */
  memset(dcc_nv_item_ptr, 0, sizeof(nv_item_type));
  /* Default S2 is 000 in ASCII and 999 in min */
  dcc_nv_item_ptr->imsi_t_s2.nam = nam;
  dcc_nv_item_ptr->imsi_t_s2.min2[DMSI_FMMIN] = DMSI_IMSI_S2_0 ;
  dcc_nv_item_ptr->imsi_t_s2.min2[DMSI_CDMAMIN] = DMSI_IMSI_S2_0 ;
  if( NV_DONE_S != dcc_put_nv_item( NV_IMSI_T_S2_I, dcc_nv_item_ptr ) )
  {
    PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_item_ptr);;
    return FALSE;
  }

  /* NV_IMSI_T_S1_I */
  memset(dcc_nv_item_ptr, 0, sizeof(nv_item_type));
  dcc_nv_item_ptr->imsi_t_s1.nam = nam;
  dcc_nv_item_ptr->imsi_t_s1.min1[DMSI_FMMIN] = DMSI_IMSI_S1_0 ;
  dcc_nv_item_ptr->imsi_t_s1.min1[DMSI_CDMAMIN] =  DMSI_IMSI_S1_0 ;
  if( NV_DONE_S != dcc_put_nv_item( NV_IMSI_T_S1_I, dcc_nv_item_ptr ) )
  {
    PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_item_ptr);;
    return FALSE;
  }

  /*-------------------------------------------------------------------------
    Free the temporary memory allocated for NV item.
  -------------------------------------------------------------------------*/
  PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_item_ptr);;

  /* All IMSI_T components reset as required here, return true */
  return TRUE;

}/* qmi_dmsi_reset_imsi_t_components() */
#endif /* def FEATURE_IS95B_TRUE_IMSI */


/*===========================================================================
  FUNCTION QMI_DMSI_CALCULATE_ACTIVATED_STATE()

  DESCRIPTION
    convert MDN NV format to ascii-text representation
    
  PARAMETERS
    dest : will contain the mobile number in ascii-text format
    src  : mobile number in NV format

  RETURN VALUE
    None
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static qmi_dmsi_activation_state_e_type qmi_dmsi_calculate_activated_state
(
  void
)
{
  qmi_dmsi_activation_state_e_type act_state;
  
  dword                            min1;
  dword                            min1_default;
  boolean                          otasp_state;
  nv_item_type                    *dcc_nv_item_ptr;
  nv_stat_enum_type                nv_status;
  cm_nam_e_type                    curr_nam;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  act_state = DMSI_ACTIVATION_STATE_MAX;
  
  otasp_state = FALSE;
  min1 = min1_default = 0;

  /*-------------------------------------------------------------------------
    Allocate temporary memory for the NV item
  -------------------------------------------------------------------------*/
  dcc_nv_item_ptr = (nv_item_type *)
    qmi_svc_ps_system_heap_mem_alloc(sizeof(nv_item_type),
                             FILE_ID_DS_QMI_DMS,__LINE__);
  if( dcc_nv_item_ptr == NULL )
  {
    return act_state;
  }

  /* see if build stores 'last' OTASP state to QMI NV item */
  if (qmi_dmsi_global.nv_use_last_otasp_act_state)
  {
    /* start with NOT_ACTIVATED in case 'last' NV not set */
    act_state = DMSI_ACTIVATION_STATE_NOT_ACTIVATED;
    if (QMI_NV_STATUS_OK == qmi_nv_read(QMI_NV_ITEM_LAST_OTASP_STATE, 0, 
                                        &otasp_state, 
                                        sizeof(otasp_state)))
    {
      /* use 'last' NV only if set and non-zero */
      if (otasp_state)
      {
        act_state = DMSI_ACTIVATION_STATE_ACTIVATED;
      }
    }
  }

  /* if act_state not set, use the ESN/MIN to determine */
  if (DMSI_ACTIVATION_STATE_MAX == act_state)
  {
    act_state = DMSI_ACTIVATION_STATE_NOT_ACTIVATED;
    if(NV_DONE_S == dcc_get_nv_item (NV_ESN_I, dcc_nv_item_ptr))
    {
      min1_default = qmi_dmsi_generate_default_min(dcc_nv_item_ptr->esn.esn);
    }
  
    nv_status = dcc_get_nv_item(NV_CURR_NAM_I, dcc_nv_item_ptr);  
    if(nv_status != NV_DONE_S || dcc_nv_item_ptr->curr_nam >= NV_MAX_NAMS)
    {
      /*-----------------------------------------------------------------------
        Default to the first NAM
      -----------------------------------------------------------------------*/
      dcc_nv_item_ptr->curr_nam = (byte) CM_NAM_1;
    }
    curr_nam = (cm_nam_e_type)dcc_nv_item_ptr->curr_nam;
    dcc_nv_item_ptr->min1.nam = curr_nam;

    if (NV_DONE_S == dcc_get_nv_item(NV_MIN1_I, dcc_nv_item_ptr))
    {
      min1 = dcc_nv_item_ptr->min1.min1[NV_CDMA_MIN_INDEX];
      if (qmi_dmsi_minnum_validate(min1, 0) && (min1 != min1_default))
      {
        act_state = DMSI_ACTIVATION_STATE_ACTIVATED;
      }
      else
      {
#ifdef FEATURE_IS95B_TRUE_IMSI
        dcc_nv_item_ptr->imsi_t_s1.nam = curr_nam;
        if (NV_DONE_S == dcc_get_nv_item(NV_IMSI_T_S1_I, dcc_nv_item_ptr))
        {
          min1 = dcc_nv_item_ptr->imsi_t_s1.min1[NV_CDMA_MIN_INDEX];
          if (qmi_dmsi_minnum_validate(min1, 0) && (min1 != min1_default))
          {
            act_state = DMSI_ACTIVATION_STATE_ACTIVATED;
          }
        }
#endif /* FEATURE_IS95B_TRUE_IMSI */
      }
    }
  }

  PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_item_ptr);

  return act_state;
} /* qmi_dmsi_calculate_activated_state */
#endif /* defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900) */
/*===========================================================================
  FUNCTION QMI_DMSI_BIND_SUBSCRIPTION()

  DESCRIPTION
    Binds user given subscription to this client

  PARAMETERS
    sp           : service state pointer
    cl_sp        : client state pointer
    cmd_buf_p    : the current command buffer
    sdu_in       : dsm_item containing incoming request

  RETURN VALUE
    ptr to response

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_bind_subscription
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;
  uint8    type;
  uint16   len;
  uint16   expected_len;
  uint32   subs_id;
  void *   value;
  qmi_dmsi_client_state_type *  dms_client_sp;
  boolean  got_v_in_reqd = FALSE;

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  response = NULL;
  errval = QMI_ERR_NONE;
  dms_client_sp = (qmi_dmsi_client_state_type *)cl_sp;

  while (*sdu_in)
  {
    if( !qmi_svc_get_tl( sdu_in, &type, &len ) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        expected_len = sizeof(subs_id);
        value = &subs_id;
        got_v_in_reqd = TRUE;
        break;

      default:
        MSG_MED( "Unrecognized TLV type (%d)",type,0,0);
        break;
    }

    if(!qmi_svc_validate_type_length( type,
                                      len,
                                      expected_len,
                                      FILE_ID_DS_QMI_DMS,__LINE__) )
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    /*-----------------------------------------------------------------------
      If type is unrecognized, value will be NULL, and qmi_svc_dsm_pullup will
      free the unrecognized value, i.e. we skip past unrecognized TLVs
    -----------------------------------------------------------------------*/
    if(len != qmi_svc_dsm_pullup( sdu_in, value, len ,FILE_ID_DS_QMI_DMS,__LINE__  ))
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  if(!got_v_in_reqd)
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

#ifndef FEATURE_DUAL_SIM
#define SUBS_ID_SECONDARY 2
  if(subs_id == SUBS_ID_SECONDARY)
  {
    errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
    goto send_result;
  }
#endif


#ifndef FEATURE_TRIPLE_SIM
#define SUBS_ID_TERTIARY 3
  if(subs_id == SUBS_ID_TERTIARY)
  {
    errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
    goto send_result;
  }
#endif

  if(!((subs_id < (DMSI_BIND_SUBS_MAX+1)) && (subs_id != 0)))
  {
    errval = QMI_ERR_INVALID_ARG;
    goto send_result;
  }

  dms_client_sp->subscription_id = (qmi_dmsi_client_subs_e_type)(subs_id - 1) ;

  send_result:
    result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS
                                     : QMI_RESULT_FAILURE);

    retval = qmi_svc_put_result_tlv(&response, result, errval);
    CHECK_RETVAL();

    return response;

} /* qmi_dmsi_bind_subscription */

/*===========================================================================
  FUNCTION QMI_DMSI_GET_BIND_SUBSCRIPTION()

  DESCRIPTION
    Returns already bound  subscription to this client

  PARAMETERS
    sp           : service state pointer
    cl_sp        : client state pointer
    cmd_buf_p    : the current command buffer
    sdu_in       : dsm_item containing incoming request

  RETURN VALUE
    ptr to response

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_get_bind_subscription
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval; 
  qmi_dmsi_client_state_type *  dms_client_sp;
  uint32 subscription_id;
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  
  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);
  
  response = NULL;
  errval = QMI_ERR_NONE;
  dms_client_sp = (qmi_dmsi_client_state_type *)cl_sp;
  
  if(  dms_client_sp->subscription_id >= DMSI_BIND_SUBS_MAX)
  {
    MSG_ERROR("Invalid subscription id stored for client %d",dms_client_sp->common.clid,0,0);
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }
  subscription_id = (dms_client_sp->subscription_id) + 1;
  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  if(FALSE == qmi_svc_put_param_tlv(&response, 
                                    DMSI_PRM_TYPE_BIND_SUBSCRIPTION,
                                    sizeof(subscription_id),
                                    &subscription_id))
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
  }
  
  send_result:
   result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                    : QMI_RESULT_FAILURE);
   retval = qmi_svc_put_result_tlv(&response, result, errval);
   CHECK_RETVAL();
       
   return response;

}/* qmi_dmsi_get_bind_subscription */

#ifdef FEATURE_MMGSDI_SESSION_LIB
#if defined(FEATURE_WCDMA) || defined(FEATURE_GSM) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA)
/*===========================================================================
  FUNCTION QMI_DMSI_GENERATE_GET_CK_STATUS_RESP()

  DESCRIPTION
    To send a response to GET_CK_STATUS requested by the client.
    
  PARAMETERS
    cnf_data : info about the Control Keys

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_dmsi_generate_get_ck_status_resp
(
  mmgsdi_session_perso_cnf_type * cnf_data
)
{
  qmi_dmsi_client_state_type *cl_sp;
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response = NULL;
  qmi_result_e_type       result = QMI_RESULT_SUCCESS;
  qmi_error_e_type        errval = QMI_ERR_NONE;
  mmgsdi_return_enum_type status;
  uint8                   feat = 0xff;
  uint8                   feat_blocked = 0;

  PACKED struct PACKED_POST
  {
    uint8 op_block;
  } v_out_block;

  PACKED struct PACKED_POST
  {
    uint8  feat_state;
    uint8  verify_retries;
    uint8  unblock_retries;
  } v_out_req;

  if (cnf_data == NULL)
  {
    LOG_MSG_INFO1_0( "Received NULL cnf_data in GET_CK_STATUS callback" );
    return;
  }
  
  status = cnf_data->response_header.mmgsdi_status;
  if (status != MMGSDI_SUCCESS)
  {
    errval = QMI_ERR_INTERNAL;
    result = QMI_RESULT_FAILURE;
    while( (cmd_buf_p = q_get(&qmi_dmsi_global.sim.get_ck_state_q)) != NULL )
    {
      response = NULL;
      if(FALSE == qmi_svc_put_result_tlv(&response, result, errval) ||
         FALSE == qmi_dmsi_send_response(cmd_buf_p, response))
      {
        LOG_MSG_INFO2_0 ("Unable to send response for GET_CK_STATUS operation");
        /*-----------------------------------------------------------------------
          Note - no need to free ind, taken care of in make_message()
          or send()
        -----------------------------------------------------------------------*/
        dsm_free_packet(&response);
        ds_qmi_fw_free_cmd_buf( &cmd_buf_p );
      }
    }
    memset( &qmi_dmsi_global.sim.perso_scratch, 0,
            sizeof(qmi_dmsi_global.sim.perso_scratch) );  
    return;
  }
  else
  {
    switch (cnf_data->perso_action)
    {
      case MMGSDI_PERSO_GET_FEATURE_IND:
        qmi_dmsi_global.sim.perso_scratch.perso_data[MMGSDI_PERSO_NW].state =
          cnf_data->perso_cnf_data.get_feature_ind_cnf.nw_ind_status;
        qmi_dmsi_global.sim.perso_scratch.perso_data[MMGSDI_PERSO_NS].state =
          cnf_data->perso_cnf_data.get_feature_ind_cnf.ns_ind_status;
        qmi_dmsi_global.sim.perso_scratch.perso_data[MMGSDI_PERSO_SP].state =
          cnf_data->perso_cnf_data.get_feature_ind_cnf.sp_ind_status;
        qmi_dmsi_global.sim.perso_scratch.perso_data[MMGSDI_PERSO_CP].state =
          cnf_data->perso_cnf_data.get_feature_ind_cnf.cp_ind_status;
        qmi_dmsi_global.sim.perso_scratch.perso_data[MMGSDI_PERSO_SIM].state =
          cnf_data->perso_cnf_data.get_feature_ind_cnf.sim_ind_status;
        qmi_dmsi_global.sim.perso_scratch.state_req_pending = FALSE;
        break;
      case MMGSDI_PERSO_GET_DCK_RETRIES:
        qmi_dmsi_global.sim.perso_scratch.perso_data[MMGSDI_PERSO_NW].verify_retries =
          (uint8)cnf_data->perso_cnf_data.dck_num_retries_cnf.nw_num_retries;
        qmi_dmsi_global.sim.perso_scratch.perso_data[MMGSDI_PERSO_NS].verify_retries =
          (uint8)cnf_data->perso_cnf_data.dck_num_retries_cnf.ns_num_retries;
        qmi_dmsi_global.sim.perso_scratch.perso_data[MMGSDI_PERSO_SP].verify_retries =
          (uint8)cnf_data->perso_cnf_data.dck_num_retries_cnf.sp_num_retries;
        qmi_dmsi_global.sim.perso_scratch.perso_data[MMGSDI_PERSO_CP].verify_retries =
          (uint8)cnf_data->perso_cnf_data.dck_num_retries_cnf.cp_num_retries;
        qmi_dmsi_global.sim.perso_scratch.perso_data[MMGSDI_PERSO_SIM].verify_retries =
          (uint8)cnf_data->perso_cnf_data.dck_num_retries_cnf.sim_num_retries;
        qmi_dmsi_global.sim.perso_scratch.verify_req_pending = FALSE;
        break;
      case MMGSDI_PERSO_GET_UNBLOCK_DCK_RETRIES:
        qmi_dmsi_global.sim.perso_scratch.perso_data[MMGSDI_PERSO_NW].unblock_retries =
          (uint8)cnf_data->perso_cnf_data.dck_num_retries_cnf.nw_num_retries;
        qmi_dmsi_global.sim.perso_scratch.perso_data[MMGSDI_PERSO_NS].unblock_retries =
          (uint8)cnf_data->perso_cnf_data.dck_num_retries_cnf.ns_num_retries;
        qmi_dmsi_global.sim.perso_scratch.perso_data[MMGSDI_PERSO_SP].unblock_retries =
          (uint8)cnf_data->perso_cnf_data.dck_num_retries_cnf.sp_num_retries;
        qmi_dmsi_global.sim.perso_scratch.perso_data[MMGSDI_PERSO_CP].unblock_retries =
          (uint8)cnf_data->perso_cnf_data.dck_num_retries_cnf.cp_num_retries;
        qmi_dmsi_global.sim.perso_scratch.perso_data[MMGSDI_PERSO_SIM].unblock_retries =
          (uint8)cnf_data->perso_cnf_data.dck_num_retries_cnf.sim_num_retries;
        qmi_dmsi_global.sim.perso_scratch.unblock_req_pending = FALSE;
        break;
      default:
        LOG_MSG_INFO1_1( "Recd unsolicited callback for perso action type %d",
                         (int)cnf_data->perso_action );
        return;
    }
    if ( qmi_dmsi_global.sim.perso_scratch.in_use &&
         !qmi_dmsi_global.sim.perso_scratch.state_req_pending &&
         !qmi_dmsi_global.sim.perso_scratch.verify_req_pending &&
         !qmi_dmsi_global.sim.perso_scratch.unblock_req_pending )
    {
      cl_sp = NULL;
      while( (cmd_buf_p = q_get(&qmi_dmsi_global.sim.get_ck_state_q)) != NULL )
      {
        response = NULL;
        cl_sp = (qmi_dmsi_client_state_type *)cmd_buf_p->x_p->cl_sp;
        feat = (uint8)cl_sp->get_ck_status_pending;
        if (feat > MMGSDI_PERSO_SIM)
        {
          LOG_MSG_ERROR_1("Recd unexpected callback for feature %d. Drop cmd_buf",feat);
          ds_qmi_fw_free_cmd_buf( &cmd_buf_p );
          /*reset global data, new command will be processed*/
          memset( &qmi_dmsi_global.sim.perso_scratch, 0,
                 sizeof(qmi_dmsi_global.sim.perso_scratch) );
          return;
        }
        feat_blocked = qmi_dmsi_global.sim.mmgsdi_perso.blocked_mask &
                       (1 << feat);
        if (feat_blocked != FALSE)
        {
          v_out_block.op_block = (uint8)TRUE;
        }
        v_out_req.verify_retries =
          qmi_dmsi_global.sim.perso_scratch.perso_data[feat].verify_retries;
        v_out_req.unblock_retries =
          qmi_dmsi_global.sim.perso_scratch.perso_data[feat].unblock_retries;
        v_out_req.feat_state =
          (v_out_req.verify_retries == 0)? DMSI_GET_CK_STATE_BLOCKED:
        qmi_dmsi_global.sim.perso_scratch.perso_data[feat].state;
        result = QMI_RESULT_SUCCESS;
        errval = QMI_ERR_NONE;

        LOG_MSG_INFO1_1 ("qmi_dmsi_generate_get_ck_status_resp -setting response %x to null",
                         response);
        response = NULL;

        if(((feat_blocked != FALSE) &&
            (FALSE == qmi_svc_put_param_tlv(&response,
                                            DMSI_PRM_TYPE_BLOCKING_OPERATION,
                                            sizeof(v_out_block),
                                            &v_out_block))) ||
           FALSE == qmi_svc_put_param_tlv(&response,
                                          QMI_TYPE_REQUIRED_PARAMETERS,
                                          sizeof(v_out_req),
                                          &v_out_req) ||
           FALSE == qmi_svc_put_result_tlv(&response, result, errval) ||
           FALSE == qmi_dmsi_send_response(cmd_buf_p, response))
        {
          LOG_MSG_INFO2_0 ("Unable to send response for GET_CK_STATUS operation");
          dsm_free_packet(&response);
          ds_qmi_fw_free_cmd_buf( &cmd_buf_p );
        }
      }
      memset( &qmi_dmsi_global.sim.perso_scratch, 0,
              sizeof(qmi_dmsi_global.sim.perso_scratch) );
      if (NULL != cl_sp)
      {
        cl_sp->get_ck_status_pending = MMGSDI_MAX_PERSO_FEATURE_ENUM;
      }
      return;
    }
    else
    {
      LOG_MSG_INFO1_0( "Waiting for other MMGSDI responses for GET_CK_STATE" );
      return;
    }
  }
} /* qmi_dmsi_generate_get_ck_status_resp */

/*===========================================================================
  FUNCTION QMI_DMSI_GENERATE_SET_CK_PROTECTION_RESP()

  DESCRIPTION
    To send a response to SET_CK_PROTECTION requested by the client.
    
  PARAMETERS
    cnf_data : response callback for the SET_CK_PROTECTION request

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_dmsi_generate_set_ck_protection_resp
(
  mmgsdi_session_perso_cnf_type *cnf_data
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response = NULL;
  qmi_result_e_type       result;
  qmi_error_e_type        errval;
  mmgsdi_return_enum_type status;
  PACKED struct PACKED_POST
  {
    uint8 retries_left;
  } v_out;

  if (cnf_data == NULL)
  {
    LOG_MSG_INFO1_0( "Received NULL cnf_data in SET_CK callback" );
    return;
  }

  cmd_buf_p = (qmi_cmd_buf_type*)cnf_data->response_header.client_data;
  if (cmd_buf_p == NULL)
  {
    LOG_MSG_INFO1_0( "Recd NULL cmd_buf in response to SET_CK" );
    return;
  }

  if( NULL == cmd_buf_p->x_p || NULL == cmd_buf_p->x_p->cl_sp )
  {
    LOG_MSG_INFO1_0("Transaction ptr or client state pointer is NULL, most likely client was released.");
    return;
  }

  status = cnf_data->response_header.mmgsdi_status;
  if (status != MMGSDI_SUCCESS)
  {
    result = QMI_RESULT_FAILURE;
    errval = qmi_if_map_mmgsdi_status_to_errval(status,__LINE__);
    v_out.retries_left =
      (uint8)cnf_data->perso_cnf_data.feature_data_cnf.num_retries;
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      DMSI_PRM_TYPE_CK_RETRY_STATUS,
                                      sizeof(v_out),
                                      &v_out))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
      goto send_result;
    }
  }
  else
  {
    result = QMI_RESULT_SUCCESS;
    errval = QMI_ERR_NONE;
  }
send_result:
  if(FALSE == qmi_svc_put_result_tlv(&response, result, errval) ||
     FALSE == qmi_dmsi_send_response(cmd_buf_p, response))
  {
    LOG_MSG_INFO2_0 ("Unable to send response for QMI read IMSI operation: %d ");
    /*-----------------------------------------------------------------------
      Note - no need to free ind, taken care of in make_message()
      or send()
    -----------------------------------------------------------------------*/
    dsm_free_packet(&response);
    ds_qmi_fw_free_cmd_buf( &cmd_buf_p );
  }
} /* qmi_dmsi_generate_set_ck_protection_resp */

/*===========================================================================
  FUNCTION QMI_DMSI_GENERATE_UNBLOCK_CK_RESP()

  DESCRIPTION
    To send a response to UNBLOCK_CK requested by the client.
    
  PARAMETERS
    cnf_data : callback data for the UNBLOCK_CK request

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_dmsi_generate_unblock_ck_resp
(
  mmgsdi_session_perso_cnf_type *cnf_data
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response = NULL;
  qmi_result_e_type       result;
  qmi_error_e_type        errval;
  mmgsdi_return_enum_type status;
  PACKED struct PACKED_POST
  {
    uint8 retries_left;
  } v_out;

  if (cnf_data == NULL)
  {
    LOG_MSG_INFO1_0( "Received NULL cnf_data in UNBLOCK_CK callback" );
    return;
  }

  cmd_buf_p = (qmi_cmd_buf_type*)cnf_data->response_header.client_data;
  if (cmd_buf_p == NULL)
  {
    LOG_MSG_INFO1_0( "Recd NULL cmd_buf in response to UNBLOCK_CK" );
    return;
  }

  if( NULL == cmd_buf_p->x_p || NULL == cmd_buf_p->x_p->cl_sp )
  {
    LOG_MSG_INFO1_0("Transaction ptr or client state pointer is NULL, most likely client was released.");
    return;
  }

  status = cnf_data->response_header.mmgsdi_status;
  if (status != MMGSDI_SUCCESS)
  {
    result = QMI_RESULT_FAILURE;
    errval = qmi_if_map_mmgsdi_status_to_errval(status,__LINE__);
    v_out.retries_left =
      (uint8)cnf_data->perso_cnf_data.feature_data_cnf.num_retries;
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      DMSI_PRM_TYPE_CK_RETRY_STATUS,
                                      sizeof(v_out),
                                      &v_out))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
      goto send_result;
    }
  }
  else
  {
    result = QMI_RESULT_SUCCESS;
    errval = QMI_ERR_NONE;
  }
send_result:
  if(FALSE == qmi_svc_put_result_tlv(&response, result, errval) ||
     FALSE == qmi_dmsi_send_response(cmd_buf_p, response))
  {
    LOG_MSG_INFO2_0 ("Unable to send response for QMI read IMSI operation: %d ");
    /*-----------------------------------------------------------------------
      Note - no need to free ind, taken care of in make_message()
      or send()
    -----------------------------------------------------------------------*/
    dsm_free_packet(&response);
    ds_qmi_fw_free_cmd_buf( &cmd_buf_p );
  }
} /* qmi_dmsi_generate_unblock_ck_resp */
#endif /* defined(FEATURE_WCDMA) || defined(FEATURE_GSM) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA) */

/*===========================================================================
  FUNCTION QMI_DMS_GENERATE_UIM_STATE_IND()

  DESCRIPTION
    Called when CM notifies us that the UIM state has changed.  Sends
    an indication with the new UIM state to clients registered for
    the event.
        
  PARAMETERS
    None
    
  RETURN VALUE
    None
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_dms_generate_uim_state_ind
(
  void
)
{
  qmi_dmsi_state_type *           dms_sp;
  qmi_dmsi_client_state_type *    cl_sp;
  int                             j;
  
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  dms_sp = (qmi_dmsi_state_type*)&qmi_dms_state;

    for( j = 0; j < DMSI_MAX_CLIDS ; j++ )
    {
      cl_sp = dms_sp->client[j];
      if( cl_sp && (cl_sp->common.clid != QMI_SVC_CLID_UNUSED) && 
          cl_sp->report_status.report_uim_state)
      {
        qmi_dmsi_event_report_ind(dms_sp,
                                  cl_sp->common.clid,
                                  DMSI_REPORT_STATUS_EV_UIM_GET_STATE,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0);
      }
    }
} /* qmi_dms_generate_uim_state_ind() */

#endif /* FEATURE_MMGSDI_SESSION_LIB */


/*===========================================================================
  FUNCTION QMI_DMSI_OPRT_MODE_FROM_CM()

  DESCRIPTION
    Maps a CM operating mode enum to a QMI DMS operating mode enum.
    
  PARAMETERS
    cm_oprt_mode  [in]
    dms_oprt_mode [out]

  RETURN VALUE
    QMI_ERR_NONE on success, QMI_ERR_INTERNAL if CM operating mode is
    unrecognized.
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static qmi_error_e_type qmi_dmsi_oprt_mode_from_cm
(
  sys_oprt_mode_e_type        cm_oprt_mode,
  qmi_dmsi_oprt_mode_e_type  *dms_oprt_mode
)
{
  qmi_error_e_type   errval;
  nv_stat_enum_type  nv_status;
  nv_item_type      *dcc_nv_item_ptr;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------------
    Allocate temporary memory for the NV item
  -------------------------------------------------------------------------*/
  dcc_nv_item_ptr = (nv_item_type *)
    qmi_svc_ps_system_heap_mem_alloc(sizeof(nv_item_type),
                               FILE_ID_DS_QMI_DMS,__LINE__);
  if( dcc_nv_item_ptr == NULL )
  {
    return QMI_ERR_NO_MEMORY;
  }

  errval = QMI_ERR_NONE;
  switch (cm_oprt_mode)
  {
    case SYS_OPRT_MODE_NONE:
      errval = QMI_ERR_INFO_UNAVAILABLE;
      break;

    case SYS_OPRT_MODE_PWROFF:
      *dms_oprt_mode = DMSI_OPRT_MODE_PWROFF;
      break;
  
    case SYS_OPRT_MODE_FTM:
      *dms_oprt_mode = DMSI_OPRT_MODE_FTM;
      break;
  
    case SYS_OPRT_MODE_OFFLINE:
    case SYS_OPRT_MODE_OFFLINE_AMPS:
    case SYS_OPRT_MODE_OFFLINE_CDMA:
      *dms_oprt_mode = DMSI_OPRT_MODE_OFFLINE;
      break;

    case SYS_OPRT_MODE_ONLINE:
      *dms_oprt_mode = DMSI_OPRT_MODE_ONLINE;
      break;
  
    case SYS_OPRT_MODE_LPM:
      *dms_oprt_mode = DMSI_OPRT_MODE_LPM;
      break;

    case SYS_OPRT_MODE_RESET:
      *dms_oprt_mode = DMSI_OPRT_MODE_RESET;
      break;

    case SYS_OPRT_MODE_NET_TEST_GW:
      *dms_oprt_mode = DMSI_OPRT_MODE_NET_TEST_GW;
      break;

    default:
      LOG_MSG_INFO2_1("Operating mode unknown (%d), can't convert to QMI DMS oprt mode",
                      cm_oprt_mode);
      errval = QMI_ERR_INTERNAL;
      break;
  }

  /*-------------------------------------------------------------------------
    Check if the device is actually in P-LPM instead of LPM
  -------------------------------------------------------------------------*/
  if (DMSI_OPRT_MODE_LPM == *dms_oprt_mode)
  {
    /* Read the nv for LPM persistence flag */
    nv_status = dcc_get_nv_item(NV_LPM_PERSISTENCE_I, dcc_nv_item_ptr);
    if(NV_DONE_S == nv_status)
    {
      /* P-LPM flag set, read the value */
      if (dcc_nv_item_ptr->lpm_persistence)
      {
        *dms_oprt_mode = DMSI_OPRT_MODE_PLPM;
      }
    }
  } 

  PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_item_ptr);
  return errval;
} /* qmi_dmsi_oprt_mode_from_cm() */

#ifdef FEATURE_MMGSDI_SESSION_LIB
/*===========================================================================
  FUNCTION QMI_DMSI_GET_UIM_STATUS()

  DESCRIPTION
    Returns the status of the UIM based on the values cached from earlier
    MMGSDI events.

  PARAMETERS
    None

  RETURN VALUE
    QMI_ERR_NONE: UIM present and unlocked
    QMI_ERR_SIM_NOT_INITIALIZED: If the SIM is not initialized yet
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
qmi_error_e_type qmi_dmsi_get_uim_status
(
  void
)
{
  qmi_if_info_type  *qmi_if_info_ptr;

  qmi_if_info_ptr = qmi_if_info_get_ptr();
  LOG_MSG_INFO2_2("qmi_dmsi_get_uim_status(): app_type=0x%x, uim_state=0x%x",
                  qmi_if_info_ptr->session_info[QMI_IF_MMGSD_GW_PROV_PRI_SESSION].app_type,
                  qmi_dmsi_global.uim_state);

  switch (qmi_dmsi_global.uim_state)
  {
    case DMSI_SIM_INIT_SUCCESS: // i.e. subscription ready ev occured
      return QMI_ERR_NONE;

    case DMSI_SIM_NOT_INSERTED: // i.e. sim not present
    case DMSI_SIM_UNKNOWN: // i.e. no mmgsdi event yet
      return QMI_ERR_SIM_NOT_INITIALIZED;

    default:
      return QMI_ERR_NONE;
  }
} /* qmi_dmsi_get_uim_status */

/*===========================================================================
  FUNCTION QMI_DMSI_GET_UIM_STATUS_EXT()

  DESCRIPTION
    Returns the status of the UIM based on the values fetched from mmgsdi module.

  PARAMETERS
    None

  RETURN VALUE
    QMI_ERR_NONE: UIM present and unlocked
    QMI_ERR_SIM_NOT_INITIALIZED: If the SIM is not initialized yet
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
qmi_error_e_type qmi_dms_get_uim_access_status_ext
(
   uint8 session_type
)
{
  mmgsdi_session_info_query_type    session_info_query;
  mmgsdi_session_get_info_type      session_get_info_ptr;
  mmgsdi_return_enum_type  mmgsdi_status;
  session_info_query.query_type = MMGSDI_SESSION_INFO_QUERY_BY_TYPE;
  session_info_query.query_key.session_type = (mmgsdi_session_type_enum_type)session_type;
  
  mmgsdi_status = mmgsdi_session_get_info_sync(session_info_query, &session_get_info_ptr);
  
  switch(mmgsdi_status)
  {
    case MMGSDI_SUCCESS:
      switch(session_get_info_ptr.app_state)
      {
        case MMGSDI_APP_STATE_READY:
          return QMI_ERR_NONE;
        default:
          return QMI_ERR_SIM_NOT_INITIALIZED;
      }
    default:
      return QMI_ERR_SIM_NOT_INITIALIZED;
  }      
}
    

/*===========================================================================
  FUNCTION QMI_DMS_GET_UIM_ACCESS_STATUS()

  DESCRIPTION
    Returns the status of the UIM for read/write of PIN1 protected values based
    on the values cached from earlier MMGSDI events.  

  PARAMETERS
    None

  RETURN VALUE
    QMI_ERR_NONE: UIM present and unlocked
    QMI_ERR_SIM_NOT_INITIALIZED: If the UIM is not initialized yet
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
qmi_error_e_type qmi_dms_get_uim_access_status
(
  void
)
{
  qmi_error_e_type errval = QMI_ERR_INTERNAL;

  LOG_MSG_INFO2_0("qmi_dms_get_uim_access_status()");

  errval = qmi_dmsi_get_uim_status();
  if (QMI_ERR_NONE == errval)
  {
      if (DMSI_SIM_INIT_FAIL_OR_PROGRESS == qmi_dmsi_global.uim_state)
      {
        /*UIM not ready to read/write*/
        errval = QMI_ERR_SIM_NOT_INITIALIZED;
      }
  }

  return errval;
} /* qmi_dms_get_uim_access_status */

/*===========================================================================
  FUNCTION QMI_DMS_GET_PIN1_STATUS()

  DESCRIPTION
    Returns the status of PIN1 from value cached from earlier MMGSDI events.

  PARAMETERS
    None

  RETURN VALUE
    Current PIN1 status
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
qmi_pin_status_enum_type qmi_dms_get_pin1_status
(
  void
)
{

  LOG_MSG_INFO2_0("qmi_dms_get_pin1_status()");

  return qmi_dmsi_global.sim.pin_info[MMGSDI_PIN1].status;

} /* qmi_dms_get_pin1_status */

#endif /* FEATURE_MMGSDI_SESSION_LIB */

/*===========================================================================
  FUNCTION QMI_DMS_GENERATE_PRL_INIT_IND()

  DESCRIPTION
    Called when CM notifies us that the PRL is loaded onto device. Sends
    an indication with the PRL init TLV.
        
  PARAMETERS
    None
    
  RETURN VALUE
    None
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void qmi_dms_generate_prl_init_ind
(
  void
)
{
  qmi_dmsi_state_type *           dms_sp;
  qmi_dmsi_client_state_type *    cl_sp;
  int                             j;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  dms_sp = (qmi_dmsi_state_type*)&qmi_dms_state;
    for( j = 0; j < DMSI_MAX_CLIDS ; j++ )
    {
      cl_sp = dms_sp->client[j];
      if( cl_sp && (cl_sp->common.clid != QMI_SVC_CLID_UNUSED) && 
          cl_sp->report_status.report_prl_init)
      {
        LOG_MSG_INFO1_1("Report PRL init to client (%d)", cl_sp->common.clid);
        qmi_dmsi_event_report_ind(dms_sp,
                                  cl_sp->common.clid,
                                  DMSI_REPORT_STATUS_EV_PRL_INIT,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0);
      }
    }
} /* qmi_dms_generate_prl_init_ind() */

/*===========================================================================
  FUNCTION QMI_DMSI_SET_TEST_CONFIG_REQ()

  DESCRIPTION
    Set TDSCDMA test config request
        
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_set_test_config_req
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response = NULL;
  qmi_error_e_type   errval = QMI_ERR_NONE;
  qmi_result_e_type  result;
  boolean            retval;
  
  PACKED struct PACKED_POST
  {
    uint32  tds_config;
  } v_in_optional;

  boolean  got_v_in_optional = FALSE;
  uint8    type;
  uint16   len;
  void *   value;
  uint16   expected_len;
#ifdef FEATURE_TDSCDMA
  tds_rrc_set_config_req_type set_config_req;
#endif
  
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);
 
  memset((void *)&v_in_optional, 0, sizeof(v_in_optional));
 
  while (*sdu_in)
  {
    if( !qmi_svc_get_tl( sdu_in, &type, &len ) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case DMSI_PRM_TYPE_TDSCDMA_CONFIG:
        expected_len = sizeof(v_in_optional);
        got_v_in_optional = TRUE;
        value = (void *) &v_in_optional;
        break;

     default:
        LOG_MSG_INFO2_1 ("qmi_dmsi_set_test_config_req(): "
                         "Unrecognized TLV type (%d)", type);
        break;
    }

    if(!qmi_svc_validate_type_length( type,
                                      len,
                                      expected_len,
                                      FILE_ID_DS_QMI_DMS,__LINE__) )
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    /*-----------------------------------------------------------------------
      If type is unrecognized, value will be NULL, and qmi_svc_dsm_pullup will 
      free the unrecognized value, i.e. we skip past unrecognized TLVs
    -----------------------------------------------------------------------*/
    if(len != qmi_svc_dsm_pullup( sdu_in, value, len ,FILE_ID_DS_QMI_DMS,__LINE__  ))
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  if (FALSE == got_v_in_optional)
  {
    errval = QMI_ERR_MISSING_ARG;
    LOG_MSG_ERROR_0("qmi_dmsi_set_test_config_req(): "
                    "Didnt get the optional message to send to MSGRT");
    goto send_result;
  }

#ifdef FEATURE_TDSCDMA
  /*-------------------------------------------------------------------------
    Populate the local structure with the values coming in the request and
    also validate the values
  -------------------------------------------------------------------------*/
  LOG_MSG_INFO1_0("qmi_dmsi_set_test_config_req(): "
                  "dms Set TDSCDMA config req");

  switch (v_in_optional.tds_config)
  {
    case DMSI_TEST_CONFIG_TDS_FIELD:
      set_config_req.config = TDS_CONFIG_FIELD;
      break;
    case DMSI_TEST_CONFIG_TDS_LAB:
      set_config_req.config = TDS_CONFIG_LAB;
      break;
    case DMSI_TEST_CONFIG_TDS_USER:
      set_config_req.config = TDS_CONFIG_USER;
      break;
    default:
    {
      /* Any other value is considered invalid */
      errval = QMI_ERR_INVALID_ARG;
      LOG_MSG_ERROR_1("qmi_dmsi_set_test_config_req(): "
                      "Invalid Set request config value, %d ",
                      v_in_optional.tds_config);
      goto send_result;
      break;
    }
  }

  
  /* Now call the MSGRT send routine */
  retval = dsmsgrsnd_msg_send(
            TDSCDMA_RRC_SET_CONFIG_REQ, 
            (msgr_hdr_struct_type*)(&set_config_req),
            sizeof(tds_rrc_set_config_req_type)
         );

  if (retval == FALSE)
  {
    LOG_MSG_ERROR_0("qmi_dmsi_set_test_config_req(): "
                    "Unable to send message to MSGRT");
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  q_put(&(qmi_dms_state.set_tdscdma_config_pending_q),
        &(((qmi_cmd_buf_type *)cmd_buf_p)->link));
#else
  errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
#endif

send_result:
  if (errval == QMI_ERR_NONE)
  {
    response = (dsm_item_type*) QMI_SVC_RESPONSE_PENDING; 
  }
  else
  {
    result = QMI_RESULT_FAILURE;
    retval = qmi_svc_put_result_tlv(&response, result, errval);

    CHECK_RETVAL();
  }
  
  return response;

} /* qmi_dmsi_set_test_config_req() */


/*===========================================================================
  FUNCTION QMI_DMSI_GET_TEST_CONFIG_REQ()

  DESCRIPTION
    Get TDSCDMA test config request
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_get_test_config_req
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *             response = NULL;
  qmi_error_e_type            errval = QMI_ERR_NONE;
  qmi_result_e_type           result;
  boolean                     retval;
#ifdef FEATURE_TDSCDMA
  tds_rrc_get_config_req_type get_config_req;
#endif

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);
  
  /*-------------------------------------------------------------------------
    Perform query action
  -------------------------------------------------------------------------*/
  LOG_MSG_INFO1_0("qmi_dmsi_get_test_config_req(): "
                  "dms query tdscdma test get config");

#ifdef FEATURE_TDSCDMA
  memset(&get_config_req, 0, sizeof(tds_rrc_get_config_req_type));

  retval = dsmsgrsnd_msg_send(
            TDSCDMA_RRC_GET_CONFIG_REQ, 
            (msgr_hdr_struct_type*)(&get_config_req),
            sizeof(tds_rrc_get_config_req_type)
         );

  if(retval == FALSE)
  {
    LOG_MSG_ERROR_0("qmi_dmsi_get_test_config_req(): "
                    "Unable to send message to DSMSGR");
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  q_put(&(qmi_dms_state.get_tdscdma_config_pending_q),
            &(((qmi_cmd_buf_type *)cmd_buf_p)->link));

send_result:
  if (errval == QMI_ERR_NONE)
  {
    result = QMI_RESULT_SUCCESS;
    response = (dsm_item_type*) QMI_SVC_RESPONSE_PENDING; 
  }
  else
  {
    result = QMI_RESULT_FAILURE;
    retval = qmi_svc_put_result_tlv(&response, result, errval);

    CHECK_RETVAL();
  }
#else
  errval = QMI_ERR_NONE;
  result = QMI_RESULT_SUCCESS;
  retval = qmi_svc_put_result_tlv(&response, result, errval);

  CHECK_RETVAL();
#endif

  return response;
 
} /* qmi_dmsi_get_test_config_req() */

/*===========================================================================
  FUNCTION QMI_DMSI_GET_OEM_CHINA_OPERATOR()

  DESCRIPTION
    Gives the operator info of the device configured by OEM.
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type*  qmi_dmsi_get_oem_china_operator
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
) 

{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval; 

 #define OPERATOR_F_NAME \
                           (char*)"/nv/item_files/modem/mmode/operator_name"
  int efs_ret;
  struct fs_stat temp_buf;
  uint8 buf ;
  uint32 oem_china_operator;
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  
  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);
  
  response = NULL;
  errval = QMI_ERR_NONE;

  /*EFS SUCCESS == 0*/
  if((efs_stat (OPERATOR_F_NAME, &temp_buf) != 0))
  {
    errval = QMI_ERR_INFO_UNAVAILABLE;
    goto send_result;
  }
  else  
  {
    efs_ret = efs_get(OPERATOR_F_NAME, &buf, sizeof(uint8));

    if (efs_ret == -1)
    {
      errval = QMI_ERR_NOT_PROVISIONED;
      goto send_result;
    }
    
    /* The actual NV values start from 0 (OPERATOR_NONE but since we are only 
       reporting CT/CU/CMCC + 1 is needed here.
    */
    if((buf >=  DMS_OPERATOR_MAX + 1) || (buf < DMS_OPERATOR_CT + 1))
    {
      errval = QMI_ERR_INFO_UNAVAILABLE;
      goto send_result;
    }
  
    oem_china_operator = buf - 1;
  }
  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  if(FALSE == qmi_svc_put_param_tlv(&response, 
                                    DMSI_PRM_TYPE_OPERATOR,
                                    sizeof(uint32),
                                    &oem_china_operator))
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
  }
  
  send_result:
   result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                    : QMI_RESULT_FAILURE);
   retval = qmi_svc_put_result_tlv(&response, result, errval);
   CHECK_RETVAL();
       
   return response;

}/* qmi_dmsi_get_oem_china_operator */

/*===========================================================================
  FUNCTION QMI_DMSI_GET_MAC_ADDRESS()

  DESCRIPTION
    Get the MAC address configured in NV for a device

  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_get_mac_address
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *        response;
  uint8                  type;
  uint16                 len;
  uint16                 expected_len;
  void *                 value;

  PACKED struct PACKED_POST
  {
    uint32  device_type;
  } v_in_required;

  PACKED struct PACKED_POST
  {
    uint8  mac_addr_len;
    uint8 mac_buf[DMSI_MAC_ADDR_LEN];
  } v_out;
    
  boolean                   got_v_in_required;
  qmi_error_e_type          errval;
  qmi_result_e_type         result;
  boolean                   retval;
  nv_stat_enum_type  nv_status;
  uint8 index ;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  memset(&v_in_required, 0, sizeof(v_in_required));

  response          = NULL;
  errval            = QMI_ERR_NONE;
  value             = NULL;
  got_v_in_required = FALSE;

  while (*sdu_in)
  {
    if( !qmi_svc_get_tl( sdu_in, &type, &len ) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        expected_len = sizeof(v_in_required);
        value = &v_in_required;
        got_v_in_required = TRUE;
        break;

      default:
        LOG_MSG_INFO2_1 ("Unrecognized TLV type (%d)",type);
        break;
    }

    if(!qmi_svc_validate_type_length( type,
                                      len,
                                      expected_len,
                                      FILE_ID_DS_QMI_DMS,__LINE__) )
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    if(len != qmi_svc_dsm_pullup( sdu_in, value, len ,FILE_ID_DS_QMI_DMS,__LINE__  ))
    {
      /*---------------------------------------------------------------------
        Mandatory TLV is invalid so in result send error
      ---------------------------------------------------------------------*/
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  if(!got_v_in_required)
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  if( DMS_DEVICE_MAX <= v_in_required.device_type )
  {
    errval = QMI_ERR_INVALID_ARG;
    goto send_result;
  }

  memset((void *)&dcc_nv_item, 0, sizeof(dcc_nv_item));

  v_out.mac_addr_len = DMSI_MAC_ADDR_LEN;
  
  if( DMS_DEVICE_MAC_WLAN == v_in_required.device_type )
  {
    nv_status = dcc_get_nv_item(NV_WLAN_MAC_ADDRESS_I, &dcc_nv_item);
    
    if(nv_status == NV_DONE_S)
    {
      for(index =0; index < DMSI_MAC_ADDR_LEN; index++)
      {
        v_out.mac_buf[index] = dcc_nv_item.wlan_mac_address[index];
      }
    }
    else
    {
      errval = QMI_ERR_NOT_PROVISIONED;
      goto send_result;
    }
  }
  else
  {
    nv_status = dcc_get_nv_item(NV_BD_ADDR_I, &dcc_nv_item);

    if(nv_status == NV_DONE_S)
    {
      for(index =0; index < DMSI_MAC_ADDR_LEN; index++)
      {
        v_out.mac_buf[index] = dcc_nv_item.bd_addr.bd_addr[index];
      }
    }
    else
    {
      errval = QMI_ERR_NOT_PROVISIONED;
      goto send_result;
    }
  }
   
  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  if(FALSE == qmi_svc_put_param_tlv(&response, 
                                    DMSI_PRM_TYPE_MAC_ADDR,
                                    sizeof(v_out),
                                    &v_out))
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
  }

send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS
                                   : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();

  return response;

} /* qmi_dmsi_get_mac_address() */

#ifdef FEATURE_TDSCDMA
/*===========================================================================
  FUNCTION QMI_DMSI_GENERATE_GET_TEST_CONFIG_RESP()

  DESCRIPTION
    Called from get config callback routine. Sends response to the caller 
        
  PARAMETERS
    config_resp   : GET response data
    
  RETURN VALUE
    None
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void qmi_dmsi_generate_get_test_config_resp
(
  tdscdma_get_config_res_type *config_resp
)
{
  dsm_item_type *             response = NULL;
  qmi_cmd_buf_type *          cmd_buf_p;
  qmi_error_e_type            errval = QMI_ERR_NONE;
  qmi_result_e_type           result;
  boolean                     retval;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  PACKED struct PACKED_POST
  {
    uint32   val;
  } v_out_config;

  ASSERT(config_resp);

  cmd_buf_p = q_get(&(qmi_dms_state.get_tdscdma_config_pending_q));
  LOG_MSG_INFO1_1("qmi_dmsi_generate_get_test_config_resp(): cmd_buf ptr = %x",
                   ((cmd_buf_p != NULL)?cmd_buf_p:0));
  if(cmd_buf_p == NULL)
  {
    return;
  }
  
  if( NULL == cmd_buf_p->x_p || NULL == cmd_buf_p->x_p->cl_sp )
  {
    LOG_MSG_INFO1_0("qmi_dmsi_generate_get_test_config_resp(): "
                    "Transaction ptr or client state pointer is NULL, "
                    "most likely client was released.");
    return;
  }

  /*-------------------------------------------------------------------------
    GET TDSCDMA config response
  -------------------------------------------------------------------------*/

  errval = qmi_dmsi_tdscdma_get_config_val(config_resp->desired_config,
                                           &(v_out_config.val));
  if (errval == QMI_ERR_INTERNAL)
  {
    LOG_MSG_INFO1_1("qmi_dmsi_generate_get_test_config_resp(): "
                    "Invalid desired config value, desired config: %d",
                    config_resp->desired_config);
    goto send_result;
  }

  /* Prepare the TLV */
  if( FALSE == qmi_svc_put_param_tlv(&response, 
                                     DMSI_PRM_TYPE_GET_TDSCDMA_DESIRED_CONFIG,
                                     sizeof(v_out_config),
                                     &v_out_config ) )
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
    goto send_result;
  }

  errval = qmi_dmsi_tdscdma_get_config_val(config_resp->active_config,
                                           &(v_out_config.val));
  if (errval == QMI_ERR_INTERNAL)
  {
    LOG_MSG_INFO1_1("qmi_dmsi_generate_get_test_config_resp(): "
                    "Invalid active config value, active config: %d",
                    config_resp->active_config);
    dsm_free_packet(&response);
    goto send_result;
  }

  if (FALSE == qmi_svc_put_param_tlv(&response, 
                                     DMSI_PRM_TYPE_GET_TDSCDMA_ACTIVE_CONFIG,
                                     sizeof(v_out_config),
                                     &v_out_config ) )
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
  }

send_result: 
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);
  retval = qmi_svc_put_result_tlv(&response, result, errval);

  if (FALSE == retval)
  {
    dsm_free_packet(&response);
    ds_qmi_fw_free_cmd_buf( &cmd_buf_p );
    return;
  }

  if (FALSE == qmi_dmsi_send_response( cmd_buf_p, response ))
  {
    LOG_MSG_INFO2_0 ("qmi_dmsi_generate_get_test_config_resp(): "
                     "Unable to send TDSCDMA Get config response");
    dsm_free_packet(&response);
    ds_qmi_fw_free_cmd_buf(&cmd_buf_p);
  }
  
} /* qmi_dms_generate_get_tdscdma_config_resp() */

/*===========================================================================
  FUNCTION      QMI_DMSI_TDSCDMA_GET_CONFIG_VAL()

  DESCRIPTION
    Mapping function for TDSCDMA GET/SET test config values
        
  PARAMETERS
    config      Config value to be mapped and return as DMSI enum
    val         Pointer to store the return value
    
  RETURN VALUE
    None
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
qmi_error_e_type qmi_dmsi_tdscdma_get_config_val
(
   tds_rrc_config_e_type config, uint32 *val
)
{
  switch (config)
  {
    case TDS_CONFIG_FIELD:
      *val = DMSI_TEST_CONFIG_TDS_FIELD;
      break;
    case TDS_CONFIG_LAB:
      *val = DMSI_TEST_CONFIG_TDS_LAB;
      break;
    case TDS_CONFIG_USER:
      *val = DMSI_TEST_CONFIG_TDS_USER;
      break;
    default:
    {
      /* Any other value is considered invalid */
      LOG_MSG_INFO1_1 ("qmi_dmsi_tdscdma_get_config_val(): "
                       "Invalid GET TDSCDMA config value, config value: %d ",
                       config);
      return QMI_ERR_INTERNAL;
      break;
    }
  }

  return QMI_ERR_NONE;
}

/*===========================================================================
  FUNCTION QMI_DMSI_GENERATE_SET_TEST_CONFIG_RESP()

  DESCRIPTION
    Called from SET TDSCDMA config callback routine.
    Sends response to the caller  for the corresponding TDSCDMA Set config
    request
        
  PARAMETERS
    config_resp : TDSCDMA config response
    
  RETURN VALUE  
    None
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void qmi_dmsi_generate_set_test_config_resp
(
  tdscdma_set_config_res_type * config_resp
)
{

  dsm_item_type *        response = NULL;
  qmi_cmd_buf_type *     cmd_buf_p;
  qmi_error_e_type       errval = QMI_ERR_NONE;
  qmi_result_e_type      result;
  boolean                retval;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(config_resp);

  cmd_buf_p = q_get(&(qmi_dms_state.set_tdscdma_config_pending_q));
 
  LOG_MSG_INFO1_1("qmi_dmsi_generate_set_test_config_resp(): cmd_buf ptr = %x",
                   ((cmd_buf_p != NULL)?cmd_buf_p:0));

  if(cmd_buf_p == NULL)
  {
    return;
  }
  
  if( NULL == cmd_buf_p->x_p || NULL == cmd_buf_p->x_p->cl_sp )
  {
    LOG_MSG_INFO1_0("Transaction ptr or client state pointer is NULL, "
                    "most likely client was released.");
    return;
  }

  /*-------------------------------------------------------------------------
    Set TDSCDMA set config response
  -------------------------------------------------------------------------*/
  if (config_resp->status != TRUE)
  {
    LOG_MSG_ERROR_0("qmi_dmsi_generate_set_test_config_resp(): "
                    "QMI TDSDCMDA Set response invalid value %d ");
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }
  
send_result: 
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  
  if(FALSE == retval)
  {
    dsm_free_packet(&response);
    ds_qmi_fw_free_cmd_buf( &cmd_buf_p );
    return;
  }

  if ( FALSE == qmi_dmsi_send_response( cmd_buf_p, response ) )
  {
    LOG_MSG_INFO2_0 ("Unable to send TDSCDMA Set config response");
    dsm_free_packet(&response);
    ds_qmi_fw_free_cmd_buf(&cmd_buf_p);
  }

} /* qmi_dms_generate_set_tdscdma_config_resp() */


/*===========================================================================
  FUNCTION QMI_DMSI_TDSCDMA_CONFIG_INIT()

  DESCRIPTION
    This function initializes TDSCDMA Get and Set response queues

  PARAMETERS
    None

  RETURN VALUE
    None

  DEPENDENCIES
    QMI DMS must already have been initialized and registered with Framework

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_dmsi_tdscdma_config_init
(
  void
)
{
  qmi_dmsi_state_type *         dms_sp;

  LOG_MSG_INFO2_0( "qmi_dmsi TDSCDMA init routine " );
  dms_sp = (qmi_dmsi_state_type*)&qmi_dms_state;
  
  /* Initialize tdscdma Get and Set response queues */
  q_init( &(dms_sp->get_tdscdma_config_pending_q) );
  q_init( &(dms_sp->set_tdscdma_config_pending_q) );
}
#endif

/*===========================================================================
  FUNCTION qmi_dmsi_policyman_config_init()

  DESCRIPTION  Init function to register to the MESSAGE ROUTER for policyman 
               indication
   
  PARAMETERS
    None

  RETURN VALUE
    None

  DEPENDENCIES
    
  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_dmsi_policyman_config_init (void)
{

   /*Register to the message router for policyman indicaiton*/
   dsmsgrrcv_reg_hdlr(POLICYMAN_CFG_UPDATE_MSIM_IND,
                      (dsmsgrcv_msg_hdlr_f) qmi_dmsi_msgr_hdlr );
}

/*===========================================================================
  FUNCTION qmi_dmsi_msgr_hdlr()

  DESCRIPTION  call back registered for message router
   
  PARAMETERS
     msgrtype  - message id requested
     dsmsg     - changed data

  RETURN VALUE
    Boolean    - true, if message is handled correctly
               - false, if not

  DEPENDENCIES
    none

  SIDE EFFECTS
    None
===========================================================================*/
boolean qmi_dmsi_msgr_hdlr 
(
  msgr_umid_type             msgrtype,
  const msgr_hdr_struct_type *dsmsg
)
{
  qmi_dmsi_cmd_buf_type *  cmd_ptr = NULL;
  boolean result = FALSE;  
  
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  LOG_MSG_INFO1_1("qmi_dmsi_msgr_hdlr  with msgr type = [%d]",msgrtype);
  switch (msgrtype) 
  {
    case  POLICYMAN_CFG_UPDATE_MSIM_IND:
       /*post a command to DCC*/
       /*Ignore payload here as info will be queried*/
       /*release the payload as it has reference in policyman*/
       policyman_msgr_attachments_release((msgr_hdr_s*)dsmsg);
        cmd_ptr = (qmi_dmsi_cmd_buf_type *)qmi_dmsi_get_cmd_buf(QMI_CMD_DMS_DEV_CURRENT_CAP_CB);
        if( cmd_ptr == NULL)
        {
          result = FALSE;
          break;
        }
        memset(cmd_ptr, 0, sizeof(qmi_dmsi_cmd_buf_type));
        cmd_ptr->cmd_id = QMI_CMD_DMS_DEV_CURRENT_CAP_CB;
        dcc_send_cmd_ex(DCC_QMI_CMD, cmd_ptr);
        result = TRUE;
      break;
    default:
      LOG_MSG_ERROR_1("Not supported msgr type %d", (int)msgrtype);
  }
  return result;
}

/*===========================================================================
  FUNCTION qmi_dms_device_mode_changed()

  DESCRIPTION  Device mode is changed and checks if an indication needs to be
               sent to clients.
    Note: Indication will be sent only in case of single sim SVLTE config
          Rest of the things will be taken care of Policyman indications
  PARAMETERS
    none
  RETURN VALUE
    none
  DEPENDENCIES
    none
  SIDE EFFECTS
    None
===========================================================================*/
void qmi_dms_device_mode_changed
(
  void
)
{
  uint32               max_subs = 0;
  uint32               max_active = 0;
  qmi_error_e_type errval = QMI_ERR_NONE;
  policyman_status_t   status = -1; 
  policyman_item_t const *cm_pm_device_config = NULL;
  policyman_item_id_t id1[] = { POLICYMAN_ITEM_DEVICE_CONFIGURATION };
  sys_subs_feature_t      subs_device_feature_mode;  

   /*Get the policyman items intially*/
  status = policyman_get_items(
              &id1[0],
              sizeof(id1)/sizeof(policyman_item_id_t),
              &cm_pm_device_config);
  if (POLICYMAN_FAILED(status) || (cm_pm_device_config == NULL) )
  {
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  status = 
    policyman_device_config_num_sims(cm_pm_device_config,
                                     (size_t*)&max_subs);
  if (POLICYMAN_FAILED(status) || (max_subs < SYS_MODEM_AS_ID_1) || 
      (max_subs > SYS_MODEM_AS_ID_3))
  {
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

   /*Indication will be sent only in case of single sim SVLTE config
     Rest of the things will be taken care of Policyman indications*/
  if(max_subs != 1)
  {
    goto send_result;
  }

  status = policyman_device_config_get_subs_feature(cm_pm_device_config,0,
                                                     SYS_MODEM_AS_ID_1,
                                                     &subs_device_feature_mode);
  if (POLICYMAN_FAILED(status))
  { 
    errval = QMI_ERR_INTERNAL;
    goto send_result;    
  }
  
  if(subs_device_feature_mode == SYS_SUBS_FEATURE_MODE_SVLTE)
  {
    qmi_dmsi_generate_current_device_capability_ind();
  }

  send_result:
   policyman_item_release(cm_pm_device_config);
   if(errval != QMI_ERR_NONE)
   {
     LOG_MSG_ERROR_2("qmi_dms_device_mode_changed: failed to send IND errval:%d status: 0x%x",
                      errval, status);
   }
   return;  
}

/*===========================================================================
  FUNCTION qmi_dmsi_generate_current_device_capability_ind()

  DESCRIPTION  Generate the Event report indicaiton for curr dev cap
   
  PARAMETERS
    None

  RETURN VALUE
    None

  DEPENDENCIES
    
  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_dmsi_generate_current_device_capability_ind ( void )
{
  qmi_dmsi_state_type *           dms_sp;
  qmi_dmsi_client_state_type *    cl_sp;
  int                             itr_clids;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  LOG_MSG_INFO2_0("qmi_dmsi_generate_current_device_capability_ind()");

  dms_sp = (qmi_dmsi_state_type *)&qmi_dms_state;

  /* Send the indication for all the registered clients */
  for( itr_clids = 0; itr_clids < DMSI_MAX_CLIDS ; itr_clids++ )
  {
    cl_sp = dms_sp->client[itr_clids];
    if( cl_sp && (cl_sp->common.clid != QMI_SVC_CLID_UNUSED) && 
        cl_sp->report_status.report_device_mode)
    {
      qmi_dmsi_event_report_ind(dms_sp,
                                cl_sp->common.clid,
                                DMSI_REPORT_STATUS_EV_DEV_MSIM_CAP,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0);
    }
  }
}

/*===========================================================================
  FUNCTION qmi_dmsi_get_device_multisim_current_cap_info()

  DESCRIPTION - fill all the tlvs related to curr dev cap in given resp pointer
    
  PARAMETERS
     response - dsm_item for response
      uint8   - resp_type, IND or RESP
 
  RETURN VALUE
   qmi_error_e_type   - error encountered while filling tlv

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static qmi_error_e_type qmi_dmsi_get_device_multisim_current_cap_info
(
  dsm_item_type ** response,
  uint8 resp_type
)
{
  policyman_status_t  status = -1; 
  policyman_item_t const *cm_policyman_device_config = NULL;
  policyman_item_id_t id1[] = { POLICYMAN_ITEM_DEVICE_CONFIGURATION };
  uint32             max_subs = 0;
  uint32             max_active = 0;
  sys_subs_feature_t      subs_device_feature_mode = 0; 
  qmi_if_info_type       *qmi_if_info_ptr = NULL;
  const policyman_item_collection_t *cm_policyman_dev_curr_cap = NULL;
  const policyman_item_t *pItem = NULL;
  policyman_item_id_t id2[] = { POLICYMAN_ITEM_RAT_CAPABILITY, POLICYMAN_ITEM_UE_MODE };
  uint32             curr_rat_tmp = 0;
  sys_ue_mode_e_type dev_vd_cap_tmp = 0;
  sys_modem_as_id_e_type sub_id;
  int              numitems = 0;
  int i = 0;
  uint32 total_size = 0;

  qmi_error_e_type   errval = QMI_ERR_NONE;
  uint8 type,tlv1,tlv2,tlv3;

  struct
  {
    uint8 max_subs;
    uint8 max_active;
  } v_dev_curr_cap;
  
  typedef struct
  {
    uint64 curr_rat_cap;
  }curr_rat_cap_type;
  
  typedef struct
  {
    uint32 dev_voice_data_cap;
    boolean simul_voice_data_cap;
  }dev_voice_data_cap_type;
  
  dev_voice_data_cap_type  v_dev_voice_data_cap[QMI_DMS_POLICYMAN_MAX_SUBS];
  curr_rat_cap_type        v_dev_curr_rat_cap[QMI_DMS_POLICYMAN_MAX_SUBS];
  
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    
    LOG_MSG_INFO1_1("qmi_dmsi_get_device_multisim_current_cap_info resp_type:%d ",resp_type);
  
    memset(&v_dev_curr_cap,0x00,sizeof(v_dev_curr_cap));
    status = policyman_get_items(
                  &id1[0],
                  sizeof(id1)/sizeof(policyman_item_id_t),
                  &cm_policyman_device_config
                  );
    if (POLICYMAN_FAILED(status))
    {
      errval = QMI_ERR_INTERNAL;
      goto send_result;
    }
  
    status = 
      policyman_device_config_num_sims(cm_policyman_device_config,(size_t*)&max_subs);
    if (POLICYMAN_FAILED(status))
    {
      errval = QMI_ERR_INTERNAL;
      goto send_result;
    }
    status = policyman_device_config_max_active(cm_policyman_device_config, (size_t*)&max_active);
    if (POLICYMAN_FAILED(status))
    {
      errval = QMI_ERR_INTERNAL;
      goto send_result;
    }
    
    v_dev_curr_cap.max_subs   = (uint8)max_subs;
    v_dev_curr_cap.max_active = (uint8)max_active;
  
  memset(&v_dev_curr_rat_cap,0x0,sizeof(v_dev_curr_rat_cap));
  memset(&v_dev_voice_data_cap,0x0,sizeof(v_dev_voice_data_cap));
  
  /*Get current RAT Capability and UE mode per subs*/
  status = policyman_get_items_msim(id2,
                            sizeof(id2)/sizeof(policyman_item_id_t),
                            &cm_policyman_dev_curr_cap);
  if ( POLICYMAN_FAILED(status) || (cm_policyman_dev_curr_cap == NULL) || (cm_policyman_dev_curr_cap->numItems == 0))
   {
     errval = QMI_ERR_INTERNAL;
     goto send_result;
   }

  /*check for the number of items it should be max_subs * items asked*/

  status = -1;
  numitems = cm_policyman_dev_curr_cap->numItems;

  LOG_MSG_INFO2_3("policyman_get_items_msim returned num_items:%d, max_sub:%d, max_active: %d", 
                   numitems, max_subs, max_active);

  for (i=0; i < numitems; i++)
  {
    pItem = (policyman_item_t *)cm_policyman_dev_curr_cap->pItems[i];

    switch (policyman_item_get_id_msim(pItem,&sub_id))
    {
      case POLICYMAN_ITEM_RAT_CAPABILITY:

        status = policyman_get_rat_capability( pItem,
                                            &curr_rat_tmp);
        if ( POLICYMAN_FAILED(status) || (sub_id < SYS_MODEM_AS_ID_1) || 
            (sub_id > SYS_MODEM_AS_ID_3))
        {
          errval = QMI_ERR_INTERNAL;
          goto send_result;
        }

        v_dev_curr_rat_cap[sub_id].curr_rat_cap = qmi_dmsi_convert_rat_mask(curr_rat_tmp);
       break;

     case POLICYMAN_ITEM_UE_MODE:

       status = policyman_get_ue_mode( pItem,
                                       &dev_vd_cap_tmp);
     if ( POLICYMAN_FAILED(status) || (sub_id < SYS_MODEM_AS_ID_1) || 
          (sub_id > SYS_MODEM_AS_ID_3))
       {
         errval = QMI_ERR_INTERNAL;
         goto send_result;
       }
       /*Convert to QMI notations*/
       /*Use new API from policyman to fill this*/
       v_dev_voice_data_cap[sub_id].simul_voice_data_cap = policyman_is_svd_operation_allowed(sub_id);
       v_dev_voice_data_cap[sub_id].dev_voice_data_cap = qmi_dmsi_is_simul_voice_data(dev_vd_cap_tmp);
      break;

     default:
        LOG_MSG_ERROR_1("Invalid policyman item: %d",i);
      break;
    }/*switch*/
  }/*for*/
  /*Policyman dont handle single sim SVLTE case. Get info from CM and update ue_mode*/
   if(max_subs == 1)
   {
     status = policyman_device_config_get_subs_feature(cm_policyman_device_config,0,
                                                     SYS_MODEM_AS_ID_1,
                                                     &subs_device_feature_mode);
     if (POLICYMAN_FAILED(status))
     {
       errval = QMI_ERR_INTERNAL;
       goto send_result;
     }
     if(subs_device_feature_mode == SYS_SUBS_FEATURE_MODE_SVLTE)
     {
       qmi_if_info_ptr = qmi_if_info_get_ptr();
       /*if hybrid stack is opertational it is SVLTE else CSFB*/
       if(qmi_if_info_ptr->ss_info[SYS_MODEM_AS_ID_1].is_hybr_stack_operational)
       {
         v_dev_voice_data_cap[SYS_MODEM_AS_ID_1].dev_voice_data_cap = 
                                           qmi_dmsi_is_simul_voice_data(SYS_UE_MODE_SVLTE);
       }
       else
       {
         v_dev_voice_data_cap[SYS_MODEM_AS_ID_1].dev_voice_data_cap = 
                                          qmi_dmsi_is_simul_voice_data(SYS_UE_MODE_CSFB);
       }
     }
     LOG_MSG_INFO1_2("Single Sim case: Subs feature mode:%d and UE_Mode: %d ",
                      subs_device_feature_mode, 
                      v_dev_voice_data_cap[SYS_MODEM_AS_ID_1].dev_voice_data_cap);
   }

  if (resp_type == QMI_DMS_DEVICE_CAP_INDICATION)
  {
    tlv1 = DMSI_PRM_TYPE_IND_CURRENT_DEVICE_CAPABILITY;
    tlv2 = DMSI_PRM_TYPE_IND_CURRENT_MSIM_SUBS_CAPABILITY;
    tlv3 = DMSI_PRM_TYPE_IND_VOICE_DATA_SUBS_CAPABILITY;
  }
  else
  {
    tlv1 = DMSI_PRM_TYPE_CURRENT_DEVICE_CAPABILITY;
    tlv2 = DMSI_PRM_TYPE_CURRENT_MSIM_SUBS_CAPABILITY;
    tlv3 = DMSI_PRM_TYPE_VOICE_DATA_SUBS_CAPABILITY;
  }

  /*Send all the info, first put tlv3, next tlv2 and then tlv1*/

  /*---TLV3---*/
  type = tlv3;
  total_size = max_subs * (sizeof(uint32) + sizeof(uint8)) + sizeof(uint8);

  for(i = max_subs-1; i>=0; i--)
  {  /*simul voice and data info*/
    if(!(QMI_SVC_PKT_PUSH((dsm_item_type **)response,&v_dev_voice_data_cap[i].simul_voice_data_cap,sizeof(uint8))) ||
       !(QMI_SVC_PKT_PUSH((dsm_item_type **)response,&v_dev_voice_data_cap[i].dev_voice_data_cap,sizeof(uint32))))
    {
      HANDLE_FAILURE();
    }
  }
  /*no of above instances pushed*/
  if(!(QMI_SVC_PKT_PUSH((dsm_item_type **)response,&max_subs,sizeof(uint8))))
  {
    HANDLE_FAILURE();
  }
  if( !(QMI_SVC_PKT_PUSH((dsm_item_type **)response, &total_size, sizeof(uint16))) ||
      !(QMI_SVC_PKT_PUSH((dsm_item_type **)response, &type, sizeof(uint8))) )
  {
    HANDLE_FAILURE();
  }
  
  /*---TLV2---*/
  type = tlv2;
  total_size = max_subs * sizeof(curr_rat_cap_type) + sizeof(uint8);
  
  for(i = max_subs-1; i>=0; i--)
  { /*current device cap maskinfo*/
    if(!(QMI_SVC_PKT_PUSH((dsm_item_type **)response,&v_dev_curr_rat_cap[i],sizeof(curr_rat_cap_type))))
    {
      HANDLE_FAILURE();
    }
  }
  /*no of above instances pushed*/
  if(!(QMI_SVC_PKT_PUSH((dsm_item_type **)response,&max_subs,sizeof(uint8))))
  {
    HANDLE_FAILURE();
  }
  if( !(QMI_SVC_PKT_PUSH((dsm_item_type **)response, &total_size, sizeof(uint16))) ||
      !(QMI_SVC_PKT_PUSH((dsm_item_type **)response, &type, sizeof(uint8))) )
  {
    HANDLE_FAILURE();
  }

  /*---TLV1---*/
  type = tlv1;
  total_size = 2 * sizeof(uint8);
  /*device configuration of subs*/
  if(!(QMI_SVC_PKT_PUSH((dsm_item_type **)response,&v_dev_curr_cap.max_active, sizeof(uint8))) ||
     !(QMI_SVC_PKT_PUSH((dsm_item_type **)response,&v_dev_curr_cap.max_subs, sizeof(uint8))))
  {
      HANDLE_FAILURE();
  }
  if( !(QMI_SVC_PKT_PUSH((dsm_item_type **)response, &total_size, sizeof(uint16))) ||
      !(QMI_SVC_PKT_PUSH((dsm_item_type **)response, &type, sizeof(uint8))) )
  {
    HANDLE_FAILURE(); 
  }

  send_result:

  /*release the policyman items after use*/
  if(cm_policyman_device_config != NULL)
  {
    policyman_item_release(cm_policyman_device_config);
  }
  if(cm_policyman_dev_curr_cap != NULL)
  {
    policyman_item_collection_release(cm_policyman_dev_curr_cap);
  }

  if(errval != QMI_ERR_NONE)
  {
    LOG_MSG_ERROR_2("qmi_dmsi_get_device_multisim_current_cap_info: Failed to get info.errval:%d Policyman. status:0x%x",
                    errval,
                    status);
  }

  return errval;

}

/*===========================================================================
  FUNCTION qmi_dmsi_is_simul_voice_data()

  DESCRIPTION  - Convert policyman ue_mode to QMI enum
    
  PARAMETERS
     ue_mode - policyman enum for ue_mode

  RETURN VALUE
     uint32 - qmi enum for ue_mode
   
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static uint32 qmi_dmsi_is_simul_voice_data 
(
   sys_ue_mode_e_type ue_mode
)
{
  qmi_dmsi_subs_voice_data_capability_type vd_cap;

  switch(ue_mode)
  {
    case SYS_UE_MODE_NONE:
    case SYS_UE_MODE_NORMAL:
        vd_cap = DMS_SUBS_VOICE_DATA_CAPABILITY_NORMAL;
      break;

    case SYS_UE_MODE_SVLTE:
    case SYS_UE_MODE_SVLTE_ONLY:
        vd_cap = DMS_SUBS_VOICE_DATA_CAPABILITY_SVLTE;
      break;

    case SYS_UE_MODE_SGLTE:
    case SYS_UE_MODE_SGLTE_ONLY:
        vd_cap = DMS_SUBS_VOICE_DATA_CAPABILITY_SGLTE;
      break;

    case SYS_UE_MODE_CSFB:
    case SYS_UE_MODE_CSFB_ONLY:
        vd_cap = DMS_SUBS_VOICE_DATA_CAPABILITY_CSFB;
      break;

    case SYS_UE_MODE_1XSRLTE_ONLY:
    case SYS_UE_MODE_1XSRLTE_CSFB_PREFERRED:
    case SYS_UE_MODE_GSMSRLTE_ONLY:
    case SYS_UE_MODE_GSMSRLTE_CSFB_PREFERRED:
        vd_cap =  DMS_SUBS_VOICE_DATA_CAPABILITY_SRLTE;
      break;

    default:
        vd_cap = DMS_SUBS_VOICE_DATA_CAPABILITY_NORMAL;
      break;
  }

  return (uint32)vd_cap;

}

/*===========================================================================
  FUNCTION qmi_dmsi_convert_rat_mask()

  DESCRIPTION  - convert rat mask from policyman to QMI rat mask
    
  PARAMETERS
    rat  - policyman rat mask

  RETURN VALUE
   uint64 - QMI rat mask

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static uint64 qmi_dmsi_convert_rat_mask 
(
   uint32 rat
)
{
  uint64 rat_mask;
  rat_mask = DMSI_SUBS_CAPABILITY_NONE; 

  if(rat & SYS_SYS_MODE_MASK_CDMA)
  {
    rat_mask |= DMSI_SUBS_CAPABILITY_CDMA;
  }
  if(rat & SYS_SYS_MODE_MASK_HDR)
  {
    rat_mask |= DMSI_SUBS_CAPABILITY_HDR;
  }
  if(rat & SYS_SYS_MODE_MASK_GSM) 
  {
    rat_mask |= DMSI_SUBS_CAPABILITY_GSM;
  }
  if(rat & SYS_SYS_MODE_MASK_WCDMA)
  {
    rat_mask |= DMSI_SUBS_CAPABILITY_WCDMA;
  }
  if(rat & SYS_SYS_MODE_MASK_LTE)
  {
    rat_mask |= DMSI_SUBS_CAPABILITY_LTE;
  }
  if(rat & SYS_SYS_MODE_MASK_TDS)
  {
    rat_mask |= DMSI_SUBS_CAPABILITY_TDS;
  }

  return rat_mask;

}

/*===========================================================================
  FUNCTION QMI_DMSI_INIT_CB()

  DESCRIPTION
  Function gets called from framework whenever the QMI DMS service registers with
  framework. This function posts a DCC command for the service to handle this
  callback.
    
  PARAMETERS
  num_instances : getting Num of QMI instances at run time.

  RETURN VALUE
  None
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_dmsi_init_cb
(
  uint16 num_instances
)
{
  qmi_dmsi_cmd_buf_type *cmd_ptr;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  cmd_ptr = (qmi_dmsi_cmd_buf_type *)qmi_dmsi_get_cmd_buf(QMI_CMD_DMS_INIT_CB);
  if( cmd_ptr == NULL)
  {
    return;
  }

  memset(cmd_ptr, 0, sizeof(qmi_dmsi_cmd_buf_type));
  cmd_ptr->cmd_id = QMI_CMD_DMS_INIT_CB;
  cmd_ptr->data.init_cb.num_instances = num_instances;

  dcc_send_cmd_ex(DCC_QMI_CMD, cmd_ptr);

} /* qmi_dmsi_init_cb() */

/*===========================================================================
FUNCTION QMI_DMSI_ALLOC_CLID_CB()

DESCRIPTION
  Function gets called from framework whenever a clid is allocated by the
  framework for QMI DMS service. This function posts a DCC command for the
  service to handle this callback.

PARAMETERS
  common_msg_hdr  : QMI Framework common msg hdr

RETURN VALUE
  Boolean : TRUE if the clid set properly
          : FALSE, otherwise
DEPENDENCIES
  None

SIDE EFFECTS
  None
===========================================================================*/
static boolean qmi_dmsi_alloc_clid_cb
(  
  qmi_framework_common_msg_hdr_type*   common_msg_hdr  
)
  {
  qmi_dmsi_cmd_buf_type *cmd_ptr;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(common_msg_hdr);

  LOG_MSG_INFO1_2("QMI DMS alloc clid cb: clid:%d, QMI Instances:%d",
                  common_msg_hdr->client_id, common_msg_hdr->qmi_instance);

  cmd_ptr = (qmi_dmsi_cmd_buf_type *)qmi_dmsi_get_cmd_buf(QMI_CMD_DMS_ALLOC_CLID_CB);
  if( cmd_ptr == NULL)
    {
    return FALSE;
    }

  /*-------------------------------------------------------------------------
    copy the framework common message header to local struct
  -------------------------------------------------------------------------*/
  memset(cmd_ptr, 0, sizeof(qmi_dmsi_cmd_buf_type));
  cmd_ptr->cmd_id = QMI_CMD_DMS_ALLOC_CLID_CB;
  cmd_ptr->data.clid_cb.common_msg_hdr.service = common_msg_hdr->service;
  cmd_ptr->data.clid_cb.common_msg_hdr.client_id = common_msg_hdr->client_id;
  cmd_ptr->data.clid_cb.common_msg_hdr.transaction_id = common_msg_hdr->transaction_id;
  cmd_ptr->data.clid_cb.common_msg_hdr.qmi_instance = common_msg_hdr->qmi_instance;

  dcc_send_cmd_ex(DCC_QMI_CMD, cmd_ptr);
  return TRUE;
} /* qmi_dmsi_alloc_clid_cb */

/*===========================================================================
FUNCTION QMI_DMSI_DEALLOC_CLID_CB()

DESCRIPTION 
  Function gets called from framework whenever a clid is deallocated by the
  framework for QMI DMS service. This function posts a DCC command for the
  service to handle this callback.

PARAMETERS
  common_msg_hdr  : QMI Framework common msg hdr

RETURN VALUE
  None
 
DEPENDENCIES
  None

SIDE EFFECTS
  None
===========================================================================*/
static void qmi_dmsi_dealloc_clid_cb
(  
  qmi_framework_common_msg_hdr_type*   common_msg_hdr  
)
{
  qmi_dmsi_cmd_buf_type *cmd_ptr;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(common_msg_hdr);
  LOG_MSG_INFO1_2("QMI DMS dealloc clid cb: clid:%d, QMI Instances:%d",
                  common_msg_hdr->client_id, common_msg_hdr->qmi_instance);

  cmd_ptr = (qmi_dmsi_cmd_buf_type *)qmi_dmsi_get_cmd_buf(QMI_CMD_DMS_DEALLOC_CLID_CB);
  if( cmd_ptr == NULL)
  {
    return;
  }

  /*-------------------------------------------------------------------------
    copy the framework common message header to local struct
  -------------------------------------------------------------------------*/
  memset(cmd_ptr, 0, sizeof(qmi_dmsi_cmd_buf_type));
  cmd_ptr->cmd_id = QMI_CMD_DMS_DEALLOC_CLID_CB;
  cmd_ptr->data.clid_cb.common_msg_hdr = *common_msg_hdr;
  cmd_ptr->data.clid_cb.common_msg_hdr.client_id = common_msg_hdr->client_id;
  cmd_ptr->data.clid_cb.common_msg_hdr.transaction_id = common_msg_hdr->transaction_id;
  cmd_ptr->data.clid_cb.common_msg_hdr.qmi_instance = common_msg_hdr->qmi_instance;

  /*-------------------------------------------------------------------------
    Positing command to QMI Service task to handle the clid callback
  -------------------------------------------------------------------------*/
  dcc_send_cmd_ex(DCC_QMI_CMD, cmd_ptr);
} /* qmi_dmsi_dealloc_clid_cb */

/*===========================================================================
  FUNCTION FRAMEWORK_DMSI_CMD_HDLR_CB()

  DESCRIPTION
    Function gets called from framework whenever a new QMI DMS request
    message is received. This function posts a DCC command for the
    service to handle this callback.

  PARAMETERS
    msg_hdr  :  msg hdr
    sdu : dsm item
 
  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void  qmi_dmsi_cmd_hdlr_cb
( 
  qmi_framework_msg_hdr_type * msg_hdr,
  dsm_item_type             ** sdu 
)
{
  qmi_dmsi_cmd_buf_type *cmd_ptr;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(msg_hdr);
  LOG_MSG_INFO1_3("qmi_dmsi_cmd_hdlr_cb: clid:%d, QMI Instances:%d, tx_id: %d",
                  msg_hdr->common_hdr.client_id,
                  msg_hdr->common_hdr.qmi_instance,
                  msg_hdr->common_hdr.transaction_id);
  
  cmd_ptr = (qmi_dmsi_cmd_buf_type *)qmi_dmsi_get_cmd_buf(QMI_CMD_DMS_CMD_HDLR_CB);
  if( cmd_ptr == NULL)
  {
    return;
  }

  /*-------------------------------------------------------------------------
    copy the framework message header to local struct
  -------------------------------------------------------------------------*/
  memset(cmd_ptr, 0, sizeof(qmi_dmsi_cmd_buf_type));
  cmd_ptr->cmd_id = QMI_CMD_DMS_CMD_HDLR_CB;
  cmd_ptr->data.cmd_hdlr_cb.msg_hdr.common_hdr.service = 
                                        msg_hdr->common_hdr.service;
  cmd_ptr->data.cmd_hdlr_cb.msg_hdr.common_hdr.client_id = 
                                        msg_hdr->common_hdr.client_id;
  cmd_ptr->data.cmd_hdlr_cb.msg_hdr.common_hdr.transaction_id = 
                                        msg_hdr->common_hdr.transaction_id;
  cmd_ptr->data.cmd_hdlr_cb.msg_hdr.common_hdr.qmi_instance = 
                                        msg_hdr->common_hdr.qmi_instance;
  cmd_ptr->data.cmd_hdlr_cb.msg_hdr.msg_ctl_flag = msg_hdr->msg_ctl_flag; 
  cmd_ptr->data.cmd_hdlr_cb.msg_hdr.msg_len = msg_hdr->msg_len; 
  cmd_ptr->data.cmd_hdlr_cb.sdu_in = *sdu;

  /*-------------------------------------------------------------------------
    Positing command to QMI Service task to handle the service request
  -------------------------------------------------------------------------*/
  dcc_send_cmd_ex(DCC_QMI_CMD, cmd_ptr);
} /* qmi_dmsi_cmd_hdlr_cb */

/*===========================================================================
FUNCTION QMI_DMSI_ALLOC_CL_SP()

  DESCRIPTION
  This function gets called whenever a client state need to be allocated.
  The client state has allocated and deallocated dynamically at runtime.
    
  PARAMETERS
  clid: client ID

  RETURN VALUE
  qmi_dmsi_client_state_type * : ptr to QMI DMS client state.

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static qmi_dmsi_client_state_type * qmi_dmsi_alloc_cl_sp
(
  uint8 clid
)
{
  qmi_dmsi_client_state_type *  cl_sp = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_INFO2_1( "qmi_dmsi_alloc_cl_sp clid :%d", clid );

  if ((clid == QMI_SVC_CLID_UNUSED ) ||
      (clid > DMSI_MAX_CLIDS ))
  {
    return cl_sp;
  }
  cl_sp = qmi_svc_ps_system_heap_mem_alloc(sizeof( qmi_dmsi_client_state_type ),
                                       FILE_ID_DS_QMI_DMS,__LINE__);
  if ( cl_sp == NULL ) 
  {
    ASSERT(cl_sp);
  }
  qmi_dms_state.client[clid - 1] = cl_sp;
  return cl_sp;
} /* qmi_dmsi_alloc_cl_sp */

/*===========================================================================
FUNCTION QMI_DMSI_DEALLOC_CL_SP()

DESCRIPTION
  This function gets called whenever a client state need to be deallocated.
  The client state is allocated and deallocated dynamically at runtime.

PARAMETERS
    clid: client ID

RETURN VALUE
  TRUE: client Id deallocated otherwise error.
 
DEPENDENCIES
  None

SIDE EFFECTS
  None
===========================================================================*/
static boolean qmi_dmsi_dealloc_cl_sp
(
  uint8 clid
)
{
  qmi_dmsi_client_state_type *  cl_sp = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_INFO2_1( "qmi_dmsi_dealloc_cl_sp clid :%d", clid );

  if ((clid == QMI_SVC_CLID_UNUSED ) ||
      (clid > DMSI_MAX_CLIDS))
  {
    return FALSE;
  }

  cl_sp = (qmi_dmsi_client_state_type *) qmi_dms_state.client[clid - 1];
  if(cl_sp)
  {
    q_destroy(&cl_sp->common.x_free_q);
    q_destroy(&cl_sp->common.cmd_free_q);

    PS_SYSTEM_HEAP_MEM_FREE(cl_sp);
    qmi_dms_state.client[clid - 1] = NULL;
  }
  else
  {
    LOG_MSG_ERROR_0("Client state pointer is already NULL");
    return FALSE;
  }
  return TRUE;
} /* qmi_dmsi_dealloc_cl_sp */


/*===========================================================================
  FUNCTION QMI_DMSI_INITIALIZE_CLIENT()

  DESCRIPTION
    Initialize the new client state for the given client state ptr.
    
  PARAMETERS
    cl_sp :  client state pointer
    
  RETURN VALUE
    None
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_dmsi_initialize_client
(
  qmi_dmsi_client_state_type * cl_sp
)
{
  qmi_dmsi_state_type *  dms_sp;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(cl_sp);
  dms_sp = (qmi_dmsi_state_type*)&qmi_dms_state;
  memset( cl_sp , 0, sizeof(qmi_dmsi_client_state_type) );

  LOG_MSG_INFO2_0( "qmi_dmsi_initialize_client " );
  cl_sp->qmi_inst     = DMSI_INVALID_INSTANCE;
  cl_sp->common.clid  = QMI_SVC_CLID_UNUSED;
  cl_sp->cookie       = ++(qmi_dmsi_global.clients_created);

  ds_qmi_fw_common_cl_init(dms_sp, &cl_sp->common);
  /*-----------------------------------------------------------------------
	init client bat levels to min/max 
  -----------------------------------------------------------------------*/
  memset(&(cl_sp->report_status), 0, sizeof(cl_sp->report_status));

  cl_sp->report_status.bat_lvl_lower_limit = DMSI_BAT_LVL_MIN;
  cl_sp->report_status.bat_lvl_upper_limit = DMSI_BAT_LVL_MAX;
  cl_sp->report_status.last_bat_lvl_reported = DMSI_BAT_LVL_INACTIVE;
  cl_sp->report_status.last_power_state_reported = DMSI_PWR_STATE_INACTIVE; 
#if defined(FEATURE_MMGSDI_SESSION_LIB)
#if defined(FEATURE_GSM) || defined(FEATURE_WCDMA) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA)
  cl_sp->get_ck_status_pending = MMGSDI_MAX_PERSO_FEATURE_ENUM;
#endif /* defined(FEATURE_GSM) || defined(FEATURE_WCDMA) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA) */
#endif /* defined(FEATURE_MMGSDI_SESSION_LIB) */

} /* qmi_dmsi_initialize_client() */

/*===========================================================================
  FUNCTION QMI_DMSI_PROCESS_CMD()

  DESCRIPTION
    This function processes a QMI DMS command or event.

    It is called by the QMI command handler and will dispatch the
    associated command/event handler function.

  PARAMETERS
    cmd_ptr:  private data buffer containing the QMI DMS command
              information.

  RETURN VALUE
    None

  DEPENDENCIES
    QMI DMS must already have been initialized

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_dmsi_process_cmd
(
  void *cmd_ptr
)
{
  qmi_dmsi_cmd_buf_type *cmd_buf_ptr = NULL;
/*-------------------------------------------------------------------------*/

  ASSERT(cmd_ptr);
  cmd_buf_ptr = (qmi_dmsi_cmd_buf_type *)cmd_ptr;

  switch(cmd_buf_ptr->cmd_id)
  {
    case QMI_CMD_DMS_INIT_CB:
    {
      qmi_dmsi_process_svc_init(cmd_buf_ptr->data.init_cb.num_instances);
      break;
    }

    case QMI_CMD_DMS_ALLOC_CLID_CB:
    {
      qmi_dmsi_process_alloc_clid(&cmd_buf_ptr->data.clid_cb.common_msg_hdr);
      break;
    }

    case QMI_CMD_DMS_DEALLOC_CLID_CB:
    {
      qmi_dmsi_process_dealloc_clid(&cmd_buf_ptr->data.clid_cb.common_msg_hdr);
      break;
    }

    case QMI_CMD_DMS_CMD_HDLR_CB:
    {
      qmi_dmsi_process_cmd_hdlr(&cmd_buf_ptr->data.cmd_hdlr_cb.msg_hdr,
                                    cmd_buf_ptr->data.cmd_hdlr_cb.sdu_in );
      break;
    }

#if defined(FEATURE_MMGSDI_SESSION_LIB)
    case QMI_CMD_PROCESS_UIM_GET_STATE:
      /* The mmgsdi_evt_info.activated field is populated and used only for
         MMGSDI_SESSION_CHANGED_EVT */
      qmi_dms_process_uim_get_state(cmd_buf_ptr->data.mmgsdi_evt_info.event,
                                       cmd_buf_ptr->data.mmgsdi_evt_info.card_err_info,
                                       cmd_buf_ptr->data.mmgsdi_evt_info.activated);
      break;

#endif /* defined(FEATURE_MMGSDI_SESSION_LIB) */

    case QMI_CMD_DMS_CM_CALL_CMD_STATUS:
      qmi_dms_process_cm_call_err(cmd_buf_ptr->data.cm_call_err.user_data,
                                     cmd_buf_ptr->data.cm_call_err.call_cmd,
                                     cmd_buf_ptr->data.cm_call_err.err_type);
      break;

    case QMI_CMD_SET_OPERATING_MODE_RESP:
      qmi_dms_generate_set_oprt_mode_resp(cmd_buf_ptr->data.cm_ph_err.user_data,
                                             cmd_buf_ptr->data.cm_ph_err.err_type);
      break;

    case QMI_CMD_DMS_EVT_REPORT_REQ_IND:
      qmi_dms_event_report_req_send_ind
                          (cmd_buf_ptr->data.dms_evt_info.evt_type,                           
                           cmd_buf_ptr->data.dms_evt_info.evt_value,
                           cmd_buf_ptr->data.dms_evt_info.clid);
      break;

    case QMI_CMD_DMS_RESET_SHUTDOWN:
      qmi_dms_generate_oprt_mode_ind(cmd_buf_ptr->data.cm_ph.info.oprt_mode);
      break;

#ifdef FEATURE_TDSCDMA	  
  	case QMI_CMD_TDSCDMA_SET_CONFIG_RES:
    {
      qmi_dmsi_generate_set_test_config_resp(&(cmd_buf_ptr->data.tdscdma_set_config_res));
      break;
    }
    case QMI_CMD_TDSCDMA_GET_CONFIG_RES:
    {
      qmi_dmsi_generate_get_test_config_resp(&(cmd_buf_ptr->data.tdscdma_get_config_res));
      break;
    }
#endif
    case QMI_CMD_DMS_DEV_CURRENT_CAP_CB:
    {
      qmi_dmsi_generate_current_device_capability_ind();
      break;
    }

    default:
      LOG_MSG_ERROR_1("Unexpected cmd(%d) recd in QMI DMS cmd handler",
                      cmd_buf_ptr->cmd_id);
      ASSERT(0);
  }
  qmi_dmsi_free_cmd_buf(cmd_ptr);
  return;
} /* qmi_dmsi_process_cmd() */

/*===========================================================================
  FUNCTION QMI_DMSI_GET_CMD_BUF()

  DESCRIPTION
    Allocate and assign a QMI DMS command buffer from the PS MEM heap
    based on the QMI CMD type
    
  PARAMETERS
    cmd - QMI command type
   
  RETURN VALUE
    cmd_buf_ptr - Pointer to the allocated command buffer

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void *qmi_dmsi_get_cmd_buf
(
  qmi_cmd_id_e_type cmd_id
)
{
  LOG_MSG_INFO1_1("dmsi_get_cmd_buf cmd_id = %d",cmd_id);
  switch (cmd_id)
  {
    case QMI_CMD_DMS_INIT_CB:
    case QMI_CMD_DMS_ALLOC_CLID_CB:
    case QMI_CMD_DMS_DEALLOC_CLID_CB:
#if defined(FEATURE_MMGSDI_SESSION_LIB)
    case QMI_CMD_PROCESS_UIM_GET_STATE:
#endif /* FEATURE_MMGSDI_SESSION_LIB */
    case QMI_CMD_DMS_CM_CALL_CMD_STATUS:
    case QMI_CMD_SET_OPERATING_MODE_RESP:
    case QMI_CMD_DMS_CMD_HDLR_CB:
    case QMI_CMD_DMS_EVT_REPORT_REQ_IND:
    case QMI_CMD_TDSCDMA_SET_CONFIG_RES:
    case QMI_CMD_TDSCDMA_GET_CONFIG_RES:
    case QMI_CMD_DMS_RESET_SHUTDOWN:
    case QMI_CMD_DMS_DEV_CURRENT_CAP_CB:
    {
      qmi_dmsi_cmd_buf_type *cmd_buf_ptr;
      cmd_buf_ptr = qmi_svc_ps_system_heap_mem_alloc(sizeof(qmi_dmsi_cmd_buf_type),
                                   FILE_ID_DS_QMI_DMS,__LINE__);
      return ((void *)cmd_buf_ptr);
    }
    default:
       break;
  }
  return NULL;
} /* qmi_dmsi_get_cmd_buf */

/*===========================================================================
  FUNCTION QMI_DMSI_PROCESS_SVC_INIT()

  DESCRIPTION
    This function processes a init callback request and intialize the service
    related infos.

  PARAMETERS
    num_instances:  nnumber of QMI Instances

  RETURN VALUE
    None

  DEPENDENCIES
    QMI DMS must already have been initialized and registered with Framework

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_dmsi_process_svc_init
(
  uint16 num_instances
)
{
  qmi_dmsi_state_type *         dms_sp;
/*-------------------------------------------------------------------------*/

  LOG_MSG_INFO2_0( "qmi_dmsi init callback " );
  dms_sp = (qmi_dmsi_state_type*)&qmi_dms_state;
  memset( dms_sp, 0, sizeof(qmi_dmsi_state_type) );
  q_init( &(dms_sp->set_oprt_mode_pending_q) );

  /*-------------------------------------------------------------------------
    initialize client state non-zero fields
  -------------------------------------------------------------------------*/

  if(qmi_dmsi_global.inited == FALSE)
  {
    /*-----------------------------------------------------------------------
      Initialize the global DMS service parameters
    -----------------------------------------------------------------------*/  
    memset( &qmi_dmsi_global, 0, sizeof(qmi_dmsi_global) );
  
    qmi_dmsi_global.pwr_rpt.low_limit       = DMSI_BAT_LVL_MIN;
    qmi_dmsi_global.pwr_rpt.high_limit      = DMSI_BAT_LVL_MAX;
    
  #ifdef FEATURE_MMGSDI_SESSION_LIB  
    qmi_dmsi_global.sim.slot = MMGSDI_SLOT_1;
    qmi_dmsi_global.sim.pin_info[MMGSDI_PIN1].pin_id = DMSI_UIM_PIN1;
    qmi_dmsi_global.sim.pin_info[MMGSDI_PIN2].pin_id = DMSI_UIM_PIN2;
    qmi_dmsi_global.sim.pin_info[MMGSDI_PIN1].status = QMI_PIN_STATUS_MIN;
    qmi_dmsi_global.sim.pin_info[MMGSDI_PIN2].status = QMI_PIN_STATUS_MIN;
    qmi_dmsi_global.uim_state = DMSI_SIM_UNKNOWN;
#if defined(FEATURE_GSM) || defined(FEATURE_WCDMA) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA)
    memset (&qmi_dmsi_global.sim.get_ck_state_q, 
            0, 
            sizeof(q_type));

    (void) q_init(&qmi_dmsi_global.sim.get_ck_state_q);
#endif /* defined(FEATURE_GSM) || defined(FEATURE_WCDMA) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA) */
    memset ( &qmi_dmsi_global.sim.mmgsdi_perso, 0,
             sizeof(qmi_dmsi_global.sim.mmgsdi_perso) );
  #endif /* FEATURE_MMGSDI_SESSION_LIB */

    /*-----------------------------------------------------------------------
      initialize the charger module
    -----------------------------------------------------------------------*/  
    qmi_charger_init();

    qmi_dmsi_global.voice_call_id = CM_CALL_ID_INVALID;

#if defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900)
    qmi_dmsi_global.activation_state = qmi_dmsi_calculate_activated_state();
#endif /* defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900) */
    
    /* init the PRL sequence number to 0 for manual activation PRL's */
    qmi_dmsi_global.prl_info.cl_sp = NULL;

    if (QMI_NV_STATUS_OK != qmi_nv_read(QMI_NV_ITEM_ALLOW_INVALID_SPC,
                                  0,
                                  &qmi_dmsi_global.nv_allow_invalid_spc,
                                  sizeof(qmi_dmsi_global.nv_allow_invalid_spc)))
    {
      qmi_dmsi_global.nv_allow_invalid_spc = FALSE;
    }
    if (QMI_NV_STATUS_OK != qmi_nv_read(QMI_NV_ITEM_RESTRICT_IMSI_ACCESS,
                               0,
                               &qmi_dmsi_global.nv_restrict_imsi_access,
                               sizeof(qmi_dmsi_global.nv_restrict_imsi_access)))
    {
      qmi_dmsi_global.nv_restrict_imsi_access = FALSE;
    }
    if (QMI_NV_STATUS_OK != qmi_nv_read(
                         QMI_NV_ITEM_USE_MAX_DEFINED_DATA_RATES,
                         0,
                         &qmi_dmsi_global.nv_use_max_defined_data_rates,
                         sizeof(qmi_dmsi_global.nv_use_max_defined_data_rates)))
    {
      qmi_dmsi_global.nv_use_max_defined_data_rates = FALSE;
    }
    if (QMI_NV_STATUS_OK != qmi_nv_read(
                         QMI_NV_ITEM_USE_OTASP_ACT_STATE,
                         0,
                         &qmi_dmsi_global.nv_use_last_otasp_act_state,
                         sizeof(qmi_dmsi_global.nv_use_last_otasp_act_state)))
    {
      qmi_dmsi_global.nv_use_last_otasp_act_state = FALSE;
    }

#ifdef FEATURE_TDSCDMA
    /* Initialize tdscdma test config module for GET/SET routines */
    qmi_dmsi_tdscdma_config_init();
#endif
    qmi_dmsi_policyman_config_init();

    qmi_dmsi_global.inited = TRUE;
  }

} /* qmi_dmsi_process_svc_init */

/*===========================================================================
  FUNCTION QMI_DMSI_PROCESS_ALLOC_CLID()

  DESCRIPTION
    This function process the client alloc callback request. This function
    allocates and initialize the new client state for the allocated client ID.

  PARAMETERS
    common_msg_hdr:  private data buffer containing the clid alloc request information.

  RETURN VALUE
    None

  DEPENDENCIES
    QMI DMS must already have been initialized and registered with Framework

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_dmsi_process_alloc_clid
(
  qmi_framework_common_msg_hdr_type * common_msg_hdr
)
{
  qmi_dmsi_client_state_type   *  cl_sp;  
  qmi_result_e_type                result = QMI_RESULT_FAILURE; 
  qmi_error_e_type                 errval = QMI_ERR_NONE;
/*-------------------------------------------------------------------------*/

  ASSERT(common_msg_hdr && (common_msg_hdr->service == QMUX_SERVICE_DMS));
 
  cl_sp = NULL;

  LOG_MSG_INFO2_0( "qmi_dmsi process clid allocation" );
  cl_sp = qmi_dmsi_alloc_cl_sp(common_msg_hdr->client_id);

  if(cl_sp)
  {
    qmi_dmsi_initialize_client(cl_sp);
    cl_sp->common.clid = common_msg_hdr->client_id;
    cl_sp->qmi_inst    = (int16)common_msg_hdr->qmi_instance;

    LOG_MSG_INFO2_1( "qmi_dmsi clid set to local client state ptr: clid%d",
                     cl_sp->common.clid );
    result = QMI_RESULT_SUCCESS;
  }
  else
  {
    LOG_MSG_INFO1_0 ("No available service clids!" );
    errval = QMI_ERR_INTERNAL;
  }

  /* Notifying the clid allocation back to client via QMI Framework*/
  qmi_framework_svc_send_alloc_clid_result_ex(result, common_msg_hdr,
                                              errval);

} /* qmi_dmsi_process_alloc_clid */

/*===========================================================================
  FUNCTION QMI_DMSI_PROCESS_DEALLOC_CLID()

  DESCRIPTION
    This function process the client dealloc callback request. This function
    reset and deallocates the new client state for the allocated client ID.

  PARAMETERS
    common_msg_hdr:  private data buffer containing the clid dealloc
    request information.
 
  RETURN VALUE
    None

  DEPENDENCIES
    QMI DMS must already have been initialized and registered with Framework

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_dmsi_process_dealloc_clid
(
  qmi_framework_common_msg_hdr_type * common_msg_hdr
)
{
  qmi_dmsi_client_state_type   *  cl_sp;   
  qmi_result_e_type                result = QMI_RESULT_FAILURE; 
  qmi_error_e_type                 errval = QMI_ERR_NONE;
/*-------------------------------------------------------------------------*/

  ASSERT(common_msg_hdr);
  LOG_MSG_INFO2_1( "qmi_dmsi process clid deallocation common_msg_hdr->client_id = %x",
                  common_msg_hdr->client_id);

  if ((common_msg_hdr->client_id == QMI_SVC_CLID_UNUSED ) ||
      (common_msg_hdr->client_id  > DMSI_MAX_CLIDS ))
  {
    /* Invalid clid */
    return;
  }

  cl_sp = NULL;

  cl_sp = (qmi_dmsi_client_state_type *) 
          qmi_dms_state.client[common_msg_hdr->client_id  - 1];

  if (cl_sp != NULL && cl_sp->common.clid == common_msg_hdr->client_id)
  {
    qmi_dmsi_reset_client(cl_sp);
    qmi_dmsi_dealloc_cl_sp(common_msg_hdr->client_id);
    LOG_MSG_INFO2_1( "qmi_dmsi clid free to local client state ptr: clid %d",
                     common_msg_hdr->client_id );
    result = QMI_RESULT_SUCCESS;
  }
  else
  {
    errval = QMI_ERR_INVALID_CLIENT_ID;
    LOG_MSG_ERROR( "Can't find clid %d!", common_msg_hdr->client_id, 0, 0 );
  }

  /* Notifying the clid deallocation back to client via QMI Framework*/
   qmi_framework_svc_send_dealloc_clid_result_ex(result, common_msg_hdr, 
                                             errval);
} /* qmi_dmsi_process_dealloc_clid */

/*===========================================================================
  FUNCTION QMI_DMSI_PROCESS_CMD_HDLR()

  DESCRIPTION
    This function process the service command handler request callback.
    This function creates a transaction and dispatches to the appropriate
    message handler
    
  PARAMETERS
    msg_hdr:  Framework message header for the request.
    sdu_in : request

  RETURN VALUE
    None

  DEPENDENCIES
    QMI DMS must already have been initialized and registered with Framework

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_dmsi_process_cmd_hdlr
(
  qmi_framework_msg_hdr_type* msg_hdr,
  dsm_item_type * sdu_in 
)
{
  qmi_dmsi_client_state_type * cl_sp;
/*-------------------------------------------------------------------------*/

  ASSERT(msg_hdr && sdu_in);
  
  if ((msg_hdr->common_hdr.client_id == QMI_SVC_CLID_UNUSED ) ||
      (msg_hdr->common_hdr.client_id  > DMSI_MAX_CLIDS ))
  {
    LOG_MSG_ERROR_1( "Invalid clid: %d", msg_hdr->common_hdr.client_id );
    return;
  }
  
  LOG_MSG_INFO2_3( "Process QMI DMS svc commoan handlr callback function: clid%d, tx_id:%d, ctl_flag:%d ",
                   msg_hdr->common_hdr.client_id,
                   msg_hdr->common_hdr.transaction_id,
                   msg_hdr->msg_ctl_flag );

  /*-------------------------------------------------------------------------
    Get client state pointer
  -------------------------------------------------------------------------*/
  cl_sp = qmi_dms_state.client[msg_hdr->common_hdr.client_id - 1];
  
  ASSERT(cl_sp);

  ds_qmi_fw_recv(&qmi_dmsi_cfg, (void*)&qmi_dms_state,
  	             msg_hdr, &cl_sp->common, sdu_in);
} /* qmi_dmsi_process_cmd_hdlr */

/*===========================================================================
FUNCTION QMI_DMSI_SEND_RESPONSE()

DESCRIPTION
  This function calls QMI Framework API to send a QMI DMS Service response to a
  clients.

PARAMETERS 
  cl_sp    : client state pointer
  cmd_buf_p: command buffer
  msg_ptr : dsm item

RETURN VALUE
  TRUE: If responses send to Framework
  FALSE: otherwise

DEPENDENCIES
  QMI DMS service must be initialized and registered with Framework

SIDE EFFECTS
  None
===========================================================================*/
static boolean qmi_dmsi_send_response
(
  qmi_cmd_buf_type *               cmd_buf_p,
  dsm_item_type *                  msg_ptr
)
{
  qmi_dmsi_client_state_type *	       cl_sp;
  qmi_framework_common_msg_hdr_type    common_hdr;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  
  if ( (cmd_buf_p == NULL) || (msg_ptr == NULL) ||
       (cmd_buf_p->x_p == NULL) || (cmd_buf_p->x_p->cl_sp == NULL) )
  {
    ASSERT(0);
    return FALSE;
  }
  
  cl_sp = (qmi_dmsi_client_state_type *)cmd_buf_p->x_p->cl_sp;
  
  common_hdr.service        = QMUX_SERVICE_DMS;
  common_hdr.client_id	    = cl_sp->common.clid;
  common_hdr.transaction_id = cmd_buf_p->x_p->x_id;
  common_hdr.qmi_instance   = cl_sp->qmi_inst;
  
  return ds_qmi_fw_send_response(&common_hdr, cmd_buf_p, msg_ptr);

} /* qmi_dmsi_send_response */

/*===========================================================================
  FUNCTION QMI_DMSI_GET_CDMA_LOCK_MODE()

  DESCRIPTION
    Retrieves CDMA lock mode
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_dmsi_get_cdma_lock_mode
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;

  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

  PACKED struct PACKED_POST
  {
    uint32  cdma_lock_mode;
  } v_out_req;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);

  memset(&v_out_req, 0, sizeof(v_out_req));

  response = NULL;
  errval = QMI_ERR_NONE;

  LOG_MSG_INFO1_1("qmi_dmsi_get_cdma_lock_mode()"
                  "qmi_dmsi_global.cdma_lock_mode %d",
                  qmi_dmsi_global.cdma_lock_mode);

  v_out_req.cdma_lock_mode = (uint8) qmi_dmsi_global.cdma_lock_mode;
	
  if( FALSE == qmi_svc_put_param_tlv(&response, 
                                     DMSI_PRM_TYPE_GET_CDMA_LOCK_MODE,
                                     sizeof(v_out_req),
                                     &v_out_req ) )
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
  }

  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                    : QMI_RESULT_FAILURE);
  LOG_MSG_INFO1_2("qmi_dmsi_get_cdma_lock_mode()"
                  "errval %d result %d", errval, result);
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
       
  return response;
  
} /* qmi_dmsi_get_cdma_lock_mode() */

/*===========================================================================
  FUNCTION QMI_DMS_EVENT_REPORT_REQ_SEND_IND()

  DESCRIPTION
    Sends the indication of a phone to the client that 
    registered for it.
    
  PARAMETERS
    event        : phone event from cm
    value        : status of the phone event
    clid         : Id of the registering client

  RETURN VALUE
    None
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void qmi_dms_event_report_req_send_ind
(
  qmi_dmsi_report_status_ev_e_type             event_type,
  void                                       * event_info,
  uint8                                        clid
)
{
  qmi_dmsi_state_type *                         dms_sp;
  boolean *                                     cdma_lock_mode = NULL;
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  dms_sp = (qmi_dmsi_state_type*)&qmi_dms_state;

  LOG_MSG_INFO1_2("qmi_dms_send_indication() event %d, clid %d",
                  event_type, clid);

  switch (event_type)
  {
    case DMSI_REPORT_STATUS_EV_CDMA_LOCK_MODE:

      cdma_lock_mode = (boolean *) event_info;	
      qmi_dmsi_event_report_ind(dms_sp,
                                clid,
                                DMSI_REPORT_STATUS_EV_CDMA_LOCK_MODE,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                *cdma_lock_mode);
   break;
   
   case DMSI_REPORT_STATUS_EV_DEV_MSIM_CAP:
          qmi_dmsi_event_report_ind(dms_sp,
                                    clid,
                                    DMSI_REPORT_STATUS_EV_DEV_MSIM_CAP,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0);
    break;

    default:
      break;
  }
} /*qmi_dms_event_report_req_send_ind*/
