/*===========================================================================

                         D S _ Q M I _ W D S _ U M T S . C

DESCRIPTION

 The Data Services Qualcomm Wireless Data Services MSM Interface source file.

EXTERNALIZED FUNCTIONS

  QMI_WDS_UMTS_GET_RUNTIME_SETTINGS()
    Get the runtime settings for a given UMTS profile.
    
Copyright (c) 2005-2012 Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary.
===========================================================================*/
/*===========================================================================

                      EDIT HISTORY FOR FILE
                      
  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/qmidata/src/ds_qmi_wds_umts.c#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $
  
when        who    what, where, why
--------    ---    ----------------------------------------------------------
09/10/12    sb     Added Len checks for QMI TLVs
10/09/09    ba     Win7 compliance modification.
05/22/09    pp     PDPREG API - CMI De-featurization impacts.
02/08/08    mct    Added support for IPv6 over UMTS.
11/02/07    ks     Added PCSCF addr using PCO flag to profiles.
10/15/07    ks     Added gateway address and mask to runtime settings.
05/28/07    ks     Added support for get_runtime_settings api
07/06/06    ks     Replaces qmi_wds_umtsi_get_tl() with qmi_svc_get_tl().
04/18/06    ks     Changed format of profile_list tlv to include 
                   num_instances.
04/10/06    ks     Fix profile name during create.
03/15/06    ks     Mapping auth preference got in Start Network Interface to 
                   umtsps auth type.
03/13/06    ks     Changed parsing of requests to return error for fixed 
                   length tlvs when tlv lengh passed is incorrect.
03/13/06    ks     Changed QMI_ERR_INVALID_ARG to QMI_ERR_MALFORMED_MSG.
03/09/06    ks     Removed header and data compression tlvs from the spec 
                   and code as umts doesn't read it from profiles currently.
03/06/06    ks     Expedite parsing in qmi_wds_umtsi_parse_profile_params() 
                   if a malformed TLV is seen.
01/17/05    mct    Fixed typo.
12/14/05    ks     Made changes in UMTS profile defn if IPV6 is defined.
11/30/05    ks     Created module
===========================================================================*/

/*===========================================================================

                          INCLUDE FILES FOR MODULE

===========================================================================*/

#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"
#if defined (FEATURE_WCDMA) || defined (FEATURE_GSM) || defined (FEATURE_LTE) || defined(FEATURE_TDSCDMA)
#include "amssassert.h"
#include "dsm.h"

#include "ds_qmi_wds_umts.h"
#include "dsumtspdpreg.h"
#include "dsumtsps.h"
#include "ps_byte.h"
#include "ps_iface.h"
#include "ps_ifacei.h"
#include "ps_phys_link.h"
#include "ps_iface_ioctl.h"

#include "ds_qmi_wds.h"
#include "ds_qmi_svc.h"
#include "ds_qmi_defs.h"

#include <stringl/stringl.h>



/*===========================================================================

                               DEFINITIONS

===========================================================================*/

#define WDSI_PRM_TYPE_PROFILE_NAME   (0x10)
#define WDSI_PRM_TYPE_PDP_TYPE       (0x11)
#define WDSI_PRM_TYPE_HDR_COMP       (0x12)
#define WDSI_PRM_TYPE_DATA_COMP      (0x13)
#define WDSI_PRM_TYPE_APN            (0x14)
#define WDSI_PRM_TYPE_PRI_DNS        (0x15)
#define WDSI_PRM_TYPE_SEC_DNS        (0x16)
#define WDSI_PRM_TYPE_UMTS_REQ_QOS   (0x17)
#define WDSI_PRM_TYPE_UMTS_MIN_QOS   (0x18)
#define WDSI_PRM_TYPE_GPRS_REQ_QOS   (0x19)
#define WDSI_PRM_TYPE_GPRS_MIN_QOS   (0x1A)
#define WDSI_PRM_TYPE_AUTH_USERNAME  (0x1B)
#define WDSI_PRM_TYPE_AUTH_PASSWORD  (0x1C)
#define WDSI_PRM_TYPE_AUTH_PREF      (0x1D)
#define WDSI_PRM_TYPE_IPV4_ADDR      (0x1E)
#define WDSI_PRM_TYPE_PCSCF_USING_PCO_FLAG  (0x1F)
#define WDSI_PRM_TYPE_RUNTIME_PCSCF_USING_PCO_FLAG  (0x22)
#define WDSI_PRM_TYPE_PRI_DNS_IPV6   (0x20)
#define WDSI_PRM_TYPE_SEC_DNS_IPV6   (0x21)
#define WDSI_PRM_TYPE_IPV6_ADDR      (0x22)
#define WDSI_PRM_TYPE_IM_CN_FLAG     (0x2C)
#define WDSI_PRM_TYPE_PROFILE_IDENTIFIER (0x1F)
#define WDSI_PRM_TYPE_PDP_TYPE_LEN     (0x01)
#define WDSI_PRM_TYPE_AUTH_PREF_LEN    (0x01)
#define WDSI_PRM_TYPE_BOOLEAN_FLAG_LEN (0x01)


#define WDS_UMTS_NO_FREE_PROFILE     (0x0)

#define WDSI_UMTS_QOS_PRM_STRUCT_LEN (0x21)
#define WDSI_GPRS_QOS_PRM_STRUCT_LEN (0x14)

#define CHECK_RETVAL()  if (FALSE == retval) { dsm_free_packet(&response); \
                                               return NULL; }

/*===========================================================================
            
                        EXTERNAL FUNCTION DEFINITIONS

===========================================================================*/

/*===========================================================================
  FUNCTION QMI_WDS_UMTS_GET_RUNTIME_SETTINGS()

  DESCRIPTION
    Get the runtime settings for a given UMTS profile.

  PARAMETERS
    iface_ptr        : the ps_iface pointer
    bearer_tech_type : the bearer technology type

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
#define HANDLE_FAILURE(pkt)  dsm_free_packet(pkt); \
                             errval = QMI_ERR_NO_MEMORY; \
                             goto failure  

qmi_error_e_type qmi_wds_umts_get_runtime_settings
(
  uint32                           req_settings,
  dsm_item_type **                 response,
  ps_iface_type *                  iface_ptr,
#ifdef FEATURE_DATA_WLAN_MAPCON
  ds_sys_system_status_info_type   bearer_tech_ex_type
#else
  ps_iface_bearer_technology_type  bearer_tech_type
#endif /* FEATURE_DATA_WLAN_MAPCON */
)
{
  PACKED struct PACKED_POST
  {
    uint8 profile_type;
    uint8 profile_index;
  } v_out_reqd;

  ps_iface_ioctl_3gpp_session_params_info_type  session_info;
  qmi_error_e_type errval;
  uint16 len;
  int return_val;
  sint15 ps_errno;
  boolean im_cn_flag;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  errval = QMI_ERR_NONE;


  if ( req_settings & QMI_WDS_MASK_IM_CN_FLAG )
  {
    return_val = ps_iface_ioctl(iface_ptr,
                                PS_IFACE_IOCTL_UMTS_GET_IM_CN_FLAG,
                                &im_cn_flag,
                                &ps_errno);
    if (return_val == -1) 
    {
      LOG_MSG_INFO1_0("Failed PS_IFACE_IOCTL_UMTS_GET_IM_CN_FLAG. Return rest of the available runtime settings");
    }else
    {
      if(FALSE == qmi_svc_put_param_tlv(response,
                                        WDSI_PRM_TYPE_IM_CN_FLAG,
                                        WDSI_PRM_TYPE_BOOLEAN_FLAG_LEN,
                                        &im_cn_flag))
      {
        HANDLE_FAILURE(response);
      }
    }
  }

  return_val = ps_iface_ioctl(iface_ptr,
                                PS_IFACE_IOCTL_UMTS_GET_SESSION_PARAMS,
                                &session_info,
                                &ps_errno);
  if (return_val == -1) 
  {
    LOG_MSG_INFO1_0("Failed PS_IFACE_IOCTL_UMTS_GET_SESSION_PARAMS. Return rest of the available runtime settings");
  }
  else
  {
    if(req_settings & QMI_WDS_MASK_PCSCF_PCO)
    {
      if(FALSE == qmi_svc_put_param_tlv(response,
                                        WDSI_PRM_TYPE_RUNTIME_PCSCF_USING_PCO_FLAG,
                                        sizeof(session_info.request_pcscf_address_flag),
                                        (void *)&(session_info.request_pcscf_address_flag)))
      {
        HANDLE_FAILURE(response);
      }
    }

    if(req_settings & QMI_WDS_MASK_GRANTED_QOS)
    {
#ifdef FEATURE_DATA_WLAN_MAPCON
      if(DS_SYS_RAT_EX_NULL_BEARER != bearer_tech_ex_type.rat_value)
      {
        if((DS_SYS_RAT_EX_3GPP_GERAN == bearer_tech_ex_type.rat_value) &&
           ((bearer_tech_ex_type.so_mask & DS_SYS_SO_EX_3GPP_GPRS) ||
            (bearer_tech_ex_type.so_mask & DS_SYS_SO_EX_3GPP_EDGE)))//GPRS
        {
          if(FALSE == qmi_svc_put_param_tlv(response,
                                            WDSI_PRM_TYPE_GPRS_REQ_QOS,
                                            WDSI_GPRS_QOS_PRM_STRUCT_LEN,
                                            (void *)&(session_info.gprs_qos.precedence)))
          {
            HANDLE_FAILURE(response);
          }
        }
        else if (( bearer_tech_ex_type.rat_value == DS_SYS_RAT_EX_3GPP_LTE) ||
                 (bearer_tech_ex_type.rat_value == DS_SYS_RAT_EX_3GPP_WLAN))
        {
          LOG_MSG_ERROR_0("LTE/WLAN QoS not supported currently");
        }
#else
      if(PS_IFACE_3GPP_NULL_BEARER != bearer_tech_type.data.umts_type.rat_mask)
      {
        if ((bearer_tech_type.data.umts_type.rat_mask == PS_IFACE_UMTS_GPRS) ||
            (bearer_tech_type.data.umts_type.rat_mask == PS_IFACE_UMTS_EDGE)) //GPRS
        {
          if(FALSE == qmi_svc_put_param_tlv(response,
                                            WDSI_PRM_TYPE_GPRS_REQ_QOS,
                                            WDSI_GPRS_QOS_PRM_STRUCT_LEN,
                                            (void *)&(session_info.gprs_qos.precedence)))
          {
            HANDLE_FAILURE(response);
          }
        }
        else if ( bearer_tech_type.data.umts_type.rat_mask == PS_IFACE_3GPP_LTE)
        {
          LOG_MSG_ERROR_0("LTE QoS not supported currently");
        }
#endif /* FEATURE_DATA_WLAN_MAPCON */
        else //UMTS/TDSCDMA
        {
          if(FALSE == qmi_svc_put_param_tlv(response,
                                            WDSI_PRM_TYPE_UMTS_REQ_QOS,
                                            WDSI_UMTS_QOS_PRM_STRUCT_LEN,
                                            (void *)&(session_info.umts_qos.traffic_class)))
          {
            HANDLE_FAILURE(response);
          }
        }
      }
    }

    if( req_settings & QMI_WDS_MASK_USERNAME)
    {
      /* Make sure that the string is NULL terminated */
      session_info.auth.username[sizeof(session_info.auth.username) -1] = '\0';
      len = strlen((char *)session_info.auth.username);
      if(len != 0)
      {
        if(FALSE == qmi_svc_put_param_tlv(response,
                                          WDSI_PRM_TYPE_AUTH_USERNAME,
                                          len,
                                          (void *)session_info.auth.username))
        {
          HANDLE_FAILURE(response);
        }
      }
    }

    if(req_settings & QMI_WDS_MASK_AUTH_PROTO)
    {
      if(FALSE == qmi_svc_put_param_tlv(response,
                                        WDSI_PRM_TYPE_AUTH_PREF,
                                        WDSI_PRM_TYPE_AUTH_PREF_LEN,
                                        (void *)&(session_info.auth.auth_type)))
      {
        HANDLE_FAILURE(response);
      }
    }

    if(req_settings & QMI_WDS_MASK_APN)
    {
      /* Make sure that the string is NULL terminated */
      session_info.apn[sizeof(session_info.apn) - 1] = '\0';
      len = strlen((char *)session_info.apn);
      if(0 != len)
      {
        if(FALSE == qmi_svc_put_param_tlv(response,
                                          WDSI_PRM_TYPE_APN,
                                          len,
                                          (void *)session_info.apn))
        {
          HANDLE_FAILURE(response);
        }
      }
    }

    if(req_settings & QMI_WDS_MASK_PDP_TYPE)
    {
      if(FALSE == qmi_svc_put_param_tlv(response,
                                        WDSI_PRM_TYPE_PDP_TYPE,
                                        WDSI_PRM_TYPE_PDP_TYPE_LEN,
                                         &session_info.pdp_type))
      {
        HANDLE_FAILURE(response);
      }
    }

    if(req_settings & QMI_WDS_MASK_PROFILE_NAME)
    {
      /* Make sure the string is NULL terminated */ 
      session_info.profile_name[sizeof(session_info.profile_name) - 1] = '\0';
      len = strlen((char *)session_info.profile_name);
      if(0 != len)
      {
        if(FALSE == qmi_svc_put_param_tlv(response,
                                          WDSI_PRM_TYPE_PROFILE_NAME,
                                          len,
                                          (void *)session_info.profile_name))
        {
          HANDLE_FAILURE(response);
        }
      }
    }

    if(req_settings & QMI_WDS_MASK_PROFILE_ID)
    {
      v_out_reqd.profile_type = WDS_PROFILE_TYPE_3GPP;
      /* Remove the upper byte (profile family) from the profile number */
      v_out_reqd.profile_index = (uint8)session_info.profile_number & 0x00FF;

      if( FALSE == qmi_svc_put_param_tlv(response,
                                         WDSI_PRM_TYPE_PROFILE_IDENTIFIER,
                                         sizeof (v_out_reqd),
                                         &v_out_reqd))
      {
        HANDLE_FAILURE(response);
      }
    }
  } /* if (return_val == -1) */

failure:
  return errval;
} /* qmi_wds_umts_get_runtime_settings */

#endif /* defined(FEATURE_WCDMA) || defined(FEATURE_GSM) || defined (FEATURE_LTE) || defined(FEATURE_TDSCDMA)*/
