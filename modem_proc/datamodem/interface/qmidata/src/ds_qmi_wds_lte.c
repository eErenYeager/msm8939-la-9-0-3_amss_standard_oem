/*===========================================================================

                         D S _ Q M I _ W D S _ L T E . C

DESCRIPTION

 The Data Services Qualcomm Wireless Data Services MSM Interface source file
 that contains LTE specific functions.
EXTERNALIZED FUNCTIONS

  QMI_WDS_SET_LTE_ATTACH_PDN_LIST()
     Set the lte attach pdn profile list by calling into the PS SYS layer
  QMI_WDS_GET_LTE_ATTACH_PDN_LIST()
     Retrieves lte attach pdn list by calling into the PS SYS layer
  QMI_WDS_GET_LTE_MAX_ATTACH_PDN_LIST_NUM()
     Retrieves max lte attach pdn list size
  QMI_WDS_SET_LTE_DATA_RETRY()
     Modifies the LTE Data Retry setting.
  QMI_WDS_GET_LTE_DATA_RETRY()
     Retrieves the LTE Data Retry setting.
  QMI_WDS_SET_LTE_ATTACH_TYPE()
     Modifies the LTE Attach Type setting.
  QMI_WDS_GET_LTE_ATTACH_TYPE()
     Retrieves the LTE Attach Type setting.
  QMI_WDS_UPDATE_LTE_ATTACH_PDN_LIST_PROFILES()
     Updates the LTE attach PDN list profile params
Copyright (c) 2013 Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary.
===========================================================================*/
/*===========================================================================

                      EDIT HISTORY FOR FILE
                      
  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/qmidata/src/ds_qmi_wds_lte.c#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $
  
when        who    what, where, why
--------    ---    ----------------------------------------------------------
05/13/13    sj     Created module
===========================================================================*/

/*===========================================================================

                          INCLUDE FILES FOR MODULE

===========================================================================*/

#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"
#if defined (FEATURE_DATA_LTE)
#include "amssassert.h"
#include "dsm.h"
#include "ds_Utils_DebugMsg.h"

#include "ds_qmi_wds_lte.h"
#include "qmi_svc_defs.h"
#include "qmi_svc_utils.h"

#include "ps_sys.h"
#include "ps_sys_conf.h"
#include "ps_sys_ioctl.h"
#include "ps_sys_event.h"
#include "dserrno.h"
#include "ds_qmi_wds_profile.h"
#include "ds_qmi_wds.h"
#include "ds_qmi_svc.h"


/*===========================================================================

                               DEFINITIONS

===========================================================================*/

#define WDSI_PRM_TYPE_BOOLEAN_FLAG_LEN (0x01)

/* LTE Attach PDN GET TLVs*/
#define WDSI_PRM_TYPE_LTE_ATTACH_PDN_LIST               (0x10)

/* GET_LTE_MAX_ATTACH_PDN_NUM TLVs*/
#define WDSI_PRM_TYPE_GET_LTE_MAX_ATTACH_PDN_NUM        (0x10)

/* LTE Data Retry GET TLVs*/
#define WDSI_PRM_TYPE_LTE_DATA_RETRY                    (0x10)

/* LTE Attach Type GET TLVs*/
#define WDSI_PRM_TYPE_LTE_ATTACH_TYPE                   (0x10)

#define CHECK_RETVAL()  if (FALSE == retval) { dsm_free_packet(&response); \
                                               return NULL; }

/*===========================================================================
            
                        EXTERNAL FUNCTION DEFINITIONS

===========================================================================*/

/*===========================================================================
  FUNCTION QMI_WDS_SET_LTE_ATTACH_PDN_LIST()

  DESCRIPTION
    Set the lte attach pdn profile list by calling into the PS SYS layer

  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
dsm_item_type*  qmi_wds_set_lte_attach_pdn_list
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *       response;
  qmi_error_e_type      errval;
  qmi_result_e_type     result;
  boolean               retval;
  int                   return_val;
  int16                 ps_errno = DS_ENOERR;
  void                * value;
  uint16                len;
  uint16                expected_len;
  uint8                 type;
  int                   cnt_pdn;
 
  PACKED struct PACKED_POST
  {
    uint8             lte_attach_pdn_list_num;
    uint16            lte_attach_pdn_list[WDS_LTE_ATTACH_PDN_PROFILE_LIST_MAX];
  } v_in_required;  

  ps_sys_3gpp_lte_attach_pdn_list_type        attach_pdn_list;
  boolean                                     got_v_in_required = FALSE;
  uint32                                      subs_id;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp);
  ASSERT(sdu_in);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);

  memset(&v_in_required, 0, sizeof(v_in_required));
  memset(&attach_pdn_list, 0, sizeof(attach_pdn_list));

  response = NULL;
  errval = QMI_ERR_NONE;

  while (*sdu_in)
  {
    if( !qmi_svc_get_tl(sdu_in, &type, &len) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        if (len > sizeof(v_in_required))
        {
          LOG_MSG_INFO2_1("LTE attach pdn TLV length %d too large", len);
          errval = QMI_ERR_MALFORMED_MSG;
          goto send_result;
        }
        expected_len = 0; // variable
        value = (void *) &v_in_required;
        got_v_in_required = TRUE;
        break;

      default:
        LOG_MSG_INFO2_1("Unrecognized TLV type (%d)", type);
        break;
    }

    if(!qmi_svc_validate_type_length( type,
                                      len,
                                      expected_len,
                                      FILE_ID_DS_QMI_WDS_LTE,__LINE__)  )
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    /*-----------------------------------------------------------------------
      if value is NULL, skip past the current TLV
    -----------------------------------------------------------------------*/
    if(len != qmi_svc_dsm_pullup( sdu_in, value, len ,
                         FILE_ID_DS_QMI_WDS_LTE,__LINE__ ))
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  if ( !got_v_in_required )
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  /* Check that that the attach_pdn_list length matches up to lte_attach_pdn_list_num */
  if (len != (sizeof( v_in_required.lte_attach_pdn_list_num) + sizeof(uint16) * v_in_required.lte_attach_pdn_list_num))
  {
    errval = QMI_ERR_MALFORMED_MSG;
    LOG_MSG_INFO2_2("Mismatch between TLV Len %d and lte_attach_pdn %d",
                    len, v_in_required.lte_attach_pdn_list_num);
    goto send_result;
  }

  attach_pdn_list.attach_pdn_profile_list_num =
     v_in_required.lte_attach_pdn_list_num;

  for (cnt_pdn = 0; cnt_pdn < attach_pdn_list.attach_pdn_profile_list_num; cnt_pdn++)
  {
    attach_pdn_list.attach_pdn_profile_list[cnt_pdn] =
      v_in_required.lte_attach_pdn_list[cnt_pdn];
  }

  subs_id = qmi_wdsi_resolve_client_subs(cl_sp);
  return_val = ps_sys_ioctl_ex( PS_SYS_TECH_3GPP,
                             PS_SYS_IOCTL_3GPP_SET_LTE_ATTACH_PDN_LIST,
                                (ps_sys_subscription_enum_type)subs_id,
                             (void *)&attach_pdn_list,
                             &ps_errno );
  
  if (-1 == return_val )
  {
    LOG_MSG_ERROR_1("Set LTE attach pdn list failed, ps_errno:%d ", ps_errno);

    switch (ps_errno)
    {                                   
      case DS_EOPNOTSUPP:
        errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
        break;

      case DS_EDBOVERFLOW:
        errval = QMI_ERR_REQUESTED_NUM_UNSUPPORTED;
        break;

      case DS_EINVAL:
        errval = QMI_ERR_INVALID_PROFILE;
        break;

      default:
        errval = QMI_ERR_INTERNAL;
        break;
    } 

    goto send_result;
  }

  send_result:
    result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS
                                     : QMI_RESULT_FAILURE);

    retval = qmi_svc_put_result_tlv(&response, result, errval);
    CHECK_RETVAL();
    return response;

} /* qmi_wds_set_lte_attach_pdn_list() */

/*===========================================================================
  FUNCTION QMI_WDS_GET_LTE_ATTACH_PDN_LIST()

  DESCRIPTION
    Retrieves lte attach pdn list by calling into the PS SYS layer

  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
dsm_item_type * qmi_wds_get_lte_attach_pdn_list
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *                     response;
  qmi_error_e_type                    errval;
  qmi_result_e_type                   result;
  boolean                             retval;
  int16                               ps_errno = 0;
  int                                 cnt_pdn;
  int16                               ps_result;

  ps_sys_3gpp_lte_attach_pdn_list_type  attach_pdn_list;
  qmi_wds_lte_attach_pdn_list_type   v_out_attach_pdn_list;
  uint32                               subs_id;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);
  ASSERT(sdu_in);

  response = NULL;
  errval = QMI_ERR_NONE;

  memset(&attach_pdn_list, 0, sizeof(attach_pdn_list));

  subs_id = qmi_wdsi_resolve_client_subs(cl_sp);
  /*-------------------------------------------------------------------------
    Retrieve attach pdn list information by calling into PS Sys Layer
  -------------------------------------------------------------------------*/
  ps_result = ps_sys_ioctl_ex ( PS_SYS_TECH_3GPP,
                             PS_SYS_IOCTL_3GPP_GET_LTE_ATTACH_PDN_LIST,
                               (ps_sys_subscription_enum_type)subs_id,
                             (void *)&attach_pdn_list,
                             &ps_errno);
  if ( ps_result  < 0 )
  {
    LOG_MSG_ERROR_2("ps_sys_ioctl %d operation returned : (%d)",
                    PS_SYS_IOCTL_3GPP_GET_LTE_ATTACH_PDN_LIST, ps_errno);
    switch (ps_errno)
    {                                   
      case DS_NOMEMORY:
        errval = QMI_ERR_NO_MEMORY;
        break;

      case DS_EOPNOTSUPP:
        errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
        break;

      default:
        errval = QMI_ERR_INTERNAL;
        break;
    } 
    goto send_result;
  }
  
  v_out_attach_pdn_list.attach_pdn_profile_list_num = 
    attach_pdn_list.attach_pdn_profile_list_num;

  for (cnt_pdn = 0; cnt_pdn < attach_pdn_list.attach_pdn_profile_list_num; cnt_pdn++)
  {
    v_out_attach_pdn_list.attach_pdn_profile_list[cnt_pdn] = 
      attach_pdn_list.attach_pdn_profile_list[cnt_pdn];
  }


  if (errval == QMI_ERR_NONE)
  {
    if ( FALSE == qmi_svc_put_param_tlv( &response,
                                          WDSI_PRM_TYPE_LTE_ATTACH_PDN_LIST,
                                         (sizeof(v_out_attach_pdn_list.attach_pdn_profile_list_num) +
                                         (v_out_attach_pdn_list.attach_pdn_profile_list_num * 
                                          sizeof(v_out_attach_pdn_list.attach_pdn_profile_list[0])) ),
                                         &v_out_attach_pdn_list ) )
    {      
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

send_result:

  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS
                                   : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;
} /* qmi_wds_get_lte_attach_pdn_list() */

/*===========================================================================
  FUNCTION QMI_WDS_GET_LTE_MAX_ATTACH_PDN_LIST_NUM()

  DESCRIPTION
    Retrieves lte attach pdn list

  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
dsm_item_type * qmi_wds_get_lte_max_attach_pdn_list_num
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *                     response;
  qmi_error_e_type                    errval;
  qmi_result_e_type                   result;
  boolean                             retval;
  int16                               ps_errno = 0;
  int16                               ps_result;
  ps_sys_ioctl_3gpp_lte_max_attach_pdn_num_type  max_attach_pdn_num_info;
  uint8                               v_out_max_attach_pdn_num;
  uint32                               subs_id;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);
  ASSERT(sdu_in);

  response = NULL;
  errval = QMI_ERR_NONE;

  v_out_max_attach_pdn_num = 0;
  subs_id = qmi_wdsi_resolve_client_subs(cl_sp);
  /*-------------------------------------------------------------------------
    Retrieve max attach pdn list number by calling into PS Sys Layer
  -------------------------------------------------------------------------*/
  ps_result = ps_sys_ioctl_ex (PS_SYS_TECH_3GPP,
                            PS_SYS_IOCTL_3GPP_GET_LTE_MAX_ATTACH_PDN_NUM,
                               (ps_sys_subscription_enum_type)subs_id,
                            (void *)&max_attach_pdn_num_info,
                            &ps_errno);
  if (ps_result < 0)
  { 
    LOG_MSG_ERROR_2("ps_sys_ioctl %d operation returned : (%d)",
                    PS_SYS_IOCTL_3GPP_GET_LTE_MAX_ATTACH_PDN_NUM, ps_errno);
    switch (ps_errno)
    {                                   
      case DS_NOMEMORY:
        errval = QMI_ERR_NO_MEMORY;
        break;

      case DS_EOPNOTSUPP:
        errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
        break;

      default:
        errval = QMI_ERR_INTERNAL;
        break;
    } 
    goto send_result;
  }
  
  v_out_max_attach_pdn_num = max_attach_pdn_num_info;

  if (errval == QMI_ERR_NONE)
  {
    if ( FALSE == qmi_svc_put_param_tlv( &response,
                                         WDSI_PRM_TYPE_GET_LTE_MAX_ATTACH_PDN_NUM,
                                         sizeof(uint8),
                                         &v_out_max_attach_pdn_num ))
    {      
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

send_result:

  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS
                                   : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;
} /* qmi_wds_get_lte_max_attach_pdn_list_num() */

/*===========================================================================
  FUNCTION QMI_WDS_SET_LTE_DATA_RETRY()

  DESCRIPTION
    Modifies the LTE Data Retry setting.

  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
dsm_item_type*  qmi_wds_set_lte_data_retry
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *	 response = NULL;
  qmi_error_e_type	 errval = QMI_ERR_NONE;
  qmi_result_e_type  result;
  boolean			 retval;
  
  int16 return_val = DSS_SUCCESS, ps_errno;

  boolean  got_v_in_req;
  uint8    type;
  uint16   len;
  void *   value;
  uint16   expected_len;

  ps_sys_ioctl_lte_data_retry_type           lte_data_retry_val;
  uint32                               subs_id;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp);
  ASSERT(sdu_in);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);

  response = NULL;

  errval = QMI_ERR_NONE;
  subs_id = qmi_wdsi_resolve_client_subs(cl_sp);

  memset(&lte_data_retry_val, 0, sizeof(lte_data_retry_val));
  while (*sdu_in)
  {
    if( !qmi_svc_get_tl(sdu_in, &type, &len) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        expected_len = WDSI_PRM_TYPE_BOOLEAN_FLAG_LEN;
        value = (void *) &lte_data_retry_val.lte_data_retry;
        got_v_in_req = TRUE;
        break;

      default:
        LOG_MSG_INFO2_1("Unrecognized TLV type (%d)", type);
        break;
    }

     if(!qmi_svc_validate_type_length( type,
                                       len,
                                       expected_len,
                                       FILE_ID_DS_QMI_WDS_LTE,__LINE__)  )
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    /*-----------------------------------------------------------------------
      if value is NULL, skip past the current TLV
    -----------------------------------------------------------------------*/
    if(len != qmi_svc_dsm_pullup( sdu_in, value, len ,
                       FILE_ID_DS_QMI_WDS_LTE,__LINE__ ))
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

  }

  if ( !got_v_in_req )
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  if ((lte_data_retry_val.lte_data_retry != TRUE) &&
      (lte_data_retry_val.lte_data_retry != FALSE))
  {
    errval = QMI_ERR_INVALID_ARG;
    goto send_result;
  }

  /*-------------------------------------------------------------------------
    Call the ioctl and check result
  -------------------------------------------------------------------------*/
  return_val = ps_sys_ioctl_ex ( PS_SYS_TECH_3GPP,
                              PS_SYS_IOCTL_3GPP_SET_LTE_DATA_RETRY,
                                 (ps_sys_subscription_enum_type)subs_id,
                              &lte_data_retry_val,
                              &ps_errno );

  if (return_val != DSS_SUCCESS)
  {
    LOG_MSG_ERROR_2("ps_sys_ioctl ret [%d] errno [%d]", return_val, ps_errno);
    switch (ps_errno)
    {
      case DS_NOMEMORY:
        errval = QMI_ERR_NO_MEMORY;
        break;
      case DS_EOPNOTSUPP:
      case DS_EINVAL:
         errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
      default:
        errval = QMI_ERR_INTERNAL;
        break;
    }
  }

send_result:
    result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS
                                     : QMI_RESULT_FAILURE);

    retval = qmi_svc_put_result_tlv(&response, result, errval);
    CHECK_RETVAL();
    return response;
} /* qmi_wds_set_lte_data_retry() */

/*===========================================================================
  FUNCTION QMI_WDS_GET_LTE_DATA_RETRY()

  DESCRIPTION
    Retrieves the LTE Data Retry setting.

  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
dsm_item_type*  qmi_wds_get_lte_data_retry
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *	 response = NULL;
  qmi_error_e_type	 errval = QMI_ERR_NONE;
  qmi_result_e_type  result;
  boolean			 retval;
  
  int16 return_val = DSS_SUCCESS, ps_errno;

  ps_sys_ioctl_lte_data_retry_type           lte_data_retry_val;
  uint32                               subs_id;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp);
  ASSERT(sdu_in);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);

  response = NULL;
  subs_id = qmi_wdsi_resolve_client_subs(cl_sp);

  errval = QMI_ERR_NONE;

  memset(&lte_data_retry_val, 0, sizeof(lte_data_retry_val));

  /*-------------------------------------------------------------------------
    Call the ioctl and check result
  -------------------------------------------------------------------------*/
  return_val = ps_sys_ioctl_ex ( PS_SYS_TECH_3GPP,
                              PS_SYS_IOCTL_3GPP_GET_LTE_DATA_RETRY,
                                 (ps_sys_subscription_enum_type)subs_id,
                              &lte_data_retry_val,
                              &ps_errno );

  if (return_val != DSS_SUCCESS)
  {
    LOG_MSG_ERROR_2("ps_sys_ioctl ret [%d] errno [%d]", return_val, ps_errno);
    switch (ps_errno)
    {
      case DS_NOMEMORY:
        errval = QMI_ERR_NO_MEMORY;
        break;
      case DS_EOPNOTSUPP:
      case DS_EINVAL:
         errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
      default:
        errval = QMI_ERR_INTERNAL;
        break;
    }
    goto send_result;
  }

  if ( FALSE == qmi_svc_put_param_tlv( &response,
                                        WDSI_PRM_TYPE_LTE_DATA_RETRY,
                                        sizeof(lte_data_retry_val.lte_data_retry),
                                        &lte_data_retry_val.lte_data_retry) )
  {      
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
  }

send_result:
    result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS
                                     : QMI_RESULT_FAILURE);

    retval = qmi_svc_put_result_tlv(&response, result, errval);
    CHECK_RETVAL();
    return response;
} /* qmi_wds_get_lte_data_retry() */
/*===========================================================================
  FUNCTION QMI_WDS_SET_LTE_ATTACH_TYPE()

  DESCRIPTION
    Modifies the LTE Attach Type setting.

  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
dsm_item_type*  qmi_wds_set_lte_attach_type
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *	 response = NULL;
  qmi_error_e_type	 errval = QMI_ERR_NONE;
  qmi_result_e_type  result;
  boolean			 retval;
  
  int16 return_val = DSS_SUCCESS, ps_errno;

  boolean  got_v_in_req;
  uint8    type;
  uint16   len;
  void *   value;
  uint16   expected_len;

  uint32   lte_attach_val = 0;
  ps_sys_lte_attach_enum_type           lte_attach_type;
  uint32                               subs_id;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp);
  ASSERT(sdu_in);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);

  response = NULL;

  errval = QMI_ERR_NONE;
  subs_id = qmi_wdsi_resolve_client_subs(cl_sp);

  while (*sdu_in)
  {
    if( !qmi_svc_get_tl(sdu_in, &type, &len) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        expected_len = sizeof(lte_attach_val);
        value = (void *) &lte_attach_val;
        got_v_in_req = TRUE;
        break;

      default:
        LOG_MSG_INFO2_1("Unrecognized TLV type (%d)", type);
        break;
    }

    if(!qmi_svc_validate_type_length( type,
                                       len,
                                       expected_len,
                                       FILE_ID_DS_QMI_WDS_LTE,__LINE__)  )
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    /*-----------------------------------------------------------------------
      if value is NULL, skip past the current TLV
    -----------------------------------------------------------------------*/
    if(len != qmi_svc_dsm_pullup( sdu_in, value, len ,
                         FILE_ID_DS_QMI_WDS_LTE,__LINE__ ))
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

  }

  if ( !got_v_in_req )
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  /* Validate the input */
  if ((lte_attach_val != PS_SYS_LTE_ATTACH_TYPE_INITIAL) &&
      (lte_attach_val != PS_SYS_LTE_ATTACH_TYPE_HANDOFF))
  {
    errval = QMI_ERR_INVALID_ARG;
    goto send_result;
  }

  lte_attach_type = (ps_sys_lte_attach_enum_type)lte_attach_val;
  /*-------------------------------------------------------------------------
    Call ps_sys_conf_set and check result
  -------------------------------------------------------------------------*/
  return_val = ps_sys_conf_set_ex ( PS_SYS_TECH_3GPP,
                                 PS_SYS_CONF_3GPP_LTE_ATTACH_TYPE,
                                    (ps_sys_subscription_enum_type)subs_id,
                                 &lte_attach_type,
                                 &ps_errno );

  if (return_val != DSS_SUCCESS)
  {
    LOG_MSG_ERROR_2("ps_sys_conf_set ret [%d] errno [%d]",
                    return_val, ps_errno);
    switch (ps_errno)
    {
      case DS_NOMEMORY:
        errval = QMI_ERR_NO_MEMORY;
        break;
      case DS_EOPNOTSUPP:
      case DS_EINVAL:
         errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
      default:
        errval = QMI_ERR_INTERNAL;
        break;
    }
  }

send_result:
    result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS
                                     : QMI_RESULT_FAILURE);

    retval = qmi_svc_put_result_tlv(&response, result, errval);
    CHECK_RETVAL();
    return response;
} /* qmi_wds_set_lte_attach_type() */

/*===========================================================================
  FUNCTION QMI_WDS_GET_LTE_ATTACH_TYPE()

  DESCRIPTION
    Retrieves the LTE Attach Type setting.

  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
dsm_item_type*  qmi_wds_get_lte_attach_type
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *	 response = NULL;
  qmi_error_e_type	 errval = QMI_ERR_NONE;
  qmi_result_e_type  result;
  boolean			 retval;
  
  int16 return_val = DSS_SUCCESS, ps_errno;

  uint32   lte_attach_val = 0;
  ps_sys_lte_attach_enum_type           lte_attach_type;
  uint32                               subs_id;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sp);
  ASSERT(sdu_in);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);

  response = NULL;

  errval = QMI_ERR_NONE;
  subs_id = qmi_wdsi_resolve_client_subs(cl_sp);

  memset(&lte_attach_type, 0, sizeof(lte_attach_type));

  /*-------------------------------------------------------------------------
    Call the ioctl and check result
  -------------------------------------------------------------------------*/
  return_val = ps_sys_conf_get_ex ( PS_SYS_TECH_3GPP,
                                 PS_SYS_CONF_3GPP_LTE_ATTACH_TYPE,
                                    (ps_sys_subscription_enum_type)subs_id,
                                 &lte_attach_type,
                                 &ps_errno );

  if (return_val != DSS_SUCCESS)
  {
    LOG_MSG_ERROR_2("ps_sys_conf_get ret [%d] errno [%d]",
                    return_val, ps_errno);
    switch (ps_errno)
    {
      case DS_NOMEMORY:
        errval = QMI_ERR_NO_MEMORY;
        break;
      case DS_EOPNOTSUPP:
      case DS_EINVAL:
         errval = QMI_ERR_OP_DEVICE_UNSUPPORTED;
      default:
        errval = QMI_ERR_INTERNAL;
        break;
    }
    goto send_result;
  }

  lte_attach_val = lte_attach_type;
  if ( FALSE == qmi_svc_put_param_tlv( &response,
                                        WDSI_PRM_TYPE_LTE_ATTACH_TYPE,
                                        sizeof(lte_attach_val),
                                        &lte_attach_val) )
  {      
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
  }

send_result:
    result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS
                                     : QMI_RESULT_FAILURE);

    retval = qmi_svc_put_result_tlv(&response, result, errval);
    CHECK_RETVAL();
    return response;
} /* qmi_wds_get_lte_attach_type() */

/*===========================================================================
  FUNCTION QMI_WDS_UPDATE_LTE_ATTACH_PDN_LIST_PROFILES()

  DESCRIPTION
    Updates the LTE attach PDN list profile params

  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
dsm_item_type * qmi_wds_update_lte_attach_pdn_list_profiles
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  uint32 subs_id;
  ASSERT(sp && cmd_buf_p && cl_sp && sdu_in);
  subs_id = qmi_wdsi_resolve_client_subs(cl_sp);
  
  /* 3gpp Specific Function */
  return qmi_wds_profile_update_lte_attach_pdn_list_params(WDS_PROFILE_TYPE_3GPP, subs_id);

} /* qmi_wds_update_lte_attach_pdn_list_profiles() */
#endif /* defined(FEATURE_DATA_LTE)*/
