/*===========================================================================
 
                         D S _ Q M I _ W D S _ P R O F I L E . C
 
DESCRIPTION
 
 The Data Services Qualcomm Wireless Data Services MSM Interface source file.
 
EXTERNALIZED FUNCTIONS
 
  QMI_WDS_PROFILE_MODIFY_SETTINGS()
    Modify profile settings 
  
  QMI_WDS_PROFILE_GET_SETTINGS()
    Get the settings for a given profile.
  
  QMI_WDS_PROFILE_GET_DEFAULT_SETTINGS()
    Get the settings of default profile.
  
  QMI_WDS_PROFILE_GET_LIST()
    Get the Profile list
  
  QMI_WDS_PROFILE_GET_DEFAULT_PROFILE_NUM()
    Get the default profile number.
  
  QMI_WDS_PROFILE_SET_DEFAULT_PROFILE_NUM()
    Set the default profile number.
  
  QMI_WDS_PROFILE_RESET_PROFILE_TO_DEFAULT()
    Reset the profile parameters to default.
  
  QMI_WDS_PROFILE_RESET_PARAM_TO_INVALID()
    Reset a given profile parameter to invalid.
  
  QMI_WDS_PROFILE_CREATE()
    Create a new Profile.
  
  QMI_WDS_PROFILE_DELETE()
    Delete a profile.
  
  QMI_WDS_PROFILE_UMTS_VALIDATE_PDP_TYPE()
    Validate PDP Type for a UMTS Profile.
  
 QMI_WDS_PROFILE_RETRIEVE_DEFAULT_PROFILE_NUM()
    Get the default profile number value. 
  
Copyright (c) 2009-2012 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/
/*===========================================================================

                      EDIT HISTORY FOR FILE

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/qmidata/src/ds_qmi_wds_profile.c#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when        who    what, where, why
--------    ---    ---------------------------------------------------------- 
4/15/12     ab     Convert IPV4 addresses to big endian for 3GPP/3GPP2 profiles
12/19/12    ab     Add PDN Throttling Timer 3GPP2 profile parameters  
12/19/12    ab     Add Disallow Timer 3GPP2 profile parameters 
12/04/12    svj    Add PCO related 3gpp and 3gpp2 profile parameters 
10/25/12    svj    Add support emergency calls 3GPP profile parameter 
09/10/12    sb     Added Len checks for QMI TLVs
04/16/12    wc     Add APN Beare 3GPP profile parameter
11/29/11    wc     Add PDN level auth/PDN label 3GPP2 profile parameters
05/25/11    wc     Support persistent/non-persistent profile creation
12/17/10    kk     Fix to update the valid fields mask after updating 
                   parameters from the default profile.
06/21/10    kk     Fix to validate parameters in MODIFY_PROFILE request.
04/26/09    rt     Created Module.
===========================================================================*/

/*===========================================================================
 
  INCLUDE FILES FOR MODULE
 
===========================================================================*/

#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"
#include "amssassert.h"

#include "ds_qmi_wds_profile.h"

#include "ps_byte.h"

#include "ds_qmi_wds.h"
#include "ds_qmi_svc.h"

#include "ds_profile.h"

//#if defined (FEATURE_WCDMA) || defined (FEATURE_GSM) || defined(FEATURE_LTE)
  #include "ds_profile_3gpp.h"
//#endif /* defined (FEATURE_WCDMA) || defined (FEATURE_GSM) || defined(FEATURE_LTE)*/

//#ifdef FEATURE_CDMA
#include "ds_profile_3gpp2.h"
//#endif /* FEATURE_CDMA */
#include "ds_Utils_DebugMsg.h"

#include <stringl/stringl.h>

/*===========================================================================
                              INTERNAL MACROS                                
===========================================================================*/

/*---------------------------------------------------------------------------
  Maximum length of any profile parameter value.
---------------------------------------------------------------------------*/
#define   WDS_MAX_PROFILE_PARAM_VAL_LEN    150

#define WDSI_PRM_TYPE_EXT_ERROR_INFO_LEN         (0x02)

/*---------------------------------------------------------------------------
 Length of Header for Profile Lists. 
 Type (3GPP, 3GPP2), Profile Number, Length of name.
---------------------------------------------------------------------------*/
#define   PROFILE_LIST_PARAM_HDR_LENGTH  3

/*---------------------------------------------------------------------------
  Macro used for error handling.
---------------------------------------------------------------------------*/
#define CHECK_RETVAL()  if (FALSE == retval) { dsm_free_packet(&response); \
                                               return NULL; }

/*===========================================================================
                                 DEFINITIONS                                 
===========================================================================*/

#ifdef FEATURE_CDMA
typedef struct
{
  ds_profile_3gpp2_param_enum_type   param_type;
  uint16                            param_len;  
}qmi_profile_3gpp2_param_tl_list_type;

/* Param length for Strings is 0*/

static const qmi_profile_3gpp2_param_tl_list_type qmi_profile_3gpp2_param_tl_list[] = 
{
  /*0x90*/
  { DS_PROFILE_3GPP2_PROFILE_PARAM_NEGOTIATE_DNS_SERVER, 
    sizeof(boolean) },
  
  /*0x91*/
  { DS_PROFILE_3GPP2_PROFILE_PARAM_SESSION_CLOSE_TIMER_DO, 
    sizeof(uint32) },
  
  /*0x92*/
  { DS_PROFILE_3GPP2_PROFILE_PARAM_SESSION_CLOSE_TIMER_1X, 
    sizeof(uint32) },
  
  /*0x93*/
  { DS_PROFILE_3GPP2_PROFILE_PARAM_ALLOW_LINGER, 
    sizeof(boolean) },
  
  /*0x94*/
  { DS_PROFILE_3GPP2_PROFILE_PARAM_LCP_ACK_TIMEOUT, 
    sizeof(uint16) },
  
  /*0x95*/
  { DS_PROFILE_3GPP2_PROFILE_PARAM_IPCP_ACK_TIMEOUT, 
    sizeof(uint16) },
  
  /*0x96*/
  { DS_PROFILE_3GPP2_PROFILE_PARAM_AUTH_TIMEOUT, 
    sizeof(uint16) },
  
  /*0x97*/
  { DS_PROFILE_3GPP2_PROFILE_PARAM_LCP_CREQ_RETRY_COUNT, 
    sizeof(uint8) },
  
  /*0x98*/
  { DS_PROFILE_3GPP2_PROFILE_PARAM_IPCP_CREQ_RETRY_COUNT, 
    sizeof(uint8) },
  
  /*0x99*/
  { DS_PROFILE_3GPP2_PROFILE_PARAM_AUTH_RETRY_COUNT, 
    sizeof(uint8) },
  
  /*0x9A*/
  { DS_PROFILE_3GPP2_PROFILE_PARAM_AUTH_PROTOCOL, 
    sizeof(uint8) },
  
  /*0x9B*/
  { DS_PROFILE_3GPP2_PROFILE_PARAM_USER_ID,
    0 },
  
  /*0x9C*/
  { DS_PROFILE_3GPP2_PROFILE_PARAM_AUTH_PASSWORD,
    0 },
  
  /*0x9D*/
  { DS_PROFILE_3GPP2_PROFILE_PARAM_DATA_RATE, 
    sizeof(uint8) },
  
  /*0x9E*/
  { DS_PROFILE_3GPP2_PROFILE_PARAM_DATA_MODE, 
    sizeof(uint8) },
  
  /*0x9F*/
  { DS_PROFILE_3GPP2_PROFILE_PARAM_APP_TYPE, 
    sizeof(uint32) },
  
  /*0xA0*/
  { DS_PROFILE_3GPP2_PROFILE_PARAM_APP_PRIORITY, 
    sizeof(uint8) },
  
  /*0xA1*/
  { DS_PROFILE_3GPP2_PROFILE_PARAM_APN_STRING, 
    0 },
  
  /*0xA2*/
  { DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_TYPE, 
    sizeof(ds_profile_3gpp2_pdn_type_enum_type) },
  
  /*0xA3*/
  { DS_PROFILE_3GPP2_PROFILE_PARAM_IS_PCSCF_ADDR_NEEDED, 
    sizeof(boolean) },
  
  /*0xA4*/
  { DS_PROFILE_3GPP2_PROFILE_PARAM_V4_DNS_ADDR_PRIMARY,
    sizeof(ds_profile_3gpp2_in_addr_type) },
  
  /*0xA5*/
  { DS_PROFILE_3GPP2_PROFILE_PARAM_V4_DNS_ADDR_SECONDARY, 
    sizeof(ds_profile_3gpp2_in_addr_type) },
  
  /*0xA6*/
  { DS_PROFILE_3GPP2_PROFILE_PARAM_V6_DNS_ADDR_PRIMARY,
    sizeof(ds_profile_3gpp2_in6_addr_type) },
  
  /*0xA7*/
  { DS_PROFILE_3GPP2_PROFILE_PARAM_V6_DNS_ADDR_SECONDARY, 
    sizeof(ds_profile_3gpp2_in6_addr_type) },
  
  /*0xA8*/
  { DS_PROFILE_3GPP2_PROFILE_PARAM_RAT_TYPE, 
    sizeof(ds_profile_3gpp2_rat_type_enum_type) },

  /*0xA9*/
  { DS_PROFILE_3GPP2_PROFILE_PARAM_APN_ENABLED, 
    sizeof(boolean) },

  /*0xAA*/
  { DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_INACTIVITY_TIMEOUT, 
    sizeof(uint32) },

  /*0xAB*/
  { DS_PROFILE_3GPP2_PROFILE_PARAM_APN_CLASS, 
    sizeof(uint8) },

  /*0xAD*/
  { DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_LEVEL_AUTH_PROTOCOL, 
    sizeof(uint8) },

  /*0xAE*/
  { DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_LEVEL_USER_ID, 
    0 },

  /*0xAF*/
  { DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_LEVEL_AUTH_PASSWORD, 
    0 },

  /*0xB0*/
  { DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_LABEL, 
    0 },

  /*0xBD*/
  { DS_PROFILE_3GPP2_PROFILE_PARAM_OP_PCO_ID, 
    sizeof(uint16) },

  /*0xBE*/
  { DS_PROFILE_3GPP2_PROFILE_PARAM_MCC, 
    sizeof(uint16) },

  /*0xBF*/
  { DS_PROFILE_3GPP2_PROFILE_PARAM_MNC, 
    sizeof(ds_profile_3gpp2_mnc_type) },

  /*0xC0*/
  { DS_PROFILE_3GPP2_PROFILE_PARAM_FAILURE_TIMERS,
    sizeof(ds_profile_3gpp2_failure_timers_type) },

  /*0xC1*/
  { DS_PROFILE_3GPP2_PROFILE_PARAM_DISALLOW_TIMERS,
    sizeof(ds_profile_3gpp2_failure_timers_type) },

  /*0xC2*/
  { DS_PROFILE_3GPP2_PROFILE_PARAM_USER_PROFILE_CLASS,
    sizeof(ds_profile_3gpp2_user_profile_class) }, 

  /*DS_PROFILE_3GPP2_PROFILE_PARAM_MAX*/
  { DS_PROFILE_3GPP2_PROFILE_PARAM_SUBS_ID,
    sizeof(ds_profile_subs_etype)}
};
#endif /* FEATURE_CDMA */

#if defined (FEATURE_WCDMA) || defined (FEATURE_GSM) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA)
typedef struct
{
  ds_profile_3gpp_param_enum_type   param_type;
  uint16                            param_len;  
}qmi_profile_3gpp_param_tl_list_type;

/* Param length for Strings is 0*/
static const qmi_profile_3gpp_param_tl_list_type qmi_profile_3gpp_param_tl_list[] = 
{
  /*0x10*/
  { DS_PROFILE_3GPP_PROFILE_PARAM_PROFILE_NAME, 
    0 },
  
  /*0x11*/
  { DS_PROFILE_3GPP_PROFILE_PARAM_PDP_CONTEXT_PDP_TYPE,
    sizeof (ds_profile_3gpp_pdp_type_enum_type) },
  
  /*0x12*/
  { DS_PROFILE_3GPP_PROFILE_PARAM_PDP_CONTEXT_H_COMP,
    sizeof (ds_profile_3gpp_pdp_header_comp_e_type) },
  
  /*0x13*/
  { DS_PROFILE_3GPP_PROFILE_PARAM_PDP_CONTEXT_D_COMP,
    sizeof(ds_profile_3gpp_pdp_data_comp_e_type) },
  
  /*0x14*/
  { DS_PROFILE_3GPP_PROFILE_PARAM_PDP_CONTEXT_APN,
    0 },
  
  /*0x15*/
  { DS_PROFILE_3GPP_PROFILE_PARAM_DNS_ADDR_V4_PRIMARY,
    sizeof(uint32) },
  
  /*0x16*/
  { DS_PROFILE_3GPP_PROFILE_PARAM_DNS_ADDR_V4_SECONDARY,
    sizeof(uint32) },
  
  /*0x17*/
  { DS_PROFILE_3GPP_PROFILE_PARAM_UMTS_REQ_QOS_EXTENDED,
    33 },
  
  /*0x18*/
  { DS_PROFILE_3GPP_PROFILE_PARAM_UMTS_MIN_QOS_EXTENDED,
    33 },
  
  /*0x19*/
  { DS_PROFILE_3GPP_PROFILE_PARAM_GPRS_REQ_QOS,
    sizeof(ds_profile_3gpp_gprs_qos_params_type) },
  
  /*0x1A*/
  { DS_PROFILE_3GPP_PROFILE_PARAM_GPRS_MIN_QOS,
    sizeof(ds_profile_3gpp_gprs_qos_params_type) },

  /*0x1B*/
  { DS_PROFILE_3GPP_PROFILE_PARAM_AUTH_USERNAME,
    0},
  
  /*0x1C*/
  { DS_PROFILE_3GPP_PROFILE_PARAM_AUTH_PASSWORD,
    0},

  /*0x1D*/
  { DS_PROFILE_3GPP_PROFILE_PARAM_AUTH_TYPE,
    sizeof (ds_profile_3gpp_auth_pref_type) },

  /*0x1E*/
  { DS_PROFILE_3GPP_PROFILE_PARAM_PDP_CONTEXT_PDP_ADDR_V4,
    sizeof(uint32) },
  
  /*0x1F*/
  { DS_PROFILE_3GPP_PROFILE_PARAM_PCSCF_REQ_FLAG,
    sizeof(ds_profile_3gpp_request_pcscf_address_flag_type) },

  /*0x20*/
  { DS_PROFILE_3GPP_PROFILE_PARAM_PDP_CONTEXT_TE_MT_ACCESS_CTRL_FLAG,
    sizeof(ds_profile_3gpp_pdp_access_control_e_type) },
  
  /*0x21*/
  { DS_PROFILE_3GPP_PROFILE_PARAM_PCSCF_DHCP_REQ_FLAG,
    sizeof(ds_profile_3gpp_request_pcscf_address_using_dhcp_flag_type) },
  
  /*0x22*/
  { DS_PROFILE_3GPP_PROFILE_PARAM_IM_CN_FLAG,
    sizeof(ds_profile_3gpp_im_cn_flag_type) },
  
  /*0x23*/
  { DS_PROFILE_3GPP_PROFILE_PARAM_TFT_FILTER_ID1,
    sizeof(ds_profile_3gpp_tft_params_type) },
  
  /*0x24*/
  { DS_PROFILE_3GPP_PROFILE_PARAM_TFT_FILTER_ID2,
    sizeof(ds_profile_3gpp_tft_params_type) },
  
  /*0x25*/
  { DS_PROFILE_3GPP_PROFILE_PARAM_PDP_CONTEXT_NUMBER,
    sizeof(byte) },
  
  /*0x26*/
  { DS_PROFILE_3GPP_PROFILE_PARAM_PDP_CONTEXT_SECONDARY_FLAG,
    sizeof(boolean) },
  
  /*0x27*/
  { DS_PROFILE_3GPP_PROFILE_PARAM_PDP_CONTEXT_PRIMARY_ID,
    sizeof(byte) },
  
  /*0x28*/
  { DS_PROFILE_3GPP_PROFILE_PARAM_PDP_CONTEXT_PDP_ADDR_V6,
    sizeof(ds_profile_3gpp_pdp_addr_type_ipv6) },
  
  /*0x29*/
  { DS_PROFILE_3GPP_PROFILE_PARAM_UMTS_REQ_QOS,
    34 },
  
  /*0x2A*/
  { DS_PROFILE_3GPP_PROFILE_PARAM_UMTS_MIN_QOS,
    34 },
  
  /*0x2B*/
  { DS_PROFILE_3GPP_PROFILE_PARAM_DNS_ADDR_V6_PRIMARY,
    sizeof(ds_profile_3gpp_pdp_addr_type_ipv6) },
  
  /*0x2C*/
  { DS_PROFILE_3GPP_PROFILE_PARAM_DNS_ADDR_V6_SECONDARY,
    sizeof(ds_profile_3gpp_pdp_addr_type_ipv6) },

  /*0x2D*/
  { DS_PROFILE_3GPP_PROFILE_PARAM_IPV4_ADDR_ALLOC,
    sizeof(ds_profile_3gpp_pdp_ipv4_addr_alloc_e_type) },

  /*0x2E*/
  { DS_PROFILE_3GPP_PROFILE_PARAM_LTE_REQ_QOS,
    sizeof(ds_profile_3gpp_lte_qos_params_type) },

  /*0x2F*/
  { DS_PROFILE_3GPP_PROFILE_PARAM_APN_DISABLE_FLAG,
    sizeof(boolean) },

  /*0x30*/
  { DS_PROFILE_3GPP_PROFILE_PARAM_INACTIVITY_TIMER_VAL,
    sizeof(uint32) },

  /*0x31*/
  { DS_PROFILE_3GPP_PROFILE_PARAM_APN_CLASS,
    sizeof(uint8) },

  /*0x35*/
  { DS_PROFILE_3GPP_PROFILE_PARAM_APN_BEARER,
    sizeof(uint64) },

  /*0x36*/
  { DS_PROFILE_3GPP_PROFILE_PARAM_EMERGENCY_CALLS_SUPPORTED,
    sizeof(boolean) },

  /*0x37*/
  { DS_PROFILE_3GPP_PROFILE_PARAM_OPERATOR_RESERVED_PCO,
    sizeof(uint16) },

  /*0x38*/
  { DS_PROFILE_3GPP_PROFILE_PARAM_MCC,
    sizeof(uint16) },

  /*0x39*/
  { DS_PROFILE_3GPP_PROFILE_PARAM_MNC,
    sizeof(ds_profile_3gpp_mnc_type) },

  /*0x3A*/
  { DS_PROFILE_3GPP_PROFILE_MAX_PDN_CONN_PER_BLOCK,
    sizeof(uint16) },
  
  /*0x3B*/
  { DS_PROFILE_3GPP_PROFILE_MAX_PDN_CONN_TIMER,
    sizeof(uint16) },
  
  /*0x3C*/
  { DS_PROFILE_3GPP_PROFILE_PDN_REQ_WAIT_TIMER,
    sizeof(uint16) },

  /*0x3D*/
  { DS_PROFILE_3GPP_PROFILE_USER_APP_DATA,
    sizeof(uint32) },
  
  /*0x3E*/
  { DS_PROFILE_3GPP_PROFILE_PARAM_ROAMING_DISALLOWED,
    sizeof(boolean) },

  /*0x3F*/
  { DS_PROFILE_3GPP_PROFILE_PARAM_PDN_DISCON_WAIT_TIME,
    sizeof(uint8) }
  
};
#endif /* defined(FEATURE_WCDMA) || defined(FEATURE_GSM) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA)*/


/*===========================================================================
 
                    INTERNAL FUNCTION DEFINITIONS
 
===========================================================================*/

/*===========================================================================
  FUNCTION QMI_WDS_PROFILE_GET_PARAM_LEN()
 
  DESCRIPTION
    Get the max length of the parameter value 
 
  PARAMETERS
    profile_param_id : Parameter identifier.
    tech_type        : Tech type of the parameter (3GPP/3GPP2)
    param_len        : Maximum Length of the param.
 
  RETURN VALUE
    0 (QMI_ERR_NONE) : Success.
    Any other value  : Failure
  
  DEPENDENCIES
    None
 
  SIDE EFFECTS
    None
===========================================================================*/
static qmi_error_e_type qmi_wds_profile_get_param_len
(
 ds_profile_identifier_type   profile_param_id,
 uint8                        tech_type,
 uint16                     * param_len
)
{
  uint8     array_size;
  uint8     idx;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  array_size = 0;

/* Param length for Strings is 0*/
  switch(tech_type)
  {
    case WDS_PROFILE_TYPE_3GPP:
    #if defined(FEATURE_WCDMA) || defined(FEATURE_GSM) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA)
      array_size = sizeof(qmi_profile_3gpp_param_tl_list)/
                     sizeof(qmi_profile_3gpp_param_tl_list_type);
      for(idx=0; idx < array_size; idx++ )
      {
        if(qmi_profile_3gpp_param_tl_list[idx].param_type == 
            profile_param_id )
        {
          *param_len = qmi_profile_3gpp_param_tl_list[idx].param_len;
          return QMI_ERR_NONE;
        }
      }
    #endif /* defined(FEATURE_WCDMA) || defined(FEATURE_GSM) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA)*/
      break;
    case WDS_PROFILE_TYPE_3GPP2:
    #ifdef FEATURE_CDMA
      array_size = sizeof(qmi_profile_3gpp2_param_tl_list)/
                     sizeof(qmi_profile_3gpp2_param_tl_list_type);
      for(idx=0; idx < array_size; idx++ )
      {
        if(qmi_profile_3gpp2_param_tl_list[idx].param_type == 
            profile_param_id )
        {
          *param_len = qmi_profile_3gpp2_param_tl_list[idx].param_len;
          return QMI_ERR_NONE;
        }
      }
    #endif /* FEATURE_CDMA */
      break;
    default:
      LOG_MSG_ERROR_1("Invalid Technology Type %d", tech_type);
      return QMI_ERR_INVALID_PROFILE_TYPE;
  }

  LOG_MSG_ERROR_1("No match found for param id %d", profile_param_id);
  return QMI_ERR_MALFORMED_MSG;
  
} /* ds_qmi_wds_profile_get_param_len() */


/*===========================================================================
  FUNCTION QMI_WDS_PROFILE_GET_PARAM_ID_INFO()
 
  DESCRIPTION
    Get the settings for a parameter for a given profile.
 
  PARAMETERS
    profile_type     : profile type (3GPP/3GPP2)
    profile_index    : profile whose settings are to be retrieved
 
  RETURN VALUE
    ptr to response
 
  DEPENDENCIES
    None
 
  SIDE EFFECTS
    None
 ==========================================================================*/
qmi_error_e_type qmi_wds_profile_get_param_id_info
(
  uint8                        profile_type,
  uint8                        profile_index,
  uint32                       subs_id,
  ds_profile_identifier_type   profile_identifier,
  ds_profile_info_type        *param_info
)
{
  qmi_error_e_type                  errval;
  ds_profile_status_etype           profile_status;
  ds_profile_hndl_type              profile_hndl;
  uint16                            param_len;
  boolean                           cancel_txn;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  
  memset(&profile_hndl, 0, sizeof(profile_hndl));
  errval      = QMI_ERR_NONE;
  profile_status = DS_PROFILE_REG_RESULT_SUCCESS;
  cancel_txn = FALSE;
  param_len = 0;

  /*-------------------------------------------------------------------------
    Begin transaction
  -------------------------------------------------------------------------*/
  profile_status = 
    ds_profile_begin_transaction_per_sub(DS_PROFILE_TRN_READ,
                                        (ds_profile_tech_etype)profile_type,
                                        (ds_profile_num_type)profile_index,
                                        (ds_profile_subs_etype)subs_id,
                                        &profile_hndl);
  if (profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
  {
    LOG_MSG_ERROR_1 ("Begin Transaction failed. Error %d", profile_status);
    return QMI_ERR_INTERNAL;
  }

  /*-----------------------------------------------------------------------
    Retrieve profile_identifier param info
  -----------------------------------------------------------------------*/
  profile_status = ds_profile_get_param(profile_hndl, 
                                        profile_identifier, 
                                        param_info);
  if (profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
  {
    LOG_MSG_ERROR_3("Cannot get Profile param %d for profile number %d Error %d",
                    profile_identifier, profile_index, profile_status);
    cancel_txn = TRUE;
    goto send_result;
  }

  /*-----------------------------------------------------------------------
    Verify that the return length is same as expected length for fixed
    length identifiers.
  -----------------------------------------------------------------------*/
  errval = qmi_wds_profile_get_param_len(profile_identifier, profile_type, 
                                         &param_len);
  if (errval != QMI_ERR_NONE)
  {
    LOG_MSG_ERROR_1("Cannot get the Length for profile param %d",
                    profile_identifier);
    ASSERT(0);
  }
  else if ((param_len != 0 && param_len != param_info->len))
  {
    LOG_MSG_ERROR_3("Invalid Length. Identifier(%d) Expected(%d) Got(%d)",
                    profile_identifier, param_len, param_info->len);
    cancel_txn = TRUE;
    goto send_result;
  }
    
  /*-------------------------------------------------------------------------
    End transaction
  -------------------------------------------------------------------------*/
  profile_status = ds_profile_end_transaction(profile_hndl, 
                                              DS_PROFILE_ACTION_COMMIT);
  if(profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
  {
    LOG_MSG_ERROR_1 ("End Transaction failed. Error %d", profile_status);
    return QMI_ERR_INTERNAL;
  }

send_result:
  if (cancel_txn == TRUE)
  {
    (void)ds_profile_end_transaction(profile_hndl, 
                                     DS_PROFILE_ACTION_CANCEL);
    return QMI_ERR_INTERNAL;
  }
  
  return QMI_ERR_NONE;
} /* qmi_wds_profile_get_param_id_info() */


/*===========================================================================
 
                      EXTERNAL FUNCTION DEFINITIONS
 
===========================================================================*/

/*===========================================================================
  FUNCTION QMI_WDS_PROFILE_MODIFY_SETTINGS()
 
  DESCRIPTION
    Modify an existing profile 
 
  PARAMETERS
    sdu_in       : incoming request
    profile_type : profile type (3GPP/3GPP2)
    profile_index: profile index to be modified.
    subs_id      : subscription identifier   
    error_val_ptr: Pointer containing the error code in case this API fails 
 
  RETURN VALUE
    response
  
  DEPENDENCIES
    None
 
  SIDE EFFECTS
    None
===========================================================================*/
dsm_item_type * qmi_wds_profile_modify_settings
(
  dsm_item_type **    sdu_in,
  uint8               profile_type,
  uint8               profile_index,
  uint32              subs_id,  
  qmi_error_e_type   *error_val_ptr
)
{
  ds_profile_hndl_type              profile_hndl;
  ds_profile_info_type              param_info;
  uint8                             type;
  uint16                            len;
  uint16                            param_len;
  byte                              value[WDS_MAX_PROFILE_PARAM_VAL_LEN];
  qmi_error_e_type                  errval;
  qmi_result_e_type                 result;
  boolean                           retval;
  dsm_item_type *                   response;
  ds_profile_status_etype           profile_status;
  boolean                           cancel_txn;
  uint32                            *ipv4_value;
  ds_profile_subs_etype              profile_subs;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  
  ASSERT(sdu_in);

  *error_val_ptr = QMI_ERR_NONE;

  memset(&profile_hndl, 0, sizeof(profile_hndl));
  memset(&param_info, 0, sizeof(param_info));
  response = NULL;
  errval = QMI_ERR_NONE;
  profile_status = DS_PROFILE_REG_RESULT_SUCCESS;
  cancel_txn = FALSE;

  /*-------------------------------------------------------------------------
    Begin transaction
  -------------------------------------------------------------------------*/
  profile_status = ds_profile_begin_transaction_per_sub(DS_PROFILE_TRN_RW, 
                                                        (ds_profile_tech_etype)profile_type,
                                                        (ds_profile_num_type)profile_index,
                                                        (ds_profile_subs_etype) subs_id,
                                                        &profile_hndl);

  if(profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
  {
    LOG_MSG_ERROR_1 ("Profile begin Tx returns error %d", profile_status);
    errval = QMI_ERR_EXTENDED_INTERNAL;
    goto send_result;
  }

  /*-------------------------------------------------------------------------
    Extract one TLV from the SDU at a time and process it.
  -------------------------------------------------------------------------*/
  while (*sdu_in)
  {
    if( !qmi_svc_get_tl( sdu_in, &type, &len ) )
    {
      continue;
    }

    // Bypass the mandatory TLV and the persistence TLV
    if ( (type == QMI_TYPE_REQUIRED_PARAMETERS) || (type == WDSI_PRM_TYPE_PROFILE_PERSISTENT) )
    {
      if (len > WDS_MAX_PROFILE_PARAM_VAL_LEN)
      {
        LOG_MSG_INFO2_2("T (%d) Invalid L (%d bytes) V - discarding",
                        type, len);
        cancel_txn = TRUE;
        errval = QMI_ERR_ARG_TOO_LONG;
        goto send_result;
      }
      else if(len != qmi_svc_dsm_pullup( sdu_in, (void*)value, len 
                             ,FILE_ID_DS_QMI_WDS_PROFILE,__LINE__  ))
      {
        cancel_txn = TRUE;
        errval = QMI_ERR_MALFORMED_MSG;
        goto send_result;
      }
      continue;
    }

    errval = qmi_wds_profile_get_param_len(type, profile_type, &param_len);

    /*-----------------------------------------------------------------------
      Check for the length of the profile parameter. For fixed length params
      verify the exact length. For variable lenght params make sure they 
      are not longer than the maximum allowed length. 
    -----------------------------------------------------------------------*/
    if (errval != QMI_ERR_NONE)
    {
      LOG_MSG_INFO1_1("Cannot get the Length for profile param type %d", type);
      cancel_txn = TRUE;
      goto send_result;
    }
    else if(!qmi_svc_validate_type_length( type,
                                           len,
                                           param_len,
                                           FILE_ID_DS_QMI_WDS_PROFILE,__LINE__)  )
    {
      errval = QMI_ERR_MALFORMED_MSG;
      cancel_txn = TRUE;
      goto send_result;
    }
    else if ((param_len == 0 && len > WDS_MAX_PROFILE_PARAM_VAL_LEN))
    {
      LOG_MSG_ERROR_2("T (%d) Invalid L (%d bytes) V - discarding", type, len);
      errval = QMI_ERR_ARG_TOO_LONG;
      cancel_txn = TRUE;
      goto send_result;
    }

    /* memset the local place holder for the value. */
    memset(value, 0, sizeof(value));

    /*-----------------------------------------------------------------------
      Get profile_identifier and param info from TLV
    -----------------------------------------------------------------------*/
    param_info.len     = len;
    param_info.buf     = (void*)value;

    /*-----------------------------------------------------------------------
      if value is NULL, skip past the current TLV
    -----------------------------------------------------------------------*/
    if(len != qmi_svc_dsm_pullup( sdu_in, (void*)value, len ,
                     FILE_ID_DS_QMI_WDS_PROFILE,__LINE__  ))
    {
      cancel_txn = TRUE;
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    /*-----------------------------------------------------------------------
      Handling APN bearer param
    -----------------------------------------------------------------------*/
    if (type == DS_PROFILE_3GPP_PROFILE_PARAM_APN_BEARER)
    {
      param_info.len = sizeof(uint8);
      if ( (*(uint64 *)value) & 0x8000000000000000ULL ) 
      {
        *(uint8 *)value = 0xFF;
      }
    }

    /*-----------------------------------------------------------------------
      Handling IPV4 address. Converting them to Big Endian format before
      calling DS Profile API's
    -----------------------------------------------------------------------*/

    if ((type == DS_PROFILE_3GPP_PROFILE_PARAM_DNS_ADDR_V4_PRIMARY)  || 
        (type == DS_PROFILE_3GPP_PROFILE_PARAM_DNS_ADDR_V4_SECONDARY)|| 
        (type == DS_PROFILE_3GPP_PROFILE_PARAM_PDP_CONTEXT_PDP_ADDR_V4) || 
        (type == DS_PROFILE_3GPP2_PROFILE_PARAM_V4_DNS_ADDR_PRIMARY) || 
        (type == DS_PROFILE_3GPP2_PROFILE_PARAM_V4_DNS_ADDR_SECONDARY))
    {
      ipv4_value = (uint32 *) param_info.buf;
      *ipv4_value = ps_htonl(*ipv4_value);
    }

    /*-----------------------------------------------------------------------
      Set the Profile with the value of profile identifier.
    -----------------------------------------------------------------------*/
    profile_status = ds_profile_set_param(profile_hndl, (uint32)type, 
                                          &param_info);

    if(profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
    {
      LOG_MSG_ERROR_2 ("Set Profile returns error. Profile_identifier %d, Error %d",
                       type, profile_status);
      cancel_txn = TRUE;
      errval = QMI_ERR_EXTENDED_INTERNAL;
      goto send_result;
    }
  } /* end while(sdu_in) */

  /*-------------------------------------------------------------------------
    End transaction
  -------------------------------------------------------------------------*/
  profile_status = ds_profile_end_transaction(profile_hndl, 
                                              DS_PROFILE_ACTION_COMMIT);
  if(profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
  {
    LOG_MSG_ERROR_1 ("End Tx Profile returns error %d", profile_status);
    errval = QMI_ERR_EXTENDED_INTERNAL;
    goto send_result;
  }

send_result:
  
  dsm_free_packet(sdu_in);
  
  if (cancel_txn == TRUE)
  {
    (void)ds_profile_end_transaction(profile_hndl, 
                                     DS_PROFILE_ACTION_CANCEL);
  }

  if (profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
  {
    retval = qmi_svc_put_param_tlv(&response,
                                   QMI_TYPE_EXT_ERROR_INFO,
                                   WDSI_PRM_TYPE_EXT_ERROR_INFO_LEN,
                                   (void*)&profile_status);
    CHECK_RETVAL();
  }

  *error_val_ptr = errval;
  
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();

  return response;
} /* qmi_wds_profile_modify_settings */


/*===========================================================================
  FUNCTION QMI_WDS_PROFILE_GET_SETTINGS()
 
  DESCRIPTION
    Get the settings for a given profile.
 
  PARAMETERS
    profile_type     : profile type (3GPP/3GPP2)
    profile_index    : profile whose settings are to be retrieved
 
  RETURN VALUE
    ptr to response
 
  DEPENDENCIES
    None
 
  SIDE EFFECTS
    None
 ==========================================================================*/
#define HANDLE_FAILURE(pkt)  dsm_free_packet(pkt); \
                             errval = QMI_ERR_NO_MEMORY; \
                             goto send_result 
dsm_item_type * qmi_wds_profile_get_settings
(
  uint8           profile_type,
  uint8           profile_index,
  uint32          subs_id
)
{
  dsm_item_type *                   response;
  qmi_error_e_type                  errval;
  qmi_result_e_type                 result;
  boolean                           retval;
  ds_profile_status_etype           profile_status;
  uint8                             index;
  uint8                             array_size;
  ds_profile_hndl_type              profile_hndl;
  ds_profile_identifier_type        profile_identifier;
  ds_profile_info_type              param_info;
  byte                              value[WDS_MAX_PROFILE_PARAM_VAL_LEN];
  uint16                            param_len;
  boolean                           cancel_txn;
  uint32                            *ipv4_value;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  
  memset(&profile_hndl, 0, sizeof(profile_hndl));
  memset(&param_info, 0, sizeof(param_info));
  memset(value, 0, sizeof(value));
  profile_identifier  = 0;
  response    = NULL;
  errval      = QMI_ERR_NONE;
  array_size  = 0;
  profile_status = DS_PROFILE_REG_RESULT_SUCCESS;
  cancel_txn = FALSE;

  switch(profile_type)
  {
    case WDS_PROFILE_TYPE_3GPP:
    #if defined(FEATURE_WCDMA) || defined(FEATURE_GSM) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA)
      array_size = sizeof(qmi_profile_3gpp_param_tl_list)/
                     sizeof(qmi_profile_3gpp_param_tl_list_type);
    #endif /* defined(FEATURE_WCDMA) || defined(FEATURE_GSM) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA)*/
      break;

    case WDS_PROFILE_TYPE_3GPP2:
    #ifdef FEATURE_CDMA
      array_size = sizeof(qmi_profile_3gpp2_param_tl_list)/
                     sizeof(qmi_profile_3gpp2_param_tl_list_type);
    #endif /* FEATURE_CDMA */
      break;

    default:
      errval = QMI_ERR_INVALID_PROFILE_TYPE;
      goto send_result;
  }

  /*-------------------------------------------------------------------------
    Begin transaction
  -------------------------------------------------------------------------*/
  profile_status = 
    ds_profile_begin_transaction_per_sub(DS_PROFILE_TRN_READ, 
                                         (ds_profile_tech_etype)profile_type,
                                         (ds_profile_num_type)profile_index,
                                         (ds_profile_subs_etype)subs_id,
                                         &profile_hndl);
  if (profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
  {
    LOG_MSG_ERROR_1 ("Begin Transaction failed. Error %d", profile_status);
    errval = QMI_ERR_EXTENDED_INTERNAL;
    goto send_result;
  }

  index = 0;
  while (index < array_size)
  {
    /*-----------------------------------------------------------------------
      Get profile_identifier.
      Exception. We don't return password TLV for 3GPP2.
    -----------------------------------------------------------------------*/
    if (profile_type == WDS_PROFILE_TYPE_3GPP2)
    {
    #ifdef FEATURE_CDMA
      profile_identifier = qmi_profile_3gpp2_param_tl_list[index].param_type;
      if (profile_identifier == DS_PROFILE_3GPP2_PROFILE_PARAM_AUTH_PASSWORD)
      {
        index++;
        continue;
      }
    #endif /* FEATURE_CDMA */
    }
    else if (profile_type == WDS_PROFILE_TYPE_3GPP)
    {
    #if defined (FEATURE_WCDMA) || defined (FEATURE_GSM) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA)
      profile_identifier = qmi_profile_3gpp_param_tl_list[index].param_type;
    #endif /* defined (FEATURE_WCDMA) || defined (FEATURE_GSM) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA) */
    }

    memset(value, 0, sizeof(value));
    param_info.len = sizeof(value);
    param_info.buf = (void*)value;
    
    /*-----------------------------------------------------------------------
      Retrieve profile_identifier and param info
    -----------------------------------------------------------------------*/
    profile_status = ds_profile_get_param(profile_hndl, 
                                          profile_identifier, 
                                          &param_info);
    if (profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
    {
      /*-------------------------------------------------------------------
      Not all parameters are available for each profile. Get whatever is 
      available. Set the profile_status to Success as it is used later
      to send the extended error code.
      -------------------------------------------------------------------*/
      LOG_MSG_INFO1_2("Cannot get Profile param %d for profile number %d, continue",
                      profile_identifier, profile_index);
      profile_status = DS_PROFILE_REG_RESULT_SUCCESS;
      index++;
      continue;
    }

    /*-----------------------------------------------------------------------
      Handling APN bearer param
    -----------------------------------------------------------------------*/
    if (profile_identifier == DS_PROFILE_3GPP_PROFILE_PARAM_APN_BEARER)
    {
      param_info.len = sizeof(uint64);
      if (*(uint8 *)value == 0xFF)
      {
        *(uint64 *)value = 0x8000000000000000ULL;
      }
    }

    /*-----------------------------------------------------------------------
      Handling IPV4 address. Converting them back to Little Endian format since
      they are stored as Big Endian after conversion
     -----------------------------------------------------------------------*/

    if ((profile_identifier == DS_PROFILE_3GPP_PROFILE_PARAM_DNS_ADDR_V4_PRIMARY)  || 
        (profile_identifier == DS_PROFILE_3GPP_PROFILE_PARAM_DNS_ADDR_V4_SECONDARY)|| 
        (profile_identifier == DS_PROFILE_3GPP_PROFILE_PARAM_PDP_CONTEXT_PDP_ADDR_V4) || 
        (profile_identifier == DS_PROFILE_3GPP2_PROFILE_PARAM_V4_DNS_ADDR_PRIMARY) || 
        (profile_identifier == DS_PROFILE_3GPP2_PROFILE_PARAM_V4_DNS_ADDR_SECONDARY))
    {
      ipv4_value = (uint32 *) param_info.buf;
      *ipv4_value = ps_htonl(*ipv4_value);
    }

    /*-----------------------------------------------------------------------
      Verify that the return length is same as expected length for fixed
      length identifiers.
    -----------------------------------------------------------------------*/
    errval = qmi_wds_profile_get_param_len(profile_identifier, profile_type, 
                                           &param_len);
    if (errval != QMI_ERR_NONE)
    {
      LOG_MSG_ERROR_1("Cannot get the Length for profile param %d",
                      profile_identifier);
      ASSERT(0);
    }
    else if ((param_len != 0 && param_len != param_info.len))
    {
      LOG_MSG_ERROR_3("Invalid Length. Identifier(%d) Expected(%d) Got(%d)",
                      profile_identifier, param_len, param_info.len);
      cancel_txn = TRUE;
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
    
    /*-----------------------------------------------------------------------
      Populate the response, containing all profile settings
    -----------------------------------------------------------------------*/
    if( FALSE == qmi_svc_put_param_tlv(&response,
                                       (uint8)profile_identifier,
                                       (uint16)param_info.len,
                                       (void*) param_info.buf))
    {
      cancel_txn = TRUE;
      HANDLE_FAILURE(&response);
    }
    index++;
  } /* while loop end */

  /*-------------------------------------------------------------------------
    End transaction
  -------------------------------------------------------------------------*/
  profile_status = ds_profile_end_transaction(profile_hndl, 
                                              DS_PROFILE_ACTION_COMMIT);
  if(profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
  {
    LOG_MSG_ERROR_1 ("End Transaction failed. Error %d", profile_status);
    errval = QMI_ERR_EXTENDED_INTERNAL;
    goto send_result;
  }

send_result:
  if (cancel_txn == TRUE)
  {
    (void)ds_profile_end_transaction(profile_hndl, 
                                     DS_PROFILE_ACTION_CANCEL);
  }
  if (profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
  {
    retval = qmi_svc_put_param_tlv(&response,
                                   QMI_TYPE_EXT_ERROR_INFO,
                                   WDSI_PRM_TYPE_EXT_ERROR_INFO_LEN,
                                   (void*)&profile_status);
    CHECK_RETVAL();
  }

  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                     : QMI_RESULT_FAILURE);
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();

  return response;
} /* qmi_wds_profile_get_settings */


/*===========================================================================
  FUNCTION QMI_WDS_PROFILE_GET_DEFAULT_SETTINGS()
 
  DESCRIPTION
    For a given technology get the settings for default profile.
 
  PARAMETERS
    profile_type     : profile type (3GPP/3GPP2)
 
  RETURN VALUE
    ptr to response
 
  DEPENDENCIES
    None
 
  SIDE EFFECTS
    None
 ==========================================================================*/
dsm_item_type * qmi_wds_profile_get_default_settings
(
  uint8           profile_type
)
{
  dsm_item_type *                   response;
  qmi_error_e_type                  errval;
  qmi_result_e_type                 result;
  boolean                           retval;
  uint16                            default_profile_num;
  uint32                            profile_family;
  ds_profile_status_etype           profile_status;
  uint32 subs_id = (uint32)DS_PROFILE_ACTIVE_SUBSCRIPTION_1; //need to check
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  
  default_profile_num   = 0;
  response    = NULL;
  errval      = QMI_ERR_NONE;
  profile_status = DS_PROFILE_REG_RESULT_SUCCESS;
  profile_family = 0;
  

  switch(profile_type)
  {
    case WDS_PROFILE_TYPE_3GPP:
#if defined (FEATURE_WCDMA) || defined (FEATURE_GSM) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA)
      profile_family = DS_PROFILE_3GPP_RMNET_PROFILE_FAMILY;
#endif /*defined (FEATURE_WCDMA) || defined (FEATURE_GSM) || defined(FEATURE_LTE) || defined(FEATURE_TDSCDMA)*/
      break;

    case WDS_PROFILE_TYPE_3GPP2:
      /* No Profile Family on CDMA side */
      break;

    default:
      errval = QMI_ERR_INVALID_PROFILE_TYPE;
      goto send_result;
  }

  /*-------------------------------------------------------------------------
    Call the API to get the default profile number.
  -------------------------------------------------------------------------*/
  profile_status = 
    ds_profile_get_default_profile_num((ds_profile_tech_etype)profile_type, 
                                        profile_family,
                                        &default_profile_num);
  if (profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
  {
    LOG_MSG_ERROR_2 ("Get Default Profile for family %d failed. Error %d",
                     profile_type, profile_status);
    errval = QMI_ERR_EXTENDED_INTERNAL;
    goto send_result;
  }
  
  /* Call the API to get the profile settings. */
  
  return qmi_wds_profile_get_settings(profile_type,
                                     (uint8)default_profile_num,
                                     (ds_profile_subs_etype)subs_id);

send_result:

  if (profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
  {
    retval = qmi_svc_put_param_tlv(&response,
                                   QMI_TYPE_EXT_ERROR_INFO,
                                   sizeof(ds_profile_status_etype),
                                   (void*)&profile_status);
    CHECK_RETVAL();
  }
  
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                     : QMI_RESULT_FAILURE);
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();

  return response;
} /* qmi_wds_profile_get_default_settings */


/*===========================================================================
  FUNCTION QMI_WDS_PROFILE_GET_LIST()

  DESCRIPTION
    Get a list of profiles.

  PARAMETERS
    sdu_in         : incoming request
    profile_type   : profile type
    subs_id        : subscription identifier

  RETURN VALUE
    ptr to response

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
dsm_item_type * qmi_wds_profile_get_list
(
  dsm_item_type **    sdu_in,
  uint8               profile_type,
  uint32              subs_id
)
{
  uint8                           type;
  uint16                          len;
  uint16                          param_len;
  byte                            value[WDS_MAX_PROFILE_PARAM_VAL_LEN];
  qmi_error_e_type                errval;
  dsm_item_type *                 response;
  qmi_result_e_type               result;
  boolean                         retval;
  #define WDS_MAX_PROFILE_LIST_LEN  1000
  byte                            profile_list[WDS_MAX_PROFILE_LIST_LEN];
  uint16                          list_length;
  uint16                          list_len_remaining;
  byte *                          profile_list_ptr;
  uint8                           num_profiles;

  ds_profile_itr_type             itr;
  ds_profile_list_info_type       list_info;
  ds_profile_info_type            profile_info;
  ds_profile_status_etype         profile_status;
  ds_profile_list_type            search_list_info;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(sdu_in);

  profile_list_ptr = profile_list;
  num_profiles = 0;
  type = 0;
  len = 0;
  memset(value, 0, sizeof(value));
  memset(&search_list_info, 0, sizeof(search_list_info));
  memset(profile_list, 0, sizeof(profile_list));
  errval = QMI_ERR_NONE;
  profile_status = DS_PROFILE_REG_RESULT_SUCCESS;
  list_length = sizeof(profile_list);
  list_len_remaining = list_length;
  response = NULL;

  /* Default operation - Get all profiles */
  search_list_info.dfn = DS_PROFILE_LIST_ALL_PROFILES;

  while (*sdu_in)
  {
    if( !qmi_svc_get_tl( sdu_in, &type, &len ) )
    {
      continue;
    }
		
    switch (type)
    {
      case WDSI_PRM_TYPE_PROFILE_TECH_TYPE:
        if(len != sizeof(uint8))
        {
          LOG_MSG_INFO2_2 ("T (%d) Invalid L (%d bytes) V - discarding",
                           type, len);
          errval = QMI_ERR_MALFORMED_MSG;
          goto send_result;
        }
        if (len != qmi_svc_dsm_pullup( sdu_in, (void*)value, len,
                           FILE_ID_DS_QMI_WDS_PROFILE,__LINE__  ))
        {
          errval = QMI_ERR_MALFORMED_MSG;
          goto send_result;
        }
        break;
				
      default:
        /* Additional TLV for Search List found. Extract it */
        LOG_MSG_INFO2_1 ("Search list for Identifier type (%d)", type);
        errval = qmi_wds_profile_get_param_len(type, profile_type, 
                                               &param_len);

        /*-------------------------------------------------------------------
          Check for the length of the profile parameter. For fixed length 
          params verify the exact length. For variable lenght params make 
          sure they are not longer than the maximum allowed length. 
        -------------------------------------------------------------------*/
        if (errval != QMI_ERR_NONE)
        {
          LOG_MSG_INFO1_1("Cannot get the Length for param type %d", type);
          goto send_result;
        }
	else if(!qmi_svc_validate_type_length( type,
                                               len,
                                               param_len,
                                               FILE_ID_DS_QMI_WDS_PROFILE,__LINE__)  )
        {
          errval = QMI_ERR_MALFORMED_MSG;
          goto send_result;
        }
        else if ((param_len == 0 && len > WDS_MAX_PROFILE_PARAM_VAL_LEN))
        {
          LOG_MSG_ERROR_2("T (%d) Invalid L (%d bytes) V - discarding",
                          type, len);
          errval = QMI_ERR_ARG_TOO_LONG;
          goto send_result;
        }

        /*-------------------------------------------------------------------
          Got a parameter for key-value pair based search.
        -------------------------------------------------------------------*/
        if (len != qmi_svc_dsm_pullup( sdu_in, (void*)value, len,
                         FILE_ID_DS_QMI_WDS_PROFILE,__LINE__  ))
        {
          errval = QMI_ERR_MALFORMED_MSG;
          goto send_result;
        }
        search_list_info.dfn = DS_PROFILE_LIST_SEARCH_PROFILES;
        search_list_info.ident = type;
        search_list_info.info.len = len;
        search_list_info.info.buf = (void*) value;
        break;
    }

    /* Break out of the while loop if we have found a search Key-Value pair */
    if(search_list_info.dfn == DS_PROFILE_LIST_SEARCH_PROFILES)
    {
      break;
    }
  } /* End while(*sdu_in) */
	
  /*-------------------------------------------------------------------------
    Get the iterator. If there is no entry then the DS Profile will return
    LIST_END error code. Fill number of instances as 0 and return success.
  -------------------------------------------------------------------------*/
  profile_status = ds_profile_get_list_itr_per_sub((ds_profile_tech_etype)profile_type, 
                                            &search_list_info, 
                                            &itr,
											subs_id);
  if(profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
  {
    if(profile_status == DS_PROFILE_REG_RESULT_LIST_END)
    {
      LOG_MSG_INFO1_0("No Profile entry found matching the request");
      profile_list[0] = 0; /*num_instances is 0 as no profiles*/
      list_length = 1;       /* 1 byte for num_instances */
      profile_status = DS_PROFILE_REG_RESULT_SUCCESS;
    }
    else
    {
      LOG_MSG_ERROR_1 ("Getting profile list iterator failed. Error %d",
                       profile_status);
      errval = QMI_ERR_EXTENDED_INTERNAL;
    }
    goto send_result;
  }

  /*-------------------------------------------------------------------------
    Reserve 1 byte for the num_instances field to be filled in later
    increment ptr by 1 and decrement empty space in list 1 byte
  -------------------------------------------------------------------------*/
  profile_list_ptr++;
  list_len_remaining -=1;

  while(profile_status != DS_PROFILE_REG_RESULT_LIST_END)
  {
    memset(&list_info, 0, sizeof (ds_profile_list_info_type));
    memset(&profile_info, 0, sizeof (ds_profile_info_type));
    memset(value, 0, sizeof(value));

    list_info.name = &profile_info;
    list_info.name->len  = sizeof(value);
    list_info.name->buf  = (void*)value;

    /*-----------------------------------------------------------------------
      Call the profile registry API to get the profile info
    -----------------------------------------------------------------------*/
    profile_status = ds_profile_get_info_by_itr(itr, &list_info);
    if(profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
    {
      LOG_MSG_ERROR_1 ("Getting profile info by iterator failed. Error %d",
                       profile_status);
      (void) ds_profile_itr_destroy(itr);
      errval = QMI_ERR_EXTENDED_INTERNAL;
      goto send_result;
    }

    if (list_len_remaining < list_info.name->len + PROFILE_LIST_PARAM_HDR_LENGTH)
    {
      /* terminate profile list generation */
      LOG_MSG_INFO1_0("No room for additional profiles in response!");
      break;
    }

    /*-----------------------------------------------------------------------
      Put the result TLV in the profile list.
      1 - Profile Type (3GPP/3GPP2).
      2 - Profile Number.
      3 - Profile Name Length. (Would be 0 for 3GPP2)
      4 - Profile Name. (Would be NULL for 3GPP2)
    -----------------------------------------------------------------------*/

    /* Add Profile Type field */
    *profile_list_ptr = profile_type;
    profile_list_ptr++;
    list_len_remaining -= 1;

    /* Add Profile Number field */
    *profile_list_ptr = (uint8)list_info.num;
    profile_list_ptr++;
    list_len_remaining -= 1;

    /* Add Profile Name length field */
    *profile_list_ptr = (uint8)list_info.name->len;
    profile_list_ptr++;
    list_len_remaining -= 1;

    /* Add Profile Name, if any */
    if(list_info.name->len != 0)
    {
      memscpy( (char *)profile_list_ptr, 
               list_info.name->len,
              (char *)list_info.name->buf,
              list_info.name->len );
      profile_list_ptr += list_info.name->len;
      list_len_remaining -= list_info.name->len;
    }

    num_profiles++;

    profile_status = ds_profile_itr_next(itr);
    if ( (profile_status != DS_PROFILE_REG_RESULT_SUCCESS) &&
         (profile_status != DS_PROFILE_REG_RESULT_LIST_END) )
    {
      LOG_MSG_ERROR_1 ("Getting Next iterator failed. Error %d",
                       profile_status);
      (void) ds_profile_itr_destroy(itr);
      errval = QMI_ERR_EXTENDED_INTERNAL;
      goto send_result;
    }
  } /* while (profile_status != DS_PROFILE_REG_RESULT_LIST_END) */

  /*-------------------------------------------------------------------------
    Destroy the iterator
  -------------------------------------------------------------------------*/
  profile_status = ds_profile_itr_destroy(itr);
  if(profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
  {
    LOG_MSG_ERROR_1 ("Destroying iterator failed. Error %d", profile_status);
    errval = QMI_ERR_EXTENDED_INTERNAL;
    goto send_result;
  }

  /*-------------------------------------------------------------------------
    Populate the first byte reserved earlier with number of profiles
  -------------------------------------------------------------------------*/
  *profile_list = num_profiles;

  /*-------------------------------------------------------------------------
    Calculate the length of the profile list
  -------------------------------------------------------------------------*/
  list_length = profile_list_ptr - profile_list;

send_result:
  dsm_free_packet(sdu_in);
  if (errval == QMI_ERR_NONE)
  {
    retval = qmi_svc_put_param_tlv(&response,
                                    QMI_TYPE_REQUIRED_PARAMETERS,
                                    list_length,
                                    (void*)profile_list);
    CHECK_RETVAL();
  }

  if (profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
  {
    retval = qmi_svc_put_param_tlv(&response,
                                   QMI_TYPE_EXT_ERROR_INFO,
                                   WDSI_PRM_TYPE_EXT_ERROR_INFO_LEN,
                                   (void*)&profile_status);
    CHECK_RETVAL();
  }

  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                     : QMI_RESULT_FAILURE);
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();

  return response;

} /* qmi_wds_profile_get_list */


/*===========================================================================
  FUNCTION QMI_WDS_PROFILE_GET_DEFAULT_PROFILE_NUM()
 
  DESCRIPTION
    For a given technology get the default profile number.
 
  PARAMETERS
    profile_type     : profile type (3GPP/3GPP2)
    profile_family   : profile family
    subs_id          : subscription identifier 
 
  RETURN VALUE
    ptr to response
 
  DEPENDENCIES
    None
 
  SIDE EFFECTS
    None
 ==========================================================================*/
dsm_item_type * qmi_wds_profile_get_default_profile_num
(
  uint8           profile_type,
  uint8           profile_family,
  uint32          subs_id
)
{
  dsm_item_type *                   response;
  qmi_error_e_type                  errval;
  qmi_result_e_type                 result;
  boolean                           retval;
  uint16                            default_profile_num;
  uint8                             profile_num;
  ds_profile_status_etype           profile_status;
  ds_profile_subs_etype             profile_subs;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  
  profile_status = DS_PROFILE_REG_RESULT_SUCCESS;
  response       = NULL;
  errval         = QMI_ERR_NONE;
  default_profile_num = 0;
  profile_subs = (ds_profile_subs_etype)subs_id;
  /*-------------------------------------------------------------------------
    Call the API to get the default profile number.
  -------------------------------------------------------------------------*/
  profile_status = 
    ds_profile_get_default_profile_num_per_subs ((ds_profile_tech_etype)profile_type, 
                                        (uint32)profile_family,
                                                  profile_subs,
                                        &default_profile_num);
  if (profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
  {
    LOG_MSG_ERROR_2 ("Get Default Profile for family %d failed. Error %d",
                     profile_type, profile_status);
    errval = QMI_ERR_EXTENDED_INTERNAL;
    goto send_result;
  }

  profile_num = (uint8)default_profile_num;
  /*-----------------------------------------------------------------------
    Populate the response, containing the default profile number.
  -----------------------------------------------------------------------*/
  if( FALSE == qmi_svc_put_param_tlv(&response,
                                     QMI_TYPE_REQUIRED_PARAMETERS,
                                     sizeof (uint8),
                                     (void*)&profile_num ))
  {
    errval = QMI_ERR_NO_MEMORY; 
    dsm_free_packet(&response);
  }

send_result:
  if (profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
  {
    retval = qmi_svc_put_param_tlv(&response,
                                   QMI_TYPE_EXT_ERROR_INFO,
                                   sizeof(ds_profile_status_etype),
                                   (void*)&profile_status);
    CHECK_RETVAL();
  }
  
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                     : QMI_RESULT_FAILURE);
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();

  return response;
} /* qmi_wds_profile_get_default_profile_num */


/*===========================================================================
  FUNCTION QMI_WDS_PROFILE_SET_DEFAULT_PROFILE_NUM()
 
  DESCRIPTION
    For a given technology set the default profile number.
 
  PARAMETERS
    profile_type     : profile type (3GPP/3GPP2)
    profile_family   : profile family
    profile_index    : profile number that should be set to default
    subs_id          : subscription identifier 
 
  RETURN VALUE
    ptr to response
 
  DEPENDENCIES
    None
 
  SIDE EFFECTS
    None
 ==========================================================================*/
dsm_item_type * qmi_wds_profile_set_default_profile_num
(
  uint8           profile_type,
  uint8           profile_family,
  uint8           profile_index,
  uint32          subs_id
)
{
  dsm_item_type *                   response;
  qmi_error_e_type                  errval;
  qmi_result_e_type                 result;
  boolean                           retval;
  ds_profile_status_etype           profile_status;
  ds_profile_subs_etype             profile_subs;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  
  profile_status = DS_PROFILE_REG_RESULT_SUCCESS;
  response       = NULL;
  errval         = QMI_ERR_NONE;
  profile_subs = (ds_profile_subs_etype)subs_id;

  /*-------------------------------------------------------------------------
    Call the API to set the default profile number.
  -------------------------------------------------------------------------*/
  profile_status = 
    ds_profile_set_default_profile_num_per_subs((ds_profile_tech_etype)profile_type, 
                                       (uint32)profile_family,
                                                 profile_subs,
                                       (ds_profile_num_type)profile_index);
  if (profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
  {
    LOG_MSG_ERROR_2 ("Set Default Profile for family %d failed. Error %d",
                     profile_type, profile_status);
    errval = QMI_ERR_EXTENDED_INTERNAL;
    goto send_result;
  }

send_result:
  if (profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
  {
    retval = qmi_svc_put_param_tlv(&response,
                                   QMI_TYPE_EXT_ERROR_INFO,
                                   WDSI_PRM_TYPE_EXT_ERROR_INFO_LEN,
                                   (void*)&profile_status);
    CHECK_RETVAL();
  }
  
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                     : QMI_RESULT_FAILURE);
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();

  return response;
} /* qmi_wds_profile_set_default_profile_num */


/*===========================================================================
  FUNCTION QMI_WDS_PROFILE_RESET_PROFILE_TO_DEFAULT()
 
  DESCRIPTION
    For a given technology resets the specified profile to default values.
 
  PARAMETERS
    profile_type     : profile type (3GPP/3GPP2)
    profile_index    : profile number that should be reset to use 
                       default values
 
  RETURN VALUE
    ptr to response
 
  DEPENDENCIES
    None
 
  SIDE EFFECTS
    None
 ==========================================================================*/
dsm_item_type * qmi_wds_profile_reset_profile_to_default
(
  uint8           profile_type,
  uint8           profile_index,
  uint32          subs_id
)
{
  dsm_item_type *                   response;
  qmi_error_e_type                  errval;
  qmi_result_e_type                 result;
  boolean                           retval;
  ds_profile_status_etype           profile_status;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  
  profile_status = DS_PROFILE_REG_RESULT_SUCCESS;
  response       = NULL;
  errval         = QMI_ERR_NONE;

  /*-------------------------------------------------------------------------
    Call the DS Profile API to reset the profile to default values
  -------------------------------------------------------------------------*/
  profile_status = 
    ds_profile_reset_profile_to_default_per_sub((ds_profile_tech_etype)profile_type, 
                                                (ds_profile_num_type)profile_index,
                                                (ds_profile_subs_etype)subs_id);
  if (profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
  {
    LOG_MSG_ERROR_3 ("Reset Profile %d to default for family %d failed. Error %d",
                     profile_index, profile_type, profile_status);
    errval = QMI_ERR_EXTENDED_INTERNAL;
    goto send_result;
  }

send_result:
  if (profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
  {
    retval = qmi_svc_put_param_tlv(&response,
                                   QMI_TYPE_EXT_ERROR_INFO,
                                   WDSI_PRM_TYPE_EXT_ERROR_INFO_LEN,
                                   (void*)&profile_status);
    CHECK_RETVAL();
  }
  
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                     : QMI_RESULT_FAILURE);
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();

  return response;
} /* qmi_wds_profile_reset_profile_to_default() */


/*===========================================================================
  FUNCTION QMI_WDS_PROFILE_RESET_PARAM_TO_INVALID()
 
  DESCRIPTION
    Set the specified parameter in the specified profile to invalid
 
  PARAMETERS
    profile_type     : profile type (3GPP/3GPP2)
    profile_index    : profile number that should be reset to use 
                       default values
    param_id         : param id that should be reset to invalid
 
  RETURN VALUE
    ptr to response
 
  DEPENDENCIES
    None
 
  SIDE EFFECTS
    None
 ==========================================================================*/
dsm_item_type * qmi_wds_profile_reset_param_to_invalid
(
  uint8           profile_type,
  uint8           profile_index,
  uint32          param_id,
  uint32          subs_id
)
{
  dsm_item_type *                   response;
  qmi_error_e_type                  errval;
  qmi_result_e_type                 result;
  boolean                           retval;
  ds_profile_status_etype           profile_status;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  
  profile_status = DS_PROFILE_REG_RESULT_SUCCESS;
  response       = NULL;
  errval         = QMI_ERR_NONE;

  /*-------------------------------------------------------------------------
    Call the DS Profile API to reset the profile param to invalid
  -------------------------------------------------------------------------*/
  profile_status = 
      ds_profile_reset_param_to_invalid_per_sub((ds_profile_tech_etype)profile_type, 
                                                (ds_profile_num_type)profile_index,
                                                param_id,
                                                (ds_profile_subs_etype)subs_id );
  if (profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
  {
    LOG_MSG_ERROR_3 ("Invalidate profile param %d profile %d failed. Error %d",
                     param_id, profile_index, profile_status);
    errval = QMI_ERR_EXTENDED_INTERNAL;
    goto send_result;
  }

send_result:
  if (profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
  {
    retval = qmi_svc_put_param_tlv(&response,
                                   QMI_TYPE_EXT_ERROR_INFO,
                                   WDSI_PRM_TYPE_EXT_ERROR_INFO_LEN,
                                   (void*)&profile_status);
    CHECK_RETVAL();
  }
  
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                     : QMI_RESULT_FAILURE);
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();

  return response;
} /* qmi_wds_profile_reset_param_to_invalid() */


/*===========================================================================
  FUNCTION QMI_WDS_PROFILE_CREATE()
 
  DESCRIPTION
    Create a new profile.
 
  PARAMETERS
    sdu_in       : incoming request
    profile_type : profile type (3GPP/3GPP2)
    subs_id      : subscription identifier
 
  RETURN VALUE
    response
  
  DEPENDENCIES
    None
 
  SIDE EFFECTS
    None
===========================================================================*/
dsm_item_type * qmi_wds_profile_create
(
  dsm_item_type **    sdu_in,
  uint8               profile_type,
  boolean             got_persistence_tlv,
  boolean             is_persistent,
  uint32              subs_id
)
{
  dsm_item_type *                   response;
  qmi_error_e_type                  errval;
  qmi_result_e_type                 result;
  boolean                           retval;
  ds_profile_status_etype           profile_status;
  uint16                            profile_num;
  ds_profile_config_type            profile_config = {0};
  PACKED struct PACKED_POST
  {
    uint8 profile_type;
    uint8 profile_index;
  } v_out_reqd;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  
  profile_status = DS_PROFILE_REG_RESULT_SUCCESS;
  response       = NULL;
  errval         = QMI_ERR_NONE;
  profile_num    = 0;

  /*-------------------------------------------------------------------------
    Call the DS Profile API to create a profile
  -------------------------------------------------------------------------*/
  if (got_persistence_tlv)
  {
    profile_config.config_mask |= DS_PROFILE_CONFIG_MASK_PERSISTENCE;
    profile_config.is_persistent = is_persistent;
  }
  profile_config.config_mask |= DS_PROFILE_CONFIG_MASK_SUBS_ID;
  profile_config.subs_id = subs_id;
  profile_status = ds_profile_create_ex( (ds_profile_tech_etype)profile_type, 
                                      &profile_config,
                                      &profile_num );

  if (profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
  {
    LOG_MSG_ERROR_2 ("Cannot create profile for tech type %d. Error %d",
                     profile_type, profile_status);
    errval = QMI_ERR_EXTENDED_INTERNAL;
    goto send_error;
  }

  /*-------------------------------------------------------------------------
    Modify the newly created profile with the input parameters. If the 
    modify returns fail delete the created profile. The sdu_in is freed
    by modify API in success as well as fail case.
  -------------------------------------------------------------------------*/
  response = qmi_wds_profile_modify_settings(sdu_in,
                                             profile_type,
                                             (uint8)profile_num,
                                             subs_id,
                                             &errval);

  if (response == NULL || errval != QMI_ERR_NONE)
  {
    (void) ds_profile_delete((ds_profile_tech_etype)profile_type, 
                              profile_num);
    return response;
  }
  
  /*Free the response and create a new one with profile type and number*/
  dsm_free_packet(&response);
  response = NULL;

  v_out_reqd.profile_type  = profile_type;
  v_out_reqd.profile_index = (uint8)profile_num;

  retval = qmi_svc_put_param_tlv(&response,
                                 QMI_TYPE_REQUIRED_PARAMETERS,
                                 sizeof (v_out_reqd),
                                 &v_out_reqd);

  CHECK_RETVAL();

send_error:
  
  if (profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
  {
    dsm_free_packet(sdu_in);
    retval = qmi_svc_put_param_tlv(&response,
                                   QMI_TYPE_EXT_ERROR_INFO,
                                   WDSI_PRM_TYPE_EXT_ERROR_INFO_LEN,
                                   (void*)&profile_status);
    CHECK_RETVAL();
  }
  
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                     : QMI_RESULT_FAILURE);
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();

  return response;
}/* qmi_wds_profile_create() */


/*===========================================================================
  FUNCTION QMI_WDS_PROFILE_DELETE()
 
  DESCRIPTION
  Deletes a specified profile for the specified technology.
 
  PARAMETERS
    profile_type     : profile type (3GPP/3GPP2)
    profile_index    : profile number that should be deleted.
 
  RETURN VALUE
    ptr to response
 
  DEPENDENCIES
    None
 
  SIDE EFFECTS
    None
 ==========================================================================*/
dsm_item_type * qmi_wds_profile_delete
(
  uint8           profile_type,
  uint8           profile_index,
  uint32          subs_id
)
{
  dsm_item_type *                   response;
  qmi_error_e_type                  errval;
  qmi_result_e_type                 result;
  boolean                           retval;
  ds_profile_status_etype           profile_status;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  
  profile_status = DS_PROFILE_REG_RESULT_SUCCESS;
  response       = NULL;
  errval         = QMI_ERR_NONE;

  /*-------------------------------------------------------------------------
    Call the DS Profile API to reset the profile to default values
  -------------------------------------------------------------------------*/
  profile_status = ds_profile_delete_per_sub((ds_profile_tech_etype)profile_type, 
                                             (ds_profile_num_type)profile_index,
                                             (ds_profile_subs_etype)subs_id);

  if (profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
  {
    LOG_MSG_ERROR_3 ("Cannot delete profile (%d) Tech (%d) Error (%d)",
                     profile_index, profile_type, profile_status);
    errval = QMI_ERR_EXTENDED_INTERNAL;
    goto send_result;
  }

send_result:
  if (profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
  {
    retval = qmi_svc_put_param_tlv(&response,
                                   QMI_TYPE_EXT_ERROR_INFO,
                                   WDSI_PRM_TYPE_EXT_ERROR_INFO_LEN,
                                   (void*)&profile_status);

    CHECK_RETVAL();
  }
  
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                     : QMI_RESULT_FAILURE);
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();

  return response;
} /* qmi_wds_profile_delete() */


/*===========================================================================
  FUNCTION QMI_WDS_PROFILE_UMTS_VALIDATE_PDP_TYPE()

  DESCRIPTION
    Validate the pdp_type set in the profile is IP

  PARAMETERS
    profile_num

  RETURN VALUE
    TRUE   - pdp_type is PDP-IP
    FALSE  - otherwise
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
qmi_error_e_type qmi_wds_profile_umts_validate_pdp_type
(
  uint8  profile_num,
  uint32 subs_id
)
{

  ds_profile_info_type                param_info;
  byte                                value[WDS_MAX_PROFILE_PARAM_VAL_LEN];
  qmi_error_e_type                    errval;
  ds_profile_3gpp_pdp_type_enum_type  pdp_type;

  errval = QMI_ERR_NONE;
  pdp_type = DS_PROFILE_3GPP_PDP_MAX;
  memset(&param_info, 0, sizeof(param_info));
  memset(value, 0, sizeof(value));

  param_info.len = sizeof(value);
  param_info.buf = (void*)value;

  errval = qmi_wds_profile_get_param_id_info
                     ( WDS_PROFILE_TYPE_3GPP, 
                       profile_num, 
                       subs_id,
                       DS_PROFILE_3GPP_PROFILE_PARAM_PDP_CONTEXT_PDP_TYPE, 
                       &param_info );
  
  if(errval == QMI_ERR_NONE)
  {
    pdp_type = *((ds_profile_3gpp_pdp_type_enum_type*)param_info.buf);
    if((pdp_type != DS_PROFILE_3GPP_PDP_IP)
        && (pdp_type != DS_PROFILE_3GPP_PDP_IPV4V6)
#ifdef FEATURE_DATA_PS_IPV6
       && (pdp_type != DS_PROFILE_3GPP_PDP_IPV6)
#endif /* FEATURE_DATA_PS_IPV6 */
      )
    {
      errval = QMI_ERR_INVALID_PDP_TYPE;
    }
  }
  return errval;
} /* qmi_wds_profile_umts_validate_pdp_type() */


/*===========================================================================
  FUNCTION QMI_WDS_PROFILE_RETRIEVE_DEFAULT_PROFILE_NUM()
  
  DESCRIPTION
  Get the default profile number for tech type.
  
  PARAMETERS
   profile_type (IN)  : Profile Type (3GPP/3GPP2)
   profile_num  (OUT) : Default Profile Number
  
  RETURN VALUE
  None
  
  DEPENDENCIES
    None
  
  SIDE EFFECTS
    None
===========================================================================*/
void qmi_wds_profile_retrieve_default_profile_num
(
  uint8                        profile_type,
  uint8                        profile_family,
  uint32                       subs_id,
  uint16                      *default_profile_num
)
{
  ds_profile_status_etype           profile_status;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  
  profile_status = DS_PROFILE_REG_RESULT_SUCCESS;
  *default_profile_num = 0;

  /*-------------------------------------------------------------------------
    Call the API to get the default profile number.
  -------------------------------------------------------------------------*/
  profile_status = 
    ds_profile_get_default_profile_num_per_subs((ds_profile_tech_etype)profile_type, 
                                               (uint32)profile_family,
                                                (ds_profile_subs_etype)subs_id,
                                                default_profile_num);
  if (profile_status != DS_PROFILE_REG_RESULT_SUCCESS && 
      profile_status != DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE)
  {
    LOG_MSG_ERROR_2 ("Get Default Profile for type %d failed. Error %d",
                     profile_type, profile_status);
  }

  return;
  
} /* qmi_wds_profile_retrieve_default_profile_num() */

#ifdef FEATURE_DATA_LTE
/*===========================================================================
  FUNCTION QMI_WDS_PROFILE_UPDATE_LTE_ATTACH_PDN_LIST_PARAMS ()
 
  DESCRIPTION
  Updates the profile parameters in the lte attach pdn list of the 
  specified technology.
 
  PARAMETERS
    profile_type     : profile type (3GPP/3GPP2)
 
  RETURN VALUE
    ptr to response
 
  DEPENDENCIES
    None
 
  SIDE EFFECTS
    None
 ==========================================================================*/
dsm_item_type * qmi_wds_profile_update_lte_attach_pdn_list_params
(
  uint8           profile_type,
  uint32          subs_id
)
{
  dsm_item_type *                   response;
  qmi_error_e_type                  errval;
  qmi_result_e_type                 result;
  boolean                           retval;
  ds_profile_status_etype           profile_status;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  
  profile_status = DS_PROFILE_REG_RESULT_SUCCESS;
  response       = NULL;
  errval         = QMI_ERR_NONE;

  /*-------------------------------------------------------------------------
    Call the DS Profile API to update the LTE attach PDN list profile params
  -------------------------------------------------------------------------*/
  profile_status = ds_profile_update_lte_attach_pdn_list_profiles_per_sub(
                     (ds_profile_tech_etype)profile_type, (ds_profile_subs_etype)subs_id);

  if (profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
  {
    LOG_MSG_ERROR_3 ("Cannot update LTE attach PDN profiles Tech(%d) Error(%d) Subs ID(%d)",
                     profile_type, profile_status, subs_id);
    errval = QMI_ERR_EXTENDED_INTERNAL;
    goto send_result;
  }

send_result:
  if (profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
  {
    retval = qmi_svc_put_param_tlv(&response,
                                   QMI_TYPE_EXT_ERROR_INFO,
                                   WDSI_PRM_TYPE_EXT_ERROR_INFO_LEN,
                                   (void*)&profile_status);

    CHECK_RETVAL();
  }
  
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                     : QMI_RESULT_FAILURE);
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();

  return response;
} /* qmi_wds_update_lte_attach_pdn_list_profiles() */
#endif /* FEATURE_DATA_LTE */
