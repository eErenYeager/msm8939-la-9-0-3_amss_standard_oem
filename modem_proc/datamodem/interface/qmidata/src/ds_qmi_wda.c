/*===========================================================================

                         D S _ Q M I _ W D A . C

DESCRIPTION

  The Data Services QMI Wireless Data Administrative Service source file.

EXTERNALIZED FUNCTIONS

  qmi_wda_init()
    Initialize the QMI WDA service

Copyright (c) 2012-2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/
/*===========================================================================

                      EDIT HISTORY FOR FILE

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/qmidata/src/ds_qmi_wda.c#1 $
  $DateTime: 2015/01/27 06:42:19 $

when        who    what, where, why
--------    ---    ----------------------------------------------------------
10/22/13    sah    Corrected validation check in qmi_wdai_packet_filter_add_rule that was rejecting 
                   valid mandatory TLV due to optional or malformed tlv
04/25/13    wc     MBIM mPDN support
04/11/13    wc     Add UL aggregation threholds TLVs
08/24/12    gk     Made sure that rmenet set_data_format is not called on second 
                   instance when NO_EFFECT error is returned in first call
08/06/12    gk     Added support for QMI logging on what messages/TLVs 
                   are supported
07/26/12    sb     Fixed KW Errors.
07/11/12    pgm    Added Support for RNDIS agg protocol.
02/02/12    cy     Added support for MBIM NTB parameters
02/01/12    cy     Support for packet filter feature
09/19/11    sy     Created module
===========================================================================*/

/*===========================================================================

                          INCLUDE FILES FOR MODULE

===========================================================================*/

#include "comdef.h"
#include "customer.h"

#include <stdio.h>
#include <string.h>

#include "dsm.h"
#include "nv.h"
#include "msg.h"
#include "err.h"
#include "amssassert.h"
#include "ds_qmi_defs.h"
#include "ds_qmi_svc.h"
#include "ds_qmi_task.h"
#include "ds_qmux_ext.h"
#include "ds_qmux.h"
#include "ds_qmi_svc_ext.h"
#include "dcc_task_defs.h"
#include "dcc_task_svc.h"

#include "qmi_svc_defs.h"
#include "qmi_svc_utils.h"
#include "qmi_framework.h"
#include "ds_qmi_wda.h"
#include "ds_rmnet_meta_sm.h"
#include "ds_rmnet_utils.h"
#include "ds_rmnet_qmap.h"

#include "ps_system_heap.h"
#include "ds_Utils_DebugMsg.h"
#include "ps_sys_conf.h"
#include "ps_dl_optimized_hdlr.h"
#include "modem_mem.h"
#include "qmi_si.h"
#include "qmi_idl_lib.h"
#include "wireless_data_administrative_service_v01.h"
#include "wireless_data_administrative_service_impl_v01.h"

/*===========================================================================

                            CONSTANT DEFINITIONS

===========================================================================*/

/*---------------------------------------------------------------------------
  Service management
---------------------------------------------------------------------------*/
#define WDAI_MAX_CLIDS    (QMI_FRAMEWORK_SVC_MAX_CLIENTS - 1)
#define WDAI_INVALID_INSTANCE    -1

/*---------------------------------------------------------------------------
  Major and Minor Version Nos for WDA
---------------------------------------------------------------------------*/
#define WDAI_BASE_VER_MAJOR    (1)
#define WDAI_BASE_VER_MINOR    (11)

#define WDAI_ADDENDUM_VER_MAJOR  (0)
#define WDAI_ADDENDUM_VER_MINOR  (0)

/*---------------------------------------------------------------------------
  Macro used in command handlers (common)
---------------------------------------------------------------------------*/
#define CHECK_RETVAL()  if (FALSE == retval) { dsm_free_packet(&response); \
                                               return NULL; }

/*---------------------------------------------------------------------------
  Macro used to validate that request was not sent on reverse IP
  transport instance
---------------------------------------------------------------------------*/
#define VALIDATE_REQUEST_INSTANCE(qmi_inst)  if (qmi_inst > QMI_INSTANCE_FORWARD_PORT_MAX) \
                                             {  \
                                               errval = QMI_ERR_INVALID_OPERATION; \
                                               goto send_result; \
                                             } \

/*---------------------------------------------------------------------------
  Message-internal TLV type field values
---------------------------------------------------------------------------*/
/* The new optional TLV to set the underlying link layer protocol mode. */
#define QMI_TYPE_QOS_DATA_FORMAT              (0x10)
#define QMI_TYPE_LINK_PROTOCOL                (0x11)
#define QMI_TYPE_UL_DATA_AGG_PROTOCOL         (0x12)
#define QMI_TYPE_DL_DATA_AGG_PROTOCOL         (0x13)
#define QMI_TYPE_NDP_SIGNATURE                (0x14)
#define QMI_TYPE_DL_DATA_AGG_MAX_DATAGRAMS    (0x15)
#define QMI_TYPE_DL_DATA_AGG_MAX_SIZE         (0x16)
#define QMI_TYPE_UL_DATA_AGG_MAX_DATAGRAMS    (0x17)
#define QMI_TYPE_UL_DATA_AGG_MAX_SIZE         (0x18)
#define QMI_TYPE_QOS_HEADER_FORMAT_REQ        (0x18)
#define QMI_TYPE_QOS_HEADER_FORMAT_RESP       (0x19)
#define QMI_TYPE_DL_MIN_PADDING_REQ           (0x19)
#define QMI_TYPE_DL_MIN_PADDING_RESP          (0x1A)

/* The optional TLV's to set packet filters settings. */
#define QMI_TYPE_PACKET_FILTERS_ENABLED       (0x10)
#define QMI_TYPE_PACKET_FILTERS_RESTRICTIVE   (0x11)

/* The optional TLV's to set packet filter config rules. */
#define QMI_TYPE_PACKET_FILTER_HANDLE         (0x10)
#define QMI_TYPE_PACKET_FILTER_RULE           (0x11)

/* The optional TLV's to get packet filter handle list. */
#define QMI_TYPE_PACKET_FILTER_HANDLE_LIST    (0x10)

#define QMI_IDL_PACKET_FILTER_MAX_NUM         (32)
#define QMI_IDL_PACKET_FILTER_MAX_SIZE        (192)

/* The optional TLV's to get loopback state. */
#define QMI_TYPE_LOOPBACK_STATE               (0x10)

/* The optional ips_id TLVs */
#define QMI_TYPE_IPS_ID_10                    (0x10)
#define QMI_TYPE_IPS_ID_11                    (0x11)

/* QMAP settings */
#define QMI_TYPE_IN_BAND_FLOW_CONTROL         (0x10)

#define QMI_TYPE_UINT8_LEN                    (0x01)

#define CHECK_RETVAL()  if (FALSE == retval) { dsm_free_packet(&response); \
                                               return NULL; }


/*===========================================================================

                                DATA TYPES

===========================================================================*/


/*---------------------------------------------------------------------------
  WDA Command enum type - not equal to the actual command values!
  mapping is in qmi_wda_cmd_callbacks table

  DO NOT REORDER THIS ENUM!
---------------------------------------------------------------------------*/
typedef enum
{
  WDAI_CMD_MIN                          = 0,
  WDAI_CMD_SET_DATA_FORMAT              = WDAI_CMD_MIN,
  WDAI_CMD_GET_DATA_FORMAT                 ,
  WDAI_CMD_PACKET_FILTER_ENABLE,
  WDAI_CMD_PACKET_FILTER_DISABLE,
  WDAI_CMD_PACKET_FILTER_GET_STATE,
  WDAI_CMD_PACKET_FILTER_ADD_RULE,
  WDAI_CMD_PACKET_FILTER_DELETE_RULE,
  WDAI_CMD_PACKET_FILTER_GET_RULE_HANDLES,
  WDAI_CMD_PACKET_FILTER_GET_RULE,
  WDAI_CMD_SET_LOOPBACK_STATE,
  WDAI_CMD_GET_LOOPBACK_STATE,
  WDAI_CMD_SET_QMAP_SETTINGS,
  WDAI_CMD_GET_QMAP_SETTINGS,
  WDAI_CMD_MAX,
  WDAI_CMD_WIDTH                        = 0xFFFF
} qmi_wdai_cmd_e_type;

typedef enum
{
  WDAI_CMD_VAL_SET_DATA_FORMAT               = 0x0020,
  WDAI_CMD_VAL_GET_DATA_FORMAT               = 0x0021,
  WDAI_CMD_VAL_PACKET_FILTER_ENABLE             = 0x0022,
  WDAI_CMD_VAL_PACKET_FILTER_DISABLE            = 0x0023,
  WDAI_CMD_VAL_PACKET_FILTER_GET_STATE          = 0x0024,
  WDAI_CMD_VAL_PACKET_FILTER_ADD_RULE           = 0x0025,
  WDAI_CMD_VAL_PACKET_FILTER_DELETE_RULE        = 0x0026,
  WDAI_CMD_VAL_PACKET_FILTER_GET_RULE_HANDLES   = 0x0027,
  WDAI_CMD_VAL_PACKET_FILTER_GET_RULE           = 0x0028,
  WDAI_CMD_VAL_SET_LOOPBACK_STATE               = 0x0029,
  WDAI_CMD_VAL_GET_LOOPBACK_STATE               = 0x002A,
  WDAI_CMD_VAL_SET_QMAP_SETTINGS                = 0x002B,
  WDAI_CMD_VAL_GET_QMAP_SETTINGS                = 0x002C,

/* QC EXTERNAL QMI COMMAND RANGE IS 0x0000 - 0x5555. 
   Add the next external QMI Command here */

/* VENDOR SPECIFIC QMI COMMAND RANGE IS 0x5556 - 0xAAAA.
   IMPORTANT!
   Add the vendor specific QMI Commands within this range only to avoid 
   conflicts with QC QMI commands that would get released in future */

/* RESERVED QC QMI COMMAND RANGE IS 0xAAAB - 0xFFFE */
  WDAI_CMD_VAL_WIDTH                      = 0xFFFF
} qmi_wdai_cmd_val_e_type;


/*---------------------------------------------------------------------------
  QMI WDA instance state definition &
  WDA client state definition
---------------------------------------------------------------------------*/

typedef struct
{
  uint16 cmd_id;
  union
  {
    struct
    {
      uint16    num_instances;
    } init_cb;

    struct
    {
      qmi_framework_common_msg_hdr_type    common_msg_hdr;
    } clid_cb;

    struct
    {
      qmi_framework_msg_hdr_type    msg_hdr;
      dsm_item_type             *   sdu_in;
    } cmd_hdlr_cb;

    struct
    {
      void           * user_data;
      dsm_item_type  * pkt;
    } wdai_resp;
  } data;
} qmi_wdai_cmd_buf_type;

/*---------------------------------------------------------------------------
  QMI WDA Service configuration
---------------------------------------------------------------------------*/
typedef struct
{
  qmi_framework_svc_config_type    fw_cfg;
  qmi_svc_cmd_hdlr_type *          cmd_hdlr_array;
  uint16                           cmd_num_entries;
  void *                           sp;
  boolean                          registered;   /* Service registration status */
} qmi_wda_svc_config_type;

typedef struct
{
  int32      qmi_instance;
  uint8      client_id;
} qmi_wda_instace_state_type;

typedef struct
{
  uint16                                 num_qmi_instances;
  qmi_wda_instace_state_type             instance[QMI_INSTANCE_MAX];
} qmi_wdai_state_type;

static qmi_wdai_state_type  qmi_wda_state;

/*===========================================================================

                               INTERNAL DATA

===========================================================================*/

/*---------------------------------------------------------------------------
  QMI service command handlers
  forward declarations & cmd handler dispatch table definition
---------------------------------------------------------------------------*/

static dsm_item_type* qmi_wdai_set_data_format(void*, void*, void*, dsm_item_type **);
static dsm_item_type* qmi_wdai_get_data_format(void*, void*, void*, dsm_item_type **);
static dsm_item_type* qmi_wdai_packet_filter_enable(void*, void*, void*, dsm_item_type **);
static dsm_item_type* qmi_wdai_packet_filter_disable(void*, void*, void*, dsm_item_type **);
static dsm_item_type* qmi_wdai_packet_filter_get_state(void*, void*, void*, dsm_item_type **);
static dsm_item_type* qmi_wdai_packet_filter_add_rule(void*, void*, void*, dsm_item_type **);
static dsm_item_type* qmi_wdai_packet_filter_delete_rule(void*, void*, void*, dsm_item_type **);
static dsm_item_type* qmi_wdai_packet_filter_get_rule_handles(void*, void*, void*, dsm_item_type **);
static dsm_item_type* qmi_wdai_packet_filter_get_rule(void*, void*, void*, dsm_item_type **);

#define WDA_HDLR(a,b)  QMI_SVC_HDLR( a, (qmi_svc_hdlr_ftype)b )



static dsm_item_type* qmi_wdai_set_loopback_state
(
  void*,
  void*,
  void*,
  dsm_item_type **
);

static dsm_item_type* qmi_wdai_get_loopback_state
(
  void*,
  void*,
  void*,
  dsm_item_type **
);

static dsm_item_type* qmi_wdai_set_qmap_settings(void*, void*, void*, dsm_item_type **);
static dsm_item_type* qmi_wdai_get_qmap_settings(void*, void*, void*, dsm_item_type **);

static qmi_svc_cmd_hdlr_type  qmi_wdai_cmd_callbacks[WDAI_CMD_MAX] =
{
  WDA_HDLR(WDAI_CMD_VAL_SET_DATA_FORMAT, 
               qmi_wdai_set_data_format),
  WDA_HDLR(WDAI_CMD_VAL_GET_DATA_FORMAT, 
               qmi_wdai_get_data_format),
  WDA_HDLR(WDAI_CMD_VAL_PACKET_FILTER_ENABLE, 
               qmi_wdai_packet_filter_enable),
  WDA_HDLR(WDAI_CMD_VAL_PACKET_FILTER_DISABLE, 
               qmi_wdai_packet_filter_disable),
  WDA_HDLR(WDAI_CMD_VAL_PACKET_FILTER_GET_STATE, 
               qmi_wdai_packet_filter_get_state),
  WDA_HDLR(WDAI_CMD_VAL_PACKET_FILTER_ADD_RULE, 
               qmi_wdai_packet_filter_add_rule),
  WDA_HDLR(WDAI_CMD_VAL_PACKET_FILTER_DELETE_RULE, 
               qmi_wdai_packet_filter_delete_rule),
  WDA_HDLR(WDAI_CMD_VAL_PACKET_FILTER_GET_RULE_HANDLES, 
               qmi_wdai_packet_filter_get_rule_handles),
  WDA_HDLR(WDAI_CMD_VAL_PACKET_FILTER_GET_RULE, 
               qmi_wdai_packet_filter_get_rule),
  WDA_HDLR(WDAI_CMD_VAL_SET_LOOPBACK_STATE, 
               qmi_wdai_set_loopback_state),
  WDA_HDLR(WDAI_CMD_VAL_GET_LOOPBACK_STATE, 
               qmi_wdai_get_loopback_state),
  WDA_HDLR(WDAI_CMD_VAL_SET_QMAP_SETTINGS, 
               qmi_wdai_set_qmap_settings),
  WDA_HDLR(WDAI_CMD_VAL_GET_QMAP_SETTINGS, 
               qmi_wdai_get_qmap_settings)
};

/*---------------------------------------------------------------------------
  QMI callback definition
---------------------------------------------------------------------------*/
static boolean qmi_wdai_alloc_clid_cb
(
  qmi_framework_common_msg_hdr_type * svc_common_hdr
);

static void qmi_wdai_dealloc_clid_cb
(
  qmi_framework_common_msg_hdr_type * svc_common_hdr
); 

static void qmi_wdai_init_cb
(
  uint16 num_instances
);

static void qmi_wdai_cmd_hdlr_cb
( 
  qmi_framework_msg_hdr_type* msg_hdr,
  dsm_item_type ** sdu
);

/*---------------------------------------------------------------------------
  QMI Service (QMI_WDA) configuration definition
---------------------------------------------------------------------------*/
static qmi_wda_svc_config_type  qmi_wdai_cfg;

static struct
{ 
  boolean inited;
} qmi_wdai_global = {FALSE,};

/*===========================================================================

                       FORWARD FUNCTION DECLARATIONS 

===========================================================================*/
static void qmi_wdai_process_cmd(void *);

static void qmi_wdai_process_svc_init
(
  uint16 num_instances
);

static void qmi_wdai_process_cmd_hdlr
(
  qmi_framework_msg_hdr_type* msg_hdr,
  dsm_item_type * sdu_in 
);

static void qmi_wdai_process_alloc_clid
(
  qmi_framework_common_msg_hdr_type * common_msg_hdr
);

static void qmi_wdai_process_dealloc_clid
(
  qmi_framework_common_msg_hdr_type * common_msg_hdr
);

static void qmi_wdai_send_response
(
  qmi_framework_msg_hdr_type *  msg_hdr,
  dsm_item_type              *  msg_ptr
);


static void *qmi_wdai_get_cmd_buf(qmi_cmd_id_e_type);

#define qmi_wdai_free_cmd_buf(buf_ptr) PS_SYSTEM_HEAP_MEM_FREE(buf_ptr)

/*===========================================================================
  FUNCTION QMI_WDA_INIT()

  DESCRIPTION
    Register the WDA service with QMI Framework 

  PARAMETERS
    None

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void qmi_wda_init
(
  void
)
{
  qmi_framework_err_e_type  reg_result = QMI_FRAMEWORK_ERR_NONE;
  qmi_idl_service_object_type   svc_obj;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_INFO1_0("QMI WDA service init");

  /*-------------------------------------------------------------------------
    Setting QMI WDA service command handler with dcc task process
  -------------------------------------------------------------------------*/
  qmi_task_set_svc_cmd_handler(QMUX_SERVICE_WDA, qmi_wdai_process_cmd);

  /*-------------------------------------------------------------------------
    QMI WDS Admin service configuration setup
  -------------------------------------------------------------------------*/
  qmi_wdai_cfg.fw_cfg.base_version.major     = WDAI_BASE_VER_MAJOR;
  qmi_wdai_cfg.fw_cfg.base_version.minor     = WDAI_BASE_VER_MINOR;

  qmi_wdai_cfg.fw_cfg.addendum_version.major = WDAI_ADDENDUM_VER_MAJOR;
  qmi_wdai_cfg.fw_cfg.addendum_version.minor = WDAI_ADDENDUM_VER_MINOR;

  qmi_wdai_cfg.fw_cfg.cbs.alloc_clid         = qmi_wdai_alloc_clid_cb;
  qmi_wdai_cfg.fw_cfg.cbs.dealloc_clid       = qmi_wdai_dealloc_clid_cb;
  qmi_wdai_cfg.fw_cfg.cbs.init_cback         = qmi_wdai_init_cb;
  qmi_wdai_cfg.fw_cfg.cbs.cmd_hdlr           = qmi_wdai_cmd_hdlr_cb;

  qmi_wdai_cfg.cmd_hdlr_array                = qmi_wdai_cmd_callbacks;
  qmi_wdai_cfg.cmd_num_entries               = WDAI_CMD_MAX;
  qmi_wdai_cfg.sp                            = &qmi_wda_state;

  /*-------------------------------------------------------------------------
    Calling QMI Framework API to register the service with Framework
  -------------------------------------------------------------------------*/
  reg_result = qmi_framework_reg_service( QMUX_SERVICE_WDA,
                                          &qmi_wdai_cfg.fw_cfg );

  if (reg_result != QMI_FRAMEWORK_ERR_NONE)
  {
    ASSERT(0);
    return;
  }
  svc_obj =  wda_get_service_object_v01();
  (void) qmi_si_register_object ( svc_obj,
                                  0, /* Service Instance */
                                  wda_get_service_impl_v01() );

  qmi_wdai_cfg.registered  = TRUE;

} /* qmi_wda_init */

/*===========================================================================
  FUNCTION qmi_wdai_process_cmd()

  DESCRIPTION
    This function processes a QMI WDA command

    It is called by the QMI command handler and will dispatch the
    associated command/event handler function.

  PARAMETERS
    cmd_ptr:  private data buffer containing the QMI WDA command
              information.

  RETURN VALUE
    None

  DEPENDENCIES
    QMI WDA must already have been initialized

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_wdai_process_cmd
(
  void *cmd_ptr
)
{
  qmi_wdai_cmd_buf_type *cmd_buf_ptr = NULL;
/*-------------------------------------------------------------------------*/

  ASSERT(cmd_ptr);
  cmd_buf_ptr = (qmi_wdai_cmd_buf_type *)cmd_ptr;

  LOG_MSG_INFO1_1("QMI WDA process cmd: cmd_id:%d", cmd_buf_ptr->cmd_id);

  switch(cmd_buf_ptr->cmd_id)
  {
    case QMI_CMD_WDA_INIT_CB:
    {
      qmi_wdai_process_svc_init(cmd_buf_ptr->data.init_cb.num_instances);
      break;
    }

    case QMI_CMD_WDA_CMD_HDLR_CB:
    {
      qmi_wdai_process_cmd_hdlr(&cmd_buf_ptr->data.cmd_hdlr_cb.msg_hdr,
                                    cmd_buf_ptr->data.cmd_hdlr_cb.sdu_in );
      break;
    }

    case QMI_CMD_WDA_ALLOC_CLID_CB:
    {
      qmi_wdai_process_alloc_clid(&cmd_buf_ptr->data.clid_cb.common_msg_hdr);
      break;
    }

    case QMI_CMD_WDA_DEALLOC_CLID_CB:
    {
      qmi_wdai_process_dealloc_clid(&cmd_buf_ptr->data.clid_cb.common_msg_hdr);
      break;
    }

    default:
      ASSERT(0);
  }
  qmi_wdai_free_cmd_buf(cmd_ptr);
  return;
} /* qmi_wdai_process_cmd() */

/*===========================================================================
FUNCTION QMI_WDAI_INIT_CB()

  DESCRIPTION
  Function gets called from framework whenever the QMI WDA service registers with
  framework. This function posts a DCC command for the service to handle this
  callback.
    
  PARAMETERS
  num_instances : getting Num of QMI instances at run time.

  RETURN VALUE
  None
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_wdai_init_cb
(
  uint16 num_instances
)
{
  qmi_wdai_cmd_buf_type *cmd_ptr;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  cmd_ptr = (qmi_wdai_cmd_buf_type *)qmi_wdai_get_cmd_buf(QMI_CMD_WDA_INIT_CB);
  if( cmd_ptr == NULL)
  {
    return;
  }

  memset(cmd_ptr, 0, sizeof(qmi_wdai_cmd_buf_type));
  cmd_ptr->cmd_id = QMI_CMD_WDA_INIT_CB;
  cmd_ptr->data.init_cb.num_instances = num_instances;

  dcc_send_cmd_ex(DCC_QMI_CMD, cmd_ptr);

} /* qmi_wdai_init_cb() */

/*===========================================================================
FUNCTION QMI_WDAI_ALLOC_CLID_CB()

DESCRIPTION
  Function gets called from framework whenever a clid is allocated by the
  framework for QMI WDA service. This function posts a DCC command for the
  service to handle this callback.

PARAMETERS
  common_msg_hdr  : QMI Framework common msg hdr

RETURN VALUE
  Boolean : TRUE if the clid set properly
          : FALSE, otherwise
DEPENDENCIES
  None

SIDE EFFECTS
  None
===========================================================================*/
static boolean qmi_wdai_alloc_clid_cb
(  
  qmi_framework_common_msg_hdr_type*   common_msg_hdr
)
  {
  qmi_wdai_cmd_buf_type *cmd_ptr;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(common_msg_hdr);

  LOG_MSG_INFO1_2("QMI WDA alloc clid cb: clid:%d, QMI Instances:%d",
                  common_msg_hdr->client_id, common_msg_hdr->qmi_instance);

  cmd_ptr = (qmi_wdai_cmd_buf_type *)qmi_wdai_get_cmd_buf(QMI_CMD_WDA_ALLOC_CLID_CB);
  if( cmd_ptr == NULL)
    {
    return FALSE;
    }

  /*-------------------------------------------------------------------------
    copy the framework common message header to local struct
  -------------------------------------------------------------------------*/
  memset(cmd_ptr, 0, sizeof(qmi_wdai_cmd_buf_type));
  cmd_ptr->cmd_id = QMI_CMD_WDA_ALLOC_CLID_CB;
  cmd_ptr->data.clid_cb.common_msg_hdr.service = common_msg_hdr->service;
  cmd_ptr->data.clid_cb.common_msg_hdr.client_id = common_msg_hdr->client_id;
  cmd_ptr->data.clid_cb.common_msg_hdr.transaction_id = common_msg_hdr->transaction_id;
  cmd_ptr->data.clid_cb.common_msg_hdr.qmi_instance = common_msg_hdr->qmi_instance;

  dcc_send_cmd_ex(DCC_QMI_CMD, cmd_ptr);
  return TRUE;
} /* qmi_wdai_alloc_clid_cb */

/*===========================================================================
FUNCTION QMI_WDAI_DEALLOC_CLID_CB()

DESCRIPTION 
  Function gets called from framework whenever a clid is deallocated by the
  framework for QMI WDA service. This function posts a DCC command for the
  service to handle this callback.

PARAMETERS
  common_msg_hdr  : QMI Framework common msg hdr

RETURN VALUE
  None
 
DEPENDENCIES
  None

SIDE EFFECTS
  None
===========================================================================*/
static void qmi_wdai_dealloc_clid_cb
(  
  qmi_framework_common_msg_hdr_type*   common_msg_hdr  
)
{
  qmi_wdai_cmd_buf_type *cmd_ptr;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(common_msg_hdr);
  LOG_MSG_INFO1_2("QMI WDA dealloc clid cb: clid:%d, QMI Instances:%d",
                  common_msg_hdr->client_id, common_msg_hdr->qmi_instance);

  cmd_ptr = (qmi_wdai_cmd_buf_type *)qmi_wdai_get_cmd_buf(QMI_CMD_WDA_DEALLOC_CLID_CB);
  if( cmd_ptr == NULL)
  {
    return;
  }

  /*-------------------------------------------------------------------------
    copy the framework common message header to local struct
  -------------------------------------------------------------------------*/
  memset(cmd_ptr, 0, sizeof(qmi_wdai_cmd_buf_type));
  cmd_ptr->cmd_id = QMI_CMD_WDA_DEALLOC_CLID_CB;
  cmd_ptr->data.clid_cb.common_msg_hdr.service = common_msg_hdr->service;
  cmd_ptr->data.clid_cb.common_msg_hdr.client_id = common_msg_hdr->client_id;
  cmd_ptr->data.clid_cb.common_msg_hdr.transaction_id = common_msg_hdr->transaction_id;
  cmd_ptr->data.clid_cb.common_msg_hdr.qmi_instance = common_msg_hdr->qmi_instance;

  /*-------------------------------------------------------------------------
    Positing command to QMI Service task to handle the clid callback
  -------------------------------------------------------------------------*/
  dcc_send_cmd_ex(DCC_QMI_CMD, cmd_ptr);
} /* qmi_wdai_dealloc_clid_cb */

/*===========================================================================
  FUNCTION FRAMEWORK_WDAI_CMD_HDLR_CB()

  DESCRIPTION
    Function gets called from framework whenever a new QMI WDA request
    message is received. This function posts a DCC command for the
    service to handle this callback.

  PARAMETERS
    msg_hdr  :  msg hdr
    sdu : dsm item
 
  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void  qmi_wdai_cmd_hdlr_cb
( 
  qmi_framework_msg_hdr_type * msg_hdr,
  dsm_item_type             ** sdu 
)
{
  qmi_wdai_cmd_buf_type *cmd_ptr;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(msg_hdr);
  LOG_MSG_INFO1_3("qmi_wdai_cmd_hdlr_cb: clid:%d, QMI Instances:%d, tx_id: %d",
                  msg_hdr->common_hdr.client_id,
                  msg_hdr->common_hdr.qmi_instance,
                  msg_hdr->common_hdr.transaction_id);
  
  cmd_ptr = (qmi_wdai_cmd_buf_type *)qmi_wdai_get_cmd_buf(QMI_CMD_WDA_CMD_HDLR_CB);
  if( cmd_ptr == NULL)
  {
    return;
  }

  /*-------------------------------------------------------------------------
    copy the framework message header to local struct
  -------------------------------------------------------------------------*/
  memset(cmd_ptr, 0, sizeof(qmi_wdai_cmd_buf_type));
  cmd_ptr->cmd_id = QMI_CMD_WDA_CMD_HDLR_CB;
  cmd_ptr->data.cmd_hdlr_cb.msg_hdr.common_hdr.service = 
                                        msg_hdr->common_hdr.service;
  cmd_ptr->data.cmd_hdlr_cb.msg_hdr.common_hdr.client_id = 
                                        msg_hdr->common_hdr.client_id;
  cmd_ptr->data.cmd_hdlr_cb.msg_hdr.common_hdr.transaction_id = 
                                        msg_hdr->common_hdr.transaction_id;
  cmd_ptr->data.cmd_hdlr_cb.msg_hdr.common_hdr.qmi_instance = 
                                        msg_hdr->common_hdr.qmi_instance;
  cmd_ptr->data.cmd_hdlr_cb.msg_hdr.msg_ctl_flag = msg_hdr->msg_ctl_flag; 
  cmd_ptr->data.cmd_hdlr_cb.msg_hdr.msg_len = msg_hdr->msg_len; 
  cmd_ptr->data.cmd_hdlr_cb.sdu_in = *sdu;

  /*-------------------------------------------------------------------------
    Posting command to QMI Service task to handle the service request
  -------------------------------------------------------------------------*/
  dcc_send_cmd_ex(DCC_QMI_CMD, cmd_ptr);
} /* qmi_wdai_cmd_hdlr_cb */

/*===========================================================================
  FUNCTION QMI_WDAI_GET_CMD_BUF()

  DESCRIPTION
    Allocate and assign a QMI AT command buffer from the PS MEM heap
    based on the QMI CMD type
    
  PARAMETERS
    cmd - QMI command type
   
  RETURN VALUE
    cmd_buf_ptr - Pointer to the allocated command buffer

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void *qmi_wdai_get_cmd_buf
(
  qmi_cmd_id_e_type cmd_id
)
{
  LOG_MSG_INFO1_1("wdai_get_cmd_buf cmd_id = %d",cmd_id);
  switch (cmd_id)
  {
    case QMI_CMD_WDA_INIT_CB:
    case QMI_CMD_WDA_ALLOC_CLID_CB:
    case QMI_CMD_WDA_DEALLOC_CLID_CB:
    case QMI_CMD_WDA_CMD_HDLR_CB:
    {
      qmi_wdai_cmd_buf_type *cmd_buf_ptr;
      cmd_buf_ptr = qmi_svc_ps_system_heap_mem_alloc(sizeof(qmi_wdai_cmd_buf_type),FILE_ID_DS_QMI_WDA,__LINE__);
      return ((void *)cmd_buf_ptr);
    }

    default:
	break;
  }

  return NULL;
} /* qmi_wdai_get_cmd_buf */

/*===========================================================================
  FUNCTION QMI_WDAI_PROCESS_SVC_INIT()

  DESCRIPTION
    This function processes a init callback request and intialize the service
    related infos.

  PARAMETERS
    num_instances:  nnumber of QMI Instances

  RETURN VALUE
    None

  DEPENDENCIES
    QMI WDA must already have been initialized and registered with Framework

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_wdai_process_svc_init
(
  uint16 num_instances
)
{
 qmi_wdai_state_type *         wda_sp;
/*-------------------------------------------------------------------------*/

  LOG_MSG_INFO1_0( "qmi_wdai init callback " );
  wda_sp = &qmi_wda_state;
  memset( wda_sp, 0, sizeof(qmi_wdai_state_type) );

  /*-------------------------------------------------------------------------
    Max QMI Instances
  -------------------------------------------------------------------------*/
  wda_sp->num_qmi_instances = num_instances;
  /*-------------------------------------------------------------------------
    initialize client state non-zero fields
  -------------------------------------------------------------------------*/
  if(qmi_wdai_global.inited == FALSE)
  {
    qmi_wdai_global.inited = TRUE;
  }
} /* qmi_wdai_process_svc_init */

/*===========================================================================
  FUNCTION QMI_WDAI_PROCESS_CMD_HDLR()

  DESCRIPTION
    This function process the service command handler request callback.
    This function creates a transaction and dispatches to the appropriate
    message handler
    
  PARAMETERS
    msg_hdr:  Framework message header for the request.
    sdu_in : request

  RETURN VALUE
    None

  DEPENDENCIES
    QMI WDA must already have been initialized and registered with Framework

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_wdai_process_cmd_hdlr
(
  qmi_framework_msg_hdr_type* msg_hdr,
  dsm_item_type * sdu_in 
)
{
  int32                         temp;
  uint16                        cmd_type;
  uint16                        sdu_len;
  uint16                        remaining_bytes;
  qmi_svc_cmd_hdlr_type         *cmd_hdlr;
  uint16                        cmd;
  boolean                       retval;
  dsm_item_type *               response = NULL;
  uint8                         err_val =0;   
  #define ERR_SVC_HDR_CMD_TYPE 1
  #define ERR_SVC_HDR_SDU_LEN  2
  #define ERR_SVC_SDU_LEN      3
  #define ERR_SVC_NO_RESPONSE  4
/*-------------------------------------------------------------------------*/

  ASSERT(msg_hdr && sdu_in);
  LOG_MSG_INFO1_3( "Process QMI WDA svc command handler callback function: clid%d, tx_id:%d, ctl_flag:%d ",
                   msg_hdr->common_hdr.client_id,
                   msg_hdr->common_hdr.transaction_id,
                   msg_hdr->msg_ctl_flag );

  /*-----------------------------------------------------------------------
    Extract service message header
  -----------------------------------------------------------------------*/
  remaining_bytes = (uint16) dsm_length_packet(sdu_in);

  temp = dsm_pull16( &sdu_in ); 
  if (temp == -1)
  {
    err_val = ERR_SVC_HDR_CMD_TYPE;
    dsm_free_packet( &sdu_in );
    goto send_result;
  }
  cmd_type = ps_ntohs(temp);
  
  temp = dsm_pull16( &sdu_in );
  if (temp == -1)
  {
    err_val = ERR_SVC_HDR_SDU_LEN;
    dsm_free_packet( &sdu_in );
    goto send_result;

  }

  sdu_len = ps_ntohs(temp);
  remaining_bytes -= 4;
  
  if (sdu_len > remaining_bytes)
  {
    err_val = ERR_SVC_SDU_LEN;
    dsm_free_packet( &sdu_in );
    goto send_result;
  }
  else if (sdu_len < remaining_bytes)
  {
    /* no bundling for now */
    LOG_MSG_ERROR_3 ("QMI WDA service does not support bundling message into one transaction. sdu_len %d, remaining_bytes %d, msg_len %d",
                     sdu_len, remaining_bytes, msg_hdr->msg_len);
    dsm_trim_packet( &sdu_in, sdu_len );
  }

  /*-------------------------------------------------------------------------
    Process the service request and send a response back to client via
    QMI Framework.
  -------------------------------------------------------------------------*/
  cmd_hdlr = qmi_wdai_cfg.cmd_hdlr_array;
  for (cmd = 0; cmd < qmi_wdai_cfg.cmd_num_entries; cmd++, cmd_hdlr++)
  {
    if (cmd_type == cmd_hdlr->cmd_type)
    {
      break;
    }
  }

  /* Send the response if request is invalid.*/
  if ( cmd == qmi_wdai_cfg.cmd_num_entries )
  {
    LOG_MSG_ERROR_1( "Unrecognized type (=%d) for WDAI service ! Return error",
                     cmd_type  );

    retval = qmi_svc_put_result_tlv( &response,
                                     QMI_RESULT_FAILURE,
                                     QMI_ERR_INVALID_QMI_CMD );
    if (FALSE == retval)
    {
     dsm_free_packet(&response);
     response = NULL;
    }

    dsm_free_packet(&sdu_in);

  }
  else
  {
    /*-------------------------------------------------------------------------
      call appropirate QMI WDSI ADMIN message handler and send the response back to
      QMI Framework.
    -------------------------------------------------------------------------*/
    response = cmd_hdlr->request_hdlr( NULL, msg_hdr, NULL, &sdu_in );

    dsm_free_packet(&sdu_in);

    if (response == NULL)
    {
      err_val = ERR_SVC_NO_RESPONSE;
      goto send_result;
    }

  }

  if(FALSE == qmi_svc_prepend_msg_hdr(&response, cmd_type)) 
  {
    return;
  }

  msg_hdr->msg_ctl_flag = QMI_FLAG_MSGTYPE_RESP;
  msg_hdr->msg_len  = (uint16) dsm_length_packet(response);

  /*-------------------------------------------------------------------------
    Send response if ready.
  -------------------------------------------------------------------------*/
  qmi_wdai_send_response( msg_hdr, response );
  send_result:
   if(err_val != 0)
   {
     LOG_MSG_ERROR_2("error %d service %x",err_val,msg_hdr->common_hdr.service);
   }
   return;

} /* qmi_wdai_process_cmd_hdlr */

/*===========================================================================
  FUNCTION QMI_WDAI_PROCESS_ALLOC_CLID()

  DESCRIPTION
    This function process the client alloc callback request. This function
    allocates and initialize the new client state for the allocated client ID.

  PARAMETERS
    common_msg_hdr:  private data buffer containing the clid alloc request information.

  RETURN VALUE
    None

  DEPENDENCIES
    QMI WDA must already have been initialized and registered with Framework

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_wdai_process_alloc_clid
(
  qmi_framework_common_msg_hdr_type * common_msg_hdr
)
{
  qmi_result_e_type                    result = QMI_RESULT_FAILURE;
  qmi_wda_instace_state_type     *instance_sp;
  qmi_error_e_type                errval = QMI_ERR_NONE;
/*-------------------------------------------------------------------------*/

  ASSERT(common_msg_hdr);

  if (common_msg_hdr->qmi_instance >= QMI_INSTANCE_MAX)
  {
    return;
  }


  LOG_MSG_INFO1_0( "qmi_wdai process clid allocation" );

  instance_sp = (qmi_wda_instace_state_type *) 
                &qmi_wda_state.instance[common_msg_hdr->qmi_instance];

  if(0 == instance_sp->client_id)
  {
    instance_sp->qmi_instance = common_msg_hdr->qmi_instance;
    instance_sp->client_id = common_msg_hdr->client_id;
    result = QMI_RESULT_SUCCESS;
  }
  else
  {
      LOG_MSG_ERROR_2( "QMI WDA clid allocation failed, qmi_instance [%d], client_id [%d]",
                       common_msg_hdr->qmi_instance,
                       common_msg_hdr->client_id );
      result = QMI_RESULT_FAILURE;
      errval = QMI_ERR_INTERNAL;
  }

  /* Notifying the clid allocation back to client via QMI Framework*/
   qmi_framework_svc_send_alloc_clid_result_ex(result, common_msg_hdr, 
                                           errval);

} /* qmi_wdai_process_alloc_clid */

/*===========================================================================
  FUNCTION QMI_WDAI_PROCESS_DEALLOC_CLID()

  DESCRIPTION
    This function process the client dealloc callback request. This function
    reset and deallocates the new client state for the allocated client ID.

  PARAMETERS
    common_msg_hdr:  private data buffer containing the clid dealloc
    request information.
 
  RETURN VALUE
    None

  DEPENDENCIES
    QMI WDA must already have been initialized and registered with Framework

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_wdai_process_dealloc_clid
(
  qmi_framework_common_msg_hdr_type * common_msg_hdr
)
{
  qmi_result_e_type                    result = QMI_RESULT_FAILURE;
  qmi_wda_instace_state_type    *instance_sp;
  qmi_error_e_type                errval = QMI_ERR_NONE;
/*-------------------------------------------------------------------------*/

  ASSERT(common_msg_hdr);

  if (common_msg_hdr->qmi_instance >= QMI_INSTANCE_MAX)
  {
    return;
  }


  LOG_MSG_INFO1_0( "qmi_wdai process clid deallocation" );

  instance_sp = (qmi_wda_instace_state_type *) 
                &qmi_wda_state.instance[common_msg_hdr->qmi_instance];

  instance_sp->qmi_instance = 0;
  instance_sp->client_id = 0;

  result = QMI_RESULT_SUCCESS;

  /* Notifying the clid allocation back to client via QMI Framework*/
  qmi_framework_svc_send_dealloc_clid_result_ex(result, common_msg_hdr,
                                              errval);

} /* qmi_wdai_process_dealloc_clid */


/*===========================================================================
FUNCTION QMI_WDAI_SEND_RESPONSE()

DESCRIPTION
  This function calls QMI Framework API to send a QMI WDA Service response to
  clients.

PARAMETERS 
  msg_hdr  : Framework message header for the request.
  response : Response message

RETURN VALUE
  None

DEPENDENCIES
  QMI WDA service must be initialized and registered with Framework

SIDE EFFECTS
  None
===========================================================================*/
static void qmi_wdai_send_response
(
  qmi_framework_msg_hdr_type      *msg_hdr,
  dsm_item_type *                  response
)
{

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(msg_hdr && response);

  if(TRUE != qmi_framework_svc_send_response(msg_hdr, response))
  {
    LOG_MSG_ERROR_0( "Unable to send QMI WDA response to QMI Framework" );
  }

} /* qmi_wdai_send_response */


/*===========================================================================
  FUNCTION QMI_WDAI_SET_DATA_FORMAT()

  DESCRIPTION
    Handle the QMI WDS Admin set data format message.

    Dispatches a request to the specified service to set the data format.

  PARAMETERS
    svc_sp:      QMI_WDA's service instance state pointer for this qmi link
                 returned by qmux_reg_service()
    msg_hdr:     Message Header
    cl_sp:       Coresponding client state pointer
    sdu_in:      input command data

  RETURN VALUE
    dsm * pointing to the response to be sent to host
    NULL if no response was generated or command was discarded

  DEPENDENCIES
    qmi_wda_init() must have been called

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type* qmi_wdai_set_data_format
(
void* svc_sp,
void* msg_hdr,
void* cl_sp,
dsm_item_type ** sdu_in
)
{
  uint8                   type;
  uint16                  len;
  uint16                  expected_len;
  void *                  value;
  dsm_item_type *         response;
  qmi_result_e_type       result;
  qmi_error_e_type        errval;
  rmnet_meta_sm_data_format_type data_format;
  boolean                 qos = FALSE;
  uint32                  qos_format = RMNET_QOS_FORMAT_6_BYTE;
  uint32                  data_format_mask = 0;
  uint32                  link_prot = 0;
  uint32                  ul_data_agg = 0;
  uint32                  dl_data_agg = 0;
  uint32                  ndp_signature    = 0;
  uint32                  dl_data_max_num  = 0;
  uint32                  dl_data_max_size = 0;
  uint32                  dl_min_padding = 0;
  rmnet_set_data_fmt_ret_e_type ret_type   = RMNET_SET_DATA_FORMAT_FAILURE;
  rmnet_instance_e_type   rmnet_instance;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(msg_hdr && sdu_in);

  errval = QMI_ERR_NONE;
  response = NULL;
  memset((void *)&data_format, 0, sizeof(rmnet_meta_sm_data_format_type));

  VALIDATE_REQUEST_INSTANCE(((qmi_framework_msg_hdr_type*)msg_hdr)->common_hdr.qmi_instance);

  rmnet_instance = QMI_IF_GET_RMNET_INSTANCE(((qmi_framework_msg_hdr_type*)msg_hdr)->common_hdr.qmi_instance,1);

  /*-------------------------------------------------------------------------
    Parse incoming command
    - identify and save expected TLVs,
      if duplicate TLV found, last present is used.
    - discard unexpected TLVs
  -------------------------------------------------------------------------*/
  while (*sdu_in)
  {
    if( !qmi_svc_get_param_tl( sdu_in, &type, &len ) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_QOS_DATA_FORMAT:
        expected_len = sizeof(uint8);
        data_format_mask |= RMNET_DATA_FORMAT_MASK_QOS;
        value = (void *)&qos;
        break;

      case QMI_TYPE_LINK_PROTOCOL:
        expected_len = sizeof(uint32);
        data_format_mask |= RMNET_DATA_FORMAT_MASK_LINK_PROT;
        value = (void *)&link_prot;
        break;

      case QMI_TYPE_UL_DATA_AGG_PROTOCOL:
        expected_len = sizeof(uint32);
        data_format_mask |= RMNET_DATA_FORMAT_MASK_UL_DATA_AGG;
        value = (void *)&ul_data_agg;
        break;

      case QMI_TYPE_DL_DATA_AGG_PROTOCOL:
        expected_len = sizeof(uint32);
        data_format_mask |= RMNET_DATA_FORMAT_MASK_DL_DATA_AGG;
        value = (void *)&dl_data_agg;
        break;

      case QMI_TYPE_NDP_SIGNATURE:
        expected_len = sizeof(uint32);
        data_format_mask |= RMNET_DATA_FORMAT_MASK_NDP_SIGNATURE;
        value = (void *)&ndp_signature;
        break;

      case QMI_TYPE_DL_DATA_AGG_MAX_DATAGRAMS:
        expected_len = sizeof(uint32);
        data_format_mask |= RMNET_DATA_FORMAT_MASK_DL_DATA_AGG_NUM;
        value = (void *)&dl_data_max_num;
        break;

      case QMI_TYPE_DL_DATA_AGG_MAX_SIZE:
        expected_len = sizeof(uint32);
        data_format_mask |= RMNET_DATA_FORMAT_MASK_DL_DATA_AGG_SIZE;
        value = (void *)&dl_data_max_size;
        break;

      case QMI_TYPE_QOS_HEADER_FORMAT_REQ:
        expected_len = sizeof(uint32);
        data_format_mask |= RMNET_DATA_FORMAT_MASK_QOS_FORMAT;
        value = (void *)&qos_format;
        break;

      case QMI_TYPE_DL_MIN_PADDING_REQ:
        expected_len = sizeof(uint32);
        data_format_mask |= RMNET_DATA_FORMAT_MASK_DL_MIN_PADDING;
        value = (void *)&dl_min_padding;
        break;

      default:
        LOG_MSG_INFO2_1 ("Unexpected TLV type (%d)", type);
        break;
    }
    if(!qmi_svc_validate_type_length( type,
                                      len,
                                      expected_len,
                                      FILE_ID_DS_QMI_WDA,__LINE__) )
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    if( len != qmi_svc_dsm_pullup( sdu_in, value, len ,FILE_ID_DS_QMI_WDA,__LINE__) )
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  } /* end while */

  data_format.qos = qos;
  data_format.qos_format = (rmnet_qos_format_e_type)qos_format;
  data_format.link_prot = (uint16) link_prot;
  data_format.data_agg_protocol.ul_data_agg_protocol = (rmnet_data_agg_enum_type)ul_data_agg;
  data_format.data_agg_protocol.dl_data_agg_protocol = (rmnet_data_agg_enum_type)dl_data_agg;
  data_format.data_agg_protocol.ndp_signature = ndp_signature;
  data_format.data_agg_protocol.dl_min_padding = (uint8)dl_min_padding;

  /*-------------------------------------------------------------------------
    Perform validity checks for QoS header format
  -------------------------------------------------------------------------*/
  if (data_format_mask & RMNET_DATA_FORMAT_MASK_QOS_FORMAT)
  {
    if (qos_format != RMNET_QOS_FORMAT_6_BYTE &&
        qos_format != RMNET_QOS_FORMAT_8_BYTE)
    {
      LOG_MSG_INFO2("Invalid QoS header format %d", qos_format, 0, 0);
      errval = QMI_ERR_INVALID_ARG;
      goto send_result;
    }
  }

  /*-------------------------------------------------------------------------
    Perform validity checks for DL max agg pkts.
  -------------------------------------------------------------------------*/
  if ((data_format_mask & RMNET_DATA_FORMAT_MASK_DL_DATA_AGG))
  {
    /*MBIM mode*/
    if (data_format.data_agg_protocol.dl_data_agg_protocol ==
        RMNET_ENABLE_DATA_AGG_MBIM)
    {
  data_format.data_agg_protocol.dl_data_agg_max_num = 
    (dl_data_max_num > PS_DL_OPT_MBIM_MAX_NDP_AGGR_DGRMS) ? 
    PS_DL_OPT_MBIM_MAX_NDP_AGGR_DGRMS : dl_data_max_num;
    }
    /*RNDIS mode*/
    else if (data_format.data_agg_protocol.dl_data_agg_protocol ==
              RMNET_ENABLE_DATA_AGG_RNDIS)
    {
      LOG_MSG_INFO1_1("dl_data_agg_max_num got from QTI %d", dl_data_max_num);
      data_format.data_agg_protocol.dl_data_agg_max_num =
      (dl_data_max_num > RMNET_DATA_FORMAT_MAX_RNDIS_AGGR_PKTS) ?
      RMNET_DATA_FORMAT_MAX_RNDIS_AGGR_PKTS : dl_data_max_num;
    }
    else if ( data_format.data_agg_protocol.dl_data_agg_protocol ==
                                   RMNET_ENABLE_DATA_AGG_QMAP
#ifdef FEATURE_DATA_QMAP_DEBUG
           #error code not present
#endif
            )
    {
      data_format.data_agg_protocol.dl_data_agg_max_num =
            (dl_data_max_num > RMNET_DATA_FORMAT_QMAP_MAX_AGGR_PKTS) ?
            RMNET_DATA_FORMAT_QMAP_MAX_AGGR_PKTS: dl_data_max_num;
    }
  }

  data_format.data_agg_protocol.dl_data_agg_max_size = dl_data_max_size;

  /* Reset local variable to 0 to re-use in response TLV*/
  link_prot = 0;

  /*-------------------------------------------------------------------------
    Perform validity checks for Link Protocol Mode
  -------------------------------------------------------------------------*/
  if( (data_format_mask & RMNET_DATA_FORMAT_MASK_LINK_PROT) && 
      ((data_format.link_prot == ((uint16) RMNET_INVALID_MODE)) ||
       (data_format.link_prot & ((uint16) ~RMNET_ALL_MODE))))
  {
    LOG_MSG_INFO2_1 ("Invalid link protocol(s) specified: 0x%x!",
                     data_format.link_prot);
    errval = QMI_ERR_INVALID_ARG;
    goto send_result;
  }

  /*-------------------------------------------------------------------------
    Perform validity checks for DL_DATA_AGG_MAX_SIZE.
  -------------------------------------------------------------------------*/
  if( (data_format_mask & RMNET_DATA_FORMAT_MASK_DL_DATA_AGG_SIZE))
  {
    /*MBIM mode*/
    if( (data_format.data_agg_protocol.dl_data_agg_protocol ==
         RMNET_ENABLE_DATA_AGG_MBIM) &&
        (data_format.data_agg_protocol.dl_data_agg_max_size <
         RMNET_DATA_FORMAT_MINIMUM_DL_DATA_AGG_SIZE)
      )
  {
      LOG_MSG_INFO2_1 ("Invalid dl_data_agg_max_size specified for MBIM: 0x%d!",
                       data_format.data_agg_protocol.dl_data_agg_max_size);
    errval = QMI_ERR_INVALID_ARG;
    goto send_result;
  }

    /*RNDIS mode*/
    else if ( (data_format.data_agg_protocol.dl_data_agg_protocol ==
               RMNET_ENABLE_DATA_AGG_RNDIS) &&
              (data_format.data_agg_protocol.dl_data_agg_max_size <
               RMNET_DATA_FORMAT_MIN_RNDIS_DL_DATA_AGG_SIZE)
            )
    {
      LOG_MSG_INFO2_1 ("Invalid dl_data_agg_max_size specified for RNDIS: 0x%d!",
                       data_format.data_agg_protocol.dl_data_agg_max_size);
      errval = QMI_ERR_INVALID_ARG;
      goto send_result;
    }
  }
#if 0
  /*-------------------------------------------------------------------------
    Perform validity checks for UL/DL data agg
  -------------------------------------------------------------------------*/
  if ( (data_format_mask & RMNET_DATA_FORMAT_MASK_UL_DATA_AGG) !=
        (data_format_mask & RMNET_DATA_FORMAT_MASK_DL_DATA_AGG) )
  {
    LOG_MSG_ERROR_0 ("Invalid data agg TLV's specified: 0x%x!");
    errval = QMI_ERR_INVALID_ARG;
    goto send_result;
  }
  else
  {
    if (data_format.data_agg_protocol.ul_data_agg_protocol != 
        data_format.data_agg_protocol.dl_data_agg_protocol)
    {
      LOG_MSG_ERROR_0 ("Invalid data agg TLV's specified: 0x%x!");
      errval = QMI_ERR_INVALID_ARG;
      goto send_result;
    }

  }
#endif

   /*-------------------------------------------------------------------------
    Perform action/gather required information for response
    - get the service configuration block of the specified QMI service
    - allocate a client ID from the specified QMI service
  -------------------------------------------------------------------------*/
  LOG_MSG_INFO1_6("Got QMI_WDA set data format mask:%d, QoS format[%d], Link Prot[%d], UL data agg[%d], DL data agg[%d], NDP sig[%d]",
                  data_format_mask,
                  data_format.qos,
                  data_format.link_prot,
                  data_format.data_agg_protocol.ul_data_agg_protocol,
                  data_format.data_agg_protocol.dl_data_agg_protocol,
                  data_format.data_agg_protocol.ndp_signature );
  LOG_MSG_INFO1_2("Got QMI_WDA set data format DL data agg max num[%d], DL data agg max size[%d]",
                  data_format.data_agg_protocol.dl_data_agg_max_num,
                  data_format.data_agg_protocol.dl_data_agg_max_size);

  if (RMNET_SET_DATA_FORMAT_QMI_CTL == rmnet_meta_sm_get_data_format_client(rmnet_instance))
  {
    LOG_MSG_ERROR_0("Data format already set by QMI CTL");
    errval = QMI_ERR_INVALID_OPERATION;
    goto send_result;
  }

  ret_type =  rmnet_meta_sm_set_data_format(rmnet_instance,
                                            &data_format,
                                            data_format_mask,
                                            RMNET_SET_DATA_FORMAT_QMI_WDA);
  if(ret_type == RMNET_SET_DATA_FORMAT_FAILURE )
  {
    errval = QMI_ERR_INVALID_OPERATION;
    goto send_result;
  }
  else if(ret_type == RMNET_SET_DATA_FORMAT_SUCCESS)
  {
    ret_type = 
      rmnet_meta_sm_set_data_format(
         (rmnet_instance_e_type)(((int32)rmnet_instance)+1),
          &data_format,
          data_format_mask,
          RMNET_SET_DATA_FORMAT_QMI_WDA);
    if(ret_type == RMNET_SET_DATA_FORMAT_FAILURE )
    {
      errval = QMI_ERR_INVALID_OPERATION;
      goto send_result;
    }
  }

  /*-------------------------------------------------------------------------
    Attach the QoS header format TLV
  -------------------------------------------------------------------------*/
  qos_format = data_format.qos_format;
  if(FALSE == qmi_svc_put_param_tlv(&response,
                                  QMI_TYPE_QOS_HEADER_FORMAT_RESP,
                                  sizeof (uint32),
                                  (void *)&qos_format))
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
  }

  /*-------------------------------------------------------------------------
    DL minimum padding
  -------------------------------------------------------------------------*/
  dl_min_padding = data_format.data_agg_protocol.dl_min_padding;
  if (FALSE == qmi_svc_put_param_tlv(&response,
                                  QMI_TYPE_DL_MIN_PADDING_RESP,
                                  sizeof (uint32),
                                  (void *)&dl_min_padding))
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
  }

  if (data_format.data_agg_protocol.ul_data_agg_max_num != 0)
  {
    /*-------------------------------------------------------------------------
      Attach the UL data aggregation max size TLV response
    -------------------------------------------------------------------------*/
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                    QMI_TYPE_UL_DATA_AGG_MAX_SIZE,
                                    sizeof (uint32),
                                    (void *)&data_format.data_agg_protocol.ul_data_agg_max_size))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
      goto send_result;
    }

    /*-------------------------------------------------------------------------
      Attach the UL data aggregation max datagrams TLV response
    -------------------------------------------------------------------------*/
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                    QMI_TYPE_UL_DATA_AGG_MAX_DATAGRAMS,
                                    sizeof (uint32),
                                    (void *)&data_format.data_agg_protocol.ul_data_agg_max_num))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
      goto send_result;
    }
  }

  /*-------------------------------------------------------------------------
    Attach the DL data aggregation max size TLV response
  -------------------------------------------------------------------------*/
  if(FALSE == qmi_svc_put_param_tlv(&response,
                                    QMI_TYPE_DL_DATA_AGG_MAX_SIZE,
                                    sizeof (uint32),
                                    (void *)&data_format.data_agg_protocol.dl_data_agg_max_size))
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
  }

  /*-------------------------------------------------------------------------
    Attach the DL data aggregation max datagrams TLV response
  -------------------------------------------------------------------------*/
  if(FALSE == qmi_svc_put_param_tlv(&response,
                                    QMI_TYPE_DL_DATA_AGG_MAX_DATAGRAMS,
                                    sizeof (uint32),
                                    (void *)&data_format.data_agg_protocol.dl_data_agg_max_num))
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
  }

  /*-------------------------------------------------------------------------
    Attach the NDP signature TLV response
  -------------------------------------------------------------------------*/
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      QMI_TYPE_NDP_SIGNATURE,
                                      sizeof (uint32),
                                      (void *)&data_format.data_agg_protocol.ndp_signature))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }

  /*-------------------------------------------------------------------------
    Attach the DL data aggregation protocol TLV response
  -------------------------------------------------------------------------*/
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      QMI_TYPE_DL_DATA_AGG_PROTOCOL,
                                      sizeof (uint32),
                                      (void *)&data_format.data_agg_protocol.dl_data_agg_protocol))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }

  /*-------------------------------------------------------------------------
    Attach the UL data aggregation protocol TLV response
  -------------------------------------------------------------------------*/
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      QMI_TYPE_UL_DATA_AGG_PROTOCOL,
                                      sizeof (uint32),
                                      (void *)&data_format.data_agg_protocol.ul_data_agg_protocol))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }

  /*-------------------------------------------------------------------------
    Attach the Link protocol TLV response
  -------------------------------------------------------------------------*/
    link_prot = data_format.link_prot;
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      QMI_TYPE_LINK_PROTOCOL,
                                      sizeof (uint32),
                                      (void *)&link_prot))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }


  /*-------------------------------------------------------------------------
    Attach the QoS TLV response
  -------------------------------------------------------------------------*/
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      QMI_TYPE_QOS_DATA_FORMAT,
                                      sizeof (uint8),
                                      (void *)&data_format.qos))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }


  /*-------------------------------------------------------------------------
    build response
  -------------------------------------------------------------------------*/
send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS
                                   : QMI_RESULT_FAILURE);

  if( FALSE == qmi_svc_put_result_tlv(&response, result, errval) )
  {
    dsm_free_packet(&response);
    return NULL;
  }

  return response;
} /* qmi_wdai_set_data_format */

/*===========================================================================
  FUNCTION QMI_WDAI_GET_DATA_FORMAT()

  DESCRIPTION
    Handle the QMI WDS Admin get data format message.

    Dispatches a request to the specified service to get the data format.

  PARAMETERS
    svc_sp:      QMI_WDA's service instance state pointer for this qmi link
                 returned by qmux_reg_service()
    msg_hdr:     Message Header
    cl_sp:       Coresponding client state pointer
    sdu_in:      input command data

  RETURN VALUE
    dsm * pointing to the response to be sent to host
    NULL if no response was generated or command was discarded

  DEPENDENCIES
    qmi_wda_init() must have been called

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type* qmi_wdai_get_data_format
(
void* svc_sp,
void* msg_hdr,
void* cl_sp,
dsm_item_type ** sdu_in
)
{
  dsm_item_type *         response;
  qmi_result_e_type       result;
  qmi_error_e_type        errval;
  rmnet_meta_sm_data_format_type data_format;
  rmnet_instance_e_type   rmnet_instance;
  uint32                  link_prot = 0;
  uint32                  qos_format;
  uint32                  dl_min_padding;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(msg_hdr && sdu_in);

  errval = QMI_ERR_NONE;
  response = NULL;
  memset((void *)&data_format, 0, sizeof(rmnet_meta_sm_data_format_type));

  VALIDATE_REQUEST_INSTANCE(((qmi_framework_msg_hdr_type*)msg_hdr)->common_hdr.qmi_instance);

  rmnet_instance = QMI_IF_GET_RMNET_INSTANCE(((qmi_framework_msg_hdr_type*)msg_hdr)->common_hdr.qmi_instance,1);

  rmnet_meta_sm_get_data_format(rmnet_instance,
                                &data_format
                               );
  /*-------------------------------------------------------------------------
    Attach QoS format TLV
  -------------------------------------------------------------------------*/
  qos_format = data_format.qos_format;
  if(FALSE == qmi_svc_put_param_tlv(&response,
                                    QMI_TYPE_QOS_HEADER_FORMAT_RESP,
                                    sizeof (uint32),
                                    (void *)&qos_format))
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
  }


  /*-------------------------------------------------------------------------
    DL minimum padding
  -------------------------------------------------------------------------*/
  dl_min_padding = data_format.data_agg_protocol.dl_min_padding;
  if (FALSE == qmi_svc_put_param_tlv(&response,
                                  QMI_TYPE_DL_MIN_PADDING_RESP,
                                  sizeof (uint32),
                                  (void *)&dl_min_padding))
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
  }

  if (data_format.data_agg_protocol.ul_data_agg_max_num != 0)
  {
    /*-------------------------------------------------------------------------
      Attach the UL data aggregation max size TLV response
    -------------------------------------------------------------------------*/
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                    QMI_TYPE_UL_DATA_AGG_MAX_SIZE,
                                    sizeof (uint32),
                                    (void *)&data_format.data_agg_protocol.ul_data_agg_max_size))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
      goto send_result;
    }

    /*-------------------------------------------------------------------------
      Attach the UL data aggregation max datagrams TLV response
    -------------------------------------------------------------------------*/
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                    QMI_TYPE_UL_DATA_AGG_MAX_DATAGRAMS,
                                    sizeof (uint32),
                                    (void *)&data_format.data_agg_protocol.ul_data_agg_max_num))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
      goto send_result;
    }
  }

  /*-------------------------------------------------------------------------
    Attach the DL data aggregation max size TLV response
  -------------------------------------------------------------------------*/
  if(FALSE == qmi_svc_put_param_tlv(&response,
                                    QMI_TYPE_DL_DATA_AGG_MAX_SIZE,
                                    sizeof (uint32),
                                    (void *)&data_format.data_agg_protocol.dl_data_agg_max_size))
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
  }

  /*-------------------------------------------------------------------------
    Attach the DL data aggregation max datagrams TLV response
  -------------------------------------------------------------------------*/
  if(FALSE == qmi_svc_put_param_tlv(&response,
                                    QMI_TYPE_DL_DATA_AGG_MAX_DATAGRAMS,
                                    sizeof (uint32),
                                    (void *)&data_format.data_agg_protocol.dl_data_agg_max_num))
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
  }

  /*-------------------------------------------------------------------------
    Attach the NDP signature TLV response
  -------------------------------------------------------------------------*/
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      QMI_TYPE_NDP_SIGNATURE,
                                      sizeof (uint32),
                                      (void *)&data_format.data_agg_protocol.ndp_signature))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }

  /*-------------------------------------------------------------------------
    Attach the DL data aggregation protocol TLV response
  -------------------------------------------------------------------------*/
  if(FALSE == qmi_svc_put_param_tlv(&response,
                                    QMI_TYPE_DL_DATA_AGG_PROTOCOL,
                                    sizeof (uint32),
                                    (void *)&data_format.data_agg_protocol.dl_data_agg_protocol))
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
  }

  link_prot = data_format.link_prot;
  /*-------------------------------------------------------------------------
    Attach the UL data aggregation protocol TLV response
  -------------------------------------------------------------------------*/
  if(FALSE == qmi_svc_put_param_tlv(&response,
                                    QMI_TYPE_UL_DATA_AGG_PROTOCOL,
                                    sizeof (uint32),
                                    (void *)&data_format.data_agg_protocol.ul_data_agg_protocol))
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
  }

  /*-------------------------------------------------------------------------
    Attach the Link protocol TLV response
  -------------------------------------------------------------------------*/
  if(FALSE == qmi_svc_put_param_tlv(&response,
                                    QMI_TYPE_LINK_PROTOCOL,
                                    sizeof (uint32),
                                    (void *)&link_prot))
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
  }

  /*-------------------------------------------------------------------------
    Attach the QoS TLV response
  -------------------------------------------------------------------------*/
  if(FALSE == qmi_svc_put_param_tlv(&response,
                                    QMI_TYPE_QOS_DATA_FORMAT,
                                    sizeof (uint8),
                                    (void *)&data_format.qos))
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
  }

  /*-------------------------------------------------------------------------
    build response
  -------------------------------------------------------------------------*/
send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS
                                   : QMI_RESULT_FAILURE);

  if( FALSE == qmi_svc_put_result_tlv(&response, result, errval) )
  {
    dsm_free_packet(&response);
    return NULL;
  }

  return response;

} /* qmi_wdai_get_data_format */

/*===========================================================================
  FUNCTION QMI_WDAI_PACKET_FILTER_ENABLE()

  DESCRIPTION
    Handle the QMI WDA packet filter enable message.

    Dispatches a request to the specified service to
    enable packet filter setting.

  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    dsm * pointing to the response to be sent to host
    NULL if no response was generated or command was discarded

  DEPENDENCIES
    qmi_wda_init() must have been called

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_wdai_packet_filter_enable
(
  void *            sp,
  void *            msg_hdr,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *        response;
  uint8                  type;
  uint16                 len;
  uint16                 expected_len;
  void *                 value;
  boolean                restrictive = FALSE;
  boolean                got_v_in_required;
  uint8                  ips_id;
  boolean                got_ips_id = FALSE;
  qmi_error_e_type       errval;
  qmi_result_e_type      result;
  boolean                retval;
  qmi_instance_e_type    qmi_instance;
  uint16                 packet_filter_id;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(msg_hdr && sdu_in);

  qmi_instance = ((qmi_framework_msg_hdr_type*)msg_hdr)->common_hdr.qmi_instance;

  response          = NULL;
  errval            = QMI_ERR_NONE;
  value             = NULL;
  got_v_in_required = FALSE;

  VALIDATE_REQUEST_INSTANCE(((qmi_framework_msg_hdr_type*)msg_hdr)->common_hdr.qmi_instance);

  while (*sdu_in)
  {
    if( !qmi_svc_get_tl( sdu_in, &type, &len ) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        expected_len = sizeof(restrictive);
        value = &restrictive;
        got_v_in_required = TRUE;
        break;

      case QMI_TYPE_IPS_ID_10:
        expected_len = QMI_TYPE_UINT8_LEN;
        value = &ips_id;
        got_ips_id = TRUE;
        break;

      default:
        LOG_MSG_INFO2_1 ("Unrecognized TLV type (%d)", type);
        break;
    }

    if(!qmi_svc_validate_type_length( type,
                                       len,
                                       expected_len,
                                       FILE_ID_DS_QMI_WDA,__LINE__) )
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    if(len != qmi_svc_dsm_pullup( sdu_in, value, len ,FILE_ID_DS_QMI_WDA,__LINE__))
    {
      /*---------------------------------------------------------------------
        Mandatory TLV is invalid so in result send error
      ---------------------------------------------------------------------*/
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  if(!got_v_in_required)
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  LOG_MSG_INFO2_1 ("Got Pkt filter restrictive setting: %d", restrictive);

  if( restrictive != 0 && restrictive != 1 )
  {
    errval = QMI_ERR_INVALID_ARG;
    goto send_result;
  }

  packet_filter_id = got_ips_id ? IPS_ID_TO_FILTER_ID(ips_id) : qmi_instance;

  if (!rmnet_utils_packet_filter_change(packet_filter_id, TRUE, restrictive))
  {
    errval = QMI_ERR_INTERNAL;
  }

send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS
                                   : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();

  return response;

} /* qmi_wdai_packet_filter_enable() */

/*===========================================================================
  FUNCTION QMI_WDAI_PACKET_FILTER_DISABLE()

  DESCRIPTION
    Handle the QMI WDA packet filter disable message.

    Dispatches a request to the specified service to
    disable packet filter setting.

  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    dsm * pointing to the response to be sent to host
    NULL if no response was generated or command was discarded

  DEPENDENCIES
    qmi_wda_init() must have been called

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_wdai_packet_filter_disable
(
  void *            sp,
  void *            msg_hdr,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *        response;
  uint8                  type;
  uint16                 len;
  uint16                 expected_len;
  void *                 value;

  uint8                  ips_id;
  boolean                got_ips_id = FALSE;
  qmi_error_e_type       errval;
  qmi_result_e_type      result;
  boolean                retval;
  qmi_instance_e_type    qmi_instance;
  uint16                 packet_filter_id;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(msg_hdr && sdu_in);

  qmi_instance = ((qmi_framework_msg_hdr_type*)msg_hdr)->common_hdr.qmi_instance;

  response          = NULL;
  errval            = QMI_ERR_NONE;
  value             = NULL;

  VALIDATE_REQUEST_INSTANCE(((qmi_framework_msg_hdr_type*)msg_hdr)->common_hdr.qmi_instance);

  while (*sdu_in)
  {
    if( !qmi_svc_get_tl( sdu_in, &type, &len ) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_IPS_ID_10:
        expected_len = QMI_TYPE_UINT8_LEN;
        value = &ips_id;
        got_ips_id = TRUE;
        break;

      default:
        LOG_MSG_INFO2_1 ("Unrecognized TLV type (%d)", type);
        break;
    }

    if(!qmi_svc_validate_type_length( type,
                                       len,
                                       expected_len,
                                       FILE_ID_DS_QMI_WDA,__LINE__) )
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    if(len != qmi_svc_dsm_pullup( sdu_in, value, len ,FILE_ID_DS_QMI_WDA,__LINE__))
    {
      /*---------------------------------------------------------------------
        Mandatory TLV is invalid so in result send error
      ---------------------------------------------------------------------*/
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  packet_filter_id = got_ips_id ? IPS_ID_TO_FILTER_ID(ips_id) : qmi_instance;

  if (!rmnet_utils_packet_filter_change(packet_filter_id, FALSE, TRUE))
  {
    errval = QMI_ERR_INTERNAL;
  }

send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS
                                   : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();

  return response;
} /* qmi_wdai_packet_filter_disable() */

/*===========================================================================
  FUNCTION QMI_WDAI_PACKET_FILTER_GET_STATE()

  DESCRIPTION
    Handle the QMI WDA packet filter get state message.

    Dispatches a request to the specified service to
    get current packet filter setting.

  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    dsm * pointing to the response to be sent to host
    NULL if no response was generated or command was discarded

  DEPENDENCIES
    qmi_wda_init() must have been called

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_wdai_packet_filter_get_state
(
  void *            sp,
  void *            msg_hdr,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *         response;
  uint8                   type;
  uint16                  len;
  uint16                  expected_len;
  void *                  value;

  uint8                   ips_id;
  boolean                 got_ips_id = FALSE;
  qmi_result_e_type       result;
  qmi_error_e_type        errval;
  boolean                 retval;
  qmi_instance_e_type     qmi_instance;
  uint16                  packet_filter_id;
  boolean                 enabled = FALSE;
  boolean                 restrictive = FALSE;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(msg_hdr && sdu_in);

  qmi_instance = ((qmi_framework_msg_hdr_type*)msg_hdr)->common_hdr.qmi_instance;

  response          = NULL;
  errval            = QMI_ERR_NONE;
  value             = NULL;

  VALIDATE_REQUEST_INSTANCE(((qmi_framework_msg_hdr_type*)msg_hdr)->common_hdr.qmi_instance);

  while (*sdu_in)
  {
    if( !qmi_svc_get_tl( sdu_in, &type, &len ) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_IPS_ID_10:
        expected_len = QMI_TYPE_UINT8_LEN;
        value = &ips_id;
        got_ips_id = TRUE;
        break;

      default:
        LOG_MSG_INFO2_1 ("Unrecognized TLV type (%d)", type);
        break;
    }

    if(!qmi_svc_validate_type_length( type,
                                      len,
                                      expected_len,
                                      FILE_ID_DS_QMI_WDA,__LINE__) )
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    if(len != qmi_svc_dsm_pullup( sdu_in, value, len ,FILE_ID_DS_QMI_WDA,__LINE__))
    {
      /*---------------------------------------------------------------------
        Mandatory TLV is invalid so in result send error
      ---------------------------------------------------------------------*/
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  packet_filter_id = got_ips_id ? IPS_ID_TO_FILTER_ID(ips_id) : qmi_instance;

  if (!rmnet_utils_packet_filter_get_state(packet_filter_id, &enabled, &restrictive))
  {
    dsm_free_packet(&response);
    errval = QMI_ERR_INTERNAL;
  }
  else
  {
    /*-------------------------------------------------------------------------
      Attach the packet filter pkt allowed TLV response
    -------------------------------------------------------------------------*/
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      QMI_TYPE_PACKET_FILTERS_RESTRICTIVE,
                                      sizeof (boolean),
                                      (void *)&restrictive))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  
    /*-------------------------------------------------------------------------
      Attach the packet filter enabled TLV response
    -------------------------------------------------------------------------*/
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        QMI_TYPE_PACKET_FILTERS_ENABLED,
                                        sizeof (boolean),
                                        (void *)&enabled))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
  }

send_result:
  /*-------------------------------------------------------------------------
    build response
  -------------------------------------------------------------------------*/
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS
                                   : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();

  return response;
} /* qmi_wdai_packet_filter_get_state() */

/*===========================================================================
  FUNCTION QMI_WDAI_PACKET_FILTER_ADD_RULE()

  DESCRIPTION
    Handle the QMI WDA packet filter add rule message.

    Dispatches a request to the specified service to
    add one packet filter rule entry.

  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    dsm * pointing to the response to be sent to host
    NULL if no response was generated or command was discarded

  DEPENDENCIES
    qmi_wda_init() must have been called

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_wdai_packet_filter_add_rule
(
  void *            sp,
  void *            msg_hdr,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *        response;
  uint8                  type;
  uint16                 len;
  uint16                 expected_len;
  void *                 value;
  uint32                 handle;

  uint8 *                v_in_required = NULL;
  uint8                  pattern_len;
  uint8 *                pattern;
  uint8                  mask_len;
  uint8 *                mask;
  uint8                  ips_id;
  boolean                got_ips_id = FALSE;

  boolean                got_v_in_required;
  uint16                 v_in_required_len = 0;
  qmi_error_e_type       errval = QMI_ERR_NONE;
  qmi_result_e_type      result;
  boolean                retval;
  qmi_instance_e_type    qmi_instance;
  uint16                 packet_filter_id;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(msg_hdr && sdu_in);

  qmi_instance = ((qmi_framework_msg_hdr_type*)msg_hdr)->common_hdr.qmi_instance;

  response          = NULL;
  errval            = QMI_ERR_NONE;
  value             = NULL;
  got_v_in_required = FALSE;

  VALIDATE_REQUEST_INSTANCE(((qmi_framework_msg_hdr_type*)msg_hdr)->common_hdr.qmi_instance);

  while (*sdu_in)
  {
    if( !qmi_svc_get_tl( sdu_in, &type, &len ) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    mask_len = 0;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        v_in_required_len = len;
        if (len <= (2+2*QMI_IDL_PACKET_FILTER_MAX_SIZE+1))
        {
          v_in_required = modem_mem_alloc(len,MODEM_MEM_CLIENT_DATACOMMON);
          if (v_in_required)
          {
            memset(v_in_required, 0, len);
            expected_len = len;
            value = v_in_required;
            got_v_in_required = TRUE;
          }
          else
          {
            errval = QMI_ERR_NO_MEMORY;
            goto send_result;
          }
        }
        else 
        {
          LOG_MSG_INFO2_2 ("Invalid TLV len (%d) for type (%d)", len, type);
          errval = QMI_ERR_MALFORMED_MSG;
          goto send_result;
        }
        break;

      case QMI_TYPE_IPS_ID_10:
        expected_len = QMI_TYPE_UINT8_LEN;
        value = &ips_id;
        got_ips_id = TRUE;
        break;

      default:
        LOG_MSG_INFO2_1 ("Unrecognized TLV type (%d)", type);
        break;
    }

    if(!qmi_svc_validate_type_length( type,
                                      len,
                                      expected_len,
                                      FILE_ID_DS_QMI_WDA,__LINE__) )
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    if(len != qmi_svc_dsm_pullup( sdu_in, value, len ,FILE_ID_DS_QMI_WDA,__LINE__))
    {
      /*---------------------------------------------------------------------
        Mandatory TLV is invalid so in result send error
      ---------------------------------------------------------------------*/
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  if(!got_v_in_required)
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  pattern_len = *v_in_required;

  if (pattern_len == 0 || (pattern_len > QMI_IDL_PACKET_FILTER_MAX_SIZE) || (pattern_len+1) >= v_in_required_len
      || ((mask_len = *(v_in_required + 1 + pattern_len)) && (pattern_len != mask_len)) 
      || (pattern_len + mask_len + 2) != v_in_required_len)
  {
    LOG_MSG_ERROR_3 ("Got Pkt filter add config wrong size: "
                     " total %d pattern %d mask%d",
                   v_in_required_len, pattern_len, mask_len);
    errval = QMI_ERR_INVALID_ARG;
    goto send_result;
  }

  pattern = v_in_required + 1;
  mask = v_in_required + 1 + pattern_len + 1;

  packet_filter_id = got_ips_id ? IPS_ID_TO_FILTER_ID(ips_id) : qmi_instance;

  if(!rmnet_utils_packet_filter_add_rule(packet_filter_id, &handle, pattern_len, pattern, mask))
  {
    errval = QMI_ERR_INTERNAL; 
  }

  if(errval == QMI_ERR_NONE)
  {
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      QMI_TYPE_PACKET_FILTER_RULE,
                                      2 * (pattern_len + 1),
                                      (void *)v_in_required)
       || FALSE == qmi_svc_put_param_tlv(&response,
                                         QMI_TYPE_PACKET_FILTER_HANDLE,
                                         sizeof (uint32),
                                         (void *)&handle))
    {  
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  } 

send_result:
  if (v_in_required)
  {
    modem_mem_free(v_in_required, MODEM_MEM_CLIENT_DATACOMMON);
  }

  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS
                                   : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();

  return response;

} /* qmi_wdai_packet_filter_add_rule() */

/*===========================================================================
  FUNCTION QMI_WDAI_PACKET_FILTER_DELETE_RULE()

  DESCRIPTION
    Handle the QMI WDA packet filter delete rule message.

    Dispatches a request to the specified service to
    delete all or one packet filter rule entry.

  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    dsm * pointing to the response to be sent to host
    NULL if no response was generated or command was discarded

  DEPENDENCIES
    qmi_wda_init() must have been called

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_wdai_packet_filter_delete_rule
(
  void *            sp,
  void *            msg_hdr,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  uint8                   type;
  uint16                  len;
  uint16                  expected_len;
  void *                  value;
  dsm_item_type *         response;
  qmi_result_e_type       result;
  qmi_error_e_type        errval;
  uint32                  handle = 0;
  uint8                   ips_id;
  boolean                 got_ips_id = FALSE;
  qmi_instance_e_type     qmi_instance;
  uint16                  packet_filter_id;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(msg_hdr && sdu_in);

  qmi_instance = ((qmi_framework_msg_hdr_type*)msg_hdr)->common_hdr.qmi_instance;

  errval = QMI_ERR_NONE;
  response = NULL;

  VALIDATE_REQUEST_INSTANCE(((qmi_framework_msg_hdr_type*)msg_hdr)->common_hdr.qmi_instance);

  /*-------------------------------------------------------------------------
    Parse incoming command
    - identify and save expected TLVs,
      if duplicate TLV found, last present is used.
    - discard unexpected TLVs
  -------------------------------------------------------------------------*/
  while (*sdu_in)
  {
    if( !qmi_svc_get_param_tl( sdu_in, &type, &len ) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_PACKET_FILTER_HANDLE:
        expected_len = sizeof(uint32);
        value = (void *)&handle;
        break;

      case QMI_TYPE_IPS_ID_11:
        expected_len = QMI_TYPE_UINT8_LEN;
        value = &ips_id;
        got_ips_id = TRUE;
        break;

      default:
        LOG_MSG_INFO2_1 ("Unexpected TLV type (%d)", type);
        break;
    }

    if(!qmi_svc_validate_type_length( type,
                                      len,
                                      expected_len,
                                      FILE_ID_DS_QMI_WDA,__LINE__) )
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    if( len != qmi_svc_dsm_pullup( sdu_in, value, len ,FILE_ID_DS_QMI_WDA,__LINE__) )
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  } /* end while */

  packet_filter_id = got_ips_id ? IPS_ID_TO_FILTER_ID(ips_id) : qmi_instance;

  /*-------------------------------------------------------------------------
    Perform validity checks for Packet Filter Handle
  -------------------------------------------------------------------------*/
  if( handle != 0 )
  {
    /* delete one rule using handle */
    if (!rmnet_utils_packet_filter_delete_rule(packet_filter_id, handle))
    {
      errval = QMI_ERR_INTERNAL;
    }
  }
  else
  {
    /* delete all rules with handle set to 0 */
    if (!rmnet_utils_packet_filter_delete_all_rules(packet_filter_id))
    {
      errval = QMI_ERR_INTERNAL;
    }
  }

  /*-------------------------------------------------------------------------
    build response
  -------------------------------------------------------------------------*/
  if(errval == QMI_ERR_NONE && 
     FALSE == qmi_svc_put_param_tlv(&response,
                                    QMI_TYPE_PACKET_FILTER_HANDLE,
                                    sizeof (uint32),
                                    (void *)&handle))
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
  }

send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS
                                   : QMI_RESULT_FAILURE);

  if( FALSE == qmi_svc_put_result_tlv(&response, result, errval) )
  {
    dsm_free_packet(&response);
    return NULL;
  }

  return response;
} /* qmi_wdai_packet_filter_delete_rule() */


/*===========================================================================
  FUNCTION QMI_WDAI_PACKET_FILTER_GET_RULE_HANDLES()

  DESCRIPTION
    Handle the QMI WDA packet filter get rule handles message.

    Dispatches a request to the specified service to
    get list of packet filter rule handles.

  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    dsm * pointing to the response to be sent to host
    NULL if no response was generated or command was discarded

  DEPENDENCIES
    qmi_wda_init() must have been called

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_wdai_packet_filter_get_rule_handles
(
  void *            sp,
  void *            msg_hdr,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *         response;
  uint8                   type;
  uint16                  len;
  uint16                  expected_len;
  void *                  value;
  qmi_result_e_type       result;
  qmi_error_e_type        errval;

  PACKED struct PACKED_POST
  {
    uint8  handle_len;  /**< Must be set to # of elements in handle */
    uint32 handle[QMI_IDL_PACKET_FILTER_MAX_NUM];
  } v_out_required;

  uint8                 ips_id;
  boolean               got_ips_id = FALSE;
  qmi_instance_e_type   qmi_instance;
  uint16                packet_filter_id;
  uint32                temp_handle[QMI_IDL_PACKET_FILTER_MAX_NUM];
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(msg_hdr && sdu_in);

  qmi_instance = ((qmi_framework_msg_hdr_type*)msg_hdr)->common_hdr.qmi_instance;

  LOG_MSG_INFO2_0 ("Got Pkt filter get config list");

  response          = NULL;
  errval            = QMI_ERR_NONE;
  value             = NULL;

  VALIDATE_REQUEST_INSTANCE(((qmi_framework_msg_hdr_type*)msg_hdr)->common_hdr.qmi_instance);

  while (*sdu_in)
  {
    if( !qmi_svc_get_tl( sdu_in, &type, &len ) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_IPS_ID_10:
        expected_len = QMI_TYPE_UINT8_LEN;
        value = &ips_id;
        got_ips_id = TRUE;
        break;

      default:
        LOG_MSG_INFO2_1 ("Unrecognized TLV type (%d)", type);
        break;
    }

    if(!qmi_svc_validate_type_length( type,
                                       len,
                                       expected_len,
                                       FILE_ID_DS_QMI_WDA,__LINE__) )
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    if(len != qmi_svc_dsm_pullup( sdu_in, value, len ,FILE_ID_DS_QMI_WDA,__LINE__))
    {
      /*---------------------------------------------------------------------
        Mandatory TLV is invalid so in result send error
      ---------------------------------------------------------------------*/
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  packet_filter_id = got_ips_id ? IPS_ID_TO_FILTER_ID(ips_id) : qmi_instance;

  if (!rmnet_utils_packet_filter_get_rule_handles(
    packet_filter_id, &v_out_required.handle_len, (uint32 *)temp_handle))
  {
    errval = QMI_ERR_INTERNAL;
  }

  if (errval  == QMI_ERR_NONE)
  {
    memscpy(v_out_required.handle, QMI_IDL_PACKET_FILTER_MAX_NUM, temp_handle, QMI_IDL_PACKET_FILTER_MAX_NUM);
    if (FALSE == qmi_svc_put_param_tlv(
       &response, QMI_TYPE_PACKET_FILTER_HANDLE_LIST,
       1 + v_out_required.handle_len*sizeof(uint32),
       (void *)&v_out_required) )
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }
send_result:
  /*-------------------------------------------------------------------------
    build response
  -------------------------------------------------------------------------*/
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS
                                   : QMI_RESULT_FAILURE);

  if( FALSE == qmi_svc_put_result_tlv(&response, result, errval) )
  {
    dsm_free_packet(&response);
    return NULL;
  }

  return response;
} /* qmi_wdai_packet_filter_get_rule_handles() */

/*===========================================================================
  FUNCTION QMI_WDAI_PACKET_FILTER_GET_RULE()

  DESCRIPTION
    Handle the QMI WDA packet filter get rule message.

    Dispatches a request to the specified service to
    get one packet filter rule entry.

  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    dsm * pointing to the response to be sent to host
    NULL if no response was generated or command was discarded

  DEPENDENCIES
    qmi_wda_init() must have been called

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_wdai_packet_filter_get_rule
(
  void *            sp,
  void *            msg_hdr,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  uint8                   type;
  uint16                  len;
  uint16                  expected_len;
  void *                  value;
  dsm_item_type *         response;
  qmi_result_e_type       result;
  qmi_error_e_type        errval;
  uint32                  handle;
  boolean                 got_v_in_required;

  uint8  v_out_required[QMI_IDL_PACKET_FILTER_MAX_SIZE*2+2];
  uint8 *pattern_len; 
  uint8 *mask_len; 
  uint8                  ips_id;
  boolean                got_ips_id = FALSE;

  qmi_instance_e_type   qmi_instance;
  uint16                packet_filter_id;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(msg_hdr && sdu_in);

  qmi_instance = ((qmi_framework_msg_hdr_type*)msg_hdr)->common_hdr.qmi_instance;

  response          = NULL;
  errval            = QMI_ERR_NONE;
  value             = NULL;
  got_v_in_required = FALSE;

  VALIDATE_REQUEST_INSTANCE(((qmi_framework_msg_hdr_type*)msg_hdr)->common_hdr.qmi_instance);

  while (*sdu_in)
  {
    if( !qmi_svc_get_tl( sdu_in, &type, &len ) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        expected_len = sizeof(handle);
        value = &handle;
        got_v_in_required = TRUE;
        break;

      case QMI_TYPE_IPS_ID_10:
        expected_len = QMI_TYPE_UINT8_LEN;
        value = &ips_id;
        got_ips_id = TRUE;
        break;

      default:
        LOG_MSG_INFO2_1 ("Unrecognized TLV type (%d)", type);
        break;
    }

    if(!qmi_svc_validate_type_length( type,
                                       len,
                                       expected_len,
                                       FILE_ID_DS_QMI_WDA,__LINE__) )
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    if(len != qmi_svc_dsm_pullup( sdu_in, value, len ,FILE_ID_DS_QMI_WDA,__LINE__))
    {
      /*---------------------------------------------------------------------
        Mandatory TLV is invalid so in result send error
      ---------------------------------------------------------------------*/
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  if(!got_v_in_required)
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  LOG_MSG_INFO2_1 ("Got Pkt filter get config handle: %d", handle);

  if (handle == 0)
  {
    errval = QMI_ERR_INVALID_ARG;
    goto send_result;
  }

  packet_filter_id = got_ips_id ? IPS_ID_TO_FILTER_ID(ips_id) : qmi_instance;

  if (!rmnet_utils_packet_filter_get_rule(packet_filter_id, handle, v_out_required))
  {
    errval = QMI_ERR_INVALID_HANDLE;
  }
  else
  {
    pattern_len = v_out_required;
    mask_len = v_out_required + 1 + *pattern_len;

    if (*pattern_len > QMI_IDL_PACKET_FILTER_MAX_SIZE || 
        *pattern_len !=*mask_len)
    {
      errval = QMI_ERR_INTERNAL;
    }
    else if (FALSE == qmi_svc_put_param_tlv(&response, 
              QMI_TYPE_PACKET_FILTER_HANDLE, sizeof(handle),
              (void *)&handle)  ||
             FALSE == qmi_svc_put_param_tlv(&response,
              QMI_TYPE_PACKET_FILTER_RULE,
              2 + 2*(*pattern_len),
              (void *)&v_out_required))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  /*-------------------------------------------------------------------------
    build response
  -------------------------------------------------------------------------*/
send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS
                                   : QMI_RESULT_FAILURE);

  if( FALSE == qmi_svc_put_result_tlv(&response, result, errval) )
  {
    dsm_free_packet(&response);
    return NULL;
  }

  return response;
} /* qmi_wdai_packet_filter_get_rule() */

/*===========================================================================
  FUNCTION QMI_WDAI_SET_LOOPBACK_STATE()

  DESCRIPTION
    Handle the QMI WDA set loopback state message.

    Dispatches a request to the specified service to
    set loopback state.

  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    dsm * pointing to the response to be sent to host
    NULL if no response was generated or command was discarded

  DEPENDENCIES
    qmi_wda_init() must have been called

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_wdai_set_loopback_state
(
  void *            sp,
  void *            msg_hdr,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *        response;
  uint8                  type;
  uint16                 len;
  uint16                 expected_len;
  void *                 value;
  boolean                enabled = FALSE;
  boolean                got_v_in_required;
  qmi_error_e_type       errval;
  qmi_result_e_type      result;
  boolean                retval;
  int16                  ps_errno  = 0;
  uint16                 ps_result;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(msg_hdr && sdu_in);

  response          = NULL;
  errval            = QMI_ERR_NONE;
  value             = NULL;
  got_v_in_required = FALSE;

  while (*sdu_in)
  {
    if( !qmi_svc_get_tl( sdu_in, &type, &len ) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        expected_len = sizeof(boolean);
        value = &enabled;
        got_v_in_required = TRUE;
        break;

      default:
        LOG_MSG_INFO2_1 ("Unrecognized TLV type (%d)", type);
        break;
    }

    if(!qmi_svc_validate_type_length( type,
                                       len,
                                       expected_len,
                                       FILE_ID_DS_QMI_WDA,__LINE__) )
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    if(len != qmi_svc_dsm_pullup( sdu_in, value, len ,FILE_ID_DS_QMI_WDA,__LINE__))
    {
      /*---------------------------------------------------------------------
        Mandatory TLV is invalid so in result send error
      ---------------------------------------------------------------------*/
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  if(!got_v_in_required)
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  if( enabled != 0 && enabled != 1 )
  {
    errval = QMI_ERR_INVALID_ARG;
    goto send_result;
  }

  ps_result = ps_sys_conf_set(PS_SYS_TECH_ALL, PS_SYS_CONF_ENABLE_LOOPBACK, 
                              (void *)&enabled, &ps_errno);

  if (ps_result)
  {
    LOG_MSG_ERROR_1("PS sys config failed:%d", ps_errno);
    errval = QMI_ERR_INTERNAL;
  } 

send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS
                                   : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();

  return response;

} /* qmi_wdai_set_loopback_state() */

/*===========================================================================
  FUNCTION QMI_WDAI_GET_LOOPBACK_STATE()

  DESCRIPTION
    Handle the QMI WDA get loopback state message.

    Dispatches a request to the specified service to
    get current lookback state.

  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    dsm * pointing to the response to be sent to host
    NULL if no response was generated or command was discarded

  DEPENDENCIES
    qmi_wda_init() must have been called

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_wdai_get_loopback_state
(
  void *            sp,
  void *            msg_hdr,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *                  response;
  qmi_result_e_type                result;
  qmi_error_e_type                 errval;
  ps_sys_conf_enable_loopback_type loopback_enabled = FALSE;
  int16                            ps_errno  = 0;
  uint16                           ps_result;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(msg_hdr && sdu_in);

  response = NULL;
  errval = QMI_ERR_NONE;

  ps_result = ps_sys_conf_get(PS_SYS_TECH_ALL, PS_SYS_CONF_ENABLE_LOOPBACK, 
                           (void *)&loopback_enabled, &ps_errno);

  if (ps_result)
  {
    LOG_MSG_ERROR_1("PS sys config failed:%d", ps_errno);
    dsm_free_packet(&response);
    errval = QMI_ERR_INTERNAL;
  }
  else 
  {
    /*-------------------------------------------------------------------------
      Attach the loopback state TLV response
    -------------------------------------------------------------------------*/
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        QMI_TYPE_LOOPBACK_STATE,
                                        sizeof (ps_sys_conf_enable_loopback_type),
                                        (void *)&loopback_enabled))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
  }

  /*-------------------------------------------------------------------------
    build response
  -------------------------------------------------------------------------*/
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS
                                   : QMI_RESULT_FAILURE);

  if( FALSE == qmi_svc_put_result_tlv(&response, result, errval) )
  {
    dsm_free_packet(&response);
    return NULL;
  }

  return response;
} /* qmi_wdai_get_loopback_state() */

/*===========================================================================
  FUNCTION QMI_WDAI_SET_QMAP_SETTINGS()

  DESCRIPTION
    Handle the QMI WDA set qmap settings message.

    Dispatches a request to the specified service to
    set qmap settings.

  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    dsm * pointing to the response to be sent to host
    NULL if no response was generated or command was discarded

  DEPENDENCIES
    qmi_wda_init() must have been called

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_wdai_set_qmap_settings
(
  void *            sp,
  void *            msg_hdr,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *        response;
  uint8                  type;
  uint16                 len;
  uint16                 expected_len;
  void *                 value;
  qmi_error_e_type       errval;
  qmi_result_e_type      result;
  boolean                retval;

  rmnet_instance_e_type        rmnet_inst = RMNET_INSTANCE_MIN;
  rmnet_instance_e_type        rmnet_inst_v6 = RMNET_INSTANCE_MIN;
  ds_rmnet_qmap_settings_type  qmap_settings;
  boolean                      in_band_fc = FALSE;
  boolean                      got_v_in_in_band_fc = FALSE;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(msg_hdr && sdu_in);

  memset(&qmap_settings, 0, sizeof(qmap_settings));
  response          = NULL;
  errval            = QMI_ERR_NONE;
  value             = NULL;

  VALIDATE_REQUEST_INSTANCE(((qmi_framework_msg_hdr_type*)msg_hdr)->common_hdr.qmi_instance);

  rmnet_inst = QMI_IF_GET_RMNET_INSTANCE(
    ((qmi_framework_msg_hdr_type*)msg_hdr)->common_hdr.qmi_instance,1);

  rmnet_inst_v6 = QMI_IF_GET_RMNET_INSTANCE(
    ((qmi_framework_msg_hdr_type*)msg_hdr)->common_hdr.qmi_instance,0);

  if ( rmnet_meta_sm_in_call(rmnet_inst) ||
       rmnet_meta_sm_in_call(rmnet_inst_v6) )
  {
    errval = QMI_ERR_NO_EFFECT;
    goto send_result;
  }

  if ( !ds_rmnet_get_qmap_settings(rmnet_inst, &qmap_settings) )
  {
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  while (*sdu_in)
  {
    if( !qmi_svc_get_tl( sdu_in, &type, &len ) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_IN_BAND_FLOW_CONTROL:
        expected_len = sizeof(boolean);
        value = &in_band_fc;
        got_v_in_in_band_fc = TRUE;
        break;

      default:
        LOG_MSG_INFO2_1 ("Unrecognized TLV type (%d)", type);
        break;
    }

    if(!qmi_svc_validate_type_length( type,
                                       len,
                                       expected_len,
                                       FILE_ID_DS_QMI_WDA,__LINE__)  )
    {
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    if(len != qmi_svc_dsm_pullup( sdu_in, value, len ,FILE_ID_DS_QMI_WDA,__LINE__))
    {
      /*---------------------------------------------------------------------
        Mandatory TLV is invalid so in result send error
      ---------------------------------------------------------------------*/
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  if (got_v_in_in_band_fc)
  {
    qmap_settings.in_band_fc = in_band_fc ? TRUE : FALSE;
  }

  if ( !ds_rmnet_set_qmap_settings(rmnet_inst, &qmap_settings) )
  {
    errval = QMI_ERR_NOT_SUPPORTED;
    goto send_result;
  }

  /*---------------------------------------------------------------------
    Send response
  ---------------------------------------------------------------------*/
  memset(&qmap_settings, 0, sizeof(qmap_settings));
  if ( !ds_rmnet_get_qmap_settings(rmnet_inst, &qmap_settings) )
  {
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  in_band_fc = qmap_settings.in_band_fc ? 1 : 0;
  if (FALSE == qmi_svc_put_param_tlv(&response,
                                     QMI_TYPE_IN_BAND_FLOW_CONTROL,
                                     sizeof (boolean),
                                     (void *)&in_band_fc))
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
    goto send_result;
  }

send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS
                                   : QMI_RESULT_FAILURE);
  if(errval != QMI_ERR_NONE)
  {
    LOG_MSG_ERROR("error code = %d rmnet inst %d or %d in data call",
  	                errval,rmnet_inst, rmnet_inst_v6);
  }
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();

  return response;

} /* qmi_wdai_set_qmap_settings() */

/*===========================================================================
  FUNCTION QMI_WDAI_GET_QMAP_SETTINGS()

  DESCRIPTION
    Handle the QMI WDA get qmap settings message.

    Dispatches a request to the specified service to
    get current qmap settings.

  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    dsm * pointing to the response to be sent to host
    NULL if no response was generated or command was discarded

  DEPENDENCIES
    qmi_wda_init() must have been called

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_wdai_get_qmap_settings
(
  void *            sp,
  void *            msg_hdr,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *                  response;
  qmi_result_e_type                result;
  qmi_error_e_type                 errval;
  boolean                          retval;

  rmnet_instance_e_type            rmnet_inst;
  ds_rmnet_qmap_settings_type      qmap_settings;
  boolean                          in_band_fc = FALSE;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(msg_hdr && sdu_in);

  memset(&qmap_settings, 0, sizeof(qmap_settings));
  response = NULL;
  errval = QMI_ERR_NONE;

  VALIDATE_REQUEST_INSTANCE(((qmi_framework_msg_hdr_type*)msg_hdr)->common_hdr.qmi_instance);

  rmnet_inst = QMI_IF_GET_RMNET_INSTANCE(
    ((qmi_framework_msg_hdr_type*)msg_hdr)->common_hdr.qmi_instance,1);

  if ( !ds_rmnet_get_qmap_settings(rmnet_inst, &qmap_settings) )
  {
    LOG_MSG_ERROR_1("Failed to get QMAP settings for rmnet inst %",
                    rmnet_inst);
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  in_band_fc = qmap_settings.in_band_fc ? 1 : 0;

  /*-------------------------------------------------------------------------
    In-band flow control TLV
  -------------------------------------------------------------------------*/
  if (FALSE == qmi_svc_put_param_tlv(&response,
                                     QMI_TYPE_IN_BAND_FLOW_CONTROL,
                                     sizeof (boolean),
                                     (void *)&in_band_fc))
  {
    errval = QMI_ERR_NO_MEMORY;
    dsm_free_packet(&response);
    goto send_result;
  }

send_result:
  /*-------------------------------------------------------------------------
    build response
  -------------------------------------------------------------------------*/
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS
                                   : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();

  return response;
} /* qmi_wdai_get_qmap_settings() */

