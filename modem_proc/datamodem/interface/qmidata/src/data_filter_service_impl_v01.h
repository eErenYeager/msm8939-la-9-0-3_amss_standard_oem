#ifndef DATA_FILTER_SERVICE_V01IMPL_H
#define DATA_FILTER_SERVICE_V01IMPL_H
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

              D A T A _ F I L T E R _ S E R V I C E _ I M P L _ V 0 1  . h

GENERAL DESCRIPTION
  This is the file which defines the dfs service Data structures.

  Copyright (c) 2013 Qualcomm Technologies, Inc.
  All rights reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.


  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/qmidata/src/data_filter_service_impl_v01.h#1 $
 *====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/* This file was generated with Tool version 6.2
   It was generated on: Wed Sep 18 2013 (Spin 0)
   From IDL File: data_filter_service_v01.idl */

#include "qmi_si.h"
#ifdef __cplusplus
extern "C" {
#endif
const qmi_implemented_messages *dfs_get_service_impl_v01 (void);


#ifdef __cplusplus
}
#endif
#endif

