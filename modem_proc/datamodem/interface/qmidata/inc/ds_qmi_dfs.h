#ifndef _DS_QMI_DFS_H
#define _DS_QMI_DFS_H
/*===========================================================================

                         D S _ Q M I _ D F S . H

DESCRIPTION

 The Data Services QMI Data Filter Service header file.

EXTERNALIZED FUNCTIONS

   qmi_dfs_init()
     Initialize the QMI-DFS service and register with QCSI

Copyright (c) 2013 QUALCOMM Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary
===========================================================================*/
/*===========================================================================

                      EDIT HISTORY FOR FILE

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/qmidata/inc/ds_qmi_dfs.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when        who    what, where, why
--------    ---    ----------------------------------------------------------
09/23/13    bh     Created module
===========================================================================*/

#include "comdef.h"
#include "customer.h"

/*===========================================================================
  FUNCTION QMI_DFS_INIT()

  DESCRIPTION
    Initialize the QMI-DFS service and register with QCSI

  PARAMETERS
    None

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
extern void  qmi_dfs_init
(
  void
);

#endif /* _DS_QMI_DFS_H */
