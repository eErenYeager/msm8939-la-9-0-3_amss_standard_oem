/*===========================================================================

                     P S _ D L _ O P T I M I Z E D _ H D L R . C
DESCRIPTION


EXTERNALIZED FUNCTIONS


Copyright (c) 2011-2014 QUALCOMM Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary
===========================================================================*/
/*===========================================================================

                            EDIT HISTORY FOR FILE
  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/rmifacectls/src/ps_dl_optimized_hdlr.c#1 $

when        who    what, where, why
--------    ---    ---------------------------------------------------------- 
03/18/14    rk     Removed feature mobileap.
07/26/13    cx     Change the ps_icmp6_get_dest_mac_addr arguments due to memcpy replacement
07/25/13    ms     Route packet to next iface for IPsec iface
07/15/13    mp     Deprecating memcpy and memmove.
05/30/13    wc     Non-MBIM packet filtering
01/29/13    vb     Fix to handle the GRE packet processing in
                   NAT optimized path.
12/20/12    at     Fix to make the IPv6 fragments go through legacy path
11/27/12    at     RNDIS DL path and CS fixes.
08/21/12    tw	   Deny ports that are reserved for RSVP or BIP
07/11/12    pgm    Added RNDIS Agg mode support.
07/05/12    cp     Fix to get the opt_filter_ptr from NAT iface in
                   MobileAP call.
05/07/12    mp     Fix to send TCP Reset for exhausted NAT entry.
03/25/12    sb     Added support for TCP_UDP protocol in optimized filters.
03/22/12    pgm    Check for valid um base iface ptr in ps_dl_phys_opt_rx_pkt.
02/29/12    am     Added Mobile AP V6 support.
02/29/12    am     Ship all packets to A5 if USB is not up.
02/13/12    pgm    Compiler warning fixes.
02/13/12    sb     Added iface statistics in optimized path
02/06/12    sb     Added router fragmentation support for optimized path
02/01/12    am     Added range checks for optimized filters.
02/01/12    cy     Added packet filter support
01/09/12    pgm    KW error and Compiler warning fixes
11/15/11    pgm    Fixed Bug for fragmented pkts in ps_dl_phys_opt_rx_pkt
11/11/11    pgm    Disabled MBIM header addition as it is in done in A2.
11/10/11    am     ICMP6 packets also need to be sent up.
11/01/11    am     Added NAT support.
09/22/11    am     Created module.
===========================================================================*/

/*===========================================================================

                          INCLUDE FILES FOR MODULE

===========================================================================*/

#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"

#include "ps_dl_optimized_hdlr.h"

#include "amssassert.h"
#include "msg.h"
#include "err.h"

#include "ps_dl_optimized_hdlri.h"
#include "ps_optimized_path_defs.h"
#include "ps_ifacei.h"
#include "ps_phys_linki.h"
#include "ps_crit_sect.h"
#include "ps_logging.h"
#include "ps_meta_info.h"
#include "ps_ifacei_utils.h"
#include "ps_arp.h"
#include "ds_rmnet_sio.h"
#include "ds_rmnet_smi.h"
#include "ps_lan_llci.h"
#include "ds_rmnet_utils.h"

#include <stringl/stringl.h>

#include "ps_in.h"
#include "ps_icmp6_nd.h"

#ifdef FEATURE_DATA_A2
#include "a2_common.h"
#endif /* FEATURE_DATA_A2 */

#define PS_OPTIMIZED_IP_DL_V4_FRAG_CHECK(val)                              \
    (( (val) & ps_htons(PS_OPT_PATH_V4_FRAG_MASK) ) != 0)

#define PS_DL_OPT_RESULT_SUCCESS       (0)
#define PS_DL_OPT_RESULT_FAILURE       (-1)
#define PS_DL_OPT_RESULT_DROP          (1)
#define PS_DL_OPT_RESULT_PKT_CONSUMED  (2)

#define PS_DL_OPT_MBIM_ALIGN_MASK      (0x0007)
#define PS_DL_OPT_MBIM_ALIGN_MAX       (0x0008)

#define PS_OPTIMIZED_IP_DL_V6_FRAG_CHECK(x) ((x) == (uint8)PS_IPPROTO_FRAG_HDR)

/*---------------------------------------------------------------------------
  LLE control block arrays
 --------------------------------------------------------------------------*/
extern lan_lle_cb_type  lle_array[LAN_LLE_MAX];

extern ps_dl_opt_filter_spec_type *global_dl_opt_fltr_arr;

int dl_aggr_align_enabled = 0;

#ifdef TEST_FRAMEWORK
#error code not present
#endif

/*===========================================================================

                        INTERNAL FUNCTION DEFINITIONS

===========================================================================*/
INLINE boolean ps_dl_opti_process_filters
(
  ps_dl_opt_filter_spec_type * filter_ptr,
  uint16 src_port,
  uint16 dst_port,
  uint16 protocol,
  uint16 max_filter_entries
)
{
  uint16                   fltr_dst_port_host;
  uint16                   fltr_src_port_host;
  uint16                   pkt_dst_port_host;
  uint16                   pkt_src_port_host;
  uint8                    filter_cnt;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if ( filter_ptr != NULL )
  {
    pkt_dst_port_host = ps_ntohs(dst_port);
    pkt_src_port_host = ps_ntohs(src_port);
    for (filter_cnt = 0;
         filter_cnt < max_filter_entries &&
           filter_ptr->fi_handle != 0;
         filter_cnt++, filter_ptr++)
    {
      fltr_dst_port_host = ps_ntohs(filter_ptr->dst_port);
      fltr_src_port_host = ps_ntohs(filter_ptr->src_port);
      if ( ((protocol == filter_ptr->protocol) || 
            (filter_ptr->protocol == PS_IPPROTO_TCP_UDP && 
             (protocol == PS_IPPROTO_TCP || protocol == PS_IPPROTO_UDP)))&&
           ((pkt_dst_port_host >= fltr_dst_port_host &&
             pkt_dst_port_host <= fltr_dst_port_host + filter_ptr->dst_port_range) ||
            (pkt_src_port_host >= fltr_src_port_host &&
             pkt_src_port_host <= fltr_src_port_host + filter_ptr->src_port_range)))
      {
        LOG_MSG_INFO1("ps_dl_opti_process_filters: "
                      "Rx filter pass protocol (%d) dst port (%d) src port (%d)",
                       protocol, pkt_dst_port_host, pkt_src_port_host);
        return TRUE;
      }
    }
  }
  return FALSE;
}

#ifdef FEATURE_DATA_PS_MBIM_SUPPORT
INLINE boolean ps_dl_opti_add_mbim_hdr
(
  rmnet_smi_info_type   *rmnet_info,
  dsm_item_type        **pkt_ptr,
  uint16                 total_len
)
{
  rmnet_ntb_hdr_type  *ntbh = rmnet_info->meta_sm.ntb_header;
  dsm_item_type      **aggr_ptr = &rmnet_info->meta_sm.aggr_pkt;
  dsm_item_type       *tmp_pkt;
  uint32               are_more_packets;
  static uint8         align_pad[7];
  static uint8         aggr_count;
  uint16               align_cnt;
  uint16               total_len_next;
  boolean              matched = FALSE;      
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (total_len)
  {
    if (*pkt_ptr == NULL)
    {
      LOG_MSG_ERROR_0("*pkt_ptr is NULL");
      dsm_free_packet(pkt_ptr);
      return FALSE;
    }

    // packet filtering
    if ( (rmnet_info->meta_sm.packet_filters != NULL) && 
         (rmnet_info->meta_sm.packet_filters->enabled) )
    {
      matched = rmnet_utils_packet_filter_match(
        &(rmnet_info->meta_sm.packet_filters->filter_list), pkt_ptr);

      LOG_MSG_INFO4_3("pkt filter enabled(%d), restrictive(%d), matche(%d)",
                      rmnet_info->meta_sm.packet_filters->enabled,
                      rmnet_info->meta_sm.packet_filters->restrictive,
                      matched);

      if (rmnet_info->meta_sm.packet_filters->restrictive != matched)
      {
        LOG_MSG_ERROR_1("Filtering packets not allowed! dropping size(%d)",
                        dsm_length_packet(*pkt_ptr) );
        dsm_free_packet(pkt_ptr);
        return FALSE;
      }
    }

    are_more_packets = (*pkt_ptr)->app_field;
    align_cnt = total_len & PS_DL_OPT_MBIM_ALIGN_MASK;
    if (align_cnt && dl_aggr_align_enabled)
    {
      align_cnt = PS_DL_OPT_MBIM_ALIGN_MAX - align_cnt;
      /* zero padding */
      tmp_pkt = *pkt_ptr;
      while (tmp_pkt->pkt_ptr)
      {
        tmp_pkt = tmp_pkt->pkt_ptr;
      }

      if (tmp_pkt->used <= (tmp_pkt->size - align_cnt))
      {
        tmp_pkt->used += align_cnt;
      }
      else
      {
        if (align_cnt != dsm_pushdown_tail(pkt_ptr, align_pad, align_cnt,
                                           DSM_DS_SMALL_ITEM_POOL))
        {
          LOG_MSG_ERROR_1("No mem, couldnt pad (%d) bytes, drop", align_cnt);
          dsm_free_packet(pkt_ptr);
          return FALSE;
        }
      }
    }
    else
    {
      align_cnt = 0;
    }

    ntbh->ipd[aggr_count].index = ntbh->nthc.mbim_block_len;
    ntbh->ipd[aggr_count].len = total_len;
    aggr_count++;
    ntbh->nthc.mbim_block_len += total_len + align_cnt;
    ntbh->ndpc.len += sizeof(ps_iface_mbim_ntb_ndp_ip_datagram_info);

    (*pkt_ptr)->app_field = 0;
    dsm_append(&rmnet_info->meta_sm.aggr_pkt, pkt_ptr);
    aggr_ptr = &rmnet_info->meta_sm.aggr_pkt;

    /* Calculate total len plus MTU of next packet */
    total_len_next = ntbh->nthc.mbim_block_len + 
      MIN(PS_IFACE_GET_MTU(rmnet_info->rmnet_iface_ptr),
          PS_IFACE_GET_MTU(rmnet_info->um_iface_ptr));

    /* Check if reaches the maximum num limit and if there's
       room for next packet */
    if ( are_more_packets != PS_DL_OPT_AGGR_MORE_PACKETS ||
         total_len_next > 
           rmnet_info->meta_sm.data_agg_protocol.dl_data_agg_max_size ||
         aggr_count == 
           rmnet_info->meta_sm.data_agg_protocol.dl_data_agg_max_num )
    {
      /* Send out the MBIM block */
      ntbh->nthc.seq_num++;
      /* Terminating NDP block */
      ntbh->ndpc.len += sizeof(ps_iface_mbim_ntb_ndp_ip_datagram_info);

      if(!qmi_svc_dsm_pushdown_packed(aggr_ptr,
                                ntbh,
                                sizeof(rmnet_ntb_hdr_type),
                                DSM_DS_SMALL_ITEM_POOL,
                                FILE_ID_PS_DL_OPTIMIZED_HDLR,__LINE__) )
      {
        dsm_free_packet(aggr_ptr);
        return FALSE;
      }

      /* Reset */
      memset(ntbh->ipd, 0, sizeof(ntbh->ipd));
      ntbh->nthc.mbim_block_len = sizeof(rmnet_ntb_hdr_type);
      ntbh->ndpc.len = sizeof(ps_iface_mbim_ntb_ndp_hdr);
      aggr_count = 0;
      *pkt_ptr = *aggr_ptr;
      return TRUE;
    }
  }
  else if (*aggr_ptr != NULL)
  {
    /* Send out the MBIM block */
    ntbh->nthc.seq_num++;
    /* Terminating NDP block */
    ntbh->ndpc.len += sizeof(ps_iface_mbim_ntb_ndp_ip_datagram_info);

    if( !qmi_svc_dsm_pushdown_packed(aggr_ptr,
                              ntbh,
                              sizeof(rmnet_ntb_hdr_type),
                              DSM_DS_SMALL_ITEM_POOL,
                              FILE_ID_PS_DL_OPTIMIZED_HDLR,__LINE__) )
    {
      dsm_free_packet(aggr_ptr);
      return FALSE;
    }

    /* Reset */
    memset(ntbh->ipd, 0, sizeof(ntbh->ipd));
    ntbh->nthc.mbim_block_len = sizeof(rmnet_ntb_hdr_type);
    ntbh->ndpc.len = sizeof(ps_iface_mbim_ntb_ndp_hdr);
    aggr_count = 0;
    *pkt_ptr = *aggr_ptr;
    return TRUE;
  }

  return FALSE;
} /* ps_dl_opti_add_mbim_hdr() */
#endif /*FEATURE_DATA_PS_MBIM_SUPPORT*/

INLINE int ps_dl_opti_rm_eth_proc
(
  dsm_item_type        **pkt_ref_ptr,
  rmnet_smi_info_type  **rmnet_info,
  ps_iface_type         *rm_iface_ptr,
  ps_ip_addr_type       *te_ip_addr,
  uint8                 *pkt_buf,
  lan_lle_enum_type      lle_instance,
  uint16                 ethertype
)
{
  lan_llc_start_info_type *ptr_start_info;
  uint32                   pkt_app_field;
  uint16                   ret_val;
  uint32                   pkt_len;
  static uint16            rndis_eth_hdr_len =
                           sizeof(rmnet_rndis_eth_hdr_type);
  static uint16            eth_hdr_len =
                           sizeof(llc_frm_packed_mode_zero_type);
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ptr_start_info = &(&lle_array[lle_instance])->start_info;
  *rmnet_info = (rmnet_smi_info_type *)ptr_start_info->tx_f_user_data;
   if (!(*rmnet_info))
   {
      LOG_MSG_ERROR_0("NULL Rm info ptr ");
      return PS_DL_OPT_RESULT_DROP;
   }
  /*-------------------------------------------------------------------------
    RNDIS mode
  -------------------------------------------------------------------------*/
  if ((*rmnet_info)->meta_sm.data_agg_protocol.dl_data_agg_protocol ==
         RMNET_ENABLE_DATA_AGG_RNDIS)
  {


    if ((*rmnet_info)->meta_sm.eth_arped == FALSE)
    {
      /*---------------------------------------------------------------------
        Fill IP addr
      ---------------------------------------------------------------------*/
      if (te_ip_addr->type == IPV4_ADDR)
      {
        te_ip_addr->addr.v4.ps_s_addr = (uint32)*(uint32*)(pkt_buf +
                                        PS_OPT_PATH_V4_DEST_ADDR_OFFSET);
        /*---------------------------------------------------------------------
        Fill Dest MAC addr if ARPed
        ---------------------------------------------------------------------*/
        if (-1 == arp_get_dest_mac_addr(
                 (*rmnet_info)->constants.lan_llc_instance,
                 te_ip_addr,
                 (*rmnet_info)->meta_sm.rndis_eth_hdr->eth_hdr.dst_hw_addr,
                 LAN_IEEE_MAC_ADDR_LEN))
        {
          LOG_MSG_ERROR_0("Failed to get TE MAC");
          /* ...cant do much either */
          return PS_DL_OPT_RESULT_FAILURE;
        }

        

      }
      else
      {
        memscpy
        (
          te_ip_addr->addr.v6.ps_s6_addr64,
          sizeof(uint64),
          pkt_buf + PS_OPT_PATH_V6_DEST_ADDR_OFFSET,
          sizeof(uint64)
        );
        memscpy
        (
          te_ip_addr->addr.v6.ps_s6_addr64 + 1,
          sizeof(uint64),
          pkt_buf + PS_OPT_PATH_V6_DEST_ADDR_OFFSET + 8,
          sizeof(uint64)
        );

      /*---------------------------------------------------------------------
        Fill Dest MAC addr if ARPed
      ---------------------------------------------------------------------*/

        if (-1 == ps_icmp6_get_dest_mac_addr(
                 rm_iface_ptr,
                 &(te_ip_addr->addr.v6),
                 (*rmnet_info)->meta_sm.rndis_eth_hdr->eth_hdr.dst_hw_addr,
                 LAN_IEEE_MAC_ADDR_LEN))
        {
          LOG_MSG_ERROR_0("Failed to get TE MAC");
          /* ...cant do much either */
          return PS_DL_OPT_RESULT_FAILURE;
        }
      }
      (*rmnet_info)->meta_sm.eth_arped = TRUE;

      /*----------------------------------------------------------------- 
       Fill src MAC addr if ARPed
      -------------------------------------------------------------------*/

      memscpy((*rmnet_info)->meta_sm.rndis_eth_hdr->eth_hdr.src_hw_addr,
               LAN_IEEE_MAC_ADDR_LEN,
               rm_iface_ptr->iface_private.hw_addr,
               LAN_IEEE_MAC_ADDR_LEN);

    }/*If not arped*/

    /*-----------------------------------------------------------------------
       Fill Ethertype
    -----------------------------------------------------------------------*/
    (*rmnet_info)->meta_sm.rndis_eth_hdr->eth_hdr.ethertype = ethertype;

    /*----------------------------------------------------------------------
      Construct RNDIS hdr.
    -----------------------------------------------------------------------*/

    pkt_len = dsm_length_packet(*pkt_ref_ptr);
    (*rmnet_info)->meta_sm.rndis_eth_hdr->rndis_hdr.msg_len =
      rndis_eth_hdr_len + pkt_len;
    (*rmnet_info)->meta_sm.rndis_eth_hdr->rndis_hdr.data_len =
      pkt_len + eth_hdr_len;

    /*-------------------------------------------------------------------
      Pushdown hdr
    -------------------------------------------------------------------*/
    ret_val = dsm_pushdown( pkt_ref_ptr, (*rmnet_info)->meta_sm.rndis_eth_hdr,
                            rndis_eth_hdr_len,
                            DSM_DS_SMALL_ITEM_POOL);
    if (rndis_eth_hdr_len != ret_val)
    {
       LOG_MSG_ERROR_1("RNDIS Eth hdr pushdown fail (%d)", ret_val);
       return PS_DL_OPT_RESULT_DROP;
    }

  } /*RNDIS mode*/

   /*------------------------------------------------------------------------
     Non-RNDIS mode
   ------------------------------------------------------------------------*/
  else
  {

  if ((*rmnet_info)->meta_sm.eth_arped == FALSE)
  {
      /*---------------------------------------------------------------------
        Fill IP addr
      ---------------------------------------------------------------------*/
    if (te_ip_addr->type == IPV4_ADDR)
    {
      te_ip_addr->addr.v4.ps_s_addr = (uint32)*(uint32*)(pkt_buf +
                                       PS_OPT_PATH_V4_DEST_ADDR_OFFSET);

      /*---------------------------------------------------------------------
       Fill Dest MAC addr if ARPed
      ---------------------------------------------------------------------*/
      if (-1 == arp_get_dest_mac_addr(
               (*rmnet_info)->constants.lan_llc_instance,
               te_ip_addr,
               (*rmnet_info)->meta_sm.eth_header->dst_hw_addr,
               LAN_IEEE_MAC_ADDR_LEN))
      {
        LOG_MSG_ERROR_0("Failed to get TE MAC");
        /* ...cant do much either */
        return PS_DL_OPT_RESULT_FAILURE;
      }


    }
    else
    {
      memscpy
      (
        te_ip_addr->addr.v6.ps_s6_addr64,
        sizeof(uint64),
        pkt_buf + PS_OPT_PATH_V6_DEST_ADDR_OFFSET,
        sizeof(uint64)
      );

      memscpy
      (
        te_ip_addr->addr.v6.ps_s6_addr64 + 1,
        sizeof(uint64),
        pkt_buf + PS_OPT_PATH_V6_DEST_ADDR_OFFSET + 8,
        sizeof(uint64)
      );

      /*---------------------------------------------------------------------
        Fill Dest MAC addr if ARPed
      ---------------------------------------------------------------------*/

      if (-1 == ps_icmp6_get_dest_mac_addr(
                rm_iface_ptr,
                &(te_ip_addr->addr.v6),
                (*rmnet_info)->meta_sm.eth_header->dst_hw_addr,
                LAN_IEEE_MAC_ADDR_LEN
                ))
      {
        LOG_MSG_ERROR_0("Failed to get TE MAC");
        /* ...cant do much either */
        return PS_DL_OPT_RESULT_FAILURE;
      }

    }
    (*rmnet_info)->meta_sm.eth_arped = TRUE;

    /*----------------------------------------------------------------- 
       Fill src MAC addr if ARPed
    -------------------------------------------------------------------*/

     memscpy((*rmnet_info)->meta_sm.eth_header->src_hw_addr,
              LAN_IEEE_MAC_ADDR_LEN,
             rm_iface_ptr->iface_private.hw_addr,
             LAN_IEEE_MAC_ADDR_LEN);
      
       
    }//if not ARPed

     /*-------------------------------------------------------------------
        Fill Ethertype
     -------------------------------------------------------------------*/
    (*rmnet_info)->meta_sm.eth_header->ethertype = ethertype;

  //Save/restore app field

  pkt_app_field = (*pkt_ref_ptr)->app_field;

    /*-------------------------------------------------------------------
      Pushdown hdr
    -------------------------------------------------------------------*/
  ret_val = dsm_pushdown(pkt_ref_ptr, (*rmnet_info)->meta_sm.eth_header,
                     sizeof(*((*rmnet_info)->meta_sm.eth_header)),
                     DSM_DS_SMALL_ITEM_POOL);

  if (sizeof(*((*rmnet_info)->meta_sm.eth_header)) != ret_val)
  {
    LOG_MSG_ERROR_1("Eth hdr pushdown fail (%d)", ret_val);
    return PS_DL_OPT_RESULT_DROP;
  }

  (*pkt_ref_ptr)->app_field = pkt_app_field;

  }/*Non-RNDIS mode*/

  return PS_DL_OPT_RESULT_SUCCESS;
} /* ps_dl_opti_rm_eth_proc() */

INLINE void ps_dl_opt_set_bearer_id
(
  ps_iface_type      * um_base_iface_ptr,
  dsm_item_type     ** pkt_ref_ptr,
  uint8                bearer_id
)
{
  ps_iface_type      * bridge_iface_ptr = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (PS_IFACE_IS_VALID(um_base_iface_ptr))
  {
    bridge_iface_ptr = PS_IFACEI_GET_OPT_RM_IFACE(um_base_iface_ptr);
    if ( PS_IFACE_IS_VALID(bridge_iface_ptr) &&
                    bridge_iface_ptr->name == SIO_IFACE )
    {
      if ( (pkt_ref_ptr != NULL) && (*pkt_ref_ptr != NULL) )
      {
        (*pkt_ref_ptr)->kind = bearer_id;
      }
    }
  }
}

/*===========================================================================

                        EXTERNAL FUNCTION DEFINITIONS

===========================================================================*/
#ifdef FEATURE_DATA_PS_MBIM_SUPPORT
boolean ps_dl_opt_add_mbim_header
(
  rmnet_smi_info_type   *rmnet_info,
  dsm_item_type        **pkt_ptr,
  uint16                 total_len
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  return ps_dl_opti_add_mbim_hdr(rmnet_info, pkt_ptr, total_len);
} // ps_dl_opt_add_mbim_hdr
#endif /*FEATURE_DATA_PS_MBIM_SUPPORT*/

boolean ps_dl_phys_opt_rx_pkt
(
  const ps_phys_link_type  * this_phys_link_ptr,
  dsm_item_type           ** pkt_ref_ptr,
  ps_rx_meta_info_type     * rx_mi_ptr
)
{
  uint8  pkt_buf[PS_OPT_PATH_EXP_HEADER_LEN_DL+1] ALIGN(4); //__attribute__((aligned(4)));
  dsm_item_type          * rx_pkt = NULL;
  ps_iface_type          * um_base_iface_ptr = NULL;
  ps_iface_type          * bridge_iface_ptr;
  ps_meta_info_type_ex   * mi_ex_ptr = NULL;
  ps_dl_opt_filter_spec_type * filter_ptr = NULL;
  rmnet_smi_info_type    * rmnet_info;
  rmnet_sio_info_type    * sio_info;
  ps_ip_addr_type          te_ip_addr;
#ifdef FEATURE_DATA_PS_MBIM_SUPPORT
  lan_lle_enum_type        lle_instance;
  lan_llc_start_info_type *ptr_start_info;
  boolean                  rmnet_info_valid = FALSE;
#endif /*FEATURE_DATA_PS_MBIM_SUPPORT*/
  int32                    rval;
  uint16                   total_len;
  uint16                   dst_port;
  uint16                   src_port;
  uint16                   ethertype;
  uint16                   buf_len;
  uint8                    version = 0;
  uint8                    protocol;
  uint16                   ip_hdr_len;
  uint8                    bearer_id = 0xFF;
  boolean                  ret_val;
  sint15                   ps_errno;
  errno_enum_type          err_no;
  ip_pkt_info_type         *ip_pkt_info = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (pkt_ref_ptr == NULL || *pkt_ref_ptr == NULL)
  {
    return FALSE;
  }
  rx_pkt = *pkt_ref_ptr;

  PS_ENTER_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
  PS_ENTER_CRIT_SECTION(&global_ps_crit_section);

  bearer_id = PS_PHYS_LINK_GET_BEARER_ID(this_phys_link_ptr);

  do
  {
    buf_len = dsm_extract( rx_pkt, 0, pkt_buf, PS_OPT_PATH_EXP_HEADER_LEN_DL );

    version = *pkt_buf & PS_OPT_PATH_IP_VERSION_MASK;

    if (version == PS_OPT_PATH_V4_VERSION)
    { //IPV4
      um_base_iface_ptr =
        this_phys_link_ptr->rx_cmd_info.ip_proto_handle.v4_iface_ptr;

      ps_dl_opt_set_bearer_id(um_base_iface_ptr, pkt_ref_ptr, bearer_id);

      if(PS_OPTIMIZED_IP_DL_V4_FRAG_CHECK((uint16)*((uint16 *)(pkt_buf +
                                          PS_OPT_PATH_V4_FRAG_OFFSET))))
      {
        break;
      }

      if (buf_len < PS_OPT_PATH_SOFTAP_IP_MIN_HEADER_LEN)
      {
        LOG_MSG_ERROR_1("ps_dl_phys_opt_rx_pkt: Too small packet len (%d)",
                        buf_len);
        break;
      }

      protocol = *(pkt_buf + PS_OPT_PATH_V4_PROT_FIELD_OFFSET);
      total_len = ps_htons((uint16)*((uint16 *)(pkt_buf +
                    PS_OPT_PATH_V4_TOTAL_LEN_OFFSET)));
      ip_hdr_len = (*pkt_buf & PS_OPT_PATH_IP_HDR_LEN_MASK) << 2;
      ethertype = ps_htons(LAN_ETHERTYPE_IPV4);
      te_ip_addr.type = IPV4_ADDR;
    }
    else if (version == PS_OPT_PATH_V6_VERSION)
    { //IPV6
      um_base_iface_ptr =
        this_phys_link_ptr->rx_cmd_info.ip_proto_handle.v6_iface_ptr;

      ps_dl_opt_set_bearer_id(um_base_iface_ptr, pkt_ref_ptr, bearer_id);

      if (buf_len < PS_OPT_PATH_V6_HDR_LEN)
      {
        LOG_MSG_ERROR_1("ps_dl_phys_opt_rx_pkt: Too small packet len (%d)",
                        buf_len);
        break;
      }

      if ( PS_IN6_IS_PREFIX_LINKLOCAL
           ((uint32) *(uint32 *) (pkt_buf + PS_OPT_PATH_V6_DEST_ADDR_OFFSET)))
      {
        break;
      }

      protocol = *(pkt_buf + PS_OPT_PATH_V6_NXT_HDR_OFFSET);
      if (PS_OPTIMIZED_IP_DL_V6_FRAG_CHECK(protocol))
      {
        break;
      }
      
      total_len = ps_htons((uint16)*((uint16 *)(pkt_buf +
                    PS_OPT_PATH_V6_TOTAL_LEN_OFFSET)));
      total_len += PS_OPT_PATH_V6_HDR_LEN;
      ip_hdr_len = PS_OPT_PATH_V6_HDR_LEN;
      ethertype = ps_htons(LAN_ETHERTYPE_IPV6);
      te_ip_addr.type = IPV6_ADDR;
    }
    else
    {
      LOG_MSG_ERROR_1("ps_dl_phys_opt_rx_pkt: Invalid IP version (%d), drop!",
                      version);
      PS_LEAVE_CRIT_SECTION(&global_ps_crit_section);
      PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
      dsm_free_packet(pkt_ref_ptr);
      return FALSE;
    }

    //if v4/v6 iface_ptr is NULL or Invalid, drop packet
    if (!PS_IFACE_IS_VALID(um_base_iface_ptr))
    {
      LOG_MSG_ERROR_0("Invalid um_base_iface_ptr, drop!");
      PS_LEAVE_CRIT_SECTION(&global_ps_crit_section);
      PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
      dsm_free_packet(pkt_ref_ptr);
      return FALSE;
    }

    //if bridge ptr is NULL, give to legacy path
    bridge_iface_ptr = PS_IFACEI_GET_OPT_RM_IFACE(um_base_iface_ptr);
    if (!PS_IFACE_IS_VALID(bridge_iface_ptr))
    {
      break;
    }

    //Get source port
    src_port = (uint16)*((uint16*)(pkt_buf + ip_hdr_len));

    //Get destination port
    dst_port = (uint16)*((uint16*)(pkt_buf + ip_hdr_len + 2));

    LOG_MSG_INFO4_1("ps_dl_phys_opt_rx_pkt: protocol: %d", protocol);

    //Ship all packets other than TCP and UDP to legacy path
    if (protocol != PS_IPPROTO_TCP && protocol != PS_IPPROTO_UDP)
    {
      LOG_MSG_ERROR_0("ps_dl_phys_opt_rx_pkt: Protocol neither TCP/UDP"
                      "Sending it to legacy path");
      break;
    }

    filter_ptr = PS_IFACEI_GET_OPT_FILTER_BASE(um_base_iface_ptr);
    if (ps_dl_opti_process_filters(filter_ptr, 
                                   src_port, 
                                   dst_port, 
                                   protocol,
                                   PS_IFACE_MAX_OPT_FILTER_ENTRIES)) 
    {
      goto legacy_proc;
    }
    //If filters installed on iface fails, then run filters on global iface
    if (ps_dl_opti_process_filters(global_dl_opt_fltr_arr, 
                                   src_port, 
                                   dst_port, 
                                   protocol,
                                   MAX_GLOBAL_OPT_FILTER_ENTRIES)) 
    {
          goto legacy_proc;
    }

    /*-----------------------------------------------------------------------
       Increment the receive packet count
    -----------------------------------------------------------------------*/
    um_base_iface_ptr->iface_i_stats.pkts_rx++;
    um_base_iface_ptr->iface_i_stats.bytes_rx += total_len;

    /*-----------------------------------------------------------------------
      Log the packet on the Rx side
    -----------------------------------------------------------------------*/
    DPL_LOG_NETWORK_RX_PACKET(um_base_iface_ptr, rx_pkt, DPL_IID_NETPROT_IP);

    rmnet_info = (rmnet_smi_info_type *)bridge_iface_ptr->iface_private.tx_cmd_info;
#ifdef FEATURE_DATA_PS_MBIM_SUPPORT
    rmnet_info_valid = TRUE;
#endif /*FEATURE_DATA_PS_MBIM_SUPPORT*/
    bridge_iface_ptr->iface_i_stats.pkts_tx++;
    bridge_iface_ptr->iface_i_stats.bytes_tx += total_len;

    /*-----------------------------------------------------------------------
      Log packet on the Tx side
    -----------------------------------------------------------------------*/
    DPL_LOG_NETWORK_TX_PACKET(bridge_iface_ptr,
                              *pkt_ref_ptr,
                              DPL_IID_NETPROT_IP);

    if ( (uint32)rmnet_info < LAN_LLE_MAX)
    {
      rval = ps_dl_opti_rm_eth_proc(pkt_ref_ptr, &rmnet_info,
                                    bridge_iface_ptr,
                                    &te_ip_addr, pkt_buf,
                                    (lan_lle_enum_type)rmnet_info, ethertype);
      if (rval == PS_DL_OPT_RESULT_FAILURE)
      {
        break;
      }
      else if (rval == PS_DL_OPT_RESULT_DROP)
      {
        PS_LEAVE_CRIT_SECTION(&global_ps_crit_section);
        PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
        dsm_free_packet(pkt_ref_ptr);
        return FALSE;
      }
    }

    // packet filtering
    if (1)
    {
      if (rmnet_info->meta_sm.packet_filters != NULL &&
          rmnet_info->meta_sm.packet_filters->enabled &&
          (rmnet_info->meta_sm.packet_filters->restrictive !=
           rmnet_utils_packet_filter_match(
          &(rmnet_info->meta_sm.packet_filters->filter_list), pkt_ref_ptr))
         )
      {
         PS_LEAVE_CRIT_SECTION(&global_ps_crit_section);
         PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
         LOG_MSG_ERROR_2("Restrictive(%d) Filtering applied, "
                         "packets not allowed! dropping size(%d)",
                         rmnet_info->meta_sm.packet_filters->restrictive,
                         dsm_length_packet(*pkt_ref_ptr));
          dsm_free_packet(pkt_ref_ptr);          
          *pkt_ref_ptr = NULL;
          return FALSE;
      }
    }

    sio_info = (rmnet_sio_info_type *) rmnet_info->meta_sm.rmnet_sio_handle;

#ifdef FEATURE_DATA_PS_MBIM_SUPPORT
    if ((rmnet_info->meta_sm.data_agg_protocol.dl_data_agg_protocol ==
        RMNET_ENABLE_DATA_AGG_MBIM || rmnet_info->meta_sm.data_agg_protocol.dl_data_agg_protocol ==
        RMNET_ENABLE_DATA_AGG_QC_NCM) &&
        FALSE == ps_dl_opti_add_mbim_hdr(rmnet_info, pkt_ref_ptr, total_len))
    {
      PS_LEAVE_CRIT_SECTION(&global_ps_crit_section);
      PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
      *pkt_ref_ptr = NULL;
      return FALSE;
    }
#endif /*FEATURE_DATA_PS_MBIM_SUPPORT*/

    PS_LEAVE_CRIT_SECTION(&global_ps_crit_section);

#ifdef FEATURE_DATA_A2
    if (pkt_ref_ptr != NULL && *pkt_ref_ptr != NULL)
    {
      A2_COMMON_SET_DSM_PKT_UNIQUE_ID(*pkt_ref_ptr, bearer_id);
    }
#endif /* FEATURE_DATA_A2 */

    /* ------------------------------------------------------------------------
      Execute powersave filters on incoming packet. If there is a match send
      it for upperlayers. Exit powersave mode if auto exit mode is enabled.

      If there is no match, drop the paket and send an indication for 
      fast dormancy algorithm to kick in.
    -------------------------------------------------------------------------*/
    if( PS_IFACE_GET_POWERSAVE_FILTERING_MODE(bridge_iface_ptr))
    {
      if( ps_rx_pkt_info_generate(pkt_ref_ptr, &rx_mi_ptr, &err_no) == -1)
      {
        dsm_free_packet(pkt_ref_ptr);
        
        um_base_iface_ptr->iface_i_stats.pkts_dropped_rx++;
        bridge_iface_ptr->iface_i_stats.pkts_dropped_tx++;

        LOG_MSG_ERROR_0("ps_dl_phys_opt_rx_pkt(): Not able to generate"
                        " rx_mi_ptr info");
        ret_val = FALSE;  
        return -1;
      }

      ip_pkt_info = &rx_mi_ptr->pkt_info;
      if( PS_IFACE_IPFLTR_NOMATCH == 
            ps_iface_ipfltr_execute( bridge_iface_ptr, 
                                     IP_FLTR_CLIENT_POWERSAVE, 
                                     PS_IFACE_IPFLTR_SUBSET_ID_DEFAULT, 
                                     ip_pkt_info))
      {
        //call the drop packet indication to run the fast dormancy delay timer algorithm
        um_base_iface_ptr->iface_i_stats.pkts_dropped_rx++;
        bridge_iface_ptr->iface_i_stats.pkts_dropped_tx++;
        
        if(!PS_IFACE_IS_POWERSAVE_FIRST_PACKET_DROP_REPORTED())
        {
          PS_IFACE_SET_POWERSAVE_FIRST_PACKET_DROP_REPORTED(TRUE);
          ps_sys_fast_dormancy_pkt_dropped_ind(bridge_iface_ptr);
        }
        
        dsm_free_packet(pkt_ref_ptr);
        ret_val = FALSE;
      }
      else
      {
        sio_transmit( sio_info->stream_id, *pkt_ref_ptr );
        ret_val = TRUE;
        if(PS_IFACE_GET_POWERSAVE_FILTERING_AUTO_EXIT_MODE(bridge_iface_ptr))
        {
          if( -1 == ps_iface_set_powersave_filtering_mode( bridge_iface_ptr,
                                                           FALSE,
                                                           &ps_errno) )
          {
            
            LOG_MSG_ERROR_0("ps_dl_phys_opt_rx_pkt:" 
              "powersave filtering mode change failed ");
          }
        }
      }
      ps_rx_meta_info_free(&rx_mi_ptr);
    }
    else
    {
      sio_transmit( sio_info->stream_id, *pkt_ref_ptr );
      ret_val = TRUE;
    }
    *pkt_ref_ptr = NULL;
    rmnet_info->meta_sm.aggr_pkt = NULL;
    PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
    return ret_val;

  } while (0);

legacy_proc:
  PS_LEAVE_CRIT_SECTION(&global_ps_crit_section);
  PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);

  PS_META_INFO_GET_EX(mi_ex_ptr);
  if (mi_ex_ptr == NULL)
  {
    LOG_MSG_ERROR_0("ps_dl_phys_opt_rx_pkt: "
                    "No PS MEM to allocate meta info buffer!");
    ASSERT( 0 );
    dsm_free_packet(pkt_ref_ptr);
    return FALSE;
  }
  mi_ex_ptr->rx_meta_info_ptr = rx_mi_ptr;

  (void) ps_iface_input_legacy(um_base_iface_ptr,
                               pkt_ref_ptr,
                               mi_ex_ptr);

  *pkt_ref_ptr = NULL;

#ifdef FEATURE_DATA_PS_MBIM_SUPPORT
  PS_ENTER_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
  PS_ENTER_CRIT_SECTION(&global_ps_crit_section);
  /* For MBIM we need to signal end of Tx if packet was consumed by
   * legacy path before reaching Rm, filters/OOO frags/drops */
  if (!rmnet_info_valid)
  {
    bridge_iface_ptr = PS_IFACEI_GET_OPT_RM_IFACE(um_base_iface_ptr);
    if (!PS_IFACE_IS_VALID(bridge_iface_ptr))
    {
      PS_LEAVE_CRIT_SECTION(&global_ps_crit_section);
      PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
      return TRUE;
    }

    rmnet_info = (rmnet_smi_info_type *)bridge_iface_ptr->iface_private.tx_cmd_info;
    if ( (uint32)rmnet_info < LAN_LLE_MAX)
    {
      lle_instance = (lan_lle_enum_type)(uint32)rmnet_info;
      ptr_start_info = &(&lle_array[lle_instance])->start_info;
      rmnet_info = (rmnet_smi_info_type *)ptr_start_info->tx_f_user_data;
    }
  }

  if (RMNET_ENABLE_DATA_AGG_MBIM == rmnet_info->meta_sm.data_agg_protocol.dl_data_agg_protocol ||
      RMNET_ENABLE_DATA_AGG_QC_NCM == rmnet_info->meta_sm.data_agg_protocol.dl_data_agg_protocol )

  {
      if (FALSE == ps_dl_opti_add_mbim_hdr(rmnet_info, pkt_ref_ptr, 0))
    {
        PS_LEAVE_CRIT_SECTION(&global_ps_crit_section);
        PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
        return FALSE;
    }

    /* Transmit remaining pkt if any */
    if (*pkt_ref_ptr)
    {
#ifdef FEATURE_DATA_A2
      A2_COMMON_SET_DSM_PKT_UNIQUE_ID(*pkt_ref_ptr, bearer_id);
#endif /* FEATURE_DATA_A2 */
      sio_info = (rmnet_sio_info_type *) rmnet_info->meta_sm.rmnet_sio_handle;
      sio_transmit( sio_info->stream_id, *pkt_ref_ptr );
      *pkt_ref_ptr = NULL;
      rmnet_info->meta_sm.aggr_pkt = NULL;
    }
  }
  PS_LEAVE_CRIT_SECTION(&global_ps_crit_section);
  PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
#endif /*FEATURE_DATA_PS_MBIM_SUPPORT*/
  return TRUE;
} //ps_dl_phys_opt_rx_pkt

boolean ps_dl_iface_opt_rx_pkt
(
  ps_iface_type            * this_iface_ptr,
  dsm_item_type           ** pkt_ref_ptr
)
{
  uint8  pkt_buf[PS_OPT_PATH_EXP_HEADER_LEN_DL+1] ALIGN(4); //__attribute__((aligned(4)));
  dsm_item_type          * rx_pkt = NULL;
  ps_iface_type          * um_base_iface_ptr = this_iface_ptr;
  ps_iface_type          * bridge_iface_ptr;
  ps_dl_opt_filter_spec_type * filter_ptr = NULL;
  rmnet_smi_info_type    * rmnet_info;
  rmnet_sio_info_type    * sio_info;
  ps_ip_addr_type          te_ip_addr;
#ifdef FEATURE_DATA_PS_MBIM_SUPPORT
  dsm_item_type           *tmp_pkt = NULL;
  lan_lle_enum_type        lle_instance;
  lan_llc_start_info_type *ptr_start_info;
#endif /*FEATURE_DATA_PS_MBIM_SUPPORT*/
  int32                    ret_val = 0;
  uint16                   total_len;
  uint16                   buf_len;
  uint16                   dst_port;
  uint16                   src_port;
  uint16                   ethertype;
  uint8                    ip_hdr_len;
  uint8                    version = 0;
  uint8                    protocol;
  uint8                    bearer_id = 0xFF;
  ps_phys_link_type      * primary_phys_link_ptr = NULL;
  sint15                   ps_errno;
  errno_enum_type          err_no;
  ps_rx_meta_info_type     * rx_mi_ptr = NULL;
  ip_pkt_info_type         *ip_pkt_info = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (pkt_ref_ptr == NULL || *pkt_ref_ptr == NULL)
  {
    return FALSE;
  }
  rx_pkt = *pkt_ref_ptr;

  PS_ENTER_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
  PS_ENTER_CRIT_SECTION(&global_ps_crit_section);

  if (PS_IFACE_IS_VALID(um_base_iface_ptr))
  {
    // XXX TODO: We should get real phys link not primary
    primary_phys_link_ptr = PS_IFACEI_GET_PHYS_LINK(um_base_iface_ptr);
    bearer_id = PS_PHYS_LINK_GET_BEARER_ID(primary_phys_link_ptr);

    ps_dl_opt_set_bearer_id(um_base_iface_ptr, pkt_ref_ptr, bearer_id);
  }

  do
  {
    buf_len = dsm_extract( rx_pkt, 0, pkt_buf, PS_OPT_PATH_EXP_HEADER_LEN_DL );

    version = *pkt_buf & PS_OPT_PATH_IP_VERSION_MASK;

    //Unicast IP check
    if (version == PS_OPT_PATH_V4_VERSION)
    { //IPV4
      if(PS_OPTIMIZED_IP_DL_V4_FRAG_CHECK((uint16)*((uint16 *)(pkt_buf +
         PS_OPT_PATH_V4_FRAG_OFFSET))))
      {
        break;
      }

      if (buf_len < PS_OPT_PATH_SOFTAP_IP_MIN_HEADER_LEN)
      {
        LOG_MSG_ERROR_1("ps_dl_iface_opt_rx_pkt: Too small packet len (%d)",
                        buf_len);
        break;
      }

      protocol = *(pkt_buf + PS_OPT_PATH_V4_PROT_FIELD_OFFSET);
      total_len = ps_htons((uint16)*((uint16 *)(pkt_buf +
                  PS_OPT_PATH_V4_TOTAL_LEN_OFFSET)));
      ip_hdr_len = (*pkt_buf & PS_OPT_PATH_IP_HDR_LEN_MASK) << 2;
      ethertype = ps_htons(LAN_ETHERTYPE_IPV4);
      te_ip_addr.type = IPV4_ADDR;
    }
    else if (version == PS_OPT_PATH_V6_VERSION)
    { //IPV6
      if (buf_len < PS_OPT_PATH_V6_HDR_LEN)
      {
        LOG_MSG_ERROR_1("ps_dl_iface_opt_rx_pkt: Too small packet len (%d)",
                        buf_len);
        break;
      }

      if ( PS_IN6_IS_PREFIX_LINKLOCAL
         ((uint32) *(uint32 *) (pkt_buf + PS_OPT_PATH_V6_DEST_ADDR_OFFSET)))
      {
        break;
      }

      protocol = *(pkt_buf + PS_OPT_PATH_V6_NXT_HDR_OFFSET);
      if (PS_OPTIMIZED_IP_DL_V6_FRAG_CHECK(protocol))
      {
        break;
      }

      total_len = ps_htons((uint16)*((uint16 *)(pkt_buf +
                    PS_OPT_PATH_V6_TOTAL_LEN_OFFSET)));
      total_len += PS_OPT_PATH_V6_HDR_LEN;
      ip_hdr_len = PS_OPT_PATH_V6_HDR_LEN;
      ethertype = ps_htons(LAN_ETHERTYPE_IPV6);
      te_ip_addr.type = IPV6_ADDR;
    }
    else
    {
      LOG_MSG_ERROR_1("ps_dl_iface_opt_rx_pkt: Invalid IP version (%d), drop!",
                      version);
      PS_LEAVE_CRIT_SECTION(&global_ps_crit_section);
      PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
      dsm_free_packet(pkt_ref_ptr);
      *pkt_ref_ptr = NULL;
      return FALSE;
    }

    //if bridge ptr is NULL, give to legacy path
    bridge_iface_ptr = PS_IFACEI_GET_OPT_RM_IFACE(um_base_iface_ptr);
    if (!PS_IFACE_IS_VALID(bridge_iface_ptr))
    {
      break;
    }

    //get source port
    src_port = (uint16)*((uint16*)(pkt_buf + ip_hdr_len));

    //Get destination port
    dst_port = (uint16)*((uint16*)(pkt_buf + ip_hdr_len + 2));

    LOG_MSG_INFO4_1("ps_dl_iface_opt_rx_pkt: protocol: %d", protocol);

    //Ship all packets other than TCP and UDP to legacy path
    if (protocol != PS_IPPROTO_TCP && protocol != PS_IPPROTO_UDP)
    {
      LOG_MSG_ERROR_0("ps_dl_iface_opt_rx_pkt: Protocol neither TCP/UDP"
                      "Sending it to legacy path");
      break;
    }

    {
      filter_ptr = PS_IFACEI_GET_OPT_FILTER_BASE(um_base_iface_ptr);
    }

    if (ps_dl_opti_process_filters(filter_ptr,
                                   src_port,
                                   dst_port,
                                   protocol,
                                   PS_IFACE_MAX_OPT_FILTER_ENTRIES)) 
    {
      goto legacy_proc;
    }

    if (ps_dl_opti_process_filters(global_dl_opt_fltr_arr,
                                   src_port,
                                   dst_port,
                                   protocol,
                                   MAX_GLOBAL_OPT_FILTER_ENTRIES)) 
    {
      goto legacy_proc;
    }
    {
      /*-----------------------------------------------------------------------
         Increment the receive packet count
      -----------------------------------------------------------------------*/
      um_base_iface_ptr->iface_i_stats.pkts_rx++;
      um_base_iface_ptr->iface_i_stats.bytes_rx += total_len;

      /*-----------------------------------------------------------------------
        Log the packet on the Rx side
      -----------------------------------------------------------------------*/
      DPL_LOG_NETWORK_RX_PACKET(um_base_iface_ptr, rx_pkt, DPL_IID_NETPROT_IP);
    }

    rmnet_info = (rmnet_smi_info_type *)bridge_iface_ptr->iface_private.tx_cmd_info;
    bridge_iface_ptr->iface_i_stats.pkts_tx++;
    bridge_iface_ptr->iface_i_stats.bytes_tx += total_len;

    /*-----------------------------------------------------------------------
      Log packet on the Tx side
    -----------------------------------------------------------------------*/
    DPL_LOG_NETWORK_TX_PACKET(bridge_iface_ptr,
                              *pkt_ref_ptr,
                              DPL_IID_NETPROT_IP);

    if ( (uint32)rmnet_info < LAN_LLE_MAX)
    {
      ret_val = ps_dl_opti_rm_eth_proc(pkt_ref_ptr, &rmnet_info,
                                       bridge_iface_ptr,
                                       &te_ip_addr, pkt_buf,
                                       (lan_lle_enum_type)rmnet_info, ethertype);
      if (ret_val == PS_DL_OPT_RESULT_FAILURE)
      {
        break;
      }
      else if (ret_val == PS_DL_OPT_RESULT_DROP)
      {
        PS_LEAVE_CRIT_SECTION(&global_ps_crit_section);
        PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
        dsm_free_packet(pkt_ref_ptr);
        return FALSE;
      }
    }

  // packet filtering

    if (1)
    {
      if (rmnet_info->meta_sm.packet_filters != NULL &&
          rmnet_info->meta_sm.packet_filters->enabled &&
          (rmnet_info->meta_sm.packet_filters->restrictive !=
           rmnet_utils_packet_filter_match(
          &(rmnet_info->meta_sm.packet_filters->filter_list), pkt_ref_ptr))
         )
      {
        PS_LEAVE_CRIT_SECTION(&global_ps_crit_section);
        PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
	LOG_MSG_ERROR_2("Restrictive(%d) Filtering applied, "
                 "packets not allowed! dropping size(%d)",
                 rmnet_info->meta_sm.packet_filters->restrictive,
                 dsm_length_packet(*pkt_ref_ptr));
        dsm_free_packet(pkt_ref_ptr);        
        *pkt_ref_ptr = NULL;
        return FALSE;

      }
    }

    sio_info = (rmnet_sio_info_type *) rmnet_info->meta_sm.rmnet_sio_handle;
#ifdef FEATURE_DATA_PS_MBIM_SUPPORT
    if ((RMNET_ENABLE_DATA_AGG_MBIM == rmnet_info->meta_sm.data_agg_protocol.dl_data_agg_protocol ||
         RMNET_ENABLE_DATA_AGG_QC_NCM == rmnet_info->meta_sm.data_agg_protocol.dl_data_agg_protocol ) &&
         FALSE == ps_dl_opti_add_mbim_hdr(rmnet_info, pkt_ref_ptr, total_len))
    {
      PS_LEAVE_CRIT_SECTION(&global_ps_crit_section);
      PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
      *pkt_ref_ptr = NULL;
      return FALSE;
    }
#endif /*FEATURE_DATA_PS_MBIM_SUPPORT*/
    PS_LEAVE_CRIT_SECTION(&global_ps_crit_section);

#ifdef FEATURE_DATA_A2
    if (pkt_ref_ptr != NULL && *pkt_ref_ptr != NULL)
    {
      A2_COMMON_SET_DSM_PKT_UNIQUE_ID(*pkt_ref_ptr, bearer_id);
    }
#endif /* FEATURE_DATA_A2 */

    /* ------------------------------------------------------------------------
      Execute powersave filters on incoming packet. If there is a match send
      it for upperlayers. Exit powersave mode if auto exit mode is enabled.

      If there is no match, drop the paket and send an indication for 
      fast dormancy algorithm to kick in.
     -------------------------------------------------------------------------*/

    if( PS_IFACE_GET_POWERSAVE_FILTERING_MODE(bridge_iface_ptr))
    {
      if( ps_rx_pkt_info_generate(pkt_ref_ptr, &rx_mi_ptr, &err_no) == -1)
      {
        dsm_free_packet(pkt_ref_ptr);

        um_base_iface_ptr->iface_i_stats.pkts_dropped_rx++;
        bridge_iface_ptr->iface_i_stats.pkts_dropped_tx++;

        LOG_MSG_ERROR_0("ps_dl_iface_opt_rx_pkt(): Not able to generate"
                        " rx_mi_ptr info");
        ret_val = FALSE;
	  
        return -1;
      }
      ip_pkt_info = &rx_mi_ptr->pkt_info;
	  
      if( PS_IFACE_IPFLTR_NOMATCH == 
          ps_iface_ipfltr_execute( bridge_iface_ptr, IP_FLTR_CLIENT_POWERSAVE, 
                                   PS_IFACE_IPFLTR_SUBSET_ID_DEFAULT, ip_pkt_info))
      {
        //drop packet and send indication to run the fast dormancy delay timer algorithm
        um_base_iface_ptr->iface_i_stats.pkts_dropped_rx++;
        bridge_iface_ptr->iface_i_stats.pkts_dropped_tx++;
        
        if(!PS_IFACE_IS_POWERSAVE_FIRST_PACKET_DROP_REPORTED())
        {
           PS_IFACE_SET_POWERSAVE_FIRST_PACKET_DROP_REPORTED(TRUE);
          ps_sys_fast_dormancy_pkt_dropped_ind(bridge_iface_ptr);
        }
        
        dsm_free_packet(pkt_ref_ptr);
        ret_val = FALSE;
      }
      else
      {
        sio_transmit( sio_info->stream_id, *pkt_ref_ptr );
        ret_val = TRUE;
        if(PS_IFACE_GET_POWERSAVE_FILTERING_AUTO_EXIT_MODE(bridge_iface_ptr))
        {
          if( -1 == ps_iface_set_powersave_filtering_mode( bridge_iface_ptr,
                                                           FALSE,
                                                           &ps_errno) )
          {
            LOG_MSG_ERROR_0("ps_dl_iface_opt_rx_pkt: "
              "powersave filtering mode change failed ");
          }
        }
      }
	  ps_rx_meta_info_free(&rx_mi_ptr);
    }
    else
    {
      sio_transmit( sio_info->stream_id, *pkt_ref_ptr );
      ret_val = TRUE;
    }
    
    *pkt_ref_ptr = NULL;
    rmnet_info->meta_sm.aggr_pkt = NULL;
    PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
    return ret_val;

  } while (0);

legacy_proc:
#ifdef FEATURE_DATA_PS_MBIM_SUPPORT
  /* Some additional processing necessary for MBIM */
  bridge_iface_ptr = PS_IFACEI_GET_OPT_RM_IFACE(um_base_iface_ptr);
  if (!PS_IFACE_IS_VALID(bridge_iface_ptr))
  {
    PS_LEAVE_CRIT_SECTION(&global_ps_crit_section);
    PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
    return TRUE;
  }

  rmnet_info = (rmnet_smi_info_type *)bridge_iface_ptr->iface_private.tx_cmd_info;
  if ( (uint32)rmnet_info < LAN_LLE_MAX)
  {
    lle_instance = (lan_lle_enum_type)(uint32)rmnet_info;
    ptr_start_info = &(&lle_array[lle_instance])->start_info;
    rmnet_info = (rmnet_smi_info_type *)ptr_start_info->tx_f_user_data;
  }

  if (RMNET_ENABLE_DATA_AGG_MBIM == rmnet_info->meta_sm.data_agg_protocol.dl_data_agg_protocol ||
      RMNET_ENABLE_DATA_AGG_QC_NCM == rmnet_info->meta_sm.data_agg_protocol.dl_data_agg_protocol )
  {
    if (FALSE == ps_dl_opti_add_mbim_hdr(rmnet_info, &tmp_pkt, 0))
    {
      PS_LEAVE_CRIT_SECTION(&global_ps_crit_section);
      PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
      return FALSE;
    }
  }

  if (tmp_pkt)
  {
#ifdef FEATURE_DATA_A2
    A2_COMMON_SET_DSM_PKT_UNIQUE_ID(tmp_pkt, bearer_id);
#endif /* FEATURE_DATA_A2 */
    sio_info = (rmnet_sio_info_type *) rmnet_info->meta_sm.rmnet_sio_handle;
    sio_transmit( sio_info->stream_id, tmp_pkt);
    rmnet_info->meta_sm.aggr_pkt = NULL;
  }
#endif /*FEATURE_DATA_PS_MBIM_SUPPORT*/

#ifdef TEST_FRAMEWORK
  #error code not present
#endif

  PS_LEAVE_CRIT_SECTION(&global_ps_crit_section);
  PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
  return FALSE;
} //ps_dl_iface_opt_rx_pkt

