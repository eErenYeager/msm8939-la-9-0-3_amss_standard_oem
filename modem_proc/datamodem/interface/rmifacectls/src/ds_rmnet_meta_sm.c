/*===========================================================================

                     D S _ R M N E T _ M E T A _ S M . C
DESCRIPTION

  The Data Services Rm Network interface State Machine source file.

  This state machine manages the Rm network interface and coordinates
  wireless WAN connectivity between the Rm and Um interfaces over all
  supported radio technologies.

EXTERNALIZED FUNCTIONS

  rmnet_meta_sm_init()
    Allocate and initialize all data required for the RmNet State Machine.

  rmnet_meta_sm_post_event()
    Post an event to RmNet State Machine.

  rmnet_meta_sm_process_ev_cmd()
    Process a cmd posted to Rm State Machine.

  rmnet_meta_sm_in_call()
    Returns true if in a packet call.

  rmnet_meta_sm_iface()
    Returns the ps_iface controlled by the given RMNET RmSM instance

  rmnet_meta_sm_set_network_cfg_params()
    This function is called to set the parameters received in Start Network
    Interface for network configuration in the rmnet info structure.

  rmnet_meta_sm_get_um_iface()
     Returns the UM ps_iface ptr for the given RmSM instance

  rmnet_meta_sm_is_auto_connect_enabled()
    Returns whether auto_connect is enabled on this instance or not

  rmnet_meta_sm_set_autoconnect_delay_timer()
    Delays the auto-connect timer

Copyright (c) 2004-2014 QUALCOMM Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary
===========================================================================*/
/*===========================================================================

                            EDIT HISTORY FOR FILE
   $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/rmifacectls/src/ds_rmnet_meta_sm.c#2 $

when        who    what, where, why
--------    ---    ----------------------------------------------------------
06/26/14    wc     QMAP support for tethered RmNet
04/21/14    rk     Removed feature mobileap.
02/24/14    vrk    CM SS events handling for SGLTE+G CM redesign
12/20/13    vrk    Removed assert(0) in rmnet_iface_ioctl_hdlr()
12/09/13    ssh    Deprecate prefix is handled in Um iface teardown
10/22/13    sah    Corrected handling of variable, autoconnect_next_backoff_time
10/21/13    sah    Corrected handling of variable, autoconnect_next_backoff_time 
10/17/13    vrk    Added support for 12 instance tags to rmnet_config file 
10/03/13    ms     Dont allow new emb call if arbitration in progress already
23/09/13    ms     Dont allow new emb call if arbitration in progress already
                   for another Rmnet instance with same Um iface ptr.
09/06/13    sah    Fix for KW Errors
08/12/13    bh     Fixed incorrect setting of proc id
07/31/13    vrk    Fixed KW Error
07/12/13    cx     Change the ps_icmp6_get_dest_mac_addr argument due to memcpy replacement
06/04/13    ash    Make logical iface as input to get_ipv6_ext_addr 
06/20/13    pgm    Fix to store and clear MBIM mpdp sesions correctly.
06/14/13    pgm    Fix to handle race condtion between MBIM call disconnect
                   and packet processing.
06/10/13    pgm    Added flow control for Legacy Wmk.
05/30/13    wc     Non-MBIM packet filtering
05/20/13    pgm    Fix to clear right mbim session in a multi pdn call.
05/13/2013  pgm    Added MBIM feature flag.
04/26/13    pgm    MBIM Multi-PDN data path changes.
04/17/13    wc     Reject unrecognized data format
02/04/13    svj    Changes to call new API ps_route_tear_down instead
                   of ps_iface_tear_down_cmd
03/21/13    wc     QMAP support
01/22/12    svj    Remove autoconnect NV writes for default values
12/10/12    svj    Support 8 concurrent PDN's
12/06/12    at     Fix for the USB TE-A5 data path issue in ETHERNET mode.
12/04/12    at     Reset eth_arped for RNDIS mode upon NULL state
11/30/12    wc     Defer resetting of data format on DTR low
10/26/12    wc     Enable NAT for every data call if configured in EFS
10/31/12    wc     Memory optimization
10/23/12    svj    Fix to disable aggregation on non A2 ports
10/23/12    gk     Disabled Data Aggregation
10/09/12    sj     8 PDN support changes
09/29/12    tw     Deprecate old IPv6 prefix
09/19/12    wc     Disable data aggregation except MBIM for FUSION 3
09/12/12    sb     Move IPv6 Rmnet state to be routable in SoftAP mode even
                   in the absence of RA.
08/24/12    gk     Fixed v4 and v6 calls getting out of sync issue, when a
                   second set_data_format was sent and v6 call is up
09/06/12    pgm    Support for different APNS in Dual-IP.
09/05/12    wc     Fix DTR race condition
08/01/12    pgm    Outstanding mbim_pkt_info handling to avoid race condition
                   b/w DCC and PS_RM task.
07/31/12    sb     Fixed Compiler Warning.
07/26/12    pgm    Fix to set default ndp sig in QCNCM/MBIM mode.
06/20/12    sa     Setting appropriate data aggregation modes.
07/11/12    pgm    Added support for RNDIS agg mode.
07/10/12    pgm    Fix for routing packets to the right PDN in a multi-PDN
                   call after flow is enabled.
07/06/12    wc     Support QCMAP rmnet EFS configuration
06/14/12    sj     Reset eth_arped flag to FALSE when call goes down so that
                   eth_header values can be repopulated during next call
06/07/12    wc     Fix writing of wrong rmnet_config.txt
06/06/12    sb     Fix to gracefully handle attempts to connect on more than 5 pdn's.
05/14/12    sb     Fixed KW Warnings.
05/08/12    sn     Updating Rmnet data format client state when DTR is High.
05/07/12    sa     Fix to store correct qmi index from rmnet instance.
04/18/12    sn     Enabling Tx flow when a call is being brought up. This
                   resets Tx flow state if it is disabled in previous call.
04/12/2012  pgm    Moving Legacy path processing to PS task from PS_RM to
                   avoid race condition issues.
04/05/12    pgm    Reset A2 mode when setting QC_NCM/MBIM mode as A2 could be
                   in TLP mode earlier.
03/14/12    wc     Added get_proc_id_from_inst routine
03/13/12    wc     Use iface handle to determine if two um ifaces are the same
03/09/12    sn     Fixed instance used as index for autoconnect setting nv.
01/03/12    sa     Changes for Set/Get autoconnect and updating efs.
02/26/12    pgm    Added PS_IFACE_COMMON_DEFAULT_IS_FLOW_ENABLED for flow
                   control check in rmnet_meta_smi_rx_ready.
02/22/12    wc     Enable EFS to configure proc_id
02/21/12    wc     Mainline eMBMS
02/17/12    sb     Fixed Compiler and KW Warnings.
02/02/12    cy     Added support for MBIM NTB parameters
02/10/12    rt     Added support for SoftAPv6
01/12/12    wc     Call a2 tlp functions only for a2 port
12/27/11    wc     De-featurize FEATURE_DATA_FUSION_PS_ARBITRATION_MGR
12/19/11    wc     Fix rmnet dual-ip signal issue
12/06/11    wc     Reset arb_close_in_progress flag when rm iface is down
11/02/11    wc     Add DTR on SIO control channel feature
11/01/11    am     Added 9x15 SoftAP support.
10/13/11    wc     Increase RMNET_RX_WM_DNE
10/04/11    sa     Q6 free floating changes for QMI.
09/22/11    am     Added DL MBIM support.
09/22/11    pgm    Added MBIM sig hdlr registration for MBIM mode.
09/18/11    wc     Nikel and MPSS.NI.1.1 port configuration
09/07/11    pgm    Added check for sizeof(rmnet_meta_smi_qos_hdr_type) to be
                   even as a part of UL optimization.
09/07/11    pgm    Added support for sig handlers for UL optimization.
09/02/11    wc     Add check to prevent autoconnect from starting if port
                   is not active.
08/18/11    sa     Sending No Effect as return value when the call is
                   already up and we try to set data format.
08/10/11    wc     Data aggregation based on CPU utilization
08/01/11    wc     Print packet type in F3 when dropping packet
07/26/11    ua     Fixed Data Call status indication for Tethered calls.
07/21/11    wc     Reject SNI if Um iface is already used by another RmNet
                   instance.
06/24/11    asn    Support for new Iface generic indication Active Out of Use
06/30/11    ua     Fixing RDM, DCC race condition due to with
                   rmnet_dual_smi_info is not getting initalized which is
                   required at RmNet init.
01/11/11    kk     Added support for data_call_type TLV in data_call_status
                   indication.
06/15/11    sa     Fixed compiler warning.
05/30/11    kk     Fix to prevent PS/DCC race condition in ETH mode.
05/17/11    kk     Adding support for DPL logging on upto ten RmNet ifaces.
05/17/11    sy     Fix to set phys link ptr in rmnet cache info to NULL after
                   de-registering from physlink events.
05/02/11    ua     Enabling Autoconnect to working with first instance across
                   all targets.
05/02/11    ua     Allowing Autoconnect with NV 3534 also. Only IPV4 is
                   supported with this NV autoconnect.
02/14/2011  ua     Added support for DUAL IP Autoconnect.
04/08/11    kk     Re-merging support for DOS flag in QoS header.
03/09/11    kk     Removing DCC command buffer dependency from RmNet.
02/07/11    kk     Fixed LINT critical errors.
01/10/11    ss     Cleaning up of Globals for thread safety in Q6 Free
                   Floating environment.
12/14/10    ash    Delete IPv6 Addresses when IPv6 call goes down.
07/16/10    asn    Support for Arbitration - round II
07/01/10    asn    Support for Arbitration
06/19/10    vs     Changes for Dual IP support over single QMI instance
06/10/10    kk     Added supported for DOS flag in QoS header.
10/23/09    ar     Moved RDM coupling for port management to QMUX
02/19/09    am     DS Task De-coupling effort and introduction of DCC task.
02/12/09    rt     Removed assert for configuration failure event in GOING
                   DOWN state.
09/21/08    pp     Metainfo optimizations.
06/18/08    mct    Fixed a corner case where call could tear down at the exact
                   time addresses are assigned.
04/01/08    mct    Added IP mode support.
10/01/07    mct    Rearchitected RmNet to support multiple interface types.
                   Added IPv6 support.
07/19/07    ks     Reset autoconnect_timer back to MIN when DTR drops.
                   Don't report failure to DCTM if DTR drops.
07/18/07    ks     Don't report DCTM failure for UM iface down as DS mode
                   handler already reports it.
06/21/07    ks     added state to track whether rmnet is registered for
                   default flow enabled/disabled event, to avoid calling
                   reg/dereg apis back to back.
05/28/07    ks     added autoconnect backoff timer retry mechanism.
05/28/07   jd/ks   Post a cmd to DS when tlp_change_cb occurs.
04/24/07    ks     Added check for flow enabled in rmnet_meta_smi_rm_rx_data().
02/22/06    ks     Handling DHCP_COMPLETE_EV in UP_STATE.
02/06/07    scb    Fixed Klockwork Critical Error in rmnet_meta_smi_set_mac()
01/08/07    jd     Added Winmobile multiple RmNet support
12/05/06    jd     Drop packets bigger than 1500 bytes rather than asserting
11/29/06    jd     Added DCTM support to RmNet
11/13/06    ks     Added support for MULTIPLE_NAI feature in rmnet.
10/09/06    ks     Added rmnet_meta_sm_process_cmd to process DS rmnet cmds.
10/09/06    ks     Reverted previous change.
10/09/06    ks     Added rmnet_meta_sm_process_cmd to process DS rmnet cmds.
                   Removed asserts during init to handle wrong nv item values
09/21/06    ks     Clear rx and tx wmks when the call ends.
09/13/06    jd     Changes to use PS extended signal mask
08/23/06    ks     Changes to support multiple rmnet instances. Added
                   rmnet_meta_sm_is_auto_connect_enabled().
07/20/06    ks     Added check for dhcp_handle before calling dhcp_stop and
                   arp_flush.
07/17/06    ks     Removed unnecessary rx and tx wmks from rmnet_info struct.
05/24/06    ks     Set bring_up to true in acl_policy as route_acl post
                   processing looks at bring_up flag to set iface in use.
03/13/06    jd     Removed notifications (rmnet_meta_sm_reg_notification)
03/07/06    jd     Added support for TLP
03/06/06    ks     Posting RMNET_META_SM_RM_DEV_START_PKT_EV if DTR is high in
                   NULL state. Added rmnet_meta_sm_get_um_iface().
01/25/06    ks     Initialize profile number and tech pref in net_params
                   struct in rmnet_meta_smi_info_type to default values for
                   autoconnect.
01/09/06    jd     Added support for bringing up 802.11 interface
12/02/05    ks     Added support for Profiles.
10/31/05    ks     Stop DHCP when RMNET SM transitions to NULL from UP state.
10/31/05    ks     Register for phys_link flow enable/disable events. Added
                   check for phys_link flow control in rmnet_meta_smi_rm_rx_data().
10/24/05    ks     Fixed pdp profile number for UMTS to use the ATCOP family.
10/18/05    ks     Checking if USB current device is NETWORK_DEVICE before
                   initializing rmnet instance in rmnet_meta_sm_init().
10/10/05    ks     Removed SIO DATA WWAN RDM device.
09/13/05    jd     Added support for starting UMTS calls
09/13/05    jd/ks  Added rmnet_meta_sm_process_dtr_changed_cmd().
09/09/05    jd     Added rmnet_meta_sm_set_auth_creds() to set the rmnet_meta_sm's
                   ps_iface auth settings.  Check NV for autoconnect setting.
09/08/05    ks     Changed qmi_ws_if to qmi_if.
06/17/05    jd     Fixed assert on UM_ADDR_CHANGE event during call setup
05/31/05    jd     Code review updates
05/17/05    jd     Moved initialization of QMI services to QMUX module
05/11/05    ks     Some clean up.
04/26/05    jd     ARP proxy check compares IP addr's in network order
04/19/05    jd     Set RmNet ps iface as broadcast interface
03/15/05    ks     Added support for Ethernet Logging.
03/14/05    ks     Removed featurization
03/08/05    ks     Clean up and changed name from wwan_rmsm.c
11/22/04    hba    Integrate LLC Mode 0 code...
10/27/04    jd     Added wwan_rmsm_iface() to return current ps_iface in use.
                   Fixed some missing taskfree's in Rm_start_pkt.
                   Generate Rm_start_pkt result notifications for WWAN start
                   packet session result.
                   Featurized auto-call connection (later: make runtime cfg)
                   Send media disconnect, unbridge Um&Rm after call teardown.
07/12/04    jd     Formalized wwani_rmsm_set_mac() API to set Rm MAC address.
                   Activate context after SIOlib is set to PKT mode -
                   ensure that data path is configured before data is sent.
05/12/04    jd     Added call startup result notification callback
                   Added rm_dev_end_pkt_event handlign (same as rm_link_down)
                   Renamed  wwan_rmsm_is_packet_call() as wwan_rmsm_in_call()
04/01/04    jd     Added wwan_rmsm_is_packet_call()
02/06/04    jd     Initial implementation
===========================================================================*/

/*===========================================================================

                          INCLUDE FILES FOR MODULE

===========================================================================*/
#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"
#include "event_defs.h"

#if defined(T_WINNT)
#error code not present
#endif /* WINNT */

/* use dsm watermark callback with arguments - define here until other
   dsm users update their code so we can mainline this feature -
   no problem with compilation since 2 args are placed in R0, R1 on arm,
   thus are overwritten/ignored where feature isn't defined.
   (no stack overflows) */
#define FEATURE_DSM_WM_CB

#include "amssassert.h"
#include "msg.h"
#include "err.h"
#include "ps_crit_sect.h"
#include "queue.h"
#include "dsm.h"

#include "ps_mem.h"
#include "ps_metai_info.h"
#include "ps_iface_defs.h"
#include "ps_iface.h"
#include "ps_ifacei.h"
#include "ps_ifacei_event.h"
#include "ps_route.h"
#include "ps_route_iface.h"
#include "ps_flow_event.h"
#include "ps_flow.h"

#include "ps_iface_logging.h"
#include "ps_logging_defs.h"
#include "secutil.h"
#include "ran.h"
#include "secerrno.h"
#include "ds_flow_control.h"

#include "dcc_task_defs.h"
#include "dcc_task_svc.h"

#include "ds_rmnet_sm_ext.h"
#include "ds_rmnet_smi.h"
#include "ds_rmnet_sio.h"
#include "ds_rmnet_sio_rt_acl.h"
#include "ds_rmnet_defs.h"

#include "ds_qmux.h"
#include "ds_qmi_defs.h"
#include "ds_qmi_ctl.h"
#include "ds_qmi_svc.h"
#include "dsati.h"
/* need to remove this dependency added for profile support */
#include "ds_qmi_wds.h"

#ifdef FEATURE_HS_USB
  #include "hsu_config_selector.h"
#endif
#include "rdevmap.h"

#include "ps_iface_ioctl.h"
#include "ps_ul_optimized_hdlr.h"

#ifdef FEATURE_QXDM_WM_TRACING
#include "fc_wm.h"
#endif /* FEATURE_QXDM_WM_TRACING */

#include "ds_profile.h"
#include "ds_profile_3gpp.h"
#include "ds_profile_3gpp2.h"
#include "ds3gmgr.h"

#include "ds_qmi_cflog.h"

#include "ps_arp.h"

#include "ps_icmp6_nd.h"
#include "ps_ifacei_addr_v6.h"
#include "ps_ip4_hdr.h"
#include "ps_icmp.h"

#include "ps_stat_iface.h"
#include "ds_qmi_nv_util.h"

#include "ps_policyi_mgr.h"
#include "ds_Utils_DebugMsg.h"
#include "dssocket.h"

#ifdef FEATURE_DATA_A2
#include "a2_ul_per.h"
#include "a2_dl_per.h"
#include "a2_common.h"
#endif /* FEATURE_DATA_A2 */

#ifdef FEATURE_DATA_PS_ADDR_MGMT
#include "ps_iface_addr_mgmt.h"
#endif /* FEATURE_DATA_PS_ADDR_MGMT */

#ifdef FEATURE_DATA_PS_MBIM_SUPPORT
#include "ps_dl_optimized_hdlri.h"
#endif /* FEATURE_DATA_PS_MBIM_SUPPORT */

#include "ps_optimized_path_defs.h"

#include "ps_system_heap.h"
#include "ps_ifacei_utils.h"

#include "ps_rm_defs.h"
#include "ps_rm_svc.h"

#include <stringl/stringl.h>
#include "fs_errno.h"
#include "fs_public.h"
#include "fs_sys_types.h"

#if defined(FEATURE_RMNET_DATA_AGG_TIMER) && defined(FEATURE_CPU_BASED_FLOW_CONTROL)
#include "fc.h"
#include "tmc.h"
#endif /* FEATURE_RMNET_DATA_AGG_TIMER && FEATURE_CPU_BASED_FLOW_CONTROL */

#include "ps_wmk_logging.h"
#include "ps_sys.h"
#include "ds_qmap.h"
#include "ds_qmi_qos.h"

/*===========================================================================

                                EXTERNAL DECLARATIONS

===========================================================================*/
extern ps_iface_type * ps_route_look_up_iface_by_policy
(
  acl_policy_info_type    * acl_pi_ptr,
  boolean                 * arb_candidate,
  uint32                        * iface_priority_mask,
  ps_iface_net_down_reason_type * net_down_reason
);

/*===========================================================================

                                DATA TYPES

===========================================================================*/

/*===========================================================================

                               DEFINITIONS

===========================================================================*/

/*---------------------------------------------------------------------------
  Serving system changed bitmask
---------------------------------------------------------------------------*/
#define RMNET_META_SM_CM_SRV_SYS_CHANGED_MASK (0x00                   |   \
                                               CM_SS_SRV_STATUS_MASK  |   \
                                               CM_SS_ROAM_STATUS_MASK |   \
                                               CM_SS_HDR_ROAM_STATUS_MASK | \
                                               CM_SS_HDR_SRV_STATUS_MASK )


/*---------------------------------------------------------------------------
  RMNET_META_SM state management and event processing functions
---------------------------------------------------------------------------*/
#define AUTOCONNECT_BACKOFF_TIMER_MIN_VALUE  1
#define AUTOCONNECT_BACKOFF_TIMER_MAX_VALUE  120

/* Maximum number of active Ifaces */
#define RMNET_IFACE_MAX (PS_SYS_MAX_AVAIL_PDNS * 2)

#define RMNET_CONFIG_EFS_FILE_SIZE 6000
typedef enum {
  RMNET_QOS_FLAG_TYPE_NO_FLAGS  = 0x0,
  RMNET_QOS_FLAG_TYPE_DOS       = 0x1,
  RMNET_QOS_FLAG_TYPE_ALL_FLAGS = 0xFF
} rmnet_qos_flag_e_type;

static dpl_iid_ifname_enum_type dpl_ifname_by_rmnet_instance[RMNET_IFACE_MAX];

ps_crit_sect_type rmnet_crit_section;
ps_crit_sect_type rmnet_rx_sig_crit_section;
ps_crit_sect_type rmnet_rx_pkt_hdlr_crit_section;

rex_crit_sect_type lan_llc_inst_to_um_iface_ptr_crit_sec;


/*---------------------------------------------------------------------
  UINT64 Application IDs for Arbitration. Temporary position.
---------------------------------------------------------------------*/
#define APP_ID_TE_DUN    0x0000000100000000ULL
#define APP_ID_APPS      0x0000000200000000ULL
#define APP_ID_TE_RMNET  0x0000000300000000ULL

/*---------------------------------------------------------------------
  Datastructure to hold mapping between sio port to proc id
---------------------------------------------------------------------*/
typedef struct rmnet_sio_port_to_proc_id_map_s
{
  sio_port_id_type sio_port;
  uint32           proc_id;
} rmnet_sio_port_to_proc_id_map_type;

/*---------------------------------------------------------------------
  Table to determine tethered RmNET ports.
---------------------------------------------------------------------*/
static rmnet_sio_port_to_proc_id_map_type rmnet_sio_proc_id_tbl[] =
{
  { SIO_PORT_DATA_MUX_1 /*1x RmNet*/,             RMNET_PROC_ID_LAPTOP1 },
  { SIO_PORT_DATA_MUX_2 /*1x RmSM TE-DUN data*/,  RMNET_PROC_ID_LAPTOP1 },
  { SIO_PORT_USB_NET_WWAN,                        RMNET_PROC_ID_LAPTOP1 },
  { SIO_PORT_USB_RMNET2,                          RMNET_PROC_ID_LAPTOP1 },
  { SIO_PORT_USB_RMNET3,                          RMNET_PROC_ID_LAPTOP1 },
  { SIO_PORT_A2_RMNET_1,                          RMNET_PROC_ID_LAPTOP1 },
  { SIO_PORT_A2_RMNET_2,                          RMNET_PROC_ID_LAPTOP1 },
  { SIO_PORT_A2_RMNET_3,                          RMNET_PROC_ID_LAPTOP1 },
  { SIO_PORT_SMD_DATA40,                          RMNET_PROC_ID_LAPTOP1 },
  { SIO_PORT_SMD_DATA39,                          RMNET_PROC_ID_LAPTOP1 },
  { SIO_PORT_SMD_DATA38,                          RMNET_PROC_ID_LAPTOP1 },
  { SIO_PORT_SMD_DATA37,                          RMNET_PROC_ID_LAPTOP1 },
  { SIO_PORT_SMD_DATA36,                          RMNET_PROC_ID_LAPTOP1 },
  { SIO_PORT_SMD_DATA35,                          RMNET_PROC_ID_LAPTOP1 },
  { SIO_PORT_SMD_DATA34,                          RMNET_PROC_ID_LAPTOP1 },
  { SIO_PORT_SMD_DATA33,                          RMNET_PROC_ID_LAPTOP1 },
  { SIO_PORT_SDIO_MUX_A2_RMNET_TETH_0,            RMNET_PROC_ID_LAPTOP1 }
};

static const uint32 SIO_PORT_TO_PROC_ID_MAPPING_TBL_MAX =
  sizeof(rmnet_sio_proc_id_tbl)/sizeof(rmnet_sio_port_to_proc_id_map_type);

/*------------------------------------------------------------------
      EFS Configuration and parsing definitions
 -------------------------------------------------------------------*/
#define RMNET_EFS_INVALID_PROFILE     0xFF
#define MAX_TAG_FIELDS                12
#define RMNET_EFS_AUTOCONNECT_INVALID 0xFF

/*Maximum num of instance tags supported in rmnet_config EFS file*/
#define RMNET_MAX_INSTANCE_TAGS       12

typedef struct
{
  char *tag_name[MAX_TAG_FIELDS];
  char *tag_value[MAX_TAG_FIELDS];
}rmnet_parser_raw_data_s_type;

char *rmnet_parser_tag_name[]={"instance",
                               "link_prot",
                               "qos",
                               "ul_tlp",
                               "autoconnect",
                               "ip_type",
                               "3gpp_profile",
                               "3gpp2_profile",
                               "dl_data_agg_protocol",
                               "proc_id",
                                 NULL
                              };

char *rmnet_parser_start_tag_name[]={"<instance>",
                               "<link_prot>",
                               "<qos>",
                               "<ul_tlp>",
                               "<autoconnect>",
                               "<ip_type>",
                               "<3gpp_profile>",
                               "<3gpp2_profile>",
                               "<dl_data_agg_protocol>",
                               "<proc_id>",
                                 NULL
                              };

char *rmnet_parser_end_tag_name[]={"</instance>\n",
                               "</link_prot>\n",
                               "</qos>\n",
                               "</ul_tlp>\n",
                               "</autoconnect>\n",
                               "</ip_type>\n",
                               "</3gpp_profile>\n",
                               "</3gpp2_profile>\n",
                               "</dl_data_agg_protocol>\n",
                               "</proc_id>\n",
                                 NULL
                              };

char *rmnet_parser_instance_start_tag[]={"<instance_1>\n",
                                     "<instance_2>\n",
                                     "<instance_3>\n",
                                     "<instance_4>\n",
                                     "<instance_5>\n",
                                     "<instance_6>\n",
                                     "<instance_7>\n",
                                     "<instance_8>\n",
                                     "<instance_9>\n",
                                     "<instance_10>\n",
                                     "<instance_11>\n",
                                     "<instance_12>\n",
                                      NULL
                                     };

char *rmnet_parser_instance_end_tag[]={"</instance_1>\n",
                                     "</instance_2>\n",
                                     "</instance_3>\n",
                                     "</instance_4>\n",
                                     "</instance_5>\n",
                                     "</instance_6>\n",
                                     "</instance_7>\n",
                                     "</instance_8>\n",
                                     "</instance_9>\n",
                                     "</instance_10>\n",
                                     "</instance_11>\n",
                                     "</instance_12>\n",
                                      NULL
                                     };

char *rmnet_parser_config_tag[]={"<config>\n",
                                "</config>\n"
                               };


extern lan_lle_cb_type  lle_array[LAN_LLE_MAX];

typedef enum
{
  RMNET_PARSER_TAG_INSTANCE = 0,    /* Tag --> Instance  */
  RMNET_PARSER_TAG_LINK_PROT,       /* Tag --> Link Protocol  */
  RMNET_PARSER_TAG_QOS,             /* Tag --> Qos enabled/disabled */
  RMNET_PARSER_TAG_UL_TLP,          /* Tag --> UL TLP enabled/disabled */
  RMNET_PARSER_TAG_AUTOCONNECT,     /* Tag --> Autoconnect enabled/disabled */
  RMNET_PARSER_TAG_IP_TYPE,         /* Tag --> IP type of IPV4/IPV6/IPV4V6 */
  RMNET_PARSER_TAG_UMTS_PROFILE,    /* Tag --> UMTS profile number     */
  RMNET_PARSER_TAG_CDMA_PROFILE,    /* Tag --> CDMA profile number  */
  RMNET_PARSER_TAG_DL_DATA_AGG,     /* Tag --> DL data aggregation protocol */
  RMNET_PARSER_TAG_PROC_ID,         /* Tag --> Proc id */
  RMNET_PARSER_TAG_MAX              /* Tag --> MAX -- internal   */
}rmnet_parser_tag_index_e_type;

/* The EFS file should have one of the below text for each instance.  */
char *rmnet_parser_instance_token[]={"instance_1",
                                     "instance_2",
                                     "instance_3",
                                     "instance_4",
                                     "instance_5",
                                     "instance_6",
                                     "instance_7",
                                     "instance_8",
                                     "instance_9",
                                     "instance_10",
                                     "instance_11",
                                     "instance_12",
                                      NULL
                                     };

rmnet_parser_raw_data_s_type rmnet_raw_config_data[RMNET_NUM_LAPTOP_INSTANCES];
rmnet_meta_sm_config_s_type rmnet_efs_config_params[RMNET_NUM_LAPTOP_INSTANCES];

/* Global Config structure */

char *rmnet_parser_global_tag_name[]= { "global_config",
                                        "ul_agg_prot_mask",
                                        "dl_agg_prot_mask",
                                        NULL
                                      };

typedef enum{

  RMNET_PARSER_TAG_GLOBAL_CONFIG_MASK = 0, /*Tag --> Global_config*/
  RMNET_PARSER_TAG_UL_AGG_PROT_MASK ,      /* Tag -->ul_agg_prot_mask  */
  RMNET_PARSER_TAG_DL_AGG_PROT_MASK,       /* Tag -->dl_agg_prot_mask */
  RMNET_PARSER_GLOBAL_TAG_MAX              /* Tag --> MAX -- internal   */
}rmnet_parser_tag_global_index_e_type;

rmnet_parser_raw_data_s_type rmnet_raw_global_config_data;

rmnet_meta_sm_global_config_s_type rmnet_efs_global_config_params;

/*===========================================================================

                           FORWARD DECLARATIONS

===========================================================================*/

static boolean rmnet_meta_smi_init
(
  rmnet_instance_e_type  instance
);

static boolean rmnet_meta_smi_iface_init
(
  uint8 i
);

static boolean rmnet_meta_smi_reg_events
(
  rmnet_smi_info_type *  info
);

static void rmnet_meta_smi_dereg_events
(
  rmnet_smi_info_type *  info
);

static boolean rmnet_meta_smi_reg_physlink_events
(
  rmnet_smi_info_type *  info
);

static void rmnet_meta_smi_dereg_physlink_events
(
  rmnet_smi_info_type *  info
);

static void rmnet_meta_smi_tlp_change_cmd_hdlr
(
  rmnet_sio_tlp_ver_type  dl_ver,
  rmnet_sio_tlp_ver_type  ul_ver,
  void *                  user_data
);

static void rmnet_meta_smi_process_dtr_changed_cmd
(
  dcc_cmd_enum_type cmd,
  void              *user_data_ptr
);

static void rmnet_meta_smi_process_phys_link_up_down_event
(
  ps_phys_link_type            *this_phys_link_ptr,
  ps_iface_event_enum_type      event,
  ps_iface_event_info_u_type    event_info,
  void                         *user_data_ptr
);

static int rmnet_meta_smi_set_rx_sig_handler
(
  rmnet_smi_info_type *  info
);

static int rmnet_meta_smi_start_addr_cfg_sm
(
  rmnet_smi_info_type *  info
);

static void rmnet_meta_smi_stop_addr_cfg_sm
(
  rmnet_smi_info_type *  info
);

static void rmnet_meta_smi_retrieve_default_profile_num
(
  ds_profile_tech_etype       profile_type,
  uint8                       profile_family,
  uint16                    * default_profile_num
);

/*---------------------------------------------------------------------------
  RMNET_META_SM state management and event processing functions
---------------------------------------------------------------------------*/

static void rmnet_meta_smi_transition_state
(
  rmnet_smi_info_type       * info,
  rmnet_meta_smi_state_type   new_state              /* state to tranistion to */
);

static void rmnet_meta_smi_handle_rm_dev_start_pkt_ev
(
  rmnet_smi_info_type *  info
);

static void rmnet_meta_smi_handle_rm_link_down_ev
(
  rmnet_smi_info_type *  info
);

static void rmnet_meta_smi_handle_um_iface_down_ev
(
  rmnet_smi_info_type *  info
);

static void rmnet_meta_smi_handle_um_iface_up_ev
(
  rmnet_smi_info_type *  info
);

static void rmnet_meta_smi_handle_addr_cfg_complete_ev
(
  rmnet_smi_info_type *  info
);

static void rmnet_meta_smi_handle_addr_cfg_failure_ev
(
  rmnet_smi_info_type *  info
);

static void rmnet_meta_smi_handle_addr_cfg_changed_ev
(
  rmnet_smi_info_type *  info
);

static void rmnet_meta_smi_handle_um_iface_authenticating_ev
(
  rmnet_smi_info_type *  info
);

static void rmnet_meta_smi_handle_um_iface_arb_ev
(
  rmnet_smi_info_type *  info
);

static void rmnet_meta_smi_handle_um_iface_bearer_tech_changed_ev
(
    rmnet_smi_info_type *  info
);

static void rmnet_meta_smi_update_physlink
(
  rmnet_instance_e_type  instance
);

static void rmnet_meta_smi_log_wmk_levels
(
   rmnet_instance_e_type            rmnet_instance,
   ps_phys_link_type               *um_phys_link_ptr,
   ps_wmk_logging_dorm_state_type   dormancy_status
);

/*---------------------------------------------------------------------------
  RMNET_META_SM callback functions registered with various external modules
---------------------------------------------------------------------------*/
static void rmnet_meta_smi_dtr_change_cb
(
  void *   user_data,
  boolean  is_asserted                /* Flag to indicate current DTR state */
);

static boolean rmnet_meta_smi_verify_dtr_high
(
  rmnet_smi_info_type   *info
);

#ifndef FEATURE_DATA_RM_NET_USES_SM
static void rmnet_meta_smi_tlp_change_mult_inst_cb
(
  rmnet_sio_tlp_ver_type  dl_ver,
  rmnet_sio_tlp_ver_type  ul_ver,
  void *                  user_data
);
static void rmnet_meta_smi_tlp_change_cb
(
  rmnet_sio_tlp_ver_type  dl_ver,
  rmnet_sio_tlp_ver_type  ul_ver,
  void *                  user_data
);
#endif /* FEATURE_DATA_RM_NET_USES_SM */

static void rmnet_meta_smi_um_iface_ev_hdlr_cb
(
  ps_iface_type *this_iface_ptr,           /* Pointer to the interface     */
  ps_iface_event_enum_type   event,        /* Event type occurred          */
  ps_iface_event_info_u_type event_info,   /* Event specific info block    */
  void *user_data_ptr           /* Data passed during event registration   */
);

static void rmnet_meta_smi_um_phys_link_ev_hdlr_cb
(
  ps_phys_link_type *this_phys_link_ptr,   /* Pointer to the interface     */
  ps_iface_event_enum_type   event,        /* Event type occurred          */
  ps_iface_event_info_u_type event_info,   /* Event specific info block    */
  void *user_data_ptr           /* Data passed during event registration   */
);

static void rmnet_meta_smi_um_flow_ev_hdlr_cb
(
  ps_flow_type                *this_flow_ptr,
  ps_iface_event_enum_type     event,
  ps_iface_event_info_u_type   event_info,
  void                        *user_data_ptr
);

static void rmnet_meta_smi_set_mac
(
  ps_iface_type *  iface,
  byte *           hw_addr,
  byte             hw_addr_len
);

#if defined(FEATURE_IP_CALL)
#if defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900)
static boolean rmnet_meta_smi_oprt_mode_change_cb
(
  sys_oprt_mode_e_type  new_oprt_mode,
  cmipapp_trans_id_type trans_id
);
#endif /* defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900) */
#endif /* defined(FEATURE_IP_CALL) */

/*---------------------------------------------------------------------------
  rmnet_sio receive function
---------------------------------------------------------------------------*/
static void rmnet_meta_smi_rm_rx_non_empty_func
(
  struct dsm_watermark_type_s *wm_ptr,
  void *user_data_ptr
);

/*---------------------------------------------------------------------------
  non_empty_cb_func will be invoked when a leagcy packet is enqueued into the
  Wmk by optimized fallback hdlr.
---------------------------------------------------------------------------*/
static void rmnet_meta_smi_legacy_wmk_non_empty_cb_func
(
  struct dsm_watermark_type_s * legacy_wm_ptr,
  void                        * user_data_ptr
);

/*---------------------------------------------------------------------------
  Low CB fn for Legacy Wmk.
---------------------------------------------------------------------------*/
static void rmnet_meta_smi_legacy_wmk_low_cb_func
(
  struct dsm_watermark_type_s * legacy_wm_ptr,
  void                        * user_data_ptr
);

/*---------------------------------------------------------------------------
  High CB fn for Legacy Wmk.
---------------------------------------------------------------------------*/
static void rmnet_meta_smi_legacy_wmk_high_cb_func
(
  struct dsm_watermark_type_s * legacy_wm_ptr,
  void                        * user_data_ptr
);

/*---------------------------------------------------------------------------
  Setup fn for Legacy Wmk.
---------------------------------------------------------------------------*/
static void rmnet_meta_smi_setup_legacy_watermarks
(
  rmnet_smi_dual_ip_info_type *  info
);

/*---------------------------------------------------------------------------
  Physical link data transfer functions
---------------------------------------------------------------------------*/

static int rmnet_meta_smi_ip_mode_tx_cmd
(
  ps_iface_type *      iface_ptr,
  dsm_item_type **     pkt_chain_ptr,
  ps_meta_info_type*   meta_info_ptr,
  void*                client_info
);

static void rmnet_meta_smi_rm_tx_data
(
  dsm_item_type **item_head_ptr,
  void *user_data
);

static void rmnet_meta_smi_rm_tx_data_mbim
(
  dsm_item_type **  item_head_ptr,
  void *            user_data
);

static void rmnet_meta_smi_init_mbim_hdr
(
  rmnet_smi_info_type *  info
);

static void rmnet_meta_smi_init_rndis_eth_hdr
(
  rmnet_smi_info_type *  info
);

static void rmnet_meta_smi_rm_tlp_tx_data
(
  dsm_item_type **  item_head_ptr,
  void *            user_data
);

/*---------------------------------------------------------------------------
  Function to dequeue packet from Legacy Wmk.
---------------------------------------------------------------------------*/
static dsm_item_type* rmnet_meta_smi_rm_rx_data
(
  rmnet_smi_dual_ip_info_type *  info,
  rmnet_meta_smi_qos_hdr_type * qos_hdr
);

static dsm_item_type* rmnet_meta_smi_rm_rx_data_qos2
(
  rmnet_smi_dual_ip_info_type *  info,
  rmnet_meta_smi_qos2_hdr_type * qos_hdr
);

static void rmnet_meta_smi_process_qos2_hdr
(
  void *                user_data,
  dsm_item_type *       pkt_ptr,
  ps_meta_info_type_ex  ** meta_info_ex_ptr_ptr,
  rmnet_meta_smi_qos2_hdr_type * qos_hdr
);

static dsm_item_type *  rmnet_meta_smi_rm_rx_llc_data
(
  void *  rmnet_smi_info_handle,
  ps_meta_info_type_ex  ** meta_info_ex_ptr_ptr
);

static void rmnet_meta_smi_autoconnect_timer_cback
(
  timer_cb_data_type  user_data
);

static void rmnet_meta_smi_process_autoconnect_timer_cmd_hdlr
(
  void              *user_data
);

static boolean rmnet_meta_instance_rx_ready
(
  uint8 dual_ip_inst
);

/*---------------------------------------------------------------------------
  Sig Hdlr to handle packets taking legacy path in PS task.
---------------------------------------------------------------------------*/
static boolean rmnet_meta_smi_legacy_sig_hdlr
(
  rmnet_legacy_sig_enum_type   rx_sig,
  void                       * user_data
);

static boolean rmnet_meta_smi_legacy_sig_hdlr_qos2
(
  rmnet_legacy_sig_enum_type   rx_sig,
  void                       * user_data
);

/*---------------------------------------------------------------------------
  Legacy Sig Hdlr cleanup fn
---------------------------------------------------------------------------*/
static void rmnet_meta_smi_legacy_sig_hdlr_cleanup
(
  rmnet_smi_dual_ip_info_type *  info
);

#ifdef FEATURE_DATA_MBIM
/*---------------------------------------------------------------------------
  Init fn for MBIM port info
---------------------------------------------------------------------------*/
static void rmnet_mbim_port_info_init
(
  rmnet_instance_e_type  instance
);

/*---------------------------------------------------------------------------
  UL MBIM data path init fn
---------------------------------------------------------------------------*/
#ifdef FEATURE_DATA_A2
#ifdef FEATURE_DATA_MBIM
static void rmnet_meta_smi_init_rx_mbim_data_path
(
  rmnet_smi_info_type *  info
);
#endif /* FEATURE_DATA_MBIM */
#endif /* FEATURE_DATA_A2 */

/*---------------------------------------------------------------------------
  UL MBIM data path configure fn
---------------------------------------------------------------------------*/
static int rmnet_meta_smi_config_rx_mbim_data_path
(
  rmnet_smi_info_type *  info
);

/*---------------------------------------------------------------------------
  UL MBIM outstanding pkt info init fn
---------------------------------------------------------------------------*/
static int8 rmnet_meta_smi_init_mbim_outstanding_info
(
  void
);

/*---------------------------------------------------------------------------
  MBIM wmk non_empty_cb_func
---------------------------------------------------------------------------*/
static void rmnet_meta_smi_mbim_wmk_non_empty_cb_func
(
  struct dsm_watermark_type_s * legacy_wm_ptr,
  void                        * user_data_ptr
);

/*---------------------------------------------------------------------------
  MBIM call cleanup fn
---------------------------------------------------------------------------*/
static void rmnet_meta_smi_mbim_call_cleanup
(
  rmnet_smi_info_type *  info,
  uint8                  ips_id_cleanup
);

#endif /*FEATURE_DATA_MBIM*/

static void rmnet_meta_smi_update_autoconnect_roam_status
(
  void
);

static void rmnet_meta_smi_update_autoconnect_state
(
  rmnet_instance_e_type  instance
);

static void rmnet_meta_smi_set_default_data_format
(
  rmnet_instance_e_type  instance
);

static void rmnet_meta_smi_cm_ss_event_cb
(
  cm_ss_event_e_type            ss_event,    /* SS event that just occured */
  const cm_mm_msim_ss_info_s_type *  ss_info_ptr  /* ptr to serving system info */
);

static void rmnet_meta_smi_process_cm_ss_event
(
  cm_ss_event_e_type      ss_event,
  rmnet_meta_sm_cm_ss_info_type  ss_info,
  sys_modem_as_id_e_type  asubs_id
);

static boolean rmnet_meta_smi_reg_cm
(
  void
);

#if defined(FEATURE_IP_CALL)
#if defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900)
static void rmnet_meta_smi_cm_op_mode_change_cmd_hdlr
(
  rmnet_smi_cmd_type * cmd_ptr
);

static void rmnet_meta_smi_initialize_cmipapp_info
(
  void
);
#endif /* defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900) */
#endif /* defined(FEATURE_IP_CALL) */


/*--------------------------------------------------------------------------
  IOCTL Handler
--------------------------------------------------------------------------*/

LOCAL int rmnet_iface_ioctl_hdlr
(
  ps_iface_type            *iface_ptr,
  ps_iface_ioctl_type      ioctl_name,
  void                     *argval_ptr,
  sint15                   *ps_errno
);

#ifdef FEATURE_QXDM_WM_TRACING
/*--------------------------------------------------------------------------
  RMNET callback function for DSM watermark reporting to FC
--------------------------------------------------------------------------*/
static void rmnet_meta_smi_fc_get_wm_trace_cb
(
  uint32            user_data,
  fc_wm_stat_type  *wm_stat_ptr
);
#endif /* FEATURE_QXDM_WM_TRACING */

/*--------------------------------------------------------------------------
  Utility routine to get proc id
--------------------------------------------------------------------------*/
static boolean rmnet_meta_smi_find_proc_id(
  uint32           *proc_id,
  sio_port_id_type  sio_port,
  rmnet_instance_e_type rmnet_inst
);

static boolean rmnet_um_iface_in_use(
  rmnet_smi_info_type  * info
);

static boolean rmnet_um_iface_is_arb_progress(
  rmnet_smi_info_type  * info
);

static int rmnet_tear_down_um_iface(
  rmnet_smi_info_type  * info
);

rmnet_sig_handler_type rmnet_set_sig_handler
(
  rmnet_sig_enum_type sig,              /* Signal assocaited with handler     */
  rmnet_sig_handler_type sig_handler,   /* Signal handler function pointer    */
  void *user_data_ptr                /* Param to be passed to the handler  */
);

/*---------------------------------------------------------------------------
  Function to set Sig hdlr per Legacy Signal in dual_ip_info.
---------------------------------------------------------------------------*/
rmnet_legacy_sig_handler_type rmnet_set_legacy_sig_handler
(
  rmnet_legacy_sig_enum_type      sig,           /* Signal assocaited with handler     */
  rmnet_legacy_sig_handler_type   sig_handler,   /* Signal handler function pointer    */
  void                          * user_data_ptr  /* Param to be passed to the handler  */
);

static boolean rmnet_ext_sig_hdlr
(
  ps_rm_sig_enum_type   rx_sig,
  void*                 user_data
);

/*---------------------------------------------------------------------------
  External Legacy Sig Hdlr invoked in PS task when PS_RX_RMNET1_SIG is set.
---------------------------------------------------------------------------*/
static boolean rmnet_ext_legacy_sig_hdlr
(
  ps_sig_enum_type   rx_sig,
  void             * user_data
);

static boolean rmnet_set_rm_iface_addr_family
(
  ps_iface_type    * rm_iface_ptr,
  uint8              addr_family
);

/*===========================================================================

                     EFS Configuration and Parsing Function

===========================================================================*/
static char * rmnet_parser_match_end_token
(
  char *token,
  uint32 token_len,
  char *buff,
  uint32 len
);

static char *rmnet_parser_match_start_token
(
  char *token,
  uint32 token_len,
  char *buff,
  uint32 len
);

static boolean rmnet_parse_matching_token
(
  char *token,
  uint32 token_len,
  char *in_buff,
  uint32 in_len,
  char * out_buff,
  uint32 * out_len
);

static void rmnet_parser_trim_config_data
(
  char * config_buff,
  uint32 config_len,
  char *trim_buff,
  uint32 *trim_buff_len
);

static void rmnet_parser_parse_config_data
(
  char *trim_buff,
  uint32 trim_buff_len
);

static void rmnet_parser_config_get_tag_value
(
  rmnet_parser_raw_data_s_type  raw_config_data,
  rmnet_parser_tag_index_e_type tag_key,
  uint32                        instance_id,
  rmnet_meta_sm_config_s_type  *config_params
);

static void rmnet_meta_sm_get_efs_config_params
(
  rmnet_meta_sm_config_s_type *config_params
);

static void rmnet_parser_global_config_get_tag_value
(
  rmnet_parser_tag_global_index_e_type tag_key
);

/*---------------------------------------------------------------------------
  RMNET Data Aggregation
---------------------------------------------------------------------------*/
#if defined(FEATURE_RMNET_DATA_AGG_TIMER) && defined(FEATURE_CPU_BASED_FLOW_CONTROL)

uint8 rmnet_da_cpu_high = 60;  // CPU utilization high threshold
uint8 rmnet_da_cpu_low  = 40;  // CPU utilization low threshold

extern boolean rmnet_sio_da_enabled; // Is DA enabled (ds_rmnet_sio.c)
q_type rmnet_fc_cmd_q;

static boolean rmnet_meta_smi_fc_register
(
  void
);

static boolean rmnet_meta_sm_process_fc_sig
(
   dcc_sig_enum_type sig,
   void *user_data_ptr
);

#endif /* FEATURE_RMNET_DATA_AGG_TIMER && FEATURE_CPU_BASED_FLOW_CONTROL */

static void rmnet_meta_smi_rx_fc
(
  rmnet_smi_info_type  * info,
  uint32                 fc_mask,
  boolean                disable_flow
);

/*===========================================================================

                               INTERNAL DATA

===========================================================================*/

/*===========================================================================
FUNCTION RMNET_META_SMI_V6_LOW_LATENCY_TRAFFIC_HANDLER()

DESCRIPTION
  This function checks whether the packet is ICMPv6 ECHO REQUEST/RESPONSE and 
  start low latency status timer mechanism

PARAMETERS
  pkt_buf         : pointer to the v6 pkt buffer
  ip_hdr_len      : IP header length
  prot_offset     : Protocol field offset in IP header
  rx_pkt          : packet reference pointer

RETURN VALUE
  None
DEPENDENCIES
  None

SIDE EFFECTS
  None.

===========================================================================*/
INLINE void rmnet_meta_smi_v6_low_latency_traffic_handler
(
  uint8 * const          pkt_buf,
  uint8                  ip_hdr_len,
  uint8                  prot_offset,
  dsm_item_type        * rx_pkt
)
{
  uint8 icmp6_hdr[PS_OPT_PATH_ICMP6_HDR_LEN_EXTRACT + 1] ALIGN(4);
/*---------------------------------------------------------------------------*/
 /*----------------------------------------------------------------------------
  call low latency timer handler in below scenarios
  1. If the pkt is ICMP6 ECHO REQ/RESP 
  2. If the pkt is fragmented, check whether next_header  is ICMPv6 and 
     fragment offset is 0
  
  In 2nd scenario we need to extract more bytes to check if it is ECHO REQ/RESP
  pkt as ICMPv6 header is not extracted yet.
  
  protocol offset and ip header length includes any headers present before IP
  header depending on the path it has come from.
  -----------------------------------------------------------------------------*/
  if( ( pkt_buf != NULL) && (*(pkt_buf + prot_offset) == PS_IPPROTO_ICMP6 ) &&
      (( (*(pkt_buf + ip_hdr_len + PS_ICMP6_TYPE_OFFSET) == ICMP6_ECHO_REQ) ||
         (*(pkt_buf + ip_hdr_len + PS_ICMP6_TYPE_OFFSET) == ICMP6_ECHO_RSP)) &&
         (*(pkt_buf + ip_hdr_len + PS_ICMP6_CODE_OFFSET) == 0)) )
  {
    ps_iface_low_latency_timer_start();
  }
  else if ( (pkt_buf != NULL) && 
            (*(pkt_buf + prot_offset) == PS_IPPROTO_FRAG_HDR ) &&
            (*(pkt_buf + ip_hdr_len + PS_FRAG_NEXT_HDR_OFFSET) == 
               PS_IPPROTO_ICMP6) &&
            !(*(uint16 *)(pkt_buf + ip_hdr_len + PS_FRAG_FRAG_OFFSET ) & 
                PS_OPT_PATH_V6_FRAG_OFFSET_MASK) )
  {
    if( PS_OPT_PATH_ICMP6_HDR_LEN_EXTRACT != 
          dsm_extract(rx_pkt, ip_hdr_len + PS_OPT_PATH_V6_OPT_HDR_LEN, 
                      icmp6_hdr, 
          PS_OPT_PATH_ICMP6_HDR_LEN_EXTRACT ) )
    {
      return;
    }

    if( (((*(icmp6_hdr + PS_ICMP6_TYPE_OFFSET) == ICMP6_ECHO_REQ) ||
            (*(icmp6_hdr + PS_ICMP6_TYPE_OFFSET) == ICMP6_ECHO_RSP)) &&
            (*(icmp6_hdr + PS_ICMP6_CODE_OFFSET) == 0)) )
    {
      ps_iface_low_latency_timer_start();
    }
  }
  else
  {
    return;
  }
}/*rmnet_meta_smi_v6_low_latency_traffic_handler()*/

/*---------------------------------------------------------------------------
  RmNet state machine info block
---------------------------------------------------------------------------*/
rmnet_smi_info_type rmnet_smi_info[RMNET_INSTANCE_MAX];

rmnet_smi_dual_ip_info_type rmnet_smi_dual_ip_info[RMNET_INSTANCE_MAX/2];

/* MAX of 16 Rmnet ifaces supported(8 for IPV4 and 8 for IPV6) */
static rmnet_smi_iface_info_type * rmnet_smi_iface_info[RMNET_IFACE_MAX];
static rmnet_in_use_signals_type   rmnet_signals[RMNET_IFACE_MAX];

uint32 rmnet_sig_mask = 0;

rmnet_sig_enum_type   rmnet_signals_table[RMNET_IFACE_MAX];

/*---------------------------------------------------------------------------
  Legacy Sig Hdlr variables.
---------------------------------------------------------------------------*/
uint32 rmnet_legacy_sig_mask = 0;
static rmnet_in_use_legacy_signals_type rmnet_legacy_signals[RMNET_IFACE_MAX];
static rmnet_legacy_sig_handler_info_type
       rmnet_legacy_sig_handler_arr[RMNET_LEGACY_MAX_SIGNAL];

rmnet_legacy_sig_enum_type rmnet_legacy_signals_table[RMNET_IFACE_MAX];

#ifdef FEATURE_DATA_MBIM
/*---------------------------------------------------------------------------
  Global MBIM call info
---------------------------------------------------------------------------*/
rmnet_mbim_call_info_type mbim_call_info;
#endif /*FEATURE_DATA_MBIM*/

static rmnet_sig_handler_info_type rmnet_sig_handler[RMNET_MAX_SIGNALS];

/*---------------------------------------------------------------------------
  RmNet cm info block
---------------------------------------------------------------------------*/
static rmnet_smi_cm_info_type  rmnet_smi_cm_info;

static rmnet_smi_eri_roam_ind_info_type rmnet_smi_eri_roam_ind_info;

#if defined(FEATURE_IP_CALL)
#if defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900)
/*---------------------------------------------------------------------------
  CMIPAPP info for rmnet interface
---------------------------------------------------------------------------*/
static rmnet_smi_cmipapp_info_type rmnet_smi_cmipapp_info;
#endif /* defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900) */
#endif /* defined(FEATURE_IP_CALL) */

/*---------------------------------------------------------------------------
  Function table registered with siolib to manage serial port
---------------------------------------------------------------------------*/
#ifndef T_ARM
/*---------------------------------------------------------------------------
  These names need to be kept in sync with the enums which they describe.
---------------------------------------------------------------------------*/
static char *rmnet_meta_smi_state_names[RMNET_META_SM_MAX_STATE] =
{
  "Null state",
  "Um Configuring",
  "Um up & Rm configuring",
  "Um & Rm up",
  "Rm Reconfiguring",
  "Waiting for Um to go down",
  "Waiting for Arb to complete"
};

/*
static char *rmnet_meta_smi_event_names[RMNET_META_SM_MAX_EV] =
{
  "Rm wants packet call",
  "Rm link layer is down",
  "Um interface is down",
  "Um interface is up",
  "Rm addr cfg complete",
  "Rm addr cfg failed",
  "Um Addr cfg changed",
  "Um interface is down arb"
};
*/
#endif /* T_ARM */

/*---------------------------------------------------------------------------
  RMNET RMSM command handler array
---------------------------------------------------------------------------*/
typedef void (*rmnet_meta_smi_cmd_hdlr_type)( rmnet_smi_info_type *);
static rmnet_meta_smi_cmd_hdlr_type rmnet_meta_smi_cmd_hdlr[RMNET_META_SM_MAX_EV] =
{
  rmnet_meta_smi_handle_rm_dev_start_pkt_ev,                        /* event 0 */
  rmnet_meta_smi_handle_rm_link_down_ev,                            /* event 1 */
  rmnet_meta_smi_handle_um_iface_down_ev,                           /* event 2 */
  rmnet_meta_smi_handle_um_iface_up_ev,                             /* event 3 */
  rmnet_meta_smi_handle_addr_cfg_complete_ev,                       /* event 4 */
  rmnet_meta_smi_handle_addr_cfg_failure_ev,                        /* event 5 */
  rmnet_meta_smi_handle_addr_cfg_changed_ev,                        /* event 6 */
  rmnet_meta_smi_handle_um_iface_authenticating_ev,                 /* event 7 */
  rmnet_meta_smi_handle_um_iface_arb_ev,                             /* event 8 */
  rmnet_meta_smi_handle_um_iface_bearer_tech_changed_ev             /* event 9 */
};

/*---------------------------------------------------------------------------
  Look up table to get the um_iface_ptr from LAN LLC instance
---------------------------------------------------------------------------*/
ps_iface_type * lan_llc_inst_to_um_iface_ptr_lut[LAN_LLE_MAX];


/*===========================================================================

                        EXTERNAL FUNCTION DEFINITIONS

===========================================================================*/

/*===========================================================================
  FUNCTION RMNET_SM_INIT()

  DESCRIPTION
    This function initializes the RMNET state machine.
    Called from serving task's task init function.

  PARAMETERS
    None

  FUTURE:
    iface_ptr: Pointer to the interface which is used by this instance
               of Rm state machine to bring up pkt calls.

  RETURN VALUE
    Signals to be handled by RmNet

  DEPENDENCIES
    Should only be called once at powerup

  SIDE EFFECTS
    None
===========================================================================*/
boolean rmnet_sm_init
(
  void
)
{
  rmnet_instance_e_type  i;
  uint8                  n_rmnets, j;
  rmnet_smi_cmd_type   * cmd_ptr;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  PS_INIT_CRIT_SECTION(&rmnet_crit_section);
  PS_INIT_CRIT_SECTION(&rmnet_rx_sig_crit_section);
  PS_INIT_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);

  rex_init_crit_sect(&lan_llc_inst_to_um_iface_ptr_crit_sec);

  /* Initialize dpl_ifname_by_rmnet_instance, rmnet_signals_table and
  rmnet_legacy_signals_table for max ifaces */
  for(j = 0; j < RMNET_IFACE_MAX; j++)
  {
    dpl_ifname_by_rmnet_instance[j] = DPL_IID_IFNAME_SIO_IFACE_RMNET_1 + j;
    rmnet_signals_table[j]          = RMNET_RX_SIGNAL_1 + j;
    rmnet_legacy_signals_table[j]   = RMNET_LEGACY_SIGNAL_1 + j;
  }

  /* Register command handlers with DCC Task */
  (void) dcc_set_cmd_handler(DCC_RMNET_SM_CMD, rmnet_sm_process_cmd);
  (void) dcc_set_cmd_handler(DCC_RMNET_SM_EV_CMD, rmnet_sm_process_ev_cmd);

  /*-------------------------------------------------------------------------
    Check if QOS header type size is even else the function
    ps_ul_optimized_hdlr_hget32_unaligned needs to be changed which reads
    16 bits.
  -------------------------------------------------------------------------*/
  if(((sizeof(rmnet_meta_smi_qos_hdr_type)) % 2 ) != 0)
  {
    LOG_MSG_ERROR_0("rmnet_meta_smi_qos_hdr_type not even");
    ASSERT (0);
  }

  /*-----------------------------------------------------------------------
   Register RX signal handler on provided signal & enable signal
  -----------------------------------------------------------------------*/
  (void) ps_rm_set_sig_handler( PS_RM_RMNET_RX_DATA_Q_SIGNAL,
                                (ps_rm_sig_handler_type)rmnet_ext_sig_hdlr,
                                NULL);

  ps_rm_enable_sig( PS_RM_RMNET_RX_DATA_Q_SIGNAL );

  /* We dont need RMNET definition and initialization for the reverse SIO ports */
  n_rmnets = MIN(qmi_get_num_instances(), (QMI_INSTANCE_FORWARD_PORT_MAX +1)) * 2; // 2 rmnet instances per QMI instance
  /*-------------------------------------------------------------------------
    check the composition at init time, For HS-usb with dynamic
    switching the initial composition may be mass storage but later can
    switch to Rmnet. So initialze always.
    For now only 1 instance is supported over HS-usb
    For FS-usb, we init the no of instances as per the composition
  -------------------------------------------------------------------------*/
  #ifdef FEATURE_HS_USB
    if(n_rmnets == 0)
    {
      n_rmnets = 2;
    }
  #endif

  /* For FS usb if rmnet is disabled return FALSE so that QMI is also not inited */
  if(n_rmnets == 0)
  {
    return FALSE;
  }

  for(j = 0; j < RMNET_IFACE_MAX; j++)
  {

    /*-------------------------------------------------------------------------
      Init RmNet Signals for SIO Wmk
    -------------------------------------------------------------------------*/
    rmnet_signals[j].in_use = FALSE;
    rmnet_signals[j].sig = rmnet_signals_table[j];

    /*-------------------------------------------------------------------------
      Init RmNet Legacy Signals for Legacy Wmk
    -------------------------------------------------------------------------*/
    rmnet_legacy_signals[j].in_use = FALSE;
    rmnet_legacy_signals[j].sig    = rmnet_legacy_signals_table[j];
  }

  /*-------------------------------------------------------------------------------
   Register Ext sig hdlr and init signals for packets taking Legacy Path. The sig
   handler will process packets taking legacy path in PS task.
  --------------------------------------------------------------------------------*/
  (void)ps_set_sig_handler( PS_RX_RMNET1_SIGNAL,
                            (ps_sig_handler_type)rmnet_ext_legacy_sig_hdlr,
                             NULL);
  ps_enable_sig(PS_RX_RMNET1_SIGNAL);


  /* Read the EFS configuration at bootup and cache the settings. */
  rmnet_meta_sm_get_efs_config_params(rmnet_efs_config_params);

  for(i = RMNET_INSTANCE_MIN; i < n_rmnets; i++)
  {
    if(rmnet_meta_smi_init(i) == FALSE)
    {
       return FALSE;
    }
  }

  memset(rmnet_smi_iface_info, 0, sizeof(rmnet_smi_iface_info));

#ifdef FEATURE_DATA_MBIM
/*---------------------------------------------------------------------
   * Init MBIM global params and per port info
   ---------------------------------------------------------------------*/
   /*if ((info->meta_sm.data_agg_protocol.ul_data_agg_protocol ==
           RMNET_ENABLE_DATA_AGG_MBIM) &&
       (info->meta_sm.data_format == 0)
      )*/
   mbim_call_info.mbim_rx_wmk  = NULL;
   mbim_call_info.mbim_rx_sig  = RMNET_MAX_SIGNALS;
   mbim_call_info.call_cnt     = 0;

   mbim_call_info.dual_ip_info = NULL;
   mbim_call_info.pkt_info     = NULL;
   mbim_call_info.rx_pkt       = NULL;

   //for(j = 0; j < 255; j++)
     //mbim_call_info.ips_id_index_map[j] = -1;

   for(j = 0; j < MBIM_IP_SESSION_MAX; j++)
   {
     rmnet_mbim_port_info_init(j);
   }

#endif /*FEATURE_DATA_MBIM*/

  /*-------------------------------------------------------------------------
    Initialize the rmnet_smi_cm_info and set SS roaming status to  roaming
    (by default) for autoconnect
  -------------------------------------------------------------------------*/
  memset(&rmnet_smi_cm_info, 0, sizeof(rmnet_smi_cm_info_type));
  rmnet_smi_cm_info.ss_roaming_status = SYS_ROAM_STATUS_ON;

  memset(&rmnet_smi_eri_roam_ind_info, 0,
                      sizeof(rmnet_smi_eri_roam_ind_info_type));

  if (QMI_NV_STATUS_OK != qmi_nv_file_read(QMI_NV_FILE_ROAM_IND_LIST,
                                  (void *) rmnet_smi_eri_roam_ind_info.data,
                                  RMNET_ROAM_IND_FILE_MAX_LEN,
                                  (fs_size_t*)&rmnet_smi_eri_roam_ind_info.len))
  {
    LOG_MSG_ERROR_0("Reading QMI_NV_FILE_RMNET_ROAM_IND failed");
  }

  /*-------------------------------------------------------------------------
    Initialize the RMNET CM client & register for serving system events
  -------------------------------------------------------------------------*/
  if( CM_CLIENT_OK != cm_client_init( CM_CLIENT_TYPE_RM_NETWORK_INTERFACE,
                                      &rmnet_smi_cm_info.cm_client_id) )
  {
    ERR ("WWAN interface layer initialization failed!",0,0,0);
    return FALSE;
  }

  /*-------------------------------------------------------------------------
    register call manager serving system event notification
  -------------------------------------------------------------------------*/
  if( CM_CLIENT_OK != cm_mm_client_ss_reg_msim(
                        rmnet_smi_cm_info.cm_client_id,
                        rmnet_meta_smi_cm_ss_event_cb,
                        CM_CLIENT_EVENT_REG,
                        CM_SS_EVENT_SRV_CHANGED,    /* min event to report */
                        CM_SS_EVENT_REG_REJECT,     /* max event to report */
                        NULL,                        /* other client errors */
                        SYS_MODEM_AS_ID_MASK_ANY))   /*Subs_id mask*/
  {
    LOG_MSG_ERROR_0("CM client SS event reg failed");
    return FALSE;
  }

  /*-------------------------------------------------------------------------
    Activate the registered callback functions with CM.
    Activating these by posting a DCC cmd (instead of doing in init()
    to avoid having a lot of cm events during dcc_task init time
    leading to exhaustion of DCC cmd buffers
  -------------------------------------------------------------------------*/
  cmd_ptr = (rmnet_smi_cmd_type *)RMNET_SMI_GET_CMD_BUF(DCC_RMNET_SM_CMD,FALSE);
  if(cmd_ptr != NULL)
  {
    cmd_ptr->sm_id     = RMNET_META_SM;
    cmd_ptr->cmd_type  = RMNET_SM_CMD_CM_CLIENT_ACTIVATE;
    dcc_send_cmd_ex(DCC_RMNET_SM_CMD, (void *)cmd_ptr);
  }
  else
  {
    return FALSE;
  }


#if defined(FEATURE_IP_CALL)
#if defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900)
    rmnet_meta_smi_initialize_cmipapp_info();
    if (CMIPAPP_STATUS_OK != cmipapp_id_allocate((cmipapp_call_type_e_type)CM_CALL_TYPE_NONE_MASK,
                                                 (cmipapp_sys_mode_e_type) SYS_SYS_MODE_MASK_NONE,
                                                 &rmnet_smi_cmipapp_info.client_id))
    {
      LOG_MSG_ERROR_0("CM IP App client alloc failed");
      return FALSE;
    }

    if (CMIPAPP_STATUS_OK != cmipapp_reg_oprt_mode_func(
                               rmnet_smi_cmipapp_info.client_id,
                               (cmipapp_inform_oprt_mode_f_type*)&rmnet_meta_smi_oprt_mode_change_cb))
    {
      LOG_MSG_ERROR_0("CM IP App client oprt mode reg failed");
      return FALSE;
    }
#endif /* defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900) */
#endif /* defined(FEATURE_IP_CALL) */


#if defined(FEATURE_RMNET_DATA_AGG_TIMER) && defined(FEATURE_CPU_BASED_FLOW_CONTROL)
  // Set FC signal handler
  (void)dcc_set_sig_handler(
                      DCC_RMNET_FC_SIGNAL,
                      rmnet_meta_sm_process_fc_sig,
                      NULL );
  dcc_enable_sig( DCC_RMNET_FC_SIGNAL );

  // Register with FC
  memset( &rmnet_fc_cmd_q, 0, sizeof(rmnet_fc_cmd_q) );
  (void)q_init( &rmnet_fc_cmd_q );
  if ( rmnet_meta_smi_fc_register() != TRUE )
  {
    LOG_MSG_ERROR_0("Failed to register with Flow Control (FC) module");
  }
#endif /* FEATURE_RMNET_DATA_AGG_TIMER && FEATURE_CPU_BASED_FLOW_CONTROL */

  ds_qmap_init();

  return TRUE;
} /* rmnet_sm_init() */



/*===========================================================================
  FUNCTION RMNET_META_SM_POST_EVENT()

  DESCRIPTION
    This function posts an event to RMNetSM and enqueues the
    corresponding command to be processed in the DS task.

  PARAMETERS
    instance:  Instance of RmNetSm to post event to
    event: The event that is being posted to the state machine.

  RETURN VALUE
    None

  DEPENDENCIES
    RM State Machine should have already been initialized.

  SIDE EFFECTS
    None
===========================================================================*/
void rmnet_meta_sm_post_event
(
  rmnet_instance_e_type      instance,
  rmnet_meta_sm_event_type   event
)
{
  rmnet_smi_ev_cmd_type * cmd_ptr;
  rmnet_smi_info_type   * info;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(instance < RMNET_INSTANCE_MAX);

  info = &rmnet_smi_info[instance];

  if (event >= RMNET_META_SM_MAX_EV)
  {
    LOG_MSG_ERROR_1("Invalid RMNET_META_SM event %d, ignored", event);
    ASSERT (0);
    return;
  }

#ifdef T_ARM
  LOG_MSG_INFO1_3( "RMNET inst %d, Event %d posted to RMNET_META_SM in state %d",
                   instance, event, info->meta_sm.state );
#else
  MSG_SPRINTF_3(MSG_SSID_DS_PS, MSG_LEGACY_HIGH,
                "RMNET inst %d, Event %d posted to RMNET_META_SM in state %d",
                instance, event, info->meta_sm.state);
#endif

  /*-------------------------------------------------------------------------
    Get a DCC command buffer
  -------------------------------------------------------------------------*/
  cmd_ptr = (rmnet_smi_ev_cmd_type *)RMNET_SMI_GET_CMD_BUF(DCC_RMNET_SM_EV_CMD,FALSE);
  if( cmd_ptr == NULL )
  {
    return;
  }

  /*-------------------------------------------------------------------------
    Post cmd to DCC task corresponding to each event
  -------------------------------------------------------------------------*/
  cmd_ptr->sm_id             = RMNET_META_SM;
  cmd_ptr->event             = event;
  cmd_ptr->rmnet_sm_info_ptr = (void *) info;

  dcc_send_cmd_ex(DCC_RMNET_SM_EV_CMD, (void *)cmd_ptr);

} /* rmnet_meta_sm_post_event() */


/*===========================================================================
  FUNCTION RMNET_META_SM_PROCESS_EV_CMD()

  DESCRIPTION
    This function processes a RmNet RmSM event cmd.

    It is called by the serving task main loop and will dispatch the
    associated event handler function.

  PARAMETERS
    cmd_ptr:  serving task command buffer containing the RmNet sm
              event information

  RETURN VALUE
    None

  DEPENDENCIES
    RM State Machine should have already been initialized

  SIDE EFFECTS
    None
===========================================================================*/
void rmnet_meta_sm_process_ev_cmd
(
  dcc_cmd_enum_type cmd,
  void * user_data_ptr
)
{
  rmnet_smi_info_type      * info;
  rmnet_meta_sm_event_type   event;
  rmnet_smi_ev_cmd_type    * cmd_ptr;
  rmnet_instance_e_type      rmnet_inst;
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  cmd_ptr = (rmnet_smi_ev_cmd_type *)user_data_ptr;
  ASSERT (cmd_ptr && (cmd == DCC_RMNET_SM_EV_CMD) && 
                      (cmd_ptr->sm_id == RMNET_META_SM));

  event = (rmnet_meta_sm_event_type) cmd_ptr->event;
  info  = (rmnet_smi_info_type *)
          cmd_ptr->rmnet_sm_info_ptr;
  rmnet_inst = RMNET_META_SM_INFO_TO_INSTANCE(info);

  LOG_MSG_INFO2_3( "RmNetSm event %d in state %d, RMNET inst %d",
                   event, info->meta_sm.state, rmnet_inst);

  /*-------------------------------------------------------------------------
    Verify that RMNET_META_SM is initialized
  -------------------------------------------------------------------------*/
  if(info->meta_sm.inited != TRUE)
  {
    ASSERT(0);
    LOG_MSG_ERROR_1("Can't process event %d, RMNET_META_SM not inited", event);
    return;
  }

  /*-------------------------------------------------------------------------
    Validate posted event before processing
  -------------------------------------------------------------------------*/
  if (event >= RMNET_META_SM_MAX_EV)
  {
    ASSERT(0);
    LOG_MSG_ERROR_1("Invald RMNET_META_SM event %d, ignoring", event );
    return;
  }

  /*-------------------------------------------------------------------------
    Validate RMNET_META_SM state before processing event - this allows event handler
    functions to assume that state is valid.
  -------------------------------------------------------------------------*/
  if (info->meta_sm.state >= RMNET_META_SM_MAX_STATE)
  {
    ASSERT( 0 );
    LOG_MSG_ERROR_1("Invalid RMNET_META_SM state %d", info->meta_sm.state);
    return;
  }

  /*-------------------------------------------------------------------------
    Invoke associated command handler function
  -------------------------------------------------------------------------*/
  (*(rmnet_meta_smi_cmd_hdlr[event]))(info);

} /* rmnet_meta_sm_process_ev_cmd() */

/*===========================================================================
  FUNCTION RMNET_META_SM_PROCESS_CMD()

  DESCRIPTION
    This function processes a RmNet Meta SM cmd.

  PARAMETERS
    cmd_ptr:  serving task command buffer containing the RmNet sm
              event information

  RETURN VALUE
    None

  DEPENDENCIES
    RM State Machine should have already been initialized

  SIDE EFFECTS
    None
===========================================================================*/
void rmnet_meta_sm_process_cmd
(
  dcc_cmd_enum_type    cmd,
  void                *user_data_ptr
)
{
  rmnet_smi_info_type *  info;
  rmnet_smi_info_type *  info_v6;
  rmnet_instance_e_type  rmnet_instance;
  rmnet_smi_cmd_type   * cmd_ptr;
  rmnet_smi_dual_ip_info_type * dual_ip_info;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  cmd_ptr = (  rmnet_smi_cmd_type *)user_data_ptr;
  ASSERT (cmd_ptr && (cmd == DCC_RMNET_SM_CMD) && 
                (cmd_ptr->sm_id == RMNET_META_SM));

  switch(cmd_ptr->cmd_type)
  {
    case RMNET_SM_CMD_DTR_CHANGED:
      dual_ip_info = (rmnet_smi_dual_ip_info_type *)cmd_ptr->info_ptr;
      ASSERT(dual_ip_info);

      cmd_ptr->info_ptr = dual_ip_info->info_v4;
      rmnet_meta_smi_process_dtr_changed_cmd(cmd, cmd_ptr);

      cmd_ptr->info_ptr = dual_ip_info->info_v6;
      rmnet_meta_smi_process_dtr_changed_cmd(cmd, cmd_ptr);
      break;

    case RMNET_SM_CMD_TX_FLOW_ENABLE:
      rmnet_instance = (rmnet_instance_e_type)((int) cmd_ptr->info_ptr);

      ASSERT((rmnet_instance + 1) < RMNET_INSTANCE_MAX);


      info = &rmnet_smi_info[rmnet_instance];

     /*---------------------------------------------------------------------
        Enable outgoing data on the Rm interface.
        The air interface should check if ps_iface flow is enabled before
        sending data on to this interface.
      ---------------------------------------------------------------------*/
      if(info->rmnet_iface_ptr != NULL)
      {
        ps_iface_enable_flow(info->rmnet_iface_ptr, DS_FLOW_RMSM_MASK);
      }

      info_v6 = &rmnet_smi_info[rmnet_instance+1];
      if(info_v6->rmnet_iface_ptr != NULL)
      {
        ps_iface_enable_flow(info_v6->rmnet_iface_ptr, DS_FLOW_RMSM_MASK);
      }
      break;

    case RMNET_SM_CMD_TX_FLOW_DISABLE:
      rmnet_instance = (rmnet_instance_e_type) (int) cmd_ptr->info_ptr;

      ASSERT((rmnet_instance + 1) < RMNET_INSTANCE_MAX);

      info = &rmnet_smi_info[rmnet_instance];
      if(info->rmnet_iface_ptr != NULL)
      {
        ps_iface_disable_flow(info->rmnet_iface_ptr, DS_FLOW_RMSM_MASK);
      }

      info_v6 = &rmnet_smi_info[rmnet_instance+1];
      if(info_v6->rmnet_iface_ptr != NULL)
      {
        ps_iface_disable_flow(info_v6->rmnet_iface_ptr, DS_FLOW_RMSM_MASK);
      }
      break;

    case RMNET_SM_CMD_TLP_CHANGE:
      rmnet_meta_smi_tlp_change_cmd_hdlr(
        (rmnet_sio_tlp_ver_type) cmd_ptr->data.tlp_change.dl_ver,
        (rmnet_sio_tlp_ver_type) cmd_ptr->data.tlp_change.ul_ver,
        cmd_ptr->info_ptr );
      break;

    case RMNET_SM_CMD_CM_CLIENT_ACTIVATE:
      if (!rmnet_meta_smi_reg_cm())
      {
        LOG_MSG_INFO1_0("rmnet_meta_smi_reg_cm() failed");
      }
      break;

    case RMNET_SM_CMD_NEW_CM_SS_INFO:
      rmnet_meta_smi_process_cm_ss_event(cmd_ptr->data.cm_ss.event,
                                         cmd_ptr->data.cm_ss.info,
                                         cmd_ptr->data.cm_ss.asubs_id);
      break;

#if defined(FEATURE_IP_CALL)
#if defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900)
     case RMNET_SM_CMD_CM_OP_MODE_CHANGE:
      rmnet_meta_smi_cm_op_mode_change_cmd_hdlr (cmd_ptr);
      break;
#endif /* defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900) */
#endif /* defined(FEATURE_IP_CALL) */

    case RMNET_SM_CMD_AUTOCONNECT_TIMER:
      rmnet_meta_smi_process_autoconnect_timer_cmd_hdlr(cmd_ptr->info_ptr);
    break;

    case RMNET_SM_CMD_PROCESS_PHYS_LINK_UP_DOWN_EVT:
      rmnet_meta_smi_process_phys_link_up_down_event(
                           cmd_ptr->data.phys_link_status.phys_link_ptr,
                           cmd_ptr->data.phys_link_status.event,
                           cmd_ptr->data.phys_link_status.event_info,
                           cmd_ptr->data.phys_link_status.user_data_ptr);

    break;

    default:
      LOG_MSG_INFO1_1("Received Unhandled rmnet_meta_sm cmd %d",
                      cmd_ptr->cmd_type);
      ASSERT(0);
      break;
  }
} /* rmnet_meta_sm_process_cmd() */


/*===========================================================================
  FUNCTION RMNET_META_SM_IN_CALL()

  DESCRIPTION
    Returns true if we're in a packet call.  Called by ws_if.c

  PARAMETERS
    instance - the RMNET Rm interface to query

  RETURN VALUE
    TRUE  - Rm interface 'instance' is starting/in/stopping a packet call
    FALSE - Rm interface 'instance' is idle

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean rmnet_meta_sm_in_call
(
  rmnet_instance_e_type  instance
)
{
  rmnet_smi_info_type *  info;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(instance < RMNET_INSTANCE_MAX);
  info = &rmnet_smi_info[instance];

  /*-------------------------------------------------------------------------
    We are in a packet call if rmsm state is non-null
// TODO:  check in_use boolean - no longer have tasklocks during small
//       window where still in NULL state while handling rm_start_dev
  -------------------------------------------------------------------------*/
  return (info->meta_sm.state == RMNET_META_SM_NULL_STATE) ? FALSE : TRUE;

} /* rmnet_meta_sm_in_call() */


/*===========================================================================
  FUNCTION RMNET_META_SM_IFACE()

  DESCRIPTION
    Returns the ps_iface controlled by the given RMNET RmSM instance

  PARAMETERS
    instance - the RMNET RmSM instance to query

  RETURN VALUE
    ps_iface *  - Pointer to the Rm ps_iface controlled by the spec'd RmSM
    NULL        - RmNetSM not initialized

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
ps_iface_type * rmnet_meta_sm_iface
(
  rmnet_instance_e_type  instance
)
{
  rmnet_smi_info_type *  info;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(instance < RMNET_INSTANCE_MAX);
  info = &rmnet_smi_info[instance];

  return info->rmnet_iface_ptr;
} /* rmnet_meta_sm_iface() */


/*===========================================================================
FUNCTION RMNET_META_SM_SET_NETWORK_CFG_PARAMS

DESCRIPTION
  This function is called to set the parameters received in Start Network
  Interface for network configuration in the rmnet info structure.

PARAMETERS
  instance       : RmNet instance
  net_cfg_params : Network Configuration parameters

DEPENDENCIES
  None

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
void rmnet_meta_sm_set_network_cfg_params
(
  rmnet_instance_e_type      instance,
  rmnet_meta_sm_network_info_type *  net_cfg_params
)
{
  rmnet_smi_info_type *  info;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(instance < RMNET_INSTANCE_MAX);
  info = &rmnet_smi_info[instance];

  memscpy(&(info->meta_sm.net_params),
          sizeof(rmnet_meta_sm_network_info_type),
          net_cfg_params,
          sizeof(rmnet_meta_sm_network_info_type));

} /* rmnet_meta_sm_set_network_cfg_params() */


/*===========================================================================
  FUNCTION RMNET_SM_GET_UM_IFACE()

  DESCRIPTION
    Returns the UM ps_iface ptr for the given RmNetSM instance

  PARAMETERS
    instance - the RmSM instance to query

  RETURN VALUE
    NULL        - Currently out of call
    ps_iface *  - Pointer to the UM ps_iface controlled by the spec'd RmSM

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
ps_iface_type * rmnet_meta_sm_get_um_iface
(
  rmnet_instance_e_type  instance
)
{
  rmnet_smi_info_type *  info;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(instance < RMNET_INSTANCE_MAX);
  info = &rmnet_smi_info[instance];

  return (info->um_iface_ptr);
} /* rmnet_sm_get_um_iface() */

/*===========================================================================
  FUNCTION RMNET_META_SM_GET_UM_IFACE_EX()

  DESCRIPTION
    Returns the UM ps_iface ptr for the given RmNetSM instance. If not
    available, gets it from net params.

  PARAMETERS
    instance - the RmSM instance to query

  RETURN VALUE
    NULL        - Currently out of call
    ps_iface *  - Pointer to the UM ps_iface controlled by the spec'd RmSM

  DEPENDENCIES
    This function is expected to be called only after net_param is set.

  SIDE EFFECTS
    None
===========================================================================*/
ps_iface_type * rmnet_meta_sm_get_um_iface_ex
(
  rmnet_instance_e_type  instance
)
{
  rmnet_smi_info_type        * info = NULL;
  rmnet_meta_sm_arb_info_type  arb_info = {0};
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (instance < RMNET_INSTANCE_MAX)
  {
    info = &rmnet_smi_info[instance];
  }

  if (info == NULL)
  {
    return NULL;
  }

  if (info->um_iface_ptr != NULL)
  {
    return info->um_iface_ptr;
  }

  return rmnet_meta_smi_get_um_iface_ptr(
                       &info->meta_sm.net_params,
                       RMNET_RT_LK_UP_TYPE_IFACE,
                       NULL,
                       instance,
                       &arb_info);

} /* rmnet_meta_sm_get_um_iface_ex() */


/*===========================================================================
  FUNCTION RMNET_META_SM_IS_AUTO_CONNECT_ENABLED()

  DESCRIPTION
    Returns whether auto_connect is enabled on this instance or not

  PARAMETERS
    instance - the RmNet instance

  RETURN VALUE
    TRUE  - auto connect is enabled
    FALSE - auto connect is disabled

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean rmnet_meta_sm_is_auto_connect_enabled
(
  rmnet_instance_e_type  instance
)
{
  rmnet_smi_info_type *  info;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(instance < RMNET_INSTANCE_MAX);
  info = &rmnet_smi_info[instance];

  LOG_MSG_INFO1_2(" rmnet_meta_sm_is_auto_connect_enabled INST [%d], Auto [%d]",
                  instance, info->meta_sm.settings.auto_connect_enabled );
  return(info->meta_sm.settings.auto_connect_enabled);
} /* rmnet_meta_sm_is_auto_connect_enabled() */

/*===========================================================================
  FUNCTION RMNET_META_SM_ENABLE_AUTO_CONNECT()

  DESCRIPTION
    Enable Autoconnect

  PARAMETERS
    instance - the RmNet instance

  RETURN VALUE
    TRUE  - auto connect is enabled
    FALSE - auto connect is disabled

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean rmnet_meta_sm_enable_auto_connect
(
  rmnet_instance_e_type  instance,
  boolean                enable
)
{
  rmnet_meta_sm_autoconnect_setting_e_type  autoconnect_setting;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /* convert boolean to auto connect setting */
  autoconnect_setting =
          enable ? RMNET_AUTOCONNECT_ENABLED : RMNET_AUTOCONNECT_DISABLED;

  /*-----------------------------------------------------------------------
    Calling new function to set autoconnect settings.
  -----------------------------------------------------------------------*/
  return(rmnet_meta_sm_set_autoconnect_setting(instance, autoconnect_setting));
} /* rmnet_meta_sm_enable_auto_connect() */

/*===========================================================================
  FUNCTION RMNET_META_SM_SET_AUTOCONNECT_SETTING()

  DESCRIPTION
    set autoconnect setting

  PARAMETERS
    instance - the RmNet instance
    autoconnect_setting  - the autoconnect value

  RETURN VALUE
    TRUE  - set autoconnect setting is successful
    FALSE - set autoconnect setting is unsuccessful

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean rmnet_meta_sm_set_autoconnect_setting
(
  rmnet_instance_e_type                      instance,
  rmnet_meta_sm_autoconnect_setting_e_type   autoconnect_setting
)
{
  rmnet_smi_info_type *  info;
  nv_item_type        *   dcc_nv_item_ptr;

  qmi_nv_status_e_type          qmi_nv_status;
  qmi_nv_autoconnect_pause_type qmi_nv_autoconnect_pause;
  boolean                       autoconnect_nv_write = FALSE;
  boolean                       pause_nv_write       = FALSE;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(instance < RMNET_INSTANCE_MAX);
  info = &rmnet_smi_info[instance];
  memset(&qmi_nv_autoconnect_pause, 0, sizeof(qmi_nv_autoconnect_pause_type));

  /*-------------------------------------------------------------------------
    Allocate temporary memory for the NV item
  -------------------------------------------------------------------------*/
  dcc_nv_item_ptr = (nv_item_type *)
    qmi_svc_ps_system_heap_mem_alloc(sizeof(nv_item_type),FILE_ID_DS_RMNET_META_SM,__LINE__);
  if( dcc_nv_item_ptr == NULL )
  {
    PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_item_ptr);
    ASSERT(0);
  }

  if(info->meta_sm.settings.autoconnect_setting != autoconnect_setting)
  {
    /*-----------------------------------------------------------------------
      read autoconnect NV(3534) and autoconnect pause QMI NV to get currently
      stored valuse.
    -----------------------------------------------------------------------*/
    qmi_nv_autoconnect_pause.index = instance/2;
    qmi_nv_status = qmi_nv_read(QMI_NV_ITEM_AUTOCONNECT_PAUSE_SETTING,
                                qmi_nv_autoconnect_pause.index,
                                &qmi_nv_autoconnect_pause,
                                sizeof(qmi_nv_autoconnect_pause_type));

    dcc_nv_item_ptr->rmnet_autoconnect.index =
      RMNET_GET_LAPTOP_INSTANCE(instance);

    if (NV_DONE_S != dcc_get_nv_item(NV_RMNET_AUTOCONNECT_I, dcc_nv_item_ptr))
    {
      dcc_nv_item_ptr->rmnet_autoconnect.enable = FALSE;
    }

    /*-----------------------------------------------------------------------
    For each autoconnect setting, there might be two NV writes. one is legacy
    NV item 3534 and new  QMI NVautoconnect pause. See below TT table for
    various combination to each autoconnect setting. Here 3534 should be active.

    T = TRUE, F = FALSE, N/A = Non Active, None = No NV write.
    -------------------------------------------------------------------------
    New          Currently  Currently
    autoconnect  stored     stored
    setting      NV3534     pause NV
    Need to set  value      value             Result NV settings.
    -------------------------------------------------------------------------
    Disabled      T      N/A or 0          write 3534 = F
    (0)         -------------------------------------------------------------
                  T       1                write 3534 = F and pause NV =0
                -------------------------------------------------------------
                  F      N/A or 0          None
                -------------------------------------------------------------
                  F      1                 write pause NV =0
    -------------------------------------------------------------------------
    Enabled       T      N/A or 0          None
    (1)         -------------------------------------------------------------
                  T       1                write pause NV =0
                -------------------------------------------------------------
                  F      N/A or 0          write 3534 = T
                -------------------------------------------------------------
                  F      1                 write 3534 = T and pause NV =0
    -------------------------------------------------------------------------
    Paused        T      N/A or 0          write 3534 = F and pause NV = 1
    (2)         -------------------------------------------------------------
                  T       1                write 3534 = F
                -------------------------------------------------------------
                  F      N/A or 0          write pause NV =1
                -------------------------------------------------------------
                  F      1                 None
    -------------------------------------------------------------------------*/

    /* from the above table Row 1, 2, 9 and 10 taken care of */
    if(dcc_nv_item_ptr->rmnet_autoconnect.enable)
    {
      if((RMNET_AUTOCONNECT_DISABLED == autoconnect_setting) ||
         (RMNET_AUTOCONNECT_PAUSED   == autoconnect_setting))
      {
        autoconnect_nv_write = TRUE;
        dcc_nv_item_ptr->rmnet_autoconnect.enable = FALSE;
      }
    }
    /* from the above table Row 7, and 8 taken care of */
    else
    {
      if(RMNET_AUTOCONNECT_ENABLED == autoconnect_setting)
      {
        autoconnect_nv_write = TRUE;
        dcc_nv_item_ptr->rmnet_autoconnect.enable = TRUE;
      }
    }

    if (QMI_NV_STATUS_OK == qmi_nv_status)
    {
      /* from the above table Row 2, 4 and 8 taken care of */
      if(((RMNET_AUTOCONNECT_DISABLED == autoconnect_setting)  ||
          (RMNET_AUTOCONNECT_ENABLED  == autoconnect_setting)) &&
         (RMNET_AUTOCONNECT_PAUSE_ON  == qmi_nv_autoconnect_pause.pause_setting))
      {
        pause_nv_write = TRUE;
        qmi_nv_autoconnect_pause.pause_setting = RMNET_AUTOCONNECT_PAUSE_OFF;
      }
      /* from the above table Row 9, and 11 taken care of */
      else if((RMNET_AUTOCONNECT_PAUSED    == autoconnect_setting) &&
              (RMNET_AUTOCONNECT_PAUSE_OFF == qmi_nv_autoconnect_pause.pause_setting))
      {
        pause_nv_write = TRUE;
        qmi_nv_autoconnect_pause.pause_setting = RMNET_AUTOCONNECT_PAUSE_ON;
      }
    }
    /* from the above table Row 9, and 11 taken care of(non active case) */
    else if(RMNET_AUTOCONNECT_PAUSED == autoconnect_setting)
    {
      pause_nv_write = TRUE;
      qmi_nv_autoconnect_pause.pause_setting = RMNET_AUTOCONNECT_PAUSE_ON;
    }

    /*-------------------------------------------------------------------------
      Write pause NV and NV 3534
    -------------------------------------------------------------------------*/
    if (autoconnect_nv_write)
    {
      if( NV_DONE_S != dcc_put_nv_item(NV_RMNET_AUTOCONNECT_I, dcc_nv_item_ptr) )
      {
        return FALSE;
      }
    }

    if (pause_nv_write)
    {
      qmi_nv_status = qmi_nv_write(QMI_NV_ITEM_AUTOCONNECT_PAUSE_SETTING,
                                   qmi_nv_autoconnect_pause.index,
                                   &qmi_nv_autoconnect_pause,
                                   sizeof(qmi_nv_autoconnect_pause_type));

      if (QMI_NV_STATUS_OK != qmi_nv_status)
      {
        LOG_MSG_ERROR_1("Couldn't write pause NV item (%d)", qmi_nv_status);
        return FALSE;
      }
    }

    /* update the new auto connect setting to cache */
    info->meta_sm.settings.autoconnect_setting = autoconnect_setting;

    /* update the new auto connect setting to efs parameter */
    if ( RMNET_IS_VALID_LAPTOP_INSTANCE(instance))
    {
      rmnet_efs_config_params[RMNET_GET_LAPTOP_INSTANCE(instance)].autoconnect
         = autoconnect_setting;
    }
    else
      return FALSE;

    /*-------------------------------------------------------------------------
      Determine the new autoconnect state(Enable or disable) based on
      new settings
    -------------------------------------------------------------------------*/
    rmnet_meta_smi_update_autoconnect_state(instance);
  }

  PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_item_ptr);

  return TRUE;
} /* rmnet_meta_sm_set_autoconnect_setting() */

/*===========================================================================
  FUNCTION RMNET_META_SM_GET_AUTOCONNECT_SETTING()

  DESCRIPTION
    get autoconnect setting

  PARAMETERS
    instance - the RmNet instance

  RETURN VALUE
    uint8  - auto connect setting value

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
rmnet_meta_sm_autoconnect_setting_e_type rmnet_meta_sm_get_autoconnect_setting
(
  rmnet_instance_e_type  instance
)
{
  rmnet_smi_info_type *  info;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(instance < RMNET_INSTANCE_MAX);
  info = &rmnet_smi_info[instance];

  /* return autoconnect setting from cache */
  return (info->meta_sm.settings.autoconnect_setting) ;
} /* rmnet_meta_sm_get_autoconnect_setting() */

/*===========================================================================
  FUNCTION RMNET_META_SM_SET_AUTOCONNECT_ROAM_SETTING()

  DESCRIPTION
    set autoconnect roam setting

  PARAMETERS
    instance      - the RmNet instance
    roam_setting  - the autoconnect roam value

  RETURN VALUE
    TRUE  - set autoconnect roam setting is successful
    FALSE - set autoconnect roam setting is unsuccessful

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean rmnet_meta_sm_set_autoconnect_roam_setting
(
  rmnet_instance_e_type                           instance,
  rmnet_meta_sm_autoconnect_roam_setting_e_type   roam_setting

)
{
  rmnet_smi_info_type *  info;

  qmi_nv_status_e_type                 qmi_nv_status;
  qmi_nv_autoconnect_roam_setting_type qmi_nv_roam_setting;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(instance < RMNET_INSTANCE_MAX);
  info = &rmnet_smi_info[instance];

  memset(&qmi_nv_roam_setting, 0, sizeof(qmi_nv_autoconnect_roam_setting_type));

  if(info->meta_sm.settings.autoconnect_roam_setting != roam_setting)
  {
    /* update the new values to NV */
    qmi_nv_roam_setting.index = instance/2;
    qmi_nv_roam_setting.roam_setting = roam_setting;
    qmi_nv_status = qmi_nv_write(QMI_NV_ITEM_AUTOCONNECT_ROAM_SETTING,
                                 qmi_nv_roam_setting.index,
                                 &qmi_nv_roam_setting,
                                 sizeof(qmi_nv_autoconnect_roam_setting_type));
    if(QMI_NV_STATUS_OK != qmi_nv_status )
    {
      LOG_MSG_ERROR_1("Couldn't write QMI_NV_ITEM_WDS_AUTOCONNECT_ROAM_SETTING (%d)",
                      qmi_nv_status);
      return FALSE;
    }

    /* update the new autoconnect roam setting to cache */
    info->meta_sm.settings.autoconnect_roam_setting =
                                              qmi_nv_roam_setting.roam_setting;

    /*-------------------------------------------------------------------------
      Determine the new autoconnect state(Enable or disable)
      based on new settings
    -------------------------------------------------------------------------*/
    rmnet_meta_smi_update_autoconnect_state(instance);
  }

  return TRUE;
} /* rmnet_meta_sm_set_autoconnect_roam_setting() */

/*===========================================================================
  FUNCTION RMNET_META_SM_SET_LINK_PROT_SETTING()

  DESCRIPTION
    Sets link protocol setting for autoconnect

  PARAMETERS
    instance      - the RmNet instance
    link_prot     - the link protocol type.

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean rmnet_meta_sm_set_link_prot_setting
(
  rmnet_instance_e_type      instance,
  rmnet_sm_link_prot_e_type  link_prot
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(instance < RMNET_INSTANCE_MAX);

  if ( RMNET_IS_VALID_LAPTOP_INSTANCE(instance))
  {
    rmnet_efs_config_params[RMNET_GET_LAPTOP_INSTANCE(instance)].link_prot
       = link_prot;
    return TRUE;
  }
  else
    return FALSE;
}

/*===========================================================================
  FUNCTION RMNET_META_SM_SET_UL_TLP_SETTING()

  DESCRIPTION
    Sets UL TLP setting for autoconnect

  PARAMETERS
    instance        - the RmNet instance
    ul_tlp_enabled  - ul tlp enabled or disabled

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean rmnet_meta_sm_set_ul_tlp_setting
(
  rmnet_instance_e_type                instance,
  rmnet_data_agg_enum_type  ul_tlp_enabled
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(instance < RMNET_INSTANCE_MAX);

  if ( RMNET_IS_VALID_LAPTOP_INSTANCE(instance))
  {
    rmnet_efs_config_params[RMNET_GET_LAPTOP_INSTANCE(instance)].ul_tlp_enabled
       = ul_tlp_enabled;
    return TRUE;
  }
  else
    return FALSE;
}

/*===========================================================================
  FUNCTION RMNET_PARSER_APPEND_INDEX()

  DESCRIPTION
    This function appends the tag info at the end of the buffer.

  PARAMETERS
    index         - tag index
    instance      - the RmNet instance
    buff          - buffer
    buff_len      - total buffer size

  RETURN VALUE
    TRUE  - If successfully appended at the end of the buffer
    FALSE - If error in appending at the end of the buffer

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
int rmnet_parser_append_index
(
  rmnet_parser_tag_index_e_type  index,
  uint8                          instance,
  char                           **buff,
  int                            buff_len
)
{
  uint8 lap_instance = 0;
  char temp_str[10];
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(instance < RMNET_INSTANCE_MAX);

  if((buff == NULL) || (*buff == NULL))
  {
    LOG_MSG_INFO2_0("Buff ptr passed is NUll ");
    return -1;
  }

  strlcat(*buff, rmnet_parser_start_tag_name[index], buff_len);

  lap_instance = RMNET_GET_LAPTOP_INSTANCE(instance);

  switch (index)
  {
    case RMNET_PARSER_TAG_INSTANCE:
      dsatutil_itoa(lap_instance+1,(byte*)temp_str,10);
      strlcat(*buff, temp_str, buff_len);
    break;

    case RMNET_PARSER_TAG_LINK_PROT:
    if(rmnet_efs_config_params[lap_instance].link_prot == RMNET_ETHERNET_MODE)
    {
      strlcat(*buff,"ETH",buff_len);
    }
    else if (rmnet_efs_config_params[lap_instance].link_prot == RMNET_IP_MODE)
    {
      strlcat(*buff,"IP",buff_len);
    }
    else
    {
      strlcat(*buff,"Invalid",buff_len);
    }
//    LOG_MSG_INFO2_0("Written Link Prot!!");
    break;

    case RMNET_PARSER_TAG_QOS:
    if(rmnet_efs_config_params[lap_instance].qos_enabled == TRUE)
    {
      strlcat(*buff,"Enabled",buff_len);
    }
    else
    {
      strlcat(*buff,"Disabled",buff_len);
    }
//    LOG_MSG_INFO2_0("Written QOS!!");
    break;

    case RMNET_PARSER_TAG_UL_TLP:
    if(rmnet_efs_config_params[lap_instance].ul_tlp_enabled == RMNET_ENABLE_DATA_AGG_TLP)
    {
      strlcat(*buff,"Enabled",buff_len);
    }
    else if(rmnet_efs_config_params[lap_instance].ul_tlp_enabled == RMNET_ENABLE_DATA_AGG_QC_NCM)
    {
      strlcat(*buff,"ul_qc_ncm_enabled",buff_len);
    }
    #ifdef FEATURE_DATA_MBIM
    else if(rmnet_efs_config_params[lap_instance].ul_tlp_enabled == RMNET_ENABLE_DATA_AGG_MBIM)
    {
      strlcat(*buff,"ul_mbim_enabled",buff_len);
    }
    #endif
    else if(rmnet_efs_config_params[lap_instance].ul_tlp_enabled == RMNET_ENABLE_DATA_AGG_RNDIS)
    {
      strlcat(*buff,"ul_rndis_enabled",buff_len);
    }
    else if(rmnet_efs_config_params[lap_instance].ul_tlp_enabled == RMNET_ENABLE_DATA_AGG_QMAP)
    {
      strlcat(*buff,"ul_qmap_enabled",buff_len);
    }
#ifdef FEATURE_DATA_QMAP_DEBUG
    #error code not present
#endif
    else
    {
      strlcat(*buff,"Disabled",buff_len);
    }
//    LOG_MSG_INFO2_0("Written UL TLP!!");
    break;

    case RMNET_PARSER_TAG_AUTOCONNECT:
    if(rmnet_efs_config_params[lap_instance].autoconnect == RMNET_AUTOCONNECT_ENABLED)
    {
      strlcat(*buff,"Enabled",buff_len);
    }
    else
    {
      strlcat(*buff,"Disabled",buff_len);
    }
//    LOG_MSG_INFO2_0("Written Autoconnect!!");
    break;

    case RMNET_PARSER_TAG_IP_TYPE:
    if(rmnet_efs_config_params[lap_instance].ip_type == RMNET_ADDR_TYPE_IPV4)
    {
      strlcat(*buff,"IPV4",buff_len);
    }
    else if(rmnet_efs_config_params[lap_instance].ip_type == RMNET_ADDR_TYPE_IPV6)
    {
      strlcat(*buff,"IPV6",buff_len);
    }
    else if(rmnet_efs_config_params[lap_instance].ip_type == RMNET_ADDR_TYPE_IPV4V6)
    {
      strlcat(*buff,"IPV4V6",buff_len);
    }
    else
    {
      strlcat(*buff,"Invalid",buff_len);
    }
//    LOG_MSG_INFO2_0("Written Family Pref!!");
    break;

    case RMNET_PARSER_TAG_UMTS_PROFILE:
      dsatutil_itoa(rmnet_efs_config_params[lap_instance].umts_profile,(byte*)temp_str,10);
      strlcat(*buff,temp_str,buff_len);
    break;

    case RMNET_PARSER_TAG_CDMA_PROFILE:
      dsatutil_itoa(rmnet_efs_config_params[lap_instance].cdma_profile,(byte*)temp_str,10);
      strlcat(*buff,temp_str,buff_len);
    break;

    case RMNET_PARSER_TAG_DL_DATA_AGG:
    if(rmnet_efs_config_params[lap_instance].dl_data_agg_protocol == RMNET_ENABLE_DATA_AGG_TLP)
    {
      strlcat(*buff,"dl_tlp_enabled",buff_len);
    }
    else if(rmnet_efs_config_params[lap_instance].dl_data_agg_protocol == RMNET_ENABLE_DATA_AGG_QC_NCM)
    {
      strlcat(*buff,"dl_qc_ncm_enabled",buff_len);
    }
    #ifdef FEATURE_DATA_MBIM
    else if(rmnet_efs_config_params[lap_instance].dl_data_agg_protocol == RMNET_ENABLE_DATA_AGG_MBIM)
    {
      strlcat(*buff,"dl_mbim_enabled",buff_len);
    }
    #endif
    else if(rmnet_efs_config_params[lap_instance].dl_data_agg_protocol == RMNET_ENABLE_DATA_AGG_RNDIS)
    {
      strlcat(*buff,"dl_rndis_enabled",buff_len);
    }
    else if(rmnet_efs_config_params[lap_instance].dl_data_agg_protocol == RMNET_ENABLE_DATA_AGG_QMAP)
    {
      strlcat(*buff,"dl_qmap_enabled",buff_len);
    }
#ifdef FEATURE_DATA_QMAP_DEBUG
    #error code not present
#endif
    else
    {
      strlcat(*buff,"Disabled",buff_len);
    }
//    LOG_MSG_INFO2_0("Written DL Data Agg!!");
    break;

    case RMNET_PARSER_TAG_PROC_ID:
      if(rmnet_efs_config_params[lap_instance].proc_id == RMNET_PROC_ID_APPS1)
      {
        strlcat(*buff,"apps",buff_len);
      }
      else if(rmnet_efs_config_params[lap_instance].proc_id == RMNET_PROC_ID_LAPTOP1)
      {
        strlcat(*buff,"laptop",buff_len);
      }
      else
      {
        strlcat(*buff,"Invalid",buff_len);
      }
    break;

    default:
      LOG_MSG_INFO2_0("Unknown tag");
      return -1;
  }

  strlcat(*buff, rmnet_parser_end_tag_name[index], buff_len);

  return 0;
}
/*===========================================================================
  FUNCTION RMNET_META_SM_UPDATE_EFS()

  DESCRIPTION
    This function updates the rmnet_config.txt file in efs with the
    autoconnect info.

  RETURN VALUE
    TRUE  - If successfully updated the efs.
    FALSE - If error in updating the efs

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
int rmnet_meta_sm_update_efs
(
  void
)
{
  static char *filename   = "rmnet_config.txt";
  char *temp_loc=NULL;
  char *buff=NULL;
  uint8 lap_instance=0;
  uint8 instance=0;
  int temp_len=0;
  int temp_len2=0;
  uint8 index=0;
  int fd=0;

  LOG_MSG_INFO2_0("Starting writing to EFS!!");

  buff = (char *) qmi_svc_ps_system_heap_mem_alloc(RMNET_CONFIG_EFS_FILE_SIZE
                       ,FILE_ID_DS_RMNET_META_SM,__LINE__);//Might need to keep a check on length of file
  if (buff == NULL)
  {
    return -1;
  }

  memset(buff,0,RMNET_CONFIG_EFS_FILE_SIZE);
  temp_loc = buff; /*temp_loc ptr will be used for appending data
                     at the end of buffer */

  /* Start building your buffer which will be used for writing to efs */
  strlcpy(temp_loc, rmnet_parser_config_tag[0], RMNET_CONFIG_EFS_FILE_SIZE);

  for (instance = 0; instance < RMNET_INSTANCE_MAX; instance++)
  {
    if ( RMNET_IS_VALID_LAPTOP_INSTANCE(instance))
    {
        lap_instance = RMNET_GET_LAPTOP_INSTANCE(instance);

        if(lap_instance < RMNET_MAX_INSTANCE_TAGS)
        {
          strlcat(temp_loc,
                  rmnet_parser_instance_start_tag[lap_instance],
                  RMNET_CONFIG_EFS_FILE_SIZE);

          for(index = 0;index < RMNET_PARSER_TAG_MAX;index++)
          {
            if(-1 == rmnet_parser_append_index((rmnet_parser_tag_index_e_type)index,instance,&temp_loc,RMNET_CONFIG_EFS_FILE_SIZE))
            {
              LOG_MSG_ERROR_0(" Error in appending tag to the buffer");
              PS_SYSTEM_HEAP_MEM_FREE(buff);
              return -1;
            }
          }

          strlcat(temp_loc,
                  rmnet_parser_instance_end_tag[lap_instance],
                  RMNET_CONFIG_EFS_FILE_SIZE);

          LOG_MSG_INFO2_1("Done with instance %d!!", lap_instance);
       }
       else
       {
         break;
       }
    }
    instance++;
  }

  temp_len = strlcat(temp_loc, rmnet_parser_config_tag[1], RMNET_CONFIG_EFS_FILE_SIZE);

  // Check if we already have memory corruption
  if (temp_len >= RMNET_CONFIG_EFS_FILE_SIZE)
  {
    LOG_MSG_ERROR_1("EFS file too large %d", temp_len);
    PS_SYSTEM_HEAP_MEM_FREE(buff);
    ASSERT(0);
    return -1;
  }

  // Write global config
  temp_len2 = snprintf((temp_loc + temp_len), RMNET_CONFIG_EFS_FILE_SIZE - temp_len,
                    "<global_config>\n"
                    "<ul_agg_prot_mask>%lu</ul_agg_prot_mask>\n"
                    "<dl_agg_prot_mask>%lu</dl_agg_prot_mask>\n"
                    "</global_config>",
                    rmnet_efs_global_config_params.ul_agg_prot_mask,
                    rmnet_efs_global_config_params.dl_agg_prot_mask);

  if (temp_len2 == 0 || (temp_len2 >= (RMNET_CONFIG_EFS_FILE_SIZE - temp_len)))
  {
    LOG_MSG_ERROR_2("Insufficient EFS buffer %d+%d", temp_len2, temp_len);
    PS_SYSTEM_HEAP_MEM_FREE(buff);
    ASSERT(0);
    return -1;
  }

  fd = efs_open(filename, O_CREAT | O_RDWR | O_TRUNC, ALLPERMS);
  if ( fd < 0 )
  {
    LOG_MSG_ERROR_0(" Rmnet Config EFS Open/Create failed");
    PS_SYSTEM_HEAP_MEM_FREE(buff);
    return -1;
  }

  /* Need to see if we need to delete the contents of file first or overwriting
    is sufficient as anyways we are going to append for all the instances */
  if(-1 == efs_write(fd, buff, strlen(temp_loc)))
  {
    LOG_MSG_ERROR_0(" Error in writing to the rmnet_config.txt file");
    efs_close(fd);
    PS_SYSTEM_HEAP_MEM_FREE(buff);
    return -1;
  }

  efs_close(fd);
  PS_SYSTEM_HEAP_MEM_FREE(buff);

  LOG_MSG_INFO2_0("EFS written with autoconnect details successfully!!");
  return 0;
}

/*===========================================================================
  FUNCTION RMNET_META_SM_GET_AUTOCONNECT_ROAM_SETTING()

  DESCRIPTION
    get autoconnect roam setting

  PARAMETERS
    instance - the RmNet instance

  RETURN VALUE
    roam_setting  - auto connect roam value

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
rmnet_meta_sm_autoconnect_roam_setting_e_type rmnet_meta_sm_get_autoconnect_roam_setting
(
  rmnet_instance_e_type  instance
)
{
  rmnet_smi_info_type *  info;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(instance < RMNET_INSTANCE_MAX);
  info = &rmnet_smi_info[instance];

  /* return autoconnect roaming setting from cache */
  return(info->meta_sm.settings.autoconnect_roam_setting);
} /* rmnet_meta_sm_get_autoconnect_roam_setting() */


/*===========================================================================
  FUNCTION RMNET_META_SM_GET_PREV_BYTE_STATS()

  DESCRIPTION
    Returns the last call byte stats

  PARAMETERS
    instance - the RmSM instance to query
    last_call_rx_bytes - pointer to Rx byte stats var
    last_call_tx_bytes - pointer to Tx byte stats var

  RETURN VALUE


  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void rmnet_meta_sm_get_prev_byte_stats
(
  rmnet_instance_e_type  instance,
  uint64 * last_call_rx_bytes,
  uint64 * last_call_tx_bytes
)
{
  rmnet_smi_info_type *  info;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT( (instance < RMNET_INSTANCE_MAX) && 
          (NULL != last_call_rx_bytes) && (NULL != last_call_tx_bytes));
  info = &rmnet_smi_info[instance];

  *last_call_rx_bytes = info->meta_sm.last_call_rx_bytes;
  *last_call_tx_bytes = info->meta_sm.last_call_tx_bytes;
} /* rmnet_meta_sm_get_prev_byte_stats() */

/*===========================================================================
  FUNCTION RMNET_META_SM_GET_IPV4_MANUAL_DNS_ADDRS()

  DESCRIPTION
    Gets the manual DNS settings for this instance.  Each address that has not
    been set, the type will be returned as IP_ADDR_INVALID.

  PARAMETERS
    instance    - the RmNet instance
    pri_dns_ptr - primary DNS address pointer
    sec_dns_ptr - secondary DNS address pointer

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void rmnet_meta_sm_get_ipv4_manual_dns_addrs
(
  rmnet_instance_e_type  instance,
  ip_addr_type           *pri_dns_ptr,
  ip_addr_type           *sec_dns_ptr
)
{
  rmnet_smi_info_type *  info;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(instance < RMNET_INSTANCE_MAX);
  info = &rmnet_smi_info[instance];

  if (NULL != pri_dns_ptr)
  {
    memscpy(pri_dns_ptr,
            sizeof(ip_addr_type),
           &info->meta_sm.settings.pri_dns_addr,
           sizeof(ip_addr_type));
  }
  if (NULL != sec_dns_ptr)
  {
    memscpy(sec_dns_ptr,
            sizeof(ip_addr_type),
           &info->meta_sm.settings.sec_dns_addr,
           sizeof(ip_addr_type));
  }
} /* rmnet_meta_sm_get_ipv4_manual_dns_addrs */

/*===========================================================================
  FUNCTION RMNET_META_SM_SET_IPV4_MANUAL_DNS_ADDRS()

  DESCRIPTION
    Sets the manual IPv4 DNS settings used for this instance.

  PARAMETERS
    instance    - the RmNet instance
    pri_dns_ptr - primary DNS address
    sec_dns_ptr - secondary DNS address

  RETURN VALUE
    0  - update successful
    -1 - udpate failed

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
int rmnet_meta_sm_set_ipv4_manual_dns_addrs
(
  rmnet_instance_e_type  instance,
  ip_addr_type           *pri_dns_ptr,
  ip_addr_type           *sec_dns_ptr
)
{
  rmnet_smi_info_type *  info;

  qmi_nv_status_e_type qmi_nv_status;
  qmi_nv_dns_addr_type qmi_nv_item;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(instance < RMNET_INSTANCE_MAX);
  info = &rmnet_smi_info[instance];

  /* only check primary address if provided */
  if (NULL != pri_dns_ptr)
  {
    /* check for IPv4 address type */
    if (IPV4_ADDR != pri_dns_ptr->type)
    {
      LOG_MSG_ERROR_2("non-IPv4 type passed, pri_dns_ptr=0x%p type=%d",
                      pri_dns_ptr, pri_dns_ptr->type);
      return -1;
    }
  }

  /* only check secondary address if provided */
  if (NULL != sec_dns_ptr)
  {
    /* check for IPv4 address type */
    if (IPV4_ADDR != sec_dns_ptr->type)
    {
      LOG_MSG_ERROR_2("non-IPv4 type passed, sec_dns_ptr=0x%p type=%d",
                      sec_dns_ptr, sec_dns_ptr->type);
      return -1;
    }
  }

  if (NULL != pri_dns_ptr)
  {
    /* update the primary DNS setting */
    qmi_nv_item.index    = instance/2;
    qmi_nv_item.dns_addr = 0;
    qmi_nv_status = qmi_nv_read(QMI_NV_ITEM_MANUAL_DNS_IPV4_PRI,
                                (uint8)instance/2,
                                &qmi_nv_item,
                                sizeof(qmi_nv_item));
    if ((QMI_NV_STATUS_OK == qmi_nv_status) &&
        (qmi_nv_item.dns_addr == pri_dns_ptr->addr.v4))
    {
      LOG_MSG_INFO2_0("pri_dns_ptr value already set");
    }
    else
    {
      /* update the NV item */
      qmi_nv_item.index    = instance/2;
      qmi_nv_item.dns_addr = pri_dns_ptr->addr.v4;
      qmi_nv_status = qmi_nv_write(QMI_NV_ITEM_MANUAL_DNS_IPV4_PRI,
                                   (uint8)instance/2,
                                   &qmi_nv_item,
                                   sizeof(qmi_nv_item));
      if( QMI_NV_STATUS_OK != qmi_nv_status )
      {
        LOG_MSG_ERROR_2 ("Unable to write NV for pri_dns_ptr (0x%p - %d)",
                         pri_dns_ptr, qmi_nv_status);
        return -1;
      }

      /* update the cached value */
      info->meta_sm.settings.pri_dns_addr.type = IPV4_ADDR;
      info->meta_sm.settings.pri_dns_addr.addr.v4 = pri_dns_ptr->addr.v4;
    }
  }

  if (NULL != sec_dns_ptr)
  {
    /* update the secondary DNS setting */
    qmi_nv_item.index    = instance/2;
    qmi_nv_item.dns_addr = 0;
    qmi_nv_status = qmi_nv_read(QMI_NV_ITEM_MANUAL_DNS_IPV4_SEC,
                                (uint8)instance/2,
                                &qmi_nv_item,
                                sizeof(qmi_nv_item));
    if ((QMI_NV_STATUS_OK == qmi_nv_status) &&
        (qmi_nv_item.dns_addr == sec_dns_ptr->addr.v4))
    {
      LOG_MSG_INFO2_0("sec_dns_ptr value already set");
    }
    else
    {
      /* update the NV item */
      qmi_nv_item.index    = instance/2;
      qmi_nv_item.dns_addr = sec_dns_ptr->addr.v4;
      qmi_nv_status = qmi_nv_write(QMI_NV_ITEM_MANUAL_DNS_IPV4_SEC,
                                   (uint8)instance/2,
                                   &qmi_nv_item,
                                   sizeof(qmi_nv_item));
      if( QMI_NV_STATUS_OK != qmi_nv_status )
      {
        LOG_MSG_ERROR_2 ("Unable to write NV for sec_dns_ptr (0x%p - %d)",
                         sec_dns_ptr, qmi_nv_status);
        return -1;
      }

      /* update the cached value */
      info->meta_sm.settings.sec_dns_addr.type = IPV4_ADDR;
      info->meta_sm.settings.sec_dns_addr.addr.v4 = sec_dns_ptr->addr.v4;
    }
  }

  return 0;
} /* rmnet_meta_sm_set_ipv4_manual_dns_addrs() */

/*===========================================================================
  FUNCTION RMNET_META_SM_GET_IPV6_MANUAL_DNS_ADDRS()

  DESCRIPTION
    Gets the manual DNS settings for this instance.  Each address that has not
    been set, the type will be returned as IP_ADDR_INVALID.

  PARAMETERS
    instance    - the RmNet instance
    pri_dns_ptr - primary DNS address pointer
    sec_dns_ptr - secondary DNS address pointer

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void rmnet_meta_sm_get_ipv6_manual_dns_addrs
(
  rmnet_instance_e_type  instance,
  ip_addr_type           *pri_dns_ptr,
  ip_addr_type           *sec_dns_ptr
)
{
  rmnet_smi_info_type *  info;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(instance < RMNET_INSTANCE_MAX);
  info = &rmnet_smi_info[instance];

  if (NULL != pri_dns_ptr)
  {
    memscpy(pri_dns_ptr,
            sizeof(ip_addr_type),
           &info->meta_sm.settings.ipv6_pri_dns_addr,
           sizeof(ip_addr_type));
  }
  if (NULL != sec_dns_ptr)
  {
    memscpy(sec_dns_ptr,
            sizeof(ip_addr_type),
           &info->meta_sm.settings.ipv6_sec_dns_addr,
           sizeof(ip_addr_type));
  }
} /* rmnet_meta_sm_get_ipv6_manual_dns_addrs */

/*===========================================================================
  FUNCTION RMNET_META_SM_SET_IPV6_MANUAL_DNS_ADDRS()

  DESCRIPTION
    Sets the manual IPv6 DNS settings used for this instance.

  PARAMETERS
    instance    - the RmNet instance
    pri_dns_ptr - primary DNS address
    sec_dns_ptr - secondary DNS address

  RETURN VALUE
    0  - update successful
    -1 - udpate failed

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
int rmnet_meta_sm_set_ipv6_manual_dns_addrs
(
  rmnet_instance_e_type  instance,
  ip_addr_type           *pri_dns_ptr,
  ip_addr_type           *sec_dns_ptr
)
{
  rmnet_smi_info_type *  info;

  qmi_nv_status_e_type      qmi_nv_status;
  qmi_nv_ipv6_dns_addr_type qmi_nv_item;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(instance < RMNET_INSTANCE_MAX);
  info = &rmnet_smi_info[instance];

  /* only check primary address if provided */
  if (NULL != pri_dns_ptr)
  {
    /* check for IPv4 address type */
    if (IPV6_ADDR != pri_dns_ptr->type)
    {
      LOG_MSG_ERROR_2("non-IPv6 type passed, pri_dns_ptr=0x%p type=%d",
                      pri_dns_ptr, pri_dns_ptr->type);
      return -1;
    }
  }

  /* only check secondary address if provided */
  if (NULL != sec_dns_ptr)
  {
    /* check for IPv6 address type */
    if (IPV6_ADDR != sec_dns_ptr->type)
    {
      LOG_MSG_ERROR_2("non-IPv6 type passed, sec_dns_ptr=0x%p type=%d",
                      sec_dns_ptr, sec_dns_ptr->type);
      return -1;
    }
  }

  if (NULL != pri_dns_ptr)
  {
    /* update the primary DNS setting */
    qmi_nv_item.index    = instance/2;
    qmi_nv_item.dns_addr[0] = 0;
    qmi_nv_item.dns_addr[1] = 0;
    qmi_nv_status = qmi_nv_read(QMI_NV_ITEM_MANUAL_DNS_IPV6_PRI,
                                (uint8)instance/2,
                                &qmi_nv_item,
                                sizeof(qmi_nv_item));
    if ((QMI_NV_STATUS_OK == qmi_nv_status) &&
        (qmi_nv_item.dns_addr[0] == pri_dns_ptr->addr.v6[0]) &&
        (qmi_nv_item.dns_addr[1] == pri_dns_ptr->addr.v6[1]))
    {
      LOG_MSG_INFO2_0("pri_dns_ptr value already set");
    }
    else
    {
      /* update the NV item */
      qmi_nv_item.index    = instance/2;
      qmi_nv_item.dns_addr[0] = pri_dns_ptr->addr.v6[0];
      qmi_nv_item.dns_addr[1] = pri_dns_ptr->addr.v6[1];
      qmi_nv_status = qmi_nv_write(QMI_NV_ITEM_MANUAL_DNS_IPV6_PRI,
                                   (uint8)instance/2,
                                   &qmi_nv_item,
                                   sizeof(qmi_nv_item));
      if( QMI_NV_STATUS_OK != qmi_nv_status )
      {
        LOG_MSG_ERROR_2 ("Unable to write NV for pri_dns_ptr (0x%p - %d)",
                         pri_dns_ptr, qmi_nv_status);
        return -1;
      }

      /* update the cached value */
      info->meta_sm.settings.ipv6_pri_dns_addr.type = IPV6_ADDR;
      info->meta_sm.settings.ipv6_pri_dns_addr.addr.v6[0] = pri_dns_ptr->addr.v6[0];
      info->meta_sm.settings.ipv6_pri_dns_addr.addr.v6[1] = pri_dns_ptr->addr.v6[1];
    }
  }

  if (NULL != sec_dns_ptr)
  {
    /* update the secondary DNS setting */
    qmi_nv_item.index    = instance/2;
    qmi_nv_item.dns_addr[0] = 0;
    qmi_nv_item.dns_addr[1] = 0;
    qmi_nv_status = qmi_nv_read(QMI_NV_ITEM_MANUAL_DNS_IPV6_SEC,
                                (uint8)instance/2,
                                &qmi_nv_item,
                                sizeof(qmi_nv_item));
    if ((QMI_NV_STATUS_OK == qmi_nv_status) &&
        (qmi_nv_item.dns_addr[0] == sec_dns_ptr->addr.v6[0]) &&
        (qmi_nv_item.dns_addr[1] == sec_dns_ptr->addr.v6[1]))
    {
      LOG_MSG_INFO2_0("sec_dns_ptr value already set");
    }
    else
    {
      /* update the NV item */
      qmi_nv_item.index    = instance/2;
      qmi_nv_item.dns_addr[0] = sec_dns_ptr->addr.v6[0];
      qmi_nv_item.dns_addr[1] = sec_dns_ptr->addr.v6[1];
      qmi_nv_status = qmi_nv_write(QMI_NV_ITEM_MANUAL_DNS_IPV6_SEC,
                                   (uint8)instance/2,
                                   &qmi_nv_item,
                                   sizeof(qmi_nv_item));
      if( QMI_NV_STATUS_OK != qmi_nv_status )
      {
        LOG_MSG_ERROR_2 ("Unable to write NV for sec_dns_ptr (0x%p - %d)",
                         sec_dns_ptr, qmi_nv_status);
        return -1;
      }

      /* update the cached value */
      info->meta_sm.settings.ipv6_sec_dns_addr.type = IPV6_ADDR;
      info->meta_sm.settings.ipv6_sec_dns_addr.addr.v6[0] = sec_dns_ptr->addr.v6[0];
      info->meta_sm.settings.ipv6_sec_dns_addr.addr.v6[1] = sec_dns_ptr->addr.v6[1];
    }
  }

  return 0;
} /* rmnet_meta_sm_set_ipv6_manual_dns_addrs() */

/*===========================================================================
FUNCTION RMNET_META_SM_SET_DATA_FORMAT

DESCRIPTION
  This function is called by QMI_CTL to set the data format used by driver

PARAMETERS
  instance         : RmNet instance
  data_format      : data format used
  data_format_mask : mask to specify data formats present in data_format
  client_type      : The caller of this API (either QMI CTL or Rmnet )

DEPENDENCIES
  None

RETURN VALUE
  RMNET_SET_DATA_FORMAT_SUCCESS : Set data format is a success.
  RMNET_SET_DATA_FORMAT_FAILURE : Set data format failed.
  RMNET_SET_DATA_FORMAT_NO_EFFECT : No effect of set data format.

SIDE EFFECTS
  None.
===========================================================================*/
rmnet_set_data_fmt_ret_e_type rmnet_meta_sm_set_data_format
(
  rmnet_instance_e_type            instance,
  rmnet_meta_sm_data_format_type * data_format,
  uint32                           data_format_mask,
  rmnet_set_data_fmt_e_type        client_type
)
{
  rmnet_smi_info_type       *  info;
  rmnet_smi_info_type       *  info_other;
  rmnet_sm_link_prot_e_type    link_prot;
  static uint32               rmnet_last_client_set_data = RMNET_SET_DATA_FORMAT_DTR_LOW;
  rmnet_set_data_fmt_ret_e_type  ret_val = RMNET_SET_DATA_FORMAT_SUCCESS;
  int                          i;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(instance < RMNET_INSTANCE_MAX);
  info = &rmnet_smi_info[instance];
  if(instance % 2 == 0)
  {
    info_other = &rmnet_smi_info[instance + 1];
  }
  else
  {
    info_other = &rmnet_smi_info[instance - 1];
  }

  if( info == NULL || info_other == NULL)
  {
    LOG_MSG_ERROR_2("NULL rmnet_smi_info struct info = %x , info_other = %x",
                    info, info_other);
    return RMNET_SET_DATA_FORMAT_FAILURE;
  }
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

   Set Data format can be set using QMI CTL or at Autoconnect. QMI SET CTL
   is given the precedence over DTR SET CTL. If QMI CTL set up the data
   format, DTR cannot change it. If a call is already up, the set data format
   cannot be changed. NO EFFECT Error is returned back with current data
   format settings.

   When call is up: No EFFECT
   When Idle: If last set data format was set by QMI CTL, DTR Set CTL has no
   effect.

   At DTR Low, the rmnet_last_client will be RESET so at a different DTR
   high, it would be a fresh take on who set the data format.
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  LOG_MSG_INFO1_5 ("Set Data format, RMNET inst %d, Client %d, last client %d,"
                   "RmNet MetaSM state %d, Data format mask %d",
                   instance,
                   client_type,
                   rmnet_last_client_set_data,
                   info->meta_sm.state,
                   data_format_mask);

  if ( client_type == RMNET_SET_DATA_FORMAT_DTR_LOW )
  {
    rmnet_last_client_set_data = RMNET_SET_DATA_FORMAT_DTR_LOW;
    ret_val =  RMNET_SET_DATA_FORMAT_NO_EFFECT;
  }

  /*-----------------------------------------------------------------------
   * Checks if logical or local ports have calls up.
   * If so, don't change settings
  -----------------------------------------------------------------------*/
  if (rmnet_sio_get_master_inst() == instance)
  {
    for (i = 0; i < RMNET_INSTANCE_MAX; i++)
    {
      if ( (rmnet_smi_info[i].meta_sm.rmnet_sio_handle != NULL) &&
           (QMUX_IS_QMAP_MUXED_PORT(
              rmnet_sio_get_port(rmnet_smi_info[i].meta_sm.rmnet_sio_handle))) &&
           (rmnet_smi_info[i].meta_sm.state != RMNET_META_SM_NULL_STATE) )
      {
        data_format->qos = info->meta_sm.data_format;
        data_format->qos_format = info->meta_sm.qos_format;
        data_format->link_prot = info->link_prot;
        data_format->data_agg_protocol = info->meta_sm.data_agg_protocol;
        ret_val = RMNET_SET_DATA_FORMAT_NO_EFFECT;
      }
    }
  }
  else if ( info->meta_sm.state != RMNET_META_SM_NULL_STATE )
  {
    /* return with NO EFFECT; As the call is already up,
    the data format to be picked up from the info */
    data_format->qos = info->meta_sm.data_format;
    data_format->qos_format = info->meta_sm.qos_format;
    data_format->link_prot = info->link_prot;
    data_format->data_agg_protocol = info->meta_sm.data_agg_protocol;
    ret_val =  RMNET_SET_DATA_FORMAT_NO_EFFECT;
  }
  else if(info_other->meta_sm.state != RMNET_META_SM_NULL_STATE)
  {
    /* check if the other instance has a call up. Send the existing settings
       in that case
    */
    data_format->qos = info_other->meta_sm.data_format;
    data_format->qos_format = info_other->meta_sm.qos_format;
    data_format->link_prot = info_other->link_prot;
    data_format->data_agg_protocol = info_other->meta_sm.data_agg_protocol;
    ret_val =  RMNET_SET_DATA_FORMAT_NO_EFFECT;
  }
  else
  {
    /* In idle, QMI CTL/WDSA setting takes the precedence. */
    if ( (client_type == RMNET_SET_DATA_FORMAT_QMI_CTL) ||
         (client_type == RMNET_SET_DATA_FORMAT_QMI_WDA) )
    {
      rmnet_last_client_set_data = client_type;
      info->meta_sm.data_format_client = client_type;
      LOG_MSG_INFO1_1("Set Data format by QMI: client_type%d", client_type);
    }
    else if (client_type == RMNET_SET_DATA_FORMAT_DTR_HGH)  /* This section is for Rmnet */
    {
      /* In idle, if QMI CTL/WDA has already updated the settings,EFS
         settings are ignored.  */
      if ( (rmnet_last_client_set_data == RMNET_SET_DATA_FORMAT_QMI_CTL) ||
           (rmnet_last_client_set_data == RMNET_SET_DATA_FORMAT_QMI_WDA) )
      {
        /* return with NO EFFECT; As the call is already up,
        the data format to be picked up from the info  */
        data_format->qos = info->meta_sm.data_format;
        data_format->qos_format = info->meta_sm.qos_format;
        data_format->link_prot = info->link_prot;
        data_format->data_agg_protocol = info->meta_sm.data_agg_protocol;
        ret_val =  RMNET_SET_DATA_FORMAT_NO_EFFECT;
      }
      else
      {
        rmnet_last_client_set_data = RMNET_SET_DATA_FORMAT_DTR_HGH;
        info->meta_sm.data_format_client = client_type;
        LOG_MSG_INFO1_0("Set data format by DTR HIGH change");
      }
    }

  }

  if ( ret_val != RMNET_SET_DATA_FORMAT_NO_EFFECT )
  {
    LOG_MSG_INFO1_3("Data format [%d] Link Prot [%d] UL TLP [%d]",
                    data_format->qos,
                    data_format->link_prot,
                    data_format->data_agg_protocol.ul_data_agg_protocol);

    if (data_format_mask & RMNET_DATA_FORMAT_MASK_QOS)
    {
      switch(data_format->qos)
      {
        case 0:
          info->meta_sm.data_format = data_format->qos;
          break;

        case 1:
          info->meta_sm.data_format = data_format->qos;

          /* This is needed for autoconnect case */
          /* Since in autoconnect we'll post start_pkt_ev immediately on power up,
             data_format will be 0 at that time and so, we'll register for
             default flow events
             but if data_format becomes 1, then need to dereg for those events, as
             we are in QoS mode */

          if(info->meta_sm.default_flow_ev_reg == TRUE)
          {
            ps_flow_event_cback_dereg(
                                PS_IFACE_GET_DEFAULT_FLOW(info->um_iface_ptr),
                                FLOW_TX_ENABLED_EV,
                                info->meta_sm.um_flow_tx_enabled_buf_ptr);
            ps_flow_event_cback_dereg(
                                PS_IFACE_GET_DEFAULT_FLOW(info->um_iface_ptr),
                                FLOW_TX_DISABLED_EV,
                                info->meta_sm.um_flow_tx_disabled_buf_ptr);

            info->meta_sm.default_flow_ev_reg = FALSE;
          }
          break;

      default:
           LOG_MSG_ERROR_1("Invalid qos setting %d", data_format->qos);
           return RMNET_SET_DATA_FORMAT_FAILURE;
      } /* switch(data_format->qos) */

    } /* if (data_format_mask & RMNET_DATA_FORMAT_MASK_QOS) */

    if (data_format_mask & RMNET_DATA_FORMAT_MASK_LINK_PROT)
    {
      link_prot = (rmnet_sm_link_prot_e_type) (data_format->link_prot);
      //TODO: SY: is this check needed
      if(link_prot != RMNET_INVALID_MODE)
      {
        /*-----------------------------------------------------------------------
          Always accept and use IP mode if supported, otherwise do nothing and
          use ethernet by default.
        -----------------------------------------------------------------------*/
        if(link_prot & RMNET_IP_MODE)
        {
          info->link_prot        = RMNET_IP_MODE;
        }
        else
        {
          info->link_prot        = RMNET_ETHERNET_MODE;
        }

        data_format->link_prot = (uint16) info->link_prot;
      }/* if(link_prot != RMNET_INVALID_MODE) */
    } /* if (data_format_mask & RMNET_DATA_FORMAT_MASK_LINK_PROT) */

    /*-----------------------------------------------------------------------
     * Set QoS header format. 8-byte header is only supported in IP mode
    ------------------------------------------------------------------------*/
    if (data_format_mask & RMNET_DATA_FORMAT_MASK_QOS_FORMAT)
    {
      if (info->link_prot == RMNET_IP_MODE &&
          data_format->qos_format == RMNET_QOS_FORMAT_8_BYTE)
      {
        info->meta_sm.qos_format = RMNET_QOS_FORMAT_8_BYTE;
      }
      else
      {
        info->meta_sm.qos_format = RMNET_QOS_FORMAT_6_BYTE;
      }
    }
    data_format->qos_format = info->meta_sm.qos_format;

    /*-----------------------------------------------------------------------
      Set data agg parameters -
      1. UL/DL data_agg mode,
      2. dl_max_num,
      3. dl_max_size
      4. NDP sig
    -----------------------------------------------------------------------*/
    if ( (data_format_mask & RMNET_DATA_FORMAT_MASK_UL_DATA_AGG) ||
         (data_format_mask & RMNET_DATA_FORMAT_MASK_DL_DATA_AGG) )
    {
      rmnet_meta_sm_set_data_agg_ex(instance, data_format, data_format_mask);
    }

  } /* if ( ret_val != RMNET_SET_DATA_FORMAT_NO_EFFECT ) */

  /*-----------------------------------------------------------------------
   * If MUX port configuration is enabled
  -----------------------------------------------------------------------*/
  if ( (ret_val == RMNET_SET_DATA_FORMAT_SUCCESS) &&
       (qmi_get_mux_port_cfg() != QMI_MUX_PORT_CFG_DISABLED) )
  {
    /*-----------------------------------------------------------------------
     * Retrieve UL aggregation thresholds
    -----------------------------------------------------------------------*/
    if (info->meta_sm.rmnet_sio_handle != NULL)
    {
      rmnet_sio_get_ul_data_agg_thresholds(
                 info->meta_sm.rmnet_sio_handle,
                 &data_format->data_agg_protocol.ul_data_agg_max_num,
                 &data_format->data_agg_protocol.ul_data_agg_max_size);

      info->meta_sm.data_agg_protocol.ul_data_agg_max_num =
                data_format->data_agg_protocol.ul_data_agg_max_num;
      info->meta_sm.data_agg_protocol.ul_data_agg_max_size =
                data_format->data_agg_protocol.ul_data_agg_max_size;
    }

    /*-----------------------------------------------------------------------
     * Apply data format to logical ports
    -----------------------------------------------------------------------*/
    if (rmnet_sio_get_master_inst() == instance)
    {
      for (i = 0; i < RMNET_INSTANCE_MAX; i++)
      {
        if ( rmnet_smi_info[i].meta_sm.rmnet_sio_handle !=
                 info->meta_sm.rmnet_sio_handle &&
             QMUX_IS_QMAP_MUXED_PORT(
                 rmnet_sio_get_port(rmnet_smi_info[i].meta_sm.rmnet_sio_handle)) )
        {
          rmnet_smi_info[i].link_prot = info->link_prot;
          rmnet_smi_info[i].meta_sm.data_format = info->meta_sm.data_format;
          rmnet_smi_info[i].meta_sm.qos_format = info->meta_sm.qos_format;
          rmnet_smi_info[i].meta_sm.data_agg_protocol = info->meta_sm.data_agg_protocol;
        }
      }
    }
  }

  return ret_val;

} /* rmnet_meta_sm_set_data_format() */

/*===========================================================================
FUNCTION RMNET_META_SM_GET_DATA_FORMAT

DESCRIPTION
  This function is called by QMI to get the data format used by driver

PARAMETERS
  instance       : RmNet instance
  data_format    : data format used

DEPENDENCIES
  None

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
void rmnet_meta_sm_get_data_format
(
  rmnet_instance_e_type            instance,
  rmnet_meta_sm_data_format_type * data_format
)
{
  rmnet_smi_info_type       *  info;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(instance < RMNET_INSTANCE_MAX);
  info = &rmnet_smi_info[instance];

  data_format->qos = info->meta_sm.data_format;
  data_format->qos_format = info->meta_sm.qos_format;
  data_format->link_prot = info->link_prot;
  data_format->data_agg_protocol = info->meta_sm.data_agg_protocol;
}

/*===========================================================================
FUNCTION RMNET_META_SM_GET_DATA_FORMAT_CLIENT

DESCRIPTION
  This function returns the last QMI client which changed the data format

PARAMETERS
  rmnet_instance       : RmNet instance

DEPENDENCIES
  None

RETURN VALUE
  rmnet_set_data_fmt_e_type  : Client type

SIDE EFFECTS
  None.
===========================================================================*/
rmnet_set_data_fmt_e_type rmnet_meta_sm_get_data_format_client
(
  rmnet_instance_e_type      rmnet_instance
)
{
  rmnet_smi_info_type       *  info;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(rmnet_instance < RMNET_INSTANCE_MAX);
  info = &rmnet_smi_info[rmnet_instance];

  return info->meta_sm.data_format_client;

}

/*===========================================================================
  FUNCTION RMNET_META_SM_SET_DATA_AGG_EX()

  DESCRIPTION
    Sets the data aggregation protocol for the given rmnet instance.
    This is the extended function for mux port config.

  PARAMETERS
    instance         : Rmnet instance
    data_format      : data aggregation protocol
    data_format_mask : Set Data format mask

  RETURN VALUE
    TRUE on success
    FALSE otherwise

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void rmnet_meta_sm_set_data_agg_ex
(
  rmnet_instance_e_type            instance,
  rmnet_meta_sm_data_format_type * data_format,
  uint32                           data_format_mask
)
{
#ifdef FEATURE_DATA_A2
  rmnet_data_agg_enum_type       ul_data_agg = RMNET_DISABLE_DATA_AGG;
  rmnet_data_agg_enum_type       dl_data_agg = RMNET_DISABLE_DATA_AGG;
  rmnet_smi_info_type          * info = NULL;
  sio_port_id_type               sio_port;
  uint32                         ncm_sig = 0;
  uint32                         ndp_sig = 0;
  uint32                         dl_agg_max_num = 0;
  uint32                         dl_agg_max_size = 0;
  uint8                          dl_min_padding = 0;
  qmi_mux_port_cfg_enum          mux_port_cfg;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  info = &rmnet_smi_info[instance];
  ul_data_agg = data_format->data_agg_protocol.ul_data_agg_protocol;
  dl_data_agg = data_format->data_agg_protocol.dl_data_agg_protocol;
  sio_port = rmnet_sio_get_port(info->meta_sm.rmnet_sio_handle);

  /*-----------------------------------------------------------------
    Set data format only if mux port config is enabled
  -----------------------------------------------------------------*/
  mux_port_cfg = qmi_get_mux_port_cfg();
  if (mux_port_cfg == QMI_MUX_PORT_CFG_DISABLED)
  {
    ul_data_agg = RMNET_DISABLE_DATA_AGG;
    dl_data_agg = RMNET_DISABLE_DATA_AGG;
    goto data_agg_out;
  }

  /*-----------------------------------------------------------------
    If not A2 port, no aggregation.
  -----------------------------------------------------------------*/
  if ( !rmnet_sio_is_a2_port(sio_port) )
  {
    LOG_MSG_INFO2_2("No DL data agg for non-A2 port 0x%x rmnet inst %d",
                    sio_port, instance);
    ul_data_agg = RMNET_DISABLE_DATA_AGG;
    dl_data_agg = RMNET_DISABLE_DATA_AGG;
    goto data_agg_out;
  }

  /*-----------------------------------------------------------------
    If not enabled by global EFS, no aggregation.
  -----------------------------------------------------------------*/
  if ( (ul_data_agg != RMNET_DISABLE_DATA_AGG) &&
       (((1 << (ul_data_agg -1)) & rmnet_efs_global_config_params.ul_agg_prot_mask) == 0) )
  {
    LOG_MSG_INFO2_3("Unsupported UL data agg [%d] on inst %d, UL data agg mask 0x%x",
                    ul_data_agg,
                    instance,
                    rmnet_efs_global_config_params.ul_agg_prot_mask);
    ul_data_agg = RMNET_DISABLE_DATA_AGG;
  }

  if ( (dl_data_agg != RMNET_DISABLE_DATA_AGG) &&
       (((1 << (dl_data_agg -1)) & rmnet_efs_global_config_params.dl_agg_prot_mask) == 0) )
  {
    LOG_MSG_INFO2_3("Unsupported DL data agg [%d] on inst %d, DL data agg mask 0x%x",
                    dl_data_agg,
                    instance,
                    rmnet_efs_global_config_params.dl_agg_prot_mask);
    dl_data_agg = RMNET_DISABLE_DATA_AGG;
  }

  /*-----------------------------------------------------------------
    Data format parameters
  -----------------------------------------------------------------*/
  if ( RMNET_DATA_AGG_IS_QMAP(ul_data_agg) &&
       RMNET_DATA_AGG_IS_QMAP(dl_data_agg) )
  {
    if (info->link_prot == RMNET_ETHERNET_MODE)
    {
      LOG_MSG_ERROR_0("QMAP not supported in Ethernet mode");
      ul_data_agg = RMNET_DISABLE_DATA_AGG;
      dl_data_agg = RMNET_DISABLE_DATA_AGG;
      goto data_agg_out;
    }

    /* dl_max_num */
    if ( (data_format_mask & RMNET_DATA_FORMAT_MASK_DL_DATA_AGG_NUM) &&
         (data_format->data_agg_protocol.dl_data_agg_max_num != 0) )
    {
      dl_agg_max_num = data_format->data_agg_protocol.dl_data_agg_max_num;
    }
    else
    {
      /* Set the default max number of packets if data agg is set and
       * max num TLV is absent or max number is set to 0 */
      dl_agg_max_num = 1;
    }

    /* dl_max_size */
    if ( (data_format_mask & RMNET_DATA_FORMAT_MASK_DL_DATA_AGG_SIZE) &&
         (data_format->data_agg_protocol.dl_data_agg_max_size != 0) )
    {
      dl_agg_max_size = data_format->data_agg_protocol.dl_data_agg_max_size;
    }
    else
    {
      /* Set the default max number of bytes if data agg is set and
       * max size TLV is absent or max size is set to 0 */
      dl_agg_max_size = 0xFFFFFFFF;
    }

    /* dl_min_padding */
    if (data_format_mask & RMNET_DATA_FORMAT_MASK_DL_MIN_PADDING)
    {
      dl_min_padding = data_format->data_agg_protocol.dl_min_padding;
    }
  }
  else if ( ul_data_agg == RMNET_ENABLE_DATA_AGG_MBIM &&
            dl_data_agg == RMNET_ENABLE_DATA_AGG_MBIM )
  {
    if (info->link_prot == RMNET_ETHERNET_MODE)
    {
      LOG_MSG_ERROR_0("MBIM not supported in Ethernet mode");
      ul_data_agg = RMNET_DISABLE_DATA_AGG;
      dl_data_agg = RMNET_DISABLE_DATA_AGG;
      goto data_agg_out;
    }

    /*-----------------------------------------------------------------------
      Set NDP Sig.This is good to do specific to MBIM UL and DL agg protocols.
      Since we might negotiate any combination and can have MBIM in either
      UL or DL in some cases.
    -----------------------------------------------------------------------*/
    if (data_format_mask & RMNET_DATA_FORMAT_MASK_NDP_SIGNATURE)
    {
      ndp_sig = data_format->data_agg_protocol.ndp_signature;
    }
    else
    {
      /* Set the default NDP signature value if data agg is set and ndp
         signature TLV is absent.Make sure data_agg being passed is MBIM */
      ndp_sig = rmnet_meta_sm_set_default_ndp_sig(info, ul_data_agg);
    }

    /*---------------------------------------------------------------
        Set dl_max_num and dl_max_size for MBIM
    ---------------------------------------------------------------*/
    /* dl_max_num */
    if ( (data_format_mask & RMNET_DATA_FORMAT_MASK_DL_DATA_AGG_NUM) &&
         (data_format->data_agg_protocol.dl_data_agg_max_num != 0) )
    {
      dl_agg_max_num = data_format->data_agg_protocol.dl_data_agg_max_num;
    }
    else
    {
      /* Set the default max number of packets if data agg is set and
       * max num TLV is absent or max number is set to 0 */
      dl_agg_max_num = PS_DL_OPT_MBIM_MAX_NDP_AGGR_DGRMS;
    }

    /* dl_max_size */
    if ( (data_format_mask & RMNET_DATA_FORMAT_MASK_DL_DATA_AGG_SIZE) &&
         (data_format->data_agg_protocol.dl_data_agg_max_size != 0) )
    {
      dl_agg_max_size = data_format->data_agg_protocol.dl_data_agg_max_size;
    }
    else
    {
      /* Set the default max number of bytes if data agg is set and
       * max size TLV is absent or max size is set to 0 */
      dl_agg_max_size = PS_DL_OPT_MBIM_MAX_NDP_AGGR_DGRMS * DEFAULT_DATA_UNIT;
    }
  }
  else if ( ul_data_agg == RMNET_DISABLE_DATA_AGG &&
            dl_data_agg == RMNET_DISABLE_DATA_AGG )
  {
    // For emb call, don't reset data format - A2 limitation
    if (!RMNET_IS_VALID_LAPTOP_INSTANCE(instance))
    {
      goto data_agg_out;
    }
  }
  else
  {
    LOG_MSG_ERROR_2("Unknown agg mode UL [%d] DL [%d]", ul_data_agg, dl_data_agg);
    ul_data_agg = RMNET_DISABLE_DATA_AGG;
    dl_data_agg = RMNET_DISABLE_DATA_AGG;
    goto data_agg_out;
  }

  /*-------------------------------------------------------------------------
    Invoke SIO set data format so A2 is updated
  -------------------------------------------------------------------------*/
  if ( rmnet_sio_set_data_format(info->meta_sm.rmnet_sio_handle,
                                 ul_data_agg,
                                 dl_data_agg,
                                 ndp_sig,
                                 dl_agg_max_num,
                                 dl_agg_max_size,
                                 dl_min_padding) == FALSE )
  {
    LOG_MSG_ERROR_3("Failed to set data agg mode UL [%d] DL [%d] rmnet inst %d",
                    ul_data_agg, dl_data_agg, instance);
    ul_data_agg = RMNET_DISABLE_DATA_AGG;
    dl_data_agg = RMNET_DISABLE_DATA_AGG;
  }

data_agg_out:
  /*-----------------------------------------------------------------------
    Update final values-DL agg_mode,max_num,max_size in info and data_format
  -----------------------------------------------------------------------*/
  data_format->data_agg_protocol.ul_data_agg_protocol = ul_data_agg;
  data_format->data_agg_protocol.dl_data_agg_protocol = dl_data_agg;
  data_format->data_agg_protocol.dl_data_agg_max_num  = dl_agg_max_num;
  data_format->data_agg_protocol.dl_data_agg_max_size = dl_agg_max_size;
  data_format->data_agg_protocol.ndp_signature        = ndp_sig;
  data_format->data_agg_protocol.dl_min_padding       = dl_min_padding;

  info->meta_sm.data_agg_protocol.ul_data_agg_protocol = ul_data_agg;
  info->meta_sm.data_agg_protocol.dl_data_agg_protocol = dl_data_agg;
  info->meta_sm.data_agg_protocol.dl_data_agg_max_num  = dl_agg_max_num;
  info->meta_sm.data_agg_protocol.dl_data_agg_max_size = dl_agg_max_size;
  info->meta_sm.data_agg_protocol.ndp_signature        = ndp_sig;
  info->meta_sm.data_agg_protocol.dl_min_padding       = dl_min_padding;

  LOG_MSG_INFO1_6(" Final Data format set: UL [%d] DL [%d] max num [%d] "
                  "max size [%d] min pad [%d] ndp sig [0x%08X]",
                  ul_data_agg, dl_data_agg, dl_agg_max_num,
                  dl_agg_max_size, dl_min_padding, ndp_sig);

#endif /* FEATURE_DATA_A2 */

} /* rmnet_meta_sm_set_data_agg_ex() */

/*===========================================================================
  FUNCTION RMNET_META_SM_SET_DEFAULT_NDP_SIG()

  DESCRIPTION
    Sets the default NDP signature for the given Rmnet instance based on the
    data aggregation protocol passed.

  PARAMETERS
    info     : Rmnet instance info pointer
    data_agg : Data aggregation protocol

  RETURN VALUE
    NDP signature

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
uint32 rmnet_meta_sm_set_default_ndp_sig
(
  rmnet_smi_info_type               *info,
  rmnet_data_agg_enum_type          data_agg
)
{
  if ( RMNET_ENABLE_DATA_AGG_QC_NCM == data_agg )
  {
    info->meta_sm.data_agg_protocol.ndp_signature = RMNET_QC_NCM_HDR_NDP16_SIG;
  }
  else if (RMNET_ENABLE_DATA_AGG_MBIM == data_agg )
  {
    info->meta_sm.data_agg_protocol.ndp_signature = RMNET_MBIM_HDR_IPS0_NDP16_SIG;
  }

  return info->meta_sm.data_agg_protocol.ndp_signature;

} /* rmnet_meta_sm_set_default_ndp_sig */

void rmnet_meta_sm_reg_iface_binding_cb
(
  rmnet_instance_e_type inst,
  rmnet_iface_binding_complete_cb_f_type rmnet_iface_binding_complete_cb,
  void *  user_data
)
{
  rmnet_smi_info_type *  info;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(rmnet_iface_binding_complete_cb);
  info = &rmnet_smi_info[inst];

  info->meta_sm.iface_binding.complete_cback = rmnet_iface_binding_complete_cb;
  info->meta_sm.iface_binding.user_data = user_data;
}

/*===========================================================================
  FUNCTION RMNET_META_SM_CALL_MULTI_INST_DTR_CB()

  DESCRIPTION
    This function calls RmNet multi instance DTR callback function

  PARAMETERS
    qmi_inst:    QMI instance
    dtr_status:  Flag which indicates current DTR state -
                 TRUE: DTR asserted, FALSE: DTR deasserted

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void rmnet_meta_sm_call_multi_inst_dtr_cb
(
  qmi_instance_e_type   qmi_inst,
  boolean               dtr_status
)
{
  rmnet_smi_dual_ip_info_type    *info;

  ASSERT( (qmi_inst >= QMI_INSTANCE_MIN) && 
          (qmi_inst < QMI_INSTANCE_MAX) && 
          (qmi_inst < (RMNET_INSTANCE_MAX/2)));
  info = &rmnet_smi_dual_ip_info[qmi_inst];

  rmnet_meta_smi_dtr_change_cb(info, dtr_status);

} /* rmnet_meta_sm_call_multi_inst_dtr_cb() */

/*===========================================================================
  FUNCTION RMNET_META_SM_GET_PROC_ID_FROM_INST

  DESCRIPTION
    This function returns the proc_id for a particular rmnet instance

  PARAMETERS
    proc_id : proc_id address
    rmnet_inst : Rmnet instance

  RETURN VALUE
    TRUE: SUCCESS
    FALSE: FAILURE

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean rmnet_meta_sm_get_proc_id_from_inst
(
  rmnet_meta_sm_proc_id_e_type * proc_id,
  rmnet_instance_e_type          rmnet_inst
)
{
  sio_port_id_type sio_port = SIO_PORT_NULL;
  uint32           tmp_proc_id = RMNET_PROC_ID_MAX;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (proc_id == NULL)
  {
    LOG_MSG_ERROR_0("NULL proc_id address");
    return FALSE;
  }

  *proc_id = RMNET_PROC_ID_MAX;

  if (rmnet_inst >= RMNET_INSTANCE_MAX)
  {
    LOG_MSG_ERROR_1("Invalid rmnet inst [%d]", rmnet_inst);
    return FALSE;
  }

  sio_port = rmnet_sio_get_port(rmnet_smi_info[rmnet_inst].meta_sm.rmnet_sio_handle);
  if (sio_port == SIO_PORT_NULL)
  {
    LOG_MSG_ERROR_1("SIO port for rmnet inst [%d] is NULL", rmnet_inst);
    return FALSE;
  }

  if (!rmnet_meta_smi_find_proc_id(&tmp_proc_id, sio_port, rmnet_inst))
  {
    tmp_proc_id = RMNET_PROC_ID_APPS1;
  }
  *proc_id = (rmnet_meta_sm_proc_id_e_type)tmp_proc_id;

  LOG_MSG_INFO2_3("rmnet inst [%d] sio port [0x%x] proc_id [%d]",
                  rmnet_inst, sio_port, *proc_id);
  return TRUE;

} /* rmnet_meta_sm_get_proc_id_from_inst() */


/*===========================================================================

                        INTERNAL FUNCTION DEFINITIONS

===========================================================================*/

/*===========================================================================
  FUNCTION RMNET_META_SMI_FIND_PROC_ID()

  DESCRIPTION
    Utility routine to get proc id. if SNI tlv has sent the call type as
    embedded, we would set proc id to embedded else we would set the proc
    id from sio port

  PARAMETERS
    sio_port
    proc_id
    rmnet_inst

  RETURN VALUE
    TRUE on success
    FALSE on failure

  DEPENDENCIES
    Should only be called once at powerup

  SIDE EFFECTS
    None
===========================================================================*/
boolean rmnet_meta_smi_find_proc_id(
  uint32           *proc_id,
  sio_port_id_type  sio_port,
  rmnet_instance_e_type rmnet_inst
)
{
  uint32 index = 0;
  uint32 laptop_inst = 0;
  boolean result = FALSE;

  LOG_MSG_INFO2_1( "rmnet_meta_smi_find_proc_id: sio_port [0x%x]",
                   (uint32)sio_port );

  if ( proc_id == NULL ) return FALSE;

  // Check if proc id is configured in EFS
  if ( RMNET_IS_VALID_LAPTOP_INSTANCE(rmnet_inst) )
  {
    laptop_inst = RMNET_GET_LAPTOP_INSTANCE(rmnet_inst);
    if ( laptop_inst < RMNET_NUM_LAPTOP_INSTANCES )
    {
      if (rmnet_efs_config_params[laptop_inst].proc_id < RMNET_PROC_ID_MAX)
      {
        *proc_id = rmnet_efs_config_params[laptop_inst].proc_id;
        LOG_MSG_INFO2_2("Rmnet inst %d uses EFS configured proc_id [%d]",
                        rmnet_inst, *proc_id);
        result = TRUE;
        goto bail;
      }
    }
    else
    {
      LOG_MSG_ERROR_2("Invalid laptop inst %d rmnet inst %d",
                      laptop_inst, rmnet_inst);
    }
  }

  //Retrieve the proc id from sio tbl if sio port match
  for ( index = 0; index < SIO_PORT_TO_PROC_ID_MAPPING_TBL_MAX; index++ )
  {
    if ( rmnet_sio_proc_id_tbl[index].sio_port  == sio_port )
    {
      *proc_id = rmnet_sio_proc_id_tbl[index].proc_id;
      LOG_MSG_INFO2_2( "sio_port [0x%x] to proc_id [%d] mapping complete",
                       sio_port, *proc_id );
      result = TRUE;
      goto bail;
    }
  }

bail:
  /*-------------------------------------------------------------------------
    If call type tlv sent in SNI request is embedded then overwrite the 
    proc_id to embedded.
  -------------------------------------------------------------------------*/
  ASSERT(rmnet_inst < RMNET_INSTANCE_MAX);
  if(RMNET_CALL_TYPE_EMBEDDED == 
     rmnet_smi_info[rmnet_inst].meta_sm.net_params.call_type)
  {
    *proc_id = RMNET_PROC_ID_APPS1;
    LOG_MSG_INFO2_0( "Setting proc_id to RMNET_CALL_TYPE_EMBEDDED as "
                     "call type sent in start network interface is embedded" ); 
    result = TRUE;
  }

  if(result == FALSE)
  {
    LOG_MSG_INFO2_0( "Failed to find proc id from sio port or call type" );
  }
  
  return result;
} /* rmnet_meta_smi_find_proc_id() */

/*===========================================================================
  FUNCTION RMNET_META_SMI_INIT()

  DESCRIPTION
    This function initializes the state machine.

  PARAMETERS
    iface_ptr: Pointer to the interface which is used by this instance
               of Rm state machine to bring up pkt calls.

  RETURN VALUE
    TRUE on success
    FALSE on failure

  DEPENDENCIES
    Should only be called once at powerup

  SIDE EFFECTS
    None
===========================================================================*/
static boolean rmnet_meta_smi_init
(
  rmnet_instance_e_type  instance
)
{
  rmnet_smi_info_type *  info;
  nv_stat_enum_type      nv_status;
  boolean                read_autoconnect_nv;
  uint16                 umts_default_profile_num = RMNET_EFS_INVALID_PROFILE;
  uint16                 cdma_default_profile_num = RMNET_EFS_INVALID_PROFILE;
  uint8                  profile_family = 0;
  nv_item_type          *dcc_nv_item_ptr;

  rmnet_instance_e_type                 autoconnect_instance;
  qmi_nv_status_e_type                  qmi_nv_status;
  qmi_nv_autoconnect_pause_type         qmi_nv_autoconnect_pause;
  qmi_nv_autoconnect_roam_setting_type  qmi_nv_autoconnect_roaming;
  qmi_nv_dns_addr_type                  qmi_ipv4_dns_nv_item;
  qmi_nv_ipv6_dns_addr_type             qmi_ipv6_dns_nv_item;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(instance < RMNET_INSTANCE_MAX);
  info = &rmnet_smi_info[instance];
  autoconnect_instance = instance;

  /*-------------------------------------------------------------------------
    Allocate temporary memory for the NV item
  -------------------------------------------------------------------------*/
  dcc_nv_item_ptr = (nv_item_type *)
    qmi_svc_ps_system_heap_mem_alloc(sizeof(nv_item_type),FILE_ID_DS_RMNET_META_SM,__LINE__);
  if( dcc_nv_item_ptr == NULL )
  {
    ASSERT(0);
  }

  /*-------------------------------------------------------------------------
    RMNET_META_SM should be initialized only once.
    Note the "if" check below assumes that rmnet_smi_info is a defined on
    heap so that all its members get inited to 0 (FALSE).
  -------------------------------------------------------------------------*/
  if (info->meta_sm.inited == TRUE)
  {
    LOG_MSG_INFO1_0("RMNET_META_SM already initialized");
    PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_item_ptr);
    return TRUE;
  }

  memset (info, 0, sizeof(rmnet_smi_info_type));
  info->meta_sm.state = RMNET_META_SM_NULL_STATE;

  /*-------------------------------------------------------------------------
    populate the constant values in rmnet_meta_sm_info based on the instance
  -------------------------------------------------------------------------*/
  info->constants.qmi_instance = instance / 2;

  /*-------------------------------------------------------------------------
    Initialize network info params
    set the profile num to default profile. For umts, this value is used for
    autoconnect which is applicable to laptop instance only. Hence ok to set
    the profile family to rmnet_umts_profile_family. For all other cases, the
    caller (wds/qos) ensures that the appropriate profile family is set.
    tech_pref is ANY (WWAN_GROUP as of now)
  -------------------------------------------------------------------------*/
  profile_family = DS_PROFILE_3GPP_RMNET_PROFILE_FAMILY;

  if ( RMNET_IS_VALID_LAPTOP_INSTANCE(instance))
  {
    umts_default_profile_num = (uint16)
     rmnet_efs_config_params[RMNET_GET_LAPTOP_INSTANCE(instance)].umts_profile;
    cdma_default_profile_num = (uint16)
     rmnet_efs_config_params[RMNET_GET_LAPTOP_INSTANCE(instance)].cdma_profile;

    if ( rmnet_efs_config_params[RMNET_GET_LAPTOP_INSTANCE(instance)].ip_type
                                                           != QMI_AF_INET_4_6 )
    {
      /* Set the default IP family pref as per EFS configuration */
      info->meta_sm.net_params.ip_family_pref = (uint8)
       rmnet_efs_config_params[RMNET_GET_LAPTOP_INSTANCE(instance)].ip_type;
    }
    else
    {
      if(info == rmnet_smi_dual_ip_info[info->constants.qmi_instance].info_v4 )
      {
        info->meta_sm.net_params.ip_family_pref = QMI_AF_INET;
      }
      else if(info ==
                 rmnet_smi_dual_ip_info[info->constants.qmi_instance].info_v6 )
      {
        info->meta_sm.net_params.ip_family_pref = QMI_AF_INET6;
      }
    }
  }

  if ( umts_default_profile_num == RMNET_EFS_INVALID_PROFILE )
  {
   (void)rmnet_meta_smi_retrieve_default_profile_num(DS_PROFILE_TECH_3GPP,
                                                    profile_family,
                                                    &umts_default_profile_num);
  }
  info->meta_sm.net_params.umts_profile_num = umts_default_profile_num;

  profile_family = 0;
  if ( cdma_default_profile_num ==RMNET_EFS_INVALID_PROFILE )
  {
    (void)rmnet_meta_smi_retrieve_default_profile_num(DS_PROFILE_TECH_3GPP2,
                                                    profile_family,
                                                    &cdma_default_profile_num);
  }
  info->meta_sm.net_params.cdma_profile_num = (uint8)cdma_default_profile_num;

  info->meta_sm.net_params.tech_pref = WWAN_GROUP;
  info->meta_sm.net_params.subscription_id = PS_SYS_DEFAULT_SUBS;

  /*-------------------------------------------------------------------------
    Initialize the net sio module
  -------------------------------------------------------------------------*/
  info->meta_sm.rmnet_sio_handle = rmnet_sio_init(instance);
  if(info->meta_sm.rmnet_sio_handle == NULL)
  {
    LOG_MSG_INFO1_1( "rmnet_sio_init(%d) returned NULL handle\n", instance );
    PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_item_ptr);
    return FALSE;
  }

  /*-------------------------------------------------------------------------
         Determine the autoconnect settings based on the EFS config cache
      -------------------------------------------------------------------------*/
  if ( (RMNET_IS_VALID_LAPTOP_INSTANCE(instance)) &&
       (rmnet_efs_config_params[RMNET_GET_LAPTOP_INSTANCE(instance)].\
                              autoconnect != RMNET_EFS_AUTOCONNECT_INVALID ))
  {
    info->meta_sm.settings.autoconnect_setting =
        (rmnet_meta_sm_autoconnect_setting_e_type)
              rmnet_efs_config_params[RMNET_GET_LAPTOP_INSTANCE(instance)].\
                                                                  autoconnect;
    LOG_MSG_INFO1_1("EFS autoconnect Setting = %d",
                    info->meta_sm.settings.autoconnect_setting);
  }
  else
  {
    LOG_MSG_INFO1_0("Autoconnect: EFS not configured. Using NV");
    /*-------------------------------------------------------------------------
      Determine autoconnect setting when EFS data is invalid
    -------------------------------------------------------------------------*/
#if defined(FEATURE_DATA_RM_NET_USES_SM) && \
    !defined(FEATURE_DATA_RM_NET_USES_SM_LAPTOP_INST)
    /*-------------------------------------------------------------------------
      RmNet only uses Shared Memory in 3rd party OS builds.  In these cases,
      autoconnect is never required since the driver on the apps processor
      controls the calls.  To ensure the NV item doesn't cause the modem
      processor to make a call unbeknownst to the driver, ensure that the NV
      is ignored by RmNet when it's using smem, i.e. 3rd party OS build
    -------------------------------------------------------------------------*/
    info->meta_sm.settings.autoconnect_setting = RMNET_AUTOCONNECT_DISABLED;
    read_autoconnect_nv = FALSE;
#else
  /*-------------------------------------------------------------------------
    Retrieve autoconnect setting from NV
  -------------------------------------------------------------------------*/
#ifdef FEATURE_DATA_RM_NET_USES_SM_LAPTOP_INST
  if( instance > RMNET_INSTANCE_APPS_PROC_MAX )
  {
    /* since in this case laptop instances start from APPS_PROC_MAX+1 */
      dcc_nv_item.rmnet_autoconnect.index = RMNET_GET_LAPTOP_INSTANCE(instance);
      /* NV_RMNET_AUTOCONNECT_I does not support dual IP autoconnect. Hence only
         one of the 2 dual ip rmnet instances is configured with the NV setting */
        read_autoconnect_nv = (instance % 2)? FALSE:TRUE;
  }
  else
  {
    /* Rmnet instances for A11 have autoconnect disabled*/
    info->meta_sm.settings.autoconnect_setting = RMNET_AUTOCONNECT_DISABLED;
    read_autoconnect_nv = FALSE;
  }
#else
  dcc_nv_item.rmnet_autoconnect.index = RMNET_GET_LAPTOP_INSTANCE(instance);
  /* NV_RMNET_AUTOCONNECT_I does not support dual IP autoconnect. Hence only
     one of the 2 dual ip rmnet instances is configured with the NV setting */
  if ( RMNET_IS_VALID_LAPTOP_INSTANCE(instance) )
  {
    read_autoconnect_nv = (instance % 2)? FALSE:TRUE;
  }
  else
  {
    read_autoconnect_nv =  FALSE;
  }
#endif /* FEATURE_DATA_RM_NET_USES_SM_LAPTOP_INST */
#endif /* defined(FEATURE_DATA_RM_NET_USES_SM) &&
          !defined(FEATURE_DATA_RM_NET_USES_SM_LAPTOP_INST) */

  /*-------------------------------------------------------------------------
    Cache filled with default values first.
    autoconnect_setting            = 0 (autoconnect Disabled)                                     .
    autoconnect_roam_setting       = 0xFF (unknown)                                               .
    auto_connect_enabled           = FALSE                                                        .
  -------------------------------------------------------------------------*/
  info->meta_sm.settings.auto_connect_enabled      = FALSE;
  info->meta_sm.settings.autoconnect_setting       = RMNET_AUTOCONNECT_DISABLED;
  info->meta_sm.settings.autoconnect_roam_setting  = RMNET_AUTOCONNECT_ROAM_SETTING_MAX;

  memset(&qmi_nv_autoconnect_roaming, 0, sizeof(qmi_nv_autoconnect_roam_setting_type));
  memset(&qmi_nv_autoconnect_pause, 0, sizeof(qmi_nv_autoconnect_pause_type));

  if(read_autoconnect_nv == TRUE)
  {
    /*-----------------------------------------------------------------------
      read autoconnect NV(3534) and autoconnect pause QMI NV to get currently
      stored valuse.
    -----------------------------------------------------------------------*/
    dcc_nv_item_ptr->rmnet_autoconnect.index =
      RMNET_GET_LAPTOP_INSTANCE(autoconnect_instance);

    nv_status = dcc_get_nv_item( NV_RMNET_AUTOCONNECT_I, dcc_nv_item_ptr );
    if (NV_DONE_S != nv_status )
    {
      dcc_nv_item_ptr->rmnet_autoconnect.enable = FALSE;
      if( NV_NOTACTIVE_S == nv_status )
      {
        LOG_MSG_INFO1_0 (" Autoconnect NV not active ");
      }
      else
      {
        LOG_MSG_ERROR_0("Couldn't retrieve RMNET autoconnect setting! using default (OFF)");
      }
    }

    qmi_nv_autoconnect_pause.index = autoconnect_instance/2;
    qmi_nv_status = qmi_nv_read(QMI_NV_ITEM_AUTOCONNECT_PAUSE_SETTING,
                                qmi_nv_autoconnect_pause.index ,
                                &qmi_nv_autoconnect_pause,
                                sizeof(qmi_nv_autoconnect_pause_type));

    /*-----------------------------------------------------------------------
    During the init time based on NV values need to set autoconnect setting
    See below TT table for autoconnect setting and nv writes.

    T = TRUE, F = FALSE, N/A = Non Active, def = default  ac = autoconnect
    ---------------------------------------------------------------------
      Currently  Currently
      stored     stored
      NV3534     pause NV
      value      value     Result NV settings and autoconnect_setting.
    ----------------------------------------------------------------------
      N/A         N/A      write 3534 = F(def), ac_setting = 0
    ----------------------------------------------------------------------
      T           N/A      ac_setting = 1, No nv writes
    ----------------------------------------------------------------------
      F           N/A      ac_setting = 0, No nv writes
    ----------------------------------------------------------------------
      N/A         1        write 3534 = T and pause NV =0, ac_setting = 1
    ----------------------------------------------------------------------
      N/A         0        write 3534 = F(def), ac_setting = 0
    ----------------------------------------------------------------------
      T           0        ac_setting = 1, No nv writes
    ----------------------------------------------------------------------
      T           1        write pause NV =0, ac_setting = 1
    ----------------------------------------------------------------------
      F           1        write 3534 = T and pause NV =0, ac_setting = 1
    ----------------------------------------------------------------------
      F           0        ac_setting = 0, No nv writes
    ---------------------------------------------------------------------*/

    /*-----------------------------------------------------------------------
      if autoconnect pause setting is on then set autoconnect NV(3535) to TRUE
      and set autoconnect pause setting to off.
      So from the above table Row 4, 7 and 8 taken care of
    -----------------------------------------------------------------------*/
    if ((QMI_NV_STATUS_OK == qmi_nv_status) &&
        (RMNET_AUTOCONNECT_PAUSE_ON == qmi_nv_autoconnect_pause.pause_setting))
    {
      if(!dcc_nv_item_ptr->rmnet_autoconnect.enable)
      {
        /*----------------------------------------------------------------------
          Set autoconnect NV(3535) to TRUE
        ----------------------------------------------------------------------*/
        dcc_nv_item_ptr->rmnet_autoconnect.enable = TRUE;
        nv_status = dcc_put_nv_item( NV_RMNET_AUTOCONNECT_I, dcc_nv_item_ptr );
      }

      if((NV_DONE_S == nv_status) &&
         (dcc_nv_item_ptr->rmnet_autoconnect.enable))
      {
        /*----------------------------------------------------------------------
          Set pause autoconnect pause to RMNET_AUTOCONNECT_PAUSE_OFF
        ----------------------------------------------------------------------*/
        qmi_nv_autoconnect_pause.index = autoconnect_instance/2;
        qmi_nv_autoconnect_pause.pause_setting = RMNET_AUTOCONNECT_PAUSE_OFF;
        qmi_nv_status = qmi_nv_write(QMI_NV_ITEM_AUTOCONNECT_PAUSE_SETTING,
                                     qmi_nv_autoconnect_pause.index,
                                     &qmi_nv_autoconnect_pause,
                                     sizeof(qmi_nv_autoconnect_pause_type));

        /*----------------------------------------------------------------------
          Set autoconnect setting cache value to Enabled
        ----------------------------------------------------------------------*/
        if (QMI_NV_STATUS_OK == qmi_nv_status )
        {
          info->meta_sm.settings.autoconnect_setting =
                                            RMNET_AUTOCONNECT_ENABLED;
        }
      }
    }
    else
    {
      /* set the cache value to the current autoconnect NV(3534) NV value */
      info->meta_sm.settings.autoconnect_setting  =
                 dcc_nv_item_ptr->rmnet_autoconnect.enable ?
                    RMNET_AUTOCONNECT_ENABLED : RMNET_AUTOCONNECT_DISABLED;
    }
    /*-------------------------------------------------------------------------
      read autoconnect roaming setting from QMI NV
    -------------------------------------------------------------------------*/
    qmi_nv_autoconnect_roaming.index = autoconnect_instance/2;
    qmi_nv_status = qmi_nv_read(QMI_NV_ITEM_AUTOCONNECT_ROAM_SETTING,
                                autoconnect_instance/2 ,
                                &qmi_nv_autoconnect_roaming,
                                sizeof(qmi_nv_autoconnect_roam_setting_type));
    if (QMI_NV_STATUS_OK == qmi_nv_status)
    {
      /* update roam setting cache */
      info->meta_sm.settings.autoconnect_roam_setting =
                                        qmi_nv_autoconnect_roaming.roam_setting;
    }
  }
}
  /*-------------------------------------------------------------------------
    Determine the new autoconnect state once settings read
  -------------------------------------------------------------------------*/
  rmnet_meta_smi_update_autoconnect_state(autoconnect_instance);

  /*-------------------------------------------------------------------------
    Determine manual DNS settings (IPv4 and IPv6)
  -------------------------------------------------------------------------*/
  info->meta_sm.settings.pri_dns_addr.type = IP_ADDR_INVALID;
  info->meta_sm.settings.pri_dns_addr.addr.v4 = 0;
  info->meta_sm.settings.sec_dns_addr.type = IP_ADDR_INVALID;
  info->meta_sm.settings.sec_dns_addr.addr.v4 = 0;

  info->meta_sm.settings.ipv6_pri_dns_addr.type = IP_ADDR_INVALID;
  info->meta_sm.settings.ipv6_pri_dns_addr.addr.v6[0] = 0;
  info->meta_sm.settings.ipv6_pri_dns_addr.addr.v6[1] = 0;
  info->meta_sm.settings.ipv6_sec_dns_addr.type = IP_ADDR_INVALID;
  info->meta_sm.settings.ipv6_sec_dns_addr.addr.v6[0] = 0;
  info->meta_sm.settings.ipv6_sec_dns_addr.addr.v6[1] = 0;

  /* Get Sec IPv4 DNS setting */
  qmi_ipv4_dns_nv_item.index = instance/2;
  qmi_nv_status = qmi_nv_read(QMI_NV_ITEM_MANUAL_DNS_IPV4_SEC,
                              (uint8)instance/2,
                              &qmi_ipv4_dns_nv_item,
                              sizeof(qmi_ipv4_dns_nv_item));
  if (QMI_NV_STATUS_OK == qmi_nv_status)
  {
    LOG_MSG_INFO2_1("Manual IPv4 DNS setting (sec) read [%x]",
                    qmi_ipv4_dns_nv_item.dns_addr);
    info->meta_sm.settings.sec_dns_addr.type = IPV4_ADDR;
    info->meta_sm.settings.sec_dns_addr.addr.v4 = qmi_ipv4_dns_nv_item.dns_addr;
  }

  /* Get Pri IPv6 DNS setting */
  qmi_ipv6_dns_nv_item.index = instance/2;
  qmi_nv_status = qmi_nv_read(QMI_NV_ITEM_MANUAL_DNS_IPV6_PRI,
                              (uint8)instance/2,
                              &qmi_ipv6_dns_nv_item,
                              sizeof(qmi_ipv6_dns_nv_item));
  if (QMI_NV_STATUS_OK == qmi_nv_status)
  {
    LOG_MSG_INFO2_2("Manual IPv6 DNS setting (pri) read [%x %x]",
                    qmi_ipv6_dns_nv_item.dns_addr[0],
                    qmi_ipv6_dns_nv_item.dns_addr[1]);
    info->meta_sm.settings.ipv6_pri_dns_addr.type = IPV6_ADDR;
    info->meta_sm.settings.ipv6_pri_dns_addr.addr.v6[0] = qmi_ipv6_dns_nv_item.dns_addr[0];
    info->meta_sm.settings.ipv6_pri_dns_addr.addr.v6[1] = qmi_ipv6_dns_nv_item.dns_addr[1];
  }

  /* Get Sec IPv6 DNS setting */
  qmi_ipv6_dns_nv_item.index = instance/2;
  qmi_nv_status = qmi_nv_read(QMI_NV_ITEM_MANUAL_DNS_IPV6_SEC,
                              (uint8)instance/2,
                              &qmi_ipv6_dns_nv_item,
                              sizeof(qmi_ipv6_dns_nv_item));
  if (QMI_NV_STATUS_OK == qmi_nv_status)
  {
    LOG_MSG_INFO2_2("Manual IPv6 DNS setting (sec) read [%x %x]",
                    qmi_ipv6_dns_nv_item.dns_addr[0],
                    qmi_ipv6_dns_nv_item.dns_addr[1]);
    info->meta_sm.settings.ipv6_sec_dns_addr.type = IPV6_ADDR;
    info->meta_sm.settings.ipv6_sec_dns_addr.addr.v6[0] = qmi_ipv6_dns_nv_item.dns_addr[0];
    info->meta_sm.settings.ipv6_sec_dns_addr.addr.v6[1] = qmi_ipv6_dns_nv_item.dns_addr[1];
  }

  /*-------------------------------------------------------------------------
    Initialize the ps_iface cback buf_ptrs for Um iface event handling
  -------------------------------------------------------------------------*/
// put the event bufs below into an array indexed by IFACE_EV of interest
  info->meta_sm.um_iface_active_out_of_use_buf_ptr
                                          = ps_iface_alloc_event_cback_buf(
                                             rmnet_meta_smi_um_iface_ev_hdlr_cb,
                                             (void *) info
                                           );
  info->meta_sm.um_iface_up_buf_ptr       = ps_iface_alloc_event_cback_buf(
                                             rmnet_meta_smi_um_iface_ev_hdlr_cb,
                                             (void *) info
                                           );
  info->meta_sm.um_iface_down_buf_ptr     = ps_iface_alloc_event_cback_buf(
                                             rmnet_meta_smi_um_iface_ev_hdlr_cb,
                                             (void *) info
                                           );

  info->meta_sm.um_iface_authenticating_buf_ptr = ps_iface_alloc_event_cback_buf(
                                             rmnet_meta_smi_um_iface_ev_hdlr_cb,
                                             (void *) info
                                           );

  info->meta_sm.um_iface_lingering_buf_ptr  = ps_iface_alloc_event_cback_buf(
                                             rmnet_meta_smi_um_iface_ev_hdlr_cb,
                                             (void *) info
                                           );

  info->meta_sm.um_iface_bearer_tech_change_buf_ptr =
            ps_iface_alloc_event_cback_buf(
                                            rmnet_meta_smi_um_iface_ev_hdlr_cb,
                                            (void *) info
                                           );
  info->meta_sm.um_flow_enabled_buf_ptr   = ps_iface_alloc_event_cback_buf(
                                             rmnet_meta_smi_um_iface_ev_hdlr_cb,
                                             (void *) info
                                           );

  info->meta_sm.um_flow_disabled_buf_ptr  = ps_iface_alloc_event_cback_buf(
                                             rmnet_meta_smi_um_iface_ev_hdlr_cb,
                                             (void *) info
                                           );

  info->meta_sm.um_phys_link_flow_enabled_buf_ptr = ps_phys_link_alloc_event_cback_buf(
                                              rmnet_meta_smi_um_phys_link_ev_hdlr_cb,
                                              (void *)info
                                            );

  info->meta_sm.um_phys_link_flow_disabled_buf_ptr = ps_phys_link_alloc_event_cback_buf(
                                               rmnet_meta_smi_um_phys_link_ev_hdlr_cb,
                                               (void *)info
                                             );

  info->meta_sm.um_flow_tx_enabled_buf_ptr = ps_flow_alloc_event_cback_buf(
                                              rmnet_meta_smi_um_flow_ev_hdlr_cb,
                                              (void *)info
                                            );

  info->meta_sm.um_flow_tx_disabled_buf_ptr = ps_flow_alloc_event_cback_buf(
                                              rmnet_meta_smi_um_flow_ev_hdlr_cb,
                                              (void *)info
                                            );

  info->meta_sm.um_phys_link_up_buf_ptr   = ps_phys_link_alloc_event_cback_buf(
                                             rmnet_meta_smi_um_phys_link_ev_hdlr_cb,
                                             (void *) info
                                           );

  info->meta_sm.um_phys_link_down_buf_ptr   = ps_phys_link_alloc_event_cback_buf(
                                             rmnet_meta_smi_um_phys_link_ev_hdlr_cb,
                                             (void *) info
                                           );
  /*-------------------------------------------------------------------------
    return false if any event buffer allocation failed
  -------------------------------------------------------------------------*/
  if ( ( info->meta_sm.um_iface_active_out_of_use_buf_ptr == NULL )     ||
       ( info->meta_sm.um_iface_down_buf_ptr == NULL )     ||
       ( info->meta_sm.um_iface_up_buf_ptr == NULL )       ||
       ( info->meta_sm.um_iface_authenticating_buf_ptr == NULL )     ||
       ( info->meta_sm.um_iface_lingering_buf_ptr == NULL )     ||
       ( info->meta_sm.um_flow_enabled_buf_ptr == NULL )     ||
       ( info->meta_sm.um_flow_disabled_buf_ptr == NULL )    ||
       ( info->meta_sm.um_phys_link_flow_enabled_buf_ptr == NULL )   ||
       ( info->meta_sm.um_phys_link_flow_disabled_buf_ptr == NULL ) ||
       ( info->meta_sm.um_flow_tx_enabled_buf_ptr == NULL ) ||
       ( info->meta_sm.um_flow_tx_disabled_buf_ptr == NULL) ||
       (info->meta_sm.um_iface_bearer_tech_change_buf_ptr == NULL) ||
       ( info->meta_sm.um_phys_link_up_buf_ptr == NULL )   ||
       ( info->meta_sm.um_phys_link_down_buf_ptr == NULL ))
  {
    LOG_MSG_ERROR_0("Event buf allocation failed");
    ASSERT(0);
  }

  /*-------------------------------------------------------------------------
    set the initial link protocol tx function to not use TLP, and register
    for notification when TLP setting changes.
  -------------------------------------------------------------------------*/
  info->meta_sm.llc_started  = FALSE;
  info->meta_sm.lp_tx_f_ptr  = rmnet_meta_smi_rm_tx_data;

 /*-------------------------------------------------------------------------
      Set the default data format
  -------------------------------------------------------------------------*/
  rmnet_meta_smi_set_default_data_format(instance);

  timer_def( &(info->meta_sm.autoconnect_timer),
             NULL,
             NULL,
             0,
             rmnet_meta_smi_autoconnect_timer_cback,
             (timer_cb_data_type)info );

  info->meta_sm.autoconnect_next_backoff_time = AUTOCONNECT_BACKOFF_TIMER_MIN_VALUE;

#ifdef FEATURE_DATA_NO_QMI_TEST
  /*-------------------------------------------------------------------------
    NO QMI DATA CALL BRINGUP TEST
  -------------------------------------------------------------------------*/
  if (info->meta_sm.settings.auto_connect_enabled)
  {
    info->meta_sm.auto_connect_trigger = FALSE;
    (void) timer_set( &(info->meta_sm.autoconnect_timer),
                        5, 0, T_SEC );
  }
#endif /* FEATURE_DATA_NO_QMI_TEST */

  /*-------------------------------------------------------------------------
    Initialize call end reason
  -------------------------------------------------------------------------*/
  info->meta_sm.call_end_reason = PS_NET_DOWN_REASON_NOT_SPECIFIED;

  /*-------------------------------------------------------------------------
    Initialize flag to monitor Iface teardown because of arbitration
  -------------------------------------------------------------------------*/
  info->meta_sm.arb_close_in_progress  = FALSE;
  info->meta_sm.proc_id                = RMNET_PROC_ID_MAX;

  /*-------------------------------------------------------------------------
    Initialize address configuration state machines.
  -------------------------------------------------------------------------*/
  rmnet_v4_sm_init(instance);

#ifdef FEATURE_DATA_PS_IPV6
  rmnet_v6_sm_init(instance);
#endif /* FEATURE_DATA_PS_IPV6 */

  /*-------------------------------------------------------------------------
    Initialization succeeded
  -------------------------------------------------------------------------*/
  info->meta_sm.inited = TRUE;

  PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_item_ptr);
  return TRUE;
} /* rmnet_meta_smi_init() */



/*===========================================================================
  FUNCTION RMNET_META_SMI_IFACE_INIT()

  DESCRIPTION
    This function creats the Rmnet Ifaces and initializes them.

  PARAMETERS
    uint8: iface number

  RETURN VALUE
    TRUE on success
    FALSE on failure

  DEPENDENCIES
    Should only be called once at powerup

  SIDE EFFECTS
    None
===========================================================================*/
static boolean rmnet_meta_smi_iface_init
(
  uint8 i
)
{
  rmnet_smi_iface_info_type *  info;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(i < RMNET_IFACE_MAX);
  info = rmnet_smi_iface_info[i];
  if (info != NULL)
  {
    LOG_MSG_ERROR_0("Info not NULL");
    ASSERT(0);
    return FALSE;
  }

  // Allocate iface info structure
  info = qmi_svc_ps_system_heap_mem_alloc(
                   sizeof(rmnet_smi_iface_info_type),FILE_ID_DS_RMNET_META_SM,__LINE__);
  if (info == NULL)
  {
    return FALSE;
  }

  memset(info, 0, sizeof(rmnet_smi_iface_info_type));
  rmnet_smi_iface_info[i] = info;

  /*-------------------------------------------------------------------------
    RmNet iface is created and managed by RmNetSm.  It is another instance
    of the (rm) SIO_IFACE.

    As SIO won't ever have more than a single physlink use '1' explicitly.

    ACLs are needed for DHCP server to resolve the RmNet interface during
    socket2() call w/ policy info.

    This is created before qmux_init so WDS can register iface callbacks on
    rmnet iface at initialization time.
  -------------------------------------------------------------------------*/
  info->rmnet_sio_acl.acl_fptr       = rmnet_sio_rt_acl;
  info->rmnet_sio_acl.post_proc_fptr = rmnet_sio_rt_acl_post_proc;
  if ( ps_iface_create(&info->rmnet_iface,
                       SIO_IFACE, //resolve bc_rm
                       &info->rmnet_sio_acl,      /* outgoing ACL */
                       NULL,                      /* incoming ACL */
                       &info->rmnet_pkt_phys_link,
                       1) == -1 )
  {
    LOG_MSG_ERROR_1("Failed to create rmnet iface [%d]", i);
    ASSERT(0);
    PS_SYSTEM_HEAP_MEM_FREE(rmnet_smi_iface_info[i]);
    return FALSE;
  }

  info->rmnet_iface.group_mask = (uint16)RM_GROUP;
  /* initialize ioctl handler */
  info->rmnet_iface.iface_ioctl_f_ptr = rmnet_iface_ioctl_hdlr;

  /*-------------------------------------------------------------------------
    Support logging on this interface
    - interface description is the default text (NULL)
  -------------------------------------------------------------------------*/
  if( i < RMNET_IFACE_MAX )
  {
    (void) ps_iface_dpl_support_network_logging(
             &info->rmnet_iface,
             dpl_ifname_by_rmnet_instance[i] );
    //add to info
     // MPDP - replace DPL ifname above by rmnet iface instnace
    (void) ps_iface_dpl_set_iface_desc(&info->rmnet_iface, NULL);

    (void) ps_iface_dpl_support_link_logging(
             &info->rmnet_iface,
             dpl_ifname_by_rmnet_instance[i] );
  }

  /*-------------------------------------------------------------------------
    Enable the RMNET interface
    bringup will fail as we don't register bring_up_cmd_hdlr
    bring up and tear down functions are not registered since sockets
    origination over SIO is not supported by this RMNET_META_SM.
  -------------------------------------------------------------------------*/
  ps_iface_enable_ind( &info->rmnet_iface );

  /*-------------------------------------------------------------------------
    Allow link layer broadcast addressing on this interface
  -------------------------------------------------------------------------*/
  PS_IFACE_SET_IS_BCAST_IFACE( &info->rmnet_iface, TRUE );

  /* Also init the lan_llc_instance and RX_Sig
     For these also (like ifaces) only a MAX of 5 are needed
     since only a max of 5 calls (3 unicast + 2 mcast) will be supported
     across 5 (A11) + 3 (laptop) Rmnet instances */

  switch(i)
  {
    case 0:
      info->constants.lan_llc_instance = LAN_LLE_RMNET1;
      info->constants.rx_sig = PS_RX_RMNET1_LLC1_SIGNAL;
      break;

    case 1:
      info->constants.lan_llc_instance = LAN_LLE_RMNET1_2;
      info->constants.rx_sig = PS_RX_RMNET1_LLC2_SIGNAL;
      break;

    case 2:
      info->constants.lan_llc_instance = LAN_LLE_RMNET2;
      info->constants.rx_sig = PS_RX_RMNET2_LLC1_SIGNAL;
      break;

    case 3:
      info->constants.lan_llc_instance = LAN_LLE_RMNET2_2;
      info->constants.rx_sig = PS_RX_RMNET2_LLC2_SIGNAL;
      break;

    case 4:
      info->constants.lan_llc_instance = LAN_LLE_RMNET3;
      info->constants.rx_sig = PS_RX_RMNET3_LLC1_SIGNAL;
      break;

    case 5:
      info->constants.lan_llc_instance = LAN_LLE_RMNET3_2;
      info->constants.rx_sig = PS_RX_RMNET3_LLC2_SIGNAL;
      break;

    case 6:
      info->constants.lan_llc_instance = LAN_LLE_RMNET4;
      info->constants.rx_sig = PS_RX_RMNET4_LLC1_SIGNAL;
      break;

    case 7:
      info->constants.lan_llc_instance = LAN_LLE_RMNET4_2;
      info->constants.rx_sig = PS_RX_RMNET4_LLC2_SIGNAL;
      break;
    case 8:
      info->constants.lan_llc_instance = LAN_LLE_RMNET5;
      info->constants.rx_sig = PS_RX_RMNET5_LLC1_SIGNAL;
      break;

    case 9:
      info->constants.lan_llc_instance = LAN_LLE_RMNET5_2;
      info->constants.rx_sig = PS_RX_RMNET5_LLC2_SIGNAL;
      break;

    case 10:
      info->constants.lan_llc_instance = LAN_LLE_RMNET6;
      info->constants.rx_sig = PS_RX_RMNET6_LLC1_SIGNAL;
      break;

    case 11:
      info->constants.lan_llc_instance = LAN_LLE_RMNET6_2;
      info->constants.rx_sig = PS_RX_RMNET6_LLC2_SIGNAL;
      break;

    case 12:
      info->constants.lan_llc_instance = LAN_LLE_RMNET7;
      info->constants.rx_sig = PS_RX_RMNET7_LLC1_SIGNAL;
      break;

    case 13:
      info->constants.lan_llc_instance = LAN_LLE_RMNET7_2;
      info->constants.rx_sig = PS_RX_RMNET7_LLC2_SIGNAL;
      break;

    case 14:
      info->constants.lan_llc_instance = LAN_LLE_RMNET8;
      info->constants.rx_sig = PS_RX_RMNET8_LLC1_SIGNAL;
      break;

    case 15:
      info->constants.lan_llc_instance = LAN_LLE_RMNET8_2;
      info->constants.rx_sig = PS_RX_RMNET8_LLC2_SIGNAL;
      break;

    default:
      LOG_MSG_ERROR_1("Unknown rmnet iface inst [%d]", i);
  }

  return TRUE;
}


/*===========================================================================
  FUNCTION RMNET_META_SMI_SET_TLP_CHANGE_CMD_HDLR()

  DESCRIPTION
    Specify the callback function that should be invoked when the USB TLP
    settings change

  PARAMETERS
    dl_ver => negotiated version of TLP for downlink
    ul_ver => negotiated version of TLP for uplink
    void * => user data (rmnet_meta_sm_info pointer)

  RETURN VALUE
    None

  DEPENDENCIES
    rmnet_sio_init() must have been called previously

  SIDE EFFECTS
    None
===========================================================================*/
static void rmnet_meta_smi_tlp_change_cmd_hdlr
(
  rmnet_sio_tlp_ver_type  dl_ver,
  rmnet_sio_tlp_ver_type  ul_ver,
  void *                  user_data
)
{
  rmnet_smi_info_type *    info;
  lan_llc_start_info_type  start_info;
  rmnet_instance_e_type    rmnet_inst;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(user_data);
  info = (rmnet_smi_info_type *) user_data;
  rmnet_inst = RMNET_META_SM_INFO_TO_INSTANCE(info);

  LOG_MSG_INFO2_3 ("TLP setting changed (DL: %d UL: %d), RMNET inst %d",
                   dl_ver, ul_ver, rmnet_inst);

  info->meta_sm.lp_tx_f_ptr = dl_ver ? rmnet_meta_smi_rm_tlp_tx_data
                              : (info->meta_sm.data_agg_protocol.dl_data_agg_protocol
                                == RMNET_ENABLE_DATA_AGG_MBIM ||
                                info->meta_sm.data_agg_protocol.dl_data_agg_protocol
                                == RMNET_ENABLE_DATA_AGG_QC_NCM) ?
                                rmnet_meta_smi_rm_tx_data_mbim:
                                rmnet_meta_smi_rm_tx_data;

  if (info->meta_sm.llc_started)
  {
    start_info.tx_f_ptr        = (lan_llc_tx_f_ptr_type)(info->meta_sm.lp_tx_f_ptr);
    start_info.tx_f_user_data  = info;
    (void) lan_llc_reconfig( info->constants.lan_llc_instance, &start_info );
  }
} /* rmnet_meta_smi_tlp_change_cmd_hdlr() */


#ifndef FEATURE_DATA_RM_NET_USES_SM
/*===========================================================================
  FUNCTION RMNET_META_SMI_TLP_CHANGE_MULT_INST_CB()

  DESCRIPTION
    Specify the callback function that should be invoked when the USB TLP
    settings change

  PARAMETERS
    dl_ver => negotiated version of TLP for downlink
    ul_ver => negotiated version of TLP for uplink
    void * => user data (rmnet_meta_sm_info pointer)

  RETURN VALUE
    None

  DEPENDENCIES
    rmnet_sio_init() must have been called previously

  SIDE EFFECTS
    None
===========================================================================*/
static void rmnet_meta_smi_tlp_change_mult_inst_cb
(
  rmnet_sio_tlp_ver_type  dl_ver,
  rmnet_sio_tlp_ver_type  ul_ver,
  void *                  user_data
)
{
  rmnet_smi_info_type *  info;
  ASSERT(user_data);
  info = (rmnet_smi_info_type *) user_data;
  rmnet_meta_smi_tlp_change_cb(dl_ver, ul_ver, rmnet_smi_dual_ip_info[info->constants.qmi_instance].info_v4);
  rmnet_meta_smi_tlp_change_cb(dl_ver, ul_ver, rmnet_smi_dual_ip_info[info->constants.qmi_instance].info_v6);
} /* rmnet_meta_smi_tlp_change_mult_inst_cb() */
/*===========================================================================
  FUNCTION RMNET_META_SMI_TLP_CHANGE_CB()

  DESCRIPTION
    Specify the callback function that should be invoked when the USB TLP
    settings change

  PARAMETERS
    dl_ver => negotiated version of TLP for downlink
    ul_ver => negotiated version of TLP for uplink
    void * => user data (rmnet_meta_sm_info pointer)

  RETURN VALUE
    None

  DEPENDENCIES
    rmnet_sio_init() must have been called previously

  SIDE EFFECTS
    None
===========================================================================*/
static void rmnet_meta_smi_tlp_change_cb
(
  rmnet_sio_tlp_ver_type  dl_ver,
  rmnet_sio_tlp_ver_type  ul_ver,
  void *                  user_data
)
{
    rmnet_smi_cmd_type * cmd_ptr;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------------
    Post a cmd to QMI (DCC task)
  -------------------------------------------------------------------------*/
  cmd_ptr = (rmnet_smi_cmd_type *)RMNET_SMI_GET_CMD_BUF(DCC_RMNET_SM_CMD,FALSE);
  if( cmd_ptr == NULL)
  {
    return;
  }

  cmd_ptr->sm_id     = RMNET_META_SM;
  cmd_ptr->cmd_type  = RMNET_SM_CMD_TLP_CHANGE;
  cmd_ptr->info_ptr  = (void *) user_data;
  cmd_ptr->data.tlp_change.dl_ver  = dl_ver;
  cmd_ptr->data.tlp_change.ul_ver  = ul_ver;
  dcc_send_cmd_ex(DCC_RMNET_SM_CMD, (void *)cmd_ptr);

} /* rmnet_meta_smi_tlp_change_cb() */
#endif /* FEATURE_DATA_RM_NET_USES_SM */

/*===========================================================================
  FUNCTION RMNET_META_SM_GET_RM_IFACE_PTR_FROM_INSTANCE

  DESCRIPTION
    This function returns the RM IFACE PTR allocated for a particular call on a
    particular instance.

  PARAMETERS
  instance: Rmnet Instance on which the call was established.

  RETURN VALUE
    Rm Iface Pointer.

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
ps_iface_type* rmnet_meta_sm_get_laptop_rm_iface_ptr
(
  rmnet_instance_e_type instance
)
{
  ASSERT( instance < RMNET_INSTANCE_MAX );

  if ( !RMNET_IS_VALID_LAPTOP_INSTANCE(instance) )
  {
    LOG_MSG_ERROR_1("Invalid laptop  instance %d", instance);
    return NULL;
  }
  return rmnet_smi_info[instance].rmnet_iface_ptr;
}/* rmnet_meta_smi_get_um_iface_ptr */


/*===========================================================================
  FUNCTION RMNET_META_SMI_GET_UM_IFACE_PTR


  DESCRIPTION
    This function retrieves the pointer to UM iface in the system which will
    be used to originate the packet data call.  The iface pointer is
    obtained by routing lookup.

    An UM iface is returned only if there is one available and it is not
    in IFACE_DOWN state.

    If the acl_pol_ptr is not NULL, then the acl policy information is
    also returned in the acl_pol_ptr parameter

  PARAMETERS
    net_params    - Parameters to build the acl policy
    rt_lk_up_type - Route lookup type option
    acl_pol_ptr   - If non-NULL, acl_policy is returned to the caller

  RETURN VALUE
    Pointer to allocated interface, or NULL if none available

  DEPENDENCIES
    Note that context lock is required because sockets calls originate
    in PS task context.  So we need to make sure that if we decide to
    originate on a particular iface, IFACE_state should be set to
    something other than DOWN and DISABLED and the UM packet iface
    ACLs should be disabled to prevent sockets call before
    freeing up the locks. This ensures only one type of call originates
    at a time on each interface.

  SIDE EFFECTS
    None
===========================================================================*/
ps_iface_type* rmnet_meta_smi_get_um_iface_ptr
(
  rmnet_meta_sm_network_info_type *   net_params,
  rmnet_meta_sm_route_look_up_e_type  rt_lk_up_type, /* bring_up/look up only */
  acl_policy_info_type            *   acl_pol_ptr,
  rmnet_instance_e_type               rmnet_inst,
  rmnet_meta_sm_arb_info_type     *   arb_info_ptr
)
{
  ps_iface_type *           um_iface_ptr;
  acl_policy_info_type      acl_policy_info;
//  ip_pkt_info_type          ip_info;
  uint32 iface_priority_mask = 0;
  uint8  arb_candidate = 0;
  uint64            tmp_app_id;
  sio_port_id_type  sio_port;
  uint32            proc_id = RMNET_PROC_ID_MAX;
  rmnet_smi_info_type *info;
  ps_iface_net_down_reason_type   net_down_reason;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------------
    Resolve the Um inteface
    make sure ROUTE_DEFAULT_DST is in network order (it's 0x00000000 for now)
  -------------------------------------------------------------------------*/
//  memset( &ip_info, 0, sizeof(ip_info) );

  ASSERT(rmnet_inst < RMNET_INSTANCE_MAX);
  /* check that rmnet_inst is within range in case ASSERT is removed */
  if (rmnet_inst >= RMNET_INSTANCE_MAX)
  {
    LOG_MSG_ERROR_1("RMNET instance out of range (%d)!", rmnet_inst);
    return NULL;
  }

  info = &rmnet_smi_info[rmnet_inst];

  memset( &acl_policy_info, 0, sizeof(acl_policy_info_type) );
  acl_init_policy_info(&acl_policy_info);

  acl_policy_info.policy_flag     = DSS_IFACE_POLICY_ANY;
  acl_policy_info.iface.kind      = DSS_IFACE_NAME;
  acl_policy_info.iface.info.name = DSS_IFACE_WWAN; //DSS_IFACE_ANY;

  switch(rt_lk_up_type)
  {
    case RMNET_RT_LK_UP_TYPE_BRINGUP:
      acl_policy_info.bring_up        = TRUE;
      break;

    case RMNET_RT_LK_UP_TYPE_IFACE:
      acl_policy_info.lookup_only = TRUE;
      break;

    case RMNET_RT_LK_UP_TYPE_DATAPATH:
      acl_policy_info.data_path_policy_flag = PS_ACL_POLICY_ROUTEABLE_ONLY;
      /* do nothing as both flags are supposed to be FALSE */
      break;

    default:
      ASSERT(0);
      return NULL;
  }

  switch(net_params->ip_family_pref)
  {
    case QMI_AF_ANY:     /* not supported currently */
    case QMI_AF_INET:
      acl_policy_info.ip_family       = IFACE_IPV4_ADDR_FAMILY;
      break;

#ifdef FEATURE_DATA_PS_IPV6
    case QMI_AF_INET6:
      acl_policy_info.ip_family       = IFACE_IPV6_ADDR_FAMILY;
      break;
#endif /* FEATURE_DATA_PS_IPV6 */

    case QMI_AF_UNSPEC:
      acl_policy_info.ip_family       = IFACE_UNSPEC_ADDR_FAMILY;
      break;

    default:
      LOG_MSG_ERROR_1("Invalid RmNet IP family: %d!",
                      net_params->ip_family_pref);
      return NULL;
  }
  /*-------------------------------------------------------------------------
    TO DO - possibly move this in a function in wds
    When you reach here tech_pref WILL have a value from the enum
    ps_iface_name_enum_type
  -------------------------------------------------------------------------*/

  acl_policy_info.iface.info.name =
                            (dss_iface_name_enum_type) net_params->tech_pref;

  /* populate umts profile num: upper byte contains profile family and lower
     byte contains the profile index. Caller of this function needs to ensure
     that both bytes get populated appropriately */
  acl_policy_info.pdp_info = (acl_pdp_ctx_cfg_type)(uint32)
                                              (net_params->umts_profile_num);

  /*-----------------------------------------------------------------------
    copy APN info to policy for validation using route_get
  -----------------------------------------------------------------------*/
  acl_policy_info.apn.length = net_params->apn.length;
  memscpy(acl_policy_info.apn.name,
          net_params->apn.length,
         net_params->apn.name,
         net_params->apn.length);

  /* populate cdma profile number
  - default (0) or client provided value */
  acl_policy_info.data_session_profile_id = net_params->cdma_profile_num;

  /* Disabling Um iface ID overwriting for EPC and NAT */
#if !defined(FEATURE_EPC_HANDOFF)
  if(net_params->um_iface_id != 0) //if um iface handle is provided
  {
    if(ps_iface_get_handle(net_params->um_iface_id) == NULL)
    {
      LOG_MSG_INFO1_0("Invalid UM iface handle passed in Rmnet call bring up req");
      return NULL;
    }
    acl_policy_info.iface.info.id = net_params->um_iface_id;

    acl_policy_info.iface.kind      = DSS_IFACE_ID;
  }
#endif /* !(FEATURE_EPC_HANDOFF) */

#ifdef FEATURE_DATA_QMI_MCAST
  if ( (net_params->tech_pref == CDMA_BCAST_IFACE) ||
       (net_params->tech_pref == DVBH_IFACE) ||
       (net_params->tech_pref == MBMS_IFACE) ||
       (net_params->tech_pref == FLO_IFACE) )
  {
    net_params->is_mcast_iface = TRUE;
  }
#endif /* FEATURE_DATA_QMI_MCAST */

  if (net_params->tech_pref == EMBMS_IFACE)
  {
    net_params->is_embms_iface = TRUE;
  }

  sio_port = rmnet_sio_get_port ( rmnet_smi_info[rmnet_inst].meta_sm.rmnet_sio_handle );
  LOG_MSG_INFO1_2( "Iface lookup, rmnet inst [%d], sio_port [0x%x]",
                   rmnet_inst, (uint32)sio_port );

  if ( !rmnet_meta_smi_find_proc_id( &proc_id, sio_port, rmnet_inst ) )
  {
    proc_id = RMNET_PROC_ID_APPS1;
    LOG_MSG_INFO2_1( "Unable to map sio_port [0x%x], assume APPS/emb",
                     (uint32)sio_port );
  }

  acl_policy_info.proc_id = (acl_proc_id_type)proc_id;
  if (proc_id == RMNET_PROC_ID_APPS1)
  {
    acl_policy_info.is_sock_orig   = TRUE;
    acl_policy_info.app_identifier = APP_ID_APPS;
  }
  else
  {
    acl_policy_info.app_identifier = APP_ID_TE_RMNET;
  }

  /* set proc_id in meta sm */
  info->meta_sm.proc_id = (rmnet_meta_sm_proc_id_e_type) acl_policy_info.proc_id;

  tmp_app_id = acl_policy_info.app_identifier;
  LOG_MSG_INFO1_3( "Iface lookup, rmnet inst [%d], app_id (upper32bits) [0x%x], proc_id [%d]",
                   rmnet_inst,
                   (uint32)(tmp_app_id>>32),
                   acl_policy_info.proc_id );

  /*-------------------------------------------------------------------------
    Update subs id in the policy
  -------------------------------------------------------------------------*/
  acl_policy_info.subs_id         = net_params->subscription_id;

  /*-------------------------------------------------------------------------
    Do the interface lookup using routing
  -------------------------------------------------------------------------*/
  um_iface_ptr = NULL;

  um_iface_ptr =  ps_route_look_up_iface_by_policy( &acl_policy_info,
                                                    &arb_candidate,
                                                    &iface_priority_mask,
                                                    &net_down_reason
                                                  );
  if( um_iface_ptr == NULL )
  {
    LOG_MSG_ERROR_0("look_up_iface_by_policy: none available");
    return NULL;
  }

  LOG_MSG_INFO1_3( "lookup: RmNet got Um Iface [0x%x:%x], prio mask [0x%x]",
                   (uint32)(um_iface_ptr->name),
                   (uint32)(um_iface_ptr->instance),
                   iface_priority_mask );
  LOG_MSG_INFO1_1( "lookup: is_arb [%d]", arb_candidate );

  arb_info_ptr->iface_priority_mask    = iface_priority_mask;
  arb_info_ptr->is_iface_arb_candidate = arb_candidate;

  if (acl_pol_ptr != NULL)
  {
    memscpy(acl_pol_ptr, 
            sizeof(acl_policy_info_type), 
            &acl_policy_info, 
            sizeof(acl_policy_info_type));
  }
  return um_iface_ptr;

} /* rmnet_meta_smi_get_um_iface_ptr() */

/*===========================================================================
  FUNCTION RMNET_META_SMI_BRING_UP_UM_IFACE_BY_POLICY


  DESCRIPTION
    This function is used to bringup Iface by policy may involve arb.

    An UM iface is returned only if there is one available and it is not
    in IFACE_DOWN state.

  PARAMETERS
    net_params    - Parameters to build the acl policy
    if_ptr        - pointer to Um Iface
    rmnet_inst    - corresponding RmNet instance
    ps_errno      - pointer to errno generated when ret code is -1

  RETURN VALUE
    0  on SUCCESS
   -1  on FAILURE

  DEPENDENCIES

  SIDE EFFECTS
    None
===========================================================================*/
int rmnet_meta_smi_bring_up_um_iface_by_policy
(
  rmnet_meta_sm_network_info_type * net_params,
  ps_iface_type                  ** if_ptr,
  rmnet_instance_e_type             rmnet_inst,
  int16                           * ps_errno,
  ps_iface_net_down_reason_type   * net_down_reason
)
{
  ps_iface_type *           um_iface_ptr;
  acl_policy_info_type      acl_policy_info;
  int    result   = 0;
  uint64                    tmp_app_id;
  sio_port_id_type          sio_port;
  uint32                    proc_id = RMNET_PROC_ID_MAX;
  rmnet_smi_info_type      *info;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(rmnet_inst < RMNET_INSTANCE_MAX);
  info = &rmnet_smi_info[rmnet_inst];

  memset( &acl_policy_info, 0, sizeof(acl_policy_info_type) );
  acl_init_policy_info(&acl_policy_info);

  acl_policy_info.policy_flag     = DSS_IFACE_POLICY_ANY;
  acl_policy_info.iface.kind      = DSS_IFACE_NAME;
  acl_policy_info.iface.info.name = DSS_IFACE_WWAN; //DSS_IFACE_ANY;

  acl_policy_info.bring_up        = TRUE;
  acl_policy_info.subs_id         = net_params->subscription_id;

  switch(net_params->ip_family_pref)
  {
    case QMI_AF_ANY:     /* not supported currently */
    case QMI_AF_INET:
      acl_policy_info.ip_family       = IFACE_IPV4_ADDR_FAMILY;
      break;

#ifdef FEATURE_DATA_PS_IPV6
    case QMI_AF_INET6:
      acl_policy_info.ip_family       = IFACE_IPV6_ADDR_FAMILY;
      break;
#endif /* FEATURE_DATA_PS_IPV6 */

    case QMI_AF_UNSPEC:
      acl_policy_info.ip_family       = IFACE_UNSPEC_ADDR_FAMILY;
      break;

    default:
      LOG_MSG_ERROR_1("Invalid RmNet IP family: %d!",
                      net_params->ip_family_pref);
      *ps_errno = DS_EINVAL;
      return -1;
  }
  /*-------------------------------------------------------------------------
    TO DO - possibly move this in a function in wds
    When you reach here tech_pref WILL have a value from the enum
    ps_iface_name_enum_type
  -------------------------------------------------------------------------*/

  acl_policy_info.iface.info.name =
                            (dss_iface_name_enum_type) net_params->tech_pref;

  /* populate umts profile num: upper byte contains profile family and lower
     byte contains the profile index. Caller of this function needs to ensure
     that both bytes get populated appropriately */
  acl_policy_info.pdp_info = (acl_pdp_ctx_cfg_type)(uint32)
                                              (net_params->umts_profile_num);

  /*-----------------------------------------------------------------------
    copy APN info to policy
  -----------------------------------------------------------------------*/
  acl_policy_info.apn.length = net_params->apn.length;
  memscpy( acl_policy_info.apn.name,
           net_params->apn.length,
          net_params->apn.name,
          net_params->apn.length );

  /* populate cdma profile number
  - default (0) or client provided value */
  acl_policy_info.data_session_profile_id = net_params->cdma_profile_num;

  /* Disabling Um iface ID overwriting for EPC and NAT */
#if !defined(FEATURE_EPC_HANDOFF)
  if(net_params->um_iface_id != 0) //if um iface handle is provided
  {
    if(ps_iface_get_handle(net_params->um_iface_id) == NULL)
    {
      LOG_MSG_INFO1_0("Invalid UM iface handle passed in Rmnet call bring up req");
      *ps_errno = DS_EINVAL;
      return -1;
    }
    acl_policy_info.iface.info.id = net_params->um_iface_id;

    acl_policy_info.iface.kind    = DSS_IFACE_ID;
  }
#endif /* !(FEATURE_EPC_HANDOFF) */

#ifdef FEATURE_DATA_QMI_MCAST
  if ( (net_params->tech_pref == CDMA_BCAST_IFACE) ||
       (net_params->tech_pref == DVBH_IFACE) ||
       (net_params->tech_pref == MBMS_IFACE) ||
       (net_params->tech_pref == FLO_IFACE) )
  {
    net_params->is_mcast_iface = TRUE;
  }
#endif /* FEATURE_DATA_QMI_MCAST */

  if (net_params->tech_pref == EMBMS_IFACE)
  {
    net_params->is_embms_iface = TRUE;
  }

  sio_port = rmnet_sio_get_port ( rmnet_smi_info[rmnet_inst].meta_sm.rmnet_sio_handle );
  LOG_MSG_INFO1_2( "Iface lookup, rmnet inst [%d], sio_port [0x%x]",
                   rmnet_inst, (uint32)sio_port );

  if ( !rmnet_meta_smi_find_proc_id( &proc_id, sio_port, rmnet_inst ) )
  {
    proc_id = RMNET_PROC_ID_APPS1;
    LOG_MSG_INFO2_1( "Unable to map sio_port [0x%x], assume APPS/emb",
                     (uint32)sio_port );
  }

  acl_policy_info.proc_id = (acl_proc_id_type)proc_id;
  if (proc_id == RMNET_PROC_ID_APPS1)
  {
    acl_policy_info.is_sock_orig   = TRUE;
    acl_policy_info.app_identifier = APP_ID_APPS;
  }
  else
  {
    acl_policy_info.app_identifier = APP_ID_TE_RMNET;
  }

  /* Rm_inst_handle identifies the port on which the call is 
     being attempted, hence set the qmi instance in the acl policy 
     structure. */
  acl_policy_info.rm_inst_handle = info->constants.qmi_instance;

  /* set proc_id in meta sm */
  info->meta_sm.proc_id = (rmnet_meta_sm_proc_id_e_type) acl_policy_info.proc_id;

  tmp_app_id = acl_policy_info.app_identifier;
  LOG_MSG_INFO1_3( "Iface lookup, rmnet inst [%d], app_id (upper32bits) [0x%x], proc_id [%d]",
                   rmnet_inst,
                   (uint32)(tmp_app_id>>32),
                   acl_policy_info.proc_id );

#ifdef FEATURE_DATA_LTE
  if(info->meta_sm.net_params.is_handoff_context_valid)
  {
    LOG_MSG_INFO1_0("Handoff context value set, copying info over to acl");
    acl_policy_info.handoff_ctxt = info->meta_sm.net_params.handoff_context;
    acl_policy_info.handoff_ctxt.handoff_class = PS_IFACE_HANDOFF_CLASS_EPC;
  }
#endif /* FEATURE_DATA_LTE */


  /*-------------------------------------------------------------------------
    Do the interface bring-up. This will involve arbitration.
  -------------------------------------------------------------------------*/
  um_iface_ptr = NULL;
  result =  ps_route_bring_up_iface_by_policy_ex( &acl_policy_info,
                                                  &um_iface_ptr,
                                                  ps_errno,
                                                 (void *)&net_params->cfg_info,
                                                  net_down_reason);
  *if_ptr = um_iface_ptr;

  /* very verbose */
  LOG_MSG_INFO1_2( "bringup: bring_up_iface_by_policy, ret [%d], errno [%d]",
                   result, *ps_errno );
  if ( result == 0 && um_iface_ptr != NULL )
  {
    LOG_MSG_INFO1_3( "bringup: RmNet inst %d got Um Iface [0x%x:%x]",
                     rmnet_inst,
                     (uint32)(um_iface_ptr->name),
                     (uint32)(um_iface_ptr->instance));
  }
  return result;
} /* rmnet_meta_smi_bring_up_um_iface_by_policy() */


/*===========================================================================
  FUNCTION RMNET_META_SMII_SET_MAC

  DESCRIPTION
    Set the specified ps_iface's MAC address to the specified value.

  PARAMETERS
    iface:        interface whose MAC address is updated
    hw_addr:      value of MAC address to be set
    hw_addr_len:  len of MAC address to be set

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
// TODO - make this a ps_iface_* function
static void rmnet_meta_smi_set_mac
(
  ps_iface_type *  iface,
  uint8 *          hw_addr,
  uint8            hw_addr_len
)
{
  uint8*           iface_hw_addr = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(hw_addr_len <= PS_IFACE_HW_ADDR_LEN_MAX(iface) && 
           (hw_addr_len == LAN_IEEE_MAC_ADDR_LEN));

  iface_hw_addr = PS_IFACE_HW_ADDR_PTR( iface );

  if(NULL == iface_hw_addr)
  {
     LOG_MSG_ERROR_0("The Interface hardware address is NULL");
     ASSERT(0);
     return;
  }

  /*-------------------------------------------------------------------------
    Set the MAC address on the specified interface.  It is left-justified
    in memory (packed towards the start of HW addr buffer).
  -------------------------------------------------------------------------*/
  PS_IFACE_SET_HW_ADDR_LEN( iface, hw_addr_len );
  memset( iface_hw_addr, 0, PS_IFACE_HW_ADDR_LEN_MAX(iface));
  memscpy( iface_hw_addr , 
           hw_addr_len,
           hw_addr, 
           hw_addr_len );
} /* rmnet_meta_smi_set_mac() */



/*===========================================================================
  FUNCTION RMNET_META_SMI_REG_EVENTS()

  DESCRIPTION
    This function registers RmNet Meta SM for all common iface, flow, and
    phys link events.

  PARAMETERS
    info - pointer to the interface controller's control block

  RETURN VALUE
    TRUE if success
    FALSE if failure

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static boolean rmnet_meta_smi_reg_events
(
  rmnet_smi_info_type *  info
)
{
  boolean is_err = FALSE;
  ps_iface_event_enum_type event = IFACE_MIN_EV;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if( info->um_iface_ptr == NULL )
  {
    LOG_MSG_ERROR_0("shldn't register Iface ev for NULL Iface" );
    return FALSE;
  }

  LOG_MSG_INFO2_0( "Registering all common iface, flow, and phys link events" );

  /*---------------------------------------------------------------------
    Register for desired events with Um_PKT_IFACE

    TODO - put the event bufs below into an array indexed by IFACE_EV of
           interest, then loop through the events.
  ---------------------------------------------------------------------*/
  do
  {
  if ( ps_iface_event_cback_reg
                         (
                           info->um_iface_ptr,
                           IFACE_ACTIVE_OUT_OF_USE_EV,
                           info->meta_sm.um_iface_active_out_of_use_buf_ptr
                         ) < 0 )
  {
      is_err = TRUE;
      event = IFACE_ACTIVE_OUT_OF_USE_EV;
      break;
  }

  if ( ps_iface_event_cback_reg( info->um_iface_ptr,
                                 IFACE_UP_EV,
                                 info->meta_sm.um_iface_up_buf_ptr
                               ) < 0 )
  {
      is_err = TRUE;
      event = IFACE_UP_EV;
      break;
  }

  if ( ps_iface_event_cback_reg( info->um_iface_ptr,
                                 IFACE_DOWN_EV,
                                 info->meta_sm.um_iface_down_buf_ptr
                               ) < 0 )
  {
      is_err = TRUE;
      event = IFACE_DOWN_EV;
      break;
  }

  if ( ps_iface_event_cback_reg( info->um_iface_ptr,
                                 IFACE_AUTHENTICATING_EV,
                                 info->meta_sm.um_iface_authenticating_buf_ptr
                               ) < 0 )
  {
      is_err = TRUE;
      event = IFACE_AUTHENTICATING_EV;
      break;
  }

  if(ps_iface_event_cback_reg( info->um_iface_ptr,
                                 IFACE_LINGERING_EV,
                                 info->meta_sm.um_iface_lingering_buf_ptr
                               ) < 0 )
  {
      is_err = TRUE;
      event = IFACE_LINGERING_EV;
      break;
  }

  if(ps_iface_event_cback_reg(
                         info->um_iface_ptr,
#ifdef FEATURE_DATA_WLAN_MAPCON
                         IFACE_BEARER_TECH_EX_CHANGED_EV,
                         info->meta_sm.um_iface_bearer_tech_change_buf_ptr
                         ) < 0)
  {
      event = IFACE_BEARER_TECH_EX_CHANGED_EV;
#else
                         IFACE_BEARER_TECH_CHANGED_EV,
                         info->meta_sm.um_iface_bearer_tech_change_buf_ptr
                         ) < 0)
  {
      event = IFACE_BEARER_TECH_CHANGED_EV;
#endif /* FEATURE_DATA_WLAN_MAPCON */
      is_err = TRUE;
      break;
  }
  if(ps_iface_event_cback_reg( info->um_iface_ptr,
                               IFACE_FLOW_ENABLED_EV,
                               info->meta_sm.um_flow_enabled_buf_ptr
                             ) < 0 )
  {
      is_err = TRUE;
      event = IFACE_FLOW_ENABLED_EV;
      break;
  }

  if(ps_iface_event_cback_reg( info->um_iface_ptr,
                               IFACE_FLOW_DISABLED_EV,
                               info->meta_sm.um_flow_disabled_buf_ptr
                             ) < 0 )
  {
      is_err = TRUE;
      event = IFACE_FLOW_DISABLED_EV;
      break;
    }
  }while(0);
  if(is_err == TRUE)
  {
    LOG_MSG_ERROR_1("reg failed for event (%d)",event);
    return FALSE;
  }

  if(info->meta_sm.data_format == 0) // i.e. Non-qos
  {
    if(ps_flow_event_cback_reg(
                           PS_IFACE_GET_DEFAULT_FLOW(info->um_iface_ptr),
                           FLOW_TX_ENABLED_EV,
                           info->meta_sm.um_flow_tx_enabled_buf_ptr) < 0)
    {
      LOG_MSG_ERROR_0("FLOW TX_ENABLED_EV reg failed" );
      return FALSE;
    }

    if(ps_flow_event_cback_reg(
                           PS_IFACE_GET_DEFAULT_FLOW(info->um_iface_ptr),
                           FLOW_TX_DISABLED_EV,
                           info->meta_sm.um_flow_tx_disabled_buf_ptr) < 0)
    {
      LOG_MSG_ERROR_0("FLOW TX_DISABLED_EV reg failed" );
      return FALSE;
    }

    info->meta_sm.default_flow_ev_reg = TRUE;
  }

  return TRUE;
} /* rmnet_meta_smi_reg_events() */

/*===========================================================================
  FUNCTION RMNET_META_SMI_REG_PHYSLINK_EVENTS()

  DESCRIPTION
    This function registers RmNet Meta SM for phys link events.

  PARAMETERS
    info - pointer to the interface controller's control block

  RETURN VALUE
    TRUE if success
    FALSE if failure

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static boolean rmnet_meta_smi_reg_physlink_events
(
  rmnet_smi_info_type *  info
)
{
  boolean is_err = FALSE;
  ps_iface_event_enum_type event = IFACE_MIN_EV;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  LOG_MSG_INFO2_0( "Registering phys link events" );

  /*---------------------------------------------------------------------
     Cache the um iface phys link
     - to be able to access phys link ptr even it is unbound from um iface
       in the case of the phys link dynamic binding
  ----------------------------------------------------------------------*/
  info->meta_sm.um_iface_phys_link_ptr =
                                 PS_IFACE_GET_PHYS_LINK(info->um_iface_ptr);

  if (info->meta_sm.um_iface_phys_link_ptr == NULL)
  {
    LOG_MSG_ERROR_0( "Physlink ptr is NULL, don't register for physlink events" );
    return FALSE;
  }
  do
  {
  if(ps_phys_link_event_cback_reg(
                              info->meta_sm.um_iface_phys_link_ptr,
                              PHYS_LINK_FLOW_ENABLED_EV,
                              info->meta_sm.um_phys_link_flow_enabled_buf_ptr
                            ) < 0 )
  {
      is_err = TRUE;
      event = PHYS_LINK_FLOW_ENABLED_EV;
      break;
  }

  if(ps_phys_link_event_cback_reg(
                              info->meta_sm.um_iface_phys_link_ptr,
                              PHYS_LINK_FLOW_DISABLED_EV,
                              info->meta_sm.um_phys_link_flow_disabled_buf_ptr
                            ) < 0)
  {
      is_err = TRUE;
      event = PHYS_LINK_FLOW_DISABLED_EV;
      break;
  }

  if(ps_phys_link_event_cback_reg(
                              info->meta_sm.um_iface_phys_link_ptr,
                              PHYS_LINK_UP_EV,
                              info->meta_sm.um_phys_link_up_buf_ptr
                            ) < 0)
  {
      is_err = TRUE;
      event = PHYS_LINK_UP_EV;
      break;
  }

  if(ps_phys_link_event_cback_reg(
                              info->meta_sm.um_iface_phys_link_ptr,
                              PHYS_LINK_DOWN_EV,
                              info->meta_sm.um_phys_link_down_buf_ptr
                            ) < 0)
  {
      is_err = TRUE;
      event = PHYS_LINK_DOWN_EV;
      break;
    }
  }while(0);
  if(is_err == TRUE)
  {
    LOG_MSG_ERROR_1("reg failed for evt = (%d)",event );
    return FALSE;
  }

  return TRUE;
} /* rmnet_meta_smi_reg_physlink_events() */

/*===========================================================================
  FUNCTION RMNET_META_SMI_DEREG_PHYSLINK_EVENTS()

  DESCRIPTION
    This function de-egisters RmNet Meta SM for phys link events.

  PARAMETERS
    info - pointer to the interface controller's control block

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void rmnet_meta_smi_dereg_physlink_events
(
  rmnet_smi_info_type *  info
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  LOG_MSG_INFO2_0( "De-registering phys link events" );

  /*---------------------------------------------------------------------
    Since de-reg is called after phys link is un-bound from the iface,
    access the un-bound phys link ptr through cache.
  ----------------------------------------------------------------------*/
  if (info->meta_sm.um_iface_phys_link_ptr != NULL)
  {
    ps_phys_link_event_cback_dereg(
                              info->meta_sm.um_iface_phys_link_ptr,
                              PHYS_LINK_FLOW_ENABLED_EV,
                              info->meta_sm.um_phys_link_flow_enabled_buf_ptr
                              );

    ps_phys_link_event_cback_dereg(
                              info->meta_sm.um_iface_phys_link_ptr,
                              PHYS_LINK_FLOW_DISABLED_EV,
                              info->meta_sm.um_phys_link_flow_disabled_buf_ptr
                            );

    ps_phys_link_event_cback_dereg(
                              info->meta_sm.um_iface_phys_link_ptr,
                              PHYS_LINK_UP_EV,
                              info->meta_sm.um_phys_link_up_buf_ptr
                              );

    ps_phys_link_event_cback_dereg(
                              info->meta_sm.um_iface_phys_link_ptr,
                              PHYS_LINK_DOWN_EV,
                              info->meta_sm.um_phys_link_down_buf_ptr
                            );

    info->meta_sm.um_iface_phys_link_ptr = NULL;
  }
  else
  {
    LOG_MSG_ERROR_0( "Physlink ptr is NULL, don't de-reg for physlink events" );
  }
} /* rmnet_meta_smi_dereg_physlink_events() */

/*===========================================================================
  FUNCTION RMNET_META_SMI_DEREG_EVENTS()

  DESCRIPTION
    This function deregisters RmNet Meta SM for all common iface, flow, and
    phys link events.

  PARAMETERS
    info - pointer to the interface controller's control block

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void rmnet_meta_smi_dereg_events
(
  rmnet_smi_info_type *  info
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if( info->um_iface_ptr == NULL )
  {
    LOG_MSG_ERROR_0("shldn't deregister Iface ev for NULL Iface" );
    return;
  }

  LOG_MSG_INFO2_0( "Deregistering all common iface, flow, and phys link events" );

  ps_iface_event_cback_dereg(
                          info->um_iface_ptr,
                          IFACE_ACTIVE_OUT_OF_USE_EV,
                          info->meta_sm.um_iface_active_out_of_use_buf_ptr );

  ps_iface_event_cback_dereg( info->um_iface_ptr,
                              IFACE_UP_EV,
                              info->meta_sm.um_iface_up_buf_ptr );

  ps_iface_event_cback_dereg( info->um_iface_ptr,
                              IFACE_DOWN_EV,
                              info->meta_sm.um_iface_down_buf_ptr );

  ps_iface_event_cback_dereg( info->um_iface_ptr,
                              IFACE_AUTHENTICATING_EV,
                              info->meta_sm.um_iface_authenticating_buf_ptr );

  ps_iface_event_cback_dereg( info->um_iface_ptr,
                              IFACE_LINGERING_EV,
                              info->meta_sm.um_iface_lingering_buf_ptr );

  ps_iface_event_cback_dereg( info->um_iface_ptr,
#ifdef FEATURE_DATA_WLAN_MAPCON
                              IFACE_BEARER_TECH_EX_CHANGED_EV,
#else
                              IFACE_BEARER_TECH_CHANGED_EV,
#endif /* FEATURE_DATA_WLAN_MAPCON */
                              info->meta_sm.um_iface_bearer_tech_change_buf_ptr );

  ps_iface_event_cback_dereg( info->um_iface_ptr,
                              IFACE_FLOW_ENABLED_EV,
                              info->meta_sm.um_flow_enabled_buf_ptr );

  ps_iface_event_cback_dereg( info->um_iface_ptr,
                              IFACE_FLOW_DISABLED_EV,
                              info->meta_sm.um_flow_disabled_buf_ptr );

  rmnet_meta_smi_dereg_physlink_events(info);

  if(info->meta_sm.default_flow_ev_reg == TRUE)
  {
    ps_flow_event_cback_dereg(
                         PS_IFACE_GET_DEFAULT_FLOW(info->um_iface_ptr),
                         FLOW_TX_ENABLED_EV,
                         info->meta_sm.um_flow_tx_enabled_buf_ptr);
    ps_flow_event_cback_dereg(
                         PS_IFACE_GET_DEFAULT_FLOW(info->um_iface_ptr),
                         FLOW_TX_DISABLED_EV,
                         info->meta_sm.um_flow_tx_disabled_buf_ptr);
    info->meta_sm.default_flow_ev_reg = FALSE;
  }
} /* rmnet_meta_smi_dereg_events() */

/*===========================================================================
  FUNCTION RMNET_META_SMI_SET_RX_SIG_HANDLER()

  DESCRIPTION
    This function sets the tx cmd information for the call

  PARAMETERS
    info - pointer to the interface controller's control block

  RETURN VALUE
     0 if success
    -1 if failure

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static int rmnet_meta_smi_set_rx_sig_handler
(
  rmnet_smi_info_type *  info
)
{
  rmnet_smi_dual_ip_info_type * rmnet_dual_ip_info =
    &rmnet_smi_dual_ip_info[info->constants.qmi_instance];

/*-------------------------------------------------------------------------
  Init Outstanding pkt to NULL. Free if it is stored from the previous call
  -------------------------------------------------------------------------*/
  if(rmnet_dual_ip_info->rx_pkt != NULL)
  {
    dsm_free_packet(&rmnet_dual_ip_info->rx_pkt);
  }
  rmnet_dual_ip_info->rx_pkt = NULL;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /*-----------------------------------------------------------------------
   Register RX signal handler on provided signal & enable signal
  -----------------------------------------------------------------------*/
  if (info->link_prot == RMNET_ETHERNET_MODE)
  {

    if (info->meta_sm.data_format)
    {
      LOG_MSG_INFO1_1( "Registering ps_ul_optimized_eth_qos_rx_sig_hdlr : RmNet inst %d",
                       RMNET_META_SM_INFO_TO_INSTANCE(info));
      (void)rmnet_set_sig_handler(
	    rmnet_smi_dual_ip_info[info->constants.qmi_instance].rx_sig,
        ps_ul_optimized_eth_qos_rx_sig_hdlr,
        (void *) &rmnet_smi_dual_ip_info[info->constants.qmi_instance]);
    }
    else
    {
      LOG_MSG_INFO1_1( "Registering ps_ul_optimized_eth_no_qos_rx_sig_hdlr : RmNet inst %d",
                       RMNET_META_SM_INFO_TO_INSTANCE(info));
      (void) rmnet_set_sig_handler( rmnet_smi_dual_ip_info[info->constants.qmi_instance].rx_sig,
                                    ps_ul_optimized_eth_no_qos_rx_sig_hdlr,
                                 (void *) &rmnet_smi_dual_ip_info[info->constants.qmi_instance]);
    }
  }/*If Ethernet mode*/
  else if (info->link_prot == RMNET_IP_MODE)
  {
    if ((info->meta_sm.data_agg_protocol.ul_data_agg_protocol ==
           RMNET_ENABLE_DATA_AGG_QC_NCM ) && (info->meta_sm.data_format == 0)
       )
    {
      /*Allocate mbim ntb header*/
      info->meta_sm.ntb_header =
        qmi_svc_ps_system_heap_mem_alloc(sizeof(rmnet_ntb_hdr_type),FILE_ID_DS_RMNET_META_SM,__LINE__);
      if (info->meta_sm.ntb_header == NULL)
      {
        return -1;
      }

      rmnet_meta_smi_init_mbim_hdr(info);

      /* Allocate outstanding mbim pkt info only if not allocated earlier. Logic is
       * once it is allocated for a MBIM call for this port it is never freed
       * and the same ptr is used for subsequent MBIM calls. Will only be memset
       * for every new MBIM call.Only for a subsequent non-MBIM call it will be freed.
       */
      if(rmnet_dual_ip_info->mbim_pkt_info == NULL)
      {
      rmnet_dual_ip_info->mbim_pkt_info =
        qmi_svc_ps_system_heap_mem_alloc(sizeof(rmnet_mbim_pkt_info_type),FILE_ID_DS_RMNET_META_SM,__LINE__);
      if (rmnet_dual_ip_info->mbim_pkt_info == NULL)
      {
        return -1;
      }

      memset(rmnet_dual_ip_info->mbim_pkt_info, 0,
           sizeof(rmnet_mbim_pkt_info_type));

      /*Allocate ndp short buf in mbim pkt info*/
      rmnet_dual_ip_info->mbim_pkt_info->ndp_short_buf =
        qmi_svc_ps_system_heap_mem_alloc(PS_OPT_PATH_MAX_ST_NDP_LEN,FILE_ID_DS_RMNET_META_SM,__LINE__);
      if (rmnet_dual_ip_info->mbim_pkt_info->ndp_short_buf == NULL)
      {
        return -1;
      }

      memset(rmnet_dual_ip_info->mbim_pkt_info->ndp_short_buf, 0,
           PS_OPT_PATH_MAX_ST_NDP_LEN);
      }
      else
      {
        /*Already allocated. Just memset individual members as we would have allocated
         * ndp short buff and dont want to lose the ptr by doing a direct
         * memset(mbim_pkt_info)*/

        rmnet_dual_ip_info->mbim_pkt_info->ndp_datagram_offset = 0;
        if(rmnet_dual_ip_info->mbim_pkt_info->ntb_dsm_item != NULL)
        {
          dsm_free_packet(&rmnet_dual_ip_info->mbim_pkt_info->ntb_dsm_item);
        }

        rmnet_dual_ip_info->mbim_pkt_info->ntb_dsm_item        = NULL;
        rmnet_dual_ip_info->mbim_pkt_info->ndp_offset          = 0;

        memset(&rmnet_dual_ip_info->mbim_pkt_info->nth_hdr,0,
               sizeof(ps_iface_mbim_ntb_nth_hdr));

        rmnet_dual_ip_info->mbim_pkt_info->ndp_hdr             = NULL;
        rmnet_dual_ip_info->rx_pkt                             = NULL;

        rmnet_dual_ip_info->mbim_pkt_info->ndp_long_buf        = NULL;
        memset(rmnet_dual_ip_info->mbim_pkt_info->ndp_short_buf, 0,
           PS_OPT_PATH_MAX_ST_NDP_LEN);
      }

      LOG_MSG_INFO1_1("Registering ps_ul_optimized_qcncm_ip_rx_sig_hdlr : RmNet inst %d",
                      RMNET_META_SM_INFO_TO_INSTANCE(info));

      (void) rmnet_set_sig_handler( rmnet_smi_dual_ip_info[info->constants.qmi_instance].rx_sig,
               ps_ul_optimized_qcncm_ip_rx_sig_hdlr,
                                 (void *) &rmnet_smi_dual_ip_info[info->constants.qmi_instance]);

    }/*If qcncm mode*/

#ifdef FEATURE_DATA_MBIM
    else if ((info->meta_sm.data_agg_protocol.ul_data_agg_protocol ==
              RMNET_ENABLE_DATA_AGG_MBIM) && (info->meta_sm.data_format == 0)
            )
    {

      if (rmnet_meta_smi_init_mbim_outstanding_info() == -1)
      {
        LOG_MSG_ERROR_0("No mem for mbim_pkt_info, exiting");
        return -1;
      }

      LOG_MSG_INFO1_1("Registering ps_ul_optimized_mbim_ip_rx_sig_hdlr:"
                      " RmNet inst %d", RMNET_META_SM_INFO_TO_INSTANCE(info));
      (void) rmnet_set_sig_handler(
               mbim_call_info.mbim_rx_sig,
                                    ps_ul_optimized_mbim_ip_rx_sig_hdlr,
               (void *) &mbim_call_info);

    }/*If mbim mode*/
#endif /*FEATURE_DATA_MBIM*/

    else
    {
      /* Its a Non-MBIM/QCNCM call. So we dont need mbim_pkt_info anymore if
       * allocated in a previous MBIM call.Need to check for NULL
       * mbim_pkt_info as we might get Um iface down event first during
       * call bring up before Rm Configuring state where this is allocated*/

      /*Free QCNCM Outstanding pkt info if still valid from previous call*/

        if(rmnet_smi_dual_ip_info[info->constants.qmi_instance].mbim_pkt_info
            != NULL)
        {
          PS_SYSTEM_HEAP_MEM_FREE(
            rmnet_smi_dual_ip_info[info->constants.qmi_instance].mbim_pkt_info->
            ndp_short_buf);
          PS_SYSTEM_HEAP_MEM_FREE(
            rmnet_smi_dual_ip_info[info->constants.qmi_instance].mbim_pkt_info);
        }

#ifdef FEATURE_DATA_MBIM
      /*Free MBIM Outstanding pkt info if still valid from previous call
       *Free ntb_dsm_item, ndp_short_buf,pkt_info if still valid*/

      if((mbim_call_info.pkt_info != NULL) && (mbim_call_info.call_cnt ==0))
      {
        if(mbim_call_info.pkt_info->ntb_dsm_item != NULL)
        {
          dsm_free_packet(&mbim_call_info.pkt_info->ntb_dsm_item);
        }

        PS_SYSTEM_HEAP_MEM_FREE(mbim_call_info.pkt_info->ndp_short_buf);
        PS_SYSTEM_HEAP_MEM_FREE(mbim_call_info.pkt_info);

        if(mbim_call_info.rx_pkt != NULL)
        {
          dsm_free_packet(&mbim_call_info.rx_pkt);
        }
        mbim_call_info.rx_pkt = NULL;

      }
#endif /*FEATURE_DATA_MBIM*/


        if (info->meta_sm.data_format)
        {
          if (info->meta_sm.qos_format == RMNET_QOS_FORMAT_8_BYTE)
          {
            LOG_MSG_INFO1_1("Registering ps_ul_optimized_ip_qos2_rx_sig_hdlr : RmNet inst %d",
                          RMNET_META_SM_INFO_TO_INSTANCE(info));
            (void) rmnet_set_sig_handler( rmnet_smi_dual_ip_info[info->constants.qmi_instance].rx_sig,
                                          ps_ul_optimized_ip_qos2_rx_sig_hdlr,
                                         (void *) &rmnet_smi_dual_ip_info[info->constants.qmi_instance]);
          }
          else
          {
              LOG_MSG_INFO1("Registering ps_ul_optimized_ip_qos_rx_sig_hdlr : RmNet inst %d",
                            RMNET_META_SM_INFO_TO_INSTANCE(info), 0, 0);
            (void) rmnet_set_sig_handler( rmnet_smi_dual_ip_info[info->constants.qmi_instance].rx_sig,
                                          ps_ul_optimized_ip_qos_rx_sig_hdlr,
                                         (void *) &rmnet_smi_dual_ip_info[info->constants.qmi_instance]);
          }
        }
        else
        {
          LOG_MSG_INFO1_1("Registering ps_ul_optimized_ip_no_qos_rx_sig_hdlr : RmNet inst %d",
                          RMNET_META_SM_INFO_TO_INSTANCE(info));
          (void) rmnet_set_sig_handler( rmnet_smi_dual_ip_info[info->constants.qmi_instance].rx_sig,
                                    ps_ul_optimized_ip_no_qos_rx_sig_hdlr,
                                 (void *) &rmnet_smi_dual_ip_info[info->constants.qmi_instance]);
        }
     }/*Non-MBIM/QCNCM mode*/
  }/*If IP Mode*/

  /*-------------------------------------------------------------------------
   Register legacy sig handler for the respective RmNet port
   (dual_ip_info instance)
  -------------------------------------------------------------------------*/
  if (info->meta_sm.data_format &&
      info->meta_sm.qos_format == RMNET_QOS_FORMAT_8_BYTE)
  {
    LOG_MSG_INFO1_1("Registering rmnet_meta_smi_legacy_sig_hdlr_qos2 : RmNet inst %d",
                   RMNET_META_SM_INFO_TO_INSTANCE(info));
    (void)rmnet_set_legacy_sig_handler(
             rmnet_smi_dual_ip_info[info->constants.qmi_instance].legacy_sig,
             rmnet_meta_smi_legacy_sig_hdlr_qos2,
                             (void *) &rmnet_smi_dual_ip_info[info->constants.qmi_instance]);
  }
  else
  {
    LOG_MSG_INFO1("Registering rmnet_meta_smi_legacy_sig_hdlr : RmNet inst %d",
                   RMNET_META_SM_INFO_TO_INSTANCE(info), 0, 0);
  (void)rmnet_set_legacy_sig_handler(
           rmnet_smi_dual_ip_info[info->constants.qmi_instance].legacy_sig,
           rmnet_meta_smi_legacy_sig_hdlr,
                             (void *) &rmnet_smi_dual_ip_info[info->constants.qmi_instance]);
  }

  /*-------------------------------------------------------------------------
    Enable MBIM on DL path if config'ed.
  -------------------------------------------------------------------------*/
  if (info->meta_sm.data_agg_protocol.dl_data_agg_protocol ==
      RMNET_ENABLE_DATA_AGG_MBIM ||
      info->meta_sm.data_agg_protocol.dl_data_agg_protocol ==
      RMNET_ENABLE_DATA_AGG_QC_NCM)
  {
    info->meta_sm.lp_tx_f_ptr = rmnet_meta_smi_rm_tx_data_mbim;

    /* Moved the Optimized path set to RmNet SM UP state for all cases */
  }
  else
  {
    info->meta_sm.lp_tx_f_ptr = rmnet_meta_smi_rm_tx_data;
  }

  return 0;
} /* rmnet_meta_smi_set_rx_sig_handler() */


/*===========================================================================
  FUNCTION RMNET_META_SMI_START_ADDR_CFG_SM()

  DESCRIPTION
    This function initiates the interface specific address configuration
    state machine determined by the Um iface received by the routing lookup.

    The supported address cfg state machines are:

    1) IPv4
    2) IPv6
    3) Multicast

  PARAMETERS
    info - pointer to the interface controller's control block

  RETURN VALUE
     0 if success
    -1 if failure

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static int rmnet_meta_smi_start_addr_cfg_sm
(
  rmnet_smi_info_type *  info
)
{
  lan_llc_start_info_type  start_info;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /*---------------------------------------------------------------------
    Initialize start info to zero.
  ---------------------------------------------------------------------*/
  memset (&start_info, 0, sizeof(start_info));

  /*-------------------------------------------------------------------------
    Check if this is a mcast instance or not
  -------------------------------------------------------------------------*/
  if( (info->meta_sm.net_params.is_mcast_iface == TRUE) ||
      (info->meta_sm.net_params.is_embms_iface == TRUE) )
  {
    /*-----------------------------------------------------------------------
      Since no addr config post addr config event
    -----------------------------------------------------------------------*/
    rmnet_meta_sm_post_event(RMNET_META_SM_INFO_TO_INSTANCE(info),
                             RMNET_META_SM_ADDR_CFG_COMPLETE_EV);

    /*-----------------------------------------------------------------------
      Post configuring on Rm as we are skipping addr config for mcast case
    -----------------------------------------------------------------------*/
    (void) ps_iface_configuring_ind(info->rmnet_iface_ptr);
  }
  else /* unicast */
  {
    if(ps_iface_addr_family_is_v4(info->um_iface_ptr))
    {
      rmnet_v4_sm_init_config(info, &start_info);

      rmnet_v4_sm_post_event(RMNET_META_SM_INFO_TO_INSTANCE(info),
                             RMNET_V4_SM_START_ADDR_CFG_EV);
    }
#ifdef FEATURE_DATA_PS_IPV6
    else if(ps_iface_addr_family_is_v6(info->um_iface_ptr))
    {
      //TO DO - mcast for IPv6
      rmnet_v6_sm_init_config(info, &start_info);

      rmnet_v6_sm_post_event(RMNET_META_SM_INFO_TO_INSTANCE(info),
                             RMNET_V6_SM_START_ADDR_CFG_EV);
    }
#endif /* FEATURE_DATA_PS_IPV6 */
    else
    {
      LOG_MSG_ERROR_0("Unsupported interface type!");
      return -1;
    }
  }

  /*-------------------------------------------------------------------------
    If the interface is configured in IP only mode, do not start LAN LLC.
  -------------------------------------------------------------------------*/
  switch(info->link_prot)
  {
    case RMNET_IP_MODE:
      ps_iface_set_tx_function( info->rmnet_iface_ptr,
                                rmnet_meta_smi_ip_mode_tx_cmd,
                                (void*) info);
      break;

    case RMNET_ETHERNET_MODE:
      /*---------------------------------------------------------------------------
        Init LAN LLC with the appropriate RmNet callbacks.
      ---------------------------------------------------------------------------*/
      start_info.iface_ptr       = info->rmnet_iface_ptr;
      start_info.lle_mode        = LAN_LLC_MODE_0;
      start_info.lle_rx_sig      = info->constants.llc_rx_sig;
      start_info.rx_f_ptr        = rmnet_meta_smi_rm_rx_llc_data;
      start_info.rx_f_user_data  = info;
      start_info.tx_f_ptr        = (lan_llc_tx_f_ptr_type)(info->meta_sm.lp_tx_f_ptr);
      start_info.tx_f_user_data  = info;

      /* Set the TX function user data. This will be over written in the LLC module. */
      ps_iface_set_tx_function( info->rmnet_iface_ptr,
                                 NULL,
                                (void*) info->constants.lan_llc_instance);

      (void)lan_llc_start( info->constants.lan_llc_instance, &start_info );

      info->meta_sm.llc_started = TRUE;

      /*---------------------------------------------------------------------------
        Allocate RNDIS hdr to pushdown on DL Ethernet packets
      ---------------------------------------------------------------------------*/
      if (info->meta_sm.data_agg_protocol.dl_data_agg_protocol ==
            RMNET_ENABLE_DATA_AGG_RNDIS )

      {

        rmnet_meta_smi_init_rndis_eth_hdr(info);

      }
      else
      {
      info->meta_sm.eth_header =
        qmi_svc_ps_system_heap_mem_alloc(sizeof(llc_frm_packed_mode_zero_type),
                      FILE_ID_DS_RMNET_META_SM,__LINE__);
      if (info->meta_sm.eth_header == NULL)
      {
        return -1;
      }
      }

      break;

    default:
      LOG_MSG_ERROR_1("Default:link_prot %d", info->link_prot);
      return -1;
  }

  return 0;
} /* rmnet_meta_smi_start_addr_cfg_sm() */

/*===========================================================================
  FUNCTION RMNET_META_SMI_STOP_ADDR_CFG_SM()

  DESCRIPTION
    This function terminates the address configuration state machine
    determined by the Um iface received selected during the routing lookup.

    The supported address cfg state machines are:

    1) IPv4
    2) IPv6
    3) Multicast

  PARAMETERS
    info - pointer to the interface controller's control block

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void rmnet_meta_smi_stop_addr_cfg_sm
(
  rmnet_smi_info_type *  info
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  switch( info->meta_sm.state )
  {
    case RMNET_META_SM_RM_CONFIGURING_STATE:
    case RMNET_META_SM_NET_IF_UP_STATE:
    case RMNET_META_SM_RM_RECONFIGURING_STATE:
    case RMNET_META_SM_WAITING_FOR_UM_DOWN_STATE:
      /*---------------------------------------------------------------------
        Stop the address configuration state machine(s). DO NOT do this for
        UM_CONFIGURING_STATE as we have not yet initialized the sub state
        machine.
      ---------------------------------------------------------------------*/
      if(ps_iface_addr_family_is_v4(info->um_iface_ptr))
      {
        rmnet_v4_sm_post_event(RMNET_META_SM_INFO_TO_INSTANCE(info),
                               RMNET_V4_SM_STOP_ADDR_CFG_EV);
      }
#ifdef FEATURE_DATA_PS_IPV6
      else if(ps_iface_addr_family_is_v6(info->um_iface_ptr))
      {
        rmnet_v6_sm_post_event(RMNET_META_SM_INFO_TO_INSTANCE(info),
                               RMNET_V6_SM_STOP_ADDR_CFG_EV);
      }
#endif /* FEATURE_DATA_PS_IPV6 */
      else
      {
        LOG_MSG_ERROR_0("Unsupported interface type!");
      }

      break;

    case RMNET_META_SM_UM_CONFIGURING_STATE:
    case RMNET_META_SM_WAITING_FOR_UM_ARB_STATE:
    case RMNET_META_SM_NULL_STATE:
      /* Do nothing since the sub SM has not been started. */
      break;

    default:
    {
      LOG_MSG_ERROR_0("Bad RmNetSM state!");
      ASSERT(0);
      break;
    }
  } /* switch( info->meta_sm.state ) */
} /* rmnet_meta_smi_stop_addr_cfg_sm() */


/*===========================================================================
  FUNCTION RMNET_META_SMI_RETRIEVE_DEFAULT_PROFILE_NUM()

  DESCRIPTION
  Get the default profile number for tech type.

  PARAMETERS
   profile_type (IN)  : Profile Type (3GPP/3GPP2)
   profile_num  (OUT) : Default Profile Number

  RETURN VALUE
   None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void rmnet_meta_smi_retrieve_default_profile_num
(
  ds_profile_tech_etype        profile_type,
  uint8                        profile_family,
  uint16                      *default_profile_num
)
{
  ds_profile_status_etype           profile_status;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  profile_status = DS_PROFILE_REG_RESULT_SUCCESS;
  *default_profile_num = 0;

  /*-------------------------------------------------------------------------
    Call the API to get the default profile number.
  -------------------------------------------------------------------------*/
  profile_status = ds_profile_get_default_profile_num(profile_type,
                                                      (uint32)profile_family,
                                                      default_profile_num);
  if (profile_status != DS_PROFILE_REG_RESULT_SUCCESS &&
      profile_status != DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE)
  {
    LOG_MSG_ERROR_2 ("Get Default Profile for type %d failed. Error %d",
                     profile_type, profile_status);
  }

  return;

} /* rmnet_meta_smi_retrieve_default_profile_num() */


/*===========================================================================
  FUNCTION RMNET_META_SMI_HANDLE_RM_DEV_START_PKT_EV()

  DESCRIPTION
    This function processes RMNET_META_SM_RM_DEV_START_PKT_EV event.

    This event indicates that the tethered device wants to start a packet
    data session.

  PARAMETERS
    info - pointer to the interface controller's control block

  RETURN VALUE
    None

  DEPENDENCIES
    must !ONLY! be called from command dispatcher
    - state and initialization of the control block is verified there

  SIDE EFFECTS
    None
===========================================================================*/
static void rmnet_meta_smi_handle_rm_dev_start_pkt_ev
(
  rmnet_smi_info_type *  info
)
{
  rmnet_instance_e_type  rmnet_inst;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  rmnet_inst = RMNET_META_SM_INFO_TO_INSTANCE(info);

#ifdef T_ARM
  LOG_MSG_INFO2_2( "Recvd RM_DEV_START_PKT_EV, RMNET inst %d, state %d",
                   rmnet_inst, info->meta_sm.state);
#else
  MSG_SPRINTF_2(MSG_SSID_DS_PS, MSG_LEGACY_MED,
                "Recvd RM_DEV_START_PKT_EV, RMNET inst %d, state '%s'\n",
                rmnet_inst, rmnet_meta_smi_state_names[info->meta_sm.state] );
#endif

    /* clear autoconnect timer, as we are starting the call */
  (void) timer_clr( &(info->meta_sm.autoconnect_timer), T_NONE );

  switch( info->meta_sm.state )
  {
    case RMNET_META_SM_NULL_STATE:
    {
      /*---------------------------------------------------------------------
        Reset flow control status.
      ---------------------------------------------------------------------*/
      PS_ENTER_CRIT_SECTION(&rmnet_crit_section);
      info->rx_fc_mask = RMNET_FC_MASK_CALL_SETUP;
      PS_LEAVE_CRIT_SECTION(&rmnet_crit_section);

      /*---------------------------------------------------------------------
        Transition to UM_CONFIGURING state.
      ---------------------------------------------------------------------*/
      rmnet_meta_smi_transition_state(info, RMNET_META_SM_UM_CONFIGURING_STATE);
      break;
    }
    case RMNET_META_SM_UM_CONFIGURING_STATE:
    case RMNET_META_SM_WAITING_FOR_UM_ARB_STATE:
    case RMNET_META_SM_RM_CONFIGURING_STATE:
    case RMNET_META_SM_NET_IF_UP_STATE:
    case RMNET_META_SM_RM_RECONFIGURING_STATE:
    case RMNET_META_SM_WAITING_FOR_UM_DOWN_STATE:
    {
      /*---------------------------------------------------------------------
        Its error to get this event in all other states.
      ---------------------------------------------------------------------*/
      LOG_MSG_ERROR_1("Unexpected RM_DEV_START_PKT_EV in %d state",
                      info->meta_sm.state);
      break;
    }

    default:
    {
      LOG_MSG_ERROR_0("Bad RmNetSM state!");
      ASSERT(0);
      break;
    }
  } /* switch( info->meta_sm.state ) */
} /* rmnet_meta_smi_handle_rm_dev_start_pkt_ev() */


/*===========================================================================
  FUNCTION RMNET_META_SMI_HANDLE_RM_LINK_DOWN_EV

  DESCRIPTION
    This function processes RMNET_META_SM_RM_LINK_DOWN_EV event.

    This event indicates that the tethered device is closing the packet data
    session, and so the RMNET_META_SM should release associated packet sessions.

  PARAMETERS
    info - pointer to the interface controller's control block

  RETURN VALUE
    None

  DEPENDENCIES
    must !ONLY! be called from command dispatcher
    - state and initialization of the control block is verified there

  SIDE EFFECTS
    None
===========================================================================*/
static void rmnet_meta_smi_handle_rm_link_down_ev
(
  rmnet_smi_info_type *  info
)
{
  rmnet_instance_e_type  rmnet_inst;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  rmnet_inst = RMNET_META_SM_INFO_TO_INSTANCE(info);

#ifdef T_ARM
  LOG_MSG_INFO2_2( "Recvd RM_LINK_DOWN_EV, RMNET inst %d, state %d",
                   rmnet_inst, info->meta_sm.state);
#else
  MSG_SPRINTF_2(MSG_SSID_DS_PS, MSG_LEGACY_MED,
                "Recvd RM_LINK_DOWN_EV, RMNET inst %d, state '%s'\n",
                rmnet_inst, rmnet_meta_smi_state_names[info->meta_sm.state] );
#endif

  switch( info->meta_sm.state )
  {
    case RMNET_META_SM_UM_CONFIGURING_STATE:
    case RMNET_META_SM_WAITING_FOR_UM_ARB_STATE:
    case RMNET_META_SM_RM_CONFIGURING_STATE:
    case RMNET_META_SM_RM_RECONFIGURING_STATE:
    {
      /* Rm bailed during call setup/handoff, fall through */
      LOG_MSG_INFO2_0("Call setup/reconfig failed");

      if(info->meta_sm.dtr_is_high == TRUE)
      {
        rmnet_sm_ext_call_failure();
      }
    }

    /* fall through */
    case RMNET_META_SM_NET_IF_UP_STATE:
    {
      /* Reset the arb flag. If this flag is still set, um iface_down_ev
       * will be converted to um iface_down_arb_ev, and be rejected in
       * rmnet_meta_sm_waiting_for_um_down state */
      info->meta_sm.arb_close_in_progress = FALSE;

      rmnet_meta_smi_transition_state(info, RMNET_META_SM_WAITING_FOR_UM_DOWN_STATE);
      break;
    }

    case RMNET_META_SM_NULL_STATE:
    case RMNET_META_SM_WAITING_FOR_UM_DOWN_STATE:
    {
      /*---------------------------------------------------------------------
        Its error to get this event in all other states.
      ---------------------------------------------------------------------*/
      LOG_MSG_INFO1_1("Ignoring RM_LINK_DOWN_EV in %d state",
                      info->meta_sm.state);
      break;
    }

    default:
    {
      LOG_MSG_ERROR_0("Bad RmNetSM state!");
      ASSERT(0);
      break;
    }
  } /* switch( info->meta_sm.state ) */
} /* rmnet_meta_smi_handle_rm_link_down_ev() */


/*===========================================================================
  FUNCTION RMNET_META_SMI_HANDLE_UM_IFACE_DOWN_EV

  DESCRIPTION
    This function processes RMNET_META_SM_UM_IFACE_DOWN_EV event.

    This event indicates that network has closed the packet data session,
    and so the RMNET_META_SM should release the tethered packet session.

  PARAMETERS
    info - pointer to the interface controller's control block

  RETURN VALUE
    None

  DEPENDENCIES
    must !ONLY! be called from command dispatcher
    - state and initialization of the control block is verified there

  SIDE EFFECTS
    None
===========================================================================*/
static void rmnet_meta_smi_handle_um_iface_down_ev
(
  rmnet_smi_info_type *  info
)
{
  rmnet_instance_e_type  rmnet_inst;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  rmnet_inst = RMNET_META_SM_INFO_TO_INSTANCE(info);

#ifdef T_ARM
  LOG_MSG_INFO2_2( "Recvd UM_IFACE_DOWN_EV, RMNET inst %d, state %d",
                   rmnet_inst, info->meta_sm.state);
#else
  MSG_SPRINTF_2(MSG_SSID_DS_PS, MSG_LEGACY_MED,
                "Recvd UM_IFACE_DOWN_EV, RMNET inst %d, state '%s'\n",
                rmnet_inst, rmnet_meta_smi_state_names[info->meta_sm.state] );
#endif

  switch( info->meta_sm.state )
  {
    case RMNET_META_SM_RM_CONFIGURING_STATE:
    case RMNET_META_SM_NET_IF_UP_STATE:
    case RMNET_META_SM_RM_RECONFIGURING_STATE:
    case RMNET_META_SM_WAITING_FOR_UM_DOWN_STATE:
    case RMNET_META_SM_UM_CONFIGURING_STATE:
    case RMNET_META_SM_WAITING_FOR_UM_ARB_STATE:
      /* update the tx rx bytes for this call before rmnet iface goes down */
      if((E_SUCCESS != ps_stat_get_iface(PS_STAT_IFACE_BYTES_RX, info->rmnet_iface_ptr, &info->meta_sm.last_call_rx_bytes, 8)) ||
         (E_SUCCESS != ps_stat_get_iface(PS_STAT_IFACE_BYTES_TX, info->rmnet_iface_ptr, &info->meta_sm.last_call_tx_bytes, 8)))
      {
        MSG_ERROR("ps_iface_stat failure while caching byte stats for rmnet call",0,0,0);
        info->meta_sm.last_call_rx_bytes = 0;
        info->meta_sm.last_call_tx_bytes = 0;
      }
      rmnet_meta_smi_transition_state(info, RMNET_META_SM_NULL_STATE);
      break;

    case RMNET_META_SM_NULL_STATE:
    {
      /*---------------------------------------------------------------------
        Its error to get this event in all other states.
      ---------------------------------------------------------------------*/
      LOG_MSG_ERROR_1("Unexpected UM_IFACE_DOWN_EV in %d state",
                      info->meta_sm.state);
      break;
    }

    default:
    {
      LOG_MSG_ERROR_0("Bad RmNetSM state!");
      ASSERT(0);
      break;
    }
  } /* switch( info->meta_sm.state ) */
} /* rmnet_meta_smi_handle_um_iface_down_ev() */


/*===========================================================================
  FUNCTION RMNET_META_SMI_HANDLE_UM_IFACE_UP_EV

  DESCRIPTION
    This function processes RMNET_META_SM_UM_IFACE_UP_EV event.

    This event indicates that network has closed the packet data session,
    and so the RMNET_META_SM should release the tethered packet session.

  PARAMETERS
    info - pointer to the interface controller's control block

  RETURN VALUE
    None

  DEPENDENCIES
    must !ONLY! be called from command dispatcher
    - state and initialization of the control block is verified there

  SIDE EFFECTS
    None
===========================================================================*/
static void rmnet_meta_smi_handle_um_iface_up_ev
(
  rmnet_smi_info_type *  info
)
{
  rmnet_instance_e_type  rmnet_inst;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  rmnet_inst = RMNET_META_SM_INFO_TO_INSTANCE(info);

#ifdef T_ARM
  LOG_MSG_INFO2_2( "Recvd UM_IFACE_UP_EV, RMNET inst %d, state %d",
                   rmnet_inst, info->meta_sm.state );
#else
  MSG_SPRINTF_2(MSG_SSID_DS_PS, MSG_LEGACY_MED,
                "Recvd UM_IFACE_UP_EV, RMNET inst %d, state '%s'\n",
                rmnet_inst, rmnet_meta_smi_state_names[info->meta_sm.state] );
#endif

  /*-------------------------------------------------------------------------
    Um interface controller has [re]established Um interface and put it
    in up state.  Generate routeable indication on that (Um) interface
    to indicate that the address is in use by us.
  -------------------------------------------------------------------------*/
  switch( info->meta_sm.state )
  {
    case RMNET_META_SM_WAITING_FOR_UM_ARB_STATE:
    case RMNET_META_SM_UM_CONFIGURING_STATE:
    {
      if( ps_iface_state(info->um_iface_ptr) == IFACE_UP ||
          ps_iface_state(info->um_iface_ptr) == IFACE_ROUTEABLE )
      {
        LOG_MSG_INFO1_0( "Iface up ev, transition to RM_CONFIGURING_STATE" );
      /*---------------------------------------------------------------------
        In dynamic binding (e.g LTE), physlink is bound to iface after iface
        is UP. Physlink pointer will be NULL before the binding. For ifaces
        with static binding, physlink ev registration still happens before
        any data transfer begins i.e. before moving to Rm configuring state.
        Flow enabled event will not be missed because the event is sent right
        after event registration.
      ---------------------------------------------------------------------*/
        LOG_MSG_INFO1_1( "um iface ptr name 0x%x", info->um_iface_ptr->name );

        if ( rmnet_meta_smi_reg_physlink_events(info) == TRUE )
        {
          ps_iface_routeable_ind( info->um_iface_ptr );
          rmnet_meta_smi_transition_state (info,
                                           RMNET_META_SM_RM_CONFIGURING_STATE);
        }
        else
        {
          rmnet_meta_sm_post_event( RMNET_META_SM_INFO_TO_INSTANCE(info),
                                    RMNET_META_SM_UM_IFACE_DOWN_EV );
        }
      }
      else
      {
        LOG_MSG_INFO2_0("UM iface not UP, Ignore UP_EV! let Iface Down ev do clean up");
      }
      break;
    }

    case RMNET_META_SM_RM_CONFIGURING_STATE:
    case RMNET_META_SM_NET_IF_UP_STATE:
    case RMNET_META_SM_RM_RECONFIGURING_STATE:
    {
      if(ps_iface_state(info->um_iface_ptr) == IFACE_UP)
      {
        ps_iface_routeable_ind( info->um_iface_ptr );
      }
      else
      {
        LOG_MSG_INFO2_0("UM iface not UP, Ignore UP_EV! let Iface Down ev do clean up");
      }
      break;
    }

    case RMNET_META_SM_WAITING_FOR_UM_DOWN_STATE:  /* Um out of sync w/ RmSm */
    {
      if (info->meta_sm.restore_um_up)
      {
        LOG_MSG_INFO2_0 ("Um interface restored to UP as before rmnet call");
        break;
      }
    }

    /* fall through */
    case RMNET_META_SM_NULL_STATE:         /* event should be deregistered */
    default:
    {
      LOG_MSG_ERROR_1("Invalid RmNETSM state (%d)!", info->meta_sm.state);
      //ASSERT(0);
      break;
    }
  } /* switch( info->meta_sm.state ) */
} /* rmnet_meta_smi_handle_um_iface_up_ev() */


/*===========================================================================
  FUNCTION RMNET_META_SMI_HANDLE_ADDR_CFG_COMPLETE_EV

  DESCRIPTION
    This function processes RMNET_META_SM_ADDR_CFG_COMPLETE_EV event.

    This event indicates that address configuration on the Rm has completed
    successfully. The call is now up and data can be transferred.

  PARAMETERS
    info - pointer to the interface controller's control block

  RETURN VALUE
    None

  DEPENDENCIES
    must !ONLY! be called from command dispatcher
    - state and initialization of the control block is verified there

  SIDE EFFECTS
    None
===========================================================================*/
static void rmnet_meta_smi_handle_addr_cfg_complete_ev
(
  rmnet_smi_info_type *  info
)
{
  rmnet_instance_e_type  rmnet_inst;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  rmnet_inst = RMNET_META_SM_INFO_TO_INSTANCE(info);

#ifdef T_ARM
  LOG_MSG_INFO2_2( "Recvd ADDR_CFG_COMPLETE_EV, RMNET inst %d, state %d",
                   rmnet_inst, info->meta_sm.state);
#else
  MSG_SPRINTF_2(MSG_SSID_DS_PS, MSG_LEGACY_MED,
                "Recvd ADDR_CFG_COMPLETE_EV, RMNET inst %d, state '%s'\n",
                rmnet_inst, rmnet_meta_smi_state_names[info->meta_sm.state] );
#endif

  switch( info->meta_sm.state )
  {

    case RMNET_META_SM_RM_CONFIGURING_STATE:
    case RMNET_META_SM_RM_RECONFIGURING_STATE:
    {
      rmnet_meta_smi_transition_state(info, RMNET_META_SM_NET_IF_UP_STATE);
      break;
    }

    case RMNET_META_SM_WAITING_FOR_UM_DOWN_STATE:
    case RMNET_META_SM_NULL_STATE:
    {
      /*-----------------------------------------------------------------------
        Ignore in this state. Scenario: disconnect occurs from the ext device
        at the same time that address cfg completes in the sub state
        machine. When LINK_DOWN event is processed meta sm goes to waiting for
        Um down and then to NULL, and once sub sm comes up it posts
        ADDR_CFG_COMPLETE in this state. Sub sm is cleaned up in um iface down
        processing.
      -----------------------------------------------------------------------*/
      LOG_MSG_INFO1_1("Unexpected ADDR_CFG_COMPLETE_EV in %d state",
                      info->meta_sm.state);
      break;
    }

    case RMNET_META_SM_NET_IF_UP_STATE:
    case RMNET_META_SM_UM_CONFIGURING_STATE:
    case RMNET_META_SM_WAITING_FOR_UM_ARB_STATE:
    {
      /*---------------------------------------------------------------------
        Its error to get this event in all other states.
      ---------------------------------------------------------------------*/
      LOG_MSG_ERROR_1("Unexpected ADDR_CFG_COMPLETE_EV in %d state",
                      info->meta_sm.state);
      break;
    }

    default:
    {
      LOG_MSG_ERROR_0("Bad RmNetSM state!");
      ASSERT(0);
      break;
    }
  } /* switch( info->meta_sm.state ) */
} /* rmnet_meta_smi_handle_addr_cfg_complete_ev() */


/*===========================================================================
  FUNCTION RMNET_META_SMI_HANDLE_ADDR_CFG_FAILURE_EV

  DESCRIPTION
    This function processes RMNET_META_SM_ADDR_CFG_FAILURE_EV event.

    This event indicates that Rm failed to properly complete its address
    configuration. Possible causes include DHCP (for IPv4 calls) failing to
    start or the guard timer expiring.

  PARAMETERS
    info - pointer to the interface controller's control block

  RETURN VALUE
    None

  DEPENDENCIES
    must !ONLY! be called from command dispatcher
    - state and initialization of the control block is verified there

  SIDE EFFECTS
    None
===========================================================================*/
static void rmnet_meta_smi_handle_addr_cfg_failure_ev
(
  rmnet_smi_info_type *  info
)
{
  rmnet_instance_e_type  rmnet_inst;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  rmnet_inst = RMNET_META_SM_INFO_TO_INSTANCE(info);

#ifdef T_ARM
  LOG_MSG_INFO2_2( "Recvd ADDR_CFG_FAILURE_EV, RMNET inst %d state %d",
                   rmnet_inst, info->meta_sm.state);
#else
  MSG_SPRINTF_2(MSG_SSID_DS_PS, MSG_LEGACY_MED,
                "Recvd ADDR_CFG_FAILURE_EV, RMNET inst %d, state '%s'\n",
                rmnet_inst, rmnet_meta_smi_state_names[info->meta_sm.state] );
#endif

  switch( info->meta_sm.state )
  {

    case RMNET_META_SM_RM_CONFIGURING_STATE:
    case RMNET_META_SM_RM_RECONFIGURING_STATE:
    {
      rmnet_meta_smi_transition_state(info,
                                      RMNET_META_SM_WAITING_FOR_UM_DOWN_STATE);
      break;
    }

    case RMNET_META_SM_WAITING_FOR_UM_DOWN_STATE:
    {
      /* Do Nothing */
      LOG_MSG_INFO1_0("ADDR_CFG_FAILURE_EV received in WAITING_FOR_UM_DOWN_STATE");
      break;
    }

    case RMNET_META_SM_NET_IF_UP_STATE:
    case RMNET_META_SM_UM_CONFIGURING_STATE:
    case RMNET_META_SM_WAITING_FOR_UM_ARB_STATE:
    case RMNET_META_SM_NULL_STATE:
    {
      /*---------------------------------------------------------------------
        Its error to get this event in all other states.
      ---------------------------------------------------------------------*/
      LOG_MSG_ERROR_1("Unexpected ADDR_CFG_FAILURE_EV in %d state",
                      info->meta_sm.state);
      break;
    }

    default:
    {
      LOG_MSG_ERROR_0("Bad RmNetSM state!");
      ASSERT(0);
      break;
    }
  } /* switch( info->meta_sm.state ) */
} /* rmnet_meta_smi_handle_addr_cfg_failure_ev() */


/*===========================================================================
  FUNCTION RMNET_META_SMI_HANDLE_ADDR_CFG_CHANGED_EV

  DESCRIPTION
    This function processes RMNET_META_SM_ADDR_CFG_CHANGED_EV event.

    This event indicates that Um has changed addresses and now the Rm
    is in the process of reconfiguring its address.

  PARAMETERS
    info - pointer to the interface controller's control block

  RETURN VALUE
    None

  DEPENDENCIES
    must !ONLY! be called from command dispatcher
    - state and initialization of the control block is verified there

  SIDE EFFECTS
    None
===========================================================================*/
static void rmnet_meta_smi_handle_addr_cfg_changed_ev
(
  rmnet_smi_info_type *  info
)
{
  rmnet_instance_e_type  rmnet_inst;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  rmnet_inst = RMNET_META_SM_INFO_TO_INSTANCE(info);

#ifdef T_ARM
  LOG_MSG_INFO2_2( "Recvd ADDR_CFG_CHANGED_EV, RMNET inst %d, state %d",
                   rmnet_inst, info->meta_sm.state);
#else
  MSG_SPRINTF_2(MSG_SSID_DS_PS, MSG_LEGACY_MED,
                "Recvd ADDR_CFG_CHANGED_EV, RMNET inst %d, state '%s'\n",
                rmnet_inst, rmnet_meta_smi_state_names[info->meta_sm.state] );
#endif

  switch( info->meta_sm.state )
  {

    case RMNET_META_SM_NET_IF_UP_STATE:
    case RMNET_META_SM_RM_CONFIGURING_STATE:
    case RMNET_META_SM_RM_RECONFIGURING_STATE:
    {
      rmnet_meta_smi_transition_state(info,
                                      RMNET_META_SM_RM_RECONFIGURING_STATE);
      break;
    }

    case RMNET_META_SM_UM_CONFIGURING_STATE:
    case RMNET_META_SM_WAITING_FOR_UM_ARB_STATE:
    case RMNET_META_SM_WAITING_FOR_UM_DOWN_STATE:
    case RMNET_META_SM_NULL_STATE:
    {
      /*---------------------------------------------------------------------
        Its error to get this event in all other states.
      ---------------------------------------------------------------------*/
      LOG_MSG_ERROR_1("Unexpected ADDR_CFG_CHANGED_EV in %d state",
                      info->meta_sm.state);
      break;
    }

    default:
    {
      LOG_MSG_ERROR_0("Bad RmNetSM state!");
      ASSERT(0);
      break;
    }
  } /* switch( info->meta_sm.state ) */
} /* rmnet_meta_smi_handle_addr_cfg_changed_ev() */

/*===========================================================================
  FUNCTION RMNET_META_SMI_HANDLE_UM_IFACE_AUTHENTICATING_EV

  DESCRIPTION
    This function processes RMNET_SM_UM_IFACE_AUTHENTICATING_EV event.

    This event indicates that authentication process is in progress,
    and so the RMNET_SM should send an authenticating event on rm iface.

  PARAMETERS
    info - pointer to the interface controller's control block

  RETURN VALUE
    None

  DEPENDENCIES
    must !ONLY! be called from command dispatcher
    - state and initialization of the control block is verified there

  SIDE EFFECTS
    None
===========================================================================*/
static void rmnet_meta_smi_handle_um_iface_authenticating_ev
(
  rmnet_smi_info_type *  info
)
{
  rmnet_instance_e_type  rmnet_inst;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  rmnet_inst = RMNET_META_SM_INFO_TO_INSTANCE(info);

#ifdef T_ARM
  LOG_MSG_INFO2_2( "Recvd UM_IFACE_AUTHENTICATING_EV, RMNET inst %d, state %d",
                   rmnet_inst, info->meta_sm.state );
#else
  MSG_SPRINTF_2(MSG_SSID_DS_PS, MSG_LEGACY_MED,
                "Recvd UM_IFACE_AUTHENTICATING_EV, RMNET inst %d, state '%s'\n",
                rmnet_inst, rmnet_meta_smi_state_names[info->meta_sm.state] );
#endif

  /*-------------------------------------------------------------------------
    Um interface controller has put Um interface in authenticating state.
    Generate authenticating indication on (Rm) interface
  -------------------------------------------------------------------------*/
  ps_iface_generic_ind( info->rmnet_iface_ptr, IFACE_AUTHENTICATING_EV, NULL );

} /* rmnet_meta_smi_handle_um_iface_authenticating_ev() */

/*===========================================================================
  FUNCTION RMNET_META_SMI_HANDLE_UM_IFACE_BEARER_TECH_CHANGED_EV

  DESCRIPTION
    This function processes
RMNET_META_SMI_HANDLE_UM_IFACE_BEARER_TECH_CHANGED_EV event.

    This event indicates that bearer tech has changed update the phys links
accordingly.

  PARAMETERS
    info - pointer to the interface controller's control block

  RETURN VALUE
    None

  DEPENDENCIES
    must !ONLY! be called from command dispatcher
    - state and initialization of the control block is verified there

  SIDE EFFECTS
    None
===========================================================================*/
static void rmnet_meta_smi_handle_um_iface_bearer_tech_changed_ev
(
  rmnet_smi_info_type *  info
)
{
  ps_iface_type *base_um_iface_ptr;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /* Update phys links where there is an active call */
  if( info->meta_sm.state != RMNET_META_SM_NULL_STATE )
  {
    rmnet_meta_smi_update_physlink(RMNET_META_SM_INFO_TO_INSTANCE(info));

    if ( PS_IFACE_IS_VALID(info->um_iface_ptr) &&
         info->meta_sm.state == RMNET_META_SM_NET_IF_UP_STATE &&
         info->meta_sm.softap_mode == FALSE)
    {
      /* Reset the optimized DL path for the new base Um iface.
       * The older Um reset is taken care by framework.
       * Do this only if RmNet call is up, If its coming up
       * the RmNet SM would take care of setting this path.
       * */
      PS_ENTER_CRIT_SECTION(&global_ps_crit_section);
      base_um_iface_ptr = PS_IFACE_GET_BASE_IFACE(info->um_iface_ptr);
      PS_IFACEI_SET_OPT_RM_IFACE(base_um_iface_ptr, info->rmnet_iface_ptr);
      PS_LEAVE_CRIT_SECTION(&global_ps_crit_section);
      LOG_MSG_INFO1_2("RmNet Bearer Tech Change Event: "
                      "Set DL opt path for iface0x%x:%d",
                      info->um_iface_ptr->name, info->um_iface_ptr->instance);
    }
  }
}/* rmnet_meta_smi_handle_um_iface_bearer_tech_changed_ev */

/*===========================================================================
  FUNCTION RMNET_META_SMI_HANDLE_UM_IFACE_ARB_EV

  DESCRIPTION
    This function processes RMNET_SM_UM_IFACE_ARB_EV event.

    This event indicates that arbitration process is in progress.

  PARAMETERS
    info - pointer to the interface controller's control block

  RETURN VALUE
    None

  DEPENDENCIES
    must !ONLY! be called from command dispatcher
    - state and initialization of the control block is verified there

  SIDE EFFECTS
    None
===========================================================================*/
static void rmnet_meta_smi_handle_um_iface_arb_ev
(
  rmnet_smi_info_type *  info
)
{
  rmnet_instance_e_type  rmnet_inst;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  rmnet_inst = RMNET_META_SM_INFO_TO_INSTANCE(info);

#ifdef T_ARM
  LOG_MSG_INFO2_2( "Recvd UM_IFACE_ARB_EV, RMNET inst %d, state %d",
                   rmnet_inst, info->meta_sm.state );
#else
  MSG_SPRINTF_2(MSG_SSID_DS_PS, MSG_LEGACY_MED,
                "Recvd UM_IFACE_ARB_EV, RMNET inst %d, state '%s'\n",
                rmnet_inst, rmnet_meta_smi_state_names[info->meta_sm.state] );
#endif

  switch( info->meta_sm.state )
  {
    case RMNET_META_SM_UM_CONFIGURING_STATE:
    case RMNET_META_SM_WAITING_FOR_UM_ARB_STATE:
      rmnet_meta_smi_transition_state(info, RMNET_META_SM_WAITING_FOR_UM_ARB_STATE);
      break;

    case RMNET_META_SM_RM_CONFIGURING_STATE:
    case RMNET_META_SM_NET_IF_UP_STATE:
    case RMNET_META_SM_RM_RECONFIGURING_STATE:
    case RMNET_META_SM_WAITING_FOR_UM_DOWN_STATE:
    case RMNET_META_SM_NULL_STATE:
    {
      /*---------------------------------------------------------------------
        Its error to get this event in all other states.
      ---------------------------------------------------------------------*/
      LOG_MSG_ERROR_1("Unexpected UM_IFACE_ARB_EV in %d state",
                      info->meta_sm.state);
      break;
    }

    default:
    {
      LOG_MSG_ERROR_0("Bad RmNetSM state!");
      ASSERT(0);
      break;
    }
  } /* switch( info->meta_sm.state ) */
} /* rmnet_meta_smi_handle_um_iface_arb_ev() */


/*===========================================================================
FUNCTION   RMNET_META_SMI_IFACE_EV_HDLR_CB()

DESCRIPTION
  This callback function is called when one of the registered ps_iface events
  occur on Um interface.  Various Um iface events are registered based on
  call type during the call setup type.  Each iface event generates
  appropriate rmnet_meta_sm event .

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
static void rmnet_meta_smi_um_iface_ev_hdlr_cb
(
  ps_iface_type             *this_iface_ptr,
  ps_iface_event_enum_type   event,
  ps_iface_event_info_u_type event_info,
  void                      *user_data_ptr
)
{
  rmnet_smi_info_type *  info;
  rmnet_instance_e_type      rmnet_inst;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT (user_data_ptr);

  info = (rmnet_smi_info_type *) user_data_ptr;
  rmnet_inst = RMNET_META_SM_INFO_TO_INSTANCE(info);

  LOG_MSG_INFO2_3( "Recvd iface ev %d from 1x i/f 0x%x, RMNET inst %d",
                   event, this_iface_ptr, rmnet_inst );

  /*-------------------------------------------------------------------------
    Process flow control events first since they may get posted during data
    transfer
  -------------------------------------------------------------------------*/

  switch( event )
  {
    case IFACE_FLOW_ENABLED_EV:
    {
      /*---------------------------------------------------------------------
        For network model calls send signal to PS to resume pulling PPP
        packets from SIO rx_wm, laptop flow will be enabled by rx_wm lo
        water func.
      ---------------------------------------------------------------------*/
      RMNET_SET_SIGNAL( info->constants.rx_sig );
      RMNET_SET_LEGACY_SIGNAL(rmnet_smi_dual_ip_info[info->constants.qmi_instance].legacy_sig);
      rmnet_meta_smi_rx_fc(info, RMNET_FC_MASK_IFACE, FALSE);
      break;
    }

    case IFACE_FLOW_DISABLED_EV:
    {
      /*---------------------------------------------------------------------
        For rmnet calls we don't need to do anything when UM flow is
        disabled, rx function will stop pulling PPP packets from SIO rx_wm.
        rx_wm hi water function will disable laptop flow.
      ---------------------------------------------------------------------*/
      rmnet_meta_smi_rx_fc(info, RMNET_FC_MASK_IFACE, TRUE);
      break;
    }

    case IFACE_LINGERING_EV:
    case IFACE_ACTIVE_OUT_OF_USE_EV:
    case IFACE_DOWN_EV:
    {
      /*---------------------------------------------------------------------
        UM iface is down. This event can also be generated when we register
        for events since that happens during call setup ie UM is in DOWN
        state. So check the previous state before taking any action. Also
        initiate the teardown of the SUB_SM, ensuring it is cleaned up
        prior to Um tear down and the um_iface_ptr being cleared from the
        RmNet instance.
        If iface down is because of arbitration, post IFACE_ARB_EV so that
        SM moves to new state to handle this special case.
      ---------------------------------------------------------------------*/
      if( info->meta_sm.arb_close_in_progress )
      {
        LOG_MSG_INFO1_1( "IFACE_DOWN_EV during arb, reason [%d], posting UM_IFACE_DOWN_ARB_EV",
                         event_info.iface_down_info.netdown_reason );
        rmnet_meta_sm_post_event( RMNET_META_SM_INFO_TO_INSTANCE(info),
                                  RMNET_META_SM_UM_IFACE_DOWN_ARB_EV );
      }
      else
      {
        info->meta_sm.call_end_reason =
          event_info.iface_down_info.netdown_reason;

        LOG_MSG_INFO1_1( "Um iface down, reason [%d], posting RMNET_META_SM_UM_IFACE_DOWN_EV",
                         event_info.iface_down_info.netdown_reason );

        rmnet_meta_smi_stop_addr_cfg_sm(info);

        rmnet_meta_sm_post_event( RMNET_META_SM_INFO_TO_INSTANCE(info),
                                  RMNET_META_SM_UM_IFACE_DOWN_EV );
      }

      break;
    }

    case IFACE_UP_EV:
    {
      /*---------------------------------------------------------------------
        Traffic channel on UM has come up. This event is used for Relay and
        network model call setup.
      ---------------------------------------------------------------------*/
      rmnet_meta_sm_post_event( RMNET_META_SM_INFO_TO_INSTANCE(info),
                                RMNET_META_SM_UM_IFACE_UP_EV );
      break;
    }

    case IFACE_AUTHENTICATING_EV:
    {
      /*---------------------------------------------------------------------
        Authentication in progress
      ---------------------------------------------------------------------*/
      rmnet_meta_sm_post_event( RMNET_META_SM_INFO_TO_INSTANCE(info),
                                RMNET_META_SM_UM_IFACE_AUTHENTICATING_EV );
      break;
    }

#ifdef FEATURE_DATA_WLAN_MAPCON
    case IFACE_BEARER_TECH_EX_CHANGED_EV:
#else
    case IFACE_BEARER_TECH_CHANGED_EV:
#endif /* FEATURE_DATA_WLAN_MAPCON */
    {
      rmnet_meta_sm_post_event( RMNET_META_SM_INFO_TO_INSTANCE(info),
                                RMNET_META_SM_UM_IFACE_BEARER_TECH_CHANGED_EV
                              );
          break;
    }

    default:
    {
      LOG_MSG_ERROR_1("Unregistered event %d recvd, ignoring", event);
      break;
    }
  } /* switch( event ) */
} /* rmnet_meta_smi_um_iface_ev_hdlr_cb() */



/*===========================================================================
FUNCTION   RMNET_META_SMI_UM_PHYS_LINK_EV_HDLR_CB()

DESCRIPTION
  This callback function is called when one of the registered ps_phys_link events
  occur on Um interface.  Various Um iface events are registered based on
  call type during the call setup type.  Each iface event generates
  appropriate rmnet_meta_sm event .

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
static void rmnet_meta_smi_um_phys_link_ev_hdlr_cb
(
  ps_phys_link_type             *this_phys_link_ptr,
  ps_iface_event_enum_type      event,
  ps_iface_event_info_u_type    event_info,
  void                          *user_data_ptr
)
{
  rmnet_smi_info_type *  info;
  rmnet_instance_e_type          rmnet_inst;
  rmnet_smi_cmd_type   * cmd_ptr;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT (user_data_ptr);

  info = (rmnet_smi_info_type *) user_data_ptr;
  rmnet_inst = RMNET_META_SM_INFO_TO_INSTANCE(info);

  LOG_MSG_INFO2_3( "Recvd phys link ev %d from phys_link ptr 0x%x, RMNET inst %d",
                   event, this_phys_link_ptr, rmnet_inst);

  /*-------------------------------------------------------------------------
    Act on change in phys link flow control
  -------------------------------------------------------------------------*/
  switch( event )
  {
    case PHYS_LINK_FLOW_ENABLED_EV:
    {
      /*---------------------------------------------------------------------
        For network model calls send signal to PS to resume pulling LLC
        packets from SIO rx_wm, laptop flow will later be enabled by rx_wm
        low water callback.
      ---------------------------------------------------------------------*/
      RMNET_SET_SIGNAL( info->constants.rx_sig );
      RMNET_SET_LEGACY_SIGNAL(rmnet_smi_dual_ip_info[info->constants.qmi_instance].legacy_sig);
      rmnet_meta_smi_rx_fc(info, RMNET_FC_MASK_PHYS_LINK, FALSE);
      break;
    }

    case PHYS_LINK_FLOW_DISABLED_EV:
    {
      /*---------------------------------------------------------------------
        For rmnet calls we don't need to do anything when UM flow is
        disabled, rx function will stop pulling PPP packets from SIO rx_wm.
        (laptop flow is disabled only when rx_wm hi water is reached)
      ---------------------------------------------------------------------*/
      rmnet_meta_smi_rx_fc(info, RMNET_FC_MASK_PHYS_LINK, TRUE);
      break;
    }

    case PHYS_LINK_UP_EV:
    case PHYS_LINK_DOWN_EV:
    {
       /*-------------------------------------------------------------------------
          Get a DCC command buffer
       -------------------------------------------------------------------------*/
       cmd_ptr = (rmnet_smi_cmd_type *)RMNET_SMI_GET_CMD_BUF(DCC_RMNET_SM_CMD,FALSE);
       if (cmd_ptr == NULL)
       {
          return;
       }

       /*-------------------------------------------------------------------------
          Post cmd to DS task.
       -------------------------------------------------------------------------*/
       cmd_ptr->sm_id     = RMNET_META_SM;
       cmd_ptr->cmd_type  = RMNET_SM_CMD_PROCESS_PHYS_LINK_UP_DOWN_EVT;
       cmd_ptr->info_ptr  = (void *) info;
       cmd_ptr->data.phys_link_status.phys_link_ptr = this_phys_link_ptr;
       cmd_ptr->data.phys_link_status.event         = event;
       cmd_ptr->data.phys_link_status.event_info    = event_info;
       cmd_ptr->data.phys_link_status.user_data_ptr = user_data_ptr;

       dcc_send_cmd_ex(DCC_RMNET_SM_CMD, (void *)cmd_ptr);
       break;
    }

    default:
    {
      LOG_MSG_ERROR_1("Unregistered event %d recvd, ignoring", event);
      break;
    }
  } /* switch( event ) */
} /* rmnet_meta_smi_um_phys_link_ev_hdlr_cb() */



/*===========================================================================
FUNCTION   RMNET_META_SMI_UM_FLOW_EV_HDLR_CB()

DESCRIPTION
  This callback function is called when one of the registered ps_phys_link events
  occur on Um interface.  Various Um iface events are registered based on
  call type during the call setup type.  Each iface event generates
  appropriate rmnet_meta_sm event .

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
static void rmnet_meta_smi_um_flow_ev_hdlr_cb
(
  ps_flow_type                *this_flow_ptr,
  ps_iface_event_enum_type     event,
  ps_iface_event_info_u_type   event_info,
  void                        *user_data_ptr
)
{
  rmnet_smi_info_type *  info;
  rmnet_instance_e_type        rmnet_inst;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT (user_data_ptr);

  info = (rmnet_smi_info_type *) user_data_ptr;
  rmnet_inst = RMNET_META_SM_INFO_TO_INSTANCE(info);

  LOG_MSG_INFO2_3( "Recvd flow ev %d from flow ptr 0x%x, RMNET inst %d",
                   event, this_flow_ptr, rmnet_inst );

  /*-------------------------------------------------------------------------
    Act on change in phys link flow control
  -------------------------------------------------------------------------*/
  switch( event )
  {
    case FLOW_TX_ENABLED_EV:
    {
      /*---------------------------------------------------------------------
        For network model calls send signal to PS to resume pulling LLC
        packets from SIO rx_wm, laptop flow will later be enabled by rx_wm
        low water callback.
      ---------------------------------------------------------------------*/
      RMNET_SET_SIGNAL( info->constants.rx_sig );
      RMNET_SET_LEGACY_SIGNAL(rmnet_smi_dual_ip_info[info->constants.qmi_instance].legacy_sig);
      rmnet_meta_smi_rx_fc(info, RMNET_FC_MASK_FLOW, FALSE);
      break;
    }

    case FLOW_TX_DISABLED_EV:
    {
      /*---------------------------------------------------------------------
        For rmnet calls we don't need to do anything when UM flow is
        disabled, rx function will stop pulling PPP packets from SIO rx_wm.
        (laptop flow is disabled only when rx_wm hi water is reached)
      ---------------------------------------------------------------------*/
      rmnet_meta_smi_rx_fc(info, RMNET_FC_MASK_FLOW, TRUE);
      break;
    }

    default:
    {
      LOG_MSG_ERROR_1("Unregistered event %d recvd, ignoring", event);
      break;
    }
  } /* switch( event ) */
} /* rmnet_meta_smi_um_flow_ev_hdlr_cb() */

/*===========================================================================
  FUNCTION RMNET_META_SMI_PROCESS_PHYS_LINK_UP_DOWN_EVENT()

  DESCRIPTION
    This function is called from DCC task and is used to process phys link
    events on UM iface. RMNET_META_SMI_UM_PHYS_LINK_EV_HDLR_CB() posts a
    RMNET_SM_CMD to handle these events in DCC task. This function generates
    DIAG event with Rm watermark levels on PHYS_LINK/PHYS_LINK_DOWN events.

  PARAMETERS
    this_phys_link_ptr:  Pointer to Physlink
    event: The event that is being posted to the state machine.
    event_info    : info for the event
    user_data_ptr : user data

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void rmnet_meta_smi_process_phys_link_up_down_event
(
  ps_phys_link_type            *this_phys_link_ptr,
  ps_iface_event_enum_type      event,
  ps_iface_event_info_u_type    event_info,
  void                         *user_data_ptr
)
{
  rmnet_smi_info_type *  info;
  rmnet_instance_e_type          rmnet_inst;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT (user_data_ptr && this_phys_link_ptr);

  info = (rmnet_smi_info_type *) user_data_ptr;
  rmnet_inst = RMNET_META_SM_INFO_TO_INSTANCE(info);

  LOG_MSG_INFO2_3( "Recvd phys link ev %d from phys_link ptr 0x%x, RMNET inst %d",
                   event, this_phys_link_ptr, rmnet_inst);

  switch( event )
  {
    case PHYS_LINK_UP_EV:
    {
     /*---------------------------------------------------------------------
       Generate wm levels DIAG event for phys_link_up_ev
      ---------------------------------------------------------------------*/
      rmnet_meta_smi_log_wmk_levels(rmnet_inst, this_phys_link_ptr,
                                     PS_WMK_LOGGING_ACTIVE);
      break;
    }

    case PHYS_LINK_DOWN_EV:
    {
     /*---------------------------------------------------------------------
       Generate wm levels DIAG event for phys_link_down_ev
      ---------------------------------------------------------------------*/
      rmnet_meta_smi_log_wmk_levels(rmnet_inst, this_phys_link_ptr,
                                    PS_WMK_LOGGING_DORMANT);

      break;
    }

    default:
    {
      LOG_MSG_ERROR_1("Unregistered event %d recvd, ignoring", event);
      break;
    }
  } /* switch( event ) */

} /* rmnet_meta_smi_process_phys_link_up_down_event */

/*===========================================================================
  FUNCTION RMNET_META_SMI_AUTCONNECT_TIMER_CBACK()

  DESCRIPTION
    Autoconnect retry timer callback

  PARAMETERS
    user_data -> rmnet_smi_info

  RETURN VALUE
    None

  DEPENDENCIES
    rmnet_sio_init() must have been called previously

  SIDE EFFECTS
    None
===========================================================================*/
static void rmnet_meta_smi_autoconnect_timer_cback
(
  timer_cb_data_type  user_data
)
{
  rmnet_smi_cmd_type   * cmd_ptr;
  rmnet_smi_info_type  * info;

  ASSERT (user_data);
  info = (rmnet_smi_info_type *) user_data;
  /*-------------------------------------------------------------------------
    Get a DCC command buffer
  -------------------------------------------------------------------------*/
  cmd_ptr = (rmnet_smi_cmd_type *)RMNET_SMI_GET_CMD_BUF(DCC_RMNET_SM_CMD,FALSE);
  if (cmd_ptr == NULL)
  {
    return;
  }

  /*-------------------------------------------------------------------------
    Post cmd to DS task.
  -------------------------------------------------------------------------*/
  cmd_ptr->sm_id     = RMNET_META_SM;
  cmd_ptr->cmd_type  = RMNET_SM_CMD_AUTOCONNECT_TIMER;
  cmd_ptr->info_ptr  = (void *) info;

  dcc_send_cmd_ex(DCC_RMNET_SM_CMD, (void *)cmd_ptr);
} /* rmnet_meta_smi_autoconnect_timer_cback() */

/*===========================================================================
  FUNCTION RMNET_META_SMI_PROCESS_AUTCONNECT_TIMER_CMD_HDLR()

  DESCRIPTION
    Autoconnect retry timer command handler

  PARAMETERS
    user_data -> rmnet_smi_info

  RETURN VALUE
    None

  DEPENDENCIES
    rmnet_sio_init() must have been called previously

  SIDE EFFECTS
    None
===========================================================================*/
static void rmnet_meta_smi_process_autoconnect_timer_cmd_hdlr
(
  void              *user_data
)
{
  rmnet_smi_info_type *  info;
  rmnet_instance_e_type  rmnet_instance;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  info = (rmnet_smi_info_type *) user_data;
  ASSERT (info);

   rmnet_instance = RMNET_META_SM_INFO_TO_INSTANCE(info);

  if (!RMNET_IS_VALID_LAPTOP_INSTANCE(rmnet_instance))
  {
    LOG_MSG_ERROR_1("Not valid Laptop instance [%d]", rmnet_instance);
    return;
  }

  LOG_MSG_INFO1_2(" Autoconnect: Timer expiry for instance %d autoconnect config %d",
                  rmnet_instance, info->meta_sm.settings.auto_connect_enabled);

  if(info == rmnet_smi_dual_ip_info[info->constants.qmi_instance].info_v4 )
  {
    if ( rmnet_efs_config_params[RMNET_GET_LAPTOP_INSTANCE(rmnet_instance)].\
                                                     ip_type == QMI_AF_INET6 )
    {
      LOG_MSG_INFO1_0("EFS:IPV6 ONLY; exiting from V4 timer expiry");
      return;
    }
  }
  else if(info == rmnet_smi_dual_ip_info[info->constants.qmi_instance].info_v6 )
  {
    if ( rmnet_efs_config_params[RMNET_GET_LAPTOP_INSTANCE(rmnet_instance)].\
                                                      ip_type == QMI_AF_INET )
    {
      LOG_MSG_INFO1_0("EFS : IPV4 ONLY; exiting from V6 timer expiry");
      return;
    }
  }

  /*-------------------------------------------------------------------------
    Post dev_start_pkt ev only if dtr is high (as it could have gone low
    between timer start and expiry)
  -------------------------------------------------------------------------*/

  if( (info->meta_sm.settings.auto_connect_enabled) &&
          (info->meta_sm.ports_active == TRUE) &&
          (rmnet_meta_smi_verify_dtr_high(info) == TRUE) )
  {
    LOG_MSG_INFO1_0("autoconnect retry timer expired.");
    rmnet_meta_smi_handle_rm_dev_start_pkt_ev(info);
  }
    else
  {
    (void) timer_clr( &(info->meta_sm.autoconnect_timer), T_NONE );
    info->meta_sm.autoconnect_next_backoff_time = AUTOCONNECT_BACKOFF_TIMER_MIN_VALUE;
    (void) timer_set( &(info->meta_sm.autoconnect_timer),
                        info->meta_sm.autoconnect_next_backoff_time, 0, T_SEC );
  }
} /* rmnet_meta_smi_process_autoconnect_timer_cmd_hdlr */


/*===========================================================================
  FUNCTION RMNET_META_SMI_TRANSITION_STATE()

  DESCRIPTION
    This function performs state transition actions.

  PARAMETERS
    new_state: the state to transition to

  RETURN VALUE
    None

  DEPENDENCIES
    Its the caller's responsibility that it initiates only valid state
    transitions. This function does not check if a state transition is
    valid or not.

  SIDE EFFECTS
    None
===========================================================================*/
static void rmnet_meta_smi_transition_state
(
  rmnet_smi_info_type      *  info,
  rmnet_meta_smi_state_type   new_state
)
{
  int16                       ps_iface_errno = 0;
  byte                        rm_hw_addr[LAN_IEEE_MAC_ADDR_LEN];
  uint16                      qmi_inst_id;
  ps_iface_event_info_u_type  event_info;
  void * net_info_cfg_ptr;
  int retval;
  uint8    i;
  rmnet_instance_e_type  rmnet_inst;
  ps_iface_net_down_reason_type net_down_reason;
#ifdef FEATURE_DATA_RMNET_USB_STANDARD_ECM
  sio_ioctl_param_type ioctl_param;
#endif
#ifdef FEATURE_QXDM_WM_TRACING
  uint32                       call_id = 0;
  sint15                       ioctl_errno = 0;
#endif /* FEATURE_QXDM_WM_TRACING */
  uint16                 umts_default_profile_num = RMNET_EFS_INVALID_PROFILE;
  uint16                 cdma_default_profile_num = RMNET_EFS_INVALID_PROFILE;
  uint8                  profile_family = 0;
  rmnet_meta_sm_arb_info_type     arb_info;
  uint8                  ips_id_cleanup = 0;
  ps_iface_type         *base_um_iface_ptr;
  sio_port_id_type       sio_port;
#ifdef FEATURE_DATA_A2
  qmi_mux_port_cfg_enum       mux_port_cfg;
#endif /* FEATURE_DATA_A2 */
  int16                  ps_errno;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  memset( &arb_info, 0, sizeof(rmnet_meta_sm_arb_info_type) );

  rmnet_inst = RMNET_META_SM_INFO_TO_INSTANCE(info);
  sio_port = rmnet_sio_get_port(info->meta_sm.rmnet_sio_handle);

#ifdef T_ARM
  LOG_MSG_INFO2_3( "RMNET_META_SM inst %d, state goes %d to %d",
                   rmnet_inst, info->meta_sm.state, new_state);
#else
  MSG_SPRINTF_3(MSG_SSID_DS_PS, MSG_LEGACY_MED,
                "RMNET_META_SM inst %d, state goes '%s' to '%s'\n",
                rmnet_inst,
                rmnet_meta_smi_state_names[info->meta_sm.state],
                rmnet_meta_smi_state_names[new_state] );
#endif

  ds_qmi_cflog_meta_sm_state_change( (uint32) info->meta_sm.state,
                                     (uint32) new_state );

  memset( &event_info, 0, sizeof(event_info) );
  info->meta_sm.state = new_state;

  /*-------------------------------------------------------------------------
    Perform action associated with entering into new state.
  -------------------------------------------------------------------------*/
  switch( new_state )
  {
    /*= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
                        RMNET_META_SM_NULL_STATE
    = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =*/
    case RMNET_META_SM_NULL_STATE:
    {
      PS_ENTER_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
      info->has_flow = FALSE;

      rmnet_smi_dual_ip_info[info->constants.qmi_instance].rx_flow_count =
        rmnet_smi_dual_ip_info[info->constants.qmi_instance].info_v4->has_flow +
        rmnet_smi_dual_ip_info[info->constants.qmi_instance].info_v6->has_flow;
      /*---------------------------------------------------------------------
        Pkt call is terminating, Perform all the necessary cleanup
      ---------------------------------------------------------------------*/
      if(info->meta_sm.llc_started)
      {
        (void) lan_llc_stop( info->constants.lan_llc_instance );
        info->meta_sm.llc_started = FALSE;
      }

#ifdef FEATURE_QXDM_WM_TRACING
      /*---------------------------------------------------------------------
        Deregister for Watermark tracing.
      ---------------------------------------------------------------------*/
      fc_post_deregister_cmd_wm_trace(info->call_id, RMNET_CLIENT);
      info->call_id = 0;
#endif /* FEATURE_QXDM_WM_TRACING */

      /*clear WM queue only if this is last SM to go down*/
      if( 0 == rmnet_smi_dual_ip_info[info->constants.qmi_instance].rx_flow_count )
      {
        rmnet_sio_clear_wmk_queues(info->meta_sm.rmnet_sio_handle);
      }

      /*---------------------------------------------------------------------
        Set the Um interface bringup preferences back to default values
        before the next session
        - set the profile num to default profile. For umts, this value is used
          for autoconnect which is applicable to laptop instance only. Hence
          ok to set the profile family to rmnet_umts_profile_family. For all
          other cases, the caller (wds/qos) ensures that the appropriate
          profile family is set.
        - tech_pref is ANY (WWAN_GROUP as of now)
      ---------------------------------------------------------------------*/

      /*Save ips_id for mbim call cleanup before clearing net params*/

      ips_id_cleanup = info->meta_sm.net_params.ips_id;

#ifdef FEATURE_DS_MULTIPLE_NAI
    #ifdef FEATURE_DS_MOBILE_IP
      mip_reg_ses_info_cb( NULL );
    #endif
#endif
      memset( &(info->meta_sm.net_params),
              0x0,
              sizeof(rmnet_meta_sm_network_info_type) );

      profile_family = DS_PROFILE_3GPP_RMNET_PROFILE_FAMILY;
      rmnet_inst = RMNET_META_SM_INFO_TO_INSTANCE(info);

      info->meta_sm.data_format_client = RMNET_SET_DATA_FORMAT_NONE;

      if ( RMNET_IS_VALID_LAPTOP_INSTANCE(rmnet_inst))
      {
        umts_default_profile_num = (uint16)
         rmnet_efs_config_params[RMNET_GET_LAPTOP_INSTANCE(rmnet_inst)].\
                                                                 umts_profile;
        cdma_default_profile_num = (uint16)
          rmnet_efs_config_params[RMNET_GET_LAPTOP_INSTANCE(rmnet_inst)].\
                                                                 cdma_profile;

        /* Set the default IP family pref as per EFS configuration */
        if ( rmnet_efs_config_params[RMNET_GET_LAPTOP_INSTANCE(rmnet_inst)].\
                                                   ip_type != QMI_AF_INET_4_6 )
        {
          /* Set the default IP family pref as per EFS configuration */
          info->meta_sm.net_params.ip_family_pref = (uint8)
             rmnet_efs_config_params[RMNET_GET_LAPTOP_INSTANCE(rmnet_inst)].\
                                                                       ip_type;
        }
        else
        {
          if(info == rmnet_smi_dual_ip_info[info->constants.qmi_instance].\
                                                                     info_v4 )
          {
            info->meta_sm.net_params.ip_family_pref = QMI_AF_INET;
          }
          else if(info == rmnet_smi_dual_ip_info\
                                      [info->constants.qmi_instance].info_v6 )
          {
            info->meta_sm.net_params.ip_family_pref = QMI_AF_INET6;
          }
        }
      }
      if ( umts_default_profile_num ==RMNET_EFS_INVALID_PROFILE )
      {
        (void)rmnet_meta_smi_retrieve_default_profile_num(DS_PROFILE_TECH_3GPP,
                                                      profile_family,
                                               &umts_default_profile_num);
      }
      /* Read EFS configuration and if it has invalid profile info, use the
         default profile information */
      info->meta_sm.net_params.umts_profile_num = umts_default_profile_num;
      profile_family = 0;
      if (cdma_default_profile_num == RMNET_EFS_INVALID_PROFILE)
      {
        (void)rmnet_meta_smi_retrieve_default_profile_num(
                                                          DS_PROFILE_TECH_3GPP2,
                                                          profile_family,
                                                          &cdma_default_profile_num);
      }
      /* Read EFS configuration and if it has invalid profile info, use the
         default profile information */
      info->meta_sm.net_params.cdma_profile_num = (uint8)cdma_default_profile_num;

      info->meta_sm.net_params.tech_pref        = WWAN_GROUP;
      info->meta_sm.net_params.subscription_id = PS_SYS_DEFAULT_SUBS;

      /*---------------------------------------------------------------------
        Unbridge Rm and Um interfaces
      ---------------------------------------------------------------------*/
      if (info->rmnet_iface_ptr)
      {
        ps_iface_set_bridge( info->rmnet_iface_ptr, NULL );
      }
      if (info->um_iface_ptr)
      {
        rex_enter_crit_sect(&lan_llc_inst_to_um_iface_ptr_crit_sec);
        lan_llc_inst_to_um_iface_ptr_lut[info->constants.lan_llc_instance] = NULL;
        rex_leave_crit_sect(&lan_llc_inst_to_um_iface_ptr_crit_sec);

        ps_iface_set_bridge( info->um_iface_ptr, NULL );

        PS_ENTER_CRIT_SECTION(&global_ps_crit_section);
        base_um_iface_ptr = PS_IFACE_GET_BASE_IFACE(info->um_iface_ptr);
        PS_IFACEI_SET_OPT_RM_IFACE(base_um_iface_ptr, NULL);
        PS_LEAVE_CRIT_SECTION(&global_ps_crit_section);
        LOG_MSG_INFO1_2("RmNet NULL state: Reset DL opt path for iface0x%x:%d",
                        info->um_iface_ptr->name,
                        info->um_iface_ptr->instance);

        /*-------------------------------------------------------------------
          Deregister all the Um interface, flow, and phys link event callbacks
        -------------------------------------------------------------------*/
        rmnet_meta_smi_dereg_events(info);

        /*-------------------------------------------------------------------
          Unbind from the Um interface
        -------------------------------------------------------------------*/
        info->um_iface_ptr = NULL;
        info->um_iface_handle = 0;
      }
      /*---------------------------------------------------------------------
        By default, don't restore resolved Um interface to UP after RmNet
        call teardown.  We'll query the Um state before calling bringup and
        save the previous state in this restore_um_up variable if this
        needs to be done.
      ---------------------------------------------------------------------*/
      info->meta_sm.restore_um_up = FALSE;

      /*---------------------------------------------------------------------
        Rm iface is down - Indicate that we are not in call anymore
      ---------------------------------------------------------------------*/
      if (info->rmnet_iface_ptr)
      {
        ps_phys_link_gone_ind(PS_IFACE_GET_PHYS_LINK(info->rmnet_iface_ptr));
        ps_iface_down_ind_ex(info->rmnet_iface_ptr,
                                         info->meta_sm.call_end_reason);
        /*-------------------------------------------------------------
          Clear the powersave mode filters installed, if any
        ----------------------------------------------------------------*/
        ps_iface_ipfltr_delete( info->rmnet_iface_ptr,
                                IP_FLTR_CLIENT_POWERSAVE,
                                PS_IFACE_IPFLTR_GENERIC_HANDLE, &ps_errno );
      }

    #ifdef FEATURE_DATA_RMNET_USB_STANDARD_ECM
      /* send notification only if this is last SM to go down*/
      if( 0 == rmnet_smi_dual_ip_info[info->constants.qmi_instance].rx_flow_count )
      {
        LOG_MSG_INFO2_0("Sending Link DOWN notification to ECM driver");
        rmnet_sio_link_up_notify(info->meta_sm.rmnet_sio_handle,
                                 FALSE); //link_DOWN
      }
    #endif
      if (info->rmnet_iface_ptr != NULL)
      {
        /* clear the RmNet iface to Rmnet instance binding */
        for (i = 0; i < RMNET_IFACE_MAX; i++)
        {
          if ( (rmnet_smi_iface_info[i] != NULL) &&
               (&rmnet_smi_iface_info[i]->rmnet_iface == info->rmnet_iface_ptr) )
          {
            break;
          }
        }

        ASSERT((i < RMNET_IFACE_MAX) && (rmnet_smi_iface_info[i]->in_use == TRUE));
        rmnet_smi_iface_info[i]->in_use = FALSE;
      }

      /* make the RmNet iface, llc_inst, rx_sig available for use
         by other rmnet instances */
      info->rmnet_iface_ptr = NULL;
      info->constants.lan_llc_instance = LAN_LLE_MIN;
      info->constants.rx_sig = RMNET_MAX_SIGNALS;

      info->meta_sm.call_end_reason = PS_NET_DOWN_REASON_NOT_SPECIFIED;

      /* reset arb related flag */
      info->meta_sm.arb_close_in_progress = FALSE;
      info->meta_sm.proc_id               = RMNET_PROC_ID_MAX;

      /* disable flow from SIO and deregister RX function only if this is last SM to go down*/
      if( 0 == rmnet_smi_dual_ip_info[info->constants.qmi_instance].rx_flow_count )
      {
#ifdef FEATURE_DATA_MBIM
        /*release signal if not in MBIM. Else will be done in cleanup() where
          we need to check if there are other calls on different ports in
          which case we need to hold on to the signal*/

        if (info->meta_sm.data_agg_protocol.ul_data_agg_protocol !=
              RMNET_ENABLE_DATA_AGG_MBIM)
        {
#endif /*FEATURE_DATA_MBIM*/
          for( i = 0; i < RMNET_IFACE_MAX; i++ )
          {
            if(rmnet_signals[i].sig == rmnet_smi_dual_ip_info[info->constants.qmi_instance].rx_sig)
            {
              rmnet_signals[i].in_use = FALSE;
              break;
            }
          }
#ifdef FEATURE_DATA_MBIM
        }
#endif/*FEATURE_DATA_MBIM*/

        rmnet_smi_dual_ip_info[info->constants.qmi_instance].rx_sig = RMNET_MAX_SIGNALS;
        /*---------------------------------------------------------------------
          Disable the flow.
        ---------------------------------------------------------------------*/
        rmnet_sio_set_rx_flow( info->meta_sm.rmnet_sio_handle,
                               RMNET_SIO_FLOWMASK_ALL,
                               TRUE );

        /*---------------------------------------------------------------------
          Deregister rmnet_sio rx function
        ---------------------------------------------------------------------*/
        rmnet_sio_set_rx_cb( info->meta_sm.rmnet_sio_handle, NULL, info );

        /* Enable flow so that USB buffers are flushed */
        rmnet_sio_set_rx_flow( info->meta_sm.rmnet_sio_handle,
                               RMNET_SIO_FLOWMASK_ALL,
                               FALSE );

        /*---------------------------------------------------------------------
          Cleanup RmNet Legacy hdlr for the dual_ip info instance only if this
          is the last call to be bought down.

          1. Release legacy signals - Set Signal in RmNet instance and Signal
             List to FALSE
          2. Cleanup Legacy path Watermark/queues
          3. Deregister non_empty CB fn for Legacy Wmk.
        ---------------------------------------------------------------------*/
        rmnet_meta_smi_legacy_sig_hdlr_cleanup(
                    &rmnet_smi_dual_ip_info[info->constants.qmi_instance]);

#if defined(FEATURE_RMNET_PORT_CONFIG_MSM) && defined(FEATURE_DATA_A2)
        if (RMNET_IS_VALID_LAPTOP_INSTANCE(rmnet_inst))
        {
          if ( rmnet_sio_is_a2_port(sio_port) &&
              !rmnet_meta_sm_in_call((rmnet_inst & 0x1) ? rmnet_inst - 1 : rmnet_inst + 1) )
          {
            // Both v4 and v6 are down
            a2_sio_notify_call_state(A2_SIO_CALL_STATE_TETH_DOWN, sio_port);
            LOG_MSG_INFO1_0("Teth call down");
          }
        }
#endif /* FEATURE_RMNET_PORT_CONFIG_MSM && FEATURE_DATA_A2 */

      }else
      {
        // if this is not last instance then check if we need to resume flow

        if( rmnet_meta_instance_rx_ready(info->constants.qmi_instance) )
        {
          // in this case it is possible that flow is disabled by instance that is shutting down
          // and the other instance is waiting for flow to enable
          // in this situation we can resume flow and ignore packets for going down instance
          // that are blocking flow
	  RMNET_SET_SIGNAL( rmnet_smi_dual_ip_info[info->constants.qmi_instance].rx_sig );
          RMNET_SET_LEGACY_SIGNAL(rmnet_smi_dual_ip_info[info->constants.qmi_instance].legacy_sig);
        }
      }

#ifdef FEATURE_DATA_MBIM
      /*---------------------------------------------------------------------
        Cleanup MBIM call info
      ---------------------------------------------------------------------*/
      if (info->meta_sm.data_agg_protocol.dl_data_agg_protocol ==
          RMNET_ENABLE_DATA_AGG_MBIM)
      {
        rmnet_meta_smi_mbim_call_cleanup(info,ips_id_cleanup);
      }
#endif/*FEATURE_DATA_MBIM*/

      /*---------------------------------------------------------------------
        Restart the autoconnect timer if autoconnect enabled and dtr is high.

        If the cable is unplugged don't auto reconnect.
      ---------------------------------------------------------------------*/
      if( (info->meta_sm.settings.auto_connect_enabled) &&
          (rmnet_meta_smi_verify_dtr_high(info) == TRUE) )
      {
        LOG_MSG_INFO1_0("Starting autoconnect timer in NULL state");
        timer_set( &(info->meta_sm.autoconnect_timer),
                    info->meta_sm.autoconnect_next_backoff_time,
                    0,
                    T_SEC );
        info->meta_sm.autoconnect_next_backoff_time *= 2;
        if(info->meta_sm.autoconnect_next_backoff_time >
           AUTOCONNECT_BACKOFF_TIMER_MAX_VALUE)
        {
          info->meta_sm.autoconnect_next_backoff_time =
            AUTOCONNECT_BACKOFF_TIMER_MAX_VALUE;
        }
      }

      if (info->meta_sm.data_agg_protocol.dl_data_agg_protocol ==
          RMNET_ENABLE_DATA_AGG_MBIM ||
          info->meta_sm.data_agg_protocol.dl_data_agg_protocol ==
          RMNET_ENABLE_DATA_AGG_QC_NCM)
      {
        if (info->meta_sm.ntb_header != NULL)
        {
          PS_SYSTEM_HEAP_MEM_FREE(info->meta_sm.ntb_header);
        }
      }

      /*---------------------------------------------------------------------------
        Free RNDIS hdr if allocated.
      ---------------------------------------------------------------------------*/
      if (info->meta_sm.data_agg_protocol.dl_data_agg_protocol ==
          RMNET_ENABLE_DATA_AGG_RNDIS &&
          info->meta_sm.rndis_eth_hdr != NULL)
      {
        PS_SYSTEM_HEAP_MEM_FREE(info->meta_sm.rndis_eth_hdr);
        info->meta_sm.eth_arped = FALSE;
      }

      else if (info->link_prot == RMNET_ETHERNET_MODE &&
          info->meta_sm.eth_header != NULL)
      {
        PS_SYSTEM_HEAP_MEM_FREE(info->meta_sm.eth_header);
        info->meta_sm.eth_header = NULL;
        info->meta_sm.eth_arped = FALSE;
      }

      /*---------------------------------------------------------------------------
        Reset data format if triggered by DTR low
      ---------------------------------------------------------------------------*/
      if (info->meta_sm.reset_data_format)
      {
        LOG_MSG_INFO1_1("Resetting data format rmnet inst %d", rmnet_inst);
        rmnet_meta_smi_set_default_data_format(rmnet_inst);
        info->meta_sm.reset_data_format = FALSE;
      }

      info->meta_sm.softap_mode = FALSE;
      PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
      break;
    }

    /*= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
                        RMNET_META_SM_UM_CONFIGURING_STATE
    = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =*/
    case RMNET_META_SM_UM_CONFIGURING_STATE:
    {

      /*---------------------------------------------------------------------
        Notify A2
      ---------------------------------------------------------------------*/
#if defined(FEATURE_RMNET_PORT_CONFIG_MSM) && defined(FEATURE_DATA_A2)
        if (RMNET_IS_VALID_LAPTOP_INSTANCE(rmnet_inst))
        {
          if ( rmnet_sio_is_a2_port(sio_port) &&
              !rmnet_meta_sm_in_call((rmnet_inst & 0x1) ? rmnet_inst - 1 : rmnet_inst + 1) )
          {
            // First call on the instance is coming up 
            a2_sio_notify_call_state(A2_SIO_CALL_STATE_TETH_UP, sio_port);
            LOG_MSG_INFO1_0("Teth call coming up");
          }
        }
#endif /* FEATURE_RMNET_PORT_CONFIG_MSM && FEATURE_DATA_A2 */

      /*---------------------------------------------------------------------
        Bring up the Um interface
      ---------------------------------------------------------------------*/
      if (rmnet_sio_check_status(info->meta_sm.rmnet_sio_handle) == FALSE)
      {
        LOG_MSG_INFO1_0("Can't start call, RMNET SIO not initialized!");
        /*-------------------------------------------------------------------
          Assume we can't receive SNI yet here - if netsio hasn't
          initialized, no control txfr (i.e. QMI command) will occur yet.
          No iface down ind required here.
        -------------------------------------------------------------------*/
        break;
      }

#ifdef FEATURE_DATA_A2
#ifdef FEATURE_DATA_MBIM
      /*---------------------------------------------------------------------
       * Init MBIM data path
      ---------------------------------------------------------------------*/
      if ((info->meta_sm.data_agg_protocol.ul_data_agg_protocol ==
            RMNET_ENABLE_DATA_AGG_MBIM) && (info->meta_sm.data_format == 0)
         )
      {
        rmnet_meta_smi_init_rx_mbim_data_path(info);
      }
#endif /* FEATURE_DATA_MBIM*/
      mux_port_cfg = qmi_get_mux_port_cfg();
      if (mux_port_cfg != QMI_MUX_PORT_CFG_DISABLED)
      {
        if (RMNET_IS_VALID_LAPTOP_INSTANCE(rmnet_inst) ||
            RMNET_DATA_AGG_IS_QMAP(info->meta_sm.data_agg_protocol.ul_data_agg_protocol))
        {
          /*---------------------------------------------------------------------
           * Set mux port configuration
          ---------------------------------------------------------------------*/
          if ( !rmnet_sio_set_mux_port_cfg(
                    info->meta_sm.rmnet_sio_handle) )
          {
            LOG_MSG_ERROR_0("Unable to set mux port cfg");
            rmnet_meta_sm_post_event( RMNET_META_SM_INFO_TO_INSTANCE(info),
                                    RMNET_META_SM_RM_LINK_DOWN_EV );
            return;
          }
        }
      }
#endif /* FEATURE_DATA_A2 */

      /* Find a free Rmnet Iface for this Rmnet instance*/
      for (i=0; i < RMNET_IFACE_MAX; i++)
      {
        if ( (rmnet_smi_iface_info[i] == NULL) ||
             (rmnet_smi_iface_info[i]->in_use == FALSE) )
        {
          break;
        }
      }

      ASSERT(i < RMNET_IFACE_MAX);

      if (rmnet_smi_iface_info[i] == NULL)
      {
        if (rmnet_meta_smi_iface_init(i) == FALSE)
        {
          LOG_MSG_ERROR_1("Unable to init rmnet iface [%d]", i);
          ASSERT(0);
          break;
        }
        ASSERT(rmnet_smi_iface_info[i] != NULL);
      }

      rmnet_smi_iface_info[i]->in_use = TRUE;

      /* populate the rmnet instance with the iface, llc_inst and rx_sig */
      info->rmnet_iface_ptr = &(rmnet_smi_iface_info[i]->rmnet_iface);
      info->constants.lan_llc_instance = rmnet_smi_iface_info[i]->constants.lan_llc_instance;
      info->constants.llc_rx_sig = rmnet_smi_iface_info[i]->constants.rx_sig;

      /*---------------------------------------------------------------------
        Set the network params values (to configure UM) in rmnet sm info
        struct before calling bring_up
      ---------------------------------------------------------------------*/
      rmnet_sm_config_net_params(&(info->meta_sm.net_params.cfg_info), info->meta_sm.net_params.call_type);

      /*---------------------------------------------------------------------
       * Resolve default subscription
      ---------------------------------------------------------------------*/
      if (info->meta_sm.net_params.subscription_id == PS_SYS_DEFAULT_SUBS)
      {
        info->meta_sm.net_params.subscription_id = ps_sys_get_default_data_subscription();
      }
      LOG_MSG_INFO2_2("rmnet inst %d subs id %d",
                 rmnet_inst, info->meta_sm.net_params.subscription_id);

      /*---------------------------------------------------------------------
        Initialize RM iface address family.
        Will be reset to UM iface address family when UM is available
      ---------------------------------------------------------------------*/
      rmnet_set_rm_iface_addr_family(info->rmnet_iface_ptr, info->meta_sm.net_params.ip_family_pref);

      /*---------------------------------------------------------------------
        Bring up a data call if we have coverage (IFACE state != DISABLED)
        and if there is no call currently active over Um packet
        (IFACE state == DOWN).

        Note that context lock is required because sockets calls originate
        in another task context (PS).

        So we need to make sure that if we decide to originate,
        IFACE state should be set to something other than DOWN (or DISABLED)
        (to prevent async call) before freeingup the locks. This ensures
        only one type of 1x call originates at a time.
      ---------------------------------------------------------------------*/
      /*---------------------------------------------------------------------
        Generate a "coming up" indication using internal API since we don't
        support bring_up_cmd handler yet.

        later - remove this, and QMI should call bringup on RmNet iface which
        should establish the laptop rmnet call.  But will have to ensure
        sockets can't bring up the RmNet interface (would need a way to
        distinguish how interfaces are to be bridged - or just have generic
        routing to handle the flow of traffic between ifaces).
      ---------------------------------------------------------------------*/
      event_info.state = info->rmnet_iface_ptr->iface_private.state;
      info->rmnet_iface_ptr->iface_private.state = IFACE_COMING_UP;
      ps_ifacei_invoke_event_cbacks(info->rmnet_iface_ptr,
                                    NULL,
                                    IFACE_COMING_UP_EV,
                                    event_info);

      /* inform QMI WDS of the rmnet_iface_ptr chosen, since it needs to
         listen on the rmnet iface events
         Need to do this after rmnet goes to COMING_UP, because otherwise
         WDS gets a DOWN_EV immediately after registering.
      */

      if(info == rmnet_smi_dual_ip_info[info->constants.qmi_instance].info_v4 )
        wds_rmnet_iface_binding_complete_cb(info->rmnet_iface_ptr, info->constants.qmi_instance, 0,
                info->meta_sm.net_params.subscription_id);
      else
        wds_rmnet_iface_binding_complete_cb(info->rmnet_iface_ptr, info->constants.qmi_instance, 1,
                info->meta_sm.net_params.subscription_id);


      /*---------------------------------------------------------------------
        Formulate the Rm network interface's 802.3 MAC address

        Vendor ID (most significant 3 bytes) is set to the IEEE assigned
        Vendor ID for the customer.

        TODO - add an NV item for this.

        Device ID (least significant 3 bytes) is set based on the media
        type and underlying QMI link ID.

        byte     description
        ----     -----------
        05 MSB   \
        04       +  Vendor ID = hard coded MSM Net device VID (TBD)
        03       /
        02       media type => see header file rmnet_mac_media_e_type
        01       host QMI link instance (obtained via QMI_CTL_SET_INSTANCE)
        00       msm  QMI link instance (RmNetSm instance)
      ---------------------------------------------------------------------*/

#define QMI_ETH_VENDOR_ID  (0x0250F3) /* To be assigned */

#define QMI_ETH_DEVICE_ID_USB  (0x00)
      if(FALSE == qmi_ctl_get_instance_id(info->constants.qmi_instance,
                                          &qmi_inst_id ))
      {
        LOG_MSG_ERROR_0("Failure in qmi_ctl_get_instance_id");
      }
      rm_hw_addr[0] = (QMI_ETH_VENDOR_ID >> 16) & 0xFF;
      rm_hw_addr[1] = (QMI_ETH_VENDOR_ID >> 8 ) & 0xFF;
      rm_hw_addr[2] = (QMI_ETH_VENDOR_ID      ) & 0xFF;
      rm_hw_addr[3] = QMI_ETH_DEVICE_ID_USB;
      rm_hw_addr[4] = (qmi_inst_id >> 8)        & 0xFF;
      rm_hw_addr[5] = (qmi_inst_id     )        & 0xFF;

      LOG_MSG_INFO2_2( "Set Rm MAC address to 0x%x:0x%x",
                       QMI_ETH_VENDOR_ID,
                       QMI_ETH_DEVICE_ID_USB << 16 | qmi_inst_id );

    #ifdef FEATURE_DATA_RMNET_USB_STANDARD_ECM
      memset(&ioctl_param, 0, sizeof(ioctl_param));

      rmnet_sio_ioctl(info->meta_sm.rmnet_sio_handle,SIO_IOCTL_IS_STANDARD_ECM,&ioctl_param);
      /* GET MAC addr from ECM only if in STD ECM mode */
      if(ioctl_param.is_standard_ecm == TRUE)
      {
        rmnet_sio_ioctl(info->meta_sm.rmnet_sio_handle,SIO_IOCTL_GET_MAC_ADDRESS,&ioctl_param);
          /* add sio ioctl to query MAC address from USB and use it instead of the above
             value */

        memscpy(rm_hw_addr, 
                (LAN_IEEE_MAC_ADDR_LEN > SIO_MAC_ADDR_STR_SIZE ? SIO_MAC_ADDR_STR_SIZE : LAN_IEEE_MAC_ADDR_LEN), 
                ioctl_param.mac_address,
          (LAN_IEEE_MAC_ADDR_LEN > SIO_MAC_ADDR_STR_SIZE ? SIO_MAC_ADDR_STR_SIZE : LAN_IEEE_MAC_ADDR_LEN) );
      }
    #endif

      /*---------------------------------------------------------------------
        Set the Rm network interface's hardware address.
      ---------------------------------------------------------------------*/
      rmnet_meta_smi_set_mac(info->rmnet_iface_ptr,
                              rm_hw_addr,
                              sizeof (rm_hw_addr) );

      /*---------------------------------------------------------------------
        if the valid_fields are 0, implies that NO overrides were set in SNI
        so pass NULL in bring_up() in which case the mode handler will retrieve
        the values from profile.
        Note - Unlike legacy modem calls, For Rmnet call UMTS mode handler will
        calculate the challenge as in sockets case
      ---------------------------------------------------------------------*/
      net_info_cfg_ptr = info->meta_sm.net_params.cfg_info.valid_fields ?
                           &(info->meta_sm.net_params.cfg_info) : NULL;

      LOG_MSG_INFO2_1( "net info cfg ptr 0x%x", net_info_cfg_ptr );


      retval = rmnet_meta_smi_bring_up_um_iface_by_policy( &info->meta_sm.net_params,
                                                           &info->um_iface_ptr,
                                                            rmnet_inst,
                                                           &ps_iface_errno,
                                                           &net_down_reason ) ;

      // Set RM iface addr family
      if (info->um_iface_ptr != NULL)
      {
        info->um_iface_handle = PS_IFACE_GET_EVENT_HANDLE(info->um_iface_ptr);
        LOG_MSG_INFO2_2("Um iface 0x%x handle 0x%x",
                        info->um_iface_ptr, info->um_iface_handle);
        rmnet_set_rm_iface_addr_family(
                     info->rmnet_iface_ptr,
                     ps_iface_get_addr_family(info->um_iface_ptr));
      }

      // If um iface is already in use by another rmnet instance, reject SNI.
      // Do this check if the um iface is up already or bringup in progress
      if ( (retval == 0 || ps_iface_errno == DS_EWOULDBLOCK) &&
           (info->um_iface_ptr != NULL) )
      {
        if ( rmnet_um_iface_in_use(info) )
        {
          LOG_MSG_ERROR_1( "Um iface 0x%x in use by another rmnet instance, tearing down",
                           info->um_iface_ptr );

          // Tear down um iface (since we already called bringup).
          // This will just decrease the ref count and return 0 normally.
          // If the recall returns EWOULDBLOCK, still need to do the same.
          (void) rmnet_tear_down_um_iface( info );

          // Set retval to -1 for later clean up (fall through)
          info->um_iface_ptr = NULL;
          info->um_iface_handle = 0;
          retval = -1;
          ps_iface_errno = DS_ECONNREFUSED;
        }
      }

      // If um iface is already in use by another rmnet instance with arb_close_in_progress set to TRUE, reject SNI.
      // Do this check if the um iface is up already or bringup in progress
      if ( ps_iface_errno == DS_ENETCLOSEINPROGRESS &&  rmnet_um_iface_is_arb_progress(info) )
      {
        LOG_MSG_ERROR_1( "Um iface 0x%x in use by another rmnet instance, tearing down",
                         info->um_iface_ptr );

        // Tear down um iface (since we already called bringup).
        // This will just decrease the ref count and return 0 normally.
        // If the recall returns EWOULDBLOCK, still need to do the same.
        (void) rmnet_tear_down_um_iface( info );

        // Set retval to -1 for later clean up (fall through)
        info->um_iface_ptr = NULL;
        info->um_iface_handle = 0;
        retval = -1;
        ps_iface_errno = DS_ECONNREFUSED;
      }

      rex_enter_crit_sect(&lan_llc_inst_to_um_iface_ptr_crit_sec);
      lan_llc_inst_to_um_iface_ptr_lut[info->constants.lan_llc_instance] = info->um_iface_ptr;
      rex_leave_crit_sect(&lan_llc_inst_to_um_iface_ptr_crit_sec);

      if( retval == 0 )
      {
        /* UP event handler will be called when we reg below */
        /*-------------------------------------------------------------------
          If bringup returns 0, interface is UP already.

          Routing rules should really enforce whether we can use this
          interface.  For now, assume that an UP interface is in use,
          unless the ALWAYS_ON attribute is set (i.e. WLAN only) or if it is
          a BCMCS/MCAST iface (which goes UP right after it is known that
          the network supports the technology).
        -------------------------------------------------------------------*/
        if ( (PS_IFACE_GET_IS_ALWAYS_ON(info->um_iface_ptr)) ||
             (PS_IFACE_GET_IS_ACTIVE(info->um_iface_ptr)) ||
             (info->meta_sm.net_params.is_mcast_iface == TRUE) )
        {
          LOG_MSG_INFO2_2("Always on or mcast Iface 0x%x handle 0x%x already up",
                          info->um_iface_ptr, info->um_iface_handle);
          info->meta_sm.restore_um_up = TRUE;
        }
        else
        {
          /*---------------------------------------------------------------
            UM iface is already UP or was lingering.
            Nothing needs to be done here. Register for events.
          ---------------------------------------------------------------*/
          LOG_MSG_INFO2_2("UM iface 0x%x handle 0x%x is already UP",
                          info->um_iface_ptr, info->um_iface_handle);
        }
      }
      else if( ps_iface_errno == DS_EWOULDBLOCK )  // retval == -1
      {
        /*-------------------------------------------------------------------
          Bringup in progress.
        -------------------------------------------------------------------*/
        LOG_MSG_INFO1_2("Iface 0x%x handle 0x%x bringup in progress",
                        info->um_iface_ptr, info->um_iface_handle);
      }
      else if( ps_iface_errno == DS_ENETCLOSEINPROGRESS )
      {
        /*-------------------------------------------------------------------
          Arbitration resulted in Iface teardown in progress. Register new
          set of Iface event handlers for arbitration.
        -------------------------------------------------------------------*/
        LOG_MSG_INFO1_2("Iface 0x%x handle 0x%x arb close in progress",
                        info->um_iface_ptr, info->um_iface_handle);
        info->meta_sm.arb_close_in_progress = TRUE;
      }
      else //ret_val == -1 and errno unhandled
      {
        /*-------------------------------------------------------------------
          Bringup failed.  Cleanup RMNET_META_SM and current pkt data call.
        -------------------------------------------------------------------*/
        LOG_MSG_ERROR_0( "UM bringup failed, posting UM_IFACE_DOWN" );

        ps_iface_down_ind_ex( info->rmnet_iface_ptr,
                              net_down_reason );

        #ifdef FEATURE_DATA_RMNET_USB_STANDARD_ECM
        LOG_MSG_INFO1_0( "Sending Link DOWN notification to ECM driver" );
        rmnet_sio_link_up_notify( info->meta_sm.rmnet_sio_handle,
                                  FALSE ); //link_DOWN
        #endif

        /*-------------------------------------------------------------------
          Iface in incorrect state or bringup failure. Reset SM to NULL.
        -------------------------------------------------------------------*/
        rmnet_meta_sm_post_event( RMNET_META_SM_INFO_TO_INSTANCE(info),
                                  RMNET_META_SM_UM_IFACE_DOWN_EV );

        break;
      } /* if retval */


      /*-------------------------------------------------------------------
        Register all the Um interface, flow, and phys link event callbacks
      -------------------------------------------------------------------*/
      if(rmnet_meta_smi_reg_events(info) == FALSE)
      {
        LOG_MSG_ERROR_0( "Failed to register for events" );
        info->meta_sm.arb_close_in_progress = FALSE;
        rmnet_meta_sm_post_event( RMNET_META_SM_INFO_TO_INSTANCE(info),
                                  RMNET_META_SM_UM_IFACE_DOWN_EV );
      }

      if(retval == 0)
      {
        /*ps_iface_bring_up returned 0*/

        if( ps_iface_state(info->um_iface_ptr) == IFACE_ROUTEABLE )
        {
          /*UM iface is already brough up and posisbly in use by
          another entity hence it is in routeable state. In this
          situation behaviour should be as if UM iface UP event
          arrived.*/
          LOG_MSG_INFO1_0("UM iface is in ROUTEABLE state");

          /*---------------------------------------------------------------------
            In dynamic binding (e.g LTE), physlink is bound to iface after iface
            is UP. Physlink pointer will be NULL before the binding. For ifaces
            with static binding, physlink ev registration still happens before
            any data transfer begins i.e. before moving to Rm configuring state.
            Flow enabled event will not be missed because the event is sent right
            after event registration.
          ---------------------------------------------------------------------*/
          LOG_MSG_INFO1_1( "um iface ptr name 0x%x",
                           info->um_iface_ptr->name );

          if ( rmnet_meta_smi_reg_physlink_events(info) == TRUE )
          {
            rmnet_meta_smi_transition_state (info,
                                             RMNET_META_SM_RM_CONFIGURING_STATE);
          }
          else
          {
            rmnet_meta_sm_post_event( RMNET_META_SM_INFO_TO_INSTANCE(info),
                                      RMNET_META_SM_UM_IFACE_DOWN_EV );
          }
        }
      }

      break;
    }

    /*= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
                        RMNET_META_SM_WAITING_FOR_UM_ARB_STATE
    = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =*/
    case RMNET_META_SM_WAITING_FOR_UM_ARB_STATE:
    {
        /*---------------------------------------------------------------------
          Enter this state when UM Iface ARB event is recevied
        ---------------------------------------------------------------------*/

        /*--------------------------------------------------------------------
          Deregister all the Um interface, flow, and phys link event callbacks
        ---------------------------------------------------------------------*/
        /* this routine tries to dereg physlink events too, confirm it's ok */
        LOG_MSG_INFO1_0( "In Arb, entered RMNET_META_SM_WAITING_FOR_UM_ARB_STATE" );

        rmnet_meta_smi_dereg_events( info );

        rmnet_inst = RMNET_META_SM_INFO_TO_INSTANCE(info);
        retval = rmnet_meta_smi_bring_up_um_iface_by_policy( &info->meta_sm.net_params,
                                                         &info->um_iface_ptr,
                                                          rmnet_inst,
                                                         &ps_iface_errno,
                                                         &net_down_reason);

        if (info->um_iface_ptr != NULL)
        {
          info->um_iface_handle = PS_IFACE_GET_EVENT_HANDLE(info->um_iface_ptr);
          LOG_MSG_INFO2_2("Um iface 0x%x handle 0x%x",
                          info->um_iface_ptr, info->um_iface_handle);
        }

        if (retval == 0)
        {
          LOG_MSG_INFO1_2( "Arb Iface 0x%x handle 0x%x already up, unexpected, post Iface UP ev",
                           info->um_iface_ptr, info->um_iface_handle );
          info->meta_sm.arb_close_in_progress = FALSE;
          rmnet_meta_sm_post_event( RMNET_META_SM_INFO_TO_INSTANCE(info),
                                    RMNET_META_SM_UM_IFACE_UP_EV );
        }
        else if( ps_iface_errno == DS_EWOULDBLOCK )
        {
          /*-------------------------------------------------------------------
            Bringup in progress.
          -------------------------------------------------------------------*/
          info->meta_sm.arb_close_in_progress = FALSE;
          LOG_MSG_INFO1_2( "Arb Iface 0x%x handle 0x%x bringup in progress",
                           info->um_iface_ptr, info->um_iface_handle );
        }
        else if( ps_iface_errno == DS_ENETCLOSEINPROGRESS )
        {
          /*-------------------------------------------------------------------
            Close is still in progress.
          -------------------------------------------------------------------*/
          info->meta_sm.arb_close_in_progress = TRUE;
          LOG_MSG_INFO1_2( "Arb Iface 0x%x handle 0x%x close is still in progress",
                           info->um_iface_ptr, info->um_iface_handle );
        }
        else
        {
          LOG_MSG_ERROR_2( "Arb Iface 0x%x, unhandled errno [%d]",
                           info->um_iface_ptr, ps_iface_errno );
          info->meta_sm.arb_close_in_progress = FALSE;
          rmnet_meta_sm_post_event( RMNET_META_SM_INFO_TO_INSTANCE(info),
                                    RMNET_META_SM_UM_IFACE_DOWN_EV );
          break;
        }

      /*-------------------------------------------------------------------
        Register all the Um interface, flow, and phys link event callbacks
      -------------------------------------------------------------------*/
      if(rmnet_meta_smi_reg_events(info) == FALSE)
      {
        LOG_MSG_ERROR_0( "Failed to register for events" );
        info->meta_sm.arb_close_in_progress = FALSE;
        rmnet_meta_sm_post_event( RMNET_META_SM_INFO_TO_INSTANCE(info),
                                  RMNET_META_SM_UM_IFACE_DOWN_EV );
      }

      break; /* RMNET_META_SM_WAITING_FOR_UM_ARB_STATE */
    }

    /*= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
                        RMNET_META_SM_RM_CONFIGURING_STATE
    = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =*/
    case RMNET_META_SM_RM_CONFIGURING_STATE:
    {
      /*---------------------------------------------------------------------
        Start LAN LLC and initiate the Rm interface specific address
        configuration state machine.
	We need to start DHCP here first because as soon Rm post phys link
	 up, runtime_settings is invoked which requires DHCP to be up too.
	Also ps_iface_set_bridge if invoked for RmNet ethernet mode
          would expect lan_llc to be started already.
      ---------------------------------------------------------------------*/
      if(-1 == rmnet_meta_smi_start_addr_cfg_sm(info))
      {
        LOG_MSG_ERROR_0("Rm bringup failed, posting Rm LINK_DOWN" );
        rmnet_meta_sm_post_event( RMNET_META_SM_INFO_TO_INSTANCE(info),
                                  RMNET_META_SM_RM_LINK_DOWN_EV );
        break;
      }

      /*---------------------------------------------------------------------
        Bridge Rm iface.
      ---------------------------------------------------------------------*/
      ps_iface_set_bridge(info->rmnet_iface_ptr, info->um_iface_ptr );

      ps_iface_phys_link_up_ind(info->rmnet_iface_ptr );

      info->has_flow = TRUE;

      rmnet_smi_dual_ip_info[info->constants.qmi_instance].rx_flow_count =
        rmnet_smi_dual_ip_info[info->constants.qmi_instance].info_v4->has_flow +
        rmnet_smi_dual_ip_info[info->constants.qmi_instance].info_v6->has_flow;

      /* if this is the first SM to start call then register call backs and enable flow*/
      if( 1 == rmnet_smi_dual_ip_info[info->constants.qmi_instance].rx_flow_count )
      {
        /*---------------------------------------------------------------------
          Switch rmnet_sio receive function to rmnet_meta_sm receive function.
          Do this before going to routeable to ensure we're ready to receive
          data, ie. data path is complete comfigured ahead of time.
          // todo:        This also ensures any outstandign data is freed.
        ---------------------------------------------------------------------*/
        rmnet_sio_set_rx_cb( info->meta_sm.rmnet_sio_handle,
                             rmnet_meta_smi_rm_rx_non_empty_func,
                             info );

        /*---------------------------------------------------------------------
          Enable the flow.
        ---------------------------------------------------------------------*/
        rmnet_sio_set_rx_flow( info->meta_sm.rmnet_sio_handle,
                               RMNET_SIO_FLOWMASK_ALL,
                               FALSE );

        /*---------------------------------------------------------------------
          Enable the Tx flow.
        ---------------------------------------------------------------------*/
        ps_iface_enable_flow(info->rmnet_iface_ptr, DS_FLOW_RMSM_MASK);

        /*---------------------------------------------------------------------
          Register Non-empty CB fn for legacy Wmk only when first SM is up.
        ---------------------------------------------------------------------*/
        rmnet_smi_dual_ip_info[info->constants.qmi_instance].legacy_wmk.
        non_empty_func_ptr = rmnet_meta_smi_legacy_wmk_non_empty_cb_func;

        rmnet_smi_dual_ip_info[info->constants.qmi_instance].legacy_wmk.
        non_empty_func_data =
          (void *) &rmnet_smi_dual_ip_info[info->constants.qmi_instance];
      }
#ifdef FEATURE_DATA_MBIM
      /*---------------------------------------------------------------------
        Assign new Rm Signals during Rm Configuring in below cases -
        1. For all non-MBIM mode calls (Std IP mode calls)
        2. In MBIM mode and if a Signal is not already assigned as a part of
           first call. If its already assigned we can skip the signal
           assignment as the same Signal will be used for all MBIM calls.
        ---------------------------------------------------------------------*/

      if ((info->meta_sm.data_agg_protocol.ul_data_agg_protocol !=
            RMNET_ENABLE_DATA_AGG_MBIM)   ||
          ((info->meta_sm.data_agg_protocol.ul_data_agg_protocol ==
            RMNET_ENABLE_DATA_AGG_MBIM)     &&
           (info->meta_sm.data_format == 0) &&
           (mbim_call_info.mbim_rx_sig == RMNET_MAX_SIGNALS)
          )
         )
      {
#endif/*FEATURE_DATA_MBIM*/
      // assign rx signal
        if( RMNET_MAX_SIGNALS == rmnet_smi_dual_ip_info[info->constants.qmi_instance].rx_sig)
        {
          for( i = 0; i < RMNET_IFACE_MAX; i++ )
          {
            if(FALSE == rmnet_signals[i].in_use)
            {
              rmnet_signals[i].in_use = TRUE;
              rmnet_smi_dual_ip_info[info->constants.qmi_instance].rx_sig = rmnet_signals[i].sig;
              break;
            }
          }
        }

        info->constants.rx_sig = rmnet_smi_dual_ip_info[info->constants.qmi_instance].rx_sig;
#ifdef FEATURE_DATA_MBIM
      }
#endif/*FEATURE_DATA_MBIM*/

     /*---------------------------------------------------------------------------
       Assign RmNet legacy signal and set legacy sig handler for the instance.
     ---------------------------------------------------------------------------*/
      if( RMNET_LEGACY_MAX_SIGNAL ==
            rmnet_smi_dual_ip_info[info->constants.qmi_instance].legacy_sig)
      {
        for( i = 0; i < RMNET_IFACE_MAX; i++ )
        {
          if(FALSE == rmnet_legacy_signals[i].in_use)
          {
            rmnet_legacy_signals[i].in_use = TRUE;
            rmnet_smi_dual_ip_info[info->constants.qmi_instance].legacy_sig =
              rmnet_legacy_signals[i].sig;
            break;
          }
        }
      }
#ifdef FEATURE_DATA_MBIM
      /*---------------------------------------------------------------------
        MBIM UL data path configuration
      ---------------------------------------------------------------------*/
      if ((info->meta_sm.data_agg_protocol.ul_data_agg_protocol ==
            RMNET_ENABLE_DATA_AGG_MBIM) && (info->meta_sm.data_format == 0)
         )
      {
        if(-1 == rmnet_meta_smi_config_rx_mbim_data_path(info))
        {
          LOG_MSG_ERROR_0("config_rx_mbim_data_path failed" );
          rmnet_meta_sm_post_event( RMNET_META_SM_INFO_TO_INSTANCE(info),
                                    RMNET_META_SM_RM_LINK_DOWN_EV );
        }
      }
#endif/*FEATURE_DATA_MBIM*/
      /*---------------------------------------------------------------------
        Set the appropriate Rx handlers according to data call.
      ---------------------------------------------------------------------*/
      if(-1 == rmnet_meta_smi_set_rx_sig_handler(info))
      {
        LOG_MSG_ERROR_0("Rm bringup failed, posting Rm LINK_DOWN" );
        rmnet_meta_sm_post_event( RMNET_META_SM_INFO_TO_INSTANCE(info),
                                  RMNET_META_SM_RM_LINK_DOWN_EV );
      }
      else
      {
        // set signal to get rid of race condition where watermark is not empty
        // from previous call and flow enable events occured before we
        // configured this signal
        RMNET_SET_SIGNAL( info->constants.rx_sig );
        RMNET_SET_LEGACY_SIGNAL(rmnet_smi_dual_ip_info[info->constants.qmi_instance].legacy_sig);
        #ifdef FEATURE_DATA_RMNET_USB_STANDARD_ECM
        LOG_MSG_INFO2_0("Sending Link UP notification to ECM driver");
        rmnet_sio_link_up_notify(info->meta_sm.rmnet_sio_handle,
                                 TRUE); //link_UP
        #endif
      }
      break;
    }

    /*= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
                        RMNET_META_SM_NET_IF_UP_STATE
    = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =*/
    case RMNET_META_SM_NET_IF_UP_STATE:
    {
      /*---------------------------------------------------------------------
        This is the common state for data connection up.  TE has configured
        its IP address at this point
      ---------------------------------------------------------------------*/
      LOG_MSG_INFO2_0 ("Host has configured IP address");

      /*---------------------------------------------------------------------
        Bridge Um interface
      ---------------------------------------------------------------------*/
      if (!info->meta_sm.softap_mode)
      {
        ps_iface_set_bridge(info->um_iface_ptr, info->rmnet_iface_ptr);
      }

      /*---------------------------------------------------------------------
        Call came up successfully, reset the back off timer to initial value
      ---------------------------------------------------------------------*/
      info->meta_sm.autoconnect_next_backoff_time = AUTOCONNECT_BACKOFF_TIMER_MIN_VALUE;

      /*---------------------------------------------------------------------
        IP Interface is now available, so put it in routeable state which
        indicates to call is connected.  QMI WDS listens and generates
        appropriate indication messages to the RMNET miniport indicating that
        the call is now connected.

        TODO: Check if phys link should be UP at configuring time. We can
              try moving this to RM_CONFIGURING.
      ---------------------------------------------------------------------*/
      ps_iface_routeable_ind(info->rmnet_iface_ptr);

#ifdef FEATURE_QXDM_WM_TRACING
      /*---------------------------------------------------------------------
        Register with FC for WMk reporting.
      ---------------------------------------------------------------------*/
      if(0 != ps_phys_link_ioctl(PS_IFACE_GET_PHYS_LINK(info->um_iface_ptr),
                                 PS_PHYS_LINK_IOCTL_GET_CALL_ID,
                                 (void*)&call_id,
                                 &ioctl_errno))
      {
        LOG_MSG_ERROR_1("Cannot get the Call ID. Error %d", ioctl_errno);
      }
      else
      {
        info->call_id = call_id;
        fc_post_reg_cmd_wm_trace(UNKNOWN_TECH,
                                 call_id,
                                 RMNET_CLIENT,
                                 rmnet_meta_smi_fc_get_wm_trace_cb,
                                 (uint32)info);
      }
#endif /* FEATURE_QXDM_WM_TRACING */

      if (!info->meta_sm.softap_mode)
      {
        ps_iface_type *base_um_iface_ptr;

        PS_ENTER_CRIT_SECTION(&global_ps_crit_section);
        base_um_iface_ptr = PS_IFACE_GET_BASE_IFACE(info->um_iface_ptr);
        PS_IFACEI_SET_OPT_RM_IFACE(base_um_iface_ptr, info->rmnet_iface_ptr);
        PS_LEAVE_CRIT_SECTION(&global_ps_crit_section);
        LOG_MSG_INFO1_2("RmNet UP State: Set DL opt path for iface0x%x:%d",
                        base_um_iface_ptr->name, base_um_iface_ptr->instance);
      }

      break;
    }

    /*= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
                        RMNET_META_SM_RM_RECONFIGURING_STATE
    = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =*/
    case RMNET_META_SM_RM_RECONFIGURING_STATE:
    {
      /*---------------------------------------------------------------------
        Unbridge Rm and Um interfaces
      ---------------------------------------------------------------------*/
      ps_iface_set_bridge( info->um_iface_ptr, NULL );

      break;
    }

    /*= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
                        RMNET_META_SM_WAITING_FOR_UM_DOWN_STATE
    = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =*/
    case RMNET_META_SM_WAITING_FOR_UM_DOWN_STATE:
    {

      /* If in configuring state, couldnt get any valid IFACE and to avoid
               the race condition, of processing Rm Link Down before IFACE DOWN.
               Ignore, processing of this if the Um Iface Pointer is NULL  There should be an Iface
               down event following this. As Iface Pointer is invalid, nothing can be done in this state. */
      if (!PS_IFACE_IS_VALID(info->um_iface_ptr))
      {
        LOG_MSG_ERROR_1( "Ignoring processing of state [%d] when Iface is invalid",
                         RMNET_META_SM_WAITING_FOR_UM_DOWN_STATE );
        return;
      }
      /*---------------------------------------------------------------------
        If Um was UP before RmNet started using it, restore the original
        state.  If the other user of the interface has already relinquished
        the Um, the teardown below will put Um interface to DOWN state.
      ---------------------------------------------------------------------*/
      if ( TRUE == info->meta_sm.restore_um_up )
      {
        ps_iface_up_ind (info->um_iface_ptr);
      }

      /* Reset DL opt cfg if any */
      if (PS_IFACE_IS_VALID(info->um_iface_ptr))
      {
        PS_ENTER_CRIT_SECTION(&global_ps_crit_section);
        base_um_iface_ptr = PS_IFACE_GET_BASE_IFACE(info->um_iface_ptr);
        PS_IFACEI_SET_OPT_RM_IFACE(base_um_iface_ptr, NULL);
        PS_LEAVE_CRIT_SECTION(&global_ps_crit_section);
        LOG_MSG_INFO1_2("RmNet waiting for Um Down State: "
                        "Reset DL opt path for iface0x%x:%d",
                        info->um_iface_ptr->name,
                        info->um_iface_ptr->instance);

      }

      /*---------------------------------------------------------------------
        Since TE has terminated the call, we need to cleanup Um before
        completely terminating the call.  Tear it down now.
        If tear down cmd fails, Um is already be down. Passing NULL policy
        here, since we don't cache it.
      ---------------------------------------------------------------------*/
      retval = ps_route_tear_down_iface_by_policy(info->um_iface_ptr,
                                                  NULL,
                                                  &ps_iface_errno,
                                                  NULL);
      LOG_MSG_INFO1_1( "PS route teardown errno [%d]", ps_iface_errno );


      /*---------------------------------------------------------------------
        Check if tear_down_cmd returned success (0) or if no tear_down_hdlr
        registered. In either case, trigger rm bring down
      ---------------------------------------------------------------------*/
      if((retval == 0) || (ps_iface_errno == DS_EOPNOTSUPP))
      {
        /*-------------------------------------------------------------------
          If the Um iface is already down we should cleanup the call
          on Rm since we won't get any other notification from Um
          controller
        -------------------------------------------------------------------*/

        LOG_MSG_INFO1_0( "tear down cmd returns success, posting RMNET_META_SM_UM_IFACE_DOWN_EV" );

        info->meta_sm.call_end_reason = PS_NET_DOWN_REASON_CLIENT_END;

        /* clean up v4/v6 sub state machine */
        rmnet_meta_smi_stop_addr_cfg_sm(info);

        rmnet_meta_sm_post_event( RMNET_META_SM_INFO_TO_INSTANCE(info),
                                  RMNET_META_SM_UM_IFACE_DOWN_EV );
      }
      break;
    }

     default:
    {
      LOG_MSG_ERROR_1("Invalid state %d", new_state);
      ASSERT(0);
    }
  } /* switch(new state) */
} /* rmnet_meta_smi_transition_state() */

/*===========================================================================
  FUNCTION RMNET_META_SMI_PROCESS_DTR_SIG_CB()

  DESCRIPTION
    This function processes the change in dtr state.  If DTR is deasserted
    appropriate event is posted to rmnet_meta_sm which initiates the call
    termination.

    DTR assert is ignored for now.

  PARAMETERS
    is_asserted: flag which indicates current DTR state -
                 TRUE: DTR asserted, FALSE: DTR deasserted

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void rmnet_meta_smi_dtr_change_cb
(
  void *  user_data,
  boolean is_asserted
)
{
  rmnet_smi_cmd_type   * cmd_ptr;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------------
    Get a DCC command buffer
  -------------------------------------------------------------------------*/
  cmd_ptr = (rmnet_smi_cmd_type *)RMNET_SMI_GET_CMD_BUF(DCC_RMNET_SM_CMD,FALSE);
  if (cmd_ptr == NULL)
  {
    return;
  }

  /*-------------------------------------------------------------------------
    Post cmd to DS task corresponding to each event
  -------------------------------------------------------------------------*/
  cmd_ptr->sm_id     = RMNET_META_SM;
  cmd_ptr->cmd_type  = RMNET_SM_CMD_DTR_CHANGED;
  cmd_ptr->info_ptr  = user_data;
  cmd_ptr->data.dtr_changed.is_asserted  = is_asserted;
  dcc_send_cmd_ex(DCC_RMNET_SM_CMD, (void *)cmd_ptr);

} /* rmnet_meta_smi_dtr_change_cb() */

/*===========================================================================
  FUNCTION RMNET_META_SM_VERIFY_DTR_HIGH_PER_INSTANCE()

  DESCRIPTION
    To check if DTR is high or low based on the instance ID.

  PARAMETERS
    instannce: instance of rmnet_meta_smi

  RETURN VALUE
    boolean : TRUE  = DTR is high
              FALSE = DTR is low

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean rmnet_meta_sm_verify_dtr_high_per_instance
(
  rmnet_instance_e_type   instance
)
{
  rmnet_smi_info_type   *info = NULL;

  if ( ( instance < RMNET_INSTANCE_MIN ) || ( instance >= RMNET_INSTANCE_MAX ))
  {
    LOG_MSG_ERROR_1("Invalid Instance %d received", instance);
    return FALSE;
  }
  info = &rmnet_smi_info[instance];

#ifdef FEATURE_DATA_QMI_DTR_ON_CTRL_CHANNEL
  return qmux_verify_dtr_high(info->constants.qmi_instance);
#else
  return rmnet_sio_verify_dtr_high(info->meta_sm.rmnet_sio_handle);
#endif /* FEATURE_DATA_QMI_DTR_ON_CTRL_CHANNEL */

} /* rmnet_meta_sm_verify_dtr_high_per_instance() */

/*===========================================================================
  FUNCTION RMNET_META_SMI_VERIFY_DTR_HIGH()

  DESCRIPTION
    To check if DTR is high or low.

  PARAMETERS
    info: ptr to rmnet_smi_info

  RETURN VALUE
    boolean : TRUE  = DTR is high
              FALSE = DTR is low

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static boolean rmnet_meta_smi_verify_dtr_high
(
  rmnet_smi_info_type   *info
)
{
  ASSERT(info);

#ifdef FEATURE_DATA_QMI_DTR_ON_CTRL_CHANNEL
  return qmux_verify_dtr_high(info->constants.qmi_instance);
#else
  return rmnet_sio_verify_dtr_high(info->meta_sm.rmnet_sio_handle);
#endif /* FEATURE_DATA_QMI_DTR_ON_CTRL_CHANNEL */

} /* rmnet_meta_smi_verify_dtr_high() */


/*===========================================================================
  FUNCTION RMNET_META_SM_PROCESS_DTR_CHANGED_CMD()

  DESCRIPTION
    This function processes a RmNet RmSM DTR event cmd.

  PARAMETERS
    cmd_ptr:  serving task command buffer containing the RmNet sm
              event information

  RETURN VALUE
    None

  DEPENDENCIES
    RM State Machine should have already been initialized

  SIDE EFFECTS
    None
===========================================================================*/
static void rmnet_meta_smi_process_dtr_changed_cmd
(
  dcc_cmd_enum_type cmd,
  void              *user_data_ptr
)
{
  rmnet_smi_info_type  * info;
  rmnet_instance_e_type  rmnet_instance;
  rmnet_smi_cmd_type   * cmd_ptr;
  rmnet_meta_sm_data_format_type data_format_var;
  uint32                  data_format_mask = 0;
#ifndef FEATURE_DATA_QMI_DTR_ON_CTRL_CHANNEL
  qmi_cmd_data_buf_type * qmi_cmd_ptr = NULL;
#endif
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  cmd_ptr = ( rmnet_smi_cmd_type * )user_data_ptr;
  ASSERT (cmd_ptr && (cmd == DCC_RMNET_SM_CMD) && 
          (cmd_ptr->cmd_type == RMNET_SM_CMD_DTR_CHANGED) &&
          (cmd_ptr->sm_id    == RMNET_META_SM));

  info = cmd_ptr->info_ptr;
  ASSERT (info);

  rmnet_instance = RMNET_META_SM_INFO_TO_INSTANCE(info);
  ASSERT(rmnet_instance < RMNET_INSTANCE_MAX);

  /* Based on EFS configuration, trigger the appropraite call either
     IPV4, IPV6 or both */
  LOG_MSG_INFO1_2 ("DTR change for RMNET inst [%d] DTR [%d]",
                   rmnet_instance, cmd_ptr->data.dtr_changed.is_asserted);

  if (RMNET_IS_VALID_LAPTOP_INSTANCE(rmnet_instance))
  {
    if(info == rmnet_smi_dual_ip_info[info->constants.qmi_instance].info_v4 )
    {
      if ( rmnet_efs_config_params[RMNET_GET_LAPTOP_INSTANCE(rmnet_instance)].\
                                                       ip_type == QMI_AF_INET6 )
      {
        LOG_MSG_INFO1_0("EFS config: IPV6 ONLY; exiting from V4 instance");
        return;
      }
    }
    else if(info == rmnet_smi_dual_ip_info[info->constants.qmi_instance].info_v6 )
    {
      if ( rmnet_efs_config_params[RMNET_GET_LAPTOP_INSTANCE(rmnet_instance)].\
                                                        ip_type == QMI_AF_INET )
      {
        LOG_MSG_INFO1_0("EFS config: IPV4 ONLY; exiting from V6 instance");
        return;
      }
    }
  }
  /*-------------------------------------------------------------------------
    Handle DTR state change.

    If DTR drops, end the call by posting RMNET_META_SM_LINK_DOWN_EV, and tell
    the QMI module that the link is down.

    If DTR goes high, check if autoconnect is enabled.  If so, post
    RM_DEV_START_PKT_EV to start a new call.
  -------------------------------------------------------------------------*/
  if ( FALSE == cmd_ptr->data.dtr_changed.is_asserted )
  {
    if(info->meta_sm.dtr_is_high == FALSE)
    {
      LOG_MSG_INFO1_1("Ignoring DTR low, RMNET inst %d", rmnet_instance);
      return;
    }

    LOG_MSG_INFO1_1( "DTR dropped - end packet call, RMNET inst %d",
                     rmnet_instance );
    info->meta_sm.dtr_is_high = FALSE;

    /*-----------------------------------------------------------------------
      Reset data format if call is not UP. Otherwise defer it to NULL state
    -----------------------------------------------------------------------*/
    if (info->meta_sm.state == RMNET_META_SM_NULL_STATE)
    {
      rmnet_meta_smi_set_default_data_format(rmnet_instance);
    }
    else
    {
      info->meta_sm.reset_data_format = TRUE;
    }

    rmnet_meta_sm_post_event(rmnet_instance, RMNET_META_SM_RM_LINK_DOWN_EV);

    /*-----------------------------------------------------------------------
      Rearm autoconnect when cable is disconnected
      TODO - merge Keyur's fix for disabling device
    -----------------------------------------------------------------------*/
    if (info->meta_sm.settings.auto_connect_enabled)
    {
      LOG_MSG_INFO1_1( "rearm autoconnect trigger, RMNET inst %d",
                       rmnet_instance );
      info->meta_sm.auto_connect_trigger = TRUE;
      /*---------------------------------------------------------------------
        Resetting the backoff time to MIN as dtr dropped
      ---------------------------------------------------------------------*/
      info->meta_sm.autoconnect_next_backoff_time =
        AUTOCONNECT_BACKOFF_TIMER_MIN_VALUE;
    }

#ifndef FEATURE_DATA_QMI_DTR_ON_CTRL_CHANNEL
    // TODO: Move below to QMUX, have netsio provide API for QMUX to
    // simultaneously reg a dtr event handler
    /*-----------------------------------------------------------------------
      Control path cleanup too when data link goes away
    -----------------------------------------------------------------------*/
    qmi_cmd_ptr = qmi_get_cmd_data_buf();
    if(qmi_cmd_ptr == NULL)
    {
      LOG_MSG_ERROR_0("Could not get cmd buffer from QMI task");
      return;
    }
    qmi_cmd_ptr->cmd.qmi.id = QMI_CMD_DTR_STATUS;
    qmi_cmd_ptr->cmd.qmi.data.dtr_status.qmi_inst = info->constants.qmi_instance;
    qmi_cmd_ptr->cmd.qmi.data.dtr_status.asserted = FALSE;
    qmi_send_cmd(QMI_CMD, qmi_cmd_ptr);
#endif /* FEATURE_DATA_QMI_DTR_ON_CTRL_CHANNEL */
  }
  else
  {
    rmnet_set_data_fmt_ret_e_type ret_type = RMNET_SET_DATA_FORMAT_FAILURE;
    if(info->meta_sm.dtr_is_high == TRUE)
    {
      LOG_MSG_INFO1_0("Ignoring DTR High");
      return;
    }
 /* The net_params values could be stale/incorrect due to a
       race between DCC(rmnet_smi_init) and RDM(rdm_open).
       So set the net_params to sane defaults during DTR high for good measure.*/
    if (RMNET_IS_VALID_LAPTOP_INSTANCE(rmnet_instance))
    {
      /* Set the default IP family pref as per EFS configuration */
      if ( rmnet_efs_config_params[RMNET_GET_LAPTOP_INSTANCE(rmnet_instance)].\
                                                 ip_type != QMI_AF_INET_4_6 )
      {
        /* Set the default IP family pref as per EFS configuration */
        info->meta_sm.net_params.ip_family_pref = (uint8)
           rmnet_efs_config_params[RMNET_GET_LAPTOP_INSTANCE(rmnet_instance)].\
                                                                     ip_type;
      }
      else
      {
        if(info == rmnet_smi_dual_ip_info[info->constants.qmi_instance].info_v4)
        {
          info->meta_sm.net_params.ip_family_pref = QMI_AF_INET;
        }
        else if(info == rmnet_smi_dual_ip_info\
                                    [info->constants.qmi_instance].info_v6 )
        {
          info->meta_sm.net_params.ip_family_pref = QMI_AF_INET6;
        }
      }
    }
    /* Reset data_format to default value from EFS, to handle case of driver
      change without phone reset. New driver will set the data format to
      1 on enumeration
    -----------------------------------------------------------------------*/
    memset(&data_format_var, 0, sizeof(data_format_var));

    if (RMNET_IS_VALID_LAPTOP_INSTANCE(rmnet_instance))
    {
      data_format_var.qos =
        rmnet_efs_config_params[RMNET_GET_LAPTOP_INSTANCE(rmnet_instance)].\
                                                                qos_enabled;
      data_format_var.link_prot = (uint16)
        rmnet_efs_config_params[RMNET_GET_LAPTOP_INSTANCE(rmnet_instance)].\
                                                                link_prot;
      data_format_var.data_agg_protocol.ul_data_agg_protocol =
        rmnet_efs_config_params[RMNET_GET_LAPTOP_INSTANCE(rmnet_instance)].\
                                                               ul_tlp_enabled;

      data_format_var.data_agg_protocol.dl_data_agg_protocol =
        rmnet_efs_config_params[RMNET_GET_LAPTOP_INSTANCE(rmnet_instance)].\
                                                          dl_data_agg_protocol;
    }
    if( RMNET_INVALID_MODE == data_format_var.link_prot)
    {
      data_format_var.link_prot = RMNET_ETHERNET_MODE;
    }

    data_format_mask = 0x000F;
    ret_type = rmnet_meta_sm_set_data_format(  rmnet_instance,
                                               &data_format_var,
                                               data_format_mask,
                                               RMNET_SET_DATA_FORMAT_DTR_HGH);
    if ( ret_type == RMNET_SET_DATA_FORMAT_FAILURE )
    {
      LOG_MSG_ERROR_3("Malformed Data Format settings in EFS(%d;%d;%d)!",
                      data_format_var.qos,
                      data_format_var.link_prot,
                      data_format_var.data_agg_protocol.ul_data_agg_protocol);
      // this will happen if efs has malformed data format info
      // at this stage some data format settings could have
      // been partially modified so we need to reset them with
      // default values
      memset(&data_format_var, 0, sizeof(data_format_var));
      data_format_var.link_prot = RMNET_ETHERNET_MODE;
      ret_type = rmnet_meta_sm_set_data_format(rmnet_instance,
                                                 &data_format_var,
                                                 data_format_mask,
                                                 RMNET_SET_DATA_FORMAT_DTR_HGH);
      data_format_var.link_prot = RMNET_ETHERNET_MODE;
      if( ret_type == RMNET_SET_DATA_FORMAT_FAILURE )
      {
        LOG_MSG_ERROR_0("Cannot set data format!");
      }
    }
    info->meta_sm.dtr_is_high = TRUE;
    if (info->meta_sm.auto_connect_trigger)
    {
      LOG_MSG_INFO1_1 ("DTR went high - start packet call, RMNET inst %d",
                       rmnet_instance );
      rmnet_meta_sm_post_event(rmnet_instance,
                               RMNET_META_SM_RM_DEV_START_PKT_EV);
      info->meta_sm.auto_connect_trigger = FALSE;
    }
  }
} /* rmnet_meta_smi_dtr_change_cb() */


/*===========================================================================
  FUNCTION RMNET_META_SMI_SIO_RX_NON_EMPTY_FUNC()

  DESCRIPTION
    This function performs the 'Non Empty' event processing for the PACKET
    DATA SERVICES shared queue. This function is called when the SIO RX
    data stream watermark item gets its first item enqueued. It sets its
    idle bit which allows Um_PKT_CTRL to end cta dormancy if needed.

  PARAMETERS
    None

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void rmnet_meta_smi_rm_rx_non_empty_func
(
  struct dsm_watermark_type_s *wm_ptr,
  void *user_data_ptr
)
{
  rmnet_smi_info_type *  info;
  int flow_cnt = 0;
  rmnet_instance_e_type  rmnet_inst;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  info = (rmnet_smi_info_type *) user_data_ptr;
  rmnet_inst = RMNET_META_SM_INFO_TO_INSTANCE(info);

  LOG_MSG_INFO3_1 ("Rx wm non-empty!, RMNET inst %d", rmnet_inst);

  // TODO: evaluate a race condition:
  //  - 1st call is up
  //  - 2nd call is being brough up, um_iface_ptr is valid but flow is not enabled
  //  - 1st call will experience data stall until flow is enabled on 2nd call
  if( rmnet_smi_dual_ip_info[info->constants.qmi_instance].info_v4->um_iface_ptr )
    flow_cnt += PS_IFACE_FLOW_ENABLED( rmnet_smi_dual_ip_info[info->constants.qmi_instance].info_v4->um_iface_ptr);

  if( rmnet_smi_dual_ip_info[info->constants.qmi_instance].info_v6->um_iface_ptr )
    flow_cnt += PS_IFACE_FLOW_ENABLED( rmnet_smi_dual_ip_info[info->constants.qmi_instance].info_v6->um_iface_ptr);

  /*-------------------------------------------------------------------------
    Send signal to PS to start pulling stuff off the SIO.
  -------------------------------------------------------------------------*/
  if( flow_cnt > 0 )
  {
    LOG_MSG_INFO3_2("rmnet_meta_smi_rm_rx_non_empty_func - set PS signal:0x%x, "
                    "RMNET inst %d",
                    rmnet_smi_dual_ip_info[info->constants.qmi_instance].rx_sig,
                    rmnet_inst);
    RMNET_SET_SIGNAL( rmnet_smi_dual_ip_info[info->constants.qmi_instance].rx_sig );
  }
  else
  {
    LOG_MSG_INFO2_1 ("Um flow controlled!, rmnet_inst %d", rmnet_inst);
  }
} /* rmnet_meta_smi_rm_rx_non_empty_func() */


/*===========================================================================
FUNCTION RMNET_META_SMI_LEGACY_SIG_HDLR

DESCRIPTION
  This function processes packets/frames that need to take legacy path.

PARAMETERS
  rx_sig    -  RmNet Legacy sig
  user_data -  user data contains rmnet_smi_dual_ip_info_type

RETURN VALUE
  None.

DEPENDENCIES
  None

SIDE EFFECTS
  None.

===========================================================================*/
static boolean rmnet_meta_smi_legacy_sig_hdlr
(
  rmnet_legacy_sig_enum_type rx_sig,
  void*            user_data
)
{
  rmnet_smi_dual_ip_info_type *  info;
  rmnet_meta_smi_qos_hdr_type    qos_hdr;
  ps_meta_info_type_ex        *  meta_info_ex_ptr = NULL;
  dsm_item_type               *  rx_pkt;
  ps_iface_type               *  rm_iface_ptr = NULL;

  ip_version_enum_type     ip_ver;
  uint8                    octet_val;
  uint16                   ethertype;
  uint32                   len = 0;
  boolean                  enable      = FALSE;
  sint15                   ps_errno;
  uint8    pkt_buf[PS_OPT_PATH_ETH_QOS_HEADER_LEN  +  1] ALIGN(4); 
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
 LOG_MSG_INFO4_0("In rmnet_meta_smi_legacy_sig_hdlr");
 PS_ENTER_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);

  ASSERT(user_data);

  memset((void*)&qos_hdr, 0, sizeof(rmnet_meta_smi_qos_hdr_type));

  info = (rmnet_smi_dual_ip_info_type  *) user_data;

  if(rx_sig != info->legacy_sig)
  {
    LOG_MSG_ERROR_2 ("RX signal mismatch: expected=%d recieved=%d",
                     info->rx_sig, rx_sig );
    PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
    return TRUE;
  }

  /*---------------------------------------------
   Get next packet via the registered rx function
  -----------------------------------------------*/
  if ( (RMNET_META_SM_NULL_STATE == info->info_v4->meta_sm.state)
       && (RMNET_META_SM_NULL_STATE == info->info_v6->meta_sm.state) )
  {
    LOG_MSG_ERROR_0 ("Both state machines are in RMNET_META_SM_NULL_STATE" );
    PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
    return TRUE;
  }

  /*-----------------------------------------------------------------------
   Dequeue packet from RmNet Legacy Wmk.
  ------------------------------------------------------------------------*/
  if ( (rx_pkt = rmnet_meta_smi_rm_rx_data(info, &qos_hdr)) != NULL)
  {

    if(RMNET_IP_MODE == info->info_v4->link_prot)
    {

      /*---------------------------------------------------------------------
        Get IP version of packet
      ---------------------------------------------------------------------*/
      if (sizeof(uint8) != dsm_extract(rx_pkt, 0, &octet_val, sizeof(uint8)))
      {
        LOG_MSG_INFO2_0("unable to get IP version");
        PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
        dsm_free_packet(&rx_pkt);
        return TRUE;
      }

      ip_ver = (ip_version_enum_type) (octet_val >> 4);

      if (IP_V4 == ip_ver)
      {
        /*---------------------------------------------------------------------
          Ensure that IPv4 call is UP
        ---------------------------------------------------------------------*/
        if (RMNET_META_SM_NULL_STATE != info->info_v4->meta_sm.state &&
            PS_IFACE_IS_VALID(info->info_v4->um_iface_ptr))
        {
          rmnet_meta_smi_process_qos_hdr((void* )info->info_v4,
                                         rx_pkt,
                                         &meta_info_ex_ptr,
                                         &qos_hdr);

          /*-------------------------------------------------------------------
            Execute powersave filters on incoming packet in v4 path. 
          -------------------------------------------------------------------*/
          rm_iface_ptr = info->info_v4->rmnet_iface_ptr;
          if( TRUE == PS_IFACE_GET_POWERSAVE_FILTERING_MODE(rm_iface_ptr) )
          {
            if( PS_IFACE_GET_POWERSAVE_FILTERING_AUTO_EXIT_MODE(rm_iface_ptr) )
            {
              ps_iface_set_powersave_filtering_mode(rm_iface_ptr, enable, 
                                                    &ps_errno);
            }
          }
          
          PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);

         /*--------------------------------------------------------------------
           Extract bytes from rx_pkt to check whether they are low latency 
           packets (ICMP ECHO REQ/RESP). Since the QoS header(if present ) 
           is already pulled off and the data format is IP mode we only need to
           extract IPv4 header length + 2 bytes (for ICMP type and code).

           If there aren't enough bytes to extract then the packet doesn't 
           belong to low latency traffic and low latency traffic handler is not
           called
         -------------------------------------------------------------------*/
          if( PS_UL_OPT_V4_IP_ICMP_LEN_CHECK ==  
                dsm_extract(rx_pkt,
                           0,
                           pkt_buf,
                           PS_OPT_PATH_V4_HDR_LEN + PS_ICMP_HDR_LEN_EXTRACT ) ) 
          {
            ps_ul_optimized_v4_low_latency_traffic_handler( pkt_buf,
                                            PS_OPT_PATH_V4_HDR_LEN,
                                            PS_OPT_PATH_V4_PROT_FIELD_OFFSET );
          }

          (void) ps_iface_input(rm_iface_ptr,
                                &rx_pkt,
                                meta_info_ex_ptr);
           return FALSE;
        }
        else
        {
          LOG_MSG_ERROR_0("Call not up! Dropping packet!");
          dsm_free_packet(&rx_pkt);
        }
      }
        else if (IP_V6 == ip_ver)
        {
        /*---------------------------------------------------------------------
          Ensure that IPv6 call is UP
        ---------------------------------------------------------------------*/
        if (RMNET_META_SM_NULL_STATE != info->info_v6->meta_sm.state &&
            PS_IFACE_IS_VALID(info->info_v6->um_iface_ptr))
        {
          rmnet_meta_smi_process_qos_hdr((void* )info->info_v6,
                                         rx_pkt,
                                         &meta_info_ex_ptr,
                                         &qos_hdr);

          /*-------------------------------------------------------------------
            Execute powersave filters on incoming packet in v6 path. 
          -------------------------------------------------------------------*/
          rm_iface_ptr = info->info_v6->rmnet_iface_ptr;
          
          if( TRUE == PS_IFACE_GET_POWERSAVE_FILTERING_MODE(rm_iface_ptr) )
          {
            if( PS_IFACE_GET_POWERSAVE_FILTERING_AUTO_EXIT_MODE(rm_iface_ptr) )
            {
              ps_iface_set_powersave_filtering_mode( rm_iface_ptr, enable, 
                                                     &ps_errno);
            }
          }
          
          PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);

          /*--------------------------------------------------------------------
           Extract bytes from rx_pkt to check whether it is a low latency
           packet (ICMP6 ECHO REQ/RESP). Since the QoS header(if present ) 
           is already pulled off and the data format is IP mode we only need to
           extract IPv6 header length + 2 bytes (for ICMP6 type and code).
           If the packet is fragmented then we need to check whether it is 
           the first fragment of the packet. i.e. next_hdr field in IPv6 header
           is fragment header() and fragment offset is 0 in fragment header.
           So we extract IPv6 header length + 4 bytes 

           If there aren't enough bytes to extract then the packet doesn't 
           belong to low latency traffic and low latency traffic handler is not
           called
          -------------------------------------------------------------------*/
          if( PS_UL_OPT_V6_IP_ICMP_LEN_CHECK == 
                dsm_extract(rx_pkt,
                            0,
                            pkt_buf,
                            PS_OPT_PATH_V6_HDR_LEN + 
                            PS_OPT_PATH_ICMP6_HDR_LEN_EXTRACT ) )
          {
            rmnet_meta_smi_v6_low_latency_traffic_handler(
                                                pkt_buf,
                                                PS_OPT_PATH_V6_HDR_LEN,
                                                PS_OPT_PATH_V6_NXT_HDR_OFFSET,
                                                rx_pkt );
          }

          (void) ps_iface_input(rm_iface_ptr,
                                &rx_pkt,
                                meta_info_ex_ptr);
           return FALSE;
        }
        else
        {
          LOG_MSG_ERROR_0("Call not up! Dropping packet!");
          dsm_free_packet(&rx_pkt);
        }
      }
      else
      {
        LOG_MSG_ERROR_1("Invalid IP version %d, dropping packet!", ip_ver);
        dsm_free_packet(&rx_pkt);
      }

      /*-------------------------------------------------------------------
       Input to PS_IFACE.

       //MCT Should IP type and Iface type be enforced here, or let the stack
        handle it? Otherwise we'll have to parse the IP header, etc.
      -------------------------------------------------------------------*/

    }
    else // ethernet mode, for now assuming LLC mode 0: Ethertye | Frame Body
    {
      (void)dsm_extract(rx_pkt, LAN_IEEE_MAC_ADDR_LEN*2, &ethertype, 2); // offset = src+dst addr len

      if (!qmi_svc_dsm_pushdown_packed( &rx_pkt,
                             &qos_hdr,
                             sizeof(qos_hdr),
                             DSM_DS_SMALL_ITEM_POOL,
                             FILE_ID_DS_RMNET_META_SM,__LINE__ ))
      {
        PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
        dsm_free_packet(&rx_pkt);
        return FALSE;
      }

      switch (ps_ntohs(ethertype))
      {
        case LAN_ETHERTYPE_IPV4:
        {
          /*---------------------------------------------------------------------
            This is an IPv4 frame
          ---------------------------------------------------------------------*/
            rm_iface_ptr = info->info_v4->rmnet_iface_ptr;
            if( TRUE == PS_IFACE_GET_POWERSAVE_FILTERING_MODE(rm_iface_ptr) )
            {
              if( PS_IFACE_GET_POWERSAVE_FILTERING_AUTO_EXIT_MODE(rm_iface_ptr) )
              {
                ps_iface_set_powersave_filtering_mode( rm_iface_ptr, enable, 
                                                       &ps_errno);
              }
            }
            
           /*--------------------------------------------------------------------
           Extract bytes from rx_pkt to check whether it is low latency packet
           (ICMP ECHO REQ/RESP). Since the QoS header is present and the data 
           format is ETHERNET we need to extract QoS Header length + 
           Ethernet Header length + IPv4 header length + 2 bytes (for ICMP type
           and code).

           If there aren't enough bytes to extract then the packet doesn't 
           belong to low latency traffic and low latency traffic handler is not
           called
          -------------------------------------------------------------------*/
            if( PS_UL_OPT_QOS_ETH_V4_ICMP_LEN_CHECK == 
                        dsm_extract(rx_pkt,
                          0,
                          pkt_buf,
                          PS_OPT_PATH_V4_HDR_LEN + PS_ICMP_HDR_LEN_EXTRACT 
                          + PS_OPT_PATH_ETH_HEADER_OFFSET + sizeof(qos_hdr) ) )
            {
              ps_ul_optimized_v4_low_latency_traffic_handler( pkt_buf,
                           PS_OPT_PATH_V4_HDR_LEN + 
                             PS_OPT_PATH_ETH_HEADER_OFFSET + sizeof(qos_hdr),
                           PS_OPT_PATH_V4_PROT_FIELD_OFFSET + 
                             PS_OPT_PATH_ETH_HEADER_OFFSET + sizeof(qos_hdr) );
            }
            
            dsm_enqueue(&info->info_v4->llc_rx_wmk,&rx_pkt);
            PS_SET_EXT1_SIGNAL( info->info_v4->constants.llc_rx_sig );
          break;
        }

    #ifdef FEATURE_DATA_PS_IPV6
        case LAN_ETHERTYPE_IPV6:
        {
          /*---------------------------------------------------------------------
            This is an IPv6 frame
          ---------------------------------------------------------------------*/
            rm_iface_ptr = info->info_v6->rmnet_iface_ptr;
            if( TRUE == PS_IFACE_GET_POWERSAVE_FILTERING_MODE(rm_iface_ptr) )
            {
              if( PS_IFACE_GET_POWERSAVE_FILTERING_AUTO_EXIT_MODE(rm_iface_ptr) )
              {
                ps_iface_set_powersave_filtering_mode(rm_iface_ptr, enable, 
                                                      &ps_errno);
              }
            }
            
           /*--------------------------------------------------------------------
           Extract bytes from rx_pkt to check whether it is low latency packet
           (ICMP6 ECHO REQ/RESP). Since the QoS header is present and the data 
           format is ETHERNET we need to extract QoS Header length + 
           Ethernet Header length + IPv6 header length + 2 bytes (for ICMP6
           type and code).

           If the packet is fragmented then we need to check whether it is 
           the first fragment of the packet. i.e. next_hdr field in IPv6 header
           is fragment header() and fragment offset is 0 in fragment header.
           So we extract QoS Header length + Ethernet Header length + IPv6 
           header length + 4 bytes 

           If there aren't enough bytes to extract then the packet doesn't 
           belong to low latency traffic and low latency traffic handler is not
           called
          -------------------------------------------------------------------*/
            if( PS_UL_OPT_QOS_ETH_V6_ICMP6_LEN_CHECK == 
                 dsm_extract(rx_pkt,
                   0,
                   pkt_buf,
                   PS_OPT_PATH_V6_HDR_LEN + PS_OPT_PATH_ICMP6_HDR_LEN_EXTRACT +
                    PS_OPT_PATH_ETH_HEADER_OFFSET + sizeof(qos_hdr) ) )
            {
              rmnet_meta_smi_v6_low_latency_traffic_handler( pkt_buf,
                             PS_OPT_PATH_V6_HDR_LEN + 
                               PS_OPT_PATH_ETH_HEADER_OFFSET + sizeof(qos_hdr),
                             PS_OPT_PATH_V6_NXT_HDR_OFFSET + 
                              PS_OPT_PATH_ETH_HEADER_OFFSET + sizeof(qos_hdr),
                              rx_pkt );
            }

            dsm_enqueue(&info->info_v6->llc_rx_wmk,&rx_pkt);
            PS_SET_EXT1_SIGNAL( info->info_v6->constants.llc_rx_sig );

          break;
        }
    #endif /* FEATURE_DATA_PS_IPV6 */

        case LAN_ETHERTYPE_ARP:
        {
          /*---------------------------------------------------------------------
            This is an IP frame
          ---------------------------------------------------------------------*/
            dsm_enqueue(&info->info_v4->llc_rx_wmk,&rx_pkt);
            PS_SET_EXT1_SIGNAL( info->info_v4->constants.llc_rx_sig );

          break;
        }
        default:
        {
          LOG_MSG_ERROR_1 ("Dropping frame with unsupported ethernet type %d",
                           ethertype );
          dsm_free_packet(&rx_pkt);
          break;
        }

      } //End switch

      //lan_llci_decapsulate
      //lan_llci_rx_sig_hdler
    }
    PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
    return FALSE;
  } /* end if */

  PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
  return TRUE;
} /* rmnet_meta_smi_legacy_sig_hdlr() */

/*===========================================================================
FUNCTION RMNET_META_SMI_LEGACY_SIG_HDLR_QOS2

DESCRIPTION
  This function processes packets/frames that need to take legacy path.
  It handles QoSv2 (8-byte header) if QoS is enabled.

PARAMETERS
  rx_sig    -  RmNet Legacy sig
  user_data -  user data contains rmnet_smi_dual_ip_info_type

RETURN VALUE
  None.

DEPENDENCIES
  None

SIDE EFFECTS
  None.

===========================================================================*/
static boolean rmnet_meta_smi_legacy_sig_hdlr_qos2
(
  rmnet_legacy_sig_enum_type rx_sig,
  void*            user_data
)
{
  rmnet_smi_dual_ip_info_type *  info;
  rmnet_meta_smi_qos2_hdr_type   qos_hdr;
  ps_meta_info_type_ex        *  meta_info_ex_ptr = NULL;
  dsm_item_type               *  rx_pkt;
  ps_iface_type               *  rm_iface_ptr = NULL;

  ip_version_enum_type     ip_ver;
  uint8                    octet_val;
  boolean  enable      = FALSE;
  sint15   ps_errno;
  uint8    pkt_buf[PS_OPT_PATH_ETH_QOS_HEADER_LEN  +  1] ALIGN(4); 
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
 LOG_MSG_INFO4_0("In rmnet_meta_smi_legacy_sig_hdlr_qos2");
 PS_ENTER_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);

  ASSERT(user_data);

  memset((void*)&qos_hdr, 0, sizeof(rmnet_meta_smi_qos2_hdr_type));

  info = (rmnet_smi_dual_ip_info_type  *) user_data;

  if(rx_sig != info->legacy_sig)
  {
    LOG_MSG_ERROR_2 ("RX signal mismatch: expected=%d recieved=%d",
                     info->rx_sig, rx_sig );
    PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
    return TRUE;
  }

  /*---------------------------------------------
   Get next packet via the registered rx function
  -----------------------------------------------*/
  if ( (RMNET_META_SM_NULL_STATE == info->info_v4->meta_sm.state)
       && (RMNET_META_SM_NULL_STATE == info->info_v6->meta_sm.state) )
  {
    LOG_MSG_ERROR_0 ("Both state machines are in RMNET_META_SM_NULL_STATE" );
    PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
    return TRUE;
  }

  /*-----------------------------------------------------------------------
   Dequeue packet from RmNet Legacy Wmk.
  ------------------------------------------------------------------------*/
  if ( (rx_pkt = rmnet_meta_smi_rm_rx_data_qos2(info, &qos_hdr)) != NULL)
  {

    if(RMNET_IP_MODE == info->info_v4->link_prot)
    {

      /*---------------------------------------------------------------------
        Get IP version of packet
      ---------------------------------------------------------------------*/
      if (sizeof(uint8) != dsm_extract(rx_pkt, 0, &octet_val, sizeof(uint8)))
      {
        LOG_MSG_INFO2_0("unable to get IP version");
        PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
        dsm_free_packet(&rx_pkt);
        return TRUE;
      }

      ip_ver = (ip_version_enum_type) (octet_val >> 4);

      if (IP_V4 == ip_ver)
      {
        /*---------------------------------------------------------------------
          Ensure that IPv4 call is UP
        ---------------------------------------------------------------------*/
        if (RMNET_META_SM_NULL_STATE != info->info_v4->meta_sm.state &&
            PS_IFACE_IS_VALID(info->info_v4->um_iface_ptr))
        {
          rmnet_meta_smi_process_qos2_hdr((void* )info->info_v4,
                                         rx_pkt,
                                         &meta_info_ex_ptr,
                                         &qos_hdr);

          rm_iface_ptr = info->info_v4->rmnet_iface_ptr;
          
          if( TRUE == PS_IFACE_GET_POWERSAVE_FILTERING_MODE(rm_iface_ptr) )
          {
            if( PS_IFACE_GET_POWERSAVE_FILTERING_AUTO_EXIT_MODE(rm_iface_ptr) )
            {
              ps_iface_set_powersave_filtering_mode(rm_iface_ptr, enable, 
                                                    &ps_errno);
            }
          }
          
          PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
         /*--------------------------------------------------------------------
           Extract bytes from rx_pkt to check whether they are low latency 
           packets (ICMP ECHO REQ/RESP). Since the QoS header(if present ) 
           is already pulled off and the data format is IP mode we only need to
           extract IPv4 header length + 2 bytes (for ICMP type and code).

           If there aren't enough bytes to extract then the packet doesn't 
           belong to low latency traffic and low latency traffic handler is not
           called
          -------------------------------------------------------------------*/

          if( PS_UL_OPT_V4_IP_ICMP_LEN_CHECK == 
                dsm_extract(rx_pkt,
                           0,
                           pkt_buf,
                           PS_OPT_PATH_V4_HDR_LEN + PS_ICMP_HDR_LEN_EXTRACT ) ) 
          {
            ps_ul_optimized_v4_low_latency_traffic_handler( pkt_buf,
                                            PS_OPT_PATH_V4_HDR_LEN,
                                            PS_OPT_PATH_V4_PROT_FIELD_OFFSET );
          }

          (void) ps_iface_input(rm_iface_ptr,
                                &rx_pkt,
                                meta_info_ex_ptr);
           return FALSE;
        }
        else
        {
          LOG_MSG_ERROR_0("Call not up! Dropping packet!");
          dsm_free_packet(&rx_pkt);
        }
      }
      else if (IP_V6 == ip_ver)
      {
        /*---------------------------------------------------------------------
          Ensure that IPv6 call is UP
        ---------------------------------------------------------------------*/
        if (RMNET_META_SM_NULL_STATE != info->info_v6->meta_sm.state &&
            PS_IFACE_IS_VALID(info->info_v6->um_iface_ptr))
        {
          rmnet_meta_smi_process_qos2_hdr((void* )info->info_v6,
                                         rx_pkt,
                                         &meta_info_ex_ptr,
                                         &qos_hdr);
          rm_iface_ptr = info->info_v6->rmnet_iface_ptr;

          if( TRUE == PS_IFACE_GET_POWERSAVE_FILTERING_MODE(rm_iface_ptr) )
          {
            if( PS_IFACE_GET_POWERSAVE_FILTERING_AUTO_EXIT_MODE(rm_iface_ptr) )
            {
              ps_iface_set_powersave_filtering_mode(rm_iface_ptr, enable, 
                                                    &ps_errno);
            }
          }
          
          PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);

          /*--------------------------------------------------------------------
           Extract bytes from rx_pkt to check whether it is a low latency
           packet (ICMP6 ECHO REQ/RESP). Since the QoS header(if present ) 
           is already pulled off and the data format is IP mode we only need to
           extract IPv6 header length + 2 bytes (for ICMP6 type and code).
           If the packet is fragmented then we need to check whether it is 
           the first fragment of the packet. i.e. next_hdr field in IPv6 header
           is fragment header() and fragment offset is 0 in fragment header.
           So we extract IPv6 header length + 4 bytes 

           If there aren't enough bytes to extract then the packet doesn't 
           belong to low latency traffic and low latency traffic handler is not
           called
          -------------------------------------------------------------------*/

          if( PS_UL_OPT_V6_IP_ICMP_LEN_CHECK == 
                dsm_extract(rx_pkt,
                 0,
                 pkt_buf,
                 PS_OPT_PATH_V6_HDR_LEN + PS_OPT_PATH_ICMP6_HDR_LEN_EXTRACT ) )
          {
            rmnet_meta_smi_v6_low_latency_traffic_handler( pkt_buf,
                                                PS_OPT_PATH_V6_HDR_LEN,
                                                PS_OPT_PATH_V6_NXT_HDR_OFFSET,
                                                  rx_pkt );
          }

          (void) ps_iface_input(rm_iface_ptr,
                                &rx_pkt,
                                meta_info_ex_ptr);
           return FALSE;
        }
        else
        {
          LOG_MSG_ERROR_0("Call not up! Dropping packet!");
          dsm_free_packet(&rx_pkt);
        }
      }
      else
      {
        LOG_MSG_ERROR_1("Invalid IP version %d, dropping packet!", ip_ver);
        dsm_free_packet(&rx_pkt);
      }

      /*-------------------------------------------------------------------
       Input to PS_IFACE.

       //MCT Should IP type and Iface type be enforced here, or let the stack
        handle it? Otherwise we'll have to parse the IP header, etc.
      -------------------------------------------------------------------*/

    }
    else
    {
      LOG_MSG_ERROR("Not in IP mode!", 0, 0, 0);
      dsm_free_packet(&rx_pkt);
    }

    PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
    return FALSE;
  } /* end if */

  PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
  return TRUE;
} /* rmnet_meta_smi_legacy_sig_hdlr_qos2() */

/*===========================================================================
FUNCTION RMNET_META_SMI_IP_MODE_TX_CMD

DESCRIPTION
  This function is used to transmit IP packets without any underlying link
  layer protocol.

PARAMETERS
  iface_ptr     -  The interface on which to transmit the packet
  pkt_chain_ptr -  message (in dsm chain) to be transmitted
  client_info   -  user data passed that contains rmnet_meta_sm info ptr

RETURN VALUE
  None.

DEPENDENCIES
  None

SIDE EFFECTS
  None.

===========================================================================*/
static int rmnet_meta_smi_ip_mode_tx_cmd
(
  ps_iface_type *      iface_ptr,
  dsm_item_type **     pkt_chain_ptr,
  ps_meta_info_type*   meta_info_ptr,
  void*                client_info
)
{
  rmnet_smi_info_type *  info;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------------
    Retrieve client info.
  -------------------------------------------------------------------------*/
  info = (rmnet_smi_info_type *) client_info;

#ifdef FEATURE_DATA_A2
  if (pkt_chain_ptr != NULL && *pkt_chain_ptr != NULL)
  {
    A2_COMMON_SET_DSM_PKT_UNIQUE_ID(
                   *pkt_chain_ptr, (*pkt_chain_ptr)->kind);
  }
#endif /* FEATURE_DATA_A2 */

  /*-------------------------------------------------------------------------
    Free meta info and transmit packet.
  -------------------------------------------------------------------------*/
  PS_META_INFO_FREE(&meta_info_ptr);
  info->meta_sm.lp_tx_f_ptr(pkt_chain_ptr, info);

  return 0;
} /* rmnet_meta_smi_ip_mode_tx_cmd() */


/*===========================================================================
FUNCTION RMNET_META_SMI_RM_TX_DATA

DESCRIPTION
  This function is called by the bound lan llc instance to transmit a framed
  packet over the RmNet interface.

PARAMETERS
  item_head_ptr -  message (in dsm chain) to be transmitted
  user_data     -  user data passed to lan_llc_start(),
                   contains rmnet_meta_sm info ptr

RETURN VALUE
  None.

DEPENDENCIES
  None

SIDE EFFECTS
  None.

===========================================================================*/
static void rmnet_meta_smi_rm_tx_data
(
  dsm_item_type **  item_head_ptr,
  void *            user_data
)
{
  rmnet_smi_info_type *  info;
  uint32   pkt_len;
  uint16   ret_val;
  boolean  enable      = FALSE;
  sint15   ps_errno;
  errno_enum_type          err_no;
  ps_rx_meta_info_type     * rx_mi_ptr = NULL;
  ip_pkt_info_type         *ip_pkt_info = NULL;
  ps_iface_type            *base_um_iface_ptr, *assc_iface_ptr;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT (user_data);
  info = (rmnet_smi_info_type *) user_data;

  // Packet filtering
  if (info->meta_sm.packet_filters != NULL &&
      info->meta_sm.packet_filters->enabled &&
      info->meta_sm.packet_filters->restrictive !=
      rmnet_utils_packet_filter_match(
        &(info->meta_sm.packet_filters->filter_list), item_head_ptr))
  {
    LOG_MSG_ERROR_2("Restrictive(%d) Filtering applied, "
                    "packets not allowed! dropping size(%d)",
                    info->meta_sm.packet_filters->restrictive,
                    dsm_length_packet(*item_head_ptr));
    dsm_free_packet(item_head_ptr);
    return;
  }

  /*---------------------------------------------------------------------------
    RNDIS mode
  ---------------------------------------------------------------------------*/
  if (info->meta_sm.data_agg_protocol.dl_data_agg_protocol ==
      RMNET_ENABLE_DATA_AGG_RNDIS)
  {

    /*---------------------------------------------------------------------------
      Fill RNDIS hdr fields and Pushdown only RNDIS hdr as we have an Ethernet packet
    ---------------------------------------------------------------------------*/

    pkt_len = dsm_length_packet(*item_head_ptr);
    info->meta_sm.rndis_eth_hdr->rndis_hdr.msg_len =
        sizeof(rmnet_rndis_hdr_type) + pkt_len;
    info->meta_sm.rndis_eth_hdr->rndis_hdr.data_len = pkt_len;

    if (!qmi_svc_dsm_pushdown_packed(item_head_ptr,
                                  &info->meta_sm.rndis_eth_hdr->rndis_hdr,
                                  sizeof(rmnet_rndis_hdr_type),
                                  DSM_DS_SMALL_ITEM_POOL,
                                  FILE_ID_DS_RMNET_META_SM,__LINE__))
    {
      dsm_free_packet(item_head_ptr);
      return;
    }

  }

  if( PS_IFACE_GET_POWERSAVE_FILTERING_MODE(info->rmnet_iface_ptr))
  {
    base_um_iface_ptr = ps_iface_bridge_iface( info->rmnet_iface_ptr );
    if( !PS_IFACE_IS_VALID( base_um_iface_ptr ) )
    {
      return;
    }
    if( ps_rx_pkt_info_generate(item_head_ptr, &rx_mi_ptr, &err_no) == -1)
    {
        dsm_free_packet(item_head_ptr);
        IFACE_INC_INSTANCE_STATS(info->rmnet_iface_ptr, pkts_dropped_tx, 1);
    
        assc_iface_ptr = PS_IFACE_GET_BASE_IFACE( base_um_iface_ptr );
        IFACE_INC_INSTANCE_STATS(assc_iface_ptr, pkts_dropped_rx, 1);

        LOG_MSG_ERROR_0("rmnet_meta_smi_rm_tx_data(): Not able to"
                        "generate rx_mi_ptr info");
  
        return;
    }
    ip_pkt_info = &rx_mi_ptr->pkt_info;
  
    if( PS_IFACE_IPFLTR_NOMATCH == 
        ps_iface_ipfltr_execute( info->rmnet_iface_ptr, IP_FLTR_CLIENT_POWERSAVE, 
                                 PS_IFACE_IPFLTR_SUBSET_ID_DEFAULT, ip_pkt_info))
    {
      //drop packet and send indication to run the fast dormancy delay timer algorithm      
      IFACE_INC_INSTANCE_STATS(info->rmnet_iface_ptr, pkts_dropped_tx, 1);

      assc_iface_ptr = PS_IFACE_GET_BASE_IFACE( base_um_iface_ptr );
      IFACE_INC_INSTANCE_STATS(assc_iface_ptr, pkts_dropped_rx, 1);

      if(!PS_IFACE_IS_POWERSAVE_FIRST_PACKET_DROP_REPORTED())
      {
        PS_IFACE_SET_POWERSAVE_FIRST_PACKET_DROP_REPORTED(TRUE);
        ps_sys_fast_dormancy_pkt_dropped_ind(info->rmnet_iface_ptr);
      }
      
      dsm_free_packet(item_head_ptr);
      ret_val = FALSE;
    }
    else
    {
      rmnet_sio_transmit( info->meta_sm.rmnet_sio_handle, item_head_ptr );
      if( PS_IFACE_GET_POWERSAVE_FILTERING_AUTO_EXIT_MODE(info->rmnet_iface_ptr))
      {
        if( -1 == ps_iface_set_powersave_filtering_mode( info->rmnet_iface_ptr,
                                                         FALSE,
                                                         &ps_errno) )
        {
            LOG_MSG_ERROR_0("rmnet_meta_smi_rm_tx_data:"
                            " powersave filtering mode change failed ");
        }
      }
    }
    ps_rx_meta_info_free(&rx_mi_ptr);
  }
  else
  {
    rmnet_sio_transmit( info->meta_sm.rmnet_sio_handle, item_head_ptr );
  } 

} /* rmnet_meta_smi_rm_tx_data() */

/*===========================================================================
FUNCTION RMNET_META_SMI_RM_TX_DATA_MBIM

DESCRIPTION
  Tx function for RmNet which adds MBIM header on top of the IP packet.

PARAMETERS
  item_head_ptr -  message (in dsm chain) to be transmitted
  user_data     -  user data passed to lan_llc_start(),
                   contains rmnet_meta_sm info ptr

RETURN VALUE
  None.

DEPENDENCIES
  None

SIDE EFFECTS
  None.

===========================================================================*/
static void rmnet_meta_smi_rm_tx_data_mbim
(
  dsm_item_type **  item_head_ptr,
  void *            user_data
)
{
  rmnet_smi_info_type *  info;
#ifdef FEATURE_DATA_PS_MBIM_SUPPORT
  uint16                 total_len;
#endif /* FEATURE_DATA_PS_MBIM_SUPPORT */
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT (user_data);
  info = (rmnet_smi_info_type *) user_data;


  //MBIM packet filtering
  if (info->meta_sm.packet_filters != NULL &&
      info->meta_sm.packet_filters->enabled &&
      info->meta_sm.packet_filters->restrictive !=
      rmnet_utils_packet_filter_match(
        &(info->meta_sm.packet_filters->filter_list), item_head_ptr))
  {
    LOG_MSG_ERROR_2("Restrictive(%d) Filtering applied, "
                    "packets not allowed! dropping size(%d)",
                    info->meta_sm.packet_filters->restrictive,
                    dsm_length_packet(*item_head_ptr));
    dsm_free_packet(item_head_ptr);
    return;
  }

#ifdef FEATURE_DATA_PS_MBIM_SUPPORT
  total_len = (uint16)dsm_length_packet(*item_head_ptr);
  if (FALSE ==
        ps_dl_opt_add_mbim_header(info, item_head_ptr,
                                  total_len))
  {
    return;
  }
#endif /* FEATURE_DATA_PS_MBIM_SUPPORT */

  info->meta_sm.aggr_pkt = NULL;
  rmnet_sio_transmit( info->meta_sm.rmnet_sio_handle, item_head_ptr );

} /* rmnet_meta_smi_rm_tx_data_mbim() */

/*===========================================================================
FUNCTION RMNET_META_SMI_INIT_MBIM_HDR

DESCRIPTION
  Tx function for RmNet which adds MBIM header on top of the IP packet.

PARAMETERS
  item_head_ptr -  message (in dsm chain) to be transmitted
  user_data     -  user data passed to lan_llc_start(),
                   contains rmnet_meta_sm info ptr

RETURN VALUE
  None.

DEPENDENCIES
  None

SIDE EFFECTS
  None.

===========================================================================*/
static void rmnet_meta_smi_init_mbim_hdr
(
  rmnet_smi_info_type *info
)
{
  rmnet_ntb_hdr_type *ntbh = info->meta_sm.ntb_header;
  ASSERT(ntbh != NULL);

  memset(ntbh, 0, sizeof(rmnet_ntb_hdr_type));
  ntbh->nthc.signature = RMNET_MBIM_HDR_NTH16_SIG;
  ntbh->nthc.hdr_len = PS_IFACE_MBIM_NTH16_HEAD_LEN;
  ntbh->nthc.mbim_block_len = sizeof(rmnet_ntb_hdr_type);
  ntbh->nthc.first_ndp_index = sizeof(ps_iface_mbim_ntb_nth_hdr);
  ntbh->ndpc.signature = info->meta_sm.data_agg_protocol.ndp_signature;
  ntbh->ndpc.len = sizeof(ps_iface_mbim_ntb_ndp_hdr);
  ntbh->ndpc.next_ndp_index = 0;
} /* rmnet_meta_smi_init_mbim_hdr() */

/*===========================================================================
FUNCTION RMNET_META_SMI_INIT_RNDIS_ETH_HDR

DESCRIPTION
  Inits RNDIS ETH HDR that needs to be added for DL packets.

PARAMETERS
  item_head_ptr -  message (in dsm chain) to be transmitted
  user_data     -  user data passed to lan_llc_start(),
                   contains rmnet_meta_sm info ptr

RETURN VALUE
  None.

DEPENDENCIES
  None

SIDE EFFECTS
  None.

===========================================================================*/
static void rmnet_meta_smi_init_rndis_eth_hdr
(
  rmnet_smi_info_type * info
)
{
  rmnet_rndis_eth_hdr_type * rndis_eth_hdr = NULL;

  rndis_eth_hdr =
    qmi_svc_ps_system_heap_mem_alloc(sizeof(rmnet_rndis_eth_hdr_type),
                        FILE_ID_DS_RMNET_META_SM,__LINE__);

  if (rndis_eth_hdr == NULL)
  {
    ASSERT(0);
  }

  memset(rndis_eth_hdr, 0, sizeof(rmnet_rndis_eth_hdr_type));

  rndis_eth_hdr->rndis_hdr.msg_type = RMNET_RNDIS_MSG_TYPE;
  rndis_eth_hdr->rndis_hdr.data_offset = RMNET_RNDIS_PAYLOAD_OFFSET;

  /*Update back in info*/
  info->meta_sm.rndis_eth_hdr = rndis_eth_hdr;

}/*rmnet_meta_smi_init_rndis_eth_hdr()*/

/*===========================================================================
FUNCTION RMNET_META_SMI_RM_TLP_TX_DATA

DESCRIPTION
  This function is called by the bound lan llc instance to transmit a
  TLP framed packet over the RmNet interface.

PARAMETERS
  item_head_ptr -  message (in dsm chain) to be transmitted
  user_data     -  user data passed to lan_llc_start(),
                   contains rmnet_meta_sm info ptr

RETURN VALUE
  None.

DEPENDENCIES
  None

SIDE EFFECTS
  If TLP header can't be prepended, the packet will be silently discarded.
===========================================================================*/
static void rmnet_meta_smi_rm_tlp_tx_data
(
  dsm_item_type **  item_head_ptr,
  void *            user_data
)
{
  rmnet_smi_info_type *  info;
  uint16                 len;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT (user_data);
  info = (rmnet_smi_info_type *) user_data;

  len = (uint16) dsm_length_packet(*item_head_ptr);
#define TLP_SYNC_BITS 0xF800
#define MAX_TLP_PACKET_SIZE 0x7FF
#define MAX_LLC_PACKET_SIZE 1514
  if( len > MAX_LLC_PACKET_SIZE )
  {
    if( len > MAX_TLP_PACKET_SIZE )
    {
      LOG_MSG_ERROR_1 ("Frame too big (%d bytes) for TLP!  dropping", len );
      dsm_free_packet(item_head_ptr);
      return;
    }
    LOG_MSG_INFO2_1( "Warning - sending oversized LLC packet (%d bytes)", len);
  }

  len |= TLP_SYNC_BITS;
  if (!qmi_svc_dsm_pushdown_packed( item_head_ptr,
                             &len,
                             sizeof(len),
                             DSM_DS_SMALL_ITEM_POOL,
                             FILE_ID_DS_RMNET_META_SM,__LINE__))
  {
    dsm_free_packet(item_head_ptr);
    return;
  }

  rmnet_sio_transmit( info->meta_sm.rmnet_sio_handle, item_head_ptr );

} /* rmnet_meta_smi_rm_tlp_tx_data() */

/*===========================================================================
  FUNCTION RMNET_META_SMI_RM_RX_LLC_DATA()

  DESCRIPTION
    Get data from the rx watermark for the LLC

  PARAMETERS
    rmnet_smi_info_handle :  rmnet sm handle

  RETURN VALUE
    DSM item pointer contained received packet

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type *  rmnet_meta_smi_rm_rx_llc_data
(
  void *  rmnet_smi_info_handle,
  ps_meta_info_type_ex  ** meta_info_ex_ptr_ptr
)
{
  rmnet_smi_info_type *  info = NULL;
  dsm_item_type *        retval;
  rmnet_meta_smi_qos_hdr_type  qos_hdr;
  byte *  temp_ptr;
  uint16  bytes_read;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  PS_ENTER_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);

  ASSERT(rmnet_smi_info_handle);
  info = (rmnet_smi_info_type *) rmnet_smi_info_handle;

  retval = dsm_dequeue( &info->llc_rx_wmk );

  LOG_MSG_INFO3_1 ("Received %d byte packet that will be passed to LLC",
                   dsm_length_packet(retval));

  if(NULL == retval)
  {
    PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
    return NULL;
  }

  if(NULL == info->um_iface_ptr)
  {
    LOG_MSG_ERROR_0("Dropping packet, call is down as um_iface_ptr is NULL");
    dsm_free_packet(&retval);
    PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
    return NULL;
  }

  /*-------------------------------------------------------------------------
    Retrieve QoS header, always added in the first receive function
  -------------------------------------------------------------------------*/
  temp_ptr = (byte*) &qos_hdr;
  bytes_read = qmi_svc_dsm_pullup(&retval, temp_ptr, sizeof(qos_hdr),
                          FILE_ID_DS_RMNET_META_SM,__LINE__  );
  if(sizeof(qos_hdr) != bytes_read)
  {
    PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
    ASSERT(0);
    return NULL;
  }

  rmnet_meta_smi_process_qos_hdr ((void* )info,
                                  retval,
                                  meta_info_ex_ptr_ptr,
                                  &qos_hdr);

  PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
  return retval;
} /* rmnet_meta_smi_rm_rx_llc_data() */

/*===========================================================================
FUNCTION RMNET_META_SMI_RX_READY

DESCRIPTION


PARAMETERS


RETURN VALUE


DEPENDENCIES


SIDE EFFECTS
  None.

===========================================================================*/
boolean rmnet_meta_smi_rx_ready
(
  rmnet_smi_info_type    * info
)
{

  if(info->meta_sm.data_format == 0 &&  //legacy i.e. non-qos
     info->meta_sm.qos_ev_enabled == FALSE) // qos event not enabled by AP
  {
    if((PS_IFACE_COMMON_DEFAULT_IS_FLOW_ENABLED(info->um_iface_ptr)))
      return TRUE;
    else
      return FALSE;
  }

  /*-------------------------------------------------------------------------
    info->meta_sm.data_format == 1 i.e. QoS
  -------------------------------------------------------------------------*/
  return PS_IFACE_FLOW_ENABLED(info->um_iface_ptr);
}
/*===========================================================================
FUNCTION RMNET_META_INSTANCE_RX_READY

DESCRIPTION


PARAMETERS


RETURN VALUE


DEPENDENCIES


SIDE EFFECTS
  None.

===========================================================================*/
static boolean rmnet_meta_instance_rx_ready
(
  uint8 dual_ip_inst
)
{
  int flow_cnt = 0;

  ASSERT( (RMNET_INSTANCE_MAX/2) > dual_ip_inst );

  if( 0 == rmnet_smi_dual_ip_info[dual_ip_inst].rx_flow_count )
    return FALSE;

  if( rmnet_meta_smi_rx_ready(rmnet_smi_dual_ip_info[dual_ip_inst].info_v4) )
    flow_cnt++;

  if( rmnet_meta_smi_rx_ready(rmnet_smi_dual_ip_info[dual_ip_inst].info_v6) )
    flow_cnt++;

  return (flow_cnt > 0);
}

/*===========================================================================
FUNCTION RMNET_META_SMI_RM_RX_DATA

DESCRIPTION
  This function is called by the RmNet_leagcy_sig_hdlr to dequeue packet from
  Legacy Wmk.

PARAMETERS
  info      -  rmnet_smi_dual_ip_info_type
  qos_hdr   -  Qos Hdr is QoS data format is set.

RETURN VALUE
  Legacy rx_pkt

DEPENDENCIES
  RmNetsm_init must have been called previously

SIDE EFFECTS
  None.

===========================================================================*/
static dsm_item_type* rmnet_meta_smi_rm_rx_data
(
  rmnet_smi_dual_ip_info_type *  info,
  rmnet_meta_smi_qos_hdr_type * qos_hdr
)
{
  dsm_item_type * legacy_rx_pkt = NULL;
  byte *  temp_ptr;
  uint16  bytes_read;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if(NULL == info)
  {
    LOG_MSG_ERROR_0("rmnet_meta_smi_rm_rx_data(): info is NULL");
    return NULL;
  }

  /*-------------------------------------------------------------------------
    Check Flow Ctrl on Um and dequeue pkt from Legacy Wmk.
  -------------------------------------------------------------------------*/
  if( !rmnet_meta_instance_rx_ready(info->info_v4->constants.qmi_instance) )
  {
    LOG_MSG_ERROR_0 ("Flow disabled!");
    return NULL;
  }

  LOG_MSG_INFO4_0("Dequeing pkt from Legacy Wmk");

  if(info->info_v4->meta_sm.data_format == 0) //legacy i.e. non-qos
  {
    legacy_rx_pkt = dsm_dequeue(&info->legacy_wmk);
    return legacy_rx_pkt;
  }

  /*-------------------------------------------------------------------------
    info->meta_sm.data_format == 1 i.e. QoS
  -------------------------------------------------------------------------*/
  temp_ptr = (byte*) qos_hdr;
  legacy_rx_pkt = dsm_dequeue(&info->legacy_wmk);
  if(legacy_rx_pkt == NULL)
  {
    return NULL;
  }
  /*-------------------------------------------------------------------------
    Retrieve QoS header if data format is set to 1
  -------------------------------------------------------------------------*/
  bytes_read = qmi_svc_dsm_pullup(&legacy_rx_pkt, temp_ptr, 6,
                      FILE_ID_DS_RMNET_META_SM,__LINE__ );
  if(6 != bytes_read)
  {
    ASSERT(0);
    return NULL;
  }
  return legacy_rx_pkt;
} /* rmnet_meta_smi_rm_rx_data() */

/*===========================================================================
FUNCTION RMNET_META_SMI_RM_RX_DATA_QOS2

DESCRIPTION
  This function is called by the RmNet_leagcy_sig_hdlr to dequeue packet from
  Legacy Wmk. It handles QoSv2 (8-byte header) if QoS is enabled.

PARAMETERS
  info      -  rmnet_smi_dual_ip_info_type
  qos_hdr   -  Qos Hdr is QoS data format is set.

RETURN VALUE
  Legacy rx_pkt

DEPENDENCIES
  RmNetsm_init must have been called previously

SIDE EFFECTS
  None.

===========================================================================*/
static dsm_item_type* rmnet_meta_smi_rm_rx_data_qos2
(
  rmnet_smi_dual_ip_info_type *  info,
  rmnet_meta_smi_qos2_hdr_type * qos_hdr
)
{
  dsm_item_type * legacy_rx_pkt = NULL;
  byte *  temp_ptr;
  uint16  bytes_read;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if(NULL == info)
  {
    LOG_MSG_ERROR_0("rmnet_meta_smi_rm_rx_data(): info is NULL");
    return NULL;
  }

  /*-------------------------------------------------------------------------
    Check Flow Ctrl on Um and dequeue pkt from Legacy Wmk.
  -------------------------------------------------------------------------*/
  if( !rmnet_meta_instance_rx_ready(info->info_v4->constants.qmi_instance) )
  {
    LOG_MSG_ERROR_0 ("Flow disabled!");
    return NULL;
  }

  LOG_MSG_INFO4_0("Dequeing pkt from Legacy Wmk");

  if(info->info_v4->meta_sm.data_format == 0) //legacy i.e. non-qos
  {
    legacy_rx_pkt = dsm_dequeue(&info->legacy_wmk);
    return legacy_rx_pkt;
  }

  /*-------------------------------------------------------------------------
    info->meta_sm.data_format == 1 i.e. QoS
  -------------------------------------------------------------------------*/
  temp_ptr = (byte*) qos_hdr;
  legacy_rx_pkt = dsm_dequeue(&info->legacy_wmk);
  if(legacy_rx_pkt == NULL)
  {
    return NULL;
  }
  /*-------------------------------------------------------------------------
    Retrieve QoS header if data format is set to 1
  -------------------------------------------------------------------------*/
  bytes_read = qmi_svc_dsm_pullup(&legacy_rx_pkt, temp_ptr, 8,FILE_ID_DS_RMNET_META_SM,__LINE__);
  if(8 != bytes_read)
  {
    ASSERT(0);
    return NULL;
  }
  return legacy_rx_pkt;
} /* rmnet_meta_smi_rm_rx_data_qos2() */


/*===========================================================================
FUNCTION RMNET_META_SMI_PROCESS_QOS_HDR

DESCRIPTION
  Processes QOS header

PARAMETERS
  user_data     -  user data passed to lan_llc_start(),
                   contains rmnet_meta_sm info ptr

RETURN VALUE


DEPENDENCIES


SIDE EFFECTS
  None.

===========================================================================*/
void rmnet_meta_smi_process_qos_hdr
(
  void *                user_data,
  dsm_item_type *       pkt_ptr,
  ps_meta_info_type_ex  ** meta_info_ex_ptr_ptr,
  rmnet_meta_smi_qos_hdr_type * qos_hdr
)
{
  rmnet_smi_info_type    * info = NULL;
  ps_tx_meta_info_type * tx_meta_info_ptr;
  ps_meta_info_type_ex * meta_info_ex_ptr;
  uint8                  octet_val = 0;
  struct ip              pkt_ip4_hdr = {0,};
  rmnet_instance_e_type  rmnet_inst;
  int                    return_val;
  int16                  ps_errno;
  ps_flow_type *         flow_ptr = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if(NULL == user_data)
  {
    LOG_MSG_ERROR_0("rmnet_meta_smi_process_qos_hdr(): user_data ptr is NULL");
    return ;
  }

  info = (rmnet_smi_info_type *) user_data;
  rmnet_inst = RMNET_META_SM_INFO_TO_INSTANCE(info);

  /*-------------------------------------------------------------------------
    info->meta_sm.data_format == 1 i.e. QoS
  -------------------------------------------------------------------------*/

  /* Replace qos handle with flow_ptr if valid */
  if (0 == qmi_qos_get_flow_ptr_from_handle(qos_hdr->qos_handle, &flow_ptr ))
  {
    LOG_MSG_ERROR_1("Flow handle 0x%x passed in is invalid, forwarding onto default flow", qos_hdr->qos_handle);
    flow_ptr = NULL;
  }
  else
  {
    LOG_MSG_INFO3_2("Replacing qos handle 0x%x with flow ptr 0x%x", qos_hdr->qos_handle, flow_ptr);
  }

  if(flow_ptr != NULL)
  {
    /* don't assert because of race condition */
    //ASSERT(PS_FLOW_IS_VALID(qos_hdr.flow_ptr));

    LOG_MSG_INFO3_2("got pkt on qos flow 0x%x, RMNET inst %d",
                    flow_ptr, rmnet_inst);

    /* always fwd on default if flow is !activated */
    if(PS_FLOW_GET_STATE(flow_ptr) != FLOW_ACTIVATED)
    {
      /* Resume flow if it is SUSPENDED, this is needed for NW init QOS on eHRPD*/
      if (PS_FLOW_IS_NW_INITIATED(flow_ptr) &&
          PS_FLOW_GET_STATE(flow_ptr) == FLOW_SUSPENDED)
      {
        LOG_MSG_INFO2_1("Resuming SUSPENDED NW init QOS flow 0x%x", flow_ptr);
        return_val =  ps_flow_ioctl(flow_ptr,
                                    PS_FLOW_IOCTL_QOS_RESUME,
                                    NULL,
                                    &ps_errno);
        if (return_val != 0)
        {
          LOG_MSG_ERROR_1("Error while trying to resume flow %d", ps_errno);
        }
      }
      LOG_MSG_INFO3_0("fwding pkt on default flow");
      flow_ptr = PS_IFACEI_GET_DEFAULT_FLOW(info->um_iface_ptr);
    }
  }
  else
  {
    LOG_MSG_INFO3_1("got pkt on default flow, RMNET inst %d", rmnet_inst);
    /* if 0 - then get default flow */
    flow_ptr = PS_IFACEI_GET_DEFAULT_FLOW(info->um_iface_ptr);
  }

  /* NOT checking whether TX flow is ENABLED for sec/pri..
      assumption: if any flow is TX_DISABLED, driver gets the ind and stops
      sending on that flow. Any packets driver had already sent before
      processing the ind can be queued on flow wmk
      verify HW and DNE values ok for the above */

  /* generate meta_info - TO DO caching */

  /* Since packet needs to be routed to Um Iface, its better we create TX meta info
     directly here */
  PS_TX_META_INFO_GET_ALL(tx_meta_info_ptr);

  if ((tx_meta_info_ptr == NULL ) ||
      (PS_TX_META_GET_RT_META_INFO_PTR(tx_meta_info_ptr) == NULL))
  {
    LOG_MSG_ERROR_0("Cannot allocate meta_info");
    ASSERT( 0 );
    return ;
  }

  PS_TX_META_SET_ROUTING_CACHE(tx_meta_info_ptr, info->um_iface_ptr);

  if( 0 != info->meta_sm.data_format )
  {
    PS_TX_META_SET_FILTER_RESULT(tx_meta_info_ptr,
                               IP_FLTR_CLIENT_QOS_OUTPUT,
                               flow_ptr);
  }

  /* If the DOS flag is set, propagate the information to the PS layer
   * by setting the appropriate flag in meta info. Use the IP_ID from
   * the packet as the DOS ack handle. */
  if (qos_hdr->flags & ((uint8)RMNET_QOS_FLAG_TYPE_DOS))
  {
    /* Extract the IP version. DOS is supported only for IPv4 packets. */
    if ( (sizeof(uint8) == dsm_extract( pkt_ptr,
                                        0,
                                        &octet_val,
                                        sizeof(uint8))) &&
         (IP_V4 == (ip_version_enum_type)(octet_val >> 4)) )
    {
      /* Peek and extract the IP_ID from the IPv4 header. */
      if (IPLEN == dsm_extract( pkt_ptr,
                                0,
                                (void*)&pkt_ip4_hdr,
                                IPLEN ))
      {
        ASSERT(tx_meta_info_ptr->pkt_meta_info_ptr != NULL);
        tx_meta_info_ptr->pkt_meta_info_ptr->tx_flags |= MSG_FAST_EXPEDITE;
        tx_meta_info_ptr->pkt_meta_info_ptr->dos_ack_handle = pkt_ip4_hdr.id;
        LOG_MSG_INFO2_1( "DOS enabled on pkt, dos_ack_handle:0x%x",
                         tx_meta_info_ptr->pkt_meta_info_ptr->dos_ack_handle);
      }
      else
      {
        LOG_MSG_INFO1_0("Invalid IP header, DOS not enabled");
      }
    }
    else
    {
      LOG_MSG_INFO1_0("Invalid IP version, DOS not enabled");
    }
  }

  /* Update meta_info_ex_ptr : create meta_info_ex_ptr and embed
  tx_meta_info_ptr inside */
  PS_META_INFO_GET_EX(meta_info_ex_ptr);
  if (meta_info_ex_ptr == NULL )
  {
    LOG_MSG_ERROR_0("Cannot allocate ps_meta_info_type_ex");
    ASSERT( 0 );
    return ;
  }

  meta_info_ex_ptr->tx_meta_info_ptr = tx_meta_info_ptr;
  (*meta_info_ex_ptr_ptr) = meta_info_ex_ptr;
} /* rmnet_meta_smi_process_qos_hdr() */

/*===========================================================================
FUNCTION RMNET_META_SMI_PROCESS_QOS2_HDR

DESCRIPTION
  Processes QOSv2 (8-byte) header

PARAMETERS
  user_data     -  user data passed to lan_llc_start(),
                   contains rmnet_meta_sm info ptr

RETURN VALUE


DEPENDENCIES


SIDE EFFECTS
  None.

===========================================================================*/
static void rmnet_meta_smi_process_qos2_hdr
(
  void *                user_data,
  dsm_item_type *       pkt_ptr,
  ps_meta_info_type_ex  ** meta_info_ex_ptr_ptr,
  rmnet_meta_smi_qos2_hdr_type * qos_hdr
)
{
  rmnet_smi_info_type    * info = NULL;
  ps_tx_meta_info_type * tx_meta_info_ptr;
  ps_meta_info_type_ex * meta_info_ex_ptr;
  uint8                  octet_val = 0;
  struct ip              pkt_ip4_hdr = {0,};
  rmnet_instance_e_type  rmnet_inst;
  int                    return_val;
  int16                  ps_errno;
  ps_flow_type *         flow_ptr = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if(NULL == user_data)
  {
    LOG_MSG_ERROR_0("rmnet_meta_smi_process_qos_hdr2(): user_data ptr is NULL");
    return ;
  }

  info = (rmnet_smi_info_type *) user_data;
  rmnet_inst = RMNET_META_SM_INFO_TO_INSTANCE(info);

  /*-------------------------------------------------------------------------
    info->meta_sm.data_format == 1 i.e. QoS
  -------------------------------------------------------------------------*/

  /* Replace qos handle with flow_ptr if valid */
  if (0 == qmi_qos_get_flow_ptr_from_handle(qos_hdr->hdr.qos_handle, &flow_ptr ))
  {
    LOG_MSG_ERROR_1("Flow handle 0x%x passed in is invalid, forwarding onto default flow", qos_hdr->hdr.qos_handle);
    flow_ptr = NULL;
  }
  else
  {
    LOG_MSG_INFO3_2("Replacing qos handle 0x%x with flow ptr 0x%x", qos_hdr->hdr.qos_handle, flow_ptr);
  }

  if(flow_ptr != NULL)
  {
    /* don't assert because of race condition */
    //ASSERT(PS_FLOW_IS_VALID(qos_hdr.flow_ptr));

    LOG_MSG_INFO3_2("got pkt on qos flow 0x%x, RMNET inst %d",
                    flow_ptr, rmnet_inst);

    /* always fwd on default if flow is !activated */
    if(PS_FLOW_GET_STATE(flow_ptr) != FLOW_ACTIVATED)
    {
      /* Resume flow if it is SUSPENDED, this is needed for NW init QOS on eHRPD*/
      if (PS_FLOW_IS_NW_INITIATED(flow_ptr) &&
          PS_FLOW_GET_STATE(flow_ptr) == FLOW_SUSPENDED)
      {
        LOG_MSG_INFO2_1("Resuming SUSPENDED NW init QOS flow 0x%x", flow_ptr);
        return_val =  ps_flow_ioctl(flow_ptr,
                                    PS_FLOW_IOCTL_QOS_RESUME,
                                    NULL,
                                    &ps_errno);
        if (return_val != 0)
        {
          LOG_MSG_ERROR_1("Error while trying to resume flow %d", ps_errno);
        }
      }
      LOG_MSG_INFO3_0("fwding pkt on default flow");
      flow_ptr = PS_IFACEI_GET_DEFAULT_FLOW(info->um_iface_ptr);
    }
  }
  else
  {
    LOG_MSG_INFO3_1("got pkt on default flow, RMNET inst %d", rmnet_inst);
    /* if 0 - then get default flow */
    flow_ptr = PS_IFACEI_GET_DEFAULT_FLOW(info->um_iface_ptr);
  }

  /* NOT checking whether TX flow is ENABLED for sec/pri..
      assumption: if any flow is TX_DISABLED, driver gets the ind and stops
      sending on that flow. Any packets driver had already sent before
      processing the ind can be queued on flow wmk
      verify HW and DNE values ok for the above */

  /* generate meta_info - TO DO caching */

  /* Since packet needs to be routed to Um Iface, its better we create TX meta info
     directly here */
  PS_TX_META_INFO_GET_ALL(tx_meta_info_ptr);

  if ((tx_meta_info_ptr == NULL ) ||
      (PS_TX_META_GET_RT_META_INFO_PTR(tx_meta_info_ptr) == NULL))
  {
    LOG_MSG_ERROR_0("Cannot allocate meta_info");
    ASSERT( 0 );
    return ;
  }

  PS_TX_META_SET_ROUTING_CACHE(tx_meta_info_ptr, info->um_iface_ptr);

  if( 0 != info->meta_sm.data_format )
  {
    PS_TX_META_SET_FILTER_RESULT(tx_meta_info_ptr,
                               IP_FLTR_CLIENT_QOS_OUTPUT,
                               flow_ptr);
  }

  /* If the DOS flag is set, propagate the information to the PS layer
   * by setting the appropriate flag in meta info. Use the IP_ID from
   * the packet as the DOS ack handle. */
  if (qos_hdr->hdr.flags & ((uint8)RMNET_QOS_FLAG_TYPE_DOS))
  {
    /* Extract the IP version. DOS is supported only for IPv4 packets. */
    if ( (sizeof(uint8) == dsm_extract( pkt_ptr,
                                        0,
                                        &octet_val,
                                        sizeof(uint8))) &&
         (IP_V4 == (ip_version_enum_type)(octet_val >> 4)) )
    {
      /* Peek and extract the IP_ID from the IPv4 header. */
      if (IPLEN == dsm_extract( pkt_ptr,
                                0,
                                (void*)&pkt_ip4_hdr,
                                IPLEN ))
      {
        ASSERT(tx_meta_info_ptr->pkt_meta_info_ptr != NULL);
        tx_meta_info_ptr->pkt_meta_info_ptr->tx_flags |= MSG_FAST_EXPEDITE;
        tx_meta_info_ptr->pkt_meta_info_ptr->dos_ack_handle = pkt_ip4_hdr.id;
        LOG_MSG_INFO2_1( "DOS enabled on pkt, dos_ack_handle:0x%x",
                         tx_meta_info_ptr->pkt_meta_info_ptr->dos_ack_handle);
      }
      else
      {
        LOG_MSG_INFO1_0("Invalid IP header, DOS not enabled");
      }
    }
    else
    {
      LOG_MSG_INFO1_0("Invalid IP version, DOS not enabled");
    }
  }

  /* Update meta_info_ex_ptr : create meta_info_ex_ptr and embed
  tx_meta_info_ptr inside */
  PS_META_INFO_GET_EX(meta_info_ex_ptr);
  if (meta_info_ex_ptr == NULL )
  {
    LOG_MSG_ERROR_0("Cannot allocate ps_meta_info_type_ex");
    ASSERT( 0 );
    return ;
  }

  meta_info_ex_ptr->tx_meta_info_ptr = tx_meta_info_ptr;
  (*meta_info_ex_ptr_ptr) = meta_info_ex_ptr;
} /* rmnet_meta_smi_process_qos2_hdr() */

/*===========================================================================
  FUNCTION RMNET_META_SM_OPEN_INSTANCE

  DESCRIPTION
    Opens all RDM instances

  PARAMETERS
    instance : instance of RmNET to open
    port_id :  SIO port identifier

  DEPENDENCIES
    None

  RETURN VALUE
    TRUE on success
    FALSE otherwise

  SIDE EFFECTS
    None
===========================================================================*/
boolean rmnet_meta_sm_open_instance
(
  rmnet_instance_e_type  instance,
  uint16                 port_id
)
{
  rmnet_smi_info_type *  info;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT( RMNET_INSTANCE_MAX > instance );

  info = &rmnet_smi_info[ instance ];

  if( info->meta_sm.ports_active == TRUE )
  {
    LOG_MSG_INFO1_1( "RmNet instance (%d) that was to be opened is already active!\n",
                     instance );
    return FALSE;
  }

  /* Try to activate this instance */
  if( !rmnet_sio_activate_instance( info->meta_sm.rmnet_sio_handle, port_id ) )
  {
    LOG_MSG_INFO1_1( "Could not activate RmNet instance %d\n", instance );
    return FALSE;
  }

  /*-------------------------------------------------------------------------
    set the initial llc tx function to not use TLP, and register
    for notification when TLP setting changes.
    -------------------------------------------------------------------------*/
#ifndef FEATURE_DATA_RM_NET_USES_SM
  rmnet_sio_set_tlp_change_cb( info->meta_sm.rmnet_sio_handle,
                               rmnet_meta_smi_tlp_change_mult_inst_cb,
                               info );
#endif /* FEATURE_DATA_RM_NET_USES_SM */

#ifndef FEATURE_DATA_QMI_DTR_ON_CTRL_CHANNEL
  /*-------------------------------------------------------------------------
    Set dtr_cb after inited, to make sure sback is called after rmnet_meta_sm
    is inited
    -------------------------------------------------------------------------*/
  /*-------------------------------------------------------------------------
    Configure rmnet_sio for packet call:
    -  Set DTR handler (cable connect/disconnect notification)
    -------------------------------------------------------------------------*/
  rmnet_sio_set_dtr_handler (info->meta_sm.rmnet_sio_handle,
                             rmnet_meta_smi_dtr_change_cb,
                             &rmnet_smi_dual_ip_info[info->constants.qmi_instance]);
#endif /* FEATURE_DATA_QMI_DTR_ON_CTRL_CHANNEL */

  info->meta_sm.ports_active = TRUE;

  return TRUE;
} /* rmnet_meta_sm_open_instance() */

/*===========================================================================
FUNCTION      RMNET_META_SMI_SETUP_LEGACY_WATERMARKS

DESCRIPTION
  Inits the watermarks for packets taking legacy path

PARAMETERS
  info - rmnet_smi_dual_ip_info_type

DEPENDENCIES
  None

 RETURN VALUE
  None

 SIDE EFFECTS
  None

===========================================================================*/
static void rmnet_meta_smi_setup_legacy_watermarks
(
  rmnet_smi_dual_ip_info_type *  info
)
{

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  dsm_queue_init ( &info->legacy_wmk,
                   RMNET_LEGACY_WM_DNE,
                   &info->legacy_q);

  info->legacy_wmk.lo_watermark    = RMNET_LEGACY_WM_LOW;
  info->legacy_wmk.hi_watermark    = RMNET_LEGACY_WM_HIGH;
  info->legacy_wmk.dont_exceed_cnt = RMNET_LEGACY_WM_DNE;

  info->legacy_wmk.lowater_func_ptr  = rmnet_meta_smi_legacy_wmk_low_cb_func;
  info->legacy_wmk.lowater_func_data = info;
  info->legacy_wmk.hiwater_func_ptr  = rmnet_meta_smi_legacy_wmk_high_cb_func;
  info->legacy_wmk.hiwater_func_data = info;

} /* rmnet_meta_smi_setup_legacy_watermarks() */

/*===========================================================================
FUNCTION      RMNET_META_SMI_LEGACY_WMK_NON_EMPTY_CB_FUNC

DESCRIPTION
  Invoked when a pkt is enqueued to Legacy Wmk.

PARAMETERS
  None

DEPENDENCIES
  None

 RETURN VALUE
  None

 SIDE EFFECTS
  None

===========================================================================*/
static void rmnet_meta_smi_legacy_wmk_non_empty_cb_func
(
  struct dsm_watermark_type_s * legacy_wm_ptr,
  void                        * user_data_ptr
)
{
  rmnet_smi_dual_ip_info_type *  info = NULL;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  LOG_MSG_INFO4_0 ("In rmnet_legacy_wmk_non_empty_cb_func");

  info = (rmnet_smi_dual_ip_info_type *) user_data_ptr;
  RMNET_SET_LEGACY_SIGNAL(info->legacy_sig);
}

#ifdef FEATURE_DATA_MBIM
/*===========================================================================
FUNCTION      RMNET_MBIM_PORT_INFO_INIT

DESCRIPTION
  Inits the mbim port info

PARAMETERS
  index - mbim port array index

DEPENDENCIES
  None

 RETURN VALUE
  None

 SIDE EFFECTS
  None

===========================================================================*/
static void rmnet_mbim_port_info_init(uint8 index)
{
  rmnet_mbim_port_info_type * port_info = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  port_info = &mbim_call_info.mbim_port_info[index];

  port_info->qmi_port_inst = QMI_INSTANCE_MAX;
  port_info->ips_id        = -1; /*XXX ?*/
  port_info->info          = NULL;

}/*rmnet_mbim_port_info_init()*/

#ifdef FEATURE_DATA_A2
/*===========================================================================
FUNCTION      RMNET_META_SMI_INIT_RX_MBIM_DATA_PATH

DESCRIPTION
  Init the UL mbim data path when a MBIM call request arrives.

PARAMETERS
  info - rmnet_smi_info_type

DEPENDENCIES
  None

 RETURN VALUE
  None

 SIDE EFFECTS
  None

===========================================================================*/
static void rmnet_meta_smi_init_rx_mbim_data_path
(
  rmnet_smi_info_type *  info
)
{

  uint8 rmnet_master_port_inst          = 0;
  rmnet_mbim_port_info_type * port_info = NULL;
  rmnet_sio_info_type       * sio_info  = NULL;
  rmnet_smi_info_type       * rm_info      = NULL;
  int8 port_index                       = -1;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/


  //port_info =  mbim_call_info.mbim_port_info[info->meta_sm.net_params.ips_id];

  /*-------------------------------------------------------------------------
    Increment call_cnt
  -------------------------------------------------------------------------*/
  mbim_call_info.call_cnt++;

  /*-------------------------------------------------------------------------
    Update mbim_port_info - Use call cnt to choose the index.
  -------------------------------------------------------------------------*/

  //mbim_call_info.ips_id_index_map[info->meta_sm.net_params.ips_id] =
    //mbim_call_info.call_cnt - 1;
  
  if (QMI_AF_INET == info->meta_sm.net_params.ip_family_pref)
  {
     port_index = info->meta_sm.net_params.ips_id * 2;

  }
  else if (QMI_AF_INET6 == info->meta_sm.net_params.ip_family_pref)
  {
     port_index = (info->meta_sm.net_params.ips_id * 2) + 1;
  }

  if ((port_index < 0) || (port_index >= MBIM_IP_SESSION_MAX))
  {
     return;
  }

  port_info = &mbim_call_info.mbim_port_info[port_index];

  port_info->qmi_port_inst = info->constants.qmi_instance;
  port_info->ips_id        = info->meta_sm.net_params.ips_id;
  port_info->info          = info;

  /*-------------------------------------------------------------------------
    Use the master sio port's watermark as the global MBIM watermark as
    subsequent actions can be taken on master sio port.
  -------------------------------------------------------------------------*/

  rmnet_master_port_inst = rmnet_sio_get_master_inst();

  if (rmnet_master_port_inst >= RMNET_INSTANCE_MAX)
  {
    LOG_MSG_ERROR_1("Invalid Master Port Inst: %d", rmnet_master_port_inst );
    ASSERT(0);
    return;
  }
  rm_info              = &rmnet_smi_info[rmnet_master_port_inst];
  mbim_call_info.mbim_rx_wmk =
    &((rmnet_sio_info_type *)(rm_info->meta_sm.rmnet_sio_handle))->rx_wmk;

  /*-------------------------------------------------------------------------
    Set A2 dynamic port configuration
  -------------------------------------------------------------------------*/

  sio_info = (rmnet_sio_info_type *)( info->meta_sm.rmnet_sio_handle);
  sio_info->mux_port_cfg.mux_idx = info->meta_sm.net_params.ips_id;
  sio_info->mux_port_cfg.ul_wm   = mbim_call_info.mbim_rx_wmk;

  LOG_MSG_INFO1_5("In rmnet_meta_smi_init_rx_mbim_data_path(): call_cnt %d"
                  " qmi_port %d ips_id %d rmnet inst %d call_type %d",
                  mbim_call_info.call_cnt,
                  port_info->qmi_port_inst,
                  port_info->ips_id,
                  RMNET_META_SM_INFO_TO_INSTANCE(info),
                  info->meta_sm.net_params.ip_family_pref);

}/*rmnet_meta_smi_init_rx_mbim_data_path()*/
#endif /* FEATURE_DATA_A2 */

/*===========================================================================
FUNCTION      RMNET_META_SMI_CONFIG_RX_MBIM_DATA_PATH

DESCRIPTION
  Configure the UL mbim data path during RM_CONFIG phase(after UM_IFACE_UP ev)

PARAMETERS
  info - rmnet_smi_info_type

DEPENDENCIES
  None

 RETURN VALUE
  None

 SIDE EFFECTS
  None

===========================================================================*/
static int rmnet_meta_smi_config_rx_mbim_data_path
(
  rmnet_smi_info_type *  info
)
{
  uint8 rmnet_master_port_inst          = 0;
  rmnet_smi_info_type       * rm_info   = NULL;
  uint8 ips_id;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ips_id = info->meta_sm.net_params.ips_id;

  LOG_MSG_INFO1_2("In rmnet_meta_smi_config_rx_mbim_data_path():ips_id %d,"
                  "rmnet inst %d",
                  ips_id, RMNET_META_SM_INFO_TO_INSTANCE(info));

  /*-------------------------------------------------------------------------
    For the first MBIM Call -

    1. Setup Rm rx signal Use the same rm signal for all subsequent calls.
       Signal assignment would be done before invoking this fn.

    2. MBIM wmk
  -------------------------------------------------------------------------*/
  if(mbim_call_info.call_cnt > 0 &&
     mbim_call_info.mbim_rx_sig == RMNET_MAX_SIGNALS)
  {

    /*Assign Signal*/
    mbim_call_info.mbim_rx_sig =
    rmnet_smi_dual_ip_info[info->constants.qmi_instance].rx_sig;

    /*Register Wmk for master port*/
    rmnet_master_port_inst = rmnet_sio_get_master_inst();

    if (rmnet_master_port_inst >= RMNET_INSTANCE_MAX)
    {
      LOG_MSG_ERROR_1("Invalid Master Port Inst: %d", rmnet_master_port_inst );
      ASSERT(0);
      return 0;
    }

    rm_info              = &rmnet_smi_info[rmnet_master_port_inst];

    rmnet_sio_set_rx_cb( rm_info->meta_sm.rmnet_sio_handle,
                         rmnet_meta_smi_mbim_wmk_non_empty_cb_func,
                         (void*)&mbim_call_info);

    LOG_MSG_INFO1_1("Storing mbim sig %d and reg non_empty_cb",
                    mbim_call_info.mbim_rx_sig );

  }
  else
  {

    rmnet_smi_dual_ip_info[info->constants.qmi_instance].rx_sig =
      mbim_call_info.mbim_rx_sig;
    info->constants.rx_sig = mbim_call_info.mbim_rx_sig;
  }

  return 0;
}/*rmnet_meta_smi_config_rx_mbim_data_path()*/

/*===========================================================================
  FUNCTION RMNET_META_SMI_INIT_MBIM_OUTSTANDING_INFO

  DESCRIPTION
  Init Outstanding MBIM pkt info

  PARAMETERS
  void

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static int8 rmnet_meta_smi_init_mbim_outstanding_info(void)
{

  /* Allocate outstanding mbim pkt info only if not allocated earlier. Logic is
   * once it is allocated for a MBIM call for this port it is never freed
   * and the same ptr is used for subsequent MBIM calls.
   *
   * We dont need to memset this for subsequent calls since there could be a
   * valid outstanding pkt to be processed from the previous MBIM call.
   *
   * It will be freed -
   *  - when last MBIM call comes down (call_ct = 0)
   *  - for a subsequent non-MBIM call
   */

  LOG_MSG_INFO1_0("In rmnet_meta_smi_init_mbim_outstanding_info()");

  if(mbim_call_info.pkt_info == NULL)
  {
    mbim_call_info.pkt_info =
      qmi_svc_ps_system_heap_mem_alloc(sizeof(rmnet_mbim_pkt_info_type),
                            FILE_ID_DS_RMNET_META_SM,__LINE__);
    if (mbim_call_info.pkt_info == NULL)
    {
      return -1;
    }

    memset(mbim_call_info.pkt_info, 0, sizeof(rmnet_mbim_pkt_info_type));

    /*Allocate ndp short buf in mbim pkt info*/
    mbim_call_info.pkt_info->ndp_short_buf =
        qmi_svc_ps_system_heap_mem_alloc(PS_OPT_PATH_MAX_ST_NDP_LEN,
                            FILE_ID_DS_RMNET_META_SM,__LINE__);
    if (mbim_call_info.pkt_info->ndp_short_buf == NULL)
    {
      return -1;
    }

    memset(mbim_call_info.pkt_info->ndp_short_buf, 0,
           PS_OPT_PATH_MAX_ST_NDP_LEN);
  }
  else
  {
    if(mbim_call_info.call_cnt == 1)
    {
      /*Already allocated. Just memset individual members as we would have
       * allocated ndp short buff and dont want to lose the ptr by doing a
       * direct memset(mbim_pkt_info).
       * We want to reset outstanding pkt info only when all calls are
       * bought down and the first call is bought up next.
       * We dont need to memset this for every subsequent call since there could
       * be a valid outstanding pkt to be processed from the previous active
       * MBIM call.*/

      LOG_MSG_INFO1_0("Resetting mbim pkt info,call_cnt=1");

      mbim_call_info.pkt_info->ndp_datagram_offset = 0;

      if(mbim_call_info.pkt_info->ntb_dsm_item != NULL)
      {
        dsm_free_packet(&mbim_call_info.pkt_info->ntb_dsm_item);
      }

      mbim_call_info.pkt_info->ntb_dsm_item        = NULL;

      if(mbim_call_info.rx_pkt != NULL)
      {
        dsm_free_packet(&mbim_call_info.rx_pkt);
      }

      mbim_call_info.rx_pkt                        = NULL;

      mbim_call_info.pkt_info->ndp_offset          = 0;
      memset(&mbim_call_info.pkt_info->nth_hdr,0,
               sizeof(ps_iface_mbim_ntb_nth_hdr));
      mbim_call_info.pkt_info->ndp_hdr             = NULL;
      mbim_call_info.pkt_info->ndp_long_buf        = NULL;
      memset(mbim_call_info.pkt_info->ndp_short_buf, 0,
           PS_OPT_PATH_MAX_ST_NDP_LEN);
      mbim_call_info.dual_ip_info = NULL;
    }
  }

  return 0;
}/*rmnet_meta_smi_init_mbim_outstanding_info()*/

/*===========================================================================
FUNCTION DS_RMNET_META_SMI_GET_MBIM_PORT_INDEX

DESCRIPTION
  This function returns the mbim port array index matching the ips_id

PARAMETERS
  info             :  rmnet instance
  ips_id           :  ips_id in the ndp hdr
  port_arr_index   :  OUT parameter containing the arr index


RETURN VALUE
  TRUE  :
  FALSE :

DEPENDENCIES
  None

SIDE EFFECTS
  None
===========================================================================*/

boolean ds_rmnet_meta_smi_get_mbim_port_index
(
  rmnet_smi_info_type * info,
  uint8                 ips_id ,
  uint8               * port_arr_index
)
{
  uint8 i;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if(info == NULL)
  {
    for(i = 0 ; i < MBIM_IP_SESSION_MAX; i++)
    {
      if (mbim_call_info.mbim_port_info[i].ips_id == ips_id)
        break;
    }
  }
  else
  {
    for(i = 0 ; i < MBIM_IP_SESSION_MAX; i++)
    {
      if ((mbim_call_info.mbim_port_info[i].ips_id == ips_id) &&
          (mbim_call_info.mbim_port_info[i].info == info)
         )
        break;
    }
  }

  if(MBIM_IP_SESSION_MAX == i)
  {
    return FALSE;
  }

  *port_arr_index = i;

  return TRUE;

}/*ds_rmnet_meta_smi_get_mbim_port_index()*/

/*===========================================================================
FUNCTION      RMNET_META_SMI_MBIM_WMK_NON_EMPTY_CB_FUNC

DESCRIPTION
  Invoked when the first pkt is enqueued to MBIM Wmk.

PARAMETERS
  mbim_wm_ptr : MBIM wmk ptr
  user_data_ptr : mbim_call_info ptr

DEPENDENCIES
  None

 RETURN VALUE
  None

 SIDE EFFECTS
  None

===========================================================================*/
static void rmnet_meta_smi_mbim_wmk_non_empty_cb_func
(
  struct dsm_watermark_type_s * mbim_wm_ptr,
  void                        * user_data_ptr
)
{
  rmnet_mbim_call_info_type *  info = NULL;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_INFO1_0 ("In rmnet_meta_smi_mbim_wmk_non_empty_cb_func");

  info = (rmnet_mbim_call_info_type *) user_data_ptr;
  RMNET_SET_SIGNAL(info->mbim_rx_sig);

}/*rmnet_meta_smi_mbim_wmk_non_empty_cb_func()*/

/*===========================================================================
  FUNCTION RMNET_META_SMI_MBIM_CALL_CLEANUP

  DESCRIPTION
  Cleanup of MBIM call info related data.

  PARAMETERS
  info - rmnet_smi_dual_ip_info_type

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void rmnet_meta_smi_mbim_call_cleanup
(
    rmnet_smi_info_type *  info,
    uint8                  ips_id_cleanup
)
{
  uint8 rmnet_master_port_inst          = 0;
  rmnet_mbim_port_info_type * port_info = NULL;
  rmnet_smi_info_type       * rm_info   = NULL;
  uint8 port_arr_index;
  uint8 i,ips_id;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ips_id   = ips_id_cleanup;

  if (ds_rmnet_meta_smi_get_mbim_port_index(info, ips_id , &port_arr_index) == FALSE)
  {
    LOG_MSG_ERROR_0("MBIM Call cleanup failed as no matching instance was found");
    return;
  }
  port_info = &mbim_call_info.mbim_port_info[port_arr_index];

  LOG_MSG_INFO1_2 ("In rmnet_meta_smi_mbim_call_cleanup():ips_id %d ,"
                   "rmnet_inst %d",
                   ips_id, RMNET_META_SM_INFO_TO_INSTANCE(info));

  //port_info =
  //    &mbim_call_info.mbim_port_info[mbim_call_info.ips_id_index_map[ips_id]];

   mbim_call_info.call_cnt--;

   port_info->qmi_port_inst = QMI_INSTANCE_MAX;
   port_info->ips_id        = -1;
   port_info->info          = NULL;

   //mbim_call_info.ips_id_index_map[ips_id] = -1;

   /*-------------------------------------------------------------------------
      Do cleanup, if Last MBIM call to be bought

      1. Cleanup Legacy watermark queues and deregister non_empty CB
      2. Clear Outstanding MBIM pkt info and the pkt.
         Free ntb_dsm_item, ndp_short_buf,pkt_info
      3. Release signals.
    -------------------------------------------------------------------------*/
    if(mbim_call_info.call_cnt == 0)
    {

       LOG_MSG_INFO1_0 ("Call_cnt 0");

      /*1.MBIM Wmk*/

      rmnet_master_port_inst = rmnet_sio_get_master_inst();
      if (rmnet_master_port_inst >= RMNET_INSTANCE_MAX)
      {
        LOG_MSG_ERROR_1("Invalid Master Port Inst: %d", rmnet_master_port_inst );
        ASSERT(0);
        return;
      }

      rm_info              = &rmnet_smi_info[rmnet_master_port_inst];
      rmnet_sio_set_rx_cb( rm_info->meta_sm.rmnet_sio_handle, NULL,
                          (void*)&mbim_call_info );

      /*2.Release Rm Signal*/

      for( i = 0; i < RMNET_IFACE_MAX; i++ )
      {
        if(rmnet_signals[i].sig == mbim_call_info.mbim_rx_sig)
        {
          rmnet_signals[i].in_use = FALSE;
          break;
        }
      }
      mbim_call_info.mbim_rx_sig = RMNET_MAX_SIGNALS;

    }/*If:last MBIM call*/

}/*rmnet_meta_smi_mbim_call_cleanup()*/

#endif/*FEATURE_DATA_MBIM*/

/*===========================================================================
FUNCTION      RMNET_META_SMI_LEGACY_WMK_LOW_CB_FUNC

DESCRIPTION
  Invoked when a pkt is enqueued to Legacy Wmk.

PARAMETERS
  None

DEPENDENCIES
  None

 RETURN VALUE
  None

 SIDE EFFECTS
  None

===========================================================================*/
static void rmnet_meta_smi_legacy_wmk_low_cb_func
(
  struct dsm_watermark_type_s * legacy_wm_ptr,
  void                        * user_data_ptr
)
{
  rmnet_smi_dual_ip_info_type *  info = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_INFO1_0 ("In rmnet_meta_smi_legacy_wmk_low_cb_func");

  info = (rmnet_smi_dual_ip_info_type *) user_data_ptr;

  /*Enable flow and set PS_RM Signal*/
  info->legacy_flow_enabled = TRUE;
  RMNET_SET_SIGNAL( info->rx_sig );

}/*rmnet_meta_smi_legacy_wmk_low_cb_func()*/

/*===========================================================================
FUNCTION      RMNET_META_SMI_LEGACY_WMK_HIGH_CB_FUNC

DESCRIPTION
  Invoked when a pkt is enqueued to Legacy Wmk.

PARAMETERS
  None

DEPENDENCIES
  None

 RETURN VALUE
  None

 SIDE EFFECTS
  None

===========================================================================*/
static void rmnet_meta_smi_legacy_wmk_high_cb_func
(
  struct dsm_watermark_type_s * legacy_wm_ptr,
  void                        * user_data_ptr
)
{
  rmnet_smi_dual_ip_info_type *  info = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_INFO1_0 ("In rmnet_meta_smi_legacy_wmk_high_cb_func");

  info = (rmnet_smi_dual_ip_info_type *) user_data_ptr;

  info->legacy_flow_enabled = FALSE;

}/*rmnet_meta_smi_legacy_wmk_high_cb_func()*/

/*===========================================================================
  FUNCTION RMNET_META_SM_SETUP_LLC_WATERMARKS()

  DESCRIPTION
    Initialize the recieve and transmit watermarks for the llc instance

  PARAMETERS
    rmnet_smi_info_type *  info - rmnet meta sm info

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/

static void rmnet_meta_sm_setup_llc_watermarks
(
  rmnet_smi_info_type *  info
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(info);

  dsm_queue_init( &info->llc_rx_wmk,
                  RMNET_LLC_RX_WM_DNE,
                  &info->llc_rx_wmk_q );

  /*-------------------------------------------------------------------------
                             SETUP LLC RX WATERMARK
  -------------------------------------------------------------------------*/

  /*-------------------------------------------------------------------------
    Network interface device is Ethernet-over-USB, i.e. fast.
    Therefore, set low, high, and dont exceed limits accordingly.
    Standard Ethernet frame is 1514 bytes.
  -------------------------------------------------------------------------*/
  info->llc_rx_wmk.lo_watermark        = RMNET_LLC_RX_WM_LOW;
  info->llc_rx_wmk.hi_watermark        = RMNET_LLC_RX_WM_HIGH;
  info->llc_rx_wmk.dont_exceed_cnt     = RMNET_LLC_RX_WM_DNE;

  info->llc_rx_wmk.lowater_func_ptr      = NULL;
  info->llc_rx_wmk.lowater_func_data     = NULL;
  info->llc_rx_wmk.hiwater_func_ptr      = NULL;
  info->llc_rx_wmk.hiwater_func_data     = NULL;
  info->llc_rx_wmk.gone_empty_func_ptr   = NULL;
  info->llc_rx_wmk.non_empty_func_ptr    = NULL;
  info->llc_rx_wmk.non_empty_func_data   = NULL;
  info->llc_rx_wmk.each_enqueue_func_ptr = NULL;

#ifdef FEATURE_DSM_MEM_CHK
  /*-------------------------------------------------------------------------
    Reset the total_rcvd_cnt, as a packet data call can really set this
    number very high.
  -------------------------------------------------------------------------*/
  info->llc_rx_wmk.total_rcvd_cnt        = 0;
#endif /* FEATURE_DSM_MEM_CHK */
} /* rmnet_meta_sm_setup_llc_watermarks() */

/*===========================================================================
  FUNCTION RMNET_META_SM_OPEN_INSTANCE_HDLR

  DESCRIPTION
    Open the specified RmNet instance. This function runs in DCC task and posts
    the result to QMUX that runs in qmi_modem task.

  PARAMETERS
    instance : instance of RmNET to open
    port_id :  SIO port identifier

  RETURN VALUE
    void

  SIDE EFFECTS
    none
===========================================================================*/
void  rmnet_meta_sm_open_instance_hdlr
(
  rmnet_instance_e_type  instance,
  uint16                 port_id
)
{
  boolean                open_result = FALSE;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT( RMNET_INSTANCE_MAX > instance );
  if(((instance/2) >= (RMNET_INSTANCE_MAX/2)) || (instance >= (RMNET_INSTANCE_MAX-1)))
  {
    LOG_MSG_ERROR_1( "RmNet instance (%d) is invalid \n", (uint32)instance );
    return;
  }
  //group 2 rmnet instances
  rmnet_smi_dual_ip_info[instance/2].info_v4 = &rmnet_smi_info[ instance ];
  rmnet_smi_dual_ip_info[instance/2].info_v6 = &rmnet_smi_info[ instance + 1 ];

  //open port, init sio info
  if ( !rmnet_meta_sm_open_instance(instance, port_id) )
  {
    LOG_MSG_INFO1_1( "Could not activate RmNet instance %d\n",
                     (uint32)instance );
    open_result = FALSE;
  }
  else
  {
    // assign sio info
    rmnet_smi_dual_ip_info[instance/2].info_v6->meta_sm.rmnet_sio_handle =
      rmnet_smi_dual_ip_info[instance/2].info_v4->meta_sm.rmnet_sio_handle;
    rmnet_smi_dual_ip_info[instance/2].info_v6->meta_sm.ports_active = TRUE;

    rmnet_smi_dual_ip_info[instance/2].info_v4->has_flow = FALSE;
    rmnet_smi_dual_ip_info[instance/2].info_v6->has_flow = FALSE;
    rmnet_smi_dual_ip_info[instance/2].rx_flow_count = 0;

    rmnet_smi_dual_ip_info[instance/2].info_v4->constants.rx_sig = RMNET_MAX_SIGNALS;
    rmnet_smi_dual_ip_info[instance/2].info_v6->constants.rx_sig = RMNET_MAX_SIGNALS;
    rmnet_smi_dual_ip_info[instance/2].rx_sig = RMNET_MAX_SIGNALS;

    rmnet_meta_sm_setup_llc_watermarks(rmnet_smi_dual_ip_info[instance/2].info_v4);
    rmnet_meta_sm_setup_llc_watermarks(rmnet_smi_dual_ip_info[instance/2].info_v6);

    /*-------------------------------------------------------------------------
      Init Outstanding MBIM pkt info ptr to NULL. This is required as we first
      have a NULL check before allocating when call is being bought up.
    -------------------------------------------------------------------------*/
    rmnet_smi_dual_ip_info[instance/2].mbim_pkt_info = NULL;
    rmnet_smi_dual_ip_info[instance/2].rx_pkt        = NULL;

    /*-------------------------------------------------------------------------
      Setup Legacy Watermarks, Init Signal and enable flow for legacy packets
    -------------------------------------------------------------------------*/
    rmnet_meta_smi_setup_legacy_watermarks(&rmnet_smi_dual_ip_info[instance/2]);
    rmnet_smi_dual_ip_info[instance/2].legacy_sig = RMNET_LEGACY_MAX_SIGNAL;
    rmnet_smi_dual_ip_info[instance/2].legacy_flow_enabled = TRUE;

    open_result = TRUE;
  }

  qmux_rmnet_inst_open_close_result( open_result,
                                     QMI_CMD_RMNET_OPEN_INSTANCE,
                                     (qmi_instance_e_type)(instance/2),
                                     port_id);
} /* rmnet_meta_sm_open_instance_hdlr() */

/*===========================================================================
  FUNCTION RMNET_META_SM_CLOSE_INSTANCES_HDLR

  DESCRIPTION
    Close the specified RmNet instance. This function runs in DCC task and posts
    the result to QMUX that runs in qmi_modem task.

  PARAMETERS
    instance : instance of RmNet to close

  RETURN VALUE
    void

  SIDE EFFECTS
    none
===========================================================================*/
void  rmnet_meta_sm_close_instance_hdlr
(
  rmnet_instance_e_type  instance
)
{
  rmnet_smi_info_type *  info;
  boolean                close_result = FALSE;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  info = &rmnet_smi_info[ instance ];

  if( info->meta_sm.ports_active == FALSE )
  {
    LOG_MSG_ERROR_1( "RmNet instance (%d) that was to be closed is not active!\n",
                     (uint32)instance );
    ASSERT(0);
  }

  if( !rmnet_sio_deactivate_instance( info->meta_sm.rmnet_sio_handle ) )
  {
    LOG_MSG_INFO1_1( "Could not deactivate RmNet instance %d\n",
                     (uint32)instance );
    close_result = FALSE;
  }
  else
  {
    info->meta_sm.ports_active = FALSE;
    close_result = TRUE;
  }

  qmux_rmnet_inst_open_close_result( close_result,
                                     QMI_CMD_RMNET_CLOSE_INSTANCE,
                                     (qmi_instance_e_type)(instance/2),
                                     0);


} /* rmnet_meta_sm_close_instance_hdlr() */

/*===========================================================================
  FUNCTION RMNET_META_SM_OPEN_DUAL_IP_INSTANCE

  DESCRIPTION
    Post a command to DCC task to open port for the specified RmNet instance.

  PARAMETERS
    instance : instance of RmNET to open
    port_id :  SIO port identifier

  DEPENDENCIES
    None

  RETURN VALUE
    TRUE on success
    FALSE otherwise

  SIDE EFFECTS
    None
===========================================================================*/
boolean rmnet_meta_sm_open_dual_ip_instance
(
  rmnet_instance_e_type  instance,
  uint16                 port_id
)
{

  rmnet_inst_open_close_req_type * cmd_ptr;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT( RMNET_INSTANCE_MAX > instance );

  /*-------------------------------------------------------------------------
    Post a cmd to QMI (DCC task)
  -------------------------------------------------------------------------*/
  cmd_ptr = (rmnet_inst_open_close_req_type *)qmi_svc_ps_system_heap_mem_alloc(
                                     sizeof(rmnet_inst_open_close_req_type),
                                     FILE_ID_DS_RMNET_META_SM,__LINE__);
  if( cmd_ptr == NULL)
  {
    ASSERT (0);
    return FALSE;
  }

  cmd_ptr->cmd  = RMNET_CMD_OPEN_INSTANCE;
  cmd_ptr->instance  = instance;
  cmd_ptr->port_id   = port_id;
  dcc_send_cmd_ex(DCC_RMNET_INST_OPEN_CLOSE_CMD, (void *)cmd_ptr);

  return TRUE;
} /* rmnet_meta_sm_open_dual_ip_instance() */

/*===========================================================================
  FUNCTION RMNET_META_SM_CLOSE_INSTANCE

  DESCRIPTION
    Post a command to DCC task to close port for the specified RmNet instance.

  PARAMETERS
    instance : instance of RmNET to open

  DEPENDENCIES
    None

  RETURN VALUE
    TRUE on success
    FALSE otherwise

  SIDE EFFECTS
    None
===========================================================================*/
boolean rmnet_meta_sm_close_instance
(
  rmnet_instance_e_type  instance
)
{
  rmnet_inst_open_close_req_type * cmd_ptr;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT( RMNET_INSTANCE_MAX > instance );

  /*-------------------------------------------------------------------------
    Post a cmd to QMI (DCC task)
  -------------------------------------------------------------------------*/
  cmd_ptr = (rmnet_inst_open_close_req_type *)qmi_svc_ps_system_heap_mem_alloc(
                                                  sizeof(rmnet_inst_open_close_req_type)
                                                  ,FILE_ID_DS_RMNET_META_SM,__LINE__);
  if( cmd_ptr == NULL)
  {
    ASSERT (0);
    return FALSE;
  }

  cmd_ptr->cmd  = RMNET_CMD_CLOSE_INSTANCE;
  cmd_ptr->instance  = instance;
  dcc_send_cmd_ex(DCC_RMNET_INST_OPEN_CLOSE_CMD, (void *)cmd_ptr);

  return TRUE;
} /* rmnet_meta_sm_close_instance() */



/*===========================================================================
FUNCTION RMNET_IFACE_IOCTL_HDLR

DESCRIPTION
  iface ioctl Callback

PARAMETERS
  *iface_ptr        - Ptr to the interface to operate on
  ioctl_name        - Type of request
  argval_ptr        - QoS specified by the application
  *ps_errno         - Ptr to error code (if any)

DEPENDENCIES
  None.

RETURN VALUE
  0  - On Success
  -1 - On Failure

SIDE EFFECTS

===========================================================================*/
LOCAL int rmnet_iface_ioctl_hdlr
(
  ps_iface_type            *iface_ptr,
  ps_iface_ioctl_type      ioctl_name,
  void                     *argval_ptr,
  sint15                   *ps_errno
)
{
  int ret_val;
  rmnet_instance_e_type  i;
  sio_port_id_type * output;
  rmnet_sm_link_prot_e_type* link_prot_output;
  ps_iface_ioctl_rmnet_watermarks_type * wmk_output;

  lan_lle_enum_type         lan_llc_instance = LAN_LLE_MIN;
  ps_ip_addr_type           ip_addr;
  ps_iface_type            *bridge_iface_ptr = NULL;
  //lan_llc_start_info_type  *ptr_start_info;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ret_val = -1;

  if(argval_ptr == NULL || iface_ptr == NULL || ps_errno == NULL)
  {
    LOG_MSG_ERROR_0("IOCTL arguments invalid. Return -1");
    return ret_val;
  }
  memset(&ip_addr, 0, sizeof(ps_ip_addr_type));
  memset(&lan_llc_instance, 0, sizeof(lan_lle_enum_type));

  LOG_MSG_INFO3_1("processing IOCTL (%d)",ioctl_name);
  switch ( ioctl_name )
  {
    case PS_IFACE_IOCTL_RM_GET_SIO_PORT:
      output = (sio_port_id_type *) argval_ptr;
      for (i = RMNET_INSTANCE_MIN; i < RMNET_INSTANCE_MAX; i++) {
        if (rmnet_smi_info[i].rmnet_iface_ptr == iface_ptr)
        {
          *output =
            rmnet_sio_get_port ( rmnet_smi_info[i].meta_sm.rmnet_sio_handle );
          ret_val = 0;
          break;//exit the loop
        }
      }
      break;

    case PS_IFACE_IOCTL_RMNET_GET_LINK_PROTO_TYPE:
      link_prot_output = (rmnet_sm_link_prot_e_type*) argval_ptr;
      for (i = RMNET_INSTANCE_MIN; i < RMNET_INSTANCE_MAX; i++) {
        if (rmnet_smi_info[i].rmnet_iface_ptr == iface_ptr)
        {
          *link_prot_output = rmnet_smi_info[i].link_prot;
          ret_val = 0;
          break;//exit the loop
        }
      }
      break;

    case PS_IFACE_IOCTL_RMNET_GET_IPV4_DEST_MAC_ADDR:
      for (i = RMNET_INSTANCE_MIN; i < RMNET_INSTANCE_MAX; i++) {
        if (rmnet_smi_info[i].rmnet_iface_ptr == iface_ptr)
        {
          lan_llc_instance = rmnet_smi_info[i].constants.lan_llc_instance;
          break;//exit the loop
        }
      }
      if(lan_llc_instance == LAN_LLE_MIN)
      {
        LOG_MSG_INFO1_1("Could not get LAN LLC instance from RmNet Iface. IOCTL: %d",
                        ioctl_name);
        ret_val = -1;
      }
      else
      {
        ip_addr.type = IPV4_ADDR;
        ps_iface_get_cached_addr( ps_iface_bridge_iface(iface_ptr), &ip_addr );

        ret_val = arp_get_dest_mac_addr(lan_llc_instance,
                                        &ip_addr,
                                        (ps_iface_ioctl_mac_addr_type)
                                        argval_ptr,
                                        LAN_IEEE_MAC_ADDR_LEN);
      }
      break;

    case PS_IFACE_IOCTL_RMNET_GET_IPV6_DEST_MAC_ADDR:
      ip_addr.type = IPV6_ADDR;
      bridge_iface_ptr = ps_iface_bridge_iface(iface_ptr);
      if(bridge_iface_ptr == NULL)
      {
        LOG_MSG_ERROR_0("bridge_iface_ptr returned as NULL");
        ret_val = -1;
        break;
      }

      if (ps_iface_get_ipv6_ext_addr(bridge_iface_ptr, &ip_addr) == FALSE)
      {
         LOG_MSG_ERROR_1("Could not get the ipv6 address. Fail IOCTL: %d",
                         ioctl_name);
         ret_val = -1;
         break;
      }
      else
      {
        ret_val = ps_icmp6_get_dest_mac_addr (
                                iface_ptr,
                                &(ip_addr.addr.v6),
                                (ps_iface_ioctl_mac_addr_type)argval_ptr,
                                PS_IFACE_MAX_HW_ADDR_LEN
                               );
      }
      break;

    case PS_IFACE_IOCTL_RMNET_IS_TETHERED_CALL:
      {
        uint32 tmp_var;
        sio_port_id_type sio_port;
        uint8  instance_idx = 0;

        ps_iface_ioctl_rmnet_is_tethered_call_type *result =
          (ps_iface_ioctl_rmnet_is_tethered_call_type *)argval_ptr;
        rmnet_smi_info_type *info =
         (rmnet_smi_info_type*)iface_ptr->iface_private.tx_cmd_info;

        if ((uint32)info < LAN_LLE_MAX)
        {
          for (instance_idx = 0; instance_idx < RMNET_INSTANCE_MAX; instance_idx++)
          {
            if (rmnet_smi_info[instance_idx].rmnet_iface_ptr == iface_ptr)
            {
              /* RmNet in eth mode, info wont work */
              lan_llc_instance = (lan_lle_enum_type)(uint32)rmnet_smi_info[instance_idx].constants.lan_llc_instance;
              if((lan_llc_instance <= LAN_LLE_MIN) || (lan_llc_instance >= LAN_LLE_MAX))
              {
                LOG_MSG_INFO1_1("Could not get LAN LLC instance from RmNet Iface. IOCTL: %d",
                                ioctl_name);
                ret_val = -1;
              }
              else
              {
                info = (rmnet_smi_info_type *)&rmnet_smi_info[instance_idx];
              }
              break;
            }
          }
        }

        if (NULL == info || instance_idx == RMNET_INSTANCE_MAX)
        {
          LOG_MSG_ERROR_2("Couldnt find matching rmnet instance %d or info NULL 0x%x",
                          instance_idx, info);
          ret_val = -1;
          break;
        }

        sio_port = rmnet_sio_get_port(info->meta_sm.rmnet_sio_handle);
        *result = rmnet_meta_smi_find_proc_id(&tmp_var, sio_port,
                                   RMNET_META_SM_INFO_TO_INSTANCE(info));
        
        /* if call is embedded then its not tethered, so we should return false */
        if(RMNET_PROC_ID_APPS1 == tmp_var)
        {
          *result = FALSE;
        }

        LOG_MSG_INFO1_1("Returning is_tethered IOCTL result as %d", *result);
        ret_val = 0;
      }
      break;

    case PS_IFACE_IOCTL_RMNET_SET_SOFTAP_MODE:
      {
        uint8  instance_idx = 0;
        ps_iface_ioctl_rmnet_softap_mode_type *softap_mode =
         (ps_iface_ioctl_rmnet_softap_mode_type *)argval_ptr;
        rmnet_smi_info_type *info =
         (rmnet_smi_info_type*)iface_ptr->iface_private.tx_cmd_info;

        if ((uint32)info < LAN_LLE_MAX)
        {
          for (instance_idx = 0; instance_idx < RMNET_INSTANCE_MAX; instance_idx++)
          {
            if (rmnet_smi_info[instance_idx].rmnet_iface_ptr == iface_ptr)
            {
              /* RmNet in eth mode, info wont work */
              lan_llc_instance = (lan_lle_enum_type)(uint32)rmnet_smi_info[instance_idx].constants.lan_llc_instance;
              if((lan_llc_instance <= LAN_LLE_MIN) || (lan_llc_instance >= LAN_LLE_MAX))
              {
                LOG_MSG_INFO1_1("Could not get LAN LLC instance from RmNet Iface. IOCTL: %d",
                                ioctl_name);
                ret_val = -1;
              }
              else
              {
                info = (rmnet_smi_info_type *)&rmnet_smi_info[instance_idx];
              }
              break;
            }
          }
        }

        if (instance_idx == RMNET_INSTANCE_MAX || NULL == info)
        {
          LOG_MSG_ERROR_2("Couldnt find matching rmnet instance %d or info NULL 0x%x",
                          instance_idx, info);
          break;
        }

        LOG_MSG_INFO1_2("Setting softap mode for RmNet instance %d as %d",
                        RMNET_META_SM_INFO_TO_INSTANCE(info), *softap_mode);
        info->meta_sm.softap_mode = *softap_mode;

        /*-------------------------------------------------------------------
           For SoftAP IPv6 LAN setup happens without WWAN.
           Move the RMNET V6 state machine which is waiting for RA event.
         ------------------------------------------------------------------*/
        if(ps_iface_addr_family_is_v6(info->um_iface_ptr))
        {
          LOG_MSG_INFO1_1("SoftAP IPv6 on RmNet instance %d",
                          RMNET_META_SM_INFO_TO_INSTANCE(info));
          rmnet_v6_sm_post_event(RMNET_META_SM_INFO_TO_INSTANCE(info),
                                 RMNET_V6_SM_RA_SENT_EV);

          /*-------------------------------------------------------------------
            For SoftAP IPV6 LAN to be completely up in ethernet mode we need
            Link Layer address also to be resolved for the RMNET V6 state
            machine to be routeable.
          -------------------------------------------------------------------*/
          if(info->link_prot == RMNET_ETHERNET_MODE)
          {
            LOG_MSG_INFO1_0( "Softap ethernet mode:posting RMNET_V6_SM_LL_ADDR_RESOL_EV" );
            rmnet_v6_sm_post_event( RMNET_META_SM_INFO_TO_INSTANCE(info),
                                    RMNET_V6_SM_LL_ADDR_RESOL_EV );
          }
        }

        ret_val = 0;
      }
      break;

    case PS_IFACE_IOCTL_RMNET_GET_WATERMARKS:
      wmk_output = (ps_iface_ioctl_rmnet_watermarks_type *) argval_ptr;
      for (i = RMNET_INSTANCE_MIN; i < RMNET_INSTANCE_MAX; i++)
      {
        if (rmnet_smi_info[i].rmnet_iface_ptr == iface_ptr)
        {
          ret_val = rmnet_sio_get_wmk_queues(
                        rmnet_smi_info[i].meta_sm.rmnet_sio_handle,
                        (dsm_watermark_type **)&wmk_output->rx_wmk,
                        (dsm_watermark_type **)&wmk_output->tx_wmk);
          break; //exit the loop
        }
      }
      *ps_errno = (ret_val == 0) ? 0 : DS_EINVAL;
      break;

    default:
      LOG_MSG_ERROR_1("Invalid request from IOCTL: %d", ioctl_name);

      *ps_errno =  DS_EINVAL;
      ret_val = -1;
      break;
  }

  return ret_val;

}

/*===========================================================================
  FUNCTION RMNET_META_SMI_LOG_WMK_LEVELS

  DESCRIPTION
    Called when PHYSLINK UP/DOWN event occurs. This function logs Watermark
    counts as DIAG events.

  PARAMETERS
    rmnet_instance   : Rmnet instance.
    um_phys_link_ptr : Pointer to the physlink
    dormancy_status  : Values: 1 - Dormant 2- Active

  RETURN VALUE
    none

  SIDE EFFECTS
    none
===========================================================================*/
static void rmnet_meta_smi_log_wmk_levels
(
   rmnet_instance_e_type           rmnet_instance,
   ps_phys_link_type              *um_phys_link_ptr,
   ps_wmk_logging_dorm_state_type dormancy_status
)
{
  rmnet_smi_info_type   *info;
  int16                  retval;
  dsm_watermark_type    *rx_wmk = NULL;
  dsm_watermark_type    *tx_wmk = NULL;

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(rmnet_instance < RMNET_INSTANCE_MAX);

  if(um_phys_link_ptr == NULL)
  {
     LOG_MSG_ERROR_0("rmnet_meta_smi_log_wmk_levels: Null physlink pointer" );
     return ;
  }

  info = &rmnet_smi_info[rmnet_instance];
  retval = rmnet_sio_get_wmk_queues(info->meta_sm.rmnet_sio_handle,
                                    &rx_wmk, &tx_wmk);
  if(retval != 0)
  {
    LOG_MSG_ERROR_1("Cannot get wmk queues from RMNET SIO instance %d ",
                    rmnet_instance);
    return;
  }

  /*---------------------------------------------------------------------
    Generate DIAG events with watermark information
  ---------------------------------------------------------------------*/
  if(-1 == ps_wmk_logging_event_report(um_phys_link_ptr,
                                 EVENT_DS_DORMANCY_STATUS_RM_QUEUE_STATS,
                                 dormancy_status,
                                 rx_wmk->current_cnt,
                                 tx_wmk->current_cnt))
  {
    LOG_MSG_ERROR_0("Cannot log Rm watermark counts");
  }

  return;

} /* rmnet_meta_smi_log_wmk_levels() */

#ifdef FEATURE_QXDM_WM_TRACING
/*===========================================================================
  FUNCTION RMNET_META_SMI_FC_GET_WM_TRACE_CB

  DESCRIPTION
    Callback called by FC module to query for the RMNET SIO wmk statistics

  PARAMETERS
    user_data   : uint32 handle containing the RMNET SM info pointer.
    wm_stat_ptr : pointer to the Watermark statistics.

  RETURN VALUE
    none

  SIDE EFFECTS
    none
===========================================================================*/
static void rmnet_meta_smi_fc_get_wm_trace_cb
(
  uint32            user_data,
  fc_wm_stat_type  *wm_stat_ptr
)
{
  rmnet_smi_info_type   *info;
  dsm_watermark_type    *rx_wmk = NULL;
  dsm_watermark_type    *tx_wmk = NULL;
  int16                  retval;
  uint8                  wm_index;

  /*-------------------------------------------------------------------------
    There should be space for wmk stats.
  -------------------------------------------------------------------------*/
  wm_index = wm_stat_ptr->num_wms;
  if(wm_index >= MAX_WM_CNT)
  {
    LOG_MSG_ERROR_0("No space in the wmk stats buffer");
    return;
  }

  info = (rmnet_smi_info_type*)user_data;

  retval = rmnet_sio_get_wmk_queues(info->meta_sm.rmnet_sio_handle,
                                    &rx_wmk, &tx_wmk);

  if(retval != 0)
  {
    LOG_MSG_ERROR_0("Cannot get the wmk queues from RMNET SIO");
    return;
  }

  /*-------------------------------------------------------------------------
    Get the Rx Wmk stats.
  -------------------------------------------------------------------------*/
  dsm_get_wm_stats(rx_wmk,
                   &(wm_stat_ptr->wm_info[wm_index].wm_counts));
  wm_stat_ptr->wm_info[wm_index].tech_id  = UNKNOWN_TECH;
  wm_stat_ptr->wm_info[wm_index].call_id  = info->call_id;
  wm_stat_ptr->wm_info[wm_index].wm_id    = WM_DIR_RMNET_SIO_UL;
  wm_index++;

  if(wm_index >= MAX_WM_CNT)
  {
    wm_stat_ptr->num_wms = wm_index;
    LOG_MSG_ERROR_0("No space in the wmk stats buffer");
    return;
  }
  /*-------------------------------------------------------------------------
    Get the Tx Wmk stats.
  -------------------------------------------------------------------------*/
  dsm_get_wm_stats(tx_wmk,
                   &(wm_stat_ptr->wm_info[wm_index].wm_counts));
  wm_stat_ptr->wm_info[wm_index].tech_id  = UNKNOWN_TECH;
  wm_stat_ptr->wm_info[wm_index].call_id  = info->call_id;
  wm_stat_ptr->wm_info[wm_index].wm_id    = WM_DIR_RMNET_SIO_DL;
  wm_index++;

  wm_stat_ptr->num_wms = wm_index;

  return;
} /* rmnet_meta_smi_fc_get_wm_trace_cb() */
#endif /* FEATURE_QXDM_WM_TRACING */

/*===========================================================================
  FUNCTION RMNET_META_SMI_UPDATE_PHYSLINK()

  DESCRIPTION
    Updates cached physlink information.

  PARAMETERS
    instance - the RmSM instance

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void rmnet_meta_smi_update_physlink
(
  rmnet_instance_e_type  instance
)
{
  rmnet_smi_info_type *  info;
  ps_phys_link_type *new_phys_link_ptr = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(instance < RMNET_INSTANCE_MAX);

  info = &rmnet_smi_info[instance];

  new_phys_link_ptr = PS_IFACE_GET_PHYS_LINK(info->um_iface_ptr);

  if( NULL != info->meta_sm.um_iface_phys_link_ptr &&
      NULL != new_phys_link_ptr &&
      new_phys_link_ptr != info->meta_sm.um_iface_phys_link_ptr)
  {
    LOG_MSG_INFO2_2 ("Changing physlink! old physlink (0x%x), new physlink (0x%x)",
                     info->meta_sm.um_iface_phys_link_ptr, new_phys_link_ptr);

    rmnet_meta_smi_dereg_physlink_events(info);
    (void)rmnet_meta_smi_reg_physlink_events(info);
  }

  return;
} /* rmnet_meta_smi_update_physlink */

void rmnet_set_signal(rmnet_sig_enum_type sig)
{
  if( sig < RMNET_MAX_SIGNALS )
  {
    PS_ENTER_CRIT_SECTION(&rmnet_rx_sig_crit_section);
    rmnet_sig_mask |= ( 1 << (sig & 0x1f) );
    PS_LEAVE_CRIT_SECTION(&rmnet_rx_sig_crit_section);
    PS_RM_SET_SIGNAL(PS_RM_RMNET_RX_DATA_Q_SIGNAL);
  }
} /* rmnet_set_ext_signal */

void rmnet_clr_signal(rmnet_sig_enum_type sig)
{
  if( sig < RMNET_MAX_SIGNALS )
  {
    PS_ENTER_CRIT_SECTION(&rmnet_rx_sig_crit_section);
    rmnet_sig_mask &= ~(uint32)( 1 << (sig & 0x1f) );
    PS_LEAVE_CRIT_SECTION(&rmnet_rx_sig_crit_section);
  }
}/* rmnet_clr_ext_signal */

/*===========================================================================
FUNCTION RMNET_SET_LEGACY_SIGNAL

DESCRIPTION
  This function sets the legacy signal

DEPENDENCIES
  None.

RETURN VALUE

SIDE EFFECTS
  None.

===========================================================================*/
void rmnet_set_legacy_signal(rmnet_legacy_sig_enum_type sig)
{
  if( sig < RMNET_LEGACY_MAX_SIGNAL )
  {
    PS_ENTER_CRIT_SECTION(&rmnet_rx_sig_crit_section);
    rmnet_legacy_sig_mask |= ( 1 << (sig & 0x1f) );
    PS_LEAVE_CRIT_SECTION(&rmnet_rx_sig_crit_section);
    PS_SET_SIGNAL(PS_RX_RMNET1_SIGNAL);
  }
} /* rmnet_set_legacy_signal */

/*===========================================================================
FUNCTION RMNET_CLR_LEGACY_SIGNAL

DESCRIPTION
  This function clears the legacy signal

DEPENDENCIES
  None.

RETURN VALUE

SIDE EFFECTS
  None.

===========================================================================*/
void rmnet_clr_legacy_signal(rmnet_legacy_sig_enum_type sig)
{
  if( sig < RMNET_LEGACY_MAX_SIGNAL )
  {
    PS_ENTER_CRIT_SECTION(&rmnet_rx_sig_crit_section);
    rmnet_legacy_sig_mask &= ~(uint32)( 1 << (sig & 0x1f) );
    PS_LEAVE_CRIT_SECTION(&rmnet_rx_sig_crit_section);
  }
}/* rmnet_clr_legacy_signal */
/*===========================================================================

FUNCTION RMNET_SET_SIG_HANDLER()

DESCRIPTION
  This function sets a handler for the specified signal

DEPENDENCIES
  None.

RETURN VALUE
  Previously registered signal handler

SIDE EFFECTS
  None.

===========================================================================*/
rmnet_sig_handler_type rmnet_set_sig_handler
(
  rmnet_sig_enum_type sig,              /* Signal assocaited with handler     */
  rmnet_sig_handler_type sig_handler,   /* Signal handler function pointer    */
  void *user_data_ptr                /* Param to be passed to the handler  */
)
{
  rmnet_sig_handler_type old_handler = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if( sig < RMNET_MAX_SIGNALS )
  {
    ASSERT( sig_handler != NULL );

    old_handler = rmnet_sig_handler[sig].sig_handler;

    rmnet_sig_handler[sig].sig_handler = sig_handler;
    rmnet_sig_handler[sig].user_data_ptr = user_data_ptr;
  }

  return old_handler;
} /* rmnet_set_sig_handler() */

/*===========================================================================

FUNCTION RMNET_SET_LEGACY_SIG_HANDLER()

DESCRIPTION
  This function sets a handler for the specified leagcy signal

DEPENDENCIES
  None.

RETURN VALUE
  Previously registered signal handler

SIDE EFFECTS
  None.

===========================================================================*/
rmnet_legacy_sig_handler_type rmnet_set_legacy_sig_handler
(
  rmnet_legacy_sig_enum_type sig,              /* Signal assocaited with handler     */
  rmnet_legacy_sig_handler_type sig_handler,   /* Signal handler function pointer    */
  void *user_data_ptr                /* Param to be passed to the handler  */
)
{
  rmnet_legacy_sig_handler_type old_handler = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if( sig < RMNET_LEGACY_MAX_SIGNAL )
  {
    ASSERT( sig_handler != NULL );

    old_handler = rmnet_legacy_sig_handler_arr[sig].sig_handler;

    rmnet_legacy_sig_handler_arr[sig].sig_handler = sig_handler;
    rmnet_legacy_sig_handler_arr[sig].user_data_ptr = user_data_ptr;
  }

  return old_handler;
} /* rmnet_set_legacy_sig_handler() */

static boolean rmnet_ext_sig_hdlr
(
  ps_rm_sig_enum_type rx_sig,
  void*               user_data
)
{
  rmnet_sig_enum_type sig;

  for( sig = RMNET_MIN_SIGNAL; sig < RMNET_MAX_SIGNALS; sig++ )
  {
    if( rmnet_sig_mask & (1 << (sig & 0x1f)))
    {
      RMNET_CLR_SIGNAL(sig);
      if ( NULL != rmnet_sig_handler[sig].sig_handler &&
           rmnet_sig_handler[sig].sig_handler
           (
            sig,
            rmnet_sig_handler[sig].user_data_ptr
           ) == FALSE )
      {
        RMNET_SET_SIGNAL(sig);
      }
    }
  }

  if( 0 != rmnet_sig_mask )
  {
    return FALSE;
  }

  return TRUE;
}

/*===========================================================================
  FUNCTION RMNET_EXT_LEGACY_SIG_HDLR

  DESCRIPTION
  External Legacy Sig Hdlr invoked in PS task when signal is set

    PARAMETERS
    sig - PS_RX_RMNET1_SIG
    user_data  - rmnet_dual_ip_info_type

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static boolean rmnet_ext_legacy_sig_hdlr
(
  ps_sig_enum_type rx_sig,
  void*               user_data
)
{
  rmnet_legacy_sig_enum_type sig;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  for( sig = RMNET_LEGACY_MIN_SIGNAL; sig < RMNET_LEGACY_MAX_SIGNAL; sig++ )
  {
    if( rmnet_legacy_sig_mask & (1 << (sig & 0x1f)))
    {
      RMNET_CLR_LEGACY_SIGNAL(sig);
      if ( NULL != rmnet_legacy_sig_handler_arr[sig].sig_handler &&
           rmnet_legacy_sig_handler_arr[sig].sig_handler
           (
            sig,
            rmnet_legacy_sig_handler_arr[sig].user_data_ptr
           ) == FALSE )
      {
        RMNET_SET_LEGACY_SIGNAL(sig);
      }
    }
  }

  if( 0 != rmnet_legacy_sig_mask )
  {
    return FALSE;
  }

  return TRUE;
}

/*===========================================================================
  FUNCTION RMNET_META_SMI_LEGACY_SIG_HDLR_CLEANUP

  DESCRIPTION
  Cleanup of Legacy sig hdlr related variables(Signals, Wmks.)

  PARAMETERS
 info - rmnet_smi_dual_ip_info_type

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void rmnet_meta_smi_legacy_sig_hdlr_cleanup(rmnet_smi_dual_ip_info_type *  info)
{
   uint8 i;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

 LOG_MSG_INFO4_0 ("rmnet_meta_smi_legacy_sig_hdlr_cleanup");

  if(info->legacy_sig != RMNET_LEGACY_MAX_SIGNAL)
  {

    /*-------------------------------------------------------------------------
      Cleanup Legacy watermark queues
    -------------------------------------------------------------------------*/

    dsm_empty_queue(&info->legacy_wmk);
    //dsm_queue_destroy(rmnet_tx_wmk);

    /*-------------------------------------------------------------------------
      Deregister non_empty CB fn for Legacy Wmk.
    -------------------------------------------------------------------------*/
    info->legacy_wmk.non_empty_func_ptr  = NULL;
    info->legacy_wmk.non_empty_func_data = NULL;

    /*-------------------------------------------------------------------------
      Release PS legacy signals - Set Signal in RmNet instance and Signal
      List to FALSE
    -------------------------------------------------------------------------*/
    for( i = 0; i < RMNET_IFACE_MAX; i++ )
    {
      if(rmnet_legacy_signals[i].sig == info->legacy_sig)
      {
        rmnet_legacy_signals[i].in_use = FALSE;
        break;
      }
    }
    info->legacy_sig = RMNET_LEGACY_MAX_SIGNAL;
  }
} /* rmnet_meta_smi_legacy_sig_hdlr_cleanup() */

/*===========================================================================
  FUNCTION RMNET_META_SMI_UPDATE_AUTOCONNECT_STATE()

  DESCRIPTION
  Determine autoconnect state(Enable or disable) based on new setting

  Auto connect state can be determined using this formula.

  auto_connect_state = (autconnect_setting &&
                        roam_setting((home && SS roaming status) || roaming always))

  By default:
    autconnect_setting = 0 (Disabled).
    roam_setting = 0(roaming alwasys)
    SS roaming status = 0(roaming)
    auto_connect_state = FALSE

  For backward compatibility(prior ICD version 3.1):
  The default value would be consider for roam_setting, SS roaming status.
  So auto_connect_state = autconnect_setting;

  PARAMETERS
    instance - the RmNet instance

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void rmnet_meta_smi_update_autoconnect_state
(
  rmnet_instance_e_type  instance
)
{
  rmnet_smi_info_type *  info;
  boolean                auto_connect_state = FALSE;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(instance < RMNET_INSTANCE_MAX);
  info = &rmnet_smi_info[instance];

  /*-------------------------------------------------------------------------
    Finding the auto connect current state.
  -------------------------------------------------------------------------*/
  if( RMNET_AUTOCONNECT_ENABLED == info->meta_sm.settings.autoconnect_setting )
  {
    /* If roam setting is home only and ss roaming status is home
       then set auto_connect_state to true*/
    if ( RMNET_AUTOCONNECT_ROAM_SETTING_HOME_ONLY ==
                  info->meta_sm.settings.autoconnect_roam_setting )
    {
      if(rmnet_smi_cm_info.ss_roaming_status == SYS_ROAM_STATUS_OFF )
      {
        auto_connect_state = TRUE;
      }
      else if((rmnet_smi_eri_roam_ind_info.len  > 0) &&
              (rmnet_smi_eri_roam_ind_info.len  <= RMNET_ROAM_IND_FILE_MAX_LEN))
      {
        uint8 i;
        for(i = 0; i < rmnet_smi_eri_roam_ind_info.len; i++)
        {
          if(rmnet_smi_cm_info.ss_roaming_status ==
                                           rmnet_smi_eri_roam_ind_info.data[i] )
          {
            auto_connect_state = TRUE;
            break;
          }
        }
      }
    }
    /* If roam setting is roaming or known set auto_connect_state is true*/
    else
    {
      auto_connect_state = TRUE;
    }
  }

  /*-------------------------------------------------------------------------
    If autoconnect state has changed then update the cache and set trigger.
  -------------------------------------------------------------------------*/
  if(info->meta_sm.settings.auto_connect_enabled != auto_connect_state)
  {
    LOG_MSG_INFO1_3("Auto_connect_state:%d has changed, Auto_connect_setting:%d, roam_setting:%d",
                    info->meta_sm.settings.auto_connect_enabled,
                    info->meta_sm.settings.autoconnect_setting,
                    info->meta_sm.settings.autoconnect_roam_setting);

    /* update the autoconnect state in cache.*/
    info->meta_sm.settings.auto_connect_enabled = auto_connect_state;

    /* update the autoconnect timer trigger */
    info->meta_sm.auto_connect_trigger =
      info->meta_sm.settings.auto_connect_enabled ? TRUE : FALSE;
  }
} /* rmnet_meta_smi_update_autoconnect_state() */

/*===========================================================================
  FUNCTION RMNET_META_SMI_UPDATE_AUTOCONNECT_ROAM_STATUS()

  DESCRIPTION
    update roaming status whenever SS roaming status changed

  PARAMETERS

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void rmnet_meta_smi_update_autoconnect_roam_status
(
  void
)
{
  rmnet_instance_e_type  i;
  sys_roam_status_e_type roam_status;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /* for autoconnect, the roaming is default */
  roam_status = SYS_ROAM_STATUS_ON;

  /*-------------------------------------------------------------------------
    Find the current SS roaming status .
  -------------------------------------------------------------------------*/
  if(rmnet_smi_cm_info.inited)
  {
    if(SYS_SRV_STATUS_SRV == rmnet_smi_cm_info.ss_info.srv_status )
    {
      roam_status = rmnet_smi_cm_info.ss_info.roam_status;
    }
  }

  /*-------------------------------------------------------------------------
    update the roaming status and auto connect state.
  -------------------------------------------------------------------------*/
  if( rmnet_smi_cm_info.ss_roaming_status != roam_status)
  {
    LOG_MSG_INFO1_2("SS Roaming status has changed g_roaming status:%d, ss roaming_status:%d",
                    rmnet_smi_cm_info.ss_roaming_status, roam_status);

    rmnet_smi_cm_info.ss_roaming_status = roam_status;
    for (i = RMNET_INSTANCE_MIN; i < RMNET_INSTANCE_MAX; i++)
    {
      /* Check RMNET instance should be initialized*/
      if (rmnet_smi_info[i].meta_sm.inited)
      {
        /* Determine autoconnect state(Enable or disable) based on new setting */
        LOG_MSG_INFO1_1("Update new roaming status to Determine autoconnect state:%d",
                        i);
        rmnet_meta_smi_update_autoconnect_state(i);

        /*-----------------------------------------------------------------------------------
        See the below TT for when the auto connect data session should be easblished
        based on various autoconnect related settings.
        ---------------------------------------------------------------------------------
        autconnect_  roam_     SS roaming   Packet Data .
        setting      setting   status       Session
        ---------------------------------------------------------------------------------
          0         0 or 0xFF    x           Not established.
        (default,   (default, (dont care)
         Disable)    roaming)
        ---------------------------------------------------------------------------------
          1         0 or 0xFF   1          Established - immediately,
        (Enable)   (roaming) (Roaming)     after power cycle, Lpm->online
                                           recover from loss of network,
        This setting support the backward compatibility
        ---------------------------------------------------------------------------------
          1        0 or 0xFF    0          Established - immediately,
        (Enable)   (Roaming) (Home)        after power cycle, Lpm->online
                                           recover from loss of network,
        ---------------------------------------------------------------------------------
          1           1         1          Not established
        (Enable)   (home)   (roaming)
        ---------------------------------------------------------------------------------
          1           1         0          Established - immediately,
        (Enable)   (home)     (home)       after power cycle, Lpm->online
                                           recover from loss of network,
        ----------------------------------------------------------------------------------
          2           X         X          if in a autoconnect packet call, no one started
        (Paused)  (dont care) (dont care)  Then will stop the packet call().

                                           After power cycled, it become enabled state and
                                           Data session will estabished.
        ----------------------------------------------------------------------------------
          0           X         X          if in a autoconnect packet call, no one started
        (Disable) (dont care) (dont care)  Then will stop the packet call().

        ---------------------------------------------------------------------------------------*/

        /*---------------------------------------------------------------------
          set auto_connect timer if autoconnect enabled, dtr is high and haven't
          started the data call. This will avoid the race condition during power
          up time (getting roaming_status after registered).

          Come back from roaming area to home, this will set auto_connect timer if
          autoconnect is enabled.
        ---------------------------------------------------------------------*/
        if( (rmnet_smi_info[i].meta_sm.settings.auto_connect_enabled) &&
                (rmnet_meta_smi_verify_dtr_high(&rmnet_smi_info[i]) == TRUE) )
        {
          if (!rmnet_meta_sm_in_call(i))
          {
            LOG_MSG_INFO1_0("Starting autoconnect timer in NULL state");
            timer_set( &(rmnet_smi_info[i].meta_sm.autoconnect_timer),
                         rmnet_smi_info[i].meta_sm.autoconnect_next_backoff_time,
                        0,
                        T_SEC );
            rmnet_smi_info[i].meta_sm.autoconnect_next_backoff_time *= 2;
            if(rmnet_smi_info[i].meta_sm.autoconnect_next_backoff_time >
               AUTOCONNECT_BACKOFF_TIMER_MAX_VALUE)
            {
              rmnet_smi_info[i].meta_sm.autoconnect_next_backoff_time =
                AUTOCONNECT_BACKOFF_TIMER_MAX_VALUE;
            }
          }
          else
          {
            LOG_MSG_INFO2_0("Packet call isalready started");
          }
        }
      }
    }
  }
  else
  {
    LOG_MSG_INFO2_0("SS roaming status not changed");
  }
} /* rmnet_meta_smi_update_autoconnect_roam_status() */

/*===========================================================================
  FUNCTION RMNET_META_SMI_SET_DEFAULT_DATA_FORMAT()

  DESCRIPTION
    Set the default RmNet link protocol to EFS config for Tethered
    and to ETH mode for everything else.

  PARAMETERS
    instance: rmnet instance

  RETURN VALUE
    None

  DEPENDENCIES
    RMNET must be initialized

  SIDE EFFECTS
    None
===========================================================================*/
static void rmnet_meta_smi_set_default_data_format
(
  rmnet_instance_e_type  instance
)
{
  rmnet_smi_info_type   *  info = NULL;
  uint8                    laptop_inst = 0;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (instance >= RMNET_INSTANCE_MAX)
  {
    LOG_MSG_ERROR_1("Invalid rmnet inst %d", instance);
    return;
  }

  info = &rmnet_smi_info[instance];

  if (RMNET_IS_VALID_LAPTOP_INSTANCE(instance))
  {
    laptop_inst = RMNET_GET_LAPTOP_INSTANCE(instance);
    if (laptop_inst >= RMNET_NUM_LAPTOP_INSTANCES)
    {
      LOG_MSG_ERROR_2("Invalid laptop inst %d rmnet inst %d",
                      laptop_inst, instance);
      return;
    }

    info->link_prot = (rmnet_sm_link_prot_e_type)
                 rmnet_efs_config_params[laptop_inst].link_prot;

    info->meta_sm.data_format =
                 rmnet_efs_config_params[laptop_inst].qos_enabled;

    info->meta_sm.data_agg_protocol.ul_data_agg_protocol =
                 rmnet_efs_config_params[laptop_inst].ul_tlp_enabled;

    info->meta_sm.data_agg_protocol.dl_data_agg_protocol =
                 rmnet_efs_config_params[laptop_inst].dl_data_agg_protocol;
  }
  else
  {
    info->link_prot = RMNET_ETHERNET_MODE;
    info->meta_sm.data_format = FALSE;
    info->meta_sm.data_agg_protocol.ul_data_agg_protocol = RMNET_DISABLE_DATA_AGG;
    info->meta_sm.data_agg_protocol.dl_data_agg_protocol = RMNET_DISABLE_DATA_AGG;
  }

  info->meta_sm.qos_format = RMNET_QOS_FORMAT_6_BYTE;
 

  (void)rmnet_meta_sm_set_default_ndp_sig(info,
               info->meta_sm.data_agg_protocol.ul_data_agg_protocol);

  (void)rmnet_meta_sm_set_default_ndp_sig(info,
               info->meta_sm.data_agg_protocol.dl_data_agg_protocol);

  info->meta_sm.data_agg_protocol.dl_min_padding = 0;
  info->meta_sm.qos_ev_enabled = FALSE;

} /* rmnet_meta_smi_set_default_data_format() */

/*===========================================================================
  FUNCTION RMNET_META_SMI_REG_CM()

  DESCRIPTION
    Register the CM SS event notification

  PARAMETERS
    None

  RETURN VALUE
    TRUE  - registration succeeded
    FALSE - registration failed

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean rmnet_meta_smi_reg_cm
(
  void
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------------
    Activate the registered callback functions with CM.
    Activating these by posting a DCC  (instead of doing in init()
    to avoid having a lot of cm events during ds_task init time
    leading to exhaustion of DCC cmd buffers
  -------------------------------------------------------------------------*/
  if( CM_CLIENT_OK != cm_client_act( rmnet_smi_cm_info.cm_client_id ) )
  {
    LOG_MSG_ERROR_0("CM client callback activation failed");
    return FALSE;
  }

  return TRUE;
} /* rmnet_meta_smi_reg_cm() */

/*===========================================================================
  FUNCTION RMNET_META_SMI_IS_DEFAULT_SUB()

  DESCRIPTION
    This function will check if the given subscription id is Default 
    Data subscription or not

  PARAMETERS
     ss_sub_id  - subscription id

  RETURN VALUE
    True  - if given sub is Default Data Subscription
    False - if not

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static boolean rmnet_meta_smi_is_default_sub
(
  sys_modem_as_id_e_type sub_id
)
{
  sys_modem_as_id_e_type asub_id;
  boolean  ret_val = TRUE;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
 /* Process SS event only if this is default data subscription*/
  asub_id = ds3g_get_ps_subs_id();

  if( (asub_id < SYS_MODEM_AS_ID_1) || (asub_id >= SYS_MODEM_AS_ID_MAX) ||
      (asub_id != sub_id) )
  {
     ret_val = FALSE;
  }
  return ret_val;
}

/*===========================================================================
  FUNCTION RMNET_META_SMI_CM_SS_EVENT_CB()

  DESCRIPTION
    Rmnet interface callback for CM serving system event.

    This function is called by CM when a serving system event occurs.
    Note that the callback is called in CM (i.e. not DS) context.

  PARAMETERS
    ss_event:     event that just occurred.
    ss_info_ptr:  current serving system info

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void rmnet_meta_smi_cm_ss_event_cb
(
  cm_ss_event_e_type            ss_event,    /* SS event that just occured */
  const cm_mm_msim_ss_info_s_type *  ss_info_ptr  /* ptr to serving system info */
)
{
  rmnet_smi_cmd_type * cmd_ptr;
  uint8   stack_id,i;
  sys_sys_mode_e_type curr_sys_mode;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if (!ss_info_ptr)
  {
    LOG_MSG_ERROR_0("ss_info_ptr is NULL");
    return;
  }
  else if(!rmnet_meta_smi_is_default_sub(ss_info_ptr->asubs_id))
  {
     LOG_MSG_INFO1_1("ss event for subs_id: %d, non default subscription.Ignore", 
                     ss_info_ptr->asubs_id);
    return;
  }
  if( (ss_info_ptr->number_of_stacks == 0) || (ss_info_ptr->number_of_stacks > CM_NO_STACKS) )
  {
    LOG_MSG_ERROR_2("invalid number of Stacks:%d in ss event %d",ss_info_ptr->number_of_stacks,ss_event);
    return;
  }

  /*By default it is main stack*/
  stack_id = 0;
  /*Get the current sysmode and select that stack*/
  curr_sys_mode = ds3g_get_current_preferred_mode_ex(ss_info_ptr->asubs_id);
  
  for ( i = 0; i < ss_info_ptr->number_of_stacks; i++ )
  {
    if (ss_info_ptr->stack_info[i].sys_mode == curr_sys_mode)
    {
      stack_id = i;
      break;
    }
  }
  /*-------------------------------------------------------------------------
    Post a cmd to rmnet (DS task)  if an ss event of interest has occured
  -------------------------------------------------------------------------*/
  switch(ss_event)
  {
    case CM_SS_EVENT_SRV_CHANGED:
    /* CM_SS_EVENT_INFO event will be generated by CM only in below cases:
      (1) after coming out of power collapse to convey latest ss_info snapshot
      (2) only if we explicitly request for ss info using
          cm_ss_cmd_get_ss_info() api */
    case CM_SS_EVENT_INFO:
#if defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900)
    case CM_SS_EVENT_SRV_NEW:
#endif /* defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900) */
      cmd_ptr = (rmnet_smi_cmd_type *)RMNET_SMI_GET_CMD_BUF(DCC_RMNET_SM_CMD,TRUE);
      if( cmd_ptr != NULL)
      {
        cmd_ptr->sm_id     = RMNET_META_SM;
        cmd_ptr->cmd_type  = RMNET_SM_CMD_NEW_CM_SS_INFO;
        cmd_ptr->data.cm_ss.info.changed_fields  = 
                       (ss_info_ptr->changed_fields_subs | ss_info_ptr->stack_info[stack_id].changed_fields);
        cmd_ptr->data.cm_ss.info.srv_status      = ss_info_ptr->stack_info[stack_id].srv_status;
        cmd_ptr->data.cm_ss.info.roam_status     = ss_info_ptr->stack_info[stack_id].roam_status;
        cmd_ptr->data.cm_ss.asubs_id         = ss_info_ptr->asubs_id;
        cmd_ptr->data.cm_ss.event = ss_event;
        dcc_send_cmd_ex(DCC_RMNET_SM_CMD, (void *)cmd_ptr);
      }
      break;

    default:
      LOG_MSG_INFO2_1 ("Unhandled cm ss event (%d) ignoring", ss_event);
  }
} /* rmnet_meta_smi_cm_ss_event_cb() */


/*===========================================================================
  FUNCTION RMNET_META_SMI_PROCESS_CM_SS_EVENT()

  DESCRIPTION
    Process the CM SS event generated and call coresponding functions in
    Rmnet interface to update the roaming status
  PARAMETERS
    ss_event  :  CM SS event
    ss_info   :  serving system info obtained from CM SS event

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void rmnet_meta_smi_process_cm_ss_event
(
  cm_ss_event_e_type      ss_event,
  rmnet_meta_sm_cm_ss_info_type ss_info,
  sys_modem_as_id_e_type  asubs_id
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /* Process SS event only if this is default data subscription*/
   if(!rmnet_meta_smi_is_default_sub(asubs_id))
   {
      LOG_MSG_INFO1_1("ss event for subs_id: %d, non default subscription.Ignore", asubs_id);
      return;
   }

   LOG_MSG_INFO2_4("Processing CM SS event (%x), changed_fields = (0x%x) (0x%x), sub_id %d",
                   ss_event,
                   QWORD_HIGH(ss_info.changed_fields),
                   QWORD_LOW(ss_info.changed_fields),asubs_id);

  switch (ss_event)
  {
    case CM_SS_EVENT_SRV_NEW:
    case CM_SS_EVENT_SRV_CHANGED:
    case CM_SS_EVENT_INFO:
    {
      if ( ((ss_event == CM_SS_EVENT_SRV_CHANGED) &&
            (ss_info.changed_fields & RMNET_META_SM_CM_SRV_SYS_CHANGED_MASK))||
           (ss_event == CM_SS_EVENT_INFO) ||
           (ss_event == CM_SS_EVENT_SRV_NEW) )
      {
        /* Determine whether to signal serving system change */
        if ((rmnet_smi_cm_info.ss_info.srv_status != ss_info.srv_status) ||
            (rmnet_smi_cm_info.ss_info.roam_status != ss_info.roam_status))
          /* one of the fileds of ss_info has new information */
        {
          LOG_MSG_INFO2_0("Got new serving system info, saving");
          memscpy(&rmnet_smi_cm_info.ss_info,
                  sizeof(  rmnet_meta_sm_cm_ss_info_type),
                 &ss_info,
                 sizeof(  rmnet_meta_sm_cm_ss_info_type));
          rmnet_smi_cm_info.inited = TRUE;

          /* update the roaming status to determine the autoconnect state */
          rmnet_meta_smi_update_autoconnect_roam_status();
        }
        else
        {
          LOG_MSG_INFO3_0("No new info in SS event, ignoring");
        }
      }
      break;
    }

    default:
      LOG_MSG_ERROR_1("Shouldn't be getting this event (%d) here", ss_event);
      break;
  }
} /* rmnet_meta_smi_process_cm_ss_event() */

#if defined(FEATURE_IP_CALL)
#if defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900)
/*===========================================================================
  FUNCTION RMNET_META_SMI_OPRT_MODE_CHANGE_CB ()

  DESCRIPTION
    Callback function for starting operation mode change procedure

  PARAMETERS
    New operation mode
    New trans ID

  RETURN VALUE
    TRUE if succeeded in sending follow-up command
    FALSE otherwise

  DEPENDENCIES
    RM State Machine should have already been initialized

  SIDE EFFECTS
    None
===========================================================================*/
static boolean rmnet_meta_smi_oprt_mode_change_cb
(
  sys_oprt_mode_e_type  new_oprt_mode,
  cmipapp_trans_id_type new_trans_id
)
{
  rmnet_smi_cmd_type   * cmd_ptr;

  LOG_MSG_INFO1_2("rmnet_meta_smi_oprt_mode_change_cb: new mode [%d] transaction ID %p",
                  new_oprt_mode, new_trans_id);

  /* Get a DCC command buffer */
  cmd_ptr = (rmnet_smi_cmd_type *)RMNET_SMI_GET_CMD_BUF(DCC_RMNET_SM_CMD,FALSE);
  if( cmd_ptr == NULL )
  {
    return FALSE;
  }

  /*-------------------------------------------------------------------------
    Post cmd to DS task corresponding to each event
  -------------------------------------------------------------------------*/
  cmd_ptr->sm_id     = RMNET_META_SM;
  cmd_ptr->cmd_type  = RMNET_SM_CMD_CM_OP_MODE_CHANGE;
  cmd_ptr->data.cm_op_mode_change.new_oprt_mode = new_oprt_mode;
  cmd_ptr->data.cm_op_mode_change.new_trans_id = new_trans_id;
  dcc_send_cmd_ex(DCC_RMNET_SM_CMD, (void *)cmd_ptr);

  return TRUE;

} /* rmnet_meta_smi_oprt_mode_change_cb */


/*===========================================================================
  FUNCTION RMNET_META_SMI_CM_OP_MODE_CHANGE_CMD_HDLR()

  DESCRIPTION
    This function processes a RmNet RmSM CMIPAPP Operation mode change event cmd.

  PARAMETERS
    cmd_ptr:  serving task command buffer containing the cmipapp event information

  RETURN VALUE
    None

  DEPENDENCIES
    RM State Machine should have already been initialized

  SIDE EFFECTS
    None
===========================================================================*/
static void rmnet_meta_smi_cm_op_mode_change_cmd_hdlr
(
  rmnet_smi_cmd_type * cmd_ptr
)
{
  rmnet_instance_e_type        i;
  rmnet_smi_cmipapp_info_type *info_ptr = &rmnet_smi_cmipapp_info;

  ASSERT (cmd_ptr && (cmd_ptr->cmd_type == RMNET_SM_CMD_CM_OP_MODE_CHANGE) &&
  	          (cmd_ptr->sm_id    == RMNET_META_SM));

  LOG_MSG_INFO1_2("Enter rmnet_meta_smi_cm_op_mode_change_cmd_hdlr: "
                  "new mode [%d] transID %p",
                  info_ptr->new_oprt_mode, info_ptr->new_trans_id);

  rmnet_smi_cmipapp_info.new_trans_id    =
                      cmd_ptr->data.cm_op_mode_change.new_trans_id;
  rmnet_smi_cmipapp_info.new_oprt_mode   =
                      cmd_ptr->data.cm_op_mode_change.new_oprt_mode;

  switch(info_ptr->new_oprt_mode)
  {
    case SYS_OPRT_MODE_PWROFF:
    case SYS_OPRT_MODE_LPM:
      info_ptr->cur_oprt_mode = info_ptr->new_oprt_mode;
      info_ptr->cur_trans_id  = info_ptr->new_trans_id;

      /* Find any instance in packet call and disconnect it  */
      for(i = RMNET_INSTANCE_MIN; i < RMNET_INSTANCE_MAX; i++)
      {
        /* Delay any auto-connect timer as needed */
        rmnet_meta_sm_set_autoconnect_delay_timer (i,
                                        AUTOCONNECT_BACKOFF_TIMER_MAX_VALUE);
      }

    default: /* NOT in any power off mode  */

      /*---------------------------------------------------------------------
        If mode changed to any not power off mode (such as Online), need to
          - Respond complete of callback procedure
          - Clean up data data structure
      ---------------------------------------------------------------------*/
      cmipapp_oprt_mode_cnf (info_ptr->client_id,
                             info_ptr->new_trans_id);

      // clean up data structure "rmnet_smi_cmipapp_info"
      rmnet_meta_smi_initialize_cmipapp_info(); ;
      break;

  } /* switch() */

} /* rmnet_meta_smi_cm_op_mode_change_cmd_hdlr */

/*===========================================================================
  FUNCTION RMNET_META_SMI_INITIALIZE_CMIPAPP_INFO()

  DESCRIPTION
    Clean up rmnet_smi_cmipapp_info data structure

  PARAMETERS
    None

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void rmnet_meta_smi_initialize_cmipapp_info
(
  void
)
{
  LOG_MSG_INFO2_0("rmnet_meta_smi_initialize_cmipapp_info");

  /* DO NOT initialize the client_id data element since it should contain
  ** the client id allocated(assigned) until re-allocated or sytem reset
  */

  /* Initialize others */
  rmnet_smi_cmipapp_info.cur_trans_id         = (cmipapp_trans_id_type) (-1);
  rmnet_smi_cmipapp_info.new_trans_id         = (cmipapp_trans_id_type) (-1);
  rmnet_smi_cmipapp_info.cur_oprt_mode        = SYS_OPRT_MODE_NONE;
  rmnet_smi_cmipapp_info.new_oprt_mode        = SYS_OPRT_MODE_NONE;
} /* rmnet_meta_smi_initialize_cmipapp_info */

#endif /* defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900) */
#endif /* defined(FEATURE_IP_CALL) */
/*===========================================================================
  FUNCTION RMNET_META_SM_GET_PROC_ID_BY_UM_IFACE()

  DESCRIPTION
    Returns the proc_id for the given Um iface ptr

  PARAMETERS
    um_iface_ptr : The Um iface ptr for which the proc_id is sought
    iface_up     : Flag indicating whether the said iface is UP now

  RETURN VALUE
    proc_id : the proc_id for the given Um iface ptr

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
rmnet_meta_sm_proc_id_e_type rmnet_meta_sm_get_proc_id_by_um_iface
(
  ps_iface_type             *um_iface_ptr,
  boolean                    iface_up
)
{
  sio_port_id_type              sio_port;
  uint32                        inst;
  uint32                        proc_id_idx;
  rmnet_meta_sm_proc_id_e_type  proc_id;

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  LOG_MSG_INFO2_3("Fetching proc_id for 0x%x of type 0x%x in state %d",
                  um_iface_ptr, um_iface_ptr->name, iface_up);
  sio_port = SIO_PORT_NULL;
  proc_id  = RMNET_PROC_ID_MAX;

  /* If the IFACE is up, search through RmNet's active UM ifaces and find the
   * instance corresponding to this UM iface, and pick the sio_port used by
   * that instance. Also, make a copy of the UM iface ptr for later use. */
  if (iface_up)
  {
    for (inst = RMNET_INSTANCE_MIN; inst < RMNET_INSTANCE_MAX; inst++)
    {
      if (um_iface_ptr == rmnet_smi_info[inst].um_iface_ptr)
      {
        /* This will be called multiple times. Once for the Tech IFACE,
         * other for the EPC Iface and also for IFACE UP and IFACE Routeable.
         * Just to make sure to avoid multiple reporting of the data call status
         * change indication.
         */
        if ( NULL == rmnet_smi_info[inst].um_iface_ptr_dup )
        {
          sio_port = rmnet_sio_get_port(
                     rmnet_smi_info[inst].meta_sm.rmnet_sio_handle );
          rmnet_smi_info[inst].um_iface_ptr_dup = um_iface_ptr;
          break;
        }
        else
        {
          LOG_MSG_INFO2_0("Um Iface Dup pointer is already up-to-date; Ignore");
        }
      }
    }
  }
  /* If the IFACE is down, search through the copies made during UP event
   * processing to find the instance corresponding to this UM iface, and pick
   * the sio_port used by that instance. Also, invalidate the copy of the UM
   * iface ptr made earlier. */
  else
  {
    for (inst = RMNET_INSTANCE_MIN; inst < RMNET_INSTANCE_MAX; inst++)
    {
      if (um_iface_ptr == rmnet_smi_info[inst].um_iface_ptr_dup)
      {
        sio_port = rmnet_sio_get_port(
                     rmnet_smi_info[inst].meta_sm.rmnet_sio_handle );
        rmnet_smi_info[inst].um_iface_ptr_dup = NULL;
        break;
      }
    }
  }

  /* If a valid SIO port has been found, find the proc_id corresponding to
   * that SIO port. */
  if (sio_port != SIO_PORT_NULL)
  {
    if (rmnet_meta_smi_find_proc_id(&proc_id_idx,sio_port,inst))
    {
      proc_id = (rmnet_meta_sm_proc_id_e_type)proc_id_idx;
    }
    else
    {
      proc_id = RMNET_PROC_ID_APPS1;
    }
  }

  return proc_id;
} /* rmnet_meta_sm_get_proc_id_by_um_iface */
/*===========================================================================
  FUNCTION RMNET_META_SM_SET_AUTOCONNECT_DELAY_TIMER()

  DESCRIPTION
    Delay the auto-connect timer

  PARAMETERS
    Starting time to delay

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void rmnet_meta_sm_set_autoconnect_delay_timer
(
  rmnet_instance_e_type instance,
  uint32 delay_time
)
{
  rmnet_smi_info_type *  info;

  ASSERT(instance < RMNET_INSTANCE_MAX);

  info = &rmnet_smi_info[instance];

  if (info->meta_sm.settings.auto_connect_enabled)
  {
    LOG_MSG_INFO2_1("rmnet_meta_sm_set_autoconnect_delay_timer = (%d)",
                    instance);

    /*---------------------------------------------------------------------
      if  data call is on going,
        - Just delay Auto connect backoff time as given
      else
        - Stop current auto-connect timer running
        - Delay Auto connect backoff time as given
        - Start auto-connect timer with the changed time
     ---------------------------------------------------------------------*/
    if (rmnet_meta_sm_in_call(instance))
    {
      info->meta_sm.autoconnect_next_backoff_time = delay_time;
    }
    else
    {
      (void) timer_clr( &(info->meta_sm.autoconnect_timer), T_NONE );
      info->meta_sm.autoconnect_next_backoff_time = delay_time;
      (void) timer_set( &(info->meta_sm.autoconnect_timer),
                          info->meta_sm.autoconnect_next_backoff_time, 0, T_SEC );
    }
  }

} /* rmnet_meta_sm_set_autoconnect_delay_timer */

/**************************************************************************
            EFS Configuration and Parsing Functions and Utilities
***************************************************************************/

/* ===========================================================================
FUNCTION RMNET_META_SM_GET_EFS_CONFIG_PARAMS

DESCRIPTION
  This function reads the file rmnet_config.txt from the efs root directory and
  parses it and saves the information. If the file is not present,
  or the content of the file is corrupted, creates a default file with
  just autoconnect to be INVALID so that the Autoconnect can be used from NV
if
  present.

PARAMETERS
  config_params : Output parameters containing the parsed EFS configuration.

RETURN VALUE
  None.

DEPENDENCIES
  None

SIDE EFFECTS
  None.

===========================================================================*/
static void rmnet_meta_sm_get_efs_config_params
(
  rmnet_meta_sm_config_s_type *config_params
)
{
  int fd=0;
  fs_ssize_t efs_ret;
  struct fs_stat  stat_buf;
  uint32    inst_index = 0;
  char      *config_buffer = NULL;
  char      *trim_buff = NULL;
  uint32     trim_buff_len = 0;
  uint32     config_buff_len = 0;
  uint32 tag_key = 0;
  uint32 inst_val = 0;
  static char *filename   = "rmnet_config.txt";
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /* Initialize config params to defaults to defaults */
  do
  {
    config_params[inst_index].instance_id = inst_index;
    config_params[inst_index].link_prot = RMNET_ETHERNET_MODE;
    config_params[inst_index].qos_enabled = FALSE;
    config_params[inst_index].ul_tlp_enabled = RMNET_DISABLE_DATA_AGG;
    config_params[inst_index].autoconnect = RMNET_EFS_AUTOCONNECT_INVALID;
    config_params[inst_index].ip_type = QMI_AF_ANY;
    config_params[inst_index].umts_profile= 0xFF;
    config_params[inst_index].cdma_profile = 0xFF;
    config_params[inst_index].dl_data_agg_protocol = RMNET_DISABLE_DATA_AGG;
    config_params[inst_index].proc_id = RMNET_PROC_ID_MAX;
    ++inst_index;
  }while(inst_index < RMNET_NUM_LAPTOP_INSTANCES );

  /* Initialize global config params to defaults */
  rmnet_efs_global_config_params.ul_agg_prot_mask =
        RMNET_GLOBAL_EFS_DATA_FORMAT_DISABLE_MASK;

  rmnet_efs_global_config_params.dl_agg_prot_mask =
        RMNET_GLOBAL_EFS_DATA_FORMAT_DISABLE_MASK;

#if defined(FEATURE_RMNET_PORT_CONFIG_MDM) || defined(FEATURE_RMNET_PORT_CONFIG_MSM)
  rmnet_efs_global_config_params.ul_agg_prot_mask |=
        RMNET_GLOBAL_EFS_DATA_FORMAT_QMAP_MASK;

  rmnet_efs_global_config_params.dl_agg_prot_mask |=
        RMNET_GLOBAL_EFS_DATA_FORMAT_QMAP_MASK;

#ifdef FEATURE_DATA_QMAP_DEBUG
  #error code not present
#endif /* FEATURE_DATA_QMAP_DEBUG */
#endif /* FEATURE_RMNET_PORT_CONFIG_MDM */

  /* Read from EFS; If there is no valid file create one with defaults */
  fd = efs_open(filename, O_RDWR, ALLPERMS);
  if ( fd < 0 )
  {
    LOG_MSG_INFO1_0 (" Rmnet Config EFS Open failed");
    return;
  }
  else
  {
    /* Get file statistics */
    efs_ret = efs_fstat(fd, &stat_buf);
    inst_index = 0;
    LOG_MSG_INFO1_1("EFS Open file success with size of %d", stat_buf.st_size);
    if ( efs_ret < 0)
    {
      LOG_MSG_ERROR_0(" EFS Stat Failed ");
      /* Not able to Stat the file, gracefully set as if its invalid data */
      efs_close(fd);
      return;
    }

    if ( (stat_buf.st_size > RMNET_CONFIG_EFS_FILE_SIZE)
         ||  ( stat_buf.st_size ==0 ))
    {
      LOG_MSG_ERROR_1("Invalid EFS Data size %d is too large ",
                      stat_buf.st_size);
      inst_index = 0;
      efs_close(fd);
      return;

    }
    config_buffer = (char *) qmi_svc_ps_system_heap_mem_alloc(stat_buf.st_size,
                                  FILE_ID_DS_RMNET_META_SM,__LINE__);

    config_buff_len = stat_buf.st_size;

    trim_buff = (char *) qmi_svc_ps_system_heap_mem_alloc(stat_buf.st_size,
                                  FILE_ID_DS_RMNET_META_SM,__LINE__);
    trim_buff_len = 0;

    /* If allocation failed return  */

    if (config_buffer == NULL || trim_buff == NULL )
    {
      PS_SYSTEM_HEAP_MEM_FREE(config_buffer);
      PS_SYSTEM_HEAP_MEM_FREE(trim_buff);
      efs_close(fd);
      return;
    }

    /* Read rmnet.txt file from efs and store the characters in config_buffer */
    (void)efs_read(fd, config_buffer, config_buff_len);
    efs_close(fd);
    /* Remove unwanted characters like 'spaces', 'tabs', 'newline' etc */
    rmnet_parser_trim_config_data(config_buffer,config_buff_len,
                                  trim_buff, &trim_buff_len);

    /* Parse the actual config data and store it in  rmnet_raw_config_data
       variable */
    rmnet_parser_parse_config_data(trim_buff,
                                   trim_buff_len);

    /* Done with the use of config and trim buffers; clean them. */
    PS_SYSTEM_HEAP_MEM_FREE(config_buffer);
    PS_SYSTEM_HEAP_MEM_FREE(trim_buff);
  }

  /* INSTANCE SPECIFIC CONFIGURATION */
  /* rmnet_raw_config_data contains the raw data, convert the data as per the
     tag and save it in config_params which is an out pointer */
  for ( inst_index = 0; inst_index < RMNET_NUM_LAPTOP_INSTANCES;++inst_index )
  {

    /* Tag instance is manually calculated;Get the actual tags from LINK PROT */
    tag_key = (uint32)RMNET_PARSER_TAG_LINK_PROT;
    /* inst_val will actually identify into which config_params index
       the rest of the tag values needs to be copied into */
    inst_val = inst_index;

    if( NULL !=
         rmnet_raw_config_data[inst_index].tag_value[RMNET_PARSER_TAG_INSTANCE])
    {
      inst_val= (dword)atoi((char*)rmnet_raw_config_data[inst_index].\
                                          tag_value[RMNET_PARSER_TAG_INSTANCE]);
    }
    /* Calculate the instance ID to fill the appropriate instance tag values */
    if (  ( inst_val != 0 ) && ( (inst_val -1) < RMNET_NUM_LAPTOP_INSTANCES ))
    {
      --inst_val; /* To occupy the appropriate index in the array */
      config_params[inst_val ].instance_id = inst_val ;
      LOG_MSG_INFO1_1("EFS Rmnet Config: Instance [%d]", inst_val);
    }
    else
    {
      LOG_MSG_ERROR_2("EFS Rmnet Config:Invalid Instance [%d]; set to default [%d]",
                      inst_val, inst_index);
      inst_val = inst_index;

    }

    for ( ; tag_key < RMNET_PARSER_TAG_MAX;++tag_key )
    {
      /* for each tag field, get its corresponding data from the raw config
         data and update it in the inst_val field only*/
      rmnet_parser_config_get_tag_value( rmnet_raw_config_data[inst_index],
                                         (rmnet_parser_tag_index_e_type)
                                         tag_key,
                                         inst_val,
                                         config_params);
      /* Free the rmnet_raw_config_data */
      PS_SYSTEM_HEAP_MEM_FREE(rmnet_raw_config_data[inst_index].\
                                                       tag_name[tag_key]);
      PS_SYSTEM_HEAP_MEM_FREE(rmnet_raw_config_data[inst_index].\
                                                      tag_value[tag_key]);
    }

  }

    /* GLOBAL CONFIGURATION */
    tag_key = (uint32)RMNET_PARSER_TAG_UL_AGG_PROT_MASK;
    for ( ; tag_key < RMNET_PARSER_GLOBAL_TAG_MAX;++tag_key )
    {
      /* for each tag field, get its corresponding data from the raw config
         data and update it in the inst_val field only*/
      rmnet_parser_global_config_get_tag_value(
                                  (rmnet_parser_tag_global_index_e_type)tag_key);
      /* Free the rmnet_raw_config_data */
      PS_SYSTEM_HEAP_MEM_FREE(rmnet_raw_global_config_data.tag_name[tag_key]);
      PS_SYSTEM_HEAP_MEM_FREE(rmnet_raw_global_config_data.tag_value[tag_key]);
    }

}/* rmnet_meta_sm_get_efs_config_params */

/*===========================================================================
FUNCTION RMNET_PARSER_MATCH_END_TOKEN

DESCRIPTION
  This function matches the end token which will be in the form  of
  </token>

PARAMETERS
  token: containing the text in <token> format.
  token_len: Length of the token buffer.
  buf:   Actual buffer which contains the raw data to be parsed.
  buff len: The lenght of the buffer to be parsed.

RETURN VALUE
  After matching the end position, the exact last character just before
  </token> is returned. which is called the end token pointer. .

DEPENDENCIES
  None

SIDE EFFECTS
  None.

===========================================================================*/
static char * rmnet_parser_match_end_token
(
  char *token,
  uint32 token_len,
  char *buff,
  uint32 len
)
{
  char *match_token = token;
  uint32 token_i = 0;
  char *end_ptr;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  while ( len > 0)
  {
    /* The sequence of data for matching the end token is as follows
       </token>; so check for '<' followed by '/' followed by the first
       character of the token */
    if ( (*buff++ == '<')&& ( *buff++ == '/') && (*buff++ == *match_token ))
    {
      /* We copy the data from the beginning of the end token, but the
         end pointer has already traversed three postioins ahead, copy
         three positions back. */
      token_i = 1;
      end_ptr = buff-3;
      len -=3;
      while( (len > 0) && ( token_i < token_len))
      {
        if ( *buff++ != match_token[token_i])
        {
          token_i=0;
          match_token = token;
          end_ptr = NULL;
          break;
        }
        --len;
        ++token_i;
      }
      /* When the complete token matches it should end with the character '>' */
      if ( token_i == token_len )
      {
        if ( *buff++ == '>' )
        {
          return end_ptr;
        }
      }
    }
    --len;
  }

  return NULL;
}/* rmnet_parser_match_end_token  */

/*===========================================================================
FUNCTION RMNET_PARSER_MATCH_START_TOKEN

DESCRIPTION
  This function matches the start token which will be in the form of
  <token>

PARAMETERS

  token: containing the text in <token> format.
  token_len: Length of the token buffer.
  buf:   Actual buffer which contains the raw data to be parsed.
  buff len: The lenght of the buffer to be parsed.

RETURN VALUE
  After the token is matched, the start pointer is returned..

DEPENDENCIES
  None

SIDE EFFECTS
  None.

===========================================================================*/
static char *rmnet_parser_match_start_token
(
  char *token,
  uint32 token_len,
  char *buff,
  uint32 len
)
{

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /* Run the loop till the given token matches with the buffere data. If
     there is a mis-match, this is not the right token, exit from here */
  while ( (len > 0) && (token_len >0))
  {
    if ( *token++ != *buff++)
    {
      break;
    }
    --len;
    --token_len;
  }
  if ( token_len == 0 )
  {
    /* If this is the right token, the ending must happen with
       the ending close  */
    if ( *buff++ == '>')
    {
      return buff;
    }
  }

  return NULL;
}/* rmnet_parser_match_start_token */

/*===========================================================================
FUNCTION RMNET_PARSE_MATCHING_TOKEN

DESCRIPTION
  This function parses a matching token based on the given token. It extracts
  the data information in between the tokens.

  eg: <token>data</token> --> data is extracted from here.

PARAMETERS

  token: containing the text in <token> format.
  token_len: Length of the token buffer.
  in_buf:   Actual buffer which contains the raw data to be parsed.
  in_buff len: The lenght of the buffer to be parsed.
  out_buff: Save the data parsed into the out buff
  out_buff_len: Out buffere length.

RETURN VALUE
  None.

DEPENDENCIES
  None

SIDE EFFECTS
  None.

===========================================================================*/
static boolean rmnet_parse_matching_token
(
  char *token,
  uint32 token_len,
  char *in_buff,
  uint32 in_len,
  char * out_buff,
  uint32 * out_len)
{
  char *working_ptr = in_buff;
  char *start_buff;
  char *end_buff;
  uint32   index = 0;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if ( token == NULL || in_buff == NULL || out_buff == NULL || out_len == NULL )
  {
    LOG_MSG_ERROR_0("Invalid input ");
    return FALSE;
  }
  while ( index < in_len )
  {
    /* Match the begin token as in <token>'data'<token>*/
    if ( *working_ptr == '<' )
    {
      ++working_ptr;
      if ( (start_buff = rmnet_parser_match_start_token( token, token_len,
                                            working_ptr,
                                            in_len - index ) ) != NULL)
      {
        working_ptr = start_buff;
        index += token_len;

        if ( (end_buff = rmnet_parser_match_end_token(token,token_len,
                                        working_ptr,
                                        in_len - index )) != NULL )
        {
          if ( end_buff > start_buff )
          {
            *out_len = (end_buff - start_buff);
            strlcpy(out_buff,start_buff, *out_len+1);
            return TRUE;
          }
          else
          {
            LOG_MSG_ERROR_0("Invalid data found ");
            return FALSE;
          }
        }
        else
        {
          return FALSE;
        }
      }
    }
    ++working_ptr;
    ++index;
  }
  return FALSE;
}/* rmnet_parse_matching_token */

/*===========================================================================
FUNCTION RMNET_PARSER_TRIM_CONFIG_DATA

DESCRIPTION
  This function removes any extra characters like new line, line feed, tab,
  spaces etc.

PARAMETERS

  config_buff: containing the original raw config data
  config_buf_len: Length of the config buffer.
  trim buf:   After deleting the unnecessary spaces and line feeds buffer.
  trimbuff len: The lenght of the buffer trimmed.

RETURN VALUE
  None.

DEPENDENCIES
  None

SIDE EFFECTS
  None.

===========================================================================*/
static void rmnet_parser_trim_config_data
(
  char * config_buff,
  uint32 config_len,
  char *trim_buff,
  uint32 *trim_buff_len
)
{
  uint32 data_i = 0;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(config_buff != NULL && trim_buff != NULL && trim_buff_len != NULL );

  *trim_buff_len = 0;
  while ( data_i < config_len)
  {
    if ( !((config_buff[data_i] == '\r') || (config_buff[data_i] == '\n') ||
           (config_buff[data_i] == ' ') || (config_buff[data_i] == '\t')) )
    {
      trim_buff[*trim_buff_len] = config_buff[data_i];
      *trim_buff_len = *trim_buff_len +1;
    }
    ++data_i;
  }
}/* rmnet_parser_trim_config_data */

/*===========================================================================
FUNCTION RMNET_PARSER_PARSE_CONFIG_DATA

DESCRIPTION
  This function parses the given buffer and saves the data into
  rmnet_raw_config_data in the raw format.

  There are two levels of parsing here.
  1. High level instance_1 which is root which has internal tags
  2. Internal tags are parsed and the data is extracted and saved.

PARAMETERS


RETURN VALUE
  None.

DEPENDENCIES
  None

SIDE EFFECTS
  None.

===========================================================================*/
static void rmnet_parser_parse_config_data
(
  char *trim_buff,
  uint32 trim_buff_len
)
{

  uint32 i = 0;
  char *node_buff = NULL;
  char *tag_buff = NULL;
  uint32 tag_buff_len = 0;
  uint32 node_len = 0;
  uint32 count = 0;
  uint32 token_len = 0;
  uint32 tag_index = 0;
  uint32 tag_name_len = 0;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*====================================================================
                    Parser Logic
  Example config file:
  <config>
  <instance_1>   --------------------------
  <tag>'data'</tag>                       |----> Step 1 extracts this data
  </instance_1>----------------------------
  </config>

  Step 2 extracts the data 'data'
  ======================================================================*/

  /* Allocate node buff as a max size of trim buff. In the worst case entire
     trim buff could be node buf */

  node_buff = (char *)
        qmi_svc_ps_system_heap_mem_alloc(trim_buff_len+1,
                            FILE_ID_DS_RMNET_META_SM,__LINE__);

  if ( node_buff == NULL )
  {
    return;
  }
  /*Only 12 instance tags are supported now in efs file rmnet_config*/
  for ( i = 0; (i < MIN(RMNET_MAX_INSTANCE_TAGS,RMNET_NUM_LAPTOP_INSTANCES)) &&
               (rmnet_parser_instance_token[i]!= NULL) ; ++i)
  {
    token_len = strlen(rmnet_parser_instance_token[i]);
    /* Step 1 */
    if ( TRUE ==
            rmnet_parse_matching_token( rmnet_parser_instance_token[i],
                                  token_len,
                                  trim_buff,
                                  trim_buff_len,
                                  node_buff,
                                  &node_len))
    {
      tag_index = RMNET_PARSER_TAG_INSTANCE;

      for ( ;tag_index < RMNET_PARSER_TAG_MAX;++tag_index )
      {
        tag_name_len = strlen(rmnet_parser_tag_name[tag_index]);

        tag_buff = (char *)
              qmi_svc_ps_system_heap_mem_alloc(node_len+1,
                         FILE_ID_DS_RMNET_META_SM,__LINE__);

        if ( tag_buff == NULL )
        {
          PS_SYSTEM_HEAP_MEM_FREE(node_buff);
          return;
        }
        /* Step 2 */
        if ( TRUE ==
               rmnet_parse_matching_token(  rmnet_parser_tag_name[tag_index],
                                            tag_name_len,
                                            node_buff,
                                            node_len,
                                            tag_buff,
                                            &tag_buff_len) )
        {
          tag_buff[tag_buff_len] = '\0';
          rmnet_raw_config_data[count].tag_name[tag_index] = (char *)
                      qmi_svc_ps_system_heap_mem_alloc(tag_name_len+1,
                        FILE_ID_DS_RMNET_META_SM,__LINE__);
          rmnet_raw_config_data[count].tag_value[tag_index] = (char *)
                      qmi_svc_ps_system_heap_mem_alloc(tag_buff_len+1,
                        FILE_ID_DS_RMNET_META_SM,__LINE__);

          if ( (rmnet_raw_config_data[count].tag_name[tag_index]  != NULL ) &&
               (rmnet_raw_config_data[count].tag_value[tag_index] != NULL ))
          {
            strlcpy(rmnet_raw_config_data[count].tag_name[tag_index],
                       rmnet_parser_tag_name[tag_index],
                       tag_name_len+1);

            strlcpy(rmnet_raw_config_data[count].tag_value[tag_index],
                      tag_buff,
                      tag_buff_len+1);
          }
        }
        PS_SYSTEM_HEAP_MEM_FREE(tag_buff);
      }
      ++count;
    }
  }

  /*Parse Global Config data*/

  memset(node_buff, 0, trim_buff_len+1);
  token_len = strlen(rmnet_parser_global_tag_name[0]);

  /* Step 1 */
  if ( TRUE ==
          rmnet_parse_matching_token( rmnet_parser_global_tag_name[0],
                                token_len,
                                trim_buff,
                                trim_buff_len,
                                node_buff,
                                &node_len))
  {
    tag_index = RMNET_PARSER_TAG_GLOBAL_CONFIG_MASK;

    for ( ;tag_index < RMNET_PARSER_GLOBAL_TAG_MAX;++tag_index )
    {
      tag_name_len =
          strlen(rmnet_parser_global_tag_name[tag_index]);

      /* For raw data in global config start and end tag */

      tag_buff = (char *)
            qmi_svc_ps_system_heap_mem_alloc(node_len+1,
                          FILE_ID_DS_RMNET_META_SM,__LINE__);

      if ( tag_buff == NULL )
      {
        PS_SYSTEM_HEAP_MEM_FREE(node_buff);
        return;
      }

      /* Step 2 */
      if ( TRUE ==
             rmnet_parse_matching_token(
                                    rmnet_parser_global_tag_name[tag_index],
                                    tag_name_len,
                                    node_buff,
                                    node_len,
                                    tag_buff,
                                    &tag_buff_len) )
      {
        tag_buff[tag_buff_len] = '\0';
        rmnet_raw_global_config_data.tag_name[tag_index] = (char *)
                    qmi_svc_ps_system_heap_mem_alloc(tag_name_len+1,
                         FILE_ID_DS_RMNET_META_SM,__LINE__);
        rmnet_raw_global_config_data.tag_value[tag_index] = (char *)
                    qmi_svc_ps_system_heap_mem_alloc(tag_buff_len+1,
                         FILE_ID_DS_RMNET_META_SM,__LINE__);

        if ( (rmnet_raw_global_config_data.tag_name[tag_index]  != NULL ) &&
             (rmnet_raw_global_config_data.tag_value[tag_index] != NULL ))
        {
          strlcpy(rmnet_raw_global_config_data.tag_name[tag_index],
                     rmnet_parser_global_tag_name[tag_index],
                     tag_name_len+1);

          strlcpy(rmnet_raw_global_config_data.tag_value[tag_index],
                    tag_buff,
                    tag_buff_len+1);
        }
      }
      PS_SYSTEM_HEAP_MEM_FREE(tag_buff);
    }
    ++count;
  }

  PS_SYSTEM_HEAP_MEM_FREE(node_buff);
}/* rmnet_parser_parse_config_data */


/*===========================================================================
FUNCTION RMNET_PARSER_CONFIG_GET_TAG_VALUE

DESCRIPTION
  This function gets the tag value from the raw data. Raw config data is in
  ASCII format and based on the apporpriate tag, it is converted to the
  respective data type.

PARAMETERS
  raw_config_data: Contains the parsed config data in the ASCII format.
  tag_key: The tag key
  config_params: Output pointer to save the config data in the right format.
RETURN VALUE
  None.

DEPENDENCIES
  None

SIDE EFFECTS
  None.

===========================================================================*/
static void rmnet_parser_config_get_tag_value
(
  rmnet_parser_raw_data_s_type  raw_config_data,
  rmnet_parser_tag_index_e_type tag_key,
  uint32                        instance_id,
  rmnet_meta_sm_config_s_type  *config_params
)
{
  char* value_ptr = NULL;
 /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if((NULL == config_params) ||
     ( tag_key >= RMNET_PARSER_TAG_MAX ) ||
     ( instance_id >= RMNET_NUM_LAPTOP_INSTANCES ))
  {
    LOG_MSG_ERROR_2("config_params/Tag key [%d]/Instance ID [%d] Invalid",
                    tag_key, instance_id);
    ASSERT(0);
  }

  value_ptr = raw_config_data.tag_value[tag_key];

  if ( value_ptr != NULL )
  {
    switch( tag_key )
    {
      /* This is redundant; Just keeping it here for future reference  */
      case RMNET_PARSER_TAG_INSTANCE :
      {
        uint32 inst_val = (dword)atoi((char*)value_ptr);
        if ( inst_val < RMNET_NUM_LAPTOP_INSTANCES )
        {
          config_params[instance_id].instance_id = inst_val;
          LOG_MSG_INFO1_1("EFS Rmnet Config: Instance [%d]", inst_val);
        }
        else
        {
          LOG_MSG_ERROR_0("EFS Rmnet Config: Invalid Instance; set to default");
        }
        break;
      }
      case RMNET_PARSER_TAG_LINK_PROT:
      {
        if ( strcasecmp(value_ptr, "IP") == 0)
        {
          config_params[instance_id].link_prot = (uint32)RMNET_IP_MODE;
        }
        else if (strcasecmp(value_ptr, "ETH") == 0)
        {
          config_params[instance_id].link_prot = (uint32)RMNET_ETHERNET_MODE;
        }
        else
        {
          LOG_MSG_ERROR_0("EFS Rmnet Config:Invalid Link Prot; setting to default");
        }
        LOG_MSG_INFO1_1("EFS Rmnet Config: Link Prot [%d]",
                          config_params[instance_id].link_prot);
        break;
      }
      case RMNET_PARSER_TAG_QOS:
      {
        if ( strcasecmp(value_ptr, "enabled") == 0)
        {
          config_params[instance_id].qos_enabled= TRUE;
        }
        else if (strcasecmp(value_ptr, "disabled") == 0)
        {
          config_params[instance_id].qos_enabled = FALSE;
        }
        else
        {
          LOG_MSG_ERROR_0("EFS Rmnet Config: Invalid qos; set to default FALSE");
        }
        LOG_MSG_INFO1_1("EFS Rmnet Config: Qos Enabled val (%d)",config_params[instance_id].qos_enabled);
        break;
      }
      case RMNET_PARSER_TAG_UL_TLP:
      {

        if ( strcasecmp(value_ptr, "enabled") == 0)
        {
          config_params[instance_id].ul_tlp_enabled= RMNET_ENABLE_DATA_AGG_TLP;
        }
        else if (strcasecmp(value_ptr, "ul_qc_ncm_enabled") == 0)
        {
          config_params[instance_id].ul_tlp_enabled = RMNET_ENABLE_DATA_AGG_QC_NCM;
        }
        #ifdef FEATURE_DATA_MBIM
        else if (strcasecmp(value_ptr, "ul_mbim_enabled") == 0)
        {
          config_params[instance_id].ul_tlp_enabled = RMNET_ENABLE_DATA_AGG_MBIM;
        }
        #endif
        else if (strcasecmp(value_ptr, "ul_rndis_enabled") == 0)
        {
          config_params[instance_id].ul_tlp_enabled = RMNET_ENABLE_DATA_AGG_RNDIS;
        }
        else if (strcasecmp(value_ptr, "ul_qmap_enabled") == 0)
        {
          config_params[instance_id].ul_tlp_enabled = RMNET_ENABLE_DATA_AGG_QMAP;
        }
#ifdef FEATURE_DATA_QMAP_DEBUG
        #error code not present
#endif
        else if (strcasecmp(value_ptr, "disabled") == 0)
        {
          config_params[instance_id].ul_tlp_enabled = RMNET_DISABLE_DATA_AGG;
        }
        else
        {
          LOG_MSG_ERROR_0("EFS Rmnet Config: Invalid UL data agg; set to default");
        }
        LOG_MSG_INFO1_1("EFS Rmnet Config: UL TLP val (%d)",config_params[instance_id].ul_tlp_enabled);
        break;
      }
      case RMNET_PARSER_TAG_AUTOCONNECT:
      {
        if ( strcasecmp(value_ptr, "enabled") == 0)
        {
          config_params[instance_id].autoconnect = TRUE;
        }
        else if (strcasecmp(value_ptr, "disabled") == 0)
        {
          config_params[instance_id].autoconnect = FALSE;
        }
        else
        {
          LOG_MSG_ERROR_0("EFS Rmnet Config: Invalid Autoconnect; set to default ");
        }
        LOG_MSG_INFO1_1("EFS Rmnet Config: Autoconnect val (%d)",config_params[instance_id].autoconnect);
        break;
      }
      case RMNET_PARSER_TAG_IP_TYPE:
      {

       if ( ( strcasecmp(value_ptr, "IP") == 0) ||
            (  strcasecmp(value_ptr, "IPV4") == 0) )
       {
         config_params[instance_id].ip_type = (uint32)QMI_AF_INET;
       }
       else if (strcasecmp(value_ptr, "IPV6") == 0)
       {
         config_params[instance_id].ip_type = (uint32)QMI_AF_INET6;
       }
       else if (strcasecmp(value_ptr, "IPV4V6") == 0)
       {
         config_params[instance_id].ip_type = (uint32)QMI_AF_INET_4_6;
       }
       else
       {
         LOG_MSG_ERROR_0("EFS Rmnet Config: Invalid IP; Settign to default");
       }
       LOG_MSG_INFO1_1("EFS Rmnet Config: IP Type (%d)",config_params[instance_id].ip_type);
       break;
      }
      case RMNET_PARSER_TAG_UMTS_PROFILE:
      {
        config_params[instance_id].umts_profile = (dword)atoi((char*)value_ptr);
        LOG_MSG_INFO1_1("EFS Rmnet Config: UMTS Profile [%d]",
                        config_params[instance_id].umts_profile);
        break;
      }
      case RMNET_PARSER_TAG_CDMA_PROFILE:
      {
        config_params[instance_id].cdma_profile = (dword)atoi((char*)value_ptr);
        LOG_MSG_INFO1_1("EFS Rmnet Config: CDMA Profile [%d]",
                        config_params[instance_id].cdma_profile);
        break;
      }
      case RMNET_PARSER_TAG_DL_DATA_AGG:
      {
        if ( strcasecmp(value_ptr, "dl_tlp_enabled") == 0)
        {
          config_params[instance_id].dl_data_agg_protocol= RMNET_ENABLE_DATA_AGG_TLP;
        }
        else if (strcasecmp(value_ptr, "dl_qc_ncm_enabled") == 0)
        {
          config_params[instance_id].dl_data_agg_protocol = RMNET_ENABLE_DATA_AGG_QC_NCM;
        }
        #ifdef FEATURE_DATA_MBIM
        else if (strcasecmp(value_ptr, "dl_mbim_enabled") == 0)
        {
          config_params[instance_id].dl_data_agg_protocol = RMNET_ENABLE_DATA_AGG_MBIM;
        }
        #endif
        else if (strcasecmp(value_ptr, "dl_rndis_enabled") == 0)
        {
          config_params[instance_id].dl_data_agg_protocol = RMNET_ENABLE_DATA_AGG_RNDIS;
        }
        else if (strcasecmp(value_ptr, "dl_qmap_enabled") == 0)
        {
          config_params[instance_id].dl_data_agg_protocol = RMNET_ENABLE_DATA_AGG_QMAP;
        }
#ifdef FEATURE_DATA_QMAP_DEBUG
        #error code not present
#endif
        else if (strcasecmp(value_ptr, "disabled") == 0)
        {
          config_params[instance_id].dl_data_agg_protocol = RMNET_DISABLE_DATA_AGG;
        }
        else
        {
          LOG_MSG_ERROR_0("EFS Rmnet Config: Invalid DL data agg; set to default");
        }
        LOG_MSG_INFO1_1("EFS Rmnet Config: DL data agg val (%d)",
			              config_params[instance_id].dl_data_agg_protocol);
        break;
      }

      case RMNET_PARSER_TAG_PROC_ID:
      {
        if ( strcasecmp(value_ptr, "apps") == 0 )
        {
          config_params[instance_id].proc_id = RMNET_PROC_ID_APPS1;
          LOG_MSG_INFO1_1("EFS Rmnet Config: Inst %d Proc id APPS",
                          instance_id);
        }
        else if ( strcasecmp(value_ptr, "laptop") == 0 )
        {
          config_params[instance_id].proc_id = RMNET_PROC_ID_LAPTOP1;
          LOG_MSG_INFO1_1("EFS Rmnet Config: Inst %d Proc id LAPTOP",
                          instance_id);
        }
        else
        {
          LOG_MSG_ERROR_1("EFS Rmnet Config: Inst %d invalid Proc id",
                          instance_id);
        }
        break;
      }

      default :
        LOG_MSG_ERROR_0("Invalid Tag");
    }
  }
}/* rmnet_parser_config_get_tag_value */

/*===========================================================================
FUNCTION RMNET_PARSER_GLOBAL_CONFIG_GET_TAG_VALUE

DESCRIPTION
  This function gets the tag value from the raw data. Raw config data is in
  ASCII format and based on the apporpriate tag, it is converted to the
  respective data type.

PARAMETERS
  tag_key: The tag key
RETURN VALUE
  None.

DEPENDENCIES
  None

SIDE EFFECTS
  None.

===========================================================================*/
static void rmnet_parser_global_config_get_tag_value
(
  rmnet_parser_tag_global_index_e_type tag_key
)
{
  char* value_ptr = NULL;
  uint32 agg_mask = 0;
 /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if( tag_key >= RMNET_PARSER_GLOBAL_TAG_MAX )
  {
    LOG_MSG_ERROR_1("Invalid Tag key [%d]", tag_key);
    ASSERT(0);
  }

  value_ptr = rmnet_raw_global_config_data.tag_value[tag_key];

  if ( value_ptr != NULL )
  {
    switch( tag_key )
    {
      case RMNET_PARSER_TAG_UL_AGG_PROT_MASK:
      {
        agg_mask = (uint32)atoi((char*)value_ptr);
	//TODO: add validation check for agg_mask
        rmnet_efs_global_config_params.ul_agg_prot_mask = agg_mask ;
        LOG_MSG_INFO1_1("EFS Rmnet Config: Global UL Data Agg Mask %d",
                        agg_mask);

        break;
      }

      case RMNET_PARSER_TAG_DL_AGG_PROT_MASK:
      {
        agg_mask = (uint32)atoi((char*)value_ptr);
	//TODO: add validation check for agg_mask
        rmnet_efs_global_config_params.dl_agg_prot_mask = agg_mask ;

        LOG_MSG_INFO1_1("EFS Rmnet Config: Global DL Data Agg Mask %d",
                        agg_mask);
        break;
      }

      default :
        LOG_MSG_ERROR_0("Invalid Tag");
    }
  }
}/* rmnet_parser_global_config_get_tag_value*/

/*===========================================================================
FUNCTION RMNET_UM_IFACE_IN_USE

DESCRIPTION
  This function checks if the um iface is already used by
  another rmnet instance.

PARAMETERS
  info: pointer to the interface controller's control block

RETURN VALUE
  TRUE:  um iface is already used by another rmnet instance
  FALSE: um iface is not used by another rmnet instance

DEPENDENCIES
  None

SIDE EFFECTS
  None.

===========================================================================*/
static boolean rmnet_um_iface_in_use
(
   rmnet_smi_info_type  * info
)
{
  int i;

  if ( (info != NULL) && (info->um_iface_ptr != NULL) )
  {
    // check if um iface is already in use by another rmnet instance
    for( i = RMNET_INSTANCE_MIN; i < RMNET_INSTANCE_MAX; i++ )
    {
      if ( (info != &rmnet_smi_info[i]) &&
           (rmnet_smi_info[i].um_iface_ptr == info->um_iface_ptr) &&
           (rmnet_smi_info[i].um_iface_handle == info->um_iface_handle) )
      {
        LOG_MSG_INFO1_3("Um Iface [0x%x] handle [0x%x] is already used by rmnet inst [%d]!",
                        info->um_iface_ptr, info->um_iface_handle, i);
        return TRUE;
      }
    }
  }

  return FALSE;
} /* rmnet_um_iface_in_use */

/*===========================================================================
FUNCTION RMNET_UM_IFACE_IS_ARB_PROGRESS

DESCRIPTION
  This function checks if the um iface is already used by
  another rmnet instance who arbitration is in progress already.

PARAMETERS
  info: pointer to the interface controller's control block

RETURN VALUE
  TRUE:  um iface is already used by another rmnet instance
  FALSE: um iface is not used by another rmnet instance

DEPENDENCIES
  None

SIDE EFFECTS
  None.

===========================================================================*/
static boolean rmnet_um_iface_is_arb_progress
(
   rmnet_smi_info_type  * info
)
{
  int i;

  if ( (info != NULL) && (info->um_iface_ptr != NULL) )
  {
    // check if um iface is already in use by another rmnet instance
    for( i = RMNET_INSTANCE_MIN; i < RMNET_INSTANCE_MAX; i++ )
    {
      if ( (info != &rmnet_smi_info[i]) &&
           (rmnet_smi_info[i].um_iface_ptr == info->um_iface_ptr) &&
           (rmnet_smi_info[i].um_iface_handle == info->um_iface_handle) &&
           (rmnet_smi_info[i].meta_sm.arb_close_in_progress == TRUE)
     )
      {
        LOG_MSG_INFO1_3("Um Iface [0x%x] handle [0x%x] is already used by rmnet inst [%d]!and arb close in progress",
                        info->um_iface_ptr, info->um_iface_handle, i);
        return TRUE;
      }
    }
  }

  return FALSE;
} /* rmnet_um_iface_is_arb_progress */

/*===========================================================================
FUNCTION RMNET_TEAR_DOWN_UM_IFACE

DESCRIPTION
  This function tears down the um iface. The um iface must have already
  been brought up by rmnet_meta_smi_bring_up_um_iface_by_policy().

PARAMETERS
  info: pointer to the interface controller's control block

RETURN VALUE
  Tear down status

DEPENDENCIES
  None

SIDE EFFECTS
  None.

===========================================================================*/
static int rmnet_tear_down_um_iface
(
  rmnet_smi_info_type   * info
)
{
  int16               ps_iface_errno = 0;
  int                 retval;

  ASSERT(info != NULL && (info->um_iface_ptr != NULL));

  /*---------------------------------------------------------------------
     Try to tear down the UM iface. Passing NULL for policy
     since don't cache it.
  ---------------------------------------------------------------------*/
  retval = ps_route_tear_down_iface_by_policy(info->um_iface_ptr,
                                              NULL,
                                              &ps_iface_errno,
                                              NULL);

  LOG_MSG_INFO2_2( "ps iface tear down retval [%d], errno [%d]",
                   retval, ps_iface_errno );

  return retval;

} /* rmnet_tear_down_um_iface */

#if defined(FEATURE_RMNET_DATA_AGG_TIMER) && defined(FEATURE_CPU_BASED_FLOW_CONTROL)
/*===========================================================================
FUNCTION RMNET_META_SMI_FC_REGISTER

DESCRIPTION
  This function registers CPU usage signal with FC

PARAMETERS
  None

RETURN VALUE
  TRUE: Registration is succesfully
  FALSE: Registration failed

DEPENDENCIES
  rmnet_fc_cmd_q must be initialized.

SIDE EFFECTS
  None.

===========================================================================*/
static boolean rmnet_meta_smi_fc_register
(
  void
)
{
  fc_in_cmd_type *fc_ptr = NULL;

  fc_ptr = fc_get_in_cmd_buf();
  if (fc_ptr == NULL)
  {
    LOG_MSG_ERROR_0("Can't get FC cmd buffer");
    return FALSE;
  }

  fc_ptr->cmd_hdr.cmd_id = FC_IN_REGISTER_CMD;
  fc_ptr->cmd_data.fc_reg.tcb = rex_self();
  fc_ptr->cmd_data.fc_reg.q = &rmnet_fc_cmd_q;
  fc_ptr->cmd_data.fc_reg.sig = ((rex_sigs_type) 1 << DCC_RMNET_FC_SIGNAL);
  fc_ptr->cmd_data.fc_reg.cmd_id = 0;
  fc_ptr->cmd_data.fc_reg.fc_cpu_threshold.high = rmnet_da_cpu_high;
  fc_ptr->cmd_data.fc_reg.fc_cpu_threshold.low = rmnet_da_cpu_low;
  fc_ptr->cmd_data.fc_reg.fc_cpu_threshold.off = 0;

  fc_put_in_cmd(fc_ptr);
  LOG_MSG_INFO2_0("RMNET registered with FC");

  return TRUE;
} /* rmnet_meta_smi_fc_register */

/*===========================================================================
FUNCTION RMNET_META_SM_PROCESS_FC_SIG

DESCRIPTION
  This function handles signal from FC, set data aggregation mode accordingly.

PARAMETERS
  sig: signal from FC
  user_data_ptr: user data ptr comes with the signal

RETURN VALUE
  None

DEPENDENCIES
  rmnet_meta_smi_fc_register

SIDE EFFECTS
  None.

===========================================================================*/
static boolean rmnet_meta_sm_process_fc_sig
(
   dcc_sig_enum_type sig,
   void *user_data_ptr
)
{
  fc_out_cmd_type *cmd_ptr = NULL;
  fc_out_cmd_enum_type fc_cmd;

  while ( (cmd_ptr = (fc_out_cmd_type *)q_get(&rmnet_fc_cmd_q)) != NULL )
  {
    fc_cmd = cmd_ptr->cmd_hdr.cmd_id;
#ifndef FEATURE_FC_CPU_MONITOR
    mem_free(&tmc_heap_small, cmd_ptr);
#else
    fc_put_cmd_buf_in_free_q(cmd_ptr);
#endif /* FEATURE_FC_CPU_MONITOR */

    switch (fc_cmd)
    {
      case FC_OUT_UP_CMD:
        // CPU load < threshold.low, turn off DA
        if (rmnet_sio_da_enabled == TRUE)
        {
          LOG_MSG_INFO2_1("FC cmd %d, turn off DA", fc_cmd);
          rmnet_sio_da_enabled = FALSE;
        }
        break;

      case FC_OUT_DN_CMD:
        // CPU load > threshold.high, turn on DA
        if (rmnet_sio_da_enabled == FALSE)
        {
          LOG_MSG_INFO2_1("FC cmd %d, turn on DA", fc_cmd);
          rmnet_sio_da_enabled = TRUE;
        }
        break;

      default:
        break;
    }
  }

  return TRUE;
} /* rmnet_meta_sm_process_fc_sig */
#endif /* FEATURE_RMNET_DATA_AGG_TIMER && FEATURE_CPU_BASED_FLOW_CONTROL */

/*===========================================================================
FUNCTION RMNET_META_SM_IS_IFACE_AVAIL

DESCRIPTION
  This function checks if there is a free iface available.

PARAMETERS
  None

RETURN VALUE
  TRUE: Iface available
  FALSE: Iface unavailable

DEPENDENCIES
  None

SIDE EFFECTS
  None.

===========================================================================*/
boolean rmnet_meta_sm_is_iface_avail
(
)
{
  uint8 i;

  /* Check if there is a free Rmnet Iface*/
  for(i=0; i < RMNET_IFACE_MAX; i++)
  {
    if ( (rmnet_smi_iface_info[i] == NULL) ||
         (rmnet_smi_iface_info[i]->in_use == FALSE) )
    {
      return TRUE;
    }
  }
  return FALSE;
} /* rmnet_meta_sm_is_iface_avail */

/*===========================================================================
FUNCTION RMNET_SET_RM_IFACE_ADDR_FAMILY

DESCRIPTION
  Set RM iface addr family

PARAMETERS
  rm_iface_ptr: RM iface pointer
  addr_family : address family

RETURN VALUE
  TRUE if success. FALSE otherwise.

DEPENDENCIES
  None.

SIDE EFFECTS
  None.

===========================================================================*/
static boolean rmnet_set_rm_iface_addr_family
(
  ps_iface_type    * rm_iface_ptr,
  uint8              addr_family
)
{
  uint8 ip_type = IFACE_ANY_ADDR_FAMILY;

  if (!PS_IFACE_IS_VALID(rm_iface_ptr))
  {
    ASSERT(0);
    return FALSE;
  }

  if ( (addr_family == IFACE_IPV4_ADDR_FAMILY) ||
       (addr_family == IFACE_IPV6_ADDR_FAMILY) )
  {
    ip_type = addr_family;
  }

  PS_ENTER_CRIT_SECTION(&global_ps_crit_section);
  rm_iface_ptr->iface_private.addr_family = ip_type;
  PS_LEAVE_CRIT_SECTION(&global_ps_crit_section);

  LOG_MSG_INFO2_2("Setting RM iface %p addr family to %d",
                  rm_iface_ptr, ip_type);

  return TRUE;

} /* rmnet_set_rm_iface_addr_family() */

/*===========================================================================
FUNCTION RMNET_META_SM_IS_QOS_EV_ENABLED 

DESCRIPTION
  This function returns checks if QOS header or QOS event is enabled

PARAMETERS
  instance       : RmNet instance

DEPENDENCIES
  None

RETURN VALUE
 TRUE : If QOS or QOS event report is enabled
 FALSE: QOS and QOS event report are not enabled

SIDE EFFECTS
  None.
===========================================================================*/
extern boolean rmnet_meta_sm_is_qos_ev_enabled
(
  rmnet_instance_e_type  instance
)
{
  rmnet_smi_info_type * info = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (instance < RMNET_INSTANCE_MAX)
  {
    info = &rmnet_smi_info[instance];
  }

  // TRUE if QoS header or QoS event report is enabled
  if ( (info != NULL) &&
       (info->meta_sm.data_format || info->meta_sm.qos_ev_enabled) )
  {
    return TRUE;
  }

  return FALSE;

} /* rmnet_meta_sm_is_qos_ev_enabled() */

/*===========================================================================
FUNCTION RMNET_META_SM_NOTIFY_QOS_EV_REPORT

DESCRIPTION
  Notify rmnet if QoS event report is enabled by AP

PARAMETERS
  instance       : RmNet instance
  qos_ev_report  : If QoS event is enabled 

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
extern void rmnet_meta_sm_notify_qos_ev_report
(
  rmnet_instance_e_type  instance,
  boolean                qos_ev_enabled
)
{
  rmnet_smi_info_type   * info = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (instance >= RMNET_INSTANCE_MAX)
  {
    return;
  }

  info = &rmnet_smi_info[instance];
  info->meta_sm.qos_ev_enabled = qos_ev_enabled;

  if (instance % 2 == 0)
  {
    info = &rmnet_smi_info[instance + 1];
  }
  else
  {
    info = &rmnet_smi_info[instance - 1];
  }

  info->meta_sm.qos_ev_enabled = qos_ev_enabled;

  LOG_MSG_INFO2_2("QoS event report %d on inst %d",
             qos_ev_enabled, instance);

} /* rmnet_meta_sm_notify_qos_ev_report() */

/*===========================================================================
  FUNCTION RMNET_META_SM_GET_SIO_PORT_BY_UM_IFACE()

  DESCRIPTION
    Returns the sio port for the given Um iface ptr(physical iface)

  PARAMETERS
    um_iface_ptr : The Um iface ptr for which the proc_id is sought

  RETURN VALUE
    sio_port     : SIO port identifier

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
sio_port_id_type rmnet_meta_sm_get_sio_port_by_um_iface
(
  ps_iface_type             *um_iface_ptr
)
{
  uint32                        inst;
  ps_iface_type                 *assoc_iface_ptr    = NULL;
  sio_port_id_type               sio_port           = SIO_PORT_NULL;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if(!um_iface_ptr)
  {
    LOG_MSG_ERROR_0("Null um_iface");
    return sio_port;
  }
  
  for (inst = RMNET_INSTANCE_MIN; inst < RMNET_INSTANCE_MAX; inst++)
  {
    if (rmnet_smi_info[inst].um_iface_ptr)
    {
      assoc_iface_ptr = 
        PS_IFACE_GET_ASSOC_IFACE(rmnet_smi_info[inst].um_iface_ptr);

      /* If there is a match with given um iface(MH iface) retrieve sio port*/
      if ((assoc_iface_ptr != NULL) && (assoc_iface_ptr == um_iface_ptr))
      {
        sio_port = rmnet_sio_get_port(
                     rmnet_smi_info[inst].meta_sm.rmnet_sio_handle );

        return sio_port;
      }
    }
  } 

  LOG_MSG_INFO3_1("Sio port returned %d", sio_port);
  return sio_port;
} /* rmnet_meta_sm_get_sio_port_by_um_iface */

/*===========================================================================
FUNCTION RMNET_META_SMI_DO_RX_FC

DESCRIPTION
  This function handles flow enable or disable events

PARAMETERS
  info:         pointer to rmnet smi info 
  fc_mask:      flow control mask indentifies who is sending fc event
  disable_flow: is flow disabled (flow control enabled)

DEPENDENCIES
  rmnet_crit_section must be locked in the caller

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
static void rmnet_meta_smi_do_rx_fc
(
  rmnet_smi_info_type  * info,
  uint32                 fc_mask,
  boolean                disable_flow
)
{
  uint32  old_mask;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (info == NULL ||
      info->meta_sm.state == RMNET_META_SM_NULL_STATE ||
      info->meta_sm.state == RMNET_META_SM_WAITING_FOR_UM_DOWN_STATE)
  {
    return;
  }

  // If QoS is enabled, QMI QoS will send FC indications
  if (info->meta_sm.data_format != 0 ||
      info->meta_sm.qos_ev_enabled)
  {
    return;
  }

  // Send WDS/QMAP FC indications
  old_mask = info->rx_fc_mask;

  if (disable_flow)
  {
    info->rx_fc_mask |= fc_mask;
    if (old_mask == 0 && info->rx_fc_mask != 0)
    {
      wds_rmnet_flow_ctrl_cb(
                 RMNET_META_SM_INFO_TO_INSTANCE(info),
                 TRUE);
    }
  }
  else
  {
    info->rx_fc_mask &= (~fc_mask);
    if (old_mask != 0 && info->rx_fc_mask == 0)
    {
      wds_rmnet_flow_ctrl_cb(
                 RMNET_META_SM_INFO_TO_INSTANCE(info),
                 FALSE);
    }
  }

  LOG_MSG_INFO2("RX FC mask 0x%x", info->rx_fc_mask, 0, 0);

} /* rmnet_meta_smi_do_rx_fc() */

/*===========================================================================
FUNCTION RMNET_META_SMI_RX_FC

DESCRIPTION
  This function handles flow enable or disable events. It is a warpper
  for rmnet_meta_smi_do_rx_fc() with critical section protection.

PARAMETERS
  info:         pointer to rmnet smi info 
  fc_mask:      flow control mask indentifies who is sending fc event
  disable_flow: is flow disabled (flow control enabled)

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
static void rmnet_meta_smi_rx_fc
(
  rmnet_smi_info_type  * info,
  uint32                 fc_mask,
  boolean                disable_flow
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  PS_ENTER_CRIT_SECTION(&rmnet_crit_section);
  rmnet_meta_smi_do_rx_fc(info, fc_mask, disable_flow);
  PS_LEAVE_CRIT_SECTION(&rmnet_crit_section);
} /* rmnet_meta_smi_rx_fc() */

/*===========================================================================
FUNCTION RMNET_META_SM_RX_WMK_FC

DESCRIPTION
  This function handles watermark flow enable or disable events.

PARAMETERS
  inst:         rmnet instance
  disable_flow: is flow disabled (flow control enabled)

DEPENDENCIES
  rmnet_crit_section must be locked in the caller

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
void rmnet_meta_sm_rx_wmk_fc
(
  rmnet_instance_e_type  inst,
  boolean                disable_flow
)
{
  rmnet_smi_dual_ip_info_type  * dual_ip_info;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (inst >= RMNET_INSTANCE_MAX)
  {
    ASSERT(0);
    return;
  }

  dual_ip_info = &rmnet_smi_dual_ip_info[inst >> 1];

  // If SIO is flow control, both v4 and v6 are flow controlled
  rmnet_meta_smi_do_rx_fc(
               dual_ip_info->info_v4,
               RMNET_FC_MASK_SIO,
               disable_flow);

  rmnet_meta_smi_do_rx_fc(
               dual_ip_info->info_v6,
               RMNET_FC_MASK_SIO,
               disable_flow);

} /* rmnet_meta_sm_rx_wmk_fc() */

/*===========================================================================
FUNCTION RMNET_META_SM_UM_IFACE_IN_USE

DESCRIPTION
  This function checks if the um iface is already used by
  another rmnet instance.

PARAMETERS
  um_iface_ptr: um iface to check
  mux_id:       ptr to mux_id which has um iface present 

RETURN VALUE
  TRUE:  um iface is already used by another rmnet instance
  FALSE: um iface is not used by another rmnet instance

DEPENDENCIES
  None

SIDE EFFECTS
  None.

===========================================================================*/
boolean rmnet_meta_sm_um_iface_in_use
(
  ps_iface_type  * um_iface_ptr,
  uint8          * mux_id
)
{
  int                    i;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (um_iface_ptr == NULL || mux_id == NULL)
  {
    return FALSE;
  }

  for (i = 0; i < RMNET_INSTANCE_MAX; i++)
  {
    if (rmnet_smi_info[i].um_iface_ptr == um_iface_ptr)
    {
      *mux_id = qmux_get_qmap_mux_id(
           rmnet_sio_get_port(rmnet_smi_info[i].meta_sm.rmnet_sio_handle));
      return TRUE;
    }
  }

  return FALSE;

} /* rmnet_meta_sm_um_iface_in_use() */

/*===========================================================================
  FUNCTION rmnet_meta_sm_media_connected()

  DESCRIPTION
    This function notifies RmNet of media connect event

  PARAMETERS
    rmnet_inst: RmNet instance

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void rmnet_meta_sm_media_connected
(
  rmnet_instance_e_type rmnet_inst
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if (rmnet_inst < RMNET_INSTANCE_MAX)
  {
    rmnet_meta_smi_rx_fc(
         &rmnet_smi_info[rmnet_inst], RMNET_FC_MASK_CALL_SETUP, FALSE);
  }

} /* rmnet_meta_sm_media_connected() */

/*===========================================================================
  FUNCTION RMNET_META_SM_SET_SUBS_ID

  DESCRIPTION
    This function sets the subs id of RMNET instance

  PARAMETERS
    rmnet_inst: RmNet instance
    subs_id : Subscription identifier

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void rmnet_meta_sm_set_subs_id
(
  rmnet_instance_e_type rmnet_inst,
  uint32                subs_id
)
{
  rmnet_smi_info_type *  info = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (rmnet_inst < RMNET_INSTANCE_MAX)
  {
    info = &rmnet_smi_info[rmnet_inst];
  }

  if (info != NULL)
  {
    info->constants.subs_id = subs_id;
    LOG_MSG_INFO2_2("Setting subs %d, RMNET inst %d", subs_id, rmnet_inst);
  }
}/* rmnet_meta_sm_set_subs_id */

/*===========================================================================
  FUNCTION RMNET_META_SM_GET_SUBS_ID

  DESCRIPTION
    This function gets the subs id of RMNET instance

  PARAMETERS
    rmnet_inst: RmNet instance
    subs_id : Subscription identifier

  RETURN VALUE
    TRUE  - Success
    FALSE - Failure

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean rmnet_meta_sm_get_subs_id
(
  rmnet_instance_e_type   rmnet_inst,
  uint32                * subs_id
)
{
  rmnet_smi_info_type *  info = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (rmnet_inst < RMNET_INSTANCE_MAX)
  {
    info = &rmnet_smi_info[rmnet_inst];
  }

  if (info != NULL)
  {
    *subs_id = info->constants.subs_id;
    LOG_MSG_INFO2_2("Returning subs %d, RMNET inst %d", *subs_id, rmnet_inst);
    return TRUE;
  }

  return FALSE;
}/* rmnet_meta_sm_get_subs_id */

