/*===========================================================================

                    D S _ R M N E T _ Q M A P . C

DESCRIPTION

  RMNET QMAP source file.

EXTERNALIZED FUNCTIONS

  ds_qmap_send_cmd()
    Send a QMAP command

Copyright (c) 2005-2013 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/rmifacectls/src/ds_rmnet_qmap.c#1 $
  $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when        who    what, where, why
--------    ---    ----------------------------------------------------------
05/24/13    wc     Initial file
===========================================================================*/

/*===========================================================================

                                INCLUDE FILES

===========================================================================*/
#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"

#include "ds_qmux.h"
#include "ds_rmnet_sio.h"
#include "ds_qmap.h"
#include "ds_rmnet_qmap.h"

/*===========================================================================

                                DATA TYPES

===========================================================================*/

/*===========================================================================

                                 DEFINITIONS

===========================================================================*/

/*===========================================================================

                           FORWARD DECLARATIONS

===========================================================================*/

/*===========================================================================

                        EXTERNAL FUNCTION DEFINITIONS

===========================================================================*/

/*===========================================================================
  FUNCTION DS_RMNET_SEND_QMAP_CMD()

  DESCRIPTION
    This function sends a QMAP command for a rmnet instance

  PARAMETERS
    rmnet_inst:            rmnet instance
    cmd_size:              QMAP command size
    cmd_ptr:               QMAP command pointer

  RETURN VALUE
    TRUE/FALSE 

  DEPENDENCIES
    SIO port must be opened and QMAP data format is enabled.

  SIDE EFFECTS
    None
===========================================================================*/
boolean ds_rmnet_send_qmap_cmd
(
  rmnet_instance_e_type  rmnet_inst,
  uint16                 cmd_size,
  void                 * cmd_ptr
)
{
  uint8                   cmd;
  rmnet_sio_info_type   * info;
  boolean                 status = TRUE;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  rmnet_inst = RMNET_SIO_GET_DEFAULT_INSTANCE(rmnet_inst);

  info = rmnet_sio_get_info(rmnet_inst);
  if (info == NULL)
  {
    LOG_MSG_ERROR_1("Failed to get rmnet sio info for rmnet inst %d",
                    rmnet_inst);
    return FALSE;
  }

  cmd = *((uint8 *)cmd_ptr + DS_QMAP_CMD_OFFSET);

  // Check if qmap command is enabled or not
  if ( (cmd == DS_QMAP_CMD_FLOW_ENABLE) ||
               (cmd == DS_QMAP_CMD_FLOW_DISABLE) )
  {
    if (info->qmap_settings.in_band_fc == FALSE)
    {
      return TRUE;
    }
  }

#ifdef FEATURE_DATA_A2
  if ( !rmnet_sio_is_a2_port(info->sio_port) )
  {
    return TRUE;
  }

  // For MSM, we allow this even if we are not in QMAP
#ifndef FEATURE_RMNET_PORT_CONFIG_MSM
  if (info->mux_port_cfg.mux_type != A2_MUX_QMAP)
  {
    return TRUE;
  }
#endif /* FEATURE_RMNET_PORT_CONFIG_MSM */

  status = ds_qmap_send_cmd(
                  info->sio_port,
                  info->mux_port_cfg.mux_idx,
                  cmd_size,
                  cmd_ptr,
                  NULL);
#endif /* FEATURE_DATA_A2 */

  return status;
} /* ds_rmnet_send_qmap_cmd() */

/*===========================================================================
  FUNCTION DS_RMNET_GET_QMAP_SETTINGS()

  DESCRIPTION
    This function retrieves QMAP protocol settings.

  PARAMETERS
    rmnet_inst:            rmnet instance
    qmap_settings:         ptr to qmap_settings 

  RETURN VALUE
    TRUE/FALSE

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean ds_rmnet_get_qmap_settings
(
  rmnet_instance_e_type           rmnet_inst,
  ds_rmnet_qmap_settings_type   * qmap_settings
)
{
  rmnet_sio_info_type     * info;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  rmnet_inst = RMNET_SIO_GET_DEFAULT_INSTANCE(rmnet_inst);

  info = rmnet_sio_get_info(rmnet_inst);
  if (info == NULL)
  {
    LOG_MSG_ERROR_1("Failed to get rmnet sio info for rmnet inst %d",
                    rmnet_inst);
    return FALSE;
  }

  *qmap_settings = info->qmap_settings;

  return TRUE;

} /* ds_rmnet_get_qmap_settings() */

/*===========================================================================
  FUNCTION DS_RMNET_SET_QMAP_SETTINGS()

  DESCRIPTION
    This function sets QMAP protocol settings.

  PARAMETERS
    rmnet_inst:            rmnet instance
    qmap_settings:         ptr to qmap_settings 

  RETURN VALUE
    TRUE/FALSE

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean ds_rmnet_set_qmap_settings
(
  rmnet_instance_e_type           rmnet_inst,
  ds_rmnet_qmap_settings_type   * qmap_settings
)
{
  rmnet_sio_info_type     * info;
  rmnet_sio_info_type     * info_i;
  int                       i;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  rmnet_inst = RMNET_SIO_GET_DEFAULT_INSTANCE(rmnet_inst);

  info = rmnet_sio_get_info(rmnet_inst);
  if (info == NULL)
  {
    LOG_MSG_ERROR_1("Failed to get rmnet sio info for rmnet inst %d",
                    rmnet_inst);
    return FALSE;
  }

  if (qmap_settings->in_band_fc != info->qmap_settings.in_band_fc)
  {
    info->qmap_settings.in_band_fc = qmap_settings->in_band_fc;
  }

  /*-----------------------------------------------------------------------
   * Apply settings to logical ports
  -----------------------------------------------------------------------*/
  if ( qmi_get_mux_port_cfg() != QMI_MUX_PORT_CFG_DISABLED &&
       rmnet_sio_get_master_inst() == rmnet_inst )
  {
    for (i = 0; i < RMNET_INSTANCE_MAX; i+=2)
    {
      info_i = rmnet_sio_get_info((rmnet_instance_e_type)i);
      if ( (info_i != NULL) && (info_i != info) &&
           QMUX_IS_QMAP_MUXED_PORT(info_i->sio_port) )
      {
        info_i->qmap_settings = info->qmap_settings;
      }
    }
  }

  return TRUE;

} /* ds_rmnet_set_qmap_settings() */

