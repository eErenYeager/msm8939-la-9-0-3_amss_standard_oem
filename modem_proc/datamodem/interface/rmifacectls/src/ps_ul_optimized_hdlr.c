/*===========================================================================

                     PS_OPT_PATH_HDLR . C

DESCRIPTION

This is the implementation of Uplink Data Optimized handler.

Copyright (c) 2011-2014 Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary
===========================================================================*/
/*===========================================================================

                            EDIT HISTORY FOR FILE
  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/rmifacectls/src/ps_ul_optimized_hdlr.c#1 $

when        who    what, where, why
--------    ---    ----------------------------------------------------------
01/09/14    vrk    Added NULL check on outstanding pkt to avoid race condition 
                   in back to back data calls
06/20/13    pgm    Fix to store and clear MBIM mpdp sesions correctly.
06/14/13    pgm    Fix to handle race condtion between MBIM call disconnect
                   and packet processing.
06/10/13    pgm    Added flow control for Legacy Wmk.
05/13/13    pgm    Added MBIM feature flag.
04/26/13    pgm    MBIM Multi-PDN data path changes
01/23/13    pgm    Added check for pushing V6 frag and ICMP packets to
                   legacy path.
01/18/13    tw     v4 Legacy check for RM filters
12/27/12    tw     Fix IPv6 call up check error
11/07/12    tw     Added added offset to pefix packet check
11/05/12    pgm    Checking Flow control before pushing packets to Legacy Wmk
                   to avoid DSM exhaustion/RLF.
09/18/12    tw     Fix for packet cmp when NW assigns new IPv6 prefix
09/20/12    dvk    Made changes for accessing tcp_ack_prio_enabled global
                   variable from a accessor function.
08/08/12    pgm    Flow control changes for supporting different APNs in
                   Dual-IP.
07/10/12    pgm    Fix for routing packets to the right PDN in a multi-PDN
                   call after flow is enabled.
07/02/12    pgm    Freeing dynamically allocated ndp buff in error cases to
                   avoid memory leak issues.
04/12/12    pgm    Moving Legacy path processing to PS task from PS_RM to
                   avoid race condition issues.
03/09/12    pgm    Added check for PS_FLOW_CAPABILITY_DATA_DISALLOWED.
12/08/02    pgm    Fix for deadlock issue in fallback_sig_hdlr()between PS and
                   PSRM task. Also added rmnet_rx_pkt_hdlr_crit_section.
11/01/11    am     Re-orged for NAT support.
10/14/11    pgm    Fixed Klocwork errors.
09/22/11    pgm    Removed NDP Signature Mask.
09/22/11    pgm    Added a new sig hdlr for MBIM support.
09/14/11    SM     Added TCP ACK PRIO NV item check for dynamic enablement
                   of ACK tagging.
09/08/11    pgm    Featurized the TCP ACK tagging with
                   FEATURE_DATA_PS_TCP_ACK_PRIO.
08/19/11    pgm    TCP ACK prioritization support
                   Code cleanup - Added macros and inline functions
08/15/11    AM     created file
===========================================================================*/

/*===========================================================================

                          INCLUDE FILES FOR MODULE

===========================================================================*/

#include "comdef.h"
#include "customer.h"

#include "amssassert.h"
#include "msg.h"
#include "err.h"
#include "ps_crit_sect.h"
#include "queue.h"
#include "dsm.h"

#include "ps_in.h"
#include "ps_iface.h"
#include "ps_ifacei.h"
#include "ps_lan_llc.h"
#include "ps_iface_defs.h"
#include "ps_phys_linki.h"
#include "ps_crit_sect.h"
#include "ps_ul_optimized_hdlr.h"
#include "ps_optimized_path_defs.h"
#include "ds_rmnet_smi.h"
#include "ds_rmnet_sio.h"
#include "ds_qmi_svc.h"

#include "ps_iface_utils.h"
#include "ps_ip4_hdr.h"
#include "ps_tcp_config.h"
#include "ps_icmp.h"
#include "ps_icmp6_msg.h"
#include "ps_logging.h"
#include "ps_metai_info.h"
#include "ps_ifacei_utils.h"
#include "ps_flow.h"
#include "ps_flowi_utils.h"
#include "ps_tx_meta_info.h"
#include "ps_lan_llc.h"
#include "ps_pkt_info_utils.h"
#include "ps_iface_mbim_defs.h"
#include "ps_system_heap.h"
#include "ds_qmi_qos.h"

extern ps_dl_opt_filter_spec_type *global_ul_opt_fltr_arr;

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

                          LOCAL DATA DECLARATIONS

=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/

uint32  pkts_rx;
uint32  pkts_tx;

uint64  bytes_tx;
uint64  bytes_rx;


/*===========================================================================

                             INTERNAL FUNCTIONS

===========================================================================*/

INLINE boolean ps_ul_opti_process_filters
(
  ps_dl_opt_filter_spec_type * filter_ptr,
  uint16 src_port,
  uint16 dst_port,
  uint16 protocol,
  uint16 max_filter_entries
)
{
  uint16                   fltr_dst_port_host;
  uint16                   fltr_src_port_host;
  uint16                   pkt_dst_port_host;
  uint16                   pkt_src_port_host;
  uint8                    filter_cnt;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if ( filter_ptr != NULL )
  {
    pkt_dst_port_host = ps_ntohs(dst_port);
    pkt_src_port_host = ps_ntohs(src_port);
    for (filter_cnt = 0;
         filter_cnt < max_filter_entries &&
           filter_ptr->fi_handle != 0;
         filter_cnt++, filter_ptr++)
    {
      fltr_dst_port_host = ps_ntohs(filter_ptr->dst_port);
      fltr_src_port_host = ps_ntohs(filter_ptr->src_port);
      if ( ((protocol == filter_ptr->protocol) || 
            (filter_ptr->protocol == PS_IPPROTO_TCP_UDP && 
             (protocol == PS_IPPROTO_TCP || protocol == PS_IPPROTO_UDP)))&&
           ((pkt_dst_port_host >= fltr_dst_port_host &&
             pkt_dst_port_host <= fltr_dst_port_host + filter_ptr->dst_port_range) ||
            (pkt_src_port_host >= fltr_src_port_host &&
             pkt_src_port_host <= fltr_src_port_host + filter_ptr->src_port_range)))
      {
        LOG_MSG_INFO1_3("ps_ul_opti_process_filters: "
                        "Rx filter pass protocol (%d) dst port (%d) src port (%d)",
                         protocol, pkt_dst_port_host, pkt_src_port_host);
        return TRUE;
      }
    }
  }
  return FALSE;
}


/*===========================================================================
FUNCTION ps_ul_optimized_ipv6_prefix_cmp

DESCRIPTION


PARAMETERS
  pkt_buf    : pointer to the pkt buffer
  info       : Rm state machine's information block

RETURN VALUE
  TRUE  : UM and packet prefix match
  FALSE : Prefix does not match

DEPENDENCIES
  None

SIDE EFFECTS
  None
===========================================================================*/
INLINE boolean ps_ul_optimized_ipv6_prefix_cmp
(
  uint8               			* const pkt_buf,
  rmnet_smi_info_type           * info
)
{
  uint32   src_v6_prefix_addr32[2] ALIGN(4); //__attribute__ ((aligned(4)));

  src_v6_prefix_addr32[0] = ((uint32) *(uint32 *)
                             (pkt_buf + PS_OPT_PATH_V6_SRC_ADDR_OFFSET));
  src_v6_prefix_addr32[1] = ((uint32) *(uint32 *)
                           (pkt_buf + PS_OPT_PATH_V6_SRC_ADDR_OFFSET+4));

  if ( (*(uint64*)(&src_v6_prefix_addr32[0])) == info->rmnet_iface_ptr->
          iface_private.ipv6_addrs[DEFAULT_V6_INDEX]->prefix
     ) // end if
  {
    return TRUE;
  }

  LOG_MSG_ERROR_1("Src V6 prefix 0x%llx doesnt match Um prefix, drop pkt!",
                  ps_ntohll(*(uint64*)((&src_v6_prefix_addr32[0]))));
  return FALSE;
} /* ps_ul_optimized_ipv6_prefix_cmp */

/*===========================================================================
FUNCTION ps_ul_optimized_ipv6_prefix_cmp_unaligned

DESCRIPTION


PARAMETERS
  pkt_buf    : pointer to the pkt buffer
  info       : Rm state machine's information block

RETURN VALUE
  TRUE  : UM and packet prefix match
  FALSE : Prefix does not match

DEPENDENCIES
  None

SIDE EFFECTS
  None
===========================================================================*/
INLINE boolean ps_ul_optimized_ipv6_prefix_cmp_unaligned
(
  uint8               			* const pkt_buf,
  rmnet_smi_info_type           * info
)
{
  uint32   src_v6_prefix_addr32[2] ALIGN(4); //__attribute__ ((aligned(4)));

  src_v6_prefix_addr32[0] = ps_optimized_hget32_unaligned(
							 pkt_buf + PS_OPT_PATH_V6_SRC_ADDR_OFFSET);
  src_v6_prefix_addr32[1] = ps_optimized_hget32_unaligned(
                             pkt_buf + PS_OPT_PATH_V6_SRC_ADDR_OFFSET+4);

  if ( (*(uint64*)(&src_v6_prefix_addr32[0])) == info->rmnet_iface_ptr->
          iface_private.ipv6_addrs[DEFAULT_V6_INDEX]->prefix
     ) // end if
  {
    return TRUE;
  }

  LOG_MSG_ERROR_1("Src V6 prefix 0x%llx doesnt match Um prefix, drop pkt!",
                  				 ps_ntohll(*(uint64*)((&src_v6_prefix_addr32[0]))));
  return FALSE;
} /* ps_ul_optimized_ipv6_prefix_cmp_unaligned */

/*===========================================================================
FUNCTION PS_UL_OPTIMIZED_IPV4_IS_LEGACY

DESCRIPTION
  This macro returns whether the packet needs to take the legacy path or should
  be forwarded to the Um iface for IPv4.
  Packets take the legacy path in following cases
  1. If packet is destined to a broadcast address
  2. if packet is destined to a limited broadcast address
  3. If packet is destined to a multicast address
  4. If packet is fragmented
  5. if packet is destined to Rm iface
  6. if IP header has options

PARAMETERS
  pkt_buf    : pointer to the pkt buffer
  temp_info  : Rm state machine's information block
  ip4_hdr_len: IPv4 Header length

RETURN VALUE
  TRUE  : if packet needs to take legacy path
  FALSE : if packet should be forwarded to the Um iface

DEPENDENCIES
  None

SIDE EFFECTS
  None

===========================================================================*/
INLINE boolean ps_ul_optmized_hdlr_ipv4_is_legacy
(
  uint8               * const pkt_buf,
  rmnet_smi_info_type * temp_info,
  uint8                 ip4_hdr_len
)
{
  ps_dl_opt_filter_spec_type * filter_ptr = NULL;
  ps_iface_type          * rm_base_iface_ptr = NULL;
  uint16                       dst_port = 0;
  uint16                       src_port = 0;
  uint8                        protocol = 0;

  if ( PS_IN_IS_ADDR_LIMITED_BROADCAST
       ( (uint32) *(uint32 *)(pkt_buf +
         PS_OPT_PATH_V4_DEST_ADDR_OFFSET)
       )                                                    ||
       PS_IN_IS_ADDR_MULTICAST
       ( (uint32) *(uint32 *)(pkt_buf +
         PS_OPT_PATH_V4_DEST_ADDR_OFFSET)
       )                                                    ||
       (((uint16) *((uint16 *) (pkt_buf +
        PS_OPT_PATH_V4_FRAG_OFFSET)) &
        ps_htons(PS_OPT_PATH_V4_FRAG_MASK)) != 0)  ||
       (ip4_hdr_len > PS_OPT_PATH_V4_HDR_LEN)
     )
   {
     LOG_MSG_INFO3_1("V4_DEST_ADDR: 0x%x",
                     (uint32) *(uint32 *)(pkt_buf + PS_OPT_PATH_V4_DEST_ADDR_OFFSET));
     LOG_MSG_INFO3_1("Frag:0x%x",
                     (uint16) *((uint16 *) (pkt_buf + PS_OPT_PATH_V4_FRAG_OFFSET)));
     LOG_MSG_INFO3_1("ip4_hdr_len:%d", ip4_hdr_len);
     LOG_MSG_INFO3_1("Rmnet_iface_ipv4_addr:0x%x",
                     temp_info->rmnet_iface_ptr->iface_private.ipv4_addr);
     return TRUE;
   }

   /*---------------------------------------------------------------------
     Extract the src port, dst port and protocol from the pkt_buf.
    ---------------------------------------------------------------------*/
     protocol = *(pkt_buf + PS_OPT_PATH_V4_PROT_FIELD_OFFSET);
     src_port = (uint16)*((uint16*)(pkt_buf + PS_OPT_PATH_V4_HDR_LEN));
     dst_port = (uint16)*((uint16*)(pkt_buf + PS_OPT_PATH_V4_HDR_LEN + 2));

   /*---------------------------------------------------------------------
     Check to see if RM filters are present instead of checking IP addr.
     Only allow packets which match a filters protocol and port range to
     be pass.
    ---------------------------------------------------------------------*/
   rm_base_iface_ptr = PS_IFACE_GET_BASE_IFACE(temp_info->rmnet_iface_ptr);
   filter_ptr = PS_IFACEI_GET_OPT_FILTER_BASE(rm_base_iface_ptr);
   if (ps_ul_opti_process_filters(filter_ptr, 
                                  src_port, 
                                  dst_port, 
                                  protocol,
                                  PS_IFACE_MAX_OPT_FILTER_ENTRIES)) 
   {
     return TRUE;
   }


   /* If filters installed on iface fails, then run filters on global filter array for Rm group */
   if (ps_ul_opti_process_filters(global_ul_opt_fltr_arr, 
                                  src_port, 
                                  dst_port, 
                                  protocol,
                                  MAX_GLOBAL_OPT_FILTER_ENTRIES)) 
   {
         return TRUE;
   }

   return FALSE;
} /* ps_ul_optmized_hdlr_ipv4_is_legacy() */

/*===========================================================================
FUNCTION PS_UL_OPTIMIZED_PATH_IPV4_OPT_IS_LEGACY

DESCRIPTION
  This macro returns whether the packet needs to take the legacy path or should
  be forwarded to the Um iface for IPv4. It uses optimized function to get 32
  bit values.
  Packets take the legacy path in following cases
  1. If packet is destined to a broadcast address
  2. if packet is destined to a limited broadcast address
  3. If packet is destined to a multicast address
  4. If packet is fragmented
  5. if packet is destined to Rm iface
  6. if IP header has options

PARAMETERS
  pkt_buf    : pointer to the pkt buffer
  temp_info  : Rm state machine's information block
  ip4_hdr_len: IPv4 Header length

RETURN VALUE
  TRUE  : if packet needs to take legacy path
  FALSE : if packet should be forwarded to the Um iface

DEPENDENCIES
  None

SIDE EFFECTS
  None
===========================================================================*/
INLINE boolean ps_ul_optimized_ipv4_opt_is_legacy
(
  uint8               * const pkt_buf,
  rmnet_smi_info_type * temp_info,
  uint8                 ip4_hdr_len
)
{
  if ( PS_IN_IS_ADDR_LIMITED_BROADCAST
       ( ps_optimized_hget32_unaligned(pkt_buf +
         PS_OPT_PATH_V4_DEST_ADDR_OFFSET)
       )                                                      ||
       PS_IN_IS_ADDR_MULTICAST
       ( ps_optimized_hget32_unaligned( pkt_buf +
         PS_OPT_PATH_V4_DEST_ADDR_OFFSET)
       )                                                      ||
       (((uint16)*((uint16 *)(pkt_buf +
        PS_OPT_PATH_V4_FRAG_OFFSET)) &
        ps_htons(PS_OPT_PATH_V4_FRAG_MASK)) != 0)    ||
       (ip4_hdr_len > PS_OPT_PATH_V4_HDR_LEN)        ||
       (temp_info->rmnet_iface_ptr->iface_private.ipv4_addr ==
        ps_optimized_hget32_unaligned ( pkt_buf +
        PS_OPT_PATH_V4_DEST_ADDR_OFFSET))
     )
   {
     return TRUE;
   }
   else
   {
     return FALSE;
   }

} /* ps_ul_optimized_ipv4_opt_is_legacy() */

/*===========================================================================
FUNCTION PS_UL_OPTIMIZED_IPV6_IS_LEGACY

DESCRIPTION
  This macro returns whether the packet needs to take the legacy path or should
  be forwarded to the Um iface for IPv6.
  Packets take the legacy path in following cases
  1. If packet is destined to a linklocal address
  2. If packet is destined to a multicast address
  3. If packet has the frag hdr
  4. If packet is ICMPv6

PARAMETERS
  pkt_buf    : pointer to the pkt buffer

RETURN VALUE
  TRUE  : if packet needs to take legacy path
  FALSE : if packet should be forwarded to the Um iface

DEPENDENCIES
  None

SIDE EFFECTS
  None
===========================================================================*/
INLINE boolean ps_ul_optimized_ipv6_is_legacy
(
   uint8 * const pkt_buf
)
{
  if ( PS_IN6_IS_PREFIX_LINKLOCAL
       ((uint32) *(uint32 *) (pkt_buf + PS_OPT_PATH_V6_DEST_ADDR_OFFSET)) ||

       ((uint8) (* (pkt_buf + PS_OPT_PATH_V6_DEST_ADDR_OFFSET)) ==
       PS_OPT_PATH_V6_MCAST_MASK) ||

       ((uint8) (* (pkt_buf + PS_OPT_PATH_V6_NXT_HDR_OFFSET)) ==
       (uint8)PS_IPPROTO_FRAG_HDR) ||

       ((uint8) (* (pkt_buf + PS_OPT_PATH_V6_NXT_HDR_OFFSET)) ==
       (uint8)PS_IPPROTO_ICMP6)

     )
  {
    return TRUE;
  }
  else
  {
    return FALSE;
  }

} /* ps_ul_optimized_ipv6_is_legacy() */

/*===========================================================================
FUNCTION PS_UL_OPTIMIZED_IPV6_OPT_IS_LEGACY

DESCRIPTION
  This macro returns whether the packet needs to take the legacy path or should
  be forwarded to the Um iface for IPv6. It uses optimized function to get 32
  bit values.
  Packets take the legacy path in following cases
  1. If packet is destined to a linklocal address
  2. If packet is destined to a multicast address
  3. If packet has the frag hdr
  4. If packet is ICMPv6

PARAMETERS
  pkt_buf    : pointer to the pkt buffer

RETURN VALUE
  TRUE  : if packet needs to take legacy path
  FALSE : if packet should be forwarded to the Um iface

DEPENDENCIES
  None

SIDE EFFECTS
  None
===========================================================================*/
INLINE boolean ps_ul_optimized_ipv6_opt_is_legacy
(
  uint8  * const pkt_buf
)
{
  if ( PS_IN6_IS_PREFIX_LINKLOCAL
       (ps_optimized_hget32_unaligned(pkt_buf +
         PS_OPT_PATH_V6_DEST_ADDR_OFFSET)) ||

       ((uint8) (* (pkt_buf + PS_OPT_PATH_V6_DEST_ADDR_OFFSET)) ==
       PS_OPT_PATH_V6_MCAST_MASK) ||

       ((uint8) (* (pkt_buf + PS_OPT_PATH_V6_NXT_HDR_OFFSET)) ==
       (uint8)PS_IPPROTO_FRAG_HDR) ||

       ((uint8) (* (pkt_buf + PS_OPT_PATH_V6_NXT_HDR_OFFSET)) ==
       (uint8)PS_IPPROTO_ICMP6)

     )
  {
    return TRUE;
  }
  else
  {
    return FALSE;
  }

} /* ps_ul_optimized_ipv6_opt_is_legacy() */

/*===========================================================================
FUNCTION PS_UL_OPTIMIZED_TCP_ACK_PRI_CHECK

DESCRIPTION
  This macro returns whether the packet is a TCP ACK and if it needs to be
  prioritized for IPv4 and IPv6.

PARAMETERS
  pkt_buf     : pointer to the pkt buffer
  ip_hdr_len  : IP header length
  total_ip_len: Total IP length

RETURN VALUE
  TRUE  : if packet is a TCP ACK
  FALSE : otherwise

DEPENDENCIES
  None

SIDE EFFECTS
  None
===========================================================================*/
INLINE boolean ps_ul_optimized_tcp_ack_pri_check
(
    uint8 * const pkt_buf,
    uint8   ip_hdr_len,
    uint16  total_ip_len,
    uint8   prot_offset
)
{
  uint8 tcp_header_length = 0;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_INFO3_0("ps_ul_optimized_tcp_ack_pri_check");
  LOG_MSG_INFO3_3("ip_hdr_len: %d, total_ip_len: %d, prot_offset: %d",
                  ip_hdr_len, total_ip_len, prot_offset);
  /*-----------------------------------------------------------------------
      Check if it is TCP
    -----------------------------------------------------------------------*/
  if( ps_pkt_info_get_tcp_ack_prio_enabled() &&
     (*(pkt_buf + prot_offset) == PS_IPPROTO_TCP) )
  {
    tcp_header_length =
        ( *(pkt_buf + ip_hdr_len + PS_OPT_PATH_TCP_HDR_LEN_OFFSET) &
             PS_OPT_PATH_TCP_HDR_LEN_MASK ) >>
             PS_OPT_PATH_WORD_SHIFT;

    LOG_MSG_INFO3_1("PS_IPPROTO_TCP: %d", tcp_header_length);

    /*-----------------------------------------------------------------------
      Check if it is a TCP ACK packet
    -----------------------------------------------------------------------*/

    if(total_ip_len - (ip_hdr_len + tcp_header_length) == 0)
    {
      return TRUE;
    }
  }

  return FALSE;

} /* ps_ul_optimized_tcp_ack_pri_check() */

/*===========================================================================
FUNCTION PS_UL_OPTIMIZED_HDLR_LEGACY_FLOW_ENABLED

DESCRIPTION
  This function return flow control status of Legacy Wmk.

PARAMETERS
  info  :  rmnet_smi_dual_ip_info_type

RETURN VALUE
  TRUE  : Flow enabled
  FALSE : Flow disabled

DEPENDENCIES
  None

SIDE EFFECTS
  None.

===========================================================================*/
INLINE boolean ps_ul_optimized_hdlr_legacy_flow_enabled
(
  rmnet_smi_dual_ip_info_type *info
)
{
  return info->legacy_flow_enabled;

}/*ps_ul_optimized_hdlr_legacy_flow_enabled()*/

/*===========================================================================
FUNCTION PS_UL_OPTIMIZED_PROCESS_QOS_HDR

DESCRIPTION
  This function processes the QOS header.

PARAMETERS
  pkt_ptr              : dsm_item_type rx packet
  tx_meta_info_ptr_ptr : tx meta info pointer to be returned
  qos_hdr              : RmNet QOS header type
  um_iface_ptr         : pointer to Um Iface.
  version              : IP Version
  ip_id                : IP Identification

RETURN VALUE
  None.

DEPENDENCIES
  None

SIDE EFFECTS
  None.

===========================================================================*/

static void ps_ul_optimized_process_qos_hdr
(
  dsm_item_type                 * pkt_ptr,
  ps_tx_meta_info_type         ** tx_meta_info_ptr_ptr,
  rmnet_meta_smi_qos_hdr_type   * qos_hdr,
  ps_iface_type                 * um_iface_ptr,
  uint8                           version,
  uint16                          ip_id

)
{
  ps_flow_type  * assoc_flow = NULL;
  int             return_val;
  int16           ps_errno;
  ps_flow_type *  flow_ptr = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /* Replace qos handle with flow_ptr if valid */
  if (0 == qmi_qos_get_flow_ptr_from_handle(qos_hdr->qos_handle, &flow_ptr ))
  {
    LOG_MSG_ERROR_1("Flow handle 0x%x passed in is invalid, forwarding onto default flow", qos_hdr->qos_handle);
    flow_ptr = NULL;
  }
  else
  {
    LOG_MSG_INFO3_2("Replacing qos handle 0x%x with flow ptr 0x%x", qos_hdr->qos_handle, flow_ptr);
  }

  if (flow_ptr != NULL)
  {
    LOG_MSG_INFO3_1("got pkt on qos flow 0x%x", flow_ptr);


    if (!PS_FLOW_IS_VALID(flow_ptr))
    {
      LOG_MSG_INFO3_0("fwding pkt on default flow");
      flow_ptr = PS_IFACEI_GET_DEFAULT_FLOW(um_iface_ptr);
    }
    else
    {
      assoc_flow = PS_FLOWI_GET_ASSOC_PS_FLOW(flow_ptr);
      if (assoc_flow != NULL)
      {
        if (!PS_FLOW_IS_VALID(assoc_flow))
        {
          flow_ptr = PS_IFACEI_GET_DEFAULT_FLOW(um_iface_ptr);
        }
        else
        {
          flow_ptr = assoc_flow;
        }
      }
    }

    /*-----------------------------------------------------------------------
      Always fwd on default if flow is not ACTIVATED
    -----------------------------------------------------------------------*/
    if (PS_FLOW_GET_STATE(flow_ptr) != FLOW_ACTIVATED)
    {
      /* Resume flow if it is SUSPENDED, this is needed for NW init QOS on eHRPD*/
      if (PS_FLOW_IS_NW_INITIATED(flow_ptr) &&
          PS_FLOW_GET_STATE(flow_ptr) == FLOW_SUSPENDED)
      {
        LOG_MSG_INFO2_1("Resuming SUSPENDED NW init QOS flow 0x%x", flow_ptr);
        return_val =  ps_flow_ioctl(flow_ptr,
                                    PS_FLOW_IOCTL_QOS_RESUME,
                                    NULL,
                                    &ps_errno);
        if (return_val != 0)
        {
          LOG_MSG_ERROR_1("Error while trying to resume flow %d", ps_errno);
        }
      }
      LOG_MSG_INFO3_0("fwding pkt on default flow");
      flow_ptr = PS_IFACEI_GET_DEFAULT_FLOW(um_iface_ptr);
    }
  }
  else
  {
    flow_ptr = PS_IFACEI_GET_DEFAULT_FLOW(um_iface_ptr);
  }

  /*-------------------------------------------------------------------------
    Update Meta info with routing cache and QoS filter result
  -------------------------------------------------------------------------*/
  PS_TX_META_INFO_GET_ALL(*tx_meta_info_ptr_ptr);
  if (*tx_meta_info_ptr_ptr == NULL ||
      PS_TX_META_GET_RT_META_INFO_PTR(*tx_meta_info_ptr_ptr) == NULL ||
      PS_TX_META_GET_PKT_META_INFO_PTR(*tx_meta_info_ptr_ptr) == NULL)
  {
    LOG_MSG_ERROR_0("Cannot allocate meta_info");
    ASSERT(0);
    return;
  }

  PS_TX_META_SET_ROUTING_CACHE(*tx_meta_info_ptr_ptr, um_iface_ptr);
  PS_TX_META_SET_FILTER_RESULT(*tx_meta_info_ptr_ptr,
                               IP_FLTR_CLIENT_QOS_OUTPUT,
                               flow_ptr);

  /*-------------------------------------------------------------------------
    Handle DoS
  -------------------------------------------------------------------------*/
  if (qos_hdr->flags & ((uint8) PS_OPT_PATH_QOS_FLAG_TYPE_DOS))
  {
    if (version == IP_V4)
    {
      (*tx_meta_info_ptr_ptr)->pkt_meta_info_ptr->tx_flags |= MSG_FAST_EXPEDITE;
      (*tx_meta_info_ptr_ptr)->pkt_meta_info_ptr->dos_ack_handle = ip_id;
      LOG_MSG_INFO2_1("DOS enabled on pkt, dos_ack_handle:0x%x",
                      (*tx_meta_info_ptr_ptr)->pkt_meta_info_ptr->dos_ack_handle);
    }
    else
    {
      LOG_MSG_INFO1_0("Invalid IP version, DOS not enabled");
    }
  }

} /* ps_ul_optimized_process_qos_hdr() */

/*===========================================================================
FUNCTION PS_UL_OPTIMIZED_PROCESS_QOS2_HDR

DESCRIPTION
  This function processes the QOSv2 (8-byte) header.

PARAMETERS
  pkt_ptr              : dsm_item_type rx packet
  tx_meta_info_ptr_ptr : tx meta info pointer to be returned
  qos_hdr              : RmNet QOS header type
  um_iface_ptr         : pointer to Um Iface.
  version              : IP Version
  ip_id                : IP Identification

RETURN VALUE
  None.

DEPENDENCIES
  None

SIDE EFFECTS
  None.

===========================================================================*/

static void ps_ul_optimized_process_qos2_hdr
(
  dsm_item_type                 * pkt_ptr,
  ps_tx_meta_info_type         ** tx_meta_info_ptr_ptr,
  rmnet_meta_smi_qos2_hdr_type   * qos_hdr,
  ps_iface_type                 * um_iface_ptr,
  uint8                           version,
  uint16                          ip_id

)
{
  ps_flow_type  * assoc_flow = NULL;
  int             return_val;
  int16           ps_errno;
  ps_flow_type  * flow_ptr = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /* Replace qos handle with flow_ptr if valid */
  if (0 == qmi_qos_get_flow_ptr_from_handle(qos_hdr->hdr.qos_handle, &flow_ptr ))
  {
    LOG_MSG_ERROR_1("Flow handle 0x%x passed in is invalid, forwarding onto default flow", qos_hdr->hdr.qos_handle);
    flow_ptr = NULL;
  }
  else
  {
    LOG_MSG_INFO3_2("Replacing qos handle 0x%x with flow ptr 0x%x", qos_hdr->hdr.qos_handle, flow_ptr);
  }

  if (flow_ptr != NULL)
  {
    LOG_MSG_INFO3_1("got pkt on qos flow 0x%x", flow_ptr);

    if (!PS_FLOW_IS_VALID(flow_ptr))
    {
      LOG_MSG_INFO3_0("fwding pkt on default flow");
      flow_ptr = PS_IFACEI_GET_DEFAULT_FLOW(um_iface_ptr);
    }
    else
    {
      assoc_flow = PS_FLOWI_GET_ASSOC_PS_FLOW(flow_ptr);
      if (assoc_flow != NULL)
      {
        if (!PS_FLOW_IS_VALID(assoc_flow))
        {
          flow_ptr = PS_IFACEI_GET_DEFAULT_FLOW(um_iface_ptr);
        }
        else
        {
          flow_ptr = assoc_flow;
        }
      }
    }

    /*-----------------------------------------------------------------------
      Always fwd on default if flow is not ACTIVATED
    -----------------------------------------------------------------------*/
    if (PS_FLOW_GET_STATE(flow_ptr) != FLOW_ACTIVATED)
    {
      /* Resume flow if it is SUSPENDED, this is needed for NW init QOS on eHRPD*/
      if (PS_FLOW_IS_NW_INITIATED(flow_ptr) &&
          PS_FLOW_GET_STATE(flow_ptr) == FLOW_SUSPENDED)
      {
        LOG_MSG_INFO2_1("Resuming SUSPENDED NW init QOS flow 0x%x", flow_ptr);
        return_val =  ps_flow_ioctl(flow_ptr,
                                    PS_FLOW_IOCTL_QOS_RESUME,
                                    NULL,
                                    &ps_errno);
        if (return_val != 0)
        {
          LOG_MSG_ERROR_1("Error while trying to resume flow %d", ps_errno);
        }
      }
      LOG_MSG_INFO3_0("fwding pkt on default flow");
      flow_ptr = PS_IFACEI_GET_DEFAULT_FLOW(um_iface_ptr);
    }
  }
  else
  {
    flow_ptr = PS_IFACEI_GET_DEFAULT_FLOW(um_iface_ptr);
  }

  /*-------------------------------------------------------------------------
    Update Meta info with routing cache and QoS filter result
  -------------------------------------------------------------------------*/
  PS_TX_META_INFO_GET_ALL(*tx_meta_info_ptr_ptr);
  if (*tx_meta_info_ptr_ptr == NULL ||
      PS_TX_META_GET_RT_META_INFO_PTR(*tx_meta_info_ptr_ptr) == NULL ||
      PS_TX_META_GET_PKT_META_INFO_PTR(*tx_meta_info_ptr_ptr) == NULL)
  {
    LOG_MSG_ERROR_0("Cannot allocate meta_info");
    ASSERT(0);
    return;
  }

  PS_TX_META_SET_ROUTING_CACHE(*tx_meta_info_ptr_ptr, um_iface_ptr);
  PS_TX_META_SET_FILTER_RESULT(*tx_meta_info_ptr_ptr,
                               IP_FLTR_CLIENT_QOS_OUTPUT,
                               flow_ptr);

  /*-------------------------------------------------------------------------
    Handle DoS
  -------------------------------------------------------------------------*/
  if (qos_hdr->hdr.flags & ((uint8) PS_OPT_PATH_QOS_FLAG_TYPE_DOS))
  {
    if (version == IP_V4)
    {
      (*tx_meta_info_ptr_ptr)->pkt_meta_info_ptr->tx_flags |= MSG_FAST_EXPEDITE;
      (*tx_meta_info_ptr_ptr)->pkt_meta_info_ptr->dos_ack_handle = ip_id;
      LOG_MSG_INFO2_1("DOS enabled on pkt, dos_ack_handle:0x%x",
                      (*tx_meta_info_ptr_ptr)->pkt_meta_info_ptr->dos_ack_handle);
    }
    else
    {
      LOG_MSG_INFO1_0("Invalid IP version, DOS not enabled");
    }
  }

} /* ps_ul_optimized_process_qos2_hdr() */

/*===========================================================================
FUNCTION PS_UL_OPTIMIZED_FALLBACK_HDLR

DESCRIPTION
  This function processes the packet in legacy path.

PARAMETERS
  user_data : pointer to user data
  rx_pkt    : dsm_item_type rx pkt

RETURN VALUE
  TRUE  :
  FALSE :

DEPENDENCIES
  None

SIDE EFFECTS
  None.

===========================================================================*/
static void ps_ul_optimized_fallback_hdlr
(
  void*               user_data,
  dsm_item_type       *rx_pkt
)
{
  rmnet_smi_dual_ip_info_type *  info;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  info = (rmnet_smi_dual_ip_info_type  *) user_data;

  /*-------------------------------------------------------------------------
    Enqueue packet in RmNet legacy Watermark. These packets will handled by
    Legacy Sig hdlr in PS task.
  -------------------------------------------------------------------------*/
  LOG_MSG_INFO4_1 ("ps_ul_optimized_fallback_hdlr():Enqueue pkt to Legacy Wmk:curr_cnt %d",
                   info->legacy_wmk.current_cnt);
  dsm_enqueue(&info->legacy_wmk, &rx_pkt);

  return;
} /* ps_ul_optimized_fallback_hdlr() */

/*===========================================================================
FUNCTION PS_OPT_PATH_MBIM_PROCESS_QCNCM_IP_NO_QOS

DESCRIPTION
  This function IP NO QoS processing fn in QCNCM mode. It decides
  if the packet needs to take the legacy path or should be forwarded to Um
  iface.

PARAMETERS
 user_data :  pointer to user data.

RETURN VALUE
 TRUE :
 FALSE:

DEPENDENCIES
  None

SIDE EFFECTS
  None.

===========================================================================*/
static uint8 ps_ul_optimized_process_qcncm_ip_no_qos
(
  void                * user_data,
  dsm_item_type       * rx_pkt
)
{
  ps_ul_optimized_hdlr_path_type  path_type = PS_UL_OPTIMIZED_HDLR_NO_PATH;
  rmnet_smi_dual_ip_info_type  * info;
  ps_tx_meta_info_type         * tx_meta_info_ptr = NULL;
  rmnet_smi_info_type          * temp_info;
  ps_iface_type                * um_base_iface_ptr = NULL;
  ps_phys_link_type            * ps_phys_link_ptr;
  ps_flow_type                 * ps_flow_ptr = NULL;
  uint16                         total_len = 0;

  uint8    pkt_buf[PS_OPT_PATH_IP_NO_QOS_HEADER_LEN + 1] ALIGN(4); //__attribute__ ((aligned(4)));
  uint8    ip4_hdr_len = 0;
  uint8    ip_hdr_len  = 0;
  uint8    prot_offset = 0;
  boolean  rval        = FALSE;
  boolean  enable      = FALSE;
  sint15   ps_errno;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_INFO3_0("In ps_ul_optimized_process_qcncm_ip_no_qos");

  PS_ENTER_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
  PS_ENTER_CRIT_SECTION(&global_ps_crit_section);

  info = (rmnet_smi_dual_ip_info_type  *) user_data;

  do
  {

    if ( PS_OPT_PATH_IP_NO_QOS_HEADER_LEN_CHECK >
           dsm_extract(rx_pkt, 0, pkt_buf,
                       PS_OPT_PATH_IP_NO_QOS_HEADER_LEN))
    {
      path_type = PS_UL_OPTIMIZED_HDLR_LEGACY_PATH;
      LOG_MSG_INFO3_0("goto legacy = true: Header length check");
      break;
    }

    /*-----------------------------------------------------------------------
      Do appropriate processing for IPv4 and IPv6 packets
    -----------------------------------------------------------------------*/

    LOG_MSG_INFO3_1("IP version: 0x%x ", *(pkt_buf) );

    if ((*(pkt_buf) & PS_OPT_PATH_IP_VERSION_MASK) ==
        PS_OPT_PATH_V4_VERSION) /*IPv4*/
    {
      temp_info   = info->info_v4;
      ip4_hdr_len = (*(pkt_buf) & PS_OPT_PATH_IP_HDR_LEN_MASK) *
                     PS_OPT_PATH_WORD_SIZE;

      /*---------------------------------------------------------------------
        Update ip_hdr_len and protocol offset for TCP ACK prioritization
      ---------------------------------------------------------------------*/
       ip_hdr_len  = ip4_hdr_len;
       prot_offset = PS_OPT_PATH_V4_PROT_FIELD_OFFSET;

      /*---------------------------------------------------------------------
        Ensure that IPv4 call is UP
      ---------------------------------------------------------------------*/
      if (RMNET_META_SM_NULL_STATE != temp_info->meta_sm.state &&
          PS_IFACE_IS_VALID(temp_info->um_iface_ptr))
      {
        /*-------------------------------------------------------------------
          Flow control mechanism
          1.Single IPv4 call - Flow control if v4 Um is flow ctrlled.
          2.Dual-IP call(Single APN/Dual APN) - Flow ctrl only when v4 and
                                                v6 is flow ctrlled.
        -------------------------------------------------------------------*/

        ps_flow_ptr      = PS_IFACEI_GET_DEFAULT_FLOW(temp_info->um_iface_ptr);
        ps_phys_link_ptr = PS_IFACEI_GET_PHYS_LINK( temp_info->um_iface_ptr);

        /*-------------------------------------------------------------------
          Check if v4 Um is flow ctrlled
        -------------------------------------------------------------------*/
        if (!(PS_FLOW_IS_TX_ENABLED(ps_flow_ptr)) ||
            !(PS_PHYS_LINK_FLOW_ENABLED( ps_phys_link_ptr)) ||
            !(PS_IFACEI_FLOW_ENABLED(temp_info->um_iface_ptr)))
        {

          /*-----------------------------------------------------------------
            Check if IPv6 call up
          -----------------------------------------------------------------*/
          if (RMNET_META_SM_NULL_STATE != info->info_v6->meta_sm.state &&
              PS_IFACE_IS_VALID(info->info_v6->um_iface_ptr))
          {

            /*---------------------------------------------------------------
              Its a Dual IP call. Check flow ctrl on V6.
            ---------------------------------------------------------------*/
            if (!(PS_FLOW_IS_TX_ENABLED(
                  PS_IFACEI_GET_DEFAULT_FLOW(info->info_v6->um_iface_ptr))) ||
                !(PS_PHYS_LINK_FLOW_ENABLED(
                  PS_IFACEI_GET_PHYS_LINK(info->info_v6->um_iface_ptr))) ||
                !(PS_IFACEI_FLOW_ENABLED(info->info_v6->um_iface_ptr)))
            {
              /*-------------------------------------------------------------
                Since Flow is disabled on both V4 and V6 APNs we can return
                TRUE and stop de-queuing pkts.
              -------------------------------------------------------------*/
              LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_qcncm_ip_no_qos():"
                               "Flow disabled on both V4V6 in a Dual IP call");
              rval = TRUE;
              break;
            }

            /*---------------------------------------------------------------
              Flow is enabled on V6 APN and disabled on V4 in Dual-IP call.
              So we need to continue de-queueing pkts so that V6 data flow is
              On.Push this V4 pkt to Um though flow controlled and let Um Wmk
              handle it .
            ---------------------------------------------------------------*/
            LOG_MSG_ERROR_0 ("ps_ul_optimized_process_qcncm_ip_no_qos():"
                             "V4:Flow disabled , V6: Enabled in Dual IP call"
                             "Cont to dequeue..");

          }/*V6 call up*/
          else
          {
            LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_qcncm_ip_no_qos():"
                             "Flow disabled on Single V4 call");

            rval = TRUE; /*Outstanding packet*/

            /*return TRUE and is an outstanding packet*/
            //rval = PS_OPT_PATH_OUTSTANDING_TRUE;
            break;
          }
        }/*V4 flow controlled*/

        /*-------------------------------------------------------------------
          Send packets up the stack for following cases
          1. If packet is destined to a broadcast address
          2. if packet is destined to a limited broadcast address
          3. If packet is destined to a multicast address
          4. If packet is fragmented
          5. if packet is destined to Rm iface
          6. if IP header has options
        -------------------------------------------------------------------*/
        if(ps_ul_optmized_hdlr_ipv4_is_legacy( pkt_buf,
                                             temp_info,
                                             ip4_hdr_len) == TRUE)
        {
          LOG_MSG_INFO3_0("IPV4_NO_QOS: goto_legacy = TRUE");

          /*Check if Legacy Wmk is flow controlled*/
          if(!(ps_ul_optimized_hdlr_legacy_flow_enabled(info)))
          {
            LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_qcncm_ip_no_qos():"
                             "Legacy Wmk Flow disabled");
            rval = TRUE; /*Outstanding packet*/
          }
          else
          {
            path_type = PS_UL_OPTIMIZED_HDLR_LEGACY_PATH;
          }

          break;
        }
      }/*V4 SM up*/
      else
      {
        LOG_MSG_ERROR_0("V4 SM not up. Dropping packet");
        dsm_free_packet(&rx_pkt); /*XXX*/
        break;
      }

      total_len = ps_htons( (uint16) *( (uint16 *) (pkt_buf +
                               PS_OPT_PATH_V4_TOTAL_LEN_OFFSET)));

      /*---------------------------------------------------------------------
       LOW LATENCY TRAFFIC handling
      ---------------------------------------------------------------------*/
      ps_ul_optimized_v4_low_latency_traffic_handler ( pkt_buf,
                                                       ip_hdr_len,
                                                       prot_offset );

    }

    else if (( *(pkt_buf) & PS_OPT_PATH_IP_VERSION_MASK) ==
               PS_OPT_PATH_V6_VERSION) /*IPv6*/
    {
      temp_info = info->info_v6;

      /*---------------------------------------------------------------------
        Update ip_hdr_len and protocol offset for TCP ACK prioritization
      ---------------------------------------------------------------------*/
       ip_hdr_len  = PS_OPT_PATH_V6_HDR_LEN;
       prot_offset = PS_OPT_PATH_V6_NXT_HDR_OFFSET;

      /*---------------------------------------------------------------------
        Ensure that IPv6 call is UP
      ---------------------------------------------------------------------*/
      if (RMNET_V6_SM_NULL_STATE != temp_info->v6_sm.state &&
          PS_IFACE_IS_VALID(temp_info->um_iface_ptr))
      {
        /*-------------------------------------------------------------------
          Flow control mechanism
          1.Single IPv6 call - Flow control if v6 Um is flow ctrlled.
          2.Dual-IP call(Single APN/Dual APN) - Flow ctrl only when v4 and
                                                v6 is flow ctrlled.
        -------------------------------------------------------------------*/
        ps_flow_ptr      = PS_IFACEI_GET_DEFAULT_FLOW(temp_info->um_iface_ptr);
        ps_phys_link_ptr = PS_IFACEI_GET_PHYS_LINK( temp_info->um_iface_ptr);

        /*-------------------------------------------------------------------
          Check if v6 Um is flow ctrlled
        -------------------------------------------------------------------*/
        if (!(PS_FLOW_IS_TX_ENABLED(ps_flow_ptr)) ||
            !(PS_PHYS_LINK_FLOW_ENABLED( ps_phys_link_ptr)) ||
            !(PS_IFACEI_FLOW_ENABLED(temp_info->um_iface_ptr)))
        {

          /*-----------------------------------------------------------------
            Check if IPv4 call up
          -----------------------------------------------------------------*/

          if (RMNET_META_SM_NULL_STATE != info->info_v4->meta_sm.state &&
              PS_IFACE_IS_VALID(info->info_v4->um_iface_ptr))
          {

            /*---------------------------------------------------------------
              Its a Dual IP call. Check flow ctrl on V4.
            ---------------------------------------------------------------*/

            if (!(PS_FLOW_IS_TX_ENABLED(
                  PS_IFACEI_GET_DEFAULT_FLOW(info->info_v4->um_iface_ptr))) ||
                !(PS_PHYS_LINK_FLOW_ENABLED(
                  PS_IFACEI_GET_PHYS_LINK(info->info_v4->um_iface_ptr))) ||
                !(PS_IFACEI_FLOW_ENABLED(info->info_v4->um_iface_ptr)))
            {
              /*-------------------------------------------------------------
                Since Flow is disabled on both V4 and V6 APNs we can return
                TRUE and stop de-queuing pkts.
              -------------------------------------------------------------*/
              LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_qcncm_ip_no_qos():"
                               "Flow disabled on both V4V6 in a Dual IP call");
              rval = TRUE;
              break;
            }

            /*---------------------------------------------------------------
              Flow is enabled on V4 APN and disabled on V6 in Dual-IP call.
              So we need to continue de-queueing pkts so that V6 data flow is
              On.Push this V6 pkt to Um though flow controlled and let Um Wmk
              handle it .
            ---------------------------------------------------------------*/
            LOG_MSG_ERROR_0 ("ps_ul_optimized_process_qcncm_ip_no_qos():"
                             "V6:Flow disabled , V4: Enabled in Dual IP call"
                             "Cont to dequeue..");

          }/*V4 call up*/
          else
          {
            LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_qcncm_ip_no_qos():"
                             "Flow disabled on Single V6 call");

            rval = TRUE; //Outstanding packet

            /*return TRUE and is an outstanding packet*/
            //rval = PS_OPT_PATH_OUTSTANDING_TRUE;
            break;
          }
        }/*V6 flow controlled*/

        /*-------------------------------------------------------------------
          Send packets up the stack for following cases
          1. If packet is destined to a linklocal address
          2. If packet is destined to a multicast address
        -------------------------------------------------------------------*/
        if(ps_ul_optimized_ipv6_is_legacy(pkt_buf) == TRUE)
        {

          /*Check if Legacy Wmk is flow controlled*/
          if(!(ps_ul_optimized_hdlr_legacy_flow_enabled(info)))
          {
            LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_qcncm_ip_no_qos():"
                             "Legacy Wmk Flow disabled");
            rval = TRUE; /*Outstanding packet*/
          }
          else
          {
            path_type = PS_UL_OPTIMIZED_HDLR_LEGACY_PATH;
          }
          break;
        }
      }/*V6 SM up*/
      else
      {
        LOG_MSG_ERROR_0("V6 SM not up. Dropping packet");
        dsm_free_packet(&rx_pkt); /*XXX*/
        break;
      }

      /*Drop packet if prefix does not match*/
      if(ps_ul_optimized_ipv6_prefix_cmp(pkt_buf, temp_info) == FALSE)
      {
        dsm_free_packet(&rx_pkt);
        break;
      }

      total_len  = ps_htons( (uint16) *((uint16 *) (pkt_buf +
                                    PS_OPT_PATH_V6_TOTAL_LEN_OFFSET)));
      total_len += PS_OPT_PATH_V6_HDR_LEN;
    }
    else
    {
      LOG_MSG_ERROR_0("Invalid IP version.Dropping packet");
      dsm_free_packet(&rx_pkt); /*XXX*/
      break;
    }

    /*-----------------------------------------------------------------------
      Increment the receive packet stats
    -----------------------------------------------------------------------*/
    temp_info->rmnet_iface_ptr->iface_i_stats.pkts_rx++;
    temp_info->rmnet_iface_ptr->iface_i_stats.bytes_rx += total_len;

    /*-----------------------------------------------------------------------
      Log the packet on the Rx side
    -----------------------------------------------------------------------*/
    DPL_LOG_NETWORK_RX_PACKET(temp_info->rmnet_iface_ptr,
                              rx_pkt,
                              DPL_IID_NETPROT_IP);

    /*---------------------------------------------------------------------
        TCP ACK prioritization
    ---------------------------------------------------------------------*/
    if (ps_ul_optimized_tcp_ack_pri_check
        (
          pkt_buf,
          ip_hdr_len,
          total_len,
          prot_offset
        ) == TRUE
       )
    {
      LOG_MSG_INFO3_0("IP_NO_QOS: TCP_ACK_PRIORITY: Tagging DSM_HIGHEST");
      (rx_pkt)->priority = DSM_HIGHEST;
    }

    um_base_iface_ptr = PS_IFACE_GET_BASE_IFACE(temp_info->um_iface_ptr);

    if (PS_IFACEI_NUM_FILTERS(um_base_iface_ptr, IP_FLTR_CLIENT_QOS_OUTPUT) > 0)
    {
      path_type = PS_UL_OPTIMIZED_HDLR_OPT_QOS_PATH;
    }
    else
    {
      /*--------------------------------------------------------------------
        If flow capability is set to PS_FLOW_CAPABILITY_DATA_DISALLOWED
        drop the packet
      --------------------------------------------------------------------*/
      if ((PS_FLOW_GET_CAPABILITY(ps_flow_ptr,
              PS_FLOW_CAPABILITY_DATA_DISALLOWED)))
      {
        LOG_MSG_ERROR_0("PS_FLOW_CAPABILITY_DATA_DISALLOWED:Dropping packet" );
        dsm_free_packet(&rx_pkt);
        rx_pkt = NULL;
        break;
      }

      PS_TX_META_INFO_AND_RT_META_INFO_GET(tx_meta_info_ptr);
      if (tx_meta_info_ptr == NULL ||
          PS_TX_META_GET_RT_META_INFO_PTR(tx_meta_info_ptr) == NULL)
      {
        LOG_MSG_ERROR_1("IP_NO_QOS: Cannot allocate meta info tx 0x%p",
                        tx_meta_info_ptr);
        dsm_free_packet(&rx_pkt); /*XXX*/
        rx_pkt = NULL;
        break;
      }

      PS_TX_META_SET_ROUTING_CACHE(tx_meta_info_ptr, um_base_iface_ptr);

      um_base_iface_ptr->iface_i_stats.pkts_tx++;
      um_base_iface_ptr->iface_i_stats.bytes_tx += total_len;

      /*---------------------------------------------------------------------
        Log packet on the Tx side
      ---------------------------------------------------------------------*/
      DPL_LOG_NETWORK_TX_PACKET(um_base_iface_ptr,
                                rx_pkt,
                                DPL_IID_NETPROT_IP);

      path_type = PS_UL_OPTIMIZED_HDLR_OPT_PATH;
    }
  } while (0);

  /*-------------------------------------------------------------------------
   Leave CS before tx_cmd to avoid possible deadlock issues.
  -------------------------------------------------------------------------*/
  PS_LEAVE_CRIT_SECTION(&global_ps_crit_section);
  PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);

  /*-------------------------------------------------------------------------
   Pass packet to optimized or legacy path based on path.
  -------------------------------------------------------------------------*/
  switch (path_type)
  {
    case PS_UL_OPTIMIZED_HDLR_OPT_PATH:
    {
      /*-----------------------------------------------------------------------
        Make sure that the power save mode is disabled when recieving the first 
        uplink packet on this path
        -----------------------------------------------------------------------*/
      if( TRUE == PS_IFACE_GET_POWERSAVE_FILTERING_MODE(temp_info->rmnet_iface_ptr) )
      {
        if( PS_IFACE_GET_POWERSAVE_FILTERING_AUTO_EXIT_MODE(temp_info->rmnet_iface_ptr) )
        {
          ps_iface_set_powersave_filtering_mode( temp_info->rmnet_iface_ptr, 
                                                 enable, &ps_errno );
        }
      }

      um_base_iface_ptr->iface_private.tx_cmd
      (
        um_base_iface_ptr,
        &rx_pkt,
        tx_meta_info_ptr,
        um_base_iface_ptr->iface_private.tx_cmd_info
      );

      rx_pkt = NULL;
      break;
    }

    case PS_UL_OPTIMIZED_HDLR_OPT_QOS_PATH:
    {
      /*-----------------------------------------------------------------------
        Make sure that the power save mode is disabled when recieving the first 
        uplink packet on this path
        -----------------------------------------------------------------------*/
      if( TRUE == PS_IFACE_GET_POWERSAVE_FILTERING_MODE(temp_info->rmnet_iface_ptr) )
      {
        if( PS_IFACE_GET_POWERSAVE_FILTERING_AUTO_EXIT_MODE(temp_info->rmnet_iface_ptr) )
        {
          ps_iface_set_powersave_filtering_mode( temp_info->rmnet_iface_ptr, 
                                                 enable, &ps_errno );
        }
      }

      ps_iface_tx_cmd(um_base_iface_ptr, &rx_pkt, NULL);
      rx_pkt = NULL;
      break;
    }

    case PS_UL_OPTIMIZED_HDLR_LEGACY_PATH:
    {
      ps_ul_optimized_fallback_hdlr(user_data, rx_pkt);
      rx_pkt = NULL;
      break;
    }

    case PS_UL_OPTIMIZED_HDLR_NO_PATH:
    {
      PS_TX_META_INFO_FREE(&tx_meta_info_ptr);
      break;
    }

    default:
    {
      LOG_MSG_ERROR_0("ps_ul_optimized_hdlr_path_type error");
      ASSERT(0);
      break;
    }
  }

  return rval;

} /* ps_ul_optimized_process_qcncm_ip_no_qos() */

#ifdef FEATURE_DATA_MBIM
/*===========================================================================
FUNCTION PS_OPT_PATH_MBIM_PROCESS_MBIM_IP_NO_QOS

DESCRIPTION
  This function is the IP NO QOS processing fn in MBIM mode. It decides
  if the packet needs to take the legacy path or should be forwarded to Um
  iface.

PARAMETERS
 user_data :  pointer to user data.

RETURN VALUE
 TRUE :
 FALSE:

DEPENDENCIES
  None

SIDE EFFECTS
  None.

===========================================================================*/
static uint8 ps_ul_optimized_process_mbim_ip_no_qos
(
  void                * user_data,
  dsm_item_type       * rx_pkt
)
{
  ps_ul_optimized_hdlr_path_type  path_type = PS_UL_OPTIMIZED_HDLR_NO_PATH;
  rmnet_smi_dual_ip_info_type  * info;
  ps_tx_meta_info_type         * tx_meta_info_ptr = NULL;
  rmnet_smi_info_type          * temp_info = NULL;
  ps_iface_type                * um_base_iface_ptr = NULL;
  ps_phys_link_type            * ps_phys_link_ptr;
  ps_flow_type                 * ps_flow_ptr = NULL;
  uint16                         total_len = 0;

  uint8    pkt_buf[PS_OPT_PATH_IP_NO_QOS_HEADER_LEN + 1] __attribute__ ((aligned(4)));
  uint8    ip4_hdr_len = 0;
  uint8    ip_hdr_len  = 0;
  uint8    prot_offset = 0;
  boolean  rval        = FALSE;
  boolean  enable      = FALSE;
  sint15   ps_errno;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_INFO3_0("In ps_ul_optimized_process_mbim_ip_no_qos");

  PS_ENTER_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
  PS_ENTER_CRIT_SECTION(&global_ps_crit_section);

  info = (rmnet_smi_dual_ip_info_type  *) user_data;

  do
  {

    if ( PS_OPT_PATH_IP_NO_QOS_HEADER_LEN_CHECK >
           dsm_extract(rx_pkt, 0, pkt_buf,
                       PS_OPT_PATH_IP_NO_QOS_HEADER_LEN))
    {
      path_type = PS_UL_OPTIMIZED_HDLR_LEGACY_PATH;
      LOG_MSG_INFO3_0("goto legacy = true: Header length check");
      break;
    }

    /*-----------------------------------------------------------------------
      Do appropriate processing for IPv4 and IPv6 packets
    -----------------------------------------------------------------------*/

    LOG_MSG_INFO3_1("IP version: 0x%x ", *(pkt_buf) );

    if ((*(pkt_buf) & PS_OPT_PATH_IP_VERSION_MASK) ==
        PS_OPT_PATH_V4_VERSION) /*IPv4*/
    {
      temp_info   = info->info_v4;
      LOG_MSG_INFO3_1("rmnet_inst %d ",
                      RMNET_META_SM_INFO_TO_INSTANCE(temp_info));
      ip4_hdr_len = (*(pkt_buf) & PS_OPT_PATH_IP_HDR_LEN_MASK) *
                     PS_OPT_PATH_WORD_SIZE;

      /*---------------------------------------------------------------------
        Update ip_hdr_len and protocol offset for TCP ACK prioritization
      ---------------------------------------------------------------------*/
       ip_hdr_len  = ip4_hdr_len;
       prot_offset = PS_OPT_PATH_V4_PROT_FIELD_OFFSET;

      /*---------------------------------------------------------------------
        Ensure that IPv4 call is UP
      ---------------------------------------------------------------------*/
      if (RMNET_META_SM_NULL_STATE != temp_info->meta_sm.state &&
          PS_IFACE_IS_VALID(temp_info->um_iface_ptr))
      {

        ps_flow_ptr      = PS_IFACEI_GET_DEFAULT_FLOW(temp_info->um_iface_ptr);
        ps_phys_link_ptr = PS_IFACEI_GET_PHYS_LINK( temp_info->um_iface_ptr);

        /*-------------------------------------------------------------------
          Check if v4 Um is flow ctrlled
        -------------------------------------------------------------------*/
        if (!(PS_FLOW_IS_TX_ENABLED(ps_flow_ptr)) ||
            !(PS_PHYS_LINK_FLOW_ENABLED( ps_phys_link_ptr)) ||
            !(PS_IFACEI_FLOW_ENABLED(temp_info->um_iface_ptr)))
        {

          LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_mbim_ip_no_qos():"
                           "Flow disabled on Single V4 call");

          rval = TRUE; /*Outstanding packet*/

          /*return TRUE and is an outstanding packet*/
          //rval = PS_OPT_PATH_OUTSTANDING_TRUE;
          break;

        }/*V4 flow controlled*/


      }/*V4 SM up*/
      else
      {
        LOG_MSG_ERROR_0("V4 SM not up. Dropping packet");
        dsm_free_packet(&rx_pkt); /*XXX*/
        break;
      }

      /*-------------------------------------------------------------------
          Send packets up the stack for following cases
          1. If packet is destined to a broadcast address
          2. if packet is destined to a limited broadcast address
          3. If packet is destined to a multicast address
          4. If packet is fragmented
          5. if packet is destined to Rm iface
          6. if IP header has options
        -------------------------------------------------------------------*/
        if(ps_ul_optmized_hdlr_ipv4_is_legacy( pkt_buf,
                                             temp_info,
                                             ip4_hdr_len) == TRUE)
        {
          LOG_MSG_INFO3_0("MBIM_IPV4_NO_QOS: goto_legacy = TRUE");

          /*Check if Legacy Wmk is flow controlled*/
          if(!(ps_ul_optimized_hdlr_legacy_flow_enabled(info)))
          {
            LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_mbim_ip_no_qos():"
                             "Legacy Wmk Flow disabled");
            rval = TRUE; /*Outstanding packet*/
          }
          else
          {
            path_type = PS_UL_OPTIMIZED_HDLR_LEGACY_PATH;
          }

          break;
        }
        else
        {
          total_len = ps_htons( (uint16) *( (uint16 *) (pkt_buf +
                               PS_OPT_PATH_V4_TOTAL_LEN_OFFSET)));
        }
        /*---------------------------------------------------------------------
         LOW LATENCY TRAFFIC handling
        ---------------------------------------------------------------------*/
        ps_ul_optimized_v4_low_latency_traffic_handler ( pkt_buf,
                                                         ip_hdr_len,
                                                         prot_offset );

    }

    else if (( *(pkt_buf) & PS_OPT_PATH_IP_VERSION_MASK) ==
               PS_OPT_PATH_V6_VERSION) /*IPv6*/
    {
      temp_info = info->info_v6;
      LOG_MSG_INFO3_1("rmnet_inst %d ",
                      RMNET_META_SM_INFO_TO_INSTANCE(temp_info));
      /*---------------------------------------------------------------------
        Update ip_hdr_len and protocol offset for TCP ACK prioritization
      ---------------------------------------------------------------------*/
       ip_hdr_len  = PS_OPT_PATH_V6_HDR_LEN;
       prot_offset = PS_OPT_PATH_V6_NXT_HDR_OFFSET;

      /*---------------------------------------------------------------------
        Ensure that IPv6 call is UP
      ---------------------------------------------------------------------*/
      if (RMNET_V6_SM_NULL_STATE != temp_info->v6_sm.state &&
          PS_IFACE_IS_VALID(temp_info->um_iface_ptr))
      {

        ps_flow_ptr      = PS_IFACEI_GET_DEFAULT_FLOW(temp_info->um_iface_ptr);
        ps_phys_link_ptr = PS_IFACEI_GET_PHYS_LINK( temp_info->um_iface_ptr);

        /*-------------------------------------------------------------------
          Check if v6 Um is flow ctrlled
        -------------------------------------------------------------------*/
        if (!(PS_FLOW_IS_TX_ENABLED(ps_flow_ptr)) ||
            !(PS_PHYS_LINK_FLOW_ENABLED( ps_phys_link_ptr)) ||
            !(PS_IFACEI_FLOW_ENABLED(temp_info->um_iface_ptr)))
        {

          LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_mbim_ip_no_qos():"
                           "Flow disabled on Single V6 call");

          rval = TRUE; //Outstanding packet

          /*return TRUE and is an outstanding packet*/
          //rval = PS_OPT_PATH_OUTSTANDING_TRUE;
          break;

        }/*V6 flow controlled*/


      }/*V6 SM up*/
      else
      {
        LOG_MSG_ERROR_0("V6 SM not up. Dropping packet");
        dsm_free_packet(&rx_pkt); /*XXX*/
        break;
      }

      /*-------------------------------------------------------------------
        Send packets up the stack for following cases
        1. If packet is destined to a linklocal address
        2. If packet is destined to a multicast address
      -------------------------------------------------------------------*/
      if(ps_ul_optimized_ipv6_is_legacy(pkt_buf) == TRUE)
      {
        LOG_MSG_INFO3_0("IPV6_NO_QOS: goto_legacy = TRUE");

        /*Check if Legacy Wmk is flow controlled*/
        if(!(ps_ul_optimized_hdlr_legacy_flow_enabled(info)))
        {
          LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_mbim_ip_no_qos():"
                           "Legacy Wmk Flow disabled");
          rval = TRUE; /*Outstanding packet*/
        }
        else
        {
          path_type = PS_UL_OPTIMIZED_HDLR_LEGACY_PATH;
        }
        break;
      }

      /*Drop packet if prefix does not match*/
      if(ps_ul_optimized_ipv6_prefix_cmp(pkt_buf, temp_info) == FALSE)
      {
        dsm_free_packet(&rx_pkt);
        break;
      }

      total_len  = ps_htons( (uint16) *((uint16 *) (pkt_buf +
                                    PS_OPT_PATH_V6_TOTAL_LEN_OFFSET)));
      total_len += PS_OPT_PATH_V6_HDR_LEN;

    }
    else
    {
      LOG_MSG_ERROR_0("Invalid IP version.Dropping packet");
      dsm_free_packet(&rx_pkt); /*XXX*/
      break;
    }

    /*-----------------------------------------------------------------------
      Increment the receive packet stats
    -----------------------------------------------------------------------*/
    temp_info->rmnet_iface_ptr->iface_i_stats.pkts_rx++;
    temp_info->rmnet_iface_ptr->iface_i_stats.bytes_rx += total_len;

    /*-----------------------------------------------------------------------
      Log the packet on the Rx side
    -----------------------------------------------------------------------*/
    DPL_LOG_NETWORK_RX_PACKET(temp_info->rmnet_iface_ptr,
                              rx_pkt,
                              DPL_IID_NETPROT_IP);

    /*---------------------------------------------------------------------
        TCP ACK prioritization
    ---------------------------------------------------------------------*/
    if (ps_ul_optimized_tcp_ack_pri_check
        (
          pkt_buf,
          ip_hdr_len,
          total_len,
          prot_offset
        ) == TRUE
       )
    {
      LOG_MSG_INFO3_0("IP_NO_QOS: TCP_ACK_PRIORITY: Tagging DSM_HIGHEST");
      if(NULL != rx_pkt)
      {
        (rx_pkt)->priority = DSM_HIGHEST;
      }
    }

    um_base_iface_ptr = PS_IFACE_GET_BASE_IFACE(temp_info->um_iface_ptr);

    if (PS_IFACEI_NUM_FILTERS(um_base_iface_ptr, IP_FLTR_CLIENT_QOS_OUTPUT) > 0)
    {
      path_type = PS_UL_OPTIMIZED_HDLR_OPT_QOS_PATH;
    }
    else
    {
      /*--------------------------------------------------------------------
        If flow capability is set to PS_FLOW_CAPABILITY_DATA_DISALLOWED
        drop the packet
      --------------------------------------------------------------------*/
      if ((PS_FLOW_GET_CAPABILITY(ps_flow_ptr,
              PS_FLOW_CAPABILITY_DATA_DISALLOWED)))
      {
        LOG_MSG_ERROR_0("PS_FLOW_CAPABILITY_DATA_DISALLOWED:Dropping packet" );
        dsm_free_packet(&rx_pkt);
        rx_pkt = NULL;
        break;
      }

      PS_TX_META_INFO_AND_RT_META_INFO_GET(tx_meta_info_ptr);
      if (tx_meta_info_ptr == NULL ||
          PS_TX_META_GET_RT_META_INFO_PTR(tx_meta_info_ptr) == NULL)
      {
        LOG_MSG_ERROR_1("IP_NO_QOS: Cannot allocate meta info tx 0x%p",
                        tx_meta_info_ptr);
        dsm_free_packet(&rx_pkt); /*XXX*/
        rx_pkt = NULL;
        break;
      }

      PS_TX_META_SET_ROUTING_CACHE(tx_meta_info_ptr, um_base_iface_ptr);

      um_base_iface_ptr->iface_i_stats.pkts_tx++;
      um_base_iface_ptr->iface_i_stats.bytes_tx += total_len;

      /*---------------------------------------------------------------------
        Log packet on the Tx side
      ---------------------------------------------------------------------*/
      DPL_LOG_NETWORK_TX_PACKET(um_base_iface_ptr,
                                rx_pkt,
                                DPL_IID_NETPROT_IP);

      path_type = PS_UL_OPTIMIZED_HDLR_OPT_PATH;
    }
  } while (0);

  /*-------------------------------------------------------------------------
   Leave CS before tx_cmd to avoid possible deadlock issues.
  -------------------------------------------------------------------------*/
  PS_LEAVE_CRIT_SECTION(&global_ps_crit_section);
  PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);

  /*-------------------------------------------------------------------------
   Pass packet to optimized or legacy path based on path.
  -------------------------------------------------------------------------*/
  switch (path_type)
  {
    case PS_UL_OPTIMIZED_HDLR_OPT_PATH:
    {
      /*-----------------------------------------------------------------------
      Make sure that the power save mode is disabled when recieving the 
      first uplink packet on this path
      -------------------------------------------------------------------------*/
      if( TRUE == PS_IFACE_GET_POWERSAVE_FILTERING_MODE(temp_info->rmnet_iface_ptr) )
      {
        if( PS_IFACE_GET_POWERSAVE_FILTERING_AUTO_EXIT_MODE(temp_info->rmnet_iface_ptr) )
        {
          ps_iface_set_powersave_filtering_mode( temp_info->rmnet_iface_ptr, 
                                                 enable, &ps_errno );
        }
      }

      um_base_iface_ptr->iface_private.tx_cmd
      (
        um_base_iface_ptr,
        &rx_pkt,
        tx_meta_info_ptr,
        um_base_iface_ptr->iface_private.tx_cmd_info
      );

      rx_pkt = NULL;
      break;
    }

    case PS_UL_OPTIMIZED_HDLR_OPT_QOS_PATH:
    {
      /*-----------------------------------------------------------------------
      Make sure that the power save mode is disabled when recieving the 
      first uplink packet on this path
      -------------------------------------------------------------------------*/
      if( TRUE == PS_IFACE_GET_POWERSAVE_FILTERING_MODE(temp_info->rmnet_iface_ptr) )
      {
        if( PS_IFACE_GET_POWERSAVE_FILTERING_AUTO_EXIT_MODE(temp_info->rmnet_iface_ptr) )
        {
          ps_iface_set_powersave_filtering_mode( temp_info->rmnet_iface_ptr, 
                                                 enable, &ps_errno );
        }
      }

      ps_iface_tx_cmd(um_base_iface_ptr, &rx_pkt, NULL);
      rx_pkt = NULL;
      break;
    }

    case PS_UL_OPTIMIZED_HDLR_LEGACY_PATH:
    {
      ps_ul_optimized_fallback_hdlr(user_data, rx_pkt);
      rx_pkt = NULL;
      break;
    }

    case PS_UL_OPTIMIZED_HDLR_NO_PATH:
    {
      PS_TX_META_INFO_FREE(&tx_meta_info_ptr);
      break;
    }

    default:
    {
      LOG_MSG_ERROR_0("ps_ul_optimized_hdlr_path_type error");
      ASSERT(0);
      break;
    }
  }

  return rval;

} /* ps_ul_optimized_process_mbim_ip_no_qos() */

/*===========================================================================
FUNCTION PS_UL_OPTIMIZED_HDLR_MBIM_NDP_PROCESS

DESCRIPTION
  This function processes the NDP in MBIM packet.

PARAMETERS
  user_data           :  pointer to user data.
  ndp_datagram_offset :  IP/Eth offset in NDP
  ntb_dsm_item        :  MBIM pkt
  rx_pkt              :  IP/ETH pkt
  ndp_buf             :  ndp buffer

RETURN VALUE
  TRUE  :
  FALSE :

DEPENDENCIES
  None

SIDE EFFECTS
  None
===========================================================================*/
static uint8 ps_ul_optimized_hdlr_mbim_ndp_process
(
  void                         * user_data,
  uint16                         ndp_datagram_offset,
  dsm_item_type                * ntb_dsm_item,
  dsm_item_type               ** rx_pkt,
  uint8                        * ndp_buf,
  ps_iface_mbim_ntb_nth_hdr      nth_hdr,
  rmnet_smi_dual_ip_info_type  * info,
  uint16 ndp_offset
)
{
  ps_iface_mbim_ntb_ndp_ip_datagram_info * ndp_ip_datagram_info;
  rmnet_mbim_call_info_type              * call_info;
  ps_iface_mbim_ntb_ndp_hdr              * ndp_hdr;
  uint8 rval = FALSE;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_INFO3_0 ("IN->ps_ul_optimized_hdlr_mbim_ndp_process");

  call_info = (rmnet_mbim_call_info_type*)user_data;

  ndp_ip_datagram_info = (ps_iface_mbim_ntb_ndp_ip_datagram_info *)
                           (ndp_buf + PS_OPT_PATH_NDP_HDR_LEN) ;
  ndp_hdr              = (ps_iface_mbim_ntb_ndp_hdr *)ndp_buf;


  while ( ndp_datagram_offset < ndp_hdr->len )
  {

    LOG_MSG_INFO3_1("Start to process ndp_dg_offset %d", ndp_datagram_offset);

    /*---------------------------------------------------------------------
      Validate datagram Index and length and perform IP processing
      dgindex == 0 -> Terminate
    ---------------------------------------------------------------------*/
    if ( ndp_ip_datagram_info->index == 0)
    {
      LOG_MSG_INFO3_1("ndp datagram %d terminated with zero",
                      ndp_datagram_offset);

      break; /*break and continue with next NDP*/
    }
    else  /*Valid IP packet to process*/
    {
      LOG_MSG_INFO3_2( "Process d/g nth_datagram_offset: %d,"
                       "ndp_datagram_offset:%d",
                       ndp_ip_datagram_info->index, ndp_datagram_offset);

      LOG_MSG_INFO3_1 ("dsm_dup of bytes:%d", ndp_ip_datagram_info->len);

      /* Dup dsm item and start processing */
      if ( qmi_svc_dsm_dup_packet( rx_pkt, ntb_dsm_item,
                           ndp_ip_datagram_info->index,
                           ndp_ip_datagram_info->len,
                           FILE_ID_PS_UL_OPTIMIZED_HDLR,__LINE__) != ndp_ip_datagram_info->len)
        
      {
        dsm_free_packet( rx_pkt );
      }
      else
      {

        /*-------------------------------------------------------------------
                   IP processing
        -------------------------------------------------------------------*/
        rval = ps_ul_optimized_process_mbim_ip_no_qos( (void*)info,*rx_pkt);
        if ( rval == TRUE )
        {

          LOG_MSG_ERROR_1 ("Flow Disabled. Storing Outstanding mbim pkt"
                           "info in rmnet_dual_ip instance,ndp_hdr_len %d",
                           ndp_hdr->len);

          /*-----------------------------------------------------------------
              Store outstanding mbim pkt info in rmnet_dual_ip instance.
              If Short NDP buff used its already stored in rmnet dual ip
              instance.
          -----------------------------------------------------------------*/
          call_info->pkt_info->ndp_datagram_offset = ndp_datagram_offset;
          call_info->pkt_info->ntb_dsm_item        = ntb_dsm_item;
          call_info->pkt_info->ndp_offset          = ndp_offset;
          call_info->pkt_info->nth_hdr             = nth_hdr;
          call_info->pkt_info->ndp_hdr             = ndp_hdr;
          call_info->dual_ip_info                  = info;
          call_info->rx_pkt                        = *rx_pkt;


          if (PS_OPT_PATH_MAX_ST_NDP_LEN < ndp_hdr->len)
          {
            call_info->pkt_info->ndp_long_buf = ndp_buf;
          }
          return TRUE;
        }

      }

    } /*Else: Valid IP packet processing*/

    ndp_datagram_offset += PS_OPT_PATH_NDP_DG_OFFSET;
    ndp_ip_datagram_info ++;

  } /* while() for datagram processing in a NDP */

  return FALSE;
}/*ps_ul_optimized_hdlr_mbim_ndp_process()*/

/*===========================================================================
FUNCTION PS_OPT_PATH_MBIM_IP_RX_SIG_HDLR

DESCRIPTION
  This fn is the IP NO QOS Signal hdlr in MBIM mode.

PARAMETERS
  rx_sig   : Rmnet Signal
  user_data : mbim_call_info

RETURN VALUE
  TRUE  : Stop packet processing
  FALSE : pkt processing complete.

DEPENDENCIES
  None

SIDE EFFECTS
  None
===========================================================================*/
boolean ps_ul_optimized_mbim_ip_rx_sig_hdlr
(
  rmnet_sig_enum_type   rx_sig,
  void                * user_data
)
{

  rmnet_smi_dual_ip_info_type  * info;
  rmnet_mbim_port_info_type    * port_info = NULL;
  rmnet_mbim_call_info_type    * call_info = NULL;
  ps_iface_mbim_ntb_ndp_hdr    * ndp_hdr;
  ps_iface_mbim_ntb_nth_hdr      nth_hdr;

  dsm_item_type  * ntb_dsm_item = NULL;   /*MBIM pkt*/
  dsm_item_type  * rx_pkt       = NULL;  /*IP pkt*/

  uint16 ndp_datagram_offset = 0;
  uint32 ndp_signature;
  uint8 port_arr_index;
  uint16 ndp_length          = 0;
  uint16 ndp_offset          = 0;
  uint8 *ndp_buf             = NULL; /*Full NDP buffer*/
  uint8  ips_id              = 0;
  uint8  rval                = FALSE;
  qmi_instance_e_type qmi_inst = QMI_INSTANCE_MAX;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_INFO3_0("In ps_ul_optimized_mbim_ip_rx_sig_hdlr");

  ASSERT(user_data);
  call_info = (rmnet_mbim_call_info_type  *) user_data;

#if 0
  info = (rmnet_smi_dual_ip_info_type  *) user_data;

  PS_ENTER_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
  ndp_signature = info->info_v4->meta_sm.data_agg_protocol.ndp_signature;
  PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
#endif

  /*-------------------------------------------------------------------------
     If rx_pkt (IP_pkt ) is outstanding from previous time, use it. Otherwise,
     dequeue a new MBIM packet.

     ntb_dsm_item -> Complete MBIM packet
     rx_pkt -> IP datagram at the specific offset after we do a dsm_dup


  -------------------------------------------------------------------------*/

  if(NULL == call_info->pkt_info)
  {
    LOG_MSG_ERROR_0("mbim pkt info not inited.");
    return FALSE;
  }

  if((NULL != call_info->pkt_info) &&
     (NULL == call_info->pkt_info->ntb_dsm_item)
    )                                            /*Not an Outstanding packet*/
  {

    ntb_dsm_item = dsm_dequeue(call_info->mbim_rx_wmk);

    if (NULL == ntb_dsm_item)
    {
      return TRUE;
    }

    /*-------------------------------------------------------------------------
        Process NCM transfer Block (NTB)
    -------------------------------------------------------------------------*/

    /*-------------------------------------------------------------------------
       Retrieve complete NTH common header at once (12 bytes)
       1.Signature
       2.HeaderLength
       3.Sequence
       4.wBlockLength
       5.NDP Index
    -------------------------------------------------------------------------*/

    if ( (PS_OPT_PATH_NTHC_LEN + PS_OPT_PATH_NTHS_LEN) !=
                             dsm_extract( ntb_dsm_item,
                                          0,
                                          &nth_hdr,
                                          PS_OPT_PATH_NTHC_LEN +
                                          PS_OPT_PATH_NTHS_LEN
                                        )
       )
    {
      LOG_MSG_ERROR_0("Unable to retrieve NTH heder");
      dsm_free_packet(&ntb_dsm_item);
      return FALSE;
    }

    //nth_c   = (ntb_nth_common_t *)(nth_buf);
    //nth16_s = (ntb_nth16_s_t *)(nth_buf + PS_OPT_PATH_NTHC_LEN);

    /*Error checking for all MBIM NTH header fields*/

    if((nth_hdr.signature != RMNET_MBIM_HDR_NTH16_SIG)  ||
       (nth_hdr.hdr_len != PS_IFACE_MBIM_NTH16_HEAD_LEN) ||
       //((nth_hdr.mbim_block_len & 0x0003) != 0)  ||
       (nth_hdr.first_ndp_index < PS_IFACE_MBIM_NTH16_HEAD_LEN )
      )
    {
      LOG_MSG_ERROR_2("Unrecognized MBIM NTH Header"
                      ": signature 0x%8X or length %d",
                      nth_hdr.signature, nth_hdr.hdr_len);
      dsm_free_packet(&ntb_dsm_item);
      return FALSE;
    }

    /*-------------------------------------------------------------------------
         Start processing NDPs one by one from NDP[0]
    -------------------------------------------------------------------------*/
    ndp_offset = nth_hdr.first_ndp_index;

    LOG_MSG_INFO3_1("First NDP Index at: %d", ndp_offset );

    while ( (ndp_offset != 0) && (ndp_offset < nth_hdr.mbim_block_len) )
    {

      /*----------------------------------------------------------------------
        Extract the NDP header length. Do a dsm_extract of length bytes to get
        complete NDP buf.
      ----------------------------------------------------------------------*/
      if ( PS_OPT_PATH_NDP_HDR_FIELD_LEN !=
              dsm_extract (
                            ntb_dsm_item,
                            (uint16)ndp_offset +
                            PS_OPT_PATH_NDP_HDR_FIELD_OFFSET,
                            &ndp_length,
                            PS_OPT_PATH_NDP_HDR_FIELD_LEN
                          )
          )
      {
        LOG_MSG_ERROR_0("Unable to retrieve NDP Header length field");
        dsm_free_packet(&ntb_dsm_item);
        return FALSE;
      }

      LOG_MSG_INFO3_1("NDP length from NDP header len field : %d",
                      ndp_length );

      /*----------------------------------------------------------------------
        1. NDP_Length <= 48 (Max 10 IP Pkts in NDP). Use Static Buffer.
        2. NDP_Length > 48 (Malloc accordingly)
      ----------------------------------------------------------------------*/
      if ( ndp_length <= PS_OPT_PATH_MAX_ST_NDP_LEN)
      {
        LOG_MSG_INFO3_0 ("ndp_length<=48" );

        if ( ndp_length != dsm_extract( ntb_dsm_item,
                                        (uint16)ndp_offset,
                                        call_info->pkt_info->ndp_short_buf,
                                        ndp_length)
           )
        {
          LOG_MSG_ERROR_0("Unable to retrieve full NDP buffer");
          dsm_free_packet(&ntb_dsm_item);
          return FALSE;

        }
        ndp_buf = call_info->pkt_info->ndp_short_buf;
      }
      else
      {

        LOG_MSG_INFO3_0 ("ndp_length>48" );

        /*malloc NDP length bytes*/
        ndp_buf = (uint8 *)(qmi_svc_ps_system_heap_mem_alloc(ndp_length,
                        FILE_ID_PS_UL_OPTIMIZED_HDLR,__LINE__));

        if(ndp_buf == NULL)
        {
          dsm_free_packet(&ntb_dsm_item);
          return FALSE;
        }

        if ( ndp_length != dsm_extract( ntb_dsm_item,
                                        (uint16)ndp_offset,
                                        ndp_buf,
                                        ndp_length)
           )
        {
          LOG_MSG_ERROR_0("Unable to retrieve full NDP buffer");
          PS_SYSTEM_HEAP_MEM_FREE (ndp_buf);
          dsm_free_packet(&ntb_dsm_item);
          return FALSE;

        }
      }

      /*NDP Common header*/
      ndp_hdr = (ps_iface_mbim_ntb_ndp_hdr *)ndp_buf;

      ndp_signature = ndp_hdr->signature;

#if 0
      /* Error checking for NDP command Header */
      if ( (ndp_hdr->signature != ndp_signature)       ||
            ndp_hdr->len > nth_hdr.mbim_block_len                      ||
            ndp_hdr->len < PS_OPT_PATH_NDP_LEN_CHECK          ||
            (ndp_hdr->len & 0x0003) != 0
         )

      {
        LOG_MSG_ERROR_2("Unrecognized NDP signature 0x%08X or length %d",
                        ndp_hdr->signature, ndp_hdr->len);
        if (PS_OPT_PATH_MAX_ST_NDP_LEN < ndp_length)
        {
          PS_SYSTEM_HEAP_MEM_FREE (ndp_buf);
        }
        dsm_free_packet(&ntb_dsm_item);
        return FALSE;
      }
#endif

      /*-----------------------------------------------------------------------
         Retrieve ips_id from ndp sig and associated rmnet info.
      -----------------------------------------------------------------------*/

      ips_id    = (uint8)((ps_ntohl(ndp_signature)) &
                           PS_OPT_PATH_MBIM_IPS_MASK);

      LOG_MSG_INFO3_2("ndp sig : 0x%08X , ips_id : %d", ndp_signature, ips_id);

#if 0
      port_arr_index = call_info->ips_id_index_map[ips_id];
      port_info      = &call_info->mbim_port_info[port_arr_index];
      info           = &rmnet_smi_dual_ip_info[port_info->qmi_port_inst];
#endif

      if(FALSE ==
          ds_rmnet_meta_smi_get_mbim_port_index
            (NULL, ips_id , &port_arr_index)
        )
      {
        LOG_MSG_ERROR_1("Could not find matching port info for ips_id %d",
                        ips_id );
        if (PS_OPT_PATH_MAX_ST_NDP_LEN < ndp_length)
        {
          PS_SYSTEM_HEAP_MEM_FREE (ndp_buf);
        }
        dsm_free_packet(&ntb_dsm_item);
        return FALSE;
      }

      port_info      = &call_info->mbim_port_info[port_arr_index];
      /* This check is required to avoid race conditions 
         when a call in teared down in dcc task, where 
         the qmi_port_inst is set to QMI_INSTANCE_MAX 
         for a given port_arr_index */
      qmi_inst = port_info->qmi_port_inst;
      if (qmi_inst >= RMNET_INSTANCE_MAX/2)
      {
        LOG_MSG_ERROR_1("Invalid Qmi instance got %d",
                      qmi_inst);
        if (PS_OPT_PATH_MAX_ST_NDP_LEN < ndp_length)
        {
          PS_SYSTEM_HEAP_MEM_FREE (ndp_buf);
        }
        dsm_free_packet(&ntb_dsm_item);
        return FALSE;
      }
      info           = &rmnet_smi_dual_ip_info[qmi_inst];

      LOG_MSG_INFO3_3("Matching mbim port info:port_arr_index %d,"
                      "qmi_port %d , rmnet_inst %d",
                      port_arr_index,
                     qmi_inst,
                      RMNET_META_SM_INFO_TO_INSTANCE(port_info->info));

      /*-----------------------------------------------------------------------
         Start processing IP datagrams offsets and length in the NDP.
         We have all IP offsets and length in ndp_buf.
      -----------------------------------------------------------------------*/

      /*Start with first IP index/len*/

      ndp_datagram_offset  = PS_OPT_PATH_NDP_HDR_LEN;

      if (TRUE == ps_ul_optimized_hdlr_mbim_ndp_process( user_data,
                                                         ndp_datagram_offset,
                                                         ntb_dsm_item,
                                                         &rx_pkt,
                                                         ndp_buf,
                                                         nth_hdr,
                                                         info,
                                                         ndp_offset ))
      {
        /*Flow disabled*/

        //PS_LEAVE_CRIT_SECTION(&global_ps_crit_section);
        return TRUE;
      }


      /*---------------------------------------------------------------------
         Free NDP buff if allocated. Here ndp_buf could be pointing to our
        static short_ndp_buf. Need to check this to free. Cant use Null check
         here since it is never NULL. So check for length and then free.

        But first get the next NDP offset and then free the curr ndp buff.
      ---------------------------------------------------------------------*/
      ndp_offset = ndp_hdr->next_ndp_index; /* Next ndp offset */

      if (ndp_hdr->len > PS_OPT_PATH_MAX_ST_NDP_LEN)
      {
         PS_SYSTEM_HEAP_MEM_FREE (ndp_buf);
      }

      LOG_MSG_INFO3_1 ("Next NDP offset:%d", ndp_offset);
    } /*while() for NDP processing*/

  }

  else  /*Outstanding packet*/
  {
    /*---------------------------------------------------------------------
      Get all outstanding mbim pkt info from mbim_call_info first.

      1. Process Outstanding pkt
      2. Process remaining pkts in outstanding NDP
      3. Process remaining NDPs.
    ---------------------------------------------------------------------*/
    ndp_datagram_offset = call_info->pkt_info->ndp_datagram_offset;
    ntb_dsm_item        = call_info->pkt_info->ntb_dsm_item;
    ndp_offset          = call_info->pkt_info->ndp_offset;
    nth_hdr             = call_info->pkt_info->nth_hdr;
    ndp_hdr             = call_info->pkt_info->ndp_hdr;
    rx_pkt              = call_info->rx_pkt;
    info                = call_info->dual_ip_info;

    if (PS_OPT_PATH_MAX_ST_NDP_LEN < ndp_hdr->len)
    {
      ndp_buf = call_info->pkt_info->ndp_long_buf;
    }
    else
    {
      ndp_buf = call_info->pkt_info->ndp_short_buf;
    }

    LOG_MSG_ERROR_3 ( "Outstanding MBIM packet info : ndp_offset : %d ,"
                      " ndp_dg_offset: %d, ndp_hdr_len : %d",
                      ndp_offset, ndp_datagram_offset, ndp_hdr->len);

    /*---------------------------------------------------------------------
      Reset all outstanding mbim pkt info in rmnet_dual_ip instance. NDP
      short buf will be released at call tear down.
    ---------------------------------------------------------------------*/
    call_info->pkt_info->ndp_datagram_offset = 0;
    call_info->pkt_info->ntb_dsm_item        = NULL;
    call_info->pkt_info->ndp_offset          = 0;
    memset(&call_info->pkt_info->nth_hdr,0,
      sizeof(ps_iface_mbim_ntb_nth_hdr));
    call_info->pkt_info->ndp_hdr             = NULL;
    call_info->dual_ip_info                  = NULL;
    call_info->rx_pkt                        = NULL;

    if (PS_OPT_PATH_MAX_ST_NDP_LEN < ndp_hdr->len)
    {
      call_info->pkt_info->ndp_long_buf = NULL;
    }

    /*---------------------------------------------------------------------
      1. Process Current Outstanding IP pkt.
    ---------------------------------------------------------------------*/

    rval = ps_ul_optimized_process_mbim_ip_no_qos( (void*)info, rx_pkt);
    if ( rval == TRUE )
    {
      LOG_MSG_ERROR_0 ("Flow Disabled. Storing Outstanding mbim pkt"
                       "info in rmnet_dual_ip instance");
      /*---------------------------------------------------------------------
        Store outstanding mbim pkt info in rmnet_dual_ip instance.
        If Short NDP buff used its already a part of rmnet dual ip
        instance.
      ---------------------------------------------------------------------*/
      call_info->pkt_info->ndp_datagram_offset = ndp_datagram_offset;
      call_info->pkt_info->ntb_dsm_item        = ntb_dsm_item;
      call_info->pkt_info->ndp_offset          = ndp_offset;
      call_info->pkt_info->nth_hdr             = nth_hdr;
      call_info->pkt_info->ndp_hdr             = ndp_hdr;
      call_info->dual_ip_info                  = info;
      call_info->rx_pkt                        = rx_pkt;

      if (PS_OPT_PATH_MAX_ST_NDP_LEN < ndp_hdr->len)
      {
        call_info->pkt_info->ndp_long_buf = ndp_buf;
      }

      return TRUE;
    }

    /*---------------------------------------------------------------------
      2. Process current Outstanding NDP. Need to free this buff if it was
      dynamically allocated earlier.
    ---------------------------------------------------------------------*/
    if ( (ndp_offset != 0) && (ndp_offset < nth_hdr.mbim_block_len) )
    {

      /*---------------------------------------------------------------------
       Current datagram offset has been above processed. So start with
       - current saved NDP(ndp_offset) and
       - next datagram offset in that NDP.
      ---------------------------------------------------------------------*/

      LOG_MSG_INFO3_2 ("Current dg offset: %d, next dg offset:%d",
                       ndp_datagram_offset,
                       ndp_datagram_offset + PS_OPT_PATH_NDP_DG_OFFSET);

      ndp_datagram_offset += PS_OPT_PATH_NDP_DG_OFFSET;

      if (TRUE == ps_ul_optimized_hdlr_mbim_ndp_process( user_data,
                                                         ndp_datagram_offset,
                                                         ntb_dsm_item,
                                                         &rx_pkt,
                                                         ndp_buf,
                                                         nth_hdr,
                                                         info,
                                                         ndp_offset ))
      {
        /*Flow disabled*/

        //PS_LEAVE_CRIT_SECTION(&global_ps_crit_section);
        return TRUE;
      }

      /*---------------------------------------------------------------------
        Free Outstanding NDP buff if dynamically allocated.
        But first get the next NDP offset and then free the curr NDP buff.
      ---------------------------------------------------------------------*/
      ndp_offset = ndp_hdr->next_ndp_index; /* Next ndp offset */

      if (PS_OPT_PATH_MAX_ST_NDP_LEN < ndp_hdr->len)
      {
         PS_SYSTEM_HEAP_MEM_FREE (ndp_buf);
      }

    }/*if: Outstanding NDP processing*/

    /*-----------------------------------------------------------------------
      3.Proceed with other NDPs in Outstanding MBIM
    -----------------------------------------------------------------------*/
    LOG_MSG_INFO3_1 ("Next NDP offset:%d", ndp_offset);

    while ( (ndp_offset != 0) && (ndp_offset < nth_hdr.mbim_block_len) )
    {

      /*---------------------------------------------------------------------
        Extract the NDP header length. Do a dsm_extract of length bytes to get
        complete NDP buf.
      ---------------------------------------------------------------------*/
      if ( PS_OPT_PATH_NDP_HDR_FIELD_LEN !=
            dsm_extract (
                          ntb_dsm_item,
                          (uint16)ndp_offset +
                          PS_OPT_PATH_NDP_HDR_FIELD_OFFSET,
                          &ndp_length,
                          PS_OPT_PATH_NDP_HDR_FIELD_LEN
                        )
         )
      {
        LOG_MSG_ERROR_0("Unable to retrieve NDP Header length field");
        dsm_free_packet(&ntb_dsm_item);
        return FALSE;
      }

      LOG_MSG_INFO3_1("NDP length from NDP header len field : %d",
                      ndp_length );

      /*---------------------------------------------------------------------
         1. NDP_Length <= 48 (Max 10 IP Pkts in NDP). Use Static Buffer.
         2. NDP_Length > 48 (Malloc accordingly)
      ---------------------------------------------------------------------*/
      if ( ndp_length <= PS_OPT_PATH_MAX_ST_NDP_LEN)
      {
        LOG_MSG_INFO3_0 ("ndp_length<=48" );

        if ( ndp_length != dsm_extract( ntb_dsm_item,
                                        (uint16)ndp_offset,
                                        info->mbim_pkt_info->ndp_short_buf,
                                        ndp_length)
           )
        {
          LOG_MSG_ERROR_0("Unable to retrieve full NDP buffer");
          dsm_free_packet(&ntb_dsm_item);
          return FALSE;

        }
        ndp_buf = info->mbim_pkt_info->ndp_short_buf;
     }
     else
     {

       LOG_MSG_INFO3_0 ("ndp_length>48" );

       /*malloc NDP length bytes*/
       ndp_buf = (uint8 *)(qmi_svc_ps_system_heap_mem_alloc(ndp_length,
                        FILE_ID_PS_UL_OPTIMIZED_HDLR,__LINE__));

       if(ndp_buf == NULL)
       {
         dsm_free_packet(&ntb_dsm_item);
         return FALSE;
       }

       if ( ndp_length != dsm_extract( ntb_dsm_item,
                                       (uint16)ndp_offset,
                                       ndp_buf,
                                       ndp_length)
          )
       {
         LOG_MSG_ERROR_0("Unable to retrieve full NDP buffer");
         PS_SYSTEM_HEAP_MEM_FREE (ndp_buf);
         dsm_free_packet(&ntb_dsm_item);
         return FALSE;
       }
     }

     /*NDP Common header*/
     ndp_hdr = (ps_iface_mbim_ntb_ndp_hdr *)ndp_buf;

     ndp_signature = ndp_hdr->signature;

     LOG_MSG_INFO3_1("NDP length: %d", ndp_hdr->len);
#if 0
     /* Error checking for NDP command Header */
     if ( (ndp_hdr->signature != ndp_signature)    ||
          ndp_hdr->len > nth_hdr.mbim_block_len                      ||
          ndp_hdr->len < PS_OPT_PATH_NDP_LEN_CHECK          ||
          (ndp_hdr->len & 0x0003) != 0
       )

     {
       LOG_MSG_ERROR_2("Unrecognized NDP signature 0x%08X or length %d",
                       ndp_hdr->signature, ndp_hdr->len);
       if (PS_OPT_PATH_MAX_ST_NDP_LEN < ndp_length)
       {
         PS_SYSTEM_HEAP_MEM_FREE (ndp_buf);
       }
       dsm_free_packet(&ntb_dsm_item);
       return FALSE;
     }
#endif

     /*-----------------------------------------------------------------------
         Retrieve ips_id from ndp sig and associated rmnet info.
      -----------------------------------------------------------------------*/

     ips_id    = (uint8)((ps_ntohl(ndp_signature)) &
                           PS_OPT_PATH_MBIM_IPS_MASK);

      LOG_MSG_INFO3_2("ndp sig : 0x%08X , ips_id : %d", ndp_signature, ips_id);

      if(FALSE ==
          ds_rmnet_meta_smi_get_mbim_port_index
            (NULL, ips_id , &port_arr_index)
        )
      {
        LOG_MSG_ERROR_1("Could not find matching port info for ips_id %d",
                        ips_id );

        if (PS_OPT_PATH_MAX_ST_NDP_LEN < ndp_length)
        {
          PS_SYSTEM_HEAP_MEM_FREE (ndp_buf);
        }

        dsm_free_packet(&ntb_dsm_item);
        return FALSE;
      }

      port_info      = &call_info->mbim_port_info[port_arr_index];
      /* This check is required to avoid race conditions 
         when a call in teared down in dcc task, where 
         the qmi_port_inst is set to QMI_INSTANCE_MAX 
         for a given port_arr_index */
      qmi_inst = port_info->qmi_port_inst;
      if (qmi_inst >= RMNET_INSTANCE_MAX/2)
      {
        LOG_MSG_ERROR_1("Invalid Qmi instance got %d",
                      qmi_inst);
        if (PS_OPT_PATH_MAX_ST_NDP_LEN < ndp_length)
        {
          PS_SYSTEM_HEAP_MEM_FREE (ndp_buf);
        }
        dsm_free_packet(&ntb_dsm_item);
        return FALSE;
      }
      info           = &rmnet_smi_dual_ip_info[qmi_inst];

      LOG_MSG_INFO3_3("Matching mbim port info:port_arr_index %d,"
                      "qmi_port %d , rmnet_inst %d",
                      port_arr_index,
                     qmi_inst,
                      RMNET_META_SM_INFO_TO_INSTANCE(port_info->info));

     /*----------------------------------------------------------------------
       Start processing IP datagrams offsets and length in the NDP.
       We have all IP offsets and length in ndp_buf.
     ----------------------------------------------------------------------*/

     /*Start with first IP index/len*/

     ndp_datagram_offset  = PS_OPT_PATH_NDP_HDR_LEN;

     if (TRUE == ps_ul_optimized_hdlr_mbim_ndp_process( user_data,
                                                        ndp_datagram_offset,
                                                        ntb_dsm_item,
                                                        &rx_pkt,
                                                        ndp_buf,
                                                        nth_hdr,
                                                        info,
                                                        ndp_offset ))
     {
        /*Flow disabled*/
        return TRUE;
     }

     /*---------------------------------------------------------------------
       Free NDP buff if allocated. Here ndp_buf could be pointing to our
       short_ndp_buf. Need to check this to free. Cant use Null check
       here since it is never NULL. So check for length and then free.

      But first get the next NDP offset and then free the curr NDP buff.
     ---------------------------------------------------------------------*/
     ndp_offset = ndp_hdr->next_ndp_index; /* Next ndp offset */

     if (ndp_hdr->len > PS_OPT_PATH_MAX_ST_NDP_LEN)
     {
       PS_SYSTEM_HEAP_MEM_FREE (ndp_buf);
     }

     LOG_MSG_INFO3_1 ("Next NDP offset:%d", ndp_offset);

   } /*while() for NDP processing*/

  } /*Else: Outstanding packet*/

  dsm_free_packet(&ntb_dsm_item);
  rx_pkt = NULL;

  LOG_MSG_INFO3_1 ("Returning rval at end : %d", rval  );

  return rval; /*TODO Usually we will be returning FALSE here??*/

} /* ps_ul_optimized_mbim_ip_rx_sig_hdlr() */

#endif /*FEATURE_DATA_MBIM*/

/*===========================================================================
FUNCTION PS_OPT_PATH_QCNCM_IP_RX_SIG_HDLR

DESCRIPTION
  This fn is the IP NO QOS Signal hdlr in QCNCM mode.

PARAMETERS
  rx_sig   : Rmnet Signal
  user_data : rmnet_dual_ip_info_type

RETURN VALUE
  TRUE  : Stop packet processing
  FALSE : pkt processing complete.

DEPENDENCIES
  None

SIDE EFFECTS
  None
===========================================================================*/
boolean ps_ul_optimized_qcncm_ip_rx_sig_hdlr
(
  rmnet_sig_enum_type   rx_sig,
  void                * user_data
)
{

  rmnet_smi_dual_ip_info_type  * info;
  uint8    rval        = FALSE;

  dsm_item_type  * ntb_dsm_item = NULL;   /*MBIM pkt*/
  dsm_item_type  * rx_pkt       = NULL;  /*IP pkt*/
  uint16    ndp_offset          = 0;
  uint16    ndp_datagram_offset = 0;

  ps_iface_mbim_ntb_nth_hdr nth_hdr;
  ps_iface_mbim_ntb_ndp_hdr * ndp_hdr;
  ps_iface_mbim_ntb_ndp_ip_datagram_info *ndp_ip_datagram_info;

  uint16 ndp_length = 0;
  uint32 ndp_signature;
  uint8 *ndp_buf = NULL; /*Full NDP buffer*/

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_INFO3_0("In ps_ul_optimized_mbim_ip_rx_sig_hdlr");

  ASSERT(user_data);

  info = (rmnet_smi_dual_ip_info_type  *) user_data;

  PS_ENTER_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);
  ndp_signature = info->info_v4->meta_sm.data_agg_protocol.ndp_signature;
  PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);

  /*-------------------------------------------------------------------------
     If rx_pkt (IP_pkt ) is outstanding from previous time, use it. Otherwise,
     dequeue a new MBIM packet.

     ntb_dsm_item -> Complete MBIM packet
     rx_pkt -> IP datagram at the specific offset after we do a dsm_dup
  -------------------------------------------------------------------------*/

  if (NULL == info->mbim_pkt_info->ntb_dsm_item) /*Not an Outstanding packet*/
  {

    ntb_dsm_item = dsm_dequeue(& ((rmnet_sio_info_type *)
                           (info->info_v4->meta_sm.rmnet_sio_handle))->rx_wmk );
    if (NULL == ntb_dsm_item)
    {
      return TRUE;
    }

    /*-------------------------------------------------------------------------
        Process NCM transfer Block (NTB)
    -------------------------------------------------------------------------*/

    /*-------------------------------------------------------------------------
       Retrieve complete NTH common header at once (12 bytes)
       1.Signature
       2.HeaderLength
       3.Sequence
       4.wBlockLength
       5.NDP Index
    -------------------------------------------------------------------------*/

    if ( (PS_OPT_PATH_NTHC_LEN + PS_OPT_PATH_NTHS_LEN) !=
                             dsm_extract( ntb_dsm_item,
                                          0,
                                          &nth_hdr,
                                          PS_OPT_PATH_NTHC_LEN +
                                          PS_OPT_PATH_NTHS_LEN
                                        )
       )
    {
      LOG_MSG_ERROR_0("Unable to retrieve NTH heder");
      dsm_free_packet(&ntb_dsm_item);
      return FALSE;
    }

    //nth_c   = (ntb_nth_common_t *)(nth_buf);
    //nth16_s = (ntb_nth16_s_t *)(nth_buf + PS_OPT_PATH_NTHC_LEN);

    /*Error checking for all MBIM/QC_NCM NTH header fields*/

    if((nth_hdr.signature != RMNET_MBIM_HDR_NTH16_SIG)  ||
       (nth_hdr.hdr_len != PS_IFACE_MBIM_NTH16_HEAD_LEN) ||
       //((nth_hdr.mbim_block_len & 0x0003) != 0)  ||
       (nth_hdr.first_ndp_index < PS_IFACE_MBIM_NTH16_HEAD_LEN )
      )
    {
      LOG_MSG_ERROR_2("Unrecognized MBIM NTH Header"
                      ": signature 0x%8X or length %d",
                      nth_hdr.signature, nth_hdr.hdr_len);
      dsm_free_packet(&ntb_dsm_item);
      return FALSE;
    }

    /*-------------------------------------------------------------------------
         Start processing NDPs one by one from NDP[0]
    -------------------------------------------------------------------------*/
    ndp_offset = nth_hdr.first_ndp_index;

    LOG_MSG_INFO3_1("First NDP Index at: %d", ndp_offset );

    while ( (ndp_offset != 0) && (ndp_offset < nth_hdr.mbim_block_len) )
    {

      /*----------------------------------------------------------------------
        Extract the NDP header length. Do a dsm_extract of length bytes to get
        complete NDP buf.
      ----------------------------------------------------------------------*/
      if ( PS_OPT_PATH_NDP_HDR_FIELD_LEN !=
              dsm_extract (
                            ntb_dsm_item,
                            (uint16)ndp_offset +
                            PS_OPT_PATH_NDP_HDR_FIELD_OFFSET,
                            &ndp_length,
                            PS_OPT_PATH_NDP_HDR_FIELD_LEN
                          )
          )
      {
        LOG_MSG_ERROR_0("Unable to retrieve NDP Header length field");
        dsm_free_packet(&ntb_dsm_item);
        return FALSE;
      }

      LOG_MSG_INFO3_1("NDP length from NDP header len field : %d",
                      ndp_length );

      /*----------------------------------------------------------------------
        1. NDP_Length <= 48 (Max 10 IP Pkts in NDP). Use Static Buffer.
        2. NDP_Length > 48 (Malloc accordingly)
      ----------------------------------------------------------------------*/
      if ( ndp_length <= PS_OPT_PATH_MAX_ST_NDP_LEN)
      {
        LOG_MSG_INFO3_0 ("ndp_length<=48" );

        if ( ndp_length != dsm_extract( ntb_dsm_item,
                                        (uint16)ndp_offset,
                                        info->mbim_pkt_info->ndp_short_buf,
                                        ndp_length)
           )
        {
          LOG_MSG_ERROR_0("Unable to retrieve full NDP buffer");
          dsm_free_packet(&ntb_dsm_item);
          return FALSE;

        }
        ndp_buf = info->mbim_pkt_info->ndp_short_buf;
      }
      else
      {

        LOG_MSG_INFO3_0 ("ndp_length>48" );

        /*malloc NDP length bytes*/
        ndp_buf = (uint8 *)(qmi_svc_ps_system_heap_mem_alloc(ndp_length,
                             FILE_ID_PS_UL_OPTIMIZED_HDLR,__LINE__));

        if(ndp_buf == NULL)
        {
          dsm_free_packet(&ntb_dsm_item);
          return FALSE;
        }

        if ( ndp_length != dsm_extract( ntb_dsm_item,
                                        (uint16)ndp_offset,
                                        ndp_buf,
                                        ndp_length)
           )
        {
          LOG_MSG_ERROR_0("Unable to retrieve full NDP buffer");
          PS_SYSTEM_HEAP_MEM_FREE (ndp_buf);
          dsm_free_packet(&ntb_dsm_item);
          return FALSE;

        }
      }

      /*NDP Common header*/
      ndp_hdr = (ps_iface_mbim_ntb_ndp_hdr *)ndp_buf;

      /* Error checking for NDP command Header */
      if ( (ndp_hdr->signature != ndp_signature)       ||
            ndp_hdr->len > nth_hdr.mbim_block_len                      ||
            ndp_hdr->len < PS_OPT_PATH_NDP_LEN_CHECK          ||
            (ndp_hdr->len & 0x0003) != 0
         )

      {
        LOG_MSG_ERROR_2("Unrecognized NDP signature 0x%08X or length %d",
                        ndp_hdr->signature, ndp_hdr->len);
        if (PS_OPT_PATH_MAX_ST_NDP_LEN < ndp_length)
        {
          PS_SYSTEM_HEAP_MEM_FREE (ndp_buf);
        }
        dsm_free_packet(&ntb_dsm_item);
        return FALSE;
      }

      /*-----------------------------------------------------------------------
         Start processing IP datagrams offsets and length in the NDP.
         We have all IP offsets and length in ndp_buf.
      -----------------------------------------------------------------------*/

      /*Start with first IP index/len*/

      ndp_datagram_offset  = PS_OPT_PATH_NDP_HDR_LEN;
      ndp_ip_datagram_info = (ps_iface_mbim_ntb_ndp_ip_datagram_info *)
                                (ndp_buf + PS_OPT_PATH_NDP_HDR_LEN) ;

      while ( ndp_datagram_offset < ndp_hdr->len )
      {
        LOG_MSG_INFO3_2("Start to process ndp 0x%4X at offset %d",
                        ndp_offset, ndp_datagram_offset);

        /*d/g offset, length structure*/
        //ndp16_s = (ntb_ndp16_s_t * )(ndp_buf + ndp_datagram_offset);

        /*---------------------------------------------------------------------
           Validate datagram Index and length and perform IP processing
           dgindex == 0 -> Terminate
        ---------------------------------------------------------------------*/
        if ( ndp_ip_datagram_info->index == 0)
        {

          LOG_MSG_INFO3_1("ndp datagram %d terminated with zero",
                          ndp_datagram_offset);

          break; /*break and continue with next NDP*/
        }

        else  /*Valid IP packet to process*/
        {
          LOG_MSG_INFO3_2(
"Process d/g nth_datagram_offset: %d,ndp_datagram_offset:%d",
ndp_ip_datagram_info->index, ndp_datagram_offset);

          LOG_MSG_INFO3_1 ("dsm_dup of bytes:%d", ndp_ip_datagram_info->len);

          /* Dup dsm item and start processing */
          if ( qmi_svc_dsm_dup_packet( &rx_pkt, ntb_dsm_item,
                               ndp_ip_datagram_info->index,
                               ndp_ip_datagram_info->len,
                               FILE_ID_PS_UL_OPTIMIZED_HDLR,__LINE__) != ndp_ip_datagram_info->len)
          {
            dsm_free_packet( &rx_pkt );
          }
          else
          {

            /*---------------------------------------------------------------
                   IP processing
            ---------------------------------------------------------------*/
            rval = ps_ul_optimized_process_qcncm_ip_no_qos( user_data,
                                                                rx_pkt);
            if ( rval == TRUE )
            {

              LOG_MSG_ERROR_1 ("Flow Disabled. Storing Outstanding mbim pkt"
                               "info in rmnet_dual_ip instance,ndp_hdr_len %d",
                               ndp_hdr->len);
              /*-------------------------------------------------------------
                Store outstanding mbim pkt info in rmnet_dual_ip instance.
                If Short NDP buff used its already stored in rmnet dual ip
                instance.
              -------------------------------------------------------------*/
              info->mbim_pkt_info->ndp_datagram_offset = ndp_datagram_offset;
              info->mbim_pkt_info->ntb_dsm_item        = ntb_dsm_item;
              info->mbim_pkt_info->ndp_offset          = ndp_offset;
              info->mbim_pkt_info->nth_hdr             = nth_hdr;
              info->mbim_pkt_info->ndp_hdr             = ndp_hdr;
              info->rx_pkt                             = rx_pkt;

              if (PS_OPT_PATH_MAX_ST_NDP_LEN < ndp_hdr->len)
              {
                info->mbim_pkt_info->ndp_long_buf = ndp_buf;
              }
              return TRUE;
            }

          }

        } /*Else: Valid IP packet processing*/

        ndp_datagram_offset += PS_OPT_PATH_NDP_DG_OFFSET;
        ndp_ip_datagram_info ++;

      } /* while() for datagram processing in a NDP */

      /*---------------------------------------------------------------------
         Free NDP buff if allocated. Here ndp_buf could be pointing to our
        static short_ndp_buf. Need to check this to free. Cant use Null check
         here since it is never NULL. So check for length and then free.

        But first get the next NDP offset and then free the curr ndp buff.
      ---------------------------------------------------------------------*/
      ndp_offset = ndp_hdr->next_ndp_index; /* Next ndp offset */

      if (ndp_hdr->len > PS_OPT_PATH_MAX_ST_NDP_LEN)
      {
         PS_SYSTEM_HEAP_MEM_FREE (ndp_buf);
      }

      LOG_MSG_INFO3_1 ("Next NDP offset:%d", ndp_offset);
    } /*while() for NDP processing*/

  }

  else  /*Outstanding packet*/
  {
    /*---------------------------------------------------------------------
      Get all outstanding mbim pkt info from rmnet_dual_ip instance.
    ---------------------------------------------------------------------*/
    ndp_datagram_offset = info->mbim_pkt_info->ndp_datagram_offset;
    ntb_dsm_item        = info->mbim_pkt_info->ntb_dsm_item;
    ndp_offset          = info->mbim_pkt_info->ndp_offset;
    nth_hdr             = info->mbim_pkt_info->nth_hdr;
    ndp_hdr             = info->mbim_pkt_info->ndp_hdr;
    rx_pkt              = info->rx_pkt;

    if (PS_OPT_PATH_MAX_ST_NDP_LEN < ndp_hdr->len)
    {
      ndp_buf = info->mbim_pkt_info->ndp_long_buf;
    }
    else
    {
      ndp_buf = info->mbim_pkt_info->ndp_short_buf;
    }

    LOG_MSG_ERROR_3 ( "Outstanding MBIM packet info : ndp_offset : %d ,"
                      " ndp_dg_offset: %d, ndp_hdr_len : %d",
                      ndp_offset, ndp_datagram_offset, ndp_hdr->len);

    /*---------------------------------------------------------------------
      Reset all outstanding mbim pkt info in rmnet_dual_ip instance. NDP
      short buf will be released at call tear down.
    ---------------------------------------------------------------------*/
    info->mbim_pkt_info->ndp_datagram_offset = 0;
    info->mbim_pkt_info->ntb_dsm_item        = NULL;
    info->mbim_pkt_info->ndp_offset          = 0;
    memset(&info->mbim_pkt_info->nth_hdr,0,sizeof(ps_iface_mbim_ntb_nth_hdr));
    info->mbim_pkt_info->ndp_hdr             = NULL;
    info->rx_pkt                             = NULL;
    if (PS_OPT_PATH_MAX_ST_NDP_LEN < ndp_hdr->len)
    {
      info->mbim_pkt_info->ndp_long_buf = NULL;
    }


    /*---------------------------------------------------------------------
      Process Current Outstanding IP pkt.
    ---------------------------------------------------------------------*/
    if (rx_pkt != NULL)
    {
      rval = ps_ul_optimized_process_qcncm_ip_no_qos( user_data,
                                                      rx_pkt);
    }
    else
    {
      LOG_MSG_ERROR_0("rx_pkt is NULL. Discard outstanding packet");
      dsm_free_packet(&ntb_dsm_item);
      return FALSE;
    }
    if ( rval == TRUE )
    {
      LOG_MSG_ERROR_0 ("Flow Disabled. Storing Outstanding mbim pkt"
                       "info in rmnet_dual_ip instance");
      /*---------------------------------------------------------------------
        Store outstanding mbim pkt info in rmnet_dual_ip instance.
        If Short NDP buff used its already a part of rmnet dual ip
        instance.
      ---------------------------------------------------------------------*/
      info->mbim_pkt_info->ndp_datagram_offset = ndp_datagram_offset;
      info->mbim_pkt_info->ntb_dsm_item        = ntb_dsm_item;
      info->mbim_pkt_info->ndp_offset          = ndp_offset;
      info->mbim_pkt_info->nth_hdr             = nth_hdr;
      info->mbim_pkt_info->ndp_hdr             = ndp_hdr;
      info->rx_pkt                             = rx_pkt;

      if (PS_OPT_PATH_MAX_ST_NDP_LEN < ndp_hdr->len)
      {
        info->mbim_pkt_info->ndp_long_buf = ndp_buf;
      }

      return TRUE;
    }

    /*---------------------------------------------------------------------
      Process current Outstanding NDP. Need to free this buff if it was
      dynamically allocated earlier.
    ---------------------------------------------------------------------*/
    if ( (ndp_offset != 0) && (ndp_offset < nth_hdr.mbim_block_len) )
    {

      /*---------------------------------------------------------------------
       Current datagram offset has been above processed. So start with
       - current saved NDP(ndp_offset) and
       - next datagram offset in that NDP.
      ---------------------------------------------------------------------*/

      LOG_MSG_INFO3_2 ("Current dg offset: %d, next dg offset:%d",
                       ndp_datagram_offset,
                       ndp_datagram_offset + PS_OPT_PATH_NDP_DG_OFFSET);

      ndp_datagram_offset += PS_OPT_PATH_NDP_DG_OFFSET;
      ndp_ip_datagram_info = (ps_iface_mbim_ntb_ndp_ip_datagram_info *)
                              (ndp_buf + ndp_datagram_offset) ;

      while ( ndp_datagram_offset < ndp_hdr->len )
      {
        LOG_MSG_INFO3_2("Start to process ndp 0x%4X at offset %d",
                        ndp_offset, ndp_datagram_offset);

        if ( ndp_ip_datagram_info->index == 0)
        {

          LOG_MSG_INFO3_1("ndp datagram %d terminated with zero",
                          ndp_datagram_offset);

          break; /*break and continue with next NDP*/
        }

        else  /*Valid IP packet to process*/
        {
          LOG_MSG_INFO3_2(
"Process d/g nth_datagram_offset: %d,ndp_datagram_offset:%d",
ndp_ip_datagram_info->index, ndp_datagram_offset);

          LOG_MSG_INFO3_1 ("dsm_dup of bytes:%d", ndp_ip_datagram_info->len);

          /* Dup dsm item and start processing */
          if ( qmi_svc_dsm_dup_packet( &rx_pkt, ntb_dsm_item,
                               ndp_ip_datagram_info->index,
                               ndp_ip_datagram_info->len,
                               FILE_ID_PS_UL_OPTIMIZED_HDLR,__LINE__) != ndp_ip_datagram_info->len)
          {
            dsm_free_packet( &rx_pkt );
          }
          else
          {

            /*---------------------------------------------------------------
                 IP processing
            ---------------------------------------------------------------*/
            rval = ps_ul_optimized_process_qcncm_ip_no_qos( user_data,
                                                                rx_pkt);
            if ( rval == TRUE )
            {

              LOG_MSG_ERROR_0("Flow Disabled. Storing Outstanding mbim pkt"
                              "info in rmnet_dual_ip instance");
              /*-------------------------------------------------------------
                Store outstanding mbim pkt info in rmnet_dual_ip instance.
                If Short NDP buff used its already a part of rmnet dual ip
                instance.
              -------------------------------------------------------------*/
              info->mbim_pkt_info->ndp_datagram_offset = ndp_datagram_offset;
              info->mbim_pkt_info->ntb_dsm_item        = ntb_dsm_item;
              info->mbim_pkt_info->ndp_offset          = ndp_offset;
              info->mbim_pkt_info->nth_hdr             = nth_hdr;
              info->mbim_pkt_info->ndp_hdr             = ndp_hdr;
              info->rx_pkt                             = rx_pkt;

              if (PS_OPT_PATH_MAX_ST_NDP_LEN < ndp_hdr->len)
              {
                info->mbim_pkt_info->ndp_long_buf = ndp_buf;
              }
              return TRUE;
            }

          }

        } /*Else: Valid IP packet processing*/

        ndp_datagram_offset += PS_OPT_PATH_NDP_DG_OFFSET;
        ndp_ip_datagram_info ++;

      } /*while: Datagram processing in Outstanding NDP*/

      /*---------------------------------------------------------------------
        Free Outstanding NDP buff if dynamically allocated.
        But first get the next NDP offset and then free the curr NDP buff.
      ---------------------------------------------------------------------*/
      ndp_offset = ndp_hdr->next_ndp_index; /* Next ndp offset */

      if (PS_OPT_PATH_MAX_ST_NDP_LEN < ndp_hdr->len)
      {
         PS_SYSTEM_HEAP_MEM_FREE (ndp_buf);
      }

    }/*if: Outstanding NDP processing*/

    /*-----------------------------------------------------------------------
        Proceed with other NDPs in Outstanding MBIM
    -----------------------------------------------------------------------*/
    LOG_MSG_INFO3_1 ("Next NDP offset:%d", ndp_offset);

    while ( (ndp_offset != 0) && (ndp_offset < nth_hdr.mbim_block_len) )
    {

      /*---------------------------------------------------------------------
        Extract the NDP header length. Do a dsm_extract of length bytes to get
        complete NDP buf.
      ---------------------------------------------------------------------*/
      if ( PS_OPT_PATH_NDP_HDR_FIELD_LEN !=
            dsm_extract (
                          ntb_dsm_item,
                          (uint16)ndp_offset +
                          PS_OPT_PATH_NDP_HDR_FIELD_OFFSET,
                          &ndp_length,
                          PS_OPT_PATH_NDP_HDR_FIELD_LEN
                        )
         )
      {
        LOG_MSG_ERROR_0("Unable to retrieve NDP Header length field");
        dsm_free_packet(&ntb_dsm_item);
        return FALSE;
      }

      LOG_MSG_INFO3_1("NDP length from NDP header len field : %d",
                      ndp_length );

      /*---------------------------------------------------------------------
         1. NDP_Length <= 48 (Max 10 IP Pkts in NDP). Use Static Buffer.
         2. NDP_Length > 48 (Malloc accordingly)
      ---------------------------------------------------------------------*/
      if ( ndp_length <= PS_OPT_PATH_MAX_ST_NDP_LEN)
      {
        LOG_MSG_INFO3_0 ("ndp_length<=48" );

        if ( ndp_length != dsm_extract( ntb_dsm_item,
                                        (uint16)ndp_offset,
                                        info->mbim_pkt_info->ndp_short_buf,
                                        ndp_length)
           )
        {
          LOG_MSG_ERROR_0("Unable to retrieve full NDP buffer");
          dsm_free_packet(&ntb_dsm_item);
          return FALSE;

        }
        ndp_buf = info->mbim_pkt_info->ndp_short_buf;
     }
     else
     {

       LOG_MSG_INFO3_0 ("ndp_length>48" );

       /*malloc NDP length bytes*/
       ndp_buf = (uint8 *)(qmi_svc_ps_system_heap_mem_alloc(ndp_length,
                           FILE_ID_PS_UL_OPTIMIZED_HDLR,__LINE__));

       if(ndp_buf == NULL)
       {
         dsm_free_packet(&ntb_dsm_item);
         return FALSE;
       }

       if ( ndp_length != dsm_extract( ntb_dsm_item,
                                       (uint16)ndp_offset,
                                       ndp_buf,
                                       ndp_length)
          )
       {
         LOG_MSG_ERROR_0("Unable to retrieve full NDP buffer");
         PS_SYSTEM_HEAP_MEM_FREE (ndp_buf);
         dsm_free_packet(&ntb_dsm_item);
         return FALSE;
       }
     }

     /*NDP Common header*/
     ndp_hdr = (ps_iface_mbim_ntb_ndp_hdr *)ndp_buf;

     LOG_MSG_INFO3_1("NDP length: %d", ndp_hdr->len);

     /* Error checking for NDP command Header */
     if ( (ndp_hdr->signature != ndp_signature)    ||
          ndp_hdr->len > nth_hdr.mbim_block_len                      ||
          ndp_hdr->len < PS_OPT_PATH_NDP_LEN_CHECK          ||
          (ndp_hdr->len & 0x0003) != 0
       )

     {
       LOG_MSG_ERROR_2("Unrecognized NDP signature 0x%08X or length %d",
                       ndp_hdr->signature, ndp_hdr->len);
       if (PS_OPT_PATH_MAX_ST_NDP_LEN < ndp_length)
       {
         PS_SYSTEM_HEAP_MEM_FREE (ndp_buf);
       }
       dsm_free_packet(&ntb_dsm_item);
       return FALSE;
     }

     /*----------------------------------------------------------------------
       Start processing IP datagrams offsets and length in the NDP.
       We have all IP offsets and length in ndp_buf.
     ----------------------------------------------------------------------*/

     /*Start with first IP index/len*/

     ndp_datagram_offset  = PS_OPT_PATH_NDP_HDR_LEN;
     ndp_ip_datagram_info = (ps_iface_mbim_ntb_ndp_ip_datagram_info *)
                              (ndp_buf + PS_OPT_PATH_NDP_HDR_LEN) ;

     while ( ndp_datagram_offset < ndp_hdr->len )
     {
       LOG_MSG_INFO3_2("Start to process ndp 0x%4X at offset %d",
                       ndp_offset, ndp_datagram_offset);

      /*d/g offset, length structure*/
      //ndp16_s = (ntb_ndp16_s_t * )(ndp_buf + ndp_datagram_offset);

      /*---------------------------------------------------------------------
         Validate datagram Index and length and perform IP processing
         dgindex == 0 -> Terminate
      ---------------------------------------------------------------------*/
       if ( ndp_ip_datagram_info->index == 0)
       {

         LOG_MSG_INFO3_1("ndp datagram %d terminated with zero",
                         ndp_datagram_offset);

         break; /*break and continue with next NDP*/
       }

       else  /*Valid IP packet to process*/
       {
         LOG_MSG_INFO3_2(
"Process d/g nth_datagram_offset: %d,ndp_datagram_offset:%d",
ndp_ip_datagram_info->index, ndp_datagram_offset);

         LOG_MSG_INFO3_1 ("dsm_dup of bytes:%d", ndp_ip_datagram_info->len);

         /* Dup dsm item and start processing */
         if ( qmi_svc_dsm_dup_packet( &rx_pkt, ntb_dsm_item,
                              ndp_ip_datagram_info->index,
                              ndp_ip_datagram_info->len,
                              FILE_ID_PS_UL_OPTIMIZED_HDLR,__LINE__) != ndp_ip_datagram_info->len)
         {
           dsm_free_packet( &rx_pkt );
         }
         else
         {

           /*----------------------------------------------------------------
                 IP processing
           -----------------------------------------------------------------*/
           rval = ps_ul_optimized_process_qcncm_ip_no_qos( user_data,
                                                               rx_pkt);
           if ( rval == TRUE )
           {

             LOG_MSG_ERROR_0("Flow Disabled. Storing Outstanding mbim pkt"
                             "info in rmnet_dual_ip instance");
             /*-------------------------------------------------------------
               Store outstanding mbim pkt info in rmnet_dual_ip instance.
               If Short NDP buff used its already a part of rmnet dual ip
               instance.
             --------------------------------------------------------------*/
             info->mbim_pkt_info->ndp_datagram_offset = ndp_datagram_offset;
             info->mbim_pkt_info->ntb_dsm_item        = ntb_dsm_item;
             info->mbim_pkt_info->ndp_offset          = ndp_offset;
             info->mbim_pkt_info->nth_hdr             = nth_hdr;
             info->mbim_pkt_info->ndp_hdr             = ndp_hdr;
             info->rx_pkt                             = rx_pkt;

             if (PS_OPT_PATH_MAX_ST_NDP_LEN < ndp_hdr->len)
             {
               info->mbim_pkt_info->ndp_long_buf = ndp_buf;
             }
             return TRUE;
           }
         }

       } /*Else: Valid IP packet processing*/

       ndp_datagram_offset += PS_OPT_PATH_NDP_DG_OFFSET;
       ndp_ip_datagram_info ++;

     } /* while() for datagram processing in a NDP */

      /*---------------------------------------------------------------------
         Free NDP buff if allocated. Here ndp_buf could be pointing to our
       short_ndp_buf. Need to check this to free. Cant use Null check
         here since it is never NULL. So check for length and then free.

       But first get the next NDP offset and then free the curr NDP buff.
      ---------------------------------------------------------------------*/
     ndp_offset = ndp_hdr->next_ndp_index; /* Next ndp offset */

     if (ndp_hdr->len > PS_OPT_PATH_MAX_ST_NDP_LEN)
     {
       PS_SYSTEM_HEAP_MEM_FREE (ndp_buf);
     }

     LOG_MSG_INFO3_1 ("Next NDP offset:%d", ndp_offset);

   } /*while() for NDP processing*/

  } /*Else: Outstanding packet*/

  dsm_free_packet(&ntb_dsm_item);
  rx_pkt = NULL;

  LOG_MSG_INFO3_1 ("Returning rval at end : %d", rval  );

  return rval; /*TODO Usually we will be returning FALSE here??*/

} /* ps_ul_optimized_qcncm_ip_rx_sig_hdlr() */

/*===========================================================================
FUNCTION PS_UL_OPTIMIZED_IP_NO_QOS_RX_SIG_HDLR

DESCRIPTION
  This function is the optimized Signal handler for IP NO QOS mode. It decides
  if the packet needs to take the legacy path or should be forwarded to Um
  iface.

PARAMETERS
 rx_sig    :  RmNet signal type
 user_data :  pointer to user data.

RETURN VALUE
 TRUE :
 FALSE:

DEPENDENCIES
  None

SIDE EFFECTS
  None.

===========================================================================*/
boolean ps_ul_optimized_ip_no_qos_rx_sig_hdlr
(
  rmnet_sig_enum_type    rx_sig,
  void                 * user_data
)
{
  ps_ul_optimized_hdlr_path_type  path_type = PS_UL_OPTIMIZED_HDLR_NO_PATH;
  rmnet_smi_dual_ip_info_type  * info;
  ps_tx_meta_info_type         * tx_meta_info_ptr = NULL;
  rmnet_smi_info_type          * temp_info = NULL;
  ps_iface_type                * um_base_iface_ptr = NULL;
  ps_phys_link_type            * ps_phys_link_ptr;
  ps_flow_type                 * ps_flow_ptr = NULL;
  dsm_item_type                * rx_pkt = NULL;

  uint8    pkt_buf[PS_OPT_PATH_IP_NO_QOS_HEADER_LEN + 1] ALIGN(4); //__attribute__ ((aligned(4)));
  uint8    ip4_hdr_len = 0;
  uint8    ip_hdr_len  = 0;
  uint8    prot_offset = 0;
  uint16   total_len   = 0;
  boolean  rval        = FALSE;
  boolean  enable      = FALSE;
  sint15   ps_errno;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  PS_ENTER_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);

  PS_ENTER_CRIT_SECTION(&global_ps_crit_section);

  ASSERT(user_data);

  info = (rmnet_smi_dual_ip_info_type  *) user_data;

  do
  {
    /*-----------------------------------------------------------------------
      If rx_pkt is outstanding from previous time, use it. Otherwise, dequeue
      a new packet
    -----------------------------------------------------------------------*/
    if (NULL == info->rx_pkt)
    {
      rx_pkt = dsm_dequeue(& ((rmnet_sio_info_type *)
                           (info->info_v4->meta_sm.rmnet_sio_handle))->rx_wmk );
      if (NULL == rx_pkt)
      {
        rval = TRUE;
        break;
      }
    }
    else
    {
      LOG_MSG_INFO2_0 ("ps_ul_optimized_hdlr_ip_no_qos_rx_sig_hdlr()"
                       "Outstanding packet");
      rx_pkt       = info->rx_pkt;
      info->rx_pkt = NULL;
    }

    if (PS_OPT_PATH_IP_NO_QOS_HEADER_LEN_CHECK >
          dsm_extract(rx_pkt, 0, pkt_buf,
                      PS_OPT_PATH_IP_NO_QOS_HEADER_LEN))
    {
      path_type = PS_UL_OPTIMIZED_HDLR_LEGACY_PATH;
      break;
    }

    /*-----------------------------------------------------------------------
      Do appropriate processing for IPv4 and IPv6 packets
    -----------------------------------------------------------------------*/

    /*IPv4*/

    if ((*(pkt_buf) & PS_OPT_PATH_IP_VERSION_MASK) ==
        PS_OPT_PATH_V4_VERSION)
    {
      temp_info   = info->info_v4;
      ip4_hdr_len = (*(pkt_buf) & PS_OPT_PATH_IP_HDR_LEN_MASK) *
                     PS_OPT_PATH_WORD_SIZE;

      /*---------------------------------------------------------------------
        Update ip_hdr_len and protocol offset for TCP ACK prioritization
      ---------------------------------------------------------------------*/
       ip_hdr_len  = ip4_hdr_len;
       prot_offset = PS_OPT_PATH_V4_PROT_FIELD_OFFSET;

      /*---------------------------------------------------------------------
        Ensure that IPv4 call is UP
      ---------------------------------------------------------------------*/
      if (RMNET_META_SM_NULL_STATE != temp_info->meta_sm.state &&
          PS_IFACE_IS_VALID(temp_info->um_iface_ptr))
      {
        /*-------------------------------------------------------------------
          Flow control mechanism
          1.Single IPv4 call - Flow control if v4 Um is flow ctrlled.
          2.Dual-IP call(Single APN/Dual APN) - Flow ctrl only when v4 and
                                                v6 is flow ctrlled.
        -------------------------------------------------------------------*/
        ps_flow_ptr      = PS_IFACEI_GET_DEFAULT_FLOW(temp_info->um_iface_ptr);
        ps_phys_link_ptr = PS_IFACEI_GET_PHYS_LINK( temp_info->um_iface_ptr);

        /*-------------------------------------------------------------------
          Check if v4 Um is flow ctrlled. If QoS event is enabled, only 
          check IFACE flow enable.
        -------------------------------------------------------------------*/
        if (!(temp_info->meta_sm.qos_ev_enabled || PS_FLOW_IS_TX_ENABLED(ps_flow_ptr)) ||
            !(temp_info->meta_sm.qos_ev_enabled || PS_PHYS_LINK_FLOW_ENABLED( ps_phys_link_ptr)) ||
            !(PS_IFACEI_FLOW_ENABLED(temp_info->um_iface_ptr)))
        {

          /*-----------------------------------------------------------------
            Check if IPv6 call up
          -----------------------------------------------------------------*/
          if (RMNET_META_SM_NULL_STATE != info->info_v6->meta_sm.state &&
              PS_IFACE_IS_VALID(info->info_v6->um_iface_ptr))
          {

            /*---------------------------------------------------------------
              Its a Dual IP call. Check flow ctrl on V6
            ---------------------------------------------------------------*/
            if (!(info->info_v6->meta_sm.qos_ev_enabled || PS_FLOW_IS_TX_ENABLED(
                  PS_IFACEI_GET_DEFAULT_FLOW(info->info_v6->um_iface_ptr))) ||
                !(info->info_v6->meta_sm.qos_ev_enabled || PS_PHYS_LINK_FLOW_ENABLED(
                  PS_IFACEI_GET_PHYS_LINK(info->info_v6->um_iface_ptr))) ||
                !(PS_IFACEI_FLOW_ENABLED(info->info_v6->um_iface_ptr)))
            {

              /*-------------------------------------------------------------
                Since Flow is disabled on both V4 and V6 APNs we can return
                TRUE and stop de-queuing pkts.
              -------------------------------------------------------------*/
              LOG_MSG_ERROR_2("ps_ul_optimized_hdlr_ip_no_qos_rx_sig_hdlr():"
                              "Flow disabled on both V4V6 in Dual IP call"
                              "v4_phys_link_ptr:0x%p v6_phys_link_ptr:0x%p",
                              ps_phys_link_ptr,
                              PS_IFACEI_GET_PHYS_LINK(info->info_v6->um_iface_ptr) );

              /*Store Outstanding pkt in rmnet_dual_ip instance*/
              info->rx_pkt = rx_pkt;
              rval = TRUE;
              break;
            }

            /*---------------------------------------------------------------
              Flow is enabled on V6 APN and disabled on V4 in Dual-IP call.
              So we need to continue de-queueing pkts so that V6 data flow is
              On.Push this V4 pkt to Um though flow controlled and let Um Wmk
              handle it.
            ---------------------------------------------------------------*/
            LOG_MSG_ERROR_2 ("ps_ul_optimized_hdlr_ip_no_qos_rx_sig_hdlr():"
                             "V4 : Flow disabled,V6 : Enabled in Dual IP call"
                             "Cont to dequeue.."
                             "v4_phys_link_ptr:0x%p v6_phys_link_ptr:0x%p",
                             ps_phys_link_ptr,
                             PS_IFACEI_GET_PHYS_LINK(info->info_v6->um_iface_ptr) );

          }/*IPv6 call UP*/
          else
          {
            LOG_MSG_INFO2_0 ("ps_ul_optimized_hdlr_ip_no_qos_rx_sig_hdlr():"
                             "Flow disabled on single V4 call" );

            /*Store Outstanding pkt in rmnet_dual_ip instance*/
            info->rx_pkt = rx_pkt;
            rval = TRUE;
            break;
          }
        }/*V4 flow controlled*/

        /*-------------------------------------------------------------------
          Send packets up the stack for following cases
            1. If packet is destined to a broadcast address
            2. if packet is destined to a limited broadcast address
            3. If packet is destined to a multicast address
            4. If packet is fragmented
            5. if packet is destined to Rm iface
            6. if IP header has options
        -------------------------------------------------------------------*/
        if(ps_ul_optmized_hdlr_ipv4_is_legacy( pkt_buf,
                                               temp_info,
                                               ip4_hdr_len) == TRUE)
        {
          LOG_MSG_INFO3_0("IPV4_NO_QOS: goto_legacy = TRUE");

          /*Check if Legacy Wmk is flow controlled*/
          if(!(ps_ul_optimized_hdlr_legacy_flow_enabled(info)))
          {
            LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_ip_no_qos_rx_sig_hdlr():"
                             "Legacy Wmk Flow disabled");
            info->rx_pkt = rx_pkt;
            rval = TRUE; /*Outstanding packet*/
          }
          else
          {
            path_type = PS_UL_OPTIMIZED_HDLR_LEGACY_PATH;
          }
          break;
        }
      } /*V4 SM is UP*/
      else
      {
        LOG_MSG_ERROR_0("ps_ul_optimized_hdlr_ip_no_qos_rx_sig_hdlr():"
                        "V4 SM not up. Dropping packet");
        dsm_free_packet(&rx_pkt);
        break;
      }

      total_len = ps_htons( (uint16) *( (uint16 *) (pkt_buf +
                               PS_OPT_PATH_V4_TOTAL_LEN_OFFSET)));

      /*---------------------------------------------------------------------
       LOW LATENCY TRAFFIC handling
      ---------------------------------------------------------------------*/
      ps_ul_optimized_v4_low_latency_traffic_handler ( pkt_buf,
                                                       ip_hdr_len,
                                                       prot_offset );

    }

    /*IPv6*/
    else if (( *(pkt_buf) & PS_OPT_PATH_IP_VERSION_MASK) ==
               PS_OPT_PATH_V6_VERSION)
    {
      temp_info = info->info_v6;

      /*---------------------------------------------------------------------
        Update ip_hdr_len and protocol offset for TCP ACK prioritization
      ---------------------------------------------------------------------*/
       ip_hdr_len  = PS_OPT_PATH_V6_HDR_LEN;
       prot_offset = PS_OPT_PATH_V6_NXT_HDR_OFFSET;

      /*---------------------------------------------------------------------
        Ensure that IPv6 call is UP
      ---------------------------------------------------------------------*/
      if (RMNET_V6_SM_NULL_STATE != temp_info->v6_sm.state &&
          PS_IFACE_IS_VALID(temp_info->um_iface_ptr))
      {
        /*-------------------------------------------------------------------
          Flow control mechanism
          1.Single IPv6 call - Flow control if v6 Um is flow ctrlled.
          2.Dual-IP call(Single APN/Dual APN) - Flow ctrl only when v4 and
                                                v6 is flow ctrlled.
        -------------------------------------------------------------------*/
        ps_flow_ptr      = PS_IFACEI_GET_DEFAULT_FLOW(temp_info->um_iface_ptr);
        ps_phys_link_ptr = PS_IFACEI_GET_PHYS_LINK( temp_info->um_iface_ptr);

        /*-------------------------------------------------------------------
          Check if v6 Um is flow ctrlled. If QoS event is enabled, only
          checks for IFACE flow enable.
        -------------------------------------------------------------------*/
        if (!(temp_info->meta_sm.qos_ev_enabled || PS_FLOW_IS_TX_ENABLED(ps_flow_ptr)) ||
            !(temp_info->meta_sm.qos_ev_enabled || PS_PHYS_LINK_FLOW_ENABLED( ps_phys_link_ptr)) ||
            !(PS_IFACEI_FLOW_ENABLED(temp_info->um_iface_ptr)))
        {

          /*-----------------------------------------------------------------
            Check if IPv4 call up
          -----------------------------------------------------------------*/
          if (RMNET_META_SM_NULL_STATE != info->info_v4->meta_sm.state &&
              PS_IFACE_IS_VALID(info->info_v4->um_iface_ptr))
          {

            /*---------------------------------------------------------------
              Its a dual-ip call.Check flow ctrl on V4
            ---------------------------------------------------------------*/
            if (!(info->info_v4->meta_sm.qos_ev_enabled || PS_FLOW_IS_TX_ENABLED(
                  PS_IFACEI_GET_DEFAULT_FLOW(info->info_v4->um_iface_ptr))) ||
                !(info->info_v4->meta_sm.qos_ev_enabled || PS_PHYS_LINK_FLOW_ENABLED(
                  PS_IFACEI_GET_PHYS_LINK(info->info_v4->um_iface_ptr))) ||
                !(PS_IFACEI_FLOW_ENABLED(info->info_v4->um_iface_ptr)))
            {
              /*-------------------------------------------------------------
                Since Flow is disabled on both V4 and V6 APNs we can return
                TRUE and stop de-queuing pkts.
              -------------------------------------------------------------*/
              LOG_MSG_ERROR_2 ("ps_ul_optimized_hdlr_ip_no_qos_rx_sig_hdlr():"
                               "Flow disabled on both V4V6 in a Dual IP call"
                               "v6_phys_link_ptr:0x%p v4_phys_link_ptr:0x%p",
                               ps_phys_link_ptr,
                               PS_IFACEI_GET_PHYS_LINK (info->info_v4->um_iface_ptr) );
              rval = TRUE;
              /*Store Outstanding pkt in rmnet_dual_ip instance*/
              info->rx_pkt = rx_pkt;
              break;
            }

            /*--------------------------------------------------------------
              Flow is enabled on V4 APN and disabled on V6 in Dual-IP call.
              So we need to continue de-queueing pkts so that V4 data flow
              is On.Push this V6 pkt to Um though flow controlled and let Um
              Wmk handle it .
            ---------------------------------------------------------------*/
            LOG_MSG_ERROR_2 ("ps_ul_optimized_hdlr_ip_no_qos_rx_sig_hdlr():"
                             "V6:Flow disabled , V4: Enabled in Dual IP call"
                             "Cont to dequeue.."
                             "v6_phys_link_ptr:0x%p v4_phys_link_ptr:0x%p",
                             ps_phys_link_ptr,
                             PS_IFACEI_GET_PHYS_LINK(info->info_v4->um_iface_ptr) );

          }/*IPv4 Call up*/
          else
          {
            LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_ip_no_qos_rx_sig_hdlr():"
                             "Flow disabled on Single V6 call");
            rval = TRUE;
            /*Store Outstanding pkt in rmnet_dual_ip instance*/
            info->rx_pkt = rx_pkt;
            break;
          }
        }/*V6 flow controlled*/

        /*-------------------------------------------------------------------
          Send packets up the stack for following cases
          1. If packet is destined to a linklocal address
          2. If packet is destined to a multicast address
        -------------------------------------------------------------------*/
        if(ps_ul_optimized_ipv6_is_legacy(pkt_buf) == TRUE)
        {

          /*Check if Legacy Wmk is flow controlled*/
          if(!(ps_ul_optimized_hdlr_legacy_flow_enabled(info)))
          {
            LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_ip_no_qos_rx_sig_hdlr():"
                             "Legacy Wmk Flow disabled");
            info->rx_pkt = rx_pkt;
            rval = TRUE; /*Outstanding packet*/
          }
          else
          {
            path_type = PS_UL_OPTIMIZED_HDLR_LEGACY_PATH;
          }

          break;
        }
      }/*V6 SM up*/
      else
      {
        LOG_MSG_ERROR_0("V6 SM not up. Dropping packet");
        dsm_free_packet(&rx_pkt);
        break;
      }

      /*Drop packet if prefix does not match*/
      if(ps_ul_optimized_ipv6_prefix_cmp(pkt_buf, temp_info) == FALSE)
      {
        dsm_free_packet(&rx_pkt);
        break;
      }

      total_len  = ps_htons( (uint16) *((uint16 *) (pkt_buf +
                                    PS_OPT_PATH_V6_TOTAL_LEN_OFFSET)));
      total_len += PS_OPT_PATH_V6_HDR_LEN;
    }
    else
    {
      LOG_MSG_ERROR_0("Invalid IP version.Dropping packet");
      dsm_free_packet(&rx_pkt);
      break;
    }

    /*-----------------------------------------------------------------------
      Increment the receive packet stats
    -----------------------------------------------------------------------*/
    temp_info->rmnet_iface_ptr->iface_i_stats.pkts_rx++;
    temp_info->rmnet_iface_ptr->iface_i_stats.bytes_rx += total_len;

    /*-----------------------------------------------------------------------
      Log the packet on the Rx side
    -----------------------------------------------------------------------*/
    DPL_LOG_NETWORK_RX_PACKET(temp_info->rmnet_iface_ptr,
                              rx_pkt,
                              DPL_IID_NETPROT_IP);

    /*---------------------------------------------------------------------
        TCP ACK prioritization
    ---------------------------------------------------------------------*/
    if (ps_ul_optimized_tcp_ack_pri_check
          (
            pkt_buf,
            ip_hdr_len,
            total_len,
            prot_offset
          ) == TRUE
       )
    {
      LOG_MSG_INFO3_0("IP_NO_QOS: TCP_ACK_PRIORITY: Tagging DSM_HIGHEST");
      (rx_pkt)->priority = DSM_HIGHEST;
    }

    um_base_iface_ptr = PS_IFACE_GET_BASE_IFACE(temp_info->um_iface_ptr);

    PS_TX_META_INFO_AND_RT_META_INFO_GET(tx_meta_info_ptr);
    if (tx_meta_info_ptr == NULL ||
        PS_TX_META_GET_RT_META_INFO_PTR(tx_meta_info_ptr) == NULL)
    {
      LOG_MSG_ERROR_1("IP_NO_QOS: Cannot allocate meta info tx 0x%p",
                      tx_meta_info_ptr);
      dsm_free_packet(&rx_pkt);
      rx_pkt = NULL;
      break;
    }

    PS_TX_META_SET_ROUTING_CACHE(tx_meta_info_ptr, um_base_iface_ptr);

    if (PS_IFACEI_NUM_FILTERS(um_base_iface_ptr, IP_FLTR_CLIENT_QOS_OUTPUT) > 0)
    {
       path_type = PS_UL_OPTIMIZED_HDLR_OPT_QOS_PATH;
    }
    else
    {
      /*--------------------------------------------------------------------
        If flow capability is set to PS_FLOW_CAPABILITY_DATA_DISALLOWED
        drop the packet
      --------------------------------------------------------------------*/
      if ((PS_FLOW_GET_CAPABILITY(ps_flow_ptr,
              PS_FLOW_CAPABILITY_DATA_DISALLOWED)))
      {
        LOG_MSG_ERROR_0("PS_FLOW_CAPABILITY_DATA_DISALLOWED:Dropping packet" );
        dsm_free_packet(&rx_pkt);
        rx_pkt = NULL;
        break;
      }

      um_base_iface_ptr->iface_i_stats.pkts_tx++;
      um_base_iface_ptr->iface_i_stats.bytes_tx += total_len;

      /*---------------------------------------------------------------------
        Log packet on the Tx side
      ---------------------------------------------------------------------*/
      DPL_LOG_NETWORK_TX_PACKET(um_base_iface_ptr,
                                rx_pkt,
                                DPL_IID_NETPROT_IP);

      path_type = PS_UL_OPTIMIZED_HDLR_OPT_PATH;
    }
  } while (0);

  /*-------------------------------------------------------------------------
   Leave CS before tx_cmd to avoid possible deadlock issues.
  -------------------------------------------------------------------------*/

  PS_LEAVE_CRIT_SECTION(&global_ps_crit_section);
  PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);

  /*-------------------------------------------------------------------------
   Pass packet to optimized or legacy path based on path.
  -------------------------------------------------------------------------*/
  switch (path_type)
  {
    case PS_UL_OPTIMIZED_HDLR_OPT_PATH:
    {
      /*-----------------------------------------------------------------------
        Make sure that the power save mode is disabled when recieving the first uplink packet
        on this path
      -------------------------------------------------------------------------*/
      if( TRUE == PS_IFACE_GET_POWERSAVE_FILTERING_MODE(temp_info->rmnet_iface_ptr) )
      {
        if( PS_IFACE_GET_POWERSAVE_FILTERING_AUTO_EXIT_MODE(temp_info->rmnet_iface_ptr) )
        {
          ps_iface_set_powersave_filtering_mode( temp_info->rmnet_iface_ptr, 
                                                 enable, &ps_errno );
        }
      }

      um_base_iface_ptr->iface_private.tx_cmd
      (
        um_base_iface_ptr,
        &rx_pkt,
        tx_meta_info_ptr,
        um_base_iface_ptr->iface_private.tx_cmd_info
      );

      rx_pkt = NULL;
      break;
    }

    case PS_UL_OPTIMIZED_HDLR_OPT_QOS_PATH:
    {
      /*-----------------------------------------------------------------------
        Make sure that the power save mode is disabled when recieving the first uplink packet
        on this path
      -------------------------------------------------------------------------*/
      if( TRUE == PS_IFACE_GET_POWERSAVE_FILTERING_MODE(temp_info->rmnet_iface_ptr) )
      {
        if( PS_IFACE_GET_POWERSAVE_FILTERING_AUTO_EXIT_MODE(temp_info->rmnet_iface_ptr) )
        {
          ps_iface_set_powersave_filtering_mode( temp_info->rmnet_iface_ptr, 
                                                 enable, &ps_errno );
        }
      }

      ps_iface_tx_cmd(um_base_iface_ptr, &rx_pkt, tx_meta_info_ptr);
      rx_pkt = NULL;
      break;
    }

    case PS_UL_OPTIMIZED_HDLR_LEGACY_PATH:
    {
      ps_ul_optimized_fallback_hdlr(user_data, rx_pkt);
      rx_pkt = NULL;
      break;
    }

    case PS_UL_OPTIMIZED_HDLR_NO_PATH:
    {
      PS_TX_META_INFO_FREE(&tx_meta_info_ptr);
      break;
    }

    default:
    {
      LOG_MSG_ERROR_0("ps_ul_optimized_hdlr_path_type error");
      ASSERT(0);
      break;
    }
  }

  return rval;

} /* ps_ul_optimized_ip_no_qos_rx_sig_hdlr() */

/*===========================================================================
FUNCTION PS_UL_OPTIMIZED_IP_QOS_RX_SIG_HDLR

DESCRIPTION
  This function is the optimized Signal handler for IP QOS mode. It decides
  if the packet needs to take the legacy path or should be forwarded to Um
  iface..

PARAMETERS
  rx_sig    : RmNet signal type.
  user_data : pointer to user data.

RETURN VALUE
  TRUE:
  FALSE:

DEPENDENCIES
  None

SIDE EFFECTS
  None.

===========================================================================*/
boolean ps_ul_optimized_ip_qos_rx_sig_hdlr
(
  rmnet_sig_enum_type    rx_sig,
  void                 * user_data
)
{
  ps_ul_optimized_hdlr_path_type  path_type = PS_UL_OPTIMIZED_HDLR_NO_PATH;
  rmnet_smi_dual_ip_info_type  * info;
  ps_tx_meta_info_type         * tx_meta_info_ptr = NULL;
  rmnet_smi_info_type          * temp_info = NULL;
  ps_iface_type                * um_base_iface_ptr = NULL;
  ps_phys_link_type            * ps_phys_link_ptr = NULL;
  ps_flow_type                 * ps_flow_ptr = NULL;
  rmnet_meta_smi_qos_hdr_type  * qos_hdr = NULL;
  dsm_item_type                * rx_pkt = NULL;
  int                            pullup_size =
                                 PS_OPT_PATH_QOS_HEADER_OFFSET;
  uint16                         total_len = 0;
  uint16                         ip_id = 0;

  uint8    pkt_buf[PS_OPT_PATH_IP_QOS_HEADER_LEN  +  1] ALIGN(4); //__attribute__ ((aligned(4)));
  uint8    version       = 0;
  uint8    ip4_hdr_len   = 0;
  uint8    ip_hdr_len    = 0;
  uint8    prot_offset   = 0;
  boolean  rval          = FALSE;
  boolean  enable        = FALSE;
  sint15   ps_errno;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  PS_ENTER_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);

  PS_ENTER_CRIT_SECTION(&global_ps_crit_section);

  ASSERT(user_data);

  info = (rmnet_smi_dual_ip_info_type  *) user_data;

  do
  {
    /*-----------------------------------------------------------------------
      If rx_pkt is outstanding from previous time, use it. Otherwise, dequeue
      a new packet
    -----------------------------------------------------------------------*/
    if (NULL == info->rx_pkt)
    {
      rx_pkt = dsm_dequeue(& ((rmnet_sio_info_type *)
                          (info->info_v4->meta_sm.rmnet_sio_handle))->rx_wmk );
      if (NULL == rx_pkt)
      {
        rval = TRUE;
        break;
      }
    }
    else
    {
      LOG_MSG_INFO2_0 ("ps_ul_optimized_hdlr_ip_qos_rx_sig_hdlr():"
                       "Outstanding packet");
      rx_pkt       = info->rx_pkt;
      info->rx_pkt = NULL;
    }

    if (PS_OPT_PATH_IP_QOS_HEADER_LEN_CHECK >
        dsm_extract(rx_pkt,
                    0,
                    pkt_buf,
                    PS_OPT_PATH_IP_QOS_HEADER_LEN))
    {
      path_type = PS_UL_OPTIMIZED_HDLR_LEGACY_PATH;
      break;
    }

    /*-----------------------------------------------------------------------
      Retrieve QoS header
    -----------------------------------------------------------------------*/
    qos_hdr = (rmnet_meta_smi_qos_hdr_type *) pkt_buf;

    /*-----------------------------------------------------------------------
      Do appropriate processing for IPv4 and IPv6 packets
    -----------------------------------------------------------------------*/

    /*IPv4*/
    if ((* (pkt_buf + PS_OPT_PATH_QOS_HEADER_OFFSET) &
            PS_OPT_PATH_IP_VERSION_MASK) ==
            PS_OPT_PATH_V4_VERSION
       )
    {
      temp_info = info->info_v4;
      ip4_hdr_len = (*(pkt_buf + PS_OPT_PATH_QOS_HEADER_OFFSET)
                     & PS_OPT_PATH_IP_HDR_LEN_MASK) *
                     PS_OPT_PATH_WORD_SIZE;

      /*---------------------------------------------------------------------
        Update ip_hdr_len and protocol offset for TCP ACK prioritization
      ---------------------------------------------------------------------*/
      ip_hdr_len  = ip4_hdr_len;
      prot_offset = PS_OPT_PATH_V4_PROT_FIELD_OFFSET;

      /*---------------------------------------------------------------------
        Ensure that IPv4 call is UP
      ---------------------------------------------------------------------*/
      if (RMNET_META_SM_NULL_STATE != temp_info->meta_sm.state &&
          PS_IFACE_IS_VALID(temp_info->um_iface_ptr))
      {
        /*-------------------------------------------------------------------
          Flow control mechanism
          1.Single IPv4 call - Flow control if v4 Um is flow ctrlled.
          2.Dual-IP call(Single APN/Dual APN) - Flow ctrl only when v4 and
                                                v6 is flow ctrlled.
          3. If QoS bearers are not established - Flow control if phys_link
                                                  is flow controlled.
        -------------------------------------------------------------------*/
        um_base_iface_ptr = PS_IFACE_GET_BASE_IFACE(temp_info->um_iface_ptr);
        ps_phys_link_ptr = PS_IFACEI_GET_PHYS_LINK( temp_info->um_iface_ptr);   

        /*-------------------------------------------------------------------
          Check if v4 Um is flow ctrlled
        -------------------------------------------------------------------*/
        if (!(PS_IFACEI_FLOW_ENABLED(temp_info->um_iface_ptr)) ||
             ((PS_IFACE_GET_NUM_SEC_FLOWS(um_base_iface_ptr) == 0) && 
               !(PS_PHYS_LINK_FLOW_ENABLED(ps_phys_link_ptr))))
        {
          /*-----------------------------------------------------------------
            Check if IPv6 call up
          -----------------------------------------------------------------*/
          if (RMNET_META_SM_NULL_STATE != info->info_v6->meta_sm.state &&
              PS_IFACE_IS_VALID(info->info_v6->um_iface_ptr))
          {
            um_base_iface_ptr = PS_IFACE_GET_BASE_IFACE(info->info_v6->um_iface_ptr);
            ps_phys_link_ptr = PS_IFACEI_GET_PHYS_LINK(info->info_v6->um_iface_ptr);
            /*---------------------------------------------------------------
              Its a Dual IP call. Check flow ctrl on V6
            ---------------------------------------------------------------*/
            if (!(PS_IFACEI_FLOW_ENABLED(info->info_v6->um_iface_ptr)) ||
                 ((PS_IFACE_GET_NUM_SEC_FLOWS(um_base_iface_ptr) == 0) && 
                   !(PS_PHYS_LINK_FLOW_ENABLED(ps_phys_link_ptr))))
            {
              /*-------------------------------------------------------------
                Since Flow is disabled on both V4 and V6 APNs we can return
                TRUE and stop de-queuing pkts.
              -------------------------------------------------------------*/
              LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_ip_qos_rx_sig_hdlr():"
                               "Flow disabled on both V4V6 in Dual IP call");
              /*Store Outstanding pkt in rmnet_dual_ip instance*/
              info->rx_pkt = rx_pkt;
              rval = TRUE;
              break;
            }

            /*---------------------------------------------------------------
              Flow is enabled on V6 APN and disabled on V4 in Dual-IP call.
              So we need to continue de-queueing pkts so that V6 data flow is
              On.Push this V4 pkt to Um though flow controlled and let Um Wmk
              handle it .
            ---------------------------------------------------------------*/
            LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_ip_qos_rx_sig_hdlr():"
                             "V4:Flow disabled , V6: Enabled in Dual IP call"
                             "Cont to dequeue..");

          }/*V6 call up*/
          else
          {
            LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_ip_qos_rx_sig_hdlr():"
                             "Flow disabled on Single V4 call");
            /*Store Outstanding pkt in rmnet_dual_ip instance*/
            info->rx_pkt = rx_pkt;
            rval = TRUE;
            break;
          }
        }/*V4 Flow controled*/

        /*-------------------------------------------------------------------
          Send packets up the stack for following cases
          1. If packet is destined to a broadcast address
          2. if packet is destined to a limited broadcast address
          3. If packet is destined to a multicast address
          4. If packet is fragmented
          5. if packet is destined to Rm iface
          6. if IP header has options
        -------------------------------------------------------------------*/
        if (ps_ul_optimized_ipv4_opt_is_legacy
          ( pkt_buf + PS_OPT_PATH_QOS_HEADER_OFFSET,
            temp_info,
            ip4_hdr_len
          ) == TRUE
         )
        {
          /*Check if Legacy Wmk is flow controlled*/
          if(!(ps_ul_optimized_hdlr_legacy_flow_enabled(info)))
          {
            LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_ip_qos_rx_sig_hdlr():"
                             "Legacy Wmk Flow disabled");
            info->rx_pkt = rx_pkt;
            rval = TRUE; /*Outstanding packet*/
          }
          else
          {
            path_type = PS_UL_OPTIMIZED_HDLR_LEGACY_PATH;
          }
          break;
        }
      }/*V4 SM up*/
      else
      {
        LOG_MSG_ERROR_0("V4 SM not up. Dropping packet");
        dsm_free_packet(&rx_pkt);
        break;
      }

      ip_id     = (uint16) (* (uint16 *) (pkt_buf +
                                 PS_OPT_PATH_QOS_HEADER_OFFSET +
                                 PS_OPT_PATH_V4_ID_OFFSET));
      version   = IP_V4;
      total_len = ps_htons ((uint16)* ((uint16 *)(pkt_buf +
                               PS_OPT_PATH_QOS_HEADER_OFFSET +
                               PS_OPT_PATH_V4_TOTAL_LEN_OFFSET)));
      /*---------------------------------------------------------------------
       LOW LATENCY TRAFFIC handling
      ---------------------------------------------------------------------*/
      ps_ul_optimized_v4_low_latency_traffic_handler ( pkt_buf,
                                                 ip_hdr_len +
                                                 PS_OPT_PATH_QOS_HEADER_OFFSET,
                                                 prot_offset + 
                                                 PS_OPT_PATH_QOS_HEADER_OFFSET);

    }

    /*IPv6*/
    else if ((*(pkt_buf + PS_OPT_PATH_QOS_HEADER_OFFSET) &
              PS_OPT_PATH_IP_VERSION_MASK) ==
              PS_OPT_PATH_V6_VERSION)
    {
      temp_info = info->info_v6;

      /*---------------------------------------------------------------------
        Update ip_hdr_len and protocol offset for TCP ACK prioritization
      ---------------------------------------------------------------------*/
       ip_hdr_len  = PS_OPT_PATH_V6_HDR_LEN;
       prot_offset = PS_OPT_PATH_V6_NXT_HDR_OFFSET;

      /*---------------------------------------------------------------------
        Ensure that IPv6 call is UP
      ---------------------------------------------------------------------*/
      if (RMNET_V6_SM_NULL_STATE != temp_info->v6_sm.state &&
          PS_IFACE_IS_VALID(temp_info->um_iface_ptr))
      {
        /*-------------------------------------------------------------------
          Flow control mechanism
          1.Single IPv6 call - Flow control if v6 Um is flow ctrlled.
          2.Dual-IP call(Single APN/Dual APN) - Flow ctrl only when v4 and
                                                v6 is flow ctrlled.
          3. If QoS bearers are not established - Flow control if phys_link
                                                  is flow controlled.
        -------------------------------------------------------------------*/
        um_base_iface_ptr = PS_IFACE_GET_BASE_IFACE(temp_info->um_iface_ptr);
        ps_phys_link_ptr = PS_IFACEI_GET_PHYS_LINK(temp_info->um_iface_ptr);

        /*-------------------------------------------------------------------
          Check Flow control on v6 Um
        -------------------------------------------------------------------*/
        if (!(PS_IFACEI_FLOW_ENABLED(temp_info->um_iface_ptr)) ||
             ((PS_IFACE_GET_NUM_SEC_FLOWS(um_base_iface_ptr) == 0) && 
               !(PS_PHYS_LINK_FLOW_ENABLED(ps_phys_link_ptr))))
        {

          /*-----------------------------------------------------------------
            Check if IPv4 call up
          -----------------------------------------------------------------*/
          if (RMNET_META_SM_NULL_STATE != info->info_v4->meta_sm.state &&
              PS_IFACE_IS_VALID(info->info_v4->um_iface_ptr))
          {
            um_base_iface_ptr = PS_IFACE_GET_BASE_IFACE(info->info_v4->um_iface_ptr);
            ps_phys_link_ptr = PS_IFACEI_GET_PHYS_LINK(info->info_v4->um_iface_ptr);

            /*---------------------------------------------------------------
              Its a Dual-IP call. Check flow ctrl on V4
            ---------------------------------------------------------------*/
            if (!(PS_IFACEI_FLOW_ENABLED(info->info_v4->um_iface_ptr)) ||
                 ((PS_IFACE_GET_NUM_SEC_FLOWS(um_base_iface_ptr) == 0) && 
                   !(PS_PHYS_LINK_FLOW_ENABLED(ps_phys_link_ptr))))
            {
              /*-------------------------------------------------------------
                Since Flow is disabled on both V4 and V6 APNs we can return
                TRUE and stop de-queuing pkts.
              -------------------------------------------------------------*/
              LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_ip_qos_rx_sig_hdlr():"
                               "Flow disabled on both V4V6 in Dual IP call");
              /*Store Outstanding pkt in rmnet_dual_ip instance*/
              info->rx_pkt = rx_pkt;
              rval = TRUE;
              break;
            }

            /*---------------------------------------------------------------
              Flow is enabled on V4 APN and disabled on V6 in Dual-IP call.
              So we need to continue de-queueing pkts so that V4 data flow is
              On.Push this V6 pkt to Um though flow controlled and let Um Wmk
              handle it .
            ---------------------------------------------------------------*/
            LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_ip_qos_rx_sig_hdlr():"
                             "V6:Flow disabled , V4: Enabled in Dual IP call"
                             "Cont to dequeue..");

          }/*V4 Call up*/
          else
          {
            LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_ip_qos_rx_sig_hdlr():"
                             "Flow disabled on Single V6 call");
            /*Store Outstanding pkt in rmnet_dual_ip instance*/
            info->rx_pkt = rx_pkt;
            rval = TRUE;
            break;
          }
        }/*V6 flow controlled*/

        /*-------------------------------------------------------------------
          Send packets up the stack for following cases
          1. If packet is destined to a linklocal address
          2. If packet is destined to a multicast address
        -------------------------------------------------------------------*/
        if(ps_ul_optimized_ipv6_opt_is_legacy(
             pkt_buf + PS_OPT_PATH_QOS_HEADER_OFFSET) == TRUE)
        {

          /*Check if Legacy Wmk is flow controlled*/
          if(!(ps_ul_optimized_hdlr_legacy_flow_enabled(info)))
          {
            LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_ip_qos_rx_sig_hdlr():"
                             "Legacy Wmk Flow disabled");
            info->rx_pkt = rx_pkt;
            rval = TRUE; /*Outstanding packet*/
          }
          else
          {
            path_type = PS_UL_OPTIMIZED_HDLR_LEGACY_PATH;
          }
          break;
        }
      }/*V6 SM up*/
      else
      {
        LOG_MSG_ERROR_0("V6 SM not up. Dropping packet");
        dsm_free_packet(&rx_pkt);
        break;
      }

      /*Drop packet if prefix does not match*/
      if(ps_ul_optimized_ipv6_prefix_cmp_unaligned(pkt_buf +
			PS_OPT_PATH_QOS_HEADER_OFFSET, temp_info) == FALSE)
      {
        dsm_free_packet(&rx_pkt);
        break;
      }

      total_len  = ps_htons((uint16)*((uint16 *)(pkt_buf +
                               PS_OPT_PATH_QOS_HEADER_OFFSET +
                               PS_OPT_PATH_V6_TOTAL_LEN_OFFSET)));
      total_len += PS_OPT_PATH_V6_HDR_LEN;

    }
    else
    {
      LOG_MSG_ERROR_0("Invalid IP version. Dropping packet");
      dsm_free_packet(&rx_pkt);
      break;
    }

    /*-----------------------------------------------------------------------
      Pull out QoS hdr
    -----------------------------------------------------------------------*/
    (void) qmi_svc_dsm_pullup(&rx_pkt, NULL, pullup_size,FILE_ID_PS_UL_OPTIMIZED_HDLR,__LINE__);

    /*---------------------------------------------------------------------
        TCP ACK prioritization after dsm_pullup
    ---------------------------------------------------------------------*/
    if (ps_ul_optimized_tcp_ack_pri_check
          ( pkt_buf + PS_OPT_PATH_QOS_HEADER_OFFSET,
            ip_hdr_len,
            total_len,
            prot_offset
          ) == TRUE
       )
    {
      LOG_MSG_INFO3_0("IP_QOS: TCP_ACK_PRIORITY: Tagging DSM_HIGHEST");
      (rx_pkt)->priority = DSM_HIGHEST;
    }

    /*-----------------------------------------------------------------------
      Increment the receive packet stats
    -----------------------------------------------------------------------*/
    temp_info->rmnet_iface_ptr->iface_i_stats.pkts_rx++;
    temp_info->rmnet_iface_ptr->iface_i_stats.bytes_rx += total_len;

    /*-----------------------------------------------------------------------
      Log the packet on the Rx side
    -----------------------------------------------------------------------*/
    DPL_LOG_NETWORK_RX_PACKET(temp_info->rmnet_iface_ptr,
                              rx_pkt,
                              DPL_IID_NETPROT_IP);

    um_base_iface_ptr = PS_IFACE_GET_BASE_IFACE(temp_info->um_iface_ptr);

    /*-----------------------------------------------------------------------
      Process QoS header
    -----------------------------------------------------------------------*/
    ps_ul_optimized_process_qos_hdr(rx_pkt,
                                    &tx_meta_info_ptr,
                                    qos_hdr,
                                    um_base_iface_ptr,
                                    version,
                                    ip_id);

    /*-----------------------------------------------------------------------
      Get Flow from Meta Info. If flow capability is set to
      PS_FLOW_CAPABILITY_DATA_DISALLOWED drop the packet
    -----------------------------------------------------------------------*/
    if ((tx_meta_info_ptr != NULL) &&
        (PS_TX_META_GET_RT_META_INFO_PTR(tx_meta_info_ptr) != NULL) &&
        (PS_TX_META_GET_PKT_META_INFO_PTR(tx_meta_info_ptr) != NULL)
       )
    {
      ps_flow_ptr = PS_FLOW_GET_FLOW_FROM_META_INFO(tx_meta_info_ptr);

      if (ps_flow_ptr == NULL)
      {
        ps_flow_ptr = PS_IFACEI_GET_DEFAULT_FLOW(temp_info->um_iface_ptr);
      }
    }

    if ((PS_FLOW_GET_CAPABILITY(ps_flow_ptr,
              PS_FLOW_CAPABILITY_DATA_DISALLOWED)))
    {
      LOG_MSG_ERROR_0("PS_FLOW_CAPABILITY_DATA_DISALLOWED:Dropping packet" );
      dsm_free_packet(&rx_pkt);
      rx_pkt = NULL;
      break;
    }

    um_base_iface_ptr->iface_i_stats.pkts_tx++;
    um_base_iface_ptr->iface_i_stats.bytes_tx += total_len;

    /*-----------------------------------------------------------------------
      Log packet on the Tx side
    -----------------------------------------------------------------------*/
    DPL_LOG_NETWORK_TX_PACKET(um_base_iface_ptr,
                              rx_pkt,
                              DPL_IID_NETPROT_IP);

    path_type = PS_UL_OPTIMIZED_HDLR_OPT_PATH;

  } while (0);

  /*-------------------------------------------------------------------------
   Leave CS before tx_cmd to avoid possible deadlock issues.
  -------------------------------------------------------------------------*/

  PS_LEAVE_CRIT_SECTION(&global_ps_crit_section);
  PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);

  /*-------------------------------------------------------------------------
   Pass packet to optimized or legacy path based on path.
  -------------------------------------------------------------------------*/
  switch (path_type)
  {
    case PS_UL_OPTIMIZED_HDLR_OPT_PATH:
    {
      /*-----------------------------------------------------------------------
        Make sure that the power save mode is disabled when recieving the first uplink packet
        on this path
      -------------------------------------------------------------------------*/
      if( TRUE == PS_IFACE_GET_POWERSAVE_FILTERING_MODE(temp_info->rmnet_iface_ptr) )
      {
        if( PS_IFACE_GET_POWERSAVE_FILTERING_AUTO_EXIT_MODE(temp_info->rmnet_iface_ptr) )
        {
          ps_iface_set_powersave_filtering_mode( temp_info->rmnet_iface_ptr, 
                                                 enable, &ps_errno );
        }
      }

    um_base_iface_ptr->iface_private.tx_cmd
    (
      um_base_iface_ptr,
      &rx_pkt,
      tx_meta_info_ptr,
      um_base_iface_ptr->iface_private.tx_cmd_info
    );

    rx_pkt           = NULL;
      break;
    }

    case PS_UL_OPTIMIZED_HDLR_LEGACY_PATH:
    {
      ps_ul_optimized_fallback_hdlr(user_data, rx_pkt);
      rx_pkt = NULL;
      break;
    }

    case PS_UL_OPTIMIZED_HDLR_NO_PATH:
    {
      PS_TX_META_INFO_FREE(&tx_meta_info_ptr);
      break;
    }

    default:
    {
      LOG_MSG_ERROR_0("ps_ul_optimized_hdlr_path_type error");
      ASSERT(0);
      break;
    }
  }
  return rval;
} /* ps_ul_optimized_ip_qos_rx_sig_hdlr() */

/*===========================================================================
FUNCTION PS_UL_OPTIMIZED_IP_QOS2_RX_SIG_HDLR

DESCRIPTION
  This function is the optimized Signal handler for IP QOSv2 (8-byte header)
  mode. It decides if the packet needs to take the legacy path or should be
  forwarded to Um iface

PARAMETERS
  rx_sig    : RmNet signal type.
  user_data : pointer to user data.

RETURN VALUE
  TRUE:
  FALSE:

DEPENDENCIES
  None

SIDE EFFECTS
  None.

===========================================================================*/
boolean ps_ul_optimized_ip_qos2_rx_sig_hdlr
(
  rmnet_sig_enum_type    rx_sig,
  void                 * user_data
)
{
  ps_ul_optimized_hdlr_path_type  path_type = PS_UL_OPTIMIZED_HDLR_NO_PATH;
  rmnet_smi_dual_ip_info_type  * info;
  ps_tx_meta_info_type         * tx_meta_info_ptr = NULL;
  rmnet_smi_info_type          * temp_info = NULL;
  ps_iface_type                * um_base_iface_ptr = NULL;
  ps_phys_link_type            * ps_phys_link_ptr = NULL;
  ps_flow_type                 * ps_flow_ptr = NULL;
  rmnet_meta_smi_qos2_hdr_type  * qos_hdr = NULL;
  dsm_item_type                * rx_pkt = NULL;
  int                            pullup_size =
                                 PS_OPT_PATH_QOS2_HEADER_OFFSET;
  uint16                         total_len = 0;
  uint16                         ip_id = 0;

  uint8    pkt_buf[PS_OPT_PATH_IP_QOS2_HEADER_LEN  +  1] ALIGN(4); //__attribute__ ((aligned(4)));
  uint8    version       = 0;
  uint8    ip4_hdr_len   = 0;
  uint8    ip_hdr_len    = 0;
  uint8    prot_offset   = 0;
  boolean  rval          = FALSE;
  boolean  enable      = FALSE;
  sint15   ps_errno;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  PS_ENTER_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);

  PS_ENTER_CRIT_SECTION(&global_ps_crit_section);

  ASSERT(user_data);

  info = (rmnet_smi_dual_ip_info_type  *) user_data;

  do
  {
    /*-----------------------------------------------------------------------
      If rx_pkt is outstanding from previous time, use it. Otherwise, dequeue
      a new packet
    -----------------------------------------------------------------------*/
    if (NULL == info->rx_pkt)
    {
      rx_pkt = dsm_dequeue(& ((rmnet_sio_info_type *)
                          (info->info_v4->meta_sm.rmnet_sio_handle))->rx_wmk );
      if (NULL == rx_pkt)
      {
        rval = TRUE;
        break;
      }
    }
    else
    {
      LOG_MSG_INFO2_0 ("ps_ul_optimized_hdlr_ip_qos2_rx_sig_hdlr():"
                       "Outstanding packet");
      rx_pkt       = info->rx_pkt;
      info->rx_pkt = NULL;
    }

    if (PS_OPT_PATH_IP_QOS2_HEADER_LEN_CHECK >
        dsm_extract(rx_pkt,
                    0,
                    pkt_buf,
                    PS_OPT_PATH_IP_QOS2_HEADER_LEN))
    {
      path_type = PS_UL_OPTIMIZED_HDLR_LEGACY_PATH;
      break;
    }

    /*-----------------------------------------------------------------------
      Retrieve QoS header
    -----------------------------------------------------------------------*/
    qos_hdr = (rmnet_meta_smi_qos2_hdr_type *) pkt_buf;

    /*-----------------------------------------------------------------------
      Do appropriate processing for IPv4 and IPv6 packets
    -----------------------------------------------------------------------*/

    /*IPv4*/
    if ((* (pkt_buf + PS_OPT_PATH_QOS2_HEADER_OFFSET) &
            PS_OPT_PATH_IP_VERSION_MASK) ==
            PS_OPT_PATH_V4_VERSION
       )
    {
      temp_info = info->info_v4;
      ip4_hdr_len = (*(pkt_buf + PS_OPT_PATH_QOS2_HEADER_OFFSET)
                     & PS_OPT_PATH_IP_HDR_LEN_MASK) *
                     PS_OPT_PATH_WORD_SIZE;

      /*---------------------------------------------------------------------
        Update ip_hdr_len and protocol offset for TCP ACK prioritization
      ---------------------------------------------------------------------*/
      ip_hdr_len  = ip4_hdr_len;
      prot_offset = PS_OPT_PATH_V4_PROT_FIELD_OFFSET;

      /*---------------------------------------------------------------------
        Ensure that IPv4 call is UP
      ---------------------------------------------------------------------*/
      if (RMNET_META_SM_NULL_STATE != temp_info->meta_sm.state &&
          PS_IFACE_IS_VALID(temp_info->um_iface_ptr))
      {
        /*-------------------------------------------------------------------
          Flow control mechanism
          1.Single IPv4 call - Flow control if v4 Um is flow ctrlled.
          2.Dual-IP call(Single APN/Dual APN) - Flow ctrl only when v4 and
                                                v6 is flow ctrlled.
          3. If QoS bearers are not established - Flow control if phys_link
                                                  is flow controlled.
        -------------------------------------------------------------------*/
        um_base_iface_ptr = PS_IFACE_GET_BASE_IFACE(temp_info->um_iface_ptr);
        ps_phys_link_ptr = PS_IFACEI_GET_PHYS_LINK(temp_info->um_iface_ptr);

        /*-------------------------------------------------------------------
          Check if v4 Um is flow ctrlled
        -------------------------------------------------------------------*/
        if (!(PS_IFACEI_FLOW_ENABLED(temp_info->um_iface_ptr)) ||
             ((PS_IFACE_GET_NUM_SEC_FLOWS(um_base_iface_ptr) == 0) && 
               !(PS_PHYS_LINK_FLOW_ENABLED(ps_phys_link_ptr))))
        {
          /*-----------------------------------------------------------------
            Check if IPv6 call up
          -----------------------------------------------------------------*/
          if (RMNET_META_SM_NULL_STATE != info->info_v6->meta_sm.state &&
              PS_IFACE_IS_VALID(info->info_v6->um_iface_ptr))
          {
            um_base_iface_ptr = PS_IFACE_GET_BASE_IFACE(info->info_v6->um_iface_ptr);
            ps_phys_link_ptr = PS_IFACEI_GET_PHYS_LINK(info->info_v6->um_iface_ptr);

            /*---------------------------------------------------------------
              Its a Dual IP call. Check flow ctrl on V6
            ---------------------------------------------------------------*/
            if (!(PS_IFACEI_FLOW_ENABLED(temp_info->um_iface_ptr)) ||
                 ((PS_IFACE_GET_NUM_SEC_FLOWS(um_base_iface_ptr) == 0) && 
                   !(PS_PHYS_LINK_FLOW_ENABLED(ps_phys_link_ptr))))
            {
              /*-------------------------------------------------------------
                Since Flow is disabled on both V4 and V6 APNs we can return
                TRUE and stop de-queuing pkts.
              -------------------------------------------------------------*/
              LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_ip_qos2_rx_sig_hdlr():"
                               "Flow disabled on both V4V6 in Dual IP call");
              /*Store Outstanding pkt in rmnet_dual_ip instance*/
              info->rx_pkt = rx_pkt;
              rval = TRUE;
              break;
            }

            /*---------------------------------------------------------------
              Flow is enabled on V6 APN and disabled on V4 in Dual-IP call.
              So we need to continue de-queueing pkts so that V6 data flow is
              On.Push this V4 pkt to Um though flow controlled and let Um Wmk
              handle it .
            ---------------------------------------------------------------*/
            LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_ip_qos2_rx_sig_hdlr():"
                             "V4:Flow disabled , V6: Enabled in Dual IP call"
                             "Cont to dequeue..");

          }/*V6 call up*/
          else
          {
            LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_ip_qos2_rx_sig_hdlr():"
                             "Flow disabled on Single V4 call");
            /*Store Outstanding pkt in rmnet_dual_ip instance*/
            info->rx_pkt = rx_pkt;
            rval = TRUE;
            break;
          }
        }/*V4 Flow controled*/

        /*-------------------------------------------------------------------
          Send packets up the stack for following cases
          1. If packet is destined to a broadcast address
          2. if packet is destined to a limited broadcast address
          3. If packet is destined to a multicast address
          4. If packet is fragmented
          5. if packet is destined to Rm iface
          6. if IP header has options
        -------------------------------------------------------------------*/
        if (ps_ul_optimized_ipv4_opt_is_legacy
          ( pkt_buf + PS_OPT_PATH_QOS2_HEADER_OFFSET,
            temp_info,
            ip4_hdr_len
          ) == TRUE
         )
        {
          /*Check if Legacy Wmk is flow controlled*/
          if(!(ps_ul_optimized_hdlr_legacy_flow_enabled(info)))
          {
            LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_ip_qos2_rx_sig_hdlr():"
                             "Legacy Wmk Flow disabled");
            info->rx_pkt = rx_pkt;
            rval = TRUE; /*Outstanding packet*/
          }
          else
          {
            path_type = PS_UL_OPTIMIZED_HDLR_LEGACY_PATH;
          }
          break;
        }
      }/*V4 SM up*/
      else
      {
        LOG_MSG_ERROR_0("V4 SM not up. Dropping packet");
        dsm_free_packet(&rx_pkt);
        break;
      }

      ip_id     = (uint16) (* (uint16 *) (pkt_buf +
                                 PS_OPT_PATH_QOS2_HEADER_OFFSET +
                                 PS_OPT_PATH_V4_ID_OFFSET));
      version   = IP_V4;
      total_len = ps_htons ((uint16)* ((uint16 *)(pkt_buf +
                               PS_OPT_PATH_QOS2_HEADER_OFFSET +
                               PS_OPT_PATH_V4_TOTAL_LEN_OFFSET)));
      /*---------------------------------------------------------------------
       LOW LATENCY TRAFFIC handling
      ---------------------------------------------------------------------*/
      ps_ul_optimized_v4_low_latency_traffic_handler ( pkt_buf,
                                                ip_hdr_len + 
                                                PS_OPT_PATH_QOS2_HEADER_OFFSET,
                                                prot_offset +
                                                PS_OPT_PATH_QOS2_HEADER_OFFSET);

    }

    /*IPv6*/
    else if ((*(pkt_buf + PS_OPT_PATH_QOS2_HEADER_OFFSET) &
              PS_OPT_PATH_IP_VERSION_MASK) ==
              PS_OPT_PATH_V6_VERSION)
    {
      temp_info = info->info_v6;

      /*---------------------------------------------------------------------
        Update ip_hdr_len and protocol offset for TCP ACK prioritization
      ---------------------------------------------------------------------*/
       ip_hdr_len  = PS_OPT_PATH_V6_HDR_LEN;
       prot_offset = PS_OPT_PATH_V6_NXT_HDR_OFFSET;

      /*---------------------------------------------------------------------
        Ensure that IPv6 call is UP
      ---------------------------------------------------------------------*/
      if (RMNET_V6_SM_NULL_STATE != temp_info->v6_sm.state &&
          PS_IFACE_IS_VALID(temp_info->um_iface_ptr))
      {
        /*-------------------------------------------------------------------
          Flow control mechanism
          1.Single IPv6 call - Flow control if v6 Um is flow ctrlled.
          2.Dual-IP call(Single APN/Dual APN) - Flow ctrl only when v4 and
                                                v6 is flow ctrlled.
          3.If QoS bearers are not established - Flow control if phys_link
                                                 is flow controlled.
        -------------------------------------------------------------------*/
        um_base_iface_ptr = PS_IFACE_GET_BASE_IFACE(temp_info->um_iface_ptr);
        ps_phys_link_ptr = PS_IFACEI_GET_PHYS_LINK(temp_info->um_iface_ptr);

        /*-------------------------------------------------------------------
          Check Flow control on v6 Um
        -------------------------------------------------------------------*/
        if (!(PS_IFACEI_FLOW_ENABLED(temp_info->um_iface_ptr)) ||
             ((PS_IFACE_GET_NUM_SEC_FLOWS(um_base_iface_ptr) == 0) && 
               !(PS_PHYS_LINK_FLOW_ENABLED(ps_phys_link_ptr))))
        {

          /*-----------------------------------------------------------------
            Check if IPv4 call up
          -----------------------------------------------------------------*/
          if (RMNET_META_SM_NULL_STATE != info->info_v4->meta_sm.state &&
              PS_IFACE_IS_VALID(info->info_v4->um_iface_ptr))
          {
            um_base_iface_ptr = PS_IFACE_GET_BASE_IFACE(info->info_v4->um_iface_ptr);
            ps_phys_link_ptr = PS_IFACEI_GET_PHYS_LINK(info->info_v4->um_iface_ptr);

            /*---------------------------------------------------------------
              Its a Dual-IP call. Check flow ctrl on V4
            ---------------------------------------------------------------*/
            if (!(PS_IFACEI_FLOW_ENABLED(temp_info->um_iface_ptr)) ||
                 ((PS_IFACE_GET_NUM_SEC_FLOWS(um_base_iface_ptr) == 0) && 
                   !(PS_PHYS_LINK_FLOW_ENABLED(ps_phys_link_ptr))))
            {
              /*-------------------------------------------------------------
                Since Flow is disabled on both V4 and V6 APNs we can return
                TRUE and stop de-queuing pkts.
              -------------------------------------------------------------*/
              LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_ip_qos2_rx_sig_hdlr():"
                               "Flow disabled on both V4V6 in Dual IP call");
              /*Store Outstanding pkt in rmnet_dual_ip instance*/
              info->rx_pkt = rx_pkt;
              rval = TRUE;
              break;
            }

            /*---------------------------------------------------------------
              Flow is enabled on V4 APN and disabled on V6 in Dual-IP call.
              So we need to continue de-queueing pkts so that V4 data flow is
              On.Push this V6 pkt to Um though flow controlled and let Um Wmk
              handle it .
            ---------------------------------------------------------------*/
            LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_ip_qos2_rx_sig_hdlr():"
                             "V6:Flow disabled , V4: Enabled in Dual IP call"
                             "Cont to dequeue..");

          }/*V4 Call up*/
          else
          {
            LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_ip_qos2_rx_sig_hdlr():"
                             "Flow disabled on Single V6 call");
            /*Store Outstanding pkt in rmnet_dual_ip instance*/
            info->rx_pkt = rx_pkt;
            rval = TRUE;
            break;
          }
        }/*V6 flow controlled*/

        /*-------------------------------------------------------------------
          Send packets up the stack for following cases
          1. If packet is destined to a linklocal address
          2. If packet is destined to a multicast address
        -------------------------------------------------------------------*/
        if(ps_ul_optimized_ipv6_opt_is_legacy(
             pkt_buf + PS_OPT_PATH_QOS2_HEADER_OFFSET) == TRUE)
        {

          /*Check if Legacy Wmk is flow controlled*/
          if(!(ps_ul_optimized_hdlr_legacy_flow_enabled(info)))
          {
            LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_ip_qos2_rx_sig_hdlr():"
                             "Legacy Wmk Flow disabled");
            info->rx_pkt = rx_pkt;
            rval = TRUE; /*Outstanding packet*/
          }
          else
          {
            path_type = PS_UL_OPTIMIZED_HDLR_LEGACY_PATH;
          }
          break;
        }
      }/*V6 SM up*/
      else
      {
        LOG_MSG_ERROR_0("V6 SM not up. Dropping packet");
        dsm_free_packet(&rx_pkt);
        break;
      }

      /*Drop packet if prefix does not match*/
      if(ps_ul_optimized_ipv6_prefix_cmp_unaligned(pkt_buf +
			PS_OPT_PATH_QOS2_HEADER_OFFSET, temp_info) == FALSE)
      {
        dsm_free_packet(&rx_pkt);
        break;
      }

      total_len  = ps_htons((uint16)*((uint16 *)(pkt_buf +
                               PS_OPT_PATH_QOS2_HEADER_OFFSET +
                               PS_OPT_PATH_V6_TOTAL_LEN_OFFSET)));
      total_len += PS_OPT_PATH_V6_HDR_LEN;

    }
    else
    {
      LOG_MSG_ERROR_0("Invalid IP version. Dropping packet");
      dsm_free_packet(&rx_pkt);
      break;
    }

    /*-----------------------------------------------------------------------
      Pull out QoS hdr
    -----------------------------------------------------------------------*/
    (void) qmi_svc_dsm_pullup(&rx_pkt, NULL, pullup_size,FILE_ID_PS_UL_OPTIMIZED_HDLR,__LINE__);

    /*---------------------------------------------------------------------
        TCP ACK prioritization after dsm_pullup
    ---------------------------------------------------------------------*/
    if (ps_ul_optimized_tcp_ack_pri_check
          ( pkt_buf + PS_OPT_PATH_QOS2_HEADER_OFFSET,
            ip_hdr_len,
            total_len,
            prot_offset
          ) == TRUE
       )
    {
      LOG_MSG_INFO3_0("IP_QOS: TCP_ACK_PRIORITY: Tagging DSM_HIGHEST");
      (rx_pkt)->priority = DSM_HIGHEST;
    }

    /*-----------------------------------------------------------------------
      Increment the receive packet stats
    -----------------------------------------------------------------------*/
    temp_info->rmnet_iface_ptr->iface_i_stats.pkts_rx++;
    temp_info->rmnet_iface_ptr->iface_i_stats.bytes_rx += total_len;

    /*-----------------------------------------------------------------------
      Log the packet on the Rx side
    -----------------------------------------------------------------------*/
    DPL_LOG_NETWORK_RX_PACKET(temp_info->rmnet_iface_ptr,
                              rx_pkt,
                              DPL_IID_NETPROT_IP);

    um_base_iface_ptr = PS_IFACE_GET_BASE_IFACE(temp_info->um_iface_ptr);

    /*-----------------------------------------------------------------------
      Process QoS header
    -----------------------------------------------------------------------*/
    ps_ul_optimized_process_qos2_hdr(rx_pkt,
                                    &tx_meta_info_ptr,
                                    qos_hdr,
                                    um_base_iface_ptr,
                                    version,
                                    ip_id);

    /*-----------------------------------------------------------------------
      Get Flow from Meta Info. If flow capability is set to
      PS_FLOW_CAPABILITY_DATA_DISALLOWED drop the packet
    -----------------------------------------------------------------------*/
    if ((tx_meta_info_ptr != NULL) &&
        (PS_TX_META_GET_RT_META_INFO_PTR(tx_meta_info_ptr) != NULL) &&
        (PS_TX_META_GET_PKT_META_INFO_PTR(tx_meta_info_ptr) != NULL)
       )
    {
      ps_flow_ptr = PS_FLOW_GET_FLOW_FROM_META_INFO(tx_meta_info_ptr);

      if (ps_flow_ptr == NULL)
      {
        ps_flow_ptr = PS_IFACEI_GET_DEFAULT_FLOW(temp_info->um_iface_ptr);
      }
    }

    if ((PS_FLOW_GET_CAPABILITY(ps_flow_ptr,
              PS_FLOW_CAPABILITY_DATA_DISALLOWED)))
    {
      LOG_MSG_ERROR_0("PS_FLOW_CAPABILITY_DATA_DISALLOWED:Dropping packet" );
      dsm_free_packet(&rx_pkt);
      rx_pkt = NULL;
      break;
    }

    um_base_iface_ptr->iface_i_stats.pkts_tx++;
    um_base_iface_ptr->iface_i_stats.bytes_tx += total_len;

    /*-----------------------------------------------------------------------
      Log packet on the Tx side
    -----------------------------------------------------------------------*/
    DPL_LOG_NETWORK_TX_PACKET(um_base_iface_ptr,
                              rx_pkt,
                              DPL_IID_NETPROT_IP);

    path_type = PS_UL_OPTIMIZED_HDLR_OPT_PATH;

  } while (0);

  /*-------------------------------------------------------------------------
   Leave CS before tx_cmd to avoid possible deadlock issues.
  -------------------------------------------------------------------------*/

  PS_LEAVE_CRIT_SECTION(&global_ps_crit_section);
  PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);

  /*-------------------------------------------------------------------------
   Pass packet to optimized or legacy path based on path.
  -------------------------------------------------------------------------*/
  switch (path_type)
  {
    case PS_UL_OPTIMIZED_HDLR_OPT_PATH:
    {
      /*-----------------------------------------------------------------------
        Make sure that the power save mode is disabled when recieving the first uplink packet
        on this path
      -------------------------------------------------------------------------*/
      if( TRUE == PS_IFACE_GET_POWERSAVE_FILTERING_MODE(temp_info->rmnet_iface_ptr) )
      {
        if( PS_IFACE_GET_POWERSAVE_FILTERING_AUTO_EXIT_MODE(temp_info->rmnet_iface_ptr) )
        {
          ps_iface_set_powersave_filtering_mode( temp_info->rmnet_iface_ptr, 
                                                 enable, &ps_errno );
        }
      }

    um_base_iface_ptr->iface_private.tx_cmd
    (
      um_base_iface_ptr,
      &rx_pkt,
      tx_meta_info_ptr,
      um_base_iface_ptr->iface_private.tx_cmd_info
    );

    rx_pkt           = NULL;
      break;
    }

    case PS_UL_OPTIMIZED_HDLR_LEGACY_PATH:
    {
      ps_ul_optimized_fallback_hdlr(user_data, rx_pkt);
      rx_pkt = NULL;
      break;
    }

    case PS_UL_OPTIMIZED_HDLR_NO_PATH:
    {
      PS_TX_META_INFO_FREE(&tx_meta_info_ptr);
      break;
    }

    default:
    {
      LOG_MSG_ERROR_0("ps_ul_optimized_hdlr_path_type error");
      ASSERT(0);
      break;
    }
  }
  return rval;
} /* ps_ul_optimized_ip_qos2_rx_sig_hdlr() */

/*===========================================================================
FUNCTION PS_UL_OPTIMIZED_ETH_NO_QOS_RX_SIG_HDLR

DESCRIPTION
  This function is the optimized Signal handler for ETH NO QOS mode. It decides
  if the packet needs to take the legacy path or should be forwarded to Um
  iface.

PARAMETERS
  rx_sig    : RmNet signal type.
  user_data : pointer to user data.

RETURN VALUE
 TRUE:
 FALSE:

DEPENDENCIES
  None

SIDE EFFECTS
  None.

===========================================================================*/
boolean ps_ul_optimized_eth_no_qos_rx_sig_hdlr
(
  rmnet_sig_enum_type    rx_sig,
  void                 * user_data
)
{
  ps_ul_optimized_hdlr_path_type  path_type = PS_UL_OPTIMIZED_HDLR_NO_PATH;
  rmnet_smi_dual_ip_info_type  * info;
  ps_tx_meta_info_type         * tx_meta_info_ptr = NULL;
  rmnet_smi_info_type          * temp_info = NULL;
  ps_iface_type                * um_base_iface_ptr = NULL;
  ps_phys_link_type            * ps_phys_link_ptr;
  ps_flow_type                 * ps_flow_ptr = NULL;
  dsm_item_type                * rx_pkt      = NULL;
  int                            pullup_size =
                                 PS_OPT_PATH_ETH_HEADER_OFFSET;
  uint16                         total_len = 0;

  uint8    pkt_buf[PS_OPT_PATH_ETH_NO_QOS_HEADER_LEN  +  1] ALIGN(4); //__attribute__ ((aligned(4)));
  uint8    ip4_hdr_len   = 0;
  uint8    ip_hdr_len    = 0;
  uint8    prot_offset   = 0;
  boolean  rval          = FALSE;
  boolean  enable      = FALSE;
  sint15   ps_errno;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  PS_ENTER_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);

  PS_ENTER_CRIT_SECTION(&global_ps_crit_section);

  ASSERT(user_data);

  info = (rmnet_smi_dual_ip_info_type  *) user_data;

  do
  {
    /*-----------------------------------------------------------------------
      If rx_pkt is outstanding from previous time, use it. Otherwise, dequeue
      a new packet
     -----------------------------------------------------------------------*/
    if (NULL == info->rx_pkt)
    {
      rx_pkt = dsm_dequeue(&( (rmnet_sio_info_type *) (
                           info->info_v4->meta_sm.rmnet_sio_handle))->rx_wmk );
      if (NULL == rx_pkt)
      {
        rval = TRUE;
        break;
      }
    }
    else
    {
      LOG_MSG_INFO2_0 ("ps_ul_optimized_hdlr_eth_no_qos_rx_sig_hdlr():"
                       "Outstanding packet");
      rx_pkt       = info->rx_pkt;
      info->rx_pkt = NULL;
    }

    if (PS_OPT_PATH_ETH_NO_QOS_HEADER_LEN_CHECK >
          dsm_extract (rx_pkt,
                       0,
                       pkt_buf,
                       PS_OPT_PATH_ETH_NO_QOS_HEADER_LEN)
                      )
    {
      path_type = PS_UL_OPTIMIZED_HDLR_LEGACY_PATH;
      break;
    }

    /*-----------------------------------------------------------------------
      Do appropriate processing for IPv4 and IPv6 packets
    -----------------------------------------------------------------------*/

    /*IPv4*/
    if (* (uint16 *) (pkt_buf + PS_OPT_PATH_ETHERTYPE_OFFSET) ==
        ps_htons(LAN_ETHERTYPE_IPV4))
    {
      temp_info = info->info_v4;
      ip4_hdr_len = (*(pkt_buf + PS_OPT_PATH_ETH_HEADER_OFFSET) &
                       PS_OPT_PATH_IP_HDR_LEN_MASK
                      ) * PS_OPT_PATH_WORD_SIZE;

      /*---------------------------------------------------------------------
        Update ip_hdr_len and protocol offset for TCP ACK prioritization
      ---------------------------------------------------------------------*/
      ip_hdr_len  = ip4_hdr_len;
      prot_offset = PS_OPT_PATH_V4_PROT_FIELD_OFFSET;

      /*---------------------------------------------------------------------
        Ensure that IPv4 call is UP
      ---------------------------------------------------------------------*/
      if (RMNET_META_SM_NULL_STATE != temp_info->meta_sm.state &&
          PS_IFACE_IS_VALID(temp_info->um_iface_ptr))
      {
        /*-------------------------------------------------------------------
          Flow control mechanism
          1.Single IPv4 call - Flow control if v4 Um is flow ctrlled.
          2.Dual-IP call(Single APN/Dual APN) - Flow ctrl only when v4 and
                                                v6 is flow ctrlled.
        -------------------------------------------------------------------*/
        ps_flow_ptr      = PS_IFACEI_GET_DEFAULT_FLOW(temp_info->um_iface_ptr);
        ps_phys_link_ptr = PS_IFACEI_GET_PHYS_LINK( temp_info->um_iface_ptr);

        /*-------------------------------------------------------------------
          Check if v4 Um is flow ctrlled
        -------------------------------------------------------------------*/
        if (!(PS_FLOW_IS_TX_ENABLED(ps_flow_ptr)) ||
            !(PS_PHYS_LINK_FLOW_ENABLED( ps_phys_link_ptr)) ||
            !(PS_IFACEI_FLOW_ENABLED(temp_info->um_iface_ptr)))
        {

          /*-----------------------------------------------------------------
            Check if IPv6 call up
          -----------------------------------------------------------------*/
          if (RMNET_META_SM_NULL_STATE != info->info_v6->meta_sm.state &&
              PS_IFACE_IS_VALID(info->info_v6->um_iface_ptr))
          {
            /*---------------------------------------------------------------
              Its a Dual IP call. Check flow ctrl on V6
            ---------------------------------------------------------------*/
            if (!(PS_FLOW_IS_TX_ENABLED(
                  PS_IFACEI_GET_DEFAULT_FLOW(info->info_v6->um_iface_ptr))) ||
                !(PS_PHYS_LINK_FLOW_ENABLED(
                  PS_IFACEI_GET_PHYS_LINK(info->info_v6->um_iface_ptr))) ||
                !(PS_IFACEI_FLOW_ENABLED(info->info_v6->um_iface_ptr)))
            {
              /*-------------------------------------------------------------
                Since Flow is disabled on both V4 and V6 APNs we can return
                TRUE and stop de-queuing pkts.
              -------------------------------------------------------------*/
              LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_eth_no_qos_rx_sig_hdlr():"
                               "Flow disabled on both V4V6 Dual IP call");
              /*Store Outstanding pkt in rmnet_dual_ip instance*/
              info->rx_pkt = rx_pkt;
              rval = TRUE;
              break;
            }

            /*---------------------------------------------------------------
              Flow is enabled on V6 APN and disabled on V4 in Dual-IP call.
              So we need to continue de-queueing pkts so that V6 data flow is
              On.Push this V4 pkt to Um though flow controlled and let Um Wmk
              handle it .
            ---------------------------------------------------------------*/
            LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_eth_no_qos_rx_sig_hdlr():"
                             "V4:Flow disabled , V6:Enabled in Dual IP call"
                             "Cont to dequeue..");

          }/*Ipv6 Call up*/
          else
          {
            LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_eth_no_qos_rx_sig_hdlr():"
                             "Flow disabled on Single V4 call");
            /*Store Outstanding pkt in rmnet_dual_ip instance*/
            info->rx_pkt = rx_pkt;
            rval = TRUE;
            break;
          }
        }/*V4 flow controlled*/

        /*-------------------------------------------------------------------
          Send packets up the stack for following cases
          1. If packet is destined to a broadcast address
          2. if packet is destined to a limited broadcast address
          3. If packet is destined to a multicast address
          4. If packet is fragmented
          5. if packet is destined to Rm iface
          6. if IP header has options
        -------------------------------------------------------------------*/
        if (ps_ul_optimized_ipv4_opt_is_legacy
          (
           pkt_buf + PS_OPT_PATH_ETH_HEADER_OFFSET,
           temp_info,
           ip4_hdr_len
           ) == TRUE
          )
        {
          /*Check if Legacy Wmk is flow controlled*/
          if(!(ps_ul_optimized_hdlr_legacy_flow_enabled(info)))
          {
            LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_eth_no_qos_rx_sig_hdlr():"
                             "Legacy Wmk Flow disabled");
            info->rx_pkt = rx_pkt;
            rval = TRUE; /*Outstanding packet*/
          }
          else
          {
            path_type = PS_UL_OPTIMIZED_HDLR_LEGACY_PATH;
          }
          break;
        }
      }/*V4 call up*/
      else
      {
        LOG_MSG_ERROR_0("V4 SM not up. Dropping packet");
        dsm_free_packet(&rx_pkt);
        break;
      }

      total_len = ps_htons ((uint16)*((uint16 *)(pkt_buf +
                               PS_OPT_PATH_ETH_HEADER_OFFSET +
                               PS_OPT_PATH_V4_TOTAL_LEN_OFFSET))
                             );
      /*---------------------------------------------------------------------
       LOW LATENCY TRAFFIC handling
      ---------------------------------------------------------------------*/
      ps_ul_optimized_v4_low_latency_traffic_handler ( pkt_buf,
                                                ip_hdr_len + 
                                                PS_OPT_PATH_ETH_HEADER_OFFSET,
                                                prot_offset + 
                                                PS_OPT_PATH_ETH_HEADER_OFFSET);

    }

    /*IPv6*/
    else if ( *(uint16 *) (pkt_buf + PS_OPT_PATH_ETHERTYPE_OFFSET) ==
              ps_htons(LAN_ETHERTYPE_IPV6))
    {
      temp_info = info->info_v6;

      /*---------------------------------------------------------------------
        Update ip_hdr_len and protocol offset for TCP ACK prioritization
      ---------------------------------------------------------------------*/
       ip_hdr_len  = PS_OPT_PATH_V6_HDR_LEN;
       prot_offset = PS_OPT_PATH_V6_NXT_HDR_OFFSET;

      /*---------------------------------------------------------------------
        Ensure that IPv6 call is UP
      ---------------------------------------------------------------------*/
      if (RMNET_V6_SM_NULL_STATE != temp_info->v6_sm.state &&
          PS_IFACE_IS_VALID(temp_info->um_iface_ptr))
      {
        /*-------------------------------------------------------------------
          Flow control mechanism
          1.Single IPv6 call - Flow control if v6 Um is flow ctrlled.
          2.Dual-IP call(Single APN/Dual APN) - Flow ctrl only when v4 and
                                                v6 is flow ctrlled.
        -------------------------------------------------------------------*/
        ps_flow_ptr      = PS_IFACEI_GET_DEFAULT_FLOW(temp_info->um_iface_ptr);
        ps_phys_link_ptr = PS_IFACEI_GET_PHYS_LINK( temp_info->um_iface_ptr);

        /*-------------------------------------------------------------------
          Check Flow control on v6 Um
        -------------------------------------------------------------------*/
        if (!(PS_FLOW_IS_TX_ENABLED(ps_flow_ptr)) ||
            !(PS_PHYS_LINK_FLOW_ENABLED( ps_phys_link_ptr)) ||
            !(PS_IFACEI_FLOW_ENABLED(temp_info->um_iface_ptr)))
        {

          /*-----------------------------------------------------------------
            Check if IPv4 call up
          -----------------------------------------------------------------*/
          if (RMNET_META_SM_NULL_STATE != info->info_v4->meta_sm.state &&
              PS_IFACE_IS_VALID(info->info_v4->um_iface_ptr))
          {

            /*---------------------------------------------------------------
              Its a Dual IP call. Check flow ctrl on V4
            ---------------------------------------------------------------*/
            if (!(PS_FLOW_IS_TX_ENABLED(
                  PS_IFACEI_GET_DEFAULT_FLOW(info->info_v4->um_iface_ptr))) ||
                !(PS_PHYS_LINK_FLOW_ENABLED(
                  PS_IFACEI_GET_PHYS_LINK(info->info_v4->um_iface_ptr))) ||
                !(PS_IFACEI_FLOW_ENABLED(info->info_v4->um_iface_ptr)))
            {
              /*-------------------------------------------------------------
                Since Flow is disabled on both V4 and V6 APNs we can return
                TRUE and stop de-queuing pkts.
              -------------------------------------------------------------*/
              LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_eth_no_qos_rx_sig_hdlr():"
                               "Flow disabled on both V4V6 in a Dual IP call");
              /*Store Outstanding pkt in rmnet_dual_ip instance*/
              info->rx_pkt = rx_pkt;
              rval = TRUE;
              break;
            }

            /*---------------------------------------------------------------
              Flow is enabled on V4 APN and disabled on V6 in Dual-IP call.
              So we need to continue de-queueing pkts so that V4 data flow is
              On.Push this V4 pkt to Um though flow controlled and let Um Wmk
              handle it .
            ---------------------------------------------------------------*/
            LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_eth_no_qos_rx_sig_hdlr():"
                             "V6:Flow disabled , V4:Enabled in Dual IP call"
                             "Cont to dequeue..");

          }/*V4 call up*/
          else
          {

            LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_eth_no_qos_rx_sig_hdlr():"
                             "Flow disabled on Single V6 call");
            /*Store Outstanding pkt in rmnet_dual_ip instance*/
            info->rx_pkt = rx_pkt;
            rval = TRUE;
            break;
          }
        }/*V6 flow controlled*/

        /*-------------------------------------------------------------------
          Send packets up the stack for following cases
          1. If packet is destined to a linklocal address
          2. If packet is destined to a multicast address
        -------------------------------------------------------------------*/
        if(ps_ul_optimized_ipv6_opt_is_legacy(
           pkt_buf + PS_OPT_PATH_ETH_HEADER_OFFSET) == TRUE)
        {

          /*Check if Legacy Wmk is flow controlled*/
          if(!(ps_ul_optimized_hdlr_legacy_flow_enabled(info)))
          {
            LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_eth_no_qos_rx_sig_hdlr():"
                             "Legacy Wmk Flow disabled");
            info->rx_pkt = rx_pkt;
            rval = TRUE; /*Outstanding packet*/
          }
          else
          {
            path_type = PS_UL_OPTIMIZED_HDLR_LEGACY_PATH;
          }
          break;
        }
      }/*V6 SM up*/
      else
      {
        LOG_MSG_ERROR_0("V6 SM not up. Dropping packet");
        dsm_free_packet(&rx_pkt);
        break;
      }

      /*Drop packet if prefix does not match*/
      if(ps_ul_optimized_ipv6_prefix_cmp_unaligned(pkt_buf +
            PS_OPT_PATH_ETH_HEADER_OFFSET, temp_info) == FALSE)
      {
        dsm_free_packet(&rx_pkt);
        break;
      }

      total_len      = ps_htons((uint16)*((uint16 *)(pkt_buf +
                                  PS_OPT_PATH_ETH_HEADER_OFFSET +
                                  PS_OPT_PATH_V6_TOTAL_LEN_OFFSET)));
      total_len     += PS_OPT_PATH_V6_HDR_LEN;
    }
    else
    {
      path_type = PS_UL_OPTIMIZED_HDLR_LEGACY_PATH;
      break;
    }

    /*-----------------------------------------------------------------------
      Pull up Ethernet header
    -----------------------------------------------------------------------*/
    (void) qmi_svc_dsm_pullup(&rx_pkt, NULL, pullup_size,FILE_ID_PS_UL_OPTIMIZED_HDLR,__LINE__);

    /*---------------------------------------------------------------------
        TCP ACK prioritization after dsm_pullup
    ---------------------------------------------------------------------*/
    if (ps_ul_optimized_tcp_ack_pri_check
          ( pkt_buf + PS_OPT_PATH_ETH_HEADER_OFFSET,
            ip_hdr_len,
            total_len,
            prot_offset
          ) == TRUE
       )
    {
      LOG_MSG_INFO3_0("ETH_NO_QOS: TCP_ACK_PRIORITY: Tagging DSM_HIGHEST");
      (rx_pkt)->priority = DSM_HIGHEST;
    }

    /*-----------------------------------------------------------------------
      Increment the receive packet stats
    -----------------------------------------------------------------------*/
    temp_info->rmnet_iface_ptr->iface_i_stats.pkts_rx++;
    temp_info->rmnet_iface_ptr->iface_i_stats.bytes_rx += total_len;

    /*-----------------------------------------------------------------------
      Log the packet on the Rx side
    -----------------------------------------------------------------------*/
    DPL_LOG_NETWORK_RX_PACKET(temp_info->rmnet_iface_ptr,
                              rx_pkt,
                              DPL_IID_NETPROT_IP);

    um_base_iface_ptr = PS_IFACE_GET_BASE_IFACE(temp_info->um_iface_ptr);

    PS_TX_META_INFO_AND_RT_META_INFO_GET(tx_meta_info_ptr);
    if (tx_meta_info_ptr == NULL ||
        PS_TX_META_GET_RT_META_INFO_PTR(tx_meta_info_ptr) == NULL)
    {
      LOG_MSG_ERROR_1("ETH_NO_QOS: Cannot allocate meta info tx 0x%p",
                      tx_meta_info_ptr);
      dsm_free_packet(&rx_pkt);
      rx_pkt = NULL;
      break;
    }

    PS_TX_META_SET_ROUTING_CACHE(tx_meta_info_ptr, um_base_iface_ptr);

    if (PS_IFACEI_NUM_FILTERS(um_base_iface_ptr, IP_FLTR_CLIENT_QOS_OUTPUT) > 0)
    {
      path_type = PS_UL_OPTIMIZED_HDLR_OPT_QOS_PATH;
    }
    else
    {
      /*--------------------------------------------------------------------
        If flow capability is set to PS_FLOW_CAPABILITY_DATA_DISALLOWED
        drop the packet
      --------------------------------------------------------------------*/
      if ((PS_FLOW_GET_CAPABILITY(ps_flow_ptr,
              PS_FLOW_CAPABILITY_DATA_DISALLOWED)))
      {
        LOG_MSG_ERROR_0("PS_FLOW_CAPABILITY_DATA_DISALLOWED:Dropping packet" );
        dsm_free_packet(&rx_pkt);
        rx_pkt = NULL;
        break;
      }


      um_base_iface_ptr->iface_i_stats.pkts_tx++;
      um_base_iface_ptr->iface_i_stats.bytes_tx += total_len;

      /*---------------------------------------------------------------------
        Log packet on the Tx side
      ---------------------------------------------------------------------*/
      DPL_LOG_NETWORK_TX_PACKET(um_base_iface_ptr,
                                rx_pkt,
                                DPL_IID_NETPROT_IP);

      path_type = PS_UL_OPTIMIZED_HDLR_OPT_PATH;
      }
  } while (0);

  /*-------------------------------------------------------------------------
   Leave CS before tx_cmd to avoid possible deadlock issues.
  -------------------------------------------------------------------------*/

  PS_LEAVE_CRIT_SECTION(&global_ps_crit_section);
  PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);

  /*-------------------------------------------------------------------------
   Pass packet to optimized or legacy path based on path.
  -------------------------------------------------------------------------*/

  switch (path_type)
  {
    case PS_UL_OPTIMIZED_HDLR_OPT_PATH:
    {
      /*-----------------------------------------------------------------------
        Make sure that the power save mode is disabled when recieving the first uplink packet
        on this path
      -------------------------------------------------------------------------*/
      if( TRUE == PS_IFACE_GET_POWERSAVE_FILTERING_MODE(temp_info->rmnet_iface_ptr) )
      {
        if( PS_IFACE_GET_POWERSAVE_FILTERING_AUTO_EXIT_MODE(temp_info->rmnet_iface_ptr) )
        {
          ps_iface_set_powersave_filtering_mode( temp_info->rmnet_iface_ptr, 
                                                 enable, &ps_errno );
        }
      }

      um_base_iface_ptr->iface_private.tx_cmd
      (
        um_base_iface_ptr,
        &rx_pkt,
        tx_meta_info_ptr,
        um_base_iface_ptr->iface_private.tx_cmd_info
      );

      rx_pkt = NULL;
      break;
    }

    case PS_UL_OPTIMIZED_HDLR_OPT_QOS_PATH:
    {
      /*-----------------------------------------------------------------------
      Make sure that the power save mode is disabled when recieving the first
      uplink packet on this path
      -------------------------------------------------------------------------*/
      if( TRUE == PS_IFACE_GET_POWERSAVE_FILTERING_MODE(temp_info->rmnet_iface_ptr) )
      {
        if( PS_IFACE_GET_POWERSAVE_FILTERING_AUTO_EXIT_MODE(temp_info->rmnet_iface_ptr) )
        {
          ps_iface_set_powersave_filtering_mode( temp_info->rmnet_iface_ptr, 
                                                 enable, &ps_errno );
        }
      }

      ps_iface_tx_cmd(um_base_iface_ptr, &rx_pkt, tx_meta_info_ptr);
      rx_pkt = NULL;
      break;
    }

    case PS_UL_OPTIMIZED_HDLR_LEGACY_PATH:
    {
      ps_ul_optimized_fallback_hdlr(user_data, rx_pkt);
      rx_pkt = NULL;
      break;
    }

    case PS_UL_OPTIMIZED_HDLR_NO_PATH:
    {
      PS_TX_META_INFO_FREE(&tx_meta_info_ptr);
      break;
    }

    default:
    {
      LOG_MSG_ERROR_0("ps_ul_optimized_hdlr_path_type error");
      ASSERT(0);
      break;
    }
  }
  return rval;
} /* ps_ul_optimized_eth_no_qos_rx_sig_hdlr() */

/*===========================================================================
FUNCTION PS_UL_OPTIMIZED_ETH_QOS_RX_SIG_HDLR

DESCRIPTION
  This function is the optimized Signal handler for ETH QOS mode. It decides
  if the packet needs to take the legacy path or should be forwarded to Um
  iface.

PARAMETERS
  rx_sig    : RmNet signal type.
  user_data : pointer to user data.

RETURN VALUE
 TRUE:
 FALSE:

DEPENDENCIES
  None

SIDE EFFECTS
  None.

===========================================================================*/
boolean ps_ul_optimized_eth_qos_rx_sig_hdlr
(
  rmnet_sig_enum_type    rx_sig,
  void                 * user_data
)
{
  ps_ul_optimized_hdlr_path_type  path_type = PS_UL_OPTIMIZED_HDLR_NO_PATH;
  rmnet_smi_dual_ip_info_type  * info;
  ps_tx_meta_info_type         * tx_meta_info_ptr = NULL;
  rmnet_smi_info_type          * temp_info = NULL;
  ps_iface_type                * um_base_iface_ptr = NULL;
  ps_flow_type                 * ps_flow_ptr = NULL;
  rmnet_meta_smi_qos_hdr_type  * qos_hdr = NULL;
  dsm_item_type                * rx_pkt  = NULL;

  /*pull up size = 20*/
  int                            pullup_size =
                                 PS_OPT_PATH_ETH_HEADER_OFFSET +
                                 PS_OPT_PATH_QOS_HEADER_OFFSET ;
  uint16                         total_len = 0;
  uint16                         ip_id = 0;

  uint8    pkt_buf[PS_OPT_PATH_ETH_QOS_HEADER_LEN  +  1] ALIGN(4); //__attribute__ ((aligned(4)));
  uint8    version       = 0;
  uint8    ip4_hdr_len   = 0;
  uint8    ip_hdr_len    = 0;
  uint8    prot_offset   = 0;
  boolean  rval          = FALSE;
  boolean  enable      = FALSE;
  sint15   ps_errno;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  PS_ENTER_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);

  PS_ENTER_CRIT_SECTION(&global_ps_crit_section);

  ASSERT(user_data);

  info = (rmnet_smi_dual_ip_info_type  *) user_data;

  do
  {
    /*-----------------------------------------------------------------------
      If rx_pkt is outstanding from previous time, use it. Otherwise, dequeue
      a new packet
    -----------------------------------------------------------------------*/
    if (NULL == info->rx_pkt)
    {
      rx_pkt = dsm_dequeue(&( (rmnet_sio_info_type *)
                          (info->info_v4->meta_sm.rmnet_sio_handle))->rx_wmk );
      if (NULL == rx_pkt)
      {
        rval = TRUE;
        break;
      }
    }
    else
    {
      LOG_MSG_INFO2_0 ("ps_ul_optimized_hdlr_eth_qos_rx_sig_hdlr():"
                       "Outstanding packet");
      rx_pkt       = info->rx_pkt;
      info->rx_pkt = NULL;
    }

    if (PS_OPT_PATH_ETH_QOS_HEADER_LEN_CHECK >
        dsm_extract(rx_pkt,
                    0,
                    pkt_buf,
                    PS_OPT_PATH_ETH_QOS_HEADER_LEN)
                   )
    {
      path_type = PS_UL_OPTIMIZED_HDLR_LEGACY_PATH;
      break;
    }

    /*-------------------------------------------------------------------------
      Retrieve QoS header
    -------------------------------------------------------------------------*/
    qos_hdr = (rmnet_meta_smi_qos_hdr_type *) pkt_buf;

    /*-----------------------------------------------------------------------
      Do appropriate processing for IPv4 and IPv6 packets
    -----------------------------------------------------------------------*/

    /*IPv4*/
    if (*(uint16 *)(pkt_buf +
        PS_OPT_PATH_QOS_HEADER_OFFSET +
        PS_OPT_PATH_ETHERTYPE_OFFSET) ==
        ps_htons(LAN_ETHERTYPE_IPV4))
    {
      temp_info = info->info_v4;
      ip4_hdr_len = (* (pkt_buf + PS_OPT_PATH_QOS_HEADER_OFFSET +
                        PS_OPT_PATH_ETH_HEADER_OFFSET) &
                        PS_OPT_PATH_IP_HDR_LEN_MASK)   *
                        PS_OPT_PATH_WORD_SIZE;

      /*---------------------------------------------------------------------
        Update ip_hdr_len and protocol offset for TCP ACK prioritization
      ---------------------------------------------------------------------*/
      ip_hdr_len  = ip4_hdr_len;
      prot_offset = PS_OPT_PATH_V4_PROT_FIELD_OFFSET;

      /*---------------------------------------------------------------------
        Ensure that IPv4 call is UP
      ---------------------------------------------------------------------*/
      if (RMNET_META_SM_NULL_STATE != temp_info->meta_sm.state &&
          PS_IFACE_IS_VALID(temp_info->um_iface_ptr))
      {
        /*-------------------------------------------------------------------
          Flow control mechanism
          1.Single IPv4 call - Flow control if v4 Um is flow ctrlled.
          2.Dual-IP call(Single APN/Dual APN) - Flow ctrl only when v4 and
                                                v6 is flow ctrlled.
        -------------------------------------------------------------------*/

        /*-------------------------------------------------------------------
          Check if v4 Um is flow ctrlled
        -------------------------------------------------------------------*/
        if (!(PS_IFACEI_FLOW_ENABLED(temp_info->um_iface_ptr)) )
        {

          /*-----------------------------------------------------------------
            Check if IPv6 call up
          -----------------------------------------------------------------*/
          if (RMNET_META_SM_NULL_STATE != info->info_v6->meta_sm.state &&
              PS_IFACE_IS_VALID(info->info_v6->um_iface_ptr))
          {
            /*---------------------------------------------------------------
              Its a Dual IP call. Check flow ctrl on V6
            ---------------------------------------------------------------*/
            if (!(PS_IFACEI_FLOW_ENABLED(info->info_v6->um_iface_ptr)))
            {
              /*-------------------------------------------------------------
                Since Flow is disabled on both V4 and V6 APNs we can return
                TRUE and stop de-queuing pkts.
              -------------------------------------------------------------*/
              LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_eth_qos_rx_sig_hdlr():"
                               "Flow disabled on both V4V6 in a Dual IP call");
              /*Store Outstanding pkt in rmnet_dual_ip instance*/
              info->rx_pkt = rx_pkt;
              rval = TRUE;
              break;
            }

            /*---------------------------------------------------------------
              Flow is enabled on V6 APN and disabled on V4 in Dual-IP call.
              So we need to continue de-queueing pkts so that V6 data flow is
              On.Push this V4 pkt to Um though flow controlled and let Um Wmk
              handle it .
            ---------------------------------------------------------------*/
            LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_eth_qos_rx_sig_hdlr():"
                             "V4:Flow disabled , V6: Enabled in Dual IP call"
                             "Cont to dequeue..");

          }
          else/*V6 call up*/
          {
            LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_eth_qos_rx_sig_hdlr():"
                             "Flow disabled on Single V4 call");
            /*Store Outstanding pkt in rmnet_dual_ip instance*/
            info->rx_pkt = rx_pkt;
            rval = TRUE;
            break;
          }
        }/*V4 flow controlled*/

        /*-------------------------------------------------------------------
          Send packets up the stack for following cases
          1. If packet is destined to a broadcast address
          2. if packet is destined to a limited broadcast address
          3. If packet is destined to a multicast address
          4. If packet is fragmented
          5. if packet is destined to Rm iface
          6. if IP header has options
        -------------------------------------------------------------------*/
        if (ps_ul_optmized_hdlr_ipv4_is_legacy
          (pkt_buf + PS_OPT_PATH_QOS_HEADER_OFFSET +
           PS_OPT_PATH_ETH_HEADER_OFFSET,
           temp_info,
           ip4_hdr_len)  == TRUE
         )
        {
          /*Check if Legacy Wmk is flow controlled*/
          if(!(ps_ul_optimized_hdlr_legacy_flow_enabled(info)))
          {
            LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_eth_qos_rx_sig_hdlr():"
                             "Legacy Wmk Flow disabled");
            info->rx_pkt = rx_pkt;
            rval = TRUE; /*Outstanding packet*/
          }
          else
          {
            path_type = PS_UL_OPTIMIZED_HDLR_LEGACY_PATH;
          }
          break;
        }
      }/*V4 SM up*/
      else
      {
        LOG_MSG_ERROR_0("V4 SM not up. Dropping packet");
        dsm_free_packet(&rx_pkt);
        break;
      }

      ip_id     = (uint16)(* (uint16 *) (pkt_buf +
                             PS_OPT_PATH_QOS_HEADER_OFFSET +
                             PS_OPT_PATH_ETH_HEADER_OFFSET +
                             PS_OPT_PATH_V4_ID_OFFSET));
      version   = IP_V4;
      total_len = ps_htons((uint16) * ( (uint16 *)(pkt_buf +
                             PS_OPT_PATH_QOS_HEADER_OFFSET +
                             PS_OPT_PATH_ETH_HEADER_OFFSET +
                             PS_OPT_PATH_V4_TOTAL_LEN_OFFSET)));
      /*---------------------------------------------------------------------
       LOW LATENCY TRAFFIC handling
      ---------------------------------------------------------------------*/
      ps_ul_optimized_v4_low_latency_traffic_handler ( pkt_buf,
                                                 ip_hdr_len +
                                                 PS_OPT_PATH_QOS_HEADER_OFFSET +
                                                 PS_OPT_PATH_ETH_HEADER_OFFSET,
                                                 prot_offset + 
                                                 PS_OPT_PATH_QOS_HEADER_OFFSET +
                                                 PS_OPT_PATH_ETH_HEADER_OFFSET);
    }

    /*IPv6*/
    else if (*(uint16 *)(pkt_buf +
              PS_OPT_PATH_QOS_HEADER_OFFSET +
              PS_OPT_PATH_ETHERTYPE_OFFSET) ==
              ps_htons(LAN_ETHERTYPE_IPV6))
    {
      temp_info = info->info_v6;

      /*---------------------------------------------------------------------
        Update ip_hdr_len and protocol offset for TCP ACK prioritization
      ---------------------------------------------------------------------*/
       ip_hdr_len  = PS_OPT_PATH_V6_HDR_LEN;
       prot_offset = PS_OPT_PATH_V6_NXT_HDR_OFFSET;

      /*---------------------------------------------------------------------
        Ensure that IPv6 call is UP
      ---------------------------------------------------------------------*/
      if (RMNET_V6_SM_NULL_STATE != temp_info->v6_sm.state &&
          PS_IFACE_IS_VALID(temp_info->um_iface_ptr))
      {
        /*-------------------------------------------------------------------
          Flow control mechanism
          1.Single IPv6call - Flow control if v6 Um is flow ctrlled.
          2.Dual-IP call(Single APN/Dual APN) - Flow ctrl only when v4 and
                                                v6 is flow ctrlled.
        -------------------------------------------------------------------*/

        /*-------------------------------------------------------------------
          Check Flow control on v6 Um
        -------------------------------------------------------------------*/
        if (!(PS_IFACEI_FLOW_ENABLED(temp_info->um_iface_ptr)) )
        {

          /*-----------------------------------------------------------------
            Check if IPv4 call up
          -----------------------------------------------------------------*/
          if (RMNET_META_SM_NULL_STATE != info->info_v4->meta_sm.state &&
              PS_IFACE_IS_VALID(info->info_v4->um_iface_ptr))
          {

            /*---------------------------------------------------------------
              Its a Dual IP call. Check flow ctrl on V4
            ---------------------------------------------------------------*/
            if (!(PS_IFACEI_FLOW_ENABLED(info->info_v4->um_iface_ptr)))
            {
              /*-------------------------------------------------------------
                Since Flow is disabled on both V4 and V6 APNs we can return
                TRUE and stop de-queuing pkts.
              -------------------------------------------------------------*/
              LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_eth_qos_rx_sig_hdlr():"
                               "Flow disabled on both V4V6 Dual IP call");
              /*Store Outstanding pkt in rmnet_dual_ip instance*/
              info->rx_pkt = rx_pkt;
              rval = TRUE;
              break;
            }

            /*---------------------------------------------------------------
              Flow is enabled on V4 APN and disabled on V6 in Dual-IP call.
              So we need to continue de-queueing pkts so that V4 data flow is
              On.Push this V4 pkt to Um though flow controlled and let Um Wmk
              handle it .
            ---------------------------------------------------------------*/
            LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_eth_qos_rx_sig_hdlr():"
                             "V6:Flow disabled , V4: Enabled in Dual IP call"
                             "Cont to dequeue..");

          }/*V4 call up*/
          else
          {
            LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_eth_qos_rx_sig_hdlr():"
                             "Flow disabled on Single V6 call");

            /*Store Outstanding pkt in rmnet_dual_ip instance*/
            info->rx_pkt = rx_pkt;
            rval = TRUE;
            break;
          }
        }/*V6 flow controlled*/

        /*-------------------------------------------------------------------
          Send packets up the stack for following cases
          1. If packet is destined to a linklocal address
          2. If packet is destined to a multicast address
        -------------------------------------------------------------------*/
        if(ps_ul_optimized_ipv6_is_legacy
         ( pkt_buf + PS_OPT_PATH_QOS_HEADER_OFFSET +
           PS_OPT_PATH_ETH_HEADER_OFFSET
         ) == TRUE
        )
        {
          /*Check if Legacy Wmk is flow controlled*/
          if(!(ps_ul_optimized_hdlr_legacy_flow_enabled(info)))
          {
            LOG_MSG_ERROR_0 ("ps_ul_optimized_hdlr_eth_qos_rx_sig_hdlr():"
                             "Legacy Wmk Flow disabled");
            info->rx_pkt = rx_pkt;
            rval = TRUE; /*Outstanding packet*/
          }
          else
          {
            path_type = PS_UL_OPTIMIZED_HDLR_LEGACY_PATH;
          }
          break;
        }
      }/*V6 SM up*/
      else
      {
        LOG_MSG_ERROR_0("V6 SM not up. Dropping packet");
        dsm_free_packet(&rx_pkt);
        break;
      }

      /*Drop packet if prefix does not match*/
      if(ps_ul_optimized_ipv6_prefix_cmp(pkt_buf  +
                     PS_OPT_PATH_QOS_HEADER_OFFSET +
                     PS_OPT_PATH_ETH_HEADER_OFFSET, temp_info) == FALSE)
      {
        dsm_free_packet(&rx_pkt);
        break;
      }

      total_len  = ps_htons((uint16)*((uint16 *)(pkt_buf  +
                     PS_OPT_PATH_QOS_HEADER_OFFSET +
                     PS_OPT_PATH_ETH_HEADER_OFFSET +
                     PS_OPT_PATH_V6_TOTAL_LEN_OFFSET)));
      total_len += PS_OPT_PATH_V6_HDR_LEN;

    }
    else
    {
      path_type = PS_UL_OPTIMIZED_HDLR_LEGACY_PATH;
      break;
    }

    /*-----------------------------------------------------------------------
      Pull out QoS hdr and Ethernet header
    -----------------------------------------------------------------------*/
    (void) qmi_svc_dsm_pullup(&rx_pkt, NULL, pullup_size,FILE_ID_PS_UL_OPTIMIZED_HDLR,__LINE__);

    /*---------------------------------------------------------------------
        TCP ACK prioritization
    ---------------------------------------------------------------------*/
    if (ps_ul_optimized_tcp_ack_pri_check
          (
           pkt_buf + PS_OPT_PATH_QOS_HEADER_OFFSET
                   + PS_OPT_PATH_ETH_HEADER_OFFSET,
           ip_hdr_len,
           total_len,
           prot_offset
          ) == TRUE
       )
    {
      LOG_MSG_INFO3_0("ETH_QOS: TCP_ACK_PRIORITY: Tagging DSM_HIGHEST");
      (rx_pkt)->priority = DSM_HIGHEST;
    }

    /*-----------------------------------------------------------------------
      Increment the receive packet stats
    -----------------------------------------------------------------------*/
    temp_info->rmnet_iface_ptr->iface_i_stats.pkts_rx++;
    temp_info->rmnet_iface_ptr->iface_i_stats.bytes_rx += total_len;

    /*-----------------------------------------------------------------------
      Log the packet on the Rx side
    -----------------------------------------------------------------------*/
    DPL_LOG_NETWORK_RX_PACKET(temp_info->rmnet_iface_ptr,
                              rx_pkt,
                              DPL_IID_NETPROT_IP);

    um_base_iface_ptr = PS_IFACE_GET_BASE_IFACE(temp_info->um_iface_ptr);

    /*-----------------------------------------------------------------------
      Process QoS header
    -----------------------------------------------------------------------*/
    ps_ul_optimized_process_qos_hdr(rx_pkt,
                                    &tx_meta_info_ptr,
                                    qos_hdr,
                                    um_base_iface_ptr,
                                    version,
                                    ip_id);

    /*---------------------------------------------------------------------
      Get Flow from Meta Info. If flow capability is set to
      PS_FLOW_CAPABILITY_DATA_DISALLOWED drop the packet
    ---------------------------------------------------------------------*/
    if ((tx_meta_info_ptr != NULL) &&
        (PS_TX_META_GET_RT_META_INFO_PTR(tx_meta_info_ptr) != NULL) &&
        (PS_TX_META_GET_PKT_META_INFO_PTR(tx_meta_info_ptr) != NULL)
       )
    {
      ps_flow_ptr = PS_FLOW_GET_FLOW_FROM_META_INFO(tx_meta_info_ptr);

      if (ps_flow_ptr == NULL)
      {
        ps_flow_ptr = PS_IFACEI_GET_DEFAULT_FLOW(temp_info->um_iface_ptr);
      }
    }

    if ((PS_FLOW_GET_CAPABILITY(ps_flow_ptr,
              PS_FLOW_CAPABILITY_DATA_DISALLOWED)))
    {
      LOG_MSG_ERROR_0("PS_FLOW_CAPABILITY_DATA_DISALLOWED:Dropping packet" );
      dsm_free_packet(&rx_pkt);
      rx_pkt = NULL;
      break;
    }

    um_base_iface_ptr->iface_i_stats.pkts_tx++;
    um_base_iface_ptr->iface_i_stats.bytes_tx += total_len;

    /*-----------------------------------------------------------------------
      Log packet on the Tx side
    -----------------------------------------------------------------------*/
    DPL_LOG_NETWORK_TX_PACKET(um_base_iface_ptr,
                              rx_pkt,
                              DPL_IID_NETPROT_IP);

    path_type = PS_UL_OPTIMIZED_HDLR_OPT_PATH;

  } while (0);

    /*-----------------------------------------------------------------------
   Leave CS before tx_cmd to avoid possible deadlock issues.
    -----------------------------------------------------------------------*/
  PS_LEAVE_CRIT_SECTION(&global_ps_crit_section);
  PS_LEAVE_CRIT_SECTION(&rmnet_rx_pkt_hdlr_crit_section);

  /*-------------------------------------------------------------------------
   Pass packet to optimized or legacy path based on path.
  -------------------------------------------------------------------------*/
  switch (path_type)
  {
    case PS_UL_OPTIMIZED_HDLR_OPT_PATH:
    {
      /*-----------------------------------------------------------------------
        Make sure that the power save mode is disabled when recieving the first
        uplink packet on this path
      -------------------------------------------------------------------------*/
      if( TRUE == PS_IFACE_GET_POWERSAVE_FILTERING_MODE(temp_info->rmnet_iface_ptr) )
      {
        if( PS_IFACE_GET_POWERSAVE_FILTERING_AUTO_EXIT_MODE(temp_info->rmnet_iface_ptr) )
        {
          ps_iface_set_powersave_filtering_mode( temp_info->rmnet_iface_ptr, 
                                                 enable, &ps_errno );
        }
      }

    um_base_iface_ptr->iface_private.tx_cmd
    (
      um_base_iface_ptr,
      &rx_pkt,
      tx_meta_info_ptr,
      um_base_iface_ptr->iface_private.tx_cmd_info
    );

    rx_pkt           = NULL;
      break;
    }

    case PS_UL_OPTIMIZED_HDLR_LEGACY_PATH:
    {
      ps_ul_optimized_fallback_hdlr(user_data, rx_pkt);
      rx_pkt = NULL;
      break;
    }

    case PS_UL_OPTIMIZED_HDLR_NO_PATH:
    {
  PS_TX_META_INFO_FREE(&tx_meta_info_ptr);
      break;
    }

    default:
  {
      LOG_MSG_ERROR_0("ps_ul_optimized_hdlr_path_type error");
      ASSERT(0);
      break;
    }
  }
  return rval;

} /* ps_ul_optimized_eth_qos_rx_sig_hdlr() */
