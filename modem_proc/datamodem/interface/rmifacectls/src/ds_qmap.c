/*===========================================================================

                         D S _ Q M A P . C

DESCRIPTION

  QMAP (Qualcomm Mux and Aggregation) source file.

EXTERNALIZED FUNCTIONS

  ds_qmap_send_cmd()
    Send a QMAP command

Copyright (c) 2005-2013 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/rmifacectls/src/ds_qmap.c#1 $
  $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when        who    what, where, why
--------    ---    ----------------------------------------------------------
05/24/13    wc     Initial file
===========================================================================*/

/*===========================================================================

                                INCLUDE FILES

===========================================================================*/
#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"

#include "dsm_item.h"
#include "ps_in.h"
#include "ds_Utils_DebugMsg.h"
#include "ds_qmap.h"
#include "queue.h"
#include "dcc_task_defs.h"
#include "dcc_task_svc.h"
#include "ps_system_heap.h"
#include "ds_qmi_task.h"
#include "ds_qmi_svc.h"
#include "ds_qmi_nv_util.h"
#include "stringl/stringl.h"

#ifdef FEATURE_DATA_A2
#include "a2_dl_per.h"
#include "a2_ul_per.h"
#endif

/*===========================================================================

                                DATA TYPES

===========================================================================*/

/*===========================================================================

                                 DEFINITIONS

===========================================================================*/

#define DS_QMAP_CMD_RESPONSE_TIME     (20)   // Response timer in ms
#define DS_QMAP_CMD_MAX_TIMEOUT       (5000) // Max time out in ms
#define DS_QMAP_CMD_MAX_RETRY         (3)   // Max retry

static q_type  qmap_cmd_pend_q;
static uint32  qmap_cmd_id;         
static uint32  qmap_cmd_resp_time;

typedef PACKED struct PACKED_POST
{
  uint8                  reserved;
  uint8                  ip_family;
  uint16                 seq_num;
  uint32                 qos_id;
} ds_qmap_payload_type;

/*---------------------------------------------------------------------------
  Data type used to qmap cmd resp functions
---------------------------------------------------------------------------*/
typedef struct
{
  sio_port_id_type                port_id;
  uint8                           mux_id;
  uint8                           cmd_name;
  uint8                           cmd_type;
  ds_qmap_payload_type            payload;
  uint32                          rtt_time;
} ds_qmap_cmd_resp_type;

/*---------------------------------------------------------------------------
  QMAP command buffer type
---------------------------------------------------------------------------*/
typedef struct
{
  uint16       cmd_id;

  union
  {
    struct
    {
      sio_port_id_type      port_id;
      uint8                 mux_id;
      uint8                 cmd_name;
      uint8                 cmd_type;
      dsm_item_type       * dsm_ptr;
      uint32                rtt_time;
    } qmap_resp;

    struct
    {
      uint32    search_id;
    } timer_info;

  } qmap;

} ds_qmap_cmd_buf_type;

/*===========================================================================

                           FORWARD DECLARATIONS

===========================================================================*/

static void ds_qmap_build_fc_cmd
(
  ds_qmap_cmd_flow_ctrl_type   * qmap_fc_cmd,
  uint8                          cmd,
  uint8                          ip_family,
  uint16                         seq_num,
  uint32                         qos_id
);

static void ds_qmap_process_cmd
(
  void * cmd_ptr
);

static void * ds_qmap_get_cmd_buf
(
  qmi_cmd_id_e_type cmd_id
);

#define ds_qmap_free_cmd_buf(buf_ptr) PS_SYSTEM_HEAP_MEM_FREE(buf_ptr)

#if defined(FEATURE_DATA_A2) && !defined(FEATURE_DATA_MPSS_ULT)
static void ds_qmap_flow_cmd_resp_cb
(
  sio_port_id_type                port_id,
  uint8                           mux_id,
  uint8                           cmd_name,   
  a2_common_qmap_ctrl_cmd_type_e  cmd_type,
  dsm_item_type*                  dsm_ptr,
  uint32                          rtt_time
);
#endif /* FEATURE_DATA_A2 */

static void ds_qmap_cmd_timer_cback
(
  timer_cb_data_type  user_data
);

static boolean ds_qmap_is_qmap_cmd_expired
(
  ds_qmap_cmd_info_type  * cmd_info_ptr,      // searched value
  ds_qmap_cmd_info_type  * comp_param_ptr     // comparison value
);

static void ds_qmap_process_qmap_resp
(
  sio_port_id_type                port_id,
  uint8                           mux_id,
  uint8                           cmd_name,   
  uint8                           cmd_type,
  dsm_item_type                 * dsm_ptr,
  uint32                          rtt_time
);

static void ds_qmap_process_timer_exp
(
  uint32 search_id
);

/*===========================================================================

                        EXTERNAL FUNCTION DEFINITIONS

===========================================================================*/

/*===========================================================================
  FUNCTION DS_QMAP_INIT()

  DESCRIPTION
    This function initializes the QMAP related functions.
                                                                                        
  PARAMETERS
    None

  RETURN VALUE
    None

  DEPENDENCIES
    Should be called once at powerup

  SIDE EFFECTS
    None
===========================================================================*/
void ds_qmap_init
(
  void
)
{
   // Initialize QMAP command response pending queue
  memset(&qmap_cmd_pend_q, 0, sizeof(qmap_cmd_pend_q));
  (void) q_init( &qmap_cmd_pend_q );

  qmi_task_set_qmap_cmd_handler(ds_qmap_process_cmd);

  if (qmi_nv_read(QMI_NV_ITEM_QMAP_CMD_RESP_TIME, 0,
                  &qmap_cmd_resp_time,
                  sizeof(qmap_cmd_resp_time)) != QMI_NV_STATUS_OK)
  {
    qmap_cmd_resp_time = DS_QMAP_CMD_RESPONSE_TIME;
  }

  LOG_MSG_INFO1_1("QMAP cmd response timer %u ms", qmap_cmd_resp_time);

  // Register a call back func when a response for a cmd is received
  if (qmap_cmd_resp_time > 0)
  {
#if defined(FEATURE_DATA_A2) && !defined(FEATURE_DATA_MPSS_ULT)
    a2_ul_per_register_qmap_rsp_cb(ds_qmap_flow_cmd_resp_cb);
#endif
  }
}

/*===========================================================================
  FUNCTION DS_QMAP_SEND_CMD()

  DESCRIPTION
    This function sends a QMAP command.

  PARAMETERS
    port_id:               SIO port id
    mux_id:                QMAP mux_id
    cmd_size:              QMAP command size
    cmd_ptr:               QMAP command pointer
    cmd_info_ptr:          Ptr to the retransmitted pkt in queue/
                           NULL if not a retransmitted pkt
    

  RETURN VALUE
    TRUE/FALSE 

  DEPENDENCIES
    SIO port must be opened and QMAP data format is enabled.

  SIDE EFFECTS
    None
===========================================================================*/
boolean ds_qmap_send_cmd
(
  sio_port_id_type         port_id,
  uint8                    mux_id,
  uint16                   cmd_size,
  void                   * cmd_ptr,
  ds_qmap_cmd_info_type  * cmd_info_ptr
)
{
  uint8                     cmd;
  uint8                     cmd_type;
  dsm_item_type           * pkt = NULL;
  boolean                   status = FALSE;
  ds_qmap_cmd_info_type  *  qmap_cmd_info = NULL;
  int                       q_count = 0;
  ds_qmap_cmd_info_type  *  item_ptr = NULL;
  ds_qmap_cmd_flow_ctrl_type  * qmap_fc = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (cmd_ptr == NULL || cmd_size < DS_QMAP_CMD_PAYLOAD_OFFSET)
  {
    LOG_MSG_ERROR_1("Invalid QMAP cmd size %d", cmd_size);
    return FALSE;
  }

  cmd = *((uint8 *)cmd_ptr + DS_QMAP_CMD_OFFSET);
  cmd_type = (*((uint8 *)cmd_ptr + DS_QMAP_CMD_TYPE_OFFSET)) & 0x03;

  pkt = dsm_offset_new_buffer(
              DSM_DS_SMALL_ITEM_POOL, DS_QMAP_CMD_PAYLOAD_OFFSET);
  if (pkt == NULL)
  {
    LOG_MSG_ERROR_0("Unable to get a new DSM item");
    return FALSE;
  }

  if (dsm_pushdown_packed(
               &pkt,
               (uint8 *)cmd_ptr + DS_QMAP_CMD_PAYLOAD_OFFSET,
               cmd_size - DS_QMAP_CMD_PAYLOAD_OFFSET,
               DSM_DS_SMALL_ITEM_POOL) != cmd_size - DS_QMAP_CMD_PAYLOAD_OFFSET)
  {
    LOG_MSG_ERROR_2("Failed to build DSM pkt for QMAP cmd %d on SIO port 0x%x",
                    cmd, port_id);
    return FALSE;
  }

#if defined(FEATURE_DATA_A2) && !defined(FEATURE_DATA_MPSS_ULT)
  if (a2_dl_per_send_qmap_ctrl_cmd(
        port_id,
        mux_id,
        cmd,
        (a2_common_qmap_ctrl_cmd_type_e)cmd_type,
        pkt,
        cmd_info_ptr ? TRUE: FALSE) == A2_SUCCESS)
  {
    status = TRUE;
  }
#endif /* FEATURE_DATA_A2 */

  if (status != TRUE)
  {
    LOG_MSG_ERROR_3("Unable to send QMAP command [%d:%d] to SIO port 0x%x",
                    cmd, cmd_type, port_id);
    dsm_free_packet(&pkt);
    return status;
  }

  if (DS_QMAP_IS_FC_CMD(cmd))
  {
    qmap_fc = cmd_ptr; 
    LOG_MSG_INFO1_3("Sent QMAP cmd [%d] seq_num [%d] qos_id [0x%x]",
                    cmd,
                    ps_ntohs(qmap_fc->seq_num),
                    ps_ntohl(qmap_fc->qos_id));
  }
  else
  {
    LOG_MSG_INFO1_3("Sent QMAP command [%d:%d] to SIO port 0x%x",
                    cmd, cmd_type, port_id);
  }

  // If response timer is configured to be 0, no response is expected
  if (qmap_cmd_resp_time == 0)
  {
    return status;
  }

  // Check if qmap command is Flow Enable or Flow Disable
  if (!DS_QMAP_IS_FC_CMD(cmd))
  {
    return status;
  }

  if (!cmd_info_ptr)
  {
    qmap_cmd_id++;

    // Copy into qmap flow cmd info structure
    qmap_cmd_info = (void *)
           ps_system_heap_mem_alloc(sizeof(ds_qmap_cmd_info_type));
    
    if (qmap_cmd_info == NULL)
    {
      LOG_MSG_ERROR_0("Unable to alloc memory for qmap flow info !");
      return status;
    }

    memset(qmap_cmd_info, 0, sizeof(ds_qmap_cmd_info_type));
    qmap_cmd_info->port_id = port_id;
    qmap_cmd_info->mux_id  = mux_id;
    qmap_cmd_info->cmd_size = cmd_size;
    qmap_cmd_info->qmap_cmd_id = qmap_cmd_id;
    qmap_cmd_info->timeout = qmap_cmd_resp_time;
    qmap_cmd_info->cmd_ptr = (void *)ps_system_heap_mem_alloc(cmd_size);

    if (qmap_cmd_info->cmd_ptr != NULL)
    {
      memscpy(qmap_cmd_info->cmd_ptr, cmd_size, cmd_ptr, cmd_size);
    }
    else
    {
      LOG_MSG_ERROR_0("Unable to alloc memory for qmap flow cmd ptr !");
      PS_SYSTEM_HEAP_MEM_FREE(qmap_cmd_info);
      return status;
    }

    // Initialize timer
    timer_def( &(qmap_cmd_info->qmap_cmd_timer),
           NULL,
           NULL,
           0,
           ds_qmap_cmd_timer_cback,
           (timer_cb_data_type)qmap_cmd_id );
  }
  else
  {
    // Just queue it back; Previous retx, double timeout.
    qmap_cmd_info = cmd_info_ptr;
    qmap_cmd_info->timeout <<= 1;
  }
    
  // Clean pending requests
  q_count = q_cnt(&qmap_cmd_pend_q);
  while (q_count--)
  {
    item_ptr = (ds_qmap_cmd_info_type *) q_get(&qmap_cmd_pend_q);

    if (item_ptr == NULL)
    {
      LOG_MSG_ERROR_0("Invalid queue entry ! Skipping");
    }
    else if ( !ds_qmap_is_qmap_cmd_expired(qmap_cmd_info, item_ptr) )
    {
      // Put back into the queue
      q_link(item_ptr, &(item_ptr->link));
      q_put(&qmap_cmd_pend_q, &(item_ptr->link));
    }
    else
    {
      // Old pending cmd; Free the memory
      (void) timer_clr( &(item_ptr->qmap_cmd_timer), T_NONE );
      PS_SYSTEM_HEAP_MEM_FREE(item_ptr->cmd_ptr);
      PS_SYSTEM_HEAP_MEM_FREE(item_ptr);
    }
  }
   
  // Add the new cmd to pending queue
  q_link(qmap_cmd_info, &(qmap_cmd_info->link));
  q_put(&qmap_cmd_pend_q, &(qmap_cmd_info->link));   

  // Start timer
  if (qmap_cmd_info->timeout > DS_QMAP_CMD_MAX_TIMEOUT)
  {
    qmap_cmd_info->timeout = DS_QMAP_CMD_MAX_TIMEOUT;
  }
  (void) timer_set( &(qmap_cmd_info->qmap_cmd_timer),
                         qmap_cmd_info->timeout, 0, T_MSEC );

  return status;

} /* ds_qmap_send_cmd() */
 
/*===========================================================================
  FUNCTION DS_QMAP_BUILD_FLOW_ENABLE_CMD()

  DESCRIPTION
    This function builds a QMAP flow enable command.

  PARAMETERS
    qmap_fc_cmd:   QMAP command ptr
    ip_family:     IP family
    seq_num:       sequence number
    qos_id:        qos_id 

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void ds_qmap_build_flow_enable_cmd
(
  ds_qmap_cmd_flow_ctrl_type   * qmap_fc_cmd,
  uint8                          ip_family,
  uint16                         seq_num,
  uint32                         qos_id
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (qmap_fc_cmd == NULL)
  {
    return;
  }

  ds_qmap_build_fc_cmd(
                   qmap_fc_cmd,
                   DS_QMAP_CMD_FLOW_ENABLE,
                   ip_family,
                   seq_num,
                   qos_id);

} /* ds_qmap_build_flow_enable_cmd() */

/*===========================================================================
  FUNCTION DS_QMAP_BUILD_FLOW_DISABLE_CMD()

  DESCRIPTION
    This function builds a QMAP flow disable command.

  PARAMETERS
    qmap_fc_cmd:   QMAP command ptr
    ip_family:     IP family
    seq_num:       sequence number
    qos_id:        qos_id 

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void ds_qmap_build_flow_disable_cmd
(
  ds_qmap_cmd_flow_ctrl_type   * qmap_fc_cmd,
  uint8                          ip_family,
  uint16                         seq_num,
  uint32                         qos_id
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (qmap_fc_cmd == NULL)
  {
    return;
  }

  ds_qmap_build_fc_cmd(
                   qmap_fc_cmd,
                   DS_QMAP_CMD_FLOW_DISABLE,
                   ip_family,
                   seq_num,
                   qos_id);

} /* ds_qmap_build_flow_disable_cmd() */

/*===========================================================================
FUNCTION DS_QMAP_CLEAN_CMD

DESCRIPTION
  Clean all the pending commands for a specific call

PARAMETERS
  port_id: SIO port id

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
void ds_qmap_clean_cmd
(
  sio_port_id_type   port_id
)
{
  ds_qmap_cmd_info_type  * item_ptr;
  uint8                  * cmd_ptr;
  int                      q_count;
  uint8                    cmd;
  uint8                    ip;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  // Clean all requests for the call
  q_count = q_cnt(&qmap_cmd_pend_q);
  while (q_count--)
  {
    item_ptr = (ds_qmap_cmd_info_type *) q_get(&qmap_cmd_pend_q);
    if (item_ptr != NULL)
    {
      if (item_ptr->port_id != port_id)
      {
        // Put back into the queue
        q_link(item_ptr, &(item_ptr->link));
        q_put(&qmap_cmd_pend_q, &(item_ptr->link));
      }
      else
      {
        cmd_ptr = item_ptr->cmd_ptr;
        cmd = *(cmd_ptr + DS_QMAP_CMD_OFFSET);
        ip = *(cmd_ptr + DS_QMAP_CMD_PAYLOAD_OFFSET + 1);

        // Old pending cmd; Free the memory
        (void) timer_clr( &(item_ptr->qmap_cmd_timer), T_NONE );
        PS_SYSTEM_HEAP_MEM_FREE(item_ptr->cmd_ptr);
        PS_SYSTEM_HEAP_MEM_FREE(item_ptr);
        LOG_MSG_INFO2_2("QMAP FC cmd [%d] IP [%d] removed", cmd, ip);
      }
    }
  }

} /* ds_qmap_clean_cmd() */

/*===========================================================================

                        INTERNAL FUNCTION DEFINITIONS

===========================================================================*/


/*===========================================================================
  FUNCTION DS_QMAP_BUILD_FC_CMD()

  DESCRIPTION
    This function builds a QMAP flow control command (without cmd type)

  PARAMETERS
    qmap_fc_cmd:   QMAP command ptr
    ip_family:     IP family
    seq_num:       sequence number
    qos_id:        qos_id 

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void ds_qmap_build_fc_cmd
(
  ds_qmap_cmd_flow_ctrl_type   * qmap_fc_cmd,
  uint8                          cmd,
  uint8                          ip_family,
  uint16                         seq_num,
  uint32                         qos_id
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (qmap_fc_cmd == NULL)
  {
    ASSERT(0);
    return;
  }

  memset(qmap_fc_cmd, 0, sizeof(ds_qmap_cmd_flow_ctrl_type));

  qmap_fc_cmd->cmd_hdr.cmd      = cmd;
  qmap_fc_cmd->cmd_hdr.cmd_type = DS_QMAP_CMD_TYPE_REQUEST;

  if (ip_family == IPV4_ADDR) 
  {
    qmap_fc_cmd->ip_family  = DS_QMAP_ADDR_FAMILY_V4;
  }
  else if (ip_family == IPV6_ADDR)
  {
    qmap_fc_cmd->ip_family  = DS_QMAP_ADDR_FAMILY_V6;
  }
  else
  {
    qmap_fc_cmd->ip_family  = DS_QMAP_ADDR_FAMILY_ANY ;
  }

  qmap_fc_cmd->seq_num      = ps_htons(seq_num);
  qmap_fc_cmd->qos_id       = ps_htonl(qos_id);

} /* ds_qmap_build_fc_cmd() */

/*===========================================================================
  FUNCTION DS_QMAP_PROCESS_CMD

  DESCRIPTION
    This function processes a QMAP command or event.

  PARAMETERS
    cmd_ptr:  private data buffer containing the QMAP command
              information.

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void ds_qmap_process_cmd
(
  void * cmd_ptr
)
{
  ds_qmap_cmd_buf_type * cmd_buf_ptr = NULL;
/*-------------------------------------------------------------------------*/

  if (cmd_ptr == NULL)
  {
    LOG_MSG_ERROR_0("NULL QMAP cmd_ptr!");
    ASSERT(0);
    return;
  }

  cmd_buf_ptr = (ds_qmap_cmd_buf_type *)cmd_ptr;

  switch (cmd_buf_ptr->cmd_id)
  {
    case QMI_CMD_QMAP_CMD_RESP_CB:
      ds_qmap_process_qmap_resp (cmd_buf_ptr->qmap.qmap_resp.port_id,
                                 cmd_buf_ptr->qmap.qmap_resp.mux_id,
                                 cmd_buf_ptr->qmap.qmap_resp.cmd_name,
                                 cmd_buf_ptr->qmap.qmap_resp.cmd_type,
                                 cmd_buf_ptr->qmap.qmap_resp.dsm_ptr,
                                 cmd_buf_ptr->qmap.qmap_resp.rtt_time);
      break;

    case QMI_CMD_QMAP_CMD_TIMER_EXP_CB:
      ds_qmap_process_timer_exp (
                  cmd_buf_ptr->qmap.timer_info.search_id);
      break;

    default:
      LOG_MSG_ERROR_1("Ignoring QMAP cmd %d", cmd_buf_ptr->cmd_id);
      break;
  }

  ds_qmap_free_cmd_buf(cmd_ptr);
  return;

} /* qmi_qmux_process_cmd() */

/*===========================================================================
  FUNCTION DS_QMAP_GET_CMD_BUF()

  DESCRIPTION
    Allocate and assign a QMAP command buffer from the PS MEM heap

  PARAMETERS
    cmd_id - QMI command type

  RETURN VALUE
    cmd_buf_ptr - Pointer to the allocated command buffer

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void * ds_qmap_get_cmd_buf
(
  qmi_cmd_id_e_type cmd_id
)
{
  void * cmd_buf_ptr;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  cmd_buf_ptr = ps_system_heap_mem_alloc(
                           sizeof(ds_qmap_cmd_buf_type));

  return cmd_buf_ptr;

} /* ds_qmap_get_cmd_buf() */

#if defined(FEATURE_DATA_A2) && !defined(FEATURE_DATA_MPSS_ULT)
/*===========================================================================
FUNCTION   DS_QMAP_FLOW_CMD_RESP_CB()

DESCRIPTION
  This callback function is called by A2 when a response for a
   QMAP command is received on the UL


PARAMETERS:
    port_id   :  Port ID on which response was received
    mux_id    :  Corresponding mux id for the port of interest
    cmd_name  :  Command name as indicated in the QMAP control command spec
                 1 = Command to enable flow control, i.e. flow disable
                 2 = Command to disable flow control, i.e. flow enable

    cmd_type  :  Command type as indicated in the QMAP control command spec

    dsm_ptr   :  Pointer to the DSM item which contains the command payload. 
                 The QMAP control header is stripped out of the dsm item and passed
                 to DS/client. DS is expected to free this item

    rtt_time  :  Time difference (in us) between the time command was sent and the time
                 response was received.


DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
static void ds_qmap_flow_cmd_resp_cb
(
  sio_port_id_type                port_id,
  uint8                           mux_id,
  uint8                           cmd_name,   
  a2_common_qmap_ctrl_cmd_type_e  cmd_type,
  dsm_item_type                 * dsm_ptr,
  uint32                          rtt_time
)
{
  ds_qmap_cmd_buf_type         * cmd_ptr = NULL;
 /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_INFO2_2("Recvd QMAP response [%d] from SIO port 0x%x",
                  cmd_name, port_id);

  if (!dsm_ptr)
  {
    LOG_MSG_ERROR_0("Invalid dsm ptr !");
    return;
  }

  cmd_ptr = (ds_qmap_cmd_buf_type *)
                    ds_qmap_get_cmd_buf(QMI_CMD_QMAP_CMD_RESP_CB);
  if (cmd_ptr != NULL)
  {
    cmd_ptr->cmd_id = QMI_CMD_QMAP_CMD_RESP_CB;
    cmd_ptr->qmap.qmap_resp.port_id  = port_id;
    cmd_ptr->qmap.qmap_resp.mux_id   = mux_id;
    cmd_ptr->qmap.qmap_resp.cmd_name = cmd_name;
    cmd_ptr->qmap.qmap_resp.cmd_type = (uint8)cmd_type;
    cmd_ptr->qmap.qmap_resp.dsm_ptr  = dsm_ptr;
    cmd_ptr->qmap.qmap_resp.rtt_time = rtt_time;
    
    dcc_send_cmd_ex(DCC_QMI_CMD, cmd_ptr);
  }
  else
  {
    LOG_MSG_ERROR_0("Out of cmd buf");
  }

} /* ds_qmap_flow_cmd_resp_cb() */

#endif /* FEATURE_DATA_A2 */

/*===========================================================================
  FUNCTION DS_QMAP_FLOW_CMD_TIMER_CBACK()

  DESCRIPTION
    QMAP Flow command timer is invoked when the QMAP cmd response is
    not received within the specified timeout(8s)

  PARAMETERS
    user_data:  Contains the id of the pending QMAP cmd

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void ds_qmap_cmd_timer_cback
(
  timer_cb_data_type  user_data
)
{ 
  uint32                    search_id;
  ds_qmap_cmd_buf_type   *  cmd_ptr = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  search_id = (uint32) user_data;
  LOG_MSG_INFO2_1( "Timer exp for QMAP resp %d", search_id);

  cmd_ptr = (ds_qmap_cmd_buf_type *)
                    ds_qmap_get_cmd_buf(QMI_CMD_QMAP_CMD_TIMER_EXP_CB);
  if (cmd_ptr != NULL)
  {
    cmd_ptr->cmd_id = QMI_CMD_QMAP_CMD_TIMER_EXP_CB;
    cmd_ptr->qmap.timer_info.search_id = search_id;
    dcc_send_cmd_ex(DCC_QMI_CMD, cmd_ptr);
  }
  else
  {
    LOG_MSG_ERROR_0("Out of cmd buf");
    return;
  }

} /* ds_qmap_cmd_timer_cback() */

/*===========================================================================
FUNCTION DS_QMAP_IS_QMAP_CMD_PENDING_RESP

DESCRIPTION
  Queue Compare Function
    Used by the queue searching functions to determine if an item is in
    the queue. This function checks whether the qmap response received
    is in the pending queue

DEPENDENCIES
  None

RETURN VALUE
  Returns TRUE if the element should be operated upon, FALSE otherwise

SIDE EFFECTS
  None.
===========================================================================*/
static int ds_qmap_is_qmap_cmd_pending_resp
(
  ds_qmap_cmd_info_type        * comp_param_ptr,    // from queue
  ds_qmap_cmd_resp_type        * cmd_resp_ptr       // resp info
)
{
  uint8                * temp_comp_param_ptr = NULL;
  int                    result = FALSE;

  uint8                  cmd; 
  uint8                  ip_family;
  uint16                 seq_no;
  uint32                 qos_id;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if ( (comp_param_ptr->mux_id == cmd_resp_ptr->mux_id) &&
       (comp_param_ptr->port_id == cmd_resp_ptr->port_id) )
  {
    temp_comp_param_ptr = comp_param_ptr->cmd_ptr;

    // Skip QMAP header
    cmd = *((uint8 *)temp_comp_param_ptr + DS_QMAP_CMD_OFFSET);

    // Compare cmd name
    if (cmd != cmd_resp_ptr->cmd_name)
    {
      return FALSE;
    }

    ip_family = *(uint8 *) (temp_comp_param_ptr + (DS_QMAP_CMD_PAYLOAD_OFFSET + 1) );
    seq_no    = *(uint16 *)(temp_comp_param_ptr + (DS_QMAP_CMD_PAYLOAD_OFFSET + 2) );
    qos_id    = *(uint32 *)(temp_comp_param_ptr + (DS_QMAP_CMD_PAYLOAD_OFFSET + 4) );

    // Compare payload
    if ( (ip_family == cmd_resp_ptr->payload.ip_family) &&
         (seq_no == cmd_resp_ptr->payload.seq_num) &&
         (qos_id == cmd_resp_ptr->payload.qos_id) )
    {
       result = TRUE;
    }
  }

  return result;

} /* ds_qmap_is_qmap_cmd_pending_resp() */

/*===========================================================================
FUNCTION DS_QMAP_IS_QMAP_CMD_ID

DESCRIPTION
  Queue Compare Function
    Used by the queue searching functions to determine if an item is in
    the queue. This function check if the passed qmap_id matched the
    queue item.
 

DEPENDENCIES
  None

RETURN VALUE
  Returns TRUE if the element should be operated upon, FALSE otherwise

SIDE EFFECTS
  None.
===========================================================================*/
static int ds_qmap_is_qmap_cmd_id
(
  ds_qmap_cmd_info_type   *  comp_param_ptr,      // comparison value
  int                     *  qmap_id             // searched value
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  return (*qmap_id == comp_param_ptr->qmap_cmd_id) ? TRUE : FALSE;  

} /* ds_qmap_is_qmap_cmd_id() */

/*===========================================================================
FUNCTION DS_QMAP_IS_QMAP_CMD_EXPIRED

DESCRIPTION
  Queue Compare Function
    Used by the queue searching functions to determine if an item is in
       the queue. This function checks if the passed qmap cmd is
       expired

DEPENDENCIES
  None

RETURN VALUE
  Returns TRUE if the element should be operated upon, FALSE otherwise

SIDE EFFECTS
  None.
===========================================================================*/
static boolean ds_qmap_is_qmap_cmd_expired
(
  ds_qmap_cmd_info_type  *  cmd_info_ptr,      // searched value
  ds_qmap_cmd_info_type  *  comp_param_ptr     // comparison value
)
{
  uint8  *  temp_comp_param_ptr = NULL;
  uint8  *  flow_comp_param_ptr = NULL;
  boolean   result = FALSE; 

  uint8 temp_ip;
  uint8 flow_ip;

  uint32 temp_qos;
  uint32 flow_qos;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  // Compare mux id, qos id, ip type
  if ( (cmd_info_ptr->mux_id == comp_param_ptr->mux_id) &&
       (cmd_info_ptr->port_id == comp_param_ptr->port_id) )
  {
    temp_comp_param_ptr = comp_param_ptr->cmd_ptr;
    flow_comp_param_ptr = cmd_info_ptr->cmd_ptr;

    // Skip QMAP header, qmap ctrl cmd header 

    temp_ip    = *(uint8 *)(temp_comp_param_ptr  + (DS_QMAP_CMD_PAYLOAD_OFFSET + 1) ); //ip type
    temp_qos   = *(uint32 *)(temp_comp_param_ptr + (DS_QMAP_CMD_PAYLOAD_OFFSET + 4) ); //qos id

    flow_ip    = *(uint8 *)(flow_comp_param_ptr  + (DS_QMAP_CMD_PAYLOAD_OFFSET + 1) ); //ip type
    flow_qos   = *(uint32 *)(flow_comp_param_ptr + (DS_QMAP_CMD_PAYLOAD_OFFSET + 4) ); //qos id

    if ( (temp_ip == flow_ip) && (temp_qos == flow_qos) )
    {
      result = TRUE;
    }
  }

  return result;

} /* ds_qmap_is_qmap_cmd_expired() */

/*===========================================================================
  FUNCTION DS_QMAP_PROCESS_QMAP_RESP()

  DESCRIPTION
    Called when a qmap command response is received
        
  PARAMETERS
    resp_info : ptr to the qmap resp info
    
  RETURN VALUE
    None
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void ds_qmap_process_qmap_resp
(
  sio_port_id_type                port_id,
  uint8                           mux_id,
  uint8                           cmd_name,   
  uint8                           cmd_type,
  dsm_item_type                 * dsm_ptr,
  uint32                          rtt_time
)
{
  ds_qmap_cmd_info_type        * resp_ptr = NULL;
  ds_qmap_cmd_resp_type          qmap_cmd_resp;
  void                         * value;
  uint16                         pkt_size;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  memset(&qmap_cmd_resp, 0, sizeof(ds_qmap_cmd_resp_type));

  qmap_cmd_resp.port_id  = port_id;
  qmap_cmd_resp.mux_id   = mux_id;
  qmap_cmd_resp.cmd_name = cmd_name;
  qmap_cmd_resp.cmd_type = cmd_type;
  qmap_cmd_resp.rtt_time = rtt_time;

  pkt_size = dsm_length_packet(dsm_ptr);
  if (pkt_size == 0)
  {
    LOG_MSG_ERROR_0( "Invalid DSM ptr " );
    dsm_free_packet(&dsm_ptr);
    return;
  }

  if( DS_QMAP_IS_FC_CMD(qmap_cmd_resp.cmd_name) )
  {
      if( pkt_size == DS_QMAP_FLOW_CTL_CMD_PAYLOAD_SIZE &&
          pkt_size <= sizeof(qmap_cmd_resp.payload) )
      {
        value = &(qmap_cmd_resp.payload);
      } 
      else
      {
        LOG_MSG_ERROR_1( "Invalid QMAP Flow ctrl e/d length pkt size %d",
                         pkt_size );
        goto send_result;
      }
  }
  else
  {
    LOG_MSG_INFO3_1( "Ingoring CB for non flow ctrl cmds %d ",
                     qmap_cmd_resp.cmd_name );
    goto send_result;
  }

  if (pkt_size != qmi_svc_dsm_pullup( &dsm_ptr, value, pkt_size ,FILE_ID_DS_QMAP,__LINE__ ))
  {
    goto send_result;
  }

  // Delete QMAP response from pending queue
  resp_ptr = q_linear_delete_new(
                     &qmap_cmd_pend_q,
                     (q_compare_func_type) ds_qmap_is_qmap_cmd_pending_resp,
                     &qmap_cmd_resp,
                     (q_action_func_type) NULL,
                     NULL);

  if (resp_ptr == NULL)
  {
    LOG_MSG_INFO2_0("Invalid/ Old response recvd: Ignoring!");
  }
  else
  {
    // Stop corresponding timer
    (void) timer_clr( &(resp_ptr->qmap_cmd_timer), T_NONE );
    PS_SYSTEM_HEAP_MEM_FREE(resp_ptr->cmd_ptr);
    PS_SYSTEM_HEAP_MEM_FREE(resp_ptr);
  }

send_result:
  dsm_free_packet(&dsm_ptr);

} /* ds_qmap_process_qmap_resp() */

/*===========================================================================
  FUNCTION DS_QMAP_PROCESS_TIMER_EXP()

  DESCRIPTION
    Called when a qmap response timer cb occurs
        
  PARAMETERS
    search_id :  ID of the expired qmap cmd
    
  RETURN VALUE
    None
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void ds_qmap_process_timer_exp
(
  uint32 search_id
)
{
  ds_qmap_cmd_info_type   * resp_ptr = NULL;
  boolean                   status = FALSE;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  // Search resp in pending queue and remove; Clear timer
  resp_ptr = q_linear_delete_new(
                   &qmap_cmd_pend_q,
                   (q_compare_func_type) ds_qmap_is_qmap_cmd_id,
                   &search_id,
                   (q_action_func_type) NULL,
                   NULL);

  if (resp_ptr == NULL)
  {
    LOG_MSG_INFO3_0("Old timer: Ignoring!" );
  }
  else
  {
    // Stop corresponding timer //Check if we need to stop ???
    (void) timer_clr( &(resp_ptr->qmap_cmd_timer), T_NONE );

    if (resp_ptr->num_retx < DS_QMAP_CMD_MAX_RETRY)
    {
      resp_ptr->num_retx++;
    }
    else
    {
      // Drop the packet; Free the memory 
      LOG_MSG_INFO2_0( "Max retries reached. No more retx !" );
      PS_SYSTEM_HEAP_MEM_FREE(resp_ptr->cmd_ptr);
      PS_SYSTEM_HEAP_MEM_FREE(resp_ptr);
      return;
    }

    // Resend QMAP cmd with retx flag set to TRUE
    status = ds_qmap_send_cmd(
                  resp_ptr->port_id,
                  resp_ptr->mux_id,
                  resp_ptr->cmd_size,
                  resp_ptr->cmd_ptr,
                  resp_ptr);

    if (!status)
    {
        LOG_MSG_ERROR_0("Unable to resend QMAP pkt ");
    }
  }

} /* ds_qmap_process_timer_exp() */

