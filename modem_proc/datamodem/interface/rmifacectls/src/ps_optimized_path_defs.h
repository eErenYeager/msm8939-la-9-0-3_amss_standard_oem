#ifndef PS_OPT_PATH_DEFS_H
#define PS_OPT_PATH_DEFS_H
/*===========================================================================

                     PS_OPT_PATH_DEFS . H
DESCRIPTION
  Header file describing all Optimized data path definitions used by
    both UL/DL.

Copyright (c) 2011-2013 Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary
===========================================================================*/
/*===========================================================================

                            EDIT HISTORY FOR FILE
  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/rmifacectls/src/ps_optimized_path_defs.h#1 $

when        who    what, where, why
--------    ---    ----------------------------------------------------------
05/13/2013  pgm    Added MBIM feature flag.
01/29/13    vb     Fix to handle the GRE packet processing in
                   NAT optimized path.
09/14/12    tw     IPv6 prefix missmatch errors fixed
05/24/2012  pgm    RNDIS defns.
05/07/2012  mp     Fix to send TCP Reset for exhausted NAT entry.
08/15/2011  AM     created module.
===========================================================================*/

/*===========================================================================

                          INCLUDE FILES FOR MODULE

===========================================================================*/

#include "comdef.h"
#include "customer.h"
#include "ps_tcp_config.h"
#include "ps_crit_sect.h"
#include "ps_icmp.h"
#include "ps_ifacei.h"

extern ps_crit_sect_type rmnet_rx_pkt_hdlr_crit_section;

#define PS_OPT_PATH_QOS_HEADER_OFFSET  6
#define PS_OPT_PATH_QOS2_HEADER_OFFSET 8
#define PS_OPT_PATH_WORD_SIZE          4
#define PS_OPT_PATH_WORD_SHIFT         2
#define PS_OPT_PATH_QOS_FLAG_TYPE_DOS  0x1
#define PS_OPT_PATH_METASM_DATAFMT_SET 1

/*-----------------------------------------------------------------------
   HEADER LEN FOR ALL MODES
   PS_OPT_PATH_IP_NO_QOS_HEADER_LEN: IP(40) + TCP Header len(13)
   PS_OPT_PATH_IP_QOS_HEADER_LEN: QOS(6) + IP(40) + TCP Header len(13)
   PS_OPT_PATH_ETH_NO_QOS_HEADER_LEN:IP(40)+ETH(14) +
                                           TCP Header len(13)
   PS_OPT_PATH_ETH_QOS_HEADER_LEN:QOS(6)+IP(40)+ETH(14) +
                                           TCP Header len(13)
 -----------------------------------------------------------------------*/
#define PS_OPT_PATH_IP_NO_QOS_HEADER_LEN         \
   (PS_OPT_PATH_V6_HDR_LEN +                     \
    PS_OPT_PATH_TCP_HDR_LEN_EXTRACT)

#define PS_OPT_PATH_IP_QOS_HEADER_LEN            \
   (PS_OPT_PATH_V6_HDR_LEN +                     \
    PS_OPT_PATH_QOS_HEADER_OFFSET +              \
    PS_OPT_PATH_TCP_HDR_LEN_EXTRACT)

#define PS_OPT_PATH_IP_QOS2_HEADER_LEN           \
   (PS_OPT_PATH_V6_HDR_LEN +                     \
    PS_OPT_PATH_QOS2_HEADER_OFFSET +             \
    PS_OPT_PATH_TCP_HDR_LEN_EXTRACT)

#define PS_OPT_PATH_ETH_NO_QOS_HEADER_LEN        \
  (PS_OPT_PATH_V6_HDR_LEN +                      \
   PS_OPT_PATH_ETH_HEADER_OFFSET +               \
   PS_OPT_PATH_TCP_HDR_LEN_EXTRACT)

#define PS_OPT_PATH_ETH_QOS_HEADER_LEN            \
  (PS_OPT_PATH_V6_HDR_LEN        +                \
   PS_OPT_PATH_QOS_HEADER_OFFSET +                \
   PS_OPT_PATH_ETH_HEADER_OFFSET +                \
   PS_OPT_PATH_TCP_HDR_LEN_EXTRACT)

/*-----------------------------------------------------------------------
    HEADER LEN CHECKS FOR DSM_EXTRACT
 -----------------------------------------------------------------------*/
#define PS_OPT_PATH_IP_NO_QOS_HEADER_LEN_CHECK    \
   PS_OPT_PATH_V6_HDR_LEN

#define PS_OPT_PATH_IP_QOS_HEADER_LEN_CHECK       \
  (PS_OPT_PATH_V6_HDR_LEN +                       \
   PS_OPT_PATH_QOS_HEADER_OFFSET)

#define PS_OPT_PATH_IP_QOS2_HEADER_LEN_CHECK      \
  (PS_OPT_PATH_V6_HDR_LEN +                       \
   PS_OPT_PATH_QOS2_HEADER_OFFSET)

#define PS_OPT_PATH_ETH_NO_QOS_HEADER_LEN_CHECK   \
  (PS_OPT_PATH_V6_HDR_LEN +                       \
   PS_OPT_PATH_ETH_HEADER_OFFSET)

#define PS_OPT_PATH_ETH_QOS_HEADER_LEN_CHECK      \
  (PS_OPT_PATH_V6_HDR_LEN        +                \
   PS_OPT_PATH_QOS_HEADER_OFFSET +                \
   PS_OPT_PATH_ETH_HEADER_OFFSET)

/*-----------------------------------------------------------------------
    ETHERNET HDR
 -----------------------------------------------------------------------*/
#define PS_OPT_PATH_ETH_HEADER_OFFSET  14
#define PS_OPT_PATH_ETHERTYPE_OFFSET   12

/*-----------------------------------------------------------------------
    IP HDR
 -----------------------------------------------------------------------*/
#define PS_OPT_PATH_IP_VERSION_MASK    0xF0
#define PS_OPT_PATH_IP_HDR_LEN_MASK    0x0F

/*-----------------------------------------------------------------------
    IPV4
 -----------------------------------------------------------------------*/
#define PS_OPT_PATH_V4_VERSION_SHIFT     4
#define PS_OPT_PATH_V4_VERSION           0x40
#define PS_OPT_PATH_V4_HDR_LEN           20
#define PS_OPT_PATH_V4_TOTAL_LEN_OFFSET  2
#define PS_OPT_PATH_V4_ID_OFFSET         4
#define PS_OPT_PATH_V4_FRAG_OFFSET       6
#define PS_OPT_PATH_V4_PROT_FIELD_OFFSET 9
#define PS_OPT_PATH_V4_CKSUM_OFFSET      10
#define PS_OPT_PATH_V4_SRC_ADDR_OFFSET   12
#define PS_OPT_PATH_V4_DEST_ADDR_OFFSET  16
#define PS_OPT_PATH_V4_FRAG_MASK         0x3FFF
#define PS_OPT_PATH_V4_ADDRESS_LEN       4
#define PS_OPT_PATH_V4_SRC_PORT_OFFSET   20
#define PS_OPT_PATH_V4_DST_PORT_OFFSET   22
#define PS_OPT_PATH_V4_TCP_FLAGS_OFFSET  33
#define PS_OPT_PATH_V4_TCP_CKSUM_OFFSET  36
#define PS_OPT_PATH_V4_TCP_SEQNO_OFFSET  24
#define PS_OPT_PATH_V4_UDP_CKSUM_OFFSET  26
#define PS_OPT_PATH_V4_GRE_CALLID_OFFSET 26

/*-----------------------------------------------------------------------
 LOW LATENCY TRAFFIC Definitions
 -----------------------------------------------------------------------*/
#define PS_OPT_PATH_V4_FRAG_OFFSET_MASK  0xFF1F
#define PS_OPT_PATH_V6_FRAG_OFFSET_MASK  0xF8FF
#define PS_ICMP_TYPE_OFFSET              0 
#define PS_ICMP_CODE_OFFSET              1
#define PS_ICMP6_TYPE_OFFSET             0
#define PS_ICMP6_CODE_OFFSET             1
#define PS_FRAG_NEXT_HDR_OFFSET          0
#define PS_FRAG_FRAG_OFFSET              2
#define PS_ICMP_HDR_LEN_EXTRACT          2
#define PS_OPT_PATH_ICMP6_HDR_LEN_EXTRACT  4
#define PS_OPT_PATH_V6_OPT_HDR_LEN       8

#define PS_UL_OPT_V4_IP_ICMP_LEN_CHECK  ( PS_OPT_PATH_V4_HDR_LEN +             \
                                                PS_ICMP_HDR_LEN_EXTRACT )

#define PS_UL_OPT_V6_IP_ICMP_LEN_CHECK                                   \
                                         ( PS_OPT_PATH_V6_HDR_LEN +            \
                                           PS_OPT_PATH_ICMP6_HDR_LEN_EXTRACT )

#define PS_UL_OPT_QOS_ETH_V4_ICMP_LEN_CHECK                                    \
                                          ( PS_OPT_PATH_QOS_HEADER_OFFSET +    \
                                            PS_OPT_PATH_ETH_HEADER_OFFSET +    \
                                            PS_OPT_PATH_V4_HDR_LEN +           \
                                            PS_ICMP_HDR_LEN_EXTRACT )

#define PS_UL_OPT_QOS_ETH_V6_ICMP6_LEN_CHECK                                   \
                                          ( PS_OPT_PATH_QOS_HEADER_OFFSET +    \
                                            PS_OPT_PATH_ETH_HEADER_OFFSET +    \
                                            PS_OPT_PATH_V6_HDR_LEN +           \
                                            PS_OPT_PATH_ICMP6_HDR_LEN_EXTRACT )


/*-----------------------------------------------------------------------
    IPV6
 -----------------------------------------------------------------------*/
#define PS_OPT_PATH_V6_VERSION           0x60
#define PS_OPT_PATH_V6_HDR_LEN           40
#define PS_OPT_PATH_V6_TOTAL_LEN_OFFSET  4
#define PS_OPT_PATH_V6_NXT_HDR_OFFSET    6
#define PS_OPT_PATH_V6_DEST_ADDR_OFFSET  24
#define PS_OPT_PATH_V6_SRC_ADDR_OFFSET   8
#define PS_OPT_PATH_V6_MCAST_MASK        0xFF

/*-----------------------------------------------------------------------
    TCP ACK PRIORITIZATION
 -----------------------------------------------------------------------*/
#define PS_OPT_PATH_TCP_HDR_LEN_OFFSET   12
#define PS_OPT_PATH_TCP_HDR_LEN_EXTRACT  13
#define PS_OPT_PATH_TCP_HDR_LEN_MASK     0xF0
#define PS_OPT_PATH_TCP_RST_FIN_SET      0x05
#define PS_OPT_PATH_TXPRT_PORT_LEN       2

#define PS_OPT_PATH_UDP_HDR_LEN          8

/*-----------------------------------------------------------------------
    MBIM HEADER
 -----------------------------------------------------------------------*/

/*NTH COMMON HEADER*/
#define PS_OPT_PATH_NTHC_LEN      8
#define PS_OPT_PATH_NTHS_LEN      4

/*NDP HEADER*/
#define PS_OPT_PATH_NDP_HDR_LEN           8
#define PS_OPT_PATH_NDP_DG_OFFSET         4
#define PS_OPT_PATH_NDP_MAX_DG            10
#define PS_OPT_PATH_NDP_HDR_FIELD_OFFSET  4
#define PS_OPT_PATH_NDP_HDR_FIELD_LEN     2
#define PS_OPT_PATH_NDP_LEN_CHECK         16
#define PS_OPT_PATH_NDP_IP_OFF_LEN_PAIR_LEN          \
        (PS_OPT_PATH_NDP_MAX_DG * PS_OPT_PATH_NDP_DG_OFFSET)
#define PS_OPT_PATH_MAX_ST_NDP_LEN                   \
        (PS_OPT_PATH_NDP_IP_OFF_LEN_PAIR_LEN +       \
         PS_OPT_PATH_NDP_HDR_LEN)

#ifdef FEATURE_DATA_MBIM
#define PS_OPT_PATH_MBIM_IPS_MASK        0x000000FF
#endif/*FEATURE_DATA_MBIM */

/*-----------------------------------------------------------------------
    RNDIS HEADER
 -----------------------------------------------------------------------*/
#define PS_OPT_PATH_RNDIS_HEADER_OFFSET       44
#define PS_OPT_PATH_RNDIS_ETH_MIN_HEADER_LEN    \
        (PS_OPT_PATH_RNDIS_HEADER_OFFSET + PS_OPT_PATH_ETH_HEADER_OFFSET \
         PS_OPT_PATH_V6_HDR_LEN + TCPLEN)

#define PS_OPT_PATH_RNDIS_ETH_IP_HDR_LEN                             \
        (PS_OPT_PATH_RNDIS_HEADER_OFFSET + PS_OPT_PATH_ETH_HEADER_OFFSET + \
         PS_OPT_PATH_V6_HDR_LEN + TCPLEN )
#define PS_OPT_PATH_RNDIS_ETH_IP_HDR_LEN_CHECK                             \
         (PS_OPT_PATH_RNDIS_HEADER_OFFSET + PS_OPT_PATH_ETH_HEADER_OFFSET + \
         PS_OPT_PATH_V6_HDR_LEN)
#define PS_OPT_PATH_RNDIS_ETH_HDR_LEN                     \
        (PS_OPT_PATH_RNDIS_HEADER_OFFSET + PS_OPT_PATH_ETH_HEADER_OFFSET)
/*-----------------------------------------------------------------------------
    SoftAP specific
-----------------------------------------------------------------------------*/

#define PS_OPT_PATH_SOFTAP_ETH_EXP_HEADER_LEN                             \
    ( PS_OPT_PATH_V4_HDR_LEN + TCPLEN + PS_OPT_PATH_ETH_HEADER_OFFSET )

#define PS_OPT_PATH_SOFTAP_ETH_MIN_HEADER_LEN                             \
    ( PS_OPT_PATH_V4_HDR_LEN + PS_OPT_PATH_UDP_HDR_LEN +              \
      PS_OPT_PATH_ETH_HEADER_OFFSET )

#define PS_OPT_PATH_SOFTAP_IP_EXP_HEADER_LEN                              \
    ( PS_OPT_PATH_V4_HDR_LEN + TCPLEN )

#define PS_OPT_PATH_SOFTAP_IP_MIN_HEADER_LEN                              \
    ( PS_OPT_PATH_V4_HDR_LEN + PS_OPT_PATH_UDP_HDR_LEN )

#define PS_OPT_PATH_PORT_INFO_LEN            4
#define PS_OPT_PATH_EXP_HEADER_LEN_DL                                     \
    ( PS_OPT_PATH_V6_HDR_LEN + PS_OPT_PATH_PORT_INFO_LEN )

/*===========================================================================
FUNCTION PS_OPT_PATH_HGET32_UNALIGNED

DESCRIPTION
  This function extracts 32 bits in host order.

PARAMETERS
  cp - Byte value.

RETURN VALUE
  Returns 32 bit algined value.

DEPENDENCIES
  None

SIDE EFFECTS
  None
===========================================================================*/
INLINE dword ps_optimized_hget32_unaligned
(
  byte *cp
)
{

  dword rval;
 /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  cp += 4;

  rval = *(--cp);
  rval <<= 8;
  rval |= *(--cp);
  rval <<= 8;
  rval |= *(--cp);
  rval <<= 8;
  rval |= *(--cp);

  return( rval);

} /* ps_optimized_hget32_unaligned() */

/*===========================================================================
FUNCTION PS_UL_OPTIMIZED_V4_LOW_LATENCY_TRAFFIC_HANDLER

DESCRIPTION
  This function checks whether the packet is ICMP ECHO REQUEST/RESPONSE and 
  kicks in timer start mechanism

PARAMETERS
  pkt_buf     : pointer to the v4 pkt buffer
  ip_hdr_len  : IP header length
  prot_offset : Next protocol offset in ip header

RETURN VALUE
  None
DEPENDENCIES
  None

SIDE EFFECTS
  None.

===========================================================================*/
INLINE void ps_ul_optimized_v4_low_latency_traffic_handler
(
  uint8 * const pkt_buf,
  uint8   ip_hdr_len,
  uint8   prot_offset
)
{
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  /*----------------------------------------------------------------------------
    If the pkt is ICMP and this is the first fragment of the packet (i.e. 
    fragment offset is 0) call low latency timer handler. 
    protocol offset and ip header length includes any headers present before IP
    header depending on the path it has come from.
    In IPv4 header fragment_offset field comes 3 bytes before next_protocol
    field and hence the value ( prot_offset - 3 ).By using this logic we can
    use the same function for ethernet mode and IP mode instead of having 
    another parameter for this function.
  -----------------------------------------------------------------------------*/
  if( ( pkt_buf != NULL ) && (*(pkt_buf + prot_offset) == PS_IPPROTO_ICMP) &&
      ( !( *(uint16 *) (pkt_buf +  prot_offset - 3 ) & 
                                            PS_OPT_PATH_V4_FRAG_OFFSET_MASK) &&
        ((*(pkt_buf + ip_hdr_len + PS_ICMP_TYPE_OFFSET) == ICMP_ECHO_REQ) ||
         (*(pkt_buf + ip_hdr_len + PS_ICMP_TYPE_OFFSET) == ICMP_ECHO_REPLY)) && 
         (*(pkt_buf + ip_hdr_len + PS_ICMP_CODE_OFFSET) == 0)) )
  {
    ps_iface_low_latency_timer_start();
  }
}/*ps_ul_optimized_v4_low_latency_traffic_handler()*/

#endif /* PS_OPT_PATH_DEFS_H */

