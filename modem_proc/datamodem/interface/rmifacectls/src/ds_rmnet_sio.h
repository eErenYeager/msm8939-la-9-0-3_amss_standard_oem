#ifndef _DS_RMNET_SIO_H
#define _DS_RMNET_SIO_H
/*===========================================================================

                         D S _ R M N E T _ S I O . H

DESCRIPTION

  Rm Network device - SIO interface RmNET module internal header file.

  Functions for data channel setup and data transfer for the Rm Network
  device.

EXTERNALIZED FUNCTIONS

  rmnet_sio_init()
    Initialize the specified RmNetSio instance and link it to the RDM
    service specified.
  rmnet_sio_transmit()
    Send data in 'tx_item' over the data channel associated with provided
    rmnetsio handle 'rmnet_sio_handle'
  rmnet_sio_receive()
    Get data from the rx watermark for the data channel associated with
    provided rmnetsio handle 'rmnet_sio_handle'
  rmnet_sio_set_dtr_handler()
    Specify the callback function that should be invoked when the DTR state
    changes on the specifed rmnetsio data link.
  rmnet_sio_set_rx_cb()
    Specify the callback function that should be invoked when the DTR state
    changes on the specifed rmnetsio data link.
  rmnet_sio_set_rx_flow()
    Flow control the dsnetsio link specified by rmnet_sio_handle.
  rmnet_sio_clear_wmk_queues()
    Clear any data present in the rx and tx wmks.

Copyright (c) 2004-2005 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/rmifacectls/src/ds_rmnet_sio.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when        who    what, where, why
--------    ---    ----------------------------------------------------------
01/12/12    wc     Call a2 tlp functions only for a2 port
09/07/11    pgm    Added rmnet_sio_info_type structure definition.
10/23/06    ar     Change rmnet_sio_activate_instance() prototype
09/27/07    ks     Added memory based flow control.
09/21/06    ks     Added rmnet_sio_clear_wmk_queues() to clear tx and rx
                   wmks when rmnet_sm transitions to NULL.
08/23/06    ks     Changes to support multiple rmnet instances.
03/07/06    jd     Add TLP support
02/22/06    ks     Added rmnet_sio_verify_dtr_high() to check if dtr is high.
10/10/05    ks     Removed SIO DATA WWAN RDM device.
                   Added rmnet_sio_check_status().
05/31/05    jd     Code review updates
03/14/05    ks     Removed featurization
03/07/05    ks     Created file - Rm Net SIO library internal definitions
                   that were in ds_netsio.h.
===========================================================================*/

/*===========================================================================

                                INCLUDE FILES

===========================================================================*/
#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"

#define FEATURE_DSM_WM_CB

#include "dsm.h"
#include "ds_rmnet_defs.h"
#include "ds_rmnet_meta_sm.h"
#include "ds_rmnet_qmap.h"

#include "sio.h"

#ifdef FEATURE_DATA_A2
#include "a2_sio_ext.h"
#endif /* FEATURE_DATA_A2 */

/*===========================================================================

                                 DEFINITIONS

===========================================================================*/

#define rmnet_sio_is_a2_port(sio_port) \
           (SIO_MAJOR_NUM_OF_DEVICE((sio_port)) == SIO_MAJOR_A2) 

#define RMNET_SIO_GET_DEFAULT_INSTANCE(rmnet_inst) \
    ((rmnet_instance_e_type)((rmnet_inst >> 1) << 1))

typedef enum
{
  RMNET_SIO_TLP_VER_NONE = 0,
  RMNET_SIO_TLP_VER_1    = 1,
  RMNET_SIO_TLP_MAX
} rmnet_sio_tlp_ver_type;

/*---------------------------------------------------------------------------
  Flow control reason masks for flow control apis
---------------------------------------------------------------------------*/
#define RMNET_SIO_FLOWMASK_ALL  (0xFFFFFFFF)
#define RMNET_SIO_FLOWMASK_MEM  (1 << 0)
#define RMNET_SIO_FLOWMASK_GLOBAL_MEM  (1 << 1) //for mem based flow control
#define RMNET_SIO_FLOWMASK_NONE (0x00000000)

/*---------------------------------------------------------------------------
  callback function to be invoked when DTR changes
---------------------------------------------------------------------------*/
typedef void (* rmnet_sio_dtr_cb_f_type)
(
  void * user_data,
  boolean dtr_asserted
);

typedef struct
{
  sio_stream_id_type       stream_id;    /* SIO stream ID of network iface */
  dsm_watermark_type       rx_wmk;       /* USB control stream receive wm  */
  q_type                   rx_wmk_q;     /* Corresponding USB ctl rx queue */
  uint32                   rx_flow_mask; /* flow mask for rx watermark     */
  dsm_watermark_type       tx_wmk;       /* USB control stream transmit wm */
  q_type                   tx_wmk_q;     /* Corresponding USB ctl tx queue */
  uint32                   tx_flow_mask; /* flow mask for tx watermark     */
  rmnet_sio_dtr_cb_f_type  dtr_cb;       /* callback for dtr events       */
  void *                   dtr_cb_user_data;
  sio_ioctl_usb_tlp_settings_type  tlp_setting; /* last tlp setting from sio */
  void (*tlp_cb)( rmnet_sio_tlp_ver_type, rmnet_sio_tlp_ver_type, void * );
                                         /* tlp change callback            */
  void *              tlp_user_data;     /* tlp change callback user data  */
#ifdef FEATURE_RMNET_DATA_AGG_TIMER
  timer_type               rmnet_smd_delay_timer;
  uint32                   rmnet_smd_timer_value;
#endif /* FEATURE_RMNET_DATA_AGG_TIMER */
  boolean                  inited;
  sio_port_id_type         sio_port;
#ifdef FEATURE_DATA_A2
  a2_sio_port_cfg_s        mux_port_cfg;
#endif
  ds_rmnet_qmap_settings_type qmap_settings;
} rmnet_sio_info_type;

/*===========================================================================

                              EXTERNAL FUNCTIONS

===========================================================================*/

/*===========================================================================
  FUNCTION RMNET_SIO_DEACTIVATE_INSTANCE()

  DESCRIPTION
    Deactivates the specified RmNetSio instance (i.e. closes SIO streams)

  PARAMETERS
    handle : handle to the instance of QMI to deactivate

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
extern boolean rmnet_sio_deactivate_instance
(
  void *handle
);


/*===========================================================================
  FUNCTION RMNET_SIO_ACTIVATE_INSTANCE()

  DESCRIPTION
    Activates the specified RmNetSio instance (i.e. opens SIO streams)

  PARAMETERS
    handle : handle to the instance of RmNET to activate
    port_id :  SIO port identifier

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
extern boolean rmnet_sio_activate_instance
(
  void *  handle,
  uint16  port_id
);


/*===========================================================================
  FUNCTION RMNET_SIO_INIT()

  DESCRIPTION
    Initialize the specified RmNetSio instance and link it to the RDM
    service specified.

  PARAMETERS
    qmi_instance : instance of QMI to register
    service      : RDM service to link this i/o path to

  RETURN VALUE
    RmNetSIO handle - opaque handle to this rmnetsio instance

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
extern void *  rmnet_sio_init
(
  rmnet_instance_e_type  instance
);

/*===========================================================================
  FUNCTION RMNET_SIO_TRANSMIT()

  DESCRIPTION
    Send data in 'tx_item' over the data channel associated with provided
    rmnetsio handle 'rmnet_sio_handle'

  PARAMETERS
    rmnet_sio_handle :  handle returned by rmnet_sio_init()
    tx_item          :  data to be transmitted

  RETURN VALUE
    None

  DEPENDENCIES
    rmnet_sio_init() must have been called previously

  SIDE EFFECTS
    None
===========================================================================*/
extern void  rmnet_sio_transmit
(
  void *            rmnet_sio_handle,
  dsm_item_type **  tx_item
);

/*===========================================================================
  FUNCTION RMNET_SIO_RECEIVE()

  DESCRIPTION
    Get data from the rx watermark for the data channel associated with
    provided rmnetsio handle 'rmnet_sio_handle'

  PARAMETERS
    rmnet_sio_handle :  handle returned by rmnet_sio_init()

  RETURN VALUE
    DSM item pointer contained received packet

  DEPENDENCIES
    rmnet_sio_init() must have been called previously

  SIDE EFFECTS
    None
===========================================================================*/
extern dsm_item_type *  rmnet_sio_receive
(
  void *  rmnet_sio_handle
);


/*===========================================================================
  FUNCTION RMNET_SIO_SET_TLP_CHANGE_CB()

  DESCRIPTION
    Specify the callback function that should be invoked when the USB TLP
    settings change

  PARAMETERS
    rmnet_sio_handle :  handle to dsnetsio link returned by rmnet_sio_init()
    tlp_cb           :  callback function to be invoked when TLP use changes
                        Arguments to callback will be:
                          dl_ver => negotiated version of TLP for downlink
                          ul_ver => negotiated version of TLP for uplink
                          void * => user data provided as 3rd arg to
                                    rmnet_sio_set_tlp_change_cb()

  RETURN VALUE
    None

  DEPENDENCIES
    rmnet_sio_init() must have been called previously

  SIDE EFFECTS
    None
===========================================================================*/
extern void  rmnet_sio_set_tlp_change_cb
(
  void *  rmnet_sio_handle,
  void (* tlp_cb)( rmnet_sio_tlp_ver_type, rmnet_sio_tlp_ver_type, void * ),
  void *  user_data
);


/*===========================================================================
  FUNCTION RMNET_SIO_SET_DTR_HANDLER()

  DESCRIPTION
    Specify the callback function that should be invoked when the DTR state
    changes on the specifed rmnetsio data link.

  PARAMETERS
    rmnet_sio_handle :  handle to dsnetsio link returned by rmnet_sio_init()
    dtr_cb           :  callback function to be invoked when DTR changes
                        First argument to callback will be DTR state -
                        TRUE = asserted, FALSE = deasserted

  RETURN VALUE
    None

  DEPENDENCIES
    rmnet_sio_init() must have been called previously

  SIDE EFFECTS
    None
===========================================================================*/
extern void  rmnet_sio_set_dtr_handler
(
  void *                   rmnet_sio_handle,
  rmnet_sio_dtr_cb_f_type  dtr_cb,
  void *                   user_data
);

/*===========================================================================
  FUNCTION RMNET_SIO_SET_RX_CB()

  DESCRIPTION
    Specify the callback function that should be invoked when the DTR state
    changes on the specifed rmnetsio data link.

  PARAMETERS
    rmnet_sio_handle :  handle to dsnetsio link returned by rmnet_sio_init()
    rx_cb            :  callback to be called when data arrives on the data
                        path associated with rmnet_sio_handle
                        1st param = dsm pointer to received packet
                        2nd param = user_data provided
    user_data        :  void * to be passed in the 2nd parameter each time
                        rx_cb is invoked

  RETURN VALUE
    None

  DEPENDENCIES
    rmnet_sio_init() must have been called previously

  SIDE EFFECTS
    Clears any data previously queued in the rx watermark
===========================================================================*/
extern void  rmnet_sio_set_rx_cb
(
  void *  rmnet_sio_handle,
  void (* rmnet_sio_default_rx_cb)( dsm_watermark_type *, void * ),
  void *  user_data
);

/*===========================================================================
  FUNCTION RMNET_SIO_SET_RX_FLOW()

  DESCRIPTION
    Flow control the dsnetsio link specified by rmnet_sio_handle.

    The reason for the flow is specified by 'mask'.  Whether to enable (0)
    or disable (1) flow is specied by 'disable' parameter.  Caller must
    enable the flow with the same mask that was used to disable.

  PARAMETERS
    rmnet_sio_handle :  handle to dsnetsio link returned by rmnet_sio_init()
    mask             :  Bitmask indicating the reason for flow control
                        (allows disabling flow if any of various reasons
                         represented by separate bits has been specified)
    disable          :  0 = enable flow for reasons in 'mask'
                        1 = enable flow for reasons in 'mask'

  RETURN VALUE
    None

  DEPENDENCIES
    rmnet_sio_init() must have been called previously

  SIDE EFFECTS
    None
===========================================================================*/
extern void rmnet_sio_set_rx_flow
(
  void *  rmnet_sio_handle,
  uint32  mask,          /* input mask of calling function                 */
  boolean disable        /* TRUE/FALSE: disable/enable flow mask           */
);

/*===========================================================================
  FUNCTION RMNET_SIO_CHECK_STATUS()

  DESCRIPTION
    To check if the Rmnet sio link was initalized.

  PARAMETERS
    rmnet_sio_handle :  handle to rmnetsio link returned by rmnet_sio_init()

  RETURN VALUE
    boolean : TRUE  = Link was intialized
              FALSE = Link wasn't initialized

  DEPENDENCIES

  SIDE EFFECTS
    None
===========================================================================*/
boolean rmnet_sio_check_status
(
  void *  rmnet_sio_handle
);


/*===========================================================================
  FUNCTION RMNET_SIO_VERIFY_DTR_HIGH()

  DESCRIPTION
    To check if DTR is high or low.

  PARAMETERS
    rmnet_sio_handle : ptr to rmnet_sio info

  RETURN VALUE
    boolean : TRUE  = DTR is high
              FALSE = DTR is low

  DEPENDENCIES
    rmnet_sio_init() must have been called previously

  SIDE EFFECTS
    None
===========================================================================*/
boolean rmnet_sio_verify_dtr_high
(
  void * rmnet_sio_handle
);


/*===========================================================================
  FUNCTION RMNET_SIO_CLEAR_WMK_QUEUES()

  DESCRIPTION
    Clear any data present in the rx and tx wmks.

  PARAMETERS
    rmnet_sio_handle : ptr to rmnet_sio info

  RETURN VALUE
    None

  DEPENDENCIES
    rmnet_sio_init() must have been called previously

  SIDE EFFECTS
    None
===========================================================================*/
void rmnet_sio_clear_wmk_queues
(
  void * rmnet_sio_handle
);
/*===========================================================================
FUNCTION RMNET_SIO_GET_PORT

DESCRIPTION
  This function gets the port number associated to the rmiface

PARAMETERS
  *rmnet_sio_handle: ptr to rmnet_sio_info

DEPENDENCIES
  None

RETURN VALUE
  sio_port_id_type - the associated port

SIDE EFFECTS
  None.
===========================================================================*/
sio_port_id_type rmnet_sio_get_port
(
  void * rmnet_sio_handle
);

#ifdef FEATURE_DATA_RMNET_USB_STANDARD_ECM
void rmnet_sio_link_up_notify
(
  void * rmnet_sio_handle,
  boolean link_up
);

void rmnet_sio_ioctl
(
  void *            rmnet_sio_handle,
  sio_ioctl_cmd_type cmd,
  sio_ioctl_param_type *param
);
#endif /* FEATURE_DATA_RMNET_USB_STANDARD_ECM */

/*===========================================================================
  FUNCTION RMNET_SIO_GET_WMK_QUEUES()

  DESCRIPTION
    This function returns the pointer to the RX and TX watermark Queues.

  PARAMETERS
    rmnet_sio_handle (IN): ptr to rmnet_sio info
    rx_wmk (OUT): pointer to RX watermark.
    tx_wmk (OUT): pointer to TX watermark.

  RETURN VALUE
    Failure : -1
    Success :  0

  DEPENDENCIES
    rmnet_sio_init() must have been called previously

  SIDE EFFECTS
    None
===========================================================================*/
int16 rmnet_sio_get_wmk_queues
(
  void                * rmnet_sio_handle,
  dsm_watermark_type  **rx_wmk,
  dsm_watermark_type  **tx_wmk
);

#ifdef FEATURE_DATA_A2
/*===========================================================================
  FUNCTION RMNET_SIO_GET_DEFAULT_PORT_CFG()

  DESCRIPTION
    This function retrieves the default port configuration parameters.

  PARAMETERS
    rmnet_sio_handle (IN): ptr to rmnet_sio info
    port_cfg: pointer to port config structure 

  RETURN VALUE
    None

  DEPENDENCIES
    rmnet_sio_init() must have been called previously

  SIDE EFFECTS
    None
===========================================================================*/
void rmnet_sio_get_default_port_cfg
(
  void               * rmnet_sio_handle,
  a2_sio_port_cfg_s  * port_cfg
);

/*===========================================================================
FUNCTION RMNET_SIO_SET_DATA_FORMAT

DESCRIPTION
  This function sets the UL and DL data format for dynamic port config

PARAMETERS
  rmnet_sio_handle: ptr to rmnet sio info
  ul_data_agg:      UL data aggregation mode
  dl_data_agg:      DL data aggregation mode
  ndp_sig:          NDP signature
  dl_agg_max_num:   DL agg max num
  dl_agg_max_size:  DL agg max size  

DEPENDENCIES
  None

RETURN VALUE
  TRUE if success, FALSE if failure

SIDE EFFECTS
  None.
===========================================================================*/
boolean rmnet_sio_set_data_format
(
  void                     * rmnet_sio_handle,
  rmnet_data_agg_enum_type   ul_data_agg,
  rmnet_data_agg_enum_type   dl_data_agg,
  uint32                     ndp_sig,
  uint32                     dl_agg_max_num,
  uint32                     dl_agg_max_size,  
  uint8                      dl_min_padding
);

/*===========================================================================
  FUNCTION RMNET_SIO_SET_MUX_PORT_CFG()

  DESCRIPTION
    This function sets rmnet mux port configuration.

  PARAMETERS
    rmnet_sio_handle (IN): ptr to rmnet_sio info

  RETURN VALUE
    Failure : -1
    Success :  0

  DEPENDENCIES
    rmnet_sio_init() must have been called previously

  SIDE EFFECTS
    None
===========================================================================*/
boolean rmnet_sio_set_mux_port_cfg
(
  void  * rmnet_sio_handle
);

#endif /* FEATURE_DATA_A2 */

/*===========================================================================
  FUNCTION RMNET_SIO_GET_MASTER_STREAM_ID()

  DESCRIPTION
    This function gets the master data channel stream ID.

  PARAMETERS
    None

  RETURN VALUE
    Stream ID for the master data channel

  DEPENDENCIES
    rmnet_sio_init() must have been called previously

  SIDE EFFECTS
    None
===========================================================================*/
sio_stream_id_type rmnet_sio_get_master_stream_id
(
  void
);

/*===========================================================================
  FUNCTION RMNET_SIO_GET_MASTER_INST()

  DESCRIPTION
    This function returns the master rmnet instance used for muxing.

  PARAMETERS
    None

  RETURN VALUE
    Master rmnet instance used for muxing. 
    RMNET_INSTANCE_MAX if not found

  DEPENDENCIES
    rmnet_sio_init() must have been called previously

  SIDE EFFECTS
    None
===========================================================================*/
rmnet_instance_e_type rmnet_sio_get_master_inst
(
  void
);

/*===========================================================================
FUNCTION RMNET_SIO_GET_UL_DATA_AGG_THRESHOLDS

DESCRIPTION
  This function retrieves UL data aggregation thresholds

PARAMETERS
  rmnet_sio_handle: ptr to rmnet sio info
  ul_agg_max_num:   Max num agg packets
  ul_agg_max_size:  Max agg size 

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
void rmnet_sio_get_ul_data_agg_thresholds
(
  void                     * rmnet_sio_handle,
  uint32                   * ul_agg_max_num,
  uint32                   * ul_agg_max_size  
);

/*===========================================================================
  FUNCTION RMNET_SIO_GET_INFO()

  DESCRIPTION
    This function returns rmnet sio ptr to an rmnet instance.

  PARAMETERS
    rmnet_inst:            rmnet instance

  RETURN VALUE
    rmnet sio info ptr

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
rmnet_sio_info_type  * rmnet_sio_get_info
(
  rmnet_instance_e_type  rmnet_inst
);

#endif /* _DS_RMNET_SIO_H */

