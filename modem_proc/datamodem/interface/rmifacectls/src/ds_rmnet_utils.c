/*===========================================================================

                     D S _ R M N E T _ U T I L S . C
DESCRIPTION

  The Data Services Rm Network interface Utility source file.

EXTERNALIZED FUNCTIONS

Copyright (c) 2012-2013 Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary
===========================================================================*/
/*===========================================================================

                            EDIT HISTORY FOR FILE
  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/rmifacectls/src/ds_rmnet_utils.c#1 $

when        who    what, where, why
--------    ---    ----------------------------------------------------------
04/10/13    wc     MBIM mPDN support
01/28/12    ab     QMI WDA to support 32 filters 
07/26/12    sb     Fixed KW Errors.
02/01/12    cy     Initial implementation
===========================================================================*/

/*===========================================================================

                          INCLUDE FILES FOR MODULE

===========================================================================*/

#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"

#include "amssassert.h"
#include "msg.h"
#include "err.h"
#include "ps_crit_sect.h"
#include "queue.h"
#include "dsm.h"
#include "ps_sys_ioctl.h"

#include "ds_rmnet_sm_ext.h"
#include "ds_rmnet_smi.h"
#include "ds_rmnet_sio.h"
#include "ds_rmnet_sio_rt_acl.h"
#include "ds_rmnet_defs.h"
#include "modem_mem.h"

#include <stringl/stringl.h>

#ifdef FEATURE_DATA_A2
#include "a2_ul_per.h"
#include "a2_dl_per.h"
#endif /* FEATURE_DATA_A2 */


/*===========================================================================

                                EXTERNAL DECLARATIONS

===========================================================================*/

/*===========================================================================

                                DATA TYPES

===========================================================================*/

typedef struct
{
  boolean   used;
  uint8     reserved;
  uint16    id;
  rmnet_utils_packet_filter_type packet_filters;
} rmnet_utils_packet_filter_item_type;

/*===========================================================================

                               DEFINITIONS

===========================================================================*/
#define ASSERT_OR_RET(cond) \
  if (!(cond)) \
  { \
    LOG_MSG_INFO2_0("Unexpected Condition!"); \
    return; \
  }

#define ASSERT_OR_RET_VALUE(cond, ret) \
  if (!(cond)) \
  { \
    LOG_MSG_INFO2_0("Unexpected Condition!"); \
    return ret; \
  }

#define ASSERT_OR_RET_FALSE(cond) \
  ASSERT_OR_RET_VALUE(cond, FALSE)

#define PACKET_FILTER_MAX_NUM   (32)
#define PACKET_FILTER_MAX_SIZE  (128)

static rmnet_utils_packet_filter_item_type packet_filter_items[RMNET_INSTANCE_MAX];

/*===========================================================================

                           FORWARD DECLARATIONS

===========================================================================*/

/*===========================================================================

                        INTERNAL FUNCTION DEFINITIONS

===========================================================================*/
/*===========================================================================
  FUNCTION RMNET_UTILSI_PACKET_FILTER_CLEANUP_NODE()

  DESCRIPTION
    free packet filter rule

  PARAMETERS
    filter_node : pointer to the filter node

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void rmnet_utilsi_packet_filter_cleanup_node
(
  rmnet_utils_packet_filter_node_type *filter_node
)
{
/*-------------------------------------------------------------------------*/
  ASSERT_OR_RET(filter_node);

  if (filter_node->rule.pattern)
  {
    modem_mem_free((filter_node->rule.pattern),MODEM_MEM_CLIENT_DATACOMMON);
  }

  if (filter_node->rule.mask)
  {
    modem_mem_free((filter_node->rule.mask),MODEM_MEM_CLIENT_DATACOMMON);
  }

  modem_mem_free(filter_node,MODEM_MEM_CLIENT_DATACOMMON);

  return;
} /*rmnet_utilsi_packet_filter_cleanup_node()*/

/*===========================================================================
  FUNCTION RMNET_UTILSI_PACKET_FILTER_CLEANUP_LIST()

  DESCRIPTION
    free packet filter list

  PARAMETERS
    filter_list : pointer to the filter list

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void rmnet_utilsi_packet_filter_cleanup_list
(
  rmnet_utils_packet_filter_node_list_type *filter_list
)
{
  rmnet_utils_packet_filter_node_type *filter_node;
/*-------------------------------------------------------------------------*/
  ASSERT_OR_RET(filter_list);

  while (list_size(filter_list) > 0)
  {
    filter_node = 
      (rmnet_utils_packet_filter_node_type *)list_pop_front(filter_list);

    rmnet_utilsi_packet_filter_cleanup_node(filter_node);
  }

  /*Free the list head node here*/
  filter_list->back_ptr = NULL;
  filter_list->front_ptr = NULL;
  filter_list->size = 0;

  return;
} /*rmnet_utilsi_packet_filter_cleanup_list()*/

/*===========================================================================
  FUNCTION RMNET_UTILSI_PACKET_FILTER_COMP_FUNC()

  DESCRIPTION
    Comparison function for filter rule linear search

  PARAMETERS
    item_ptr    : pointer to the filter node
    compare_val : pointer to the rule handle

  RETURN VALUE
    1 if matches, 0 otherwise

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static int rmnet_utilsi_packet_filter_comp_func
( 
  void* item_ptr, 
  void* compare_val
)
{
  rmnet_utils_packet_filter_node_type *filter_node;
  uint32 *handle;
/*-------------------------------------------------------------------------*/
  ASSERT_OR_RET_FALSE(item_ptr);
  ASSERT_OR_RET_FALSE(compare_val);

  filter_node = (rmnet_utils_packet_filter_node_type *) item_ptr;
  handle = (uint32 *)compare_val;

  return (filter_node->handle == *handle);
} /*rmnet_utilsi_packet_filter_comp_func()*/

/*===========================================================================
  FUNCTION RMNET_UTILSI_PACKET_FILTER_PATTERN_MATCH()

  DESCRIPTION
    Match data buffer against one filter patter/mask pair

  PARAMETERS
    buf_size    : size of data buffer to be compared
    buf         : pointer to data buffer to be compared
    filter_size : size of filter pattern/mask
    pattern     : pointer to filter pattern array
    mask        : pointer to filter mask array

  RETURN VALUE
    TRUE if matches, FALSE otherwise

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static boolean rmnet_utilsi_packet_filter_pattern_match
(
  uint16 buf_size,
  uint8 *buf,
  uint8 filter_size,
  uint8 *pattern,
  uint8 *mask
)
{
  uint8 *pdata;
  uint8 i;
  boolean found;

  if (buf_size < filter_size)
  {
    return FALSE;
  }

  //@TODO replace with better String match algorithm if available
  pdata=buf;
  found = TRUE;

  for (i=0; i<filter_size; i++)
  {
    if ((pdata[i] & mask[i]) != pattern[i])
    {
      found = FALSE;
    }
  }
  if (found)
  {
    LOG_MSG_INFO4_3("packet_filters_matched offset %d (%x - %x)",
                      pdata-buf, pattern[0], pattern[filter_size-1]);
    return TRUE;
  }
 
  
  return FALSE;
} /*rmnet_utilsi_packet_filter_pattern_match()*/


/*===========================================================================

                        EXTERNAL FUNCTION DEFINITIONS

===========================================================================*/

/*===========================================================================
  FUNCTION RMNET_UTILS_PACKET_FILTER_SET()

  DESCRIPTION
    Set rmnet packet filters.

  PARAMETERS
    rmnet_instance: rmnet instance
    id:             packet filter item ID

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void rmnet_utils_packet_filter_set
(
  rmnet_instance_e_type rmnet_instance,
  uint16                id
)
{
  rmnet_utils_packet_filter_type *filters = NULL;
/*-------------------------------------------------------------------------*/

  if (rmnet_instance >= RMNET_INSTANCE_MAX)
  {
    ASSERT(0);
    return;
  }

  filters = rmnet_utils_packet_filter_retrieve(id, FALSE);
  rmnet_smi_info[rmnet_instance].meta_sm.packet_filters = filters;

  LOG_MSG_INFO2_2("Set packet filters id 0x%x filters 0x%p", id, filters);

  return;
}

/*===========================================================================
  FUNCTION RMNET_UTILS_PACKET_FILTER_RETRIEVE()

  DESCRIPTION
    Retrieve packet filters based on item ID. Allocate one if not found.

  PARAMETERS
    id:      packet filter item ID
    alloc:   allocate packet filter item if not found

  RETURN VALUE
    Pointer to packet filters. NULL if failed.

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
rmnet_utils_packet_filter_type * rmnet_utils_packet_filter_retrieve
(
  uint16    id,
  boolean   alloc
)
{
  int   i;
  int   empty_index = RMNET_INSTANCE_MAX;
/*-------------------------------------------------------------------------*/

  for (i = 0; i < RMNET_INSTANCE_MAX; i++)
  {
    if (packet_filter_items[i].used == FALSE)
    {
      if (empty_index == RMNET_INSTANCE_MAX)
      {
        empty_index = i;
      }
    }
    else if (packet_filter_items[i].id == id)
    {
      return &packet_filter_items[i].packet_filters;
    } 
  }

  if (alloc)
  {
    if (empty_index < RMNET_INSTANCE_MAX)
    {
      packet_filter_items[empty_index].used = TRUE;
      packet_filter_items[empty_index].id = id;

      // Apply the filter to any applicable RMNET instances
      for (i = 0; i < RMNET_INSTANCE_MAX; i++)
      {
        if (rmnet_smi_info[i].meta_sm.net_params.packet_filter_id == id)
        {
          rmnet_smi_info[i].meta_sm.packet_filters = 
                  &packet_filter_items[empty_index].packet_filters;
        }
      }

      return &packet_filter_items[empty_index].packet_filters;
    }
    else
    {
      LOG_MSG_ERROR_1("Failed to allocate a packet filter item, id %d", id);
    }
  }
  else
  {
    LOG_MSG_INFO2_1("Packet filter item not found, id %d", id);
  }

  return NULL;
}

/*===========================================================================
  FUNCTION RMNET_UTILS_PACKET_FILTER_CHANGE()

  DESCRIPTION
    enable or disable packet filter setting in rmnet

  PARAMETERS
    id         : packet filter item ID
    enabled    : enabled(1) or disabled(0)
    restrictive: Whitelist(1) or blacklist(0) only applies to enable, will be
                 ignored for disable.

  RETURN VALUE
    boolean    : TRUE for success, FALSE otherwise

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean rmnet_utils_packet_filter_change
(
  uint16                id,
  boolean               enabled,
  boolean               restrictive
)
{
  rmnet_utils_packet_filter_type *filters;
  ps_sys_ioctl_in_order_delivery_info in_order_delivery = TRUE;
  int16	ps_errno;
  int i;
/*-------------------------------------------------------------------------*/

  filters = rmnet_utils_packet_filter_retrieve(id, TRUE);
  if (filters == NULL)
  {
    return FALSE;
  }

  filters->enabled = enabled;
  filters->restrictive = restrictive;

  if (enabled == 1)
  {
    /* Switch off A2 Bridge to filter all data packets */	
    if ( 0 != ps_sys_ioctl( PS_SYS_TECH_ALL, 
                            PS_SYS_IOCTL_SWITCH_DATA_PATH_TO_SW,
                            NULL, 
                            &ps_errno) )
   {
     LOG_MSG_ERROR_0("Failed IOCTL SWITCH_DATA_PATH_TO_SW");
   }
  }
  else
  {
    // Check if last filter disable
    for (i = 0; i < RMNET_INSTANCE_MAX; i++)
    {
      if ( (packet_filter_items[i].used == TRUE) &&
           (packet_filter_items[i].packet_filters.enabled) )
      {
        // At least one filter is enabled
        break;
      }
    }

    if (i >= RMNET_INSTANCE_MAX)
    {
      /* Switch A2 Bridge back on when packet filter is disabled */
      if ( 0 != ps_sys_ioctl( PS_SYS_TECH_ALL, 
                              PS_SYS_IOCTL_SWITCH_DATA_PATH_TO_HW,
                              (void*)&in_order_delivery, 
                              &ps_errno) )
      {
   	    LOG_MSG_ERROR_0("Failed IOCTL SWITCH_DATA_PATH_TO_HW");
      }
    }
  }

  LOG_MSG_INFO2_2("packet_filter_change_setting enabled %d restrictive %d",
                  enabled, restrictive);

  return TRUE;
} /*rmnet_utils_packet_filter_change()*/

/*===========================================================================
  FUNCTION RMNET_UTILS_PACKET_FILTER_GET_STATE()

  DESCRIPTION
    retrieve current packet filter setting in rmnet

  PARAMETERS
    id         : packet filter item ID
    enabled    : enabled(TRUE) or disabled(FALSE)
    restrictive: Whitelist(TRUE) or blacklist(FALSE)

  RETURN VALUE
    boolean    : TRUE for success, FALSE otherwise

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean rmnet_utils_packet_filter_get_state
(
  uint16                id,
  uint8                *enabled,
  uint8                *restrictive
)
{
  rmnet_utils_packet_filter_type *filters;
/*-------------------------------------------------------------------------*/

  filters = rmnet_utils_packet_filter_retrieve(id, FALSE);
  if (filters == NULL)
  {
    *enabled = 0;
    *restrictive = 0;
    return TRUE;
  }

  *enabled = filters->enabled;
  *restrictive = filters->restrictive;

  LOG_MSG_INFO2_2("packet_filter_get_setting enabled %d restrictive %d",
                  *enabled, *restrictive);

  return TRUE;
} /*rmnet_utils_packet_filter_get_state()*/

/*===========================================================================
  FUNCTION RMNET_UTILS_PACKET_FILTER_ADD_RULE()

  DESCRIPTION
    Add one filter rule into filter link list

  PARAMETERS
    id       : packet filter item ID
    handle   : pointer to rule handle
    len      : size of filter pattern/mask
    pattern  : pointer to filter pattern array
    mask     : pointer to filter mask array

  RETURN VALUE
    boolean  : TRUE for success, FALSE otherwise

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean rmnet_utils_packet_filter_add_rule
(
  uint16                id,
  uint32               *handle,
  uint8                 len,
  uint8                *pattern,
  uint8                *mask
)
{ 
  rmnet_utils_packet_filter_type           *filters;
  rmnet_utils_packet_filter_node_list_type *filter_list;
  rmnet_utils_packet_filter_node_type      *filter_node;
/*-------------------------------------------------------------------------*/

  ASSERT_OR_RET_FALSE(pattern);
  ASSERT_OR_RET_FALSE(mask);
  ASSERT_OR_RET_FALSE(len <= PACKET_FILTER_MAX_SIZE);

  filters = rmnet_utils_packet_filter_retrieve(id, TRUE);
  if (filters == NULL)
  {
    return FALSE;
  }

  filter_list = &filters->filter_list;

  ASSERT_OR_RET_FALSE(list_size(filter_list) < PACKET_FILTER_MAX_NUM);

  /*create a Node Element*/
  filter_node = (rmnet_utils_packet_filter_node_type *)
    modem_mem_alloc (sizeof(rmnet_utils_packet_filter_node_type),MODEM_MEM_CLIENT_DATACOMMON);

  ASSERT_OR_RET_FALSE(filter_node);

  memset(filter_node, 0, sizeof(rmnet_utils_packet_filter_node_type));

  filter_node->rule.pattern = (uint8 *)modem_mem_alloc(len,MODEM_MEM_CLIENT_DATACOMMON);
  if (!filter_node->rule.pattern)
  {
    LOG_MSG_ERROR_0("rmnet_utils_packet_filter_add_config:unable to malloc memory"
                    "for filter node's pattern\n");
    rmnet_utilsi_packet_filter_cleanup_node(filter_node);
    return FALSE;
  }

  filter_node->rule.mask = (uint8 *)modem_mem_alloc(len,MODEM_MEM_CLIENT_DATACOMMON);
  if (!filter_node->rule.mask)
  {
    LOG_MSG_ERROR_0("rmnet_utils_packet_filter_add_config:unable to malloc memory"
                    "for filter node's mask\n");
    rmnet_utilsi_packet_filter_cleanup_node(filter_node);
    return FALSE;
  }

  filters->max_handle++;
  filter_node->handle = filters->max_handle;
  *handle =  filter_node->handle;

  filter_node->rule.len = len;
  memscpy(filter_node->rule.pattern, len, pattern, len);
  memscpy(filter_node->rule.mask, len, mask, len);

  /*If we reach here, a filter node has been successfully created,
    Insert the node into the list Now*/
  list_push_back(filter_list, &(filter_node->link));

  LOG_MSG_INFO2_2("packet_filter_add_config handle %d len %d", *handle, len);

  return TRUE;
} /*rmnet_utils_packet_filter_add_rule()*/

/*===========================================================================
  FUNCTION RMNET_UTILS_PACKET_FILTER_DELETE_ALL_RULES()

  DESCRIPTION
    Delete all filter rules

  PARAMETERS
    id       : packet filter item ID

  RETURN VALUE
    boolean  : TRUE for success, FALSE otherwise

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean rmnet_utils_packet_filter_delete_all_rules
(
  uint16      id
)
{ 
  rmnet_utils_packet_filter_type *filters;
/*-------------------------------------------------------------------------*/

  filters = rmnet_utils_packet_filter_retrieve(id, FALSE);
  if (filters == NULL)
  {
    return TRUE;
  }

  rmnet_utilsi_packet_filter_cleanup_list(&filters->filter_list);

  filters->max_handle = 0;

  return TRUE;
} /*rmnet_utils_packet_filter_delete_all_rules()*/

/*===========================================================================
  FUNCTION RMNET_UTILS_PACKET_FILTER_DELETE_RULE()

  DESCRIPTION
    Delete one filter rule using rule handle

  PARAMETERS
    id       : packet filter item ID

  RETURN VALUE
    boolean  : TRUE for success, FALSE otherwise

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean rmnet_utils_packet_filter_delete_rule
(
  uint16                id,
  uint32                handle
)
{
  rmnet_utils_packet_filter_type           *filters;
  rmnet_utils_packet_filter_node_list_type *filter_list;
  rmnet_utils_packet_filter_node_type      *filter_node;
/*-------------------------------------------------------------------------*/

  filters = rmnet_utils_packet_filter_retrieve(id, FALSE);
  if (filters == NULL)
  {
    return FALSE;
  }

  filter_list = &filters->filter_list;

  filter_node = (rmnet_utils_packet_filter_node_type *)
    list_linear_search(filter_list,
                       rmnet_utilsi_packet_filter_comp_func, 
                       &handle);
  if (!filter_node)
  {
    LOG_MSG_ERROR_1("packet_filter_delete_rule:unable to find handle %d",
                    handle );

    return FALSE;
  }
  else
  {
    list_pop_item(filter_list, &(filter_node->link));
    rmnet_utilsi_packet_filter_cleanup_node(filter_node);
  }

  return TRUE;
} /*rmnet_utils_packet_filter_delete_rule()*/

/*===========================================================================
  FUNCTION RMNET_UTILS_PACKET_FILTER_GET_RULE_HANDLES()

  DESCRIPTION
    Get the list of rule handles

  PARAMETERS
    id       : packet filter item ID
    p_num    : pointer to number of filters
    p_handle : pointer to array of handles

  RETURN VALUE
    boolean  : TRUE for success, FALSE otherwise

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean rmnet_utils_packet_filter_get_rule_handles
(
  uint16                id,
  uint8                *p_num,
  uint32               *p_handle
)
{
  rmnet_utils_packet_filter_type           *filters;
  rmnet_utils_packet_filter_node_list_type *filter_list;
  rmnet_utils_packet_filter_node_type      *filter_node;
  uint8 i = 0;
/*-------------------------------------------------------------------------*/

  ASSERT_OR_RET_FALSE(p_num);
  ASSERT_OR_RET_FALSE(p_handle);

  filters = rmnet_utils_packet_filter_retrieve(id, FALSE);
  if (filters == NULL)
  {
    *p_num = 0;
    return TRUE;
  }

  filter_list = &filters->filter_list;
  ASSERT_OR_RET_FALSE(list_size(filter_list) <= PACKET_FILTER_MAX_NUM);

  *p_num = list_size(filter_list);

  filter_node = list_peek_front(filter_list);
  while (filter_node != NULL && i < *p_num)
  {
    *p_handle = filter_node->handle;
    LOG_MSG_INFO2_3("packet_filter_get_handle_list index %d handle %d size %d",
                    i, filter_node->handle, *p_num);

    p_handle++;
    i++;
    filter_node = list_peek_next(filter_list, &(filter_node->link));
  }

  if (i < *p_num)
  {
    LOG_MSG_ERROR_2("packet_filter_get_handle_list:unexpected stop at"
                    " %d w total %d\n", i, *p_num);

    //@TODO: delete the whole list and recover.
    rmnet_utilsi_packet_filter_cleanup_list(filter_list);

    return FALSE;
  }

  return TRUE;
} /*rmnet_utils_packet_filter_get_rule_handles()*/

/*===========================================================================
  FUNCTION RMNET_UTILS_PACKET_FILTER_GET_RULE()

  DESCRIPTION
    Get filter rule using rule handle

  PARAMETERS
    id       : packet filter item ID
    handle   : rule handle
    p_buf    : pointer to bytes array in sequence of len/pattern/len/mask

  RETURN VALUE
    boolean  : TRUE for success, FALSE otherwise

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean rmnet_utils_packet_filter_get_rule
(
  uint16                id,
  uint32                handle,
  uint8                *p_buf
)
{
  rmnet_utils_packet_filter_type           *filters;
  rmnet_utils_packet_filter_node_list_type *filter_list;
  rmnet_utils_packet_filter_node_type      *filter_node;
/*-------------------------------------------------------------------------*/

  ASSERT_OR_RET_FALSE(p_buf);

  filters = rmnet_utils_packet_filter_retrieve(id, FALSE);
  if (filters == NULL)
  {
    return FALSE;
  }

  filter_list = &filters->filter_list;

  filter_node = (rmnet_utils_packet_filter_node_type *)
    list_linear_search(filter_list,
                       rmnet_utilsi_packet_filter_comp_func, 
                       &handle);
  if (!filter_node || !filter_node->rule.pattern || !filter_node->rule.mask)
  {
    LOG_MSG_ERROR_1("packet_filter_get_config:unable to find handle %d",
                    handle );

    return FALSE;
  }
  else
  {
    // Patten Len
    *p_buf = filter_node->rule.len;

    //Pattern
    p_buf++;
    memscpy(p_buf, filter_node->rule.len, 
            filter_node->rule.pattern, filter_node->rule.len);

    // Mask Len
    p_buf += filter_node->rule.len;
    *p_buf = filter_node->rule.len;

    // Mask 
    p_buf++;
    memscpy(p_buf, filter_node->rule.len, 
            filter_node->rule.mask, filter_node->rule.len);

    LOG_MSG_INFO2_2("packet_filter_get_config handle %d len %d",
                    handle, filter_node->rule.len);
  }

  return TRUE;
} /*rmnet_utils_packet_filter_get_rule()*/

/*===========================================================================
  FUNCTION RMNET_UTILS_PACKET_FILTER_MATCH()

  DESCRIPTION
    Match data buffer against one filter patter/mask pair

  PARAMETERS
    filter_list   : pointer to filter list
    item_head_ptr : pointer to dsm item containing data packet buffer

  RETURN VALUE
    TRUE if matches, FALSE otherwise

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean rmnet_utils_packet_filter_match
(
  rmnet_utils_packet_filter_node_list_type *filter_list,
  dsm_item_type **                          item_head_ptr
)
{
  uint8 *databuf;

  uint16 len = 0;
  dsm_item_type *p_dsm;
  rmnet_utils_packet_filter_node_type *filter_node;
  p_dsm = *item_head_ptr;

  /* Pkt filter is for low throughput traffic only, so copy dsm
     data to contingent buffer for easy processing.
     @TODO efficiency could be improved without copying */
  databuf = modem_mem_alloc(dsm_length_packet(*item_head_ptr),MODEM_MEM_CLIENT_DATACOMMON);
  if (!databuf)
  {
    /* forward packets to host if memory allocation fails */
    return TRUE;
  }

  do
  {
    memscpy(databuf+len, p_dsm->used, p_dsm->data_ptr, p_dsm->used);
    len += p_dsm->used;

    p_dsm = p_dsm->pkt_ptr;
  }while(p_dsm);

  filter_node = list_peek_front(filter_list);
  while (filter_node != NULL)
  {
    LOG_MSG_INFO2_1("packet_filters_match handle %d", filter_node->handle);

    if(rmnet_utilsi_packet_filter_pattern_match(
        len, databuf, filter_node->rule.len, 
        filter_node->rule.pattern, filter_node->rule.mask))
    {
      LOG_MSG_INFO4_2("packet_filters_matched handle %d len %d",
                      filter_node->handle, len);
      modem_mem_free(databuf, MODEM_MEM_CLIENT_DATACOMMON);
      return TRUE;
    }

    filter_node = list_peek_next(filter_list, &(filter_node->link));
  }
  
  modem_mem_free(databuf, MODEM_MEM_CLIENT_DATACOMMON);
  return FALSE;
} /*rmnet_utils_packet_filter_match()*/
