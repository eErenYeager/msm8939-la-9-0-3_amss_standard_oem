/*===========================================================================

                         D S _ R M N E T _ S I O . C

DESCRIPTION

  Rm Network device - SIO interface source file.

  Handles data channel setup and data transfer for the Rm Network device.

EXTERNALIZED FUNCTIONS

  rmnet_sio_init()
    Initialize the specified RmNetSio instance and link it to the RDM
    service specified.
  rmnet_sio_transmit()
    Send data in 'tx_item' over the data channel associated with provided
    rmnetsio handle 'rmnet_sio_handle'
  rmnet_sio_receive()
    Get data from the rx watermark for the data channel associated with
    provided rmnetsio handle 'rmnet_sio_handle'
  rmnet_sio_set_dtr_handler()
    Specify the callback function that should be invoked when the DTR state
    changes on the specifed rmnetsio data link.
  rmnet_sio_set_rx_cb()
    Specify the callback function that should be invoked when the DTR state
    changes on the specifed rmnetsio data link.
  rmnet_sio_set_rx_flow()
    Flow control the dsnetsio link specified by rmnet_sio_handle.
  rmnet_sio_clear_wmk_queues()
    Clear any data present in the rx and tx wmks.

Copyright (c) 2004-2013 Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary
===========================================================================*/
/*===========================================================================

                      EDIT HISTORY FOR FILE

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/rmifacectls/src/ds_rmnet_sio.c#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when        who    what, where, why
--------    ---    ----------------------------------------------------------
09/24/13    sah    Fix for KW errors
03/21/13    wc     QMAP support
01/12/12    wc     Call a2 tlp functions only for a2 port
11/02/11    wc     Add DTR on SIO control channel feature
10/04/11    sa     Q6 free floating changes for QMI.
09/07/11    pgm    Removed rmnet_sio_info_type structure definition and moved to
                   ds_rmnet_sio.h.
08/10/11    wc     Data aggregation based on CPU utilizatio
05/18/11    sha    fix for the initilization of info ptr in HI water mark function.
05/02/11    ua     Enabling Autoconnect to working with first instance across
                   all targets.
05/09/11    sha    fix for aggregate identical back-to-back tx flow events in RMNET
03/09/11    kk     Removing DCC command buffer dependency from RmNet.
02/09/11    kk     Enabling DROP_PENDING on all opened SMD ports.
01/10/11    ss     Cleaning up of Globals for thread safety in Q6 Free
                   Floating environment.
10/23/09    ar     Add support port configuration from NVRAM.
02/19/09    am     DS Task De-coupling effort and introduction of DCC task.
07/02/07    nl     Changes to map SIO_PORT_SMD_WINMOB_QMI_WWAN for 3rd party OS builds
                   in qmi/rmnet code instead of relying on external modules' featurization
01/08/07    jd     Added Winmobile multiple RmNet support
10/09/06    ks     Added support to flow control rmnet iface based on tx wmk.
09/21/06    ks     Added rmnet_sio_clear_wmk_queues() to clear tx and rx
                   wmks when rmnet_sm transitions to NULL.
08/23/06    ks     Changes to support multiple rmnet instances.
03/07/06    jd     Add TLP support
02/22/06    ks     Added rmnet_sio_verify_dtr_high() to check if dtr is high.
10/10/05    ks     Removed SIO DATA WWAN RDM device.
05/31/05    jd     Code review updates
11/21/04    jd     Defer QMI initialization until after RDM open command.
                   Reject RDM close - once mapped, QMI will not give up
                   network device.
05/11/05    ks     Changed include file name.
03/14/05    ks     Removed featurization
03/08/05    ks     name changed from ds_netsio.c
11/21/04    jd     Created module
===========================================================================*/

/*===========================================================================

                                INCLUDE FILES

===========================================================================*/

#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"

/* This should be the first include to allow stubs on test framework */
#if defined(T_WINNT)
#error code not present
#endif /* WINNT*/

#define FEATURE_DSM_WM_CB

#include "amssassert.h"
#include "msg.h"
#include "err.h"
#include "rex.h"
#include "sio.h"
#include "task.h"
#include "dcc_task_defs.h"
#include "dcc_task_svc.h"

#include "ds_rmnet_sio.h"
#include "ds_rmnet_defs.h"
#include "ds_rmnet_smi.h"
#include "ds_rev_ip_transport_hdlr.h"

#include "ds_qmux.h"
#include "ds_qmi_ctl.h"
#include "ds_qmi_dms.h"
#include "ds_qmi_wds.h"

#include "ps_crit_sect.h"
#include "ds_Utils_DebugMsg.h"
#include "ps_system_heap.h"

#ifdef FEATURE_DATA_A2
#include "a2_dl_per.h"
#include "a2_ul_per.h"
#endif /* FEATURE_DATA_A2 */

/*===========================================================================

                               DEFINITIONS

===========================================================================*/


/*===========================================================================

                               INTERNAL DATA

===========================================================================*/
static rmnet_sio_info_type rmnet_sio_info[RMNET_INSTANCE_MAX] =
{
  { 0, }, // TODO - if this works, if so remove memset in init
};


/* This info to instance conversion is valid only if there is a 1-1
     mapping between SIO and QMI instance */
#define RMNET_SIO_INFO_TO_INSTANCE(info) \
                               (rmnet_instance_e_type)(info - rmnet_sio_info)

extern ps_crit_sect_type rmnet_crit_section;
#ifdef FEATURE_RMNET_DATA_AGG_TIMER

#define RMNET_DATA_AGG_FIRST_NV_ITEM 5
#define RMNET_DATA_AGG_DEFAULT_TIMER_VALUE 5

/*----------------------------------------------------------------------
  Is data aggregation enabled
  May be changed in ds_rmnet_meta_sm.c (in DCC task)
  If FC is not available (CDMA only build), always enable the data
  aggregation (This is the old behavior). If FC is available, set
  default to false and it will be enabled if cpu utilization is high.
-----------------------------------------------------------------------*/
#ifdef FEATURE_CPU_BASED_FLOW_CONTROL
boolean rmnet_sio_da_enabled = FALSE;
#else
boolean rmnet_sio_da_enabled = TRUE;
#endif /* FEATURE_CPU_BASED_FLOW_CONTROL */

LOCAL void rmnet_smd_delay_timer_expired(timer_cb_data_type user_data)
{
  sio_ioctl_param_type   wait_param;
  rmnet_sio_info_type *  info;

  info = (rmnet_sio_info_type *) user_data;
  wait_param.wm_wait = FALSE;
  sio_ioctl(info->stream_id, SIO_IOCTL_WM_SET_WAIT, &wait_param);
  LOG_MSG_INFO2_1( "rmnet_smd_delay_timer_expired for stream-id %d",
                   info->stream_id );
}
#endif /* FEATURE_RMNET_DATA_AGG_TIMER */
/*===========================================================================

                            FORWARD DECLARATIONS

===========================================================================*/
static boolean rmnet_sio_open
(
  rmnet_sio_info_type * info,
  sio_port_id_type  port_id
);
static boolean rmnet_sio_close
(
  rmnet_sio_info_type * info
);
static void  rmnet_sio_default_rx_cb
(
  dsm_watermark_type* rx_wm_ptr,
  void* rx_wm_user_data
);
static void rmnet_sio_dtr_cb(void *);
static void rmnet_sio_setup_sio_watermarks
(
  rmnet_sio_info_type * info
);
static void rmnet_sio_rx_lowater_func
(
  struct dsm_watermark_type_s *wm_ptr,
  void *user_data_ptr
);
static void rmnet_sio_rx_hiwater_func
(
  struct dsm_watermark_type_s *wm_ptr,
  void *user_data_ptr
);

static void rmnet_sio_tx_lowater_func
(
  struct dsm_watermark_type_s *wm_ptr,
  void *user_data_ptr
);

static void rmnet_sio_tx_hiwater_func
(
  struct dsm_watermark_type_s *wm_ptr,
  void *user_data_ptr
);

#ifdef FEATURE_RMNET_DATA_AGG_TIMER
static void rmnet_sio_tx_non_empty_func
(
  struct dsm_watermark_type_s *wm_ptr,
  void *user_data_ptr
);
#endif /* FEATURE_RMNET_DATA_AGG_TIMER */

#ifndef FEATURE_DATA_RM_NET_USES_SM
static void rmnet_sioi_tlp_cb
(
  sio_ioctl_usb_tlp_settings_type  current_setting,
  void *                           user_data
);
#endif /* FEATURE_DATA_RM_NET_USES_SM */

void rmnet_sio_set_tx_flow
(
  void *  rmnet_sio_handle,
  uint32  mask,
  boolean disable,
  void *  user_data_ptr
);


/*===========================================================================

                             EXTERNAL FUNCTIONS

===========================================================================*/

/*===========================================================================
  FUNCTION RMNET_SIO_INIT()

  DESCRIPTION
    Initialize the specified RmNetSio instance.

    Only one caller can own the rmnetsio instance at a time.  Initialization
    of all rmnet_sio instances is assumed to take place in the same task for
    now, hence no tasklock/taskfree is required to avoid contention.

  PARAMETERS
    qmi_instance : instance of QMI to register

  RETURN VALUE
    RmNetSIO handle - opaque handle to this rmnetsio instance
    NULL if initialization fails

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void *  rmnet_sio_init
(
  rmnet_instance_e_type  instance
)
{
  rmnet_sio_info_type *  info;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT (instance < RMNET_INSTANCE_MAX);

  info = &rmnet_sio_info[instance];

  if (info->inited)
  {
    LOG_MSG_INFO1_1 ("RmNetsio instance %d already initialized!", instance);
    return info;
  }

  /*-------------------------------------------------------------------------
    Clear this control block

    TODO - if info block is already zeroed by static initalizer, remove
    memset below
  -------------------------------------------------------------------------*/
  memset( info, 0, sizeof(rmnet_sio_info_type) );

  /*-------------------------------------------------------------------------
    Initialize the watermarks and queues used for USB network interface.
    RX Do not exceed level is set at top of the file, TX is infinite.
  -------------------------------------------------------------------------*/
  dsm_queue_init( &info->tx_wmk,
                  RMNET_TX_WM_DNE,
                  &info->tx_wmk_q );
  dsm_queue_init( &info->rx_wmk,
                  RMNET_RX_WM_DNE,
                  &info->rx_wmk_q );
  rmnet_sio_setup_sio_watermarks( info );

  info->inited = TRUE;
  return info;
} /* rmnet_sio_init() */



/*===========================================================================
  FUNCTION RMNET_SIO_DEACTIVATE_INSTANCE()

  DESCRIPTION
    Uninitialize the specified RmNetSio instance.

  PARAMETERS
    instance : instance of QMI to register

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean rmnet_sio_deactivate_instance
(
  void *handle
)
{
  rmnet_sio_info_type *  info;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT (handle != NULL);

  info = (rmnet_sio_info_type *)handle;

  if (!info->inited)
  {
    LOG_MSG_INFO1_0 ("RmNetsio instance not initialized!");
    return FALSE;
  }

  // close the sio stream
#ifndef FEATURE_DATA_QMI_DTR_ON_CTRL_CHANNEL
  rmnet_sio_set_dtr_handler( info, NULL, NULL );
#endif /* FEATURE_DATA_QMI_DTR_ON_CTRL_CHANNEL */
  rmnet_sio_set_tlp_change_cb( info, NULL, NULL );
  rmnet_sio_clear_wmk_queues( info );

  if (!rmnet_sio_close(info))
  {
    LOG_MSG_INFO1_0 ("RmNetsio instance not could not be closed!");
    return FALSE;
  }

  return TRUE;
}


/*===========================================================================
  FUNCTION RMNET_SIO_ACTIVATE_INSTANCE()

  DESCRIPTION
    Activates the specified RmNetSio instance (i.e. opens SIO streams)

  PARAMETERS
    handle : handle to the instance of RmNET to activate
    port_id :  SIO port identifier

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean rmnet_sio_activate_instance
(
  void *  handle,
  uint16  port_id
)
{
  rmnet_sio_info_type *  info;
  rmnet_instance_e_type  instance;
#ifndef FEATURE_DATA_RM_NET_USES_SM
  sio_ioctl_param_type   ioctl_param;
#endif /* FEATURE_DATA_RM_NET_USES_SM */
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT (handle != NULL);
  info = (rmnet_sio_info_type *)handle;

  if (!info->inited)
  {
    LOG_MSG_INFO1_0 ("RmNetsio instance not initialized!");
    return FALSE;
  }

  instance = RMNET_SIO_INFO_TO_INSTANCE(info);
  ASSERT (instance < RMNET_INSTANCE_MAX);

  info->sio_port = (sio_port_id_type)port_id;
  if (FALSE == rmnet_sio_open(info, info->sio_port ))
  {
    LOG_MSG_ERROR_1 ("Can't open the SIO link! (%x)", info);
    return FALSE;
  }

  /*-----------------------------------------------------------------------
    Register callback function for change in TLP setting.
  -----------------------------------------------------------------------*/
#ifndef FEATURE_DATA_RM_NET_USES_SM
  ioctl_param.tlp_change_cb.callback  = rmnet_sioi_tlp_cb;
  ioctl_param.tlp_change_cb.user_data = info;
  if(info->stream_id >= SIO_MAX_STREAM)
  {
    LOG_MSG_ERROR_1("Invalid SIO stream ID %d",info->stream_id);
    return FALSE;
  }

  sio_ioctl( info->stream_id, SIO_IOCTL_REG_TLP_CHANGE_CB, &ioctl_param );
#endif /* FEATURE_DATA_RM_NET_USES_SM */

  return TRUE;
}


#ifdef FEATURE_DATA_RMNET_USB_STANDARD_ECM
/*===========================================================================
FUNCTION RMNET_SIO_LINK_UP_NOTIFY()

DESCRIPTION
To check if DTR is high or low.

PARAMETERS
rmnet_sio_handle : ptr to rmnet_sio info

RETURN VALUE
boolean : TRUE  = DTR is high
FALSE = DTR is low

DEPENDENCIES
rmnet_sio_init() must have been called previously

SIDE EFFECTS
None
===========================================================================*/
void rmnet_sio_link_up_notify
(
 void * rmnet_sio_handle,
 boolean link_up
 )
{
  sio_ioctl_param_type   ioctl_param;
  rmnet_sio_info_type *  info;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(rmnet_sio_handle);
  info = (rmnet_sio_info_type *) rmnet_sio_handle;

  if (info->inited == FALSE)
  {
    LOG_MSG_ERROR_0 ("Device not yet initialized!");
    return ;
  }

  ioctl_param.link_up = link_up;

  sio_ioctl(info->stream_id, SIO_IOCTL_NETWORK_CONNECTION_NOTIF, &ioctl_param);

  /* Also send connection speed change when link goes UP */
  if(link_up == TRUE)
  {
    /* For now hardcode Max rates - To Do need to get from MH */
    ioctl_param.connection_speed_change_notif.upstream_bitrate = 5700000;
    ioctl_param.connection_speed_change_notif.downstream_bitrate = 28000000;

    sio_ioctl( info->stream_id,
      SIO_IOCTL_CONNECTION_SPEED_CHANGE_NOTIF,
      &ioctl_param );
  }
} /* rmnet_sio_link_up_notify() */



/*===========================================================================
FUNCTION RMNET_SIO_IOCTL()

DESCRIPTION
To call IOCTLs into SIO.

PARAMETERS
rmnet_sio_handle : ptr to rmnet_sio info

RETURN VALUE
boolean : TRUE  = DTR is high
FALSE = DTR is low

DEPENDENCIES
rmnet_sio_init() must have been called previously

SIDE EFFECTS
None
===========================================================================*/
void rmnet_sio_ioctl
(
  void *            rmnet_sio_handle,
  sio_ioctl_cmd_type cmd,
  sio_ioctl_param_type *param
)
{
  rmnet_sio_info_type *  info;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if(!rmnet_sio_handle)
  {
    LOG_MSG_ERROR_0 ("Can't send data on control interface - Netsio not initialized!");
    return;
  }

  /*-------------------------------------------------------------------------
  If rmnet_sio_handle is non-null, the netsio instance must have been
  initialized, hence no need to validate that.
  -------------------------------------------------------------------------*/
  info = (rmnet_sio_info_type *) rmnet_sio_handle;
  sio_ioctl( info->stream_id,
    cmd, param );
}
#endif


/*===========================================================================
  FUNCTION RMNET_SIO_TRANSMIT()

  DESCRIPTION
    Send data in 'tx_item' over the data channel associated with provided
    rmnetsio handle 'rmnet_sio_handle'

  PARAMETERS
    rmnet_sio_handle :  handle returned by rmnet_sio_init()
    tx_item          :  data to be transmitted

  RETURN VALUE
    None

  DEPENDENCIES
    rmnet_sio_init() must have been called previously

  SIDE EFFECTS
    None
===========================================================================*/
void  rmnet_sio_transmit
(
  void *            rmnet_sio_handle,
  dsm_item_type **  tx_item
)
{
  rmnet_sio_info_type *  info;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if(!rmnet_sio_handle)
  {
    LOG_MSG_ERROR_0 ("Can't send data on control interface - Netsio not initialized!");
    dsm_free_packet( tx_item );
    return;
  }

  /*-------------------------------------------------------------------------
    If rmnet_sio_handle is non-null, the netsio instance must have been
    initialized, hence no need to validate that.
  -------------------------------------------------------------------------*/
  info = (rmnet_sio_info_type *) rmnet_sio_handle;

  sio_transmit( info->stream_id, *tx_item );
  *tx_item = NULL;

} /* rmnet_sio_transmit() */



/*===========================================================================
  FUNCTION RMNET_SIO_RECEIVE()

  DESCRIPTION
    Get data from the rx watermark for the data channel associated with
    provided rmnetsio handle 'rmnet_sio_handle'

  PARAMETERS
    rmnet_sio_handle :  handle returned by rmnet_sio_init()

  RETURN VALUE
    DSM item pointer contained received packet

  DEPENDENCIES
    rmnet_sio_init() must have been called previously

  SIDE EFFECTS
    None
===========================================================================*/
dsm_item_type *  rmnet_sio_receive
(
  void *  rmnet_sio_handle
)
{
  rmnet_sio_info_type *  info;
  dsm_item_type *        retval;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(rmnet_sio_handle);
  info = (rmnet_sio_info_type *) rmnet_sio_handle;

  retval = dsm_dequeue( &info->rx_wmk );
  LOG_MSG_INFO3_1 ("Received %d byte packet over RMNET port",
                   dsm_length_packet(retval));

  return retval;

} /* rmnet_sio_receive() */



/*===========================================================================
  FUNCTION RMNET_SIO_SET_TLP_CHANGE_CB()

  DESCRIPTION
    Specify the callback function that should be invoked when the USB TLP
    settings change

  PARAMETERS
    rmnet_sio_handle :  handle to dsnetsio link returned by rmnet_sio_init()
    tlp_cb           :  callback function to be invoked when TLP use changes
                        Arguments to callback will be:
                          dl_ver => negotiated version of TLP for downlink
                          ul_ver => negotiated version of TLP for uplink
                          void * => user data provided as 3rd arg to
                                    rmnet_sio_set_tlp_change_cb()

  RETURN VALUE
    None

  DEPENDENCIES
    rmnet_sio_init() must have been called previously

  SIDE EFFECTS
    None
===========================================================================*/
void  rmnet_sio_set_tlp_change_cb
(
  void *  rmnet_sio_handle,
  void (* tlp_cb)( rmnet_sio_tlp_ver_type, rmnet_sio_tlp_ver_type, void * ),
  void *  user_data
)
{
  void (* prev_cb)( rmnet_sio_tlp_ver_type, rmnet_sio_tlp_ver_type, void * );
  rmnet_sio_info_type *  info;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(rmnet_sio_handle != NULL);

  info = (rmnet_sio_info_type *) rmnet_sio_handle;

  /*-------------------------------------------------------------------------
    save the callback and user data passed by the caller, to be invoked
    when the use of TLP negotiated over USB changes
  -------------------------------------------------------------------------*/
  PS_ENTER_CRIT_SECTION(&rmnet_crit_section);
  prev_cb             = info->tlp_cb;
  info->tlp_cb        = tlp_cb;
  info->tlp_user_data = user_data;
  PS_LEAVE_CRIT_SECTION(&rmnet_crit_section);

  /*-------------------------------------------------------------------------
   if the callback has changed, invoke the callback to tell the caller
   the current state of the TLP settings
  -------------------------------------------------------------------------*/
  if( (prev_cb != tlp_cb) && info->tlp_cb )
  {
    info->tlp_cb( info->tlp_setting.downlink_tlp_on ?
                    (rmnet_sio_tlp_ver_type)info->tlp_setting.version : RMNET_SIO_TLP_VER_NONE,
                  info->tlp_setting.uplink_tlp_on ?
                    (rmnet_sio_tlp_ver_type)info->tlp_setting.version : RMNET_SIO_TLP_VER_NONE,
                  info->tlp_user_data );
  }

} /* rmnet_sio_set_tlp_change_cb() */



/*===========================================================================
  FUNCTION RMNET_SIO_SET_DTR_HANDLER()

  DESCRIPTION
    Specify the callback function that should be invoked when the DTR state
    changes on the specifed rmnetsio data link.

  PARAMETERS
    rmnet_sio_handle :  handle to dsnetsio link returned by rmnet_sio_init()
    dtr_cb           :  callback function to be invoked when DTR changes
                        First argument to callback will be DTR state -
                        TRUE = asserted, FALSE = deasserted

                        FUTURE:  void (* dtr_cb)( boolean, void *user_data )

  RETURN VALUE
    None

  DEPENDENCIES
    rmnet_sio_init() must have been called previously

  SIDE EFFECTS
    None
===========================================================================*/
void  rmnet_sio_set_dtr_handler
(
  void *                   rmnet_sio_handle,
  rmnet_sio_dtr_cb_f_type  dtr_cb,
  void *                   user_data
)
{
  rmnet_sio_info_type *  info;
  sio_ioctl_param_type   ioctl_param;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(rmnet_sio_handle);

  info = (rmnet_sio_info_type *) rmnet_sio_handle;
  if (info->inited == FALSE)
  {
    LOG_MSG_ERROR_0 ("Device not yet initialized!");
    ASSERT(0);
  }

  info->dtr_cb = dtr_cb;
  info->dtr_cb_user_data = user_data;
  ioctl_param.enable_dte_ready_event_ext.cb_data = info;

  if (dtr_cb)
  {
    /*-----------------------------------------------------------------------
      Register callback functions for changes in DTR state.

      TODO - get SIO to take user_data with this so same callback can be
             used for multiple rmnetsio instances.
             For now, DTR event callback assumes instance 0.

      FUTURE:  ioctl_param.enable_dte_ready_event.user_data = info;
    -----------------------------------------------------------------------*/
    ioctl_param.enable_dte_ready_event_ext.cb_func = rmnet_sio_dtr_cb;
    sio_ioctl(info->stream_id,
              SIO_IOCTL_ENABLE_DTR_EVENT_EXT,
              &ioctl_param);

    /* workaround - today SMD does not generate a cback, if it has got DTR from
       usb before the client registers the cback. Next cback will be called by SMD
       only on change of DTR. For now, call the cback within rmnet to handle
       the case of autoconnect on power up with cable connected */
    rmnet_sio_dtr_cb(info);
  }
  else
  {
    ioctl_param.enable_dte_ready_event_ext.cb_func = NULL;
    sio_ioctl(info->stream_id,
              SIO_IOCTL_DISABLE_DTR_EVENT_EXT,
              &ioctl_param );
  }
} /* rmnet_sio_set_dtr_handler() */



/*===========================================================================
  FUNCTION RMNET_SIO_SET_RX_CB()

  DESCRIPTION
    Specify the callback function that should be invoked when data arrives
    on the data path associated with rmnet_sio_handle

  PARAMETERS
    rmnet_sio_handle :  handle to dsnetsio link returned by rmnet_sio_init()
    rx_cb            :  callback to be called when data arrives on the data
                        path associated with rmnet_sio_handle
                        1st param = dsm pointer to received packet
                        2nd param = user_data provided
    user_data        :  void * to be passed in the 2nd parameter each time
                        rx_cb is invoked

  RETURN VALUE
    None

  DEPENDENCIES
    rmnet_sio_init() must have been called previously

  SIDE EFFECTS
    Clears any data previously queued in the rx watermark
===========================================================================*/
void  rmnet_sio_set_rx_cb
(
  void *  rmnet_sio_handle,
  void (* rx_cb)( dsm_watermark_type *, void * ),
  void *  user_data
)
{
  rmnet_sio_info_type *  info;
  uint32                 bytes_cleared = 0;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(rmnet_sio_handle);
  info = (rmnet_sio_info_type *) rmnet_sio_handle;

  /*-------------------------------------------------------------------------
    user data is passed back to the rx_cb as the second parameter (void *)
    to allow the upper layer to distinguish between multiple instances of
    rmnet_sio.
  -------------------------------------------------------------------------*/

  /*-------------------------------------------------------------------------
    If passed receive callback is null, set the default rx callback.
  -------------------------------------------------------------------------*/
  info->rx_wmk.non_empty_func_ptr   = rx_cb ? rx_cb
                                            : rmnet_sio_default_rx_cb;
  info->rx_wmk.non_empty_func_data  = user_data;

  /*-------------------------------------------------------------------------
    When changing the receive function, it is important to clear any
    outstanding data as by changing the receive callback, the caller is
    interrupting the normal data flow and hence the previously registered
    logic will no longer be invoked to handle the previously received data.
  -------------------------------------------------------------------------*/
  if(info->rx_wmk.current_cnt != 0)
  {
    bytes_cleared = info->rx_wmk.current_cnt;
    dsm_empty_queue(&info->rx_wmk);
  }

  if (bytes_cleared)
  {
    LOG_MSG_INFO1_2("Cleared %d bytes from Rx WM 0x%x",
                    bytes_cleared, &info->rx_wmk);
  }
} /* rmnet_sio_set_rx_cb() */



/*===========================================================================
  FUNCTION RMNET_SIO_SET_RX_FLOW()

  DESCRIPTION
    Flow control the dsnetsio link specified by rmnet_sio_handle.

    The reason for the flow is specified by 'mask'.  Whether to enable (0)
    or disable (1) flow is specied by 'disable' parameter.  Caller must
    enable the flow with the same mask that was used to disable.

  PARAMETERS
    rmnet_sio_handle :  handle to dsnetsio link returned by rmnet_sio_init()
    mask             :  Bitmask indicating the reason for flow control
                        (allows disabling flow if any of various reasons
                         represented by separate bits has been specified)
    disable          :  FALSE = enable flow for reasons in 'mask'
                        TRUE  = disable flow for reasons in 'mask'

  RETURN VALUE
    None

  DEPENDENCIES
    rmnet_sio_init() must have been called previously

  SIDE EFFECTS
    None
===========================================================================*/
void rmnet_sio_set_rx_flow
(
  void *  rmnet_sio_handle,
  uint32  mask,
  boolean disable
)
{
  rmnet_sio_info_type *  info;
  sio_ioctl_param_type   ioctl_param;
  boolean                qmap_fc = FALSE;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(rmnet_sio_handle);
  info = (rmnet_sio_info_type *) rmnet_sio_handle;

  /*-------------------------------------------------------------------------
    Set bits specified by mask in flow state if disable.
    Clear bits specified by mask in flow state if enable (!disable).
    If mask is null, return immediately without change.
  -------------------------------------------------------------------------*/
  if (!mask)
  {
    LOG_MSG_ERROR_0( "set_rx_flow called with 0 mask!");
    return;
  }

  /*-------------------------------------------------------------------------
   * Don't do HW flow control if QMAP muxing and in-band flow control
   * is enabled. These settings can only be changed while not in a data
   * call so no race conditions.
  -------------------------------------------------------------------------*/
#ifdef FEATURE_DATA_A2
  if ( (mask == RMNET_SIO_FLOWMASK_MEM) &&
#ifndef FEATURE_RMNET_PORT_CONFIG_MSM
       (info->mux_port_cfg.mux_type == A2_MUX_QMAP) &&
#endif
       (info->qmap_settings.in_band_fc == TRUE) )
  {
    // Don't do ioctl, but do qmi/qmap flow control
    qmap_fc = TRUE;
  }
#endif /* FEATURE_DATA_A2 */

  PS_ENTER_CRIT_SECTION(&rmnet_crit_section);
  if (disable)
  {
    /*-----------------------------------------------------------------------
      If flow was previously enabled, send the ioctl to disable flow
      through the device now.  If flow was previously disabled, do nothing
      as we're already flow controlled.
    -----------------------------------------------------------------------*/
    if (RMNET_SIO_FLOWMASK_NONE == info->rx_flow_mask)
    {
      if (qmap_fc)
      {
        rmnet_meta_sm_rx_wmk_fc(
                 RMNET_SIO_INFO_TO_INSTANCE(info),
                 TRUE);
      }
      else
      {
        sio_ioctl( info->stream_id,
                   SIO_IOCTL_INBOUND_FLOW_DISABLE,
                   &ioctl_param );
      }
    }

    /*-----------------------------------------------------------------------
      Set the passed bitmask from the rmnetsio instance flow control mask
    -----------------------------------------------------------------------*/
    info->rx_flow_mask |= mask;
  }
  else /* enable */
  {
    /*-----------------------------------------------------------------------
      Clear the passed bitmask from the rmnetsio instance flow control mask
    -----------------------------------------------------------------------*/
    info->rx_flow_mask &= ~(mask);

    /*-----------------------------------------------------------------------
      If flow was previously disabled and is now enabled, send the ioctl to
      enable flow through the device now.  If flow was previously enabled,
      do nothing as we're already flowing.
    -----------------------------------------------------------------------*/
    if (RMNET_SIO_FLOWMASK_NONE == info->rx_flow_mask)
    {
      if (qmap_fc)
      {
        rmnet_meta_sm_rx_wmk_fc(
                 RMNET_SIO_INFO_TO_INSTANCE(info),
                 FALSE);
      }
      else
      {
        sio_ioctl( info->stream_id,
                   SIO_IOCTL_INBOUND_FLOW_ENABLE,
                   &ioctl_param );
      }
    }
  }
  PS_LEAVE_CRIT_SECTION(&rmnet_crit_section);

  LOG_MSG_INFO2_3( "RX FLOW CTRL MASK:  %c0x%x = fctl mask 0x%x",
                   disable ? '+' : '-', mask, info->rx_flow_mask );

} /* rmnet_sio_set_rx_flow() */



/*===========================================================================
  FUNCTION RMNET_SIO_CHECK_STATUS()

  DESCRIPTION
    To check if the Rmnet sio link was initalized.

  PARAMETERS
    rmnet_sio_handle :  handle to rmnetsio link returned by rmnet_sio_init()

  RETURN VALUE
    boolean : TRUE  = Link was intialized
              FALSE = Link wasn't initialized

  DEPENDENCIES

  SIDE EFFECTS
    None
===========================================================================*/
boolean rmnet_sio_check_status
(
  void *  rmnet_sio_handle
)
{
  rmnet_sio_info_type *  info;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(rmnet_sio_handle);
  info = (rmnet_sio_info_type *) rmnet_sio_handle;

  return (info->inited);
} /* rmnet_sio_check_status */



/*===========================================================================
  FUNCTION RMNET_SIO_VERIFY_DTR_HIGH()

  DESCRIPTION
    To check if DTR is high or low.

  PARAMETERS
    rmnet_sio_handle : ptr to rmnet_sio info

  RETURN VALUE
    boolean : TRUE  = DTR is high
              FALSE = DTR is low

  DEPENDENCIES
    rmnet_sio_init() must have been called previously

  SIDE EFFECTS
    None
===========================================================================*/
boolean rmnet_sio_verify_dtr_high
(
  void * rmnet_sio_handle
)
{
  sio_ioctl_param_type   ioctl_param;
  rmnet_sio_info_type *  info;
  boolean                dtr_status;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(rmnet_sio_handle);
  info = (rmnet_sio_info_type *) rmnet_sio_handle;

  if (info->inited == FALSE)
  {
    LOG_MSG_ERROR_0 ("Device not yet initialized!");
    return FALSE;
  }

  dtr_status = FALSE;
  ioctl_param.dte_ready_asserted = &dtr_status;

  sio_ioctl(info->stream_id, SIO_IOCTL_DTE_READY_ASSERTED, &ioctl_param);

  LOG_MSG_INFO1_2("SIO Status : Stream ID [%d] DTR Status[%d]",
                  info->stream_id, dtr_status);
  return (dtr_status);
} /* rmnet_sio_verify_dtr_high() */



/*===========================================================================
  FUNCTION RMNET_SIO_CLEAR_WMK_QUEUES()

  DESCRIPTION
    Clear any data present in the rx and tx wmks.

  PARAMETERS
    rmnet_sio_handle : ptr to rmnet_sio info

  RETURN VALUE
    None

  DEPENDENCIES
    rmnet_sio_init() must have been called previously

  SIDE EFFECTS
    None
===========================================================================*/
void rmnet_sio_clear_wmk_queues
(
  void * rmnet_sio_handle
)
{
  rmnet_sio_info_type *  info;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(rmnet_sio_handle);
  info = (rmnet_sio_info_type *) rmnet_sio_handle;

  if (info->inited == FALSE)
  {
    LOG_MSG_ERROR_0 ("Device not yet initialized!");
    return;
  }

  /*-------------------------------------------------------------------------
    Free any buffered data (Cleanup Rm rx and tx watermark queues)
  -------------------------------------------------------------------------*/
  dsm_empty_queue(&info->rx_wmk);
  dsm_empty_queue(&info->tx_wmk);
}



/*===========================================================================
  FUNCTION RMNET_SIO_GET_WMK_QUEUES()

  DESCRIPTION
    This function returns the pointer to the RX and TX watermark Queues.

  PARAMETERS
    rmnet_sio_handle (IN): ptr to rmnet_sio info
    rx_wmk (OUT): pointer to RX watermark.
    tx_wmk (OUT): pointer to TX watermark.

  RETURN VALUE
    Failure : -1
    Success :  0

  DEPENDENCIES
    rmnet_sio_init() must have been called previously

  SIDE EFFECTS
    None
===========================================================================*/
int16 rmnet_sio_get_wmk_queues
(
  void                * rmnet_sio_handle,
  dsm_watermark_type  **rx_wmk,
  dsm_watermark_type  **tx_wmk
)
{
  rmnet_sio_info_type *  info;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(rmnet_sio_handle);
  info = (rmnet_sio_info_type *) rmnet_sio_handle;

  if (info->inited == FALSE)
  {
    LOG_MSG_ERROR_0 ("Device not yet initialized!");
    return -1;
  }

  *rx_wmk = &info->rx_wmk;
  *tx_wmk = &info->tx_wmk;

  return 0;
}



/*===========================================================================
FUNCTION RMNET_SIO_GET_PORT

DESCRIPTION
  This function gets the port number associated to the rmiface

PARAMETERS
  *rmnet_sio_handle: ptr to rmnet_sio_info

DEPENDENCIES
  None

RETURN VALUE
  sio_port_id_type - the associated port

SIDE EFFECTS
  None.
===========================================================================*/
sio_port_id_type rmnet_sio_get_port
(
  void *rmnet_sio_handle
)
{
  rmnet_sio_info_type *  info;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  info = ( rmnet_sio_info_type *)(rmnet_sio_handle);
  if (info == NULL || !info->inited)
  {
    LOG_MSG_INFO1_0 ("RmNetsio instance not initialized!");
    return SIO_PORT_NULL;
  }

  return info->sio_port;
} /* rmnet_sio_get_port() */

/*===========================================================================
  FUNCTION RMNET_SIO_GET_MASTER_STREAM_ID()

  DESCRIPTION
    This function gets the master data channel stream ID.

  PARAMETERS
    None

  RETURN VALUE
    Stream ID for the master data channel

  DEPENDENCIES
    rmnet_sio_init() must have been called previously

  SIDE EFFECTS
    None
===========================================================================*/
sio_stream_id_type rmnet_sio_get_master_stream_id
(
  void
)
{
  rmnet_instance_e_type  master_rmnet_inst;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  master_rmnet_inst = rmnet_sio_get_master_inst();
  if (master_rmnet_inst < RMNET_INSTANCE_MAX)
  {
    return rmnet_sio_info[master_rmnet_inst].stream_id;
  }

  return 0;

} /* rmnet_sio_get_master_stream_id() */

/*===========================================================================
  FUNCTION RMNET_SIO_GET_MASTER_INST()

  DESCRIPTION
    This function returns the master rmnet instance used for muxing.

  PARAMETERS
    None

  RETURN VALUE
    Master rmnet instance used for muxing. 
    RMNET_INSTANCE_MAX if not found

  DEPENDENCIES
    rmnet_sio_init() must have been called previously

  SIDE EFFECTS
    None
===========================================================================*/
rmnet_instance_e_type rmnet_sio_get_master_inst
(
  void
)
{
  sio_port_id_type  master_sio_port;
  int               i;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  master_sio_port = (sio_port_id_type)qmi_get_mux_master_sio_port();

  if (master_sio_port != SIO_PORT_NULL)
  {
    for (i = 0; i < RMNET_INSTANCE_MAX; i+=2)
    {
      if (rmnet_sio_info[i].sio_port == master_sio_port)
      {
        return (rmnet_instance_e_type)i;
      }
    }
  }

  return RMNET_INSTANCE_MAX;

} /* rmnet_sio_get_master_inst() */

#ifdef FEATURE_DATA_A2
/*===========================================================================
  FUNCTION RMNET_SIO_GET_DEFAULT_PORT_CFG()

  DESCRIPTION
    This function retrieves the default port configuration parameters.

  PARAMETERS
    rmnet_sio_handle (IN): ptr to rmnet_sio info
    port_cfg: pointer to port config structure 

  RETURN VALUE
    None

  DEPENDENCIES
    rmnet_sio_init() must have been called previously

  SIDE EFFECTS
    None
===========================================================================*/
void rmnet_sio_get_default_port_cfg
(
  void               * rmnet_sio_handle,
  a2_sio_port_cfg_s  * port_cfg
)
{
  rmnet_sio_info_type   * info;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if ( (rmnet_sio_handle == NULL) || (port_cfg == NULL) )
  {
    LOG_MSG_ERROR_2("Invalid params rmmnet_sio_handle %p, port_cfg %p",
                    rmnet_sio_handle, port_cfg);
    ASSERT(0);
    return;
  }

  info = (rmnet_sio_info_type *) rmnet_sio_handle;

  /*-------------------------------------------------------------------------
    Default dynamic port config
  -------------------------------------------------------------------------*/
  memset(port_cfg, 0, sizeof(a2_sio_port_cfg_s));

  port_cfg->open_port_stream_id   = info->stream_id;
  port_cfg->sio_port_id           = info->sio_port;
  port_cfg->ul_cs_action          = A2_CS_DISABLE;
  port_cfg->dl_cs_action          = A2_CS_DISABLE;
  port_cfg->dl_aggregation        = A2_DL_PER_TLP_DISABLE;
  port_cfg->ul_wm                 = &info->rx_wmk;

  port_cfg->mux_idx = qmux_get_default_mux_idx(info->sio_port);
  if (port_cfg->mux_idx)
  {
    // Muxed port
    port_cfg->mux_type = A2_MUX_QMAP;
    port_cfg->master_port_stream_id = rmnet_sio_get_master_stream_id();
  }
  else
  {
    port_cfg->mux_type = A2_MUX_NONE;
    port_cfg->master_port_stream_id = info->stream_id;
  }

} /* rmnet_sio_get_default_port_cfg() */

/*===========================================================================
FUNCTION RMNET_SIO_SET_DATA_FORMAT

DESCRIPTION
  This function sets the UL and DL data format for dynamic port config

PARAMETERS
  rmnet_sio_handle: ptr to rmnet sio info
  ul_data_agg:      UL data aggregation mode
  dl_data_agg:      DL data aggregation mode
  ndp_sig:          NDP signature
  dl_agg_max_num:   DL agg max num
  dl_agg_max_size:  DL agg max size  

DEPENDENCIES
  None

RETURN VALUE
  TRUE if success, FALSE if failure

SIDE EFFECTS
  None.
===========================================================================*/
boolean rmnet_sio_set_data_format
(
  void                     * rmnet_sio_handle,
  rmnet_data_agg_enum_type   ul_data_agg,
  rmnet_data_agg_enum_type   dl_data_agg,
  uint32                     ndp_sig,
  uint32                     dl_agg_max_num,
  uint32                     dl_agg_max_size,
  uint8                      dl_min_padding
)
{
  rmnet_sio_info_type        * info;
  a2_sio_port_cfg_s            a2_port_cfg = {0};
  a2_sio_port_cfg_s          * logical_port_cfg;
  int                          i;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (rmnet_sio_handle == NULL)
  {
    ASSERT(0);
    return FALSE;
  }

  info = (rmnet_sio_info_type *) rmnet_sio_handle;
  rmnet_sio_get_default_port_cfg(info, &a2_port_cfg);

  if (RMNET_DATA_AGG_IS_QMAP(ul_data_agg) &&
      RMNET_DATA_AGG_IS_QMAP(dl_data_agg))
  {
    a2_port_cfg.mux_type = A2_MUX_QMAP;
    a2_port_cfg.mux_idx = qmux_get_qmap_mux_id(info->sio_port);
    a2_port_cfg.dl_aggregation = (dl_agg_max_num > 1) ?
            A2_DL_PER_TLP_QMAP_ENABLE : A2_DL_PER_TLP_DISABLE;
    a2_port_cfg.nego_min_pad = dl_min_padding;

    a2_port_cfg.master_port_stream_id = a2_port_cfg.mux_idx ?
            rmnet_sio_get_master_stream_id() :  info->stream_id;

#ifdef FEATURE_DATA_QMAP_DEBUG
    #error code not present
#endif /* FEATURE_DATA_QMAP_DEBUG */
  }
  else if (ul_data_agg == RMNET_ENABLE_DATA_AGG_MBIM &&
           dl_data_agg == RMNET_ENABLE_DATA_AGG_MBIM)
  {
    a2_port_cfg.mux_type = A2_MUX_MBIM;
    a2_port_cfg.dl_aggregation = A2_DL_PER_TLP_MS_MBIM_NCM_V20_ENABLE;
    a2_port_cfg.ndp_sig = ndp_sig;
    a2_port_cfg.ncm_sig = RMNET_MBIM_HDR_NTH16_SIG;
  }
  else if (ul_data_agg != RMNET_DISABLE_DATA_AGG ||
           dl_data_agg != RMNET_DISABLE_DATA_AGG)
  {

    LOG_MSG_ERROR_2("Unsupported data format UL [%d] DL [%d]",
              ul_data_agg, dl_data_agg);
    return FALSE;
  }

  // Set A2 dynamic port config
  if (a2_sio_set_dynamic_port_cfg(&a2_port_cfg) != A2_SUCCESS)
  {
    LOG_MSG_ERROR_0("Failed to set A2 dynamic port cfg");
    return FALSE;
  }

  // Set DL agg thresholds 
  if (dl_agg_max_num > 1)
  {
    if (RMNET_DATA_AGG_IS_QMAP(dl_data_agg) ||
        dl_data_agg == RMNET_ENABLE_DATA_AGG_MBIM)
    {
      a2_dl_per_set_thresholds_tlp(info->sio_port,
                      dl_agg_max_num, dl_agg_max_size);
    }
  }

  // Copy the successful config
  info->mux_port_cfg = a2_port_cfg;

  // If this is a master channel, apply data format to all logical channels
  if (info->sio_port == qmi_get_mux_master_sio_port())
  {
    // RmNet
    for (i = 0; i < RMNET_INSTANCE_MAX; i+=2)
    {
      if (QMUX_IS_QMAP_MUXED_PORT(rmnet_sio_info[i].sio_port) &&
          &rmnet_sio_info[i] != info)
      {
        logical_port_cfg = &rmnet_sio_info[i].mux_port_cfg;

        if (a2_port_cfg.mux_type == A2_MUX_NONE)
        {
          rmnet_sio_get_default_port_cfg(
                 &rmnet_sio_info[i], logical_port_cfg);
          continue;
        }

        logical_port_cfg->mux_type       = a2_port_cfg.mux_type;
        logical_port_cfg->dl_aggregation = a2_port_cfg.dl_aggregation;
        logical_port_cfg->ul_cs_action   = a2_port_cfg.ul_cs_action;
        logical_port_cfg->dl_cs_action   = a2_port_cfg.dl_cs_action;
        logical_port_cfg->ndp_sig        = a2_port_cfg.ndp_sig;
        logical_port_cfg->ncm_sig        = a2_port_cfg.ncm_sig;
        logical_port_cfg->nego_min_pad   = a2_port_cfg.nego_min_pad;

        logical_port_cfg->mux_idx =
               qmux_get_qmap_mux_id(rmnet_sio_info[i].sio_port);
        logical_port_cfg->master_port_stream_id =
               rmnet_sio_get_master_stream_id();
      }
    }

#ifdef FEATURE_DATA_WLAN_MAPCON
    // Reversed RmNet
    rev_ip_transport_set_data_format(&a2_port_cfg);
#endif /* FEATURE_DATA_WLAN_MAPCON */
  }

  return TRUE;

} /* rmnet_sio_set_data_format() */

/*===========================================================================
  FUNCTION RMNET_SIO_SET_MUX_PORT_CFG()

  DESCRIPTION
    This function sets rmnet mux port configuration.

  PARAMETERS
    rmnet_sio_handle (IN): ptr to rmnet_sio info

  RETURN VALUE
    Failure : -1
    Success :  0

  DEPENDENCIES
    rmnet_sio_init() must have been called previously

  SIDE EFFECTS
    None
===========================================================================*/
boolean rmnet_sio_set_mux_port_cfg
(
  void  * rmnet_sio_handle
)
{
  rmnet_sio_info_type   * info;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (rmnet_sio_handle == NULL)
  {
    LOG_MSG_ERROR_0("NULL sio handle");
    return FALSE;
  }

  info = (rmnet_sio_info_type *) rmnet_sio_handle;

  if ( !rmnet_sio_is_a2_port(info->sio_port) )
  {
    LOG_MSG_INFO2_1("Non-A2 port 0x%x", info->sio_port);
    return TRUE;
  }

  if (a2_sio_set_dynamic_port_cfg(&info->mux_port_cfg) != A2_SUCCESS)
  {
    LOG_MSG_ERROR_0("Failed to set A2 dynamic port cfg");
    return FALSE;
  }

  LOG_MSG_INFO2_3("Set dynamic port 0x%x mux_type %d mux_idx %d",
                  info->sio_port,
                  info->mux_port_cfg.mux_type,
                  info->mux_port_cfg.mux_idx);

  return TRUE;

} /* rmnet_sio_set_mux_port_cfg() */

#endif /* FEATURE_DATA_A2 */

/*===========================================================================
FUNCTION RMNET_SIO_GET_UL_DATA_AGG_THRESHOLDS

DESCRIPTION
  This function retrieves UL data aggregation thresholds

PARAMETERS
  rmnet_sio_handle: ptr to rmnet sio info
  ul_agg_max_num:   Max num agg packets
  ul_agg_max_size:  Max agg size 

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
void rmnet_sio_get_ul_data_agg_thresholds
(
  void                     * rmnet_sio_handle,
  uint32                   * ul_agg_max_num,
  uint32                   * ul_agg_max_size  
)
{
  rmnet_sio_info_type        * info;
#ifdef FEATURE_DATA_A2
  a2_ul_per_aggr_thresholds_s  ul_agg_thresh = {0};
#endif
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if ( (ul_agg_max_num == NULL) || (ul_agg_max_size == NULL) )
  {
    LOG_MSG_ERROR_0("Invalid params");
    ASSERT(0);
    return;
  }

  *ul_agg_max_num = 0;
  *ul_agg_max_size = 0;

  if (rmnet_sio_handle == NULL)
  {
    LOG_MSG_ERROR_0("NULL sio_handle");
    return;
  }

  info = (rmnet_sio_info_type *) rmnet_sio_handle;

  if ( !rmnet_sio_is_a2_port(info->sio_port) )
  {
    LOG_MSG_INFO2_1("Non-A2 port 0x%x", info->sio_port);
    return;
  }

#ifdef FEATURE_DATA_A2
  a2_ul_per_get_aggr_thresholds(
              info->mux_port_cfg.mux_type, &ul_agg_thresh);

  *ul_agg_max_num = ul_agg_thresh.max_num_pkts;
  *ul_agg_max_size = ul_agg_thresh.max_num_bytes; 
#endif

} /* rmnet_sio_get_ul_data_agg_thresholds() */


/*===========================================================================

                            INTERNAL FUNCTIONS

===========================================================================*/


/*===========================================================================
  FUNCTION RMNET_SIO_OPEN()

  DESCRIPTION
    Initializes the data link.

  PARAMETERS
    info    :  Rmnet sio info structure
    port_id :  The port that has been mapped to the service (implied by
               this function).

  RETURN VALUE
    None

  DEPENDENCIES
    rmnet_sio_init() must have been called previously

  SIDE EFFECTS
    None
===========================================================================*/
static boolean  rmnet_sio_open
(
  rmnet_sio_info_type * info,
  sio_port_id_type      port_id        /* Port id of the port to be opened */
)
{
  sio_open_type          open_param;
  sio_ioctl_param_type   ioctl_param;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_INFO1_1( "RmNet opening sio port id: %d", port_id );

  /*-------------------------------------------------------------------------
    Open SIO control stream

    For now, assert that this is always the USB_NET_WWAN device since that
    is the only device that can be mapped to the only rmnetsio instance
    (WWAN).
  -------------------------------------------------------------------------*/
  memset (&open_param, 0, sizeof (open_param));

  open_param.port_id        = port_id;
  open_param.stream_mode    = SIO_GENERIC_MODE;
  open_param.rx_func_ptr    = NULL;
  open_param.rx_queue       = &info->rx_wmk;
  open_param.tx_queue       = &info->tx_wmk;
  open_param.rx_bitrate     = SIO_BITRATE_BEST;
  open_param.tx_bitrate     = SIO_BITRATE_BEST;
  open_param.tail_char_used = FALSE;
  open_param.rx_flow        = SIO_FCTL_BEST;
  open_param.tx_flow        = SIO_FCTL_BEST;

  /*-------------------------------------------------------------------------
    Open the serial port, and store the assigned stream id. If the open was
    unsuccessful, log an error.
  -------------------------------------------------------------------------*/
  info->stream_id = sio_open( &open_param );
  if( info->stream_id == SIO_NO_STREAM_ID )
  {
    LOG_MSG_ERROR_0( "Data Services couldn't open NET WWAN control stream" );
    return FALSE;
  }
  else
  {
     /* We need to set the DROP_PENDING ioctl on SMD ports, in order to
      * enable the SMD module to drop packets when the onward link to apps
      * proc is not available. */
    if ( (port_id >= SIO_PORT_SMD_DATA_FIRST) &&
         (port_id <= SIO_PORT_SMD_DATA_LAST) )
    {
      memset( &ioctl_param, 0, sizeof(sio_ioctl_param_type) );
      ioctl_param.drop_asserted = TRUE;
      sio_ioctl(info->stream_id,
                SIO_IOCTL_SET_DROP_PEND,
                &ioctl_param);
    }
  }

#ifdef FEATURE_DATA_A2
  /*-------------------------------------------------------------------------
    Get default mux port config parameters
  -------------------------------------------------------------------------*/
  rmnet_sio_get_default_port_cfg(info, &info->mux_port_cfg);
#endif /* FEATURE_DATA_A2 */

  return TRUE;
} /* rmnet_sio_open() */


/*===========================================================================
  FUNCTION RMNET_SIO_CLOSE()

  DESCRIPTION
    Closes the data link

  PARAMETERS
    info    :  Rmnet sio info structure

  RETURN VALUE
    None

  DEPENDENCIES
    rmnet_sio_init() must have been called previously

  SIDE EFFECTS
    None
===========================================================================*/
static boolean rmnet_sio_close
(
  rmnet_sio_info_type * info
)
{
  if( info == NULL )
  {
    LOG_MSG_INFO1_0( "rmnet_sio_close() info ptr was NULL\n" );
    ASSERT(0);
  }

  sio_close( info->stream_id, NULL );
  return TRUE;
} /* rmnet_sio_close() */


/*===========================================================================
  FUNCTION RMNET_SIO_DEFAULT_RX_CB()

  DESCRIPTION
    Default recieve function for dsnetsio instance.  Simply frees received
    data.  Controller of dsnetsio instance must register a real receive
    callback via rmnet_sio_set_rx_cb() in order to connect the data link
    to the link layer data protcol above.

  PARAMETERS
    rx_wm_ptr           -  receive watermark on which rx data is enqueued
    rx_wm_ptr_user_data -  ignored

  RETURN VALUE
    None

  DEPENDENCIES
    rmnet_sio_init() must have been called previously

  SIDE EFFECTS
    None
===========================================================================*/
static void rmnet_sio_default_rx_cb
(
  dsm_watermark_type *  rx_wm_ptr,
  void *                rx_wm_user_data
)
{
  dsm_item_type *  item;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------------
    When no rx_cb set by upper layer, data shouldn't arrive, but if it
    does, just throw it away
  -------------------------------------------------------------------------*/
  while ((item = dsm_dequeue(rx_wm_ptr)) != NULL)
  {
    dsm_free_packet( &item );
  }
} /* rmnet_sio_default_rx_cb() */



/*===========================================================================
  FUNCTION RMNET_SIO_DTR_CB()

  DESCRIPTION
    DTR change callback called by SIO when the DTR state changes.

    TODO:  Currently assumes DTR event occurred on dsnetsio link #0
           (WWAN) - need to get sio to take a user_data_ptr that
           would be passed by client (dsnetsio would pass instance)

  PARAMETERS
    None
    FUTURE:  void *  user_data - indicates which rmnet_sio instance dtr event

  RETURN VALUE
    None

  DEPENDENCIES
    rmnet_sio_init() must have been called previously

  SIDE EFFECTS
    None
===========================================================================*/
static void rmnet_sio_dtr_cb
(
  void *  user_data
)
{
  rmnet_sio_info_type *  info;
  sio_ioctl_param_type  ioctl_param;
  boolean               dtr_status;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT (user_data);
  info = (rmnet_sio_info_type *) user_data;
  ASSERT(info->dtr_cb);

  dtr_status = FALSE;
  ioctl_param.dte_ready_asserted = &dtr_status;
  sio_ioctl( info->stream_id,
             SIO_IOCTL_DTE_READY_ASSERTED,
             &ioctl_param );

  /*-------------------------------------------------------------------------
    Call registered callback - user_data indicates which rmnet device
  -------------------------------------------------------------------------*/
  info->dtr_cb(info->dtr_cb_user_data, dtr_status);

} /* rmnet_sio_dtr_cb() */


#ifndef FEATURE_DATA_RM_NET_USES_SM

/*===========================================================================
  FUNCTION RMNET_SIOI_TLP_CB()

  DESCRIPTION
    TLP setting change callback called by SIO when the TLP settings change.

  PARAMETERS
    None
    FUTURE:  void *  user_data - indicates which rmnet_sio instance dtr event

  RETURN VALUE
    None

  DEPENDENCIES
    rmnet_sio_init() must have been called previously

  SIDE EFFECTS
    None
===========================================================================*/
static void rmnet_sioi_tlp_cb
(
  sio_ioctl_usb_tlp_settings_type  current_setting,
  void *                           user_data
)
{
  rmnet_sio_info_type *  info;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT (user_data);
  info = (rmnet_sio_info_type *) user_data;

  /*-------------------------------------------------------------------------
    Invoke callback registered by upper layer if not NULL
  -------------------------------------------------------------------------*/
  info->tlp_setting = current_setting;
  if (info->tlp_cb)
  {
    info->tlp_cb(
      current_setting.downlink_tlp_on ?
        (rmnet_sio_tlp_ver_type)current_setting.version : RMNET_SIO_TLP_VER_NONE,
      current_setting.uplink_tlp_on ?
        (rmnet_sio_tlp_ver_type)current_setting.version : RMNET_SIO_TLP_VER_NONE,
      info->tlp_user_data );
  }
} /* rmnet_sioi_tlp_cb() */
#endif /* FEATURE_DATA_RM_NET_USES_SM */


/*===========================================================================
  FUNCTION RMNET_SIO_SETUP_SIO_WATERMARKS()

  DESCRIPTION
    Initialize the recieve and transmit watermarks for the dsrmnetsio
    instance inferred by 'rmnet_sio_handle'

  PARAMETERS
    rx_wm_ptr           -  receive watermark on which rx data is enqueued
    rx_wm_ptr_user_data -  ignored

  RETURN VALUE
    None

  DEPENDENCIES
    rmnet_sio_init() must have been called previously

  SIDE EFFECTS
    None
===========================================================================*/
static void rmnet_sio_setup_sio_watermarks
(
  rmnet_sio_info_type *  info
)
{
#ifdef FEATURE_RMNET_DATA_AGG_TIMER
  nv_stat_enum_type   nv_status;
  nv_item_type       *dcc_nv_item_ptr;
#endif /* FEATURE_RMNET_DATA_AGG_TIMER */
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(info);

  /*-------------------------------------------------------------------------
    FUTURE:  externalize this API when multiple rmnetsio instances
             supported, so that mode controller can customize the settings.
             TASKLOCK/INTLOCK when this happens
  -------------------------------------------------------------------------*/
  /*-------------------------------------------------------------------------
    Make sure that the watermarks passed in are empty
  -------------------------------------------------------------------------*/
  if(info->tx_wmk.current_cnt != 0)
  {
    LOG_MSG_ERROR_2("Tx WM 0x%x not empty, %d bytes",
                    &info->tx_wmk, info->tx_wmk.current_cnt);
    dsm_empty_queue(&info->tx_wmk);
  }

  if(info->rx_wmk.current_cnt != 0)
  {
    LOG_MSG_ERROR_2("Rx WM 0x%x not empty, %d bytes",
                    &info->rx_wmk, info->rx_wmk.current_cnt);
    dsm_empty_queue(&info->rx_wmk);
  }

  /*-------------------------------------------------------------------------
                             SETUP SIO TX WATERMARK
  -------------------------------------------------------------------------*/

  /*-------------------------------------------------------------------------
    This WaterMark Queue is to be used for the purposes of SIO to laptop
    transmisssion.
    Since over the air link is slower than SIO TX bitrate,flow control is
    not necessary. Hence not setting the low and high levels.
  -------------------------------------------------------------------------*/
#ifdef FEATURE_WCDMA_3C_HSDPA
  info->tx_wmk.lo_watermark          = 144000;
  info->tx_wmk.hi_watermark          = 360000;
#elif defined(FEATURE_WCDMA_REL7) && defined(FEATURE_WCDMA_64QAM)
  info->tx_wmk.lo_watermark          = 80000;
  info->tx_wmk.hi_watermark          = 200000;
#else
  info->tx_wmk.lo_watermark          = 8192;
  info->tx_wmk.hi_watermark          = 65536;
#endif /* FEATURE_WCDMA_3C_HSDPA */

  info->tx_wmk.dont_exceed_cnt =
    RMNET_TX_WM_DNE;
  //TO DO maybe not infinite, for usb fctl reasons
  LOG_MSG_INFO1_1( "Set sio_tx_wm to be infinite siz: %d",
                   info->tx_wmk.dont_exceed_cnt );

  info->tx_wmk.gone_empty_func_ptr   = NULL;
#ifdef FEATURE_RMNET_DATA_AGG_TIMER
  info->tx_wmk.non_empty_func_ptr    = rmnet_sio_tx_non_empty_func;
  info->tx_wmk.non_empty_func_data   = info;
#else
  info->tx_wmk.non_empty_func_ptr    = NULL;
#endif /* FEATURE_RMNET_DATA_AGG_TIMER */
  info->tx_wmk.lowater_func_ptr      = rmnet_sio_tx_lowater_func;
  info->tx_wmk.lowater_func_data     = (void *) RMNET_SIO_INFO_TO_INSTANCE(info);
  info->tx_wmk.hiwater_func_ptr      = rmnet_sio_tx_hiwater_func;
  info->tx_wmk.hiwater_func_data     = (void *) RMNET_SIO_INFO_TO_INSTANCE(info);
  info->tx_wmk.each_enqueue_func_ptr = NULL;

  /*-------------------------------------------------------------------------
                             SETUP SIO RX WATERMARK
  -------------------------------------------------------------------------*/

  /*-------------------------------------------------------------------------
    Configure the watermark used for receiving data from SIO.
    to SIO.
    SIO watermark use should be synchonized with the ISR used by the
    serial driver.
  -------------------------------------------------------------------------*/

  /*-------------------------------------------------------------------------
    Network interface device is Ethernet-over-USB, i.e. fast.
    Therefore, set low, high, and dont exceed limits accordingly.
    Standard Ethernet frame is 1514 bytes.
  -------------------------------------------------------------------------*/
  info->rx_wmk.lo_watermark        = RMNET_RX_WM_LOW;
  info->rx_wmk.hi_watermark        = RMNET_RX_WM_HIGH;
  info->rx_wmk.dont_exceed_cnt     = RMNET_RX_WM_DNE;

  info->rx_wmk.lowater_func_ptr      = rmnet_sio_rx_lowater_func;
  info->rx_wmk.lowater_func_data     = info;
  info->rx_wmk.hiwater_func_ptr      = rmnet_sio_rx_hiwater_func;
  info->rx_wmk.hiwater_func_data     = info;
  info->rx_wmk.gone_empty_func_ptr   = NULL;
  info->rx_wmk.non_empty_func_ptr    = rmnet_sio_default_rx_cb;
  info->rx_wmk.non_empty_func_data   = info;
  info->rx_wmk.each_enqueue_func_ptr = NULL;

#ifdef FEATURE_DSM_MEM_CHK
  /*-------------------------------------------------------------------------
    Reset the total_rcvd_cnt, as a packet data call can really set this
    number very high.
  -------------------------------------------------------------------------*/
  info->tx_wmk.total_rcvd_cnt        = 0;
  info->rx_wmk.total_rcvd_cnt        = 0;
#endif /* FEATURE_DSM_MEM_CHK */
#ifdef FEATURE_RMNET_DATA_AGG_TIMER
 timer_def(&(info->rmnet_smd_delay_timer),NULL,NULL,NULL,
             rmnet_smd_delay_timer_expired,(timer_cb_data_type)info);

  /*-------------------------------------------------------------------------
    Allocate temporary memory for the NV item
  -------------------------------------------------------------------------*/
  dcc_nv_item_ptr = (nv_item_type *)
    qmi_svc_ps_system_heap_mem_alloc(sizeof(nv_item_type),
                      FILE_ID_DS_RMNET_SIO,__LINE__);
  if( dcc_nv_item_ptr == NULL )
  {
    ASSERT(0);
  }

  nv_status = dcc_get_nv_item(NV_DATA_AGG_TIMER_I, dcc_nv_item_ptr);
  if( nv_status == NV_DONE_S)
  {
    /* NV ITEM 5 corresponds to first RMNET instance timer value.
    SMD_DATA_PORT5 = RMNET_INSTANCE1 hence NV ITEM 5 */

    info->rmnet_smd_timer_value =
    dcc_nv_item_ptr->data_agg_timer[RMNET_DATA_AGG_FIRST_NV_ITEM+RMNET_SIO_INFO_TO_INSTANCE(info)];

    if(info->rmnet_smd_timer_value == 0)
    {
      LOG_MSG_ERROR_0("NV Item NV_DATA_AGG_TIMER_I not set, Setting the value to 5 ms");
      info->rmnet_smd_timer_value = RMNET_DATA_AGG_DEFAULT_TIMER_VALUE;
    }
    else
    {
      LOG_MSG_INFO1_1("NV Item NV_DATA_AGG_TIMER_I is %d",
                      info->rmnet_smd_timer_value);
    }
  }
  else
  {
    info->rmnet_smd_timer_value = RMNET_DATA_AGG_DEFAULT_TIMER_VALUE;
  }
  PS_SYSTEM_HEAP_MEM_FREE(dcc_nv_item_ptr);
#endif /* FEATURE_RMNET_DATA_AGG_TIMER */

} /* rmnet_sio_setup_sio_watermarks() */



/*===========================================================================
  FUNCTION RMNET_SIO_RX_LOWATER_FUNC()

  DESCRIPTION
    Receive watermark low-water callback.
    Reenables rx flow on the data link specified by user_data
    (which is set to dsnetsio handle).

  PARAMETERS
    rx_wm_ptr           -  receive watermark on which rx data is enqueued
    rx_wm_ptr_user_data -  ignored

  RETURN VALUE
    None

  DEPENDENCIES
    rmnet_sio_init() must have been called previously

  SIDE EFFECTS
    None
===========================================================================*/
static void rmnet_sio_rx_lowater_func
(
  struct dsm_watermark_type_s *  wm_ptr,
  void *                         user_data_ptr
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  rmnet_sio_set_rx_flow( user_data_ptr, RMNET_SIO_FLOWMASK_MEM, FALSE );
} /* rmnet_sio_rx_lowater_func() */



/*===========================================================================
  FUNCTION RMNET_SIO_RX_HIWATER_FUNC()

  DESCRIPTION
    Receive watermark high-water callback.
    Disabled rx flow on the data link specified by user_data
    (which is set to dsnetsio handle).

  PARAMETERS
    rx_wm_ptr           -  receive watermark on which rx data is enqueued
    rx_wm_ptr_user_data -  ignored

  RETURN VALUE
    None

  DEPENDENCIES
    rmnet_sio_init() must have been called previously

  SIDE EFFECTS
    None
===========================================================================*/
static void rmnet_sio_rx_hiwater_func
(
  struct dsm_watermark_type_s *wm_ptr,
  void *user_data_ptr
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  rmnet_sio_set_rx_flow( user_data_ptr, RMNET_SIO_FLOWMASK_MEM, TRUE );
} /* rmnet_sio_rx_hiwater_func() */

/*===========================================================================
  FUNCTION RMNET_SIO_TX_HIWATER_FUNC()

  DESCRIPTION
   This function disables flow on rmnet iface, when the hiwater point
   in the tx watermark is reached.

  PARAMETERS
    None

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void rmnet_sio_tx_hiwater_func
(
  struct dsm_watermark_type_s *wm_ptr,
  void *user_data_ptr
)
{
  rmnet_sio_info_type *  info = NULL;
#ifdef FEATURE_RMNET_DATA_AGG_TIMER
  sio_ioctl_param_type wait_param;
#endif /* FEATURE_RMNET_DATA_AGG_TIMER */
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if( ((uint32)user_data_ptr) >= RMNET_INSTANCE_MAX)
  {
    LOG_MSG_ERROR_0("Invalid RMNET Instance");
    ASSERT(0);
  }
  info = &rmnet_sio_info[(uint32)user_data_ptr];

#ifdef FEATURE_RMNET_DATA_AGG_TIMER
  timer_clr(&(info->rmnet_smd_delay_timer), T_MSEC);
  /* The High Watermark is hit.Clear the WAIT in SIO and clear the
     rmnet_smd_delay_timer for this interface. */
  wait_param.wm_wait = FALSE;
  sio_ioctl(info->stream_id, SIO_IOCTL_WM_SET_WAIT, &wait_param);
#endif /* FEATURE_RMNET_DATA_AGG_TIMER */

  LOG_MSG_INFO2_0("rmnet_sio_tx_hiwater_func" );
  rmnet_sio_set_tx_flow( info, RMNET_SIO_FLOWMASK_MEM, TRUE, user_data_ptr );
} /* rmnet_sio_tx_hiwater_func() */

#ifdef FEATURE_RMNET_DATA_AGG_TIMER
/*===========================================================================
  FUNCTION RMNET_SIO_TX_NON_EMPTY_FUNC()

  DESCRIPTION
    Transmit Non empty callback.

  PARAMETERS
    tx_wm_ptr           -  receive watermark on which rx data is enqueued
    tx_wm_ptr_user_data -  ignored

  RETURN VALUE
    None

  DEPENDENCIES
    rmnet_sio_init() must have been called previously

  SIDE EFFECTS
    None
===========================================================================*/
static void rmnet_sio_tx_non_empty_func
(
  struct dsm_watermark_type_s *  wm_ptr,
  void *                         user_data_ptr
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  sio_ioctl_param_type wait_param;
  rmnet_sio_info_type *  info;
  info = ( rmnet_sio_info_type *)(user_data_ptr);

  if (rmnet_sio_da_enabled)
  {
    /* Set the WAIT to true to stop SMD transfers.
       Also start the delay timer */
    wait_param.wm_wait = TRUE;
    sio_ioctl(info->stream_id, SIO_IOCTL_WM_SET_WAIT, &wait_param);

    timer_set(&(info->rmnet_smd_delay_timer),
              info->rmnet_smd_timer_value,  // MS delay.defined ds_task
             0,
              T_MSEC);
    LOG_MSG_INFO2_1("rmnet_sio_tx_non_empty_func delay is %d",
                    info->rmnet_smd_timer_value);
  }
} /* rmnet_sio_tx_non_empty_func */
#endif /* FEATURE_RMNET_DATA_AGG_TIMER */


/*===========================================================================
  FUNCTION RMNET_SIO_TX_LOWATER_FUNC()

  DESCRIPTION
   This function enables flow on the rmnet iface, when the lowater point
   in the tx watermark is reached.

  PARAMETERS
    None

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void rmnet_sio_tx_lowater_func
(
  struct dsm_watermark_type_s *wm_ptr,
  void *user_data_ptr
)
{
 rmnet_sio_info_type *       info;        /* serving task cmd buffer */
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if( ((uint32)user_data_ptr) >= RMNET_INSTANCE_MAX)
  {
    LOG_MSG_ERROR_0("Invalid RMNET Instance");
    ASSERT(0);
  }

  info = &rmnet_sio_info[(uint32)user_data_ptr];
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  rmnet_sio_set_tx_flow( info, RMNET_SIO_FLOWMASK_MEM, FALSE, user_data_ptr );

} /* rmnet_sio_tx_lowater_func() */

/*===========================================================================
  FUNCTION RMNET_SIO_SET_TX_FLOW()

  DESCRIPTION
    Flow control the dsnetsio link specified by rmnet_sio_handle.

    The reason for the flow is specified by 'mask'.  Whether to enable (0)
    or disable (1) flow is specied by 'disable' parameter.  Caller must
    enable the flow with the same mask that was used to disable.

  PARAMETERS
    rmnet_sio_handle :  handle to dsnetsio link returned by rmnet_sio_init()
    mask             :  Bitmask indicating the reason for flow control
                        (allows disabling flow if any of various reasons
                         represented by separate bits has been specified)
    disable          :  FALSE = enable flow for reasons in 'mask'
                        TRUE  = disable flow for reasons in 'mask'

  RETURN VALUE
    None

  DEPENDENCIES
    rmnet_sio_init() must have been called previously

  SIDE EFFECTS
    None
===========================================================================*/
void rmnet_sio_set_tx_flow
(
  void *  rmnet_sio_handle,
  uint32  mask,
  boolean disable,
  void *  user_data_ptr
)
{
  rmnet_smi_cmd_type *       cmd_ptr;
  rmnet_sio_info_type *      info;
  uint32                     prev_mask;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(rmnet_sio_handle);
  info = (rmnet_sio_info_type *) rmnet_sio_handle;

  /*-------------------------------------------------------------------------
    Set bits specified by mask in flow state if disable.
    Clear bits specified by mask in flow state if enable (!disable).
    If mask is null, return immediately without change.
  -------------------------------------------------------------------------*/
  if (!mask)
  {
    MSG_ERROR( "set_tx_flow called with 0 mask!",0,0,0);
    return;
  }

  PS_ENTER_CRIT_SECTION(&rmnet_crit_section);
  if (disable)
  {
    /*-----------------------------------------------------------------------
      If flow was previously enabled, send the cmd to disable flow
      through the device now.  If flow was previously disabled, do nothing
      as we're already flow controlled.
    -----------------------------------------------------------------------*/
    if (RMNET_SIO_FLOWMASK_NONE == info->tx_flow_mask)
    {
      /*-------------------------------------------------------------------------
      Post cmd to DCC task to Disable outgoing data on the Rm interface.
       -------------------------------------------------------------------------*/
      cmd_ptr = (rmnet_smi_cmd_type *)RMNET_SMI_GET_CMD_BUF(DCC_RMNET_SM_CMD,FALSE);
      if( cmd_ptr == NULL )
      {
        PS_LEAVE_CRIT_SECTION(&rmnet_crit_section);
        return;
      }
      else
      {
        cmd_ptr->sm_id    = RMNET_META_SM;
        cmd_ptr->cmd_type = RMNET_SM_CMD_TX_FLOW_DISABLE;
        cmd_ptr->info_ptr = user_data_ptr;
        dcc_send_cmd_ex(DCC_RMNET_SM_CMD, (void *)cmd_ptr );
      }
    }

    /*-----------------------------------------------------------------------
      Set the passed bitmask from the rmnetsio instance flow control mask
    -----------------------------------------------------------------------*/
    info->tx_flow_mask |= mask;
  }
  else /* enable */
  {
    /* save the prev mask value */
     prev_mask = info->tx_flow_mask;

     info->tx_flow_mask &= ~(mask);

    /*-----------------------------------------------------------------------
      If flow was previously disabled and is now enabled, send the cmd to
      enable flow through the device now.  If flow was previously enabled,
      do nothing as we're already flowing.
    -----------------------------------------------------------------------*/
    if ((RMNET_SIO_FLOWMASK_NONE == info->tx_flow_mask) && (prev_mask))
    {
      /*-------------------------------------------------------------------------
      Post cmd to DCC task to Enable outgoing data on the Rm interface.
      -------------------------------------------------------------------------*/
      cmd_ptr = (rmnet_smi_cmd_type *)RMNET_SMI_GET_CMD_BUF(DCC_RMNET_SM_CMD,FALSE);
      if( cmd_ptr == NULL )
      {
        PS_LEAVE_CRIT_SECTION(&rmnet_crit_section);
        return;
      }
      else
      {
        cmd_ptr->sm_id    = RMNET_META_SM;
        cmd_ptr->cmd_type = RMNET_SM_CMD_TX_FLOW_ENABLE;
        cmd_ptr->info_ptr = user_data_ptr;
        dcc_send_cmd_ex(DCC_RMNET_SM_CMD, (void *)cmd_ptr );
      }
    }
  }
  PS_LEAVE_CRIT_SECTION(&rmnet_crit_section);

} /* rmnet_sio_set_tx_flow() */

/*===========================================================================
  FUNCTION RMNET_SIO_GET_INFO()

  DESCRIPTION
    This function returns rmnet sio ptr to an rmnet instance.

  PARAMETERS
    rmnet_inst:            rmnet instance

  RETURN VALUE
    rmnet sio info ptr

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
rmnet_sio_info_type  * rmnet_sio_get_info
(
  rmnet_instance_e_type  rmnet_inst
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (rmnet_inst >= RMNET_INSTANCE_MAX)
  {
    return NULL;
  }

  return &rmnet_sio_info[rmnet_inst];

} /* rmnet_sio_get_info() */

