#ifndef _DS_RMNET_QMAP_H
#define _DS_RMNET_QMAP_H
/*===========================================================================

                    D S _ R M N E T _ Q M A P . H

DESCRIPTION

  RMNET QMAP - Definitions header file.

EXTERNALIZED FUNCTIONS

Copyright (c) 2005-2013 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/rmifacectls/inc/ds_rmnet_qmap.h#1 $
  $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when        who    what, where, why
--------    ---    ----------------------------------------------------------
05/24/13    wc     Initial file
===========================================================================*/

/*===========================================================================

                                INCLUDE FILES

===========================================================================*/
#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"

#include "ds_rmnet_defs.h"

/*===========================================================================

                                 DEFINITIONS

===========================================================================*/

typedef struct
{
  boolean in_band_fc;       // In-band flow control
} ds_rmnet_qmap_settings_type;


/*===========================================================================

                           EXTERNAL FUNCTIONS 

===========================================================================*/


/*===========================================================================
  FUNCTION DS_RMNET_SEND_QMAP_CMD()

  DESCRIPTION
    This function sends a QMAP command for a rmnet instance

  PARAMETERS
    rmnet_inst:            rmnet instance
    cmd_size:              QMAP command size
    cmd_ptr:               QMAP command pointer

  RETURN VALUE
    TRUE/FALSE 

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean ds_rmnet_send_qmap_cmd
(
  rmnet_instance_e_type  rmnet_inst,
  uint16                 cmd_size,
  void                 * cmd_ptr
);

/*===========================================================================
  FUNCTION DS_RMNET_GET_QMAP_SETTINGS()

  DESCRIPTION
    This function retrieves QMAP protocol settings.

  PARAMETERS
    rmnet_inst:            rmnet instance
    qmap_settings:         ptr to qmap_settings 

  RETURN VALUE
    TRUE/FALSE

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean ds_rmnet_get_qmap_settings
(
  rmnet_instance_e_type           rmnet_inst,
  ds_rmnet_qmap_settings_type   * qmap_settings
);

/*===========================================================================
  FUNCTION DS_RMNET_SET_QMAP_SETTINGS()

  DESCRIPTION
    This function sets QMAP protocol settings.

  PARAMETERS
    rmnet_inst:            rmnet instance
    qmap_settings:         ptr to qmap_settings 

  RETURN VALUE
    TRUE/FALSE

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean ds_rmnet_set_qmap_settings
(
  rmnet_instance_e_type           rmnet_inst,
  ds_rmnet_qmap_settings_type   * qmap_settings
);

#endif /* _DS_RMNET_QMAP_H */

