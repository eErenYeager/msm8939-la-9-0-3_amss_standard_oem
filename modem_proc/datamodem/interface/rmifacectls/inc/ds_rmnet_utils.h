#ifndef _DS_RMNET_UTILS_H
#define _DS_RMNET_UTILS_H
/*===========================================================================

                        D S _ R M N E T _ U T I L S . H

DESCRIPTION

  Rm Network device - utility definitions internal header file.

EXTERNALIZED FUNCTIONS

Copyright (c) 2012-2013 Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/rmifacectls/inc/ds_rmnet_utils.h#1 $

when        who    what, where, why
--------    ---    ----------------------------------------------------------
04/10/13    wc     MBIM mPDN support
02/01/12    cy     Initial implementation
===========================================================================*/

/*===========================================================================

                                INCLUDE FILES 

===========================================================================*/
#include "comdef.h"
#include "customer.h"

#include "dsm.h"
#include "ds_rmnet_meta_sm.h"

/*===========================================================================

                                 DEFINITIONS

===========================================================================*/

#define IPS_ID_TO_FILTER_ID(ips_id) (ips_id | 0x0100)

/*---------------------------------------------------------------------------
  Struct defintions for packet filter setting and list of rules
---------------------------------------------------------------------------*/
typedef list_type rmnet_utils_packet_filter_node_list_type;

typedef struct 
{
  uint8  len;
  uint8 *pattern;
  uint8 *mask;
}rmnet_utils_packet_filter_rule_type;

typedef struct
{
  list_link_type                        link;
  rmnet_utils_packet_filter_rule_type rule;
  uint32 handle;
}rmnet_utils_packet_filter_node_type;

typedef struct
{
  boolean enabled;
  boolean restrictive;
  uint32  max_handle;

  rmnet_utils_packet_filter_node_list_type  filter_list;
}rmnet_utils_packet_filter_type;


/*===========================================================================

                        PUBLIC FUNCTION DECLARATIONS

===========================================================================*/



/*===========================================================================
  FUNCTION RMNET_UTILS_PACKET_FILTER_SET()

  DESCRIPTION
    Set rmnet packet filters.

  PARAMETERS
    rmnet_instance: rmnet instance
    id:             packet filter item ID

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void rmnet_utils_packet_filter_set
(
  rmnet_instance_e_type rmnet_instance,
  uint16                id
);

/*===========================================================================
  FUNCTION RMNET_UTILS_PACKET_FILTER_RETRIEVE()

  DESCRIPTION
    Retrieve packet filters based on item ID. Allocate one if not found.

  PARAMETERS
    id:       packet filter item ID
    alloc:    allocate packet filter item if not found

  RETURN VALUE
    Pointer to packet filters. NULL if failed.

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
rmnet_utils_packet_filter_type * rmnet_utils_packet_filter_retrieve
(
  uint16    id,
  boolean   alloc
);

/*===========================================================================
  FUNCTION RMNET_UTILS_PACKET_FILTER_CHANGE()

  DESCRIPTION
    enable or disable packet filter setting in rmnet

  PARAMETERS
    id         : packet filter item ID
    enabled    : enabled(1) or disabled(0)
    restrictive: Whitelist(1) or blacklist(0)

  RETURN VALUE
    boolean    : TRUE for success, FALSE otherwise

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean rmnet_utils_packet_filter_change
(
  uint16                id,
  boolean               enabled,
  boolean               restrictive
);

/*===========================================================================
  FUNCTION RMNET_UTILS_PACKET_FILTER_GET_STATE()

  DESCRIPTION
    retrieve current packet filter setting in rmnet

  PARAMETERS
    id         : packet filter item ID
    enabled    : enabled(TRUE) or disabled(FALSE)
    restrictive: Whitelist(TRUE) or blacklist(FALSE)

  RETURN VALUE
    boolean    : TRUE for success, FALSE otherwise

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean rmnet_utils_packet_filter_get_state
(
  uint16                id,
  uint8                *enabled,
  uint8                *restrictive
);

/*===========================================================================
  FUNCTION RMNET_UTILS_PACKET_FILTER_ADD_RULE()

  DESCRIPTION
    Add one filter rule into filter link list

  PARAMETERS
    id       : packet filter item ID
    handle   : pointer to rule handle
    len      : size of filter pattern/mask
    pattern  : pointer to filter pattern array
    mask     : pointer to filter mask array

  RETURN VALUE
    boolean  : TRUE for success, FALSE otherwise

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean rmnet_utils_packet_filter_add_rule
(
  uint16                id,
  uint32               *handle,
  uint8                 len,
  uint8                *pattern,
  uint8                *mask
);

/*===========================================================================
  FUNCTION RMNET_UTILS_PACKET_FILTER_DELETE_ALL_RULES()

  DESCRIPTION
    Delete all filter rules

  PARAMETERS
    id      : packet filter item ID

  RETURN VALUE
    boolean  : TRUE for success, FALSE otherwise

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean rmnet_utils_packet_filter_delete_all_rules
(
  uint16  id
);

/*===========================================================================
  FUNCTION RMNET_UTILS_PACKET_FILTER_DELETE_RULE()

  DESCRIPTION
    Delete one filter rule using rule handle

  PARAMETERS
    id: packet filter item ID

  RETURN VALUE
    boolean  : TRUE for success, FALSE otherwise

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean rmnet_utils_packet_filter_delete_rule
(
  uint16                id,
  uint32                handle
);

/*===========================================================================
  FUNCTION RMNET_UTILS_PACKET_FILTER_GET_RULE_HANDLES()

  DESCRIPTION
    Get the list of rule handles

  PARAMETERS
    id       : packet filter item ID
    p_num    : pointer to number of filters
    p_handle : pointer to array of handles

  RETURN VALUE
    boolean  : TRUE for success, FALSE otherwise

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean rmnet_utils_packet_filter_get_rule_handles
(
  uint16                id,
  uint8                *p_num,
  uint32               *p_handle
);

/*===========================================================================
  FUNCTION RMNET_UTILS_PACKET_FILTER_GET_RULE()

  DESCRIPTION
    Get filter rule using rule handle

  PARAMETERS
    id       : packet filter item ID
    handle   : rule handle
    p_buf    : pointer to bytes array in sequence of len/pattern/len/mask

  RETURN VALUE
    boolean  : TRUE for success, FALSE otherwise

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean rmnet_utils_packet_filter_get_rule
(
  uint16                id,
  uint32                handle,
  uint8                *p_buf
);

/*===========================================================================
  FUNCTION RMNET_UTILS_PACKET_FILTER_MATCH()

  DESCRIPTION
    Match data buffer against one filter patter/mask pair

  PARAMETERS
    filter_list   : pointer to filter list
    item_head_ptr : pointer to dsm item containing data packet buffer

  RETURN VALUE
    TRUE if matches, FALSE otherwise

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean rmnet_utils_packet_filter_match
(
  rmnet_utils_packet_filter_node_list_type *filter_list,
  dsm_item_type **                          item_head_ptr
);

#endif /* _DS_RMNET_UTILS_H */
