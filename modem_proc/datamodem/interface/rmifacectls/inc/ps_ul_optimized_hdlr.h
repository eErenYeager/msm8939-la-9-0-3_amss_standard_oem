#ifndef _PS_OPTIMIZED_HDLR_H
#define _PS_OPTIMIZED_HDLR_H
/*===========================================================================

                     PS_UL_OPTIMIZED_H . H
DESCRIPTION
Header file describing all UL data optimization Signal handlers

Copyright (c) 2011-2014 Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary
===========================================================================*/
/*===========================================================================

                            EDIT HISTORY FOR FILE
  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/rmifacectls/inc/ps_ul_optimized_hdlr.h#1 $

when        who    what, where, why
--------    ---    ----------------------------------------------------------
03/18/14    rk     Removed feature mobileap.
05/13/2013  pgm    Added MBIM feature flag.
03/28/2013  pgm    MBIM sig hdlr to support Multi PDN.
07/11/2012  pgm    Added RNDIS sig hdlr.
09/22/2011  pgm    Added MBIM sig hdlr.
08/19/2011  AM     Code Cleanup
08/15/2011  AM     UL optimized funtion prototypes
===========================================================================*/

/*===========================================================================

                          INCLUDE FILES FOR MODULE

===========================================================================*/

#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"

#include "ds_rmnet_meta_sm.h"
//#include "ds_rmnet_smi.h"

/*---------------------------------------------------------------------------
  ps optimized hdlr path types
  PS_UL_OPTIMIZED_HDLR_NO_PATH: Error cases, packet drops, Flow_controlled
  PS_UL_OPTIMIZED_HDLR_OPT_PATH: Um tx_cmd
  PS_UL_OPTIMIZED_HDLR_OPT_QOS_PATH: When Filters installed-ps_iface_tx_cmd()
  PS_UL_OPTIMIZED_HDLR_LEGACY_PATH: fallback_rx_sig_hdlr()
---------------------------------------------------------------------------*/
typedef enum
{
  PS_UL_OPTIMIZED_HDLR_NO_PATH      = 0,
  PS_UL_OPTIMIZED_HDLR_OPT_PATH     = 1,
  PS_UL_OPTIMIZED_HDLR_OPT_QOS_PATH = 2,
  PS_UL_OPTIMIZED_HDLR_LEGACY_PATH  = 3
} ps_ul_optimized_hdlr_path_type;

/*---------------------------------------------------------------------------
  ps nat optimized hdlr path types

  PS_NAT_OPT_HDLR_NO_PATH     : Error cases, packet drops, Flow_controlled
  PS_NAT_OPT_HDLR_LAN_PATH    : pkts to/from A5
  PS_NAT_OPT_HDLR_WWAN_PATH   : pkts to Um
  PS_NAT_OPT_HDLR_LEGACY_PATH : pkts up the Protocol Stack
---------------------------------------------------------------------------*/
typedef enum
{
  PS_NAT_OPT_HDLR_NO_PATH      = 0,
  PS_NAT_OPT_HDLR_LAN_PATH     = 1,
  PS_NAT_OPT_HDLR_WWAN_PATH    = 2,
  PS_NAT_OPT_HDLR_LEGACY_PATH  = 3,
  PS_NAT_OPT_HDLR_MAX_PATH
} ps_nat_opt_hdlr_path_type;

/*===========================================================================
FUNCTION PS_UL_OPTIMIZED_IP_QOS_RX_SIG_HDLR

DESCRIPTION
  This function is the optimized Signal handler for IP QOS mode. It decides
  if the packet needs to take the legacy path or should be forwarded to Um
  iface.

PARAMETERS
  rx_sig    : RmNet signal type.
  user_data : pointer to user data.

RETURN VALUE
  TRUE:
  FALSE:

DEPENDENCIES
  None

SIDE EFFECTS
  None.

===========================================================================*/
boolean ps_ul_optimized_ip_qos_rx_sig_hdlr
(
  rmnet_sig_enum_type    rx_sig,
  void                 * user_data
);

/*===========================================================================
FUNCTION PS_UL_OPTIMIZED_IP_QOS2_RX_SIG_HDLR

DESCRIPTION
  This function is the optimized Signal handler for IP QOSv2 (8-byte header)
  mode. It decides if the packet needs to take the legacy path or should be
  forwarded to Um iface.

PARAMETERS
  rx_sig    : RmNet signal type.
  user_data : pointer to user data.

RETURN VALUE
  TRUE:
  FALSE:

DEPENDENCIES
  None

SIDE EFFECTS
  None.

===========================================================================*/
boolean ps_ul_optimized_ip_qos2_rx_sig_hdlr
(
  rmnet_sig_enum_type    rx_sig,
  void                 * user_data
);

/*===========================================================================
FUNCTION PS_UL_OPTIMIZED_IP_NO_QOS_RX_SIG_HDLR

DESCRIPTION
  This function is the optimized Signal handler for IP NO QOS mode. It decides
  if the packet needs to take the legacy path or should be forwarded to Um
  iface.

PARAMETERS
 rx_sig    :  RmNet signal type
 user_data :  pointer to user data.

RETURN VALUE
 TRUE :
 FALSE:

DEPENDENCIES
  None

SIDE EFFECTS
  None.

===========================================================================*/
boolean ps_ul_optimized_ip_no_qos_rx_sig_hdlr
(
  rmnet_sig_enum_type    rx_sig,
  void                 * user_data
);


/*===========================================================================
FUNCTION PS_UL_OPTIMIZED_ETH_NO_QOS_RX_SIG_HDLR

DESCRIPTION
  This function is the optimized Signal handler for ETH NO QOS mode. It decides
  if the packet needs to take the legacy path or should be forwarded to Um
  iface.

PARAMETERS
  rx_sig    : RmNet signal type.
  user_data : pointer to user data.

RETURN VALUE
 TRUE:
 FALSE:

DEPENDENCIES
  None

SIDE EFFECTS
  None.

===========================================================================*/
boolean ps_ul_optimized_eth_no_qos_rx_sig_hdlr
(
  rmnet_sig_enum_type    rx_sig,
  void                 * user_data
);

/*===========================================================================
FUNCTION PS_UL_OPTIMIZED_ETH_QOS_RX_SIG_HDLR

DESCRIPTION
  This function is the optimized Signal handler for ETH QOS mode. It decides
  if the packet needs to take the legacy path or should be forwarded to Um
  iface.

PARAMETERS
  rx_sig    : RmNet signal type.
  user_data : pointer to user data.

RETURN VALUE
 TRUE:
 FALSE:

DEPENDENCIES
  None

SIDE EFFECTS
  None.

===========================================================================*/
boolean ps_ul_optimized_eth_qos_rx_sig_hdlr
(
  rmnet_sig_enum_type    rx_sig,
  void                 * user_data
);

#ifdef FEATURE_DATA_MBIM
/*===========================================================================
FUNCTION PS_UL_OPTIMIZED_MBIM_IP_RX_SIG_HDLR

DESCRIPTION
  This function is the optimized Signal handler for MBIM mode. It decides
  if the packet needs to take the legacy path or should be forwarded to Um
  iface.

PARAMETERS
  rx_sig    : RmNet signal type.
  user_data : pointer to user data.

RETURN VALUE
 TRUE:
 FALSE:

DEPENDENCIES
  None

SIDE EFFECTS
  None.

===========================================================================*/
boolean ps_ul_optimized_mbim_ip_rx_sig_hdlr
(
  rmnet_sig_enum_type   rx_sig,
  void                * user_data
);
#endif /*FEATURE_DATA_MBIM*/

/*===========================================================================
FUNCTION PS_UL_OPTIMIZED_QCNCM_IP_RX_SIG_HDLR

DESCRIPTION
  This function is the optimized Signal handler for QCNCM mode. It decides
  if the packet needs to take the legacy path or should be forwarded to Um
  iface.

PARAMETERS
  rx_sig    : RmNet signal type.
  user_data : pointer to user data.

RETURN VALUE
 TRUE:
 FALSE:

DEPENDENCIES
  None

SIDE EFFECTS
  None.

===========================================================================*/
boolean ps_ul_optimized_qcncm_ip_rx_sig_hdlr
(
  rmnet_sig_enum_type   rx_sig,
  void                * user_data
);
#endif /* _PS_OPTIMIZED_HDLR_H */

