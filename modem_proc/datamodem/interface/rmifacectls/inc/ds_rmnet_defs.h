#ifndef _DS_RMNET_DEFS_H
#define _DS_RMNET_DEFS_H
/*===========================================================================

                         D S _ R M N E T _ D E F S . H

DESCRIPTION

  Rm Network device - Definitions internal header file.

EXTERNALIZED FUNCTIONS

Copyright (c) 2005-2013 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/rmifacectls/inc/ds_rmnet_defs.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when        who    what, where, why
--------    ---    ----------------------------------------------------------
07/11/12    pgm    Added support for RNDIS agg mode.
02/02/12    cy     Added support for MBIM NTB parameters
09/18/11    wc     Nikel and MPSS.NI.1.1 port configuration
05/02/11    ua     Enabling Autoconnect to working with first instance across
                   all targets.
03/09/11    kk     Removing DCC command buffer dependency from RmNet.
06/19/10    vs     Changes for Dual IP support over single QMI instance
11/29/06    jd     Added DCTM support to RmNet
10/09/06    ks     Added rmnet cmds for Flow enable/disable.
08/23/06    ks     Created file
===========================================================================*/

/*===========================================================================

                                INCLUDE FILES

===========================================================================*/
#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"

#include "cm.h"

#if defined(FEATURE_IP_CALL)
#if defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900)
  #include "cmipapp.h"
#endif /* defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900) */
#endif /* defined(FEATURE_IP_CALL) */


/*===========================================================================

                                 DEFINITIONS

===========================================================================*/
/*---------------------------------------------------------------------------
  Net-RMSM instance enumeration.
  2 RMNET instances per QMI instance, one for IPV4 and the other for IPV6

  Important names:
  RMNET_INSTANCE_APPS_PROC_MAX --> Used to define the MAX embedded port
  RMNET_INSTANCE_LAPTOP_PROC_MIN --> Used to define the MIN laptop port
  RMNET_INSTANCE_LAPTOP_PROC_MAX --> Used to define the MAX laptop port

  NOTE: This enum should be in line with the enum 'qmi_instance_e_type'.
        Editing this enum independently of the other enum can cause boot-up
        crashes.
---------------------------------------------------------------------------*/
typedef enum
{
  RMNET_INSTANCE_MIN =  0,
#if defined(FEATURE_RMNET_PORT_CONFIG_MSM)
  /*-------------------------------------------------------------------------
    This tier is for NikeL architecture.
      Require 8 instances to support 4 dual-IP connections from apps
        Optional 2 more instances for dual-IP connections on 5th PDN
      Require 1 instance to support 1 dual-IP connection TE
  -------------------------------------------------------------------------*/
  RMNET_INSTANCE_1_1   =  RMNET_INSTANCE_MIN,
  RMNET_INSTANCE_1_2,
  RMNET_INSTANCE_2_1,
  RMNET_INSTANCE_2_2,
  RMNET_INSTANCE_3_1,
  RMNET_INSTANCE_3_2,
  RMNET_INSTANCE_4_1,
  RMNET_INSTANCE_4_2,
  RMNET_INSTANCE_5_1,
  RMNET_INSTANCE_5_2,
  RMNET_INSTANCE_6_1,
  RMNET_INSTANCE_6_2,
  RMNET_INSTANCE_7_1,
  RMNET_INSTANCE_7_2,
  RMNET_INSTANCE_8_1,
  RMNET_INSTANCE_8_2,
  RMNET_INSTANCE_9_1,
  RMNET_INSTANCE_9_2,
  RMNET_INSTANCE_10_1,
  RMNET_INSTANCE_10_2,
#if defined(FEATURE_8960_SGLTE_FUSION)
  RMNET_INSTANCE_11_1,
  RMNET_INSTANCE_11_2,
  RMNET_INSTANCE_APPS_PROC_MAX = RMNET_INSTANCE_11_2,
  RMNET_INSTANCE_LAPTOP_PROC_MIN,
  RMNET_INSTANCE_12_1 = RMNET_INSTANCE_LAPTOP_PROC_MIN,
  RMNET_INSTANCE_12_2,
  RMNET_INSTANCE_LAPTOP_PROC_MAX = RMNET_INSTANCE_12_2,
#else
  RMNET_INSTANCE_APPS_PROC_MAX = RMNET_INSTANCE_10_2,
  RMNET_INSTANCE_LAPTOP_PROC_MIN,
  RMNET_INSTANCE_11_1 = RMNET_INSTANCE_LAPTOP_PROC_MIN,
  RMNET_INSTANCE_11_2,
  RMNET_INSTANCE_LAPTOP_PROC_MAX = RMNET_INSTANCE_11_2,
#endif /* defined(FEATURE_8960_SGLTE_FUSION) */
#elif defined(FEATURE_DATA_FUSION_MDM)
  /*-------------------------------------------------------------------------
    This tier is for Fusion architecture (MSM7x30 - MDM9K velcro) - MDM Tier.
      Require 8 instances to support 4 dual-IP connections from 7x30 Apps
      Require 1 instances to support 1 dual-IP connection over single rmnet from TE
        Optional 1 more tethered instance for CSFB Fusion
  -------------------------------------------------------------------------*/
  RMNET_INSTANCE_1_1   =  RMNET_INSTANCE_MIN,
  RMNET_INSTANCE_1_2,
  RMNET_INSTANCE_2_1,
  RMNET_INSTANCE_2_2,
  RMNET_INSTANCE_3_1,
  RMNET_INSTANCE_3_2,
  RMNET_INSTANCE_4_1,
  RMNET_INSTANCE_4_2,
  RMNET_INSTANCE_5_1,
  RMNET_INSTANCE_5_2,
  RMNET_INSTANCE_6_1,
  RMNET_INSTANCE_6_2,
  RMNET_INSTANCE_7_1,
  RMNET_INSTANCE_7_2,
  RMNET_INSTANCE_8_1,
  RMNET_INSTANCE_8_2,
  RMNET_INSTANCE_APPS_PROC_MAX = RMNET_INSTANCE_8_2,
  RMNET_INSTANCE_LAPTOP_PROC_MIN,
  #ifndef FEATURE_DATA_FUSION_CSFB
    RMNET_INSTANCE_9_1= RMNET_INSTANCE_LAPTOP_PROC_MIN,
    RMNET_INSTANCE_9_2,
    RMNET_INSTANCE_10_1,
    RMNET_INSTANCE_10_2,
    RMNET_INSTANCE_LAPTOP_PROC_MAX = RMNET_INSTANCE_10_2,
  #else
    #error code not present
#endif /* FEATURE_DATA_FUSION_CSFB */
#elif defined(FEATURE_DATA_FUSION_MSM)
  /*-------------------------------------------------------------------------
    This tier is for Fusion architecture (MSM7x30 - MDM9K velcro) - MSM Tier.
      Require 3 QMI instances to support embedded calls from 7x30 Apps.
        Optional 2 instances for embedded MCAST calls from 7x30 Apps.
        Optional 2 instances for laptop calls using SMD-USB port bridge.
      Require 1 QMI instance to support 1X proxy calls from MDM9K.
  -------------------------------------------------------------------------*/
  RMNET_INSTANCE_1_1   =  RMNET_INSTANCE_MIN,
  RMNET_INSTANCE_1_2,
  RMNET_INSTANCE_2_1,
  RMNET_INSTANCE_2_2,
  RMNET_INSTANCE_3_1,
  RMNET_INSTANCE_3_2,
  RMNET_INSTANCE_4_1,
  RMNET_INSTANCE_4_2,
  RMNET_INSTANCE_5_1,
  RMNET_INSTANCE_5_2,
  RMNET_INSTANCE_APPS_PROC_MAX = RMNET_INSTANCE_5_2,
  #ifdef FEATURE_DATA_RM_NET_USES_SM_LAPTOP_INST
    RMNET_INSTANCE_LAPTOP_PROC_MIN,
    RMNET_INSTANCE_6_1 = RMNET_INSTANCE_LAPTOP_PROC_MIN,
    RMNET_INSTANCE_6_2,
    RMNET_INSTANCE_LAPTOP_PROC_MAX = RMNET_INSTANCE_6_2,
  #endif /* FEATURE_DATA_RM_NET_USES_SM_LAPTOP_INST */
  RMNET_INSTANCE_8_1,
  RMNET_INSTANCE_8_2,
#elif defined(FEATURE_DATA_RM_NET_USES_SM)
  /*-------------------------------------------------------------------------
    This tier is for regular multi-proc (7x30 or 8x60) architecture.
      Require 8 instances to support 4 dual-IP connections from apps
      Require 1 instances to support 1 dual-IP connection over single rmnet from TE
        Optional 1 more tethered instance for multi-RmNet over USB
  -------------------------------------------------------------------------*/
  RMNET_INSTANCE_1_1   =  RMNET_INSTANCE_MIN,
  RMNET_INSTANCE_1_2,
  RMNET_INSTANCE_2_1,
  RMNET_INSTANCE_2_2,
  RMNET_INSTANCE_3_1,
  RMNET_INSTANCE_3_2,
  RMNET_INSTANCE_4_1,
  RMNET_INSTANCE_4_2,
  RMNET_INSTANCE_5_1,
  RMNET_INSTANCE_5_2,
  RMNET_INSTANCE_6_1,
  RMNET_INSTANCE_6_2,
  RMNET_INSTANCE_7_1,
  RMNET_INSTANCE_7_2,
  RMNET_INSTANCE_8_1,
  RMNET_INSTANCE_8_2,
  RMNET_INSTANCE_APPS_PROC_MAX = RMNET_INSTANCE_8_2,
  #ifdef FEATURE_DATA_RM_NET_USES_SM_LAPTOP_INST
    RMNET_INSTANCE_LAPTOP_PROC_MIN,
    RMNET_INSTANCE_9_1=RMNET_INSTANCE_LAPTOP_PROC_MIN,
    RMNET_INSTANCE_9_2,
    RMNET_INSTANCE_LAPTOP_PROC_MAX = RMNET_INSTANCE_9_2,
  #endif /* FEATURE_DATA_RM_NET_USES_SM_LAPTOP_INST */

#elif defined(FEATURE_RMNET_PORT_CONFIG_MDM)
  /*-------------------------------------------------------------------------
    MDM configuration. Support 10 dual IP RMNET instances.
  -------------------------------------------------------------------------*/
  RMNET_INSTANCE_LAPTOP_PROC_MIN = RMNET_INSTANCE_MIN,
  RMNET_INSTANCE_1_1   = RMNET_INSTANCE_LAPTOP_PROC_MIN,
  RMNET_INSTANCE_10_2  = 19,
  RMNET_INSTANCE_LAPTOP_PROC_MAX = RMNET_INSTANCE_10_2,

#elif defined(FEATURE_RMNET_PORT_CONFIG_MDM_LE)
  /*-------------------------------------------------------------------------
   * MDM LE. Support 21 * 2 = 42 RMNET instances.
  -------------------------------------------------------------------------*/
  RMNET_INSTANCE_LAPTOP_PROC_MIN =RMNET_INSTANCE_MIN,
  RMNET_INSTANCE_1_1  =  RMNET_INSTANCE_LAPTOP_PROC_MIN,
  RMNET_INSTANCE_21_2  =  41,
  RMNET_INSTANCE_LAPTOP_PROC_MAX = RMNET_INSTANCE_21_2,

#else
  /*-------------------------------------------------------------------------
    This tier is for regular single-proc (6xxx) architecture.
      Require 5 instances to support 5 dual-IP connections over USB/MUX
  -------------------------------------------------------------------------*/
  RMNET_INSTANCE_LAPTOP_PROC_MIN =RMNET_INSTANCE_MIN,
  RMNET_INSTANCE_1_1   =  RMNET_INSTANCE_LAPTOP_PROC_MIN,
  RMNET_INSTANCE_1_2,
  RMNET_INSTANCE_2_1,
  RMNET_INSTANCE_2_2,
  RMNET_INSTANCE_3_1,
  RMNET_INSTANCE_3_2,
  RMNET_INSTANCE_4_1,
  RMNET_INSTANCE_4_2,
  RMNET_INSTANCE_5_1,
  RMNET_INSTANCE_5_2,
  RMNET_INSTANCE_LAPTOP_PROC_MAX = RMNET_INSTANCE_5_2,
#endif /* defined(FEATURE_RMNET_PORT_CONFIG_MSM) */
  RMNET_INSTANCE_MAX
} rmnet_instance_e_type;

#define RMNET_NUM_LAPTOP_INSTANCES \
     ((RMNET_INSTANCE_LAPTOP_PROC_MAX - RMNET_INSTANCE_LAPTOP_PROC_MIN + 1)/2 )

#if defined(FEATURE_DATA_RM_NET_USES_SM) || defined(FEATURE_DATA_FUSION_MSM) || defined(FEATURE_DATA_FUSION_MDM) || defined(FEATURE_RMNET_PORT_CONFIG_MSM)
  #define RMNET_IS_VALID_LAPTOP_INSTANCE(inst)\
    ( ( inst >= RMNET_INSTANCE_LAPTOP_PROC_MIN ) && (inst <= RMNET_INSTANCE_LAPTOP_PROC_MAX  ) )
#else
  #define RMNET_IS_VALID_LAPTOP_INSTANCE(inst)\
    ( inst <= RMNET_INSTANCE_LAPTOP_PROC_MAX  )
#endif /* defined(FEATURE_DATA_RM_NET_USES_SM) || defined(FEATURE_DATA_FUSION_MSM) ||\
        defined(FEATURE_DATA_FUSION_MDM) || defined(FEATURE_RMNET_PORT_CONFIG_MSM) */

#define RMNET_GET_LAPTOP_INSTANCE(inst)\
  ( (inst - RMNET_INSTANCE_LAPTOP_PROC_MIN)/2)

/*---------------------------------------------------------------------------
  Data format mask used in set data format.
---------------------------------------------------------------------------*/
#define RMNET_DATA_FORMAT_MASK_QOS                (0x00000001)
#define RMNET_DATA_FORMAT_MASK_LINK_PROT          (0x00000002)
#define RMNET_DATA_FORMAT_MASK_UL_DATA_AGG        (0x00000004)
#define RMNET_DATA_FORMAT_MASK_DL_DATA_AGG        (0x00000008)
#define RMNET_DATA_FORMAT_MASK_NDP_SIGNATURE      (0x00000010)
#define RMNET_DATA_FORMAT_MASK_DL_DATA_AGG_NUM    (0x00000020)
#define RMNET_DATA_FORMAT_MASK_DL_DATA_AGG_SIZE   (0x00000040)
#define RMNET_DATA_FORMAT_MASK_QOS_FORMAT         (0x00000080)
#define RMNET_DATA_FORMAT_MASK_DL_MIN_PADDING     (0x00000100)

/*---------------------------------------------------------------------------
  Default value used in set data format.
---------------------------------------------------------------------------*/
#define RMNET_DATA_FORMAT_MINIMUM_DL_DATA_AGG_SIZE   (2048)

/*---------------------------------------------------------------------------
  Default Values used in set data format for DL Aggr params
---------------------------------------------------------------------------*/
#define RMNET_RNDIS_HDR_LEN           (44)         /*RNDIS spec defined*/
#define RMNET_ETH_PKT_LEN             (1512)       /*14 byte Eth hdr + 1498*/
#define RMNET_MIN_RNDIS_PKT_SIZE       \
        (RMNET_RNDIS_HDR_LEN + RMNET_ETH_PKT_LEN)

/*Default DL aggr params that will be set in A2*/
#define RMNET_DATA_FORMAT_MAX_RNDIS_AGGR_PKTS   (15)
#define RMNET_DATA_FORMAT_MAX_RNDIS_AGGR_SIZE   \
        (RMNET_DATA_FORMAT_MAX_RNDIS_AGGR_PKTS * RMNET_MIN_RNDIS_PKT_SIZE)
#define RMNET_DATA_FORMAT_MIN_RNDIS_DL_DATA_AGG_SIZE  RMNET_MIN_RNDIS_PKT_SIZE

#define RMNET_DATA_FORMAT_QMAP_MAX_AGGR_PKTS (20)
#define RMNET_DATA_FORMAT_QMAP_DEFAULT_AGGR_PKTS (10)
#define RMNET_DATA_FORMAT_QMAP_DEFAULT_AGGR_SIZE (1536)

typedef enum
{
  RMNET_SET_DATA_FORMAT_NONE          = 0,
  RMNET_SET_DATA_FORMAT_QMI_CTL       = 1, /* To be used by QMI CTL message when setting
                                              the data format */
  RMNET_SET_DATA_FORMAT_QMI_WDA,           /* To be used by QMI WDA message when setting
                                    the data format */
  RMNET_SET_DATA_FORMAT_DTR_HGH,     /* This will be used as part of auto connect
                                    triggering the call and setting the data
                                    format as part of it.  */
  RMNET_SET_DATA_FORMAT_DTR_LOW        /* This will be used in set data format at
                                    DTR Low, allowing flexibilty at each DTR
                                    high to pick either Driver SET CTL or the
                                    configuration in the EFS */
}rmnet_set_data_fmt_e_type;

typedef enum
{
  RMNET_SET_DATA_FORMAT_SUCCESS = 1,
  RMNET_SET_DATA_FORMAT_FAILURE,
  RMNET_SET_DATA_FORMAT_NO_EFFECT
}rmnet_set_data_fmt_ret_e_type;

#endif /* _DS_RMNET_DEFS_H */
