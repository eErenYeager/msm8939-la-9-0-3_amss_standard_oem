/*=========================================================================*/
/*!
  @file
  BearerTechEx.cpp

  @brief
  This file provides implementation of the ds::Net::BearerTechEx class.

  Copyright (c) 2013 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary
*/
/*=========================================================================*/

/*===========================================================================
  EDIT HISTORY FOR MODULE

  Please notice that the changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/dsnet/src/ds_Net_BearerTechEx.cpp#1 $
  $DateTime: 2015/01/27 06:42:19 $
  $Author: mplp4svc $

  when       who what, where, why
  ---------- --- ------------------------------------------------------------
  2013-02-13 da  Created module.

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "comdef.h"
#include "ds_Utils_StdErr.h"
#include "ds_Utils_DebugMsg.h"
#include "ds_Utils_CreateInstance.h"
#include "ds_Net_BearerTechEx.h"
#include "ds_Net_Utils.h"
#include "ds_Utils_CCritSect.h"
#include <stringl/stringl.h>

using namespace ds::Net;
using namespace ds::Error;

/*===========================================================================

                     PRIVATE FUNCTION DEFINITIONS

===========================================================================*/
ds::ErrorType BearerTechEx::GetParams
(
  ParamsType paramName,
  void *     pOut,
  void *     pIn,
  int        len
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (NULL == pOut)
  {
    LOG_MSG_INVALID_INPUT_0 ("BearerTechEx::GetParams(): "
                             "NULL out arg");
    return QDS_EFAULT;
  }

  mpICritSect->Enter();
  memscpy (pOut, len, pIn, len);
  mpICritSect->Leave();

  LOG_MSG_INFO1_3 ("BearerTechEx::GetParams(): "
                   "Obj 0x%p, param %d, val %d",
                   this, paramName, *((int *)pOut));

  return AEE_SUCCESS;

} /* GetParams() */

/*===========================================================================

                     PUBLIC FUNCTION DEFINITIONS

===========================================================================*/
/*---------------------------------------------------------------------------
  CONSTRUCTOR/DESTRUCTOR
---------------------------------------------------------------------------*/
BearerTechEx::BearerTechEx
(
  uint32   technology,
  uint32   rat,
  uint32   soMask
)
: mTechnology(technology),
  mRatValue(rat),
  mSoMask(soMask),
  mpICritSect(0),
  refCnt (1)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_INFO1_1 ("BearerTechEx::GetParams(): "
                   "Obj 0x%p", this);

  if (AEE_SUCCESS != DS_Utils_CreateInstance (NULL,
                                              AEECLSID_CCritSect,
                                              (void **) &mpICritSect))
  {
    LOG_MSG_ERROR_0 ("BearerTechEx::GetParams(): "
                     "Cannot create crit sect");
    ASSERT (0);
  }

} /* BearerTechEx() */

BearerTechEx::~BearerTechEx
(
  void
)
throw()
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_INFO1_1 ("BearerTechEx::~BearerTechEx(): "
                   "Obj 0x%p", this);

  /*lint -save -e1550, -e1551 */
  DS_UTILS_RELEASEIF(mpICritSect);
  /*lint -restore */

} /* ~BearerTechEx() */

/*---------------------------------------------------------------------------
  Functions inherited from IBearerInfo
---------------------------------------------------------------------------*/

AEEResult BearerTechEx::GetTechnology(unsigned int* value)
{
  return GetParams(DS_NET_BEARER_TECH_EX_PARAMS_NETWORK_TYPE,
                   value,
                   &mTechnology,
                   sizeof(mTechnology));
}
         
AEEResult BearerTechEx::GetRatValue(unsigned int* value)
{
  return GetParams(DS_NET_BEARER_TECH_EX_PARAMS_RAT_VALUE,
    value,
    &mRatValue,
    sizeof(mRatValue));
}
         
AEEResult BearerTechEx::GetSoMask(unsigned int* value)
{
  return GetParams(DS_NET_BEARER_TECH_EX_PARAMS_SO_MASK,
    value,
    &mSoMask,
    sizeof(mSoMask));
}

