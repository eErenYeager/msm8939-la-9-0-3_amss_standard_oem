/*==========================================================================*/
/*!
  @file 
  ds_Net_Init.cpp

  @brief
  This file provides functions that are used to perform initializations 
  of DSNET module.

  Copyright (c) 2008-2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary
*/
/*==========================================================================*/
/*===========================================================================
  EDIT HISTORY FOR MODULE

  Please notice that the changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/dsnet/src/ds_Net_Init.cpp#1 $
  $DateTime: 2015/01/27 06:42:19 $$Author: mplp4svc $

  when       who what, where, why
  ---------- --- ------------------------------------------------------------
  2010-12-01 aa  Removing usage of scratchpad. Using system heap instead.
  2009-08-17 pp  DS_Net module uses system heap memory via scratchpad.
  2009-02-27 hm  Added scratchpad memory for DSNET client.
  2008-05-02 hm  Created module.

===========================================================================*/

/*---------------------------------------------------------------------------
  Include Files
---------------------------------------------------------------------------*/
#include "comdef.h"
#include "ds_Utils_DebugMsg.h"
#include "ds_Net_Init.h"
#include "ds_Net_MemManager.h"
#include "ds_Net_EventManager.h"
#include "ds_Net_Platform.h"
#include "ds_Net_NetworkFactory.h"

#include "ds_Net_INetworkFactory.h"
#include "ds_Net_ITechUMTS.h"
#include "ds_Net_CNetworkFactory.h"
#include "ds_Net_CreateInstance.h"

#include "ds_Net_NetworkFactory.h"
#include "ds_Net_TechUMTSFactory.h"

using namespace ds::Net;

/*---------------------------------------------------------------------------
  Local declarations.
---------------------------------------------------------------------------*/

void DSNetPowerup
(
  void
)
{
  int res = 0;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_INFO1_0 ("DSNetPowerup(): "
                   "Powerup init of DSNET");
  MemoryManager::MemPoolInit();

  NetPlatform::Init();

  ds::Net::EventManager::Init();

  res = NetworkFactory::InstanceInit();
  if (AEE_SUCCESS != res) {
    LOG_MSG_ERROR_1("DSSGlobals::Init(): "
      "Can't create NetworkFactory singleton (%d)", res);
    ASSERT(0);
  }

  res = TechUMTSFactory::InstanceInit();
  if (AEE_SUCCESS != res) {
    LOG_MSG_ERROR_1("DSSGlobals::Init(): "
      "Can't create TechUMTSFactory singleton (%d)", res);
    ASSERT(0);
  }

} /* DSNetPowerupInit() */

void DSNetPowerdown
(
  void
)
{
  int res = 0;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_INFO1_0 ("DSNetPowerdown(): "
                   "Powerdown init of DSNET");

  EventManager::Deinit();

  res = NetworkFactory::InstanceRelease();
  if (AEE_SUCCESS != res) {
    LOG_MSG_ERROR_1("DSSGlobals::Deinit(): "
      "NetworkFactory singleton already released (%d)", res);
    ASSERT(0);
  }

  res = TechUMTSFactory::InstanceRelease();
  if (AEE_SUCCESS != res) {
    LOG_MSG_ERROR_1("DSSGlobals::Deinit(): "
      "TechUMTSFactory singleton already released (%d)", res);
    ASSERT(0);
  }

} /* DSNetPowerdown() */


void DSNetInit
(
  void
)
{

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_INFO1_0 ("DSNetInit(): "
                   "After-task start init of DSNET");

} /* DSNetInit() */

void DSNetDeinit
(
  void
)
{

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_INFO1_0 ("DSNetDeinit(): "
                   "After-task stop init of DSNET");

} /* DSNetDeinit() */


