#ifndef DS_NET_CLASSIDINSTANTIATOR_H
#define DS_NET_CLASSIDINSTANTIATOR_H
/*===========================================================================
  @file ds_Net_ClassIDInstantiator.h

  This file defines various methods which are used to create instances of
  network interfaces.

  Copyright (c) 2010 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary
===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/dsnet/inc/ds_Net_ClassIDInstantiator.h#1 $
  $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "comdef.h"

#include "ds_Utils_StdErr.h"
#include "ds_Utils_CSSupport.h"


/*===========================================================================

                      PUBLIC DATA DECLARATIONS

===========================================================================*/
DSIQI_DECL_CREATE_INSTANCE2( ds, Net, NetworkFactory)

#endif /* DS_NET_CLASSIDINSTANTIATOR_H */
