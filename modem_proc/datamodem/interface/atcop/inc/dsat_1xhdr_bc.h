#ifndef DSAT_1XHDR_BC_H
#define DSAT_1XHDR_BC_H
/*===========================================================================

            D A T A   S E R V I C E S  1 X - H D R   B C M C S

                A T   C O M M A N D   P R O C E S S O R
                
                I N T E R N A L   H E A D E R   F I L E


DESCRIPTION
  This file contains the definitions of data structures, defined and
  enumerated constants and function prototypes required for the
  data services AT command ( BC-Rm commands ) processor.

EXTERNALIZED FUNCTIONS
dsat_1xhdr_exec_qcbcen_cmd()
  This function takes the result from the command line parser
  and executes it. It executes AT$QCBCEN command.
  
dsat_1xhdr_exec_qcbcip_cmd()
  This function takes the result from the command line parser
  and executes it. It executes AT$QCBCIP command.

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None.

Copyright (c) 2004 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/


/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/atcop/inc/dsat_1xhdr_bc.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when        who    what, where, why
--------    ---    ----------------------------------------------------------
03/04/09     sa    AU level CMI modifications.
06/08/04     vr    Fixed lint error
04/06/04     vr    Initial revision.
===========================================================================*/


/*===========================================================================

                          INCLUDE FILES FOR MODULE

===========================================================================*/
#include "comdef.h"
#include "customer.h"

#include "dsati.h"

/*===========================================================================

                          EXTERNAL FUNCTION DEFINITIONS

==========================================================================*/
/*===========================================================================
  FUNCTION DSAT_1XHDR_EXEC_QCBCEN_CMD

  DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes AT$QCBCEN command.
  Enable or disable BCMCS service to the laptop.
  0 - Disable BCMCS service to the laptop.
  1 - Enable BCMCS service to the laptop.
  
  DEPENDENCIES
  Valid BCMCS IP addresses that the laptop is interested in must be 
  previously set using the AT$QCBCIP command, before enabling BCMCS service 
  to the laptop. Otherwise, the BCMCS service to the laptop will be enabled 
  but the laptop won't receive any BCMCS content. 
  
  RETURN VALUE
  Returns an enum that describes the result of the command execution.
  possible values:
  DSAT_ERROR : if there was any problem in executing the command
  DSAT_OK : if it is a success.
  
  SIDE EFFECTS
  None.
===========================================================================*/
dsat_result_enum_type dsat_1xhdr_exec_qcbcen_cmd
(
  dsat_mode_enum_type      mode,        /*  AT command mode:            */
  const dsati_cmd_type     *tab_entry,  /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type            *rb_ptr      /*  Place to put response       */
);

/*===========================================================================
  FUNCTION DSAT_1XHDR_EXEC_QCBCIP_CMD

  DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes AT$QCBCIP command.
  
  Set the multicast IP addresses that the laptop wants to listen to for BCMCS
  service. The parameter for the AT command is a string that has 3 IPv4
  addresses. This function checks to ensure that there are 3 IPv4 addresses 
  and they are all either in the multicast range or 0.0.0.0 If this is true,
  then the BCMCS IP addresses that the laptop wants to listen to are set to
  these three IP addresses and they are also written to NV. If the number of
  IP addresses in the string is not 3 or at least one of them is not a valid
  multicast IP address (0.0.0.0), ALL the BCMCS IP addresses that the laptop
  is interested in are left untouched.
  
  Setting the multicast IP addresses for BCMCS does not automatically enable
  BCMCS service to the laptop. That is controlled by the AT$QCBCEN command.

  DEPENDENCIES
  None
  
  RETURN VALUE
  Returns an enum that describes the result of the command execution.
  possible values:
  DSAT_ERROR : if there was any problem in executing the command
  DSAT_OK : if it is a success.
  
  If the return value is DSAT_OK that means the IP addresses have been 
  committed to EFS, otherwise the IP addresses are unchanged.
  
  SIDE EFFECTS
  None.
===========================================================================*/
dsat_result_enum_type dsat_1xhdr_exec_qcbcip_cmd
(
  dsat_mode_enum_type      mode,        /*  AT command mode:            */
  const dsati_cmd_type     *tab_entry,  /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type            *rb_ptr      /*  Place to put response       */
);

#endif /* DSAT_1XHDR_BC_H */
