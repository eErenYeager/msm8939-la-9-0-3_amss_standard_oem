#ifndef DSAT_1XHDR_BCCTAB_H
#define DSAT_1XHDR_BCCTAB_H
/*===========================================================================

                        D A T A   S E R V I C E S
                A T   C O M M A N D   P R O C E S S O R

                B C M C S  C O M M A N D   T A B L E S                
                I N T E R N A L   H E A D E R   F I L E


DESCRIPTION
  This file contains the definitions of data structures, defined and
  enumerated constants, and function prototypes required for the
  data services AT command processor command tables that define
  1X-HDR BCMCS service specific commands.
  

Copyright (c) 2004 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/atcop/inc/dsat_1xhdr_bcctab.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
08/23/04    vr     Moved #define of BC IP string len to internal header file.
04/09/04    vr     Initial revision of file.

===========================================================================*/

/*===========================================================================

                          INCLUDE FILES FOR MODULE

===========================================================================*/
#include "comdef.h"
#include "customer.h"

#include "dsati.h"
#include "dsatctab.h"

/*===========================================================================
  1X-HDR BCMCS related AT command table
===========================================================================*/
extern const dsati_cmd_type dsat_1xhdr_bc_table[ ];
extern const unsigned int dsat_1xhdr_bc_table_size;

/* Data declarations for 1X-HDR BCMCS commands */
extern dsat_string_item_type  dsat_1xhdr_qcbcip_val[];
extern dsat_num_item_type     dsat_1xhdr_qcbcen_val;

extern dflm_type dsat_1xhdr_qcbcen_dflm [];

#endif /* DSAT_1XHDR_BCCTAB_H */
