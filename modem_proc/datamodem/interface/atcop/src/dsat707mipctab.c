/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                        D A T A   S E R V I C E S
                A T   C O M M A N D   P R O C E S S O R

               M I P   C O M M A N D   T A B L E S

GENERAL DESCRIPTION
  This module contains the command tables and data definitions required
  to define the Mobile IP AT modem commands for the IS-707 mode.

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None

Copyright (c) 1995-2013 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$PVCSPath: L:/src/asw/MM_DATA/vcs/dsat707mipctab.c_v   1.5   07 Feb 2003 10:24:10   sramacha  $
$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/atcop/src/dsat707mipctab.c#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
01/19/12   sk      Feature cleanup.
07/22/11   dvk     Global Variable Cleanup
01/11/11   ad      Remove extern usage.used get/set API for command associated  
                   value pointers.
10/25/10   ad      Init Changes for DSDS. 
05/10/10   kk      Featurization fix for $QCMIPNAI.
09/04/09   ss      CMI SU level modifications.
04/28/04   jd      Include dsmip.h for length constants, remove unused vars
09/18/02   jay     Modified comments and formatted code.
04/03/01   rsl     Initial release.

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"

#ifdef FEATURE_DATA_IS707

#include "dsati.h"
#include "dsat707mip.h"
#include "dsat707mipctab.h"
#include "dsatparm.h"
#include "ds_1x_profile.h"
#include "msg.h"

#ifdef FEATURE_DS_MOBILE_IP
#include "dsmip_v.h"
#endif /* FEATURE_DS_MOBILE_IP */

#ifdef FEATURE_DS_MOBILE_IP

/*=========================================================================*/
/*  Mobile IP related AT command table */
/*=========================================================================*/

const dsati_cmd_type dsat707_mip_table [] =
{
#ifdef FEATURE_DS_AT_TEST_ONLY
  { "$QCMIP",
      EXTENDED | LOCAL_TEST | COMMON_CMD,
      SPECIAL_NONE,
      1,
      DSAT707_MIP_QCMIP_IDX,
      NULL,
      dsat707_exec_qcmip_cmd,
      NULL },
#endif /* FEATURE_DS_AT_TEST_ONLY */
/*-------------------------------------------------------------------------*/
  { "$QCMIPP",
      EXTENDED  | LOCAL_TEST | DO_PREF_CMD,
      SPECIAL_NONE,
      1,
      DSAT707_MIP_QCMIPP_IDX,
      NULL,
      dsat707_exec_qcmipp_cmd,
      NULL },
/*-------------------------------------------------------------------------*/
#ifdef FEATURE_DS_AT_TEST_ONLY
  { "$QCMIPT",
      EXTENDED  | LOCAL_TEST | DO_PREF_CMD,
      SPECIAL_NONE,
      1,
      DSAT707_MIP_QCMIPT_IDX,
      NULL,
      dsat707_exec_qcmipt_cmd,
      NULL },
#endif /* FEATURE_DS_AT_TEST_ONLY */
/*-------------------------------------------------------------------------*/
  { "$QCMIPEP",
      EXTENDED  | LOCAL_TEST | DO_PREF_CMD,
      SPECIAL_NONE,
      1,
      DSAT707_MIP_QCMIPEP_IDX,
      NULL,
      dsat707_exec_qcmipep_cmd,
      NULL },
/*-------------------------------------------------------------------------*/
  { "$QCMIPMASS",
      EXTENDED  | LOCAL_TEST | STRING | NO_QUOTE | DO_PREF_CMD,
      SPECIAL_QCMIPMASS,
      MIP_MN_AAA_SS_MAX_LEN+1,
      DSAT707_MIP_QCMIPMASS_IDX,
      NULL,
      dsat707_exec_qcmipmass_cmd,
      NULL },
/*-------------------------------------------------------------------------*/
  { "$QCMIPMHSS",
      EXTENDED | LOCAL_TEST | STRING | NO_QUOTE | DO_PREF_CMD,
      SPECIAL_QCMIPMHSS,
      MIP_MN_HA_SS_MAX_LEN+1,
      DSAT707_MIP_QCMIPMHSS_IDX,
      NULL,
      dsat707_exec_qcmipmhss_cmd,
      NULL },
/*-------------------------------------------------------------------------*/
  { "$QCMIPMASPI",
      EXTENDED  | LOCAL_TEST | DO_PREF_CMD,
      SPECIAL_QCMIPMASPI,
      2,
      DSAT707_MIP_QCMIPMASPI_IDX,
      NULL,
      dsat707_exec_qcmipmaspi_cmd,
      NULL },
/*-------------------------------------------------------------------------*/
  { "$QCMIPMHSPI",
      EXTENDED  | LOCAL_TEST | DO_PREF_CMD ,
      SPECIAL_QCMIPMHSPI,
      2,
      DSAT707_MIP_QCMIPMHSPI_IDX,
      NULL,
      dsat707_exec_qcmipmhspi_cmd,
      NULL },
/*-------------------------------------------------------------------------*/
  { "$QCMIPRT",
      EXTENDED | LOCAL_TEST | DO_PREF_CMD,
      SPECIAL_NONE,
      2,
      DSAT707_MIP_QCMIPRT_IDX,
      NULL,
      dsat707_exec_qcmiprt_cmd,
      NULL },
/*-------------------------------------------------------------------------*/
#ifdef FEATURE_MDN_BASED_NAI
  { "$QCMIPNAI",
      EXTENDED | READ_ONLY | LOCAL_TEST | STRING | NO_QUOTE | DO_PREF_CMD,
      SPECIAL_NONE,
      MAX_NAI_LEN,
      DSAT707_MIP_QCMIPNAI_IDX,
      NULL,
      dsat707_exec_qcmipnai_cmd,
      NULL },
#else /* FEATURE_MDN_BASED_NAI */
  { "$QCMIPNAI",
      EXTENDED  | LOCAL_TEST | STRING | NO_QUOTE | DO_PREF_CMD,
      SPECIAL_NONE,
      MAX_NAI_LEN,
      DSAT707_MIP_QCMIPNAI_IDX,
      NULL,
      dsat707_exec_qcmipnai_cmd,
      NULL },
#endif /* FEATURE_MDN_BASED_NAI */
/*-------------------------------------------------------------------------*/
  { "$QCMIPHA",
      EXTENDED  | LOCAL_TEST | STRING | NO_QUOTE | DO_PREF_CMD,
      SPECIAL_QCMIPHA,
      16,
      DSAT707_MIP_QCMIPHA_IDX,
      NULL,
      dsat707_exec_qcmipha_cmd,
      NULL },
/*-------------------------------------------------------------------------*/
  { "$QCMIPPHA",
      EXTENDED  | LOCAL_TEST | STRING | NO_QUOTE | DO_PREF_CMD,
      SPECIAL_QCMIPPHA,
      16,
      DSAT707_MIP_QCMIPPHA_IDX,
      NULL,
      dsat707_exec_qcmippha_cmd,
      NULL },
/*-------------------------------------------------------------------------*/
  { "$QCMIPSHA",
      EXTENDED| LOCAL_TEST | STRING | NO_QUOTE | DO_PREF_CMD,
      SPECIAL_QCMIPSHA,
      16,
      DSAT707_MIP_QCMIPSHA_IDX,
      NULL,
      dsat707_exec_qcmipsha_cmd,
      NULL },
/*-------------------------------------------------------------------------*/
  { "$QCMIPGETP",
      EXTENDED  | LOCAL_TEST | DO_PREF_CMD,
      SPECIAL_NONE,
      1,
      DSAT707_MIP_QCMIPGETP_IDX,
      NULL,
      dsat707_exec_qcmipgetp_cmd,
      NULL },
/*-------------------------------------------------------------------------*/
  { "$QCMIPMASSX",
      EXTENDED | LOCAL_TEST | STRING | DO_PREF_CMD,
      SPECIAL_QCMIPMASSX,
      2*MIP_MN_AAA_SS_MAX_LEN+1,
      DSAT707_MIP_QCMIPMASSX_IDX,
      NULL,
      dsat707_exec_qcmipmassx_cmd,
      NULL },
/*-------------------------------------------------------------------------*/
  { "$QCMIPMHSSX",
      EXTENDED  | LOCAL_TEST | STRING | DO_PREF_CMD,
      SPECIAL_QCMIPMHSSX,
      2*MIP_MN_HA_SS_MAX_LEN+1,
      DSAT707_MIP_QCMIPMHSSX_IDX,
      NULL,
      dsat707_exec_qcmipmhssx_cmd,
      NULL }
/*-------------------------------------------------------------------------*/
};

/* Size of Mobile IP command table */
const unsigned int dsat707_mip_table_size = ARR_SIZE( dsat707_mip_table );

#endif /* FEATURE_DS_MOBILE_IP */
#endif /* FEATURE_DATA_IS707     */

