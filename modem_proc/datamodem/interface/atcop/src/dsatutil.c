/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                        D A T A   S E R V I C E S

                        A T   C O M M A N D
                        
                        U T I L I T Y   S E R V I C E S

GENERAL DESCRIPTION
  This module contains utility functions that are used in executing/processing
  AT commands.

EXTERNALIZED FUNCTIONS

  dsatutil_strncmp_ig_sp_case
    This function compares two strings for <n> characters, ignoring case.
    Lower case letters are converted to upper case before the 
    difference is taken.
	

  dsatutil_fmt_num_val
    This function formats the result of a value inquiry "?" for a normal
    numeric variable.  The return string is a null terminated 3 - digit
    decimal number with leading zeros.

  dsatutil_fmt_num_val_ex
    This function formats the value of an extended format numeric
    variable.  The value may be formatted as the result of a READ
    "?" command, or in the form of a SET command for use in uploading
    the configuration.

   dsatutil_fmt_mixed_param_val
    This function formats the values of a mixed parameter.
    The values may be formatted as the result of a READ "?" command,
    or in the form of a SET command for use in uploading the configuration.

  dsatutil_fmt_num_range
    This function formats the result of a range inquiry "=?" command
    for a numeric variable.

  dsatutil_fmt_string_range
    This function formats the result of a range inquiry "=?" for a string
    variable.

  dsatutil_fmt_string_val
    This function formats the value of an extended format string
    variable.  The value may be formatted as the result of a READ
    "?" command, or in the form of a SET command for use in uploading
    the configuration.

  dsatutil_put_num_arg
    This function checks the value and number of numeric arguments for
    a command, and, if they are correct, stores the value(s) in the
    parameter.

  dsatutil_put_string_arg
    This checks an extended format string WRITE command for correct
    syntax and string length.

  dsatutil_itoa
    This function converts an integer to ASCII, using a specified
    radix. The resulting string is null terminated.

  dsatutil_atoi
    This function converts an ASCII string to an integer, using a specified
    radix.  Spaces are ignored. Letters used for radices above 10 may be of
    either case.

  dsatutil_strcmp_ig_sp_case
    This function compares two null terminated strings, ignoring case.

  dsatutil_strip_quotes_out
    This function strips the quotes out from the quoted_str, the results
    are in out_str as a null-terminated string.  str_len is the max length 
    of the out_str string, the applocated space for out_str.

  dsatutil_reset_password
    Clear the password variable or specified length.
    The password string is set to 0xFF, null terminated.  

  dsatutil_append_dsm_item
    This function checks the buffer length against the passed threshold
    value.  If the threshold is exceeded, a new DSM item is requested and
    appended to the buffer chain.  An error is generated if no dsm_items
    are available.

  dsatutil_validate_dial_char
    This function examines the passed dial string charater and returns a
    validation result.  It is up to the caller to act upon the result as
    approppriate within its context.

  dsatutil_srch_list
    This function searchs a list of strings, and returns the index
    to the item found.
      
  dsatutil_init_pkt_dial_string_from_nv
    This function reads Packet Dial Sting lookup table from the NV during
    initialization. If these items were never written in NV, default
    values are loaded and stored to NV.
    
  dsatutil_convert_tuple
    This function checks for the validity of known tuple formats and converts
    between string and number representations.  For IPv6 addresses, the number
    pointer is assumed to be an array of uint32 values.

  dsatutil_convert_chset
    This function performs conversion of a string in one character set 
    to another supported character set.

  dsatutil_init_timers
    This function registers timers with the opeating system.

  dsatutil_dispatch_timer_handler
    This function dispatches the common timer expiry event to the
    correct timer handler.

  dsatutil_swapn_word_msb_lsb
    This function is used to swap the msb and lsb of the given word array

  dsatutil_reverse_word_arr
    This function reverses the given word array by the 
    specified number of words

  dsatutil_reversen_byte
    This function reverses the given byte array by the specified 
    number of bytes.

INITIALIZATION AND SEQUENCING REQUIREMENTS

   Copyright (c) 2001 - 2014 by Qualcomm Technologies Incorporated.
   All Rights Reserved.
   Qualcomm Confidential and Proprietary.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/


/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$PVCSPath: L:/src/asw/MM_DATA/vcs/dsatutil.c_v   1.7   11 Nov 2002 14:02:48   sbandaru  $
$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/atcop/src/dsatutil.c#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $


when       who     what, where, why
--------   ---     -----------------------------------------------------------------
7/27/14    pg      ATCoP changes for Data Plus Data.
06/27/14   sc      Fixed PLMN name issue having '@'.
04/15/14   sc      Fixed ATCoP KW errors.
04/14/14   tk      Replaced __FILE__ with __FILENAME__ in DSM API wrappers.
03/11/14   tk      Optimized debug macros usage in ATCoP.
02/15/14   pg      Added support for SGLTE+G
01/22/14   sc      Fixed compilation errors when FEATURE_SECONDARY_PDP  is undefined.
01/02/14   sc      Fixed static code bugs.
12/31/13   sc      Used cm_user* api instead of sys_sel* api
12/25/13   tk      Enhanced $QCSIMAPP command for better user experience.
11/18/13   sc      Fixed ATCoP KW errors.
11/12/13   sc      Added support for $QCCSGCOPS command.
10/18/13   sc      Added support for $QCNSP, $QCSIMT, $QCRCIND commands and
                   REMOTE CALL END, REMOTE RING, REMOTE ANSWER
                   unsolicited result codes.
10/11/13   sc      Fixed compilation errors when FEATURE_WCDMA is removed.
09/13/13   sc      Fixed KW errors.
08/01/13   sc      Fixed KW errors.
07/16/13   pg      Fixed no ^MODE URC when UE goes out of service and comes back
07/02/13   pg      Migrated to MMGSDI API to find PLMN info in SE13 table
07/01/13   tk      Fixed issue in memory allocation for SGLTE second stack.
04/08/13   tk      Refactored the code in dsatutil_convert_tuple().
01/28/12   sk      SE13 table updated to 26-Nov-2012 version released by GSMA.
01/08/12   sk      Encoding type MMGSDI_EONS_ENC_UCS2_PACKED added.
01/23/13   sk      DS command buffer memory reduction.
11/07/12   sk      SE13:Fixed parenthesis issue & Updated version to 05-OCT-12. 
11/30/12   tk      ATCoP changes for Triton DSDA.
11/09/12   sk      Modified plmn_by_rat api to report name even if RAT not macthes.
10/16/12   sk      uinetwk_network_table updated to latest SE13 list.
08/22/12   sk      Mainlining Nikel Changes.
07/26/12   sb      Fixed KW Errors.
07/25/12   tk      Added new entries in SE13 table as per Carrier requirements.
07/16/12   tk      Added RAT input to lookup SE13 table.
06/01/12   sk      TDS mode changes.
05/18/12   tk      Migrated to MSG 2.0 macros
05/18/12   sk      Fixed ATD command issue.
04/20/12   sk      DSAT_NIKEL featurization changes.
02/20 12   sb      Fixed Compiler Warnings.
02/17/12   sb      Fixed Compiler and KW Warnings.
02/06/12   ad      Added CSTF/CEN/CGPIAF/CVMOD command support.
01/25/12   nd      Deprecating API cm_get_pref_srv_type_for_packet_orig.
01/19/12   sk      Feature cleanup.
10/20/11   nc      Added support for CUAD/CEAP/CERP commands.
02/10/11   ad      Fixed +CGDCONT issue.
01/11/11   ad      Remove extern usage.used get/set API for command associated  
                   value pointers.
12/07/10   ad      Added Dual IMEI support  for DSDS.
11/22/10   nc      Fixed an unaligned access crash in 'dsatutil_convert_ipaddr'.
10/25/10   ad      Init Changes for DSDS. 
05/31/10   ad      Added support for S7 command.
05/10/10   kk      Mainlining pending GOBI changes.
01/04/10   vk      Removing certain macros from PDP reg API header file
                   that cause problems with autogenerated RPC stub code
11/16/09   ca      Added utility function dsatutil_reversen_byte().
12/15/09   nc      Featurisation changes for LTE.
10/12/09   vk      PDP profile changes for IPv4v6 support
09/22/09   vrk	   Merged LTE changes.
06/29/09   vg	   Replacing MSG_XXX with MSG_SPRINTF_xxx.
06/25/09   vk      Removing use of certain deprecated macros in pdp reg api
05/19/09   vk      Defeaturization of PDP registry for CMI
04/29/09   ua      Fixed Lint Critical Errors.
04/29/09   ua      Fixed compiler warnings. 
02/02/09   bs      Added support for extended IRA table.
12/29/08   nc      Added support for +VTS Command
11/28/08   cs      Off-Target build Compiler and Lint warning fixes.
10/23/08   bs      Added support for 1X AT Phonebook commands.
09/03/08   bs      Fixed Alignment issues for MDM8200.
07/16/08   ua      Added utility function dsatutil_strncmp_ig_sp_case
07/24/08   nc      Added util functions dsatutil_swap_msb_lsb_word_arr() and
                   dsatutil_reverse_word_arr()
03/24/08   bs      Fixed Klocwork criticals.
11/24/07   ua      Fixed Lint-lows. 
07/05/07   ss      Full UCS2 character support for phone book commands.
05/02/07   ua      Fixed lint medium errors. 
03/13/07   sa      Correcting the usage of strlcpy.
02/26/07   ua      Correcting Lint fixes. 
02/15/07   ss      Fixed lint high errors
02/14/07   ph      Multiple out-of-bound checks
02/14/07   pkp     Fixed Klocwork (NULL dereference) errors.
02/13/07   pkp     Fixed RVCT compiler warnings.
01/30/07   ss      Replaced banned string API calls.
01/29/07   ua      Added support for GSM 7bit '@' character (0x00).
06/28/06   au      Correct PS data feature wrappings.
03/30/06   snb     Correct PS data feature wrappings.
02/28/06   snb     Fix +CGTFT TOS/Mask conversion.
01/12/06   snb     Support PBM 8bit alphabet conversions.
01/06/06   snb     Correctly NULL terminate password on reset.
12/08/05   snb     Add IPV6 support and Lint corrections
11/09/05   snb     Lint errors.
10/05/05   snb     Add timer to wait for dsm items to be freed by USB.
04/20/05    sb     Library function to send URC.
02/07/05   tkk     Renamed function "is_date_valid" according to the coding 
                   conventions.
12/29/04   snb     Further changes to allow use of character set conversion 
                   function with 8bit DCS SMSes and fix for packet dial string.
11/22/04    rr     Set higher order byte of profile number to DS_UMTS_PDP_
                   ATCOP_PROFILE_FAMILY to access profiles from 
                   "pdp_profile/atcop_profiles" directory
11/18/04   snb     Add support for GSM and UCS2 character sets to +CSCS command
10/27/04   tkk     Added dsatutil_strtok/is_date_valid/is_leap_year for +CCLK
                   command processing.  
10/21/04   snb     Correct parsing of packet dial strings containing
                   multiple CIDs.
10/18/04   ar      Add generic timer support
04/09/04   snb     Fix for problem with dsat_pkt_dial_string_table size.
11/19/03   ar      Added dsatutil_convert_tuple() routine, replaces
                   obsolete dsatutil_format_ipaddr().
07/09/03   ar      Use DSMGR mode for pkt dial str lookup if CM mode invalid.
06/12/03   ar      Add packet dial string lookup table support. Some cleanup.
04/17/03   sb      Allow 0-1 options in *98 dial string  and 0-3 options in 
                   *99 dial string.
04/02/03   ar      Correct null parameter handling for BROKEN_RANGE attribute
03/26/03   ar      Fix formatting in diagnostic messages
03/24/03   ar      Enforce cid option position on packet dial string
03/17/03   ar      Correct test response formatting for BROKEN_RANGE logic.
03/07/03   ar      Remove FEATURE_DATA_ETSI_SUPSERV wrappers
02/20/03   wx      Return (x) instead of (x-x) for test command when a
                   command's numerical range is only one value
02/20/03   wx      Accept leading 0's for numerical arguments
01/24.03   ar      Migrated context cmds to persistent PDP profile support 
01/20/03   ar      Wrapped dsat_cgdcont_contexts under ETSI Packet FEATURE
01/17/03   wx      Added dsatutil_fill_modifier_info()
12/31/02   wx      Add function dsatutil_strcmp_pkt_dial_str and 
                   dsatutil_check_opt_dial_str
01/07/03   wx      Import dsat707_pkt_orig_str
11/11/02   sb      Definition of dsatutil_srch_list() function
09/18/02   jay     Removed casting to word in calls to itoa.
09/09/02   ar      Added dsatutil_validate_dial_char routine
06/27/02   ar      Added dsatutil_append_dsm_item routine
02/15/02   ar      Added dsatutil_format_ipaddr routine
10/09/01   wx      Modify function dsatutil_put_string_list_arg so that it
                   supports double quoted arguments
08/10/01   sjd     Initial release to MSM5200 archives.
05/15/01   sb      Created module.

===========================================================================*/


/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"

#include <stringl/stringl.h>


#if defined(T_WINNT)
#error code not present
#endif /* WINNT */
#include "dstaski.h"
#include "nv.h"
#include "dsati.h"
#include "err.h"
#include "msg.h"
#include "amssassert.h"
#include "ps_utils.h"
#include <string.h>
#include <stdio.h>
#include "dsatctab.h"
#include "dsatvoice.h"
#include "ds3gmgr.h"

#include "dsatact.h"

#ifdef FEATURE_DATA_IS707 
#include  "dsat707extctab.h"
#include  "dsat707faxctab.h"
#include  "dsat707hdrctab.h"
#include  "dsat707mipctab.h"
#include  "dsat707pstatsctab.h"
#include  "dsat707smsctab.h"
#include  "dsat707vendctab.h"
#endif /* FEATURE_DATA_IS707   */
#ifdef FEATURE_BCMCS
#include  "dsat_1xhdr_bcctab.h"
#endif /* FEATURE_BCMCS */

#ifdef FEATURE_DSAT_ETSI_MODE
#include "dsatetsicall.h"
#include "dsatetsictab.h"
#include "dsatetsime.h"
#include "dsatetsicmif.h"
#endif /* FEATURE_DSAT_ETSI_MODE */

#ifdef FEATURE_DSAT_ETSI_DATA
#include "dsumtspdpreg.h"
#endif /* FEATURE_DSAT_ETSI_DATA */

#include "dsat707mip.h"
#include "dsatetsipkt.h"

/*===========================================================================

            LOCAL DEFINITIONS AND DECLARATIONS FOR MODULE

  This section contains local definitions for constants, macros, types,
  variables and other items needed by this module.

===========================================================================*/

#define UI_WIDE  12

/*-------------------------------------------------------------------------
  These macros used to be in dsumtspdpreg.h but had to removed from there 
  due to compilation issues seen with pdp reg rpc stub code.
-------------------------------------------------------------------------*/
#define pdp_addr_ipv4 pdp_addr.pdp_addr_ipv4
#define pdp_addr_ipv6 pdp_addr.pdp_addr_ipv6

/*-------------------------------------------------------------------------
            Import External Variables
-------------------------------------------------------------------------*/
/* None */

/*-------------------------------------------------------------------------
            Local Variables
-------------------------------------------------------------------------*/
/* To get whether the originated call is in dialing or alerting state. */
boolean dsat_mo_dailing_state[MAX_ACTIVE_SUBS];

#ifdef FEATURE_DSAT_ETSI_MODE
/*-------------------------------------
  Service Centers for Packet calls.
---------------------------------------*/
LOCAL byte etsi_pkt_call_SC98[] = "*98";
LOCAL byte etsi_pkt_call_SC99[] = "*99";
#endif /* FEATURE_DSAT_ETSI_MODE */

/*-------------------------------------
   Packet Dial String lookup table 
---------------------------------------*/
LOCAL dsati_pkt_dial_string_table_s_type dsat_pkt_dial_string_table;
LOCAL boolean pkt_mode_override;

/*-------------------------------------
  Table to convert IRA characters to GSM 7 bit default alphabets
  Refer to 3G TS 27.005 Annex A for more details                 
---------------------------------------*/
const uint8 ira_to_gsm7[256] =
{
  255, 255, 255, 255, 255, 255, 255, 255, 255, 255,  10, 255, 255,  13, 255, 255,
  255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
  32,  33,  34,  35,   2,  37,  38,  39,  40,  41,  42,  43,  44,  45,  46,  47,
  48,  49,  50,  51,  52,  53,  54,  55,  56,  57,  58,  59,  60,  61,  62,  63,
  32,  65,  66,  67,  68,  69,  70,  71,  72,  73,  74,  75,  76,  77,  78,  79,
  80,  81,  82,  83,  84,  85,  86,  87,  88,  89,  90, 255, 255, 255, 255,  17,
  255,  97,  98,  99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111,
  112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 255, 255, 255, 255, 255,
  255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
  255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
  255, 64, 255, 1, 36, 3, 255, 95, 255, 255, 255, 255, 255, 255, 255, 255,
  255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 96,
  255, 255, 255, 255, 91, 14, 28, 9, 255, 31, 255, 255, 255, 255, 255, 255,
  255, 93, 255, 255, 255, 255, 92, 255, 11, 255, 255, 255, 94, 255, 255, 30,
  127, 255, 255, 255, 123, 15, 29, 255, 4, 5, 255, 255, 7, 255, 255, 255,
  255, 125, 8, 255, 255, 255, 124, 255, 12, 6, 255, 255, 126, 255, 255, 255
};

/*-------------------------------------
  Table to convert GSM 7 bit default alphabets to IRA characters 
---------------------------------------*/
const uint8 gsm7_to_ira[128] =
{
  64, 163,  36, 165, 232, 233, 249, 236, 242, 199,  10, 216, 248,  13, 197, 229,
  255,  95, 255, 255, 255, 255, 255, 255, 255, 255, 255, 32, 198, 230, 223, 201,
  32,  33,  34,  35, 164,  37,  38,  39,  40,  41,  42,  43,  44,  45,  46,  47,
  48,  49,  50,  51,  52,  53,  54,  55,  56,  57,  58,  59,  60,  61,  62,  63,
  161,  65,  66,  67,  68,  69,  70,  71,  72,  73,  74,  75,  76,  77,  78,  79,
  80,  81,  82,  83,  84,  85,  86,  87,  88,  89,  90, 196, 214, 209,  220, 167,
  191,  97,  98,  99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111,
  112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 228, 246, 241, 252, 224
};

/* Escape character to enter GSM alphabet extension table of 23.038 */
#define GSM_EXT_TBL_ESC_CHAR 0x1B
/* minimum and maximum acceptable value for characters in these alphabets:
   0 value cannot be accepted for NULL terminated strings used throughout
   ATCOP and 0x7F is max possible in 7 bit alphabet */
#define MIN_IRA_GSM_CHAR 0x01
#define MAX_GSM_CHAR     0x7F
#define MAX_IRA_CHAR     0xFF
#define MAX_8BIT_CHAR    0xFF

LOCAL const char hex_chars[] = "0123456789ABCDEF";

/* Characters in the "C0 control and Basic Latin" code chart of UTF-16 that 
   aren't common to IRA T.50(C0+G0) and GSM 7bit alphabets */
LOCAL const char basic_latin_not_common[] = 
{
  0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0B,0x0C,0x0E,0x0F,0x10,0x11,0x12,
  0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x1B,0x1C,0x1D,0x1E,0x1F,0x24,0x40,0x5B,
  0x5C,0x5D,0x5E,0x5F,0x60,0x7B,0x7C,0x7D,0x7E,0x7F,0x00
};

/* Complementary arrays for converting between PBM 8bit and UCS2 hex 4-tuples:
   for those values that aren't common in the range supported by both */
LOCAL const char pbm8_to_ucs2_not_common[] = 
  "\x90\x92\x93\x94\x95\x96\x97\x98\x99\x9A";
LOCAL const byte ucs2_to_pbm8_not_common[][8] = 
{
  "0394", "03A6", "0393", "039B", "03A9", 
  "03A0", "03A8", "03A3", "0398", "039E",
  ""
};
 
LOCAL const byte ucs2_to_pbm8_not_supported[][8] =
{
  "0090", "0092", "0093", "0094", "0095", 
  "0096", "0097", "0098", "0099", "009A",
  ""
};

LOCAL const char pbm8_to_gsm7_exttbl[] = "\x5B\x5C\x5D\x5E\x7B\x7C\x7D\x7E";
LOCAL const char gsm7_to_pbm8_exttbl[] = "\x3C\x2F\x3E\x14\x28\x40\x29\x3D";

/*---------------------------------------------------------------------------
     Operating system timers
---------------------------------------------------------------------------*/
LOCAL void dsatutil_timer_cb(unsigned long timer_id);

dsat_timer_s_type dsatutil_timer_table[] =
{
  {DSAT_TIMER_VOICE_RING,
   &dsatvoice_ring_timer, dsatutil_timer_cb,
   dsatvoice_timer_expired_handler,
   0, FALSE, FALSE, &dsatvoice_sys_mode[PRIMA_SUBS]}  /* Duration varies, set in cmd handler */

#ifdef FEATURE_DUAL_ACTIVE
  ,{DSAT_TIMER_VOICE_RING_2,
   &dsatvoice_ring_timer_2, dsatutil_timer_cb,
   dsatvoice_timer_expired_handler,
   0, FALSE, FALSE, &dsatvoice_sys_mode[SECON_SUBS]}  /* Duration varies, set in cmd handler */
#endif /* FEATURE_DUAL_ACTIVE */

#ifdef FEATURE_DSAT_ETSI_MODE
  ,{DSAT_TIMER_CAOC_REPORT,
   &dsatetsicall_aoc_timer, dsatutil_timer_cb,
   dsatetsicall_caoc_timer_expired_handler,
   DSATETSICALL_CAOC_REPORT_TIMER, FALSE, FALSE,NULL}

  ,{DSAT_INBAND_DTMF_RING,
   &dsatetsicall_inband_dtmf_timer, dsatutil_timer_cb,
   dsatetsicall_inband_dtmf_timer_expired_handler,
   DSAT_INBAND_DTMF_RING_DELAY, FALSE, FALSE,NULL}
#endif /* FEATURE_DSAT_ETSI_MODE */

  ,{DSAT_TIMER_VOICE_CALL_TIMEOUT,
   &dsatvoice_voice_connection_timer, dsatutil_timer_cb,
   dsatvoice_timer_expired_handler,
   0, FALSE, FALSE,&dsatvoice_voice_call_id[PRIMA_SUBS]}/* Duration is the S7 value */

#ifdef FEATURE_DUAL_ACTIVE
  ,{DSAT_TIMER_VOICE_CALL_TIMEOUT_2,
   &dsatvoice_voice_connection_timer_2, dsatutil_timer_cb,
   dsatvoice_timer_expired_handler,
   0, FALSE, FALSE,&dsatvoice_voice_call_id[SECON_SUBS]}/* Duration is the S7 value */
#endif /* FEATURE_DUAL_ACTIVE */

   ,{DSAT_TIMER_DATA_CALL_TIMEOUT,
   &dsatvoice_data_connection_timer, dsatutil_timer_cb,
   dsatvoice_timer_expired_handler,
   0, FALSE, FALSE,&dsatvoice_data_call_id[PRIMA_SUBS]}/* Duration is the S7 value */

   ,{DSAT_TIMER_DATA_CALL_TIMEOUT_2,
   &dsatvoice_data_connection_timer_2, dsatutil_timer_cb,
   dsatvoice_timer_expired_handler,
   0, FALSE, FALSE,&dsatvoice_data_call_id[SECON_SUBS]}/* Duration is the S7 value */

#ifdef FEATURE_DATA_MUX
  ,{DSAT_TIMER_RDM_OPEN_TIMEOUT,
  &dsat_rdm_open_timer, dsatutil_timer_cb,
   dsatact_rdm_timer_expired_handler,
   DSAT_RDM_OPEN_TIMEOUT, FALSE, FALSE,&dsat_port_open_count}
#endif /* FEATURE_DATA_MUX */

#ifdef FEATURE_DATA_PS_EAP
   ,{DSAT_TIMER_CEAP_REPORT_TIMEOUT,
   &dsatetsime_ceap_report_timer, dsatutil_timer_cb,
   dsatetsime_ceap_timer_expired_handler,
   DSAT_CEAP_CMD_WAIT_DELAY, FALSE, FALSE,NULL}

   ,{DSAT_TIMER_CEAP_EAP_TIMEOUT,
   &dsatetsime_ceap_eap_process_timer, dsatutil_timer_cb,
   dsatetsime_ceap_process_timer_expired_handler,
   DSAT_CEAP_PROCESS_WAIT_DELAY, FALSE, FALSE,NULL}
#endif /* FEATURE_DATA_PS_EAP */
  ,{DSAT_TIMER_FREE_ITEM,
   &dsati_free_item_timer, dsatutil_timer_cb,
   dsati_free_item_timer_expired_handler,
   DSAT_FREE_ITEM_DLY, FALSE, FALSE,NULL}  
};
/* Local Variable */
LOCAL dsat_string_item_type  csmp_fo_val[MAX_SUBS][CSMP_FO_STR_MAX_LEN+1];
LOCAL dsat_string_item_type  csmp_vp_val[MAX_SUBS][CSMP_VP_STR_MAX_LEN+1];
LOCAL dsat_string_item_type  qcpdpp_passw_val[DS_UMTS_MAX_QCPDP_STRING_LEN+4];
LOCAL dsat_string_item_type  qcpdpp_uname_val[DS_UMTS_MAX_QCPDP_STRING_LEN+4];
LOCAL dsat_string_item_type  cusd_str_val[CUSD_STR_MAX_LEN+1];
LOCAL dsat_string_item_type  cusd_dcs_val[CUSD_DCS_STR_MAX_LEN+1];
LOCAL dsat_string_item_type  cgans_l2p_type_val[8];
LOCAL dsat_string_item_type  cpin_pin_val[MAX_SUBS][MAX_PIN_STRING_LEN+1];
LOCAL dsat_string_item_type  cpin_newpin_val[MAX_SUBS][MAX_PIN_STRING_LEN+1];
LOCAL dsat_string_item_type  cops_oper_val[MAX_SUBS][COPS_OPER_STR_MAX_LEN+1];
LOCAL dsat_string_item_type  cops_csg_id_val[MAX_SUBS][COPS_CSG_ID_STR_MAX_LEN+1];
LOCAL dsat_string_item_type  cpol_oper_val[MAX_SUBS][CPOL_OPER_STR_MAX_LEN+1];
/*  CGDCONT  */
LOCAL dsat_string_item_type  cgdcont_pdptype_val[8];
LOCAL dsat_string_item_type  cgdcont_apn_val[DS_UMTS_MAX_APN_STRING_LEN+3];
LOCAL dsat_string_item_type  cgdcont_pdpaddr_val[DS_UMTS_MAX_APN_STRING_LEN+3];
/*  Value items: "+3" to allow for quotes and NULL  */
LOCAL dsat_string_item_type  cgtft_addr_subnet_val[MAX_ADDR_SUBNET_STRING_LEN+3];
LOCAL dsat_string_item_type  cgtft_src_port_val[MAX_PORT_STRING_LEN+3];
LOCAL dsat_string_item_type  cgtft_dst_port_val[MAX_PORT_STRING_LEN+3];
LOCAL dsat_string_item_type  cgtft_tos_tclass_val[MAX_TOS_TCLASS_STRING_LEN+3];
LOCAL dsat_string_item_type  cgeqreq_sdu_err_val[MAX_QOSERR_STRING_LEN+1];
LOCAL dsat_string_item_type  cgeqreq_res_biterr_val[MAX_QOSERR_STRING_LEN+1];
LOCAL dsat_string_item_type  cgeqmin_sdu_err_val[MAX_QOSERR_STRING_LEN+1];
LOCAL dsat_string_item_type  cgeqmin_res_biterr_val[MAX_QOSERR_STRING_LEN+1];
LOCAL dsat_string_item_type  cscb_mids_val[CSCB_MIDS_STR_MAX_LEN+1];
LOCAL dsat_string_item_type  cscb_dcss_val[CSCB_DCSS_STR_MAX_LEN+1];
LOCAL dsat_string_item_type  dsat_es_orig_rqst_val[ES_ORGI_RQST_MAX_LEN+1];
LOCAL dsat_string_item_type  dsat_es_orig_fbk_val[ES_ORIG_FBK_MAX_LEN+1];
LOCAL dsat_string_item_type  dsat_es_ans_fbk_val[ES_ANS_FBK_MAX_LEN+1];
LOCAL dsat_string_item_type  dsat_esa_trans_idle_val[ESA_FRAMED_IDLE_MAX_LEN+1];
LOCAL dsat_string_item_type  dsat_esa_framed_un_ov_val[ESA_FRAMED_UN_OV_MAX_LEN+1];
LOCAL dsat_string_item_type  dsat_esa_hd_auto_val[ESA_HD_AUTO_MAX_LEN+1];
LOCAL dsat_string_item_type  dsat_esa_syn2_val[ESA_SYN2_MAX_LEN+1];
LOCAL dsat_string_item_type  dsat_csca_sca_val[MAX_SUBS][CSCA_SCA_STR_MAX_LEN+1];
LOCAL dsat_string_item_type  dsat_csca_tosca_val[MAX_SUBS][CSCA_TOSCA_STR_MAX_LEN+1];
#ifdef FEATURE_DSAT_CDMA_PIN 
LOCAL dsat_string_item_type  dsat707_cpin_pin_val[MAX_PIN_STRING_LEN+1];
LOCAL dsat_string_item_type  dsat707_cpin_newpin_val[MAX_PIN_STRING_LEN+1];
#endif /* FEATURE_DSAT_CDMA_PIN */
#ifdef FEATURE_CDMA_SMS
LOCAL dsat_string_item_type  qcsmp_vp_val[QCSMP_VP_STR_MAX_LEN+1];
LOCAL dsat_string_item_type  qcsmp_tdd_val[QCSMP_DDT_STR_MAX_LEN+1];
#endif/* FEATURE_CDMA_SMS*/
#if defined(FEATURE_DSAT_SP_CMDS)
LOCAL dsat_string_item_type  spc_val[NV_SEC_CODE_SIZE + 1];
LOCAL dsat_string_item_type  unlock_val[UNLOCK_STRING_LEN + 1];
LOCAL dsat_string_item_type  lock_val[NV_LOCK_CODE_SIZE + 1];
LOCAL dsat_string_item_type  old_lock_val[LOCK_OLD_STRING_LEN + 1];
LOCAL dsat_string_item_type  new_lock_val[NV_LOCK_CODE_SIZE + 1];
#endif /*defined(FEATURE_DSAT_SP_CMDS)*/

/*Command table Val structure pointers*/
dsat_basic_table_ss_val_struct_type            *dsat_basic_vals     =  NULL;
dsat_basic_action_table_ss_val_struct_type     *dsat_basic_act_vals =  NULL;
dsat_basic_sreg_table_ss_val_struct_type       *dsat_basic_sreg_vals =  NULL;
dsat_basic_ext_table_ss_val_struct_type        *dsat_basic_ext_vals =  NULL;
dsat_basic_vend_table_ss_val_struct_type       *dsat_basic_vend_ss_vals = NULL;
dsat_basic_vend_table_ms_val_struct_type       *dsat_basic_vend_ms_vals[DSAT_MS_MAX_SUBS]={NULL,NULL,NULL};
dsat_etsi_ext_act_table_ss_val_struct_type     *dsat_etsi_ext_act_ss_vals = NULL;
dsat_etsi_ext_act_table_ms_val_struct_type     *dsat_etsi_ext_act_ms_vals[DSAT_MS_MAX_SUBS]={NULL,NULL,NULL};
dsat_etsi_ext_table_ss_val_struct_type         *dsat_etsi_ext_ss_vals = NULL;
dsat_etsi_ext_table_ms_val_struct_type         *dsat_etsi_ext_ms_vals[DSAT_MS_MAX_SUBS]={NULL,NULL,NULL};
dsat_basic_etsi_table_common_val               *dsat_basic_etsi_cmn_ms_vals[DSAT_MS_MAX_SUBS]={NULL,NULL,NULL};
#ifdef FEATURE_DATA_IS707 
dsat707_ext_para_table_ss_val_struct_type      *dsat707_ext_ss_vals = NULL;
#ifdef FEATURE_DSAT_CDMA_PIN 
dsat707_ext_vend_act_table_common_val          *dsat707_ext_act_cmn_ss_vals = NULL;
#endif /* FEATURE_DSAT_CDMA_PIN */
dsat707_fax_table_ss_val_struct_type           *dsat707_fax_ss_vals = NULL;
#ifdef FEATURE_HDR
#ifdef FEATURE_HDR_AT_TEST_ONLY 
  dsat707_hdr_table_ss_val_struct_type         *dsat707_hdr_ss_vals = NULL;
#endif /* FEATURE_HDR_AT_TEST_ONLY */
#endif /* FEATURE_HDR */
#ifdef FEATURE_CDMA_SMS
dsat707_sms_table_ss_val_struct_type            *dsat707_sms_ss_vals         = NULL;
#endif /* FEATURE_CDMA_SMS*/
dsat707_vend_para_table_ss_val_struct_type      *dsat707_vend_para_ss_vals   = NULL;
dsat707_vend_act_table_ss_val_struct_type       *dsat707_vend_act_ss_vals    = NULL;
#endif/* FEATURE_DATA_IS707 */
#ifdef FEATURE_BCMCS
dsat707_vend_1xhdr_table_ss_val_struct_type     *dsat707_1xhdr_ss_vals       = NULL;
#endif/*#ifdef FEATURE_BCMCS*/
/*--------------------------------------------------------------*/
/*ATCoP CM interface state vairables*/
dsatcmif_servs_state_info         dsatcmif_ss_ph_val = {NULL,{NULL,NULL,NULL}};

dsatetsicall_call_state_info      dsatcmif_call_val  = {NULL,{NULL,NULL,NULL},{NULL,NULL,NULL}};

dsatetsicmif_sups_state_info      dsatcmif_sups_val  = {NULL,{NULL,NULL,NULL}};

dsatme_mmgsdi_state_info          dsatme_mmgsdi_val  = {NULL,{NULL,NULL,NULL}};

dsatme_pbm_state_info             dsatme_pbm_val     = {NULL};

dsat_common_state_info            dsat_common_val    = {NULL};

dsatetsipkt_gprs_msg_info         dsatetsipkt_gprs_val = {NULL,NULL,NULL};

/*-------------------------------------------------------------*/
/*Remove below variable before check-in, Defined only sake of compilation
variable used by other modules*/
#ifdef FEATURE_DS_IS707A_CMUX
  dsat_num_item_type dsat707_cmux_val[2];
#else
  dsat_num_item_type dsat707_cmux_val;
#endif /*FEATURE_DS_IS707A_CMUX*/
  dsat_num_item_type dsat707_cta_val = 30;
  dsat_num_item_type dsat707_qcmdr_val = 3; 
#ifdef FEATURE_IS2000_R_SCH
/* TURN SCRM on/off */
  dsat_num_item_type dsat707_qcscrm_val = 1;
/* TURN R-SCH throttle on/off */
  dsat_num_item_type dsat707_qctrtl_val = 1;
#endif /* FEATURE_IS2000_R_SCH */
#ifdef FEATURE_IS2000_REL_A
 dsat_num_item_type dsat707_qcqospri_val = 13;
#endif /* FEATURE_IS2000_REL_A */
#ifdef FEATURE_IS2000_CHS
/* Enable/disable Mobile-originated Control/Hold */
 dsat_num_item_type dsat707_qcchs_val;
#endif /* FEATURE_IS2000_CHS */
#ifdef FEATURE_BCMCS
  dsat_string_item_type  dsat_1xhdr_qcbcip_val[DSAT_1XHDR_MAX_BCIPS_STR_LEN];
#endif/*# FEATURE_BCMCS*/
  dsat_num_item_type  dsat_1xhdr_qcbcen_val;
  dsat_num_item_type  dsat_crlp_val[MAX_CRLP_VERSIONS][MAX_CRLP_PARAMS];
  dsat_num_item_type  dsat_chsn_val[4];  
  dsat_num_item_type  dsat_cgauto_val;
/*Removed till here*/

/*-------------------------------------------------------------------------
    Protypes for local functions:
-------------------------------------------------------------------------*/
LOCAL byte * itoa_zfill
(
  word v,
  byte *rb_ptr
);

LOCAL action_quote_e_type determine_quote_action(uint32 attrib);

LOCAL boolean check_operating_mode_override
(
  dsati_mode_e_type            *mode_ptr,         /* Mode of Operation */
  nv_operating_mode_enum_type  modes_permitted    /* Allowed modes     */
);

LOCAL void pbm_char_to_gsm
(
  char pbm_in, 
  char **gsm_out
);

LOCAL void gsm_char_to_pbm
(
  char **gsm_in, 
  char *pbm_out
);

LOCAL int is_leap_year
(
  int year
);

LOCAL boolean dsatutil_hex_to_uint8
(
  const char *in_octet,
  uint8      *out_uint8
);

LOCAL boolean dsatutil_hex_to_uint16
(
  const char *in_str,
  uint16     *out_uint16
);

/*-------------------------------------------------------------------------
    Function Definitions:
-------------------------------------------------------------------------*/

/*===========================================================================

FUNCTION DSATUTIL_FMT_NUM_VAL

DESCRIPTION
  This function formats the result of a value inquiry "?" for a normal
  numeric variable.  The return string is a null terminated 3 - digit
  decimal number with leading zeros.

DEPENDENCIES

RETURN VALUE
  Number of characters stored, not including the trailing null character.

SIDE EFFECTS

===========================================================================*/
unsigned int dsatutil_fmt_num_val
(
  const dsati_cmd_type *tab_entry,      /*  entry in parse table  */
  byte *rb_ptr                          /*  point to buffer for result  */
)
{
  unsigned int result = 0;

  /* Abort formatting if no display attribute set */
  if ((tab_entry->attrib & NO_DISPLAY) == NO_DISPLAY)
  {
    *rb_ptr = '\0';
  }
  else
  {
     dsat_num_item_type *val_ptr = (dsat_num_item_type *)EXEC_VAL_PTR(tab_entry);
     if (NULL != val_ptr)
     {
       result = (unsigned int)( itoa_zfill((word)(0xFF & *(val_ptr)),rb_ptr) - rb_ptr );
     }
  }
  return result;
} /*  dsatutil_fmt_num_val  */

/*===========================================================================

FUNCTION DSATUTIL_FMT_NUM_VAL_EX

DESCRIPTION
  This function formats the value of an extended format numeric
  variable.  The value may be formatted as the result of a READ
  "?" command, or in the form of a SET command for use in uploading
  the configuration.  For the READ command, the returned string is
  of the form:

      +NAME: <value>                            for a simple variable, or
      +NAME: <value1>, <value2>, ... , <valueN> for a compound variable.

  For the SET command, it is in the form

      +NAME=<value>;                            for a simple variable, or
      +NAME=<value1>, <value2>, ... , <valueN>; for a compound variable.


  The value will be in decimal, unless the parameter is specified with
  a HEX attribute in the parse table entry.  Leading zeros are suppressed.

  ".0" is added to the result for +FCLASS=2.0

  The formatted value is returned as a null terminated string.

DEPENDENCIES

RETURN VALUE
  Number of characters stored, not including the trailing null character.

SIDE EFFECTS

===========================================================================*/
unsigned int dsatutil_fmt_num_val_ex
(
  const dsati_cmd_type *tab_entry,      /*  entry in parse table  */
  byte *rb_ptr,                         /*  point to buffer for result  */
  fmt_enum_type fmt_sw                  /*  selects response or set fmt */
)
{
  unsigned int i, j;
  boolean print_name;
  uint16 radix;
  byte c, *w_ptr;
  const byte *s, *l_ptr;
  boolean skip_fmt = FALSE;
  dsat_num_item_type *val_ptr;

  w_ptr = rb_ptr;
  radix = ( (tab_entry->attrib & HEX) ? 16 : 10);

  print_name = ( ((tab_entry->attrib & FCLASS2P0) == 0)
        && ((tab_entry->special) != SPECIAL_FCLASS))
      || fmt_sw == SET
      || fmt_sw == RESPONSE_ALL;

#if 0
  if (tab_entry->attrib & EXTENDED) /*  Since this function is also */
  {                               /*  used to SET CONFIG for the  */
    *w_ptr++ = '+';                 /*  S-registers.                */
  }
#endif /* if 0 */

  if (print_name)
    {
      s = tab_entry->name;

      while ( (c = *s++) !=0 )
        {
          *w_ptr++ = c;
        }
    }

  switch (fmt_sw)
    {
      case RESPONSE:
      case RESPONSE_ALL:
        if (print_name)
        {
          *w_ptr++ = ':';
          *w_ptr++ = ' ';
        }
        /* Abort further formatting if no display attribute set */
        if ((tab_entry->attrib & NO_DISPLAY) == NO_DISPLAY)
        {
          skip_fmt = TRUE;
        }
        break;

    case SET:
        *w_ptr++ = '=';
        break;

    default:
        /*  ERROR */
        ;
    }
  val_ptr = (dsat_num_item_type *) EXEC_VAL_PTR(tab_entry);
  if ( (NULL != val_ptr) && (!skip_fmt))
  {
    if ((tab_entry->attrib & LIST) != 0)
    {
      for (j=0; j < tab_entry->compound; ++j)
      {
        i = val_ptr[j];
        l_ptr = ((def_list_type *) tab_entry->def_lim_ptr)[j].list_v[i];
      
        if(tab_entry->attrib & YES_QUOTE)
        {
          *w_ptr++ = '"';
        }

        while ( (c = *l_ptr++) != '\0')
        {
          *w_ptr++ = c;
        }

        if(tab_entry->attrib & YES_QUOTE)
        {
          *w_ptr++ = '"';
        }
        *w_ptr++ = ',';
      }
      --w_ptr;
    }
    else if (tab_entry->compound > 1)
    {
      for (j=0; j < tab_entry->compound; ++j)
      {
        w_ptr = dsatutil_itoa(val_ptr[j], w_ptr, radix );
        *w_ptr++ = ',';
      }
      --w_ptr;
    }
    else
    {
      w_ptr = dsatutil_itoa(*val_ptr, w_ptr, radix );
    }
  
    if ( (tab_entry->special) == SPECIAL_FCLASS && *(val_ptr) == 2)
    {
      *w_ptr++ = '.';
      *w_ptr++ = '0';
    }

    if (fmt_sw == SET && tab_entry->attrib & EXTENDED)
    {
      *w_ptr++ = ';';
    }
  }
  *w_ptr = '\0';

  return (unsigned int) (w_ptr - rb_ptr);
} /*  dsatutil_fmt_num_val_ex */

/*===========================================================================

FUNCTION DSATUTIL_FMT_NUM_RANGE

DESCRIPTION
  This function formats the result of a range inquiry "=?" command
  for a numeric variable.

  The formatted value is returned as a null terminated string.
DEPENDENCIES

RETURN VALUE
  Number of characters stored, not including the trailing null character.


SIDE EFFECTS

===========================================================================*/
unsigned int dsatutil_fmt_num_range
(
  const dsati_cmd_type *tab_entry,      /*  entry in parse table  */
  byte *rb_ptr                          /*  point to buffer for result  */
)
{
  unsigned int j;
  unsigned int offset = 0;
  word radix;
  byte c, *w_ptr;
  const byte *r_ptr, *s;
  uint32 brtest = 0;

  w_ptr = rb_ptr;

  radix = ( (tab_entry->attrib & HEX) ? 16 : 10);

  s = tab_entry->name;

  if ((tab_entry->attrib & FCLASS2P0) == 0)
  {
    while ( (c = *s++) !=0 )
    {
      *w_ptr++ = c;
    }

    *w_ptr++ = ':';
    *w_ptr++ = ' ';
  }

  if ( (tab_entry->attrib & LIST) == 0)
  {
    /* Loop over number of parameters */
    for (j=0; j < tab_entry->compound; ++j)
    {
      brtest = (tab_entry->attrib & BROKEN_RANGE);
      *w_ptr++ = '(';

      /* Loop over BROKEN_RANGE segments (if any) */
      do
      {
        /* if the lower and upper are the same value, such as 7
           we should return (7) instead of (7-7) */
        if(((dflm_type *) tab_entry->def_lim_ptr)[j+offset].lower ==
           ((dflm_type *) tab_entry->def_lim_ptr)[j+offset].upper)
        {
          w_ptr = dsatutil_itoa (
                                 ((dflm_type *) tab_entry->def_lim_ptr)[j+offset].lower,
                                 w_ptr,
                                 radix );
        }
        else
        {
          w_ptr = dsatutil_itoa (
                                 ((dflm_type *) tab_entry->def_lim_ptr)[j+offset].lower,
                                 w_ptr,
                                 radix );
          *w_ptr++ = '-';
          w_ptr = dsatutil_itoa (
                                 ((dflm_type *) tab_entry->def_lim_ptr)[j+offset].upper,
                                 w_ptr,
                                 radix );

        }
        if (BROKEN_RANGE == brtest)
        {
          *w_ptr++ = ',';
          offset++;
        }
      } while (brtest &&
               (MAX_BROKEN_RANGE !=
                ((dflm_type *) tab_entry->def_lim_ptr)[j+offset].default_v));

      /* Remove last comma if broken range */
      if (BROKEN_RANGE == brtest)
      {
        w_ptr--;
      }

      *w_ptr++ = ')';
      *w_ptr++ = ',';
    }
    *(w_ptr-1) = '\0';
  }
  else
  {
    for (j=0; j < tab_entry->compound; ++j)
    {
      r_ptr = ((def_list_type *) tab_entry->def_lim_ptr)[j].list_t;
      while ( (c = *r_ptr++) != '\0')
      {
        *w_ptr++ = c;
      }
      *w_ptr++ = ',';
    }
    *(w_ptr-1) = '\0';
  }

  return (unsigned int) (w_ptr - rb_ptr);

} /*  dsatutil_fmt_num_range  */

/*===========================================================================

FUNCTION DSATUTIL_FMT_STRING_RANGE

DESCRIPTION
  This function formats the result of a range inquiry "=?" for a string
  variable.  The function always returns
      +NAME: (20,21,23-7E)
  which is the range of printable ASCII characters.

DEPENDENCIES

RETURN VALUE
  None.

SIDE EFFECTS

===========================================================================*/
void dsatutil_fmt_string_range
(
  const dsati_cmd_type *tab_entry,          /*  entry in parse table  */
  byte *rb_ptr,                             /*  pointer to buffer for result  */
  const dsat_string_item_type *range_ptr    /*  pointer to range  */
)
{
  dsat_string_item_type c;
  const dsat_string_item_type *s_ptr;

  if ((tab_entry->attrib & FCLASS2P0) == 0)
  {
    s_ptr = tab_entry->name;

    while ( (c = *s_ptr++) !=0 )
    {
      *rb_ptr++ = c;
    }

    *rb_ptr++ = ':';
    *rb_ptr++ = ' ';
  }

  s_ptr = range_ptr;
  while ( (*rb_ptr++ = *s_ptr++) != 0)
    ;
} /*  dsatutil_fmt_string_range */

/*===========================================================================

FUNCTION DSATUTIL_FMT_STRING_VAL

DESCRIPTION
  This function formats the value of an extended format string
  variable.  The value may be formatted as the result of a READ
  "?" command, or in the form of a SET command for use in uploading
  the configuration.  For the READ command, the returned string is
  of the form:

      +NAME: <string>

  For the SET command, it is in the form

      +NAME=<value>;

  The formatted value is returned as a null terminated string.

DEPENDENCIES

RETURN VALUE
  Number of characters stored, not including the trailing null character.

SIDE EFFECTS

===========================================================================*/
unsigned int dsatutil_fmt_string_val
(
  const dsati_cmd_type *tab_entry,      /*  entry in parse table  */
  byte *rb_ptr,                         /*  point to buffer for result  */
  fmt_enum_type fmt_sw                  /*  selects response or set fmt */
)
{
  dsat_string_item_type c, *w_ptr;
  const dsat_string_item_type *s;
  boolean print_name;
  boolean skip_fmt = FALSE;

  w_ptr = rb_ptr;

  print_name = ( ((tab_entry->attrib & FCLASS2P0) == 0)
        || fmt_sw == SET
        || fmt_sw == RESPONSE_ALL);

  if (print_name)
  {
    s = tab_entry->name;

    while ( (c = *s++) != 0 )
    {
      *w_ptr++ = c;
    }
  }

  switch (fmt_sw)
  {
    case RESPONSE:
    case RESPONSE_ALL:
      if (print_name)
      {
        *w_ptr++ = ':';
        *w_ptr++ = ' ';
      }
      
      /* Abort further formatting if no display attribute set */
      if ((tab_entry->attrib & NO_DISPLAY) == NO_DISPLAY)
      {
        skip_fmt = TRUE;
      }
      break;

    case SET:
      *w_ptr++ = '=';
      break;

    default:
      /*  ERROR */
      ;

  }
    s = (dsat_string_item_type *)EXEC_VAL_PTR(tab_entry);
  if( (NULL != s) && (!skip_fmt) )
  {
    if ((tab_entry->attrib & NO_QUOTE) == 0)
    {
      *w_ptr++ = '"';
    }

    while ( (c = *s++) != 0)
    {
      *w_ptr++ = c;
    }

    if ((tab_entry->attrib & NO_QUOTE) == 0)
    {
      *w_ptr++ = '"';
    }

    if (fmt_sw == SET)
    {
      *w_ptr++ = ';';
    }
  }

  *w_ptr = '\0';

  return (unsigned int) (w_ptr - rb_ptr);
} /*  dsatutil_fmt_string_val */

/*===========================================================================

FUNCTION DSATUTIL_FMT_MIXED_PARAM_VAL

DESCRIPTION
  This function formats the values of a mixed parameter.
  The values may be formatted as the result of a READ "?" command,
  or in the form of a SET command for use in uploading the configuration.
  For the READ command, the returned string is of the form:

      +NAME: <p1>,<p2>,...

  For the SET command, it is in the form

      +NAME=<value>;

  The formatted value is returned as a null terminated string.

DEPENDENCIES

RETURN VALUE
  Number of characters stored, not including the trailing null character.

SIDE EFFECTS

===========================================================================*/
unsigned int dsatutil_fmt_mixed_param_val
(
  const dsati_cmd_type *tab_entry,      /*  entry in parse table  */
  const dsm_item_type *res_buff_ptr,    /*  Place to put response       */
  fmt_enum_type fmt_sw                  /*  selects response or set fmt */
)
{
  dsat_string_item_type c, *w_ptr;
  const dsat_string_item_type *s;
  boolean print_name;
  int index = 0;
  uint16 buff_len = 0;
  
  dsat_mixed_param_val_type * val_list = 
    (dsat_mixed_param_val_type *) EXEC_VAL_PTR(tab_entry);

  mixed_def_s_type ** mixed_def_list = 
    (mixed_def_s_type **) tab_entry->def_lim_ptr;

  w_ptr = res_buff_ptr->data_ptr + res_buff_ptr->used;
  buff_len = res_buff_ptr->size - res_buff_ptr->used;
  
  print_name = ( ((tab_entry->attrib & FCLASS2P0) == 0)
                 || fmt_sw == SET
                 || fmt_sw == RESPONSE_ALL);

  if (print_name)
  {
    s = tab_entry->name;

    while ( (c = *s++) != 0 )
    {
      *w_ptr++ = c;
    }
  }

  switch (fmt_sw)
  {
    case RESPONSE:
    case RESPONSE_ALL:
      if (print_name)
      {
        *w_ptr++ = ':';
        *w_ptr++ = ' ';
      }
      break;

    case SET:
      *w_ptr++ = '=';
      break;

    default:
      /*  ERROR */
      ;

  }
  if(NULL != val_list)
  {
  for (index = 0; index < tab_entry->compound; index++)
  {
    /* the index-th parameter is not to be displayed  */
    if( (mixed_def_list[index]->attrib & (NO_DISPLAY)) == 
        (NO_DISPLAY) )
    {
      /* Skip formatting this parameter */
    }
    
    /* the index-th parameter is of type STRING, 
       val_ptr points to its real value */
    else if( (mixed_def_list[index]->attrib & (CONFIG | STRING | LIST)) == 
             (CONFIG | STRING) )
    {
      dsat_string_item_type * val_string = val_list[index].string_item;
        
      if(mixed_def_list[index]->attrib & YES_QUOTE )
      {
         w_ptr += (word) snprintf((char *)w_ptr,buff_len,
                                        "\"%s\",", val_string);
      }
      else
      {
         w_ptr += (word) snprintf((char *)w_ptr,buff_len,
                                        "%s,", val_string);
      }
    }

    /* the index-th parameter is a string within a string list,
         we use val_ptr to index into the string list to get the string */
    else if( (mixed_def_list[index]->attrib & (CONFIG | STRING | LIST)) == 
             (CONFIG | LIST) )
    {
      const dsat_string_item_type * val_string = 
        mixed_def_list[index]->def->def_list.list_v[val_list[index].num_item];
        
      if(mixed_def_list[index]->attrib & YES_QUOTE )
      {
        w_ptr += (word) snprintf((char *)w_ptr,buff_len,
                                       "\"%s\",", val_string);
      }
      else
      {
        w_ptr += (word) snprintf((char *)w_ptr, buff_len,
                                       "%s,", val_string);
      }
    }

    else if( (mixed_def_list[index]->attrib & (CONFIG | STRING | LIST)) == 
             (CONFIG) )
    {

      w_ptr += (word) snprintf((char *)w_ptr,buff_len, "%u,",
                                     val_list[index].num_item);
    }

    /* the argument is not anticipated */
    else
    {
      DS_AT_MSG3_ERROR("Mixed parameter setting action %d, %dth param has wrong attrib %d",
          tab_entry->name, index, mixed_def_list[index]->attrib);
      return 0;
    }
  }
  }	

  /* get rid of the last ',' & terminate string */
  *(--w_ptr) = '\0';

  return (unsigned int) (w_ptr - (res_buff_ptr->data_ptr + res_buff_ptr->used));
} /*  dsatutil_fmt_mixed_param_val */

/*===========================================================================

FUNCTION DSATUTIL_PUT_NUM_ARG

DESCRIPTION
  This function checks the value and number of numeric arguments for
  a command, and, if they are correct, stores the value(s) in the
  parameter.

DEPENDENCIES
  If command uses the BROKEN_RANGE attribute, all parameters in the
  default/limits array must be configured with MAX_BROKEN_RANGE delimiters.

RETURN VALUE
  Returns a boolean. Returns FALSE if there is an error else returns
  TRUE

SIDE EFFECTS
  Describe here any side effects that may occur as a result of calling this
  function. This includes such things as global vars that are modified, etc.

===========================================================================*/
boolean dsatutil_put_num_arg
(
  const dsati_cmd_type *tab_entry,      /*  entry in parse table        */
  const tokens_struct_type *tok_ptr     /*  Command tokens from parser  */
)
{
  dsat_num_item_type scr_val [MAX_ARG];
  byte flag [MAX_ARG]; /* used to flag that there is match in the val list */
  atoi_enum_type e;
  param_srch_enum_type srchrc;
  word radix;
  uint32 j,i;
  uint32 range_offset = 0;
  uint32 brtest = (tab_entry->attrib & BROKEN_RANGE);
  dsat_num_item_type *val_ptr;

  for (i=0;i<MAX_ARG;i++)
  {
    flag[i]=0;
    scr_val[i]=0;
  }

  radix = ( (tab_entry->attrib & HEX) ? 16 : 10);

  if (tok_ptr->args_found > tab_entry->compound)
  {
    return (FALSE);
  }

  for (j = 0; j < tok_ptr->args_found; ++j)
  {
    flag [j] = 0;
    
    if ((tab_entry->attrib & LIST) == 0)
    {
      e = dsatutil_atoi (&scr_val [j], tok_ptr->arg[j], radix);
      if (e == ATOI_OK)
      {
        /* Adjust for broken ranges */
        i = j + range_offset;

        if (0 == brtest)
        {
          /* Continuous range */
          if (scr_val [j] < ((dflm_type *) tab_entry->def_lim_ptr)[i].lower ||
              scr_val [j] > ((dflm_type *) tab_entry->def_lim_ptr)[i].upper)
          {
            /* The argument is out of range */
            return FALSE;
          }
          else
          {
            flag [j] = 1;
          }
        } 
        else
        {
          /* Discontinuous range so loop over each segment */
          while (MAX_BROKEN_RANGE !=
                 ((dflm_type *) tab_entry->def_lim_ptr)[i].default_v)
          {
            if ((scr_val [j] >= ((dflm_type *) tab_entry->def_lim_ptr)[i].lower) &&
                (scr_val [j] <= ((dflm_type *) tab_entry->def_lim_ptr)[i].upper))
            {
              flag [j] = 1;
            }
            i++;
          }
          /* See if one segment worked */
          if (0 == flag [j])
          {
            /* The argument is out of range */
            return FALSE;
          }
          range_offset = i - j;
        }
      } 
      else if (e == ATOI_OUT_OF_RANGE)
      {
        /* the char code of is out of range */
        return FALSE;
      }
      else if (0 != brtest)
      {
        /* Advance to next discontinuous range segment */
        i = j + range_offset;
        while (MAX_BROKEN_RANGE !=
               ((dflm_type *) tab_entry->def_lim_ptr)[i].default_v) 
        { 
          i++; 
        }
        range_offset = i - j;
      }
      /*else if (e == ATOI_NO_ARG) do nothing: flag[j] remains unset  */
      /*    and the value is not stored                               */
    }
    else  /*  LIST  */
    {
      srchrc = dsatutil_srch_list (
                 &scr_val [j], 
                 ((def_list_type *) tab_entry->def_lim_ptr)[j].list_v,
                 tok_ptr->arg[j],
                 determine_quote_action(tab_entry->attrib) );
      
      /* Continue if paramter detected */
      if (PARAM_NULL != srchrc)
      {
        if (PARAM_HIT == srchrc)
        {
          flag [j] = 1;
        }
        else
        {
          /* not found in list */
          return FALSE;
        }
      }
    }
  }
  
  val_ptr = (dsat_num_item_type *)EXEC_VAL_PTR(tab_entry);

  if(NULL == val_ptr)
  {
    return FALSE;
  }

  for (j = 0; j < tok_ptr->args_found; ++j)
  {
    if (flag[j])
    {
      * (val_ptr + j) = scr_val [j];
    }
  }
  return TRUE;
} /*  dsatutil_put_num_arg  */

/*===========================================================================

FUNCTION DSATUTIL_PUT_STRING_ARG

DESCRIPTION
  This checks an extended format string WRITE command for correct
  syntax and string length.  If everthing is OK, the string is copied
  into the specified parameter.  Otherwise, the global dsat_result" is
  set to ERROR. Non space characters outside of the '"' are considered
  an error.

DEPENDENCIES


RETURN VALUE
  returns a boolean. Returns FALSE if there is an error else returns
  TRUE

SIDE EFFECTS


===========================================================================*/
boolean dsatutil_put_string_arg
(
  const dsati_cmd_type *tab_entry,   /*  entry in parse table        */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  put_string_enum_type action     /*  Store or concatenate  */
)
{
  byte c, *s_ptr, *t_ptr;
  unsigned int arg_cnt, total_cnt;
  unsigned int n_args;

  if ((tok_ptr->args_found != 1) &&         /*  One and only one argument */
      (tab_entry->attrib & NO_QUOTE) == 0)  /*  allowed, unless NOQUOTE   */
  {
     return ( FALSE );
  }

  s_ptr = tok_ptr->arg[0];
  arg_cnt = 0;
  total_cnt = 0;

  if ((tab_entry->attrib & NO_QUOTE) == 0)
  {
    while ( ((c = *s_ptr++) !=0 ) && c != '"')/* No characters except spaces */
    {                                         /* allowed between '=' and '"' */
      if (c != ' ')
      {
         return ( FALSE );
      }
    }

    t_ptr = s_ptr;                        /*  Points to 1st char after '"'*/
    while ( (c = *s_ptr++) != '"')        /*  Count characters inside the */
    {                                     /*  '"'. Non-print not allowed. */
      ++arg_cnt;
      if (arg_cnt > tab_entry->compound || c <' ' || c > '\x7E')
      {
         return ( FALSE );
      }
    }

    total_cnt = arg_cnt;

    while ( (c = *s_ptr++) != 0 )         /*  No trailing characters before */
    {                                     /*  ';' except spaces             */
      if (c != ' ')
      {
          return ( FALSE );
      }
    }
  }
  else                                    /*  NO_QUOTE: count all chars in  */
  {                                       /*  all args to end of command.   */
    n_args = 0;
    t_ptr = s_ptr;
    for ( ;; )
    {
      if ((c = *s_ptr++) == '\0')
      {
        if( (++n_args < tok_ptr->args_found) && ( MAX_ARG > n_args ) )
        {
          s_ptr = tok_ptr->arg[n_args];
          c = ',';
        }
        else
        {
          break;
        }
      }
      ++arg_cnt;
      if (arg_cnt > tab_entry->compound || c <' ' || c > '\x7E')
      {
         return ( FALSE );
      }
    }
  }

  /* Update the parameter value only if non-null string */
  if ( 0 != arg_cnt )
  {
    s_ptr = (dsat_string_item_type *) EXEC_VAL_PTR(tab_entry);
    if( NULL == s_ptr)
      return FALSE;
    
    /*----------------------------------------------------------  
    If concatenate requested, and argument is not a NULL string
    advance the store point to the end of the current value, and
    increment total character count.  Compare count to allowable
    for the parameter.
    
    If concatenate requested, and argument is the NULL string,
    leave pointers alone. This will cause a NULL character to
    be stored in the paramter.
    ----------------------------------------------------------*/
    
    if (action == CAT)
    {
      while (*s_ptr)                      /*  Count current length and add  */
      {                                   /*  to total count                */
        ++s_ptr;
        ++total_cnt;
      }
      if (total_cnt > tab_entry->compound)
      {
        return ( FALSE );
      }
    }
    
    n_args = 0;
    while (arg_cnt--)                   /*  Copy arg to end to parameter  */
    {
      c = *t_ptr++;
      if (c == '\0')
      {
        if ( (++n_args < tok_ptr->args_found) && ( MAX_ARG > n_args ) )
        {
          c = ',';
          t_ptr = tok_ptr->arg[n_args];
        }
      }
      *s_ptr++ = c;
    }
    
    /* Verify string length */
    if (s_ptr >
        ((dsat_string_item_type *) EXEC_VAL_PTR(tab_entry) + tab_entry->compound))
    {
      ERR_FATAL ("OVERFLOW", 0, 0, 0);
    }
    
    *s_ptr = '\0';
  }
  return ( TRUE );

} /*  dsatutil_put_string_arg */

/*===========================================================================

FUNCTION DSATUTIL_MIXED_PARAM_SET_NUM_ARG

DESCRIPTION
  This function checks the value of a string argument for
  a command in the def list.  It determines the string argument's index 
  into the string list.  Use this index to set the val_ptr.

DEPENDENCIES


RETURN VALUE
  returns a param_srch_enum_type:
    PARAM_MISS - Not found in values list 
    PARAM_HIT  - Found in values list            
    PARAM_NULL - No parameter specified          

SIDE EFFECTS
  None

===========================================================================*/
param_srch_enum_type dsatutil_mixed_param_set_num_arg
(
  dsat_num_item_type * val_num_ptr, /* the index number to be returned */
  const def_list_type * def_list_ptr, /* the def list */
  const byte * arg,                   /* the token argument */
  uint32 attrib                       /* the attrib of the MIXED_PARAM */
)
{
  return dsatutil_srch_list(val_num_ptr, 
                   def_list_ptr->list_v,
                   arg,
                   determine_quote_action(attrib));
}

/*===========================================================================

FUNCTION DSATUTIL_ITOA

DESCRIPTION
  This function converts an integer to ASCII, using a specified
  radix. The resulting string is null terminated.

DEPENDENCIES
  The function does not work correctly if r < 2 or r > 36.
  (r is the radix argument.)

  The length of the returned string depends on the value and the
  radix.  The worst case is the value 0xFFFFFFFF and a radix of 2,
  which returns 33 bytes (32 one's and the null terminator.)
  The array pointed to by 's' must be large enough for the returned
  string.

RETURN VALUE
  A pointer to the null terminator is returned.

SIDE EFFECTS

===========================================================================*/
byte * dsatutil_itoa
(
  uint32 v,         /*  Value to convert to ASCII     */
  byte *s,        /*  Pointer to string for result  */
  uint16 r          /*  Conversion radix              */
)
{
  byte buf[MAX_ITOA_LEN], c;
  int n;

  n = sizeof(buf) - 1;

  buf[n] = '\0';


  do
  {
    c = (byte) (v % r);

    if (n <= 0)
    {
      DS_AT_MSG0_HIGH("OVERFLOW");
      break;
    }

    buf[--n] = (byte) ( (c > 9) ? c + 'A' - 10 : c + '0');

  } while ( (v /= r) > 0);

  while ( (*s++ = buf[n++]) != 0)
      ;

  return (s-1);
} /*  dsatutil_itoa */

/*===========================================================================

FUNCTION DSATUTIL_XITOA

DESCRIPTION
  Formats an unsigned long to hexadecimal.
  Supplies leading zeros.

DEPENDENCIES

RETURN VALUE
  A pointer to the null terminator is returned.


SIDE EFFECTS

===========================================================================*/
unsigned char * dsatutil_xitoa
(
  unsigned long val,  /*  value to convert  */
  byte * a_ptr        /*  pointer to null terminator of result  */
)
{
  byte c, *s_ptr ;
  int count = 8;
  
  ASSERT(a_ptr != NULL);
  s_ptr = a_ptr + 8;

  *s_ptr-- = '\0';

  while (--count >= 0)
  {
    c = (byte) (val & 0x0F);
    val >>= 4;

    *s_ptr-- = (byte) ( (c > 9) ? c + 'A' - 10 : c + '0');
  }

  return a_ptr+8;
} /* dsatutil_xitoa() */

/*===========================================================================

FUNCTION DSATUTIL_STRCMP_IG_SP_CASE

DESCRIPTION
  This function compares two null terminated strings, ignoring case.

DEPENDENCIES
  None.

RETURN VALUE
  Returns   0 if the strings are identical, except for case,
            otherwise, it returns the difference between the first
            characters to disagree. The difference is first arg minus
            second arg.  Lower case letters are converted to upper
            case before the difference is taken.

SIDE EFFECTS
  None.

===========================================================================*/
int dsatutil_strcmp_ig_sp_case
(
  const byte *s1,             /*  Pointer to string 1 */
  const byte *s2              /*  Pointer to string 2 */
)
{
  byte c1, c2;
  int d;

  do
  {
    c1 = *s1++;
    c2 = *s2++;

    d = UPCASE(c1) - UPCASE(c2);
  }
  while ( d == 0 && c1 != '\0' && c2 != '\0');

  return d;
} /*  dsatutil_strcmp_ig_sp_case  */

/*===========================================================================

FUNCTION DSATUTIL_STRNCMP_IG_SP_CASE

DESCRIPTION
  This function compares two strings for <n> characters, ignoring case.
  Lower case letters are converted to upper case before the 
  difference is taken.

DEPENDENCIES
  None.

RETURN VALUE
  Returns   0 if the strings are identical till <n> characters, except for 
            case otherwise, it returns the difference between 
            the first characters to disagree. The difference is first arg 
            minus second arg.  

SIDE EFFECTS
  None.

===========================================================================*/
int dsatutil_strncmp_ig_sp_case
(
  const byte *s1,             /*  Pointer to string 1 */
  const byte *s2,             /*  Pointer to string 2 */
  uint32      n               /* Number of characters to compare */
)
{
  byte c1, c2;
  int d;

  if ( n== 0 )
  {
    return -1;
  }
  do
  {
    c1 = *s1++;
    c2 = *s2++;

    d = UPCASE(c1) - UPCASE(c2);
    --n;
  }
  while ( d == 0 && c1 != '\0' && c2 != '\0' && n != 0 );

  return d;
}/* dsatutil_strncmp_ig_sp_case */

/*===========================================================================

FUNCTION DSATUTIL_ATOI

DESCRIPTION
  This function converts an ASCII string to an integer, using a specified
  radix.  Spaces are ignored. Letters used for radices above 10 may be of
  either case.

DEPENDENCIES


RETURN VALUE
  Returns 
    ATOI_OK           : for a correct conversion,
    ATOI_NO_ARG       : 1 if no argument was found,
    ATOI_OUT_OF_RANGE : if a character or the total value is out of range.

SIDE EFFECTS

===========================================================================*/

atoi_enum_type dsatutil_atoi
(
  dsat_num_item_type *val_arg_ptr,      /*  value returned  */
  const byte *s,                        /*  points to string to eval  */
  unsigned int r                        /*  radix */
)
{
  atoi_enum_type err_ret = ATOI_NO_ARG;
  byte c;
  dsat_num_item_type val, val_lim, dig_lim;

  val = 0;
  val_lim = (dsat_num_item_type) ((unsigned int)MAX_VAL_NUM_ITEM / r);
  dig_lim = (dsat_num_item_type) ((unsigned int)MAX_VAL_NUM_ITEM % r);

  while ( (c = *s++) != '\0')
  {
    if (c != ' ')
    {
      c = (byte) UPCASE (c);
      if (c >= '0' && c <= '9')
      {
        c -= '0';
      }
      else if (c >= 'A')
      {
        c -= 'A' - 10;
      }
      else
      {
        err_ret = ATOI_OUT_OF_RANGE;  /*  char code too small */
        break;
      }

      if (c >= r || val > val_lim
          || (val == val_lim && c > dig_lim))
      {
        err_ret = ATOI_OUT_OF_RANGE;  /*  char code too large */
        break;
      }
      else
      {
        err_ret = ATOI_OK;            /*  arg found: OK so far*/
        val = (dsat_num_item_type) (val * r + c);
      }
    }
    *val_arg_ptr =  val;
  }
  
  return err_ret;

} /*  dsatutil_atoi */

/*===========================================================================
FUNCTION DSATUTIL_STRIP_QUOTES_OUT

DESCRIPTION
  This function strips the quotes out from the quoted_str, the results
  are in out_str as a null-terminated string.  str_len is the max length 
  of the out_str string, the applocated space for out_str.

  The quoted_str is a null terminated string.

DEPENDENCIES
  None

RETURN VALUE
  TRUE if the action succeeds.
  FALSE if the action fails.
        The reasons will output to ERR.  The reasons could be 
        1. the str_len is less than the length of
           quoted_str -2, ie the out_str is too small to hold the result.
        2. the quoted_str is not quoted

SIDE EFFECTS
  None
===========================================================================*/
boolean dsatutil_strip_quotes_out
( 
  const byte * quoted_str, byte * out_str, word str_len
)
{
  int index;
  boolean end_quote_found = FALSE;

  if(quoted_str[0] != '"')
  {
    /* This error is caused by user input, not a system error.
       Use MSG_MED */
    DS_AT_MSG0_MED("The in_str must be quoted");
    return FALSE;
  }
  
  for (index = 0; 
       (index < str_len) && (quoted_str[index + 1] != '\0') 
         && !end_quote_found; 
       index++)
  {
    if(quoted_str[index + 1] == '"')
    {
      end_quote_found = TRUE;
      out_str[index] = '\0';
    }
    else
    {
      out_str[index] = quoted_str[index + 1];
    }
  }

  if(!end_quote_found)
  {
    /* This error is caused by user input, not a system error.
       Use MSG_MED */
    DS_AT_MSG1_MED("The in_str has opened quotes, or out_str is small: %d",
            str_len);
    return FALSE;
  }
  return TRUE;
} /* dsatutil_strip_quotes_out */

/*===========================================================================
FUNCTION DSATUTIL_SRCH_LIST

DESCRIPTION
  This function searchs a list of strings, and returns the index to the
  item found.  (The first item index is 0).  Index set to NULL if 
  parameter is null, as with empty commas entered, 

  If ignore_quote is true, then srch_list will find quoted args in 
  unquoted lists.  For instance, \"ME\" will find itself as the first 
  item in list ME, MT, SM

DEPENDENCIES
  None

RETURN VALUE
  PARAM_HIT if found.
  PARAM_MISS if no match.
  PARAM_NULL if no parameter specified.

SIDE EFFECTS
  None
===========================================================================*/
param_srch_enum_type dsatutil_srch_list
(
  dsat_num_item_type *val_arg_ptr,  /*  value returned  */
  const byte l[][8],
  const byte * arg,
  action_quote_e_type action_quote
)
{
  unsigned int i = 0;
  const byte * use_arg = arg; /* the argument used to search through arg list */
  byte stripped_arg[MAX_LINE_SIZE];
  dsat_num_item_type arg_num, list_ele_num;

  if(arg[0] == '\0')
  {
    val_arg_ptr = NULL;
    return PARAM_NULL;
  }
  else
  {
    /* In ignore_quote case, if the arg has double quote 
       surrounding it, strip the quotes out */
    switch (action_quote)
    {
      case ACTION_QUOTE_BOTH_OK:
        /* don't care about the quote */
        if(arg[0] == '"')
        {
          (void) dsatutil_strip_quotes_out(arg, stripped_arg, MAX_LINE_SIZE);
          use_arg = stripped_arg;
        }
        break;

      case ACTION_QUOTE_YES_QUOTE:
        /* strip out the quotes */
        if(!dsatutil_strip_quotes_out(arg, stripped_arg, MAX_LINE_SIZE))
        {
          return PARAM_MISS;
        }
        use_arg = stripped_arg;
        break;
        
      case ACTION_QUOTE_NO_QUOTE:
        /* don't strip quote */
        break;
       
      default:
        /*lint -save -e641 */
        ERR_FATAL("No such case: %d",action_quote,0,0);
        /*lint -restore */
        break;
    }
  }

  /* Abort if parameter null */  
  if (0 == *use_arg)
  {
    val_arg_ptr = NULL;
    return PARAM_NULL;
  }
  
  /* Search values list for match */  
  while (*l[i] != 0)
  {
    /* if both the argument and l[i] are valid numbers, do 
       numerical comparision */
    if(dsatutil_atoi(&arg_num, use_arg, 10)   == ATOI_OK &&
       dsatutil_atoi(&list_ele_num, l[i], 10) == ATOI_OK)
    {
      if(arg_num == list_ele_num)
      {
        *val_arg_ptr = i;
        return PARAM_HIT;
      }
    }
    /* do string comparision */
    else if (dsatutil_strcmp_ig_sp_case (l[i], use_arg) == 0)
    {
      *val_arg_ptr = i;
      return PARAM_HIT;
    }
    ++i;
  }

  /* if the execution come here, there is no match */
  return PARAM_MISS;
} /* dsatutil_srch_list ()*/


/*===========================================================================

FUNCTION ITOA_ZFILL

DESCRIPTION
  This function formats a word variable.  The return string is a null
  terminated 3 - digit decimal number with leading zeros.

DEPENDENCIES

RETURN VALUE
  Pointer to the NULL terminator.

SIDE EFFECTS

===========================================================================*/
LOCAL byte * itoa_zfill
(
  word v,
  byte *rb_ptr
)
{
  int n;
  byte c, *ptr;

  ptr = rb_ptr + 3;
  *ptr = '\0';

  for (n = 0; n < 3; ++n)
  {
    c = (byte) (v % 10);
    v /= 10;
    *--ptr = (c + '0');
  }
  
  return rb_ptr + 3;
} /*  itoa_zfill */

/*===========================================================================
FUNCTION DETERMINE_QUOTE_ACTION

DESCRIPTION
  This function determines the action_quote from a parameter's attrib.

DEPENDENCIES
  None

RETURN VALUE
  ACTION_QUOTE_YES_QUOTE if attrib has YES_QUOTE 
  ACTION_QUOTE_NO_QUOTE  if attrib has NO_QUOTE 
  ACTION_QUOTE_BOTH_OK if none of the above

SIDE EFFECTS
  None
===========================================================================*/
LOCAL action_quote_e_type determine_quote_action(uint32 attrib)
{
  /* if the command table does not say this command's 
     arguments do not contain quotes, we ignore the 
     surrounding quotes */
  action_quote_e_type action_quote = ACTION_QUOTE_BOTH_OK;

  if(attrib & NO_QUOTE)
  {
    action_quote = ACTION_QUOTE_NO_QUOTE;
  }
  else if(attrib & YES_QUOTE)
  {
    action_quote = ACTION_QUOTE_YES_QUOTE;
  }
  
  return action_quote;
} /* determine_quote_action */



/*===========================================================================

FUNCTION DSATUTIL_RESET_PASSWORD

DESCRIPTION
  This function clears the password variable of specified length.
  The password string is set to 0xFF, null terminated.  

  The provided 'length' should be the total length of the buffer pointed to by 'password' including allocation 
  for the trailing NULL

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
void dsatutil_reset_password
(
  byte * password,            /* String containing password */
  word length                 /* Maximum length of storage pointed to by "password" */
)
{
  int index;

  /* Do nothing if null pointer passed */
  if (NULL == password) return; 

  /* init the password */
  for (index = 0; index < length-1; index++)
  {
    password[index] = (char) 0xFF;
  }
  password[index] = 0;
  return;
} /* dsatutil_reset_password */

/*===========================================================================

FUNCTION DSATUTIL_DSM_NEW_BUFFER

DESCRIPTION
  This function is a wrapper for dsm_new_buffer() API.

DEPENDENCIES
  None

RETURN VALUE
  A pointer to a 'dsm_item_type'

SIDE EFFECTS
  None

===========================================================================*/
dsm_item_type *dsatutil_dsm_new_buffer
(
  dsm_mempool_id_type  pool_id,
  const char          *filename,
  uint32               line,
  boolean              graceful
)
{
  dsm_item_type *dsm_item_ptr = NULL;

  dsm_item_ptr = dsmi_new_buffer(pool_id, filename, line);
  if (NULL == dsm_item_ptr)
  {
    DS_AT_MSG_SPRINTF_3_ERROR("DSM items exhausted in pool 0x%x (%s, %d)",
                              pool_id, filename, line);

    DSAT_GRACEFUL_ERR_FATAL(graceful);
  }
  else
  {
    DS_AT_MSG_SPRINTF_4_MED("Allocated DSM item in pool 0x%x with pointer 0x%x "
                            "(%s, %d)", pool_id, dsm_item_ptr, filename, line);
  }

  return dsm_item_ptr;
} /* dsatutil_dsm_new_buffer */

/*===========================================================================

FUNCTION DSATUTIL_DSM_PUSHDOWN_TAIL

DESCRIPTION
  This function is a wrapper for dsm_pushdown_tail() API.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  If the pushdown requires an additional data item, the pkt_ptr field of the 
  last item in the passed packet chain may be changed.

===========================================================================*/
void dsatutil_dsm_pushdown_tail
(
  dsm_item_type       **pkt_head_ptr,
  const void           *buf,
  uint16                size,
  dsm_mempool_id_type   pool_id,
  const char           *filename,
  uint32                line,
  boolean               graceful
)
{
  uint16 bytes_copied;

  bytes_copied = dsmi_pushdown_tail(pkt_head_ptr, buf, size, pool_id, filename, line);
  if (bytes_copied != size)
  {
    if (NULL != *pkt_head_ptr)
    {
      dsmi_free_packet(pkt_head_ptr, filename, line);
      *pkt_head_ptr = NULL;
    }
  }

  if (NULL == *pkt_head_ptr)
  {
    DS_AT_MSG_SPRINTF_5_ERROR("Failed to pushdown data of size %d on DSM packet "
                              "in pool 0x%x and bytes copied %d (%s, %d)",
                              size, pool_id, bytes_copied, filename, line);

    DSAT_GRACEFUL_ERR_FATAL(graceful);
  }
  else
  {
    DS_AT_MSG_SPRINTF_4_MED("Pushed down data of size %d on DSM packet in pool "
                            "0x%x (%s, %d)", size, pool_id, filename, line);
  }

  return;
} /* dsatutil_dsm_pushdown_tail */

/*===========================================================================

FUNCTION DSATUTIL_DSM_CREATE_PACKET

DESCRIPTION
  This function creates a DSM packet.

DEPENDENCIES
  None

RETURN VALUE
  A pointer to a 'dsm_item_type'

SIDE EFFECTS
  None

===========================================================================*/
dsm_item_type *dsatutil_dsm_create_packet
(
  const void *buf,
  uint16      size,
  const char *filename,
  uint32      line,
  boolean     graceful
)
{
  dsm_item_type *dsm_item_ptr = NULL;

  dsm_item_ptr = dsatutil_dsm_new_buffer(DSM_DS_LARGE_ITEM_POOL,
                                         filename, line, graceful);
  if (NULL == dsm_item_ptr)
  {
    dsm_item_ptr = dsatutil_dsm_new_buffer(DSM_DS_SMALL_ITEM_POOL,
                                           filename, line, graceful);
  }

  if (NULL != dsm_item_ptr)
  {
    (void) dsatutil_dsm_pushdown_tail(&dsm_item_ptr, buf, size,
                                      DSM_ITEM_POOL(dsm_item_ptr),
                                      filename, line, graceful);
  }

  return dsm_item_ptr;
} /* dsatutil_dsm_create_packet */


/*===========================================================================

FUNCTION DSATUTIL_APPEND_DSM_ITEM

DESCRIPTION
  This function checks the buffer length against the passed threshold
  value.  If the threshold is exceeded, a new DSM item is requested and
  appended to the buffer chain.  An error is generated if no dsm_items
  are available. A pointer to the new DSM item is returned as a parameter;
  it equals the input DSM item if no action taken.

DEPENDENCIES
  None

RETURN VALUE
  Pointer to current(appended) DSM item 

SIDE EFFECTS
  A new dsm_item may be allocated.
  
===========================================================================*/
dsm_item_type * dsatutil_append_dsm_item
(
  dsm_item_type * dsm_item_ptr,          /* Pointer to head DSM item */
  word threshold                         /* Threshold length of buffer */
)
{
  dsm_item_type *overflow_buff_ptr;
  dsm_item_type *cur_buff_ptr;
  byte x;
  byte y;
  word * used_ptr;
  byte * buff_ptr;

  cur_buff_ptr = dsm_item_ptr;
  buff_ptr = &dsm_item_ptr->data_ptr[dsm_item_ptr->used];
  used_ptr = &dsm_item_ptr->used;
  
  /*  If the data item is full, acquire a new data item. ERROR if none  */
  /*  available.                                                        */
  /*  Remove the last two characters from the old data item and put     */
  /*  them on the front of the new item, so they can be over written    */
  /*  if need be.                                                       */
  /*  Use dsm_append to append the two buffers                          */
  if ( *used_ptr > threshold )
  {
    y = *(buff_ptr-1);
    x = *(buff_ptr-2);

    overflow_buff_ptr = dsat_dsm_new_buffer(DSM_DS_LARGE_ITEM_POOL, FALSE);

    (*used_ptr) -= 2;

    /* Now the buffer ptr should point to the new dsm item ptr */
    buff_ptr = overflow_buff_ptr->data_ptr;
    used_ptr = &overflow_buff_ptr->used;
    cur_buff_ptr = overflow_buff_ptr;

    dsm_append( &dsm_item_ptr, &overflow_buff_ptr );
      
    buff_ptr[(*used_ptr)++] = x;
    buff_ptr[(*used_ptr)++] = y;
  }
  return cur_buff_ptr;
} /* dsatutil_append_dsm_item */



/*===========================================================================

FUNCTION DSATUTIL_VALIDATE_DIAL_CHAR

DESCRIPTION
  This function examines the passed dial string charater and returns a
  validation result.  It is up to the caller to act upon the result as
  approppriate within its context.

  Refer to 3GPP TS 27.007 section 6.2 for details on the valid dialing 
  digits.  The supported set are summarized as follows:
    0 - 9 
    A - C
    + * ; #           
    D - E             (ETSI SMS PDU mode)
    W , $ T P ! @ D   (accepted but flagged for strip)
  
DEPENDENCIES
  None

RETURN VALUE
  Returns enum value indicating validation result.

SIDE EFFECTS
  None

===========================================================================*/
dsat_dial_val_e_type dsatutil_validate_dial_char
(
  const byte * c_ptr                 /*  Input character   */
)
{
  dsat_dial_val_e_type result =  DSAT_DIAL_EMPTY;

  /* Characters that are ignored in a dial string */
  const byte d_strip        [] = "W,$TP!@D";

  /* Test for empty input */
  if (*c_ptr != '\0')
  {
    byte c = UPCASE (*c_ptr);

    /* Catagorize character */
    if (c >= '0' && c <= '9')
    {
      result = DSAT_DIAL_DIGIT;
    }
    else if (c >= 'A' && c <= 'C')
    {
      result = DSAT_DIAL_ASCII_ABC;
    }
    else if (c == 'D')
    {
      result = DSAT_DIAL_ASCII_D;
    }
    else if (c == 'E')
    {
      result = DSAT_DIAL_ASCII_E;
    }
    else if (c == '*')
    {
      result = DSAT_DIAL_STAR;
    }
    else if (c == '+')
    {
      result = DSAT_DIAL_PLUS;
    }
    else if (c == '#')
    {
      result = DSAT_DIAL_POUND;
    }
    else if (c == ';')
    {
      result = DSAT_DIAL_SEMICOLON;
    }
    else if (0 != strchr ((char*)d_strip, (char)c))
    {
      result = DSAT_DIAL_STRIP;
    }
    else
    {
      result = DSAT_DIAL_IGNORE;
    }
  }
  return result;
} /* dsatutil_validate_dial_char () */


/*===========================================================================

FUNCTION DSATUTIL_STRCMP_PKT_DIAL_STR

DESCRIPTION
  This function performs a lookup in the Packet call dial string registry
  to see if the input string prefix is found.  Further validation is
  done base on call type mode.   Input mode may be overridden for some
  call types.  Returns flag indicating valid packet call type detected.

DEPENDENCIES
  Lookup table must have been initialized from NV prior to use.

RETURN VALUE
  Returns TRUE if match in lookup table, FALSE otherwise

SIDE EFFECTS
  None.

===========================================================================*/
boolean dsatutil_strcmp_pkt_dial_str
(
  byte               *orig_dial_str,   /*  Pointer to the dial string   */
  dsati_mode_e_type  *mode_ptr         /*  Pointer to Mode of Operation */
)
{
  #define MAX_PKT_DIAL_STRING_LEN 32
  boolean result = FALSE;
  boolean hit = FALSE;
  uint8 prefix_len = 0;
  uint8 i = 0;
  byte *s_ptr, *p_ptr = NULL;
  byte pdsbuf[MAX_PKT_DIAL_STRING_LEN+1];
  
  ASSERT(NULL != orig_dial_str);

  /* Perform aggressive strip on dial string to remove unwanted characters. */
  /* We do this assuming packet dial strings are limited to range [0-9#*].  */
  /* Avoids issue where characters allowed in the spec are before or        */
  /* after the token searched for against dial string table.                */
  p_ptr = orig_dial_str;
  s_ptr = pdsbuf;
  while (('\0' != *p_ptr) &&
         (MAX_PKT_DIAL_STRING_LEN > (s_ptr - pdsbuf)))
  {
    /* Test range */
    if ( (('0' <= *p_ptr) && ('9' >= *p_ptr)) ||
         ('#' == *p_ptr) ||
         ('*' == *p_ptr) )
    {
      /* Save wanted character */
      *s_ptr++ = *p_ptr;
    }
    p_ptr++;
  }
  *s_ptr = '\0';
  
  
  /* Perform lookup for packet call prefix */
  while (i < dsat_pkt_dial_string_table.size)
  {
     byte * prefix = dsat_pkt_dial_string_table.dial_string_info[i].dial_string;
     prefix_len = (uint8) strlen((char*) prefix);
     if( prefix_len > MAX_DIAL_STRING_SIZE )
     {
       DS_AT_MSG1_ERROR("Invalid prefix length : %d", prefix_len);
       return result;
     }

     /* Test packet prefix within dial string buffer */
     if ( 0 == memcmp(pdsbuf, prefix, prefix_len) ) 
     {
        /* Exit loop on hit */
        hit = TRUE;
        break;
     }
     i++;
  }

  /* Mode specific validation */
  if (hit)
  {
     dsati_pkt_dial_string_s_type * pds_ptr =
        &dsat_pkt_dial_string_table.dial_string_info[i];
     pkt_mode_override = FALSE;
     /* CDMA mode validation */
     if ( 0 == ((NV_OPMODE_GPRS | NV_OPMODE_WCDMA) & pds_ptr->call_type) )
     {
        /* Ensure prefix matches input string exactly */
        if ('\0' == pdsbuf[prefix_len])
        {
           /* Verify CM preferred mode allowed for dial string */
           result = pkt_mode_override =
              check_operating_mode_override (mode_ptr,
                                             pds_ptr->modes_permitted);
        }
     }
     else
     {
        /* ETSI mode validation */
        /* Check to see if the dial string is terminated by a '#' */
        if( s_ptr > pdsbuf )
        {
           s_ptr = pdsbuf;
           while (*s_ptr != 0 )
           {
              s_ptr++;
           }
           if ( '#' == *(s_ptr-1) )
           {
             result = TRUE;
           }
        }
      }
    }
  
  DS_AT_MSG1_HIGH("strcmp_pkt_dial_str result = %d", result);
  return result;
} /*  dsatutil_strcmp_pkt_dial_str() */



/*===========================================================================

FUNCTION CHECK_OPERATING_MODE_OVERRIDE

DESCRIPTION
  This function checks the Call Manager preferred packet call mode against
  a list of permitted modes.  If CM mode is permitted, the ATCOP current
  operating mode is changed to match.  This affects later processing and
  call type invocation.  If CM mode not recognized, the current mode set
  by DSMGR is used instead.

DEPENDENCIES
  None

RETURN VALUE
  Returns TRUE if operating mode was changed, FALSE otherwise

SIDE EFFECTS
  None.

===========================================================================*/
LOCAL boolean check_operating_mode_override
(
  dsati_mode_e_type            *mode_ptr,         /* Mode of Operation */
  nv_operating_mode_enum_type  modes_permitted    /* Allowed modes     */
)
{
   boolean result = FALSE;
   nv_operating_mode_enum_type preferred_mode = 0;
   sys_sys_mode_e_type cm_pref_mode;
   dsat_sio_info_s_type *sio_info_ptr = NULL;
           
   /* Query DS3G for preferred operating mode */
   /* Currently only single Data Subscription, so no need to verify with subs. */ 
   cm_pref_mode = ds3g_get_current_network_mode_ex(dsat_get_current_subs_id());

   /* Map mode to NV enum */
   switch (cm_pref_mode)
   {
   case SYS_SYS_MODE_CDMA:
   case SYS_SYS_MODE_HDR:
      preferred_mode = NV_OPMODE_CDMA;
      break;
   case SYS_SYS_MODE_GSM:
      preferred_mode = NV_OPMODE_GPRS;
      break;
   case SYS_SYS_MODE_TDS:
   case SYS_SYS_MODE_WCDMA:
      preferred_mode = NV_OPMODE_WCDMA;
      break;
   default:
      /* If CM preferred mode not recognized, use current mode set by
         DSMGR instead */
      DS_AT_MSG1_HIGH("Unsupported CM pref_srv_type_for_packet_orig mode: %d",
               cm_pref_mode);
      
      switch (*mode_ptr)
      {
      case DSAT_MODE_GSM:
        preferred_mode = NV_OPMODE_GPRS;
        break;
      case DSAT_MODE_TDS:
      case DSAT_MODE_WCDMA:
        preferred_mode = NV_OPMODE_WCDMA;
        break;
      case DSAT_MODE_CDMA:
        preferred_mode = NV_OPMODE_CDMA;
        break;
      default:
        DS_AT_MSG1_HIGH("Unsupported DSMGR mode: %d",*mode_ptr);
      }
   }
           
   if (0 != preferred_mode)
   {
      /* Verify preferred mode is permitted */
      if ( 0 != (preferred_mode & modes_permitted) )
      {
         /* Change current mode to preferred mode */
        switch(preferred_mode)
        {
        case NV_OPMODE_GPRS:
          *mode_ptr = DSAT_MODE_GSM;
          break;
        case NV_OPMODE_WCDMA:
          *mode_ptr = DSAT_MODE_WCDMA;
          break;
        case NV_OPMODE_CDMA:
          *mode_ptr = DSAT_MODE_CDMA;
          break;
        default:
          DS_AT_MSG1_HIGH("Unsupported preferred mode: %d",preferred_mode);
        }
         result = TRUE;
      }
   }
   return result;
} /* check_operating_mode_override() */


#ifdef FEATURE_DSAT_ETSI_MODE
#ifdef FEATURE_DSAT_ETSI_DATA
/*===========================================================================

FUNCTION DSATUTIL_CHECK_OPT_DIAL_STR

DESCRIPTION
  Parses a Packet data dial string. Only options {*<cid>,***<cid>}
  are supported.  If cid is specified but not defined (via +CGDCONT),
  an error is returned.  The cid is passed back to caller.
  
  Validation is skipped if packet call mode override is set, indicating
  a non-ETSI mode dial string is being processed in ETSI mode.

DEPENDENCIES
  The calling function is responsible for cid parameter initialization.

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_OK : if the command has been successfully executed
    DSAT_ERROR : if there was any problem in executing the command

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatutil_check_opt_dial_str
(
  const byte * in_ptr,                /*  Input string, null terminated   */
  byte * cid_ptr                /*  Connection ID return value      */
)
{
#define MAX_OPTIONS 4
#define MAX_OPTION_SIZE 8
#define SC98_OPTION 1
#define SC99_OPTION 3
  byte options [MAX_OPTIONS][MAX_OPTION_SIZE];
  byte c;
  byte * o_ptr;
  byte curropt = 0;
  dsat_num_item_type gprs_sc, cid;
  dsat_result_enum_type result = DSAT_OK;
  boolean valid_flg = FALSE;
          
  /* Bypass validation if packet mode override occurred */
  if (pkt_mode_override)
  {
     return DSAT_OK;
  }
  
  /* Initialize options array */
  memset((void*)options, 0, sizeof(options));
  curropt = 0;

  /* Process first '*' */
  c = *in_ptr++;
  o_ptr = options[curropt];

  /* Scan input string, storing options as discovered */
  while ((c = *in_ptr++) != '\0')
  {
    /* Stop when hash seen */
    if (c == '#')
    {
      *o_ptr = '\0';
      break;
    }
    
    /* Option delimiter found; increment option field.
       Added ',' as option delimiter to allow processing of multiple
       <cid>s in dial string, which is not currently supported and should
       generate ERROR */
    if( c == '*' ||
        c == ',' )
    {
      if (curropt++ < (MAX_OPTIONS-1))
      {  
        *o_ptr = '\0';
        o_ptr = options[curropt];
      }
      else
      {
        DS_AT_MSG0_HIGH("Unsupported number of options");
        return DSAT_ERROR;
      }
    }
    /* Store option character */
    else
    {
      if ((o_ptr - options[curropt]) < (MAX_OPTION_SIZE-1))
      {
        *o_ptr++ = c;
      }
      else
      {
        *o_ptr = '\0';
        DS_AT_MSG0_MED("Option exceeds expected length");
      }
    }
  }
  *o_ptr = '\0';

  /* First option is actually GPRS Service Center, used to drive validation */
  if (ATOI_OK != dsatutil_atoi(&gprs_sc, options[0], 10))
  {
     DS_AT_MSG_SPRINTF_1_HIGH( 
      "Cannot convert to integer: %s", options[0]);
     return DSAT_ERROR;
  }

  /* Validate options based on SC */
  switch (gprs_sc)
  {
    case 98:
      /* Verify at most one option given (ATD*98*<cid>#) */
      if ((0 < curropt) && (SC98_OPTION != curropt))
      {
         DS_AT_MSG1_HIGH("Incorrect options for SC98: %d",curropt);
         result = DSAT_ERROR;
      }

      /* Check for valid conection id, if specified as first option */
      else if ('\0' != options[SC98_OPTION][0])
      {
        if (ATOI_OK == dsatutil_atoi(&cid, options[SC98_OPTION], 10))
        {
          if ( cid > MAX_BYTE_VALUE ||
               DS_UMTS_PDP_SUCCESS != 
                 ds_umts_get_pdp_profile_context_info_is_valid_per_subs(
#ifndef FEATURE_DSAT_LEGACY_GCF_SUPPORT		 
                   (uint16)(cid | (dsat_num_item_type)DS_UMTS_PDP_ATCOP_PROFILE_FAMILY), 
#else
                   (uint16)cid, 
#endif /* FEATURE_DSAT_LEGACY_GCF_SUPPORT */
                   dsat_get_qcsimapp_val(),
                   &valid_flg) )
          {
            DS_AT_MSG1_HIGH("Error on PDP profile valid query: %d",cid);
          }

          if (FALSE == valid_flg)
          {
            DS_AT_MSG1_HIGH("Undefined PDP context cid: %d",cid);
            result = DSAT_ERROR;
          }
          else
          {
            /* Set CID return value */
            *cid_ptr = (byte)cid;
          }
        }
        else
        {
          DS_AT_MSG_SPRINTF_1_HIGH( "Cannot convert to \
            integer: %s", options[SC98_OPTION]);
          result = DSAT_ERROR;
        }
      }
      break;
      
    case 99:
      /* Verify at most 3 options given (ATD*99*<called_address>*<L2P>*<cid>#) */
      if ((0 < curropt) && SC99_OPTION < curropt)
      {
         DS_AT_MSG1_HIGH("Incorrect options for SC99: %d",curropt);
         result = DSAT_ERROR;
      }
      
      /* Check for valid conection id, if specified as third option */
      if ('\0' != options[SC99_OPTION][0])
      {
        if (ATOI_OK == dsatutil_atoi(&cid, options[SC99_OPTION], 10))
        {
          if (DS_UMTS_PDP_SUCCESS !=
              ds_umts_get_pdp_profile_context_info_is_valid(
#ifndef FEATURE_DSAT_LEGACY_GCF_SUPPORT		 
                (uint16)(cid | (dsat_num_item_type)DS_UMTS_PDP_ATCOP_PROFILE_FAMILY), &valid_flg))
#else
                   (uint16)cid, &valid_flg))
#endif /* FEATURE_DSAT_LEGACY_GCF_SUPPORT */
          {
            DS_AT_MSG1_HIGH("Error on PDP profile valid query: %d",cid);
          }

          if (FALSE == valid_flg)
          {
            DS_AT_MSG1_HIGH("Undefined PDP context cid: %d",cid);
            result = DSAT_ERROR;
          }
          else
          {
            /* Set CID return value */
            *cid_ptr = (byte)cid;
          }
        }
        else
        {
           DS_AT_MSG_SPRINTF_1_HIGH("Cannot convert to \
             integer: %s", options[SC99_OPTION]);
           result = DSAT_ERROR;
        }
      }
      break;
      
    default:
      DS_AT_MSG1_HIGH("Unsupport GPRS Service Center: %d", gprs_sc);
      return DSAT_ERROR;
  }
  
  return result;
} /* dsatutil_check_opt_dial_str () */
#endif /* FEATURE_DSAT_ETSI_DATA */


/*===========================================================================

FUNCTION DSATUTIL_FILL_MODIFIER_INFO

DESCRIPTION
  This function fill out the struct pointed by mod_info_ptr according
  to attribute of dial_ptr and <index> and <info> of +CCUG command.

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_OK : if the command has been successfully executed
    DSAT_ERROR : if there was any problem in executing the command

SIDE EFFECTS
  None.

===========================================================================*/
dsat_result_enum_type dsatutil_fill_modifier_info
(
  /* input, contains properties of dial string */
  const dsati_dial_str_type     * dial_ptr,

  /* output, contains modifier info of the dial string */
  dsat_dial_modifier_info_type  * mod_info_ptr
)
{
  dsat_result_enum_type result = DSAT_OK;
  mod_info_ptr->modifier_present = FALSE;

  if(dial_ptr->attrib & DIAL_ATTRIB_PKT)
  {
#ifdef FEATURE_DSAT_ETSI_DATA
    byte cid = 0;
    
    /* Check to see if the dial string contains optional characters */
    /* Refer to 3GPP TS 27.007 sections 10.2.1.1 & 10.2.1.2         */
    if (DSAT_OK == dsatutil_check_opt_dial_str ( dial_ptr->num, &cid ))
    {
      if (0 != cid)
      {
        /* this is a packet call */
        mod_info_ptr->modifier_present = TRUE;
        mod_info_ptr->modifier = DSAT_CID_DIAL_MODIFIER;

        mod_info_ptr->modifier_info.cid.cid_val = cid;
      }
    }
    else
    {
      /* Problem decoding connection ID */
      result = DSAT_ERROR;
    }
#endif /* FEATURE_DSAT_ETSI_DATA */
  }
  else
  {
#ifdef FEATURE_DSAT_ETSI_MODE
    /* fill out CLIR related fields */
    if(dial_ptr->attrib & DIAL_ATTRIB_UP_I)
    {
      mod_info_ptr->modifier_present = TRUE;
      mod_info_ptr->modifier = DSAT_CLI_DIAL_MODIFIER;
      mod_info_ptr->modifier_info.cli.cli_enabled = TRUE;
    }
    else if(dial_ptr->attrib & DIAL_ATTRIB_DN_I)
    {
      mod_info_ptr->modifier_present = TRUE;
      mod_info_ptr->modifier = DSAT_CLI_DIAL_MODIFIER;
      mod_info_ptr->modifier_info.cli.cli_enabled = FALSE;
    }

    /* fill out +CCUG related fields */
    if(
       /* dial modifier enables CUG temporary mode */
       dial_ptr->attrib & DIAL_ATTRIB_UP_G ||

       /* NOT disable CUG, and +CCUG <n> enables CUG temporary mode */
       ( (!(dial_ptr->attrib & DIAL_ATTRIB_DN_G)) && 
         ((dsat_num_item_type)dsatutil_get_val(DSATETSI_EXT_ACT_CCUG_ETSI_IDX,0,0,NUM_TYPE) == 1))
      )
    {
      mod_info_ptr->modifier_present = TRUE;
      if(dial_ptr->attrib & (DIAL_ATTRIB_UP_I | DIAL_ATTRIB_DN_I) )
      {
        mod_info_ptr->modifier = DSAT_CLI_AND_CUG_MODIFIER;
      }
      else
      {
        mod_info_ptr->modifier = DSAT_CUG_DIAL_MODIFIER;
      }

      mod_info_ptr->modifier_info.cug.cug_enabled = TRUE;

      /* see if CUG index is present */
      if((dsat_num_item_type)dsatutil_get_val(DSATETSI_EXT_ACT_CCUG_ETSI_IDX,0,1,NUM_TYPE) != 10)
      {
        ASSERT((dsat_num_item_type)dsatutil_get_val(DSATETSI_EXT_ACT_CCUG_ETSI_IDX,0,1,NUM_TYPE) <= 9);
        mod_info_ptr->modifier_info.cug.cug_index_enabled = TRUE;
        mod_info_ptr->modifier_info.cug.cug_index_val = (byte)
          (dsat_num_item_type)dsatutil_get_val(DSATETSI_EXT_ACT_CCUG_ETSI_IDX,0,1,NUM_TYPE);
      }
      else
      {
        mod_info_ptr->modifier_info.cug.cug_index_enabled = FALSE;
      }

      /* fill out fields related to <info> of +CCUG */
      switch((dsat_num_item_type)dsatutil_get_val(DSATETSI_EXT_ACT_CCUG_ETSI_IDX,0,2,NUM_TYPE))
      {
        case 0:
          mod_info_ptr->modifier_info.cug.suppress_pref_cug = FALSE;
          mod_info_ptr->modifier_info.cug.suppress_outgoing_access = FALSE;
          break;

        case 1:
          /* +CCUG cmd <info>: suppress OA */
          mod_info_ptr->modifier_info.cug.suppress_pref_cug = FALSE;
          mod_info_ptr->modifier_info.cug.suppress_outgoing_access = TRUE;
          break;

        case 2:
          /* +CCUG cmd <info>: suppress preferential CUG */
          mod_info_ptr->modifier_info.cug.suppress_pref_cug = TRUE;
          mod_info_ptr->modifier_info.cug.suppress_outgoing_access = FALSE;
          break;
      
        case 3:
          /* +CCUG cmd <info>: suppress OA and preferential CUG */
          mod_info_ptr->modifier_info.cug.suppress_outgoing_access = TRUE;
          mod_info_ptr->modifier_info.cug.suppress_pref_cug = TRUE;
          break;

        default:
          DS_AT_MSG1_HIGH("incorrect dsat_ccug_val[2]: %d", (dsat_num_item_type)
            dsatutil_get_val(DSATETSI_EXT_ACT_CCUG_ETSI_IDX,0,2,NUM_TYPE));
          mod_info_ptr->modifier_info.cug.suppress_pref_cug = FALSE;
          mod_info_ptr->modifier_info.cug.suppress_outgoing_access = FALSE;
          result = DSAT_ERROR;
          break;
      }
    }
#endif /* FEATURE_DSAT_ETSI_MODE */
  }

  return result;
} /* dsatutil_fill_modifier_info */

#endif /* FEATURE_DSAT_ETSI_MODE */



/*===========================================================================

FUNCTION DSATUTIL_INIT_PKT_DIAL_STRING_FROM_NV

DESCRIPTION
  This function reads Packet Dial Sting lookup table from the NV during
  initialization. If these items were never written in NV, default
  values are loaded and stored to NV.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None.

===========================================================================*/
void dsatutil_init_pkt_dial_string_from_nv ( void )
{
  nv_stat_enum_type    nv_status;          
  nv_item_type         ds_nv_item;                /* actual data item     */

  /* Read the packet dial sting list from the NV */
  nv_status = dsatutil_get_nv_item (NV_PKT_DIAL_STRINGS_I, &ds_nv_item);
  
  /* Check if the NV has valid data */
  if( nv_status == NV_DONE_S)
  {
    /* i is index into NV items read, j is index into dial string table
       being populated. Allows for NV to have null entries at beginning,
       end or middle */
    uint8 i,j;
     
    /* Create local table from NV results */
    memset ((void*)&dsat_pkt_dial_string_table, 0,
            sizeof(dsat_pkt_dial_string_table));
     
    for ( i=0, j = 0; 
          i < NV_PKT_DIAL_STRING_LIST_SIZE;
          i++ )
    {
      if ( '\0' != ds_nv_item.pkt_dial_strings[i].dial_string[0] )
      {
        dsati_pkt_dial_string_s_type * pds_ptr =
          &dsat_pkt_dial_string_table.dial_string_info[j];

        /* Transfer data items */
        pds_ptr->call_type = ds_nv_item.pkt_dial_strings[i].call_type;
        pds_ptr->modes_permitted =
          ds_nv_item.pkt_dial_strings[i].modes_permitted;
        (void) dsatutil_memscpy((void*)pds_ptr->dial_string,
                MAX_DIAL_STRING_SIZE,
                (void*)ds_nv_item.pkt_dial_strings[i].dial_string,
                NV_PKT_DIAL_STRING_SIZE);
        j++;
      }
    }

    dsat_pkt_dial_string_table.size = j;
  }
  else
  {
    /* NV not available so use default table */
    dsati_pkt_dial_string_table_s_type default_pkt_dial_string_table =
    {
      3,
      {
        /* String,  Call Type,                         Permitted Modes                 */
        { "*98",    (NV_OPMODE_GPRS|NV_OPMODE_WCDMA),  (NV_OPMODE_GPRS|NV_OPMODE_WCDMA) },
        { "*99",    (NV_OPMODE_GPRS|NV_OPMODE_WCDMA),  (NV_OPMODE_GPRS|NV_OPMODE_WCDMA) },
        { "#777",   NV_OPMODE_CDMA,                    NV_OPMODE_CDMA                   },
        { "",       0,                                 0                                }
      }
    };
     
    (void) dsatutil_memscpy((void*)&dsat_pkt_dial_string_table,
            sizeof(dsat_pkt_dial_string_table),
            (void*)&default_pkt_dial_string_table,
            sizeof(default_pkt_dial_string_table));

    /* Update NV if never written */
    if( nv_status == NV_NOTACTIVE_S )
    {
      uint8 i = 0;
      memset ((void*)&ds_nv_item, 0, sizeof(ds_nv_item));
     
      while (i < dsat_pkt_dial_string_table.size)
      {
        dsati_pkt_dial_string_s_type * pds_ptr =
          &dsat_pkt_dial_string_table.dial_string_info[i];

        /* Transfer data items */
        ds_nv_item.pkt_dial_strings[i].call_type = pds_ptr->call_type;
        ds_nv_item.pkt_dial_strings[i].modes_permitted =
          pds_ptr->modes_permitted;
        (void) dsatutil_memscpy((void*)ds_nv_item.pkt_dial_strings[i].dial_string,
                NV_PKT_DIAL_STRING_SIZE,
                (void*)pds_ptr->dial_string,
               sizeof(ds_nv_item.pkt_dial_strings[i].dial_string));
        i++;
      }
       
      /* Post to NV */
      (void)dsatutil_put_nv_item(NV_PKT_DIAL_STRINGS_I, &ds_nv_item);
    }
  }

  return;
}

/*===========================================================================

FUNCTION DSATUTIL_INIT_OS_TIMERS

DESCRIPTION
  This function registers timers with the opeating system.  Starting
  and clearing timers is done in individual command handlers.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None.

===========================================================================*/
void dsatutil_init_timers ( void )
{
  uint8 i;

  /* Loop over timer table */
  for(i=0; i<(uint8)DSAT_TIMER_MAX; i++)
  {
    /* Register each timer with operating system */
    if ( !dsatutil_timer_table[i].is_defined )
    {
      rex_def_timer_ex(dsatutil_timer_table[i].timer_ptr,
                       dsatutil_timer_table[i].cb_func, 
                       (unsigned long) dsatutil_timer_table[i].timer_id);
      dsatutil_timer_table[i].is_defined = TRUE;
    }
  }
} /* dsatutil_init_timers */



/*===========================================================================

FUNCTION DSATUTIL_DISPATCH_TIMER_HANDLER

DESCRIPTION
  This function dispatches the common timer expiry event to the
  correct timer handler.  If the timer is not found, an error message
  is generated.

DEPENDENCIES
  None

RETURN VALUE
  Returns an enum that describes the result of the command execution.
  Possible values:
    DSAT_ASYNCH_EVENT : asynch event has been dispatched

SIDE EFFECTS
  None.

===========================================================================*/
dsat_result_enum_type dsatutil_dispatch_timer_handler
(
  dsat_mode_enum_type   mode,                /* AT command mode            */
  ds_cmd_type         * cmd_ptr              /* DS Command pointer         */
)
{
  dsat_result_enum_type result = DSAT_ASYNC_EVENT;
  boolean timer_found = FALSE;
  uint8 i;
  
  /* Loop over timer table */
  for(i=0; i<(uint8)DSAT_TIMER_MAX; i++)
  {
    /* Check for match of timer ID */
    if( (unsigned int)dsatutil_timer_table[i].timer_id == cmd_ptr->cmd.dsat_timer_id )
    {
      timer_found = TRUE;
      
      /* Invoke the timer handler function */
      dsatutil_timer_table[i].is_running = FALSE;
      result = dsatutil_timer_table[i].hdlr_func (mode, cmd_ptr);
    }
  }

  /* Report error if unknown timer */
  if( FALSE == timer_found )
  {
    DS_AT_MSG1_ERROR("Timer ID not found in table: %d",
         cmd_ptr->cmd.dsat_timer_id);
  }

  return result;
} /* dsatutil_dispatch_timers */



/*===========================================================================
FUNCTION DSATUTIL_GET_NV_ITEM 

DESCRIPTION
  Get an item from the nonvolatile memory.  

RETURN VALUE
  The NV return code.
  
DEPENDENCIES
  NV task is expected to be available.

SIDE EFFECTS
  None

===========================================================================*/
nv_stat_enum_type dsatutil_get_nv_item 
(
  nv_items_enum_type  item,           /* Which item */
  nv_item_type       *data_ptr        /* Pointer to space for item */
)
{
  nv_cmd_type  nv_command;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  memset((void*)&nv_command, 0x0, sizeof(nv_cmd_type));
  
#ifdef FEATURE_MODEM_RCINIT_PHASE2
  if (rex_self() == rcinit_lookup_rextask("ds"))
  {
#if defined(FEATURE_DUAL_SIM) && defined(FEATURE_DUAL_IMEI )
    nv_command.status = dsi_get_nv_item_ext(item, 
                                            data_ptr,
                                            dsat_get_qcsimapp_val());
#else
    nv_command.status = dsi_get_nv_item (item, data_ptr);
#endif /* FEATURE_DUAL_SIM && FEATURE_DUAL_IMEI */
  }
  else if (rex_self() == rcinit_lookup_rextask("ps")) 
  {
        nv_command.status = ps_get_nv_item (item, data_ptr);
  }
#else
  if (rex_self() == &ds_tcb)
  {
#if defined(FEATURE_DUAL_SIM) && defined(FEATURE_DUAL_IMEI )
    nv_command.status = dsi_get_nv_item_ext(item, 
                                            data_ptr,
                                            dsat_get_qcsimapp_val());
#else
    nv_command.status = dsi_get_nv_item (item, data_ptr);
#endif /* FEATURE_DUAL_SIM && FEATURE_DUAL_IMEI */
  }
  else if (rex_self() == &ps_tcb) 
  {
        nv_command.status = ps_get_nv_item (item, data_ptr);
  }
#endif /* FEATURE_MODEM_RCINIT_PHASE2 */

  if (NV_DONE_S != nv_command.status)
  {
    DS_AT_MSG2_ERROR("Failed to get NV item %d, status = %d",
                     item, nv_command.status);
  }

  return( nv_command.status );
}

/*===========================================================================
FUNCTION DSATUTIL_GET_NV_ITEM_PER_SUBS 

DESCRIPTION
  Get an item from the nonvolatile memory.  

RETURN VALUE
  The NV return code.
  
DEPENDENCIES
  NV task is expected to be available.

SIDE EFFECTS
  None

===========================================================================*/
nv_stat_enum_type dsatutil_get_nv_item_per_subs 
(
  nv_items_enum_type  item,           /* Which item */
  nv_item_type       *data_ptr,        /* Pointer to space for item */
  dsat_num_item_type  subs_id
)
{
  nv_cmd_type  nv_command;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  memset((void*)&nv_command, 0x0, sizeof(nv_cmd_type));
  
#ifdef FEATURE_MODEM_RCINIT_PHASE2
  if (rex_self() == rcinit_lookup_rextask("ds"))
  {
#if defined(FEATURE_DUAL_SIM) && defined(FEATURE_DUAL_IMEI )
    nv_command.status = dsi_get_nv_item_ext(item, 
                                            data_ptr,
                                            subs_id);
#else
    nv_command.status = dsi_get_nv_item (item, data_ptr);
#endif /* FEATURE_DUAL_SIM && FEATURE_DUAL_IMEI */
  }
  else if (rex_self() == rcinit_lookup_rextask("ps")) 
  {
        nv_command.status = ps_get_nv_item (item, data_ptr);
  }
#else
  if (rex_self() == &ds_tcb)
  {
#if defined(FEATURE_DUAL_SIM) && defined(FEATURE_DUAL_IMEI )
    nv_command.status = dsi_get_nv_item_ext(item, 
                                            data_ptr,
                                            subs_id);
#else
    nv_command.status = dsi_get_nv_item (item, data_ptr);
#endif /* FEATURE_DUAL_SIM && FEATURE_DUAL_IMEI */
  }
  else if (rex_self() == &ps_tcb) 
  {
        nv_command.status = ps_get_nv_item (item, data_ptr);
  }
#endif /* FEATURE_MODEM_RCINIT_PHASE2 */

  if (NV_DONE_S != nv_command.status)
  {
    DS_AT_MSG2_ERROR("Failed to get NV item %d, status = %d",
                     item, nv_command.status);
  }

  return( nv_command.status );
}


/*===========================================================================
FUNCTION DSATUTIL_PUT_NV_ITEM 

DESCRIPTION
  Get an item from the nonvolatile memory.  

RETURN VALUE
  The NV return code.
  
DEPENDENCIES
  NV task is expected to be available.

SIDE EFFECTS
  None

===========================================================================*/
nv_stat_enum_type dsatutil_put_nv_item 
(
  nv_items_enum_type  item,           /* Which item */
  nv_item_type       *data_ptr        /* Pointer to space for item */
)
{
  nv_cmd_type  nv_command;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  memset((void*)&nv_command, 0x0, sizeof(nv_cmd_type));
#ifdef FEATURE_MODEM_RCINIT_PHASE2
  if (rex_self() == rcinit_lookup_rextask("ds"))
  {
    nv_command.status = dsi_put_nv_item (item, data_ptr);
  }
  else if (rex_self() == rcinit_lookup_rextask("ps")) 
  {
        nv_command.status = ps_put_nv_item (item, data_ptr);
  }
#else
  if (rex_self() == &ds_tcb)
  {
    nv_command.status = dsi_put_nv_item (item, data_ptr);
  }
  else if (rex_self() == &ps_tcb) 
  {
        nv_command.status = ps_put_nv_item (item, data_ptr);
  }
#endif /* FEATURE_MODEM_RCINIT_PHASE2 */

  if (NV_DONE_S != nv_command.status)
  {
    DS_AT_MSG2_ERROR("Failed to put NV item %d, status = %d",
                     item, nv_command.status);
  }

  return( nv_command.status );
}


/*===========================================================================

FUNCTION DSATUTIL_CONVERT_TUPLE

DESCRIPTION
  This function checks for the validity of known tuple formats and converts
  between string and number representations.  
  For IPV6 addresses only the preferred text representation of RFC2373 is 
  supported: quantity 8, 16 bit HEX numbers, separated by colons and without
  zero compressing double colon pairs "::".
    
      For IPv6 addresses, the number
  pointer is assumed to be an array of uint32 values.
  
  Tuples supported:
     IPv4       range: 0.0.0.0 - 255.255.255.255
     IPv4_254   range: 0.0.0.0 - 254.254.254.254
     PORT       range: 0.0 - 65535.65535
     TMASK      range: 0.0 - 255.255
     IPv6       range: 0:0:0:0:0:0:0:0 - 
                         FFFF:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF

DEPENDENCIES
  For IPv6 addresses, the number pointer is assumed to be an array[4] of
  uint32 values.

RETURN VALUE
  returns an flag that describes the result of execution.
  possible values:
    TRUE : if validation successful
    FALSE : if error occurred

SIDE EFFECTS
  None
  
===========================================================================*/
dsat_result_enum_type dsatutil_convert_tuple 
(
   tuple_convert_enum_type mode,             /* Mode of conversion      */
   tuple_type_enum_type ttype,               /* Type of tuple           */
   uint32* num_ptr,                          /* Integer representation */
   byte* buf_ptr,                            /* String representation   */
   uint8 buf_len,                            /* String length           */
   dsat_ip_mask_enum_type ip_mask            /* IP or Mask Convertion   */
)
{
  #define MAX_TOKENS 16             /* Max num of delimiter separated tokens */
  #define MAX_TOKEN_LEN 5           /* Max num chars in a token */

  /* Only RFC2732 preferred text representation for IPV6 addresses uses ':'
     separators */
  char delimiter = 
    (ttype == TUPLE_TYPE_IPv6 ? IPV6_DELIMITER : IPV4_DELIMITER);
  /* When parsing IPV6 addresses, how many tokens per uint32 number */
  uint8 max_tokens_per_number = (ttype == TUPLE_TYPE_IPv6_OCTETS ? 4 : 2);
  atoi_enum_type result;
  /* Only RFC2732 preferred text representation for IPV6 addresses uses hex
     characters */
  byte radix = (ttype == TUPLE_TYPE_IPv6 ? 16 : 10);
  uint8  num_tokens;
  uint8  token_bits;
  uint32 token_mask;
  uint32 token_limit;
  dsat_num_item_type val[MAX_TOKENS]={0};
  uint32 *num_ptr_orig = num_ptr; 
  uint8 num_ptr_max_len=1;

  /* Setup conversion paramaters */
  switch (ttype)
  {
  case TUPLE_TYPE_IPv4:
    token_bits = 8;
    token_mask = token_limit = (1 << token_bits) - 1;
    max_tokens_per_number = num_tokens = 4;
    break;

  case TUPLE_TYPE_IPv4_254:
    token_bits = 8;
    token_mask  = (1 << token_bits) - 1;
    token_limit = (1 << token_bits) - 2;
    max_tokens_per_number = num_tokens = 4;
    break;
    
  case TUPLE_TYPE_PORT:
    token_bits = 16;
    token_mask = token_limit = (1 << token_bits) - 1;
    max_tokens_per_number = num_tokens = 2;
    break;
    
  case TUPLE_TYPE_TMASK:
    token_bits = 8;
    token_mask = token_limit = (1 << token_bits) - 1;
    max_tokens_per_number = num_tokens = 2;
    break;
    
  case TUPLE_TYPE_IPv6:
    token_bits = 16;
    token_mask = token_limit = (1 << token_bits) - 1;
    num_tokens = 8;
    max_tokens_per_number = 2;
    num_ptr_max_len = 4;
    break;
    
  case TUPLE_TYPE_IPv6_OCTETS:
    token_bits = 8;
    token_mask = token_limit = (1 << token_bits) - 1;
    num_tokens = 16;
    max_tokens_per_number = 4;
    num_ptr_max_len = 4;
    break;
    
  default:
    DS_AT_MSG1_ERROR("Unsupported tuple type: %d", ttype);
    return DSAT_ERROR;
  }

  /* Perform conversion */
  if (STRING_TO_INT == mode)
  { 
#define NUM_BITS_VAL 32

    int cpi=0, vali=0;
    byte cp[MAX_TOKEN_LEN+1];
    byte * c = buf_ptr;
    int i,j;
    boolean loop = TRUE;
    memset((void*)cp,0,sizeof(cp));

    /*---------------------------------------------------------------
      The following loop processes the string representing an tuple.
      Each token is converted in to decimal integer and stored in
      a temporary array.
      If the value is greater than the limit, we return an error.
    ---------------------------------------------------------------*/
    while (loop && (num_tokens > vali))
    {
      /* Skip quotes */
      if ('"' != *c)
      {
        /* Build token until delimiter or end of buffer */
        loop = (('\0' != *c)? TRUE : FALSE );
        if ( (delimiter != *c) && loop )
        {
          cp[cpi++] = *c;

          if (MAX_TOKEN_LEN < cpi)
          {
            DS_AT_MSG2_ERROR("Token exceeds max length: %d < %d",
                       MAX_TOKEN_LEN,cpi);
            return DSAT_ERROR;
          }
        }
        else 
        {
          /* Convert to integer */
          result = dsatutil_atoi( (val+vali), cp, radix);
          if (ATOI_OK != result)
          {
            return DSAT_ERROR;
          }

          /* Test token against limit */
          if((*(val+vali)) > token_limit)
          {
            DS_AT_MSG2_MED("Token %d larger than limit %d",*(val+vali),token_limit);
            return DSAT_ERROR;
          }

          /* Reinitialize buffer */
          memset((void*)cp,0,sizeof(cp));
          cpi = 0;         
          vali++;
        }
      }
      c++;
    }
    
    /* Verify expected tokens found */
    if (num_tokens != vali)
    {
      DS_AT_MSG2_ERROR("Expected number of tokens violated: %d != %d",
                num_tokens,vali);
      return DSAT_ERROR;
    }

    /* Convert to number, processing tokens in order encountered */
    *num_ptr = 0;
    j = 0;
    for (i = num_tokens - 1; i >= 0; i--)
    {
      /* Check for number array element change (IPv6 only) */
      if (max_tokens_per_number <= j)
      {
        num_ptr++;
        if ((num_ptr - num_ptr_orig) >= num_ptr_max_len)
        {
          DS_AT_MSG1_ERROR("exceeded num_ptr_max_len %d",num_ptr_max_len);
          return DSAT_ERROR;
        }

        *num_ptr = 0;
        j = 0;
      }

      *num_ptr = (*num_ptr >> token_bits) | 
                 (val[i] << (NUM_BITS_VAL - token_bits));
      j++;

      /* If all the tokens per this 32 bit number have been processed and
         it didn't use up all 32 bits of number:
         right shift number down by quantity of unused bits.
         (this currently only applies to case  of TOS/mask) */
      if ( j == max_tokens_per_number &&
           NUM_BITS_VAL > (token_bits * max_tokens_per_number) ) 
      {
        *num_ptr = (*num_ptr >> (NUM_BITS_VAL - 
                                 (token_bits * max_tokens_per_number)));
      }
    }
  }
  else /* INT_TO_STRING */
  {
    uint32 num;
    int i,j;
    dsat_num_item_type   cgpiaf_add_format        = 0;
    dsat_num_item_type   cgpiaf_LeadingZero_format = 0;
    dsat_num_item_type   cgpiaf_CompressZero_format = 0;
    byte num_tokens_per_word = 
      ((ttype == TUPLE_TYPE_IPv6 || ttype == TUPLE_TYPE_IPv6_OCTETS) ? 
        (num_tokens >> 2) : num_tokens);
    byte delim_str[2];

    delim_str [0] = delimiter;
    delim_str [1] = '\0';

    memset ((void*)buf_ptr, 0, buf_len);
    j = num_tokens_per_word-1;

    /* uint32 from DSUMTS looks like this: 
       byte0="4", byte1="3", byte2="2", byte 3="1" for IP address like
       "1.2.3.4" so parse off token bits from bottom of uint32s
       inserting into val array starting from top */
    for (i = num_tokens - 1; i >= 0; i--)
    {
      if ((num_ptr - num_ptr_orig) >= num_ptr_max_len)
      {
        DS_AT_MSG1_ERROR("exceeded num_ptr_max_len %d",num_ptr_max_len);
        return DSAT_ERROR;
      }
      num = *num_ptr;
      num >>= (token_bits * ((num_tokens_per_word-1) - j));

      val[i] = (dsat_num_item_type)(num & token_mask);
      j--;
      
      /* Check for number array element change (IPv6 only) */
      if (0 > j)
      {
        num_ptr++;
        j = num_tokens_per_word-1;
      }
    }
    
    /* then go through val array of token integers creating delimited 
       string */
    /* Decides IPv6 address representation based on +CGPIAF setting*/
     cgpiaf_add_format          = (dsat_num_item_type)dsatutil_get_val(
                                      DSATETSI_EXT_CGPIAF_IDX,0,0,NUM_TYPE);

    if( (DSAT_CONVERT_IP == ip_mask)&&
        (ttype == TUPLE_TYPE_IPv6)&&
        (1 == cgpiaf_add_format))
    {
      int last_count = 0;
      int count = 0;
      int index = 0;
      int last_token = 0;
      byte work_buffer[6];

      cgpiaf_CompressZero_format  = (dsat_num_item_type)dsatutil_get_val(
                                        DSATETSI_EXT_CGPIAF_IDX,0,3,NUM_TYPE);
      if(1 == cgpiaf_CompressZero_format)
      {
        for(i = 0 ; i < num_tokens ; i++)
        {
          if(val[i] == 0)
          {   
            count++;
            if(last_count <= count)
            {
               last_count = count;
               index = i;
            }
          }
          else
          {
              count = 0;
          }
        }
      }
      DS_AT_MSG3_HIGH("MAX count %d last index %d first index %d \n",
                                last_count,index,index-last_count+1);
      
      cgpiaf_LeadingZero_format  = (dsat_num_item_type)dsatutil_get_val(
                                        DSATETSI_EXT_CGPIAF_IDX,0,2,NUM_TYPE);

      for( i = 0 ; i < num_tokens ; i++)
      {
        if (i == (num_tokens - 1))
          last_token = 1;

        if(i < index-last_count+1 || i > index)
        {
          if(1 == cgpiaf_LeadingZero_format)
          {
          snprintf((char *)work_buffer, sizeof(work_buffer),
                     "%04X%c",
                     val[i], (last_token ? '\0' : delimiter));
          }
          else
          {
            snprintf((char *)work_buffer, sizeof(work_buffer),
                     "%X%c",
                   val[i], (last_token ? '\0' : delimiter));
          }
          strlcat((char *)buf_ptr, (char *)work_buffer, buf_len);
        }
        else
        {
          if(i == index)
          {
            strlcat((char *)buf_ptr, (char *)delim_str, buf_len);
          }
          if(0 == index-last_count+1 && i == 0)
          {
            strlcat((char *)buf_ptr, (char *)delim_str, buf_len);
          }
        }
      }
    }
    else
    {
      /* This size is guaranteed not to overflow with any radix */
      byte work_buf[MAX_ITOA_LEN];
      for (i=0; i < num_tokens; i++)
      {
        dsatutil_itoa (val[i], work_buf, radix);
        strlcat((char *)buf_ptr, (char *)work_buf, buf_len);
        if (i < (num_tokens - 1))
          strlcat((char *)buf_ptr, (char *)delim_str, buf_len);
      }
    }    
  }

  return DSAT_OK;
} /* dsatutil_convert_tuple */

/*===========================================================================

FUNCTION DSATUTIL_CONVERT_ALPHA_GSM_WITH_AT_TO_ALPHA_GSM

DESCRIPTION
This function converts a GSM string which could contain a GSM '@' character (0x00)
to a GSM string with replacing '@' (0x00) to a special character (0xe6) which is known to 
ATCOP module alone. 

DEPENDENCIES
  This function should be invoked when the string (input to ATCOP) is comming from other
  than TE (eg: network or some gsdi modules etc)

  
RETURN VALUE
  possible values:
    TRUE: Succesfull conversion. The out_str is a NULL terminated string.
    FALSE: could not convert the string. 
SIDE EFFECTS
  None
  
===========================================================================*/
boolean dsatutil_convert_alpha_gsm_with_at_to_alpha_gsm
(
  const char *in_str, 
  char *out_str,
  uint16 in_len
)
{
  uint16 index = 0;
  
  if( (in_str == NULL)|| (out_str == NULL)|| in_len==0 )
  {
    DS_AT_MSG0_MED("Invalid len/string passed ");
    return FALSE;
  }
  while( index < in_len )
  {
    if ( in_str[index] == GSM_AT_CHAR )
    {
      out_str[index] = GSM_AT_CHAR_REPLACEMENT;
    }
    else
    {
      out_str[index]= in_str[index];
    }
    ++index;
  }
  out_str[index] = '\0';

  return TRUE;
}/* dsatutil_convert_alpha_gsm_with_at_to_alpha_gsm */


/*===========================================================================

FUNCTION DSATUTIL_CONVERT_CHSET

DESCRIPTION
  This function performs conversion of a given null terminated string in one 
  supported character set to another supported character set, writing it into
  an output buffer provided and null terminating it. It will handle 
  inconvertible characters as indicated by drop_inconvertible, simply not 
  copying them if TRUE or stopping conversion and returning NULL if FALSE. 
  The character with value 0x00 is not supported in the GSM 7 bit or IRA 
  alphabet as NULL is used for terminating strings, neither will the function 
  output this character value except as a termination. When converting from 
  HEX/UCS2 for instance this counts as an inconvertible character.
  It will stop the conversion if indicated maximum output buffer length 
  in bytes is reached.

DEPENDENCIES
  None.

RETURN VALUE
  possible values:
    NULL : if drop_inconvertible FALSE and conversion fails
    pointer to terminating NULL in output buffer : if conversion successful

SIDE EFFECTS
  None
  
===========================================================================*/
char *dsatutil_convert_chset
(
  const char *in_str, 
  dsati_chset_type in_chset,
  const char *out_str,
  dsati_chset_type out_chset,
  uint16 out_max,
  boolean drop_inconvertible
)
{
  #define LWR_BYTE 0x00FF
  /* Macro to convert a character to a NULL-terminated pair of hex characters
     at the location pointed to by out_str_dup, which is left pointing to the 
     terminating NULL */
  #define CVT_TO_HEX_PAIR(ch)\
    if (ch < 16) *out_str_dup++ = '0';\
    out_str_dup = (char *)dsatutil_itoa((uint32)ch, (byte *)out_str_dup, 16);

  char *in_str_dup = (char *)in_str;
  char *out_str_dup = (char *)out_str;
  uint8 in_char_len = 0, out_char_len = 0;
  char   conv_char;
  dsat_num_item_type conv_word;
  char   conv_buf[5] = {0};
  dsat_num_item_type in_char_val;
  char *tmp_chr_ptr = NULL;
  /* Mark the end of output buffer */
  char *last_char_lim;
  uint16 in_str_max_len; /* max length of the data area that can be accessed from in_str */

  ASSERT(( in_str != NULL ) && ( out_str != NULL ));

  DS_AT_MSG2_HIGH("in_char = %d, out_char = %d",in_chset,out_chset);
  if ( in_chset >= ALPHA_MAX ||
       out_chset >= ALPHA_MAX )
  {
    return NULL;
  }

  /* Save character lengths */
  switch ( in_chset )
  {
    case ALPHA_UCS2:
      in_char_len = UCS2_CHAR_LEN;
      break;
    case ALPHA_HEX:
      in_char_len = HEX_CHAR_LEN;
      break;
    case ALPHA_GSM:
    case ALPHA_GSM_WITH_AT:
      in_char_len = GSM_CHAR_LEN;
      break;
    default:
    case ALPHA_IRA:
    case ALPHA_PBM_8BIT:
      in_char_len = IRA_CHAR_LEN;
      break;
    case ALPHA_PBM_UCS2:
      in_char_len = PBM_UCS2_CHAR_LEN;
      break;
  }
  switch ( out_chset )
  {
    case ALPHA_UCS2:
      out_char_len = UCS2_CHAR_LEN;
      break;
    case ALPHA_HEX:
      out_char_len = HEX_CHAR_LEN;
      break;
    case ALPHA_GSM:
    case ALPHA_GSM_WITH_AT:
      out_char_len = GSM_CHAR_LEN;
      break;
    default:
    case ALPHA_IRA:
    case ALPHA_PBM_8BIT:
      out_char_len = IRA_CHAR_LEN;
      break;
    case ALPHA_PBM_UCS2:
      out_char_len = PBM_UCS2_CHAR_LEN;
      break;
  }

  /* Adjust last possible char position to save room for 
     the character size and a terminating NULL */
  if ( out_chset == ALPHA_PBM_UCS2 )
  {
   last_char_lim = (out_str_dup + out_max) - (2 * out_char_len );
  }
  else
  {
    last_char_lim = (out_str_dup + out_max) - (out_char_len+1);
  }
  /* compute max size of in_str */
  if (in_chset == ALPHA_8BIT)
  { 
    /* in the case of ALPHA_8BIT let the output buffer determine how much to read from in_str.  This 
    is because NULL characters can be expected in the input stream for conversion and hence can't rely
    on strlen() */
    in_str_max_len = (uint16)(((out_max-1)/out_char_len) * in_char_len); 
  }
  else if (in_chset == ALPHA_PBM_UCS2 )
  {
    in_str_max_len = (uint16)(dsatutil_ucs2_strlen(in_str) * 2);
  }
  else
  {
    /* For all other character sets, we're guaranteed to get NULL-terminated strings */
    in_str_max_len = (uint16)strlen(in_str_dup);
  }

  if ((in_str_max_len % in_char_len) != 0 )
  {
    /* Return with error condition if not a 
       'whole' number of input chars in input string */
    DS_AT_MSG2_ERROR("incorrect input string len %d for char set size %d",in_str_max_len-1,in_char_len);
    return NULL;
  }

  /* Go until there are characters still to be read AND 
  there is room left in out_str for converted output */
  while ( ((in_str_dup - in_str) < in_str_max_len) &&
          (out_str_dup <= last_char_lim) )
  {
    /* sanity check for internal error */
   if ((in_str_dup - in_str) % in_char_len) /* we didn't consume the right number of chars */
   {
     ERR_FATAL("Internal Error: in_char_len %d processed %d",in_char_len, in_str_dup-in_str,0);
   }

   switch ( in_chset )
    {
      /* Input character set IRA */
      case ALPHA_IRA:
        switch ( out_chset )
        {
          case ALPHA_GSM:
          case ALPHA_GSM_WITH_AT:
            if ( *in_str_dup == 0x40 ) /* ASCII @ */
            {
              if ( out_chset == ALPHA_GSM )
              {
                *out_str_dup++= GSM_AT_CHAR_REPLACEMENT; 
              }
              else
              {
                *out_str_dup++= GSM_AT_CHAR;
              }
            }
            else
            {
              conv_char = ira_to_gsm7[(uint8)(*in_str_dup)];
              if ( conv_char != MAX_8BIT_CHAR && conv_char >= MIN_IRA_GSM_CHAR )
              {
                *out_str_dup++ = conv_char;
              }
              else if ( !drop_inconvertible )
              {
                return NULL;
              }
              else
              {
                /* Replace inconvertible char with a space */
                *out_str_dup++ = (char)REPLACEMENT_CHAR;
              }
            }
            break;
          case ALPHA_UCS2:
            memset( out_str_dup, '0', 2 );
            out_str_dup += 2;
            CVT_TO_HEX_PAIR( *in_str_dup );
            break;
          case ALPHA_HEX:
            CVT_TO_HEX_PAIR( *in_str_dup );
            break;
          case ALPHA_IRA:
          case ALPHA_8BIT:
          case ALPHA_PBM_8BIT:
            *out_str_dup++ = *in_str_dup;
            break;
          case ALPHA_PBM_UCS2:
            *out_str_dup++ = *in_str_dup;
            *out_str_dup++ ='\0';
            break;
          /* If unknown output character set specified */
          default:
            return NULL;
        }
        /* Advance by one input octet */
        in_str_dup += IRA_CHAR_LEN;
        break;

      /* Input character set GSM */
      case ALPHA_GSM:
        if ( (*in_str_dup > MAX_GSM_CHAR )&& (*in_str_dup != GSM_AT_CHAR_REPLACEMENT ))
        {
          return NULL;
        }
        switch ( out_chset )
        {
          case ALPHA_IRA:
            if ( *in_str_dup == GSM_AT_CHAR_REPLACEMENT)
            {
              *out_str_dup++ = 0x40; /* 0x40 is IRA '@' character */
            }
            else if ( (conv_char = gsm7_to_ira[(uint8)(*in_str_dup)]) != MAX_8BIT_CHAR ) 
            {
              *out_str_dup++ = conv_char;
            }
            else if ( !drop_inconvertible )
            {
              return NULL;
            }
            else
            {
              /* Replace inconvertible char with a space */
              *out_str_dup++ = (char)REPLACEMENT_CHAR;
            }
            break;
          case ALPHA_UCS2:
            /* First convert to PBM 8bit alphabet, which is nearly identical to
               UCS2: if escape character is found in_str_dup will be advanced 
               by one and left pointing at character following it */
            if ( *in_str_dup == GSM_AT_CHAR_REPLACEMENT )
            {
              memset( out_str_dup, '0', 2 );
              out_str_dup += 2;
              /* 0x40 is UCS2 equivalent for @ */
              out_str_dup = 
                 (char *)dsatutil_itoa( 0x40, (byte *)out_str_dup, 16);
            }
            else
            {
              gsm_char_to_pbm( &in_str_dup, &conv_char );
              /* If the PBM 8bit character is one not common to UCS2... */
              if ( NULL != (tmp_chr_ptr = 
                          (char *)strchr( pbm8_to_ucs2_not_common, conv_char )) )
              {
                /* Replace it with the proper UCS2 value as hex 4-tuple */
                (void)strlcpy( out_str_dup, 
                                  (char *)ucs2_to_pbm8_not_common[tmp_chr_ptr - 
                                                        pbm8_to_ucs2_not_common],
                                   UCS2_CHAR_LEN + 1 );
                out_str_dup += UCS2_CHAR_LEN;
              }
              else
              {
                *out_str_dup++ = '0';
                out_str_dup = (char *)dsatutil_itoa( 0, (byte *)out_str_dup, 16);
                CVT_TO_HEX_PAIR( conv_char );
              }
            }
            break;
          case ALPHA_HEX:
            if ( *in_str_dup == GSM_AT_CHAR_REPLACEMENT )
            {
              /* 0x40 is UCS2 equivalent for @ */
              out_str_dup = 
                 (char *)dsatutil_itoa( 0x40, (byte *)out_str_dup, 16);
            }
            else
            {
              CVT_TO_HEX_PAIR( *in_str_dup );
            }
            break;
          case ALPHA_8BIT:
          case ALPHA_GSM:
            *out_str_dup++ = *in_str_dup;
            break;
          case ALPHA_GSM_WITH_AT:
           if ( *in_str_dup == GSM_AT_CHAR_REPLACEMENT )
           {
             *out_str_dup++ = GSM_AT_CHAR;
           }
           else
           {
             *out_str_dup++ = *in_str_dup;
           }
           break; 
          case ALPHA_PBM_8BIT:
            /* if escape character is found in_str_dup will be advanced by one
               and left pointing at character following it */
            if ( *in_str_dup == GSM_AT_CHAR_REPLACEMENT )
            {
              *out_str_dup++=0x40; /* 0x40 is PBM8bit '@' character */
            }
            else
            {
              gsm_char_to_pbm( &in_str_dup, out_str_dup++ );
            }
            break;
          case ALPHA_PBM_UCS2:
            /* if escape character is found in_str_dup will be advanced by one
               and left pointing at character following it */
            if ( *in_str_dup == GSM_AT_CHAR_REPLACEMENT )
            {
              *out_str_dup++=0x40; /* 0x40 is PBM8bit '@' character */
            }
            else
            {
              gsm_char_to_pbm( &in_str_dup, out_str_dup++ );
            }
            *out_str_dup++ ='\0';
            break;
          /* If unknown output character set specified */
          default:
            return NULL;
        }
        /* Advance by one input octet */
        in_str_dup += GSM_CHAR_LEN;
        break;

      /* Input character set UCS2 */
      case ALPHA_UCS2:
        /* Input UCS2 character must be composed completely of HEX chars */
        if ( strchr(hex_chars, *in_str_dup) == NULL ||
             strchr(hex_chars, *(in_str_dup + 1)) == NULL ||
             strchr(hex_chars, *(in_str_dup + 2)) == NULL ||
             strchr(hex_chars, *(in_str_dup + 3)) == NULL ) 
        {
          return NULL;
        }

        switch ( out_chset )
        {
          case ALPHA_IRA:
          case ALPHA_8BIT:
          case ALPHA_HEX:
            /* If first 2 chars are not '0's */
            if ( *in_str_dup != '0' ||
                 *(in_str_dup + 1) != '0' )
            {
              if ( drop_inconvertible )
              {
                /* Replace with a space */
                *out_str_dup++ = (char)REPLACEMENT_CHAR;
              }
              else
              {
                return NULL;
              }
            }
            else
            {
              if ( ALPHA_HEX == out_chset )
              {
                (void) dsatutil_memscpy((void*)out_str_dup,
                        HEX_CHAR_LEN,(void*)(in_str_dup+2),HEX_CHAR_LEN);             
                out_str_dup += HEX_CHAR_LEN;
              }
              else /* ALPHA_IRA or ALPHA_8BIT */
              {
                /* First convert to a char... */
                (void)strlcpy( conv_buf, in_str_dup+2, HEX_CHAR_LEN + 1 );
                /* Already verified input is all HEX chars 
                   so safe to ignore return value */
                (void)dsatutil_atoi( &in_char_val, 
                                     (const byte *)&conv_buf[0], 16 );

                /* Extended IRA is 8 bit alphabet and character 
                   with value 0x00 is not allowed*/
                if ( out_chset == ALPHA_IRA &&
                     ( in_char_val < MIN_IRA_GSM_CHAR ||
                       in_char_val > MAX_GSM_CHAR ) )
                {
                  if ( !drop_inconvertible )
                  {
                    return NULL;
                  }
                  else
                  {
                    /* Replace inconvertible char with a space */
                    *out_str_dup++ = (char)REPLACEMENT_CHAR;
                  }
                }
                else
                {
                  *out_str_dup++ = (uint8)in_char_val;
                }
              }
            }
            break;
          case ALPHA_GSM:
          case ALPHA_PBM_8BIT:
          case ALPHA_GSM_WITH_AT:
            /* Copy UCS2 character (as hex 4-tuple) into temporary buffer */
            (void)strlcpy( conv_buf, in_str_dup, UCS2_CHAR_LEN + 1 );
            /* If found in array of characters not common to both 
               PBM8 and UCS2... */
            if ( PARAM_HIT == dsatutil_srch_list( 
                                     &conv_word,  
                                     ucs2_to_pbm8_not_common,     
                                     (const byte *)conv_buf,
                                     ACTION_QUOTE_NO_QUOTE ) )
            {
              /* Get word from same offset of complementary array */
              in_char_val = pbm8_to_ucs2_not_common[conv_word];
            }
            else if ( PARAM_HIT == dsatutil_srch_list( 
                                          &conv_word,
                                          ucs2_to_pbm8_not_supported,     
                                          (const byte *)conv_buf,
                                          ACTION_QUOTE_NO_QUOTE ) )
            {
              in_char_val = (char)REPLACEMENT_CHAR;
            }
            else
            {
              /* Convert to a word:
                 Already verified input is all HEX chars 
                 so safe to ignore return value */
              (void)dsatutil_atoi( &in_char_val, 
                                   (const byte *)conv_buf, 16 );
            }

            /* If upper byte is not zero */
            if ( in_char_val > MAX_8BIT_CHAR )
            {
              if ( drop_inconvertible )
              {
                /* Replace with a space */
                *out_str_dup++ = (char)REPLACEMENT_CHAR;         
              }
              else
              {
                return NULL;
              }
            }
            else
            {
              if ( ALPHA_GSM == out_chset || ALPHA_GSM_WITH_AT == out_chset)
              {
                if ( (in_char_val & LWR_BYTE) == 0x40 ) /* UCS2 @ char */
                {
                  if ( ALPHA_GSM == out_chset )
                  {
                    *out_str_dup = GSM_AT_CHAR_REPLACEMENT; 
                  }
                  else
                  {
                    *out_str_dup = GSM_AT_CHAR;
                  }
                }
                else
                {
                  pbm_char_to_gsm( (char)(in_char_val & LWR_BYTE), &out_str_dup );
                }
              }
              else
              {
                *out_str_dup = (char)(in_char_val & LWR_BYTE);
              }
              out_str_dup += GSM_CHAR_LEN;
            }
            break;
          case ALPHA_UCS2:
            (void) dsatutil_memscpy((void*)out_str_dup,
                    UCS2_CHAR_LEN,(void*)in_str_dup,UCS2_CHAR_LEN);
            out_str_dup += UCS2_CHAR_LEN;
            break;
          case ALPHA_PBM_UCS2:
            /* Convert the lower 16 bits of the input and add to output then 
             * the upper 16 bits */
            (void)strlcpy( conv_buf, in_str_dup + 2, PBM_UCS2_CHAR_LEN + 1 );
            (void)dsatutil_atoi( &in_char_val, 
                                 (const byte *)conv_buf, 16 );
            *out_str_dup++ = (char)in_char_val;
            (void)strlcpy( conv_buf, in_str_dup, PBM_UCS2_CHAR_LEN + 1 );
            (void)dsatutil_atoi( &in_char_val, 
                                 (const byte *)conv_buf, 16 );
            *out_str_dup++ = (char)in_char_val;
            break;
          /* If unknown output character set specified */
          default:
            return NULL;
        }
        in_str_dup += UCS2_CHAR_LEN;
        break;

      /* Input character set HEX */
      case ALPHA_HEX:
        /* Input HEX character pair must be composed completely of HEX chars */
        if ( strchr(hex_chars, *in_str_dup) == NULL ||
             strchr(hex_chars, *(in_str_dup + 1)) == NULL )
        {
          return NULL;
        }

        switch ( out_chset )
        {
          case ALPHA_IRA:
          case ALPHA_GSM:
          case ALPHA_8BIT:
          case ALPHA_GSM_WITH_AT:
            (void)strlcpy( conv_buf, in_str_dup, (HEX_CHAR_LEN + 1) );

            /* Already verified input is all HEX chars 
               so safe to ignore return value */
            (void)dsatutil_atoi( &in_char_val,
                           (const byte *)&conv_buf[0], 16 );


            /* GSM and IRA are 7 bit & 8 bits alphabets respectively 
               and character with value 0x00 is not allowed */
            if ( out_chset != ALPHA_8BIT &&
                 ( in_char_val < MIN_IRA_GSM_CHAR ||
                   in_char_val > MAX_GSM_CHAR ) )
            {
              if ( !drop_inconvertible )
              {
                return NULL;
              }
              else
              {
                /* Replace inconvertible char with a space */
                *out_str_dup++ = (char)REPLACEMENT_CHAR;
              }
            }
            else
            { /* Check for HEX @ character */
              if ( ( in_char_val == 0x40 ) && ( out_chset == ALPHA_GSM ))
              {
                *out_str_dup++ = GSM_AT_CHAR_REPLACEMENT;
              }
              else if ( ( in_char_val == 0x40 ) && ( out_chset == ALPHA_GSM_WITH_AT ))
              {
                *out_str_dup++ = GSM_AT_CHAR;
              }
              else
              {
                *out_str_dup++ = (uint8)in_char_val;
              }
            }
            break;
          case ALPHA_UCS2:
            memset( out_str_dup, '0', HEX_CHAR_LEN );
            out_str_dup += HEX_CHAR_LEN;
            break;
          case ALPHA_HEX:
            (void) dsatutil_memscpy((void*)out_str_dup,
                                HEX_CHAR_LEN,(void*)in_str_dup,HEX_CHAR_LEN);
            out_str_dup += HEX_CHAR_LEN;
            break;
          /* If unsupported output character set specified */
          default:
            return NULL;
        }
        in_str_dup += HEX_CHAR_LEN;
        break;

      /* Input character set 8BIT */
      case ALPHA_8BIT:
        switch ( out_chset )
        {
          case ALPHA_IRA:
          case ALPHA_GSM:
          case ALPHA_GSM_WITH_AT:
            if ( *in_str_dup < MIN_IRA_GSM_CHAR ||
                 *in_str_dup > MAX_GSM_CHAR )
            {
              if ( !drop_inconvertible )
              {
               return NULL;
              }
              else
              {
                /* Replace inconvertible char with a space */
                *out_str_dup++ = (char)REPLACEMENT_CHAR;
              }
            }
            else
            {  /* check for ALPHA 8 BIT @ character */
              if ( ( *in_str_dup == 0x40 ) && ( out_chset == ALPHA_GSM ))
              {
                *out_str_dup++ = GSM_AT_CHAR_REPLACEMENT;
              }
              else if ( ( *in_str_dup == 0x40 ) && ( out_chset == ALPHA_GSM_WITH_AT ) )
              {
                *out_str_dup++ = GSM_AT_CHAR;
              }
              else
              {
                *out_str_dup++ = (uint8)*in_str_dup;
              }
            }
            break;
          case ALPHA_UCS2:
            memset( out_str_dup, '0', 2 );
            out_str_dup += 2;
            CVT_TO_HEX_PAIR( *in_str_dup );
            break;
          case ALPHA_HEX:
            CVT_TO_HEX_PAIR( *in_str_dup );
            break;
          /* If unsupported output character set specified */
          default:
            return NULL;
        }
        in_str_dup += EIGHT_BIT_CHAR_LEN;
        break;

      /* Input character set PBM 8BIT */
      case ALPHA_PBM_8BIT:
        switch ( out_chset )
        {
          case ALPHA_IRA:
            /* PBM 8bit and IRA T.50 are the same over the range 0x00-0x7F */
            *out_str_dup++ = ( *in_str_dup <= MAX_GSM_CHAR ) ?
                               *in_str_dup : REPLACEMENT_CHAR;
            break;
          case ALPHA_UCS2:
            /* If the PBM 8bit character is one not common to UCS2... */
            if ( NULL != (tmp_chr_ptr = 
                      (char *)strchr( pbm8_to_ucs2_not_common, *in_str_dup )) )
            {
              /* Replace it with the proper UCS2 value as hex 4-tuple */
              (void)strlcpy( out_str_dup, 
                                (char *)ucs2_to_pbm8_not_common[tmp_chr_ptr - 
                                                      pbm8_to_ucs2_not_common],
                                 UCS2_CHAR_LEN + 1);
              out_str_dup += UCS2_CHAR_LEN;
            }
            else
            {
              /* Converting from 8bit alphabet upper 16 bits always 0 */
              memset( out_str_dup, '0', 2 );
              out_str_dup += 2;
              CVT_TO_HEX_PAIR( *in_str_dup );
            }
            break;
          case ALPHA_GSM:
          case ALPHA_GSM_WITH_AT: /* first check for PBM 8 bit @ character */
            if ( ( out_chset == ALPHA_GSM ) && ( *in_str_dup == 0x40 ))
            {
              *out_str_dup = GSM_AT_CHAR_REPLACEMENT; 
            }
            else if ( ( out_chset == ALPHA_GSM_WITH_AT ) && ( *in_str_dup == 0x40 ))
            {
              *out_str_dup = GSM_AT_CHAR; 
            }
            else
            {
              pbm_char_to_gsm( *in_str_dup, &out_str_dup );
            }
            out_str_dup++;
            break;
          /* If unsupported output character set specified */
          default:
            DS_AT_MSG0_ERROR("Conversion from PBM alpha limited to IRA, GSM, UCS2");
            return NULL;
        }
        in_str_dup += PBM_8BIT_CHAR_LEN;
        break;
      /* Input character set PBM UCS2 */
      case ALPHA_PBM_UCS2:
        switch ( out_chset )
        {
          case ALPHA_IRA:
            /* Conversion not possible if in the range 0x0100-0xFFFF */
            if ( *(in_str_dup + 1) != '\0' )
            {
              *out_str_dup++ = REPLACEMENT_CHAR;
              break;
            }
            /* PBM UCS2 and IRA T.50 are the same over the range 0x0000-0x007F */
            *out_str_dup++ = ( *in_str_dup <= MAX_GSM_CHAR ) ?
                               *in_str_dup : REPLACEMENT_CHAR;
            break;
          case ALPHA_GSM:
          case ALPHA_GSM_WITH_AT: 
            /* Conversion not possible if in the range 0x0100-0xFFFF */
            if ( *(in_str_dup + 1) != '\0' )
            {
              *out_str_dup++ = REPLACEMENT_CHAR;
              break;
            }
            /* first check for PBM UCS2 bit @ character */
            if ( ( out_chset == ALPHA_GSM ) && ( *in_str_dup == 0x40 ))
            {
              *out_str_dup = GSM_AT_CHAR_REPLACEMENT; 
            }
            else if ( ( out_chset == ALPHA_GSM_WITH_AT ) && ( *in_str_dup == 0x40 ))
            {
              *out_str_dup = GSM_AT_CHAR; 
            }
            else
            {
              pbm_char_to_gsm( *in_str_dup, &out_str_dup );
            }
            out_str_dup++;
            break;
          case ALPHA_UCS2:
              CVT_TO_HEX_PAIR( *(in_str_dup + 1) );
              CVT_TO_HEX_PAIR( *in_str_dup );
            break;
          /* If unknown output character set specified */
          default:
            DS_AT_MSG0_ERROR("Conversion from ALPHA_PBM_UCS2 is limited to IRA, GSM, UCS2");
            return NULL;
        }
        in_str_dup += PBM_UCS2_CHAR_LEN;
        break ;
      /* If unknown input character set specified */
      default:
        DS_AT_MSG1_ERROR("Invalid input character set specified %d",
                   in_chset);
        return NULL;
    }
  }
  
  if (out_chset == ALPHA_PBM_UCS2)
  {
    *out_str_dup++ = '\0';
  }
  *out_str_dup = '\0';
  return out_str_dup;
} /* dsatutil_convert_chset */

/*===========================================================================

FUNCTION DSATUTIL_CONVERT_GSM_UCS2_TO_PBM_UCS2

DESCRIPTION
  This function performs the special conversion of one of the three GSM UCS2
  codings specified in 3GPP GSM TS 11.11 Annex B into PBM UCS2 format.  Sets 
  the terminating NULL (0x0000) character if there is enough room in out_ary
  for it; if there isn't then no error is thrown.

DEPENDENCIES
  None.

RETURN VALUE
  Error value enum

SIDE EFFECTS
  None
  
===========================================================================*/
dsat_cme_error_e_type dsatutil_convert_gsm_ucs2_to_pbm_ucs2
(
  const char  *in_str,
  uint16      *out_ary,
  uint16       out_max /* size of out_ary (in uint16's, NOT bytes) */
)
{
  uint16  i;
  uint16  page_offset = 0;
  uint16  out_idx = 0;
  uint16  in_len;
  uint16  ucs2_char_tmp;
  uint8   octet_tmp;
  uint8   coding_scheme = 0;
  uint8   num_chars = 0;

  /* Coding scheme identifiers */
  #define GSM_UCS2_CODING_SCHEME_ONE    0x80
  #define GSM_UCS2_CODING_SCHEME_TWO    0x81
  #define GSM_UCS2_CODING_SCHEME_THREE  0x82

  in_len = strlen(in_str);
  if ( (in_len % HEX_CHAR_LEN) )
  {
    DS_AT_MSG1_ERROR("invalid input length %d",in_len);
    return DSAT_CME_UNKNOWN;
  }

  for (i = 0; i < in_len; i += HEX_CHAR_LEN)
  {
    /* First, determine that each octet is within the valid range for hex chars */
    if ( strchr(hex_chars, UPCASE(in_str[i])) == NULL ||
         strchr(hex_chars, UPCASE(in_str[i + 1])) == NULL )
    {
      return DSAT_CME_INVALID_CHAR_IN_TEXT;
    }

    /*--------------------------------------------------------------------------
     *  Process special meaning octets
     *    Note: each branch has a continue at the end so if a special header
     *          octet is processed, it will skip normal processing below
     *------------------------------------------------------------------------*/
    if ( i == 0 )
    {
      /* First octet determines which coding scheme we use */
      (void) dsatutil_hex_to_uint8( in_str, &coding_scheme );
      if ( (coding_scheme < GSM_UCS2_CODING_SCHEME_ONE) ||
           (coding_scheme > GSM_UCS2_CODING_SCHEME_THREE) )
      {
        DS_AT_MSG1_ERROR("invalid GSM coding scheme 0x%02x", coding_scheme);
        return DSAT_CME_INVALID_CHAR_IN_TEXT;
      }
      continue;
    }
    else if ( (i == 2) &&
              ( (coding_scheme == GSM_UCS2_CODING_SCHEME_TWO) ||
                (coding_scheme == GSM_UCS2_CODING_SCHEME_THREE) ) )
    {
      /* For coding schemes 2 & 3, the second octet is the number of characters 
         in the string */
      (void) dsatutil_hex_to_uint8( &in_str[i], &num_chars );
      if ( num_chars == 0 )
      {
        DS_AT_MSG0_ERROR("number of characters in coding scheme is 0");
        return DSAT_CME_UNKNOWN;
      }
      else if ( num_chars > out_max )
      {
        DS_AT_MSG2_ERROR("too many characters specified %d - max is %d",
                  num_chars, out_max);
        return DSAT_CME_TEXT_STRING_TOO_LONG;
      }
      continue;
    }
    else if ( i == 4 )
    {
      if ( coding_scheme == GSM_UCS2_CODING_SCHEME_TWO )
      {
        /* The 3rd octet in coding scheme two contains an 8 bit number which 
           defines bits 15 to 8 of a 16 bit base pointer, where bit 16 is set to 
           zero, and bits 7 to 1 are also set to zero */
        (void) dsatutil_hex_to_uint8( &in_str[i], &octet_tmp );
        page_offset = (octet_tmp << 7);
        continue;
      }
      else if ( coding_scheme == GSM_UCS2_CODING_SCHEME_THREE )
      {
        /* For coding scheme 3, the 2nd & 3rd octet specify the page offset,
           so make sure we have a 3rd octet then parse it with the 2nd octet */
        if (in_len < 6)
        {
          DS_AT_MSG1_ERROR("not enough characters (%d) for coding scheme 3",in_len);
          return DSAT_CME_UNKNOWN;
        }
        (void) dsatutil_hex_to_uint16( &in_str[i], &page_offset );
        i += HEX_CHAR_LEN;
        continue;
      }
    }

    /*--------------------------------------------------------------------------
     * Process normal characters
     *------------------------------------------------------------------------*/
    switch ( coding_scheme )
    {
      /* All octets now represent UCS2 characters.  Unused fields are set to 0xFFFF */
      case GSM_UCS2_CODING_SCHEME_ONE:
      {
        /* Make sure we have at least 2 octets left then parse */
        if ( (in_len - i) < UCS2_CHAR_LEN )
        {
          DS_AT_MSG2_ERROR("input character stream incomplete (len = %d i = %d)",in_len,i);
          return DSAT_CME_UNKNOWN;
        }
        (void) dsatutil_hex_to_uint16( &in_str[i], &ucs2_char_tmp );

        /* Ignore unused characters (0xFFFF) */
        if ( ucs2_char_tmp != 0xFFFF )
        {
          /* Make sure we have enough room in the output array for another character */
          if ( out_idx >= out_max )
          {
            DS_AT_MSG0_ERROR("not enough room in output");
            return DSAT_CME_TEXT_STRING_TOO_LONG;
          }
          out_ary[out_idx++] = ucs2_char_tmp;
        }
        i += HEX_CHAR_LEN; /* Add 2 to i since we processed 4 hex characters */
        break;
      } /* case GSM_UCS2_CODING_SCHEME_ONE */

      case GSM_UCS2_CODING_SCHEME_TWO:
      case GSM_UCS2_CODING_SCHEME_THREE:
      {
        /* Make sure we have enough room in the output array for another character */
        if ( out_idx >= out_max )
        {
          DS_AT_MSG0_ERROR("not enough room in output");
          return DSAT_CME_TEXT_STRING_TOO_LONG;
        }

        /* If bit 8 of the octet is set to zero, the remaining 7 bits of the 
           octet contain a GSM Default Alphabet character, whereas if bit 8 of 
           the octet is set to one, then the remaining seven bits are an offset 
           value */
        (void) dsatutil_hex_to_uint8( &in_str[i], &octet_tmp );
        if ( octet_tmp & 0x80 )
        {
          out_ary[out_idx++] = page_offset + (octet_tmp & 0x7F);
        }
        else
        {
          if ( octet_tmp == GSM_AT_CHAR )
          {
            out_ary[out_idx++] = 0x0040; /* '@' character */
          }
          else
          {
            char  conv_tmp;
            char  tmp_chr_ary[3];
            char *tmp_chr_ptr;

            /* Need to do special setup if using an escape character, but don't
               need to worry about GSM_AT_CHAR_REPLACEMENT) */
            tmp_chr_ary[0] = (char) octet_tmp;
            if ( octet_tmp == GSM_EXT_TBL_ESC_CHAR )
            {
              /* Need another octet since this is an escape sequence */
              i += HEX_CHAR_LEN;
              if ( (in_len - i) < HEX_CHAR_LEN )
              {
                DS_AT_MSG0_ERROR("input character stream incomplete for GSM escape sequence");
                return DSAT_CME_INVALID_CHAR_IN_TEXT;
              }
              (void) dsatutil_hex_to_uint8( &in_str[i], (uint8 *) &tmp_chr_ary[1] );
              tmp_chr_ary[2] = '\0';
            }
            else
            {
              tmp_chr_ary[1] = '\0';
            }
            tmp_chr_ptr = tmp_chr_ary;
            gsm_char_to_pbm( &tmp_chr_ptr, &conv_tmp );

            if ( NULL != (tmp_chr_ptr = 
                          (char *) strchr( pbm8_to_ucs2_not_common, conv_tmp )) )
            {
              /* GSM->PBM conversion doesn't match GSM->UCS2, so use the proper
                 UCS2 value */
              (void) dsatutil_hex_to_uint16( 
                        (const char *) ucs2_to_pbm8_not_common[tmp_chr_ptr - pbm8_to_ucs2_not_common],
                        &out_ary[out_idx++]);
            }
            else
            {
              /* Can safely use the PBM 8bit value as UCS2 */
              out_ary[out_idx++] = conv_tmp;
            }
          }
        }
        break;
      } /* case GSM_UCS2_CODING_SCHEME_TWO or THREE */
    } /* switch( coding_scheme ) */

    /* Break out of loop early if we hit the expected number of characters 
       (this is per spec) */
    if ( (num_chars != 0) && (out_idx == num_chars) )
    {
      break;
    }
  } /* for( each octet in input string )*/

  /* If the coding scheme specified the number of characters, make sure it
     matches what we processed */
  if ( (num_chars != 0) && (out_idx != num_chars) )
  {
    DS_AT_MSG2_ERROR("did not process expected number of characters; expected %d got %d",
              num_chars, out_idx);
    return DSAT_CME_UNKNOWN;
  }

  /* Set the terminating NULL character if we can */
  if ( out_idx < out_max )
  {
    out_ary[out_idx] = 0;
  }
  return DSAT_CME_NO_ERROR;
} /* dsatutil_convert_gsm_ucs2_to_pbm_ucs2() */

/*===========================================================================
FUNCTION DSATUTIL_HEX_TO_UINT8

DESCRIPTION
  Converts the first two characters of a hex string into uint8 
  representation.

DEPENDENCIES
  None

RETURN VALUE
  TRUE on success, FALSE if dsatutil_atoi failed (i.e. invalid character
  in string)

SIDE EFFECTS
  None.
===========================================================================*/
LOCAL boolean dsatutil_hex_to_uint8
(
  const char *in_octet,
  uint8      *out_uint8
)
{
  char tmp[3];
  dsat_num_item_type out_tmp;

  (void) dsatutil_memscpy((void*)tmp, sizeof(tmp),(void*)in_octet,2);
  tmp[2] = '\0';

  if ( ATOI_OK == dsatutil_atoi( &out_tmp, (const byte *) tmp, 16 ) )
  {
    *out_uint8 = (uint8) out_tmp;
    return TRUE;
  }
  return FALSE;
} /* dsatutil_hex_to_uint8() */

/*===========================================================================
FUNCTION DSATUTIL_HEX_TO_UINT16

DESCRIPTION
  Converts the first four characters of a hex string into uint16 
  representation.

DEPENDENCIES
  None

RETURN VALUE
  TRUE on success, FALSE if dsatutil_atoi failed (i.e. invalid character
  in string)

SIDE EFFECTS
  None.
===========================================================================*/
LOCAL boolean dsatutil_hex_to_uint16
(
  const char *in_str,
  uint16     *out_uint16
)
{
  char tmp[5];
  dsat_num_item_type out_tmp;
  (void) dsatutil_memscpy((void*)tmp, sizeof(tmp),(void*)in_str,4); 
  tmp[4] = '\0';
  if ( ATOI_OK == dsatutil_atoi( &out_tmp, (const byte *) tmp, 16 ) )
  {
    *out_uint16 = (uint16) out_tmp;
    return TRUE;
  }
  return FALSE;
} /* dsatutil_hex_to_uint16() */

/*===========================================================================
FUNCTION dsatutil_ucs2_strlen

DESCRIPTION
  This function counts the number of UCS2 characters in a string.
  The  string must be terminated by 0x0000.

DEPENDENCIES
  None

RETURN VALUE
  The number of UCS2 characters in the given string.

SIDE EFFECTS
  None.
===========================================================================*/
 uint16 dsatutil_ucs2_strlen
(
  const char* str
)
{
  uint16 i = 0;
  while( str[i] || str[i + 1])
  {
    i += 2;
  }
  return i/2;
}

/*===========================================================================

FUNCTION pbm_char_to_gsm

DESCRIPTION
  This function performs conversion of a single PBM 8bit character into GSM
  7bit alphabet. On return **gsm_out will point to the last converted character 
  written and the pointer *gsm_out may have been advanced by one place encode 
  a GSM character of the alphabet extension table.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
  
===========================================================================*/
LOCAL void pbm_char_to_gsm
(
  char pbm_in, 
  char **gsm_out
)
{
  char *tmp_chr_ptr = NULL;
  /* Complementary arrays for converting between PBM 8bit and GSM 7bit:
     for those values that aren't common to both.
     Used separate arrays for pbm_char_to_gsm() and gsm_char_to_pbm() to handle
     the conversion of GSM character '@'. */
  const char pbm8_to_gsm7_single[] = 
  {
    0x24,0x40,0x5F,0x90,0x92,0x93,0x94,0x95,0x96,0x97,0x98,0x99,0x9A,0xA1,0xA3,0xA4,
    0xA5,0xA7,0xBF,0xC4,0xC5,0xC6,0xC7,0xC9,0xD1,0xD6,0xD8,0xDC,0xDF,0xE0,0xE4,0xE5,
    0xE6,0xE8,0xE9,0xEC,0xF1,0xF2,0xF6,0xF8,0xF9,0xFC,0x00
  };
  const char gsm7_to_pbm8_single[] = 
  {
    0x02,0x20,0x11,0x10,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x40,0x01,0x24,
    0x03,0x5F,0x60,0x5B,0x0E,0x1C,0x09,0x1F,0x5D,0x5C,0x0B,0x5E,0x1E,0x7F,0x7B,0x0F,
    0x1D,0x04,0x05,0x07,0x7D,0x08,0x7C,0x0C,0x06,0x7E,0x00
  };

  ASSERT( NULL != gsm_out && NULL != *gsm_out );

  /* If pbm_in character is in the range 0x00-0x7F and
     is in the group of Basic Latin characters common to PBM 8bit and 
     GSM 7bit alphabets... */
  if ( pbm_in <= MAX_GSM_CHAR &&
       NULL == strchr( basic_latin_not_common, pbm_in ) )
  {
    **gsm_out = pbm_in;
  }
  /* If convertible to GSM 7bit as a single character... */
  else if ( NULL != 
            ( tmp_chr_ptr = (char *)strchr( pbm8_to_gsm7_single, pbm_in ) ) )
  {
    /* Output character at same offset in complementary array */
    **gsm_out = gsm7_to_pbm8_single[tmp_chr_ptr - pbm8_to_gsm7_single];
  }
  /* If convertible to GSM 7bit as two characters:
     part of the GSM 7bit alphabet extension table of 23.038... */
  else if ( NULL != 
            ( tmp_chr_ptr = (char *)strchr( pbm8_to_gsm7_exttbl, pbm_in ) ) )
  {
    /* Output escape character, advancing pointer... */
    *(*gsm_out)++ = (char)GSM_EXT_TBL_ESC_CHAR;
    /* Then output character at same offset in complementary array
       implementing extension table */
    **gsm_out = gsm7_to_pbm8_exttbl[tmp_chr_ptr - pbm8_to_gsm7_exttbl];
  }
  else
  {
    /* Replace inconvertible character with a space */
    **gsm_out = (char)REPLACEMENT_CHAR;
  }
}



/*===========================================================================

FUNCTION gsm_char_to_pbm

DESCRIPTION
  This function performs conversion of a one or two GSM 7bit characters
  into the PBM 8bit alphabet. If the escape character is encountered the
  function will advance gsm_in by one character and attempt to find the next
  character in the alphabet extension table. If found this is conversion 
  character, if not found it will use the replacement character. 
  On return *pbm_out will have been set to the converted character but 
  pointer will not have been advanced.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
  
===========================================================================*/
LOCAL void gsm_char_to_pbm
(
  char **gsm_in, 
  char *pbm_out
)
{
  char *tmp_chr_ptr = NULL;
  /* Complementary arrays for converting between PBM 8bit and GSM 7bit:
     for those values that aren't common to both.
     Used separate arrays for pbm_char_to_gsm() and gsm_char_to_pbm() to handle
     the conversion of GSM character '@'. */
  const char pbm8_to_gsm7_single[] = 
  {
    0x24,0x5F,0x90,0x92,0x93,0x94,0x95,0x96,0x97,0x98,0x99,0x9A,0xA1,0xA3,0xA4,
    0xA5,0xA7,0xBF,0xC4,0xC5,0xC6,0xC7,0xC9,0xD1,0xD6,0xD8,0xDC,0xDF,0xE0,0xE4,
    0xE5,0xE6,0xE8,0xE9,0xEC,0xF1,0xF2,0xF6,0xF8,0xF9,0xFC,0x00
  };
  const char gsm7_to_pbm8_single[] = 
  {
    0x02,0x11,0x10,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x40,0x01,0x24,
    0x03,0x5F,0x60,0x5B,0x0E,0x1C,0x09,0x1F,0x5D,0x5C,0x0B,0x5E,0x1E,0x7F,0x7B,
    0x0F,0x1D,0x04,0x05,0x07,0x7D,0x08,0x7C,0x0C,0x06,0x7E,0x00
  };

  ASSERT( NULL != gsm_in && NULL != *gsm_in && NULL != pbm_out );

  /* If gsm_in character is in the range 0x00-0x7F and
     is in the group of Basic Latin characters common to PBM 8bit and 
     GSM 7bit alphabets... */
  if ( **gsm_in <= MAX_GSM_CHAR &&
       NULL == strchr( basic_latin_not_common, **gsm_in ) )
  {
    *pbm_out = **gsm_in;
  }
  /* If a single non-escape character is convertible to PBM 8bit... */
  else if ( GSM_EXT_TBL_ESC_CHAR != **gsm_in &&
            NULL != ( tmp_chr_ptr = 
                        (char *)strchr( gsm7_to_pbm8_single, **gsm_in ) ) )
  {
    /* Output character at same offset in complementary array */
    *pbm_out = pbm8_to_gsm7_single[tmp_chr_ptr - gsm7_to_pbm8_single];
  }
  /* If using the GSM 7bit alphabet extension table of 23.038... */
  else if ( GSM_EXT_TBL_ESC_CHAR == **gsm_in )
  {
    /* Advance to next character after escape character... */
    (*gsm_in)++;

    /* If that character is found in the extension table... */
    if ( NULL != ( tmp_chr_ptr = 
                     (char *)strchr( gsm7_to_pbm8_exttbl, **gsm_in ) ) )
    {
      /* Then output character at same offset in complementary array
         implementing extension table */
      *pbm_out = pbm8_to_gsm7_exttbl[tmp_chr_ptr - gsm7_to_pbm8_exttbl];
    }
    else
    {
      *pbm_out = (char)REPLACEMENT_CHAR;
    }
  }
  else
  {
    /* Replace inconvertible character with a space */
    *pbm_out = (char)REPLACEMENT_CHAR;
  }
}



/*===========================================================================

FUNCTION DSATUTIL_TIMER_CB

DESCRIPTION
  This function is called when timer expires.

DEPENDENCIES

RETURN VALUE
  None

SIDE EFFECTS
  None
  
===========================================================================*/
LOCAL void dsatutil_timer_cb(unsigned long timer_id)
{
  /* submit dstask a cmd */
  ds_cmd_type *cmd_ptr;

    /*-------------------------------------------------------------------------
    Send a DS_TIMER_EXPIRED_CMD to the DS task, and indicate which timer
    expired.
  -------------------------------------------------------------------------*/
  cmd_ptr = dsat_get_cmd_buf(TRUE);
  if (NULL != cmd_ptr)
  {
    cmd_ptr->hdr.cmd_id = DS_AT_TIMER_EXPIRED_CMD;
    cmd_ptr->cmd.dsat_timer_id = timer_id;
    ds_put_cmd( cmd_ptr );
  }
} /* dsatutil_timer_cb */


/*===========================================================================

FUNCTION dsatutil_strtok

DESCRIPTION
  This function is used to return the tokens in tok_str string passed

DEPENDENCIES

RETURN VALUE
  Returns an enum that describes the result of the execution.
  Possible values:
    DSAT_ERROR : if there was any problem in execution
    DSAT_OK : if it is a success.

SIDE EFFECTS
  None
  
===========================================================================*/
dsat_result_enum_type dsatutil_strtok
(
  const byte *str,
  uint32 str_len,
  byte delimiter,
  byte *tok_str
)
{
  uint32 i = 0;

  while(*str != delimiter)
  {
    if( i < str_len)
    {
      *(tok_str + i) = *str++;
      i++;
    }
    else
    {
      return DSAT_ERROR;
    }
  }
  return DSAT_OK;
} /* dsatutil_strtok */


/*===========================================================================

FUNCTION is_leap_year

DESCRIPTION
  This function is used to return whether the year passed is a leap year or not

DEPENDENCIES

RETURN VALUE
  Returns an enum that describes the result of the execution.
  Possible values:
    FALSE : if there was any problem in execution
    TRUE : if it is a success.

SIDE EFFECTS
  None
  
===========================================================================*/
LOCAL int is_leap_year(int year)
{
  int result;

  /* year is not divisible by 4 */
  if((year % 4) != 0 )
  {
    result = FALSE;
  }
  /* year is divisible by 400 */
  else if ((year % 400) == 0 )
  {
    result = TRUE;
  }
  /* year is divisible by 100 */
  else if ((year % 100) == 0 )
  {
    result = FALSE;
  }
  else
  {
    result = TRUE;
  }

  return result;
} /* is_leap_year */


/*===========================================================================

FUNCTION dsatutil_is_date_valid

DESCRIPTION
  This function is used to check whether the year/day/month 
  combination is correct or not

DEPENDENCIES

RETURN VALUE
  Returns an enum that describes the result of the execution.
  Possible values:
    FALSE : if there was any problem in execution
    TRUE : if it is a success.

SIDE EFFECTS
  None
  
===========================================================================*/
int dsatutil_is_date_valid(int year, int month, int day)
{
  int valid = TRUE;
  int month_length[13] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

   /* 29 days in February in a leap year (including year 2000) */
  if(is_leap_year(year) )
  {
    month_length[2] = 29;
  }

  if(month < 1 || month > 12)
  {
    valid = FALSE;
  }
  else if(day < 1 || day > month_length[month])
  {
    valid = FALSE;
  }

  return valid;
} /* dsatutil_is_date_valid */

/*===========================================================================

FUNCTION DSATUTIL_SWAPN_WORD_MSB_LSB

DESCRIPTION
  This function is used to swap the msb and lsb of the given word array
DEPENDENCIES
  None.
RETURN VALUE
  Returns an enum that describes the result of the execution.
  Possible values:
    FALSE : if there was any problem in execution
    TRUE : if it is a success.

SIDE EFFECTS
  None
  
===========================================================================*/
boolean dsatutil_swapn_word_msb_lsb
(
  const word * source_arr ,                       /* Pointer to Source array */
  word * dest_arr,                          /* Pointer to Destination array */
  dsat_num_item_type source_length,         /* No of words in source */
  dsat_num_item_type dest_length            /* No of words in destination */
)
{
  dsat_num_item_type index;
  if ( ( dest_length == 0 ) || ( source_length == 0 ) )
  {
    DS_AT_MSG0_HIGH("Invalid length in dsatutil_swapn_word_msb_lsb");
    return FALSE;
  }

  if( (source_arr == NULL) || (dest_arr == NULL) )
  {
    DS_AT_MSG0_HIGH("Invalid source or dest pointer");
    return FALSE;
  }
  
  /* Begining from the source add each word */
  /* by shifting it by 8 bits right and left.*/
  for(index = 0; index < MIN(source_length,dest_length) ;index++)
  {
    *dest_arr = (word)(( source_arr[index]>>8 ) | ( source_arr[index]<<8 ));
                  dest_arr++;
  }
  return TRUE;
  
  

}/* dsatutil_swap_msb_lsb_word_arr */

/*===========================================================================

FUNCTION DSATUTIL_REVERSEN_WORD

DESCRIPTION
  This function reverses the given word array by the specified number of words
DEPENDENCIES
  None.
RETURN VALUE
  Returns an enum that describes the result of the execution.
  Possible values:
    FALSE : if there was any problem in execution
    TRUE : if it is a success.

SIDE EFFECTS
  None
  
===========================================================================*/

boolean dsatutil_reversen_word
(
  word * wrd_arry,                        /* Word Array to be reversed */
  dsat_num_item_type len                 /* Number of words to be reversed */
)
{
  dsat_num_item_type index;
  word temp;

  if( (wrd_arry != NULL) && ( len > 0 ) )
  {
    /* Reverse the contents of the array */
    for(index = 0; index < (len/2) ;index++)
    {
      temp = wrd_arry[index];
      wrd_arry[index] = wrd_arry[(len - index) - 1];
      wrd_arry[(len - index) -1] = temp;
    }
    return TRUE;
  }
  else
  {
    DS_AT_MSG0_HIGH(" Invalid params in dsatutil_reverse_word_arr ");
    return FALSE;
  }
}/* dsatutil_reverse_word_arr */

/*===========================================================================

FUNCTION DSATUTIL_REVERSEN_BYTE

DESCRIPTION
  This function reverses the given byte array by the specified number of bytes.

DEPENDENCIES
  None.

RETURN VALUE
  Returns an enum that describes the result of the execution.
  Possible values:
    FALSE : if there was any problem in execution
    TRUE : if it is a success.

SIDE EFFECTS
  None
  
===========================================================================*/
boolean dsatutil_reversen_byte
(
  byte * byte_arry,                      /*  Byte Array to be reversed */
  dsat_num_item_type len                 /* Number of bytes to be reversed */
)
{
  dsat_num_item_type index;
  byte temp;

  if( (byte_arry != NULL) && ( len > 0 ) )
  {
    /* Reverse the contents of the array */
    for(index = 0; index < (len/2) ;index++)
    {
      temp = byte_arry[index];
      byte_arry[index] = byte_arry[(len - index) - 1];
      byte_arry[(len - index) -1] = temp;
    }
    return TRUE;
  }
  else
  {
    DS_AT_MSG0_HIGH(" Invalid params in dsatutil_reverse_byte_arr ");
    return FALSE;
  }
}/* dsatutil_reversen_byte */

#ifdef FEATURE_DSAT_ETSI_DATA

/*===========================================================================

FUNCTION DSATUTIL_CONVERT_IPADDR

DESCRIPTION
  This function converts between IP V4/V6 addresses and ds_umts_pdp_addr_types.
  In identifies the type of conversion to be done and passes the rest of 
  the work off to the convert tuple function. In string to number mode the 
  presence of a colon in string identifies an IPV6 conversion else IPV4 is 
  assumed. In number to string mode the setting of 
  ds_umts_pdp_addr_type.ip_vsn identifies conversion type.

DEPENDENCIES
  Buffer pointed to by buf_ptr should be at least MAX_IPADDR_STR_LEN bytes 
  in length to hold NULL terminated, maximum length IPV6 address for 
  INT_TO_STRING conversion.

RETURN VALUE
  returns an flag that describes the result of execution.
  possible values:
    DSAT_OK : if validation successful
    DSAT_ERROR : if error occurred

SIDE EFFECTS
  None
  
===========================================================================*/
dsat_result_enum_type dsatutil_convert_ipaddr
(
   tuple_convert_enum_type mode,             /* Mode of conversion      */
   ds_umts_pdp_addr_type* addr_ptr,          /* Number representation   */
   byte* buf_ptr                             /* NULL terminated string  */
)
{
  /* word aligned address */
  uint32 addr32[4];
  dsat_num_item_type   cgpiaf_add_format = 0;
  tuple_type_enum_type tuple_type = TUPLE_TYPE_MAX;

  memset( (void *)addr32, 0, (sizeof(uint32) * 4) );

  if ( INT_TO_STRING == mode )  
  {
    /* copy to local word aligned address */
    if ( DS_IP_V6 == addr_ptr->ip_vsn )
    {
      addr32[0] = addr_ptr->pdp_addr_ipv6.in6_u.u6_addr32[0];
      addr32[1] = addr_ptr->pdp_addr_ipv6.in6_u.u6_addr32[1];
      addr32[2] = addr_ptr->pdp_addr_ipv6.in6_u.u6_addr32[2];
      addr32[3] = addr_ptr->pdp_addr_ipv6.in6_u.u6_addr32[3];
    }
    else
    {
      addr32[0] = addr_ptr->pdp_addr_ipv4;
    }
  }

  /* For IPV6 conversion either mode... */
  if ( (STRING_TO_INT == mode && strchr((char *)buf_ptr, IPV6_DELIMITER)) ||
       (INT_TO_STRING == mode && DS_IP_V6 == addr_ptr->ip_vsn) )
  {
    /* PDP address parm may not be given but IPV6 delimiter has already 
       been found in string, so no need to test for NULL string as
       in else block below */
       
    /* Decides IPv6 address representation based on +CGPIAF setting*/
    tuple_type = TUPLE_TYPE_IPv6;
    if(INT_TO_STRING == mode)
    {
      cgpiaf_add_format = (dsat_num_item_type)dsatutil_get_val(
                                    DSATETSI_EXT_CGPIAF_IDX,0,0,NUM_TYPE);
      /* "1" Use IPv6-like colon-notation. IP address */
      if(1 == cgpiaf_add_format)
      {
         tuple_type = TUPLE_TYPE_IPv6;
      }
      else
      {
         tuple_type = TUPLE_TYPE_IPv6_OCTETS;
      }
    }
    if ( DSAT_OK != dsatutil_convert_tuple(
                                     mode,
                                     tuple_type,
                                     addr32,
                                     buf_ptr,
                                     MAX_IPADDR_STR_LEN+1,
                                     DSAT_CONVERT_IP) )
    {
      DS_AT_MSG0_ERROR("Failed IPV6 address conversion");
      return DSAT_ERROR;
    }

    if ( STRING_TO_INT == mode )
    {
      addr_ptr->ip_vsn = DS_IP_V6;
    }
  }
  else /* Assume IPV4 conversion */
  {
    if ( DSAT_OK != dsatutil_convert_tuple(mode,
                                           TUPLE_TYPE_IPv4,
                                           addr32,
                                           buf_ptr,
                                           MAX_IPADDR_STR_LEN+1,
                                           DSAT_CONVERT_IP) )
    {
      DS_AT_MSG0_ERROR("Failed IPV4 address conversion");
      return DSAT_ERROR;
    }

    if ( STRING_TO_INT == mode )
    {
      addr_ptr->ip_vsn = DS_IP_V4;
    }
  }

  if ( STRING_TO_INT == mode )
  {
    /* copy from local word aligned address */
    if ( DS_IP_V6 == addr_ptr->ip_vsn )
    {
      addr_ptr->pdp_addr_ipv6.type = DS_UMTS_PDP_IPV6_32;
      addr_ptr->pdp_addr_ipv6.in6_u.u6_addr32[0] = addr32[0];
      addr_ptr->pdp_addr_ipv6.in6_u.u6_addr32[1] = addr32[1];
      addr_ptr->pdp_addr_ipv6.in6_u.u6_addr32[2] = addr32[2];
      addr_ptr->pdp_addr_ipv6.in6_u.u6_addr32[3] = addr32[3];
    }
    else
    {
      addr_ptr->pdp_addr_ipv4 = addr32[0];
    }
  }

  return DSAT_OK;
} /* dsatutil_convert_ipaddr */

/*===========================================================================

FUNCTION DSATUTIL_CONVERT_ADDR_MASK

DESCRIPTION
  This function converts between an address mask string as used by +CGTFT and 
  ds_umts_address_mask_type. It determines the type of conversion needed and
  passes the work on to dsatutil_convert_tuple(). In the IPV6 mask conversion
  creates a dummy address representing the mask first.

DEPENDENCIES
  String buffer pointed to should be MAX_ADDR_SUBNET_STRING_LEN+1 bytes 
  in length to hold NULL terminated, maximum length IP address and mask for 
  INT_TO_STRING conversion.

RETURN VALUE
  returns an flag that describes the result of execution.
  possible values:
    DSAT_OK : if validation successful
    DSAT_ERROR : if error occurred

SIDE EFFECTS
  None
  
===========================================================================*/
dsat_result_enum_type dsatutil_convert_addr_mask
(
  tuple_convert_enum_type mode,             /* Mode of conversion      */
  ds_umts_address_mask_type *addr_mask_num, /* Number representations  */
  byte *addr_mask_str                       /* NULL terminated string  */
)
{
#define ADDR_MASK_SEP_IPV4  4
#ifdef FEATURE_DATA_PS_IPV6
#define DELIMITERS_PER_IPV6 31
#define ADDR_MASK_SEP_IPV6  16
#define NUM_BITS_LESS_THAN_8  3
#define MASK_BITS_LESS_THAN_8 0x07
#endif

  uint8 num_delimiters = 0;
  byte *curr_char = addr_mask_str;
  dsat_num_item_type   cgpiaf_add_format = 0;
  dsat_num_item_type   cgpiaf_subnet_format = 0;
  tuple_type_enum_type tuple_type        = TUPLE_TYPE_MAX;
  /* word aligned temporary address */
  typedef union addr_u
  {
    uint32 addr32[4];
#ifdef FEATURE_DATA_PS_IPV6
    uint8  addr8[16];
#endif
  } addr_u_type;

  addr_u_type addr;

  ASSERT( ( NULL != addr_mask_num ) && ( NULL != addr_mask_str ) );

  if ( STRING_TO_INT == mode )
  {
#ifdef FEATURE_DATA_PS_IPV6
    /* for string to number conversion count the number of '.' delimiters in
       address mask string to determine IP version */
    while ( NULL !=
            (curr_char = (byte *)strchr((char *)curr_char, IPV4_DELIMITER)) )
    {
      num_delimiters++;
      curr_char++;
    }
#endif /* FEATURE_DATA_PS_IPV6 */

    memset( addr.addr32, 0, (sizeof(uint32) * 4) );
  }
  else
  {
    /* for number to string conversion first copy address to 
       local word aligned structure */
    if ( DS_IP_V6 == addr_mask_num->address.ip_vsn )
    {
      addr.addr32[0] = addr_mask_num->address.pdp_addr_ipv6.in6_u.u6_addr32[0];
      addr.addr32[1] = addr_mask_num->address.pdp_addr_ipv6.in6_u.u6_addr32[1];
      addr.addr32[2] = addr_mask_num->address.pdp_addr_ipv6.in6_u.u6_addr32[2];
      addr.addr32[3] = addr_mask_num->address.pdp_addr_ipv6.in6_u.u6_addr32[3];
    }
    else
    {
      addr.addr32[0] = addr_mask_num->address.pdp_addr_ipv4 ;
    }
  }

#ifdef FEATURE_DATA_PS_IPV6
  /* For IPV6 address / mask conversion in either mode... */
  if ( (STRING_TO_INT == mode && DELIMITERS_PER_IPV6 == num_delimiters ) ||
       (INT_TO_STRING == mode && DS_IP_V6 == addr_mask_num->address.ip_vsn) )
  {
    /* First convert between address and dot separated 8 bit decimal values 
       or the reverse. */
    /* Decides IPv6 address representation based on +CGPIAF setting*/
    
     tuple_type = TUPLE_TYPE_IPv6_OCTETS;
    
     if(INT_TO_STRING == mode)
     {
       cgpiaf_add_format = (dsat_num_item_type)dsatutil_get_val(
                                     DSATETSI_EXT_CGPIAF_IDX,0,0,NUM_TYPE);
       /* "1" Use IPv6-like colon-notation. IP address */
       if(1 == cgpiaf_add_format)
       {
          tuple_type = TUPLE_TYPE_IPv6;
       }
       else
       {
          tuple_type = TUPLE_TYPE_IPv6_OCTETS;
       }
     }
    if ( DSAT_OK != dsatutil_convert_tuple(
                        mode,
                        tuple_type,
                        addr.addr32,
                        addr_mask_str,
                        MAX_ADDR_SUBNET_STRING_LEN+1,
                        DSAT_CONVERT_IP) )
    {
      DS_AT_MSG0_ERROR("Failed IPV6 address conversion");
      return DSAT_ERROR;
    }

    if ( STRING_TO_INT == mode )
    {
      /* for string to number conversion copy address from word aligned 
         address back to packed DSUMTS struct */
      addr_mask_num->address.pdp_addr_ipv6.type = DS_UMTS_PDP_IPV6_32;

      addr_mask_num->address.pdp_addr_ipv6.in6_u.u6_addr32[0] = addr.addr32[0];
      addr_mask_num->address.pdp_addr_ipv6.in6_u.u6_addr32[1] = addr.addr32[1];
      addr_mask_num->address.pdp_addr_ipv6.in6_u.u6_addr32[2] = addr.addr32[2];
      addr_mask_num->address.pdp_addr_ipv6.in6_u.u6_addr32[3] = addr.addr32[3];

    }

    /* Then convert Mask... */
    if ( INT_TO_STRING == mode )
    {
    /* for subnet mask to string conversion create a dummy address representing
       mask, The mask should have some number of contiguous MS bits set and the
       rest zeroed */
      /* starting at MS byte of array representing address */
      int index = sizeof(addr) - 1;
      uint8 num_bytes_set = 
        addr_mask_num->mask.prefix_len >> NUM_BITS_LESS_THAN_8;

      /* zero all bytes first, */
      memset( addr.addr32, 0, (sizeof(uint32) * 4) );

      /* then set whole bytes with all bits set, */
      while ( num_bytes_set && index >= 0 )
      {
        addr.addr8[index--] = 0xFF;
        num_bytes_set--;
      }

      /* then set the byte with only some of its upper bits set, if any */
      if ( index >= 0 )
      {
        addr.addr8[index] = (uint8)
          (0xFF << (8 - (addr_mask_num->mask.prefix_len & MASK_BITS_LESS_THAN_8)));
      }

      /* add '.' delimiter between address and mask parts */
      curr_char = addr_mask_str + strlen((char *)addr_mask_str);
      /* Decides IPv6 address representation based on +CGPIAF setting*/
      cgpiaf_add_format     = (dsat_num_item_type)dsatutil_get_val(
                                    DSATETSI_EXT_CGPIAF_IDX,0,0,NUM_TYPE);
      cgpiaf_subnet_format  = (dsat_num_item_type)dsatutil_get_val(
                                     DSATETSI_EXT_CGPIAF_IDX,0,1,NUM_TYPE);
      if(1 == cgpiaf_add_format && 1 == cgpiaf_subnet_format)
      {
        *curr_char++ = '/'; /* Subnet 1 ,Applying (forward slash) /CIDR */
      }else if(1 == cgpiaf_add_format && 0 == cgpiaf_subnet_format)
      {
        *curr_char++ = ' '; /* Subnet 0 ,Both IP Address and subnet mask are separated by a space.*/
      }
      else
      {
        *curr_char++ = '.'; /* Default setting when address format is 0 */
      }
      *curr_char = '\0';
    }
    else
    {
      /* for string to subnet mask number conversion... */
      num_delimiters = 0;
      curr_char = addr_mask_str;

      /* Leave current character pointing to 1st character of mask string */
      while ((num_delimiters < ADDR_MASK_SEP_IPV6) && 
             (NULL != (curr_char = (byte *)strchr((char *)curr_char, IPV4_DELIMITER))))
      {
        num_delimiters++;
        curr_char++;
      }
    }

    /* convert between mask and dot separated 8 bit decimal values 
       or the reverse conversion, */

    /* sanity check before passing buffer to dsatutil_convert_tuple */
    if (curr_char == NULL)
    {
      DS_AT_MSG0_ERROR("Invalid address mask string");
      return DSAT_ERROR;
    }

    if(INT_TO_STRING == mode)
    {
      /* Decides IPv6 address representation based on +CGPIAF setting*/
      cgpiaf_add_format     = (dsat_num_item_type)dsatutil_get_val(
                                    DSATETSI_EXT_CGPIAF_IDX,0,0,NUM_TYPE);
      cgpiaf_subnet_format  = (dsat_num_item_type)dsatutil_get_val(
                                     DSATETSI_EXT_CGPIAF_IDX,0,1,NUM_TYPE);
      if(1 == cgpiaf_add_format && 1 == cgpiaf_subnet_format)
      {
        /* Convert to integer */
         dsatutil_itoa ((uint32)addr_mask_num->mask.prefix_len,(byte *) curr_char, 10);
      }
      else
      {
        if(1 == cgpiaf_add_format)
        {
          tuple_type = TUPLE_TYPE_IPv6; 
        }
        else
        {
          tuple_type = TUPLE_TYPE_IPv6_OCTETS; 
        }
  
        if ( DSAT_OK != dsatutil_convert_tuple(
                             mode,
                             tuple_type,
                             addr.addr32,
                             curr_char,
                             (uint8)((MAX_ADDR_SUBNET_STRING_LEN+1) - 
                             (curr_char-addr_mask_str)),
                             DSAT_CONVERT_MASK) )
        {
          DS_AT_MSG0_ERROR("Failed IPV6 address conversion");
          return DSAT_ERROR;
        }
      }
    }
    else
    {
      if ( DSAT_OK != dsatutil_convert_tuple(
                    mode,
                    TUPLE_TYPE_IPv6_OCTETS,
                    addr.addr32,
                    curr_char,
                    (uint8)((MAX_ADDR_SUBNET_STRING_LEN+1) - 
                      (curr_char-addr_mask_str)),
                      DSAT_CONVERT_MASK) )
      {
        DS_AT_MSG0_ERROR("Failed IPV6 address conversion");
        return DSAT_ERROR;
      }
    }
    if ( STRING_TO_INT == mode )
    {
      /* for string to mask number conversion count number of bits set in 
         temp address: start from MS byte of array representing address... */
      int index = sizeof(addr) - 1;

      addr_mask_num->address.ip_vsn = DS_IP_V6;

      while ( index >= 0 )
      {
        if ( 0xFF == addr.addr8[index] )
        {
          /* count number of whole bytes set, */
          addr_mask_num->mask.prefix_len += 8;
        }
        else if ( 0 != addr.addr8[index] )
        {
          /* and number of bits in the byte partially set */
          uint8 bit_mask = 0x80;

          while ( addr.addr8[index] & bit_mask )
          {
            addr_mask_num->mask.prefix_len++;
            bit_mask = bit_mask >> 1;
          }

          break;
        }
        else
        {
          break;
        }

        index--;
      }
    }
  }
  else 
#endif /* FEATURE_DATA_PS_IPV6 */
  {
    byte *mask_start = NULL;

    /* Convert IPV4 address to string or vv. */
    if ( DSAT_OK != dsatutil_convert_tuple(
                               mode,
                               TUPLE_TYPE_IPv4,
                               addr.addr32,
                               addr_mask_str,
                               MAX_IPADDR_STR_LEN,
                               DSAT_CONVERT_IP) )
    {
      DS_AT_MSG0_ERROR("Failed IPV4 address conversion");
      return DSAT_ERROR;
    }

    if ( INT_TO_STRING == mode )
    {
      /* add '.' delimiter between address and mask parts */
      curr_char = addr_mask_str + strlen((char *)addr_mask_str);
      *curr_char++ = '.';
      *curr_char = '\0';
      mask_start = curr_char;

    /* copy mask from DSUMTS packed struct to local word aligned mask */
      addr.addr32[0] = addr_mask_num->mask.mask;
    }
    else /* STRING_TO_INT */
    {
      num_delimiters = 0;
      curr_char = addr_mask_str;

    /* copy local word aligned address back to DSUMTS packed struct */
      addr_mask_num->address.ip_vsn = DS_IP_V4;

      addr_mask_num->address.pdp_addr_ipv4 = addr.addr32[0];

      /* Leave current character pointing to 1st character of mask string */
      while ((num_delimiters < ADDR_MASK_SEP_IPV4) && 
             (NULL != (curr_char = (byte *)strchr((char *)curr_char,IPV4_DELIMITER))))
      {
        num_delimiters++;
        curr_char++; 
      }

      mask_start = curr_char;
    }

    /* sanity check before passing buffer to dsatutil_convert_tuple */
    if (mask_start == NULL)
    {
      DS_AT_MSG0_ERROR("Invalid address mask string");
      return DSAT_ERROR;
    }

    /* Convert mask */
    if ( DSAT_OK != dsatutil_convert_tuple(
                          mode,
                          TUPLE_TYPE_IPv4,
                          addr.addr32,
                          mask_start,
                          (uint8)(MAX_IPADDR_STR_LEN - strlen((char *)addr_mask_str)),
                          DSAT_CONVERT_MASK) )
    {
      DS_AT_MSG0_ERROR("Failed IPV4 address conversion");
      return DSAT_ERROR;
    }

    if ( STRING_TO_INT == mode )
    {
      /* copy from local word aligned mask back to packed DSUMTS struct */
      addr_mask_num->mask.mask = addr.addr32[0];
    }
  }

  return DSAT_OK;
} /* dsatutil_convert_addr_mask */

#endif /* FEATURE_DSAT_ETSI_DATA */



#ifdef FEATURE_DSAT_GOBI_MAINLINE
/*===========================================================================

FUNCTION DSATUTIL_GET_UQCN_VERSION

DESCRIPTION
  This command reads the appropriate NV item containing the UQCN version 
  information for UMTS / CDMA.

DEPENDENCIES
  NONE

RETURN VALUE
  uint32 - UQCN Version

SIDE EFFECTS
  None
  
===========================================================================*/
uint32 dsatutil_get_uqcn_version()
{
  static uint32 uqcn_version = 0;
  nv_item_type  ds_nv_item;                /* actual data item     */

  if (!uqcn_version) 
  {
    memset(&ds_nv_item, 0, sizeof(ds_nv_item));
#if (defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900))
    /* Fetch NV item #4995 */
    if (NV_DONE_S == dsi_get_nv_item(NV_CUST_UQCN_C2K_VERSION_I, &ds_nv_item))
    {
      uqcn_version = ds_nv_item.cust_uqcn_c2k_version;
    }
#elif (defined(FEATURE_WCDMA) || defined(FEATURE_GSM))
    /* Fetch NV item #4996 */
    if (NV_DONE_S == dsi_get_nv_item(NV_CUST_UQCN_UMTS_VERSION_I, &ds_nv_item))
    {
      uqcn_version = ds_nv_item.cust_uqcn_umts_version;
    }
#endif
  }

  return uqcn_version;
} /* dsatutil_get_uqcn_version */
#endif /* FEATURE_DSAT_GOBI_MAINLINE */

/*===========================================================================
FUNCTION  DSATUTIL_ALLOC_MEMORY

DESCRIPTION
  Allocates a block of memory from Modem Heap memory

DEPENDENCIES
  FEATURE_DATA_MODEM_HEAP

RETURNS
  The Data Pointer to the allocated Memory.

SIDE EFFECTS
  None

===========================================================================*/
void *dsatutil_alloc_memory
(
  uint32      data_size,
  const char *filename,
  uint32      line,
  boolean     graceful
)
{
  void * data_ptr;

  data_ptr = modem_mem_alloc(data_size,MODEM_MEM_CLIENT_DATA);
  if (NULL == data_ptr)
  {
    DS_AT_MSG_SPRINTF_3_ERROR("Failed to allocate heap memory block of size %d "
			                        "(%s, %d)", data_size, filename, line);

    DSAT_GRACEFUL_ERR_FATAL(graceful);
  }
  else
  {
    DS_AT_MSG_SPRINTF_4_MED("Allocated heap memory block of size %d with pointer "
                            "0x%x (%s, %d)", data_size, data_ptr, filename, line);

    memset(data_ptr, 0x00, data_size);
  }

  return data_ptr;
}

/*===========================================================================
FUNCTION  DSATUTIL_FREE_MEMORY

DESCRIPTION
  Frees a block of memory from Modem Heap memory

DEPENDENCIES
  FEATURE_DATA_MODEM_HEAP

RETURNS
  None

SIDE EFFECTS
  None

===========================================================================*/

void dsatutil_free_memory
(
  void *data_ptr
)
{
  DS_AT_MSG1_MED("modem_mem_free 0x%x:",(unsigned int)data_ptr);
  if (NULL != data_ptr)
  {
    modem_mem_free(data_ptr,MODEM_MEM_CLIENT_DATA);
    data_ptr = NULL;
  }
} /* dsatutil_free_memory() */

/*===========================================================================

FUNCTION DSATUTIL_GET_CMD_BUF

DESCRIPTION
  This function is a wrapper for ds_get_cmd_buf() API. It calls an error F3 in case of
  graceful handing and ERR_FATAL otherwise. The caller of this function must check
  for a NULL return value in case of graceful handing.

DEPENDENCIES
  None

RETURN VALUE
  Returns a pointer to a command buffer if avaiable or NULL in case of graceful handing
  if no command buffers are available.

SIDE EFFECTS
  None

===========================================================================*/

ds_cmd_type *dsatutil_get_cmd_buf
(
  const char *filename,
  uint32      line,
  boolean     graceful
)
{
  ds_cmd_type *cmd_buf;

  cmd_buf = ds_get_cmd_buf();
  if (NULL == cmd_buf)
  {
    DS_AT_MSG_SPRINTF_2_ERROR("Failed to get DS command buffer (%s, %d)",
                              filename, line);

    DSAT_GRACEFUL_ERR_FATAL(graceful);
  }
  else
  {
    DS_AT_MSG_SPRINTF_3_MED("Allocated DS command buffer with pointer 0x%x (%s, %d)",
                            cmd_buf, filename, line);
  }

  return cmd_buf;
} /* dsatutil_get_cmd_buf() */

/*===========================================================================
FUNCTION  DSATUTIL_MEMSCPY

DESCRIPTION
  
 This function will validate and copy data from src pointer to dest pointer.
  
DEPENDENCIES
  None

RETURNS
  None

SIDE EFFECTS
  None

===========================================================================*/

size_t dsatutil_memscpy
(
  void        *dst,
  size_t      dst_size,
  const void  *src,
  size_t      src_size

)
{
  if(dst_size == 0 || src_size == 0 || dst == NULL || src == NULL)
  {
    return 0;
  }
  else
  {
    return memscpy( dst,dst_size, src,src_size);
  }
} /* dsatutil_free_memory() */

/*===========================================================================
FUNCTION  DSATUTIL_GET_BASIC_VALS

DESCRIPTION
  This function will take basic table command ID and will return val pointer corresponding .
  to the command ID.
DEPENDENCIES
  None

RETURNS
  val pointer

SIDE EFFECTS
  None

===========================================================================*/

void* dsatutil_get_basic_vals
(
  dsat_num_item_type       cmd_id,
  dsat_memory_subs_type    subs
)
{
  void *resp = NULL;
  if(NULL == dsat_basic_vals)
  {
/*Allocate the memory and proceed*/
   dsatutil_default_constructor( DSAT_TABLE_BASIC,subs);
/*Initialize Val pointers from dlfm*/
   dsatcmdp_init_parse_table( dsat_basic_table, 
                            (dsat_num_item_type)dsat_basic_table_size);
  }
  switch(cmd_id)
  {
    case DSAT_BASIC_N_C_IDX:
      resp =  &dsat_basic_vals->dsat_amp_c_val;
      break;
    case DSAT_BASIC_N_D_IDX:
      resp = &dsat_basic_vals->dsat_amp_d_val;
      break;
    case DSAT_BASIC_N_E_IDX:
      resp = &dsat_basic_vals->dsat_amp_e_val;
      break;
   case DSAT_BASIC_N_F_IDX:
     resp = &dsat_basic_vals->dsat_amp_f_val;
     break;
   case DSAT_BASIC_N_S_IDX:
     resp = &dsat_basic_vals->dsat_amp_s_val;
     break;
   case DSAT_BASIC_N_W_IDX:
     resp = &dsat_basic_vals->dsat_amp_w_val;
     break;
   case DSAT_BASIC_E_IDX:
#ifdef FEATURE_ATCOP_ECHO_CTL_MULTI_PORTS
     resp = &dsat_basic_vals->dsat_e_val_multi_port[0];
#else
     resp = &dsat_basic_vals->dsat_e_val;
#endif /*FEATURE_ATCOP_ECHO_CTL_MULTI_PORTS*/
     break;
   case DSAT_BASIC_L_IDX:
     resp = &dsat_basic_vals->dsat_l_val;
     break;
   case DSAT_BASIC_M_IDX:
     resp = &dsat_basic_vals->dsat_m_val;
     break;
   case DSAT_BASIC_Q_IDX :
     resp = &dsat_basic_vals->dsat_q_val;
     break;
   case DSAT_BASIC_V_IDX:
     resp = &dsat_basic_vals->dsat_v_val;
     break;
   case DSAT_BASIC_X_IDX:
     resp =  &dsat_basic_vals->dsat_x_val;
     break;
   case DSAT_BASIC_Z_IDX:
     resp = &dsat_basic_vals->dsat_z_val;
     break;
   case DSAT_BASIC_DS_Q_IDX:
     resp = &dsat_basic_vals->dsat_slash_q_val;
     break;
   case DSAT_BASIC_DS_S_IDX:
     resp = &dsat_basic_vals->dsat_slash_s_val;
     break;
   case DSAT_BASIC_DS_V_IDX :
     resp = &dsat_basic_vals->dsat_slash_v_val;
     break;
   default:
    resp = NULL;
    break;
 }
 return resp;
}/*dsatutil_get_basic_vals*/
/*===========================================================================
FUNCTION  DSATUTIL_GET_BASIC_ACTION_VALS

DESCRIPTION
  This function will take basic action table command ID and will return val pointer corresponding .
  to the command ID.
DEPENDENCIES
  None

RETURNS
  val pointer

SIDE EFFECTS
  None

===========================================================================*/

void* dsatutil_get_basic_action_vals
(
  dsat_num_item_type       cmd_id,
  dsat_memory_subs_type    subs
)
{
  void *resp = NULL;
  if(NULL == dsat_basic_act_vals)
  {
/*Allocate the memory and proceed*/
   dsatutil_default_constructor(  DSAT_TABLE_BASIC_ACTION,subs);
/*Initialize Val pointers from dlfm*/
   dsatcmdp_init_parse_table( dsat_basic_action_table, 
                            (dsat_num_item_type)dsat_basic_action_table_size);
  }
  if(cmd_id == DSAT_BASIC_ACT_O_IDX)
  {
    resp = &dsat_basic_act_vals->dsat_o_val;
  }
  return resp;
}/*dsatutil_get_basic_action_vals*/

/*===========================================================================
FUNCTION  DSATUTIL_GET_BASIC_SREG_VALS

DESCRIPTION
  This function will take basic sreg table command ID and will return val pointer corresponding .
  to the command ID.
DEPENDENCIES
  None

RETURNS
  val pointer

SIDE EFFECTS
  None

===========================================================================*/

void* dsatutil_get_basic_sreg_vals
(
  dsat_num_item_type       cmd_id,
  dsat_memory_subs_type    subs
)
{
  void *resp = NULL;
  if(NULL == dsat_basic_sreg_vals)
  {
/*Allocate the memory and proceed*/
   dsatutil_default_constructor(  DSAT_TABLE_SREG,subs);
/*Initialize Val pointers from dlfm*/
   dsatcmdp_init_parse_table( dsat_sreg_table, 
                            (dsat_num_item_type)dsat_sreg_table_size);
  }
  switch(cmd_id)
  {
    case DSAT_SREG_S0_IDX:
      resp =  &dsat_basic_sreg_vals->dsat_s0_val;
      break;
    case DSAT_SREG_S2_IDX:
      resp = &dsat_basic_sreg_vals->dsat_s2_val;
      break;
    case DSAT_SREG_S3_IDX:
      resp = &dsat_basic_sreg_vals->dsat_s3_val;
      break;
   case DSAT_SREG_S4_IDX:
     resp = &dsat_basic_sreg_vals->dsat_s4_val;
     break;
   case DSAT_SREG_S5_IDX:
     resp = &dsat_basic_sreg_vals->dsat_s5_val;
     break;
   case DSAT_SREG_S6_IDX:
     resp = &dsat_basic_sreg_vals->dsat_s6_val;
     break;
   case DSAT_SREG_S7_IDX:
     resp = &dsat_basic_sreg_vals->dsat_s7_val;
     break;
   case DSAT_SREG_S8_IDX:
     resp = &dsat_basic_sreg_vals->dsat_s8_val;
     break;
   case DSAT_SREG_S9_IDX:
     resp = &dsat_basic_sreg_vals->dsat_s9_val;
     break;
   case DSAT_SREG_S10_IDX:
     resp = &dsat_basic_sreg_vals->dsat_s10_val;
     break;
   case DSAT_SREG_S11_IDX:
     resp = &dsat_basic_sreg_vals->dsat_s11_val;
     break;
   case DSAT_SREG_S30_IDX:
     resp =  &dsat_basic_sreg_vals->dsat_s30_val;
     break;
   case DSAT_SREG_S103_IDX:
     resp = &dsat_basic_sreg_vals->dsat_s103_val;
     break;
   case DSAT_SREG_S104_IDX:
     resp = &dsat_basic_sreg_vals->dsat_s104_val;
     break;
   default:
    resp = NULL;
    break;
 }
 return resp;

}/*dsatutil_get_basic_sreg_vals*/

/*===========================================================================
FUNCTION  DSATUTIL_GET_BASIC_EXT_VALS

DESCRIPTION
  This function will take basic extended table command ID and will return val pointer corresponding .
  to the command ID.
DEPENDENCIES
  None

RETURNS
  val pointer

SIDE EFFECTS
  None

===========================================================================*/

void* dsatutil_get_basic_ext_vals
(
  dsat_num_item_type       cmd_id,
  dsat_memory_subs_type    subs
)
{
  void *resp = NULL;
  if(NULL == dsat_basic_ext_vals)
  {
/*Allocate the memory and proceed*/
   dsatutil_default_constructor(  DSAT_TABLE_EXT,subs);
/*Initialize Val pointers from dlfm*/
   dsatcmdp_init_parse_table( dsat_ext_table, 
                            (dsat_num_item_type)dsat_ext_table_size);
  }
  switch(cmd_id)
  {
    case DSAT_EXT_FCLASS_IDX:
      resp =  &dsat_basic_ext_vals->dsat_fclass_val;
      break;
    case DSAT_EXT_ICF_IDX:
      resp = &dsat_basic_ext_vals->dsat_icf_val[0];
      break;
    case DSAT_EXT_IFC_IDX:
      resp = &dsat_basic_ext_vals->dsat_ifc_val[0];
      break;
   case DSAT_EXT_IPR_IDX:
     resp = &dsat_basic_ext_vals->dsat_ipr_val;
     break;
   case DSAT_EXT_DR_IDX:
     resp = &dsat_basic_ext_vals->dsat_dr_val;
     break;
   case DSAT_EXT_DS_IDX:
     resp = &dsat_basic_ext_vals->dsat_ds_val[0];
     break;
   case DSAT_EXT_CMEE_IDX:
     resp = &dsat_basic_ext_vals->dsat_cmee_val;
     break;
#if defined(FEATURE_DATA_GCSD) || defined(FEATURE_DATA_WCDMA)
   case DSAT_EXT_WS46_IDX:
     resp = &dsat_basic_ext_vals->dsat_ws46_val;
     break;
#endif /* defined(FEATURE_DATA_GCSD) || defined(FEATURE_DATA_WCDMA)*/
#ifdef FEATURE_DSAT_TEST_32ARG
   case DSAT_EXT_TST32_IDX:
     resp = &dsat_basic_ext_vals->dsat_tst32_val[0];
     break;
#endif /*FEATURE_DSAT_TEST_32ARG*/
   case DSAT_EXT_CFUN_IDX:
     resp = &dsat_basic_ext_vals->dsat_cfun_val[0];
     break;

   default:
    resp = NULL;
    break;
 }
 return resp;

}/*dsatutil_get_basic_ext_vals*/

/*===========================================================================
FUNCTION  DSATUTIL_GET_BASIC_VEND_VALS

DESCRIPTION
  This function will take basic vender table command ID and will return val pointer corresponding .
  to the command ID.
DEPENDENCIES
  None

RETURNS
  val pointer

SIDE EFFECTS
  None

===========================================================================*/

void* dsatutil_get_basic_vend_vals
(
  dsat_num_item_type       cmd_id,
  dsat_memory_subs_type    subs
)
{
  void *resp = NULL;
  if(NULL == dsat_basic_vend_ss_vals)
  {
    /*Allocate the memory and proceed*/
    dsatutil_default_constructor(  DSAT_TABLE_VEND_EXT_SS, subs);
    dsatutil_default_constructor(  DSAT_TABLE_VEND_EXT_MS, DSAT_MS_MAX_SUBS);
    /*Initialize Val pointers from dlfm*/
    dsatcmdp_init_parse_table( dsat_vendor_table, 
                            (dsat_num_item_type)dsat_vendor_table_size);
  }
  if( subs >= DSAT_MS_MAX_SUBS)
  {
    return NULL;
  }
  if( NULL == dsat_basic_vend_ms_vals[subs])
  {
    dsatutil_default_constructor(  DSAT_TABLE_VEND_EXT_MS, subs);
  }
  switch(cmd_id)
  {
#ifdef FEATURE_DSAT_ETSI_MODE
    case DSAT_VENDOR_QCSIMSTAT_IDX:
      resp =  &dsat_basic_vend_ss_vals->dsat_qcsimstat_val;
      break;
#if defined(FEATURE_ETSI_PBM) || defined(FEATURE_DSAT_CDMA_PBM)
    case DSAT_VENDOR_QCPBMPREF_IDX:
      resp = &dsat_basic_vend_ss_vals->dsat_qcpbmpref_val;
      break;
#endif /* defined(FEATURE_ETSI_PBM) || defined(FEATURE_DSAT_CDMA_PBM)*/
#ifdef FEATURE_DSAT_DEV_CMDS
    case DSAT_VENDOR_CREG_IDX:
      resp = &dsat_basic_vend_ms_vals[subs]->dsat_qccreg_val;
      break;
#endif/*FEATURE_DSAT_DEV_CMDS*/
#ifdef FEATURE_ETSI_SMS
   case DSAT_VENDOR_QCCNMI_IDX:
     resp = &dsat_basic_etsi_cmn_ms_vals[subs]->dsat_cnmi_val[0];
     break;
#endif /*FEATURE_ETSI_SMS*/
#ifdef FEATURE_DSAT_DEV_CMDS
   case DSAT_VENDOR_CNTI_IDX:
     resp = &dsat_basic_vend_ms_vals[subs]->dsat_qccnti_val;
     break;
#endif /*FEATURE_DSAT_DEV_CMDS*/
#endif /*FEATURE_DSAT_ETSI_MODE*/
   case DSAT_VENDOR_QCDMR_IDX:
     resp = &dsat_basic_vend_ss_vals->dsat_qcdmr_val;
     break;
   case DSAT_VENDOR_QCDNSP_IDX:
     resp = &dsat_basic_vend_ss_vals->dsat_qcdnspri_val[0];
     break;
   case DSAT_VENDOR_QCDNSS_IDX:
     resp = &dsat_basic_vend_ss_vals->dsat_qcdnspri_val[0];
     break;
   case DSAT_VENDOR_QCTER_IDX:
     resp = &dsat_basic_vend_ss_vals->dsat_qcter_val;
     break;
   case DSAT_VENDOR_QCSIMAPP_IDX:
     resp = &dsat_basic_vend_ss_vals->dsat_qcsimapp_val;
     break;
   case DSAT_VENDOR_QCPDPP_IDX:
     resp = &dsat_basic_vend_ss_vals->dsat_qcpdpp_val[0];
     break;
   case DSAT_VENDOR_QCPDPLT_IDX:
     resp = &dsat_basic_vend_ss_vals->dsat_qcpdplt_val;
     break;
#ifdef FEATURE_DATA_TE_MT_PDP
   case DSAT_VENDOR_QCGANSM_IDX:
     resp = &dsat_basic_vend_ss_vals->dsat_qcgansm_val;
     break;
#endif /*FEATURE_DATA_TE_MT_PDP*/
#ifdef FEATURE_DATA_UCSD_SCUDIF_TEST
   case DSAT_VENDOR_QCSCFTEST_IDX:
     resp = &dsat_basic_vend_ss_vals->dsat_qcscftest_val[0];
     break;
#endif /*  FEATURE_DATA_UCSD_SCUDIF_TEST */
#ifdef FEATURE_TTY
   case DSAT_VENDOR_QCCTM_IDX:
     resp = &dsat_basic_vend_ss_vals->dsat_qcctm_val;
     break;
#endif/* FEATURE_TTY */
#ifdef FEATURE_HDR
  case DSAT_VENDOR_PREFMODE_IDX:
     resp = &dsat_basic_vend_ss_vals->dsat_prefmode_val;
     break;
#endif /* FEATURE_HDR*/
#ifdef FEATURE_DSAT_ETSI_MODE 
  case DSAT_VENDOR_DSCI_IDX:
     resp = &dsat_basic_vend_ss_vals->dsat_dsci_val;
     break;
#endif /*FEATURE_DSAT_ETSI_MODE*/
#ifdef FEATURE_ECALL_APP 
   case DSAT_VENDOR_ECALL_IDX:
     resp = &dsat_basic_vend_ss_vals->dsat_ecall_val[0];
     break;
#endif /*FEATURE_ECALL_APP*/
#ifdef FEATURE_DSAT_ETSI_DATA
   case DSAT_VENDOR_QCDEFPROF_IDX:
     resp = &dsat_basic_vend_ss_vals->dsat_qcdefprof_val[0];
     break;
#endif /*FEATURE_DSAT_ETSI_DATA */
#ifdef FEATURE_DSAT_LTE
   case DSAT_VENDOR_QCDRX_IDX:
     resp = &dsat_basic_vend_ss_vals->dsat_qcdrx_val;
     break;
#endif/* FEATURE_DSAT_LTE*/	 
   case DSAT_VENDOR_MODE_IDX:
     resp = &dsat_basic_vend_ss_vals->dsat_mode_val;
     break;
     case DSAT_VENDOR_QCRCIND_IDX:
     resp = &dsat_basic_vend_ss_vals->dsat_qcrcind_val;
     break;
   case DSAT_VENDOR_QCNSP_IDX:
     resp = &dsat_basic_vend_ss_vals->dsat_qcnsp_val[0];
     break;
   default:
    resp = NULL;
    break;
 }
 return resp;

}/*dsatutil_get_basic_vend_vals*/


/*===========================================================================
FUNCTION  DSATUTIL_GET_ETSI_EXT_ACT_VALS

DESCRIPTION
  This function will take etsi ext action table command ID and will return val pointer corresponding .
  to the command ID.
DEPENDENCIES
  None

RETURNS
  val pointer

SIDE EFFECTS
  None

===========================================================================*/

void* dsatutil_get_etsi_ext_act_vals
(
  dsat_num_item_type       cmd_id,
  dsat_memory_subs_type    subs
)
{
  void *resp = NULL;
  if(NULL == dsat_etsi_ext_act_ss_vals)
  {
 /*Allocate the memory and proceed*/
    dsatutil_default_constructor(  DSAT_TABLE_ETSI_EXT_ACT_SS, subs);
    dsatutil_default_constructor(  DSAT_TABLE_ETSI_EXT_ACT_MS, DSAT_MS_MAX_SUBS);
 /*Initialize Val pointers from dlfm*/
    dsatcmdp_init_parse_table( dsatetsi_ext_action_table, 
                             (dsat_num_item_type)dsatetsi_ext_action_table_size);
   }
   if( subs >= DSAT_MS_MAX_SUBS)
   {
     return NULL;
   }
   if( NULL == dsat_etsi_ext_act_ms_vals[subs])
   {
     dsatutil_default_constructor(  DSAT_TABLE_ETSI_EXT_ACT_MS, subs);
   }
  
  switch(cmd_id)
  {
#ifdef FEATURE_DATA_ETSI_PIN 
    case DSATETSI_EXT_ACT_CPIN_ETSI_IDX:
      resp =  &dsat_etsi_ext_act_ms_vals[subs]->dsat_cpin_val[0];
      break;
#endif /*FEATURE_DATA_ETSI_PIN*/
    case DSATETSI_EXT_ACT_CMEC_ETSI_IDX:
      resp = &dsat_etsi_ext_act_ss_vals->dsat_cmec_val[0];
      break;
    case DSATETSI_EXT_ACT_CIND_ETSI_IDX:
      resp = &dsat_etsi_ext_act_ms_vals[subs]->dsat_cind_val[0];
      break;

   case DSATETSI_EXT_ACT_CMER_ETSI_IDX:
     resp = &dsat_etsi_ext_act_ss_vals->dsat_cmer_val[0];
     break;
#ifdef FEATURE_DSAT_ETSI_DATA
   case DSATETSI_EXT_ACT_CGATT_ETSI_IDX:
     resp = &dsat_etsi_ext_act_ms_vals[subs]->dsat_cgatt_val;
     break;
   case DSATETSI_EXT_ACT_CGACT_ETSI_IDX:
     resp = &dsat_etsi_ext_act_ss_vals->dsat_cgact_val;
     break;
#endif /*#FEATURE_DSAT_ETSI_DATA*/
#ifdef FEATURE_ETSI_PBM
   case DSATETSI_EXT_ACT_CPBS_ETSI_IDX:
     resp = &dsat_etsi_ext_act_ss_vals->dsat_cpbs_val;
     break;
#endif/* FEATURE_ETSI_PBM*/

#ifdef FEATURE_ETSI_SMS
   case DSATETSI_EXT_ACT_CPMS_ETSI_IDX:
     resp = &dsat_etsi_ext_act_ms_vals[subs]->dsat_cpms_val[0];
     break;
     /* CNMI belongs into CTAB and ETSICTAB,Both have same variable and it is already allocated 
        in vender Structure */
   case DSATETSI_EXT_ACT_CNMI_ETSI_IDX:
     if(dsat_basic_etsi_cmn_ms_vals[subs] != NULL)
     {
       resp =  &dsat_basic_etsi_cmn_ms_vals[subs]->dsat_cnmi_val[0];
     }
     break;
   case DSATETSI_EXT_ACT_CMMS_ETSI_IDX:
     resp = &dsat_etsi_ext_act_ms_vals[subs]->dsat_cmms_val;
     break;
#endif /* FEATURE_ETSI_SMS*/

#ifdef FEATURE_DATA_GCSD_FAX
   case DSATETSI_EXT_ACT_FTS_ETSI_IDX:
     resp = &dsat_etsi_ext_act_ss_vals->dsat_fts_val;
     break;

   case DSATETSI_EXT_ACT_FRS_ETSI_IDX:
     resp = &dsat_etsi_ext_act_ss_vals->dsat_frs_val;
     break;


   case DSATETSI_EXT_ACT_FTH_ETSI_IDX:
     resp = &dsat_etsi_ext_act_ss_vals->dsat_fth_val;
     break;
     
   case DSATETSI_EXT_ACT_FRH_ETSI_IDX:
     resp = &dsat_etsi_ext_act_ss_vals->dsat_frh_val;
     break;

   case DSATETSI_EXT_ACT_FTM_ETSI_IDX:
     resp = &dsat_etsi_ext_act_ss_vals->dsat_ftm_val;
     break;

   case DSATETSI_EXT_ACT_FRM_ETSI_IDX:
     resp = &dsat_etsi_ext_act_ss_vals->dsat_frm_val;
     break;
#endif/*FEATURE_DATA_GCSD_FAX*/
     
   case DSATETSI_EXT_ACT_CCUG_ETSI_IDX:
     resp = &dsat_etsi_ext_act_ss_vals->dsat_ccug_val[0];
     break;

   case DSATETSI_EXT_ACT_COPS_ETSI_IDX:
     resp = &dsat_etsi_ext_act_ms_vals[subs]->dsat_cops_val[0];
     break;

   case DSATETSI_EXT_ACT_CUSD_ETSI_IDX:
     resp = &dsat_etsi_ext_act_ss_vals->dsat_cusd_val[0];
     break;
   case DSATETSI_EXT_ACT_CAOC_ETSI_IDX:
     resp = &dsat_etsi_ext_act_ss_vals->dsat_caoc_val;
     break;

   case DSATETSI_EXT_ACT_CCWA_ETSI_IDX:
     resp = &dsat_etsi_ext_act_ms_vals[subs]->dsat_ccwa_val;
     break;
#ifdef FEATURE_MMGSDI
   case DSATETSI_EXT_ACT_CPOL_ETSI_IDX:
     resp = &dsat_etsi_ext_act_ms_vals[subs]->dsat_cpol_val[0];
     break;
#endif /* FEATURE_MMGSDI*/
  case DSATETSI_EXT_ACT_CPLS_ETSI_IDX:
     resp = &dsat_etsi_ext_act_ms_vals[subs]->dsat_cpls_val;
     break;

  case DSATETSI_EXT_ACT_CTZR_ETSI_IDX:
     resp = &dsat_etsi_ext_act_ss_vals->dsat_ctzr_val;
     break;

   case DSATETSI_EXT_ACT_CTZU_ETSI_IDX:
     resp =  &dsat_etsi_ext_act_ss_vals->dsat_ctzu_val;
     break;

   case DSATETSI_EXT_ACT_CLIP_ETSI_IDX:
     resp =  &dsat_etsi_ext_act_ss_vals->dsat_clip_val;
     break;

   case DSATETSI_EXT_ACT_COLP_ETSI_IDX:
     resp =  &dsat_etsi_ext_act_ss_vals->dsat_colp_val;
     break;

   case DSATETSI_EXT_ACT_CDIP_ETSI_IDX:
     resp =  &dsat_etsi_ext_act_ss_vals->dsat_cdip_val;
     break;

   case DSATETSI_EXT_ACT_CLIR_ETSI_IDX:
     resp =  &dsat_etsi_ext_act_ss_vals->dsat_clir_val;
     break;

#ifdef FEATURE_DATA_TE_MT_PDP
   case DSATETSI_EXT_ACT_CGANS_ETSI_IDX:
     resp =  &dsat_etsi_ext_act_ss_vals->dsat_cgans_val[0];
     break;
#endif /* FEATURE_DATA_TE_MT_PDP*/
#ifdef FEATURE_WCDMA_L1_HS_CQI_STAT
   case DSATETSI_EXT_ACT_CQI_ETSI_IDX:
     resp =  &dsat_etsi_ext_act_ss_vals->dsat_cqi_val;
     break;
#endif /* FEATURE_WCDMA_L1_HS_CQI_STAT*/
   case DSATETSI_EXT_ACT_CSDF_ETSI_IDX:
     resp =  &dsat_etsi_ext_act_ss_vals->dsat_csdf_val;
     break;

   case DSATETSI_EXT_ACT_CEN_ETSI_IDX:
     resp = &dsat_etsi_ext_act_ms_vals[subs]->dsat_cen_val;
     break;

   default:
    resp = NULL;
    break;
 }
 return resp;

}/*dsatutil_get_etsi_ext_act_vals*/


/*===========================================================================
FUNCTION  DSATUTIL_GET_ETSI_EXT_VALS

DESCRIPTION
  This function will take etsi ext table command ID and will return val pointer corresponding .
  to the command ID.
DEPENDENCIES
  None

RETURNS
  val pointer

SIDE EFFECTS
  None

===========================================================================*/

void* dsatutil_get_etsi_ext_vals
(
  dsat_num_item_type       cmd_id,
  dsat_memory_subs_type    subs
)
{
  void *resp = NULL;
  if(NULL == dsat_etsi_ext_ss_vals)
  {
 /*Allocate the memory and proceed*/
    dsatutil_default_constructor(  DSAT_TABLE_ETSI_EXT_SS, subs);
    dsatutil_default_constructor(  DSAT_TABLE_ETSI_EXT_MS, DSAT_MS_MAX_SUBS);
 /*Initialize Val pointers from dlfm*/
    dsatcmdp_init_parse_table( dsatetsi_ext_table, 
                             (dsat_num_item_type)dsatetsi_ext_table_size);
  }
  if( subs >= DSAT_MS_MAX_SUBS)
  {
     return NULL;
  }
  if( NULL == dsat_etsi_ext_ms_vals[subs])
  {
    dsatutil_default_constructor(  DSAT_TABLE_ETSI_EXT_MS, subs);
  }
  switch(cmd_id)
  {
#if defined(FEATURE_DATA_GCSD) || defined(FEATURE_DATA_WCDMA_CS)
    case DSATETSI_EXT_CBST_IDX:
      resp =  &dsat_etsi_ext_ss_vals->dsat_cbst_val[0];
      break;
    case DSATETSI_EXT_CRLP_IDX:
      resp =  &dsat_etsi_ext_ss_vals->dsat_crlp_val[0][0];
      break;

    case DSATETSI_EXT_CV120_IDX:
      resp =  &dsat_etsi_ext_ss_vals->dsat_cv120_val[0];
      break;
    case DSATETSI_EXT_CHSN_IDX:
      resp =  &dsat_etsi_ext_ss_vals->dsat_chsn_val[0];
      break;
#endif /* defined(FEATURE_DATA_GCSD) || defined(FEATURE_DATA_WCDMA_CS)*/
    case DSATETSI_EXT_CSSN_IDX:
      resp = &dsat_etsi_ext_ss_vals->dsat_cssn_val[0];

    case DSATETSI_EXT_CREG_IDX:
      resp = &dsat_etsi_ext_ms_vals[subs]->dsat_creg_val;
      break;

   case DSATETSI_EXT_CGREG_IDX:
     resp = &dsat_etsi_ext_ms_vals[subs]->dsat_cgreg_val;
     break;
#ifdef FEATURE_DSAT_LTE
   case DSATETSI_EXT_CEREG_IDX:
     resp = &dsat_etsi_ext_ms_vals[subs]->dsat_cereg_val;
     break;
#endif /*FEATURE_DSAT_LTE*/
   case DSATETSI_EXT_CSCS_IDX:
     resp = &dsat_etsi_ext_ss_vals->dsat_cscs_val;
     break;

   case DSATETSI_EXT_CSTA_IDX:
     resp = &dsat_etsi_ext_ss_vals->dsat_csta_val;
     break;

   case DSATETSI_EXT_CR_IDX:
       resp = &dsat_etsi_ext_ss_vals->dsat_cr_val;
       break;

   case DSATETSI_EXT_CRC_IDX:
       resp = &dsat_etsi_ext_ss_vals->dsat_crc_val;
       break;
#ifdef FEATURE_DSAT_ETSI_DATA
   case DSATETSI_EXT_CGDCONT_IDX:
     resp = &dsat_etsi_ext_ss_vals->dsat_cgdcont_val[0];
     break;
#ifdef FEATURE_SECONDARY_PDP
   case  DSATETSI_EXT_CGDSCONT_IDX:
     resp = &dsat_etsi_ext_ss_vals->dsat_cgdscont_val[0];
      break;
  case DSATETSI_EXT_CGTFT_IDX:
    resp = &dsat_etsi_ext_ss_vals->dsat_cgtft_val[0];
    break;
#endif/*FEATURE_SECONDARY_PDP*/
#ifdef FEATURE_DATA_WCDMA_PS
   case  DSATETSI_EXT_CGEQREQ_IDX:
     resp = &dsat_etsi_ext_ss_vals->dsat_cgeqreq_val[0];
     break;
  case DSATETSI_EXT_CGEQMIN_IDX:
     resp = &dsat_etsi_ext_ss_vals->dsat_cgeqmin_val[0];
     break;
#endif /*# FEATURE_DATA_WCDMA_PS*/

#ifdef FEATURE_DSAT_LTE
    case  DSATETSI_EXT_CGEQOS_IDX:
      resp = &dsat_etsi_ext_ss_vals->dsat_cgeqos_val[0];
      break;
#endif /* FEATURE_DSAT_LTE*/
    case  DSATETSI_EXT_CGQREQ_IDX:
      resp = &dsat_etsi_ext_ss_vals->dsat_cgqreq_val[0];
      break;
    case  DSATETSI_EXT_CGQMIN_IDX:
      resp = &dsat_etsi_ext_ss_vals->dsat_cgqmin_val[0];
      break;
    case  DSATETSI_EXT_CGEREP_IDX:
      resp = &dsat_etsi_ext_ss_vals->dsat_cgerep_val[0];
      break;
    case  DSATETSI_EXT_CGDATA_IDX:
     resp = &dsat_etsi_ext_ss_vals->dsat_cgdata_val;
     break;
#if defined(FEATURE_GSM)
    case  DSATETSI_EXT_CGCLASS_IDX:
      resp = &dsat_etsi_ext_ss_vals->dsat_cgclass_val;
      break;
#endif /* FEATURE_GSM*/
#ifdef FEATURE_DATA_TE_MT_PDP
    case  DSATETSI_EXT_CGAUTO_IDX:
      resp = &dsat_etsi_ext_ss_vals->dsat_cgauto_val;
      break;
#endif /* FEATURE_DATA_TE_MT_PDP*/
    case  DSATETSI_EXT_CGPIAF_IDX:
      resp = &dsat_etsi_ext_ss_vals->dsat_cgpiaf_val[0];
      break;
#endif /* FEATURE_DSAT_ETSI_DATA*/

#ifdef FEATURE_ETSI_SMS
#ifdef FEATURE_ETSI_SMS_PS
    case  DSATETSI_EXT_CGSMS_IDX:
      resp = &dsat_etsi_ext_ms_vals[subs]->dsat_cgsms_val;
      break;
#endif  /*FEATURE_ETSI_SMS_PS*/
    case  DSATETSI_EXT_CSMS_IDX:
       resp = &dsat_etsi_ext_ms_vals[subs]->dsat_csms_val;
       break;
    case  DSATETSI_EXT_CMGF_IDX:
       resp = &dsat_etsi_ext_ss_vals->dsat_cmgf_val;
       break;
    case  DSATETSI_EXT_CSAS_IDX:
       resp = &dsat_etsi_ext_ss_vals->dsat_csas_val;
       break;
    case  DSATETSI_EXT_CRES_IDX:
       resp = &dsat_etsi_ext_ss_vals->dsat_cres_val;
       break;
    case  DSATETSI_EXT_CSCA_IDX:
       resp = &dsat_etsi_ext_ms_vals[subs]->dsat_csca_val[0];
       break;
    case  DSATETSI_EXT_CSMP_IDX:
       resp = &dsat_etsi_ext_ms_vals[subs]->dsat_csmp_val[0];
       break;
    case  DSATETSI_EXT_CSDH_IDX:
       resp = &dsat_etsi_ext_ss_vals->dsat_csdh_val;
       break;
#ifdef FEATURE_ETSI_SMS_CB
    case  DSATETSI_EXT_CSCB_IDX:
       resp = &dsat_etsi_ext_ss_vals->dsat_cscb_val[0];
       break;
#endif/* FEATURE_ETSI_SMS_CB*/
#endif /*FEATURE_ETSI_SMS*/


#ifdef FEATURE_DATA_GCSD_FAX
    case  DSATETSI_EXT_FDD_IDX:
       resp = &dsat_etsi_ext_ss_vals->dsat_fdd_val;
       break;
    case  DSATETSI_EXT_FAR_IDX :
       resp = &dsat_etsi_ext_ss_vals->dsat_far_val;
       break;
    case  DSATETSI_EXT_FCL_IDX:
       resp = &dsat_etsi_ext_ss_vals->dsat_fcl_val;
       break;
    case  DSATETSI_EXT_FIT_IDX:
       resp = &dsat_etsi_ext_ss_vals->dsat_fit_val[0];
       break;       
#endif/*FEATURE_DATA_GCSD_FAX*/
#if defined(FEATURE_WCDMA) || defined(FEATURE_TDSCDMA)
    case  DSATETSI_EXT_ES_IDX:
       resp = &dsat_etsi_ext_ss_vals->dsat_es_val[0];
       break; 
    case  DSATETSI_EXT_ESA_IDX:
       resp = &dsat_etsi_ext_ss_vals->dsat_esa_val[0];
       break; 
#endif /* FEATURE_WCDMA || FEATURE_TDSCDMA*/ 
    case  DSATETSI_EXT_CMOD_IDX:
       resp = &dsat_etsi_ext_ss_vals->dsat_cmod_val;
       break;
#ifdef FEATURE_DSAT_LTE
    case  DSATETSI_EXT_CEMODE_IDX:
       resp = &dsat_etsi_ext_ss_vals->dsat_cemode_val;
       break;
#endif /*FEATURE_DSAT_LTE*/
    case  DSATETSI_EXT_CVHU_IDX:
       resp = &dsat_etsi_ext_ss_vals->dsat_cvhu_val;
       break;
   default:
    resp = NULL;
    break;
 }
 return resp;

}/*dsatutil_get_etsi_ext_vals*/
#ifdef FEATURE_DATA_IS707 
/*===========================================================================
FUNCTION  DSATUTIL_GET_707_EXT_VALS

DESCRIPTION
  This function will take CDMA(707) ext table command ID and will return val pointer corresponding .
  to the command ID.
DEPENDENCIES
  None

RETURNS
  val pointer

SIDE EFFECTS
  None

===========================================================================*/

void* dsatutil_get_707_ext_vals
(
  dsat_num_item_type       cmd_id,
  dsat_memory_subs_type    subs
)
{
  void *resp = NULL;
  if(NULL == dsat707_ext_ss_vals)
  {
    /*Allocate the memory and proceed*/
      dsatutil_default_constructor(  DSAT_TABLE_707_PARA_EXT, subs);
    /*Initialize Val pointers from dlfm*/
      dsatcmdp_init_parse_table( dsat707_ext_para_table, 
                            (dsat_num_item_type)dsat707_ext_para_table_size);
  }
  switch(cmd_id)
  {
    case DSAT707_EXT_PARA_EB_IDX:
      resp =  &dsat707_ext_ss_vals->dsat707_eb_val[0];
      break;
    case DSAT707_EXT_PARA_EFCS_IDX:
      resp = &dsat707_ext_ss_vals->dsat707_efcs_val;
      break;
    case DSAT707_EXT_PARA_ER_IDX:
      resp = &dsat707_ext_ss_vals->dsat707_er_val;
      break;
    case DSAT707_EXT_PARA_ESR_IDX:
      resp = &dsat707_ext_ss_vals->dsat707_esr_val;
      break;
    case DSAT707_EXT_PARA_ETBM_IDX:
      resp = &dsat707_ext_ss_vals->dsat707_etbm_val[0];
      break;
    case DSAT707_EXT_PARA_MA_IDX:
      resp = &dsat707_ext_ss_vals->dsat707_ma_val[0];
      break;
    case DSAT707_EXT_PARA_MR_IDX:
      resp = &dsat707_ext_ss_vals->dsat707_mr_val;
      break;
    case DSAT707_EXT_PARA_MS_IDX:
      resp = &dsat707_ext_ss_vals->dsat707_ms_val[0];
      break;
    case DSAT707_EXT_PARA_MV18R_IDX:
      resp = &dsat707_ext_ss_vals->dsat707_mv18r_val;
      break;
    case DSAT707_EXT_PARA_MV18S_IDX:
      resp = &dsat707_ext_ss_vals->dsat707_mv18s_val[0];
      break;
    case DSAT707_EXT_PARA_CXT_IDX:
      resp = &dsat707_ext_ss_vals->dsat707_cxt_val;
      break;
    case DSAT707_EXT_PARA_CDR_IDX:
      resp = &dsat707_ext_ss_vals->dsat707_cdr_val;
      break;
    case DSAT707_EXT_PARA_CDS_IDX:
      resp = &dsat707_ext_ss_vals->dsat707_cds_val[0];
      break;
    case DSAT707_EXT_PARA_CFC_IDX:
      resp = &dsat707_ext_ss_vals->dsat707_cfc_val;
      break;
    case DSAT707_EXT_PARA_CFG_IDX:
      resp = &dsat707_ext_ss_vals->dsat707_cfg_val[0];
      break;
    case DSAT707_EXT_PARA_CQD_IDX:
      resp = &dsat707_ext_ss_vals->dsat707_cqd_val;
      break;
    case DSAT707_EXT_PARA_CRC_IDX:
      resp = &dsat707_ext_ss_vals->dsat707_crc_val;
      break;
    case DSAT707_EXT_PARA_CMUX_IDX:
 #ifdef FEATURE_DS_IS707A_CMUX
      resp = &dsat707_ext_ss_vals->dsat707_cmux_val[0];
 #else
      resp = &dsat707_ext_ss_vals->dsat707_cmux_val;
 #endif   /*FEATURE_DS_IS707A_CMUX*/
      break;
    case DSAT707_EXT_PARA_CTA_IDX:
      resp = &dsat707_ext_ss_vals->dsat707_cta_val;
      break;
    case DSAT707_EXT_PARA_ILRR_IDX:
      resp = &dsat707_ext_ss_vals->dsat707_ilrr_val;
      break;
      
    default:
     resp = NULL;
     break;
  }
  return resp;

}/*dsatutil_get_707_ext_vals*/
#ifdef FEATURE_DSAT_CDMA_PIN 
/*===========================================================================
FUNCTION  DSATUTIL_GET_707_ACT_VALS

DESCRIPTION
  This function will take CDMA(707) ext table command ID and will return val pointer corresponding .
  to the command ID.
DEPENDENCIES
  None

RETURNS
  val pointer

SIDE EFFECTS
  None

===========================================================================*/

void* dsatutil_get_707_act_vals
(
  dsat_num_item_type       cmd_id,
  dsat_memory_subs_type    subs
)
{
  void *resp = NULL;
  if(NULL == dsat707_ext_act_cmn_ss_vals)
  {
    /*Allocate the memory and proceed*/
      dsatutil_default_constructor(  DSAT_TABLE_707_ACTION_EXT, subs);
    /*Initialize Val pointers from dlfm*/
      dsatcmdp_init_parse_table( dsat707_ext_action_table, 
                            (dsat_num_item_type)dsat707_ext_action_table_size);
  }
  if(DSAT707_EXT_ACT_QCPIN_IDX == cmd_id)
  {
    resp = &dsat707_ext_act_cmn_ss_vals->dsat707_cpin_val[0];
  }
  return resp;
} /*dsatutil_get_707_act_vals*/
#endif /* FEATURE_DSAT_CDMA_PIN */
/*===========================================================================
FUNCTION  DSATUTIL_GET_707_FAX_VALS

DESCRIPTION
  This function will take CDMA(707) ext table command ID and will return val pointer corresponding .
  to the command ID.
DEPENDENCIES
  None

RETURNS
  val pointer

SIDE EFFECTS
  None

===========================================================================*/

void* dsatutil_get_707_fax_vals
(
  dsat_num_item_type       cmd_id,
  dsat_memory_subs_type    subs
)
{
  void *resp = NULL;
  if(NULL == dsat707_fax_ss_vals)
  {
/*Allocate the memory and proceed*/
   dsatutil_default_constructor(  DSAT_TABLE_707_FAX, subs);
/*Initialize Val pointers from dlfm*/
   dsatcmdp_init_parse_table( dsat707_fax_table, 
                            (dsat_num_item_type)dsat707_fax_table_size);
  }
  switch(cmd_id)
  {
    case DSAT707_FAX_FAA_IDX:
      resp =  &dsat707_fax_ss_vals->dsat707_faa_val;
      break;
    case DSAT707_FAX_FAP_IDX:
      resp = &dsat707_fax_ss_vals->dsat707_fap_val[0];
      break;
    case DSAT707_FAX_FBO_IDX:
      resp = &dsat707_fax_ss_vals->dsat707_fbo_val;
      break;
    case DSAT707_FAX_FBU_IDX:
      resp = &dsat707_fax_ss_vals->dsat707_fbu_val;
      break;
    case DSAT707_FAX_FCQ_IDX:
      resp = &dsat707_fax_ss_vals->dsat707_fcq_val[0];
      break;
    case DSAT707_FAX_FCC_IDX:
      resp = &dsat707_fax_ss_vals->dsat707_fcc_val[0];
      break;
    case DSAT707_FAX_FCR_IDX:
      resp = &dsat707_fax_ss_vals->dsat707_fcr_val;
      break;
    case DSAT707_FAX_FCT_IDX:
      resp = &dsat707_fax_ss_vals->dsat707_fct_val;
      break;
    case  DSAT707_FAX_FEA_IDX:
      resp = &dsat707_fax_ss_vals->dsat707_fea_val;
      break;
    case DSAT707_FAX_FFC_IDX:
      resp = &dsat707_fax_ss_vals->dsat707_ffc_val[0];
      break;
    case DSAT707_FAX_FHS_IDX:
      resp = &dsat707_fax_ss_vals->dsat707_fhs_val;
      break;
    case DSAT707_FAX_FIE_IDX:
      resp = &dsat707_fax_ss_vals->dsat707_fie_val;
      break;
    case DSAT707_FAX_FIP_IDX:
      resp = &dsat707_fax_ss_vals->dsat707_fip_val;
      break;
    case DSAT707_FAX_FIS_IDX:
      resp = &dsat707_fax_ss_vals->dsat707_fis_val[0];
      break;
    case DSAT707_FAX_FLI_IDX:
      resp = &dsat707_fax_ss_vals->dsat707_fli_val[0];
      break;
    case DSAT707_FAX_FLO_IDX:
      resp = &dsat707_fax_ss_vals->dsat_flo_val;
      break;
    case DSAT707_FAX_FLP_IDX:
      resp = &dsat707_fax_ss_vals->dsat707_flp_val;
      break;
    case DSAT707_FAX_FMS_IDX:
      resp = &dsat707_fax_ss_vals->dsat707_fms_val;
      break;
    case DSAT707_FAX_FNR_IDX:
      resp = &dsat707_fax_ss_vals->dsat707_fnr_val[0];
      break;
    case DSAT707_FAX_FNS_IDX:
      resp = &dsat707_fax_ss_vals->dsat707_fns_val[0];
      break;
    case DSAT707_FAX_FPA_IDX:
      resp = &dsat707_fax_ss_vals->dsat707_fpa_val[0];
      break;
    case DSAT707_FAX_FPI_IDX:
      resp = &dsat707_fax_ss_vals->dsat707_fpi_val[0];
      break;
    case DSAT707_FAX_FPP_IDX:
      resp = &dsat707_fax_ss_vals->dsat707_fpp_val;
      break;
    case DSAT707_FAX_FPR_IDX:
      resp = &dsat707_fax_ss_vals->dsat707_fpr_val;
      break;
    case DSAT707_FAX_FPS_IDX:
      resp = &dsat707_fax_ss_vals->dsat707_fps_val;
      break;
    case DSAT707_FAX_FPW_IDX:
      resp = &dsat707_fax_ss_vals->dsat707_fpw_val[0];
      break;
    case DSAT707_FAX_FRQ_IDX:
      resp = &dsat707_fax_ss_vals->dsat707_frq_val[0];
      break;
    case DSAT707_FAX_FRY_IDX:
      resp = &dsat707_fax_ss_vals->dsat707_fry_val;
      break;
    case DSAT707_FAX_FSA_IDX:
      resp = &dsat707_fax_ss_vals->dsat707_fsa_val[0];
      break;
    case DSAT707_FAX_FSP_IDX:
      resp = &dsat707_fax_ss_vals->dsat707_fsp_val;
      break;
    default:
     resp = NULL;
     break;
  }
  return resp;
}/*dsatutil_get_707_fax_vals*/

#ifdef FEATURE_HDR
#ifdef FEATURE_HDR_AT_TEST_ONLY 
/*===========================================================================
FUNCTION  DSATUTIL_GET_707_HDR_VALS

DESCRIPTION
  This function will take  HDR(707) table command ID and will return val pointer corresponding .
  to the command ID.
DEPENDENCIES
  None

RETURNS
  val pointer

SIDE EFFECTS
  None

===========================================================================*/

void* dsatutil_get_707_hdr_vals
(
  dsat_num_item_type       cmd_id,
  dsat_memory_subs_type    subs
)
{
  void *resp = NULL;
  if(NULL == dsat707_hdr_ss_vals)
  {
/*Allocate the memory and proceed*/
   dsatutil_default_constructor(  DSAT_TABLE_707_HDR, subs);
/*Initialize Val pointers from dlfm*/
   dsatcmdp_init_parse_table( dsat707_hdr_table, 
                            (dsat_num_item_type)dsat707_hdr_table_size);
  }
  switch(cmd_id)
  {
    case DSAT707_HDR_QCHDRT_IDX:
      resp =  &dsat707_hdr_ss_vals->dsat707_qchdrt_val;
      break;
    case DSAT707_HDR_QCHDRC_IDX:
      resp =  &dsat707_hdr_ss_vals->dsat707_qchdrc_val;
      break;
    case DSAT707_HDR_QCHDRR_IDX:
      resp =  &dsat707_hdr_ss_vals->dsat707_qchdrr_val;
      break;
    case DSAT707_HDR_QCHDRET_IDX:
      resp =  &dsat707_hdr_ss_vals->dsat707_qchdret_val;
      break;
    default:
     resp = NULL;
     break;
  }
  return resp;
}/*dsatutil_get_707_hdr_vals*/
#endif /* FEATURE_HDR_AT_TEST_ONLY */
#endif /* FEATURE_HDR */

#ifdef FEATURE_DS_MOBILE_IP
/*===========================================================================
FUNCTION  DSATUTIL_GET_707_MIP_VALS

DESCRIPTION
  This function will take  MIP table command ID and will return val pointer corresponding .
  to the command ID.
DEPENDENCIES
  None

RETURNS
  val pointer

SIDE EFFECTS
  None

===========================================================================*/

void* dsatutil_get_707_mip_vals
(
  dsat_num_item_type       cmd_id,
  dsat_memory_subs_type    subs
)
{
  void *resp = NULL;
  /*DS 707 Will own this variable. ATCoP will use get/set api*/
  return resp;
}/*dsatutil_get_707_mip_vals*/
#endif /* FEATURE_DS_MOBILE_IP*/
#ifdef FEATURE_CDMA_SMS
/*===========================================================================
FUNCTION  DSATUTIL_GET_707_SMS_VALS

DESCRIPTION
  This function will take  SMS(707) table command ID and will return val pointer corresponding .
  to the command ID.
DEPENDENCIES
  None

RETURNS
  val pointer

SIDE EFFECTS
  None

===========================================================================*/

void* dsatutil_get_707_sms_vals
(
  dsat_num_item_type       cmd_id,
  dsat_memory_subs_type    subs
)
{
  void *resp = NULL;
  if(NULL == dsat707_sms_ss_vals)
  {
/*Allocate the memory and proceed*/
   dsatutil_default_constructor(  DSAT_TABLE_707_SMS,subs);
/*Initialize Val pointers from dlfm*/
   dsatcmdp_init_parse_table( dsat707_sms_table, 
                            (dsat_num_item_type)dsat707_sms_table_size);
  }
  switch(cmd_id)
  {
    case DSAT707_SMS_QCNMI_IDX:
      resp =  &dsat707_sms_ss_vals->dsat707sms_qcnmi_val[0];
      break;
    case DSAT707_SMS_QCSMP_IDX:
      resp =  &dsat707_sms_ss_vals->dsat707sms_qcsmp_val[0];
      break;
    case DSAT707_SMS_QCPMS_IDX:
      resp =  &dsat707_sms_ss_vals->dsat707sms_qcpms_val[0];
      break;
    case DSAT707_SMS_QCMGF_IDX:
      resp =  &dsat707_sms_ss_vals->dsat707sms_qcmgf_val;
      break;
    case DSAT707_SMS_HSMSSS_IDX:
      resp =  &dsat707_sms_ss_vals->dsat707sms_smsss_val[0];
      break;
    default:
     resp = NULL;
     break;
  }
  return resp;
}/*dsatutil_get_707_sms_vals*/
#endif /* FEATURE_CDMA_SMS*/

/*===========================================================================
FUNCTION  DSATUTIL_GET_707_VEND_PARA_VALS

DESCRIPTION
  This function will take  Vender Parameter(707) table command ID and will return val pointer corresponding .
  to the command ID.
DEPENDENCIES
  None

RETURNS
  val pointer

SIDE EFFECTS
  None

===========================================================================*/

void* dsatutil_get_707_vend_para_vals
(
  dsat_num_item_type       cmd_id,
  dsat_memory_subs_type    subs
)
{
  void *resp = NULL;
  if(NULL == dsat707_vend_para_ss_vals)
  {
/*Allocate the memory and proceed*/
   dsatutil_default_constructor(  DSAT_TABLE_707_VENDOR_PARA, subs);
/*Initialize Val pointers from dlfm*/
   dsatcmdp_init_parse_table( dsat707_vend_para_table, 
                            (dsat_num_item_type)dsat707_vend_para_table_size);
  }
  switch(cmd_id)
  {
    case DSAT707_VEND_PARA_QCMDR_IDX:
      resp =  &dsat707_vend_para_ss_vals->dsat707_qcmdr_val;
      break;
#ifdef  FEATURE_IS2000_R_SCH 
    case DSAT707_VEND_PARA_QCSCRM_IDX:
      resp =  &dsat707_vend_para_ss_vals->dsat707_qcscrm_val;
      break;
    case DSAT707_VEND_PARA_QCTRTL_IDX:
      resp =  &dsat707_vend_para_ss_vals->dsat707_qctrtl_val;
      break;
#endif /* FEATURE_IS2000_R_SCH */
    case DSAT707_VEND_PARA_QCVAD_IDX:
      resp =  &dsat707_vend_para_ss_vals->dsat707_qcvad_val;
      break;
#ifdef FEATURE_IS2000_REL_A
    case DSAT707_VEND_PARA_QCQOSPRI_IDX:
      resp =  &dsat707_vend_para_ss_vals->dsat707_qcqospri_val;
      break;
#endif /* FEATURE_IS2000_REL_A*/
#ifdef FEATURE_IS2000_CHS
    case DSAT707_VEND_PARA_QCCHS_IDX:
      resp =  &dsat707_vend_para_ss_vals->dsat707_qcchs_val;
      break;
#endif /*FEATURE_IS2000_CHS*/
    default:
     resp = NULL;
     break;
  }
  return resp;
}/*dsatutil_get_707_vend_para_vals*/

/*===========================================================================
FUNCTION  DSATUTIL_GET_707_VEND_ACT_VALS

DESCRIPTION
  This function will take  Vender Action(707) table command ID and will return val pointer corresponding .
  to the command ID.
DEPENDENCIES
  None

RETURNS
  val pointer

SIDE EFFECTS
  None

===========================================================================*/

void* dsatutil_get_707_vend_act_vals
(
  dsat_num_item_type       cmd_id,
  dsat_memory_subs_type    subs
)
{
  void *resp = NULL;
  if(NULL == dsat707_vend_act_ss_vals)
  {
/*Allocate the memory and proceed*/
   dsatutil_default_constructor(  DSAT_TABLE_707_VENDOR_ACTION, subs);
/*Initialize Val pointers from dlfm*/
   dsatcmdp_init_parse_table( dsat707_vend_action_table, 
                            (dsat_num_item_type)dsat707_vend_action_table_size);
  }
  switch(cmd_id)
  {
    case DSAT707_VEND_ACT_QCOTC_IDX:
      resp =  &dsat707_vend_act_ss_vals->dsat707_qcotc_val[0];
      break;
#ifdef FEATURE_DSAT_CDMA_PIN 
    case DSAT707_VEND_ACT_CPIN_IDX:
      if(NULL != dsat707_ext_act_cmn_ss_vals)
      {
        resp = &dsat707_ext_act_cmn_ss_vals->dsat707_cpin_val[0];
      }
      break;
#endif/* FEATURE_DSAT_CDMA_PIN */
#if defined(FEATURE_DSAT_SP_CMDS)
    case DSAT707_VEND_ACT_SPSPC_IDX:
      resp =  &dsat707_vend_act_ss_vals->dsat_spc_val;
      break;
    case DSAT707_VEND_ACT_SPROAM_IDX:
      resp =  &dsat707_vend_act_ss_vals->dsat707_roam_pref_val;
      break;
    case DSAT707_VEND_ACT_SPUNLOCK_IDX:
      resp =  &dsat707_vend_act_ss_vals->dsat_unlock_val;
      break;
    case DSAT707_VEND_ACT_SPLOCK_IDX:
      resp =  &dsat707_vend_act_ss_vals->dsat_lock_val;
      break;
    case DSAT707_VEND_ACT_SPLOCKCHG_IDX:
      resp =  &dsat707_vend_act_ss_vals->dsat_lock_chg_val[0];
      break;
#endif /* FEATURE_DSAT_SP_CMDS */

#if defined(FEATURE_DSAT_GPS_CMDS)
    case DSAT707_VEND_ACT_SPNMEA_IDX:
      resp =  &dsat707_vend_act_ss_vals->dsat707_gps_nmea_config_val[0];
      break;
    case DSAT707_VEND_ACT_SPLOCATION_IDX:
      resp =  &dsat707_vend_act_ss_vals->dsat707_sp_lcs_state;
      break;
    case DSAT707_VEND_ACT_ACT_GPS_MODE_IDX:
      resp =  &dsat707_vend_act_ss_vals->dsat707_gps_mode_val;
      break;
    case DSAT707_VEND_ACT_ACT_GPS_PORT_IDX:
      resp =  &dsat707_vend_act_ss_vals->dsat707_gps_port_val;
      break;
    case DSAT707_VEND_ACT_ACT_PDE_TRS_IDX:
      resp =  &dsat707_vend_act_ss_vals->dsat707_pde_trs_val;
      break;
    case DSAT707_VEND_ACT_ACT_GPS_PDEADDR_IDX:
      resp =  &dsat707_vend_act_ss_vals->dsat707_gps_pdeaddr_val[0];
      break;
    case DSAT707_VEND_ACT_ACT_INIT_MASK_IDX:
      resp =  &dsat707_vend_act_ss_vals->dsat707_gps_init_mask_val;
      break; 
#endif /*defined(FEATURE_DSAT_GPS_CMDS)*/
    default:
     resp = NULL;
     break;
  }
  return resp;
}/*dsatutil_get_707_vend_act_vals*/
#endif /* FEATURE_DATA_IS707 */

#ifdef FEATURE_BCMCS
/*===========================================================================
FUNCTION  DSATUTIL_GET_707_1XHDR_VALS

DESCRIPTION
  This function will take  HDR table command ID and will return val pointer corresponding .
  to the command ID.
DEPENDENCIES
  None

RETURNS
  val pointer

SIDE EFFECTS
  None

===========================================================================*/

void* dsatutil_get_707_1xhdr_vals
(
  dsat_num_item_type       cmd_id,
  dsat_memory_subs_type    subs
)
{
  void *resp = NULL;
  if(NULL == dsat707_1xhdr_ss_vals)
  {
/*Allocate the memory and proceed*/
   dsatutil_default_constructor(  DSAT_TABLE_707_1XHDR, subs);
/*Initialize Val pointers from dlfm*/
   dsatcmdp_init_parse_table( dsat_1xhdr_bc_table, 
                            (dsat_num_item_type)dsat_1xhdr_bc_table_size);
  }
  switch(cmd_id)
  {
    case DSAT_1XHDR_BC_QCBCEN_IDX:
      resp =  &dsat707_1xhdr_ss_vals->dsat_1xhdr_qcbcen_val;
      break;
    case DSAT_1XHDR_BC_QCBCIPETSI_IDX:
      resp =  &dsat707_1xhdr_ss_vals->dsat_1xhdr_qcbcip_val[0];
      break;
    default:
     resp = NULL;
     break;
  }
  return resp;
}/*dsatutil_get_707_1xhdr_vals*/
#endif /* FEATURE_BCMCS*/
/*===========================================================================
FUNCTION  DSATUTIL_GET_VAL_FROM_CMD_ID

DESCRIPTION
  This function will take valid command ID and will return val pointer corresponding .
  to the command ID.
DEPENDENCIES
  None

RETURNS
  val pointer

SIDE EFFECTS
  None

===========================================================================*/

void* dsatutil_get_val_from_cmd_id
(
  dsat_num_item_type       cmd_id,
  dsat_memory_subs_type    subs
)
{
void *resp = NULL;

#ifdef FEATURE_DATA_IS707 
 /* 1X command tables*/
  if(cmd_id < DSAT707_EXT_PARA_MAX_IDX)
  {
    resp = dsatutil_get_707_ext_vals(cmd_id,subs);
  }
  else if( DSAT707_EXT_ACT_CDV_IDX <= cmd_id &&
                                  cmd_id < DSAT707_EXT_ACT_MAX_IDX)
  {
#ifdef FEATURE_DSAT_CDMA_PIN 
   resp = dsatutil_get_707_act_vals(cmd_id,subs);
#endif /* FEATURE_DSAT_CDMA_PIN */
  }
  else if(DSAT707_FAX_FAA_IDX <= cmd_id &&
                                cmd_id < DSAT707_FAX_MAX_IDX)
  {
   resp = dsatutil_get_707_fax_vals(cmd_id,subs);
  }
  else if(  DSAT707_HDR_QCHDRT_IDX <= cmd_id && 
                          cmd_id < DSAT707_HDR_MAX_IDX)
  {
#ifdef FEATURE_HDR
#ifdef FEATURE_HDR_AT_TEST_ONLY 
   resp = dsatutil_get_707_hdr_vals(cmd_id,subs);
#endif /* FEATURE_HDR_AT_TEST_ONLY */
#endif /* FEATURE_HDR */
  }
#ifdef FEATURE_DS_MOBILE_IP
  else if( DSAT707_MIP_QCMIP_IDX <= cmd_id &&
                                 cmd_id < DSAT707_MIP_MAX_IDX)
  {
    resp = dsatutil_get_707_mip_vals(cmd_id,subs);
  }
#endif /*FEATURE_DS_MOBILE_IP*/
  else if( DSAT707_PSTATS_QCRLPD_IDX <= cmd_id &&
                                   cmd_id < DSAT707_PSTATS_MAX_IDX)
  {
#ifdef FEATURE_DS_PSTATS
     /*PSTATE table doesnt have val pointes*/
    return resp;
#endif /* FEATURE_DS_PSTATS */
  }
#ifdef FEATURE_CDMA_SMS
  else if( DSAT707_SMS_QCNMI_IDX <= cmd_id && 
                                     cmd_id < DSAT707_SMS_MAX_IDX)
  {
    resp = dsatutil_get_707_sms_vals(cmd_id,subs);
  }
#endif/* FEATURE_CDMA_SMS*/
  else if( DSAT707_VEND_PARA_QCPREV_IDX <= cmd_id && 
                                cmd_id < DSAT707_VEND_PARA_MAX_IDX)
  {
    resp = dsatutil_get_707_vend_para_vals(cmd_id,subs);
  }
  else if( DSAT707_VEND_ACT_CPBR_IDX <= cmd_id && 
                          cmd_id < DSAT707_VEND_ACT_MAX_IDX)
  {
     resp = dsatutil_get_707_vend_act_vals(cmd_id,subs);
  }
  else
#endif /* FEATURE_DATA_IS707   */
#ifdef FEATURE_BCMCS
  if( DSAT_1XHDR_BC_QCBCEN_IDX <= cmd_id && 
                               cmd_id < DSAT_1XHDR_BC_MAX_IDX)
  {
     resp = dsatutil_get_707_1xhdr_vals(cmd_id,subs);
  }  /* Basic command tables*/
  else 
#endif /* FEATURE_BCMCS */ 
  if( DSAT_BASIC_N_C_IDX <= cmd_id && 
           cmd_id < DSAT_BASIC_MAX_IDX)
  {
    resp = dsatutil_get_basic_vals(cmd_id,subs);
  }
  else if( DSAT_BASIC_ACT_D_IDX <= cmd_id && 
                cmd_id < DSAT_BASIC_ACT_MAX_IDX)
  {
    resp = dsatutil_get_basic_action_vals(cmd_id,subs);
  }
  else if(  DSAT_SREG_S0_IDX <= cmd_id && 
                 cmd_id < DSAT_SREG_MAX_IDX )
  {
    resp = dsatutil_get_basic_sreg_vals(cmd_id,subs);
  }
  else if( DSAT_EXT_FCLASS_IDX <= cmd_id && 
                cmd_id < DSAT_EXT_MAX_IDX)
  {
    resp = dsatutil_get_basic_ext_vals(cmd_id,subs);
  }
  else if( DSAT_VENDOR_QCSIMSTAT_IDX <= cmd_id &&
                cmd_id < DSAT_VENDOR_MAX_IDX)
  {
    resp = dsatutil_get_basic_vend_vals(cmd_id,subs);
  }
#ifdef FEATURE_DSAT_ETSI_MODE
  /* ETSI command tables*/
  else if( DSATETSI_EXT_ACT_CSQ_ETSI_IDX <= cmd_id && 
                    cmd_id < DSATETSI_EXT_ACT_MAX_ETSI_IDX)
  {
    resp = dsatutil_get_etsi_ext_act_vals(cmd_id,subs);
  }
  else if( DSATETSI_EXT_CBST_IDX <= cmd_id && 
                    cmd_id < DSATETSI_EXT_MAX_IDX )
  {
    resp = dsatutil_get_etsi_ext_vals(cmd_id,subs);
  }
#endif /* FEATURE_DSAT_ETSI_MODE */
  else
  {
    DS_AT_MSG1_ERROR("Table not found for %d index",cmd_id);
    ASSERT(0);
  }
  return resp;
}/*dsatutil_get_val_from_cmd_id*/
/*===========================================================================
FUNCTION  DSATUTIL_DEFAULT_CONSTRUCTOR

DESCRIPTION
  This function will allocate and initialize the dynamic memory.
  
DEPENDENCIES
  None

RETURNS
  TRUE  - SUCCESS
  FALSE - FAILURE

SIDE EFFECTS
  None

===========================================================================*/

boolean dsatutil_default_constructor
(
  dsat_mem_type           mem_type,
  dsat_memory_subs_type    subs
)
{
  boolean                result       = DSAT_SUCCESS;
  dsat_num_item_type     alloc_size   = 0;
  dsat_num_item_type     i            = 0;
  dsat_num_item_type     max_subs     = 1;
  dsat_num_item_type     max_act_subs = 1;

  ALLOWED_MAX_SUB(max_subs)
  ALLOWED_MAX_ACTIVE(max_act_subs)
  switch(mem_type)
  {
    case DSAT_ALL_ATCOP_STRUCT_TABLE:
    {
      alloc_size = sizeof(dsat_basic_table_ss_val_struct_type);
      dsat_basic_vals =  (dsat_basic_table_ss_val_struct_type *)dsat_alloc_memory(alloc_size, FALSE);

      alloc_size = sizeof(dsat_basic_action_table_ss_val_struct_type);
      dsat_basic_act_vals =(dsat_basic_action_table_ss_val_struct_type *)dsat_alloc_memory(alloc_size, FALSE);
     
      alloc_size = sizeof(dsat_basic_sreg_table_ss_val_struct_type);
      dsat_basic_sreg_vals =(dsat_basic_sreg_table_ss_val_struct_type *)dsat_alloc_memory(alloc_size, FALSE);
      
      alloc_size = sizeof(dsat_basic_ext_table_ss_val_struct_type);
      dsat_basic_ext_vals = (dsat_basic_ext_table_ss_val_struct_type *)dsat_alloc_memory(alloc_size, FALSE);
     
/*Allocate and initilize basic vender table val*/
      alloc_size = sizeof(dsat_basic_vend_table_ss_val_struct_type);
      dsat_basic_vend_ss_vals = (dsat_basic_vend_table_ss_val_struct_type *)dsat_alloc_memory(alloc_size, FALSE);
/*Initilize val pointers to default values*/
      dsat_basic_vend_ss_vals->dsat_qcpdpp_val[2].string_item = qcpdpp_passw_val;
      dsat_basic_vend_ss_vals->dsat_qcpdpp_val[3].string_item = qcpdpp_uname_val;
      
      alloc_size = sizeof(dsat_basic_vend_table_ms_val_struct_type);
      for(i = 0 ; i <max_subs; i++)
      {
        dsat_basic_vend_ms_vals[i] = (dsat_basic_vend_table_ms_val_struct_type *)dsat_alloc_memory(alloc_size, FALSE);
      }
/*Allocating memory to common variable*/
      alloc_size = sizeof(dsat_basic_etsi_table_common_val);
      for(i = 0 ; i <max_subs; i++)
      {
        dsat_basic_etsi_cmn_ms_vals[i] = (dsat_basic_etsi_table_common_val *)dsat_alloc_memory(alloc_size, FALSE);
      }

/*Allocate and initilize ETSI Ext table val*/
      alloc_size = sizeof(dsat_etsi_ext_act_table_ss_val_struct_type);
      dsat_etsi_ext_act_ss_vals = (dsat_etsi_ext_act_table_ss_val_struct_type *)dsat_alloc_memory(alloc_size, FALSE);
      
/*Initilize val pointers to static memory*/
      dsat_etsi_ext_act_ss_vals->dsat_cusd_val[1].string_item = cusd_str_val;
      dsat_etsi_ext_act_ss_vals->dsat_cusd_val[2].string_item = cusd_dcs_val;
      dsat_etsi_ext_act_ss_vals->dsat_csdf_val = 1;
#ifdef FEATURE_DATA_TE_MT_PDP
      dsat_etsi_ext_act_ss_vals->dsat_cgans_val[1].string_item = cgans_l2p_type_val;
#endif /* FEATURE_DATA_TE_MT_PDP*/
      alloc_size = sizeof(dsat_etsi_ext_act_table_ms_val_struct_type);
      for(i = 0 ; i <max_subs; i++)
      {
        dsat_etsi_ext_act_ms_vals[i] = (dsat_etsi_ext_act_table_ms_val_struct_type *)dsat_alloc_memory(alloc_size, FALSE);
/*Initilize val pointers to static memory*/
        dsat_etsi_ext_act_ms_vals[i]->dsat_cpin_val[0].string_item = cpin_pin_val[i];
        dsat_etsi_ext_act_ms_vals[i]->dsat_cpin_val[1].string_item = cpin_newpin_val[i];
        dsat_etsi_ext_act_ms_vals[i]->dsat_cops_val[2].string_item = cops_oper_val[i];
        dsat_etsi_ext_act_ms_vals[i]->dsat_cpol_val[2].string_item = cpol_oper_val[i];
        dsat_etsi_ext_act_ms_vals[i]->dsat_cops_val[4].string_item = cops_csg_id_val[i];
      }
/*Allocate and initilize ETSI Ext table val*/
      alloc_size = sizeof(dsat_etsi_ext_table_ss_val_struct_type);
      dsat_etsi_ext_ss_vals = (dsat_etsi_ext_table_ss_val_struct_type *)dsat_alloc_memory(alloc_size, FALSE);
            
/*Initilize val pointers to static memory*/
      dsat_etsi_ext_ss_vals->dsat_cgdcont_val[1].string_item = cgdcont_pdptype_val;
      dsat_etsi_ext_ss_vals->dsat_cgdcont_val[2].string_item = cgdcont_apn_val;
      dsat_etsi_ext_ss_vals->dsat_cgdcont_val[3].string_item = cgdcont_pdpaddr_val;
       
#ifdef FEATURE_SECONDARY_PDP
      dsat_etsi_ext_ss_vals->dsat_cgtft_val[3].string_item = cgtft_addr_subnet_val;
      dsat_etsi_ext_ss_vals->dsat_cgtft_val[5].string_item = cgtft_dst_port_val;
      dsat_etsi_ext_ss_vals->dsat_cgtft_val[6].string_item = cgtft_src_port_val;
      dsat_etsi_ext_ss_vals->dsat_cgtft_val[8].string_item = cgtft_tos_tclass_val;
#endif /* FEATURE_SECONDARY_PDP */

#ifdef FEATURE_DATA_WCDMA_PS
      dsat_etsi_ext_ss_vals->dsat_cgeqreq_val[8].string_item = cgeqreq_sdu_err_val;
      dsat_etsi_ext_ss_vals->dsat_cgeqreq_val[9].string_item = cgeqreq_res_biterr_val;
      
      dsat_etsi_ext_ss_vals->dsat_cgeqmin_val[8].string_item = cgeqmin_sdu_err_val;
      dsat_etsi_ext_ss_vals->dsat_cgeqmin_val[9].string_item =cgeqmin_res_biterr_val;
#endif /* FEATURE_DATA_WCDMA_PS */
      dsat_etsi_ext_ss_vals->dsat_cscb_val[1].string_item= cscb_mids_val;
      dsat_etsi_ext_ss_vals->dsat_cscb_val[2].string_item= cscb_dcss_val;

#if  defined(FEATURE_WCDMA) || defined(FEATURE_TDSCDMA)   
      dsat_etsi_ext_ss_vals->dsat_es_val[0].string_item =  dsat_es_orig_rqst_val;
      dsat_etsi_ext_ss_vals->dsat_es_val[1].string_item =  dsat_es_orig_fbk_val;
      dsat_etsi_ext_ss_vals->dsat_es_val[2].string_item =  dsat_es_ans_fbk_val;

      dsat_etsi_ext_ss_vals->dsat_esa_val[1].string_item = dsat_esa_trans_idle_val;
      dsat_etsi_ext_ss_vals->dsat_esa_val[2].string_item = dsat_esa_framed_un_ov_val;
      dsat_etsi_ext_ss_vals->dsat_esa_val[3].string_item = dsat_esa_hd_auto_val;
      dsat_etsi_ext_ss_vals->dsat_esa_val[7].string_item = dsat_esa_syn2_val;
#endif /* FEATURE_WCDMA ||  FEATURE_TDSCDMA*/

      alloc_size = sizeof(dsat_etsi_ext_table_ms_val_struct_type);
      for(i = 0 ; i < max_subs; i++)
      {
        dsat_etsi_ext_ms_vals[i] = (dsat_etsi_ext_table_ms_val_struct_type *)dsat_alloc_memory(alloc_size, FALSE);
/*Initilize val pointers to static memory*/
        dsat_etsi_ext_ms_vals[i]->dsat_csca_val[0].string_item = dsat_csca_sca_val[i];
        dsat_etsi_ext_ms_vals[i]->dsat_csca_val[1].string_item = dsat_csca_tosca_val[i];
        dsat_etsi_ext_ms_vals[i]->dsat_csmp_val[0].string_item =  csmp_fo_val[i];
        dsat_etsi_ext_ms_vals[i]->dsat_csmp_val[1].string_item =  csmp_vp_val[i];
      }
#ifdef FEATURE_DATA_IS707 
/*Allocate and initilize 707 Ext table val*/
      alloc_size = sizeof(dsat707_ext_para_table_ss_val_struct_type);
      dsat707_ext_ss_vals = (dsat707_ext_para_table_ss_val_struct_type *)dsat_alloc_memory(alloc_size, FALSE);
#ifdef FEATURE_DSAT_CDMA_PIN 
      alloc_size = sizeof(dsat707_ext_vend_act_table_common_val);
      dsat707_ext_act_cmn_ss_vals = (dsat707_ext_vend_act_table_common_val *)dsat_alloc_memory(alloc_size, FALSE);
      dsat707_ext_act_cmn_ss_vals->dsat707_cpin_val[0].string_item = dsat707_cpin_pin_val;
      dsat707_ext_act_cmn_ss_vals->dsat707_cpin_val[1].string_item = dsat707_cpin_newpin_val;
#endif /* FEATURE_DSAT_CDMA_PIN */
      
      alloc_size = sizeof(dsat707_fax_table_ss_val_struct_type);
      dsat707_fax_ss_vals = (dsat707_fax_table_ss_val_struct_type *)dsat_alloc_memory(alloc_size, FALSE);
#ifdef FEATURE_HDR
#ifdef FEATURE_HDR_AT_TEST_ONLY 
      alloc_size = sizeof(dsat707_hdr_table_ss_val_struct_type);
      dsat707_hdr_ss_vals = (dsat707_hdr_table_ss_val_struct_type *)dsat_alloc_memory(alloc_size, FALSE);
#endif /* FEATURE_HDR_AT_TEST_ONLY */
#endif /* FEATURE_HDR */
#ifdef FEATURE_CDMA_SMS
      alloc_size = sizeof(dsat707_sms_table_ss_val_struct_type);
      dsat707_sms_ss_vals = (dsat707_sms_table_ss_val_struct_type *)dsat_alloc_memory(alloc_size, FALSE);
        
      dsat707_sms_ss_vals->dsat707sms_qcsmp_val[2].string_item = qcsmp_vp_val;
      dsat707_sms_ss_vals->dsat707sms_qcsmp_val[4].string_item = qcsmp_tdd_val;
#endif /* FEATURE_CDMA_SMS*/
      alloc_size = sizeof(dsat707_vend_para_table_ss_val_struct_type);
      dsat707_vend_para_ss_vals = (dsat707_vend_para_table_ss_val_struct_type *)dsat_alloc_memory(alloc_size, FALSE);
        
      alloc_size = sizeof(dsat707_vend_act_table_ss_val_struct_type);
      dsat707_vend_act_ss_vals = (dsat707_vend_act_table_ss_val_struct_type *)dsat_alloc_memory(alloc_size, FALSE);
        
#if defined(FEATURE_DSAT_SP_CMDS)
      dsat707_vend_act_ss_vals->dsat_spc_val.string_item = spc_val;
      dsat707_vend_act_ss_vals->dsat_unlock_val.string_item =  unlock_val;
      dsat707_vend_act_ss_vals->dsat_lock_val.string_item = lock_val;
      dsat707_vend_act_ss_vals->dsat_lock_chg_val[0].string_item = old_lock_val;
      dsat707_vend_act_ss_vals->dsat_lock_chg_val[1].string_item = new_lock_val;
#endif /*FEATURE_DSAT_SP_CMDS*/
#endif /* FEATURE_DATA_IS707 */
#ifdef FEATURE_BCMCS
      alloc_size = sizeof(dsat707_vend_1xhdr_table_ss_val_struct_type);
      dsat707_1xhdr_ss_vals = (dsat707_vend_1xhdr_table_ss_val_struct_type *)dsat_alloc_memory(alloc_size, FALSE);
#endif /* FEATURE_BCMCS*/
/* CM interface Variable */
/* SS event and PH event*/
      alloc_size = sizeof(dsatcmif_servs_state_ss_info);
      dsatcmif_ss_ph_val.ss = (dsatcmif_servs_state_ss_info *)dsat_alloc_memory(alloc_size, FALSE);
      dsatcmif_initialize_to_default(DSAT_SS_PH_SS_VALS,dsatcmif_ss_ph_val.ss);

      alloc_size = sizeof(dsatcmif_servs_state_ms_info);
      for(i = 0 ; i < max_subs; i++)
      {
        dsatcmif_ss_ph_val.ms[i] = (dsatcmif_servs_state_ms_info *)dsat_alloc_memory(alloc_size, FALSE);
        dsatcmif_initialize_to_default(DSAT_SS_PH_MS_VALS,dsatcmif_ss_ph_val.ms[i]);
      }
/* Call Info */
      alloc_size = sizeof(dsatetsicall_call_state_ss_info);
      dsatcmif_call_val.ss = (dsatetsicall_call_state_ss_info *)dsat_alloc_memory(alloc_size, FALSE);
      dsatcmif_initialize_to_default(DSAT_CALL_SS_VALS,dsatcmif_call_val.ss);

      alloc_size = sizeof(dsatetsicall_call_state_ms_info);
      for(i = 0 ; i < max_subs; i++)
      {
        dsatcmif_call_val.ms[i] = (dsatetsicall_call_state_ms_info *)dsat_alloc_memory(alloc_size, FALSE);
        dsatcmif_initialize_to_default(DSAT_CALL_MS_VALS,dsatcmif_call_val.ms[i]);
      }

      alloc_size = sizeof(dsatetsicall_call_state_da_info);
      for(i = 0 ; i < max_act_subs; i++)
      {
        dsatcmif_call_val.da[i] = (dsatetsicall_call_state_da_info *)dsat_alloc_memory(alloc_size, FALSE);
        dsatcmif_initialize_to_default(DSAT_CALL_DA_VALS,dsatcmif_call_val.da[i]);
      }
      
/*SUPs Info*/
      alloc_size = sizeof(dsatetsicmif_sups_state_ss_info);
      dsatcmif_sups_val.ss = (dsatetsicmif_sups_state_ss_info *)dsat_alloc_memory(alloc_size, FALSE);
      dsatcmif_initialize_to_default(DSAT_SUPS_SS_VALS,dsatcmif_sups_val.ss);

      alloc_size = sizeof(dsatetsicmif_sups_state_ms_info);
      for(i = 0 ; i < max_subs; i++)
      {
        dsatcmif_sups_val.ms[i] = (dsatetsicmif_sups_state_ms_info *)dsat_alloc_memory(alloc_size, FALSE);
        dsatcmif_initialize_to_default(DSAT_SUPS_MS_VALS,dsatcmif_sups_val.ms[i]);
      }
/*MMGSDI Info*/
      alloc_size = sizeof(dsatme_mmgsdi_state_ss_info);
      dsatme_mmgsdi_val.ss = (dsatme_mmgsdi_state_ss_info *)dsat_alloc_memory(alloc_size, FALSE);
      dsatme_initialize_to_default(DSAT_MMGSDI_SS_VALS,dsatme_mmgsdi_val.ss);
      
      alloc_size = sizeof(dsatme_mmgsdi_state_ms_info);
      for(i = 0 ; i < max_subs; i++)
      {
        dsatme_mmgsdi_val.ms[i] = (dsatme_mmgsdi_state_ms_info *)dsat_alloc_memory(alloc_size, FALSE);
        dsatme_initialize_to_default(DSAT_MMGSDI_MS_VALS,dsatme_mmgsdi_val.ms[i]);
      }
/*PBM info*/
      alloc_size = sizeof(dsatme_pbm_state_ss_info);
      dsatme_pbm_val.ss = (dsatme_pbm_state_ss_info *)dsat_alloc_memory(alloc_size, FALSE);
      dsatme_initialize_to_default(DSAT_PBM_SS_VALS,dsatme_pbm_val.ss);

/*Common info*/
      alloc_size = sizeof(dsat_common_state_ss_info);
      dsat_common_val.ss = (dsat_common_state_ss_info *)dsat_alloc_memory(alloc_size, FALSE);
      dsatme_initialize_to_default(DSAT_COMMON_SS_VALS,dsat_common_val.ss);

/*MS MD info*/
      alloc_size = sizeof(dsatetsipkt_gprs_msg_ms_info);
      for(i = 0 ; i < max_subs; i++)
      {
        dsatetsipkt_gprs_val.ms[i] = (dsatetsipkt_gprs_msg_ms_info *)dsat_alloc_memory(alloc_size, FALSE);
        dsatme_initialize_to_default(DSAT_MS_MD_VALS,dsatetsipkt_gprs_val.ms[i]);
      }

    }
    break;
    case DSAT_TABLE_BASIC:
    {
      alloc_size = sizeof(dsat_basic_table_ss_val_struct_type);
      dsat_basic_vals =  (dsat_basic_table_ss_val_struct_type *)dsat_alloc_memory(alloc_size, FALSE);
    }
    break;
    case DSAT_TABLE_BASIC_ACTION:
    {
      alloc_size = sizeof(dsat_basic_action_table_ss_val_struct_type);
      dsat_basic_act_vals =(dsat_basic_action_table_ss_val_struct_type *)dsat_alloc_memory(alloc_size, FALSE);
    }
    break;
    case DSAT_TABLE_SREG:
    {
      alloc_size = sizeof(dsat_basic_sreg_table_ss_val_struct_type);
      dsat_basic_sreg_vals =(dsat_basic_sreg_table_ss_val_struct_type *)dsat_alloc_memory(alloc_size, FALSE);
    }
    break;
    case DSAT_TABLE_EXT:
    {
      alloc_size = sizeof(dsat_basic_ext_table_ss_val_struct_type);
      dsat_basic_ext_vals = (dsat_basic_ext_table_ss_val_struct_type *)dsat_alloc_memory(alloc_size, FALSE);
    }
    break;
    case DSAT_TABLE_VEND_EXT_SS:
    {
      alloc_size = sizeof(dsat_basic_vend_table_ss_val_struct_type);
      dsat_basic_vend_ss_vals = (dsat_basic_vend_table_ss_val_struct_type *)dsat_alloc_memory(alloc_size, FALSE);
        /*Initilize val pointers to default values*/
      dsat_basic_vend_ss_vals->dsat_qcpdpp_val[2].string_item = qcpdpp_passw_val;
      dsat_basic_vend_ss_vals->dsat_qcpdpp_val[3].string_item = qcpdpp_uname_val;
    }
    break;
    case DSAT_TABLE_VEND_EXT_MS:
    {
     
      if(DSAT_MS_MAX_SUBS == subs)
      {
        for(i = 0 ; i < max_subs; i++)
        {
          alloc_size = sizeof(dsat_basic_vend_table_ms_val_struct_type);
          dsat_basic_vend_ms_vals[i] = (dsat_basic_vend_table_ms_val_struct_type *)dsat_alloc_memory(alloc_size, FALSE);
          if(dsat_basic_etsi_cmn_ms_vals[i] == NULL)
          {
            alloc_size = sizeof(dsat_basic_etsi_table_common_val);
            dsat_basic_etsi_cmn_ms_vals[i] = (dsat_basic_etsi_table_common_val *)dsat_alloc_memory(alloc_size, FALSE);
          }
        }
      }
      else if(subs < DSAT_MS_MAX_SUBS)
      {
        alloc_size = sizeof(dsat_basic_vend_table_ms_val_struct_type);
        dsat_basic_vend_ms_vals[subs] = (dsat_basic_vend_table_ms_val_struct_type *)dsat_alloc_memory(alloc_size, FALSE);
        if(dsat_basic_etsi_cmn_ms_vals[subs] == NULL)
        {
          alloc_size = sizeof(dsat_basic_etsi_table_common_val);
          dsat_basic_etsi_cmn_ms_vals[subs] = (dsat_basic_etsi_table_common_val *)dsat_alloc_memory(alloc_size, FALSE);
        }
      }
    }
    break;

    case DSAT_TABLE_ETSI_EXT_ACT_SS:
    {
      
      alloc_size = sizeof(dsat_etsi_ext_act_table_ss_val_struct_type);
      dsat_etsi_ext_act_ss_vals = (dsat_etsi_ext_act_table_ss_val_struct_type *)dsat_alloc_memory(alloc_size, FALSE);
        /*Initilize val pointers to static memory*/
      dsat_etsi_ext_act_ss_vals->dsat_cusd_val[1].string_item = cusd_str_val;
      dsat_etsi_ext_act_ss_vals->dsat_cusd_val[2].string_item = cusd_dcs_val;
      dsat_etsi_ext_act_ss_vals->dsat_csdf_val = 1;
#ifdef FEATURE_DATA_TE_MT_PDP
      dsat_etsi_ext_act_ss_vals->dsat_cgans_val[1].string_item = cgans_l2p_type_val;
#endif/* FEATURE_DATA_TE_MT_PDP*/
    }
    break;
    case DSAT_TABLE_ETSI_EXT_ACT_MS:
    {
      if(DSAT_MS_MAX_SUBS == subs)
      {
        for(i = 0 ; i < max_subs; i++)
        {
          alloc_size = sizeof(dsat_etsi_ext_act_table_ms_val_struct_type);
          dsat_etsi_ext_act_ms_vals[i] = (dsat_etsi_ext_act_table_ms_val_struct_type *)dsat_alloc_memory(alloc_size, FALSE);
            /*Initilize val pointers to static memory*/
          dsat_etsi_ext_act_ms_vals[i]->dsat_cpin_val[0].string_item = cpin_pin_val[i];
          dsat_etsi_ext_act_ms_vals[i]->dsat_cpin_val[1].string_item = cpin_newpin_val[i];
          dsat_etsi_ext_act_ms_vals[i]->dsat_cops_val[2].string_item = cops_oper_val[i];
          dsat_etsi_ext_act_ms_vals[i]->dsat_cpol_val[2].string_item = cpol_oper_val[i];
          dsat_etsi_ext_act_ms_vals[i]->dsat_cops_val[4].string_item = cops_csg_id_val[i];
          if(dsat_basic_etsi_cmn_ms_vals[i] == NULL)
          {
            alloc_size = sizeof(dsat_basic_etsi_table_common_val);
            dsat_basic_etsi_cmn_ms_vals[i] = (dsat_basic_etsi_table_common_val *)dsat_alloc_memory(alloc_size, FALSE);
          }
        }
      }
      else if(subs < DSAT_MS_MAX_SUBS)
      {
        alloc_size = sizeof(dsat_etsi_ext_act_table_ms_val_struct_type);
        dsat_etsi_ext_act_ms_vals[subs] = (dsat_etsi_ext_act_table_ms_val_struct_type *)dsat_alloc_memory(alloc_size, FALSE);
            /*Initilize val pointers to static memory*/
        dsat_etsi_ext_act_ms_vals[subs]->dsat_cpin_val[0].string_item = cpin_pin_val[subs];
        dsat_etsi_ext_act_ms_vals[subs]->dsat_cpin_val[1].string_item = cpin_newpin_val[subs];
        dsat_etsi_ext_act_ms_vals[subs]->dsat_cops_val[2].string_item = cops_oper_val[subs];
        dsat_etsi_ext_act_ms_vals[subs]->dsat_cpol_val[2].string_item = cpol_oper_val[subs];
        dsat_etsi_ext_act_ms_vals[subs]->dsat_cops_val[4].string_item = cops_csg_id_val[subs];
        if(dsat_basic_etsi_cmn_ms_vals[subs] == NULL)
        {
          alloc_size = sizeof(dsat_basic_etsi_table_common_val);
          dsat_basic_etsi_cmn_ms_vals[subs] = (dsat_basic_etsi_table_common_val *)dsat_alloc_memory(alloc_size, FALSE);
        }
      }
    }
    break;
    case DSAT_TABLE_ETSI_EXT_SS:
    {
      
      alloc_size = sizeof(dsat_etsi_ext_table_ss_val_struct_type);
      dsat_etsi_ext_ss_vals = (dsat_etsi_ext_table_ss_val_struct_type *)dsat_alloc_memory(alloc_size, FALSE);
/*Initilize val pointers to static memory*/
      dsat_etsi_ext_ss_vals->dsat_cgdcont_val[1].string_item  = cgdcont_pdptype_val;
      dsat_etsi_ext_ss_vals->dsat_cgdcont_val[2].string_item  = cgdcont_apn_val;
      dsat_etsi_ext_ss_vals->dsat_cgdcont_val[3].string_item  = cgdcont_pdpaddr_val;
      
#ifdef FEATURE_SECONDARY_PDP
      dsat_etsi_ext_ss_vals->dsat_cgtft_val[3].string_item  = cgtft_addr_subnet_val;
      dsat_etsi_ext_ss_vals->dsat_cgtft_val[5].string_item  = cgtft_dst_port_val;
      dsat_etsi_ext_ss_vals->dsat_cgtft_val[6].string_item  = cgtft_src_port_val;
      dsat_etsi_ext_ss_vals->dsat_cgtft_val[8].string_item  = cgtft_tos_tclass_val;
#endif /* FEATURE_SECONDARY_PDP */

#ifdef FEATURE_DATA_WCDMA_PS
      dsat_etsi_ext_ss_vals->dsat_cgeqreq_val[8].string_item  = cgeqreq_sdu_err_val;
      dsat_etsi_ext_ss_vals->dsat_cgeqreq_val[9].string_item  = cgeqreq_res_biterr_val;
      dsat_etsi_ext_ss_vals->dsat_cgeqmin_val[8].string_item  = cgeqmin_sdu_err_val;
      dsat_etsi_ext_ss_vals->dsat_cgeqmin_val[9].string_item  = cgeqmin_res_biterr_val;
#endif /* FEATURE_DATA_WCDMA_PS */

      dsat_etsi_ext_ss_vals->dsat_cscb_val[1].string_item = cscb_mids_val;
      dsat_etsi_ext_ss_vals->dsat_cscb_val[2].string_item = cscb_dcss_val;
						
#if  defined(FEATURE_WCDMA) || defined(FEATURE_TDSCDMA)   
      dsat_etsi_ext_ss_vals->dsat_es_val[0].string_item  =  dsat_es_orig_rqst_val;
      dsat_etsi_ext_ss_vals->dsat_es_val[1].string_item  =  dsat_es_orig_fbk_val;
      dsat_etsi_ext_ss_vals->dsat_es_val[2].string_item  =  dsat_es_ans_fbk_val;

      dsat_etsi_ext_ss_vals->dsat_esa_val[1].string_item  = dsat_esa_trans_idle_val;
      dsat_etsi_ext_ss_vals->dsat_esa_val[2].string_item  = dsat_esa_framed_un_ov_val;
      dsat_etsi_ext_ss_vals->dsat_esa_val[3].string_item  = dsat_esa_hd_auto_val;
      dsat_etsi_ext_ss_vals->dsat_esa_val[7].string_item  = dsat_esa_syn2_val;
#endif /* FEATURE_WCDMA || FEATURE_TDSCDMA */						

    }
    break;
    case DSAT_TABLE_ETSI_EXT_MS:
    {
      alloc_size = sizeof(dsat_etsi_ext_table_ms_val_struct_type);
      if(DSAT_MS_MAX_SUBS == subs)
      {
        for(i = 0 ; i < max_subs; i++)
        {
          dsat_etsi_ext_ms_vals[i] = (dsat_etsi_ext_table_ms_val_struct_type *)dsat_alloc_memory(alloc_size, FALSE);
/*Initilize val pointers to static memory*/
          dsat_etsi_ext_ms_vals[i]->dsat_csca_val[0].string_item  = dsat_csca_sca_val[i];
          dsat_etsi_ext_ms_vals[i]->dsat_csca_val[1].string_item  = dsat_csca_tosca_val[i];
          dsat_etsi_ext_ms_vals[i]->dsat_csmp_val[0].string_item =  csmp_fo_val[i];
          dsat_etsi_ext_ms_vals[i]->dsat_csmp_val[1].string_item =  csmp_vp_val[i];
        }
      }
      else if(subs < DSAT_MS_MAX_SUBS)
      {
        dsat_etsi_ext_ms_vals[subs] = (dsat_etsi_ext_table_ms_val_struct_type *)dsat_alloc_memory(alloc_size, FALSE);
 /*Initilize val pointers to static memory*/
        dsat_etsi_ext_ms_vals[subs]->dsat_csca_val[0].string_item  = dsat_csca_sca_val[subs];
        dsat_etsi_ext_ms_vals[subs]->dsat_csca_val[1].string_item  = dsat_csca_tosca_val[subs];
        dsat_etsi_ext_ms_vals[subs]->dsat_csmp_val[0].string_item =  csmp_fo_val[subs];
        dsat_etsi_ext_ms_vals[subs]->dsat_csmp_val[1].string_item =  csmp_vp_val[subs];
      }
    }
    break;
#ifdef FEATURE_DATA_IS707 
   case DSAT_TABLE_707_PARA_EXT:
   {
     alloc_size = sizeof(dsat707_ext_para_table_ss_val_struct_type);
     dsat707_ext_ss_vals = (dsat707_ext_para_table_ss_val_struct_type *)dsat_alloc_memory(alloc_size, FALSE);
   }
   break;
   case DSAT_TABLE_707_ACTION_EXT:
   {
#ifdef FEATURE_DSAT_CDMA_PIN 
     if(NULL == dsat707_ext_act_cmn_ss_vals)
     {
       alloc_size = sizeof(dsat707_ext_vend_act_table_common_val);
       dsat707_ext_act_cmn_ss_vals = (dsat707_ext_vend_act_table_common_val *)dsat_alloc_memory(alloc_size, FALSE);
       dsat707_ext_act_cmn_ss_vals->dsat707_cpin_val[0].string_item  = dsat707_cpin_pin_val;
       dsat707_ext_act_cmn_ss_vals->dsat707_cpin_val[1].string_item  = dsat707_cpin_newpin_val;
     }
#endif /* FEATURE_DSAT_CDMA_PIN */
   }
   break;
   case DSAT_TABLE_707_FAX:
   {
     alloc_size = sizeof(dsat707_fax_table_ss_val_struct_type);
     dsat707_fax_ss_vals = (dsat707_fax_table_ss_val_struct_type *)dsat_alloc_memory(alloc_size, FALSE);
   }
   break;
#ifdef FEATURE_HDR
#ifdef FEATURE_HDR_AT_TEST_ONLY 
   case DSAT_TABLE_707_HDR:
   {
      alloc_size = sizeof(dsat707_hdr_table_ss_val_struct_type);
      dsat707_hdr_ss_vals = (dsat707_hdr_table_ss_val_struct_type *)dsat_alloc_memory(alloc_size, FALSE);
    }
    break;
#endif /* FEATURE_HDR_AT_TEST_ONLY */
#endif /* FEATURE_HDR */
#ifdef FEATURE_CDMA_SMS
    case DSAT_TABLE_707_SMS:
    {
      alloc_size = sizeof(dsat707_sms_table_ss_val_struct_type);
      dsat707_sms_ss_vals = (dsat707_sms_table_ss_val_struct_type *)dsat_alloc_memory(alloc_size, FALSE);
      dsat707_sms_ss_vals->dsat707sms_qcsmp_val[2].string_item  = qcsmp_vp_val;
      dsat707_sms_ss_vals->dsat707sms_qcsmp_val[4].string_item  = qcsmp_tdd_val;
    }
    break;
#endif /* FEATURE_CDMA_SMS*/
    case DSAT_TABLE_707_VENDOR_PARA:
    {
      alloc_size = sizeof(dsat707_vend_para_table_ss_val_struct_type);
      dsat707_vend_para_ss_vals = (dsat707_vend_para_table_ss_val_struct_type *)dsat_alloc_memory(alloc_size, FALSE);
    }
    break;
    case DSAT_TABLE_707_VENDOR_ACTION:
    {
      alloc_size = sizeof(dsat707_vend_act_table_ss_val_struct_type);
      dsat707_vend_act_ss_vals = (dsat707_vend_act_table_ss_val_struct_type *)dsat_alloc_memory(alloc_size, FALSE);
#if defined(FEATURE_DSAT_SP_CMDS)
      dsat707_vend_act_ss_vals->dsat_spc_val.string_item  = spc_val;
      dsat707_vend_act_ss_vals->dsat_unlock_val.string_item  =  unlock_val;
      dsat707_vend_act_ss_vals->dsat_lock_val.string_item  = lock_val;
      dsat707_vend_act_ss_vals->dsat_lock_chg_val[0].string_item  = old_lock_val;
      dsat707_vend_act_ss_vals->dsat_lock_chg_val[1].string_item  = new_lock_val;
#endif /*FEATURE_DSAT_SP_CMDS*/
#ifdef FEATURE_DSAT_CDMA_PIN 
      if(NULL == dsat707_ext_act_cmn_ss_vals)
      {
        alloc_size = sizeof(dsat707_ext_vend_act_table_common_val);
        dsat707_ext_act_cmn_ss_vals = (dsat707_ext_vend_act_table_common_val *)dsat_alloc_memory(alloc_size, FALSE);
        dsat707_ext_act_cmn_ss_vals->dsat707_cpin_val[0].string_item  = dsat707_cpin_pin_val;
        dsat707_ext_act_cmn_ss_vals->dsat707_cpin_val[1].string_item  = dsat707_cpin_newpin_val;
      }
#endif /* FEATURE_DSAT_CDMA_PIN */
    }
    break;
#endif/* FEATURE_DATA_IS707 */
#ifdef FEATURE_BCMCS
    case DSAT_TABLE_707_1XHDR:
    {
      alloc_size = sizeof(dsat707_vend_1xhdr_table_ss_val_struct_type);
      dsat707_1xhdr_ss_vals = (dsat707_vend_1xhdr_table_ss_val_struct_type *)dsat_alloc_memory(alloc_size, FALSE);
    }
    break;
#endif /* FEATURE_BCMCS*/
    case DSAT_SS_PH_SS_VALS:
    {
      alloc_size = sizeof(dsatcmif_servs_state_ss_info);
      dsatcmif_ss_ph_val.ss = (dsatcmif_servs_state_ss_info *)dsat_alloc_memory(alloc_size, FALSE);
      dsatcmif_initialize_to_default(DSAT_SS_PH_SS_VALS,dsatcmif_ss_ph_val.ss);
    }
    break;
    case DSAT_SS_PH_MS_VALS:
    {
      alloc_size = sizeof(dsatcmif_servs_state_ms_info);
      if(DSAT_MS_MAX_SUBS == subs)
      {
        for(i = 0 ; i < max_subs; i++)
        {
          dsatcmif_ss_ph_val.ms[i] = (dsatcmif_servs_state_ms_info *)dsat_alloc_memory(alloc_size, FALSE);
          dsatcmif_initialize_to_default(DSAT_SS_PH_MS_VALS,dsatcmif_ss_ph_val.ms[i]);
        }
      }else if(subs < DSAT_MS_MAX_SUBS)
      {
        dsatcmif_ss_ph_val.ms[subs] = (dsatcmif_servs_state_ms_info *)dsat_alloc_memory(alloc_size, FALSE);
        dsatcmif_initialize_to_default(DSAT_SS_PH_MS_VALS,dsatcmif_ss_ph_val.ms[subs]);
      }
    }
    break;
    case DSAT_CALL_SS_VALS:
    {
      alloc_size = sizeof(dsatetsicall_call_state_ss_info);
      dsatcmif_call_val.ss = (dsatetsicall_call_state_ss_info *)dsat_alloc_memory(alloc_size, FALSE);
      dsatcmif_initialize_to_default(DSAT_CALL_SS_VALS,dsatcmif_call_val.ss);
    }
    break;
    case DSAT_CALL_DA_VALS:
    {
      alloc_size = sizeof(dsatetsicall_call_state_da_info);
      if(DSAT_MS_MAX_SUBS == subs)
      {
        for(i = 0 ; i < max_act_subs; i++)
        {
          dsatcmif_call_val.da[i] = (dsatetsicall_call_state_da_info *)dsat_alloc_memory(alloc_size, FALSE);
          dsatcmif_initialize_to_default(DSAT_CALL_DA_VALS,dsatcmif_call_val.da[i]);
        }
      }else if(subs < MAX_ACTIVE_SUBS)
      {
        dsatcmif_call_val.da[subs] = (dsatetsicall_call_state_da_info *)dsat_alloc_memory(alloc_size, FALSE);
        dsatcmif_initialize_to_default(DSAT_CALL_DA_VALS,dsatcmif_call_val.da[subs]);
      }
    }
    break;
    case DSAT_CALL_MS_VALS:
    {
      
      alloc_size = sizeof(dsatetsicall_call_state_ms_info);
      if(DSAT_MS_MAX_SUBS == subs)
      {
        for(i = 0 ; i < max_subs; i++)
        {
          dsatcmif_call_val.ms[i] = (dsatetsicall_call_state_ms_info *)dsat_alloc_memory(alloc_size, FALSE);
          dsatcmif_initialize_to_default(DSAT_CALL_MS_VALS,dsatcmif_call_val.ms[i]);
        }
      }else if(subs < DSAT_MS_MAX_SUBS)
      {
        dsatcmif_call_val.ms[subs] = (dsatetsicall_call_state_ms_info *)dsat_alloc_memory(alloc_size, FALSE);
        dsatcmif_initialize_to_default(DSAT_CALL_MS_VALS,dsatcmif_call_val.ms[subs]);
      }
    }
    break;
    case DSAT_SUPS_SS_VALS:
    {
      alloc_size = sizeof(dsatetsicmif_sups_state_ss_info);
      dsatcmif_sups_val.ss = (dsatetsicmif_sups_state_ss_info *)dsat_alloc_memory(alloc_size, FALSE);
      dsatcmif_initialize_to_default(DSAT_SUPS_SS_VALS,dsatcmif_sups_val.ss);
    }
    break;
    case DSAT_SUPS_MS_VALS:
    {
      alloc_size = sizeof(dsatetsicmif_sups_state_ms_info);
      if(DSAT_MS_MAX_SUBS == subs)
      {
        for(i = 0 ; i < max_subs; i++)
        {
          dsatcmif_sups_val.ms[i] = (dsatetsicmif_sups_state_ms_info *)dsat_alloc_memory(alloc_size, FALSE);
          dsatcmif_initialize_to_default(DSAT_SUPS_MS_VALS,dsatcmif_sups_val.ms[i]);
        }
      }else if(subs < DSAT_MS_MAX_SUBS)
      {
        dsatcmif_sups_val.ms[subs] = (dsatetsicmif_sups_state_ms_info *)dsat_alloc_memory(alloc_size, FALSE);
        dsatcmif_initialize_to_default(DSAT_SUPS_MS_VALS,dsatcmif_sups_val.ms[subs]);
      }
    }
    break;
    case DSAT_MMGSDI_SS_VALS :
    {
      alloc_size = sizeof(dsatme_mmgsdi_state_ss_info);
      dsatme_mmgsdi_val.ss = (dsatme_mmgsdi_state_ss_info *)dsat_alloc_memory(alloc_size, FALSE);
      dsatme_initialize_to_default(DSAT_MMGSDI_SS_VALS,dsatme_mmgsdi_val.ss);
    }
    break;
    case DSAT_MMGSDI_MS_VALS:
    {
      alloc_size = sizeof(dsatme_mmgsdi_state_ms_info);
      if(DSAT_MS_MAX_SUBS == subs)
      {
        for(i = 0 ; i < max_subs; i++)
        {
          dsatme_mmgsdi_val.ms[i] = (dsatme_mmgsdi_state_ms_info *)dsat_alloc_memory(alloc_size, FALSE);
          dsatme_initialize_to_default(DSAT_MMGSDI_MS_VALS,dsatme_mmgsdi_val.ms[i]);
        }
      }else if(subs < DSAT_MS_MAX_SUBS)
      {
        dsatme_mmgsdi_val.ms[subs] = (dsatme_mmgsdi_state_ms_info *)dsat_alloc_memory(alloc_size, FALSE);
        dsatme_initialize_to_default(DSAT_MMGSDI_MS_VALS,dsatme_mmgsdi_val.ms[subs]);
      }
    }
    break;
    case DSAT_PBM_SS_VALS:
    {
        /*PBM info*/
      alloc_size = sizeof(dsatme_pbm_state_ss_info);
      dsatme_pbm_val.ss = (dsatme_pbm_state_ss_info *)dsat_alloc_memory(alloc_size, FALSE);
      dsatme_initialize_to_default(DSAT_PBM_SS_VALS,dsatme_pbm_val.ss);
    }
    break;
    case DSAT_COMMON_SS_VALS:
    {
        /*Common info*/
      alloc_size = sizeof(dsat_common_state_ss_info);
      dsat_common_val.ss = (dsat_common_state_ss_info *)dsat_alloc_memory(alloc_size, FALSE);
      dsatme_initialize_to_default(DSAT_COMMON_SS_VALS,dsat_common_val.ss);
    }
    break;
    case DSAT_MS_MD_VALS:
    {
      alloc_size = sizeof(dsatetsipkt_gprs_msg_ms_info);
      if(DSAT_MS_MAX_SUBS == subs)
      {
        for(i = 0 ; i < max_subs; i++)
        {
          dsatetsipkt_gprs_val.ms[i] = (dsatetsipkt_gprs_msg_ms_info *)dsat_alloc_memory(alloc_size, FALSE);
          dsatme_initialize_to_default(DSAT_MS_MD_VALS,dsatetsipkt_gprs_val.ms[i]);
        }
      }else if(subs < DSAT_MS_MAX_SUBS)
      {
        dsatetsipkt_gprs_val.ms[subs] = (dsatetsipkt_gprs_msg_ms_info *)dsat_alloc_memory(alloc_size, FALSE);
        dsatme_initialize_to_default(DSAT_MS_MD_VALS,dsatetsipkt_gprs_val.ms[subs]);
      }
    }
    break;
    case DSAT_PANDING_VARIABLE:
    {
    }
    break;
    default:
    break;
  }
  return result;
}
/*===========================================================================
FUNCTION  DSATUTIL_DEFAULT_DESTRUCTOR

DESCRIPTION
  This function will de allocate  the dynamic memory.
  
DEPENDENCIES
  None

RETURNS
  TRUE  - SUCCESS
  FALSE - FAILURE

SIDE EFFECTS
  None

===========================================================================*/

boolean dsatutil_default_destructor
(
  dsat_mem_type           mem_type,
  dsat_memory_subs_type    subs
)
{
  boolean                result       = DSAT_SUCCESS;
  dsat_num_item_type     i            = 0;
  dsat_num_item_type     max_subs     = 1;
  dsat_num_item_type     max_act_subs = 1;
  
  ALLOWED_MAX_SUB(max_subs)
  ALLOWED_MAX_ACTIVE(max_act_subs)

  switch(mem_type)
  {
    case DSAT_ALL_ATCOP_STRUCT_TABLE:
    {
      DSAT_MEMFREE(dsat_basic_vals);
      DSAT_MEMFREE(dsat_basic_act_vals);
      DSAT_MEMFREE(dsat_basic_sreg_vals);
      DSAT_MEMFREE(dsat_basic_ext_vals);
      DSAT_MEMFREE(dsat_basic_vend_ss_vals);
      for(i = 0 ; i < max_subs; i++)
      {
        DSAT_MEMFREE(dsat_basic_vend_ms_vals[i]);
      }
      DSAT_MEMFREE(dsat_etsi_ext_act_ss_vals);
      for(i = 0 ; i < max_subs; i++)
      {
        DSAT_MEMFREE(dsat_etsi_ext_act_ms_vals[i]);
      }
      DSAT_MEMFREE(dsat_etsi_ext_ss_vals);
      for(i = 0 ; i < max_subs; i++)
      {
        DSAT_MEMFREE(dsat_etsi_ext_ms_vals[i]);
      }
#ifdef FEATURE_DATA_IS707
      DSAT_MEMFREE(dsat707_ext_ss_vals);
      DSAT_MEMFREE(dsat707_ext_act_cmn_ss_vals);
      DSAT_MEMFREE(dsat707_fax_ss_vals);
#ifdef FEATURE_HDR
#ifdef FEATURE_HDR_AT_TEST_ONLY 
      DSAT_MEMFREE(dsat707_hdr_ss_vals);
#endif /* FEATURE_HDR_AT_TEST_ONLY */
#endif /* FEATURE_HDR */
#ifdef FEATURE_CDMA_SMS
      DSAT_MEMFREE(dsat707_sms_ss_vals);
#endif/* FEATURE_CDMA_SMS*/
      DSAT_MEMFREE(dsat707_vend_para_ss_vals);
      DSAT_MEMFREE(dsat707_vend_act_ss_vals);
#endif /* FEATURE_DATA_IS707*/
#ifdef FEATURE_BCMCS
      DSAT_MEMFREE(dsat707_1xhdr_ss_vals);
#endif /* FEATURE_BCMCS*/
       /*Freeing CM SS vals*/
      DSAT_MEMFREE(dsatcmif_ss_ph_val.ss);
      for(i = 0 ; i < max_subs; i++)
      {
        DSAT_MEMFREE(dsatcmif_ss_ph_val.ms[i]);
      }
      
      DSAT_MEMFREE(dsatcmif_call_val.ss);
      for(i = 0 ; i < max_subs; i++)
      {
        DSAT_MEMFREE(dsatcmif_call_val.ms[i]);
      }
      for(i = 0 ; i < max_act_subs; i++)
      {
        DSAT_MEMFREE(dsatcmif_call_val.da[i]);
      }
      DSAT_MEMFREE(dsatcmif_sups_val.ss);
      for(i = 0 ; i < max_subs; i++)
      {
        DSAT_MEMFREE(dsatcmif_sups_val.ms[i]);
      }
      DSAT_MEMFREE(dsatme_mmgsdi_val.ss);
      for(i = 0 ; i < max_subs; i++)
      {
        DSAT_MEMFREE(dsatme_mmgsdi_val.ms[i]);
      }
      DSAT_MEMFREE(dsatme_pbm_val.ss);
      DSAT_MEMFREE(dsat_common_val.ss);
      for(i = 0 ; i < max_subs; i++)
      {
#ifdef FEATURE_DSAT_ETSI_DATA
        /* Initialize queues for +CGEREP command */
        /*  Initialize the GPRS events message queue and the free message  */
        /*  queue, and link the message items onto the free message queue. */
    
        (void) q_destroy(&dsatetsipkt_gprs_val.ms[i]->etsipkt_gprs_msg_q );
        (void) q_destroy(&dsatetsipkt_gprs_val.ms[i]->etsipkt_gprs_msg_free_q);
#endif /* FEATURE_DSAT_ETSI_DATA */

        DSAT_MEMFREE(dsatetsipkt_gprs_val.ms[i]);
      }
    }
    break;
    case DSAT_TABLE_BASIC:
    {
      DSAT_MEMFREE(dsat_basic_vals);
    }
    break;
    case DSAT_TABLE_BASIC_ACTION:
    {
      DSAT_MEMFREE(dsat_basic_act_vals);
    }
    break;
    case DSAT_TABLE_SREG:
    {
      DSAT_MEMFREE(dsat_basic_sreg_vals);
    }
    break;
    case DSAT_TABLE_EXT:
    {
      DSAT_MEMFREE(dsat_basic_ext_vals);
    }
    break;
    case DSAT_TABLE_VEND_EXT_SS:
    {
      DSAT_MEMFREE(dsat_basic_vend_ss_vals);
    }
    break;
    case DSAT_TABLE_VEND_EXT_MS:
    {
      if(DSAT_MS_MAX_SUBS == subs)
      {
        for(i = 0 ; i < max_subs; i++)
        {
          DSAT_MEMFREE(dsat_basic_vend_ms_vals[i]);
        }
      }
      else if(subs < DSAT_MS_MAX_SUBS)
      {
        DSAT_MEMFREE(dsat_basic_vend_ms_vals[subs]);
      }
    }
    break;
    case DSAT_TABLE_ETSI_EXT_ACT_SS:
    {
      DSAT_MEMFREE(dsat_etsi_ext_act_ss_vals);
    }
    break;
    case DSAT_TABLE_ETSI_EXT_ACT_MS:
    {
      if(DSAT_MS_MAX_SUBS == subs)
      {
        for(i = 0 ; i < max_subs; i++)
        {
          DSAT_MEMFREE(dsat_etsi_ext_act_ms_vals[i]);
        }
      }
      else if(subs < DSAT_MS_MAX_SUBS)
      {
        DSAT_MEMFREE(dsat_etsi_ext_act_ms_vals[subs]);
      }
    }
    break;
    case DSAT_TABLE_ETSI_EXT_SS:
    {
      DSAT_MEMFREE(dsat_etsi_ext_ss_vals);
    }
    break;
    case DSAT_TABLE_ETSI_EXT_MS:
    {
      if(DSAT_MS_MAX_SUBS == subs)
      {
        for(i = 0 ; i < max_subs; i++)
        {
          DSAT_MEMFREE(dsat_etsi_ext_ms_vals[i]);
        }
      }
      else if(subs < DSAT_MS_MAX_SUBS)
      {
        DSAT_MEMFREE(dsat_etsi_ext_ms_vals[subs]);
      }
    }
    break;
#ifdef FEATURE_DATA_IS707
    case DSAT_TABLE_707_PARA_EXT:
    {
      DSAT_MEMFREE(dsat707_ext_ss_vals);
    }
    case DSAT_TABLE_707_ACTION_EXT:
    {
     /* TBD:-Common memory will not free until ref count become zero */
     
    }
    case DSAT_TABLE_707_FAX:
    {
      DSAT_MEMFREE(dsat707_fax_ss_vals);
    }
    case DSAT_TABLE_707_HDR:
    {
#ifdef FEATURE_HDR
#ifdef FEATURE_HDR_AT_TEST_ONLY 
      DSAT_MEMFREE(dsat707_hdr_ss_vals);
#endif /* FEATURE_HDR_AT_TEST_ONLY */
#endif /* FEATURE_HDR */
    }
#ifdef FEATURE_CDMA_SMS
    case DSAT_TABLE_707_SMS:
    {
      DSAT_MEMFREE(dsat707_sms_ss_vals);
    }
#endif/* FEATURE_CDMA_SMS*/
    case DSAT_TABLE_707_VENDOR_PARA:
    {
      DSAT_MEMFREE(dsat707_vend_para_ss_vals);
    }
    case DSAT_TABLE_707_VENDOR_ACTION:
    {
      DSAT_MEMFREE(dsat707_vend_act_ss_vals);
    }
#endif /*#ifdef FEATURE_DATA_IS707*/
#ifdef FEATURE_BCMCS
    case DSAT_TABLE_707_1XHDR:
    {
      DSAT_MEMFREE(dsat707_1xhdr_ss_vals);
    }
#endif /*FEATURE_BCMCS*/
    
    case DSAT_SS_PH_SS_VALS:
    {
      DSAT_MEMFREE(dsatcmif_ss_ph_val.ss);
    }
    break;
    case DSAT_SS_PH_MS_VALS:
    {
      if(DSAT_MS_MAX_SUBS == subs)
      {
        for(i = 0 ; i < max_subs; i++)
        {
           DSAT_MEMFREE(dsatcmif_ss_ph_val.ms[i]);
        }
      }
      else if(subs < DSAT_MS_MAX_SUBS)
      {
         DSAT_MEMFREE(dsatcmif_ss_ph_val.ms[subs ]);
      }
    }
    break;
    case DSAT_CALL_SS_VALS:
    {
      DSAT_MEMFREE(dsatcmif_call_val.ss);
    }
    break;
    case DSAT_CALL_MS_VALS:
    {
      if(DSAT_MS_MAX_SUBS == subs)
      {
        for(i = 0 ; i < max_subs; i++)
        {
           DSAT_MEMFREE(dsatcmif_call_val.ms[i]);
        }
      }
      else if(subs < DSAT_MS_MAX_SUBS)
      {
         DSAT_MEMFREE(dsatcmif_call_val.ms[subs ]);
      }
    }
    break;
    case DSAT_CALL_DA_VALS:
    {
      if(DSAT_MS_MAX_SUBS == subs)
      {
        for(i = 0 ; i < max_act_subs; i++)
        {
           DSAT_MEMFREE(dsatcmif_call_val.da[i]);
        }
      }
      else if(subs < MAX_ACTIVE_SUBS )
      {
         DSAT_MEMFREE(dsatcmif_call_val.da[subs ]);
      }
    }
    break;
    case DSAT_SUPS_SS_VALS:
    {
      DSAT_MEMFREE(dsatcmif_sups_val.ss);
    }
    break;
    case DSAT_SUPS_MS_VALS:
    {
      if(DSAT_MS_MAX_SUBS == subs)
      {
        for(i = 0 ; i < max_subs; i++)
        {
           DSAT_MEMFREE(dsatcmif_sups_val.ms[i]);
        }
      }
      else if(subs < DSAT_MS_MAX_SUBS)
      {
         DSAT_MEMFREE(dsatcmif_sups_val.ms[subs ]);
      }
    }
    break;
    case DSAT_MMGSDI_SS_VALS:
    {
      DSAT_MEMFREE(dsatme_mmgsdi_val.ss);
    }
    break;
    case DSAT_MMGSDI_MS_VALS:
    {
      if(DSAT_MS_MAX_SUBS == subs)
      {
        for(i = 0 ; i < max_subs; i++)
        {
           DSAT_MEMFREE(dsatme_mmgsdi_val.ms[i]);
        }
      }
      else if(subs < DSAT_MS_MAX_SUBS)
      {
         DSAT_MEMFREE(dsatme_mmgsdi_val.ms[subs ]);
      }
    }
    break;
    case DSAT_PBM_SS_VALS:
    {
      DSAT_MEMFREE(dsatme_pbm_val.ss);
    }
    break;
    case DSAT_COMMON_SS_VALS:
    {
      DSAT_MEMFREE(dsat_common_val.ss);
    }
    break;
    case DSAT_MS_MD_VALS:
    {
      if(DSAT_MS_MAX_SUBS == subs)
      {
        for(i = 0 ; i < max_subs; i++)
        {
#ifdef FEATURE_DSAT_ETSI_DATA
           /* Initialize queues for +CGEREP command */
           /*  Initialize the GPRS events message queue and the free message  */
           /*  queue, and link the message items onto the free message queue. */
           
           (void) q_destroy(&dsatetsipkt_gprs_val.ms[i]->etsipkt_gprs_msg_q );
           (void) q_destroy(&dsatetsipkt_gprs_val.ms[i]->etsipkt_gprs_msg_free_q);
#endif /* FEATURE_DSAT_ETSI_DATA */

           DSAT_MEMFREE(dsatetsipkt_gprs_val.ms[i]);
        }
      }
      else if(subs < DSAT_MS_MAX_SUBS)
      {
#ifdef FEATURE_DSAT_ETSI_DATA
         /* Initialize queues for +CGEREP command */
         /*  Initialize the GPRS events message queue and the free message  */
         /*  queue, and link the message items onto the free message queue. */
         
         (void) q_destroy(&dsatetsipkt_gprs_val.ms[subs]->etsipkt_gprs_msg_q );
         (void) q_destroy(&dsatetsipkt_gprs_val.ms[subs]->etsipkt_gprs_msg_free_q);
#endif /* FEATURE_DSAT_ETSI_DATA */
         DSAT_MEMFREE(dsatetsipkt_gprs_val.ms[subs ]);
      }
    }
    break;
    case DSAT_PANDING_VARIABLE:
    {
    }
    break;
    default:
    break;
  }
  return result;
}
/*===========================================================================
FUNCTION  DSATUTIL_GET_BASE_ADDR

DESCRIPTION
  This function will allocate,deallocate and get the dynamic memory.
  
DEPENDENCIES
  None

RETURNS
  TRUE  - SUCCESS
  FALSE - FAILURE

SIDE EFFECTS
  None

===========================================================================*/

boolean dsatutil_get_base_addr
(
  dsat_mem_type           mem_type,
  void                   **resp,
  dsat_memory_subs_type    subs
)
{
  boolean                result       = DSAT_SUCCESS;
  dsat_num_item_type     max_subs     = 1;
  dsat_num_item_type     max_act_subs = 1;
  
  ALLOWED_MAX_SUB(max_subs)
  ALLOWED_MAX_ACTIVE(max_act_subs)
  *resp = NULL;
  
  switch(mem_type)
  {
    case DSAT_SS_PH_SS_VALS:
      {
        if(NULL == dsatcmif_ss_ph_val.ss)
        {
          dsatutil_default_constructor(mem_type,subs);
        }
        *resp = dsatcmif_ss_ph_val.ss;
      }
    break;
    case DSAT_SS_PH_MS_VALS:
      {
#ifndef FEATURE_SGLTE
        if((subs <  DSAT_MS_MAX_SUBS) && ((dsat_num_item_type)subs < max_subs))
#else
        if(subs <  DSAT_MS_MAX_SUBS)
#endif /* FEATURE_SGLTE */
        {
          if(NULL == dsatcmif_ss_ph_val.ms[subs])
          {
            dsatutil_default_constructor(mem_type,subs);
          }
          *resp = dsatcmif_ss_ph_val.ms[subs];
        }
      }
    break;
    case DSAT_CALL_SS_VALS:
     {
       if(NULL == dsatcmif_call_val.ss)
       {
         dsatutil_default_constructor(mem_type,subs);
       }
       *resp = dsatcmif_call_val.ss;
     }
    break;
    case DSAT_CALL_DA_VALS:
      {
        if((subs <  MAX_ACTIVE_SUBS) && ((dsat_num_item_type)subs < max_act_subs))
        {
          if(NULL == dsatcmif_call_val.da[subs])
          {
            dsatutil_default_constructor(mem_type,subs);
          }
          *resp = dsatcmif_call_val.da[subs];
        }
      }
    break;
    case DSAT_CALL_MS_VALS:
      {
        if((subs <  DSAT_MS_MAX_SUBS) && ((dsat_num_item_type)subs < max_subs))
        {
          if(NULL == dsatcmif_call_val.ms[subs])
          {
            dsatutil_default_constructor(mem_type,subs);
          }
          *resp = dsatcmif_call_val.ms[subs];
        }
      }
    break;
    case DSAT_SUPS_SS_VALS:
      {
        if(NULL == dsatcmif_sups_val.ss)
        {
          dsatutil_default_constructor(mem_type,subs);
        }
        *resp = dsatcmif_sups_val.ss;
      }
    break;
    case DSAT_SUPS_MS_VALS:
      {
        if((subs <  DSAT_MS_MAX_SUBS) && ((dsat_num_item_type)subs < max_subs))
        {
          if(NULL == dsatcmif_sups_val.ms[subs])
          {
            dsatutil_default_constructor(mem_type,subs);
          }
          *resp = dsatcmif_sups_val.ms[subs];
        }
      }
    break;
    case DSAT_MMGSDI_SS_VALS:
      {
        if(NULL == dsatme_mmgsdi_val.ss)
        {
          dsatutil_default_constructor(mem_type,subs);
        }
        *resp = dsatme_mmgsdi_val.ss;
      }
    break;
    case DSAT_MMGSDI_MS_VALS:
      {
        if((subs <  DSAT_MS_MAX_SUBS) && ((dsat_num_item_type)subs < max_subs))
        {
          if(NULL == dsatme_mmgsdi_val.ms[subs])
          {
            dsatutil_default_constructor(mem_type,subs);
          }
          *resp = dsatme_mmgsdi_val.ms[subs];
        }
      }
    break;
    case DSAT_PBM_SS_VALS:
      {
        if(NULL == dsatme_pbm_val.ss)
        {
          dsatutil_default_constructor(mem_type,subs);
        }
        *resp = dsatme_pbm_val.ss;
      }
    break;
    case DSAT_COMMON_SS_VALS:
      {
        if(NULL == dsat_common_val.ss)
        {
          dsatutil_default_constructor(mem_type,subs);
        }
        *resp = dsat_common_val.ss;
      }
    break;
    case DSAT_MS_MD_VALS:
    {
      if((subs <  DSAT_MS_MAX_SUBS) && ((dsat_num_item_type)subs < max_subs))
      {
        if(NULL == dsatetsipkt_gprs_val.ms[subs])
        {
          dsatutil_default_constructor(mem_type,subs);
        }
        *resp = dsatetsipkt_gprs_val.ms[subs];
      }
    }
    break;
    default:
    break;
  }
  if(*resp == NULL)
  {
    DS_AT_MSG2_HIGH("Return value NULL %d mem type %d subs",mem_type,subs);
    result =  DSAT_FAILURE;
  }
  return result;
}/*dsatutil_get_base_addr*/

/*===========================================================================

FUNCTION DSATUTIL_VALUE_GET

DESCRIPTION
  This FUNCTION copy value from source pointer into destination pointer .  

DEPENDENCIES
  None.
RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
void * dsatutil_value_get
(
  const void                    *dest,                           /*  Destination pointer*/
  dsati_value_info_struct_type  *value_info         /*  value information*/
)
{
  dsat_mixed_param_val_type * val_list;
  dsat_string_item_type     * val_string ;
  dsat_num_item_type *element;

  switch(value_info->val_type)
  {
    /* Single number Type*/
  case NUM_TYPE:
    element = (dsat_num_item_type *)dest;
    return (void *)(*(element + (value_info ->index)));
    
    /* Array Type or Struct Type */
  case STR_TYPE:
    return ((void *)dest);

    /* Array of string*/
  case ARRAY_OF_STR_TYPE :
    /* TO DO*/   
    break;
    
  case MIX_NUM_TYPE :
    val_list =  (dsat_mixed_param_val_type *) dest;
    return (void *)(val_list[value_info->index].num_item);

    /* Mixed string type*/
  case MIX_STR_TYPE :
    val_list =  (dsat_mixed_param_val_type *) dest;
    val_string = val_list[value_info->index].string_item;
    return ((void *)val_string);

  case MIX_INDEX_TYPE:
    val_list =  (dsat_mixed_param_val_type *) dest;
    return (void *)( val_list + value_info->index );

  default:
    break;
  }
  return NULL;
} /*dsatutil_value_get*/
/*===========================================================================

FUNCTION DSATUTIL_GET_PTR_TO_VALUE

DESCRIPTION
  This FUNCTION take entry index of parse table and extract base address of table .
  according base table it will  search entry in the table, if record found then return
  value_ptr otherwise return NULL.

DEPENDENCIES
  None.
RETURN VALUE
  Return value pointer . 

SIDE EFFECTS
 None.
===========================================================================*/
 void * dsatutil_get_ptr_to_value
(
  dsat_num_item_type             cmd_id,            /*  entry index in parse table  */
  dsat_num_item_type             apps_id,           /* subscription ID*/
  dsati_value_info_struct_type  *value_info         /*  value information*/
)
{
  void * ret_val = NULL;
  ret_val = dsatutil_get_val_from_cmd_id(cmd_id,apps_id);
  if(ret_val !=  NULL)
  {
    ret_val = dsatutil_value_get(ret_val ,value_info);
  }
  return ret_val;
} /*  dsatutil_get_ptr_to_value  */
/*===========================================================================

FUNCTION DSATUTIL_VALUE_COPY

DESCRIPTION
  This FUNCTION copy value from source pointer into destination pointer .  

DEPENDENCIES
  None.
RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
void dsatutil_value_copy
(
  const void                    *dest,                           /*  Destination pointer*/
  dsati_value_info_struct_type  *value_info         /*  value information*/
)
{
  dsat_mixed_param_val_type * val_list;
  dsat_string_item_type     * val_string ;
  switch(value_info->val_type)
  {
    /* Single number Type*/
    case NUM_TYPE:
      ((dsat_num_item_type *)dest)[value_info->index]=(dsat_num_item_type)value_info->val_ptr;
      break;
    /* Array Type or Struct Type */
    case STR_TYPE:
      (void) dsatutil_memscpy((void*)dest, value_info->size,(void*)value_info->val_ptr,value_info->size); 
      break;
    /* Array of string*/
    case ARRAY_OF_STR_TYPE :
       /*Currently leave it implement is later*/   
      break;
    /* Mixed number type*/
    case MIX_NUM_TYPE :
       val_list =  (dsat_mixed_param_val_type *) dest;
       val_list[value_info->index].num_item = (dsat_num_item_type)value_info->val_ptr;
       break;
    /* Mixed string type*/
    case MIX_STR_TYPE :
      val_list =  (dsat_mixed_param_val_type *) dest;
      val_string = val_list[value_info->index].string_item;
      (void) dsatutil_memscpy((void*)val_string, value_info->size,(void*)value_info->val_ptr,value_info->size);
      break;
    default:
      DS_AT_MSG0_HIGH("Default case");
      break;
   }
} /*dsatutil_value_copy*/
/*===========================================================================

FUNCTION DSATUTIL_SET_VALUE

DESCRIPTION
  This FUNCTION take entry index of parse table and extract base address of table .
  according base table it will  search entry in  table, if record found then typecast
  value_ptr into value_type and set it into table value pointer .  

DEPENDENCIES
  None.
RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
void dsatutil_set_value
(
  dsat_num_item_type             cmd_id,           /*  entry index in parse table  */
  dsat_num_item_type             apps_id,          /*  subscription ID*/ 
  dsati_value_info_struct_type  *value_info        /*  value information*/
)
{
  void * ret_val = NULL;

  ret_val = dsatutil_get_val_from_cmd_id(cmd_id,apps_id);
  if(ret_val !=  NULL)
  {
    dsatutil_value_copy(ret_val ,value_info);
    return ;
  }
} /*  dsatutil_set_value  */
 /*===========================================================================
 
 FUNCTION dsatutil_get_val
 
 DESCRIPTION
   This FUNCTION take command index and return corresponding value stored in table.  
 
 DEPENDENCIES
   None.
 RETURN VALUE
   None
 
 SIDE EFFECTS
   None.
 ===========================================================================*/

 void * dsatutil_get_val
(
  dsat_num_item_type       cmd_id,            /*  entry index in parse table  */
  dsat_num_item_type       apps_id,           /* subscription ID*/
  dsat_num_item_type       index,
  value_type_enum_type     val_type
)
{
  dsati_value_info_struct_type val_info;
  void                        *return_val = NULL;
  memset((void *)&val_info,0,sizeof(val_info));
  val_info.index = index;
  val_info.val_type = val_type;
  return_val = ((void *)dsatutil_get_ptr_to_value( cmd_id,apps_id,&val_info));
  if((val_type == STR_TYPE) || 
     (val_type == MIX_STR_TYPE) || 
     (val_type == MIX_INDEX_TYPE) )
  {
    if(return_val == NULL)
    {
       DS_AT_MSG0_HIGH("String Type return value NULL");
       ASSERT(0);
    }
  }
  return return_val;
}/* dsatutil_get_val*/

 /*===========================================================================
  
  FUNCTION dsatutil_get_val_ext
  
  DESCRIPTION
    This FUNCTION take command index and return corresponding value stored in table.  
  
  DEPENDENCIES
    None.
  RETURN VALUE
    None
  
  SIDE EFFECTS
    None.
  ===========================================================================*/
 
  void * dsatutil_get_val_ext
 (
   dsat_num_item_type       cmd_id,            /*  entry index in parse table  */
   dsat_num_item_type       apps_id,           /* subscription ID*/
   dsat_num_item_type       index,
   value_type_enum_type     val_type
 )
 {
   dsati_value_info_struct_type val_info;
   void                        *return_val = NULL;
   memset((void *)&val_info,0,sizeof(val_info));
   val_info.index = index;
   val_info.val_type = val_type;
   return_val = ((void *)dsatutil_get_ptr_to_value( cmd_id,apps_id,&val_info));
   if((val_type == STR_TYPE) || 
      (val_type == MIX_STR_TYPE) || 
      (val_type == MIX_INDEX_TYPE) )
   {
     if(return_val == NULL)
     {
        DS_AT_MSG0_HIGH("String Type return value NULL");
     }
   }
   return return_val;
 }/* dsatutil_get_val_ext*/

 /*===========================================================================
 FUNCTION	   dsat_get_cbst_export_val
 
 DESCRIPTION   This function returns pointer to a prticular index in array 
			   dsat_cbst_export_val. 
 
 PARAMETERS    int index - index of the array
 
 DEPENDENCIES  None
 
 RETURN VALUE  dsat_num_item_type*
 
 SIDE EFFECTS  None
 ===========================================================================*/
 dsat_num_item_type* dsat_get_cbst_export_val(int index)
 {
   return &dsat_cbst_export_val[index];
 }
 
 /*===========================================================================
 FUNCTION	   dsat_get_cv120_export_val
 
 DESCRIPTION   This function gets global variable value of dsat_cv120_export_val
 
 PARAMETERS    None
 
 DEPENDENCIES  None
 
 RETURN VALUE  variable value
 
 SIDE EFFECTS  None
 ===========================================================================*/
 dsat_num_item_type dsat_get_cv120_export_val(void)
 {
   return dsat_cv120_export_val;
 }
 
 /*===========================================================================
 FUNCTION	   dsat_set_cv120_export_val
 
 DESCRIPTION   This function sets global variable value of dsat_cv120_export_val
 
 PARAMETERS    ptr to variable, value to be set
 
 DEPENDENCIES  None
 
 RETURN VALUE  None
 
 SIDE EFFECTS  None
 ===========================================================================*/
 
 
 void dsat_set_cv120_export_val(dsat_num_item_type variable_value)
 {
	dsat_cv120_export_val = variable_value;
 }

  
 /*==============================================================================
 
 FUNCTION  dsat_get_qcscftest_exp_val
 
 DESCRIPTION This function returns the dsat_qcscftest_exp_val global variable .  
 
 PARAMETERS index of array
 
 DEPENDENCIES None
 
 RETURN VALUE global variable
 
 SIDE EFFECTS	None
 ===========================================================================*/
#ifdef FEATURE_DATA_UCSD_SCUDIF_TEST
 uint8 dsat_get_qcscftest_exp_val(int index)
 {
   return dsat_qcscftest_exp_val[index];
 }
#endif

  /*==============================================================================
  
  FUNCTION	dsat_set_qcscftest_exp_val
  
  DESCRIPTION This function sets the dsat_qcscftest_exp_val global variable .  
  
  PARAMETERS value, index
  
  DEPENDENCIES None
  
  RETURN VALUE None
  
  SIDE EFFECTS	 None
  ===========================================================================*/
#ifdef FEATURE_DATA_UCSD_SCUDIF_TEST
  uint8 dsat_set_qcscftest_exp_val(int index, uint8 value)
  {
	 dsat_qcscftest_exp_val[index] = value;
  }
#endif

#ifdef FEATURE_WCDMA
 /*==============================================================================
 
 FUNCTION dsat_get_es_export_val
 
 DESCRIPTION This function returns the dsat_es_export_val global variable .  
 
 PARAMETERS index of array
 
 DEPENDENCIES None
 
 RETURN VALUE global variable
 
 SIDE EFFECTS None
 ===========================================================================*/
 dsat_num_item_type dsat_get_es_export_val(int index)
 {
   return dsat_es_export_val[index];
 }
 
  /*==============================================================================
 
 FUNCTION dsat_set_es_export_val
 
 DESCRIPTION This function returns the dsat_es_export_val global variable .  
 
 PARAMETERS index of array
 
 DEPENDENCIES None
 
 RETURN VALUE global variable
 
 SIDE EFFECTS None
 ===========================================================================*/
 void dsat_set_es_export_val(int index, dsat_num_item_type value)
 {
    dsat_es_export_val[index] = value;
 }
  
 /*==============================================================================
 
 FUNCTION dsat_get_esa_export_val
 
 DESCRIPTION This function returns the dsat_esa_export_val global variable .  
 
 PARAMETERS index of array
 
 DEPENDENCIES None
 
 RETURN VALUE global variable
 
 SIDE EFFECTS None
 ===========================================================================*/
 dsat_num_item_type dsat_get_esa_export_val(int index)
 {
   return dsat_esa_export_val[index];
 }

 /*==============================================================================
 
 FUNCTION dsat_set_esa_export_val
 
 DESCRIPTION This function sets the dsat_esa_export_val global variable .  
 
 PARAMETERS index of array, value
 
 DEPENDENCIES None
 
 RETURN VALUE global variable
 
 SIDE EFFECTS None
 ===========================================================================*/
 void dsat_set_esa_export_val(int index, dsat_num_item_type value)
 {
   dsat_esa_export_val[index] = value;
 }
#endif  /* FEATURE_WCDMA */

 /*===========================================================================
 
 FUNCTION DSAT_SET_VAL_INFO
 
 DESCRIPTION
       Set values into structure .  
 
 DEPENDENCIES
   None.
 RETURN VALUE
   None
 
 SIDE EFFECTS
   None.
 ===========================================================================*/

 void dsat_set_val_info
 (           
   dsat_num_item_type             index,
   dsat_num_item_type             size,
   void *                         val_ptr,
   value_type_enum_type           val_type,
   dsati_value_info_struct_type   *val_info
 )
 {
   val_info->index = index;
   val_info->size = size;
   val_info->val_ptr = val_ptr;
   val_info->val_type = val_type;

 } /*dsat_set_val_info*/
/*Other get set APIs for globals*/
void dsat_mo_dailing_state_init(void)
{
  ds_subs_e_type subs_id;

  for (subs_id = DS_FIRST_SUBS; subs_id < MAX_ACTIVE_SUBS; subs_id++)
  {
    dsat_mo_dailing_state[subs_id] = TRUE;
  }
}

boolean dsat_mo_dailing_state_get
(
  ds_subs_e_type subs_id
)
{
    return dsat_mo_dailing_state[subs_id];
}

void dsat_mo_dailing_state_set
(
  ds_subs_e_type subs_id,
  boolean        value
)
{
     dsat_mo_dailing_state[subs_id] = value;
}

/*===========================================================================
FUNCTION UI_STRNICMP

DESCRIPTION
Case in-sensitive string compare.  This function returns a number greater
than 0 if s1 is greater than s2, a number less than 0 if s1 is less than
s2, and 0 if s1 is the same as s2.  The comparison continues until both
strings reach the null character or len characters are reached.

DEPENDENCIES

SIDE EFFECTS
===========================================================================*/
int ui_strnicmp(const char *s1, const char *s2, int len)
{
  int ret_val = 0;
  /* While there are still characters to test AND
   * *s1 and *s2 are not both NULL characters AND
   * *s1 and *s2 are the same character (when in upper case)
   * check the next character.
   * Otherwise, return the result from the last comparison which has the
   * difference or, if there is no difference, it is set to the value 0.
   */
  while (len-- && (*s1 || *s2) && ((ret_val = UPCASE(*s1) - UPCASE(*s2)) == 0))
  {
    s1++;
    s2++; 
  }
  return ret_val;
}

/*===========================================================================
FUNCTION  DSATUTIL_DECODE_UCS2_80_81_82_ENCODING

DESCRIPTION
  Extracts the UCS2 80, 81, or 82 encoded buffer and decodes into UCS2 little
  endian.

DEPENDENCIES
  None.

RETURN VALUE
  size_t len: Length of the output buffer, number of uint16 characters.
  
SIDE EFFECTS
  None
==============================================================================*/
size_t dsatutil_decode_ucs2_80_81_82_encoding( uint8 *buf_in, 
                                             size_t buf_in_len,
                                             uint16 *buf_out, 
                                             size_t buf_out_max_len)
{
  uint8 i, j;
  uint8 *in_p = buf_in;
  uint16 *out_p16 = buf_out;
  uint16 ucs2_base = 0;
  uint8 ucs2_len = 0;
  size_t buf_len = 0;

#define DSATUTIL_MMGSDI_UCS2_80 0x80
#define DSATUTIL_MMGSDI_UCS2_81 0x81
#define DSATUTIL_MMGSDI_UCS2_82 0x82

  if ((buf_in == NULL) || (buf_out == NULL)) {
    return buf_len;
  }

  in_p++;
  /* The first octet, if 0x80, 0x81, 0x82 denotes the UCS2 encoding, else
   * the GSM encoding is used.
   */
  DS_AT_MSG1_HIGH("In dsatutil_decode_ucs2_80_81_82_encoding : Incoming buffer encoding used 0x%02x", buf_in[0]);
  switch (buf_in[0]) 
  {
    case DSATUTIL_MMGSDI_UCS2_80:
      for (i=0,j=0; (i<(buf_out_max_len-1)) && (j < (buf_in_len-1)); i+=1, j+=2, in_p+=2, out_p16++) 
      {
        if((in_p == NULL)||(in_p+1 == NULL) || (out_p16 == NULL) ||(*in_p == 0xFF))
        {
          break;
        }
        else 
        {
          *out_p16 = *(in_p);
          *out_p16 <<= 8;
          *out_p16 |= *(in_p+1);
        }
      }
      buf_len = i;
      break;

    case DSATUTIL_MMGSDI_UCS2_81:
      /* the second-octet is length of UCS2 chars in the EF-SPN */
      ucs2_len = *(in_p++);
      if ((ucs2_len > 0) && (ucs2_len < buf_out_max_len+1) && (in_p != NULL))
      {
        /* the third octet contains an 8-bit number which defines bits
         * 15 - 8 of a 16-base pointer, where bit 16 is set to 0, and bits 7 - 1 
         * are also set to zero. These 16 bits consitute a base pointer to a 
         * half-page in the UCS2 code space, to be used with some or all of the 
         * remaining octets in the string. 
         */
        ucs2_base = *(in_p++);
        ucs2_base <<= 7;
        
        for (i=0, j=0; (i < ucs2_len) && (i < buf_in_len ) && (j < buf_out_max_len); i++, j++, in_p++, out_p16++) 
        {
          if((in_p == NULL) || (out_p16 == NULL))
          {
            break;
          }
          /*                                        UNICODE              :  GSM-7bit */
          *out_p16 = (*(in_p) & 0x80) ? ( ucs2_base + (*(in_p) & 0x7F) ) : *(in_p);
        }
        buf_len = i;
      }
      break;

    case DSATUTIL_MMGSDI_UCS2_82:
      ucs2_len = *(in_p++);
      if ((ucs2_len > 0) && (ucs2_len < buf_out_max_len) && (in_p != NULL) && (in_p+1 != NULL)) 
      {
        /* the third and fourth octets contain a 16-bit number which defines the complete
         * 16-bit base pointer to a 'half-page' in the UCS2 code space, for use with some
         * or all of the remaining octets in the string. 
         */
        ucs2_base = *(in_p++);
        ucs2_base <<= 8;
        ucs2_base |= *(in_p++);

        for (i=0, j=0; (i < ucs2_len) && (j < buf_out_max_len); i+=1, j+=1, in_p++, out_p16++) 
        {
          if((in_p == NULL) || (out_p16 == NULL))
          {
            break;
          }
          /*                                            UNICODE          :  GSM-7bit */
          *out_p16 = (*(in_p) & 0x80) ? ( ucs2_base + (*(in_p) & 0x7F) ) : *(in_p);
        }
        buf_len  = i;
      }
      break;

    default:
      DS_AT_MSG0_ERROR("Input buffer not encoded in UCS2 80,81 or 82");
      break;
  }

  return buf_len;
} /* dsatutil_decode_ucs2_80_81_82_encoding */
