/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                D A T A   S E R V I C E S

                V E N D O R   S P E C I F I C

                A T    C O M M A N D   
                
                P R O C E S S I N G

GENERAL DESCRIPTION
  This module executes AT commands. It implements execution of common 
  vendor specific AT commands.

EXTERNALIZED FUNCTIONS

  dsatvend_exec_qcclr_cmd
    This function takes the result from the command line parser
    and executes it. It executes at$qcclr command.
    at$qcclr command is used for clearing mobile error log.

  dsatvend_exec_qcdmg_cmd
    This function takes the result from the command line parser
    and executes it. It executes at$qcdmg command.
    at$qcdmg is used to transition the serial port to diagnostic
    monitor.

  dsatvend_exec_qcdmr_cmd
    This function takes the result from the command line parser
    and executes it. It executes at$qcdmr command.
    at$qcdmr is used to set the DM baud rate.

  dsatvend_exec_qcdom_cmd
    This function takes the result from the command line parser
    and executes it. It executes at$qcdom command.
    at$qcdom is used to set domain name address.

  dsatvend_exec_qcdnsp_cmd
    This function takes the result from the command line parser
    and executes it. It executes at$qcdnsp command.
    at$qcdnsp is used to set primary DNS IP address.

  dsatvend_exec_qcdnss_cmd
    This function takes the result from the command line parser
    and executes it. It executes at$qcdnss command.
    at$qcdnss is used to set secondary DNS IP address.

  dsatvend_exec_qcter_cmd
    This function takes the result from the command line parser
    and executes it. It executes at$qcter command.
    at$qcter is used to set TE-DCE baud rate.

  dsatvend_exec_brew_cmd
    This function takes the result from the command line parser
    and executes it. It executes at$qcdmg command.
    at$brew is used to transition the serial port to diagnostic
    monitor.

  dsatvend_exec_qcpwrdn_cmd
    This function takes the result from the command line parser
    and executes it. It executes at$qcpwrdn command.
    at$qcpwrdn is used to power down the terminal.
  
  dsatvend_exec_qcdgen_cmd
    This function takes the result from the command line parser
    and executes it. It executes at$qcdgen command.
    at$qcdgen is used to send a specified quantity of PS data as IP packet(s) 
    over a primary PDP-IP context already activated by +CGACT command.

  dsatvend_exec_qcslip_cmd
    This function brings up the SLIP Iface by posting SLIP_IFACE_UP_EV to the
    DS task. It reserves a serial port that will be used by the slip stack.

  dsatvend_exec_prefmode_cmd
    This function takes the result from the command line parser
    and executes it. It executes at^prefmode command. This command provides 
    the ability to set/get the network mode preferences.

  dsatvend_exec_sysinfo_cmd
    This function takes the result from the command line parser
    and executes it. It executes AT^SYSINFO command. It provides
    the current SYSTEM information of <srv_status>, <srv_domain>
    <roam_status>, <sys_mode>,<sim_status>. 

  dsatvend_exec_pacsp_cmd
  This function takes the result from the command line parser
  and executes it. It executes AT+PACSP command.
  This function returns the current PLMN mode bit setting of EF CSP file.

  dsatvend_exec_qcpdpcfge_cmd
    This function takes the result from the command line parser
    and executes it. It executes at$qcpdpcfge command. 
    at$qcpdpcfge is used to edit values in APN table. 

  dsatvend_exec_qcpdpimscfge_cmd
  This function takes the result from the command line parser
  and executes it. It executes at$qcpdpimscfge command.
  at$qcpdpimscfge is used to edit PDP profile registry . 

  dsatvend_exec_qcdrx_cmd
    This function takes the result from the command line parser
    and executes it. It executes AT^QCDRX command.
    This command provides the ability to set/get the drx coefficient.

INITIALIZATION AND SEQUENCING REQUIREMENTS

   Copyright (c) 2001 - 2014 by Qualcomm Technologies Incorporated.
   All Rights Reserved.
   Qualcomm Confidential and Proprietary.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/


/*===========================================================================

                        EDIT HISTORY FOR MODULE    

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $PVCSPath: L:/src/asw/MM_DATA/vcs/dsatvend.c_v   1.3   11 Oct 2002 10:27:58   sbandaru  $
  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/atcop/src/dsatvend.c#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $
  
when       who     what, where, why
--------   ---     ----------------------------------------------------------
06/05/14   sc      Fixed LLVM errors.
04/15/14   sc      Fixed ATCoP KW errors.
03/17/14   pg      Fixed number of SW resets in Ni/Di targets.
03/18/14   pg      Fixed $QCMRUE SD LTE scan issue.
03/26/14   sc      Fixed SYSINFO display issue on SUB2.
03/11/14   tk/sc   Optimized debug macros usage in ATCoP.
02/15/14   pg      Added support for SGLTE+G
02/06/14   sc      Fixed $QCRSRP display issue.
01/10/14   sc      Added support for $QCPDPCFGEXT command.
01/10/14   sc      Converted high usage ATCoP macros to functions.
12/31/13   sc      Used cm_user* api instead of sys_sel* api
11/12/13   sc      Added support for $QCCSGCOPS command.
10/25/13   sc      Fixed $QCRSRP and $QCRSRQ result display issue.
10/18/13   sc      Added support for $QCNSP, $QCSIMT, $QCRCIND commands and
                   REMOTE CALL END, REMOTE RING, REMOTE ANSWER
                   unsolicited result codes.
10/03/13   tk      ATCoP changes for L+G DSDS.
09/13/13   sc      Fixed KW errors.
08/26/13   pg      Fixed $QCRMCALL ip invalid issue.
08/21/13   pg      Added support for emergency calls, operator res pco, mcc and mnc fields in $QCPDPCFGE.
06/05/13   tk      ATCoP changes for SGLTE support on Dime Plus.
05/28/13   tk      Fixed $QCBANDPREF issue with WLAN France 5000 band.
05/17/13   sk      Added data retry fields to $QCPDPCFGE command.
03/27/13   tk      Fixed issues in dual stack commands.
03/18/13   tk      Fixed $QCMRUE issue with invalid input PLMN ID length.
01/04/13   tk      ATCoP changes for DSDX C/G+G with True World Mode Support.
12/20/12   tk      Fixed $QCRMCALL command memory leak issue.
10/11/12   tk      Fixed $QCRMCALL command issue.
09/24/12   sk      Added $QCRSRP and $QCRSRQ command Support.
09/24/12   tk      Migrated to CM APIs for MRU table access.
08/22/12   sk      Mainlining Nikel Changes.
08/06/12   tk      Added support for $QCDRX command.
07/26/12   sb      Fixed KW Errors.
06/21/12   sk      Fixed $QCPDPCFGE apn bearer issue.
06/12/12   tk      Extended $QCSYSMODE command for 3GPP2 modes.
02/25/12   sb      Fixed compiler Warnings.
05/18/12   tk      Migrated to MSG 2.0 macros
05/18/12   sk      Added APN bearer field to $QCPDPCFGE.
05/14/12   sb      Fixed KW Warnings.
04/10/12   nc      Fixed Read concurrency issues for MRU commands.
03/29/12   sk      Fixed errors & warnings when FEATURE_DSAT_EXTENDED_CMD is enabled.
03/15/12   nc      Initialized mru_entry before writing onto NV.
02/17/11   ua      Added support for $QCRMCALL.
02/17/12   sk      Migrating Data related AT commands interface to unified MH.
02/06/12   ad      Added CSTF/CEN/CGPIAF/CVMOD command support.
01/19/12   sk      Feature cleanup.
01/04/12   sb      Fixed KW Warnings.
11/28/11   sb      Fixed $QCMRUE command to not show entries with NO_SRV as mode
11/03/11   nc      Replaced the deprecated API 'hw_partnum_version' with 'DalChipInfo_GetChipId'.
10/17/11   ad      Added +CECALL support.
09/27/11   mk      Added support for $QCCLAC command.
09/06/10   nc      Fixed the RSSI and RSCP display when 
                   FEATURE_WCDMA_DATA_FOR_UI_BAR_DISPLAY is enabled
07/05/11   nc      Added support for ^MODE,^CARDMODE,^SYSCONFIG,^SYSINFO,^DSCI.
06/07/11   nc      Added support for $QCAPNE command.
05/26/11   dvk     Global Variables Cleanup
05/13/11   ad      Added $QCPDPIMSCFGE support.
04/29/11   bs      Fixed $QCDGEN command to use COMPTASK.
03/14/11   ad      Added APN class support in $QCPDPCFGE command.
01/11/11   ad      Removed pending variable .
01/11/11   ad      Remove extern usage.used get/set API for command associated  
                   value pointers.
10/25/10   ad      Init Changes for DSDS. 
10/06/10   ad      Added Error msg before Assert(0).
10/04/10   ad      Generates PS data on the context ID when ID is less then  SYS_MAX_PDP_CONTEXTS.  
08/31/10   sa      Added support for default pdp type as IPv4v6.
09/13/10   sa      Fixed QCDGEN handler to send more data.
08/06/10   nc      Using vbatt_read_pmic_temperature instead of 
                   adc_read(ADC_MSM_THERM_DEGC).
07/28/10   bs      Fixed $QCDGEN command for LTE builds.
07/13/10   ad      Fixed CNTI issue.
07/08/10   ad      Added support for $ECALL command.
06/03/10   sn      Added FEATURE_BT_MODEM featurization for BT specific 
                   functionality. This feature will be enabled in MODEM proc 
                   on multi-proc targets.
05/31/10   ad      Fixed lint error.
05/28/10   sa      Fixed crash during $QCPDPCFGE execution.
05/20/10   kk      Fixed on-target compilation error.
05/17/10   ad      Added Support for $QCPDPCFGE.
05/10/10   kk      Added support for $QCVOLT, $QCHWREV, $QCBOOTVER, $QCTEMP, 
                   $QCAGC, $QCALLUP.
03/18/10   nc      Added Support for $QCDGEN for LTE.
01/18/10   bs      Added support for +PACSP.
06/10/09   ua      Added support for ^PREFMODE command. 
12/15/09   nc      Featurisation changes for LTE.
11/02/09   sa      Replacing snprintf with std_snprintf.
09/22/09   vrk     Merged LTE changes.
09/09/09   ua      Added support for EONS (spec 22.101).
09/18/09   ss      DataCommon CMI SU level impacts.
09/13/09   pp      AT$QCSLIP posts CallP UP command to DCC.
07/04/09   pp      hton/ntoh* macros replaced with ps_hton/ntoh*.
06/30/09   bs      Migration to modified diag interface.
06/12/09   ua      Added support for $QCBANDPREF command.
04/29/09   ua      Fixed compiler warnings. 
12/12/08   ua      Fixed Off target lint errors.
11/07/08   sa      Modified transition of serial port to diagnostic
                   monitor using $BREW command to be non-persistent.
10/18/08   pp      Added support for SLIP interface (AT$QCSLIP)
03/24/08   bs      Fixed Klocwork criticals.
02/18/08   sa      Added modification for Passport Feature support.
02/08/08   sa      Added support for $QCSQ command.
01/11/08   sa      Added support for $QCANTE and $QCRPW commands.
12/04/07   pp      Fixed RVCT 2.2 Compiler warning.
11/24/07   ua      Fixed Lint-lows. 
07/18/07   sa      Fixed feature wrap errors for GSM 1x builds.
05/02/07   ua      Fixed lint medium errors. 
02/15/07   ss      Fixed lint high errors
01/30/07   ss      Replaced banned string API calls
11/03/06   snb     Dual processor changes.
09/26/06   sa      Inter RAT handover support for $QCDGEN.
05/22/06   rsl     Add support for at$qcsysmode to return current sysmode, 
                   possible values returned: HSDPA/HSUPA/HSDPA+HSUPA/WCDMA/GSM.
03/10/06   snb     Verify datagen_info_idx before use.
01/26/06   snb     Have $QCDGEN use DSM_DS_SMALL_ITEM_POOL,
                   correct size of and indexing into datagen_info array,
                   Lint fixes.
09/08/05   ar      Add support for $QCSDFTEST vendor command.
08/10/05   snb     Allow secondary contexts for $QCDGEN.
08/02/05   snb     Add WCDMA-mode $QCDGEN support.
08/01/05   snb     Fix for 6500-GSM builds: use legacy functions instead of
                   ps_ip4_hdr functionality.
07/18/05   snb     Use PS functions for dummy IP frame construction.
07/08/05   snb     Call the registration function to SNDCP in the same way as
                   the DS UMTS PS handler for consistent operation.
06/27/05   snb     Add support for $QCDGEN sending data simultaneously on 
                   multiple contexts.
05/16/05   ar      Merge in serialzer port support to $QCDMG 
05/10/05   snb     Add flow control to $QCDGEN GPRS data send.
04/28/05   snb     Fix 6550 GSM-CDMA build issues.
02/24/05   snb     Add $QCDGEN command support.
11/19/03   ar      Replace dsatutil_format_ipaddr with dsatutil_convert_tuple
10/21/03   ar      Added $QCPWRDN command support.
10/08/03   ar      Add wrapper FEATURE_DSAT_BREW_SUPPORT to BREW commands.
                   Lint corrections.
07/07/03   sb      Added AT$BREW command
12/06/02   ak      Removed include of nv_items.h under UNFINISHED, and
                   instead include nv.h
10/09/02   sb      ERROR check to see if it is a valid baud in $QCTER
04/17/02   rc      Removed FEATURE wrap FEATURE_DS_SOCKETS around dns 
                   functions.
12/13/01   sb      Changed the rdm_data_got_at$qcdmg() function name to 
                   rdm_data_got_atqcdmg()
08/27/01   sb      ATCOP code is now partitioned into multiple VUs. As a 
                   result of the partitioning, some of the files have to be
                   renamed. So making the file/interface name changes.
08/10/01   sjd     Initial release to MSM5200 archives.
05/15/01   sb      Created module.

===========================================================================*/


/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"

#include <stringl/stringl.h>


#if defined(T_WINNT)
#error code not present
#endif /* WINNT */

#include "dstaski.h"
#include "dsati.h"
#include "dsatcmif.h"
#include "dsatctab.h"
#include "dsatparm.h"
#include "dsatvend.h"
#include "dsatme.h"
#include "msg.h"
#include "err.h"
#include "diag.h"
#include "nv.h"
#include "dsutil.h"
#ifdef FEATURE_ECALL_APP
#include "ecall_app.h"
#endif /* FEATURE_ECALL_APP */

#include <stdio.h>
#include <string.h>

#ifdef FEATURE_DATA_PS_SLIP
extern ds3g_siolib_portid_e_type slip_ser_port;
#include "slip_iface_ext.h"
#endif /* FEATURE_DATA_PS_SLIP */

#ifdef FEATURE_RUNTIME_DEVMAP
#include "rdevmap.h"
#include "diagcomm.h"
#endif /* FEATURE_RUNTIME_DEVMAP */

#ifdef FEATURE_DATA_IS707
#include "ds707_jcdma_m51.h"
#include "ds707_pkt_mgr.h"
#endif /* FEATURE_DATA_IS707 */

#ifdef FEATURE_DSAT_ETSI_DATA
#define FEATURE_DSM_WM_CB
#include "dsm.h"
#include "mn_cm_exp.h"
#include "dsatetsipkt.h"
#include "dsatetsictab.h"
#include "ps_in.h"
#include "dsumtspdpreg.h"
#include "ds_profile_3gpp.h"

#include "comptask_v.h"

#ifndef FEATURE_DSAT_LEGACY_GCF_SUPPORT          
#include "ps_ip4_hdr.h"
#else  /* FEATURE_DSAT_LEGACY_GCF_SUPPORT */
/* If legacy GCF feature is defined, Q304Maint branch of MMDATA_PS is being
   used and provides no ps_ip4_hdr functionality. */
#include "ps_iputil.h"
#endif /* FEATURE_DSAT_LEGACY_GCF_SUPPORT */

#endif /* FEATURE_DSAT_ETSI_DATA */

#ifdef FEATURE_DATA_WCDMA_PS
#include "rrcdata.h"
#include "wcdmamvsif.h"
#endif /* FEATURE_DATA_WCDMA_PS */

#ifdef FEATURE_GSM_GPRS
#include "gsndcp.h"
#endif /* FEATURE_GSM_GPRS */

#include "dsatetsime.h"

#include "vbatt.h"
#include "DDIChipInfo.h"
#include "DALDeviceId.h"

#ifdef FEATURE_HDR_AT_TEST_ONLY
#include "dsat707hdrctab.h"
#endif /* FEATURE_HDR_AT_TEST_ONLY */
#ifdef FEATURE_DS_PSTATS
#include "dsatpstatsctab.h"
#endif /* FEATURE_DS_PSTATS */
#ifdef FEATURE_CDMA_SMS
#include "dsat707smsctab.h"
#endif /* FEATURE_CDMA_SMS */
#ifdef FEATURE_BCMCS
#include "dsat_1xhdr_bcctab.h"
#endif /* FEATURE_BCMCS */
#ifdef FEATURE_DATA_IS707
#include "dsat707vendctab.h"
#include "dsat707mipctab.h"
#include "dsat707extctab.h"
#include "dsat707faxctab.h"
#endif /*FEATURE_DATA_IS707*/

#ifdef FEATURE_DSAT_GOBI_MAINLINE 
#include "ftm.h"
#include "ftm_diagpkt.h"
#include "gobi_nv.h"
#include "gobi_im_common.h"
#include "ftm_rf_cmd.h"
#include "bsp.h"
/* Variable to find out what command is being processed currently */
#endif /* FEATURE_DSAT_GOBI_MAINLINE */
#include "ds_rmnet_meta_sm.h"
#include "ds_rmnet_defs.h"
#include "ds_qmi_defs.h"
#include "ds_qmux.h"
#include "ds_profile.h"
#include "ps_iface.h"
#include "ps_iface_defs.h"
#include "sd.h"
#include "ds3gsiolib_ex.h"

#ifdef FEATURE_DSAT_LTE
#include "lte_rrc_ext_api.h"
#ifdef FEATURE_VOIP 
#include "ims_settings_atcop.h"
#endif /*FEATURE_VOIP*/
#endif /*FEATURE_DSAT_LTE*/

#include "sys_m_reset.h"
/*===========================================================================

            LOCAL DEFINITIONS AND DECLARATIONS FOR MODULE

  This section contains local definitions for constants, macros, types,
  variables and other items needed by this module.

===========================================================================*/

#define MAX_DNS_SIZE 18
dsat_string_item_type dsat_qcdnssec_val[MAX_DNS_SIZE];

/* RSSI antenna level for $QCANTE*/
typedef enum
{
  RSSI_LEVEL_INVALID = -1,
  RSSI_LEVEL_0 = 0,
  RSSI_LEVEL_1,
  RSSI_LEVEL_2,
  RSSI_LEVEL_3,
  RSSI_LEVEL_4
}dsat_ante_rssi_level_e_type;

/* PACSP command  */
boolean dsat_pcasp_cmd_pending = FALSE;
/* PREFMODE global variable */

dsat_sysinfo_s_type dsat_sysinfo_cmd = {0};

extern dsat_qcrmcall_s_type dsat_qcrmcall_info;

void (*dsat_apn_disable_flag_chg_cb_fptr) (uint16 prof_num, boolean apn_disable_flag) = NULL;

typedef struct 
{ 
  ps_iface_type *iface_ptr[RMNET_NUM_LAPTOP_INSTANCES+1];
}dsat_qcrmcall_global_info_s_type;

dsat_qcrmcall_global_info_s_type dsat_qcrmcall_global_info;
/* ==================================MRU Related======================================= */

#define DSAT_CDMA_RAT 0
#define DSAT_HDR_RAT 1
#define DSAT_GSM_RAT 2
#define DSAT_WCDMA_RAT 3
#define DSAT_LTE_RAT 4

#define DSAT_MRU_BAND_INPUT_3GPP2_BC2 2
#define DSAT_MRU_BAND_INPUT_3GPP2_MAX 20

typedef struct
{
  dsat_num_item_type index;
  dsat_num_item_type rat;
  dsat_num_item_type band;
  char band_pref[100];
  dsat_num_item_type channel;
  sys_plmn_id_s_type     plmn;
  boolean is_plmn_valid;
}dsat_mru_input_s_type;

dsat_mru_input_s_type dsat_mru_input;

LOCAL dsat_num_item_type dsat_mru_read_index;

LOCAL dsat_result_enum_type dsat_mru_result;

const LOCAL dsat_mru_band_pref_map_gw_s_type mru_band_pref_map_gw[] =
{
  { DSAT_MRU_BAND_PREF_GSM_450            ,     "0"},
  { DSAT_MRU_BAND_PREF_GSM_480            ,     "1"},
  { DSAT_MRU_BAND_PREF_GSM_750            ,     "2"},
  { DSAT_MRU_BAND_PREF_GSM_850            ,     "3"},
  { DSAT_MRU_BAND_PREF_GSM_EGSM_900       ,     "4"},
  { DSAT_MRU_BAND_PREF_GSM_PGSM_900       ,     "5"},
  { DSAT_MRU_BAND_PREF_GSM_RGSM_900       ,     "6"},
  { DSAT_MRU_BAND_PREF_GSM_DCS_1800       ,     "7"},
  { DSAT_MRU_BAND_PREF_GSM_PCS_1900       ,     "8"},
  { DSAT_MRU_BAND_PREF_WCDMA_I_IMT_2000   ,     "0"},
  { DSAT_MRU_BAND_PREF_WCDMA_II_PCS_1900  ,     "1"},
  { DSAT_MRU_BAND_PREF_WCDMA_III_1700     ,     "2"},
  { DSAT_MRU_BAND_PREF_WCDMA_IV_1700      ,     "3"},
  { DSAT_MRU_BAND_PREF_WCDMA_V_850        ,     "4"},
  { DSAT_MRU_BAND_PREF_WCDMA_VI_800       ,     "5"},
  { DSAT_MRU_BAND_PREF_WCDMA_VII_2600     ,     "6"},
  { DSAT_MRU_BAND_PREF_WCDMA_VIII_900     ,     "7"},
  { DSAT_MRU_BAND_PREF_WCDMA_IX_1700      ,     "8"},
  { DSAT_MRU_BAND_PREF_WCDMA_XI_1500      ,     "9"}
};

const LOCAL dsat_mru_band_pref_map_lte_s_type mru_band_pref_map_lte[] =
{
  { DSAT_MRU_BAND_PREF_LTE_EUTRAN_1       ,     "0"},
  { DSAT_MRU_BAND_PREF_LTE_EUTRAN_2       ,     "1"},
  { DSAT_MRU_BAND_PREF_LTE_EUTRAN_3       ,     "2"},
  { DSAT_MRU_BAND_PREF_LTE_EUTRAN_4       ,     "3"},
  { DSAT_MRU_BAND_PREF_LTE_EUTRAN_5       ,     "4"},
  { DSAT_MRU_BAND_PREF_LTE_EUTRAN_6       ,     "5"},
  { DSAT_MRU_BAND_PREF_LTE_EUTRAN_7       ,     "6"},
  { DSAT_MRU_BAND_PREF_LTE_EUTRAN_8       ,     "7"},
  { DSAT_MRU_BAND_PREF_LTE_EUTRAN_9       ,     "8"},
  { DSAT_MRU_BAND_PREF_LTE_EUTRAN_10      ,     "9"},
  { DSAT_MRU_BAND_PREF_LTE_EUTRAN_11      ,     "10"},
  { DSAT_MRU_BAND_PREF_LTE_EUTRAN_12      ,     "11"},
  { DSAT_MRU_BAND_PREF_LTE_EUTRAN_13      ,     "12"},
  { DSAT_MRU_BAND_PREF_LTE_EUTRAN_14      ,     "13"},
  { DSAT_MRU_BAND_PREF_LTE_EUTRAN_17      ,     "14"},
  { DSAT_MRU_BAND_PREF_LTE_EUTRAN_18      ,     "15"},
  { DSAT_MRU_BAND_PREF_LTE_EUTRAN_19      ,     "16"},
  { DSAT_MRU_BAND_PREF_LTE_EUTRAN_20      ,     "17"},
  { DSAT_MRU_BAND_PREF_LTE_EUTRAN_21      ,     "18"},
  { DSAT_MRU_BAND_PREF_LTE_EUTRAN_33      ,     "19"},
  { DSAT_MRU_BAND_PREF_LTE_EUTRAN_34      ,     "20"},
  { DSAT_MRU_BAND_PREF_LTE_EUTRAN_35      ,     "21"},
  { DSAT_MRU_BAND_PREF_LTE_EUTRAN_36      ,     "22"},
  { DSAT_MRU_BAND_PREF_LTE_EUTRAN_37      ,     "23"},
  { DSAT_MRU_BAND_PREF_LTE_EUTRAN_38      ,     "24"},
  { DSAT_MRU_BAND_PREF_LTE_EUTRAN_39      ,     "25"},
  { DSAT_MRU_BAND_PREF_LTE_EUTRAN_40      ,     "26"}
};


#define ETSIPKT_BEGIN_PROFILE_TRANSACTION(profile_id) \
          profile_status = ds_profile_begin_transaction(DS_PROFILE_TRN_RW, \
                                                           DS_PROFILE_TECH_3GPP,\
                                                          (ds_profile_num_type)profile_id,\
                                                           &profile_hndl);
                                                           
#define ETSIPKT_END_PROFILE_TRANSACTION(profile_hndl) \
          profile_status = ds_profile_end_transaction(profile_hndl, \
                                                           DS_PROFILE_ACTION_COMMIT);
  
#define ETSIPKT_SET_PROFILE_PARAM(profile_hndl,param_type,param_info)\
          profile_status = ds_profile_set_param(profile_hndl, \
                                               param_type, \
                                               &param_info);
                                               
#define ETSIPKT_GET_PROFILE_PARAM(profile_hndl,param_type,param_info)\
          profile_status = ds_profile_get_param(profile_hndl, \
                                               param_type, \
                                               &param_info);


/* ==================================MRU Related======================================= */

/*-------------------------------------------------------------------------
    Function Definitions:
-------------------------------------------------------------------------*/

/* ==================================MRU Related functions======================================= */
LOCAL uint32 vend_convert_band_pref_to_sys_band32
(
  uint32 dsat_band_pref,
  char *band_ptr,
  uint32 src_size
);
LOCAL uint32 vend_convert_lte_band_pref_to_sys_band
(
  uint64 dsat_band_pref,
  char *band_ptr,
  uint32 src_size
);

LOCAL boolean vend_convert_mru
(
  cm_mmode_mru_table_entry_u_type *dsat_mru_entry,
  dsat_mru_input_s_type           *mru_data
);

LOCAL uint32 vend_convert_mru_band_input_to_gsm_band_pref
(
  dsat_mru_band_input_gsm_e_type band
);

LOCAL uint32 vend_convert_mru_band_input_to_wcdma_band_pref
(
  dsat_mru_band_input_wcdma_e_type band
);

LOCAL uint64 vend_convert_mru_band_input_to_lte_band_pref
(
  dsat_mru_band_input_lte_e_type band
);

LOCAL boolean vend_validate_mru_inputs
(
  dsat_num_item_type index,
  dsat_num_item_type rat,
  dsat_num_item_type band
);

LOCAL boolean vend_mru_change_sys_oprt_mode
(
  sys_oprt_mode_e_type new_mode
);

LOCAL dsat_result_enum_type dsatvend_mru_read_cm_cmd
(
  dsat_num_item_type index
);
#ifdef FEATURE_DSAT_LTE
#ifdef FEATURE_VOIP 
void dsatvend_voipm_config_cb
(
  ims_settings_at_cmd_rsp_info at_cmd_rsp_info
);
#endif /*FEATURE_VOIP*/
#endif /*FEATURE_DSAT_LTE*/

/* ==================================MRU Related functions======================================= */

LOCAL dsat_result_enum_type vend_convert_ph_pref_type
(
  boolean to_at_state,
  dsat_sysconfig_type *at_pref_ptr,
  ds_at_cm_ph_pref_type *cm_pref_ptr
);

LOCAL void clr_err_log( void);

/* PREFMODE global variable */
#define DSAT_BANDPREF_NON_PERS 0
#define DSAT_BANDPREF_PERS 1

dsat_num_item_type dsat_bandpref_pers_val;
dsat_bandpref_e_type  dsat_bandpref_val[(uint32)DSAT_MAX_BANDPREF+1];

const dsat_string_item_type dsat_bandpref_tststr[][30] =
{ 
"None","\"1.BC0_A\"","\"2.BC0_B\"","\"3.BC0\"","\"4.BC1\"","\"5.BC3\"","\"6.BC4\"","\"7.BC5\"",
"\"8.GSM_DCS_1800\"","\"9.GSM_EGSM_900\"","\"10.GSM_PGSM_900\"",
"\"11.BC6\"","\"12.BC7\"","\"13.BC8\"","\"14.BC9\"","\"15.BC10\"","\"16.BC11\"",
"\"17.BC12\"","\"18.BC14\"","\"19.BC15\"","\"20.BC16",
"\"21.GSM_450\"","\"22.GSM_480\"","\"23.GSM_750\"","\"24.GSM_850\"",
"\"25.GSM_RGSM_900\"","\"26.GSM_PCS_1900\"",
"\"27.WCDMA_I_IMT_2000\"","\"28.WCDMA_II_PCS_1900\"","\"29.WCDMA_III_1700 \"",
"\"30.WCDMA_IV_1700\"","\"31.WCDMA_V_850\"","\"32.WCDMA_VI_800 \"",
"\"33.WCDMA_VII_2600\"","\"34.WCDMA_VIII_900\"","\"35.WCDMA_IX_1700\"",
"\"36.WLAN_US_2400 \"","\"37.WLAN_JAPAN_2400\"","\"38.WLAN_EUROPE_2400\"",
"\"39.WLAN_SPAIN_2400\"","\"40.WLAN_FRANCE_2400 \"","\"41.WLAN_US_5000\"",
"\"42.WLAN_JAPAN_5000\"","\"43.WLAN_EUROPE_5000\"","\"44.WLAN_SPAIN_5000\"",
"\"45.WLAN_FRANCE_5000 \"","\"46.Any\"","" };

static const dsat_string_item_type dsat_mru_tststr[][30] =
{ 
"CDMA/HDR",
"\"0.BC0\"","\"1.BC1\"","\"3.BC3\"","\"4.BC4\"","\"5.BC5\"","\"6.BC6\"",
"\"7.BC7\"","\"8.BC8\"","\"9.BC9\"","\"10.BC10\"","\"11.BC11\"","\"12.BC12\"",
"\"13.BC13\"","\"14.BC14\"","\"15.BC15\"","\"16.BC16\"","\"17.BC17","\"18.BC18\"","\"19.BC19",
"GSM",
"\"0.GSM_450\"","\"1.GSM_480\"","\"2.GSM_750\"","\"3.GSM_850\"","\"4.GSM_EGSM_900\"",
"\"5.GSM_PGSM_900\"","\"6.GSM_RGSM_900\"","\"7.GSM_DCS_1800\"","\"8.GSM_PCS_1900\"",
"WCDMA",
"\"0.WCDMA_I_IMT_2000\"","\"1.WCDMA_II_PCS_1900\"","\"2.WCDMA_III_1700 \"",
"\"3.WCDMA_IV_1700\"","\"4.WCDMA_V_850\"","\"5.WCDMA_VI_800 \"",
"\"6.WCDMA_VII_2600\"","\"7.WCDMA_VIII_900\"","\"8.WCDMA_IX_1700\"","\"9.WCDMA_XI_1500\"",
#ifdef FEATURE_DSAT_LTE
"LTE",
"\"0.EUTRAN_BAND1\"","\"1.EUTRAN_BAND2\"","\"2.EUTRAN_BAND3 \"","\"3.EUTRAN_BAND4 \"",
"\"4.EUTRAN_BAND5\"","\"5.EUTRAN_BAND6\"","\"6.EUTRAN_BAND7 \"",
"\"7.EUTRAN_BAND8\"","\"8.EUTRAN_BAND9\"","\"9.EUTRAN_BAND10 \"",
"\"10.EUTRAN_BAND11\"","\"11.EUTRAN_BAND12\"","\"12.EUTRAN_BAND13 \"","\"13.EUTRAN_BAND14 \"","\"14.EUTRAN_BAND17 \"",
"\"15.EUTRAN_BAND18 \"","\"16.EUTRAN_BAND19\"","\"17.EUTRAN_BAND20 \"","\"18.EUTRAN_BAND21\"",
"\"19.EUTRAN_BAND33 \"","\"20.EUTRAN_BAND34 \"","\"21.EUTRAN_BAND35\"","\"22.EUTRAN_BAND36\"","\"23.EUTRAN_BAND37 \"",
"\"24.EUTRAN_BAND38\"","\"25.EUTRAN_BAND39\"","\"26.EUTRAN_BAND40 \"",
#endif /* FEATURE_DSAT_LTE */
 "\"(0 - 2047)\"","" };

LOCAL const char *dsat_qcsysmode_text[DSAT_QCSYSMODE_ID_MAX] =
{
  "NOT DETERMINED",
  "CDMA",
  "GSM",
  "HDR",
  "WCDMA",
  "LTE",
  "TDS",
  "HDR - REV0",
  "HDR - REVA",
  "HDR - REVB",
  "HDR - EMPA EHRPD",
  "HDR - MMPA EHRPD",
  "WCDMA - HSDPA",
  "WCDMA - HSUPA",
  "WCDMA - HSDPA and HSUPA",
  "WCDMA - HSDPA+",
  "WCDMA - HSDPA+ and HSUPA",
  "WCDMA - DC HSDPA+",
  "WCDMA - DC HSDPA+ and HSUPA",
  "WCDMA - 64QAM HSDPA+",
  "WCDMA - 64QAM HSDPA+ and HSUPA",
  "CDMA and HDR",
  "CDMA and LTE",
};

#ifdef FEATURE_DSAT_GOBI_MAINLINE
typedef struct dsatdvend_proc_qcagc_s
{
   int16            ftm_dispatch_id;
   int16            ftm_dispatch_div_id;
   int8             enable_rx_diversity;
   ftm_mode_id_type mode_band;
   uint16           lna; 
   uint16           path;
   union
   {
     struct
     {
       uint16           tx_chan;        
       uint16           rx_chan;
   int16            vga_gain_offset;
   int8             vga_vs_chan_freq;
     } etsi;
     struct
     {
       uint16           chan;        
   int16            rx_agc_range;
   int16            rx_min_rssi;
     } cdma;
   } sys_mode;
} dsatdvend_proc_qcagc_type;

/*============================================================================

   Local definitions and declarations used by dsatvend_ftm* functions
   
===========================================================================*/
typedef dsat_result_enum_type (dsatvend_ftm_diagpkt_cb_f_type) 
                              (ftm_pkt_type *ftm_pkt, void *rsp_arg);

typedef struct dsatvend_ftm_diagpkt_rsp_params_s {
   ftm_pkt_type   ftm_diagpkt;
   dsatvend_ftm_diagpkt_cb_f_type 
                 *rsp_cb;
   void          *rsp_arg;
} dsatvend_ftm_diagpkt_rsp_params_type;

static dsatvend_ftm_diagpkt_rsp_params_type 
            dsatvend_ftm_diagpkt_rsp_params = {NULL, NULL};

typedef void (ftm_diagpkt_rsp_cb_f_type)(char *rsp_ftm_pkt);

void dsatvend_ftm_diagpkt_fwd_cb(char *rsp_ftm_pkt);


/*===========================================================================

Local definitions and declarations used by dsatvend_exec_qcallup_* functions

============================================================================*/
typedef struct dsatdvend_proc_qcallup_s {
   int              ftm_dispatch_id;
   ftm_mode_id_type mode_band;
   int8             tx_enable;
   unsigned int     tx_chan;        
   unsigned int     pdm;
} dsatdvend_proc_qcallup_type;

/* Variable to find out what command is being processed currently */
#endif /* FEATURE_DSAT_GOBI_MAINLINE */

/* =============================================================
           QCRMCALL Related Functions and Definitions 
===============================================================*/

LOCAL boolean dsat_iface_event_register(ps_iface_type *iface_ptr);
LOCAL void dsat_iface_event_deregister(ps_iface_type *iface_ptr);



/*===========================================================================

FUNCTION DSATVEND_EXEC_QCCLR_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes at$qcclr command.
  at$qcclr command is used for clearing mobile error log.

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    OK : if the command has been successfully executed
    ERROR : if there was any problem in executing the command

SIDE EFFECTS
  None

===========================================================================*/
/* ARGSUSED */
dsat_result_enum_type dsatvend_exec_qcclr_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type result;

  /*---------------------------------------
   Processing command with no args
   ----------------------------------------*/
  if (tok_ptr->op == (NA) ) 
  {
    clr_err_log();
    result = DSAT_OK;
  }
  /*--------------------------------------
  Standard processing for a TEST command:
  ----------------------------------------*/
  else if (tok_ptr->op == (NA|EQ|QU) )
  {
    /*  NO-OP but prevents ERROR --                     */
    /*  Code at top will output                         */
    /*  the command name for at TEST command.           */
    result = DSAT_OK;
  }
  else  /*  Wrong Operator  */
  {
    result = DSAT_ERROR;
  }

  return result;

} /* dsatvend_exec_qcclr_cmd () */


/*===========================================================================
FUNCTION CLR_ERR_LOG

DESCRIPTION
  This function clears the NV error log whenever the command AT$QCCLR is
  issued.

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
LOCAL void clr_err_log( void)
{
  byte i;
  
  /*-----------------------------------------------------------------------
    Loop through all entries in the error log and clear them.
  -------------------------------------------------------------------------*/
  for (i=0; i < NV_MAX_ERR_LOG; i++ )
  {
    (void) err_clr_log( i);
  }

} /* clr_err_log () */


/*===========================================================================

FUNCTION DSATVEND_EXEC_QCDMR_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes at$qcdmr command.
  at$qcdmr is used to set the DM baud rate.

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_OK : if the command has been successfully executed
    DSAT_ERROR : if there was any problem in executing the command

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatvend_exec_qcdmr_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type result;
  
  result = dsatparm_exec_param_cmd(
                                    mode,
                                    parse_table,
                                    tok_ptr,
                                    res_buff_ptr
                                  );
  return result;
}  /* dsatvend_exec_qcdmr_cmd() */

/*  ------------------------------------------------------------------  */



/*===========================================================================

FUNCTION DSATVEND_EXEC_QCDNSP_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes at$qcdnsp command.
  at$qcdnsp is used to set primary DNS IP address.

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_OK : if the command has been successfully executed
    DSAT_ERROR : if there was any problem in executing the command

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatvend_exec_qcdnsp_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type result;
  uint32                ds707_qcdns_val[2]={0,0};
  
#ifdef FEATURE_DATA_IS707
  ds707_pkt_mgr_get_1x_profile_val(DS707_QCDNS_VAL,(void *)ds707_qcdns_val,2);
#else
  if((tok_ptr->op == (NA|QU)) && (NV_DONE_S == dsatutil_get_nv_item( NV_PRIMARY_DNS_I,&ds_nv_item )))
  {
    ds707_qcdns_val[0] = ds_nv_item.primary_dns;
  }
#endif /*FEATURE_DATA_IS707*/
  result = dsatparm_exec_param_cmd(
                                   mode,
                                   parse_table,
                                   tok_ptr,
                                   res_buff_ptr
                                  );
  
  if (DSAT_OK == result) 
  {
    if (tok_ptr->op == (NA|EQ|AR))
    {
      /*---------------------------------------------------------------
      Check for validity of the IP address & convert to internal format
      ---------------------------------------------------------------*/
      if (DSAT_OK == dsatutil_convert_tuple (STRING_TO_INT,
					     TUPLE_TYPE_IPv4_254,
                                             &ds707_qcdns_val[0],
					     (byte*)dsatutil_get_val(parse_table->cmd_id,0,0,STR_TYPE),
					     MAX_DNS_SIZE,
					     DSAT_CONVERT_IP) )
      {
        /*---------------------------------------------------------------
        Write Primary DNS item into the NV 
        ---------------------------------------------------------------*/
#ifdef FEATURE_DATA_IS707
        ds707_pkt_mgr_set_1x_profile_val(DS707_QCDNS_VAL,(void *)ds707_qcdns_val,2);
#endif /*FEATURE_DATA_IS707*/
        ds_nv_item.primary_dns  = ds707_qcdns_val[0];
        (void)dsatutil_put_nv_item( NV_PRIMARY_DNS_I, &ds_nv_item );
        result = DSAT_OK;
      }
      else
      {
        result = DSAT_ERROR;
      }
    }
    else if (tok_ptr->op == (NA))
    {
      result = DSAT_OK;
    }
    else if (tok_ptr->op == (NA|QU))
    {
      /*---------------------------------------------------------------
      Format NV data to 4-tuple string
      ---------------------------------------------------------------*/
      if (DSAT_OK == dsatutil_convert_tuple (INT_TO_STRING,
                                             TUPLE_TYPE_IPv4_254,
                                             &ds707_qcdns_val[0],
                                             (byte*)dsatutil_get_val(
                                             DSAT_VENDOR_QCDNSP_IDX,0,0,STR_TYPE),
                                             MAX_DNS_SIZE,
                                             DSAT_CONVERT_IP) )
      {
        /* Use extended format on value query */
         res_buff_ptr->used +=
              (word)snprintf ((char*)res_buff_ptr->data_ptr,
                                    res_buff_ptr->size,
                                    "%s: %s",
                                    (char*)parse_table->name,
                                    (char*)dsatutil_get_val(
                                    DSAT_VENDOR_QCDNSP_IDX,0,0,STR_TYPE));

         result = DSAT_OK;
      }
      else
      {
        result = DSAT_ERROR;
      }
    }
    else if (tok_ptr->op == (NA|EQ|QU))
    {
      res_buff_ptr->used =
           (word)snprintf ((char*)res_buff_ptr->data_ptr,
                                 res_buff_ptr->size,
                                 "%s:",
                                 (char*)parse_table->name);      
      result = DSAT_OK;
    }
    else
    {
      result = DSAT_ERROR;
    }
  }
  return result;
}  /* dsatvend_exec_qcdnsp_cmd() */


/*===========================================================================

FUNCTION DSATVEND_EXEC_QCDNSS_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes at$qcdnss command.
  at$qcdnss is used to set secondary DNS IP address.

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_OK : if the command has been successfully executed
    DSAT_ERROR : if there was any problem in executing the command

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatvend_exec_qcdnss_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type result;
  uint32                ds707_qcdns_val[2]={0,0};

#ifdef FEATURE_DATA_IS707
  ds707_pkt_mgr_get_1x_profile_val(DS707_QCDNS_VAL,(void *)ds707_qcdns_val,2);
#else
  if((tok_ptr->op == (NA|QU)) && (NV_DONE_S == dsatutil_get_nv_item( NV_SECONDARY_DNS_I,&ds_nv_item )))
  {
    ds707_qcdns_val[1] = ds_nv_item.secondary_dns;
  }
#endif /*FEATURE_DATA_IS707*/

  result = dsatparm_exec_param_cmd(
                                   mode,
                                   parse_table,
                                   tok_ptr,
                                   res_buff_ptr
                                  );

  if (DSAT_OK == result)
  {
    if (tok_ptr->op == (NA|EQ|AR))
    {
      /*---------------------------------------------------------------
      Check for validity of the IP address & convert to internal format
      ---------------------------------------------------------------*/
      if (DSAT_OK == dsatutil_convert_tuple (STRING_TO_INT,
                                             TUPLE_TYPE_IPv4_254,
                                             &ds707_qcdns_val[1],
                                             (byte*)dsatutil_get_val(parse_table->cmd_id,0,0,STR_TYPE),
                                             sizeof(dsat_qcdnssec_val),
                                             DSAT_CONVERT_IP) )
      {
        /*---------------------------------------------------------------
        Write Primary DNS item into the NV 
        ---------------------------------------------------------------*/
#ifdef FEATURE_DATA_IS707
        ds707_pkt_mgr_set_1x_profile_val(DS707_QCDNS_VAL,(void *)ds707_qcdns_val,2);
#endif /*FEATURE_DATA_IS707*/
        ds_nv_item.secondary_dns  = ds707_qcdns_val[1];
        (void)dsatutil_put_nv_item( NV_SECONDARY_DNS_I, &ds_nv_item );
        result = DSAT_OK;
      }
      else
      {
        result = DSAT_ERROR;
      }
    }
    else if (tok_ptr->op == (NA))
    {
      result = DSAT_OK;
    }
    else if (tok_ptr->op == (NA|QU))
    {
      /*---------------------------------------------------------------
      Format NV data to 4-tuple string
      ---------------------------------------------------------------*/
      if (DSAT_OK == dsatutil_convert_tuple (INT_TO_STRING,
                                             TUPLE_TYPE_IPv4_254,
                                             &ds707_qcdns_val[1],
                                             dsat_qcdnssec_val,
                                             sizeof(dsat_qcdnssec_val),
                                             DSAT_CONVERT_IP) )
      {
        /* Use extended format on value query */
         res_buff_ptr->used +=
              (word)snprintf ((char*)res_buff_ptr->data_ptr,
                                    res_buff_ptr->size,
                                    "%s: %s",
                                    (char*)parse_table->name,
                                     (char*)dsat_qcdnssec_val);
         result = DSAT_OK;
      }
      else
      {
        result = DSAT_ERROR;
      }
    }
    else if (tok_ptr->op == (NA|EQ|QU))
    {
       res_buff_ptr->used =
             (word)snprintf ((char*)res_buff_ptr->data_ptr,
                                   res_buff_ptr->size,
                                   "%s:",
                                   (char*)parse_table->name);    
      result = DSAT_OK;
    }
    else
    {
      result = DSAT_ERROR;
    }
  }
  return result;
} /* dsatvend_exec_qcdnss_cmd() */


/*===========================================================================

FUNCTION DSATVEND_EXEC_QCTER_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes at$qcter command.
  at$qcter is used to set current and default TE-DCE baud rate.

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_OK : if the command has been successfully executed
    DSAT_ERROR : if there was any problem in executing the command

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatvend_exec_qcter_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type result;
  dsat_num_item_type    old_qcter_val;
  dsat_num_item_type    temp_val;
  old_qcter_val = (dsat_num_item_type)dsatutil_get_val(
                                DSAT_VENDOR_QCTER_IDX,0,0,NUM_TYPE); 
                                 /* Retain old value for error checking */
  
  result = dsatparm_exec_param_cmd(
                                   mode,
                                   parse_table,
                                   tok_ptr,
                                   res_buff_ptr
                                  );
#ifdef FEATURE_DATA_IS707
  if( (result == DSAT_OK) && (IS_JCDMA_MODE()))
  {
    if((tok_ptr->op == (NA|EQ|AR) &&
       (SIO_BITRATE_115200 != (dsat_num_item_type)dsatutil_get_val(
                                DSAT_VENDOR_QCTER_IDX,0,0,NUM_TYPE))) )
    {
      /* Baudrate not allowed in JCDMA mode, 
       * tell user "ERROR".  Restore old IPR value. 
       */
      result = DSAT_ERROR;
      DSATUTIL_SET_VAL(DSAT_VENDOR_QCTER_IDX,0,0,0,old_qcter_val,NUM_TYPE)
      
    }
    else if (tok_ptr->op == (NA|EQ|QU))
    {
      res_buff_ptr->used =
        (word) snprintf((char*)res_buff_ptr->data_ptr,
                               res_buff_ptr->size,
                               "$QCTER:(115200)");
    }
  }
#endif /* FEATURE_DATA_IS707 */
  if (DSAT_OK == result) 
  {
    if (tok_ptr->op == (NA|EQ|AR)) 
    { 
      temp_val = (dsat_num_item_type)dsatutil_get_val(
                                          DSAT_VENDOR_QCTER_IDX,0,0,NUM_TYPE);
      if (sio_is_baud_valid ((sio_bitrate_type)temp_val) == FALSE)
      {
        /* Baudrate not allowed, tell user "ERROR" */
        DSATUTIL_SET_VAL(DSAT_VENDOR_QCTER_IDX,0,0,0,old_qcter_val,NUM_TYPE)
        result = DSAT_ERROR;
      }
      else
      {
#ifdef FEATURE_DATA_IS707
        /*--------------------------------------------------------------------
          Note that this if statement does the function call internally, which
          will result in the baud rate being changed, if the passed in 
          qcter_val is okay
        --------------------------------------------------------------------*/
        temp_val = (dsat_num_item_type) dsatutil_get_val(
                                           DSAT_VENDOR_QCTER_IDX,0,0,NUM_TYPE);
        if ((IS_JCDMA_MODE()) && 
            (ds707_jcdma_chng_m513_via_at((sio_bitrate_type)temp_val)  == FALSE))
        {
          DSATUTIL_SET_VAL(DSAT_VENDOR_QCTER_IDX,0,0,0,old_qcter_val,NUM_TYPE)
          result = DSAT_ERROR;
        }
        else
#endif /* FEATURE_DATA_IS707 */
        {
          /* Change current baud and default in NV */
          temp_val = (dsat_num_item_type)dsatutil_get_val(
                                            DSAT_VENDOR_QCTER_IDX,0,0,NUM_TYPE);
          dsat_change_baud( (sio_bitrate_type)temp_val , FALSE);
        }
      }  /* brackets else in sio_is_baud_valid() == FALSE */
    }
  }
  return result;
}  /* dsatvend_exec_qcter_cmd() */


/*===========================================================================

FUNCTION DSATVEND_EXEC_QCPWRDN_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes at$qcpwrdn command.
  at$qcpwrdn is used to power down the terminal.

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_OK : if the command has been successfully executed
    DSAT_ERROR : if there was any problem in executing the command

SIDE EFFECTS
  Phone is powered off.

===========================================================================*/
/* ARGSUSED */
dsat_result_enum_type dsatvend_exec_qcpwrdn_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  /* function return                    */
  dsat_result_enum_type result = DSAT_OK;

  /*---------------------------------------
   Processing command with no args
   ----------------------------------------*/
  if (tok_ptr->op == (NA) ) 
  {
     if(TRUE == cm_rpm_check_reset_allowed())
    {
     sys_m_initiate_poweroff();
  }
    else
    {
      DS_AT_MSG0_HIGH("PowerDown is blocked");
      result = DSAT_ERROR;
    }
  }
  else if (tok_ptr->op == (NA|EQ|QU) )
  {
    /*  NO-OP but prevents ERROR --                     */
    result = DSAT_OK;
  }
  else  /*  Wrong Operator  */
  {
    result = DSAT_ERROR;
  }

  return result;

} /* dsatvend_exec_qcpwrdn_cmd () */



#ifdef FEATURE_DSAT_BREW_SUPPORT

/*===========================================================================

FUNCTION DSATVEND_EXEC_BREW_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes at$brew command.
  at$brew is used to transition the serial port to diagnostic
  monitor.This port change will be non-persistent. The port
  change will not be written on to EFS.

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_OK : if the command has been successfully executed
    DSAT_ERROR : if there was any problem in executing the command

SIDE EFFECTS
  Transitions the data port to Brew.

===========================================================================*/
/* ARGSUSED */
dsat_result_enum_type dsatvend_exec_brew_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  /* function return                    */
  dsat_result_enum_type result;

  /*result for diagcomm_sio_qcdmg       */
  boolean               brew_ret = FALSE;

  /*---------------------------------------
   Processing command with no args
   ----------------------------------------*/
  if (tok_ptr->op == (NA) ) 
  {
    rdm_device_enum_type device;

    DS_AT_MSG0_HIGH("Transitioning to BREW");
    
    /* Query the RDM for the port currently being used by Data Services */
    device = rdm_get_device(RDM_DATA_SRVC);
  
    /* Relinquish the port to BREW */
    brew_ret = rdm_assign_port_tmp ( RDM_BREW_SRVC, device, NULL );

    if ( brew_ret )
    {
      result = DSAT_DO_DM;
    }
    else
    {
      result = DSAT_ERROR;
    }
  }
  /*--------------------------------------
  Processing for a TEST command:
  ----------------------------------------*/
  else if (tok_ptr->op == (NA|EQ|QU) )
  {
    /*  NO-OP but prevents ERROR --                     */
    result = DSAT_OK;
  }
  else  /*  Wrong Operator  */
  {
    result = DSAT_ERROR;
  }

  return result;

} /* dsatvend_exec_brew_cmd () */

#endif /* FEATURE_DSAT_BREW_SUPPORT */


#ifdef FEATURE_DSAT_ETSI_DATA

/*===========================================================================

FUNCTION DSATVEND_EXEC_QCDGEN_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes at$qcdgen command.
  at$qcdgen is used to send a specified quantity of PS data as IP packet(s) 
  over a primary PDP-IP context already activated by +CGACT command.

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_OK : if the command has been successfully executed
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_ASYNC_CMD : if async processing to start data send in progress

SIDE EFFECTS
  Phone is powered off.

===========================================================================*/
/* ARGSUSED */
dsat_result_enum_type dsatvend_exec_qcdgen_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type result = DSAT_ASYNC_CMD; /* default function return */
  dsat_num_item_type cid = 0;             /* context ID */
  if (tok_ptr->op == (NA|EQ|AR) ) 
  {
    /* Write Command */

    /* If <cid> parameter in range 1-DS_UMTS_MAX_PDP_PROFILE_NUM has not been
       provided exit with diag message and ERROR result code */
    if ( !VALID_TOKEN(0) ||
         ATOI_OK != dsatutil_atoi(&cid, tok_ptr->arg[0], 10) ||
         cid < 1 ||
         cid > DS_UMTS_MAX_PDP_PROFILE_NUM )
    {
      DS_AT_MSG1_HIGH("Invalid <cid> specified: %d",cid);
      result = DSAT_ERROR;
    }
    else
    {
        dsat_num_item_type num_of_bytes = 0;
        
        if ( !VALID_TOKEN(1) ||
             ATOI_OK != dsatutil_atoi(&num_of_bytes, 
                                      tok_ptr->arg[1], 
                                      10)||
                            num_of_bytes < DSAT_DGEN_MIN_DATA_LEN )
        {
          DS_AT_MSG1_HIGH("Invalid <data_len> specified: %d",num_of_bytes);
          result = DSAT_ERROR;
        }
        else
        {
          SET_PENDING(DSAT_VENDOR_QCDGEN_IDX ,0, DSAT_PENDING_TRUE)
          
          result = ds_3gpp_rmsm_atif_qcdgen_handler ( (uint32)cid,dsat_get_current_subs_id(),
                                                      (uint32)num_of_bytes,NULL);
          if(result != DSAT_ASYNC_CMD)
          {
            DS_AT_MSG0_HIGH("Unsuccessful Result from RMSM");
            SET_PENDING(DSAT_VENDOR_QCDGEN_IDX ,0, DSAT_PENDING_FALSE)
            result = DSAT_ERROR;
          }
        }
     }
   } /* End write command */
  else if (tok_ptr->op == (NA|EQ|QU) )
  {
    /* Test Command */
    result = DSAT_OK;
  }
  else  
  {
    /* Read Command */
    result = DSAT_ERROR;
  }

  return result;
} /* dsatvend_exec_qcdgen_cmd () */


/*===========================================================================

FUNCTION DSATVEND_EXEC_QCPDPCFGEXT_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes at$qcpdpcfgext command.
  at$qcpdpcfgext is used for setting and getting MBIM context type.

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_OK : if the command has been successfully executed
    DSAT_ERROR : if there was any problem in executing the command

SIDE EFFECTS
  None

===========================================================================*/
/*ARGSUSED*/
dsat_result_enum_type dsatvend_exec_qcpdpcfgext_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
#define MBIM_CONTEXT_MAX_VAL_BYTES 4
  
  dsat_num_item_type  profile_id    = 0;
  dsat_num_item_type  mbim_context  = 0;
  dsat_num_item_type  roaming_dis_flag  = 0;
  dsat_num_item_type  roaming_dis_tmr  = 0;
    
  ds_umts_pdp_profile_status_etype      result = DS_UMTS_PDP_SUCCESS;
  dsat_configure_pdp_result_type        result_code = DSAT_WRITE_SUCCESS ;

  if (tok_ptr->op == (NA|EQ|AR) ) 
  {
    /* Check the number of arguments.*/
    if ( tok_ptr->args_found > parse_table->compound)
    {
      result_code = result_code | DSAT_INVALID_NUMBER_OF_ARGS ;
    }
    else
    {
      if( !(VALID_TOKEN(0) && ( VALID_TOKEN(1) || VALID_TOKEN(2)|| VALID_TOKEN(3))) )
      {
        result_code = result_code | DSAT_INVALID_TOKENS ;
      }
      else
      {
        if( ATOI_OK != dsatutil_atoi(&profile_id,tok_ptr->arg[0],10))
        {
          result_code = result_code | DSAT_ARGS_ATOI_FAILURE ;
        }
        else
        {
          if( !((profile_id > 0) && (profile_id <= DS_UMTS_MAX_PDP_PROFILE_NUM)) )
          {
            result_code = result_code | DSAT_INVALID_PROFILE_ID ;
          }
          else
          {
            if( VALID_TOKEN(1))
            {
              if(ATOI_OK == dsatutil_atoi(&mbim_context,tok_ptr->arg[1],10))
              {
            /*Write MBIM context value */
                result = ds_umts_set_pdp_profile_user_app_data_per_subs( (uint16)profile_id,
                                                                         dsat_get_current_subs_id(),
                                                                         (uint32)mbim_context);
                if( DS_UMTS_PDP_SUCCESS != result )
                {
                  result_code = result_code | DSAT_MBIM_WRITE_FAILURE ;
                }
              }else
              {
               result_code = result_code | DSAT_ARGS_ATOI_FAILURE ;
              }
            }
            if(result_code == DSAT_WRITE_SUCCESS && VALID_TOKEN(2) )
            {
              if(ATOI_OK == dsatutil_atoi(&roaming_dis_flag,tok_ptr->arg[2],10) && 
                (roaming_dis_flag == 0 || roaming_dis_flag == 1))
              {
             /*Write Roaming Disable Flag  value */
                ds_profile_status_etype           profile_status;
                ds_profile_hndl_type              profile_hndl;
                ds_profile_info_type              param_info;
                uint8                             temp_val;
                
                ETSIPKT_BEGIN_PROFILE_TRANSACTION(profile_id);
                if(profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
                {
                  return DSAT_ERROR;
                }
                temp_val =  (uint8)roaming_dis_flag;
                param_info.len     = sizeof(temp_val);
                param_info.buf     = (void*)&temp_val;
                ETSIPKT_SET_PROFILE_PARAM(profile_hndl,DS_PROFILE_3GPP_PROFILE_PARAM_ROAMING_DISALLOWED,param_info);
                if(profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
                {
                  result_code = result_code | DSAT_ROAMING_FLAG_WRITE_FAILUE ;
                }
                ETSIPKT_END_PROFILE_TRANSACTION(profile_hndl);
                
              }else
              {
                result_code = result_code | DSAT_ARGS_ATOI_FAILURE ;
              }
            }
            if(result_code == DSAT_WRITE_SUCCESS && VALID_TOKEN(3) )
            {
              if(ATOI_OK == dsatutil_atoi(&roaming_dis_tmr,tok_ptr->arg[3],10) && 
                (roaming_dis_tmr < 256))
              {
             /*Write Roaming Disable Flag  timer value */
                ds_profile_status_etype           profile_status;
                ds_profile_hndl_type              profile_hndl;
                ds_profile_info_type              param_info;
                uint8                             temp_val;
                
                ETSIPKT_BEGIN_PROFILE_TRANSACTION(profile_id);
                if(profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
                {
                  return DSAT_ERROR;
                }
                temp_val =  (uint8)roaming_dis_tmr;
                param_info.len     = sizeof(temp_val);
                param_info.buf     = (void*)&temp_val;
                ETSIPKT_SET_PROFILE_PARAM(profile_hndl,DS_PROFILE_3GPP_PROFILE_PARAM_PDN_DISCON_WAIT_TIME,param_info);
                if(profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
                {
                  result_code = result_code | DSAT_ROAMING_TIME_WRITE_FAILUE ;
                }
                ETSIPKT_END_PROFILE_TRANSACTION(profile_hndl);

              }else
              {
                result_code = result_code | DSAT_ARGS_ATOI_FAILURE ;
              }
            }
            
          }
        }
      }
    }
    if(result_code != DSAT_WRITE_SUCCESS)
    {
      DS_AT_MSG1_HIGH("$QCPDPCFGEXT: result_code = %d", result_code);
      return DSAT_ERROR;
    }
  }
  else if (tok_ptr->op == (NA|EQ|QU) )
  {
    res_buff_ptr->used = (word) snprintf((char *)res_buff_ptr->data_ptr,
                                               res_buff_ptr->size,
                                               "$QCPDPCFGEXT: (%d-%d),%d,(0-1),(0-255)\n",
                                               1 ,DS_UMTS_MAX_PDP_PROFILE_NUM,
                                               MBIM_CONTEXT_MAX_VAL_BYTES);
  }
  else if(tok_ptr->op == (NA|QU))
  {
    uint16  profile_counter;
    ds_profile_status_etype           profile_status;
    ds_profile_hndl_type              profile_hndl;
    ds_profile_info_type              param_info;
    uint8                             temp_val;

    
    res_buff_ptr->used = 0;
    
    for(profile_counter = 1;profile_counter <  DS_UMTS_MAX_PDP_PROFILE_NUM + 1;profile_counter++)
    {
      ETSIPKT_BEGIN_PROFILE_TRANSACTION(profile_counter);
      
      if(profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
      {
        continue;
      }
      
      res_buff_ptr->used += 
        (word) snprintf((char*)&res_buff_ptr->data_ptr[res_buff_ptr->used],
                               res_buff_ptr->size - res_buff_ptr->used,
                               "$QCPDPCFGEXT: %d,",
                                profile_counter);
    
      result = ds_umts_get_pdp_profile_user_app_data_per_subs( profile_counter,
                                                      dsat_get_current_subs_id(),
                                                      (uint32 *) &mbim_context);
      if(DS_UMTS_PDP_SUCCESS == result )
      {
        res_buff_ptr->used += 
        (word) snprintf((char*)&res_buff_ptr->data_ptr[res_buff_ptr->used],
                               res_buff_ptr->size - res_buff_ptr->used,
                               "%d",mbim_context);
      }
      res_buff_ptr->used += 
        (word) snprintf((char*)&res_buff_ptr->data_ptr[res_buff_ptr->used],
                               res_buff_ptr->size - res_buff_ptr->used,
                               ",");

      param_info.len     = sizeof(temp_val);
      param_info.buf     = (void*)&temp_val;
      
      ETSIPKT_GET_PROFILE_PARAM(profile_hndl,DS_PROFILE_3GPP_PROFILE_PARAM_ROAMING_DISALLOWED,param_info);
      if(profile_status == DS_PROFILE_REG_RESULT_SUCCESS)
      {
        res_buff_ptr->used += 
        (word) snprintf((char*)&res_buff_ptr->data_ptr[res_buff_ptr->used],
                               res_buff_ptr->size - res_buff_ptr->used,
                               "%d",temp_val);
      }
      res_buff_ptr->used += 
        (word) snprintf((char*)&res_buff_ptr->data_ptr[res_buff_ptr->used],
                               res_buff_ptr->size - res_buff_ptr->used,
                               ",");

      ETSIPKT_GET_PROFILE_PARAM(profile_hndl,DS_PROFILE_3GPP_PROFILE_PARAM_PDN_DISCON_WAIT_TIME,param_info);
      if(profile_status == DS_PROFILE_REG_RESULT_SUCCESS)
      {
        res_buff_ptr->used += 
        (word) snprintf((char*)&res_buff_ptr->data_ptr[res_buff_ptr->used],
                               res_buff_ptr->size - res_buff_ptr->used,
                               "%d",temp_val);
      }
      res_buff_ptr->used += 
        (word) snprintf((char*)&res_buff_ptr->data_ptr[res_buff_ptr->used],
                               res_buff_ptr->size - res_buff_ptr->used,
                               "\n");
     ETSIPKT_END_PROFILE_TRANSACTION(profile_hndl);
    }
    
    if(res_buff_ptr->used == 0)
    {
      res_buff_ptr->used =(word) snprintf((char*)&res_buff_ptr->data_ptr[0],
                                            res_buff_ptr->size,
                                            "$QCPDPCFGEXT: ");
    }

  }
  else
  {
      /* Not Supported */
      return DSAT_ERROR;
  }
  
  return DSAT_OK;
} /* dsatvend_exec_qcpdpcfgext_cmd */

/*===========================================================================

FUNCTION DSATVEND_EXEC_QCPDPCFGE_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes at$qcpdpcfge command.
  at$qcpdpcfge is used to edit APN values in APN table. 

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_OK : if the command has been successfully executed
    DSAT_ERROR : if there was any problem in executing the command

SIDE EFFECTS
  None

===========================================================================*/
/*ARGSUSED*/
dsat_result_enum_type dsatvend_exec_qcpdpcfge_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  #define DEF_MAX_PDN_CONN_PER_BLK       1023
  #define DEF_MAX_PDN_CONN_TIME           300 //300 seconds 
  #define DEF_PDN_REQ_WAIT_TIME             0 //0 seconds
  #define MAX_PDN_CONN_TIME              3600
  #define MIN_OPRT_RESV_PCO             65280 /* FF00H to FFFFH reserved for operator specific use spec 3GPP TS 24.008 Table 10.5.154*/
  #define MAX_OPRT_RESV_PCO             65535
  #define MAX_MNC_MCC_VAL                 999

  dsat_num_item_type       profile_id    = 0;
  dsat_num_item_type       flag_i_val    = 0;
  dsat_num_item_type       apn_class     = DSAT_APN_CLASS_ONE;
  boolean      flag_val      = FALSE;      /* Used to Update an APN  flag value      */
  dsat_num_item_type       timer_val     = 0;      /* Used to Update  inactivity timer value */
  boolean      pre_flag_val  = FALSE;      /* Used to restore an APN  flag value     */
  uint8                    pre_apn_class = DSAT_APN_CLASS_ONE; /* Used to restore an APN  Class value     .*/
  dsat_num_item_type       apn_bearer    = DS_UMTS_PDP_PROFILE_APN_BEARER_FLAG_ALL;
  dsat_num_item_type       max_pdn_conn_per_blk = DEF_MAX_PDN_CONN_PER_BLK;
  dsat_num_item_type       max_pdn_req_wait_time = DEF_PDN_REQ_WAIT_TIME;
  dsat_num_item_type       max_pdn_conn_time = DEF_MAX_PDN_CONN_TIME;
  dsat_num_item_type       emergency_calls_flag_val  = FALSE;/* Flag used to indentify if IMS emergency calls can be made using profile*/
  dsat_num_item_type       oprt_reserved_pco;/*Indicates Operator Reserved Pco id to be used in the PCO*/
  dsat_num_item_type       mcc;           /*Indicates Mcc to be used in the PCO*/
  ds_profile_3gpp_mnc_type mnc;           /*Indicates Mnc to be used in the PCO*/
  ds_umts_pdp_profile_status_etype apn_result = DS_UMTS_PDP_SUCCESS;
  dsat_num_item_type       mnc_val;
  dsat_error_info_s_type                    err_info;

  err_info.errval   =  DSAT_ERR_NO_ERROR;
  err_info.arg_num  =  0; 
  

  if (tok_ptr->op == (NA|EQ|AR) ) 
  {
    /* Check the number of arguments. 5th Parameter is optional. */
    if ( (tok_ptr->args_found > parse_table->compound) || (tok_ptr->args_found < 4))
    {
      err_info.errval = DSAT_ERR_INVALID_NUM_OF_ARGS;
      goto send_error;
    }

    if( VALID_TOKEN(0) && VALID_TOKEN(1) && VALID_TOKEN(2) && VALID_TOKEN(3))
    {
      if((ATOI_OK ==dsatutil_atoi(&profile_id,tok_ptr->arg[0],10))
          &&
         (ATOI_OK ==dsatutil_atoi(&flag_i_val,tok_ptr->arg[1],10))
          &&
         (ATOI_OK ==dsatutil_atoi(&timer_val,tok_ptr->arg[2],10))
           &&
         (ATOI_OK ==dsatutil_atoi(&apn_class,tok_ptr->arg[3],10))
        )
      {
        /*Checking Profile ID range */
        if( (profile_id > 0) && (profile_id <= DS_UMTS_MAX_PDP_PROFILE_NUM) )
        {
          /*Checking Flag validity */
          if((flag_i_val != 0) && (flag_i_val != 1 ))
          {
            err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
            err_info.arg_num = 1;
            goto send_error;
          }
          if(flag_i_val == 1)
          {
            flag_val = TRUE;
          }
          else
          {
            flag_val = FALSE;
          }
          /*Checking Timer range */
          if( timer_val > 122820 )
          {
            err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
            err_info.arg_num = 2;
            goto send_error;
          }
          if( apn_class > DSAT_APN_CLASS_MAX )
          {
            err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
            err_info.arg_num = 3;
            goto send_error;
          }
          if(VALID_TOKEN(4))
          {
            if(ATOI_OK != dsatutil_atoi(&apn_bearer,tok_ptr->arg[4],10))
            {
              err_info.errval = DSAT_ERR_ATOI_CONVERSION_FAILURE;
              err_info.arg_num = 4;
              goto send_error;
            }
            if(apn_bearer > DS_UMTS_PDP_PROFILE_APN_BEARER_FLAG_ALL)
            {
              err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
              err_info.arg_num = 4;
              goto send_error;
            }
          }
          if(VALID_TOKEN(5))
          {
            if(ATOI_OK != dsatutil_atoi(&max_pdn_conn_per_blk,tok_ptr->arg[5],10))
            {
              return DSAT_ERROR;
            }
            if( max_pdn_conn_per_blk > DEF_MAX_PDN_CONN_PER_BLK )
            {
              return DSAT_ERROR;
            }
          }
          if(VALID_TOKEN(6))
          {
            if(ATOI_OK != dsatutil_atoi(&max_pdn_conn_time,tok_ptr->arg[6],10))
            {
              return DSAT_ERROR;
            }
            if( max_pdn_conn_time > MAX_PDN_CONN_TIME )
            {
              return DSAT_ERROR;
            }
          }
          if(VALID_TOKEN(7))
          {
            if(ATOI_OK != dsatutil_atoi(&max_pdn_req_wait_time,tok_ptr->arg[7],10))
            {
              return DSAT_ERROR;
            }
            if(max_pdn_req_wait_time > DEF_MAX_PDN_CONN_PER_BLK)
            {
              return DSAT_ERROR;
            }
          }
          if(VALID_TOKEN(8))
          {
            if((ATOI_OK != dsatutil_atoi(&emergency_calls_flag_val,tok_ptr->arg[8],10)) 
                   || (emergency_calls_flag_val > 1))
            {
              return DSAT_ERROR;
            }
          }
          if(VALID_TOKEN(9))
          {
            if((ATOI_OK != dsatutil_atoi(&oprt_reserved_pco,tok_ptr->arg[9],10))
                || (oprt_reserved_pco < MIN_OPRT_RESV_PCO)
                || (oprt_reserved_pco > MAX_OPRT_RESV_PCO))
            {
              return DSAT_ERROR;
            }
          }
          if(VALID_TOKEN(10))
          {
            if((ATOI_OK != dsatutil_atoi(&mcc,tok_ptr->arg[10],10))
                 || (mcc > MAX_MNC_MCC_VAL))
            {
              return DSAT_ERROR;
            }
          }
          if(VALID_TOKEN(11))
          {
            mnc.mnc_includes_pcs_digit = FALSE;
            if(PLMN_STR_MNC_LEN == strlen((char*)tok_ptr->arg[11]))
            {
              mnc.mnc_includes_pcs_digit = TRUE;
            }
            if(ATOI_OK != dsatutil_atoi(&mnc_val,tok_ptr->arg[11],10))
            {
              return DSAT_ERROR;
            }
            mnc.mnc_digits = (uint16)mnc_val;
            if(mnc.mnc_digits > MAX_MNC_MCC_VAL)
            {
              return DSAT_ERROR;
            }
          }
        }
        else
        {
          err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
          err_info.arg_num = 0;
          goto send_error;
        }
      }
      else
      {
        DS_AT_MSG2_ERROR("$QCPDPCFGE: AtoI Conversion Failed Profile ID %s Flag %s ",
                   &tok_ptr->arg[0],
                   &tok_ptr->arg[1]);
        DS_AT_MSG2_ERROR(" Timer  %s Apn  Class %s ",
                   &tok_ptr->arg[2],
                   &tok_ptr->arg[3]);

        return DSAT_ERROR;
      }
    }
    else
    {
      err_info.errval = DSAT_ERR_INVALID_TOKENS;
      goto send_error;
    }
    /*Read Current flag value*/
    apn_result = ds_umts_get_pdp_profile_apn_disable_flag_per_subs( 
                                                          (uint16)profile_id,
                                                          dsat_get_current_subs_id(),
                                                          &pre_flag_val);
    /*Read Current Class value*/
    apn_result = ds_umts_get_pdp_profile_apn_class_per_subs((uint16)profile_id,
                                                            dsat_get_current_subs_id(),
                                                            (uint8 *)&pre_apn_class);
    if(DS_UMTS_PDP_SUCCESS != apn_result )
    {
      err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
      err_info.arg_num = 0;
      goto send_error;
    }
    /*Write APN flag value */
    apn_result = ds_umts_set_pdp_profile_apn_disable_flag_per_subs( (uint16)profile_id,
                                                                    dsat_get_current_subs_id(),
                                                                    flag_val);
    if(DS_UMTS_PDP_SUCCESS != apn_result )
    {
       DS_AT_MSG0_ERROR(" $QCPDPCFGE: APN Flag write Failed ");
       return DSAT_ERROR;
    }
      /*Write APN Class value */
      apn_result = ds_umts_set_pdp_profile_apn_class_per_subs((uint16)profile_id,
                                                               dsat_get_current_subs_id(),
                                                               (uint8 )apn_class);
    if(DS_UMTS_PDP_SUCCESS != apn_result)
    {
      /*Restore APN flag value */
      apn_result = ds_umts_set_pdp_profile_apn_disable_flag_per_subs((uint16)profile_id,
                                                                      dsat_get_current_subs_id(),
                                                                      pre_flag_val);
      DS_AT_MSG0_ERROR("$QCPDPCFGE: APN  Class write Failed ");
      return DSAT_ERROR;
    }
    /*Write APN Timer value */
    apn_result = ds_umts_set_pdp_profile_inactivity_timer_val_per_subs((uint16)profile_id,
                                                                        dsat_get_current_subs_id(),
                                                                        timer_val);
    if(DS_UMTS_PDP_SUCCESS != apn_result )
    {
      DS_AT_MSG0_ERROR("$QCPDPCFGE: APN Timer write Failed ");
      
      /*Restore APN flag and APN Class value */
      apn_result = ds_umts_set_pdp_profile_apn_disable_flag_per_subs((uint16)profile_id,
                                                                      dsat_get_current_subs_id(),
                                                                      pre_flag_val);
      apn_result = ds_umts_set_pdp_profile_apn_class_per_subs((uint16)profile_id,
                                                              dsat_get_current_subs_id(),
                                                              (uint8 )pre_apn_class);
      
      if(DS_UMTS_PDP_SUCCESS != apn_result )
      {
        DS_AT_MSG0_ERROR("$QCPDPCFGE: APN  Restore Failed ");
        return DSAT_ERROR;
      }
      return DSAT_ERROR;
     }

     /* Call 3GPP Callback function only if APN_Disable_flag value if different */
     if ((pre_flag_val != flag_val) &&
         (dsat_apn_disable_flag_chg_cb_fptr != NULL))
     {
       DS_AT_MSG0_HIGH("$QCPDPCFGE:MH Callback for APN_Disable_flag_chg");

       dsat_apn_disable_flag_chg_cb_fptr((uint16)profile_id, flag_val);
     }
    /* Write APN bearer value */
    if(VALID_TOKEN(4))
    {
      apn_result = ds_umts_set_pdp_profile_apn_bearer_per_subs(profile_id,dsat_get_current_subs_id(),apn_bearer);
      if(DS_UMTS_PDP_SUCCESS != apn_result )
      {
        DS_AT_MSG0_ERROR("$QCPDPCFGE: APN  Bearer write Failed ");
        return DSAT_ERROR;
      }
    }
    if(VALID_TOKEN(5) || VALID_TOKEN(6) || VALID_TOKEN(7) || 
        VALID_TOKEN(8) || VALID_TOKEN(9) || VALID_TOKEN(10) || VALID_TOKEN(11))
    {
      ds_profile_status_etype           profile_status;
      ds_profile_hndl_type              profile_hndl;
      ds_profile_info_type              param_info;
      ETSIPKT_BEGIN_PROFILE_TRANSACTION(profile_id);
      if(profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
      {
        return DSAT_ERROR;
      }
      if(VALID_TOKEN(5))
      {
        param_info.len     = sizeof(uint16);
        param_info.buf     = (void*)&max_pdn_conn_per_blk;
        ETSIPKT_SET_PROFILE_PARAM(profile_hndl,DS_PROFILE_3GPP_PROFILE_MAX_PDN_CONN_PER_BLOCK,param_info);
        if(profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
        {
          ETSIPKT_END_PROFILE_TRANSACTION(profile_hndl);
          return DSAT_ERROR;
        }
      }
      if(VALID_TOKEN(6))
      {
        param_info.len     = sizeof(uint16);
        param_info.buf     = (void*)&max_pdn_conn_time;
        ETSIPKT_SET_PROFILE_PARAM(profile_hndl,DS_PROFILE_3GPP_PROFILE_MAX_PDN_CONN_TIMER,param_info);
        if(profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
        {
          ETSIPKT_END_PROFILE_TRANSACTION(profile_hndl);
          return DSAT_ERROR;
        }
      }
      if(VALID_TOKEN(7))
      {
        param_info.len     = sizeof(uint16);
        param_info.buf     = (void*)&max_pdn_req_wait_time;
        ETSIPKT_SET_PROFILE_PARAM(profile_hndl,DS_PROFILE_3GPP_PROFILE_PDN_REQ_WAIT_TIMER,param_info);
        if(profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
        {
          ETSIPKT_END_PROFILE_TRANSACTION(profile_hndl);
          return DSAT_ERROR;
        }
      }
      if(VALID_TOKEN(8))
      {
        param_info.len     = sizeof(boolean);
        param_info.buf     = (void*)&emergency_calls_flag_val;
        ETSIPKT_SET_PROFILE_PARAM(profile_hndl,DS_PROFILE_3GPP_PROFILE_PARAM_EMERGENCY_CALLS_SUPPORTED,param_info);
        if(profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
        {
          ETSIPKT_END_PROFILE_TRANSACTION(profile_hndl);
          return DSAT_ERROR;
        }
      }
      if(VALID_TOKEN(9))
      {
        param_info.len     = sizeof(uint16);
        param_info.buf     = (void*)&oprt_reserved_pco;
        ETSIPKT_SET_PROFILE_PARAM(profile_hndl,DS_PROFILE_3GPP_PROFILE_PARAM_OPERATOR_RESERVED_PCO,param_info);
        if(profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
        {
          ETSIPKT_END_PROFILE_TRANSACTION(profile_hndl);
          return DSAT_ERROR;
        }
      }
      if(VALID_TOKEN(10))
      {
        param_info.len     = sizeof(uint16);
        param_info.buf     = (void*)&mcc;
        ETSIPKT_SET_PROFILE_PARAM(profile_hndl,DS_PROFILE_3GPP_PROFILE_PARAM_MCC,param_info);
        if(profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
        {
          ETSIPKT_END_PROFILE_TRANSACTION(profile_hndl);
          return DSAT_ERROR;
        }
      }
      if(VALID_TOKEN(11))
      {
        param_info.len     = sizeof(ds_profile_3gpp_mnc_type);
        param_info.buf     = (void*)&mnc;
        ETSIPKT_SET_PROFILE_PARAM(profile_hndl,DS_PROFILE_3GPP_PROFILE_PARAM_MNC,param_info);
        if(profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
        {
          ETSIPKT_END_PROFILE_TRANSACTION(profile_hndl);
          return DSAT_ERROR;
        }
      }
      ETSIPKT_END_PROFILE_TRANSACTION(profile_hndl);

    }
    
  }
  else if (tok_ptr->op == (NA|EQ|QU) )
  {
/*  
  APN bearer is a bit mask and the input can be a combination of bit masks below. 
  
  #define  DS_UMTS_PDP_PROFILE_APN_BEARER_FLAG_G   (0x1)   
  #define  DS_UMTS_PDP_PROFILE_APN_BEARER_FLAG_W   (0x2)  
  #define  DS_UMTS_PDP_PROFILE_APN_BEARER_FLAG_L   (0x4)   
  #define  DS_UMTS_PDP_PROFILE_APN_BEARER_FLAG_T   (0x8)
  #define  DS_UMTS_PDP_PROFILE_APN_BEARER_FLAG_ALL (0xff)  

  Hence the allowed range is 0x1 - 0xF(G+W+L+T) and 0xFF for all.
  
*/
  
    res_buff_ptr->used = (word) snprintf((char *)res_buff_ptr->data_ptr,
                                               res_buff_ptr->size,
                                               "$QCPDPCFGE: (%d - %d),(%d - %d ),(%d - %d),(%d - %d),(%d - %d,%d),(%d - %d),(%d - %d),(%d - %d),(%d - %d),(%d - %d),(%d - %d),(%d - %d)\n",
                                               1 ,DS_UMTS_MAX_PDP_PROFILE_NUM,
                                               0,1,
                                               0,122820,
                                               DSAT_APN_CLASS_ONE,DSAT_APN_CLASS_MAX,
                                               DS_UMTS_PDP_PROFILE_APN_BEARER_FLAG_G,15,DS_UMTS_PDP_PROFILE_APN_BEARER_FLAG_ALL,
                                               0,DEF_MAX_PDN_CONN_PER_BLK,
                                               0,MAX_PDN_CONN_TIME,
                                               0,DEF_MAX_PDN_CONN_PER_BLK,
                                               0,1,
                                               MIN_OPRT_RESV_PCO,MAX_OPRT_RESV_PCO,
                                               0,MAX_MNC_MCC_VAL,
                                               0,MAX_MNC_MCC_VAL);

  }
  else if(tok_ptr->op == (NA|QU))
  {
    uint16  profile_counter;
    uint32  inac_timer;
    
    for(profile_counter = 1;profile_counter <  DS_UMTS_MAX_PDP_PROFILE_NUM + 1;profile_counter++)
    {
    
      apn_result = ds_umts_get_pdp_profile_apn_disable_flag_per_subs( 
                                                            profile_counter,
                                                            dsat_get_current_subs_id(),
                                                            &flag_val);
      if(DS_UMTS_PDP_SUCCESS == apn_result )
      {      
        apn_result = ds_umts_get_pdp_profile_inactivity_timer_val_per_subs(
                                                                  profile_counter,
                                                                  dsat_get_current_subs_id(),
                                                                  (uint32 *)&inac_timer);
        if(DS_UMTS_PDP_SUCCESS == apn_result )
        {
          apn_result = ds_umts_get_pdp_profile_apn_class_per_subs((uint16)profile_counter,
                                                                   dsat_get_current_subs_id(),
                                                                   &pre_apn_class);
          if(DS_UMTS_PDP_SUCCESS == apn_result )
          {
            apn_result = ds_umts_get_pdp_profile_apn_bearer_per_subs(profile_counter,dsat_get_current_subs_id(),(uint8*)&apn_bearer);
            if(DS_UMTS_PDP_SUCCESS == apn_result )
            {
              ds_profile_status_etype           profile_status;
              ds_profile_hndl_type              profile_hndl;
              ds_profile_info_type              param_info;
              max_pdn_conn_per_blk  = DEF_MAX_PDN_CONN_PER_BLK;
              max_pdn_req_wait_time = DEF_PDN_REQ_WAIT_TIME;
              max_pdn_conn_time     = DEF_MAX_PDN_CONN_TIME;
              emergency_calls_flag_val   = 0;
              oprt_reserved_pco          = MAX_OPRT_RESV_PCO;
              mcc                        = 0;
              mnc.mnc_includes_pcs_digit = FALSE;
              mnc.mnc_digits             = 0;
              ETSIPKT_BEGIN_PROFILE_TRANSACTION(profile_counter);
              if(profile_status == DS_PROFILE_REG_RESULT_SUCCESS)
              {
                param_info.len     = sizeof(uint16);
                param_info.buf     = (void*)&max_pdn_conn_per_blk;
                ETSIPKT_GET_PROFILE_PARAM(profile_hndl,DS_PROFILE_3GPP_PROFILE_MAX_PDN_CONN_PER_BLOCK,param_info);
                if(profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
                {
                  ETSIPKT_END_PROFILE_TRANSACTION(profile_hndl);
                  return DSAT_ERROR;
                }
                param_info.len     = sizeof(uint16);
                param_info.buf     = (void*)&max_pdn_conn_time;
                ETSIPKT_GET_PROFILE_PARAM(profile_hndl,DS_PROFILE_3GPP_PROFILE_MAX_PDN_CONN_TIMER,param_info);
                if(profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
                {
                  ETSIPKT_END_PROFILE_TRANSACTION(profile_hndl);
                  return DSAT_ERROR;
                }
                param_info.len     = sizeof(uint16);
                param_info.buf     = (void*)&max_pdn_req_wait_time;
                ETSIPKT_GET_PROFILE_PARAM(profile_hndl,DS_PROFILE_3GPP_PROFILE_PDN_REQ_WAIT_TIMER,param_info);
                if(profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
                {
                  ETSIPKT_END_PROFILE_TRANSACTION(profile_hndl);
                  return DSAT_ERROR;
                }
                param_info.len     = sizeof(boolean);
                param_info.buf     = (void*)&emergency_calls_flag_val;
                ETSIPKT_GET_PROFILE_PARAM(profile_hndl,DS_PROFILE_3GPP_PROFILE_PARAM_EMERGENCY_CALLS_SUPPORTED,param_info);
                if(profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
                {
                  ETSIPKT_END_PROFILE_TRANSACTION(profile_hndl);
                  return DSAT_ERROR;
                }
                param_info.len     = sizeof(uint16);
                param_info.buf     = (void*)&oprt_reserved_pco;
                ETSIPKT_GET_PROFILE_PARAM(profile_hndl,DS_PROFILE_3GPP_PROFILE_PARAM_OPERATOR_RESERVED_PCO,param_info);
                if(profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
                {
                  ETSIPKT_END_PROFILE_TRANSACTION(profile_hndl);
                  return DSAT_ERROR;
                }
                param_info.len     = sizeof(uint16);
                param_info.buf     = (void*)&mcc;
                ETSIPKT_GET_PROFILE_PARAM(profile_hndl,DS_PROFILE_3GPP_PROFILE_PARAM_MCC,param_info);
                if(profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
                {
                  ETSIPKT_END_PROFILE_TRANSACTION(profile_hndl);
                  return DSAT_ERROR;
                }
                param_info.len     = sizeof(ds_profile_3gpp_mnc_type);
                param_info.buf     = (void*)&mnc;
                ETSIPKT_GET_PROFILE_PARAM(profile_hndl,DS_PROFILE_3GPP_PROFILE_PARAM_MNC,param_info);
                if(profile_status != DS_PROFILE_REG_RESULT_SUCCESS)
                {
                  ETSIPKT_END_PROFILE_TRANSACTION(profile_hndl);
                  return DSAT_ERROR;
                }
                ETSIPKT_END_PROFILE_TRANSACTION(profile_hndl);
              }
              res_buff_ptr->used += 
                (word) snprintf((char*)&res_buff_ptr->data_ptr[res_buff_ptr->used],
                                 res_buff_ptr->size - res_buff_ptr->used,
                                 "$QCPDPCFGE: %d , %d , %u , %d ,%d,%d,%d,%d,%d,%d,%d,%d\n",
                                 profile_counter,
                                 flag_val,
                                 (unsigned int)inac_timer,
                                 pre_apn_class,
                                 apn_bearer,
                                 max_pdn_conn_per_blk,
                                 max_pdn_conn_time,
                                 max_pdn_req_wait_time,
                                 emergency_calls_flag_val,
                                 oprt_reserved_pco,
                                 mcc,
                                 mnc.mnc_digits);
            }   
          }
        }
      }
    }
  }
  else
  {
    err_info.errval = DSAT_ERR_INVALID_SYNTAX;
    goto send_error;
  }

  return DSAT_OK;

send_error:
  if( err_info.errval == DSAT_ERR_INVALID_NUM_OF_ARGS || 
      err_info.errval == DSAT_ERR_INVALID_TOKENS || 
      err_info.errval == DSAT_ERR_INVALID_SYNTAX )
  {
    DS_AT_MSG1_ERROR("Error:%d, ar",err_info.errval);
  }
  else
  {
    DS_AT_MSG2_ERROR("Error:%d, ar",err_info.errval,err_info.arg_num);
  }
  return DSAT_ERROR;
}/*dsatvend_exec_qcpdpcfge_cmd*/

/*===========================================================================

FUNCTION DSATVEND_EXEC_QCPDPIMSCFGE_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes at$qcpdpimscfge command.
  at$qcpdpimscfge is used to edit PDP profile registry . 

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_OK : if the command has been successfully executed
    DSAT_ERROR : if there was any problem in executing the command

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatvend_exec_qcpdpimscfge_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_num_item_type       profile_id       = 0;
  dsat_num_item_type       flag_i_val[3]    = {0,0,0};
  boolean                  flag_val[3]      = {FALSE,FALSE,FALSE};
  ds_umts_pdp_profile_status_etype result = DS_UMTS_PDP_SUCCESS;
  dsat_error_info_s_type   err_info;

  err_info.errval  = DSAT_ERR_NO_ERROR;
  err_info.arg_num = 0;

  if (tok_ptr->op == (NA|EQ|AR) ) 
  {
    /* Check the number of arguments. */
    if ( (tok_ptr->args_found > parse_table->compound) || (tok_ptr->args_found < 2) )
    {
      err_info.errval = DSAT_ERR_INVALID_NUM_OF_ARGS;
      goto send_error;
    }
    if( !(VALID_TOKEN(0) && (VALID_TOKEN(1) || VALID_TOKEN(2) || VALID_TOKEN(3))))
    {
      err_info.errval = DSAT_ERR_INVALID_TOKENS;
      goto send_error;
    }
    if((ATOI_OK ==dsatutil_atoi(&profile_id,tok_ptr->arg[0],10)) && 
        ( (profile_id > 0) && (profile_id <= DS_UMTS_MAX_PDP_PROFILE_NUM) ))
    {
      if(VALID_TOKEN(1) && (ATOI_OK ==dsatutil_atoi(&flag_i_val[0],tok_ptr->arg[1],10)))
      {
        if(0 == flag_i_val[0] || 1 == flag_i_val[0])
        {
          result= ds_umts_set_pdp_profile_request_pcscf_address_flag_per_subs(
                    (uint16)profile_id,
                    dsat_get_current_subs_id(),
                    ((0 == flag_i_val[0])? FALSE : TRUE));
          if(DS_UMTS_PDP_SUCCESS != result )
          {
            DS_AT_MSG1_ERROR("$QCPDPIMSCFGE: Address flag write failed  %d", result);
            return DSAT_ERROR;
          }
        }
        else
        {
          err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
          err_info.arg_num = 1;
          goto send_error;
        }
      }
      if(VALID_TOKEN(2) && (ATOI_OK ==dsatutil_atoi(&flag_i_val[1],tok_ptr->arg[2],10)))
      {
        if(0 == flag_i_val[1] || 1 == flag_i_val[1])
        {
          result = ds_umts_set_pdp_profile_request_pcscf_address_using_dhcp_flag_per_subs(
                    (uint16)profile_id,
                    dsat_get_current_subs_id(),
                    ((0 == flag_i_val[1])? FALSE : TRUE));
          if(DS_UMTS_PDP_SUCCESS != result )
          {
            DS_AT_MSG1_ERROR("$QCPDPIMSCFGE: DHCP flag write failed  %d", result);
            return DSAT_ERROR;
          }
        }
        else
        {
          err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
          err_info.arg_num = 2;
          goto send_error;
         }
      }
      if(VALID_TOKEN(3) && (ATOI_OK ==dsatutil_atoi(&flag_i_val[2],tok_ptr->arg[3],10)))
      {
        if(0 == flag_i_val[2] || 1 == flag_i_val[2])
        {
          result = ds_umts_set_pdp_profile_im_cn_flag_per_subs(
                   (uint16)profile_id,
                   dsat_get_current_subs_id(),
                   ((0 == flag_i_val[2])? FALSE : TRUE));
          if(DS_UMTS_PDP_SUCCESS != result )
          {
            DS_AT_MSG1_ERROR("$QCPDPIMSCFGE: IM CN flag write failed  %d", result);
            return DSAT_ERROR;
          }

        }
        else
        {
          err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
          err_info.arg_num = 3;
          goto send_error;
        }
      }
    }
    else
    {
      err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
      err_info.arg_num = 0;
      goto send_error;
    }
  }
  else if (tok_ptr->op == (NA|EQ|QU) )
  {
    res_buff_ptr->used = (word) snprintf((char *)res_buff_ptr->data_ptr,
                                                res_buff_ptr->size,
                                               "$QCPDPIMSCFGE: (%d - %d),(0 - 1 ),(0 - 1),(0 - 1)\n",
                                                1,
                                                DS_UMTS_MAX_PDP_PROFILE_NUM );
    
  }
  else  if(tok_ptr->op == (NA|QU))
  {
    uint16  profile_counter;
    for(profile_counter=1; profile_counter<= DS_UMTS_MAX_PDP_PROFILE_NUM;
               profile_counter++)
    {
      result = ds_umts_get_pdp_profile_request_pcscf_address_flag_per_subs( 
                                                                profile_counter,
                                                                dsat_get_current_subs_id(),
                                                                &flag_val[0]);
      if(DS_UMTS_PDP_SUCCESS == result )
      {      
        result = ds_umts_get_pdp_profile_request_pcscf_address_using_dhcp_flag_per_subs(
                                                                profile_counter,
                                                                dsat_get_current_subs_id(),
                                                                &flag_val[1]);
        if(DS_UMTS_PDP_SUCCESS == result )
        {
          result = ds_umts_get_pdp_profile_im_cn_flag_per_subs(
                                                               profile_counter,
                                                               dsat_get_current_subs_id(),
                                                               &flag_val[2]);
          if(DS_UMTS_PDP_SUCCESS == result )
          {
            res_buff_ptr->used += 
             (word) snprintf((char*)&res_buff_ptr->data_ptr[res_buff_ptr->used],
                                    res_buff_ptr->size - res_buff_ptr->used,
                                   "$QCPDPIMSCFGE: %d , %d , %d , %d\n",
                                    profile_counter,
                                    flag_val[0],
                                    flag_val[1],
                                    flag_val[2]);
           }
         }
      }
    }
  }
  else
  {
    err_info.errval = DSAT_ERR_INVALID_SYNTAX;
    goto send_error;
  }
  return DSAT_OK;

send_error:
  if( err_info.errval == DSAT_ERR_INVALID_ARGUMENT )
  {
    DS_AT_MSG2_ERROR("Error:%d, arg_num:%d",err_info.errval,err_info.arg_num);
  }
  else
  {
    DS_AT_MSG1_ERROR("Error:%d",err_info.errval);
  }
  return DSAT_ERROR;

} /*dsatvend_exec_qcpdpimscfge_cmd*/

/*===========================================================================

FUNCTION DSATVEND_EXEC_QCAPNE_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes at$qcapne command.
  at$qcapne is used to edit APN values in APN table. 

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_OK : if the command has been successfully executed
    DSAT_ERROR : if there was any problem in executing the command

SIDE EFFECTS
  None

===========================================================================*/
/*ARGSUSED*/
dsat_result_enum_type dsatvend_exec_qcapne_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
#define MAX_APNE_OUTPUT 150
  dsat_num_item_type       profile_id    = 0;
  dsat_string_item_type    apnb_type[3];
  dsat_num_item_type       flag_i_val    = 0;
  dsat_num_item_type       apn_class     = DSAT_APN_CLASS_ONE;
  dsat_string_item_type    pdptype_val[8];
  boolean                  flag_val      = FALSE;              /* Used to Update an APN  flag value      */
  dsat_num_item_type       timer_val     = 0;                  /* Used to Update  inactivity timer value */
  byte                     apn_buff[DS_UMTS_MAX_APN_STRING_LEN +1];
  boolean                  pre_flag_val  = FALSE;              /* Used to restore an APN  flag value     */
  uint8                    pre_apn_class = DSAT_APN_CLASS_ONE; /* Used to restore an APN  Class value     .*/
  ds_umts_pdp_profile_status_etype apn_result = DS_UMTS_PDP_SUCCESS;
  ds_umts_pdp_context_type pdata;

  dsat_error_info_s_type   err_info;

  err_info.errval  = DSAT_ERR_NO_ERROR;
  err_info.arg_num = 0;

  if (tok_ptr->op == (NA|EQ|AR) ) 
  {
    /* Check the number of arguments. */
    if ( tok_ptr->args_found != parse_table->compound )
    {
      err_info.errval = DSAT_ERR_INVALID_NUM_OF_ARGS;
      goto send_error;
    }
/*                                                          PARAMs check                                                                       */	
    /*         Profile ID          */
    err_info.arg_num = 0;
    if(!(VALID_TOKEN(0) && (ATOI_OK == dsatutil_atoi(&profile_id,tok_ptr->arg[0],10))))
    {
      err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
      goto send_error;
    }
    else
    {
      if( (profile_id < 1) || (profile_id > DS_UMTS_MAX_PDP_PROFILE_NUM) )
      {
        err_info.errval = DSAT_ERR_PARAMETER_OUT_OF_RANGE;
        goto send_error;
      }
    }
	
    /*         APN CLASS          */
    err_info.arg_num = 1;
    if(!(VALID_TOKEN(1) && (ATOI_OK == dsatutil_atoi(&apn_class,tok_ptr->arg[1],10))))
    {
      err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
      goto send_error;
    }
    else
    {
      if( apn_class > DSAT_APN_CLASS_MAX )
      {
        err_info.errval = DSAT_ERR_PARAMETER_OUT_OF_RANGE;
        goto send_error;
      }
    }
	
    /*         APN Name          */
    err_info.arg_num = 2;
    if( (VALID_TOKEN(2) ) && ('"' != *tok_ptr->arg[2]) )
    {
      /* APN should be entered in quotes ("") */
      err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
      goto send_error;
    }
    else
    {
      if ( !dsatutil_strip_quotes_out(tok_ptr->arg[2],
                                      apn_buff,sizeof(apn_buff)))
      {
        DS_AT_MSG0_ERROR("Cannot process APN parameter");
        return DSAT_ERROR;
      }
/*
      if (!(('\0' == apn_buff[0]) ||
            (DSAT_OK == etsipkt_validate_apn((byte*)apn_buff))))
      {
        DS_AT_MSG0_ERROR("Failed APN validation");
        return DSAT_ERROR;
      }
*/
    }
    /*         PDP Type          */
    err_info.arg_num = 3;
    if( (VALID_TOKEN(3) ) && ('"' != *tok_ptr->arg[3]) )
    {
      /* PDP type should be entered in quotes ("") */
      err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
      goto send_error;
    }
    else
    {
      if ( !dsatutil_strip_quotes_out(tok_ptr->arg[3],
                                      pdptype_val,sizeof(pdptype_val)))
      {
        err_info.errval = DSAT_ERR_QUOTE_STRIP_FAILURE;
        goto send_error;
      }
    }

    /*         APN Bearer type          */
    err_info.arg_num = 4;
    if( (VALID_TOKEN(4) ) && ('"' != *tok_ptr->arg[4]) )
    {
      /* APN should be entered in quotes ("") */
      err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
      goto send_error;
    }
    else
    {
      if ( !dsatutil_strip_quotes_out(tok_ptr->arg[4],
                                      apnb_type,sizeof(apn_buff)))
      {
        err_info.errval = DSAT_ERR_QUOTE_STRIP_FAILURE;
        goto send_error;
      }
      if( !dsatutil_strcmp_ig_sp_case((const byte *)pdptype_val, (const byte *)"LTE") )
      {
        err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
        goto send_error;
      }
    }
    /*         APN FLAG          */
    if(!(VALID_TOKEN(5) && (ATOI_OK == dsatutil_atoi(&flag_i_val,tok_ptr->arg[5],10))))
    {
      err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
      goto send_error;
    }
    else
    {
      if((flag_i_val != 0) && (flag_i_val != 1 ))
      {
        err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
        goto send_error;
      }
      if(flag_i_val == 1)
      {
        flag_val = TRUE;
      }
      else
      {
        flag_val = FALSE;
      }
    }
	
    /*         APN Timer          */
    err_info.arg_num = 6;
    if(!(VALID_TOKEN(6) && (ATOI_OK == dsatutil_atoi((dsat_num_item_type*)&timer_val,tok_ptr->arg[6],10))))
    {
      err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
      goto send_error;
    }
    else
    {
      if( timer_val > 122820 )
      {
        err_info.errval = DSAT_ERR_PARAMETER_OUT_OF_RANGE;
        goto send_error;
       }
    }
/*                                                          Read from PDP REG                                                               */
    memset((void*)&pdata, 0, sizeof(pdata));
   
    /* Try getting the profile info from EFS */
    if (DS_UMTS_PDP_SUCCESS !=
          ds_umts_get_pdp_profile_context_info_per_subs( SET_CID( profile_id ), dsat_get_current_subs_id(), &pdata) )
    {
      DS_AT_MSG1_HIGH("Could not get profile %d info",profile_id);
    }
	
    pdata.valid_flg = TRUE;
    pdata.pdp_context_number = profile_id;
    (void)strlcpy((char *)pdata.apn,(const char *)apn_buff,DS_UMTS_MAX_APN_STRING_LEN + 1);
    
    if( !dsatutil_strcmp_ig_sp_case((const byte *)pdptype_val, (const byte *)"IPV6") )
    {
      pdata.pdp_type = DS_UMTS_PDP_IPV6;
    }
    else if( !dsatutil_strcmp_ig_sp_case((const byte *)pdptype_val, (const byte *)"IPV4V6") )
    {
      pdata.pdp_type = DS_UMTS_PDP_IPV4V6;
    }
    else
    {
      err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
      err_info.arg_num = 3;
      goto send_error;
    }

    /*Read Current flag value*/
    apn_result = ds_umts_get_pdp_profile_apn_disable_flag_per_subs( 
                                                          (uint16)profile_id,
                                                          dsat_get_current_subs_id(),
                                                          &pre_flag_val);
    /*Read Current Class value*/
    apn_result = ds_umts_get_pdp_profile_apn_class_per_subs((uint16)profile_id,
                                                            dsat_get_current_subs_id(),
                                                            (uint8 *)&pre_apn_class);
    if(DS_UMTS_PDP_SUCCESS != apn_result )
    {
       err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
       err_info.arg_num = 0;
       goto send_error;
    }


/*                                                          WRITE to PDP REG                                                                     */
	
    /*Write APN flag value */
    apn_result = ds_umts_set_pdp_profile_apn_disable_flag_per_subs( (uint16)profile_id,
                                                                     dsat_get_current_subs_id(),
                                                                     flag_val);
    if(DS_UMTS_PDP_SUCCESS != apn_result )
    {
       DS_AT_MSG0_ERROR(" APN Flag write Failed ");
       return DSAT_ERROR;
    }
      /*Write APN Class value */
      apn_result = ds_umts_set_pdp_profile_apn_class_per_subs((uint16)profile_id,
                                                              dsat_get_current_subs_id(),
                                                              (uint8 )apn_class);
    if(DS_UMTS_PDP_SUCCESS != apn_result)
    {
      /*Restore APN flag value */
      apn_result = ds_umts_set_pdp_profile_apn_disable_flag_per_subs((uint16)profile_id,
                                                                      dsat_get_current_subs_id(),
                                                                      pre_flag_val);
      DS_AT_MSG0_ERROR(" APN  Class write Failed ");
      return DSAT_ERROR;
    }
      /*Write APN Timer value */
      apn_result = ds_umts_set_pdp_profile_inactivity_timer_val_per_subs((uint16)profile_id,
                                                                          dsat_get_current_subs_id(),
                                                                          timer_val);
    if(DS_UMTS_PDP_SUCCESS != apn_result )
    {
      DS_AT_MSG0_ERROR("APN Timer write Failed ");
      
      /*Restore APN flag and APN Class value */
      apn_result = ds_umts_set_pdp_profile_apn_disable_flag_per_subs((uint16)profile_id,
                                                                      dsat_get_current_subs_id(),
                                                                      pre_flag_val);
      apn_result = ds_umts_set_pdp_profile_apn_class_per_subs((uint16)profile_id,
                                                              dsat_get_current_subs_id(),
                                                              (uint8 )pre_apn_class);
      
      if(DS_UMTS_PDP_SUCCESS != apn_result )
      {
        DS_AT_MSG0_ERROR("APN  Restore Failed ");
        return DSAT_ERROR;
      }
      return DSAT_ERROR;
    }
    else
    {
        /* Write to EFS */
      if (DS_UMTS_PDP_SUCCESS !=
          ds_umts_set_pdp_profile_context_info_per_subs(SET_CID( profile_id ), dsat_get_current_subs_id(), &pdata))
      {
        DS_AT_MSG1_ERROR("Error QCAPNE PDP profile write: %d",profile_id);
        return DSAT_ERROR;
      }
    }
    
  }
  else if (tok_ptr->op == (NA|EQ|QU) )
  {
  
    res_buff_ptr->used = (word) snprintf((char *)res_buff_ptr->data_ptr,
                                               res_buff_ptr->size,
                                               "$QCAPNE: (%d - %d),(%d - %d ),,(%s and %s),\n%s,(%d - %d ),(%d - %d)\n",
                                               1 ,DS_UMTS_MAX_PDP_PROFILE_NUM,
                                               DSAT_APN_CLASS_ONE,DSAT_APN_CLASS_MAX,
                                               "IPV6","IPV4V6","LTE",0,1,
                                               0,122820);

  }
  else if(tok_ptr->op == (NA|QU))
  {
    uint16  profile_counter;
    uint32  inac_timer;
    
    for(profile_counter=1;profile_counter <= DS_UMTS_MAX_PDP_PROFILE_NUM ;profile_counter++)
    {
      memset((void*)&pdata, 0, sizeof(pdata));
      if (DS_UMTS_PDP_SUCCESS !=
          ds_umts_get_pdp_profile_context_info_per_subs( SET_CID( profile_counter ), dsat_get_current_subs_id(), &pdata) )
      {
        DS_AT_MSG1_ERROR("Error CGDCONT PDP profile read: %d",profile_counter);
        continue;
      }
      else
      {
        apn_result = ds_umts_get_pdp_profile_apn_disable_flag_per_subs( 
                                                              profile_counter,
                                                              dsat_get_current_subs_id(),
                                                              &flag_val);
        if(DS_UMTS_PDP_SUCCESS == apn_result )
        {      
          apn_result = ds_umts_get_pdp_profile_inactivity_timer_val_per_subs(
                                                                       profile_counter,
                                                                       dsat_get_current_subs_id(),
                                                                       (uint32 *)&inac_timer);
          if(DS_UMTS_PDP_SUCCESS == apn_result)
          {
            apn_result = ds_umts_get_pdp_profile_apn_class_per_subs((uint16)profile_counter,
                                                            dsat_get_current_subs_id(),
                                                            &pre_apn_class);
            if( DS_UMTS_PDP_SUCCESS == apn_result )
            {
              byte temp_buff[8];
              memset((void *)temp_buff,0,sizeof(temp_buff));
              switch(pdata.pdp_type)
              {
                case DS_UMTS_PDP_IPV6:
                  strlcpy((char*)temp_buff,"IPV6",sizeof(temp_buff));
                  break;
                case DS_UMTS_PDP_IPV4V6:
                  strlcpy((char*)temp_buff,"IPV4V6",sizeof(temp_buff));
                  break;
                default:
                  continue;
              }
              if(flag_val)
              {
                flag_i_val = 1;
              }
              else
              {
                flag_i_val = 0;
              }
              DS_AT_MSG1_HIGH("flag_i_val is %d",flag_i_val);
              if ( (res_buff_ptr->used + MAX_APNE_OUTPUT) > res_buff_ptr->size )
              {
                dsm_item_type *new_buff_ptr;
                new_buff_ptr = dsatutil_append_dsm_item(
                                                 res_buff_ptr,
                                 DSM_BUFFER_THRESHOLD);
                /* If buffer appended... */
                if ( new_buff_ptr != res_buff_ptr )
                {
                  res_buff_ptr = new_buff_ptr;
                  res_buff_ptr->used = 0;
                  res_buff_ptr->data_ptr[res_buff_ptr->used] = '\0';
                }
              }
              res_buff_ptr->used += 
                (word) snprintf((char*)&res_buff_ptr->data_ptr[res_buff_ptr->used],
                                      res_buff_ptr->size - res_buff_ptr->used,
                                      "$QCAPNE: %d , %d ,\"%s\",\"%s\",\"%s\", %d, %u\n",
                                      profile_counter,pre_apn_class,pdata.apn,temp_buff,"LTE",
                                      flag_i_val,(unsigned int)inac_timer );
            }
          }
        }
      }
    }
    if(res_buff_ptr->used != 0)
    {
      res_buff_ptr->used--;
      res_buff_ptr->data_ptr[res_buff_ptr->used] = '\0';
    }
  }
  else
  {
    err_info.errval = DSAT_ERR_INVALID_SYNTAX;
    goto send_error;
      return DSAT_ERROR;
  }

  return DSAT_OK;
send_error:
  if( err_info.errval == DSAT_ERR_INVALID_NUM_OF_ARGS ||
      err_info.errval == DSAT_ERR_INVALID_SYNTAX )
  {
    DS_AT_MSG1_ERROR("Error:%d",err_info.errval);
  }
  else
  {
    DS_AT_MSG2_ERROR("Error:%d, arg_num:%d",err_info.errval,err_info.arg_num);
  }
  return DSAT_ERROR;
}/*dsatvend_exec_qcapne_cmd*/

#endif /* FEATURE_DSAT_ETSI_DATA */

/*===========================================================================

FUNCTION  VEND_CONVERT_MRU

DESCRIPTION

DEPENDENCIES
  None

RETURN VALUE
  Returns a boolean that describes the result of the command execution.
    FALSE : if there was any problem in execution.
    TRUE  : if it is a success. 

SIDE EFFECTS
  None

===========================================================================*/
boolean vend_convert_mru
(
  cm_mmode_mru_table_entry_u_type *mru_entry, 
  dsat_mru_input_s_type *mru_data
)
{
#ifdef FEATURE_DSAT_LTE
  uint64                lte_mru_band_pref;
#endif/* FEATURE_DSAT_LTE */
  boolean               invalid_data = FALSE;
#if defined(FEATURE_GSM) || defined(FEATURE_WCDMA) || defined(FEATURE_DSAT_LTE)
  uint32                mru_band_pref;
  uint32                size;
#endif  /* defined(FEATURE_GSM) || defined(FEATURE_WCDMA) || defined(FEATURE_DSAT_LTE) */
  
  if ( ( mru_entry == NULL ) || ( mru_data == NULL ) )
  {
    DS_AT_MSG0_ERROR("NULL pointer argument");
    return FALSE;
  }

  DS_AT_MSG1_HIGH("Found mode - %d ",mru_entry->mode);

  switch ( mru_entry->mode )
  {
    case SYS_SYS_MODE_CDMA:
      mru_data->rat = DSAT_CDMA_RAT;
      mru_data->band = (sys_band_class_e_type) mru_entry->cdma.band;
      mru_data->channel= (sys_channel_num_type) mru_entry->cdma.channel;
      break;

    case SYS_SYS_MODE_HDR:
      mru_data->rat = DSAT_HDR_RAT;
      mru_data->band = (sys_band_class_e_type) mru_entry->cdma.band;
      mru_data->channel= (sys_channel_num_type) mru_entry->cdma.channel;
      break;

    case SYS_SYS_MODE_GSM:
#ifdef FEATURE_GSM
      mru_data->rat = DSAT_GSM_RAT;
      mru_band_pref  = (uint32) mru_entry->gsm.band_pref.bit_31_16;
      mru_band_pref  = (uint32)((uint32)mru_band_pref << 16);
      mru_band_pref  = DSAT_BAND_ADD32( mru_band_pref, mru_entry->gsm.band_pref.bit_15_0);

      size = vend_convert_band_pref_to_sys_band32(mru_band_pref,mru_data->band_pref,sizeof(mru_data->band_pref));

      if(size == 0)
      {
        invalid_data = TRUE;
      }
     (void) dsatutil_memscpy((void*)mru_data->plmn.identity, 3,
                             (void*)&mru_entry->gsm.plmn_id.identity[0],3);    
#endif /* FEATURE_GSM */
      break;

    case SYS_SYS_MODE_WCDMA:
#ifdef FEATURE_WCDMA
      mru_data->rat = DSAT_WCDMA_RAT;
      mru_band_pref  = (uint32) mru_entry->wcdma.bc.band_pref.bit_31_16;
      mru_band_pref  = (uint32)((uint32)mru_band_pref << 16);
      mru_band_pref  = DSAT_BAND_ADD32( mru_band_pref, mru_entry->wcdma.bc.band_pref.bit_15_0);
      if( mru_entry->wcdma.is_chan )
      {
        mru_data->band = (sys_band_class_e_type) mru_entry->wcdma.bc.band_chan.band;
        mru_data->channel = (sys_channel_num_type) mru_entry->wcdma.bc.band_chan.chan;
      }
      else
      {
       size = vend_convert_band_pref_to_sys_band32(mru_band_pref,mru_data->band_pref,sizeof(mru_data->band_pref));

        if(size == 0)
        {
          invalid_data = TRUE;
        }
      }
     (void) dsatutil_memscpy((void*)mru_data->plmn.identity, 3,
                        (void*)&mru_entry->wcdma.plmn_id.identity[0],3);     
#endif /* FEATURE_WCDMA */
      break;

    case SYS_SYS_MODE_GW:
#if defined(FEATURE_GSM) || defined(FEATURE_WCDMA)
      mru_band_pref  = (uint32) mru_entry->gw.band_pref.bit_31_16;
      mru_band_pref  = (uint32)((uint32)mru_band_pref << 16);
      mru_band_pref  = DSAT_BAND_ADD32( mru_band_pref, mru_entry->gw.band_pref.bit_15_0);

      size = vend_convert_band_pref_to_sys_band32(mru_band_pref,mru_data->band_pref,sizeof(mru_data->band_pref));
      if(size == 0)
      {
        invalid_data = TRUE;
      }
      mru_data->rat = (mru_entry->gw.acq_mode == SYS_SYS_MODE_GSM) ? DSAT_GSM_RAT : DSAT_WCDMA_RAT;
      (void) dsatutil_memscpy((void*)mru_data->plmn.identity, 3,
                              (void*)&mru_entry->gw.plmn_id.identity[0],3);      
#endif /* FEATURE_GSM || FEATURE_WCDMA */
      break;

    case SYS_SYS_MODE_GWL:
#ifdef FEATURE_DSAT_LTE
      DS_AT_MSG1_HIGH("Found acq mode - %d ",mru_entry->gwl.acq_mode);

      mru_band_pref  = (uint32) mru_entry->gwl.band_pref.bit_31_16;
      mru_band_pref  = (uint32)((uint32)mru_band_pref << 16);
      mru_band_pref  = DSAT_BAND_ADD32( mru_band_pref, mru_entry->gwl.band_pref.bit_15_0);
      
      size = vend_convert_band_pref_to_sys_band32(mru_band_pref,mru_data->band_pref,sizeof(mru_data->band_pref));
      if(size == 0)
      {
        invalid_data = TRUE;
      }
      if(mru_entry->gwl.acq_mode == SYS_SYS_MODE_GSM )
      {
        mru_data->rat = DSAT_GSM_RAT;
      }
      else if( mru_entry->gwl.acq_mode == SYS_SYS_MODE_WCDMA )
      {
        mru_data->rat = DSAT_WCDMA_RAT;
      }
      else if ( mru_entry->gwl.acq_mode == SYS_SYS_MODE_LTE )
      {
        mru_data->rat = DSAT_LTE_RAT;
      }
      else
      {
        invalid_data = TRUE;
      }

      if(mru_data->rat == DSAT_LTE_RAT)
      {
        lte_mru_band_pref = (uint64)mru_entry->gwl.lte_band_pref.bit_15_0;
        lte_mru_band_pref |= (uint64)((uint64)mru_entry->gwl.lte_band_pref.bit_31_16<<16);
        lte_mru_band_pref |= (uint64)((uint64)mru_entry->gwl.lte_band_pref.bit_47_32<<32);
        lte_mru_band_pref |= (uint64)((uint64)mru_entry->gwl.lte_band_pref.bit_63_48<<48);
  
        size = vend_convert_lte_band_pref_to_sys_band( lte_mru_band_pref ,mru_data->band_pref,sizeof(mru_data->band_pref));
        if(size == 0)
        {
          invalid_data = TRUE;
        }
      }
     (void) dsatutil_memscpy((void*)mru_data->plmn.identity, 3,
                        (void*)&mru_entry->gwl.plmn_id.identity[0],3);      
#endif/* FEATURE_DSAT_LTE */
    break;

    case SYS_SYS_MODE_NO_SRV:
    case SYS_SYS_MODE_GPS:
    case SYS_SYS_MODE_MAX:
    case SYS_SYS_MODE_WLAN:
    case SYS_SYS_MODE_LTE:
    default:
     invalid_data = TRUE;
     break;
  }

  if( invalid_data )
  {
    DS_AT_MSG1_HIGH("Invalid Data in MRU ,%d mode ",mru_entry->mode);
    return FALSE;
  }
  return TRUE;
}/* vend_convert_mru */

/*===========================================================================

FUNCTION  VEND_CONVERT_BAND_PREF_TO_SYS_BAND32

DESCRIPTION

DEPENDENCIES
  None

RETURN VALUE

SIDE EFFECTS
  None

===========================================================================*/

uint32 vend_convert_band_pref_to_sys_band32
(
  uint32 dsat_band_pref,
  char *band_ptr,
  uint32 src_size
)
{
  dsat_num_item_type  size = 0;
  uint8 index = 0;
  boolean comma_required = FALSE;
  
  ASSERT(band_ptr != NULL);

  memset((void *)band_ptr,0x0,src_size);
  size = strlcat( band_ptr, "\"",src_size);

  DS_AT_MSG1_MED("vend_convert_band_pref_to_sys_band32 dsat_band_pref = %lx", dsat_band_pref);
  
  for(index = 0 ; index < ARR_SIZE(mru_band_pref_map_gw); index++)
  {
    if( DSAT_BAND_CHECK32( dsat_band_pref, mru_band_pref_map_gw[index].mask) )
    {
      if(comma_required)
      {
        size = strlcat( band_ptr, ",", src_size);
      }
      size = strlcat( band_ptr, mru_band_pref_map_gw[index].str, src_size);
      comma_required = TRUE;  
    }
  }

  size = strlcat( band_ptr, "\"",src_size);

  return size;
}/* vend_convert_band_pref_to_sys_band32 */

/*===========================================================================

FUNCTION  VEND_CONVERT_LTE_BAND_PREF_TO_SYS_BAND

DESCRIPTION

DEPENDENCIES
  None

RETURN VALUE

SIDE EFFECTS
  None

===========================================================================*/
uint32 vend_convert_lte_band_pref_to_sys_band
(
  uint64 dsat_band_pref,
  char *band_ptr,
  uint32 src_size
)
{
  dsat_num_item_type  size = 0;
  uint8 index = 0;
  boolean comma_required = FALSE;
  ASSERT(band_ptr != NULL);

  memset((void *)band_ptr,0x0,src_size);
  size = strlcat( band_ptr, "\"",src_size);
  
  DS_AT_MSG1_HIGH("Converting LTE %lx - dsat_band_pref ",dsat_band_pref);
  
  for( index = 0; index < ARR_SIZE(mru_band_pref_map_lte); index++)
  {
    if( DSAT_BAND_CHECK64( dsat_band_pref, mru_band_pref_map_lte[index].mask) )
    {
       if(comma_required)
       {
         size = strlcat( band_ptr, ",", src_size);
       }
       size = strlcat( band_ptr, mru_band_pref_map_lte[index].str ,src_size);
       comma_required = TRUE;
    }
  }
  
  size = strlcat( band_ptr, "\"",src_size);

  return size;

}/* vend_convert_lte_band_pref_to_sys_band */

/*===========================================================================

FUNCTION  VEND_CONVERT_MRU_BAND_INPUT_TO_GSM_BAND_PREF

DESCRIPTION
  Convert MRU band input value to corresponding GSM band preference value.

DEPENDENCIES
  None

RETURN VALUE

SIDE EFFECTS
  None

===========================================================================*/

LOCAL uint32 vend_convert_mru_band_input_to_gsm_band_pref
(
  dsat_mru_band_input_gsm_e_type band
)
{
  uint32 band_pref = DSAT_MRU_BAND_PREF_NONE;

  switch (band)
  {
    case DSAT_MRU_BAND_INPUT_GSM_450:
      band_pref = DSAT_MRU_BAND_PREF_GSM_450;
      break;

    case DSAT_MRU_BAND_INPUT_GSM_480:
      band_pref = DSAT_MRU_BAND_PREF_GSM_480;
      break;

    case DSAT_MRU_BAND_INPUT_GSM_750:
      band_pref = DSAT_MRU_BAND_PREF_GSM_750;
      break;

    case DSAT_MRU_BAND_INPUT_GSM_850:
      band_pref = DSAT_MRU_BAND_PREF_GSM_850;
      break;

    case DSAT_MRU_BAND_INPUT_GSM_EGSM_900:
      band_pref = DSAT_MRU_BAND_PREF_GSM_EGSM_900;
      break;

    case DSAT_MRU_BAND_INPUT_GSM_PGSM_900:
      band_pref = DSAT_MRU_BAND_PREF_GSM_PGSM_900;
      break;

    case DSAT_MRU_BAND_INPUT_GSM_RGSM_900:
      band_pref = DSAT_MRU_BAND_PREF_GSM_RGSM_900;
      break;

    case DSAT_MRU_BAND_INPUT_GSM_DCS_1800:
      band_pref = DSAT_MRU_BAND_PREF_GSM_DCS_1800;
      break;

    case DSAT_MRU_BAND_INPUT_GSM_PCS_1900:
      band_pref = DSAT_MRU_BAND_PREF_GSM_PCS_1900;
      break;

    default:
      break;
  }

  return band_pref;
}/* vend_convert_mru_band_input_to_gsm_band_pref */

/*===========================================================================

FUNCTION  VEND_CONVERT_MRU_BAND_INPUT_TO_WCDMA_BAND_PREF

DESCRIPTION
  Convert MRU band input value to corresponding WCDMA band preference value.

DEPENDENCIES
  None

RETURN VALUE

SIDE EFFECTS
  None

===========================================================================*/

LOCAL uint32 vend_convert_mru_band_input_to_wcdma_band_pref
(
  dsat_mru_band_input_wcdma_e_type band
)
{
  uint32 band_pref = DSAT_MRU_BAND_PREF_NONE;

  switch (band)
  {
    case DSAT_MRU_BAND_INPUT_WCDMA_I_IMT_2000:
      band_pref = DSAT_MRU_BAND_PREF_WCDMA_I_IMT_2000;
      break;

    case DSAT_MRU_BAND_INPUT_WCDMA_II_PCS_1900:
      band_pref = DSAT_MRU_BAND_PREF_WCDMA_II_PCS_1900;
      break;

    case DSAT_MRU_BAND_INPUT_WCDMA_III_1700:
      band_pref = DSAT_MRU_BAND_PREF_WCDMA_III_1700;
      break;

    case DSAT_MRU_BAND_INPUT_WCDMA_IV_1700:
      band_pref = DSAT_MRU_BAND_PREF_WCDMA_IV_1700;
      break;

    case DSAT_MRU_BAND_INPUT_WCDMA_V_850:
      band_pref = DSAT_MRU_BAND_PREF_WCDMA_V_850;
      break;

    case DSAT_MRU_BAND_INPUT_WCDMA_VI_800:
      band_pref = DSAT_MRU_BAND_PREF_WCDMA_VI_800;
      break;

    case DSAT_MRU_BAND_INPUT_WCDMA_VII_2600:
      band_pref = DSAT_MRU_BAND_PREF_WCDMA_VII_2600;
      break;

    case DSAT_MRU_BAND_INPUT_WCDMA_VIII_900:
      band_pref = DSAT_MRU_BAND_PREF_WCDMA_VIII_900;
      break;

    case DSAT_MRU_BAND_INPUT_WCDMA_IX_1700:
      band_pref = DSAT_MRU_BAND_PREF_WCDMA_IX_1700;
      break;

    case DSAT_MRU_BAND_INPUT_WCDMA_XI_1500:
      band_pref = DSAT_MRU_BAND_PREF_WCDMA_XI_1500;
      break;

    default:
      break;
  }

  return band_pref;
}/* vend_convert_mru_band_input_to_wcdma_band_pref */

/*===========================================================================

FUNCTION  VEND_CONVERT_MRU_BAND_INPUT_TO_LTE_BAND_PREF

DESCRIPTION
  Convert MRU band input value to corresponding LTE band preference value.

DEPENDENCIES
  None

RETURN VALUE

SIDE EFFECTS
  None

===========================================================================*/

LOCAL uint64 vend_convert_mru_band_input_to_lte_band_pref
(
  dsat_mru_band_input_lte_e_type band
)
{
  uint64 band_pref = DSAT_MRU_BAND_PREF_LTE_EMPTY;

  switch (band)
  {
    case DSAT_MRU_BAND_INPUT_LTE_EUTRAN_BAND1:
      band_pref = DSAT_MRU_BAND_PREF_LTE_EUTRAN_1;
      break;

    case DSAT_MRU_BAND_INPUT_LTE_EUTRAN_BAND2:
      band_pref = DSAT_MRU_BAND_PREF_LTE_EUTRAN_2;
      break;

    case DSAT_MRU_BAND_INPUT_LTE_EUTRAN_BAND3:
      band_pref = DSAT_MRU_BAND_PREF_LTE_EUTRAN_3;
      break;

    case DSAT_MRU_BAND_INPUT_LTE_EUTRAN_BAND4:
      band_pref = DSAT_MRU_BAND_PREF_LTE_EUTRAN_4;
      break;

    case DSAT_MRU_BAND_INPUT_LTE_EUTRAN_BAND5:
      band_pref = DSAT_MRU_BAND_PREF_LTE_EUTRAN_5;
      break;

    case DSAT_MRU_BAND_INPUT_LTE_EUTRAN_BAND6:
      band_pref = DSAT_MRU_BAND_PREF_LTE_EUTRAN_6;
      break;

    case DSAT_MRU_BAND_INPUT_LTE_EUTRAN_BAND7:
      band_pref = DSAT_MRU_BAND_PREF_LTE_EUTRAN_7;
      break;

    case DSAT_MRU_BAND_INPUT_LTE_EUTRAN_BAND8:
      band_pref = DSAT_MRU_BAND_PREF_LTE_EUTRAN_8;
      break;

    case DSAT_MRU_BAND_INPUT_LTE_EUTRAN_BAND9:
      band_pref = DSAT_MRU_BAND_PREF_LTE_EUTRAN_9;
      break;

    case DSAT_MRU_BAND_INPUT_LTE_EUTRAN_BAND10:
      band_pref = DSAT_MRU_BAND_PREF_LTE_EUTRAN_10;
      break;

    case DSAT_MRU_BAND_INPUT_LTE_EUTRAN_BAND11:
      band_pref = DSAT_MRU_BAND_PREF_LTE_EUTRAN_11;
      break;

    case DSAT_MRU_BAND_INPUT_LTE_EUTRAN_BAND12:
      band_pref = DSAT_MRU_BAND_PREF_LTE_EUTRAN_12;
      break;

    case DSAT_MRU_BAND_INPUT_LTE_EUTRAN_BAND13:
      band_pref = DSAT_MRU_BAND_PREF_LTE_EUTRAN_13;
      break;

    case DSAT_MRU_BAND_INPUT_LTE_EUTRAN_BAND14:
      band_pref = DSAT_MRU_BAND_PREF_LTE_EUTRAN_14;
      break;

    case DSAT_MRU_BAND_INPUT_LTE_EUTRAN_BAND17:
      band_pref = DSAT_MRU_BAND_PREF_LTE_EUTRAN_17;
      break;

    case DSAT_MRU_BAND_INPUT_LTE_EUTRAN_BAND18:
      band_pref = DSAT_MRU_BAND_PREF_LTE_EUTRAN_18;
      break;

    case DSAT_MRU_BAND_INPUT_LTE_EUTRAN_BAND19:
      band_pref = DSAT_MRU_BAND_PREF_LTE_EUTRAN_19;
      break;

    case DSAT_MRU_BAND_INPUT_LTE_EUTRAN_BAND20:
      band_pref = DSAT_MRU_BAND_PREF_LTE_EUTRAN_20;
      break;

    case DSAT_MRU_BAND_INPUT_LTE_EUTRAN_BAND21:
      band_pref = DSAT_MRU_BAND_PREF_LTE_EUTRAN_21;
      break;

    case DSAT_MRU_BAND_INPUT_LTE_EUTRAN_BAND33:
      band_pref = DSAT_MRU_BAND_PREF_LTE_EUTRAN_33;
      break;

    case DSAT_MRU_BAND_INPUT_LTE_EUTRAN_BAND34:
      band_pref = DSAT_MRU_BAND_PREF_LTE_EUTRAN_34;
      break;

    case DSAT_MRU_BAND_INPUT_LTE_EUTRAN_BAND35:
      band_pref = DSAT_MRU_BAND_PREF_LTE_EUTRAN_35;
      break;

    case DSAT_MRU_BAND_INPUT_LTE_EUTRAN_BAND36:
      band_pref = DSAT_MRU_BAND_PREF_LTE_EUTRAN_36;
      break;

    case DSAT_MRU_BAND_INPUT_LTE_EUTRAN_BAND37:
      band_pref = DSAT_MRU_BAND_PREF_LTE_EUTRAN_37;
      break;

    case DSAT_MRU_BAND_INPUT_LTE_EUTRAN_BAND38:
      band_pref = DSAT_MRU_BAND_PREF_LTE_EUTRAN_38;
      break;

    case DSAT_MRU_BAND_INPUT_LTE_EUTRAN_BAND39:
      band_pref = DSAT_MRU_BAND_PREF_LTE_EUTRAN_39;
      break;

    case DSAT_MRU_BAND_INPUT_LTE_EUTRAN_BAND40:
      band_pref = DSAT_MRU_BAND_PREF_LTE_EUTRAN_40;
      break;

    default:
      break;
  }

  return band_pref;
}/* vend_convert_mru_band_input_to_lte_band_pref */

/*===========================================================================

FUNCTION  VEND_VALIDATE_MRU_INPUTS

DESCRIPTION
  Validate input values in MRU write command.

DEPENDENCIES
  None

RETURN VALUE

SIDE EFFECTS
  None

===========================================================================*/

LOCAL boolean vend_validate_mru_inputs
(
  dsat_num_item_type index,
  dsat_num_item_type rat,
  dsat_num_item_type band
)
{
  boolean is_valid = FALSE;

  if ( index >= CM_MRU_TABLE_SIZE )
  {
    return FALSE;
  }

  switch ( rat )
  {
    case DSAT_CDMA_RAT:
    case DSAT_HDR_RAT:
      if ( ( band < DSAT_MRU_BAND_INPUT_3GPP2_MAX ) &&
           ( band != DSAT_MRU_BAND_INPUT_3GPP2_BC2 ) )
      {
        is_valid = TRUE;
      }
      break;

    case DSAT_GSM_RAT:
      if ( band < DSAT_MRU_BAND_INPUT_GSM_MAX )
      {
        is_valid = TRUE;
      }
      break;

    case DSAT_WCDMA_RAT:
      if ( band < DSAT_MRU_BAND_INPUT_WCDMA_MAX )
      {
        is_valid = TRUE;
      }
      break;

    case DSAT_LTE_RAT:
      if ( band < DSAT_MRU_BAND_INPUT_LTE_MAX )
      {
        is_valid = TRUE;
      }
      break;

    default:
      break;
  }

  return is_valid;
}/* vend_validate_mru_inputs */

/*===========================================================================

FUNCTION  VEND_MRU_CHANGE_SYS_OPRT_MODE

DESCRIPTION
  Change system operating mode in MRU commands.

DEPENDENCIES
  None

RETURN VALUE

SIDE EFFECTS
  None

===========================================================================*/

LOCAL boolean vend_mru_change_sys_oprt_mode
(
  sys_oprt_mode_e_type new_mode
)
{
  dsat_result_enum_type result = DSAT_ASYNC_CMD;

  if ( FALSE == cm_ph_cmd_oprt_mode ( dsatcmif_ph_cmd_cb_func,
                                      NULL,
                                      dsatcm_client_id,
                                      new_mode ) )
  {
    DS_AT_MSG1_ERROR("Failed to change sys oprt mode to %d", new_mode);

    if ( CHECK_NOT_PENDING( DSAT_VENDOR_QCMRUE_IDX, 0, DSAT_PENDING_MRU_NONE ) )
    {
      SET_PENDING( DSAT_VENDOR_QCMRUE_IDX, 0, DSAT_PENDING_MRU_NONE );
    }

    if ( CHECK_NOT_PENDING( DSAT_VENDOR_QCMRUC_IDX, 0, DSAT_PENDING_MRU_NONE ) )
    {
      SET_PENDING( DSAT_VENDOR_QCMRUC_IDX, 0, DSAT_PENDING_MRU_NONE );
    }

    result = DSAT_ERROR;
  }

  return result;
} /* vend_mru_change_sys_oprt_mode */

/*===========================================================================

FUNCTION DSATVEND_EXEC_QCMRUE_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes at$QCMRUE command.
  at$QCMRUE is used to edit MRU database. 

  As per the spec 3GPP2 C.S0057 and SD implementation only the
  following 3GPP2 bands and channels are supported: -
  Band 0, Channel 1-799, 991-1023
  Band 1, Channel 0-1199
  Band 3, Channel 1-799, 801-1039, 1041-1199, 1201-1600
  Band 4, Channel 0-599
  Band 5, Channel 1-400,  472-871, 1039-1473, 1536-1715, 1792-2016
  Band 6, Channel 0-1199
  Band 10, Channel 0-919
  Band 11, Channel 1-400, 472-871, 1536-1715
  Band 12, Channel 0-239
  Band 14, Channel 0-1299
  Band 15, Channel 0-899
  Band 16, Channel 140-459, 470-2719
  Band 17, Channel 140-1459 (Only in HDR)
  Band 18, Channel 0-240
  Band 19, Channel 0-360

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_OK : if the command has been successfully executed
    DSAT_ERROR : if there was any problem in executing the command

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatvend_exec_qcmrue_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
#define DSAT_MIN_MRU_PLMN_LEN 5
#define DSAT_MAX_MRU_PLMN_LEN 6
  dsat_result_enum_type result = DSAT_OK;
  byte oper[DSAT_MAX_MRU_PLMN_LEN+1];
  byte buffer[8];
  dsat_num_item_type mcc = 0;
  dsat_num_item_type mnc = 0;
  boolean pcs_flag  = FALSE;
  dsat_error_info_s_type   err_info;

  err_info.errval = DSAT_ERR_NO_ERROR;
  err_info.arg_num = 0;

  if(tok_ptr->op == (NA|QU)||(tok_ptr->op == (NA|EQ|AR)))
  {
    if ((dsat_num_item_type)dsatutil_get_val(
             DSAT_EXT_CFUN_IDX,0,0,MIX_NUM_TYPE) != DSAT_ME_FUNC_FULL)
    {
      DS_AT_MSG0_HIGH(" MRU can be operated only in Online mode ");
      dsatme_set_cme_error(DSAT_CME_OP_NOT_SUPPORTED, res_buff_ptr);
      return  DSAT_CMD_ERR_RSP;
    }
    else
    {
      if(tok_ptr->op == (NA|EQ|AR))
      {
        if( tok_ptr->args_found != 4 )
        {
          err_info.errval = DSAT_ERR_INVALID_NUM_OF_ARGS;
          goto send_error;
        }
        memset((void*)&dsat_mru_input,0x0,sizeof(dsat_mru_input));

        if ( ATOI_OK != dsatutil_atoi( &dsat_mru_input.index, tok_ptr->arg[0], 10 ) )
        {
          err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
          err_info.errval = 0;
          goto send_error;
        }

        if ( ATOI_OK != dsatutil_atoi( &dsat_mru_input.rat, tok_ptr->arg[1], 10 ) )
        {
          err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
          err_info.errval = 1;
          goto send_error;
        }

        if ( ATOI_OK != dsatutil_atoi( &dsat_mru_input.band, tok_ptr->arg[2], 10 ) )
        {
          err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
          err_info.errval = 2;
          goto send_error;
        }
        /* Channel input is valid only for CDMA and HDR */
        err_info.errval = 3;
        if((dsat_mru_input.rat == DSAT_CDMA_RAT) || (dsat_mru_input.rat == DSAT_HDR_RAT))
        {
          if ( ATOI_OK != dsatutil_atoi( &dsat_mru_input.channel, tok_ptr->arg[3], 10 ) )
          {
            err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
            goto send_error;;
          }
        }
        else /* For GSM,WCDMA,LTE treat the arg[3] as PLMN ID */
        {
            memset ((void*)oper, 0x0, sizeof(oper));

          /* Strip quotes */
           if ('"' == *tok_ptr->arg[3])
           {
             if (!dsatutil_strip_quotes_out(
                    tok_ptr->arg[3], &oper[0],
                    DSAT_MAX_MRU_PLMN_LEN+1))
             {
               err_info.errval = DSAT_ERR_QUOTE_STRIP_FAILURE;
               goto send_error;
             }
           }
           else
           {
             err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
             goto send_error;
           }

           if(strlen((char *)oper) == 0)
           {
             DS_AT_MSG0_HIGH("NULL PLMN ID found setting the acq_mode to Auto ");
             dsat_mru_input.is_plmn_valid = FALSE;
           }
           else if (strlen((char *)oper) < DSAT_MIN_MRU_PLMN_LEN)
           {
             err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
             goto send_error;
           }
           else
           {
             memset ((void*)buffer, 0x0, sizeof(buffer));
            (void)strlcat ((char*)buffer,
                               (char*)oper,
                               PLMN_STR_MCC_LEN+1);
             if ( ATOI_OK !=
                  dsatutil_atoi (&mcc, buffer, 10) )
             {
               DS_AT_MSG0_ERROR("Cannot convert MCC value");
               return DSAT_ERROR;
             }
            
             /* MNC is next 2 or 3 characters */
             memset ((void*)buffer, 0x0, sizeof(buffer));
             (void)strlcat ((char*)buffer,
                                (char*)(oper+PLMN_STR_MCC_LEN),
                                PLMN_STR_MNC_LEN+1);
             if ( ATOI_OK !=
                  dsatutil_atoi (&mnc, buffer, 10) )
             {
               DS_AT_MSG0_ERROR("Cannot convert MNC value");
               return DSAT_ERROR;
             }
            
             /* Convert MCC/MNC to PLMN, set 3rd MNC digit if MNC string
                was of 3 characters */
  
             if((PLMN_STR_MNC_LEN == strlen((char*)buffer)))
             {
               pcs_flag = TRUE;
             }
  
             if ( FALSE == 
                  sys_plmn_set_mcc_mnc( (mnc > 99 || pcs_flag)? TRUE : FALSE,
                                        mcc, mnc, &dsat_mru_input.plmn ) ) 
             { 
               DS_AT_MSG2_ERROR("Invalid mcc %d mnc %d found. Assuming Manual selection",mcc,mnc);
               dsat_mru_input.is_plmn_valid = FALSE;
             }
             else
             {
               dsat_mru_input.is_plmn_valid = TRUE;
             }
          }
        }

         /* Validate input */
         if ( FALSE == vend_validate_mru_inputs( dsat_mru_input.index,
                                                 dsat_mru_input.rat,
                                                 dsat_mru_input.band ) )
         {
           err_info.errval = DSAT_ERR_INVALID_TOKENS;
           goto send_error;
         }

         SET_PENDING( DSAT_VENDOR_QCMRUE_IDX, 0, DSAT_PENDING_MRU_WRITE );
         result = vend_mru_change_sys_oprt_mode( SYS_OPRT_MODE_LPM );
      }/* NA|EQ|AR */
      else
      {
        SET_PENDING( DSAT_VENDOR_QCMRUE_IDX, 0, DSAT_PENDING_MRU_READ );
        result = dsatvend_mru_action(DSAT_PENDING_MRU_READ);
      }
    }
  }
  else if ( tok_ptr->op == (NA|EQ|QU))
  {
    char local_buffer[30];
    word data_size = 0;
    uint32 band_index = 0;

    res_buff_ptr->used = (word)snprintf((char *)res_buff_ptr->data_ptr,
                                 res_buff_ptr->size,
#ifdef FEATURE_DSAT_LTE
                   "$QCMRUE: (0-11),\n(0-CDMA,1-HDR,2-GSM,3-WCDMA,4-LTE),\n");
#else
                   "$QCMRUE: (0-11),\n(0-CDMA,1-HDR,2-GSM,3-WCDMA),\n");
#endif /* FEATURE_DSAT_LTE */
    while ( dsat_mru_tststr[band_index][0] != NULL )
    {
      data_size = (word)snprintf(local_buffer,
                                       sizeof(local_buffer),
                                       "%s,\n",dsat_mru_tststr[band_index]);

      dsat_dsm_pushdown_tail(&res_buff_ptr,
                             local_buffer,
                             data_size,
                             DSM_ITEM_POOL(res_buff_ptr),
                             FALSE);

      ++band_index;
    }
    result = DSAT_OK;
  }
  return result;
   
send_error:
  if( err_info.errval == DSAT_ERR_INVALID_NUM_OF_ARGS ||
      err_info.errval == DSAT_ERR_INVALID_TOKENS )
  {
    DS_AT_MSG1_ERROR("Error:%d",err_info.errval);
  }
  else
  {
    DS_AT_MSG2_ERROR("Error:%d, arg_num:%d",err_info.errval,err_info.arg_num);
  }
  return DSAT_ERROR;

}/* dsatvend_exec_qcmrue_cmd */


/*===========================================================================

FUNCTION DSATVEND_EXEC_QCMRUC_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes at$QCMRUC command.
  at$QCMRUC is used to clear MRU database. 

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_OK : if the command has been successfully executed
    DSAT_ERROR : if there was any problem in executing the command

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatvend_exec_qcmruc_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type result = DSAT_OK;

  if(tok_ptr->op == (NA))
  {
    if ((dsat_num_item_type)dsatutil_get_val(
             DSAT_EXT_CFUN_IDX,0,0,MIX_NUM_TYPE) != DSAT_ME_FUNC_FULL)
    {
      DS_AT_MSG0_HIGH(" MRU can be operated only in Online mode ");
      dsatme_set_cme_error(DSAT_CME_OP_NOT_SUPPORTED, res_buff_ptr);
      return  DSAT_CMD_ERR_RSP;
    }
    else
    {
      SET_PENDING( DSAT_VENDOR_QCMRUC_IDX, 0, DSAT_PENDING_MRU_CLEAR );
      result = vend_mru_change_sys_oprt_mode( SYS_OPRT_MODE_LPM );
    }
  }
  else
  {
    DS_AT_MSG0_HIGH(" Unsupported command format ");
    result = DSAT_ERROR;
  }
  return result;
}/* dsatvend_exec_qcmrue_cmd */

/*===========================================================================

FUNCTION  DSATVEND_MRU_READ_CM_CMD

DESCRIPTION
  This function commands CM to read a MRU table index.
  The read data is returned in callback function.

DEPENDENCIES
  None

RETURN VALUE
  Returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR :    if there was any problem in executing the command
    DSAT_ASYNC_CMD :  if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
/* ARGSUSED */
dsat_result_enum_type dsatvend_mru_read_cm_cmd
(
  dsat_num_item_type index
)
{
  dsat_result_enum_type result = DSAT_ASYNC_CMD;
  cm_mmode_mru_table_entry_u_type mru_table_entry;

  memset(&mru_table_entry, 0, sizeof(mru_table_entry));

  DS_AT_MSG1_HIGH("Commanding CM to read MRU table index %d", index);

  if (TRUE != cm_ph_cmd_read_mru( dsatcmif_ph_cmd_cb_func,
                                  &mru_table_entry,
                                  dsatcm_client_id,
                                  index ))
  {
    DS_AT_MSG0_ERROR("CM command to read MRU table index is failed");
    SET_PENDING( DSAT_VENDOR_QCMRUE_IDX, 0, DSAT_PENDING_MRU_NONE );
    result = DSAT_ERROR;
  }

  return result;
} /* dsatvend_mru_read_cm_cmd */

/*===========================================================================

FUNCTION DSATVEND_MRU_ACTION

DESCRIPTION
  This function carries the action on the MRU tables

DEPENDENCIES
  Phone should be in Offline.

RETURN VALUE
  Returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ASYNC_EVENT: If there is a further Async calls made
    DSAT_OK: If Everything is fine.
    DSAT_ERROR : if there was any problem in executing the command

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatvend_mru_action
(
  dsat_cmd_pending_enum_type action
)
{
  dsat_result_enum_type result = DSAT_ASYNC_CMD;
  cm_mmode_mru_table_entry_u_type mru_entry;
  uint32 temp;
#ifdef FEATURE_DSAT_LTE
  uint64 temp2;
#endif /* FEATURE_DSAT_LTE */

  DS_AT_MSG1_HIGH("In MRU %d action ",action);

  switch(action)
  {
    case DSAT_PENDING_MRU_CLEAR:
    {
      if (TRUE != cm_ph_cmd_mru_update( dsatcmif_ph_cmd_cb_func,
                                        NULL,
                                        dsatcm_client_id,
                                        &mru_entry,
                                        0,
                                        0,
                                        CM_MRU_CLEAN ))
      {
        DS_AT_MSG0_ERROR("CM command to update MRU table is failed");

        SET_PENDING( DSAT_VENDOR_QCMRUC_IDX, 0, DSAT_PENDING_MRU_RESULT );

        /* Store result to report it after operating mode changed back to ONLINE */
        dsat_mru_result = DSAT_ERROR;

        result = vend_mru_change_sys_oprt_mode( SYS_OPRT_MODE_ONLINE );
      }
    }
    break;

    case DSAT_PENDING_MRU_READ:
    {
      dsm_item_type *res_buff_ptr = NULL;

      dsat_mru_read_index = 0;

      result = dsatvend_mru_read_cm_cmd(dsat_mru_read_index);
      if (result == DSAT_ASYNC_CMD)
      {
        res_buff_ptr = dsat_dsm_new_buffer(DSM_DS_SMALL_ITEM_POOL, FALSE);
        res_buff_ptr->used += (word)snprintf ( (char *)res_buff_ptr->data_ptr,
                                               res_buff_ptr->size,
                                               "$QCMRUE: ");

        dsatcmdp_handle_async_cmd_rsp(res_buff_ptr, DSAT_COMPLETE_RSP);
      }
    }
    break;

    case DSAT_PENDING_MRU_WRITE:
    {
      memset((void *)&mru_entry, 0x0, sizeof(mru_entry));

      switch(dsat_mru_input.rat)
      {
        case DSAT_CDMA_RAT:
        {
          mru_entry.mode = SYS_SYS_MODE_CDMA;
          mru_entry.cdma.mode = SYS_SYS_MODE_CDMA;
          mru_entry.cdma.band = (sys_band_class_e_type)dsat_mru_input.band;
          mru_entry.cdma.channel = (sys_channel_num_type)dsat_mru_input.channel;
        }
        break;
        case DSAT_HDR_RAT:
        {
          mru_entry.mode = SYS_SYS_MODE_HDR;
          mru_entry.cdma.mode = SYS_SYS_MODE_HDR;
          mru_entry.cdma.band = (sys_band_class_e_type)dsat_mru_input.band;
          mru_entry.cdma.channel = (sys_channel_num_type)dsat_mru_input.channel;
        }
        break;
        case DSAT_GSM_RAT:
        {
          mru_entry.mode = SYS_SYS_MODE_GSM;
          mru_entry.gsm.mode = SYS_SYS_MODE_GSM;
          temp = vend_convert_mru_band_input_to_gsm_band_pref(dsat_mru_input.band);
          mru_entry.gsm.band_pref.bit_15_0 = (word) ((word) temp & 0xffff );
          mru_entry.gsm.band_pref.bit_31_16 =(word) ( (uint32)temp >> 16 );
          if(dsat_mru_input.is_plmn_valid )
          {
            mru_entry.gsm.net_select_mode = 4; /* SD_NET_SELECT_MODE_MANUAL_SYS */
          (void) dsatutil_memscpy((void*)&mru_entry.gsm.plmn_id.identity[0], 3,
                             (void*)dsat_mru_input.plmn.identity,3);          
          }
          else
          {
            mru_entry.gsm.net_select_mode = 1; /* SD_NET_SELECT_MODE_AUTO */
            memset((byte *)&mru_entry.gsm.plmn_id.identity[0],0xFF,3);
          }
        }
        break;
        case DSAT_WCDMA_RAT:
        {
          mru_entry.mode = SYS_SYS_MODE_WCDMA;
          mru_entry.wcdma.mode = SYS_SYS_MODE_WCDMA;
          temp = vend_convert_mru_band_input_to_wcdma_band_pref(dsat_mru_input.band);
          mru_entry.wcdma.bc.band_pref.bit_15_0 = (word) ((word) temp & 0xffff );
          mru_entry.wcdma.bc.band_pref.bit_31_16 =(word) ( (uint32)temp >> 16 );
          if(dsat_mru_input.is_plmn_valid )
          {
            mru_entry.wcdma.net_select_mode = 4; /* SD_NET_SELECT_MODE_MANUAL_SYS */
          (void) dsatutil_memscpy((void*)&mru_entry.wcdma.plmn_id.identity[0], 3,
                             (void*)dsat_mru_input.plmn.identity,3);         
          }
          else
          {
            mru_entry.wcdma.net_select_mode = 1; /* SD_NET_SELECT_MODE_AUTO */
            memset((byte *)&mru_entry.wcdma.plmn_id.identity[0],0xFF,3);
          }
        }
        break;
#ifdef FEATURE_DSAT_LTE
        case DSAT_LTE_RAT:
        {
          mru_entry.mode = SYS_SYS_MODE_GWL;
          mru_entry.gwl.mode = SYS_SYS_MODE_GWL;
          mru_entry.gwl.acq_mode = SYS_SYS_MODE_LTE;
          temp2 = vend_convert_mru_band_input_to_lte_band_pref(dsat_mru_input.band);
          mru_entry.gwl.lte_band_pref.bit_15_0 = (word) ((word) temp2 & 0xffff );
          mru_entry.gwl.lte_band_pref.bit_31_16 =(word) ((uint64)temp2 >> 16 );
          mru_entry.gwl.lte_band_pref.bit_47_32 = (word) ((uint64) temp2 >> 32 );
          mru_entry.gwl.lte_band_pref.bit_63_48 =(word) ((uint64)temp2 >> 48 );
          mru_entry.gwl.num_rats = 1; 
          mru_entry.gwl.rat[0] = SD_MODE_LTE;
          if(dsat_mru_input.is_plmn_valid )
          {
            mru_entry.gwl.net_select_mode = 4; /* SD_NET_SELECT_MODE_MANUAL_SYS */
           (void) dsatutil_memscpy((void*)&mru_entry.gwl.plmn_id.identity[0], 3,
                             (void*)dsat_mru_input.plmn.identity,3);           
          }
          else
          {
            mru_entry.gwl.net_select_mode = 1; /* SD_NET_SELECT_MODE_AUTO */
            memset((byte *)&mru_entry.gwl.plmn_id.identity[0],0xFF,3);
          }
        }
        break;
#endif /* FEATURE_DSAT_LTE */
        default:
        break;
      }

      if (TRUE != cm_ph_cmd_mru_update( dsatcmif_ph_cmd_cb_func,
                                        NULL,
                                        dsatcm_client_id,
                                        &mru_entry,
                                        dsat_mru_input.index,
                                        mru_entry.mode,
                                        CM_MRU_UPDATE ))
      {
        DS_AT_MSG0_ERROR("CM command to update MRU table is failed");

        SET_PENDING( DSAT_VENDOR_QCMRUE_IDX, 0, DSAT_PENDING_MRU_RESULT );

        /* Store result to report it after operating mode changed back to ONLINE */
        dsat_mru_result = DSAT_ERROR;

        result = vend_mru_change_sys_oprt_mode( SYS_OPRT_MODE_ONLINE );
      }
    }/* case - DSAT_MRU_WRITE */
    break;

    case DSAT_PENDING_MRU_RESULT:
    {
      result = dsat_mru_result;
    }
    break;

    default:
      result = DSAT_OK;
  }

  return result;
}/* dsatvend_mru_action */

#ifdef FEATURE_DSAT_LTE
/*===========================================================================

FUNCTION DSATVEND_EXEC_QCACQDB_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes AT$QCACQDBC command.
  at$QCACQDBC is used to clear ACQ database. 

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_OK : if the command has been successfully executed
    DSAT_ERROR : if there was any problem in executing the command

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatvend_exec_qcacqdbc_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type result = DSAT_OK;

  if(tok_ptr->op == (NA))
  {
    if(0 != lte_rrc_delete_acq_db())
    {
      result = DSAT_ERROR;
    }
  }
  else
  {
    result = DSAT_ERROR;
  }
  return result;
}/* dsatvend_exec_qcacqdb_cmd */
#endif /* FEATURE_DSAT_LTE */
/*===========================================================================
FUNCTION DSATVEND_PROCESS_MRU_UPDATE_RESULT

DESCRIPTION
  This function processes the MRU update result.

DEPENDENCIES
  None

RETURN VALUE
  Returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ASYNC_EVENT: If there is a further Async calls made
    DSAT_OK: If Everything is fine.
    DSAT_ERROR : if there was any problem in executing the command

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatvend_process_mru_update_result
(
  cm_ph_cmd_err_e_type err
)
{
  dsat_result_enum_type result = DSAT_ASYNC_CMD;

  DS_AT_MSG0_MED("Process MRU update result");

  if ( CHECK_PENDING( DSAT_VENDOR_QCMRUE_IDX, 0, DSAT_PENDING_MRU_WRITE ) )
  {
    SET_PENDING( DSAT_VENDOR_QCMRUE_IDX, 0, DSAT_PENDING_MRU_RESULT );
  }
  else if ( CHECK_PENDING( DSAT_VENDOR_QCMRUC_IDX, 0, DSAT_PENDING_MRU_CLEAR ) )
  {
    SET_PENDING( DSAT_VENDOR_QCMRUC_IDX, 0, DSAT_PENDING_MRU_RESULT );
  }
  else
  {
    DS_AT_MSG0_ERROR("Unexpected MRU update result");
    return DSAT_ERROR;
  }

  /* Store result to report it after operating mode changed back to ONLINE */
  if (CM_PH_CMD_ERR_NOERR == err)
  {
    dsat_mru_result = DSAT_OK;
  }
  else
  {
    dsat_mru_result = DSAT_ERROR;
  }

  result = vend_mru_change_sys_oprt_mode( SYS_OPRT_MODE_ONLINE );

  return result;
}/* dsatvend_process_mru_update_result */

/*===========================================================================

FUNCTION DSATVEND_PROCESS_MRU_READ_RESULT

DESCRIPTION
  This function processes the MRU read result.

DEPENDENCIES
  None

RETURN VALUE
  Returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ASYNC_EVENT: If there is a further Async calls made
    DSAT_OK: If Everything is fine.
    DSAT_ERROR : if there was any problem in executing the command

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatvend_process_mru_read_result
(
  cm_ph_cmd_err_e_type             err,
  cm_mmode_mru_table_entry_u_type *mru_entry
)
{
  dsat_result_enum_type result = DSAT_ASYNC_CMD;
  dsm_item_type *res_buff_ptr = NULL;
  dsat_mru_input_s_type mru_data;

  DS_AT_MSG0_MED("Process MRU read result");

  if ( CHECK_NOT_PENDING( DSAT_VENDOR_QCMRUE_IDX, 0, DSAT_PENDING_MRU_READ ) )
  {
    DS_AT_MSG0_ERROR("Unexpected MRU read result");
    return DSAT_ASYNC_CMD;
  }

  if ( CM_PH_CMD_ERR_NOERR != err )
  {
    DS_AT_MSG1_ERROR("Process MRU read result: err = %d", err);
    SET_PENDING( DSAT_VENDOR_QCMRUE_IDX, 0, DSAT_PENDING_MRU_NONE );
    return DSAT_ERROR;
  }

  if( TRUE == vend_convert_mru(mru_entry, &mru_data) )
  {
    res_buff_ptr = dsat_dsm_new_buffer(DSM_DS_SMALL_ITEM_POOL, FALSE);
    if((mru_data.rat == DSAT_CDMA_RAT)||(mru_data.rat == DSAT_HDR_RAT))
    {
      res_buff_ptr->used += (word)snprintf((char *)res_buff_ptr->data_ptr,
                                           res_buff_ptr->size,
                                           "%d,%d,%d,%d",
                                           dsat_mru_read_index,
                                           mru_data.rat,
                                           mru_data.band,
                                           mru_data.channel);
    }
    else
    {  
      sys_mcc_type  mcc;
      sys_mnc_type  mnc;
      boolean undef = TRUE;
      boolean pcs_flag = FALSE;
      sys_plmn_get_mcc_mnc( mru_data.plmn, &undef, &pcs_flag, &mcc, &mnc );
      if (TRUE == undef)
      {
        mcc = mnc = 0;
        pcs_flag = FALSE;
      }
      if(pcs_flag)
      {
        res_buff_ptr->used += (word)snprintf((char *)res_buff_ptr->data_ptr,
                                             res_buff_ptr->size,
                                             "%d,%d,%s,\"%03lu%03lu\"",
                                             dsat_mru_read_index,
                                             mru_data.rat,
                                             mru_data.band_pref,
                                             mcc,mnc);
      }
      else
      {
        res_buff_ptr->used += (word)snprintf((char *)res_buff_ptr->data_ptr,
                                             res_buff_ptr->size,
                                             "%d,%d,%s,\"%03lu%02lu\"",
                                             dsat_mru_read_index,
                                             mru_data.rat,
                                             mru_data.band_pref,
                                             mcc,mnc);
      }
    }

    dsatcmdp_handle_async_cmd_rsp(res_buff_ptr, DSAT_END_OF_RSP);
  }

  dsat_mru_read_index++;

  if (dsat_mru_read_index >= CM_MRU_TABLE_SIZE)
  {
    SET_PENDING( DSAT_VENDOR_QCMRUE_IDX, 0, DSAT_PENDING_MRU_NONE );
    result = DSAT_OK;
  }
  else
  {
    result = dsatvend_mru_read_cm_cmd(dsat_mru_read_index);
  }

  return result;
}/* dsatvend_process_mru_read_result */

#ifdef FEATURE_DATA_UCSD_SCUDIF_TEST
void ds_ucsd_trigger_incall_modification( uint8 dir, uint8 test_id );



/*===========================================================================

FUNCTION DSATVEND_EXEC_QCSCFTEST_CMD

DESCRIPTION
  This function sets the SCUIDF test mode.

DEPENDENCIES
  The values exported may or may not be appropriate for the mode-specific
  handler.  Validation in mode-specific handler is assumed. 
  
RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success.

SIDE EFFECTS
  None
  
===========================================================================*/
/* ARGSUSED */
dsat_result_enum_type dsatvend_exec_qcscftest_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type result = DSAT_ERROR;
  #define MO_INDEX 0
  #define MT_INDEX 1
  #define INCALL_SC_MODE_TEST 6
  
  /* Processing for WRITE syntax */
  if ( (tok_ptr->op == (NA|EQ|AR)) )
  {
    dsat_num_item_type orig_val[2];

    (void) dsatutil_memscpy((void*)orig_val, sizeof(orig_val),
             (void*)dsatutil_get_val(DSAT_VENDOR_QCSCFTEST_IDX,0,0,STR_TYPE),sizeof(orig_val));
    
    /* Perform default paramater processing */
    if (DSAT_OK == dsatparm_exec_param_cmd( mode,
                                            parse_table,
                                            tok_ptr,
                                            res_buff_ptr ))
    {
      /* Test for action conditions */
      if( INCALL_SC_MODE_TEST == (dsat_num_item_type)dsatutil_get_val(
                               DSAT_VENDOR_QCSCFTEST_IDX,0,1,NUM_TYPE))
      {
        if( MO_INDEX == (dsat_num_item_type)dsatutil_get_val(
                              DSAT_VENDOR_QCSCFTEST_IDX,0,0,NUM_TYPE) )
        {
          ds_ucsd_trigger_incall_modification( MO_INDEX, INCALL_SC_MODE_TEST );
          (void) dsatutil_memscpy((void*)dsatutil_get_val(DSAT_VENDOR_QCSCFTEST_IDX,0,0,STR_TYPE), sizeof(orig_val),
                   (void*)orig_val,sizeof(orig_val));
        }
        else if( MT_INDEX == (dsat_num_item_type)dsatutil_get_val(
                                DSAT_VENDOR_QCSCFTEST_IDX,0,0,NUM_TYPE) )
        {
          ds_ucsd_trigger_incall_modification( MT_INDEX, INCALL_SC_MODE_TEST );

          (void) dsatutil_memscpy((void*)dsatutil_get_val(DSAT_VENDOR_QCSCFTEST_IDX,0,0,STR_TYPE), sizeof(orig_val),
                   (void*)orig_val,sizeof(orig_val));
        }
      }
      /* Otherwise populate the exported values */
      else
      {
        dsat_set_qcscftest_exp_val(((dsat_num_item_type)dsatutil_get_val(
              DSAT_VENDOR_QCSCFTEST_IDX,0,0,NUM_TYPE)), 
           (uint8)dsatutil_get_val(DSAT_VENDOR_QCSCFTEST_IDX,0,1,NUM_TYPE));
      }
      result = DSAT_OK;
    }
  }
  /* Processing for TEST and READ syntax */
  else if ( tok_ptr->op == (NA|QU) )
  {
    char buffer[80];  /* scratch buffer */
    word size;
    
    size = (word)snprintf( buffer, sizeof(buffer),
                           "%s: %s, %d\n"
                           "%s: %s, %d",
                           parse_table->name,
                           dsat_qcscftest_list[0].list_v[DSAT_QCSCFTEST_MO_INDEX],
                           dsat_get_qcscftest_exp_val(DSAT_QCSCFTEST_MO_INDEX),
                           parse_table->name,
                           dsat_qcscftest_list[0].list_v[DSAT_QCSCFTEST_MT_INDEX],
                           dsat_get_qcscftest_exp_val(DSAT_QCSCFTEST_MT_INDEX) );
    
    dsat_dsm_pushdown_tail(&res_buff_ptr, buffer, size,
                           DSM_ITEM_POOL(res_buff_ptr), FALSE);
    
    result = DSAT_OK;
  }
  else if ( (tok_ptr->op == (NA|EQ|QU)) ||
            (tok_ptr->op == (NA)) )
  {
    result = dsatparm_exec_param_cmd( mode,
                                      parse_table,
                                      tok_ptr,
                                      res_buff_ptr );

    /* For default case, export default parameters */
    if (tok_ptr->op == (NA))
    {
      dsat_set_qcscftest_exp_val( DSAT_QCSCFTEST_MO_INDEX, 
                                  (uint8)dsat_qcscftest_list[1].default_v);
      dsat_set_qcscftest_exp_val( DSAT_QCSCFTEST_MT_INDEX,
                                  (uint8)dsat_qcscftest_list[1].default_v);
    }
  }

  return result;
} /* dsatvend_exec_qcscftest_cmd */

#endif /* FEATURE_DATA_UCSD_SCUDIF_TEST */
/*===========================================================================

FUNCTION DSATVEND_EXEC_QCSYSMODE_CMD

DESCRIPTION
  This function returns the current system mode.

DEPENDENCIES
  
RETURN VALUE

SIDE EFFECTS
  None
  
===========================================================================*/
/* ARGSUSED */
dsat_result_enum_type dsatvend_exec_qcsysmode_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type result = DSAT_ASYNC_CMD;
  
  if ((tok_ptr->op == NA) || (tok_ptr->op == (NA|QU)) )
  {
    SET_PENDING(DSAT_VENDOR_QCSYSMODE_IDX, 0, DSAT_PENDING_TRUE)

    if (dsatcmif_get_cm_ss_info() == DSAT_ERROR)
    {
      result = DSAT_ERROR;

      SET_PENDING(DSAT_VENDOR_QCSYSMODE_IDX, 0, DSAT_PENDING_FALSE)
    }
  }
  else
  {
    result = DSAT_ERROR;
  }
  return result;
}/* dsatvend_exec_qcsysmode_cmd */

/*===========================================================================

FUNCTION DSAVEND_PROCESS_QCSYSMODE_CMD

DESCRIPTION
  This function process the response for the $QCSYSMODE command
  on return from asynchronous processing.

DEPENDENCIES
  None

RETURN VALUE
  Returns an enum that describes the result of the command execution.

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatvend_process_qcsysmode_cmd
(
  dsat_cm_msg_s_type *msg_ptr
)
{
  dsat_result_enum_type result = DSAT_ERROR;
  dsat_qcsysmode_id_e_type qcsysmode_id = DSAT_QCSYSMODE_ID_MAX;
#ifdef FEATURE_DATA_IS707
  ps_iface_bearer_technology_type bearer_tech;
#endif /* FEATURE_DATA_IS707 */
  dsm_item_type *res_buff_ptr = NULL;

  DS_AT_MSG3_HIGH("hdr_hybrid = %d, hdr_srv_status = %d, hs_ind = %d",
                  msg_ptr->msg.ss_event.event_info[DSAT_STACK_HYBR].hdr_hybrid,
                  msg_ptr->msg.ss_event.event_info[DSAT_STACK_HYBR].hdr_srv_status,
                  msg_ptr->msg.ss_event.event_info[DSAT_STACK_MAIN].cell_srv_ind.hs_ind);

  switch (msg_ptr->msg.ss_event.event_info[DSAT_STACK_MAIN].sys_mode)
  {
    case SYS_SYS_MODE_NO_SRV:
      qcsysmode_id = DSAT_QCSYSMODE_ID_NO_SRV;
      break;

    case SYS_SYS_MODE_CDMA:
      if ((msg_ptr->msg.ss_event.event_info[DSAT_STACK_HYBR].hdr_hybrid == TRUE) &&
         (msg_ptr->msg.ss_event.event_info[DSAT_STACK_HYBR].hdr_srv_status == SYS_SRV_STATUS_SRV))
      {
        qcsysmode_id = DSAT_QCSYSMODE_ID_CDMA_HDR;
      }
      else if ((msg_ptr->msg.ss_event.event_info[DSAT_STACK_HYBR].is_operational == TRUE) &&
              (msg_ptr->msg.ss_event.event_info[DSAT_STACK_HYBR].sys_mode == SYS_SYS_MODE_LTE))
      {
        qcsysmode_id = DSAT_QCSYSMODE_ID_CDMA_LTE;
      }
      else
      {
        qcsysmode_id = DSAT_QCSYSMODE_ID_CDMA;
      }
     break;

    case SYS_SYS_MODE_GSM:
      qcsysmode_id = DSAT_QCSYSMODE_ID_GSM;
      break;

    case SYS_SYS_MODE_HDR:
      qcsysmode_id = DSAT_QCSYSMODE_ID_HDR;

#ifdef FEATURE_DATA_IS707
      if (ds707_pkt_mgr_get_bearer_tech_info(&bearer_tech) == TRUE)
      {
        switch (bearer_tech.data.cdma_type.rat_mask)
        {
          case PS_IFACE_CDMA_EVDO_REV0:
            qcsysmode_id = DSAT_QCSYSMODE_ID_HDR_REV0;
            break;

          case PS_IFACE_CDMA_EVDO_REVA:
            if (bearer_tech.data.cdma_type.so_mask == PS_IFACE_CDMA_EVDO_EMPA_EHRPD)
            {
              qcsysmode_id = DSAT_QCSYSMODE_ID_HDR_EMPA_EHRPD;
            }
            else
            {
              qcsysmode_id = DSAT_QCSYSMODE_ID_HDR_REVA;
            }
            break;

          case PS_IFACE_CDMA_EVDO_REVB:
            if (bearer_tech.data.cdma_type.so_mask == PS_IFACE_CDMA_EVDO_MMPA_EHRPD)
            {
              qcsysmode_id = DSAT_QCSYSMODE_ID_HDR_MMPA_EHRPD;
            }
            else
            {
              qcsysmode_id = DSAT_QCSYSMODE_ID_HDR_REVB;
            }
            break;

          default:
            DS_AT_MSG0_ERROR("Invalid HDR Protocol");

        }
      }
      else
      {
        DS_AT_MSG0_ERROR("Couldn't get HDR Protocol");
      }
#endif /* FEATURE_DATA_IS707 */
      break;

    case SYS_SYS_MODE_WCDMA:
      qcsysmode_id = DSAT_QCSYSMODE_ID_WCDMA;

      switch(msg_ptr->msg.ss_event.event_info[DSAT_STACK_MAIN].cell_srv_ind.hs_ind)
      {
        case SYS_HS_IND_HSDPA_HSUPA_UNSUPP_CELL:
          break;

        case SYS_HS_IND_HSDPA_SUPP_CELL: 
          qcsysmode_id = DSAT_QCSYSMODE_ID_WCDMA_HSDPA;
          break;

        case SYS_HS_IND_HSUPA_SUPP_CELL:
          qcsysmode_id = DSAT_QCSYSMODE_ID_WCDMA_HSUPA;
          break;

        case SYS_HS_IND_HSDPA_HSUPA_SUPP_CELL: 
          qcsysmode_id = DSAT_QCSYSMODE_ID_WCDMA_HSDPA_HSUPA;
          break;

        case SYS_HS_IND_HSDPAPLUS_SUPP_CELL: 
          qcsysmode_id = DSAT_QCSYSMODE_ID_WCDMA_HSDPA_PLUS;
          break;

        case SYS_HS_IND_HSDPAPLUS_HSUPA_SUPP_CELL: 
          qcsysmode_id = DSAT_QCSYSMODE_ID_WCDMA_HSDPA_PLUS_HSUPA;
          break;

        case SYS_HS_IND_DC_HSDPAPLUS_SUPP_CELL: 
          qcsysmode_id = DSAT_QCSYSMODE_ID_WCDMA_DC_HSDPA_PLUS;
          break;

        case SYS_HS_IND_DC_HSDPAPLUS_HSUPA_SUPP_CELL: 
          qcsysmode_id = DSAT_QCSYSMODE_ID_WCDMA_DC_HSDPA_PLUS_HSUPA;
          break;

        case SYS_HS_IND_HSDPAPLUS_64QAM_SUPP_CELL: 
          qcsysmode_id = DSAT_QCSYSMODE_ID_WCDMA_HSDPA_PLUS_64QAM;
          break;

        case SYS_HS_IND_HSDPAPLUS_64QAM_HSUPA_SUPP_CELL: 
          qcsysmode_id = DSAT_QCSYSMODE_ID_WCDMA_HSDPA_PLUS_HSUPA_64QAM;
          break;

        default:
          DS_AT_MSG0_ERROR("Invalid HS Indication");
          break;
      }
      break;

    case SYS_SYS_MODE_LTE:
      qcsysmode_id = DSAT_QCSYSMODE_ID_LTE;
      break;

    case SYS_SYS_MODE_TDS:
      qcsysmode_id = DSAT_QCSYSMODE_ID_TDS;
      break;

    default:
      DS_AT_MSG0_ERROR("Invalid Sys Mode");
      break;
  }

  if (qcsysmode_id < DSAT_QCSYSMODE_ID_MAX)
  {
    res_buff_ptr = dsat_dsm_new_buffer(DSM_DS_SMALL_ITEM_POOL, FALSE);
    res_buff_ptr->used = strlcpy((char*)res_buff_ptr->data_ptr,
                                 (const char*)dsat_qcsysmode_text[qcsysmode_id],
                                 res_buff_ptr->size);

    dsatcmdp_handle_async_cmd_rsp(res_buff_ptr, DSAT_COMPLETE_RSP);

    result = DSAT_OK;
  }
  else
  {
    result = dsat_send_cme_error(DSAT_CME_UNKNOWN);
  }

  return result;
}/* dsatvend_process_qcsysmode_cmd */
#ifdef FEATURE_DSAT_EXT_CLIENT_SUPPORT

/*===========================================================================

FUNCTION DSATVEND_EXEC_QCATMOD_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes at$qcatmod command.
  AT$QCATMOD is used indicate the SIOLIB AT Command Processing State.
  
DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_OK : if the command has been successfully executed
    DSAT_ERROR : if there was any problem in executing the command

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatvend_exec_qcatmod_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{

   ds3g_siolib_port_e_type     port_count = DS3G_SIOLIB_DATA_PORT;
   boolean                     dtr_status = FALSE;
   ds3g_siolib_state_info_type *state = NULL;
   uint8                       flag = FALSE;

  if (tok_ptr->op == (NA|EQ|AR) ) 
  {
    DS_AT_MSG0_ERROR("Write command is not supported ");
    return DSAT_ERROR;
  } 
  else if(tok_ptr->op == (NA|EQ|QU))
  {
    res_buff_ptr->used = (word) snprintf((char *)res_buff_ptr->data_ptr,
                                              res_buff_ptr->size,
                                              "$QCATMOD:(%d - %d),(%d - %d)",
                                              DS3G_SIOLIB_DATA_PORT,
                                              DS3G_SIOLIB_PORT_MAX-1,
                                              DS3G_SIOLIB_NULL_AT_STATE,
                                              DS3G_SIOLIB_ONLINE_CMD_AT_STATE);
  }
  else if(tok_ptr->op == (NA|QU) || (tok_ptr->op == NA))
  {
    while( port_count < DS3G_SIOLIB_PORT_MAX ) 
    { 
      (void)ds3g_siolib_ex_is_dtr_asserted(&dtr_status, port_count); 
      state = ds3g_siolib_get_ds3g_siolib_state(port_count); 
      if( state->serial_info.serial_state == DS3G_SIOLIB_AUTODETECT_SERIAL_STATE && dtr_status == TRUE) 
      {
        res_buff_ptr->used += (word) snprintf((char *)&res_buff_ptr->data_ptr[res_buff_ptr->used],
                                                       res_buff_ptr->size - res_buff_ptr->used,
                                                      "$QCATMOD: %d, %d \n",port_count,state->at_state);
        flag = TRUE;
     
      }
      port_count++;
    }
    if(flag == FALSE)
    {
      state = ds3g_siolib_get_ds3g_siolib_state(DS3G_SIOLIB_CLIENT_VSP_PORT);
     res_buff_ptr->used = (word) snprintf((char *)res_buff_ptr->data_ptr,
                                            res_buff_ptr->size,
                                                         "$QCATMOD: %d, %d \n",DS3G_SIOLIB_CLIENT_VSP_PORT,
                                                          state->at_state);
    }
  }
  else
  {
    return DSAT_ERROR;
  }
  return DSAT_OK;
}/*dsatvend_exec_qcatmod_cmd*/
#endif /* FEATURE_DSAT_EXT_CLIENT_SUPPORT*/
/*===========================================================================

FUNCTION  VEND_CONVERT_RSSI_ANTENNA_NUMBER

DESCRIPTION
  This function converts rssi value to antenna signal level.

DEPENDENCIES
  None

RETURN VALUE
  dsat_ante_rssi_level_e_type.

SIDE EFFECTS
  None

===========================================================================*/
LOCAL dsat_ante_rssi_level_e_type vend_convert_rssi_antenna_number
(
  void
)
{
#define SIGNAL_LEVEL_60   60
#define SIGNAL_LEVEL_75   75
#define SIGNAL_LEVEL_90   90
#define SIGNAL_LEVEL_105  105

  dsat_ante_rssi_level_e_type antenna_lvl;
  dsatcmif_servs_state_ms_info  *ph_ss_ms_val = NULL;
  
  
  if((dsat_get_qcsimapp_val() >= MAX_SUBS) || 
          (DSAT_FAILURE == dsatutil_get_base_addr(DSAT_SS_PH_MS_VALS,
                      (void **)&ph_ss_ms_val,dsat_get_qcsimapp_val())))
  {
    return RSSI_LEVEL_INVALID;
  }

  if ( ph_ss_ms_val->dsatcmif_signal_reporting.rssi < SIGNAL_LEVEL_60 )
  {
    antenna_lvl = RSSI_LEVEL_4;
  }
  else if ( ph_ss_ms_val->dsatcmif_signal_reporting.rssi < SIGNAL_LEVEL_75 )
  {
    antenna_lvl = RSSI_LEVEL_3;
  }
  else if ( ph_ss_ms_val->dsatcmif_signal_reporting.rssi < SIGNAL_LEVEL_90 )
  {
    antenna_lvl = RSSI_LEVEL_2;
  }
  else if ( ph_ss_ms_val->dsatcmif_signal_reporting.rssi < SIGNAL_LEVEL_105 )
  {
    antenna_lvl = RSSI_LEVEL_1;
  }
  else
  {
    antenna_lvl = RSSI_LEVEL_0;
  }
  if( ph_ss_ms_val->dsatcmif_signal_reporting.rssi == RSSI_NO_SIGNAL )
  {
    /* out of service */
    antenna_lvl = RSSI_LEVEL_0;
  }
  return antenna_lvl;
} /* vend_convert_rssi_antenna_number */


/*===========================================================================

FUNCTION DSATVEND_EXEC_QCANTE_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes $QCANTE command.
  The result is in res_buff_ptr, reporting number of antenna bars
  in a scale of 0-4.

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR :    if there was any problem in executing the command
    DSAT_OK :         if it is a success and execution completed.

SIDE EFFECTS
  None

===========================================================================*/
/*ARGSUSED*/
dsat_result_enum_type dsatvend_exec_qcante_cmd
(
  dsat_mode_enum_type mode,                /*  AT command mode:            */
  const dsati_cmd_type *parse_table,       /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,       /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr              /*  Place to put response       */
)
{
  dsat_result_enum_type result = DSAT_OK;

  /* process command */
  if ( tok_ptr->op == NA )
  {
    dsat_ante_rssi_level_e_type antenna_level;
    
    antenna_level = vend_convert_rssi_antenna_number();
    if(antenna_level == RSSI_LEVEL_INVALID)
    {
      return DSAT_ERROR;
    }
    /* Generate response */
    res_buff_ptr->used = 
      (word) snprintf ((char*)res_buff_ptr->data_ptr, 
                              res_buff_ptr->size,
                              "%s: %d", 
                              "$QCANTE", 
                              antenna_level );
  }
  /* process the TEST command */
  else if ( tok_ptr->op == (NA|EQ|QU) )
  {
    /* Generate supported values response */
    res_buff_ptr->used =
      (word)snprintf((char*)res_buff_ptr->data_ptr, 
                           res_buff_ptr->size ,
                           "%s",
                           "$QCANTE: (0-4)" );
  }
  /* other commands are not supported */
  else
  {
    result = DSAT_ERROR;
  }
  
  return result;
} /* dsatvend_exec_qcante_cmd */


/*===========================================================================

FUNCTION DSATVEND_EXEC_QCRPW_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes $QCRPW command.
  The result is in res_buff_ptr, reporting recieved radio signal 
  power in a scale of 0-75.

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR :    if there was any problem in executing the command
    DSAT_OK :         if it is a success and execution completed.

SIDE EFFECTS
  None

===========================================================================*/
/*ARGSUSED*/
dsat_result_enum_type dsatvend_exec_qcrpw_cmd
(
  dsat_mode_enum_type mode,                /*  AT command mode:            */
  const dsati_cmd_type *parse_table,       /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,       /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr              /*  Place to put response       */
)
{
#define DSAT_QCRPW_MAX_SIGNAL 75
#define DSAT_QCRPW_MIN_SIGNAL 0
  dsat_result_enum_type result = DSAT_OK;
  dsatcmif_servs_state_ms_info  *ph_ss_ms_val = NULL;
  

  if(dsat_get_qcsimapp_val() >= MAX_SUBS)
	return DSAT_ERROR;
  DSATUTIL_CMD_GET_BASE_ADDR(DSAT_SS_PH_MS_VALS,(void **)&ph_ss_ms_val,dsat_get_qcsimapp_val());

  /* process command */
  if ( tok_ptr->op == NA )
  {
   /* Convert RSSI value to reporting range of 0-75.
    * The RSSI value reported by CM is actually, RSCP value (l1 layer)
    * for WCDMA.
    */
    uint8 rssi_code = dsatetsime_convert_rssi(
                         ph_ss_ms_val->dsatcmif_signal_reporting.rssi,
                         DSAT_QCRPW_MAX_SIGNAL );
    if ( RSSI_TOOHI_CODE == rssi_code )
    {
       /* Capped maximum for QCRPW*/
      rssi_code = DSAT_QCRPW_MAX_SIGNAL;
    }
    else if ( RSSI_UNKNOWN_CODE == rssi_code )
    {
      rssi_code = DSAT_QCRPW_MIN_SIGNAL;
    }

    /* Generate response */
    res_buff_ptr->used = 
      (word) snprintf ((char*)res_buff_ptr->data_ptr, 
                              res_buff_ptr->size,
                              "%s: %d", 
                              "$QCRPW", 
                              rssi_code);
  }
  /* process the TEST command */
  else if ( tok_ptr->op == (NA|EQ|QU) )
  {
    /* Generate supported values response */
    res_buff_ptr->used =
      (word)snprintf((char*)res_buff_ptr->data_ptr, 
                           res_buff_ptr->size,
                           "%s",
                           "$QCRPW: (0-75)" );
  }
  /* other commands are not supported */
  else
  {
    result = DSAT_ERROR;
  }
  
  return result;
} /* dsatvend_exec_qcrpw_cmd */


/*===========================================================================

FUNCTION DSATVEND_EXEC_QCSQ_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes at$qcsq command.
  This function returns RSCP, ECIO,SIR,PATHLOSS and RSSI if present
  in the following format $QCSQ: <rscp>,<ecio>,<sir>,<pathloss>,<rssi>.

DEPENDENCIES
  None
  
RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR :    if there was any problem in executing the command
    DSAT_OK :       if it is a success and execution completed.
  
SIDE EFFECTS
  None
  
===========================================================================*/
/*ARGSUSED*/
dsat_result_enum_type dsatvend_exec_qcsq_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsatcmif_servs_state_ms_info  *ph_ss_ms_val = NULL;
  
  /* Read Command */
  if ( tok_ptr->op == (NA) )
  {
    uint16 rssi=0;
    
    if(dsat_get_qcsimapp_val() >= MAX_SUBS)
       return DSAT_ERROR;
    /* RSSI reported by CM is RSCP in actual in case of WCDMA.
     * The values reported by CM are absolute values in case of 
     * RSCP and ECIO.
     * In case of GSM , first value in $QCSQ denotes RSSI.
     * rssi(dbm)= rscp(dbm) - ecio(dbm).
     * QCSQ: <rscp>,<ecio>,<sir>,<pathloss>,<rssi>.
     */
    DSATUTIL_CMD_GET_BASE_ADDR(DSAT_SS_PH_MS_VALS,(void **)&ph_ss_ms_val,dsat_get_qcsimapp_val());

    DS_AT_MSG3_HIGH("Reported Values of RSSI %d, RSCP %d and RSSI2 %d",
              ph_ss_ms_val->dsatcmif_signal_reporting.rssi,
              ph_ss_ms_val->dsatcmif_signal_reporting.rscp,
              ph_ss_ms_val->dsatcmif_signal_reporting.rssi2);

    if( RSSI_NO_SIGNAL <= ph_ss_ms_val->dsatcmif_signal_reporting.rssi)
    {
      res_buff_ptr->used = (word)snprintf(
                               (char*)res_buff_ptr->data_ptr,
                                res_buff_ptr->size,
                                "$QCSQ :");
      return DSAT_OK;
    }
    #ifdef FEATURE_WCDMA_DATA_FOR_UI_BAR_DISPLAY
    #error code not present
#else
    res_buff_ptr->used = (word)snprintf(
                               (char*)res_buff_ptr->data_ptr,
                                res_buff_ptr->size,
                                "$QCSQ :%d,",
                                (-1)*(ph_ss_ms_val->dsatcmif_signal_reporting.rssi));
    #endif /* FEATURE_WCDMA_DATA_FOR_UI_BAR_DISPLAY */
    /* ECIO value will be reported by higher layer only 
     * when FEATURE_UMTS_REPORT_ECIO is enabled. ECIO is not valid
     * for GSM and CM will report a MAX value equivalent to 0xFF.
     */
    if( ph_ss_ms_val->dsatcmif_signal_reporting.ecio < DSAT_ECIO_NO_SIGNAL )
    {
      /* The ecio recieved from CM is 2*<actual_ecio> */
      res_buff_ptr->used = (word)snprintf(
                               (char*)res_buff_ptr->data_ptr,
                                res_buff_ptr->size,
                                "%s%d,",
                                res_buff_ptr->data_ptr,
                                (-1)*(ph_ss_ms_val->dsatcmif_signal_reporting.ecio/2));

      rssi = ph_ss_ms_val->dsatcmif_signal_reporting.rssi + 
             (uint16)(ph_ss_ms_val->dsatcmif_signal_reporting.ecio/2);
    }
    else
    {
      res_buff_ptr->used = (word)snprintf(
                              (char*)res_buff_ptr->data_ptr,
                                res_buff_ptr->size,
                                "%s,",
                                res_buff_ptr->data_ptr);
    }
    /* SIR and PATHLOSS values will be reported by higher layer only 
     * when FEATURE_UMTS_SIR_PATHLOSS is enabled. Both SIR and PATHLOSS 
     * are valid for WCDMA when in connected mode. ie, when a data/voice
     * call is present. These values are not valid in GSM mode.
     */
    if( ph_ss_ms_val->dsatcmif_signal_reporting.sir < DSAT_SIR_NO_SIGNAL )
    {
      res_buff_ptr->used = (word)snprintf(
                               (char*)res_buff_ptr->data_ptr,
                                res_buff_ptr->size,
                                "%s%d,",
                                res_buff_ptr->data_ptr,
                                ph_ss_ms_val->dsatcmif_signal_reporting.sir);
    }
    else
    {
      res_buff_ptr->used = (word)snprintf(
                               (char*)res_buff_ptr->data_ptr,
                                res_buff_ptr->size,
                                "%s,",
                                res_buff_ptr->data_ptr);
    }
    if( ph_ss_ms_val->dsatcmif_signal_reporting.pathloss < DSAT_PATHLOSS_NO_SIGNAL )
    {
      res_buff_ptr->used = (word)snprintf(
                               (char*)res_buff_ptr->data_ptr,
                                res_buff_ptr->size,
                                "%s%d,",
                                res_buff_ptr->data_ptr,
                                ph_ss_ms_val->dsatcmif_signal_reporting.pathloss);
    }
    else
    {
      res_buff_ptr->used = (word)snprintf(
                               (char*)res_buff_ptr->data_ptr,
                                res_buff_ptr->size,
                                "%s,",
                                res_buff_ptr->data_ptr);
    }
    /* RSSI value will be reported for WCDMA only 
     * when FEATURE_UMTS_REPORT_ECIO is enabled.
     */
    #ifdef FEATURE_WCDMA_DATA_FOR_UI_BAR_DISPLAY
      #error code not present
#else
    if( 0 != rssi)
    {
      res_buff_ptr->used = (word)snprintf(
                               (char*)res_buff_ptr->data_ptr,
                                res_buff_ptr->size,
                                "%s%d",
                                res_buff_ptr->data_ptr,
                                (-1)*rssi);
    }
    #endif /* FEATURE_WCDMA_DATA_FOR_UI_BAR_DISPLAY */
  }
  /* Test Command */
  else if (tok_ptr->op == (NA|EQ|QU))
  {
    res_buff_ptr->used = 
      (word) snprintf ((char*)res_buff_ptr->data_ptr, 
                              res_buff_ptr->size,
                              "$QCSQ: (-121 - -25),"
                              "(-31 - 0),(-10 - 20),"
                              "(46 - 148),(-121 - -25)" );
  }
  else
  {
    /* other commands are not supported */
    return DSAT_ERROR;
  }
  return DSAT_OK;
} /* dsatvend_exec_qcsq_cmd */


#ifdef FEATURE_DATA_PS_SLIP
/*===========================================================================

FUNCTION DSATVEND_EXEC_QCSLIP_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes AT$QCSLIP command.
  AT$QCSLIP command is used to switch to SLIP mode.
  It brings up the SLIP iface and applications can use the SLIP iface for
  data transfer.

DEPENDENCIES
  None

RETURN VALUE
  Returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR   : if there was any problem in executing the command
    DSAT_CONNECT : if SLIP is ready

SIDE EFFECTS
  None

===========================================================================*/
/* ARGSUSED */
dsat_result_enum_type dsatvend_exec_qcslip_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  ds3g_siolib_port_alloc_info_s_type  alloc_info;

  /* Allocate the command buffer and queue the command to DCC/DS Task */
  /*-------------------------------------------------------------------------
    DCC task is available, post the command to DCC.
  -------------------------------------------------------------------------*/
  slip_iface_post_cmd(SLIP_IFACE_CALLP_UP_CMD);
  /*-------------------------------------------------------------------------
  Try to allocate SIO port for external data call.  Without successfully
  downing the semaphore, the call cannot proceed.
  -------------------------------------------------------------------------*/
  memset((void*)&alloc_info, 0x0, sizeof(ds3g_siolib_port_alloc_info_s_type));
  alloc_info.mode = DS3G_SIOLIB_PORT_ALLOC_MODE_AUTOMATIC;
  alloc_info.call_dir = DS3G_SIOLIB_PORT_CALL_DIR_ORIGINATED;

  slip_ser_port = ds3g_siolib_allocate_port(&alloc_info);
  if (slip_ser_port == DS3G_SIOLIB_PORTID_NONE) 
  {
    DS_AT_MSG0_ERROR("Cannot allocate serial port for SLIP connection");
    return DSAT_ERROR;
  }

  return DSAT_CONNECT;
}/* dsatvend_exec_qcslip_cmd () */
#endif /* FEATURE_DATA_PS_SLIP */

#ifdef FEATURE_DSAT_DEV_CMDS
#ifdef FEATURE_DSAT_ETSI_MODE
/*===========================================================================

FUNCTION DSATVEND_EXEC_QCCNTI_CMD

DESCRIPTION
  This function takes the result from the command line parser and
  executes it. It executes *CNTI command, which deals as follows:
  *CNTI=<n>
  *CNTI: <n>,<tech>[,<tech>[...]]
  <n>: parameter controls the results displayed by the response
  
   0- Technology currently in use to access the network  
   1- The available technologies on the current network  
   2- All technologies supported by the device
  
  <tech>: alphanumeric string used to identify technology
   GSM
   GPRS
   EDGE
   UMTS
   HSDPA
   HSUPA

DEPENDENCIES
  None
  
RETURN VALUE
  Returns an enum that describes the result of the command execution.
  Possible values:
                        
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success for local execution
SIDE EFFECTS
  None
  
===========================================================================*/
/* ARGSUSED */
dsat_result_enum_type dsatvend_exec_qccnti_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
#define DSAT_CNTI_TECH_CURRENT 0
#define DSAT_CNTI_TECH_AVAILABLE 1
#define DSAT_CNTI_TECH_SUPPORTED 2
#define DSAT_CNTI_BUFFER_LEN    60
  dsat_result_enum_type result = DSAT_OK;
  dsat_num_item_type temp1;
  char qccnti_tech[8][12]={"NONE","GSM","GPRS","EDGE","UMTS","HSDPA","HSUPA",
                          "HSDPA+HSUPA"};

  if ( (tok_ptr->op == (NA|EQ|AR) )|| (tok_ptr->op == (NA|EQ|QU)) )
  {
    result = dsatparm_exec_param_cmd( mode,
                                      parse_table,
                                      tok_ptr,
                                      res_buff_ptr );
  }
  else if ( (tok_ptr->op != (NA|QU) ) )
  {
    result = dsat_send_cme_error( DSAT_CME_OP_NOT_SUPPORTED );
  }

  if ( result == DSAT_OK )
  {
    if ( ( tok_ptr->op == (NA|EQ|AR))||
         ( tok_ptr->op == (NA|QU)))
    {
      temp1 = (dsat_num_item_type)dsatutil_get_val(
                    DSAT_VENDOR_CNTI_IDX,dsat_get_qcsimapp_val(),0,NUM_TYPE);
      res_buff_ptr->used = (word)snprintf((char*)&res_buff_ptr->data_ptr[res_buff_ptr->used],
                                                 res_buff_ptr->size,
                                                 "*CNTI: %d",
                                                 temp1);
      temp1  = (dsat_num_item_type)dsatutil_get_val(
                    DSAT_VENDOR_CNTI_IDX,dsat_get_qcsimapp_val(),0,NUM_TYPE);
      switch ( temp1 )
      {
      case DSAT_CNTI_TECH_CURRENT:
        res_buff_ptr->used += (word)snprintf((char*)&res_buff_ptr->data_ptr[res_buff_ptr->used],
                                                     (res_buff_ptr->size - res_buff_ptr->used),
                                                     ",%s",
                                                     qccnti_tech[dsat_qccnti_tech]);
        break;
      case DSAT_CNTI_TECH_AVAILABLE:
        /* List out all technologies starting with lowest till higher 
           Display NONE when No technology is selected */
        if( (dsat_qccnti_tech == DSAT_CNTI_GSM) || 
            (dsat_qccnti_tech == DSAT_CNTI_UMTS) ||
            (dsat_qccnti_tech == DSAT_CNTI_NONE) )
        {
          res_buff_ptr->used += (word)snprintf((char*)&res_buff_ptr->data_ptr[res_buff_ptr->used],
                                                     (res_buff_ptr->size - res_buff_ptr->used),
                                                     ",%s",
                                                     qccnti_tech[dsat_qccnti_tech]);
        }
        else if( (dsat_qccnti_tech == DSAT_CNTI_GPRS) || 
                 (dsat_qccnti_tech == DSAT_CNTI_EGPRS) )
        {
          res_buff_ptr->used += (word)snprintf((char*)&res_buff_ptr->data_ptr[res_buff_ptr->used],
                                                      (res_buff_ptr->size - res_buff_ptr->used),
                                                       ",%s,%s",qccnti_tech[DSAT_CNTI_GSM],
                                                       qccnti_tech[dsat_qccnti_tech]);
        }
        else if ( dsat_qccnti_tech == DSAT_CNTI_HSDPA_HSUPA )
        {
          res_buff_ptr->used += (word)snprintf((char*)&res_buff_ptr->data_ptr[res_buff_ptr->used],
                                                      (res_buff_ptr->size - res_buff_ptr->used),
                                                      ",%s,%s,%s",qccnti_tech[DSAT_CNTI_UMTS],
                                                      qccnti_tech[DSAT_CNTI_HSDPA],
                                                      qccnti_tech[DSAT_CNTI_HSUPA]);
        }
        else
        {
          res_buff_ptr->used += (word)snprintf((char*)&res_buff_ptr->data_ptr[res_buff_ptr->used],
                                                     (res_buff_ptr->size - res_buff_ptr->used),
                                                     ",%s,%s",qccnti_tech[DSAT_CNTI_UMTS],
                                                     qccnti_tech[dsat_qccnti_tech]);
        }
        break;
      case DSAT_CNTI_TECH_SUPPORTED:
      /* List out supported technologies based on code feauturization*/
      {
        uint16 len = 0; /* Technology NONE */
        char fmt_string[DSAT_CNTI_BUFFER_LEN];
        
        memset(fmt_string,0x0,DSAT_CNTI_BUFFER_LEN);
#ifdef FEATURE_GSM
         /* Technology GSM */
         len = (word)snprintf(fmt_string,sizeof(fmt_string),", %s",qccnti_tech[1]);
#endif /* FEATURE_GSM */
#ifdef FEATURE_GSM_GPRS
        /* Technology GPRS */
          if( len < DSAT_CNTI_BUFFER_LEN )
          {
            len += (word)snprintf(fmt_string+len,sizeof(fmt_string),", %s",qccnti_tech[2]);
          }
#endif /* FEATURE_GSM_GPRS */
#ifdef FEATURE_GSM_EGPRS
        /* Technology EGPRS */
          if( len < DSAT_CNTI_BUFFER_LEN )
          {
            len += (word)snprintf(fmt_string+len,sizeof(fmt_string),", %s",qccnti_tech[3]);
          }
#endif /* FEATURE_GSM_EGPRS */
#ifdef FEATURE_WCDMA
        /* Technology UMTS */
          if( len < DSAT_CNTI_BUFFER_LEN )
          {
            len += (word)snprintf(fmt_string+len,sizeof(fmt_string),", %s",qccnti_tech[4]);
          }
#endif /* FEATURE_WCDMA */
#ifdef FEATURE_HSDPA
        /* Technology HSDPA */
          if( len < DSAT_CNTI_BUFFER_LEN )
          {
            len += (word)snprintf(fmt_string+len,sizeof(fmt_string),", %s",qccnti_tech[5]);
          }
#endif /* FEATURE_HSDPA */
#ifdef FEATURE_WCDMA_HSUPA
        /* Technology HSUPA */
          if( len < DSAT_CNTI_BUFFER_LEN )
          {
            len += (word)snprintf(fmt_string+len,sizeof(fmt_string),", %s",qccnti_tech[6]);
          }
#endif /* FEATURE_WCDMA_HSUPA */
          res_buff_ptr->used += (word)snprintf((char*)&res_buff_ptr->data_ptr[res_buff_ptr->used],
                                                      (res_buff_ptr->size - res_buff_ptr->used),
                                                      "%s", fmt_string);
      }
        break;
      default:
        result = DSAT_ERROR;
        break;
      }
    }
  }
  if(result == DSAT_ERROR)
  {
    result = dsat_send_cme_error( DSAT_CME_OP_NOT_ALLOWED );
  }
  return result;
} /* dsatvend_exec_qccnti_cmd */
#endif /* FEATURE_DSAT_ETSI_MODE */
#endif /* FEATURE_DSAT_DEV_CMDS */
/*===========================================================================

FUNCTION VEND_PROCESS_INBAND_VALUES

DESCRIPTION
  This function process the inband values and returns the values in CM
  data type. This function is used for ^BANDPREF write command to convert
  the receievd input value to CM band type. 
 
DEPENDENCIES
  None

RETURN VALUE

  CM_BAND_PREF_E_TYPE value. 

SIDE EFFECTS
  None

===========================================================================*/
cm_band_pref_e_type vend_process_inband_values
(
  dsat_bandpref_e_type in_band
)
{
  cm_band_pref_e_type out_band;
  switch( in_band )
  {
    case DSAT_BAND_PREF_BC0_A :
      out_band = CM_BAND_PREF_BC0_A;
      break;
    case DSAT_BAND_PREF_BC0_B :
      out_band = CM_BAND_PREF_BC0_B;
      break;
    case DSAT_BAND_PREF_BC0 :
      out_band = CM_BAND_PREF_BC0;
      break;
    case DSAT_BAND_PREF_BC1 :
      out_band = CM_BAND_PREF_BC1;
      break;
    case DSAT_BAND_PREF_BC3 :
      out_band = CM_BAND_PREF_BC3;
      break;
    case DSAT_BAND_PREF_BC4 :
      out_band = CM_BAND_PREF_BC4;
      break;
    case DSAT_BAND_PREF_BC5 :
      out_band = CM_BAND_PREF_BC5;
      break;
    case DSAT_BAND_PREF_GSM_DCS_1800 :
      out_band = CM_BAND_PREF_GSM_DCS_1800;
      break;
    case DSAT_BAND_PREF_GSM_EGSM_900 :
      out_band = CM_BAND_PREF_GSM_EGSM_900;
      break;
    case DSAT_BAND_PREF_GSM_PGSM_900 :
      out_band = CM_BAND_PREF_GSM_PGSM_900;
      break;
    case DSAT_BAND_PREF_BC6 :
      out_band = CM_BAND_PREF_BC6;
      break;
    case DSAT_BAND_PREF_BC7 :
      out_band = CM_BAND_PREF_BC7;
      break;
    case DSAT_BAND_PREF_BC8 :
      out_band =  CM_BAND_PREF_BC8;
      break;
    case DSAT_BAND_PREF_BC9 :
      out_band = CM_BAND_PREF_BC9;
      break;
    case DSAT_BAND_PREF_BC10 :
      out_band = CM_BAND_PREF_BC10;
      break;
    case DSAT_BAND_PREF_BC11 :
      out_band = CM_BAND_PREF_BC11;
      break;
    case DSAT_BAND_PREF_BC12 :
      out_band = CM_BAND_PREF_BC12;
      break;
    case DSAT_BAND_PREF_BC14 :
      out_band = CM_BAND_PREF_BC14;
      break;
    case DSAT_BAND_PREF_BC15 :
      out_band = CM_BAND_PREF_BC15;
      break;
    case DSAT_BAND_PREF_BC16 :
      out_band = CM_BAND_PREF_BC16;
      break;
    case DSAT_BAND_PREF_GSM_450 :
      out_band = CM_BAND_PREF_GSM_450;
      break;
    case DSAT_BAND_PREF_GSM_480 :
      out_band = CM_BAND_PREF_GSM_480;
      break;
    case DSAT_BAND_PREF_GSM_750 :
      out_band = CM_BAND_PREF_GSM_750;
      break;
    case DSAT_BAND_PREF_GSM_850 :
      out_band = CM_BAND_PREF_GSM_850;
      break;
    case DSAT_BAND_PREF_GSM_RGSM_900 :
      out_band = CM_BAND_PREF_GSM_RGSM_900;
      break;
    case DSAT_BAND_PREF_GSM_PCS_1900 :
      out_band = CM_BAND_PREF_GSM_PCS_1900;
      break;
    case DSAT_BAND_PREF_WCDMA_I_IMT_2000 :
      out_band = CM_BAND_PREF_WCDMA_I_IMT_2000;
      break;
    case DSAT_BAND_PREF_WCDMA_II_PCS_1900 :
      out_band = CM_BAND_PREF_WCDMA_II_PCS_1900;
      break;
    case DSAT_BAND_PREF_WCDMA_III_1700 :
      out_band = CM_BAND_PREF_WCDMA_III_1700;
      break;
    case DSAT_BAND_PREF_WCDMA_IV_1700 :
      out_band = CM_BAND_PREF_WCDMA_IV_1700;
      break;
    case DSAT_BAND_PREF_WCDMA_V_850 :
      out_band = CM_BAND_PREF_WCDMA_V_850;
      break;
    case DSAT_BAND_PREF_WCDMA_VI_800 :
      out_band = CM_BAND_PREF_WCDMA_VI_800;
      break;
    case DSAT_BAND_PREF_WCDMA_VII_2600 :
      out_band = CM_BAND_PREF_WCDMA_VII_2600;
      break;
    case DSAT_BAND_PREF_WCDMA_VIII_900 :
      out_band = CM_BAND_PREF_WCDMA_VIII_900;
      break;
    case DSAT_BAND_PREF_WCDMA_IX_1700 :
      out_band = CM_BAND_PREF_WCDMA_IX_1700;
      break;
    case DSAT_BAND_PREF_WLAN_2400_US :
      out_band = CM_BAND_PREF_WLAN_2400_US;
      break;
    case DSAT_BAND_PREF_WLAN_2400_JP :
      out_band = CM_BAND_PREF_WLAN_2400_JP;
      break;
    case DSAT_BAND_PREF_WLAN_2400_ETSI :
      out_band = CM_BAND_PREF_WLAN_2400_ETSI;
      break;
    case DSAT_BAND_PREF_WLAN_2400_SP :
      out_band = CM_BAND_PREF_WLAN_2400_SP;
      break;
    case DSAT_BAND_PREF_WLAN_2400_FR :
      out_band = CM_BAND_PREF_WLAN_2400_FR;
      break;
    case DSAT_BAND_PREF_WLAN_5000_US :
      out_band = CM_BAND_PREF_WLAN_5000_US;
      break;
    case DSAT_BAND_PREF_WLAN_5000_JP :
      out_band = CM_BAND_PREF_WLAN_5000_JP;
      break;
    case DSAT_BAND_PREF_WLAN_5000_ETSI :
      out_band = CM_BAND_PREF_WLAN_5000_ETSI;
      break;
    case DSAT_BAND_PREF_WLAN_5000_SP :
      out_band = CM_BAND_PREF_WLAN_5000_SP;
      break;
    case DSAT_BAND_PREF_WLAN_5000_FR :
      out_band = CM_BAND_PREF_WLAN_5000_FR;
      break;
    case DSAT_BAND_PREF_ANY :
      out_band = CM_BAND_PREF_ANY;
      break;
    default:
      out_band = CM_BAND_PREF_NONE;
  }

  return out_band;
}
/*===========================================================================

FUNCTION VEND_PROCESS_OUTBAND_VALUES

DESCRIPTION
  This function process is used for ^BANDPREF read command to convert
  the CM band pref type to DSAT band pref type and prepares a list to 
  display the data in strings. 
 
DEPENDENCIES
  None

RETURN VALUE
  TRUE: Success in conversion.
  FAIL: Failed in conversion. 

SIDE EFFECTS
  None

===========================================================================*/
boolean vend_process_outband_values
(
  cm_band_pref_e_type  out_band,
  dsat_bandpref_e_type result_list[],
  uint32               result_list_size
)
{
  uint32 out_index = 0;
  dsat_bandpref_e_type local_dsat_band = DSAT_BAND_PREF_NONE;
  cm_band_pref_e_type  local_cm_band   = CM_BAND_PREF_NONE;

  /* Reset the values to 0 so that the last data after valid band would be 
     DSAT_BAND_PREF_NONE */
  memset((void *)result_list, 0X00, result_list_size );
  while ( DSAT_BAND_PREF_MAX > local_dsat_band )
  {
    if ( result_list_size < out_index )
    {
      DS_AT_MSG2_ERROR("Insufficient space for BAND PREF list size %d, out_index %d",
                                                        result_list_size,out_index);
      return FALSE;
    }
    /* Point to the next DSAT_BAND_PREF */
    ++local_dsat_band;
    /* get the corresponding CM_BAND_PREF */
    local_cm_band = vend_process_inband_values ( local_dsat_band );
    /* Verify if this is set in CM */
    if ( local_cm_band == (out_band & local_cm_band ) )
    {
      result_list[out_index++] = local_dsat_band;
    }
  }
  DS_AT_MSG0_MED("Processing of out bands done; List is updated");
  return TRUE;
}/* vend_process_outband_values  */
/*===========================================================================
FUNCTION PROCESS_QCBANDPREF_PARAMS

DESCRIPTION
  This function process the input band values received in a byte array
  and updates the global variable dsat_bandpref_val list after parsing the data.
 
DEPENDENCIES
  None

RETURN VALUE
  returns a boolean that describes the result of parsing of data. 
  possible values:
  FALSE :    if there was any problem in parsing the data
  TRUE :     if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
boolean process_qcbandpref_params
(
  byte *band_pref  
)
{
  #define MAX_DIG_NUM 6
  
  byte noquotes_band[DSAT_MAX_BANDPREF_STRING_LENGTH] = {0};
  uint32 index = 0;
  uint32 loop = 0;
  byte temp[MAX_DIG_NUM]={0};  /* Maxnumber of digits of a max integer */
  dsat_num_item_type value = 0;
  dsat_bandpref_e_type index_band = DSAT_BAND_PREF_NONE;

  /* Extract data without quoutes */
  if ( FALSE == dsatutil_strip_quotes_out( band_pref, 
                                           noquotes_band, 
                                           DSAT_MAX_BANDPREF_STRING_LENGTH ) )
  {
    DS_AT_MSG0_ERROR("Error in stripping quotes");
    return FALSE;
  }

  /* loop through the data till null */
  while( loop < DSAT_MAX_BANDPREF_STRING_LENGTH && noquotes_band[loop] != '\0')
  {
    if ( index_band >= DSAT_MAX_BANDPREF )
    {
      DS_AT_MSG1_ERROR(" invalid band received %d",index_band);
      return FALSE;
    }
    index = 0;
    /* parse the byte data till the delimiter of , or end of string */
    while (loop < DSAT_MAX_BANDPREF_STRING_LENGTH && noquotes_band[loop] !=',' && noquotes_band [loop]!= '\0'   )
    {
      /* ERROR if more number of digits than 6 */
      if ( index >= (MAX_DIG_NUM -1))
      {
        return FALSE;
      }
      temp[index++] = noquotes_band[loop++];
    }
    temp[index] = '\0';

    /* convert the parsed ascii format of bytes of data into integer number */
    if ( ATOI_OK != dsatutil_atoi( &value, temp, 10 ) )
    {
      return FALSE;
    }

    dsat_bandpref_val[index_band++]=(dsat_bandpref_e_type)value;
    loop++;
  }
  dsat_bandpref_val[index_band] = DSAT_BAND_PREF_NONE;

  return TRUE;

}/* process_qcbandpref_params */

/*===========================================================================
FUNCTION DSATVEND_EXEC_QCBANDPREF_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes at$qcbandpref command.
  This command provides the ability to set/get the band preferences

  
<band_pref>: Is a string containing indexes separated by comma. 
for eg: "1,2,3,4,5,6" which means set all the bandprefs which has indexes from
1,2,3,4,5,6 in the test command.

<persitence>:0/1

currently, the band preference change is not persistent. 

DEPENDENCIES
  
RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_ASYNC_CMD : if success.
SIDE EFFECTS
  None
  
===========================================================================*/
/* ARGSUSED */
dsat_result_enum_type dsatvend_exec_qcbandpref_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type result = DSAT_OK;
  dsat_error_info_s_type  err_info;
  
  err_info.errval = DSAT_ERR_NO_ERROR;
  err_info.arg_num = 0;

  if ( (NA|EQ|AR) == tok_ptr->op )
  {
    if ( ( tok_ptr->args_found > 2 ) || ( !VALID_TOKEN(1) ))
    {
      err_info.errval = DSAT_ERR_INVALID_NUM_OF_ARGS;
      goto send_error;
    }

  /* Prepare a list of band pref values out of the given string and 
       update it in dsat_bandpref_val*/
    if ( FALSE == process_qcbandpref_params(tok_ptr->arg[1]))
    {
      err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
      err_info.arg_num = 1;
      goto send_error;
    }

    /* Extract persistence details; default is NOT PERSISTANT  */
    err_info.arg_num = 0;
    if ( VALID_TOKEN(0) )
    {
       if ( ( ATOI_OK != dsatutil_atoi( &dsat_bandpref_pers_val,
                                     tok_ptr->arg[0], 
                                     10 ) ) )
       {
         err_info.errval =  DSAT_ERR_INVALID_ARGUMENT;
         goto send_error;
       }
       if ( ( dsat_bandpref_pers_val != DSAT_BANDPREF_NON_PERS ) &&
           ( dsat_bandpref_pers_val != DSAT_BANDPREF_PERS ) )
       {
         err_info.errval =  DSAT_ERR_PARAMETER_OUT_OF_RANGE;
         goto send_error;
       }
    }
    else
    {
       /* Set default persistance value to 0 when second parameter is not given */
       dsat_bandpref_pers_val = DSAT_BANDPREF_NON_PERS;
    }
    /* Setting bandpref would happen in two stages. 
       1. Get the mode pref to validate the band pref values 
       2. Set the band pref */
    SET_PENDING(DSAT_VENDOR_QCBANDPREF_IDX ,0, DSAT_PENDING_PREFMODE_BANDPREF_VERIFY)
    /* The return value is ignored as both success and failure cases
      * are handled by the call back function */
#ifdef FEATURE_DUAL_SIM
     cm_ph_cmd_get_ph_info_per_subs(dsatcmif_ph_cmd_cb_func,
                                           NULL,
                                           dsatcm_client_id,
                                           dsat_qcsimapp_info[dsat_get_qcsimapp_info_idx()].subs_id
                                           );
#else
    (void)cm_ph_cmd_get_ph_info (dsatcmif_ph_cmd_cb_func,
                                 NULL,
                                 dsatcm_client_id
                                 );
#endif /* FEATURE_DUAL_SIM */
    result = DSAT_ASYNC_CMD;
  }
  if ( (NA|QU) == tok_ptr->op )
  {
    SET_PENDING(DSAT_VENDOR_QCBANDPREF_IDX ,0, DSAT_PENDING_PREFMODE_BANDPREF_READ)
    /* The return value is ignored as both success and failure cases
     * are handled by the call back function */
#ifdef FEATURE_DUAL_SIM
    cm_ph_cmd_get_ph_info_per_subs(dsatcmif_ph_cmd_cb_func,
                                          NULL,
                                          dsatcm_client_id,
                                          dsat_qcsimapp_info[dsat_get_qcsimapp_info_idx()].subs_id
                                          );
#else
    (void)cm_ph_cmd_get_ph_info (dsatcmif_ph_cmd_cb_func,
                                 NULL,
                                 dsatcm_client_id
                                 );
#endif /* FEATURE_DUAL_SIM */

    result = DSAT_ASYNC_CMD;

  }
  if ( (NA|EQ|QU) == tok_ptr->op )
  {
    char local_buffer[30];
    word data_size = 0;
    uint32 band_index = 1;

    res_buff_ptr->used = (word)snprintf((char *)res_buff_ptr->data_ptr,
                                 res_buff_ptr->size,
                                 "$QCBANDPREF: (0-1),\n");
    while ( dsat_bandpref_tststr[band_index][0] != NULL )
    {
      data_size = (word)snprintf(local_buffer,
                                       sizeof(local_buffer),
                                       "%s\n",dsat_bandpref_tststr[band_index]);

      dsat_dsm_pushdown_tail(&res_buff_ptr,
                             local_buffer,
                             data_size,
                             DSM_ITEM_POOL(res_buff_ptr),
                             FALSE);

      ++band_index;
    }
    result = DSAT_OK;
  }

  return result;

send_error:
  if( err_info.errval == DSAT_ERR_INVALID_NUM_OF_ARGS )
  {
    DS_AT_MSG1_ERROR("Error:%d",err_info.errval);
  }
  else
  {
    DS_AT_MSG2_ERROR("Error:%d, arg_num:%d",err_info.errval,err_info.arg_num);
  }
  return DSAT_ERROR;
}/* dsatvend_exec_qcbandpref_cmd */
/*===========================================================================
FUNCTION DSATVEND_EXEC_PREFMODE_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes at^prefmode command.
  This command provides the ability to set/get the network mode preferences. 

<pref_mode> network mode, values as follows:
0 Automatic
2 CDMA mode
4 HDR mode
8 CDMA/HDR HYBRID mode

 The following note is ignored:
For the data card which only support CDMA 1X, the command isn't realized.

DEPENDENCIES
  
RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK/DSAT_ASYNC_CMD : if success.
    
SIDE EFFECTS
  None
  
===========================================================================*/
/* ARGSUSED */
dsat_result_enum_type dsatvend_exec_prefmode_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type result = DSAT_OK;
  cm_mode_pref_e_type  pref_mode;
  cm_hybr_pref_e_type  hybr_pref = CM_HYBR_PREF_NO_CHANGE;
  dsat_num_item_type in_pref_val;
  dsat_error_info_s_type  err_info;
  
  err_info.errval = DSAT_ERR_NO_ERROR;
  err_info.arg_num = 0;

  if ( !IS_CDMA_MODE(dsatcmdp_get_current_mode() ))
  {
    DS_AT_MSG0_ERROR("Not applicable in UMTS mode ");
    return DSAT_ERROR;
  }
  if ( (NA|EQ|AR) == tok_ptr->op )
  {
    /* Verify the input parameter range  */

    if ( ( tok_ptr->args_found > 1 ) || ( !VALID_TOKEN(0) ) )
    {
      err_info.errval = DSAT_ERR_INVALID_NUM_OF_ARGS;
      goto send_error;
    }
    if ( dsatutil_atoi( &in_pref_val, 
                       tok_ptr->arg[0], 10) != ATOI_OK )
    {
      err_info.errval = DSAT_ERR_ATOI_CONVERSION_FAILURE;
      err_info.errval = 0;
      goto send_error;
    }

    switch( in_pref_val )
    {
      
#ifndef FEATURE_DSAT_EXTENDED_CMD
        case DSAT_PREFMODE_SYSMODE_AUTOMATIC:
        pref_mode = CM_MODE_PREF_AUTOMATIC;
        break;
#endif /* FEATURE_DSAT_EXTENDED_CMD */
      case DSAT_PREFMODE_SYSMODE_CDMA:
        pref_mode = CM_MODE_PREF_CDMA_ONLY;
        break;
      case DSAT_PREFMODE_SYSMODE_HDR:
        pref_mode = CM_MODE_PREF_HDR_ONLY;
        break;
      case DSAT_PREFMODE_SYSMODE_HYBRID_CDMA_HDR:
        pref_mode = CM_MODE_PREF_CDMA_HDR_ONLY;
        /* 8960 onwards HYBD_PREF can be set only for HDR_LTE_CDMA  */
        hybr_pref = CM_HYBR_PREF_NO_CHANGE;
        break;
      default:
        err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
        err_info.errval = 0;
        goto send_error;
    }

    /* Verify if the pref values are correct */
    if ( FALSE == cm_is_valid_mode_pref(pref_mode))
    {
      DS_AT_MSG0_ERROR("Invalid CM MODE PREF setting");
      (void)dsat_send_cme_error(DSAT_CME_OP_NOT_ALLOWED);
      return DSAT_CMD_ERR_RSP;
    }
    SET_PENDING(DSAT_VENDOR_PREFMODE_IDX ,0, DSAT_PENDING_PREFMODE_SYSMODE_WRITE)
    (void) cm_ph_cmd_sys_sel_pref_new ( dsatcmif_ph_cmd_cb_func,
                                    NULL,
                                    dsatcm_client_id,
                                    pref_mode,
                                    CM_PREF_TERM_PERMANENT,
                                    0,
                                    CM_GW_ACQ_ORDER_PREF_NO_CHANGE,
                                    CM_BAND_PREF_NO_CHANGE,
                                    CM_PRL_PREF_NO_CHANGE,
                                    CM_ROAM_PREF_NO_CHANGE,
                                    hybr_pref,
                                    CM_SRV_DOMAIN_PREF_NO_CHANGE,
                                    CM_NETWORK_SEL_MODE_PREF_AUTOMATIC,
                                    NULL 
                                  ); 
    result = DSAT_ASYNC_CMD;
  }
  else if ( (NA|EQ|QU) == tok_ptr->op )
  {
    res_buff_ptr->used = (word)snprintf((char *)res_buff_ptr->data_ptr,
                                        res_buff_ptr->size,
    #ifdef FEATURE_DSAT_EXTENDED_CMD
                                              "^PREFMODE:(%d,%d,%d)",
#else 
                                              "^PREFMODE:(%d,%d,%d,%d)",
                                        DSAT_PREFMODE_SYSMODE_AUTOMATIC,
#endif /* FEATURE_DSAT_EXTENDED_CMD */
                                        DSAT_PREFMODE_SYSMODE_CDMA,
                                        DSAT_PREFMODE_SYSMODE_HDR,
                                        DSAT_PREFMODE_SYSMODE_HYBRID_CDMA_HDR);
    result = DSAT_OK;
  }
  else if ( (NA|QU) == tok_ptr->op )
  {
    SET_PENDING(DSAT_VENDOR_PREFMODE_IDX ,0, DSAT_PENDING_PREFMODE_SYSMODE_READ)
    /* The return value is ignored as both success and failure cases
     * are handled by the call back function */
    (void)cm_ph_cmd_get_ph_info (dsatcmif_ph_cmd_cb_func,
                                 NULL,
                                 dsatcm_client_id
                                 );
    
    result = DSAT_ASYNC_CMD;

  }
  else
  {
    result = DSAT_ERROR;
  }

  return result;

send_error:
  if( err_info.errval == DSAT_ERR_INVALID_NUM_OF_ARGS )
  {
    DS_AT_MSG1_ERROR("Error:%d",err_info.errval);
  }
  else
  {
    DS_AT_MSG2_ERROR("Error:%d, arg_num:%d",err_info.errval,err_info.arg_num);
  }
  return DSAT_ERROR;

}/*  dsatvend_exec_prefmode_cmd */
/*===========================================================================

FUNCTION DSAVEND_PROCESS_PREFMODE_CMD

DESCRIPTION
  This function process the response for the ^PREFMODE command on return from
  asynchronous processing. 
 
DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR :    if there was any problem in executing the command
    DSAT_CMD_ERR_RESP:  If mode value is not valid. 
    DSAT_OK :       if it is a success.

SIDE EFFECTS
  None

===========================================================================*/

dsat_result_enum_type dsatvend_process_prefmode_cmd 
(
  dsat_cmd_pending_enum_type cmd_type,
  ds_at_cm_ph_pref_type  *pref_mode    /* Network preference mode */
)
{
  dsat_result_enum_type result = DSAT_ERROR;
  ASSERT( pref_mode != NULL );
  DS_AT_MSG2_HIGH("Processing PREFMODE cmd %d, mode pref %d", cmd_type,
                                           pref_mode->network_rat_mode_pref);
  switch ( cmd_type )
  {
    case DSAT_PENDING_PREFMODE_SYSMODE_READ:
    {
      dsm_item_type *res_buff_ptr = NULL;
      switch ( pref_mode->network_rat_mode_pref )
      {
        case CM_MODE_PREF_CDMA_ONLY:
          DSATUTIL_SET_VAL(DSAT_VENDOR_PREFMODE_IDX,0,0,0,DSAT_PREFMODE_SYSMODE_CDMA,NUM_TYPE)
          break;
        case CM_MODE_PREF_HDR_ONLY:
          DSATUTIL_SET_VAL(DSAT_VENDOR_PREFMODE_IDX,0,0,0,DSAT_PREFMODE_SYSMODE_HDR,NUM_TYPE)
          break;
        case CM_MODE_PREF_CDMA_HDR_ONLY:
          DSATUTIL_SET_VAL(DSAT_VENDOR_PREFMODE_IDX,0,0,0,DSAT_PREFMODE_SYSMODE_HYBRID_CDMA_HDR,NUM_TYPE)
          break;
        case CM_MODE_PREF_NO_CHANGE:
          /* Nothing changed report the old prefmode value */
          break;
        case CM_MODE_PREF_AUTOMATIC:
        default:
          DSATUTIL_SET_VAL(DSAT_VENDOR_PREFMODE_IDX,0,0,0,DSAT_PREFMODE_SYSMODE_AUTOMATIC,NUM_TYPE)
          DS_AT_MSG1_ERROR("Invalid pref mode %d received, seeting it to Automatic",
                                          pref_mode->network_rat_mode_pref);
      }
      res_buff_ptr = dsat_dsm_new_buffer(DSM_DS_SMALL_ITEM_POOL, FALSE);
      res_buff_ptr->used = (word)snprintf((char *)res_buff_ptr->data_ptr,
                                              res_buff_ptr->size,
                                             "^PREFMODE:%d", 
                                             (dsat_num_item_type)dsatutil_get_val(
                                              DSAT_VENDOR_PREFMODE_IDX,0,0,NUM_TYPE));
      dsatcmdp_handle_async_cmd_rsp(res_buff_ptr, DSAT_COMPLETE_RSP);
      result = DSAT_OK;
    }
      break;
    case DSAT_PENDING_PREFMODE_SYSMODE_WRITE:
    {
      /* Nothing to do  */
      result = DSAT_OK;
    }
      break;
    case DSAT_PENDING_PREFMODE_BANDPREF_READ:
    {

      dsat_bandpref_e_type  result_list[DSAT_MAX_BANDPREF];
      char local_buffer[30];
      dsm_item_type *res_buff_ptr = NULL;
      word data_size = 0;
      uint32 index = 0;

      if ( FALSE == vend_process_outband_values( pref_mode->network_band_pref,
                                   &result_list[0],
                                   (uint32)DSAT_MAX_BANDPREF ) )
      {
        DS_AT_MSG0_ERROR("Band preferene query failed");
        result = DSAT_ERROR;
        break;
      }
      res_buff_ptr = dsat_dsm_new_buffer(DSM_DS_LARGE_ITEM_POOL, FALSE);
      res_buff_ptr->used = (word)snprintf ( (char *)res_buff_ptr->data_ptr,
                                             res_buff_ptr->size,
                                             "$QCBANDPREF: \n");
      /* Need to add one more condition for dsat_bandpref_tststr boundary */
      while ( ( index < (uint32)DSAT_MAX_BANDPREF )&& 
              ( result_list[index] != DSAT_BAND_PREF_NONE ) )
      {
        data_size = (word)snprintf( local_buffer,
                                    30,
                             "%s\n",dsat_bandpref_tststr[result_list[index]]);

        dsat_dsm_pushdown_tail(&res_buff_ptr,
                                local_buffer, data_size,
                                DSM_ITEM_POOL(res_buff_ptr), FALSE);

        ++index;
      }
      dsatcmdp_handle_async_cmd_rsp(res_buff_ptr, DSAT_COMPLETE_RSP);
      result = DSAT_OK;
    }
      break;
    case DSAT_PENDING_PREFMODE_BANDPREF_VERIFY:
      {
        uint32 index = 0;
        cm_band_pref_e_type temp_band_pref = CM_BAND_PREF_NONE;
        cm_band_pref_e_type req_band_pref  = CM_BAND_PREF_NONE;
        cm_pref_term_e_type pref_term = CM_PREF_TERM_PWR_CYCLE;

        /* Prepare the required CM band pref type from the received input
           which is stored as DSAT band pref type. */
        while ( dsat_bandpref_val[index] != DSAT_BAND_PREF_NONE )
        {
          if ( CM_BAND_PREF_NONE == 
                ( temp_band_pref = vend_process_inband_values(
                                               dsat_bandpref_val[index])))
          {
            DS_AT_MSG1_ERROR("Invalid band preference %d",dsat_bandpref_val[index ]);
            memset((void *)dsat_bandpref_val, 0x0, sizeof(dsat_bandpref_val));
            SET_PENDING(DSAT_VENDOR_QCBANDPREF_IDX ,0, DSAT_PENDING_PREFMODE_NONE)
            return DSAT_ERROR;
          }
          req_band_pref |= temp_band_pref;
          ++index;
        }
        /* Verify if the requested bandpref is valid in current mode */
        DS_AT_MSG2_MED("network_rat_mode_pref = %d, req_band_pref = %d",
                       pref_mode->network_rat_mode_pref, req_band_pref);
        if ( FALSE == cm_is_valid_mode_band_pref( 
                                     pref_mode->network_rat_mode_pref, 
                                     req_band_pref ))
        {
          DS_AT_MSG0_ERROR("Invalid Mode+Band Preference");
          SET_PENDING(DSAT_VENDOR_QCBANDPREF_IDX ,0, DSAT_PENDING_PREFMODE_NONE)
          (void)dsat_send_cme_error(DSAT_CME_OP_NOT_ALLOWED);
          return DSAT_CMD_ERR_RSP;
        }

        if ( DSAT_BANDPREF_PERS == dsat_bandpref_pers_val )
        {
          pref_term = CM_PREF_TERM_PERMANENT;
        }
#ifdef FEATURE_DUAL_SIM
        (void) cm_ph_cmd_sys_sel_pref_per_subs( dsatcmif_ph_cmd_cb_func,
                                                NULL,
                                                dsatcm_client_id,
                                                dsat_qcsimapp_info[dsat_get_qcsimapp_info_idx()].subs_id,
                                                CM_MODE_PREF_NO_CHANGE,
                                                pref_term,
                                                0,
                                                CM_GW_ACQ_ORDER_PREF_NO_CHANGE,
                                                req_band_pref,
                                                CM_PRL_PREF_NO_CHANGE,
                                                CM_ROAM_PREF_NO_CHANGE,
                                                CM_HYBR_PREF_NO_CHANGE,
                                                CM_SRV_DOMAIN_PREF_NO_CHANGE,
                                                CM_NETWORK_SEL_MODE_PREF_AUTOMATIC,
                                                NULL
                                               );
#else
        (void) cm_ph_cmd_sys_sel_pref_new ( dsatcmif_ph_cmd_cb_func,
                                      NULL,
                                      dsatcm_client_id,
                                      CM_MODE_PREF_NO_CHANGE,
                                      pref_term,
                                      0,
                                      CM_GW_ACQ_ORDER_PREF_NO_CHANGE,
                                      req_band_pref,
                                      CM_PRL_PREF_NO_CHANGE,
                                      CM_ROAM_PREF_NO_CHANGE,
                                      CM_HYBR_PREF_NO_CHANGE,
                                      CM_SRV_DOMAIN_PREF_NO_CHANGE,
                                      CM_NETWORK_SEL_MODE_PREF_AUTOMATIC,
                                      NULL 
                                            ); 
#endif /* FEATURE_DUAL_SIM */

        DS_AT_MSG0_HIGH(" Band pref phase 1 succesful, posted commadn for phase 2");
        return DSAT_ASYNC_CMD;

      }
    case DSAT_PENDING_PREFMODE_BANDPREF_WRITE:
    {
      /* Setting of band pref is done, Nothing to do */
      return DSAT_OK;
    }
    default:
      DS_AT_MSG1_ERROR("Preference mode mismatch %d",pref_mode->network_rat_mode_pref);
      ASSERT(0);
  }

  return result;
}/* dsatvend_process_prefmode_cmd */
/*===========================================================================
  FUNCTION DSATVEND_EXEC_SYSINFO_CMD

  DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes AT^SYSINFO command.

  The command inquires the current system message. Such as 
  system service status, domain, roam, system mode, UIM card status, etc.

  <srv_status> system service status, values as follows:
  0 no service
  1 limited service
  2 service available
  3 limited area service
  4 power saving and dormancy status.

  <srv_domain> system service, values as follows:
  0 no service
  1 only CS service
  2 only PS service
  3 PS+CS service
  4 CS and PS don't register and are in the status of serching.
  255 CDMA doesn't support.

  <roam_status> roaming status, values as follows:
  0 non-roaming status.
  1 roaming status.

  <sys_mode> system mode, values as follows:
  0 no service
  1 AMPS mode (not use provisionally)
  2 CDMA mode
  3 GSM/GPRS mode
  4 HDR mode
  5 WCDMA mode
  6 GPS mode
  7 GSM/WCDMA
  8 CDMA/HDR HYBRID

  <sim_stat> UIM card status, values as follows:
  1 UIM card status available
  240 ROMSIM version
  255 UIM card doesn't exist

   Currently following are not supported
  <lock_state> CDMA production doesn't use the parameter.
  <sys_submode> CDMA production doesn't use the parameter.


  DEPENDENCIES
  None

  RETURN VALUE
  Returns an enum that describes the result of the command execution.
  possible values:
  DSAT_ERROR : if there was any problem in executing the command
  DSAT_ASYNC_CMD : if it is a success.

  SIDE EFFECTS
  None.
===========================================================================*/
dsat_result_enum_type dsatvend_exec_sysinfo_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *tab_entry,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type result = DSAT_ASYNC_CMD;

  if (( dsat_sysinfo_cmd.dsat_rtre_config == DSAT_RTRE_CONTROL_NONE ) && 
      (dsatcmdp_get_operating_cmd_mode() == CDMA_CMD_MODE))
  {
    DS_AT_MSG0_ERROR("RTRE not configured yet");
    return DSAT_ERROR;
  }
  /* Only NA is supported */
  if ( tok_ptr->op == NA )
  {
    dsat_sysinfo_cmd.cmd_pending = TRUE;
    if ( DSAT_ERROR == dsatcmif_get_cm_ss_info() )
    {
      result = DSAT_ERROR;
    }
  }
  else
  {
    return DSAT_ERROR;
  }

  return result;
}/* dsatvend_exec_sysinfo_cmd */
/*===========================================================================

FUNCTION  VEND_CONVERT_PH_PREF_TYPE

DESCRIPTION
  This Function converts various SYS parameters to ATCoP values and vice-versa 
DEPENDENCIES
  None

RETURN VALUE
    DSAT_ERROR : if there was any problem in execution.
    DSAT_OK  : if it is a success. 

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type vend_convert_ph_pref_type
(
  boolean to_at_state,
  dsat_sysconfig_type *at_pref_ptr,
  ds_at_cm_ph_pref_type *cm_pref_ptr
)
{

  if( to_at_state == TRUE )
  {
    switch( cm_pref_ptr->network_rat_mode_pref )
    {
      case CM_MODE_PREF_AUTOMATIC:
        at_pref_ptr->mode_val = DSAT_SYSCONFIG_MODE_AUTOMATIC;
        break;
      case CM_MODE_PREF_GSM_ONLY:
        at_pref_ptr->mode_val = DSAT_SYSCONFIG_MODE_GSM ;
        break;
      case CM_MODE_PREF_WCDMA_ONLY:
        at_pref_ptr->mode_val = DSAT_SYSCONFIG_MODE_WCDMA;
        break;
#ifdef FEATURE_TDSCDMA		
      case CM_MODE_PREF_TDS_ONLY:
        at_pref_ptr->mode_val = DSAT_SYSCONFIG_MODE_TDSCDMA;
        break;
#endif /* FEATURE_TDSCDMA */		
      case CM_MODE_PREF_NO_CHANGE:
        at_pref_ptr->mode_val = DSAT_SYSCONFIG_MODE_NO_CHANGE;
        break;
      default:
        DS_AT_MSG1_ERROR("Unsupported pref mode %d ",cm_pref_ptr->network_rat_mode_pref);
        at_pref_ptr->mode_val = DSAT_SYSCONFIG_MODE_UNKNOWN;
    }


    switch( cm_pref_ptr->acq_order_pref )
    {
      case CM_GW_ACQ_ORDER_PREF_AUTOMATIC:
        at_pref_ptr->order_val =DSAT_SYSCONFIG_ORDER_AUTOMATIC ;
        break;
      case CM_GW_ACQ_ORDER_PREF_GSM_WCDMA:
        at_pref_ptr->order_val =DSAT_SYSCONFIG_ORDER_GSM_WCDMA ;
        break;
      case CM_GW_ACQ_ORDER_PREF_WCDMA_GSM:
        at_pref_ptr->order_val =DSAT_SYSCONFIG_ORDER_WCDMA_GSM ;
        break;
      case CM_GW_ACQ_ORDER_PREF_NO_CHANGE:
        at_pref_ptr->order_val =DSAT_SYSCONFIG_ORDER_NO_CHANGE ;
        break;
      default:
        DS_AT_MSG1_ERROR("Invalid acq_order %d ",cm_pref_ptr->acq_order_pref);
        at_pref_ptr->order_val =DSAT_SYSCONFIG_ORDER_UNKNOWN ;

    }

    switch( cm_pref_ptr->network_roam_pref )
    {
      case CM_ROAM_PREF_HOME:
        at_pref_ptr->roam_val =  DSAT_SYSCONFIG_ROAM_NONE;
        break;
      case CM_ROAM_PREF_ANY:
        at_pref_ptr->roam_val = DSAT_SYSCONFIG_ROAM_ANY;
        break;
      case CM_ROAM_PREF_NO_CHANGE:
        at_pref_ptr->roam_val = DSAT_SYSCONFIG_ROAM_NO_CHANGE ;
        break;
      default:
        DS_AT_MSG1_ERROR("Invalid roam mode %d ",cm_pref_ptr->network_roam_pref);
        at_pref_ptr->roam_val = DSAT_SYSCONFIG_ROAM_UNKNOWN ;

    }
	
    switch( cm_pref_ptr->service_domain_pref )
    {
      case CM_SRV_DOMAIN_PREF_CS_ONLY:
        at_pref_ptr->domain_val =  DSAT_SYSCONFIG_DOMAIN_CS;
        break;
      case CM_SRV_DOMAIN_PREF_PS_ONLY:
        at_pref_ptr->domain_val =  DSAT_SYSCONFIG_DOMAIN_PS;
        break;
      case CM_SRV_DOMAIN_PREF_CS_PS:
        at_pref_ptr->domain_val =  DSAT_SYSCONFIG_DOMAIN_CS_PS;
        break;
      case CM_SRV_DOMAIN_PREF_ANY:
        at_pref_ptr->domain_val =  DSAT_SYSCONFIG_DOMAIN_ANY;
        break;
      case CM_SRV_DOMAIN_PREF_NO_CHANGE:
        at_pref_ptr->domain_val =  DSAT_SYSCONFIG_DOMAIN_NO_CHANGE;
        break;
      default:
        DS_AT_MSG1_ERROR("Invalid roam mode %d ",cm_pref_ptr->service_domain_pref);
        at_pref_ptr->domain_val =  DSAT_SYSCONFIG_DOMAIN_UNKNOWN;

    }
  }
  else
  {
    switch( at_pref_ptr->mode_val )
    {
      case DSAT_SYSCONFIG_MODE_AUTOMATIC:
        cm_pref_ptr->network_rat_mode_pref = CM_MODE_PREF_AUTOMATIC;
        break;
      case DSAT_SYSCONFIG_MODE_GSM:
        cm_pref_ptr->network_rat_mode_pref = CM_MODE_PREF_GSM_ONLY;
        break;
      case DSAT_SYSCONFIG_MODE_WCDMA:
        cm_pref_ptr->network_rat_mode_pref = CM_MODE_PREF_WCDMA_ONLY;
        break;
#ifdef FEATURE_TDSCDMA		
      case DSAT_SYSCONFIG_MODE_TDSCDMA:
        cm_pref_ptr->network_rat_mode_pref = CM_MODE_PREF_TDS_ONLY;
        break;
#endif /* FEATURE_TDSCDMA */
      case DSAT_SYSCONFIG_MODE_NO_CHANGE:
        cm_pref_ptr->network_rat_mode_pref = CM_MODE_PREF_NO_CHANGE;
        break;
      default:
        DS_AT_MSG1_ERROR("Invalid pref mode %d ",at_pref_ptr->mode_val);
        return DSAT_ERROR;
    }

    switch( at_pref_ptr->order_val )
    {
      case DSAT_SYSCONFIG_ORDER_AUTOMATIC:
        cm_pref_ptr->acq_order_pref = CM_GW_ACQ_ORDER_PREF_AUTOMATIC;
        break;
      case DSAT_SYSCONFIG_ORDER_GSM_WCDMA:
        cm_pref_ptr->acq_order_pref = CM_GW_ACQ_ORDER_PREF_GSM_WCDMA;
        break;
      case DSAT_SYSCONFIG_ORDER_WCDMA_GSM:
        cm_pref_ptr->acq_order_pref = CM_GW_ACQ_ORDER_PREF_WCDMA_GSM;
        break;
      case DSAT_SYSCONFIG_ORDER_NO_CHANGE:
        cm_pref_ptr->acq_order_pref = CM_GW_ACQ_ORDER_PREF_NO_CHANGE;
        break;
      default:
        DS_AT_MSG1_ERROR("Invalid acq_order %d ",at_pref_ptr->order_val);
        return DSAT_ERROR;
    }
	
    switch( at_pref_ptr->roam_val )
    {
      case DSAT_SYSCONFIG_ROAM_NONE:
        cm_pref_ptr->network_roam_pref = CM_ROAM_PREF_HOME;
        break;
      case DSAT_SYSCONFIG_ROAM_ANY:
        cm_pref_ptr->network_roam_pref = CM_ROAM_PREF_ANY;
        break;
      case DSAT_SYSCONFIG_ROAM_NO_CHANGE:
        cm_pref_ptr->network_roam_pref = CM_ROAM_PREF_NO_CHANGE;
        break;

      default:
        DS_AT_MSG1_ERROR("Invalid roam mode %d ",at_pref_ptr->roam_val);
        return DSAT_ERROR;
    }
	
    switch( at_pref_ptr->domain_val )
    {
      case DSAT_SYSCONFIG_DOMAIN_CS:
        cm_pref_ptr->service_domain_pref = CM_SRV_DOMAIN_PREF_CS_ONLY;
        break;
      case DSAT_SYSCONFIG_DOMAIN_PS:
        cm_pref_ptr->service_domain_pref = CM_SRV_DOMAIN_PREF_PS_ONLY;
        break;
      case DSAT_SYSCONFIG_DOMAIN_CS_PS:
        cm_pref_ptr->service_domain_pref = CM_SRV_DOMAIN_PREF_CS_PS;
        break;
      case DSAT_SYSCONFIG_DOMAIN_ANY:
        cm_pref_ptr->service_domain_pref = CM_SRV_DOMAIN_PREF_ANY;
        break;
      case DSAT_SYSCONFIG_DOMAIN_NO_CHANGE:
        cm_pref_ptr->service_domain_pref = CM_SRV_DOMAIN_PREF_NO_CHANGE;
        break;
      default:
        DS_AT_MSG1_ERROR("Invalid roam mode %d ",at_pref_ptr->domain_val);
        return DSAT_ERROR;
    }

  }
  return DSAT_OK;
}/* vend_convert_ph_pref_type */

/*===========================================================================

FUNCTION DSATVEND_PROCESS_SYSCONFIG_CMD

DESCRIPTION
  This function process the response for the ^SYSCONFIG command on return from
  asynchronous processing. 
 
DEPENDENCIES
  None

RETURN VALUE
  Returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR :    if there was any problem in executing the command
    DSAT_OK :       if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatvend_process_sysconfig_cmd 
(
  dsat_cmd_pending_enum_type cmd_type,
  ds_at_cm_ph_pref_type  *pref_mode    /* Network preference mode */
)
{
  dsat_result_enum_type result = DSAT_ERROR;

  switch ( cmd_type )
  {
    case DSAT_PENDING_SYSCONFIG_READ:
    {
      dsm_item_type *res_buff_ptr = NULL;
      dsat_sysconfig_type in_pref_val;

      result = vend_convert_ph_pref_type( TRUE,&in_pref_val,pref_mode );
      res_buff_ptr = dsat_dsm_new_buffer(DSM_DS_SMALL_ITEM_POOL, FALSE);
      res_buff_ptr->used = (word)snprintf((char *)res_buff_ptr->data_ptr,
                                              res_buff_ptr->size,
                                             "^SYSCONFIG: %d,%d,%d,%d\n",
                                             in_pref_val.mode_val,
                                             in_pref_val.order_val,
                                             in_pref_val.roam_val,
                                             in_pref_val.domain_val);
      dsatcmdp_handle_async_cmd_rsp(res_buff_ptr, DSAT_COMPLETE_RSP);
      result = DSAT_OK;
    }
      break;
    case DSAT_PENDING_SYSCONFIG_WRITE:
    {
      /* Nothing to do  */
      result = DSAT_OK;
    }
    break;
      default:
      ASSERT(0);
  }

  return result;
}/* dsatvend_process_sysconfig_cmd */

/*===========================================================================
  FUNCTION DSATVEND_EXEC_SYSCONFIG_CMD

  DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes ^SYSCONFIG command

^ SYSCONFIG =  <mode>,<acqorder>,<roam>,<srvdomain>	<CR><LF>OK<CR><LF>
^ SYSCONFIG?
    ^SYSCONFIG:<mode>,<acqorder>,<roam>,<srvdomain><CR><LF><CR><LF>OK<CR><LF>

  <mode> system mode reference:
  2    Automatically select
  13   GSM ONLY
  14   WCDMA ONLY
  15   TDSCDMA ONLY
  16   no change
  <acqorder>: network accessing order reference:
  0    Automatically
  1    GSM first, UTRAN second
  2    UTRAN first, GSM second
  3    No change
  <roam>: roaming support:
  0	not support
  1	can roam
  2   No change
  <srvdomain>: domain configuration:
  0	CS_ONLY
  1	PS_ONLY
  2   CS_PS
  3   ANY
  4   No change

  DEPENDENCIES
  None

  RETURN VALUE
  
  Returns an enum that describes the result of the command execution.
  possible values:
  DSAT_ERROR : if there was any problem in executing the command
  DSAT_ASYNC_CMD : if it is a success.

  SIDE EFFECTS
  None.
===========================================================================*/
dsat_result_enum_type dsatvend_exec_sysconfig_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *tab_entry,      /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type result = DSAT_OK;
  dsat_sysconfig_type in_pref_val;
  ds_at_cm_ph_pref_type ph_pref_type;
  dsat_error_info_s_type err_info;

  err_info.errval= DSAT_ERR_NO_ERROR;
  err_info.arg_num = 0;

  if ( !IS_ETSI_MODE(dsatcmdp_get_current_mode() ))
  {
    DS_AT_MSG0_ERROR("Not applicable in CDMA mode ");
    return DSAT_ERROR;
  }
  if ( (NA|EQ|AR) == tok_ptr->op )
  {
    /* Verify the input parameter range  */

    if ( tok_ptr->args_found != 4 )
    {
      err_info.errval = DSAT_ERR_INVALID_NUM_OF_ARGS;
      goto send_error;
    }
    /*         MODE            */
    if ( dsatutil_atoi( &in_pref_val.mode_val, 
                       tok_ptr->arg[0], 10) != ATOI_OK )
    {
      err_info.errval = DSAT_ERR_ATOI_CONVERSION_FAILURE;
      err_info.arg_num = 0;
      goto send_error;
    }
	
    /*        ACQ_ORDER            */	
    if ( dsatutil_atoi( &in_pref_val.order_val, 
                       tok_ptr->arg[1], 10) != ATOI_OK )
    {
      err_info.errval = DSAT_ERR_ATOI_CONVERSION_FAILURE;
      err_info.arg_num = 1;
      goto send_error;
    }
	
    /*        ROAM_PREF            */	
    if ( dsatutil_atoi( &in_pref_val.roam_val, 
                       tok_ptr->arg[2], 10) != ATOI_OK )
    {
      err_info.errval = DSAT_ERR_ATOI_CONVERSION_FAILURE;
      err_info.arg_num = 2;
      goto send_error;
    }
	
    /*        DOMAIN_PREF            */	
    if ( dsatutil_atoi( &in_pref_val.domain_val, 
                       tok_ptr->arg[3], 10) != ATOI_OK )
    {
      err_info.errval = DSAT_ERR_ATOI_CONVERSION_FAILURE;
      err_info.arg_num = 3;
      goto send_error;
    }
    /* Convert to CM enum types */
    result = vend_convert_ph_pref_type( FALSE,&in_pref_val,&ph_pref_type );
    if(DSAT_OK != result)
    {
      err_info.errval = DSAT_ERR_INVALID_TOKENS;
      goto send_error;
    }
	
    /* Verify if the pref values are correct */
    if ( FALSE == cm_is_valid_mode_pref(ph_pref_type.network_rat_mode_pref))
    {
      DS_AT_MSG0_ERROR("Invalid CM MODE PREF setting");
      (void)dsat_send_cme_error(DSAT_CME_OP_NOT_ALLOWED);
      return DSAT_CMD_ERR_RSP;
    }


    SET_PENDING(DSAT_VENDOR_SYSCONFIG_IDX ,0, DSAT_PENDING_SYSCONFIG_WRITE)
#ifdef FEATURE_DUAL_SIM
    (void) cm_ph_cmd_sys_sel_pref_per_subs(
                                            dsatcmif_ph_cmd_cb_func,
                                            NULL,
                                            dsatcm_client_id,
                                            dsat_qcsimapp_info[dsat_get_qcsimapp_info_idx()].subs_id,
                                            ph_pref_type.network_rat_mode_pref,
                                            CM_PREF_TERM_PERMANENT,
                                            0,
                                            ph_pref_type.acq_order_pref,
                                            CM_BAND_PREF_NO_CHANGE,
                                            CM_PRL_PREF_NO_CHANGE,
                                            ph_pref_type.network_roam_pref,
                                            CM_HYBR_PREF_NO_CHANGE,
                                            ph_pref_type.service_domain_pref,
                                            CM_NETWORK_SEL_MODE_PREF_NO_CHANGE,
                                            NULL 
                                           );
#else
    (void) cm_ph_cmd_sys_sel_pref_new ( dsatcmif_ph_cmd_cb_func,
                                    NULL,
                                    dsatcm_client_id,
                                    ph_pref_type.network_rat_mode_pref,
                                    CM_PREF_TERM_PERMANENT,
                                    0,
                                    ph_pref_type.acq_order_pref,
                                    CM_BAND_PREF_NO_CHANGE,
                                    CM_PRL_PREF_NO_CHANGE,
                                    ph_pref_type.network_roam_pref,
                                    CM_HYBR_PREF_NO_CHANGE,
                                    ph_pref_type.service_domain_pref,
                                    CM_NETWORK_SEL_MODE_PREF_NO_CHANGE,
                                    NULL 
                                  ); 
#endif /* FEATURE_DUAL_SIM */
    result = DSAT_ASYNC_CMD;
  }
  else if ( (NA|QU) == tok_ptr->op )
  {
    SET_PENDING(DSAT_VENDOR_SYSCONFIG_IDX ,0, DSAT_PENDING_SYSCONFIG_READ)
    /* The return value is ignored as both success and failure cases
     * are handled by the call back function */
    (void)cm_ph_cmd_get_ph_info_per_subs (dsatcmif_ph_cmd_cb_func,
                                 NULL,
                                 dsatcm_client_id,
                                 dsat_qcsimapp_info[dsat_get_qcsimapp_val()].subs_id
                                 );
    
    result = DSAT_ASYNC_CMD;

  }
  else
  {
    result = DSAT_ERROR;
  }

  return result;

send_error:
  if( err_info.errval == DSAT_ERR_INVALID_NUM_OF_ARGS ||
      err_info.errval == DSAT_ERR_INVALID_TOKENS)
  {
    DS_AT_MSG1_ERROR("Error:%d",err_info.errval);
  }
  else
  {
    DS_AT_MSG2_ERROR("Error:%d, arg_num:%d",err_info.errval,err_info.arg_num);
  }
  return DSAT_ERROR;

}/* dsatvend_exec_sysconfig_md */
/*===========================================================================

FUNCTION DSATVEND_EXEC_PACSP_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes AT+PACSP command.
  This function returns the current PLMN mode bit setting of EF CSP file.

DEPENDENCIES
  
RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : If the data is read at initalization, result is returned
              immediately. This is another success case. 
SIDE EFFECTS
  None
  
===========================================================================*/
/* ARGSUSED */
dsat_result_enum_type dsatvend_exec_pacsp_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type result = DSAT_ERROR;
  dsatme_mmgsdi_state_ms_info  *me_ms_val = NULL;
  
  DSATUTIL_CMD_GET_BASE_ADDR(DSAT_MMGSDI_MS_VALS,(void **)&me_ms_val,dsat_get_qcsimapp_val());

  /* Only read command is supported */
  if ( tok_ptr->op == (NA|QU) )
  {
    if(me_ms_val->dsatme_plmn_mode_sel_bit != CSP_PLMN_MODE_BIT_UNSET)
    {
      res_buff_ptr->used =
        (word) snprintf((char*)res_buff_ptr->data_ptr,
                              res_buff_ptr->size,
                              "+PACSP%d",
                              me_ms_val->dsatme_plmn_mode_sel_bit);
      result = DSAT_OK;
    }
    else
    {
      DS_AT_MSG0_ERROR("EONS files not read at Init");
    }
  }
  return result;
}/* dsatvend_exec_pacsp_cmd */

/*===========================================================================

FUNCTION DSATVEND_EXEC_QCVOLT_CMD

DESCRIPTION
  This function returns the current main chip voltage in milli-volts.

DEPENDENCIES
  
RETURN VALUE
  Returns an enum that describes the result of the command execution.
  Possible values:
                        
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success for local execution

SIDE EFFECTS
  None
  
===========================================================================*/
/*ARGSUSED*/
dsat_result_enum_type dsatvend_exec_qcvolt_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type result = DSAT_OK;

  /*-------------------------------------------------------
  command with no arguments will return the info string
  ---------------------------------------------------------*/
  if (tok_ptr->op == NA)
  {
    int32 adc_ret = 0;

#ifdef FEATURE_DSAT_EXTENDED_CMD
    adc_ret = vbatt_read_ichg_out_mv();                      
    if( !dsatutil_strcmp_ig_sp_case((const byte *)parse_table->name, (const byte *)"^VOLT") )
      {
        res_buff_ptr->used =
         (word)snprintf((char*)res_buff_ptr->data_ptr, res_buff_ptr->size,
                               "%s: %d", parse_table->name, (int)adc_ret);
      }
      else
#endif/* FEATURE_DSAT_EXTENDED_CMD */
      {
        res_buff_ptr->used =
        (word)snprintf((char*)res_buff_ptr->data_ptr, res_buff_ptr->size,
                               "%d", (int)adc_ret);
      }
  }
  else
  {
     result = DSAT_ERROR;
  }
  return result;
} /* dsatvend_exec_qcvolt_cmd */

/*===========================================================================

FUNCTION DSATVEND_EXEC_QCHWREV_CMD

DESCRIPTION
  This function returns the MSM chip HW part number and revision info.

DEPENDENCIES
  
RETURN VALUE
  Returns an enum that describes the result of the command execution.
  Possible values:
                        
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success for local execution

SIDE EFFECTS
  None
  
===========================================================================*/
/*ARGSUSED*/
dsat_result_enum_type dsatvend_exec_qchwrev_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type result = DSAT_OK;

  /*-------------------------------------------------------
  command with no arguments will return the info string
  ---------------------------------------------------------*/
  if (tok_ptr->op == NA)
  {
    DalDeviceHandle *phChipInfo = NULL;
    DalChipInfoIdType nChipId = DALCHIPINFO_ID_UNKNOWN;
    DalChipInfoVersionType pnVersion = 0;
    
    if (DAL_DeviceAttach(DALDEVICEID_CHIPINFO, &phChipInfo) != DAL_SUCCESS)
    {
      DS_AT_MSG0_ERROR("Could not get DAL Device Handle, bailing out");
      return DSAT_ERROR;
    }
    DalChipInfo_GetChipId( phChipInfo, &nChipId );
    DalChipInfo_GetChipVersion(phChipInfo,  &pnVersion);

#ifdef FEATURE_DSAT_EXTENDED_CMD
    if( !dsatutil_strcmp_ig_sp_case((const byte *)parse_table->name, (const byte *)"^HWVER") )
    {
      res_buff_ptr->used =
        (uint16)snprintf((char*)res_buff_ptr->data_ptr, res_buff_ptr->size,
                          "%s: Part=%d  version=%d.%d",
                            parse_table->name,
                            nChipId,
                           (int)(pnVersion >> 16),(int)(pnVersion & 0xFF));
    }
    else
#endif/* FEATURE_DSAT_EXTENDED_CMD */
    {
      res_buff_ptr->used =
        (uint16)snprintf((char*)res_buff_ptr->data_ptr, res_buff_ptr->size,
                      "Part=%d  version=%d.%d",
                      nChipId,
                      (int)(pnVersion >> 16),(int)(pnVersion & 0xFF));
    }
  }
  else
  {
     result = DSAT_ERROR;
  }
  return result;
} /* dsatvend_exec_qchwrev_cmd */

/*===========================================================================

FUNCTION DSATVEND_EXEC_QCBOOTVER_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes $QCQCBOOTVER command.
  The result is in res_buff_ptr, reporting boot code build info.

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR :    if there was any problem in executing the command
    DSAT_OK :         if it is a success and execution completed.

SIDE EFFECTS
  None

===========================================================================*/
/*ARGSUSED*/
dsat_result_enum_type dsatvend_exec_qcbootver_cmd
(
  dsat_mode_enum_type mode,                /*  AT command mode:            */
  const dsati_cmd_type *parse_table,       /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,       /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr              /*  Place to put response       */
)
{
  dsat_result_enum_type result = DSAT_ERROR;

#ifdef FEATURE_DSAT_GOBI_MAINLINE 
  /* process command */
  if ( tok_ptr->op == NA )
  {
    /* Request boot code build info  */
    char       boot_ver[GOBI_IM_BUILD_ID_LEN];

    gobi_nv_status_e_type nv_status;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    (void) memset(boot_ver, 0, sizeof(boot_ver));
    nv_status = gobi_nv_read(GOBI_NV_ITEM_BOOT_SW_VER, 0, boot_ver, 
                             sizeof(boot_ver));
    
    /* Request boot code build info  */
    status = dsatutil_get_nv_item( (nv_items_enum_type)5252/*NV_BOOT_SW_VER_I*/, &ds_nv_item );
    if ( status == NV_DONE_S )
    {
      /*-----------------------------------------------------------------------
        Store value retrieved from NV.
      -----------------------------------------------------------------------*/
          /* Generate response */
          /* boot_sw_ver need to be added, when NV item is provisioned */
          res_buff_ptr->used = 
            (word)snprintf((char*)res_buff_ptr->data_ptr, 
                                 res_buff_ptr->size,
                                 "%s",
                                 boot_ver);
          result = DSAT_OK;
      }
    }
#endif /* FEATURE_DSAT_GOBI_MAINLINE */

  return result;
} /* dsatvend_exec_qcbootver_cmd */

#ifdef FEATURE_DSAT_GOBI_MAINLINE
/*============================================================================

FUNCTION DSATVEND_FTM_DIAGPKT_FWD 

DESCRIPTION
   Forward (via ONRPC) FTM diagpkt to FTM

DEPENDENCIES
  
RETURN VALUE

SIDE EFFECTS
  None
  
===========================================================================*/
/*ARGSUSED*/
void dsatvend_ftm_diagpkt_fwd
(
   ftm_pkt_type *ftm_pkt, 
   dsatvend_ftm_diagpkt_cb_f_type rsp_cb,
   void         *rsp_arg
)
{
   dsatvend_ftm_diagpkt_rsp_params.rsp_cb  = rsp_cb;
   dsatvend_ftm_diagpkt_rsp_params.rsp_arg = rsp_arg;
   ftm_diagpkt_fwd((char *)ftm_pkt, dsatvend_ftm_diagpkt_fwd_cb);
} /* dsatvend_ftm_diagpkt_fwd */
  
/*============================================================================

FUNCTION DSATVEND_FTM_DIAGPKT_FWD_CB

DESCRIPTION
   Called in ONRPC context for FTM diagpkt response call back.

   This callback schedules DS_AT_FTM_DIAGPKT_RSP_CMD event to DS task
   in order to process the pktdiag message in DS context.

DEPENDENCIES
  
RETURN VALUE

SIDE EFFECTS
  None
  
===========================================================================*/
/*ARGSUSED*/
void dsatvend_ftm_diagpkt_fwd_cb(char *rsp_ftm_pkt)
{
  ftm_pkt_type *ftm_pkt;
  ds_cmd_type  *cmd_buf;
  ftm_pkt = (ftm_pkt_type *)rsp_ftm_pkt;

  /* Schedule callback under DS to process the response from FTM  */
  DS_AT_MSG0_HIGH("Schedule callback under DS to process the response from FTM");

  /* Save FTM packet response - must be processed in callback under DS
  to avoid deadlock if the response callback sends FTM diagpkt */
  cmd_buf = dsat_get_cmd_buf(FALSE);
  cmd_buf->hdr.cmd_id = (ds_cmd_enum_type)DS_AT_FTM_DIAGPKT_RSP_CMD;
  (void) dsatutil_memscpy((void*)&cmd_buf->cmd.ftm_diagpkt,
           sizeof(ftm_pkt_type),(void*)ftm_pkt,sizeof(ftm_pkt_type));       
  ds_put_cmd(cmd_buf);
} /* dsatvend_ftm_diagpkt_fwd_cb */

/*============================================================================

FUNCTION DSATVEND_FTM_DIAGPKT_RSP_HANDLER

DESCRIPTION
   Called in DS context for DS_AT_FTM_DIAGPKT_RSP_CMD event.
   This event is used to execute callbacks to process FTM diag packets.

DEPENDENCIES
  
RETURN VALUE
  Returns an enum that describes the result of the command execution.
  Possible values:
                        
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success for local execution
    DSAT_ASYNC_EVENT : if it a ASYNC event.
SIDE EFFECTS
  None
  
===========================================================================*/
/*ARGSUSED*/
dsat_result_enum_type dsatvend_ftm_diagpkt_rsp_handler
(
  dsat_mode_enum_type  mode,               /* AT command mode            */
  ds_cmd_type         *cmd_buf             /* DS Command pointer         */
)
{

   dsat_result_enum_type result = DSAT_ASYNC_EVENT;
   
   /* If callback registered, execute callback with response diagpkt */
   if ( dsatvend_ftm_diagpkt_rsp_params.rsp_cb )
   {
     dsatvend_ftm_diagpkt_cb_f_type *rsp_cb;
     (void) dsatutil_memscpy((void*)&dsatvend_ftm_diagpkt_rsp_params.ftm_diagpkt,
             sizeof(ftm_pkt_type),(void*)&cmd_buf->cmd.ftm_diagpkt,sizeof(ftm_pkt_type));    
     rsp_cb = dsatvend_ftm_diagpkt_rsp_params.rsp_cb;
     dsatvend_ftm_diagpkt_rsp_params.rsp_cb = NULL;
     return rsp_cb(&dsatvend_ftm_diagpkt_rsp_params.ftm_diagpkt,
                              dsatvend_ftm_diagpkt_rsp_params.rsp_arg);
   }
   
   return result;
} /* dsatvend_ftm_diagpkt_rsp_handler */

/*===========================================================================

FUNCTION DSATVEND_EXEC_QCTEMP_CMD

DESCRIPTION
  This function returns the current temperature reported by MDM thermistor

DEPENDENCIES
  
RETURN VALUE
  Returns an enum that describes the result of the command execution.
  Possible values:
                        
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success for local execution

SIDE EFFECTS
  None
  
===========================================================================*/
/*ARGSUSED*/
dsat_result_enum_type dsatvend_exec_qctemp_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type result = DSAT_OK;
  dsatdvend_proc_qcagc_type proc_qcagc;

  if (tok_ptr->op == NA)
  {
    int32 adc_ret;

    adc_ret = vbatt_read_pmic_temperature();
        res_buff_ptr->used = 
          (uint16)snprintf((char*)res_buff_ptr->data_ptr,
                                 res_buff_ptr->size,
                                 "%d", 
                                   adc_ret);
  }
  else
  {
     result = DSAT_ERROR;
  }

  return result; 
} /* dsatvend_exec_qctemp_cmd */

/*============================================================================

FUNCTION DSATVEND_EXEC_QCAGC_GET_AGC_CB

DESCRIPTION
   FTM response AGC value is converted and displayed.  
   AT command is completed.

DEPENDENCIES
  
RETURN VALUE
  Returns an enum that describes the result of the command execution.
  Possible values:
                        
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success for local execution

SIDE EFFECTS
  None
  
===========================================================================*/
/*ARGSUSED*/
dsat_result_enum_type dsatvend_exec_qcagc_get_agc_cb
(
   ftm_pkt_type *ftm_pkt,
   void *rsp_arg
)
{
   dsatdvend_proc_qcagc_type *proc_qcagc = 
                      (dsatdvend_proc_qcagc_type *)rsp_arg;
   dsm_item_type *res_buff_ptr;
   short agc_val;
   short rssi_val;

   dsati_mode_e_type current_mode = dsatcmdp_get_current_mode();
   
   agc_val = ftm_pkt->rf_params.ftm_rf_factory_data.gen_w;

   /* Convert 10bit 2's complement */
   if (agc_val > 511) 
   {
      agc_val -= 1024;
   }

   if ( IS_ETSI_MODE(current_mode) )
   {
     /* Convert mdsp db value to mdb using equation:
      *     RSSI  = (RXAGC + 512)/AgcUnitsPerDB + MinRSSIofInterest
      *     where AgcUnitsPerDB=12 and MinRSSIofInterest= -106dBm
      */
   rssi_val = (agc_val + 512)/12  - 106;
     /* Use Cal NV items to adjust the RSSI -
      * NV_WCDMA_VGA_GAIN_OFFSET, NV_WCDMA_VGA_GAIN_OFFSET_VS_FREQ 
      * 
      * Cal Adjustment Val = -(_OFFSET + _VS_FREQ) / 12
      * 
      * Adjusted RSSI = measured RSSI + Cal Adjustment
      */ 
     rssi_val -= ((proc_qcagc->sys_mode.etsi.vga_gain_offset + proc_qcagc->sys_mode.etsi.vga_vs_chan_freq)/12);
   }
   else if ( IS_CDMA_MODE(current_mode) )
   {
   /* Convert mdsp db value to mDb using equation:
      RSSI  = (RXAGC + 512)/AgcUnitsPerDB + MinRSSIofInterest */
     rssi_val = (agc_val + 512)/proc_qcagc->sys_mode.cdma.rx_agc_range + 
                  proc_qcagc->sys_mode.cdma.rx_min_rssi;
   }
   else
   {
     return DSAT_ERROR;
   }

  res_buff_ptr = dsat_dsm_new_buffer(DSM_DS_LARGE_ITEM_POOL, FALSE);
  res_buff_ptr->used = snprintf((char*)res_buff_ptr->data_ptr,
                                     res_buff_ptr->size, "RSSI: %d",
                                     rssi_val );

  dsatcmdp_handle_async_cmd_rsp(res_buff_ptr, DSAT_COMPLETE_RSP);

  SET_PENDING(DSAT_VENDOR_QCAGC_IDX ,0, DSAT_PENDING_QCAGC_NONE)
  return DSAT_OK;
} /* dsatvend_exec_qcagc_get_agc_cb */

/*============================================================================

FUNCTION DSATVEND_EXEC_QCAGC_SET_LNA_CB

DESCRIPTION
   Received FTM command FTM_SET_LNA_RANGE response.
   Send FTM FTM_GET_CAGC_RX_AGC command on specified path

DEPENDENCIES
  
RETURN VALUE
  Returns an enum that describes the result of the command execution.
  Possible values:
                        
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success for local execution
    DSAT_ASYNC_EVENT : if its a ASYNC EVENT
SIDE EFFECTS
  None
  
===========================================================================*/
/*ARGSUSED*/
dsat_result_enum_type dsatvend_exec_qcagc_set_lna_cb
(
   ftm_pkt_type *ftm_pkt,
   void *rsp_arg
)
{
   dsatdvend_proc_qcagc_type *proc_qcagc = 
                      (dsatdvend_proc_qcagc_type *)rsp_arg;
   
   ftm_diagpkt_subsys_header_type *ftm_diag_hdr;

   /* Get AGC */
   ftm_diag_hdr = (ftm_diagpkt_subsys_header_type *)&ftm_pkt->header;
   ftm_diag_hdr->subsys_cmd_code = (diagpkt_subsys_cmd_code_type)
            (proc_qcagc->enable_rx_diversity ?
          proc_qcagc->ftm_dispatch_div_id : proc_qcagc->ftm_dispatch_id);

   ftm_pkt->rf_params.ftm_rf_cmd  = (word)FTM_GET_CAGC_RX_AGC;

   dsatvend_ftm_diagpkt_fwd(ftm_pkt, 
                            dsatvend_exec_qcagc_get_agc_cb,
                            (void *)proc_qcagc);
    
   SET_PENDING(DSAT_VENDOR_QCAGC_IDX ,0, DSAT_PENDING_QCAGC_GET_AGC)
   return DSAT_ASYNC_EVENT;
} /* dsatvend_exec_qcagc_set_lna_cb */

/*============================================================================

FUNCTION DSATVEND_EXEC_QCAGC_SET_SECONDARY_CHAIN_CB

DESCRIPTION
   Process received FTM_SET_SECONDARY_CHAIN response.
   Send FTM_SET_LNA_RANGE command on diversity path

DEPENDENCIES
  
RETURN VALUE
  Returns an enum that describes the result of the command execution.
  Possible values:
                        
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success for local execution
    DSAT_ASYNC_EVENT : if it is a ASYNC EVENT
SIDE EFFECTS
  None
  
===========================================================================*/
/*ARGSUSED*/
dsat_result_enum_type dsatvend_exec_qcagc_set_secondary_chain_cb
(
   ftm_pkt_type *ftm_pkt,
   void *rsp_arg
)
{
   dsatdvend_proc_qcagc_type *proc_qcagc = 
                      (dsatdvend_proc_qcagc_type *)rsp_arg;
   
   ftm_diagpkt_subsys_header_type *ftm_diag_hdr;

   /* Send LNA selection on diversity path */
   ftm_diag_hdr = (ftm_diagpkt_subsys_header_type *)&ftm_pkt->header;
   ftm_diag_hdr->subsys_cmd_code = (diagpkt_subsys_cmd_code_type)
                                    proc_qcagc->ftm_dispatch_div_id;

   ftm_pkt->rf_params.ftm_rf_cmd  = (word)FTM_SET_LNA_RANGE;
   ftm_pkt->rf_params.ftm_rf_factory_data.range = 
                             (ftm_pa_range_type)proc_qcagc->lna;

   dsatvend_ftm_diagpkt_fwd(ftm_pkt, 
                             dsatvend_exec_qcagc_set_lna_cb,
                            (void *)proc_qcagc);
  
   SET_PENDING(DSAT_VENDOR_QCAGC_IDX ,0, DSAT_PENDING_QCAGC_SET_LNA)
   return DSAT_ASYNC_EVENT;
} /* dsatvend_exec_qcagc_set_secondary_chain_cb */


/*============================================================================

FUNCTION DSATVEND_EXEC_QCAGC_GET_SYNTH_CB

DESCRIPTION
   Process received FTM_GET_SYNTH_STATE response.
   If RX diversity path
      Send FTM_SET_SECONDARY_CHAIN command.
   Else
      Send FTM_SET_LNA_RANGE command.

DEPENDENCIES
  
RETURN VALUE
  Returns an enum that describes the result of the command execution.
  Possible values:
                        
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success for local execution
    DSAT_ASYNC_EVENT : if it is a ASYNC EVENT
    DSAT_CMD_ERR_RSP : if sending out error_response

SIDE EFFECTS
  None
  
===========================================================================*/
/*ARGSUSED*/
dsat_result_enum_type dsatvend_exec_qcagc_get_synth_cb
(
   ftm_pkt_type *ftm_pkt,
   void *rsp_arg
)
{
   dsatdvend_proc_qcagc_type *proc_qcagc = 
                      (dsatdvend_proc_qcagc_type *)rsp_arg;
   
   ftm_diagpkt_subsys_header_type *ftm_diag_hdr;

   /* Verify Lock state in synth */
   if (!ftm_pkt->rf_params.ftm_rf_factory_data.on_off) 
   {
      dsm_item_type *res_buff_ptr;
      res_buff_ptr = dsat_dsm_new_buffer(DSM_DS_LARGE_ITEM_POOL, FALSE);
      res_buff_ptr->used = (word)snprintf((char*)res_buff_ptr->data_ptr,
                                                res_buff_ptr->size,
                                                "AGC:Synth not locked");
      dsatcmdp_handle_async_cmd_rsp(res_buff_ptr, DSAT_COMPLETE_RSP);

      SET_PENDING(DSAT_VENDOR_QCAGC_IDX ,0, DSAT_PENDING_QCAGC_NONE)
      return DSAT_CMD_ERR_RSP;
   }

   /* If RX diversity path is used, enable path via FTM_SET_SECONDARY_CHAIN */
   if (proc_qcagc->enable_rx_diversity)
   {
      ftm_diag_hdr = (ftm_diagpkt_subsys_header_type *)&ftm_pkt->header;
      ftm_diag_hdr->subsys_cmd_code = (diagpkt_subsys_cmd_code_type)
                                       proc_qcagc->ftm_dispatch_div_id;

      ftm_pkt->rf_params.ftm_rf_cmd  = (word)FTM_SET_SECONDARY_CHAIN;
      ftm_pkt->rf_params.ftm_rf_factory_data.on_off = 1;

      dsatvend_ftm_diagpkt_fwd(ftm_pkt, 
                               dsatvend_exec_qcagc_set_secondary_chain_cb,
                               (void *)proc_qcagc);

      SET_PENDING(DSAT_VENDOR_QCAGC_IDX ,0, DSAT_PENDING_QCAGC_SET_SEC_CHAIN)
      return DSAT_ASYNC_EVENT;
   }

   /* Else send LNA selection */
   ftm_diag_hdr = (ftm_diagpkt_subsys_header_type *)&ftm_pkt->header;
   ftm_diag_hdr->subsys_cmd_code = (diagpkt_subsys_cmd_code_type)
                                    proc_qcagc->ftm_dispatch_id;

   ftm_pkt->rf_params.ftm_rf_cmd  = (word)FTM_SET_LNA_RANGE;
   ftm_pkt->rf_params.ftm_rf_factory_data.range = 
                             (ftm_pa_range_type)proc_qcagc->lna;

   dsatvend_ftm_diagpkt_fwd(ftm_pkt, 
                             dsatvend_exec_qcagc_set_lna_cb,
                             (void *)proc_qcagc);
  
   SET_PENDING(DSAT_VENDOR_QCAGC_IDX ,0, DSAT_PENDING_QCAGC_SET_LNA)
   return DSAT_ASYNC_EVENT;
} /* dsatvend_exec_qcagc_get_synth_cb */

/*============================================================================

FUNCTION DSATVEND_EXEC_QCAGC_SET_CHAN_CB

DESCRIPTION
   Process received FTM_SET_CHAN response.
   Send FTM_GET_SYNTH_STATE command.

DEPENDENCIES
  
RETURN VALUE
  Returns an enum that describes the result of the command execution.
  Possible values:
                        
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success for local execution
    DSAT_ASYNC_EVENT : if it is a ASYNC EVENT

SIDE EFFECTS
  None
  
===========================================================================*/
/*ARGSUSED*/
dsat_result_enum_type dsatvend_exec_qcagc_set_chan_cb
(
   ftm_pkt_type *ftm_pkt,
   void *rsp_arg
)
{
   dsatdvend_proc_qcagc_type *proc_qcagc = 
                      (dsatdvend_proc_qcagc_type *)rsp_arg;
   
   ftm_diagpkt_subsys_header_type *ftm_diag_hdr;

   /* Send Get SYNTH state */
   ftm_diag_hdr = (ftm_diagpkt_subsys_header_type *)&ftm_pkt->header;
   ftm_diag_hdr->subsys_cmd_code = (diagpkt_subsys_cmd_code_type)
                                    proc_qcagc->ftm_dispatch_id;
 
   ftm_pkt->rf_params.ftm_rf_cmd  = (word)FTM_GET_SYNTH_STATE;
 
   dsatvend_ftm_diagpkt_fwd(ftm_pkt, 
                             dsatvend_exec_qcagc_get_synth_cb,
                             (void *)proc_qcagc);
  
   SET_PENDING(DSAT_VENDOR_QCAGC_IDX ,0, DSAT_PENDING_QCAGC_GET_SYNTH)
   return DSAT_ASYNC_EVENT; 
} /* dsatvend_exec_qcagc_set_chan_cb */


/*============================================================================

FUNCTION DSATVEND_EXEC_QCAGC_SET_MODE_CB

DESCRIPTION
   Process received FTM_SET_MODE response.
   Send FTM_SET_CHAN command.

DEPENDENCIES
  
RETURN VALUE
  Returns an enum that describes the result of the command execution.
  Possible values:
                        
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success for local execution
    DSAT_ASYNC_EVENT : if it is a ASYNC EVENT

SIDE EFFECTS
  None
  
===========================================================================*/
/*ARGSUSED*/
dsat_result_enum_type dsatvend_exec_qcagc_set_mode_cb
(
   ftm_pkt_type *ftm_pkt,
   void *rsp_arg
)
{
   dsatdvend_proc_qcagc_type *proc_qcagc = 
                      (dsatdvend_proc_qcagc_type *)rsp_arg;
   ftm_diagpkt_subsys_header_type *ftm_diag_hdr;

   /* Send channel selection */
   ftm_diag_hdr = (ftm_diagpkt_subsys_header_type *)&ftm_pkt->header;
   ftm_diag_hdr->subsys_cmd_code = (diagpkt_subsys_cmd_code_type)
                                    proc_qcagc->ftm_dispatch_id;

   ftm_pkt->rf_params.ftm_rf_cmd  = (word)FTM_SET_CHAN;
   ftm_pkt->rf_params.ftm_rf_factory_data.chan = proc_qcagc->sys_mode.cdma.chan;

   dsatvend_ftm_diagpkt_fwd(ftm_pkt, 
                            dsatvend_exec_qcagc_set_chan_cb,
                            (void *)proc_qcagc);
   
   
   SET_PENDING(DSAT_VENDOR_QCAGC_IDX ,0, DSAT_PENDING_QCAGC_SET_CHAN)
   
   return DSAT_ASYNC_EVENT;
} /* dsatvend_exec_qcagc_set_mode_cb */

/*============================================================================

FUNCTION DSATVEND_EXEC_QCAGC_CMD

DESCRIPTION
  Get current AGC value from specified band/channel
   
  AT$QCAGC <band>,<rx_chan>[,<path>]
     band
     chan
     path  ["main"] "aux"

   band -
      CDMA_800    (Cell)
      CDMA_1900   (PCS)
   
  Note: Restricted to FTM
        WCDMA version implemented (below)
  
DEPENDENCIES
  
RETURN VALUE
  Returns an enum that describes the result of the command execution.
  Possible values:
                        
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success for local execution
    DSAT_ASYNC_EVENT : if it is a ASYNC EVENT

SIDE EFFECTS
  None
  
===========================================================================*/
/*ARGSUSED*/
dsat_result_enum_type dsatvend_exec_qcagc_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type result = DSAT_OK;
  dsati_mode_e_type oper_mode = dsatcmdp_get_current_mode();

#define  CHAN_LIST_RANGE_VAL -1 /* denotes inclusive range pair */
#define  CHAN_LIST_EOL_VAL   -2 /* end of list */
#define  CHAN_LIST_ENTRIES_MAX 20  /* maximum entries in the channel-list */

  if ( IS_CDMA_MODE (oper_mode) )
  {     
     static dsatdvend_proc_qcagc_type proc_qcagc;
     
     typedef struct band_label_s {
        int              ftm_dispatch_main_id;
        int              ftm_dispatch_div_id;
        char            *name;
        ftm_mode_id_type mode_band;
        int              chan_list[CHAN_LIST_ENTRIES_MAX];
     } band_label_type;
  
     band_label_type *band_label;
     
     /* Supported Mode/Band labels with associated FTM and NV defs */
     band_label_type agc_band_labels[] = {
        { FTM_1X_C, FTM_1X_RX_2_C,           "\"CDMA_800\"",  
                       FTM_PHONE_MODE_CDMA_800,
                       {   1, CHAN_LIST_RANGE_VAL,  799,
                         991, CHAN_LIST_RANGE_VAL, 1023,
                         CHAN_LIST_EOL_VAL} },
        
        { FTM_1X_C, FTM_1X_RX_2_C,           "\"CDMA_1900\"",  
                        FTM_PHONE_MODE_CDMA_1900,
                       { 0, CHAN_LIST_RANGE_VAL , 1199,
                         CHAN_LIST_EOL_VAL} },
       };
       
     /* If arguments passed... */ 
     if (tok_ptr->op == (NA|EQ|AR) ) 
     {
        int i;
        int chan;        
        
        if ((tok_ptr->args_found < 1) || (tok_ptr->args_found > 3)) 
        {
           return DSAT_ERROR;
        }
        
        if (ftm_get_mode() != FTM_MODE) 
        {
          res_buff_ptr->used =
            (uint16)snprintf((char*)res_buff_ptr->data_ptr, res_buff_ptr->size,
                                "ERROR - restricted to FTM");
           return DSAT_CMD_ERR_RSP;  
        }
  
        /* Get and validate mode/band arg */
        band_label = NULL;
        for (i = 0; (!band_label) && (i < (int)ARR_SIZE(agc_band_labels)); i++)
        {
           if (!dsatutil_strcmp_ig_sp_case((const byte *)agc_band_labels[i].name, 
                                           tok_ptr->arg[0])) 
           {
              band_label = &agc_band_labels[i];
           }
        }
        if (!band_label) 
        {
          res_buff_ptr->used = 
            (uint16)snprintf((char*)res_buff_ptr->data_ptr, res_buff_ptr->size,
                                            "ERROR: Invalid <band>\n");
          return DSAT_CMD_ERR_RSP;
        }
        proc_qcagc.mode_band  = band_label->mode_band;
  
        /* Get chan and validate */
        if (dsatutil_atoi((dsat_num_item_type *)&chan, tok_ptr->arg[1], 10)) 
        {
           return DSAT_ERROR;
        }
  
        /* Verify chan value is in list */
        for (i = 0; (CHAN_LIST_EOL_VAL != band_label->chan_list[i]) 
              && (i < (CHAN_LIST_ENTRIES_MAX-2)); i++)
        {
           if ((int)chan >= band_label->chan_list[i])
           {
              if (chan == band_label->chan_list[i])
              {
                 break;
              }
              if ((CHAN_LIST_EOL_VAL   != band_label->chan_list[i+2]) &&
                  (CHAN_LIST_RANGE_VAL == band_label->chan_list[i+1]) &&
                  ((int)chan <= band_label->chan_list[i+2])) 
              {
                 break;
              }
           }
        }
        if (CHAN_LIST_EOL_VAL == band_label->chan_list[i])
        {
          res_buff_ptr->used = 
            (uint16)snprintf((char*)res_buff_ptr->data_ptr, res_buff_ptr->size,
                                                "ERROR: Invalid <chan>\n");
          return DSAT_CMD_ERR_RSP;
        }

        proc_qcagc.sys_mode.cdma.chan = chan;

        /* If path specified, get and validate */
        proc_qcagc.ftm_dispatch_id     = band_label->ftm_dispatch_main_id;
        proc_qcagc.ftm_dispatch_div_id = band_label->ftm_dispatch_div_id;
        proc_qcagc.enable_rx_diversity = 0;
        
        if (tok_ptr->args_found == 3)
        {
           if (!dsatutil_strcmp_ig_sp_case("\"aux\"", tok_ptr->arg[2]))
           {
              proc_qcagc.enable_rx_diversity = 1;
           } 
           else if (dsatutil_strcmp_ig_sp_case("\"main\"", tok_ptr->arg[2]))
           {
             res_buff_ptr->used = 
               (uint16)snprintf((char*)res_buff_ptr->data_ptr, res_buff_ptr->size,
                                                   "ERROR: Invalid <path>\n");
             return DSAT_CMD_ERR_RSP;
           }
        }
        
        proc_qcagc.lna = 0;
  
        /* Start FTM command sequence to get AGC via commands -
         *    Get NV Cal values for specific band
         *    FTM_SET_MODE
         *    FTM_SET_CHAN
         *    FTM_GET_SYNTH_STATE
         *    FTM_SET_LNA_RANGE
         *    FTM_GET_CAGC_RX_AGC
         */
        {
           ftm_pkt_type ftm_pkt;
           ftm_diagpkt_subsys_header_type *ftm_diag_hdr;
           nv_stat_enum_type nv_status;
           
           /* Default RSSI measurement variables */
           proc_qcagc.sys_mode.cdma.rx_agc_range   = 10;
           proc_qcagc.sys_mode.cdma.rx_min_rssi    = -115;
           
           /* Adjust RSSI measurement per NV Cal values if present */
           nv_status = dsatutil_get_nv_item(NV_CDMA_DYNAMIC_RANGE_I, 
                                            &ds_nv_item);
           if (nv_status == NV_DONE_S) 
           {
              proc_qcagc.sys_mode.cdma.rx_agc_range = ds_nv_item.cdma_dynamic_range;
           }
           
           nv_status = dsatutil_get_nv_item(NV_CDMA_MIN_RX_RSSI_I,
                                            &ds_nv_item);
           if (nv_status == NV_DONE_S) 
           {
              proc_qcagc.sys_mode.cdma.rx_min_rssi = ds_nv_item.cdma_min_rx_rssi;
           }
  
           /* Send FTM Mode / band selection */
           ftm_diag_hdr = (ftm_diagpkt_subsys_header_type *)&ftm_pkt.header;
           ftm_diag_hdr->subsys_cmd_code = (diagpkt_subsys_cmd_code_type)
                                            proc_qcagc.ftm_dispatch_id;
           ftm_pkt.rf_params.ftm_rf_cmd  = (word)FTM_SET_MODE;
           ftm_pkt.rf_params.ftm_rf_factory_data.mode = 
                                             proc_qcagc.mode_band;
           
           dsatvend_ftm_diagpkt_fwd(&ftm_pkt, 
                                    dsatvend_exec_qcagc_set_mode_cb,
                                    (void *)&proc_qcagc);

           SET_PENDING(DSAT_VENDOR_QCAGC_IDX ,0, DSAT_PENDING_QCAGC_SET_MODE)
           result = DSAT_ASYNC_CMD;
        }
     }
     else if (tok_ptr->op == (NA|EQ|QU) )
     {
        uint16 i, j, buf_size;
        buf_size =  res_buff_ptr->size;
        res_buff_ptr->used = 
          (uint16)snprintf((char*)res_buff_ptr->data_ptr, buf_size,
                                              "AGC: <band>,<rx_chan>[,<path>]\n");
  
        for (i = 0; i < (int)ARR_SIZE(agc_band_labels); i++)
        {
           band_label =  &agc_band_labels[i];
           res_buff_ptr->used += 
             (uint16)snprintf((char*)res_buff_ptr->data_ptr + res_buff_ptr->used, 
                                              buf_size - res_buff_ptr->used,
                                              "  <band>,<rx_chan> %s,(", band_label->name);
           
           for (j = 0; (CHAN_LIST_EOL_VAL != band_label->chan_list[j]) 
                 && (j < CHAN_LIST_ENTRIES_MAX); j++)
           {
              char str_buf[24];
              if (CHAN_LIST_RANGE_VAL == band_label->chan_list[j])
              {
                 (void)snprintf(str_buf, sizeof(str_buf), "-");
              }
              else 
              {
                 if (!j || (CHAN_LIST_RANGE_VAL == band_label->chan_list[j-1]))
                 {
                    (void)snprintf(str_buf, sizeof(str_buf), "%d",
                                   band_label->chan_list[j]);
                 }
                 else
                 {
                    (void)snprintf(str_buf, sizeof(str_buf), ",%d",
                                   band_label->chan_list[j]);
                 }
              }
              res_buff_ptr->used += 
                (uint16)snprintf((char*)res_buff_ptr->data_ptr + res_buff_ptr->used,
                                   buf_size - res_buff_ptr->used,
                                   "%s", str_buf);
              
           } /* End for (j = 0; CHAN_LIST_EOL_VAL */
           
           res_buff_ptr->used += 
             (uint16)snprintf((char*)res_buff_ptr->data_ptr + res_buff_ptr->used,
                               buf_size - res_buff_ptr->used,
                               ")\n");
        } /* End for (i = 0; i < ARR_SIZE(agc_band_labels) */
        
        res_buff_ptr->used += 
          (uint16)snprintf((char*)res_buff_ptr->data_ptr + res_buff_ptr->used, 
                            buf_size - res_buff_ptr->used,
                            "  <path> (\"MAIN\",\"AUX\")\n");
        return DSAT_OK;
        
     } /* End else if (tok_ptr->op == (NA|EQ|QU) ) */
     else
     {
       result = DSAT_ERROR;
     }
   }
   else if ( IS_ETSI_MODE(oper_mode))
   {        
     static dsatdvend_proc_qcagc_type proc_qcagc;
        
     typedef struct band_nv_cal_s {
       nv_items_enum_type  cal_rx_chan;        
       nv_items_enum_type  vga_gain_offset;
       nv_items_enum_type  vga_vs_freq;
     } band_nv_cal_type;
        
     typedef struct band_label_s {
       int              ftm_dispatch_main_id;
       int              ftm_dispatch_div_id;
       char            *name;
       ftm_mode_id_type mode_band;
       int              rx_tx_chan_offset; /* Offset from Rx to Tx */
       band_nv_cal_type cal_id_main;       /* Cal NV items for main path */
       band_nv_cal_type cal_id_div;        /* Cal NV items for diversity path */
       int     chan_list[CHAN_LIST_ENTRIES_MAX];
     } band_label_type;
     
     band_label_type *band_label;
        
     /* Supported Mode/Band labels with associated FTM and NV defs */
     band_label_type agc_band_labels[] = {
       { FTM_WCDMA_C, FTM_WCDMA_RX_2_C,           "\"WCDMA_IMT\"",  
         FTM_PHONE_MODE_WCDMA_IMT,
         BSP_RF_WCDMA_BC1_RX_TX_CHAN_NUMBER_OFFSET,
         { NV_WCDMA_RX_CAL_CHAN_I ,
           NV_WCDMA_VGA_GAIN_OFFSET_I, 
           NV_WCDMA_VGA_GAIN_OFFSET_VS_FREQ_I },
         { NV_C1_WCDMA_2100_RX_CAL_CHAN_I ,
           NV_C1_WCDMA_2100_VGA_GAIN_OFFSET_I, 
           NV_C1_WCDMA_2100_VGA_GAIN_OFFSET_VS_FREQ_I },
         { BSP_RF_RX_CHAN_WCDMA_BC1_MIN, CHAN_LIST_RANGE_VAL, 
           BSP_RF_RX_CHAN_WCDMA_BC1_MAX, CHAN_LIST_EOL_VAL} },
       
       { FTM_WCDMA_C, FTM_WCDMA_RX_2_C,           "\"WCDMA_800\"",  
         FTM_PHONE_MODE_WCDMA_800 ,
         BSP_RF_WCDMA_BC5_RX_TX_CHAN_NUMBER_OFFSET,
         { NV_WCDMA_800_RX_CAL_CHAN_I ,
           NV_WCDMA_800_VGA_GAIN_OFFSET_I, 
           NV_WCDMA_800_VGA_GAIN_OFFSET_VS_FREQ_I },
         { NV_C1_WCDMA_800_RX_CAL_CHAN_I ,
           NV_C1_WCDMA_800_VGA_GAIN_OFFSET_I, 
           NV_C1_WCDMA_800_VGA_GAIN_OFFSET_VS_FREQ_I },
         { BSP_RF_RX_CHAN_WCDMA_BC5_MIN, CHAN_LIST_RANGE_VAL , 
           BSP_RF_RX_CHAN_WCDMA_BC5_MAX, CHAN_LIST_EOL_VAL} },
       
      { FTM_WCDMA_C, FTM_WCDMA_RX_2_C,           "\"WCDMA_900\"",  
                     FTM_PHONE_MODE_WCDMA_BC8 ,
                     BSP_RF_WCDMA_BC8_RX_TX_CHAN_NUMBER_OFFSET,
                     { NV_WCDMA_900_RX_CAL_CHAN_I, 
                       NV_WCDMA_900_VGA_GAIN_OFFSET_I, 
                       NV_WCDMA_900_VGA_GAIN_OFFSET_VS_FREQ_I },
                     { NV_C1_WCDMA_900_RX_CAL_CHAN_I ,
                       NV_C1_WCDMA_900_VGA_GAIN_OFFSET_I, 
                       NV_C1_WCDMA_900_VGA_GAIN_OFFSET_VS_FREQ_I },
                     { BSP_RF_RX_CHAN_WCDMA_BC8_MIN, CHAN_LIST_RANGE_VAL , 
                       BSP_RF_RX_CHAN_WCDMA_BC8_MAX, CHAN_LIST_EOL_VAL} },
      
       { FTM_WCDMA_C, FTM_WCDMA_RX_2_C,           "\"WCDMA_1900\"", 
         FTM_PHONE_MODE_WCDMA_1900A, 
         BSP_RF_WCDMA_BC2_RX_TX_CHAN_NUMBER_OFFSET,
         { NV_WCDMA_1900_RX_CAL_CHAN_I ,
           NV_WCDMA_1900_VGA_GAIN_OFFSET_I, 
           NV_WCDMA_1900_VGA_GAIN_OFFSET_VS_FREQ_I },
         { NV_C1_WCDMA_1900_RX_CAL_CHAN_I ,
           NV_C1_WCDMA_1900_VGA_GAIN_OFFSET_I, 
           NV_C1_WCDMA_1900_VGA_GAIN_OFFSET_VS_FREQ_I },
         { BSP_RF_RX_CHAN_WCDMA_BC2_MIN, CHAN_LIST_RANGE_VAL, 
           BSP_RF_RX_CHAN_WCDMA_BC2_MAX, CHAN_LIST_EOL_VAL } }
     };
          
      /* If arguments passed */ 
     if (tok_ptr->op == (NA|EQ|AR) )
     {
       int i;
       int chan;        
       
       if ((tok_ptr->args_found < 1) || (tok_ptr->args_found > 3))
       {
         return DSAT_ERROR;
       }
           
       if (ftm_get_mode() != FTM_MODE)
       {
         res_buff_ptr->used =
           (uint16)snprintf ((char*)res_buff_ptr->data_ptr, res_buff_ptr->size,
                               "ERROR - restricted to FTM");
         return DSAT_CMD_ERR_RSP;  
       }
     
       /* Get and validate mode/band arg */
       band_label = NULL;
       for (i = 0; (!band_label) && (i < (int)ARR_SIZE(agc_band_labels)); i++)
       {
         if (!dsatutil_strcmp_ig_sp_case((const byte *)agc_band_labels[i].name, 
                                         tok_ptr->arg[0])) 
         {
           band_label = (band_label_type *)&agc_band_labels[i];
         }
       }
       if (!band_label)
       {
         res_buff_ptr->used = 
           (uint16)snprintf((char*)res_buff_ptr->data_ptr, res_buff_ptr->size,
                                               "ERROR: Invalid <band>\n");
         return DSAT_CMD_ERR_RSP;
       }
       proc_qcagc.mode_band  = band_label->mode_band;
     
       /* Get chan and validate */
       if (dsatutil_atoi((dsat_num_item_type *)&chan, tok_ptr->arg[1], 10))
       {
         return DSAT_ERROR;
       }
     
       /* Verify chan value is in list */
       for (i = 0; (CHAN_LIST_EOL_VAL != band_label->chan_list[i])
             && (i < (CHAN_LIST_ENTRIES_MAX-2)); i++)
       {
         if ((int)chan >= band_label->chan_list[i])
         {
           if (chan == band_label->chan_list[i])
           {
             break;
           }
           if ((CHAN_LIST_EOL_VAL   != band_label->chan_list[i+2]) &&
               (CHAN_LIST_RANGE_VAL == band_label->chan_list[i+1]) &&
               ((int)chan <= band_label->chan_list[i+2]))
           {
             break;
           }
         }
       }
       if (CHAN_LIST_EOL_VAL == band_label->chan_list[i])
       {
         res_buff_ptr->used = 
           (uint16)snprintf((char*)res_buff_ptr->data_ptr, res_buff_ptr->size,
                                               "ERROR: Invalid <chan>\n");
         return DSAT_CMD_ERR_RSP;
       }

       proc_qcagc.sys_mode.etsi.rx_chan = chan;
       proc_qcagc.sys_mode.etsi.tx_chan = proc_qcagc.sys_mode.etsi.rx_chan -
         band_label->rx_tx_chan_offset;
       
       /* If path specified, get and validate */
       proc_qcagc.ftm_dispatch_id     = band_label->ftm_dispatch_main_id;
       proc_qcagc.ftm_dispatch_div_id = band_label->ftm_dispatch_div_id;
       proc_qcagc.enable_rx_diversity = 0;
       
       if (tok_ptr->args_found == 3)
       {
         if (!dsatutil_strcmp_ig_sp_case("\"aux\"", tok_ptr->arg[2]))
         {
           proc_qcagc.enable_rx_diversity = 1;
         } 
         else if (dsatutil_strcmp_ig_sp_case("\"main\"", tok_ptr->arg[2]))
         {
           res_buff_ptr->used = 
             (uint16)snprintf((char*)res_buff_ptr->data_ptr, res_buff_ptr->size,
                                                 "ERROR: Invalid <path>\n");
           return DSAT_CMD_ERR_RSP;
         }
       }
           
       proc_qcagc.lna = 0;
       
       /* Start FTM command sequence to get AGC via commands -
        *    Get NV Cal values for specific band
        *    FTM_SET_MODE
        *    FTM_SET_CHAN
        *    FTM_GET_SYNTH_STATE
        *    FTM_SET_LNA_RANGE
        *    FTM_GET_CAGC_RX_AGC
        */
       {
         ftm_pkt_type ftm_pkt;
         ftm_diagpkt_subsys_header_type *ftm_diag_hdr;
         nv_stat_enum_type nv_status;
         int missing_calibration = 0;
         int16 rx_cal_chan[NV_FREQ_TABLE_SIZ];
         
     
         /* Get NV Cal values for specified band / path */
         proc_qcagc.sys_mode.etsi.vga_gain_offset = 0;
         proc_qcagc.sys_mode.etsi.vga_vs_chan_freq = 0;
         missing_calibration = FALSE;
              
         nv_status = dsatutil_get_nv_item(
           proc_qcagc.enable_rx_diversity ?
           band_label->cal_id_div.cal_rx_chan :
           band_label->cal_id_main.cal_rx_chan, &ds_nv_item);
         if (nv_status == NV_DONE_S)
         {
         (void) dsatutil_memscpy((void*)rx_cal_chan,
           NV_FREQ_TABLE_SIZ,(void*)ds_nv_item.wcdma_rx_cal_chan,
                  sizeof(rx_cal_chan));
         }
         else
         {
           missing_calibration = TRUE;
         }
         
         nv_status = dsatutil_get_nv_item(
           proc_qcagc.enable_rx_diversity ?
           band_label->cal_id_div.vga_gain_offset :
           band_label->cal_id_main.vga_gain_offset, &ds_nv_item);
         if (nv_status == NV_DONE_S)
         {
           proc_qcagc.sys_mode.etsi.vga_gain_offset = ds_nv_item.wcdma_vga_gain_offset;
         }
         else
         {
           missing_calibration = TRUE;
         }
         
         /* Get the VGA offset based on channel frequency */
         nv_status = dsatutil_get_nv_item(
           proc_qcagc.enable_rx_diversity ?
           band_label->cal_id_div.vga_vs_freq :
           band_label->cal_id_main.vga_vs_freq, &ds_nv_item);
         if ((nv_status == NV_DONE_S) && !missing_calibration)
         {
           int16 *nv_chan_freq = &rx_cal_chan[0];
           for (i = 0; i < NV_FREQ_TABLE_SIZ; i++)
           {
             if (proc_qcagc.sys_mode.etsi.rx_chan <= nv_chan_freq[i])
             { 
               break;
             }
           }
           if (i == NV_FREQ_TABLE_SIZ)
           {
             i = NV_FREQ_TABLE_SIZ - 1;
           }
           proc_qcagc.sys_mode.etsi.vga_vs_chan_freq = 
             ds_nv_item.wcdma_vga_gain_offset_vs_freq[i];
         }
         else
         {
           missing_calibration = TRUE;
         }
         
         if (missing_calibration)
         {
           res_buff_ptr->used =
             (uint16)snprintf((char*)res_buff_ptr->data_ptr, res_buff_ptr->size,
                                 "ERROR - Missing NV calibration values");
           return DSAT_CMD_ERR_RSP;  
         }
         
         /* Send FTM Mode / band selection */
         ftm_diag_hdr = (ftm_diagpkt_subsys_header_type *)&ftm_pkt.header;
         ftm_diag_hdr->subsys_cmd_code = (diagpkt_subsys_cmd_code_type)
         proc_qcagc.ftm_dispatch_id;
         ftm_pkt.rf_params.ftm_rf_cmd  = (word)FTM_SET_MODE;
         ftm_pkt.rf_params.ftm_rf_factory_data.mode = 
           proc_qcagc.mode_band;
         
         dsatvend_ftm_diagpkt_fwd(&ftm_pkt, 
                                  dsatvend_exec_qcagc_set_mode_cb,
                                  (void *)&proc_qcagc);

         SET_PENDING(DSAT_VENDOR_QCAGC_IDX ,0, DSAT_PENDING_QCAGC_SET_MODE)
         result = DSAT_ASYNC_CMD;
       }
     }
     else if (tok_ptr->op == (NA|EQ|QU) )
     {
       int i, j, buf_size;
       buf_size =  res_buff_ptr->size;
       res_buff_ptr->used = 
         (uint16)snprintf((char*)res_buff_ptr->data_ptr, buf_size,
                                             "AGC: <band>,<rx_chan>[,<path>]\n");
       
       for (i = 0; i < (int)ARR_SIZE(agc_band_labels); i++)
       {
         band_label =  &agc_band_labels[i];
         res_buff_ptr->used += 
           (uint16)snprintf((char*)res_buff_ptr->data_ptr + res_buff_ptr->used, 
           buf_size - res_buff_ptr->used,
           "  <band>,<rx_chan> %s,(", band_label->name);
         
         for (j = 0; (CHAN_LIST_EOL_VAL != band_label->chan_list[j]) 
               && (j < CHAN_LIST_ENTRIES_MAX); j++)
         {
           char str_buf[24];
           if (CHAN_LIST_RANGE_VAL == band_label->chan_list[j])
           {
             (void)snprintf(str_buf, sizeof(str_buf), "-");
           }
           else
           {
             if (!j || (CHAN_LIST_RANGE_VAL == band_label->chan_list[j-1]))
             {
               (void)snprintf(str_buf, sizeof(str_buf), "%d",
                                  band_label->chan_list[j]);
             }
             else
             {
               (void)snprintf(str_buf, sizeof(str_buf), ",%d",
                                  band_label->chan_list[j]);
             }
           }
           res_buff_ptr->used += 
             (uint16)snprintf((char*)res_buff_ptr->data_ptr + res_buff_ptr->used,
             buf_size - res_buff_ptr->used,
             "%s", str_buf);
           
         } /* End for (j = 0; CHAN_LIST_EOL_VAL  */
         
         res_buff_ptr->used += 
           (uint16)snprintf((char*)res_buff_ptr->data_ptr + res_buff_ptr->used,
           buf_size - res_buff_ptr->used,
           ")\n");
       } /* End for (i = 0; i < ARR_SIZE(agc_band_labels) */
       
       res_buff_ptr->used += 
         (uint16)snprintf((char*)res_buff_ptr->data_ptr + res_buff_ptr->used, 
         buf_size - res_buff_ptr->used,
         "  <path> (\"MAIN\",\"AUX\")\n");
       return DSAT_OK;
       
     } /* End else if (tok_ptr->op == (NA|EQ|QU) ) */
     else
     {
       result = DSAT_ERROR;
     }
   }
   return result;   
} /* dsatvend_exec_qcagc_cmd */

#ifdef FEATURE_WCDMA
/*============================================================================

FUNCTION DSATVEND_EXEC_QCALLUP_SET_PA_RANGE_CB

DESCRIPTION
   Process received FTM_SET_PA_RANGE response

DEPENDENCIES
  
RETURN VALUE
  Returns an enum that describes the result of the command execution.
  Possible values:
                        
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success for local execution

SIDE EFFECTS
  None
  
===========================================================================*/
/*ARGSUSED*/
dsat_result_enum_type dsatvend_exec_qcallup_set_pa_range_cb
(
   ftm_pkt_type *ftm_pkt,
   void *rsp_arg
)
{
   dsatdvend_proc_qcallup_type *proc_qcallup = 
                      (dsatdvend_proc_qcallup_type *)rsp_arg;
   dsm_item_type *res_buff_ptr;
   
   res_buff_ptr = dsat_dsm_new_buffer(DSM_DS_LARGE_ITEM_POOL, FALSE);
   res_buff_ptr->used = (word)snprintf((char*)res_buff_ptr->data_ptr,
                                             res_buff_ptr->size,
                                             "ALLUP: %s",
                                       proc_qcallup->tx_enable ?
                                          "ON" : "OFF");
   dsatcmdp_handle_async_cmd_rsp(res_buff_ptr, DSAT_COMPLETE_RSP);
   SET_PENDING(DSAT_VENDOR_QCALLUP_IDX ,0, DSAT_PENDING_QCALLUP_NONE)
   return DSAT_OK;
} /* dsatvend_exec_qcallup_set_pa_range_cb */

/*============================================================================

FUNCTION DSATVEND_EXEC_QCALLUP_SET_TX_CB

DESCRIPTION
   Process received FTM_SET_TX_ON or FTM_SET_TX_OFF response
   Send FTM_SET_PA_RANGE command.

DEPENDENCIES
  
RETURN VALUE
  Returns an enum that describes the result of the command execution.
  Possible values:
                        
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success for local execution
    DSAT_ASYNC_EVENT : if it is a ASYNC EVENT

SIDE EFFECTS
  None
  
===========================================================================*/
/*ARGSUSED*/
dsat_result_enum_type dsatvend_exec_qcallup_set_tx_cb
(
   ftm_pkt_type *ftm_pkt,
   void *rsp_arg
)
{
   dsatdvend_proc_qcallup_type *proc_qcallup = 
                      (dsatdvend_proc_qcallup_type *)rsp_arg;
   
   ftm_diagpkt_subsys_header_type *ftm_diag_hdr;

   /* Send Get SYNTH state */
   ftm_diag_hdr = (ftm_diagpkt_subsys_header_type *)&ftm_pkt->header;
   ftm_diag_hdr->subsys_cmd_code = (diagpkt_subsys_cmd_code_type)
                                    proc_qcallup->ftm_dispatch_id;
 
   ftm_pkt->rf_params.ftm_rf_cmd  = (word)FTM_SET_PA_RANGE;
   ftm_pkt->rf_params.ftm_rf_factory_data.range = FTM_PA_R3;
 
   dsatvend_ftm_diagpkt_fwd(ftm_pkt, 
                             dsatvend_exec_qcallup_set_pa_range_cb,
                             (void *)proc_qcallup);
   SET_PENDING(DSAT_VENDOR_QCALLUP_IDX ,0, DSAT_PENDING_QCALLUP_SET_PA_RANGE)
   return DSAT_ASYNC_EVENT;
} /* dsatvend_exec_qcallup_set_tx_cb */

/*============================================================================

FUNCTION DSATVEND_EXEC_QCALLUP_SET_PDM_CB

DESCRIPTION
   Process received FTM_SET_PDM response
   Send FTM_SET_TX_ON or FTM_SET_TX_OFF

DEPENDENCIES
  
RETURN VALUE
  Returns an enum that describes the result of the command execution.
  Possible values:
                        
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success for local execution
    DSAT_ASYNC_EVENT : if it is a ASYNC EVENT

SIDE EFFECTS
  None
  
===========================================================================*/
/*ARGSUSED*/
dsat_result_enum_type dsatvend_exec_qcallup_set_pdm_cb
(
   ftm_pkt_type *ftm_pkt,
   void *rsp_arg
)
{
   dsatdvend_proc_qcallup_type *proc_qcallup = 
                      (dsatdvend_proc_qcallup_type *)rsp_arg;
   
   ftm_diagpkt_subsys_header_type *ftm_diag_hdr;

   /* Send Get SYNTH state */
   ftm_diag_hdr = (ftm_diagpkt_subsys_header_type *)&ftm_pkt->header;
   ftm_diag_hdr->subsys_cmd_code = (diagpkt_subsys_cmd_code_type)
                                    proc_qcallup->ftm_dispatch_id;
 
   if (proc_qcallup->tx_enable)
   {
      ftm_pkt->rf_params.ftm_rf_cmd  = (word)FTM_SET_TX_ON;
   }
   else
   {
      ftm_pkt->rf_params.ftm_rf_cmd  = (word)FTM_SET_TX_OFF;
   }
 
   dsatvend_ftm_diagpkt_fwd(ftm_pkt, 
                             dsatvend_exec_qcallup_set_tx_cb,
                             (void *)proc_qcallup);
   SET_PENDING(DSAT_VENDOR_QCALLUP_IDX ,0, DSAT_PENDING_QCALLUP_SET_TX)
   return DSAT_ASYNC_EVENT;
} /* dsatvend_exec_qcallup_set_pdm_cb */

/*============================================================================

FUNCTION DSATVEND_EXEC_QCALLUP_SET_CW_WAVEFORM_CB

DESCRIPTION
   Process received FTM_SET_CDMA_CW_WAVEFORM response.
   Send FTM_SET_PDM command.

DEPENDENCIES
  
RETURN VALUE
  Returns an enum that describes the result of the command execution.
  Possible values:
                        
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success for local execution
    DSAT_ASYNC_EVENT : if it is a ASYNC EVENT

SIDE EFFECTS
  None
  
===========================================================================*/
/*ARGSUSED*/
dsat_result_enum_type dsatvend_exec_qcallup_set_cw_waveform_cb
(
   ftm_pkt_type *ftm_pkt,
   void *rsp_arg
)
{
   dsatdvend_proc_qcallup_type *proc_qcallup = 
                      (dsatdvend_proc_qcallup_type *)rsp_arg;
   
   ftm_diagpkt_subsys_header_type *ftm_diag_hdr;

   /* Send Get SYNTH state */
   ftm_diag_hdr = (ftm_diagpkt_subsys_header_type *)&ftm_pkt->header;
   ftm_diag_hdr->subsys_cmd_code = (diagpkt_subsys_cmd_code_type)
                                    proc_qcallup->ftm_dispatch_id;
 
   ftm_pkt->rf_params.ftm_rf_cmd  = (word)FTM_SET_PDM;
   ftm_pkt->rf_params.ftm_rf_factory_data.id_val.id  = (word)FTM_PDM_TX_AGC_ADJ;
   ftm_pkt->rf_params.ftm_rf_factory_data.id_val.val = proc_qcallup->pdm;
   dsatvend_ftm_diagpkt_fwd(ftm_pkt, 
                             dsatvend_exec_qcallup_set_pdm_cb,
                             (void *)proc_qcallup);
   SET_PENDING(DSAT_VENDOR_QCALLUP_IDX ,0, DSAT_PENDING_QCALLUP_SET_PDM)
   return DSAT_ASYNC_EVENT;
  
} /* dsatvend_exec_qcallup_set_cw_waveform_cb */

/*============================================================================

FUNCTION DSATVEND_EXEC_QCALLUP_GET_SYNTH_CB

DESCRIPTION
   Process received FTM_GET_SYNTH_STATE response.
   Send FTM_SET_CDMA_CW_WAVEFORM command.

DEPENDENCIES
  
RETURN VALUE
  Returns an enum that describes the result of the command execution.
  Possible values:
                        
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success for local execution
    DSAT_ASYNC_EVENT : if it is a ASYNC EVENT
    DSAT_CMD_ERR_RSP : if sending out error_response

SIDE EFFECTS
  None
  
===========================================================================*/
/*ARGSUSED*/
dsat_result_enum_type dsatvend_exec_qcallup_get_synth_cb
(
   ftm_pkt_type *ftm_pkt,
   void *rsp_arg
)
{
   dsatdvend_proc_qcallup_type *proc_qcallup = 
                      (dsatdvend_proc_qcallup_type *)rsp_arg;
   
   ftm_diagpkt_subsys_header_type *ftm_diag_hdr;

   /* Verify Lock state in synth */
   if (!ftm_pkt->rf_params.ftm_rf_factory_data.on_off)
   {
      dsm_item_type *res_buff_ptr;
      res_buff_ptr = dsat_dsm_new_buffer(DSM_DS_LARGE_ITEM_POOL, FALSE);
      res_buff_ptr->used = (word)snprintf((char*)res_buff_ptr->data_ptr,
                                                res_buff_ptr->size,
                                                "AGC:Synth not locked");
      dsatcmdp_handle_async_cmd_rsp(res_buff_ptr, DSAT_COMPLETE_RSP);
      SET_PENDING(DSAT_VENDOR_QCALLUP_IDX ,0, DSAT_PENDING_QCALLUP_NONE)
      return DSAT_CMD_ERR_RSP;
   }


   /* Send FTM_SET_CDMA_CW_WAVEFORM (0) */
   ftm_diag_hdr = (ftm_diagpkt_subsys_header_type *)&ftm_pkt->header;
   ftm_diag_hdr->subsys_cmd_code = (diagpkt_subsys_cmd_code_type)
                                    proc_qcallup->ftm_dispatch_id;

   ftm_pkt->rf_params.ftm_rf_cmd  = (word)FTM_SET_CDMA_CW_WAVEFORM;
   ftm_pkt->rf_params.ftm_rf_factory_data.on_off = 0;

   dsatvend_ftm_diagpkt_fwd(ftm_pkt, 
                             dsatvend_exec_qcallup_set_cw_waveform_cb,
                             (void *)proc_qcallup);
  
  SET_PENDING(DSAT_VENDOR_QCALLUP_IDX ,0, DSAT_PENDING_QCALLUP_SET_CW_WAVE)
   return DSAT_ASYNC_EVENT;
} /* dsatvend_exec_qcallup_get_synth_cb */

/*============================================================================

FUNCTION DSATVEND_EXEC_QCALLUP_SET_CHAN_CB

DESCRIPTION
   Process received FTM_SET_CHAN response.
   Send FTM_GET_SYNTH_STATE command.

DEPENDENCIES
  
RETURN VALUE
  Returns an enum that describes the result of the command execution.
  Possible values:
                        
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success for local execution
    DSAT_ASYNC_EVENT : if it is a ASYNC EVENT

SIDE EFFECTS
  None
  
===========================================================================*/
/*ARGSUSED*/
dsat_result_enum_type dsatvend_exec_qcallup_set_chan_cb
(
   ftm_pkt_type *ftm_pkt,
   void *rsp_arg
)
{
   dsatdvend_proc_qcallup_type *proc_qcallup = 
                      (dsatdvend_proc_qcallup_type *)rsp_arg;
   
   ftm_diagpkt_subsys_header_type *ftm_diag_hdr;

   /* Send Get SYNTH state */
   ftm_diag_hdr = (ftm_diagpkt_subsys_header_type *)&ftm_pkt->header;
   ftm_diag_hdr->subsys_cmd_code = (diagpkt_subsys_cmd_code_type)
                                    proc_qcallup->ftm_dispatch_id;
 
   ftm_pkt->rf_params.ftm_rf_cmd  = (word)FTM_GET_SYNTH_STATE;
 
   dsatvend_ftm_diagpkt_fwd(ftm_pkt, 
                             dsatvend_exec_qcallup_get_synth_cb,
                             (void *)proc_qcallup);
  
   SET_PENDING(DSAT_VENDOR_QCALLUP_IDX ,0, DSAT_PENDING_QCAGC_GET_SYNTH)
   return DSAT_ASYNC_EVENT;
} /* dsatvend_exec_qcallup_set_chan_cb */

/*============================================================================

FUNCTION DSATVEND_EXEC_QCALLUP_SET_MODE_CB

DESCRIPTION
   Process received FTM_SET_MODE response.
   Send FTM_SET_CHAN command.

DEPENDENCIES
  
RETURN VALUE
  Returns an enum that describes the result of the command execution.
  Possible values:
                        
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success for local execution
    DSAT_ASYNC_EVENT : if it is a ASYNC EVENT

SIDE EFFECTS
  None
  
===========================================================================*/
/*ARGSUSED*/
dsat_result_enum_type dsatvend_exec_qcallup_set_mode_cb
(
   ftm_pkt_type *ftm_pkt,
   void *rsp_arg
)
{
   dsatdvend_proc_qcallup_type *proc_qcallup = 
                      (dsatdvend_proc_qcallup_type *)rsp_arg;
   ftm_diagpkt_subsys_header_type *ftm_diag_hdr;

   /* Send channel selection */
   ftm_diag_hdr = (ftm_diagpkt_subsys_header_type *)&ftm_pkt->header;
   ftm_diag_hdr->subsys_cmd_code = (diagpkt_subsys_cmd_code_type)
                                    proc_qcallup->ftm_dispatch_id;

   ftm_pkt->rf_params.ftm_rf_cmd  = (word)FTM_SET_CHAN;
   ftm_pkt->rf_params.ftm_rf_factory_data.chan = proc_qcallup->tx_chan;

  
   dsatvend_ftm_diagpkt_fwd(ftm_pkt, 
                            dsatvend_exec_qcallup_set_chan_cb,
                            (void *)proc_qcallup);
   
   SET_PENDING(DSAT_VENDOR_QCALLUP_IDX ,0, DSAT_PENDING_QCALLUP_SET_CHAN)
   return DSAT_ASYNC_EVENT;
} /* dsatvend_exec_qcallup_set_mode_cb */

/*============================================================================

FUNCTION DSATVEND_EXEC_QCALLUP_CMD

DESCRIPTION
  Turn RF transmitter on / off for specified band tuned to the first channel
   
  AT$QCALLUP <band>,<tx_chan>[,on|off]
  
   band -
      WCDMA_IMT     
      WCDMA_800    (Cell)
      WCDMA_1900   (PCS)
   
  Note: Restricted to FTM

DEPENDENCIES
  
RETURN VALUE
  Returns an enum that describes the result of the command execution.
  Possible values:
                        
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success for local execution
    DSAT_ASYNC_EVENT : if it is a ASYNC EVENT
    DSAT_CMD_ERR_RSP : if sending out error_response

SIDE EFFECTS
  None
  
===========================================================================*/
/*ARGSUSED*/
dsat_result_enum_type dsatvend_exec_qcallup_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
   dsat_result_enum_type result = DSAT_OK;
   
   static dsatdvend_proc_qcallup_type proc_qcallup;
   
   typedef struct tx_band_label_s {
      int              ftm_dispatch_id;
      char            *name;
      ftm_mode_id_type mode_band;
      int              chan_list[20];
   #define  CHAN_LIST_RANGE_VAL -1 /* denotes inclusive range pair */
   #define  CHAN_LIST_EOL_VAL   -2 /* end of list */
   } tx_band_label_type;

   tx_band_label_type *band_label;
   
   /* Supported Mode/Band labels with associated FTM and NV defs */
   tx_band_label_type allup_band_labels[] = {
      { FTM_WCDMA_C,                             "\"WCDMA_IMT\"",  
                     FTM_PHONE_MODE_WCDMA_IMT,
                     { BSP_RF_TX_CHAN_WCDMA_BC1_MIN, CHAN_LIST_RANGE_VAL, 
                       BSP_RF_TX_CHAN_WCDMA_BC1_MAX, CHAN_LIST_EOL_VAL} },
      
      { FTM_WCDMA_C,                             "\"WCDMA_800\"",  
                     FTM_PHONE_MODE_WCDMA_800 ,
                     { BSP_RF_TX_CHAN_WCDMA_BC5_MIN, CHAN_LIST_RANGE_VAL , 
                       BSP_RF_TX_CHAN_WCDMA_BC5_MAX, CHAN_LIST_EOL_VAL} },
      
      { FTM_WCDMA_C,                             "\"WCDMA_1900\"", 
                     FTM_PHONE_MODE_WCDMA_1900A, 
                     { BSP_RF_TX_CHAN_WCDMA_BC2_MIN, CHAN_LIST_RANGE_VAL, 
                       BSP_RF_TX_CHAN_WCDMA_BC2_MAX, CHAN_LIST_EOL_VAL } }
     };
     
   /* If arguments passed */
   if (tok_ptr->op == (NA|EQ|AR))
   {
      int i;
      int chan;        
      
      if ((tok_ptr->args_found < 1) || (tok_ptr->args_found > 4))
      {
         return DSAT_ERROR;
      }
      
      if (ftm_get_mode() != FTM_MODE)
      {
         res_buff_ptr->used =
            (uint16)snprintf ((char*)res_buff_ptr->data_ptr, res_buff_ptr->size,
                           "ERROR - restricted to FTM");
         return DSAT_CMD_ERR_RSP;  
      }

      /* Get and validate mode/band arg */
      band_label = NULL;
      for (i = 0; (!band_label) && (i < (int)ARR_SIZE(allup_band_labels)); i++)
      {
         if (!dsatutil_strcmp_ig_sp_case((const byte *)allup_band_labels[i].name, 
                                         tok_ptr->arg[0]))
         {
            band_label = &allup_band_labels[i];
         }
      }
      if (!band_label)
      {
         res_buff_ptr->used = 
           (uint16)snprintf((char*)res_buff_ptr->data_ptr, res_buff_ptr->size,
                            "ERROR: Invalid <band>\n");
         return DSAT_CMD_ERR_RSP;
      }
      proc_qcallup.mode_band       = band_label->mode_band;
      proc_qcallup.ftm_dispatch_id = band_label->ftm_dispatch_id;
      proc_qcallup.tx_enable       = 1;

      /* Get chan and validate */
      if (dsatutil_atoi((dsat_num_item_type *)&chan, tok_ptr->arg[1], 10))
      {
         return DSAT_ERROR;
      }

      /* Verify chan value is in list */
      for (i = 0; CHAN_LIST_EOL_VAL != band_label->chan_list[i]; i++)
      {
         if (chan >= band_label->chan_list[i])
         {
            if (chan == band_label->chan_list[i])
            {
               break;
            }
            if ((CHAN_LIST_EOL_VAL   != band_label->chan_list[i+2]) &&
                (CHAN_LIST_RANGE_VAL == band_label->chan_list[i+1]) &&
               (chan <= band_label->chan_list[i+2]))
            {
               break;
            }
         }
      }
      if (CHAN_LIST_EOL_VAL == band_label->chan_list[i])
      {
         res_buff_ptr->used = 
           (uint16)snprintf((char*)res_buff_ptr->data_ptr, res_buff_ptr->size,
                            "ERROR: Invalid <chan>\n");
         return DSAT_CMD_ERR_RSP;
      }
      proc_qcallup.tx_chan = chan;
      
      /* Check for optional arg "on" or "off" */
      if (tok_ptr->args_found > 2)
      {
         if (!dsatutil_strcmp_ig_sp_case("\"off\"", tok_ptr->arg[2]))
         {
            proc_qcallup.tx_enable = 0;
         } 
         else if (dsatutil_strcmp_ig_sp_case("\"on\"", tok_ptr->arg[2]))
         {
            res_buff_ptr->used =
              (uint16)snprintf((char*)res_buff_ptr->data_ptr, res_buff_ptr->size,
                            "ERROR: Invalid arg for \"on\"|\"off\"\n");
            return DSAT_CMD_ERR_RSP;
         }
      }
      
      /* Check for optional pdm value */
      proc_qcallup.pdm = 200;
      if (tok_ptr->args_found > 3)
      {
         if (dsatutil_atoi((dsat_num_item_type *)&proc_qcallup.pdm, tok_ptr->arg[3], 10))
         {
            return DSAT_ERROR;
         }
      }
      
      /* Start FTM command sequence to get AGC via commands -
       *    FTM_SET_MODE
       *    FTM_SET_CHAN
       *    FTM_GET_SYNTH_STATE
       *    FTM_SET_CDMA_CW_WAVEFORM (0)
       *    FTM_SET_PDM      (FTM_PDM_TX_AGC_ADJ, val)
       *    FTM_SET_TX_ON or FTM_SET_TX_OFF
       *    FTM_SET_PA_RANGE (3)
       */
      {
         ftm_pkt_type ftm_pkt;
         ftm_diagpkt_subsys_header_type *ftm_diag_hdr;

         /* Send FTM Mode / band selection */
         ftm_diag_hdr = (ftm_diagpkt_subsys_header_type *)&ftm_pkt.header;
         ftm_diag_hdr->subsys_cmd_code = (diagpkt_subsys_cmd_code_type)
                                          proc_qcallup.ftm_dispatch_id;
         ftm_pkt.rf_params.ftm_rf_cmd  = (word)FTM_SET_MODE;
         ftm_pkt.rf_params.ftm_rf_factory_data.mode = 
                                           proc_qcallup.mode_band;
         
         dsatvend_ftm_diagpkt_fwd(&ftm_pkt, 
                                  dsatvend_exec_qcallup_set_mode_cb,
                                   (void *)&proc_qcallup);
         SET_PENDING(DSAT_VENDOR_QCALLUP_IDX ,0, DSAT_PENDING_QCALLUP_SET_MODE)
         result = DSAT_ASYNC_CMD;
      }
   }
   else if (tok_ptr->op == (NA|EQ|QU) )
   {
      int i, j, buf_size;
      buf_size =  res_buff_ptr->size;
      res_buff_ptr->used = 
        (uint16)snprintf((char*)res_buff_ptr->data_ptr, buf_size,
                               "ALLUP: <band>,<tx_chan>[,\"on\"|\"off\"[,<pdm>]]\n");
      

      for (i = 0; i < (int)ARR_SIZE(allup_band_labels); i++)
      {
         band_label =  &allup_band_labels[i];
         res_buff_ptr->used += 
           (uint16)snprintf((char*)res_buff_ptr->data_ptr + res_buff_ptr->used, 
                             buf_size - res_buff_ptr->used,
                             "  <band>,<tx_chan> %s,(", band_label->name);
         
         for (j = 0; CHAN_LIST_EOL_VAL != band_label->chan_list[j]; j++)
         {
            char str_buf[24];
            if (CHAN_LIST_RANGE_VAL == band_label->chan_list[j])
            {
               (void)snprintf(str_buf, sizeof(str_buf), "-");
            }
            else
            {
               if (!j || (CHAN_LIST_RANGE_VAL == band_label->chan_list[j-1]))
               {
                  (void)snprintf(str_buf, sizeof(str_buf), "%d",
                                 band_label->chan_list[j]);
               }
               else 
               {
                  (void)snprintf(str_buf, sizeof(str_buf), ",%d",
                                 band_label->chan_list[j]);
               }
            }
            res_buff_ptr->used += 
              (uint16)snprintf((char*)res_buff_ptr->data_ptr + res_buff_ptr->used,
                                 buf_size - res_buff_ptr->used,
                                 "%s", str_buf);
            
         } /* End for (j = 0; CHAN_LIST_EOL_VAL */
         
         res_buff_ptr->used += 
           (uint16)snprintf((char*)res_buff_ptr->data_ptr + res_buff_ptr->used,
                             buf_size - res_buff_ptr->used,
                             ")\n");
      } /* End for (i = 0; i < ARR_SIZE(allup_band_labels) */
      
      res_buff_ptr->used += 
        (uint16)snprintf((char*)res_buff_ptr->data_ptr + res_buff_ptr->used,
                          buf_size - res_buff_ptr->used,
                          "  \"on\"|\"off\" Turns Tx on or off [\"on\" if not specified]\n");
      res_buff_ptr->used += 
        (uint16)snprintf((char*)res_buff_ptr->data_ptr + res_buff_ptr->used,
                          buf_size - res_buff_ptr->used,
                          "   <pdm> optional TX PDM AGC value [200 if not specified]");
      
      return DSAT_OK;
   } /* End else if (tok_ptr->op == (NA|EQ|QU) ) */
   else
   {
     result = DSAT_ERROR;
   }
   return result;
} /* dsatvend_exec_qcallup_cmd */
#endif /* FEATURE_WCDMA */

/*===========================================================================

FUNCTION dsatvend_exec_qcsku_cmd

DESCRIPTION
  This function returns the SKU info

DEPENDENCIES
  
RETURN VALUE

SIDE EFFECTS
  None
  
===========================================================================*/
dsat_result_enum_type dsatvend_exec_qcsku_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type result = DSAT_OK;

  /*-------------------------------------------------------
  command with no arguments will return the info string
  ---------------------------------------------------------*/
  if (tok_ptr->op == NA)
  {
     nv_stat_enum_type nv_status;

     memset(&ds_nv_item, 0, sizeof(ds_nv_item));
     nv_status = dsatutil_get_nv_item( NV_FACTORY_DATA_4_I , &ds_nv_item );

     if ( nv_status == NV_DONE_S )
     {
       res_buff_ptr->used = strlcpy((char*)res_buff_ptr->data_ptr, 
                          (const char*)ds_nv_item.factory_data_4,
                          res_buff_ptr->size);
     }
     else
     {
       res_buff_ptr->used =
         (word)snprintf ((char*)res_buff_ptr->data_ptr, 12,
                               "SKU Not Set");
     }
  }
  else
  {
     result = DSAT_ERROR;
  }
  return result;
} /* dsatvend_exec_qcsku_cmd */
#endif /* FEATURE_DSAT_GOBI_MAINLINE */
#ifdef FEATURE_ECALL_APP
/*===========================================================================
FUNCTION  DSAT_ECALL_CB_FUNC

DESCRIPTION
  ECALL status command callback function

DEPENDENCIES
  None

RETURNS
  None

SIDE EFFECTS
  Adds command in DS command buffer

===========================================================================*/
/* ARGSUSED */
LOCAL void dsat_ecall_cb_func 
(
  ecall_session_status_type  session_status 
)
{
  ds_cmd_type * cmd_buf;
  
  cmd_buf = dsat_get_cmd_buf(FALSE);
  /* send the message to ATCOP */
  cmd_buf->hdr.cmd_id = DS_AT_ECALL_CMD;
  cmd_buf->cmd.ecall_cmd.session_status = session_status;
  ds_put_cmd(cmd_buf);
}/*dsat_ecall_cb_func*/

/*===========================================================================
FUNCTION DSATVEND_ECALL_AT_CMD_HANDLER

DESCRIPTION
  This function is handler function for reporting ecall session response.

DEPENDENCIES
  None
  
RETURN VALUE
  DSAT_ASYNC_EVENT: Always Returns ASYNC_EVENT.
 
SIDE EFFECTS
  None
  
======================================================================*/
/*ARGSUSED*/
dsat_result_enum_type dsatvend_ecall_at_cmd_handler
(
  dsat_mode_enum_type   mode,                /* AT command mode            */
  ds_cmd_type         * cmd_ptr              /* DS Command pointer         */
)
{
  dsat_result_enum_type      result       = DSAT_ERROR;
  dsat_num_item_type         cecall_type  = DSAT_CECALL_MAX;
  dsm_item_type            * res_buff_ptr = NULL;
  
  if(!dsatcmdp_is_etsi_cmd())
  {
    return DSAT_ASYNC_EVENT;
  }
  
  ASSERT(cmd_ptr != NULL);
  DS_AT_MSG3_HIGH("Response for Ecall  %d call type %d Active type %d",
                  cmd_ptr->cmd.ecall_cmd.session_status,
                  cmd_ptr->cmd.ecall_cmd.type_of_call,
                  cmd_ptr->cmd.ecall_cmd.activation_type);
  
  if(cmd_ptr->hdr.cmd_id != DS_AT_ECALL_CMD)
  {
    DS_AT_MSG1_ERROR("ECALL cmd id  mismatch %d",cmd_ptr->hdr.cmd_id);
    return DSAT_ASYNC_EVENT;
  }
  if(CHECK_PENDING(DSATETSI_EXT_CECALL_IDX,0,DSAT_PENDING_CECALL_WRITE))
  {
    if(cmd_ptr->cmd.ecall_cmd.session_status == ECALL_FAILURE)
    {
      result = DSAT_NO_CARRIER;
    }
    else
    {
      result = DSAT_OK;
    }
    
    SET_PENDING(DSATETSI_EXT_CECALL_IDX ,0, DSAT_PENDING_CECALL_NONE)
    
  }
  else if (CHECK_PENDING(DSATETSI_EXT_CECALL_IDX,0,DSAT_PENDING_CECALL_READ))
  {
    SET_PENDING(DSATETSI_EXT_CECALL_IDX ,0, DSAT_PENDING_CECALL_NONE)
    if(cmd_ptr->cmd.ecall_cmd.type_of_call == ECALL_NO_ACTIVE_CALL)
    {
      result = DSAT_OK;
    }
    else
    {
       if((cmd_ptr->cmd.ecall_cmd.type_of_call == ECALL_TEST) &&  
             (cmd_ptr->cmd.ecall_cmd.activation_type == ECALL_MANUAL_INITIATED))
       {
         cecall_type = DSAT_CECALL_TEST;
         result = DSAT_OK;
       }
       else if((cmd_ptr->cmd.ecall_cmd.type_of_call == ECALL_RECONFIG) &&  
                  (cmd_ptr->cmd.ecall_cmd.activation_type == ECALL_MANUAL_INITIATED))
       {
         cecall_type = DSAT_CECALL_RECONF;
         result = DSAT_OK;
       }
       else if((cmd_ptr->cmd.ecall_cmd.type_of_call == ECALL_EMERGENCY )&&  
                 (cmd_ptr->cmd.ecall_cmd.activation_type == ECALL_MANUAL_INITIATED))
       {  
      
         cecall_type = DSAT_CECALL_MANUAL;
         result = DSAT_OK;
       }
       else if((cmd_ptr->cmd.ecall_cmd.type_of_call ==  ECALL_EMERGENCY) &&  
                (cmd_ptr->cmd.ecall_cmd.activation_type == ECALL_AUTOMATIC_INITIATED))
       {
         cecall_type = DSAT_CECALL_AUTO;
         result = DSAT_OK;
       }
       else
       {
         result = DSAT_ERROR;
       }

      if(result == DSAT_OK)
      {
        res_buff_ptr = dsat_dsm_new_buffer(DSM_DS_SMALL_ITEM_POOL, FALSE);
        res_buff_ptr->used = (word)snprintf ( (char*)res_buff_ptr->data_ptr,
                                                     res_buff_ptr->size,
                                                     "+CECALL: %d",
                                                     cecall_type);
        dsatcmdp_handle_async_cmd_rsp(res_buff_ptr, DSAT_COMPLETE_RSP);
      
      }
    }
  }
  else
  {
    if(cmd_ptr->cmd.ecall_cmd.session_status == ECALL_FAILURE)
    {
      dsatrsp_send_cell_result(DSAT_NO_CARRIER);
    }
    result = DSAT_ASYNC_EVENT;
  }
  return result;
}/*dsatvend_ecall_at_cmd_handler*/

/*===========================================================================

FUNCTION DSAVEND_EXEC_ECALL_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes AT$ECALL command.
  This command provides the ability to make a eCall to the network. 

dsat_ecall_val[0]  -> Start/Stop the eCall 0 - Stop ; 1 - Start
dsat_ecall_val[1]  -> Type of the eCall    0 - Test ; 1 - Emergency
dsat_ecall_val[2]  -> Activation type      0 - Manual; 1- Automatic

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR :    if there was any problem in executing the command
    DSAT_OK :       if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
/*ARGSUSED*/
dsat_result_enum_type dsatvend_exec_ecall_cmd 
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
#define START_ECALL 1
  dsat_result_enum_type result = DSAT_ERROR;
  dsat_num_item_type dsat_prev_ecall_val[3];
  dsati_operating_cmd_mode_type oprt_mode = dsatcmdp_get_operating_cmd_mode();

  if(oprt_mode != ETSI_CMD_MODE)
  {
    DS_AT_MSG0_ERROR("Invalid Operating Mode");
    return result;
  }

  if( (tok_ptr->op == (NA|EQ|AR)) ||
      (tok_ptr->op == (NA|EQ|QU)) ||
      (tok_ptr->op == (NA|QU)))
  {
    dsat_prev_ecall_val[0] = (dsat_num_item_type)dsatutil_get_val(
        DSAT_VENDOR_ECALL_IDX,0,0,NUM_TYPE);
    dsat_prev_ecall_val[1] = (dsat_num_item_type)dsatutil_get_val(
        DSAT_VENDOR_ECALL_IDX,0,1,NUM_TYPE);
    dsat_prev_ecall_val[2] =(dsat_num_item_type)dsatutil_get_val(
        DSAT_VENDOR_ECALL_IDX,0,2,NUM_TYPE);

    /* First argument is mandatory for an Action command.
       If omitted throw an Error */
    if(((tok_ptr->op == (NA|EQ|AR))) && 
       ( !VALID_TOKEN(0) && (tok_ptr->args_found > 1) ))
    {
      DS_AT_MSG0_ERROR("Invalid params");
      return result;
    }
    result = dsatparm_exec_param_cmd ( mode,
                                        parse_table,
                                        tok_ptr,
                                        res_buff_ptr
                                      );
    if((result == DSAT_OK)&&
       (tok_ptr->op == (NA|EQ|AR)))
    {
      if((dsat_num_item_type)dsatutil_get_val(
                   DSAT_VENDOR_ECALL_IDX,0,0,NUM_TYPE) == START_ECALL)
      {
        if( FALSE == ecall_session_start((ecall_type_of_call)(dsat_num_item_type)dsatutil_get_val(
                                         DSAT_VENDOR_ECALL_IDX,0,1,NUM_TYPE),
                                         (ecall_activation_type)(dsat_num_item_type)dsatutil_get_val(
                                         DSAT_VENDOR_ECALL_IDX,0,2,NUM_TYPE),
                                         dsat_ecall_cb_func))
        {
          DS_AT_MSG0_ERROR("Unable to start the Ecall");

          DSATUTIL_SET_VAL(DSAT_VENDOR_ECALL_IDX,0,0,0,dsat_prev_ecall_val[0],NUM_TYPE)
          DSATUTIL_SET_VAL(DSAT_VENDOR_ECALL_IDX,0,1,0,dsat_prev_ecall_val[1],NUM_TYPE)
          DSATUTIL_SET_VAL(DSAT_VENDOR_ECALL_IDX,0,2,0,dsat_prev_ecall_val[2],NUM_TYPE)
  
          result = DSAT_ERROR;
        }
      }
      else
      {
        if( FALSE == ecall_session_stop( ))
        {
          DS_AT_MSG0_ERROR("Unable to stop the Ecall");

          DSATUTIL_SET_VAL(DSAT_VENDOR_ECALL_IDX,0,0,0,dsat_prev_ecall_val[0],NUM_TYPE)
          DSATUTIL_SET_VAL(DSAT_VENDOR_ECALL_IDX,0,1,0,dsat_prev_ecall_val[1],NUM_TYPE)
          DSATUTIL_SET_VAL(DSAT_VENDOR_ECALL_IDX,0,2,0,dsat_prev_ecall_val[2],NUM_TYPE)

  
          result = DSAT_ERROR;
        }
      }

    }
  }
  return result;
}/*dsatvend_exec_ecall_cmd*/
#endif /* FEATURE_ECALL_APP */
/*===========================================================================

FUNCTION dsatvend_exec_qcsku_cmd

DESCRIPTION
  This function returns the SKU info

DEPENDENCIES
  
RETURN VALUE

SIDE EFFECTS
  None
  
===========================================================================*/
dsat_result_enum_type dsatvend_exec_qcsku_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type result = DSAT_OK;

  /*-------------------------------------------------------
  command with no arguments will return the info string
  ---------------------------------------------------------*/
  if (tok_ptr->op == NA)
  {
     nv_stat_enum_type nv_status;

     memset(&ds_nv_item, 0, sizeof(ds_nv_item));
     nv_status = dsatutil_get_nv_item( NV_FACTORY_DATA_4_I , &ds_nv_item );

     if ( nv_status == NV_DONE_S )
     {
       res_buff_ptr->used = strlcpy((char*)res_buff_ptr->data_ptr, 
                          (const char*)ds_nv_item.factory_data_4,
                          res_buff_ptr->size);
     }
     else
     {
       res_buff_ptr->used =
         (word)snprintf ((char*)res_buff_ptr->data_ptr, 12,
                               "SKU Not Set");
     }
  }
  else
  {
     result = DSAT_ERROR;
  }
  return result;
} /* dsatvend_exec_qcsku_cmd */

/*===========================================================================

FUNCTION DSATVEND_EXEC_QCGSN_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes +GSN command.
  It reads the required NV items (ESN/MEID) and prints them out.

DEPENDENCIES

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR:        syntax error.
    DSAT_OK          : if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatvend_exec_qcgsn_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type result = DSAT_OK;

  /*-------------------------------------------------------
  command with no arguments will return the info string
  ---------------------------------------------------------*/
  if (tok_ptr->op == NA)
  {
     nv_stat_enum_type nv_status;

     nv_status = dsatutil_get_nv_item( NV_MEID_I , &ds_nv_item );

     /* If MEID is not set, then report ESN. */
     if (( nv_status != NV_DONE_S ) || 
         ((0 == qw_hi(ds_nv_item.meid)) && (0 == qw_lo(ds_nv_item.meid)))) 
     {
        
        /* ESN Reporting */
        nv_status = dsatutil_get_nv_item (NV_ESN_I, &ds_nv_item);
        if ((nv_status == NV_DONE_S) && 
            (0 != ds_nv_item.esn.esn)) 
        {
          res_buff_ptr->used = (word)snprintf(
                                              (char*)res_buff_ptr->data_ptr,
                                              res_buff_ptr->size,
                                              "0x%08X",(unsigned int) ds_nv_item.esn.esn);
        }
        else 
        {
           res_buff_ptr->used = (word)snprintf(
                                              (char*)res_buff_ptr->data_ptr,
                                               res_buff_ptr->size,
                                               "0x0");
        }
     }

     /* Else report MEID
     */
     else
     {
#if defined(FEATURE_DSAT_EXTENDED_CMD)
       if ( (!strncmp((const char *)parse_table->name,(const char *)"^MEID",strlen((const char *)"^MEID"))) || 
            (!strncmp((const char *)parse_table->name,(const char *)"^GSN",strlen((const char *)"^GSN"))) )
       {
         res_buff_ptr->used = (word)snprintf(
                                              (char*)res_buff_ptr->data_ptr,
                                              res_buff_ptr->size,
                                              "0x%06X%08X", 
                                              (unsigned int)qw_hi(ds_nv_item.meid),
                                              (unsigned int)qw_lo(ds_nv_item.meid));
       }
       else
#endif /* defined(FEATURE_DSAT_EXTENDED_CMD) */
       {
       
          res_buff_ptr->used = (word)snprintf(
                                              (char*)res_buff_ptr->data_ptr,
                                              res_buff_ptr->size,
                                              "0x%08X%08X", 
                                              (unsigned int)qw_hi(ds_nv_item.meid),
                                              (unsigned int)qw_lo(ds_nv_item.meid));
       }
     }
  }
  /*-------------------------------------------------------
  General commands with a query should produce no response
  ---------------------------------------------------------*/
  else if (tok_ptr->op == (NA|EQ|QU))
  {
    /* this command is valid, but do nothing */
    result = DSAT_OK;
  }  
  else
  {
     result = DSAT_ERROR;
  }
  return result;
}/* dsatvend_exec_qcgsn_cmd */

/*===========================================================================

FUNCTION DSATVEND_EXEC_QCCLAC_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes $QCCLAC command.
  It reads all the supported AT commands and prints them.

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR :    if there was any problem in executing the command
    DSAT_OK :         if it is a success and execution completed.
SIDE EFFECTS
  None

===========================================================================*/
/* ARGSUSED */
dsat_result_enum_type dsatvend_exec_qcclac_cmd
(
  dsat_mode_enum_type mode,                /*  AT command mode:            */
  const dsati_cmd_type *parse_table,       /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,       /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr              /*  Place to put response       */
)
{
  /* process command */
  if ((tok_ptr->op == NA) | (tok_ptr->op == (NA|QU)))
  {
    unsigned int i=0;
    char  buffer[MAX_CMD_SIZE];
    word size = 0;

    for (i=0;i<dsat_basic_table_size;i++)
    {
      size = (word)snprintf(&(buffer[0]),
                             MAX_CMD_SIZE,
                             "%s\n",
                             dsat_basic_table[i].name);

      dsat_dsm_pushdown_tail(&res_buff_ptr, buffer, size,
                             DSM_ITEM_POOL(res_buff_ptr), FALSE);
    }

    for (i=0;i<dsat_basic_action_table_size;i++)
    {
      size = (word)snprintf(&(buffer[0]),
                             MAX_CMD_SIZE,
                             "%s\n",
                             dsat_basic_action_table[i].name);

      dsat_dsm_pushdown_tail(&res_buff_ptr, buffer, size,
                             DSM_ITEM_POOL(res_buff_ptr), FALSE);
    }

    for (i=0;i<dsat_sreg_table_size;i++)
    {
      size = (word)snprintf(&(buffer[0]),
                             MAX_CMD_SIZE,
                             "%s\n",
                             dsat_sreg_table[i].name);

      dsat_dsm_pushdown_tail(&res_buff_ptr, buffer, size,
                             DSM_ITEM_POOL(res_buff_ptr), FALSE);
    }

    for (i=0;i<dsat_ext_table_size;i++)
    {
      size = (word)snprintf(&(buffer[0]),
                             MAX_CMD_SIZE,
                             "%s\n",
                             dsat_ext_table[i].name);

      dsat_dsm_pushdown_tail(&res_buff_ptr, buffer, size,
                             DSM_ITEM_POOL(res_buff_ptr), FALSE);
    }

#ifdef FEATURE_DATA_IS707
    for (i=0;i<dsat707_ext_para_table_size;i++)
    {
      size = (word)snprintf(&(buffer[0]),
                             MAX_CMD_SIZE,
                             "%s\n",
                             dsat707_ext_para_table[i].name);

      dsat_dsm_pushdown_tail(&res_buff_ptr, buffer, size,
                             DSM_ITEM_POOL(res_buff_ptr), FALSE);
    }

    for (i=0;i<dsat707_ext_action_table_size;i++)
    {
      size = (word)snprintf(&(buffer[0]),
                             MAX_CMD_SIZE,
                             "%s\n",
                             dsat707_ext_action_table[i].name);

      dsat_dsm_pushdown_tail(&res_buff_ptr, buffer, size,
                             DSM_ITEM_POOL(res_buff_ptr), FALSE);
    }

    for (i=0;i<dsat707_fax_table_size;i++)
    {
      size = (word)snprintf(&(buffer[0]),
                             MAX_CMD_SIZE,
                             "%s\n",
                             dsat707_fax_table[i].name);

      dsat_dsm_pushdown_tail(&res_buff_ptr, buffer, size,
                             DSM_ITEM_POOL(res_buff_ptr), FALSE);
    }
#endif /* FEATURE_DATA_IS707 */

    for (i=0;i<dsat_vendor_table_size;i++)
    {
      size = (word)snprintf(&(buffer[0]),
                             MAX_CMD_SIZE,
                             "%s\n",
                             dsat_vendor_table[i].name);

      dsat_dsm_pushdown_tail(&res_buff_ptr, buffer, size,
                             DSM_ITEM_POOL(res_buff_ptr), FALSE);
    }

#ifdef FEATURE_DATA_IS707

    for (i=0;i<dsat707_vend_para_table_size;i++)
    {
      size = (word)snprintf(&(buffer[0]),
                             MAX_CMD_SIZE,
                             "%s\n",
                             dsat707_vend_para_table[i].name);

      dsat_dsm_pushdown_tail(&res_buff_ptr, buffer, size,
                             DSM_ITEM_POOL(res_buff_ptr), FALSE);
    }

    for (i=0;i<dsat707_vend_action_table_size;i++)
    {
      size = (word)snprintf(&(buffer[0]),
                             MAX_CMD_SIZE,
                             "%s\n",
                             dsat707_vend_action_table[i].name);

      dsat_dsm_pushdown_tail(&res_buff_ptr, buffer, size,
                             DSM_ITEM_POOL(res_buff_ptr), FALSE);
    }

#ifdef FEATURE_HDR_AT_TEST_ONLY
    for (i=0;i<dsat707_hdr_table_size;i++)
    {
      size = (word)snprintf(&(buffer[0]),
                             MAX_CMD_SIZE,
                             "%s\n",
                             dsat707_hdr_table[i].name);

      dsat_dsm_pushdown_tail(&res_buff_ptr, buffer, size,
                             DSM_ITEM_POOL(res_buff_ptr), FALSE);
    }
#endif /* FEATURE_HDR_AT_TEST_ONLY */

#ifdef FEATURE_DS_MOBILE_IP

    for (i=0;i<dsat707_mip_table_size;i++)
    {
      size = (word)snprintf(&(buffer[0]),
                             MAX_CMD_SIZE,
                             "%s\n",
                             dsat707_mip_table[i].name);

      dsat_dsm_pushdown_tail(&res_buff_ptr, buffer, size,
                             DSM_ITEM_POOL(res_buff_ptr), FALSE);
    }
#endif /* FEATURE_DS_MOBILE_IP */

#ifdef FEATURE_DS_PSTATS
    for (i=0;i<dsat707_pstats_table_size;i++)
    {
      size = (word)snprintf(&(buffer[0]),
                             MAX_CMD_SIZE,
                             "%s\n",
                             dsat707_pstats_table[i].name);

      dsat_dsm_pushdown_tail(&res_buff_ptr, buffer, size,
                             DSM_ITEM_POOL(res_buff_ptr), FALSE);
    }
#endif /* FEATURE_DS_PSTATS */

#ifdef FEATURE_CDMA_SMS
    for (i=0;i<dsat707_sms_table_size;i++)
    {
      size = (word)snprintf(&(buffer[0]),
                             MAX_CMD_SIZE,
                             "%s\n",
                             dsat707_sms_table[i].name);

      dsat_dsm_pushdown_tail(&res_buff_ptr, buffer, size,
                             DSM_ITEM_POOL(res_buff_ptr), FALSE);
    }
#endif /* FEATURE_CDMA_SMS */

#ifdef FEATURE_BCMCS
    for (i=0;i<dsat_1xhdr_bc_table_size;i++)
    {
      size = (word)snprintf(&(buffer[0]),
                             MAX_CMD_SIZE,
                             "%s\n",
                             dsat_1xhdr_bc_table[i].name);

      dsat_dsm_pushdown_tail(&res_buff_ptr, buffer, size,
                             DSM_ITEM_POOL(res_buff_ptr), FALSE);
    }
#endif /* FEATURE_BCMCS */
#endif /* FEATURE_DATA_IS707 */

#ifdef FEATURE_DSAT_ETSI_MODE
    for (i=0;i<dsatetsi_ext_action_table_size;i++)
    {
      size = (word)snprintf(&(buffer[0]),
                             MAX_CMD_SIZE,
                             "%s\n",
                             dsatetsi_ext_action_table[i].name);

      dsat_dsm_pushdown_tail(&res_buff_ptr, buffer, size,
                             DSM_ITEM_POOL(res_buff_ptr), FALSE);
    }

    for (i=0;i<dsatetsi_ext_table_size;i++)
    {
      size = (word)snprintf(&(buffer[0]),
                             MAX_CMD_SIZE,
                             "%s\n",
                             dsatetsi_ext_table[i].name);

      dsat_dsm_pushdown_tail(&res_buff_ptr, buffer, size,
                             DSM_ITEM_POOL(res_buff_ptr), FALSE);
    }
#endif /* FEATURE_DSAT_ETSI_MODE */

    return DSAT_OK;
  }
  else if (tok_ptr->op == (NA|EQ|QU))
  {
    return DSAT_OK;
  }
  else
  {
    return DSAT_ERROR;
  }
} /* dsatvend_exec_qcclac_cmd */

/*===========================================================================
FUNCTION   DSATVEND_IFACE_EV_CB()

DESCRIPTION
  This callback function is called when one of the registered ps_iface events
  occur on external interface.  COMING UP, DOWN and CONFIG iface events are
  registered, and affect the DUN call info message and indication behaviour.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
static void dsatvend_iface_ev_cb
(
  ps_iface_type             *this_iface_ptr,
  ps_iface_event_enum_type   event,
  ps_iface_event_info_u_type event_info,
  void                      *user_data_ptr
)
{
  ds_cmd_type * cmd_buf = NULL;
  ps_iface_name_enum_type  iface_name;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  iface_name = (ps_iface_name_enum_type)
                      PS_IFACE_GET_NAME_FROM_ID(PS_IFACE_GET_ID(this_iface_ptr));

  if ( iface_name != SIO_IFACE )
  {
    return;
  }
  
  DS_AT_MSG2_HIGH("Received Iface Event [%d] from i/f [0x%x]", event, this_iface_ptr);

  switch( event )
  {
    case IFACE_COMING_UP_EV:
    case IFACE_DOWN_EV:
    case IFACE_CONFIGURING_EV:
    {
      cmd_buf = dsat_get_cmd_buf(FALSE);
      /* send the message to ATCOP */
      cmd_buf->hdr.cmd_id = DS_AT_IFACE_EV_CMD;
      cmd_buf->cmd.iface_info.iface_ptr = this_iface_ptr;
      cmd_buf->cmd.iface_info.event = event;
      cmd_buf->cmd.iface_info.event_info = event_info;
      cmd_buf->cmd.iface_info.user_data = user_data_ptr;
      ds_put_cmd(cmd_buf);
      break;
    }

    default:
    {
      DS_AT_MSG1_ERROR("Unregistered event %d recvd, ignoring", event);
      break;
    }
  } /* switch( event ) */
} /* dsatvend_iface_ev_cb() */

/*===========================================================================
FUNCTION   DSATVEND_IFACE_EV_HANDLER()

DESCRIPTION
 This is a handler function for all the IFACE releated events. ATCoP is intrested in IFACE COMING UP, DOWN,
 CONFIGURE events. 

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
dsat_result_enum_type dsatvend_iface_ev_handler
(
  dsat_mode_enum_type mode, /* AT command mode    */
  ds_cmd_type * cmd_ptr     /* DS Command pointer */
)
{
  dsat_iface_info_s_type *iface_info = NULL;
  ps_iface_type          *v4_rmnet_iface_ptr = NULL;
  ps_iface_type          *v6_rmnet_iface_ptr = NULL;
  uint32                  lap_inst = 0;
  dsat_result_enum_type result = DSAT_ASYNC_EVENT;
  boolean v4_instance = FALSE; 
  boolean v6_instance = FALSE;
        

  ASSERT(NULL !=cmd_ptr );

  iface_info = &(cmd_ptr->cmd.iface_info);

  DS_AT_MSG1_HIGH("dsatvend_iface_ev_handler State [%d]",dsat_qcrmcall_info.state);

  for ( lap_inst = 0; lap_inst <=RMNET_NUM_LAPTOP_INSTANCES ; ++lap_inst )
  {
    if ( iface_info->iface_ptr == dsat_qcrmcall_global_info.iface_ptr[lap_inst] )
    {
      DS_AT_MSG1_HIGH("Match found at instance [%d]", lap_inst);
      break;
    }
  }

  if ( lap_inst <= RMNET_NUM_LAPTOP_INSTANCES)
  {
    if (0 == (lap_inst %2 )) /* V4 INSTANCE */
    {
      v4_instance = TRUE;
    }
    else 
    {
      v6_instance = TRUE;
    }
  }
 
  switch(iface_info->event )
  {
    case IFACE_COMING_UP_EV:
      if ( DSAT_QCRMCALL_MIN == dsat_qcrmcall_info.state )
      {
        /* Event not related to QCRMCALL */
        return result;
      }
      /* For Start QCRMCALL, Coming Up Ev is the first event until which the Instance should be valid 
               and later, it will be based on the Rm Iface matching to send the final response. */
      if ( RMNET_IS_VALID_LAPTOP_INSTANCE(dsat_qcrmcall_info.v4_instance))
      {
        v4_rmnet_iface_ptr = 
        rmnet_meta_sm_get_laptop_rm_iface_ptr(dsat_qcrmcall_info.v4_instance );
      }
      if ( RMNET_IS_VALID_LAPTOP_INSTANCE(dsat_qcrmcall_info.v6_instance))
      {
        v6_rmnet_iface_ptr = 
        rmnet_meta_sm_get_laptop_rm_iface_ptr(dsat_qcrmcall_info.v6_instance );
      }
      /* Not a relating event for QCRMCALL */
      if ( (NULL == v4_rmnet_iface_ptr) && ( NULL == v6_rmnet_iface_ptr ))
      {
        DS_AT_MSG3_MED("Event [%d] received in wrong state V4 Instance [%d] V6 Instance [%d] ", 
                              iface_info->event, 
                              dsat_qcrmcall_info.v4_instance,
                              dsat_qcrmcall_info.v6_instance );
        return result;
      }
      /* Not related to QCRMCALL */
      if ( (iface_info->iface_ptr != v4_rmnet_iface_ptr )  &&
           (iface_info->iface_ptr != v6_rmnet_iface_ptr ) )
      {
        DS_AT_MSG3_MED(" Ignoring Iface [0x%x] event Waiting for V4 Iface [0x%x] or V6  Iface [0x%x] events", 
                                            iface_info->iface_ptr, 
                                            v4_rmnet_iface_ptr, 
                                            v6_rmnet_iface_ptr );
        return result;
      }

      if ( (NULL != v4_rmnet_iface_ptr ) && 
           ( iface_info->iface_ptr == v4_rmnet_iface_ptr ) )
      {
        dsat_qcrmcall_info.v4_rm_iface_ptr = v4_rmnet_iface_ptr;
        dsat_qcrmcall_global_info.iface_ptr[2*(RMNET_GET_LAPTOP_INSTANCE(dsat_qcrmcall_info.v4_instance))] =  v4_rmnet_iface_ptr;
        dsat_qcrmcall_info.state = DSAT_QCRMCALL_COMING_UP;
      }
      if ( ( NULL != v6_rmnet_iface_ptr ) && 
           ( iface_info->iface_ptr == v6_rmnet_iface_ptr ) )
      {
        dsat_qcrmcall_info.v6_rm_iface_ptr = v6_rmnet_iface_ptr;
        dsat_qcrmcall_global_info.iface_ptr[1+ (2*(RMNET_GET_LAPTOP_INSTANCE(dsat_qcrmcall_info.v6_instance)))] 
                                       =  v6_rmnet_iface_ptr;
        dsat_qcrmcall_info.state = DSAT_QCRMCALL_COMING_UP;
      }
      
      break;
    case IFACE_DOWN_EV:
      {

        DS_AT_MSG0_HIGH(" QCRMCALL Iface down handler");
 
        /* When there is no active command and the call got disconnected either 
                 due to Network end or OOS Send URC NO CARRIER */
        if ( (TRUE == v4_instance ) || ( TRUE == v6_instance ))
        {
          dsat_qcrmcall_global_info.iface_ptr[lap_inst]= NULL;
          result = DSAT_NO_CARRIER;
        }
        
        /* When Start Call is active and in progress  */
        if ( ( dsat_qcrmcall_info.state == DSAT_QCRMCALL_COMING_UP ) ||
             (dsat_qcrmcall_info.state == DSAT_QCRMCALL_START_INIT ))
        {
          /* If rm_iface_ptr is set, the Start Action is still in pending  */

          /* By the time Rm Iface down is received, the Rm Coming up shoudl 
                     have happend */
          if ( TRUE == v4_instance )
          {
            dsat_qcrmcall_info.v4_instance = RMNET_INSTANCE_MAX;
            DS_AT_MSG0_HIGH(" QCRMCALL V4 Failed");
          }
          if ( TRUE == v6_instance )
          {
            dsat_qcrmcall_info.v6_instance = RMNET_INSTANCE_MAX;
            DS_AT_MSG0_HIGH(" QCRMCALL V6 Failed");
          }
          if ( ( dsat_qcrmcall_info.v4_instance == RMNET_INSTANCE_MAX ) && 
               ( dsat_qcrmcall_info.v6_instance == RMNET_INSTANCE_MAX ) )
  
          {
             result = dsat_send_cme_error(DSAT_CME_UNKNOWN);
          }
        }
        /* When Stop call is triggered */
        if ( (dsat_qcrmcall_info.state == DSAT_QCRMCALL_STOP_INIT ) ||
             (dsat_qcrmcall_info.state == DSAT_QCRMCALL_ABORT ))
        {
          result = DSAT_ASYNC_EVENT;
          if ( TRUE == v4_instance )
          {
            dsat_qcrmcall_info.v4_instance = RMNET_INSTANCE_MAX;
            DS_AT_MSG0_HIGH(" QCRMCALL V4 Disonnected");
          }
          if ( TRUE == v6_instance )
          {
            dsat_qcrmcall_info.v6_instance = RMNET_INSTANCE_MAX;
            DS_AT_MSG0_HIGH(" QCRMCALL V6 Disonnected");
          }
          if ( ( dsat_qcrmcall_info.v4_instance == RMNET_INSTANCE_MAX ) && 
               ( dsat_qcrmcall_info.v6_instance == RMNET_INSTANCE_MAX ) )
          {
            result = DSAT_OK;
          }
        }
        /* Clean up as required */
        if ( ( dsat_qcrmcall_info.v4_instance == RMNET_INSTANCE_MAX ) && 
             ( dsat_qcrmcall_info.v6_instance == RMNET_INSTANCE_MAX ) )
        {
          dsat_iface_event_deregister(NULL); 
          memset(&dsat_qcrmcall_info,0x0,sizeof(dsat_qcrmcall_info));
          dsat_qcrmcall_info.v4_instance = RMNET_INSTANCE_MAX;
          dsat_qcrmcall_info.v6_instance = RMNET_INSTANCE_MAX;
        }
      }
     break;
    case IFACE_CONFIGURING_EV:
      {
        dsm_item_type *res_buff_ptr = NULL;
        if (dsat_qcrmcall_info.state == DSAT_QCRMCALL_COMING_UP )
        {
          if ( (TRUE == v4_instance ) && 
               ( dsat_qcrmcall_info.v4_instance != RMNET_INSTANCE_MAX ))
          {
            res_buff_ptr = dsat_dsm_new_buffer(DSM_DS_SMALL_ITEM_POOL, FALSE);
            res_buff_ptr->used = snprintf((char *)res_buff_ptr->data_ptr,
                                           res_buff_ptr->size,
                                           "$QCRMCALL: %d, V4",
           (int)(1+RMNET_GET_LAPTOP_INSTANCE(dsat_qcrmcall_info.v4_instance)));
                dsat_qcrmcall_info.v4_instance = RMNET_INSTANCE_MAX;
            (void)dsatcmdp_send_respose_to_te(res_buff_ptr, DSAT_COMPLETE_RSP);

            DS_AT_MSG0_HIGH(" QCRMCALL V4 Connected");
          }
          if ( (TRUE == v6_instance ) &&
               (dsat_qcrmcall_info.v6_instance != RMNET_INSTANCE_MAX ))
          {
            res_buff_ptr = dsat_dsm_new_buffer(DSM_DS_SMALL_ITEM_POOL, FALSE);
            res_buff_ptr->used = snprintf((char *)res_buff_ptr->data_ptr,
                                           res_buff_ptr->size,
                                           "$QCRMCALL: %d, V6",(int)
               (1+RMNET_GET_LAPTOP_INSTANCE(dsat_qcrmcall_info.v6_instance)));
            (void)dsatcmdp_send_respose_to_te(res_buff_ptr, DSAT_COMPLETE_RSP);

            dsat_qcrmcall_info.v6_instance = RMNET_INSTANCE_MAX;
            DS_AT_MSG0_HIGH(" QCRMCALL V6 Connected");
          }
        }
        if ( ( dsat_qcrmcall_info.v4_instance == RMNET_INSTANCE_MAX ) && 
             ( dsat_qcrmcall_info.v6_instance == RMNET_INSTANCE_MAX ) )
        {
          dsat_iface_event_deregister(NULL);
          memset(&dsat_qcrmcall_info,0x0,sizeof(dsat_qcrmcall_info));
          dsat_qcrmcall_info.v4_instance = RMNET_INSTANCE_MAX;
          dsat_qcrmcall_info.v6_instance = RMNET_INSTANCE_MAX;
          result = DSAT_OK;
        }
      }
      break;
    default:
      DS_AT_MSG0_MED("Igorning other events");
    
  }

  return result;
}


/*===========================================================================
FUNCTION   DSAT_IFACE_EVENT_REGISTER()

DESCRIPTION
  This function is used to register events on a particular IFACE including NULL Iface. 

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

boolean dsat_iface_event_register(ps_iface_type *iface_ptr)
{

 DS_AT_MSG1_HIGH("Registering IFACE UP, DOWN, ROUTEABLE, CFG for IFACE [0x%x]", iface_ptr);

 
  dsat_qcrmcall_info.qcrmcall_iface_coming_up_buff_ptr =
    ps_iface_alloc_event_cback_buf( dsatvend_iface_ev_cb, NULL );
  ASSERT(dsat_qcrmcall_info.qcrmcall_iface_coming_up_buff_ptr);

  dsat_qcrmcall_info.qcrmcall_iface_down_buff_ptr =
    ps_iface_alloc_event_cback_buf( dsatvend_iface_ev_cb, NULL );
  ASSERT(dsat_qcrmcall_info.qcrmcall_iface_down_buff_ptr);

  dsat_qcrmcall_info.qcrmcall_iface_configuring_buf_ptr =
    ps_iface_alloc_event_cback_buf( dsatvend_iface_ev_cb, NULL );
  ASSERT(dsat_qcrmcall_info.qcrmcall_iface_configuring_buf_ptr);


  if(0 != ps_iface_event_cback_reg( iface_ptr,
                                    IFACE_COMING_UP_EV,
                                   dsat_qcrmcall_info.qcrmcall_iface_coming_up_buff_ptr ) )
  {
    DS_AT_MSG0_ERROR("Couldn't register for DOWN Request Iface event!");
    ASSERT(0);
  }

  if(0 != ps_iface_event_cback_reg( iface_ptr,
                                    IFACE_DOWN_EV,
                                   dsat_qcrmcall_info.qcrmcall_iface_down_buff_ptr ) )
  {
    DS_AT_MSG0_ERROR("Couldn't register for DOWN Request Iface event!");
    ASSERT(0);
  }

  if(0 != ps_iface_event_cback_reg(iface_ptr,
                                   IFACE_CONFIGURING_EV,
                                   dsat_qcrmcall_info.qcrmcall_iface_configuring_buf_ptr))
  {
    DS_AT_MSG0_ERROR("Couldn't register for rmnet iface configuring event!");
    ASSERT( 0 );
  }

  return TRUE;
}

/*===========================================================================
FUNCTION   DSAT_IFACE_EVENT_DEREGISTER()

DESCRIPTION
  This function is used to register events on a particular IFACE including NULL Iface. 

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

void  dsat_iface_event_deregister(ps_iface_type *iface_ptr)
{

  if (NULL != dsat_qcrmcall_info.qcrmcall_iface_coming_up_buff_ptr )
  {
    DS_AT_MSG1_HIGH("De-Registering IFACE UP, DOWN, ROUTEABLE, CFG for IFACE [0x%x]", iface_ptr);

    ps_iface_event_cback_dereg( iface_ptr,
                                IFACE_COMING_UP_EV,
                                dsat_qcrmcall_info.qcrmcall_iface_coming_up_buff_ptr
                              );
    ps_iface_free_event_cback_buf(dsat_qcrmcall_info.qcrmcall_iface_coming_up_buff_ptr);

    ps_iface_event_cback_dereg( iface_ptr,
                                IFACE_DOWN_EV,
                                dsat_qcrmcall_info.qcrmcall_iface_down_buff_ptr 
                               );
    ps_iface_free_event_cback_buf(dsat_qcrmcall_info.qcrmcall_iface_down_buff_ptr);

    ps_iface_event_cback_dereg(iface_ptr,
                               IFACE_CONFIGURING_EV,
                               dsat_qcrmcall_info.qcrmcall_iface_configuring_buf_ptr
                              );
    ps_iface_free_event_cback_buf(dsat_qcrmcall_info.qcrmcall_iface_configuring_buf_ptr);
  }

}

/*===========================================================================
FUNCTION   DSAT_GET_DEFAULT_PROFILE_NUMBER

DESCRIPTION
  This function is used to retreive default profile number. 

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

void dsat_get_default_profile_number
(
  uint8                        profile_type,
  uint8                        profile_family,
  uint16                      *default_profile_num
)
{
  ds_profile_status_etype           profile_status;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    
  profile_status = DS_PROFILE_REG_RESULT_SUCCESS;
  *default_profile_num = 0;

  /*-------------------------------------------------------------------------
                       Call the API to get the default profile number.
    -------------------------------------------------------------------------*/
  profile_status = 
    ds_profile_get_default_profile_num((ds_profile_tech_etype)profile_type, 
                                        (uint32)profile_family,
                                        default_profile_num);
  if (profile_status != DS_PROFILE_REG_RESULT_SUCCESS && 
      profile_status != DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE)
  {
    DS_AT_MSG2_ERROR ("Get Default Profile for type %d failed. Error %d", 
               profile_type, profile_status);
  }

}

/*===========================================================================

FUNCTION QCRMCALL_VALIDATE_PARAMS

DESCRIPTION
  This functions validates the parameters received. 

DEPENDENCIES

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR:        syntax error.
    DSAT_ASYNC_CMD          : if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
boolean qcrmcall_validate_input_params
(
  const tokens_struct_type *tok_ptr, 
  rmnet_meta_sm_network_info_type *sni_params
)
{
  #define QCRMCALL_ACT_3GPP2  1
  #define QCRMCALL_ACT_3GPP   2
  #define QCRMCALL_IP_V4      1
  #define QCRMCALL_IP_V6      2
  #define QCRMCALL_IP_V4V6    3
    
  dsat_num_item_type  temp_data = 0;
  uint16  def_profile_num = 0;
  uint32  len = 0;
/*------------------------------------------------------------------------*/

  ASSERT(NULL != sni_params );

  if ( (NA|EQ|AR) == tok_ptr->op )
  {
    /* Set Defaults */
     dsat_get_default_profile_number(DS_PROFILE_TECH_3GPP,
                                                   DS_PROFILE_3GPP_RMNET_PROFILE_FAMILY,
                                                   &def_profile_num);
     sni_params->umts_profile_num = def_profile_num;
     dsat_get_default_profile_number(DS_PROFILE_TECH_3GPP2,
                                                   DS_PROFILE_3GPP_RMNET_PROFILE_FAMILY,
                                                   &def_profile_num);
    sni_params->cdma_profile_num = def_profile_num;
    sni_params->tech_pref = WWAN_GROUP;
    sni_params->ip_family_pref = QMI_AF_INET;

    /* Retreive optional parameter IP type */
    if ( VALID_TOKEN(2))
    {
      if ( dsatutil_atoi( &temp_data, 
                       tok_ptr->arg[2], 10) != ATOI_OK )
      {
        DS_AT_MSG0_ERROR("Ascii to integer conversion failed");
        return FALSE;
      }
      if ( (QCRMCALL_IP_V4V6 < temp_data) || (temp_data < QCRMCALL_IP_V4) )
      {
        DS_AT_MSG1_ERROR("Invalid IP Type received [%d]",temp_data);
        return FALSE; 
      }
      switch (temp_data)
      {
        case QCRMCALL_IP_V4:         /* IPv4        */
          sni_params->ip_family_pref = QMI_AF_INET;
          break;          
        case QCRMCALL_IP_V6:        /* IPv6        */
          sni_params->ip_family_pref = QMI_AF_INET6;
          break;
        case QCRMCALL_IP_V4V6:       /* IPV4V6 */
          sni_params->ip_family_pref = QMI_AF_INET_4_6;
          break;
        default:
          sni_params->ip_family_pref = QMI_AF_INET;
      }
    }

    /* Retreive Tech Preference */
    if ( VALID_TOKEN(3))
    {
      if ( dsatutil_atoi( &temp_data, 
                       tok_ptr->arg[3], 10) != ATOI_OK )
      {
        DS_AT_MSG0_ERROR("Ascii to integer conversion failed");
        return FALSE;
      }
      if ( QCRMCALL_ACT_3GPP2 == temp_data )
      {
        sni_params->tech_pref = CDMA_SN_IFACE;
      }
      else if ( QCRMCALL_ACT_3GPP == temp_data )
      {
        sni_params->tech_pref = UMTS_IFACE;
      }
      else
      {
        DS_AT_MSG1_ERROR("Invalid Tech Pref [%d]", temp_data);
        return FALSE;
      }
    }
    /* Retreive optional parameter UMTS Profile number */
    if ( VALID_TOKEN(4))
    {
      if ( dsatutil_atoi( &temp_data, 
                       tok_ptr->arg[4], 10) != ATOI_OK )
      {
        DS_AT_MSG0_ERROR("Ascii to integer conversion failed");
        return FALSE;
      }
      if ( (temp_data == 0 ) || 
           (DS_UMTS_MAX_PDP_PROFILE_NUM < temp_data ) )
      {
        DS_AT_MSG1_ERROR("Invalid UMTS Profile [%d]", temp_data);
        return FALSE;
      }
      sni_params->umts_profile_num = temp_data;
    }
    /* Retreive optional parameter CDMA Profile number */
    if ( VALID_TOKEN(5))
    {
      if ( dsatutil_atoi( &temp_data, 
                       tok_ptr->arg[5], 10) != ATOI_OK )
      {
        DS_AT_MSG0_ERROR("Ascii to integer conversion failed");
        return FALSE;
      }
      sni_params->cdma_profile_num = temp_data;
    }
    /* Retreive optional parameterAPN Name */
    if ( VALID_TOKEN(6))
    {
      len = strlen((char *)tok_ptr->arg[6]);
      if ( len > ( 2 + DS_UMTS_MAX_APN_STRING_LEN ))
      {
        DS_AT_MSG1_ERROR("APN is too long [%d]",len);
        return FALSE;
      }
      if ( '"' == *tok_ptr->arg[6]) /* Strip Quotes */
      {
        if(!dsatutil_strip_quotes_out((const byte*)tok_ptr->arg[6],
                                      (byte*)tok_ptr->arg[6], len))
        {
          return FALSE;        
        }
        (void) dsatutil_memscpy((void*)sni_params->apn.name,255,
                                (void*)tok_ptr->arg[6],(len-2));
        
        sni_params->apn.length = len-2;
      }
      else
      {
        return FALSE;
      }

    }

    
    return TRUE;
    
  }

  return FALSE;
}/* qcrmcall_validate_input_params */

/*===========================================================================

FUNCTION DSATVEND_QCRMCALL_ABORT_CMD_HANDLER

DESCRIPTION
  This function is an abort command handler, which stops the current originate 
  request which if it is still in progress. 

DEPENDENCIES

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR:        syntax error.
    DSAT_ASYNC_CMD          : if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
boolean dsatvend_qcrmcall_abort_cmd_handler
(
  const dsati_cmd_type *cmd_table_entry_ptr  /* Ptr to cmd entry in table. */
)
{
  /* If there is still an active pending call, abort the call */

  DS_AT_MSG3_HIGH("QCRMCALL Abort Handler State [%d] V4 Instance [%d] V6 Instance [%d]",
                    dsat_qcrmcall_info.state,
                    dsat_qcrmcall_info.v4_instance,
                    dsat_qcrmcall_info.v6_instance);
                    
  if ( ( dsat_qcrmcall_info.state == DSAT_QCRMCALL_START_INIT ) ||
       (dsat_qcrmcall_info.state == DSAT_QCRMCALL_COMING_UP))
  {

    dsat_qcrmcall_info.state = DSAT_QCRMCALL_ABORT;

    if ( dsat_qcrmcall_info.v4_instance != RMNET_INSTANCE_MAX )
    {
      DS_AT_MSG1_HIGH("Abort V4 QCRMCall on instance %d",
                          dsat_qcrmcall_info.v4_instance);
      rmnet_meta_sm_post_event(dsat_qcrmcall_info.v4_instance,
                               RMNET_META_SM_RM_LINK_DOWN_EV);
    }

    if ( dsat_qcrmcall_info.v6_instance != RMNET_INSTANCE_MAX )
    {
      DS_AT_MSG1_HIGH("Abort V6 QCRMCall on instance %d",
                          dsat_qcrmcall_info.v6_instance);
      rmnet_meta_sm_post_event(dsat_qcrmcall_info.v6_instance,
                               RMNET_META_SM_RM_LINK_DOWN_EV);
    }
    
  }

  return TRUE;
  
}/* dsatvend_qcrmcall_abort_cmd_handler */


/*===========================================================================

FUNCTION DSATVEND_EXEC_QCRMCALL_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes $QCRMCALL command.
  $QCRMCALL command is used to trigger a RmNet call via AT command. 
  It just passes on the Start Network Interface parameters into RmNet to
  bring up the call. 

  $QCRMCALL =<action>, <instance>, [<IP type>, <[Act],<umts_profile>,<cdma_profile>,<APN>]

   Response:
   $QCRMCALL:<Instance>, <V4>
   $QCRMCALL:<Instance>, <V6> 
   OK

DEPENDENCIES

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR:    syntax error.
    DSAT_ASYNC_CMD : if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatvend_exec_qcrmcall_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  #define QCRMCALL_ACTION_START 1
  #define QCRMCALL_ACTION_STOP  0
  
  rmnet_meta_sm_network_info_type  sni_params;
  dsat_num_item_type               temp_instance = 0;
  rmnet_instance_e_type  v4_rmnet_instance = RMNET_INSTANCE_MAX;
  rmnet_instance_e_type  v6_rmnet_instance = RMNET_INSTANCE_MAX;
  dsat_num_item_type action = 0;
  dsat_error_info_s_type   err_info;

  err_info.errval = DSAT_ERR_NO_ERROR;
  err_info.arg_num = 0;

/* -----------------------------------------------------------------------------*/


  /*========================Few Basic Concepts ======================================
     *
     *  Instance concept: 
     *     User is shown about the available Laptop Instances only. Rmnet has a combination of
     *       Embedded Instances Followed by Laptop Instances. 
     *      Each Laptop instance which is displayed to user actually is split into V4 and V6 internally. 
     *       If users chooses Laptop Instance 1 which is equvalent to 
     *
     *             1 + (1 - RMNET_LAPTOP_PROC_MIN ) --> This is for V4 (Laptop proc min in Rmnet starts from 0)
     *             1 + (RMNET_LAPTOP_PROC_MIN ) --> V6
     *
     *       User deals instances interms of 1,2,3 etc the same is mapped as
     *            RMNET_LAPTOP_PROC_MIN, 1+LAPTOP_PROC_MIN, 2+LAPTOP_PROC_MIN etc.
     *
     *       There are two global variables. 
     *         1. dsat_qcrmcall_info --> This is per command and has the state and v4, v6 instance details
     *             which is used to send the responses precisely. 
     *
     *         2. dsat_qcrmcall_global_info --> This has the global info of all calls made through AT command. 
     *             This is used to especially allow the stop calls. Only calls made by AT are allowed to terminate 
     *             by AT. 
     *
     *         Action and Instance Parameters are mandatory and rest all optionals with a default behavior. 
     *         IP Type default is V4. 
     *
     *         For a call bring up, two sequneces are possible
     *         1. IFACE COMING UP --> IFACE CONFIGURING --> This is treated as call connected. 
     *         2. IFACE COMING UP --> IFACE Down --> Call oriniation Failed. 
     *
     *         Always V4 is Odd numbered index interms of RmNet and V6 is Odd. 
     *
    =============================================================================  */
  /* Validate Input Params for Write command */

  if ( (NA|EQ|AR) == tok_ptr->op )
  {
    memset(&sni_params,0x0,sizeof(rmnet_meta_sm_network_info_type));
    
    /* Action Param; Mandatory */ 
    err_info.arg_num = 0;
    if ( VALID_TOKEN(0))
    {
      if ( dsatutil_atoi( &action, 
                       tok_ptr->arg[0], 10) != ATOI_OK )
      {
        err_info.errval = DSAT_ERR_ATOI_CONVERSION_FAILURE;
        goto send_error;
      }
  
      if( (QCRMCALL_ACTION_START != action ) &&
          (QCRMCALL_ACTION_STOP != action  ))
      {
        err_info.errval = DSAT_ERR_PARAMETER_OUT_OF_RANGE;
        goto send_error;
      }
    }
    else
    { /* Action is mandatory */
      err_info.errval = DSAT_ERR_PARAMETER_MANDATORY;
      goto send_error;
    }

    err_info.arg_num = 1;
    if ( VALID_TOKEN(1))
    {
      if ( ( dsatutil_atoi( &temp_instance, 
                       tok_ptr->arg[1], 10) != ATOI_OK ) || ( temp_instance == 0))
      {
        err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
        goto send_error;
      }
      /* Actual laptop instance will be one more by RMNET_INSTANCE_LAPTOP_PROC_MIN  
             First Rmnet Laptop instance starts from 0 + RMNET_INSTANCE_LAPTOP_PROC_MIN 
             But ATCoP instances starts from 1 to RMNET_NUM_LAPTOP_INSTANCES */
      v4_rmnet_instance = (rmnet_instance_e_type)(2*( temp_instance - 1 ))+ 
                                        RMNET_INSTANCE_LAPTOP_PROC_MIN;
      v6_rmnet_instance = v4_rmnet_instance +1;

      /* Check if the RmNet instance is a valid Laptop instance. */
      if ( (v4_rmnet_instance >= (qmi_get_num_instances()*2)) ||
           (!RMNET_IS_VALID_LAPTOP_INSTANCE(v4_rmnet_instance)) ||
           (v6_rmnet_instance >= ((qmi_get_num_instances()*2)+1)) ||
           (!RMNET_IS_VALID_LAPTOP_INSTANCE(v6_rmnet_instance)))
      {
        DS_AT_MSG1_ERROR("Invalid Laptop Instance [%d]",v4_rmnet_instance);
        return DSAT_ERROR;
      }

      if ( FALSE == rmnet_meta_sm_verify_dtr_high_per_instance(v4_rmnet_instance))
      {
        DS_AT_MSG1_ERROR(" DTR is not High on instance [%d]",v4_rmnet_instance);
        return DSAT_ERROR;
      }
    }
    else
    {
      err_info.errval = DSAT_ERR_PARAMETER_MANDATORY;
      goto send_error;
    }
  
    if ( FALSE == qcrmcall_validate_input_params( tok_ptr, &sni_params) )
    {
       err_info.errval = DSAT_ERR_INVALID_TOKENS;
       goto send_error;
    }
  
    /* Register for Iface events on NULL Iface */
    
    if ( FALSE == dsat_iface_event_register(NULL))
    {
      DS_AT_MSG1_ERROR(" Registration for IFACE on %d failed",NULL);
      return DSAT_ERROR;
    }
  
    if ( QCRMCALL_ACTION_START == action )
    {
      sni_params.call_type = RMNET_CALL_TYPE_LAPTOP; 
      dsat_qcrmcall_info.state = DSAT_QCRMCALL_START_INIT;
      dsat_qcrmcall_info.v4_instance = RMNET_INSTANCE_MAX;
      dsat_qcrmcall_info.v6_instance = RMNET_INSTANCE_MAX;
      switch ( sni_params.ip_family_pref )
      {
        case QMI_AF_INET6:
        {
          if ( FALSE == rmnet_meta_sm_in_call(v6_rmnet_instance))
          {
            dsat_qcrmcall_info.v6_instance = v6_rmnet_instance;
            DS_AT_MSG1_HIGH("Start V6 QCRMCall on instance %d",v6_rmnet_instance);
            rmnet_meta_sm_set_network_cfg_params(v6_rmnet_instance,
                                             &sni_params);
            rmnet_meta_sm_post_event(v6_rmnet_instance,
                                         RMNET_META_SM_RM_DEV_START_PKT_EV);
           }
           else
           {
             DS_AT_MSG1_HIGH("V6 Call already up on Instance[%d]",
                                        (int)((1+v6_rmnet_instance/2)));
           }
        }
          break;
        case QMI_AF_INET_4_6:
          {
            boolean v4_call = rmnet_meta_sm_in_call(v4_rmnet_instance);
            boolean v6_call = rmnet_meta_sm_in_call(v6_rmnet_instance);
            if ( ( FALSE == v4_call) || ( FALSE == v6_call) )
            {
                if ( FALSE == v4_call )
              {
                sni_params.ip_family_pref = QMI_AF_INET;
                dsat_qcrmcall_info.v4_instance = v4_rmnet_instance;
                rmnet_meta_sm_set_network_cfg_params(v4_rmnet_instance,
                                                 &sni_params);
                DS_AT_MSG1_HIGH("Start V4 QCRMCall on instance %d",v4_rmnet_instance);
                rmnet_meta_sm_post_event(v4_rmnet_instance,
                                             RMNET_META_SM_RM_DEV_START_PKT_EV);
              }
              if ( FALSE == v6_call )
              {
                sni_params.ip_family_pref = QMI_AF_INET6;
                dsat_qcrmcall_info.v6_instance = v6_rmnet_instance;
                rmnet_meta_sm_set_network_cfg_params(v6_rmnet_instance,
                                                &sni_params);
                DS_AT_MSG1_HIGH("Start V6 QCRMCall on instance %d",v6_rmnet_instance);
                rmnet_meta_sm_post_event(v6_rmnet_instance,
                                             RMNET_META_SM_RM_DEV_START_PKT_EV);
              }
            }
            else
            {
              DS_AT_MSG1_HIGH(" V4, V6 Calls are alread up on instance " ,(int)((1+v4_rmnet_instance/2)));
            }
          }
          break;
        case QMI_AF_INET:
        default:
        {
          if (FALSE == rmnet_meta_sm_in_call(v4_rmnet_instance))
          {
            dsat_qcrmcall_info.v4_instance = v4_rmnet_instance;
            rmnet_meta_sm_set_network_cfg_params(v4_rmnet_instance,
                                             &sni_params);
            DS_AT_MSG1_HIGH("Start V4 QCRMCall on instance %d",v4_rmnet_instance);
            rmnet_meta_sm_post_event(v4_rmnet_instance,
                                         RMNET_META_SM_RM_DEV_START_PKT_EV);
          }
          else
          {
            DS_AT_MSG1_HIGH("V4 Call already up on Instance[%d]",
                                       (int)((1+v4_rmnet_instance/2)));
          }
        }
      }
      
      if ( ( dsat_qcrmcall_info.v4_instance == RMNET_INSTANCE_MAX ) && 
           ( dsat_qcrmcall_info.v6_instance == RMNET_INSTANCE_MAX ) )
      {
        dsat_iface_event_deregister(NULL); 
        memset(&dsat_qcrmcall_info,0x0,sizeof(dsat_qcrmcall_info));
        dsat_qcrmcall_info.v4_instance = RMNET_INSTANCE_MAX;
        dsat_qcrmcall_info.v6_instance = RMNET_INSTANCE_MAX;
        return DSAT_OK;
      }
      else
      {
        return DSAT_ASYNC_CMD;
      }
    }
    else
    {
      dsat_qcrmcall_info.state = DSAT_QCRMCALL_STOP_INIT;
      dsat_qcrmcall_info.v4_instance = RMNET_INSTANCE_MAX;
      dsat_qcrmcall_info.v6_instance = RMNET_INSTANCE_MAX;
      /* Calls originated from ATCoP are only terminated  */
      switch ( sni_params.ip_family_pref )
      {
        case QMI_AF_INET6:
        {
          if((2*(RMNET_GET_LAPTOP_INSTANCE(v6_rmnet_instance))) < RMNET_NUM_LAPTOP_INSTANCES)
          {
            if ( (rmnet_meta_sm_in_call(v6_rmnet_instance)) && 
                 (dsat_qcrmcall_global_info.iface_ptr[1+(2*(RMNET_GET_LAPTOP_INSTANCE(v6_rmnet_instance)))]))
            {
              dsat_qcrmcall_info.v6_instance = v6_rmnet_instance;
              DS_AT_MSG1_HIGH("Stop V6 QCRMCall on instance %d",v6_rmnet_instance);
              rmnet_meta_sm_post_event(v6_rmnet_instance,
                                       RMNET_META_SM_RM_LINK_DOWN_EV);
            }
          }
        }
          break;
        case QMI_AF_INET_4_6:
          if((2*(RMNET_GET_LAPTOP_INSTANCE(v4_rmnet_instance))) < (RMNET_NUM_LAPTOP_INSTANCES+1))
          {
            if ( (rmnet_meta_sm_in_call(v4_rmnet_instance)) && 
                 (dsat_qcrmcall_global_info.iface_ptr[2*(RMNET_GET_LAPTOP_INSTANCE(v4_rmnet_instance))]))

            {
              dsat_qcrmcall_info.v4_instance = v4_rmnet_instance;
              DS_AT_MSG1_HIGH("Stop V4 QCRMCall on instance %d",v4_rmnet_instance);
              rmnet_meta_sm_post_event(v4_rmnet_instance,
                                       RMNET_META_SM_RM_LINK_DOWN_EV);
            }
          }
          if((2*(RMNET_GET_LAPTOP_INSTANCE(v6_rmnet_instance))) < RMNET_NUM_LAPTOP_INSTANCES)
          {
            if ( (rmnet_meta_sm_in_call(v6_rmnet_instance)) && 
                 (dsat_qcrmcall_global_info.iface_ptr[1+(2*(RMNET_GET_LAPTOP_INSTANCE(v6_rmnet_instance)))]))
            {
              dsat_qcrmcall_info.v6_instance = v6_rmnet_instance;
              DS_AT_MSG1_HIGH("Stop V6 QCRMCall on instance %d",v6_rmnet_instance);
              rmnet_meta_sm_post_event(v6_rmnet_instance,
                                       RMNET_META_SM_RM_LINK_DOWN_EV);
            }
          }
          break;
        case QMI_AF_INET:
        default:
          {
            if((2*(RMNET_GET_LAPTOP_INSTANCE(v4_rmnet_instance))) < (RMNET_NUM_LAPTOP_INSTANCES+1))
            {
              if ( (rmnet_meta_sm_in_call(v4_rmnet_instance)) && 
                   (dsat_qcrmcall_global_info.iface_ptr[2*(RMNET_GET_LAPTOP_INSTANCE(v4_rmnet_instance))]))
              {
                dsat_qcrmcall_info.v4_instance = v4_rmnet_instance;
                DS_AT_MSG1_HIGH("Stop V4 QCRMCall on instance %d",v4_rmnet_instance);
                rmnet_meta_sm_post_event(v4_rmnet_instance,
                                         RMNET_META_SM_RM_LINK_DOWN_EV);
              }
            }
          }
      }

      if ( ( dsat_qcrmcall_info.v4_instance == RMNET_INSTANCE_MAX ) && 
           ( dsat_qcrmcall_info.v6_instance == RMNET_INSTANCE_MAX ) )
      {
        DS_AT_MSG1_ERROR("No Active V4/V6 call at Instance [%d]",(1+ RMNET_GET_LAPTOP_INSTANCE(v4_rmnet_instance) ));
        dsat_iface_event_deregister(NULL); 
        memset(&dsat_qcrmcall_info,0x0,sizeof(dsat_qcrmcall_info));
        dsat_qcrmcall_info.v4_instance = RMNET_INSTANCE_MAX;
        dsat_qcrmcall_info.v6_instance = RMNET_INSTANCE_MAX;
        return DSAT_ERROR;
      }
      else
      {
        return DSAT_ASYNC_CMD;
      }
    }
  }
  else if ( (NA|EQ|QU) == tok_ptr->op )
  {
    rmnet_instance_e_type rmnet_instance = RMNET_INSTANCE_MAX;

    /* Report Action Data */
    res_buff_ptr->used = (word)snprintf((char *)res_buff_ptr->data_ptr,
                                        res_buff_ptr->size,
                                        "$QCRMCALL: (0-1),(");

   /* Report available instances  */
   for ( rmnet_instance = RMNET_INSTANCE_MIN; 
         rmnet_instance < (qmi_get_num_instances()*2);rmnet_instance+=2 )
   {
     if ( (RMNET_IS_VALID_LAPTOP_INSTANCE(rmnet_instance)) && 
          (TRUE == rmnet_meta_sm_verify_dtr_high_per_instance(rmnet_instance)))
     {
       res_buff_ptr->used += (word)snprintf(
                        (char *)&res_buff_ptr->data_ptr[res_buff_ptr->used], 
                        res_buff_ptr->size -  res_buff_ptr->used,
                                      "%d,",(int)1+RMNET_GET_LAPTOP_INSTANCE(rmnet_instance));
     }
   }

   if ( ',' == res_buff_ptr->data_ptr[res_buff_ptr->used - 1] )
   {
     res_buff_ptr->used--;
   }
   res_buff_ptr->used += (word)snprintf(
                        (char *)&res_buff_ptr->data_ptr[res_buff_ptr->used], 
                        res_buff_ptr->size -  res_buff_ptr->used,
                                        ")");


   /*Report PDP Type,  AcT, UMTS, CDMA profile numbers*/

   res_buff_ptr->used += (word)snprintf(
                         (char *)&res_buff_ptr->data_ptr[res_buff_ptr->used], 
                          res_buff_ptr->size -  res_buff_ptr->used,
                          ",(1-3),(1-2),(1-%d),,",DS_UMTS_MAX_PDP_PROFILE_NUM); 
    return DSAT_OK;
  }
  else if ( tok_ptr->op == (NA|QU))
  {
    uint32 rmnet_instance = 0;

    res_buff_ptr->used = 0;
    for ( rmnet_instance = 0; rmnet_instance <=RMNET_NUM_LAPTOP_INSTANCES;++ rmnet_instance )
    {
      if ( dsat_qcrmcall_global_info.iface_ptr[rmnet_instance] != NULL )
      {

        res_buff_ptr->used += (word)snprintf(
                          (char *)&res_buff_ptr->data_ptr[res_buff_ptr->used], 
                          res_buff_ptr->size -  res_buff_ptr->used,
                          "$QCRMCALL: %d",(int)(1+(rmnet_instance/2)));
      if ( ( 0 == (rmnet_instance %2 ))
)
      {
        res_buff_ptr->used += (word)snprintf(
                          (char *)&res_buff_ptr->data_ptr[res_buff_ptr->used], 
                          res_buff_ptr->size -  res_buff_ptr->used,
                          ", V4\n");
      }
      else
      {
        res_buff_ptr->used += (word)snprintf(
                          (char *)&res_buff_ptr->data_ptr[res_buff_ptr->used], 
                          res_buff_ptr->size -  res_buff_ptr->used,
                          ", V6\n");

      }

      }
    }
    if ( res_buff_ptr->used > 0)
    {
      res_buff_ptr->data_ptr[res_buff_ptr->used - 1]= '\0';
    }
    return DSAT_OK;
  }

send_error:
  if( err_info.errval == DSAT_ERR_INVALID_TOKENS)
  {
    DS_AT_MSG1_ERROR("Error:%d",err_info.errval);
  }
  else
  {
    DS_AT_MSG2_ERROR("Error:%d, arg_num:%d",err_info.errval,err_info.arg_num);
  }

  return DSAT_ERROR;
}/*  dsatvend_exec_qcrmcall_cmd */

/*===========================================================================

FUNCTION DSATVEND_EXEC_QCSIMT_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes at$qcsimt command.
  This function returns SIM TYPE
    * RUIM
    * USIM
    * SIM

DEPENDENCIES
  None
  
RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR :    if there was any problem in executing the command
    DSAT_OK :       if it is a success and execution completed.
  
SIDE EFFECTS
  None

===========================================================================*/
/* ARGSUSED */

dsat_result_enum_type dsatvend_exec_qcsimt_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type result = DSAT_OK;
  if ( tok_ptr->op == (NA) ||
       tok_ptr->op == (NA|QU))
  {
    if ( dsat_qcsimapp_info[dsat_get_qcsimapp_info_idx()].app_type == MMGSDI_APP_USIM)
    {
      res_buff_ptr->used = (word) snprintf ( (char *) res_buff_ptr->data_ptr,
                                               res_buff_ptr->size,
                                               "$QCSIMT: USIM");
    }
    else if( dsat_qcsimapp_info[dsat_get_qcsimapp_info_idx()].app_type == MMGSDI_APP_SIM)
    {
      res_buff_ptr->used = (word) snprintf ( (char *) res_buff_ptr->data_ptr,
                                               res_buff_ptr->size,
                                               "$QCSIMT: SIM");
    }
    else if( dsat_qcsimapp_info[dsat_get_qcsimapp_info_idx()].app_type == MMGSDI_APP_RUIM)
    {
      res_buff_ptr->used = (word) snprintf ( (char *) res_buff_ptr->data_ptr,
                                               res_buff_ptr->size,
                                               "$QCSIMT: RUIM");
    }
    else
    {
        result = DSAT_ERROR;
    }   
  }
  else
  {
    /* other operations are not supported */
    result = DSAT_ERROR;
  }
  return result;
}

/*===========================================================================

FUNCTION DSATVEND_EXEC_QCNSP_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes AT$QCNSP command.
  AT$QCSNT command is used to configure the 
  network selection mode.

DEPENDENCIES
  None

RETURN VALUE
  Returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR   : if there was any problem in executing the command
    DSAT_OK        :  if it is a success and execution completed.

SIDE EFFECTS
  None

===========================================================================*/
/* ARGSUSED */

dsat_result_enum_type dsatvend_exec_qcnsp_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{ 

  dsat_result_enum_type  result          = DSAT_ASYNC_CMD;
  dsat_num_item_type     mode_val        = DSAT_QCNSP_RAT_MODE_MAX;
  dsat_num_item_type     net_sel_mode    = DSAT_QCNSP_NET_SEL_MODE_MAX;
  dsat_num_item_type     acq_pri_order_pref  = DSAT_QCNSP_ACQ_ORDER_PREF_MAX;

  if (tok_ptr->op == (NA|EQ|AR))
  {		
    if(VALID_TOKEN(0) && (ATOI_OK == dsatutil_atoi( &mode_val, tok_ptr->arg[0], 10 )))
    {
       switch(mode_val)
       {
         case DSAT_QCNSP_RAT_MODE_AUTOMATIC:
           DSATUTIL_SET_VAL(DSAT_VENDOR_QCNSP_IDX,0,0,0,CM_MODE_PREF_AUTOMATIC,NUM_TYPE)
            break;
         case DSAT_QCNSP_RAT_MODE_GSM:
           DSATUTIL_SET_VAL(DSAT_VENDOR_QCNSP_IDX,0,0,0,CM_MODE_PREF_GSM_ONLY,NUM_TYPE)
            break;
         case DSAT_QCNSP_RAT_MODE_WCDMA:
           DSATUTIL_SET_VAL(DSAT_VENDOR_QCNSP_IDX,0,0,0,CM_MODE_PREF_WCDMA_ONLY,NUM_TYPE)
            break;
         case DSAT_QCNSP_RAT_MODE_LTE:
           DSATUTIL_SET_VAL(DSAT_VENDOR_QCNSP_IDX,0,0,0,CM_MODE_PREF_LTE_ONLY,NUM_TYPE)
           break;
         default:
            return DSAT_ERROR;
       }
    }
    else
    {
    	 DSATUTIL_SET_VAL(DSAT_VENDOR_QCNSP_IDX,0,0,0,CM_MODE_PREF_NO_CHANGE,NUM_TYPE)
    }
    if(VALID_TOKEN(1) && (ATOI_OK == dsatutil_atoi ( &net_sel_mode, tok_ptr->arg[1], 10 ) ))
    {
      if( (net_sel_mode == CM_NETWORK_SEL_MODE_PREF_AUTOMATIC) ||
          (net_sel_mode == CM_NETWORK_SEL_MODE_PREF_LIMITED_SRV) )
      {
         DSATUTIL_SET_VAL(DSAT_VENDOR_QCNSP_IDX,0,1,0,net_sel_mode,NUM_TYPE)
      }
      else
      {
        return DSAT_ERROR;
      }
    }
    else
    {
       DSATUTIL_SET_VAL(DSAT_VENDOR_QCNSP_IDX,0,1,0,CM_NETWORK_SEL_MODE_PREF_NO_CHANGE,NUM_TYPE)
    }
    if(VALID_TOKEN(2) && (ATOI_OK == dsatutil_atoi ( &acq_pri_order_pref, tok_ptr->arg[2], 10 ) ))
    {
      switch(acq_pri_order_pref)
      {
        case DSAT_QCNSP_ACQ_ORDER_PREF_AUTOMATIC:
          DSATUTIL_SET_VAL(DSAT_VENDOR_QCNSP_IDX,0,2,0,SYS_SYS_MODE_NONE,NUM_TYPE)
         break;
        case DSAT_QCNSP_ACQ_ORDER_PREF_GSM:
          DSATUTIL_SET_VAL(DSAT_VENDOR_QCNSP_IDX,0,2,0,SYS_SYS_MODE_GSM,NUM_TYPE)
         break;
        case DSAT_QCNSP_ACQ_ORDER_PREF_WCDMA:
          DSATUTIL_SET_VAL(DSAT_VENDOR_QCNSP_IDX,0,2,0,SYS_SYS_MODE_WCDMA,NUM_TYPE)
         break;
        case DSAT_QCNSP_ACQ_ORDER_PREF_LTE:
          DSATUTIL_SET_VAL(DSAT_VENDOR_QCNSP_IDX,0,2,0,SYS_SYS_MODE_LTE,NUM_TYPE)
         break;
        default:
	        return DSAT_ERROR;
       }
    }
    else
    {
      DSATUTIL_SET_VAL(DSAT_VENDOR_QCNSP_IDX,0,2,0,SYS_SYS_MODE_NONE,NUM_TYPE)
    }

    if(  mode_val != DSAT_QCNSP_RAT_MODE_MAX ||
         net_sel_mode  != DSAT_QCNSP_NET_SEL_MODE_MAX ||
         acq_pri_order_pref  != DSAT_QCNSP_ACQ_ORDER_PREF_MAX
      )
    {
      SET_PENDING(DSAT_VENDOR_QCNSP_IDX,0, DSAT_PENDING_QCNSP_GET_PRI_RAT)
      if ( FALSE == cm_ph_cmd_get_ph_info_per_subs(dsatcmif_ph_cmd_cb_func,
                                    NULL,
                                    dsatcm_client_id,
                                    dsat_get_current_subs_id()
                                    ) )
      {
        SET_PENDING(DSAT_VENDOR_QCNSP_IDX ,0, DSAT_PENDING_QCNSP_NONE)
        result = DSAT_ERROR;
      }
    }
    else
    {
      /*CM will not send events if ther is NO CHANGE*/
       result = DSAT_ERROR;
    }
  }
  else if (tok_ptr->op == (NA|QU))
  {
    SET_PENDING(DSAT_VENDOR_QCNSP_IDX,0, DSAT_PENDING_QCNSP_READ)
    if ( FALSE == cm_ph_cmd_get_ph_info_per_subs(dsatcmif_ph_cmd_cb_func,
                                    NULL,
                                    dsatcm_client_id,
                                    dsat_get_current_subs_id()
                                    ) )
    {
      SET_PENDING(DSAT_VENDOR_QCNSP_IDX ,0, DSAT_PENDING_QCNSP_NONE)
      result = DSAT_ERROR;
    }
  }
  else if (tok_ptr->op == (NA|EQ|QU))
  {
    res_buff_ptr->used = 
      (word) snprintf ((char*)res_buff_ptr->data_ptr, 
                              res_buff_ptr->size,
                              "$QCNSP: (0,1,2,6),"
                              "(0-2),"
                              "(0-3)" );
    result = DSAT_OK;
  }
  else
  {
    result = DSAT_ERROR;
  }
  
  return result;
}

/*===========================================================================

FUNCTION DSAVEND_PROCESS_QCNSP_CMD

DESCRIPTION
  This function process the response for the $QCNSP command on return from
  asynchronous processing. 
 
DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
.

SIDE EFFECTS
  None

===========================================================================*/

dsat_result_enum_type dsatvend_process_qcnsp_cmd 
(
  dsat_cmd_pending_enum_type cmd_type,
  ds_at_cm_ph_pref_type  *pref_mode    /* Network preference mode */
)
{
  dsat_result_enum_type     result                    = DSAT_ASYNC_CMD;
  dsm_item_type            *res_buff_ptr              = NULL;
  dsat_num_item_type        mode_pref                 = DSAT_QCNSP_RAT_MODE_MAX;
  dsat_num_item_type        network_sel_mode_pref     = DSAT_QCNSP_NET_SEL_MODE_MAX;
  dsat_num_item_type        acq_pri_order_pref        = DSAT_QCNSP_ACQ_ORDER_PREF_MAX;

  if( NULL == pref_mode)
  {
    return DSAT_ERROR;
  }
  switch( cmd_type )
  {
    case DSAT_PENDING_QCNSP_READ:
    {
      DS_AT_MSG3_HIGH("mode_pref=%d, network_sel_mode_pref=%d, acq_order_pref=%d",
      	                                         pref_mode->network_rat_mode_pref,
      	                                         pref_mode ->network_sel_mode_pref,
      	                                         pref_mode ->acq_pri_order_pref.acq_order[0]);
      res_buff_ptr = dsat_dsm_new_buffer( DSM_DS_SMALL_ITEM_POOL, FALSE );

      switch( pref_mode->network_rat_mode_pref )
      {
        case CM_MODE_PREF_AUTOMATIC:
          mode_pref = DSAT_QCNSP_RAT_MODE_AUTOMATIC;
          break;
        case CM_MODE_PREF_GSM_ONLY:
          mode_pref = DSAT_QCNSP_RAT_MODE_GSM;
          break;
        case CM_MODE_PREF_WCDMA_ONLY:
          mode_pref = DSAT_QCNSP_RAT_MODE_WCDMA;
          break;
        case CM_MODE_PREF_LTE_ONLY:
          mode_pref = DSAT_QCNSP_RAT_MODE_LTE;
          break;
        default:
          mode_pref = DSAT_QCNSP_RAT_MODE_MAX;
      }
      switch (pref_mode ->network_sel_mode_pref)
      {
        case CM_NETWORK_SEL_MODE_PREF_AUTOMATIC:
          network_sel_mode_pref = DSAT_QCNSP_NET_SEL_MODE_AUTOMATIC;
          break;
        case CM_NETWORK_SEL_MODE_PREF_MANUAL:
          network_sel_mode_pref = DSAT_QCNSP_NET_SEL_MODE_MANUAL;
          break;
        case CM_NETWORK_SEL_MODE_PREF_LIMITED_SRV:
          network_sel_mode_pref = DSAT_QCNSP_NET_SEL_MODE_LIMITED_SRV;
          break;
        default:
          network_sel_mode_pref = DSAT_QCNSP_NET_SEL_MODE_MAX;
      }
      switch(pref_mode ->acq_pri_order_pref.acq_order[0])
      {
        case SYS_SYS_MODE_NONE:
          acq_pri_order_pref = DSAT_QCNSP_ACQ_ORDER_PREF_AUTOMATIC;
          break;
        case SYS_SYS_MODE_GSM:
          acq_pri_order_pref = DSAT_QCNSP_ACQ_ORDER_PREF_GSM;
          break;
        case SYS_SYS_MODE_WCDMA:
          acq_pri_order_pref = DSAT_QCNSP_ACQ_ORDER_PREF_WCDMA;
          break;
        case SYS_SYS_MODE_LTE:
          acq_pri_order_pref = DSAT_QCNSP_ACQ_ORDER_PREF_LTE;
          break;
        default:
          acq_pri_order_pref = DSAT_QCNSP_ACQ_ORDER_PREF_MAX;;
      }
      res_buff_ptr->used = (word)snprintf((char *)res_buff_ptr->data_ptr,
                                                  res_buff_ptr->size,	
                                                  "$QCNSP: %d,%d,%d",
                                                   mode_pref,
                                                   network_sel_mode_pref,
                                                   acq_pri_order_pref);
      dsatcmdp_handle_async_cmd_rsp(res_buff_ptr, DSAT_COMPLETE_RSP);
      result = DSAT_OK;
    
  	}
   break;
   case DSAT_PENDING_QCNSP_GET_PRI_RAT:
   {
     uint8                        rat_index;
     boolean                      is_rat_exists = FALSE;
     cm_cmd_user_pref_update_type user_pref;
     cm_rat_acq_order_pref_s_type rat_acq_order_pref;

     memset(&user_pref,0x0,sizeof(user_pref));
     if ( !cm_user_pref_init( &user_pref ) )
     {
       return DSAT_ERROR;
     }

     /*Mode Pref*/
     user_pref.mode_pref = 
                 (dsat_num_item_type)dsatutil_get_val(DSAT_VENDOR_QCNSP_IDX,0,0,NUM_TYPE);
     /*Network Selection Pref*/
     user_pref.network_sel_mode_pref = 
     	            (dsat_num_item_type)dsatutil_get_val(DSAT_VENDOR_QCNSP_IDX,0,1,NUM_TYPE);
     user_pref.asubs_id = dsat_qcsimapp_info[dsat_get_qcsimapp_val()].subs_id;
     /* RAT order*/
     acq_pri_order_pref = (dsat_num_item_type)dsatutil_get_val(DSAT_VENDOR_QCNSP_IDX,0,2,NUM_TYPE);
     user_pref.client_id = dsatcm_client_id;
     user_pref.pref_term = CM_PREF_TERM_PERMANENT;

     if(SYS_SYS_MODE_NONE == acq_pri_order_pref)
     {
       user_pref.rat_acq_order_pref_ptr = NULL;
     }
     else
     {
       memscpy(&rat_acq_order_pref.acq_order.rat_acq_pri_order, 
       	       sizeof(cm_acq_pri_order_pref_s_type),
               &pref_mode->acq_pri_order_pref,
               sizeof(cm_acq_pri_order_pref_s_type));
       rat_acq_order_pref.type = CM_ACQ_ORDER_TYPE_RAT_PRI;
       for(rat_index = 0; (rat_index < pref_mode->acq_pri_order_pref.num_rat && rat_index < SYS_MAX_ACQ_PREF_RAT_LIST_NUM) ; rat_index++ )
       {
         if( acq_pri_order_pref == pref_mode->acq_pri_order_pref.acq_order[rat_index])
         {
           is_rat_exists = TRUE;
           break;
         }
       }
       
       if( FALSE == is_rat_exists)
       {
         return DSAT_ERROR;
       }
       else
       {
         for( ; 0 != rat_index; rat_index--)
         {
           rat_acq_order_pref.acq_order.rat_acq_pri_order.acq_order[rat_index] = 
                                             rat_acq_order_pref.acq_order.rat_acq_pri_order.acq_order[rat_index-1];
         }
         rat_acq_order_pref.acq_order.rat_acq_pri_order.acq_order[0] = acq_pri_order_pref;
       }
        user_pref.rat_acq_order_pref_ptr = &rat_acq_order_pref;
     }
      
     SET_PENDING(DSAT_VENDOR_QCNSP_IDX,0, DSAT_PENDING_QCNSP_WRITE)
     if(FALSE == cm_user_pref_update_req(&user_pref,dsatcmif_ph_cmd_cb_func,NULL))
     {
       SET_PENDING(DSAT_VENDOR_QCNSP_IDX ,0, DSAT_PENDING_QCNSP_NONE)
       result = DSAT_ERROR;
     }
     break;
   }
   case DSAT_PENDING_QCNSP_WRITE:
    result = DSAT_OK;
    break;
   default:
     DS_AT_MSG1_ERROR("Invalid cmd type %d",cmd_type);
     result = DSAT_ERROR;
  }
  return result;
}/* dsatvend_process_qcnsp_cmd */


#ifdef FEATURE_DSAT_LTE
#ifdef FEATURE_VOIP
/*===========================================================================
FUNCTION   DSAT_CONVERT_AMRNUM_TO_AMRSTR

DESCRIPTION
 Function will convert AMR number value int AMR string. It will check the SET bit in given number
 and generate corresponding string.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

void dsat_convert_amrnum_to_amrstr
(
  char                   *rsp_buf,
  dsat_num_item_type      buf_len,
  dsat_num_item_type      amr_num
)
{
  dsat_num_item_type bit_num = 0;
  dsat_num_item_type cur_buf_len = 0;
  
  if(NULL == rsp_buf)
  {
    return;
  }
  memset(rsp_buf,0,buf_len);
  
  while((amr_num != 0) && cur_buf_len < buf_len)
  {
    if(0 != (amr_num & 1))
    {
      cur_buf_len += snprintf(&rsp_buf[cur_buf_len],buf_len - cur_buf_len,"%d,",bit_num);
    }
    amr_num = amr_num>>1;
    bit_num++;
  }
  /*Replace , with Null terminator*/
  if(cur_buf_len != 0)
  {
    rsp_buf[cur_buf_len-1] = '\0';
  }
  return;
}/*dsat_convert_amrnum_to_amrstr*/

/*===========================================================================
FUNCTION DSATVEND_VOIPM_CONFIG_CB

DESCRIPTION
  This function handles set/get response from IMS.  

DEPENDENCIES
  None


RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
void dsatvend_voipm_config_cb
(
  ims_settings_at_cmd_rsp_info at_cmd_rsp_info
)
{
  ds_cmd_type * cmd_buf = NULL;

  DS_AT_MSG3_HIGH("QCVOIPM Response cmd=%d, type=%d, user data =%d",
                             at_cmd_rsp_info.eCmd,
                             at_cmd_rsp_info.rspType,
                            (dsat_num_item_type)at_cmd_rsp_info.pRspCbUserData);
  
  if( (cmd_buf = ds_get_cmd_buf()) == NULL)
  {
    ERR_FATAL("No ds command buffer",0,0,0);
  }
  else
  {
    /* Send the message to ATCOP */
    cmd_buf->hdr.cmd_id = DS_AT_VOIPM_AT_CMD;
/*Allocate memory for IMS response*/
    cmd_buf->cmd.voip_rsp_info.voip_parm = 
                     dsat_alloc_memory(sizeof(ims_settings_at_cmd_rsp_info),FALSE);

    /*
         * Copy complete reponse from IMS
         */
    if(NULL != cmd_buf->cmd.voip_rsp_info.voip_parm)
    {
      dsatutil_memscpy(cmd_buf->cmd.voip_rsp_info.voip_parm,
                       sizeof(ims_settings_at_cmd_rsp_info),
                       &at_cmd_rsp_info,
                       sizeof(ims_settings_at_cmd_rsp_info));
    }
    ds_put_cmd(cmd_buf);
  }
}/*dsatvend_voipm_config_cb*/

/*===========================================================================
FUNCTION DSATVEND_VOIPM_CONFIG_HANDLER

DESCRIPTION
  Handler function for IMS  response. 

DEPENDENCIES
  None
  
RETURN VALUE
  
DSAT_ERROR : if there was any problem in executing the command.
DSAT_OK : if it is a success and execution completed.

SIDE EFFECTS
  None
  
======================================================================*/
/*ARGSUSED*/
dsat_result_enum_type dsatvend_voipm_config_handler
(
  dsat_mode_enum_type   mode,                /* AT command mode            */
  ds_cmd_type         * cmd_ptr              /* DS Command pointer         */
)
{
  dsat_result_enum_type              result            = DSAT_OK;
  dsm_item_type                     *res_buff_ptr      = NULL;
  ims_settings_at_cmd_rsp_info      *rsp_info;
  
  if((NULL == cmd_ptr) || (NULL == cmd_ptr->cmd.voip_rsp_info.voip_parm))
  {
    SET_PENDING(DSAT_VENDOR_QCVOIPM_IDX ,0, DSAT_PENDING_VOIP_NONE)
    return DSAT_ERROR;
  }
  rsp_info = (ims_settings_at_cmd_rsp_info *)cmd_ptr->cmd.voip_rsp_info.voip_parm;
    
  if( (rsp_info->rspType != QC_IMS_SETTINGS_AT_CMD_RSP_SUCCESS) )
  {
    DS_AT_MSG1_ERROR("Failure response received %d",rsp_info->rspType);
    SET_PENDING(DSAT_VENDOR_QCVOIPM_IDX ,0, DSAT_PENDING_VOIP_NONE)
    result = DSAT_ERROR;
  }
  else if(CHECK_PENDING( DSAT_VENDOR_QCVOIPM_IDX, 0, DSAT_PENDING_VOIP_WRITE ))
  {
    if(rsp_info->eCmd == QC_IMS_SETTINGS_AT_CMD_SET_VOIP_CONFIG_RSP)
    {
      result = DSAT_OK;
    }
    else
    {
      result = DSAT_ERROR;
    }
  }
  else if(CHECK_PENDING( DSAT_VENDOR_QCVOIPM_IDX, 0, DSAT_PENDING_VOIP_READ ))
  {
    if(rsp_info->eCmd == QC_IMS_SETTINGS_AT_CMD_GET_VOIP_CONFIG_RSP)
    {
      ims_settings_at_voip_configuration *rsp_data = NULL;
      char              temp_str[QC_SETTINGS_AT_CMD_VOIP_AMR_WB_MODE_STR_LEN+1];
      
      rsp_data = &rsp_info->rspData.voipConfig;
      
      if( (res_buff_ptr = dsm_new_buffer( DSM_DS_SMALL_ITEM_POOL ))== NULL )
      {
        dsatutil_free_memory(rsp_info);
        DS_AT_MSG0_ERROR("Running out of memory for pref_mode ");
        SET_PENDING(DSAT_VENDOR_QCVOIPM_IDX ,0, DSAT_PENDING_VOIP_NONE)
        return DSAT_ERROR;
      }
      res_buff_ptr->used = (word)snprintf((char *)res_buff_ptr->data_ptr,
                                                res_buff_ptr->size,
                                                "$QCVOIPM: ");
      if(rsp_data->iIncludeSessionExpires)
      {
        res_buff_ptr->used += snprintf((char *)&res_buff_ptr->data_ptr[res_buff_ptr->used],
              res_buff_ptr->size - res_buff_ptr->used,"%d",rsp_data->iSessionExpires);
      
      }
      res_buff_ptr->used += snprintf((char *)&res_buff_ptr->data_ptr[res_buff_ptr->used],
              res_buff_ptr->size - res_buff_ptr->used,",");
    
      if(rsp_data->iIncludeMinSessionExpiry)
      {
        res_buff_ptr->used += snprintf((char *)&res_buff_ptr->data_ptr[res_buff_ptr->used],
            res_buff_ptr->size - res_buff_ptr->used,"%d",rsp_data->iMinSessionExpiry);
    
      }
      res_buff_ptr->used += snprintf((char *)&res_buff_ptr->data_ptr[res_buff_ptr->used],
            res_buff_ptr->size - res_buff_ptr->used,",");
      
      if(rsp_data->iIncludeRtpRtcpInactivityTimer)
      {
        res_buff_ptr->used += snprintf((char *)&res_buff_ptr->data_ptr[res_buff_ptr->used],
            res_buff_ptr->size - res_buff_ptr->used,"%d",(int)rsp_data->iRtpRtcpInactivityTimer);
    
      }
      res_buff_ptr->used += snprintf((char *)&res_buff_ptr->data_ptr[res_buff_ptr->used],
            res_buff_ptr->size - res_buff_ptr->used,",");

      if(rsp_data->iIncludeRingingTimerValid)
      {
        res_buff_ptr->used += snprintf((char *)&res_buff_ptr->data_ptr[res_buff_ptr->used],
            res_buff_ptr->size - res_buff_ptr->used,"%d",rsp_data->iRingingTimer/1000);
      
      }
      res_buff_ptr->used += snprintf((char *)&res_buff_ptr->data_ptr[res_buff_ptr->used],
            res_buff_ptr->size - res_buff_ptr->used,",");
  
      if(rsp_data->iIncludeRingbackTimerValid)
      {
        res_buff_ptr->used += snprintf((char *)&res_buff_ptr->data_ptr[res_buff_ptr->used],
            res_buff_ptr->size - res_buff_ptr->used,"%d",rsp_data->iRingbackTimer/1000);
      
      }
      res_buff_ptr->used += snprintf((char *)&res_buff_ptr->data_ptr[res_buff_ptr->used],
            res_buff_ptr->size - res_buff_ptr->used,",");
  
      if(rsp_data->iIncludeAmrWbEnable)
      {
        res_buff_ptr->used += snprintf((char *)&res_buff_ptr->data_ptr[res_buff_ptr->used],
            res_buff_ptr->size - res_buff_ptr->used,"%d",rsp_data->iAmrWbEnable);
      
      }
      res_buff_ptr->used += snprintf((char *)&res_buff_ptr->data_ptr[res_buff_ptr->used],
            res_buff_ptr->size - res_buff_ptr->used,",");
  
  
      if(rsp_data->iIncludeScrAmrEnable)
      {
        res_buff_ptr->used += snprintf((char *)&res_buff_ptr->data_ptr[res_buff_ptr->used],
            res_buff_ptr->size - res_buff_ptr->used,"%d",rsp_data->iScrAmrEnable);
      
      }
      res_buff_ptr->used += snprintf((char *)&res_buff_ptr->data_ptr[res_buff_ptr->used],
            res_buff_ptr->size - res_buff_ptr->used,",");
  
      if(rsp_data->iIncludeScrAmrWbEnable)
      {
        res_buff_ptr->used += snprintf((char *)&res_buff_ptr->data_ptr[res_buff_ptr->used],
            res_buff_ptr->size - res_buff_ptr->used,"%d",rsp_data->iScrAmrWbEnable);
      
      }
      res_buff_ptr->used += snprintf((char *)&res_buff_ptr->data_ptr[res_buff_ptr->used],
            res_buff_ptr->size - res_buff_ptr->used,",");
  
      if(rsp_data->iIncludeAmrMode)
      {
        dsat_convert_amrnum_to_amrstr(temp_str,sizeof(temp_str),rsp_data->iAmrMode);
        res_buff_ptr->used += snprintf((char *)&res_buff_ptr->data_ptr[res_buff_ptr->used],
            res_buff_ptr->size - res_buff_ptr->used,"\"%s\"",temp_str);
      
      }
      res_buff_ptr->used += snprintf((char *)&res_buff_ptr->data_ptr[res_buff_ptr->used],
            res_buff_ptr->size - res_buff_ptr->used,",");
  
      if(rsp_data->iIncludeAmrWbMode)
      {
        dsat_convert_amrnum_to_amrstr(temp_str,sizeof(temp_str),rsp_data->iAmrWbMode);
        res_buff_ptr->used += snprintf((char *)&res_buff_ptr->data_ptr[res_buff_ptr->used],
            res_buff_ptr->size - res_buff_ptr->used,"\"%s\"",temp_str);
      
      }
      dsatcmdp_handle_async_cmd_rsp(res_buff_ptr, DSAT_COMPLETE_RSP);
      result = DSAT_OK;
    }
    else
    {
      result = DSAT_ERROR;
    }
  }
  SET_PENDING(DSAT_VENDOR_QCVOIPM_IDX ,0, DSAT_PENDING_VOIP_NONE)
  dsatutil_free_memory(rsp_info);
  
  return result;
  
}/*dsatvend_voipm_config_handler*/

/*===========================================================================

FUNCTION DSATVEND_EXEC_QCVOIPM_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes AT$QCVOIPM command.
  This command provides the ability to set/get the drx coefficient.


DEPENDENCIES

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR : if there was any problem in executing the command.
    DSAT_ASYNC_CMD : if success.
    DSAT_OK : if it is a success and execution completed.

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatvend_exec_qcvoipm_cmd
(
  dsat_mode_enum_type       mode,             /*  AT command mode:            */
  const dsati_cmd_type     *parse_table,      /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,          /*  Command tokens from parser  */
  dsm_item_type            *res_buff_ptr      /*  Place to put response       */
)
{
#define DSAT_UINT16_MAX_NUM           65535
#define DSAT_MIN_SESSION_MINSE_TIMER  90
#define DSAT_MAX_RTCP_TIMER           20
#define DSAT_MIN_RINGER_TIMER         30
#define DSAT_MAX_RINGER_TIMER         45

#define DSAT_MIN_RINGER_BACK_TIMER    35
#define DSAT_MAX_RINGER_BACK_TIMER    50


  dsat_result_enum_type               result     = DSAT_OK;
  ims_settings_at_voip_configuration *ims_config = NULL;
  ims_settings_at_cmd_req_info        voip_params;
  dsat_num_item_type                  arg_val    = 0;

  memset(&voip_params, 0x0 , sizeof(voip_params));
  /*----------------------------------------------------------------
 Standard processing for a command with an argument (WRITE command):
 -----------------------------------------------------------------*/
   if (tok_ptr->op == (NA|EQ|AR))
  {
    
     ims_config = &voip_params.reqData.voipConfig;
/* Check the number of arguments.*/
    if ( tok_ptr->args_found != parse_table->compound)
    {
      DS_AT_MSG1_ERROR("$QCVOIPM: Invalid Number of arguements  %d", tok_ptr->args_found);
      result = DSAT_ERROR;
    }
/*Arg 0  Indicates the session_timer_ims parameter ; Unit is Sec*/
    if( (result == DSAT_OK) && (VALID_TOKEN(0))&& 
        (ATOI_OK == dsatutil_atoi(&arg_val,tok_ptr->arg[0],10))&&
        (arg_val >= DSAT_MIN_SESSION_MINSE_TIMER && arg_val <= DSAT_UINT16_MAX_NUM))
    {
      ims_config->iIncludeSessionExpires = 1;
      ims_config->iSessionExpires        = arg_val;
       
    }else
    {
      DS_AT_MSG0_ERROR("$QCVOIPM: SessionExpires is mandatory ");
      result = DSAT_ERROR;
    }
/*Arg 1  Indicates the value of min_se parameter in seconds ;*/
    if( (result == DSAT_OK) && (VALID_TOKEN(1))&& 
        (ATOI_OK == dsatutil_atoi(&arg_val,tok_ptr->arg[1],10))&&
        (arg_val >= DSAT_MIN_SESSION_MINSE_TIMER && arg_val <= DSAT_UINT16_MAX_NUM))
    {
      ims_config->iIncludeMinSessionExpiry = 1;
      ims_config->iMinSessionExpiry        = arg_val;
        
    }else
    {
      DS_AT_MSG0_ERROR("$QCVOIPM: MinSessionExpiry is mandatory  ");
      result = DSAT_ERROR;
    }
/*Arg 2  Indicates the value of  rtp_rtcp_inactivity_timer_ims parameter in seconds;*/
    if( (result == DSAT_OK) && (VALID_TOKEN(2))&& 
        (ATOI_OK == dsatutil_atoi(&arg_val,tok_ptr->arg[2],10))&&
        (arg_val <= DSAT_MAX_RTCP_TIMER))
    {
      ims_config->iIncludeRtpRtcpInactivityTimer = 1;
      ims_config->iRtpRtcpInactivityTimer        = arg_val;
              
    }else
    {
      DS_AT_MSG0_ERROR("$QCVOIPM: RtpRtcpInactivityTimer is mandatory  ");
      result = DSAT_ERROR;
    }
/*Arg 3  Indicates the value of ringing_timer_ims parameter in seconds*/
    if( (result == DSAT_OK) && (VALID_TOKEN(3))&& 
        (ATOI_OK == dsatutil_atoi(&arg_val,tok_ptr->arg[3],10))&&
        (arg_val >= DSAT_MIN_RINGER_TIMER && arg_val <= DSAT_MAX_RINGER_TIMER))
    {
/* IMS expects value in Ms, convert Sec into Ms*/
      ims_config->iIncludeRingingTimerValid = 1;
      ims_config->iRingingTimer             = arg_val * 1000;
     
    }else
    {
      DS_AT_MSG0_ERROR("$QCVOIPM: RingingTimer is mandatory  ");
      result = DSAT_ERROR;
    }
/*Arg 4 Indicates the  ringback_timer_ims parameter in seconds*/
    if( (result == DSAT_OK) && (VALID_TOKEN(4))&& 
        (ATOI_OK == dsatutil_atoi(&arg_val,tok_ptr->arg[4],10))&&
        (arg_val >= DSAT_MIN_RINGER_BACK_TIMER && arg_val <= DSAT_MAX_RINGER_BACK_TIMER))
    {
/* IMS expect value in Ms, convert Sec into Ms*/
      ims_config->iIncludeRingbackTimerValid = 1;
      ims_config->iRingbackTimer             = arg_val * 1000;
    }else
    {
      DS_AT_MSG0_ERROR("$QCVOIPM: RingbackTimer is mandatory  ");
      result = DSAT_ERROR;
    }
/*Arg 5  Indicates the value of amrwb parameter*/
    if( (result == DSAT_OK) && (VALID_TOKEN(5))&& 
        (ATOI_OK == dsatutil_atoi(&arg_val,tok_ptr->arg[5],10))&&
        (arg_val == 0 || arg_val == 1))
    {
      ims_config->iIncludeAmrWbEnable = 1;
      ims_config->iAmrWbEnable        = arg_val;
       
    }else
    {
      DS_AT_MSG0_ERROR("$QCVOIPM: AmrWbEnable is mandatory  ");
      result = DSAT_ERROR;
    }
/*Arg 6  Indicates the value of scr_amr parameter*/
    if( (result == DSAT_OK) && (VALID_TOKEN(6))&& 
        (ATOI_OK == dsatutil_atoi(&arg_val,tok_ptr->arg[6],10))&&
        (arg_val == 0 || arg_val == 1))
    {
      ims_config->iIncludeScrAmrEnable = 1;
      ims_config->iScrAmrEnable        = arg_val;
       
    }else
    {
      DS_AT_MSG0_ERROR("$QCVOIPM: ScrAmrEnable is mandatory  ");
      result = DSAT_ERROR;
    }
/*Arg 7 Indicates the value of scr_amrwb parameter*/
    if( (result == DSAT_OK) && (VALID_TOKEN(7))&& 
        (ATOI_OK == dsatutil_atoi(&arg_val,tok_ptr->arg[7],10))&&
        (arg_val == 0 || arg_val == 1))
    {
      ims_config->iIncludeScrAmrWbEnable = 1;
      ims_config->iScrAmrWbEnable        = arg_val;
       
    }else
    {
      DS_AT_MSG0_ERROR("$QCVOIPM: ScrAmrWbEnable  is mandatory  ");
      result = DSAT_ERROR;
    }
/*Arg 8 Indicates the value of amrmodeset parameter*/
    if( (result == DSAT_OK) && (VALID_TOKEN(8))&& 
        (TRUE == dsatutil_strip_quotes_out((const byte*)tok_ptr->arg[8],
        (byte *)&ims_config->cAmrModeStr[0],QC_SETTINGS_AT_CMD_VOIP_AMR_MODE_STR_LEN + 1)))
    {
      ims_config->iIncludeAmrModeStr = 1;
       
    }else
    {
      DS_AT_MSG0_ERROR("$QCVOIPM: AmrModeStr is mandatory  ");
      result = DSAT_ERROR;
    }
/*Arg 9 Indicates the value of amrwbmodset parameter*/
    if( (result == DSAT_OK) && (VALID_TOKEN(9))&& 
        (TRUE == dsatutil_strip_quotes_out((const byte*)tok_ptr->arg[9],
        (byte *)&ims_config->cAmrWbModeStr[0],QC_SETTINGS_AT_CMD_VOIP_AMR_WB_MODE_STR_LEN + 1)))
    {
      ims_config->iIncludeAmrWbModeStr = 1;
     
    }else
    {
      DS_AT_MSG0_ERROR("$QCVOIPM: Amr WbModeStr is mandatory  ");
      result = DSAT_ERROR;
    }

    if(result == DSAT_OK)
    {
      voip_params.eCmd           = QC_IMS_SETTINGS_AT_CMD_SET_VOIP_CONFIG_REQ;
      voip_params.pRspCbUserData = (QPVOID *)DSAT_PENDING_VOIP_WRITE;
      voip_params.pRspCallback   = dsatvend_voipm_config_cb;
      
      SET_PENDING(DSAT_VENDOR_QCVOIPM_IDX ,0, DSAT_PENDING_VOIP_WRITE)
      result = DSAT_ASYNC_CMD;
      
      if(AEE_IMS_SUCCESS != ims_settings_at_command_req(voip_params))
      {
        SET_PENDING(DSAT_VENDOR_QCVOIPM_IDX ,0, DSAT_PENDING_VOIP_NONE)
        result = DSAT_ERROR;
      }
    }
    
  }
  /*--------------------------------------  
 Standard processing for a READ command:
 ----------------------------------------*/
        
  else if ( tok_ptr->op == (NA|QU) )
  {
    voip_params.eCmd           = QC_IMS_SETTINGS_AT_CMD_GET_VOIP_CONFIG_REQ;
    voip_params.pRspCbUserData = (QPVOID *)DSAT_PENDING_VOIP_READ;
    voip_params.pRspCallback   = dsatvend_voipm_config_cb;
    
    SET_PENDING(DSAT_VENDOR_QCVOIPM_IDX ,0, DSAT_PENDING_VOIP_READ)
    result = DSAT_ASYNC_CMD;
    
    if(AEE_IMS_SUCCESS != ims_settings_at_command_req(voip_params))
    {
      SET_PENDING(DSAT_VENDOR_QCVOIPM_IDX ,0, DSAT_PENDING_VOIP_NONE)
      result = DSAT_ERROR;
    }
  }
  /*--------------------------------------
Standard processing for a TEST command:
----------------------------------------*/

  else if ( tok_ptr->op == (NA|EQ|QU) )
  {
   /*Command will display current supported range*/
   res_buff_ptr->used = (word) 
    snprintf((char *)res_buff_ptr->data_ptr,
                     res_buff_ptr->size,
                    "$QCVOIPM: (%d - %d),(%d - %d ),(0 - %d),(%d - %d),(%d - %d),(0 - 1),(0 - 1),(0 - 1),(%s),(%s)\n",
                     DSAT_MIN_SESSION_MINSE_TIMER,
                     DSAT_UINT16_MAX_NUM,
                     DSAT_MIN_SESSION_MINSE_TIMER,
                     DSAT_UINT16_MAX_NUM,
                     DSAT_MAX_RTCP_TIMER,
                     DSAT_MIN_RINGER_TIMER,
                     DSAT_MAX_RINGER_TIMER,
                     DSAT_MIN_RINGER_BACK_TIMER,
                     DSAT_MAX_RINGER_BACK_TIMER,
                     "\"0,1,2,3,4,5,6,7,8\"",
                     "\"0,1,2,3,4,5,6,7,8,9\"");
  }
  /*--------------------------------------------------
Command error: Operator(s) not allowed.
----------------------------------------------------*/
  else
  {
    result = DSAT_ERROR;
  }
  return result;
}/*dsatvend_exec_qcvoipm_cmd*/
#endif /* FEATURE_VOIP */
/*===========================================================================

FUNCTION DSATVEND_EXEC_QCDRX_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes AT^QCDRX command.
  This command provides the ability to set/get the drx coefficient.

  <drx_coefficient> drx coefficient, values as follows:
    0 Not specified by MS
    6 CN = 6, T = 32
    7 CN = 7, T = 64
    8 CN = 8, T = 128
    9 CN = 9, T = 256

DEPENDENCIES

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR : if there was any problem in executing the command.
    DSAT_ASYNC_CMD : if success.
    DSAT_OK : if it is a success and execution completed.

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatvend_exec_qcdrx_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type result = DSAT_ERROR;

  if ( tok_ptr->op == (NA|EQ|AR) )
  {
    if ( dsatparm_exec_param_cmd(mode, parse_table, tok_ptr, res_buff_ptr) != DSAT_ERROR )
    {
      cm_set_drx_info_s_type set_drx_info;

      SET_PENDING(DSAT_VENDOR_QCDRX_IDX, 0, DSAT_PENDING_QCDRX_SET);

      result = DSAT_ASYNC_CMD;

      set_drx_info.asubs_id = dsat_get_current_subs_id();
      set_drx_info.drx_coefficient = (sys_drx_cn_coefficient_s1_e_type)
        dsatutil_get_val ( DSAT_VENDOR_QCDRX_IDX, 0, 0, NUM_TYPE );
      if ( cm_ph_cmd_set_drx_req_per_subs ( dsatcmif_ph_cmd_cb_func,
                                            NULL,
                                            dsatcm_client_id,
                                            &set_drx_info ) == FALSE )
      {
        DS_AT_MSG0_ERROR("No buffer to send request");

        SET_PENDING(DSAT_VENDOR_QCDRX_IDX, 0, DSAT_PENDING_QCDRX_NONE);

        result = DSAT_ERROR;
      }
    }
  }
  else if ( tok_ptr->op == (NA|QU) )
  {
    cm_get_drx_info_s_type get_drx_info;

    SET_PENDING(DSAT_VENDOR_QCDRX_IDX, 0, DSAT_PENDING_QCDRX_GET);

    result = DSAT_ASYNC_CMD;

    get_drx_info.asubs_id = dsat_get_current_subs_id();
    if ( cm_ph_cmd_get_drx_req_per_subs ( dsatcmif_ph_cmd_cb_func,
                                          NULL,
                                          dsatcm_client_id,
                                          &get_drx_info ) == FALSE )
    {
      DS_AT_MSG0_ERROR("No buffer to send request");

      SET_PENDING(DSAT_VENDOR_QCDRX_IDX, 0, DSAT_PENDING_QCDRX_NONE);

      result = DSAT_ERROR;
    }
  }
  else if ( tok_ptr->op == (NA|EQ|QU) )
  {
    result = dsatparm_exec_param_cmd(mode, parse_table, tok_ptr, res_buff_ptr);
  }

  return result;
}/* dsatvend_exec_qcdrx_cmd */

/*===========================================================================
FUNCTION DSATVEND_PROCESS_QCDRX_CMD

DESCRIPTION
  This function process the response for the $QCDRX command on return from
  asynchronous processing.
 
DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR : if there was any problem in executing the command.
    DSAT_OK : if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatvend_process_qcdrx_cmd
(
  dsat_cmd_pending_enum_type cmd_type,  /* cmd type */
  ds_at_ph_info_u_type  *ph_info        /* PH info */
)
{
  dsat_result_enum_type result = DSAT_ERROR;

  SET_PENDING(DSAT_VENDOR_QCDRX_IDX, 0, DSAT_PENDING_QCDRX_NONE);

  switch ( cmd_type )
  {
    case DSAT_PENDING_QCDRX_SET:
      {
        if ( ph_info->set_drx_result == TRUE )
        {
          result = DSAT_OK;
        }
      }
      break;

    case DSAT_PENDING_QCDRX_GET:
      {
        dsm_item_type *res_buff_ptr;
  
        res_buff_ptr = dsat_dsm_new_buffer(DSM_DS_SMALL_ITEM_POOL, FALSE);
        res_buff_ptr->used = (word) snprintf ( (char *) res_buff_ptr->data_ptr,
                                               res_buff_ptr->size,
                                               "$QCDRX: %d",
                                               ph_info->drx_coefficient );
  
        dsatcmdp_handle_async_cmd_rsp(res_buff_ptr, DSAT_COMPLETE_RSP);
  
        result = DSAT_OK;
      }
      break;

    default:
      DS_AT_MSG1_ERROR("Invalid cmd type %d", cmd_type);
  }

  return result;
}/* dsatvend_process_qcdrx_cmd */

/*===========================================================================
FUNCTION DSATVEND_EXEC_QCRSRP_CMD

DESCRIPTION
  This function executes $QCRSRP and $QCRSRQ commands and provides response 
  Calls CM API to receive neighbour cell info like Cell id,EARFCN,RSRP,RSRQ.
  RSRP - Reference Signal Received Power
  RSRQ - Reference Signal Received Quality
 
DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR : if there was any problem in executing the command.
    DSAT_OK :    if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatvend_exec_qcrsrp_cmd
(
    dsat_mode_enum_type mode,             /*  AT command mode:            */
    const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
    const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
    dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  #define CMD_RSRP 0
  #define CMD_RSRQ 1
  #define MAX_RSR_COUNT CMAPI_LTE_NGBR_IND_NUM_CELLS
  
  dsat_result_enum_type result = DSAT_ERROR;
  cmapi_err_e_type err_wcdma_mode = CMAPI_SUCCESS;
  cmapi_err_e_type err_lte_mode = CMAPI_SUCCESS;
  cmapi_err_e_type err_cmapi = CMAPI_SUCCESS;
  cmapi_rat_meas_info_s_type meas_info;
  boolean cmd = 0;
  int i = 0,j = 0;
  int rsr_count = 0;
  int16 value = 0;

  /* Supported only NA|QU */
  if( tok_ptr->op != (NA|QU) )
  {
    return result;
  }

  memset( &meas_info, 0, sizeof(meas_info) );
  res_buff_ptr->data_ptr[0] = '\0';
  if (0 == strncmp((const char *)parse_table->name,(const char *)"$QCRSRP",strlen((const char *)"$QCRSRP")))
  {
    res_buff_ptr->used = (word)snprintf((char*)res_buff_ptr->data_ptr,
                                                res_buff_ptr->size,
                                                "$QCRSRP: ");
    cmd = CMD_RSRP;
  }
  else if(0 == strncmp((const char *)parse_table->name,(const char *)"$QCRSRQ",strlen((const char *)"$QCRSRQ")))
  {
    res_buff_ptr->used = (word)snprintf((char*)res_buff_ptr->data_ptr,
                                                res_buff_ptr->size,
                                                "$QCRSRQ: ");
    cmd = CMD_RSRQ;
  }
  else
  {
    return result;
  }

  //get LTE Cell info
  err_lte_mode = cmapi_get_rat_meas_info( CMAPI_SYS_MODE_LTE, &meas_info );
  if( err_lte_mode == CMAPI_SUCCESS )
  {
    if( meas_info.is_service_available && (meas_info.sys_mode == CMAPI_SYS_MODE_LTE) &&
         meas_info.meas_info_u.lte_nbr_info.valid )
    {
      cmapi_lte_ngbr_ind_s* nbr_info = &meas_info.meas_info_u.lte_nbr_info;
     /*lte intra start: in intra cell nbr info each cell different rsrp and rsrq values get the info for all cells */
      DS_AT_MSG1_HIGH("LTE: num intra num cells: %d", nbr_info->lte_intra.num_lte_cells);
      for( i = 0; i<nbr_info->lte_intra.num_lte_cells && i < CMAPI_LTE_NGBR_IND_NUM_CELLS; i++ )
      {
        value = (cmd == CMD_RSRP) ? nbr_info->lte_intra.cell[i].rsrp : nbr_info->lte_intra.cell[i].rsrq;
        if( cmd == CMD_RSRP )
        {
          res_buff_ptr->used += (word)snprintf(
                                (char*)&res_buff_ptr->data_ptr[res_buff_ptr->used],
                                res_buff_ptr->size - res_buff_ptr->used,
                                "%3.3d,%lu,\"%3.3d.%d0\",",
                                nbr_info->lte_intra.cell[i].pci,
                                nbr_info->lte_intra.earfcn,
                                value/10,
                                DSAT_ABSOLUTE_VAL(value)%10);
        }
        else
        {
          res_buff_ptr->used += (word)snprintf(
                                   (char*)&res_buff_ptr->data_ptr[res_buff_ptr->used],
                                    res_buff_ptr->size - res_buff_ptr->used,
                                   "%3.3d,%lu,\"%2.2d.%d0\"," ,
                                    nbr_info->lte_intra.cell[i].pci,
                                    nbr_info->lte_intra.earfcn,
                                    value/10,
                                    DSAT_ABSOLUTE_VAL(value)%10);
        }
          rsr_count++;
      }
      /*lte intra end*/

      /*lte inter start :in the inter  cell nbr info multriple ferequencies are available hence collect all cells info for each frequency*/
      DS_AT_MSG2_HIGH("LTE: num inter num freqs: %d current rsr_count = %d", nbr_info->lte_inter.num_freqs,rsr_count);
      for( i=0; ( i<nbr_info->lte_inter.num_freqs && i < CMAPI_LTE_NGBR_IND_NUM_FREQS) &&
                ( rsr_count < MAX_RSR_COUNT); i++ )
      {
        DS_AT_MSG1_HIGH("LTE: num inter num cells: %d", nbr_info->lte_inter.freqs[i].num_lte_cells);
        
        for( j=0; ( j<nbr_info->lte_inter.freqs[i].num_lte_cells && j < CMAPI_LTE_NGBR_IND_NUM_CELLS) && 
                  ( rsr_count < MAX_RSR_COUNT); j++ )
        {
            value = (cmd == CMD_RSRP) ? nbr_info->lte_inter.freqs[i].cells[j].rsrp : nbr_info->lte_inter.freqs[i].cells[j].rsrq;
          if( cmd == CMD_RSRP )
          {
            res_buff_ptr->used += (word)snprintf(
                                   (char*)&res_buff_ptr->data_ptr[res_buff_ptr->used],
                                   res_buff_ptr->size - res_buff_ptr->used,
                                   "%3.3d,%lu,\"%3.3d.%d0\"," ,
                                   nbr_info->lte_inter.freqs[i].cells[j].pci,
                                   nbr_info->lte_inter.freqs[i].earfcn,
                                   value/10,
                                   DSAT_ABSOLUTE_VAL(value)%10);
          }
          else
          {
            res_buff_ptr->used += (word)snprintf(
                                   (char*)&res_buff_ptr->data_ptr[res_buff_ptr->used],
                                   res_buff_ptr->size - res_buff_ptr->used,
                                   "%3.3d,%lu,\"%2.2d.%d0\",",
                                   nbr_info->lte_inter.freqs[i].cells[j].pci,
                                   nbr_info->lte_inter.freqs[i].earfcn,
                                   value/10,
                                   DSAT_ABSOLUTE_VAL(value)%10);
          }
            rsr_count++;
        }
      }
      /*lte inter enter*/
    }
    else
    {
      DS_AT_MSG0_MED("LTE info is not valid");
    }
  }
  else
  {
    DS_AT_MSG0_ERROR("cmapi for meas info failed in CMAPI_SYS_MODE_LTE ");
  }

  /*Get WCDMA cell Info: here we are collecting LTE nbr cells of current cell(wcdma) 
     .their might be multiple earfcn nbr info hence collect cell info for each earfcn*/
  err_wcdma_mode = cmapi_get_rat_meas_info( CMAPI_SYS_MODE_WCDMA, &meas_info);
  if( (err_wcdma_mode == CMAPI_SUCCESS) && ( rsr_count < MAX_RSR_COUNT))
  {
    if( meas_info.is_service_available && (meas_info.sys_mode == CMAPI_SYS_MODE_WCDMA) &&
      meas_info.meas_info_u.wcdma_nbr_info.is_data_valid )
    {
     cmapi_wcdma_signal_info_struct_type* nbr_info = &meas_info.meas_info_u.wcdma_nbr_info;
     for( i=0; (i< nbr_info->cmapi_wcdma_lte_cell_info.num_earfcn && i < CMAPI_WCDMA_SRCH_LTE_EARFCN_MAX) &&
                ( rsr_count < MAX_RSR_COUNT) ; i++ )
     {
       DS_AT_MSG1_HIGH("WCDMA info : num cells = %d",nbr_info->cmapi_wcdma_lte_cell_info.lte_earfcn[i].num_cells);
       for( j=0; ( j< nbr_info->cmapi_wcdma_lte_cell_info.lte_earfcn[i].num_cells && i < CMAPI_WCDMA_MAX_CELLS_REPORTED) &&
                  ( rsr_count < MAX_RSR_COUNT); j++ )
       {
            value = (cmd == CMD_RSRP) ? 
            	        nbr_info->cmapi_wcdma_lte_cell_info.lte_earfcn[i].cell[j].rsrp :
            	        nbr_info->cmapi_wcdma_lte_cell_info.lte_earfcn[i].cell[j].rsrq ;
         if(cmd == CMD_RSRP)
         {
            res_buff_ptr->used += (word)snprintf(
                                   (char*)&res_buff_ptr->data_ptr[res_buff_ptr->used],
                                   res_buff_ptr->size - res_buff_ptr->used,
                                   "%3.3d,%lu,\"%3.3d.%d0\"," ,
                                   nbr_info->cmapi_wcdma_lte_cell_info.lte_earfcn[i].cell[j].cell_id,
                                   nbr_info->cmapi_wcdma_lte_cell_info.lte_earfcn[i].earfcn,
                                   value/10,
                                   DSAT_ABSOLUTE_VAL(value)%10);
         }
         else
         {
            res_buff_ptr->used += (word)snprintf(
                                   (char*)&res_buff_ptr->data_ptr[res_buff_ptr->used],
                                   res_buff_ptr->size - res_buff_ptr->used,
                                   "%3.3d,%lu,\"%2.2d.%d0\",",
                                   nbr_info->cmapi_wcdma_lte_cell_info.lte_earfcn[i].cell[j].cell_id,
                                   nbr_info->cmapi_wcdma_lte_cell_info.lte_earfcn[i].earfcn,
                                   value/10,
                                   DSAT_ABSOLUTE_VAL(value)%10);
         }
            rsr_count++;
        }
      }
    }
    else
    {
      DS_AT_MSG0_MED("WCDMA info is not valid");
    }
  }
  else
  {
    DS_AT_MSG0_ERROR("cmapi for meas info failed in CMAPI_SYS_MODE_WCDMA ");
  }
  if( rsr_count )
  {
    res_buff_ptr->used--;
  }
  /* if api returns failure for both systems then we need to disaply error message*/
  if( err_wcdma_mode != CMAPI_SUCCESS && err_lte_mode != CMAPI_SUCCESS)
  {
    res_buff_ptr->used = 0;
    res_buff_ptr->data_ptr[0] = '\0';
    if(dsatcmdp_get_current_mode() == DSAT_MODE_WCDMA)
    {
      err_cmapi = err_wcdma_mode;
    }
    else
    {
      err_cmapi = err_lte_mode;
    }
    switch( err_cmapi )
    {
      case CMAPI_NO_SERVICE:
        result = dsat_send_cme_error(DSAT_CME_NO_NETWORK_SERVICE);
        break;
      default:
        DS_AT_MSG1_MED("CMAPI Error : %d",err_cmapi);
        result = dsat_send_cme_error(DSAT_CME_UNKNOWN);
        break;
    }
  }
  /* if api call is successful and data is not valid display error*/
  else if( meas_info.meas_info_u.wcdma_nbr_info.is_data_valid == 0 && meas_info.meas_info_u.lte_nbr_info.valid == 0)
  {
    res_buff_ptr->used = 0;
    res_buff_ptr->data_ptr[0] = '\0';
    result = DSAT_ERROR;
  }
  /* if both or any one of the api calls are successful then return success.*/
  else
  {
    result = DSAT_OK;
  }
  return result;
}
#endif /* FEATURE_DSAT_LTE */

#ifdef FEATURE_SGLTE
/*===========================================================================

FUNCTION DSATVEND_EXEC_QCHCOPS_CMD

DESCRIPTION
  This function takes the result from the command line parser and executes
  it. It executes $QCHCOPS command, a read only command and a version of
  +COPS read command for hybrid stack.

DEPENDENCIES
  None

RETURN VALUE
  Returns an enum that describes the result of the command execution.
  Possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_CMD_ERR_RSP: if there was a problem in lower layers
    DSAT_ASYNC_CMD : if aynchronous command continues successfully
    DSAT_OK : if it is a success for local execution

SIDE EFFECTS
  None

===========================================================================*/
/* ARGSUSED */
dsat_result_enum_type dsatvend_exec_qchcops_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type result = DSAT_ERROR;

  if ((tok_ptr->op == (NA|QU)) && (TRUE == dsat_qcsimapp_info[dsat_get_qcsimapp_val()].is_sglte_sub))
  {
    dsatcmif_servs_state_ms_info *ph_ss_ms_val = NULL;
    dsat_net_reg_state_s_type    *net_reg_state_ptr = NULL;
    net_id_info_s_type           *net_id_info_ptr = NULL;
    dsat_num_item_type            mode_val;
    dsat_cops_fmt_e_type          fmt;
    dsat_memory_subs_type         hybr_subs_id;

    hybr_subs_id = dsatcmif_get_sglte_hybr_subs_info();
    DSATUTIL_CMD_GET_BASE_ADDR(DSAT_SS_PH_MS_VALS, (void **)&ph_ss_ms_val, hybr_subs_id);

    net_reg_state_ptr = &ph_ss_ms_val->dsat_net_reg_state;
    net_id_info_ptr = &net_reg_state_ptr->net_id_info;

    mode_val = (dsat_num_item_type) dsatutil_get_val( DSATETSI_EXT_ACT_COPS_ETSI_IDX,
                                      dsat_get_qcsimapp_val(), 0, MIX_NUM_TYPE);

    fmt = (dsat_num_item_type) dsatutil_get_val( DSATETSI_EXT_ACT_COPS_ETSI_IDX,
                                 dsat_get_qcsimapp_val(), 1, MIX_NUM_TYPE);

    if ( FALSE == net_id_info_ptr->present )
    {
      res_buff_ptr->used = (word) snprintf( (char *) res_buff_ptr->data_ptr,
                                            res_buff_ptr->size,
                                            "%s: %d",
                                            parse_table->name,
                                            mode_val );

      result = DSAT_OK;
    }
    else if ( fmt == DSAT_COPS_FMT_PLMN )
    {
      boolean plmn_undefined = FALSE;
      sys_mcc_type mcc = 0;
      sys_mnc_type mnc = 0;
      boolean pcs_flag = FALSE;

      sys_plmn_get_mcc_mnc( net_id_info_ptr->plmn,
                            &plmn_undefined,
                            &pcs_flag,
                            &mcc,
                            &mnc );

      /* PLMN may be undefined */
      if (TRUE == plmn_undefined)
      {
        res_buff_ptr->used = (word) snprintf ( (char *) res_buff_ptr->data_ptr,
                                               res_buff_ptr->size,
                                               "%s: %d,%d,\"\",%d",
                                               (const char *) parse_table->name,
                                               mode_val,
                                               fmt,
                                               net_reg_state_ptr->act );
      }
      else
      {
        if ( pcs_flag )
        {
          /* Print MNC as 3 digits */
          res_buff_ptr->used = (word) snprintf ( (char *) res_buff_ptr->data_ptr,
                                                 res_buff_ptr->size,
                                                 "%s: %d,%d,\"%03lu%03lu\",%d",
                                                 (const char *) parse_table->name,
                                                 mode_val,
                                                 fmt,
                                                 mcc, mnc,
                                                 net_reg_state_ptr->act );
        }
        else
        {
          res_buff_ptr->used = (word) snprintf ( (char *) res_buff_ptr->data_ptr,
                                                 res_buff_ptr->size,
                                                 "%s: %d,%d,\"%03lu%02lu\",%d",
                                                 (const char *) parse_table->name,
                                                 mode_val,
                                                 fmt,
                                                 mcc, mnc,
                                                 net_reg_state_ptr->act );
        }
      }

      result = DSAT_OK;
    }
    else
    {
#ifdef FEATURE_MMGSDI
      mmgsdi_plmn_id_list_type plmn_id_list;
      mmgsdi_plmn_id_type plmn_id;

      plmn_id_list.num_of_plmn_ids = 1;

      (void) dsatutil_memscpy( (void *) &plmn_id.plmn_id_val[0], MMGSDI_PLMN_ID_SIZE,
                               (void *) &net_id_info_ptr->plmn.identity[0],
                               MMGSDI_PLMN_ID_SIZE );

      plmn_id.csg_id = 0xFFFFFFFF;

      switch (net_reg_state_ptr->sys_mode)
      {
        case SYS_SYS_MODE_WCDMA:
          plmn_id.rat = MMGSDI_RAT_UMTS;
          break;
#ifdef FEATURE_TDSCDMA
        case SYS_SYS_MODE_TDS:
#endif /* FEATURE_TDSCDMA */
          plmn_id.rat = MMGSDI_RAT_TDS;
          break;

        case SYS_SYS_MODE_LTE:
          plmn_id.rat = MMGSDI_RAT_LTE;
          break;

        case SYS_SYS_MODE_GSM:
          plmn_id.rat = MMGSDI_RAT_GSM;
          break;

        default:
          plmn_id.rat = MMGSDI_RAT_NONE;
          break;
      }

      plmn_id_list.plmn_list_ptr = &plmn_id;

      ph_ss_ms_val->dsatetsicall_network_list.cmd_state = DSAT_COPS_ASTATE_GET_NAME;

      if( MMGSDI_SUCCESS == mmgsdi_session_get_operator_name (
                dsat_qcsimapp_info[dsat_get_qcsimapp_info_idx()].session_id,
                plmn_id_list,
                dsatme_mmgsdi_get_operator_name_cb,
                DSAT_MMGSDI_GET_CURR_HYBR_OPER_NAME) )
      {
        result = DSAT_ASYNC_CMD;
      }
      else
      {
        ph_ss_ms_val->dsatetsicall_network_list.cmd_state = DSAT_COPS_ASTATE_NULL;
        ph_ss_ms_val->dsatetsicall_network_list.cmd_idx = CMD_IDX_NONE;
        DS_AT_MSG0_ERROR("mmgsdi_session_get_operator_name API call failed");
        dsatme_set_cme_error(DSAT_CME_PHONE_FAILURE, res_buff_ptr);
        result = DSAT_CMD_ERR_RSP;
      }
#endif /* FEATURE_MMGSDI */
    }
  }

  return result;
} /* dsatvend_exec_qchcops_cmd */

/*===========================================================================

FUNCTION DSATVEND_EXEC_QCHCREG_CMD

DESCRIPTION
  This function takes the result from the command line parser and executes
  it. It executes $QCHCREG command, a read only command and a version of
  +CREG read command for hybrid stack.

DEPENDENCIES
  None

RETURN VALUE
  Returns an enum that describes the result of the command execution.
  Possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success for local execution

SIDE EFFECTS
  None

===========================================================================*/
/* ARGSUSED */
dsat_result_enum_type dsatvend_exec_qchcreg_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
#define CREG_DISP_LAC_CELL_ID (2)
  dsat_result_enum_type result = DSAT_ERROR;

  if ((tok_ptr->op == (NA|QU)) && (TRUE == dsat_qcsimapp_info[dsat_get_qcsimapp_val()].is_sglte_sub) )
  {
    dsatcmif_servs_state_ms_info *ph_ss_ms_val = NULL;
    dsat_net_reg_state_s_type    *net_reg_state_ptr = NULL;
    dsat_creg_result_s_type      *creg_reported_ptr = NULL;
    dsat_num_item_type            creg_n;
    char                          fmt_string[70];
    dsat_memory_subs_type         hybr_subs_id;

    hybr_subs_id = dsatcmif_get_sglte_hybr_subs_info();
    DSATUTIL_CMD_GET_BASE_ADDR(DSAT_SS_PH_MS_VALS, (void **)&ph_ss_ms_val, hybr_subs_id);

    net_reg_state_ptr = &ph_ss_ms_val->dsat_net_reg_state;
    creg_reported_ptr = &net_reg_state_ptr->dsat_creg_reported;

    creg_n = (dsat_num_item_type) dsatutil_get_val( DSATETSI_EXT_CREG_IDX,
                                    dsat_get_qcsimapp_val(), 0, NUM_TYPE );

    res_buff_ptr->used =
      (word)snprintf((char*)res_buff_ptr->data_ptr,res_buff_ptr->size,
                            "%s: %d,%d",
                            parse_table->name,
                            creg_n,
                            creg_reported_ptr->net_domain[DSAT_NET_REG_DOMAIN_CS]);

    /* location id, cell id is required only when CREG = 2*/
    /* Cell ID should be in  hex format */
    /* Display Cell id and location id only when camped */
    if ( creg_n == CREG_DISP_LAC_CELL_ID )
    {
      /* $QCHCREG requires only lac and cell_id */
      if ( ( ( creg_reported_ptr->net_domain[DSAT_NET_REG_DOMAIN_CS] == DSAT_NET_REG_HOME ) ||
             ( creg_reported_ptr->net_domain[DSAT_NET_REG_DOMAIN_CS] == DSAT_NET_REG_ROAMING ) ) &&
           ( ( creg_reported_ptr->cell_id != 0xFFFFFFFF ) &&
             ( creg_reported_ptr->plmn_lac != 0XFFFF ) ) )
      {
        (void) snprintf( fmt_string,
                         sizeof(fmt_string),
                         ",\"%X\",\"%X\",%X",
                         creg_reported_ptr->plmn_lac,
                         (unsigned int) creg_reported_ptr->cell_id,
                         net_reg_state_ptr->act );

        res_buff_ptr->used +=
          (word) snprintf( (char*) &res_buff_ptr->data_ptr[res_buff_ptr->used],
                           res_buff_ptr->size - res_buff_ptr->used,
                           "%s", fmt_string );
      }
    }

    result = DSAT_OK;
  }

  return result;
} /* dsatvend_exec_qchcreg_cmd */
#endif /* FEATURE_SGLTE */

/*===========================================================================

FUNCTION DSATVEND_REG_APN_DISABLE_FLAG_CHG_CB

DESCRIPTION
  This API is exposed to 3GPP MH to register for a callback when ever APN 
  disable flag is updated in Profile using ATCmd

DEPENDENCIES
  None

RETURN VALUE
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success for local execution

SIDE EFFECTS
  None

===========================================================================*/
void  dsatvend_reg_apn_disable_flag_chg_cb
(
  void *func_ptr
)
{
  if(func_ptr != NULL)
  {
    dsat_apn_disable_flag_chg_cb_fptr = func_ptr;
    
    DS_AT_MSG0_HIGH("reg_apn_disable_flag_chg_cb Success");
  }

  return;
}


