/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                        D A T A   S E R V I C E S
                A T   C O M M A N D   P R O C E S S O R

               C O M M O N   C O M M A N D   T A B L E S

GENERAL DESCRIPTION
  This module contains the command tables and data definitions required
  to define the AT modem commands that are common across all modes of
  operation (ETSI, CDMA2000, IS-95, etc.).

EXTERNALIZED FUNCTIONS
  dsatctab_data_init
    This function initializes the AT command defaults, limits, and lists
    that depend on a run-time determination of the hardware.

    
INITIALIZATION AND SEQUENCING REQUIREMENTS
  None

   Copyright (c) 2001 - 2014 by Qualcomm Technologies Incorporated.
   All Rights Reserved.
   Qualcomm Confidential and Proprietary
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/


/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $PVCSPath: L:/src/asw/MM_DATA/vcs/dsatctab.c_v   1.10   11 Nov 2002 14:02:12   sbandaru  $
  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/atcop/src/dsatctab.c#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
7/27/14    pg      ATCoP changes for Data Plus Data.
03/11/14   tk      Optimized debug macros usage in ATCoP.
01/10/14   sc      Added support for $QCPDPCFGEXT command.
12/25/13   tk      Enhanced $QCSIMAPP command for better user experience.
11/14/13   tk      Extended +IPR and $QCTER commands for bit-rates up to 4Mbps.
11/12/13   sc      Added support for $QCCSGCOPS command.
10/18/13   sc      Added support for $QCNSP, $QCSIMT, $QCRCIND commands and
                   REMOTE CALL END, REMOTE RING, REMOTE ANSWER
                   unsolicited result codes.
10/01/13   sc      Adding support for $QCACQDBC command to clear acq_DB
08/21/13   pg      Added support for emergency calls, operator res pco, mcc and mnc fields in $QCPDPCFGE.
07/25/13   pg      Modified $QCPDPCFGE to COMMON_CMD
07/16/13   pg      Fixed no ^MODE URC when UE goes out of service and comes back
07/16/13   pg      Fixed ^SYSINFO o/p for "SIM is not inserted"
06/05/13   tk      ATCoP changes for SGLTE support on Dime Plus.
05/17/13   sk      Added data retry fields to $QCPDPCFGE command.
05/16/13   sk      $QCCOPS command for Extension of network search.
03/27/13   tk      Fixed issues in dual stack commands.
01/04/13   tk      ATCoP changes for DSDX C/G+G with True World Mode Support.
11/30/12   tk      ATCoP changes for Triton DSDA.
09/24/12   sk      Added $QCRSRP and $QCRSRQ command Support.
08/06/12   tk      Added support for $QCDRX command.
06/30/12   sk      Added UMTS_CMD check.
05/18/12   tk      Migrated to MSG 2.0 macros
05/18/12   sk      Added APN bearer field to $QCPDPCFGE.
03/07/12   sk      Made CLCC common for all modes.
02/17/11   ua      Added support for $QCRMCALL.
02/06/12   nc      Added support for ^SPN and URC Queue.
01/19/12   sk      Feature cleanup.
09/27/11   mk      Added support for $QCCLAC command.
07/05/11   nc      Added support for ^MODE,^CARDMODE,^SYSCONFIG,^SYSINFO,^DSCI.
06/07/11   nc      Added support for $QCAPNE command.
05/13/11   ad      Added $QCPDPIMSCFGE support.
04/01/11   ttv     Added changes to use get/set functions.
03/14/11   ad      Added APN class support in $QCPDPCFGE command.
02/28/11   ad      Allowed L and M commands to execute in restricted mode . 
02/16/11   ttv     Added DSDS changes for consolidated profile family.
01/11/11   ad      Removed pending variable .
01/11/11   ad      Remove extern usage.used get/set API for command associated  
                   value pointers.
10/25/10   ad      Init Changes for DSDS. 
09/28/10   ad      Added LTE support in +WS46.
08/08/10   ua      Allowing ATL, ATM, ATS0 commands in RESTRICTED mode for 
                   Fusion solution. 
07/08/10   ad      Added support for $ECALL command.
05/31/10   ad      Added support for S7 command.
05/17/10   ad      Added Support for $QCPDPCFGE.
05/10/10   kk      Mainlining pending GOBI changes.
01/18/10   bs      Added support for +PACSP.
01/20/10   kk      Extended +CFUN to CDMA/MM targets.
01/15/10   ua      PBM AT Commands support for DSDS and CSIM. 
11/16/09   ca      Added support for MMGSDI Auth CME errors.
06/10/09   ua      Added support for ^PREFMODE command. 
12/15/09   nc      Featurisation changes for LTE.
08/04/09   nc      Added support for *CNTI.
07/15/09   sa      Added support for $CSQ command.
07/14/09   bs      Added support for $CREG & $CCLK commands.
06/12/09   ua      Added support for $QCBANDPREF command.
04/29/09   ua      Fixed compiler warnings. 
01/19/09   bs      Added support for $qcctm.
12/12/08   ua      Fixed Off target lint errors.
11/28/08   cs      OffTarget Lint and Compiler warnings.
10/23/08   bs      Added support for 1X AT Phonebook commands.
09/30/08   ua      Changing the default values for &E and X to be 0 and 1 
                   respectively to display the connect rate as radio rate 
                   by default.
10/23/08   sa      Added support for $QCPDPFAM command.
10/18/08   pp      Added support for SLIP interface (AT$QCSLIP).
02/20/08   sa      Added support for $QCSQ command.
02/18/08   sa      Added modification for Passport Feature support.
01/31/08   bs      Fixing $QCSIMSTAT featurization issue.
01/21/08   bs      Added new command $QCSIMSTAT for sim status.
01/11/08   sa      Added support for $QCANTE and $QCRPW commands.
10/26/07   ua      Added support for AT&S command. 
10/09/07   ss      Added support for $QCGANSM, $QCGARL, $QCGAPL commands.
10/31/07   sa      Allow Q,V,E,+FCLASS,+GMM,+GMR,+GMI,+GSN in restricted mode.
09/13/07   sa      Feature wrapped S2 register in FEATURE_DATA_S2_REG.
08/17/07   ua      Added support for $QCCNMI command which compensates CNMI 
                   GCF test cases.
08/06/07   pp      Moved AT\Q command to mainline.
07/06/07   ar      Added export of dsat_ds_dflm variable.
02/07/07   pp      Added full support for AT&W, AT\S commands.
05/25/07   sa      ATI and ATZ commands are allowed in restricted mode.
05/15/07   sa      &F should be allowed in restricted mode as per spec.
04/23/07   pp      Lint Medium fixes.
04/11/07   ua      Modifications as per KDDI requirements. 
04/03/07   pkp     New commands: &E, \V, &S, \S, &W, S2, S30, S103, S104, %V.
05/22/06   rsl     Add support for at$qcsysmode to return current sysmode, 
                   possible values returned: HSDPA/HSUPA/HSDPA+HSUPA/WCDMA/GSM.
11/03/05   ar      Make +WS46 responses consistent with spec CR.
09/08/05   ar      Add support for $QCSDFTEST vendor command.
04/14/05   sb      Changes for incoming CS call when serializer is enabled
03/30/05   tkk     Featurized ATB command.
03/03/05   snb     Add $QCDGEN command for data generation in +CGACT context
                   activation.
03/03/05   tkk     Added NO_DISPLAY attribute to ATB command to force AT&V not
                   to display ATB's parameter value.
03/02/05   tkk     Added support for ATB command.
01/24/05   hap     Adding support for AT\Q command
01/24/05   hap     Adding support for AT\V command
01/11/05   ar      Add support for +WS46 command.
09/17/04   snb     Use dsatact_exec_ate_cmd handler.
11/14/03   snb     Change ATX default to 0 in support of CSD call data rate 
                   reporting on CONNECT.
10/29/03   snb     Added support for $QCPINSTAT command and for async IMSI 
                   read from either card slot by +CIMI command.
10/08/03   ar      Add wrapper FEATURE_DSAT_BREW_SUPPORT to BREW commands.
08/15/03   ar      Added support for $QCSLOT command.
08/13/03   sb      Call dsatparm_exec_fclass_cmd() from +FCLASS command table
08/13/03   rsl     Moved at+fclass to dsatctab.c
08/13/03   snb     Added support for $QCPDPLT command
07/11/03   snb     Correct feature definitions.
07/07/03   sb      Added AT$BREW command
05/28/03   sb      Fix for WCDMA CS Calls
02/20/02   sb      changed the range of S10 register from 1-255
02/19/03   ar      Fix DSPE build error.
02/11/03   ar      Adjust +DS string paramater range
01/27/03   ar      Added 230400 baud to common +IPR and $QCTER value lists.
01/16/03   ar      Migrated context cmds to persistent PDP profile support
01/07/03   rsl     Removed dsat_qcpma_val, no longer used.
11/11/02   sb      NV intialization in dsatctab_data_init()
11/07/02   ar      Added FEATURE_GSM_GPRS to FEATURE_DATA_WCDMA_PS wrappers
10/15/02   sb      Changed the power on default of &C in JCDMA mode
04/17/02   rc      Removed FEATURE wrap FEATURE_DS_SOCKETS around dns 
                   variables.
04/11/02   ar      Add support for $QCPDPP command
09/12/01   ar      Added commands to extended AT command table:
                     +GMI,+GMM,+GMR,+GSN,+GCAP,I
08/27/01   sjd     Initial release of new VU structure to MSM archives.

===========================================================================*/


/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"


#include "target.h"
#include "dsati.h"
#include "msg.h"
#include "dsatparm.h"
#include "dsatact.h"
#include "dsatctab.h"
#include "dsatme.h"
#include "dsatvend.h"
#include "dstaski.h"
#include "nv.h"

#ifdef FEATURE_DSAT_ETSI_DATA
#include "dsatetsipkt.h"
#include "dsumtspdpreg.h"
#endif /* FEATURE_DSAT_ETSI_DATA */

#ifdef FEATURE_DSAT_ETSI_MODE
#include "dsatetsictab.h"
#ifdef FEATURE_DSAT_DEV_CMDS
#include "dsatetsicall.h"
#endif /* FEATURE_DSAT_DEV_CMDS */
#include "dsatme.h"
#include "dsatcmif.h"
#ifdef FEATURE_ETSI_SMS
#include "dsatetsisms.h"
#endif /* FEATURE_ETSI_SMS */
#ifdef FEATURE_DATA_ETSI_PIN
#include "dsatetsime.h"
#endif /* FEATURE_DATA_ETSI_PIN */
#endif /* FEATURE_DSAT_ETSI_MODE */
#include "ds_rmnet_defs.h"

/*===========================================================================

                    REGIONAL DEFINITIONS AND DECLARATIONS

===========================================================================*/

/*---------------------------------------------------------------------------
  NV interface data 
---------------------------------------------------------------------------*/
nv_item_type         ds_nv_item;                  /* actual data item     */

/*===========================================================================

            LOCAL DEFINITIONS AND DECLARATIONS FOR MODULE

  This section contains local definitions for constants, macros, types,
  variables and other items needed by this module.

===========================================================================*/

/*---------------------------------------------------------------------------
                           Defaults and limits
---------------------------------------------------------------------------*/


dsat_subs_info_type dsat_qcsimapp_info[MAX_SUBS+1];  /* for TSTS/CSIM  */
boolean dsatme_pending_mmgsdi_command[MAX_SUBS] = {FALSE,FALSE};

dsat_global_state dsat_pending_state[MAX_PORT];

#if defined(FEATURE_ETSI_PBM) || defined(FEATURE_DSAT_CDMA_PBM)
/* +CPBS Command */
/* the sequence of the storage type is important, it has to be the same as
   that of dsatetsi_pb_pbm_table 
   DEFAULT_CPB_STORAGE is SM, we use macro here to synchronize it with 
   me_cpbs_default */
#ifdef FEATURE_PBM_USE_EFS_PB
const dsat_string_item_type dsat_cpbs_storage_valstr [][8] =
{ DEFAULT_CPB_STORAGE, "DC", "FD", "LD","MC",DEFAULT_NV_CPB_STORAGE, "RC", "EN","ON",""};

LOCAL const dsat_string_item_type dsat_cpbs_storage_tststr [] =
{ "(\"SM\",\"DC\",\"FD\",\"LD\",\"MC\",\"ME\",\"RC\",\"EN\",\"ON\")" };

#else
const dsat_string_item_type dsat_cpbs_storage_valstr [][8] =
{ DEFAULT_CPB_STORAGE, "DC", "FD", "LD","MC", "RC", "EN","ON",""};

LOCAL const dsat_string_item_type dsat_cpbs_storage_tststr [] =
{ "(\"SM\",\"DC\",\"FD\",\"LD\",\"MC\",\"RC\",\"EN\",\"ON\")" };

#endif/*FEATURE_PBM_USE_EFS_PB*/

const def_list_type dsat_cpbs_list =
{ 0, dsat_cpbs_storage_valstr, dsat_cpbs_storage_tststr };

#ifdef FEATURE_TRIPLE_SIM
LOCAL const dflm_type dsat_qcpbmpref_dflm[]  =
{ 
  { 0, 0, 6 }, /* 0 -- Slot 1 Global Phone Book (default)*/
               /* 1 -- Slot 2 Global Phone Book */
               /* 2 -- Slot 2 Global Phone Book */
               /* 3 -- GW Primay Local Phone Book */
               /* 4 -- GW Primay Local Phone Book */
               /* 5 -- GW Seconday Local Phone book */
               /* 6 -- 1X Primary local Phone book */
};
#else
LOCAL const dflm_type dsat_qcpbmpref_dflm[]  =
{ 
  { 0, 0, 4 }, /* 0 -- Slot 1 Global Phone Book (default)*/
               /* 1 -- Slot 2 Global Phone Book */
               /* 2 -- GW Primay Local Phone Book */
               /* 3 -- GW Seconday Local Phone book */
               /* 4 -- 1X Primary local Phone book */
};
#endif /* FEATURE_TRIPLE_SIM */
#endif /* defined(FEATURE_ETSI_PBM) || defined(FEATURE_DSAT_CDMA_PBM) */

LOCAL const dflm_type dsat_amp_c_dflm [] =
{
  { 2  , 0  , 2    },
} ;

LOCAL const dflm_type dsat_amp_d_dflm [] =
{
  { 2  , 0  , 2    },
} ;

LOCAL const dflm_type dsat_amp_f_dflm [] =
{
  { 0  , 0  , 0    },
} ;

LOCAL const dflm_type dsat_amp_e_dflm [] =
{
  { 0 , 0 , 1 },
} ;

LOCAL const dflm_type dsat_amp_s_dflm [] =
{
  { 0 , 0 , 1 },
} ;

LOCAL const dflm_type dsat_amp_w_dflm [] =
{
  { 0 , 0 , 0 },
} ;

LOCAL const dflm_type dsat_fclass_dfl =
{ 0, 0, 2 };

LOCAL const dflm_type dsat_icf_dflm [] =
{
  { 3  , 3  , 3    },
  { 3  , 0  , 3    },
} ;
LOCAL const dflm_type dsat_ifc_dflm [] =
{
  { 2  , 0  , 3    },
  { 2  , 0  , 2    },
} ;

/*  The bit rates in the "valstr" table must match exactly the  */
/*  value and order of the "sio_baudrate_type" enum in sio.h    */
/*  Unused values must have space strings as placeholders       */
/*  The "valstr" table must be terminated with a null string    */

/*  Bit rates for MSM 2.3 and later                             */
/* Applies to ETSI & IS707 modes */  
 const dsat_string_item_type dsat_ipr_valstr [][8] =
  { " ", " ", " ", " ", " ", " ", "300", "600", "1200","2400",
    "4800", "9600", " ", "19200", "38400", "57600", " ", "115200",
#ifdef FEATURE_UART_TCXO_CLK_FREQ
    "230400", " ", "921600", "2000000", "2900000", "3000000", "3200000"
    , "3686400", "4000000",
#endif /* FEATURE_UART_TCXO_CLK_FREQ */
    "" };

LOCAL const dsat_string_item_type dsat_ipr_tststr [] =
  { "(),"
#ifdef FEATURE_UART_TCXO_CLK_FREQ
    "(300,600,1200,2400,4800,9600,19200,38400,57600,115200,230400,921600,2000000,2900000,3000000,3200000,3686400,4000000)"
#else
    "(300,600,1200,2400,4800,9600,19200,38400,57600,115200)"
#endif /* FEATURE_UART_TCXO_CLK_FREQ */
  };

/* Initialized at powerup by dsatctab_data_init().This initialization to 
MSM 2.2 is "just in case". */
LOCAL def_list_type dsat_ipr_list =
{
  17,  /*  115200 */
  dsat_ipr_valstr,  
  dsat_ipr_tststr
} ;

LOCAL const dsat_string_item_type dsat_qcdmr_valstr [][8] =
  { " ", " ", " ", " ", " ", " ", " ", " ", " ",
    " ", " ", " ", " ", " ", "38400", "57600", " ", "115200", " ", "" };

LOCAL const dsat_string_item_type dsat_qcdmr_tststr [] =
  { "(38400,57600,115200)" };

/* $QCTER baud rates should match those for +IPR */
/* Applies to ETSI & IS707 modes */
LOCAL const dsat_string_item_type dsat_qcter_valstr [][8] =
  { " ", " ", " ", " ", " ", " ", "300", "600", "1200","2400",
    "4800", "9600", " ", "19200", "38400", "57600", " ", "115200",
#ifdef FEATURE_UART_TCXO_CLK_FREQ
    "230400", " ", "921600", "2000000", "2900000", "3000000", "3200000"
    , "3686400", "4000000",
#endif /* FEATURE_UART_TCXO_CLK_FREQ */
    "" };

LOCAL const dsat_string_item_type dsat_qcter_tststr [] =
  { "(),"
#ifdef FEATURE_UART_TCXO_CLK_FREQ
    "(300,600,1200,2400,4800,9600,19200,38400,57600,115200,230400,921600,2000000,2900000,3000000,3200000,3686400,4000000)"
#else
    "(300,600,1200,2400,4800,9600,19200,38400,57600,115200)"
#endif /* FEATURE_UART_TCXO_CLK_FREQ */
  };

LOCAL const def_list_type dsat_qcdmr_list =
{
  17,  /*  115200 */ 
  dsat_qcdmr_valstr,
  dsat_qcdmr_tststr
} ;

LOCAL def_list_type dsat_qcter_list =
{
  17,  /*  115200 */
  dsat_qcter_valstr,    
  dsat_qcter_tststr
} ;

LOCAL const dflm_type dsat_e_dflm [] =
{
  { 1  , 0  , 1    },
} ;

LOCAL const dflm_type dsat_l_dflm [] =
{
  { 0  , 0  , 3    },
} ;

LOCAL const dflm_type dsat_m_dflm [] =
{
  { 0  , 0  , 2    },
} ;

LOCAL const dflm_type dsat_q_dflm [] =
{
  { 0  , 0  , 1    },
} ;

LOCAL const dflm_type dsat_s0_dflm [] =
{
  { 0  , 0  , 255  },
} ;

/* V.25 defines a range of 1 - 154            */
/* IS707 has a range of 1 - 255, IS-707 Wins! */
LOCAL const dflm_type dsat_s10_dflm [] =
{
  { 14 , 1  , 255  },
} ;

LOCAL const dflm_type dsat_s11_dflm [] =
{
  { 95 , 50 , 255  },
} ;

LOCAL const dflm_type dsat_s2_dflm [] =
{
  { 43 , 0 , 127 },
} ;

LOCAL const dflm_type dsat_o_dflm[]=
{
  { 0, 0, 2 },
};

LOCAL const dflm_type dsat_s3_dflm [] =
{
  { 13 , 0  , 127  },
} ;
LOCAL const dflm_type dsat_s4_dflm [] =
{
  { 10 , 0  , 127  },
} ;

LOCAL const dflm_type dsat_s5_dflm [] =
{
  { 8  , 0  , 127  },
} ;

LOCAL const dflm_type dsat_x_dflm [] =
{
  { 1  , 0  , 4    },
} ;

LOCAL const dflm_type dsat_s6_dflm [] =
{
  { 2  , 2  , 10   },
} ;

LOCAL const dflm_type dsat_s7_dflm [] =
{
  { 0 , 0 , 255  },/* 0 - Default - Will not Start the timer  */
} ;

LOCAL const dflm_type dsat_s8_dflm [] =
{
  { 2  , 0  , 255  },
} ;

LOCAL const dflm_type dsat_s9_dflm [] =
{
  { 6  , 0  , 255  },
} ;

LOCAL const dflm_type dsat_s30_dflm [] =
{
  { 0 , 0 , 255 },
} ;

LOCAL const dflm_type dsat_s103_dflm [] =
{
  { 1 , 0 , 2 },
} ;

LOCAL const dflm_type dsat_s104_dflm [] =
{
  { 1 , 0 , 2 },
} ;

LOCAL const dflm_type dsat_slash_s_dflm [] =
{
  { 0 , 0 , 0 },
} ;

LOCAL const dflm_type dsat_slash_v_dflm [] =
{
  { 0 , 0 , 1 },
} ;

LOCAL const dflm_type dsat_v_dflm [] =
{
  { 1  , 0  , 1    },
} ;

LOCAL const dflm_type dsat_z_dflm [] =
{
  { 0  , 0  , 0    },
} ;

/* AT\Q command */
LOCAL const dflm_type dsat_slash_q_dflm [] =
{
  { 3, 0, 1 },                 
  { 3, 3, 3 },
  { MAX_BROKEN_RANGE, 0, 0 },
};

/* +CMEE Command */
LOCAL const dsat_string_item_type dsat_cmee_valstr [][8] =
{ "0", "1", "2", ""};

LOCAL const dsat_string_item_type dsat_cmee_tststr [] =
{ "(0,1,2)" };

const def_list_type dsat_cmee_list =
{ 2, dsat_cmee_valstr, dsat_cmee_tststr };

#ifdef FEATURE_DSAT_DEV_CMDS
#ifdef FEATURE_DSAT_ETSI_MODE
/* $CREG Command */
LOCAL const dflm_type dsat_qccreg_dfl[] =
{ 
#ifdef FEATURE_GSM_RR_CELL_CALLBACK
  { 0, 0, 2 },    /* 0 - disable, 1 - enable, 
     2 - enable with location, cell id and PSC if present */
#else
  { 0, 0, 1 },     /* 0 - disable, 1 - enable */
#endif /* FEATURE_GSM_RR_CELL_CALLBACK */
};
#endif /* ( FEATURE_DSAT_ETSI_MODE) */
#endif /* FEATURE_DSAT_DEV_CMDS */


/* +DR Command */
LOCAL const dflm_type dsat_dr_dflm [] =
{
  { 0  ,  0  , 1    }
};
#ifdef FEATURE_TTY
LOCAL const dflm_type dsat_qcctm_dflm [] =
{
  { 0  ,  0  , 1    }
};
#endif/* FEATURE_TTY */
/* +DS Command */
const dflm_type dsat_ds_dflm [] =
{
  { 0  ,  0  , 3    },  /* Direction       */
  { 0  ,  0  , 0    },  /* Negotiation     */
  { 2048, 512, 2048 },  /* Max Dictionary  */
  { 6  ,  6  , 6   }    /* Max String      */
} ;

#ifdef FEATURE_DSAT_ETSI_MODE
/* +WS46 Command */
#ifdef FEATURE_DSAT_LTE
/* G+L or W+L combination is not supported by lower layers */
LOCAL const dsat_string_item_type dsat_ws46_valstr [][8] =
  { "12", "22", "25", "28", "29", "" };
LOCAL const dsat_string_item_type dsat_ws46_tststr [] =
  { "(12,22,25,28,29)" };
#elif defined( FEATURE_WCDMA )
LOCAL const dsat_string_item_type dsat_ws46_valstr [][8] =
  { "12", "22", "25", "" };
LOCAL const dsat_string_item_type dsat_ws46_tststr [] =
  { "(12,22,25)" };
#else
LOCAL const dsat_string_item_type dsat_ws46_valstr [][8] =
  { "12", "" };
LOCAL const dsat_string_item_type dsat_ws46_tststr [] =
  { "(12)" };
#endif /* defined ( FEATURE_DSAT_LTE ) || defined( FEATURE_WCDMA ) */
const def_list_type dsat_ws46_list[] =
{
  { 0, dsat_ws46_valstr, dsat_ws46_tststr },
};
#endif /* FEATURE_DSAT_ETSI_MODE */
/* $QCSIMAPP */
LOCAL const dflm_type dsat_qcsimapp_dflm [] =
{
#ifdef FEATURE_TRIPLE_SIM 
  { 0, 0, 3 }
#elif defined(FEATURE_DUAL_SIM)
  { 0, 0, 2 }
#else
  { 0, 0, 0 }
#endif /* FEATURE_DUALSTANDBY */
} ;
#ifdef FEATURE_DSAT_ETSI_MODE
/* $QCSIMISTAT Command */
LOCAL const dflm_type dsat_qcsimstat_dflm [] =
{ 
#ifdef FEATURE_TRIPLE_SIM 
        { 0, 0, 2 }
#elif defined(FEATURE_DUAL_SIM)
        { 0, 0, 1 }
#else
        { 0, 0, 0 }
#endif /* FEATURE_DUALSTANDBY */
};
#endif /* FEATURE_DSAT_ETSI_MODE*/

#ifdef FEATURE_DSAT_ETSI_DATA
/* $QCPDPP Command */
LOCAL const dflm_type dsat_qcpdpp_cid_dflm [] =
{
  { 1  , 1  , DS_UMTS_MAX_PDP_PROFILE_NUM },
} ;

LOCAL const dflm_type dsat_qcpdpp_auth_dflm [] =
{
  { 0  , 0  , 3  },
} ;

/*  Use over-long strings to validate max permitted length */
/* Accomodate quotes in addition to DS_MAX_QCPDP_STRING_LEN characters */
LOCAL const mixed_def_u_type dsat_qcpdpp_uname_string_len = 
                                            {{DS_UMTS_MAX_QCPDP_STRING_LEN+5}};
LOCAL const mixed_def_u_type dsat_qcpdpp_passw_string_len = 
                                            {{DS_UMTS_MAX_QCPDP_STRING_LEN+5}};



LOCAL const mixed_def_s_type dsat_qcpdpp_cid_def_val =
{
  CONFIG | LOCAL_TEST, (mixed_def_u_type *) dsat_qcpdpp_cid_dflm
};

LOCAL const mixed_def_s_type dsat_qcpdpp_auth_def_val =
{
  CONFIG | LOCAL_TEST, (mixed_def_u_type *) dsat_qcpdpp_auth_dflm
};

LOCAL const mixed_def_s_type dsat_qcpdpp_passw_def_val =
{
  CONFIG | STRING | NO_DISPLAY | NO_QUOTE,
  (mixed_def_u_type *) &(dsat_qcpdpp_passw_string_len)
};

LOCAL const mixed_def_s_type dsat_qcpdpp_uname_def_val =
{
  CONFIG | STRING | YES_QUOTE,
  (mixed_def_u_type *) &(dsat_qcpdpp_uname_string_len)
};

/*lint -save -e708 */
LOCAL const mixed_def_s_type * dsat_qcpdpp_mixed_dfl[4] = 
{
  &dsat_qcpdpp_cid_def_val,
  &dsat_qcpdpp_auth_def_val,
  &dsat_qcpdpp_passw_def_val,
  &dsat_qcpdpp_uname_def_val
};
/*lint -restore */
/*lint -restore */


LOCAL const dflm_type dsat_qcpdplt_dflm [] =
{
  { QCPDPLT_DEF_VAL, 0, 1 },
} ;

#endif /* FEATURE_DSAT_ETSI_DATA */


#ifdef FEATURE_DATA_UCSD_SCUDIF_TEST
/* SCUDIF Test Modes */
LOCAL const dsat_string_item_type dsat_qcscftest_dir_valstr [][8] =
  { "MO", "MT", "" };

LOCAL const dsat_string_item_type dsat_qcscftest_dir_tststr [] =
  { "(MO,MT)" };

/* List of test configurations defined in CSData code. */
const dsat_string_item_type dsat_qcscftest_mode_valstr [][8] =
{ "0", "1", "2", "3", "4", "5", "6", "7", "8", "" };

LOCAL const dsat_string_item_type dsat_qcscftest_mode_tststr [] =
{ "(0,1,2,3,4,5,6,7,8)" };

const def_list_type dsat_qcscftest_list[] =
{
  { 0, dsat_qcscftest_dir_valstr, dsat_qcscftest_dir_tststr },
  { 0, dsat_qcscftest_mode_valstr, dsat_qcscftest_mode_tststr }
};


uint8 dsat_qcscftest_exp_val[2];
#endif /* FEATURE_DATA_UCSD_SCUDIF_TEST */
#ifdef FEATURE_ECALL_APP
/*
dsat_ecall_val[0]  -> Start/Stop the eCall 0 - Stop ; 1 - Start
dsat_ecall_val[1]  -> Type of the eCall    0 - Test ; 1 - Emergency
dsat_ecall_val[2]  -> Activation type      0 - Manual; 1- Automatic
*/
LOCAL const dsat_string_item_type dsat_ecall_session_valstr [][8] =
{
  "0", "1", ""
};

LOCAL const dsat_string_item_type dsat_ecall_session_tststr [] =
{ 
  "(0,1)" 
};

LOCAL const dsat_string_item_type dsat_ecall_act_type_valstr [][8] =
{
  "0", "1", ""
};

LOCAL const dsat_string_item_type dsat_ecall_act_type_tststr [] =
{ 
  "(0,1)" 
};

LOCAL const dsat_string_item_type dsat_ecall_type_of_call_valstr [][8] =
{
  "0", "1", "2",""
};

LOCAL const dsat_string_item_type dsat_ecall_type_of_call_tststr [] =
{ 
  "(0,2)" 
};

const def_list_type dsat_ecall_list[] =
{
  { 0, dsat_ecall_session_valstr, dsat_ecall_session_tststr },
  { 0, dsat_ecall_type_of_call_valstr, dsat_ecall_type_of_call_tststr },
  { 0, dsat_ecall_act_type_valstr, dsat_ecall_act_type_tststr }

} ;
#endif /* FEATURE_ECALL_APP */

#ifdef FEATURE_DSAT_TEST_32ARG 

/*---------------------------------------------------------------------------
  The following def and val's are for test purpose only.
  AT+TST32, AT+TTT32, and AT$TVT32 are used to test 32 bit argument support
  Since we don't currently have a AT command with 32 bit argument,
  we have to make up commands to test the functionality.
---------------------------------------------------------------------------*/


LOCAL const dflm_type dsat_tst32_dflm[] = 
{
  { 0x075BCD15,  /* 123456789 default*/
    0x00000fff,  /* 4095  */
    ~0         },/* 0xffffffff, 4294967295 */

  { 0x075BCD15,  /* 123456789 */
    0x0000ffff,  /* 65535  */
    ~0         } /* 4294967295 */
};

#define DSAT_TUT32_FIRST_STR_LEN 21

LOCAL dsat_string_item_type 
dsat_tut32_first_str_val[DSAT_TUT32_FIRST_STR_LEN + 1];

LOCAL dsat_mixed_param_val_type dsat_tut32_val[2] = {
  dsat_tut32_first_str_val, 0
};

LOCAL uint16 dsat_tut32_first_str_len = DSAT_TUT32_FIRST_STR_LEN;

LOCAL mixed_def_s_type dsat_tut32_first_def =
{ CONFIG | STRING, (mixed_def_u_type *) & dsat_tut32_first_str_len};

LOCAL dflm_type dsat_tut32_second_dflm =
{ 0x075BCD15,   /* 123456789  */
  0x0000ffff,   /* 65535      */
  ~0         }; /* 4294967295 */

LOCAL mixed_def_s_type dsat_tut32_second_def =
{ CONFIG | LOCAL_TEST, (mixed_def_u_type *) &dsat_tut32_second_dflm };

LOCAL mixed_def_s_type * dsat_tut32_mixed_dfl[] =
{
  &dsat_tut32_first_def, &dsat_tut32_second_def
};

#endif /* FEATURE_DSAT_TEST_32ARG */

#ifdef FEATURE_DATA_TE_MT_PDP
/* $QCGANSM command */
const dsat_string_item_type dsat_qcgansm_state_valstr [][8] =
{ "0", "1", "2","" } ;

LOCAL const dsat_string_item_type dsat_qcgansm_state_tststr [] =
{ "(0-2)" };

LOCAL def_list_type dsat_qcgansm_list =
{
  0,
  dsat_qcgansm_state_valstr,
  dsat_qcgansm_state_tststr
};


#endif /* FEATURE_DATA_TE_MT_PDP */

#ifdef FEATURE_DSAT_DEV_CMDS
#ifdef FEATURE_DSAT_ETSI_MODE
/* *CNTI command*/
LOCAL const dflm_type dsat_qccnti_dflm [] =
{
  { 0 , 0 , 2 } /* 0 -- default indicates Current Technology */
};
dsati_cnti_tech_enum_type dsat_qccnti_tech;
#endif /* FEATURE_DSAT_ETSI_MODE*/
#endif /* FEATURE_DSAT_DEV_CMDS */
/* ^PREFMODE variables  */
LOCAL const dflm_type dsat_prefmode_dfl[] =
{
  {0,0,8}  /* Defaulting ^PREFMODE to Automatic 
              But valid values are 0 - Automatic, 2-CDMA, 4-HDR, 8-HYBRID */
};

LOCAL const dflm_type dsat_dsci_dflm [] =
{
  { 0  , 0  , 1  },
} ;

/* +CFUN Command */
/* Functionality */
LOCAL const dflm_type dsat_cfun_fun_dfl[] =
{ 
  { 1, 0, 1 },
#if defined(FEATURE_DSAT_EXTENDED_CMD)
  { 1, 4, 6 },
#else
  { 1, 4, 7 },
#endif /* defined(FEATURE_DSAT_EXTENDED_CMD) */
  { MAX_BROKEN_RANGE, 0, 0 }
};

LOCAL const mixed_def_s_type dsat_cfun_fun_def_val =
{
  CONFIG | NO_RESET | NO_DISPLAY | LOCAL_TEST | BROKEN_RANGE,
  (mixed_def_u_type *) dsat_cfun_fun_dfl
};

/* Note: reset parameter value of 1 is only supported for full
   functionality */
LOCAL const dflm_type dsat_cfun_rst_dfl[] =
{
  { 0, 0, 1 }
};

LOCAL const mixed_def_s_type dsat_cfun_rst_def_val =
{
  CONFIG | NO_RESET | NO_DISPLAY | LOCAL_TEST,
  (mixed_def_u_type *) dsat_cfun_rst_dfl
};

LOCAL const mixed_def_s_type * dsat_cfun_mixed_dfl[2] = 
{
  &dsat_cfun_fun_def_val,
  &dsat_cfun_rst_def_val
};


/* $QCDEFPROF Command */
LOCAL const dflm_type dsat_qcdefprof_dfl[] =
{ 
  { 0, 0, 1 },    /* family */
#ifdef FEATURE_DUAL_SIM
  { 1, 1, DS_SUBSCRIPTION_MAX },    /* subscription id */
#else  /* FEATURE_DUAL_SIM */
  { 1, 1, 1 },    /* subscription id */
#endif /* FEATURE_DUAL_SIM */
  { 1, 1, 16 },   /* profile num */
};

dsat_qcrmcall_s_type dsat_qcrmcall_info;
#ifdef FEATURE_DSAT_LTE
LOCAL const dflm_type dsat_qcdrx_dflm[] =
{
  { 6, 0, 0 },
  { 6, 6, 9 },
  { MAX_BROKEN_RANGE, 0, 0 },
};
#endif /* FEATURE_DSAT_LTE*/
/*^MODE Command*/
LOCAL const dflm_type dsat_mode_dflm[] =
{
  { 0, 0, 1},
};
/*$QCRCIND Command*/
LOCAL const dflm_type dsat_qcrcind_dflm[] =
{
  { 0, 0, 1},
};

/* $QCCOPS Command*/
extern const mixed_def_s_type * dsat_cops_mixed_dfl[6];

/*--------------------------------------------------------------------------
                          Common Command Tables 
--------------------------------------------------------------------------*/

/* Basic common AT command table. */
const dsati_cmd_type dsat_basic_table [] =
{

  { "&C",       CONFIG | SIMPLE_DEC | COMMON_CMD,
    SPECIAL_NONE, 1,DSAT_BASIC_N_C_IDX,   &dsat_amp_c_dflm[0],
    dsatact_exec_ampc_cmd,            NULL },

  { "&D",       CONFIG | SIMPLE_DEC | COMMON_CMD,
    SPECIAL_NONE,1,DSAT_BASIC_N_D_IDX,    &dsat_amp_d_dflm[0],
    dsatparm_exec_param_cmd,            NULL },

  { "&E",       CONFIG | SIMPLE_DEC | COMMON_CMD,
    SPECIAL_NONE,  1,DSAT_BASIC_N_E_IDX,   &dsat_amp_e_dflm[0],
    dsatparm_exec_param_cmd,            NULL },

  { "&F",       CONFIG | SIMPLE_DEC | RESTRICTED | COMMON_CMD,
    SPECIAL_AMP_F, 1,DSAT_BASIC_N_F_IDX,   &dsat_amp_f_dflm[0],
    dsatact_exec_ampf_cmd,              NULL },

  { "&S",       CONFIG | COMMON_CMD,
    SPECIAL_NONE, 1,DSAT_BASIC_N_S_IDX,   &dsat_amp_s_dflm[0],
    dsatact_exec_amps_cmd,            NULL },

  { "&V",       ATTRIB_NONE | COMMON_CMD,
    SPECIAL_AMP_V, 0 ,DSAT_BASIC_N_V_IDX,   NULL,
    dsatparm_exec_ampv_cmd,             NULL },

  { "&W",       CONFIG | SIMPLE_DEC | COMMON_CMD,
    SPECIAL_NONE,  1,DSAT_BASIC_N_W_IDX, &dsat_amp_w_dflm[0],
    dsatact_exec_ampw_cmd,            NULL },

#ifdef FEATURE_ATCOP_ECHO_CTL_MULTI_PORTS
  { "E",        CONFIG | SIMPLE_DEC | DOWN_LOAD | RESTRICTED|NOT_PROVISIONED | COMMON_CMD,
    SPECIAL_NONE,1,DSAT_BASIC_E_IDX,&dsat_e_dflm[0],
    dsatact_exec_ate_cmd,               NULL },
#else
  { "E",        CONFIG | SIMPLE_DEC | DOWN_LOAD | RESTRICTED|NOT_PROVISIONED | COMMON_CMD,
    SPECIAL_NONE,  1 , DSAT_BASIC_E_IDX ,   &dsat_e_dflm[0],
    dsatact_exec_ate_cmd,            NULL },
#endif /*FEATURE_ATCOP_ECHO_CTL_MULTI_PORTS*/

  { "I",        READ_ONLY | RESTRICTED|NOT_PROVISIONED | COMMON_CMD,
    SPECIAL_NONE,    0,   DSAT_BASIC_I_IDX,  NULL,
    dsatact_exec_ati_cmd,                 NULL },

{ "L",        CONFIG | SIMPLE_DEC | RESTRICTED|NOT_PROVISIONED | COMMON_CMD,
  SPECIAL_NONE,    1,DSAT_BASIC_L_IDX,    &dsat_l_dflm[0],
  dsatparm_exec_param_cmd,            NULL },

{ "M",        CONFIG | SIMPLE_DEC | RESTRICTED|NOT_PROVISIONED | COMMON_CMD,
  SPECIAL_NONE,    1,DSAT_BASIC_M_IDX,    &dsat_m_dflm[0],
  dsatparm_exec_param_cmd,            NULL },

  { "Q",        CONFIG | SIMPLE_DEC | DOWN_LOAD | RESTRICTED | COMMON_CMD|NOT_PROVISIONED,
    SPECIAL_NONE,   1, DSAT_BASIC_Q_IDX ,       &dsat_q_dflm[0],
    dsatparm_exec_param_cmd,            NULL },

  { "V",        CONFIG | SIMPLE_DEC | DOWN_LOAD | RESTRICTED|NOT_PROVISIONED | COMMON_CMD,
    SPECIAL_NONE, 1, DSAT_BASIC_V_IDX,       &dsat_v_dflm[0],
    dsatparm_exec_param_cmd,            NULL },

  { "X",        CONFIG | SIMPLE_DEC | DOWN_LOAD | COMMON_CMD,
    SPECIAL_NONE, 1, DSAT_BASIC_X_IDX,     &dsat_x_dflm[0],
    dsatact_exec_atx_cmd,            NULL },

  { "Z",        CONFIG | SIMPLE_DEC | RESTRICTED | COMMON_CMD,
    SPECIAL_Z,   1, DSAT_BASIC_Z_IDX,       &dsat_z_dflm[0],
    dsatact_exec_atz_cmd,               NULL },

  { "T",       ATTRIB_NONE | COMMON_CMD,
    SPECIAL_NONE,   0, DSAT_BASIC_T_IDX, NULL,
    dsatparm_exec_dtype_cmd,            NULL },

  { "P",       ATTRIB_NONE | COMMON_CMD,
    SPECIAL_NONE,   0, DSAT_BASIC_P_IDX,NULL,
    dsatparm_exec_dtype_cmd,            NULL },

  { "\\Q", SIMPLE_DEC | CONFIG | BROKEN_RANGE,
    SPECIAL_NONE,1,DSAT_BASIC_DS_Q_IDX, &dsat_slash_q_dflm[0], 
    dsatact_exec_slash_q_cmd,          NULL },

  { "\\S",    CONFIG | SIMPLE_DEC | COMMON_CMD,
    SPECIAL_NONE, 1,DSAT_BASIC_DS_S_IDX,&dsat_slash_s_dflm[0], 
    dsatact_exec_slashs_cmd,            NULL },

  { "\\V",    CONFIG | SIMPLE_DEC | COMMON_CMD,
    SPECIAL_NONE, 1,DSAT_BASIC_DS_V_IDX,&dsat_slash_v_dflm[0], 
    dsatparm_exec_param_cmd,            NULL },

  { "%V",     READ_ONLY | COMMON_CMD, /* output similar to +GMR */
    SPECIAL_NONE, 0, DSAT_BASIC_P_V_IDX,NULL,
    dsatact_exec_gmr_cmd,               NULL },
} ;

/* Basic common action command table. */
const dsati_cmd_type dsat_basic_action_table [] =
{
  
  { "D",       RESTRICTED|NOT_PROVISIONED | COMMON_CMD,
    SPECIAL_ATD,    0,DSAT_BASIC_ACT_D_IDX,  NULL,
    dsatact_exec_atd_cmd,               dsatact_call_abort_handler },

  { "A",       ATTRIB_NONE | COMMON_CMD,
    SPECIAL_NONE,    0,DSAT_BASIC_ACT_A_IDX,NULL,
    dsatact_exec_ata_cmd,               dsatact_call_abort_handler },

  { "H",       ATTRIB_NONE | COMMON_CMD,
    SPECIAL_NONE,    0,DSAT_BASIC_ACT_H_IDX,NULL, 
    dsatact_exec_ath_cmd,               NULL },

  { "O",       CONFIG | COMMON_CMD,
    SPECIAL_NONE,    1,DSAT_BASIC_ACT_O_IDX,   &dsat_o_dflm[0],
    dsatact_exec_ato_cmd,               NULL }
};

/* Common S-register command table. */
const dsati_cmd_type dsat_sreg_table [] =
{

  { "S0",       CONFIG | COMMON_CMD | RESTRICTED|NOT_PROVISIONED,
  SPECIAL_NONE,    1,DSAT_SREG_S0_IDX,  &dsat_s0_dflm[0],
  dsatparm_exec_szero_cmd,            NULL },


  { "S2",       CONFIG | DOWN_LOAD | COMMON_CMD,
    SPECIAL_NONE,    1,DSAT_SREG_S2_IDX, &dsat_s2_dflm[0],
    dsatparm_exec_param_cmd,            NULL },

  { "S3",       CONFIG | DOWN_LOAD | COMMON_CMD,
    SPECIAL_NONE,    1,DSAT_SREG_S3_IDX,  &dsat_s3_dflm[0],
    dsatparm_exec_sthree_cmd,            NULL },

  { "S4",       CONFIG | DOWN_LOAD | COMMON_CMD,
    SPECIAL_NONE,    1,DSAT_SREG_S4_IDX,  &dsat_s4_dflm[0],
    dsatparm_exec_sfour_cmd,            NULL },

  { "S5",       CONFIG | DOWN_LOAD | COMMON_CMD,
    SPECIAL_NONE,    1,DSAT_SREG_S5_IDX,  &dsat_s5_dflm[0],
    dsatparm_exec_sfive_cmd,            NULL },

  { "S6",       CONFIG | DOWN_LOAD | COMMON_CMD,
    SPECIAL_NONE,    1,DSAT_SREG_S6_IDX, &dsat_s6_dflm[0],
    dsatparm_exec_param_cmd,            NULL },

  { "S7",       CONFIG | DOWN_LOAD | COMMON_CMD,
    SPECIAL_NONE,    1,DSAT_SREG_S7_IDX,  &dsat_s7_dflm[0],
    dsatparm_exec_sseven_cmd,            NULL },

  { "S8",       CONFIG | DOWN_LOAD | COMMON_CMD,
    SPECIAL_NONE,    1,DSAT_SREG_S8_IDX, &dsat_s8_dflm[0],
    dsatparm_exec_param_cmd,            NULL },

  { "S9",       CONFIG | DOWN_LOAD | COMMON_CMD,
    SPECIAL_NONE,    1,DSAT_SREG_S9_IDX, &dsat_s9_dflm[0],
    dsatparm_exec_param_cmd,            NULL },

  { "S10",      CONFIG | DOWN_LOAD | COMMON_CMD,
    SPECIAL_NONE,    1,DSAT_SREG_S10_IDX, &dsat_s10_dflm[0],
    dsatparm_exec_param_cmd,            NULL },

  { "S11",       CONFIG | DOWN_LOAD | COMMON_CMD,
    SPECIAL_NONE,    1,DSAT_SREG_S11_IDX,  &dsat_s11_dflm[0],
    dsatparm_exec_param_cmd,            NULL },

  { "S30",       CONFIG | DOWN_LOAD | COMMON_CMD,
    SPECIAL_NONE,    1,DSAT_SREG_S30_IDX, &dsat_s30_dflm[0],
    dsatparm_exec_param_cmd,            NULL },

  { "S103",       CONFIG | DOWN_LOAD | COMMON_CMD,
    SPECIAL_NONE,    1,DSAT_SREG_S103_IDX, &dsat_s103_dflm[0],
    dsatparm_exec_param_cmd,            NULL },

  { "S104",       CONFIG | DOWN_LOAD | COMMON_CMD,
    SPECIAL_NONE,    1,DSAT_SREG_S104_IDX,&dsat_s104_dflm[0],
    dsatparm_exec_param_cmd,            NULL }
} ;

/* Extended common AT command table */
const dsati_cmd_type dsat_ext_table [] =
{
  { "+FCLASS",  EXTENDED | CONFIG | DOWN_LOAD | RESTRICTED|NOT_PROVISIONED | COMMON_CMD,
    SPECIAL_FCLASS,  1,DSAT_EXT_FCLASS_IDX,&dsat_fclass_dfl,
    dsatparm_exec_fclass_cmd,            NULL},

  { "+ICF",     EXTENDED | CONFIG | LOCAL_TEST | COMMON_CMD,
    SPECIAL_NONE, 2,DSAT_EXT_ICF_IDX,  &dsat_icf_dflm[0],
    dsatact_exec_icf_cmd,            NULL },

  { "+IFC",     EXTENDED | CONFIG | LOCAL_TEST | COMMON_CMD,
    SPECIAL_NONE, 2,DSAT_EXT_IFC_IDX,   &dsat_ifc_dflm[0],
    dsatact_exec_ifc_cmd,            NULL },

  { "+IPR",     EXTENDED | CONFIG | LIST | LOCAL_TEST | COMMON_CMD,
    SPECIAL_NONE, 1, DSAT_EXT_IPR_IDX,      &dsat_ipr_list,
    dsatparm_exec_ipr_cmd,              NULL },

  { "+GMI",     READ_ONLY | RESTRICTED | COMMON_CMD,
    SPECIAL_NONE, 0,DSAT_EXT_GMI_IDX,   NULL,
    dsatact_exec_gmi_cmd,               NULL },

  { "+GMM",     READ_ONLY | RESTRICTED|NOT_PROVISIONED | COMMON_CMD,
    SPECIAL_NONE,   0, DSAT_EXT_GMM_IDX,   NULL,
    dsatact_exec_gmm_cmd,               NULL },

  { "+GMR",     READ_ONLY | RESTRICTED | COMMON_CMD,
    SPECIAL_NONE,   0, DSAT_EXT_GMR_IDX,   NULL,
    dsatact_exec_gmr_cmd,               NULL },

  { "+GCAP",    READ_ONLY | COMMON_CMD,
    SPECIAL_NONE,   0, DSAT_EXT_GCAP_IDX,    NULL,
    dsatact_exec_gcap_cmd,              NULL },

  { "+GSN",     READ_ONLY | RESTRICTED | COMMON_CMD,
    SPECIAL_NONE,   0,  DSAT_EXT_GSN_IDX,    NULL,
    dsatact_exec_gsn_cmd,               NULL },

  { "+DR",      EXTENDED | CONFIG | LOCAL_TEST | COMMON_CMD,
    SPECIAL_NONE,   1,  DSAT_EXT_DR_IDX,    &dsat_dr_dflm[0],
    dsatparm_exec_ext_param_cmd,            NULL },

  { "+DS",      EXTENDED | CONFIG | LOCAL_TEST | COMMON_CMD,
    SPECIAL_NONE,   4,DSAT_EXT_DS_IDX,   &dsat_ds_dflm[0],
    dsatparm_exec_ext_param_cmd,            NULL },

  { "+CMEE",     EXTENDED | CONFIG | LIST | LOCAL_TEST | RESTRICTED | COMMON_CMD,
    SPECIAL_NONE,   1,DSAT_EXT_CMEE_IDX,     &dsat_cmee_list,
    dsatparm_exec_param_cmd,            NULL },

#if defined(FEATURE_DATA_GCSD) || defined(FEATURE_DATA_WCDMA)
/* Exclude CDMA (for now, may be required later) */
  { "+WS46",      EXTENDED | CONFIG |  LIST | LOCAL_TEST | UMTS_CMD,
    SPECIAL_NONE,   1,DSAT_EXT_WS46_IDX,      &dsat_ws46_list[0],
    dsatact_exec_ws46_cmd,            NULL },
#endif /* FEATURE_DATA_GCSD || FEATURE_DATA_WCDMA */

#if ( defined(FEATURE_WCDMA) || defined(FEATURE_GSM))
  { "+PACSP",       READ_ONLY | UMTS_CMD,
    SPECIAL_NONE,    0,DSAT_EXT_PACSP_IDX,   NULL,
    dsatvend_exec_pacsp_cmd,      NULL },
#endif /* defined(FEATURE_WCDMA) || defined(FEATURE_GSM)*/

#ifdef FEATURE_DSAT_TEST_32ARG 
  { "+TST32",   EXTENDED | CONFIG | LOCAL_TEST | COMMON_CMD,
    SPECIAL_NONE,   2,DSAT_EXT_TST32_IDX,   &dsat_tst32_dflm[0],
    dsatparm_exec_param_cmd,            NULL },
#endif /* FEATURE_DSAT_TEST_32ARG */

  { "+CFUN",    EXTENDED | CONFIG | LOCAL_TEST | LIST | MIXED_PARAM | 
                NO_RESET | RESTRICTED | COMMON_CMD,
    SPECIAL_NONE, 2,DSAT_EXT_CFUN_IDX,   dsat_cfun_mixed_dfl,
    dsatme_exec_cfun_cmd,              NULL },

#ifndef FEATURE_DSAT_CUST
  { "+CLCC",   COMMON_CMD ,
    SPECIAL_NONE,    0,DSAT_EXT_CLCC_IDX,  NULL,
    dsatact_exec_clcc_cmd,  dsatact_call_info_abort_handler },
#endif /* FEATURE_DSAT_CUST  */

} ;

/* Vendor specific common AT command table. */
const dsati_cmd_type dsat_vendor_table [] =
{
#ifdef FEATURE_DSAT_ETSI_MODE
  { "$QCSIMSTAT",    EXTENDED | CONFIG | RESTRICTED | UMTS_CMD,
    SPECIAL_NONE,1,DSAT_VENDOR_QCSIMSTAT_IDX, &dsat_qcsimstat_dflm[0],
    dsatme_exec_qcsimstat_cmd,            NULL },

#if defined(FEATURE_ETSI_PBM) || defined(FEATURE_DSAT_CDMA_PBM)
  { "$QCPBMPREF",    EXTENDED | CONFIG | RESTRICTED | COMMON_CMD,
    SPECIAL_NONE, 1,DSAT_VENDOR_QCPBMPREF_IDX, &dsat_qcpbmpref_dflm[0],
    dsatme_exec_qcpbmpref_cmd,            NULL },
#endif /* defined(FEATURE_ETSI_PBM) || defined(FEATURE_DSAT_CDMA_PBM) */

#ifdef FEATURE_DSAT_DEV_CMDS
  { "$CREG",    EXTENDED | CONFIG | LOCAL_TEST| MULTI_STACK | UMTS_CMD,
    SPECIAL_NONE, 1,DSAT_VENDOR_CREG_IDX,dsat_qccreg_dfl,
    dsatetsicall_exec_creg_cmd,            NULL },

  { "$CCLK",    EXTENDED | COMMON_CMD,
    SPECIAL_NONE, 0, DSAT_VENDOR_CCLK_IDX,NULL,
    dsatetsime_exec_cclk_cmd,           NULL},
#endif /* FEATURE_DSAT_DEV_CMDS */

#ifdef FEATURE_ETSI_SMS
  { "$QCCNMI",    EXTENDED | CONFIG | LIST | LOCAL_TEST| MULTI_STACK | UMTS_CMD,
    SPECIAL_NONE,    5, DSAT_VENDOR_QCCNMI_IDX,&dsat_cnmi_list[0],
    dsatetsisms_exec_cnmi_cmd,          NULL },
#endif /* FEATURE_ETSI_SMS */
  
#ifdef FEATURE_DSAT_DEV_CMDS
  { "*CNTI",   EXTENDED | CONFIG  | LOCAL_TEST | UMTS_CMD,
    SPECIAL_NONE,   1, DSAT_VENDOR_CNTI_IDX,&dsat_qccnti_dflm[0],
    dsatvend_exec_qccnti_cmd,            NULL },
#endif /* FEATURE_DSAT_DEV_CMDS */
#endif /* FEATURE_DSAT_ETSI_MODE*/

  { "$QCCLR",   EXTENDED | READ_ONLY | COMMON_CMD,
    SPECIAL_NONE,   0,DSAT_VENDOR_QCCLR_IDX,   NULL,
    dsatvend_exec_qcclr_cmd,            NULL },

  { "$QCDMR",   EXTENDED | CONFIG | LIST | LOCAL_TEST | COMMON_CMD,
    SPECIAL_NONE,   1, DSAT_VENDOR_QCDMR_IDX, &dsat_qcdmr_list,
    dsatvend_exec_qcdmr_cmd,            NULL },


  { "$QCDNSP",  EXTENDED | CONFIG | STRING | NO_QUOTE | COMMON_CMD,
    SPECIAL_NONE,  MAX_DNS_SIZE,DSAT_VENDOR_QCDNSP_IDX,NULL, 
    dsatvend_exec_qcdnsp_cmd,           NULL },

  { "$QCDNSS",  EXTENDED | CONFIG | STRING | NO_QUOTE | COMMON_CMD,
    SPECIAL_NONE,  MAX_DNS_SIZE, DSAT_VENDOR_QCDNSS_IDX,NULL,  
    dsatvend_exec_qcdnss_cmd,           NULL },

  { "$QCTER",  EXTENDED | CONFIG | LIST | LOCAL_TEST | COMMON_CMD,
    SPECIAL_NONE,    1,DSAT_VENDOR_QCTER_IDX, &dsat_qcter_list,
    dsatvend_exec_qcter_cmd,            NULL },
  { "$QCSIMAPP",  EXTENDED | CONFIG | LOCAL_TEST | RESTRICTED | COMMON_CMD,
    SPECIAL_NONE,1, DSAT_VENDOR_QCSIMAPP_IDX,  &dsat_qcsimapp_dflm[0],
     dsatme_exec_qcsimapp_cmd,            NULL },
#ifdef FEATURE_DATA_PS_SLIP
  { "$QCSLIP",   ATTRIB_NONE | RESTRICTED | COMMON_CMD,
    SPECIAL_NONE, 0, DSAT_VENDOR_QCSLIP_IDX,    NULL,
    dsatvend_exec_qcslip_cmd,           NULL },
#endif /* FEATURE_DATA_PS_SLIP */

#ifdef FEATURE_DATA_ETSI_PIN
  { "$QCPINSTAT",  READ_ONLY | RESTRICTED | UMTS_CMD,
    SPECIAL_NONE,    0, DSAT_VENDOR_QCPINSTAT_IDX,   NULL,
    dsatetsime_exec_qcpinstat_cmd,      NULL },
#endif /* FEATURE_DATA_ETSI_PIN */
  
#ifdef FEATURE_DSAT_ETSI_DATA
  { "$QCPDPP",  EXTENDED | CONFIG | MIXED_PARAM | LIST | LOCAL_TEST | UMTS_CMD,
    SPECIAL_NONE,   4, DSAT_VENDOR_QCPDPP_IDX,   &dsat_qcpdpp_mixed_dfl[0],
    dsatetsipkt_exec_qcpdpp_cmd,       NULL },

  { "$QCPDPLT",   EXTENDED | CONFIG | NO_RESET | UMTS_CMD,
    SPECIAL_NONE,   1, DSAT_VENDOR_QCPDPLT_IDX,    &dsat_qcpdplt_dflm[0],
    dsatetsipkt_exec_qcpdplt_cmd,      NULL },


#endif /* FEATURE_DSAT_ETSI_DATA */

#ifdef FEATURE_DATA_TE_MT_PDP
  { "$QCGANSM",    EXTENDED | CONFIG | LIST | LOCAL_TEST | NO_RESET | COMMON_CMD,
    SPECIAL_NONE,   1, DSAT_VENDOR_QCGANSM_IDX,   &dsat_qcgansm_list,
    dsatparm_exec_param_cmd,       NULL },

  { "$QCGARL",    EXTENDED | UMTS_CMD,
    SPECIAL_NONE,   2,DSAT_VENDOR_QCGARL_IDX,   NULL,
    dsatetsipkt_exec_qcgarl_cmd,       NULL },
    
  { "$QCGAPL",    EXTENDED | UMTS_CMD,
    SPECIAL_NONE,   2,DSAT_VENDOR_QCGAPL_IDX,   NULL,
    dsatetsipkt_exec_qcgapl_cmd,       NULL },
#endif /* FEATURE_DATA_TE_MT_PDP */

  { "$QCPWRDN",  EXTENDED | COMMON_CMD| RESTRICTED,
    SPECIAL_NONE,    0, DSAT_VENDOR_QCPWRDN_IDX,   NULL,
    dsatvend_exec_qcpwrdn_cmd,          NULL },

#ifdef FEATURE_DSAT_ETSI_DATA
  { "$QCDGEN",  EXTENDED | UMTS_CMD,
    SPECIAL_NONE,    2,DSAT_VENDOR_QCDGEN_IDX,   NULL,
    dsatvend_exec_qcdgen_cmd,          NULL },  

  { "$QCPDPCFGE",  EXTENDED | COMMON_CMD,
     SPECIAL_NONE,   12,DSAT_VENDOR_QCPDPCFGE_IDX,   NULL,
    dsatvend_exec_qcpdpcfge_cmd,          NULL },
#endif /* FEATURE_DSAT_ETSI_DATA */
  
#ifdef FEATURE_DSAT_BREW_SUPPORT
  { "$BREW",   EXTENDED | COMMON_CMD,
    SPECIAL_NONE,   0, DSAT_VENDOR_BREW_IDX,     NULL,
    dsatvend_exec_brew_cmd,            NULL },
#endif /* FEATURE_DSAT_BREW_SUPPORT */

#ifdef FEATURE_DATA_UCSD_SCUDIF_TEST
  { "$QCSCFTEST",   EXTENDED | CONFIG | LIST | LOCAL_TEST | UMTS_CMD,
    SPECIAL_NONE, 2,DSAT_VENDOR_QCSCFTEST_IDX,  &dsat_qcscftest_list[0],   
    dsatvend_exec_qcscftest_cmd,            NULL },
#endif /*  FEATURE_DATA_UCSD_SCUDIF_TEST */

#ifdef FEATURE_DSAT_GOBI_MAINLINE
  { "$QCGSN", READ_ONLY | RESTRICTED | COMMON_CMD,
	SPECIAL_NONE,   0,DSAT_VENDOR_QCGSN_IDX,     NULL,
	dsatvend_exec_qcgsn_cmd,           NULL },

  { "$QCSKU",   READ_ONLY | RESTRICTED | COMMON_CMD,
    SPECIAL_NONE,   0,DSAT_VENDOR_QCSKU_IDX,  NULL,
    dsatvend_exec_qcsku_cmd,       NULL },
#endif /* FEATURE_DSAT_GOBI_MAINLINE */

  { "$QCANTE",   READ_ONLY | UMTS_CMD,
    SPECIAL_NONE,   0,DSAT_VENDOR_QCANTE_IDX,  NULL,
    dsatvend_exec_qcante_cmd,            NULL },
    
  { "$QCRPW",  READ_ONLY | UMTS_CMD,
    SPECIAL_NONE,    0,DSAT_VENDOR_QCRPW_IDX,   NULL,
    dsatvend_exec_qcrpw_cmd,            NULL },

  { "$QCSQ",  READ_ONLY | UMTS_CMD,
    SPECIAL_NONE,    0,DSAT_VENDOR_QCSQ_IDX,   NULL,
    dsatvend_exec_qcsq_cmd,            NULL },
#ifdef FEATURE_DSAT_DEV_CMDS
  { "$CSQ",  READ_ONLY | UMTS_CMD,
    SPECIAL_NONE,    0,DSAT_VENDOR_CSQ_IDX,   NULL,
    dsatetsime_exec_csq_cmd,            NULL },
#endif /* FEATURE_DSAT_DEV_CMDS */
  { "$QCSYSMODE",    READ_ONLY | COMMON_CMD,
    SPECIAL_NONE,   0,DSAT_VENDOR_QCSYSMODE_IDX,  NULL,
    dsatvend_exec_qcsysmode_cmd,              NULL }
#ifdef FEATURE_TTY
  ,{ "$QCCTM",    EXTENDED | CONFIG | LOCAL_TEST | COMMON_CMD,
    SPECIAL_NONE,   1,DSAT_VENDOR_QCCTM_IDX, &dsat_qcctm_dflm[0],
    dsatparm_exec_param_cmd,              NULL }
#endif/* FEATURE_TTY */
 ,{ "$QCBANDPREF",    EXTENDED | LOCAL_TEST | COMMON_CMD,
      SPECIAL_NONE,    1,DSAT_VENDOR_QCBANDPREF_IDX, NULL,
      dsatvend_exec_qcbandpref_cmd,    NULL }
#ifdef FEATURE_HDR
  ,{ "^PREFMODE",    EXTENDED | CONFIG | LOCAL_TEST | COMMON_CMD,
    SPECIAL_NONE,    1,DSAT_VENDOR_PREFMODE_IDX,  dsat_prefmode_dfl,
    dsatvend_exec_prefmode_cmd,            NULL }
#endif /* FEATURE_HDR */

 ,{ "^SYSINFO",    READ_ONLY | DO_PREF_CMD | RESTRICTED,
    SPECIAL_NONE ,     0, DSAT_VENDOR_SYSINFO_IDX,      NULL,
    dsatvend_exec_sysinfo_cmd,    NULL }
 
 ,{ "^SYSCONFIG",    READ_ONLY | UMTS_CMD,
    SPECIAL_NONE ,     0, DSAT_VENDOR_SYSCONFIG_IDX,     NULL,
    dsatvend_exec_sysconfig_cmd,    NULL }

 ,{ "^CARDMODE",    READ_ONLY | COMMON_CMD | RESTRICTED,
    SPECIAL_NONE ,     0, DSAT_VENDOR_CARDMODE_IDX,     NULL,
    dsatme_exec_cardmode_cmd,    NULL }
#ifdef FEATURE_DSAT_ETSI_MODE 
  ,{ "^DSCI",    EXTENDED | CONFIG | LOCAL_TEST | UMTS_CMD,
    SPECIAL_NONE,    1, DSAT_VENDOR_DSCI_IDX,   &dsat_dsci_dflm[0],
    dsatparm_exec_param_cmd,            NULL }
#endif /* FEATURE_DSAT_ETSI_MODE */
  ,{ "$QCVOLT", READ_ONLY | RESTRICTED | COMMON_CMD,
  SPECIAL_NONE,   0,DSAT_VENDOR_QCVOLT_IDX,  NULL,
  dsatvend_exec_qcvolt_cmd,          NULL }

  ,{ "$QCHWREV", READ_ONLY | RESTRICTED | COMMON_CMD,
  SPECIAL_NONE,   0,DSAT_VENDOR_QCHWREV_IDX,   NULL,
  dsatvend_exec_qchwrev_cmd,         NULL }

  ,{ "$QCBOOTVER",   READ_ONLY | RESTRICTED | COMMON_CMD,
    SPECIAL_NONE,   0, DSAT_VENDOR_QCBOOTVER_IDX,  NULL,
    dsatvend_exec_qcbootver_cmd,       NULL }

#ifdef FEATURE_DSAT_GOBI_MAINLINE 
  ,{ "$QCTEMP", READ_ONLY | RESTRICTED | COMMON_CMD,
    SPECIAL_NONE,   0, DSAT_VENDOR_QCTEMP_IDX,  NULL,
    dsatvend_exec_qctemp_cmd,          NULL }

  ,{ "$QCAGC", READ_ONLY | RESTRICTED | COMMON_CMD,
    SPECIAL_NONE,   0, DSAT_VENDOR_QCAGC_IDX,   NULL,
    dsatvend_exec_qcagc_cmd,           NULL }

#ifdef FEATURE_WCDMA
  ,{ "$QCALLUP", READ_ONLY | RESTRICTED | UMTS_CMD,
    SPECIAL_NONE,   0, DSAT_VENDOR_QCALLUP_IDX,  NULL,
    dsatvend_exec_qcallup_cmd,         NULL }
#endif /* FEATURE_WCDMA */
#endif /* FEATURE_DSAT_GOBI_MAINLINE */
#ifdef FEATURE_ECALL_APP 
  ,{ "$ECALL",    EXTENDED | CONFIG | LIST| LOCAL_TEST | UMTS_CMD,
    SPECIAL_NONE,    3,DSAT_VENDOR_ECALL_IDX,   &dsat_ecall_list[0],
    dsatvend_exec_ecall_cmd,            NULL }
#endif /* FEATURE_ECALL_APP */

#ifdef FEATURE_DSAT_ETSI_DATA
  ,{ "$QCDEFPROF",   EXTENDED | CONFIG | LOCAL_TEST | UMTS_CMD,
  SPECIAL_NONE,    3, DSAT_VENDOR_QCDEFPROF_IDX,    &dsat_qcdefprof_dfl[0],
  dsatetsipkt_exec_qcdefprof_cmd,            NULL }
#endif /* FEATURE_DSAT_ETSI_DATA */

  ,{ "$QCMRUE",  EXTENDED | COMMON_CMD,
     SPECIAL_NONE,    4,  DSAT_VENDOR_QCMRUE_IDX,  NULL,
    dsatvend_exec_qcmrue_cmd,          NULL }
  
  ,{ "$QCMRUC",  EXTENDED | COMMON_CMD,
     SPECIAL_NONE,    0,  DSAT_VENDOR_QCMRUC_IDX,  NULL,
    dsatvend_exec_qcmruc_cmd,          NULL }
#ifdef FEATURE_DSAT_ETSI_DATA  
  ,{ "$QCAPNE",  EXTENDED | UMTS_CMD,
     SPECIAL_NONE,    7,DSAT_VENDOR_QCAPNE_IDX,  NULL,
    dsatvend_exec_qcapne_cmd,          NULL }

  ,{ "$QCPDPIMSCFGE",  EXTENDED | UMTS_CMD,
       SPECIAL_NONE,    4,DSAT_VENDOR_QCPDPIMSCFGE_IDX,  NULL,
       dsatvend_exec_qcpdpimscfge_cmd,          NULL }
      
#endif /* FEATURE_DSAT_ETSI_DATA */
  ,{ "$QCCLAC",  READ_ONLY|RESTRICTED | COMMON_CMD,
       SPECIAL_NONE,    0,DSAT_VENDOR_QCCLAC_IDX,  NULL,
       dsatvend_exec_qcclac_cmd,          NULL }

  ,{ "^SPN",    READ_ONLY | UMTS_CMD,
    SPECIAL_NONE ,     0, DSAT_VENDOR_SPN_IDX,  NULL,
    dsatme_exec_spn_cmd,    NULL }
  ,{ "$QCRMCALL",  EXTENDED | COMMON_CMD, 
       SPECIAL_NONE, 6, DSAT_VENDOR_QCRMCALL_IDX,  NULL,
       dsatvend_exec_qcrmcall_cmd,        dsatvend_qcrmcall_abort_cmd_handler}
#ifdef FEATURE_DSAT_LTE
  ,{ "$QCDRX", CONFIG | LOCAL_TEST | EXTENDED | BROKEN_RANGE | UMTS_CMD,
     SPECIAL_NONE, 1, DSAT_VENDOR_QCDRX_IDX,  dsat_qcdrx_dflm,
     dsatvend_exec_qcdrx_cmd, NULL }

  ,{ "$QCRSRP", READ_ONLY| EXTENDED | UMTS_CMD,
     SPECIAL_NONE, 0 , DSAT_VENDOR_QCRSRP_IDX,  NULL,
     dsatvend_exec_qcrsrp_cmd, NULL }
  
  ,{ "$QCRSRQ", READ_ONLY| EXTENDED | UMTS_CMD,
     SPECIAL_NONE, 0 , DSAT_VENDOR_QCRSRQ_IDX,   NULL,
     dsatvend_exec_qcrsrp_cmd, NULL }

  ,{ "$QCACQDBC",  EXTENDED ,
     SPECIAL_NONE,    0,  DSAT_VENDOR_QCACQDBC_IDX,   NULL,
    dsatvend_exec_qcacqdbc_cmd,          NULL }
#endif /* FEATURE_DSAT_LTE */
#ifdef FEATURE_DSAT_EXT_CLIENT_SUPPORT
  ,{ "$QCATMOD",    EXTENDED | LOCAL_TEST| COMMON_CMD,
     SPECIAL_NONE,   1,DSAT_VENDOR_QCATMOD_IDX, NULL,
     dsatvend_exec_qcatmod_cmd,              NULL }
#endif /* FEATURE_DSAT_EXT_CLIENT_SUPPORT*/
#ifdef FEATURE_DSAT_HIGH_TIER
  ,{ "$QCCOPS",     EXTENDED | LOCAL_TEST | CONFIG | LIST | MIXED_PARAM | MULTI_STACK | UMTS_CMD,
     SPECIAL_NONE,   4,DSATETSI_EXT_ACT_COPS_ETSI_IDX, &dsat_cops_mixed_dfl[0],
     dsatetsicall_exec_cops_cmd,          dsatetsicall_cops_abort_cmd_handler }
#endif /* FEATURE_DSAT_HIGH_TIER */
#ifdef FEATURE_SGLTE
  ,{ "$QCHCOPS", READ_ONLY | EXTENDED | UMTS_CMD,
     SPECIAL_NONE, 0, DSAT_VENDOR_QCHCOPS_IDX, NULL,
     dsatvend_exec_qchcops_cmd, NULL }

  ,{ "$QCHCREG", READ_ONLY | EXTENDED | UMTS_CMD,
     SPECIAL_NONE, 0, DSAT_VENDOR_QCHCREG_IDX, NULL,
     dsatvend_exec_qchcreg_cmd, NULL }
#endif /* FEATURE_SGLTE */
  ,{ "^MODE",    EXTENDED | CONFIG | UMTS_CMD,
    SPECIAL_NONE ,     1, DSAT_VENDOR_MODE_IDX,  dsat_mode_dflm,
    dsatparm_exec_param_cmd,    NULL }

  ,{ "$QCSIMT", READ_ONLY ,
     SPECIAL_NONE,    0,	DSAT_VENDOR_QCSIMT_IDX,	  NULL,
     dsatvend_exec_qcsimt_cmd,			 NULL }
	
  ,{ "$QCNSP",    EXTENDED |LOCAL_TEST | UMTS_CMD,
    SPECIAL_NONE,    3,   DSAT_VENDOR_QCNSP_IDX,      NULL,
    dsatvend_exec_qcnsp_cmd,            NULL }

  ,{ "$QCRCIND",    EXTENDED | CONFIG | UMTS_CMD,
    SPECIAL_NONE ,     1, DSAT_VENDOR_QCRCIND_IDX,  dsat_qcrcind_dflm,
    dsatparm_exec_param_cmd,    NULL }

  ,{ "$QCCSGCOPS",     EXTENDED | LOCAL_TEST | CONFIG | LIST | MIXED_PARAM | MULTI_STACK | UMTS_CMD,
    SPECIAL_NONE,   6,DSATETSI_EXT_ACT_COPS_ETSI_IDX, &dsat_cops_mixed_dfl[0],
    dsatetsicall_exec_cops_cmd,          dsatetsicall_cops_abort_cmd_handler }

#ifdef FEATURE_DSAT_ETSI_DATA
  ,{ "$QCPDPCFGEXT",  EXTENDED | COMMON_CMD,
     SPECIAL_NONE,   4,DSAT_VENDOR_QCPDPCFGEXT_IDX,   NULL,
    dsatvend_exec_qcpdpcfgext_cmd,          NULL }
#endif /* FEATURE_DSAT_ETSI_DATA */
#ifdef FEATURE_DSAT_LTE
#ifdef FEATURE_VOIP 
  ,{ "$QCVOIPM",  EXTENDED | UMTS_CMD,
     SPECIAL_NONE, 10, DSAT_VENDOR_QCVOIPM_IDX,  NULL,
     dsatvend_exec_qcvoipm_cmd, NULL }
#endif /* FEATURE_VOIP */
#endif /* FEATURE_DSAT_LTE */

} ;

/* Command table sizes. */
const unsigned int dsat_basic_table_size = ARR_SIZE( dsat_basic_table );
const unsigned int dsat_basic_action_table_size =
                        ARR_SIZE( dsat_basic_action_table );
const unsigned int dsat_sreg_table_size = ARR_SIZE( dsat_sreg_table );
const unsigned int dsat_ext_table_size = ARR_SIZE( dsat_ext_table );
const unsigned int dsat_vendor_table_size = ARR_SIZE( dsat_vendor_table );


/*===========================================================================

FUNCTION DSATCTAB_DATA_INIT

DESCRIPTION
  This function initializes the AT command defaults, limits, and lists
  that depend on a run-time determination of the hardware.

DEPENDENCIES
  None
  
RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/

void dsatctab_data_init
(
  void
)
{
  /*---------------------------------------------------------------------------
  status from NV call. This variable will be used to obtain the status of the 
  read/write operations to the NV.
  ---------------------------------------------------------------------------*/
  nv_stat_enum_type    nv_status;          

  /*-----------------------------------------------------------------------
  Read the stored Baud rate from the NV. If NV is not written, then use 
  the default values.
  -----------------------------------------------------------------------*/
  nv_status = dsatutil_get_nv_item(NV_DS_DEFAULT_BAUDRATE_I, &ds_nv_item);
  
  /*-----------------------------------------------------------------------
  Check if the NV has valid data inside and load the data from NV to 
  local item.
  
  If NV is ACTIVE, confirm that the value inside NV is the valid value 
  by calling sio_is_baud_valid()       
  -----------------------------------------------------------------------*/
  if( nv_status == NV_DONE_S)
  {
    if( sio_is_baud_valid((sio_bitrate_type)ds_nv_item.ds_default_baudrate) 
      == TRUE)
    {
      dsat_qcter_list.default_v = ds_nv_item.ds_default_baudrate;
      dsat_ipr_list.default_v   = ds_nv_item.ds_default_baudrate;
    }
  }
  else
  {
    /*-----------------------------------------------------------------------
    If NV was never written then copy the default values into the ipr_val. 
    -----------------------------------------------------------------------*/
    dsat_qcter_list.default_v  = (dsat_num_item_type)SIO_BITRATE_115200;
    dsat_ipr_list.default_v    = (dsat_num_item_type)SIO_BITRATE_115200;  
    
    /*-----------------------------------------------------------------------
      Initialize NV from statically-initialized default.
    -----------------------------------------------------------------------*/
    if( nv_status == NV_NOTACTIVE_S )
    {
      ds_nv_item.ds_default_baudrate = (uint16)dsat_qcter_list.default_v;
      (void)dsatutil_put_nv_item(NV_DS_DEFAULT_BAUDRATE_I, &ds_nv_item);
    }
  }
}  /* dsatctab_data_init( ) */
/*===========================================================================

FUNCTION  DSAT_SET_QCSIMAPP_VAL

DESCRIPTION
  This function set dsat_qcsimapp_val or dsat_qcsimapp_val_updated

DEPENDENCIES
  None
  
RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/

void dsat_set_qcsimapp_val(dsat_num_item_type value)
{
  ds3g_siolib_port_e_type port_count = DS3G_SIOLIB_DATA_PORT;
  dsat_sio_info_s_type *sio_info_ptr = NULL;

  dsatcmdp_dds_qcsimapp_val = value;
  while( port_count < DS3G_SIOLIB_PORT_MAX )
  { 
    if (TRUE == dsatcmdp_is_sio_info_inited(port_count))
    {
      sio_info_ptr = dsatcmdp_get_sio_info_ptr(port_count);
      if (FALSE == sio_info_ptr->qcsimapp_val_user_flag)
      {
        if((TRUE == dsatcmdp_processing_async_cmd) && (port_count == ds3g_siolib_get_active_port()))
        {
          DS_AT_MSG1_MED("Setting qcsimapp_val_updated = %d", value);
          sio_info_ptr->qcsimapp_val_updated = value;
        }
        else
        {
          DS_AT_MSG1_MED("Setting qcsimapp_val = %d", value);
          sio_info_ptr->qcsimapp_val = value;
        }
      }
    }
    port_count++;
  }
}/*dsat_set_qcsimapp_val*/

/*===========================================================================

FUNCTION  DSAT_UPDATE_QCSIMAPP_VAL

DESCRIPTION
  This function updates dsat_qcsimapp_val

DEPENDENCIES
  None
  
RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/

void dsat_update_qcsimapp_val(void)
{
  dsat_sio_info_s_type *sio_info_ptr = NULL;

  sio_info_ptr = dsatcmdp_get_sio_info_ptr(ds3g_siolib_get_active_port());

  if(sio_info_ptr->qcsimapp_val_updated != INVALID_SUBS)
  {
    sio_info_ptr->qcsimapp_val = sio_info_ptr->qcsimapp_val_updated;
    sio_info_ptr->qcsimapp_val_updated = INVALID_SUBS;
  }
}/*dsat_update_qcsimapp_val*/

/*===========================================================================

FUNCTION  DSAT_GET_QCSIMAPP_VAL

DESCRIPTION
  This function returns value of dsat_qcsimapp_val 

DEPENDENCIES
  None
  
RETURN VALUE
  value of dsat_qcsimapp_val

SIDE EFFECTS
  None

===========================================================================*/

dsat_num_item_type dsat_get_qcsimapp_val(void)
{
  dsat_sio_info_s_type *sio_info_ptr = NULL;

  if (DS3G_SIOLIB_PORT_NONE != ds3g_siolib_get_active_port())
  {
    sio_info_ptr = dsatcmdp_get_sio_info_ptr(ds3g_siolib_get_active_port());
    return sio_info_ptr->qcsimapp_val;
  }
  else
  {
    return dsatcmdp_dds_qcsimapp_val;
  }
}/*dsat_get_qcsimapp_val*/

/*===========================================================================

FUNCTION  DSAT_SET_QCSIMAPP_VAL_USER_FLAG

DESCRIPTION
  This function sets dsat_qcsimapp_val_user_flag

DEPENDENCIES
  None
  
RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/

void dsat_set_qcsimapp_val_user_flag(void)
{
  dsat_sio_info_s_type *sio_info_ptr = NULL;
  
  sio_info_ptr = dsatcmdp_get_sio_info_ptr(ds3g_siolib_get_active_port());

  sio_info_ptr->qcsimapp_val_user_flag = TRUE;
  
} /* dsat_set_qcsimapp_val_user_flag */

/*===========================================================================

FUNCTION  DSAT_GET_QCSIMAPP_INFO_IDX

DESCRIPTION
  This function returns value of dsat_qcsimapp_info index based on dsat_qcsimapp_val

DEPENDENCIES
  None
  
RETURN VALUE
  value of dsat_qcsimapp_info index

SIDE EFFECTS
  None

===========================================================================*/

dsat_num_item_type dsat_get_qcsimapp_info_idx(void)
{
  dsat_num_item_type idx;

  idx = dsat_get_qcsimapp_val();
  if ( (PRIMA_SUBS == idx) &&
       (dsatcmdp_get_current_mode() != DSAT_MODE_1XLTE)&&
       (IS_CDMA_MODE(dsatcmdp_get_current_mode())))
  {
    idx = CDMA_SUBS;
  }
  DS_AT_MSG1_MED("dsat_qcsimapp_val idx = %d", idx);
  return idx;
}/*dsat_get_qcsimapp_info_idx*/

#ifdef FEATURE_ATCOP_ECHO_CTL_MULTI_PORTS
/*===========================================================================

FUNCTION DSATCTAB_GET_MODEM_PORT_ID

DESCRIPTION
  This function returns the corresponding modem port id for AT terminal

DEPENDENCIES
  None
  
RETURN VALUE
  -1: if the input port is not modem port

SIDE EFFECTS
  None

===========================================================================*/
int dsatctab_get_modem_port_id(ds3g_siolib_port_e_type port)
{

  int modem_port_id = DSAT_MODEM_PORT_NONE;

  if ( port == DS3G_SIOLIB_USB_CDC_PORT )
  {
    modem_port_id = DSAT_MODEM_PORT_1;
  }
  else if ( port == DS3G_SIOLIB_USB_SER3_PORT ) 
  {
    modem_port_id = DSAT_MODEM_PORT_2;
  }
  else
  {
  }
  return modem_port_id;
} /*dsatctab_get_modem_port_id*/

#endif /*FEATURE_ATCOP_ECHO_CTL_MULTI_PORTS*/

