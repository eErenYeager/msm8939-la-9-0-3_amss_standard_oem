#ifndef DSATETSICMIF_H
#define DSATETSICMIF_H
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                D A T A   S E R V I C E S

                A T   C O M M A N D   
                
                ( E T S I  C A L L  M A N A G E R  I N T E R F A C E )
                
                P R O C E S S I N G

                E X T E R N A L   H E A D E R   F I L E


GENERAL DESCRIPTION
  This file contains the definitions of data structures, defined and
  enumerated constants and function prototypes required for the
  call manager interface.

INITIALIZATION AND SEQUENCING REQUIREMENTS

   Copyright (c) 2002 - 2013 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/


/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $PVCSPath:  L:/src/asw/MSM5200/data/vcs/dsatetsicmif.h_v   1.1   12 Jul 2002 10:09:44   randrew  $
  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/atcop/src/dsatetsicmif.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     ---------------------------------------------------------
02/15/14   pg      Added support for SGLTE+G
06/05/13   tk      ATCoP changes for SGLTE support on Dime Plus.
05/16/13   sk      $QCCOPS command for Extension of network search.
01/04/13   tk      Fixed +CPAS and +CEER issues.
11/30/12   tk      ATCoP changes for Triton DSDA.
06/15/12   sk      Fixed CEER issue.
03/07/12   sk      Made CLCC common for all modes.
02/17/12   sk      Migrating Data related AT commands interface to unified MH.
12/26/11   ad      Added Support the New Call Control Interface in Call Manager.
01/19/12   sk      Feature cleanup.
06/04/10   bs      MMGSDI SESSION Migration.
01/15/10   ua      Moving out send_response_to_te to common file. 
12/15/09   nc      Featurisation changes for LTE.
04/20/09   ua      Fixed issues related with call commands followed by +CIND?
03/04/09   sa      AU level CMI modifications.
11/04/07   pp      Lint low - fixes.
02/21/06   sa      Merged changes from Sirius Branch
                   -Removed prototypes for activate & deactivate SS service.
01/16/06   ua      Added support for +CDIP command.
07/29/05   ar      Migrate CM SS events to watermark queue. 
07/25/05   snb     Make function dsatetsicmif_find_context_state_from_call_id()
                   external.
04/29/05   ar      Add support for capturing call end status.
04/05/05   iv      Modified +CLCC to support CS data calls.
04/04/05   ar      Added support for +CIND command.
03/29/05   ar      Adjusted change_network_registration() description.
02/03/05   tkk     Added prototypes for SS activate and deactivate.
03/19/04   snb     Add external function to display +CCWA unsolicited result 
                   code.
01/09/04   ar      Add dsatetsicmif_deactivate_all_contexts() prototype
01/05/05   iv      Added supoport for +CLCC command
11/11/04   tkk     Added prototypes for dsatetsicmif_report_cxxp_result 
12/07/04   ar      Add support for +COPS command.
10/22/03   ar      Move Call Manager PH & SS API calls from ETSI to COMMON.
10/15/04   ar      Add Advice of Charge support.
09/23/03   snb     Add support for PS attach timeout failure
07/31/03   ar      Add address type constants.
03/07/03   ar      Remove FEATURE_DATA_ETSI_SUPSERV wrappers
01/16/03   ar      Added dsatetsicmif_send_respose_to_te() prototype
01/10/03   sb      Added support fot +CGATT command
11/21/01   ar      Added support for +CLCK command
10/31/02   ar      Added support for +CCFC command
10/28/02   sb      Opened Voice call related handlers (moved to a common file)
04/18/02   ar      Created module.

===========================================================================*/
#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"


/* this file should be included only in GSM or WCDMA mode */
#ifdef FEATURE_DSAT_ETSI_MODE

#include "dsat_v.h"
#include "dsati.h"
#include "dsatcmif.h"
#include "dsatetsicall.h"
#ifdef FEATURE_DSAT_ETSI_DATA
#include "dsatetsipkt.h"
#endif /* FEATURE_DSAT_ETSI_DATA */

#include "mcs_sups.h"

/*===========================================================================

                        PUBLIC DATA DECLARATIONS

===========================================================================*/

#define DSAT_TA_UNKNOWN       129 /* 0x80|CM_TON_UNKNOWN      |CM_NPI_ISDN */
#define DSAT_TA_INTERNATIONAL 145 /* 0x80|CM_TON_INTERNATIONAL|CM_NPI_ISDN */
#define DSAT_TA_INTER_PREFIX  '+' /* ETSI international call dial prefix */
#define E_SUCCESS              0  /*0 is E_SUCCESS for MCS  */

typedef enum clcc_state
{
  DSAT_CLCC_STATE_ACTIVE   = 0,
  DSAT_CLCC_STATE_HELD     = 1,
  DSAT_CLCC_STATE_DIALING  = 2,
  DSAT_CLCC_STATE_ALERTING = 3,
  DSAT_CLCC_STATE_INCOMING = 4,
  DSAT_CLCC_STATE_WAITING  = 5,
  DSAT_CLCC_STATE_MAX
}dsat_clcc_state_e_type;

typedef enum clcc_mode
{
  DSAT_CLCC_MODE_VOICE = 0,
  DSAT_CLCC_MODE_DATA  = 1,
  DSAT_CLCC_MODE_FAX   = 2
}dsat_clcc_mode_e_type;

typedef enum clcc_dir
{
  DSAT_CLCC_DIRECTION_MO = 0,
  DSAT_CLCC_DIRECTION_MT  = 1
}dsat_clcc_dir_e_type;

#define DSAT_CLCC_MPTY_CALL        1
typedef struct
{
  byte                  dsat_reg_number[CM_CALLED_PARTY_BCD_NO_LENGTH];
}dsatetsicmif_sups_state_ss_info;
/* Sups management data */
typedef struct supserv_cm_op_s
{
  ss_user_data_s_type       user_data;   /* User entered parameters         */
  supserv_cm_parm_u_type    cm_parms;    /* CM API parameters               */
  uint16                    curr_class;  /* Track current class             */ 
  uint16                    done_class;  /* Track classes already processed */
  uint8                     sc_code;     /* Service code for current class  */
  boolean                   pending_msg; /* Flag pending msg                */    
  boolean                   pending_rsp; /* Flag pending response           */    
  boolean                   abort_cmd;   /* Flag command abort              */    
  cusd_state_e_type         ussd_state;  /* USSD state tracking             */
  uint8                     invoke_id;   /* Network invocation ID           */
  uint8                     ussd_oper_type; /* USSD operation type */
  boolean                   pending_clck_pwd_ind; /* Flag for pending
                                              password indication for +CLCK */
} supserv_cm_op_s_type;

/* Union for storing async event payload info in DSM item */ 
typedef union
{
  struct
  {
    ss_password_value        guidance_info;   /* Password guidance information */
  } cpwd;

  struct 
{
    uss_data_s_type          uss_data;        /* Unstructured SS data */
    cm_uss_data_e_type       uss_data_type;   /* Type of USS message */
  } cusd;

  struct
{
    basic_service_s_type                basic_service;   
    /* Basic Service Group information */
    ie_forwarding_feature_list_s_type   fwd_feature_list;
    /* Forwarding Feature list information */
  } ccfc;

  struct
{
    basic_service_s_type                basic_service;   
    /* Basic Service Group information */
    ie_call_barring_info_s_type         call_barring_info;
    /* Call barring information */
    basic_service_group_list_s_type     bsg_list;
    /* Basic Service Group list information */
  } clck;

  struct
{
    basic_service_s_type                basic_service;   
    /* Basic Service Group information */
    basic_service_group_list_s_type     bsg_list;
    /* Basic Service Group list information */
  } ccwa;
} dsat_sups_cmd_info_u_type;
typedef struct dsat_sups_state_ms_info
  {
  supserv_cm_op_s_type  dsat_ss_cm_data;
/* Holding buffer for supplementary services cmd event info. */
/* Used in place of DS task cmd buffer to conserve storage.  */
  dsat_sups_cmd_info_u_type   dsat_sups_cmd_info;
  ss_operation_code_T         dsat_interr_ss_service;
  boolean                     dsat_ss_abort_status;
 cli_restriction_info_T       dsatetsicall_clir_subscription_option;

}dsatetsicmif_sups_state_ms_info;

typedef struct
{
  dsatetsicmif_sups_state_ss_info  *ss;
  dsatetsicmif_sups_state_ms_info  *ms[DSAT_MS_MAX_SUBS];
}dsatetsicmif_sups_state_info;

/*-------------------------------------------------------------------------
            PUBLIC FUNCTION DECLARATIONS
-------------------------------------------------------------------------*/

/*===========================================================================

FUNCTION  DSATETSICMIF_INITIATE_VOICE_CALL

DESCRIPTION
  This function requests CM to start a voice call.

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    FALSE :    if there was any problem in executing the command
    TRUE  :    if it succeeds.

SIDE EFFECTS
  None

===========================================================================*/
boolean dsatetsicmif_initiate_voice_call
(
  /* contains the dialing number and its attributes */
  const dsati_dial_str_type     * dial_str,

  /* returns the call id which is returned by cm */
  uint8 * call_id_ptr
);


/*===========================================================================

FUNCTION DSATETSICMIF_SUPS_CLASS_ACTION

DESCRIPTION
  This function intiates the action in Call Manager to perform supplemental
  service commands for classes.  The user parameters stored in regional variable
  dsat_ss_cm_data are expected to be validated. This routine populates
  the Call Manager API structures.
  
DEPENDENCIES
  None

RETURN VALUE
  Returns an enum that describes the result of the execution.
  Possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_ASYNC_CMD : if it is a success.

SIDE EFFECTS
  Command is sent to Call Manager
  
===========================================================================*/
dsat_result_enum_type dsatetsicmif_sups_class_action
(
  boolean                    first_call,       /* Flag for recursive loop   */
  ds_subs_e_type             subs_info
);


/*===========================================================================

FUNCTION DSATETSICMIF_SUPS_CHANGE_PASSWORD

DESCRIPTION
  This function intiates the change of supplementary services facility
  password. The user parameters stored in regional variable
  dsat_ss_cm_data are expected to be validated. This routine populates
  the Call Manager API structures.
  
DEPENDENCIES
  None

RETURN VALUE
  Returns an enum that describes the result of the execution.
  Possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_ASYNC_CMD : if it is a success.

SIDE EFFECTS
  Command is sent to Call Manager
  
===========================================================================*/
dsat_result_enum_type dsatetsicmif_sups_change_password (void);


/*===========================================================================

FUNCTION DSATETSICMIF_SUPS_PROCESS_USSD

DESCRIPTION
  This function intiates the action in Call Manager to perform supplemental
  service commands for Unstructured Supplementary Service Data (USSD).
  The user parameters stored in regional variable dsat_ss_cm_data are expected
  to be validated. This routine populates the Call Manager API structures.
  
DEPENDENCIES
  None

RETURN VALUE
  Returns an enum that describes the result of the execution.
  Possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_ASYNC_CMD : if it is a success.

SIDE EFFECTS
  Command is sent to Call Manager
  
===========================================================================*/
dsat_result_enum_type dsatetsicmif_sups_process_ussd (void);


/*===========================================================================

FUNCTION  DSATETSICMIF_CM_SUPS_CMD_HANDLER

DESCRIPTION
  This function is the handler function for the CM supplementary service
  commands.

DEPENDENCIES
  None

RETURN VALUE
  Returns an enum that describes the result of the command execution.
  Possible values:
    DSAT_ERROR :      if there was any problem in execution.
    DSAT_ASYNC_CMD :  if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatetsicmif_cm_sups_cmd_handler
(
  dsat_mode_enum_type   mode,                /* AT command mode            */
  ds_cmd_type         * cmd_ptr              /* DS Command pointer         */
);


/*===========================================================================
FUNCTION  DSATETSICMIF_SUPS_EVENT_CB_FUNC

DESCRIPTION
  CM supplementary service event callback function

DEPENDENCIES
  None

RETURNS
  None

SIDE EFFECTS
  Adds command in DS command buffer

===========================================================================*/
void dsatetsicmif_sups_event_cb_func 
(
  cm_sups_event_e_type             event,          /* Event ID              */
  const cm_sups_info_s_type       *event_ptr      /* Pointer to Event info */
);
/*===========================================================================

FUNCTION  DSATETSICMIF_UPDATE_SUBS_GLOBAL

DESCRIPTION
  This function update required subs global variables.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/

void dsatetsicmif_update_sups_global
(
  ds_cmd_type         * cmd_ptr              /* DS Command pointer         */
);

/*===========================================================================

FUNCTION  DSATETSICMIF_CM_SUPS_EVENT_HANDLER

DESCRIPTION
  This function is the handler function for the CM supplementary service
  related events

DEPENDENCIES
  None

RETURN VALUE
  Returns an enum that describes the result of the execution.
  Possible values:
    DSAT_ERROR :       if there was any problem in execution
    DSAT_ASYNC_EVENT : if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatetsicmif_cm_sups_event_handler
(
  dsat_mode_enum_type   mode,                /* AT command mode            */
  ds_cmd_type         * cmd_ptr              /* DS Command pointer         */
);


/*===========================================================================

FUNCTION DSATETSICMIF_SUPS_ABORT_HANDLER

DESCRIPTION
  This function does nothing but return a result to indicate the
  supplementary service command should be aborted.  It should be
  installed in the command table as the abort handler function for
  abortable supplementary service commands.

DEPENDENCIES
  None

RETURN VALUE
  Value indicating whether or not call should be aborted:
      TRUE if call should be aborted, FALSE otherwise.

SIDE EFFECTS
  None

===========================================================================*/
/* ARGSUSED */
boolean dsatetsicmif_sups_abort_handler
(
  const dsati_cmd_type *cmd_table_entry_ptr  /* Ptr to cmd entry in table. */
);


/*===========================================================================
FUNCTION  DSATETSICMIF_REPORT_CCWA_RESULT

DESCRIPTION
  This function generates the unsolicited result code for +CCWA command.
  The +CCWA command <n> parameter controls whether results are presented
  or suppressed.

  If ATCOP is not in command mode or TE indications are blocked,
  the result code is not sent to TE.
  
DEPENDENCIES
  None

RETURNS
  Returns an enum that describes the result of the execution.
  Possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_ASYNC_EVENT : if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatetsicmif_report_ccwa_result 
(
  dsat_mode_enum_type            mode,          /* AT command mode */
  ds_subs_e_type                 subs_id,       /* Subscription ID */
  const ds_at_call_info_s_type  *call_info_ptr  /* Incoming call information */
);


/*===========================================================================
FUNCTION  DSATETSICMIF_AOC_SET_ACMMAX

DESCRIPTION
  Submit command to Call Manager to set the Advice of Charge (AoC)
  call meter maximum.  Note a value of zero will disable AoC.

DEPENDENCIES
  None

RETURNS
  Returns an enum that describes the result of the execution.
    DSAT_ASYNC_CMD :   if command sent to Call Manager
    DSAT_ERROR     :   if error occurred  

SIDE EFFECTS
  Sends command to Call Manager

===========================================================================*/
dsat_result_enum_type dsatetsicmif_aoc_set_acmmax
(
  uint32 acmmax
);

/*===========================================================================
FUNCTION  DSATETSICMIF_AOC_RESET_ACM

DESCRIPTION
  Submit command to Call Manager to reset the Advice of Charge (AoC)
  call meter.

DEPENDENCIES
  None

RETURNS
  Returns an enum that describes the result of the execution.
    DSAT_ASYNC_CMD :   if command sent to Call Manager
    DSAT_ERROR     :   if error occurred  

SIDE EFFECTS
  Sends command to Call Manager

===========================================================================*/
dsat_result_enum_type dsatetsicmif_aoc_reset_acm ( void );


/*===========================================================================
FUNCTION  DSATETSICMIF_GEN_CMEE_ERROR

DESCRIPTION
  Generate the passed +CME error response.  One DSM item is allocated for
  response buffer.

DEPENDENCIES
  None

RETURNS
  Returns an enum that describes the result of the execution.
    DSAT_CMD_ERR_RSP :   if +CME error respose was generated

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatetsicmif_gen_cmee_error 
(
  dsat_cme_error_e_type   error_code,                      /* Error ID      */
  ds_subs_e_type          subs_info
);


/*===========================================================================

FUNCTION  DSATETSICMIF_QUERY_NETWORKS

DESCRIPTION
  This function invokes the CM API to get either the list of available
  networks from the access stratum or the preferred network list from
  the SIM/USIM.  Handling of the results is done asynchronously in the
  Phone command event handler for the CM_PH_EVENT_AVAILABLE_NETWORKS_CONF
  or CM_PH_EVENT_PREFERRED_NETWORKS_CONF events.

DEPENDENCIES
  None

RETURN VALUE
  Returns an enum that describes the result of the execution.
  Possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_ASYNC_CMD : if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatetsicmif_query_networks
(
  cm_network_list_type_e_type net_type
);


/*===========================================================================

FUNCTION   DSATETSICMIF_CHANGE_NETWORK_REGISTRATION

DESCRIPTION
  This function invokes the CM API to change the network registration
  preference.  The passed in mode, PLMN, and access technology
  parameters are used to configure the call to the CM API.  For manual
  or automatic network registration, a single API call is required.
  For automatic if manual fails, a second call to the CM API is
  required once the NO_SRV indication is reported.  The aborting flag
  indicates whether the state machine should be updated.

  If the requested mode is AUTO and matches the current preference, no
  action is taken as the lower layers may do unnecessary detach &
  attach cycle.

DEPENDENCIES
  None

RETURN VALUE
  Returns an enum that describes the result of the execution.
  Possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if no action was taken.
    DSAT_ASYNC_CMD : if command was sent to CM API successfully.

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatetsicmif_change_network_registration
(
  dsat_cops_mode_e_type       net_mode,
  sys_plmn_id_s_type *        plmn_ptr,
  dsat_cops_act_e_type        net_act,
    ds_subs_e_type            subs_info
);

/*===========================================================================

FUNCTION   DSATETSICMIF_LOOKUP_CM_RAT_PREF

DESCRIPTION
  This function performs lookup to/from the Call Manager radio access
  technology preference based on the input ATCOP AcT parameter value.

DEPENDENCIES
  None

RETURN VALUE
  TRUE on sucessful lookup; FALSE otherwise.

SIDE EFFECTS
  None

===========================================================================*/
boolean dsatetsicmif_lookup_cm_rat_pref
(
  boolean                to_cm_pref,
  dsat_cops_act_e_type  *net_act_ptr,
  cm_mode_pref_e_type   *cm_rat_ptr
);

#ifdef FEATURE_DSAT_ETSI_DATA

#ifdef FEATURE_ETSI_ATTACH
/*===========================================================================

FUNCTION  DSATETSICMIF_ATTACH_PS

DESCRIPTION
  Wrapper function to inform CM to attach to a PS domain. Called from CGATT
  command

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR :    if there was any problem in executing the command
    DSAT_OK :       if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatetsicmif_attach_ps ( void );

/*===========================================================================

FUNCTION  DSATETSICMIF_DETACH_PS

DESCRIPTION
  Wrapper function to inform CM to detach from the PS domain. Called from 
  CGATT command

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR :    if there was any problem in executing the command
    DSAT_OK :       if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatetsicmif_detach_ps ( void );
#endif /* FEATURE_ETSI_ATTACH */

/*===========================================================================

FUNCTION DSATETSICMIF_INIT_PDP_CONNECT_STATE

DESCRIPTION
  This function intializes the PDP context profile connection state
  information.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
  
===========================================================================*/
void dsatetsicmif_init_pdp_connect_state ( void );


/*===========================================================================

FUNCTION  DSATETSICMIF_GET_CM_CALL_STATE

DESCRIPTION
  This function queries Call Manager to get the current call state
  information.  The results of the query are returned asychronously.

DEPENDENCIES
  None

RETURN VALUE
  Returns an enum that describes the result of the execution.
  Possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_ASYNC_CMD : if it is a success.

SIDE EFFECTS
  CM is sent a async command

===========================================================================*/
dsat_result_enum_type dsatetsicmif_get_cm_call_state ( void );

/*===========================================================================

FUNCTION  DSATETSICMIF_DEACTIVATE_ALL_CONTEXTS

DESCRIPTION
  This function initiates PDP context deactivation for all contexts.
  This should be called when network domain changes to PS detach, which
  must result in all active contexts becoming deactivated according
  to 3GPP TS 27.007 section 10.1.9

DEPENDENCIES
  None

RETURN VALUE
  Returns an enum that describes the result of the execution.
  Possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_ASYNC_CMD : if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatetsicmif_deactivate_all_contexts ( void );

/*===========================================================================

FUNCTION  DSATETSICMIF_CONTEXT_ACTIVATION_ATZ_CB

DESCRIPTION
  This function intiates PDP context deactivation in response to ATZ command.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
void dsatetsicmif_context_activation_atz_cb ( void );
          
#endif /* FEATURE_DSAT_ETSI_DATA */

/*===========================================================================

FUNCTION  DSATETSICMIF_PROCESS_CM_MM_INFO_DATA

DESCRIPTION
  This function processes the Call Manager MM information data to display 
  the timezone status to TE

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR :    if there was any problem in executing the command
    DSAT_OK :       if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatetsicmif_process_cm_mm_info_data
(
  const sys_mm_information_s_type * mm_info_ptr,        /* MM Info pointer */
  ds_subs_e_type                    subs_info
);

/*===========================================================================

FUNCTION  DSATETSICMIF_PROCESS_CM_MM_INFO_NET_REG

DESCRIPTION
  This function processes the Call Manager MM information to set
  network registration information.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
void dsatetsicmif_process_cm_mm_info_net_reg
(
  const sys_mm_information_s_type *mm_info_ptr,        /* MM Info pointer */
  ds_subs_e_type                   subs_info
);

/*===========================================================================

FUNCTION  DSATETSICMIF_GET_COLP_INFO_OR_REPORT_RESULT

DESCRIPTION
  This function gets the connected line info (or) reports failure if not 
  successful.

DEPENDENCIES
  None

RETURN VALUE
  Returns an enum that describes the result of the execution.
  Possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatetsicmif_get_colp_info_or_report_result
(
  dsat_mode_enum_type           mode,           /* AT command mode */
  ds_subs_e_type                subs_id,        /* Subscription ID */
  const ds_at_call_info_s_type *call_info_ptr   /* Incoming call information */
);


/*==========================================================================
FUNCTION  DSATETSICMIF_GET_CLIP_INFO_OR_REPORT_RESULT

DESCRIPTION
  This function gets call information given a call ID.

DEPENDENCIES
  None

RETURN VALUE
  Returns an enum that describes the result of the execution.
  Possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatetsicmif_get_clip_info_or_report_result
(
  ds_subs_e_type  subs_id,
  cm_call_id_type call_id
);

/*==========================================================================
FUNCTION  DSATETSICMIF_GET_CDIP_INFO_OR_REPORT_RESULT

DESCRIPTION
  This function report Caller ID to the TE if CDIP reporting is enabled.

DEPENDENCIES
  None

RETURN VALUE
  Returns an enum that describes the result of the execution.
  Possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatetsicmif_get_cdip_info_or_report_result
(
  ds_subs_e_type  subs_id,
  cm_call_id_type call_id
);

/*==========================================================================
FUNCTION  DSAETSICMIF_CM_SUPS_CMD_INTERROGATE

DESCRIPTION
  This function interrogates the n/w for a particular SS service.

DEPENDENCIES
  None

RETURN VALUE
  Returns an enum that describes the result of the execution.
  Possible values:
True- Success
FALSE - failure

SIDE EFFECTS
  None

===========================================================================*/
boolean
dsatetsicmif_cm_sups_cmd_interrogate
(
  void                               *data_block_ptr,
  cm_client_id_type                   dsatcm_client_id_lcl,
  const cm_interr_ss_params_s_type   *interr_clip_params
);

/*===========================================================================
FUNCTION DSATETSICMIF_CLCC_ABORT_EVENT_HANDLER

DESCRIPTION
  This function handles the +CLCC abort event handling. Most of the
  processing is done in the abort command handler.  This routine just
  ensures the AT command processor returns to normal processing.

DEPENDENCIES
  None

RETURN VALUE
  DSAT_ERROR : As command is aborted

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatetsicmif_clcc_abort_event_handler
(
  ds_at_cmd_status_type cmd_info
);


/*==========================================================================
FUNCTION  DSATETSICMIF_PROCESS_RSSI_EVENT

DESCRIPTION
  This function processes the CM_SS_EVENT_RSSI from Call Manager.  It
  updates the +CIND indicator value.

DEPENDENCIES
  None

RETURN VALUE
  Returns an enum that describes the result of the execution.
  Possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatetsicmif_process_rssi_event
(
  const ds_at_cm_ss_event_type * event_ptr,      /* Event structure         */
  dsat_stack_id_e_type           stack_id,
  ds_subs_e_type                 subs_info
);


/*==========================================================================
FUNCTION  DSATETSICMIF_CAPTURE_CALL_END_STATUS

DESCRIPTION
  This function captures the call end status information from CM
  events.  The events contianing the informaiton are CM_CALL_EVENT_END
  and CM_CALL_EVENT_CALL_CONF.  Both CS and PS domain calls are
  monitored.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
void dsatetsicmif_capture_call_end_status
(
  ds_subs_e_type          subs_id,
  cm_call_event_e_type    event,            /* Event ID              */
  ds_at_call_info_s_type *event_ptr         /* Pointer to Event info */
);
/*===========================================================================

FUNCTION  DSAT_UPDATE_MODE_INFO

DESCRIPTION
  This function update mode info for the CM call-related 
  events.

DEPENDENCIES
  None

RETURN VALUE
  None.

SIDE EFFECTS
  None

===========================================================================*/

void dsat_update_mode_info
(
  dsat_cm_call_mode_info_s_type        *dsat_mode_info, /*Destination*/
  cm_call_mode_info_s_type             *cm_mode_info   /*Source*/
);
/*===========================================================================

FUNCTION  DSATETSICMIF_REPORT_CCFC

DESCRIPTION
  This function reports the call forwarding supplementary service status
  received from Call Manager.

DEPENDENCIES
  None

RETURN VALUE
  Returns an enum that describes the result of the execution.
  Possible values:
    DSAT_ERROR :    if there was any problem in execution.
    DSAT_OK :       if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatetsicmif_report_ccfc
(
  const dsat_mode_enum_type   mode,           /* AT command mode            */
  ds_at_cm_sups_event_type*  sups_event,       /* SUPS Event pointer         */
  const dsat_sups_cmd_info_u_type * cinfo_ptr, /* Command event info pointer */
  ds_subs_e_type                 subs_info
);

/*===========================================================================

FUNCTION  DSATETSICMIF_RETURN_CCFC_REG_NUM

DESCRIPTION
  This function return a pointer to the number
DEPENDENCIES
  None

RETURN VALUE
   Returns a pointer to number .

SIDE EFFECTS
  None

===========================================================================*/
byte* dsatetsicmif_return_ccfc_reg_num(void);
/*===========================================================================

FUNCTION  DSATETSICMIF_REPORT_CLCK

DESCRIPTION
  This function reports the facility lock supplementary service status
  received from Call Manager or GSDI.

DEPENDENCIES
  None

RETURN VALUE
  Returns an enum that describes the result of the execution.
  Possible values:
    DSAT_ERROR :    if there was any problem in execution.
    DSAT_OK :       if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatetsicmif_report_clck
(
  const dsat_mode_enum_type      mode,        /* AT command mode            */
  const ds_cmd_type            * cmd_ptr,     /* DS Command pointer         */
  const ds_at_cmd_status_type  * status_ptr,  /* DS Status pointer          */
  const dsat_sups_cmd_info_u_type * cinfo_ptr, /* Command event info pointer */
  ds_subs_e_type                    subs_info
);

/*===========================================================================

FUNCTION  DSATETSICMIF_REPORT_CCWA

DESCRIPTION
  This function reports the call waiting supplementary service status
  received from Call Manager. 

DEPENDENCIES
  None

RETURN VALUE
  Returns an enum that describes the result of the execution.
  Possible values:
    DSAT_ERROR :    if there was any problem in execution.
    DSAT_OK :       if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatetsicmif_report_ccwa
(
  const dsat_mode_enum_type      mode,        /* AT command mode            */
  const ds_cmd_type            * cmd_ptr,     /* DS Command pointer         */
  const ds_at_cmd_status_type  * status_ptr,  /* DS Status pointer          */
  const dsat_sups_cmd_info_u_type * cinfo_ptr, /* Command event info pointer */
  ds_subs_e_type                    subs_info
);
#endif /* FEATURE_DSAT_ETSI_MODE */
#endif /* DSATETSICMIF_H */
