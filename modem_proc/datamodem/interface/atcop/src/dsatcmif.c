/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                D A T A   S E R V I C E S

                A T   C O M M A N D   
                
                ( C A L L  M A N A G E R  I N T E R F A C E )
                
                P R O C E S S I N G

GENERAL DESCRIPTION
  This software unit contains functions for interfacing to Call Manager.

EXTERNALIZED FUNCTIONS

EXTERNALIZED FUNCTIONS INTERNAL TO DSAT UNIT
  dsatcmif_cm_call_cmd_handler
    This function is the handler function for the CM call related commands

  dsatcmif_cm_call_event_handler
    This function is the handler function for the CM call related events

  dsatcmif_cm_ss_cmd_handler
    This function is the handler function for the CM serving system commands

  dsatcmif_cm_ss_event_handler
    This function is the handler function for the CM serving system
    related events

  dsatcmif_get_cm_ss_info
    This function issue query to Call Manager to get serving system (SS) info

  dsatcmif_end_calls
    This function issues a request to CM to end call(s).

  dsatcmif_change_operating_mode
    This function submits request to Call Manager to change the phone
    operating mode.  The results of the call are handled as asynch
    events.
    
  dsatcmif_signal_handler
    This function processes the asynchronous signals assosiacted with
    messages from Call Manager.

INITIALIZATION AND SEQUENCING REQUIREMENTS

   Copyright (c) 2002 - 2015 by Qualcomm Technologies Incorporated.
   All Rights Reserved.
   Qualcomm Confidential and Proprietary.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/


/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $PVCSPath: L:/src/asw/MM_DATA/vcs/dsatcmif.c_v   1.4   12 Nov 2002 13:00:24   sramacha  $
  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/atcop/src/dsatcmif.c#3 $ $DateTime: 2015/10/20 04:33:55 $ $Author: c_sjchau $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
10/20/15   skc     Fixed +CHLD AT Command IP call issue. 
10/20/15   skc     Fixed +CHLD AT command issue.
12/26/14   sc      Fixed VoLTE issue related to IP mode info.
7/27/14    pg      ATCoP changes for Data Plus Data.
07/08/14   sc      Fixed SGLTE+G issue on SUB2.
06/05/14   sc      Fixed LLVM errors.
05/27/14   pg      Fixed memory leak for emergency number.
05/21/14   pg      Fixed NOCARRIER response for command issue.
05/07/14   sc      Fixed +CLCC issue.
04/29/14   sc      Added csg_list_cat support to $QCCSGCOPS command.
04/23/14   sc      Fixed +CFUN issue.
04/15/14   sc      Fixed +COPS DSDS issue.
04/03/14   pg      Fixed subs feature mode updation.
03/26/14   sc      Fixed SYSINFO display issue on SUB2.
03/20/14   sc      Fixed SYSCONFIG issue on SUB2.
03/11/14   tk      Optimized debug macros usage in ATCoP.
02/15/14   pg      Added support for SGLTE+G.
01/13/14   sc      Fixed +COPS issue in invalid state for subs proc.
01/09/14   sc      Fixed $QCSIMAPP issue in 3GPP2 service mode.
01/09/14   tk      Fixed +CFUN issue in C+G DSDS configuration at boot-up.
12/25/13   tk      Enhanced $QCSIMAPP command for better user experience.
11/12/13   sc      Added support for $QCCSGCOPS command.
11/01/13   sc      LTE related CM call events support for AT+CEER
10/23/13   sc      Fixed +CFUN and +COPS issue.
10/18/13   tk      Added support for OnChip SIM in +CFUN command.
10/18/13   sc      Added support for $QCNSP, $QCSIMT, $QCRCIND commands and
                   REMOTE CALL END, REMOTE RING, REMOTE ANSWER
                   unsolicited result codes.
10/03/13   tk      ATCoP changes for L+G DSDS.
09/24/13   sc      Added support for +CNMPSD command.
09/13/13   tk      ATCoP changes for LTE and DSDS concurrency solution.
09/13/13   sc      Fixed KW errors.
08/20/13   sc      Fixed +COPS issue with subscription already available error.
07/26/13   pg      Fixed no response for ^PREFMODE=2.
07/27/13   pg      Fixed CLCC issue on SUB2.
07/24/13   tk      Mainlined FEATURE_DSAT_HIGH_TIER for +CGATT command.
07/16/13   pg      Fixed no ^MODE URC when UE goes out of service and comes back
07/16/13   pg      Fixed ^SYSINFO o/p for "SIM is not inserted"
06/05/13   tk      ATCoP changes for SGLTE support on Dime Plus.
05/24/13   sd      TSTS changes for DS3G.
05/16/13   sk      $QCCOPS command for Extension of network search.
04/19/13   tk      Fixed issue in DSDS device mode.
04/08/13   tk      Fixed issue with restricted mode in few scenarios.
03/27/13   tk      Fixed +CGEV URC issue.
03/27/13   tk      Fixed double free issue with emergency number list.
03/27/13   tk      Fixed issues in dual stack commands.
03/17/13   tk      Fixed stack overflow issue with emergency number list.
03/15/13   tk      Fixed +CFUN issue in CDMA in single SIM device mode.
03/01/13   tk      Fixed COPS issue in DSDS.
02/22/13   tk      ATCoP - CM interface changes for Triton DSDA.
02/07/13   tk      ATCoP changes for DSDx Co-existence with TDS.
02/07/13   tk      Fixed CGATT issue when ciphering is enabled.
02/01/13   tk      Fixed DSCI URC issue with call end.
01/23/13   sk      DS command buffer memory reduction.
01/09/13   tk      Fixed issues found in DTF CI runs.
01/04/13   tk      Fixed +CPAS and +CEER issues.
01/04/13   tk      ATCoP changes for DSDX C/G+G with True World Mode Support.
11/30/12   tk      ATCoP changes for Triton DSDA.
11/21/12   sk      Fixed CSQ issue due to invalid updation of RSSI,BER values.
09/24/12   tk      Migrated to CM APIs for MRU table access.
08/22/12   sk      Mainlining Nikel Changes.
08/10/12   tk      Fixed ^MODE issue in reporting CDMA/HDR hybrid mode.
08/06/12   tk      Added support for $QCDRX command.
07/26/12   sb      Fixed KW Errors.
07/16/12   nc      New interface changes for Manual PLMN selection.
06/15/12   nc      Fixed CGATT in no service.
06/20/12   tk      Fixed issues related to ^HDRCSQ and ^SYSINFO commands.
06/12/12   tk      Extended $QCSYSMODE command for 3GPP2 modes.
06/01/12   sk      TDS mode changes.
05/21/12   tk      Fixed COPS manual PLMN selection issue.
05/18/12   tk      Migrated to MSG 2.0 macros
05/11/12   sk      Fixed CLCC MT calls reporting in CDMA mode.
04/19/12   sk      Resolved compilation error when FEATURE_HS_USB_SER3_PORT enabled.
04/11/12   sk      Adding support for forwarding CFUN.
03/22/12   sk      Fixed CFUN command when executed without a card.
03/07/12   sk      Made CLCC common for all modes.
02/20 12   sb      Fixed Compiler Warnings.
02/17/12   sb      Fixed Compiler and KW Warnings.
02/17/12   sk      Migrating Data related AT commands interface to unified MH.
02/06/12   ad      Added CSTF/CEN/CGPIAF/CVMOD command support.
02/06/12   nc      Added support for ^SPN and URC Queue.
01/27/12   sk      Fixed +CLCC in CDMA mode.
12/26/11   ad      Added Support the New Call Control Interface in Call Manager.
01/19/12   sk      Feature cleanup.
01/04/12   sb      Fixed KW Warnings.
12/06/11   sk      Fixed AT$QCBANDPREF write and read issue CR:321373
11/28/11   sb      Fixed the mode check for answering voice calls when mode is in LTE 
11/25/11   sb      Fixed Not considering SYS_SRV_STATUS_LIMITED_REGIONAL as a valid srv_status.
11/05/11   nc      Feature wrapped '^MODE' URC with FEATURE_TDSCDMA.
09/06/10   nc      Fixed the RSSI and RSCP display when 
                   FEATURE_WCDMA_DATA_FOR_UI_BAR_DISPLAY is enabled.
09/09/11   nc      Fixed $QCDGEN over IRAT scenarios.
09/01/11   ad      Fixed +CEMODE issue. 
07/21/11   sa      Added support for 64QAM configuration indication while roaming.
07/01/11   bs      Fixed +CGATT issue in PS only mode.
07/05/11   nc      Added support for ^MODE,^CARDMODE,^SYSCONFIG,^SYSINFO,^DSCI.
05/17/11   bs      Fixed +COPS issue.
04/29/11   bs      Fixed $QCDGEN command to use COMPTASK.
04/27/11   bs      Added NIKEL Phase I support.
03/29/11   nc      Fixed +VTS command.
03/01/11   nc      Modified +CEMODE to be inline with CM Design.
02/24/11   bs      Fixed Klocwork errors.
02/10/11   ad      Fixed CM related commands .
02/11/10   bs      Fixed incorrect PNN display for LTE.
01/11/11   ad      Removed pending variable .
01/11/11   ad      Remove extern usage.used get/set API for command associated  
                   value pointers.
10/25/10   ad      Init Changes for DSDS. 
10/19/10   bs      Fixed +COPS handling for power save.
10/19/10   ad      Relocated MSG_HIGH before memory free.  
10/06/10   ad      Added Error msg before Assert(0).
10/12/10   bs      Fixed +CREG issue.
09/20/10   bs      Fixed +CGATT issue.
07/09/10   bs      Fixed +CGACT for LTE.
06/04/10   bs      MMGSDI SESSION Migration.
06/02/10   bs      Fixed compilation issues.
05/31/10   ad      Added support for S7 command.
05/17/10   ad      Added Support for +CEREG for LTE.
05/10/10   kk      Added support for $$GPS* and $SP* commands.
05/10/10   kk      Added support for ^HC SMS commands. 
05/10/10   kk      Added support for ^DSDORMANT.
05/10/10   kk      Added ^SYSINFO, ^HDRCSQ, ^HRSSILVL commands. 
03/15/10   bs      Added +CQI support.
03/04/10   kk      Featurization fixes for multiple commands.
01/20/10   kk      Extended +CFUN to CDMA/MM targets.
01/15/10   ua      Added support for CDMA CLCC command. 
06/10/09   ua      Added support for ^PREFMODE command. 
06/12/09   ua      Added support for $QCBANDPREF command.
12/15/09   nc      Featurisation changes for LTE.
10/12/09   nc      Using memcpy() to copy dsat_incom_call_num from event_ptr.
10/05/09   ua\bs   Fixed +COPS issue.
10/30/09   nc      Returning OK after STOP_CONT_DTMF_CONF from CM for +VTS.
09/22/09   vrk	   Merged LTE changes.
09/09/09   ua      Added support for EONS (spec 22.101).
08/21/09   sa      Fixed Lint Errors.
08/04/09   nc      Added support for *CNTI.
07/15/09   sa      Added support for $CSQ command.
06/11/09   nc      Modified the prototype of dsatcmif_answer_voice_call 
                   to include answer or reject.
07/22/09   nc      Fixed async state after +COPS query followed by an Emergency call.
06/25/09   vg      Fixed +COPS related issue.
06/14/09   nc      Fixed the border case of PDP activation for cid equals 16.
04/20/09   ua      Fixed issues related with call commands followed by +CIND query.
05/20/09   bs      Fixed wrong CREG reporting in flight mode.
04/29/09   ua      Fixed compiler warnings. 
04/23/09   bs      Fixed +CREG issue.
03/11/09   ua      Correcting dsatcmif_is_socket_call function.
01/30/09   ua      Added support of CGEV for DUN calls.
02/16/09   nc      Added support to handle CM_CALL_EVENT_CALL_FORWARDED
01/19/09   bs      Fixed +COPS issue associated with DSAT_CFUN_CARD_POWER_CTL enabled.
01/09/09   nc      Fixing Compilation issue in dsatcmif.c
12/29/08   nc      Added support for +VTS Command.
12/29/08   ua      Processing CM_PH_EVENT_OPRT_MODE in both ETSI and CDMA mode.
12/23/08   sa      Replaced q_init with dsm_queue_init for watermark initialization.
12/17/08   dh      Lint fixes
12/15/08   ua      Register with CM to receive event notifications during 
                   power collapse. 
11/26/08   ss      Off target Lint fix.
10/22/08   bs      Fixed +CHLD UI-ATCOP corner case scenario.
10/13/08   bs      Fixing compilation issues.
09/02/08   sa      Updating RSSI value in SS event info.
07/21/08   bs      Added support for +CREG=2 command.
06/13/08   ua      Removing the changes for reading the NV RTRE control 
                   through CM. 
04/24/08   bs      Added support for power collapse registration for SS events.
02/20/08   sa      Added support for $QCSQ command.
02/05/08   bs      Fixing +COPS initial query command.
01/25/08   bs      Fixing COPS test command behaviour on SMS arrival scenario.
11/24/07   ua      Fixed Lint-lows. 
10/30/07   sa      Fixed initial CFUN query command.
10/12/07   sa      Correcting CREG unsolicited response and COPS issues.
10/09/07   ss      Correcting the behaviour for GSM+1x targets.
10/05/07   ss      Correcting featurization. 
09/21/07   sa      Fixed +CLCC reporting when call is in set-up indication phase.
09/05/07   ua      Correcting featurization. 
08/20/07   ua      Correcting error handling of COPS cmd and handling events
                   for COPS de-registration. 
08/01/07   sa      Fixed inter RAT Handover support for Multi Processor builds.
07/12/07   pp      Fixed Lint high error.
07/02/07   ua      Modifications to have rtre_control update through CM API. 
06/12/07   ss      Fixes for +WS46 read on power on.
04/23/07   pp      Lint Medium fixes.
04/24/07   sa      Correcting ETSI feature wrapping
04/05/07   ua      Added support for COPS=2 (deregistration )
04/09/07   ua      Corrected the error handling of CGATT command.
04/04/07   sa      Correcting COPS abort handling.
03/28/07   sa      Correcting +CHLD for 2x send.
03/23/07   ua      Correcting COPS considering LIMITED SERVICE as not registered.
03/08/07   ua      Handle REGISTRATION_DENIED in CREG/CGREG when SIM state is INVALID.
03/02/07   sa      Added outgoing voice call support when there is an active call  
                   for ATD command.
02/15/07   ss      Fixed lint high errors
02/13/07   pkp     Fixed RVCT compiler warnings.  
01/30/07   ss      Replaced banned string API calls.
11/17/06   sa      Correcting +CFUN command.
11/03/06   snb     Dual processor changes.
10/27/06   ua      Correcting ETSI feature wrapping 
09/26/06   sa      Inter RAT handover support for $QCDGEN.
09/01/06   ua      Correcting COPS Manual registration and CREG URC
07/11/06   ua      CACM/CAMM modifications according to the new CM APIs
06/08/06   ua      Eliminating usage of CM internal function cmph_info_get.
05/22/06   rsl     Add support for at$qcsysmode to return current sysmode, 
				   possible values returned: HSDPA/HSUPA/HSDPA+HSUPA/WCDMA/GSM.
05/12/06   ua      CPOL command is wrapped under FEATURE_MMGSDI				   
04/27/06   snb     Attempt to allocate first large DSM item or on failing that
                   try small item for CM message.
02/28/06   snb     CDMA-only build issue fix.
02/21/06   sa      Merged changes from Sirius Branch.
                   -Removed "dsat_clir_subscription_option" global variable.
01/27/06   snb     Correct indexing into dsatvend_datagen_info
01/16/06   ua      Added support for +CDIP command
12/19/05   snb     Lint corrections.
11/30/05   snb     Add support for +CGCMOD command.
11/11/05   ar      Return immediately to command mode for voice calls.
10/02/05   snb     On +CGATT detach command when already detached still send 
                   user preference to lower layers.
09/13/05   snb     Support for +CFUN=4 LPM mode.
08/29/05   snb     Add SIM card power control to +CFUN.
08/15/05   snb     Add support for reset via +CFUN command.
08/03/05   hap     Modified cmif_call_event_cb_func 
07/29/05   ar      Migrate CM SS events to watermark queue and discard 
                   redundant RSSI notices. 
07/22/05   snb     Register for RAB events and direct them to proper handler.
06/27/05   snb     Add call event stop filter: don't allow manage call 
                   confirmation events when no un-confirmed +CHLD call in 
                   progress.
06/20/05   snb     Don't register for SS or call confirmation events in CDMA 
                   only builds.
05/05/05   iv      Bug fix for +CLCC command
04/29/05   ar      Add support for capturing call end status.
04/22/05   snb     Changes to CM_CALL_EVENT_MNG_CALLS_CONF behavior needed
                   because of changes to CM reporting.
04/12/05   ar      Add support for +COPS access technology selection.
04/06/05   hap     Corrections for +CSSN command
04/05/05   iv      Modified +CLCC to support CS data calls.
04/01/05   tkk     dsat_pdp_deactivation_cause is wrapped under 
                   (WCDMA_PS || GSM_GPRS)
04/01/05   ar      Add support for +CIND service reporting
03/15/05   tkk     Added featurization for ETSI PS data items, corrected detach
                   report handling for +CGEREP and moved queue initialization 
                   routines to ETSI files.
03/03/05   snb     Redo Corrections to +CTZR/+CTZU event handling.
03/02/05   snb     On PS data call events copy nsapi and nsapi validity 
                   indicator to DS command.
02/28/05   hap     Added support for +CSSN command
02/15/05   snb     Fix CDMA-only build issues.
02/03/05   tkk     Added support for +CLIR command.
02/03/05   iv      Added support for +CPOL command.
01/27/05   pdv     Merge from Sirius-ATCoP development.
01/27/05   pdv     Rebase from Mainline.
01/17/05   tkk     Added support for +CGEREP command.
01/10/05   tkk     Added support for +CGPADDR command.
01/06/05   iv      Add support for ^SCNI command.
01/05/05   ar      Suppress duplicate registration domain notifications.
                   Code review action items.
01/05/05   iv      Added support for +CLCC command
12/30/04   tkk     Added CLIP and COLP support.
12/29/04   snb     Allow additional events through call event filter to fix
                   LD entry problem.
12/15/04   snb     Register with CM to receive CM_CALL_EVENT_ORIG so that call
                   originations by UI can be tracked and ended by ATH and 
                   +CHUP commands, correct +CGATT behavior on entry into power
                   save mode from PS_ONLY domain.
12/07/04   ar      Add support for +COPS command.
10/27/04   tkk     "cmif_ss_event_cb_func", "dsatcmif_cm_ss_event_handler" 
                   functions are modified to handle support for timezone 
                   information (for AT+CTZV)
10/15/04   ar      Add Advice of Charge support.
09/13/04   snb     Fix 6500 build warning.
09/03/04   snb     Add +CHLD support.
07/16/04   snb     Correct behavior when compound command requests CM call info
06/21/04   snb     Fix CM call command response when no error and removed
                   ERROR result code for unsupported CM call command.
05/12/04   snb     Fix +CGACT response when deactivating contexts activated via
                   embedded sockets and use local buffer for +CCWA unsolicited
                   response number.
04/15/04   snb     Fix for Jaguar build, using older CM API.
03/19/04   snb     Added support for +CCWA command.
11/07/03   snb     Fix out of order responses due to local formatted response
                   buffers.
10/22/03   ar      Move Call Manager PH & SS API calls from ETSI to COMMON.
10/07/03   ar      Add network registration reporting support
10/10/03   ar      Adjust call event filter to include emergency calls.
09/23/03   snb     Add support for PS attach timeout failure
09/16/03   snb     Added registration for CM_PH_EVENT_PS_ATTACH_FAILED event.
09/05/03   ar      Process only voice or outgoing packet data call events 
                   for ETSI mode.
08/04/03   ar      Removed obsolete dsatcmif_get_cm_call_info()
06/26/03   sb      CM_CALL_CMD_ORIG is now valid in both ETSI and 1X modes.
                   Now the event handler is in common files. Hence the API
                   name has changed from dsatetsicall* to dsatvoice*
03/12/03   ar      Ensure call end event processed by both voice and packet 
                   call handlers.
03/07/03   ar      Adjust CM event reg sequence for better error reporting
                   Remove FEATURE_DATA_ETSI_SUPSERV wrappers
02/26/03   ar      Add support for PDP context activation
02/21/03   ar      Adjust +CPAS no call ID error handling.
02/12/03   wx      Accommodate cm_mm_call_cmd_end api change and 
                   cm_end_params_s_type struct change
01/16/03   ar      Featurized USSD event registration.
11/14/02   jd      Featurized gw_cs_ans under (GSM|WCDMA) for other builds
11/11/02   sb      Added CM Supplementary Services Registration.
11/08/02   sb      Initialize cm_status to CM_CLIENT_OK.
11/07/02   ar      Added FEATURE_GSM_GPRS to FEATURE_DATA_WCDMA_PS wrappers
10/28/02   sb      Created module.


===========================================================================*/


/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"

#include <stringl/stringl.h>


#if defined(T_WINNT)
#error code not present
#endif /* WINNT */

/* this file should be included only in GSM or WCDMA mode */
#ifdef FEATURE_DSAT_ETSI_MODE
#include "dsatetsime.h"
#include "dsatetsictab.h"
#include "dsatetsicmif.h"
#include "dsatetsicall.h"

#include "dstask.h"
#include "queue.h"
#include "stdio.h"

#ifdef FEATURE_DSAT_ETSI_DATA
#include "ds_3gpp_pdn_context.h"
#include "ds_3gpp_bearer_context.h"
#include "dsatetsipkt.h"

/* needed for receiving RAB events and inter RAT HO*/
#include "dsatvend.h"

#include "gsndcp.h"
#endif /* FEATURE_DSAT_ETSI_DATA */

#include "mn_cm_exp.h"

#include "mmgsdilib.h"
#include "mmgsdilib_v.h"

#endif /* FEATURE_DSAT_ETSI_MODE */

#ifdef FEATURE_DATA_IS707
#include "dsat707ext.h"
#if defined(FEATURE_DSAT_SP_CMDS)
#include "dsat707vend.h"
#endif /* defined(FEATURE_DSAT_SP_CMDS) */
#endif /* FEATURE_DATA_IS707  */

#include "dsatme.h"
#include "dsati.h"
#include "dsatact.h"
#include "dsatctab.h"
#include "dsatcmif.h"
#include "dsatvoice.h"
#include "dsatparm.h"
#include "cm.h"
#include "amssassert.h"
#include "err.h"
#include "msg.h"
#include "dsm.h"

#include <stringl/stringl.h>
#include "dsatvend.h"
#include "ds3gmshif.h"
#include "ds3gmgr.h"

/*===========================================================================

            PUBLIC DEFINITIONS AND DECLARATIONS FOR MODULE

  This section contains public definitions for constants, macros, types,
  variables and other items needed by this module.

===========================================================================*/

/* Flag to indicate pending WRITE operation of attach command */
/* TRUE if it is a WRITE and FALSE otherwise                  */ 
cgatt_write_e_type dsatcmif_attach_write = DSAT_CGATT_MAX;

/* Flag for detach writes intended for local stack only:
   This change for GCF test cases allows user preference to be given to 
   local stack only and lets async event processing terminate once CMPH
   event is received as no serving system event is expected.  */
boolean dsatcmif_ss_evt_not_expected = FALSE;
/* +CGEREP variables */
#ifdef FEATURE_ETSI_ATTACH
dsat_num_item_type     dsat_gprs_attach_status[MAX_SUBS];
#endif /* FEATURE_ETSI_ATTACH */
#ifdef FEATURE_DSAT_ETSI_DATA
boolean                dsat_power_off[MAX_SUBS] = {FALSE};
#endif /* FEATURE_DSAT_ETSI_DATA */

#ifdef FEATURE_DSAT_LTE
typedef enum
{
  DSAT_EPS_PS_MODE_2      = 0,  /* UE registers only to EPS ,UE's usage setting is "data centric" */
  DSAT_EPS_CS_PS_MODE_1   = 1,  /* UE registers only to EPS and Non-EPS ,UE's usage setting is "Voice centric" */
  DSAT_EPS_CS_PS_MODE_2   = 2,  /* UE registers only to EPS and Non-EPS ,UE's usage setting is "Data centric" */
  DSAT_EPS_PS_MODE_1      = 3,  /* UE registers only to EPS ,UE's usage setting is "Voice centric" */
  DSAT_EPS_MODE_MAX
} dsatcmif_eps_mode_e_type;
#endif /*FEATURE_DSAT_LTE  */
/*===========================================================================

            LOCAL DEFINITIONS AND DECLARATIONS FOR MODULE

  This section contains local definitions for constants, macros, types,
  variables and other items needed by this module.

===========================================================================*/

/*-------------------------------------------------------------------------
    import definitions:
-------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------
    Local definitions:
-------------------------------------------------------------------------*/
/* CM client (ATCOP) Identifier */
cm_client_id_type dsatcm_client_id;
extern uint8 is_card_action;

/* PREFMODE extern variable */
#ifdef FEATURE_DSAT_ETSI_MODE
/* Holding buffer for CM incoming call event number. */
/* Used in place of DS task cmd buffer to conserve storage and */
/* required for +CCWA unsolicited result code */
extern boolean                   dsatetsime_cind_pending[MAX_ACTIVE_SUBS];
extern boolean                   dsat_colp_pending[MAX_ACTIVE_SUBS];
extern boolean                   dsat_clip_pending[MAX_ACTIVE_SUBS];

#ifdef FEATURE_LTE_TO_1X
static boolean is_csfb_call_active = FALSE;
#endif /* FEATURE_LTE_TO_1X */
/* variable which stores whether listing of preffered PLMN or
   PLMN list stored on ME need to be aborted(TRUE) or not(FALSE) */
extern boolean dsat_abort_plmn_listing;
#endif /* FEATURE_DSAT_ETSI_MODE */
extern dsat_global_state dsat_pending_state[MAX_PORT];
/* Call Manager message watermark queue */
LOCAL dsm_watermark_type  dsat_cm_msg_wm;
LOCAL q_type              dsat_cm_msg_q;
LOCAL boolean             dsat_cm_msg_wm_full;
#ifdef IMAGE_QDSP6_PROC
rex_crit_sect_type dsat_cm_wm_crit_sect;
#endif /* IMAGE_QDSP6_PROC*/
/* Lo Water, Hi Water and Don't Exceed counts for the   */
/* watermark used for select events from Call Manager.   */
#define DSAT_CM_MSG_SIZE            (sizeof(dsat_cm_msg_s_type))
#define DSAT_FROM_CM_LOWATER_CNT          (DSAT_CM_MSG_SIZE * 2)
#define DSAT_FROM_CM_HIWATER_CNT          (DSAT_CM_MSG_SIZE * 9)
#define DSAT_FROM_CM_DONT_EXCEED_CNT      (DSAT_CM_MSG_SIZE * 10)
#define DSAT_RSSI     1
#define DSAT_HDR_RSSI 2

/* Cache of RX signal data */
dsat_num_item_type dsat_rssi_enqueue = 0;
#ifdef IMAGE_QDSP6_PROC
rex_crit_sect_type dsat_rssi_crit_sect;
#endif /* IMAGE_QDSP6_PROC*/
#ifdef FEATURE_TDSCDMA
#define AT_MODE_TDS 4
#endif /* FEATURE_TDSCDMA */
#define AT_MODE_NONE 0

#if defined(FEATURE_8960_SGLTE_FUSION) || defined(FEATURE_8960_DSDA_FUSION)  
#define AT_MAX_ALLOWED_SLOT 1
#elif defined(FEATURE_TRIPLE_SIM)
#define AT_MAX_ALLOWED_SLOT 3
#elif defined(FEATURE_DUAL_SIM)
#define AT_MAX_ALLOWED_SLOT 2
#else
#define AT_MAX_ALLOWED_SLOT 1
#endif /* defined(FEATURE_8960_SGLTE_FUSION) || defined(FEATURE_8960_DSDA_FUSION) */

#define DSAT_ASUBS_MASK 7
const LOCAL dsatcmif_ss_mask_map_s_type dsatcmif_ss_subs_mask_map[] =
{
#ifdef FEATURE_LTE_TO_1X
  { CM_SS_EVT_CSFB_CALL_ACTIVE_MASK  , DSAT_SS_CSFB_CALL_ACTIVE_MASK },
#endif /* FEATURE_LTE_TO_1X */
  { CM_SS_EVT_PS_DATA_SUSPEND_MASK  , DSAT_SS_PS_DATA_SUSPEND_MASK }
};

/*Change field 1 mapping*/
const LOCAL dsatcmif_ss_mask_map_s_type dsatcmif_ss_stack_mask_map[] =
{
  { CM_SS_EVT_SRV_STATUS_MASK       , DSAT_SS_SRV_STATUS_MASK       },
  { CM_SS_EVT_SRV_DOMAIN_MASK       , DSAT_SS_SRV_DOMAIN_MASK       },
  { CM_SS_EVT_SYS_MODE_MASK         , DSAT_SS_SYS_MODE_MASK         },
  { CM_SS_EVT_SYS_ID_MASK           , DSAT_SS_SYS_ID_MASK           },
  { CM_SS_EVT_SRV_IND_MASK          , DSAT_SS_SRV_IND_MASK          },
  { CM_SS_EVT_MOBILITY_MGMT_MASK    , DSAT_SS_MOBILITY_MGMT_MASK    },
  { CM_SS_EVT_SIM_STATE_MASK        , DSAT_SS_SIM_STATE_MASK        },
  { CM_SS_EVT_SRV_STATUS_MASK       , DSAT_SS_HDR_SRV_STATUS_MASK   },
#ifdef FEATURE_GSM_RR_CELL_CALLBACK
  { CM_SS_EVT_CELL_INFO_MASK        , DSAT_SS_CELL_INFO_MASK        },
#endif /* FEATURE_GSM_RR_CELL_CALLBACK */
  { CM_SS_LAC_TAC_RAC_CHGD_MASK     , DSAT_SS_LAC_TAC_RAC_CHGD_MASK },
  { CM_SS_EVT_ROAM_STATUS_MASK      , DSAT_SS_ROAM_STATUS_MASK      }, 
  { CM_SS_EVT_SYS_FORBIDDEN_MASK    , DSAT_SS_SYS_FORBIDDEN_MASK    }, 
  { CM_SS_EVT_SRV_CAPABILITY_MASK   , DSAT_SS_SRV_CAPABILITY_MASK   },
  { CM_SS_EVT_CSG_INFO_CHANGED_MASK , DSAT_SS_CSG_INFO_CHANGED_MASK } 
  /*{ CM_SS_EVT_CQI_INFO_MASK         , DSAT_SS_CQI_INFO_MASK },
  { CM_SS_EVT_CELL_SRV_IND_MASK     , DSAT_SS_CELL_SRV_IND_MASK      },*/  
};

/*Change field 2 mapping - TBD*/

/*Place holder for Second stack information in SGLTE*/
ds_subs_e_type dsat_hybr_subs_id = DS_ALL_SUBS - 1;

/*-------------------------------------------------------------------------
    Local functions:
-------------------------------------------------------------------------*/


LOCAL void cmif_call_event_cb_func 
(
  cm_call_event_e_type            event,            /* Event ID              */
  const cm_mm_call_info_s_type   *event_ptr         /* Pointer to Event info */
);

LOCAL void cmif_call_info_list_cb_func 
(
  const cm_call_state_info_list_s_type *list_ptr  /* Pointer to info list */
);

LOCAL void cmif_ss_event_msim_cb_func 
(
  cm_ss_event_e_type            event,                  /* Event ID              */
  const cm_mm_msim_ss_info_s_type   *event_ptr         /* Pointer to Event info */
);

LOCAL void cmif_format_sysinfo_response
(
  dsat_cm_msg_s_type    * msg_ptr
);

LOCAL dsat_result_enum_type dsatcmif_cm_ss_event_handler
(
  dsat_cm_msg_s_type    * msg_ptr
);

LOCAL void cmif_ss_cmd_cb_func 
(
  void                         *data_ptr,         /* Data block pointer    */
  cm_ss_cmd_e_type              cmd,              /* Command ID            */
  cm_ss_cmd_err_e_type          cmd_err           /* Command error code    */
);

LOCAL void cmif_cm_ph_event_cb_func 
(
  cm_ph_event_e_type         event,            /* Event ID              */
  const cm_ph_info_s_type   *event_ptr         /* Pointer to Event info */
);

LOCAL boolean cmif_is_gw_subscription_available
(
  ds_subs_e_type             subs_info,
  ds_at_cm_ph_event_type    *ph_event
);

LOCAL dsat_result_enum_type cmif_process_cm_reg_data
(
  const ds_at_cm_ss_event_type * event_ptr,        /* DS event pointer */
  dsat_stack_id_e_type           stack_id,
  ds_subs_e_type                 subs_info
);

#ifdef FEATURE_SGLTE
LOCAL void cmif_process_cm_hybr_reg_data
(
  const ds_at_cm_ss_event_type *event_ptr,        /* DS event pointer */
  dsat_stack_id_e_type          stack_id,
  ds_subs_e_type                subs_info
);
#endif /* FEATURE_SGLTE */

LOCAL void dsat_set_net_sel_mode_from_nv
(
  ds_subs_e_type                 subs_info
);

LOCAL void cmif_process_emerg_list
(
  const ds_at_ss_info_s_type   * event_info_ptr,        /* DS event  info pointer */
  ds_subs_e_type                 subs_info
);

LOCAL void cmif_report_mode
(
  sys_srv_status_e_type     srv_status,
  sys_srv_domain_e_type     srv_domain,
  sys_sys_mode_e_type       sys_mode,
  ds_subs_e_type            subs_info
);

#ifdef FEATURE_DSAT_EXTENDED_CMD
LOCAL void cmif_report_mode_3gpp2
(
  ds_at_ss_info_s_type *ss_info_ptr
);
#endif /* FEATURE_DSAT_EXTENDED_CMD */

#if defined(FEATURE_DSAT_SP_CMDS)
LOCAL void cmif_determine_spservice
(
  ds_at_ss_info_s_type *ss_info_ptr
);
#endif /* defined(FEATURE_DSAT_SP_CMDS) */

void cmif_process_rssi_info_3gpp
(
  const ds_at_cm_ss_event_type *event_ptr, /* DS event pointer */
  dsat_stack_id_e_type          stack_id,
  ds_subs_e_type                subs_info
);

LOCAL void cmif_process_rssi_info_3gpp2
(
  ds_at_ss_info_s_type *ss_info_ptr
);

#ifdef FEATURE_DSAT_ETSI_MODE
#ifdef FEATURE_WCDMA_L1_HS_CQI_STAT
void cmif_process_cqi_info
(
  ds_at_ss_info_s_type *ss_info_ptr
);
#endif /* FEATURE_WCDMA_L1_HS_CQI_STAT  */
#endif /* FEATURE_DSAT_ETSI_MODE */

LOCAL void cmif_setup_msg_watermarks( void );
LOCAL void cmif_register_cb_func ( void );

#ifdef FEATURE_DSM_WM_CB
LOCAL void cmif_cm_msg_wm_hiwater_cb 
(
  struct dsm_watermark_type_s * wm_ptr, 
  void * cb_ptr
);
LOCAL void cmif_cm_msg_wm_lowater_cb 
(
  struct dsm_watermark_type_s * wm_ptr, 
  void * cb_ptr
);
LOCAL void cmif_cm_msg_wm_non_empty_cb
(
  struct dsm_watermark_type_s * wm_ptr, 
  void * cb_ptr
);
#else
LOCAL void cmif_cm_msg_wm_hiwater_cb ( void );
LOCAL void cmif_cm_msg_wm_lowater_cb ( void );
LOCAL void cmif_cm_msg_wm_non_empty_cb ( void );
#endif /* FEATURE_DSM_WM_CB */

#if defined(FEATURE_HS_USB_SER3_PORT)
LOCAL dsat_result_enum_type cmif_generate_dormant_ind(void);
#endif /* defined(FEATURE_HS_USB_SER3_PORT) */

LOCAL void cmif_start_s_seven_timer
( 
  ds_subs_e_type        subs_id,
  ds_profile_num_type   profile_id,
  cm_call_type_e_type   call_type,
  cm_call_id_type       call_id,
  cm_client_type_e_type client_id
);
LOCAL void cmif_stop_s_seven_timer
( 
  ds_subs_e_type        subs_id,
  ds_profile_num_type   profile_id,
  cm_call_type_e_type   call_type,
  cm_call_id_type       call_id,
  cm_client_type_e_type client_id
);
#ifdef FEATURE_DSAT_ETSI_MODE
LOCAL void cmif_report_dsci_result
(
  ds_subs_e_type            subs_id,
  ds_at_cm_call_event_type *call_event
);
#endif /* FEATURE_DSAT_ETSI_MODE */

LOCAL void cmif_cm_dual_standby_sub_info_cmd_handler
(
  mmgsdi_session_type_enum_type  session_type,
  ds3g_cm_sub_info_type         *pref_event_ptr
);

/*===========================================================================
FUNCTION  DSATCMIF_CALL_CMD_CB_FUNC

DESCRIPTION
  CM Call status command callback function

DEPENDENCIES
  None

RETURNS
  None

SIDE EFFECTS
  Adds command in DS command buffer

===========================================================================*/
void dsatcmif_call_cmd_cb_func 
(
  void                         *data_ptr,         /* Data block pointer    */
  cm_call_cmd_e_type            cmd,              /* Command ID            */
  cm_call_cmd_err_e_type        cmd_err           /* Command error code    */
)
{
  ds_cmd_type            *cmd_buf;
  ds_at_cm_call_cmd_type *call_cmd = NULL;

  cmd_buf = dsat_get_cmd_buf(FALSE);
  /* send the message to ATCOP */
  cmd_buf->hdr.cmd_id = DS_AT_CM_CALL_CMD;
  call_cmd = (ds_at_cm_call_cmd_type *)dsat_alloc_memory(
                                         sizeof(ds_at_cm_call_cmd_type), FALSE);
  call_cmd->cmd = cmd;
  call_cmd->cmd_err = cmd_err;
  call_cmd->data_ptr = data_ptr;
  cmd_buf->cmd.cm_cmd.sub_cmd = call_cmd;
  ds_put_cmd(cmd_buf);
}  /* dsatcmif_call_cmd_cb_func */

/*===========================================================================
FUNCTION  DSATCMIF_CALL_CMD_CB_EXTEN_FUNC

DESCRIPTION
  CM Call status command callback function

DEPENDENCIES
  None

RETURNS
  None

SIDE EFFECTS
  Adds command in DS command buffer

=========================================================================== */
void dsatcmif_call_cmd_cb_exten_func 
(
  void                            *data_ptr,          /* Data block pointer    */
  cm_call_cmd_e_type               cmd,               /* Command ID            */
  cm_call_cmd_err_e_type           cmd_err,           /* Command error code    */
  cm_call_cmd_err_cause_e_type     call_cmd_err_cause,/* Cause of the error */
  cm_alpha_s_type                  alpha              /* Alpha value */
)
{
  ds_cmd_type            *cmd_buf;
  ds_at_cm_call_cmd_type *call_cmd = NULL;

  cmd_buf = dsat_get_cmd_buf(FALSE);
  /* send the message to ATCOP */
  cmd_buf->hdr.cmd_id = DS_AT_CM_CALL_CMD;
  call_cmd = (ds_at_cm_call_cmd_type *)dsat_alloc_memory(sizeof(ds_at_cm_call_cmd_type), FALSE);
  call_cmd->cmd = cmd;
  call_cmd->cmd_err = cmd_err;
  call_cmd->data_ptr = data_ptr;
/* As of now we are not using err_cause and alpha fields and it can be used for
extracting Cause of Error .Cause of Error to be used when cmd_err is other than CM_SUPS_CMD_ERR_NOERR */
  call_cmd->err_cause = (cm_sups_cmd_err_cause_e_type)call_cmd_err_cause;
  call_cmd->alpha.len = alpha.len;
  if(alpha.len > 0)
  {
  (void) dsatutil_memscpy((void*)&call_cmd->alpha.buf[0],
          CM_MAX_ALPHA_TAG_CHARS,(void*)&alpha.buf[0],MIN(alpha.len,CM_MAX_ALPHA_TAG_CHARS));     
  }
  cmd_buf->cmd.cm_cmd.sub_cmd = call_cmd;
  ds_put_cmd(cmd_buf);
} /* dsatcmif_call_cmd_cb_exten_func  */

/*===========================================================================

FUNCTION DSATCMIF_END_CALLS

DESCRIPTION
  This function issues a request to CM to end call(s).
  Num_call_ids should specify thee size of the call id array pointed to by
  call_id_ptr.

DEPENDENCIES

RETURN VALUE
  TRUE:     The end request is sent, callback function
              will be called.
  FALSE:    The end request failed.

SIDE EFFECTS
  None
  
===========================================================================*/
/* ARGSUSED */
boolean dsatcmif_end_calls( 
  const uint8 num_call_ids, 
  const cm_call_id_type * call_id_ptr 
)
{
  /* Hangup the call */
  boolean                 call_status;
  cm_end_params_s_type    end_params[CM_CALL_ID_MAX];
  dsati_mode_e_type       mode;
  uint8 index;
  dsat_num_item_type        subs_id = dsat_get_qcsimapp_val();
  cm_call_mode_info_e_type  info_type = CM_CALL_MODE_INFO_GW_CS;

  ASSERT( call_id_ptr != NULL );

  mode = dsatcmdp_get_current_mode();

  if ( num_call_ids > CM_CALL_ID_MAX )
  {
    return FALSE;
  }
  memset((void*)end_params,0,sizeof(end_params));

  for ( index = 0; (index < num_call_ids && index < CM_CALL_ID_MAX); index++ )
  {
    memset( &end_params[index],
            CM_CALL_CMD_PARAM_DEFAULT_VALUE,
            sizeof ( cm_end_params_s_type ));
  
    info_type = voice_state[subs_id][call_id_ptr[index]].call_mode;

    end_params[index].info_type = info_type;


    if(info_type != CM_CALL_MODE_INFO_IP)
    {
      if (IS_VOICE_IN_CDMA_MODE(mode))
      {
        end_params[index].info_type = CM_CALL_MODE_INFO_CDMA;
      }
#ifdef FEATURE_LTE_TO_1X
      if(is_csfb_call_active == TRUE)
      {
        end_params[index].info_type = CM_CALL_MODE_INFO_CDMA;
      }
#endif /* FEATURE_LTE_TO_1X */
    }
    end_params[index].call_id = call_id_ptr[index];
  }

  call_status = cm_mm_call_cmd_end(dsatcmif_call_cmd_cb_func,
                                   NULL,
                                   dsatcm_client_id,
                                   num_call_ids,
                                   end_params);

  return call_status;
} /* dsatcmif_end_calls */

/*===========================================================================

FUNCTION DSATCMIF_ANSWER_VOICE_CALL

DESCRIPTION
  This command answer a voice call by calling cm_call_cmd_answer

DEPENDENCIES

RETURN VALUE
  TRUE : If the command succeeds.
  FALSE: If the command fails.

SIDE EFFECTS
  None
  
===========================================================================*/
boolean dsatcmif_answer_voice_call
(
  ds_subs_e_type subs_id,
  uint8 call_id_ptr,
  boolean answer_call,
  cm_call_mode_info_e_type call_mode
)
{
  boolean                 call_status;
  cm_ans_params_s_type    answer_params;

  if ((subs_id <= DS_SUBS_NONE) || (subs_id >= DS_ALL_SUBS))
  {
    return FALSE;
  }

  memset( (void *)&answer_params, CM_CALL_CMD_PARAM_DEFAULT_VALUE, sizeof( cm_ans_params_s_type));
  
if( call_mode == CM_CALL_MODE_INFO_IP )
  {
    answer_params.info_type = CM_CALL_MODE_INFO_IP;
    answer_params.ans_params.ip_ans.accept = answer_call;
    answer_params.ans_params.ip_ans.call_reject_reason = answer_call ? 
                                                         CM_CALL_END_NONE : 
                                                         CM_CALL_END_SIP_486_BUSY_HERE;
    answer_params.ans_params.ip_ans.call_type = CM_CALL_TYPE_VOICE;
  }
  else
  {
    dsati_mode_e_type       mode;
    mode = dsatcmdp_get_current_mode_per_subs(dsat_qcsimapp_info[subs_id].subs_id);

    answer_params.info_type = CM_CALL_MODE_INFO_GW_CS;
    answer_params.ans_params.gw_cs_ans.accept = answer_call;
    answer_params.ans_params.gw_cs_ans.call_type = CM_CALL_TYPE_VOICE;

    if (IS_VOICE_IN_CDMA_MODE(mode))
    {
      answer_params.info_type = CM_CALL_MODE_INFO_CDMA;
      answer_params.ans_params.cdma_ans.call_type = CM_CALL_TYPE_VOICE;
    }
  }

  call_status = cm_mm_call_cmd_answer(dsatcmif_call_cmd_cb_func,
                                   NULL,
                                   dsatcm_client_id,
                                   call_id_ptr,
                                   &answer_params
                                   );
  return call_status;
} /* dsatcmif_answer_voice_call */
/*===========================================================================

FUNCTION  DSAT_UPDATE_MODE_INFO

DESCRIPTION
  This function update mode info for the CM call-related 
  events.

DEPENDENCIES
  None

RETURN VALUE
  None.

SIDE EFFECTS
  None

===========================================================================*/

void dsat_update_mode_info
(
  dsat_cm_call_mode_info_s_type        *dsat_mode_info, /*Destination*/
  cm_call_mode_info_s_type             *cm_mode_info   /*Source*/
)
{
 /*Update connect info : Mode Info(CDMA CALL & GW CS CALL only)*/
  dsat_mode_info->cdma_call.srv_opt = 
             cm_mode_info->info.cdma_call.srv_opt;
  /* MODE INFO : GW CS INFO*/
  dsat_mode_info->gw_cs_call.sups_type =
             cm_mode_info->info.gw_cs_call.sups_type;
  dsat_mode_info->gw_cs_call.call_ss_success =
             cm_mode_info->info.gw_cs_call.call_ss_success;
  (void) dsatutil_memscpy((void*)&dsat_mode_info->gw_cs_call.bearer_capability_1,
                sizeof(cm_bearer_capability_s_type),
                (void*)&cm_mode_info->info.gw_cs_call.bearer_capability_1,
                sizeof(cm_bearer_capability_s_type));
  (void) dsatutil_memscpy((void*)&dsat_mode_info->gw_cs_call.active_calls_list,
                sizeof(active_calls_list_s_type),
                (void*)&cm_mode_info->info.gw_cs_call.active_calls_list,
                sizeof(active_calls_list_s_type) );
  (void) dsatutil_memscpy((void*)&dsat_mode_info->gw_cs_call.calling_party_bcd_number,
                sizeof(cm_calling_party_bcd_no_s_type),
                (void*)&cm_mode_info->info.gw_cs_call.calling_party_bcd_number,
                sizeof(cm_calling_party_bcd_no_s_type) );
  (void) dsatutil_memscpy((void*)&dsat_mode_info->gw_cs_call.calling_party_subaddress,
                sizeof(cm_calling_party_subaddress_s_type),
                (void*)&cm_mode_info->info.gw_cs_call.calling_party_subaddress,
                sizeof(cm_calling_party_subaddress_s_type) );
  (void) dsatutil_memscpy((void*)&dsat_mode_info->gw_cs_call.called_party_subaddress,
                sizeof(cm_called_party_subaddress_s_type),
                (void*)&cm_mode_info->info.gw_cs_call.called_party_subaddress,
                sizeof(cm_called_party_subaddress_s_type) );
  (void) dsatutil_memscpy((void*)&dsat_mode_info->gw_cs_call.connected_subaddress,
                sizeof(cm_connected_subaddress_s_type),
                (void*)&cm_mode_info->info.gw_cs_call.connected_subaddress,
                 sizeof(cm_connected_subaddress_s_type) );
  (void) dsatutil_memscpy((void*)&dsat_mode_info->gw_cs_call.called_party_bcd_number,
                sizeof(cm_called_party_bcd_no_s_type),
                (void*)&cm_mode_info->info.gw_cs_call.called_party_bcd_number,
                sizeof(cm_called_party_bcd_no_s_type) );
  (void) dsatutil_memscpy((void*)&dsat_mode_info->gw_cs_call.cause_of_no_cli,
                sizeof(cm_cause_of_no_cli_s_type),
                (void*)&cm_mode_info->info.gw_cs_call.cause_of_no_cli,
                sizeof(cm_cause_of_no_cli_s_type) );
  (void) dsatutil_memscpy((void*)&dsat_mode_info->gw_cs_call.cc_cause,
                sizeof(ie_cm_cc_cause_s_type),
                (void*)&cm_mode_info->info.gw_cs_call.cc_cause,
                sizeof(ie_cm_cc_cause_s_type) );
  (void) dsatutil_memscpy((void*)&dsat_mode_info->gw_cs_call.cc_reject,
                sizeof(ie_cm_cc_reject_s_type),
                (void*)&cm_mode_info->info.gw_cs_call.cc_reject,
                sizeof(ie_cm_cc_reject_s_type) );
}
/*===========================================================================

FUNCTION  DSAT_UPDATE_CALL_INFO_GLOBAL

DESCRIPTION
  This function update call related global variables for the CM call-related 
  events.

DEPENDENCIES
  None

RETURN VALUE
  None.

SIDE EFFECTS
  None

===========================================================================*/

void dsat_update_call_info_global
(
  ds_cmd_type         * cmd_ptr,  /* DS Command pointer */
  ds_subs_e_type        subs_id
)
{
  dsati_mode_e_type current_mode;
  cm_call_event_e_type      event;
  ds_at_call_info_s_type   *event_ptr =NULL;
  ds_at_cm_call_event_type*  call_event = NULL;
  dsatetsicall_call_state_da_info    *call_da_val = NULL;
  

  call_event = (ds_at_cm_call_event_type*)cmd_ptr->cmd.cm_cmd.sub_cmd;
  ASSERT(call_event != NULL);
  event_ptr = &call_event->event_info;
  event     =  call_event->event;     
  current_mode = dsatcmdp_get_current_mode_per_subs(cmd_ptr->cmd.cm_cmd.subs);
  if(DSAT_FAILURE == dsatutil_get_base_addr(DSAT_CALL_DA_VALS,
                                             (void **)&call_da_val,subs_id))
  {
     return;
  }

#ifdef FEATURE_DSAT_ETSI_MODE
  if( event == CM_CALL_EVENT_SETUP_IND )
#else
  if( event == CM_CALL_EVENT_CALLER_ID)
#endif /* FEATURE_DSAT_ETSI_MODE */
  {
    /*back up current incoming call info number*/
    if(event_ptr->dsat_num.len != 0)
    {
      (void) dsatutil_memscpy((void*)&call_da_val->dsat_cm_curr_incoming_call_num,
              sizeof(cm_num_s_type),(void*)&event_ptr->dsat_num,sizeof(cm_num_s_type));     
    }
  }
  else if ( event == CM_CALL_EVENT_END )
  {
    /*reset the call info number when call ended*/
    memset(&call_da_val->dsat_cm_curr_incoming_call_num, 0,sizeof(cm_num_s_type));
  }
  if ( dsatcmif_is_voice_call_type(event_ptr->call_type)
      /* outgoing packet data calls(ETSI only), */
#ifdef FEATURE_DSAT_ETSI_DATA
      || ((CM_CALL_TYPE_PS_DATA == event_ptr->call_type) &&
          (CM_CALL_STATE_INCOM != event_ptr->call_state))
#endif /* FEATURE_DSAT_ETSI_DATA */

#ifdef FEATURE_DSAT_ETSI_MODE
       /* Incoming, connected or end events for CS data calls */
      || (CM_CALL_TYPE_CS_DATA == event_ptr->call_type &&
         (CM_CALL_STATE_INCOM == event_ptr->call_state ||
          CM_CALL_STATE_CONV  == event_ptr->call_state ||
          CM_CALL_STATE_IDLE  == event_ptr->call_state ) )

       /* CM_CALL_EVENT_MNG_CALLS_CONF
          For ATCOP originated events: 
            Need to check success and update state or give CME ERROR.
          For other client:
            Need to update held calls info as no other events are posted. */
      || event == CM_CALL_EVENT_MNG_CALLS_CONF
      || event == CM_CALL_EVENT_PROGRESS_INFO_IND
      || event == CM_CALL_EVENT_CALL_IS_WAITING
      || event == CM_CALL_EVENT_CALL_BARRED
      || event == CM_CALL_EVENT_CALL_FORWARDED
      || event == CM_CALL_EVENT_CALL_BEING_FORWARDED
      || event == CM_CALL_EVENT_INCOM_FWD_CALL
      || event == CM_CALL_EVENT_CALL_ON_HOLD 
      || event == CM_CALL_EVENT_CALL_RETRIEVED
      || event == CM_CALL_EVENT_ORIG_FWD_STATUS
      || event == CM_CALL_EVENT_ORIG
      || event == CM_CALL_EVENT_ANSWER
#endif /* FEATURE_DSAT_ETSI_MODE */
      || event == CM_CALL_EVENT_INFO
#ifdef FEATURE_DATA_WCDMA_PS
      /* RAB events to support WCDMA-mode $QCDGEN */
      || event == CM_CALL_EVENT_RAB_REESTAB_IND 
#endif /* FEATURE_DATA_WCDMA_PS */
#if defined(FEATURE_HS_USB_SER3_PORT)
      || (CM_CALL_EVENT_CONNECT == event && 
          CM_CALL_TYPE_PS_DATA == event_ptr->call_type)
#endif /* defined(FEATURE_HS_USB_SER3_PORT) */
     )
  {
    DS_AT_MSG3_HIGH("ATCOP: cm event cb, event: %d, type: %d, state: %d",
            event, event_ptr->call_type, event_ptr->call_state);
    if( NULL != event_ptr )
    {
      if(CM_CALL_EVENT_INFO == event)
      {
        /* Update mode info */
         dsat_update_mode_info(&call_da_val->dsat_cm_call_info.mode_info,&event_ptr->dsat_mode_info);
        
        (void) dsatutil_memscpy((void*)&call_da_val->dsat_cm_call_info.num,
                sizeof(cm_num_s_type),(void*)&event_ptr->dsat_num,sizeof(cm_num_s_type));
        call_da_val->dsat_cm_call_info.call_id = event_ptr->call_id;
        call_da_val->dsat_cm_call_info.direction = event_ptr->call_direction;
        call_da_val->dsat_cm_call_info.call_state = event_ptr->call_state;
        call_da_val->dsat_cm_call_info.call_type = event_ptr->call_type;
        call_da_val->dsat_cm_call_info.sys_mode = event_ptr->sys_mode;
      }
#ifdef FEATURE_DSAT_ETSI_MODE
      if ( IS_ETSI_MODE(current_mode) )
      {
        if(CM_CALL_EVENT_SETUP_IND == event || 
           CM_CALL_EVENT_CONNECT == event || 
           CM_CALL_EVENT_END == event)
        {
          /*Update Mode Info*/
          dsat_update_mode_info(&call_da_val->dsat_call_mode_info,&event_ptr->dsat_mode_info);
#ifdef FEATURE_DSAT_ETSI_DATA
          if(CM_CALL_EVENT_END == event)
          {
            call_da_val->dsat_pdp_deactivation_cause = event_ptr->end_status;
          }
#endif /* FEATURE_DSAT_ETSI_DATA */
        }
        if(CM_CALL_EVENT_INFO == event)
        {
            
          /*---- Updating  connect info ----*/
          /*Update Mode Info*/
          dsat_update_mode_info(&call_da_val->dsat_connect_info.mode_info,&event_ptr->dsat_mode_info);
          /*Update connect info : NUM Info*/
          (void) dsatutil_memscpy((void*)&call_da_val->dsat_connect_info.num,
                          sizeof(cm_num_s_type),
                          (void*)&event_ptr->dsat_num,
                   sizeof(cm_num_s_type) );
          
          /*------Updating CM call Info--------*/
          /*Update Mode Info*/
          dsat_update_mode_info(&call_da_val->dsat_cm_call_info.mode_info,&event_ptr->dsat_mode_info);
          /*Update NUM info*/
          (void) dsatutil_memscpy((void*)&call_da_val->dsat_cm_call_info.num,
                        sizeof(cm_num_s_type),
                        (void*)&event_ptr->dsat_num,
                     sizeof(cm_num_s_type) );
          /*Update other call details*/
          call_da_val->dsat_cm_call_info.call_id = event_ptr->call_id;
          call_da_val->dsat_cm_call_info.direction = event_ptr->call_direction;
          call_da_val->dsat_cm_call_info.call_state = event_ptr->call_state;
          call_da_val->dsat_cm_call_info.call_type = event_ptr->call_type;
          
        }
        if (CM_CALL_STATE_INCOM == event_ptr->call_state)
        {
          (void) dsatutil_memscpy((void*)&call_da_val->dsat_incom_call_num,
                        sizeof(cm_num_s_type),
                        (void*)&event_ptr->dsat_num,
                        sizeof(cm_num_s_type));         
        }
      }
#endif /* FEATURE_DSAT_ETSI_MODE */
    }
  }
  #ifdef FEATURE_DSAT_ETSI_MODE
  /* Capture call end status */
  if( IS_ETSI_MODE(current_mode) &&
      (
	(CM_CALL_EVENT_END == event) ||
        (CM_CALL_EVENT_CALL_CONF == event) ||
        (CM_CALL_EVENT_PDN_CONN_REJ_IND == event) ||
        (CM_CALL_EVENT_PDN_CONN_FAIL_IND == event)
      ) 
    )
  {
    dsatetsicmif_capture_call_end_status( subs_id, event, event_ptr );
  }
#endif /* FEATURE_DSAT_ETSI_MODE */
}/* dsat_update_call_info_global */

#ifdef FEATURE_DSAT_ETSI_MODE
/*===========================================================================

FUNCTION  CMIF_REPORT_DSCI_RESULT

DESCRIPTION
^DSCI: <id>,<idr>,<stat>,<type>,<mpty>,<number>,<num_type>,[<bs_type>][,cause].

<id>: Integer value, 1-7, Call identifier, will be used by +CHLD command.
<dir>:The initiator of the call
    	0 	the call initiated by the user
	1	the call ended by the user
<stat>:Call status
    	0  	Activating
	1	Holding
	2	Dialing (MO call)
	3	Ringing (MO call)
	4	Incoming call establishing (MT call)
	5	waiting
    6   Call ended
<type>:Call type
 	0   voice service
    1   data service
<mpty>:Whether the call belongs to the multi-user talk
   	0   the call does not belong to the multi-user talk
	1	the call belongs to the multi-user talk
<number> 	Call address number, its format is appointed by <type>
<num_type>     	Address type (refer to Section 10.5.4.7 of TS 24.008)
<bs_type>: When <type> is not the voice service, this field is effective, and indicates the specific type of the data service. 
0,ASYNC  Asynchronous and transparent
1,SYNC   Synchronous and transparent
2,REL ASYNC Asynchronous and non-transparent
3, REL SYNC   Synchronous and non-transparent
<casue> : the reason value of call hangup, referring to Section 10.5.4.11 of 3GPP TS 24.008.



DEPENDENCIES
  None

RETURN VALUE

SIDE EFFECTS
  None

===========================================================================*/
LOCAL void cmif_report_dsci_result
(
  ds_subs_e_type            subs_id,
  ds_at_cm_call_event_type *call_event
)
{
#define CALL_HOLD 1
#define CALL_ORIG 2
#define CALL_CONNECT 3
#define CALL_INCOM 4
#define CALL_WAITING 5
#define CALL_END 6

  dsm_item_type *res_buff = NULL;
  byte * data_ptr;
  dsat_num_item_type stat = 0;
  uint8 i;
  cm_call_id_type           temp_call_id;
  /*                                                                              */

  ASSERT( call_event != NULL )
  if((call_event->event_info.call_type != CM_CALL_TYPE_VOICE ) &&
     (call_event->event_info.call_type != CM_CALL_TYPE_CS_DATA ) && 
     (call_event->event_info.call_type != CM_CALL_TYPE_PS_DATA ))
  {
    return;
  }

  res_buff = dsat_dsm_new_buffer(DSM_DS_SMALL_ITEM_POOL, TRUE);
  if (NULL != res_buff)
  {
    res_buff->used = 0;
  
    data_ptr = res_buff->data_ptr;
    switch (call_event->event)
    {
      case CM_CALL_EVENT_CALL_ON_HOLD:
        stat = CALL_HOLD;
        break;
      case CM_CALL_EVENT_ORIG:
        stat = CALL_ORIG;
        break;
      case CM_CALL_EVENT_CONNECT:
        stat = CALL_CONNECT;
        break;
      case CM_CALL_EVENT_INCOM:
        stat = CALL_INCOM;
        break;
      case CM_CALL_EVENT_CALL_IS_WAITING:
        stat = CALL_WAITING;
        break;
      case CM_CALL_EVENT_END:
        stat = CALL_END;
        break;
      default:
        break;
    }
    if(stat == 0)
    {
      DS_AT_MSG0_LOW("Ignoring the URC");
      dsm_free_buffer(res_buff);
      return;
    }
    for( i = 0; i< CM_CALL_ID_MAX ;i++)
    {
      temp_call_id = dsatetsicall_seqnum_to_callid(subs_id, i);
      if(temp_call_id == call_event->event_info.call_id )
      {
        break;
      }
      
    }
    if( i != CM_CALL_ID_MAX )
    {
    /* ^DSCI: <id>,<idr>,<stat>,<type>,<mpty>,<number>,<num_type>,[<bs_type>][,cause]. */
  
    /* <mpty>,[bs_type] and [cause] TBD */
  
      res_buff->used = (word)snprintf ((char*)data_ptr,
                                               res_buff->size,
                                               "^DSCI: %d,%d,%d,%d,"
                                               ,i,((call_event->event_info.call_direction == CM_CALL_DIRECTION_MO )? 0:1),
                                               stat,((call_event->event_info.call_type == CM_CALL_TYPE_VOICE )? 0:1)  );
  
      (void) dsatutil_memscpy((void*)&res_buff->data_ptr[res_buff->used],
              DSAT_BUF_SIZE(res_buff->size,res_buff->used),
              (void*)call_event->event_info.dsat_num.buf,
              MIN(CM_MAX_NUMBER_CHARS,call_event->event_info.dsat_num.len));   
      res_buff->used += call_event->event_info.dsat_num.len;
  
      data_ptr = &res_buff->data_ptr[res_buff->used];
  
      res_buff->used += (word)snprintf ((char*)data_ptr,
                                               (res_buff->size - res_buff->used),
                                               ",%d",call_event->event_info.dsat_num.number_type);
    }
    if (res_buff->used != 0)
    {
      /* Send response to TE */
      dsatcmdp_send_urc( subs_id, res_buff, DSAT_COMPLETE_RSP );
    }
    else
    {
      (void )dsm_free_buffer(res_buff);
    }
  }
}/* cmif_report_dsci_result */
#endif /* FEATURE_DSAT_ETSI_MODE */
/*===========================================================================

FUNCTION  DSATCMIF_CM_CALL_EVENT_HANDLER

DESCRIPTION
  This function is the handler function for the CM call-related events.

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR :       if there was any problem in executing the command
    DSAT_ASYNC_EVENT : if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
/* ARGSUSED */
dsat_result_enum_type dsatcmif_cm_call_event_handler
(
  dsat_mode_enum_type   mode,                /* AT command mode            */
  ds_cmd_type         * cmd_ptr              /* DS Command pointer         */
)
{
  dsat_result_enum_type result = DSAT_ASYNC_EVENT;
  dsati_mode_e_type current_mode;
  ds_at_cm_call_event_type*  call_event = NULL;
  dsatetsicall_call_state_da_info  *call_da_val = NULL;
  dsat_num_item_type i = 0;
  dsat_num_item_type max_calls = 0;
#ifdef FEATURE_DSAT_ETSI_MODE
  ds_subs_e_type subs_id = DS_FIRST_SUBS;
#endif /*FEATURE_DSAT_ETSI_MODE*/
  ASSERT( cmd_ptr != NULL );

  current_mode = dsatcmdp_get_current_mode_per_subs(cmd_ptr->cmd.cm_cmd.subs);
  call_event = (ds_at_cm_call_event_type*)cmd_ptr->cmd.cm_cmd.sub_cmd;
  ASSERT( call_event != NULL );

  if((call_event->event_info.dsat_info_list_avail == TRUE)&&
     (call_event->event ==  CM_CALL_EVENT_INFO))
  {
    subs_id = dsat_get_qcsimapp_val();
    if (subs_id >= (ds_subs_e_type)MAX_SUBS)
    {
      subs_id = DS_FIRST_SUBS;
    }
     if(DSAT_FAILURE == dsatutil_get_base_addr(DSAT_CALL_DA_VALS,(void **)&call_da_val,subs_id))
      {
         dsatutil_free_memory((void*)call_event);
         return DSAT_ASYNC_EVENT;
      }
        /* Copy Number of calls */
     call_da_val->dsat_cm_call_info_list.number_of_active_calls = 
           call_event->event_info.dsat_cm_call_info_list.number_of_active_calls;
     max_calls = MIN(CM_CALL_ID_MAX,call_da_val->dsat_cm_call_info_list.number_of_active_calls);
     /*Coping Call Info*/
     for(i = 0 ; i < max_calls; i++)
     {
       call_da_val->dsat_cm_call_info_list.info[i].call_id        = 
            call_event->event_info.dsat_cm_call_info_list.info[i].call_id;
       call_da_val->dsat_cm_call_info_list.info[i].call_type      =
            call_event->event_info.dsat_cm_call_info_list.info[i].call_type;
       call_da_val->dsat_cm_call_info_list.info[i].call_state     =
            call_event->event_info.dsat_cm_call_info_list.info[i].call_state;
       call_da_val->dsat_cm_call_info_list.info[i].sys_mode       =
            call_event->event_info.dsat_cm_call_info_list.info[i].sys_mode;
       call_da_val->dsat_cm_call_info_list.info[i].call_direction =
            call_event->event_info.dsat_cm_call_info_list.info[i].call_direction;
       call_da_val->dsat_cm_call_info_list.info[i].primary_pdp    =
            call_event->event_info.dsat_cm_call_info_list.info[i].primary_pdp;
       call_da_val->dsat_cm_call_info_list.info[i].primary_call_id=
            call_event->event_info.dsat_cm_call_info_list.info[i].primary_call_id;
       call_da_val->dsat_cm_call_info_list.info[i].profile_number =
            call_event->event_info.dsat_cm_call_info_list.info[i].profile_number;

       (void) dsatutil_memscpy((void*)&call_da_val->dsat_cm_call_info_list.info[i].call_subst,
                sizeof(cm_call_substate_type),
                (void*)&call_event->event_info.dsat_cm_call_info_list.info[i].call_subst,
                sizeof(cm_call_substate_type) );
     }
  }
  else
  {
    subs_id = dsatcmif_update_subs_idx(cmd_ptr->cmd.cm_cmd.subs);
    if (subs_id >= DS_SUBS_MAX)
    {
      dsatutil_free_memory((void*)call_event);
      return result;
    }
    dsat_update_call_info_global(cmd_ptr, subs_id);
    if (DSAT_FAILURE == dsatutil_get_base_addr(DSAT_CALL_DA_VALS,(void **)&call_da_val,subs_id))
    {
      dsatutil_free_memory((void*)call_event);
      return DSAT_ASYNC_EVENT;
    }
  }
  
  DS_AT_MSG2_MED("subs_id = %d, etsicall_was_call_active = %d",
                 subs_id, call_da_val->etsicall_was_call_active);

  switch (call_event->event)
  {

    case CM_CALL_EVENT_INFO:
        if (CHECK_PENDING(DSAT_EXT_CLCC_IDX,0,DSAT_PENDING_TRUE))
        {
          result = dsatact_call_info_list_clcc_cmd_handler(cmd_ptr, subs_id);
          dsatutil_free_memory((void*)call_event);
          return result;
        }
        else if(call_da_val->dsatact_num_clcc_calls > 0)
        {
          result = dsatact_call_info_clcc_cmd_handler(cmd_ptr, subs_id);
          dsatutil_free_memory((void*)call_event);
          return result;
        } 

        if(dsat_colp_pending[subs_id])
        {
          dsatetsicall_report_colp_result(subs_id);
          /* Return to command mode after reporting +COLP for voice call. */
          if( dsatvoice_is_voice_call(subs_id,
              call_event->event_info.call_id ) )
          {
            voice_state[subs_id][call_event->event_info.call_id].cmd_pending = FALSE;
            result = DSAT_OK;
          }
        }
        else if(dsat_clip_pending[subs_id])
        {
          dsatetsicall_report_clip_result(subs_id);
        }
        else if(dsat_cdip_pending[subs_id])
        {
          dsatetsicall_report_cdip_result(subs_id);
        }
        else if( dsatetsime_cind_pending[subs_id] )
        {
        result = dsatetsime_generate_cind_response( cmd_ptr , subs_id);
        }
        else if(CHECK_PENDING(DSATETSI_EXT_ACT_CPAS_ETSI_IDX,0,DSAT_PENDING_TRUE))
        {
          /* Process phone activity results */
          SET_PENDING(DSATETSI_EXT_ACT_CPAS_ETSI_IDX ,0, DSAT_PENDING_FALSE);
          result = dsatetsime_report_cpas (mode, NULL, subs_id);
        }
        else
        {
          result = DSAT_ASYNC_EVENT;
        }
      break;

      
    case CM_CALL_EVENT_INCOM:
      /* Assign a sequence number (as described in 3GPP TS 22.030 
         section 6.5.5.1) to this incoming call */
      if((call_event->event_info.call_type == CM_CALL_TYPE_VOICE ) ||
         (call_event->event_info.call_type == CM_CALL_TYPE_CS_DATA)||
         (call_event->event_info.call_type == CM_CALL_TYPE_PS_DATA))
      {
        dsatact_track_sequence_number(subs_id,
            call_event->event_info.call_id,
            call_event->event,
            call_event->event_info.call_type);
      }
#ifdef FEATURE_DSAT_ETSI_MODE
      /* Maintain call state info: add waiting call id and
         pass event to dsatvoice for RINGing or dsatetsicmif for +CCWA 
         unsolicited result code dependent upon active call or not */
      if ( IS_ETSI_MODE(current_mode) )
      {
        result = dsatetsicall_call_event_incom(mode, call_event ,subs_id);
      }
      else 
#endif /* FEATURE_DSAT_ETSI_MODE */
      {
        result = dsatvoice_call_event_incom(subs_id, &call_event->event_info);
      }
      break;
    case CM_CALL_EVENT_ABRV_ALERT:
      if( 1 == (dsat_num_item_type)dsatutil_get_val(DSAT_VENDOR_QCRCIND_IDX,0,0,NUM_TYPE) )
      {
        dsatetsicall_report_qcrcind_result(subs_id, &call_event->event);
      }
      break;
    case CM_CALL_EVENT_CONNECT:
      /* Reset the S7 timer if running  */
      cmif_stop_s_seven_timer(subs_id,
                              call_event->event_info.profile_number,
                              call_event->event_info.call_type,
                              call_event->event_info.call_id,
                              call_event->event_info.call_client_id);
      /* Assign a sequence number (as described in 3GPP TS 22.030 
              section 6.5.5.1) to this connected call unless already done when
              it was incoming */
      if((call_event->event_info.call_type == CM_CALL_TYPE_VOICE ) || 
        (call_event->event_info.call_type == CM_CALL_TYPE_CS_DATA) || 
        (call_event->event_info.call_type == CM_CALL_TYPE_PS_DATA))
      {
        dsatact_track_sequence_number(subs_id,
                         call_event->event_info.call_id,
                         call_event->event,
                         call_event->event_info.call_type);
      }  
      /* Check for call cmd originator */
#ifdef FEATURE_DSAT_ETSI_MODE
      /* ETSI builds */
      if ( IS_ETSI_MODE(current_mode) )
      {
        result = dsatetsicall_call_event_connect(mode, call_event , subs_id);
      }
      else
#endif /* FEATURE_DSAT_ETSI_MODE */
      {
      /* CDMA-only builds */
        result = dsatvoice_call_event_connect(subs_id,
                   &call_event->event_info);
      }
#if defined(FEATURE_HS_USB_SER3_PORT)
      if (CM_CALL_TYPE_PS_DATA ==
            call_event->event_info.call_type)
      {
        dsatcmif_servs_state_ss_info  *ph_ss_val = NULL;
        if(DSAT_FAILURE == dsatutil_get_base_addr(DSAT_SS_PH_SS_VALS,(void **)&ph_ss_val,
                                           DSAT_MS_FIRST_SUBS))
        {
           dsatutil_free_memory((void*)call_event);
           return DSAT_ASYNC_EVENT;
        }
        ph_ss_val->dsatcmif_dormant_ind_report_enabled = TRUE;
        DS_AT_MSG0_MED("cm call connected");
        (void)cmif_generate_dormant_ind();
      }
#endif /* defined(FEATURE_HS_USB_SER3_PORT) */
  
     if( (1 == (dsat_num_item_type)dsatutil_get_val(DSAT_VENDOR_QCRCIND_IDX,0,0,NUM_TYPE)) &&
      	                   (call_event->event_info.call_direction == CM_CALL_DIRECTION_MO) )
      {
        dsatetsicall_report_qcrcind_result(subs_id, &call_event->event);
      }
      break;

    case CM_CALL_EVENT_PROGRESS_INFO_IND:
      /* The MO (originating) call moved to alerting state, 
         started ringing on the other side */
      dsat_mo_dailing_state_set(subs_id, FALSE);
#ifdef FEATURE_DSAT_ETSI_MODE 
      /* Update +CIND callsetup indicator */
      (void)dsatetsime_change_indicator_state( DSAT_CIND_INDEX_CALLSETUP,
                                               DSAT_CIND_CSETUP_MORING, subs_id );
      break;
#endif /* FEATURE_DSAT_ETSI_MODE */

    case CM_CALL_EVENT_END:
      /* As call got ended make dsat_mo_dailing_state true */
      dsat_mo_dailing_state_set(subs_id, TRUE);
      /* Reset the S7 timer if running  */
      cmif_stop_s_seven_timer(subs_id,
                              call_event->event_info.profile_number,
                              call_event->event_info.call_type,
                              call_event->event_info.call_id,
                              call_event->event_info.call_client_id);

#ifdef FEATURE_DSAT_ETSI_MODE
      if (( (dsat_num_item_type)dsatutil_get_val(DSAT_VENDOR_DSCI_IDX,0,0,NUM_TYPE) == 1) &&
          IS_ETSI_MODE(current_mode))
      {
        cmif_report_dsci_result(subs_id, call_event);
      }
#endif /* FEATURE_DSAT_ETSI_MODE*/

      /* Free call sequence number (as described in 3GPP TS 22.030 
               section 6.5.5.1) of this ending call */
      if((call_event->event_info.call_type == CM_CALL_TYPE_VOICE ) || 
        (call_event->event_info.call_type == CM_CALL_TYPE_CS_DATA) || 
        (call_event->event_info.call_type == CM_CALL_TYPE_PS_DATA))
      {
        dsatact_track_sequence_number( subs_id,
                         call_event->event_info.call_id,
                         call_event->event,
                         call_event->event_info.call_type);
      }
#ifdef FEATURE_DSAT_ETSI_MODE 
      if ( IS_ETSI_MODE(current_mode) )
      {
        result = dsatetsicall_call_event_end(mode, cmd_ptr, subs_id);
      }
      else
#endif /* FEATURE_DSAT_ETSI_MODE */
      {
        result = dsatvoice_call_event_end(subs_id,
                                          &call_event->event_info);
      }
      if( (1 == (dsat_num_item_type)dsatutil_get_val(DSAT_VENDOR_QCRCIND_IDX,0,0,NUM_TYPE)) &&
                              call_event->event_info.end_status != CM_CALL_END_NETWORK_END)
      {
        dsatetsicall_report_qcrcind_result(subs_id, &call_event->event);
      }
      break;

#ifdef FEATURE_DSAT_ETSI_MODE
    case CM_CALL_EVENT_ORIG_FWD_STATUS:

      dsatetsicall_call_event_cssi_report(
                                           mode, 
                                           cmd_ptr,
                                           subs_id,
                                           DSAT_CSSI_ORIG_FWD_STAT
                                         );
      break;

    case CM_CALL_EVENT_CALL_BARRED:

      dsatetsicall_call_event_cssi_report(
                                           mode, 
                                           cmd_ptr,
                                           subs_id,
                                           DSAT_CSSI_OUTGOING_CALLS_BARRED
                                         );

      break;

    case CM_CALL_EVENT_CALL_IS_WAITING:

      dsatetsicall_call_event_cssi_report(
                                           mode, 
                                           cmd_ptr,
                                           subs_id,
                                           DSAT_CSSI_CALL_WAITING
                                         );
      break;

    case CM_CALL_EVENT_CALL_ON_HOLD:

      dsatetsicall_call_event_cssu_report(
                                           mode, 
                                           cmd_ptr,
                                           subs_id,
                                           DSAT_CSSU_CALL_HOLD
                                         );
      break;

    case CM_CALL_EVENT_CALL_RETRIEVED:

      dsatetsicall_call_event_cssu_report(
                                           mode, 
                                           cmd_ptr,
                                           subs_id,
                                           DSAT_CSSU_CALL_RETRIEVED
                                         );

      break;

    case CM_CALL_EVENT_CALL_FORWARDED:

      dsatetsicall_call_event_cssu_report(
                                           mode, 
                                           cmd_ptr,
                                           subs_id,
                                           DSAT_CSSU_ADDITIONAL_INCOM_CALL_FWD
                                         );
      break;

    case CM_CALL_EVENT_CALL_BEING_FORWARDED:

      dsatetsicall_call_event_cssi_report(
                                           mode, 
                                           cmd_ptr,
                                           subs_id,
                                           DSAT_CSSI_CALL_FORWARDED
                                         );
      break;    

    case CM_CALL_EVENT_INCOM_FWD_CALL:

      dsatetsicall_call_event_cssu_report(
                                           mode, 
                                           cmd_ptr,
                                           subs_id,
                                           DSAT_CSSU_FORWARDED_CALL
                                         );
      break;

    case CM_CALL_EVENT_MNG_CALLS_CONF:
      /* Reset the S7 timer if running  */
      cmif_stop_s_seven_timer(subs_id,
                              call_event->event_info.profile_number,
                              call_event->event_info.call_type,
                              call_event->event_info.call_id,
                              call_event->event_info.call_client_id);

      result = dsatetsicall_call_event_mng_calls_conf(mode, cmd_ptr, subs_id);
      break;
#endif /* FEATURE_DSAT_ETSI_MODE */
    case CM_CALL_EVENT_ANSWER:
    case CM_CALL_EVENT_ORIG:
      {
        /* Start the S7 timer iff it is ATCoP originated/answered Call */
        if( (dsat_num_item_type)dsatutil_get_val(
                                    DSAT_SREG_S7_IDX,0,0,NUM_TYPE) > 0)
        {
          cmif_start_s_seven_timer(subs_id,
                                   call_event->event_info.profile_number,
                                   call_event->event_info.call_type,
                                   call_event->event_info.call_id,
                                   call_event->event_info.call_client_id);
        }
        if( call_event->event == CM_CALL_EVENT_ORIG )
        {
          result = dsatvoice_call_event_orig(subs_id,
                                             &call_event->event_info);
        }
      }
      break;

    case CM_CALL_EVENT_SETUP_IND:
#ifdef FEATURE_DSAT_ETSI_MODE
    if(IS_ETSI_MODE(current_mode))
    {
      result = 
      dsatetsicmif_get_clip_info_or_report_result(subs_id,
                         call_event->event_info.call_id);
      result = 
      dsatetsicmif_get_cdip_info_or_report_result(subs_id,
                         call_event->event_info.call_id);
    }
#endif /* FEATURE_DSAT_ETSI_MODE */
      if((call_event->event_info.call_type == CM_CALL_TYPE_VOICE ) ||
         (call_event->event_info.call_type == CM_CALL_TYPE_CS_DATA)||
         (call_event->event_info.call_type == CM_CALL_TYPE_PS_DATA))
      {

        dsatact_track_sequence_number(subs_id,
          call_event->event_info.call_id,
          call_event->event,
          call_event->event_info.call_type);
      }
      break;

    case CM_CALL_EVENT_ACT_BEARER_IND:
    case CM_CALL_EVENT_PDN_CONN_REJ_IND:
    case CM_CALL_EVENT_PDN_CONN_FAIL_IND:
    case CM_CALL_EVENT_RES_ALLOC_REJ_IND:
    case CM_CALL_EVENT_RES_ALLOC_FAIL_IND:
      if(call_event->event_info.call_type == CM_CALL_TYPE_PS_DATA)
      {
        dsatact_track_sequence_number(subs_id,
          call_event->event_info.call_id,
          call_event->event,
          call_event->event_info.call_type);
      }
      break;

    case CM_CALL_EVENT_CALL_CONF:
      /* Do nothing, event info captured in callback */
      break;

#ifdef FEATURE_DATA_WCDMA_PS
    case CM_CALL_EVENT_RAB_REESTAB_IND:
      if((call_event->event_info.call_type == CM_CALL_TYPE_PS_DATA) &&
         (call_event->event_info.sys_mode == SYS_SYS_MODE_LTE))
      {
        dsatact_track_sequence_number(subs_id,
          call_event->event_info.call_id,
          call_event->event,
          call_event->event_info.call_type);
      }
      break;
#endif /* FEATURE_DATA_WCDMA_PS */

#ifdef FEATURE_DUAL_ACTIVE
    case CM_CALL_EVENT_LOCAL_HOLD:
      if ( IS_ETSI_MODE(current_mode) )
      {
        dsatetsicall_call_event_local_hold(mode, call_event, subs_id);
      }

      dsatvoice_call_event_local_hold(subs_id, &call_event->event_info);
      break;
#endif /* FEATURE_DUAL_ACTIVE */

    case CM_CALL_EVENT_PS_SIG_REL_CNF:
      if(CHECK_PENDING(DSATETSI_EXT_ACT_CNMPSD_ETSI_IDX ,0, DSAT_PENDING_TRUE))
      {
         if( PS_SIG_REL_SUCCESS == call_event->event_info.dsat_mode_info.info.gw_ps_call.ps_sig_rel_status )
         {
           SET_PENDING(DSATETSI_EXT_ACT_CNMPSD_ETSI_IDX ,0, DSAT_PENDING_FALSE)
           result = DSAT_OK;
         }
         else
         {
           result = DSAT_ERROR;
         }
      }
      break;

    case CM_CALL_EVENT_HO_COMPLETE:
    case CM_CALL_EVENT_SRVCC_COMPLETE_IND:
      {
        result = dsatvoice_call_event_ho_complete(subs_id,&call_event->event_info);
      }
      break;
    default:
      DS_AT_MSG1_HIGH("Unsupport CM CALL event: %d",
                call_event->event);
      result = DSAT_ERROR;
      break;
  }
#ifdef FEATURE_DSAT_ETSI_MODE
  if (( (dsat_num_item_type)dsatutil_get_val(DSAT_VENDOR_DSCI_IDX,0,0,NUM_TYPE) == 1) &&
      IS_ETSI_MODE(current_mode) && (CM_CALL_EVENT_END != call_event->event))
  {
    cmif_report_dsci_result(subs_id, call_event);
  }
#endif /* FEATURE_DSAT_ETSI_MODE*/
  dsatutil_free_memory((void*)call_event);
  return result;
} /* dsatcmif_cm_call_event_handler */



/*===========================================================================

FUNCTION  DSATCMIF_CM_CALL_CMD_HANDLER

DESCRIPTION
  This function is the handler function for the CM call-related commands

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR :    if there was any problem in executing the command
    DSAT_OK :       if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
/* ARGSUSED */
dsat_result_enum_type dsatcmif_cm_call_cmd_handler
(
  dsat_mode_enum_type   mode,                /* AT command mode            */
  ds_cmd_type         * cmd_ptr              /* DS Command pointer         */
)
{
#define DSAT_CONTEXT_ID_MASK 0xFF

  dsat_result_enum_type result = DSAT_NO_RSP;
  ds_at_cm_call_cmd_type*    call_cmd = NULL;
#ifdef FEATURE_DSAT_ETSI_MODE
  dsati_mode_e_type current_mode = dsatcmdp_get_current_mode();
#endif /* FEATURE_DSAT_ETSI_MODE */
  dsat_num_item_type subs_id = dsat_get_qcsimapp_val();
  dsatetsicmif_sups_state_ms_info  *sups_ms_val = NULL;
  
  if(subs_id >= MAX_SUBS || NULL == cmd_ptr || NULL == cmd_ptr->cmd.cm_cmd.sub_cmd)
  {
    return DSAT_ERROR;
  }
  call_cmd = (ds_at_cm_call_cmd_type*)cmd_ptr->cmd.cm_cmd.sub_cmd;
  DS_AT_MSG3_HIGH("Call cmd handler cmd_err %d [Applicable for only voice] cause %d alpha len %d",
            call_cmd->cmd_err, call_cmd->err_cause, call_cmd->alpha.len);
  if(DSAT_FAILURE == dsatutil_get_base_addr(DSAT_SUPS_MS_VALS,(void **)&sups_ms_val,subs_id))
  {
    dsatutil_free_memory((void*)call_cmd);
    return DSAT_ASYNC_EVENT;
  }
  switch (call_cmd->cmd)
  {
    case CM_CALL_CMD_INFO_GET:
      if (CM_CALL_CMD_ERR_NOERR != call_cmd->cmd_err)
      {
#ifdef FEATURE_DSAT_ETSI_MODE
        if ( IS_ETSI_MODE(current_mode) )
        {
          if( sups_ms_val->dsat_interr_ss_service == clip || 
              sups_ms_val->dsat_interr_ss_service == colp ||
              sups_ms_val->dsat_interr_ss_service == allLineIdentificationSS
            )
          {
            sups_ms_val->dsat_interr_ss_service = (ss_operation_code_T)DSAT_INVALID_OP_CODE;
            dsatutil_free_memory((void*)call_cmd);
            return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
          }
          else 
          {
            DS_AT_MSG1_HIGH("Failed on CM call info list query: %d",
                    call_cmd->cmd_err);
            dsatutil_free_memory((void*)call_cmd);
            return dsat_send_cme_error(DSAT_CME_UNKNOWN);
          }
        }
        else
#endif /* FEATURE_DSAT_ETSI_MODE */
        {
          result = DSAT_ERROR;
        }
      }
      else
      {
        result = DSAT_ASYNC_CMD;
      }
      break;

    case CM_CALL_CMD_INFO_LIST_GET:

      if (CM_CALL_CMD_ERR_NOERR != call_cmd->cmd_err)
      {
        DS_AT_MSG1_HIGH("Failed on CM call info list query: %d",
                  call_cmd->cmd_err);
#ifdef FEATURE_DSAT_ETSI_MODE
        dsatetsime_cind_pending[subs_id] = FALSE;
#endif /* FEATURE_DSAT_ETSI_MODE */
#ifdef FEATURE_DSAT_ETSI_DATA
        dsat_pdp_state.pending = FALSE;
#endif /* FEATURE_DSAT_ETSI_DATA */
        result = DSAT_ERROR;
      }
      else
      {
        result = DSAT_ASYNC_CMD;
      }
      break;

    case CM_CALL_CMD_ORIG:
      /* Check for call cmd originator */
#ifdef FEATURE_DSAT_ETSI_DATA
      if ( IS_ETSI_MODE(current_mode) )
      {
        if (FALSE == dsat_pdp_state.pending)
        {
          /* Voice call */
          result = dsatvoice_cmd_orig_cb_handler(subs_id,
                     call_cmd->cmd_err);
        }
      }
      else
#endif /* FEATURE_DSAT_ETSI_DATA */
      {
        /* Voice call */
        result = dsatvoice_cmd_orig_cb_handler(subs_id,
                   call_cmd->cmd_err);
      }
      break;

    case CM_CALL_CMD_ANSWER:
      result = dsatvoice_cmd_answer_cb_handler(subs_id,
               call_cmd->cmd_err);
      break;
  
    case CM_CALL_CMD_END:
      /* Check for call cmd originator */
      if ( FALSE == dsat_pdp_state.pending )
      {
        /* Voice call */
        result = dsatvoice_cmd_end_cb_handler(subs_id, 
                   call_cmd->cmd_err);
      }
      break;
    case CM_CALL_CMD_SUPS:
#ifdef FEATURE_DSAT_ETSI_MODE
      if ( IS_ETSI_MODE(current_mode) )
      {
        result = dsatetsicall_sups_cmd_cb_handler(subs_id, cmd_ptr);
      }
#endif /* FEATURE_DSAT_ETSI_MODE */
      break;
    case CM_CALL_CMD_PS_SIG_REL:
      if( CHECK_PENDING(DSATETSI_EXT_ACT_CNMPSD_ETSI_IDX ,0, DSAT_PENDING_TRUE) &&
                                       (call_cmd->cmd_err != CM_CALL_CMD_ERR_NOERR )) 
      { 
         SET_PENDING(DSATETSI_EXT_ACT_CNMPSD_ETSI_IDX ,0, DSAT_PENDING_FALSE);
         result = dsat_send_cme_error(DSAT_CME_OP_NOT_ALLOWED);
      }
      else
      {
        result = DSAT_ASYNC_EVENT;
      }
      break;
    
    default:
    DS_AT_MSG1_HIGH("Unsupported CM CALL command: %d",call_cmd->cmd);
    break;
  }
  dsatutil_free_memory((void*)call_cmd);
  return result;
} /* dsatcmif_cm_call_cmd_handler */



/*===========================================================================

FUNCTION  DSATCMIF_CM_SS_CMD_HANDLER

DESCRIPTION
  This function is the handler function for the CM service system-related
  commands

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR :    if there was any problem in executing the command
    DSAT_ASYNC_CMD :  if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
/* ARGSUSED */
dsat_result_enum_type dsatcmif_cm_ss_cmd_handler
(
  dsat_mode_enum_type   mode,                /* AT command mode            */
  ds_cmd_type         * cmd_ptr              /* DS Command pointer         */
)
{
  dsat_result_enum_type result = DSAT_ASYNC_CMD;
  ds_at_cm_ss_cmd_type*      ss_cmd;
  ASSERT( cmd_ptr != NULL );

  ss_cmd = (ds_at_cm_ss_cmd_type*)cmd_ptr->cmd.cm_cmd.sub_cmd;
  ASSERT( ss_cmd != NULL );

  switch (ss_cmd->cmd)
  {
  case CM_SS_CMD_INFO_GET:
    /* Verify there is no error condition */
    if (CM_SS_CMD_ERR_NOERR != ss_cmd->cmd_err)
    {
      DS_AT_MSG1_HIGH("CM SS command error: %d",ss_cmd->cmd_err);
      SET_PENDING(DSAT_VENDOR_QCSYSMODE_IDX, 0, DSAT_PENDING_FALSE)
      result = DSAT_ERROR;
    }
    break;
    
  default:
    DS_AT_MSG1_HIGH("Unsupport CM SS command: %d",ss_cmd->cmd);
    result = DSAT_ERROR;
    break;
  }
  dsatutil_free_memory((void *)ss_cmd);
  return result;
} /* dsatcmif_cm_ss_cmd_handler */

/*===========================================================================

FUNCTION  DSATCMIF_CM_GET_INCOMING_CALL_NUM

DESCRIPTION
  This function is used to get the incoming call number. 

DEPENDENCIES
  None

RETURN VALUE
  None
SIDE EFFECTS
  None

===========================================================================*/
void dsatcmif_cm_get_incoming_call_num
(
  ds_subs_e_type subs_id,
  cm_num_s_type* num_ptr
)
{
  dsatetsicall_call_state_da_info  *call_da_val = NULL;
  
  if(DSAT_FAILURE == dsatutil_get_base_addr(DSAT_CALL_DA_VALS,(void **)&call_da_val,subs_id))
  {
     return;
  }

  ASSERT( num_ptr != NULL );
  *num_ptr = call_da_val->dsat_cm_curr_incoming_call_num;
} /*dsatcmif_cm_get_call_num*/

#ifdef FEATURE_DSAT_ETSI_MODE
/*===========================================================================

FUNCTION  DSATCMIF_CM_INBAND_CMD_CB_FUNC

DESCRIPTION
  This function is the callback function for the CM inband events 

DEPENDENCIES
  None

RETURNS
  None

SIDE EFFECTS
  None

===========================================================================*/
/* ARGSUSED */
 void dsatcmif_cm_inband_cmd_cb_func
(
  void*                     data_ptr,  /*  Not used.  */
  cm_inband_cmd_e_type      cmd,
  cm_inband_cmd_err_e_type  cmd_err
)
{
  if ( cmd_err != CM_INBAND_CMD_ERR_NOERR )
  {
    ds_cmd_type              *cmd_buf;
    ds_at_cm_inband_cmd_type *inband_cmd = NULL;

    cmd_buf = dsat_get_cmd_buf(FALSE);
    /* send the message to ATCOP */
    DS_AT_MSG2_ERROR(" CM Inband Cmd Err cmd=%x err=%x", cmd, cmd_err);
    cmd_buf->hdr.cmd_id = DS_AT_CM_INBAND_CMD;
    inband_cmd = (ds_at_cm_inband_cmd_type*)dsat_alloc_memory(
                                              sizeof(ds_at_cm_inband_cmd_type), FALSE);
    inband_cmd->cmd = cmd;
    inband_cmd->cmd_err = cmd_err;
    cmd_buf->cmd.cm_cmd.sub_cmd = inband_cmd;
    ds_put_cmd( cmd_buf );
  }
} /* dsatcmif_cm_inband_cb_func */

/*===========================================================================

FUNCTION  DSATCMIF_CM_INBAND_CMD_HANDLER

DESCRIPTION
  This function is the handler function for the CM inband events related
  commands

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR :    if there was any problem in executing the command
    DSAT_ASYNC_EVENT :  if it is a success.
SIDE EFFECTS
  None

===========================================================================*/
/* ARGSUSED */
dsat_result_enum_type dsatcmif_cm_inband_cmd_handler
(
  dsat_mode_enum_type   mode,                /* AT command mode            */
  ds_cmd_type         * cmd_ptr              /* DS Command pointer         */
)
{
  dsat_result_enum_type result = DSAT_ASYNC_EVENT;
  dsati_mode_e_type current_mode = dsatcmdp_get_current_mode();
  ds_at_cm_inband_cmd_type*    inband_cmd = NULL;
  
  ASSERT( cmd_ptr != NULL );

  inband_cmd = (ds_at_cm_inband_cmd_type*)cmd_ptr->cmd.cm_cmd.sub_cmd;
  ASSERT( inband_cmd != NULL );
  if ( IS_ETSI_MODE(current_mode) )
  {
    switch (inband_cmd->cmd)
    {
      case CM_INBAND_CMD_START_CONT_DTMF:
      case CM_INBAND_CMD_STOP_CONT_DTMF:
        SET_PENDING(DSATETSI_EXT_ACT_VTS_ETSI_IDX ,0, DSAT_PENDING_FALSE)
        result = DSAT_ERROR;
        break;
      default:
        DS_AT_MSG1_HIGH("Unsupported CM command: %d", inband_cmd->cmd);
        result = DSAT_ERROR;
        break;
    }
  }
  dsatutil_free_memory((void*)inband_cmd);
  return result;
}/*dsatcmif_cm_inband_event_handler*/
#endif /* FEATURE_DSAT_ETSI_MODE */

#ifdef FEATURE_DATA_IS707
/*===========================================================================

FUNCTION  DSATCMIF_CM_GET_HDR_RSSI_INTENSITY_LEVEL

DESCRIPTION
  This function converts hdr_rssi value to intensity signal level.

DEPENDENCIES
  None

RETURN VALUE
  uint16: intensity level

SIDE EFFECTS
  None

===========================================================================*/
uint16 dsatcmif_cm_get_hdr_rssi_intensity_level
(
  void
)
{
#define SIGNAL_LEVEL_60   60
#define SIGNAL_LEVEL_75   75
#define SIGNAL_LEVEL_90   90
#define SIGNAL_LEVEL_105  105

  uint16 intensity_level = 0;
  uint16 hdr_rssi;
  dsatcmif_servs_state_ss_info  *ph_ss_val = NULL;
  
  if(DSAT_FAILURE == dsatutil_get_base_addr(DSAT_SS_PH_SS_VALS,(void **)&ph_ss_val,DSAT_MS_FIRST_SUBS))
  {
     return intensity_level;
  }

  /*  If we are not Hybrid mode && HDR combination use regular rssi 
      for all others use hdr_rssi.
  */
  if( ( ph_ss_val->dsatcmif_cdma_signal_reporting.hdr_hybrid == FALSE ) &&
      ( ph_ss_val->dsatcmif_cdma_signal_reporting.sys_mode == SYS_SYS_MODE_HDR ) )
  {
    hdr_rssi = ph_ss_val->dsatcmif_cdma_signal_reporting.rssi;
  }
  else
  {
    hdr_rssi = ph_ss_val->dsatcmif_cdma_signal_reporting.hdr_rssi;
  }
  /*  When the oprt mode is not online then the below "if condion" 
      will make hdr_rssi into "no signal".
  */
  if( SYS_OPRT_MODE_ONLINE != ph_ss_val->dsat_oprt_mode )
  {
    hdr_rssi = HDR_RSSI_NO_SIGNAL;
  }

  if ( hdr_rssi < SIGNAL_LEVEL_60 )
  {
    intensity_level = 99;
  }
  else if ( hdr_rssi < SIGNAL_LEVEL_75 )
  {
    intensity_level = 80;
  }
  else if ( hdr_rssi < SIGNAL_LEVEL_90 )
  {
    intensity_level = 60;
  }
  else if ( hdr_rssi < SIGNAL_LEVEL_105 )
  {
    intensity_level = 40;
  }
  else
  {
    intensity_level = 20;
  }
  if( hdr_rssi == HDR_RSSI_NO_SIGNAL )
  {
    /* out of service */
    intensity_level = 0;
  }
  return intensity_level;
} /* dsatcmif_cm_get_hdr_rssi_intensity_level */
#endif /* FEATURE_DATA_IS707 */

/*===========================================================================
FUNCTION  CMIF_SS_CMD_CB_FUNC

DESCRIPTION
  CM serving system status command callback function

DEPENDENCIES
  None

RETURNS
  None

SIDE EFFECTS
  Adds command in DS command buffer

===========================================================================*/
/* ARGSUSED */
LOCAL void cmif_ss_cmd_cb_func 
(
  void                         *data_ptr,         /* Data block pointer    */
  cm_ss_cmd_e_type              cmd,              /* Command ID            */
  cm_ss_cmd_err_e_type          cmd_err           /* Command error code    */
)
{
  ds_cmd_type * cmd_buf;
    
  /* Filter supported commands */
  switch (cmd)
  {
  case CM_SS_CMD_INFO_GET:
    /* Verify there is no error condition */
    if (CM_SS_CMD_ERR_NOERR != cmd_err)
    {
      ds_at_cm_ss_cmd_type *ss_cmd = NULL;

      /* Send error to DS task context */
      cmd_buf = dsat_get_cmd_buf(FALSE);
      /* send the message to ATCOP */
      cmd_buf->hdr.cmd_id = DS_AT_CM_SS_CMD;
      ss_cmd = (ds_at_cm_ss_cmd_type *)dsat_alloc_memory(
                                         sizeof(ds_at_cm_ss_cmd_type), FALSE);
      ss_cmd->cmd = cmd;
      ss_cmd->cmd_err = cmd_err;
      cmd_buf->cmd.cm_cmd.sub_cmd = ss_cmd;
      ds_put_cmd(cmd_buf);
    }
    break;
    
  default:
    /* Do nothing for unsupported commands */
    break;
  }

  return;
} /* cmif_ss_cmd_cb_func */



/*===========================================================================

FUNCTION   CMIF_SUBSCRIPTION_AVAILABLE_PROCESS_COPS

DESCRIPTION
  This function processes cops after the subscription
  is available.

DEPENDENCIES
  None

RETURN VALUE
  dsat_result_enum_type

SIDE EFFECTS
  None

===========================================================================*/
LOCAL dsat_result_enum_type cmif_subscription_available_process_cops
(
  dsatcmif_servs_state_ms_info *ph_ss_ms_val,
  ds_subs_e_type                subs_info
)
{
  dsat_result_enum_type result = DSAT_ERROR;

  if ( ( DSAT_COPS_MODE_MANUAL== ph_ss_ms_val->net_pref.mode ) ||
       ( DSAT_COPS_MODE_MANAUTO == ph_ss_ms_val->net_pref.mode ))
  {
     result = dsatetsicmif_change_network_registration ( 
                          ph_ss_ms_val->net_pref.mode,
                          &ph_ss_ms_val->net_pref.plmn,
                          ph_ss_ms_val->net_pref.act,
                          subs_info );
  }
  else if ( DSAT_COPS_MODE_AUTO == ph_ss_ms_val->net_pref.mode )
  {
     /* in case of QCCOPS plmn need to be paased. will try to camp on passed PLMN first 
        in auto mode*/
     if(ph_ss_ms_val->dsatetsicall_network_list.cmd_idx == CMD_IDX_QCCOPS ||
        ph_ss_ms_val->dsatetsicall_network_list.cmd_idx == CMD_IDX_QCCSGCOPS)
     {
        result = dsatetsicmif_change_network_registration ( 
                             ph_ss_ms_val->net_pref.mode,
                             &(ph_ss_ms_val->net_pref.plmn),
                             ph_ss_ms_val->net_pref.act,
                             subs_info );
     }
     else
     {
        result = dsatetsicmif_change_network_registration ( 
                             ph_ss_ms_val->net_pref.mode,
                             NULL,
                             ph_ss_ms_val->net_pref.act,
                             subs_info);
     }
   }
   else
   {
      DS_AT_MSG0_ERROR(" Other modes should not invoke subcription available");
   }
              
   return result;
} /* cmif_subscription_available_process_cops */

/*===========================================================================

FUNCTION   CMIF_GET_SUBS_INFO

DESCRIPTION
  This function maps a SS event CM subs id into a ATCOP subs ID.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
LOCAL ds_subs_e_type cmif_get_subs_info
(
  ds_sys_modem_as_id_e_type cm_subs_id
)
{
    ds_subs_e_type          subs_info = DS_FIRST_SUBS;
    
  if( dsat_qcsimapp_info[PRIMA_SUBS].subs_id == cm_subs_id )
  {
    subs_info = DS_FIRST_SUBS;
  }
  else if( dsat_qcsimapp_info[SECON_SUBS].subs_id == cm_subs_id )
  {
    subs_info = DS_SECOND_SUBS;
  }
#ifdef FEATURE_TRIPLE_SIM
  else if( dsat_qcsimapp_info[THIRD_SUBS].subs_id == cm_subs_id )
  {
    subs_info = DS_THIRD_SUBS;
  }
#endif
  else
  {
    subs_info = DS_FIRST_SUBS;
  }
  return subs_info;
}/* cmif_get_subs_info */
/*===========================================================================

FUNCTION   CMIF_DETERMINE_SUBS_MODE

DESCRIPTION
  This function determines mode of subscription in a SS event.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
LOCAL dsat_subs_mode_e_type cmif_determine_subs_mode
(
  ds_at_cm_ss_event_type *event_ptr
)
{
  dsat_subs_mode_e_type subs_mode = DSAT_SUB_INACTIVE;

  if(event_ptr->number_of_stacks == 2)
  {

   /*NO Service + NO Service
    Cleared all stored information */
    if((event_ptr->event_info[DSAT_STACK_MAIN].sys_mode == SYS_SYS_MODE_NO_SRV) &&
       (event_ptr->event_info[DSAT_STACK_HYBR].sys_mode == SYS_SYS_MODE_NO_SRV))
    {
      subs_mode = DSAT_SUB_NO_SRV;
    }
  /*
    **SGLTE Cases**
     LTE + G
     LTE + No Service
     No Service + G
 */
    else if((event_ptr->event_info[DSAT_STACK_MAIN].sys_mode == SYS_SYS_MODE_LTE) ||
     (event_ptr->event_info[DSAT_STACK_MAIN].sys_mode == SYS_SYS_MODE_TDS) ||
     (event_ptr->event_info[DSAT_STACK_HYBR].sys_mode == SYS_SYS_MODE_GSM))
   {
     subs_mode = DSAT_SUB_SGLTE;
   }
    /*
    **SVLTE Cases**
    1X + LTE
    1X + No Service 
    NO Service + LTE
    */
    else if(((event_ptr->event_info[DSAT_STACK_MAIN].sys_mode == SYS_SYS_MODE_CDMA) ||
        (event_ptr->event_info[DSAT_STACK_MAIN].sys_mode == SYS_SYS_MODE_NO_SRV)) &&
       ((event_ptr->event_info[DSAT_STACK_HYBR].sys_mode == SYS_SYS_MODE_LTE) ||
        (event_ptr->event_info[DSAT_STACK_HYBR].sys_mode  == SYS_SYS_MODE_NO_SRV)))
    {
      subs_mode = DSAT_SUB_SVLTE;
    }
    
/*
    **SVDO Cases**
    1X + HDR
    1X + No Service
    NO Service + HDR
*/
    else if(((event_ptr->event_info[DSAT_STACK_MAIN].sys_mode == SYS_SYS_MODE_CDMA) ||
        (event_ptr->event_info[DSAT_STACK_MAIN].sys_mode == SYS_SYS_MODE_NO_SRV)) &&
       ((event_ptr->event_info[DSAT_STACK_HYBR].sys_mode == SYS_SYS_MODE_HDR) ||
        (event_ptr->event_info[DSAT_STACK_HYBR].sys_mode  == SYS_SYS_MODE_NO_SRV)))
    {
      subs_mode = DSAT_SUB_SVDO;
    }
    
  }
  else
  {
    subs_mode = DSAT_SUB_STANDALONE;
  }

  DS_AT_MSG5_HIGH("is_main_operational: %d is_hybr_operational: %d Subs Mode: %d Main sys mode %d Hybr sys mode %d", 
    event_ptr->event_info[DSAT_STACK_MAIN].is_operational, event_ptr->event_info[DSAT_STACK_HYBR].is_operational, subs_mode,
    event_ptr->event_info[DSAT_STACK_MAIN].sys_mode,event_ptr->event_info[DSAT_STACK_HYBR].sys_mode);
    
  return subs_mode;
  
}/*cmif_determine_subs_mode*/

/*===========================================================================

FUNCTION  DSATCMIF_CM_SS_EVENT_HANDLER

DESCRIPTION
  This function is the handler function for the CM serving system
  related events

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR :    if there was any problem in executing the command
    DSAT_OK :       if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatcmif_cm_ss_event_handler
(
  dsat_cm_msg_s_type *msg_ptr
)
{
  dsat_result_enum_type   result = DSAT_ASYNC_EVENT;
  dsat_result_enum_type   result_tmp = DSAT_ASYNC_EVENT;
  ds_at_cm_ss_event_type *event_ptr = NULL;
  dsat_num_item_type      rssi_enqueue = 0;
  dsat_stack_id_e_type    stack_id;
  ds_subs_e_type          subs_info = DS_FIRST_SUBS;
  dsat_subs_mode_e_type   subs_mode;
  
  #define DSAT_SS_SRV_CHG_MASK DSAT_SS_SRV_DOMAIN_MASK|DSAT_SS_SYS_ID_MASK |DSAT_SS_SRV_IND_MASK |DSAT_SS_SRV_STATUS_MASK | \
                             DSAT_SS_SYS_MODE_MASK |DSAT_SS_CSG_INFO_CHANGED_MASK|DSAT_SS_PS_DATA_SUSPEND_MASK | \
                             DSAT_SS_SIM_STATE_MASK |DSAT_SS_LAC_TAC_RAC_CHGD_MASK \

  if (   (NULL == msg_ptr) 
       ||(msg_ptr->msg.ss_event.number_of_stacks < 1) 
       ||(msg_ptr->msg.ss_event.number_of_stacks > 2)
     )
  {
    DS_AT_MSG0_ERROR("Either msg ptr is NULL or No of Stack is 0: Ignored SS event processing");
    return result;
  }

  /*Assign  SS event info  pointer*/
  event_ptr = &msg_ptr->msg.ss_event;
  /* Calculate subs_info */
  subs_info = cmif_get_subs_info(event_ptr->subs);
  /* Calculate subscription mode  */
  subs_mode = cmif_determine_subs_mode(event_ptr);
  if(subs_mode == DSAT_SUB_SGLTE)
  {
    dsat_qcsimapp_info[subs_info].is_sglte_sub = TRUE;
  }
  
  DS_AT_MSG3_HIGH("Processing CM SS event: %d ATCoP Subs ID %d CM Subs ID %d",
          event_ptr->event,subs_info,event_ptr->subs);

  switch (event_ptr->event)
  {
    case CM_SS_EVENT_SRV_CHANGED:
    {
      for ( stack_id = DSAT_STACK_MAIN; stack_id < event_ptr->number_of_stacks; stack_id++ )
      {
        if(DSAT_STACK_MAIN == stack_id)
        { 
             switch(event_ptr->event_info[stack_id].sys_mode)
            {
              /*Process 3GPP information*/
              case SYS_SYS_MODE_NO_SRV:
              case SYS_SYS_MODE_GSM:
              case SYS_SYS_MODE_WCDMA:
              case SYS_SYS_MODE_LTE:
              case SYS_SYS_MODE_TDS:
              case SYS_SYS_MODE_GW:
              case SYS_SYS_MODE_GWL:
                if ( ( event_ptr->event_info[stack_id].changed_fields & 
                        (DSAT_SS_SRV_CHG_MASK 
#ifdef FEATURE_GSM_RR_CELL_CALLBACK 
                         | DSAT_SS_CELL_INFO_MASK 
#endif /* FEATURE_GSM_RR_CELL_CALLBACK */
                        )) || 
                      ( DSAT_CM_REQ_ID == event_ptr->event_info[stack_id].cm_req_id ) )
                {
                  /*Process CM registration information */
                  result_tmp = cmif_process_cm_reg_data(event_ptr, stack_id,subs_info);
                }
                if ( event_ptr->event_info[stack_id].changed_fields & DSAT_SS_MOBILITY_MGMT_MASK )
                {
                  (void)dsatetsicmif_process_cm_mm_info_data(&event_ptr->event_info[stack_id].mm_info, subs_info);
                }
              break;
              /*Undefined system mode for ATCoP*/
              default:
              break;
            }
        }	
        else
        {
           switch(subs_mode)
           {
              case DSAT_SUB_SGLTE:
#ifdef FEATURE_SGLTE
               /* Keep GSM information at MAX -1 index for SGLTE*/
                subs_info = dsatcmif_get_sglte_hybr_subs_info();
#endif /* FEATURE_SGLTE */
                if ( ( event_ptr->event_info[stack_id].changed_fields & 
                      (DSAT_SS_SRV_CHG_MASK 
#ifdef FEATURE_GSM_RR_CELL_CALLBACK 
                       | DSAT_SS_CELL_INFO_MASK 
#endif /* FEATURE_GSM_RR_CELL_CALLBACK */
                       )) || 
                    ( DSAT_CM_REQ_ID == event_ptr->event_info[stack_id].cm_req_id ) )
                {
#ifdef FEATURE_SGLTE
                 cmif_process_cm_hybr_reg_data(event_ptr, stack_id, subs_info);
#endif /* FEATURE_SGLTE */
                }
                if ( event_ptr->event_info[stack_id].changed_fields & DSAT_SS_MOBILITY_MGMT_MASK )
                {
                   dsatetsicmif_process_cm_mm_info_net_reg(
                           &event_ptr->event_info[stack_id].mm_info, subs_info);
                }
                break;
              case DSAT_SUB_SVLTE:
                 if ( ( event_ptr->event_info[stack_id].changed_fields & 
                      (DSAT_SS_SRV_CHG_MASK 
#ifdef FEATURE_GSM_RR_CELL_CALLBACK 
                       | DSAT_SS_CELL_INFO_MASK 
#endif /* FEATURE_GSM_RR_CELL_CALLBACK */
                       )) || 
                      ( DSAT_CM_REQ_ID == event_ptr->event_info[stack_id].cm_req_id ) )
                 {
                   /*Process LTE information */
                   result_tmp = cmif_process_cm_reg_data(event_ptr, stack_id,
                                                          subs_info);
                 }
                 if ( event_ptr->event_info[stack_id].changed_fields & DSAT_SS_MOBILITY_MGMT_MASK )
                 {
                   (void)dsatetsicmif_process_cm_mm_info_data(&event_ptr->event_info[stack_id].mm_info, subs_info);
                 }
                 break;
              case DSAT_SUB_NO_SRV:
                 if ( ( event_ptr->event_info[stack_id].changed_fields & 
                      (DSAT_SS_SRV_CHG_MASK 
#ifdef FEATURE_GSM_RR_CELL_CALLBACK 
                       | DSAT_SS_CELL_INFO_MASK 
#endif /* FEATURE_GSM_RR_CELL_CALLBACK */
                       )) || 
                      ( DSAT_CM_REQ_ID == event_ptr->event_info[stack_id].cm_req_id ) )
                 {
#ifdef FEATURE_SGLTE
                    /* Keep GSM information at MAX -1 index for SGLTE*/
                    cmif_process_cm_hybr_reg_data(event_ptr, stack_id, dsatcmif_get_sglte_hybr_subs_info());
#endif /* FEATURE_SGLTE */
                 }
                 if ( event_ptr->event_info[stack_id].changed_fields & DSAT_SS_MOBILITY_MGMT_MASK )
                 {
#ifdef FEATURE_SGLTE
                     dsatetsicmif_process_cm_mm_info_net_reg(&event_ptr->event_info[stack_id].mm_info, dsatcmif_get_sglte_hybr_subs_info());
#endif /* FEATURE_SGLTE */
                 }
                 break;
              default:
                 break;
              }
        }
           /*check the result from both the stack */
        if (DSAT_ASYNC_EVENT != result_tmp)
        {
          result = result_tmp;
        }
      }
#ifdef FEATURE_DSAT_EXTENDED_CMD
      switch(event_ptr->event_info[DSAT_STACK_MAIN].sys_mode)
      {
         /*Process 3GPP2 information*/
         case SYS_SYS_MODE_CDMA:
         case SYS_SYS_MODE_HDR:
            if ( event_ptr->event_info[DSAT_STACK_MAIN].changed_fields &
                 ( DSAT_SS_SYS_MODE_MASK |
                   DSAT_SS_HDR_SRV_STATUS_MASK ))
            {
               cmif_report_mode_3gpp2(&event_ptr->event_info[DSAT_STACK_MAIN]);
            }
         break;
         case SYS_SYS_MODE_NO_SRV:
         if ( (event_ptr->event_info[DSAT_STACK_MAIN].changed_fields &
            ( DSAT_SS_SYS_MODE_MASK |
              DSAT_SS_HDR_SRV_STATUS_MASK )) && (event_ptr->subs == dsat_qcsimapp_info[CDMA_SUBS].subs_id) )
          {
              cmif_report_mode_3gpp2(&event_ptr->event_info[DSAT_STACK_MAIN]);
          }
          break;
      }
#endif /* FEATURE_DSAT_EXTENDED_CMD */
      
#ifdef FEATURE_LTE_TO_1X
     if ( event_ptr->event_info[DSAT_STACK_MAIN].changed_fields &
          DSAT_SS_CSFB_CALL_ACTIVE_MASK )
     {
        is_csfb_call_active = event_ptr->event_info[DSAT_STACK_MAIN].is_csfb_call_active;
        DS_AT_MSG1_HIGH("voice_support_on_lte updated %d",is_csfb_call_active);
     }
#endif /* FEATURE_LTE_TO_1X */

#if defined(FEATURE_DSAT_SP_CMDS)
     cmif_determine_spservice(&event_ptr->event_info[DSAT_STACK_MAIN]);
#endif /* defined(FEATURE_DSAT_SP_CMDS) */
    }
    break;

    case CM_SS_EVENT_RSSI:
    {
      rssi_enqueue = DSAT_RSSI;

      /* Pre process RSSI data */
      for ( stack_id = DSAT_STACK_MAIN; stack_id < event_ptr->number_of_stacks; stack_id++ )
      {
        if(DSAT_STACK_MAIN == stack_id)
        {
          switch(event_ptr->event_info[stack_id].sys_mode)
          {
            case SYS_SYS_MODE_NO_SRV:
            case SYS_SYS_MODE_GSM:
            case SYS_SYS_MODE_WCDMA:
            case SYS_SYS_MODE_GW:
            case SYS_SYS_MODE_LTE:
            case SYS_SYS_MODE_TDS:
              cmif_process_rssi_info_3gpp(event_ptr, stack_id, subs_info);
            break;
           default:
             break;
          }
        }
        else
        {
          if(subs_mode == DSAT_SUB_SVLTE)
          {
            cmif_process_rssi_info_3gpp(event_ptr, stack_id, subs_info);
          }
        }
      }
      switch(event_ptr->event_info[DSAT_STACK_MAIN].sys_mode)
      {
        case SYS_SYS_MODE_NO_SRV:
          if(event_ptr->subs == dsat_qcsimapp_info[CDMA_SUBS].subs_id)
          {
            cmif_process_rssi_info_3gpp2(&event_ptr->event_info[DSAT_STACK_MAIN]);
          }
          break;
        case SYS_SYS_MODE_CDMA:
        case SYS_SYS_MODE_HDR:
          cmif_process_rssi_info_3gpp2(&event_ptr->event_info[DSAT_STACK_MAIN]);
          break;
        default:
          break;
      }

#ifdef FEATURE_DSAT_ETSI_MODE
#ifdef FEATURE_WCDMA_L1_HS_CQI_STAT
     /*Command is not DSDS compliant:-  Support for  Primary SUBS only*/
     if(event_ptr->subs == dsat_qcsimapp_info[PRIMA_SUBS].subs_id)
     {
       cmif_process_cqi_info(&event_ptr->event_info[DSAT_STACK_MAIN]);
     }
#endif /* FEATURE_WCDMA_L1_HS_CQI_STAT */
#endif /* FEATURE_DSAT_ETSI_MODE */
    }
    break;

    case CM_SS_EVENT_INFO:
    {
      if ( TRUE == dsat_sysinfo_cmd.cmd_pending )
      {
        cmif_format_sysinfo_response( msg_ptr );
        result = DSAT_OK;
      }
      else if (CHECK_PENDING(DSAT_VENDOR_QCSYSMODE_IDX, 0, DSAT_PENDING_TRUE))
      {
        SET_PENDING(DSAT_VENDOR_QCSYSMODE_IDX, 0, DSAT_PENDING_FALSE);

        result = dsatvend_process_qcsysmode_cmd( msg_ptr );
      }
    }
    break;

    case CM_SS_EVENT_HDR_RSSI:
    {
      rssi_enqueue = DSAT_HDR_RSSI;

      cmif_process_rssi_info_3gpp2(&event_ptr->event_info[DSAT_STACK_MAIN]);
    }
    break;

    case CM_SS_EVENT_EMERG_NUM_LIST:
    {
      /* We support only main stack emergency list :TBD  Hybrid stack support*/
      if (NULL != event_ptr->event_info[DSAT_STACK_MAIN].emerg_num_list)
      {
        /*  +CEN command is 3GPP specific command */
        if (!(event_ptr->event_info[DSAT_STACK_MAIN].sys_mode == SYS_SYS_MODE_CDMA || 
                 event_ptr->event_info[DSAT_STACK_MAIN].sys_mode == SYS_SYS_MODE_HDR))
        {
          cmif_process_emerg_list(&event_ptr->event_info[DSAT_STACK_MAIN], subs_info);
        }
        /* Free the memory allocated in call back */
        dsatutil_free_memory(event_ptr->event_info[DSAT_STACK_MAIN].emerg_num_list);
      }
    }
    break;

    case CM_SS_EVENT_REG_REJECT:
    {
      for ( stack_id = DSAT_STACK_MAIN; stack_id < event_ptr->number_of_stacks; stack_id++ )
      {
        if(DSAT_STACK_MAIN == stack_id)
        {
          switch(event_ptr->event_info[stack_id].sys_mode)
          {
            case SYS_SYS_MODE_NO_SRV:
            case SYS_SYS_MODE_GSM:
            case SYS_SYS_MODE_WCDMA:
            case SYS_SYS_MODE_GW:
            case SYS_SYS_MODE_LTE:
            case SYS_SYS_MODE_TDS:
              dsatetsicall_creg_state_machine( event_ptr, stack_id,subs_info);
            break;
           default:
             break;
          }
        }
        else
        {
          if(subs_mode == DSAT_SUB_SGLTE)
          {
#ifdef FEATURE_SGLTE
            subs_info = dsatcmif_get_sglte_hybr_subs_info();
            dsatetsicall_creg_state_machine( event_ptr, stack_id, subs_info );
#endif /*FEATURE_SGLTE*/
          }
          else if(subs_mode == DSAT_SUB_SVLTE)
          {
            dsatetsicall_creg_state_machine( event_ptr, stack_id, subs_info );
          }
          else if(subs_mode == DSAT_SUB_NO_SRV )
          {
            dsatetsicall_creg_state_machine( event_ptr, stack_id, subs_info );
#ifdef FEATURE_SGLTE
            subs_info = dsatcmif_get_sglte_hybr_subs_info();
            dsatetsicall_creg_state_machine( event_ptr, stack_id, subs_info );
#endif /* FEATURE_SGLTE */
          }
        }
      }
    }
    break;

    default:
      DS_AT_MSG0_ERROR("Unexpected CM Serving System event");
      break;
  }

  if ( rssi_enqueue )
  {
#ifdef IMAGE_QDSP6_PROC
    rex_enter_crit_sect(&dsat_rssi_crit_sect);
#else
    INTLOCK();
#endif /* IMAGE_QDSP6_PROC*/
    dsat_rssi_enqueue = dsat_rssi_enqueue & ~rssi_enqueue;
#ifdef IMAGE_QDSP6_PROC
    rex_leave_crit_sect(&dsat_rssi_crit_sect);
#else
    INTFREE();
#endif /* IMAGE_QDSP6_PROC*/
  }

  return result;
} /* dsatcmif_cm_ss_event_handler */

/*===========================================================================

FUNCTION   CMIF_MAP_SS_CHANGED_FIELDS

DESCRIPTION
  This function maps CM SS changed fields to ATCoP specific changed fields.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
LOCAL uint16 cmif_map_ss_changed_fields
(
  uint64                            cm_changed_fields_per_sub,
  uint64                            cm_changed_fields1,
  uint64                            cm_changed_fields2
)
{
  uint16 dsat_changed_fields = 0;
  uint8  idx;
  const dsatcmif_ss_mask_map_s_type *ss_mask_map;
  size_t                      ss_mask_map_size;

  DS_AT_MSG3_HIGH("CM change fields 0x%X 0x%X 0x%X",cm_changed_fields_per_sub,cm_changed_fields1,cm_changed_fields2);
/*
 Convert and Store  Subs  level changes field 
*/
    ss_mask_map       = &dsatcmif_ss_subs_mask_map[0];
    ss_mask_map_size  = ARR_SIZE( dsatcmif_ss_subs_mask_map );
                 
  for (idx = 0; idx < ss_mask_map_size; idx++)
  {
    if (cm_changed_fields_per_sub & ss_mask_map[idx].cm_ss_mask)
    {
      dsat_changed_fields |= ss_mask_map[idx].dsat_ss_mask;
    }
  }
  
  /*
     Convert and Store per Stack changes field1 
  */
    ss_mask_map       = &dsatcmif_ss_stack_mask_map[0];
    ss_mask_map_size  = ARR_SIZE( dsatcmif_ss_stack_mask_map );
 
    for (idx = 0; idx < ss_mask_map_size; idx++)
    {
      if (cm_changed_fields1 & ss_mask_map[idx].cm_ss_mask)
      {
        dsat_changed_fields |= ss_mask_map[idx].dsat_ss_mask;
      }
    }
    
/*
    Convert and Store per Stack changes field2 -We are not interested right Now.
    New chnage field will be added in change field 2 as change field one got exhausted 
 */
  
  return dsat_changed_fields;
} /* cmif_map_ss_changed_fields */

/*===========================================================================
FUNCTION  CMIF_SS_EVENT_MSIM_CB_FUNC

DESCRIPTION
  CM serving system status event callback function for multi-sim targets.
  CM sends SS_events per subscription 
  Single subscription can have mutiple stacks.

DEPENDENCIES
  None

RETURNS
  None

SIDE EFFECTS
  Adds command in DS command buffer

===========================================================================*/
/*ARGSUSED*/
LOCAL void cmif_ss_event_msim_cb_func
(
  cm_ss_event_e_type            event,            /* Event ID              */
  const cm_mm_msim_ss_info_s_type   *event_ptr         /* Pointer to Event info */
)
{
  boolean               post_ss_event = FALSE;
  dsat_num_item_type    rssi_enqueue = 0;
  dsat_num_item_type    local_dsat_rssi_enqueue;
  boolean               local_cm_msg_wm_full;
  dsat_cm_msg_s_type   *msg_ptr = NULL;
  ds_at_ss_info_s_type *info_ptr = NULL;
  dsm_item_type        *dsm_item_ptr = NULL;
  dsat_stack_id_e_type  stack_id;

  /* Invalid Number of stacks */
  if( NULL == event_ptr ||
      0 == event_ptr->number_of_stacks)
  {
    return;
  }
  
  DS_AT_MSG3_HIGH("In cmif_ss_event_msim_cb_func: event  %d CM Subs ID  %d number of stack %d ",event, event_ptr->asubs_id ,event_ptr->number_of_stacks);
  /* Allocate memory for the message. This will be freed at the end of the function.
     So there is no return until the end of the function. */
  msg_ptr = dsat_alloc_memory(sizeof(dsat_cm_msg_s_type), FALSE);

  msg_ptr->msg.ss_event.number_of_stacks = event_ptr->number_of_stacks;
  msg_ptr->msg.ss_event.subs             = event_ptr->asubs_id;
  
    switch (event)
    {
      case CM_SS_EVENT_SRV_CHANGED:
  /*Unified CM change fields into ATCoP change fields*/
      DS_AT_MSG1_MED("sys_sel_pref_req_id = %d", event_ptr->sys_sel_pref_req_id);
      for( stack_id = DSAT_STACK_MAIN; (stack_id < event_ptr->number_of_stacks) && (stack_id < DSAT_STACK_MAX); stack_id++ )
      {
        info_ptr = &msg_ptr->msg.ss_event.event_info[stack_id];
        info_ptr->changed_fields =
          cmif_map_ss_changed_fields( event_ptr->changed_fields_subs,
                                      event_ptr->stack_info[stack_id].changed_fields,
                                      event_ptr->stack_info[stack_id].changed_fields2 );
           

        if ( ( 0 != info_ptr->changed_fields ) ||
             ( DSAT_CM_REQ_ID == event_ptr->sys_sel_pref_req_id ) )
        {
          post_ss_event = TRUE;
        }
      }
        break;

      case CM_SS_EVENT_RSSI:
        rssi_enqueue = DSAT_RSSI;
        post_ss_event = TRUE;
        break;

      case CM_SS_EVENT_INFO:
        post_ss_event = TRUE;
        break;

      case CM_SS_EVENT_HDR_RSSI:
        rssi_enqueue = DSAT_HDR_RSSI;
        post_ss_event = TRUE;
        break;

      case CM_SS_EVENT_EMERG_NUM_LIST:
        if ( ( event_ptr->stack_info[DSAT_STACK_MAIN].emerg_num_list.num_list_len > 0 ) &&
             ( event_ptr->stack_info[DSAT_STACK_MAIN].emerg_num_list.num_list_len <= CM_MAX_EMERGENCY_NUM_COUNT ) )
        {
          info_ptr = &msg_ptr->msg.ss_event.event_info[DSAT_STACK_MAIN];
          info_ptr->emerg_num_list =
            dsat_alloc_memory(sizeof(cm_emerg_num_list_s_type), TRUE);

          if (NULL != info_ptr->emerg_num_list)
          {
            dsatutil_memscpy(info_ptr->emerg_num_list,
                             sizeof(cm_emerg_num_list_s_type),
                             &event_ptr->stack_info[DSAT_STACK_MAIN].emerg_num_list,
                             sizeof(cm_emerg_num_list_s_type));

            post_ss_event = TRUE;
          }
        }

        break;

      case CM_SS_EVENT_REG_REJECT:
        post_ss_event = TRUE;
        break;

      default:
        break;
    }

  if ( rssi_enqueue )
  {
#ifdef IMAGE_QDSP6_PROC
    rex_enter_crit_sect(&dsat_rssi_crit_sect);
#else
    INTLOCK();
#endif /* IMAGE_QDSP6_PROC*/
    local_dsat_rssi_enqueue = dsat_rssi_enqueue;
#ifdef IMAGE_QDSP6_PROC
    rex_leave_crit_sect(&dsat_rssi_crit_sect);
#else
    INTFREE();
#endif /* IMAGE_QDSP6_PROC*/

    /* Do not post multiple RSSI events (only one required) */
    if ( local_dsat_rssi_enqueue & rssi_enqueue )
    {
      DS_AT_MSG0_MED("Dropping redundant RSSI event");
      post_ss_event = FALSE;
    }
  }

  if ( TRUE == post_ss_event )
  {
#ifdef IMAGE_QDSP6_PROC
    rex_enter_crit_sect(&dsat_cm_wm_crit_sect);
#else
    INTLOCK();
#endif /* IMAGE_QDSP6_PROC*/
    local_cm_msg_wm_full = dsat_cm_msg_wm_full;
#ifdef IMAGE_QDSP6_PROC
    rex_leave_crit_sect(&dsat_cm_wm_crit_sect);
#else
    INTFREE();
#endif /* IMAGE_QDSP6_PROC*/

    /* Ensure there is capacity in message watermark for new one */
    if( FALSE == local_cm_msg_wm_full )
    {
      /* Populate the message from CM event */
      msg_ptr->cmd = DS_AT_CM_SS_INFO_CMD;
      msg_ptr->msg.ss_event.event = event;

      for(stack_id=DSAT_STACK_MAIN;(stack_id<event_ptr->number_of_stacks) && (stack_id < DSAT_STACK_MAX) ;stack_id++)
      {
        info_ptr = &msg_ptr->msg.ss_event.event_info[stack_id];
        
        info_ptr->is_operational = event_ptr->stack_info[stack_id].is_operational;
        info_ptr->roam_status = event_ptr->stack_info[stack_id].roam_status;
        info_ptr->hdr_roam_status = event_ptr->stack_info[stack_id].roam_status;
        info_ptr->srv_status = event_ptr->stack_info[stack_id].srv_status;
        info_ptr->srv_domain = event_ptr->stack_info[stack_id].srv_domain;
        info_ptr->rssi = event_ptr->stack_info[stack_id].rssi;
        info_ptr->sys_id = event_ptr->stack_info[stack_id].sys_id;
        dsatutil_memscpy(&info_ptr->mm_info,
                         sizeof(sys_mm_information_s_type),
                         &event_ptr->stack_info[stack_id].mode_info.gw_info.mm_information,
                         sizeof(sys_mm_information_s_type));
        info_ptr->sim_state = event_ptr->stack_info[stack_id].mode_info.gw_info.sim_state;
        dsatutil_memscpy(&info_ptr->cell_srv_ind,
                         sizeof(cm_cell_srv_ind_s_type),
                         &event_ptr->stack_info[stack_id].cell_srv_ind,
                         sizeof(cm_cell_srv_ind_s_type));
        
        info_ptr->sys_mode = event_ptr->stack_info[stack_id].sys_mode;
        switch(info_ptr->sys_mode)
        {
          case SYS_SYS_MODE_WCDMA:
            info_ptr->csg_id = event_ptr->stack_info[stack_id].rat_mode_info.wcdma_mode_info.csg_info.csg_id;
          break;
          case SYS_SYS_MODE_LTE:
            info_ptr->csg_id = event_ptr->stack_info[stack_id].rat_mode_info.lte_mode_info.csg_info.csg_id;
          break;
          default:
            info_ptr->csg_id = SYS_CSG_ID_INVALID;
          break;
        }
        info_ptr->hdr_hybrid = event_ptr->stack_info[stack_id].rat_mode_info.hdr_mode_info.hdr_hybrid;
        info_ptr->hdr_srv_status = event_ptr->stack_info[stack_id].srv_status;
        info_ptr->hdr_active_prot = event_ptr->stack_info[stack_id].rat_mode_info.hdr_mode_info.hdr_active_prot;
        
        info_ptr->ps_data_suspend = event_ptr->ps_data_suspend;
        info_ptr->is_sys_forbidden = event_ptr->stack_info[stack_id].is_sys_forbidden;
        info_ptr->srv_capability = event_ptr->stack_info[stack_id].srv_capability;
        info_ptr->cm_req_id = event_ptr->sys_sel_pref_req_id;
#ifdef FEATURE_LTE_TO_1X
        info_ptr->is_csfb_call_active = event_ptr->is_csfb_call_active;
#endif /* FEATURE_LTE_TO_1X */
#ifdef FEATURE_DSAT_LTE
        info_ptr->lte_tac = event_ptr->stack_info[stack_id].rat_mode_info.lte_mode_info.tac;
       info_ptr->lte_rac_or_mme_code = event_ptr->stack_info[stack_id].rat_mode_info.lte_mode_info.rac_or_mme_code;
#endif /* FEATURE_DSAT_LTE */
#ifdef FEATURE_GSM_RR_CELL_CALLBACK
        dsatutil_memscpy(&info_ptr->cell_info,
                         sizeof(sys_cell_info_s_type),
                         &event_ptr->stack_info[stack_id].cell_info,
                         sizeof(sys_cell_info_s_type));
#endif /* FEATURE_GSM_RR_CELL_CALLBACK */
        info_ptr->dsatcmif_signal_reporting.rssi = event_ptr->stack_info[stack_id].rssi;
        info_ptr->dsatcmif_signal_reporting.hdr_rssi = event_ptr->stack_info[stack_id].rssi;
        info_ptr->dsatcmif_signal_reporting.rssi2 = event_ptr->stack_info[stack_id].rssi2;
        info_ptr->dsatcmif_signal_reporting.rscp = event_ptr->stack_info[stack_id].rscp;
        info_ptr->dsatcmif_signal_reporting.bit_err_rate = 
          event_ptr->stack_info[stack_id].mode_info.gw_info.bit_err_rate;
#ifdef FEATURE_UMTS_REPORT_ECIO
        info_ptr->dsatcmif_signal_reporting.ecio = event_ptr->stack_info[stack_id].ecio;
#endif /* FEATURE_UMTS_REPORT_ECIO */
#ifdef FEATURE_UMTS_SIR_PATHLOSS
        info_ptr->dsatcmif_signal_reporting.pathloss = event_ptr->stack_info[stack_id].pathloss;
        info_ptr->dsatcmif_signal_reporting.sir = event_ptr->stack_info[stack_id].sir;
#endif /* FEATURE_UMTS_SIR_PATHLOSS */
        info_ptr->dsatcmif_signal_reporting.sys_mode = event_ptr->stack_info[stack_id].sys_mode;
        info_ptr->dsatcmif_signal_reporting.hdr_hybrid = event_ptr->stack_info[stack_id].rat_mode_info.hdr_mode_info.hdr_hybrid;
#ifdef FEATURE_DSAT_ETSI_MODE
#ifdef FEATURE_WCDMA_L1_HS_CQI_STAT
        dsatutil_memscpy(&info_ptr->dsat_cqi_info,
                         sizeof(cm_cqi_info_s_type),
                         &event_ptr->stack_info[stack_id].cqi_info,
                         sizeof(cm_cqi_info_s_type));
#endif /* FEATURE_WCDMA_L1_HS_CQI_STAT  */
#endif /* FEATURE_DSAT_ETSI_MODE */
        dsatutil_memscpy(&info_ptr->reg_reject_info,
                         sizeof(cm_reg_reject_info_s_type),
                         &event_ptr->stack_info[stack_id].reg_reject_info,
                         sizeof(cm_reg_reject_info_s_type));
      }
      
      /* Post message to CM watermark */
      dsm_item_ptr = dsat_dsm_create_packet(msg_ptr, sizeof(dsat_cm_msg_s_type), TRUE);
      if (NULL != dsm_item_ptr)
      {
        /* Add to queue and raise subtask signal */
        dsm_enqueue(&dsat_cm_msg_wm, &dsm_item_ptr);

        if ( rssi_enqueue )
        {
          /* Flag one RSSI event in queue */
#ifdef IMAGE_QDSP6_PROC
          rex_enter_crit_sect(&dsat_rssi_crit_sect);
#else
          INTLOCK();
#endif /* IMAGE_QDSP6_PROC*/
          dsat_rssi_enqueue = dsat_rssi_enqueue | rssi_enqueue;
#ifdef IMAGE_QDSP6_PROC
          rex_leave_crit_sect(&dsat_rssi_crit_sect);
#else
          INTFREE();
#endif /* IMAGE_QDSP6_PROC*/
        }
      }
      else
      {
        post_ss_event = FALSE;
      }
    }
    else
    {
      /* Watermark flow control engaged */
      DS_AT_MSG1_ERROR("CM message queue full - dropping event: %d", event);
      post_ss_event = FALSE;
    }
  }

  /* At this point, post_ss_event can be TRUE only when the message is successfully
     posted and in all other cases it will be FALSE. */
  if ( FALSE == post_ss_event )
  {
    for ( stack_id = DSAT_STACK_MAIN; stack_id < event_ptr->number_of_stacks; stack_id++ )
    {
      if (NULL != msg_ptr->msg.ss_event.event_info[stack_id].emerg_num_list)
      {
        dsatutil_free_memory( msg_ptr->msg.ss_event.event_info[stack_id].emerg_num_list );
      }
    }
  }

  dsatutil_free_memory( msg_ptr );

  return;
} /* cmif_ss_event_msim_cb_func */

/*===========================================================================
FUNCTION  CMIF_FORMAT_SYSINFO_RESPONSE

DESCRIPTION
  This function handles the Async response for the ^SYSINFO command

DEPENDENCIES
  None

RETURNS
  None

SIDE EFFECTS
  Adds command in DS command buffer

===========================================================================*/
LOCAL void cmif_format_sysinfo_response
(
  dsat_cm_msg_s_type    * msg_ptr
)
{
  dsm_item_type                 *res_buff_ptr = NULL;
  sys_sys_mode_e_type           sys_mode;
  uint16                        sim_state  = DSAT_SYSINFO_ROMSIM;
  sys_srv_status_e_type         srv_status;
  sys_srv_domain_e_type         srv_domain;
  sys_roam_status_e_type        roam_status;
  ds_at_ss_info_s_type          *info_ptr = NULL;
  dsat_num_item_type            temp_mode = AT_MODE_NONE;
#ifdef FEATURE_DSAT_EXTENDED_CMD
  dsatcmif_servs_state_ss_info  *ph_ss_val = NULL;
#endif/*FEATURE_DSAT_EXTENDED_CMD*/

  info_ptr = &msg_ptr->msg.ss_event.event_info[DSAT_STACK_MAIN];
  dsat_sysinfo_cmd.cmd_pending = FALSE;
  res_buff_ptr = dsat_dsm_new_buffer(DSM_DS_SMALL_ITEM_POOL, FALSE);
  srv_status  = info_ptr->srv_status;
  sys_mode    = info_ptr->sys_mode;
  roam_status = info_ptr->roam_status;
  srv_domain  = info_ptr->srv_domain;
  if( CDMA_CMD_MODE == dsatcmdp_get_operating_cmd_mode() )
  {
    if ( TRUE == info_ptr->hdr_hybrid )
    {
      /* Merge srv_status with hdr_srv_status */
      if ( (SYS_SRV_STATUS_PWR_SAVE == srv_status) ||
           (SYS_SRV_STATUS_SRV == info_ptr->hdr_srv_status) )
      {
        srv_status = info_ptr->hdr_srv_status;
      }

      if ( SYS_SRV_STATUS_SRV == info_ptr->hdr_srv_status )
      {
        /* Include HDR in the system mode if HDR has service */
        if ( SYS_SYS_MODE_CDMA == sys_mode )
        {
          /*Type casting it to sys mode e type as hybrid 
         is not available in sys mode type */
          sys_mode = (sys_sys_mode_e_type)DSAT_SYS_HYBRID;
        }
        else
        {
          sys_mode = SYS_SYS_MODE_HDR;
        }
      }
#ifdef FEATURE_DSAT_EXTENDED_CMD
      /* to be used in SMS in HDR mode */
       if(DSAT_FAILURE == dsatutil_get_base_addr(DSAT_SS_PH_SS_VALS,(void **)&ph_ss_val,DSAT_MS_FIRST_SUBS))
       {
          return;
       }
       ph_ss_val->dsat_sys_mode = sys_mode;
#endif /* FEATURE_DSAT_EXTENDED_CMD */ 
       /* Update roaming status - we want to display "not roaming"
             if & only if both CDMA & HDR are not roaming */
      if ( (SYS_ROAM_STATUS_OFF == roam_status) &&
           (SYS_ROAM_STATUS_OFF != info_ptr->hdr_roam_status) )
      {
        roam_status = info_ptr->hdr_roam_status;
      }
    }
#ifdef FEATURE_DSAT_EXTENDED_CMD
    /* Only display on or off for roaming status */
    if ( SYS_ROAM_STATUS_OFF != roam_status )
    {
      roam_status = SYS_ROAM_STATUS_ON;
    }
    /* Default roam status to OFF when no service */
    if ( SYS_SRV_STATUS_SRV != srv_status )
    {
      roam_status = SYS_ROAM_STATUS_OFF;
    }
#endif /* FEATURE_DSAT_EXTENDED_CMD */

    switch( dsat_sysinfo_cmd.dsat_rtre_config )
    {
      case DSAT_RTRE_CONTROL_NV:
        sim_state = DSAT_SYSINFO_ROMSIM;
        break;
      case DSAT_RTRE_CONTROL_RUIM:
        if ( DSAT_SIM_VALID_PRESENT == dsat_sysinfo_cmd.sim_valid[CDMA_SUBS] )
        {
          sim_state = DSAT_SYSINFO_UIM;
        }
        else
        {
          sim_state = DSAT_SYSINFO_NOUIM;
        }
        break;
      case DSAT_RTRE_CONTROL_NONE:
      default:
        DS_AT_MSG0_ERROR("RTRE control has incorrect value");
    }
    res_buff_ptr->used = snprintf((char *)res_buff_ptr->data_ptr,
                            res_buff_ptr->size,
                            "^SYSINFO:%d,%d,%d,%d,%d",
                            srv_status,
                            srv_domain,
                            roam_status,
                            sys_mode,
                            sim_state);

    dsatcmdp_handle_async_cmd_rsp(res_buff_ptr, DSAT_COMPLETE_RSP);
  }
  else
  {
    if ( DSAT_SIM_VALID_PRESENT == dsat_sysinfo_cmd.sim_valid[dsat_get_qcsimapp_val()] )
    {
      sim_state = DSAT_SYSINFO_UIM;
    }
    else
    {
      sim_state = DSAT_SYSINFO_NOUIM;
    }
    temp_mode = sys_mode;
#ifdef FEATURE_TDSCDMA
   /* TDS mode in spec is different from SYS_SYS_MODE_TDS  */
    if(sys_mode == SYS_SYS_MODE_TDS)
    {
      temp_mode = AT_MODE_TDS;
    }
#endif /* FEATURE_TDSCDMA */
    res_buff_ptr->used = snprintf((char *)res_buff_ptr->data_ptr,
                        res_buff_ptr->size,
                        "^SYSINFO: %d,%d,%d,%d,%d",
                        srv_status,
                        srv_domain,
                        roam_status,
                        temp_mode,
                        sim_state);

    dsatcmdp_handle_async_cmd_rsp(res_buff_ptr, DSAT_COMPLETE_RSP);
  }
}/* cmif_format_sysinfo_response */
/*===========================================================================

FUNCTION  DSATCMIF_GET_CM_SS_INFO

DESCRIPTION
  This function issue query to Call Manager to get serving system (SS) info

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR :    if there was any problem in executing the command
    DSAT_OK :       if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatcmif_get_cm_ss_info ( void )
{
  dsat_result_enum_type result = DSAT_OK;

  DS_AT_MSG1_ERROR("Subs id %d ",dsat_qcsimapp_info[dsat_get_qcsimapp_val()].subs_id);
  if (TRUE != cm_ss_cmd_get_ss_info_msim (
                                          cmif_ss_cmd_cb_func,
                                          NULL,
                                          dsatcm_client_id,
                                          dsat_qcsimapp_info[dsat_get_qcsimapp_val()].subs_id
                                         ))

  {
    DS_AT_MSG0_ERROR("Could not request CM SS phone info");
    result = DSAT_ERROR;
  }
  
  return result;
} /* dsatcmif_get_cm_ss_info */



/*===========================================================================

FUNCTION  DSATCMIF_CHANGE_OPERATING_MODE

DESCRIPTION
  This function submits request to Call Manager to change the phone
  operating mode.  The results of the call are handled as asynch events.

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR :    if there was any problem in executing the command
    DSAT_ASYNC_CMD :  if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
/* ARGSUSED */
dsat_result_enum_type dsatcmif_change_operating_mode
(
  sys_oprt_mode_e_type  new_mode            /* Operating mode */
)
{
  dsat_result_enum_type result = DSAT_ASYNC_CMD;
  boolean cm_result;

  if (dsatcmif_get_oprt_mode() == new_mode)
  {
    DS_AT_MSG0_HIGH("Requested operating mode is same as current");
    return DSAT_OK;
  }

  DS_AT_MSG1_HIGH("Requesting CM operating mode change: %d",new_mode);
  cm_result = cm_ph_cmd_oprt_mode ( dsatcmif_ph_cmd_cb_func,
                                    NULL,
                                    dsatcm_client_id,
                                    new_mode );

  if (!cm_result)
  {
    /* ERROR tell user about it */
    result = DSAT_ERROR;
  }

  return result;
} /* dsatcmif_change_operating_mode */



/*===========================================================================
FUNCTION  DSATCMIF_PH_CMD_CB_FUNC

DESCRIPTION
  CM phone command callback function

DEPENDENCIES
  None

RETURNS
  None

SIDE EFFECTS
  Adds command in DS command buffer

===========================================================================*/
/* ARGSUSED */
void dsatcmif_ph_cmd_cb_func 
(
  void                         *data_ptr,         /* Data block pointer    */
  cm_ph_cmd_e_type              cmd,              /* Command ID            */
  cm_ph_cmd_err_e_type          cmd_err           /* Command error code    */
)
{
  ds_cmd_type * cmd_buf;
  ds_at_cm_ph_cmd_type*      ph_cmd = NULL;
#ifdef FEATURE_DSAT_ETSI_MODE
  dsati_mode_e_type current_mode = dsatcmdp_get_current_mode();
#endif /* FEATURE_DSAT_ETSI_MODE */

  DS_AT_MSG3_MED("dsatcmif_ph_cmd_cb_func: data_ptr = %p, cmd = %d, cmd_err = %d",
                  data_ptr, cmd, cmd_err);

  /* Filter supported commands */
  switch (cmd)
  {
  case CM_PH_CMD_SET_PREFERRED_NETWORKS:
#ifdef FEATURE_DSAT_ETSI_MODE
    if ( IS_ETSI_MODE(current_mode) )
    {
      /* send result to DS task */
      cmd_buf = dsat_get_cmd_buf(FALSE);
      /* send the message to ATCOP */
      cmd_buf->hdr.cmd_id = DS_AT_CM_PH_CMD;
      ph_cmd = (ds_at_cm_ph_cmd_type *)dsat_alloc_memory(
                                         sizeof(ds_at_cm_ph_cmd_type), FALSE);
      ph_cmd->cmd = cmd;
      ph_cmd->cmd_err = cmd_err;
      cmd_buf->cmd.cm_cmd.sub_cmd = ph_cmd;
      ds_put_cmd(cmd_buf);
    }
#endif /* FEATURE_DSAT_ETSI_MODE */
    break;
  case CM_PH_CMD_INFO_GET:
  case CM_PH_CMD_SYS_SEL_PREF:
  case CM_PH_CMD_SUBSCRIPTION_NOT_AVAILABLE:
  case CM_PH_CMD_SUBSCRIPTION_AVAILABLE:
  case CM_PH_CMD_OPRT_MODE:
  case CM_PH_CMD_GET_NETWORKS:
#ifdef FEATURE_DSAT_LTE
  case CM_PH_CMD_GET_DRX_REQ:
  case CM_PH_CMD_SET_DRX_REQ:
#endif/* FEATURE_DSAT_LTE*/
    /* Verify there is no error condition */
    if (CM_PH_CMD_ERR_NOERR != cmd_err)
    {
      /* Send error to DS task context */
      cmd_buf = dsat_get_cmd_buf(FALSE);
      /* send the message to ATCOP */
      cmd_buf->hdr.cmd_id = DS_AT_CM_PH_CMD;
      ph_cmd = (ds_at_cm_ph_cmd_type *)dsat_alloc_memory(
                                         sizeof(ds_at_cm_ph_cmd_type), FALSE);
      ph_cmd->cmd = cmd;
      ph_cmd->cmd_err = cmd_err;
      cmd_buf->cmd.cm_cmd.sub_cmd = ph_cmd;
      ds_put_cmd(cmd_buf);
    }
    break;
  case CM_PH_CMD_RESET_ACM:
  case CM_PH_CMD_SET_ACMMAX:
  case CM_PH_CMD_MRU_UPDATE:
    /* Send it to DS task context */
    cmd_buf = dsat_get_cmd_buf(FALSE);
    /* send the message to ATCOP */
    cmd_buf->hdr.cmd_id = DS_AT_CM_PH_CMD;
    ph_cmd = (ds_at_cm_ph_cmd_type *)dsat_alloc_memory(
                                       sizeof(ds_at_cm_ph_cmd_type), FALSE);
    ph_cmd->cmd = cmd;
    ph_cmd->cmd_err = cmd_err;
    cmd_buf->cmd.cm_cmd.sub_cmd = ph_cmd;
    ds_put_cmd(cmd_buf);
    break;
  case CM_PH_CMD_READ_MRU:
    /* Send it to DS task context */
    cmd_buf = dsat_get_cmd_buf(FALSE);
    /* send the message to ATCOP */
    cmd_buf->hdr.cmd_id = DS_AT_CM_PH_CMD;
    ph_cmd = (ds_at_cm_ph_cmd_type *)dsat_alloc_memory(
                                       sizeof(ds_at_cm_ph_cmd_type), FALSE);
    ph_cmd->cmd = cmd;
    ph_cmd->cmd_err = cmd_err;

    if ( data_ptr == NULL )
    {
      DS_AT_MSG0_ERROR("Invalid data pointer");
      ph_cmd->cmd_err = CM_PH_CMD_ERR_NO_BUF_L;
    }
    else
    {
      (void) dsatutil_memscpy((void*)&ph_cmd->mru_table_entry, 
                             sizeof(cm_mmode_mru_table_entry_u_type),
                            (void*)data_ptr,
              sizeof(cm_mmode_mru_table_entry_u_type) );
    }
    cmd_buf->cmd.cm_cmd.sub_cmd = ph_cmd;
    ds_put_cmd(cmd_buf);
  default:
    /* Do nothing for unsupported commands */
    break;
  }

  return;
} /* dsatcmif_ph_cmd_cb_func */


/*===========================================================================

FUNCTION  DSATCMIF_CM_PH_CMD_HANDLER

DESCRIPTION
  This function is the handler function for the CM phone-related commands

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR :    if there was any problem in executing the command
    DSAT_ASYNC_CMD :  if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
/* ARGSUSED */
dsat_result_enum_type dsatcmif_cm_ph_cmd_handler
(
  dsat_mode_enum_type   mode,                /* AT command mode            */
  ds_cmd_type         * cmd_ptr              /* DS Command pointer         */
)
{
  dsat_result_enum_type result = DSAT_ASYNC_CMD;
  ds_at_cm_ph_cmd_type*      ph_cmd = NULL;
#ifdef FEATURE_DSAT_ETSI_MODE
  dsati_mode_e_type current_mode = dsatcmdp_get_current_mode();
#endif /* FEATURE_DSAT_ETSI_MODE */
  dsatcmif_servs_state_ms_info     *ph_ss_ms_val = NULL;
  dsatetsicall_call_state_ms_info  *call_ms_val = NULL;
  dsatme_mmgsdi_state_ss_info      *me_ss_val = NULL;

  ASSERT( cmd_ptr != NULL );
  ph_cmd = (ds_at_cm_ph_cmd_type*)cmd_ptr->cmd.cm_cmd.sub_cmd;
 
  ASSERT( ph_cmd != NULL );

  DS_AT_MSG2_MED("dsatcmif_cm_ph_cmd_handler: cmd = %d, cmd_err = %d",
                 ph_cmd->cmd, ph_cmd->cmd_err);

  if(DSAT_FAILURE == dsatutil_get_base_addr(DSAT_SS_PH_MS_VALS,(void **)&ph_ss_ms_val,dsat_get_qcsimapp_val()) ||
     DSAT_FAILURE == dsatutil_get_base_addr(DSAT_CALL_MS_VALS, (void **)&call_ms_val,dsat_get_qcsimapp_val()) ||
     DSAT_FAILURE == dsatutil_get_base_addr(DSAT_MMGSDI_SS_VALS,(void **)&me_ss_val,DSAT_MS_FIRST_SUBS))
    {
      dsatutil_free_memory((void*)ph_cmd);
      return DSAT_ASYNC_EVENT;
    }
  switch (ph_cmd->cmd)
  {
  case CM_PH_CMD_SYS_SEL_PREF:
  case CM_PH_CMD_OPRT_MODE:
  case CM_PH_CMD_GET_NETWORKS:
  case CM_PH_CMD_INFO_GET:
    /* Verify there is no error condition */
    if (CM_PH_CMD_ERR_NOERR != ph_cmd->cmd_err)
    {
      if( CHECK_NOT_PENDING( DSAT_VENDOR_QCMRUE_IDX, 0, DSAT_PENDING_MRU_NONE ) )
      {
        SET_PENDING( DSAT_VENDOR_QCMRUE_IDX, 0, DSAT_PENDING_MRU_NONE );
      }
      if( CHECK_NOT_PENDING( DSAT_VENDOR_QCMRUC_IDX, 0, DSAT_PENDING_MRU_NONE ) )
      {
        SET_PENDING( DSAT_VENDOR_QCMRUC_IDX, 0, DSAT_PENDING_MRU_NONE );
      }
#ifdef FEATURE_DSAT_ETSI_MODE
      if ( IS_ETSI_MODE(current_mode) )
      {
        /* Clear flags on failure of attach write */
        if ( dsatcmif_attach_write != DSAT_CGATT_MAX )
        {
          dsatcmif_ss_evt_not_expected = FALSE;
          dsatcmif_attach_write = DSAT_CGATT_MAX;
        }
#if defined(FEATURE_DATA_GCSD) || defined(FEATURE_DATA_WCDMA)
        if ( (ph_ss_ms_val->dsat_ws46_res_type != DSAT_WS46_NONE) && 
             (CM_PH_CMD_INFO_GET == ph_cmd->cmd) )
        {
          ph_ss_ms_val->dsat_ws46_res_type = DSAT_WS46_NONE;
          ph_ss_ms_val->dsat_ws46_curr_val = DSAT_WS46_FUNC_MAX;
        }
#endif /* defined(FEATURE_DATA_GCSD) || defined(FEATURE_DATA_WCDMA) */
        if ( CM_PH_CMD_SYS_SEL_PREF == ph_cmd->cmd) 
        {
          if (DSAT_COPS_ASTATE_ABORT == ph_ss_ms_val->dsatetsicall_network_list.cmd_state)
          {
            /* Abort handler responsible for sending result code */
            /* COPS state will be updated by abort event handler */
            dsatutil_free_memory((void*)ph_cmd);
            return DSAT_ASYNC_EVENT;
          }
          if(DSAT_COPS_ASTATE_NULL != ph_ss_ms_val->dsatetsicall_network_list.cmd_state)
          {
            DSATUTIL_SET_VAL(DSATETSI_EXT_ACT_COPS_ETSI_IDX,dsat_get_qcsimapp_val(),0,0,
             (dsat_num_item_type)ph_ss_ms_val->dsatetsicall_network_list.previous_pref.mode,MIX_NUM_TYPE)
          }
          /* Asynch command processing is done */
          ph_ss_ms_val->dsatetsicall_network_list.cmd_state = DSAT_COPS_ASTATE_NULL;
          ph_ss_ms_val->dsatetsicall_network_list.cmd_idx = CMD_IDX_NONE;
        }
        /* Reset CACM/CAMM command */
        call_ms_val->dsat_acm_res_type = DSAT_ACM_NONE;
      }/* IS_ETSI_MODE() */
#endif /* FEATURE_DSAT_ETSI_MODE */
      if(CHECK_NOT_PENDING(DSAT_VENDOR_PREFMODE_IDX,0,DSAT_PENDING_PREFMODE_NONE))
      {
        SET_PENDING(DSAT_VENDOR_PREFMODE_IDX ,0, DSAT_PENDING_PREFMODE_NONE)
      }
      if (CHECK_NOT_PENDING(DSAT_VENDOR_QCBANDPREF_IDX,0,DSAT_PENDING_PREFMODE_NONE))
      {
        SET_PENDING(DSAT_VENDOR_QCBANDPREF_IDX ,0, DSAT_PENDING_PREFMODE_NONE)
      }

      if(CHECK_NOT_PENDING(DSAT_VENDOR_SYSCONFIG_IDX,0,DSAT_PENDING_SYSCONFIG_NONE))
      {
        SET_PENDING(DSAT_VENDOR_SYSCONFIG_IDX ,0, DSAT_PENDING_SYSCONFIG_NONE)
      }
      if(CHECK_NOT_PENDING(DSATETSI_EXT_ACT_CVMOD_ETSI_IDX ,0,DSAT_PENDING_FALSE) )
      {    
        SET_PENDING(DSATETSI_EXT_ACT_CVMOD_ETSI_IDX ,0, DSAT_PENDING_FALSE)
      }
      if(CHECK_NOT_PENDING(DSAT_VENDOR_QCNSP_IDX ,0, DSAT_PENDING_QCNSP_NONE))
      {
        SET_PENDING(DSAT_VENDOR_QCNSP_IDX ,0, DSAT_PENDING_QCNSP_NONE)
        result = DSAT_ERROR;
      }
      if (CHECK_PENDING(DSAT707_VEND_ACT_RESET_IDX, 0, DSAT_PENDING_RESET_WRITE))
      {
        SET_PENDING(DSAT707_VEND_ACT_RESET_IDX, 0, DSAT_PENDING_RESET_NONE);
        result = DSAT_ERROR;
      }
      if ( CHECK_NOT_PENDING(DSAT_EXT_CFUN_IDX,0,DSAT_PENDING_CFUN_NONE))
      {
        SET_PENDING(DSAT_EXT_CFUN_IDX ,0, DSAT_PENDING_CFUN_NONE)
        if (CM_PH_CMD_INFO_GET == ph_cmd->cmd)
        {
          DSATUTIL_SET_VAL(DSAT_EXT_CFUN_IDX,0,0,0,DSAT_ME_FUNC_MAX,MIX_NUM_TYPE)
          result = dsat_send_cme_error (DSAT_CME_PHONE_FAILURE);
        }
        else if (CM_PH_CMD_OPRT_MODE == ph_cmd->cmd)
        {
          if (CM_PH_CMD_ERR_OPRT_MODE_S == ph_cmd->cmd_err)
          {
            result = dsat_send_cme_error (DSAT_CME_OP_NOT_SUPPORTED);
          }
          else
          {
            result = dsat_send_cme_error (DSAT_CME_PHONE_FAILURE);
          }
        }
      }
      else
      {
        result = DSAT_ERROR;
      }
    }
    break;

  case CM_PH_CMD_SET_PREFERRED_NETWORKS:
#ifdef FEATURE_DSAT_ETSI_MODE
    if ( IS_ETSI_MODE(current_mode) && dsat_abort_plmn_listing == TRUE)
    {
      /* User aborted +CPOL command */
      dsat_abort_plmn_listing = FALSE;

      /* Abort handler responsible for sending error */
      dsatutil_free_memory((void*)ph_cmd);
      return DSAT_ASYNC_EVENT;
    }
#endif /* FEATURE_DSAT_ETSI_MODE */
    /* Deliberately falls through to case below */
  /*lint -fallthrough */
  case CM_PH_CMD_RESET_ACM:
  case CM_PH_CMD_SET_ACMMAX:
    /* Verify there is no error condition */
    if (CM_PH_CMD_ERR_NOERR == ph_cmd->cmd_err)
    {
      result = DSAT_OK;
    }
    else
    {
#ifdef FEATURE_DSAT_ETSI_MODE
      if ( IS_ETSI_MODE(current_mode) )
      {
        /* CM_PH_CMD_ERR_AOC_NOT_READY_S is valid only if CM_API_AOC_INFO 
          is defined. CM returns CM_PH_CMD_ERR_AOC_NOT_READY_S if SIM is busy
        */
        call_ms_val->dsat_acm_res_type = DSAT_ACM_NONE;
#ifdef CM_API_AOC_INFO
        if (CM_PH_CMD_ERR_AOC_NOT_READY_S == ph_cmd->cmd_err)
        {
          result = dsat_send_cme_error(DSAT_CME_SIM_BUSY);
        }
        else 
#endif /* CM_API_AOC_INFO */
        {
          result = dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
        }
      }
      else
#endif /* FEATURE_DSAT_ETSI_MODE */
      {
        result = DSAT_ERROR;
      }
    }
    break;
  case CM_PH_CMD_SUBSCRIPTION_AVAILABLE:
    if ( CM_PH_CMD_ERR_NOERR != ph_cmd->cmd_err )
    {
      if ( CM_PH_CMD_ERR_SUBSCRIPTION_AVAILABLE_S == ph_cmd->cmd_err )
      {
        if ( ph_ss_ms_val->net_pref.subscription_invoked == TRUE )
             /* To make sure this is not after phone boot up but after deregistration command */
        {
          result = cmif_subscription_available_process_cops(ph_ss_ms_val, dsat_get_qcsimapp_val());
          /* reset the net_pref structure and make the boolean false */
          ph_ss_ms_val->net_pref.subscription_invoked = FALSE; 
        }
      }
      else
      {
#ifdef FEATURE_DSAT_ETSI_MODE
      if ( IS_ETSI_MODE(current_mode) )
      {
        
        /* If there is an ERROR for SUBSCRIPTION AVAILABLE, then reset
           back subscription_invoked */
        if ( ph_ss_ms_val->net_pref.subscription_invoked == TRUE )
        {
          ph_ss_ms_val->net_pref.subscription_invoked = FALSE;
        }
        if ( DSAT_COPS_ASTATE_NULL == ph_ss_ms_val->dsatetsicall_network_list.cmd_state)
        {
          dsatutil_free_memory((void*)ph_cmd);
          return DSAT_ASYNC_EVENT;
        }
        ph_ss_ms_val->dsatetsicall_network_list.cmd_state = DSAT_COPS_ASTATE_NULL;
        ph_ss_ms_val->dsatetsicall_network_list.cmd_idx = CMD_IDX_NONE;
      }
#endif /* FEATURE_DSAT_ETSI_MODE */
      result = DSAT_ERROR;
    }
  }
    break;
  case CM_PH_CMD_SUBSCRIPTION_NOT_AVAILABLE:
    if ( CM_PH_CMD_ERR_NOERR != ph_cmd->cmd_err )
    {
      /* Error has occured while deregistration.  */
#ifdef FEATURE_DSAT_ETSI_MODE
      if ( IS_ETSI_MODE(current_mode) )
      {
        if( ( DSAT_COPS_ASTATE_NULL == ph_ss_ms_val->dsatetsicall_network_list.cmd_state ) ||
            ( DSAT_COPS_ASTATE_ABORT == ph_ss_ms_val->dsatetsicall_network_list.cmd_state) )
        {
          dsatutil_free_memory((void*)ph_cmd);
          return DSAT_ASYNC_EVENT;
        }
        else
        {
          DSATUTIL_SET_VAL(DSATETSI_EXT_ACT_COPS_ETSI_IDX,dsat_get_qcsimapp_val(),0,0,
             (dsat_num_item_type)ph_ss_ms_val->dsatetsicall_network_list.previous_pref.mode,MIX_NUM_TYPE)
        }
        ph_ss_ms_val->dsatetsicall_network_list.cmd_state = DSAT_COPS_ASTATE_NULL;
        ph_ss_ms_val->dsatetsicall_network_list.cmd_idx = CMD_IDX_NONE;
      }
#endif /* FEATURE_DSAT_ETSI_MODE */
      result = DSAT_ERROR;
    }
    break;
#ifdef FEATURE_DSAT_LTE
  case CM_PH_CMD_GET_DRX_REQ:
  case CM_PH_CMD_SET_DRX_REQ:
    SET_PENDING(DSAT_VENDOR_QCDRX_IDX, 0, DSAT_PENDING_QCDRX_NONE);
    result = DSAT_ERROR;
#endif/* FEATURE_DSAT_LTE*/
  case CM_PH_CMD_MRU_UPDATE:
    result = dsatvend_process_mru_update_result(ph_cmd->cmd_err);
    break;

  case CM_PH_CMD_READ_MRU:
    result = dsatvend_process_mru_read_result( ph_cmd->cmd_err,
                                               &ph_cmd->mru_table_entry );
    break;

  default:
    DS_AT_MSG1_ERROR("Unsupport CM PH command: %d",ph_cmd->cmd);
    result = DSAT_ERROR;
    break;
  }
  dsatutil_free_memory((void*)ph_cmd);
  return result;
} /* dsatcmif_cm_ph_cmd_handler */



/*===========================================================================
FUNCTION  DSATCMIF_CM_PH_EVENT_HANDLER

DESCRIPTION
  This function is the event handler invoked by CM in response to Phone
  group commands:
    - PS domain attach or detach triggered by +CGATT command
    - functionality level/operating mode (power setting)
    - Available/preferred network reporting
    - Phone preference changes

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR :    if there was any problem in executing the command
    DSAT_OK :       if it is a success.
    DSAT_ASYNC_EVENT : if success and no response required.

SIDE EFFECTS
  None

===========================================================================*/
/* ARGSUSED */
dsat_result_enum_type dsatcmif_cm_ph_event_handler
(
  dsat_mode_enum_type   mode,                /* AT command mode            */
  ds_cmd_type         * cmd_ptr              /* DS Command pointer         */
)
{
  dsat_result_enum_type result = DSAT_ASYNC_EVENT;
  ds_subs_e_type index = DS_FIRST_SUBS;
  dsat_result_enum_type result_subs[MAX_SUBS] = {
                                       DSAT_ASYNC_EVENT,
                                       DSAT_ASYNC_EVENT
#ifdef FEATURE_TRIPLE_SIM
                                      ,DSAT_ASYNC_EVENT
#endif /* FEATURE_TRIPLE_SIM */
                                      };
  boolean oprt_mode_changed = FALSE;
  dsat_num_item_type prev_cfun_val;
  dsat_mixed_param_val_type *temp_mix_type;

  boolean power_up_slot1 = FALSE;
  boolean power_up_slot2 = FALSE;
#ifdef FEATURE_TRIPLE_SIM
  boolean power_up_slot3 = FALSE;
#endif /* FEATURE_TRIPLE_SIM */
  dsati_mode_e_type current_mode = dsatcmdp_get_current_mode();
  ds_at_cm_ph_event_type*    ph_event = NULL;
#ifdef FEATURE_DSAT_LTE
  dsatcmif_eps_mode_e_type eps_mode = DSAT_EPS_MODE_MAX;
#endif /* FEATURE_DSAT_LTE */  
  ds_subs_e_type subs_info = DS_FIRST_SUBS;
  dsatcmif_servs_state_ss_info     *ph_ss_val = NULL;
  dsatcmif_servs_state_ms_info     *ph_ss_ms_val = NULL;
  dsatetsicall_call_state_ms_info  *call_ms_val = NULL;
  dsatme_mmgsdi_state_ss_info       *me_ss_val = NULL;
  
  ASSERT( cmd_ptr != NULL );

  ph_event = (ds_at_cm_ph_event_type*)cmd_ptr->cmd.cm_cmd.sub_cmd;
  ASSERT( ph_event != NULL );

  DS_AT_MSG3_HIGH("Processing CM PH event: %d, OPRT mode %d, device mode %d",
                        ph_event->event,
                        ph_event->event_info.oprt_mode,
                        ph_event->event_info.device_mode);

  if(DSAT_FAILURE == dsatutil_get_base_addr(DSAT_SS_PH_SS_VALS,(void **)&ph_ss_val,DSAT_MS_FIRST_SUBS))
  {
    dsatutil_free_memory((void*)ph_event);
    return DSAT_ASYNC_EVENT;
  }
  ph_ss_val->dsat_device_mode = ph_event->event_info.device_mode;

#ifdef FEATURE_DUAL_SIM
  if(ph_event->event == CM_PH_EVENT_DUAL_STANDBY_PREF)
  {
    /* Set QCSIMAPP value based on default data subscription and subs_id in QCSIMAPP info. */
    if(dsat_qcsimapp_info[DSAT_MMGSDI_1X_PRIM].subs_id == 
              ph_event->event_info.default_data_subs)
    {
      dsat_set_qcsimapp_val(PRIMA_SUBS);
    }
    else if(dsat_qcsimapp_info[DSAT_MMGSDI_GW_SECD].subs_id == 
               ph_event->event_info.default_data_subs)
    {
      dsat_set_qcsimapp_val(SECON_SUBS);
    }
#ifdef FEATURE_TRIPLE_SIM
    else if(dsat_qcsimapp_info[DSAT_MMGSDI_GW_TER].subs_id == 
               ph_event->event_info.default_data_subs)
    {
      dsat_set_qcsimapp_val(THIRD_SUBS);
    }
#endif
    else
    {
      dsat_set_qcsimapp_val(PRIMA_SUBS);
    }
    DS_AT_MSG2_HIGH("Default_data_subs = %d, qcsimapp = %d",
        ph_event->event_info.default_data_subs,
        dsat_get_qcsimapp_val());
  }
#endif /* FEATURE_DUAL_SIM */

#ifdef FEATURE_DUAL_SIM
  index = dsatcmif_update_subs_idx(cmd_ptr->cmd.cm_cmd.subs);
  if(index == DS_SUBS_MAX)
  {
    dsatutil_free_memory((void*)ph_event);
    return result;
  }
#else
  index = DS_FIRST_SUBS;
#endif /* FEATURE_DUAL_SIM */
if(index != 0)
  {  /*CB always kept information at 0th index.*/
   ph_event->event_info.available_networks[index] = 
             ph_event->event_info.available_networks[0];
   ph_event->event_info.user_pref_networks[index] = 
             ph_event->event_info.user_pref_networks[0];
   ph_event->event_info.aoc_info[index] = 
             ph_event->event_info.aoc_info[0];
   dsatutil_memscpy( (void*)&ph_event->event_info.pref_info[index], sizeof(ds_at_cm_ph_pref_type),
                     (void*)&ph_event->event_info.pref_info[0], sizeof(ds_at_cm_ph_pref_type) );
  }
#ifdef FEATURE_DSAT_ETSI_MODE 
  if ( (ph_event->event == CM_PH_EVENT_INFO ) 
       ||(ph_event->event == CM_PH_EVENT_SYS_SEL_PREF) 
       ||(ph_event->event == CM_PH_EVENT_SUBSCRIPTION_NOT_AVAILABLE ) 
       ||(ph_event->event == CM_PH_EVENT_SUBSCRIPTION_AVAILABLE )  
#ifdef FEATURE_SGLTE
       ||(ph_event->event == CM_PH_EVENT_SIM_AVAILABLE )       
#endif/* FEATURE_SGLTE*/
     )
  {
    if (DSAT_FAILURE == dsatutil_get_base_addr(DSAT_SS_PH_MS_VALS,(void **)&ph_ss_ms_val,index))
    {
      dsatutil_free_memory((void*)ph_event);
      return DSAT_ASYNC_EVENT;
    }

    ph_ss_ms_val->dsat_prev_ms_service_preference = 
                         ph_ss_ms_val->dsat_ms_service_preference;
    ph_ss_ms_val->dsat_ms_service_preference = 
           ph_event->event_info.dsat_srv_domain_pref;

    if( ph_event->event_info.pref_info[index].network_sel_mode_pref ==
                                                               CM_NETWORK_SEL_MODE_PREF_NO_CHANGE )
    {
          ph_event->event_info.pref_info[index].network_sel_mode_pref =
            ph_ss_ms_val->dsat_net_reg_state.cmph_pref.network_sel_mode_pref ;
    }

    if(ph_event->event_info.pref_info[index].network_rat_mode_pref ==
                                                                           CM_MODE_PREF_NO_CHANGE )
    {
      ph_event->event_info.pref_info[index].network_rat_mode_pref = 
            ph_ss_ms_val->dsat_net_reg_state.cmph_pref.network_rat_mode_pref ;
    }    

    if(ph_event->event_info.pref_info[index].network_roam_pref ==
                                                                          CM_ROAM_PREF_NO_CHANGE )
    {
      ph_event->event_info.pref_info[index].network_roam_pref =
             ph_ss_ms_val->dsat_net_reg_state.cmph_pref.network_roam_pref ;
    }  

            
    if(ph_event->event_info.pref_info[index].network_band_pref ==
                                                                         CM_BAND_PREF_NO_CHANGE  )
    {
      ph_event->event_info.pref_info[index].network_band_pref = 
              ph_ss_ms_val->dsat_net_reg_state.cmph_pref.network_band_pref ;
    }
  }
#endif /* FEATURE_DSAT_ETSI_MODE */


  if ( ( (ph_event->event == CM_PH_EVENT_OPRT_MODE) ||
         (ph_event->event == CM_PH_EVENT_INFO) ) &&
       (ph_ss_val->dsat_oprt_mode != ph_event->event_info.oprt_mode) )
  {
    dsatcmif_servs_state_ss_info  *ph_ss_val = NULL;
    if(DSAT_FAILURE == dsatutil_get_base_addr(DSAT_SS_PH_SS_VALS,(void **)&ph_ss_val,DSAT_MS_FIRST_SUBS))
    {
       dsatutil_free_memory((void*)ph_event);
       return DSAT_ASYNC_EVENT;
    }
    ph_ss_val->dsat_oprt_mode = ph_event->event_info.oprt_mode;
    DS_AT_MSG1_HIGH("dsat_oprt_mode is set to %d", ph_ss_val->dsat_oprt_mode);
    oprt_mode_changed = TRUE;
  }
  switch (ph_event->event)
  {
  case CM_PH_EVENT_STANDBY_SLEEP:
   {
     dsatcmif_servs_state_ss_info  *ph_ss_val = NULL;
     if(DSAT_FAILURE == dsatutil_get_base_addr(DSAT_SS_PH_SS_VALS,(void **)&ph_ss_val,DSAT_MS_FIRST_SUBS))
     {
        dsatutil_free_memory((void*)ph_event);
        return DSAT_ASYNC_EVENT;
     }
     ph_ss_val->cm_ph_deep_sleep = TRUE;
   }
    break;
    
  case CM_PH_EVENT_STANDBY_WAKE:
   {
     dsatcmif_servs_state_ss_info  *ph_ss_val = NULL;
     if(DSAT_FAILURE == dsatutil_get_base_addr(DSAT_SS_PH_SS_VALS,(void **)&ph_ss_val,DSAT_MS_FIRST_SUBS))
     {
        dsatutil_free_memory((void*)ph_event);
        return DSAT_ASYNC_EVENT;
     }

     ph_ss_val->cm_ph_deep_sleep = FALSE;
   }
    break;
#ifdef FEATURE_DATA_IS707
  case CM_PH_EVENT_NVRUIM_CONFIG_CHANGED:
    dsat_sysinfo_cmd.dsat_rtre_config = (dsat_rtre_control_e_type)
                ph_event->event_info.dsat_rtre_config;
    break;
#endif /* FEATURE_DATA_IS707 */

#ifdef FEATURE_ETSI_ATTACH
  case CM_PH_EVENT_PS_ATTACH_FAILED:
    /* Attach timeout event handling */
    if ( dsatcmif_attach_write != DSAT_CGATT_MAX )
    {
      dsatcmif_attach_write = DSAT_CGATT_MAX;
      dsatutil_free_memory((void*)ph_event);
      /* Report +CME error asynchronously */
      return dsat_send_cme_error(DSAT_CME_NO_NETWORK_SERVICE);
    }
    break;
#endif /* FEATURE_ETSI_ATTACH */

  case CM_PH_EVENT_OPRT_MODE:
    {
      dsat_subs_e_type subs;
      if ( CHECK_PENDING( DSAT_VENDOR_QCMRUE_IDX, 0, DSAT_PENDING_MRU_WRITE ) && 
           ( ph_event->event_info.oprt_mode == SYS_OPRT_MODE_LPM ) )
      {
        /* Perform the MRU write action */
        result = dsatvend_mru_action(DSAT_PENDING_MRU_WRITE);
      }
      else if ( CHECK_PENDING( DSAT_VENDOR_QCMRUC_IDX, 0, DSAT_PENDING_MRU_CLEAR ) &&
                ( ph_event->event_info.oprt_mode == SYS_OPRT_MODE_LPM ) )
      {
        /* Perform the MRU clear action */
        result = dsatvend_mru_action(DSAT_PENDING_MRU_CLEAR);
      }
      else if ( CHECK_PENDING( DSAT_VENDOR_QCMRUE_IDX, 0, DSAT_PENDING_MRU_RESULT ) && 
                ( ph_event->event_info.oprt_mode == SYS_OPRT_MODE_ONLINE ) )
      {
        /* Perform the MRU result action */
        result = dsatvend_mru_action(DSAT_PENDING_MRU_RESULT);
        SET_PENDING( DSAT_VENDOR_QCMRUE_IDX, 0, DSAT_PENDING_MRU_NONE );
      }
      else if ( CHECK_PENDING( DSAT_VENDOR_QCMRUC_IDX, 0, DSAT_PENDING_MRU_RESULT ) && 
                ( ph_event->event_info.oprt_mode == SYS_OPRT_MODE_ONLINE ) )
      {
        /* Perform the MRU result action */
        result = dsatvend_mru_action(DSAT_PENDING_MRU_RESULT);
        SET_PENDING( DSAT_VENDOR_QCMRUC_IDX, 0, DSAT_PENDING_MRU_NONE );
      }
      else if (CHECK_PENDING(DSAT707_VEND_ACT_RESET_IDX, 0, DSAT_PENDING_RESET_WRITE))
      {
        SET_PENDING(DSAT707_VEND_ACT_RESET_IDX, 0, DSAT_PENDING_RESET_NONE);
        result = DSAT_OK;
      }
      else
      {
        if (TRUE == oprt_mode_changed)
        {
#ifdef FEATURE_DSAT_ETSI_DATA
          for( subs = PRIMA_SUBS; subs < MAX_SUBS; subs++)
          {
            dsat_power_off[subs] = TRUE; /* Added to indicate power on detach */
          }
#endif /* FEATURE_DSAT_ETSI_DATA */
        /* Operating mode event updates +CFUN parameter */
          DS_AT_MSG3_HIGH("current CFUN = %d, received state = %d, is_card_action mode =%d",
                      (dsat_num_item_type)dsatutil_get_val(
                                   DSAT_EXT_CFUN_IDX,0,0,MIX_NUM_TYPE),
                      ph_event->event_info.oprt_mode,
                      is_card_action );

          prev_cfun_val = (dsat_num_item_type) dsatutil_get_val( 
                                     DSAT_EXT_CFUN_IDX,0,0,MIX_NUM_TYPE );

          temp_mix_type = (dsat_mixed_param_val_type *)dsatutil_get_val(
                                        DSAT_EXT_CFUN_IDX,0,0,MIX_INDEX_TYPE);
          if (FALSE == dsatme_convert_phone_state
                        (TRUE, &ph_event->event_info.oprt_mode,
                        (dsat_me_func_e_type*)&(temp_mix_type->num_item) ))
          {
            if ( CHECK_NOT_PENDING(DSAT_EXT_CFUN_IDX,0,DSAT_PENDING_CFUN_NONE) )
            {
              SET_PENDING(DSAT_EXT_CFUN_IDX ,0, DSAT_PENDING_CFUN_NONE)
              result = DSAT_ERROR;
            }
          }
        }

        /* Check for report requirement */
        if (  CHECK_NOT_PENDING(DSAT_EXT_CFUN_IDX,0,DSAT_PENDING_CFUN_NONE) )
        {
        if(DSAT_FAILURE == dsatutil_get_base_addr(DSAT_MMGSDI_SS_VALS,(void **)&me_ss_val,DSAT_MS_FIRST_SUBS) ||
           DSAT_FAILURE == dsatutil_get_base_addr(DSAT_SS_PH_MS_VALS,(void **)&ph_ss_ms_val,0))
        {
          dsatutil_free_memory((void*)ph_event);
          SET_PENDING(DSAT_EXT_CFUN_IDX ,0, DSAT_PENDING_CFUN_NONE)
          return DSAT_ERROR;
        }
#if defined(FEATURE_DSAT_CFUN_CARD_POWER_CTL) && defined (FEATURE_DSAT_ETSI_MODE)
          switch (dsat_pending_state[PORT_ZERO].cmd_pending[0])
          {
            case DSAT_PENDING_CFUN_WRITE_MIN:
          {
            /*Do card operation when EFS enabled and Received mode is equal to requested mode*/
            if((is_card_action == TRUE) &&
                  ( MMGSDI_INVALID_SESSION_ID != me_ss_val->dsat_mmgsdi_client_id ))
            {
              if( DSAT_ME_FUNC_MIN == (dsat_num_item_type)dsatutil_get_val(
                                          DSAT_EXT_CFUN_IDX,0,0,MIX_NUM_TYPE))
              {
                  /* Case: SIM Card is not present */
                result = DSAT_OK;
                SET_PENDING(DSAT_EXT_CFUN_IDX ,0, DSAT_PENDING_CFUN_NONE)
                   
          for(index = 0 ; index < AT_MAX_ALLOWED_SLOT; index++) 
          {
            if(dsat_qcsimapp_info[index].slot != MMGSDI_MAX_SLOT_ID_ENUM )
            {
              if (DSAT_FAILURE == dsatutil_get_base_addr(DSAT_SS_PH_MS_VALS,(void **)&ph_ss_ms_val,index))
              {
                dsatutil_free_memory((void*)ph_event);
                return DSAT_ERROR;
              }
              /* Store the slot to power_up the card_slot later for +CFUN=1 command */
                    ph_ss_ms_val->dsat_cfun_pref.dsat_card_pdown_slot =  dsat_qcsimapp_info[index].slot;
  
                    if ( MMGSDI_RESTRICTED_IN_ONCHIP_MODE != mmgsdi_card_pdown( 
                         me_ss_val->dsat_mmgsdi_client_id,
                         dsat_qcsimapp_info[index].slot,
                         (mmgsdi_callback_type)dsatetsime_mmgsdi_card_power_ctl_cb,
                         MMGSDI_CARD_POWER_DOWN_NOTIFY_GSDI,
                         0 ) )
                    {
                      result = DSAT_ASYNC_CMD;
                      SET_PENDING(DSAT_EXT_CFUN_IDX ,0, DSAT_PENDING_CFUN_WRITE_CARD_DOWN)
#if defined(FEATURE_8960_SGLTE_FUSION) || defined(FEATURE_8960_DSDA_FUSION)
/*Moved provisioned state to CARD POWER DOWN and cached slot  information for CFUN=1*/
                      dsat_set_provision_state(DSAT_MMGSDI_DOWN);
#endif /* defined(FEATURE_8960_SGLTE_FUSION) || defined(FEATURE_8960_DSDA_FUSION) */
                    }
                  }
               }
            }
            else
            {
              /*Will wait for next operating mode change event(Hint: SGLTE case)*/
              result = DSAT_ASYNC_CMD;
            }
          }
          else
          {
            SET_PENDING(DSAT_EXT_CFUN_IDX ,0, DSAT_PENDING_CFUN_NONE)
            result = DSAT_OK;
          }
        }
              break;
        case DSAT_PENDING_CFUN_WRITE_FULL:
        {
          if( ((is_card_action == TRUE)) &&
                  ( MMGSDI_INVALID_SESSION_ID != me_ss_val->dsat_mmgsdi_client_id ))
        {
              /* Valid: Only CFUN =0  to CFUN =1 card PUP operation is allowed*/

              if( DSAT_ME_FUNC_FULL == (dsat_num_item_type)dsatutil_get_val(
                                              DSAT_EXT_CFUN_IDX,0,0,MIX_NUM_TYPE))
              {
              if(DSAT_ME_FUNC_MIN == prev_cfun_val)
              {
          /* Send power_up to the slot which either was powered down due to +CFUN=0 or 
             due to CARD_INSERTED event */
          for(index = 0 ; index <AT_MAX_ALLOWED_SLOT; index++)
          {
            if (DSAT_FAILURE == dsatutil_get_base_addr(DSAT_SS_PH_MS_VALS,(void **)&ph_ss_ms_val,index))
            {
              dsatutil_free_memory((void*)ph_event);
              return DSAT_ERROR;
            }

            if((dsat_qcsimapp_info[index].slot == MMGSDI_SLOT_1 )||
                  ph_ss_ms_val->dsat_cfun_pref.dsat_card_pdown_slot == MMGSDI_SLOT_1)
            {
              power_up_slot1 = TRUE;
              ph_ss_ms_val->dsat_cfun_pref.dsat_card_pdown_slot = MMGSDI_MAX_SLOT_ID_ENUM;
            }
            if((dsat_qcsimapp_info[index].slot == MMGSDI_SLOT_2 )||
                ph_ss_ms_val->dsat_cfun_pref.dsat_card_pdown_slot == MMGSDI_SLOT_2)
            {
              power_up_slot2 = TRUE;
              ph_ss_ms_val->dsat_cfun_pref.dsat_card_pdown_slot = MMGSDI_MAX_SLOT_ID_ENUM;
            }
#ifdef FEATURE_TRIPLE_SIM
            if((dsat_qcsimapp_info[index].slot == MMGSDI_SLOT_3 )||
                ph_ss_ms_val->dsat_cfun_pref.dsat_card_pdown_slot == MMGSDI_SLOT_3)
            {
              power_up_slot3 = TRUE;
              ph_ss_ms_val->dsat_cfun_pref.dsat_card_pdown_slot = MMGSDI_MAX_SLOT_ID_ENUM;
            }
#endif /* FEATURE_TRIPLE_SIM */
          }
                   /* Case: SIM Card is not present */
                result = DSAT_OK;
                SET_PENDING(DSAT_EXT_CFUN_IDX ,0, DSAT_PENDING_CFUN_NONE)

          if(power_up_slot1)
          {
            if ( MMGSDI_RESTRICTED_IN_ONCHIP_MODE != mmgsdi_card_pup( 
                         me_ss_val->dsat_mmgsdi_client_id,
                         MMGSDI_SLOT_1,
                         (mmgsdi_callback_type)dsatetsime_mmgsdi_card_power_ctl_cb,
                         MMGSDI_CARD_POWER_UP_INITIAL_PUP,
                         0 ) )
            {
              result = DSAT_ASYNC_CMD;
              SET_PENDING(DSAT_EXT_CFUN_IDX ,0, DSAT_PENDING_CFUN_WRITE_CARD_PUP)
#if defined(FEATURE_8960_SGLTE_FUSION) || defined(FEATURE_8960_DSDA_FUSION)  
              dsat_set_provision_state(DSAT_MMGSDI_UP);
#endif /* defined(FEATURE_8960_SGLTE_FUSION) || defined(FEATURE_8960_DSDA_FUSION) */
            }
          }
          if(power_up_slot2)
          {
            if ( MMGSDI_RESTRICTED_IN_ONCHIP_MODE != mmgsdi_card_pup( 
                         me_ss_val->dsat_mmgsdi_client_id,
                         MMGSDI_SLOT_2,
                         (mmgsdi_callback_type)dsatetsime_mmgsdi_card_power_ctl_cb,
                         MMGSDI_CARD_POWER_UP_INITIAL_PUP,
                         0 ) )
            {
#if defined(FEATURE_8960_SGLTE_FUSION) || defined(FEATURE_8960_DSDA_FUSION)			  
              dsat_set_provision_state(DSAT_MMGSDI_UP);
#endif /* defined(FEATURE_8960_SGLTE_FUSION) || defined(FEATURE_8960_DSDA_FUSION) */
              result = DSAT_ASYNC_CMD;
              SET_PENDING(DSAT_EXT_CFUN_IDX ,0, DSAT_PENDING_CFUN_WRITE_CARD_PUP)
            }
          }
#ifdef FEATURE_TRIPLE_SIM
          if(power_up_slot3)
          {
            if ( MMGSDI_RESTRICTED_IN_ONCHIP_MODE != mmgsdi_card_pup( 
                         me_ss_val->dsat_mmgsdi_client_id,
                         MMGSDI_SLOT_3,
                         (mmgsdi_callback_type)dsatetsime_mmgsdi_card_power_ctl_cb,
                         MMGSDI_CARD_POWER_UP_INITIAL_PUP,
                         0 ) )
            {
#if defined(FEATURE_8960_SGLTE_FUSION) || defined(FEATURE_8960_DSDA_FUSION)			  
              dsat_set_provision_state(DSAT_MMGSDI_UP);
#endif /* defined(FEATURE_8960_SGLTE_FUSION) || defined(FEATURE_8960_DSDA_FUSION) */
              result = DSAT_ASYNC_CMD;
              SET_PENDING(DSAT_EXT_CFUN_IDX ,0, DSAT_PENDING_CFUN_WRITE_CARD_PUP)
            }
          }
#endif /* FEATURE_TRIPLE_SIM */
              }
              else
          {
            result = DSAT_OK;
          }
              }
              else
              {
                /*Will wait for next operating mode change event(Hint: SGLTE case)*/
                result = DSAT_ASYNC_CMD;
              }
          }
          else
          {
             SET_PENDING(DSAT_EXT_CFUN_IDX ,0, DSAT_PENDING_CFUN_NONE)
          result = DSAT_OK;
        }
        }
              break;
            case DSAT_PENDING_CFUN_WRITE_RFOFF:
            case DSAT_PENDING_CFUN_WRITE_FTM:
            case DSAT_PENDING_CFUN_WRITE_RESET:
            case DSAT_PENDING_CFUN_WRITE_OFFLINE:
              {
          SET_PENDING(DSAT_EXT_CFUN_IDX ,0, DSAT_PENDING_CFUN_NONE)
                result = DSAT_OK;
              }
              break;
        case DSAT_PENDING_CFUN_WRITE:
        break;
            default:
              break;
          }
#else
          SET_PENDING(DSAT_EXT_CFUN_IDX ,0, DSAT_PENDING_CFUN_NONE)
          result = DSAT_OK;
#endif /* FEATURE_DSAT_CFUN_CARD_POWER_CTL */
        }
      }
    }
    break;
  case CM_PH_EVENT_INFO:
#ifdef FEATURE_DSAT_ETSI_MODE
  case CM_PH_EVENT_SUBSCRIPTION_NOT_AVAILABLE:
  case CM_PH_EVENT_SYS_SEL_PREF:
  case CM_PH_EVENT_AVAILABLE_NETWORKS_CONF:
  case CM_PH_EVENT_SUBSCRIPTION_AVAILABLE:
  case CM_PH_EVENT_TERMINATE_GET_NETWORKS:
#ifdef FEATURE_SGLTE 	
  case CM_PH_EVENT_SIM_AVAILABLE:
#endif/* FEATURE_SGLTE*/  	
#ifdef FEATURE_DSAT_LTE
      if(ph_event->event == CM_PH_EVENT_INFO )
      {
         switch(ph_event->event_info.eps_mode)
         {
           case CM_LTE_UE_MODE_PREF_PS_MODE1:
             eps_mode = DSAT_EPS_PS_MODE_1;
             break;
           case CM_LTE_UE_MODE_PREF_PS_MODE2:  
             eps_mode = DSAT_EPS_PS_MODE_2;
             break;
           case CM_LTE_UE_MODE_PREF_CS_PS_MODE1:
             eps_mode = DSAT_EPS_CS_PS_MODE_1;
             break;
           case CM_LTE_UE_MODE_PREF_CS_PS_MODE2:
             eps_mode = DSAT_EPS_CS_PS_MODE_2;
             break;
           default:
             break;
         }
         if(((dsat_num_item_type)
               dsatutil_get_val( DSATETSI_EXT_CEMODE_IDX,0,0,NUM_TYPE) != eps_mode)&& 
               (eps_mode != DSAT_EPS_MODE_MAX))
         {
           DSATUTIL_SET_VAL(DSATETSI_EXT_CEMODE_IDX,0,0,0,eps_mode,NUM_TYPE)
           DS_AT_MSG1_HIGH(" UPDATED CEMODE as %d ",eps_mode);
         }
      }
#endif /* FEATURE_DSAT_LTE */
#endif /* FEATURE_DSAT_ETSI_MODE */

#ifdef FEATURE_DUAL_SIM
      subs_info = dsatcmif_update_subs_idx(cmd_ptr->cmd.cm_cmd.subs);
      if(subs_info == DS_SUBS_MAX)
      {
        dsatutil_free_memory((void*)ph_event);
        return result;;
      }
#else
      subs_info = DS_FIRST_SUBS;
#endif /* FEATURE_DUAL_SIM */

        DS_AT_MSG1_MED("Subs id = %d",subs_info);
        ASSERT(subs_info >= DS_FIRST_SUBS && subs_info < DS_ALL_SUBS);

        if(DSAT_FAILURE == dsatutil_get_base_addr(DSAT_SS_PH_MS_VALS,(void **)&ph_ss_ms_val,subs_info) ||
            DSAT_FAILURE == dsatutil_get_base_addr(DSAT_CALL_MS_VALS, (void **)&call_ms_val ,subs_info))
        {
          dsatutil_free_memory((void*)ph_event);
          return DSAT_ASYNC_EVENT;
        }

        switch(ph_event->event)
        {
          case CM_PH_EVENT_INFO:
#ifdef FEATURE_DSAT_ETSI_MODE 
            if (call_ms_val->dsat_acm_res_type != DSAT_ACM_NONE)
          /* aoc_info member variable should be used only if CM_API_AOC_INFO
              is defined */
#ifdef CM_API_AOC_INFO
          {
            if (TRUE == ph_event->event_info.aoc_info[subs_info].aoc_ready)
            {
              uint32 acm_val=0;
              dsm_item_type * res_buff_ptr;
              char cmd_name[6];
    
              res_buff_ptr = dsat_dsm_new_buffer(DSM_DS_SMALL_ITEM_POOL, FALSE);
              if (call_ms_val->dsat_acm_res_type == DSAT_ACM_READ)
              {
                acm_val = ph_event->event_info.aoc_info[subs_info].ACM;
                (void)strlcpy(cmd_name,"+CACM",sizeof(cmd_name));
              }
              /* This is for +CAMM command */
              else if (call_ms_val->dsat_acm_res_type == DSAT_ACM_MAX_READ)
              {
                acm_val = ph_event->event_info.aoc_info[subs_info].ACMmax;
                (void)strlcpy(cmd_name,"+CAMM",sizeof(cmd_name));
              }
              /* Format the response */
              res_buff_ptr->used = (word)snprintf((char*)res_buff_ptr->data_ptr,
                                                   res_buff_ptr->size,
                                                  "%s: \"%02X%02X%02X\"",
                                                  cmd_name,
                                                  (uint8)(acm_val>>16 & 0xFF),
                                                  (uint8)(acm_val>>8  & 0xFF),
                                                  (uint8)(acm_val     & 0xFF));
              res_buff_ptr->data_ptr[res_buff_ptr->used] = '\0';
              dsatcmdp_handle_async_cmd_rsp (res_buff_ptr, DSAT_COMPLETE_RSP);
              call_ms_val->dsat_acm_res_type = DSAT_ACM_NONE;
              result_subs[subs_info]= DSAT_OK;
            }
            /* If aoc_ready is false then SIM might be busy */
            else
            {
                call_ms_val->dsat_acm_res_type = DSAT_ACM_NONE;
              DS_AT_MSG0_ERROR("Advice of Charge is invalid/not ready");
              result_subs[subs_info] = dsat_send_cme_error(DSAT_CME_SIM_BUSY);
            }
          }  
  /* If CM_API_AOC_INFO is not defined this operation is not supported */
#else
          {
              call_ms_val->dsat_acm_res_type = DSAT_ACM_NONE;
            DS_AT_MSG0_ERROR("CM_API_AOC_INFO not defined");
            result_subs[subs_info] = dsat_send_cme_error(DSAT_CME_OP_NOT_SUPPORTED);
          }
#endif /* CM_API_AOC_INFO */
#if defined(FEATURE_DATA_GCSD) || defined(FEATURE_DATA_WCDMA)
            else if ( ph_ss_ms_val->dsat_ws46_res_type != DSAT_WS46_NONE )
          {
            result_subs[subs_info] = dsatact_process_ws46_cmd (
                      &ph_event->event_info.pref_info[subs_info]);
          }
#endif /* defined(FEATURE_DATA_GCSD) || defined(FEATURE_DATA_WCDMA) */
      /* This condition is satisfied only during initial phone boot-up.
           Update +COPS <mode> using network_sel_mode_pref. */
          if( (dsat_num_item_type)DSAT_COPS_MODE_MAX == 
              (dsat_num_item_type)dsatutil_get_val(
                   DSATETSI_EXT_ACT_COPS_ETSI_IDX,subs_info,0,MIX_NUM_TYPE))
          {
            if(ph_event->event_info.pref_info[subs_info].network_sel_mode_pref
                  == CM_NETWORK_SEL_MODE_PREF_MANUAL)
            {
              DSATUTIL_SET_VAL(DSATETSI_EXT_ACT_COPS_ETSI_IDX,subs_info,0,0,
                     (dsat_num_item_type)DSAT_COPS_MODE_MANUAL,MIX_NUM_TYPE)
            }
            else
            {
              DSATUTIL_SET_VAL(DSATETSI_EXT_ACT_COPS_ETSI_IDX,subs_info,0,0,
                     (dsat_num_item_type)DSAT_COPS_MODE_AUTO,MIX_NUM_TYPE)
            }
          }
#endif /* FEATURE_DSAT_ETSI_MODE */

          if(CHECK_NOT_PENDING(DSAT_EXT_CFUN_IDX,0,DSAT_PENDING_CFUN_NONE))
          {
            DS_AT_MSG3_HIGH(" dsat_cfun_res_type = %d oprt mode = %d operating mode = %d", 
                                 dsat_pending_state[0].cmd_pending[0],
                                 ph_event->event_info.oprt_mode, 
                                 current_mode );
            result_subs[subs_info] = dsatme_process_cfun_cmd (
                    ph_event->event_info.oprt_mode, (dsat_me_func_e_type)NULL);
          }
          else
          {
            /* This is during Init unless we miss CM_PH_EVENT_OPRT_MODE event */
               DS_AT_MSG2_HIGH(" dsat_cfun_val_type = %d Recvd oprt mode = %d ",
                    (dsat_num_item_type)dsatutil_get_val(DSAT_EXT_CFUN_IDX,0,0,MIX_NUM_TYPE),
                         ph_event->event_info.oprt_mode);

               temp_mix_type = (dsat_mixed_param_val_type *)dsatutil_get_val(
                              DSAT_EXT_CFUN_IDX,0,0,MIX_INDEX_TYPE);

               (void)dsatme_convert_phone_state
                        (TRUE, &ph_event->event_info.oprt_mode, 
                         (dsat_me_func_e_type*)&(temp_mix_type->num_item) );
          }

          if ( CHECK_PENDING(DSAT_VENDOR_SYSCONFIG_IDX,0,DSAT_PENDING_SYSCONFIG_READ) )
          {
            SET_PENDING(DSAT_VENDOR_SYSCONFIG_IDX ,0, DSAT_PENDING_SYSCONFIG_NONE)
            result_subs[subs_info] = dsatvend_process_sysconfig_cmd( DSAT_PENDING_SYSCONFIG_READ,
                            &ph_event->event_info.pref_info[subs_info]);
          }
          if ( CHECK_PENDING(DSAT_VENDOR_PREFMODE_IDX,0,DSAT_PENDING_PREFMODE_SYSMODE_READ) )
          {
            SET_PENDING(DSAT_VENDOR_PREFMODE_IDX ,0, DSAT_PENDING_PREFMODE_NONE)
            result_subs[subs_info] = dsatvend_process_prefmode_cmd( DSAT_PENDING_PREFMODE_SYSMODE_READ,
                                       &ph_event->event_info.pref_info[subs_info]);
          }
          if ( CHECK_PENDING(DSAT_VENDOR_QCBANDPREF_IDX,0,DSAT_PENDING_PREFMODE_BANDPREF_READ) )
          {
            SET_PENDING(DSAT_VENDOR_QCBANDPREF_IDX ,0, DSAT_PENDING_PREFMODE_NONE)
            result_subs[subs_info] = dsatvend_process_prefmode_cmd( DSAT_PENDING_PREFMODE_BANDPREF_READ,
                                       &ph_event->event_info.pref_info[subs_info]);
          }
          if ( CHECK_PENDING(DSAT_VENDOR_QCBANDPREF_IDX,0,DSAT_PENDING_PREFMODE_BANDPREF_VERIFY) )
          {
            /* BandPref setting needs two phases 1. get the current mode 
                  now set the band pref after validation */
            SET_PENDING(DSAT_VENDOR_QCBANDPREF_IDX ,0, DSAT_PENDING_PREFMODE_BANDPREF_WRITE)
            result_subs[subs_info] = dsatvend_process_prefmode_cmd(DSAT_PENDING_PREFMODE_BANDPREF_VERIFY,
                                       &ph_event->event_info.pref_info[subs_info]);
          }
          if( CHECK_PENDING(DSAT_VENDOR_QCNSP_IDX,0,DSAT_PENDING_QCNSP_READ) &&
              (subs_info == dsat_get_qcsimapp_val()) )
          {
            SET_PENDING(DSAT_VENDOR_QCNSP_IDX ,0, DSAT_PENDING_QCNSP_NONE)
            result_subs[subs_info] = dsatvend_process_qcnsp_cmd( DSAT_PENDING_QCNSP_READ,
                                       &ph_event->event_info.pref_info[subs_info]);
          }
          if( CHECK_PENDING(DSAT_VENDOR_QCNSP_IDX,0,DSAT_PENDING_QCNSP_GET_PRI_RAT) &&
              (subs_info == dsat_get_qcsimapp_val()))
          {
            SET_PENDING(DSAT_VENDOR_QCNSP_IDX ,0, DSAT_PENDING_QCNSP_NONE)
            result_subs[subs_info] = dsatvend_process_qcnsp_cmd( DSAT_PENDING_QCNSP_GET_PRI_RAT,
                                       &ph_event->event_info.pref_info[subs_info]);
          }
        break;
#ifdef FEATURE_DSAT_ETSI_MODE 
     case CM_PH_EVENT_TERMINATE_GET_NETWORKS:
         if( ph_ss_ms_val->dsatetsicall_network_list.cmd_state == DSAT_COPS_ASTATE_ANETS )
       {
         DS_AT_MSG0_ERROR("Received Terminate GET_NETWORKS event from CM");
         ph_ss_ms_val->dsatetsicall_network_list.cmd_state = DSAT_COPS_ASTATE_NULL;
         ph_ss_ms_val->dsatetsicall_network_list.cmd_idx = CMD_IDX_NONE;
         result_subs[subs_info] = dsat_send_cme_error(DSAT_CME_UNKNOWN);
       }
      break;
      case CM_PH_EVENT_SUBSCRIPTION_NOT_AVAILABLE:
        if( FALSE == cmif_is_gw_subscription_available( subs_info , ph_event ) )
        {
          DS_AT_MSG0_HIGH("CM_PH_EVENT_SUBSCRIPTION_NOT_AVAILABLE event is successful");
            ph_ss_ms_val->net_pref.last_dereg_success = TRUE;
            ph_ss_ms_val->net_pref.mode = DSAT_COPS_MODE_DEREG;
          DSATUTIL_SET_VAL(DSATETSI_EXT_ACT_COPS_ETSI_IDX,subs_info,0,0,
                     (dsat_num_item_type)DSAT_COPS_MODE_DEREG,MIX_NUM_TYPE)
            memset ((void*)&ph_ss_ms_val->dsat_net_reg_state.net_id_info, 0x0,
            sizeof(ph_ss_ms_val->dsat_net_reg_state.net_id_info));
          
          dsatetsicall_set_net_reg(DSAT_NET_REG_DOMAIN_CS ,DSAT_NET_REG_NONE,subs_info);
          dsatetsicall_set_net_reg(DSAT_NET_REG_DOMAIN_PS ,DSAT_NET_REG_NONE,subs_info);

          /*Update the COPS status accordingly */
            if( (DSAT_COPS_ASTATE_PREF == ph_ss_ms_val->dsatetsicall_network_list.cmd_state) ||
                 (DSAT_COPS_ASTATE_PREF_ONLY == ph_ss_ms_val->dsatetsicall_network_list.cmd_state) )
          {
            result_subs[subs_info] = dsatetsicall_cops_async_handler
                         (ph_ss_ms_val->dsatetsicall_network_list.cmd_state,subs_info, cmd_ptr);
          }
        }
      break;
      case CM_PH_EVENT_SYS_SEL_PREF:
        {
          ds_at_cm_ph_pref_type * pref_info_ptr =
            &ph_event->event_info.pref_info[subs_info];
    
          /* Update network registration state tracking */
            ph_ss_ms_val->dsat_net_reg_state.cmph_pref.network_sel_mode_pref =
            pref_info_ptr->network_sel_mode_pref;
            ph_ss_ms_val->dsat_net_reg_state.cmph_pref.network_rat_mode_pref =
            pref_info_ptr->network_rat_mode_pref;
    
            if( (DSAT_COPS_ASTATE_PREF == ph_ss_ms_val->dsatetsicall_network_list.cmd_state) ||
                (DSAT_COPS_ASTATE_PREF_ONLY == ph_ss_ms_val->dsatetsicall_network_list.cmd_state) )
            {
              result_subs[subs_info] = dsatetsicall_cops_async_handler
                  (ph_ss_ms_val->dsatetsicall_network_list.cmd_state,subs_info, cmd_ptr);
            }
            else if( TRUE == cmif_is_gw_subscription_available( subs_info , ph_event) )
            {
            /* Client other than ATCOP may have made change so update
             * +COPS mode.  The enum values correspond to the command
             * table list indexes so this simplifies update. */
              switch (ph_ss_ms_val->dsat_net_reg_state.cmph_pref.network_sel_mode_pref)
              {
              case CM_NETWORK_SEL_MODE_PREF_AUTOMATIC:
                  DSATUTIL_SET_VAL(DSATETSI_EXT_ACT_COPS_ETSI_IDX,subs_info,0,0,
                     (dsat_num_item_type)DSAT_COPS_MODE_AUTO,MIX_NUM_TYPE)
                break;
              case CM_NETWORK_SEL_MODE_PREF_MANUAL:
              case CM_NETWORK_SEL_MODE_PREF_LIMITED_SRV:
              DSATUTIL_SET_VAL(DSATETSI_EXT_ACT_COPS_ETSI_IDX,subs_info,0,0,
                     (dsat_num_item_type)DSAT_COPS_MODE_MANUAL,MIX_NUM_TYPE)
                break;
              default:
              DS_AT_MSG1_ERROR(" network_sel_mode_pref = %d not handled",
                   ph_ss_ms_val->dsat_net_reg_state.cmph_pref.network_sel_mode_pref);
              /* No update required */
              break;
            }
          }
    
          if ( (dsatcmif_attach_write != DSAT_CGATT_MAX) && dsatcmif_ss_evt_not_expected )
          {
            dsatcmif_attach_write = DSAT_CGATT_MAX;
            dsatcmif_ss_evt_not_expected = FALSE;
            result_subs[subs_info] = DSAT_OK;
          }

          if ( CHECK_PENDING(DSAT_VENDOR_SYSCONFIG_IDX,0,DSAT_PENDING_SYSCONFIG_WRITE) )
          {
            SET_PENDING(DSAT_VENDOR_SYSCONFIG_IDX ,0, DSAT_PENDING_SYSCONFIG_NONE)
            result_subs[subs_info] = dsatvend_process_sysconfig_cmd( DSAT_PENDING_SYSCONFIG_WRITE,
                               &ph_event->event_info.pref_info[subs_info]);
          }
          if ( CHECK_PENDING(DSAT_VENDOR_QCBANDPREF_IDX,0,DSAT_PENDING_PREFMODE_BANDPREF_WRITE) )
          {
            /* BandPref setting completed Fill the result_subs to return Write status.*/
            SET_PENDING(DSAT_VENDOR_QCBANDPREF_IDX ,0, DSAT_PENDING_PREFMODE_NONE)
            result_subs[subs_info] = dsatvend_process_prefmode_cmd(DSAT_PENDING_PREFMODE_BANDPREF_WRITE,
                     &ph_event->event_info.pref_info[subs_info]);
          }
          if ( CHECK_PENDING(DSAT_VENDOR_PREFMODE_IDX,0,DSAT_PENDING_PREFMODE_SYSMODE_WRITE) )
          {
            SET_PENDING(DSAT_VENDOR_PREFMODE_IDX ,0, DSAT_PENDING_PREFMODE_NONE)
            result_subs[subs_info] = dsatvend_process_prefmode_cmd(DSAT_PENDING_PREFMODE_SYSMODE_WRITE,
                     &ph_event->event_info.pref_info[subs_info]);
          }
          if ( CHECK_PENDING(DSATETSI_EXT_ACT_CVMOD_ETSI_IDX ,0,DSAT_PENDING_TRUE) )
          {  
            SET_PENDING(DSATETSI_EXT_ACT_CVMOD_ETSI_IDX ,0, DSAT_PENDING_FALSE)
            result_subs[subs_info] = DSAT_OK;
          }
          if ( CHECK_PENDING(DSAT_VENDOR_QCNSP_IDX,0,DSAT_PENDING_QCNSP_WRITE) &&
               (subs_info == dsat_get_qcsimapp_val()) )
          {
            SET_PENDING(DSAT_VENDOR_QCNSP_IDX,0,DSAT_PENDING_QCNSP_NONE)
            result_subs[subs_info] = dsatvend_process_qcnsp_cmd( DSAT_PENDING_QCNSP_WRITE,
                                       &ph_event->event_info.pref_info[subs_info]);
          }
        }
      break;
  
      case CM_PH_EVENT_AVAILABLE_NETWORKS_CONF:
          if (DSAT_COPS_ASTATE_ANETS == ph_ss_ms_val->dsatetsicall_network_list.cmd_state)
        {
          result_subs[subs_info] = dsatetsicall_cops_async_handler 
                       (ph_ss_ms_val->dsatetsicall_network_list.cmd_state,subs_info, cmd_ptr);
        }
      break;
	
#ifdef FEATURE_SGLTE
     case CM_PH_EVENT_SIM_AVAILABLE:
     if( TRUE == cmif_is_gw_subscription_available( subs_info , ph_event ) )
     {
      ph_ss_ms_val->net_pref.last_dereg_success = FALSE;
        if ( ph_ss_ms_val->net_pref.subscription_invoked == TRUE ) 
        {
            result_subs[subs_info] = cmif_subscription_available_process_cops(ph_ss_ms_val, subs_info);
            /* reset the net_pref structure and make the boolean false */
            ph_ss_ms_val->net_pref.subscription_invoked = FALSE; 
        }	
        DS_AT_MSG0_HIGH("Called Sys Sel pref");
     	}
break;
#endif/* FEATURE_SGLTE*/
      case CM_PH_EVENT_SUBSCRIPTION_AVAILABLE:
        if( TRUE == cmif_is_gw_subscription_available( subs_info , ph_event ) )
        {
          ph_ss_ms_val->net_pref.last_dereg_success = FALSE;
          if ( ph_ss_ms_val->net_pref.subscription_invoked == TRUE ) 
          /* To make sure this is not after phone boot up
             but after deregistration command */
          {
#ifdef FEATURE_SGLTE
            /* CM sends SIM AVAILABLE event only in valid state for subs proc */
            if (TRUE == cm_is_valid_state_for_sub_proc(ph_event->event_info.oprt_mode))
            {
              DS_AT_MSG0_HIGH("call Sys Sel pref in SIM AVAILABLE event for SGLTE");
            }
            else
            {
#endif /* FEATURE_SGLTE */
              result_subs[subs_info] = cmif_subscription_available_process_cops(ph_ss_ms_val, subs_info);
              /* reset the net_pref structure and make the boolean false */
              ph_ss_ms_val->net_pref.subscription_invoked = FALSE;
#ifdef FEATURE_SGLTE
            }
#endif /* FEATURE_SGLTE */
          }
          else
          {
            if(ph_event->event_info.pref_info[subs_info].network_sel_mode_pref
                      == CM_NETWORK_SEL_MODE_PREF_MANUAL)
            {
              DSATUTIL_SET_VAL(DSATETSI_EXT_ACT_COPS_ETSI_IDX,subs_info,0,0,
                     (dsat_num_item_type)DSAT_COPS_MODE_MANUAL,MIX_NUM_TYPE)
            }
            else
            {
              DSATUTIL_SET_VAL(DSATETSI_EXT_ACT_COPS_ETSI_IDX,subs_info,0,0,
                     (dsat_num_item_type)DSAT_COPS_MODE_AUTO,MIX_NUM_TYPE)
            }
          }
            if ( ( DSAT_COPS_ASTATE_NULL == ph_ss_ms_val->dsatetsicall_network_list.cmd_state)||
                 ( DSAT_COPS_ASTATE_ABORT == ph_ss_ms_val->dsatetsicall_network_list.cmd_state) )
          {
            result_subs[subs_info] = DSAT_ASYNC_EVENT;
          }
        }
        break;
#endif /* FEATURE_DSAT_ETSI_MODE */
        default:
        DS_AT_MSG0_HIGH("Default case ");
        break;
      }/* Switch */

 /*  Subs_info ++;while (subs_info <DSAT_MAX);*/
      result = result_subs[dsat_get_qcsimapp_val()];
      DS_AT_MSG3_HIGH("result = %d, subs = %d, qcsimapp = %d", result, subs_info, dsat_get_qcsimapp_val());
    break;

#if defined(FEATURE_HS_USB_SER3_PORT)
  case CM_PH_EVENT_PACKET_STATE:
    /* Send indication to TE for dormant or non-dormant call */
    {
      dsatcmif_call_status_e_type acm_val= DS_CALL_STATUS_NONE;
      cm_packet_state_e_type curr_state;
      dsatcmif_servs_state_ss_info  *ph_ss_val = NULL;

      if(DSAT_FAILURE == dsatutil_get_base_addr(DSAT_SS_PH_SS_VALS,(void **)&ph_ss_val,DSAT_MS_FIRST_SUBS))
      {
        dsatutil_free_memory((void*)ph_event);
        return DSAT_ASYNC_EVENT;
      }

      result = DSAT_ASYNC_EVENT;

      curr_state = ph_event->event_info.packet_state;
      DS_AT_MSG1_MED("current call state %d",curr_state);

      /* This is for dormancy status report */
      if (curr_state == CM_PACKET_STATE_DORMANT)
      {
        acm_val = DS_CALL_STATUS_DORMANT;
      }
      else
      {
        acm_val = DS_CALL_STATUS_NONDORMANT;
      }

      /* need to report the dormancy status*/
      ph_ss_val->dsatcmif_cm_pkt_state = acm_val;

      if( FALSE == ph_ss_val->dsatcmif_dormant_ind_report_enabled )
      {
        DS_AT_MSG0_HIGH("Call has not connected yet");
        dsatutil_free_memory((void*)ph_event);
        return DSAT_ASYNC_EVENT;
      }

      DS_AT_MSG0_MED("report dormant status");

      result = cmif_generate_dormant_ind();
      if( CM_PACKET_STATE_NULL == curr_state )
      {
        ph_ss_val->dsatcmif_dormant_ind_report_enabled = FALSE;
      }

    }
    break;
#endif /* defined(FEATURE_HS_USB_SER3_PORT) */
#ifdef FEATURE_DSAT_LTE
  case CM_PH_EVENT_GET_DRX_CNF:
    if ( CHECK_PENDING(DSAT_VENDOR_QCDRX_IDX, 0, DSAT_PENDING_QCDRX_GET) &&
         (index == dsat_get_qcsimapp_val()) )
    {
      result = dsatvend_process_qcdrx_cmd (DSAT_PENDING_QCDRX_GET,
                                           &ph_event->event_info);
    }
    break;

  case CM_PH_EVENT_SET_DRX_CNF:
    if ( CHECK_PENDING(DSAT_VENDOR_QCDRX_IDX, 0, DSAT_PENDING_QCDRX_SET) &&
         (index == dsat_get_qcsimapp_val()) )
    {
      result = dsatvend_process_qcdrx_cmd (DSAT_PENDING_QCDRX_SET,
                                           &ph_event->event_info);
    }
    break;
#endif /* FEATURE_DSAT_LTE*/
  default:
    DS_AT_MSG1_ERROR("Unsupported PH event: %d", ph_event->event);
    break;
  }
  dsatutil_free_memory((void*)ph_event);
  return result;
} /* dsatcmif_cm_ph_event_handler */


/*===========================================================================

FUNCTION DSATCMIF_SIGNAL_HANDLER

DESCRIPTION
  This function processes the asynchronous signals assosiacted with
  messages from Call Manager.

DEPENDENCIES
  None

RETURN VALUE
  DSAT_ERROR: if we find an error in procssing the event
  DSAT_OK: if processing of the event is complete and right
  DSAT_ASYNC_CMD : if we still have more events to process
  DSAT_ASYNC_EVENT : if the event is ignored

SIDE EFFECTS
  None

===========================================================================*/
/* ARGSUSED */
dsat_result_enum_type dsatcmif_signal_handler
(
 dsat_mode_enum_type at_mode
)
{
  dsm_item_type         *dsm_item_ptr;
  dsat_cm_msg_s_type    *msg_ptr = NULL;
  dsat_result_enum_type  result = DSAT_ASYNC_EVENT;

  msg_ptr = dsat_alloc_memory(sizeof(dsat_cm_msg_s_type), TRUE);
  if (NULL == msg_ptr)
  {
    return result;
  }

  while ( (dsm_item_ptr = dsm_dequeue( &dsat_cm_msg_wm )) != NULL)
  {
    (void)dsm_pullup( &dsm_item_ptr, msg_ptr, sizeof(dsat_cm_msg_s_type) );

    DS_AT_MSG1_MED("ATCOP:Freed 0x%p DSM item, signal handler", dsm_item_ptr);
    dsm_free_packet( &dsm_item_ptr );
    
    /* Dispatch event message to handler */
    switch( msg_ptr->cmd )
    {
      case DS_AT_CM_SS_INFO_CMD:
        result = dsatcmif_cm_ss_event_handler( msg_ptr );
        break;

    default:
      DS_AT_MSG1_ERROR("Command = %d", msg_ptr->cmd);
    }

    /* Check for response reporting requirement */
    if( result != DSAT_ASYNC_CMD && result != DSAT_ASYNC_EVENT )
    {
      /* If pending msgs in queue, set signal again */
      if( dsat_cm_msg_wm.current_cnt != 0 )
      {
#ifdef FEATURE_MODEM_RCINIT_PHASE2
        (void)rex_set_sigs( rcinit_lookup_rextask("ds"), DS_AT_CM_MSG_SIG );
#else
        (void)rex_set_sigs( &ds_tcb, DS_AT_CM_MSG_SIG );
#endif /* FEATURE_MODEM_RCINIT_PHASE2 */
      }

      break;
    }
  } /* while */

  dsatutil_free_memory( msg_ptr );

  return result;
} /* dsatcmif_signal_handler */



/*===========================================================================

FUNCTION  DSATCMIF_INIT_CMIF

DESCRIPTION
  CM Interface initialization function. This function does the following:
  - Initializes ATCOP as one of the clients to Call Manager.
  - Setup the Call Manager message watermark.
  - Registers the client call back functions.
  - Requests initial phone info (for preferences).
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
void dsatcmif_init_cmif ( void )
{
  /* Register ATCoP as a client to Call Manager */
  cmif_client_init ( );

  /* Setup the Call Manager message watermark */
  cmif_setup_msg_watermarks();
  
  /* Call the registration function to register call back functions */
  cmif_register_cb_func ( );


  /* Initialize RTRE control value */
#ifdef FEATURE_UIM_RUIM
#ifdef FEATURE_UIM_RUN_TIME_ENABLE
  switch (nv_rtre_control())
  {
    case NV_RTRE_CONTROL_USE_RUIM:
      dsat_sysinfo_cmd.dsat_rtre_config = DSAT_RTRE_CONTROL_RUIM;
      break;
    case NV_RTRE_CONTROL_NO_RUIM:
      dsat_sysinfo_cmd.dsat_rtre_config = DSAT_RTRE_CONTROL_NV;
      break;
    #ifdef FEATURE_UIM_RUIM_W_GSM_ACCESS
    case NV_RTRE_CONTROL_SIM_ACCESS:
      dsat_sysinfo_cmd.dsat_rtre_config = DSAT_RTRE_CONTROL_SIM;
      break;
    #endif  /* FEATURE_UIM_RUIM_W_GSM_ACCESS */
    default:
      DS_AT_MSG0_ERROR("Invalid RTRE control setting from NV");
      dsat_sysinfo_cmd.dsat_rtre_config = DSAT_RTRE_CONTROL_NONE;
  }
#else /* FEATURE_UIM_RUN_TIME_ENABLE */
  dsat_sysinfo_cmd.dsat_rtre_config = DSAT_RTRE_CONTROL_RUIM;
#endif /* FEATURE_UIM_RUN_TIME_ENABLE */
#else /* FEATURE_UIM_RUIM */
  dsat_sysinfo_cmd.dsat_rtre_config = DSAT_RTRE_CONTROL_NV;
#endif /* FEATURE_UIM_RUIM */


  dsat_mo_dailing_state_init();

  /* Request phone settings for preferences */
  if (TRUE != cm_ph_cmd_get_ph_info( dsatcmif_ph_cmd_cb_func,
                                     NULL,
                                     dsatcm_client_id ))
  {
    DS_AT_MSG0_ERROR("Problem requesting initial CM PH info ");
  }

  return;
}/* dsatcmif_init_cmif */



/************************ LOCAL FUNCTIONS *************************/
/*===========================================================================

FUNCTION   CMIF_CLIENT_INIT

DESCRIPTION
  Initializes ATCOP as a client with Call Manager task.
  
DEPENDENCIES
  None

RETURN VALUE
  none

SIDE EFFECTS
  None

===========================================================================*/
LOCAL void cmif_client_init ( void )
{
  cm_client_status_e_type status;

  /* Initialize ATCOP as one of the clients of CM */
  status = cm_client_init (
                           CM_CLIENT_TYPE_ATCOP,
                           &dsatcm_client_id 
                          );

  /* This should not happen, raise an error */
  if (status != CM_CLIENT_OK)
  {
    ERR_FATAL( "ATCOP unable to register as CM client: %d", 
               (int)status, 0, 0 );
  }

  return;
}/* cmif_client_init */



/*===========================================================================
FUNCTION  CMIF_CALL_EVENT_CB_FUNC

DESCRIPTION
  CM Call status event callback function

DEPENDENCIES
  None

RETURNS
  None

SIDE EFFECTS
  Adds command in DS command buffer
===========================================================================*/
LOCAL void cmif_call_event_cb_func 
(
  cm_call_event_e_type            event,            /* Event ID              */
  const cm_mm_call_info_s_type   *event_ptr         /* Pointer to Event info */
)
{
  ds_cmd_type * cmd_buf;
  ds_at_cm_call_event_type*  call_event;
  static cm_call_sups_type_e_type  dsat_sups_type;
  ASSERT (event_ptr != NULL);

  if( (event == CM_CALL_EVENT_END_REQ) ||
      (event == CM_CALL_EVENT_MOD_BEARER_IND) ||
      (event == CM_CALL_EVENT_GET_PDN_CONN_IND) ||
      (event == CM_CALL_EVENT_PDN_DISCONNECT_REJECT_IND) ||
      (event == CM_CALL_EVENT_SETUP_RES ) ||
      (event == CM_CALL_EVENT_MT_CALL_PAGE_FAIL) )
  {
    DS_AT_MSG1_LOW("%d Event not required for ATCoP client",event);
    return;
  }
  /* Filter to pass the following events... */
      /* voice/emergency calls,*/
if ( dsatcmif_is_voice_call_type(event_ptr->call_type)

      /* outgoing packet data calls(ETSI only), */
#ifdef FEATURE_DSAT_ETSI_DATA
      || ((CM_CALL_TYPE_PS_DATA == event_ptr->call_type) &&
          (CM_CALL_STATE_INCOM != event_ptr->call_state))
#endif /* FEATURE_DSAT_ETSI_DATA */

#ifdef FEATURE_DSAT_ETSI_MODE
       /* Incoming, connected or end events for CS data calls */
      || (CM_CALL_TYPE_CS_DATA == event_ptr->call_type &&
           ( CM_CALL_STATE_INCOM == event_ptr->call_state ||
             CM_CALL_STATE_CONV  == event_ptr->call_state ||
             CM_CALL_STATE_IDLE  == event_ptr->call_state ) )

       /* CM_CALL_EVENT_MNG_CALLS_CONF
          For ATCOP originated events: 
            Need to check success and update state or give CME ERROR.
          For other client:
            Need to update held calls info as no other events are posted. */
      || event == CM_CALL_EVENT_MNG_CALLS_CONF
      || event == CM_CALL_EVENT_PROGRESS_INFO_IND
      || event == CM_CALL_EVENT_ABRV_ALERT
      || event == CM_CALL_EVENT_CALL_IS_WAITING
      || event == CM_CALL_EVENT_CALL_BARRED
      || event == CM_CALL_EVENT_CALL_FORWARDED
      || event == CM_CALL_EVENT_CALL_BEING_FORWARDED
      || event == CM_CALL_EVENT_INCOM_FWD_CALL
      || event == CM_CALL_EVENT_CALL_ON_HOLD 
      || event == CM_CALL_EVENT_CALL_RETRIEVED
      || event == CM_CALL_EVENT_ORIG_FWD_STATUS
      || event == CM_CALL_EVENT_ORIG
      || event == CM_CALL_EVENT_ACT_BEARER_IND
      || event == CM_CALL_EVENT_PDN_CONN_REJ_IND
      || event == CM_CALL_EVENT_PDN_CONN_FAIL_IND
      || event == CM_CALL_EVENT_RES_ALLOC_REJ_IND
      || event == CM_CALL_EVENT_RES_ALLOC_FAIL_IND
      || event == CM_CALL_EVENT_ANSWER
#endif /* FEATURE_DSAT_ETSI_MODE */
      || event == CM_CALL_EVENT_INFO
#ifdef FEATURE_DATA_WCDMA_PS
      || event == CM_CALL_EVENT_RAB_REESTAB_IND 
#endif /* FEATURE_DATA_WCDMA_PS */
#if defined(FEATURE_HS_USB_SER3_PORT)
      || (CM_CALL_EVENT_CONNECT == event && 
          CM_CALL_TYPE_PS_DATA == event_ptr->call_type)
#endif /* defined(FEATURE_HS_USB_SER3_PORT) */
#ifdef FEATURE_DSAT_ETSI_MODE
      ||( event == CM_CALL_EVENT_SETUP_IND )
#else
      ||( event == CM_CALL_EVENT_CALLER_ID)
#endif /* FEATURE_DSAT_ETSI_MODE */
#ifdef FEATURE_DUAL_ACTIVE
      ||( event == CM_CALL_EVENT_LOCAL_HOLD )
#endif /* FEATURE_DUAL_ACTIVE */
      ||( event == CM_CALL_EVENT_PS_SIG_REL_CNF)
      ||( event == CM_CALL_EVENT_HO_COMPLETE)
      ||( event == CM_CALL_EVENT_SRVCC_COMPLETE_IND)
     )
  {
    DS_AT_MSG3_MED("ATCOP: cm event cb, event: %d, type: %d, state: %d",
            event, event_ptr->call_type, event_ptr->call_state);

    if (CM_CALL_EVENT_SUPS == event) 
    {
      if (CM_CALL_MODE_INFO_GW_CS == event_ptr->mode_info.info_type)
      {
        dsat_sups_type = event_ptr->mode_info.info.gw_cs_call.sups_type;
        DS_AT_MSG1_MED("dsat_sups_type: %d", dsat_sups_type);
      }
      return;
    }

    cmd_buf = dsat_get_cmd_buf(TRUE);
    if (NULL != cmd_buf)
    {
      /* send the message to ATCOP */
      cmd_buf->hdr.cmd_id = DS_AT_CM_CALL_INFO_CMD;
#ifdef FEATURE_DUAL_SIM
      cmd_buf->cmd.cm_cmd.subs = event_ptr->asubs_id;
#else
      cmd_buf->cmd.cm_cmd.subs = 0;
#endif /*FEATURE_DUAL_SIM*/
      call_event = (ds_at_cm_call_event_type *)dsat_alloc_memory(
                                                 sizeof(ds_at_cm_call_event_type), TRUE);
      if (NULL == call_event)
      {
        ds_release_cmd_buf(&cmd_buf);
        return;
      }

      /* If event_ptr is NULL we shouldn't get this far */
      /* Pleasing lint here                          */
      if( NULL != event_ptr )  /*lint !e774 */
      {
        call_event->event_info.dsat_info_list_avail = FALSE;
        call_event->event_info.call_client_id = (cm_client_type_e_type)event_ptr->call_client_id;
        call_event->event_info.call_type = event_ptr->call_type;
        call_event->event_info.call_state = event_ptr->call_state;

        (void) dsatutil_memscpy((void*)&call_event->event_info.dsat_num, 
                               sizeof(cm_num_s_type),
                              (void*)&event_ptr->num,
                sizeof(cm_num_s_type));
        call_event->event_info.call_direction =  event_ptr->direction;
        call_event->event_info.sys_mode =  event_ptr->sys_mode;
#ifdef FEATURE_DUAL_ACTIVE
        call_event->event_info.local_hold_state = event_ptr->local_hold_state;
#endif /* FEATURE_DUAL_ACTIVE */

#ifdef FEATURE_DSAT_ETSI_MODE
        if ( IS_ETSI_MODE(dsatcmdp_get_current_mode_per_subs(cmd_buf->cmd.cm_cmd.subs)) )
        {
         (void) dsatutil_memscpy((void*)&call_event->event_info.dsat_mode_info, 
                                sizeof(cm_call_mode_info_s_type),
                               (void*)&event_ptr->mode_info,
                sizeof(cm_call_mode_info_s_type));
         call_event->event_info.csd_rate_adaption = 
                event_ptr->mode_info.info.gw_cs_call.bearer_capability_1.other_rate_adaption;
         call_event->event_info.end_status = event_ptr->end_status;
         call_event->event_info.call_direction = event_ptr->direction;
         if ( CM_CALL_EVENT_MNG_CALLS_CONF == event )
          {
            /* CM no longer reports info_type: as this code wrapped in ETSI
               feature define can assume GW and only CS events of this type
               allowed through */
            if (event_ptr->mode_info.info_type == CM_CALL_MODE_INFO_GW_CS)
            {
              call_event->event_info.success = 
                       event_ptr->mode_info.info.gw_cs_call.call_ss_success;
            }
            else if (event_ptr->mode_info.info_type == CM_CALL_MODE_INFO_IP)
            {
              call_event->event_info.success = 
                         event_ptr->mode_info.info.ip_call.call_ss_success;
              dsat_sups_type = event_ptr->mode_info.info.ip_call.sups_type;
            }
            DS_AT_MSG3_HIGH("mode_info:%d, ss_success:%d, sups_type:%d", event_ptr->mode_info.info_type,
                                                                         call_event->event_info.success,
                                                                         dsat_sups_type);
            call_event->event_info.active_calls_list =
                       event_ptr->mode_info.info.gw_cs_call.active_calls_list;
            call_event->event_info.call_id = event_ptr->call_id;
            /* This occurs if another call is dialed via UI or ATD when a call
               is already up and corresponds to the case
               'Entering "Directory Number"' in 22.030 section 6.5.5.1.
               Enum cm_call_sups_type_e_type has no value to describe this 
               case. Use CM_CALL_SUPS_TYPE_SELECT_CALL_X */
            if ( event_ptr->call_state == CM_CALL_STATE_ORIG )
            {
              call_event->event_info.sups_type = CM_CALL_SUPS_TYPE_SELECT_CALL_X;
            }
            else
            {
              /* Assume that this manage calls confirmation event is for the 
                 supplementary services command */
              call_event->event_info.sups_type = dsat_sups_type;
            }
          }
          else
          {
            /* Copy nsapi and its validity indicator into command */
            if ( event_ptr->mode_info.info_type == CM_CALL_MODE_INFO_GW_PS )
            {
              DS_AT_MSG2_HIGH("nsapi valid = %d, nsapi = %d",
                event_ptr->mode_info.info.gw_ps_call.nsapi.valid,
                event_ptr->mode_info.info.gw_ps_call.nsapi.nsapi);
              call_event->event_info.nsapi_valid = event_ptr->mode_info.info.gw_ps_call.nsapi.valid;
              call_event->event_info.nsapi = event_ptr->mode_info.info.gw_ps_call.nsapi.nsapi;
              call_event->event_info.profile_number = event_ptr->mode_info.info.gw_ps_call.profile_number;
            }
            call_event->event_info.call_id = event_ptr->call_id;
          }
        }
        else
#endif /* FEATURE_DSAT_ETSI_MODE */
        {
          call_event->event_info.call_id = event_ptr->call_id;
        }
      }
      call_event->event      = event;
      cmd_buf->cmd.cm_cmd.sub_cmd = call_event;
      ds_put_cmd(cmd_buf);
    }
  }
} /* cmif_call_event_cb_func */

/*===========================================================================
FUNCTION  CMIF_CM_PH_EVENT_CB_FUNC

DESCRIPTION
  CM PH event callback function

DEPENDENCIES
  None

RETURNS
  None

SIDE EFFECTS
  Adds command in DS command buffer

===========================================================================*/
/* ARGSUSED */
LOCAL void cmif_cm_ph_event_cb_func 
(
  cm_ph_event_e_type         event,            /* Event ID              */
  const cm_ph_info_s_type   *event_ptr         /* Pointer to Event info */
)
{
  ds_cmd_type * cmd_buf;
  ds_at_cm_ph_event_type *ph_event = NULL;
  uint8 index;
  ASSERT (event_ptr != NULL);

  DS_AT_MSG3_HIGH("IN cmif_cm_ph_event_cb_func oprt mode = %d  event = %d Subs=%d",
                  event_ptr->oprt_mode,event,event_ptr->asubs_id);

  /* Filter events to DS task */
  switch (event)
  {
  case CM_PH_EVENT_OPRT_MODE:
  case CM_PH_EVENT_INFO:
  case CM_PH_EVENT_SYS_SEL_PREF:
  case CM_PH_EVENT_SUBSCRIPTION_NOT_AVAILABLE:
  case CM_PH_EVENT_SUBSCRIPTION_AVAILABLE:
  case CM_PH_EVENT_AVAILABLE_NETWORKS_CONF:
  case CM_PH_EVENT_PREFERRED_NETWORKS_CONF:
  case CM_PH_EVENT_FUNDS_LOW:
  case CM_PH_EVENT_TERMINATE_GET_NETWORKS:
#ifdef FEATURE_ETSI_ATTACH
  case CM_PH_EVENT_PS_ATTACH_FAILED:
#endif /* FEATURE_ETSI_ATTACH */
#if defined(FEATURE_HS_USB_SER3_PORT)
  case CM_PH_EVENT_PACKET_STATE:
#endif /* defined(FEATURE_HS_USB_SER3_PORT) */
  case CM_PH_EVENT_STANDBY_SLEEP:
  case CM_PH_EVENT_STANDBY_WAKE:
#ifdef FEATURE_DATA_IS707
  case CM_PH_EVENT_NVRUIM_CONFIG_CHANGED:
#endif /* FEATURE_DATA_IS707 */
#ifdef FEATURE_DUAL_SIM
  case CM_PH_EVENT_DUAL_STANDBY_PREF:
#endif /* FEATURE_DUAL_SIM */
#ifdef FEATURE_DSAT_LTE
  case CM_PH_EVENT_GET_DRX_CNF:
  case CM_PH_EVENT_SET_DRX_CNF:
#endif/*FEATURE_DSAT_LTE*/
#ifdef FEATURE_SGLTE
  case CM_PH_EVENT_SIM_AVAILABLE:
#endif/* FEATURE_SGLTE*/
    /* Post event to DSTASK */
    cmd_buf = dsat_get_cmd_buf(FALSE);
    /* Send the message to ATCOP */
    cmd_buf->hdr.cmd_id = DS_AT_CM_PH_INFO_CMD;
    cmd_buf->cmd.cm_cmd.type = DS_AT_CM_PH_INFO_CMD;
    ph_event = (ds_at_cm_ph_event_type *)dsat_alloc_memory(
                                           sizeof(ds_at_cm_ph_event_type), FALSE);
    ph_event->event = event;
    index = PRIMA_SUBS; /*Always keep value at 0th index */
#ifdef FEATURE_DUAL_SIM
    cmd_buf->cmd.cm_cmd.subs = event_ptr->asubs_id;
#else
    cmd_buf->cmd.cm_cmd.subs = SYS_MODEM_AS_ID_1;
#endif /* FEATURE_DUAL_SIM */
    
    switch (event)
    {
#ifdef FEATURE_DSAT_ETSI_MODE
    case CM_PH_EVENT_AVAILABLE_NETWORKS_CONF:
      DS_AT_MSG1_MED("available_networks_list_cnf = %d",event_ptr->available_networks_list_cnf);

     /* Copy the PLMN list only if list_cnf is SYS_PLMN_LIST_SUCCESS else treat the lenght as -1*/        
      if(event_ptr->available_networks_list_cnf == 
          SYS_PLMN_LIST_SUCCESS)
      {
        ph_event->event_info.available_networks[index] = event_ptr->available_networks;
      }
      else
      {
        ph_event->event_info.available_networks[index].length = INVALID_AVAIL_NET_LENGTH;
      }
      break;
      
    case CM_PH_EVENT_PREFERRED_NETWORKS_CONF:
      ph_event->event_info.user_pref_networks[index] = event_ptr->user_pref_networks;
      break;
#endif /* FEATURE_DSAT_ETSI_MODE  */
      
    case CM_PH_EVENT_INFO:
    case CM_PH_EVENT_SYS_SEL_PREF:
#ifdef FEATURE_DSAT_ETSI_MODE 
    case CM_PH_EVENT_SUBSCRIPTION_NOT_AVAILABLE:
    case CM_PH_EVENT_SUBSCRIPTION_AVAILABLE:
#ifdef FEATURE_SGLTE
  case CM_PH_EVENT_SIM_AVAILABLE:	  
#endif/* FEATURE_SGLTE*/
#ifdef FEATURE_DSAT_LTE
       if( event == CM_PH_EVENT_INFO )
       {
         ph_event->event_info.eps_mode = event_ptr->lte_ue_mode_pref;
         DS_AT_MSG1_HIGH(" EPS UE MODE - %d ",event_ptr->lte_ue_mode_pref);
       }
#endif /* FEATURE_DSAT_LTE */
      /* Backup the previous MS service selection preferences before 
         overwriting with the new one */
      ph_event->event_info.dsat_srv_domain_pref = event_ptr->srv_domain_pref ;        
      ph_event->event_info.pref_info[index].service_domain_pref= event_ptr->srv_domain_pref;
      ph_event->event_info.pref_info[index].network_sel_mode_pref = event_ptr->network_sel_mode_pref;
      ph_event->event_info.pref_info[index].network_rat_mode_pref = event_ptr->mode_pref;
      ph_event->event_info.pref_info[index].network_roam_pref = event_ptr->roam_pref;
      ph_event->event_info.pref_info[index].network_band_pref = event_ptr->band_pref;
      ph_event->event_info.pref_info[index].acq_order_pref = event_ptr->acq_order_pref;
      ph_event->event_info.pref_info[index].acq_pri_order_pref = event_ptr->acq_pri_order_pref;

#ifdef CM_API_AOC_INFO
      ph_event->event_info.aoc_info[index] = event_ptr->aoc_info;
#endif /* CM_API_AOC_INFO */
#endif /* FEATURE_DSAT_ETSI_MODE */

      ph_event->event_info.oprt_mode = event_ptr->oprt_mode;
      if ( (CHECK_NOT_PENDING(DSAT_VENDOR_PREFMODE_IDX,0,DSAT_PENDING_PREFMODE_NONE)) ||
           (CHECK_NOT_PENDING(DSAT_VENDOR_QCBANDPREF_IDX,0,DSAT_PENDING_PREFMODE_NONE)))
      {
        ph_event->event_info.pref_info[index].network_rat_mode_pref = event_ptr->mode_pref;
        ph_event->event_info.pref_info[index].network_band_pref = event_ptr->band_pref;
      }

      /* Subscription available info */
      ph_event->event_info.is_gw_subscription_available = 
                                  event_ptr->is_gw_subscription_available;
      ph_event->event_info.is_hybr_gw_subscription_available = 
      	                           event_ptr->is_hybr_gw_subscription_available;
      ph_event->event_info.is_hybr_gw3_subscription_available = 
                                  event_ptr->is_hybr_gw3_subscription_available;

      break;

#if defined(FEATURE_HS_USB_SER3_PORT)
    case CM_PH_EVENT_PACKET_STATE:
      ph_event->event_info.packet_state = event_ptr->packet_state;
      break;
#endif /* defined(FEATURE_HS_USB_SER3_PORT) */
    case CM_PH_EVENT_STANDBY_SLEEP:
    case CM_PH_EVENT_STANDBY_WAKE:
      break;
#ifdef FEATURE_DATA_IS707
    case CM_PH_EVENT_NVRUIM_CONFIG_CHANGED:
      ph_event->event_info.dsat_rtre_config = (uint16) event_ptr->rtre_control;
      break;
    case CM_PH_EVENT_OPRT_MODE:
      ph_event->event_info.oprt_mode = event_ptr->oprt_mode;
      break;
#endif /*FEATURE_DATA_IS707*/
#ifdef FEATURE_DUAL_SIM
   case CM_PH_EVENT_DUAL_STANDBY_PREF:
      ph_event->event_info.default_data_subs = event_ptr->default_data_subs;
      break;
#endif /* FEATURE_DUAL_SIM */
#ifdef FEATURE_DSAT_LTE
    case CM_PH_EVENT_GET_DRX_CNF:
      DS_AT_MSG1_HIGH("drx_coefficient = %d", event_ptr->drx_coefficient);
      ph_event->event_info.drx_coefficient = event_ptr->drx_coefficient;
      break;
    case CM_PH_EVENT_SET_DRX_CNF:
      DS_AT_MSG1_HIGH("set_drx_result = %d", event_ptr->set_drx_result);
      ph_event->event_info.set_drx_result = event_ptr->set_drx_result;
      break;
#endif /*FEATURE_DSAT_LTE*/
    default:  
      ph_event->event_info.oprt_mode = event_ptr->oprt_mode;
      break;
    }

    ph_event->event_info.device_mode = event_ptr->device_mode;

    cmd_buf->cmd.cm_cmd.sub_cmd = ph_event;

    ds_put_cmd(cmd_buf);
    break;

  default:
    DS_AT_MSG1_ERROR("Unsupported CM PH event: %d", event);
  }
  return;
} /* cmif_cm_ph_event_cb_func */



/*===========================================================================
FUNCTION  CMIF_CALL_INFO_LIST_CB_FUNC

DESCRIPTION
  CM Call info list query callback function

DEPENDENCIES
  None

RETURNS
  None

SIDE EFFECTS
  Adds command in DS command buffer
===========================================================================*/
LOCAL void cmif_call_info_list_cb_func
(
  const cm_call_state_info_list_s_type    *list_ptr
)
{
  ds_cmd_type * cmd_buf;
  ds_at_cm_call_event_type*  call_event = NULL;
    
  ASSERT (list_ptr != NULL);

  cmd_buf = dsat_get_cmd_buf(FALSE);
  /* send the message to ATCOP */
  cmd_buf->hdr.cmd_id = DS_AT_CM_CALL_INFO_CMD;
  call_event = (ds_at_cm_call_event_type *)dsat_alloc_memory(
                                             sizeof(ds_at_cm_call_event_type), FALSE);
  (void) dsatutil_memscpy((void*)&call_event->event_info.dsat_cm_call_info_list, 
                           sizeof(cm_call_state_info_list_s_type),
                          (void*)list_ptr,
          sizeof(cm_call_state_info_list_s_type));
  call_event->event_info.dsat_info_list_avail = TRUE;
  call_event->event =  CM_CALL_EVENT_INFO;
  cmd_buf->cmd.cm_cmd.sub_cmd = call_event;
  ds_put_cmd(cmd_buf);
} /* cmif_call_info_list_cb_func */

/*===========================================================================

FUNCTION  DSAT_SET_NET_SEL_MODE_FROM_NV

DESCRIPTION
   This function processes read Network selection mode from NV item . 
   and update +cops state.
DEPENDENCIES
  None

RETURN VALUE
   None
SIDE EFFECTS
  None

===========================================================================*/


LOCAL void dsat_set_net_sel_mode_from_nv
(
  ds_subs_e_type                 subs_info
)
{
  dsat_num_item_type net_select_mode = 0;
  nv_item_type nv_item;
  nv_stat_enum_type  nv_stat = NV_DONE_S;
  
  memset ((void*)&nv_item, 0, sizeof(nv_item));      
  nv_stat = dsatutil_get_nv_item( NV_NET_SEL_MODE_PREF_I, &nv_item );
  
  if (nv_stat == NV_DONE_S)
  {
    net_select_mode = nv_item.net_sel_mode_pref.net_sel_mode;
    DS_AT_MSG1_HIGH(" Network selection mode from NV %d", net_select_mode);
    DSATUTIL_SET_VAL(DSATETSI_EXT_ACT_COPS_ETSI_IDX,subs_info,0,0,
       (dsat_num_item_type)net_select_mode,MIX_NUM_TYPE)
  }

} /* dsat_set_net_sel_mode_from_nv*/

/*===========================================================================

FUNCTION  CMIF_PROCESS_CM_NET_REG_DATA

DESCRIPTION
  This function processes the Call Manager registration event data to
  set network registration state information.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
LOCAL void cmif_process_cm_net_reg_data
(
  const ds_at_cm_ss_event_type *event_ptr,        /* DS event pointer */
  dsat_stack_id_e_type          stack_id,
  ds_subs_e_type                subs_info
)
{
  dsatcmif_servs_state_ms_info *ph_ss_ms_val = NULL;
  dsat_net_reg_state_s_type    *net_reg_state_ptr = NULL;

  if ( ( NULL == event_ptr ) ||
       ( FALSE == IS_VALID_STACK_ID(stack_id) ) ||
       ( FALSE == IS_VALID_SUBS_INFO(subs_info) ) )
  {
    DS_AT_MSG3_ERROR("Invalid Arg: event_ptr = %p, stack_id = %d, net_id = %d",
                     event_ptr, stack_id, subs_info);
    return;
  }

  DS_AT_MSG3_HIGH("PLMN structure: 0x%X 0x%X 0x%X",
                  event_ptr->event_info[stack_id].sys_id.id.plmn.identity[0],
                  event_ptr->event_info[stack_id].sys_id.id.plmn.identity[1],
                  event_ptr->event_info[stack_id].sys_id.id.plmn.identity[2]);

  if(DSAT_FAILURE == dsatutil_get_base_addr(DSAT_SS_PH_MS_VALS, (void **)&ph_ss_ms_val, subs_info))
  {
    return;
  }
  net_reg_state_ptr = &ph_ss_ms_val->dsat_net_reg_state;

  /* Check for full service and capture operator information.
     If it is a non-full service, the COPS read command should not show
     the PLMN as it is not registerd so reset the PLMN */
  if ( SYS_SRV_STATUS_SRV == event_ptr->event_info[stack_id].srv_status )
  {
    #define COPS_NULL_PLMN 0xFF
    const sys_mm_information_s_type *mode_info_ptr =
      &event_ptr->event_info[stack_id].mm_info;
      net_reg_state_ptr->net_id_info.csg_id = event_ptr->event_info[stack_id].csg_id;

    /* Update locally cached network identifier info */
    if( COPS_NULL_PLMN != event_ptr->event_info[stack_id].sys_id.id.plmn.identity[0] )
    {
      net_reg_state_ptr->net_id_info.present = TRUE;
      /* sys_id.id is a union, so plmn and plmn_lac would have valid
         values. for readibility, copy it as they are differnt. */
      net_reg_state_ptr->net_id_info.plmn =
        event_ptr->event_info[stack_id].sys_id.id.plmn;

      /* For now keep it though it is redundant information */
      (void) dsatutil_memscpy((void*)&net_reg_state_ptr->net_id_info.plmn_lac,
                              sizeof(sys_plmn_lac_id_s_type),
                              (void*)&event_ptr->event_info[stack_id].sys_id.id.plmn_lac,
                              sizeof(sys_plmn_lac_id_s_type));
#ifdef FEATURE_DSAT_LTE
      /* In LTE, LAC is TAC */
      if(event_ptr->event_info[stack_id].sys_mode == SYS_SYS_MODE_LTE)
      {
        net_reg_state_ptr->net_id_info.plmn_lac.lac = event_ptr->event_info[stack_id].lte_tac;
      }
#endif /* FEATURE_DSAT_LTE */
    }
    else
    {
      net_reg_state_ptr->net_id_info.present = FALSE;
    }

    /* Capture operator name information if available. */
    if (TRUE == mode_info_ptr->full_name_avail)
    {
      (void) dsatutil_memscpy((void*)&net_reg_state_ptr->net_id_info.full_name,
                              sizeof(sys_network_full_name_s_type),
                              (void*)&mode_info_ptr->full_name,
                              sizeof(sys_network_full_name_s_type));
      net_reg_state_ptr->net_id_info.full_name.
        name[DSAT_MAX_FULLNAME_STR_LEN]='\0';
    }
    if (TRUE == mode_info_ptr->short_name_avail)
    {
      (void) dsatutil_memscpy((void*)&net_reg_state_ptr->net_id_info.short_name,
                               sizeof(sys_network_short_name_s_type),
                              (void*)&mode_info_ptr->short_name,
                               sizeof(sys_network_short_name_s_type));
      net_reg_state_ptr->net_id_info.short_name.
        name[DSAT_MAX_SHORTNAME_STR_LEN]='\0';
    }

    net_reg_state_ptr->sys_mode = event_ptr->event_info[stack_id].sys_mode;

    /* Derive AcT based on system mode */
    switch ( event_ptr->event_info[stack_id].sys_mode )
    {
      case SYS_SYS_MODE_WCDMA:
        net_reg_state_ptr->act = DSAT_COPS_ACT_UTRAN;
        net_reg_state_ptr->csg_rat = DSAT_COPS_CSG_RAT_UMTS;
        break;
#ifdef FEATURE_TDSCDMA
      case SYS_SYS_MODE_TDS:
      net_reg_state_ptr->act = DSAT_COPS_ACT_UTRAN;
        net_reg_state_ptr->csg_rat = DSAT_COPS_CSG_RAT_TDS;
      break;
#endif /* FEATURE_TDSCDMA */

    case SYS_SYS_MODE_GSM:
      net_reg_state_ptr->act = DSAT_COPS_ACT_GSM;
        net_reg_state_ptr->csg_rat = DSAT_COPS_CSG_RAT_GSM;
      break;
#ifdef FEATURE_DSAT_LTE
    case SYS_SYS_MODE_LTE:
      net_reg_state_ptr->act = DSAT_COPS_ACT_EUTRAN;
      net_reg_state_ptr->csg_rat= DSAT_COPS_CSG_RAT_LTE;
      break;
#endif /* FEATURE_DSAT_LTE */
    default:
      net_reg_state_ptr->act = DSAT_COPS_ACT_MAX;
      net_reg_state_ptr->csg_rat = DSAT_COPS_CSG_RAT_MAX;
      break;
    }
  }
  /* Clear network identifier on non-full service */
  else
  {
    /* Clean PLMN & network name information */
    memset((void*)&net_reg_state_ptr->net_id_info, 0x0,
           sizeof(net_reg_state_ptr->net_id_info));

    net_reg_state_ptr->sys_mode = SYS_SYS_MODE_NO_SRV;

    net_reg_state_ptr->act = DSAT_COPS_ACT_MAX;
    net_reg_state_ptr->csg_rat = DSAT_COPS_CSG_RAT_MAX;
    net_reg_state_ptr->net_id_info.csg_id = SYS_CSG_ID_INVALID; 
  }

  return;
} /* cmif_process_cm_net_reg_data */


/*===========================================================================

FUNCTION  CMIF_PROCESS_CM_REG_DATA

DESCRIPTION
  This function processes the Call Manager registration event data to
  set PS attach and general network registration state information.

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR :    if there was any problem in executing the command
    DSAT_OK :       if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
LOCAL dsat_result_enum_type cmif_process_cm_reg_data
(
  const ds_at_cm_ss_event_type * event_ptr,        /* DS event pointer */
  dsat_stack_id_e_type           stack_id,
  ds_subs_e_type                 subs_info 
)
{
  #define PLMN_NULL 0xFF
  dsat_result_enum_type result = DSAT_ASYNC_EVENT;
  dsatcmif_servs_state_ms_info  *ph_ss_ms_val = NULL;
  sys_srv_domain_e_type          domain;
  sys_roam_status_e_type         roaming;
  if ( ( NULL == event_ptr ) ||
       ( FALSE == IS_VALID_STACK_ID(stack_id) ) ||
       ( FALSE == IS_VALID_SUBS_INFO(subs_info) ) )
  {
    DS_AT_MSG3_ERROR("Invalid Arg: event_ptr = %p, stack_id = %d, subs_info = %d",
                     event_ptr, stack_id, subs_info);
    return result;
  }
  domain = event_ptr->event_info[stack_id].srv_domain;
  roaming = event_ptr->event_info[stack_id].roam_status;

#ifdef FEATURE_DSAT_ETSI_MODE
#ifdef FEATURE_DSAT_DEV_CMDS
  /* Update CNTI technology as NONE as UE has not camped yet*/
  dsat_qccnti_tech = DSAT_CNTI_NONE;
#endif /* FEATURE_DSAT_DEV_CMDS */
#endif /* FEATURE_DSAT_ETSI_MODE */

  DS_AT_MSG4_HIGH("Processing registration update: chgfld=%p %p srv=%d dom=%d",
                  ((event_ptr->event_info[stack_id].changed_fields) >> 32),
                  event_ptr->event_info[stack_id].changed_fields,
                  event_ptr->event_info[stack_id].srv_status, domain);

#ifdef FEATURE_DSAT_UMTS_UNDP 
  /* Update CNTI technology as NONE as it has not camped yet*/
  dsat_qccnti_tech = DSAT_CNTI_NONE;
#endif

#ifdef FEATURE_TDSCDMA
  cmif_report_mode( event_ptr->event_info[stack_id].srv_status,
                    domain,
                    event_ptr->event_info[stack_id].sys_mode,
                    (ds_subs_e_type)subs_info);
#endif /* FEATURE_TDSCDMA */

  DSATUTIL_EVENT_GET_BASE_ADDR(DSAT_SS_PH_MS_VALS,(void **)&ph_ss_ms_val,subs_info);

  /* Check for network service. */
  /* Use the registration domain (CS,PS,CS+PS) to set the various
   * indicators within the subsystem. */
  if ( SYS_SRV_STATUS_NO_SRV != event_ptr->event_info[stack_id].srv_status &&
       SYS_SRV_STATUS_PWR_SAVE != event_ptr->event_info[stack_id].srv_status )
  {
    /* Update camped-only state */
    /* DOMAIN_CAMPED is not exactly registration success */
    /* In limited service and no domain, update the net domain. */
    if ( (SYS_SRV_DOMAIN_CAMPED == domain) ||
         (SYS_SRV_DOMAIN_NO_SRV == domain) )
    {
#ifdef FEATURE_ETSI_ATTACH
      /* Set PS attach state */
      DSATUTIL_SET_VAL(DSATETSI_EXT_ACT_CGATT_ETSI_IDX,subs_info,0,0,0,NUM_TYPE)
#endif /*  FEATURE_ETSI_ATTACH */
    }
    /* Update CS+PS registration */
    else if (SYS_SRV_DOMAIN_CS_PS == domain)
    {
#ifdef FEATURE_ETSI_ATTACH
      /* Set PS attach state */
      DSATUTIL_SET_VAL(DSATETSI_EXT_ACT_CGATT_ETSI_IDX,subs_info,0,0,1,NUM_TYPE)
#endif /* FEATURE_ETSI_ATTACH */

      /* Set CS+PS registration state */
      if( SYS_SRV_STATUS_SRV == event_ptr->event_info[stack_id].srv_status )
      {
#ifdef FEATURE_DSAT_DEV_CMDS
#ifdef FEATURE_DSAT_ETSI_MODE
        if ( event_ptr->event_info[stack_id].sys_mode == SYS_SYS_MODE_GSM )
        {
          if ( event_ptr->event_info[stack_id].cell_srv_ind.egprs_supp 
                                          == SYS_EGPRS_SUPPORT_AVAIL )
          {
            dsat_qccnti_tech = DSAT_CNTI_EGPRS;
          }
          else 
          {
            dsat_qccnti_tech = DSAT_CNTI_GPRS;
          }
        }
        else if ( (event_ptr->event_info[stack_id].sys_mode == SYS_SYS_MODE_WCDMA) ||
                  (event_ptr->event_info[stack_id].sys_mode == SYS_SYS_MODE_TDS) )
        {
          if(event_ptr->event_info[stack_id].cell_srv_ind.hs_ind == SYS_HS_IND_HSDPA_SUPP_CELL)
          {
            dsat_qccnti_tech = DSAT_CNTI_HSDPA;
          }
          else if (event_ptr->event_info[stack_id].cell_srv_ind.hs_ind == SYS_HS_IND_HSUPA_SUPP_CELL)
          {
            dsat_qccnti_tech = DSAT_CNTI_HSUPA;
          }
          else if (event_ptr->event_info[stack_id].cell_srv_ind.hs_ind == SYS_HS_IND_HSDPA_HSUPA_SUPP_CELL)
          {
            dsat_qccnti_tech = DSAT_CNTI_HSDPA_HSUPA;
          }
          else
          {
            dsat_qccnti_tech = DSAT_CNTI_UMTS;
          }
        }
#endif /* FEATURE_DSAT_ETSI_MODE */
#endif /* FEATURE_DSAT_DEV_CMDS */
      }
    }
    /* Update CS only registration */
    else if (SYS_SRV_DOMAIN_CS_ONLY == domain)
    {
      /* Set CS registration state */
      if( SYS_SRV_STATUS_SRV == event_ptr->event_info[stack_id].srv_status )
      {
#ifdef FEATURE_DSAT_DEV_CMDS
#ifdef FEATURE_DSAT_ETSI_MODE
        if ( event_ptr->event_info[stack_id].sys_mode == SYS_SYS_MODE_GSM )
        {
          dsat_qccnti_tech = DSAT_CNTI_GSM;
        }
        else if ( (event_ptr->event_info[stack_id].sys_mode == SYS_SYS_MODE_WCDMA)||
                  (event_ptr->event_info[stack_id].sys_mode == SYS_SYS_MODE_TDS) )
        {
          dsat_qccnti_tech = DSAT_CNTI_UMTS;
        }
#endif /* FEATURE_DSAT_ETSI_MODE */
#endif /* FEATURE_DSAT_DEV_CMDS */
      }
#ifdef FEATURE_ETSI_ATTACH
      /* Set PS attach state */
      DSATUTIL_SET_VAL(DSATETSI_EXT_ACT_CGATT_ETSI_IDX,subs_info,0,0,0,NUM_TYPE)
#endif /*  FEATURE_ETSI_ATTACH */
    }
    /* Update PS only registration */
    else if (SYS_SRV_DOMAIN_PS_ONLY == domain)
    {
#ifdef FEATURE_ETSI_ATTACH
      /* Set PS attach state */
      DSATUTIL_SET_VAL(DSATETSI_EXT_ACT_CGATT_ETSI_IDX,subs_info,0,0,1,NUM_TYPE)
#endif /*  FEATURE_ETSI_ATTACH */
    
      /* Set PS registration state */
      if( SYS_SRV_STATUS_SRV == event_ptr->event_info[stack_id].srv_status )
      {
      
#ifdef FEATURE_DSAT_DEV_CMDS
#ifdef FEATURE_DSAT_ETSI_MODE
        if ( event_ptr->event_info[stack_id].sys_mode == SYS_SYS_MODE_GSM)
        {
          if ( event_ptr->event_info[stack_id].cell_srv_ind.egprs_supp 
                                          == SYS_EGPRS_SUPPORT_AVAIL )
          {
            dsat_qccnti_tech = DSAT_CNTI_EGPRS;
          }
          else 
          {
            dsat_qccnti_tech = DSAT_CNTI_GPRS;
          }
        }
        else if ( (event_ptr->event_info[stack_id].sys_mode == SYS_SYS_MODE_WCDMA) ||
                  (event_ptr->event_info[stack_id].sys_mode == SYS_SYS_MODE_TDS) )
        {
          if(event_ptr->event_info[stack_id].cell_srv_ind.hs_ind == SYS_HS_IND_HSDPA_SUPP_CELL)
          {
            dsat_qccnti_tech = DSAT_CNTI_HSDPA;
          }
          else if (event_ptr->event_info[stack_id].cell_srv_ind.hs_ind == SYS_HS_IND_HSUPA_SUPP_CELL)
          {
            dsat_qccnti_tech = DSAT_CNTI_HSUPA;
          }
          else if (event_ptr->event_info[stack_id].cell_srv_ind.hs_ind == SYS_HS_IND_HSDPA_HSUPA_SUPP_CELL)
          {
            dsat_qccnti_tech = DSAT_CNTI_HSDPA_HSUPA;
          }
          else
          {
            dsat_qccnti_tech = DSAT_CNTI_UMTS;
          }
        }
#endif /* FEATURE_DSAT_ETSI_MODE */
#endif /* FEATURE_DSAT_DEV_CMDS */
      }
    }

#ifdef FEATURE_DSAT_ETSI_MODE

    if( SYS_SRV_STATUS_SRV == event_ptr->event_info[stack_id].srv_status )
    {
      #define COPS_NULL_PLMN 0xFF

      /* ATcoP will receive Full service before Subscription available
         AtcoP should change it's +COPS mode*/
      
      if ( DSAT_COPS_MODE_DEREG == (dsat_num_item_type)dsatutil_get_val(
                           DSATETSI_EXT_ACT_COPS_ETSI_IDX,subs_info,0,MIX_NUM_TYPE))
      {
        dsat_set_net_sel_mode_from_nv(subs_info);
        
      }

      if( COPS_NULL_PLMN != event_ptr->event_info[stack_id].sys_id.id.plmn.identity[0] )
      {
         /* Update PLMN for emergency list update*/
         (void) dsatutil_memscpy((void*)&ph_ss_ms_val->dsat_cen_num_list.plmn, 
                                  sizeof(sys_plmn_id_s_type),
                                 (void*)&event_ptr->event_info[stack_id].sys_id.id.plmn,
               sizeof(sys_plmn_id_s_type));
         ph_ss_ms_val->dsat_cen_num_list.plmn_num_flag |= DSAT_CEN_PLMN_PRESENT;
      }

      /* Update service indicator */
      (void)dsatetsime_change_indicator_state( DSAT_CIND_INDEX_SERVICE, 
                                               DSAT_CIND_BINARY_SET,subs_info );
    }
    else
    {
      /* Update service indicator */
      (void)dsatetsime_change_indicator_state( DSAT_CIND_INDEX_SERVICE,
                                               DSAT_CIND_BINARY_UNSET,subs_info  );
    }

    /* Update +CIND status indicators */
    (void)dsatetsime_change_indicator_state(
              DSAT_CIND_INDEX_ROAM,
              ((SYS_ROAM_STATUS_OFF == roaming)? 
                DSAT_CIND_BINARY_UNSET : DSAT_CIND_BINARY_SET),subs_info  );
    (void)dsatetsime_change_indicator_state(
              DSAT_CIND_INDEX_PACKET,
              ( ( 1 == (dsat_num_item_type)dsatutil_get_val(
                          DSATETSI_EXT_ACT_CGATT_ETSI_IDX,subs_info,0,NUM_TYPE) )?
                DSAT_CIND_BINARY_SET : DSAT_CIND_BINARY_UNSET ),subs_info  );
    
#endif /* FEATURE_DSAT_ETSI_MODE */
  }
  /* Update no service state, thus no registration */
  else 
  {
#ifdef FEATURE_ETSI_ATTACH
    /* Set PS attach state */
    DSATUTIL_SET_VAL(DSATETSI_EXT_ACT_CGATT_ETSI_IDX,subs_info,0,0,0,NUM_TYPE)
#endif /*  FEATURE_ETSI_ATTACH */
#ifdef FEATURE_DSAT_ETSI_MODE

    /* Update service indicator */
    (void)dsatetsime_change_indicator_state( DSAT_CIND_INDEX_SERVICE, 0, subs_info );
    (void)dsatetsime_change_indicator_state( DSAT_CIND_INDEX_PACKET, 0, subs_info );
    (void)dsatetsime_change_indicator_state( DSAT_CIND_INDEX_ROAM, 0, subs_info );

#endif /* FEATURE_DSAT_ETSI_MODE */

  }

#ifdef FEATURE_DSAT_ETSI_MODE
  /* Check for active +COPS command */
  DS_AT_MSG2_HIGH("COPS: state=%d reg_mode=%d",
            ph_ss_ms_val->dsatetsicall_network_list.cmd_state,
            ph_ss_ms_val->dsatetsicall_network_list.requested_pref.mode);
  if ( ( DSAT_COPS_ASTATE_WAIT == ph_ss_ms_val->dsatetsicall_network_list.cmd_state ) ||
       ( DSAT_COPS_ASTATE_WAIT_2 == ph_ss_ms_val->dsatetsicall_network_list.cmd_state ) )
  {
    /* 
       Match of req_id will confirm the associated SS event to look for.
     */
    if(event_ptr->event_info[stack_id].cm_req_id == DSAT_CM_REQ_ID)
    {
      DS_AT_MSG1_HIGH ("REQ_ID: %d Match",event_ptr->event_info[stack_id].cm_req_id);

#ifdef FEATURE_SGLTE
      if ( ( DSAT_COPS_ASTATE_WAIT == ph_ss_ms_val->dsatetsicall_network_list.cmd_state ) &&
           ( dsat_qcsimapp_info[subs_info].is_sglte_sub )&&
           ( TRUE == event_ptr->event_info[DSAT_STACK_HYBR].is_operational ) )
      {
        ph_ss_ms_val->dsatetsicall_network_list.cmd_state = DSAT_COPS_ASTATE_WAIT_2;
      }
      else
#endif /* FEATURE_SGLTE */
      {
        switch( event_ptr->event_info[stack_id].srv_status )
        {
          case SYS_SRV_STATUS_PWR_SAVE:
          case SYS_SRV_STATUS_NO_SRV:
          case SYS_SRV_STATUS_LIMITED:
          case SYS_SRV_STATUS_LIMITED_REGIONAL:
          if( DSAT_COPS_MODE_MANAUTO ==
              ph_ss_ms_val->dsatetsicall_network_list.requested_pref.mode )
          {
    /* Change to auto registration on mismatch. Set COPS mode accordingly */
            DS_AT_MSG0_HIGH ("COPS manual reg PLMN mismatch, changing to automatic");
            ph_ss_ms_val->dsatetsicall_network_list.cmd_state = DSAT_COPS_ASTATE_PREF;
            /* in case of QCCOPS plmn need to be paased. will try to camp on passed PLMN first 
               in auto mode*/
            if(ph_ss_ms_val->dsatetsicall_network_list.cmd_idx == CMD_IDX_QCCOPS ||
               ph_ss_ms_val->dsatetsicall_network_list.cmd_idx == CMD_IDX_QCCSGCOPS)
            {
              result =
              dsatetsicmif_change_network_registration( DSAT_COPS_MODE_AUTO,
                                 &(ph_ss_ms_val->net_pref.plmn),
                                 DSAT_COPS_ACT_AUTO,subs_info );
            }
            else
            {
            result =
              dsatetsicmif_change_network_registration( DSAT_COPS_MODE_AUTO,
                                                        NULL,
                                                        DSAT_COPS_ACT_AUTO,subs_info );
            }
            DSATUTIL_SET_VAL(DSATETSI_EXT_ACT_COPS_ETSI_IDX,subs_info,0,0,
                                                 DSAT_COPS_MODE_AUTO,MIX_NUM_TYPE)
          }
          /* For manual, mismatch means no requested network service */
          else if ( DSAT_COPS_MODE_MANUAL ==
                    ph_ss_ms_val->dsatetsicall_network_list.requested_pref.mode )
          {
            DS_AT_MSG0_ERROR ("COPS manual reg NO SRV");
            ph_ss_ms_val->dsatetsicall_network_list.cmd_state = DSAT_COPS_ASTATE_NULL;
            ph_ss_ms_val->dsatetsicall_network_list.cmd_idx = CMD_IDX_NONE;
            result = dsat_send_cme_error(DSAT_CME_NO_NETWORK_SERVICE);
          }
          /* incase of QCCOPS command even in auto mode we need to wait for request ID match*/
          else if((DSAT_COPS_MODE_AUTO == ph_ss_ms_val->dsatetsicall_network_list.requested_pref.mode) 
              && (ph_ss_ms_val->dsatetsicall_network_list.cmd_idx == CMD_IDX_QCCOPS ||
                  ph_ss_ms_val->dsatetsicall_network_list.cmd_idx == CMD_IDX_QCCSGCOPS))
          {
            DS_AT_MSG0_ERROR ("QCCOPS/CSGCOPS auto reg NO SRV");
            ph_ss_ms_val->dsatetsicall_network_list.cmd_state = DSAT_COPS_ASTATE_NULL;
            ph_ss_ms_val->dsatetsicall_network_list.cmd_idx = CMD_IDX_NONE;
            result = dsat_send_cme_error(DSAT_CME_NO_NETWORK_SERVICE);
          }
          /* Otherwise, asynch command processing is done */
          else
          {
            ph_ss_ms_val->dsatetsicall_network_list.cmd_state = DSAT_COPS_ASTATE_NULL;
            ph_ss_ms_val->dsatetsicall_network_list.cmd_idx = CMD_IDX_NONE;
            DS_AT_MSG0_HIGH ("COPS automatic reg NO SRV");
            result = DSAT_OK;
           }
           break;
          case SYS_SRV_STATUS_SRV:
            if( DSAT_COPS_MODE_MANAUTO ==
               ph_ss_ms_val->dsatetsicall_network_list.requested_pref.mode )
            { 
              /* Update COPS state based on the current network state.*/ 
              dsat_cops_mode_e_type curr_cops_mode = (dsat_cops_mode_e_type)
                ph_ss_ms_val->dsat_net_reg_state.cmph_pref.network_sel_mode_pref;
              DSATUTIL_SET_VAL(DSATETSI_EXT_ACT_COPS_ETSI_IDX,subs_info,0,0,
                                 (dsat_num_item_type)curr_cops_mode,MIX_NUM_TYPE)
            }
            DS_AT_MSG0_HIGH ("COPS processing complete.");
            ph_ss_ms_val->dsatetsicall_network_list.cmd_state = DSAT_COPS_ASTATE_NULL;
            ph_ss_ms_val->dsatetsicall_network_list.cmd_idx = CMD_IDX_NONE;
            result = DSAT_OK;
            break;
          default:
           DS_AT_MSG0_HIGH ("Unknown state");
        }
      }
    }
    else
    {
      if(ph_ss_ms_val->net_pref.mode == DSAT_COPS_MODE_DEREG)
      {
        DS_AT_MSG0_HIGH ("COPS dereg processing complete.");
        ph_ss_ms_val->dsatetsicall_network_list.cmd_state = DSAT_COPS_ASTATE_NULL;
        ph_ss_ms_val->dsatetsicall_network_list.cmd_idx = CMD_IDX_NONE;
        result = DSAT_OK;
      }
    }
  }
  else if (DSAT_COPS_ASTATE_ABORT == ph_ss_ms_val->dsatetsicall_network_list.cmd_state)
  {
    /* Asynch command processing is done */
    ph_ss_ms_val->dsatetsicall_network_list.cmd_state = DSAT_COPS_ASTATE_NULL;
    ph_ss_ms_val->dsatetsicall_network_list.cmd_idx = CMD_IDX_NONE;
    
    /* Abort handler responsible for sending result code */
    result = DSAT_ASYNC_EVENT;
  }

  cmif_process_cm_net_reg_data(event_ptr, stack_id, subs_info);

  /* Process registration data for CREG and generate URC */
  /* Do not report if +COPS is underway as the lower layers do a few
   * transitions before things settle down. */
  if (DSAT_COPS_ASTATE_NULL == ph_ss_ms_val->dsatetsicall_network_list.cmd_state)
  {
    dsatetsicall_creg_state_machine(event_ptr, stack_id, subs_info);
  } /* Update CELL ID ,LAC ID TAC ID when cops is under process */
  else
  {
    dsatetsicall_update_creg_lac_cell_id(event_ptr, stack_id, subs_info);
  }
#endif /* FEATURE_DSAT_ETSI_MODE */
#ifdef FEATURE_ETSI_ATTACH

#ifdef FEATURE_DSAT_ETSI_DATA
  /* Check if there is a change in the PS state */
  if(dsat_gprs_attach_status[subs_info] != (dsat_num_item_type)dsatutil_get_val(
                              DSATETSI_EXT_ACT_CGATT_ETSI_IDX,subs_info,0,NUM_TYPE))
  {
    if (dsat_gprs_attach_status[subs_info] == 1)
    {
      /* PS detach: report TE depending on +CGEREP buffer mode settings */
      if((ph_ss_ms_val->dsat_prev_ms_service_preference == CM_SRV_DOMAIN_PREF_CS_PS && 
          ph_ss_ms_val->dsat_ms_service_preference == CM_SRV_DOMAIN_PREF_CS_ONLY) ||
         (dsat_power_off[subs_info] == TRUE) ||
         (ph_ss_ms_val->dsat_ms_service_preference == CM_SRV_DOMAIN_PREF_PS_DETACH))
      {
        /* Detach was initiated by MS */
        dsatetsipkt_report_gprs_event_to_te(
                                         DSAT_MS_INITIATED_DETACH, 
                                         (void *)NULL,
                                         subs_info);
        dsat_power_off[subs_info] = FALSE;
      }
      else
      {
        /* Detach was initiated by N/W */
        dsatetsipkt_report_gprs_event_to_te(
                               DSAT_NW_INITIATED_DETACH, 
                               (void *)NULL,
                               subs_info);
      }
    }

    dsat_gprs_attach_status[subs_info] = (dsat_num_item_type)dsatutil_get_val(
                              DSATETSI_EXT_ACT_CGATT_ETSI_IDX,subs_info,0,NUM_TYPE);
  }
#endif /* FEATURE_DSAT_ETSI_DATA */

  /* Activate request should not bail out on Camped domain or No Service
     should rather wait till power save is received */
  if((dsatcmif_attach_write == DSAT_CGATT_ACTIVATE )&&
     (( SYS_SRV_DOMAIN_CAMPED == event_ptr->event_info[stack_id].srv_domain)||
      ( SYS_SRV_STATUS_NO_SRV == event_ptr->event_info[stack_id].srv_status )))
  {
    DS_AT_MSG0_HIGH("Waiting for POWER SAVE (or) Service available");
    return DSAT_ASYNC_EVENT;
  }

  /* For +CGATT command, check to see if we are in write operation */
  if ( dsatcmif_attach_write != DSAT_CGATT_MAX )
  {
    /* Only CGATT = 1 Pass cases are handled here; Failure case is reported via 
       ATTACHED_FAILED event*/
    if(dsatcmif_attach_write == DSAT_CGATT_ACTIVATE)
    {
      if((event_ptr->event_info[stack_id].srv_domain == SYS_SRV_DOMAIN_CS_PS) ||
         (event_ptr->event_info[stack_id].srv_domain == SYS_SRV_DOMAIN_PS_ONLY) )
      {
        DSATUTIL_SET_VAL(DSATETSI_EXT_ACT_CGATT_ETSI_IDX,subs_info,0,0,1,NUM_TYPE)
        dsatcmif_attach_write = DSAT_CGATT_MAX;
        result = DSAT_OK; 
      }
    }
    else if( (dsatcmif_attach_write == DSAT_CGATT_DEACTIVATE) && 
             ( ( event_ptr->event_info[stack_id].changed_fields ) & 
               ( DSAT_SS_SRV_DOMAIN_MASK |
                 DSAT_SS_SRV_STATUS_MASK ) ) )
    {
      if((event_ptr->event_info[stack_id].srv_domain == SYS_SRV_DOMAIN_CS_ONLY) ||
         (event_ptr->event_info[stack_id].srv_domain == SYS_SRV_DOMAIN_NO_SRV) )
      {
        DSATUTIL_SET_VAL(DSATETSI_EXT_ACT_CGATT_ETSI_IDX,subs_info,0,0,0,NUM_TYPE)
        dsatcmif_attach_write = DSAT_CGATT_MAX;
        result = dsatetsicmif_deactivate_all_contexts();
      }
    }

  }
#endif /* FEATURE_ETSI_ATTACH */
  return result;
} /* cmif_process_cm_reg_data */

#ifdef FEATURE_SGLTE
/*===========================================================================

FUNCTION  CMIF_PROCESS_CM_HYBR_REG_DATA

DESCRIPTION
  This function processes the Call Manager registration event data to
  set general network registration state information of hybrid stack.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
LOCAL void cmif_process_cm_hybr_reg_data
(
  const ds_at_cm_ss_event_type *event_ptr,        /* DS event pointer */
  dsat_stack_id_e_type          stack_id,
  ds_subs_e_type                subs_info
)
{
  dsatcmif_servs_state_ms_info *ph_ss_ms_val = NULL;

  if ( ( NULL == event_ptr ) ||
       ( FALSE == IS_VALID_STACK_ID(stack_id) ) ||
       ( FALSE == IS_VALID_SUBS_INFO(subs_info) ) )
  {
    DS_AT_MSG3_ERROR("Invalid Arg: event_ptr = %p, stack_id = %d, subs_info = %d",
                     event_ptr, stack_id, subs_info);
    return;
  }

  DS_AT_MSG4_HIGH("Processing hybrid registration update: chgfld=%p %p srv=%d dom=%d",
                  ((event_ptr->event_info[stack_id].changed_fields) >> 32),
                  event_ptr->event_info[stack_id].changed_fields,
                  event_ptr->event_info[stack_id].srv_status,
                  event_ptr->event_info[stack_id].srv_domain);

  cmif_process_cm_net_reg_data(event_ptr, stack_id, subs_info);

  if(DSAT_FAILURE == dsatutil_get_base_addr(DSAT_SS_PH_MS_VALS, (void **)&ph_ss_ms_val, subs_info))
  {
    return;
  }

  /* Process registration data for CREG and generate URC */
  /* Do not report if +COPS is underway as the lower layers do a few
   * transitions before things settle down. */
  if (DSAT_COPS_ASTATE_NULL == ph_ss_ms_val->dsatetsicall_network_list.cmd_state)
  {
    dsatetsicall_creg_state_machine(event_ptr, stack_id, subs_info);
  } /* Update CELL ID ,LAC ID TAC ID when cops is under process */
  else
  {
    dsatetsicall_update_creg_lac_cell_id(event_ptr, stack_id, subs_info);
  }

  return;
} /* cmif_process_cm_hybr_reg_data */
#endif /* FEATURE_SGLTE */

/*===========================================================================

FUNCTION  CMIF_REPORT_MODE

DESCRIPTION
  This function Reports System Mode between No Service and Full Service

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
LOCAL void cmif_report_mode
(
  sys_srv_status_e_type     srv_status,
  sys_srv_domain_e_type     srv_domain,
  sys_sys_mode_e_type       sys_mode,
  ds_subs_e_type            subs_info
)
{

  dsm_item_type * res_buff_ptr = NULL;
  dsat_num_item_type mode = sys_mode;
  static dsat_num_item_type prev_reported_mode[DUAL_SUBS] = {AT_MODE_NONE,AT_MODE_NONE};
  
  /* When in FULL_SRV, the Domain should be valid  */
  if( (srv_status == SYS_SRV_STATUS_SRV) &&
     ( srv_domain != SYS_SRV_DOMAIN_CS_ONLY ) &&
     ( srv_domain != SYS_SRV_DOMAIN_CS_PS ) &&
     ( srv_domain != SYS_SRV_DOMAIN_PS_ONLY ) )
  {
    return;
  }
  else
  {
    if( srv_status != SYS_SRV_STATUS_SRV )
    {
      mode = AT_MODE_NONE;
    }
#ifdef FEATURE_TDSCDMA
   /* TDS mode in spec is different from SYS_SYS_MODE_TDS  */
    if((srv_status == SYS_SRV_STATUS_SRV) && 
       (sys_mode == SYS_SYS_MODE_TDS))
    {
      mode = AT_MODE_TDS;
    }
#endif /* FEATURE_TDSCDMA */
    /* Report only the change in the mode. */
    if(prev_reported_mode[subs_info] != mode)
    {
      prev_reported_mode[subs_info] = mode;
      if ( (1 == (dsat_num_item_type)dsatutil_get_val(
                                    DSAT_VENDOR_MODE_IDX,0,0,NUM_TYPE)))
      {
        res_buff_ptr = dsat_dsm_new_buffer(DSM_DS_SMALL_ITEM_POOL, TRUE);
        if (NULL != res_buff_ptr)
        {
          res_buff_ptr->used =
            (word) snprintf((char*)res_buff_ptr->data_ptr,res_buff_ptr->size,
                                   "%s: %d",
                                   "^MODE",mode);
          dsatcmdp_send_urc( subs_info,res_buff_ptr, DSAT_COMPLETE_RSP );
        }
      }
    }
  }
}/* cmif_report_mode */

#ifdef FEATURE_DSAT_EXTENDED_CMD
/*===========================================================================

FUNCTION  CMIF_REPORT_MODE_3GPP2

DESCRIPTION
  This function reports 3GPP2 system mode when it is changed.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
LOCAL void cmif_report_mode_3gpp2
(
  ds_at_ss_info_s_type *ss_info_ptr
)
{
  static sys_sys_mode_e_type    prev_sys_mode = SYS_SYS_MODE_NO_SRV;
  dsm_item_type                *res_buff_ptr = NULL;
  sys_sys_mode_e_type           sys_mode = ss_info_ptr->sys_mode;
  dsatcmif_servs_state_ss_info *ph_ss_val = NULL;

  if ( NULL == ss_info_ptr )
  {
    return;
  }

  /* If hybrid is turned ON and any protcol running 
        on the hybrid stack(Secondary) then the sys_mode would be Hybrid 
        otherwise the sys_mode based on the primary stack. */
  if ( TRUE == ss_info_ptr->hdr_hybrid )
  {
    if ( SYS_SRV_STATUS_SRV == ss_info_ptr->hdr_srv_status )
    {
      /* Include HDR in the system mode if HDR has service */
      if ( SYS_SYS_MODE_CDMA == sys_mode )
      {
        /* Type casting it to sys mode e type as hybrid 
          is not available in sys mode tyep */
        sys_mode = (sys_sys_mode_e_type)DSAT_SYS_HYBRID;
      }
      else
      {
        sys_mode = SYS_SYS_MODE_HDR;
      }
    }
  }

  if(DSAT_FAILURE == dsatutil_get_base_addr(DSAT_SS_PH_SS_VALS, (void **)&ph_ss_val, DSAT_MS_FIRST_SUBS))
  {
     return;
  }
  ph_ss_val->dsat_sys_mode = sys_mode;

  /* If sys mode is changed and report an indication */
  if ( (prev_sys_mode != sys_mode) && 
       ( ( SYS_SYS_MODE_NO_SRV == sys_mode ) ||
         ( SYS_SYS_MODE_CDMA == sys_mode ) ||
         ( (sys_sys_mode_e_type)DSAT_SYS_HYBRID == sys_mode ) ||
         ( SYS_SYS_MODE_HDR == sys_mode ) ) )
  {
    /* need to report the system node status*/
    res_buff_ptr = dsat_dsm_new_buffer(DSM_DS_SMALL_ITEM_POOL, TRUE);
    if (NULL != res_buff_ptr)
    {
      res_buff_ptr->used = (word)snprintf ((char*)res_buff_ptr->data_ptr,
                                           res_buff_ptr->size,
                                           "^MODE:%d",
                                           sys_mode);
      dsatcmdp_send_urc(PRIMA_SUBS, res_buff_ptr, DSAT_COMPLETE_RSP);
    }
    prev_sys_mode = sys_mode;
  }

  return;
} /* cmif_report_mode_3gpp2 */
#endif /* FEATURE_DSAT_EXTENDED_CMD */

#if defined(FEATURE_DSAT_SP_CMDS)
/*===========================================================================

FUNCTION  CMIF_DETERMINE_SPSERVICE

DESCRIPTION
  This function determines current service capabilities for $SPSERVICE command.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
LOCAL void cmif_determine_spservice
(
  ds_at_ss_info_s_type *ss_info_ptr
)
{
  if ( NULL == ss_info_ptr )
  {
    return;
  }

  switch ( ss_info_ptr->sys_mode )
  {
    case SYS_SYS_MODE_CDMA:
      dsat_spservice = DSAT_SRV_CDMA;
      break;

    case SYS_SYS_MODE_HDR:
      switch ( ss_info_ptr->hdr_active_prot )
      {
        case SYS_ACTIVE_PROT_HDR_REL0:
          dsat_spservice = DSAT_SRV_EVDO;
          break;

        case SYS_ACTIVE_PROT_HDR_RELA:
          dsat_spservice = DSAT_SRV_EVDO_REV_A;
          break;

        default:
          dsat_spservice = DSAT_SRV_NONE;
          break;
      }
      break;

    default:
      dsat_spservice = DSAT_SRV_NONE;
      break;
  }

  return;
} /* cmif_determine_spservice */
#endif /* defined(FEATURE_DSAT_SP_CMDS) */

/*===========================================================================

FUNCTION  CMIF_PROCESS_RSSI_INFO_3GPP

DESCRIPTION
  This function processes RSSI information for 3GPP mode.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
void cmif_process_rssi_info_3gpp
(
  const ds_at_cm_ss_event_type *event_ptr, /* DS event pointer */
  dsat_stack_id_e_type          stack_id,
  ds_subs_e_type                subs_info
)
{
  dsatcmif_servs_state_ms_info *ph_ss_ms_val = NULL;

  if ( ( NULL == event_ptr ) ||
       ( FALSE == IS_VALID_STACK_ID(stack_id) ) ||
       ( FALSE == IS_VALID_SUBS_INFO(subs_info) )||
         DSAT_FAILURE == dsatutil_get_base_addr(DSAT_SS_PH_MS_VALS, (void **)&ph_ss_ms_val, subs_info))
  {
    DS_AT_MSG3_ERROR("Invalid Arg: event_ptr = %p, stack_id = %d, subs_info = %d",
                     event_ptr, stack_id, subs_info);
    return;
  }

  dsatutil_memscpy(&ph_ss_ms_val->dsatcmif_signal_reporting,
                   sizeof(dsatcmif_sigrep_s_type),
                   &event_ptr->event_info[stack_id].dsatcmif_signal_reporting,
                   sizeof(dsatcmif_sigrep_s_type));

  DS_AT_MSG8_MED("stack_id = %d, rssi = %d, rssi2 = %d, rscp = %d, "
                 "bit_err_rate = %d, ecio = %d, pathloss = %d, sir = %d",
                 stack_id,
                 ph_ss_ms_val->dsatcmif_signal_reporting.rssi,
                 ph_ss_ms_val->dsatcmif_signal_reporting.rssi2,
                 ph_ss_ms_val->dsatcmif_signal_reporting.rscp,
                 ph_ss_ms_val->dsatcmif_signal_reporting.bit_err_rate,
                 ph_ss_ms_val->dsatcmif_signal_reporting.ecio,
                 ph_ss_ms_val->dsatcmif_signal_reporting.pathloss,
                 ph_ss_ms_val->dsatcmif_signal_reporting.sir);

  (void)dsatetsicmif_process_rssi_event(event_ptr, stack_id, subs_info);

  return;
} /* cmif_process_rssi_info_3gpp */

/*===========================================================================

FUNCTION  CMIF_PROCESS_RSSI_INFO_3GPP2

DESCRIPTION
  This function processes RSSI information for 3GPP2 mode.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
LOCAL void cmif_process_rssi_info_3gpp2
(
  ds_at_ss_info_s_type *ss_info_ptr
)
{
  dsatcmif_servs_state_ss_info *ph_ss_val = NULL;
#ifdef FEATURE_DSAT_EXTENDED_CMD
  uint16                        curr_hdr_rssi_intensity_level = 0;
  static uint16                 prev_hdr_rssi_intensity_level = 0;
  dsm_item_type                *res_buff_ptr = NULL;
#endif /* FEATURE_DSAT_EXTENDED_CMD */

  if ( NULL == ss_info_ptr ||
       DSAT_FAILURE == dsatutil_get_base_addr(DSAT_SS_PH_SS_VALS, (void **)&ph_ss_val, DSAT_MS_FIRST_SUBS))
  {
    return;
  }

  dsatutil_memscpy(&ph_ss_val->dsatcmif_cdma_signal_reporting,
                   sizeof(dsatcmif_sigrep_s_type),
                   &ss_info_ptr->dsatcmif_signal_reporting,
                   sizeof(dsatcmif_sigrep_s_type));

  DS_AT_MSG3_MED("rssi = %d, hdr_hybrid = %d, hdr_rssi = %d",
                 ph_ss_val->dsatcmif_cdma_signal_reporting.rssi,
                 ph_ss_val->dsatcmif_cdma_signal_reporting.hdr_hybrid,
                 ph_ss_val->dsatcmif_cdma_signal_reporting.hdr_rssi);

#ifdef FEATURE_DSAT_EXTENDED_CMD
  curr_hdr_rssi_intensity_level = dsatcmif_cm_get_hdr_rssi_intensity_level();

  if ( curr_hdr_rssi_intensity_level != prev_hdr_rssi_intensity_level )
  {
    /* Need to report HDR RSSI intensity level*/
    res_buff_ptr = dsat_dsm_new_buffer(DSM_DS_SMALL_ITEM_POOL, TRUE);
    if (NULL != res_buff_ptr)
    {
      res_buff_ptr->used = (word)snprintf ((char*)res_buff_ptr->data_ptr,
                                           res_buff_ptr->size,
                                           "^HRSSILVL:%d",
                                           curr_hdr_rssi_intensity_level);

      dsatcmdp_send_urc(DS_ALL_SUBS, res_buff_ptr, DSAT_COMPLETE_RSP);
    }
    prev_hdr_rssi_intensity_level = curr_hdr_rssi_intensity_level;
  }
#endif /* FEATURE_DSAT_EXTENDED_CMD */

  return;
} /* cmif_process_rssi_info_3gpp2 */

#ifdef FEATURE_DSAT_ETSI_MODE
#ifdef FEATURE_WCDMA_L1_HS_CQI_STAT
/*===========================================================================

FUNCTION  CMIF_PROCESS_CQI_INFO

DESCRIPTION
  This function processes CQI information.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
void cmif_process_cqi_info
(
  ds_at_ss_info_s_type *ss_info_ptr
)
{
  dsatcmif_servs_state_ss_info *ph_ss_val = NULL;


  if(ss_info_ptr->sys_mode != SYS_SYS_MODE_GSM &&
     ss_info_ptr->sys_mode != SYS_SYS_MODE_WCDMA &&
     ss_info_ptr->sys_mode != SYS_SYS_MODE_GW )
  {
    DS_AT_MSG0_ERROR("Unsupported system mode");
    return;
  }
  if ( NULL == ss_info_ptr ||
       DSAT_FAILURE == dsatutil_get_base_addr(DSAT_SS_PH_SS_VALS, (void **)&ph_ss_val, DSAT_MS_FIRST_SUBS))
  {
    return;
  }

  (void) dsatutil_memscpy((void*)&ph_ss_val->dsat_cqi_info,
                          sizeof(cm_cqi_info_s_type),
                          (void*)&ss_info_ptr->dsat_cqi_info,
                          sizeof(cm_cqi_info_s_type));

  /* Report CQI if unsolicit response is enabled */
  if((1 == (dsat_num_item_type)dsatutil_get_val(
        DSATETSI_EXT_ACT_CQI_ETSI_IDX,0,0,NUM_TYPE))&&(ph_ss_val->dsat_cqi_info.gw_cqi != 0xFF))
  {
    DS_AT_MSG0_HIGH("CQI unsolicit reporting");
    dsatetsicall_report_cqi_result();
  }

  return;
} /* cmif_process_cqi_info */
#endif /* FEATURE_WCDMA_L1_HS_CQI_STAT  */
#endif /* FEATURE_DSAT_ETSI_MODE */

/*===========================================================================

FUNCTION  DSATCMIF_REPORT_CEN_LIST

DESCRIPTION
  This function display PLMN and emegency list based on their availability

DEPENDENCIES
  None

RETURN VALUE
  Response buffer pointer filled with CEN list.

SIDE EFFECTS
  None

===========================================================================*/
void dsatcmif_report_cen_list
(
  dsm_item_type       * res_buff_ptr,
  ds_subs_e_type        subs_info
)
{
  dsat_num_item_type    cen_reporting = 0;
  sys_mnc_type          mnc;
  sys_mcc_type          mcc;
  boolean               plmn_is_undefined;
  boolean               mnc_includes_pcs_digit;
  dsatcmif_servs_state_ms_info  *ph_ss_ms_val = NULL;

  if(DSAT_FAILURE == dsatutil_get_base_addr(DSAT_SS_PH_MS_VALS,(void **)&ph_ss_ms_val,subs_info))
  {
    return;
  }
  
  DS_AT_MSG2_HIGH("CEN reporting  flag value %d subs_info %d",
                ph_ss_ms_val->dsat_cen_num_list.plmn_num_flag,subs_info);
  
  cen_reporting = (dsat_num_item_type)dsatutil_get_val(DSATETSI_EXT_ACT_CEN_ETSI_IDX,
                                                         subs_info,0,NUM_TYPE);
  res_buff_ptr->used =
       (word) snprintf((char*)res_buff_ptr->data_ptr,
                              res_buff_ptr->size,
                              "+CEN1: %d",
                              cen_reporting);
  /* Report PLMN info if present */
  if(ph_ss_ms_val->dsat_cen_num_list.plmn_num_flag & DSAT_CEN_PLMN_PRESENT)
  {
    /* Extract the mcc and mnc from the PLMN.*/
    sys_plmn_get_mcc_mnc( ph_ss_ms_val->dsat_cen_num_list.plmn,
                        &plmn_is_undefined,
                        &mnc_includes_pcs_digit,
                        &mcc,
                        &mnc );
    if(plmn_is_undefined == FALSE)
    {
      res_buff_ptr->used +=(word) snprintf((char*)&res_buff_ptr->data_ptr[res_buff_ptr->used],
                                                  (res_buff_ptr->size - res_buff_ptr->used),
                                                  ",%d,%d\n",
                                                  (int)mcc,(int)mnc);
     /* Report NUMBER info if present */
      if(ph_ss_ms_val->dsat_cen_num_list.plmn_num_flag & DSAT_CEN_NUMBER_PRESENT)
      {
        dsat_num_item_type  num_counter = 0;
        for(num_counter = 0;(num_counter < ph_ss_ms_val->dsat_cen_num_list.num_list_len && num_counter < CM_MAX_EMERGENCY_NUM_COUNT );num_counter++)
        {
         if(ph_ss_ms_val->dsat_cen_num_list.cen_num[num_counter]!= NULL)
         {
           res_buff_ptr->used +=(word) snprintf((char*)&res_buff_ptr->data_ptr[res_buff_ptr->used],
                                                  (res_buff_ptr->size - res_buff_ptr->used),
                                                  "+CEN2: %d,",
                                                  (int)ph_ss_ms_val->dsat_cen_num_list.cen_num[num_counter]->cat);
           /* Copy number */

           (void) dsatutil_memscpy((void*)&res_buff_ptr->data_ptr[res_buff_ptr->used], 
                                    DSAT_BUF_SIZE(res_buff_ptr->size,res_buff_ptr->used),
                                   (void*)&ph_ss_ms_val->dsat_cen_num_list.cen_num[num_counter]->buf[0],
                  MIN(ph_ss_ms_val->dsat_cen_num_list.cen_num[num_counter]->len,CM_MAX_NUMBER_CHARS));
           /* Update Used based on number length*/
           res_buff_ptr->used +=(word) MIN(ph_ss_ms_val->dsat_cen_num_list.cen_num[num_counter]->len,CM_MAX_NUMBER_CHARS);
           /* Insert Next line */
           res_buff_ptr->used +=(word) snprintf((char*)&res_buff_ptr->data_ptr[res_buff_ptr->used],
                                                  (res_buff_ptr->size - res_buff_ptr->used),
                                                  "\n");
         }
        }
      }
    }
  }

}/*dsatcmif_report_cen_list*/

/*===========================================================================

FUNCTION  CMIF_PROCESS_EMERG_LIST

DESCRIPTION
  This function processes the Call Manager registration event data to
  set PS attach and general network registration state information.

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR :    if there was any problem in executing the command
    DSAT_OK :       if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
LOCAL void cmif_process_emerg_list
(
  const ds_at_ss_info_s_type   * event_info_ptr,        /* DS event info pointer */
  ds_subs_e_type                 subs_info
)
{
  dsat_num_item_type emerg_list_len = 0;
  dsat_num_item_type index =0;
  dsat_num_item_type    cen_reporting = 0;
  dsm_item_type       * res_buff_ptr = NULL;
  dsatcmif_servs_state_ms_info  *ph_ss_ms_val = NULL;

  if ( ( NULL == event_info_ptr ) ||
       ( FALSE == IS_VALID_SUBS_INFO(subs_info) ) )
  {
    DS_AT_MSG2_ERROR("Invalid Arg: event_ptr = %p, subs_info = %d",
                     event_info_ptr, subs_info);
    return;
  }

  if(NULL == event_info_ptr->emerg_num_list || 0 == event_info_ptr->emerg_num_list->num_list_len ||
      DSAT_FAILURE == dsatutil_get_base_addr(DSAT_SS_PH_MS_VALS,(void **)&ph_ss_ms_val,subs_info))
  {
    DS_AT_MSG0_HIGH("Emergency list is empty ");
    return;
  }
  
  emerg_list_len  =  event_info_ptr->emerg_num_list->num_list_len;
    
  DS_AT_MSG3_HIGH("Emergency New list count %d flag %d old count %d ",
     emerg_list_len,ph_ss_ms_val->dsat_cen_num_list.plmn_num_flag,ph_ss_ms_val->dsat_cen_num_list.num_list_len);
  
  if(ph_ss_ms_val->dsat_cen_num_list.plmn_num_flag & DSAT_CEN_NUMBER_PRESENT)
  {
    /* Previous list is present.Remove old list .Update new list */
    /* browse whole old list and free memory till last index */

    for(index = 0 ;index < ph_ss_ms_val->dsat_cen_num_list.num_list_len && index < CM_MAX_EMERGENCY_NUM_COUNT;index++)
    {
      if (NULL != ph_ss_ms_val->dsat_cen_num_list.cen_num[index])
      {
        modem_mem_free( ph_ss_ms_val->dsat_cen_num_list.cen_num[index] , MODEM_MEM_CLIENT_DATA);
      }
    }
    
  }
  /* Re allocate memory and copy number */

  ph_ss_ms_val->dsat_cen_num_list.plmn_num_flag |= DSAT_CEN_NUMBER_PRESENT;
  ph_ss_ms_val->dsat_cen_num_list.num_list_len = MIN (emerg_list_len, CM_MAX_EMERGENCY_NUM_COUNT);
  
   /* Allocate memory for Emergency numbers */
  for(index = 0;(index < ph_ss_ms_val->dsat_cen_num_list.num_list_len) ;index++ )
  {
    ph_ss_ms_val->dsat_cen_num_list.cen_num[index] = dsat_alloc_memory(sizeof(dsat_cen_emerg_num_s_type), TRUE);
    if (NULL != ph_ss_ms_val->dsat_cen_num_list.cen_num[index])
    {
      memset(ph_ss_ms_val->dsat_cen_num_list.cen_num[index],0x0, sizeof(dsat_cen_emerg_num_s_type));

      ph_ss_ms_val->dsat_cen_num_list.cen_num[index]->cat = 
                         event_info_ptr->emerg_num_list->num_list[index].num_type;
      ph_ss_ms_val->dsat_cen_num_list.cen_num[index]->len =
                         MIN(CM_MAX_NUMBER_CHARS,event_info_ptr->emerg_num_list->num_list[index].num.len);

      (void) dsatutil_memscpy((void*)&ph_ss_ms_val->dsat_cen_num_list.cen_num[index]->buf[0], 
                               CM_MAX_NUMBER_CHARS,
                              (void*)&event_info_ptr->emerg_num_list->num_list[index].num.buf[0],
                               MIN(CM_MAX_NUMBER_CHARS,ph_ss_ms_val->dsat_cen_num_list.cen_num[index]->len));
    
      DS_AT_MSG3_HIGH("Rec EMERG num cat %d len %d index %d",ph_ss_ms_val->dsat_cen_num_list.cen_num[index]->cat,
                                  ph_ss_ms_val->dsat_cen_num_list.cen_num[index]->len,index);
    }
  }

    /*Report emergency number as URC if CEN has enabled*/
    cen_reporting = (dsat_num_item_type)dsatutil_get_val(DSATETSI_EXT_ACT_CEN_ETSI_IDX,
                                                           subs_info,0,NUM_TYPE);
    if(1 == cen_reporting)
    {
      res_buff_ptr = dsat_dsm_new_buffer(DSM_DS_LARGE_ITEM_POOL, TRUE);
      if (NULL != res_buff_ptr)
      {
        dsatcmif_report_cen_list(res_buff_ptr,subs_info);
        /* Report URC : call URC QUEUE*/
        dsatcmdp_send_urc( subs_info, res_buff_ptr, DSAT_COMPLETE_RSP );
      }
    }

  return;
} /*cmif_process_emerg_list*/

/*===========================================================================

FUNCTION DSATCMIF_GET_OPRT_MODE

DESCRIPTION
  This function return cfun operating mode status.
         
DEPENDENCIES
  None
  
RETURN VALUE
  None
    
SIDE EFFECTS
  None

===========================================================================*/

sys_oprt_mode_e_type dsatcmif_get_oprt_mode(void)
{
  dsatcmif_servs_state_ss_info  *ph_ss_val = NULL;
  if(DSAT_FAILURE == dsatutil_get_base_addr(DSAT_SS_PH_SS_VALS,(void **)&ph_ss_val,DSAT_MS_FIRST_SUBS))
  {
    return SYS_OPRT_MODE_ONLINE;
  }
   return ph_ss_val->dsat_oprt_mode;
   
}/*dsatcmif_get_dsat_oprt_mode */

/*===========================================================================

FUNCTION DSATCMIF_GET_DEVICE_MODE

DESCRIPTION
  This function returns device mode.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/

sys_modem_device_mode_e_type dsatcmif_get_device_mode(void)
{
  dsatcmif_servs_state_ss_info  *ph_ss_val = NULL;
  if(DSAT_FAILURE == dsatutil_get_base_addr(DSAT_SS_PH_SS_VALS,(void **)&ph_ss_val,DSAT_MS_FIRST_SUBS))
  {
     return SYS_MODEM_DEVICE_MODE_SINGLE_SIM;
  }
  return ph_ss_val->dsat_device_mode;
} /* dsatcmif_get_device_mode */
 
/*===========================================================================

FUNCTION CMIF_CM_MSG_WM_HIWATER_CB

DESCRIPTION
  This is the high watermark function registered with the watermark for 
  flow control.
         
DEPENDENCIES
  None
  
RETURN VALUE
  None
    
SIDE EFFECTS
  None

===========================================================================*/
#ifdef FEATURE_DSM_WM_CB
/*ARGSUSED*/
LOCAL void cmif_cm_msg_wm_hiwater_cb 
(
  struct dsm_watermark_type_s * wm_ptr, 
  void * cb_ptr
)
#else
LOCAL void cmif_cm_msg_wm_hiwater_cb ( void )
#endif
{
#ifdef IMAGE_QDSP6_PROC
  rex_enter_crit_sect(&dsat_cm_wm_crit_sect);
#else
  INTLOCK();
#endif /* IMAGE_QDSP6_PROC*/
  if( FALSE == dsat_cm_msg_wm_full )
  {
    /* Set flag indicating received messages should be dropped */
    dsat_cm_msg_wm_full = TRUE;
    DS_AT_MSG0_HIGH("Enable flow control on CM event queue");
  }  
#ifdef IMAGE_QDSP6_PROC
    rex_leave_crit_sect(&dsat_cm_wm_crit_sect);
#else
    INTFREE();
#endif /* IMAGE_QDSP6_PROC*/

}


/*===========================================================================

FUNCTION CMIF_CM_MSG_WM_LOWATER_CB

DESCRIPTION
  This is the low watermark function registered with the watermark for 
  flow control.
         
DEPENDENCIES
  None
  
RETURN VALUE
  None
    
SIDE EFFECTS
  None

===========================================================================*/
#ifdef FEATURE_DSM_WM_CB
/*ARGSUSED*/
LOCAL void cmif_cm_msg_wm_lowater_cb 
(
  struct dsm_watermark_type_s * wm_ptr, 
  void * cb_ptr
)
#else
LOCAL void cmif_cm_msg_wm_lowater_cb ( void )
#endif
{
#ifdef IMAGE_QDSP6_PROC
  rex_enter_crit_sect(&dsat_cm_wm_crit_sect);
#else
  INTLOCK();
#endif /* IMAGE_QDSP6_PROC*/
  if( TRUE == dsat_cm_msg_wm_full )
  {
    /* Set flag indicating received messages should no longer be dropped */
    dsat_cm_msg_wm_full = FALSE;
    DS_AT_MSG0_HIGH("Disable flow control on CM event queue");
  }
#ifdef IMAGE_QDSP6_PROC
  rex_leave_crit_sect(&dsat_cm_wm_crit_sect);
#else
  INTFREE();
#endif /* IMAGE_QDSP6_PROC*/

}


/*===========================================================================

FUNCTION CMIF_CM_MSG_WM_NON_EMPTY_CB

DESCRIPTION
  Callback function invoked when the received message watermark goes non-empty.

  This function simply sets the event received subtask signal.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
#ifdef FEATURE_DSM_WM_CB
/*ARGSUSED*/
LOCAL void cmif_cm_msg_wm_non_empty_cb
(
  struct dsm_watermark_type_s * wm_ptr, 
  void * cb_ptr
)
#else
LOCAL void cmif_cm_msg_wm_non_empty_cb ( void )
#endif
{
#ifdef FEATURE_MODEM_RCINIT_PHASE2
  (void)rex_set_sigs( rcinit_lookup_rextask("ds"), DS_AT_CM_MSG_SIG );
#else
  (void)rex_set_sigs( &ds_tcb, DS_AT_CM_MSG_SIG );
#endif /* FEATURE_MODEM_RCINIT_PHASE2 */

} /* cmif_cm_msg_wm_non_empty_cb */



/*===========================================================================
FUNCTION  CMIF_SETUP_MSG_WATERMARKS

DESCRIPTION
  This function initializes the event watermarks used for receiving event 
  messages from CM. The lowater, hiwater and don't exceed counts are 
  configured. Queue to be used by WM is also initialized

DEPENDENCIES
  None

RETURNS
  None

SIDE EFFECTS
  None
===========================================================================*/
LOCAL void cmif_setup_msg_watermarks( void )
{
#ifdef IMAGE_QDSP6_PROC
        rex_enter_crit_sect(&dsat_cm_wm_crit_sect);
#else
        INTLOCK();
#endif /* IMAGE_QDSP6_PROC*/
  dsat_cm_msg_wm_full = FALSE;
#ifdef IMAGE_QDSP6_PROC
      rex_leave_crit_sect(&dsat_cm_wm_crit_sect);
#else
      INTFREE();
#endif /* IMAGE_QDSP6_PROC*/

  /*-------------------------------------------------------------------------
    Initialize , set Don't Exceed count and register the associated queue
  -------------------------------------------------------------------------*/
  dsm_queue_init(&dsat_cm_msg_wm, DSAT_FROM_CM_DONT_EXCEED_CNT, &dsat_cm_msg_q);

  /*-------------------------------------------------------------------------
    Set the Lo Water and Hi Water  counts for the watermark.
  -------------------------------------------------------------------------*/
  dsat_cm_msg_wm.lo_watermark    =  DSAT_FROM_CM_LOWATER_CNT;
  dsat_cm_msg_wm.hi_watermark    =  DSAT_FROM_CM_HIWATER_CNT;

  /*-------------------------------------------------------------------------
    Register callback functions for the watermark flow control.
  -------------------------------------------------------------------------*/
  /*lint -save -e64 suppress error 64*/
  dsat_cm_msg_wm.lowater_func_ptr      = cmif_cm_msg_wm_lowater_cb;
  dsat_cm_msg_wm.hiwater_func_ptr      = cmif_cm_msg_wm_hiwater_cb;
  dsat_cm_msg_wm.non_empty_func_ptr    = cmif_cm_msg_wm_non_empty_cb;
  /*lint -restore suppress error 64*/


} /* cmif_setup_msg_watermarks */



/*===========================================================================

FUNCTION  CMIF_REGISTER_CB_FUNC

DESCRIPTION
  Registers the callback functions.with other tasks.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
LOCAL void cmif_register_cb_func ( void )
{
  cm_client_status_e_type cm_status = CM_CLIENT_OK;

  /* Register the CM call event callback function */
  cm_status = (cm_client_status_e_type)
                  ((int32)cm_status | 
                   (int32)cm_mm_client_call_reg(dsatcm_client_id,
                                  cmif_call_event_cb_func,
                                  CM_CLIENT_EVENT_REG,
                                  CM_CALL_EVENT_ORIG,
                                  CM_CALL_EVENT_CONNECT,
                                  NULL ));
    cm_status = (cm_client_status_e_type)
                  ((int32)cm_status |
                   (int32)cm_mm_client_call_reg(dsatcm_client_id,
                                  cmif_call_event_cb_func,
                                  CM_CLIENT_EVENT_REG,
                                  CM_CALL_EVENT_ACT_BEARER_IND,
                                  CM_CALL_EVENT_RES_ALLOC_FAIL_IND,
                                  NULL ));
    cm_status = (cm_client_status_e_type)
                  ((int32)cm_status |
                   (int32)cm_mm_client_call_reg(dsatcm_client_id,
                                  cmif_call_event_cb_func,
                                  CM_CLIENT_EVENT_REG,
                                  CM_CALL_EVENT_ABRV_ALERT,
                                  CM_CALL_EVENT_ABRV_ALERT,
                                  NULL ));
  
/* Register for call info list query results */
cm_status = (cm_client_status_e_type)
                ((int32)cm_status | 
                 (int32)cm_mm_client_call_info_list_reg(
                                dsatcm_client_id,
                                cmif_call_info_list_cb_func,
                                CM_CLIENT_EVENT_REG,
                                NULL));

/* Register the CM call event callback function - call info for a call id */
cm_status = (cm_client_status_e_type)
                ((int32)cm_status |
                 (int32)cm_mm_client_call_reg(
                                 dsatcm_client_id,
                                 cmif_call_event_cb_func,
                                 CM_CLIENT_EVENT_REG,
                                 CM_CALL_EVENT_INFO,
                                 CM_CALL_EVENT_INFO,
                                 NULL
                                ));

cm_status = (cm_client_status_e_type)
                ((int32)cm_status |
                 (int32)cm_mm_client_call_reg(
                                 dsatcm_client_id,
                                 cmif_call_event_cb_func,
                                 CM_CLIENT_EVENT_REG,
                                 CM_CALL_EVENT_PS_SIG_REL_CNF,
                                 CM_CALL_EVENT_PS_SIG_REL_CNF,
                                 NULL
                                ));
cm_status = (cm_client_status_e_type)
                ((int32)cm_status |
                 (int32)cm_mm_client_call_reg(
                                 dsatcm_client_id,
                                 cmif_call_event_cb_func,
                                 CM_CLIENT_EVENT_REG,
                                 CM_CALL_EVENT_HO_COMPLETE,
                                 CM_CALL_EVENT_SRVCC_COMPLETE_IND,
                                 NULL
                                ));

/*For new interface CM for SGLTE+G*/
cm_status = (cm_client_status_e_type)
                  ((int32)cm_status |
                   (int32)cm_mm_client_ss_reg_msim (
                             dsatcm_client_id,
                             cmif_ss_event_msim_cb_func,
                             CM_CLIENT_EVENT_REG,
                             CM_SS_EVENT_SRV_CHANGED,
                             CM_SS_EVENT_INFO,
                             NULL,
                             DSAT_ASUBS_MASK
                            ));

cm_status = (cm_client_status_e_type)
                ((int32)cm_status |
                   (int32)cm_mm_client_ss_reg_msim (
                             dsatcm_client_id,
                             cmif_ss_event_msim_cb_func,
                             CM_CLIENT_EVENT_REG,
                             CM_SS_EVENT_EMERG_NUM_LIST,
                             CM_SS_EVENT_EMERG_NUM_LIST ,
                             NULL,
                             DSAT_ASUBS_MASK
                            ));
cm_status = (cm_client_status_e_type)
                ((int32)cm_status |
                   (int32)cm_mm_client_ss_reg_msim (
                             dsatcm_client_id,
                             cmif_ss_event_msim_cb_func,
                             CM_CLIENT_EVENT_REG,
                             CM_SS_EVENT_REG_REJECT,
                             CM_SS_EVENT_REG_REJECT ,
                             NULL,
                             DSAT_ASUBS_MASK
                            ));


#if (defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900))
  /* Register for CM_CALL_EVENT_CALLER_ID */
  cm_status = (cm_client_status_e_type)
                ((int32)cm_status | 
                (int32)cm_mm_client_call_reg(  dsatcm_client_id,
                                               cmif_call_event_cb_func,
                                               CM_CLIENT_EVENT_REG,
                                               CM_CALL_EVENT_CALLER_ID,
                                               CM_CALL_EVENT_CALLER_ID,
                                               NULL ));
#endif /* (defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900)) */

cm_status = (cm_client_status_e_type)
                ((int32)cm_status |
                 (int32)cm_client_ph_reg(
                           dsatcm_client_id,
                           cmif_cm_ph_event_cb_func,
                           CM_CLIENT_EVENT_REG,
                           CM_PH_EVENT_STANDBY_SLEEP,
                           CM_PH_EVENT_PS_ATTACH_FAILED,
                           NULL));

  cm_status = (cm_client_status_e_type)
                ((int32)cm_status |
                 (int32)cm_client_ph_reg(
                           dsatcm_client_id,
                           cmif_cm_ph_event_cb_func,
                           CM_CLIENT_EVENT_REG,
                           CM_PH_EVENT_OPRT_MODE,
                           CM_PH_EVENT_SYS_SEL_PREF,
                           NULL));

  cm_status = (cm_client_status_e_type)
                ((int32)cm_status | 
                 (int32)cm_mm_client_call_reg(dsatcm_client_id,
                                cmif_call_event_cb_func,
                                CM_CLIENT_EVENT_REG,
                                CM_CALL_EVENT_PROGRESS_INFO_IND,
                                CM_CALL_EVENT_PROGRESS_INFO_IND,
                                NULL));

#if defined(FEATURE_WCDMA) || defined(FEATURE_GSM)
  cm_status = (cm_client_status_e_type)
                  ((int32)cm_status | 
                   (int32)cm_mm_client_call_reg(dsatcm_client_id,
                                  cmif_call_event_cb_func,
                                  CM_CLIENT_EVENT_REG,
                                  CM_CALL_EVENT_SETUP_IND,
                                  CM_CALL_EVENT_CALL_CONF,
                                  NULL ));
  
  cm_status = (cm_client_status_e_type)
                  ((int32)cm_status |
                   (int32)cm_mm_client_call_reg(dsatcm_client_id,
                                  cmif_call_event_cb_func,
                                  CM_CLIENT_EVENT_REG,
                                  CM_CALL_EVENT_MNG_CALLS_CONF,
                                  CM_CALL_EVENT_INCOM_FWD_CALL,
                                  NULL));  

    /* Register for blocks of supserv events */
  cm_status = (cm_client_status_e_type)
                  ((int32)cm_status |
                   (int32)cm_client_sups_reg(
                               dsatcm_client_id,
                               dsatetsicmif_sups_event_cb_func,
                               CM_CLIENT_EVENT_REG,
                               CM_SUPS_EVENT_REGISTER,
                               CM_SUPS_EVENT_GET_PASSWORD_RES,
                               NULL
                              ));

  /* This should not happen, raise an error */
  if (cm_status!= CM_CLIENT_OK)
  {
    ERR_FATAL( "ATCOP unable to register SUPS events: %d",
               (int)cm_status, 0,0 );
  }

/* Registration for INBAND EVENTS */
 cm_status = (cm_client_status_e_type)
                  ((int32)cm_status | 
                   (int32)cm_mm_client_inband_reg(
                             dsatcm_client_id,
                             dsatcmif_cm_inband_event_cb_func,
                             CM_CLIENT_EVENT_REG,
                             CM_INBAND_EVENT_REV_START_CONT_DTMF,
                             CM_INBAND_EVENT_REV_STOP_CONT_DTMF_CONF,
                             NULL )); 

/* For Terminating GET_NETWORKS */
 cm_status = (cm_client_status_e_type)
                  ((int32)cm_status | 
                      (int32)cm_client_ph_reg(
                             dsatcm_client_id,
                             cmif_cm_ph_event_cb_func,
                             CM_CLIENT_EVENT_REG,
                             CM_PH_EVENT_TERMINATE_GET_NETWORKS,
                             CM_PH_EVENT_TERMINATE_GET_NETWORKS,
                             NULL ));

#ifdef FEATURE_DATA_WCDMA_PS
  /* If WCDMA PS data available register for RAB events so that dormant
     contexts can be handled by $QCDGEN command */
  cm_status = (cm_client_status_e_type)
                  ((int32)cm_status |
                   (int32)cm_mm_client_call_reg( dsatcm_client_id,
                                      cmif_call_event_cb_func,
                                      CM_CLIENT_EVENT_REG,
                                      CM_CALL_EVENT_RAB_REESTAB_IND,
                                      CM_CALL_EVENT_RAB_REESTAB_IND,
                                      NULL ));
#endif /* FEATURE_DATA_WCDMA_PS */

#endif /* defined(FEATURE_WCDMA) || defined(FEATURE_GSM) */

#ifdef FEATURE_DUAL_ACTIVE
  cm_status = (cm_client_status_e_type)
                  ((int32)cm_status |
                   (int32)cm_mm_client_call_reg(dsatcm_client_id,
                                                cmif_call_event_cb_func,
                                                CM_CLIENT_EVENT_REG,
                                                CM_CALL_EVENT_LOCAL_HOLD,
                                                CM_CALL_EVENT_LOCAL_HOLD,
                                                NULL));
#endif /* FEATURE_DUAL_ACTIVE */

#ifdef FEATURE_DATA_IS707

 cm_status = (cm_client_status_e_type)
                  ((int32)cm_status |
                   (int32)cm_mm_client_ss_reg_msim (
                                     dsatcm_client_id,
                                     cmif_ss_event_msim_cb_func,
                                     CM_CLIENT_EVENT_REG,
                                     CM_SS_EVENT_HDR_RSSI,
                                     CM_SS_EVENT_HDR_RSSI,
                                     NULL,
                                     DSAT_ASUBS_MASK
                              ));
#endif /* FEATURE_DATA_IS707 */

#ifdef FEATURE_DUAL_SIM
    cm_status = (cm_client_status_e_type)
                    ((int32)cm_status |
                     (int32)cm_client_ph_reg(
                               dsatcm_client_id,
                               cmif_cm_ph_event_cb_func,
                               CM_CLIENT_EVENT_REG,
                               CM_PH_EVENT_DUAL_STANDBY_PREF,
                               CM_PH_EVENT_DUAL_STANDBY_PREF,
                               NULL));
#endif /* FEATURE_DUAL_SIM */
#ifdef FEATURE_DSAT_LTE
  cm_status = (cm_client_status_e_type)
                 ((int32)cm_status |
                   (int32)cm_client_ph_reg( dsatcm_client_id,
                                            cmif_cm_ph_event_cb_func,
                                            CM_CLIENT_EVENT_REG,
                                            CM_PH_EVENT_GET_DRX_CNF,
                                            CM_PH_EVENT_SET_DRX_CNF,
                                            NULL ) );
#endif/* FEATURE_DSAT_LTE*/

#ifdef FEATURE_SGLTE
	cm_status = (cm_client_status_e_type)
		       ((int32)cm_status |
			 (int32)cm_client_ph_reg( dsatcm_client_id,
						cmif_cm_ph_event_cb_func,
						CM_CLIENT_EVENT_REG,
						CM_PH_EVENT_SIM_AVAILABLE,
						CM_PH_EVENT_SIM_AVAILABLE,
						NULL ) );
#endif/* FEATURE_SGLTE*/

  /* This should not happen, raise an error */
  if (cm_status!= CM_CLIENT_OK)
  {
    ERR_FATAL( "ATCOP unable to register CM events: %d",
               (int)cm_status, 0,0 );
  }

  /*-----------------------------------------------------------------------
    Activate the registered callback functions.
  -----------------------------------------------------------------------*/
  cm_status = cm_client_act( dsatcm_client_id );
  
  if (cm_status!= CM_CLIENT_OK)
  {
    ERR_FATAL( "ATCOP unable to activate client: %d",
               (int)cm_status, 0,0 );
  }

  return;
}/* cmif_register_cb_func */ 


/*===========================================================================

FUNCTION  CMIF_START_S_SEVEN_TIMER

DESCRIPTION
  This function Starts S7 timer

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
LOCAL void cmif_start_s_seven_timer
( 
  ds_subs_e_type        subs_id,
  ds_profile_num_type   profile_id,
  cm_call_type_e_type   call_type,
  cm_call_id_type       cm_call_id,
  cm_client_type_e_type client_id
)
{
  dsat_timer_s_type *t_ptr = NULL;
  sys_sys_mode_e_type  current_mode = SYS_SYS_MODE_NO_SRV;
  dsatetsicall_call_state_ss_info * call_ss_val = NULL;
  
  if(DSAT_FAILURE == dsatutil_get_base_addr( DSAT_CALL_SS_VALS, (void **)&call_ss_val, DSAT_MS_FIRST_SUBS))
  {
    return;
  }

  current_mode = ds3g_get_current_network_mode_ex( subs_id );
  if( ( client_id == (cm_client_type_e_type)dsatcm_client_id ) && 
      ( call_type == CM_CALL_TYPE_VOICE ))
  {
    t_ptr = &dsatutil_timer_table[DSAT_TIMER_VOICE_CALL_TIMEOUT]; 

#ifdef FEATURE_DUAL_ACTIVE
       if (DS_SECOND_SUBS == subs_id)
       {
         t_ptr = &dsatutil_timer_table[DSAT_TIMER_VOICE_CALL_TIMEOUT_2];
       }
#endif /* FEATURE_DUAL_ACTIVE */

    ASSERT(t_ptr->user_data != NULL);

    if ( t_ptr->is_running == TRUE )
    {
      DS_AT_MSG0_HIGH(" Ignoring Start of new S7 timer ");
      return;
    }

    (void)rex_set_timer( t_ptr->timer_ptr, 
                         (dsat_num_item_type)dsatutil_get_val(
                         DSAT_SREG_S7_IDX,0,0,NUM_TYPE) * 1000 ); 
    t_ptr->is_running = TRUE;
    *(cm_call_id_type *)(t_ptr->user_data) = cm_call_id;
    DS_AT_MSG2_HIGH("VOICE S7 Timer Started for Call ID %d - Duration %d",
              cm_call_id,
              (dsat_num_item_type)dsatutil_get_val(
              DSAT_SREG_S7_IDX,0,0,NUM_TYPE));
  }
  else if( ( call_type == CM_CALL_TYPE_PS_DATA )||
           ( call_type == CM_CALL_TYPE_CS_DATA ))
  {
    if( ( client_id == (cm_client_type_e_type)dsatcm_client_id )||
        (TRUE == ds3g_msh_is_atcop_call( call_type,cm_call_id,
                    current_mode)) )
    {
      t_ptr = &dsatutil_timer_table[DSAT_TIMER_DATA_CALL_TIMEOUT];
      if(t_ptr->is_running == FALSE)
      {
        call_ss_val->dsatetsicall_s7_info[0].profile_id = profile_id;
        call_ss_val->dsatetsicall_s7_info[0].subs_id = subs_id;
      }
      else
      {
        t_ptr = &dsatutil_timer_table[DSAT_TIMER_DATA_CALL_TIMEOUT_2];
        if ( t_ptr->is_running == TRUE )
        {
          DS_AT_MSG0_HIGH(" Ignoring Start of new S7 timer ");
          return;
        }
        call_ss_val->dsatetsicall_s7_info[1].profile_id = profile_id;
        call_ss_val->dsatetsicall_s7_info[1].subs_id = subs_id;
      }
      ASSERT(t_ptr->user_data != NULL);
      (void)rex_set_timer( t_ptr->timer_ptr, 
                          (dsat_num_item_type)dsatutil_get_val(
                           DSAT_SREG_S7_IDX,0,0,NUM_TYPE) * 1000 );
      t_ptr->is_running = TRUE;
      *(cm_call_id_type *)(t_ptr->user_data) = cm_call_id;
      DS_AT_MSG2_HIGH("DATA S7 Timer Started for Call ID %d - Duration %d",
                 cm_call_id,
                (dsat_num_item_type)dsatutil_get_val(
                DSAT_SREG_S7_IDX,0,0,NUM_TYPE));
    }
  }
  else
  {
    DS_AT_MSG1_HIGH("Ignoring S7 Start request for call id %d ", cm_call_id);
  }
  return;
}/* cmif_start_s_seven_timer */

/*===========================================================================

FUNCTION  CMIF_STOP_S_SEVEN_TIMER

DESCRIPTION
  This functions clears the S7 timer if running.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
LOCAL void cmif_stop_s_seven_timer
( 
  ds_subs_e_type          subs_id,
  ds_profile_num_type     profile_id,
  cm_call_type_e_type     call_type,
  cm_call_id_type         cm_call_id,
  cm_client_type_e_type   client_id
)
{
  dsat_timer_s_type *t_ptr = NULL;
  dsatetsicall_call_state_ss_info * call_ss_val = NULL;
  uint8 t_idx;
 
  if(DSAT_FAILURE == dsatutil_get_base_addr( DSAT_CALL_SS_VALS, (void **)&call_ss_val, DSAT_MS_FIRST_SUBS))
  {
    return;
  }
  if(( call_type == CM_CALL_TYPE_VOICE ) &&
     (voice_state[subs_id][cm_call_id].state == DSAT_VOICE_STATE_CONNECTING ))
  {
    t_ptr = &dsatutil_timer_table[DSAT_TIMER_VOICE_CALL_TIMEOUT];

#ifdef FEATURE_DUAL_ACTIVE
    if (DS_SECOND_SUBS == subs_id)
    {
      t_ptr = &dsatutil_timer_table[DSAT_TIMER_VOICE_CALL_TIMEOUT_2];
    }
#endif /* FEATURE_DUAL_ACTIVE */

    if(( t_ptr->is_running) && ( *(cm_call_id_type *)(t_ptr->user_data) == cm_call_id))
    {
     (void)rex_clr_timer(t_ptr->timer_ptr);
      t_ptr->is_running = FALSE;
      DS_AT_MSG1_HIGH("VOICE S7 Timer Stopped for Call ID - %d",cm_call_id);
    }
  }
  else if( ( call_type == CM_CALL_TYPE_PS_DATA )||
           ( call_type == CM_CALL_TYPE_CS_DATA ) )
  {
    for (t_idx = 0; t_idx < DSAT_MAX_DATA_CALLS; t_idx++)
    {
      if (call_ss_val->dsatetsicall_s7_info[t_idx].subs_id == subs_id)
      {
        /* Call is profile and subs based */
        if ((FALSE == call_ss_val->dsatetsicall_s7_info[t_idx].is_profile_id_valid) ||
            (call_ss_val->dsatetsicall_s7_info[t_idx].profile_id == profile_id))
        {
          break;
        }
      }
    }
    
    if (t_idx != DSAT_MAX_DATA_CALLS)
    {
      if (0 == t_idx)
      {
        t_ptr = &dsatutil_timer_table[DSAT_TIMER_DATA_CALL_TIMEOUT];
      }
      else
      {
        t_ptr = &dsatutil_timer_table[DSAT_TIMER_DATA_CALL_TIMEOUT_2];
      }
      call_ss_val->dsatetsicall_s7_info[t_idx].data_s7_abort_handler = NULL;
      call_ss_val->dsatetsicall_s7_info[t_idx].is_profile_id_valid = FALSE;
      call_ss_val->dsatetsicall_s7_info[t_idx].subs_id = INVALID_SUBS;
    }

    if ( (NULL != t_ptr) && (NULL != t_ptr->user_data) && (cm_call_id == *(cm_call_id_type *)(t_ptr->user_data)) )
    {
      if( t_ptr->is_running )
      {
        (void)rex_clr_timer(t_ptr->timer_ptr);
        t_ptr->is_running = FALSE;
        DS_AT_MSG1_HIGH("DATA S7 Timer Stopped for Call ID - %d",cm_call_id);
      }
      
      
      *(cm_call_id_type *)(t_ptr->user_data) = CM_CALL_ID_INVALID;
    }
  }
  else
  {
    DS_AT_MSG1_HIGH("Ignoring S7 stop request for call id %d ",cm_call_id);
  }

  return;
}/* cmif_stop_s_seven_timer */
#ifdef FEATURE_DSAT_ETSI_MODE
/*===========================================================================
FUNCTION  DSATCMIF_INBAND_EVENT_CB_FUNC

DESCRIPTION
  CM Inband event command callback function

DEPENDENCIES
  None

RETURNS
  None

SIDE EFFECTS
  Adds command in DS command buffer

===========================================================================*/
/*ARGSUSED*/
void dsatcmif_cm_inband_event_cb_func
(
 cm_inband_event_e_type        inband_event,
 const cm_inband_info_s_type*  inband_info_ptr
)
{
  ds_cmd_type * cmd_buf;

  ASSERT( inband_info_ptr != NULL );
  DS_AT_MSG1_HIGH("In inband_event_cb_func due to event %d",inband_event);

  switch( inband_event )
  {
  case CM_INBAND_EVENT_REV_START_CONT_DTMF_CONF:
  case CM_INBAND_EVENT_REV_STOP_CONT_DTMF_CONF:
    if( CHECK_PENDING(DSATETSI_EXT_ACT_VTS_ETSI_IDX,0,DSAT_PENDING_TRUE))
    {
      ds_at_cm_inband_event_type*  inband_event_ptr = NULL;

      DS_AT_MSG1_HIGH("Dtmf tone played %d",inband_info_ptr->success);
      cmd_buf = dsat_get_cmd_buf(FALSE);
      /* send the event to ATCOP */
      cmd_buf->hdr.cmd_id = DS_AT_CM_INBAND_INFO_CMD;
      inband_event_ptr = (ds_at_cm_inband_event_type*)dsat_alloc_memory(
                            sizeof(ds_at_cm_inband_event_type), FALSE);
      inband_event_ptr->event = inband_event;
      inband_event_ptr->event_info = inband_info_ptr;
      cmd_buf->cmd.cm_cmd.sub_cmd = inband_event_ptr;
      ds_put_cmd( cmd_buf );
    }
    break;
  default :
     /* To Please the Lint*/
    break;
  }
}/*dsatcmif_cm_inband_event_cb_func*/

/*===========================================================================

FUNCTION  DSATCMIF_CM_INBAND_EVENT_HANDLER

DESCRIPTION
  This function is the handler function for the CM info inband events related
  commands

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR :    if there was any problem in executing the command
    DSAT_ASYNC_EVENT :  if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
/* ARGSUSED */
dsat_result_enum_type dsatcmif_cm_inband_event_handler
(
  dsat_mode_enum_type   mode,                /* AT command mode            */
  ds_cmd_type         * cmd_ptr              /* DS Command pointer         */
)
{
  dsat_result_enum_type result = DSAT_ASYNC_EVENT;
  dsat_timer_s_type *t_ptr;
  const cm_inband_info_s_type *event_info;
  ds_at_cm_inband_event_type*  inband_event = NULL;

  ASSERT( cmd_ptr != NULL );

  inband_event = (ds_at_cm_inband_event_type*)cmd_ptr->cmd.cm_cmd.sub_cmd;
  ASSERT( inband_event != NULL );

  if(!dsatcmdp_is_etsi_cmd())
  {
    dsatutil_free_memory((void*)inband_event);
    return result;
  }
  
  DS_AT_MSG1_HIGH("In inband_event_handler due to event %d", inband_event->event);
  event_info = inband_event->event_info;
  switch( inband_event->event )
  {
    case CM_INBAND_EVENT_REV_START_CONT_DTMF_CONF:
      if(event_info->success)
      {
        DS_AT_MSG1_HIGH("Dtmf tone played %d",event_info->digits[0]);
        t_ptr = &dsatutil_timer_table[DSAT_INBAND_DTMF_RING];
        (void) rex_set_timer( t_ptr->timer_ptr, t_ptr->duration ); 
      }
      else
      {
        DS_AT_MSG0_HIGH("DTMF Tone Could not be played");
        SET_PENDING(DSATETSI_EXT_ACT_VTS_ETSI_IDX ,0, DSAT_PENDING_FALSE)
        result = DSAT_ERROR;
      }
      break;
    case CM_INBAND_EVENT_REV_STOP_CONT_DTMF_CONF:   
      SET_PENDING(DSATETSI_EXT_ACT_VTS_ETSI_IDX ,0, DSAT_PENDING_FALSE)
      result = DSAT_OK;
    break;
    default:
      break;
  }
  dsatutil_free_memory((void*)inband_event);
  return result;

} /* dsatcmif_cm_inband_event_handler */

#endif /* FEATURE_DSAT_ETSI_MODE*/

#if defined(FEATURE_HS_USB_SER3_PORT)
/*===========================================================================

FUNCTION  CMIF_GENERATE_DORMANT_IND

DESCRIPTION
  This function generate the dormant indication when dormant status changed

DEPENDENCIES
  None

RETURN VALUE
  dsat_result_enum_type

SIDE EFFECTS
  None

===========================================================================*/
LOCAL dsat_result_enum_type cmif_generate_dormant_ind
(
  void
)
{
  dsm_item_type *res_buff_ptr = NULL;
  dsm_item_type *formatted_rsp_ptr = NULL;
  dsatcmif_servs_state_ss_info  *ph_ss_val = NULL;
  DSATUTIL_CMD_GET_BASE_ADDR(DSAT_SS_PH_SS_VALS,(void **)&ph_ss_val,DSAT_MS_FIRST_SUBS);

  if (DS_CALL_STATUS_NONE == ph_ss_val->dsatcmif_cm_pkt_state)
  {
    DS_AT_MSG0_HIGH("no dormant status to report");
    return DSAT_ERROR;
  }

  res_buff_ptr = dsat_dsm_new_buffer(DSM_DS_SMALL_ITEM_POOL, FALSE);
  res_buff_ptr->used = snprintf((char*)res_buff_ptr->data_ptr,
                                       res_buff_ptr->size,
                                       "^DSDORMANT:%d",
                                       ph_ss_val->dsatcmif_cm_pkt_state
                                       );
  res_buff_ptr->data_ptr[res_buff_ptr->used] = '\0';

  formatted_rsp_ptr = dsat_dsm_new_buffer(DSM_DS_SMALL_ITEM_POOL, TRUE);
  if (NULL != formatted_rsp_ptr)
  {
    /* format the result code and send response */
    dsat_fmt_response( &res_buff_ptr, DSAT_COMPLETE_RSP, TRUE,
                       &formatted_rsp_ptr );

    if (NULL != formatted_rsp_ptr)
    {
      /* Flush the updated queue*/
      dsatcmdp_send_queued_cmd_results( );
  
      if ( formatted_rsp_ptr->used != 0 )
      {
        ds3g_siolib_set_tx_type(  DS3G_SIOLIB_UNSOLICTED_FAX_RESULT_CODE,
                                  DS3G_SIOLIB_USB_SER3_PORT);
        dsatrsp_send_response( formatted_rsp_ptr, TRUE );
      }
    }
  }

  return  DSAT_OK;

} /*cmif_generate_dormant_ind*/
#endif /* defined(FEATURE_HS_USB_SER3_PORT) */

/*===========================================================================

FUNCTION  DSATCMIF_IS_VOICE_CALL_TYPE

DESCRIPTION
  This function determines if the given call is voice call or not

DEPENDENCIES
  None

RETURN VALUE
  boolean

SIDE EFFECTS
  None

===========================================================================*/
boolean dsatcmif_is_voice_call_type( cm_call_type_e_type type)
{
  if(   (CM_CALL_TYPE_VOICE == type)
     || (CM_CALL_TYPE_EMERGENCY == type) 
#if defined (FEATURE_CDMA_800) || defined (FEATURE_CDMA_1900)
     || (CM_CALL_TYPE_STD_OTASP == type) 
     || (CM_CALL_TYPE_NON_STD_OTASP == type) 
#endif /*defined (FEATURE_CDMA_800) || defined (FEATURE_CDMA_1900)*/
     )
  {
    return TRUE;
  }
  else
  {
    return FALSE;
  }
} /* dsatcmif_is_voice_call_type */
/*===========================================================================
FUNCTION DSATCMIF_INITIALIZE_TO_DEFAULT

DESCRIPTION
  .

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
void dsatcmif_initialize_to_default( dsat_mem_type mem_type,void* ptr)
{
  dsat_num_item_type i = 0;
  
  if(NULL == ptr)
  {
    DS_AT_MSG0_ERROR("NULL pointer");
    return;
  }
  switch(mem_type)
  {
    case DSAT_SS_PH_SS_VALS:
     {
       dsatcmif_servs_state_ss_info  *ss = (dsatcmif_servs_state_ss_info  *)ptr;
       ss->cm_ph_deep_sleep = FALSE;
       ss->dsat_oprt_mode = SYS_OPRT_MODE_NONE;
#if defined(FEATURE_HS_USB_SER3_PORT)
       ss->dsatcmif_cm_pkt_state = DS_CALL_STATUS_NONE;
       ss->dsatcmif_dormant_ind_report_enabled = FALSE;
#endif /* FEATURE_HS_USB_SER3_PORT */
       memset ((void *)&ss->dsatcmif_cdma_signal_reporting, 0x0, sizeof(dsatcmif_sigrep_s_type));
       ss->dsatcmif_cdma_signal_reporting.rssi = RSSI_NO_SIGNAL;
       ss->dsatcmif_cdma_signal_reporting.sys_mode = SYS_SYS_MODE_NO_SRV;
       ss->dsatcmif_cdma_signal_reporting.hdr_hybrid = FALSE;
       ss->dsatcmif_cdma_signal_reporting.hdr_rssi = HDR_RSSI_NO_SIGNAL;
       
     }
     break;
    case DSAT_SS_PH_MS_VALS:
     {
       dsatcmif_servs_state_ms_info  *ms = (dsatcmif_servs_state_ms_info  *)ptr;
       ms->net_pref.last_dereg_success = TRUE;
       memset ((void *)&ms->dsat_net_reg_state, 0x0,sizeof(dsat_net_reg_state_s_type));
       memset ((void *)&ms->dsat_cen_num_list,0x0,sizeof(dsat_cen_emerg_num_list_s_type));
       memset ((void *)&ms->dsatcmif_signal_reporting, 0x0,sizeof(dsatcmif_sigrep_s_type));
#ifdef FEATURE_DSAT_ETSI_MODE
       ms->dsatcmif_signal_reporting.rssi = RSSI_NO_SIGNAL;
       ms->dsatcmif_signal_reporting.bit_err_rate = DSAT_BER_NO_SIGNAL;
       ms->dsatcmif_signal_reporting.ecio = DSAT_ECIO_NO_SIGNAL;
       ms->dsatcmif_signal_reporting.pathloss = DSAT_PATHLOSS_NO_SIGNAL;
       ms->dsatcmif_signal_reporting.sir = DSAT_SIR_NO_SIGNAL;
       dsatetsicall_reset_last_reported_creg(ms);
#endif /* FEATURE_DSAT_ETSI_MODE */
  /* HDR CSQ Initailzation */
       ms->dsatcmif_signal_reporting.hdr_hybrid = FALSE;
       ms->dsatcmif_signal_reporting.sys_mode = SYS_SYS_MODE_NO_SRV;
#ifdef FEATURE_DATA_IS707
       ms->dsatcmif_signal_reporting.hdr_rssi = HDR_RSSI_NO_SIGNAL;
#endif /* FEATURE_DATA_IS707 */
       ms->dsat_ws46_res_type = DSAT_WS46_NONE;
       ms->dsat_cfun_pref.is_card_action = TRUE;
       ms->dsat_cfun_pref.dsat_card_pdown_slot = MMGSDI_MAX_SLOT_ID_ENUM;

     }
     break;
    case DSAT_CALL_SS_VALS:
     {
       dsatetsicall_call_state_ss_info *ss = (dsatetsicall_call_state_ss_info *)ptr;
       for(i = 0; i < DSAT_MAX_DATA_CALLS; i++)
      {
        ss->dsatetsicall_s7_info[i].profile_id = 0;
        ss->dsatetsicall_s7_info[i].subs_id = INVALID_SUBS;
        ss->dsatetsicall_s7_info[i].data_s7_abort_handler = NULL;
      }
     }
     break;
    case DSAT_CALL_DA_VALS:
     {
       dsatetsicall_call_state_da_info  *da = (dsatetsicall_call_state_da_info  *)ptr;
       da->etsicall_was_waiting = FALSE;
       da->etsicall_was_call_active = FALSE;
         /* Initialize to no valid sequence numbers */
       for(i = 0; i< CM_CALL_ID_MAX ; i++)
       {
         da->dsat_seqnum_callid[i].call_id = CM_CALL_ID_INVALID;
         da->dsat_seqnum_callid[i].call_type= CM_CALL_TYPE_NONE;
       }
         /* Initialize +CEER reporting */
       memset((void*)&da->dsatetsicmif_call_end_status, 0x0, 
                             sizeof(dsatetsicmif_ces_info_s_type));
       
     }
     break;
    case DSAT_CALL_MS_VALS:
     {
       dsatetsicall_call_state_ms_info  *ms = (dsatetsicall_call_state_ms_info  *)ptr;
       ms->etsicall_prev_ccm_val = 0;
     }
     break;
    case DSAT_SUPS_SS_VALS:
     {
       
     }
     break;
    case DSAT_SUPS_MS_VALS:
     {
       dsatetsicmif_sups_state_ms_info  *ms = (dsatetsicmif_sups_state_ms_info  *)ptr;
       ms->dsat_ss_abort_status =  FALSE;
       memset ((void*)&ms->dsat_ss_cm_data, 0x0, sizeof(supserv_cm_op_s_type));
       ms->dsat_ss_cm_data.ussd_oper_type = processUnstructuredSS_Request;
       memset((void*)&ms->dsat_sups_cmd_info, 0x0, sizeof(dsat_sups_cmd_info_u_type));
       memset((void *)&ms->dsatetsicall_clir_subscription_option, 0,sizeof(cli_restriction_info_T));
       
     }
     break;
   default:
    break;
  }
}

/*===========================================================================
FUNCTION  DSAT_CM_PH_SUBS_PREF_HANDLER

DESCRIPTION
  This function is the event handler invoked by CM for informing subscription
  specific info to ATCoP.
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
void dsat_cm_ph_subs_pref_handler
(
  void         * ds_cmd_ptr              /* DS Command pointer         */
)
{
  ds3g_cm_sub_info_type *pref_event_ptr;
  ds_cmd_type         * cmd_ptr = (ds_cmd_type*)ds_cmd_ptr;

  ASSERT( cmd_ptr != NULL );

  pref_event_ptr = &cmd_ptr->cmd.sub_info;

  DS_AT_MSG4_MED("session_type_1x = %d, session_type_gw = %d, "
                 "subscription_id = %d, is_operational = %d",
                 pref_event_ptr->session_type_1x,
                 pref_event_ptr->session_type_gw,
                 pref_event_ptr->subscription_id,
                 pref_event_ptr->is_operational);

  DS_AT_MSG4_MED("is_default_data = %d, network_sel_mode_pref = %d, "
                 "sub_feature = %d, is_available_gw = %d",
                 pref_event_ptr->is_default_data,
                 pref_event_ptr->pref_info.network_sel_mode_pref,
                 pref_event_ptr->sub_feature_mode,
                 pref_event_ptr->is_available_gw);

  switch (cmd_ptr->hdr.cmd_id )
  {
    case DS_CM_STANDBY_SUB_INFO_CMD:
#ifndef  FEATURE_TRIPLE_SIM
      if (pref_event_ptr->session_type_gw != MMGSDI_GW_PROV_SEC_SESSION)
      {
        cmif_cm_dual_standby_sub_info_cmd_handler(pref_event_ptr->session_type_1x,
                                                  pref_event_ptr);
      }
#endif /* FEATURE_TRIPLE_SIM */
      cmif_cm_dual_standby_sub_info_cmd_handler(pref_event_ptr->session_type_gw,
                                                pref_event_ptr);
      break;

    default:
      DS_AT_MSG1_ERROR("Unknown event %d",cmd_ptr->hdr.cmd_id);
      break;
  }
}

/*===========================================================================
FUNCTION  DSAT_CM_DUAL_STANDBY_SUB_INFO_CMD_HANDLER

DESCRIPTION
  This function is the event handler invoked by CM for informing
  dual standby subscription specific info to ATCoP.
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
LOCAL void cmif_cm_dual_standby_sub_info_cmd_handler
(
  mmgsdi_session_type_enum_type  session_type,
  ds3g_cm_sub_info_type         *pref_event_ptr
)
{
  uint8 apps_id = 0;
  dsatcmif_servs_state_ms_info  *ph_ss_ms_val = NULL;

  ASSERT( pref_event_ptr != NULL );

  DS_AT_MSG1_MED("Processing CM DUAL STANDBY SUB INFO for Session type = %d",
                 session_type);

  /* Update apps_id */
  switch(session_type)
  {
    case MMGSDI_GW_PROV_PRI_SESSION:
      apps_id = PRIMA_SUBS;
      break;
    case MMGSDI_GW_PROV_SEC_SESSION:
      apps_id = SECON_SUBS;
      break;
#ifdef FEATURE_TRIPLE_SIM
    case MMGSDI_GW_PROV_TER_SESSION:
      apps_id = THIRD_SUBS;
      break;
#endif /* FEATURE_TRIPLE_SIM */
    case MMGSDI_1X_PROV_PRI_SESSION:
    case MMGSDI_MAX_SESSION_TYPE_ENUM: /* CDMA NV subscription */
      apps_id = CDMA_SUBS;
      break;
    default:
      DS_AT_MSG1_ERROR("Unsupported Session Type: %d", session_type);
      return;
  }
  
  dsat_qcsimapp_info[apps_id].sub_feature_mode = pref_event_ptr->sub_feature_mode;
  
  if(pref_event_ptr->is_operational == FALSE )
  {
    if(apps_id == CDMA_SUBS)
    {
      dsat_qcsimapp_info[apps_id].subs_id = SYS_MODEM_AS_ID_MAX;
    }
    return;
  }
  
  dsat_qcsimapp_info[apps_id].subs_id = pref_event_ptr->subscription_id;
  dsat_qcsimapp_info[apps_id].is_default_data = pref_event_ptr->is_default_data;
  if( SYS_SUBS_FEATURE_MODE_SGLTE == pref_event_ptr->sub_feature_mode )
  {
    dsat_qcsimapp_info[apps_id].is_sglte_sub = TRUE;
    /*Update SGLTE second stack place holder */
#ifdef FEATURE_SGLTE
    dsatcmif_set_sglte_hybr_subs_info(DS_ALL_SUBS);
#endif /* FEATURE_SGLTE */
  }
  /* Indicates subscription ready */
  dsat_qcsimapp_info[apps_id].active = TRUE;
  /* To allow QCSIMAPP to select the subscription */
  dsat_qcsimapp_info[apps_id].app_selected = TRUE;
  /* Set qcsimapp to data subscription by default */
  if(dsat_qcsimapp_info[apps_id].is_default_data == TRUE)
  {
    if (apps_id == CDMA_SUBS)
    {
      dsat_set_qcsimapp_val(PRIMA_SUBS);
    }
    else
    {
      dsat_set_qcsimapp_val(apps_id);
    }
  }

  if(pref_event_ptr->is_operational == TRUE )
  {
    if ((apps_id < MAX_SUBS) && (pref_event_ptr->is_available_gw == TRUE))
    {
      if(DSAT_FAILURE == dsatutil_get_base_addr(DSAT_SS_PH_MS_VALS,(void **)&ph_ss_ms_val,apps_id))
      {
        return;
      }
      ph_ss_ms_val->net_pref.last_dereg_success = FALSE;
      if(pref_event_ptr->pref_info.network_sel_mode_pref == CM_NETWORK_SEL_MODE_PREF_MANUAL)
      {
        DSATUTIL_SET_VAL( DSATETSI_EXT_ACT_COPS_ETSI_IDX, apps_id, 0, 0,
                          DSAT_COPS_MODE_MANUAL, MIX_NUM_TYPE );
      }
      else
      {
        DSATUTIL_SET_VAL( DSATETSI_EXT_ACT_COPS_ETSI_IDX, apps_id, 0, 0,
                          DSAT_COPS_MODE_AUTO, MIX_NUM_TYPE );
      }
    }
  }
}

/*===========================================================================

FUNCTION    DSATCMIF_UPDATE_SUBS_IDX

DESCRIPTION
  Returns  subs_id based on as_id
  
DEPENDENCIES
  None

RETURN VALUE
  uint8

SIDE EFFECTS
  None

===========================================================================*/

ds_subs_e_type dsatcmif_update_subs_idx(sys_modem_as_id_e_type as_id)
{
  ds_subs_e_type subs_id = DS_FIRST_SUBS;

  switch( as_id )
    {
    case SYS_MODEM_AS_ID_1:
      subs_id = DS_FIRST_SUBS;
      break;
    case SYS_MODEM_AS_ID_2:
      subs_id = DS_SECOND_SUBS;
      break;
#ifdef FEATURE_TRIPLE_SIM
    case SYS_MODEM_AS_ID_3:
      subs_id = DS_THIRD_SUBS;
      break;
#endif /* FEATURE_TRIPLE_SIM */
    default:
      subs_id = DS_SUBS_MAX;
      DS_AT_MSG1_ERROR("Invalid as_id:%d",as_id);
      break;
  }
  return subs_id;
}

#ifdef FEATURE_SGLTE

/*===========================================================================

FUNCTION DSATCMIF_GET_SGLTE_HYBR_SUBS_INFO
    
DESCRIPTION
   This function used to get hybrid subscription info.
    
DEPENDENCIES
   None
RETURN VALUE
.  dsat_memory_subs_type
    
SIDE EFFECTS
   None
      
===========================================================================*/

ds_subs_e_type dsatcmif_get_sglte_hybr_subs_info()
{
  return dsat_hybr_subs_id;
}/* dsatcmif_get_sglte_hybr_subs_info */

/*===========================================================================

FUNCTION DSATCMIF_SET_SGLTE_HYBR_SUBS_INFO
    
DESCRIPTION
   This function used to set hybrid subscription info.
    
DEPENDENCIES
   None
RETURN VALUE
.  None
    
SIDE EFFECTS
   None
      
===========================================================================*/

void dsatcmif_set_sglte_hybr_subs_info(ds_subs_e_type hybr_subs)
{
  if(hybr_subs < DS_SUBS_MAX)
  {
    dsat_hybr_subs_id = hybr_subs;
  }
}/* dsatcmif_set_sglte_hybr_subs_info */

#endif /*FEATURE_SGLTE*/

/*===========================================================================
FUNCTION CMIF_IS_GW_SUBSCRIPTION_AVAILABLE 

DESCRIPTION
   This function is used to check if we have subscription available
   for the subs_info passed to this function.

DEPENDENCIES
   None

RETURN VALUE
.  TRUE : if the subscription is available for the subs_info passed.
   FALSE: if the subscription is unavailable for the subs_info passed.

SIDE EFFECTS
   None

===========================================================================*/
LOCAL boolean cmif_is_gw_subscription_available
(
  ds_subs_e_type             subs_info,
  ds_at_cm_ph_event_type     *ph_event
)
{
  boolean is_gw_subscription_available = FALSE;
  switch(subs_info)
  {
    case DS_FIRST_SUBS:
    {
      is_gw_subscription_available = ph_event->event_info.is_gw_subscription_available;
      break;
    }
    case DS_SECOND_SUBS:
    {
      /* For SGLTE+G, is_hybr_gw_subscription_available is used for stack1, SUB1 
         is_hybr_gw_subscription_available is used for stack2,SUB1
         is_hybr_gw3_subscription_available is used for SUB2 */
      if (SYS_SUBS_FEATURE_MODE_NORMAL != dsat_qcsimapp_info[DS_FIRST_SUBS].sub_feature_mode)
      {
        is_gw_subscription_available = ph_event->event_info.is_hybr_gw3_subscription_available;
      }
      else
      {
        is_gw_subscription_available = ph_event->event_info.is_hybr_gw_subscription_available;
      }
      break;
    }
#ifdef FEATURE_TRIPLE_SIM
    case DS_THIRD_SUBS:
    {
      is_gw_subscription_available = ph_event->event_info.is_hybr_gw3_subscription_available;
      break;
    }
#endif /* FEATURE_TRIPLE_SIM */
    default:
     DS_AT_MSG1_ERROR("Invalid subs_info %d", subs_info);
     is_gw_subscription_available = FALSE;
  }
  
  DS_AT_MSG2_HIGH("subs_info: %d,is_gw_subscription_available:%d", subs_info,
                                                                   is_gw_subscription_available);
  return is_gw_subscription_available;
}
