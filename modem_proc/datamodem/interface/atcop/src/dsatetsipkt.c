/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                D A T A   S E R V I C E S

                U M T S

                P A C K E T   D O M A I N
                                
                A T   C O M M A N D   P R O C E S S I N G

GENERAL DESCRIPTION
  This module executes AT commands. It executes UMTS & GPRS specific packet
  domain AT commands.

  
EXTERNALIZED FUNCTIONS

  dsatetsipkt_exec_cgclass_cmd
    This function takes the result from the command line parser 
    and executes it. It handles the +CGCLASS command. It supports
    only CLASSB. 

  dsatetsipkt_exec_cgpaddr_cmd
    This function takes the result from the command line parser
    and executes it. It handles the +CGPADDR command.
    This command returns a list of PDP addresses for the specified
    context identifiers

  dsatetsipkt_cgpaddr_response_fmt
    This function generates the response for the +CGPADDR read & test
    commands.  Data is extracted from data structure and formatted according
    to parameter flag.
  
  dsatetsipkt_exec_cgdcont_cmd
    This function takes the result from the command line parser
    and executes it. It handles the +CGDCONT command.
    This command sets basic PDP context parameters.

  dsatetsipkt_exec_cgdscont_cmd
    This function takes the result from the command line parser
    and executes it. It handles the +CGDSCONT command.
    This command sets PDP secondary context parameters.

  dsatetsipkt_exec_cgtft_cmd  
    This function takes the result from the command line parser
    and executes it. It handles the +CGTFT command.
    This command sets PDP Traffic Flow Template parameters.

  dsatetsipkt_cgdcont_response_fmt
    This function generates the response for the +CGDCONT read & test
    commands.  Data is extracted from data structure and formatted according
    to parameter flag.

  dsatetsipkt_exec_cgeqreq_cmd
    This function takes the result from the command line parser
    and executes it. It handles the +CGEQREQ command.
    This command sets PDP context parameters for UMTS QoS Profile
    (Requested).

  dsatetsipkt_exec_cgeqmin_cmd
    This function takes the result from the command line parser
    and executes it. It handles the +CGEQMIN command.
    This command sets PDP context parameters for UMTS QoS Profile
    (Minimum).

  dsatetsipkt_cgeqreq_response_fmt
    This function generates the response for the +CGEQREQ read & test
    commands.  Data is extracted from data structure and formatted according
    to parameter flag.

  dsatetsipkt_exec_cgqreq_cmd
    This function takes the result from the command line parser
    and executes it. It handles the +CGQREQ command.
    This command sets PDP context parameters for GPRS QoS Profile
    (Requested).

  dsatetsipkt_cgqreq_response_fmt
    This function generates the response for the +CGQREQ read & test
    commands.  Data is extracted from data structure and formatted according
    to parameter flag.

  dsatetsipkt_exec_cgqmin_cmd
    This function takes the result from the command line parser
    and executes it. It handles the +CGQMIN command.
    This command sets PDP context parameters for GPRS QoS Profile
    (Minimum Acceptable).

  dsatetsipkt_cgqmin_response_fmt
    This function generates the response for the +CGQMIN read & test
    commands.  Data is extracted from data structure and formatted according
    to parameter flag.

  dsatetsipkt_exec_qcpdpp_cmd
    This function takes the result from the command line parser
    and executes it. It handles the $QCDPDP command.
    This command sets PDP authentication parameters.

  dsatetsipkt_exec_cgatt_cmd
    This function takes the result from the command line parser
    and executes it. It handles the +CGATT command.
    This command reports the connection status for network packet
    domain service.

  dsatetsipkt_exec_cgact_cmd
    This function takes the result from the command line parser
    and executes it. It handles the +CGACT command.
    This command activates or deactivates PDP context profiles.

  dsatetsipkt_reset_pdp_context_profiles
    This function resets all persistent PDP context profiles to their
    default values.

  dsatetsipkt_exec_cgcmod_cmd
    This function takes the result from the command line parser
    and executes it. It handles the +CGCMOD command.
    This command modifies the Qos, TFT and APN of active PDP contexts.

  dsatetsipkt_exec_cgauto_cmd
    This function takes the result from the command line parser
    and executes it. It handles the +CGAUTO command. This command disables or 
	enables an automatic response for a MT PDP Context Activation message. 
	
  dsatetsipkt_exec_cgans_cmd
    This function takes the result from the command line parser
    and executes it. It handles the +CGANS command.
	This command is used to answer a incoming MT PDP call.

  dsatetsipkt_exec_cemode_cmd
    This function takes the result from the command line parser
    and executes it. It handles the +CEMODE command.

  dsatetsipkt_exec_cnmpsd_cmd
    This function executes +CNMPSD command which releases PS signaling.
    This command indicates that no application on the MT is expected to
    exchange data.

    
INITIALIZATION AND SEQUENCING REQUIREMENTS


   Copyright (c) 2001 - 2014 by Qualcomm Technologies Incorporated.
   All Rights Reserved.
   Qualcomm Confidential and Proprietary.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/


/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $PVCSPath:  L:/src/asw/MSM5200/data/vcs/dsatetsipkt.c_v   1.7   12 Jul 2002 10:05:36   randrew  $  
  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/atcop/src/dsatetsipkt.c#1 $   $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $
  
when       who     what, where, why
--------   ---     ----------------------------------------------------------
7/27/14    pg      ATCoP changes for Data Plus Data.
03/11/14   tk/sc   Optimized debug macros usage in ATCoP.
11/14/13   tk      Fixed issues in online command state.
09/24/13   sc      Added support for +CNMPSD command.
09/13/13   sc      Fixed KW errors.
05/22/13   sk      Fixed Issues with AT+CGCONTRDP and CGTFTRDP.
03/27/13   tk      Fixed issues in dual stack commands.
03/21/13   sk      Fixed CGPADDR dual IP separator issue.
02/01/13   sk      Fixed CGEREP URC Issue with PDP reject.
08/22/12   sk      Mainlining Nikel Changes.
07/26/12   sb      Fixed KW Errors.
07/25/12   tk      Fixed CGPADDR issue in display of static IP addresses.
07/19/12   sk      CGACT issue fixed.
05/18/12   tk      Migrated to MSG 2.0 macros
03/19/12   sk      traffic handling priority check removed for CGEQREQ.
02/17/12   sk      Migrating Data related AT commands interface to unified MH.
02/06/12   ad      Added CSTF/CEN/CGPIAF/CVMOD command support.
01/19/12   sk      Feature cleanup.
01/04/12   sb      Fixed KW Warnings.
12/06/11   sb      Avoid QOS mismatch between GPRS and Umts profile data.
11/03/11   sb      Fixed KW Warnings.
10/05/11   ht      Fixed Klocwork Issue.
09/13/11   ttv     Changes to add src_stat_desc as profile parameter.
07/27/11   ttv     Added changes required for source statistics descriptor
                   addition.
05/23/11   ad      Allowed to set Packet Filter ID from 1 to 16.
04/01/11   ttv     Added changes to use get/set functions.
03/01/11   nc      Modified +CEMODE to be inline with CM Design.
02/16/11   ttv     Added DSDS changes for consolidated profile family.
02/16/11   bs      Fixed memory alignment issue.
01/11/11   ad      Remove extern usage.used get/set API for command associated  
                   value pointers.
10/25/10   ad      Init Changes for DSDS. 
10/06/10   ad      Added Error msg before Assert(0).
09/16/10   nc      Fixed CGCONTRDP.
09/16/10   nc      Fixed the loop for forwarding a secondary PDP context
                   request to MH.
08/31/10   sa      Added support for default pdp type as IPv4v6.
07/09/10   bs      Fixed +CGACT for LTE.
05/31/10   ad      Fixed Klocwork errors. 
05/15/10   nc      Added support fot +CGCONTRDP,+CGSCONTRDP,
                   +CGEQOSRDP and +CGTFTRDP for LTE.
05/14/10   sa      Added support for LTE packet event reporting.
04/20/10   bs      Added support for +CGCMOD for LTE.
03/18/10   nc      Added Support for +CGEQOS and +CGPADDR for LTE.
01/15/10   ua      Using new send_response_to_te API. 
01/06/10   bs      Added +CGACT command support for LTE.
01/04/10   vk      Removing certain macros from PDP reg API header file
                   that cause problems with autogenerated RPC stub code
12/15/09   nc      Featurisation changes for LTE.
12/07/09   nc      Added support for <IPv4AddrAlloc> parameter for +CGDCONT.
10/12/09   vk      PDP profile changes for IPv4v6 support
10/09/09   sa      Adding support for +CEMODE command.
09/10/09   nc      Fixed Klocwork errors. 
09/21/09   sch     Accepting DS_UMTS_PDP_AUTH_CHAP_OR_PAP as a valid auth type
08/20/09   nc      Fixed the conversion of Uplink Max Bit rate for Rel7.
08/10/09   ars     CMI featurization related changes. Modified NUM_PROTOCOL_ID
06/29/09   vg	   Replacing MSG_XXX with MSG_SPRINTF_X.
04/20/09   ua      Fixed issues related with call commands followed by +CIND Query.
06/25/09   vk      Removing use of certain deprecated macros in pdp reg api
06/18/09   sa      Integrated the support for higher maximum bitrate and
                   guaranteed bit rate in the QoS IE for Rel7.
05/19/09   vk      Defeaturization of PDP registry for CMI
04/16/09   sa      Added DS_UMTS_QOS_RES_BER_1E5 for Conversational class.
01/30/09   ua      Added support of CGEV for DUN calls.
12/12/08   ua      Fixed Off target lint errors.
12/02/08   ss      Off target Lint fix.
10/23/08   bs      Added support for 1X AT Phonebook commands.
09/03/08   bs      Fixed Alignment issues with MDM8200.
07/31/08   nc      Verification of transfer delay is 
                   ignored for interactive and background class 
03/24/08   bs      Fixed Klocwork criticals.
11/04/07   pp      Lint low - fixes.
10/09/07   ss      Added support for $QCGANSM, $QCGARL, $QCGAPL commands.
08/22/07   sa      Added support for +CGANS and +CGAUTO commands.
08/06/07   sa      Removed +CGEQREQ and +CGEQMIN support in GSM only builds.
08/02/07   sa      Feature wrap changes on header and data compression for +CGDCONT
                   and +CGDSCONT.
07/12/07   sa      Fixing side effects of Banned APIs changes. 
04/26/07   sa      Fixed lint medium errors.
05/02/07   ss      Modified dsatetsipkt_setup_cm_orig_params to fix compilation
                   error when FEATURE_SECONDARY_PDP is disabled
04/05/07   pkp     +CGATT syntax corrections.
03/30/07   sa      Fixing buffer size for APN string in +CGDCONT.
03/29/07   ua      Reverting back CGCLASS modifications which supported CLASS B.
                   Now it supports only CLASS A. 
02/15/07   ss      Fixed lint high errors
02/15/07   ph      Lint CRITICAL fixes
02/13/07   pkp     Fixed RVCT compiler warnings.
01/30/07   ss      Replace banned string API calls.
12/07/07   ua      Fixing buffer overflow and also correcting the display 
                   for cgdscont test command.
11/03/06   snb     Dual processor changes.
10/30/06   ua      Correcting +CGACT state value verification.
10/27/06   sa      Fix for cid values not getting undefined for CGPADDR.
09/14/06   snb     Comply with IPCP API change.
08/03/06   sa      Add header and data compression support for +CGDCONT and +CGDSCONT.
08/01/06   snb     Clear query_mode on failure.
06/08/06   ua      Modification of +CGCLASS command to support only CLASSB.
02/21/06   sa      Merged changs from Sirius Branch
                   -Fixed additional buffer allocation for 
                   etsipkt_initiate_packet_call.
12/08/05   snb     Add IPV6 support
11/30/05   snb     Add support for +CGCMOD command.
10/02/05   snb     On +CGATT detach command when already detached still send 
                   user preference to lower layers.
07/29/05   snb     Use correct mode type in call to dial command for +CGDATA
                   processing.
05/09/05   tkk     Modification to dsat_addr_info for CID retrieval.
05/03/05   tkk     Added execution syntax to +CGPADDR command.
04/12/05   snb     Removed mutual exclusive setting of +CGQREQ/+CGEQREQ and
                   +CGQMIN/+CGEQMIN parameters.
04/05/05   tkk     Corrected test syntax for +CGPADDR command.
04/04/05   snb     Made external a function returning number of contexts 
                   without an assigned TFT and removed verification of 
                   "1 context without TFT" rule.
03/15/05   tkk     Fixed bugs for +CGPADDR and +CGEREP commands.
01/31/05   tkk     Added +CGDATA command.
01/27/05   pdv     Merge from Sirius-ATCoP development.
01/27/05   pdv     Rebase from Mainline.
01/17/05   tkk     Added support for +CGEREP command.
01/11/05   snb     Output +CGDCONT's PDP address as quoted string.
01/10/05   tkk     Added support for +CGPADDR command.
11/22/04    rr     Set higher order byte of profile number to DS_UMTS_PDP_
                   ATCOP_PROFILE_FAMILY to access profiles from 
                   "pdp_profile/atcop_profiles" directory
09/13/04   snb     Fix buffer overflow in $QCPDPP read command when 16 profiles
                   defined and auth parms set for each with maximum length user
                   names.
06/21/04   snb     Add support for 16 PDP profiles.
06/01/04   snb     Corrected +CGDCONT portion of &V response.
05/12/04   snb     Corrected +CGACT reponse when no contexts defined.
01/05/04   ar      Added mapping for profile QoS delivery err SDUs param.
11/19/03   ar      Replace dsatutil_format_ipaddr with dsatutil_convert_tuple
                   Added support for +CGDSCONT and +CGTFT commands.
                   Removed unused etsipkt_reset_multi_param().
09/29/03   ar      Remove default value handling on +CGACT parameters.
09/25/03   sb      Fix the order in which h_comp and d_comp parameters of 
                   +CGDCONT are copied into the profile (EFS).
08/13/03   snb     Added support for $QCPDPLT command
06/24/03   ar      Remove LOCAL on staticly declared structures.
03/20/03   ar      Generate +CGACT response only for defined contexts.
                   Gracefully handle PDP context API failure in &V response.
03/17/03   ar      Move VALID_TOKEN macro to common header file.
02/26/03   ar      Reset profile section to default before update on
                   +CGxxxx write syntax
02/11/03   ar      Use persistent context profile for +CGEQMIN. Remove order
                   dependency on context profile commands.
02/03/03   ar      Suppress action in +CGATT if demand same as current state.
01/30/03   ar      Add support for +CGEQMIN command
01/10/03   sb      Added WRITE operation for +CGATT command
01/06/03   ar      Migrated context cmds to persistent PDP profile support 
08/26/02   ar      Move special paramater validation before saving input
                   parameters. Code review action items.
07/11/02   ar      Add support for +CGQMIN command
07/10/02   ar      Add validation of +CGDCONT APN parameter
06/28/02   ar      Add support for +CGEQREQ and +CGQREQ commands
04/11/02   ar      Add support for $QCPDPP command
02/04/02   ar      Add support for +CGDCONT command
08/27/01   sb      Created module.

===========================================================================*/


/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"

#include <stringl/stringl.h>

#ifdef FEATURE_DSAT_ETSI_DATA
#include "stdio.h"

#include <stringl/stringl.h>
#include "msg.h"
#include "nv.h"
#include "dstaski.h"

#include "dsati.h"
#include "dsatparm.h"
#include "dsatetsictab.h"
#include "dsatetsime.h"
#include "dsatetsipkt.h"
#include "dsumtspdpreg.h"
#include "ds_profile.h"
#include "dsatetsicmif.h"
#include "dstaski.h"
#include "nv.h"
#include "sm.h"
#include "sm_common.h"
#include "cm.h"
#include "dsatact.h"

#ifdef FEATURE_DSAT_UMTS_DATA
#include "dsumtspspco.h"
#include "dsumtspsqos.h"
#endif /* FEATURE_DSAT_UMTS_DATA */

#include "ps_ppp_defs.h"
#include "ps_utils.h"
#include "ps_in.h"


/*===========================================================================

            LOCAL DEFINITIONS AND DECLARATIONS FOR MODULE

  This section contains local definitions for constants, macros, types,
  variables and other items needed by this module.

===========================================================================*/

/*-------------------------------------------------------------------------
  These macros used to be in dsumtspdpreg.h but had to removed from there 
  due to compilation issues seen with pdp reg rpc stub code.
-------------------------------------------------------------------------*/
#define pdp_addr_ipv4 pdp_addr.pdp_addr_ipv4
#define pdp_addr_ipv6 pdp_addr.pdp_addr_ipv6

/* Constant Macros */
#define MAX_INDEX 255   /* Table terminator */
#define MAX_TMP_BUFF_SIZE 200     /* Temporary buffer limit */

/* Macro to perform assigment only if user parameter specified */
#define CONDITIONAL_ASSIGN(i,dest,src)\
          if (VALID_TOKEN(i))\
          {\
            dest = src;\
          }

/* NOTE : THE FOLLOWING MACROS RETURN FROM THE CALLING FUNCTION 
 *        IF AN ERROR OCCURS */

/* Macro to reset the context base paramaters  */
#define RESET_BASE_CONTEXT_PARAMS(cid)\
        if (DS_UMTS_PDP_SUCCESS != \
            ds_umts_reset_pdp_profile_context_info_per_subs (\
              SET_CID( cid ),dsat_get_current_subs_id() )) \
        { \
          DS_AT_MSG1_HIGH("Error on profile context reset: %d",cid); \
          result = DSAT_ERROR; \
        }

/* Macros to set the context base parameters to IPV4 defaults */
#define DEFAULT_CONTEXT_PARAMS(cid) SET_CID( cid ),dsat_get_current_subs_id(),DS_IP_V4

#define DEFAULT_BASE_CONTEXT_PARAMS(cid)\
        if (DS_UMTS_PDP_SUCCESS != \
            ds_umts_set_pdp_profile_context_info_to_default_per_subs (\
              DEFAULT_CONTEXT_PARAMS(cid) )) \
        { \
          DS_AT_MSG1_HIGH("Error on profile context default: %d",cid); \
          return DSAT_ERROR; \
        }

/* Macro to test pbase profile validity and set valid flag  */
#define IS_BASE_CONTEXT_VALID(cid,result)\
        if (DS_UMTS_PDP_SUCCESS != \
            ds_umts_get_pdp_profile_context_info_is_valid_per_subs(\
              SET_CID( cid ), dsat_get_current_subs_id(), &valid_flg)) \
        { \
          DS_AT_MSG1_HIGH("Error on PDP profile valid query: %d",cid); \
          result = DSAT_ERROR; \
        }

#define CHECK_APPEND_RESPONSE_BUFFER(buf_limit)\
        if (NULL == (res_buff_ptr = \
            etsipkt_check_response_buffer(res_buff_ptr, \
                                          buf_limit))) \
        { \
          return 0; \
        }

#define CHECK_APPEND_RESPONSE_BUFFER_NEW(buf_limit)\
        if (NULL == (res_buff_ptr = \
            etsipkt_check_response_buffer(res_buff_ptr, \
                                          buf_limit))) \
        { \
          DS_AT_MSG0_ERROR("NO DSM Items available");\
          return DSAT_ERROR; \
        }


#ifdef FEATURE_DATA_PS_IPV6
  /* IP(0)/IPv4(0),,PPP(1), IPv6(2) and IPv4v6(3)are valid */
  #define DSAT_MAX_PDP_TYPE DS_UMTS_PDP_IPV4V6 
#else  /* FEATURE_DATA_PS_IPV6 */
  /* IP,PPP and IPv4v6 are valid */
  #define DSAT_MAX_PDP_TYPE  2  
#endif /* FEATURE_DATA_PS_IPV6 */

#ifdef FEATURE_SECONDARY_PDP
/* Enum for TFT parameter references */
enum cgtft_params
{
  CGTFT_CID,
  CGTFT_PFI,
  CGTFT_EPI,
  CGTFT_SA_SM,
  CGTFT_PN_NH,
  CGTFT_DSTP,
  CGTFT_SRCP,
  CGTFT_SPI,
  CGTFT_TOS,
  CGTFT_FLOW,
  CGTFT_MAX      /* Internal use only! */
};
#endif /* FEATURE_SECONDARY_PDP */

/* Defaults from dsatetsictab.c */
extern const dflm_type dsat_pdp_cid_dflm[];
extern const dflm_type dsat_emergency_dflm[];

extern const dsat_string_item_type dsat_pdptype_valstr[][8];
extern const dsat_string_item_type dsat_cgdcont_hcomp_valstr [][8];
extern const dsat_string_item_type dsat_cgdcont_hcomp_tststr[];
extern const dsat_string_item_type dsat_cgdcont_dcomp_valstr [][8];
extern const dsat_string_item_type dsat_cgdcont_dcomp_tststr[];

#ifdef FEATURE_SECONDARY_PDP
extern const dflm_type dsat_cgtft_pfid_dflm[];
extern const dflm_type dsat_cgtft_prec_dflm[];
extern const dflm_type dsat_cgtft_pn_nh_dflm[];
extern const dflm_type dsat_cgtft_spi_dflm[];
extern const dflm_type dsat_cgtft_flow_dflm[];
#endif /* FEATURE_SECONDARY_PDP */

#ifdef FEATURE_DATA_WCDMA_PS
extern const dflm_type dsat_cgeqreq_tclass_dflm[];
extern const dflm_type dsat_cgeqreq_req_ul_bitrate_dflm[];
extern const dflm_type dsat_cgeqreq_req_dl_bitrate_dflm[];
extern const dflm_type dsat_cgeqreq_gtd_ul_bitrate_dflm[];
extern const dflm_type dsat_cgeqreq_gtd_dl_bitrate_dflm[];
extern const dflm_type dsat_cgeqreq_dlvry_order_dflm[];
extern const dflm_type dsat_cgeqreq_max_sdu_size_dflm[];
extern const dsat_string_item_type dsat_cgeqreq_sdu_err_tststr[];
extern const dsat_string_item_type dsat_cgeqreq_res_biterr_tststr[];
extern const dflm_type dsat_cgeqreq_dlvry_err_dflm[];
extern const dflm_type dsat_cgeqreq_trans_delay_dflm[];
extern const dflm_type dsat_cgeqreq_thandle_prio_dflm[];
extern const dsat_string_item_type dsat_cgeqreq_sdu_err_valstr[][8];
extern const dsat_string_item_type dsat_cgeqreq_res_biterr_valstr [][8];

extern const dflm_type dsat_cgeqmin_tclass_dflm[];
extern const dflm_type dsat_cgeqmin_req_ul_bitrate_dflm[];
extern const dflm_type dsat_cgeqmin_req_dl_bitrate_dflm[];
extern const dflm_type dsat_cgeqmin_gtd_ul_bitrate_dflm[];
extern const dflm_type dsat_cgeqmin_gtd_dl_bitrate_dflm[];
extern const dflm_type dsat_cgeqmin_dlvry_order_dflm[];
extern const dflm_type dsat_cgeqmin_max_sdu_size_dflm[];
extern const dsat_string_item_type dsat_cgeqmin_sdu_err_tststr[];
extern const dsat_string_item_type dsat_cgeqmin_res_biterr_tststr[];
extern const dflm_type dsat_cgeqmin_dlvry_err_dflm[];
extern const dflm_type dsat_cgeqmin_trans_delay_dflm[];
extern const dflm_type dsat_cgeqmin_thandle_prio_dflm[];
extern const dsat_string_item_type dsat_cgeqmin_sdu_err_valstr[][8];
extern const dsat_string_item_type dsat_cgeqmin_res_biterr_valstr [][8];
#endif /* FEATURE_DATA_WCDMA_PS */

extern const dflm_type dsat_cgqreq_prec_dflm[];
extern const dflm_type dsat_cgqreq_delay_dflm[];
extern const dflm_type dsat_cgqreq_rel_dflm[];
extern const dflm_type dsat_cgqreq_peak_dflm[];
extern const dflm_type dsat_cgqreq_mean_dflm[];

extern const dflm_type dsat_cgqmin_prec_dflm[];
extern const dflm_type dsat_cgqmin_delay_dflm[];
extern const dflm_type dsat_cgqmin_rel_dflm[];
extern const dflm_type dsat_cgqmin_peak_dflm[];
extern const dflm_type dsat_cgqmin_mean_dflm[];

/* Flag for detach writes intended for local stack only:
   This change for GCF test cases allows user preference to be given to 
   local stack only and lets async event processing terminate once CMPH
   event is received as no serving system event is expected.  */
extern boolean dsatcmif_ss_evt_not_expected;

#ifdef FEATURE_DSAT_LTE
extern const dflm_type dsat_qci_dflm[];
extern const dflm_type dsat_cgeqos_req_dl_gbr_dflm[];
extern const dflm_type dsat_cgeqos_req_ul_gbr_dflm[];
extern const dflm_type dsat_cgeqos_req_dl_mbr_dflm[];
extern const dflm_type dsat_cgeqos_req_ul_mbr_dflm[];
#endif /* FEATURE_DSAT_LTE */

/* Invalid PDP profile id */
#define INVALID_CID (0x0)

/* PDP context state information*/
byte  dsat_pdp_cid[DS_UMTS_MAX_PDP_PROFILE_NUM];  /* Context profile id */
dsat_pdp_connect_s_type dsat_pdp_state;

LOCAL ds_3gpp_atif_dynamic_params_info_type  *dynamic_param_ptr;


/* Scratch buffer */
byte etsipkt_tmp_buf[MAX_TMP_BUFF_SIZE];


/* Added for +CGDATA support */
LOCAL dsat_cgdata_info_s_type    dsat_cgdata_context;

/* +CGEREP variables */

LOCAL char *etsipkt_cgerep_event_buffer[] = {"+CGEV: REJECT ",
                                             "+CGEV: NW REACT ",
                                             "+CGEV: NW DEACT ",
                                             "+CGEV: ME DEACT ",
                                             "+CGEV: NW DETACH",
                                             "+CGEV: ME DETACH",
                                             "+CGEV: NW CLASS CG",
                                             "+CGEV: NW CLASS CC",
                                             "+CGEV: ME CLASS B",
                                             "+CGEV: ME CLASS CG",
                                             "+CGEV: ME CLASS CC",
                                             "+CGEV: PDN ACT",
                                             "+CGEV: PDN DEACT",
                                             "+CGEV: DED NW DED ACT",
                                             "+CGEV: ME ACT",
                                             "+CGEV: DED NW DEACT",
                                             "+CGEV: ME DEACT",
                                             "+CGEV: NW MODIFY",
                                             "+CGEV: ME MODIFY"
};


/*-------------------------------------------------------------------------
    Protypes for local functions:
-------------------------------------------------------------------------*/

LOCAL dsat_result_enum_type etsipkt_validate_apn
(
  byte *s_ptr                           /*  Points to string  */
);

LOCAL dsat_result_enum_type etsipkt_validate_tclass
(
  const dsati_cmd_type *parse_table,        /* Ptr to cmd in parse table   */
  ds_umts_umts_qos_params_type * pdata_ptr  /* Ptr to Profile data */
);

LOCAL dsat_result_enum_type etsipkt_process_param
(
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  const uint32 pindex,                  /*  Parameter index             */
  void * result_ptr ,                    /*  Pointer to result           */  
  uint16 result_buff_len           /*  Length of result buffer */
);

LOCAL boolean etsipkt_map_sdu_err
(
  const boolean  to_enum,                 /* Conversion flag  */
  dsat_num_item_type *index,              /* Value list index */
  ds_umts_qos_sdu_error_enum_type *eval   /* Enum value       */
);

LOCAL boolean etsipkt_map_dlvr_err_sdu
(
  const boolean  to_enum,                 /* Conversion flag  */
  dsat_num_item_type *index,              /* Value list index */
  ds_umts_qos_sdu_dlvr_enum_type *eval    /* Enum value       */
);

LOCAL boolean etsipkt_map_res_ber
(
  const boolean  to_enum,                 /* Conversion flag  */
  dsat_num_item_type *index,              /* Value list index */
  ds_umts_qos_res_ber_enum_type *eval     /* Enum value       */
);

LOCAL boolean etsipkt_map_dorder
(
  const boolean  to_enum,                 /* Conversion flag  */
  dsat_num_item_type *index,              /* Value list index */
  ds_umts_qos_dorder_enum_type *eval      /* Enum value       */
);

LOCAL boolean etsipkt_map_tclass
(
  const boolean  to_enum,                 /* Conversion flag  */
  dsat_num_item_type *index,              /* Value list index */
  ds_umts_qos_tclass_enum_type *eval      /* Enum value       */
);

LOCAL boolean etsipkt_build_context_processing_list
(
  const tokens_struct_type *tok_ptr,     /* Command tokens from parser   */
  dsat_call_query_mode_e_type type_list, /* For which command */
  boolean * state_changed_ptr            /* Pointer to state change flag */
);

LOCAL dsm_item_type* etsipkt_check_response_buffer
(
  dsm_item_type  *res_buff_ptr,   /* Response buffer */
  uint16          buf_limit       /* Buffer contents limit */
);

LOCAL boolean etsipkt_print_ipv4_addr
(
  ds_umts_pdp_addr_type   *pdp_adr,
  uint32                  src_adr,
  boolean                 is_static_addr,
  dsm_item_type           *res_buff_ptr,
  char                    *buffer
);

LOCAL boolean etsipkt_print_ipv6_addr
(
  ds_umts_pdp_addr_type           *pdp_adr,
  ds_umts_pdp_addr_type_ipv6      *src_adr,
  boolean                         is_static_addr,
  dsm_item_type                   *res_buff_ptr,
  char                            *buffer
);

#ifdef FEATURE_SECONDARY_PDP
LOCAL boolean etsipkt_validate_cgtft_param_combination
(
  const tokens_struct_type *tok_ptr /* Parameter Values */
);

LOCAL boolean etsipkt_get_primary_profile_list
(
  byte * buf_ptr,    /* Pointer to buffer */
  byte   buf_size    /* Size of buffer */ 
);

LOCAL boolean etsipkt_validate_port_param
(
  byte   * buf_ptr,    /* Pointer to string buffer */
  byte     buf_size,   /* Size of string buffer */ 
  uint16 * from_ptr,   /* Pointer to from port numeric equivalent */
  uint16 * to_ptr      /* Pointer to to port numeric equivalent */
);

LOCAL boolean etsipkt_validate_address_mask_param
(
  byte   * buf_ptr,      /* Pointer to string buffer */
  uint16   buf_size,     /* Size of string buffer */ 
  uint32 * address_ptr,  /* Pointer to address numeric equivalent */
  uint32 * snmask_ptr    /* Pointer to mask numeric equivalent */
);

LOCAL boolean etsipkt_find_linked_profiles
(
  const byte   primary_id, /* Primary profile identifier */
  byte * size_ptr,         /* Size of output list */
  byte * list_ptr          /* List of secondary profile identifiers */
);

LOCAL boolean etsipkt_find_profile_group
(
  const byte  cid,         /* Profile identifier */
  byte * size_ptr,         /* Size of output list */
  byte * list_ptr          /* List of secondary profile identifiers */
);

LOCAL boolean etsipkt_reset_linked_context
(
  const byte   primary_id  /* Primary profile identifier */
);

LOCAL boolean etsipkt_validate_tft_epi_param
(
  const byte   cid,        /* Context profile identifier */
  const byte   pfi,        /* Packet filter index        */
  const byte   epi         /* Evaluation precedence index */
);

LOCAL byte etsipkt_cal_index_frm_pfi 
(
  const byte   cid,        /* Context profile identifier */
  const byte   pfi        /* Packet filter identifier */
);


#endif /* FEATURE_SECONDARY_PDP */

LOCAL dsat_result_enum_type etsipkt_cgpaddr_build_cid_list
(
  const tokens_struct_type *tok_ptr,  /* Command tokens from parser  */
  uint8 *cid_list,                    /* Parse results list */
  uint8 *index                        /* Length of list */
);

LOCAL dsat_result_enum_type etsipkt_cgpaddr_format_response
(
  const byte               *cid_list_ptr,
  const byte                cid_list_len,
  const tokens_struct_type *tok_ptr,
  dsm_item_type            *format_buffer
);

LOCAL dsat_result_enum_type etsipkt_cgcontrdp_format_response
(
  const byte               *cid_list_ptr,
  const byte                cid_list_len,
  const tokens_struct_type *tok_ptr,
  dsm_item_type            *format_buffer
);

LOCAL dsat_result_enum_type etsipkt_cgscontrdp_format_response
(
  const byte               *cid_list_ptr,
  const byte                cid_list_len,
  const tokens_struct_type *tok_ptr,
  dsm_item_type            *format_buffer
);

LOCAL dsat_result_enum_type etsipkt_cgeqosrdp_format_response
(
  const byte               *cid_list_ptr,
  const byte                cid_list_len,
  const tokens_struct_type *tok_ptr,
  dsm_item_type            *format_buffer
);

LOCAL dsat_result_enum_type etsipkt_cgtftrdp_format_response
(
  const byte               *cid_list_ptr,
  const byte                cid_list_len,
  const tokens_struct_type *tok_ptr,
  dsm_item_type            *res_buff_ptr
);


LOCAL dsat_cme_error_e_type etsipkt_cgdata_get_cid_info(byte *cid);

LOCAL dsat_result_enum_type etsipkt_initiate_packet_call(
                                      dsm_item_type *res_buff_ptr);


LOCAL void etsipkt_add_gprs_msg_to_queue
(
  char    *buffer,
  uint16   buf_len,
  ds_subs_e_type subs_id
);

LOCAL void etsipkt_clear_gprs_events_buffer
(
 ds_subs_e_type subs_id
);

#ifdef FEATURE_DSAT_UMTS_DATA
#ifdef FEATURE_DATA_TE_MT_PDP
LOCAL dsat_result_enum_type etsipkt_process_access_control_write
(
  dsat_pdp_access_control_e_type operation,  /*  Acess control type  */
  const tokens_struct_type *tok_ptr          /*  Command tokens from parser  */
);

LOCAL dsat_result_enum_type etsipkt_process_access_control_read
(
  dsat_pdp_access_control_e_type operation, /*  Acess control type  */
  const tokens_struct_type *tok_ptr,        /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr               /*  Place to put response       */
);
#endif /* FEATURE_DATA_TE_MT_PDP */

#endif /* FEATURE_DSAT_UMTS_DATA */

/*-------------------------------------------------------------------------
    Function Definitions:
-------------------------------------------------------------------------*/

/*===========================================================================

FUNCTION DSATETSIPKT_EXEC_CGPADDR_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It handles the +CGPADDR command.
  This command returns a list of PDP addresses for the specified
  contect identifiers

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success.

SIDE EFFECTS
  None
  
===========================================================================*/
/* ARGSUSED */
dsat_result_enum_type dsatetsipkt_exec_cgpaddr_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type  result = DSAT_OK;

  /*---------------------------------------------------------
   Processing for a command with an argument (WRITE command)
  -----------------------------------------------------------*/
  if(tok_ptr->op == (NA|EQ|AR))
  {
    if( !VALID_TOKEN(0) )
    {
      /* No CID list specified */
      result = etsipkt_cgpaddr_format_response( NULL, 0,
						tok_ptr,
						res_buff_ptr);
    }
    else
    {
      /* CID list specified and must be parsed */
      uint8 cid_list[DS_UMTS_MAX_PDP_PROFILE_NUM];
      uint8 list_len = 0;

      /* Build the CID list */
      result = etsipkt_cgpaddr_build_cid_list( tok_ptr,
                                               cid_list,
                                               &list_len );

      /* Generate response */
      if( DSAT_OK == result)
      {
        result = etsipkt_cgpaddr_format_response( cid_list, list_len,
                                                  tok_ptr,
                                                  res_buff_ptr);
      }
    }
  }
  /*-----------------------------------------------------------
   Processing for a command without an argument (WRITE command)
   and TEST syntax
  ------------------------------------------------------------*/
  else if( (tok_ptr->op == (NA)) ||
           (tok_ptr->op == (NA|EQ|QU)) )
  {
    result = etsipkt_cgpaddr_format_response( NULL, 0,
                                              tok_ptr,
                                              res_buff_ptr );
  }
  else
  {
    result = DSAT_ERROR;
  }

  return result;
} /* dsatetsipkt_exec_cgpaddr_cmd */



/*===========================================================================

FUNCTION DSATETSIPKT_EXEC_CGDCONT_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It handles the +CGDCONT command.
  This command sets basic PDP context parameters.

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatetsipkt_exec_cgdcont_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type result = DSAT_OK;
  ds_umts_pdp_addr_type ipaddr;
  dsat_num_item_type val = 0;
  dsat_error_info_s_type    err_info;
  boolean            emergency_flag = FALSE;
#ifdef FEATURE_SECONDARY_PDP
  boolean valid_flg = FALSE;
  boolean reset_linked_profiles = FALSE;
#endif /* FEATURE_SECONDARY_PDP */
  dsat_mixed_param_val_type * val_ptr = 
           (dsat_mixed_param_val_type *)dsatutil_get_val(parse_table->cmd_id,0,0,STR_TYPE);

  err_info.errval = DSAT_ERR_NO_ERROR;
  err_info.arg_num = 0;

  memset( (void *)&ipaddr, 0, sizeof(ipaddr) );

  if (tok_ptr->op == (NA|EQ|AR))
  {
    /*---------------------------------------------------------------
      Processing for a command with an argument (WRITE command)
    ---------------------------------------------------------------*/
    ds_umts_pdp_context_type pdata;
    dsat_num_item_type num_val = 0;

    /* Process connection ID */
    if (DSAT_OK != etsipkt_process_param(parse_table, tok_ptr, 0, &val,1))
    {
      err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
      err_info.arg_num = 0;
      goto send_error;
    }

#ifdef FEATURE_SECONDARY_PDP
    /* Query previously saved profile (if any) */
    memset((void*)&pdata, 0, sizeof(pdata));
    IS_BASE_CONTEXT_VALID(val,result);
    if ( result == DSAT_ERROR )
    {
      return result;
    }
    if (TRUE == valid_flg)
    {
      /* Query context profile data */
      if (DS_UMTS_PDP_SUCCESS !=
          ds_umts_get_pdp_profile_context_info_per_subs( SET_CID( val ),dsat_get_current_subs_id(),&pdata) )
      {
        DS_AT_MSG1_ERROR("Error CGDCONT PDP profile read: %d",val);
        return DSAT_ERROR;
      }
      /* Set flag to reset linked profiles on type change */
      reset_linked_profiles = (TRUE == pdata.secondary_flag)? TRUE : FALSE;
    }

    /* Test for only CID specified */
    if (1 == tok_ptr->args_found)
    {
      if (TRUE == valid_flg)
      {
        /* Ensure cid to reset is primary context */
        if (TRUE == pdata.secondary_flag)
        {
          DS_AT_MSG1_ERROR("Cannot reset SECONDARY context with CGDCONT: %d",val);
          return DSAT_ERROR;
        }
        else
        {
          /* Reset any linked secondary profiles to given cid on 
           * primary undefine.  We do this to ensure secondary
           * does not loose link to its primary, becoming orphan.  */
          if (FALSE == etsipkt_reset_linked_context((byte)val))
          {
            DS_AT_MSG1_ERROR("Error resetting linked context: %d",val);
            return DSAT_ERROR;
          }
          /* Reset specified context profile */
          RESET_BASE_CONTEXT_PARAMS(val);
          if( result == DSAT_ERROR)
          {
            return result;
          }
        }
      }
    }
#else
    /* Test for only CID specified */
    if ((0 < val) &&
        (val <= DS_UMTS_MAX_PDP_PROFILE_NUM) &&
        (1 == tok_ptr->args_found))
    {
      /* Clear the user specified +CGDCONT context parameters */
      RESET_BASE_CONTEXT_PARAMS(val);
      if( result == DSAT_ERROR)
      {
        return result;
      }
    }
#endif /* FEATURE_SECONDARY_PDP */
    else
    {
      byte temp_buf[MAX_IPADDR_STR_LEN+1];
      byte apn_buff[DS_UMTS_MAX_APN_STRING_LEN +1];
      byte cid = (byte)val;

      memset( temp_buf, 0, sizeof(temp_buf) );
      memset( apn_buff, 0, sizeof(apn_buff) );
      
      /* Verify IP address is correct; OK if null */
      if (DSAT_OK != etsipkt_process_param(parse_table, tok_ptr, 3,
                                 temp_buf,sizeof(temp_buf)))
      {
        err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
        err_info.arg_num = 3;
        goto send_error;
      }

      if ( !( ('\0' == temp_buf[0]) ||
              (DSAT_OK == dsatutil_convert_ipaddr(STRING_TO_INT,
                                                  &ipaddr,
                                                  temp_buf)) ) )
      {
        /* There is a MSG_MACRO inside function. */
        return DSAT_ERROR;
      }

      /* Verify APN is correct; OK if null */
      if( (VALID_TOKEN(2) ) && ('"' != *tok_ptr->arg[2]) )
      {
        /* APN should be entered in quotes ("") */
        err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
        err_info.arg_num = 2;
        goto send_error;
      }
      if (DSAT_OK != etsipkt_process_param(parse_table, tok_ptr, 2, 
                                apn_buff,sizeof(apn_buff)))
      {
        err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
        err_info.arg_num = 2;
        goto send_error;
      }
      if (!(('\0' == apn_buff[0]) ||
            (DSAT_OK == etsipkt_validate_apn((byte*)apn_buff))))
      {
        /* There is a MSG_MACRO inside function. */
        return DSAT_ERROR;
      }

      /* Perform normal parameter processing */
      if (DSAT_OK == dsatparm_exec_param_cmd(
                                             mode,
                                             parse_table,
                                             tok_ptr,
                                             res_buff_ptr
                                            ))
      {
        /* Transfer user data to internal storage */

#ifdef FEATURE_SECONDARY_PDP
        /* Check for linked profiles to reset */
        if (TRUE == reset_linked_profiles)
        {
          /* Reset any linked secondary profiles to given cid if profile
           * type changes from primary to secondary.  We do this to ensure
           * a secondary does not link to another secondary.  */
          if (FALSE == etsipkt_reset_linked_context(cid))
          {
            DS_AT_MSG1_HIGH("Error resetting linked context: %d",cid);
            return DSAT_ERROR;
          }
        }
#endif /* FEATURE_SECONDARY_PDP */

        /* Set specified context profile to IPV4 defaults */
        DEFAULT_BASE_CONTEXT_PARAMS(cid);
        
        /* Query default profile from EFS */
        memset((void*)&pdata, 0, sizeof(pdata));
        if (DS_UMTS_PDP_SUCCESS !=
            ds_umts_get_pdp_profile_context_info_per_subs(SET_CID( cid ), dsat_get_current_subs_id(), &pdata))
        {
          DS_AT_MSG1_HIGH("Error CGDCONT PDP profile read: %d",cid);
          return DSAT_ERROR;
        }

        /* Make conditional edits to profile */
        pdata.valid_flg = TRUE;
        pdata.pdp_context_number = cid;
        CONDITIONAL_ASSIGN(1, pdata.pdp_type, (ds_umts_pdp_type_enum_type)val_ptr[1].num_item);
        CONDITIONAL_ASSIGN(3, pdata.pdp_addr, ipaddr);
        if( VALID_TOKEN(4) )
        {
          if( ATOI_OK == dsatutil_atoi(&num_val,
              &dsat_cgdcont_dcomp_valstr[val_ptr[4].num_item][0] ,10))
          {
            pdata.d_comp = (ds_umts__pdp_data_comp_e_type)num_val;
          }
          else
          {
            return DSAT_ERROR;
          }
        }

        if( VALID_TOKEN(5) )
        {
          if( ATOI_OK == dsatutil_atoi(&num_val,
              &dsat_cgdcont_hcomp_valstr[val_ptr[5].num_item][0], 10) )
          {
            pdata.h_comp = (ds_umts_pdp_header_comp_e_type)num_val;
          }
          else
          {
            return DSAT_ERROR;
          }
        }
        
        if( VALID_TOKEN(6) )
        {
          if (DSAT_OK != etsipkt_process_param(parse_table, tok_ptr, 6, &val,1))
          {
            DS_AT_MSG0_ERROR("Cannot convert ipv4 addr alloc type parameter");
            return DSAT_ERROR;
          }
          else
          {
            pdata.ipv4_addr_alloc = (ds_umts_pdp_ipv4_addr_alloc_e_type)val;
          }
        }
        
        if( VALID_TOKEN(7) )
        {
          if (DSAT_OK != etsipkt_process_param(parse_table, tok_ptr, 7, &val,1))
          {
            DS_AT_MSG0_ERROR("Cannot convert emergency flag");
            return DSAT_ERROR;
          }
          emergency_flag = (boolean)val;
        }
        
        if (VALID_TOKEN(2))
        {
          (void)strlcpy((char *)pdata.apn, 
                            (const char *)val_ptr[2].string_item,
                            DS_UMTS_MAX_APN_STRING_LEN + 1);
         }

        /* Write to EFS */
        if (DS_UMTS_PDP_SUCCESS !=
            ds_umts_set_pdp_profile_context_info_ex_per_subs(SET_CID( cid ), dsat_get_current_subs_id(), &pdata))
        {
          DS_AT_MSG1_ERROR("Error CGDCONT PDP profile write: %d",cid);
          return DSAT_ERROR;
        }

        if( VALID_TOKEN(7) )
        {
          /* Write to EFS */
          if (DS_UMTS_PDP_SUCCESS !=
              ds_umts_set_pdp_profile_emergency_calls_are_supported_flag_per_subs(SET_CID( cid ), 
                                                                                  dsat_get_current_subs_id(),
                                                                                  emergency_flag))
          {
            DS_AT_MSG1_ERROR("Error CGDCONT Emergency Flag cid: %d",cid);
            return DSAT_ERROR;
          }
        }
      }
      else
      {
        result = DSAT_ERROR;
      }
    }
  }
  else if (tok_ptr->op == (NA|EQ|QU))
  {
    /*---------------------------------------------------------------
      Processing for a command to query valid parameters
    ---------------------------------------------------------------*/
    if (0 == dsatetsipkt_cgdcont_response_fmt (parse_table,
                                               &res_buff_ptr,
                                               MULTI_INDEX_TEST))
    {
      DS_AT_MSG0_ERROR("Problem generating query response");
      result = DSAT_ERROR;
    }
  }
  else if (tok_ptr->op == (NA|QU))
  {
    /*---------------------------------------------------------------
       Processing for a command to query current parameters
    ---------------------------------------------------------------*/
    if (0 == dsatetsipkt_cgdcont_response_fmt (parse_table,
                                               &res_buff_ptr,
                                               MULTI_INDEX_READ))
    {
      DS_AT_MSG0_ERROR("Problem generating query response");
      result = DSAT_ERROR;
    }
  }
  else if (tok_ptr->op == (NA))
  {
    /* Do nothing */
    result = DSAT_OK;
  }
  else
  {
    err_info.errval = DSAT_ERR_INVALID_SYNTAX;
    goto send_error;
  }
  return result;

send_error:
  if( err_info.errval == DSAT_ERR_INVALID_SYNTAX )
  {
    DS_AT_MSG1_ERROR("Error:%d",err_info.errval);
  }
  else
  {
    DS_AT_MSG2_ERROR("Error:%d, arg_num:%d",err_info.errval,err_info.arg_num);
  }
  return DSAT_ERROR;
} /* dsatetsipkt_exec_cgdcont_cmd */


#ifdef FEATURE_DATA_TE_MT_PDP

/*===========================================================================

FUNCTION DSATETSIPKT_GET_ACCESS_CONTROL

DESCRIPTION
 This function gives the access control status for MT PDP.

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the access control.

SIDE EFFECTS
  None

===========================================================================*/
dsat_pdp_access_control_e_type dsatetsipkt_get_access_control(void)
{
  return (dsat_pdp_access_control_e_type)dsatutil_get_val(
                                 DSAT_VENDOR_QCGANSM_IDX,0,0,NUM_TYPE);
} /* dsatetsipkt_get_access_control */


#define DSAT_ACCESS_CTRL_ADD  0
#define DSAT_ACCESS_CTRL_DELETE  1


/*===========================================================================

FUNCTION DSATETSIPKT_EXEC_QCGARL_CMD

DESCRIPTION
  This function takes the result from the command line parser and executes it.
  It handles the $QCGARL command. This command sets PDP context reject list.

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatetsipkt_exec_qcgarl_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type result = DSAT_OK;
  dsat_pdp_access_control_e_type operation = DSAT_PDP_ACCESS_CONTROL_REJECT;
  
  if (tok_ptr->op == (NA|EQ|AR))
  {
    /*  Processing WRITE command  */
    result = etsipkt_process_access_control_write( operation, tok_ptr);
  }
  else if (tok_ptr->op == (NA|QU))
  {
    /*  Processing READ command  */
    result = etsipkt_process_access_control_read( operation, tok_ptr, res_buff_ptr);
  }
  else  if (tok_ptr->op == (NA|EQ|QU))
  {
    /*  Processing TEST command  */
    res_buff_ptr->used += (word) snprintf(
                           (char *)res_buff_ptr->data_ptr, 
                           res_buff_ptr->size, 
                           "%s: (0,1)",
                           (char *) parse_table->name);
  }
  else
  {
    result = DSAT_ERROR;
  }
  return result;
} /* dsatetsipkt_exec_qcgarl_cmd */


/*===========================================================================

FUNCTION DSATETSIPKT_EXEC_QCGAPL_CMD

DESCRIPTION
  This function takes the result from the command line parser and executes it.
  It handles the $QCGAPL command. This command sets PDP context permission list.

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatetsipkt_exec_qcgapl_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type result = DSAT_OK;
  dsat_pdp_access_control_e_type operation = DSAT_PDP_ACCESS_CONTROL_PERMISSION;
  if (tok_ptr->op == (NA|EQ|AR))
  {
    /*  Processing WRITE command  */
    result = etsipkt_process_access_control_write( operation, tok_ptr);
  }
  else if (tok_ptr->op == (NA|QU))
  {
    /*  Processing READ command  */
    result = etsipkt_process_access_control_read( operation, tok_ptr, res_buff_ptr);
  }
  else  if (tok_ptr->op == (NA|EQ|QU))
  {
    /*  Processing TEST command  */
    res_buff_ptr->used += (word) snprintf(
                           (char *)res_buff_ptr->data_ptr, 
                           res_buff_ptr->size, 
                           "%s: (0,1)",
                           (char *) parse_table->name);
  }
  else
  {
    result = DSAT_ERROR;
  }
  return result;
} /* dsatetsipkt_exec_qcgapl_cmd */

/*===========================================================================
FUNCTION ETSIPKT_PRCOESS_ACCESS_CONTROL_WRITE

DESCRIPTION
  This function peforms the write operation to the access control list.

DEPENDENCIES
  None.
  
RETURN VALUE
  DSAT_OK
  DSAT_ERROR
  
SIDE EFFECTS 
  None.
  
===========================================================================*/
LOCAL dsat_result_enum_type etsipkt_process_access_control_write
(
  dsat_pdp_access_control_e_type operation,  /*  Access control type  */
  const tokens_struct_type *tok_ptr          /*  Command tokens from parser  */
)
{
  dsat_num_item_type cid = 0;
  dsat_num_item_type num_val = 0;
  ds_umts_pdp_context_type pdata;
  byte i;
  char* command;
  dsat_error_info_s_type    err_info;

  err_info.errval = DSAT_ERR_NO_ERROR;
  err_info.arg_num = 0;

  /* Set the command name */
  if (operation == DSAT_PDP_ACCESS_CONTROL_REJECT)
  {
    command="$QCGARL";
  }
  else
  {
    command="$QCGAPL";
  }
  
  if (tok_ptr->args_found > 2) 
  {
    err_info.errval = DSAT_ERR_INVALID_NUM_OF_ARGS;
    goto send_error;
  }
  else if (tok_ptr->args_found == 2) 
  {
    if(!VALID_TOKEN(1) || dsatutil_atoi(&cid,tok_ptr->arg[1],10) != ATOI_OK)
    {
      err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
      err_info.arg_num = 1;
      goto send_error;
    }
    /* Query default profile from EFS */
    memset((void*)&pdata, 0, sizeof(pdata));
    if (DS_UMTS_PDP_SUCCESS !=
          ds_umts_get_pdp_profile_context_info_per_subs( SET_CID( cid ), dsat_get_current_subs_id(), &pdata))
    {
      DS_AT_MSG_SPRINTF_2_ERROR( 
       "Error %s PDP profile read: %d",command,cid);
      return DSAT_ERROR;
    }
    /* Make conditional edits to profile */
    if(dsatutil_atoi(&num_val,tok_ptr->arg[0],10) != ATOI_OK)
    {
      err_info.errval = DSAT_ERR_ATOI_CONVERSION_FAILURE;
      err_info.arg_num = 0;
      goto send_error;
    }
    if(num_val == DSAT_ACCESS_CTRL_ADD) /* Adding*/
    {
      pdata.access_ctrl_flag = operation;
    }
    else if(num_val == DSAT_ACCESS_CTRL_DELETE)   /* Deleting */
    {
      pdata.access_ctrl_flag = DSAT_PDP_ACCESS_CONTROL_NONE;
    }
    else
    {
      err_info.errval = DSAT_ERR_PARAMETER_OUT_OF_RANGE;
      err_info.arg_num = 0;
      goto send_error;
    }
    /* Write to EFS */
    if (DS_UMTS_PDP_SUCCESS !=
          ds_umts_set_pdp_profile_context_info_ex_per_subs( SET_CID( cid ), dsat_get_current_subs_id(), &pdata))
    {
       DS_AT_MSG1_ERROR("Error PDP profile write: %d",cid);
       return DSAT_ERROR;
    }
  }
  else if ( tok_ptr->args_found == 1) 
  {
    if(!VALID_TOKEN(0) || dsatutil_atoi(&num_val,tok_ptr->arg[0],10) != ATOI_OK)
    {
      err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
      err_info.arg_num = 0;
      goto send_error;
    }
    /* Loop over each PDP context and modify the access control*/
    for (i=1; i <= DS_UMTS_MAX_PDP_PROFILE_NUM; i++) 
    {
      boolean valid_flg = FALSE;
      dsat_result_enum_type result_out = DSAT_OK;
      /* Check if valid profile */
      IS_BASE_CONTEXT_VALID(i,result_out);
      if (result_out == DSAT_ERROR)
      {
        return result_out;
      }
      if (TRUE == valid_flg)
      {
        memset((void*)&pdata, 0, sizeof(pdata));
      
        /* Query profile data */
        if (DS_UMTS_PDP_SUCCESS !=
              ds_umts_get_pdp_profile_context_info_per_subs( SET_CID( i ),dsat_get_current_subs_id(),&pdata))
        {
          DS_AT_MSG1_ERROR("Error PDP profile read: %d",i);
          return DSAT_ERROR;
        }
          
#ifdef FEATURE_SECONDARY_PDP
        /* Skip further processing if secondary profile QCGAPL, QCGARL are
           not applicable for secondary contexts*/
        if (TRUE == pdata.secondary_flag)
        {
          continue;
        }
#endif /* FEATURE_SECONDARY_PDP */
        if(num_val == DSAT_ACCESS_CTRL_ADD) /* Adding*/
        {
          pdata.access_ctrl_flag = operation;
        }
        else if(num_val == DSAT_ACCESS_CTRL_DELETE) /* Deleting */
        {
          /* Delete only those entries which were earlier marked 
             as the same operation */
          if (pdata.access_ctrl_flag == operation )
          {
            pdata.access_ctrl_flag = DSAT_PDP_ACCESS_CONTROL_NONE;
          }
        }
        else
        {
          err_info.errval = DSAT_ERR_PARAMETER_OUT_OF_RANGE;
          err_info.arg_num = 0;
          goto send_error;
        }

        /* Write to EFS */
        if (DS_UMTS_PDP_SUCCESS !=
             ds_umts_set_pdp_profile_context_info_ex_per_subs( SET_CID( i ), dsat_get_current_subs_id(), &pdata))
        {
          DS_AT_MSG1_ERROR("Error PDP profile write: %d",i);
          return DSAT_ERROR;
        }
      }
    }
  }
  else 
  {
    err_info.errval = DSAT_ERR_INVALID_NUM_OF_ARGS;
    goto send_error;
  }
  return DSAT_OK;

send_error:
  if( err_info.errval == DSAT_ERR_INVALID_NUM_OF_ARGS )
  {
    DS_AT_MSG1_ERROR("Error:%d",err_info.errval);
  }
  else
  {
    DS_AT_MSG2_ERROR("Error:%d, arg_num:%d",err_info.errval,err_info.arg_num);
  }
  return DSAT_ERROR;
} /* etsipkt_process_access_control_write */


/*===========================================================================
FUNCTION ETSIPKT_PRCOESS_ACCESS_CONTROL_READ

DESCRIPTION
  This function peforms the read operation to the access control list.

DEPENDENCIES
  None.
  
RETURN VALUE
  DSAT_OK
  DSAT_ERROR
  
SIDE EFFECTS 
  None.
  
===========================================================================*/
LOCAL dsat_result_enum_type etsipkt_process_access_control_read
(
  dsat_pdp_access_control_e_type operation,  /*  Access control type  */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  ds_umts_pdp_context_type pdata;
  byte i;
  char* command;
  
  /* Set the command name */
  if (operation == DSAT_PDP_ACCESS_CONTROL_REJECT)
  {
    command="$QCGARL";
  }
  else
  {
    command="$QCGAPL";
  }
  /* Report each profile on seperate line */
  for (i=1; i <= DS_UMTS_MAX_PDP_PROFILE_NUM; i++) 
  {
    boolean valid_flg = FALSE;
    dsat_result_enum_type result_out = DSAT_OK;
    /* Check if valid profile */
    IS_BASE_CONTEXT_VALID(i,result_out);
    if ( result_out == DSAT_ERROR )
    {
      return result_out;
    }
    if (TRUE == valid_flg)
    {
      memset((void*)&pdata, 0, sizeof(pdata));
    
      /* Query profile data */
      if (DS_UMTS_PDP_SUCCESS !=
           ds_umts_get_pdp_profile_context_info_per_subs( SET_CID( i ), dsat_get_current_subs_id(), &pdata))
      {
        DS_AT_MSG1_ERROR("Error PDP profile read: %d",i);
        return DSAT_ERROR;
      }
       
#ifdef FEATURE_SECONDARY_PDP
      /* Skip further processing if secondary profile QCGAPL, QCGARL are
         not applicable for secondary contexts*/
      if (TRUE == pdata.secondary_flag)
      {
        continue;
      }
#endif /* FEATURE_SECONDARY_PDP */
          
      if ( pdata.access_ctrl_flag == operation )
      {
        res_buff_ptr->used = (word) snprintf(
                              (char *)res_buff_ptr->data_ptr, 
                              res_buff_ptr->size,
                              "%s\n%s:%d",
                              res_buff_ptr->data_ptr,
                              command,
                              pdata.pdp_context_number);
      }
    }
  }
  return DSAT_OK;
} /* etsipkt_process_access_control_read */
#endif /* FEATURE_DATA_TE_MT_PDP*/

/*===========================================================================

FUNCTION DSATETSIPKT_CGDCONT_RESPONSE_FMT

DESCRIPTION
  This function generates the response for the +CGDCONT read & test
  commands.  Data is extracted from data structure and formatted according
  to parameter flag.

DEPENDENCIES
  None

RETURN VALUE
  Returns number of characters in response buffer.

SIDE EFFECTS
  None

===========================================================================*/
dsat_num_item_type dsatetsipkt_cgdcont_response_fmt
(
  const dsati_cmd_type  *parse_table,   /*  Ptr to cmd in parse table   */
  dsm_item_type * const *out_buff_ptr,  /*  Place to put response       */
  multi_index_fmt_e_type format         /*  Flag for format type        */
)
{
  byte name_buff[16];
  byte ipaddr_str[MAX_IPADDR_STR_LEN+1];
  byte i, cntr = 0;
  int size = 0;
  int result = 0;
  dsm_item_type * res_buff_ptr = *out_buff_ptr;
  ds_umts_pdp_context_type pdata;
  boolean                  emergency_flag = FALSE;

  /* Format the command name */
  (void)snprintf((char*)name_buff,
                       sizeof(name_buff),
                       "%s: ",
                       parse_table->name);
  res_buff_ptr->data_ptr[res_buff_ptr->used] = '\0';
  ipaddr_str[0] = '\0';

  switch (format)
  {
    case MULTI_INDEX_TEST:
      /* Report each PDP type on seperate line */
      for (i=0; i <= (byte)DSAT_MAX_PDP_TYPE; i++) 
      {
        size = snprintf ((char *)res_buff_ptr->data_ptr,
                        res_buff_ptr->size,
                        "%s%s%s(%d-%d),\"%s\",%s,%s,%s,%s,,(%d-%d)",
                        res_buff_ptr->data_ptr,
                        (i > 0) ? "\n" : "",
                        name_buff,
                        ((dflm_type *)dsat_pdp_cid_dflm)->lower,
                        ((dflm_type *)dsat_pdp_cid_dflm)->upper,
                        dsat_pdptype_valstr[i],
                        "",
                        "",
                        dsat_cgdcont_dcomp_tststr,
                        dsat_cgdcont_hcomp_tststr,
                        ((dflm_type *)dsat_emergency_dflm)->lower,
                        ((dflm_type *)dsat_emergency_dflm)->upper
                       );
      }
      res_buff_ptr->used = (uint16)size;
      result = size;
      break;

    case MULTI_INDEX_READ:
      /* Report each profile on seperate line */
      for (i=1; i <= DS_UMTS_MAX_PDP_PROFILE_NUM; i++) 
      {
        boolean valid_flg = FALSE;
        dsat_result_enum_type result_out = DSAT_OK;
        /* Ensure the +CGDCONT connection is defined */
        IS_BASE_CONTEXT_VALID(i,result_out);
        if ( result_out == DSAT_ERROR )
        {
          return 0;
        }
        if (TRUE == valid_flg)
        {
          memset((void*)&pdata, 0, sizeof(pdata));
          memset((void*)ipaddr_str, 0, sizeof(ipaddr_str));
        
          /* Query profile data */
          if (DS_UMTS_PDP_SUCCESS !=
              ds_umts_get_pdp_profile_context_info_per_subs(
                SET_CID( i ), dsat_get_current_subs_id(), &pdata))
          {
            DS_AT_MSG1_HIGH("Error CGDCONT PDP profile read: %d",i);
            return 0;
          }
          /* Query emergency flag */
          if (DS_UMTS_PDP_SUCCESS !=
              ds_umts_get_pdp_profile_emergency_calls_are_supported_flag_per_subs(SET_CID( i ),
                                                                                  dsat_get_current_subs_id(),
                                                                                  &emergency_flag))
          {
            DS_AT_MSG1_ERROR("Error CGDCONT Emergency Flag cid: %d",i);
            return 0;
          }

#ifdef FEATURE_SECONDARY_PDP
          /* Skip further processing if secondary profile */
          if (TRUE == pdata.secondary_flag)
          {
            continue;
          }
#endif /* FEATURE_SECONDARY_PDP */
          
          /* Decode PDP IP address if set */
          if ( DSAT_OK != dsatutil_convert_ipaddr(INT_TO_STRING,
                                                  &(pdata.pdp_addr),
                                                  ipaddr_str) )
          {
            return 0;
          }

          /* Check to see if a new DSM item is required */
          CHECK_APPEND_RESPONSE_BUFFER ((DSM_BUFFER_THRESHOLD - \
                                         DS_UMTS_MAX_APN_STRING_LEN) );
                                        
          res_buff_ptr->used = (uint16)snprintf(
                    (char *)res_buff_ptr->data_ptr, 
                    res_buff_ptr->size,
                    "%s%s%s%d,\"%s\",\"%s\",\"%s\",%d,%d,,%d",
                    res_buff_ptr->data_ptr,
                    (cntr > 0) ? "\n" : "",
                    name_buff,
                    pdata.pdp_context_number,
                    dsat_pdptype_valstr[pdata.pdp_type],
                    pdata.apn,
                    ipaddr_str,
                    pdata.d_comp,
                    pdata.h_comp,
                    emergency_flag
                   );

          cntr++;
        }
      }
      
      /* No contexts defined, just print command name */
      if (0 == cntr)
      {
        res_buff_ptr->used = (uint16)snprintf(
                                         (char *)res_buff_ptr->data_ptr,
                                         res_buff_ptr->size, "%s", name_buff);
      }
      result = res_buff_ptr->used;
      break;

    case MULTI_INDEX_AMPV:
      size = res_buff_ptr->used;
      res_buff_ptr->used = (word)snprintf(
                                   (char *)res_buff_ptr->data_ptr,
                                   res_buff_ptr->size,
                                   "%s%s",
                                   res_buff_ptr->data_ptr,
                                   name_buff);

      /* Report each profile in set notation */
      for (i=1; i <= DS_UMTS_MAX_PDP_PROFILE_NUM; i++) 
      {
        boolean valid_flg = FALSE;
        
        /* Ensure the +CGDCONT connection is defined */
        if (DS_UMTS_PDP_SUCCESS !=
            ds_umts_get_pdp_profile_context_info_is_valid_per_subs(
              SET_CID( i ), dsat_get_current_subs_id(),&valid_flg))
        {
          DS_AT_MSG1_HIGH("Error on PDP profile valid query: %d",i);
          result = (res_buff_ptr->used - size);
          res_buff_ptr->used -= (word)result;  /* caller increments used */
          return (dsat_num_item_type)result;
        }

        /* Contine processing if profile data is valid */      
        if (TRUE == valid_flg)
        {
          memset((void*)&pdata, 0, sizeof(pdata));
          memset((void*)ipaddr_str, 0, sizeof(ipaddr_str));
        
          /* Query profile data */
          if (DS_UMTS_PDP_SUCCESS !=
              ds_umts_get_pdp_profile_context_info_per_subs(
                SET_CID( i ), dsat_get_current_subs_id(), &pdata))
          {
            DS_AT_MSG1_HIGH("Error CGDCONT PDP profile read: %d",i);
            result = (res_buff_ptr->used - size);
            res_buff_ptr->used -= (word)result;  /* caller increments used */
            return (dsat_num_item_type)result;
          }
          
#ifdef FEATURE_SECONDARY_PDP
          /* Skip further processing if secondary profile */
          if (TRUE == pdata.secondary_flag)
          {
            continue;
          }
#endif /* FEATURE_SECONDARY_PDP */
          
          /* Decode PDP IP address if set */
          if ( DSAT_OK != dsatutil_convert_ipaddr( 
                                        INT_TO_STRING,
                                        &(pdata.pdp_addr),
                                        ipaddr_str) )
          {
            return 0;
          }

          /* Check to see if a new DSM item is required */
          CHECK_APPEND_RESPONSE_BUFFER ((DSM_BUFFER_THRESHOLD - \
                                          DS_UMTS_MAX_APN_STRING_LEN) );
                                        
          /* Generate response from internal storage */
          res_buff_ptr->used = (word)snprintf (
                    (char *)res_buff_ptr->data_ptr, 
                    res_buff_ptr->size,
                    "%s%s(%d,\"%s\",\"%s\",\"%s\",%d,%d)",
                    res_buff_ptr->data_ptr,
                    (cntr > 0) ? "," : "",
                    pdata.pdp_context_number,
                    dsat_pdptype_valstr[pdata.pdp_type],
                    pdata.apn,
                    ipaddr_str,
                    pdata.d_comp,
                    pdata.h_comp
                   );
          cntr++;
        }
      }

      /* Return zero if DSM item changed */
      result = (res_buff_ptr == *out_buff_ptr)?
               (res_buff_ptr->used - size) : 0;
      (*out_buff_ptr)->used -= (word)result;  /* caller increments used */
      break;
      
    default:
      DS_AT_MSG0_HIGH("Unsupported multi-index format type");
  }
  return (dsat_num_item_type)result;
}  /* dsatetsipkt_cgdcont_response_fmt */

#ifdef FEATURE_SECONDARY_PDP

/*===========================================================================

FUNCTION DSATETSIPKT_EXEC_CGDSCONT_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It handles the +CGDSCONT command.
  This command sets PDP secondary context parameters.

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatetsipkt_exec_cgdscont_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type result = DSAT_OK;
  dsat_num_item_type val = 0;
  dsat_mixed_param_val_type * val_ptr = 
    (dsat_mixed_param_val_type *) dsatutil_get_val(parse_table->cmd_id,0,0,STR_TYPE);

  dsat_error_info_s_type    err_info;

  err_info.errval = DSAT_ERR_NO_ERROR;
  err_info.arg_num = 0;

  if (tok_ptr->op == (NA|EQ|AR))
  {
    /*---------------------------------------------------------------
      Processing for a command with an argument (WRITE command)
    ---------------------------------------------------------------*/
    ds_umts_pdp_context_type pdata;
    boolean valid_flg = FALSE;
    boolean reset_linked_profiles = FALSE;

    /* Process connection ID */
    if (DSAT_OK != etsipkt_process_param(parse_table, tok_ptr, 0, &val,1))
    {
      err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
      err_info.arg_num = 0;
      goto send_error;
    }

    /* Query context profile data for cid */
    memset((void*)&pdata, 0, sizeof(pdata));
    IS_BASE_CONTEXT_VALID(val,result);
    if( result == DSAT_ERROR )
    {
      return result;
    }
    if (TRUE == valid_flg)
    {
      if (DS_UMTS_PDP_SUCCESS !=
          ds_umts_get_pdp_profile_context_info_per_subs(
            SET_CID( val ), dsat_get_current_subs_id(), &pdata))
      {
        DS_AT_MSG1_HIGH("Error CGDSCONT PDP profile read: %d",val);
        return DSAT_ERROR;
      }
    }
       
    /* Test for only CID specified */
    if (1 == tok_ptr->args_found)
    {
      /* Ensure cid to reset is secondary context */
      if (FALSE == pdata.secondary_flag)
      {
        DS_AT_MSG1_HIGH("Cannot reset PRIMARY context with CGDSCONT: %d",val);
        return DSAT_ERROR;
      }
      
      /* Clear the user specified +CGDSCONT context parameters */
      RESET_BASE_CONTEXT_PARAMS(val);
      if( result == DSAT_ERROR)
      {
        return result;
      }
    }
    else
    {
      byte cid = (byte)val;
      dsat_num_item_type num_val = 0;

      /* Check for overwriting a primary profile. */
      /* We will reset any linked secondaries later. */
      reset_linked_profiles = (FALSE == pdata.secondary_flag)? TRUE : FALSE;

      /* Verify primary context is valid */
      if (VALID_TOKEN(1))
      {
        /* Process primary connection ID */
        if (DSAT_OK != etsipkt_process_param(parse_table, tok_ptr, 1, &val,1))
        {
          err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
          err_info.arg_num = 1;
          goto send_error;
        }
        
        /* Query PRIMARY context profile data */
        memset((void*)&pdata, 0, sizeof(pdata));
        IS_BASE_CONTEXT_VALID(val,result);
        if( result == DSAT_ERROR )
        {
          return result;
        }
        if (TRUE == valid_flg)
        {
          if (DS_UMTS_PDP_SUCCESS !=
              ds_umts_get_pdp_profile_context_info_per_subs(
                SET_CID( val ), dsat_get_current_subs_id(), &pdata))
          {
            DS_AT_MSG1_HIGH("Error CGDSCONT PDP profile read: %d",val);
            return DSAT_ERROR;
          }
        }
        
        /* Ensure primary context defined */
        if (! ((TRUE == pdata.valid_flg) &&
               (FALSE == pdata.secondary_flag)))
        {
          DS_AT_MSG1_HIGH("Primary context ID is undefined: %d",val);
          return DSAT_ERROR;
        }

        /* Prohibit circular references */
        if (cid == val)
        {
          DS_AT_MSG1_HIGH("Primary and Secondary CID same: %d",cid);
          return DSAT_ERROR;
        }
      }

      /* Perform normal parameter processing */
      if (DSAT_OK == dsatparm_exec_param_cmd(
                                             mode,
                                             parse_table,
                                             tok_ptr,
                                             res_buff_ptr
                                            ))
      {
        /* Reset linked profiles if detectd earlier */
        if (TRUE == reset_linked_profiles)
        {
          /* Reset any linked secondary profiles to given cid if profile
           * type changes from primary to secondary.  We do this to ensure
           * a secondary does not link to another secondary.  */
          if (FALSE == etsipkt_reset_linked_context(cid))
          {
            DS_AT_MSG1_HIGH("Error resetting linked context: %d",cid);
            return DSAT_ERROR;
          }
        }

        /* Transfer user data to internal storage */
        
        /* Set specified profile to IPV4 defaults */
        /* Note: we are intentionally permitting overwrite of either a */
        /*       primary or secondary profile here.                   */
        DEFAULT_BASE_CONTEXT_PARAMS(cid);
        
        /* Query default profile from EFS */
        memset((void*)&pdata, 0, sizeof(pdata));
        if (DS_UMTS_PDP_SUCCESS !=
            ds_umts_get_pdp_profile_context_info_per_subs(
              SET_CID( cid ), dsat_get_current_subs_id(), &pdata))
        {
          DS_AT_MSG1_HIGH("Error CGDSCONT PDP profile read: %d",cid);
          return DSAT_ERROR;
        }

        /* Make conditional edits to profile */
        pdata.valid_flg = TRUE;
        pdata.pdp_context_number = cid;
        pdata.secondary_flag = TRUE;
        CONDITIONAL_ASSIGN(1, pdata.primary_id,
                              (byte)val_ptr[1].num_item);
        if( VALID_TOKEN(2) )
        {
          if( ATOI_OK == dsatutil_atoi(&num_val, 
            &dsat_cgdcont_dcomp_valstr[val_ptr[2].num_item][0], 10) )
          {
            pdata.d_comp = (ds_umts__pdp_data_comp_e_type)num_val;
          }
          else
          {
            return DSAT_ERROR;
          }
        }
        if( VALID_TOKEN(3) )
        {
          if( ATOI_OK == dsatutil_atoi(&num_val, 
              &dsat_cgdcont_hcomp_valstr[val_ptr[3].num_item][0], 10))
          { 
            pdata.h_comp =(ds_umts_pdp_header_comp_e_type)num_val;
          }
          else
          {
            return DSAT_ERROR;
          }
        }
        /* Write to EFS */
        if (DS_UMTS_PDP_SUCCESS !=
            ds_umts_set_pdp_profile_context_info_ex_per_subs(
              SET_CID( cid ), dsat_get_current_subs_id(), &pdata))
        {
          DS_AT_MSG1_HIGH("Error CGDSCONT PDP profile write: %d",cid);
            return DSAT_ERROR;
        }
      }
      else
      {
        result = DSAT_ERROR;
      }
    }
  }
  else if (tok_ptr->op == (NA|EQ|QU))
  {
    /*---------------------------------------------------------------
      Processing for a command to query valid parameters
    ---------------------------------------------------------------*/
    if (0 == dsatetsipkt_cgdscont_response_fmt (parse_table,
                                                &res_buff_ptr,
                                                MULTI_INDEX_TEST))
    {
      DS_AT_MSG0_HIGH("Problem generating query response");
      result = DSAT_ERROR;
    }
  }
  else if (tok_ptr->op == (NA|QU))
  {
    /*---------------------------------------------------------------
       Processing for a command to query current parameters
    ---------------------------------------------------------------*/
    if (0 == dsatetsipkt_cgdscont_response_fmt (parse_table,
                                                &res_buff_ptr,
                                                MULTI_INDEX_READ))
    {
      DS_AT_MSG0_HIGH("Problem generating query response");
      result = DSAT_ERROR;
    }
  }
  else if (tok_ptr->op == (NA))
  {
    /* Do nothing */
    result = DSAT_OK;
  }
  else
  {
    result = DSAT_ERROR;
  }
  return result;

send_error:
  DS_AT_MSG2_ERROR("Error:%d, arg_num:%d",err_info.errval,err_info.arg_num);
  return DSAT_ERROR;

} /* dsatetsipkt_exec_cgdcont_cmd */



/*===========================================================================

FUNCTION DSATETSIPKT_CGDSCONT_RESPONSE_FMT

DESCRIPTION
  This function generates the response for the +CGDSCONT read & test
  commands.  Data is extracted from data structure and formatted according
  to parameter flag.

DEPENDENCIES
  None

RETURN VALUE
  Returns number of characters in response buffer.

SIDE EFFECTS
  None

===========================================================================*/
dsat_num_item_type dsatetsipkt_cgdscont_response_fmt
(
  const dsati_cmd_type  *parse_table,   /*  Ptr to cmd in parse table   */
  dsm_item_type * const *out_buff_ptr,  /*  Place to put response       */
  multi_index_fmt_e_type format         /*  Flag for format type        */
)
{
  #define MAX_BUFFER_SIZE (3*DS_UMTS_MAX_PDP_PROFILE_NUM) 
  /* MAX size should be thrice the size of DS_UMTS_MAX_PDP_PROFILE_NUM 
   to store profile number (in ascii) followed by a coma. It requires 
   3 bytes for profile numbers greater than or equal to 10.*/
  byte temp_buf[MAX_BUFFER_SIZE];
  byte name_buff[MAX_BUFFER_SIZE];
  byte i, cntr = 0;
  int size = 0;
  int result = 0;
  dsm_item_type * res_buff_ptr = *out_buff_ptr;
  ds_umts_pdp_context_type pdata;

  /* Format the command name */
  (void)snprintf((char*)name_buff, 
                        sizeof(name_buff),
                        "%s: ", parse_table->name);
  res_buff_ptr->data_ptr[res_buff_ptr->used] = '\0';

  switch (format)
  {
    case MULTI_INDEX_TEST:
      /* Get list of defines primary profiles */
      if (FALSE ==
          etsipkt_get_primary_profile_list (temp_buf, sizeof(temp_buf)))
      {
        DS_AT_MSG0_HIGH("Error on get PDP primary profile list");
        return 0;
      }
      
      /* Report each PDP type on seperate line */
      for (i=0; i <= (byte)DSAT_MAX_PDP_TYPE; i++) 
      {
        size = snprintf ((char *)res_buff_ptr->data_ptr,
                        res_buff_ptr->size,
                        "%s%s%s(%d-%d),(%s),\"%s\",%s,%s,%s,%s",
                        res_buff_ptr->data_ptr,
                        (i > 0) ? "\n" : "",
                        name_buff,
                        ((dflm_type *)dsat_pdp_cid_dflm)->lower,
                        ((dflm_type *)dsat_pdp_cid_dflm)->upper,
                        temp_buf,
                        dsat_pdptype_valstr[i],
                        "",
                        "",
                        dsat_cgdcont_dcomp_tststr,
                        dsat_cgdcont_hcomp_tststr
                       );
      }
      res_buff_ptr->used = (uint16)size;
      result = size;
      break;

    case MULTI_INDEX_READ:
      /* Report each profile on seperate line */
      for (i=1; i <= DS_UMTS_MAX_PDP_PROFILE_NUM; i++) 
      {
        boolean valid_flg = FALSE;
        dsat_result_enum_type result_out = DSAT_OK;
        
        /* Ensure the +CGDSCONT connection is defined */
        IS_BASE_CONTEXT_VALID(i,result_out);
        if( result_out == DSAT_ERROR )
        {
          return 0;
        }
        if (TRUE == valid_flg)
        {
          memset((void*)&pdata, 0, sizeof(pdata));
        
          /* Query profile data */
          if (DS_UMTS_PDP_SUCCESS !=
              ds_umts_get_pdp_profile_context_info_per_subs(
                SET_CID( i ),dsat_get_current_subs_id(), &pdata))
          {
            DS_AT_MSG1_HIGH("Error CGDCONT PDP profile read: %d",i);
            return 0;
          }

          /* Skip further processing if primary profile */
          if (FALSE == pdata.secondary_flag)
          {
            continue;
          }
          
          size = snprintf ((char *)res_buff_ptr->data_ptr, 
                    res_buff_ptr->size,
                    "%s%s%s%d,%d,%d,%d",
                    res_buff_ptr->data_ptr,
                    (cntr > 0) ? "\n" : "",
                    name_buff,
                    pdata.pdp_context_number,
                    pdata.primary_id,
                    pdata.d_comp,
                    pdata.h_comp
                   );
          cntr++;
        }
      }
      
      /* No contexts defined, just print command name */
      if (0 == cntr)
      {
        size = snprintf((char *)res_buff_ptr->data_ptr,
                              res_buff_ptr->size, "%s", name_buff);
      }
      res_buff_ptr->used = (uint16)size;
      result = size;
      break;

    case MULTI_INDEX_AMPV:
      size = res_buff_ptr->used;
      res_buff_ptr->used = (word)snprintf(
                                   (char *)res_buff_ptr->data_ptr,
                                   res_buff_ptr->size,
                                   "%s%s",
                                   res_buff_ptr->data_ptr,
                                   name_buff);

      /* Report each profile in set notation */
      for (i=1; i <= DS_UMTS_MAX_PDP_PROFILE_NUM; i++) 
      {
        boolean valid_flg = FALSE;
        
        /* Ensure the +CGDCONT connection is defined */
        if (DS_UMTS_PDP_SUCCESS !=
            ds_umts_get_pdp_profile_context_info_is_valid_per_subs(
              SET_CID( i ), dsat_get_current_subs_id(), &valid_flg))
        {
          DS_AT_MSG1_HIGH("Error on PDP profile valid query: %d",i);
          result = (res_buff_ptr->used - size);
          res_buff_ptr->used -= (word)result;  /* caller increments used */
          return (dsat_num_item_type)result;
        }
          
        /* Contine processing if profile data is valid */      
        if (TRUE == valid_flg)
        {
          memset((void*)&pdata, 0, sizeof(pdata));
        
          /* Query profile data */
          if (DS_UMTS_PDP_SUCCESS !=
              ds_umts_get_pdp_profile_context_info_per_subs(
                SET_CID( i ), dsat_get_current_subs_id(), &pdata))
          {
            DS_AT_MSG1_HIGH("Error CGDSCONT PDP profile read: %d",i);
            result = (res_buff_ptr->used - size);
            res_buff_ptr->used -= (word)result;  /* caller increments used */
            return (dsat_num_item_type)result;
          }

          /* Skip further processing if primary profile */
          if (FALSE == pdata.secondary_flag)
          {
            continue;
          }

          /* Check to see if a new DSM item is required */
          CHECK_APPEND_RESPONSE_BUFFER (0);
          
          /* Generate response from internal storage */
          res_buff_ptr->used = (word)snprintf (
                    (char *)res_buff_ptr->data_ptr, 
                    res_buff_ptr->size,
                    "%s%s(%d,%d,%d,%d)",
                    res_buff_ptr->data_ptr,
                    (cntr > 0) ? "," : "",
                    pdata.pdp_context_number,
                    pdata.primary_id,
                    pdata.d_comp,
                    pdata.h_comp
                   );
          cntr++;
        }
      }

      /* Return zero if DSM item changed */
      result = (res_buff_ptr == *out_buff_ptr)?
               (res_buff_ptr->used - size) : 0;
      (*out_buff_ptr)->used -= (word)result;  /* caller increments used */
      break;
      
    default:
      DS_AT_MSG0_HIGH("Unsupported multi-index format type");
  }
  return (dsat_num_item_type)result;
}  /* dsatetsipkt_cgdcont_response_fmt */



/*===========================================================================

FUNCTION DSATETSIPKT_EXEC_CGTFT_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It handles the +CGTFT command.
  This command sets PDP Traffic Flow Template parameters.

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
/* ARGSUSED */
dsat_result_enum_type dsatetsipkt_exec_cgtft_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type result = DSAT_OK;
  dsat_num_item_type val = 0;
  dsat_error_info_s_type    err_info;

  err_info.errval = DSAT_ERR_NO_ERROR;
  err_info.arg_num = 0;

  if (tok_ptr->op == (NA|EQ|AR))
  {
    /*---------------------------------------------------------------
      Processing for a command with an argument (WRITE command)
    ---------------------------------------------------------------*/

    /* Process connection ID */
    if (DSAT_OK !=
        etsipkt_process_param(parse_table, tok_ptr, (uint32)CGTFT_CID, &val,1))
    {
      
      err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
      err_info.arg_num = (uint8) CGTFT_CID;
      goto send_error;
    }

    /* Test for only CID specified */
    if (1 == tok_ptr->args_found)
    {
      /* Clear the user specified +CGTFT context parameters */
      if (DS_UMTS_PDP_SUCCESS !=
          ds_umts_reset_pdp_profile_tft_all_info_per_subs (
            SET_CID( val ),dsat_get_current_subs_id() ))
      {
        DS_AT_MSG1_HIGH("Error on CGTFT reset: %d",val);
        return DSAT_ERROR;
      }
    }
    else
    {
      #define PFI_INVALID_INDEX 3
      byte cid = (byte)val;
      byte temp_buf[MAX_ADDR_SUBNET_STRING_LEN+1];
      uint32 temp_val=0;
      ds_umts_tft_params_type pdata;
      byte filter_index = PFI_INVALID_INDEX;
      memset ((void*)&pdata, 0, sizeof(ds_umts_tft_params_type));

      /* Given the command has no default paramater values, all validation */
      /* must be done explictly to avoid previous value handling in normal */
      /* parameter processing.  Need to detect skipped parameters to       */
      /* determine valid combinations.                                     */
      
      /* Validate packet filter identifier.                                */
      /* Note the PFI will be unique as we use it for storage position,    */
      /* overwriting any of same identifier.                               */
      if ( VALID_TOKEN((uint32)CGTFT_PFI) )
      {
        if (DSAT_OK != etsipkt_process_param(parse_table, tok_ptr, (uint32)CGTFT_PFI,
                                             (byte*)&temp_val,sizeof(temp_val)))
        {
          err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
          err_info.arg_num = (uint8) CGTFT_PFI;
          goto send_error;
        }
        /*
         * Check if this packet Filter ID exist then overwrite it
         * If both filter ID filled and New Filter ID doesn't match to set 
         * Filter ids then return ERROR.
        */
        filter_index =  etsipkt_cal_index_frm_pfi (cid,(byte)temp_val);
        DS_AT_MSG1_HIGH(" Filter index %d ",filter_index);
        if (PFI_INVALID_INDEX == filter_index)
        {
          DS_AT_MSG0_ERROR(" MAXIMUM two filter can set ");
          return DSAT_ERROR;
        }
        pdata.filter_id = (byte)temp_val;
      }

      /* Validate evaluation precedence index (EPI) */
      if ( VALID_TOKEN((uint32)CGTFT_EPI) )
      {
        if (DSAT_OK != etsipkt_process_param(parse_table, tok_ptr, (uint32)CGTFT_EPI,
                                             (byte*)&temp_val,sizeof(temp_val)))
        {
          err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
          err_info.arg_num = (uint8) CGTFT_EPI;
          goto send_error;
        }
        else
        {
          /* Search for duplicate EPI in related TFTs */
          if (TRUE == etsipkt_validate_tft_epi_param (cid, filter_index,
                                                      (byte)temp_val))
          {
            pdata.eval_prec_id = (byte)temp_val; 
          }
          else
          {
            DS_AT_MSG1_HIGH("EPI is not unique for all profiles: %d",temp_val);
            dsatme_set_cme_error (DSAT_CME_TFT_EPI_DUPLICATE, res_buff_ptr);
            return DSAT_CMD_ERR_RSP;
          }
        }
      }

      /* Ensure both PFI and EPI are provided */
      if ( !(VALID_TOKEN((uint32)CGTFT_PFI) && VALID_TOKEN((uint32)CGTFT_EPI)) )
      {
        DS_AT_MSG2_HIGH("Need both PFI and EPI parameters: PFI=%d, EPI=%d",
                  pdata.filter_id,pdata.eval_prec_id);
        return DSAT_ERROR;
      }
      
      /* Validate source address and subnet mask */
      if ( VALID_TOKEN((uint32)CGTFT_SA_SM) )
      {
        temp_buf[0] = '\0';
        if (DSAT_OK != etsipkt_process_param(parse_table, tok_ptr, (uint32)CGTFT_SA_SM,
                                             temp_buf,sizeof(temp_buf)))
        {
          err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
          err_info.arg_num = (uint8) CGTFT_SA_SM;
          goto send_error;
        }
        else
        {
          if ( DSAT_OK != dsatutil_convert_addr_mask( STRING_TO_INT,
                                                      &pdata.src_addr_mask,
                                                      temp_buf ) )
          {
            DS_AT_MSG0_HIGH("Error validating Address Mask parameter");
            return DSAT_ERROR;
          }
        }
      }

      /* Validate protocol number / next header */
      if ( VALID_TOKEN((uint32)CGTFT_PN_NH) )
      {
        if (DSAT_OK != etsipkt_process_param(parse_table, tok_ptr, (uint32)CGTFT_PN_NH,
                                             (byte*)&temp_val,sizeof(temp_val)))
        {
          err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
          err_info.arg_num = (uint8) CGTFT_PN_NH;
          goto send_error;
        }
        pdata.prot_num = (byte)temp_val;
      }
      
      /* Validate destination port range */
      if ( VALID_TOKEN((uint32)CGTFT_DSTP) )
      {
        temp_buf[0] = '\0';
        if (DSAT_OK != etsipkt_process_param(parse_table, tok_ptr, (uint32)CGTFT_DSTP,
                                             temp_buf,sizeof(temp_buf)))
        {
          err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
          err_info.arg_num = (uint8) CGTFT_DSTP;
          goto send_error;
        }
        else
        {
          uint16 from,to;

          /* Convert & validate string tuple */
          if (FALSE ==
              etsipkt_validate_port_param(temp_buf,
                                          sizeof(temp_buf),
                                          (uint16*)&from,
                                          (uint16*)&to))
          {
            err_info.errval = DSAT_ERR_PARAMETER_OUT_OF_RANGE;
            err_info.arg_num = (uint8) CGTFT_DSTP;
            goto send_error;
          }
          pdata.dest_port_range.from = from;
          pdata.dest_port_range.to = to;
        }
      }
      
      /* Validate source port range */
      if ( VALID_TOKEN((uint32)CGTFT_SRCP) )
      {
        temp_buf[0] = '\0';
        if (DSAT_OK != etsipkt_process_param(parse_table, tok_ptr, (uint32)CGTFT_SRCP,
                                             temp_buf,sizeof(temp_buf))) 
        {
          err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
          err_info.arg_num = (uint8) CGTFT_SRCP;
          goto send_error;
        }
        else
        {
          uint16 from,to;

          /* Convert & validate string tuple */
          if (FALSE ==
              etsipkt_validate_port_param(temp_buf,
                                          sizeof(temp_buf),
                                          (uint16*)&from,
                                          (uint16*)&to))
          {
            err_info.errval = DSAT_ERR_PARAMETER_OUT_OF_RANGE;
            err_info.arg_num = (uint8) CGTFT_SRCP;
            goto send_error;
          }
          pdata.src_port_range.from = from;
          pdata.src_port_range.to = to;
        }
      }

      /* Validate security parameter index*/
      if ( VALID_TOKEN((uint32)CGTFT_SPI) )
      {
        if (DSAT_OK != etsipkt_process_param(parse_table, tok_ptr, (uint32)CGTFT_SPI,
                                             (uint32*)&temp_val,sizeof(temp_val)))
        {
          err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
          err_info.arg_num = (uint8) CGTFT_SPI;
          goto send_error;
        }
        pdata.ipsec_spi = temp_val;
      }

      /* Validate TOS / traffic class and mask */
      if ( VALID_TOKEN((uint32)CGTFT_TOS) )
      {
        temp_buf[0] = '\0';
        if (DSAT_OK != etsipkt_process_param(parse_table, tok_ptr, (uint32)CGTFT_TOS,
                                             temp_buf,sizeof(temp_buf)))
        {
          err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
          err_info.arg_num = (uint8) CGTFT_TOS;
          goto send_error;
        }
        else
        {
          /* Convert & validate string tuple */
          uint32 tmask;
          if (DSAT_OK != dsatutil_convert_tuple (STRING_TO_INT,
                                                 TUPLE_TYPE_TMASK,
                                                 &tmask,
                                                 temp_buf,
                                                 sizeof(temp_buf),
                                                 DSAT_CONVERT_IP) )
          {
            err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
            err_info.arg_num = (uint8) CGTFT_TOS;
            goto send_error;
          }
          pdata.tos_mask = (uint16)tmask;
        }
      }

      /* Validate flow label */
      if ( VALID_TOKEN((uint32)CGTFT_FLOW) )
      {
        if (DSAT_OK != etsipkt_process_param(parse_table, tok_ptr, (uint32)CGTFT_FLOW,
                                             (uint32*)&temp_val,sizeof(temp_val)))
        {
          DS_AT_MSG0_HIGH("Cannot process FLOW parameter");
          return DSAT_ERROR;
        }
        pdata.flow_label = temp_val;
      }
      
      /* Validate combination of parameters */
      if (!etsipkt_validate_cgtft_param_combination(tok_ptr) )
      {
        DS_AT_MSG0_HIGH("Paramater combination invalid");
        dsatme_set_cme_error (DSAT_CME_INVALID_PARAM_COMB, res_buff_ptr);
        return DSAT_CMD_ERR_RSP;
      }
      else
      {
        /* Write to EFS */
        pdata.valid_flg = TRUE;
        if (DS_UMTS_PDP_SUCCESS !=
            ds_umts_set_pdp_profile_tft_info_per_subs(
              SET_CID( cid ), dsat_get_current_subs_id(), filter_index, &pdata))
        {
          DS_AT_MSG1_HIGH("Error CGTFT PDP profile write: %d",cid);
          return DSAT_ERROR;
        }
      }
    }
  }
  else if (tok_ptr->op == (NA|EQ|QU))
  {
    /*---------------------------------------------------------------
      Processing for a command to query valid parameters
    ---------------------------------------------------------------*/
    if (0 == dsatetsipkt_cgtft_response_fmt (parse_table,
                                             &res_buff_ptr,
                                             MULTI_INDEX_TEST))
    {
      DS_AT_MSG0_HIGH("Problem generating query response");
      result = DSAT_ERROR;
    }
  }
  else if (tok_ptr->op == (NA|QU))
  {
    /*---------------------------------------------------------------
       Processing for a command to query current parameters
    ---------------------------------------------------------------*/
    if (0 == dsatetsipkt_cgtft_response_fmt (parse_table,
                                             &res_buff_ptr,
                                             MULTI_INDEX_READ))
    {
      DS_AT_MSG0_HIGH("Problem generating query response");
      result = DSAT_ERROR;
    }
  }
  else if (tok_ptr->op == (NA))
  {
    /* Do nothing */
    result = DSAT_OK;
  }
  else
  {
    err_info.errval = DSAT_ERR_INVALID_SYNTAX;
    goto send_error;
  }
  return result;

  
send_error:
  if( err_info.errval == DSAT_ERR_INVALID_NUM_OF_ARGS  || 
      err_info.errval == DSAT_ERR_INVALID_SYNTAX )
  {
    DS_AT_MSG1_ERROR("Error:%d",err_info.errval);
  }
  else
  {
    DS_AT_MSG2_ERROR("Error:%d, arg_num:%d",err_info.errval,err_info.arg_num);
  }
  return DSAT_ERROR;
} /* dsatetsipkt_exec_cgtft_cmd */



/*===========================================================================

FUNCTION DSATETSIPKT_CGTFT_RESPONSE_FMT

DESCRIPTION
  This function generates the response for the +CGTFT read & test
  commands.  Data is extracted from data structure and formatted according
  to parameter flag.

DEPENDENCIES
  None

RETURN VALUE
  Returns number of characters in response buffer.

SIDE EFFECTS
  None

===========================================================================*/
dsat_num_item_type dsatetsipkt_cgtft_response_fmt
(
  const dsati_cmd_type  *parse_table,   /*  Ptr to cmd in parse table   */
  dsm_item_type * const *out_buff_ptr,  /*  Place to put response       */
  multi_index_fmt_e_type format         /*  Flag for format type        */
)
{
  #define NET_PORT_RANGE "(0-65535.0-65535)"
  #define TMASK_RANGE "(0-255.0-255)"
  byte name_buff[16];
  byte i,j,cntr = 0;
  int size = 0;
  int result = 0;
  dsm_item_type * res_buff_ptr = *out_buff_ptr;
  ds_umts_tft_params_type pdata;

  /* Format the command name */
  (void)snprintf((char*)name_buff, 
                        sizeof(name_buff),
                        "%s: ", parse_table->name);
  res_buff_ptr->data_ptr[res_buff_ptr->used] = '\0';

  switch (format)
  {
    case MULTI_INDEX_TEST:
      /* Report each PDP type on seperate line */
      for (i=0; i <= (byte)DSAT_MAX_PDP_TYPE; i++) 
      {
        size = snprintf ((char *)res_buff_ptr->data_ptr, 
                        res_buff_ptr->size,
                        "%s%s%s\"%s\",(%d-%d),(%d-%d),%s,(%d-%d),"
                        "%s,%s,(%X-%X),%s,(%X-%X)",
                        res_buff_ptr->data_ptr,
                        (i > 0) ? "\n" : "",
                        name_buff,
                        dsat_pdptype_valstr[i],
                        ((dflm_type *)dsat_cgtft_pfid_dflm)->lower,
                        ((dflm_type *)dsat_cgtft_pfid_dflm)->upper,
                        ((dflm_type *)dsat_cgtft_prec_dflm)->lower,
                        ((dflm_type *)dsat_cgtft_prec_dflm)->upper,
                        "",
                        ((dflm_type *)dsat_cgtft_pn_nh_dflm)->lower,
                        ((dflm_type *)dsat_cgtft_pn_nh_dflm)->upper,
                        NET_PORT_RANGE,
                        NET_PORT_RANGE,
                        ((dflm_type *)dsat_cgtft_spi_dflm)->lower,
                        ((dflm_type *)dsat_cgtft_spi_dflm)->upper,
                        TMASK_RANGE,
                        ((dflm_type *)dsat_cgtft_flow_dflm)->lower,
                        ((dflm_type *)dsat_cgtft_flow_dflm)->upper
                       );
      }
      res_buff_ptr->used = (uint16)size;
      result = size;
      break;

    case MULTI_INDEX_READ:
      /* Report each profile on seperate line */
      for (i=1; i <= DS_UMTS_MAX_PDP_PROFILE_NUM; i++) 
      {
        /* Loop over all templates */
        for (j=1; j<=DS_UMTS_MAX_TFT_PARAM_SETS; j++)
        {
          ds_umts_pdp_profile_status_etype rc;
          memset((void*)&pdata, 0, sizeof(pdata));
        
          /* Query profile data */
          rc = ds_umts_get_pdp_profile_tft_info_per_subs(
                  SET_CID( i ),dsat_get_current_subs_id(),j,&pdata);
          if (DS_UMTS_PDP_INVALID_FILTER_ID == rc)
          {
            continue;
          }
          else if (DS_UMTS_PDP_SUCCESS != rc)
          {
            DS_AT_MSG2_HIGH("Error CGTFT PDP profile read: %d,%d",i,j);
            return 0;
          }
            
          /* Generate response for valid templates */
          if (TRUE == pdata.valid_flg)
          {
            uint8 tmask = (pdata.tos_mask & 0xFF);
            byte addr_mask_buf[MAX_ADDR_SUBNET_STRING_LEN+1];

            memset(addr_mask_buf, 0, sizeof(addr_mask_buf));

            if ( DSAT_OK != dsatutil_convert_addr_mask( INT_TO_STRING,
                                                        &pdata.src_addr_mask,
                                                        addr_mask_buf ) )
            {
              DS_AT_MSG2_HIGH("Error converting CGTFT address: %d,%d",i,j);
              return 0;
            }
              
            /* Check to see if a new DSM item is required */
            CHECK_APPEND_RESPONSE_BUFFER ((DSM_BUFFER_THRESHOLD - 32));
            
            size = res_buff_ptr->used = (word) 
                   snprintf ((char *)res_buff_ptr->data_ptr, 
                            res_buff_ptr->size,
                            "%s%s%s%d,%d,%d,\"%s\",%d,%d.%d,%d.%d,%X,%d.%d,%X",
                            res_buff_ptr->data_ptr,
                            (cntr > 0) ? "\n" : "",
                            name_buff,
                            i,
                            pdata.filter_id,
                            pdata.eval_prec_id,
                            addr_mask_buf,
                            pdata.prot_num,
                            pdata.dest_port_range.from,
                            pdata.dest_port_range.to,
                            pdata.src_port_range.from,
                            pdata.src_port_range.to,
                            (int)pdata.ipsec_spi,
                            (pdata.tos_mask >> 8),
                            tmask,
                            (int)pdata.flow_label
                           );
            cntr++;
          }
        } /* for */
      } /* for */
      
      /* No contexts defined, just print command name */
      if (0 == cntr)
      {
        size = snprintf((char *)res_buff_ptr->data_ptr,
                              res_buff_ptr->size, "%s", name_buff);
      }
      res_buff_ptr->used = (uint16)size;
      result = size;
      break;

    case MULTI_INDEX_AMPV:
      size = res_buff_ptr->used;
      res_buff_ptr->used = (word)snprintf(
                                   (char *)res_buff_ptr->data_ptr,
                                   res_buff_ptr->size,
                                   "%s%s",
                                   res_buff_ptr->data_ptr,
                                   name_buff);

      /* Report each profile in set notation */
      for (i=1; i <= DS_UMTS_MAX_PDP_PROFILE_NUM; i++) 
      {
        /* Loop over all templates */
        for (j=1; j<=DS_UMTS_MAX_TFT_PARAM_SETS; j++)
        {
          ds_umts_pdp_profile_status_etype rc;
          memset((void*)&pdata, 0, sizeof(pdata));
        
          /* Query profile data */
          rc = ds_umts_get_pdp_profile_tft_info_per_subs(
                 SET_CID( i ),dsat_get_current_subs_id(),j,&pdata);
          if (DS_UMTS_PDP_INVALID_FILTER_ID == rc)
          {
            continue;
          }
          else if (DS_UMTS_PDP_SUCCESS != rc)
          {
            DS_AT_MSG2_HIGH("Error CGTFT PDP profile read: %d,%d",i,j);
            return 0;
          }
            
          /* Generate response for valid templates */
          if (TRUE == pdata.valid_flg)
          {
            uint8 tmask = (pdata.tos_mask & 0xFF);
            byte addr_mask_buf[MAX_ADDR_SUBNET_STRING_LEN+1];

            memset(addr_mask_buf, 0, sizeof(addr_mask_buf));

            if ( DSAT_OK != dsatutil_convert_addr_mask( INT_TO_STRING,
                                                        &pdata.src_addr_mask,
                                                        addr_mask_buf ) )
            {
              DS_AT_MSG2_HIGH("Error converting CGTFT address: %d,%d",i,j);
              return 0;
            }
                            
            /* Check to see if a new DSM item is required */
            CHECK_APPEND_RESPONSE_BUFFER ((DSM_BUFFER_THRESHOLD - 32));
            
            /* Generate response from internal storage */
            res_buff_ptr->used =
              (word)snprintf (
                             (char *)res_buff_ptr->data_ptr, 
                             res_buff_ptr->size,
                             "%s%s(%d,%d,%d,\"%s\",%d,%d.%d,%d.%d,%X,%d.%d,%X)",
                             res_buff_ptr->data_ptr,
                             (cntr > 0) ? "," : "",
                             i,
                             pdata.filter_id,
                             pdata.eval_prec_id,
                             addr_mask_buf,
                             pdata.prot_num,
                             pdata.dest_port_range.from,
                             pdata.dest_port_range.to,
                             pdata.src_port_range.from,
                             pdata.src_port_range.to,
                             (int)pdata.ipsec_spi,
                             (pdata.tos_mask >> 8),
                             tmask,
                             (int)pdata.flow_label
                             );
            cntr++;
          }
        } /* for */
      } /* for */

      /* Return zero if DSM item changed */
      result = (res_buff_ptr == *out_buff_ptr)?
               (res_buff_ptr->used - size) : 0;
      (*out_buff_ptr)->used -= (word)result;  /* caller increments used */
      break;
      
    default:
      DS_AT_MSG0_HIGH("Unsupported multi-index format type");
  }
  return (dsat_num_item_type)result;
}  /* dsatetsipkt_cgtft_response_fmt */
#endif /* FEATURE_SECONDARY_PDP */

#ifdef FEATURE_DATA_WCDMA_PS

/*===========================================================================

FUNCTION DSATETSIPKT_EXEC_CGEQREQ_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It handles the +CGEQREQ command.
  This command sets PDP context parameters for UMTS QoS Profile
  (Requested).

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success.

SIDE EFFECTS
  Defining +CGEQREQ parameters will undefine +CGQREQ/+CGQMIN parameters;
  these commands are mutually exclusive.

===========================================================================*/
dsat_result_enum_type dsatetsipkt_exec_cgeqreq_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type result = DSAT_OK;
  dsat_num_item_type val = 0;
  dsat_mixed_param_val_type * val_ptr = 
    (dsat_mixed_param_val_type *) dsatutil_get_val(parse_table->cmd_id,0,0,STR_TYPE);
  uint32                           src_stat_desc_req = 0;
  dsat_error_info_s_type    err_info;

  err_info.errval = DSAT_ERR_NO_ERROR;
  err_info.arg_num = 0;

  if (tok_ptr->op == (NA|EQ|AR))
  {
    /*---------------------------------------------------------------
      Processing for a command with an argument (WRITE command)
    ---------------------------------------------------------------*/

    /* Validate connection ID */
    if (DSAT_OK != etsipkt_process_param(parse_table, tok_ptr, 0, &val,1))
    {
      err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
      err_info.arg_num = 0;
      goto send_error;
    }
    
    /* Test for only CID specified */
    if (1 == tok_ptr->args_found)
    {
      /* Clear the user specified context parameters */
      if ( DS_UMTS_PDP_SUCCESS !=
          ds_umts_reset_pdp_profile_umts_qos_req_info_per_subs(
            SET_CID( val ),dsat_get_current_subs_id() ))
      {
        DS_AT_MSG1_HIGH("Error on profile UMTS REQ undefine: %d",val);
        return DSAT_ERROR;
      }
      if ( DS_UMTS_PDP_SUCCESS != 
           ds_umts_reset_pdp_profile_src_stat_desc_req_info_per_subs(
              SET_CID( val ),dsat_get_current_subs_id()) )
      {
        DS_AT_MSG1_HIGH("Error on profile UMTS REQ undefine: %d",val);
        return DSAT_ERROR;
      }
    }
    else
    {
      byte cid = (byte)val;
      
      /* Perform normal parameter processing */
      if (DSAT_OK == dsatparm_exec_param_cmd(
                                             mode,
                                             parse_table,
                                             tok_ptr,
                                             res_buff_ptr
                                            ))
      {
        /* Transfer user data to internal storage */
        ds_umts_umts_qos_params_type pdata;

        /* Local variables for PACKED structure */
        ds_umts_qos_tclass_enum_type traffic_class;
        ds_umts_qos_dorder_enum_type dlvry_order;
        ds_umts_qos_sdu_error_enum_type  sdu_err;
        ds_umts_qos_sdu_dlvr_enum_type   sdu_dlvr;
        ds_umts_qos_res_ber_enum_type    res_biterr;


         /* Set specified profile to defaults in both GPRS and UMTS */
        if ((DS_UMTS_PDP_SUCCESS !=
                 ds_umts_reset_pdp_profile_umts_qos_req_info_per_subs(SET_CID( cid ),dsat_get_current_subs_id() ))|| 
            (DS_UMTS_PDP_SUCCESS !=
                 ds_umts_reset_pdp_profile_gprs_qos_req_info_per_subs(SET_CID( cid ),dsat_get_current_subs_id() )))
        {
          DS_AT_MSG1_HIGH("Error on PDP profile to default: %d",cid);
          return DSAT_ERROR;
        }
        if ( DS_UMTS_PDP_SUCCESS != 
             ds_umts_reset_pdp_profile_src_stat_desc_req_info_per_subs(
                SET_CID( cid ),dsat_get_current_subs_id()) )
        {
          DS_AT_MSG1_HIGH("Error on PDP profile to default: %d",cid);
          return DSAT_ERROR;
        }
        
        /* Query default profile from EFS */
        memset((void*)&pdata, 0, sizeof(pdata));
        if ( DS_UMTS_PDP_SUCCESS !=
             ds_umts_get_pdp_profile_umts_qos_req_info_per_subs(
                SET_CID( cid ),dsat_get_current_subs_id(),&pdata) )
        {
          DS_AT_MSG1_HIGH("Error CGEQREQ PDP profile read: %d",cid);
          return DSAT_ERROR;
        }
        if ( DS_UMTS_PDP_SUCCESS != 
             ds_umts_get_pdp_profile_src_stat_desc_req_info_per_subs(
                               SET_CID( cid ), 
                dsat_get_current_subs_id(),&src_stat_desc_req) )
        {
          DS_AT_MSG1_HIGH("Error CGEQREQ PDP profile read: %d",cid);
          return DSAT_ERROR;
        }
        
        /* Make conditional edits to profile */
        pdata.valid_flg        = TRUE;
        CONDITIONAL_ASSIGN(2,  pdata.max_ul_bitrate, val_ptr[2].num_item);
        CONDITIONAL_ASSIGN(3,  pdata.max_dl_bitrate, val_ptr[3].num_item);
        CONDITIONAL_ASSIGN(4,  pdata.gtd_ul_bitrate, val_ptr[4].num_item);
        CONDITIONAL_ASSIGN(5,  pdata.gtd_dl_bitrate, val_ptr[5].num_item);
        CONDITIONAL_ASSIGN(7,  pdata.max_sdu_size,   val_ptr[7].num_item);
        CONDITIONAL_ASSIGN(11, pdata.trans_delay,    val_ptr[11].num_item);
        CONDITIONAL_ASSIGN(12, pdata.thandle_prio,   val_ptr[12].num_item);
        CONDITIONAL_ASSIGN(13, src_stat_desc_req,    val_ptr[13].num_item);
        CONDITIONAL_ASSIGN(14, pdata.sig_ind,        val_ptr[14].num_item);

        /* Map Traffic Class parameter to enum type */
        if (FALSE == etsipkt_map_tclass (TRUE, &val_ptr[1].num_item,
                                         &traffic_class))
        {
          err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
          err_info.arg_num = 1;
          goto send_error;
        }
        else
        {
          CONDITIONAL_ASSIGN(1, pdata.traffic_class, traffic_class);
        }

        /* Map Delivery Order parameter to enum type */
        if (FALSE == etsipkt_map_dorder (TRUE, &val_ptr[6].num_item,
                                         &dlvry_order))
        {
          err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
          err_info.arg_num = 6;
          goto send_error;
        }
        else
        {
          CONDITIONAL_ASSIGN(6, pdata.dlvry_order, dlvry_order);
        }
          
        /* Map SDU error parameter to enum type */
        if (FALSE == etsipkt_map_sdu_err(TRUE, &val_ptr[8].num_item,
                                         &sdu_err))
        {
          err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
          err_info.arg_num = 8;
          goto send_error;
        }
        else
        {
          CONDITIONAL_ASSIGN(8, pdata.sdu_err, sdu_err);
        }
          
        /* Map Erroneous SDU Delivery parameter to enum type */
        if (FALSE == etsipkt_map_dlvr_err_sdu(TRUE, &val_ptr[10].num_item,
                                              &sdu_dlvr))
        {
          err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
          err_info.arg_num = 10;
          goto send_error;
        }
        else
        {
          CONDITIONAL_ASSIGN(10, pdata.dlvr_err_sdu, sdu_dlvr);
        }

        /* Map Residual BER parameter to enum type */
        if (FALSE == etsipkt_map_res_ber(TRUE, &val_ptr[9].num_item,
                                         &res_biterr))
        {
          err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
          err_info.arg_num = 9;
          goto send_error;
        }
        else
        {
          CONDITIONAL_ASSIGN(9, pdata.res_biterr, res_biterr);
        }


        /* Validate the traffic class against dependent parameters.    */
        /* This must be done after user edits applied but before write */
        if (DSAT_ERROR == etsipkt_validate_tclass(parse_table, &pdata))
        {
          return DSAT_ERROR;
        }
        else
        {
          DS_AT_MSG1_HIGH("TClass validated: %d",pdata.traffic_class);
        }

        
        /* Write to EFS */
        if ( DS_UMTS_PDP_SUCCESS !=
            ds_umts_set_pdp_profile_umts_qos_req_info_per_subs(
              SET_CID( cid ), dsat_get_current_subs_id(), &pdata) )
        {
          DS_AT_MSG1_HIGH("Error CGEQREQ PDP profile write: %d",cid);
          return DSAT_ERROR;
        }
        if( src_stat_desc_req == 1) 
        {
          if( !((pdata.traffic_class == DS_UMTS_QOS_TCLASS_CONV) ||
                (pdata.traffic_class == DS_UMTS_QOS_TCLASS_STRM)) )
          {
            DS_AT_MSG0_HIGH("Error Src Stat Desc shouldn't set with traffic class"
                      "other than conversational or streaming");
            return DSAT_ERROR;
          }
        }
        if ( DS_UMTS_PDP_SUCCESS != 
             ds_umts_set_pdp_profile_src_stat_desc_req_info_per_subs(
                SET_CID( cid ), 
                dsat_get_current_subs_id(),
                src_stat_desc_req) )
        {
          DS_AT_MSG1_HIGH("Error CGEQREQ PDP profile write: %d",cid);
          return DSAT_ERROR;
        }
      }
      else
      {
        result = DSAT_ERROR;
      }
    }
  }
  else if (tok_ptr->op == (NA|EQ|QU))
  {
    /*---------------------------------------------------------------
      Processing for a command to query valid parameters
    ---------------------------------------------------------------*/
    if (0 == dsatetsipkt_cgeqreq_response_fmt (parse_table,
                                               &res_buff_ptr,
                                               MULTI_INDEX_TEST))
    {
      DS_AT_MSG0_HIGH("Problem generating query response");
      result = DSAT_ERROR;
    }
  }
  else if (tok_ptr->op == (NA|QU))
  {
    /*---------------------------------------------------------------
       Processing for a command to query current parameters
    ---------------------------------------------------------------*/
    if (0 == dsatetsipkt_cgeqreq_response_fmt (parse_table,
                                               &res_buff_ptr,
                                               MULTI_INDEX_READ))
    {
      DS_AT_MSG0_HIGH("Problem generating query response");
      result = DSAT_ERROR;
    }
  }
  else if (tok_ptr->op == (NA))
  {
    /* Do nothing */
    result = DSAT_OK;
  }
  else
  {
    err_info.errval = DSAT_ERR_INVALID_SYNTAX;
    goto send_error;
  }
  return result;

send_error:
  if( err_info.errval == DSAT_ERR_INVALID_SYNTAX)
  {
    DS_AT_MSG1_ERROR("Error:%d",err_info.errval);
  }
  else
  {
    DS_AT_MSG2_ERROR("Error:%d, arg_num:%d",err_info.errval,err_info.arg_num);
  }
  return DSAT_ERROR;
} /* dsatetsipkt_exec_cgeqreq_cmd */



/*===========================================================================

FUNCTION DSATETSIPKT_CGEQREQ_RESPONSE_FMT

DESCRIPTION
  This function generates the response for the +CGEQREQ read & test
  commands.  Data is extracted from data structure and formatted according
  to parameter flag.

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
dsat_num_item_type dsatetsipkt_cgeqreq_response_fmt
(
  const dsati_cmd_type  *parse_table,   /*  Ptr to cmd in parse table   */
  dsm_item_type * const *out_buff_ptr,  /*  Place to put response       */
  multi_index_fmt_e_type format         /*  Flag for format type        */
)
{
  byte name_buff[16];
  byte i, cntr = 0;
  int size = 0;
  int result = 0;
  dsm_item_type * res_buff_ptr = *out_buff_ptr;
  ds_umts_umts_qos_params_type pdata;
  uint32                           src_stat_desc_req = 0;
    
  /* Format the command name */
  (void)snprintf((char*)name_buff, 
                       sizeof(name_buff),
                       "%s: ", parse_table->name);
  res_buff_ptr->data_ptr[res_buff_ptr->used] = '\0';
  
  switch (format)
  {
    case MULTI_INDEX_TEST:
      /* Report each PDP type on seperate line */
      for (i=0; i <= (byte)DSAT_MAX_PDP_TYPE; i++) 
      {
        size = snprintf ((char *)res_buff_ptr->data_ptr, 
                 res_buff_ptr->size,
                 "%s%s%s\"%s\",(%d-%d),(%d-%d),(%d-%d),(%d-%d),(%d-%d),(%d-%d),(%d-%d)",
                 res_buff_ptr->data_ptr,
                 (i > 0) ? "\n" : "",
                 name_buff,
                 dsat_pdptype_valstr[i],       
                 ((dflm_type *)dsat_cgeqreq_tclass_dflm)->lower,
                 ((dflm_type *)dsat_cgeqreq_tclass_dflm)->upper,
                 ((dflm_type *)dsat_cgeqreq_req_ul_bitrate_dflm)->lower,
                 ((dflm_type *)dsat_cgeqreq_req_ul_bitrate_dflm)->upper,
                 ((dflm_type *)dsat_cgeqreq_req_dl_bitrate_dflm)->lower,
                 ((dflm_type *)dsat_cgeqreq_req_dl_bitrate_dflm)->upper,
                 ((dflm_type *)dsat_cgeqreq_gtd_ul_bitrate_dflm)->lower,
                 ((dflm_type *)dsat_cgeqreq_gtd_ul_bitrate_dflm)->upper,
                 ((dflm_type *)dsat_cgeqreq_gtd_dl_bitrate_dflm)->lower,
                 ((dflm_type *)dsat_cgeqreq_gtd_dl_bitrate_dflm)->upper,
                 ((dflm_type *)dsat_cgeqreq_dlvry_order_dflm)->lower,
                 ((dflm_type *)dsat_cgeqreq_dlvry_order_dflm)->upper,
                 ((dflm_type *)dsat_cgeqreq_max_sdu_size_dflm)->lower,
                 ((dflm_type *)dsat_cgeqreq_max_sdu_size_dflm)->upper
                );
        
        size = snprintf ((char *)res_buff_ptr->data_ptr, 
                 res_buff_ptr->size,	
                 "%s,%s,%s,(%d-%d),(%d,%d-%d),(%d-%d),(0,1),(0,1)",
                 res_buff_ptr->data_ptr,
                 dsat_cgeqreq_sdu_err_tststr,
                 dsat_cgeqreq_res_biterr_tststr,
                 ((dflm_type *)dsat_cgeqreq_dlvry_err_dflm)->lower,
                 ((dflm_type *)dsat_cgeqreq_dlvry_err_dflm)->upper,
                 ((dflm_type *)dsat_cgeqreq_trans_delay_dflm)->lower,
                 ((dflm_type *)dsat_cgeqreq_trans_delay_dflm+1)->lower,
                 ((dflm_type *)dsat_cgeqreq_trans_delay_dflm+1)->upper,
                 ((dflm_type *)dsat_cgeqreq_thandle_prio_dflm)->lower,
                 ((dflm_type *)dsat_cgeqreq_thandle_prio_dflm)->upper
                );
      }
      res_buff_ptr->used = (uint16)size;
      result = size;
      break;

    case MULTI_INDEX_READ:
      /* Report each profile on seperate line */
      for (i=1; i <= DS_UMTS_MAX_PDP_PROFILE_NUM; i++) 
      {
        ds_umts_qos_tclass_enum_type traffic_class;
        ds_umts_qos_dorder_enum_type dlvry_order;
        ds_umts_qos_sdu_error_enum_type  sdu_err;
        ds_umts_qos_sdu_dlvr_enum_type   sdu_dlvr;
        ds_umts_qos_res_ber_enum_type    res_biterr;
        dsat_num_item_type tclass = 0;
        dsat_num_item_type dorder = 0;
        dsat_num_item_type sdu_err_index = 0;
        dsat_num_item_type dlvr_err_sdu = 0;
        dsat_num_item_type res_ber_index = 0;
        
        /* Query profile from EFS */
        memset((void*)&pdata, 0, sizeof(pdata));
        if ( DS_UMTS_PDP_SUCCESS !=
            ds_umts_get_pdp_profile_umts_qos_req_info_per_subs(
              SET_CID( i ), dsat_get_current_subs_id(), &pdata) )
        {
          DS_AT_MSG1_HIGH("Error CGEQREQ PDP profile read: %d",i);
          return 0;
        }
        if ( DS_UMTS_PDP_SUCCESS != 
             ds_umts_get_pdp_profile_src_stat_desc_req_info_per_subs(
                          SET_CID( i ), 
                dsat_get_current_subs_id(),&src_stat_desc_req) )
        {
          DS_AT_MSG1_HIGH("Error CGEQREQ PDP profile read: %d",i);
          return 0;
        }
        
        /* Skip reporting if profile data invalid */
        if (FALSE == pdata.valid_flg)
        {
          continue;
        }
        
        /* Perform table lookups */
        traffic_class = pdata.traffic_class;
        if (FALSE ==
            etsipkt_map_tclass(FALSE, &tclass, &traffic_class))
        {
          DS_AT_MSG1_HIGH("Error mapping TClass: %d",traffic_class);
          return 0;
        }
        
        dlvry_order = pdata.dlvry_order;
        if (FALSE ==
            etsipkt_map_dorder(FALSE, &dorder, &dlvry_order))
        {
          DS_AT_MSG1_HIGH("Error mapping DOrder: %d",dlvry_order);
          return 0;
        }
        
        sdu_err = pdata.sdu_err;
        if (FALSE ==
            etsipkt_map_sdu_err(FALSE, &sdu_err_index, &sdu_err))
        {
          DS_AT_MSG1_HIGH("Error mapping SDU error: %d",sdu_err);
          return 0;
        }
        
        sdu_dlvr = pdata.dlvr_err_sdu;
        if (FALSE ==
            etsipkt_map_dlvr_err_sdu(FALSE, &dlvr_err_sdu, &sdu_dlvr))
        {
          DS_AT_MSG1_HIGH("Error mapping Dlvr Err SDU: %d",sdu_dlvr);
          return 0;
        }

        res_biterr = pdata.res_biterr;
        if (FALSE ==
            etsipkt_map_res_ber(FALSE, &res_ber_index, &res_biterr))
        {
          DS_AT_MSG1_HIGH("Error mapping RES BER: %d",pdata.res_biterr);
          return 0;
        }
        
        /* Check to see if a new DSM item is required */
        CHECK_APPEND_RESPONSE_BUFFER ((DSM_BUFFER_THRESHOLD - 30));
          
        /* Generate response from internal storage */
        res_buff_ptr->used = (uint16)snprintf((char *)res_buff_ptr->data_ptr, 
                 res_buff_ptr->size,
                 "%s%s%s%d,%d,%lu,%lu,%lu,%lu,%d,%lu,\"%s\",\"%s\",%d,%lu,%lu,%u,%d",
                 res_buff_ptr->data_ptr,
                 (cntr > 0) ? "\n" : "",
                 name_buff,
                 i,
                 tclass,
                 pdata.max_ul_bitrate,       
                 pdata.max_dl_bitrate,       
                 pdata.gtd_ul_bitrate,       
                 pdata.gtd_dl_bitrate,       
                 dorder,       
                 pdata.max_sdu_size,       
                 dsat_cgeqreq_sdu_err_valstr[sdu_err_index],
                 dsat_cgeqreq_res_biterr_valstr[res_ber_index],
                 dlvr_err_sdu,
                 pdata.trans_delay,
                 pdata.thandle_prio,
                 (unsigned int)src_stat_desc_req,
                 pdata.sig_ind
                );
        cntr++;
      }
      
      /* No contexts defined, just print command name */
      if (0 == cntr)
      {
        res_buff_ptr->used = (uint16)snprintf((char *)res_buff_ptr->data_ptr,
                       res_buff_ptr->size,
                       "%s%s",
                       res_buff_ptr->data_ptr,
                       name_buff);
      }
      result = res_buff_ptr->used;
      break;

    case MULTI_INDEX_AMPV:
      size = res_buff_ptr->used;
      res_buff_ptr->used = (word)snprintf(
                                   (char *)res_buff_ptr->data_ptr,
                                   res_buff_ptr->size,
                                   "%s%s",
                                   res_buff_ptr->data_ptr,
                                   name_buff);

      /* Report each profile in set notation */
      for (i=1; i <= DS_UMTS_MAX_PDP_PROFILE_NUM; i++) 
      {
        /* Local variables for PACKED structure */
        ds_umts_qos_tclass_enum_type traffic_class;
        ds_umts_qos_dorder_enum_type dlvry_order;
        ds_umts_qos_sdu_error_enum_type  sdu_err;
        ds_umts_qos_sdu_dlvr_enum_type   sdu_dlvr;
        ds_umts_qos_res_ber_enum_type    res_biterr;
        
        dsat_num_item_type tclass = 0;
        dsat_num_item_type dorder = 0;
        dsat_num_item_type sdu_err_index = 0;
        dsat_num_item_type dlvr_err_sdu = 0;
        dsat_num_item_type res_ber_index = 0;
        
        /* Query profile from EFS */
        memset((void*)&pdata, 0, sizeof(pdata));
        if ( DS_UMTS_PDP_SUCCESS !=
             ds_umts_get_pdp_profile_umts_qos_req_info_per_subs(
                SET_CID( i ), dsat_get_current_subs_id(), &pdata))
        {
          DS_AT_MSG1_HIGH("Error CGEQREQ PDP profile read: %d",i);
          result = res_buff_ptr->used - size;
          res_buff_ptr->used -= (word)result;  /* caller increments used */
          return (dsat_num_item_type)result;
        }
        if ( DS_UMTS_PDP_SUCCESS != 
             ds_umts_get_pdp_profile_src_stat_desc_req_info_per_subs(
                               SET_CID( i ), 
                dsat_get_current_subs_id(),&src_stat_desc_req) )
        {
          DS_AT_MSG1_HIGH("Error CGEQREQ PDP profile read: %d",i);
          result = res_buff_ptr->used - size;
          res_buff_ptr->used -= (word)result;  /* caller increments used */
          return (dsat_num_item_type)result;
        }
            
        /* Skip reporting if profile data invalid */
        if (FALSE == pdata.valid_flg)
        {
          continue;
        }
        
        /* Perform table lookups */
        traffic_class = pdata.traffic_class;
        if (FALSE ==
            etsipkt_map_tclass(FALSE, &tclass, &traffic_class))
        {
          DS_AT_MSG1_HIGH("Error mapping TClass: %d",traffic_class);
          return 0;
        }
        
        dlvry_order = pdata.dlvry_order;
        if (FALSE ==
            etsipkt_map_dorder(FALSE, &dorder, &dlvry_order))
        {
          DS_AT_MSG1_HIGH("Error mapping DOrder: %d",dlvry_order);
          return 0;
        }
        
        sdu_err = pdata.sdu_err;
        if (FALSE ==
            etsipkt_map_sdu_err(FALSE, &sdu_err_index, &sdu_err))
        {
          DS_AT_MSG1_HIGH("Error mapping SDU error: %d",sdu_err);
          return 0;
        }
        
        sdu_dlvr = pdata.dlvr_err_sdu;
        if (FALSE ==
            etsipkt_map_dlvr_err_sdu(FALSE, &dlvr_err_sdu, &sdu_dlvr))
        {
          DS_AT_MSG1_HIGH("Error mapping Dlvr Err SDU: %d",sdu_dlvr);
          return 0;
        }

        res_biterr = pdata.res_biterr;
        if (FALSE ==
            etsipkt_map_res_ber(FALSE, &res_ber_index, &res_biterr))
        {
          DS_AT_MSG1_HIGH("Error mapping RES BER: %d",pdata.res_biterr);
          return 0;
        }
        
        /* Check to see if a new DSM item is required */
        CHECK_APPEND_RESPONSE_BUFFER ((DSM_BUFFER_THRESHOLD - 30));
          
        /* Generate response from internal storage */
        res_buff_ptr->used = (word)snprintf (
                 (char *)res_buff_ptr->data_ptr, 
                 res_buff_ptr->size,
                 "%s%s(%d,%d,%lu,%lu,%lu,%lu,%d,%lu,\"%s\",\"%s\",%d,%lu,%lu,%u,%d)",
                 res_buff_ptr->data_ptr,
                 (cntr > 0) ? "," : "",
                 i,
                 tclass,
                 pdata.max_ul_bitrate,       
                 pdata.max_dl_bitrate,       
                 pdata.gtd_ul_bitrate,       
                 pdata.gtd_dl_bitrate,       
                 dorder,       
                 pdata.max_sdu_size,       
                 dsat_cgeqreq_sdu_err_valstr[sdu_err_index],
                 dsat_cgeqreq_res_biterr_valstr[res_ber_index],
                 dlvr_err_sdu,
                 pdata.trans_delay,
                 pdata.thandle_prio,
                 (unsigned int)src_stat_desc_req,
                 pdata.sig_ind
                );
        cntr++;
      }

      /* Return zero if DSM item changed */
      result = (res_buff_ptr == *out_buff_ptr)?
               (res_buff_ptr->used - size) : 0;
      (*out_buff_ptr)->used -= (word)result;  /* caller increments used */
      break;
      
    default:
      DS_AT_MSG0_HIGH("Unsupported multi-index format type");
  }

  return (dsat_num_item_type)result;
}  /* dsatetsipkt_cgeqreq_response_fmt */



/*===========================================================================

FUNCTION DSATETSIPKT_EXEC_CGEQMIN_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It handles the +CGEQMIN command.
  This command sets PDP context parameters for UMTS QoS Profile
  (Minimum).

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success.

SIDE EFFECTS
  Defining +CGEQMIN parameters will undefine +CGQREQ/+CGQMIN parameters;
  these commands are mutually exclusive.

===========================================================================*/
dsat_result_enum_type dsatetsipkt_exec_cgeqmin_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type result = DSAT_OK;
  dsat_num_item_type val = 0;
  dsat_mixed_param_val_type * val_ptr = 
    (dsat_mixed_param_val_type *) dsatutil_get_val(parse_table->cmd_id,0,0,STR_TYPE);
  uint32                           src_stat_desc_min = 0;
  dsat_error_info_s_type    err_info;

  err_info.errval = DSAT_ERR_NO_ERROR;
  err_info.arg_num = 0;

  if (tok_ptr->op == (NA|EQ|AR))
  {
    /*---------------------------------------------------------------
      Processing for a command with an argument (WRITE command)
    ---------------------------------------------------------------*/

    /* Validate connection ID */
    if (DSAT_OK != etsipkt_process_param(parse_table, tok_ptr, 0, &val,1))
    {
      err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
      err_info.arg_num = 0;
      goto send_error;
    }
    /* Test for only CID specified */
    if (1 == tok_ptr->args_found)
    {
      /* Clear the user specified context parameters */
      if ( DS_UMTS_PDP_SUCCESS !=
          ds_umts_reset_pdp_profile_umts_qos_min_info_per_subs(
            SET_CID( val ),dsat_get_current_subs_id()))
      {
        DS_AT_MSG1_HIGH("Error on profile UMTS MIN undefine: %d",val);
        return DSAT_ERROR;
      }
      
      if ( DS_UMTS_PDP_SUCCESS != 
           ds_umts_reset_pdp_profile_src_stat_desc_min_info_per_subs(
              SET_CID( val ),dsat_get_current_subs_id()) )
      {
        DS_AT_MSG1_HIGH("Error on profile UMTS MIN undefine: %d",val);
        return DSAT_ERROR;
      }
    }
    else
    {
      byte cid = (byte)val;
      
      /* Perform normal parameter processing */
      if (DSAT_OK == dsatparm_exec_param_cmd(
                                             mode,
                                             parse_table,
                                             tok_ptr,
                                             res_buff_ptr
                                            ))
      {
        /* Transfer user data to internal storage */
        ds_umts_umts_qos_params_type pdata;
        
        /* Local variables for PACKED structure */
        ds_umts_qos_tclass_enum_type traffic_class;
        ds_umts_qos_dorder_enum_type dlvry_order;
        ds_umts_qos_sdu_error_enum_type  sdu_err;
        ds_umts_qos_sdu_dlvr_enum_type   sdu_dlvr;
        ds_umts_qos_res_ber_enum_type    res_biterr;

        /* Set specified profile to defaults */
        if ( DS_UMTS_PDP_SUCCESS !=
            ds_umts_reset_pdp_profile_umts_qos_min_info_per_subs(
              SET_CID( cid ),dsat_get_current_subs_id() ))
        {
          DS_AT_MSG1_HIGH("Error on PDP profile to default: %d",cid);
          return DSAT_ERROR;
        }
        if ( DS_UMTS_PDP_SUCCESS != 
             ds_umts_reset_pdp_profile_src_stat_desc_min_info_per_subs(
                SET_CID( cid ),dsat_get_current_subs_id()) )
        {
          DS_AT_MSG1_HIGH("Error on PDP profile to default: %d",cid);
          return DSAT_ERROR;
        }
        
        /* Query default profile from EFS */
        memset((void*)&pdata, 0, sizeof(pdata));
        if ( DS_UMTS_PDP_SUCCESS !=
            ds_umts_get_pdp_profile_umts_qos_min_info_per_subs(
              SET_CID( cid ),dsat_get_current_subs_id(),&pdata) )
        {
          DS_AT_MSG1_HIGH("Error CGEQMIN PDP profile read: %d",cid);
          return DSAT_ERROR;
        }
        if ( DS_UMTS_PDP_SUCCESS != 
             ds_umts_get_pdp_profile_src_stat_desc_min_info_per_subs(
                               SET_CID( cid ), 
                dsat_get_current_subs_id(),&src_stat_desc_min) )
        {
          DS_AT_MSG1_HIGH("Error CGEQMIN PDP profile read: %d",cid);
          return DSAT_ERROR;
        }
        
        /* Make conditional edits to profile */
        pdata.valid_flg        = TRUE;
        CONDITIONAL_ASSIGN(2,  pdata.max_ul_bitrate, val_ptr[2].num_item);
        CONDITIONAL_ASSIGN(3,  pdata.max_dl_bitrate, val_ptr[3].num_item);
        CONDITIONAL_ASSIGN(4,  pdata.gtd_ul_bitrate, val_ptr[4].num_item);
        CONDITIONAL_ASSIGN(5,  pdata.gtd_dl_bitrate, val_ptr[5].num_item);
        CONDITIONAL_ASSIGN(7,  pdata.max_sdu_size,   val_ptr[7].num_item);
        CONDITIONAL_ASSIGN(11, pdata.trans_delay,    val_ptr[11].num_item);
        CONDITIONAL_ASSIGN(12, pdata.thandle_prio,   val_ptr[12].num_item);
        CONDITIONAL_ASSIGN(13, src_stat_desc_min,    val_ptr[13].num_item);
        CONDITIONAL_ASSIGN(14, pdata.sig_ind,        val_ptr[14].num_item);

        /* Map Traffic Class parameter to enum type */
        if (FALSE == etsipkt_map_tclass (TRUE, &val_ptr[1].num_item,
                                         &traffic_class))
        {
          err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
          err_info.arg_num = 1;
          goto send_error;
        }
        else
        {
          CONDITIONAL_ASSIGN(1, pdata.traffic_class, traffic_class);
        }

        /* Map Delivery Order parameter to enum type */
        if (FALSE == etsipkt_map_dorder (TRUE, &val_ptr[6].num_item,
                                         &dlvry_order))
        {
          err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
          err_info.arg_num = 6;
          goto send_error;
        }
        else
        {
          CONDITIONAL_ASSIGN(6, pdata.dlvry_order, dlvry_order);
        }
          
        /* Map SDU error parameter to enum type */
        if (FALSE == etsipkt_map_sdu_err(TRUE, &val_ptr[8].num_item,
                                         &sdu_err))
        {
          err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
          err_info.arg_num = 8;
          goto send_error;
        }
        else
        {
          CONDITIONAL_ASSIGN(8, pdata.sdu_err, sdu_err);
        }
          
        /* Map Erroneous SDU Delivery parameter to enum type */
        if (FALSE == etsipkt_map_dlvr_err_sdu(TRUE, &val_ptr[10].num_item,
                                              &sdu_dlvr))
        {
          err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
          err_info.arg_num = 10;
          goto send_error;
        }
        else
        {
          CONDITIONAL_ASSIGN(10, pdata.dlvr_err_sdu, sdu_dlvr);
        }

        /* Map Residual BER parameter to enum type */
        if (FALSE == etsipkt_map_res_ber(TRUE, &val_ptr[9].num_item,
                                         &res_biterr))
        {
          err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
          err_info.arg_num = 9;
          goto send_error;
        }
        else
        {
          CONDITIONAL_ASSIGN(9, pdata.res_biterr, res_biterr);
        }


        /* Validate the traffic class against dependent parameters.    */
        /* This must be done after user edits applied but before write */
        if (DSAT_ERROR == etsipkt_validate_tclass(parse_table, &pdata))
        {
          return DSAT_ERROR;
        }
        else
        {
          DS_AT_MSG1_HIGH("TClass validated: %d",pdata.traffic_class);
        }

        /* Write to EFS */
        if ( DS_UMTS_PDP_SUCCESS !=
            ds_umts_set_pdp_profile_umts_qos_min_info_per_subs(
              SET_CID( cid ), dsat_get_current_subs_id(), &pdata) )
        {
          DS_AT_MSG1_HIGH("Error CGEQMIN PDP profile write: %d",cid);
          return DSAT_ERROR;
        }
        if( src_stat_desc_min == 1) 
        {
          if( !((pdata.traffic_class == DS_UMTS_QOS_TCLASS_CONV) ||
                (pdata.traffic_class == DS_UMTS_QOS_TCLASS_STRM)) )
          {
            DS_AT_MSG0_HIGH("Error Src Stat Desc shouldn't set with traffic class"
                      "other than conversational or streaming");
            return DSAT_ERROR;
          }
        }
        if ( DS_UMTS_PDP_SUCCESS != 
             ds_umts_set_pdp_profile_src_stat_desc_min_info_per_subs(
                SET_CID( cid ), 
                dsat_get_current_subs_id(),
                src_stat_desc_min) )
        {
          DS_AT_MSG1_HIGH("Error CGEQMIN PDP profile write: %d",cid);
          return DSAT_ERROR;
        }
      }
      else
      {
        result = DSAT_ERROR;
      }
    }
  }
  else if (tok_ptr->op == (NA|EQ|QU))
  {
    /*---------------------------------------------------------------
      Processing for a command to query valid parameters
    ---------------------------------------------------------------*/
    if (0 == dsatetsipkt_cgeqmin_response_fmt (parse_table,
                                               &res_buff_ptr,
                                               MULTI_INDEX_TEST))
    {
      DS_AT_MSG0_HIGH("Problem generating query response");
      result = DSAT_ERROR;
    }
  }
  else if (tok_ptr->op == (NA|QU))
  {
    /*---------------------------------------------------------------
       Processing for a command to query current parameters
    ---------------------------------------------------------------*/
    if (0 == dsatetsipkt_cgeqmin_response_fmt (parse_table,
                                               &res_buff_ptr,
                                               MULTI_INDEX_READ))
    {
      DS_AT_MSG0_HIGH("Problem generating query response");
      result = DSAT_ERROR;
    }
  }
  else if (tok_ptr->op == (NA))
  {
    /* Do nothing */
    result = DSAT_OK;
  }
  else
  {
    err_info.errval = DSAT_ERR_INVALID_SYNTAX;
    goto send_error;
  }
  return result;
  
send_error:
  if( err_info.errval == DSAT_ERR_INVALID_SYNTAX)
  {
    DS_AT_MSG1_ERROR("Error:%d",err_info.errval);
  }
  else
  {
    DS_AT_MSG2_ERROR("Error:%d, arg_num:%d",err_info.errval,err_info.arg_num);
  }
  return DSAT_ERROR;

} /* dsatetsipkt_exec_cgeqmin_cmd */



/*===========================================================================

FUNCTION DSATETSIPKT_CGEQMIN_RESPONSE_FMT

DESCRIPTION
  This function generates the response for the +CGEQMIN read & test
  commands.  Data is extracted from data structure and formatted according
  to parameter flag.

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
dsat_num_item_type dsatetsipkt_cgeqmin_response_fmt
(
  const dsati_cmd_type  *parse_table,   /*  Ptr to cmd in parse table   */
  dsm_item_type * const *out_buff_ptr,  /*  Place to put response       */
  multi_index_fmt_e_type format         /*  Flag for format type        */
)
{
  byte name_buff[16];
  byte i, cntr = 0;
  int size = 0;
  int result = 0;
  dsm_item_type * res_buff_ptr = *out_buff_ptr;
  ds_umts_umts_qos_params_type pdata;
  uint32                           src_stat_desc_min = 0;
    
  /* Format the command name */
  (void)snprintf((char*)name_buff,sizeof(name_buff),
                       "%s: ", parse_table->name);
  res_buff_ptr->data_ptr[res_buff_ptr->used] = '\0';
  
  switch (format)
  {
    case MULTI_INDEX_TEST:
      /* Report each PDP type on seperate line */
      for (i=0; i <= (byte)DSAT_MAX_PDP_TYPE; i++) 
      {
        size = snprintf ((char *)res_buff_ptr->data_ptr, 
                 res_buff_ptr->size,
                 "%s%s%s\"%s\",(%d-%d),(%d-%d),(%d-%d),(%d-%d),(%d-%d),(%d-%d),(%d-%d)",
                 res_buff_ptr->data_ptr,
                 (i > 0) ? "\n" : "",
                 name_buff,
                 dsat_pdptype_valstr[i],       
                 ((dflm_type *)dsat_cgeqmin_tclass_dflm)->lower,
                 ((dflm_type *)dsat_cgeqmin_tclass_dflm)->upper,
                 ((dflm_type *)dsat_cgeqmin_req_ul_bitrate_dflm)->lower,
                 ((dflm_type *)dsat_cgeqmin_req_ul_bitrate_dflm)->upper,
                 ((dflm_type *)dsat_cgeqmin_req_dl_bitrate_dflm)->lower,
                 ((dflm_type *)dsat_cgeqmin_req_dl_bitrate_dflm)->upper,
                 ((dflm_type *)dsat_cgeqmin_gtd_ul_bitrate_dflm)->lower,
                 ((dflm_type *)dsat_cgeqmin_gtd_ul_bitrate_dflm)->upper,
                 ((dflm_type *)dsat_cgeqmin_gtd_dl_bitrate_dflm)->lower,
                 ((dflm_type *)dsat_cgeqmin_gtd_dl_bitrate_dflm)->upper,
                 ((dflm_type *)dsat_cgeqmin_dlvry_order_dflm)->lower,
                 ((dflm_type *)dsat_cgeqmin_dlvry_order_dflm)->upper,
                 ((dflm_type *)dsat_cgeqmin_max_sdu_size_dflm)->lower,
                 ((dflm_type *)dsat_cgeqmin_max_sdu_size_dflm)->upper
                );
        
        size = snprintf ((char *)res_buff_ptr->data_ptr, 
                 res_buff_ptr->size,
                 "%s,%s,%s,(%d-%d),(%d,%d-%d),(%d-%d),(0,1),(0,1)",
                 res_buff_ptr->data_ptr,
                 dsat_cgeqmin_sdu_err_tststr,
                 dsat_cgeqmin_res_biterr_tststr,
                 ((dflm_type *)dsat_cgeqmin_dlvry_err_dflm)->lower,
                 ((dflm_type *)dsat_cgeqmin_dlvry_err_dflm)->upper,
                 ((dflm_type *)dsat_cgeqmin_trans_delay_dflm)->lower,
                 ((dflm_type *)dsat_cgeqmin_trans_delay_dflm+1)->lower,
                 ((dflm_type *)dsat_cgeqmin_trans_delay_dflm+1)->upper,
                 ((dflm_type *)dsat_cgeqmin_thandle_prio_dflm)->lower,
                 ((dflm_type *)dsat_cgeqmin_thandle_prio_dflm)->upper
                );
      }
      res_buff_ptr->used = (uint16)size;
      result = size;
      break;

    case MULTI_INDEX_READ:
      /* Report each profile on seperate line */
      for (i=1; i <= DS_UMTS_MAX_PDP_PROFILE_NUM; i++) 
      {
        ds_umts_qos_tclass_enum_type traffic_class;
        ds_umts_qos_dorder_enum_type dlvry_order;
        ds_umts_qos_sdu_error_enum_type  sdu_err;
        ds_umts_qos_sdu_dlvr_enum_type   sdu_dlvr;
        ds_umts_qos_res_ber_enum_type    res_biterr;
        dsat_num_item_type tclass = 0;
        dsat_num_item_type dorder = 0;
        dsat_num_item_type sdu_err_index = 0;
        dsat_num_item_type dlvr_err_sdu = 0;
        dsat_num_item_type res_ber_index = 0;
        
        /* Query profile from EFS */
        memset((void*)&pdata, 0, sizeof(pdata));
        if ( DS_UMTS_PDP_SUCCESS !=
            ds_umts_get_pdp_profile_umts_qos_min_info_per_subs(
              SET_CID( i ),dsat_get_current_subs_id(),&pdata) )
        {
          DS_AT_MSG1_HIGH("Error CGEQMIN PDP profile read: %d",i);
          return 0;
        }
        if ( DS_UMTS_PDP_SUCCESS != 
             ds_umts_get_pdp_profile_src_stat_desc_min_info_per_subs(
                          SET_CID( i ), 
                dsat_get_current_subs_id(),&src_stat_desc_min) )
        {
          DS_AT_MSG1_HIGH("Error CGEQMIN PDP profile read: %d",i);
          return 0;
        }
        
        /* Skip reporting if profile data invalid */
        if (FALSE == pdata.valid_flg)
        {
          continue;
        }
        
        /* Perform table lookups */
        traffic_class = pdata.traffic_class;
        if (FALSE ==
            etsipkt_map_tclass(FALSE, &tclass, &traffic_class))
        {
          DS_AT_MSG1_HIGH("Error mapping TClass: %d",traffic_class);
          return 0;
        }
        
        dlvry_order = pdata.dlvry_order;
        if (FALSE ==
            etsipkt_map_dorder(FALSE, &dorder, &dlvry_order))
        {
          DS_AT_MSG1_HIGH("Error mapping DOrder: %d",dlvry_order);
          return 0;
        }
        
        sdu_err = pdata.sdu_err;
        if (FALSE ==
            etsipkt_map_sdu_err(FALSE, &sdu_err_index, &sdu_err))
        {
          DS_AT_MSG1_HIGH("Error mapping SDU error: %d",sdu_err);
          return 0;
        }
        
        sdu_dlvr = pdata.dlvr_err_sdu;
        if (FALSE ==
            etsipkt_map_dlvr_err_sdu(FALSE, &dlvr_err_sdu, &sdu_dlvr))
        {
          DS_AT_MSG1_HIGH("Error mapping Dlvr Err SDU: %d",sdu_dlvr);
          return 0;
        }

        res_biterr = pdata.res_biterr;
        if (FALSE ==
            etsipkt_map_res_ber(FALSE, &res_ber_index, &res_biterr))
        {
          DS_AT_MSG1_HIGH("Error mapping RES BER: %d",pdata.res_biterr);
          return 0;
        }
        
        /* Check to see if a new DSM item is required */
        CHECK_APPEND_RESPONSE_BUFFER ((DSM_BUFFER_THRESHOLD - 30));
        
        /* Generate response from internal storage */
        res_buff_ptr->used = (uint16)snprintf(
                 (char *)res_buff_ptr->data_ptr, 
                 res_buff_ptr->size,
                 "%s%s%s%d,%d,%lu,%lu,%lu,%lu,%d,%lu,\"%s\",\"%s\",%d,%lu,%lu",
                 res_buff_ptr->data_ptr,
                 (cntr > 0) ? "\n" : "",
                 name_buff,
                 i,
                 tclass,
                 pdata.max_ul_bitrate,       
                 pdata.max_dl_bitrate,       
                 pdata.gtd_ul_bitrate,       
                 pdata.gtd_dl_bitrate,       
                 dorder,       
                 pdata.max_sdu_size,       
                 dsat_cgeqmin_sdu_err_valstr[sdu_err_index],
                 dsat_cgeqmin_res_biterr_valstr[res_ber_index],
                 dlvr_err_sdu,
                 pdata.trans_delay,
                 pdata.thandle_prio
                );
        cntr++;
      }
      
      /* No contexts defined, just print command name */
      if (0 == cntr)
      {
        res_buff_ptr->used = (uint16)snprintf(
                       (char *)res_buff_ptr->data_ptr,
                       res_buff_ptr->size,
                       "%s%s",
                       res_buff_ptr->data_ptr,
                       name_buff);
      }
      result = res_buff_ptr->used;
      break;

    case MULTI_INDEX_AMPV:
      size = res_buff_ptr->used;
      res_buff_ptr->used = (word)snprintf(
                                   (char *)res_buff_ptr->data_ptr,
                                   res_buff_ptr->size,
                                   "%s%s",
                                   res_buff_ptr->data_ptr,
                                   name_buff);

      /* Report each profile in set notation */
      for (i=1; i <= DS_UMTS_MAX_PDP_PROFILE_NUM; i++) 
      {
        /* Local variables for PACKED structure */
        ds_umts_qos_tclass_enum_type traffic_class;
        ds_umts_qos_dorder_enum_type dlvry_order;
        ds_umts_qos_sdu_error_enum_type  sdu_err;
        ds_umts_qos_sdu_dlvr_enum_type   sdu_dlvr;
        ds_umts_qos_res_ber_enum_type    res_biterr;
        
        dsat_num_item_type tclass = 0;
        dsat_num_item_type dorder = 0;
        dsat_num_item_type sdu_err_index = 0;
        dsat_num_item_type dlvr_err_sdu = 0;
        dsat_num_item_type res_ber_index = 0;
        
        /* Query profile from EFS */
        memset((void*)&pdata, 0, sizeof(pdata));
        if ( DS_UMTS_PDP_SUCCESS !=
            ds_umts_get_pdp_profile_umts_qos_min_info_per_subs(
              SET_CID( i ), dsat_get_current_subs_id(), &pdata) )
        {
          DS_AT_MSG1_HIGH("Error CGEQMIN PDP profile read: %d",i);
          result = res_buff_ptr->used - size;
          res_buff_ptr->used -= (word)result;  /* caller increments used */
          return (dsat_num_item_type)result;
        }
        if ( DS_UMTS_PDP_SUCCESS != 
             ds_umts_get_pdp_profile_src_stat_desc_min_info_per_subs(
                          SET_CID( i ), 
                dsat_get_current_subs_id(),&src_stat_desc_min) )
        {
          DS_AT_MSG1_HIGH("Error CGEQMIN PDP profile read: %d",i);
          result = res_buff_ptr->used - size;
          res_buff_ptr->used -= (word)result;  /* caller increments used */
          return (dsat_num_item_type)result;
        }
            
        /* Skip reporting if profile data invalid */
        if (FALSE == pdata.valid_flg)
        {
          continue;
        }
        
        /* Perform table lookups */
        traffic_class = pdata.traffic_class;
        if (FALSE ==
            etsipkt_map_tclass(FALSE, &tclass, &traffic_class))
        {
          DS_AT_MSG1_HIGH("Error mapping TClass: %d",traffic_class);
          return 0;
        }
        
        dlvry_order = pdata.dlvry_order;
        if (FALSE ==
            etsipkt_map_dorder(FALSE, &dorder, &dlvry_order))
        {
          DS_AT_MSG1_HIGH("Error mapping DOrder: %d",dlvry_order);
          return 0;
        }
        
        sdu_err = pdata.sdu_err;
        if (FALSE ==
            etsipkt_map_sdu_err(FALSE, &sdu_err_index, &sdu_err))
        {
          DS_AT_MSG1_HIGH("Error mapping SDU error: %d",sdu_err);
          return 0;
        }
        
        sdu_dlvr = pdata.dlvr_err_sdu;
        if (FALSE ==
            etsipkt_map_dlvr_err_sdu(FALSE, &dlvr_err_sdu, &sdu_dlvr))
        {
          DS_AT_MSG1_HIGH("Error mapping Dlvr Err SDU: %d",sdu_dlvr);
          return 0;
        }

        res_biterr = pdata.res_biterr;
        if (FALSE ==
            etsipkt_map_res_ber(FALSE, &res_ber_index, &res_biterr))
        {
          DS_AT_MSG1_HIGH("Error mapping RES BER: %d",pdata.res_biterr);
          return 0;
        }
        
        /* Check to see if a new DSM item is required */
        CHECK_APPEND_RESPONSE_BUFFER ((DSM_BUFFER_THRESHOLD - 30));
        
        /* Generate response from internal storage */
        res_buff_ptr->used = (word)snprintf (
                 (char *)res_buff_ptr->data_ptr, 
                 res_buff_ptr->size,
                 "%s%s(%d,%d,%lu,%lu,%lu,%lu,%d,%lu,\"%s\",\"%s\",%d,%lu,%lu,%u,%d)",
                 res_buff_ptr->data_ptr,
                 (cntr > 0) ? "," : "",
                 i,
                 tclass,
                 pdata.max_ul_bitrate,       
                 pdata.max_dl_bitrate,       
                 pdata.gtd_ul_bitrate,       
                 pdata.gtd_dl_bitrate,       
                 dorder,       
                 pdata.max_sdu_size,       
                 dsat_cgeqmin_sdu_err_valstr[sdu_err_index],
                 dsat_cgeqmin_res_biterr_valstr[res_ber_index],
                 dlvr_err_sdu,
                 pdata.trans_delay,
                 pdata.thandle_prio,
                 (unsigned int)src_stat_desc_min,
                 pdata.sig_ind
                );
        cntr++;
      }

      /* Return zero if DSM item changed */
      result = (res_buff_ptr == *out_buff_ptr)?
               (res_buff_ptr->used - size) : 0;
      (*out_buff_ptr)->used -= (word)result;  /* caller increments used */
      break;
      
    default:
      DS_AT_MSG0_HIGH("Unsupported multi-index format type");
  }

  return (dsat_num_item_type)result;
}  /* dsatetsipkt_cgeqmin_response_fmt */
#endif /* FEATURE_DATA_WCDMA_PS */
#ifdef FEATURE_DSAT_LTE
/*===========================================================================
FUNCTION DSATETSIPKT_EXEC_CGEQOS_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It handles the +CGEQOS command.
  This command sets EPS QOS parameters for LTE/UMTS/GPRS QoS Profile
  (Requested) depending upon the mode.

DEPENDENCIES
  None

RETURN VALUE
  Returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success.

SIDE EFFECTS

===========================================================================*/
/*ARGSUSED*/
dsat_result_enum_type dsatetsipkt_exec_cgeqos_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type result = DSAT_OK;
  dsat_num_item_type val = 0;
  dsat_mixed_param_val_type * val_ptr = 
    (dsat_mixed_param_val_type *) dsatutil_get_val(parse_table->cmd_id,0,0,STR_TYPE);
  dsat_error_info_s_type    err_info;

  err_info.errval = DSAT_ERR_NO_ERROR;
  err_info.arg_num = 0;

  if (tok_ptr->op == (NA|EQ|AR))
  {
    /*---------------------------------------------------------------
      Processing for a command with an argument (WRITE command)
    ---------------------------------------------------------------*/

    if(( tok_ptr->args_found == 3)||( tok_ptr->args_found == 5))
    {
      err_info.errval = DSAT_ERR_INVALID_NUM_OF_ARGS;
      goto send_error;
    }

    /* Validate connection ID */
    if (DSAT_OK != etsipkt_process_param(parse_table, tok_ptr, 0, &val,1))
    {
      err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
      err_info.arg_num = 0;
      goto send_error;
    }
    
    /* Test for only CID specified */
    if (1 == tok_ptr->args_found)
    {
      /* Clear the user specified context parameters */
      if (DS_UMTS_PDP_SUCCESS !=
          ds_3gpp_reset_pdp_profile_lte_qos_info_per_subs(
            SET_CID( val ),dsat_get_current_subs_id() ))
      {
        DS_AT_MSG1_ERROR("Unable to Reset for <cid> : %d",val);
        return DSAT_ERROR;
      }
    }
    else
    {
      byte cid = (byte)val;

      
      /* Perform normal parameter processing */
      if (DSAT_OK == dsatparm_exec_param_cmd(
                                             mode,
                                             parse_table,
                                             tok_ptr,
                                             res_buff_ptr
                                            ))
      {
        /* Transfer user data to internal storage */
        ds_3gpp_lte_qos_params_type pdata;

        /* Set specified profile to defaults */
        if (DS_UMTS_PDP_SUCCESS !=
            ds_3gpp_reset_pdp_profile_lte_qos_info_per_subs(
              SET_CID( cid ),dsat_get_current_subs_id() ))
        {
          DS_AT_MSG1_ERROR("Unable to Reset for <cid> : %d",cid);
          return DSAT_ERROR;
        }
        
        /* Read the profile values from EFS */
        memset((void*)&pdata, 0, sizeof(pdata));
        if (DS_UMTS_PDP_SUCCESS !=
            ds_3gpp_get_pdp_profile_lte_qos_info_per_subs(
              SET_CID( cid ), dsat_get_current_subs_id(), &pdata))
        {
          DS_AT_MSG1_ERROR("Error CGEQOS PDP profile read: %d",cid);
          return DSAT_ERROR;
        }
        
        /* Make conditional edits to profile */
        pdata.valid_flg        = TRUE;
        CONDITIONAL_ASSIGN(1,  pdata.qci, (ds_3gpp_lte_qci_e_type)val_ptr[1].num_item);
        CONDITIONAL_ASSIGN(2,  pdata.g_dl_bit_rate,   val_ptr[2].num_item);
        CONDITIONAL_ASSIGN(3,  pdata.g_ul_bit_rate,   val_ptr[3].num_item);
        CONDITIONAL_ASSIGN(4,  pdata.max_dl_bit_rate, val_ptr[4].num_item);
        CONDITIONAL_ASSIGN(5,  pdata.max_ul_bit_rate, val_ptr[5].num_item);

        /* Write to EFS */
        if (DS_UMTS_PDP_SUCCESS !=
            ds_3gpp_set_pdp_profile_lte_qos_info_per_subs(
              SET_CID( cid ), dsat_get_current_subs_id(), &pdata))
        {
          DS_AT_MSG1_ERROR("Error CGEQOS PDP profile write: %d",cid);
          return DSAT_ERROR;
        }
      }
      else
      {
      err_info.errval = DSAT_ERR_INVALID_TOKENS;
      goto send_error;
      }
    }
  }
  else if (tok_ptr->op == (NA|EQ|QU))
  {
    /*---------------------------------------------------------------
      Processing Test command  
    ---------------------------------------------------------------*/
    if (0 == dsatetsipkt_cgeqos_response_fmt (parse_table,
                                               &res_buff_ptr,
                                               MULTI_INDEX_TEST))
    {
      DS_AT_MSG0_HIGH("Problem generating query response");
      result = DSAT_ERROR;
    }
  }
  else if (tok_ptr->op == (NA|QU))
  {
    /*---------------------------------------------------------------
       Processing Read command  
    ---------------------------------------------------------------*/
    if (0 == dsatetsipkt_cgeqos_response_fmt (parse_table,
                                               &res_buff_ptr,
                                               MULTI_INDEX_READ))
    {
      DS_AT_MSG0_HIGH("Problem generating query response");
      result = DSAT_ERROR;
    }
  }
  else if (tok_ptr->op == (NA))
  {
    /* Do nothing */
    result = DSAT_OK;
  }
  else
  {
    err_info.errval = DSAT_ERR_INVALID_SYNTAX;
    goto send_error;
  }
  return result;
  
send_error:
  if( err_info.errval == DSAT_ERR_INVALID_NUM_OF_ARGS || 
      err_info.errval == DSAT_ERR_INVALID_SYNTAX ||
      err_info.errval == DSAT_ERR_INVALID_TOKENS )
  {
    DS_AT_MSG1_ERROR("Error:%d",err_info.errval);
  }
  else
  {
    DS_AT_MSG2_ERROR("Error:%d, arg_num:%d",err_info.errval,err_info.arg_num);
  }
  return DSAT_ERROR;
}/* dsatetsipkt_exec_cgeqos_cmd */

/*===========================================================================

FUNCTION DSATETSIPKT_CGEQREQ_RESPONSE_FMT

DESCRIPTION
  This function generates the response for the +CGEQREQ read & test
  commands.  Data is extracted from data structure and formatted according
  to parameter flag.

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
dsat_num_item_type dsatetsipkt_cgeqos_response_fmt
(
  const dsati_cmd_type  *parse_table,   /*  Ptr to cmd in parse table   */
  dsm_item_type * const *out_buff_ptr,  /*  Place to put response       */
  multi_index_fmt_e_type format         /*  Flag for format type        */
)
{
  byte name_buff[16];
  byte i, cntr = 0;
  int size = 0;
  int result = 0;
  dsm_item_type * res_buff_ptr = *out_buff_ptr;
  ds_3gpp_lte_qos_params_type pdata;
    
  /* Format the command name */
  (void)snprintf((char*)name_buff, 
                       sizeof(name_buff),
                       "%s: ", parse_table->name);
  res_buff_ptr->data_ptr[res_buff_ptr->used] = '\0';
  
  switch (format)
  {
    case MULTI_INDEX_TEST:

      size = snprintf ((char *)res_buff_ptr->data_ptr, 
               res_buff_ptr->size,
               "%s%s%s (%d-%d),(%d-%d),(%d-%d),(%d-%d),(%d-%d),(%d-%d)",
               res_buff_ptr->data_ptr,
               "\n",
               name_buff,
               ((dflm_type *)dsat_pdp_cid_dflm)->lower,
               ((dflm_type *)dsat_pdp_cid_dflm)->upper,
               ((dflm_type *)dsat_qci_dflm )->lower,
               ((dflm_type *)dsat_qci_dflm)->upper,
               ((dflm_type *)dsat_cgeqos_req_dl_gbr_dflm)->lower,
               ((dflm_type *)dsat_cgeqos_req_dl_gbr_dflm)->upper,
               ((dflm_type *)dsat_cgeqos_req_ul_gbr_dflm)->lower,
               ((dflm_type *)dsat_cgeqos_req_ul_gbr_dflm)->upper,
               ((dflm_type *)dsat_cgeqos_req_dl_mbr_dflm)->lower,
               ((dflm_type *)dsat_cgeqos_req_dl_mbr_dflm)->upper,
               ((dflm_type *)dsat_cgeqos_req_ul_mbr_dflm)->lower,
               ((dflm_type *)dsat_cgeqos_req_ul_mbr_dflm)->upper
              );
      res_buff_ptr->used = (uint16)size;
      result = size;
      break;

    case MULTI_INDEX_READ:
      /* Report each profile on seperate line */
      for (i=1; i <= DS_UMTS_MAX_PDP_PROFILE_NUM; i++) 
      {
        /* Query profile from EFS */
        memset((void*)&pdata, 0, sizeof(pdata));
        if (DS_UMTS_PDP_SUCCESS !=
            ds_3gpp_get_pdp_profile_lte_qos_info_per_subs(
              SET_CID( i ), dsat_get_current_subs_id(), &pdata))
        {
          DS_AT_MSG1_ERROR("Error CGEQOS PDP profile read: %d",i);
          return 0;
        }
        
        /* Skip reporting if profile data invalid */
        if (FALSE == pdata.valid_flg)
        {
          continue;
        }
        
        
        /* Check to see if a new DSM item is required */
        CHECK_APPEND_RESPONSE_BUFFER ((DSM_BUFFER_THRESHOLD - 30));
          
        /* Generate response from internal storage */
        res_buff_ptr->used = (word)snprintf (
                        (char *)res_buff_ptr->data_ptr, 
                        res_buff_ptr->size,
                        "%s%s%s%d,%d,%lu,%lu,%lu,%lu",
                        res_buff_ptr->data_ptr,
                        (cntr > 0) ? "\n" : "",
                        name_buff,
                        i,
                        pdata.qci,
                        pdata.g_dl_bit_rate,       
                        pdata.g_ul_bit_rate,       
                        pdata.max_dl_bit_rate,       
                        pdata.max_ul_bit_rate       
                       );
        cntr++;
      }
      
      /* No contexts defined, just print command name */
      if (0 == cntr)
      {
        res_buff_ptr->used = (uint16)snprintf((char *)res_buff_ptr->data_ptr,
                       res_buff_ptr->size,
                       "%s%s",
                       res_buff_ptr->data_ptr,
                       name_buff);
      }
      result = res_buff_ptr->used;
      break;

    case MULTI_INDEX_AMPV:
      size = res_buff_ptr->used;
      res_buff_ptr->used = (word)snprintf(
                                   (char *)res_buff_ptr->data_ptr,
                                   res_buff_ptr->size,
                                   "%s%s",
                                   res_buff_ptr->data_ptr,
                                   name_buff);
      /* Report each profile in set notation */
      for (i=1; i <= DS_UMTS_MAX_PDP_PROFILE_NUM; i++) 
      {
        /* Query profile from EFS */
        memset((void*)&pdata, 0, sizeof(pdata));
        if (DS_UMTS_PDP_SUCCESS !=
            ds_3gpp_get_pdp_profile_lte_qos_info_per_subs(
              SET_CID( i ), dsat_get_current_subs_id(), &pdata))
        {
          result = res_buff_ptr->used - size;
          res_buff_ptr->used -= (word)result;  /* caller increments used */
          return (dsat_num_item_type)result;
        }
        
        /* Skip reporting if profile data invalid */
        if (FALSE == pdata.valid_flg)
        {
          continue;
        }
        
        
        /* Check to see if a new DSM item is required */
        CHECK_APPEND_RESPONSE_BUFFER ((DSM_BUFFER_THRESHOLD - 30));
          
        /* Generate response from internal storage */
        res_buff_ptr->used = (word)snprintf (
                        (char *)res_buff_ptr->data_ptr, 
                        res_buff_ptr->size,
                        "%s%s(%d,%d,%lu,%lu,%lu,%lu)",
                        res_buff_ptr->data_ptr,
                        (cntr > 0) ? "," : "",
                        i,
                        pdata.qci,
                        pdata.g_dl_bit_rate,       
                        pdata.g_ul_bit_rate,       
                        pdata.max_dl_bit_rate,       
                        pdata.max_ul_bit_rate       
                       );
        cntr++;
      }
      
      /* Return zero if DSM item changed */
      result = (res_buff_ptr == *out_buff_ptr)?
               (res_buff_ptr->used - size) : 0;
      (*out_buff_ptr)->used -= (word)result;  /* caller increments used */
      break;

      
    default:
      DS_AT_MSG0_HIGH("Unsupported multi-index format type");
  }

  return (dsat_num_item_type)result;
}  /* dsatetsipkt_cgeqos_response_fmt */
#endif /* FEATURE_DSAT_LTE */
/*===========================================================================

FUNCTION DSATETSIPKT_EXEC_CGCONTRDP_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It handles the +CGCONTRDP command.
  This command returns a set of dynamic parameters associated with the specified
  context identifier.

DEPENDENCIES
  None

RETURN VALUE
  Returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success.

SIDE EFFECTS
  None
  
===========================================================================*/
/*ARGSUSED*/
dsat_result_enum_type dsatetsipkt_exec_cgcontrdp_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type  result = DSAT_OK;

  /*---------------------------------------------------------
   Processing for a command with an argument (WRITE command)
  -----------------------------------------------------------*/
  if(tok_ptr->op == (NA|EQ|AR))
  {
    if(tok_ptr->args_found > 1)
    {
      DS_AT_MSG0_ERROR("Invalid number of Arguments in CGCONTRDP");
      result = DSAT_ERROR;
    }
    else
    {
      if( !VALID_TOKEN(0) )
      {
        /* No CID specified */
        result = etsipkt_cgcontrdp_format_response( NULL, 0,
                          tok_ptr,
                          res_buff_ptr);
      }
      else
      {
        dsat_num_item_type p_cid = 0 ;
    
        if( ATOI_OK == dsatutil_atoi (&p_cid, tok_ptr->arg[0], 10) )
        {
          /* Validate range */
          if( p_cid > DS_UMTS_MAX_PDP_PROFILE_NUM )
          {
            DS_AT_MSG1_ERROR("CID out of range value=%d",p_cid);
            result = DSAT_ERROR;
          }
        }
    
        /* Generate response */
        if( DSAT_OK == result)
        {
          result = etsipkt_cgcontrdp_format_response( (uint8 *)&p_cid, 1,
                                                    tok_ptr,
                                                    res_buff_ptr);
        }
      }
    }
  }
    /*-----------------------------------------------------------
     Processing for a command without an argument (WRITE command)
     and TEST syntax
    ------------------------------------------------------------*/
  else if( (tok_ptr->op == (NA)) ||
           (tok_ptr->op == (NA|EQ|QU)) )
  {
    result = etsipkt_cgcontrdp_format_response( NULL, 0,
                                              tok_ptr,
                                              res_buff_ptr );
  }
  else
  {
    result = DSAT_ERROR;
  }
  return result;
}/* dsatetsipkt_exec_cgcontrdp_cmd */

/*===========================================================================

FUNCTION DSATETSIPKT_EXEC_CGSCONTRDP_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It handles the +CGSCONTRDP command.
  This command returns a set of dynamic parameters associated with the specified
  context identifier.

DEPENDENCIES
  None

RETURN VALUE
  Returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success.

SIDE EFFECTS
  None
  
===========================================================================*/
/*ARGSUSED*/
dsat_result_enum_type dsatetsipkt_exec_cgscontrdp_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type  result = DSAT_OK;
  
  /*---------------------------------------------------------
   Processing for a command with an argument (WRITE command)
  -----------------------------------------------------------*/
  if(tok_ptr->op == (NA|EQ|AR))
  {
    if(tok_ptr->args_found > 1)
    {
      DS_AT_MSG0_ERROR("Invalid number of Arguments in CGSCONTRDP");
      result = DSAT_ERROR;
    }
    else
    {
      if( !VALID_TOKEN(0) )
      {
        /* No CID specified */
        result = etsipkt_cgscontrdp_format_response( NULL, 0,
                          tok_ptr,
                          res_buff_ptr);
      }
      else
      {
        dsat_num_item_type cid = 0 ;
    
        if( ATOI_OK != dsatutil_atoi (&cid, tok_ptr->arg[0], 10) )
        {
          result = DSAT_ERROR;
        }
    
        /* Generate response */
        if( DSAT_OK == result)
        {
          result = etsipkt_cgscontrdp_format_response( (uint8 *)&cid, 1,
                                                    tok_ptr,
                                                    res_buff_ptr);
        }
      }
    }
  }
    /*-----------------------------------------------------------
     Processing for a command without an argument (WRITE command)
     and TEST syntax
    ------------------------------------------------------------*/
  else if( (tok_ptr->op == (NA)) ||
            (tok_ptr->op == (NA|EQ|QU)) )
  {
    result = etsipkt_cgscontrdp_format_response( NULL, 0,
                                              tok_ptr,
                                              res_buff_ptr );
  }
  else
  {
    result = DSAT_ERROR;
  }
  return result;
}/* dsatetsipkt_exec_cgscontrdp_cmd */

/*===========================================================================

FUNCTION DSATETSIPKT_EXEC_CGEQOSRDP_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It handles the +CGEQOSRDP command.
  This command returns a set of dynamin parameters associated with the specified
  context identifier.

DEPENDENCIES
  None

RETURN VALUE
  Returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success.

SIDE EFFECTS
  None
  
===========================================================================*/
/*ARGSUSED*/
dsat_result_enum_type dsatetsipkt_exec_cgeqosrdp_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type  result = DSAT_OK;
  
  /*---------------------------------------------------------
   Processing for a command with an argument (WRITE command)
  -----------------------------------------------------------*/
  if(tok_ptr->op == (NA|EQ|AR))
  {
    if(tok_ptr->args_found > 1)
    {
      DS_AT_MSG0_ERROR("Invalid number of Arguments in CGEQOSRDP");
      result = DSAT_ERROR;
    }
    else
    {
      if( !VALID_TOKEN(0) )
      {
        /* No CID specified */
        result = etsipkt_cgeqosrdp_format_response( NULL, 0,
                                                    tok_ptr,
                                                    res_buff_ptr);
      }
      else
      {
        dsat_num_item_type cid = 0 ;
    
        if( ATOI_OK  != dsatutil_atoi (&cid, tok_ptr->arg[0], 10) )
        {
          DS_AT_MSG1_ERROR("CID out of range value=%d",cid);
          result = DSAT_ERROR;
        }
    
        /* Generate response */
        if( DSAT_OK == result)
        {
          result = etsipkt_cgeqosrdp_format_response( (uint8 *)&cid, 1,
                                                       tok_ptr,
                                                       res_buff_ptr);
        }
      }
    }
  }
    /*-----------------------------------------------------------
     Processing for a command without an argument (WRITE command)
     and TEST syntax
    ------------------------------------------------------------*/
  else if( (tok_ptr->op == (NA)) ||
           (tok_ptr->op == (NA|EQ|QU)) )
  {
    result = etsipkt_cgeqosrdp_format_response( NULL, 0,
                                                tok_ptr,
                                                res_buff_ptr );
  }
  else
  {
    result = DSAT_ERROR;
  }
  return result;

}/* dsatetsipkt_exec_cgqosrdp_cmd */

/*===========================================================================

FUNCTION DSATETSIPKT_EXEC_CGTFTRDP_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It handles the +CGTFTRDP command.
  This command returns a set of dynamin parameters associated with the specified
  context identifier.

DEPENDENCIES
  None

RETURN VALUE
  Returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success.

SIDE EFFECTS
  None
  
===========================================================================*/
/*ARGSUSED*/
dsat_result_enum_type dsatetsipkt_exec_cgtftrdp_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type  result = DSAT_OK;
  
  /*---------------------------------------------------------
   Processing for a command with an argument (WRITE command)
  -----------------------------------------------------------*/
  if(tok_ptr->op == (NA|EQ|AR))
  {
    if(tok_ptr->args_found > 1)
    {
      DS_AT_MSG0_ERROR("Invalid number of Arguments in CGTFTRDP");
      result = DSAT_ERROR;
    }
    else
    {
      if( !VALID_TOKEN(0) )
      {
        /* No CID specified */
        result = etsipkt_cgtftrdp_format_response( NULL, 0,
                                                   tok_ptr,
                                                   res_buff_ptr);
      }
      else
      {
        dsat_num_item_type cid = 0 ;
    
        if( ATOI_OK  != dsatutil_atoi (&cid, tok_ptr->arg[0], 10) )
        {
          DS_AT_MSG1_ERROR("CID out of range value=%d",cid);
          result = DSAT_ERROR;
        }
    
        /* Generate response */
        if( DSAT_OK == result)
        {
          result = etsipkt_cgtftrdp_format_response( (uint8 *)&cid, 1,
                                                      tok_ptr,
                                                      res_buff_ptr);
        }
      }
    }
  }
    /*-----------------------------------------------------------
     Processing for a command without an argument (WRITE command)
     and TEST syntax
    ------------------------------------------------------------*/
  else if( (tok_ptr->op == (NA)) ||
           (tok_ptr->op == (NA|EQ|QU)) )
  {
    result = etsipkt_cgtftrdp_format_response( NULL, 0,
                                              tok_ptr,
                                              res_buff_ptr );
  }
  else
  {
    result = DSAT_ERROR;
  }
  return result;

}/* dsatetsipkt_exec_cgtftrdp_cmd */


/*===========================================================================

FUNCTION DSATETSIPKT_EXEC_CGQREQ_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It handles the +CGQREQ command.
  This command sets PDP context parameters for GPRS QoS Profile
  (Requested).

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success.

SIDE EFFECTS
  Defining +CGQREQ parameters will undefine +CGEQREQ parameters;
  these commands are mutually exclusive.

===========================================================================*/
dsat_result_enum_type dsatetsipkt_exec_cgqreq_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type result = DSAT_OK;
  dsat_num_item_type val = 0;
  dsat_mixed_param_val_type * val_ptr = 
    (dsat_mixed_param_val_type *) dsatutil_get_val(parse_table->cmd_id,0,0,STR_TYPE);


  if (tok_ptr->op == (NA|EQ|AR))
  {
    /*---------------------------------------------------------------
      Processing for a command with an argument (WRITE command)
    ---------------------------------------------------------------*/

    /* Validate connection ID */
    if (DSAT_OK != etsipkt_process_param(parse_table, tok_ptr, 0, &val,1))
    {
      DS_AT_MSG0_HIGH("Cannot convert connection ID parameter");
      return DSAT_ERROR;
    }
    
    /* Test for only CID specified */
    if (1 == tok_ptr->args_found)
    {
      /* Clear the user specified context parameters */
      if (DS_UMTS_PDP_SUCCESS != 
          ds_umts_reset_pdp_profile_gprs_qos_req_info_per_subs(
            SET_CID( val ),dsat_get_current_subs_id() ))
      {
        DS_AT_MSG1_HIGH("Error on profile GPRS REQ undefine: %d",val);
        return DSAT_ERROR;
      }
    }
    else
    {
      byte cid = (byte)val;

      /* Perform normal parameter processing */
      if (DSAT_OK == dsatparm_exec_param_cmd(
                                             mode,
                                             parse_table,
                                             tok_ptr,
                                             res_buff_ptr
                                            ))
      {
        /* Transfer user data to internal storage */
        ds_umts_gprs_qos_params_type pdata;
          
        /* Set specified profile to defaults in both GPRS and UMTS */
        if ((DS_UMTS_PDP_SUCCESS !=
                 ds_umts_reset_pdp_profile_gprs_qos_req_info_per_subs(SET_CID( cid ),dsat_get_current_subs_id() ))||
            (DS_UMTS_PDP_SUCCESS !=
                 ds_umts_reset_pdp_profile_umts_qos_req_info_per_subs(SET_CID( cid ),dsat_get_current_subs_id() )))
        {
          DS_AT_MSG1_HIGH("Error on PDP profile to default: %d",cid);
          return DSAT_ERROR;
        }
        
        /* Query default profile from EFS */
        memset((void*)&pdata, 0, sizeof(pdata));
        if (DS_UMTS_PDP_SUCCESS !=
            ds_umts_get_pdp_profile_gprs_qos_req_info_per_subs(
              SET_CID( cid ), dsat_get_current_subs_id(),&pdata))
        {
          DS_AT_MSG1_HIGH("Error CGQREQ PDP profile read: %d",cid);
          return DSAT_ERROR;
        }
        
        /* Make conditional edits to profile */
        pdata.valid_flg     = TRUE;
        CONDITIONAL_ASSIGN(1, pdata.precedence,  val_ptr[1].num_item);
        CONDITIONAL_ASSIGN(2, pdata.delay,       val_ptr[2].num_item);
        CONDITIONAL_ASSIGN(3, pdata.reliability, val_ptr[3].num_item);
        CONDITIONAL_ASSIGN(4, pdata.peak,        val_ptr[4].num_item);
        CONDITIONAL_ASSIGN(5, pdata.mean,        val_ptr[5].num_item);

        /* Write to EFS */
        if (DS_UMTS_PDP_SUCCESS !=
            ds_umts_set_pdp_profile_gprs_qos_req_info_per_subs(
              SET_CID( cid ), dsat_get_current_subs_id(), &pdata))
        {
          DS_AT_MSG1_HIGH("Error CGQREQ PDP profile write: %d",cid);
          return DSAT_ERROR;
        }
      }
      else
      {
        result = DSAT_ERROR;
      }
    }
  }
  else if (tok_ptr->op == (NA|EQ|QU))
  {
    /*---------------------------------------------------------------
      Processing for a command to query valid parameters
    ---------------------------------------------------------------*/
    if (0 == dsatetsipkt_cgqreq_response_fmt (parse_table,
                                              &res_buff_ptr,
                                              MULTI_INDEX_TEST))
    {
      DS_AT_MSG0_HIGH("Problem generating query response");
      result = DSAT_ERROR;
    }
  }
  else if (tok_ptr->op == (NA|QU))
  {
    /*---------------------------------------------------------------
       Processing for a command to query current parameters
    ---------------------------------------------------------------*/
    if (0 == dsatetsipkt_cgqreq_response_fmt (parse_table,
                                              &res_buff_ptr,
                                              MULTI_INDEX_READ))
    {
      DS_AT_MSG0_HIGH("Problem generating query response");
      result = DSAT_ERROR;
    }
  }
  else if (tok_ptr->op == (NA))
  {
    /* Do nothing */
    result = DSAT_OK;
  }
  else
  {
    result = DSAT_ERROR;
  }
  return result;
} /* dsatetsipkt_exec_cgqreq_cmd */



/*===========================================================================

FUNCTION DSATETSIPKT_CGQREQ_RESPONSE_FMT

DESCRIPTION
  This function generates the response for the +CGQREQ read & test
  commands.  Data is extracted from data structure and formatted according
  to parameter flag.

DEPENDENCIES
  None

RETURN VALUE
  Returns number of characters in response buffer.

SIDE EFFECTS
  None

===========================================================================*/
dsat_num_item_type dsatetsipkt_cgqreq_response_fmt
(
  const dsati_cmd_type  *parse_table,   /*  Ptr to cmd in parse table   */
  dsm_item_type * const *out_buff_ptr,  /*  Place to put response       */
  multi_index_fmt_e_type format         /*  Flag for format type        */
)
{
  byte name_buff[16];
  byte i, j, cntr = 0;
  int size = 0;
  int result = 0;
  dsm_item_type * res_buff_ptr = *out_buff_ptr;
  ds_umts_gprs_qos_params_type pdata;

  /* Format the command name */
  (void)snprintf((char*)name_buff,sizeof(name_buff),
                       "%s: ", parse_table->name);
  res_buff_ptr->data_ptr[res_buff_ptr->used] = '\0';
  
  switch (format)
  {
    case MULTI_INDEX_TEST:
      /* Report each PDP type on seperate line */
      for (i=0; i <= (byte)DSAT_MAX_PDP_TYPE; i++) 
      {
        size = snprintf ((char *)res_buff_ptr->data_ptr, 
                 res_buff_ptr->size,
                 "%s%s%s\"%s\",(%d-%d),(%d-%d),(%d-%d),(%d-%d),(",
                 res_buff_ptr->data_ptr,
                 (i > 0) ? "\n" : "",
                 name_buff,
                 dsat_pdptype_valstr[i],       
                 ((dflm_type *)dsat_cgqreq_prec_dflm)->lower,
                 ((dflm_type *)dsat_cgqreq_prec_dflm)->upper,
                 ((dflm_type *)dsat_cgqreq_delay_dflm)->lower,
                 ((dflm_type *)dsat_cgqreq_delay_dflm)->upper,
                 ((dflm_type *)dsat_cgqreq_rel_dflm)->lower,
                 ((dflm_type *)dsat_cgqreq_rel_dflm)->upper,
                 ((dflm_type *)dsat_cgqreq_peak_dflm)->lower,
                 ((dflm_type *)dsat_cgqreq_peak_dflm)->upper
                );

        /* Handle disjoint mean range */
        j = 0;
        while (MAX_BROKEN_RANGE !=
                ((dflm_type *)dsat_cgqreq_mean_dflm)[j].default_v)
        {
          /* Check for single value */
          if ( ((dflm_type *)dsat_cgqreq_mean_dflm)[j].lower ==
               ((dflm_type *)dsat_cgqreq_mean_dflm)[j].upper )
          {
            size = snprintf ((char *)res_buff_ptr->data_ptr,
                            res_buff_ptr->size,
                            "%s%d,",
                            res_buff_ptr->data_ptr,
                            ((dflm_type *)dsat_cgqreq_mean_dflm)[j].lower
                           );
          }
          else
          {
            size = snprintf ((char *)res_buff_ptr->data_ptr,
                            res_buff_ptr->size,
                            "%s%d-%d,",
                            res_buff_ptr->data_ptr,
                            ((dflm_type *)dsat_cgqreq_mean_dflm)[j].lower,
                            ((dflm_type *)dsat_cgqreq_mean_dflm)[j].upper
                           );
          }
          j++;
        }
        res_buff_ptr->data_ptr[size-1] = ')';
      }
      res_buff_ptr->used = (uint16)size;
      result = size;
      break;

    case MULTI_INDEX_READ:
      /* Report each context on seperate line */
      for (i=1; i <= DS_UMTS_MAX_PDP_PROFILE_NUM; i++) 
      {
        /* Query profile from EFS */
        memset((void*)&pdata, 0, sizeof(pdata));
        if (DS_UMTS_PDP_SUCCESS !=
            ds_umts_get_pdp_profile_gprs_qos_req_info_per_subs(
              SET_CID( i ), dsat_get_current_subs_id(),&pdata))
        {
          DS_AT_MSG1_HIGH("Error CGQREQ PDP profile read: %d",i);
          return 0;
        }
        
        /* Skip reporting if profile data invalid */
        if (FALSE == pdata.valid_flg)
        {
          continue;
        }
        
        /* Generate response from internal storage */
        size = snprintf ((char *)res_buff_ptr->data_ptr,
                 res_buff_ptr->size,
                 "%s%s%s%d,%lu,%lu,%lu,%lu,%lu",
                 res_buff_ptr->data_ptr,
                 (cntr > 0) ? "\n" : "",
                 name_buff,
                 i,
                 pdata.precedence,       
                 pdata.delay,       
                 pdata.reliability,
                 pdata.peak,       
                 pdata.mean
                );
        cntr++;
      }
      
      /* No contexts defined, just print command name */
      if (0 == cntr)
      {
        size = snprintf((char *)res_buff_ptr->data_ptr,
                       res_buff_ptr->size,
                       "%s%s",
                       res_buff_ptr->data_ptr,
                       name_buff);
      }
      res_buff_ptr->used = (uint16)size;
      result = size;
      break;

    case MULTI_INDEX_AMPV:
      size = res_buff_ptr->used;
      res_buff_ptr->used = (word)snprintf(
                                   (char *)res_buff_ptr->data_ptr,
                                   res_buff_ptr->size,
                                   "%s%s",
                                   res_buff_ptr->data_ptr,
                                   name_buff);

      /* Report each context in set notation */
      for (i=1; i <= DS_UMTS_MAX_PDP_PROFILE_NUM; i++) 
      {
        /* Query profile from EFS */
        memset((void*)&pdata, 0, sizeof(pdata));
        if (DS_UMTS_PDP_SUCCESS !=
            ds_umts_get_pdp_profile_gprs_qos_req_info_per_subs(
              SET_CID( i ), dsat_get_current_subs_id(), &pdata))
        {
          DS_AT_MSG1_HIGH("Error CGQREQ PDP profile read: %d",i);
          result = res_buff_ptr->used - size;
          res_buff_ptr->used -= (word)result;  /* caller increments used */
          return (dsat_num_item_type)result;
        }
        
        /* Skip reporting if profile data invalid */
        if (FALSE == pdata.valid_flg)
        {
          continue;
        }
        
        /* Check to see if a new DSM item is required */
        CHECK_APPEND_RESPONSE_BUFFER ((DSM_BUFFER_THRESHOLD - 30));
         
        /* Generate response from internal storage */
        res_buff_ptr->used = (word)snprintf (
                 (char *)res_buff_ptr->data_ptr, 
                 res_buff_ptr->size,
                 "%s%s(%d,%lu,%lu,%lu,%lu,%lu)",
                 res_buff_ptr->data_ptr,
                 (cntr > 0) ? "," : "",
                 i,
                 pdata.precedence,       
                 pdata.delay,       
                 pdata.reliability,
                 pdata.peak,       
                 pdata.mean
                );
        cntr++;
      }

      /* Return zero if DSM item changed */
      result = (res_buff_ptr == *out_buff_ptr)?
               (res_buff_ptr->used - size) : 0;
      (*out_buff_ptr)->used -= (word)result;  /* caller increments used */
      break;
      
    default:
      DS_AT_MSG0_HIGH("Unsupported multi-index format type");
  }

  return (dsat_num_item_type)result;
} /* dsatetsipkt_cgqreq_response_fmt */



/*===========================================================================

FUNCTION DSATETSIPKT_EXEC_CGQMIN_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It handles the +CGQMIN command.
  This command sets PDP context parameters for GPRS QoS Profile
  (Minimum Acceptable).

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success.

SIDE EFFECTS
  Defining +CGQMIN parameters will undefine +CGEQREQ parameters;
  these commands are mutually exclusive.
  
===========================================================================*/
dsat_result_enum_type dsatetsipkt_exec_cgqmin_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type result = DSAT_OK;
  dsat_num_item_type val = 0;
  dsat_mixed_param_val_type * val_ptr = 
    (dsat_mixed_param_val_type *) dsatutil_get_val(parse_table->cmd_id,0,0,STR_TYPE);


  if (tok_ptr->op == (NA|EQ|AR))
  {
    /*---------------------------------------------------------------
      Processing for a command with an argument (WRITE command)
    ---------------------------------------------------------------*/

    /* Validate connection ID */
    if (DSAT_OK != etsipkt_process_param(parse_table, tok_ptr, 0, &val,1))
    {
      DS_AT_MSG0_HIGH("Cannot convert connection ID parameter");
      return DSAT_ERROR;
    }
    
    /* Test for only CID specified */
    if (1 == tok_ptr->args_found)
    {
      /* Clear the user specified context parameters */
      if (DS_UMTS_PDP_SUCCESS != 
          ds_umts_reset_pdp_profile_gprs_qos_min_info_per_subs(
            SET_CID( val ),dsat_get_current_subs_id() ))
      {
        DS_AT_MSG1_HIGH("Error on profile GPRS MIN undefine: %d",val);
        return DSAT_ERROR;
      }
    }
    else
    {
      byte cid = (byte)val;

      /* Perform normal parameter processing */
      if (DSAT_OK == dsatparm_exec_param_cmd(
                                             mode,
                                             parse_table,
                                             tok_ptr,
                                             res_buff_ptr
                                            ))
      {
        /* Transfer user data to internal storage */
        ds_umts_gprs_qos_params_type pdata;
          
        /* Set specified profile to defaults */
        if (DS_UMTS_PDP_SUCCESS !=
            ds_umts_reset_pdp_profile_gprs_qos_min_info_per_subs(
              SET_CID( cid ),dsat_get_current_subs_id() ))
        {
          DS_AT_MSG1_HIGH("Error on PDP profile to default: %d",cid);
          return DSAT_ERROR;
        }

        /* Query default profile from EFS */
        memset((void*)&pdata, 0, sizeof(pdata));
        if (DS_UMTS_PDP_SUCCESS !=
            ds_umts_get_pdp_profile_gprs_qos_min_info_per_subs(
              SET_CID( cid ), dsat_get_current_subs_id(), &pdata))
        {
          DS_AT_MSG1_HIGH("Error CGQMIN PDP profile read: %d",cid);
          return DSAT_ERROR;
        }
        
        /* Make conditional edits to profile */
        pdata.valid_flg     = TRUE;
        CONDITIONAL_ASSIGN(1, pdata.precedence,  val_ptr[1].num_item);
        CONDITIONAL_ASSIGN(2, pdata.delay,       val_ptr[2].num_item);
        CONDITIONAL_ASSIGN(3, pdata.reliability, val_ptr[3].num_item);
        CONDITIONAL_ASSIGN(4, pdata.peak,        val_ptr[4].num_item);
        CONDITIONAL_ASSIGN(5, pdata.mean,        val_ptr[5].num_item);

        /* Write to EFS */
        if (DS_UMTS_PDP_SUCCESS !=
            ds_umts_set_pdp_profile_gprs_qos_min_info_per_subs(
              SET_CID( cid ), dsat_get_current_subs_id(), &pdata))
        {
          DS_AT_MSG1_HIGH("Error CGQMIN PDP profile write: %d",cid);
          return DSAT_ERROR;
        }
      }
      else
      {
        result = DSAT_ERROR;
      }
    }
  }
  else if (tok_ptr->op == (NA|EQ|QU))
  {
    /*---------------------------------------------------------------
      Processing for a command to query valid parameters
    ---------------------------------------------------------------*/
    if (0 == dsatetsipkt_cgqmin_response_fmt (parse_table,
                                              &res_buff_ptr,
                                              MULTI_INDEX_TEST))
    {
      DS_AT_MSG0_HIGH("Problem generating query response");
      result = DSAT_ERROR;
    }
  }
  else if (tok_ptr->op == (NA|QU))
  {
    /*---------------------------------------------------------------
       Processing for a command to query current parameters
    ---------------------------------------------------------------*/
    if (0 == dsatetsipkt_cgqmin_response_fmt (parse_table,
                                              &res_buff_ptr,
                                              MULTI_INDEX_READ))
    {
      DS_AT_MSG0_HIGH("Problem generating query response");
      result = DSAT_ERROR;
    }
  }
  else if (tok_ptr->op == (NA))
  {
    /* Do nothing */
  }
  else
  {
    result = DSAT_ERROR;
  }
  return result;
} /* dsatetsipkt_exec_cgqmin_cmd */



/*===========================================================================

FUNCTION DSATETSIPKT_CGQMIN_RESPONSE_FMT

DESCRIPTION
  This function generates the response for the +CGQMIN read & test
  commands.  Data is extracted from data structure and formatted according
  to parameter flag.

DEPENDENCIES
  None

RETURN VALUE
  Returns number of characters in response buffer.

SIDE EFFECTS
  None

===========================================================================*/
dsat_num_item_type dsatetsipkt_cgqmin_response_fmt
(
  const dsati_cmd_type  *parse_table,   /*  Ptr to cmd in parse table   */
  dsm_item_type * const *out_buff_ptr,  /*  Place to put response       */
  multi_index_fmt_e_type format         /*  Flag for format type        */
)
{
  byte name_buff[16];
  byte i, j, cntr = 0;
  int size = 0;
  int result = 0;
  dsm_item_type * res_buff_ptr = *out_buff_ptr;
  ds_umts_gprs_qos_params_type pdata;

  /* Format the command name */
  (void)snprintf((char*)name_buff,
                                  sizeof(name_buff),
                                  "%s: ",
                                  parse_table->name
                                  );
  res_buff_ptr->data_ptr[res_buff_ptr->used] = '\0';
  
  switch (format)
  {
    case MULTI_INDEX_TEST:
      /* Report each PDP type on seperate line */
      for (i=0; i <= (byte)DSAT_MAX_PDP_TYPE; i++) 
      {
        size = snprintf ((char *)res_buff_ptr->data_ptr, 
                 res_buff_ptr->size,
                 "%s%s%s\"%s\",(%d-%d),(%d-%d),(%d-%d),(%d-%d),(",
                 res_buff_ptr->data_ptr,
                 (i > 0) ? "\n" : "",
                 name_buff,
                 dsat_pdptype_valstr[i],       
                 ((dflm_type *)dsat_cgqmin_prec_dflm)->lower,
                 ((dflm_type *)dsat_cgqmin_prec_dflm)->upper,
                 ((dflm_type *)dsat_cgqmin_delay_dflm)->lower,
                 ((dflm_type *)dsat_cgqmin_delay_dflm)->upper,
                 ((dflm_type *)dsat_cgqmin_rel_dflm)->lower,
                 ((dflm_type *)dsat_cgqmin_rel_dflm)->upper,
                 ((dflm_type *)dsat_cgqmin_peak_dflm)->lower,
                 ((dflm_type *)dsat_cgqmin_peak_dflm)->upper
                );

        /* Handle disjoint mean range */
        j = 0;
        while (MAX_BROKEN_RANGE !=
                ((dflm_type *)dsat_cgqmin_mean_dflm)[j].default_v)
        {
          /* Check for single value */
          if ( ((dflm_type *)dsat_cgqmin_mean_dflm)[j].lower ==
               ((dflm_type *)dsat_cgqmin_mean_dflm)[j].upper )
          {
            size = snprintf ((char *)res_buff_ptr->data_ptr, 
                            res_buff_ptr->size,
                            "%s%d,",
                            res_buff_ptr->data_ptr,
                            ((dflm_type *)dsat_cgqmin_mean_dflm)[j].lower
                           );
          }
          else
          {
            size = snprintf ((char *)res_buff_ptr->data_ptr, 
                            res_buff_ptr->size,
                            "%s%d-%d,",
                            res_buff_ptr->data_ptr,
                            ((dflm_type *)dsat_cgqmin_mean_dflm)[j].lower,
                            ((dflm_type *)dsat_cgqmin_mean_dflm)[j].upper
                           );
          }
          j++;
        }
        res_buff_ptr->data_ptr[size-1] = ')';
      }
      res_buff_ptr->used = (uint16)size;
      result = size;
      break;

    case MULTI_INDEX_READ:
      /* Report each context on seperate line */
      for (i=1; i <= DS_UMTS_MAX_PDP_PROFILE_NUM; i++) 
      {
        /* Query profile from EFS */
        memset((void*)&pdata, 0, sizeof(pdata));
        if (DS_UMTS_PDP_SUCCESS !=
            ds_umts_get_pdp_profile_gprs_qos_min_info_per_subs(
              SET_CID( i ),dsat_get_current_subs_id(),&pdata))
        {
          DS_AT_MSG1_HIGH("Error CGQMIN PDP profile read: %d",i);
          return 0;
        }
        
        /* Skip reporting if profile data invalid */
        if (FALSE == pdata.valid_flg)
        {
          continue;
        }
        
        /* Generate response from internal storage */
        size = snprintf ((char *)res_buff_ptr->data_ptr, 
                 res_buff_ptr->size,
                 "%s%s%s%d,%lu,%lu,%lu,%lu,%lu",
                 res_buff_ptr->data_ptr,
                 (cntr > 0) ? "\n" : "",
                 name_buff,
                 i,
                 pdata.precedence,       
                 pdata.delay,       
                 pdata.reliability,
                 pdata.peak,       
                 pdata.mean
                );
        cntr++;
      }
      
      /* No contexts defined, just print command name */
      if (0 == cntr)
      {
        size = snprintf((char *)res_buff_ptr->data_ptr,
                       res_buff_ptr->size,
                       "%s%s",
                       res_buff_ptr->data_ptr,
                       name_buff);
      }
      res_buff_ptr->used = (uint16)size;
      result = size;
      break;

    case MULTI_INDEX_AMPV:
      size = res_buff_ptr->used;
      res_buff_ptr->used = (word)snprintf(
                                   (char *)res_buff_ptr->data_ptr,
                                   res_buff_ptr->size,
                                   "%s%s",
                                   res_buff_ptr->data_ptr,
                                   name_buff);

      /* Report each context in set notation */
      for (i=1; i <= DS_UMTS_MAX_PDP_PROFILE_NUM; i++) 
      {
        /* Query profile from EFS */
        memset((void*)&pdata, 0, sizeof(pdata));
        if (DS_UMTS_PDP_SUCCESS !=
            ds_umts_get_pdp_profile_gprs_qos_min_info_per_subs(
              SET_CID( i ),dsat_get_current_subs_id(),&pdata))
        {
          DS_AT_MSG1_HIGH("Error CGQMIN PDP profile read: %d",i);
          result = res_buff_ptr->used - size;
          res_buff_ptr->used -= (word)result;  /* caller increments used */
          return (dsat_num_item_type)result;
        }
        
        /* Skip reporting if profile data invalid */
        if (FALSE == pdata.valid_flg)
        {
          continue;
        }
        
        /* Check to see if a new DSM item is required */
        CHECK_APPEND_RESPONSE_BUFFER ((DSM_BUFFER_THRESHOLD - 30));
         
        /* Generate response from internal storage */
        res_buff_ptr->used = (word)snprintf (
                 (char *)res_buff_ptr->data_ptr,
                 res_buff_ptr->size,
                 "%s%s(%d,%lu,%lu,%lu,%lu,%lu)",
                 res_buff_ptr->data_ptr,
                 (cntr > 0) ? "," : "",
                 i,
                 pdata.precedence,       
                 pdata.delay,       
                 pdata.reliability,
                 pdata.peak,       
                 pdata.mean
                );
        cntr++;
      }

      /* Return zero if DSM item changed */
      result = (res_buff_ptr == *out_buff_ptr)?
               (res_buff_ptr->used - size) : 0;
      (*out_buff_ptr)->used -= (word)result;  /* caller increments used */
      break;
      
    default:
      DS_AT_MSG0_HIGH("Unsupported multi-index format type");
  }

  return (dsat_num_item_type)result;
} /* dsatetsipkt_cgqmin_response_fmt */



/*===========================================================================

FUNCTION DSATETSIPKT_EXEC_QCPDPP_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It handles the $QCDPDP command.
  This command sets PDP authentication parameters.

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatetsipkt_exec_qcpdpp_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  #define MAX_QCPDPP_ENTRY_LEN (DS_UMTS_MAX_QCPDP_STRING_LEN + 20)
  dsat_result_enum_type result = DSAT_OK;
  dsm_item_type *buff_ptr = res_buff_ptr;
  byte name_buff[16];
  dsat_num_item_type val = 0;
  byte i, cntr = 0;
  dsat_mixed_param_val_type * val_ptr = 
    (dsat_mixed_param_val_type *)dsatutil_get_val(parse_table->cmd_id,0,0,STR_TYPE);
  ds_umts_pdp_auth_type pdata;
  dsat_error_info_s_type    err_info;

  err_info.errval = DSAT_ERR_NO_ERROR;
  err_info.arg_num = 0;

  if (tok_ptr->op == (NA|EQ|AR))
  {
    /*---------------------------------------------------------------
      Processing for a WRITE command 
      ---------------------------------------------------------------*/

    /* Validate connection ID */
    if (DSAT_OK != etsipkt_process_param(parse_table, tok_ptr, 0, &val,1))
    {
      err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
      err_info.arg_num = 0;
      goto send_error;
    }
    
    /* Test for only CID specified */
    if (1 == tok_ptr->args_found)
    {
      /* Clear the user specified context parameters */
      if (DS_UMTS_PDP_SUCCESS !=
          ds_umts_set_pdp_profile_auth_info_to_default_per_subs(
            SET_CID( val ),dsat_get_current_subs_id() ))
      {
        DS_AT_MSG1_HIGH("Error on profile AUTH undefine: %d",val);
        return DSAT_ERROR;
      }
    }
    else
    {
      byte user_buf[DS_UMTS_MAX_QCPDP_STRING_LEN+5];
      byte pass_buf[DS_UMTS_MAX_QCPDP_STRING_LEN+5];
      byte cid = (byte)val;
      
      /* Validate auth type and parameters */
      if (DSAT_OK != etsipkt_process_param(parse_table, tok_ptr, 1, &val,1))
      {
        err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
        err_info.arg_num = 1;
        goto send_error;
      }

      memset((void*)user_buf,0,sizeof(user_buf));
      memset((void*)pass_buf,0,sizeof(pass_buf));

      /* Validate authentication strings */
      /* For a valid AUTH, need username and password */
      if ( ((byte)DS_UMTS_PDP_AUTH_NONE != val) &&
           ((byte)DS_UMTS_PDP_AUTH_MAX != val) )
      {
        if (VALID_TOKEN(2) && (DSAT_OK !=
            etsipkt_process_param(parse_table, tok_ptr, 2,
                             pass_buf,sizeof(pass_buf))))
        {
          err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
          err_info.arg_num = 2;
          goto send_error;
        }

        if (VALID_TOKEN(3) && (DSAT_OK !=
            etsipkt_process_param(parse_table, tok_ptr, 3, 
                            user_buf,sizeof(user_buf))))
        {
          err_info.errval = DSAT_ERR_INVALID_ARGUMENT;
          err_info.arg_num = 3;
          goto send_error;
        }
        if (('\0' == pass_buf[0]) || ('\0' == user_buf[0]))
        {
          DS_AT_MSG0_HIGH("PAP auth requires username and password");
          return DSAT_ERROR;
        }
      }
      /* For NONE, no parameters allowed */
      else if ((byte)DS_UMTS_PDP_AUTH_NONE == val)
      {
        if (2 < tok_ptr->args_found)
        {
          DS_AT_MSG0_HIGH("NO auth rejects username/password");
          return DSAT_ERROR;
        }
      }
      
      /* Validate username/password string length */
      if ((DS_UMTS_MAX_QCPDP_STRING_LEN < strlen((const char *)pass_buf)) ||
          (DS_UMTS_MAX_QCPDP_STRING_LEN < strlen((const char *)user_buf)))
      {
        DS_AT_MSG1_HIGH("Username or password exceeds %d characters",
                 DS_UMTS_MAX_QCPDP_STRING_LEN);
        return DSAT_ERROR;
      }

      /* Perform normal parameter processing */
      if (DSAT_OK == dsatparm_exec_param_cmd(
                                             mode,
                                             parse_table,
                                             tok_ptr,
                                             res_buff_ptr
                                            ))
      {
        /* Transfer user data to internal storage */

        /* Set specified profile to defaults */
        if (DS_UMTS_PDP_SUCCESS !=
            ds_umts_set_pdp_profile_auth_info_to_default_per_subs(
              SET_CID( cid ),dsat_get_current_subs_id() ))
        {
          DS_AT_MSG1_HIGH("Error on AUTH profile to default: %d",cid);
          return DSAT_ERROR;
        }
      
        /* Query default profile from EFS */
        memset((void*)&pdata, 0, sizeof(pdata));
        if (DS_UMTS_PDP_SUCCESS !=
            ds_umts_get_pdp_profile_auth_info_per_subs(
              SET_CID( cid ), dsat_get_current_subs_id(), &pdata))
        {
          DS_AT_MSG1_HIGH("Error QCPDPP profile read: %d",cid);
          return DSAT_ERROR;
        }
        
        CONDITIONAL_ASSIGN(1, pdata.auth_type,(ds_umts_pdp_auth_enum_type)val_ptr[1].num_item);
        
        /* Preserve password for valid authentication */
        if (VALID_TOKEN(2))
        {
          if ( (DS_UMTS_PDP_AUTH_NONE != pdata.auth_type) &&
               (DS_UMTS_PDP_AUTH_MAX != pdata.auth_type) )
          {
            (void)strlcpy((char *)pdata.password,
                              (const char *)pass_buf,
                              DS_UMTS_MAX_QCPDP_STRING_LEN+1);
         
          
            (void)strlcpy((char *)val_ptr[2].string_item,
                              (const char *)pass_buf,
                              DS_UMTS_MAX_QCPDP_STRING_LEN+1);
           }
        }
        
        /* Preserve username for valid authentication */
        if (VALID_TOKEN(3))
        {
          if ( (DS_UMTS_PDP_AUTH_NONE != pdata.auth_type) &&
               (DS_UMTS_PDP_AUTH_MAX != pdata.auth_type) )
          {
            (void)strlcpy((char *)pdata.username, 
                          (const char *)user_buf,
                          DS_UMTS_MAX_QCPDP_STRING_LEN+1);
                     
            (void)strlcpy((char *)val_ptr[3].string_item,
                          (const char *)user_buf,
                          DS_UMTS_MAX_QCPDP_STRING_LEN+1);
           }
        }

        /* Write to EFS */
        if (DS_UMTS_PDP_SUCCESS !=
            ds_umts_set_pdp_profile_auth_info_per_subs(
              SET_CID( cid ), dsat_get_current_subs_id(), &pdata))
        {
          DS_AT_MSG1_HIGH("Error QCPDPP PDP profile write: %d",cid);
          return DSAT_ERROR;
        }
      }
      else
      {
        result = DSAT_ERROR;
      }
    }
  }
  else if (tok_ptr->op == (NA|EQ|QU))
  {
    /*---------------------------------------------------------------
      Processing for a command to TEST command
      ---------------------------------------------------------------*/
    if (DSAT_OK != dsatparm_exec_param_cmd(
                                           mode,
                                           parse_table,
                                           tok_ptr,
                                           res_buff_ptr
                                          ))
    {
      result = DSAT_ERROR;
    }
  }
  else if (tok_ptr->op == (NA|QU))
  {
    /*---------------------------------------------------------------
      Processing for a READ command 
      ---------------------------------------------------------------*/
    
    /* Format the command name */
    (void)snprintf((char*)name_buff,
                         sizeof(name_buff),
                         "%s: ",
                         parse_table->name
                         );
    buff_ptr->data_ptr[buff_ptr->used] = '\0';
    
    /* Report each profile on seperate line */
    for (i=1; i <= DS_UMTS_MAX_PDP_PROFILE_NUM; i++) 
    {
      /* Query profile from EFS */
      memset((void*)&pdata, 0, sizeof(pdata));
      if (DS_UMTS_PDP_SUCCESS !=
          ds_umts_get_pdp_profile_auth_info_per_subs(
            SET_CID( i ), dsat_get_current_subs_id(), &pdata))
      {
        DS_AT_MSG1_HIGH("Error QCPDPP profile read: %d",i);
        return DSAT_ERROR;
      }
      
      if (NULL == (buff_ptr = etsipkt_check_response_buffer(
                                buff_ptr,
                                DSM_BUFFER_THRESHOLD - MAX_QCPDPP_ENTRY_LEN)))
      { 
        return DSAT_ERROR; 
      }

      /* Generate response from internal storage (excluding password) */
      buff_ptr->used =
        (word)snprintf ((char *)buff_ptr->data_ptr, 
                       buff_ptr->size,
                       "%s%s%s%d,%d",
                       buff_ptr->data_ptr,
                       (cntr > 0) ? "\n" : "",
                       name_buff,
                       i,
                       pdata.auth_type
                       );
      
      /* Display username for valid auth type */
      if ( (DS_UMTS_PDP_AUTH_NONE != pdata.auth_type) &&
           (DS_UMTS_PDP_AUTH_MAX != pdata.auth_type) )
      {
        buff_ptr->used =
          (word)snprintf ((char *)buff_ptr->data_ptr, 
                         buff_ptr->size,
                         "%s,\"%s\"",
                         buff_ptr->data_ptr,
                         pdata.username
                         );
      }
      cntr++;
    }
  }
  else if (tok_ptr->op == (NA))
  {
    /* Do nothing */
    result = DSAT_OK;
  }
  else
  {
    err_info.errval = DSAT_ERR_INVALID_SYNTAX;
    goto send_error;
  }

  return result;

send_error:
  if( err_info.errval == DSAT_ERR_INVALID_SYNTAX)
  {
    DS_AT_MSG1_ERROR("Error:%d",err_info.errval);
  }
  else
  {
    DS_AT_MSG2_ERROR("Error:%d, arg_num:%d",err_info.errval,err_info.arg_num);
  }
  return DSAT_ERROR;
} /* dsatetsipkt_exec_qcpdpp_cmd */


/*===========================================================================

FUNCTION DSATETSIPKT_EXEC_QCPDPLT_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes at$qcpdplt command storing write value to NV.
  $qcpdplt value is used to enable/disable tolerance to long delays in PDP 
  call setup.

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_OK : if the command has been successfully executed
    DSAT_ERROR : if there was any problem in executing the command

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatetsipkt_exec_qcpdplt_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type result;
  dsat_num_item_type    temp_val; 

  char rbuffer[32];
  int ctr = 0;

  result = dsatparm_exec_param_cmd(
                                   mode,
                                   parse_table,
                                   tok_ptr,
                                   res_buff_ptr
                                  );
  
  if (DSAT_OK == result) 
  {
    /*--------------------------------------
    Processing for a WRITE command:
    ----------------------------------------*/
    if (tok_ptr->op == (NA|EQ|AR) || tok_ptr->op == (NA))
    {
      /*---------------------------------------------------------------
      Write item into the NV 
      ---------------------------------------------------------------*/
      temp_val = (dsat_num_item_type)dsatutil_get_val(DSAT_VENDOR_QCPDPLT_IDX,0,0,NUM_TYPE) ;
      ds_nv_item.long_pdpact_tolerance_for_te  = (boolean)temp_val;
      (void)dsatutil_put_nv_item( NV_LONG_PDPACT_TOLERANCE_FOR_TE_I,
                                  &ds_nv_item );
      result = DSAT_OK;
    }
    /*--------------------------------------
    Processing for a READ command:
    ----------------------------------------*/
    else if (tok_ptr->op == (NA|QU))
    {
      /* Use extended format on value query */
      ctr = snprintf ((char*)rbuffer,sizeof(rbuffer),"%s: %d", 
                            (char*)parse_table->name, 
                            (int)dsatutil_get_val(
                            DSAT_VENDOR_QCPDPLT_IDX,0,0,NUM_TYPE));

      (void) strlcpy((char*)res_buff_ptr->data_ptr,
                         (const char*)rbuffer,
                         res_buff_ptr->size);
      res_buff_ptr->used += (unsigned short)ctr;
          
      result = DSAT_OK;
    }
  }

  return result;
}  /* dsatvend_exec_qcpdplt_cmd() */


/*===========================================================================
FUNCTION DSATETSIPKT_INIT_TOLERANCE_FROM_NV

DESCRIPTION
  This function reads pdp call setup delay tolerance from the NV during 
  initialization. If this item was never written in NV, the value is set to
  QCPDPLT_DEF_VAL.
    
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
  
===========================================================================*/
void dsatetsipkt_init_tolerance_from_nv
(
  void
)
{
  nv_stat_enum_type status;

  /*-------------------------------------------------------------------------
    Get tolerance from NV. Look at the status returned. If status !=NV_DONE_S,
    then either NV was never written or it cannot be reused. 
    Set as QCPDPLT_DEF_VAL if NV was never written.
  -------------------------------------------------------------------------*/
  status = dsatutil_get_nv_item( NV_LONG_PDPACT_TOLERANCE_FOR_TE_I, &ds_nv_item );
  if ( status != NV_DONE_S )
  {
    DSATUTIL_SET_VAL(DSAT_VENDOR_QCPDPLT_IDX,0,0,0,QCPDPLT_DEF_VAL,NUM_TYPE)
  }
  else 
  {
    /*-----------------------------------------------------------------------
      Store value retrieved from NV.
    -----------------------------------------------------------------------*/
    DSATUTIL_SET_VAL(DSAT_VENDOR_QCPDPLT_IDX,0,0,0,ds_nv_item.long_pdpact_tolerance_for_te,NUM_TYPE)
  }

} /* dsatetsipkt_init_tolerance_from_nv( ) */


/*===========================================================================
FUNCTION DSATETSIPKT_RESET_TOLERANCE

DESCRIPTION
  This function resets pdp call setup delay tolerance to a default value of 
  QCPDPLT_DEF_VAL and stores the same in NV.
    
DEPENDENCIES
  None

RETURN VALUE
  None.

SIDE EFFECTS
  None
  
===========================================================================*/
void dsatetsipkt_reset_tolerance
(
  void
)
{
  /*---------------------------------------------------------------
  Set item value and write into the NV 
  ---------------------------------------------------------------*/
  DSATUTIL_SET_VAL(DSAT_VENDOR_QCPDPLT_IDX,0,0,0,QCPDPLT_DEF_VAL,NUM_TYPE)
  ds_nv_item.long_pdpact_tolerance_for_te = (boolean)QCPDPLT_DEF_VAL;
  (void)dsatutil_put_nv_item( NV_LONG_PDPACT_TOLERANCE_FOR_TE_I,
                              &ds_nv_item );
} /* dsatetsipkt_reset_tolerance( ) */

/*===========================================================================

FUNCTION DSATETSIPKT_EXEC_CNMPSD_CMD

DESCRIPTION
  This function takes the result from the command line parser and executes 
  it. It executes +CNMPSD command which releases PS signaling. This command
  indicates that no application on the MT is expected to exchange data.

DEPENDENCIES
  None
  
RETURN VALUE
  Returns an enum that describes the result of the command execution.
  Possible values:
    DSAT_ERROR : if the command is read or write OR when the default command
    execution fails.
    DSAT_OK : if it is a query command 
    DSAT_ASYNC_CMD :  successful execution of CM API cm_mm_call_cmd_ps_sig_release.

SIDE EFFECTS
  None
  
===========================================================================*/
/* ARGSUSED */
dsat_result_enum_type dsatetsipkt_exec_cnmpsd_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type result = DSAT_OK;
  if( (DSAT_MODE_WCDMA != dsatcmdp_get_current_mode()) && 
      (DSAT_MODE_TDS != dsatcmdp_get_current_mode()) )
  {
    result = dsat_send_cme_error(DSAT_CME_OP_NOT_ALLOWED);
  }
  else
  {
     if (tok_ptr->op == (NA))
     {
       SET_PENDING(DSATETSI_EXT_ACT_CNMPSD_ETSI_IDX ,0, DSAT_PENDING_TRUE);
       if ( FALSE == cm_mm_call_cmd_ps_sig_release ( dsatcmif_call_cmd_cb_func,
                                                     NULL,
                                                     dsatcm_client_id
                                                   ) )
       {
         SET_PENDING(DSATETSI_EXT_ACT_CNMPSD_ETSI_IDX ,0, DSAT_PENDING_FALSE);
         result = DSAT_ERROR; 
       }
       else
       {
         result = DSAT_ASYNC_CMD;
       }
     }
     else if (tok_ptr->op == (NA|EQ|QU))
     {
       result = DSAT_OK;
     }
     else
     {
       result = DSAT_ERROR;
     }
  }

  return result;
} /* dsatetsipkt_exec_cnmpsd_cmd */

/*===========================================================================

FUNCTION DSATETSIPKT_EXEC_CGATT_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It handles the +CGATT command.
  This command reports the connection status for network packet
  domain service.
  
DEPENDENCIES
  This function only supports read & test commands at this time.

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success.

SIDE EFFECTS
  None
  
===========================================================================*/
/* ARGSUSED */
dsat_result_enum_type dsatetsipkt_exec_cgatt_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type result = DSAT_OK;
  dsat_num_item_type    qcsimapp_val = dsat_get_qcsimapp_val();

  /*---------------------------------------------------------
    Processing for a READ & TEST command
  -----------------------------------------------------------*/
  if ((tok_ptr->op == (NA|QU)) ||
      (tok_ptr->op == (NA|EQ|QU)))
  {
    if (DSAT_OK != dsatparm_exec_param_cmd(
                                           mode,
                                           parse_table,
                                           tok_ptr,
                                           res_buff_ptr
                                          ))
    {
      result = DSAT_ERROR;
    }
  }
#ifdef FEATURE_ETSI_ATTACH
  /*---------------------------------------------------------
    Processing for a WRITE command
  -----------------------------------------------------------*/
  else if ( tok_ptr->op == (NA|EQ|AR) )
  {
    dsat_num_item_type state;
	
    /* syntax check: number of arguments should be one! */
    if (tok_ptr->args_found > 1)
    {
      return DSAT_ERROR;
    }
	
    if (ATOI_OK != dsatutil_atoi(&state, tok_ptr->arg[0], 10))
    {
      DS_AT_MSG0_HIGH("+CGATT: cannot convert state parameter");
      return DSAT_ERROR;
    }
	
    /* Attach requested */
    if ( state == 1 )
    {
      /* Ensure detached currently */ 
      if ( 0 == (dsat_num_item_type)dsatutil_get_val(
                                DSATETSI_EXT_ACT_CGATT_ETSI_IDX,qcsimapp_val,0,NUM_TYPE) )
      {
        return dsatetsicmif_attach_ps ();
      }
      else
      {
        DS_AT_MSG0_HIGH("Already packet domain attached");
      }
    }
    /* Detach requested */
    else if ( state == 0 )
    {
      /* If detached currently... */ 
      if ( 1 == (dsat_num_item_type)dsatutil_get_val(
                                  DSATETSI_EXT_ACT_CGATT_ETSI_IDX,qcsimapp_val,0,NUM_TYPE))
      {
        return dsatetsicmif_detach_ps ();
      }
      else
      {
        /* Even when already detached call CMPH function indicating user 
           preference CM_SRV_DOMAIN_PREF_PS_DETACH to local stack but 
           terminate processing on CMPH event. */
        DS_AT_MSG0_HIGH("Already packet domain detached");
        dsatcmif_ss_evt_not_expected = TRUE;

        if ( (result = dsatetsicmif_detach_ps ()) == DSAT_ERROR )
        {
          dsatcmif_ss_evt_not_expected = FALSE;
        }

        return result;
      }
    }
    else
    {
      /* Invalid value */
      return DSAT_ERROR;
    }
  }
#endif /* FEATURE_ETSI_ATTACH */
  else 
  {
    /* unsupported operation */
    result = DSAT_ERROR;
  }
  return result;
} /* dsatetsipkt_exec_cgatt_cmd */

/*===========================================================================

FUNCTION DSATETSIPKT_CGACT_PROC_CMD

DESCRIPTION
  This function does the CGACT processing for all the requested profile id's.
  
DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success.
    DSAT_ASYNC_CMD : If the response is asynchronous.
    
SIDE EFFECTS
  None
  
===========================================================================*/
dsat_result_enum_type dsatetsipkt_cgact_proc_cmd( void )
{
  dsat_result_enum_type result = DSAT_OK;
  
  /* 
     Loop over the 'dsat_pdp_cid' to activate all the entries with VALID CID.
     Primary profile entries exist from the begin of this array to the middle while 
     secondary profiles exist from end to the middle of this array !!
  */
  for( ;(dsat_pdp_state.change_index < DS_UMTS_MAX_PDP_PROFILE_NUM); )
  {
    if(dsat_pdp_cid[dsat_pdp_state.change_index] == INVALID_CID)
    {
      dsat_pdp_state.change_index++;
      continue;
    }

    dsat_pdp_state.pending = TRUE;
    result = ds_3gpp_rmsm_atif_cgact_handler(
           dsat_pdp_cid[dsat_pdp_state.change_index],
           (ds_3gpp_atif_cgact_act_mode_e_type)dsat_pdp_state.demand_state,
            NULL,
            dsat_get_current_subs_id());
    if(result == DSAT_OK)
    {
      dsat_pdp_state.pending = FALSE;
      dsat_pdp_state.change_index++;
    }
    else if(result == DSAT_ERROR)
    {
      DS_AT_MSG1_ERROR("PDP request failed for CID = %d",
        dsat_pdp_cid[dsat_pdp_state.change_index]);
      break;
    }
    else if(result == DSAT_ASYNC_CMD)
    {
      DS_AT_MSG1_HIGH("PDP request for CID %d sent to Mode handler:",
        dsat_pdp_cid[dsat_pdp_state.change_index]);
      return DSAT_ASYNC_CMD;
    }
    else
    {
      DS_AT_MSG1_ERROR("PDP request failed result code is %d ",result);
      ASSERT(0);
    }
  }
  memset(&dsat_pdp_cid[0],0,sizeof(dsat_pdp_cid));
  memset((void *)&dsat_pdp_state,0,sizeof(dsat_pdp_state));
  return result;
} /* dsatetsipkt_cgact_proc_cmd */

/*===========================================================================
FUNCTION DSATETSIPKT_AT_CMD_HANDLER

DESCRIPTION
  This function is handler function for reporting +CGACT,+CGCMOD command response.

DEPENDENCIES
  None
  
RETURN VALUE
  DSAT_ERROR : if there was any problem in executing the command
  DSAT_OK : if it is a success.

SIDE EFFECTS
  None
  
======================================================================*/
/*ARGSUSED*/
dsat_result_enum_type dsatetsipkt_at_cmd_handler
(
  dsat_mode_enum_type   mode,                /* AT command mode            */
  ds_cmd_type         * cmd_ptr              /* DS Command pointer         */
)
{
  
  dsat_result_enum_type result = DSAT_ERROR;

  if(!dsatcmdp_is_etsi_cmd())
  {
    return DSAT_ASYNC_EVENT;
  }
  
  ASSERT(cmd_ptr != NULL);

  DS_AT_MSG2_HIGH("Response for %d = %d",cmd_ptr->cmd.pdp_info.dsat_rmsm_cmd,
                  cmd_ptr->cmd.pdp_info.response);
  
  if(cmd_ptr->hdr.cmd_id != DS_AT_PDP_CMD)
  { 
    DS_AT_MSG1_ERROR(" LTE PDP command ID not found %d ",cmd_ptr->hdr.cmd_id);
    ASSERT(0);
  }

  if(cmd_ptr->cmd.pdp_info.response == DSAT_OK)
  {
    result = cmd_ptr->cmd.pdp_info.response;
  }
  else
  {
    DS_AT_MSG3_HIGH("Reason_type %d, %d net_down, %d ext_info",cmd_ptr->cmd.pdp_info.reason_type,
                       cmd_ptr->cmd.pdp_info.iface_net_down,cmd_ptr->cmd.pdp_info.ext_info_code);

    if(cmd_ptr->cmd.pdp_info.reason_type == DSAT_RMSM_IFACE_NET_DOWN_REASON)
    {
      
      switch(cmd_ptr->cmd.pdp_info.iface_net_down)
      {
        case PS_NET_DOWN_REASON_OPTION_NOT_SUPPORTED:
          result =  dsat_send_cme_error(DSAT_CME_SERV_OP_NOT_SUPPORTED);
          break;
        case PS_NET_DOWN_REASON_OPTION_TEMP_OOO:
          result = dsat_send_cme_error(DSAT_CME_SERV_OP_TEMP_OUT_OF_ORDER);
          break;
        case PS_NET_DOWN_REASON_OPTION_UNSUBSCRIBED:
          result = dsat_send_cme_error(DSAT_CME_REQ_SERV_OP_NOT_SUBSCRIBED);
          break;
        case PS_NET_DOWN_REASON_AUTH_FAILED:
          result = dsat_send_cme_error(DSAT_CME_PDP_AUTH_FAILURE);
          break;
        case PS_NET_DOWN_REASON_NO_SRV:
        case PS_NET_DOWN_REASON_NO_GW_SRV:
           result = dsat_send_cme_error(DSAT_CME_NO_NETWORK_SERVICE);
           break;
        default:
          result = dsat_send_cme_error(DSAT_CME_UNKNOWN);
        
      }
    }
    else
    {
      result = dsat_send_cme_error(DSAT_CME_UNKNOWN);
    }
  }

  return result;
}/* dsatetsipkt_at_cmd_handler */

#ifdef FEATURE_DSAT_ETSI_DATA
/*===========================================================================
FUNCTION DSATETSIPKT_CGACT_DONE_HANDLER

DESCRIPTION
  This function is called when +CGACT processing for a profile 
  is complete.

DEPENDENCIES
  None
  
RETURN VALUE
  None

SIDE EFFECTS
  None
  
======================================================================*/
void dsatetsipkt_cgact_done_handler
(
  dsat_rmsm_info_s_type  dsat_rmsm_info,
  void* user_info_ptr
)
{
  ds_cmd_type * cmd_buf;
  dsat_result_enum_type response = dsat_rmsm_info.result;

  DS_AT_MSG1_HIGH("CGACT done handler, result = %d",response);
  if(dsat_pdp_state.pending == TRUE)
  {
    dsat_pdp_state.pending = FALSE;
    if(response == DSAT_ERROR)
    {
      memset(&dsat_pdp_cid[0],0,sizeof(dsat_pdp_cid));
      memset((void*)&dsat_pdp_state,0,sizeof(dsat_pdp_state));
    }
    else if(response == DSAT_OK)
    {
      dsat_pdp_state.change_index++;
      response = dsatetsipkt_cgact_proc_cmd();
      if(response == DSAT_ASYNC_CMD)
      {
        return;
      }
      dsat_reg_atz_cb (dsatetsicmif_context_activation_atz_cb);
    }
    else
    {
      DS_AT_MSG0_ERROR("No pending command, ignore");
      return;
    }

    cmd_buf = dsat_get_cmd_buf(FALSE);
    /* send the message to ATCOP */
    cmd_buf->hdr.cmd_id = DS_AT_PDP_CMD;
    cmd_buf->cmd.pdp_info.response = response;
    cmd_buf->cmd.pdp_info.reason_type = dsat_rmsm_info.reason_type;
    cmd_buf->cmd.pdp_info.iface_net_down = dsat_rmsm_info.reason.iface_net_down;
    cmd_buf->cmd.pdp_info.ext_info_code = dsat_rmsm_info.reason.ext_info_code;
    cmd_buf->cmd.pdp_info.dsat_rmsm_cmd = DSAT_RMSM_CGACT_CMD;
    ds_put_cmd(cmd_buf);
  }
  else
  {
    DS_AT_MSG0_ERROR("No pending +CGACT command");
  }
} /* dsatetsipkt_cgact_done_handler */

/*===========================================================================
FUNCTION DSATETSIPKT_CGCMOD_DONE_HANDLER

DESCRIPTION
  This function is called when +CGCMOD processing for a profile 
  is complete.

DEPENDENCIES
  None
  
RETURN VALUE
  None

SIDE EFFECTS
  None
  
======================================================================*/
/*ARGSUSED*/
void dsatetsipkt_cgcmod_done_handler
(
  dsat_result_enum_type result,
  void* user_info_ptr
)
{
  ds_cmd_type * cmd_buf;
  dsat_result_enum_type response = result;

  DS_AT_MSG1_HIGH("CGCMOD done handler, result = %d",response);
  if(dsat_pdp_state.pending == TRUE)
  {
    dsat_pdp_state.pending = FALSE;
    if(response == DSAT_ERROR)
    {
      memset(&dsat_pdp_cid[0],0,sizeof(dsat_pdp_cid));
      memset((void*)&dsat_pdp_state,0,sizeof(dsat_pdp_state));
    }
    else if(response == DSAT_OK)
    {
      dsat_pdp_state.change_index++;
      if((dsat_pdp_state.change_index < DS_UMTS_MAX_PDP_PROFILE_NUM)&&
         (dsat_pdp_cid[dsat_pdp_state.change_index] != INVALID_CID))
      {
        response = ds_3gpp_rmsm_atif_cgcmod_handler(
               dsat_pdp_cid[dsat_pdp_state.change_index],dsat_get_current_subs_id(),
                NULL);
        if(response == DSAT_ASYNC_CMD)
        {
          dsat_pdp_state.pending = TRUE;
          return;
        }
      }
    }
    else
    {
      DS_AT_MSG0_ERROR("No pending command, ignore");
      return;
    }

    cmd_buf = dsat_get_cmd_buf(FALSE);
    /* send the message to ATCOP */
    cmd_buf->hdr.cmd_id = DS_AT_PDP_CMD;
    cmd_buf->cmd.pdp_info.response = response;
    cmd_buf->cmd.pdp_info.dsat_rmsm_cmd = DSAT_RMSM_CGCMOD_CMD;
    ds_put_cmd(cmd_buf);
  }
  else
  {
    DS_AT_MSG0_ERROR("Invalid Response");
  }
} /* dsatetsipkt_cgcmod_done_handler */
#endif /* FEATURE_DSAT_ETSI_DATA */

/*===========================================================================

FUNCTION DSATETSIPKT_EXEC_CGACT_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It handles the +CGACT command.
  This command activates or deactivates PDP context profiles.
  
DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success.

SIDE EFFECTS
  None
  
===========================================================================*/
dsat_result_enum_type dsatetsipkt_exec_cgact_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type result = DSAT_OK;
  uint8 loop;
  
  /*---------------------------------------------------------
    Processing for a READ command
  -----------------------------------------------------------*/
  if ( tok_ptr->op == (NA|QU) )
  {
      ds_3gpp_atif_profile_status_info_type profile_status;
      uint16 cntr = 0;
      uint32 length = 0;

      memset(&profile_status,0,sizeof(profile_status));

      if(DSAT_OK != ds_3gpp_rmsm_atif_cgact_query(&profile_status,dsat_get_current_subs_id()))
      {
        DS_AT_MSG0_ERROR("CGACT query failed");
        return DSAT_ERROR;
      }

      res_buff_ptr->data_ptr[res_buff_ptr->used] = '\0';
  
      for (loop=1; loop <= DS_UMTS_MAX_PDP_PROFILE_NUM; loop++) 
      {
        DS_AT_MSG1_HIGH("CGACT query executing: loop - %d ",loop);
         /* Ensure context is valid OR currently active */
        if(( profile_status.list[loop-1].valid_flag != TRUE )&&
           ( profile_status.list[loop-1].act_status != DS_3GPP_ATIF_PDP_ACTIVATED))
        {
           continue;
        }
        
        /* Generate response from data structure */
        length = snprintf ( (char *)res_buff_ptr->data_ptr,
           res_buff_ptr->size,
           "%s%s%s: %d,%d",
           res_buff_ptr->data_ptr,
           (cntr++ > 0) ? "\n" : "",
           "+CGACT",
           loop,
           (profile_status.list[loop-1].act_status == DS_3GPP_ATIF_PDP_ACTIVATED)?
           DS_3GPP_ATIF_ACTIVATION : DS_3GPP_ATIF_DEACTIVATION );
      }
           /* No contexts defined, just print command name */
      if (0 == cntr)
      {
        length = snprintf((char *)res_buff_ptr->data_ptr, 
                                res_buff_ptr->size,"%s: ", "+CGACT");
      }
      res_buff_ptr->used = (uint16)length;
      return DSAT_OK;
    }
  /*---------------------------------------------------------
    Processing for a WRITE command
  -----------------------------------------------------------*/
  else if ( tok_ptr->op == (NA|EQ|AR) )
  {
    boolean state_changed = FALSE;
    /* Build the processing order for CID list */
    if ( !etsipkt_build_context_processing_list(tok_ptr, 
                                                DSAT_CQM_CGACT_WRITE, 
                                                &state_changed) )
    {
      DS_AT_MSG0_ERROR("Error building CGACT processing order list");
      return DSAT_ERROR;
    }
    return dsatetsipkt_cgact_proc_cmd();
  }
  /*---------------------------------------------------------
    Processing for a TEST command
  -----------------------------------------------------------*/
  else if (tok_ptr->op == (NA|EQ|QU))
  {
    if (DSAT_OK != dsatparm_exec_param_cmd(
                                           mode,
                                           parse_table,
                                           tok_ptr,
                                           res_buff_ptr
                                          ))
    {
      result = DSAT_ERROR;
    }
  }
  else 
  {
    /* unsupported operation */
    result = DSAT_ERROR;
  }
  return result;
} /* dsatetsipkt_exec_cgact_cmd */

/*===========================================================================

FUNCTION DSATETSIPKT_CGACT_RESPONSE_FMT

DESCRIPTION
  This function generates +CGACT response for AMPV command  
  commands.  Data is queried from ModeHandlers and formatted according
  to parameter flag.

DEPENDENCIES
  Query to Mode Handlers.

RETURN VALUE
  Returns number of characters in response buffer.

SIDE EFFECTS
  None

===========================================================================*/
/*ARGSUSED*/
dsat_num_item_type dsatetsipkt_cgact_response_fmt
(
  const dsati_cmd_type  *parse_table,   /*  Ptr to cmd in parse table   */
  dsm_item_type * const *out_buff_ptr,  /*  Place to put response       */
  multi_index_fmt_e_type format         /*  Flag for format type        */
)
{
  byte name_buff[16] = "+CGACT";
  int size = 0;
  ds_3gpp_atif_profile_status_info_type profile_status;
  int result = 0;
  dsm_item_type * res_buff_ptr = *out_buff_ptr;
  uint8 loop;
  byte cntr = 0;

  res_buff_ptr->data_ptr[res_buff_ptr->used] = '\0';
  
  if( format != MULTI_INDEX_AMPV )
  {
    DS_AT_MSG0_HIGH("UnSupported format");
    return DSAT_ERROR;
  }

  size = res_buff_ptr->used;
  result = (word)snprintf((char *)res_buff_ptr->data_ptr,
                             res_buff_ptr->size,
                             "%s%s",
                             res_buff_ptr->data_ptr,
                             name_buff);
  memset(&profile_status,0,sizeof(profile_status));
		
  if(DSAT_OK != ds_3gpp_rmsm_atif_cgact_query(&profile_status,dsat_get_current_subs_id()))
  {
    DS_AT_MSG0_ERROR("CGACT query failed for AMPV Command");
    return DSAT_ERROR;
  }
  /* Go profile_status and add 
     CID of each active context to response list... */
  for (loop = 1; loop < DS_UMTS_MAX_PDP_PROFILE_NUM; loop++) 
  {     
 	/* Ensure context is valid OR currently active */
    if(( profile_status.list[loop-1].valid_flag == TRUE )||
             ( profile_status.list[loop-1].act_status == DS_3GPP_ATIF_PDP_ACTIVATED))
    {
      /* Check to see if a new DSM item is required */
      CHECK_APPEND_RESPONSE_BUFFER ((DSM_BUFFER_THRESHOLD - 30));
      result = (word)snprintf ((char *)res_buff_ptr->data_ptr, 
                     res_buff_ptr->size,
                     "%s%s(%d,%d)",
                     res_buff_ptr->data_ptr,
                     (cntr++ > 0) ? "," : "",
                     loop,
                     profile_status.list[loop-1].act_status
                     );
    }
      
  }
  /* caller increments res_buff_ptr->used */
  result -= size;
  DS_AT_MSG0_HIGH("CGACT - AMPV Query response");
  return (dsat_num_item_type)result;
} /* dsatetsipkt_cgact_response_fmt */

/*===========================================================================

FUNCTION DSATETSIPKT_RESET_PDP_CONTEXT_PROFILES

DESCRIPTION
  This function resets all persistent PDP context profiles to their
  default values.
  
DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR : if there was any problem in execution
    DSAT_OK : if it is a success.

SIDE EFFECTS
  None
  
===========================================================================*/
dsat_result_enum_type dsatetsipkt_reset_pdp_context_profiles ( void )
{
  byte i;
  
  /* Loop over each profile to UNDEFINE */
  for (i=1; i <= DS_UMTS_MAX_PDP_PROFILE_NUM; i++) 
  {
    /* Perform reset for specific profile */
    if (DS_UMTS_PDP_SUCCESS !=
        ds_umts_reset_pdp_profile_to_undefined_per_subs(
          SET_CID( i ),dsat_get_current_subs_id() ))
    {
      DS_AT_MSG1_HIGH("Error resetting profile: %d",i);
      return DSAT_ERROR;
    }
  }
  
  /* RESET default socket profile to default values */
  if ( DS_UMTS_PDP_SUCCESS !=
         ds_umts_set_pdp_profile_all_data_to_default_per_subs( DS_UMTS_DEF_SOCKET_PROFILE
                                                     ,dsat_get_current_subs_id()
                                                     ,DS_IP_V4
                                                    ) )
  {
    DS_AT_MSG1_HIGH("Error defaulting profile: %d",DS_UMTS_DEF_SOCKET_PROFILE);
    return DSAT_ERROR;
  }

  /* Reset the default sockets call profile number */
  if (DS_UMTS_PDP_SUCCESS !=
      ds_umts_set_pdp_profile_num_for_embedded_calls_per_subs (DS_UMTS_DEF_SOCKET_PROFILE,dsat_get_current_subs_id()))
  {
    DS_AT_MSG1_HIGH("Error setting socket profile: %d",DS_UMTS_DEF_SOCKET_PROFILE);
    return DSAT_ERROR;
  }

  return DSAT_OK;
} /* dsatetsipkt_reset_pdp_context_profiles */



/*===========================================================================

FUNCTION ETSIPKT_VALIDATE_APN

DESCRIPTION
  This function validates the format of an access point name (APN)
  per specification TS23.003 section 9.1

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success.

SIDE EFFECTS
  None
  
===========================================================================*/
LOCAL dsat_result_enum_type etsipkt_validate_apn
(
  byte *s_ptr                             /*  Points to string  */
)
{
  byte temp_buf[DS_UMTS_MAX_APN_STRING_LEN+1];
  byte c;
  byte * w_ptr = s_ptr;
  int i, cnt = 0;

  /* Do nothing with empty string */
  if ((0 == s_ptr) || ('\0' == *s_ptr))
    return DSAT_OK;

  /* Strip quotes if required */
  if ('"' == *s_ptr)
  {
    (void)dsatutil_strip_quotes_out(s_ptr, temp_buf,
                                    (word)strlen((const char*)s_ptr));
    w_ptr = temp_buf;
  }
 
  /* Process until end of string */
  while ('\0' != *(w_ptr+cnt))
  {
    c = UPCASE(*(w_ptr+cnt)); 

    /* Check for valid character */
    if ( ! ((('A' <= c ) && ('Z' >= c)) ||
            (('0' <= c ) && ('9' >= c)) ||
            ('-' == c) ||
            ('.' == c)) )
    {
      DS_AT_MSG1_HIGH("Invalid character in APN: %c", c);
      return DSAT_ERROR;
    }
    cnt++;
  }

  if ( cnt == 1 )
  {
    /* We received a single character for APN */
    /* No need for further error checks       */
    return DSAT_OK;
  }

  /* Check first and last characters */
  for (i = 0; i < cnt; i += cnt-1)
  {
    c = UPCASE(*(w_ptr+i));
    if ( ! ((('A' <= c ) && ('Z' >= c)) ||
            (('0' <= c ) && ('9' >= c))) )
    {
      DS_AT_MSG1_HIGH("Invalid begin/end character in APN: %c", c);
      return DSAT_ERROR;
    }
  }
  
  /* Check APN length < maximum (100) */
  if (DS_UMTS_MAX_APN_STRING_LEN < cnt)
  {
      DS_AT_MSG1_HIGH("APN exceeds maximum length: %d", cnt);
      return DSAT_ERROR;
  }
  
  return DSAT_OK;
} /* etsipkt_validate_apn */



/*===========================================================================

FUNCTION ETSIPKT_VALIDATE_TCLASS

DESCRIPTION
  This function validates the UMTS QoS traffic class against dependent
  parameters.  The mapping is specified in 3GPP TS 23.107 section 6.5.1

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of validation.
  possible values:
    DSAT_ERROR : if there was any problem
    DSAT_OK : if it is a success.

SIDE EFFECTS
  None
  
===========================================================================*/
/* ARGSUSED */
LOCAL dsat_result_enum_type etsipkt_validate_tclass
(
  const dsati_cmd_type *parse_table,        /* Ptr to cmd in parse table   */
  ds_umts_umts_qos_params_type * pdata_ptr  /* Ptr to Profile data */
)
{
  uint32 index;
  uint32 j = 0;
  boolean hit = FALSE;

  /* Map traffic class to dependent parameters */
  #define MAX_TCLASS_ENTRIES 4
  typedef struct 
  {
    ds_umts_qos_tclass_enum_type    traffic_class;
    ds_umts_qos_sdu_error_enum_type sdu_errors[7];
    ds_umts_qos_res_ber_enum_type   res_ber[8];
    dsat_num_item_type              min_trans_delay;
    boolean                         use_gtd_bitrate;
    boolean                         use_thandle_prio;
  } tclass_dep_s_type;
  
  tclass_dep_s_type * tclass_dep_ptr;
  static const tclass_dep_s_type traffic_class_dependency_table[] =
  {
    {
      DS_UMTS_QOS_TCLASS_CONV,
      {
        DS_UMTS_QOS_SDU_ERR_1E2, DS_UMTS_QOS_SDU_ERR_7E3, DS_UMTS_QOS_SDU_ERR_1E3,
        DS_UMTS_QOS_SDU_ERR_1E4, DS_UMTS_QOS_SDU_ERR_1E5,
        DS_UMTS_QOS_SDU_ERR_RESERVED, DS_UMTS_QOS_SDU_ERR_RESERVED
      },
      {
        DS_UMTS_QOS_RES_BER_5E2, DS_UMTS_QOS_RES_BER_1E2, DS_UMTS_QOS_RES_BER_5E3,
        DS_UMTS_QOS_RES_BER_1E3, DS_UMTS_QOS_RES_BER_1E4, DS_UMTS_QOS_RES_BER_1E5,
        DS_UMTS_QOS_RES_BER_1E6, DS_UMTS_QOS_RES_BER_RESERVED
      },
      100,
      TRUE,
      FALSE
    },
    {
      DS_UMTS_QOS_TCLASS_STRM,
      {
        DS_UMTS_QOS_SDU_ERR_1E1, DS_UMTS_QOS_SDU_ERR_1E2, DS_UMTS_QOS_SDU_ERR_7E3,
        DS_UMTS_QOS_SDU_ERR_1E3, DS_UMTS_QOS_SDU_ERR_1E4, DS_UMTS_QOS_SDU_ERR_1E5,
        DS_UMTS_QOS_SDU_ERR_RESERVED
      },
      {
        DS_UMTS_QOS_RES_BER_5E2, DS_UMTS_QOS_RES_BER_1E2, DS_UMTS_QOS_RES_BER_5E3,
        DS_UMTS_QOS_RES_BER_1E3, DS_UMTS_QOS_RES_BER_1E4, DS_UMTS_QOS_RES_BER_1E5,
        DS_UMTS_QOS_RES_BER_1E6, DS_UMTS_QOS_RES_BER_RESERVED
      },
      250,
      TRUE,
      FALSE
    },
    {
      DS_UMTS_QOS_TCLASS_INTR,
      {
        DS_UMTS_QOS_SDU_ERR_1E3, DS_UMTS_QOS_SDU_ERR_1E4, DS_UMTS_QOS_SDU_ERR_1E6,
        DS_UMTS_QOS_SDU_ERR_RESERVED, DS_UMTS_QOS_SDU_ERR_RESERVED,
        DS_UMTS_QOS_SDU_ERR_RESERVED, DS_UMTS_QOS_SDU_ERR_RESERVED
      },
      {
        DS_UMTS_QOS_RES_BER_4E3, DS_UMTS_QOS_RES_BER_1E5, DS_UMTS_QOS_RES_BER_6E8,
        DS_UMTS_QOS_RES_BER_RESERVED, DS_UMTS_QOS_RES_BER_RESERVED,
        DS_UMTS_QOS_RES_BER_RESERVED, DS_UMTS_QOS_RES_BER_RESERVED,
        DS_UMTS_QOS_RES_BER_RESERVED,
      },
      (dsat_num_item_type)0xFFFFFFFF,
      FALSE,
      TRUE
    },
    {
      DS_UMTS_QOS_TCLASS_BACK,
      {
        DS_UMTS_QOS_SDU_ERR_1E3, DS_UMTS_QOS_SDU_ERR_1E4, DS_UMTS_QOS_SDU_ERR_1E6,
        DS_UMTS_QOS_SDU_ERR_RESERVED, DS_UMTS_QOS_SDU_ERR_RESERVED,
        DS_UMTS_QOS_SDU_ERR_RESERVED, DS_UMTS_QOS_SDU_ERR_RESERVED
      },
      {
        DS_UMTS_QOS_RES_BER_4E3, DS_UMTS_QOS_RES_BER_1E5, DS_UMTS_QOS_RES_BER_6E8,
        DS_UMTS_QOS_RES_BER_RESERVED, DS_UMTS_QOS_RES_BER_RESERVED,
        DS_UMTS_QOS_RES_BER_RESERVED, DS_UMTS_QOS_RES_BER_RESERVED,
        DS_UMTS_QOS_RES_BER_RESERVED
      },
      (dsat_num_item_type)0xFFFFFFFF,
      FALSE,
      FALSE
    }
  };
  if( (parse_table == NULL)||(pdata_ptr == NULL) )
  {
     DS_AT_MSG0_HIGH("Invalid params in etsipkt_validate_tclass()");
     return DSAT_ERROR;
  }
  /* Validate dependent parameters based on traffic class */
  for (index = 0; index < MAX_TCLASS_ENTRIES; index++)
  {
    /* Find lookup table entry */
    if (pdata_ptr->traffic_class ==
        traffic_class_dependency_table[index].traffic_class)
    {
      tclass_dep_ptr = (tclass_dep_s_type*) &traffic_class_dependency_table[index];
      
      /* Verify SDU error ratio applicable to traffic class */
      j = 0;
      hit = FALSE;
      while (DS_UMTS_QOS_SDU_ERR_RESERVED != tclass_dep_ptr->sdu_errors[j])
      {
        if ((pdata_ptr->sdu_err == tclass_dep_ptr->sdu_errors[j]) ||
            (pdata_ptr->sdu_err == DS_UMTS_QOS_SDU_ERR_SUBSCRIBE))
        {
          hit = TRUE;
          break;
        }
        j++;
      }
      if (FALSE == hit)
      {
          DS_AT_MSG0_ERROR("SDU error ratio not valid for TClass");
          return DSAT_ERROR;
      }
      
      /* Verify residual BER applicable to traffic class */
      j = 0;
      hit = FALSE;
      while (DS_UMTS_QOS_RES_BER_RESERVED != tclass_dep_ptr->res_ber[j])
      {
        if ((pdata_ptr->res_biterr == tclass_dep_ptr->res_ber[j]) ||
            (pdata_ptr->res_biterr == DS_UMTS_QOS_RES_BER_SUBSCRIBE))
        {
          hit = TRUE;
          break;
        }
        j++;
      }
      if (FALSE == hit)
      {
        DS_AT_MSG0_ERROR("Residual BER not valid for TClass");
        return DSAT_ERROR;
      }
      
      /* Verify transfer delay > minimum for traffic class */
      /* As per Per 3gpp 24.008, Sec 10.5.6.5, the Transfer delay */
      /* value is ignored if the Traffic Class is Interactive class */
      /* or Background class */
      if( ( pdata_ptr->traffic_class != DS_UMTS_QOS_TCLASS_INTR ) && 
          ( pdata_ptr->traffic_class != DS_UMTS_QOS_TCLASS_BACK ) )
      {
        if ((pdata_ptr->trans_delay < tclass_dep_ptr->min_trans_delay) &&
            (0 < pdata_ptr->trans_delay))
        {
          DS_AT_MSG1_ERROR("Transfer delay not valid for %d TClass ",
                        pdata_ptr->traffic_class);
          return DSAT_ERROR;
        }
      }
      /* Verify guaranteed bitrate specified only when allowed */
      if ((FALSE == tclass_dep_ptr->use_gtd_bitrate) &&
          ((0 < pdata_ptr->gtd_ul_bitrate) ||
           (0 < pdata_ptr->gtd_dl_bitrate)))
      {
        DS_AT_MSG0_ERROR("Grntd bitrate not valid for TClass");
        return DSAT_ERROR;
      }
      break;
    }
  }

#ifdef FEATURE_DATA_TCLASS_SUBSCRITION_OVERRIDE
  /* For Traffic Class CONVERSATIONAL and STREAMING, ensure   */
  /* subscribed maximum & guaranteed UL/DL bit rates are set  */
  /* explicitly (TS 27.103 CR 111)                            */
  if ((DS_UMTS_QOS_TCLASS_CONV == pdata_ptr->traffic_class) ||
      (DS_UMTS_QOS_TCLASS_STRM == pdata_ptr->traffic_class))
  {
    mixed_def_s_type ** def_lim_pptr = 
      ( mixed_def_s_type **) parse_table->def_lim_ptr;

#define SET_MAX_IF_DEFAULT(i,param) \
    if (def_lim_pptr[i]->def->dflm.default_v == param) \
    { \
      param = def_lim_pptr[i]->def->dflm.upper; \
    } 

    /* If subscribed, set to parameter upper limit */
    SET_MAX_IF_DEFAULT(2, pdata_ptr->max_ul_bitrate);
    SET_MAX_IF_DEFAULT(3, pdata_ptr->max_dl_bitrate);
    SET_MAX_IF_DEFAULT(4, pdata_ptr->gtd_ul_bitrate);
    SET_MAX_IF_DEFAULT(5, pdata_ptr->gtd_dl_bitrate);
  }
#endif /* FEATURE_DATA_TCLASS_SUBSCRITION_OVERRIDE */

  return DSAT_OK;
} /* etsipkt_validate_tclass */



/*===========================================================================

FUNCTION ETSIPKT_PROCESS_PARAM

DESCRIPTION
  This function performs multitype parameter set processing/conversion.
  The target parameter to process is passed as an index.  The result is
  passed back to the caller by reference.
  If no string value was specified, the previous value is returned.

DEPENDENCIES
  The caller must allocate sufficient storage for string result buffer.

RETURN VALUE
  returns an enum that describes the result of validation.
  possible values:
    DSAT_ERROR : if there was any problem
    DSAT_OK : if it is a success.

SIDE EFFECTS
  None
  
===========================================================================*/
LOCAL dsat_result_enum_type etsipkt_process_param
(
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  const uint32 pindex,                  /*  Parameter index             */
  void * result_ptr,                     /*  Pointer to result           */  
  uint16 result_buff_len           /*  Length of result buffer */
)
{
  word radix;
  dsat_mixed_param_val_type * val_ptr = 
    (dsat_mixed_param_val_type *) dsatutil_get_val(parse_table->cmd_id,0,0,STR_TYPE);
  mixed_def_s_type ** def_lim_pptr = 
    ( mixed_def_s_type **) parse_table->def_lim_ptr;
  
  /* Check for user specified value */
  if ( VALID_TOKEN(pindex) &&
      ((pindex+1) <= tok_ptr->args_found))
  {
    /* Check for list attribute */
    if (0 != (def_lim_pptr[pindex]->attrib & LIST))
    {
      /* Lookup position in list */
      const def_list_type * def_list_ptr = &def_lim_pptr[pindex]->def->def_list;
	  param_srch_enum_type srchrc = 
      dsatutil_mixed_param_set_num_arg(result_ptr, def_list_ptr,
                                         tok_ptr->arg[pindex], 
                                         def_lim_pptr[pindex]->attrib);
      
      /* Continue if parameter found */
      if (PARAM_NULL != srchrc)
      {
        if(PARAM_MISS == srchrc)
        {
          DS_AT_MSG1_HIGH("Cannot find in list parameter %d",pindex);
          return DSAT_ERROR;
        }
      }
    }
    /* Check for STRING attribute */
    else if (0 != (def_lim_pptr[pindex]->attrib & STRING))
    {
      word len = (word) strlen((char*)tok_ptr->arg[pindex]);
      if( len > def_lim_pptr[pindex]->def->string_len )
      {
        DS_AT_MSG0_HIGH("The size of the parameter is greater ");
        return DSAT_ERROR;
      }
      /* if the argument is quoted, strip out the quotes */
      if ((((def_lim_pptr[pindex]->attrib & YES_QUOTE)!= 0) ||
           ((def_lim_pptr[pindex]->attrib & NO_QUOTE) != 0)) &&
          ('"' == *tok_ptr->arg[pindex]))
      {
        if(!dsatutil_strip_quotes_out((const byte*)tok_ptr->arg[pindex],
                                      (byte*)result_ptr, len))
        {
          return DSAT_ERROR;        
        }
      }
      else
      {
        (void)strlcpy ((char*)result_ptr, (const char*)tok_ptr->arg[pindex],
                           result_buff_len);
       }
    }
    /* Assume numeric otherwise */
    else 
    {
      /* Convert to number */
      radix = ( (def_lim_pptr[pindex]->attrib & HEX) ? 16 : 10);
      if (ATOI_OK != dsatutil_atoi(result_ptr, tok_ptr->arg[pindex], radix))
      {
        DS_AT_MSG1_HIGH("Cannot convert parameter %d",pindex);
        return DSAT_ERROR;
      }

      /* Check against limits */
      if ( (def_lim_pptr[pindex]->def->dflm.lower > *(uint32 *)result_ptr) ||
           (def_lim_pptr[pindex]->def->dflm.upper < *(uint32 *)result_ptr))
      {
        /* Value outside limits */
        DS_AT_MSG3_HIGH("Value outside range: %d [%d, %d]",*(int32 *)(result_ptr),
                  def_lim_pptr[pindex]->def->dflm.lower,
                  def_lim_pptr[pindex]->def->dflm.upper);
        return DSAT_ERROR;
      }
    }
  }
  else
  {
    /* Load previous value */
    if (0 != (def_lim_pptr[pindex]->attrib & STRING))
    {
      /* String values (quotes already stripped) */
      (void)strlcpy ((char*)result_ptr, (const char*)val_ptr[pindex].string_item,
                         result_buff_len);
     }
    else
    {
      /* Numeric & LIST values */
      *(dsat_num_item_type *)(result_ptr) = val_ptr[pindex].num_item;
    }
  }
  return DSAT_OK; 
} /* etsipkt_process_param */


/*===========================================================================

FUNCTION ETSIPKT_MAP_SDU_ERR

DESCRIPTION
  This function performs table lookup to map the ATCOP display index to
  the PDP context enum value for SDU errors.  

DEPENDENCIES
  None

RETURN VALUE
  returns an flag that describes the result of execution.
  possible values:
    TRUE : if lookup successful
    FALSE : if error occurred

SIDE EFFECTS
  None
  
===========================================================================*/
LOCAL boolean etsipkt_map_sdu_err
(
  const boolean  to_enum,                 /* Conversion flag  */
  dsat_num_item_type *index,              /* Value list index */
  ds_umts_qos_sdu_error_enum_type *eval   /* Enum value       */
)
{
  byte cntr = 0;

  /* Table to map between the dsat_cgeqreq_sdu_err_valstr[] array  */
  /* values for display and ds_umts_qos_sdu_error_enum_type values */
  static const struct sdu_error_conv_s
  {
    byte                             index;
    ds_umts_qos_sdu_error_enum_type  sdu_error;
  } sdu_error_conv_table[] =
  {
    {0,  DS_UMTS_QOS_SDU_ERR_SUBSCRIBE},
    {1,  DS_UMTS_QOS_SDU_ERR_1E1},
    {2,  DS_UMTS_QOS_SDU_ERR_1E2},
    {3,  DS_UMTS_QOS_SDU_ERR_7E3},
    {4,  DS_UMTS_QOS_SDU_ERR_1E3},
    {5,  DS_UMTS_QOS_SDU_ERR_1E4},
    {6,  DS_UMTS_QOS_SDU_ERR_1E5},
    {7,  DS_UMTS_QOS_SDU_ERR_1E6},
    {MAX_INDEX,  DS_UMTS_QOS_SDU_ERR_RESERVED}
  };

  while (MAX_INDEX != sdu_error_conv_table[cntr].index )
  {
    /* Convert from list index to enum value */
    if (to_enum)
    {
      if (*index == sdu_error_conv_table[cntr].index)
      {
        *eval = sdu_error_conv_table[cntr].sdu_error;
        return TRUE;
      }
    }
    /* Convert to list index from enum value */
    else
    {
      if (*eval == sdu_error_conv_table[cntr].sdu_error)
      {
        *index = sdu_error_conv_table[cntr].index;
        return TRUE;
      }
    }
    cntr++;
    if(cntr >= ARR_SIZE(sdu_error_conv_table))
    {
      DS_AT_MSG0_ERROR("Invalid Access into sdu_error_conv_table");
      return FALSE;
    }
  }
  return FALSE;
} /* etsipkt_map_sdu_err */
  

/*===========================================================================

FUNCTION ETSIPKT_MAP_RES_BER

DESCRIPTION
  This function performs table lookup to map the ATCOP display index to
  the PDP context enum value for Residual BER.  

DEPENDENCIES
  None

RETURN VALUE
  returns an flag that describes the result of execution.
  possible values:
    TRUE : if lookup successful
    FALSE : if error occurred

SIDE EFFECTS
  None
  
===========================================================================*/
LOCAL boolean etsipkt_map_res_ber
(
  const boolean  to_enum,                 /* Conversion flag  */
  dsat_num_item_type *index,              /* Value list index */
  ds_umts_qos_res_ber_enum_type *eval     /* Enum value       */
)
{
  byte cntr = 0;

  /* Table to map between the dsat_cgeqreq_res_biterr_valstr[] array  */
  /* values for display and ds_umts_qos_res_ber_enum_type values      */
  static const struct res_ber_conv_s
  {
    byte                             index;
    ds_umts_qos_res_ber_enum_type    res_ber;
  } res_ber_conv_table[] =
  {
    {0,  DS_UMTS_QOS_RES_BER_SUBSCRIBE},
    {1,  DS_UMTS_QOS_RES_BER_5E2},
    {2,  DS_UMTS_QOS_RES_BER_1E2},
    {3,  DS_UMTS_QOS_RES_BER_5E3},
    {4,  DS_UMTS_QOS_RES_BER_4E3},
    {5,  DS_UMTS_QOS_RES_BER_1E3},
    {6,  DS_UMTS_QOS_RES_BER_1E4},
    {7,  DS_UMTS_QOS_RES_BER_1E5},
    {8,  DS_UMTS_QOS_RES_BER_1E6},
    {9,  DS_UMTS_QOS_RES_BER_6E8},
    {MAX_INDEX,  DS_UMTS_QOS_RES_BER_RESERVED}
  };

  while (MAX_INDEX != res_ber_conv_table[cntr].index )
  {
    /* Convert from list index to enum value */
    if (to_enum)
    {
      if (*index == res_ber_conv_table[cntr].index)
      {
        *eval = res_ber_conv_table[cntr].res_ber;
        return TRUE;
      }
    }
    /* Convert to list index from enum value */
    else
    {
      if (*eval == res_ber_conv_table[cntr].res_ber)
      {
        *index = res_ber_conv_table[cntr].index;
        return TRUE;
      }
    }
    cntr++;
    if(cntr >= ARR_SIZE(res_ber_conv_table))
    {
      DS_AT_MSG0_ERROR("Invalid Access into res_ber_conv_table");
      return FALSE;
    }

  }
  return FALSE;
} /* etsipkt_map_res_ber */


/*===========================================================================

FUNCTION ETSIPKT_MAP_DORDER

DESCRIPTION
  This function performs table lookup to map the ATCOP display index to
  the PDP context enum value for Delivery Order.  

DEPENDENCIES
  None

RETURN VALUE
  returns an flag that describes the result of execution.
  possible values:
    TRUE : if lookup successful
    FALSE : if error occurred

SIDE EFFECTS
  None
  
===========================================================================*/
LOCAL boolean etsipkt_map_dorder
(
  const boolean  to_enum,                 /* Conversion flag  */
  dsat_num_item_type *index,              /* Value list index */
  ds_umts_qos_dorder_enum_type *eval      /* Enum value       */
)
{
  byte cntr = 0;

  /* Table to map between the dsat_cgeqreq_dlvry_order_dflm[] range  */
  /* values for display and ds_umts_qos_dorder_enum_type values      */
  static const struct dorder_conv_s
  {
    byte                            index;
    ds_umts_qos_dorder_enum_type    dorder;
  } dorder_conv_table[] =
  {
    {0,  DS_UMTS_QOS_DORDER_NO},
    {1,  DS_UMTS_QOS_DORDER_YES},
    {2,  DS_UMTS_QOS_DORDER_SUBSCRIBE},
    {MAX_INDEX,  DS_UMTS_QOS_DORDER_RESERVED}
  };

  while (MAX_INDEX != dorder_conv_table[cntr].index )
  {
    /* Convert from list index to enum value */
    if (to_enum)
    {
      if (*index == dorder_conv_table[cntr].index)
      {
        *eval = dorder_conv_table[cntr].dorder;
        return TRUE;
      }
    }
    /* Convert to list index from enum value */
    else
    {
      if (*eval == dorder_conv_table[cntr].dorder)
      {
        *index = dorder_conv_table[cntr].index;
        return TRUE;
      }
    }
    cntr++;
    if(cntr >= ARR_SIZE(dorder_conv_table))
    {
      DS_AT_MSG0_ERROR("Invalid Access into dorder_conv_table");
      return FALSE;
    }
  }
  return FALSE;
} /* etsipkt_map_dorder */


/*===========================================================================

FUNCTION ETSIPKT_MAP_DLVR_ERR_SDU

DESCRIPTION
  This function performs table lookup to map the ATCOP display index to
  the PDP context enum value for Delivery of Erroneous SDUs.  

DEPENDENCIES
  None

RETURN VALUE
  returns an flag that describes the result of execution.
  possible values:
    TRUE : if lookup successful
    FALSE : if error occurred

SIDE EFFECTS
  None
  
===========================================================================*/
LOCAL boolean etsipkt_map_dlvr_err_sdu
(
  const boolean  to_enum,                 /* Conversion flag  */
  dsat_num_item_type *index,              /* Value list index */
  ds_umts_qos_sdu_dlvr_enum_type *eval    /* Enum value       */
)
{
  byte cntr = 0;

  /* Table to map between the dsat_cgeqreq_dlvry_order_dflm[] range  */
  /* values for display and ds_umts_qos_sdu_dlvr_enum_type values    */
  static const struct sdu_dlvr_conv_s
  {
    byte                            index;
    ds_umts_qos_sdu_dlvr_enum_type  sdu_dlvr;
  } sdu_dlvr_conv_table[] =
  {
    {0,  DS_UMTS_QOS_SDU_DLVR_NO},
    {1,  DS_UMTS_QOS_SDU_DLVR_YES},
    {2,  DS_UMTS_QOS_SDU_DLVR_NODETECT},
    {3,  DS_UMTS_QOS_SDU_DLVR_SUBSCRIBE},
    {MAX_INDEX,  DS_UMTS_QOS_SDU_DLVR_RESERVED}
  };

  while (MAX_INDEX != sdu_dlvr_conv_table[cntr].index )
  {
    /* Convert from list index to enum value */
    if (to_enum)
    {
      if (*index == sdu_dlvr_conv_table[cntr].index)
      {
        *eval = sdu_dlvr_conv_table[cntr].sdu_dlvr;
        return TRUE;
      }
    }
    /* Convert to list index from enum value */
    else
    {
      if (*eval == sdu_dlvr_conv_table[cntr].sdu_dlvr)
      {
        *index = sdu_dlvr_conv_table[cntr].index;
        return TRUE;
      }
    }
    cntr++;
    if(cntr >= ARR_SIZE(sdu_dlvr_conv_table))
    {
      DS_AT_MSG0_ERROR("Invalid Access into sdu_dlvr_conv_table");
      return FALSE;
    }

  }
  return FALSE;
} /* etsipkt_map_dlvr_err_sdu */



/*===========================================================================

FUNCTION ETSIPKT_MAP_TCLASS

DESCRIPTION
  This function performs table lookup to map the ATCOP display index to
  the PDP context enum value for Traffic Class.  

DEPENDENCIES
  None

RETURN VALUE
  returns an flag that describes the result of execution.
  possible values:
    TRUE : if lookup successful
    FALSE : if error occurred

SIDE EFFECTS
  None
  
===========================================================================*/
LOCAL boolean etsipkt_map_tclass
(
  const boolean  to_enum,                 /* Conversion flag  */
  dsat_num_item_type *index,              /* Value list index */
  ds_umts_qos_tclass_enum_type *eval      /* Enum value       */
)
{
  byte cntr = 0;

  /* Table to map between the dsat_cgeqreq_tclass_dflm[] range   */
  /* values for display and ds_umts_qos_tclass_enum_type values  */
  static const struct tclass_conv_s
  {
    byte                            index;
    ds_umts_qos_tclass_enum_type    tclass;
  } tclass_conv_table[] =
  {
    {0,  DS_UMTS_QOS_TCLASS_CONV},
    {1,  DS_UMTS_QOS_TCLASS_STRM},
    {2,  DS_UMTS_QOS_TCLASS_INTR},
    {3,  DS_UMTS_QOS_TCLASS_BACK},
    {4,  DS_UMTS_QOS_TCLASS_SUBSCRIBE},
    {MAX_INDEX,  DS_UMTS_QOS_TCLASS_RESERVED}
  };

  while (MAX_INDEX != tclass_conv_table[cntr].index )
  {
    /* Convert from list index to enum value */
    if (to_enum)
    {
      if (*index == tclass_conv_table[cntr].index)
      {
        *eval = tclass_conv_table[cntr].tclass;
        return TRUE;
      }
    }
    /* Convert to list index from enum value */
    else
    {
      if (*eval == tclass_conv_table[cntr].tclass)
      {
        *index = tclass_conv_table[cntr].index;
        return TRUE;
      }
    }
    cntr++;
    if(cntr >= ARR_SIZE(tclass_conv_table))
    {
      DS_AT_MSG0_ERROR("Invalid Access into tclass_conv_table");
      return FALSE;
    }

  }
  return FALSE;
} /* etsipkt_map_tclass */

#ifdef FEATURE_SECONDARY_PDP

/*===========================================================================

FUNCTION ETSIPKT_VALIDATE_CGTFT_PARAM_COMBINATION

DESCRIPTION
  This function validates the combination of parameters given in CGTFT
  command parameters.  Returns error if illegal combination detected.
  The list of combinations is documented in 3GPP TS 23.060 section 15.3.2.

DEPENDENCIES
  None

RETURN VALUE
  returns an flag that describes the result of execution.
  possible values:
    TRUE : if validation successful
    FALSE : if error occurred

SIDE EFFECTS
  None
  
===========================================================================*/
LOCAL boolean etsipkt_validate_cgtft_param_combination
(
  const tokens_struct_type *tok_ptr /* Parameter Values */
)
{
  #define MAX_LIST_SIZE 5
  #define MAX_LISTS 2
  #define MAX_COMBINATIONS 3
  #define ALLOWED_LIST 0
  #define EXCLUDE_LIST 1

  /* Define valid conbinations based on allowed and exclusion list. */
  /* Combination is invalid if any parameter on the exclude list is */
  /* specified.  At least one paramater on the allowed list must be */
  /* specified for the parameter set to be a valid combination.     */
  struct valid_combi
  {
    byte size;
    byte list[MAX_LIST_SIZE];
  } valid_combi_table[MAX_COMBINATIONS][MAX_LISTS] =
  {
    {{ 5, {(byte)CGTFT_SA_SM, (byte)CGTFT_TOS,  (byte)CGTFT_PN_NH, 
           (byte)CGTFT_DSTP,  (byte)CGTFT_SRCP} },
     { 2, {(byte)CGTFT_SPI,   (byte)CGTFT_FLOW, (byte)CGTFT_MAX,   
           (byte)CGTFT_MAX,   (byte)CGTFT_MAX } }},
    {{ 4, {(byte)CGTFT_SA_SM, (byte)CGTFT_TOS,  (byte)CGTFT_PN_NH, 
           (byte)CGTFT_SPI,   (byte)CGTFT_MAX } },
     { 3, {(byte)CGTFT_DSTP,  (byte)CGTFT_SRCP, (byte)CGTFT_FLOW,  
           (byte)CGTFT_MAX,   (byte)CGTFT_MAX } }},
    {{ 3, {(byte)CGTFT_SA_SM, (byte)CGTFT_TOS,  (byte)CGTFT_FLOW,  
           (byte)CGTFT_MAX,   (byte)CGTFT_MAX } },
     { 4, {(byte)CGTFT_PN_NH, (byte)CGTFT_DSTP, (byte)CGTFT_SRCP,  
           (byte)CGTFT_SPI,   (byte)CGTFT_MAX } }}
  };
  boolean exclude_hit = FALSE;
  boolean allowed_hit = FALSE;
  byte i,j;
  
  ASSERT (NULL != tok_ptr);

  /* Loop over posssible combinations until an allowable one is found */
  for (i=0; ((i < MAX_COMBINATIONS) && !(allowed_hit && !exclude_hit)); i++)
  {
    /* Test against exclude list; skip if param set */
    exclude_hit = FALSE;
    for (j=0; j < valid_combi_table[i][EXCLUDE_LIST].size; j++)
    {
      if ( VALID_TOKEN( valid_combi_table[i][EXCLUDE_LIST].list[j] ) )
      {
        exclude_hit = TRUE;
        DS_AT_MSG3_MED("Param combination excluded: [%d,%d]=%d",
                 i,j,valid_combi_table[i][EXCLUDE_LIST].list[j]);
        break;
      }
    }
    
    /* Test against allowed list; one param must be set */
    allowed_hit = FALSE;
    for (j=0; !exclude_hit &&
              (j < valid_combi_table[i][ALLOWED_LIST].size); j++)
    {
      if ( VALID_TOKEN( valid_combi_table[i][ALLOWED_LIST].list[j] ) )
      {
        allowed_hit = TRUE;
        DS_AT_MSG3_MED("Param combination allowed: [%d,%d]=%d",
                 i,j,valid_combi_table[i][ALLOWED_LIST].list[j]);
        break;
      }
    }
  }

  if (!allowed_hit && !exclude_hit)
  {
    DS_AT_MSG0_HIGH("Missing mandatory TFT parameter");
  }

  return (allowed_hit && !exclude_hit);
} /* etsipkt_validate_cgtft_param_combination */



/*===========================================================================

FUNCTION ETSIPKT_GET_PRIMARY_PROFILE_LIST

DESCRIPTION
  This function generates a list string of defined primary context profile
  connection identifiers.  If no primary profiles are defined, the string
  is returned empty.  The caller must pass in the string buffer size.

DEPENDENCIES
  None

RETURN VALUE
  returns an flag that describes the result of execution.
  possible values:
    TRUE : if generation successful
    FALSE : if error occurred

SIDE EFFECTS
  None
  
===========================================================================*/
LOCAL boolean etsipkt_get_primary_profile_list
(
  byte * buf_ptr,    /* Pointer to buffer */
  byte   buf_size    /* Size of buffer */ 
)
{
  ds_umts_pdp_context_type pdata;
  boolean valid_flg;
  byte * c = buf_ptr;
  byte i;
  dsat_result_enum_type  result = DSAT_OK;

  ASSERT((NULL != buf_ptr) && (0 < buf_size));
  
  memset ((void*)buf_ptr,0,buf_size);
  
  /* Loop over all contexts */
  for (i=1; i <= DS_UMTS_MAX_PDP_PROFILE_NUM; i++) 
  {
    /* See if context profile is valid */
    IS_BASE_CONTEXT_VALID(i,result);
    if( result == DSAT_ERROR )
    {
      return FALSE;
    }

    /* Continue processing if profile data is valid */      
    if (TRUE == valid_flg)
    {
      /* Query profile data */
      memset((void*)&pdata, 0, sizeof(pdata));
      if (DS_UMTS_PDP_SUCCESS !=
          ds_umts_get_pdp_profile_context_info_per_subs(
            SET_CID( i ), dsat_get_current_subs_id(), &pdata))
      {
        DS_AT_MSG1_HIGH("Error PDP profile read: %d",i);
        return FALSE;
      }

      /* Skip further processing if secondary profile */
      if ((FALSE ==  pdata.valid_flg) || 
          (TRUE == pdata.secondary_flag))
      {
        continue;
      }

      /* Check for adequate buffer space (char + terminator) 
      assuming a max of two char + terminator so check the availability 
      of 3 bytes */
      if ( buf_size >= ((c - buf_ptr) + 3) )
      {
        /* Add primary cid to buffer */
        if ( pdata.pdp_context_number < 10 )
        {
        /* for single digit, directly convert */
          *c++ = pdata.pdp_context_number + '0';
        }
        else
        {
        /* for two digit, use the utility function */
          c = dsatutil_itoa (pdata.pdp_context_number, c, 10); 
        }
        *c++ = ',';
      }
      else
      {
        /* Indicate error in processing */
        return FALSE;
      }
    }
  }

  /* Remove last comma */
  if ( c != buf_ptr )
  {
    *(--c) = '\0';
  }

  return TRUE;
} /* etsipkt_get_primary_profile_list */




/*===========================================================================

FUNCTION ETSIPKT_VALIDATE_PORT_PARAM

DESCRIPTION
  This function converts the network port range tupple into a numeric and
  validates the range.  If conversion files or range is invalid, FALSE is
  returned to the caller.

DEPENDENCIES
  None

RETURN VALUE
  returns an flag that describes the result of execution.
  possible values:
    TRUE : if validation successful
    FALSE : if error occurred

SIDE EFFECTS
  None
  
===========================================================================*/
LOCAL boolean etsipkt_validate_port_param
(
  byte   * buf_ptr,    /* Pointer to string buffer */
  byte     buf_size,   /* Size of string buffer */ 
  uint16 * from_ptr,   /* Pointer to from port numeric equivalent */
  uint16 * to_ptr      /* Pointer to to port numeric equivalent */
)
{
  uint32 port_range;
  
  ASSERT((NULL != from_ptr) && (NULL != to_ptr) &&
         (NULL != buf_ptr) && (0 < buf_size));

  if (DSAT_OK == dsatutil_convert_tuple (STRING_TO_INT,
                                         TUPLE_TYPE_PORT,
                                         &port_range,
                                         buf_ptr,
                                         buf_size,
                                         DSAT_CONVERT_IP) )
  {
    uint16 to   = (uint16)(port_range & 0xFFFF);
    uint16 from = (uint16)(port_range >> 16);
    
    /* Ensure range is not descending */
    if (from > to )
    {
      DS_AT_MSG2_HIGH("Source from/to port range invalid: %d > %d",
                from, to);
    }
    else
    {
      *from_ptr = from;
      *to_ptr   = to;
      return TRUE;
    }
  }
  return FALSE;
} /* etsipkt_validate_port_param */


/*===========================================================================

FUNCTION ETSIPKT_FIND_LINKED_PROFILES

DESCRIPTION
  This function finds all PDP secondary context profiles linked to a
  given primary profile. The list is returned as an array and size.
  The input profile is NOT in the returned list.  The caller is
  responsible for allocating array of sufficient size.
  
DEPENDENCIES
  Input array must be of size DS_UMTS_MAX_PDP_PROFILE_NUM.

RETURN VALUE
  returns an flag that describes the result of execution.
  possible values:
    TRUE : if processing successful
    FALSE : if error occurred

SIDE EFFECTS
  None
  
===========================================================================*/
LOCAL boolean etsipkt_find_linked_profiles
(
  const byte   primary_id, /* Primary profile identifier */
  byte * size_ptr,         /* Size of output list */
  byte * list_ptr          /* List of secondary profile identifiers */
)
{
  boolean valid_flg = FALSE;
  byte i;
  ds_umts_pdp_context_type pdata;
  dsat_result_enum_type  result = DSAT_OK;
  
  ASSERT( (((primary_id > 0) && (primary_id <= DS_UMTS_MAX_PDP_PROFILE_NUM))) &&
          (NULL != size_ptr) && (NULL != list_ptr) ); 

  *size_ptr = 0;
  
  /* Loop over all profiles */
  for (i=1; i<=DS_UMTS_MAX_PDP_PROFILE_NUM; i++)
  {
    /* Query for valid context profile */
    IS_BASE_CONTEXT_VALID(i,result);
    if ( result == DSAT_ERROR )
    {
      return FALSE;
    }
    if (TRUE == valid_flg)
    {
      /* Query context profile data */
      memset((void*)&pdata, 0, sizeof(pdata));
      if (DS_UMTS_PDP_SUCCESS !=
          ds_umts_get_pdp_profile_context_info_per_subs(
            SET_CID( i ),dsat_get_current_subs_id(),&pdata))
      {
        DS_AT_MSG1_HIGH("Error PDP profile read: %d",i);
        return FALSE;
      }

      /* See if secondary is linked to given primary */
      if (pdata.primary_id == primary_id)
      {
        if (TRUE == pdata.secondary_flag)
        {
          /* Add profile to list */
          list_ptr[(*size_ptr)++] = pdata.pdp_context_number;
        }
        else
        {
          DS_AT_MSG2_ERROR("Primary profiles cannot be linked together: %d->%d",
                     pdata.primary_id, primary_id);
          return FALSE;
        }
      }
    }
  }
  return TRUE;
} /* etsipkt_find_linked_profiles */



/*===========================================================================

FUNCTION ETSIPKT_FIND_PROFILE_GROUP

DESCRIPTION
  This function generates list of all profiles in a profile group.
  The group is defined as one primary profile and all secondary
  profiles linked to the the primary.  The input profile is in the
  returned list.  The caller is responsible for allocating array of
  sufficient size.
  
DEPENDENCIES
  Input array must be of size DS_UMTS_MAX_PDP_PROFILE_NUM.

RETURN VALUE
  returns an flag that describes the result of execution.
  possible values:
    TRUE : if processing successful
    FALSE : if error occurred

SIDE EFFECTS
  None
  
===========================================================================*/
LOCAL boolean etsipkt_find_profile_group
(
  const byte  cid,         /* Profile identifier */
  byte * size_ptr,         /* Size of output list */
  byte * list_ptr          /* List of profile identifiers */
)
{
  ds_umts_pdp_context_type pdata;
  byte primary_id;
  boolean valid_flg;
  dsat_result_enum_type  result = DSAT_OK;
  
  ASSERT( (((cid > 0) && (cid <= DS_UMTS_MAX_PDP_PROFILE_NUM))) &&
          (NULL != size_ptr) && (NULL != list_ptr) );

  *size_ptr = 0;
  
  /* See if profile defined; immediately exit if not as no lookup possible */
  IS_BASE_CONTEXT_VALID(cid,result)
  if ( result == DSAT_ERROR )
  {
    return FALSE;
  }
  
  if (TRUE != valid_flg)
  {
    return TRUE;
  }
  
  /* Query profile data */
  if (DS_UMTS_PDP_SUCCESS !=
      ds_umts_get_pdp_profile_context_info_per_subs(
        SET_CID( cid ),dsat_get_current_subs_id(),&pdata))
  {
    DS_AT_MSG1_HIGH("Error PDP profile read: %d",cid);
    return FALSE;
  }

  /* Determine primary context profile */
  primary_id = (TRUE == pdata.secondary_flag)? pdata.primary_id : cid;
    
  /* Get list of all profiles mapped to same primary profile. */
  if (FALSE ==
      etsipkt_find_linked_profiles (primary_id, size_ptr, list_ptr))
  {
    DS_AT_MSG1_HIGH("Error finding linked profiles: %d",primary_id);
    return FALSE;
  }

  /* Add primary profile to group list */
  list_ptr[(*size_ptr)++] = primary_id;
  
  return TRUE;
} /* etsipkt_find_profile_group */



/*===========================================================================

FUNCTION ETSIPKT_RESET_LINKED_CONTEXT

DESCRIPTION
  This function finds secondary profiles linked to the
  passed primary context ID and resets them.
  
DEPENDENCIES
  None

RETURN VALUE
  returns an flag that describes the result of execution.
  possible values:
    TRUE : if processing successful
    FALSE : if error occurred

SIDE EFFECTS
  None
  
===========================================================================*/
LOCAL boolean etsipkt_reset_linked_context
(
  const byte   primary_id        /* Primary profile identifier */
)
{
  byte profile_list[DS_UMTS_MAX_PDP_PROFILE_NUM];
  byte size_list;

  ASSERT (((primary_id > 0) && (primary_id <= DS_UMTS_MAX_PDP_PROFILE_NUM)));
  
  if (TRUE ==
      etsipkt_find_linked_profiles (primary_id, &size_list, profile_list))
  {
    byte i;
    dsat_result_enum_type result = DSAT_OK;
    for (i=0; i<size_list; i++)
    {
      /* Clear the specified context parameters */
      RESET_BASE_CONTEXT_PARAMS(profile_list[i]);
      if( result == DSAT_ERROR)
      {
        return FALSE;
      }
    }
  }
  else
  {
    DS_AT_MSG1_HIGH("Error finding linked profiles: %d",primary_id);
    return FALSE;
  }
  
  return TRUE;
} /* etsipkt_reset_linked_context */



/*===========================================================================

FUNCTION ETSIPKT_VALIDATE_TFT_EPI_PARAM

DESCRIPTION
  This function validates the Evaluation Precedence Index (EPI) for a
  Traffic Flow Template.  The validation rule is given in 3GPP TS 27.007
  section 10.1.3.

  Must ensure the EPI is unique within all TFTs for all contexts
  having the same PDP address.  Given the address is assigned on
  primary context activation yet the address field in the primary
  profile may not be the same address assigned by the network, we
  assume each primary will have a unique address for a given APN. Then
  it is sufficuent that the EPI is unique for all TFTs mapped to given
  primary profile.

  From a secondary CID, the mapped primary is found then all profiles
  mapped to it.  From this list, all associated TFTs are read,
  checking EPI for a duplicate.
  
DEPENDENCIES
  None

RETURN VALUE
  returns an flag that describes the result of execution.
  possible values:
    TRUE : if validation successful
    FALSE : otherwise

SIDE EFFECTS
  None
  
===========================================================================*/
LOCAL boolean etsipkt_validate_tft_epi_param
(
  const byte   cid,        /* Context profile identifier */
  const byte   pfi,        /* Packet filter index        */
  const byte   epi         /* Evaluation precedence index */
)
{
  ds_umts_tft_params_type tdata;
  byte profile_list[DS_UMTS_MAX_PDP_PROFILE_NUM];
  byte size_list;
  byte i,j;
  boolean hit = FALSE;
  
  ASSERT (((cid > 0) && (cid <= DS_UMTS_MAX_PDP_PROFILE_NUM)));

  /* Get profile group for input profile. */
  if (FALSE ==
      etsipkt_find_profile_group (cid, &size_list, profile_list))
  {
    DS_AT_MSG1_HIGH("Error finding profile group: %d",cid);
    return FALSE;
  }

  /* Seach for same EPI value */
  for (i=0; ((i<size_list && i < DS_UMTS_MAX_PDP_PROFILE_NUM) && !hit); i++)
  {
    /* Scan TFTs for each profile */
    for (j=1; j<=DS_UMTS_MAX_TFT_PARAM_SETS; j++)
    {
      ds_umts_pdp_profile_status_etype rc;
      memset((void*)&tdata, 0, sizeof(tdata));

      /* Ignore TFT PFI belonging to input profile (overwrite case) */
      if ((cid == profile_list[i]) && (j == pfi))
      {
        continue;
      }
      
      /* Query TFT data */
      rc = ds_umts_get_pdp_profile_tft_info_per_subs(
              SET_CID( profile_list[i] ),dsat_get_current_subs_id(),j,&tdata);
      if (DS_UMTS_PDP_INVALID_FILTER_ID == rc)
      {
        continue;
      }
      else if (DS_UMTS_PDP_SUCCESS != rc)
      {
        DS_AT_MSG2_HIGH("Error TFT PDP profile read: %d,%d",
                  profile_list[i],j);
        return FALSE;
      }
            
      /* Test EPI for match */
      if ( (TRUE == tdata.valid_flg) &&
           (epi  == tdata.eval_prec_id) )
      {
        hit = TRUE;
        break;
      }
    } /* for */
  } /* for */

  return !hit;
  
} /* etsipkt_validate_tft_epi_param */

/*===========================================================================

FUNCTION ETSIPKT_CAL_INDEX_FRM_PFI

DESCRIPTION
   If both filter ID filled and New Filter ID doesn't match to set 
   Filter ids then return INVALID INDEX else return VALID INDEX .
  
DEPENDENCIES
  None

RETURN VALUE
  returns an INDEX
    VALID INDEX (1 or 2) : if validation successful
    INVALID INDEX (3)    : otherwise

SIDE EFFECTS
  None
  
===========================================================================*/

LOCAL byte etsipkt_cal_index_frm_pfi 
(
  const byte   cid,        /* Context profile identifier */
  const byte   pfi        /* Packet filter identifier */
)
{
  #define PFI_INVALID_INDEX  3
  
  ds_umts_tft_params_type temp_data;
  ds_umts_pdp_profile_status_etype return_code;
  byte filter_index = 0;
  byte filter_idx[DS_UMTS_MAX_TFT_PARAM_SETS] = {PFI_INVALID_INDEX ,
                                                 PFI_INVALID_INDEX};
  
  memset((void*)&temp_data, 0, sizeof(temp_data));
  
  for(filter_index = 1; filter_index <= DS_UMTS_MAX_TFT_PARAM_SETS ; filter_index++)
  {
    return_code = ds_umts_get_pdp_profile_tft_info_per_subs(
                            SET_CID( cid ),dsat_get_current_subs_id(),filter_index ,&temp_data);
    
    if (DS_UMTS_PDP_INVALID_FILTER_ID == return_code)
    {
      DS_AT_MSG2_HIGH(" Empty index found PFI %d Index %d",pfi,filter_index);
      filter_idx[filter_index -1]=  filter_index ;
    }
    else if( DS_UMTS_PDP_SUCCESS == return_code) 
    { 
      DS_AT_MSG3_HIGH("Filter Id  PFI %d Index %d Flag %d",pfi,filter_index,temp_data.valid_flg);
      if( temp_data.valid_flg == TRUE )
      {
        if(temp_data.filter_id == pfi)
        {
          return filter_index;
        }
      }
      else
      {
        filter_idx[filter_index -1]=  filter_index ;
      }
    }
    else if (DS_UMTS_PDP_SUCCESS != return_code)
    {
      DS_AT_MSG2_ERROR("Error TFT PDP profile read: %d,%d",cid,filter_index);
      return PFI_INVALID_INDEX;
    }
  }
  if(filter_idx[0] !=  PFI_INVALID_INDEX)
  {
    return filter_idx[0];
  }
  if(filter_idx[1] !=  PFI_INVALID_INDEX)
  {
    return filter_idx[1];
  }
  return PFI_INVALID_INDEX;
}/* etsipkt_cal_index_frm_pfi */
#endif /* FEATURE_SECONDARY_PDP */

/*===========================================================================

FUNCTION  ETSIPKT_CHECK_RESPONSE_BUFFER

DESCRIPTION
  This function checks the lenth of the response buffer to see if
  another must be chained to it.  A buffer limit value of zero means
  use normal DSM buffer default.

DEPENDENCIES
  None

RETURN VALUE
  returns pointer to current DSM item buffer on success; NULL otherwise

SIDE EFFECTS
  DSM item may be chained to passed in buffer, moving input pointer
  to new item.
  
===========================================================================*/
LOCAL dsm_item_type* etsipkt_check_response_buffer
(
  dsm_item_type  *res_buff_ptr,   /* Response buffer */
  uint16          buf_limit       /* Buffer contents limit */
)
{
  dsm_item_type  *cur_buff_ptr = res_buff_ptr;

  ASSERT (NULL != res_buff_ptr);

  /* Use default DSM packet limit if necessary */
  if (0 == buf_limit)
  {
    buf_limit = DSM_BUFFER_THRESHOLD;
  }
  
  /* Append new buffer if required. */
  cur_buff_ptr = dsatutil_append_dsm_item( cur_buff_ptr,
                                           buf_limit );
  /* If buffer appended, ensure null terminated for string functions. */
  if ( cur_buff_ptr != res_buff_ptr)
  {
    cur_buff_ptr->data_ptr[cur_buff_ptr->used]   = '\0';
  }

  return cur_buff_ptr;
} /* etsipkt_check_response_buffer */


/*===========================================================================

FUNCTION  ETSIPKT_BUILD_CONTEXT_PROCESSING_LIST

DESCRIPTION
  This function builds the processing order for +CGACT and +CGCMOD list of 
  context profiles.  The order of activation is all primary contexts before
  any secondary.  This avoids unnecessary failures should a dependent
  secondary context attempt activation before the primary yet both are
  in user specified list. 
  *state_changed_ptr only set when type_list is DSAT_CQM_CGACT_WRITE.
   
DEPENDENCIES
  None

RETURN VALUE
  returns an flag that describes the result of execution.
  possible values:
    TRUE : if processing successful 
    FALSE : otherwise

SIDE EFFECTS
  None

===========================================================================*/
LOCAL boolean etsipkt_build_context_processing_list
(
  const tokens_struct_type *tok_ptr,     /* Command tokens from parser   */
  dsat_call_query_mode_e_type type_list, /* For which command */
  boolean * state_changed_ptr            /* Pointer to state change flag */
)
{
  byte ctxlist[DS_UMTS_MAX_PDP_PROFILE_NUM];
  byte *pri_ptr = ctxlist;
  boolean no_cid_list;
  byte i = 0;
  byte cid_limit = 0;
#ifdef FEATURE_SECONDARY_PDP
  byte *sec_ptr = &ctxlist[DS_UMTS_MAX_PDP_PROFILE_NUM-1];
#endif /* FEATURE_SECONDARY_PDP */
  boolean is_activation = FALSE;
  dsat_num_item_type demand_state = (dsat_num_item_type)DSAT_PDP_CONNECT_MAX;
  ASSERT (NULL != tok_ptr);

  switch ( type_list )
  {
  case DSAT_CQM_CGACT_WRITE:
    *state_changed_ptr = FALSE;
    /* For +CGACT write command the first parm is state */
    no_cid_list = (1 == tok_ptr->args_found);
    break;

  case DSAT_CQM_CGCMOD_WRITE:
    /* For +CGCMOD write command there is only CID list */
    no_cid_list = (0 == tok_ptr->args_found);
    break;

  default:
    return FALSE;
  }
  
  memset((void*)ctxlist, 0x0, sizeof(ctxlist));

  if ( type_list == DSAT_CQM_CGACT_WRITE )
  {
    cid_limit = (byte)((no_cid_list)?
                  DS_UMTS_MAX_PDP_PROFILE_NUM :
                  MIN((tok_ptr->args_found-1), DS_UMTS_MAX_PDP_PROFILE_NUM));

    /* Convert state parameter string to integer */
    if (ATOI_OK !=
        dsatutil_atoi(&demand_state, tok_ptr->arg[0], 10))
    {
      DS_AT_MSG1_ERROR("Cannot convert to integer parameter: %d",0);
      return FALSE;
    }

    if ((dsat_num_item_type)DSAT_PDP_CONNECT_MAX <= demand_state)
    {
      DS_AT_MSG1_ERROR("Demand state invalid: %d",demand_state);
      return FALSE;
    }
    is_activation = (DSAT_PDP_CONNECT_ACTIVATED == 
                      (dsat_pdp_connect_state_e_type)demand_state);
  }
  else if ( type_list == DSAT_CQM_CGCMOD_WRITE )
  {
    cid_limit = (byte)((no_cid_list)?
                  DS_UMTS_MAX_PDP_PROFILE_NUM :
                  MIN(tok_ptr->args_found, DS_UMTS_MAX_PDP_PROFILE_NUM));
  }
    
  /* Loop over each cid to build list of profiles to work on */
  /* Limit loop to maximum number of supported context profiles */
  for (i=1; i <= cid_limit && i <= DS_UMTS_MAX_PDP_PROFILE_NUM; i++)
  {
    boolean valid_flg = FALSE;
    dsat_result_enum_type  result = DSAT_OK;
#ifdef FEATURE_SECONDARY_PDP
    ds_umts_pdp_context_type pdata;
#endif /* FEATURE_SECONDARY_PDP */
    dsat_num_item_type cid = i;    
    
    /* If cid list provided, determine cid */
    if (!no_cid_list)
    {
      /* First parameter is <state> for +CGACT, 
         start of <cid> list for +CGCMOD */
      byte *parm_str = (type_list == DSAT_CQM_CGACT_WRITE) ? 
        tok_ptr->arg[i] : tok_ptr->arg[i-1];

      if (ATOI_OK !=
          dsatutil_atoi(&cid, parm_str, 10))
      {
        DS_AT_MSG1_ERROR("Cannot convert to integer parameter: %d",i);
        return FALSE;
      }

      /* Test for CID within valid range */
      if ((0 == cid) || (DS_UMTS_MAX_PDP_PROFILE_NUM < cid))
      {
        DS_AT_MSG1_ERROR("Connection ID invalid: %d",cid);
        return FALSE;
      }
    }
    /* Build list of primary contexts.    */
    
#ifdef FEATURE_SECONDARY_PDP    
      memset((void*)&pdata, 0x0, sizeof(pdata));
#endif /* FEATURE_SECONDARY_PDP */
    
      /* For activation, need profile info */
      if ( is_activation )
      {
        /* Query profile data */
        IS_BASE_CONTEXT_VALID((uint16)cid,result);
        if ( result == DSAT_ERROR )
        {
          return FALSE;
        }
       
        if (FALSE == valid_flg)
        {
          if ( no_cid_list )
          {
            /* Skip profile if undefined */
            continue;
          }
        }
#ifdef FEATURE_SECONDARY_PDP
        else
        {
          if (DS_UMTS_PDP_SUCCESS !=
              ds_umts_get_pdp_profile_context_info_per_subs(
                 SET_CID( (uint16)cid ),dsat_get_current_subs_id(), &pdata))
          {
            DS_AT_MSG1_HIGH("Error PDP profile read: %d",cid);
            return FALSE;
          }
        }
      } /* if ( is_activation ) */
    
      /* Build list of primary & secondary contexts.    */
      /* For deactivation and modification, profile type doesn't matter. */
      if ( (!is_activation) ||
           (FALSE == pdata.secondary_flag) )
      {
        /* Primaries at front of list */
        *pri_ptr++ = (byte)cid;
      }
      else
      {
        /* Secondaries at end of list */
        *sec_ptr-- = (byte)cid;
      }
#else
    }
   *pri_ptr++ = cid;
#endif /* FEATURE_SECONDARY_PDP */

  } /* for */

  /* Copy to structure */

  (void) dsatutil_memscpy((void*)&dsat_pdp_cid[0],
          DS_UMTS_MAX_PDP_PROFILE_NUM,(void*)&ctxlist[0],DS_UMTS_MAX_PDP_PROFILE_NUM); 
  dsat_pdp_state.change_index = 0;
  if ( type_list == DSAT_CQM_CGACT_WRITE )
  {
    dsat_pdp_state.demand_state = (dsat_pdp_connect_state_e_type)demand_state;
  }
  return TRUE;
} /* etsipkt_build_context_processing_list */

/*===========================================================================

FUNCTION  ETSIPKT_PRINT_IPV4_ADDR

DESCRIPTION
  This function prints IPV4 address in the required format.
   
DEPENDENCIES
  None

RETURN VALUE
  returns an flag that describes the result of execution.
  possible values:
    DSAT_SUCCESS : if processing successful 
    DSAT_FAILURE : otherwise

SIDE EFFECTS
  None

===========================================================================*/

LOCAL boolean etsipkt_print_ipv4_addr
(
  ds_umts_pdp_addr_type   *pdp_adr_ptr,
  uint32                  src_adr,
  boolean                 is_static_addr,
  dsm_item_type           *res_buff_ptr,
  char                    *buffer
)
{
  pdp_adr_ptr->ip_vsn = DS_IP_V4; 
  if( FALSE == is_static_addr ) 
  {
    pdp_adr_ptr->pdp_addr_ipv4 = ps_ntohl(src_adr);
  }
  else 
  { 
    pdp_adr_ptr->pdp_addr_ipv4 = src_adr;
  }
  if( dsatutil_convert_ipaddr(INT_TO_STRING,pdp_adr_ptr,(byte *)buffer) != DSAT_OK )
  {
    return DSAT_FAILURE;
  } 
  else
  {
    res_buff_ptr->used += (word) snprintf((char *)&res_buff_ptr->data_ptr[res_buff_ptr->used],
                                          res_buff_ptr->size - res_buff_ptr->used,
                                          "%s",buffer);
  }
  return DSAT_SUCCESS;
} /* etsipkt_print_ipv4_addr */

/*===========================================================================

FUNCTION  ETSIPKT_PRINT_IPV6_ADDR

DESCRIPTION
  This function prints IPV6 address in the required format.
   
DEPENDENCIES
  None

RETURN VALUE
  returns an flag that describes the result of execution.
  possible values:
    DSAT_SUCCESS : if processing successful 
    DSAT_FAILURE : otherwise

SIDE EFFECTS
  None

===========================================================================*/

LOCAL boolean etsipkt_print_ipv6_addr
(
  ds_umts_pdp_addr_type           *pdp_adr_ptr,
  ds_umts_pdp_addr_type_ipv6      *src_adr_ptr,
  boolean                         is_static_addr,
  dsm_item_type                   *res_buff_ptr,
  char                            *buffer
)
{
  pdp_adr_ptr->ip_vsn = DS_IP_V6;
  if( FALSE == is_static_addr )
  {
    pdp_adr_ptr->pdp_addr_ipv6.in6_u.u6_addr64[0] = ps_ntohll(src_adr_ptr->in6_u.u6_addr64[1]);
    pdp_adr_ptr->pdp_addr_ipv6.in6_u.u6_addr64[1] = ps_ntohll(src_adr_ptr->in6_u.u6_addr64[0]);
  }
  else
  {
    pdp_adr_ptr->pdp_addr_ipv6.in6_u.u6_addr64[0] = src_adr_ptr->in6_u.u6_addr64[0];
    pdp_adr_ptr->pdp_addr_ipv6.in6_u.u6_addr64[1] = src_adr_ptr->in6_u.u6_addr64[1];
  }
  if( dsatutil_convert_ipaddr(INT_TO_STRING,pdp_adr_ptr,(byte *)buffer) != DSAT_OK )
  {
    return DSAT_FAILURE;
  }
  else
  {
    res_buff_ptr->used += (word) snprintf( (char *)&res_buff_ptr->data_ptr[res_buff_ptr->used],
                                           res_buff_ptr->size - res_buff_ptr->used,
                                           "%s",buffer);
  }
  return DSAT_SUCCESS;
} /* etsipkt_print_ipv6_addr */


/*===========================================================================

FUNCTION DSATETSIPKT_EXEC_CGEREP_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It handles the +CGEREP command.
  This command reports (buffers) certain GPRS events when occured viz.,
  detach, PDP deactivate, class change.

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatetsipkt_exec_cgerep_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type  result = DSAT_OK;
  dsat_num_item_type cgerep_val ;
  dsatetsipkt_gprs_msg_ms_info * gprs_dd_val = NULL;
  if(DSAT_FAILURE == dsatutil_get_base_addr( DSAT_MS_MD_VALS, (void **)&gprs_dd_val, dsat_get_qcsimapp_val()))
  {
    return DSAT_ERROR;
  }
  result = dsatparm_exec_param_cmd(
                                   mode,
                                   parse_table,
                                   tok_ptr,
                                   res_buff_ptr
                                  );

  if(result == DSAT_OK)
  {
    if(VALID_TOKEN(1))
    {
      cgerep_val =(dsat_num_item_type)dsatutil_get_val(
                                         DSATETSI_EXT_CGEREP_IDX,dsat_get_qcsimapp_val(),0,NUM_TYPE);
      if(cgerep_val == DSAT_CGEREP_BUFFER_DISCARD_FWD_TE ||
         cgerep_val == DSAT_CGEREP_BUFFER_FWD_TE )
      {
        cgerep_val =(dsat_num_item_type)dsatutil_get_val(
                                         DSATETSI_EXT_CGEREP_IDX,dsat_get_qcsimapp_val(),1,NUM_TYPE);
        if(cgerep_val == DSAT_CGEREP_EMPTY_BUFFER)
        {
          /* Clearing the event buffer */
          etsipkt_clear_gprs_events_buffer(dsat_get_qcsimapp_val());
        }
        else
        {
          gprs_dd_val->flush_gprs_buffer = TRUE;
        }
      }
    }
  }

  return result;
} /* dsatetsipkt_exec_cgerep_cmd */


/*===========================================================================
FUNCTION  ETSIPKT_CGPADDR_BUILD_CID_LIST

DESCRIPTION
  This function builds the CID list from the passed tokens.
   
DEPENDENCIES
  None

RETURN VALUE
  DSAT_OK: if a valid CID was issued
  DSAT_ERROR: otherwise

SIDE EFFECTS
  None

===========================================================================*/
LOCAL dsat_result_enum_type etsipkt_cgpaddr_build_cid_list
(
  const tokens_struct_type *tok_ptr,  /* Command tokens from parser  */
  uint8 *cid_list,                    /* Parse results list */
  uint8 *index                        /* Length of list */
)
{
  dsat_result_enum_type  result = DSAT_OK;

  ASSERT( ( NULL != cid_list ) && ( NULL != index ) );
  
  while( (tok_ptr->args_found > *index) &&
         (DS_UMTS_MAX_PDP_PROFILE_NUM > *index ) )
  {
    dsat_num_item_type cid;

    if( *index >= MAX_ARG )
    {
      DS_AT_MSG1_ERROR("Index out of range: %d",*index);
      return DSAT_ERROR;
    }

    /* Convert token to numeric */
    if( ATOI_OK == dsatutil_atoi (&cid, tok_ptr->arg[*index], 10) )
    {
      /* Validate range */
      if( ( DS_UMTS_MAX_PDP_PROFILE_NUM >= cid ) &&
          ( 0 < cid ) )
      {
        /* Store in list to report on */
        cid_list[(*index)++] = (uint8)cid;
      }
      else
      {
        DS_AT_MSG2_ERROR("CID out of range: index=%d value=%d",*index,cid);
        result = DSAT_ERROR;
        break;
      }
    }
    else
    {
      DS_AT_MSG1_ERROR("Cannot convert CID value: index=%d",*index);
      result = DSAT_ERROR;
      break;
    }
  } /* while */

  return result;
} /* etsipkt_cgpaddr_build_cid_list */


/*===========================================================================
FUNCTION  ETSIPKT_CGPADDR_FORMAT_RESPONSE

DESCRIPTION
  This function builds the CID, PDP address formatting before sending to TE.
   
DEPENDENCIES
  None

RETURN VALUE
  DSAT_OK: if a valid CID was issued
  DSAT_ERROR: otherwise

SIDE EFFECTS
  None

===========================================================================*/
LOCAL dsat_result_enum_type etsipkt_cgpaddr_format_response
(
  const byte               *cid_list_ptr,
  const byte                cid_list_len,
  const tokens_struct_type *tok_ptr,
  dsm_item_type            *res_buff_ptr
)
{
  dsat_result_enum_type  result = DSAT_OK;
  uint32                 i;
  boolean                valid_cid_found = FALSE;
  char                   buf[MAX_IPADDR_STR_LEN+1];
  uint16                 used;
  ds_umts_pdp_context_type pdp_context;

  memset((void*)&pdp_context, 0x0, sizeof(pdp_context));
  if(tok_ptr->op == (NA|EQ|QU))
  {
    /* TEST syntax */
    
    /* Loop through the list to check if atleast one CID is defined 
       Else just return OK */

    res_buff_ptr->used = (word)snprintf(
             (char *)&res_buff_ptr->data_ptr[res_buff_ptr->used], 
             res_buff_ptr->size - res_buff_ptr->used,
             "%s ", "+CGPADDR: (");
    used = res_buff_ptr->used;

    for(i = 0; i < DS_UMTS_MAX_PDP_PROFILE_NUM; ++i)
    {
      valid_cid_found = FALSE;

      if((DS_UMTS_PDP_SUCCESS == 
                   ds_umts_get_pdp_profile_context_info_per_subs((uint16)(i+1), 
                                                        dsat_get_current_subs_id(),
                                                        &pdp_context )) &&
                                                  pdp_context.valid_flg == TRUE)
      {
        valid_cid_found = TRUE;
      }
      if(valid_cid_found == TRUE)
      {
        res_buff_ptr->used += (word)snprintf(
             (char *)&res_buff_ptr->data_ptr[res_buff_ptr->used], 
             res_buff_ptr->size - res_buff_ptr->used,
             "%ld,", i+1);
      }
    }
    if( used == res_buff_ptr->used )
    {
      /* If atleast one Context is not valid Reset data_ptr */
      res_buff_ptr->used = 0;
      res_buff_ptr->data_ptr[res_buff_ptr->used] = '\0';
    }
    else
    {
      res_buff_ptr->used--; 
      res_buff_ptr->used += (word)snprintf(
               (char *)&res_buff_ptr->data_ptr[res_buff_ptr->used],
               res_buff_ptr->size - res_buff_ptr->used,
               ")");
    }
  }
  else
  {
    /* WRITE syntax */
    boolean is_context_valid = FALSE;
    boolean is_context_active = FALSE;
    ds_umts_pdp_addr_type* pdp_addr = NULL;
    ds_umts_pdp_addr_type temp_pdp_addr;
    boolean is_static_addr = FALSE;

    res_buff_ptr->used = 0;
    
    /* Check for all contexts processing case */
    if( NULL == cid_list_ptr )
    {
      
      for(i = 0; i < DS_UMTS_MAX_PDP_PROFILE_NUM; ++i)
      {
        if((DS_UMTS_PDP_SUCCESS == 
                      ds_umts_get_pdp_profile_context_info_per_subs((uint16)(i+1), 
                                                          dsat_get_current_subs_id(),
                                                          &pdp_context )) &&
                                                 pdp_context.valid_flg == TRUE)
        {
          is_context_valid = TRUE;
          is_context_active = TRUE;
          is_static_addr = FALSE;
          if(DSAT_OK != ds_3gpp_rmsm_atif_get_pdp_addr(
                                   (uint32)(i+1),dsat_get_current_subs_id(),
                                   &pdp_context.pdp_addr))
          {
            is_static_addr = TRUE;
            DS_AT_MSG1_MED("PDP context - %d has a static address.",i);
          }
          pdp_addr = &pdp_context.pdp_addr;
        }
        else
        {
          is_context_active = FALSE;
          is_context_valid = FALSE;
        }
        if(is_context_valid == TRUE)
        {
          if(is_context_active == TRUE)
          {
            buf[0] = '\0';
            res_buff_ptr->used += (uint16)snprintf(
                 (char *)&res_buff_ptr->data_ptr[res_buff_ptr->used],
                  res_buff_ptr->size - res_buff_ptr->used,
                  "%s %ld,", "+CGPADDR:",i+1);

            if( pdp_addr->ip_vsn == DS_IP_V4)
            {
              if( DSAT_FAILURE == etsipkt_print_ipv4_addr( &temp_pdp_addr , pdp_context.pdp_addr.pdp_addr_ipv4,
                                                      is_static_addr, res_buff_ptr, buf ))
              {
                return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
              }
            }
            else if( pdp_addr->ip_vsn == DS_IP_V6 )
            {
              if(DSAT_FAILURE == etsipkt_print_ipv6_addr( &temp_pdp_addr , &pdp_context.pdp_addr.pdp_addr_ipv6,
                                                      is_static_addr, res_buff_ptr, buf ))
              {
                return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
              }
            }
            else if( pdp_addr->ip_vsn == DS_IP_V4V6 )
            {
              if(DSAT_FAILURE == etsipkt_print_ipv4_addr( &temp_pdp_addr , pdp_context.pdp_addr.pdp_addr_ipv4,
                                                      is_static_addr, res_buff_ptr, buf ))
              {
                return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
              }
              buf[0] = '\0';
              res_buff_ptr->data_ptr[res_buff_ptr->used++] = ',';
              if(DSAT_FAILURE == etsipkt_print_ipv6_addr( &temp_pdp_addr , &pdp_context.pdp_addr.pdp_addr_ipv6,
                                                      is_static_addr, res_buff_ptr, buf ))
              {
                return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
              }
            }
          }
          res_buff_ptr->data_ptr[res_buff_ptr->used++] = '\n';
        }
      }
    }
    else
    {
      /* Process specific contexts */
      for(i = 0; i < cid_list_len; ++i)
      {
        byte index = cid_list_ptr[i]-1;
        if((DS_UMTS_PDP_SUCCESS == 
                         ds_umts_get_pdp_profile_context_info_per_subs((uint16)(index+1), 
                                                                dsat_get_current_subs_id(),
                                                                &pdp_context )) &&
                                                    pdp_context.valid_flg == TRUE)
        {
          is_context_valid = TRUE;
          is_context_active = TRUE;
          is_static_addr = FALSE;
          if(DSAT_OK != ds_3gpp_rmsm_atif_get_pdp_addr(
                                    (uint32)(index+1),dsat_get_current_subs_id(),
                                    &pdp_context.pdp_addr))
          {
            is_static_addr = TRUE;
            DS_AT_MSG1_MED("PDP context - %d has a static address.",index);
          }
          pdp_addr = &pdp_context.pdp_addr;
        }
        else
        {
          is_context_active = FALSE;
          is_context_valid = FALSE;
        }
        if( TRUE == is_context_valid )
        {
          if(TRUE == is_context_active)
          {
            buf[0] = '\0';
            res_buff_ptr->used += (word) snprintf(
                       (char *)&res_buff_ptr->data_ptr[res_buff_ptr->used],
                       res_buff_ptr->size - res_buff_ptr->used,
                       "%s %u,", "+CGPADDR:",cid_list_ptr[i]);
			
            if( pdp_addr->ip_vsn == DS_IP_V4)
            {
              if(DSAT_FAILURE == etsipkt_print_ipv4_addr( &temp_pdp_addr , pdp_context.pdp_addr.pdp_addr_ipv4,
                                                      is_static_addr, res_buff_ptr, buf ))
              {
                return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
              }
            }
            else if( pdp_addr->ip_vsn == DS_IP_V6 )
            {
              if(DSAT_FAILURE == etsipkt_print_ipv6_addr( &temp_pdp_addr , &pdp_context.pdp_addr.pdp_addr_ipv6,
                                                      is_static_addr, res_buff_ptr, buf ))
              {
                return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
              }
            }
            else if( pdp_addr->ip_vsn == DS_IP_V4V6 )
            {
              if(DSAT_FAILURE == etsipkt_print_ipv4_addr( &temp_pdp_addr , pdp_context.pdp_addr.pdp_addr_ipv4,
                                                      is_static_addr, res_buff_ptr, buf ))
              {
                return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
              }
              buf[0] = '\0';
              res_buff_ptr->data_ptr[res_buff_ptr->used++] = ',';
              if(DSAT_FAILURE == etsipkt_print_ipv6_addr( &temp_pdp_addr , &pdp_context.pdp_addr.pdp_addr_ipv6,
                                                      is_static_addr, res_buff_ptr, buf ))
              {
                return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
              }
            }
          }
          res_buff_ptr->data_ptr[res_buff_ptr->used++] = '\n';
        }
      }
    }
    if(res_buff_ptr->used != 0)
    {
      res_buff_ptr->used--;
      res_buff_ptr->data_ptr[res_buff_ptr->used] = '\0';
    }
  }
  return result;
} /* etsipkt_cgpaddr_format_response */

/*===========================================================================
FUNCTION  ETSIPKT_CGCONTRDP_FORMAT_RESPONSE

DESCRIPTION
This function builds the P-CID, and related Dynamic parameters info
into a formatted form as mentioned in the 27.007.
   
DEPENDENCIES
  None

RETURN VALUE
  DSAT_OK: if a valid CID was issued
  DSAT_ERROR: otherwise

SIDE EFFECTS
  None

===========================================================================*/
LOCAL dsat_result_enum_type etsipkt_cgcontrdp_format_response
(
  const byte               *cid_list_ptr,
  const byte                cid_list_len,
  const tokens_struct_type *tok_ptr,
  dsm_item_type            *res_buff_ptr
)
{
  dsat_result_enum_type  result = DSAT_OK;
  uint32                 i;
  char                   buf[MAX_IPADDR_STR_LEN+1];
  uint16                 used;
  ds_3gpp_atif_prim_profile_list_type prim_profile_list;

  memset((void*)&prim_profile_list, 0x0, sizeof(prim_profile_list));
  res_buff_ptr->used = 0;

  /* Get the Active Primary Contexts list */

  ds_3gpp_rmsm_atif_get_all_active_prim_profiles(dsat_get_current_subs_id(),
                                                 &prim_profile_list);

  if(tok_ptr->op == (NA|EQ|QU))
  {
    /* TEST syntax */

    res_buff_ptr->used = (word)snprintf(
             (char *)&res_buff_ptr->data_ptr[res_buff_ptr->used], 
             res_buff_ptr->size - res_buff_ptr->used,
             "%s ", "+CGCONTRDP: (");
    used = res_buff_ptr->used;

    for(i = 0;(i< DS_3GPP_MAX_PDN_CONTEXT)&&(i< prim_profile_list.data_len); ++i)
    {
      res_buff_ptr->used += (word)snprintf(
           (char *)&res_buff_ptr->data_ptr[res_buff_ptr->used], 
           res_buff_ptr->size - res_buff_ptr->used,
           "%ld,",(long int) prim_profile_list.data_ptr[i]);
    }
    if( used == res_buff_ptr->used )
    {
      /* If atleast one Context is not valid Reset data_ptr */
      
      res_buff_ptr->used = 0;
      res_buff_ptr->data_ptr[res_buff_ptr->used] = '\0';
    }
    else
    {
      res_buff_ptr->used--; 
      res_buff_ptr->used += (word)snprintf(
               (char *)&res_buff_ptr->data_ptr[res_buff_ptr->used],
               res_buff_ptr->size - res_buff_ptr->used,
               " )");
    }
  }
  else
  {
    /* WRITE syntax */
    dynamic_param_ptr = 
      (ds_3gpp_atif_dynamic_params_info_type *) dsat_alloc_memory(
                               sizeof(ds_3gpp_atif_dynamic_params_info_type), FALSE);

    memset((void*)dynamic_param_ptr,0x0,sizeof(ds_3gpp_atif_dynamic_params_info_type));

    /* Check for all contexts processing case */
    if( NULL == cid_list_ptr )
    {
      /* Process specified context */

      for(i = 0;(i< DS_3GPP_MAX_PDN_CONTEXT)&&(i< prim_profile_list.data_len); ++i)
      {
        memset((void*)dynamic_param_ptr,0x0,sizeof(ds_3gpp_atif_dynamic_params_info_type));
        if(TRUE == ds_3gpp_rmsm_atif_read_dynamic_params(prim_profile_list.data_ptr[i],
                                                         dsat_get_current_subs_id(),
                                  DS_3GPP_ATIF_PRIM_INFO,dynamic_param_ptr))
        {
          ds_umts_pdp_addr_type temp_pdp_addr;

          if( DS_3GPP_ATIF_PRIM_INFO == dynamic_param_ptr->info_type)
          {
            
            ds_3gpp_atif_primary_dynamic_info_type *prim_dynamic_ptr = 
                                              &dynamic_param_ptr->u.prim_dynamic_info;
            uint16 check_size = 20+9*MAX_IPADDR_STR_LEN+ \
                              strlen((const char *)prim_dynamic_ptr->apn);
            /*One loop below would need 40 max for IPV6, 20 for other params 
              +length of APN */

            CHECK_APPEND_RESPONSE_BUFFER_NEW (((uint16)DSM_BUFFER_THRESHOLD - check_size));

            res_buff_ptr->used += (word)snprintf(
                     (char *)&res_buff_ptr->data_ptr[res_buff_ptr->used], 
                     res_buff_ptr->size - res_buff_ptr->used,
                    "%s %d,%u,%s,", "+CGCONTRDP:",
                      prim_profile_list.data_ptr[i],
                      prim_dynamic_ptr->bearer_id,
                      prim_dynamic_ptr->apn);

            if( prim_dynamic_ptr->ip_addr.ip_vsn == DS_IP_V4V6)
            {
              if(DSAT_FAILURE == etsipkt_print_ipv4_addr( &temp_pdp_addr , prim_dynamic_ptr->ip_addr.pdp_addr_ipv4,
                                                      FALSE, res_buff_ptr, buf ))
              {
                return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
              }

              buf[0] = '\0';

              res_buff_ptr->data_ptr[res_buff_ptr->used++] = ',';
              if(DSAT_FAILURE == etsipkt_print_ipv6_addr( &temp_pdp_addr , &prim_dynamic_ptr->ip_addr.pdp_addr_ipv6,
                                                      FALSE, res_buff_ptr, buf ))
              {
                return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
              }
            }
            else if(prim_dynamic_ptr->ip_addr.ip_vsn == DS_IP_V4)
            {
              if(DSAT_FAILURE == etsipkt_print_ipv4_addr( &temp_pdp_addr , prim_dynamic_ptr->ip_addr.pdp_addr_ipv4,
                                                      FALSE, res_buff_ptr, buf ))
              {
                return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
              }
            }
            else if(prim_dynamic_ptr->ip_addr.ip_vsn == DS_IP_V6)
            {
              if(DSAT_FAILURE == etsipkt_print_ipv6_addr( &temp_pdp_addr , &prim_dynamic_ptr->ip_addr.pdp_addr_ipv6,
                                                      FALSE, res_buff_ptr, buf ))
              {
                return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
              }
            }
            buf[0] = '\0';

            res_buff_ptr->data_ptr[res_buff_ptr->used++] = ',';
            
            /* Print the <gw_addr> - First the IPv4 followed by IPv6 */
            if(prim_dynamic_ptr->v4_ipcp.ipcp4_options.gateway_addr != 0)
            {
              if(DSAT_FAILURE == etsipkt_print_ipv4_addr( &temp_pdp_addr ,
              	                                       prim_dynamic_ptr->v4_ipcp.ipcp4_options.gateway_addr,
                                                      FALSE, res_buff_ptr, buf ))
              {
                return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
              }
              
            }
            buf[0] = '\0';
            
            if(prim_dynamic_ptr->v6_ipcp.ipv6cp_options.gateway_addr.ps_s6_addr64[1] != 0)
            {
              res_buff_ptr->data_ptr[res_buff_ptr->used++] = ' ';
              if(DSAT_FAILURE == etsipkt_print_ipv6_addr( &temp_pdp_addr ,
              	                                          (ds_umts_pdp_addr_type_ipv6*) &prim_dynamic_ptr->v6_ipcp.ipv6cp_options.gateway_addr,
                                                         FALSE, res_buff_ptr, buf ))
              {
                return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
              }
            }
            buf[0] = '\0';

            res_buff_ptr->data_ptr[res_buff_ptr->used++] = ',';

            /* Print the <DNS_prim_addr> - First the IPv4 followed by IPv6 */
            if(prim_dynamic_ptr->v4_ipcp.ipcp4_options.primary_dns!= 0)
            {
              if(DSAT_FAILURE == etsipkt_print_ipv4_addr( &temp_pdp_addr ,
                                                      prim_dynamic_ptr->v4_ipcp.ipcp4_options.primary_dns,
                                                      FALSE, res_buff_ptr, buf ))
              {
                return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
              }
            }
            buf[0] = '\0';
            
            if(prim_dynamic_ptr->v6_ipcp.ipv6cp_options.primary_dns.ps_s6_addr64[1] != 0)
            {
              res_buff_ptr->data_ptr[res_buff_ptr->used++] = ' ';
              if(DSAT_FAILURE == etsipkt_print_ipv6_addr( &temp_pdp_addr ,
              	                                           (ds_umts_pdp_addr_type_ipv6*)&prim_dynamic_ptr->v6_ipcp.ipv6cp_options.primary_dns,
                                                           FALSE, res_buff_ptr, buf ))
              {
                return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
              }
            }
            buf[0] = '\0';
            res_buff_ptr->data_ptr[res_buff_ptr->used++] = ',';

            /* Print the <DNS_sec_addr> - First the IPv4 followed by IPv6 */
            if(prim_dynamic_ptr->v4_ipcp.ipcp4_options.secondary_dns!= 0)
            {
              if(DSAT_FAILURE == etsipkt_print_ipv4_addr( &temp_pdp_addr ,
              	                                       prim_dynamic_ptr->v4_ipcp.ipcp4_options.secondary_dns,
                                                      FALSE, res_buff_ptr, buf ))
              {
                return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
              }
            }
            buf[0] = '\0';
            
            if(prim_dynamic_ptr->v6_ipcp.ipv6cp_options.secondary_dns.ps_s6_addr64[1] != 0)
            {
              res_buff_ptr->data_ptr[res_buff_ptr->used++] = ' ';
              if(DSAT_FAILURE == etsipkt_print_ipv6_addr( &temp_pdp_addr ,
              	                                         (ds_umts_pdp_addr_type_ipv6*)&prim_dynamic_ptr->v6_ipcp.ipv6cp_options.secondary_dns,
                                                        FALSE, res_buff_ptr, buf ))
              {
                return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
              }
            }
            buf[0] = '\0';

            /* Print the <P_CSCF_prim_addr>  */
            if( prim_dynamic_ptr->prim_pcscf_addr.ip_vsn == DS_IP_V4V6)
            {
              res_buff_ptr->data_ptr[res_buff_ptr->used++] = ',';
              
              if(DSAT_FAILURE == etsipkt_print_ipv4_addr( &temp_pdp_addr ,
              	                                       prim_dynamic_ptr->prim_pcscf_addr.pdp_addr_ipv4,
                                                      FALSE, res_buff_ptr, buf ))
              {
                return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
              }
            
              buf[0] = '\0';
            
              res_buff_ptr->data_ptr[res_buff_ptr->used++] = ',';
              if(DSAT_FAILURE == etsipkt_print_ipv6_addr( &temp_pdp_addr ,
                            	                        (ds_umts_pdp_addr_type_ipv6*)&prim_dynamic_ptr->prim_pcscf_addr.pdp_addr_ipv6,
                                                      FALSE, res_buff_ptr, buf ))
              {
                return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
              }
            }
            else if(prim_dynamic_ptr->prim_pcscf_addr.ip_vsn == DS_IP_V4)
            {
              res_buff_ptr->data_ptr[res_buff_ptr->used++] = ',';
              
              if(DSAT_FAILURE == etsipkt_print_ipv4_addr( &temp_pdp_addr ,
              	                                       prim_dynamic_ptr->prim_pcscf_addr.pdp_addr_ipv4,
                                                      FALSE, res_buff_ptr, buf ))
              {
                return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
              }
            }
            else if(prim_dynamic_ptr->prim_pcscf_addr.ip_vsn == DS_IP_V6)
            {
              res_buff_ptr->data_ptr[res_buff_ptr->used++] = ',';
              if(DSAT_FAILURE == etsipkt_print_ipv6_addr( &temp_pdp_addr , &prim_dynamic_ptr->prim_pcscf_addr.pdp_addr_ipv6,
                                                      FALSE, res_buff_ptr, buf ))
              {
                return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
              }
            }
            buf[0] = '\0';

            /* Print the <P_CSCF_sec_addr>  */
            if( prim_dynamic_ptr->sec_pcscf_addr.ip_vsn == DS_IP_V4V6)
            {
              res_buff_ptr->data_ptr[res_buff_ptr->used++] = ',';
              
              if(DSAT_FAILURE == etsipkt_print_ipv4_addr( &temp_pdp_addr ,
              	                                       prim_dynamic_ptr->sec_pcscf_addr.pdp_addr_ipv4,
                                                      FALSE, res_buff_ptr, buf ))
              {
                return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
              }
            
              buf[0] = '\0';
            
              res_buff_ptr->data_ptr[res_buff_ptr->used++] = ',';
              if(DSAT_FAILURE == etsipkt_print_ipv6_addr( &temp_pdp_addr , &prim_dynamic_ptr->sec_pcscf_addr.pdp_addr_ipv6,
                                                      FALSE, res_buff_ptr, buf ))
              {
                return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
              }
            }
            else if(prim_dynamic_ptr->sec_pcscf_addr.ip_vsn == DS_IP_V4)
            {
              res_buff_ptr->data_ptr[res_buff_ptr->used++] = ',';

              if(DSAT_FAILURE == etsipkt_print_ipv4_addr( &temp_pdp_addr ,
              	                                       prim_dynamic_ptr->sec_pcscf_addr.pdp_addr_ipv4,
                                                      FALSE, res_buff_ptr, buf ))
              {
                return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
              }
            }
            else if(prim_dynamic_ptr->sec_pcscf_addr.ip_vsn == DS_IP_V6)
            {
              res_buff_ptr->data_ptr[res_buff_ptr->used++] = ',';
              if(DSAT_FAILURE == etsipkt_print_ipv6_addr( &temp_pdp_addr , &prim_dynamic_ptr->sec_pcscf_addr.pdp_addr_ipv6,
                                                      FALSE, res_buff_ptr, buf ))
              {
                return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
              }
            }
            buf[0] = '\0';


            res_buff_ptr->data_ptr[res_buff_ptr->used++] = '\n';
          }
          else
          {
            DS_AT_MSG1_ERROR(" Info type mismatch with DS_3GPP_ATIF_PRIM_INFO %d ",dynamic_param_ptr->info_type);
            ASSERT(0);
          }
        }
      }
    }
    else if(cid_list_len == 1)
    {
      /* Process specified context */

      if(TRUE == ds_3gpp_rmsm_atif_read_dynamic_params(cid_list_ptr[0],
                                                       dsat_get_current_subs_id(),
                                DS_3GPP_ATIF_PRIM_INFO,dynamic_param_ptr))
      {
        ds_umts_pdp_addr_type temp_pdp_addr;

        if( DS_3GPP_ATIF_PRIM_INFO == dynamic_param_ptr->info_type)
        {
          ds_3gpp_atif_primary_dynamic_info_type *prim_dynamic_ptr = 
                                            &dynamic_param_ptr->u.prim_dynamic_info;

          res_buff_ptr->used = (word)snprintf(
                   (char *)&res_buff_ptr->data_ptr[res_buff_ptr->used], 
                   res_buff_ptr->size - res_buff_ptr->used,
                  "%s %d,%u,%s,", "+CGCONTRDP:",
                    cid_list_ptr[0],
                    prim_dynamic_ptr->bearer_id,
                    prim_dynamic_ptr->apn);

          DS_AT_MSG2_HIGH(" IP addr version for the cid %d is %d ",cid_list_ptr[0],prim_dynamic_ptr->ip_addr.ip_vsn);

          if( prim_dynamic_ptr->ip_addr.ip_vsn == DS_IP_V4V6)
          {
            if(DSAT_FAILURE == etsipkt_print_ipv4_addr( &temp_pdp_addr ,
                                                    prim_dynamic_ptr->ip_addr.pdp_addr_ipv4,
                                                    FALSE, res_buff_ptr, buf ))
            {
              return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
            }
          
            buf[0] = '\0';
          
            res_buff_ptr->data_ptr[res_buff_ptr->used++] = ',';
            if(DSAT_FAILURE == etsipkt_print_ipv6_addr( &temp_pdp_addr , &prim_dynamic_ptr->ip_addr.pdp_addr_ipv6,
                                                    FALSE, res_buff_ptr, buf ))
            {
              return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
            }
          }
          else if(prim_dynamic_ptr->ip_addr.ip_vsn == DS_IP_V4)
          {
            if(DSAT_FAILURE == etsipkt_print_ipv4_addr( &temp_pdp_addr ,
                                                    prim_dynamic_ptr->ip_addr.pdp_addr_ipv4,
                                                    FALSE, res_buff_ptr, buf ))
            {
              return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
            }
          }
          else if(prim_dynamic_ptr->ip_addr.ip_vsn == DS_IP_V6)
          {
            if(DSAT_FAILURE == etsipkt_print_ipv6_addr( &temp_pdp_addr , &prim_dynamic_ptr->ip_addr.pdp_addr_ipv6,
                                                    FALSE, res_buff_ptr, buf ))
            {
              return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
            }
          }
          buf[0] = '\0';

          res_buff_ptr->data_ptr[res_buff_ptr->used++] = ',';
          
          /* Print the <gw_addr> - First the IPv4 followed by IPv6 */
          if(prim_dynamic_ptr->v4_ipcp.ipcp4_options.gateway_addr != 0)
          {
            if(DSAT_FAILURE == etsipkt_print_ipv4_addr( &temp_pdp_addr ,
                                                    prim_dynamic_ptr->v4_ipcp.ipcp4_options.gateway_addr,
                                                    FALSE, res_buff_ptr, buf ))
            {
              return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
            }
          }
          buf[0] = '\0';
          
          if(prim_dynamic_ptr->v6_ipcp.ipv6cp_options.gateway_addr.ps_s6_addr64[1] != 0)
          {
            res_buff_ptr->data_ptr[res_buff_ptr->used++] = ' ';
            if(DSAT_FAILURE == etsipkt_print_ipv6_addr( &temp_pdp_addr ,
            	                                         (ds_umts_pdp_addr_type_ipv6*)&prim_dynamic_ptr->v6_ipcp.ipv6cp_options.gateway_addr,
                                                      FALSE, res_buff_ptr, buf ))
            {
              return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
            }
          }
          buf[0] = '\0';
          
          res_buff_ptr->data_ptr[res_buff_ptr->used++] = ',';
          
          /* Print the <DNS_prim_addr> - First the IPv4 followed by IPv6 */
          if(prim_dynamic_ptr->v4_ipcp.ipcp4_options.primary_dns!= 0)
          {
            if(DSAT_FAILURE == etsipkt_print_ipv4_addr( &temp_pdp_addr ,
                                                    prim_dynamic_ptr->v4_ipcp.ipcp4_options.primary_dns,
                                                    FALSE, res_buff_ptr, buf ))
            {
              return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
            }
          }
          buf[0] = '\0';
          
          if(prim_dynamic_ptr->v6_ipcp.ipv6cp_options.primary_dns.ps_s6_addr64[1] != 0)
          {
            res_buff_ptr->data_ptr[res_buff_ptr->used++] = ' ';
            if(DSAT_FAILURE == etsipkt_print_ipv6_addr( &temp_pdp_addr ,
            	                                        (ds_umts_pdp_addr_type_ipv6*)&prim_dynamic_ptr->v6_ipcp.ipv6cp_options.primary_dns,
                                                      FALSE, res_buff_ptr, buf ))
            {
              return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
            }
          }
          buf[0] = '\0';
          res_buff_ptr->data_ptr[res_buff_ptr->used++] = ',';
          
          /* Print the <DNS_sec_addr> - First the IPv4 followed by IPv6 */
          if(prim_dynamic_ptr->v4_ipcp.ipcp4_options.secondary_dns!= 0)
          {
            if(DSAT_FAILURE == etsipkt_print_ipv4_addr( &temp_pdp_addr ,
                                                    prim_dynamic_ptr->v4_ipcp.ipcp4_options.secondary_dns,
                                                    FALSE, res_buff_ptr, buf ))
            {
              return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
            }
          }
          buf[0] = '\0';
          
          if(prim_dynamic_ptr->v6_ipcp.ipv6cp_options.secondary_dns.ps_s6_addr64[1] != 0)
          {
            res_buff_ptr->data_ptr[res_buff_ptr->used++] = ' ';
            if(DSAT_FAILURE == etsipkt_print_ipv6_addr( &temp_pdp_addr ,
            	                                        (ds_umts_pdp_addr_type_ipv6*)&prim_dynamic_ptr->v6_ipcp.ipv6cp_options.secondary_dns,
                                                     FALSE, res_buff_ptr, buf ))
            {
              return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
            }
          }
          buf[0] = '\0';
          
          /* Print the <P_CSCF_prim_addr>  */
          if( prim_dynamic_ptr->prim_pcscf_addr.ip_vsn == DS_IP_V4V6)
          {
            res_buff_ptr->data_ptr[res_buff_ptr->used++] = ',';
            if(DSAT_FAILURE == etsipkt_print_ipv4_addr( &temp_pdp_addr ,
                                                    prim_dynamic_ptr->prim_pcscf_addr.pdp_addr_ipv4,
                                                    FALSE, res_buff_ptr, buf ))
            {
              return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
            }
          
            buf[0] = '\0';
          
            res_buff_ptr->data_ptr[res_buff_ptr->used++] = ',';
            if(DSAT_FAILURE == etsipkt_print_ipv6_addr( &temp_pdp_addr , &prim_dynamic_ptr->prim_pcscf_addr.pdp_addr_ipv6,
                                                    FALSE, res_buff_ptr, buf ))
            {
              return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
            }
          }
          else if(prim_dynamic_ptr->prim_pcscf_addr.ip_vsn == DS_IP_V4)
          {
            res_buff_ptr->data_ptr[res_buff_ptr->used++] = ',';
            if(DSAT_FAILURE == etsipkt_print_ipv4_addr( &temp_pdp_addr ,
                                                    prim_dynamic_ptr->prim_pcscf_addr.pdp_addr_ipv4,
                                                    FALSE, res_buff_ptr, buf ))
            {
              return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
            }
          }
          else if(prim_dynamic_ptr->prim_pcscf_addr.ip_vsn == DS_IP_V6)
          {
            res_buff_ptr->data_ptr[res_buff_ptr->used++] = ',';
            if(DSAT_FAILURE == etsipkt_print_ipv6_addr( &temp_pdp_addr , &prim_dynamic_ptr->prim_pcscf_addr.pdp_addr_ipv6,
                                                    FALSE, res_buff_ptr, buf ))
            {
              return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
            }
          }
          buf[0] = '\0';
          
          /* Print the <P_CSCF_sec_addr>  */
          if( prim_dynamic_ptr->sec_pcscf_addr.ip_vsn == DS_IP_V4V6)
          {
            res_buff_ptr->data_ptr[res_buff_ptr->used++] = ',';
            if(DSAT_FAILURE == etsipkt_print_ipv4_addr( &temp_pdp_addr ,
                                                    prim_dynamic_ptr->sec_pcscf_addr.pdp_addr_ipv4,
                                                    FALSE, res_buff_ptr, buf ))
            {
              return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
            }
          
            buf[0] = '\0';
          
          res_buff_ptr->data_ptr[res_buff_ptr->used++] = ',';
            if(DSAT_FAILURE == etsipkt_print_ipv6_addr( &temp_pdp_addr , &prim_dynamic_ptr->sec_pcscf_addr.pdp_addr_ipv6,
                                                    FALSE, res_buff_ptr, buf ))
            {
              return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
            }
          }
          else if(prim_dynamic_ptr->sec_pcscf_addr.ip_vsn == DS_IP_V4)
          {
            res_buff_ptr->data_ptr[res_buff_ptr->used++] = ',';
            if(DSAT_FAILURE == etsipkt_print_ipv4_addr( &temp_pdp_addr ,
                                                    prim_dynamic_ptr->sec_pcscf_addr.pdp_addr_ipv4,
                                                    FALSE, res_buff_ptr, buf ))
            {
              return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
            }
          }
          else if(prim_dynamic_ptr->sec_pcscf_addr.ip_vsn == DS_IP_V6)
          {
            res_buff_ptr->data_ptr[res_buff_ptr->used++] = ',';
            if(DSAT_FAILURE == etsipkt_print_ipv6_addr( &temp_pdp_addr , &prim_dynamic_ptr->sec_pcscf_addr.pdp_addr_ipv6,
                                                    FALSE, res_buff_ptr, buf ))
            {
              return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
            }
          }
          buf[0] = '\0';
          
          
          res_buff_ptr->data_ptr[res_buff_ptr->used++] = '\n';

        }
        else
        {
          DS_AT_MSG1_ERROR(" Info type mismatch with DS_3GPP_ATIF_PRIM_INFO %d ",dynamic_param_ptr->info_type);
          ASSERT(0);
        }
      }
    }

    if(res_buff_ptr->used != 0)
    {
      res_buff_ptr->used--;
      res_buff_ptr->data_ptr[res_buff_ptr->used] = '\0';
    }

  modem_mem_free( dynamic_param_ptr , MODEM_MEM_CLIENT_DATA);

  }

  return result;
} /* etsipkt_cgcontrdp_format_response */

/*===========================================================================
FUNCTION  ETSIPKT_CGSCONTRDP_FORMAT_RESPONSE

DESCRIPTION
This function builds the CID, and related Dynamic parameters of all 
Non Primary Contexts info into a formatted form as mentioned in the 27.007.

   
DEPENDENCIES
  None

RETURN VALUE
  DSAT_OK: if a valid CID was issued
  DSAT_ERROR: otherwise

SIDE EFFECTS
  None

===========================================================================*/
LOCAL dsat_result_enum_type etsipkt_cgscontrdp_format_response
(
  const byte               *cid_list_ptr,
  const byte                cid_list_len,
  const tokens_struct_type *tok_ptr,
  dsm_item_type            *format_buffer
)
{
  dsat_result_enum_type  result = DSAT_OK;
  uint32                 i;
  uint16                 used;
  ds_3gpp_atif_flow_cid_list_type flow_profile_list;

  memset((void*)&flow_profile_list, 0x0, sizeof(flow_profile_list));
  format_buffer->used = 0;

  /* Get the Active Non-Primary Contexts list */
   ds_3gpp_rmsm_atif_get_all_active_flow_profiles(dsat_get_current_subs_id(),
                                                  &flow_profile_list);


  if(tok_ptr->op == (NA|EQ|QU))
  {
    /* TEST syntax */
    
    format_buffer->used = (word)snprintf(
             (char *)&format_buffer->data_ptr[format_buffer->used], 
             format_buffer->size - format_buffer->used,
             "%s ", "+CGSCONTRDP: (");
    used = format_buffer->used;

    for(i = 0;(i< DS_3GPP_MAX_FLOW_CONTEXT)&&(i< flow_profile_list.data_len); ++i)
    {
      format_buffer->used += (word)snprintf(
           (char *)&format_buffer->data_ptr[format_buffer->used], 
           format_buffer->size - format_buffer->used,
           "%ld,", (long int) flow_profile_list.data_ptr[i]);
    }
    if( used == format_buffer->used )
    {
      /* If atleast one Context is not valid Reset data_ptr */
      format_buffer->used = 0;
      format_buffer->data_ptr[format_buffer->used] = '\0';
    }
    else
    {
      format_buffer->used--; 
      format_buffer->used += (word)snprintf(
               (char *)&format_buffer->data_ptr[format_buffer->used],
               format_buffer->size - format_buffer->used,
               " )");
    }
  }
  else
  {
    /* WRITE syntax */
    dynamic_param_ptr = 
      (ds_3gpp_atif_dynamic_params_info_type *) dsat_alloc_memory(
                               sizeof(ds_3gpp_atif_dynamic_params_info_type), FALSE);

    /* Check for all contexts processing case */
    if( NULL == cid_list_ptr )
    {
      /* Process All Non - Primary contexts */

      for(i = 0;(i< DS_3GPP_MAX_FLOW_CONTEXT)&&(i< flow_profile_list.data_len); ++i)
      {

        if(TRUE == ds_3gpp_rmsm_atif_read_dynamic_params(flow_profile_list.data_ptr[i],
                                                         dsat_get_current_subs_id(),
                                  DS_3GPP_ATIF_SEC_INFO,dynamic_param_ptr))
        {
          if( DS_3GPP_ATIF_SEC_INFO == dynamic_param_ptr->info_type)
          {
            ds_3gpp_atif_secondary_dynamic_info_type *sec_dynamic_ptr = 
                                              &dynamic_param_ptr->u.sec_dynamic_info;
  
  
            format_buffer->used += (word)snprintf(
                     (char *)&format_buffer->data_ptr[format_buffer->used], 
                     format_buffer->size - format_buffer->used,
                    "%s %d,%d,%d \n", "+CGSCONTRDP:",
                      (int)sec_dynamic_ptr->cid,
                      (int)sec_dynamic_ptr->p_cid,
                      (int)sec_dynamic_ptr->bearer_id);
          }
          else
          {
            DS_AT_MSG1_ERROR(" Info type mismatch with DS_3GPP_ATIF_SEC_INFO %d ",dynamic_param_ptr->info_type);
            ASSERT(0);
          }
        }
      }/* for Loop */
    }
    else if(cid_list_len == 1)
    {
      /* Process specified context */

      if(TRUE == ds_3gpp_rmsm_atif_read_dynamic_params(cid_list_ptr[0],
                                                       dsat_get_current_subs_id(),
                                DS_3GPP_ATIF_SEC_INFO,dynamic_param_ptr))
      {
        if( DS_3GPP_ATIF_SEC_INFO == dynamic_param_ptr->info_type)
        {
          ds_3gpp_atif_secondary_dynamic_info_type *sec_dynamic_ptr = 
                                            &dynamic_param_ptr->u.sec_dynamic_info;
      
      
          format_buffer->used = (word)snprintf(
                   (char *)&format_buffer->data_ptr[format_buffer->used], 
                   format_buffer->size - format_buffer->used,
                  "%s %d,%d,%d ", "+CGSCONTRDP:",
                   (int)sec_dynamic_ptr->cid,
                   (int)sec_dynamic_ptr->p_cid,
                   (int)sec_dynamic_ptr->bearer_id);
      
        }
        else
        {
          DS_AT_MSG1_ERROR(" Info type mismatch with DS_3GPP_ATIF_SEC_INFO %d ",dynamic_param_ptr->info_type);
          ASSERT(0);
        }
      }
    }

    if(format_buffer->used != 0)
    {
      format_buffer->used--;
      format_buffer->data_ptr[format_buffer->used] = '\0';
    }
    modem_mem_free( dynamic_param_ptr , MODEM_MEM_CLIENT_DATA);

  }

  return result;
}/* etsipkt_cgscontrdp_format_response */

/*===========================================================================
FUNCTION  ETSIPKT_CGEQOSRDP_FORMAT_RESPONSE

DESCRIPTION
This function builds the CID, and related QOS Dynamic parameters of all 
Non Primary Contexts info into a formatted form as mentioned in the 27.007.

   
DEPENDENCIES
  None

RETURN VALUE
  DSAT_OK: if a valid CID was issued
  DSAT_ERROR: otherwise

SIDE EFFECTS
  None

===========================================================================*/
LOCAL dsat_result_enum_type etsipkt_cgeqosrdp_format_response
(
  const byte               *cid_list_ptr,
  const byte                cid_list_len,
  const tokens_struct_type *tok_ptr,
  dsm_item_type            *format_buffer
)
{
  dsat_result_enum_type  result = DSAT_OK;
  uint32                 i;
  uint16                 used;
  ds_3gpp_atif_flow_cid_list_type flow_profile_list;

  memset((void*)&flow_profile_list, 0x0, sizeof(flow_profile_list));
  format_buffer->used = 0;

  /* Get the Active Non-Primary Contexts list */
  ds_3gpp_rmsm_atif_get_all_active_flow_profiles(dsat_get_current_subs_id(),
                                                 &flow_profile_list);

  if(tok_ptr->op == (NA|EQ|QU))
  {
    /* TEST syntax */
    
    format_buffer->used = (word)snprintf(
             (char *)&format_buffer->data_ptr[format_buffer->used], 
             format_buffer->size - format_buffer->used,
             "%s ", "+CGEQOSRDP: (");
    used = format_buffer->used;

    for(i = 0;(i< DS_3GPP_MAX_FLOW_CONTEXT)&&(i< flow_profile_list.data_len); ++i)
    {
      format_buffer->used += (word)snprintf(
           (char *)&format_buffer->data_ptr[format_buffer->used], 
           format_buffer->size - format_buffer->used,
           "%ld,", (long int)flow_profile_list.data_ptr[i]);
    }
    if( used == format_buffer->used )
    {
      /* If atleast one Context is not valid Reset data_ptr */
      format_buffer->used = 0;
      format_buffer->data_ptr[format_buffer->used] = '\0';
    }
    else
    {
      format_buffer->used--; 
      format_buffer->used += (word)snprintf(
               (char *)&format_buffer->data_ptr[format_buffer->used],
               format_buffer->size - format_buffer->used,
               " )");
    }
  }
  else
  {
    /* WRITE syntax */

    dynamic_param_ptr = 
      (ds_3gpp_atif_dynamic_params_info_type *) dsat_alloc_memory(
                               sizeof(ds_3gpp_atif_dynamic_params_info_type), FALSE);
    /* Check for all contexts processing case */
    if( NULL == cid_list_ptr )
    {
      /* Process specified context */

      for(i = 0;(i< DS_3GPP_MAX_FLOW_CONTEXT)&&(i< flow_profile_list.data_len); ++i)
      {

        if(TRUE == ds_3gpp_rmsm_atif_read_dynamic_params(flow_profile_list.data_ptr[i],
                                                         dsat_get_current_subs_id(),
                                  DS_3GPP_ATIF_QOS_INFO,dynamic_param_ptr))
        {
          if( DS_3GPP_ATIF_QOS_INFO == dynamic_param_ptr->info_type)
          {
            ds_3gpp_lte_qos_params_type *qos_dynamic_ptr = 
                                              &dynamic_param_ptr->u.dynamic_qos;

            format_buffer->used += (word)snprintf(
                     (char *)&format_buffer->data_ptr[format_buffer->used], 
                     format_buffer->size - format_buffer->used,
                    "%s %d,%d,%u,%u,%u,%u \n", "+CGEQOSRDP:",
                     flow_profile_list.data_ptr[i],
                     qos_dynamic_ptr->qci,
                     (unsigned int)qos_dynamic_ptr->g_dl_bit_rate,
                     (unsigned int)qos_dynamic_ptr->g_ul_bit_rate,
                     (unsigned int)qos_dynamic_ptr->max_dl_bit_rate,
                     (unsigned int)qos_dynamic_ptr->max_ul_bit_rate);
          }
          else
          {
            DS_AT_MSG1_ERROR(" Info type mismatch with DS_3GPP_ATIF_QOS_INFO %d ",dynamic_param_ptr->info_type);
            ASSERT(0);
          }
        }
      }/* for */
    }
    else if(cid_list_len == 1)
    {
      /* Process specified context */

      if(TRUE == ds_3gpp_rmsm_atif_read_dynamic_params(cid_list_ptr[0],
                                                       dsat_get_current_subs_id(),
                                DS_3GPP_ATIF_QOS_INFO,dynamic_param_ptr))
      {
        if( DS_3GPP_ATIF_QOS_INFO == dynamic_param_ptr->info_type)
        {
          ds_3gpp_lte_qos_params_type *qos_dynamic_ptr = 
                                            &dynamic_param_ptr->u.dynamic_qos;
      
          format_buffer->used = (word)snprintf(
                   (char *)&format_buffer->data_ptr[format_buffer->used], 
                   format_buffer->size - format_buffer->used,
                  "%s %d,%d,%u,%u,%u,%u ", "+CGEQOSRDP:",
                   cid_list_ptr[0],
                   qos_dynamic_ptr->qci,
                   (unsigned int)qos_dynamic_ptr->g_dl_bit_rate,
                   (unsigned int)qos_dynamic_ptr->g_ul_bit_rate,
                   (unsigned int)qos_dynamic_ptr->max_dl_bit_rate,
                   (unsigned int)qos_dynamic_ptr->max_ul_bit_rate);
        }
        else
        {
          DS_AT_MSG1_ERROR(" Info type mismatch with DS_3GPP_ATIF_QOS_INFO %d ",dynamic_param_ptr->info_type);
          ASSERT(0);
        }
      }
    }

    if(format_buffer->used != 0)
    {
      format_buffer->used--;
      format_buffer->data_ptr[format_buffer->used] = '\0';
    }
    modem_mem_free( dynamic_param_ptr , MODEM_MEM_CLIENT_DATA);

  }

  return result;
}/* etsipkt_cgeqosrdp_format_response */

/*===========================================================================
FUNCTION  ETSIPKT_CGTFTRDP_FORMAT_RESPONSE

DESCRIPTION
This function builds the CID, and TFT related Dynamic parameters of all 
Non Primary Contexts info into a formatted form as mentioned in the 27.007.

   
DEPENDENCIES
  None

RETURN VALUE
  DSAT_OK: if a valid CID was issued
  DSAT_ERROR: otherwise

SIDE EFFECTS
  None

===========================================================================*/
LOCAL dsat_result_enum_type etsipkt_cgtftrdp_format_response
(
  const byte               *cid_list_ptr,
  const byte                cid_list_len,
  const tokens_struct_type *tok_ptr,
  dsm_item_type            *res_buff_ptr
)
{
  dsat_result_enum_type  result = DSAT_OK;
  uint32                 j;
  byte addr_mask_buf[MAX_ADDR_SUBNET_STRING_LEN+1];

  uint16                 used;
  ds_3gpp_atif_flow_cid_list_type flow_profile_list;

  memset((void*)&flow_profile_list, 0x0, sizeof(flow_profile_list));
  res_buff_ptr->used = 0;

  /* Get the Active Non-Primary Contexts list */
  ds_3gpp_rmsm_atif_get_all_active_flow_profiles(dsat_get_current_subs_id(),
                                                 &flow_profile_list);

  if(tok_ptr->op == (NA|EQ|QU))
  {
    uint32                 i;
    /* TEST syntax */
    
    res_buff_ptr->used = (word)snprintf(
             (char *)&res_buff_ptr->data_ptr[res_buff_ptr->used], 
             res_buff_ptr->size - res_buff_ptr->used,
             "%s ", "+CGTFTRDP: (");
    used = res_buff_ptr->used;

    for(i = 0;(i< DS_3GPP_MAX_FLOW_CONTEXT)&&(i< flow_profile_list.data_len); ++i)
    {
      res_buff_ptr->used += (word)snprintf(
           (char *)&res_buff_ptr->data_ptr[res_buff_ptr->used], 
           res_buff_ptr->size - res_buff_ptr->used,
           "%ld,",(long int) flow_profile_list.data_ptr[i]);
    }
    if( used == res_buff_ptr->used )
    {
      /* If atleast one Context is not valid Reset data_ptr */
      res_buff_ptr->used = 0;
      res_buff_ptr->data_ptr[res_buff_ptr->used] = '\0';
    }
    else
    {
      res_buff_ptr->used--; 
      res_buff_ptr->used += (word)snprintf(
               (char *)&res_buff_ptr->data_ptr[res_buff_ptr->used],
               res_buff_ptr->size - res_buff_ptr->used,
               " )");
    }
  }
  else
  {
    /* WRITE syntax */
    dynamic_param_ptr = 
      (ds_3gpp_atif_dynamic_params_info_type *) dsat_alloc_memory(
                               sizeof(ds_3gpp_atif_dynamic_params_info_type), FALSE);
    /* Check for all contexts processing case */
    if( NULL == cid_list_ptr )
    {
        uint32                 i;
      /* Process specified context */

      for(i = 0;(i< DS_3GPP_MAX_FLOW_CONTEXT)&&(i< flow_profile_list.data_len); ++i)
      {

        if(TRUE == ds_3gpp_rmsm_atif_read_dynamic_params(flow_profile_list.data_ptr[i],
                                                         dsat_get_current_subs_id(),
                                  DS_3GPP_ATIF_TFT_INFO,dynamic_param_ptr))
        {
          if( DS_3GPP_ATIF_TFT_INFO == dynamic_param_ptr->info_type)
          {
            ds_3gpp_atif_tft_info_type *flow_dynamic_ptr = 
                                              &dynamic_param_ptr->u.dynamic_tft[0];

            for(j = 0;j < DS_3GPP_MAX_FILTERS ; j++)
            {
              if(TRUE == flow_dynamic_ptr[j].tft_params.valid_flg)
              {
                uint16 check_size = 0;

                if ( DSAT_OK != dsatutil_convert_addr_mask( INT_TO_STRING,
                                 &dynamic_param_ptr->u.dynamic_tft[j].tft_params.src_addr_mask,
                                                            addr_mask_buf ) )
                {
                  DS_AT_MSG0_HIGH("Error validating Address Mask parameter");
                  return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
                }

                /*One loop below  50 for other params +length of mask_buf */
                check_size = 60 + strlen((const char *)addr_mask_buf);

                CHECK_APPEND_RESPONSE_BUFFER_NEW (((uint16)DSM_BUFFER_THRESHOLD - check_size));

                res_buff_ptr->used += (word)snprintf(
                      (char *)&res_buff_ptr->data_ptr[res_buff_ptr->used], 
                       res_buff_ptr->size - res_buff_ptr->used,
                       "%s %d,%d,%d,\"%s\",%d,%d.%d,%d.%d,%X,%d.%d,%X,%d,%d\n",
                       "+CGTFTRDP:",
                       flow_profile_list.data_ptr[i],
                       flow_dynamic_ptr[j].tft_params.filter_id,
                       flow_dynamic_ptr[j].tft_params.eval_prec_id,
                       addr_mask_buf,
                       flow_dynamic_ptr[j].tft_params.prot_num,
                       flow_dynamic_ptr[j].tft_params.dest_port_range.from,
                       flow_dynamic_ptr[j].tft_params.dest_port_range.to,
                       flow_dynamic_ptr[j].tft_params.src_port_range.from,
                       flow_dynamic_ptr[j].tft_params.src_port_range.to,
                       (int)flow_dynamic_ptr[j].tft_params.ipsec_spi,
                       (flow_dynamic_ptr[j].tft_params.tos_mask >> 8),
                       (uint8)(flow_dynamic_ptr[j].tft_params.tos_mask & 0xFF ),
                       (unsigned int)flow_dynamic_ptr[j].tft_params.flow_label,
                       (uint8)flow_dynamic_ptr[j].direction,
                        flow_dynamic_ptr[j].tft_params.filter_id);
              }
            }/* Inner for Loop */
          }
          else
          {
            DS_AT_MSG1_ERROR(" Info type mismatch with DS_3GPP_ATIF_TFT_INFO %d ",dynamic_param_ptr->info_type);
            ASSERT(0);
          }
        }
      }/* Outer for Loop */
    }
    else if(cid_list_len == 1)
    {
      /* Process specified context */

      if(TRUE == ds_3gpp_rmsm_atif_read_dynamic_params(cid_list_ptr[0],
                                                       dsat_get_current_subs_id(),
                                DS_3GPP_ATIF_TFT_INFO,dynamic_param_ptr))
      {
        if( DS_3GPP_ATIF_TFT_INFO == dynamic_param_ptr->info_type)
        {
          ds_3gpp_atif_tft_info_type *flow_dynamic_ptr = 
                                            &dynamic_param_ptr->u.dynamic_tft[0];

          for(j = 0;j < DS_3GPP_MAX_FILTERS ; j++)
          {
            if(TRUE == flow_dynamic_ptr[j].tft_params.valid_flg)
            {
              uint16 check_size = 0;
              if ( DSAT_OK != dsatutil_convert_addr_mask( INT_TO_STRING,
                               &dynamic_param_ptr->u.dynamic_tft[j].tft_params.src_addr_mask,
                                                          addr_mask_buf ) )
              {
                DS_AT_MSG0_HIGH("Error validating Address Mask parameter");
                return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
              }

              /*One loop below  50 for other params +length of mask_buf */
              check_size = 60 + strlen((const char *)addr_mask_buf);
              
              CHECK_APPEND_RESPONSE_BUFFER_NEW (((uint16)DSM_BUFFER_THRESHOLD - check_size));

              res_buff_ptr->used += (word)snprintf(
                    (char *)&res_buff_ptr->data_ptr[res_buff_ptr->used], 
                     res_buff_ptr->size - res_buff_ptr->used,
                     "%s %d,%d,%d,\"%s\",%d,%d.%d,%d.%d,%X,%d.%d,%X,%d,%d\n",
                     "+CGTFTRDP:",
                     cid_list_ptr[0],
                     flow_dynamic_ptr[j].tft_params.filter_id,
                     flow_dynamic_ptr[j].tft_params.eval_prec_id,
                     addr_mask_buf,
                     flow_dynamic_ptr[j].tft_params.prot_num,
                     flow_dynamic_ptr[j].tft_params.dest_port_range.from,
                     flow_dynamic_ptr[j].tft_params.dest_port_range.to,
                     flow_dynamic_ptr[j].tft_params.src_port_range.from,
                     flow_dynamic_ptr[j].tft_params.src_port_range.to,
                     (int)flow_dynamic_ptr[j].tft_params.ipsec_spi,
                     (flow_dynamic_ptr[j].tft_params.tos_mask >> 8),
                     (uint8)(flow_dynamic_ptr[j].tft_params.tos_mask & 0xFF ),
                     (unsigned int)flow_dynamic_ptr[j].tft_params.flow_label,
                     (uint8)flow_dynamic_ptr[j].direction,
                     flow_dynamic_ptr[j].tft_params.filter_id);
            }
          }/* Inner for Loop */
        }
        else
        {
          DS_AT_MSG1_ERROR(" Info type mismatch with DS_3GPP_ATIF_TFT_INFO %d ",dynamic_param_ptr->info_type);
          ASSERT(0);
        }
      }
    }

    if(res_buff_ptr->used != 0)
    {
      res_buff_ptr->used--;
      res_buff_ptr->data_ptr[res_buff_ptr->used] = '\0';
    }
    modem_mem_free( dynamic_param_ptr , MODEM_MEM_CLIENT_DATA);

  }

  return result;
}/* etsipkt_cgtftrdp_format_response */


/*===========================================================================
FUNCTION  ETSIPKT_CGDATA_GET_CID_INFO

DESCRIPTION
  Gets the defined CID from the PDP table.

DEPENDENCIES
  None

RETURNS
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
dsat_cme_error_e_type etsipkt_cgdata_get_cid_info(byte *cid)
{
  uint8 i;
  uint8 no_of_cids_defined = 0;
  ds_umts_pdp_context_type pdp_context;
  
  memset((void*)&pdp_context, 0x0, sizeof(pdp_context));
  for(i = 0; i < DS_UMTS_MAX_PDP_PROFILE_NUM; ++i)
  {
    if((DS_UMTS_PDP_SUCCESS == 
            ds_umts_get_pdp_profile_context_info_per_subs((uint16)(i+1), 
                                 dsat_get_current_subs_id(),
                                 &pdp_context )) &&
                                 pdp_context.valid_flg == TRUE)
    {
    
      no_of_cids_defined++;
      if(no_of_cids_defined > 1 || pdp_context.secondary_flag)
      {
        return DSAT_CME_OP_NOT_SUPPORTED;
      }
      *cid = i;
    }
  }

  if(no_of_cids_defined == 0)
  {
    return DSAT_CME_INVALID_INDEX;
  }

  return DSAT_CME_NO_ERROR;
} /* etsipkt_cgdata_get_cid_info */


/*===========================================================================

FUNCTION DSATETSIPKT_EXEC_CGDATA_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It handles the +CGDATA command.
  This command initiates a packet data call.

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatetsipkt_exec_cgdata_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type  result = DSAT_OK;

  if(tok_ptr->op == (NA|EQ|QU))   /* Test command */ 
  {
    result = dsatparm_exec_param_cmd(
                                     mode,
                                     parse_table,
                                     tok_ptr,
                                     res_buff_ptr
                                    );
  }
  else if((tok_ptr->op == (NA|EQ))   /* Default arg */
     || (tok_ptr->op == (NA|EQ|AR)))  /* Write command */
  {
    dsat_cme_error_e_type  cid_info_result;

    memset((void*)&dsat_cgdata_context, 0, sizeof(dsat_cgdata_info_s_type));

    if(tok_ptr->op == (NA|EQ))
    {
      /* We would take the default values by going through the PDP table for 
         which the context IDs have been defined. */
      if((cid_info_result = 
                      etsipkt_cgdata_get_cid_info(&dsat_cgdata_context.cid))
                          != DSAT_CME_NO_ERROR)
      {
        return dsat_send_cme_error(cid_info_result);
      }
    }
    else if(tok_ptr->op == (NA|EQ|AR))
    {
      dsat_num_item_type       cid_result = FALSE;
      byte out_string[4];
      ds_umts_pdp_context_type pdp_context;
      word str_len = sizeof(out_string);
      memset((void*)&pdp_context, 0x0, sizeof(pdp_context));

      /* Verifying for lower and upper case */

      if(dsatutil_strip_quotes_out((const byte *)tok_ptr->arg[0], 
                                   out_string, str_len) != TRUE)
      {
        return DSAT_ERROR;
      }

      if((dsatutil_strcmp_ig_sp_case((const byte *)out_string, 
                                     (const byte *)"PPP") != 0)     || 
         (tok_ptr->args_found > 2))
      {
        /* Potentially more than one PDP context activation is requested 
         or a different L2P is requested. We currently do not support it */
        return dsat_send_cme_error(DSAT_CME_OP_NOT_SUPPORTED);
      }

      if(VALID_TOKEN(1))
      {
        if(dsatutil_atoi(&cid_result, tok_ptr->arg[1], 10) != ATOI_OK)
        {
          return dsat_send_cme_error(DSAT_CME_PHONE_FAILURE);
        }
        else
        {
          /* Check if the CID is within range of supported set or not */

          if(cid_result < 1 || cid_result > DS_UMTS_MAX_PDP_PROFILE_NUM )
          {
            return DSAT_ERROR;
          }

          /* Verify against PDP table whether this context ID 
           is defined */
          if((DS_UMTS_PDP_SUCCESS == 
                             ds_umts_get_pdp_profile_context_info_per_subs((uint16)(cid_result),
                                       dsat_get_current_subs_id(),
                                       &pdp_context )) &&
                                       pdp_context.valid_flg == TRUE)
          {

            if(pdp_context.secondary_flag == FALSE)
            {
              dsat_cgdata_context.cid =(byte)cid_result;
              return etsipkt_initiate_packet_call(res_buff_ptr);
            }
            else
            {
              /* We currently do not support secondary PDP activations 
                 through TE */
               return dsat_send_cme_error(DSAT_CME_OP_NOT_SUPPORTED);
            }
          }
          else
          {
            /* If we are here, there was no match for the context ID 
               specified */
            return dsat_send_cme_error(DSAT_CME_INVALID_INDEX);
          }
        }
      }
      else
      {
        if((cid_info_result = 
                 etsipkt_cgdata_get_cid_info(&dsat_cgdata_context.cid))
                          != DSAT_CME_NO_ERROR)
        {
          return dsat_send_cme_error(cid_info_result);
        }
      }
    }
  }
  else
  {
    result = DSAT_ERROR;
  }

  return result;
} /* dsatetsipkt_exec_cgdata_cmd */


/*===========================================================================
FUNCTION  ETSIPKT_INITIATE_PACKET_CALL

DESCRIPTION
  Initiate packet data call by looking at the context ID for which this call 
  makes sense

DEPENDENCIES
  None

RETURNS
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
LOCAL dsat_result_enum_type etsipkt_initiate_packet_call
(
  dsm_item_type            *res_buff_ptr
)
{
  tokens_struct_type        cgdata_tok_ptr;
  char                      pkt_dial_string[15];
  uint8                     used_buffer = 0;

  /* Dial the packet call - 
     Reusing AT command ATD call functionality */

  /* Suppressing the L2P part as we support PPP alone for now 
     and atd doesn't expect this */
  used_buffer = (uint8) snprintf(pkt_dial_string, 
                                       sizeof(pkt_dial_string), "*%d***" ,
                                       DSATETSIPKT_GPRS_SERVICE_CODE);

  /* Activate PDP context as defined in +CGDATA command earlier */
  if(used_buffer < 15)
  {
    used_buffer += (uint8)snprintf(&pkt_dial_string[used_buffer],
                                       sizeof(pkt_dial_string)-used_buffer,
                                       "%d#",
                                       dsat_cgdata_context.cid);
  }

  memset((void*)&cgdata_tok_ptr, 0, sizeof(tokens_struct_type));

  /* Only tok_ptr's arg[0] and end_of_line fields are of interest for 
     dial atd function  */

  cgdata_tok_ptr.arg[0] = (byte *)pkt_dial_string;
  cgdata_tok_ptr.end_of_line = (byte *)(pkt_dial_string + 
                                        strlen(pkt_dial_string));

  return dsatact_exec_atd_cmd(dsatcmdp_at_state, 
           NULL, &cgdata_tok_ptr, res_buff_ptr);
} /* etsipkt_initiate_packet_call */


/*===========================================================================
FUNCTION  ETSIPKT_ADD_GPRS_MSG_TO_QUEUE

DESCRIPTION
  Buffers the GPRS event report in MT.

DEPENDENCIES
  None

RETURNS
  None

SIDE EFFECTS
  None

===========================================================================*/
LOCAL void etsipkt_add_gprs_msg_to_queue
(
  char    *buffer,
  uint16   buf_len,
  ds_subs_e_type subs_id
)
{
  etsipkt_gprs_msg_type *msg_ptr;
  dsatetsipkt_gprs_msg_ms_info     *gprs_dd_ms_val = NULL;

  if(DSAT_FAILURE == dsatutil_get_base_addr(DSAT_MS_MD_VALS,(void **)&gprs_dd_ms_val,subs_id))
  {
    return;
  }

  /* Get a command buffer from the free command queue */
  if( (msg_ptr = q_get(&gprs_dd_ms_val->etsipkt_gprs_msg_free_q)) == NULL )
  {
    /* Get the first element in the actual queue 
       Strategy -circular buffer, discard the oldest ones */
    if( (msg_ptr = q_get(&gprs_dd_ms_val->etsipkt_gprs_msg_q)) == NULL )
    {
      DS_AT_MSG0_ERROR("Failed to buffer the GPRS event report in MT");
      return;
    }

    /* Add event data to it */
    (void) dsatutil_memscpy((void*)msg_ptr->event_data,
            GPRS_EVENT_DATA_SIZE,(void*)buffer,MIN(buf_len,GPRS_EVENT_DATA_SIZE));    
    msg_ptr->event_data_len = MIN(buf_len,GPRS_EVENT_DATA_SIZE);

    /* Put the message on the message queue */
    q_put( &gprs_dd_ms_val->etsipkt_gprs_msg_q, &msg_ptr->link );
  }
  else
  {
    /* Add event data to the free list */
    (void) dsatutil_memscpy((void*)msg_ptr->event_data,
            GPRS_EVENT_DATA_SIZE,(void*)buffer,MIN(buf_len,GPRS_EVENT_DATA_SIZE));   
    msg_ptr->event_data_len = MIN(buf_len,GPRS_EVENT_DATA_SIZE);

    /* Put the message on the message queue */
    q_put( &gprs_dd_ms_val->etsipkt_gprs_msg_q, &msg_ptr->link );
  }

  return;
} /* etsipkt_add_gprs_msg_to_queue */


/*===========================================================================
FUNCTION  ETSIPKT_CLEAR_GPRS_EVENTS_BUFFER

DESCRIPTION
  Clears the GPRS event buffer queue

DEPENDENCIES
  None

RETURNS
  None

SIDE EFFECTS
  None

===========================================================================*/
LOCAL void etsipkt_clear_gprs_events_buffer
(
 ds_subs_e_type subs_id
)
{
  etsipkt_gprs_msg_type  *msg_ptr;
  dsatetsipkt_gprs_msg_ms_info     *gprs_dd_ms_val = NULL;
  
  if(DSAT_FAILURE == dsatutil_get_base_addr(DSAT_MS_MD_VALS,(void **)&gprs_dd_ms_val,subs_id))
  {
    return;
  }

  /* Get messages from the queue until the queue is empty */
  while( (msg_ptr = (etsipkt_gprs_msg_type *)
                    q_get( &gprs_dd_ms_val->etsipkt_gprs_msg_q )) != NULL )
  {
    /* Return the message buffer to the free queue */
    q_put( &gprs_dd_ms_val->etsipkt_gprs_msg_free_q, &msg_ptr->link);
  }
} /* etsipkt_clear_gprs_events_buffer */


/*===========================================================================
FUNCTION  DSATETSIPKT_REPORT_GPRS_EVENT_TO_TE

DESCRIPTION
  Reports the GPRS event report buffer in MT.

DEPENDENCIES
  None

RETURNS
  None

SIDE EFFECTS
  None

===========================================================================*/
void dsatetsipkt_report_gprs_event_to_te
(
  dsat_cgerep_e_type event_type, 
  void *event_data,
  ds_subs_e_type subs_id
)
{
  char    buffer[GPRS_EVENT_DATA_SIZE];
  char    ip_address[MAX_IPADDR_STR_LEN+1];
  uint16  buf_len = 0;
  dsat_num_item_type cgerep_val ;
  dsat_cgerep_evt_info_s_type *  cgerep_evt_info = NULL;
  ds_umts_pdp_context_type pdp_context;
  ds_umts_pdp_addr_type temp_pdp_addr;

  memset ( ip_address, 0, sizeof(ip_address));
  memset((void*)&pdp_context, 0x0, sizeof(pdp_context));
  memset(&temp_pdp_addr,0x00,sizeof(temp_pdp_addr));
  if (event_type >= ARR_SIZE(etsipkt_cgerep_event_buffer))
  {
    DS_AT_MSG1_ERROR("Invalid event type %d",(int)event_type);
    return;
  }

  buf_len = (uint16)strlen(etsipkt_cgerep_event_buffer[event_type]);
  buf_len = MIN(buf_len,sizeof(buffer) );
  DS_AT_MSG2_HIGH("In dsatetsipkt_report_gprs_event_to_te event = %d  subs = %d",event_type,subs_id);
  (void) dsatutil_memscpy((void*)buffer,
          GPRS_EVENT_DATA_SIZE,(void*)etsipkt_cgerep_event_buffer[event_type],buf_len);
  switch (event_type)
  {
    case DSAT_MS_INITIATED_PDP_DEACTIVATE:
    case DSAT_NW_INITIATED_PDP_DEACTIVATE:
    case DSAT_NW_INITIATED_PDP_REACTIVATE:
      {
	  	
        if((DS_UMTS_PDP_SUCCESS == 
                         ds_umts_get_pdp_profile_context_info_per_subs((uint16)((dsat_cgerep_evt_info_s_type*)event_data)->cid, 
                                                                subs_id,
                                                                &pdp_context )) &&
                                                                pdp_context.valid_flg == TRUE)
        {
          if(DSAT_OK == ds_3gpp_rmsm_atif_get_pdp_addr(
                                      (uint32)(int)((dsat_cgerep_evt_info_s_type*)event_data)->cid,
                                      subs_id,
                                      &pdp_context.pdp_addr))
          {
            if( pdp_context.pdp_addr.ip_vsn == DS_IP_V4)
            {
              temp_pdp_addr.ip_vsn = DS_IP_V4; 
              temp_pdp_addr.pdp_addr_ipv4 = ps_ntohl(pdp_context.pdp_addr.pdp_addr_ipv4);
              dsatutil_convert_ipaddr(INT_TO_STRING,&temp_pdp_addr,(byte *)ip_address);
			  	
            }
            else if( pdp_context.pdp_addr.ip_vsn == DS_IP_V6 || pdp_context.pdp_addr.ip_vsn == DS_IP_V4V6 )
            {
              temp_pdp_addr.ip_vsn = DS_IP_V6;
              temp_pdp_addr.pdp_addr_ipv6.in6_u.u6_addr64[0] = ps_ntohll(pdp_context.pdp_addr.pdp_addr_ipv6.in6_u.u6_addr64[1]);
              temp_pdp_addr.pdp_addr_ipv6.in6_u.u6_addr64[1] = ps_ntohll(pdp_context.pdp_addr.pdp_addr_ipv6.in6_u.u6_addr64[0]);
              dsatutil_convert_ipaddr(INT_TO_STRING,&temp_pdp_addr,(byte *)ip_address);
            }
            buf_len += (uint16)
            snprintf((char *)&buffer[buf_len],
            sizeof(buffer)-buf_len,
            "IP, %s, %d", 
            ip_address, 
            (int)((dsat_cgerep_evt_info_s_type*)event_data)->cid);
          }
          else
          {
            if( pdp_context.pdp_addr.ip_vsn == DS_IP_V6 || pdp_context.pdp_addr.ip_vsn == DS_IP_V4V6 )
            {
              temp_pdp_addr.ip_vsn = DS_IP_V6;
            }
            else
            {
              temp_pdp_addr.ip_vsn = DS_IP_V4;
            }
            dsatutil_convert_ipaddr(INT_TO_STRING,&temp_pdp_addr,(byte *)ip_address);
            buf_len += (uint16)
            snprintf((char *)&buffer[buf_len],
            sizeof(buffer)-buf_len,
            "IP, %s, %d", 
            ip_address, 
            (int)((dsat_cgerep_evt_info_s_type*)event_data)->cid);
          }
        }
      }
      break;
    case DSAT_PDN_ACTIVATE:
    case DSAT_PDN_DEACTIVATE:
      buf_len += (uint16)snprintf((char *)&buffer[buf_len],
                                          sizeof(buffer)-buf_len,
                                          "%d", 
                    (int)((dsat_cgerep_evt_info_s_type*)event_data)->cid);
      break;
    case DSAT_DED_NW_DED_ACTIVATE:
    case DSAT_DED_NW_DEACTIVATE:
    case DSAT_MS_ACTIVATE:
    case DSAT_MS_DEACTIVATE:
      cgerep_evt_info = (dsat_cgerep_evt_info_s_type*)event_data;

      buf_len += (uint16)snprintf((char *)&buffer[buf_len],
                                          sizeof(buffer)-buf_len,
                                          "%d %d %d", 
                                          (int)cgerep_evt_info->p_cid,
                                          (int)cgerep_evt_info->cid,
                                          cgerep_evt_info->evt_type);
      break;
    case DSAT_NW_MODIFY:
    case DSAT_MS_MODIFY:
      cgerep_evt_info = (dsat_cgerep_evt_info_s_type*)event_data;

      buf_len += (uint16)snprintf((char *)&buffer[buf_len],
                                          sizeof(buffer)-buf_len,
                                          "%d %d %d", 
                                          (int)cgerep_evt_info->cid,
                                          cgerep_evt_info->change_reason,
                                          cgerep_evt_info->evt_type);
      break;
    default:;
  }/* Switch */
  cgerep_val =(dsat_num_item_type)dsatutil_get_val(
                                          DSATETSI_EXT_CGEREP_IDX,subs_id,0,NUM_TYPE);
  if ( !dsatcmdp_block_indications() )
  {
    /* TA-TE link is not reserved, forward the results directly to TE 
       except when mode is DSAT_CGEREP_BUFFER_AT_MT */
    if ( ( cgerep_val == DSAT_CGEREP_BUFFER_DISCARD_FWD_TE ) ||
         ( cgerep_val == DSAT_CGEREP_BUFFER_FWD_TE ))
    {
      /* Fwd to TE as the MT is not in online-data mode */
      dsm_item_type *res_buff_ptr;
  
      res_buff_ptr = dsat_dsm_new_buffer(DSM_DS_LARGE_ITEM_POOL, TRUE);
      if (NULL != res_buff_ptr)
      {
        (void) dsatutil_memscpy((void*)res_buff_ptr->data_ptr,
                res_buff_ptr->size,(void*)buffer,MIN(buf_len,sizeof(buffer)));
        
        res_buff_ptr->used = MIN(buf_len,sizeof(buffer));
        (void)dsatcmdp_send_urc(subs_id, res_buff_ptr, DSAT_COMPLETE_RSP);
      }
    }
    /* TA-TE link is not reserved but mode is  
       DSAT_CGEREP_BUFFER_AT_MT so buffer it*/
    else if (  cgerep_val == DSAT_CGEREP_BUFFER_AT_MT )
    {
      etsipkt_add_gprs_msg_to_queue(buffer, buf_len, subs_id);
    }
  }
  else
  {
    /* TA-TE link is reserved. Buffer the result only when 
       mode is DSAT_CGEREP_BUFFER_FWD_TE or DSAT_CGEREP_BUFFER_AT_MT */
    /* Buffer the unsolicited result codes in MT. No forwarding 
       to TE needed */
    if ( ( cgerep_val == DSAT_CGEREP_BUFFER_FWD_TE )||
         ( cgerep_val == DSAT_CGEREP_BUFFER_AT_MT ))
    {
      etsipkt_add_gprs_msg_to_queue(buffer, buf_len, subs_id);
    }
  }
} /* dsatetsipkt_report_gprs_event_to_te */


/*===========================================================================
FUNCTION  DSATETSIPKT_FLUSH_GPRS_EVENT_INDICATIONS

DESCRIPTION
  Flushes the buffered GPRS event reports to TE

DEPENDENCIES
  None

RETURNS
  None

SIDE EFFECTS
  None

===========================================================================*/
void dsatetsipkt_flush_gprs_event_indications
(
 ds_subs_e_type subs_id
)
{
  dsm_item_type          *res_buff_ptr;
  etsipkt_gprs_msg_type  *msg_ptr;
  dsatetsipkt_gprs_msg_ms_info     *gprs_dd_ms_val = NULL;
  
  if(DSAT_FAILURE == dsatutil_get_base_addr(DSAT_MS_MD_VALS,(void **)&gprs_dd_ms_val,subs_id))
  {
    return;
  }
  /* Get messages from the queue until the queue is empty */
  while( (msg_ptr = (etsipkt_gprs_msg_type *)
                    q_get( &gprs_dd_ms_val->etsipkt_gprs_msg_q )) != NULL )
  {
    res_buff_ptr = dsat_dsm_new_buffer(DSM_DS_LARGE_ITEM_POOL, TRUE);
    if (NULL != res_buff_ptr)
    {
      (void) dsatutil_memscpy((void*)res_buff_ptr->data_ptr,
              res_buff_ptr->size,(void*) msg_ptr->event_data,
              MIN(msg_ptr->event_data_len,GPRS_EVENT_DATA_SIZE));
    
      res_buff_ptr->used =  MIN(msg_ptr->event_data_len,GPRS_EVENT_DATA_SIZE);
      (void)dsatcmdp_send_respose_to_te (res_buff_ptr, DSAT_COMPLETE_RSP);
    }

    /* Return the message buffer to the free queue */
    q_put( &gprs_dd_ms_val->etsipkt_gprs_msg_free_q, &msg_ptr->link);
  }
} /* dsatetsipkt_flush_gprs_event_indications */


/*===========================================================================
FUNCTION  DSATETSIPKT_INIT_SERVICE_PREFERENCE_FROM_NV

DESCRIPTION
  Retrieves MS's service preference option from NV.

DEPENDENCIES
  None

RETURNS
  None

SIDE EFFECTS
  None

===========================================================================*/
void dsatetsipkt_init_service_preference_from_nv(void)
{
  nv_item_type      nv_item;   /* NV interface data */  
  dsatcmif_servs_state_ms_info  *ph_ss_ms_val = NULL;
  dsat_num_item_type  index;
  for(index = 0; index < MAX_SUBS; index++)
  {
    if(DSAT_FAILURE == dsatutil_get_base_addr(DSAT_SS_PH_MS_VALS,(void **)&ph_ss_ms_val,index))
    {
      return;
    }

    if(dsatutil_get_nv_item_per_subs(NV_SERVICE_DOMAIN_PREF_I, &nv_item, index) == NV_DONE_S)
    {
      ph_ss_ms_val->dsat_ms_service_preference = (cm_srv_domain_pref_e_type)nv_item.service_domain_pref.srv_domain;
    }
  }
} /* dsatetsipkt_init_service_preference_from_nv */

/*===========================================================================

FUNCTION DSATETSIPKT_EXEC_CGCMOD_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It handles the +CGCMOD command.
  This command modifies the Qos, TFT and APN of active PDP contexts.  

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success.

SIDE EFFECTS
  None
  
===========================================================================*/
/* ARGSUSED */
dsat_result_enum_type dsatetsipkt_exec_cgcmod_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type result = DSAT_OK;
  /*---------------------------------------------------------
    Processing for a WRITE command
  -----------------------------------------------------------*/
  if ( tok_ptr->op == (NA|EQ|AR) )
  {
    /* Build the processing order for CID list */
    if ( !etsipkt_build_context_processing_list(tok_ptr, 
                                                DSAT_CQM_CGCMOD_WRITE, 
                                                NULL) )
    {
      DS_AT_MSG0_ERROR("+CGCMOD <cid> list contained invalid parameter");
      return DSAT_ERROR;
    }

      result = ds_3gpp_rmsm_atif_cgcmod_handler(
                 dsat_pdp_cid[dsat_pdp_state.change_index],
                 dsat_get_current_subs_id(),
                   NULL);

      if(result == DSAT_ASYNC_CMD)
      {
        DS_AT_MSG1_MED("CGCMOD processing in progress, CID = %d",
                dsat_pdp_cid[dsat_pdp_state.change_index]);
      dsat_pdp_state.pending = TRUE;
    }
  }
  /*---------------------------------------------------------
    Processing for a TEST command
  -----------------------------------------------------------*/
  else if (tok_ptr->op == (NA|EQ|QU))
  {
    ds_3gpp_atif_profile_status_info_type profile_status;
    byte num_active;
    uint8 loop;

    memset(&profile_status,0,sizeof(profile_status));

    if(DSAT_OK != ds_3gpp_rmsm_atif_cgact_query(&profile_status,dsat_get_current_subs_id()))
    {
      DS_AT_MSG0_ERROR("CGACT query failed for CGCMOD query");
      return DSAT_ERROR;
    }

    res_buff_ptr->used = (uint16)snprintf( (char *)res_buff_ptr->data_ptr, 
                                       res_buff_ptr->size,
                                       "+CGCMOD: (" );

  /* Go through connect state table and add 
     CID of each active context to response list... */
    for (loop = 1,num_active = 0; loop < DS_UMTS_MAX_PDP_PROFILE_NUM; loop++) 
    {     
      DS_AT_MSG1_HIGH("CGCMOD query executing: loop - %d ",loop);

  	/* Ensure context is valid OR currently active */
      if(( profile_status.list[loop-1].valid_flag != TRUE )&&
             ( profile_status.list[loop-1].act_status != DS_3GPP_ATIF_PDP_ACTIVATED))
      {
	      DS_AT_MSG1_HIGH("MHM CGCMOD lnvalid loop : %d ",loop);
         continue;
      }
      
      if(profile_status.list[loop-1].act_status == DS_3GPP_ATIF_PDP_ACTIVATED)
      {
        res_buff_ptr->used += 
          (uint16)snprintf( (char *)(&res_buff_ptr->data_ptr[res_buff_ptr->used]),
                        res_buff_ptr->size - res_buff_ptr->used,
                          "%d,", loop );
        num_active++;
      }
    }
  /* Step back over last comma */
    if ( num_active )
    {
      res_buff_ptr->used--;
    }

    res_buff_ptr->used += (uint16)
      snprintf( (char *)(&res_buff_ptr->data_ptr[res_buff_ptr->used]),
                    res_buff_ptr->size - res_buff_ptr->used,
                    ")" );
    return DSAT_OK;
  }
  else
  {
    /* unsupported operation */
    result = DSAT_ERROR;
  }
  return result;
} /* dsatetsipkt_exec_cgcmod_cmd */


#ifdef FEATURE_DATA_TE_MT_PDP
/*===========================================================================

FUNCTION DSATETSIPKT_EXEC_CGAUTO_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It handles the +CGAUTO command.
  This command disables or enables an automatic positive response 
  (auto-answer) to the receipt of a Request PDP Context Activation message 
  from the network.
  
DEPENDENCIES
  None.

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK : if it is a success.

SIDE EFFECTS
  None
  
===========================================================================*/
/* ARGSUSED */
dsat_result_enum_type dsatetsipkt_exec_cgauto_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  #define DSAT_CGATT_VAL_0   0
  dsat_result_enum_type result = DSAT_OK;
  dsat_num_item_type    qcsimapp_val = dsat_get_qcsimapp_val();

  if (DSAT_OK != dsatparm_exec_param_cmd(
                                         mode,
                                         parse_table,
                                         tok_ptr,
                                         res_buff_ptr
                                        ))
  {
    return DSAT_ERROR;
  }

/*---------------------------------------------------------
  Processing for a WRITE command
-----------------------------------------------------------*/
  if ( (tok_ptr->op == (NA|EQ|AR)) && 
       ((dsat_num_item_type)dsatutil_get_val(DSATETSI_EXT_CGAUTO_IDX,
                               0,0,NUM_TYPE) == DSAT_CGAUTO_AUTO_ANS_ON ))
  {
#ifdef FEATURE_ETSI_ATTACH
    /* if packet domain is detached attempt PS attach */
    if ( DSAT_CGATT_VAL_0 == (dsat_num_item_type)dsatutil_get_val(
                                  DSATETSI_EXT_ACT_CGATT_ETSI_IDX,qcsimapp_val,0,NUM_TYPE))
    {
      result = dsatetsicmif_attach_ps ();
    }
    else
    {
      DS_AT_MSG0_HIGH("Already packet domain attached");
    }
#endif /* FEATURE_ETSI_ATTACH */
  }

  return result;
} /* dsatetsipkt_exec_cgauto_cmd */


/*===========================================================================

FUNCTION DSATETSIPKT_EXEC_CGANS_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It handles the +CGANS command.
  This command is used to answer a incoming MT PDP call.
  
DEPENDENCIES
  None.

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_NO_RSP : if answer cb is called.
    DSAT_OK : if the test command succeeds.

SIDE EFFECTS
  None
  
===========================================================================*/
/* ARGSUSED */
dsat_result_enum_type dsatetsipkt_exec_cgans_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  
#define RESPONSE_TYPE_MAX 2
  dsat_result_enum_type result = DSAT_OK;
  dsat_pdp_answer_data_type  answer_data;
  dsat_num_item_type response_type = 0;
  dsat_num_item_type cid;
  uint8 index =0;
  uint32 flag=0;
  dsat_error_info_s_type    err_info;

  err_info.errval = DSAT_ERR_NO_ERROR;
  err_info.arg_num = 0;
  /*---------------------------------------------------------
    Processing for a WRITE command
  -----------------------------------------------------------*/
  if ( ( mode == DSAT_CMD ) && (tok_ptr->op == (NA|EQ|AR)) )
  {
    if ( (dsat_num_item_type)dsatutil_get_val( DSATETSI_EXT_CGAUTO_IDX,
                                     0,0,NUM_TYPE) == DSAT_CGAUTO_AUTO_ANS_ON )
    {
      DS_AT_MSG0_ERROR(" +CGANS not allowed when +CGAUTO = 1");
      return  dsat_send_cme_error(DSAT_CME_OP_NOT_ALLOWED);
    }
    if (VALID_TOKEN(0))
    {
      /* Convert response parameter string to integer */
      if (ATOI_OK !=
          dsatutil_atoi(&response_type, tok_ptr->arg[0], 10))
      {
        err_info.errval = DSAT_ERR_ATOI_CONVERSION_FAILURE;
        err_info.arg_num = 0;
        goto send_error;
      }

      if ((dsat_num_item_type)RESPONSE_TYPE_MAX <= response_type)
      {
        err_info.errval = DSAT_ERR_PARAMETER_OUT_OF_RANGE;
        err_info.arg_num = 0;
        goto send_error;
      }
      if( response_type == 0) 
      {
        /* MT PDP request is rejected when response type = 0 */
        if ( dial_string_handler.pdp_hangup_cb != NULL )
        {
          dial_string_handler.pdp_hangup_cb ();
          result = DSAT_ATH_CMD;
        }
        else
        {
          DS_AT_MSG0_HIGH("Unregistered MT PDP hangup cb handler");
          result = DSAT_NO_CARRIER;
        }
      }
      else 
      {
        /* MT PDP request is accepted if response_type = 1 */
        /* continue processing if there is a valid pdp_answer_cb */
        if ( VALID_TOKEN(1))
        {
          byte out_string[4];
          word str_len = sizeof(out_string);

           /* Verifying for lower and upper case */

           if(dsatutil_strip_quotes_out((const byte *)tok_ptr->arg[1], 
                                   out_string, str_len) != TRUE)
           {
             err_info.errval = DSAT_ERR_QUOTE_STRIP_FAILURE;
             err_info.arg_num = 1;
             goto send_error;
           }

          if (dsatutil_strcmp_ig_sp_case((const byte *)out_string, 
                                     (const byte *)"PPP") != 0)
          {
            /* A different L2P is requested. We currently do not support it */
            return dsat_send_cme_error(DSAT_CME_OP_NOT_SUPPORTED);
          }
        } /* VALID_TOKEN(1) */
        memset ((void*)&answer_data, 0, sizeof(answer_data));
        /* the first param is response_type and second L2P type */
        for(index = 2;index < tok_ptr->args_found ;index++) 
        {
          if(VALID_TOKEN(index))
          {
            byte *parm_str = tok_ptr->arg[index];
            err_info.arg_num = index;
            if (ATOI_OK !=
              dsatutil_atoi(&cid, parm_str, 10))
            {
              err_info.errval = DSAT_ERR_ATOI_CONVERSION_FAILURE;
              err_info.arg_num = index;
              goto send_error;
            }
            /* Test for CID within valid range */
            if ((0 == cid) || (DS_UMTS_MAX_PDP_PROFILE_NUM < cid))
            {
              err_info.errval = DSAT_ERR_PARAMETER_OUT_OF_RANGE;
              goto send_error;
            }
            if( answer_data.number_of_profiles < DS_UMTS_MAX_PDP_PROFILE_NUM)
            {
              /* check for duplicate cid entered */
              if(!(flag & (1<<cid)))
              {
                flag |= (1<<cid);
                answer_data.profile_number[answer_data.number_of_profiles]= 
                 SET_CID(cid);
                answer_data.number_of_profiles++;
              }
            }
          }
        } /* end of for loop */
        
        /* return NO_CARRIER if no handler is registered */
        if ( dial_string_handler.pdp_answer_cb == NULL ) 
        {
          DS_AT_MSG0_HIGH("Unregistered MT PDP answer cb handler");
          return DSAT_NO_CARRIER;
        }
        else
        {
          result = DSAT_NO_RSP;

          /* Slam the modem port to the current active port */
          ds3g_siolib_set_answer_port ( DS3G_SIOLIB_ANSWER_MODE_MANUAL );
          dsat_curr_abort_handler = 
            dial_string_handler.pdp_answer_cb (&answer_data);

          if ( dsat_curr_abort_handler == NULL )
          {
            DS_AT_MSG0_HIGH("unregistered +CGANS Abort handler");
          }
        }
      } /* response type = 1 */
    } /* VALID_TOKEN(0) */
    else
    {
      result = DSAT_ERROR;
    }
  }/* end of processing of write command */
  
  /*---------------------------------------------------------
    Processing for a TEST command
  -----------------------------------------------------------*/
  else if (tok_ptr->op == (NA|EQ|QU))
  {
    result = dsatparm_exec_param_cmd(
                                     mode,
                                     parse_table,
                                     tok_ptr,
                                     res_buff_ptr
                                    );
  }
  else
  {
    err_info.errval = DSAT_ERR_INVALID_SYNTAX;
    goto send_error;
  }
  return result;

send_error:
  if( err_info.errval == DSAT_ERR_INVALID_SYNTAX)
  {
    DS_AT_MSG1_ERROR("Error:%d",err_info.errval);
  }
  else
  {
    DS_AT_MSG2_ERROR("Error:%d, arg_num:%d",err_info.errval,err_info.arg_num);
  }
  return DSAT_ERROR;

} /* dsatetsipkt_exec_cgans_cmd */

#endif /* FEATURE_DATA_TE_MT_PDP */

/*===========================================================================
FUNCTION DSATETSIPKT_QCDEFPROF_RESPONSE_FMT

DESCRIPTION
  This function generates the response for the $QCDEFPROF read command.
  Data is extracted from data structure and formatted according
  to parameter flag.

DEPENDENCIES
  None

RETURN VALUE
  Returns number of characters in response buffer.

SIDE EFFECTS
  None

===========================================================================*/
static dsat_num_item_type dsatetsipkt_qcdefprof_response_fmt
(
  const dsati_cmd_type  *parse_table,   /*  Ptr to cmd in parse table   */
  dsm_item_type * const *out_buff_ptr   /*  Place to put response       */
)
{
  ds_profile_tech_etype   tech   = DS_PROFILE_TECH_3GPP;
  uint32                  family;
  ds_profile_subs_etype  subs_id ;
  ds_profile_num_type     prof_num = (ds_profile_num_type)0;
  dsm_item_type *         res_buff_ptr = *out_buff_ptr;

  for( family = DS_PROFILE_EMBEDDED_PROFILE_FAMILY; 
       family <= DS_PROFILE_TETHERED_PROFILE_FAMILY; 
       family++ )
  {
    for( subs_id =DS_PROFILE_ACTIVE_SUBSCRIPTION_1; 
         subs_id < DS_PROFILE_ACTIVE_SUBSCRIPTION_MAX; 
         subs_id++ )
    {
#ifdef FEATURE_DUAL_SIM
      if( ds_profile_get_default_profile_num_per_subs (
                     tech, family, subs_id, &prof_num ) !=
          DS_PROFILE_REG_RESULT_SUCCESS )
      {
        DS_AT_MSG0_MED("Problem reading profile num");
        continue;
      }
#else
      if( subs_id != DS_PROFILE_ACTIVE_SUBSCRIPTION_1)
      {
        continue;
      }
      if( ds_profile_get_default_profile_num_per_subs (
                     tech, family, subs_id, &prof_num ) !=
          DS_PROFILE_REG_RESULT_SUCCESS )
      {
        DS_AT_MSG0_MED("Problem reading profile num");
        continue;
      }
#endif /* FEATURE_DUAL_SIM */   
      res_buff_ptr->used += snprintf
                              (
		                              (char *)&res_buff_ptr->data_ptr[res_buff_ptr->used],
	 	                             res_buff_ptr->size,
                                "%s: %d,%d,%d\n",
                                parse_table->name,
                                (int)family,
                                subs_id,
                                (int)prof_num 
                               );
      DS_AT_MSG1_HIGH("res_buff_ptr->used = %d",res_buff_ptr->used);
    }
  }
  res_buff_ptr->data_ptr[res_buff_ptr->used-1] = '\0';
  return ((dsat_num_item_type)res_buff_ptr->used);
} /* dsatetsipkt_qcdefprof_response_fmt */

/*===========================================================================

FUNCTION DSATETSIPKT_EXEC_QCDEFPROF_CMD

DESCRIPTION
  This function takes the result from the command line parser and executes 
  it. It executes $QCDEFPROF command. This command is used to set the default 
  profile number for specific subscription and family.


DEPENDENCIES
  None
  
RETURN VALUE
  Returns an enum that describes the result of the command execution.
  Possible values:
    DSAT_ERROR : if there was any problem in executing the command
    DSAT_OK    : if it is a success.

SIDE EFFECTS
  None
  
===========================================================================*/
/* ARGSUSED */
dsat_result_enum_type dsatetsipkt_exec_qcdefprof_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type   result   = DSAT_OK;
  uint32                  family;
  ds_profile_subs_etype   subs_id  = DS_PROFILE_ACTIVE_SUBSCRIPTION_NONE;
  ds_profile_num_type     prof_num = (ds_profile_num_type)0;
  dsat_num_item_type      data_len = (dsat_num_item_type)0;
  dsat_num_item_type      temp;

  /*---------------------------------------------------------
    Processing for a command with an argument (WRITE command)
    Processing for TEST command 
  -----------------------------------------------------------*/
  if ((tok_ptr->op == (NA|EQ|AR)) ||
      (tok_ptr->op == (NA|EQ|QU)))
  {
    result = dsatparm_exec_param_cmd(
                                     mode,
                                     parse_table,
                                     tok_ptr,
                                     res_buff_ptr
                                    );
    
  }
  /*--------------------------------------
    Processing for a READ command
  ----------------------------------------*/
  else if (tok_ptr->op == (NA|QU) )
  {
    /* Report for read command */
    data_len = dsatetsipkt_qcdefprof_response_fmt(parse_table, &res_buff_ptr);
    if (0 == data_len)
    {
      DS_AT_MSG0_ERROR("Problem generating query response");
      result = DSAT_ERROR;
    }
  }
  else
  {
    /* wrong operation */
    result = DSAT_ERROR;
  }
  if ( result != DSAT_ERROR)
  {
    if (tok_ptr->op == (NA|EQ|AR))
    {
      temp     = (dsat_num_item_type)dsatutil_get_val(
                           DSAT_VENDOR_QCDEFPROF_IDX,0,0,NUM_TYPE);
      family   = (uint32)temp;
      temp     = (dsat_num_item_type)dsatutil_get_val(
                           DSAT_VENDOR_QCDEFPROF_IDX,0,1,NUM_TYPE);
      subs_id  = (ds_profile_subs_etype)temp;
      temp     = (dsat_num_item_type)dsatutil_get_val(
                           DSAT_VENDOR_QCDEFPROF_IDX,0,2,NUM_TYPE);
      prof_num = (ds_profile_num_type)temp;
#ifdef FEATURE_DUAL_SIM
      if ( ds_profile_set_default_profile_num_per_subs (
                      DS_PROFILE_TECH_3GPP, family, subs_id, prof_num ) !=
           DS_PROFILE_REG_RESULT_SUCCESS )
#else
      if ( subs_id != DS_PROFILE_ACTIVE_SUBSCRIPTION_1 ||
          (ds_profile_set_default_profile_num_per_subs (
                      DS_PROFILE_TECH_3GPP, family, subs_id, prof_num ) !=
           DS_PROFILE_REG_RESULT_SUCCESS) )
#endif /* FEATURE_DUAL_SIM */
      {
        DS_AT_MSG3_ERROR("Could not set profile parameters. Subs_id = %d family  = %d "
                  "profile number = %d", (int)subs_id, (int)family, (int)prof_num);
        result = DSAT_ERROR;
      }
    }
  }
  return result;
} /* dsatetsipkt_exec_qcdefprof_cmd */

#endif /* FEATURE_DSAT_ETSI_DATA */

#ifdef FEATURE_DSAT_LTE

/*===========================================================================
FUNCTION DSATETSIPKT_EXEC_CEMODE_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It handles the +CEMODE command.
  The set command is used to set the MT to operate according to the specified 
  mode of operation for EPS. The read command returns the mode of operation set
  by the TE, independent of the current serving cell capability and independent
  of the current serving cell Access Technology. The test command is used for 
  requesting information on the supported MT mode of operation.
  
DEPENDENCIES
  None.

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_CME_OP_NOT_SUPPORTED : in case of failure
    DSAT_OK : if the command succeeds.

SIDE EFFECTS
  None
  
===========================================================================*/
/* ARGSUSED */
dsat_result_enum_type dsatetsipkt_exec_cemode_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
)
{
  dsat_result_enum_type result = DSAT_ERROR;
  if(tok_ptr->op == (NA|QU) || (tok_ptr->op == (NA|EQ|QU))) 
  {
    result = dsatparm_exec_param_cmd(
                                   mode,
                                   parse_table,
                                   tok_ptr,
                                   res_buff_ptr
                                  );
  }
  if (DSAT_OK != result)
  {
    /* In case of error return operation not supported */
    result = dsat_send_cme_error(DSAT_CME_OP_NOT_SUPPORTED);
  }
  return result;
}/* dsatetsipkt_exec_cemode_cmd */

#endif /* FEATURE_DSAT_LTE */

