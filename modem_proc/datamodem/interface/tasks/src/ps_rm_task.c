/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                              P S _ R M _ T A S K . C

DESCRIPTION
  This is the source file for the RM Data processing Task. Contained herein
  are the functions needed to initialize all the modules that execute in RM
  task context and the main task processing loop.


EXTERNALIZED FUNCTIONS
  ps_rm_task()
    RM Task entry point and main processing loop.

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None of the RM services can be used unless MC has created the RM task
  and ps_rm_task() has been invoked.

Copyright (c) 2010-2011 Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/tasks/src/ps_rm_task.c#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when        who    what, where, why
--------    ---    ----------------------------------------------------------
10/01/11   bvd     Adding rcinit featurization 
10/07/10    am     created module.
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"       /* Customer Specific Features */


#include "queue.h"
#include "err.h"
#include "dog_hb_rex.h"
#include "ps_rm_task.h"
#include "ps_rm_taski.h"
#include "ps_rm_svc.h"
#include "ps_rm_defs.h"

#include "rcinit_rex.h"

#include "ds_Utils_DebugMsg.h"

rex_tcb_type    * rex_ps_rm_tcb;

/*===========================================================================

                           LOCAL FUNCTION DEFINITIONS FOR MODULE

===========================================================================*/

/*===========================================================================
FUNCTION PS_RM_INIT()

DESCRIPTION
  Initialization function for the rm data processing task. This function
  performs the functions needed for the data task to exit disabled state.

DEPENDENCIES
  RM task should have been started.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
void ps_rm_init
(
  void
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/


} /* ps_rm_init() */


/*===========================================================================

                     EXTERNAL FUNCTION DEFINITIONS FOR MODULE

===========================================================================*/

/*===========================================================================
FUNCTION PS_RM_TASK()

DESCRIPTION
  This function is the entry point and main processing loop for the RM data task.

DEPENDENCIES
  Does not Return.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
void ps_rm_task
(
  uint32 ignored    /* Parameter received from Main Control task - ignored */
)
{
  rex_sigs_type     loop_sigs;      /* Signals to be processed within a task
                                       loop */
  rex_sigs_type     set_sigs = 0;   /* Currently set signals               */
  uint8             loop_cnt;       /* # of times task loop is iterated
                                       on a signal mask                    */
  dog_report_type   ps_rm_dog_rpt_id = 0;	  /* Initial Safety */

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
#ifdef TEST_FRAMEWORK
  #error code not present
#endif
  
  rex_ps_rm_tcb = rex_self();

#ifdef TEST_FRAMEWORK
  #error code not present
#endif
  rcinit_handshake_startup();

  ps_rm_dog_rpt_id = dog_hb_register_rex(
                (rex_sigs_type)1 << (rex_sigs_type)PS_RM_DOG_HB_REPORT_SIGNAL);

  ps_rm_init();

  /*-------------------------------------------------------------------------
    Main task loop. Never exit!
  -------------------------------------------------------------------------*/
  for( ;;)
  {
    /*-----------------------------------------------------------------------
      Wait for work to do. No return from wait until one of the enabled
      signals is set. Even if we come out of wait, process only those
      signals which are currently enabled.
    -----------------------------------------------------------------------*/

    set_sigs |= rex_get_sigs( rex_ps_rm_tcb );

    set_sigs &= ps_rm_enabled_sig_mask;

    /*-----------------------------------------------------------------------
      Enter into the Wait only if no requested signal events have occurred
    -----------------------------------------------------------------------*/
    if( set_sigs == 0 )
    {
      set_sigs = rex_wait( ps_rm_enabled_sig_mask );
      set_sigs &= ps_rm_enabled_sig_mask;
    }

    (void)rex_clr_sigs( rex_ps_rm_tcb, set_sigs );

    loop_sigs = set_sigs & ~( (rex_sigs_type) 1
                             << (rex_sigs_type) PS_RM_SIG_MASK_CHANGE_SIGNAL );
    set_sigs = 0;

    /*-----------------------------------------------------------------------
      Now handle the active signals one by one in the order of priority.
      If the loop iterates for more than DCC_MAX_SIGNALS times, some signal
      is not handled by this loop. So instead of going in to infinite loop
      perform a check
    -----------------------------------------------------------------------*/
    loop_cnt = 0;
    while( loop_sigs && loop_cnt++ < (rex_sigs_type) PS_RM_MAX_SIGNALS )
    {
      /*---------------------------------------------------------------------
        If dog heart beat received, ack the beat. Do ping<->pong.
      ---------------------------------------------------------------------*/
      if( loop_sigs & ( (rex_sigs_type) 1 <<
                        (rex_sigs_type) PS_RM_DOG_HB_REPORT_SIGNAL ) )
      {
        loop_sigs &= ~( (rex_sigs_type) 1 <<
                        (rex_sigs_type)PS_RM_DOG_HB_REPORT_SIGNAL );
        dog_hb_report( ps_rm_dog_rpt_id );
      }
      else if( loop_sigs & ( (rex_sigs_type) 1 <<
                             (rex_sigs_type) PS_RM_RX_SPLIT_DATA_SIGNAL ) )
      {
        loop_sigs &= ~( (rex_sigs_type) 1 <<
                        (rex_sigs_type) PS_RM_RX_SPLIT_DATA_SIGNAL );

        if ( ps_rm_sig_handler[PS_RM_RX_SPLIT_DATA_SIGNAL].sig_handler
             (
               PS_RM_RX_SPLIT_DATA_SIGNAL ,
               ps_rm_sig_handler[PS_RM_RX_SPLIT_DATA_SIGNAL].user_data_ptr
             ) == FALSE )
        {
          set_sigs |= ( (rex_sigs_type) 1 <<
                        (rex_sigs_type) PS_RM_RX_SPLIT_DATA_SIGNAL);
        }
      }
      else if( loop_sigs & ( (rex_sigs_type) 1 <<
                             (rex_sigs_type) PS_RM_LLC_RX_DATA_Q_SIGNAL ) )
      {
        loop_sigs &= ~( (rex_sigs_type) 1 <<
                        (rex_sigs_type) PS_RM_LLC_RX_DATA_Q_SIGNAL );

        if ( ps_rm_sig_handler[PS_RM_LLC_RX_DATA_Q_SIGNAL].sig_handler
             (
               PS_RM_LLC_RX_DATA_Q_SIGNAL ,
               ps_rm_sig_handler[PS_RM_LLC_RX_DATA_Q_SIGNAL].user_data_ptr
             ) == FALSE )
        {
          set_sigs |= ( (rex_sigs_type) 1 <<
                        (rex_sigs_type) PS_RM_LLC_RX_DATA_Q_SIGNAL);
        }
      }
      else if( loop_sigs & ( (rex_sigs_type) 1 <<
                             (rex_sigs_type) PS_RM_RMNET_RX_DATA_Q_SIGNAL ) )
      {
        loop_sigs &= ~( (rex_sigs_type) 1 <<
                        (rex_sigs_type) PS_RM_RMNET_RX_DATA_Q_SIGNAL );

        if ( ps_rm_sig_handler[PS_RM_RMNET_RX_DATA_Q_SIGNAL].sig_handler
             (
               PS_RM_RMNET_RX_DATA_Q_SIGNAL ,
               ps_rm_sig_handler[PS_RM_RMNET_RX_DATA_Q_SIGNAL].user_data_ptr
             ) == FALSE )
        {
          set_sigs |= ( (rex_sigs_type) 1 <<
                        (rex_sigs_type) PS_RM_RMNET_RX_DATA_Q_SIGNAL);
        }
      }

      /*---------------------------------------------------------------------
        PS Task command queue signal
      ---------------------------------------------------------------------*/
      else if( loop_sigs & ( (rex_sigs_type) 1 <<
                             (rex_sigs_type) PS_RM_CMD_Q_SIGNAL ) )
      {
        loop_sigs &= ~( (rex_sigs_type) 1 <<
                        (rex_sigs_type) PS_RM_CMD_Q_SIGNAL );

        if (ps_rm_process_cmd() == FALSE )
        {
          set_sigs |= ( (rex_sigs_type) 1 <<
                        (rex_sigs_type) PS_RM_CMD_Q_SIGNAL );
        }
      }

#ifdef TEST_FRAMEWORK
      #error code not present
#endif /* TEST_FRAMEWORK */
      /*---------------------------------------------------------------------
        Check if offline command signal was set.  If set then clear signal,
        and process task offline procedures.
      ---------------------------------------------------------------------*/
      else if( loop_sigs & ( (rex_sigs_type) 1 <<
                             (rex_sigs_type) PS_RM_TASK_OFFLINE_SIGNAL ) )
      {
        LOG_MSG_INFO2_0("ps_rm_task(): "
                        "PS_RM_TASK_OFFLINE_SIGNAL received");

        loop_sigs &= ~( (rex_sigs_type) 1 <<
                        (rex_sigs_type) PS_RM_TASK_OFFLINE_SIGNAL );
      }
    }

    ASSERT(0 == loop_sigs);
  } /* forever */

} /* ps_rm_task() */
