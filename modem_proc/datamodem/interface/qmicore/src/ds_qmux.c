/*===========================================================================

                         D S _ Q M U X . C

DESCRIPTION

  The Data Services Qualcomm MSM Interface control channel message 
  multiplexing protocol source file.

EXTERNALIZED FUNCTIONS

  Upper interface APIs:

  qmux_init()
    Initialize a particular QMUX instance
  qmux_reg_service()
    Register service with a QMUX instance
  qmux_dereg_service()
    Deregister serivce with a QMUX instance
  qmux_send()
    Send a service message (PDU) over a specified QMI link
  qmux_link_down()
    Indicate that the specified QMUX link is down, results in cleanup of
    any outstanding QMUX and registered service resources.

  +----------------------------+
  |  Service(s)                |
  +----------------------------+
    |                        ^
    | qmux_send()            | service.rx_cb
    v                        |
  +----------------------------+
  | QMUX dispatch layer        |
  +----------------------------+
    |                        ^
    | sio_control_write()    | qmuxi_input()
    |                        |
    v                        |
  +----------------------------+
  |  SIO device control stream |
  +----------------------------+

  Lower interface APIs (internal functions):

  qmux_process_rx_sig()
    Called by serving task in response to QMI_RX_SIG signal

INTERNAL FUNCTIONS:

  qmuxi_sio_rx_cb()
    QMUX control rx watermark non-empty callback function - sends the
    QMUX_RX command to the serving task to process the input QMUX PDU
  qmuxi_input()
    Decapsulate the provided QMUX PDU and dispatch to the appropriate
    registered service

Copyright (c) 2004-2014 Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary
===========================================================================*/
/*===========================================================================

                      EDIT HISTORY FOR FILE

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/qmicore/src/ds_qmux.c#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when        who    what, where, why
--------    ---    ---------------------------------------------------------- 
03/19/14    vrk    Fixed a race condition in QMUX_REG_SERVICE
12/13/13    wc     Enable QMAP for TN by default
04/12/13    wc     Replace rdm_assign_port() with rdm_assign_port_tmp()
03/21/13    wc     QMAP support
12/10/12    svj    Support 8 concurrent PDN's  
10/31/12    wc     Memory optimization
09/13/12    ab     Fixed Race Condition in QMUX_REG_SERVICE 
09/13/12    svj    Changes to block STX and SIM_INTERNAL services from Apps and USB 
                   on non QMI_DEV_MUX2 ports 
09/05/12    wc     Fix DTR race condition
08/21/12    sn     Fix to open Control channel when Data channel is not used.
04/15/12    wc     Make RDM timer non-deferrable
03/06/12    wc     Remove QMI UIM/CAT from Data
11/02/11    wc     Add DTR on SIO control channel feature
10/04/11    sa     Q6 free floating changes for QMI.
09/18/11    wc     Nikel and MPSS.NI.1.1 port configuration
02/09/11    kk     Enabling DROP_PENDING on all opened SMD ports.
01/10/11    ss     Cleaning up of Globals for thread safety in Q6 Free 
                   Floating environment.
12/24/10    kk     Featurizing calls to QMI VOICE APIs.
10/13/10    kk     Added log messages before ASSERTs.
06/19/10    vs     Changes for Dual IP support over single QMI instance
11/30/09    ar     Add QMUX PDU framing supprot for streaming devices
10/23/09    ar     Add supprot for power save indication filtering and 
                   port configuration from NVRAM.
02/19/09    am     DS Task De-coupling effort and introduction of DCC task.
07/02/07    nl     Changes to map SIO_PORT_SMD_WINMOB_QMI_WWAN for 3rd party OS builds 
                   in qmi/rmnet code instead of relying on external modules' featurization. 
01/08/07    jd/ks  Added Winmobile multiple RmNet support
12/12/06    ks     Moved service initialization fn calls to external init().
09/02/06    jd     Changed DNE of RX wm to ensure complete commands received
08/23/06    ks     Changes to support multiple qmi/rmnet instances.
07/12/06    ks     Returning from qmux_process_rx_sig() if qmux is not 
                   initialized.Will temporarily mask the random sig issue.
04/05/06    jd     Dump entire QMUX PDU chain to F3 rather than single item
09/08/05    ks     Moved qmux_process_cmd() to file ds_qmi_task.c.
09/06/05    ks     Changed SIO_BITRATE_AUTO to SIO_BITRATE_BEST.
06/27/05    jd     Added QMI command handler.  Fixed bug in QMUX header ctl 
                   flags.  Process only one QMI command per signal to
                   minimize time spent in processing loop
05/31/05   jd/ks   Code review updates
05/17/05    jd     Make sure client ID callbacks are non-NULL before calling
                   Moved initialization of QMI services here from rmnet_sm
05/11/05    ks     fixed error and changed include file name.
03/14/05    ks     Removed featurization
03/08/05    ks     Clean up
11/21/04    jd     Created module
===========================================================================*/

/*===========================================================================

                          INCLUDE FILES FOR MODULE

===========================================================================*/

/*---------------------------------------------------------------------------
  Define FEATURE_DSM_WM_CB so dsm passes watermark and user data to 
  watermark callbacks
---------------------------------------------------------------------------*/
#define FEATURE_DSM_WM_CB

#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"

/* This should be the first include to allow stubs on test framework */
#if defined(T_WINNT)
#error code not present
#endif /* WINNT*/

#include "amssassert.h"
#include "msg.h"

#include "queue.h"
#include "rex.h"
#include "task.h"
#include "sio.h"
#include "rdevmap.h"
#include "dsm.h"
#include "ps_byte.h"
#include "ps_utils.h"
#include "modem_mem.h"
#include "ps_crit_sect.h"
#include "pstimer.h"
#include "qmi_modem_task_svc.h"
#include "qmi_modem_task_cmd.h"
#include "qmi_crit_sect.h"
#include "dcc_task_defs.h"
#include "dcc_task_svc.h"

#include "qmi_svc_defs.h"
#include "ds_qmux_ext.h"
#include "ds_qmux.h"
#include "ds_qmux_msg.h"
#include "ds_qmuxi.h"
#include "ds_qmi_defs.h"
#include "ds_qmi_svc.h"
#include "ds_qmi_task.h"
#include "qmi_cmd_process.h"
#include "ds_qmi_ctl.h"
#include "ds_qmi_if.h"
#include "ds_qmux_logging.h"
#include "ds_qmi_svc_ext.h"
#include "ds_qmi_nv_util.h"
#include "qmi_charger.h"
#include "qmi_frameworki.h"
#include "dsutil.h"

#ifdef FEATURE_DATA_QMI_APPS_POWER_COLLAPSE
#include "ds_qmi_pwrmgmt_shim.h"
#endif /* FEATURE_DATA_QMI_APPS_POWER_COLLAPSE */

#ifdef FEATURE_DATA_WLAN_MAPCON
#include "ds_wlan_util.h"
#endif  /* FEATURE_DATA_WLAN_MAPCON */

#include "ds_rmnet_sm_ext.h"
#include "ds_rmnet_meta_sm.h"

#ifdef FEATURE_HS_USB
  #include "hsu_config_selector.h"
#endif

#if defined(T_WINNT)
#error code not present
#endif /* WINNT*/

#include "ds_Utils_DebugMsg.h"
#include "ps_system_heap.h"

#include "smem_log.h"
#include "ds_rev_ip_transport_hdlr.h"

/*===========================================================================

                               DEFINITIONS

===========================================================================*/

/*---------------------------------------------------------------------------
  QMUX Watermark level definitions
---------------------------------------------------------------------------*/
#define QMUX_RX_WM_LOW    (0)
#define QMUX_RX_WM_HIGH   (8192)
#define QMUX_RX_WM_DNE    (102400)

/*---------------------------------------------------------------------------
  Max number of outstanding commands per service
---------------------------------------------------------------------------*/
#define QMUXI_MAX_DSM_ITEMS_PER_SVC  (10)
/*-------------------------------------------------------------------------- 
  DSM SMALL ITEM SIZE TO USE (QMUXI_MAX_DSM_ITEMS_PER_SVC * 
                  QMUX_SERVICE_MAX * 
                  DSM_DS_SMALL_ITEM_SIZ)
---------------------------------------------------------------------------*/
#define QMUXI_TX_DNE           290560

#define SMEM_QMUX_RX_EVENT     (SMEM_LOG_DEBUG_EVENT_BASE +  3)
#define SMEM_QMUX_TX_EVENT     (SMEM_LOG_DEBUG_EVENT_BASE +  4)


/*---------------------------------------------------------------------------
  Macro to determine if QMI device for USB driver is supported for
  build configuration.  On multi-processor builds, USB driver is on
  Apps image so need to register only statically bridged SMD ports. On
  single-processor builds, only the native USB ports are used.
---------------------------------------------------------------------------*/
#ifdef FEATURE_MULTIPROCESSOR    
#define IS_DEVICE_SUPPORTED( dev )                  \
        ( (QMI_DEV_USB_BEGIN > dev ) ||             \
          (QMI_DEV_USB_END   < dev) ) 
#else
#define IS_DEVICE_SUPPORTED( dev )                  \
        ( (QMI_DEV_STATIC_BRIDGE_BEGIN > dev ) ||   \
          (QMI_DEV_STATIC_BRIDGE_END   < dev)) 
#endif    

/*---------------------------------------------------------------------------
  Default MUX port configuration
---------------------------------------------------------------------------*/
#if defined(FEATURE_RMNET_PORT_CONFIG_MDM) || defined(FEATURE_RMNET_PORT_CONFIG_MSM)
  #define QMI_MUX_PORT_CFG_DEFAULT QMI_MUX_PORT_CFG_QMAP
#else
  #define QMI_MUX_PORT_CFG_DEFAULT QMI_MUX_PORT_CFG_DISABLED
#endif

/*===========================================================================

                                DATA TYPES

===========================================================================*/

/* This is the default QMI_QMUX command buffer. Make sure that it does not grow
 * beyond 512 bytes. In case you need a command buffer larger than 512 bytes,
 * declare a separate structure. */
typedef struct
{
  uint16  cmd_id; /* qmi_cmd_id_e_type */
  union
  {
    qmi_instance_e_type clean_instance;

  } data;
} qmi_qmux_cmd_buf_type;

/*===========================================================================

                               INTERNAL DATA

===========================================================================*/

/*---------------------------------------------------------------------------
  QMUX state array (one element per QMI link/instance)
---------------------------------------------------------------------------*/
static qmux_state_type  qmux_state[QMI_INSTANCE_MAX];
static uint8            qmux_num_instances = 0;
static boolean          qmux_powerup_inited = FALSE;

static qmi_mux_port_cfg_enum     qmi_mux_port_cfg = QMI_MUX_PORT_CFG_DEFAULT;
static sio_port_id_type          qmi_mux_master_sio_port = SIO_PORT_NULL;
/*---------------------------------------------------------------------------
  QMUX port list array
---------------------------------------------------------------------------*/
#define QMI_PORT_LIST_MAX_SIZE 40
typedef struct { 
  uint8                                            num_ports;
  nv_qmi_port_config_type                          port_list[QMI_PORT_LIST_MAX_SIZE];
} qmi_port_list_type;

/*---------------------------------------------------------------------------
  QMI Device to RDM device map
---------------------------------------------------------------------------*/
typedef struct qmux_rdm_dev_map_s
{
  qmux_device_id_type   qmi_dev;
  rdm_device_enum_type  rdm_dev;
  sio_port_id_type      sio_port;
} qmux_rdm_dev_map_type;

static qmux_rdm_dev_map_type qmux_rdm_dev_map_tbl[] =
{
  { QMI_DEV_UART1,   RDM_UART1_DEV,          SIO_PORT_UART_MAIN     },
  { QMI_DEV_UART2,   RDM_UART2_DEV,          SIO_PORT_UART_AUX      },
  { QMI_DEV_UART3,   RDM_UART3_DEV,          SIO_PORT_UART_THIRD    },
  { QMI_DEV_USB1,    RDM_USB_NET_WWAN_DEV,   SIO_PORT_USB_NET_WWAN  },
  { QMI_DEV_USB2,    RDM_USB_NET_WWAN2_DEV,  SIO_PORT_USB_RMNET2    },
  { QMI_DEV_USB3,    RDM_USB_NET_WWAN3_DEV,  SIO_PORT_USB_RMNET3    },
  { QMI_DEV_USB4,    RDM_USB_NET_WWAN4_DEV,  SIO_PORT_USB_RMNET4    },
  { QMI_DEV_USB5,    RDM_USB_NET_WWAN5_DEV,  SIO_PORT_USB_RMNET5    },
  { QMI_DEV_MUX1,    RDM_MUX_DEV_1,          SIO_PORT_DATA_MUX_1    },
  { QMI_DEV_MUX2,    RDM_MUX_DEV_2,          SIO_PORT_DATA_MUX_2    },
  { QMI_DEV_MUX3,    RDM_MUX_DEV_3,          SIO_PORT_DATA_MUX_3    },
  { QMI_DEV_MUX4,    RDM_MUX_DEV_4,          SIO_PORT_DATA_MUX_4    },
  { QMI_DEV_MUX5,    RDM_MUX_DEV_5,          SIO_PORT_DATA_MUX_5    },
  { QMI_DEV_MUX6,    RDM_MUX_DEV_6,          SIO_PORT_DATA_MUX_6    },
  { QMI_DEV_MUX7,    RDM_MUX_DEV_7,          SIO_PORT_DATA_MUX_7    },
  { QMI_DEV_MUX8,    RDM_MUX_DEV_8,          SIO_PORT_DATA_MUX_8    },
  { QMI_DEV_MUX9,    RDM_MUX_DEV_9,          SIO_PORT_DATA_MUX_9    },
  { QMI_DEV_MUX10,   RDM_MUX_DEV_10,         SIO_PORT_DATA_MUX_10   },
  { QMI_DEV_MUX11,   RDM_MUX_DEV_11,         SIO_PORT_DATA_MUX_11   },
  { QMI_DEV_MUX12,   RDM_MUX_DEV_12,         SIO_PORT_DATA_MUX_12   },
  { QMI_DEV_MUX13,   RDM_MUX_DEV_13,         SIO_PORT_DATA_MUX_13   },
  { QMI_DEV_MUX14,   RDM_MUX_DEV_14,         SIO_PORT_DATA_MUX_14   },
  { QMI_DEV_MUX15,   RDM_MUX_DEV_15,         SIO_PORT_DATA_MUX_15   },
  { QMI_DEV_MUX16,   RDM_MUX_DEV_16,         SIO_PORT_DATA_MUX_16   },
  { QMI_DEV_SMD1_EMBEDDED,    RDM_SMD_DATA5_DEV,      SIO_PORT_SMD_DATA5     },
  { QMI_DEV_SMD2_EMBEDDED,    RDM_SMD_DATA6_DEV,      SIO_PORT_SMD_DATA6     },
  { QMI_DEV_SMD3_EMBEDDED,    RDM_SMD_DATA7_DEV,      SIO_PORT_SMD_DATA7     },
  { QMI_DEV_SMD4_EMBEDDED,    RDM_SMD_DATA8_DEV,      SIO_PORT_SMD_DATA8     },
  { QMI_DEV_SMD5_EMBEDDED,    RDM_SMD_DATA9_DEV,      SIO_PORT_SMD_DATA9     },
  { QMI_DEV_SMD6_EMBEDDED,    RDM_SMD_DATA12_DEV,     SIO_PORT_SMD_DATA12    },
  { QMI_DEV_SMD7_EMBEDDED,    RDM_SMD_DATA13_DEV,     SIO_PORT_SMD_DATA13    },
  { QMI_DEV_SMD8_EMBEDDED,    RDM_SMD_DATA14_DEV,     SIO_PORT_SMD_DATA14    },
  { QMI_DEV_SMD1_TETHERED,    RDM_SMD_DATA40_DEV,     SIO_PORT_SMD_DATA40    },
  { QMI_DEV_SMD2_TETHERED,    RDM_SMD_DATA39_DEV,     SIO_PORT_SMD_DATA39    },
  { QMI_DEV_SMD3_TETHERED,    RDM_SMD_DATA38_DEV,     SIO_PORT_SMD_DATA38    },
  { QMI_DEV_SMD4_TETHERED,    RDM_SMD_DATA37_DEV,     SIO_PORT_SMD_DATA37    },
  { QMI_DEV_SMD5_TETHERED,    RDM_SMD_DATA36_DEV,     SIO_PORT_SMD_DATA36    },
  { QMI_DEV_SMD6_TETHERED,    RDM_SMD_DATA35_DEV,     SIO_PORT_SMD_DATA35    },
  { QMI_DEV_SMD7_TETHERED,    RDM_SMD_DATA34_DEV,     SIO_PORT_SMD_DATA34    },
  { QMI_DEV_SMD8_TETHERED,    RDM_SMD_DATA33_DEV,     SIO_PORT_SMD_DATA33    },
  { QMI_DEV_SMD9_TETHERED,    RDM_SMD_DATA32_DEV,     SIO_PORT_SMD_DATA32    },
  { QMI_DEV_A2P1,    RDM_A2_RMNET_1_DEV,     SIO_PORT_A2_RMNET_1    },
  { QMI_DEV_A2P2,    RDM_A2_RMNET_2_DEV,     SIO_PORT_A2_RMNET_2    },
  { QMI_DEV_A2P3,    RDM_A2_RMNET_3_DEV,     SIO_PORT_A2_RMNET_3    },

  { QMI_DEV_SDIO_MUX_A2_0_EMBEDDED,  RDM_SDIO_MUX_A2_RMNET_0_DEV,  SIO_PORT_SDIO_MUX_A2_RMNET_0 },
  { QMI_DEV_SDIO_MUX_A2_1_EMBEDDED,  RDM_SDIO_MUX_A2_RMNET_1_DEV,  SIO_PORT_SDIO_MUX_A2_RMNET_1 },
  { QMI_DEV_SDIO_MUX_A2_2_EMBEDDED,  RDM_SDIO_MUX_A2_RMNET_2_DEV,  SIO_PORT_SDIO_MUX_A2_RMNET_2 },
  { QMI_DEV_SDIO_MUX_A2_3_EMBEDDED,  RDM_SDIO_MUX_A2_RMNET_3_DEV,  SIO_PORT_SDIO_MUX_A2_RMNET_3 },
  { QMI_DEV_SDIO_MUX_A2_4_EMBEDDED,  RDM_SDIO_MUX_A2_RMNET_4_DEV,  SIO_PORT_SDIO_MUX_A2_RMNET_4 },
  { QMI_DEV_SDIO_MUX_A2_5_EMBEDDED,  RDM_SDIO_MUX_A2_RMNET_5_DEV,  SIO_PORT_SDIO_MUX_A2_RMNET_5 },
  { QMI_DEV_SDIO_MUX_A2_6_EMBEDDED,  RDM_SDIO_MUX_A2_RMNET_6_DEV,  SIO_PORT_SDIO_MUX_A2_RMNET_6 },
  { QMI_DEV_SDIO_MUX_A2_7_EMBEDDED,  RDM_SDIO_MUX_A2_RMNET_7_DEV,  SIO_PORT_SDIO_MUX_A2_RMNET_7 },
  { QMI_DEV_SDIO_MUX_A2_8_EMBEDDED,  RDM_SDIO_MUX_A2_RMNET_8_DEV,  SIO_PORT_SDIO_MUX_A2_RMNET_8 },
  { QMI_DEV_SDIO_MUX_A2_9_EMBEDDED,  RDM_SDIO_MUX_A2_RMNET_9_DEV,  SIO_PORT_SDIO_MUX_A2_RMNET_9 },
#if defined(FEATURE_RMNET_PORT_CONFIG_MDM_LE)
  { QMI_DEV_SDIO_MUX_A2_10_EMBEDDED,  RDM_SDIO_MUX_A2_RMNET_10_DEV,  SIO_PORT_SDIO_MUX_A2_RMNET_10 },
#endif /* FEATURE_RMNET_PORT_CONFIG_MDM_LE */
  { QMI_DEV_SDIO_MUX_A2_0_TETHERED,  RDM_SDIO_MUX_A2_RMNET_TETH_0_DEV,  SIO_PORT_SDIO_MUX_A2_RMNET_TETH_0 },

  { QMI_DEV_MUX_A2_CH_0,      RDM_MUX_A2_CH_0_DEV,      SIO_PORT_MUX_A2_CH_0 },
  { QMI_DEV_MUX_A2_CH_1,      RDM_MUX_A2_CH_1_DEV,      SIO_PORT_MUX_A2_CH_1 },
  { QMI_DEV_MUX_A2_CH_2,      RDM_MUX_A2_CH_2_DEV,      SIO_PORT_MUX_A2_CH_2 },
  { QMI_DEV_MUX_A2_CH_3,      RDM_MUX_A2_CH_3_DEV,      SIO_PORT_MUX_A2_CH_3 },
  { QMI_DEV_MUX_A2_CH_4,      RDM_MUX_A2_CH_4_DEV,      SIO_PORT_MUX_A2_CH_4 },
  { QMI_DEV_MUX_A2_CH_5,      RDM_MUX_A2_CH_5_DEV,      SIO_PORT_MUX_A2_CH_5 },
  { QMI_DEV_MUX_A2_CH_6,      RDM_MUX_A2_CH_6_DEV,      SIO_PORT_MUX_A2_CH_6 },
  { QMI_DEV_MUX_A2_CH_7,      RDM_MUX_A2_CH_7_DEV,      SIO_PORT_MUX_A2_CH_7 },

#ifdef FEATURE_DATA_WLAN_MAPCON
  { QMI_DEV_SDIO_MUX_A2_REV_IP_0,    RDM_SDIO_MUX_A2_REV_IP_0_DEV, SIO_PORT_SDIO_MUX_A2_REV_IP_0 },
  { QMI_DEV_SDIO_MUX_A2_REV_IP_1,    RDM_SDIO_MUX_A2_REV_IP_1_DEV, SIO_PORT_SDIO_MUX_A2_REV_IP_1 },
  { QMI_DEV_SDIO_MUX_A2_REV_IP_2,    RDM_SDIO_MUX_A2_REV_IP_2_DEV, SIO_PORT_SDIO_MUX_A2_REV_IP_2 },
  { QMI_DEV_SDIO_MUX_A2_REV_IP_3,    RDM_SDIO_MUX_A2_REV_IP_3_DEV, SIO_PORT_SDIO_MUX_A2_REV_IP_3 },
  { QMI_DEV_SDIO_MUX_A2_REV_IP_4,    RDM_SDIO_MUX_A2_REV_IP_4_DEV, SIO_PORT_SDIO_MUX_A2_REV_IP_4 },
  { QMI_DEV_SDIO_MUX_A2_REV_IP_5,    RDM_SDIO_MUX_A2_REV_IP_5_DEV, SIO_PORT_SDIO_MUX_A2_REV_IP_5 },
  { QMI_DEV_SDIO_MUX_A2_REV_IP_6,    RDM_SDIO_MUX_A2_REV_IP_6_DEV, SIO_PORT_SDIO_MUX_A2_REV_IP_6 },
  { QMI_DEV_SDIO_MUX_A2_REV_IP_7,    RDM_SDIO_MUX_A2_REV_IP_7_DEV, SIO_PORT_SDIO_MUX_A2_REV_IP_7 },
  { QMI_DEV_SDIO_MUX_A2_REV_IP_8,    RDM_SDIO_MUX_A2_REV_IP_8_DEV, SIO_PORT_SDIO_MUX_A2_REV_IP_8 }, 
  { QMI_DEV_MUX_A2_REV_CH_0,  RDM_MUX_A2_REV_CH_0_DEV,  SIO_PORT_MUX_A2_REV_CH_0 },
  { QMI_DEV_MUX_A2_REV_CH_1,  RDM_MUX_A2_REV_CH_1_DEV,  SIO_PORT_MUX_A2_REV_CH_1 },
  { QMI_DEV_MUX_A2_REV_CH_2,  RDM_MUX_A2_REV_CH_2_DEV,  SIO_PORT_MUX_A2_REV_CH_2 },
  { QMI_DEV_MUX_A2_REV_CH_3,  RDM_MUX_A2_REV_CH_3_DEV,  SIO_PORT_MUX_A2_REV_CH_3 },
  { QMI_DEV_MUX_A2_REV_CH_4,  RDM_MUX_A2_REV_CH_4_DEV,  SIO_PORT_MUX_A2_REV_CH_4 },
  { QMI_DEV_MUX_A2_REV_CH_5,  RDM_MUX_A2_REV_CH_5_DEV,  SIO_PORT_MUX_A2_REV_CH_5 },
  { QMI_DEV_MUX_A2_REV_CH_6,  RDM_MUX_A2_REV_CH_6_DEV,  SIO_PORT_MUX_A2_REV_CH_6 },
  { QMI_DEV_MUX_A2_REV_CH_7,  RDM_MUX_A2_REV_CH_7_DEV,  SIO_PORT_MUX_A2_REV_CH_7 },
  { QMI_DEV_MUX_A2_REV_CH_8,  RDM_MUX_A2_REV_CH_8_DEV,  SIO_PORT_MUX_A2_REV_CH_8 }
#endif /* FEATURE_DATA_WLAN_MAPCON */

  /* Consider featurizing the SMUX port mapping before uncommenting */
  /*{ QMI_DEV_SMUX0,   RDM_SMUX_0_DEV,         SIO_PORT_SMUX_0        },
  { QMI_DEV_SMUX1,   RDM_SMUX_1_DEV,         SIO_PORT_SMUX_1        },
  { QMI_DEV_SMUX2,   RDM_SMUX_2_DEV,         SIO_PORT_SMUX_2        },
  { QMI_DEV_SMUX3,   RDM_SMUX_3_DEV,         SIO_PORT_SMUX_3        },
  { QMI_DEV_SMUX4,   RDM_SMUX_4_DEV,         SIO_PORT_SMUX_4        },
  { QMI_DEV_SMUX5,   RDM_SMUX_5_DEV,         SIO_PORT_SMUX_5        },
  { QMI_DEV_SMUX6,   RDM_SMUX_6_DEV,         SIO_PORT_SMUX_6        },
  { QMI_DEV_SMUX7,   RDM_SMUX_7_DEV,         SIO_PORT_SMUX_7        }*/
};

#define QMUX_RDM_DEV_MAP_TBL_SIZE \
        (sizeof(qmux_rdm_dev_map_tbl) / sizeof(qmux_rdm_dev_map_tbl[0]))

#define IS_USB_DRIVER(port)                      \
        ( (SIO_PORT_USB_NET_WWAN == port) ||     \
          (SIO_PORT_USB_RMNET2   == port) ||     \
          (SIO_PORT_USB_RMNET3   == port) )

/*---------------------------------------------------------------------------
  For logical ports, only the first channel (SIO_PORT_MUX_A2_CH_0)'s
  SMD open param is used to open physical SMD, so only need to put CH_0 here.
---------------------------------------------------------------------------*/
#define IS_TETHERED_PORT(port)                   \
        ( (SIO_PORT_SMD_DATA40 == port) ||       \
          (SIO_PORT_SMD_DATA39 == port) ||       \
          (SIO_PORT_SMD_DATA38 == port) ||       \
          (SIO_PORT_SMD_DATA37 == port) ||       \
          (SIO_PORT_SMD_DATA36 == port) ||       \
          (SIO_PORT_SMD_DATA35 == port) ||       \
          (SIO_PORT_SMD_DATA34 == port) ||       \
          (SIO_PORT_SMD_DATA33 == port) ||       \
          (SIO_PORT_SMD_DATA32 == port) ||       \
          (SIO_PORT_SDIO_MUX_A2_RMNET_TETH_0 == port) ||       \
          (SIO_PORT_MUX_A2_CH_0 == port) ||  \
          (SIO_PORT_A2_RMNET_1 == port) ||       \
          (SIO_PORT_A2_RMNET_2 == port) ||       \
          (SIO_PORT_A2_RMNET_3 == port) )

#ifdef FEATURE_DATA_WLAN_MAPCON
#define IS_REVERSE_PORT(port)                   \
        ( (SIO_PORT_SDIO_MUX_A2_REV_IP_0 == port) ||       \
          (SIO_PORT_SDIO_MUX_A2_REV_IP_1 == port) ||       \
          (SIO_PORT_SDIO_MUX_A2_REV_IP_2 == port) ||       \
          (SIO_PORT_SDIO_MUX_A2_REV_IP_3 == port) ||       \
          (SIO_PORT_SDIO_MUX_A2_REV_IP_4 == port) ||       \
          (SIO_PORT_SDIO_MUX_A2_REV_IP_5 == port) ||       \
          (SIO_PORT_SDIO_MUX_A2_REV_IP_6 == port) ||       \
          (SIO_PORT_SDIO_MUX_A2_REV_IP_7 == port) ||       \
          (SIO_PORT_SDIO_MUX_A2_REV_IP_8 == port) ||       \
          (SIO_PORT_MUX_A2_REV_CH_0 == port)  ||           \
          (SIO_PORT_MUX_A2_REV_CH_1 == port)  ||           \
          (SIO_PORT_MUX_A2_REV_CH_2 == port)  ||           \
          (SIO_PORT_MUX_A2_REV_CH_3 == port)  ||           \
          (SIO_PORT_MUX_A2_REV_CH_4 == port)  ||           \
          (SIO_PORT_MUX_A2_REV_CH_5 == port)  ||           \
          (SIO_PORT_MUX_A2_REV_CH_6 == port)  ||           \
          (SIO_PORT_MUX_A2_REV_CH_7 == port)  ||           \
          (SIO_PORT_MUX_A2_REV_CH_8 == port) )
#endif /* FEATURE_DATA_WLAN_MAPCON */

static struct {
  qmi_instance_e_type       instance;
  timer_type                timer;                      /* for RDM open delay  */
  timer_group_type          timer_group;                /* No-deferrable group */
  uint8                     retries;                    /* count attempts      */
} qmux_rdm_open_info;

#define QMUX_RDM_OPEN_DELAY         (200)          /* millisec units      */
#define QMUX_MAX_RDM_OPEN_RETRIES   (150)

/*---------------------------------------------------------------------------
  CRC table to be used for FCS calculation 
  Inserted from 3GPP TS27.010 V5
---------------------------------------------------------------------------*/
#define QMUX_CRC_TABLE_ELEMENTS          256
#define QMUX_FCS_INIT_VAL                (0xFFu)

static uint8  qmux_crc_table[QMUX_CRC_TABLE_ELEMENTS] = 
{   /* reversed, 8-bit, poly=0x07 */
    0x00, 0x91, 0xE3, 0x72, 0x07, 0x96, 0xE4, 0x75,  
    0x0E, 0x9F, 0xED, 0x7C, 0x09, 0x98, 0xEA, 0x7B,
    0x1C, 0x8D, 0xFF, 0x6E, 0x1B, 0x8A, 0xF8, 0x69,  
    0x12, 0x83, 0xF1, 0x60, 0x15, 0x84, 0xF6, 0x67,
    0x38, 0xA9, 0xDB, 0x4A, 0x3F, 0xAE, 0xDC, 0x4D,  
    0x36, 0xA7, 0xD5, 0x44, 0x31, 0xA0, 0xD2, 0x43,
    0x24, 0xB5, 0xC7, 0x56, 0x23, 0xB2, 0xC0, 0x51,  
    0x2A, 0xBB, 0xC9, 0x58, 0x2D, 0xBC, 0xCE, 0x5F,

    0x70, 0xE1, 0x93, 0x02, 0x77, 0xE6, 0x94, 0x05,  
    0x7E, 0xEF, 0x9D, 0x0C, 0x79, 0xE8, 0x9A, 0x0B,
    0x6C, 0xFD, 0x8F, 0x1E, 0x6B, 0xFA, 0x88, 0x19,  
    0x62, 0xF3, 0x81, 0x10, 0x65, 0xF4, 0x86, 0x17,
    0x48, 0xD9, 0xAB, 0x3A, 0x4F, 0xDE, 0xAC, 0x3D,  
    0x46, 0xD7, 0xA5, 0x34, 0x41, 0xD0, 0xA2, 0x33,
    0x54, 0xC5, 0xB7, 0x26, 0x53, 0xC2, 0xB0, 0x21,  
    0x5A, 0xCB, 0xB9, 0x28, 0x5D, 0xCC, 0xBE, 0x2F,

    0xE0, 0x71, 0x03, 0x92, 0xE7, 0x76, 0x04, 0x95,  
    0xEE, 0x7F, 0x0D, 0x9C, 0xE9, 0x78, 0x0A, 0x9B,
    0xFC, 0x6D, 0x1F, 0x8E, 0xFB, 0x6A, 0x18, 0x89,  
    0xF2, 0x63, 0x11, 0x80, 0xF5, 0x64, 0x16, 0x87,
    0xD8, 0x49, 0x3B, 0xAA, 0xDF, 0x4E, 0x3C, 0xAD,  
    0xD6, 0x47, 0x35, 0xA4, 0xD1, 0x40, 0x32, 0xA3,
    0xC4, 0x55, 0x27, 0xB6, 0xC3, 0x52, 0x20, 0xB1,  
    0xCA, 0x5B, 0x29, 0xB8, 0xCD, 0x5C, 0x2E, 0xBF,

    0x90, 0x01, 0x73, 0xE2, 0x97, 0x06, 0x74, 0xE5,  
    0x9E, 0x0F, 0x7D, 0xEC, 0x99, 0x08, 0x7A, 0xEB,
    0x8C, 0x1D, 0x6F, 0xFE, 0x8B, 0x1A, 0x68, 0xF9,  
    0x82, 0x13, 0x61, 0xF0, 0x85, 0x14, 0x66, 0xF7,
    0xA8, 0x39, 0x4B, 0xDA, 0xAF, 0x3E, 0x4C, 0xDD,  
    0xA6, 0x37, 0x45, 0xD4, 0xA1, 0x30, 0x42, 0xD3,
    0xB4, 0x25, 0x57, 0xC6, 0xB3, 0x22, 0x50, 0xC1,  
    0xBA, 0x2B, 0x59, 0xC8, 0xBD, 0x2C, 0x5E, 0xCF
};

/*----------------------------------------------------------------------
 * SIO to mux_id mapping table
-----------------------------------------------------------------------*/
typedef struct mux_id_map_s
{
  sio_port_id_type  sio_port;
  uint8             default_mux_id;
  uint8             qmap_mux_id;
} mux_id_map_type;

static mux_id_map_type mux_id_map[] =
{
#if defined(FEATURE_RMNET_PORT_CONFIG_MDM) || defined(FEATURE_RMNET_PORT_CONFIG_MDM_LE)
  {SIO_PORT_MUX_A2_CH_0, 1, 1},
  {SIO_PORT_MUX_A2_CH_1, 2, 2},
  {SIO_PORT_MUX_A2_CH_2, 3, 3},
  {SIO_PORT_MUX_A2_CH_3, 4, 4},
  {SIO_PORT_MUX_A2_CH_4, 5, 5},
  {SIO_PORT_MUX_A2_CH_5, 6, 6},
  {SIO_PORT_MUX_A2_CH_6, 7, 7},
  {SIO_PORT_MUX_A2_CH_7, 8, 8},
  {SIO_PORT_MUX_A2_REV_CH_0, 9, 9},
  {SIO_PORT_MUX_A2_REV_CH_1, 10, 10},
  {SIO_PORT_MUX_A2_REV_CH_2, 11, 11},
  {SIO_PORT_MUX_A2_REV_CH_3, 12, 12},
  {SIO_PORT_MUX_A2_REV_CH_4, 13, 13},
  {SIO_PORT_MUX_A2_REV_CH_5, 14, 14},
  {SIO_PORT_MUX_A2_REV_CH_6, 15, 15},
  {SIO_PORT_MUX_A2_REV_CH_7, 16, 16},
  {SIO_PORT_MUX_A2_REV_CH_8, 17, 17}
#elif defined(FEATURE_RMNET_PORT_CONFIG_MSM)
  {SIO_PORT_SDIO_MUX_A2_RMNET_1, 0, 1},
  {SIO_PORT_SDIO_MUX_A2_RMNET_2, 0, 2},
  {SIO_PORT_SDIO_MUX_A2_RMNET_3, 0, 3},
  {SIO_PORT_SDIO_MUX_A2_RMNET_4, 0, 4},
  {SIO_PORT_SDIO_MUX_A2_RMNET_5, 0, 5},
  {SIO_PORT_SDIO_MUX_A2_RMNET_6, 0, 6},
  {SIO_PORT_SDIO_MUX_A2_RMNET_7, 0, 7},
  {SIO_PORT_SDIO_MUX_A2_RMNET_8, 0, 8},
  {SIO_PORT_SDIO_MUX_A2_REV_IP_0, 0, 9},
  {SIO_PORT_SDIO_MUX_A2_REV_IP_1, 0, 10},
  {SIO_PORT_SDIO_MUX_A2_REV_IP_2, 0, 11},
  {SIO_PORT_SDIO_MUX_A2_REV_IP_3, 0, 12},
  {SIO_PORT_SDIO_MUX_A2_REV_IP_4, 0, 13},
  {SIO_PORT_SDIO_MUX_A2_REV_IP_5, 0, 14},
  {SIO_PORT_SDIO_MUX_A2_REV_IP_6, 0, 15},
  {SIO_PORT_SDIO_MUX_A2_REV_IP_7, 0, 16},
  {SIO_PORT_SDIO_MUX_A2_REV_IP_8, 0, 17},
#endif
};

#if defined(FEATURE_8960_SGLTE_FUSION)
#define QMI_SIM_INTERNAL  35
#define QMI_STX           38
#endif

/*===========================================================================

                           FORWARD DECLARATIONS

===========================================================================*/
static boolean qmuxi_init
(
  qmi_instance_e_type  qmi_instance
);

static void  qmuxi_sio_rx_cb
(
  struct dsm_watermark_type_s *  wm_ptr,
  void *                         user_data_ptr
);

static boolean  qmuxi_input
(
  qmux_state_type *  qmux_s,
  dsm_item_type **   qmux_pdu
);

static void qmuxi_process_msg
( 
  qmux_state_type *  qmux_s,
  dsm_item_type *    qmux_pdu
);

static boolean qmuxi_close_instance
(
  qmi_instance_e_type  qmi_instance
);

static boolean qmuxi_open_instance
(
  qmi_instance_e_type  qmi_instance,
  uint16               sio_port
);

static boolean qmuxi_process_rx_sig
( 
  qmi_sig_enum_type sig,                   /* Signal to be processed       */
  void *user_data_ptr                      /* Parameter carrying user data */
);

static boolean qmuxi_find_mapped_qmi_device
(
  uint16                  qmi_dev,
  qmux_rdm_dev_map_type **tbl_pptr
);

static boolean qmuxi_find_mapped_sio_port
(
  uint16                  sio_port,
  qmux_rdm_dev_map_type **tbl_pptr
);

static void qmuxi_sio_ioctl
(
  qmux_state_type       * qmux_s,
  sio_ioctl_cmd_type      cmd,
  sio_ioctl_param_type  * ioctl_param
);

#ifdef FEATURE_DATA_QMI_DTR_ON_CTRL_CHANNEL
static void qmuxi_set_dtr_handler
(
  qmi_instance_e_type    inst,
  sio_vpu_func_ptr_type  dtr_cb
);

static void qmuxi_dtr_cb
(
  void    * user_data
);
#endif /* FEATURE_DATA_QMI_DTR_ON_CTRL_CHANNEL */

static qmi_instance_e_type qmi_instance_by_qmux_state
(
  qmux_state_type * qmux_s
);

static void *qmi_qmux_get_cmd_buf
(
  qmi_cmd_id_e_type cmd_id
);

#define qmi_qmux_free_cmd_buf(buf_ptr) PS_SYSTEM_HEAP_MEM_FREE(buf_ptr)

static void qmi_qmux_process_cmd
(
  void * cmd_ptr
);
/*===========================================================================

                        EXTERNAL FUNCTION DEFINITIONS

===========================================================================*/
/*===========================================================================
  FUNCTION QMUX_POWERUP_INIT()

  DESCRIPTION
    QMUX power up initialization.

  PARAMETERS
    None

  RETURN VALUE
    TRUE on successful initialization
    FALSE otherwise

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean qmux_powerup_init
(
  void
)
{
  /*-------------------------------------------------------------------------
    Initialize the QMI state structure.
  -------------------------------------------------------------------------*/
  memset( qmux_state, 0, sizeof(qmux_state) );
  qmux_powerup_inited = TRUE;
 
  return TRUE;
} /* qmux_powerup_init */

/*===========================================================================
  FUNCTION QMUX_INIT()

  DESCRIPTION
    Initialize specified QMUX instance.  SIO control IO path will be set up,
    and QMUX instance state will be reset to default.

  PARAMETERS
    None

  RETURN VALUE
    TRUE on successful initialization
    FALSE otherwise

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean qmux_init
(
  void
)
{
  uint8              i;
  uint8              n_qmis = 0;
  qmi_cmd_init_legacy_services_type * dcc_cmd_ptr = NULL;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------------
    Initialize the QMI state structure.
  -------------------------------------------------------------------------*/
  memset( qmux_state, 0x0, sizeof(qmux_state) );

  /*-------------------------------------------------------------------------
    Initialize the QMI EFS NV conf file.
  -------------------------------------------------------------------------*/
  if (qmi_nv_init_efs_conf_file() != QMI_NV_STATUS_OK)
  {
    LOG_MSG_ERROR_0("Failed to initialize QMI EFS NV conf file");
  }

  /*-------------------------------------------------------------------------
    Load the SIO port configuraiton table from NVRAM, and register with
    Runtime Device Mapper (RDM) to open/close operations.
  -------------------------------------------------------------------------*/
  if( !qmuxi_get_port_configuration_from_nv( &n_qmis ) )
  {
    LOG_MSG_ERROR_0("Couldn't read SIO port configuration!");
  }
  else
  {
    if( n_qmis > 0 )
    {
      if( !qmuxi_register_devices_with_rdm() )
      {
        LOG_MSG_ERROR_0("Couldn't registerd device with RDM !");
      }
    }
    else
    {
      LOG_MSG_ERROR_0("No QMI instances preconfigured in NVRAM!");
    }
  }

  /*-------------------------------------------------------------------------
    Initialize the control path
  -------------------------------------------------------------------------*/
  for(i=0; i < n_qmis; i++)
  {
    if(qmuxi_init((qmi_instance_e_type)i) == FALSE)
    {
      return FALSE;
    }
    qmi_ctl_init((qmi_instance_e_type)i);    /* initialize QMI Control service           */
  }

  /* initialize QMI service on DCC task    */
  (void) dcc_set_cmd_handler( DCC_QMI_INIT_LEGACY_SERVICES_CMD, 
                              qmi_process_init_legacy_services);
  dcc_cmd_ptr = (qmi_cmd_init_legacy_services_type *) modem_mem_alloc(
                                         sizeof(qmi_cmd_init_legacy_services_type),
                                         MODEM_MEM_CLIENT_DATACOMMON );
  if(NULL == dcc_cmd_ptr)
  {
    LOG_MSG_ERROR_0( "QMI can't get modem memory heap allocation" );
    ASSERT(0);
    return FALSE;
  }
  memset(dcc_cmd_ptr, 0, sizeof(qmi_cmd_init_legacy_services_type));
  dcc_cmd_ptr->num_qmi_instance = n_qmis;
  dcc_send_cmd_ex(DCC_QMI_INIT_LEGACY_SERVICES_CMD, (void *)dcc_cmd_ptr);

  /*-----------------------------------------------------------------------------
    Set cmd hander with DCC task for RmNet(port) instance open and close handling
    RmNet instance open/close should happen after legacy QMI services get initialized
  ------------------------------------------------------------------------------*/
  (void) dcc_set_cmd_handler( DCC_RMNET_INST_OPEN_CLOSE_CMD, 
                              rmnet_sm_process_inst_open_close_cmd_hdlr);

#ifdef FEATURE_DATA_WLAN_MAPCON
  /*-----------------------------------------------------------------------------
    Set cmd hander with DCC task for Reverse IP transport module
  ------------------------------------------------------------------------------*/
  (void) dcc_set_cmd_handler( DCC_REV_IP_TRANSPORT_PERFORM_OPERATION_CMD, 
                              rev_ip_transport_process_cmd);
#endif /* FEATURE_DATA_WLAN_MAPCON*/

#ifdef FEATURE_DATA_QMI_APPS_POWER_COLLAPSE
  if (!qmi_pwrmgmt_shim_init())
  {
    LOG_MSG_ERROR_0("Couldn't initialize QMI_PWRMGMT_SHIM module!");
  }
#endif /* FEATURE_DATA_QMI_APPS_POWER_COLLAPSE */
 

  /*-------------------------------------------------------------------------
    Set signal handler for QMUX_RX signal and enable the signal.
    One qmux sig for all instances.
  -------------------------------------------------------------------------*/
  (void)qmi_set_sig_handler(QMI_QMUX_RX_SIGNAL,
                            qmuxi_process_rx_sig,
                            NULL);
  qmi_enable_sig(QMI_QMUX_RX_SIGNAL);

  /*-------------------------------------------------------------------------
    Set cmd handlers with the QMI MODEM task.
  -------------------------------------------------------------------------*/ 
  (void) qmi_set_cmd_handler(QMI_CMD, qmi_process_cmd);

  (void) qmi_task_set_qmux_cmd_handler(qmi_qmux_process_cmd);

  return TRUE;
}

/*===========================================================================
  FUNCTION QMUX_REG_VS_SERVICE_LIST()

  DESCRIPTION
    Register service table with QMUX

  PARAMETERS
    svc_list             : array pointer to all services for qmi instance
    num_services         : total number of services in list that
                           are to be registered
    qmi_instance         : instance of QMI

  RETURN VALUE
    TRUE if table registeration was successful
    FALSE if table format/content was invalid

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean qmux_reg_vs_service_list
(
  qmi_instance_e_type  qmi_instance,
  qmux_svc_info_type * svc_list,
  uint8                num_services
)
{
  LOG_MSG_INFO1_0("qmux_reg_vs_service_list is deprecated");
  return TRUE;
}/* qmux_reg_service_list() */


/*===========================================================================
  FUNCTION QMUX_REG_VS_SERVICE()

  DESCRIPTION
    Register the vendor specific service instance identified by 'service' 
    with the qmux instance identified by 'qmi_instance' with the 
    configuration provided in 'cfg'.     
    Function is used to register VS Services ONLY
   
  PARAMETERS
    qmi_instance : instance of QMI
    service      : service type
    cfg          : configuration parameters for the service instance

  RETURN VALUE
    NULL           -  service registration failed
    service handle -  qmux service handle 

  DEPENDENCIES
    qmux_reg_service_list() must be called to register service list before
    qmux is able to use it.

  SIDE EFFECTS
    None
===========================================================================*/
void * qmux_reg_vs_service
(
  qmi_instance_e_type     qmi_instance,
  qmux_service_e_type     service,
  qmux_svc_config_type *  cfg
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/  

  return qmux_reg_service(qmi_instance, service, cfg);

} /* qmux_reg_vs_service() */



/*===========================================================================
  FUNCTION QMUX_REG_SERVICE()

  DESCRIPTION
    Register the service instance identified by 'service' with the qmux
    instance identified by 'qmi_instance' with the configuration provided
    in 'cfg'.
    
  PARAMETERS
    qmi_instance : instance of QMI
    service      : service type
    cfg          : configuration parameters for the service instance

  RETURN VALUE
    NULL           -  service registration failed
    service handle -  qmux service handle 

  DEPENDENCIES
    qmux_init() must have been called on 'qmi_instance' previously

  SIDE EFFECTS
    None
===========================================================================*/
void * qmux_reg_service
(
  qmi_instance_e_type     qmi_instance,
  qmux_service_e_type     service,
  qmux_svc_config_type *  cfg
)
{
  qmux_svc_info_type *  svc_s;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (service >= QMUX_SERVICE_MAX)
  {
    LOG_MSG_ERROR_1("Service ID %d out of range!", service);
    return NULL;
  }

  /* check that qmi_instance is within range in case ASSERT is removed */
  if (qmi_instance >= QMI_INSTANCE_MAX)
  {
    LOG_MSG_ERROR_1 ("QMI instance out of range (%d)!", qmi_instance);
    return NULL;
  }
  if( qmux_powerup_inited != TRUE )
  {
    ERR ("QMUX Powerup initialization has not happened!", 0, 0, 0);
    return NULL;
  }

  /* check that cfg pointer is not NULL in case ASSERT is removed */
  if (cfg == NULL)
  {
    LOG_MSG_ERROR_0("QMI service cfg is NULL!");
    return NULL;
  }

  if( !qmux_state[qmi_instance].initialized )
  {
    LOG_MSG_ERROR_1 ("QMI link %d not initialized!", qmi_instance);
    return NULL;
  }

  svc_s = qmux_state[qmi_instance].service[service];

  /* Alloc service info memory */
  if ( svc_s == NULL)
  {
    svc_s =  (qmux_svc_info_type *)qmi_svc_ps_system_heap_mem_alloc(
                                     sizeof(qmux_svc_info_type),
                                     FILE_ID_DS_QMUX,__LINE__);
    if (svc_s == NULL)
    {
      return NULL;
    }

    memset(svc_s, 0, sizeof(qmux_svc_info_type));
    svc_s->service_id = service;  /* QMUX service type */
    svc_s->cfg        = *cfg;     /* store the provided config */
    qmux_state[qmi_instance].service[service] = svc_s;
  }


  if( svc_s->registered == TRUE)
  {
    LOG_MSG_ERROR_2 ("Service %d already registered with QMUX#%d!",
                     service, qmi_instance);
    return NULL;
  }

  /*-------------------------------------------------------------------------
    validate passed config
    - service must have a non-null receive function.
    - Everything else is optional
    - if dealloc callback is not specified (using the service library),
      must specify a reset clid callback
  -------------------------------------------------------------------------*/
  if ( ((cfg->cbs.recv == NULL) && 
        ((cfg->cmd_hdlr_array == NULL) || (cfg->cmd_num_entries == 0))))/* ||
       ((cfg->cbs.recv != NULL) && 
        ((cfg->cmd_hdlr_array != NULL) || (cfg->cmd_num_entries != 0))) )*/
  {
    LOG_MSG_ERROR_0 ("Invalid QMI service config");
    return NULL;
  }

  /*-------------------------------------------------------------------------
    if the service is not QCTL, then when it's using the common dealloc_clid,
    reset client must be registered, otherwise it would cause an error
  -------------------------------------------------------------------------*/
  if (service != QMUX_SERVICE_CTL)
  {
    if ((cfg->cbs.dealloc_clid == NULL) && (cfg->cbs.reset_client == NULL))
    {
      LOG_MSG_ERROR_0 ("Invalid QMI service config");
      return NULL;
    }
  }
  

  /*-------------------------------------------------------------------------
    state pointer will be passed back in callbacks to tie this to the 
    associated service instance
  -------------------------------------------------------------------------*/
  svc_s->registered = TRUE;     /* indication that service is registered */
  
#ifdef FEATURE_DATA_QMI_SVC_AVAILABILITY
  /*Notify Control Point about service availability*/
  qmux_notify_service_registration
               (svc_s->service_id,
                qmi_instance,
                TRUE); // Send TRUE here as this is an old QMUX based Service
#endif /*FEATURE_DATA_QMI_SVC_AVAILABILITY*/

  /*-------------------------------------------------------------------------
    Return service handle which service should use to deregister, etc.
  -------------------------------------------------------------------------*/
  return svc_s;

} /* qmux_reg_service() */

/*===========================================================================
  FUNCTION QMUX_DEREG_SERVICE()

  DESCRIPTION
    Deregister the previously registered service instance identified by 
    the passed service handle.

  PARAMETERS
    service handle -  qmux service handle returned by qmux_reg_service()

  RETURN VALUE
    None

  DEPENDENCIES
    qmux_init() must have been called on 'qmi_instance' previously

  SIDE EFFECTS
    None
===========================================================================*/
void qmux_dereg_service
(
  void *  service_handle
)
{
  qmi_instance_e_type   qmi_instance;
  qmux_svc_info_type *  svc_s;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT (service_handle);

  /* check that service_handle is not NULL in case ASSERT is removed */
  if (service_handle == NULL)
  {
    LOG_MSG_ERROR_0 ("Deregistering NULL service handle!");
    return;
  }

  qmi_instance = qmi_instance_by_qmux_handle( service_handle );
  if (qmi_instance >= QMI_INSTANCE_MAX)
  {
    LOG_MSG_ERROR_1 ("Deregistering invalid service handle (0x%x)!",
                     service_handle);
    return;
  }

  svc_s = (qmux_svc_info_type *) service_handle;
  ASSERT (svc_s->registered);
  LOG_MSG_INFO2_2 ("Deregistering service %d from QMUX#%d",
                   svc_s->service_id, qmi_instance);

  /*-------------------------------------------------------------------------
    Clear service info block
  -------------------------------------------------------------------------*/
  memset (svc_s, 0, sizeof(qmux_svc_info_type));

} /* qmux_dereg_service() */

#ifdef FEATURE_DATA_QMI_SVC_AVAILABILITY
/*===========================================================================
FUNCTION QMUX_NOTIFY_SERVICE_REGISTRATION()

DESCRIPTION
  Function to learn about qmi service registration with qmux. This function 
  posts a command to qmi_modem task to notify TE about the new 
  service availability. 

PARAMETERS 
  service_id   : QMI Service ID
  qmi_instance : QMI Instance (ignored for new QMUX based service)
  is_legacy    : The value is true if legacy QMI service,
                 else FALSE for new QMUX based Service
 
RETURN VALUE
  None.

DEPENDENCIES
  None.

SIDE EFFECTS
  None.
===========================================================================*/
void qmux_notify_service_registration
(
  qmux_service_e_type                   service_id,
  qmi_instance_e_type                   qmi_instance,
  boolean                               is_legacy_svc
)
{
  qmi_cmd_data_buf_type         *  cmd_ptr;
/*-------------------------------------------------------------------------*/

  if (service_id >= QMUX_SERVICE_MAX)
  {
    LOG_MSG_ERROR_1("Unknown service [%d]", service_id);
    ASSERT(0);
    return;
  }
  cmd_ptr = qmi_get_cmd_data_buf();
  if (cmd_ptr == NULL)
  {
    LOG_MSG_ERROR_1 ("No cmd buffer! service %d response message ",
                     service_id);
    ASSERT(0);
    return;
  }
  /*-------------------------------------------------------------------------
    Sending svc ID, qmi instance and is_legacy_svc response 
  -------------------------------------------------------------------------*/

  LOG_MSG_INFO1_1("Posting cmd [%d] to QMI Modem task.",
                  QMI_CMD_CTL_SVC_AVAILABLE);

  cmd_ptr->cmd.qmi.id = QMI_CMD_CTL_SVC_AVAILABLE;
  cmd_ptr->cmd.qmi.data.ctl_avail_ind.is_legacy_svc  = is_legacy_svc;
  cmd_ptr->cmd.qmi.data.ctl_avail_ind.service_id     = service_id;
  cmd_ptr->cmd.qmi.data.ctl_avail_ind.qmi_inst       = qmi_instance;
  qmi_send_cmd(QMI_CMD, cmd_ptr);

} /* qmux_notify_service_registration() */
#endif /*FEATURE_DATA_QMI_SVC_AVAILABILITY*/

/*===========================================================================
  FUNCTION QMUX_CALCULATE_FCS()

  DESCRIPTION
    Calculate frame checksum (FCS) over the passed SDU packet.
    Refer to 3GPP TS27.010 V5 Annex B for details of 8-bit FCS algorithm.

  PARAMETERS
    qmux_sdu       : service PDU to be sent
    fcs_ptr        : pointer to FCS result    
    
  RETURN VALUE
    TRUE on successful calcaulation
    FALSE otherwise

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static boolean qmux_calculate_fcs
(
  dsm_item_type * qmux_sdu,
  uint8         * fcs_ptr
)
{
  uint32  fcs_num_bytes = 0;
  uint16  peek_offset = 0;
  uint8   fcs = QMUX_FCS_INIT_VAL;
  uint8   octet_buf = 0;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT( fcs_ptr );

  LOG_MSG_INFO3_0("qmux_calculate_fcs");
  
  fcs_num_bytes = dsm_length_packet( qmux_sdu );

  /* Loop over SDU size */
  while( fcs_num_bytes--)
  {
    /* Note: peek_offset is being incremented */
    if( FALSE == dsm_peek_byte( qmux_sdu, peek_offset++, &octet_buf ) )
    {
      LOG_MSG_INFO1_0("Error getting SDU octet");
      return FALSE;
    }

    fcs = qmux_crc_table[fcs ^ octet_buf];
  }

  /* Apply one's complement */
  fcs = QMUX_FCS_INIT_VAL - fcs;

  /* Return FCS value to caller */
  *fcs_ptr = fcs;
  return TRUE;
}


/*===========================================================================
  FUNCTION QMUX_FRAME_SDU()

  DESCRIPTION
    Apply framing to the QMUX SDU based on the framing mode/IFtype.
    There are two framing modes supported:
    
    (1) No framing - The packet is framed by the transport driver so
                     no PDU framing is required.

    (2) Frame Mode - The PDU is framed using start and stop flags, and
                     an checksum is computed.  The octects are
                     arranged as shown below.  No octet transparency
                     (escaping) is done in this format.

                     
       |Start|I/F  | QMUX PDU                     |     |Stop |
       |Flag |Type | Length   Info                | FCS |Flag |
       +-----+-----+--------+---------------------+-----+-----+
       |  1  |  1  |    2   | specified in Length |  1  |  1  |octets
       +-----+-----+--------+---------------------+-----+-----+
                     

  PARAMETERS
    qmux_sdu_pptr  : pointer to service PDU
    
  RETURN VALUE
    TRUE on successful framing operaiton
    FALSE otherwise

  DEPENDENCIES
    qmux_init() must have been called on 'qmi_instance' previously

  SIDE EFFECTS
    None
===========================================================================*/
static boolean qmux_frame_sdu
(
  dsm_item_type **        qmux_sdu_pptr
)
{
  uint8   sdu_fcs = 0;
  uint8   flag = 0;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT( qmux_sdu_pptr );

  LOG_MSG_INFO3_0("qmux_frame_sdu");
  
  /*-------------------------------------------------------------------------
    Calculate FCS over SDU.  This includs the IFtype field and QMUX PDU.
  -------------------------------------------------------------------------*/
  if( !qmux_calculate_fcs( *qmux_sdu_pptr, &sdu_fcs ) )
  {
    MSG_FATAL ("Error calculating FCS value",0,0,0);
    ASSERT(0);
    return FALSE;
  }
  
  /* Append FCS to SDU */
  if( sizeof(sdu_fcs) != dsm_pushdown_tail( qmux_sdu_pptr, 
                                            &sdu_fcs, 
                                            sizeof(sdu_fcs), 
                                            DSM_DS_SMALL_ITEM_POOL ) )
  {
    LOG_MSG_INFO1_0 ("Out of memory on SDU frame FCS");
    return FALSE;
  }

  /*-------------------------------------------------------------------------
    Add start and stop flags to SDU
  -------------------------------------------------------------------------*/
  flag = QMUX_FRAME_FLAG_START;
  if(!qmi_svc_dsm_pushdown_packed( qmux_sdu_pptr, 
                                           &flag, 
                                           sizeof(flag), 
                                   DSM_DS_SMALL_ITEM_POOL,
                                   FILE_ID_DS_QMUX,__LINE__) )
  {
    return FALSE;
  }
  
  flag = QMUX_FRAME_FLAG_STOP;
  if( sizeof(flag) != dsm_pushdown_tail( qmux_sdu_pptr, 
                                         &flag, 
                                         sizeof(flag), 
                                         DSM_DS_SMALL_ITEM_POOL ) )
  {
    LOG_MSG_INFO1_0 ("Out of memory on SDU frame append stop flag");
    return FALSE;
  }
  
  return TRUE;
} /* qmux_frame_sdu() */


/*===========================================================================
  FUNCTION QMUX_UNFRAME_SDU()

  DESCRIPTION
    Undo framing on the QMUX SDU based on the framing mode/iftype.
    See QMUX_FRAME_SDU() for discussion of frame structure.

    During unframing the START and STOP flags are tested for, the
    latter located using the length field within the QMUX PDU. If the
    QMUX PDU length exceeds the unframed SDU size, it is assumed the
    input is a partial SDU. Given there is no octet transparency done
    in framing, it may be the frame STOP flag appears within the frame
    payload.  This causes the SIO driver to deliver a partial SDU.
    When detetected, the partial SDU is cached for prepend on
    subsequent SDU delivery, and no SDU is passed back to the caller.

    Finally the FCS calcaulated for the unframed SDU is compared to
    the frame FCS field.  If values do not match, the SDU is discarded
    and no SDU is passed back to the caller.
    

  PARAMETERS
    qmux_sdu_pptr     : pointer to service PDU
    pending_sdu_pptr  : pointer to cached patrial PDU
    
  RETURN VALUE
    TRUE on successful unframing operaiton
    FALSE otherwise

  DEPENDENCIES
    qmux_init() must have been called on 'qmi_instance' previously

  SIDE EFFECTS
    None
===========================================================================*/
static boolean qmux_unframe_sdu
(
  dsm_item_type **  qmux_sdu_pptr,	
  dsm_item_type **  pending_sdu_pptr
)
{
  uint8   sdu_fcs = 0;
  uint8   expected_fcs = 0;
  uint8   flag = 0;
  uint32  sdu_len = 0;   /* framed SDU length   */
  uint16  pdu_len = 0;   /* unframed PDU length */
  uint16  seek_len = sizeof(pdu_len);
  uint16  offset = QMUX_FRAME_PDU_LEN_OFFSET;
  dsm_item_type * sdu_ptr = NULL;
  dsm_item_type * pdu_ptr = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT( qmux_sdu_pptr );
  ASSERT( *qmux_sdu_pptr );
  ASSERT( pending_sdu_pptr );

  LOG_MSG_INFO3_0("qmux_unframe_sdu");
  
  /*-------------------------------------------------------------------------
    If exists, prepend partial SDU from previous delivery. 
  -------------------------------------------------------------------------*/
  if( *pending_sdu_pptr )
  {
    LOG_MSG_INFO2_0("Prepending previous SDU to current");

    sdu_ptr = *pending_sdu_pptr;
    *pending_sdu_pptr = NULL;

    dsm_append( &sdu_ptr, qmux_sdu_pptr);
  }
  else
  {
    sdu_ptr = *qmux_sdu_pptr;
    *qmux_sdu_pptr = NULL; 
  }

  ASSERT( sdu_ptr );
  
  /*-------------------------------------------------------------------------
    Verify START and STOP flags are at head and tail of SDU.
  -------------------------------------------------------------------------*/
  if( !dsm_peek_byte(sdu_ptr, QMUX_FRAME_PDU_START_OFFSET, &flag) )
  {
    LOG_MSG_INFO2_0("Error extracting QMUX PDU start flag");
    dsm_free_packet( &sdu_ptr );
    return FALSE;
  }

  if( QMUX_FRAME_FLAG_START != flag )
  {
    LOG_MSG_INFO2_1("QMUX PDU start flag mismatch: 0x%X", flag);
    dsm_free_packet( &sdu_ptr );
    return FALSE;
  }

  sdu_len = dsm_length_packet( sdu_ptr );

  /*-------------------------------------------------------------------------
    Suppressing STOP flag validation for now.  Driver may deliver DSM
    packet based on stale data timeout, in which case there will be no
    valid STOP flag.  We will handle this case as a partial PDU
    delivery, caching the DSM packet for the next delivery cycle.
    This approach relies on the PDU length valiation to catch PDU
    corruption.
  -------------------------------------------------------------------------*/
#if 0
  if( !dsm_peek_byte (sdu_ptr,
		      (uint16)(sdu_len-QMUX_FRAME_PDU_STOP_OFFSET),
		      &flag ) )
  {
    LOG_MSG_INFO2_0("Error extracting QMUX PDU start flag");
    dsm_free_packet( &sdu_ptr );
    return FALSE;
  }
  
  if( QMUX_FRAME_FLAG_STOP != flag )
  {
    LOG_MSG_INFO2_1("QMUX PDU stop flag mismatch: 0x%X", flag);
    dsm_free_packet( &sdu_ptr );
    return FALSE;
  }
#endif /* 0 */
  
  /*-------------------------------------------------------------------------
    Determine length of QMUX PDU and compare to unframed SDU size.  If
    expected length exceeds SDU size, assume this is a partial SDU.
    Cache DSM packet in state info for next SDU delivery.
    Discard SDU if expected length less than SDU size.
  -------------------------------------------------------------------------*/
  if( !dsm_seek_extract( &sdu_ptr, &offset, &pdu_len, &seek_len) )
  {
    LOG_MSG_INFO2_0("Error extracting QMUX PDU length");
    dsm_free_packet( &sdu_ptr );
    return FALSE;
  }
    
  if( (sdu_len-QMUX_FRAME_PDU_LEN_OVERHEAD) != pdu_len )
  {
    /* Check for too small SDU; discard case */
    if( (uint32)(sdu_len-QMUX_FRAME_PDU_LEN_OVERHEAD) > pdu_len )
    {
      LOG_MSG_INFO2_2("SDU length greater than expected size: %d > %d",
                      	 (sdu_len-QMUX_FRAME_PDU_LEN_OVERHEAD), pdu_len);
      dsm_free_packet( &sdu_ptr );
      return FALSE;
    }

    /* Assume this is a partial SDU with STOP flag embedded */
    LOG_MSG_INFO2_0("Caching current SDU for next segmet");
    *pending_sdu_pptr = sdu_ptr;
    return TRUE;
  }
  
  /*-------------------------------------------------------------------------
    Extract just QMUX PDU and IFtype fields into new packet.  This will be 
    used for FCS calculation.
  -------------------------------------------------------------------------*/
  if( (pdu_len+1) !=
      dsm_dup_packet_pool( &pdu_ptr, DSM_DS_SMALL_ITEM_POOL,
		           sdu_ptr, QMUX_FRAME_PDU_IF_OFFSET, (pdu_len+1) ) )
  {
    LOG_MSG_INFO2_0("Error creating QMUX PDU");
    dsm_free_packet( &pdu_ptr );
    dsm_free_packet( &sdu_ptr );
    return FALSE;
  }


  /*-------------------------------------------------------------------------
    Calculate FCS over PDU and compare to FCS in SDU.  This includes
    the IFtype field and QMUX PDU.  If value does not match FCS from
    SDU field, discard SDU.
  -------------------------------------------------------------------------*/
  if( !dsm_peek_byte (sdu_ptr,
		      (uint16)(sdu_len-QMUX_FRAME_PDU_FCS_OFFSET),
		      &expected_fcs ) )
  {
    LOG_MSG_INFO2_0("Error extracting QMUX PDU FCS");
    dsm_free_packet( &pdu_ptr );
    dsm_free_packet( &sdu_ptr );
    return FALSE;
  }
    
  if( !qmux_calculate_fcs( pdu_ptr, &sdu_fcs ) )
  {
    LOG_MSG_INFO2_0("Error calculating FCS on PDU");
    dsm_free_packet( &pdu_ptr );
    dsm_free_packet( &sdu_ptr );
    return FALSE;
  }

  if( expected_fcs !=  sdu_fcs )
  {
    LOG_MSG_INFO2_2("FCS mismatch on SDU: 0x%X != 0x%X",
                    expected_fcs, sdu_fcs);
    dsm_free_packet( &pdu_ptr );
    dsm_free_packet( &sdu_ptr );
    return FALSE;
  }

  /*-------------------------------------------------------------------------
    Pass validated QMUX PDU to caller.
  -------------------------------------------------------------------------*/
  *qmux_sdu_pptr = pdu_ptr; 
  dsm_free_packet( &sdu_ptr );
  
  return TRUE;
} /* qmux_unframe_sdu() */



/*===========================================================================
  FUNCTION QMUX_SIO_SEND()

  DESCRIPTION
    Package and send the provided service layer PDU as a QMUX message on the
    link inferred by service.

  PARAMETERS
    qmi_instance   : qmi instance
    service        : service id
    qmux_sdu       : service PDU to be sent
    clid           : Client ID for which the message is destined
    
  RETURN VALUE
    None

  DEPENDENCIES
    qmux_init() must have been called on 'qmi_instance' previously

  SIDE EFFECTS
    None
===========================================================================*/
void qmux_sio_send
(
  qmi_instance_e_type   qmi_instance,
  qmux_service_e_type   service, 
  uint32                clid,
  dsm_item_type *       qmux_sdu
)
{
  
  qmux_hdr_type         qmux_hdr;
  byte                  iftype;
  uint8                 msg_ctl;
  uint8                 tx_id_hb;
  uint8                 tx_id_lb;
  uint16                tx_id;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/


  ASSERT (qmi_instance < QMI_INSTANCE_MAX);

  /* check that qmi_instance is within range in case ASSERT is removed */
  if (qmi_instance >= QMI_INSTANCE_MAX)
  {
    LOG_MSG_ERROR_1("QMI instance out of range (%d)!  Discarding",
                    qmi_instance);
    dsm_free_packet (&qmux_sdu);
    return;
  }

  if( dsm_length_packet(qmux_sdu) == 0)
  {
    LOG_MSG_INFO1_2 ("0-length QMUX SDU (%d type, clid %d)!  Discarding",
                     service, clid);
    dsm_free_packet (&qmux_sdu);
    return;
  }

  if (qmux_state[qmi_instance].io.sio_handle == SIO_NO_STREAM_ID)
  {
    LOG_MSG_INFO1_0( "SIO control stream not open!  freeing tx pkt");
    dsm_free_packet (&qmux_sdu);
    return;
  }

  if ( (FALSE == qmux_state[qmi_instance].io.ports_active) ||
       (QMI_DEV_NONE == qmux_state[qmi_instance].io.port_info.qmi_port) )
  {
    LOG_MSG_INFO1_0( "Ports not active!  freeing tx pkt");
    dsm_free_packet (&qmux_sdu);
    return;
  }

  /*-------------------------------------------------------------------------
    Build and insert QMUX header.  Length is total of qmi header + payload.
  -------------------------------------------------------------------------*/
  qmux_hdr.len       = (uint16) (dsm_length_packet(qmux_sdu) + sizeof(qmux_hdr_type));
  qmux_hdr.ctl_flags = (QMUXI_CFLAG_SENDER_SERVICE) & ~QMUXI_CFLAG_MASK_RESERVED;
  qmux_hdr.svc_type  = service;
  qmux_hdr.clid      = (uint8) clid;

  /*-------------------------------------------------------------------------
    Log the QMUX hdr info, msg_ctl, QMI instance and tx_id in SMEM
  -------------------------------------------------------------------------*/
  dsm_peek_byte(qmux_sdu, 0, &msg_ctl );
  dsm_peek_byte(qmux_sdu, 1, &tx_id_lb );
  dsm_peek_byte(qmux_sdu, 2, &tx_id_hb );
  tx_id = ((((uint16)(tx_id_hb) & 0x00FF) << 8) | (uint16)tx_id_lb);
  SMEM_LOG_EVENT6(SMEM_QMUX_TX_EVENT, 
                  qmux_hdr.len, qmux_hdr.ctl_flags,
                  qmux_hdr.svc_type, qmux_hdr.clid,
                  ((((uint32)(qmi_instance) & 0x0000FFFF) << 16) | (uint32)msg_ctl),
                  tx_id);

  if (!qmi_svc_dsm_pushdown_packed( &qmux_sdu, 
                                (void *) &qmux_hdr, 
                                sizeof(qmux_hdr),
                                DSM_DS_SMALL_ITEM_POOL,
                                FILE_ID_DS_QMUX,__LINE__ ))
  {
    dsm_free_packet (&qmux_sdu);
    return;
  }

  /*------------------------------------------------------------------------- 
    Add iftype (not part of qmux header)
  -------------------------------------------------------------------------*/
  /* The port configuration framing mode value is synonymous with iftype. */
  iftype = (byte)qmux_state[ qmi_instance ].io.port_info.frame_mode;

  if(!qmi_svc_dsm_pushdown_packed( &qmux_sdu, 
                                   &iftype, 
                                   sizeof(byte), 
                                   DSM_DS_SMALL_ITEM_POOL,
                                   FILE_ID_DS_QMUX,__LINE__) )
  {
    dsm_free_packet( &qmux_sdu );
    return;
  }

  /*-------------------------------------------------------------------------
    Log the outgoing QMI message before passing to SIO control.
  -------------------------------------------------------------------------*/
  qmi_log_packet(qmux_sdu, qmi_instance, QMUX_TX);

  /*-------------------------------------------------------------------------
    Check for requirement to perform framing on PDU.  This will be
    preconfigured for each QMI port.
  -------------------------------------------------------------------------*/
  if( IS_FRAMING_REQUIRED( iftype ) )
  {
    if( !qmux_frame_sdu( &qmux_sdu ) )
    {
      LOG_MSG_INFO1_2 ("Failed on PDU framing(%d type, clid %d) Discarding",
                       service, clid);
      dsm_free_packet (&qmux_sdu);
      return;
    }
  }

  #if 0
    LOG_MSG_INFO2_0("Sending QMUX");
    pdu_p = qmux_sdu;
    while (pdu_p)
    {
      memdump( pdu_p->data_ptr, pdu_p->used );
      pdu_p = pdu_p->pkt_ptr;
    }
  #endif

  /*-------------------------------------------------------------------------
    Send the encapsulated response
  -------------------------------------------------------------------------*/
  if( QMUX_DEVSTREAM_CONTROL != qmux_state[qmi_instance].io.port_info.qmi_stream )
  {
    sio_transmit( qmux_state[qmi_instance].io.sio_handle, qmux_sdu );
  }
  else
  {
    sio_control_transmit( qmux_state[qmi_instance].io.sio_handle, qmux_sdu );
  }
} /* qmux_sio_send */

/*===========================================================================
  FUNCTION QMUX_SEND()

  DESCRIPTION
    Package and send the provided service layer PDU as a QMUX message on the
    link inferred by service_handle

  PARAMETERS
    service_handle : handle to the service's qmux link
    qmux_sdu       : service PDU to be sent
    clid           : Client ID for which the message is destined
    
  RETURN VALUE
    None

  DEPENDENCIES
    qmux_init() must have been called on 'qmi_instance' previously

  SIDE EFFECTS
    None
===========================================================================*/
void qmux_send 
(
void *           service_handle,
dsm_item_type *  qmux_sdu,
byte             clid
)
{
   qmux_svc_info_type *  svc_s;
   qmi_instance_e_type   qmi_instance;
   
   ASSERT(service_handle);
   ASSERT(qmux_sdu);

  /* check that service_handle is not NULL in case ASSERT is removed */
  if (service_handle == NULL)
  {
    LOG_MSG_ERROR_0 ("QMUX send with NULL service handle!");
    dsm_free_packet (&qmux_sdu);
    return;
  }

  /* check that qmux_sdu is not NULL in case ASSERT is removed */
  if (qmux_sdu == NULL)
  {
    LOG_MSG_ERROR_0("QMUX SDU NULL!");
    return;
  }

   svc_s = (qmux_svc_info_type *) service_handle;
   qmi_instance = qmi_instance_by_qmux_handle (svc_s);
   qmux_sio_send(qmi_instance,svc_s->service_id, clid, qmux_sdu); 
   return;
} /* qmux_send() */


/*===========================================================================
  FUNCTION QMUX_LINK_DOWN

  DESCRIPTION
    Callback to cleanup QMUX and associated service resources when their 
    serving QMI link is disconnected

  PARAMETERS
    instance - the QMI instance whose link was disconnected

  DEPENDENCIES
    qmux_init() must have been called on 'qmi_instance' previously

  RETURN VALUE
    None

  SIDE EFFECTS
    QMI link disconnection closes all QMI application state
===========================================================================*/
void qmux_link_down
(
  qmi_instance_e_type  instance
)
{
  qmux_state_type *     qmux_s;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(instance < QMI_INSTANCE_MAX);

  /* check that instance is within range in case ASSERT is removed */
  if (instance >= QMI_INSTANCE_MAX)
  {
    LOG_MSG_ERROR_1("QMI instance out of range (%d)!  Discarding", instance);
    return;
  }

  LOG_MSG_INFO1_1 ("QMI instance %d link down - reset all services", instance);
  qmux_s = &qmux_state[instance];

  qmux_clean_instance_resources(instance);

  /*-------------------------------------------------------------------------
    Free any QMI messages still in RX watermark.
  -------------------------------------------------------------------------*/
  dsm_empty_queue( &qmux_s->io.rx_wm );

} /* qmux_link_down() */

/*===========================================================================
   FUNCTION QMUX_CLEAN_LEGACY_SVC_RESOURCES

  DESCRIPTION
     Cleanup QMUX and associated service resources related to Legacy Services

  PARAMETERS
    instance - the QMI instance whose link was disconnected

  DEPENDENCIES
    qmux_init() must have been called on 'qmi_instance' previously

  RETURN VALUE
    None

  SIDE EFFECTS
    Closes all QMI Legacy Service application state
===========================================================================*/
void  qmux_clean_legacy_svc_resources
(
  qmi_instance_e_type  instance
)
{
  qmux_state_type *     qmux_s;
  qmux_svc_info_type *  svci;
  int i;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /* check that instance is within range */
  if (instance >= QMI_INSTANCE_MAX)
  {
    LOG_MSG_ERROR_1("QMI instance out of range (%d)!  Discarding", instance);
    return;
  }

  LOG_MSG_INFO1_1 ("QMI Legacy Service cleanup on instance %d", instance);
  qmux_s = &qmux_state[instance];

  /*-------------------------------------------------------------------------
    Cleanup each service's state
  -------------------------------------------------------------------------*/
  for (i = 0; i < QMUX_SERVICE_MAX; i++)
  {
    svci = qmux_s->service[i];
    if (svci == NULL)
    {
      continue;
    }

    if (svci->registered == TRUE)
    {
      if( svci->cfg.cbs.qmux_closed )
      {
        svci->cfg.cbs.qmux_closed( svci->cfg.sp );
      }
      else
      {
        qmi_svc_qmi_link_closed(svci->cfg.sp);
      }
    }
  }
}/*qmux_clean_legacy_svc_resources*/

/*===========================================================================
  FUNCTION QMUX_CLEAN_INSTANCE_RESOURCES

  DESCRIPTION
    Cleanup QMUX and associated service resources

  PARAMETERS
    instance - the QMI instance whose link was disconnected

  DEPENDENCIES
    qmux_init() must have been called on 'qmi_instance' previously

  RETURN VALUE
    None

  SIDE EFFECTS
    Closes all QMI application state
===========================================================================*/
void qmux_clean_instance_resources
(
  qmi_instance_e_type  instance
)
{
  qmi_qmux_cmd_buf_type * cmd_ptr = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /* check that instance is within range */
  if (instance >= QMI_INSTANCE_MAX)
  {
    LOG_MSG_ERROR_1("QMI instance out of range (%d)!  Discarding", instance);
    return;
  }

  LOG_MSG_INFO1_1 ("QMI clean instance %d resources", instance);

  /*-------------------------------------------------------------------------
    Posting to DCC for QMI Legacy service Cleanup when link closed.
  -------------------------------------------------------------------------*/
  cmd_ptr = (qmi_qmux_cmd_buf_type *)qmi_qmux_get_cmd_buf(QMI_CMD_LEGACY_SVC_QMI_LINK_CLOSED);
  if(cmd_ptr == NULL)
  {
    LOG_MSG_ERROR_0("Out of QMUX cmd buf");
    return;
  }
  cmd_ptr->cmd_id = QMI_CMD_LEGACY_SVC_QMI_LINK_CLOSED;
  cmd_ptr->data.clean_instance = instance;

  dcc_send_cmd_ex(DCC_QMI_CMD, cmd_ptr);
  
  /*-------------------------------------------------------------------------
    QMI FW service Cleanup when link closed.
  -------------------------------------------------------------------------*/
  qmi_framework_svc_qmi_link_closed(instance);

} /* qmux_clean_instance_resources */


#if 0 //Not used at present

/*===========================================================================
  FUNCTION ARRAY_IX_BY_PTR()

  DESCRIPTION
    To obtain an array index from a pointer to any field within the array

  PARAMETERS
    ptr          - pointer to resolve ix for
    array_base   - base address of array
    element_size - array element size
    n_elements   - number of elements in the array
    
  RETURN VALUE
    index of array element containing ptr
    -1 if handle is invalid

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
int array_ix_by_ptr
(
  uint8 *  ptr,
  uint8 *  array_base,
  int      element_size,
  int      n_elements
)
{
  uint32  retval;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------------
    validate this does out of range ok
  -------------------------------------------------------------------------*/
  retval = ((ptr - array_base) / element_size);
  if (retval >= n_elements)
  {
    return -1;
  }

  return retval;
} /* array_ix_by_ptr() */
#endif //#if 0, Not used present


/*===========================================================================
  FUNCTION QMUX_GET_SERVICE_CFG()

  DESCRIPTION
    Get Service configuration

  PARAMETERS
    qmux_handle : handle assigned to each service by qmux
    svc_type    : type of qmux service    

  RETURN VALUE
    qmux_svc_config_type
    NULL if input service handle or service type is invalid

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
qmux_svc_config_type *  qmux_get_service_cfg
(
  void *               qmux_handle,
  qmux_service_e_type  svc_type
)
{
  qmi_instance_e_type  qmi_instance;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT (qmux_handle);

  /* check that qmux_handle is not NULL in case ASSERT is removed */
  if (qmux_handle == NULL)
  {
    LOG_MSG_ERROR_0("QMUX handle NULL!");
    return NULL;
  }

 /*---------------------------------------------------------------------
    Return NULL if:
     - svc_type is not Internal QC service range or not in VS range
  ---------------------------------------------------------------------*/
  if (svc_type >= QMUX_SERVICE_MAX)
  {
    return NULL;
  }

  qmi_instance = qmi_instance_by_qmux_handle(qmux_handle);
  if (qmi_instance >= QMI_INSTANCE_MAX)
  {
    return NULL;
  }

  /*---------------------------------------------------------------------
     First determine if svc_type is referring to QC internal services or 
     external services and then return appropriate cfg.
  ---------------------------------------------------------------------*/
  if ( (qmux_state[qmi_instance].service[svc_type] != NULL) &&
       (qmux_state[qmi_instance].service[svc_type]->registered == TRUE) )
  {
    return &qmux_state[qmi_instance].service[svc_type]->cfg;
  }

  return NULL;

} /* qmux_get_service_cfg() */



/*===========================================================================
  FUNCTION QMUX_PROCESS_RX_SIG

  DESCRIPTION
    Called when QMUX RX signal is set - will check each QMI link's rx 
    watermark and process all outstanding commands.

    Runs in QMI Task.

  PARAMETERS  
    None

  DEPENDENCIES
    qmux_init() must have been called on 'qmi_instance' previously

  RETURN VALUE
    TRUE if signal processed successfully
    FALSE otherwise

  SIDE EFFECTS
    None
===========================================================================*/
static boolean qmuxi_process_rx_sig
( 
  qmi_sig_enum_type sig,                   /* Signal to be processed       */
  void *user_data_ptr                      /* Parameter carrying user data */
)
{
  qmux_state_type *    qmux_s;
  dsm_item_type *      qmux_pdu;
  qmi_instance_e_type  qmi_instance;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  qmux_s = qmux_state;
  
  /*-------------------------------------------------------------------------
    Process each QMI link
  -------------------------------------------------------------------------*/
  for (qmi_instance = QMI_INSTANCE_MIN;
       qmi_instance < QMI_INSTANCE_MAX;
       qmi_instance++, qmux_s++)
  {
    if(qmux_s->initialized == FALSE)
    {
      /* Skip uninitialized instances. */
      continue;
    }

    /*---------------------------------------------------------------------
      Retrieve and dispatch the pending QMUX command
    ---------------------------------------------------------------------*/
    qmux_pdu = dsm_dequeue( &qmux_s->io.rx_wm );
    if (qmux_pdu == NULL)
    {
      continue;
    }

    /*---------------------------------------------------------------------
      Unframe the SDU if the port has been preconfigured for framing.
    ---------------------------------------------------------------------*/
    if( IS_FRAMING_REQUIRED( qmux_s->io.port_info.frame_mode ) )
    {
      if( !qmux_unframe_sdu( &qmux_pdu, &qmux_s->io.pending_sdu_ptr ) )
      {
        LOG_MSG_INFO2_1 ("Error on unframing SDU: %d", qmi_instance);
        continue;
      }
      
      /* Check for partial PDU case, where SDU was cached for next cycle */
      if (qmux_pdu == NULL)
      {
        LOG_MSG_INFO2_1 ("Partial PDU cached for next cycle: %d",
                         qmi_instance);
        continue;
      }
    }

    qmuxi_process_msg( qmux_s, qmux_pdu );

    /*-----------------------------------------------------------------------
      Process one command per call.  Set signal if commands remain so we'll
      pass through the DS main loop again, allowing DS commands to be 
      processed.
    -----------------------------------------------------------------------*/
    if( q_cnt( qmux_s->io.rx_wm.q_ptr ) )
    {
      qmi_set_signal( QMI_QMUX_RX_SIGNAL );
    }
  }

  return TRUE;
} /* qmux_process_rx_sig() */



/*===========================================================================
  FUNCTION QMI_INSTANCE_BY_QMUX_HANDLE()

  DESCRIPTION
    To obtain QMI instance from QMUX handle. Qmux handle passed in must 
    ALWAYS be the service handle.

  PARAMETERS  
    qmux_handle : QMUX service handle returned to service by qmux_reg_service

  RETURN VALUE
    QMI Instance if qmux_handle is valid
    QMI_INSTANCE_MAX if qmux_handle is invalid

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
qmi_instance_e_type  qmi_instance_by_qmux_handle
(
  qmux_svc_info_type * service_handle
)
{
  qmi_instance_e_type  retval;
  int                  i;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  for (retval = QMI_INSTANCE_MIN; retval < QMI_INSTANCE_MAX; retval++)
  {
    for (i = 0; i < QMUX_SERVICE_MAX; i++)
    {
      if (qmux_state[retval].service[i] == service_handle)
      {
        return retval;
      }
    }
  }
    
  if (retval >= QMI_INSTANCE_MAX)
  {
    LOG_MSG_ERROR_1 ("Invalid QMUX Service handle! (0x%x)", service_handle);
    return QMI_INSTANCE_MAX;
  }

  return retval;

} /* qmi_instance_by_qmux_handle() */
  

/*===========================================================================
  FUNCTION QMI_GET_NUM_INSTANCES()

  DESCRIPTION
    Returns number of QMI instances.

  PARAMETERS  
    qmi_instance : instance of QMI

  RETURN VALUE
    Number of QMI instances defined

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
uint8 qmi_get_num_instances( void )
{
  return qmux_num_instances;
}

/*===========================================================================
  FUNCTION QMI_GET_MUX_PORT_CFG()

  DESCRIPTION
    Returns QMI MUX port configuration.

  PARAMETERS
    None

  RETURN VALUE
   MUX port configuration.

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
extern qmi_mux_port_cfg_enum qmi_get_mux_port_cfg( void )
{
  return qmi_mux_port_cfg;
} /* qmi_get_mux_port_cfg() */

/*===========================================================================
  FUNCTION QMI_GET_MUX_MASTER_SIO_PORT()

  DESCRIPTION
    Returns the master SIO port for muxing

  PARAMETERS
    None

  RETURN VALUE
    The master sio port that is used for muxing
    SIO_PORT_NULL if not defined

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
extern uint16 qmi_get_mux_master_sio_port( void )
{
  return (uint16)qmi_mux_master_sio_port;

} /* qmi_get_mux_master_sio_port() */

/*===========================================================================
  FUNCTION QMI_GET_INSTANCE_INITIALIZED()
   
  DESCRIPTION
    Returns instance initialized flag.

  PARAMETERS
    qmi_instance : instance of QMI

  RETURN VALUE
    TRUE if QMUX instance initialized
    FALSE otherwise

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean  qmi_get_instance_initialized
(
  qmi_instance_e_type     qmi_instance
)
{
  return qmux_state[qmi_instance].initialized;
} /* qmi_instance_initialized() */

/*===========================================================================
  FUNCTION QMUXI_GET_QMI_CONTROL_DEVICE_ID()
   
  DESCRIPTION
    Returns instance QMUX device ID type associated with QMI instance.

  PARAMETERS
    qmi_instance : instance of QMI

  RETURN VALUE
    Device ID if valid instance.
    QMI_DEV_NONE otherwise

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
qmux_device_id_type qmuxi_get_qmi_control_device_id
(
  qmi_instance_e_type     qmi_inst
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  
  if (QMI_INSTANCE_MAX <= qmi_inst)
  {
    LOG_MSG_ERROR_1 ("QMI instance %d out of range", qmi_inst);
    return QMI_DEV_NONE;
  }

  if (FALSE == qmux_state[qmi_inst].initialized)
  {
    LOG_MSG_ERROR_1 ("QMUX instance %d not initialized", qmi_inst);
    return QMI_DEV_NONE;
  }

  return (qmux_device_id_type)qmux_state[qmi_inst].io.port_info.qmi_port;

} /* qmuxi_get_qmi_control_device_id() */

/*===========================================================================
  FUNCTION QMUX_VERIFY_DTR_HIGH()

  DESCRIPTION
    Returns DTR status on QMI instance

  PARAMETERS
    qmi_inst : instance of QMI

  RETURN VALUE
    TRUE if DTR is asserted for the instance
    FALSE otherwise

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean  qmux_verify_dtr_high
(
  qmi_instance_e_type     qmi_inst
)
{
#ifndef FEATURE_DATA_NO_QMI_TEST
  qmux_state_type       *qmux_s;
  sio_ioctl_param_type  ioctl_param;
  boolean               dtr_status = FALSE;

  ASSERT( (qmi_inst >= QMI_INSTANCE_MIN) && (qmi_inst < QMI_INSTANCE_MAX) );
  qmux_s = &qmux_state[qmi_inst];

  if ( (qmux_s->io.sio_handle == 0 ) ||
       (qmux_s->io.sio_handle == SIO_NO_STREAM_ID) )
  {
    LOG_MSG_ERROR_1("QMI inst [%d] port not initialized", qmi_inst);
    return FALSE;
  }

  memset(&ioctl_param, 0, sizeof(ioctl_param));
  ioctl_param.dte_ready_asserted = &dtr_status;

  qmuxi_sio_ioctl(qmux_s,
              SIO_IOCTL_DTE_READY_ASSERTED,
              &ioctl_param);

  LOG_MSG_INFO2_3("QMI inst [%d] stream ID [%d] DTR status [%d]",
                  qmi_inst, qmux_s->io.sio_handle, dtr_status);

  return dtr_status;
#else
  return TRUE;
#endif /* FEATURE_DATA_NO_QMI_TEST */

} /* qmux_verify_dtr_high() */

/*===========================================================================
  FUNCTION QMUX_GET_INST_FROM_SIO_DATA_PORT()

  DESCRIPTION
    Returns QMI instance for the specified SIO data port

  PARAMETERS
    sio_port : Data channel SIO port

  RETURN VALUE
    QMI instance that uses the specified SIO data port
    QMI_INSTANCE_MAX if not found

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
qmi_instance_e_type qmux_get_inst_from_sio_data_port
(
  uint16  sio_port
)
{
  qmux_rdm_dev_map_type  * map_ptr = NULL;
  int                      i;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (sio_port == SIO_PORT_NULL)
  {
    return QMI_INSTANCE_MAX;
  }

  for (i = 0; i < QMI_INSTANCE_MAX; i++)
  {
    if (qmuxi_find_mapped_qmi_device(
                qmux_state[i].io.port_info.data_port, &map_ptr))
    {
      if (map_ptr->sio_port == sio_port)
      {
        return (qmi_instance_e_type)i;
      }
    }
  }

  return QMI_INSTANCE_MAX;

} /* qmux_get_inst_from_sio_data_port() */

/*===========================================================================
FUNCTION QMUX_GET_DEFAULT_MUX_IDX

DESCRIPTION
  This function returns the default mux index of the SIO data port

PARAMETERS
  sio_port: SIO port

DEPENDENCIES
  None

RETURN VALUE
  default mux index of the port. 0 if not a muxed port.

SIDE EFFECTS
  None.
===========================================================================*/
uint8 qmux_get_default_mux_idx
(
  sio_port_id_type sio_port
)
{
  int i, n;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  n = sizeof(mux_id_map) / sizeof(mux_id_map[0]);

  for (i = 0; i < n; i++)
  {
    if (mux_id_map[i].sio_port == sio_port)
    {
      return mux_id_map[i].default_mux_id;
    }
  }

  return 0;

} /* qmux_get_default_mux_idx() */

/*===========================================================================
FUNCTION QMUX_GET_QMAP_MUX_ID

DESCRIPTION
  This function returns the qmap mux id of the SIO data port

PARAMETERS
  sio_port: SIO port

DEPENDENCIES
  None

RETURN VALUE
  qmap mux_id of the port. 0 if not a qmap muxed port.

SIDE EFFECTS
  None.
===========================================================================*/
uint8 qmux_get_qmap_mux_id
(
  sio_port_id_type sio_port
)
{
  int i, n;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  n = sizeof(mux_id_map) / sizeof(mux_id_map[0]);

  for (i = 0; i < n; i++)
  {
    if (mux_id_map[i].sio_port == sio_port)
    {
      return mux_id_map[i].qmap_mux_id;
    }
  }

  return 0;

} /* qmux_get_qmap_mux_id() */

/*===========================================================================
FUNCTION QMUX_GET_PORT_FROM_QMAP_MUX_ID

DESCRIPTION
  This function returns the sio port which has the qmap mux_id

PARAMETERS
  qmap_mux_id: qmap mux id

DEPENDENCIES
  None

RETURN VALUE
  sio port which has the qmap mux_id. SIO_PORT_NULL if not found.

SIDE EFFECTS
  None.
===========================================================================*/
sio_port_id_type qmux_get_port_from_qmap_mux_id
(
  uint8  qmap_mux_id
)
{
  int i, n;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  n = sizeof(mux_id_map) / sizeof(mux_id_map[0]);

  for (i = 0; i < n; i++)
  {
    if (mux_id_map[i].qmap_mux_id == qmap_mux_id)
    {
      return mux_id_map[i].sio_port;
    }
  }

  return SIO_PORT_NULL;

} /* qmux_get_port_from_qmap_mux_id() */


/*===========================================================================

                        INTERNAL FUNCTION DEFINITIONS

===========================================================================*/
/*===========================================================================
  FUNCTION QMUXI_INIT()

  DESCRIPTION
    Initialize specified QMUX instance.  SIO control IO path will be set up,
    and QMUX instance state will be reset to default.

  PARAMETERS
    qmi_instance : qmi link to initialize 

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static boolean qmuxi_init
(
  qmi_instance_e_type  qmi_instance
)
{
  qmux_state_type *  qmux_s;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT (qmi_instance < QMI_INSTANCE_MAX);
  
  /* check that qmi_instance is within range in case ASSERT is removed */
  if (qmi_instance >= QMI_INSTANCE_MAX)
  {
    LOG_MSG_ERROR_1("QMI instance out of range (%d)!", qmi_instance);
    return FALSE;
  }
  
  qmux_s = &qmux_state[qmi_instance];

  if (qmux_s->initialized)
  {
    LOG_MSG_INFO1_0 ("QMUX instance already initialized!");
    return TRUE;
  }

  /*-------------------------------------------------------------------------
    Initialize the control stream RX and TX watermarks
  -------------------------------------------------------------------------*/
  /*-------------------------------------------------------------------------
    Initialize the watermarks and queues used for USB control stream
  -------------------------------------------------------------------------*/
  memset (&qmux_s->io.rx_wm, 0, sizeof(qmux_s->io.rx_wm));
  memset (&qmux_s->io.rx_wm_q, 0, sizeof(qmux_s->io.rx_wm_q));

  dsm_queue_init( &qmux_s->io.rx_wm,
                  QMUX_RX_WM_DNE,
                  &qmux_s->io.rx_wm_q );
  
  memset (&qmux_s->io.tx_wm, 0, sizeof(qmux_s->io.tx_wm));
  memset (&qmux_s->io.tx_wm_q, 0, sizeof(qmux_s->io.tx_wm_q));

  dsm_queue_init( &qmux_s->io.tx_wm,
                  QMUXI_TX_DNE,
                  &qmux_s->io.tx_wm_q );

  /*-------------------------------------------------------------------------
                             SETUP SIO TX WATERMARK
  -------------------------------------------------------------------------*/

  /*-------------------------------------------------------------------------
    This DSM watermark queue holds data to be sent by sio on the QMUX control 
    channel to the host.

    If this watermark reaches the high water mark, we'll reset the connection
    to maintain order.  QMUX should never reach this point.
  -------------------------------------------------------------------------*/
  qmux_s->io.tx_wm.lo_watermark          = 0;
  qmux_s->io.tx_wm.hi_watermark          = 0;

  qmux_s->io.tx_wm.dont_exceed_cnt = QMUXI_TX_DNE;
  LOG_MSG_INFO1_1( "Set control tx_wm to be infinite size: %d",
                   qmux_s->io.tx_wm.dont_exceed_cnt );

  qmux_s->io.tx_wm.gone_empty_func_ptr   = NULL;
  qmux_s->io.tx_wm.non_empty_func_ptr    = NULL;
  qmux_s->io.tx_wm.lowater_func_ptr      = NULL;
  qmux_s->io.tx_wm.hiwater_func_ptr      = NULL;
  qmux_s->io.tx_wm.each_enqueue_func_ptr = NULL;

  /*-------------------------------------------------------------------------
                             SETUP SIO RX WATERMARK
  -------------------------------------------------------------------------*/

  /*-------------------------------------------------------------------------
    This DSM watermark queue holds data received by sio on the QMUX control 
    channel from the host.

    Generally, when something arrives in this queue, the qmuxi_sio_rx_cb will
    be called and all outstanding QMUX messages will be processed.  If for
    some reason all messages can't be processed right then (e.g. QMUX channel
    is blocked on completion of some synchronous operation), the subroutine
    that later completes the operation is responsible for prompting the
    serving task to continue processing the QMUX tx queue.
    
    SIO watermark use should be synchonized with the ISR used by the
    serial driver.
  -------------------------------------------------------------------------*/
  /*-------------------------------------------------------------------------
    Set low, high, and dont exceed limits.
  -------------------------------------------------------------------------*/
  qmux_s->io.rx_wm.lo_watermark           = QMUX_RX_WM_LOW;
  qmux_s->io.rx_wm.hi_watermark           = QMUX_RX_WM_HIGH;
  qmux_s->io.rx_wm.dont_exceed_cnt        = QMUX_RX_WM_DNE;
 
  qmux_s->io.rx_wm.lowater_func_ptr       = NULL;
  qmux_s->io.rx_wm.hiwater_func_ptr       = NULL; 
  qmux_s->io.rx_wm.gone_empty_func_ptr    = NULL;
  qmux_s->io.rx_wm.non_empty_func_ptr     = NULL; 
  qmux_s->io.rx_wm.each_enqueue_func_ptr  = qmuxi_sio_rx_cb; 
  qmux_s->io.rx_wm.each_enqueue_func_data = qmux_s; 

#ifdef FEATURE_DSM_MEM_CHK
  /*-------------------------------------------------------------------------
    Reset the total_rcvd_cnt, as a packet data call can really set this
    number very high.
  -------------------------------------------------------------------------*/
  qmux_s->io.tx_wm.total_rcvd_cnt        = 0;
  qmux_s->io.rx_wm.total_rcvd_cnt        = 0;
#endif

  qmux_s->initialized = TRUE;
  return TRUE;
} /* qmuxi_init() */



/*===========================================================================

  FUNCTION QMUXI_SIO_RX_CB

  DESCRIPTION
    Invoked when data arrives in the QMUX control channel receive watermark.

  PARAMETERS
    wm_ptr        -  pointer to control channel receive watermark
    user_data_ptr -  opaque handle to QMUX instance state
    
  DEPENDENCIES
    None
  
  RETURN VALUE
    None

  SIDE EFFECTS
    None

===========================================================================*/
static void  qmuxi_sio_rx_cb
(  
  struct dsm_watermark_type_s *  wm_ptr,
  void *                         user_data_ptr
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------------
    Signal to serving task that QMI commands are outstanding.
    Multiplexing all QMI links on the same RX signal.
  -------------------------------------------------------------------------*/
  qmi_set_signal( QMI_QMUX_RX_SIGNAL );

} /* qmuxi_sio_rx_cb() */



/*===========================================================================
  FUNCTION QMUXI_INPUT

  DESCRIPTION
    Receive incoming QMUX message, decapsulate QMUX header and dispatch to
    the appropriate registered service input function.

  PARAMETERS  
    qmux_s   : pointer to QMUX link state for the receiving control interface
    qmux_pdu : incoming pdu
  
  DEPENDENCIES
    qmux_init() must have been called on 'qmi_instance' previously

  RETURN VALUE
    TRUE  - more commands pending on the receive queue for this QMI link
    FALSE - Done processing commands on this QMI link for now. Caller
            will free the input pdu

  SIDE EFFECTS
    None
===========================================================================*/
static boolean qmuxi_input
(
  qmux_state_type *  qmux_s,
  dsm_item_type **   qmux_pdu
)
{
  qmux_svc_info_type *  svci;
  qmux_hdr_type         qmux_hdr;
  boolean               status = FALSE;
  uint8                 msg_ctl;
  uint8                 tx_id_hb;
  uint8                 tx_id_lb;
  uint16                tx_id;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT (qmux_s);
  ASSERT (qmux_pdu);
  svci = NULL; 


  /* check that qmux_s is not NULL in case ASSERT is removed */
  if (qmux_s == NULL)
  {
    LOG_MSG_ERROR_0("QMUX state NULL!");
    return FALSE;
  }

  /* check that qmux_pdu is not NULL in case ASSERT is removed */
  if (qmux_pdu == NULL)
  {
    LOG_MSG_ERROR_0("QMUX PDU NULL!");
    return FALSE;
  }
  
  LOG_MSG_INFO2_2( "Received QMUX PDU (%d bytes) on QMI link %d",
                   dsm_length_packet(*qmux_pdu),
                   qmi_instance_by_qmux_state(qmux_s) );

  /*-------------------------------------------------------------------------
    Pull up QMUX header
  -------------------------------------------------------------------------*/
  if( sizeof(qmux_hdr) != 
      qmi_svc_dsm_pullup( qmux_pdu, (void *) &qmux_hdr, sizeof(qmux_hdr) ,
                        FILE_ID_DS_QMUX,__LINE__  ) )
  { 
    return FALSE;
  } 

  /*-------------------------------------------------------------------------
    Check if MSM can handle this message
    - Make sure PDU is exact length spec'd in hdr, otherwise throw it out.
    - Sender must be a control point
    - Service type must be defined and registered
    - Client ID must be valid
  -------------------------------------------------------------------------*/
  if( qmux_hdr.len != dsm_length_packet(*qmux_pdu) + sizeof(qmux_hdr) )
  {
    LOG_MSG_ERROR_2 ("Invalid control frame len (%d) expected (%d)!",
                     qmux_hdr.len, dsm_length_packet(*qmux_pdu));
    return FALSE;
  }
  
  if( (qmux_hdr.ctl_flags & QMUXI_CFLAG_MASK_SENDER) !=
      QMUXI_CFLAG_SENDER_CONTROL_POINT )
  {
    LOG_MSG_ERROR_0 ("QMI service message not handled");
    return FALSE;
  }

  /*-----------------------------------------------------------------------
     Return error if:
     - svc_type is not Internal QC service range and not in VS range
  -----------------------------------------------------------------------*/
  if (qmux_hdr.svc_type >= QMUX_SERVICE_MAX)
  {
    LOG_MSG_ERROR_1 ("Invalid QMUX type (%d)!", qmux_hdr.svc_type);
    return FALSE;
  }

  /*-----------------------------------------------------------------------
    Block services STX and SIM_INTERNAL from Apps
  -----------------------------------------------------------------------*/
#if defined(FEATURE_8960_SGLTE_FUSION)
  if((qmux_hdr.svc_type == QMI_SIM_INTERNAL) || (qmux_hdr.svc_type == QMI_STX))
  {
    if (!(qmux_s->io.port_info.qmi_port == QMI_DEV_MUX2))
    {
      LOG_MSG_INFO2_1 ("Request is dropped. Blocking service %d to SGLTE",
                       qmux_hdr.svc_type);
      return FALSE;
    }
  }
#endif

  /*-----------------------------------------------------------------------
    Get service info.
  -----------------------------------------------------------------------*/
  svci = qmux_s->service[qmux_hdr.svc_type];

  /*-------------------------------------------------------------------------
    Log the QMUX hdr info, msg_ctl, QMI instance and tx_id in SMEM
  -------------------------------------------------------------------------*/
  dsm_peek_byte(*qmux_pdu, 0, &msg_ctl );
  dsm_peek_byte(*qmux_pdu, 1, &tx_id_lb );
  dsm_peek_byte(*qmux_pdu, 2, &tx_id_hb );
  tx_id = ((((uint16)(tx_id_hb) & 0x00FF) << 8) | (uint16)tx_id_lb);
  SMEM_LOG_EVENT6(SMEM_QMUX_RX_EVENT, 
                  qmux_hdr.len, qmux_hdr.ctl_flags,
                  qmux_hdr.svc_type, qmux_hdr.clid,
                  ((((uint32)(qmi_instance_by_qmux_state(qmux_s)) & 0x0000FFFF) << 16) | (uint32)msg_ctl),
                  tx_id);
  /*-----------------------------------------------------------------------
    if service is not found in legacy framework or VS service, then
    check in new QMI framework.
  -----------------------------------------------------------------------*/
  if((svci == NULL) || (!(svci->registered)))
  {
    
    LOG_MSG_INFO1_1 ("Calling Framework receive function as the service%d was not found in legacy framework",
                     qmux_hdr.svc_type);
    
    status =  qmi_framework_svc_recv( qmi_instance_by_qmux_state(qmux_s),
                               (qmux_service_e_type) qmux_hdr.svc_type,
                               qmux_hdr.clid,
                               *qmux_pdu );
    *qmux_pdu = NULL;
    return status;
  }

  /*-------------------------------------------------------------------------
    Dispatch incoming request to QCTL or legacy service.
    Since QCTL is running on QMI Modem task and has own svc recv input function
    the below check is required to differentiate the dispatch function(legacy
    or QCTL).
  -------------------------------------------------------------------------*/
  if (svci->service_id == QMUX_SERVICE_CTL)
  {
    LOG_MSG_INFO2_3 ("Dispatching svc_type %d msg (%d bytes) for client (%d)",
                     qmux_hdr.svc_type,
                     dsm_length_packet(*qmux_pdu),
                     qmux_hdr.clid );
    if (svci->cfg.cbs.recv)
    {
      (svci->cfg.cbs.recv)( svci->cfg.sp, qmux_hdr.clid, *qmux_pdu );
    }
    *qmux_pdu = NULL;  
    return TRUE;
  }
  else
  {
    /*-----------------------------------------------------------------------
      Post the service request to legacy service.
    -----------------------------------------------------------------------*/
    status =  qmi_cmd_recv_legacy_svc_req(svci, qmux_hdr.clid, *qmux_pdu);
    *qmux_pdu = NULL;
    return status;
  }
} /* qmuxi_input() */



/*===========================================================================
  FUNCTION QMUX_PROCESS_MSG

  DESCRIPTION
    Process a received QMUX message

  PARAMETERS  
    qmux_s   -  QMI link state pointer
    qmux_pdu -  received QMI command to be processed
  
  DEPENDENCIES
    qmux_init() must have been called on 'qmi_instance' previously

  RETURN VALUE
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmuxi_process_msg  // rename as qmuxi_process_pdu
( 
  qmux_state_type *  qmux_s,
  dsm_item_type *    qmux_pdu
)
{
  int16            iftype;
#if 0
  dsm_item_type *  pdu_p;
#endif
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------------
    Log received QMI packet if logging is enabled
  -------------------------------------------------------------------------*/
  qmi_log_packet(qmux_pdu, 
                 qmi_instance_by_qmux_state(qmux_s),
                 QMUX_RX);

#if 0 /* replace with log frame when available */
  LOG_MSG_INFO2_0("Got QMUX");
  pdu_p = qmux_pdu;
  while (pdu_p)
  {
    memdump( pdu_p->data_ptr, pdu_p->used );
    pdu_p = pdu_p->pkt_ptr;
  }
#endif /* replace with log frame when available */

  /*-------------------------------------------------------------------------
    Pull off control message PDU type
  -------------------------------------------------------------------------*/
  iftype = dsm_pull8(&qmux_pdu);
  if( (qmux_frame_mode_type)iftype != qmux_s->io.port_info.frame_mode )
  {
    /* IFType does not match port configuration */
    LOG_MSG_ERROR_2( "IFType configuration mismatch (%d!=%d), dropping packet",
                     	 iftype, qmux_s->io.port_info.frame_mode );
  }
  else
  {
    /* IFType consistent with configuration, process packet */
    switch (iftype)
    {
      case QMUXI_IFTYPE_QMUX:
      case QMUXI_IFTYPE_QMUX_FRAMED:
      {
	/*-------------------------------------------------------------------
	  Ignore return value
	-------------------------------------------------------------------*/
	(void) qmuxi_input (qmux_s, &qmux_pdu);
	break;
      }
      default:
      {
	LOG_MSG_ERROR_1 ("Invalid control frame type (%d)!", iftype);
	break;
      }
    }
  }
  /*-------------------------------------------------------------------------
    Make sure input message is freed if input function hasn't done so
  -------------------------------------------------------------------------*/
  dsm_free_packet (&qmux_pdu);

} /* qmuxi_process_msg() */



/*===========================================================================
  FUNCTION QMUXI_OPEN_INSTANCE

  DESCRIPTION
    Opens up a QMI instance

  PARAMETERS  
    qmi_instance   -  QMI instance
    sio_port       -  SIO port ID
  
  DEPENDENCIES
    qmux_init() must have been called on 'qmi_instance' previously

  RETURN VALUE
    None

  SIDE EFFECTS
    None
===========================================================================*/
static boolean qmuxi_open_instance
(
  qmi_instance_e_type  qmi_instance,
  uint16               sio_port
)
{
  sio_open_type      sioctl_config;
  qmux_state_type *  qmux_s;
  sio_ioctl_param_type  ioctl_param;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT (qmi_instance < QMI_INSTANCE_MAX);

  /* check that qmi_instance is within range in case ASSERT is removed */
  if (qmi_instance >= QMI_INSTANCE_MAX)
  {
    LOG_MSG_ERROR_1("QMI instance out of range (%d)!", qmi_instance);
    return FALSE;
  }

  qmux_s = &qmux_state[qmi_instance];

  /*-------------------------------------------------------------------------
    Init the qmi state for the provided instance.
    Note usb control bitrates should not affect sio behaviour.
  -------------------------------------------------------------------------*/
  memset(&sioctl_config, 0, sizeof(sio_open_type));
  sioctl_config.stream_mode    = SIO_GENERIC_MODE;
  sioctl_config.rx_queue       = &qmux_s->io.rx_wm;
  sioctl_config.tx_queue       = &qmux_s->io.tx_wm;
  sioctl_config.rx_bitrate     = SIO_BITRATE_BEST;
  sioctl_config.tx_bitrate     = SIO_BITRATE_BEST;
  sioctl_config.port_id        = (sio_port_id_type)sio_port;
  sioctl_config.rx_func_ptr    = NULL;
  sioctl_config.rx_flow        = SIO_FCTL_BEST;
  sioctl_config.tx_flow        = SIO_FCTL_BEST;

  /*-------------------------------------------------------------------------
    Assign tail char for streaming driver packetization if so
    configured. This ensures packets are delivered from SIO driver
    only when this octet is detected.
  -------------------------------------------------------------------------*/
  if( IS_FRAMING_REQUIRED( qmux_s->io.port_info.frame_mode ) )
  {
     sioctl_config.tail_char_used = TRUE;
     sioctl_config.tail_char      = QMUX_FRAME_FLAG_STOP;
  }
  else
  {
     sioctl_config.tail_char_used = FALSE;
     sioctl_config.tail_char      = 0;
  }

  /*-------------------------------------------------------------------------
    Open the Network interface control channel with these parameters
  -------------------------------------------------------------------------*/
  if( QMUX_DEVSTREAM_CONTROL == qmux_s->io.port_info.qmi_stream )
  {
    /* Set the SMD FIFO size to 8K for tethered ports, so that it can handle
       max USB packet size of 4K. If this is not set, default size is 1K */
    if (IS_TETHERED_PORT(sio_port))
    {
      sioctl_config.open_param = (sio_open_param_type *)
        qmi_svc_ps_system_heap_mem_alloc(sizeof(sio_open_param_type),
                           FILE_ID_DS_QMUX,__LINE__);
      if (NULL == sioctl_config.open_param)
      {
        ASSERT(0);
        return FALSE;
      }
      memset(sioctl_config.open_param, 0, sizeof(sio_open_param_type));
      sioctl_config.open_param->dsm_pool_param.smd_fifo_size = 0x2000; 
    }
    qmux_s->io.sio_handle = sio_control_open (&sioctl_config);
    if (sioctl_config.open_param)
    {
      PS_SYSTEM_HEAP_MEM_FREE(sioctl_config.open_param);
    }
  }
  else
  {
    qmux_s->io.sio_handle = sio_open( &sioctl_config );
  }
  
  if (qmux_s->io.sio_handle == SIO_NO_STREAM_ID)
  {
    LOG_MSG_ERROR_1( "SIO open failed on stream: %d",
                     qmux_s->io.port_info.qmi_stream);
    return FALSE;
  }
  else
  {
     /* We need to set the DROP_PENDING ioctl on SMD ports, in order to
      * enable the SMD module to drop packets when the onward link to apps
      * proc is not available. */
    if ( (sio_port >= SIO_PORT_SMD_DATA_FIRST) &&
         (sio_port <= SIO_PORT_SMD_DATA_LAST) )
    {
      memset( &ioctl_param, 0, sizeof(sio_ioctl_param_type) );
      ioctl_param.drop_asserted = TRUE;
      if (QMUX_DEVSTREAM_CONTROL == qmux_s->io.port_info.qmi_stream)
      {
        sio_control_ioctl(qmux_s->io.sio_handle,
                          SIO_IOCTL_SET_DROP_PEND,
                          &ioctl_param);
      }
      else
      {
        sio_ioctl(qmux_s->io.sio_handle,
                  SIO_IOCTL_SET_DROP_PEND,
                  &ioctl_param);
      }
    }
  }

#ifdef FEATURE_DATA_RM_NET_USES_SM_LAPTOP_INST // Temporary: pending SMD supprot for internal DSR assertion 
  if( QMI_INSTANCE_APPS_PROC_MAX >= qmi_instance )
  {
    /*-----------------------------------------------------------------------
      Assert DSR signal so remote processor can detect port opened.
    -----------------------------------------------------------------------*/
    memset( &ioctl_param, 0, sizeof(sio_ioctl_param_type) );
    sio_control_ioctl( qmux_s->io.sio_handle, SIO_IOCTL_DSR_ASSERT, &ioctl_param );
  }
#endif /* FEATURE_DATA_RM_NET_USES_SM_LAPTOP_INST */

#ifdef FEATURE_DATA_QMI_DTR_ON_CTRL_CHANNEL
  /* Though DTR is received on Control Channel, there is internal dependency
     on Rmnet/Data Channel to be used. Currently, the underlying USB
     support for DTR on Control channel is not available on TN platform, 
     though the feature is enabled. Hence, adding this check on data port.
     This means there will not be any DTR support when data port is not used.
     This dependency, and inturn this check, needs to be cleaned up when the
     corresponding USB support to enable DTR on Control Channel is available
     on all platforms.
   */    
  if( QMI_DEV_NONE != (qmux_device_id_type)qmux_s->io.port_info.data_port )
  {
    qmuxi_set_dtr_handler(qmi_instance, qmuxi_dtr_cb);
  }  
#endif /* FEATURE_DATA_QMI_DTR_ON_CTRL_CHANNEL */

  return TRUE;
} /* qmuxi_open_instance() */


/*===========================================================================
  FUNCTION QMUXI_CLOSE_INSTANCE

  DESCRIPTION
    Closes a QMI instance

  PARAMETERS  
    qmi_instance   -  QMI instance
  
  DEPENDENCIES
    qmux_init() must have been called on 'qmi_instance' previously

  RETURN VALUE
    None

  SIDE EFFECTS
    None
===========================================================================*/
static boolean qmuxi_close_instance
(
  qmi_instance_e_type  qmi_instance
)
{
  qmux_state_type *  qmux_s;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT (qmi_instance < QMI_INSTANCE_MAX);
  
  /* check that qmi_instance is within range in case ASSERT is removed */
  if (qmi_instance >= QMI_INSTANCE_MAX)
  {
    LOG_MSG_ERROR_1("QMI instance out of range (%d)!", qmi_instance);
    return FALSE;
  }
  
  qmux_s = &qmux_state[qmi_instance];

  qmux_link_down( qmi_instance );

  ASSERT (qmux_s->io.sio_handle != SIO_NO_STREAM_ID);

  /* check that qmux_s->io.sio_handle is set in case ASSERT is removed */
  if (qmux_s->io.sio_handle == SIO_NO_STREAM_ID)
  {
    LOG_MSG_ERROR_1("QMUX sio_handle stream ID not set (%d)!",
                    qmux_s->io.sio_handle);
    return FALSE;
  }

#ifdef FEATURE_DATA_QMI_DTR_ON_CTRL_CHANNEL
  qmuxi_set_dtr_handler(qmi_instance, NULL);
#endif /* FEATURE_DATA_QMI_DTR_ON_CTRL_CHANNEL */

  if( QMUX_DEVSTREAM_CONTROL == qmux_s->io.port_info.qmi_stream )
  {
  sio_control_close( qmux_s->io.sio_handle, NULL );
  }
  else
  {
    sio_close( qmux_s->io.sio_handle, NULL );
  }

  return TRUE;
}



/*===========================================================================
  FUNCTION QMUXI_SET_DEFAULT_PORT_CONFIGURATION

  DESCRIPTION
    This function assigns the default port configuraiton table.  It shoudl be
    used when NVRAM value has not been previously assigned (e.g. factory
    settings). 
  
  PARAMETERS  
    list_ptr   -  Pointer to list of port configurations
  
  DEPENDENCIES
    None

  RETURN VALUE
    Number of ports in configuraiton table

  SIDE EFFECTS
    None
===========================================================================*/
static uint8 qmuxi_set_default_port_configuration
(
  qmi_port_list_type  *list_ptr
)
{
  uint8 index = 0;
#if defined(FEATURE_RMNET_PORT_CONFIG_MSM) && defined(FEATURE_DATA_WLAN_MAPCON)
  ds_wlan_offload_config_enum_type offload_cfg = 
                                      DS_WLAN_OFFLOAD_CONFIG_DEFAULT;
#endif /* defined(FEATURE_RMNET_PORT_CONFIG_MSM) && defined(FEATURE_DATA_WLAN_MAPCON) */
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  
  ASSERT( NULL != list_ptr );

  /* check that list_ptr is not NULL in case ASSERT is removed */
  if (list_ptr == NULL)
  {
    LOG_MSG_ERROR_0("NV QMI port list pointer NULL!");
    return 0;
  }

#define QMUX_SET_DEFAULT_DEVICE_CONFIG(i,ctrldev,datadev)           \
  list_ptr->port_list[i].qmi_port    = (int)(ctrldev);              \
  list_ptr->port_list[i].qmi_stream  = (int)QMUX_DEVSTREAM_CONTROL; \
  list_ptr->port_list[i].frame_mode  = (int)QMUX_FRAME_MODE_NONE;   \
  list_ptr->port_list[i++].data_port = (int)(datadev);                    

  /*-----------------------------------------------------------------------
    Assign default ports based on build configuration.
  -----------------------------------------------------------------------*/

#if defined(FEATURE_RMNET_PORT_CONFIG_MDM)
  /*-----------------------------------------------------------------------
    DATA MPSS MDM configuration. 
      * Support 3 A2 ports and 5 SMD ports for tethered calls
      * And 1 additional control port
  -----------------------------------------------------------------------*/
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_A2P1, QMI_DEV_A2P1);
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_A2P2, QMI_DEV_A2P2);
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_A2P3, QMI_DEV_A2P3);
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SMD4_TETHERED, QMI_DEV_SMD4_TETHERED);
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SMD5_TETHERED, QMI_DEV_SMD5_TETHERED);
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SMD6_TETHERED, QMI_DEV_SMD6_TETHERED);
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SMD7_TETHERED, QMI_DEV_SMD7_TETHERED);
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SMD8_TETHERED, QMI_DEV_SMD8_TETHERED);
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SMD9_TETHERED, QMI_DEV_NONE);

#elif defined(FEATURE_RMNET_PORT_CONFIG_MDM_LE)
  /*-----------------------------------------------------------------------
    DATA MPSS MDM LE configuration. 
      * Support 8 MUX ports for embedded calls
      * 1 non-MUX A2 for tethered calls 
  -----------------------------------------------------------------------*/
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_0_EMBEDDED, QMI_DEV_SDIO_MUX_A2_0_EMBEDDED);
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_1_EMBEDDED, QMI_DEV_SDIO_MUX_A2_1_EMBEDDED);
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_2_EMBEDDED, QMI_DEV_SDIO_MUX_A2_2_EMBEDDED);
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_3_EMBEDDED, QMI_DEV_SDIO_MUX_A2_3_EMBEDDED);
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_4_EMBEDDED, QMI_DEV_SDIO_MUX_A2_4_EMBEDDED);
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_5_EMBEDDED, QMI_DEV_SDIO_MUX_A2_5_EMBEDDED);
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_6_EMBEDDED, QMI_DEV_SDIO_MUX_A2_6_EMBEDDED);
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_7_EMBEDDED, QMI_DEV_SDIO_MUX_A2_7_EMBEDDED);
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_8_EMBEDDED, QMI_DEV_SDIO_MUX_A2_8_EMBEDDED);
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_9_EMBEDDED, QMI_DEV_SDIO_MUX_A2_9_EMBEDDED);
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_10_EMBEDDED, QMI_DEV_SDIO_MUX_A2_10_EMBEDDED);
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_A2P1, QMI_DEV_A2P1);

#elif defined(FEATURE_RMNET_PORT_CONFIG_MSM)
  /*-----------------------------------------------------------------------
    DATA MPSS MSM configuration. 
      * Support 10 MUX_A2 ports for embedded calls 
      * 1 MUX_A2 port for tethered calls
  -----------------------------------------------------------------------*/
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_0_EMBEDDED, QMI_DEV_SDIO_MUX_A2_0_EMBEDDED);
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_1_EMBEDDED, QMI_DEV_SDIO_MUX_A2_1_EMBEDDED);
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_2_EMBEDDED, QMI_DEV_SDIO_MUX_A2_2_EMBEDDED);
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_3_EMBEDDED, QMI_DEV_SDIO_MUX_A2_3_EMBEDDED);
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_4_EMBEDDED, QMI_DEV_SDIO_MUX_A2_4_EMBEDDED);
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_5_EMBEDDED, QMI_DEV_SDIO_MUX_A2_5_EMBEDDED);
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_6_EMBEDDED, QMI_DEV_SDIO_MUX_A2_6_EMBEDDED);
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_7_EMBEDDED, QMI_DEV_SDIO_MUX_A2_7_EMBEDDED);
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_8_EMBEDDED, QMI_DEV_SDIO_MUX_A2_8_EMBEDDED);
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_9_EMBEDDED, QMI_DEV_SDIO_MUX_A2_9_EMBEDDED);
  #if defined(FEATURE_8960_SGLTE_FUSION)
    /* 1 port to QSC1215 for SGLTE */
     QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_MUX2, QMI_DEV_MUX2 );  
  #endif /* defined(FEATURE_8960_SGLTE_FUSION) */
  // Tethered port
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_0_TETHERED, QMI_DEV_SDIO_MUX_A2_0_TETHERED);

  /*---------------------------------------------------------------------------------- 
     1 port for WLAN Local Breakout and 8 ports for EPDG Reverse SIO,
     the corresponding data path will be handled by Reverse IP transport handler instead of RMNET
   -------------------------------------------------------------------------------------*/
#ifdef FEATURE_DATA_WLAN_MAPCON
  offload_cfg = ds_wlan_get_wlan_offload_config_nv( );

  LOG_MSG_INFO1_1("wlan_offload_config_nv set to %d", offload_cfg);
    
  if (DS_WLAN_OFFLOAD_CONFIG_DISABLED != offload_cfg )
  {
    QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_REV_IP_0, QMI_DEV_SDIO_MUX_A2_REV_IP_0);
    QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_REV_IP_1, QMI_DEV_SDIO_MUX_A2_REV_IP_1);
    QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_REV_IP_2, QMI_DEV_SDIO_MUX_A2_REV_IP_2);
    QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_REV_IP_3, QMI_DEV_SDIO_MUX_A2_REV_IP_3);
    QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_REV_IP_4, QMI_DEV_SDIO_MUX_A2_REV_IP_4);
    QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_REV_IP_5, QMI_DEV_SDIO_MUX_A2_REV_IP_5);
    QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_REV_IP_6, QMI_DEV_SDIO_MUX_A2_REV_IP_6);
    QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_REV_IP_7, QMI_DEV_SDIO_MUX_A2_REV_IP_7);
    QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_REV_IP_8, QMI_DEV_SDIO_MUX_A2_REV_IP_8);
  }
#endif /* FEATURE_DATA_WLAN_MAPCON */
  qmi_mux_master_sio_port = SIO_PORT_SDIO_MUX_A2_RMNET_0;

#elif defined(FEATURE_DATA_FUSION_MDM)
  /*-----------------------------------------------------------------------
    FUSION_MDM configuration. 
      * Support 8 SDIO ports for embedded calls 
      * 1 USB A2 port for embedded calls
  -----------------------------------------------------------------------*/
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_0, QMI_DEV_SDIO_MUX_A2_0);
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_1, QMI_DEV_SDIO_MUX_A2_1);
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_2, QMI_DEV_SDIO_MUX_A2_2);
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_3, QMI_DEV_SDIO_MUX_A2_3);
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_4, QMI_DEV_SDIO_MUX_A2_4);
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_5, QMI_DEV_SDIO_MUX_A2_5);
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_6, QMI_DEV_SDIO_MUX_A2_6);
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_7, QMI_DEV_SDIO_MUX_A2_7);

  #if defined(FEATURE_DATA_FUSION_CSFB) || defined(FEATURE_DATA_FUSION_MDM_TYPE_2)
    QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_8, QMI_DEV_SDIO_MUX_A2_8);
  #else
    QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_A2P1, QMI_DEV_A2P1 );
  #endif

#elif defined(FEATURE_DATA_FUSION_MSM)
  /*-----------------------------------------------------------------------
    FUSION_MSM configuration. 
      * Support upto 5 SMD ports for embedded calls 
      * 1 port to support tethered 1x/G/W call from MDM 
  -----------------------------------------------------------------------*/
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SMD1_EMBEDDED, QMI_DEV_SMD1_EMBEDDED );
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SMD2_EMBEDDED, QMI_DEV_SMD2_EMBEDDED );
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SMD3_EMBEDDED, QMI_DEV_SMD3_EMBEDDED );
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SMD4_EMBEDDED, QMI_DEV_SMD4_EMBEDDED );
  QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SMD5_EMBEDDED, QMI_DEV_SMD5_EMBEDDED );

  #ifdef FEATURE_DATA_FUSION_MSM_TYPE_2
    /* SVLTE Fusion Type 2 configuration */
    QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_MUX1, QMI_DEV_SMD1_TETHERED);
  #else
    /* SVLTE Fusion Type 1 configuration */
    QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_MUX1, QMI_DEV_MUX1);
  #endif

#else /* default configuration */

  #ifdef FEATURE_DATA_RM_NET_USES_SM
    QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SMD1_EMBEDDED, 
                                           QMI_DEV_SMD1_EMBEDDED );
    QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SMD2_EMBEDDED, 
                                           QMI_DEV_SMD2_EMBEDDED );
    QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SMD3_EMBEDDED, 
                                           QMI_DEV_SMD3_EMBEDDED );
    QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SMD4_EMBEDDED, 
                                           QMI_DEV_SMD4_EMBEDDED );
    QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SMD5_EMBEDDED, 
                                           QMI_DEV_SMD5_EMBEDDED );
    QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SMD6_EMBEDDED, 
                                           QMI_DEV_SMD6_EMBEDDED );
    QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SMD7_EMBEDDED, 
                                           QMI_DEV_SMD7_EMBEDDED );
    QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SMD8_EMBEDDED, 
                                           QMI_DEV_SMD8_EMBEDDED );
  #ifdef FEATURE_DATA_RM_NET_USES_SM_LAPTOP_INST
    QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SMD1_TETHERED, 
                                           QMI_DEV_SMD1_TETHERED );
  #endif /*  FEATURE_DATA_RM_NET_USES_SM_LAPTOP_INST */


  #else /* FEATURE_DATA_RM_NET_USES_SM */

  #ifdef FEATURE_DATA_A2
    /* A2 processor port#1 
       temporarily opening SMD1 instead of SMD6 for now, till
       SMD bridge is fixed to change to SMD6 */
    QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_A2P1, QMI_DEV_A2P1 );
     
    /* A2 processor port#2 */
    QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_A2P2, QMI_DEV_A2P2 );
     
     
  #else /* FEATURE_DATA_A2 */
       
    /* USB port#1 */
    QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_USB1, QMI_DEV_USB1 );
    
    /* USB port#2 */
    QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_USB2, QMI_DEV_USB2 );
      
    /* USB port#3 */
    QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_USB3, QMI_DEV_USB3 );
    /* USB port#4 */
    QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_USB4, QMI_DEV_USB4 );
    /* USB port#5 */
    QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_USB5, QMI_DEV_USB5 );

  #endif /* FEATURE_DATA_A2 */
  #endif /* FEATURE_DATA_RM_NET_USES_SM */

#endif 

  list_ptr->num_ports = index;

  return index;
} /* qmuxi_set_default_port_configuration() */

/*===========================================================================
  FUNCTION QMUXI_SET_MUX_PORT_CONFIGURATION

  DESCRIPTION
    This function assigns the mux port configuraiton table based on
    NV EFS (QMI_NV_ITEM_MUX_PORT_CFG) value.
  
  PARAMETERS  
    list_ptr   -  Pointer to list of port configurations
  
  DEPENDENCIES
    None

  RETURN VALUE
    Number of ports in configuraiton table

  SIDE EFFECTS
    None
===========================================================================*/
static uint8 qmuxi_set_mux_port_configuration
(
  qmi_port_list_type  * list_ptr
)
{
  uint8 index = 0;
#if defined(FEATURE_RMNET_PORT_CONFIG_MDM) && defined(FEATURE_DATA_WLAN_MAPCON)
  ds_wlan_offload_config_enum_type offload_cfg = 
                                      DS_WLAN_OFFLOAD_CONFIG_DEFAULT;
  nv_stat_enum_type  status;
#endif /* defined(FEATURE_RMNET_PORT_CONFIG_MDM) && defined(FEATURE_DATA_WLAN_MAPCON) */
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  
  if (list_ptr == NULL)
  {
    LOG_MSG_ERROR_0("NV QMI port list pointer NULL!");
    ASSERT(0);
    return 0;
  }

  if (qmi_nv_read(QMI_NV_ITEM_MUX_PORT_CFG, 0,
                  &qmi_mux_port_cfg,
                  sizeof(qmi_mux_port_cfg)) != QMI_NV_STATUS_OK)
  {
    qmi_mux_port_cfg = QMI_MUX_PORT_CFG_DEFAULT;
  }

  LOG_MSG_INFO1_1("QMI mux port cfg %d", qmi_mux_port_cfg);

  switch(qmi_mux_port_cfg)
  {
    case QMI_MUX_PORT_CFG_DISABLED:
    case QMI_MUX_PORT_CFG_QMAP:
      break;

#ifdef FEATURE_RMNET_PORT_CONFIG_MDM
    case QMI_MUX_PORT_CFG_QMAP_MUX1:
      #ifdef FEATURE_DATA_WLAN_MAPCON
      offload_cfg = ds_wlan_get_wlan_offload_config_nv();
      LOG_MSG_INFO1_1("wlan_offload_config_nv set to %d", offload_cfg);
      #endif /* FEATURE_DATA_WLAN_MAPCON */

      /*-----------------------------------------------------------------------
        QMAP logical ports configuration:
          * 1 physical data channel for muxing, 8 muxed ports for embedded calls,
          * 1 non-muxed port for tethered calls, additional 9 muxed reversed ports.
          * Total 19.
      -----------------------------------------------------------------------*/
      QMUX_SET_DEFAULT_DEVICE_CONFIG(index, QMI_DEV_NONE, QMI_DEV_A2P2); // mux master
      QMUX_SET_DEFAULT_DEVICE_CONFIG(index, QMI_DEV_MUX_A2_CH_0, QMI_DEV_MUX_A2_CH_0);
      QMUX_SET_DEFAULT_DEVICE_CONFIG(index, QMI_DEV_MUX_A2_CH_1, QMI_DEV_MUX_A2_CH_1);
      QMUX_SET_DEFAULT_DEVICE_CONFIG(index, QMI_DEV_MUX_A2_CH_2, QMI_DEV_MUX_A2_CH_2);
      QMUX_SET_DEFAULT_DEVICE_CONFIG(index, QMI_DEV_MUX_A2_CH_3, QMI_DEV_MUX_A2_CH_3);
      QMUX_SET_DEFAULT_DEVICE_CONFIG(index, QMI_DEV_MUX_A2_CH_4, QMI_DEV_MUX_A2_CH_4);
      QMUX_SET_DEFAULT_DEVICE_CONFIG(index, QMI_DEV_MUX_A2_CH_5, QMI_DEV_MUX_A2_CH_5);
      QMUX_SET_DEFAULT_DEVICE_CONFIG(index, QMI_DEV_MUX_A2_CH_6, QMI_DEV_MUX_A2_CH_6);
      QMUX_SET_DEFAULT_DEVICE_CONFIG(index, QMI_DEV_MUX_A2_CH_7, QMI_DEV_MUX_A2_CH_7);
      QMUX_SET_DEFAULT_DEVICE_CONFIG(index, QMI_DEV_A2P1, QMI_DEV_A2P1); // tethered
      #ifdef FEATURE_DATA_WLAN_MAPCON
      if (DS_WLAN_OFFLOAD_CONFIG_DISABLED != offload_cfg )
      {
        QMUX_SET_DEFAULT_DEVICE_CONFIG(index, QMI_DEV_MUX_A2_REV_CH_0, QMI_DEV_MUX_A2_REV_CH_0);
        QMUX_SET_DEFAULT_DEVICE_CONFIG(index, QMI_DEV_MUX_A2_REV_CH_1, QMI_DEV_MUX_A2_REV_CH_1);
        QMUX_SET_DEFAULT_DEVICE_CONFIG(index, QMI_DEV_MUX_A2_REV_CH_2, QMI_DEV_MUX_A2_REV_CH_2);
        QMUX_SET_DEFAULT_DEVICE_CONFIG(index, QMI_DEV_MUX_A2_REV_CH_3, QMI_DEV_MUX_A2_REV_CH_3);
        QMUX_SET_DEFAULT_DEVICE_CONFIG(index, QMI_DEV_MUX_A2_REV_CH_4, QMI_DEV_MUX_A2_REV_CH_4);
        QMUX_SET_DEFAULT_DEVICE_CONFIG(index, QMI_DEV_MUX_A2_REV_CH_5, QMI_DEV_MUX_A2_REV_CH_5);
        QMUX_SET_DEFAULT_DEVICE_CONFIG(index, QMI_DEV_MUX_A2_REV_CH_6, QMI_DEV_MUX_A2_REV_CH_6);
        QMUX_SET_DEFAULT_DEVICE_CONFIG(index, QMI_DEV_MUX_A2_REV_CH_7, QMI_DEV_MUX_A2_REV_CH_7);
        QMUX_SET_DEFAULT_DEVICE_CONFIG(index, QMI_DEV_MUX_A2_REV_CH_8, QMI_DEV_MUX_A2_REV_CH_8);
      }
      #endif
      qmi_mux_master_sio_port = SIO_PORT_A2_RMNET_2; // Muxed port master is the 1st instance
      break;

    case QMI_MUX_PORT_CFG_QMAP_MUX2:
      QMUX_SET_DEFAULT_DEVICE_CONFIG(index, QMI_DEV_SMD9_TETHERED, QMI_DEV_A2P2); // mux master
      QMUX_SET_DEFAULT_DEVICE_CONFIG(index, QMI_DEV_SMD1_TETHERED, QMI_DEV_MUX_A2_CH_0);
      QMUX_SET_DEFAULT_DEVICE_CONFIG(index, QMI_DEV_SMD2_TETHERED, QMI_DEV_MUX_A2_CH_1);
      QMUX_SET_DEFAULT_DEVICE_CONFIG(index, QMI_DEV_SMD3_TETHERED, QMI_DEV_MUX_A2_CH_2);
      QMUX_SET_DEFAULT_DEVICE_CONFIG(index, QMI_DEV_SMD4_TETHERED, QMI_DEV_MUX_A2_CH_3);
      QMUX_SET_DEFAULT_DEVICE_CONFIG(index, QMI_DEV_SMD5_TETHERED, QMI_DEV_MUX_A2_CH_4);
      QMUX_SET_DEFAULT_DEVICE_CONFIG(index, QMI_DEV_SMD6_TETHERED, QMI_DEV_MUX_A2_CH_5);
      QMUX_SET_DEFAULT_DEVICE_CONFIG(index, QMI_DEV_SMD7_TETHERED, QMI_DEV_MUX_A2_CH_6);
      QMUX_SET_DEFAULT_DEVICE_CONFIG(index, QMI_DEV_SMD8_TETHERED, QMI_DEV_MUX_A2_CH_7);
      qmi_mux_master_sio_port = SIO_PORT_A2_RMNET_2;
      break;

#endif /* FEATURE_RMNET_PORT_CONFIG_MDM */

#if defined(FEATURE_RMNET_PORT_CONFIG_MDM) && defined(FEATURE_DATA_MBIM)
    case QMI_MUX_PORT_CFG_MBIM_MDM:
      /*-----------------------------------------------------------------------
        MBIM multi-PDN TN configuration
          * 1 master channel, 8 logical channels, 1 GPS channel 
      -----------------------------------------------------------------------*/
      QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_A2P1, QMI_DEV_A2P1);
      QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_NONE, QMI_DEV_MUX_A2_CH_0);
      QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_NONE, QMI_DEV_MUX_A2_CH_1);
      QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_NONE, QMI_DEV_MUX_A2_CH_2);
      QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_NONE, QMI_DEV_MUX_A2_CH_3);
      QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_NONE, QMI_DEV_MUX_A2_CH_4);
      QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_NONE, QMI_DEV_MUX_A2_CH_5);
      QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_NONE, QMI_DEV_MUX_A2_CH_6);
      QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_NONE, QMI_DEV_MUX_A2_CH_7);
      QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SMD2_TETHERED, QMI_DEV_NONE); // For GPS
      qmi_mux_master_sio_port = SIO_PORT_A2_RMNET_1;
      break;
#endif /* FEATURE_RMNET_PORT_CONFIG_MDM && FEATURE_DATA_MBIM */

#if defined(FEATURE_RMNET_PORT_CONFIG_MDM_LE)
    case QMI_MUX_PORT_CFG_MBIM_MDM_LE:
      /*-----------------------------------------------------------------------
        MBIM multi-PDN LE configuration (Total 21)
          * 8 embedded channels. 3 multi-rmnet, 1 tethered channel,
          * 1 GPS channel, 8 mPDN logical channels
      -----------------------------------------------------------------------*/
      QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_0_EMBEDDED, QMI_DEV_SDIO_MUX_A2_0_EMBEDDED);
      QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_1_EMBEDDED, QMI_DEV_SDIO_MUX_A2_1_EMBEDDED);
      QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_2_EMBEDDED, QMI_DEV_SDIO_MUX_A2_2_EMBEDDED);
      QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_3_EMBEDDED, QMI_DEV_SDIO_MUX_A2_3_EMBEDDED);
      QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_4_EMBEDDED, QMI_DEV_SDIO_MUX_A2_4_EMBEDDED);
      QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_5_EMBEDDED, QMI_DEV_SDIO_MUX_A2_5_EMBEDDED);
      QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_6_EMBEDDED, QMI_DEV_SDIO_MUX_A2_6_EMBEDDED);
      QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_7_EMBEDDED, QMI_DEV_SDIO_MUX_A2_7_EMBEDDED);
      QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_8_EMBEDDED, QMI_DEV_SDIO_MUX_A2_8_EMBEDDED);
      QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_9_EMBEDDED, QMI_DEV_SDIO_MUX_A2_9_EMBEDDED);
      QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SDIO_MUX_A2_10_EMBEDDED, QMI_DEV_SDIO_MUX_A2_10_EMBEDDED);
      QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_A2P1, QMI_DEV_A2P1);
      QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_SMD2_TETHERED, QMI_DEV_NONE); // For GPS
      QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_NONE, QMI_DEV_MUX_A2_CH_0);
      QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_NONE, QMI_DEV_MUX_A2_CH_1);
      QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_NONE, QMI_DEV_MUX_A2_CH_2);
      QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_NONE, QMI_DEV_MUX_A2_CH_3);
      QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_NONE, QMI_DEV_MUX_A2_CH_4);
      QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_NONE, QMI_DEV_MUX_A2_CH_5);
      QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_NONE, QMI_DEV_MUX_A2_CH_6);
      QMUX_SET_DEFAULT_DEVICE_CONFIG( index, QMI_DEV_NONE, QMI_DEV_MUX_A2_CH_7);
      qmi_mux_master_sio_port = SIO_PORT_A2_RMNET_1; // master (physical) channel is on the 12th instance
      break;
#endif /* FEATURE_RMNET_PORT_CONFIG_MDM_LE */

    default:
      LOG_MSG_ERROR_1("Unknown port cfg %d", qmi_mux_port_cfg);
      qmi_mux_port_cfg = QMI_MUX_PORT_CFG_DEFAULT;
      break;
  }

  list_ptr->num_ports = index;
  return index;

} /* qmuxi_set_mux_port_configuration() */


/*===========================================================================
  FUNCTION QMUXI_GET_PORT_CONFIGURATION_FROM_NV

  DESCRIPTION
    This function gets the port configuraiton table from NV RAM.
  
  PARAMETERS  
    None
  
  DEPENDENCIES
    None

  RETURN VALUE
    TRUE if read operation successful 
    FALSE if error occurs

  SIDE EFFECTS
    None
===========================================================================*/
boolean qmuxi_get_port_configuration_from_nv
(
  uint8   *count_ptr
)
{ 
#ifndef FEATURE_DATA_QMI_SUPPRESS_NV  
  nv_stat_enum_type        nv_status = NV_NOTACTIVE_S;
  nv_item_type            *qmi_nv_item_ptr;
  nv_qmi_port_config_type *list_ptr = NULL;
  uint8                    j = 0;
#endif
  uint8                    i = 0;
  qmi_port_list_type       qmi_port_list;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

#define ASSIGN_NV_INFO( dst, src)                                    \
    (dst).qmi_port     = (src).qmi_port;                             \
    (dst).data_port    = (src).data_port;                            \
    (dst).qmi_stream   = (qmux_device_stream_type)(src).qmi_stream;  \
    (dst).frame_mode   = (qmux_frame_mode_type)(src).frame_mode;     

  memset(&qmi_port_list, 0, sizeof(qmi_port_list));

  /*-----------------------------------------------------------------------
    Check if we are using mux port configuration
  -----------------------------------------------------------------------*/
  if (qmuxi_set_mux_port_configuration(&qmi_port_list) > 0)
  {
    goto preserve_qmux_ports;
  }

#ifndef FEATURE_DATA_QMI_SUPPRESS_NV  

  ASSERT( NULL != count_ptr );

  /* check that count_ptr is not NULL in case ASSERT is removed */
  if (count_ptr == NULL)
  {
    LOG_MSG_ERROR_0("Count pointer NULL!");
    return FALSE;
  }

  /*-------------------------------------------------------------------------
    Allocate temporary memory for the NV item
  -------------------------------------------------------------------------*/
  qmi_nv_item_ptr = (nv_item_type *)
    modem_mem_alloc(sizeof(nv_item_type), MODEM_MEM_CLIENT_QMI_CRIT);
  if( qmi_nv_item_ptr == NULL )
  {
    LOG_MSG_ERROR_0("Mem alloc from system heap failed." );
    ASSERT(0);
    return FALSE;
  }
  
  /*-----------------------------------------------------------------------
    Query NVRAM for QMI port configuration table.
  -----------------------------------------------------------------------*/
  nv_status = qmi_get_nv_item( NV_QMI_PORT_LIST_I, qmi_nv_item_ptr );

  /* Check status or NV query */
  if( NV_DONE_S != nv_status )
  {
    /*---------------------------------------------------------------------
      Writeback default configuration table if NVRAM unassigned.
    ---------------------------------------------------------------------*/
    if( NV_NOTACTIVE_S != nv_status )
    {
      /* Some other error occurred */
      LOG_MSG_ERROR_1 ("Error reading config table from NVRAM: %d", nv_status);
    }

    (void)qmuxi_set_default_port_configuration( &qmi_port_list );
  }
  else
  {
    /*---------------------------------------------------------------------
      Validate input configuration parmeters
    ---------------------------------------------------------------------*/
    for( i=0; i< qmi_nv_item_ptr->qmi_port_list.num_ports; i++ )
    {
      list_ptr = &qmi_nv_item_ptr->qmi_port_list.port_list[i];
      
      /*  Check frame mode is valid  */
      if( !IS_FRAME_MODE_VALID( list_ptr->frame_mode ) )
      {
        LOG_MSG_ERROR_2 ("Invalid QMI frame_mode[%d]: %d",
                         i, list_ptr->frame_mode);
        modem_mem_free(qmi_nv_item_ptr, MODEM_MEM_CLIENT_QMI_CRIT);
        return FALSE;
      }

      /*  Check QMI device stream is valid  */
      if( !IS_DEVSTREAM_VALID( list_ptr->qmi_stream ) )
      {
        LOG_MSG_ERROR_2 ("Invalid QMI device stream[%d]: %d",
                         i, list_ptr->qmi_stream);
        modem_mem_free(qmi_nv_item_ptr, MODEM_MEM_CLIENT_QMI_CRIT);
        return FALSE;
      }

      /* Check the control and data ports are unique */
      for( j=(i+1);
           j < MIN( NV_QMI_PORT_LIST_MAX_SIZE,
                    qmi_nv_item_ptr->qmi_port_list.num_ports );
           j++ )
      {
        if( qmi_nv_item_ptr->qmi_port_list.port_list[i].qmi_port ==
            qmi_nv_item_ptr->qmi_port_list.port_list[j].qmi_port )
        {
          LOG_MSG_ERROR_2 ("Duplicate QMI device specified: [%d]%d",
                           j,
                           qmi_nv_item_ptr->qmi_port_list.port_list[j].qmi_port);
          modem_mem_free(qmi_nv_item_ptr, MODEM_MEM_CLIENT_QMI_CRIT);

          return FALSE;
        }
        if( (qmi_nv_item_ptr->qmi_port_list.port_list[i].data_port ==
             qmi_nv_item_ptr->qmi_port_list.port_list[j].data_port) &&
	    (QMI_DEV_NONE !=
	     (qmux_device_id_type)qmi_nv_item_ptr->qmi_port_list.port_list[j].data_port) )
        {
          LOG_MSG_ERROR_2 ("Duplicate RMNET device specified: [%d]%d",
                           j,
                           qmi_nv_item_ptr->qmi_port_list.port_list[j].data_port);
          modem_mem_free(qmi_nv_item_ptr, MODEM_MEM_CLIENT_QMI_CRIT);

          return FALSE;
        }
      }

      // Copy from qmi_nv_item_ptr to qmi_port_list
      if ( (i < QMI_PORT_LIST_MAX_SIZE) && (i < NV_QMI_PORT_LIST_MAX_SIZE) )
      {
        qmi_port_list.num_ports++;
        qmi_port_list.port_list[i] = qmi_nv_item_ptr->qmi_port_list.port_list[i];
      }
    }
  }
  
  modem_mem_free(qmi_nv_item_ptr, MODEM_MEM_CLIENT_QMI_CRIT);
#else
  /* Temporary block to handle case when NV updates not avaialble. */
  {
    (void)qmuxi_set_default_port_configuration( &qmi_port_list );
  }
#endif /* FEATURE_DATA_QMI_SUPPRESS_NV */
    
preserve_qmux_ports:
  /*-----------------------------------------------------------------------
    Preserve configuration table in local state.
  -----------------------------------------------------------------------*/
  qmux_num_instances = MIN( QMI_INSTANCE_MAX, qmi_port_list.num_ports );

  for( i=0; i < qmux_num_instances; i++ )
  {
    ASSIGN_NV_INFO( qmux_state[ i ].io.port_info, 
                    qmi_port_list.port_list[ i ] );
  }
  
  *count_ptr = qmux_num_instances;
  return TRUE;
} /* qmuxi_get_port_configuration_from_nv() */



/*===========================================================================
  FUNCTION QMUXI_FIND_INSTANCE_FROM_PORT

  DESCRIPTION
    Locate QMUX instance based on SIO port ID.

  PARAMETERS
    port_id    - SIO port identifier
    inst_ptr  - Pointer to QMUX instance; output

  RETURN VALUE
    TRUE on successful lookup
    FALSE otherwise

  SIDE EFFECTS
    none
===========================================================================*/
boolean qmuxi_find_instance_from_port
(
  uint16               port_id,
  qmi_instance_e_type *inst_ptr
)
{
  uint8 i;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT( NULL != inst_ptr );

  /* check that count_ptr is not NULL in case ASSERT is removed */
  if (inst_ptr == NULL)
  {
    LOG_MSG_ERROR_0("Instance pointer NULL!");
    return FALSE;
  }

  LOG_MSG_INFO3_1("qmuxi_find_instance_from_port: port=0x%x", port_id);

  /*-----------------------------------------------------------------------
    Determine QMI instance from SIO port
  -----------------------------------------------------------------------*/
  for( i=0; i<QMI_INSTANCE_MAX; i++ )
  {
    if( (qmux_state[i].io.port_info.qmi_port == port_id) ||
        (qmux_state[i].io.port_info.data_port == port_id) )
    {
      *inst_ptr = (qmi_instance_e_type)i;
      return TRUE;
    }
  }

  return FALSE;
} /* qmuxi_find_instance_from_port() */


/*===========================================================================
  FUNCTION QMUXI_RDM_WAIT_TIMER_CB

  DESCRIPTION
    QMUX timer callback used to delay port open until RDM ready.

  PARAMETERS
    data    - context data

  RETURN VALUE
    void

  SIDE EFFECTS
    none
===========================================================================*/
static void qmuxi_rdm_wait_timer_cb
(
  timer_cb_data_type data
)
{
  qmi_cmd_data_buf_type  *cmd_ptr = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_INFO3_0("qmuxi_rdm_wait_timer_cb");

  if( (timer_cb_data_type)qmux_state != data )
  {
    LOG_MSG_ERROR_1("RDM wait timer callback invoked incorrectly! 0x%x",
                    	 data);
    return;
  }
  
  /*---------------------------------------------------------------------
    Requeue RDM assign port command
  ---------------------------------------------------------------------*/
  cmd_ptr = qmi_get_cmd_data_buf();
  if( cmd_ptr != NULL )
  {
    /* Set message payload to indicate RDM open retry. */
    cmd_ptr->cmd.qmi.id = QMI_CMD_QMUX_PORT_ASSIGN;
    cmd_ptr->cmd.qmi.data.qmux_assign.device = RDM_DEV_MAX;
    cmd_ptr->cmd.qmi.data.qmux_assign.status = RDM_DEVMAP_BUSY_S;

    LOG_MSG_INFO2_2( "Posting QMI_CMD_QMUX_PORT_ASSIGN for port 0x%X %d",
                     	cmd_ptr->cmd.qmi.data.qmux_assign.device,
                     	 cmd_ptr->cmd.qmi.data.qmux_assign.status );
    qmi_send_cmd( QMI_CMD, cmd_ptr );
  }
  else
  {
    LOG_MSG_ERROR_1( "Could not get cmd buffer from QMI task for cmd id %d",
                     	 QMI_CMD_QMUX_PORT_ASSIGN );
    ASSERT(0);
    return;
  }
  
} /* qmuxi_rdm_wait_timer_cb() */


/*===========================================================================
  FUNCTION QMUXI_RDM_ASSIGN_CMD_CB

  DESCRIPTION
    QMUX assign service event handler (runs in QMI task).

  PARAMETERS
    status     - assign port status
    serviuce   - requesting service
    device     - device; may be RDM_NULL_DEV for closeure

  RETURN VALUE
    void

  SIDE EFFECTS
    none
===========================================================================*/
static void qmuxi_rdm_assign_port_cb
(
  rdm_assign_status_type status,
  rdm_service_enum_type  service,
  rdm_device_enum_type   device
)
{
  qmi_cmd_data_buf_type *cmd_ptr = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_INFO3_0("qmuxi_rdm_assign_port_cb");

  if( RDM_DATA_WWAN_SRVC != service )
  {
    LOG_MSG_ERROR_1("RDM callback for non WWAN service! %d", service);
    return;
  }
  
  /*-------------------------------------------------------------------------
    Post message to host task to begin assigning RDM devices to WWAN
    service., which triggers port openning Do this asynchronously to
    give RDM time to complete initialization during powerup phase.
  -------------------------------------------------------------------------*/
  cmd_ptr = qmi_get_cmd_data_buf();
  if( cmd_ptr != NULL )
  {
    /* Set message payload to indicate initial assignment */
    cmd_ptr->cmd.qmi.id = QMI_CMD_QMUX_PORT_ASSIGN_STATUS;
    cmd_ptr->cmd.qmi.data.qmux_assign.device   = device;
    cmd_ptr->cmd.qmi.data.qmux_assign.status   = status;

    LOG_MSG_INFO2_2( "Posting QMI_CMD_QMUX_PORT_ASSIGN_STATUS for port 0x%X %d",
                     	cmd_ptr->cmd.qmi.data.qmux_assign.device,
                     	 cmd_ptr->cmd.qmi.data.qmux_assign.status );
    qmi_send_cmd( QMI_CMD, cmd_ptr );
  }
  else
  {
    LOG_MSG_ERROR_1( "Could not get cmd buffer from QMI task for cmd id %d",
                     QMI_CMD_QMUX_PORT_ASSIGN );
    ASSERT(0);
    return;
  }

} /* qmuxi_rdm_assign_port_cb() */


/*===========================================================================
  FUNCTION QMUX_RDM_ASSIGN_STATUS_HDLR

  DESCRIPTION
    QMUX assign status event handler (runs in QMI task).
    
    This handler is used during powerup to process the status returned
    from Runtime Device Mapper for the assign port request.

  PARAMETERS
    qmi_device - QMI device to open
    data       - Context user data

  RETURN VALUE
    void

  SIDE EFFECTS
    none
===========================================================================*/
void  qmux_rdm_assign_status_hdlr
(
  rdm_device_enum_type   rdm_device,
  rdm_assign_status_type rdm_status
)
{
  qmi_cmd_data_buf_type  *cmd_ptr = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  
  LOG_MSG_INFO3_0("qmux_rdm_assign_status_hdlr");

  /*---------------------------------------------------------------------
    Check status of last RDM assign operation:
    DONE - Proceed to open next QMI instance
    DEVMAP_BUSY, APP_BUSY - Requeue open request for same instance
    NOT_ALLOWED, RESTRICTED - Hard error conditions
  ---------------------------------------------------------------------*/
  switch( rdm_status )
  {
    case RDM_DONE_S:
      LOG_MSG_INFO2_2( "RDM open done on port %d with %d retires",
                       qmux_rdm_open_info.instance,
                       qmux_rdm_open_info.retries );

      qmux_rdm_open_info.instance++;
      qmux_rdm_open_info.retries = 0;
      break;
    case RDM_APP_BUSY_S:
      LOG_MSG_INFO1_1( "RDM open can't be done on port %d",
                       qmux_rdm_open_info.instance );
 
      qmux_rdm_open_info.instance++;
      qmux_rdm_open_info.retries = 0;
      break;

    case RDM_DEVMAP_BUSY_S:
      /* Check for control value indicating open retry after delay
       * timer expired */
      if( RDM_DEV_MAX == rdm_device )
      {
        qmux_rdm_open_info.retries++;
        if( QMUX_MAX_RDM_OPEN_RETRIES <= qmux_rdm_open_info.retries )
        {
          LOG_MSG_ERROR_2( "Max open retries exceeded! [%d]%d",
                           qmux_rdm_open_info.instance,
                           qmux_rdm_open_info.retries );
          return;
        }

        LOG_MSG_INFO1_2( "Retry RDM open on current instance [%d]%d",
                         qmux_rdm_open_info.instance,
                         qmux_rdm_open_info.retries );
      }
      else
      {
        /* Start delay timer to trigger open retry */
        timer_set(&qmux_rdm_open_info.timer,
                  QMUX_RDM_OPEN_DELAY,
                  0,
                  T_MSEC);
        return;
      }
      break;

    case RDM_NOT_ALLOWED_S:
    case RDM_RESTRICTED_S:
      LOG_MSG_ERROR_3( "Restricted usage or Invalid port, app combination: "
                       "%d [%d] 0x%X",
                       rdm_status, qmux_rdm_open_info.instance, rdm_device );
      qmux_rdm_open_info.instance++;
      qmux_rdm_open_info.retries = 0;
      break;

    default:
      LOG_MSG_ERROR_1( "Unsupported RDM status code %d", rdm_status );
      ASSERT(0);
      break;
  }

  /* Check if all instances have been opened */
  if( qmux_num_instances <= qmux_rdm_open_info.instance )
  {
    LOG_MSG_INFO3_0("All instances have been tried to be opened!");
    return;
  }
     
  /*---------------------------------------------------------------------
    Enqueue RDM assign command to open next port
  ---------------------------------------------------------------------*/
  cmd_ptr = qmi_get_cmd_data_buf();
  if( cmd_ptr != NULL )
  {
    /* Set message payload. */
    cmd_ptr->cmd.qmi.id = QMI_CMD_QMUX_PORT_ASSIGN;
    cmd_ptr->cmd.qmi.data.qmux_assign.device = rdm_device;
    cmd_ptr->cmd.qmi.data.qmux_assign.status = rdm_status;

    LOG_MSG_INFO2_2( "Posting QMI_CMD_QMUX_PORT_ASSIGN for port 0x%X %d",
                     cmd_ptr->cmd.qmi.data.qmux_assign.device,
                     cmd_ptr->cmd.qmi.data.qmux_assign.status );
    qmi_send_cmd( QMI_CMD, cmd_ptr );
  }
  else
  {
    LOG_MSG_ERROR_1( "Could not get cmd buffer from QMI task for cmd id %d",
                     QMI_CMD_QMUX_PORT_ASSIGN );
    ASSERT(0);
  }
  
  return;
} /* qmux_rdm_assign_status_hdlr() */


/*===========================================================================
  FUNCTION QMUX_RDM_ASSIGN_CMD_HDLR

  DESCRIPTION
    QMUX assign service event handler (runs in QMI task).
    
    This handler is used during powerup to open the SIO ports mapped
    to QMI devices.  Currently RDM cannot support changing its device
    map until after it issues open commands.  RDM assign port API used
    to permit dynamic port assignment based on NVRAM configuration
    table.

  PARAMETERS
    qmi_device - QMI device to open
    data       - Context user data

  RETURN VALUE
    void

  SIDE EFFECTS
    none
===========================================================================*/
void  qmux_rdm_assign_cmd_hdlr
(
  rdm_device_enum_type   rdm_device,
  rdm_assign_status_type rdm_status
)
{
  qmux_state_type        *qmux_s = NULL;
  qmux_rdm_dev_map_type  *map_ptr = NULL;
  int16                   port_id;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  
  LOG_MSG_INFO3_0("qmux_rdm_assign_cmd_hdlr");
  (void)rdm_device;
  (void)rdm_status;
  
  /*-------------------------------------------------------------------------
    Determine the RDM device to open from QMI instance
  -------------------------------------------------------------------------*/
  qmux_s = &qmux_state[ qmux_rdm_open_info.instance ];

  port_id = (qmux_s->io.port_info.qmi_port == QMI_DEV_NONE) ?
          qmux_s->io.port_info.data_port : qmux_s->io.port_info.qmi_port; 

  if( !qmuxi_find_mapped_qmi_device( port_id, &map_ptr ) )
  {
    LOG_MSG_ERROR_1( "RDM map instance was not located!: qmi port=0x%X",
                     port_id );
    ASSERT(0);
    return;
  }
  
  /*-------------------------------------------------------------------------
    Request RDM to assign WWAN service to the specified QMI device.
    Callback will be invoked to report status of operation.
  -------------------------------------------------------------------------*/
  if( rdm_assign_port_tmp( RDM_DATA_WWAN_SRVC,
		       map_ptr->rdm_dev,
		       qmuxi_rdm_assign_port_cb ) )
  {
    /* Request was successful, outcome arrives later */
    LOG_MSG_INFO2_1("Requested RDM device assignment: rdm dev 0x%X",
                    map_ptr->rdm_dev);
  }
  else
  {
    /* Request failed, callback handles outcome */
    LOG_MSG_ERROR_2( "RDM assign port failed!: [%d] qmi port 0x%X",
                     qmux_rdm_open_info.instance, port_id );
  }

  return;
} /* qmux_rdm_assign_cmd_hdlr() */


/*===========================================================================
  FUNCTION QMUX_RDM_OPEN_CMD_HDLR

  DESCRIPTION
    QMUX open event handler (runs in QMI task)

  PARAMETERS
    sio_port   - SIO port ID
    data       - Context user data

  RETURN VALUE
    void

  SIDE EFFECTS
    none
===========================================================================*/
void  qmux_rdm_open_cmd_hdlr
(
  uint16    sio_port,
  void*     data
)
{
  qmux_state_type *  qmux_s = NULL;
  qmi_instance_e_type qmux_instance = QMI_INSTANCE_MAX;
  qmux_rdm_dev_map_type *map_ptr = NULL;
  boolean result = FALSE;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_INFO3_0("qmux_rdm_open_cmd_hdlr");

  /*-----------------------------------------------------------------------
    Determine QMI instance from SIO port
  -----------------------------------------------------------------------*/
  if( !qmuxi_find_mapped_sio_port( sio_port, &map_ptr ) )
  {
    LOG_MSG_ERROR_1( "RDM map instance was not located!: port=0x%X",
                     (uint16)sio_port );
    rdm_notify( RDM_DATA_WWAN_SRVC, RDM_APP_BUSY_S );
    return;
  }
  
  if( map_ptr && !qmuxi_find_instance_from_port( (uint16)map_ptr->qmi_dev,
                                                 &qmux_instance ) )
  {
    LOG_MSG_ERROR_1( "QMUX instance was not located!: port=0x%X", sio_port );
    /* Post BUSY status to RDM incase not QMI instance mapped to port. */
    rdm_notify( RDM_DATA_WWAN_SRVC, RDM_APP_BUSY_S );
    return;
  }

  if( QMI_INSTANCE_MAX == qmux_instance )
  {
    LOG_MSG_ERROR_0( "Invalid QMUX instance!");
    rdm_notify( RDM_DATA_WWAN_SRVC, RDM_APP_BUSY_S );
    return;
  }
  
  qmux_s = &qmux_state[qmux_instance];

  /*---------------------------------------------------------------------
      Open SIO port for the specified QMI instance
  ---------------------------------------------------------------------*/
  if( qmux_s )
  {
    if( qmux_s->initialized == FALSE ||
        qmux_s->io.ports_active == TRUE )
    {
      LOG_MSG_ERROR_1( "QMUX instance was not initialized or was already active!: %d",
                       qmux_instance );
      rdm_notify( RDM_DATA_WWAN_SRVC, RDM_APP_BUSY_S );
      return;
    }

    /*---------------------------------------------------------------------
      Open stream on SIO port for data via associated RmNET instance.
      Do this first to maintain legacy order of operations.
    ---------------------------------------------------------------------*/
    if( QMI_DEV_NONE != (qmux_device_id_type)qmux_s->io.port_info.data_port )
    {
      /* Check for different data device */
      if( qmux_s->io.port_info.data_port != qmux_s->io.port_info.qmi_port )
      {
        /* Find mapped SIO port for data device */
        if( !qmuxi_find_mapped_qmi_device( qmux_s->io.port_info.data_port,
                                           &map_ptr ) )
        {
          LOG_MSG_ERROR_1( "RDM map instance was not located!: port=0x%X",
                           sio_port );
          rdm_notify( RDM_DATA_WWAN_SRVC, RDM_APP_BUSY_S );
          return;
        }
      }

      /* Open the Rev IP transport or RmNET instance on SIO port */

#ifdef FEATURE_DATA_WLAN_MAPCON
      if (IS_REVERSE_PORT(map_ptr->sio_port))
      {
        result = rev_ip_transport_open_instance(GET_REV_IP_TRANSPORT_INSTANCE_FROM_QMUX_INSTANCE(qmux_instance),
                                           map_ptr->sio_port);
      }
      else
      {
#endif /* FEATURE_DATA_WLAN_MAPCON */
        result = rmnet_meta_sm_open_dual_ip_instance( QMI_IF_GET_RMNET_INSTANCE(qmux_instance,1),
                                               map_ptr->sio_port );
#ifdef FEATURE_DATA_WLAN_MAPCON
      }
#endif /* FEATURE_DATA_WLAN_MAPCON */

      if( FALSE == result)
      {
        LOG_MSG_ERROR_2( "Rev IP transport or RMNET instance open failed!: %d, SIO port %d",
                         qmux_instance, map_ptr->sio_port );
        rdm_notify( RDM_DATA_WWAN_SRVC, RDM_APP_BUSY_S );
        return;
      }
    }
    else
    {
      qmux_process_rmnet_inst_open_close_result(TRUE, 
                                            	QMI_CMD_RMNET_OPEN_INSTANCE,
                                            	qmux_instance,
                                            	sio_port);
    }
  }
  
} /* qmux_rdm_open_cmd_hdlr() */


/*===========================================================================
  FUNCTION QMUX_RDM_CLOSE_CMD_HDLR

  DESCRIPTION
    QMUX close event handler (runs in QMI task)

  PARAMETERS
    sio_port   - SIO port ID
    data       - Context user data

  RETURN VALUE
    void

  SIDE EFFECTS
    none
===========================================================================*/
void  qmux_rdm_close_cmd_hdlr
(
  uint16    sio_port,
  void*     data
)
{
  qmux_state_type *  qmux_s = NULL;
  qmi_instance_e_type qmux_instance  = QMI_INSTANCE_MAX;
  qmux_rdm_dev_map_type *map_ptr = NULL;
  boolean result = FALSE;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_INFO3_0("qmux_rdm_close_cmd_hdlr");

  /*-----------------------------------------------------------------------
    Determine QMI instance from SIO port
  -----------------------------------------------------------------------*/
  if( !qmuxi_find_mapped_sio_port( sio_port, &map_ptr ) )
  {
    LOG_MSG_ERROR_1( "RDM map instance was not located!: port=0x%X",
                     (uint16)sio_port );
    ASSERT(0);
    return;
  }
  
  if( map_ptr && !qmuxi_find_instance_from_port( map_ptr->qmi_dev,
                                                 &qmux_instance ) )
  {
    LOG_MSG_ERROR_1( "QMUX instance was not located!: port=0x%X", sio_port );
    ASSERT(0);
    return;
  }

  if( QMI_INSTANCE_MAX == qmux_instance )
  {
    LOG_MSG_ERROR_0( "Invalid QMUX instance!");
    rdm_notify( RDM_DATA_WWAN_SRVC, RDM_APP_BUSY_S );
    return;
  }

  qmux_s = &qmux_state[qmux_instance];

  /*-----------------------------------------------------------------------
    Close port for the specified QMI instance
  -----------------------------------------------------------------------*/
  if( qmux_s )
  {
    if( qmux_s->initialized == FALSE ||
        qmux_s->io.ports_active == FALSE )
    {
      LOG_MSG_ERROR_1( "QMUX instance was not initialized or was inactive!: %d",
                       qmux_instance );
      rdm_notify( RDM_DATA_WWAN_SRVC, RDM_APP_BUSY_S );
      return;
    }
   
    if (qmux_s->io.port_info.qmi_port != QMI_DEV_NONE)
    {
      if( !qmuxi_close_instance( qmux_instance ) )
      {
        LOG_MSG_ERROR_1( "QMUX instance close failed!: %d", qmux_instance );
        rdm_notify( RDM_DATA_WWAN_SRVC, RDM_APP_BUSY_S );
        return;
      }
    }

    /* Close the Reverse SIO or RmNET instance on SIO port */
    if (qmux_s->io.port_info.data_port != QMI_DEV_NONE)
    {
#ifdef FEATURE_DATA_WLAN_MAPCON
      if (IS_REVERSE_PORT(map_ptr->sio_port))
      {
        result = rev_ip_transport_close_instance(GET_REV_IP_TRANSPORT_INSTANCE_FROM_QMUX_INSTANCE(qmux_instance));
      }
      else
      {
#endif /* FEATURE_DATA_WLAN_MAPCON */
        result = rmnet_meta_sm_close_instance( QMI_IF_GET_RMNET_INSTANCE(qmux_instance,1) );
#ifdef FEATURE_DATA_WLAN_MAPCON
      }
#endif /* FEATURE_DATA_WLAN_MAPCON*/

      if( FALSE == result)
      {
        LOG_MSG_ERROR_1( "RMNET instance close failed!: %d", qmux_instance );
        rdm_notify( RDM_DATA_WWAN_SRVC, RDM_APP_BUSY_S );
        return;
      }
    }
    else
    {
      qmux_process_rmnet_inst_open_close_result(TRUE, 
                                            	QMI_CMD_RMNET_CLOSE_INSTANCE,
                                            	qmux_instance,
                                            	sio_port);
    }
  }
  
} /* qmux_rdm_close_cmd_hdlr() */


/*===========================================================================
  FUNCTION QMUXI_RDM_OPEN_PORT_CB
           QMUXI_RDM_CLOSE_PORT_CB

  DESCRIPTION
    These functions are registered with Runtime Device Mapper to
    receive notice to open/close specified SIO port.
  
  PARAMETERS  
    port        - SIO port to open
    data        - Client context data
    
  DEPENDENCIES
    qmux_init() must have been called on 'qmi_instance' previously

  RETURN VALUE
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmuxi_rdm_open_port_cb
(
  sio_port_id_type      port,
  void *                data
)
{
  qmi_cmd_data_buf_type *cmd_ptr;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  
  LOG_MSG_INFO3_1( "RDM Open CB: port=%d", port );

  /*-----------------------------------------------------------------------
    Check for case were port is SIO_PORT_NULL, which will be ignored.
    This can happen when QMI registers a device which may be
    dynamically switched to WWAN service after powerup, such as USB
    driver. The initial SIO port will be SIO_PORT_NULL in this case.
    The RDM will send another open later with the actual SIO port to
    be used.
  -----------------------------------------------------------------------*/
  if( SIO_PORT_NULL == port )
  {
    LOG_MSG_INFO2_0("Ignoring RDM open for NULL port");
    rdm_notify( RDM_DATA_WWAN_SRVC, RDM_DONE_S );
    return;
  }
  
  /*-----------------------------------------------------------------------
    Post commnd to host task for execution in its context.
  -----------------------------------------------------------------------*/
  cmd_ptr = qmi_get_cmd_data_buf();
  if( cmd_ptr != NULL )
  {
    cmd_ptr->cmd.qmi.id = QMI_CMD_QMUX_PORT_OPEN;
    cmd_ptr->cmd.qmi.data.qmux_port.sio_port = port;
    cmd_ptr->cmd.qmi.data.qmux_port.data     = data;

    qmi_send_cmd( QMI_CMD, cmd_ptr );
  }
  else
  {
    LOG_MSG_ERROR_1( "Could not get cmd buffer from QMI task for cmd id %d",
                     QMI_CMD_QMUX_PORT_OPEN );
    ASSERT(0);
    rdm_notify( RDM_DATA_WWAN_SRVC, RDM_APP_BUSY_S );
  }
} /* qmuxi_rdm_open_port_cb() */

static void qmuxi_rdm_close_port_cb
(
  sio_port_id_type      port,
  void *                data
)
{
  qmi_cmd_data_buf_type *cmd_ptr;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_INFO3_1( "RDM Close CB: port=%d", port );

  /*-----------------------------------------------------------------------
    Post commnd to host task for execution in its context.
  -----------------------------------------------------------------------*/
  cmd_ptr = qmi_get_cmd_data_buf();
  if( cmd_ptr != NULL )
  {
    cmd_ptr->cmd.qmi.id = QMI_CMD_QMUX_PORT_CLOSE;
    cmd_ptr->cmd.qmi.data.qmux_port.sio_port = port;
    cmd_ptr->cmd.qmi.data.qmux_port.data     = data;

    qmi_send_cmd( QMI_CMD, cmd_ptr );
  }
  else
  {
    LOG_MSG_ERROR_1( "Could not get cmd buffer from QMI task for cmd id %d",
                     QMI_CMD_QMUX_PORT_CLOSE );
    ASSERT(0);
    rdm_notify( RDM_DATA_WWAN_SRVC, RDM_APP_BUSY_S );
  }
} /* qmuxi_rdm_close_port_cb() */


/*===========================================================================
  FUNCTION QMUXI_FIND_MAPPED_QMI_DEVICE

  DESCRIPTION
    This function finds the device map record for the specified QMI
    device ID.
  
  PARAMETERS  
    qmi_dev   - QMI device ID
    tbl_pptr  - Pointer to map table record
    
  DEPENDENCIES
    None

  RETURN VALUE
    TRUE if lookup operation successful 
    FALSE if error occurs

  SIDE EFFECTS
    None
===========================================================================*/
static boolean qmuxi_find_mapped_qmi_device
(
  uint16                  qmi_dev,
  qmux_rdm_dev_map_type **tbl_pptr
)
{
  uint8  i;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT( tbl_pptr );

  /* check that tbl_pptr is not NULL in case ASSERT is removed */
  if (tbl_pptr == NULL)
  {
    LOG_MSG_ERROR_0("Table pointer NULL!");
    return FALSE;
  }

  /*-------------------------------------------------------------------------
    Scan mapping table to specified device ID.
  -------------------------------------------------------------------------*/
  for(i=0; i<QMUX_RDM_DEV_MAP_TBL_SIZE; i++)
  {
    /* Check for match */
    if( (qmux_device_id_type)qmi_dev == qmux_rdm_dev_map_tbl[ i ].qmi_dev )
    {
      /* Return the table record to caller */
      *tbl_pptr = &qmux_rdm_dev_map_tbl[ i ];
      return TRUE;
    }
  }
  
  /*-------------------------------------------------------------------------
    Report no match found.
  -------------------------------------------------------------------------*/
  return FALSE;
} /* qmuxi_find_mapped_qmi_device() */

/*===========================================================================
  FUNCTION QMUXI_FIND_MAPPED_SIO_PORT

  DESCRIPTION
    This function finds the device map record for the specified SIO port.
  
  PARAMETERS  
    sio_port  - SIO port ID
    tbl_pptr  - Pointer to map table record
    
  DEPENDENCIES
    None

  RETURN VALUE
    TRUE if lookup operation successful 
    FALSE if error occurs

  SIDE EFFECTS
    None
===========================================================================*/
static boolean qmuxi_find_mapped_sio_port
(
  uint16                  sio_port,
  qmux_rdm_dev_map_type **tbl_pptr
)
{
  uint8  i;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT( tbl_pptr );

  /* check that tbl_pptr is not NULL in case ASSERT is removed */
  if (tbl_pptr == NULL)
  {
    LOG_MSG_ERROR_0("Table pointer NULL!");
    return FALSE;
  }

  /*-------------------------------------------------------------------------
    Scan mapping table to specified device ID.
  -------------------------------------------------------------------------*/
  for(i=0; i<QMUX_RDM_DEV_MAP_TBL_SIZE; i++)
  {
    /* Check for match */
    if( (sio_port_id_type)sio_port == qmux_rdm_dev_map_tbl[ i ].sio_port )
    {
      /* Return the table record to caller */
      *tbl_pptr = &qmux_rdm_dev_map_tbl[ i ];
      return TRUE;
    }
  }
  
  /*-------------------------------------------------------------------------
    Report no match found.
  -------------------------------------------------------------------------*/
  return FALSE;
} /* qmuxi_find_mapped_sio_port() */

/*===========================================================================
  FUNCTION QMUX_RMNET_INST_OPEN_CLOSE_RESULT()

  DESCRIPTION
    Notify QMUX about the specified Rmnet instances open/close result. The
    QMUX post a cmd to qmi_modem task to process it.
 

  PARAMETERS
    result      : Open/Close result
    cmd         : cmd(open or close)
    instance    : QMUX instance
    sio_port    : sio_port 

  RETURN VALUE
    None

  DEPENDENCIES
    QMI Data port(Rmnet or Rev IP transport) should open before control port.

  SIDE EFFECTS
    None
===========================================================================*/
void qmux_rmnet_inst_open_close_result(  
  boolean                                  result,
  qmi_cmd_id_e_type                        cmd,
  qmi_instance_e_type                      instance,
  uint16                                   sio_port
)
{
  qmi_cmd_data_buf_type *cmd_ptr;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_INFO2_3("QMUX receive qmi instance:%d open close_result:%d sio_port:%d",
                  instance, result, sio_port);

  /*-----------------------------------------------------------------------
    Post commnd to host task for execution in its context.
  -----------------------------------------------------------------------*/
  cmd_ptr = qmi_get_cmd_data_buf();
  if( cmd_ptr != NULL )
  {
    cmd_ptr->cmd.qmi.data.rmnet_inst.result     = result;
    cmd_ptr->cmd.qmi.id = cmd;
    cmd_ptr->cmd.qmi.data.rmnet_inst.instance   = instance;
    cmd_ptr->cmd.qmi.data.rmnet_inst.sio_port   = sio_port;
    qmi_send_cmd( QMI_CMD, cmd_ptr );
  }
  else
  {
    LOG_MSG_ERROR_1( "Could not get cmd buffer from QMI modem task for cmd id %d",
                     cmd );
    ASSERT(0);
    rdm_notify( RDM_DATA_WWAN_SRVC, RDM_APP_BUSY_S );
  }
} /* qmux_rmnet_inst_open_close_result() */

/*===========================================================================
  FUNCTION QMUX_PROCESS_RMNET_INST_OPEN_CLOSE_RESULT()

  DESCRIPTION
    This function processes RmNet (data) port open/close result in qmi modem
    task and then proceeds with the control port operation.
    
  PARAMETERS
    result      : Open/Close result
    cmd         : cmd(open or close)
    instance    : Rmnet instance
    sio_port    : sio_port 

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void qmux_process_rmnet_inst_open_close_result
(  
  boolean                                  result,
  qmi_cmd_id_e_type                        cmd,
  qmi_instance_e_type                      qmux_instance,
  uint16                                   sio_port
)
{
  qmux_state_type *   qmux_s = NULL;
  qmux_rdm_dev_map_type *map_ptr = NULL;
  uint16                 sio_control_port;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_INFO2_3("QMUX process qmux instance:%d result:%d sio_port:%d",
                  qmux_instance, result, sio_port);

  sio_control_port = sio_port;

  if(!result)
  {
    LOG_MSG_ERROR_0( "RMNET instance open/close failed!: " );
    rdm_notify( RDM_DATA_WWAN_SRVC, RDM_APP_BUSY_S );
    return;
  }

  if (qmux_instance >= QMI_INSTANCE_MAX) 
  {
    LOG_MSG_ERROR_0( "Invalid QMUX instance!");
    rdm_notify( RDM_DATA_WWAN_SRVC, RDM_APP_BUSY_S );
    return;
  }

  qmux_s = &qmux_state[qmux_instance];

  if( qmux_s )
  {
    if( qmux_s->initialized == FALSE ||
        qmux_s->io.ports_active == TRUE )
    {
      LOG_MSG_ERROR_1( "QMUX instance was not initialized or was already active!: %d",
                       qmux_instance );
      rdm_notify( RDM_DATA_WWAN_SRVC, RDM_APP_BUSY_S );
      return;
    }
    /*---------------------------------------------------------------------
        Open SIO port for the specified QMI instance
    ---------------------------------------------------------------------*/
    if(cmd == QMI_CMD_RMNET_OPEN_INSTANCE)
    {

      /*---------------------------------------------------------------------
        Find a SIO port for control 
      ---------------------------------------------------------------------*/
      if( QMI_DEV_NONE != (qmux_device_id_type)qmux_s->io.port_info.qmi_port )
      {
        /* Check for different data device */
        if( qmux_s->io.port_info.data_port != qmux_s->io.port_info.qmi_port )
        {
          /* Find mapped SIO port for control device */
          if( !qmuxi_find_mapped_qmi_device( qmux_s->io.port_info.qmi_port,
                                             &map_ptr ) )
          {
            LOG_MSG_ERROR_1( "RDM map instance was not located!: port=0x%X",
                             sio_port );
            rdm_notify( RDM_DATA_WWAN_SRVC, RDM_APP_BUSY_S );
            return;
          }
          /* Assign the correct SIO port for control port */
          sio_control_port = map_ptr->sio_port;
        }

        /*---------------------------------------------------------------------
          Open stream on SIO port for control.
        ---------------------------------------------------------------------*/
        if( !qmuxi_open_instance( qmux_instance,
                                  sio_control_port ) )
        {
          LOG_MSG_ERROR_1( "QMUX instance open failed!: %d", qmux_instance );
          rdm_notify( RDM_DATA_WWAN_SRVC, RDM_APP_BUSY_S );
          return;
        }
      }
  
      /* Notify RDM of successful completion. */
      rdm_notify( RDM_DATA_WWAN_SRVC, RDM_DONE_S );
      qmux_s->io.ports_active = TRUE;
  
      /*---------------------------------------------------------------------
        Generate SYNC indication for now opened port. 
      ---------------------------------------------------------------------*/
      if (qmux_s->io.port_info.qmi_port != QMI_DEV_NONE)
      {
        qmi_ctl_send_sync( qmux_instance );
      }
    }
    /*---------------------------------------------------------------------
        Close SIO port for the specified QMI instance
    ---------------------------------------------------------------------*/
    else if(cmd == QMI_CMD_RMNET_CLOSE_INSTANCE)
    {
      /* Notify RDM of successful completion. */
      rdm_notify( RDM_DATA_WWAN_SRVC, RDM_DONE_S );
      qmux_s->io.ports_active = FALSE;
    }
    else
    {
      LOG_MSG_ERROR_1( "QMUX recv unexpected cmd: %d", cmd );
    }
  }
} /* qmux_process_rmnet_inst_open_close_result() */


/*===========================================================================
  FUNCTION QMUXI_REGISTER_DEVICES_WITH_RDM

  DESCRIPTION
    This function registers the configured ports with SIO Runtime
    Device Mapper (RDM)
  
  PARAMETERS  
    None
  
  DEPENDENCIES
    None

  RETURN VALUE
    TRUE if read operation successful 
    FALSE if error occurs

  SIDE EFFECTS
    None
===========================================================================*/
boolean qmuxi_register_devices_with_rdm (void)
{ 
  rdm_service_params_type svc_reg_info;
  qmi_cmd_data_buf_type *cmd_ptr = NULL;
  uint8 i;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_INFO2_0("Registering ports with RDM");
  
  /*-------------------------------------------------------------------------
    Register service for multiple devices.
  -------------------------------------------------------------------------*/
  memset((void*)&svc_reg_info, 0x0, sizeof(svc_reg_info));
  svc_reg_info.device_support = RDM_MULTIPLE_DEV_SUPPORT;
  svc_reg_info.open_multi_func = qmuxi_rdm_open_port_cb;
  svc_reg_info.close_multi_func = qmuxi_rdm_close_port_cb;
  svc_reg_info.data = 0;
  
  rdm_register_service( RDM_DATA_WWAN_SRVC, &svc_reg_info );
    
  /*-------------------------------------------------------------------------
    Register set of devices (ports) for the WWAN service.  All
    possible devices are registered such that the subset of QMI
    devices specified in NVRAM may be dynamically opened once RDM has
    completed initialization.  Prior to this, only those devices
    statically assigned to WWAN service may be opened at powerup. This
    RDM constraint should be removed in later releases.
  -------------------------------------------------------------------------*/
  for( i=0; i<QMUX_RDM_DEV_MAP_TBL_SIZE; i++ )
  {
    rdm_set_compatibility( RDM_DATA_WWAN_SRVC,
			   qmux_rdm_dev_map_tbl[i].rdm_dev );
  }

  /*-------------------------------------------------------------------------
    Allocate timer for waiting on RDM open
    Make timer non-deferrable to prevent Q6 from Power Collpase
  -------------------------------------------------------------------------*/ 
  memset( (void*)&qmux_rdm_open_info, 0x0, sizeof(qmux_rdm_open_info) );
  qmux_rdm_open_info.instance = QMI_INSTANCE_MIN;

  timer_group_set_deferrable(&qmux_rdm_open_info.timer_group, FALSE);
  timer_def(&qmux_rdm_open_info.timer,
            &qmux_rdm_open_info.timer_group,
            NULL,
            0,
            qmuxi_rdm_wait_timer_cb,
            (timer_cb_data_type)qmux_state);

  /*-------------------------------------------------------------------------
    Post message to host task to begin assigning RDM devices to WWAN
    service, which triggers port opening. Do this asynchronously to
    give RDM time to complete initialization during powerup phase.
  -------------------------------------------------------------------------*/
  cmd_ptr = qmi_get_cmd_data_buf();
  if( cmd_ptr != NULL )
  {
    /* Set message payload to indicate initial assignment */
    cmd_ptr->cmd.qmi.id = QMI_CMD_QMUX_PORT_ASSIGN;
    cmd_ptr->cmd.qmi.data.qmux_assign.device = RDM_NULL_DEV;
    cmd_ptr->cmd.qmi.data.qmux_assign.status = RDM_ASSIGN_STATUS_MAX_S;

    LOG_MSG_INFO2_0( "Posting QMI_CMD_QMUX_PORT_ASSIGN" );
    qmi_send_cmd( QMI_CMD, cmd_ptr );
  }
  else
  {
    LOG_MSG_ERROR_1( "Could not get cmd buffer from QMI task for cmd id %d",
                     QMI_CMD_QMUX_PORT_ASSIGN );
    ASSERT(0);
    return FALSE;
  }
  
  return TRUE;
} /* qmuxi_register_devices_with_rdm() */

#ifdef TEST_FRAMEWORK
#error code not present
#endif /* TEST_FRAMEWORK */

/*===========================================================================
  FUNCTION QMUXI_SIO_IOCTL

  DESCRIPTION
    This function calls sio ioctl function based on stream type.
  
  PARAMETERS  
    qmux_s:      qmux_state pointer
    cmd:         sio command
    ioctl_param: ioctl param pointer
  
  DEPENDENCIES
    None

  RETURN VALUE
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmuxi_sio_ioctl
(
  qmux_state_type       * qmux_s,
  sio_ioctl_cmd_type      cmd,
  sio_ioctl_param_type  * ioctl_param
)
{
  ASSERT ( (qmux_s != NULL) && (ioctl_param != NULL) );

  if (qmux_s->io.port_info.qmi_stream == QMUX_DEVSTREAM_CONTROL)
  {
    sio_control_ioctl(qmux_s->io.sio_handle, cmd, ioctl_param);
  }
  else
  {
    sio_ioctl(qmux_s->io.sio_handle, cmd, ioctl_param);
  }
} /* qmuxi_sio_ioctl() */

#ifdef FEATURE_DATA_QMI_DTR_ON_CTRL_CHANNEL
/*===========================================================================
  FUNCTION QMUXI_SET_DTR_HANDLER

  DESCRIPTION
    This function sets the DTR handler per the given qmi instance
  
  PARAMETERS  
    inst:   QMI instance
    dtr_cb: DTR callback function 
  
  DEPENDENCIES
    None

  RETURN VALUE
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmuxi_set_dtr_handler
(
  qmi_instance_e_type    inst,
  sio_vpu_func_ptr_type  dtr_cb
)
{
  qmux_state_type       *qmux_s;
  sio_ioctl_param_type  ioctl_param;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT( (inst >= QMI_INSTANCE_MIN) && (inst < QMI_INSTANCE_MAX) );
  qmux_s = &qmux_state[inst];

  memset(&ioctl_param, 0, sizeof(ioctl_param));
  ioctl_param.enable_dte_ready_event_ext.cb_func = dtr_cb;
  ioctl_param.enable_dte_ready_event_ext.cb_data = qmux_s;

  if (dtr_cb)
  {
    qmux_s->dtr_status = FALSE;
    qmuxi_sio_ioctl(qmux_s,
                SIO_IOCTL_ENABLE_DTR_EVENT_EXT,
                &ioctl_param);

    // DTR callback will be invoked immediately once registered with
    // SMD so there is no need to check DTR status here
  }
  else
  {
    qmuxi_sio_ioctl(qmux_s,
                SIO_IOCTL_DISABLE_DTR_EVENT_EXT,
                &ioctl_param);
  }

} /* qmuxi_set_dtr_handler() */

/*===========================================================================
  FUNCTION QMUXI_DTR_CB

  DESCRIPTION
    This is the QMI DTR CB function
  
  PARAMETERS  
    user_data:  QMUX state pointer
  
  DEPENDENCIES
    None

  RETURN VALUE
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmuxi_dtr_cb
(
  void    * user_data
)
{
  qmux_state_type       *qmux_s;
  qmi_instance_e_type   inst;
  sio_ioctl_param_type  ioctl_param;
  boolean               dtr_status = FALSE;
  qmi_cmd_data_buf_type *cmd_ptr = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(user_data);
  qmux_s = (qmux_state_type *) user_data;

  inst = qmi_instance_by_qmux_state(qmux_s);
  ASSERT( (inst >= QMI_INSTANCE_MIN) && (inst < QMI_INSTANCE_MAX) );

  memset(&ioctl_param, 0, sizeof(ioctl_param));
  ioctl_param.dte_ready_asserted = &dtr_status;

  qmuxi_sio_ioctl(qmux_s,
              SIO_IOCTL_DTE_READY_ASSERTED,
              &ioctl_param);

  QMI_ENTER_CRIT_SECTION(&global_qmi_crit_section);
  if (dtr_status == qmux_s->dtr_status)
  {
    QMI_LEAVE_CRIT_SECTION(&global_qmi_crit_section);
    LOG_MSG_INFO2_2("Ignore DTR [%d] on QMI inst [%d]", dtr_status, inst);
    return;
  }
  qmux_s->dtr_status = dtr_status;
  QMI_LEAVE_CRIT_SECTION(&global_qmi_crit_section);

  LOG_MSG_INFO2_2("DTR [%d] on QMI inst [%d]", dtr_status, inst);

  // Free any QMI messages still in RX wmk if DTR drops
  if (dtr_status == FALSE)
  {
    dsm_empty_queue(&qmux_s->io.rx_wm);
  }

  // Send DTR status to QMI modem task
  cmd_ptr = qmi_get_cmd_data_buf();
  if (cmd_ptr != NULL)
  {
    cmd_ptr->cmd.qmi.id = QMI_CMD_DTR_STATUS;
    cmd_ptr->cmd.qmi.data.dtr_status.qmi_inst = inst;
    cmd_ptr->cmd.qmi.data.dtr_status.asserted = dtr_status;
    qmi_send_cmd(QMI_CMD, cmd_ptr);
  }
  else
  {
    LOG_MSG_ERROR_0("Can't allocate QMI cmd buf");
    return;
  }

} /* qmuxi_dtr_cb() */
#endif /* FEATURE_DATA_QMI_DTR_ON_CTRL_CHANNEL */

/*===========================================================================
  FUNCTION QMI_INSTANCE_BY_QMUX_STATE()

  DESCRIPTION
    To obtain QMI instance from QMUX state. Qmux state passed in must 
    ALWAYS be the QMUX state address.

  PARAMETERS  
    qmux_s : QMUX state address

  RETURN VALUE
    QMI Instance if qmux_s is valid
    QMI_INSTANCE_MAX if qmux_s is invalid

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static qmi_instance_e_type  qmi_instance_by_qmux_state
(
  qmux_state_type * qmux_s
)
{
  int qmi_inst;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  qmi_inst = qmux_s - qmux_state;

  if ( (qmi_inst >= QMI_INSTANCE_MIN) && (qmi_inst < QMI_INSTANCE_MAX) )
  {
    return (qmi_instance_e_type)qmi_inst;
  }

  LOG_MSG_ERROR_2("Invalid qmux state [%p], inst [%d]", qmux_s, qmi_inst);

  return QMI_INSTANCE_MAX;

} /* qmi_instance_by_qmux_state() */

/*===========================================================================
  FUNCTION QMI_IF_GET_CMD_GET_BUF

  DESCRIPTION
    Allocate and assign a QMI IF command buffer from the PS MEM heap
    based on the QMI IF type
    
  PARAMETERS
    cmd_id - QMI command type
   
  RETURN VALUE
    cmd_buf_ptr - Pointer to the allocated command buffer

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void *qmi_qmux_get_cmd_buf
(
  qmi_cmd_id_e_type cmd_id
)
{
  switch (cmd_id)
  {
    case QMI_CMD_LEGACY_SVC_QMI_LINK_CLOSED:
    {
      qmi_qmux_cmd_buf_type *cmd_buf_ptr = NULL;
      cmd_buf_ptr = qmi_svc_ps_system_heap_mem_alloc(sizeof(qmi_qmux_cmd_buf_type),
                             FILE_ID_DS_QMUX,__LINE__);
      return ((void*)cmd_buf_ptr);
    }

    default:
      LOG_MSG_ERROR_1("Unknown QMI IF command %d", (int32)cmd_id);
  }
  return NULL;
} /* qmi_qmux_get_cmd_buf */

/*===========================================================================
  FUNCTION QMI_QMUX_PROCESS_CMD

  DESCRIPTION
    This function processes a QMI QMUX command or event.

    It is called by the QMI command handler and will dispatch the
    associated command/event handler function.

  PARAMETERS
    cmd_ptr:  private data buffer containing the QMI IF command
              information.

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_qmux_process_cmd
(
  void * cmd_ptr
)
{
  qmi_qmux_cmd_buf_type *cmd_buf_ptr = NULL;
/*-------------------------------------------------------------------------*/

  ASSERT(cmd_ptr);
  cmd_buf_ptr = (qmi_qmux_cmd_buf_type *)cmd_ptr;
  switch(cmd_buf_ptr->cmd_id)
  {
    case QMI_CMD_LEGACY_SVC_QMI_LINK_CLOSED:
      qmi_legacy_svc_qmi_link_closed(cmd_buf_ptr->data.clean_instance);      
      break;

    default:
      LOG_MSG_INFO2_1 ("Ignoring QMI QMUX cmd %d", cmd_buf_ptr->cmd_id);
      break;

  }/* switch(cmd_buf_ptr->cmd_id) */

  qmi_qmux_free_cmd_buf(cmd_ptr);
  return;
} /* qmi_qmux_process_cmd() */


#ifdef TEST_FRAMEWORK
#error code not present
#endif /* TEST_FRAMEWORK */

