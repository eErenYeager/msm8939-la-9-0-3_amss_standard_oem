#ifndef _DS_QMUX_EXT_H
#define _DS_QMUX_EXT_H
/*===========================================================================

                         D S _ Q M U X _ E X T . H

DESCRIPTION

  The Data Services Qualcomm MSM Interface control channel message
  multiplexing protocol external header file.

  External entry point into the QMI module.

EXTERNALIZED FUNCTIONS

  qmux_process_rx_sig()
    Called by serving task in response to QMUX_RX_SIG

Copyright (c) 2004-2013 Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary.
===========================================================================*/
/*===========================================================================

                      EDIT HISTORY FOR FILE

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/qmicore/inc/ds_qmux_ext.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when        who    what, where, why
--------    ---    ---------------------------------------------------------- 
12/10/12    svj    Support 8 concurrent PDN's 
09/18/11    wc     Nikel and MPSS.NI.1.1 port configuration
02/19/09    am     DS Task De-coupling effort and introduction of DCC task.
08/23/06    ks     Moved qmux_init() here as it is now called from ds_task().
                   Moved qmi cmd enum to qmi_defs.h.
07/11/06    ks     Renamed QMI GSDI READ/WRITE cmds.
07/06/06    ks     Added new cmds to process CM events in DS context.
09/08/05    ks     Added cmds for gsdi read/write async cbacks.
06/27/05    jd     Added qmux_process_cmd
05/31/05   jd/ks   Code review updates
05/11/05    ks     changed name from ds_qmux.h to ds_qmux_ext.h
03/08/05    ks     Separated into another file ds_qmux_svc.h
11/21/04    jd     Created module
===========================================================================*/

/*===========================================================================

                          INCLUDE FILES FOR MODULE

===========================================================================*/

#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"

#include "dcc_task_defs.h"
#include "qmi_modem_task_cmd.h"
#include "qmi_svc_defs.h"
#include "rex.h"
#include "ds_qmi_svc_ext.h"

/*===========================================================================

                               DEFINITIONS

===========================================================================*/

/*===========================================================================

                                DATA TYPES

===========================================================================*/

/*---------------------------------------------------------------------------
  QMI Device identifiers, used to map to RDM devices/SIO ports
  ***DO NOT CHANGE EXISTING VALUES AS THEY ARE USED IN NVRAM***
---------------------------------------------------------------------------*/
typedef enum qmux_device_id_e
{
  QMI_DEV_NONE      = 0x0000, 
  QMI_DEV_UART1     = 0x0100, 
  QMI_DEV_UART2     = 0x0101, 
  QMI_DEV_UART3     = 0x0102, 
  QMI_DEV_USB1      = 0x0200,  
  QMI_DEV_USB2      = 0x0201,  
  QMI_DEV_USB3      = 0x0202, 
  QMI_DEV_USB4      = 0x0203,
  QMI_DEV_USB5      = 0x0204,
  QMI_DEV_MUX1      = 0x0300,  
  QMI_DEV_MUX2      = 0x0301,  
  QMI_DEV_MUX3      = 0x0302,  
  QMI_DEV_MUX4      = 0x0303,  
  QMI_DEV_MUX5      = 0x0304,  
  QMI_DEV_MUX6      = 0x0305,  
  QMI_DEV_MUX7      = 0x0306,  
  QMI_DEV_MUX8      = 0x0307,  
  QMI_DEV_MUX9      = 0x0308,  
  QMI_DEV_MUX10     = 0x0309, 
  QMI_DEV_MUX11     = 0x030A, 
  QMI_DEV_MUX12     = 0x030B, 
  QMI_DEV_MUX13     = 0x030C, 
  QMI_DEV_MUX14     = 0x030D, 
  QMI_DEV_MUX15     = 0x030E, 
  QMI_DEV_MUX16     = 0x030F, 
  QMI_DEV_SMD1      = 0x0400,  
  QMI_DEV_SMD2      = 0x0401,  
  QMI_DEV_SMD3      = 0x0402,  
  QMI_DEV_SMD4      = 0x0403,  
  QMI_DEV_SMD5      = 0x0404,
  QMI_DEV_SMD6      = 0x0405,
  QMI_DEV_SMD7      = 0x0406,
  QMI_DEV_SMD8      = 0x0407,
  QMI_DEV_SMD1_EMBEDDED = QMI_DEV_SMD1,
  QMI_DEV_SMD2_EMBEDDED = QMI_DEV_SMD2,
  QMI_DEV_SMD3_EMBEDDED = QMI_DEV_SMD3,
  QMI_DEV_SMD4_EMBEDDED = QMI_DEV_SMD4,
  QMI_DEV_SMD5_EMBEDDED = QMI_DEV_SMD5,
  QMI_DEV_SMD6_EMBEDDED = QMI_DEV_SMD6,
  QMI_DEV_SMD7_EMBEDDED = QMI_DEV_SMD7,
  QMI_DEV_SMD8_EMBEDDED = QMI_DEV_SMD8,
  QMI_DEV_SMD1_TETHERED = 0x0408,
  QMI_DEV_SMD2_TETHERED = 0x0409,
  QMI_DEV_SMD3_TETHERED = 0x040A,
  QMI_DEV_SMD4_TETHERED = 0x040B,
  QMI_DEV_SMD5_TETHERED = 0x040C,
  QMI_DEV_SMD6_TETHERED = 0x040D,
  QMI_DEV_SMD7_TETHERED = 0x040E,
  QMI_DEV_SMD8_TETHERED = 0x040F,
  QMI_DEV_SMD9_TETHERED = 0x0410,
  QMI_DEV_A2P1      = 0x0500,
  QMI_DEV_A2P2      = 0x0501,
  QMI_DEV_A2P3      = 0x0502,

  /* QMI devices for SDIO MUX ports (fusion architecture) */
  QMI_DEV_SDIO_MUX_A2_0  = 0x0600,
  QMI_DEV_SDIO_MUX_A2_1  = 0x0601,
  QMI_DEV_SDIO_MUX_A2_2  = 0x0602,
  QMI_DEV_SDIO_MUX_A2_3  = 0x0603,
  QMI_DEV_SDIO_MUX_A2_4  = 0x0604,
  QMI_DEV_SDIO_MUX_A2_5  = 0x0605,
  QMI_DEV_SDIO_MUX_A2_6  = 0x0606,
  QMI_DEV_SDIO_MUX_A2_7  = 0x0607,
  QMI_DEV_SDIO_MUX_A2_8  = 0x0608,  // tethered
  QMI_DEV_SDIO_MUX_A2_9  = 0x0609,
  QMI_DEV_SDIO_MUX_A2_10 = 0x060A,
  QMI_DEV_SDIO_MUX_A2_11 = 0x0614,
  QMI_DEV_SDIO_MUX_A2_0_EMBEDDED = QMI_DEV_SDIO_MUX_A2_0,
  QMI_DEV_SDIO_MUX_A2_1_EMBEDDED = QMI_DEV_SDIO_MUX_A2_1,
  QMI_DEV_SDIO_MUX_A2_2_EMBEDDED = QMI_DEV_SDIO_MUX_A2_2,
  QMI_DEV_SDIO_MUX_A2_3_EMBEDDED = QMI_DEV_SDIO_MUX_A2_3,
  QMI_DEV_SDIO_MUX_A2_4_EMBEDDED = QMI_DEV_SDIO_MUX_A2_4,
  QMI_DEV_SDIO_MUX_A2_5_EMBEDDED = QMI_DEV_SDIO_MUX_A2_5,
  QMI_DEV_SDIO_MUX_A2_6_EMBEDDED = QMI_DEV_SDIO_MUX_A2_6,
  QMI_DEV_SDIO_MUX_A2_7_EMBEDDED = QMI_DEV_SDIO_MUX_A2_7,
  QMI_DEV_SDIO_MUX_A2_8_EMBEDDED = QMI_DEV_SDIO_MUX_A2_9,
  QMI_DEV_SDIO_MUX_A2_9_EMBEDDED = QMI_DEV_SDIO_MUX_A2_10,
  QMI_DEV_SDIO_MUX_A2_10_EMBEDDED = QMI_DEV_SDIO_MUX_A2_11,
  QMI_DEV_SDIO_MUX_A2_0_TETHERED = QMI_DEV_SDIO_MUX_A2_8,

  QMI_DEV_SDIO_MUX_A2_REV_IP_0  = 0x060B,
  QMI_DEV_SDIO_MUX_A2_REV_IP_1  = 0x060C,
  QMI_DEV_SDIO_MUX_A2_REV_IP_2  = 0x060D,
  QMI_DEV_SDIO_MUX_A2_REV_IP_3  = 0x060E,
  QMI_DEV_SDIO_MUX_A2_REV_IP_4  = 0x060F,
  QMI_DEV_SDIO_MUX_A2_REV_IP_5  = 0x0610,
  QMI_DEV_SDIO_MUX_A2_REV_IP_6  = 0x0611,
  QMI_DEV_SDIO_MUX_A2_REV_IP_7  = 0x0612,
  QMI_DEV_SDIO_MUX_A2_REV_IP_8  = 0x0613,  
  // XXX 0x0614 is already in use

  /* QMI devices for SMUX ports */
  QMI_DEV_SMUX0     = 0x0700,
  QMI_DEV_SMUX1     = 0x0701,
  QMI_DEV_SMUX2     = 0x0702,
  QMI_DEV_SMUX3     = 0x0703,
  QMI_DEV_SMUX4     = 0x0704,
  QMI_DEV_SMUX5     = 0x0705,
  QMI_DEV_SMUX6     = 0x0706,
  QMI_DEV_SMUX7     = 0x0707,

  /* A2 MUX-ed ports (logical) */
  QMI_DEV_MUX_A2_CH_0      = 0x0800,
  QMI_DEV_MUX_A2_CH_1      = 0x0801,
  QMI_DEV_MUX_A2_CH_2      = 0x0802,
  QMI_DEV_MUX_A2_CH_3      = 0x0803,
  QMI_DEV_MUX_A2_CH_4      = 0x0804,
  QMI_DEV_MUX_A2_CH_5      = 0x0805,
  QMI_DEV_MUX_A2_CH_6      = 0x0806,
  QMI_DEV_MUX_A2_CH_7      = 0x0807,
  QMI_DEV_MUX_A2_REV_CH_0  = 0x0820,
  QMI_DEV_MUX_A2_REV_CH_1  = 0x0821,
  QMI_DEV_MUX_A2_REV_CH_2  = 0x0822,
  QMI_DEV_MUX_A2_REV_CH_3  = 0x0823,
  QMI_DEV_MUX_A2_REV_CH_4  = 0x0824,
  QMI_DEV_MUX_A2_REV_CH_5  = 0x0825,
  QMI_DEV_MUX_A2_REV_CH_6  = 0x0826,
  QMI_DEV_MUX_A2_REV_CH_7  = 0x0827,
  QMI_DEV_MUX_A2_REV_CH_8  = 0x0828,

  /* Control values */
  QMI_DEV_STATIC_BRIDGE_BEGIN = QMI_DEV_SMD1_TETHERED,
  QMI_DEV_STATIC_BRIDGE_END   = QMI_DEV_SMD9_TETHERED,
  QMI_DEV_USB_BEGIN           = QMI_DEV_USB1,
  QMI_DEV_USB_END             = QMI_DEV_USB5,
  QMI_DEV_MAX                 = 0xFFFF,
} qmux_device_id_type;


#ifdef FEATURE_DATA_QMI
/*===========================================================================

                        EXTERNAL FUNCTION DEFINTIONS

===========================================================================*/
/*===========================================================================
  FUNCTION QMUX_POWERUP_INIT()

  DESCRIPTION
    QMUX power up initialization.

  PARAMETERS
    None

  RETURN VALUE
    TRUE on successful initialization
    FALSE otherwise

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean qmux_powerup_init
(
  void
);

/*===========================================================================
  FUNCTION QMUX_INIT()

  DESCRIPTION
    Initialize specified QMUX instance.  SIO control IO path will be set up,
    and QMUX instance state will be reset to default.

  PARAMETERS
    qmi_instance : qmi link to initialize
    serving_tcb  : REX tcb pointer for serving task
                   (messages will be processed in this task context)

  RETURN VALUE
    TRUE on successful initialization
    FALSE otherwise

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean qmux_init
(
  void
);

/*===========================================================================
  FUNCTION QMI_PROCESS_CMD()

  DESCRIPTION
    This function processes a QMI cmd.

    It is called by the serving task main loop and will dispatch the
    associated event handler function.

  PARAMETERS
    cmd          :  QMI command to be processed
    user_data_ptr:  private data buffer containing the QMI command
                    information.

  RETURN VALUE
    None

  DEPENDENCIES
    QMI module must already have been initialized

  SIDE EFFECTS
    None
===========================================================================*/
void qmi_process_cmd
(
  qmi_cmd_enum_type        cmd,
  void                   * user_data_ptr
);

/*===========================================================================
  FUNCTION QMUX_REG_VS_SERVICE_LIST()

  DESCRIPTION
    Register service table with QMUX

  PARAMETERS
    qmi_instance         : instance of QMI
    svc_list             : array pointer to all services for qmi instance
    num_services         : total number of services in list that
                           are to be registered

  RETURN VALUE
    TRUE if table registration was successful
    FALSE if table format/content was invalid

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
extern boolean qmux_reg_vs_service_list
(
  qmi_instance_e_type  qmi_instance,
  qmux_svc_info_type * svc_list,
  uint8                num_services
);

/*===========================================================================
  FUNCTION QMUX_REG_VS_SERVICE()

  DESCRIPTION
    Register the vendor specific service along with the QMI instance 
    and service configuration.  Function is used to register VS Services 
    ONLY.
   
  PARAMETERS
    qmi_instance : instance of QMI
    service      : service type
    cfg          : configuration parameters for the service instance

  RETURN VALUE
    NULL           -  service registration failed
    service handle -  qmux service handle 

  DEPENDENCIES
    The service list that should contain the services for a specified QMI 
    instance should be registered with QMUX using qmux_reg_service_list().

  SIDE EFFECTS
    None
===========================================================================*/
extern void * qmux_reg_vs_service
(
  qmi_instance_e_type     qmi_instance,
  qmux_service_e_type     service,
  qmux_svc_config_type *  cfg
);

#endif /* FEATURE_DATA_QMI */
#endif /* _DS_QMUX_EXT_H */
