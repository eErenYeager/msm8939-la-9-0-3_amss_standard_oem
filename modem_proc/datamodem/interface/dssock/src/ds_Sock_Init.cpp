/*===========================================================================
  FILE: ds_Sock_Init.cpp

  OVERVIEW: This file provides functions that are used to perform DSSock module
  initialization

  DEPENDENCIES: None

  Copyright (c) 2008 - 2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary
===========================================================================*/

/*===========================================================================
  EDIT HISTORY FOR MODULE

  Please notice that the changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/dssock/src/ds_Sock_Init.cpp#1 $
  $DateTime: 2015/01/27 06:42:19 $$Author: mplp4svc $

  when       who what, where, why
  ---------- --- ------------------------------------------------------------
  2008-05-14 msr Created module

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "ds_Sock_Init.h"
#include "ds_Sock_MemManager.h"
#include "ds_Sock_EventManager.h"
#include "ds_Utils_DebugMsg.h"

#include "ds_Sock_ISocketFactory.h"
#include "ds_Sock_CreateInstance.h"
#include "ds_Sock_CSocketFactory.h"
#include "ds_Utils_CSSupport.h"

using namespace ds::Sock;

static ISocketFactory *ISocketFactoryPtr = 0;
static ISocketFactory *ISocketFactoryPrivPtr = 0;

/*===========================================================================

                           EXTERNAL FUNCTIONS

===========================================================================*/

void DSSockPowerup
(
  void
)
{
  int32  ret_val;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_FUNCTION_ENTRY_0("DSSockPowerup(): "
                           "");

  MemManager::MemPoolInit();

  /*-------------------------------------------------------------------------
    Create singletons and only release during powerdown
    Allocate a SocketFactory object if it is not already allocated.
  -------------------------------------------------------------------------*/

  ret_val = DSSockCreateInstance(0, 
                                 AEECLSID_CSocketFactory, 
                                 (void**)&ISocketFactoryPtr);
  if (AEE_SUCCESS != ret_val)
  {
    LOG_MSG_ERROR_1 ("DSSockPowerup(): CreateInstance SocketFactory "
                     "returned %d ", ret_val);
    ASSERT(0);
    return;
  }

  LOG_MSG_FUNCTION_EXIT_0("DSSockPowerup(): "
                          "Success");

} /* DSSockPowerup() */

void DSSockPowerdown
(
  void
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  DS_UTILS_RELEASEIF(ISocketFactoryPrivPtr);
  DS_UTILS_RELEASEIF(ISocketFactoryPtr);
} /* DSPowerdown() */


void DSSockInit
(
  void
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_FUNCTION_ENTRY_0("DSSockInit(): "
                           "");

  ds::Sock::EventManager::Init();

  LOG_MSG_FUNCTION_EXIT_0("DSSockInit(): "
                          "Success");

} /* DSSockInit() */

void DSSockDeinit
(
  void
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  EventManager::Deinit();

} /* DSSockDeinit() */
