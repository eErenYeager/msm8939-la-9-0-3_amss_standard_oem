/*===========================================================================
  FILE: ds_Sock_ICMPErrInfo.cpp

  OVERVIEW: This file provides implementation of the ICMPErrInfo class.

  DEPENDENCIES: None

  Copyright (c) 2009-2010 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary
===========================================================================*/

/*===========================================================================
  EDIT HISTORY FOR MODULE

  Please notice that the changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/dssock/src/ds_Sock_ICMPErrInfo.cpp#1 $
  $DateTime: 2015/01/27 06:42:19 $$Author: mplp4svc $

  when       who what, where, why
  ---------- --- ------------------------------------------------------------
  2009-03-02 msr Created module

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "comdef.h"
#include "customer.h"
#include "target.h"
#include "ds_Utils_StdErr.h"

#include "ds_Sock_ICMPErrInfo.h"
#include "ds_Utils_DebugMsg.h"
#include "ps_mem.h"

#include "stringl.h"

using namespace ds::Sock;
using namespace ds::Error;


/*===========================================================================

                     PUBLIC MEMBER FUNCTIONS

===========================================================================*/
void * ICMPErrInfo::operator new
(
  unsigned int numBytes
)
throw()
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  return ps_mem_get_buf( PS_MEM_ICMP_ERR_INFO_TYPE);

} /* ICMPErrInfo::operator new() */


void ICMPErrInfo::operator delete
(
  void *  bufPtr
)
throw()
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (0 == bufPtr)
  {
    LOG_MSG_ERROR_0("delete(): "
                    "NULL ptr");
    ASSERT( 0);
    return;
  }

  PS_MEM_FREE( bufPtr);
  return;

} /* ICMPErrInfo::operator delete() */


ICMPErrInfo::ICMPErrInfo
(
  struct ps_sock_extended_err  psExtendedErrInfo,
  ds::SockAddrInternalType*    _addr
) :
    addr(*_addr)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_INFO1_1("addr(): "
                  "Obj 0x%x", this);

  extendedErrInfo.error_number = psExtendedErrInfo.ee_errno;
  extendedErrInfo.origin       = psExtendedErrInfo.ee_origin;
  extendedErrInfo.type         = psExtendedErrInfo.ee_type;
  extendedErrInfo.code         = psExtendedErrInfo.ee_code;
  extendedErrInfo.info         = psExtendedErrInfo.ee_info;

  /*-------------------------------------------------------------------------
    Using explicit scope to shut up lint. It complains because AddRef() is
    declared as virtual to shut up compiler
  -------------------------------------------------------------------------*/
  (void) ICMPErrInfo::AddRef();
  return;

} /* ICMPErrInfo::ICMPErrInfo() */


ds::ErrorType CDECL ICMPErrInfo::GetAncID
(
  ds::Sock::AncDataIDType *  ancIDPtr
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (0 == ancIDPtr)
  {
    LOG_MSG_INVALID_INPUT_0("ICMPErrInfo::GetAncID(): "
                            "NULL arg");
    return QDS_EFAULT;
  }

  *ancIDPtr = AncData::ICMP_ERROR_INFO;
  return AEE_SUCCESS;

} /* ICMPErrInfo::GetAncID() */


ds::ErrorType CDECL ICMPErrInfo::SetAncID
(
  ds::Sock::AncDataIDType  ancID
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  return AEE_SUCCESS;

} /* ICMPErrInfo::SetAncID() */


ds::ErrorType CDECL ICMPErrInfo::GetExtendedErr
(
  ExtendedErrType *  extendedErrInfoPtr
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_FUNCTION_ENTRY_1("ICMPErrInfo::GetExtendedErr(): "
                           "Obj 0x%x", this);

  if (0 == extendedErrInfoPtr)
  {
    LOG_MSG_INVALID_INPUT_1("ICMPErrInfo::GetExtendedErr(): "
                            "NULL arg, obj 0x%x", this);
    return QDS_EFAULT;
  }

  *extendedErrInfoPtr = extendedErrInfo;

  LOG_MSG_FUNCTION_EXIT_1("ICMPErrInfo::GetExtendedErr(): "
                          "Success, obj 0x%x", this);
  return AEE_SUCCESS;

} /* ICMPErrInfo::GetExtendedErr() */


ds::ErrorType CDECL ICMPErrInfo::GetAddr
(
  ds::SockAddrStorageType _addr
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_FUNCTION_ENTRY_1("ICMPErrInfo::GetAddr(): "
                           "Obj 0x%x", this);

  if (0 == _addr)
  {
    LOG_MSG_INVALID_INPUT_1("ICMPErrInfo::GetAddr(): "
                            "NULL arg, obj 0x%x", this);
    return QDS_EFAULT;
  }

  memscpy( _addr, sizeof(ds::SockAddrStorageType),
           &addr, sizeof(ds::SockAddrStorageType));

  LOG_MSG_FUNCTION_EXIT_1("ICMPErrInfo::GetAddr(): "
                          "Success, obj 0x%x", this);
  return AEE_SUCCESS;

} /* ICMPErrInfo::GetAddr() */


ds::ErrorType CDECL ICMPErrInfo::QueryInterface
(
  AEEIID   iid,
  void **  objPtrPtr
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_FUNCTION_ENTRY_2("ICMPErrInfo::QueryInterface(): "
                           "Obj 0x%x iid %d", this, iid);

  if (0 == objPtrPtr)
  {
    LOG_MSG_INVALID_INPUT_1("ICMPErrInfo::QueryInterface(): "
                            "NULL arg, obj 0x%x", this);
    return QDS_EFAULT;
  }

  switch (iid)
  {
    case AEEIID_IAncDataPriv:
    {
      *objPtrPtr = static_cast <IAncDataPriv *> ( this);
      (void) AddRef();
      break;
    }

    case AEEIID_IICMPErrInfoPriv:
    {
      *objPtrPtr = this;
      (void) AddRef();
      break;
    }

    case AEEIID_IQI:
    {
      *objPtrPtr = static_cast <IQI *> ( this);
      (void) AddRef();
      break;
    }

    default:
    {
      *objPtrPtr = 0;
      LOG_MSG_INVALID_INPUT_2("ICMPErrInfo::QueryInterface(): "
                              "Unknown iid %d, obj 0x%x", iid, this);
      return AEE_ECLASSNOTSUPPORT;
    }
  } /* switch */

  LOG_MSG_FUNCTION_EXIT_1("ICMPErrInfo::QueryInterface(): "
                          "Success, obj 0x%x", this);
  return AEE_SUCCESS;

} /* ICMPErrInfo::QueryInterface() */

