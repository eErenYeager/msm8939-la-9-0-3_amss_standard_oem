/*===========================================================================
  FILE: ds_Sock_RecvIFInfo.cpp

  OVERVIEW: This file provides implementation of the RecvIFInfo class.

  DEPENDENCIES: None

  Copyright (c) 2009-2010 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary
===========================================================================*/

/*===========================================================================
  EDIT HISTORY FOR MODULE

  Please notice that the changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/dssock/src/ds_Sock_RecvIFInfo.cpp#1 $
  $DateTime: 2015/01/27 06:42:19 $$Author: mplp4svc $

  when       who what, where, why
  ---------- --- ------------------------------------------------------------
  2009-03-02 msr Created module

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "comdef.h"
#include "customer.h"
#include "target.h"
#include "ds_Utils_StdErr.h"

#include "ds_Sock_RecvIFInfo.h"
#include "ds_Utils_DebugMsg.h"
#include "ps_mem.h"

using namespace ds::Sock;
using namespace ds::Error;


/*===========================================================================

                     PUBLIC MEMBER FUNCTIONS

===========================================================================*/
void * RecvIFInfo::operator new
(
  unsigned int numBytes
)
throw()
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  return ps_mem_get_buf( PS_MEM_RECV_IF_INFO_TYPE);

} /* RecvIFInfo::operator new() */


void RecvIFInfo::operator delete
(
  void *  bufPtr
)
throw()
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (0 == bufPtr)
  {
    LOG_MSG_ERROR_0("delete(): "
                    "NULL ptr");
    ASSERT( 0);
    return;
  }

  PS_MEM_FREE( bufPtr);
  return;

} /* RecvIFInfo::operator delete() */


RecvIFInfo::RecvIFInfo
(
  unsigned int  _recvIFHandle
) :
    recvIFHandle( _recvIFHandle)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_INFO1_2("Obj 0x%x, RecvIF %d", this, recvIFHandle);

  /*-------------------------------------------------------------------------
    Using explicit scope to shut up lint. It complains because AddRef() is
    declared as virtual to shut up compiler
  -------------------------------------------------------------------------*/
  (void) RecvIFInfo::AddRef();
  return;

} /* RecvIFInfo::RecvIFInfo() */


ds::ErrorType CDECL RecvIFInfo::GetAncID
(
  ds::Sock::AncDataIDType *  ancIDPtr
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (0 == ancIDPtr)
  {
    LOG_MSG_INVALID_INPUT_0("RecvIFInfo::GetAncID(): "
                            "NULL arg");
    return QDS_EFAULT;
  }

  *ancIDPtr = AncData::RECV_IF_INFO;
  return AEE_SUCCESS;

} /* RecvIFInfo::GetAncID() */


ds::ErrorType CDECL RecvIFInfo::SetAncID
(
  ds::Sock::AncDataIDType  ancID
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  return AEE_SUCCESS;

} /* RecvIFInfo::SetAncID() */


ds::ErrorType CDECL RecvIFInfo::GetRecvIFHandle
(
  unsigned int *  recvIFHandlePtr
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_FUNCTION_ENTRY_1("RecvIFInfo::GetRecvIFHandle(): "
                           "Obj 0x%x", this);

  if (0 == recvIFHandlePtr)
  {
    LOG_MSG_INVALID_INPUT_1("RecvIFInfo::GetRecvIFHandle(): "
                            "NULL arg, obj 0x%x", this);
    return QDS_EFAULT;
  }

  *recvIFHandlePtr = recvIFHandle;

  LOG_MSG_FUNCTION_EXIT_1("RecvIFInfo::GetRecvIFHandle(): "
                          "Success, obj 0x%x", this);
  return AEE_SUCCESS;

} /* RecvIFInfo::GetRecvIFHandle() */


ds::ErrorType CDECL RecvIFInfo::QueryInterface
(
  AEEIID   iid,
  void **  objPtrPtr
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_FUNCTION_ENTRY_2("RecvIFInfo::QueryInterface(): "
                           "Obj 0x%x iid %d", this, iid);

  if (0 == objPtrPtr)
  {
    LOG_MSG_INVALID_INPUT_1("RecvIFInfo::QueryInterface(): "
                            "NULL arg, obj 0x%x", this);
    return QDS_EFAULT;
  }

  switch (iid)
  {
    case AEEIID_IAncDataPriv:
    {
      *objPtrPtr = static_cast <IAncDataPriv *> ( this);
      (void) AddRef();
      break;
    }

    case AEEIID_IRecvIFInfoPriv:
    {
      *objPtrPtr = this;
      (void) AddRef();
      break;
    }

    case AEEIID_IQI:
    {
      *objPtrPtr = static_cast <IQI *> ( this);
      (void) AddRef();
      break;
    }

    default:
    {
      *objPtrPtr = 0;
      LOG_MSG_INVALID_INPUT_2("RecvIFInfo::QueryInterface(): "
                              "Unknown iid %d, obj 0x%x", iid, this);
      return AEE_ECLASSNOTSUPPORT;
    }
  } /* switch */

  LOG_MSG_FUNCTION_EXIT_1("RecvIFInfo::QueryInterface(): "
                          "Success, obj 0x%x", this);
  return AEE_SUCCESS;

} /* RecvIFInfo::QueryInterface() */

