#ifndef DS_SOCK_ROUTINGMANAGER_H
#define DS_SOCK_ROUTINGMANAGER_H
/*===========================================================================
  @file ds_Sock_RoutingManager.h

  This file declares a class which provides a set of helper methods useful
  in routing and filtering.

  Copyright (c) 2008 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary
===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/dssock/src/ds_Sock_RoutingManager.h#1 $
  $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "comdef.h"

#include "ds_Utils_CSSupport.h"
#include "ds_Net_IPolicy.h"
#include "ds_Sock_Socket.h"
#include "ps_rt_meta_info.h"
#include "ps_iface_defs.h"


/*===========================================================================

                      PUBLIC DATA DECLARATIONS

===========================================================================*/
namespace ds
{
  namespace Sock
  {
    class RoutingManager
    {                                                         /*lint !e578 */
      public:
        static ds::ErrorType RoutePacket
        (
          ds::Sock::Socket *      sockPtr,
          bool                    isSystemSocket,
          ds::Net::IPolicy *      policyPtr,
          ps_rt_meta_info_type *  newRtMetaInfoPtr
        );

        static void FltrClient
        (
          ds::Sock::Socket *                   sockPtr,
          ps_iface_ipfltr_client_id_enum_type  fltrClient,
          ps_rt_meta_info_type *               newRtMetaInfoPtr
        );
    };

  } /* namespace Sock */
} /* namespace ds */

#endif /* DS_SOCK_ROUTINGMANAGER_H */
