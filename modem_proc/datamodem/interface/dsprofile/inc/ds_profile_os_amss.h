/******************************************************************************
  @file    ds_profile_os_amss.h
  @brief   Operating System specific header

  DESCRIPTION
  This header defines API for OS (AMSS) specific logging, locking mechanisms. 

  INITIALIZATION AND SEQUENCING REQUIREMENTS
  N/A

  ---------------------------------------------------------------------------
  Copyright (C) 2009 Qualcomm Technologies Incorporated.
  All Rights Reserved. QUALCOMM Proprietary and Confidential.
  ---------------------------------------------------------------------------
******************************************************************************/
/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/dsprofile/inc/ds_profile_os_amss.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
06/21/10   vk      Support for apn based profile search
04/30/10   ars     Made modem_mem_alloc/free and str fns platform independent
09/30/09   mg      Created the module. First version of the file.
===========================================================================*/

#ifndef DS_PROFILE_OS_AMSS_H
#define DS_PROFILE_OS_AMSS_H

#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"
#include "rex.h"
#include "data_msg.h"

#include <stringl/stringl.h>
#ifdef FEATURE_DATA_MODEM_HEAP
  #include "modem_mem.h"
#endif /* FEATURE_DATA_MODEM_HEAP */

typedef rex_crit_sect_type  plm_lock_type;

int ds_profile_log_init(  char *lib );
int ds_profile_lock_init( plm_lock_type *lock );

int ds_profile_lock_acq( plm_lock_type *lock );
int ds_profile_lock_rel( plm_lock_type *lock );

#define DS_PROFILE_LOGD(format, arg1) DATA_MSG1( MSG_LEGACY_MED, format, arg1 )
#define DS_PROFILE_LOGE(format, arg1) DATA_MSG1( MSG_LEGACY_ERROR, format, arg1 ) 

#ifdef FEATURE_DATA_MODEM_HEAP
  #define DS_PROFILE_MEM_ALLOC( size, client ) modem_mem_alloc( size, client )
  #define DS_PROFILE_MEM_FREE( ptr, client ) modem_mem_free( ptr, client )
#else 
  #define DS_PROFILE_MEM_ALLOC( size, client ) malloc( size )
  #define DS_PROFILE_MEM_FREE( ptr, client ) free( ptr )
#endif /* FEATURE_DATA_MODEM_HEAP */

#define DS_PROFILE_STR_LEN( str ) strlen( str )
#define DS_PROFILE_STR_CPY( str1, str2, size ) strlcpy( str1, str2, size )
#define DS_PROFILE_STR_CMP(str1, str2, len ) strncmp( str1, str2, len )
#define DS_PROFILE_STR_CASE_CMP(str1, str2, len ) strncasecmp( str1, str2, len )
#define DS_PROFILE_STR_PRINTF snprintf

#endif /* DS_PROFILE_OS_AMSS_H */
