/******************************************************************************
  @file    ds_profile.c
  @brief   DS PROFILE API implementation

  DESCRIPTION
  This file contains implementation of DS PROFILE API.

  INITIALIZATION AND SEQUENCING REQUIREMENTS
  N/A

  ---------------------------------------------------------------------------
  Copyright (C) 2009-2012 Qualcomm Technologies Incorporated.
  All Rights Reserved. QUALCOMM Proprietary and Confidential.
  ---------------------------------------------------------------------------
******************************************************************************/
/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/dsprofile/src/ds_profile.c#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
11/27/12   vs      Removed ds_profile ENTRY and EXIT log messages
05/31/11   bvd     Added changes for supporting non-persistent profile and other profile attributes
02/16/11   ttv     Added DSDS changes for consolidated profile family.
07/14/10   asn     Address initialization of DS Profile and UMTS Profile Reg
04/30/10   ars     Made modem_mem_alloc/free and str fns platform independent
09/30/09   mg      Created the module. First version of the file.
===========================================================================*/

/*---------------------------------------------------------------------------
                           INCLUDE FILES
---------------------------------------------------------------------------*/
#include "datamodem_variation.h"
#include "ds_profile_plm.h"
#include "ds_profile_os.h"
#include "ds_profilei.h"
#include "ds_profile_server.h"
#include "ds_profile_remote_client.h"
#include "ps_crit_sect.h"

#define DS_PROFILE_LIB_STATE_INVAL   0x00
#define DS_PROFILE_LIB_STATE_INITED  0x01
#define MAX_LIB_INST_NAME            10
static char           lib_state;

/* Platform specific lock */
static plm_lock_type  lib_lock;
static uint32         instance;
static boolean ds_profile_modified = FALSE;

static char logging_prefix[MAX_LIB_INST_NAME] = "PRFREG_";

/*---------------------------------------------------------------------------
                               UTILITY MACROS
---------------------------------------------------------------------------*/
/*lint -save -e655*/
/* Macro to check if profile handle is valid   */
#define HNDL_IS_VALID( hndl ) \
          ( ( hndl ) != NULL )

/* Macro to check if lib is inited             */
#define LIB_STATE_IS_VALID( lib_state ) \
  ( lib_state == DS_PROFILE_LIB_STATE_INITED )

/* Macro to check if transation type is valid  */
#define TRN_IS_VALID( trn ) \
  ( trn & DS_PROFILE_TRN_VALID )

/* Macro to check if list_type is valid        */
#define LIST_TYPE_IS_VALID( lst ) \
  ( lst != NULL )
 
/* Macro to check if itr is valid              */
#define ITR_IS_VALID( itr ) \
  ( itr != NULL )

/* TEST_FRAMEWORK for QTF                      */
#if 0
/* Macro to acquire lock                       */
#define ACQ_LOCK( msg ) \
  if ( ds_profile_lock_acq( lib_lock ) != 0 ) \
  { \
    DS_PROFILE_LOGE( msg , 0 ); \
    DS_PROFILE_LOGE( "FAIL: unable to acquire lock", 0 ); \
    return DS_PROFILE_REG_RESULT_FAIL; \
  } 

/* Macro to release lock                       */
#define REL_LOCK( msg ) \
  if ( ds_profile_lock_rel( lib_lock ) != 0 ) \
  { \
    DS_PROFILE_LOGE( msg , 0 ); \
    DS_PROFILE_LOGE( "FAIL: unable to release lock", 0 ); \
    return DS_PROFILE_REG_RESULT_FAIL; \
  }
#else
/* Macro to acquire lock                       */
#define ACQ_LOCK( msg ) \
  if ( ds_profile_lock_acq( &lib_lock ) != 0 ) \
  { \
    DS_PROFILE_LOGE( msg , 0 ); \
    DS_PROFILE_LOGE( "FAIL: unable to acquire lock", 0 ); \
    return DS_PROFILE_REG_RESULT_FAIL; \
  } 

/* Macro to release lock                       */
#define REL_LOCK( msg ) \
  if ( ds_profile_lock_rel( &lib_lock ) != 0 ) \
  { \
    DS_PROFILE_LOGE( msg , 0 ); \
    DS_PROFILE_LOGE( "FAIL: unable to release lock", 0 ); \
    return DS_PROFILE_REG_RESULT_FAIL; \
  }
#endif 

#define FAMILY_IS_VALID(family) \
  ( family == DS_PROFILE_EMBEDDED_PROFILE_FAMILY  || \
    family == DS_PROFILE_TETHERED_PROFILE_FAMILY )

/*===========================================================================
FUNCTION DS_PROFILE_POWER_UP_INIT

DESCRIPTION
  This function performs the DS Profile power up init.

PARAMETERS

DEPENDENCIES 
  
RETURN VALUE 

SIDE EFFECTS 
 
===========================================================================*/
void ds_profile_power_up_init (void)
{
  (void)ds_profile_lock_init( &lib_lock );
  return;
}

/*===========================================================================
FUNCTION DS_PROFILE_INIT_LIB

DESCRIPTION
  This function initializes the DS profile library. On modem, this function is 
  called only once at initialization. This will initialize the library for
  that process domain.

PARAMETERS

DEPENDENCIES 
  ds_profile_power_up_init should have been called already
  
RETURN VALUE 
  DS_PROFILE_REG_RESULT_SUCCESS  : On successful operation
  DS_PROFILE_REG_RESULT_FAIL     : On general errors. This return code 
                                   provides blanket coverage
SIDE EFFECTS 
 
===========================================================================*/
ds_profile_status_etype ds_profile_init_lib (
  void
)
{
  uint8 tech_mask = 0;
  ds_profile_status_etype return_status = DS_PROFILE_REG_RESULT_FAIL;
  char lib[MAX_LIB_INST_NAME + 5];

  /* The flag below is used to ensure that atexit() function is called only once */
  static boolean called_atexit = FALSE;

  /*---------------------------------------------------------------------------
    multiple calls to init lib allowed from different thread contexts
  ---------------------------------------------------------------------------*/
  instance++;
  instance %= DS_PROFILE_MAX_NUM_HNDL;
  (void)DS_PROFILE_STR_PRINTF( lib, 
                        sizeof(logging_prefix) + sizeof(instance), 
                        "%s[%ld]", logging_prefix, instance );

  /*--------------------------------------------------------------------------- 
    Initialize logging
  ---------------------------------------------------------------------------*/
  if( ds_profile_log_init( lib ) != 0 )
  {
    DS_PROFILE_LOGE( "_init_lib: FAIL, log_init", 0 );
    goto ret_err;
  }

  /* Acquire lock */
  ACQ_LOCK( "_init_lib" );

  /*--------------------------------------------------------------------------- 
    Allow initialization only if not inited, ignore otherwise
  ---------------------------------------------------------------------------*/
  if ( LIB_STATE_IS_VALID( lib_state ) )
  {
    REL_LOCK( "_init_lib" );
    DS_PROFILE_LOGD( "_init_lib: lib already initialized, ignoring", 0 );
    return DS_PROFILE_REG_RESULT_SUCCESS;
  }

  /*--------------------------------------------------------------------------- 
    Initialize server thread, takes care of SubSystem Restart events
  ---------------------------------------------------------------------------*/
  return_status = ds_profile_server_start();
  if (DS_PROFILE_REG_RESULT_SUCCESS != return_status)
  {
    REL_LOCK( "_init_lib" );
    DS_PROFILE_LOGE( "_init_lib: Could not initialize ds_profile_server", 0 );
    goto ret_err;
  }

  /*--------------------------------------------------------------------------- 
    Call internal function for initialization. Returns the ORed value of all
    tech types, if 0 then error 
  ---------------------------------------------------------------------------*/
  if ( (tech_mask = dsi_profile_init()) ==  0 )
  {
    REL_LOCK( "_init_lib" );
    DS_PROFILE_LOGE( "_init_lib: FAIL, dsi_profile_init() failed, tech mask %x", tech_mask );
    goto ret_err;
  }

  DS_PROFILE_LOGD( "_init_lib: tech mask returned %x", tech_mask );

  /* Register process exit cleanup handler */
  if (FALSE == called_atexit)
  {
    (void)atexit(dsi_profile_close_lib);
    called_atexit = TRUE;
  }

  lib_state = DS_PROFILE_LIB_STATE_INITED;

  /* To register handler with dcc task to handle remote operations */
  ds_profile_remote_init();

  REL_LOCK( "_init_lib" );
  return DS_PROFILE_REG_RESULT_SUCCESS; 

ret_err:
  DS_PROFILE_LOGD( "_init_lib: EXIT with ERR", 0 );
  return DS_PROFILE_REG_RESULT_FAIL;
} 

/*===========================================================================
FUNCTION DS_PROFILE_CLOSE_LIB 
 
DESCRIPTION
  This functions cleans up any open handles, closes the library

PARAMETERS

DEPENDENCIES 
  
RETURN VALUE 
  DS_PROFILE_REG_RESULT_SUCCESS  : On successful operation
  DS_PROFILE_REG_RESULT_FAIL     : On general errors. This return code 
                                   provides blanket coverage
SIDE EFFECTS  
===========================================================================*/
ds_profile_status_etype ds_profile_close_lib (
  void
)
{
  /* Check lib was initialized */
  if ( !LIB_STATE_IS_VALID( lib_state ) ) 
  {
    DS_PROFILE_LOGE( "_close_lib: FAIL lib was not initialized", 0 );
    DS_PROFILE_LOGD( "_close_lib: EXIT with ERR", 0 );
    return DS_PROFILE_REG_RESULT_FAIL;
  }

  ACQ_LOCK( "_close_lib" );
  lib_state = DS_PROFILE_LIB_STATE_INVAL;
  /*--------------------------------------------------------------------------- 
    Check handles have been released and iterators are destroyed
  ---------------------------------------------------------------------------*/
  dsi_profile_close_lib();

  REL_LOCK( "_close_lib" );
  return DS_PROFILE_REG_RESULT_SUCCESS;
} 

/*===========================================================================
FUNCTION DS_PROFILE_BEGIN_TRANSACTION

DESCRIPTION
  This returns a Handle that the clients of this software library can use for 
  subsequent Profile operations. The Handle returned is of requested 
  transaction type. All Profile operations using this Handle require that 
  DS_PROFILE_END_TRANSACTION be called at the end. 

PARAMETERS
  trn  : requested transaction type
  tech : technology type
  num  : profile number
  hndl : pointer to return requested handle

DEPENDENCIES 
  
RETURN VALUE 
  DS_PROFILE_REG_RESULT_SUCCESS            : On successful operation
  DS_PROFILE_REG_RESULT_ERR_LIB_NOT_INITED : Library is not initialized
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE : Invalid tech type
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_NUM  :  Invalid profile number 
  DS_PROFILE_REG_RESULT_FAIL               : On general errors. This return 
                                             code provides blanket coverage
SIDE EFFECTS 
 
===========================================================================*/
ds_profile_status_etype ds_profile_begin_transaction (
  ds_profile_trn_etype   trn,
  ds_profile_tech_etype  tech,
  ds_profile_num_type    num,
  ds_profile_hndl_type  *hndl
)
{
  ds_profile_status_etype return_status = DS_PROFILE_REG_RESULT_FAIL;
  ds_profile_subs_etype subs_id = 
    (ds_profile_subs_etype)ps_sys_get_default_data_subscription();

  /* Validate lib state */
  if( !LIB_STATE_IS_VALID( lib_state ) )
  {
    DS_PROFILE_LOGE( "_begin_transaction: FAIL lib not inited ", 0 );
    return_status = DS_PROFILE_REG_RESULT_ERR_LIB_NOT_INITED; 
    goto ret_err;
  }

  /* Validate transaction */
  if ( !TRN_IS_VALID( trn ) )
  {
    DS_PROFILE_LOGE( "_begin_transaction: FAIL invalid transaction type ", 0 );
    return_status = DS_PROFILE_REG_RESULT_FAIL;
    goto ret_err;
  }

  /* Validate tech type */
  if ( !TECH_IS_VALID( tech ) ) 
  {
    DS_PROFILE_LOGE( "_begin_transaction: FAIL invalid tech type ", 0 );
    return_status = DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE;
    goto ret_err;
  }

  /* Validate profile number */
  if ((return_status = dsi_profile_validate_profile_num(tech, num, subs_id))
      != DS_PROFILE_REG_RESULT_SUCCESS )
  {
    DS_PROFILE_LOGE( "_begin_transaction: FAIL invalid profile number ", 0 );
    goto ret_err;
  }

  ACQ_LOCK( "_begin_transaction" );
  /*-------------------------------------------------------------------------- 
    Call internal function to allocate handle for transaction
  --------------------------------------------------------------------------*/
  if ((return_status = dsi_profile_alloc_hndl(trn, tech, num, hndl, subs_id))
      != DS_PROFILE_REG_RESULT_SUCCESS)
  {
    DS_PROFILE_LOGE("_begin_transaction: FAIL alloc hndl", 0);
    REL_LOCK("_begin_transaction");
    goto ret_err;
  }

  REL_LOCK("_begin_transaction");
  return DS_PROFILE_REG_RESULT_SUCCESS;

ret_err:
  DS_PROFILE_LOGD("_begin_transaction: EXIT with ERR", 0);
  return return_status;
}

/*===========================================================================
FUNCTION DS_PROFILE_BEGIN_TRANSACTION_PER_SUB

DESCRIPTION
  This returns a Handle that the clients of this software library can use for 
  subsequent Profile operations. The Handle returned is of requested 
  transaction type. All Profile operations using this Handle require that 
  DS_PROFILE_END_TRANSACTION be called at the end. 

PARAMETERS
  trn      : requested transaction type
  tech     : technology type
  num      : profile number
  subs_id  : Profile subscription to identify on which subs_id to reset
  hndl     : pointer to return requested handle
  

DEPENDENCIES 
  
RETURN VALUE 
  DS_PROFILE_REG_RESULT_SUCCESS            : On successful operation
  DS_PROFILE_REG_RESULT_ERR_LIB_NOT_INITED : Library is not initialized
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE : Invalid tech type
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_NUM  :  Invalid profile number 
  DS_PROFILE_REG_RESULT_FAIL               : On general errors. This return 
                                             code provides blanket coverage
SIDE EFFECTS 
 
===========================================================================*/
ds_profile_status_etype ds_profile_begin_transaction_per_sub(
  ds_profile_trn_etype trn,
  ds_profile_tech_etype tech,
  ds_profile_num_type num,
  ds_profile_subs_etype subs_id,
  ds_profile_hndl_type *hndl  
)
{
  ds_profile_status_etype return_status = DS_PROFILE_REG_RESULT_FAIL;

  /* Validate lib state */
  if (!LIB_STATE_IS_VALID(lib_state))
  {
    DS_PROFILE_LOGE("_begin_transaction: FAIL lib not inited ", 0);
    return_status = DS_PROFILE_REG_RESULT_ERR_LIB_NOT_INITED;
    goto ret_err;
  }

  /* Validate transaction */
  if (!TRN_IS_VALID(trn))
  {
    DS_PROFILE_LOGE("_begin_transaction: FAIL invalid transaction type ", 0);
    return_status = DS_PROFILE_REG_RESULT_FAIL;
    goto ret_err;
  }

  /* Validate tech type */
  if (!TECH_IS_VALID(tech))
  {
    DS_PROFILE_LOGE("_begin_transaction: FAIL invalid tech type ", 0);
    return_status = DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE;
    goto ret_err;
  }
  //TODO:BHarath Validate subs_id type everywhere

  /* Validate profile number */
  if ((return_status = dsi_profile_validate_profile_num(tech, num, subs_id))
      != DS_PROFILE_REG_RESULT_SUCCESS)
  {
    DS_PROFILE_LOGE("_begin_transaction: FAIL invalid profile number ", 0);
    goto ret_err;
  }

  ACQ_LOCK("_begin_transaction");
  /*-------------------------------------------------------------------------- 
    Call internal function to allocate handle for transaction
  --------------------------------------------------------------------------*/
  if ((return_status = dsi_profile_alloc_hndl(trn, tech, num, hndl, subs_id))
      != DS_PROFILE_REG_RESULT_SUCCESS)
  {
    DS_PROFILE_LOGE("_begin_transaction: FAIL alloc hndl", 0);
    REL_LOCK("_begin_transaction");
    goto ret_err;
  }

  REL_LOCK("_begin_transaction");
  return DS_PROFILE_REG_RESULT_SUCCESS;

ret_err:
  DS_PROFILE_LOGD("_begin_transaction: EXIT with ERR", 0);
  return return_status;
}

/*===========================================================================
FUNCTION DS_PROFILE_BEGIN_TRANSACTION_EX

DESCRIPTION
  This returns a Handle that the clients of this software library can use for 
  subsequent Profile operations. The Handle returned is of requested 
  transaction type. All Profile operations using this Handle require that 
  DS_PROFILE_END_TRANSACTION be called at the end. 

PARAMETERS
  trn     : requested transaction type
  tech    : technology type
  num     : profile number
  hndl    : pointer to return requested handle
  subs_id : subs_id id of the profile

DEPENDENCIES 
  
RETURN VALUE 
  DS_PROFILE_REG_RESULT_SUCCESS            : On successful operation
  DS_PROFILE_REG_RESULT_ERR_LIB_NOT_INITED : Library is not initialized
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE : Invalid tech type
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_NUM  :  Invalid profile number 
  DS_PROFILE_REG_RESULT_FAIL               : On general errors. This return 
                                             code provides blanket coverage
SIDE EFFECTS 
 
===========================================================================*/
ds_profile_status_etype ds_profile_begin_transaction_ex(
  ds_profile_trn_etype  trn,
  ds_profile_tech_etype tech,
  ds_profile_num_type   num,
  ds_profile_hndl_type  *hndl
)
{
  ds_profile_status_etype return_status = DS_PROFILE_REG_RESULT_FAIL;
  ds_profile_subs_etype subs_id = 
    (ds_profile_subs_etype)ps_sys_get_default_data_subscription();

  /* Validate lib state */
  if (!LIB_STATE_IS_VALID(lib_state))
  {
    DS_PROFILE_LOGE("_begin_transaction: FAIL lib not inited ", 0);
    return_status = DS_PROFILE_REG_RESULT_ERR_LIB_NOT_INITED;
    goto ret_err;
  }

  /* Validate transaction */
  if (!TRN_IS_VALID(trn))
  {
    DS_PROFILE_LOGE("_begin_transaction: FAIL invalid transaction type ", 0);
    return_status = DS_PROFILE_REG_RESULT_FAIL;
    goto ret_err;
  }

  /* Validate tech type */
  if (!TECH_IS_VALID(tech))
  {
    DS_PROFILE_LOGE("_begin_transaction: FAIL invalid tech type ", 0);
    return_status = DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE;
    goto ret_err;
  }

  /* Validate profile number */
  if ((return_status = dsi_profile_validate_profile_num(tech, num, subs_id))
      != DS_PROFILE_REG_RESULT_SUCCESS )
  {
    DS_PROFILE_LOGE( "_begin_transaction: FAIL invalid profile number ", 0 );
    goto ret_err;
  }

  ACQ_LOCK( "_begin_transaction" );
  /*-------------------------------------------------------------------------- 
    Call internal function to allocate handle for transaction
  --------------------------------------------------------------------------*/
  if ( ( return_status = dsi_profile_alloc_hndl( trn, tech, num, hndl,subs_id ) ) 
       !=  DS_PROFILE_REG_RESULT_SUCCESS )
  {
    DS_PROFILE_LOGE("_begin_transaction: FAIL alloc hndl", 0 ); 
    REL_LOCK( "_begin_transaction" );
    goto ret_err;
  }

  REL_LOCK( "_begin_transaction" );
  return DS_PROFILE_REG_RESULT_SUCCESS;

ret_err:
  DS_PROFILE_LOGD( "_begin_transaction: EXIT with ERR", 0 );
  return return_status;
} 

/*===========================================================================
FUNCTION DS_PROFILE_END_TRANSACTION

DESCRIPTION
  This function commits the prefetched modified profile to the persistent 
  storage on the modem. It also invokes cleanup routines for the profile
  handle specified. On return the handle becomes unusable

PARAMETERS
 hndl  : profile handle
 act   : action (commit / cancel)

DEPENDENCIES 
  
RETURN VALUE 
  DS_PROFILE_REG_RESULT_SUCCESS        : On successful operation
  DS_PROFILE_REG_RESULT_ERR_LIB_NOT_INITED : Library is not initialized
  DS_PROFILE_REG_RESULT_ERR_INVAL_HNDL : Invalid handle
  DS_PROFILE_REG_RESULT_FAIL           : On general errors.
SIDE EFFECTS 
  none
===========================================================================*/
ds_profile_status_etype ds_profile_end_transaction (
  ds_profile_hndl_type    profile_hndl,
  ds_profile_action_etype act
)
{
  ds_profile_num_type     profile_num;
  ds_profile_status_etype return_status = DS_PROFILE_REG_RESULT_FAIL;
  
  /* Validate lib state */
  if( !LIB_STATE_IS_VALID( lib_state ) )
  {
    DS_PROFILE_LOGE( "_end_transaction: FAIL lib not inited ", 0 );
    return_status = DS_PROFILE_REG_RESULT_ERR_LIB_NOT_INITED; 
    goto ret_err;
  }

  /* Validate handle */
  if ( !HNDL_IS_VALID( profile_hndl )  ) 
  {
    DS_PROFILE_LOGE( "_end_transaction: INVAL hndl", 0 );
    return_status = DS_PROFILE_REG_RESULT_ERR_INVAL_HNDL;
    goto ret_err;
  }

  /* Lock here */
  ACQ_LOCK( "_end_transaction" );
  if(act == DS_PROFILE_ACTION_COMMIT)
  {
    /*----------------------------------------------------------------------- 
      Call internal function to end transaction and write back the changes
      made on the profile
    ------------------------------------------------------------------------*/
    if ( ( return_status = dsi_profile_end_transaction( profile_hndl ) ) 
       !=  DS_PROFILE_REG_RESULT_SUCCESS )
    {
      /*-------------------------------------------------------------------- 
        If end transaction fails, call internal function to dealloc handle,
        release memory
      ---------------------------------------------------------------------*/
      (void)dsi_profile_dealloc_hndl(&profile_hndl );
      DS_PROFILE_LOGE( "_end_transaction: FAIL internal function", 0 ); 
      REL_LOCK( "_end_transaction" );
      goto ret_err;
    }
  }
  
  profile_num = dsi_profile_get_profile_num_from_handle ( profile_hndl );
  
  /*-------------------------------------------------------------------- 
    Call internal function to dealloc handle, release memory
  ---------------------------------------------------------------------*/
  if ( ( return_status = dsi_profile_dealloc_hndl(&profile_hndl ) ) 
       !=  DS_PROFILE_REG_RESULT_SUCCESS )
  {
    DS_PROFILE_LOGE( "_end_transaction: FAIL dealloc hndl", 0 ); 
    REL_LOCK( "_end_transaction" );
    goto ret_err;
  }
  
  /*-------------------------------------------------------------------- 
    Call operation to modify profile on remote modem
  ---------------------------------------------------------------------*/
  if(ds_profile_modified == TRUE)
  {
    ds_profile_modified = FALSE;
    if(act == DS_PROFILE_ACTION_COMMIT)
    {      
      ds_profile_remote_modify_profile(profile_num);
    }
  }
  
  REL_LOCK( "_end_transaction" );
  return DS_PROFILE_REG_RESULT_SUCCESS;

ret_err:  
  if(ds_profile_modified == TRUE)
  {
    ds_profile_modified = FALSE;
  }

  DS_PROFILE_LOGD( "_end_transaction: EXIT with ERR", 0 );
  return return_status;
} 

/*===========================================================================
FUNCTION DS_PROFILE_SET_PARAM

DESCRIPTION
  This function is used to set profile data element identified by the 
  identifier. The identifiers are specified in the corresponding tech 
  header file. The prefetched copy is modified. end_transaction will
  modify the profile on modem. 

PARAMETERS
  profile_hndl  : handle to profile to set profile data elements
  identifier    : to identify profile data elements
  info          : pointer to value to which data element is to be set
                  (size of buffer passed can atmost be the max size of 
                  the parameter which needs to be set)
DEPENDENCIES 
  
RETURN VALUE 
  DS_PROFILE_REG_RESULT_SUCCESS            : On successful operation
  DS_PROFILE_REG_RESULT_ERR_LIB_NOT_INITED : Library is not initialized
  DS_PROFILE_REG_RESULT_ERR_INVAL_HNDL     : Invalid handle 
  DS_PROFILE_REG_RESULT_ERR_INVAL_IDENT    : Invalid identifier
  DS_PROFILE_REG_RESULT_ERR_LEN_INVALID    : Buffer size more than expected
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE : Invalid tech type 
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_NUM  : Invalid profile number
  DS_PROFILE_REG_RESULT_FAIL               : On general errors 
SIDE EFFECTS 
  none
===========================================================================*/
ds_profile_status_etype ds_profile_set_param (  
  ds_profile_hndl_type        hndl,  
  ds_profile_identifier_type  identifier,
  const ds_profile_info_type *info
)
{
  ds_profile_status_etype return_status = DS_PROFILE_REG_RESULT_FAIL;

  /* Validate lib state */
  if( !LIB_STATE_IS_VALID( lib_state ) )
  {
    DS_PROFILE_LOGE( "_set_param: FAIL lib not inited ", 0 );
    return_status = DS_PROFILE_REG_RESULT_ERR_LIB_NOT_INITED; 
    goto ret_err;
  }

  /* Validate handle */
  if ( (!HNDL_IS_VALID( hndl )) ) 
  {
    DS_PROFILE_LOGE( "_set_param: INVAL hndl", 0 );
    return_status = DS_PROFILE_REG_RESULT_ERR_INVAL_HNDL;
    goto ret_err;
  }

  ACQ_LOCK( "_set_param" );
  /*-------------------------------------------------------------------- 
    Call internal function which depending on tech and identifier
    calls tech-specific set function
  ---------------------------------------------------------------------*/
  if ( ( return_status = dsi_profile_set_param( hndl, identifier, info ) ) 
       !=  DS_PROFILE_REG_RESULT_SUCCESS )
  {
    DS_PROFILE_LOGE("_set_param: FAIL internal set function", 0 );
    REL_LOCK( "_set_param" );
    goto ret_err;
  }

  /*-------------------------------------------------------------------- 
    We want to make sure that everytime profile is modified
    we want to invoke ds_profile remote client to send the modify operation
    to QSC
  ---------------------------------------------------------------------*/  
  ds_profile_modified = TRUE;
  ds_profile_remote_profile_identifier_set(identifier, TRUE);

  REL_LOCK( "_set_param" );
  return DS_PROFILE_REG_RESULT_SUCCESS;

ret_err:
  DS_PROFILE_LOGD( "_set_param: EXIT with ERR", 0 );
  return return_status;
} 

/*===========================================================================
FUNCTION DS_PROFILE_GET_PARAM

DESCRIPTION
  This function is used to get Profile data element identified by the 
  identifier. The identifiers are specified in the corresponding tech 
  header file. The data elements are read from the prefetched Profile and
  info is returned with that value and length.

PARAMETERS
  profile_hndl : handle to profile to get profile data element
  identifier   : to identify profile data element
  info         : pointer to store value and length of data element
                 (size of buffer allocated should atleast be the max size of 
                  the parameter which needs to be fetched)

DEPENDENCIES 
  
RETURN VALUE 
  DS_PROFILE_REG_RESULT_SUCCESS            : On successful operation
  DS_PROFILE_REG_RESULT_ERR_LIB_NOT_INITED : Library is not initialized
  DS_PROFILE_REG_RESULT_ERR_INVAL_HNDL     : Invalid handle 
  DS_PROFILE_REG_RESULT_ERR_INVAL_IDENT    : Invalid identifier
  DS_PROFILE_REG_RESULT_ERR_LEN_INVALID    : Buffer size less than required
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE : Invalid tech type 
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_NUM  : Invalid profile number
  DS_PROFILE_REG_RESULT_FAIL               : On general errors 
SIDE EFFECTS 
 
===========================================================================*/
ds_profile_status_etype ds_profile_get_param (  
  ds_profile_hndl_type        hndl,
  ds_profile_identifier_type  identifier,
  ds_profile_info_type       *info
)
{
  ds_profile_status_etype return_status = DS_PROFILE_REG_RESULT_FAIL;  

  /* Validate lib state */
  if( !LIB_STATE_IS_VALID( lib_state ) )
  {
    DS_PROFILE_LOGE( "_get_param: FAIL lib not inited ", 0 );
    return_status = DS_PROFILE_REG_RESULT_ERR_LIB_NOT_INITED; 
    goto ret_err;
  }
  /* Validate handle */
  if ( !HNDL_IS_VALID( hndl )  ) 
  {
    DS_PROFILE_LOGE( "_get_param: INVAL hndl",0);
    return_status = DS_PROFILE_REG_RESULT_ERR_INVAL_HNDL;
    goto ret_err;
  }

  ACQ_LOCK( "_get_param" );
  /*-------------------------------------------------------------------- 
    Call internal function, depending on tech and identifier
    calls tech-specific get function
  --------------------------------------------------------------------*/
  if ( ( return_status = dsi_profile_get_param( hndl, identifier, info ) ) 
       !=  DS_PROFILE_REG_RESULT_SUCCESS )
  {
    DS_PROFILE_LOGE("_get_param: FAIL internal get function", 0 );
    REL_LOCK( "_get_param" );
    goto ret_err;
  }

  REL_LOCK( "_get_param" );
  return DS_PROFILE_REG_RESULT_SUCCESS;

ret_err:
  DS_PROFILE_LOGD( "_get_param: EXIT with ERR", 0 );
  return return_status;
}

/*===========================================================================
FUNCTION DS_PROFILE_DELETE

DESCRIPTION
  This is used to reset a profile to undefined and return to free pool. Not
  all technology types support this operation.

PARAMETERS
  tech : technology type
  num  : profile number

DEPENDENCIES 
  
RETURN VALUE 
  DS_PROFILE_REG_RESULT_SUCCESS  : On successful operation
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE : Invalid tech type
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_NUM  : Invalid profile number
  DS_PROFILE_REG_RESULT_ERR_LIB_NOT_INITED : Library not initialized
  DS_PROFILE_REG_RESULT_ERR_INVAL_OP : Operation not supported for tech type
  DS_PROFILE_REG_RESULT_FAIL     : On general errors. This return code 
                                   provides blanket coverage
SIDE EFFECTS 
  none
===========================================================================*/
ds_profile_status_etype ds_profile_delete ( 
  ds_profile_tech_etype  tech,
  ds_profile_num_type    num
)
{
  ds_profile_subs_etype subs_id = 
    (ds_profile_subs_etype)ps_sys_get_default_data_subscription();
  return ds_profile_delete_per_sub(tech, num, subs_id);
} 

/*===========================================================================
FUNCTION DS_PROFILE_DELETE_PER_SUB
============================================================================*/
/** @ingroup DS_PROFILE_DELETE_PER_SUB
  Resets a profile to undefined and return to free pool. 

  @note1hang Not all technology types support this operation.
 
  @datatypes
  #ds_profile_tech_etype\n
  #ds_profile_num_type\n
  #boolean\n


  @vertspace
  @param[in] tech                              Technology.
  @param[in] num                               Profile number to be deleted.
  @param[in] enable_deleting_default_profile   Enable deleting default profile
  #param[in] subs_id                           Profile subscription to identify
                                               on which subs_id to reset                           
 
  @return
  DS_PROFILE_REG_RESULT_SUCCESS                -- Operation succeeded.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE -- Invalid technology type.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_NUM  -- Invalid profile number.\n
  DS_PROFILE_REG_RESULT_ERR_LIB_NOT_INITED     -- Library is not initialized.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_OP           -- Operation is not supported  
                                                  for the technology type.\n
  DS_PROFILE_REG_RESULT_FAIL                   -- General failure. 

  @dependencies 
  The software library must be initialized.
  @newpage
*/
ds_profile_status_etype ds_profile_delete_per_sub(
  ds_profile_tech_etype tech,
  ds_profile_num_type num,
  ds_profile_subs_etype subs_id
)
{
  ds_profile_status_etype return_status = DS_PROFILE_REG_RESULT_FAIL;

  /* Validate lib state */
  if (!LIB_STATE_IS_VALID(lib_state))
  {
    DS_PROFILE_LOGE("_delete: FAIL lib not inited ", 0);
    return_status = DS_PROFILE_REG_RESULT_ERR_LIB_NOT_INITED;
    goto ret_err;
  }

  /* Validate tech type */
  if (!TECH_IS_VALID(tech))
  {
    DS_PROFILE_LOGE("_delete: FAIL invalid tech type ", 0);
    return_status = DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE;
    goto ret_err;
  }

  /* Validate profile number */
  if ((return_status = dsi_profile_validate_profile_num(tech, num, subs_id)) !=
        DS_PROFILE_REG_RESULT_SUCCESS)
  {
    DS_PROFILE_LOGE("_delete: FAIL invalid profile number ", 0);
    goto ret_err;
  }
  /*-------------------------------------------------------------------
      Delete the profile under PS critical section in order to avoid 
      a scenario wherein route lookup is happening during call bring-up 
      in PS task for a specific profile and client/application deletes 
      the same profile in DCC task preempting PS task
   ------------------------------------------------------------------*/
  PS_ENTER_CRIT_SECTION(&global_ps_crit_section);

  ACQ_LOCK("_delete");
  /*-------------------------------------------------------------------- 
    Call internal function which depending on tech calls the
    tech specific delete function
  --------------------------------------------------------------------*/
  if ((return_status = dsi_profile_delete(tech, num, subs_id)) !=
       DS_PROFILE_REG_RESULT_SUCCESS )
  {
    DS_PROFILE_LOGE("_delete: FAIL internal delete function", 0 );
    REL_LOCK( "_delete" );
    PS_LEAVE_CRIT_SECTION(&global_ps_crit_section);
    goto ret_err;
  }

  /*-------------------------------------------------------------------- 
    Delete the profile on remote modem
  --------------------------------------------------------------------*/
  if(tech == DS_PROFILE_TECH_3GPP)
  {
    ds_profile_remote_delete_profile ( tech, (uint8)num );
  }

  REL_LOCK( "_delete" );
  PS_LEAVE_CRIT_SECTION(&global_ps_crit_section);

  return DS_PROFILE_REG_RESULT_SUCCESS;

ret_err:
  DS_PROFILE_LOGD( "_delete: EXIT with ERR", 0 );
  return return_status;
} 

/*===========================================================================
FUNCTION DS_PROFILE_CREATE

DESCRIPTION
  This function is used to return a profile number from a pool of free
  profiles. Not all technology types support this operation.

PARAMETERS
  tech  : technology type
  num   : pointer to return profile number of profile created

DEPENDENCIES 
  
RETURN VALUE 
  DS_PROFILE_REG_RESULT_SUCCESS  : On successful operation
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE : Invalid tech type
  DS_PROFILE_REG_RESULT_ERR_LIB_NOT_INITED : Library not initialized
  DS_PROFILE_REG_RESULT_ERR_INVAL_OP : Operation not supported for tech type
  DS_PROFILE_REG_RESULT_FAIL     : On general errors. This return code 
                                   provides blanket coverage
SIDE EFFECTS 
===========================================================================*/
ds_profile_status_etype ds_profile_create
( 
  ds_profile_tech_etype  tech,
  ds_profile_num_type   *num
)
{
  ds_profile_config_type config_ptr;
  config_ptr.subs_id = ps_sys_get_default_data_subscription();
  config_ptr.config_mask |= DS_PROFILE_CONFIG_MASK_SUBS_ID;
  return ds_profile_create_ex(tech, &config_ptr, num);  
}

/*===========================================================================
FUNCTION DS_PROFILE_CREATE_EX

DESCRIPTION
  This function is used to return a profile number from a pool of free
  profiles. Not all technology types support this operation.

PARAMETERS
  tech  : technology type
  config_ptr : config type for persistent property and other profile attributes
  num   : pointer to return profile number of profile created

DEPENDENCIES 
  
RETURN VALUE 
  DS_PROFILE_REG_RESULT_SUCCESS  : On successful operation
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE : Invalid tech type
  DS_PROFILE_REG_RESULT_ERR_LIB_NOT_INITED : Library not initialized
  DS_PROFILE_REG_RESULT_ERR_INVAL_OP : Operation not supported for tech type
  DS_PROFILE_REG_RESULT_FAIL     : On general errors. This return code 
                                   provides blanket coverage
SIDE EFFECTS 
===========================================================================*/
ds_profile_status_etype ds_profile_create_ex
( 
  ds_profile_tech_etype  tech,
  ds_profile_config_type *config_ptr,
  ds_profile_num_type    *num
)
{
  ds_profile_status_etype return_status = DS_PROFILE_REG_RESULT_FAIL;
  ds_profile_config_type local_config_ptr;
  ds_profile_config_type *config_ptr_to_pass = NULL;
  

  if (NULL == config_ptr) 
  {
    DS_PROFILE_LOGD("ds_profile_create_ex: started with tech %d, NULL config",
      tech);
    memset(&local_config_ptr, 0, sizeof(ds_profile_config_type));
    local_config_ptr.subs_id = ps_sys_get_default_data_subscription();
    local_config_ptr.config_mask |= DS_PROFILE_CONFIG_MASK_SUBS_ID;
    config_ptr_to_pass = &local_config_ptr;
  } 
  else 
  {
    if (0 == (config_ptr->config_mask & DS_PROFILE_CONFIG_MASK_SUBS_ID))
    {
      config_ptr->config_mask |= DS_PROFILE_CONFIG_MASK_SUBS_ID;
      config_ptr->subs_id = ps_sys_get_default_data_subscription();
    }
    config_ptr_to_pass = config_ptr;
  }
  

  /* Validate lib state */
  if( !LIB_STATE_IS_VALID( lib_state ) )
  {
    DS_PROFILE_LOGE( "_create: FAIL lib not inited ", 0 );
    return_status = DS_PROFILE_REG_RESULT_ERR_LIB_NOT_INITED; 
    goto ret_err;
  }

  /* Validate tech type */
  if ( !TECH_IS_VALID( tech ) ) 
  {
    DS_PROFILE_LOGE( "_create: FAIL invalid tech type ", 0 );
    return_status = DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE;
    goto ret_err;
  }

  if ( num == NULL)
  {
    return_status = DS_PROFILE_REG_RESULT_ERR_INVAL;
    goto ret_err;
  }

  ACQ_LOCK( "_create" );

  /*-------------------------------------------------------------------- 
    Call internal function which depending on tech calls the
    tech specific create function
  --------------------------------------------------------------------*/
  return_status = dsi_profile_create( tech, config_ptr_to_pass, num );
  if (return_status !=  DS_PROFILE_REG_RESULT_SUCCESS )
  {
    DS_PROFILE_LOGE("_create: FAIL internal delete function", 0 );
    REL_LOCK( "_create" );
    goto ret_err;
  }

  /*-------------------------------------------------------------------- 
    Create profile on remote modem
  --------------------------------------------------------------------*/
  if(tech == DS_PROFILE_TECH_3GPP)
  {
    ds_profile_remote_create_profile ( tech );
  }

  REL_LOCK( "_create" );
  return DS_PROFILE_REG_RESULT_SUCCESS;

ret_err:
  DS_PROFILE_LOGD( "_create: EXIT with ERR", 0 );
  return return_status;
}

/*===========================================================================
FUNCTION DS_PROFILE_RESET_PARAM_TO_INVALID

DESCRIPTION
  This function resets the value of parameter in a profile to default. It
  directly changes the value on modem, so begin/end transaction need not
  be called before/after this function.

PARAMETERS
  tech  : technology type
  num   : profile number
  ident : to identify the profile parameter to be set to default

DEPENDENCIES 
  
RETURN VALUE 
  DS_PROFILE_REG_RESULT_SUCCESS   : On successful operation
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE : Profile type is invalid
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_NUM  : Invalid profile number
  DS_PROFILE_REG_RESULT_ERR_INVAL_OP : Operation not supported for tech type
  DS_PROFILE_REG_RESULT_FAIL      : On general errors. This return 
                                    code provides blanket coverage
SIDE EFFECTS 
  
===========================================================================*/
ds_profile_status_etype ds_profile_reset_param_to_invalid (  
  ds_profile_tech_etype       tech,
  ds_profile_num_type         num,
  ds_profile_identifier_type  ident
)
{
  ds_profile_status_etype return_status = DS_PROFILE_REG_RESULT_FAIL;
  ds_profile_subs_etype subs_id = 
    (ds_profile_subs_etype)ps_sys_get_default_data_subscription();


  /* Validate tech type */
  if (!TECH_IS_VALID(tech))
  {
    DS_PROFILE_LOGE("_reset_param: FAIL invalid tech type ", 0);
    return_status = DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE;
    goto ret_err;
  }

  /* Validate profile number */
  if ((return_status = dsi_profile_validate_profile_num(tech, num, subs_id))
      != DS_PROFILE_REG_RESULT_SUCCESS)
  {
    DS_PROFILE_LOGE("_reset_param: FAIL invalid profile number ", 0);
    goto ret_err;
  }

  ACQ_LOCK("_reset_param");
  /*-------------------------------------------------------------------- 
    Call internal function which depending on tech calls the
    tech specific reset function
  --------------------------------------------------------------------*/
  if ((return_status = dsi_profile_reset_param(tech, num, ident, subs_id))
      != DS_PROFILE_REG_RESULT_SUCCESS)
  {
    DS_PROFILE_LOGE("_reset_param: FAIL internal reset_param function", 0);
    REL_LOCK("_reset_param");
    goto ret_err;
  }

  REL_LOCK("_reset_param");
  return DS_PROFILE_REG_RESULT_SUCCESS;

ret_err:
  DS_PROFILE_LOGD("_reset_param: EXIT with ERR", 0);
  return return_status;
}

/*===========================================================================
FUNCTION DS_PROFILE_RESET_PARAM_TO_INVALID_PER_SUB

DESCRIPTION
  This function resets the value of parameter in a profile to default. It
  directly changes the value on modem, so begin/end transaction need not
  be called before/after this function.

PARAMETERS
  tech    : technology type
  num     : profile number
  ident   : to identify the profile parameter to be set to default
  subs_id : subscription id of the profile

DEPENDENCIES 
  
RETURN VALUE 
  DS_PROFILE_REG_RESULT_SUCCESS   : On successful operation
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE : Profile type is invalid
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_NUM  : Invalid profile number
  DS_PROFILE_REG_RESULT_ERR_INVAL_OP : Operation not supported for tech type
  DS_PROFILE_REG_RESULT_FAIL      : On general errors. This return 
                                    code provides blanket coverage
SIDE EFFECTS 
  
===========================================================================*/
ds_profile_status_etype ds_profile_reset_param_to_invalid_per_sub(
  ds_profile_tech_etype tech,
  ds_profile_num_type num,
  ds_profile_identifier_type ident,
  ds_profile_subs_etype subs_id
)
{
  ds_profile_status_etype return_status = DS_PROFILE_REG_RESULT_FAIL;

  /* Validate tech type */
  if ( !TECH_IS_VALID( tech ) ) 
  {
    DS_PROFILE_LOGE( "_reset_param: FAIL invalid tech type ", 0 );
    return_status = DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE;
    goto ret_err;
  }

  /* Validate profile number */
  if ((return_status = dsi_profile_validate_profile_num(tech, num, subs_id))
      != DS_PROFILE_REG_RESULT_SUCCESS )
  {
    DS_PROFILE_LOGE( "_reset_param: FAIL invalid profile number ", 0 );
    goto ret_err;
  }

  ACQ_LOCK( "_reset_param" );
  /*-------------------------------------------------------------------- 
    Call internal function which depending on tech calls the
    tech specific reset function
  --------------------------------------------------------------------*/
   if ((return_status = dsi_profile_reset_param(tech, num, ident, subs_id))
       !=  DS_PROFILE_REG_RESULT_SUCCESS )
  {
    DS_PROFILE_LOGE("_reset_param: FAIL internal reset_param function", 0 );
    REL_LOCK( "_reset_param" );
    goto ret_err;
  }

  REL_LOCK( "_reset_param" );
  return DS_PROFILE_REG_RESULT_SUCCESS;

ret_err:
  DS_PROFILE_LOGD( "_reset_param: EXIT with ERR", 0 );
  return return_status;
}

/*===========================================================================
FUNCTION DS_PROFILE_RESET_PROFILE_TO_DEFAULT

DESCRIPTION
  This function resets all the parameters of the profile to default. It
  directly changes the value on modem, so begin/end transaction need not
  be called before/after this function.

PARAMETERS
  tech  : technology type
  num   : profile number

DEPENDENCIES 
  
RETURN VALUE 
  DS_PROFILE_REG_RESULT_SUCCESS   : On successful operation
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE : Profile type is invalid
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_NUM  : Invalid profile number
  DS_PROFILE_REG_RESULT_ERR_INVAL_OP : Operation not supported for tech type
  DS_PROFILE_REG_RESULT_FAIL      : On general errors. This return 
                                    code provides blanket coverage
SIDE EFFECTS 
 
===========================================================================*/
ds_profile_status_etype ds_profile_reset_profile_to_default (  
  ds_profile_tech_etype  tech,
  ds_profile_num_type    num
)
{
  ds_profile_status_etype return_status = DS_PROFILE_REG_RESULT_FAIL;
  ds_profile_subs_etype subs_id = 
    (ds_profile_subs_etype)ps_sys_get_default_data_subscription();

  /* Validate tech type */
  if (!TECH_IS_VALID(tech))
  {
    DS_PROFILE_LOGE("_reset_profile_to_default: FAIL invalid tech type ", 0);
    return_status = DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE;
    goto ret_err;
  }

  /* Validate profile number */
  if ((return_status = dsi_profile_validate_profile_num(tech, num, subs_id))
      != DS_PROFILE_REG_RESULT_SUCCESS)
  {
    DS_PROFILE_LOGE("_reset_profile_to_default: FAIL invalid profile number ", 0);
    return_status = DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_NUM;
    goto ret_err;
  }

  ACQ_LOCK("_reset_profile_to_default");
  /*-------------------------------------------------------------------- 
    Call internal function which depending on tech calls the
    tech specific reset_profile function
  --------------------------------------------------------------------*/
  if ((return_status = dsi_profile_reset_profile_to_default(tech, num, subs_id))
      != DS_PROFILE_REG_RESULT_SUCCESS)
  {
    DS_PROFILE_LOGE("_reset_profile_to_default: FAIL internal reset_profile function", 0);
    REL_LOCK("_reset_profile_to_default");
    goto ret_err;
  }

  /*-------------------------------------------------------------------- 
    Reset profile on remote modem
  --------------------------------------------------------------------*/
  ds_profile_remote_reset_profile_to_default((uint8)num);

  REL_LOCK("_reset_profile_to_default");
  return DS_PROFILE_REG_RESULT_SUCCESS;

ret_err:
  DS_PROFILE_LOGD("_reset_profile_to_default: EXIT with ERR", 0);
  return return_status;
}

/*===========================================================================
FUNCTION DS_PROFILE_RESET_PROFILE_TO_DEFAULT_PER_SUB

DESCRIPTION
  This function resets all the parameters of the profile to default. It
  directly changes the value on modem, so begin/end transaction need not
  be called before/after this function.

PARAMETERS
  tech     : technology type
  num      : profile number
  subs_id  : Subscription Id of the profile

DEPENDENCIES 
  
RETURN VALUE 
  DS_PROFILE_REG_RESULT_SUCCESS   : On successful operation
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE : Profile type is invalid
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_NUM  : Invalid profile number
  DS_PROFILE_REG_RESULT_ERR_INVAL_OP : Operation not supported for tech type
  DS_PROFILE_REG_RESULT_FAIL      : On general errors. This return 
                                    code provides blanket coverage
SIDE EFFECTS 
 
===========================================================================*/
ds_profile_status_etype ds_profile_reset_profile_to_default_per_sub(
  ds_profile_tech_etype tech,
  ds_profile_num_type num,
  ds_profile_subs_etype subs_id
)
{
  ds_profile_status_etype return_status = DS_PROFILE_REG_RESULT_FAIL;


  /* Validate tech type */
  if ( !TECH_IS_VALID( tech ) ) 
  {
    DS_PROFILE_LOGE( "_reset_profile_to_default: FAIL invalid tech type ", 0 );
    return_status = DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE;
    goto ret_err;
  }

  /* Validate profile number */
  if ((return_status = dsi_profile_validate_profile_num(tech, num, subs_id))
      != DS_PROFILE_REG_RESULT_SUCCESS )
  {
    DS_PROFILE_LOGE( "_reset_profile_to_default: FAIL invalid profile number ", 0 );
    return_status = DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_NUM;
    goto ret_err;
  }

  ACQ_LOCK( "_reset_profile_to_default" );
  /*-------------------------------------------------------------------- 
    Call internal function which depending on tech calls the
    tech specific reset_profile function
  --------------------------------------------------------------------*/
  if ((return_status = dsi_profile_reset_profile_to_default(tech, num, subs_id))
       !=  DS_PROFILE_REG_RESULT_SUCCESS )
  {
    DS_PROFILE_LOGE("_reset_profile_to_default: FAIL internal reset_profile function", 0 );
    REL_LOCK( "_reset_profile_to_default" );
    goto ret_err;
  }

  /*-------------------------------------------------------------------- 
    Reset profile on remote modem
  --------------------------------------------------------------------*/
  ds_profile_remote_reset_profile_to_default((uint8)num);
  
  REL_LOCK( "_reset_profile_to_default" );
  return DS_PROFILE_REG_RESULT_SUCCESS; 

ret_err:
  DS_PROFILE_LOGD( "_reset_profile_to_default: EXIT with ERR", 0 );
  return return_status;
}

/*===========================================================================
FUNCTION DS_PROFILE_SET_DEFAULT_PROFILE_NUM

DESCRIPTION
  This function sets the given profile number as default profile for the
  family of the specified tech.

PARAMETERS
  tech   : technology type
  family : profile family
  num    : profile number

DEPENDENCIES 
  
RETURN VALUE 
  DS_PROFILE_REG_RESULT_SUCCESS   : On successful operation
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE : Profile type is invalid
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_NUM  : Invalid profile number
  DS_PROFILE_REG_RESULT_ERR_INVAL_OP : Operation not supported for tech type
  DS_PROFILE_REG_RESULT_FAIL      : On general errors. This return 
                                    code provides blanket coverage
SIDE EFFECTS 
 
===========================================================================*/
ds_profile_status_etype ds_profile_set_default_profile_num (  
  ds_profile_tech_etype  tech,
  uint32                 family, 
  ds_profile_num_type    num
)
{
  ds_profile_status_etype return_status = DS_PROFILE_REG_RESULT_FAIL;
  ds_profile_subs_etype subs_id = 
    (ds_profile_subs_etype)ps_sys_get_default_data_subscription();

  /* Validate tech type */
  if ( !TECH_IS_VALID( tech ) ) 
  {
    DS_PROFILE_LOGE( "_set_default_profile_num: FAIL invalid tech type ", 0 );
    return_status = DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE;
    goto ret_err;
  }

  if( !FAMILY_IS_VALID( family ) )
  {
    DS_PROFILE_LOGE( "_set_default_profile_num: FAIL invalid family ", 0 );
    return_status = DS_PROFILE_REG_INVAL_PROFILE_FAMILY;
    goto ret_err;
  }

  /* Validate profile number */
  if ((return_status = dsi_profile_validate_profile_num(tech, num, subs_id))
      != DS_PROFILE_REG_RESULT_SUCCESS )
  {
    DS_PROFILE_LOGE( "_set_default_profile_num: FAIL invalid profile number ", 0 );
    return_status = DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_NUM;
    goto ret_err;
  }

  ACQ_LOCK( "_set_default_profile_num" );
  /*-------------------------------------------------------------------- 
    Call internal function which depending on tech calls the
    tech specific function
  --------------------------------------------------------------------*/
  if ( ( return_status = dsi_profile_set_default_profile( tech, family, num ) ) 
       !=  DS_PROFILE_REG_RESULT_SUCCESS )
  {
    DS_PROFILE_LOGE("_set_default_profile_num: FAIL internal reset_profile function", 0 );
    REL_LOCK( "_set_default_profile_num" );
    goto ret_err;
  }

  REL_LOCK( "_set_default_profile_num" );
  return DS_PROFILE_REG_RESULT_SUCCESS;   

ret_err:
  DS_PROFILE_LOGD( "_set_default_profile_num: EXIT with ERR", 0 );
  return return_status;
}

/*===========================================================================
FUNCTION DS_PROFILE_SET_DEFAULT_PROFILE_NUM_PER_SUBS

DESCRIPTION
  This function calls the internal function to set the given profile number as 
  default profile for the specified tech, family and subscription.

PARAMETERS
  tech    : technology type (3gpp or 3gpp2)
  family  : profile family  (embedded or tethered)
  subs_id : subscription id
  num     : profile number

DEPENDENCIES 
  None

RETURN VALUE 
  DS_PROFILE_REG_RESULT_SUCCESS   : On successful operation
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE : Profile type is invalid
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_NUM  : Invalid profile number
  DS_PROFILE_REG_RESULT_ERR_INVAL_OP : Operation not supported for tech type
  DS_PROFILE_REG_RESULT_FAIL      : On general errors. This return 
                                    code provides blanket coverage
  DS_PROFILE_REG_RESULT_ERR_INVAL_SUBS_ID : Invalid subscription id passed

SIDE EFFECTS
  None
===========================================================================*/
ds_profile_status_etype ds_profile_set_default_profile_num_per_subs (
  ds_profile_tech_etype  tech,
  uint32                 family,
  ds_profile_subs_etype subs_id,
  ds_profile_num_type    num
)
{
  ds_profile_status_etype return_status = DS_PROFILE_REG_RESULT_FAIL;

  /* Validate tech type */
  if( !TECH_IS_VALID( tech ) ) 
  {
    DS_PROFILE_LOGE( "_set_default_profile_num_per_subs: "
                     "FAIL invalid tech type ", 0 );
    return_status = DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE;
    DS_PROFILE_LOGD( "_set_default_profile_num_per_subs: EXIT with ERR", 0 );
    return return_status;
  }

  /* Validate family */
  if( !FAMILY_IS_VALID( family ) )
  {
    DS_PROFILE_LOGE( "_set_default_profile_num: FAIL invalid family", 0 );
    return_status = DS_PROFILE_REG_INVAL_PROFILE_FAMILY;
    DS_PROFILE_LOGD( "_set_default_profile_num_per_subs: EXIT with ERR", 0 );
    return return_status;
  }

  /* Validate subscription id */
  if( (subs_id <= DS_PROFILE_ACTIVE_SUBSCRIPTION_NONE) || 
      (subs_id >= DS_PROFILE_ACTIVE_SUBSCRIPTION_MAX) )
  {
    DS_PROFILE_LOGE( "_set_default_profile_num_per_subs: "
                    "FAIL invalid subs_id id ", 0);
    return_status = DS_PROFILE_REG_RESULT_ERR_INVAL_SUBS_ID;
    DS_PROFILE_LOGD( "_set_default_profile_num_per_subs: EXIT with ERR", 0 );
    return return_status;
  }

  /* Validate profile number */
  if ((return_status = dsi_profile_validate_profile_num(tech, num, subs_id))
      != DS_PROFILE_REG_RESULT_SUCCESS )
  {
    DS_PROFILE_LOGE( "_set_default_profile_num_per_subs: "
                     "FAIL invalid profile number ", 0 );
    return_status = DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_NUM;
    DS_PROFILE_LOGD( "_set_default_profile_num_per_subs: EXIT with ERR", 0 );
    return return_status;
  }

  ACQ_LOCK( "_set_default_profile_num_per_subs" );
  /*-------------------------------------------------------------------- 
    Call internal function which depending on tech calls the
    tech specific function
  --------------------------------------------------------------------*/
  if( ( return_status = 
          dsi_profile_set_default_profile_per_subs( tech, family, subs_id, num ) )
        !=  DS_PROFILE_REG_RESULT_SUCCESS )
  {
    DS_PROFILE_LOGE("_set_default_profile_num_per_subs: "
                    "FAIL internal reset_profile function", 0 );
    REL_LOCK( "_set_default_profile_num_per_subs" );
    DS_PROFILE_LOGD( "_set_default_profile_num_per_subs: EXIT with ERR", 0 );
    return return_status;
  }

  REL_LOCK( "_set_default_profile_num_per_subs" );
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================
FUNCTION DS_PROFILE_GET_DEFAULT_PROFILE_NUM

DESCRIPTION
  This function gets the default profile number for the family of the
  specified tech.

PARAMETERS
  tech   : technology type
  family : profile family
  num    : pointer to store default profile number

DEPENDENCIES 
  
RETURN VALUE 
  DS_PROFILE_REG_RESULT_SUCCESS   : On successful operation
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE : Profile type is invalid
  DS_PROFILE_REG_RESULT_ERR_INVAL_OP : Operation not supported for tech type
  DS_PROFILE_REG_RESULT_FAIL      : On general errors. This return 
                                    code provides blanket coverage
SIDE EFFECTS 
  
===========================================================================*/
ds_profile_status_etype ds_profile_get_default_profile_num (  
  ds_profile_tech_etype   tech,
  uint32                  family, 
  ds_profile_num_type    *num
)
{
  ds_profile_status_etype return_status = DS_PROFILE_REG_RESULT_FAIL;
  /* Validate tech type */
  if ( !TECH_IS_VALID( tech ) ) 
  {
    DS_PROFILE_LOGE( "_get_default_profile_num: FAIL invalid tech type ", 0 );
    return_status = DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE;
    goto ret_err;
  }

  if( !FAMILY_IS_VALID( family ) )
  {
    DS_PROFILE_LOGE( "_set_default_profile_num: FAIL invalid family ", 0 );
    return_status = DS_PROFILE_REG_INVAL_PROFILE_FAMILY;
    goto ret_err;
  }

  if ( num == NULL )
  {
    DS_PROFILE_LOGE( "_get_default_profile_num: FAIL num ptr NULL", 0 );
    return_status = DS_PROFILE_REG_RESULT_ERR_INVAL;
    goto ret_err;
  }

  ACQ_LOCK( "_get_default_profile_num" );
  /*-------------------------------------------------------------------- 
    Call internal function which depending on tech calls the
    tech specific function
  --------------------------------------------------------------------*/
  if ( ( return_status = dsi_profile_get_default_profile( tech, family, num ) ) 
       !=  DS_PROFILE_REG_RESULT_SUCCESS )
  {
    DS_PROFILE_LOGE("_get_default_profile_num: FAIL internal reset_profile function", 0 );
    REL_LOCK( "_get_default_profile_num" );
    goto ret_err;
  }

  REL_LOCK( "_get_default_profile_num" );
  return DS_PROFILE_REG_RESULT_SUCCESS;   

ret_err:
  DS_PROFILE_LOGD( "_get_default_profile_num: EXIT with ERR", 0 );
  return return_status;
}

/*===========================================================================
FUNCTION DS_PROFILE_GET_DEFAULT_PROFILE_NUM_PER_SUBS

DESCRIPTION
  This function calls the internal function to get the default profile number 
  form the specified tech, family and subscription.

PARAMETERS
  tech    : technology type (3gpp or 3gpp2)
  family  : profile family  (embedded or tethered)
  subs_id : subscription id
  num     : pointer to store default profile number

DEPENDENCIES
  None

RETURN VALUE 
  DS_PROFILE_REG_RESULT_SUCCESS   : On successful operation
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE : Profile type is invalid
  DS_PROFILE_REG_RESULT_ERR_INVAL_OP : Operation not supported for tech type
  DS_PROFILE_REG_RESULT_FAIL      : On general errors. This return 
                                    code provides blanket coverage
  DS_PROFILE_REG_RESULT_ERR_INVAL_SUBS_ID : Invalid subscription id passed

SIDE EFFECTS
  None
===========================================================================*/
ds_profile_status_etype ds_profile_get_default_profile_num_per_subs (  
  ds_profile_tech_etype   tech,
  uint32                  family,
  ds_profile_subs_etype  subs_id,
  ds_profile_num_type     *num
)
{
  ds_profile_status_etype return_status = DS_PROFILE_REG_RESULT_FAIL;
  /* Validate tech type */
  if ( !TECH_IS_VALID( tech ) ) 
  {
    DS_PROFILE_LOGE( "_get_default_profile_num_per_subs: "
                     "FAIL invalid tech type ", 0 );
    return_status = DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE;
    DS_PROFILE_LOGD( "_get_default_profile_num_per_subs: EXIT with ERR", 0 );
    return return_status;
  }

  if( !FAMILY_IS_VALID( family ) )
  {
    DS_PROFILE_LOGE( "_set_default_profile_num: FAIL invalid family ", 0 );
    return_status = DS_PROFILE_REG_INVAL_PROFILE_FAMILY;
    DS_PROFILE_LOGD( "_set_default_profile_num_per_subs: EXIT with ERR", 0 );
    return return_status;
  }

  /* Validate subscription id */
  if( (subs_id <= DS_PROFILE_ACTIVE_SUBSCRIPTION_NONE) || 
      (subs_id > DS_PROFILE_ACTIVE_SUBSCRIPTION_MAX) )
  {
    DS_PROFILE_LOGE( "_get_default_profile_num_per_subs: "
                     "FAIL invalid subs id ", 0 );
    return_status = DS_PROFILE_REG_RESULT_ERR_INVAL_SUBS_ID;
    DS_PROFILE_LOGD( "_get_default_profile_num_per_subs: EXIT with ERR", 0 );
    return return_status;
  }

  if ( num == NULL )
  {
    DS_PROFILE_LOGE( "_get_default_profile_num_per_subs: "
                     "FAIL num ptr NULL", 0 );
    return_status = DS_PROFILE_REG_RESULT_ERR_INVAL;
    DS_PROFILE_LOGD( "_get_default_profile_num_per_subs: EXIT with ERR", 0 );
    return return_status;
  }

  ACQ_LOCK( "_get_default_profile_num_per_subs" );
  /*-------------------------------------------------------------------- 
    Call internal function which depending on tech calls the
    tech specific function
  --------------------------------------------------------------------*/
  if ( ( return_status = dsi_profile_get_default_profile_per_subs( 
                             tech, family, subs_id, num ) )
         !=  DS_PROFILE_REG_RESULT_SUCCESS )
  {
    DS_PROFILE_LOGE("_get_default_profile_num_per_subs: "
                    "FAIL internal reset_profile function", 0 );
    REL_LOCK( "_get_default_profile_num_per_subs" );
    DS_PROFILE_LOGD( "_get_default_profile_num_per_subs: EXIT with ERR", 0 );
    return return_status;
  }

  REL_LOCK( "_get_default_profile_num_per_subs" );
  return DS_PROFILE_REG_RESULT_SUCCESS;   
}

/*=========================================================================
FUNCTION DS_PROFILE_GET_MAX_NUM

DESCRIPTION
  This function returns the maximum number of Profiles possible for a 
  given technology type  

PARAMETERS
  tech    : technology type
  max_num : pointer to store maximum number of profiles possible

DEPENDENCIES 
  
RETURN VALUE 
  DS_PROFILE_REG_RESULT_SUCCESS   : On successful operation
  DS_PROFILE_REG_RESULT_ERR_INVALID_PROFILE_TYPE : Profile type is invalid
  DS_PROFILE_REG_RESULT_FAIL      : On general errors. This return 
                                    code provides blanket coverage
SIDE EFFECTS 
  
===========================================================================*/
ds_profile_status_etype ds_profile_get_max_num(
  ds_profile_tech_etype  tech,
  uint32                *max_num
)
{
  uint16 min_num = 0;
  uint16 mx = 0;
  ds_profile_status_etype return_status = DS_PROFILE_REG_RESULT_FAIL;

  if ( max_num == NULL  ) 
  {
    DS_PROFILE_LOGE( "_get_max_num: INVAL max_num ptr", 0 );
    return_status = DS_PROFILE_REG_RESULT_ERR_INVAL;
    goto ret_err;
  }

  /* Validate tech */
  if ( !TECH_IS_VALID( tech ) ) 
  {
    DS_PROFILE_LOGE( "_get_max_num: FAIL invalid tech type ", 0 );
    return_status = DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE;
    goto ret_err;
  }

  ACQ_LOCK( "_get_max_num" );
  /* Call internal function */
  dsi_profile_get_profile_num_range(tech, &min_num, &mx);
  if ( mx == 0 && min_num == 0 )
  {
    REL_LOCK( "_get_max_num" );  
    DS_PROFILE_LOGD( "_get_max_num: EXIT with ERR ",0);
    goto ret_err;
  }
  *max_num = (uint32)mx;

  REL_LOCK( "_get_max_num" );  
  return DS_PROFILE_REG_RESULT_SUCCESS;

ret_err:
  DS_PROFILE_LOGE( "_get_max_num: EXIT with ERR", 0 );
  return return_status;
}

/*===========================================================================
FUNCTION DS_PROFILE_GET_SUPPORTED_TYPE 
 
---NOT EXPOSED AS AN API 
===========================================================================*/
ds_profile_status_etype ds_profile_get_supported_type (  
  uint32                 *num,
  ds_profile_tech_etype  *tech
)
{
  if ( num == NULL || tech == NULL )
  {
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }
  if (dsi_profile_get_supported_type(num, tech) != 
      DS_PROFILE_REG_RESULT_SUCCESS)
  {
    DS_PROFILE_LOGD( "_get_supported_type: EXIT with ERR ", 0 );
    return DS_PROFILE_REG_RESULT_FAIL;
  }
  return DS_PROFILE_REG_RESULT_SUCCESS;
}


/*===========================================================================
FUNCTION DS_PROFILE_GET_LIST_ITR

DESCRIPTION
  This is used to get the list of Profiles of a particular tech type. This 
  function returns an Iterator. The Iterator is traversed using 
  DS_PROFILE_ITR_NEXT. After traversal is complete, the caller is 
  expected to call DS_PROFILE_ITR_DESTROY.

PARAMETERS
  tech  : technology type
  lst   : type of list, (list with all profiles / depending on some search)
  itr   : iterator to traverse through search result

DEPENDENCIES 
  
RETURN VALUE 
  DS_PROFILE_REG_RESULT_SUCCESS  : On successful operation
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE : Invalid tech type
  DS_PROFILE_REG_RESULT_FAIL     : On general errors. This return code 
                                   provides blanket coverage
SIDE EFFECTS 
  none
===========================================================================*/
ds_profile_status_etype ds_profile_get_list_itr ( 
  ds_profile_tech_etype  tech,
  ds_profile_list_type  *lst,
  ds_profile_itr_type   *itr
)
{
  ds_profile_status_etype return_status = DS_PROFILE_REG_RESULT_FAIL;
  ds_profile_subs_etype subs_id = 
    (ds_profile_subs_etype)ps_sys_get_default_data_subscription();

  if (NULL == lst)
  {
    DS_PROFILE_LOGE("_get_list_itr: FAIL NULL lst pointer ", 0);
    return_status = DS_PROFILE_REG_RESULT_ERR_INVAL;
    goto ret_err;
  }

  /* Validate tech type */
  if ( !TECH_IS_VALID( tech ) ) 
  {
    DS_PROFILE_LOGE( "_get_list_itr: FAIL invalid tech type ", 0 );
    return_status = DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE;
    goto ret_err;
  }

  /* Validate list_type and itr_type */
  if ( !LIST_TYPE_IS_VALID( lst ) )
  {
    DS_PROFILE_LOGE( "_get_list_itr: list_type NULL ", 0 );
    return_status = DS_PROFILE_REG_RESULT_ERR_INVAL; 
    goto ret_err;
  }

  /* Validate itr_type */
  if ( !ITR_IS_VALID( itr ) )
  {
    DS_PROFILE_LOGE( "_get_list_itr: itr NULL ", 0 );
    return_status = DS_PROFILE_REG_RESULT_ERR_INVAL; 
    goto ret_err;
  }

  ACQ_LOCK( "_get_list_itr" );
  if ((return_status = dsi_profile_get_list_itr(tech, lst, itr, subs_id))
      != DS_PROFILE_REG_RESULT_SUCCESS)
  {
    REL_LOCK("_get_list_itr");
    DS_PROFILE_LOGE("_get_list_itr: EXIT with ERR  ", 0);
    goto ret_err;
  }

  REL_LOCK("_get_list_itr");
  return return_status;

ret_err:
  DS_PROFILE_LOGD("_get_list_itr: EXIT with ERR", 0);
  return return_status;
}

/*===========================================================================
FUNCTION DS_PROFILE_GET_LIST_ITR_PER_SUB

DESCRIPTION
  This is used to get the list of Profiles of a particular tech type. This 
  function returns an Iterator. The Iterator is traversed using 
  DS_PROFILE_ITR_NEXT. After traversal is complete, the caller is 
  expected to call DS_PROFILE_ITR_DESTROY.

PARAMETERS
  tech     : technology type
  lst      : type of list, (list with all profiles / depending on some search)
  itr      : iterator to traverse through search result
  subs_id  : Subscription ID for which the list is required

DEPENDENCIES 
  
RETURN VALUE 
  DS_PROFILE_REG_RESULT_SUCCESS  : On successful operation
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE : Invalid tech type
  DS_PROFILE_REG_RESULT_FAIL     : On general errors. This return code 
                                   provides blanket coverage
SIDE EFFECTS 
  none
===========================================================================*/
ds_profile_status_etype ds_profile_get_list_itr_per_sub(
  ds_profile_tech_etype tech,
  ds_profile_list_type *lst,
  ds_profile_itr_type *itr,
  ds_profile_subs_etype subs_id
)
{
  ds_profile_status_etype return_status = DS_PROFILE_REG_RESULT_FAIL;

  if (NULL == lst)
  {
    DS_PROFILE_LOGE( "_get_list_itr: FAIL NULL lst pointer ", 0 );
    return_status = DS_PROFILE_REG_RESULT_ERR_INVAL;
    goto ret_err;
  }

  /* Validate tech type */
  if ( !TECH_IS_VALID( tech ) ) 
  {
    DS_PROFILE_LOGE( "_get_list_itr: FAIL invalid tech type ", 0 );
    return_status = DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE;
    goto ret_err;
  }

  /* Validate list_type and itr_type */
  if ( !LIST_TYPE_IS_VALID( lst ) )
  {
    DS_PROFILE_LOGE( "_get_list_itr: list_type NULL ", 0 );
    return_status = DS_PROFILE_REG_RESULT_ERR_INVAL; 
    goto ret_err;
  }

  /* Validate itr_type */
  if ( !ITR_IS_VALID( itr ) )
  {
    DS_PROFILE_LOGE( "_get_list_itr: itr NULL ", 0 );
    return_status = DS_PROFILE_REG_RESULT_ERR_INVAL; 
    goto ret_err;
  }

  ACQ_LOCK( "_get_list_itr" );
  if ((return_status = dsi_profile_get_list_itr(tech, lst, itr, subs_id))
      != DS_PROFILE_REG_RESULT_SUCCESS)
  {
    REL_LOCK( "_get_list_itr" );
    DS_PROFILE_LOGE( "_get_list_itr: EXIT with ERR  ", 0 ); 
    goto ret_err;
  }

  REL_LOCK( "_get_list_itr" );
  return return_status;

ret_err:
  DS_PROFILE_LOGD( "_get_list_itr: EXIT with ERR", 0 );
  return return_status;
}

/*===========================================================================
FUNCTION DS_PROFILE_ITR_NEXT

DESCRIPTION
  This routine advances the Iterator to the next element.  

PARAMETERS
  itr : iterator

DEPENDENCIES 
  
RETURN VALUE 
  DS_PROFILE_REG_RESULT_SUCCESS   : On successful operation
  DS_PROFILE_REG_RESULT_FAIL      : On general errors. This return code 
                                    provides blanket coverage
  DS_PROFILE_REG_RESULT_ERR_INVAL : Invalid argument (iterator)
  DS_PROFILE_REG_RESULT_LIST_END  : End of list
SIDE EFFECTS 
  none
===========================================================================*/
ds_profile_status_etype ds_profile_itr_next (  
  ds_profile_itr_type   itr
)
{
  ds_profile_status_etype return_status = DS_PROFILE_REG_RESULT_FAIL;

  /* Validate itr_type */
  if ( !ITR_IS_VALID( itr ) )
  {
    DS_PROFILE_LOGE( "_itr_next: itr NULL ", 0 );
    return_status = DS_PROFILE_REG_RESULT_ERR_INVAL; 
    goto ret_err;
  }

  ACQ_LOCK( "_itr_next" );
  if ( (return_status = dsi_profile_itr_next( itr ) )
      != DS_PROFILE_REG_RESULT_SUCCESS)
  {
    REL_LOCK( "_itr_next" );
    DS_PROFILE_LOGE( "_itr_next: EXIT with ERR ", 0 ); 
    goto ret_err;
  }

  REL_LOCK( "_itr_next" );
  return return_status;

ret_err:
  DS_PROFILE_LOGD( "_itr_next: EXIT with ERR", 0 );
  return return_status;
}

/*===========================================================================
FUNCTION DS_PROFILE_ITR_FIRST

DESCRIPTION
  This routine resets the Iterator to the beginning of the list.  

PARAMETERS
  itr : iterator

DEPENDENCIES 
  
RETURN VALUE 
  DS_PROFILE_REG_RESULT_SUCCESS   : On successful operation
  DS_PROFILE_REG_RESULT_FAIL      : On general errors. This return code 
                                    provides blanket coverage
  DS_PROFILE_REG_RESULT_ERR_INVAL : Invalid argument (iterator)
SIDE EFFECTS 
  none
===========================================================================*/
ds_profile_status_etype ds_profile_itr_first ( 
  ds_profile_itr_type   itr
)
{
  ds_profile_status_etype return_status = DS_PROFILE_REG_RESULT_FAIL;

  /* Validate itr_type */
  if ( !ITR_IS_VALID( itr ) )
  {
    DS_PROFILE_LOGE( "_itr_first: itr NULL ", 0 );
    return_status = DS_PROFILE_REG_RESULT_ERR_INVAL; 
    goto ret_err;
  }

  ACQ_LOCK( "_itr_first" );

  if ( (return_status = dsi_profile_itr_first( itr ) )
      != DS_PROFILE_REG_RESULT_SUCCESS)
  {
    REL_LOCK( "_itr_first" );
    DS_PROFILE_LOGE( "_itr_first: EXIT with ERR", 0 ); 
    goto ret_err;
  }

  REL_LOCK( "_itr_first" );
  return return_status;

ret_err:
  DS_PROFILE_LOGD( "_itr_first: EXIT with ERR", 0 );
  return return_status;
}

/*===========================================================================
FUNCTION DS_PROFILE_GET_INFO_BY_ITR

DESCRIPTION
  This routine gets info stored in that Iterator node.  

PARAMETERS
  itr       : iterator
  list_info : pointer to structure to return profile info

DEPENDENCIES 
  
RETURN VALUE 
  DS_PROFILE_REG_RESULT_SUCCESS   : On successful operation
  DS_PROFILE_REG_RESULT_ERR_INVAL : Invalid argument (iterator)
  DS_PROFILE_REG_RESULT_FAIL      : On general errors. This return code 
                                    provides blanket coverage
SIDE EFFECTS 
  none
===========================================================================*/
ds_profile_status_etype ds_profile_get_info_by_itr ( 
  ds_profile_itr_type         itr,
  ds_profile_list_info_type  *list_info
)
{
  ds_profile_status_etype return_status = DS_PROFILE_REG_RESULT_FAIL;

  /* Validate itr_type */
  if ( !ITR_IS_VALID( itr ) )
  {
    DS_PROFILE_LOGE( "_get_info_by_itr: itr NULL ", 0 );
    return_status = DS_PROFILE_REG_RESULT_ERR_INVAL; 
    goto ret_err;
  }

  if ( list_info == NULL)
  {
    DS_PROFILE_LOGE( "_get_info_by_itr: list_info ptr NULL", 0 ); 
    return_status = DS_PROFILE_REG_RESULT_ERR_INVAL;
    goto ret_err;
  }

  ACQ_LOCK( "_get_info_by_itr" );
  if ( (return_status = dsi_profile_get_info_by_itr( itr, list_info ) )
      != DS_PROFILE_REG_RESULT_SUCCESS)
  {
    REL_LOCK( "_get_info_by_itr" );
    goto ret_err;
  }

  REL_LOCK( "_get_info_by_itr" );
  return return_status;

ret_err:
  DS_PROFILE_LOGD( "_get_info_by_itr: EXIT with ERR", 0 );
  return return_status;
}

/*===========================================================================
FUNCTION DS_PROFILE_ITR_DESTROY

DESCRIPTION
  This routine destroys the Iterator  

PARAMETERS
  itr : iterator

DEPENDENCIES 
  
RETURN VALUE 
  DS_PROFILE_REG_RESULT_SUCCESS   : On successful operation
  DS_PROFILE_REG_RESULT_FAIL      : On general errors. This return code 
                                    provides blanket coverage
  DS_PROFILE_REG_RESULT_ERR_INVAL : Invalid argument (iterator)
SIDE EFFECTS 
  none
===========================================================================*/
ds_profile_status_etype ds_profile_itr_destroy ( 
  ds_profile_itr_type   itr
)
{
  ds_profile_status_etype return_status = DS_PROFILE_REG_RESULT_FAIL;

  /* Validate itr_type */
  if ( !ITR_IS_VALID( itr ) )
  {
    DS_PROFILE_LOGE( "_itr_destroy: itr NULL ", 0 );
    return_status = DS_PROFILE_REG_RESULT_ERR_INVAL; 
    goto ret_err;
  }

  ACQ_LOCK( "_itr_destroy" );
  if ( (return_status = dsi_profile_itr_destroy( itr ) )
      != DS_PROFILE_REG_RESULT_SUCCESS)
  {
    REL_LOCK( "_itr_destroy" );
    goto ret_err;
  }

  REL_LOCK( "_itr_destroy" );
  return return_status;

ret_err:
  DS_PROFILE_LOGD( "_itr_destroy: EXIT with ERR", 0 );
  return return_status;
}

#ifdef FEATURE_DATA_LTE
/*===========================================================================
FUNCTION DS_PROFILE_UPDATE_LTE_ATTACH_PDN_LIST_PROFILES

DESCRIPTION
  This function updates the profile parameters in the lte attach pdn
  list of the specified technology

PARAMETERS
  tech  : technology type

DEPENDENCIES 
  
RETURN VALUE 
  DS_PROFILE_REG_RESULT_SUCCESS   : On successful operation
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE : Profile type is invalid
  DS_PROFILE_REG_RESULT_ERR_INVAL_OP : Operation not supported for tech type
  DS_PROFILE_REG_RESULT_FAIL      : On general errors. This return 
                                    code provides blanket coverage
SIDE EFFECTS 
  
===========================================================================*/
ds_profile_status_etype ds_profile_update_lte_attach_pdn_list_profiles (  
  ds_profile_tech_etype       tech
)
{
  ds_profile_status_etype return_status = DS_PROFILE_REG_RESULT_FAIL;
  ds_profile_subs_etype subs_id = 
    (ds_profile_subs_etype)ps_sys_get_default_data_subscription();
  /* Validate lib state */
  if (!LIB_STATE_IS_VALID(lib_state))
  {
    DS_PROFILE_LOGE("_update_lte_profiles: FAIL lib not inited ", 0);
    return_status = DS_PROFILE_REG_RESULT_ERR_LIB_NOT_INITED;
    goto ret_err;
  }

  /* Validate tech type */
  if (!TECH_IS_VALID(tech))
  {
    DS_PROFILE_LOGE("_update_lte_profiles: FAIL invalid tech type ", 0);
    return_status = DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE;
    goto ret_err;
  }

  ACQ_LOCK("_update_lte_profiles");

  /*-------------------------------------------------------------------- 
    Call internal function which depending on tech calls the
    tech specific create function
  --------------------------------------------------------------------*/
  return_status = dsi_profile_update_lte_attach_pdn_list_profiles(tech, subs_id);
  if (return_status != DS_PROFILE_REG_RESULT_SUCCESS)
  {
    DS_PROFILE_LOGE("_update_lte_profiles: FAIL internal delete function", 0);
    REL_LOCK("_update_lte_profiles");
    goto ret_err;
  }

  REL_LOCK("_update_lte_profiles");
  return DS_PROFILE_REG_RESULT_SUCCESS;

ret_err:
  DS_PROFILE_LOGD("_update_lte_profiles: EXIT with ERR", 0);
  return return_status;
}
/*lint -restore Restore lint error 655*/


/*===========================================================================
FUNCTION DS_PROFILE_UPDATE_LTE_ATTACH_PDN_LIST_PROFILES_PER_SUB

DESCRIPTION
  This function updates the profile parameters in the lte attach pdn
  list of the specified technology

PARAMETERS
  tech     : technology type
  subs_id  : subscription id

DEPENDENCIES 
  
RETURN VALUE 
  DS_PROFILE_REG_RESULT_SUCCESS   : On successful operation
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE : Profile type is invalid
  DS_PROFILE_REG_RESULT_ERR_INVAL_OP : Operation not supported for tech type
  DS_PROFILE_REG_RESULT_FAIL      : On general errors. This return 
                                    code provides blanket coverage
SIDE EFFECTS 
  
===========================================================================*/
ds_profile_status_etype ds_profile_update_lte_attach_pdn_list_profiles_per_sub(
  ds_profile_tech_etype tech,
  ds_profile_subs_etype subs_id
)
{
  ds_profile_status_etype return_status = DS_PROFILE_REG_RESULT_FAIL;

  /* Validate lib state */
  if( !LIB_STATE_IS_VALID( lib_state ) )
  {
    DS_PROFILE_LOGE( "_update_lte_profiles: FAIL lib not inited ", 0 );
    return_status = DS_PROFILE_REG_RESULT_ERR_LIB_NOT_INITED; 
    goto ret_err;
  }

  /* Validate tech type */
  if ( !TECH_IS_VALID( tech ) ) 
  {
    DS_PROFILE_LOGE( "_update_lte_profiles: FAIL invalid tech type ", 0 );
    return_status = DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE;
    goto ret_err;
  }

  ACQ_LOCK( "_update_lte_profiles" );

  /*-------------------------------------------------------------------- 
    Call internal function which depending on tech calls the
    tech specific create function
  --------------------------------------------------------------------*/
  return_status = dsi_profile_update_lte_attach_pdn_list_profiles(tech, subs_id);
  if (return_status !=  DS_PROFILE_REG_RESULT_SUCCESS )
  {
    DS_PROFILE_LOGE("_update_lte_profiles: FAIL internal delete function", 0 );
    REL_LOCK( "_update_lte_profiles" );
    goto ret_err;
  }

  REL_LOCK( "_update_lte_profiles" );
  return DS_PROFILE_REG_RESULT_SUCCESS;

ret_err:
  DS_PROFILE_LOGD( "_update_lte_profiles: EXIT with ERR", 0 );
  return return_status;
}
#endif /* FEATURE_DATA_LTE */
/*lint -restore Restore lint error 655*/


