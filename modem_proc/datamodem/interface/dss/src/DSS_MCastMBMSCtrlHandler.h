#ifndef __DSS_MCASTMBMSCTRLHANDLER_H__
#define __DSS_MCASTMBMSCTRLHANDLER_H__
#ifdef FEATUTE_DATA_PS_MCAST
/*====================================================

FILE:  DSS_MCastMBMSCtrlHandler.h

SERVICES:
   Handle network extended IP config events.

=====================================================

Copyright (c) 2008 Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary

=====================================================*/
/*===========================================================================
  EDIT HISTORY FOR MODULE

  Please notice that the changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/dss/src/DSS_MCastMBMSCtrlHandler.h#1 $
  $DateTime: 2015/01/27 06:42:19 $$Author: mplp4svc $

  when       who what, where, why
  ---------- --- ------------------------------------------------------------
  2010-04-18 en  History added.

===========================================================================*/

#include "DSS_EventHandlerMCastMBMSCtrl.h"

class DSSMCastMBMSCtrlHandler : public DSSEventHandlerMCastMBMSCtrl
{
protected:

   virtual void EventOccurred();
   virtual AEEResult RegisterIDL();

public:

   DSSMCastMBMSCtrlHandler();
   static DSSMCastMBMSCtrlHandler* CreateInstance();
};
#endif // FEATUTE_DATA_PS_MCAST
#endif // __DSS_MCASTMBMSCTRLHANDLER_H__
