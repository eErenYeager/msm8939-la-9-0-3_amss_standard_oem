#ifndef __DSS_BEARERTECHEXHANDLER_H__
#define __DSS_BEARERTECHEXHANDLER_H__

/*====================================================

FILE:  DSS_BearerTechExHandler.h

SERVICES:
   Handle network bearer technology extended changed events.

=====================================================

Copyright (c) 2013 Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary

=====================================================*/
/*===========================================================================
  EDIT HISTORY FOR MODULE

  Please notice that the changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/dss/src/DSS_BearerTechExHandler.h#1 $
  $DateTime: 2015/01/27 06:42:19 $
  $Author: mplp4svc $

  when       who what, where, why
  ---------- --- ------------------------------------------------------------
  2013-02-13 da  Created module.

===========================================================================*/

#include "DSS_EventHandlerNetApp.h"

class DSSBearerTechExHandler : public DSSEventHandlerNetApp
{
protected:
   virtual void EventOccurred();
   virtual AEEResult RegisterIDL();

public:
   static DSSBearerTechExHandler* CreateInstance();
   DSSBearerTechExHandler();
};

#endif // __DSS_BEARERTECHEXHANDLER_H__
