/**
@file ps_sys.h
@brief
This file defines function, variables and data structures common to all PS 
System API module 
*/

#ifndef PS_SYS_H
#define PS_SYS_H
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                        P S _ S Y S . H


GENERAL DESCRIPTION
  This file defines function, variables and data structures common to all
  PS System API module

Copyright (c) 2011-2014 Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/sysapi/inc/ps_sys.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $
 
when        who    what, where, why
--------    ---    ---------------------------------------------------------- 
03/29/13    svj    New APIs to modify and query attach pdn list 
09/09/11    bvd    Created module 
 
===========================================================================*/


/*===========================================================================

                          INCLUDE FILES FOR MODULE

===========================================================================*/
#include "comdef.h"
#include "customer.h"
#include "queue.h"
#include "ds_sys.h"
#include "ps_in.h"

/**
  RAT Mask for 3GPP
*/
#define PS_SYS_RAT_3GPP_WCDMA                DS_SYS_RAT_3GPP_WCDMA
#define PS_SYS_RAT_3GPP_GPRS                 DS_SYS_RAT_3GPP_GPRS
#define PS_SYS_RAT_3GPP_HSDPA                DS_SYS_RAT_3GPP_HSDPA
#define PS_SYS_RAT_3GPP_HSUPA                DS_SYS_RAT_3GPP_HSUPA
#define PS_SYS_RAT_3GPP_EDGE                 DS_SYS_RAT_3GPP_EDGE
#define PS_SYS_RAT_3GPP_LTE                  DS_SYS_RAT_3GPP_LTE
#define PS_SYS_RAT_3GPP_HSDPAPLUS            DS_SYS_RAT_3GPP_HSDPAPLUS
#define PS_SYS_RAT_3GPP_DC_HSDPAPLUS         DS_SYS_RAT_3GPP_DC_HSDPAPLUS
#define PS_SYS_RAT_3GPP_64_QAM               DS_SYS_RAT_3GPP_64_QAM
#define PS_SYS_RAT_3GPP_TDSCDMA              DS_SYS_RAT_3GPP_TDSCDMA
#define PS_SYS_RAT_3GPP_NULL_BEARER          DS_SYS_RAT_3GPP_NULL_BEARER

/**
  RAT Mask for 3GPP2
*/
#define PS_SYS_RAT_3GPP2_1X               DS_SYS_RAT_3GPP2_1X
#define PS_SYS_RAT_3GPP2_EVDO_REV0        DS_SYS_RAT_3GPP2_EVDO_REV0
#define PS_SYS_RAT_3GPP2_EVDO_REVA        DS_SYS_RAT_3GPP2_EVDO_REVA
#define PS_SYS_RAT_3GPP2_EVDO_REVB        DS_SYS_RAT_3GPP2_EVDO_REVB
#define PS_SYS_RAT_3GPP2_EHRPD            DS_SYS_RAT_3GPP2_EHRPD
#define PS_SYS_RAT_3GPP2_FMC              DS_SYS_RAT_3GPP2_FMC
#define PS_SYS_RAT_3GPP2_NULL_BEARER      DS_SYS_RAT_3GPP2_NULL_BEARER

/**
  SO Mask for 1x
*/
#define PS_SYS_SO_3GPP2_1X_IS95                DS_SYS_SO_3GPP2_1X_IS95
#define PS_SYS_SO_3GPP2_1X_IS2000              DS_SYS_SO_3GPP2_1X_IS2000
#define PS_SYS_SO_3GPP2_1X_IS2000_REL_A        DS_SYS_SO_3GPP2_1X_IS2000_REL_A

/**
  SO Mask for DO. 
   
  The following table gives which of the following so_masks are supported by 
  which technology 
   
  Rev0 supports @li DS_SYS_SO_3GPP2_EVDO_DPA 
   
  RevA supports @li DS_SYS_SO_3GPP2_EVDO_DPA 
                @li DS_SYS_SO_3GPP2_EVDO_MFPA
                @li DS_SYS_SO_3GPP2_EVDO_EMPA
                @li DS_SYS_SO_3GPP2_EVDO_EMPA_EHRPD
   
  RevB supports @li DS_SYS_SO_3GPP2_EVDO_DPA 
                @li DS_SYS_SO_3GPP2_EVDO_MFPA
                @li DS_SYS_SO_3GPP2_EVDO_EMPA
                @li DS_SYS_SO_3GPP2_EVDO_EMPA_EHRPD
                @li DS_SYS_SO_3GPP2_EVDO_MMPA
                @li DS_SYS_SO_3GPP2_EVDO_MMPA_EHRPD
 */
#define PS_SYS_SO_3GPP2_EVDO_DPA           DS_SYS_SO_3GPP2_EVDO_DPA
#define PS_SYS_SO_3GPP2_EVDO_MFPA          DS_SYS_SO_3GPP2_EVDO_MFPA
#define PS_SYS_SO_3GPP2_EVDO_EMPA          DS_SYS_SO_3GPP2_EVDO_EMPA
#define PS_SYS_SO_3GPP2_EVDO_EMPA_EHRPD    DS_SYS_SO_3GPP2_EVDO_EMPA_EHRPD
#define PS_SYS_SO_3GPP2_EVDO_MMPA          DS_SYS_SO_3GPP2_EVDO_MMPA
#define PS_SYS_SO_3GPP2_EVDO_MMPA_EHRPD    DS_SYS_SO_3GPP2_EVDO_MMPA_EHRPD


/**
   Defines the MAX length for PDN/APN name string
*/
#define PS_SYS_MAX_APN_LEN        DS_SYS_MAX_APN_LEN
#define PS_SYS_MAX_APNS           DS_SYS_MAX_APNS /**< Max number of APNs */
#define PS_SYS_MAX_BEARER         DS_SYS_MAX_BEARER
#define PS_SYS_LTE_ATTACH_PDN_PROFILE_LIST_MAX DS_SYS_LTE_ATTACH_PDN_PROFILE_LIST_MAX  
/**< Max number of LTE Attach PDN profile IDs */

#define PS_SYS_MAX_AVAIL_SYS      DS_SYS_MAX_AVAIL_SYS
#define PS_SYS_PREF_SYS_INDEX_INVALID DS_SYS_PREF_SYS_INDEX_INVALID
/**< If number of available system is zero then there are no available 
system on which UE can make the call. In such cases, preferred system index
would be set to  PS_SYS_PREF_SYS_INDEX_INVALID. If any APN's preferred system
is not available then its preferred system index field would be set to 
PS_SYS_PREF_SYS_INDEX_INVALID. */

#define PS_SYS_MAC_ADDR_LEN       DS_SYS_MAC_ADDR_LEN

#define PS_SYS_MAX_NUM_THROUGHPUT_INFO    DS_SYS_MAX_NUM_THROUGHPUT_INFO
#define PS_SYS_MAX_SSID_LEN       DS_SYS_MAX_SSID_LEN

/* For low tier architecture like triton, only 6 pdns are supported */
#ifdef FEATURE_DATA_MPSS_ULT
  #define PS_SYS_MAX_AVAIL_PDNS 6
#else 
  #define PS_SYS_MAX_AVAIL_PDNS 8
#endif /* FEATURE_DATA_MPSS_ULT */

#ifdef __cplusplus
extern "C" {
#endif

/*===========================================================================

                         PUBLIC DATA DECLARATIONS

===========================================================================*/

/**
  @brief Defines different Technology type values
*/
typedef enum
{
  PS_SYS_TECH_MIN     = DS_SYS_TECH_MIN,
  PS_SYS_TECH_3GPP    = DS_SYS_TECH_3GPP,  /**< Tech type 3gpp */
  PS_SYS_TECH_3GPP2   = DS_SYS_TECH_3GPP2, /**< Tech type 3gpp2 */
  PS_SYS_TECH_WLAN    = DS_SYS_TECH_WLAN,  /**< Tech type WLAN */
  PS_SYS_TECH_ALL     = DS_SYS_TECH_ALL,   /**< All the tech types */
  PS_SYS_TECH_MAX     = DS_SYS_TECH_MAX,
  PS_SYS_TECH_INVALID = DS_SYS_TECH_INVALID
}ps_sys_tech_enum_type;


/**
  @brief This is the type that is used to define different network types
*/
typedef enum
{
  PS_SYS_NETWORK_3GPP,  /**< Network type 3gpp */
  PS_SYS_NETWORK_3GPP2, /**< Network type 3gpp2 */
  PS_SYS_NETWORK_WLAN,  /**< Network type WLAN */
  PS_SYS_NETWORK_MAX,
} ps_sys_network_enum_type;


/**
  @brief This is the type that is used to define core network details 
  @see   ds_sys_network_info_type 
*/
typedef struct
{
  uint32   rat_mask;   /**< RAT Mask */
  uint32   so_mask;    /**< SO Mask */
} ps_sys_network_info_type;

/**
  @brief This is the type that is used to define system status. It is 
         common to both configuration PS_SYS_CONF_SYSTEM_STATUS and event
         PS_SYS_EVENT_SYSTEM_STATUS_CHANGE
   
  @see PS_SYS_CONF_SYSTEM_STATUS
  @see PS_SYS_EVENT_SYSTEM_STATUS_CHANGE 
  @see ds_sys_system_status_type 
*/
typedef struct
{
  ps_sys_network_enum_type        preferred_network; /**< pref network value*/
  ps_sys_network_info_type        network_info[PS_SYS_NETWORK_MAX];
  /**< Array of rat and SO mask for all networks */
} ps_sys_system_status_type;

/**
  @brief This is the type that is used to define WQE status
*/
typedef enum
{
  PS_SYS_WLAN_WQE_INACTIVE     = 0x1,
  PS_SYS_WLAN_WQE_ACTIVE       = 0x2
} ps_sys_wlan_wqe_status_enum_type;

/**
  @brief This is the type that is used for 
         PS_SYS_CONF_WLAN_STATUS configuration and
         PS_SYS_EVENT_WLAN_STATUS_CHANGE
   
  @see PS_SYS_CONF_WLAN_STATUS
  @see PS_SYS_EVENT_WLAN_STATUS_CHANGE 
*/

typedef struct
{
  boolean                          is_wlan_available; /**< WLAN availability*/
  ps_ip_addr_type                  ipv4_addr;  /**< WLAN IPV4 address */
  ps_ip_addr_type                  ipv6_addr;  /**< WLAN IPV6 address */
  uint8                            ipv6_prefix_len;  /**< IPV6 addr prefix length */
  uint8                            ap_mac_addr[PS_SYS_MAC_ADDR_LEN]; /**< WLAN Access Point MAC addr*/
  ps_sys_wlan_wqe_status_enum_type wqe_status;  /**< Whether Wifi Quality Estimation was performed on the Apps Proc*/
  ps_ip_addr_type                  dns_ipv4_addr1;  /**< DNS IPV4 address 1 */
  ps_ip_addr_type                  dns_ipv4_addr2;  /**< DNS IPV4 address 2 */
  ps_ip_addr_type                  dns_ipv6_addr1;  /**< DNS IPV6 address 1 */
  ps_ip_addr_type                  dns_ipv6_addr2;  /**< DNS IPV6 address 2 */
  ps_ip_addr_type                  epdg_ipv4_addr1;  /**< EPDG IPV4 address 1 */
  ps_ip_addr_type                  epdg_ipv4_addr2;  /**< EPDG IPV4 address 2 */
  ps_ip_addr_type                  epdg_ipv6_addr1;  /**< EPDG IPV6 address 1 */
  ps_ip_addr_type                  epdg_ipv6_addr2;  /**< EPDG IPV6 address 2 */
  char                             ssid[PS_SYS_MAX_SSID_LEN]; /**< SSID */
} ps_sys_wlan_status_type;

/**
  @brief This is the type that is used for 
         PS_SYS_CONF_GET_WLAN_MAC_ADDR configuration and
         PS_SYS_EVENT_WLAN_MAC_ADDR_CHANGE
   
  @see PS_SYS_CONF_GET_WLAN_MAC_ADDR
  @see PS_SYS_EVENT_WLAN_MAC_ADDR_CHANGE 
*/
typedef struct
{
  uint8            ap_mac_addr[DS_SYS_MAC_ADDR_LEN]; 
  /**< WLAN Access Point MAC addr*/
} ps_sys_wlan_mac_addr_type;

/**
  @brief This is the type that is used for 
         PS_SYS_CONF_GET_WLAN_SSID configuration and
         PS_SYS_EVENT_WLAN_SSID_CHANGE
   
  @see PS_SYS_CONF_GET_WLAN_SSID
  @see PS_SYS_EVENT_WLAN_SSID_CHANGE 
*/
typedef struct
{
  char             ssid[PS_SYS_MAX_SSID_LEN]; 
  /**< WLAN Service Set Identifier*/
} ps_sys_wlan_ssid_type;

/**
  @brief This is the type that is used for 
         PS_SYS_CONF_WLAN_PREFERENCE configuration and
         PS_SYS_EVENT_WLAN_PREFERENCE_CHANGE
   
  @see PS_SYS_CONF_WLAN_PREFERENCE
  @see PS_SYS_EVENT_WLAN_PREFERENCE_CHANGE 
*/
typedef enum
{
  PS_SYS_WLAN_LB_PREFERRED     = 0x00,
  PS_SYS_WLAN_LB_NOT_PREFERRED = 0x01
} ps_sys_wlan_preference_enum_type;

/**
  @brief Defines structure for PS_SYS_EVENT_WLAN_MAX_CONN event
  
  @see PS_SYS_EVENT_WLAN_MAX_CONN 
*/
typedef struct
{
  boolean is_max_conn_reached; 
  uint8   num_apns;
  char    active_apn_name[PS_SYS_MAX_AVAIL_PDNS][PS_SYS_MAX_APN_LEN];
}ps_sys_wlan_max_conn_info_type;

/**
  @brief This is the type that is used for PS_SYS_CONF_3GPP2_S101_STATUS_CHANGE 
         configuration and PS_SYS_EVENT_3GPP2_S101_STATUS_CHANGE event
  @see   PS_SYS_CONF_3GPP2_S101_STATUS_CHANGE 
  @see   PS_SYS_EVENT_3GPP2_S101_STATUS_CHANGE 
*/
typedef struct
{
  boolean tunnel_call_allowed;           /**< if tunnel call is allowed */
  boolean lcp_throttled;                 /**< if lcp is throttle */
  boolean virtual_connection_throttled;  /**< if virtual connection is 
                                              throttled */
  boolean ehrpd_active_handoff_in_prog;   /**< if eHRPD Active Handoff is in
                                              progress*/
} ps_sys_3gpp2_s101_status_change_type;

/**
  @brief Defines structure for PS_SYS_IOCTL_PDN_THROTTLE_INFO ioctl command and
         PS_SYS_EVENT_PDN_THROTTLE_INFO event
  
  @see PS_SYS_EVENT_PDN_THROTTLE_INFO 
  @see PS_SYS_IOCTL_PDN_THROTTLE_INFO 
*/
typedef struct
{
  boolean    is_ipv4_throttled;             /**< ipv4 is throttled */
  boolean    is_ipv6_throttled;             /**< ipv6 is throttled */
  uint32     remaining_ipv4_throttled_time; /**< ipv4 remaining time in 
                                                 miliseconds */ 
  uint32     remaining_ipv6_throttled_time; /**< ipv6 remaining time in 
                                                 miliseconds */
  char       apn_string[PS_SYS_MAX_APN_LEN]; /**< PDN name */
  uint8      apn_string_len;                 /**< PDN name length */
} ps_sys_pdn_ctxt_throttle_info_type;

typedef struct
{
  ps_sys_pdn_ctxt_throttle_info_type pdn_throttle_info[PS_SYS_MAX_APNS];
  /**< For each internet, we have the values ie max apns */

  uint8 num_apn; /**< Refers to the no. of valid array elements in 
                      pdn_throttle_info[] */
  boolean  is_epc_ho_throttle; /**< This flag indicates whether the 
                                    throttle cmd is invoked by EPC  
                                    or mode handler */
} ps_sys_pdn_throttle_info_type;

/**
  @brief Defines the pdn status
*/
typedef enum
{
  PS_SYS_PDN_DOWN,
  PS_SYS_PDN_UP, 
} ps_sys_pdn_status_enum_type;

/**
  @brief Defines structure for PS_SYS_EVENT_PDN_STATUS_INFO event
  pdn_status     : pdn is up or down
  ip_type        : IP address type
  apn_string     : apn name
  apn_string_len : length of the apn name
  @see PS_SYS_EVENT_PDN_STATUS_INFO 
*/
typedef struct
{
  ps_sys_pdn_status_enum_type  pdn_status; 
  ip_addr_enum_type            ip_type;                     
  uint8                        apn_string_len;                 
  char                         apn_string[PS_SYS_MAX_APN_LEN]; 
} ps_sys_pdn_status_info_type;

/**
  @brief Event info type for PS_SYS_EVENT_3GPP2_EVDO_PAGE_MONITOR_PERIOD_CHANGE
         event
*/
typedef uint8 ps_sys_3gpp2_page_monitor_period_type;

typedef struct
{
  ps_sys_3gpp2_page_monitor_period_type       page_monitor_period;
  boolean                                     force_long_sleep;
} ps_sys_3gpp2_evdo_page_monitor_period_type;

/**
  @brief Gives the information about hysteresis cancellation 
         or expiry on the current EPZID.
   
  @see PS_SYS_EVENT_3GPP2_EPZID_INFO
*/
typedef struct
{
  uint8   epzid_info_val;
} ps_sys_3gpp2_epzid_info_type;

/**
  @brief Gives whether the UE is roaming or in the home network.
  roaming_status = TRUE  : UE is roaming
  roaming_status = FALSE : UE is in the home network   
  @see PS_SYS_EVENT_3GPP_ROAMING_STATUS
*/
typedef struct
{
  boolean   roaming_status;
} ps_sys_3gpp_roaming_status_type;

/**
  @brief This is the type that is used to  define different RATs types
*/
typedef enum
{
  PS_SYS_RAT_EX_NULL_BEARER           = DS_SYS_RAT_EX_NULL_BEARER,

  /* 3GPP RAT Values */
  PS_SYS_RAT_EX_3GPP_WCDMA             = DS_SYS_RAT_EX_3GPP_WCDMA,     
  PS_SYS_RAT_EX_3GPP_GERAN             = DS_SYS_RAT_EX_3GPP_GERAN,
  PS_SYS_RAT_EX_3GPP_LTE               = DS_SYS_RAT_EX_3GPP_LTE,
  PS_SYS_RAT_EX_3GPP_TDSCDMA           = DS_SYS_RAT_EX_3GPP_TDSCDMA,
  PS_SYS_RAT_EX_3GPP_WLAN              = DS_SYS_RAT_EX_3GPP_WLAN,
  PS_SYS_RAT_EX_3GPP_MAX               = DS_SYS_RAT_EX_3GPP_MAX,

  /* 3GPP2 RAT Values */
  PS_SYS_RAT_EX_3GPP2_1X               = DS_SYS_RAT_EX_3GPP2_1X,
  PS_SYS_RAT_EX_3GPP2_HRPD             = DS_SYS_RAT_EX_3GPP2_HRPD,
  PS_SYS_RAT_EX_3GPP2_EHRPD            = DS_SYS_RAT_EX_3GPP2_EHRPD,
  PS_SYS_RAT_EX_3GPP2_WLAN             = DS_SYS_RAT_EX_3GPP2_WLAN,
  PS_SYS_RAT_EX_3GPP2_MAX              = DS_SYS_RAT_EX_3GPP2_MAX, 

  /* WLAN RAT Values */
  PS_SYS_RAT_EX_WLAN                   = DS_SYS_RAT_EX_WLAN,
  PS_SYS_RAT_EX_WLAN_MAX               = DS_SYS_RAT_EX_WLAN_MAX,
 
  PS_SYS_RAT_MAX,
} ps_sys_rat_ex_enum_type;

/* SO Mask not available or not specified */
#define PS_SYS_SO_EX_UNSPECIFIED            DS_SYS_SO_EX_UNSPECIFIED

/* 3GPP SO Mask */
#define PS_SYS_SO_EX_3GPP_WCDMA             DS_SYS_SO_EX_3GPP_WCDMA
#define PS_SYS_SO_EX_3GPP_HSDPA             DS_SYS_SO_EX_3GPP_HSDPA
#define PS_SYS_SO_EX_3GPP_HSUPA             DS_SYS_SO_EX_3GPP_HSUPA
#define PS_SYS_SO_EX_3GPP_HSDPAPLUS         DS_SYS_SO_EX_3GPP_HSDPAPLUS
#define PS_SYS_SO_EX_3GPP_DC_HSDPAPLUS      DS_SYS_SO_EX_3GPP_DC_HSDPAPLUS
#define PS_SYS_SO_EX_3GPP_64_QAM            DS_SYS_SO_EX_3GPP_64_QAM
#define PS_SYS_SO_EX_3GPP_HSPA              DS_SYS_SO_EX_3GPP_HSPA
#define PS_SYS_SO_EX_3GPP_GPRS              DS_SYS_SO_EX_3GPP_GPRS
#define PS_SYS_SO_EX_3GPP_EDGE              DS_SYS_SO_EX_3GPP_EDGE

/* GSM so mask is depreciated. Can only be GPRS or EDGE */
#define PS_SYS_SO_EX_3GPP_GSM               DS_SYS_SO_EX_3GPP_GSM

#define PS_SYS_SO_EX_3GPP_S2B               DS_SYS_SO_EX_3GPP_S2B
#define PS_SYS_SO_EX_3GPP_LTE_LIMITED_SRVC  DS_SYS_SO_EX_3GPP_LTE_LIMITED_SRVC
#define PS_SYS_SO_EX_3GPP_LTE_FDD           DS_SYS_SO_EX_3GPP_LTE_FDD
#define PS_SYS_SO_EX_3GPP_LTE_TDD           DS_SYS_SO_EX_3GPP_LTE_TDD
#define PS_SYS_SO_EX_3GPP_TDSCDMA           DS_SYS_SO_EX_3GPP_TDSCDMA
#define PS_SYS_SO_EX_3GPP_DC_HSUPA          DS_SYS_SO_EX_3GPP_DC_HSUPA

/* 3GPP2 SO Mask */
#define PS_SYS_SO_EX_3GPP2_1X_IS95           DS_SYS_SO_EX_3GPP2_1X_IS95
#define PS_SYS_SO_EX_3GPP2_1X_IS2000         DS_SYS_SO_EX_3GPP2_1X_IS2000
#define PS_SYS_SO_EX_3GPP2_1X_IS2000_REL_A   DS_SYS_SO_EX_3GPP2_1X_IS2000_REL_A
#define PS_SYS_SO_EX_3GPP2_HDR_REV0_DPA      DS_SYS_SO_EX_3GPP2_HDR_REV0_DPA
#define PS_SYS_SO_EX_3GPP2_HDR_REVA_DPA      DS_SYS_SO_EX_3GPP2_HDR_REVA_DPA
#define PS_SYS_SO_EX_3GPP2_HDR_REVB_DPA      DS_SYS_SO_EX_3GPP2_HDR_REVB_DPA
#define PS_SYS_SO_EX_3GPP2_HDR_REVA_MPA      DS_SYS_SO_EX_3GPP2_HDR_REVA_MPA
#define PS_SYS_SO_EX_3GPP2_HDR_REVB_MPA      DS_SYS_SO_EX_3GPP2_HDR_REVB_MPA
#define PS_SYS_SO_EX_3GPP2_HDR_REVA_EMPA     DS_SYS_SO_EX_3GPP2_HDR_REVA_EMPA
#define PS_SYS_SO_EX_3GPP2_HDR_REVB_EMPA     DS_SYS_SO_EX_3GPP2_HDR_REVB_EMPA
#define PS_SYS_SO_EX_3GPP2_HDR_REVB_MMPA     DS_SYS_SO_EX_3GPP2_HDR_REVB_MMPA
#define PS_SYS_SO_EX_3GPP2_EVDO_FMC          DS_SYS_SO_EX_3GPP2_EVDO_FMC

/**
  @brief This is the type that is used to  define different subscription types
         Unknown subs id is used when the client does not care/provide the subs id
*/
typedef enum 
{
  PS_SYS_DEFAULT_SUBS      = DS_SYS_DEFAULT_SUBS,
  PS_SYS_PRIMARY_SUBS      = DS_SYS_PRIMARY_SUBS, /**< Primary */
  PS_SYS_SECONDARY_SUBS    = DS_SYS_SECONDARY_SUBS, /**< Secondary */
  PS_SYS_TERTIARY_SUBS     = DS_SYS_TERTIARY_SUBS,  /**< Tertiary */  
  PS_SYS_SUBS_MAX          = DS_SYS_SUBS_MAX
} ps_sys_subscription_enum_type;

/**
  @brief This is the tupple that defines network, RAT, SO and service status
*/
typedef struct 
{
  ps_sys_network_enum_type                 technology; /**< technology type*/
  ps_sys_rat_ex_enum_type                  rat_value;  /**< rat type*/
  uint64                                   so_mask;   /**< so type*/
} ps_sys_system_status_info_type;

/**
  @brief  This is the type that defines apn to preferred network relationship. 
          For every APN, there would be tuple that describes its preferred 
          system.  
*/
typedef struct 
{
   char                           apn_name[PS_SYS_MAX_APN_LEN]; /**< PDN name*/
   uint32                         num_avail_sys; 
   /**< Number of valid entries to available system status array */
   ps_sys_system_status_info_type avail_sys[PS_SYS_MAX_AVAIL_SYS];  
  /**<  Array of available systems for the APN. The topmost element of the 
        array will convey the preferred system for the APN.
        @see ps_sys_system_status_ex_type 
        @see ps_sys_system_status_info_type */
} ps_sys_apn_pref_sys_info_type;

/**
  @brief This is the type that is used to define system status. It is
         common to both configuration PS_SYS_CONF_SYSTEM_STATUS_EX and event
         PS_SYS_EVENT_SYSTEM_STATUS_EX
*/
typedef struct 
{
  uint32   num_avail_sys; 
  /**< Number of valid entries to available system status array */

  ps_sys_system_status_info_type  avail_sys[PS_SYS_MAX_AVAIL_SYS]; 
  /**< Array of current system status,  the topmost element in the array will 
       contain the preferred system*/

  uint32   num_avail_apns;
  /**< Number of apn entries to pdn_info */

  ps_sys_apn_pref_sys_info_type apn_pref_sys_info[PS_SYS_MAX_APNS];    
  /**< Array of all apn available */
} ps_sys_system_status_ex_type;

/**
  @brief Enum type describing APN Preferred system.
  @see   ps_sys_conf_apn_pref_sys_type
*/
typedef enum
{
  PS_SYS_APN_PREF_SYS_WWAN  = DS_SYS_APN_PREF_SYS_WWAN,
  PS_SYS_APN_PREF_SYS_WLAN  = DS_SYS_APN_PREF_SYS_WLAN,
  PS_SYS_APN_PREF_SYS_IWLAN = DS_SYS_APN_PREF_SYS_IWLAN
} ps_sys_apn_pref_sys_enum_type;

/**
  @brief Data type to be used with PS_SYS_CONF_APN_PREF_SYS and 
         PS_SYS_EVENT_APN_PREF_SYS_CHANGE. It describes preferred 
         system for apn.         
*/
typedef struct
{
  unsigned char                  apn[PS_SYS_MAX_APN_LEN];
  ps_sys_apn_pref_sys_enum_type  pref_sys;
} ps_sys_apn_pref_sys_type;

/**
  @brief Data type to hold the apn name
*/
typedef struct
{
  unsigned char     apn_name[PS_SYS_MAX_APN_LEN];
} ps_sys_apn_name_type;

/**   
  @see ds_sys_roaming_disallowed_info_type
*/
typedef struct
{
  boolean                 is_roaming_disallowed;
  uint8                   num_apn_names;
  ps_sys_apn_name_type    apn_list[PS_SYS_MAX_APNS];
} ps_sys_roaming_disallowed_info_type;

/**
  @brief Data type to be used with lte attach pdn list. 
   
  @see  PS_SYS_IOCTL_3GPP_SET_LTE_ATTACH_PDN_LIST 
  @see  PS_SYS_IOCTL_3GPP_GET_LTE_ATTACH_PDN_LIST 
  @see  PS_SYS_EVENT_3GPP_LTE_ATTACH_PDN_LIST_CHANGE 
*/
typedef ds_sys_3gpp_lte_attach_pdn_list_type ps_sys_3gpp_lte_attach_pdn_list_type;
/**
  @brief This is the type that is used for 
         PS_SYS_CONF_3GPP_LTE_ATTACH_TYPE configuration and
         PS_SYS_EVENT_3GPP_LTE_ATTACH_TYPE_CHANGED
   
  @see PS_SYS_CONF_3GPP_LTE_ATTACH_TYPE
  @see PS_SYS_EVENT_3GPP_LTE_ATTACH_TYPE_CHANGED 
*/
typedef enum
{
  PS_SYS_LTE_ATTACH_TYPE_INITIAL  = 0x00,
  PS_SYS_LTE_ATTACH_TYPE_HANDOFF  = 0x01
} ps_sys_lte_attach_enum_type;

/**
  @brief system status event payload for DIAG events
         EVENT_PS_SYSTEM_STATUS
         EVENT_PS_SYSTEM_STATUS_EX
         EVENT_QMI_SYSTEM_STATUS
         EVENT_QMI_SYSTEM_STATUS_EX
  */
typedef struct 
{
  ps_sys_network_enum_type       pref_network; 
  uint32                         rat_value;  
  uint64                         so_mask;
  ps_sys_subscription_enum_type  subs_id;   
} ps_sys_system_status_diag_event_logging_type;

/**
  @brief Traffic state enum type
  Note : Donot change the existing values of below enum type. 
         START : 1 (TRUE) and STOP : 0 (FALSE)
  */
typedef enum
{
  PS_SYS_LOW_LATENCY_TRAFFIC_STATE_STOP,
  PS_SYS_LOW_LATENCY_TRAFFIC_STATE_START,
  PS_SYS_LOW_LATENCY_TRAFFIC_STATE_CONTINUE,
  PS_SYS_LOW_LATENCY_TRAFFIC_STATE_MAX
} ps_sys_low_latency_traffic_state_enum_type;

/**
  @brief Low latency traffic status information. 
         Example : 
         1. If packet arrives at time T, then sleep for
         pkt_interval_time_in_ms - pkt_arrival_delta_in_ms. 
         2. Wait for next packet to arrive with in pkt_max_wait_time_in_ms.
         3. Once packet arrive. Goto step 1     
         4. If packet doesnt arrive then low latency traffic end indication 
            should have been sent         

  @see PS_SYS_EVENT_LOW_LATENCY_TRAFFIC_STATUS
*/
typedef struct
{
  /*! Traffic State */  
  ps_sys_low_latency_traffic_state_enum_type   traffic_state;
  
  /*! Filter result */  
  uint32    filter_handle;    
  /*! Expected periodicity of the packet arrivals for this low latency 
    traffic (in ms).
    Value 0xFFFFFFFF - means unknown */
  uint32                     pkt_interval_time_in_ms;
  
  /*! Expected delta time of the packet from its interval time to wake up
    Value 0xFFFFFFFF - means unknown */
  uint32                     pkt_arrival_delta_in_ms;
  /*! Expected time to wait for the packets at every period 
    for this low latency traffic (in ms).
    Value 0xFFFFFFFF - means unknown  */
  uint32                     pkt_max_wait_time_in_ms;
} ps_sys_low_latency_traffic_info_type;

/**
  @brief Throughput information for all active PDNs  
  @see  PS_SYS_CONF_THROUGHPUT_INFO 
  @see  PS_SYS_EVENT_THROUGHPUT_INFO 
*/

typedef ds_sys_ip_addr_enum_type                ps_sys_ip_addr_enum_type;
typedef ds_sys_throughput_quality_enum_type     ps_sys_throughput_quality_enum_type;
typedef ds_sys_throughput_info_list_type        ps_sys_throughput_info_list_type;
typedef ds_sys_throughput_info_type             ps_sys_throughput_info_type; 
typedef ds_sys_bearer_throughput_info_list_type ps_sys_bearer_throughput_info_list_type;
typedef ds_sys_bearer_throughput_info_type      ps_sys_bearer_throughput_info_type;

/**
  @brief Data type to be used with PS_SYS_CONF_3GPP_EMERGENCY_PDN_STATUS and 
         PS_SYS_EVENT_3GPP_EMERGENCY_PDN_STATUS. 
  @see   ds_sys_is_pdn_only_emergency_type
*/
typedef ds_sys_is_pdn_only_emergency_type ps_sys_is_pdn_only_emergency_type;

/**
  @brief Data type to be used with network aware status
   
  @see   PS_SYS_CONF_3GPP2_QOS_NETWORK_STATUS
  @see   PS_SYS_EVENT_3GPP2_QOS_NETWORK_STATUS_CHANGE  
*/
typedef ds_sys_3gpp2_network_qos_aware_status ps_sys_3gpp2_network_qos_aware_status;

/**
  @brief Data type to be used with network aware status
   
  @see   PS_SYS_IOCTL_3GPP2_GET_PAGE_MONITOR_PERIOD
  @see   PS_SYS_EVENT_3GPP2_PAGE_MONITOR_PERIOD_CHANGE
 */
typedef ds_sys_3gpp2_page_monitor_period_info_type ps_sys_3gpp2_page_monitor_period_info_type;

/*===========================================================================

                      PUBLIC FUNCTION DEFINITIONS

===========================================================================*/

/** 
  @brief This function initializes data structures for ps_sys 
         module. It initializes event queues, ps sys critical section,
         and sets the command handler for event callback in PS task. It also
         allocates memory for holding temporary storage for config data
*/
void ps_sys_init
(
  void
);

/** 
  @brief This function sets the DDS(Default data subscription) 
         in the system. Only Ds3g module can set this value
*/
void ps_sys_set_default_data_subscription
(
  ps_sys_subscription_enum_type  def_data_subs
);

/** 
  @brief This function gets the DDS(Default data subscription) 
         in the system
*/
ps_sys_subscription_enum_type ps_sys_get_default_data_subscription
(
  void
);


#ifdef __cplusplus
}
#endif

#endif /* PS_SYS_H */
