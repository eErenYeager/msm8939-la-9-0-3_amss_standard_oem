#ifndef PS_SYS_CONFI_H
#define PS_SYS_CONFI_H
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                        P S _ S Y S _ C O N F I . H


GENERAL DESCRIPTION
  This is internal header file that defines functions, enums and structs 
  internal to ps_sys configuration module

Copyright (c) 2011 Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/interface/sysapi/src/ps_sys_confi.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when        who    what, where, why
--------    ---    ---------------------------------------------------------- 
03/10/12    msh    Added changes for PS_SYS_CONF_3GPP2_AN_AUTH_STATUS  
09/09/11    bvd    Created module 
 
===========================================================================*/


/*===========================================================================

                          INCLUDE FILES FOR MODULE

===========================================================================*/
#include "comdef.h"
#include "customer.h"

#include "ps_sys_conf.h"

#ifdef __cplusplus
extern "C" {
#endif

/*===========================================================================

                         PUBLIC DATA DECLARATIONS

===========================================================================*/
/**
  @brief Temporary storage for configuration data 
*/
typedef struct
{
  ps_sys_tech_enum_type              tech_type;
  ps_sys_conf_enum_type              conf_name;
  void                              *argval_ptr;
} ps_sys_confi_data;


/**
  @brief Temporary storage for configuration data 
*/
typedef struct
{
  ps_sys_system_status_type             * system_status_type;
  ps_sys_conf_enable_loopback_type      * enable_loopback_ptr;
  ps_sys_conf_get_dun_ctrl_pref_type    * get_dun_ctrl_pref_ptr;
  ps_sys_system_status_ex_type          * system_status_ex_ptr;
  ps_sys_apn_pref_sys_type              * apn_pref_sys_ptr;
  ps_sys_conf_throughput_timer_type     * throughput_timer_ptr;
  ps_sys_throughput_info_list_type      * throughput_info_ptr;
} ps_sys_confi_tech_all;

/**
  @brief Temporary storage for 3gpp2 configuration data 
*/
typedef struct
{
  ps_sys_3gpp2_s101_status_change_type  * s101_status_change_type;
  ps_sys_3gpp2_epzid_info_type          * epzid_info_type;
  ps_sys_conf_3gpp2_an_auth_status_enum_type * an_auth_status;
  ps_sys_3gpp2_network_qos_aware_status * net_aware_status;
} ps_sys_confi_tech_3gpp2;

/**
  @brief Temporary storage for 3gpp configuration data 
*/
typedef struct
{
  ps_sys_lte_attach_enum_type           * lte_attach_type;
  ps_sys_is_pdn_only_emergency_type     * only_emergency_pdn_info;
  ps_sys_roaming_disallowed_info_type   * roaming_disallowed_info_ptr;
} ps_sys_confi_tech_3gpp;

/**
  @brief Temporary storage for wlan configuration data 
*/
typedef struct
{
  ps_sys_wlan_status_type               * wlan_status;
  ps_sys_wlan_preference_enum_type      * wlan_preference;
  ps_sys_wlan_max_conn_info_type        * wlan_max_conn_ptr;
  ps_sys_wlan_mac_addr_type             * wlan_mac_addr_ptr;
  ps_sys_wlan_ssid_type                 * wlan_ssid_ptr;
} ps_sys_confi_tech_wlan;




/*===========================================================================

                      PUBLIC FUNCTION DEFINITIONS

===========================================================================*/


/**
  @brief  This function performs operations to allocate memory for temporary 
          storage
  
  @return 
    None 
*/
void ps_sys_confi_alloc_temp_data
(
  void
);

/**
  @brief  This function verifies authencity of configuration name and technology
          name.

  @param[in] tech_type           States Technology type 
  @param[in] conf_name           Configuration name  
   
  @return  DSS_SUCCESS          On Success.
  @return  DSS_ERROR            On failure.  
  
*/
int ps_sys_confi_verify_tech_conf_name
(
  ps_sys_tech_enum_type                         tech_type,
  ps_sys_conf_enum_type                         conf_name
);

#ifdef __cplusplus
}
#endif

#endif /* PS_SYS_CONFI_H */
