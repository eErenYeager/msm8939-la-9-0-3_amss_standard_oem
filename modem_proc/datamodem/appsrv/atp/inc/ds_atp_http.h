/******************************************************************************
  @file    ds_atp_internal.h
  @brief   Internal header file for ATP(Application Traffic Paring)

  DESCRIPTION
  Internal header file for ATP(Application Traffic Paring)

  INITIALIZATION AND SEQUENCING REQUIREMENTS
  N/A

  ---------------------------------------------------------------------------
  Copyright (C) 2009-2013 Qualcomm Technologies Incorporated.
  All Rights Reserved. QUALCOMM Proprietary and Confidential.
  ---------------------------------------------------------------------------
******************************************************************************/
/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/appsrv/atp/inc/ds_atp_http.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
07/18/13   jz      Created
===========================================================================*/
#ifndef DS_ATP_HTTP_H
#define DS_ATP_HTTP_H

#include "datamodem_variation.h"

#ifdef FEATURE_DATA_ATP
#ifdef __cplusplus
extern "C"
{
#endif

  void ds_atp_http_process_cmd_q(void);
  boolean ds_atp_http_get_policy(void);
  //void ds_atp_http_release_get_content(void);

#ifdef __cplusplus
}
#endif

#endif

#endif
