/******************************************************************************
  @file    ds_atp_xml.h
  @brief   XML parser header file

  DESCRIPTION
  XML parser header file
 
  INITIALIZATION AND SEQUENCING REQUIREMENTS
  N/A

  ---------------------------------------------------------------------------
  Copyright (C) 2009-2013 Qualcomm Technologies Incorporated.
  All Rights Reserved. QUALCOMM Proprietary and Confidential.
  ---------------------------------------------------------------------------
******************************************************************************/
/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/appsrv/atp/inc/ds_atp_xml.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
07/18/13   jz      Created
===========================================================================*/
#ifndef DS_ATP_XML_H
#define DS_ATP_XML_H

#include "datamodem_variation.h"

#ifdef FEATURE_DATA_ATP

#ifdef __cplusplus
extern "C"
{
#endif

boolean ds_atp_xml_parsing(const char *xml_string_ptr, 
                           ds_atp_policy_info_internal_type *policy_ptr);

#ifdef __cplusplus
}
#endif

#endif  /*FEATURE_DATA_ATP*/

#endif
