/*=============================================================================

    __ds_wlan_proxy_mh_sm_int.c

Description:
  This file contains the machine generated source file for the state machine
  specified in the file:
  ds_wlan_proxy_mh_sm.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


/* Include STM compiler generated external and internal header files */
#include "__ds_wlan_proxy_mh_sm.h"
#include "__ds_wlan_proxy_mh_sm_int.h"

/* Include INPUT_DEF_FILE specified files */
#include <ds_wlan_proxy_mh_sm_defs.h>

/* Begin machine generated internal source for state machine array: wlan_proxy_sm[] */

#ifndef STM_DATA_STRUCTURES_ONLY
/* Transition table */
static const stm_transition_fn_t
  wlan_proxy_sm_transitions[ wlan_proxy_sm_NUM_STATES * wlan_proxy_sm_NUM_INPUTS ] =
{
  /* Transition functions for state WLAN_PROXY_SM_STATE_CLOSED */
  wlan_proxy_sm_proc_bring_up_in_closed,    /* WLAN_PROXY_BRING_UP_EV */
  wlan_proxy_sm_ignore_ev,    /* WLAN_PROXY_SIO_CONFIG_COMPLETE_EV */
  wlan_proxy_sm_ignore_ev,    /* WLAN_PROXY_RM_CONFIG_COMPLETE_EV */
  wlan_proxy_sm_ignore_ev,    /* WLAN_PROXY_RM_DOWN_EV */
  wlan_proxy_sm_ignore_ev,    /* WLAN_PROXY_WLAN_UNAVAILABLE_EV */
  wlan_proxy_sm_proc_wlan_avail_in_closed,    /* WLAN_PROXY_WLAN_AVAILABLE_EV */
  wlan_proxy_sm_ignore_ev,    /* WLAN_PROXY_TEAR_DOWN_EV */

  /* Transition functions for state WLAN_PROXY_SM_STATE_SIO_CONFIG */
  wlan_proxy_sm_ignore_ev,    /* WLAN_PROXY_BRING_UP_EV */
  wlan_proxy_sm_proc_sio_config_complete_ev_in_sio_config,    /* WLAN_PROXY_SIO_CONFIG_COMPLETE_EV */
  wlan_proxy_sm_ignore_ev,    /* WLAN_PROXY_RM_CONFIG_COMPLETE_EV */
  wlan_proxy_proc_rm_down_ev_in_sio_config,    /* WLAN_PROXY_RM_DOWN_EV */
  wlan_proxy_proc_wlan_unavailable_ev_in_sio_config,    /* WLAN_PROXY_WLAN_UNAVAILABLE_EV */
  wlan_proxy_sm_ignore_ev,    /* WLAN_PROXY_WLAN_AVAILABLE_EV */
  wlan_proxy_sm_proc_tear_down_ev_in_sio_config,    /* WLAN_PROXY_TEAR_DOWN_EV */

  /* Transition functions for state WLAN_PROXY_SM_STATE_RM_CONFIG */
  wlan_proxy_sm_ignore_ev,    /* WLAN_PROXY_BRING_UP_EV */
  wlan_proxy_sm_ignore_ev,    /* WLAN_PROXY_SIO_CONFIG_COMPLETE_EV */
  wlan_proxy_sm_proc_rm_config_complete_ev_in_rm_config,    /* WLAN_PROXY_RM_CONFIG_COMPLETE_EV */
  wlan_proxy_proc_rm_down_ev_in_rm_config,    /* WLAN_PROXY_RM_DOWN_EV */
  wlan_proxy_proc_wlan_unavailable_ev_in_rm_config,    /* WLAN_PROXY_WLAN_UNAVAILABLE_EV */
  wlan_proxy_proc_wlan_available_ev_in_rm_config,    /* WLAN_PROXY_WLAN_AVAILABLE_EV */
  wlan_proxy_sm_proc_tear_down_ev_in_rm_config,    /* WLAN_PROXY_TEAR_DOWN_EV */

  /* Transition functions for state WLAN_PROXY_SM_STATE_NET_UP */
  wlan_proxy_sm_ignore_ev,    /* WLAN_PROXY_BRING_UP_EV */
  wlan_proxy_sm_ignore_ev,    /* WLAN_PROXY_SIO_CONFIG_COMPLETE_EV */
  wlan_proxy_sm_ignore_ev,    /* WLAN_PROXY_RM_CONFIG_COMPLETE_EV */
  wlan_proxy_proc_rm_down_ev_in_net_up,    /* WLAN_PROXY_RM_DOWN_EV */
  wlan_proxy_proc_wlan_unavailable_ev_in_net_up,    /* WLAN_PROXY_WLAN_UNAVAILABLE_EV */
  wlan_proxy_proc_wlan_available_ev_in_net_up,    /* WLAN_PROXY_WLAN_AVAILABLE_EV */
  wlan_proxy_sm_proc_tear_down_ev_in_net_up,    /* WLAN_PROXY_TEAR_DOWN_EV */

  /* Transition functions for state WLAN_PROXY_SM_STATE_RECONFIG */
  wlan_proxy_sm_ignore_ev,    /* WLAN_PROXY_BRING_UP_EV */
  wlan_proxy_sm_ignore_ev,    /* WLAN_PROXY_SIO_CONFIG_COMPLETE_EV */
  wlan_proxy_sm_ignore_ev,    /* WLAN_PROXY_RM_CONFIG_COMPLETE_EV */
  wlan_proxy_proc_rm_down_ev_in_reconfig,    /* WLAN_PROXY_RM_DOWN_EV */
  wlan_proxy_sm_ignore_ev,    /* WLAN_PROXY_WLAN_UNAVAILABLE_EV */
  wlan_proxy_proc_wlan_available_ev_in_reconfig,    /* WLAN_PROXY_WLAN_AVAILABLE_EV */
  wlan_proxy_sm_proc_tear_down_ev_in_reconfig,    /* WLAN_PROXY_TEAR_DOWN_EV */

  /* Transition functions for state WLAN_PROXY_SM_STATE_RM_RESYNC */
  wlan_proxy_sm_ignore_ev,    /* WLAN_PROXY_BRING_UP_EV */
  wlan_proxy_sm_ignore_ev,    /* WLAN_PROXY_SIO_CONFIG_COMPLETE_EV */
  wlan_proxy_sm_ignore_ev,    /* WLAN_PROXY_RM_CONFIG_COMPLETE_EV */
  wlan_proxy_proc_rm_down_ev_in_rm_resync,    /* WLAN_PROXY_RM_DOWN_EV */
  wlan_proxy_proc_wlan_unavailable_ev_in_rm_resync,    /* WLAN_PROXY_WLAN_UNAVAILABLE_EV */
  wlan_proxy_sm_ignore_ev,    /* WLAN_PROXY_WLAN_AVAILABLE_EV */
  wlan_proxy_sm_proc_tear_down_ev_in_rm_resync,    /* WLAN_PROXY_TEAR_DOWN_EV */

};
#endif /* !STM_DATA_STRUCTURES_ONLY */

/* State { name, entry, exit, child SM } table */
static const stm_state_map_t
  wlan_proxy_sm_states[ wlan_proxy_sm_NUM_STATES ] =
{
  {"WLAN_PROXY_SM_STATE_CLOSED",
#ifndef STM_DATA_STRUCTURES_ONLY
    wlan_proxy_sm_enter_closed, wlan_proxy_sm_exit_closed,
#else /* STM_DATA_STRUCTURES_ONLY */
    NULL, NULL,
#endif /* STM_DATA_STRUCTURES_ONLY */
    NULL},
  {"WLAN_PROXY_SM_STATE_SIO_CONFIG",
#ifndef STM_DATA_STRUCTURES_ONLY
    wlan_proxy_sm_enter_sio_config, wlan_proxy_sm_exit_sio_config,
#else /* STM_DATA_STRUCTURES_ONLY */
    NULL, NULL,
#endif /* STM_DATA_STRUCTURES_ONLY */
    NULL},
  {"WLAN_PROXY_SM_STATE_RM_CONFIG",
#ifndef STM_DATA_STRUCTURES_ONLY
    wlan_proxy_sm_enter_rm_config, wlan_proxy_sm_exit_rm_config,
#else /* STM_DATA_STRUCTURES_ONLY */
    NULL, NULL,
#endif /* STM_DATA_STRUCTURES_ONLY */
    NULL},
  {"WLAN_PROXY_SM_STATE_NET_UP",
#ifndef STM_DATA_STRUCTURES_ONLY
    wlan_proxy_sm_enter_net_up, wlan_proxy_sm_exit_net_up,
#else /* STM_DATA_STRUCTURES_ONLY */
    NULL, NULL,
#endif /* STM_DATA_STRUCTURES_ONLY */
    NULL},
  {"WLAN_PROXY_SM_STATE_RECONFIG",
#ifndef STM_DATA_STRUCTURES_ONLY
    wlan_proxy_sm_enter_reconfig, wlan_proxy_sm_exit_reconfig,
#else /* STM_DATA_STRUCTURES_ONLY */
    NULL, NULL,
#endif /* STM_DATA_STRUCTURES_ONLY */
    NULL},
  {"WLAN_PROXY_SM_STATE_RM_RESYNC",
#ifndef STM_DATA_STRUCTURES_ONLY
    wlan_proxy_sm_enter_rm_resync, wlan_proxy_sm_exit_rm_resync,
#else /* STM_DATA_STRUCTURES_ONLY */
    NULL, NULL,
#endif /* STM_DATA_STRUCTURES_ONLY */
    NULL},
};

/* Input { name, value } table */
static const stm_input_map_t
  wlan_proxy_sm_inputs[ wlan_proxy_sm_NUM_INPUTS ] =
{
  { "WLAN_PROXY_BRING_UP_EV" , (stm_input_t) WLAN_PROXY_BRING_UP_EV },
  { "WLAN_PROXY_SIO_CONFIG_COMPLETE_EV" , (stm_input_t) WLAN_PROXY_SIO_CONFIG_COMPLETE_EV },
  { "WLAN_PROXY_RM_CONFIG_COMPLETE_EV" , (stm_input_t) WLAN_PROXY_RM_CONFIG_COMPLETE_EV },
  { "WLAN_PROXY_RM_DOWN_EV" , (stm_input_t) WLAN_PROXY_RM_DOWN_EV },
  { "WLAN_PROXY_WLAN_UNAVAILABLE_EV" , (stm_input_t) WLAN_PROXY_WLAN_UNAVAILABLE_EV },
  { "WLAN_PROXY_WLAN_AVAILABLE_EV" , (stm_input_t) WLAN_PROXY_WLAN_AVAILABLE_EV },
  { "WLAN_PROXY_TEAR_DOWN_EV" , (stm_input_t) WLAN_PROXY_TEAR_DOWN_EV },
};


/* Constant all-instance state machine data */
static const stm_state_machine_constdata_t wlan_proxy_sm_constdata =
{
  wlan_proxy_sm_NUM_INSTANCES, /* number of state machine instances */
  wlan_proxy_sm_NUM_STATES, /* number of states */
  wlan_proxy_sm_states, /* array of state mappings */
  wlan_proxy_sm_NUM_INPUTS, /* number of inputs */
  wlan_proxy_sm_inputs, /* array of input mappings */
#ifndef STM_DATA_STRUCTURES_ONLY
  wlan_proxy_sm_transitions, /* array of transition function mappings */
  wlan_proxy_sm_init, /* state machine entry function */
  wlan_proxy_sm_cleanup, /* state machine exit function */
  NULL, /* state machine error hook function */
  NULL, /* state machine debug hook function */
  WLAN_PROXY_SM_STATE_CLOSED /* state machine initial state */
#else /* STM_DATA_STRUCTURES_ONLY */
  NULL, /* array of transition function mappings */
  NULL, /* state machine entry function */
  NULL, /* state machine exit function */
  NULL, /* state machine error hook function */
  NULL, /* state machine debug hook function */
  0 /* state machine initial state */
#endif /* STM_DATA_STRUCTURES_ONLY */
};

/* Constant per-instance state machine data */
static const stm_state_machine_perinst_constdata_t
  wlan_proxy_sm_perinst_constdata[ wlan_proxy_sm_NUM_INSTANCES ] =
{
  {
    &wlan_proxy_sm_constdata, /* state machine constant data */
    "wlan_proxy_sm", /* state machine name */
    0xa7aaefa5, /* state machine unique ID (md5("wlan_proxy_sm") & 0xFFFFFFFF) */
    0  /* this state machine instance */
  },
};

/* State machine instance array definition */
stm_state_machine_t
  wlan_proxy_sm[ wlan_proxy_sm_NUM_INSTANCES ] =
{
  {
    &wlan_proxy_sm_perinst_constdata[ 0 ], /* per instance constant data array */
    STM_DEACTIVATED_STATE, /* current state */
    -1, /* current input index */
    NULL, /* calling SM instance */
    FALSE, /* propagate input to parent */
    FALSE, /* locked flag */
    NULL, /* user defined per-instance data */
    0, /* user defined debug mask */
  },
};

#ifndef STM_DATA_STRUCTURES_ONLY
/* User called 'reset' routine.  Should never be needed, but can be used to
   effect a complete reset of all a given state machine's instances. */
void wlan_proxy_sm_reset(void)
{
  uint32 idx;
  void **tricky;

  /* Reset all the child SMs (if any) */
  

  /* Reset the parent */
  for( idx = 0; idx < wlan_proxy_sm_NUM_INSTANCES; idx++)
  {
    tricky = (void **)&wlan_proxy_sm[ idx ].pi_const_data; /* sleight of hand to assign to const ptr below */
    *tricky = (void *)&wlan_proxy_sm_perinst_constdata[ idx ]; /* per instance constant data array */
    wlan_proxy_sm[ idx ].current_state = STM_DEACTIVATED_STATE; /* current state */
    wlan_proxy_sm[ idx ].curr_input_index = -1; /* current input index */
    wlan_proxy_sm[ idx ].propagate_input = FALSE; /* propagate input to parent */
    wlan_proxy_sm[ idx ].is_locked = FALSE; /* locked flag */
    wlan_proxy_sm[ idx ].user_data = NULL; /* user defined per-instance data */
    wlan_proxy_sm[ idx ].debug_mask = 0; /* user defined debug mask */
  }

}
#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated internal source for state machine array: wlan_proxy_sm[] */


