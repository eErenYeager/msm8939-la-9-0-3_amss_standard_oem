/*=============================================================================

    __ds_wlan_proxy_mh_sm_int.h

Description:
  This file contains the machine generated header file for the state machine
  specified in the file:
  ds_wlan_proxy_mh_sm.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


#ifndef __DS_WLAN_PROXY_MH_SM_INT_H
#define __DS_WLAN_PROXY_MH_SM_INT_H

#ifdef __cplusplus
/* If compiled into a C++ file, ensure symbols names are not mangled */
extern "C"
{
#endif

/* Include external state machine header */
#include "__ds_wlan_proxy_mh_sm.h"

/* Begin machine generated internal header for state machine array: wlan_proxy_sm[] */

/* Suppress Lint suggestions to const-ify state machine and payload ptrs */
/*lint -esym(818,sm,payload) */

/* Define a macro for the number of SM instances */
#define wlan_proxy_sm_NUM_INSTANCES 1

/* Define a macro for the number of SM states */
#define wlan_proxy_sm_NUM_STATES 6

/* Define a macro for the number of SM inputs */
#define wlan_proxy_sm_NUM_INPUTS 7

#ifndef STM_DATA_STRUCTURES_ONLY
/* State Machine entry/exit function prototypes */
void wlan_proxy_sm_init(stm_state_machine_t *sm,void *payload);
void wlan_proxy_sm_cleanup(stm_state_machine_t *sm,void *payload);


/* State entry/exit function prototypes */
void wlan_proxy_sm_enter_closed(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void wlan_proxy_sm_exit_closed(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void wlan_proxy_sm_enter_sio_config(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void wlan_proxy_sm_exit_sio_config(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void wlan_proxy_sm_enter_rm_config(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void wlan_proxy_sm_exit_rm_config(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void wlan_proxy_sm_enter_net_up(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void wlan_proxy_sm_exit_net_up(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void wlan_proxy_sm_enter_reconfig(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void wlan_proxy_sm_exit_reconfig(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void wlan_proxy_sm_enter_rm_resync(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void wlan_proxy_sm_exit_rm_resync(stm_state_machine_t *sm,stm_state_t _state,void *payload);


/* Transition function prototypes */
stm_state_t wlan_proxy_sm_proc_bring_up_in_closed(stm_state_machine_t *sm, void *payload);
stm_state_t wlan_proxy_sm_ignore_ev(stm_state_machine_t *sm, void *payload);
stm_state_t wlan_proxy_sm_proc_wlan_avail_in_closed(stm_state_machine_t *sm, void *payload);
stm_state_t wlan_proxy_sm_proc_sio_config_complete_ev_in_sio_config(stm_state_machine_t *sm, void *payload);
stm_state_t wlan_proxy_proc_rm_down_ev_in_sio_config(stm_state_machine_t *sm, void *payload);
stm_state_t wlan_proxy_proc_wlan_unavailable_ev_in_sio_config(stm_state_machine_t *sm, void *payload);
stm_state_t wlan_proxy_sm_proc_tear_down_ev_in_sio_config(stm_state_machine_t *sm, void *payload);
stm_state_t wlan_proxy_sm_proc_rm_config_complete_ev_in_rm_config(stm_state_machine_t *sm, void *payload);
stm_state_t wlan_proxy_proc_rm_down_ev_in_rm_config(stm_state_machine_t *sm, void *payload);
stm_state_t wlan_proxy_proc_wlan_unavailable_ev_in_rm_config(stm_state_machine_t *sm, void *payload);
stm_state_t wlan_proxy_proc_wlan_available_ev_in_rm_config(stm_state_machine_t *sm, void *payload);
stm_state_t wlan_proxy_sm_proc_tear_down_ev_in_rm_config(stm_state_machine_t *sm, void *payload);
stm_state_t wlan_proxy_proc_rm_down_ev_in_net_up(stm_state_machine_t *sm, void *payload);
stm_state_t wlan_proxy_proc_wlan_unavailable_ev_in_net_up(stm_state_machine_t *sm, void *payload);
stm_state_t wlan_proxy_proc_wlan_available_ev_in_net_up(stm_state_machine_t *sm, void *payload);
stm_state_t wlan_proxy_sm_proc_tear_down_ev_in_net_up(stm_state_machine_t *sm, void *payload);
stm_state_t wlan_proxy_proc_rm_down_ev_in_reconfig(stm_state_machine_t *sm, void *payload);
stm_state_t wlan_proxy_proc_wlan_available_ev_in_reconfig(stm_state_machine_t *sm, void *payload);
stm_state_t wlan_proxy_sm_proc_tear_down_ev_in_reconfig(stm_state_machine_t *sm, void *payload);
stm_state_t wlan_proxy_proc_rm_down_ev_in_rm_resync(stm_state_machine_t *sm, void *payload);
stm_state_t wlan_proxy_proc_wlan_unavailable_ev_in_rm_resync(stm_state_machine_t *sm, void *payload);
stm_state_t wlan_proxy_sm_proc_tear_down_ev_in_rm_resync(stm_state_machine_t *sm, void *payload);


/* State enumeration */
enum
{
  WLAN_PROXY_SM_STATE_CLOSED,
  WLAN_PROXY_SM_STATE_SIO_CONFIG,
  WLAN_PROXY_SM_STATE_RM_CONFIG,
  WLAN_PROXY_SM_STATE_NET_UP,
  WLAN_PROXY_SM_STATE_RECONFIG,
  WLAN_PROXY_SM_STATE_RM_RESYNC,
};

#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated internal header for state machine array: wlan_proxy_sm[] */


#ifdef __cplusplus
} /* extern "C" {...} */
#endif

#endif /* ! __DS_WLAN_PROXY_MH_SM_INT_H */
