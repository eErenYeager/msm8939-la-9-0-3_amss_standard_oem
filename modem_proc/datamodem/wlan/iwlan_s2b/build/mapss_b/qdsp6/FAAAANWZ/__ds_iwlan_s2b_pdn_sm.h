/*=============================================================================

    __ds_iwlan_s2b_pdn_sm.h

Description:
  This file contains the machine generated header file for the state machine
  specified in the file:
  ds_iwlan_s2b_pdn_sm.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


#ifndef __DS_IWLAN_S2B_PDN_SM_H
#define __DS_IWLAN_S2B_PDN_SM_H

#ifdef __cplusplus
/* If compiled into a C++ file, ensure symbols names are not mangled */
extern "C"
{
#endif

/* Include STM framework header */
#include <stm2.h>

/* Begin machine generated code for state machine array: ds_iwlan_s2b_pdn_sm[] */

/* Define a macro for the number of SM instances */
#define ds_iwlan_s2b_pdn_sm_NUM_INSTANCES 8

/* External reference to state machine structure */
extern stm_state_machine_t ds_iwlan_s2b_pdn_sm[ ds_iwlan_s2b_pdn_sm_NUM_INSTANCES ];

/* External enumeration representing state machine's states */
enum
{
  ds_iwlan_s2b_pdn_sm__DS_IWLAN_S2B_PDN_SM_CLOSED_STATE,
  ds_iwlan_s2b_pdn_sm__DS_IWLAN_S2B_PDN_SM_TUNNEL_PRE_CONFIG_STATE,
  ds_iwlan_s2b_pdn_sm__DS_IWLAN_S2B_PDN_SM_TUNNEL_CONFIGURING_STATE,
  ds_iwlan_s2b_pdn_sm__DS_IWLAN_S2B_PDN_SM_SIO_CONFIGURING_STATE,
  ds_iwlan_s2b_pdn_sm__DS_IWLAN_S2B_PDN_SM_RM_CONFIGURING_STATE,
  ds_iwlan_s2b_pdn_sm__DS_IWLAN_S2B_PDN_SM_NET_UP_STATE,
  ds_iwlan_s2b_pdn_sm__DS_IWLAN_S2B_PDN_SM_TUNNEL_RESYNC_STATE,
  ds_iwlan_s2b_pdn_sm__DS_IWLAN_S2B_PDN_SM_TUNNEL_TEARDOWN_STATE,
};

#ifndef STM_DATA_STRUCTURES_ONLY
/* User called 'reset' routine.  Should never be needed, but can be used to
   effect a complete reset of all a given state machine's instances. */
extern void ds_iwlan_s2b_pdn_sm_reset(void);
#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated code for state machine array: ds_iwlan_s2b_pdn_sm[] */


#ifdef __cplusplus
} /* extern "C" {...} */
#endif

#endif /* __DS_IWLAN_S2B_PDN_SM_H */
