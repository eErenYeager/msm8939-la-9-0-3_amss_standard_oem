/*=============================================================================

    __ds_iwlan_s2b_pdn_sm_int.h

Description:
  This file contains the machine generated header file for the state machine
  specified in the file:
  ds_iwlan_s2b_pdn_sm.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


#ifndef __DS_IWLAN_S2B_PDN_SM_INT_H
#define __DS_IWLAN_S2B_PDN_SM_INT_H

#ifdef __cplusplus
/* If compiled into a C++ file, ensure symbols names are not mangled */
extern "C"
{
#endif

/* Include external state machine header */
#include "__ds_iwlan_s2b_pdn_sm.h"

/* Begin machine generated internal header for state machine array: ds_iwlan_s2b_pdn_sm[] */

/* Suppress Lint suggestions to const-ify state machine and payload ptrs */
/*lint -esym(818,sm,payload) */

/* Define a macro for the number of SM instances */
#define ds_iwlan_s2b_pdn_sm_NUM_INSTANCES 8

/* Define a macro for the number of SM states */
#define ds_iwlan_s2b_pdn_sm_NUM_STATES 8

/* Define a macro for the number of SM inputs */
#define ds_iwlan_s2b_pdn_sm_NUM_INPUTS 11

#ifndef STM_DATA_STRUCTURES_ONLY
/* State Machine entry/exit function prototypes */
void ds_iwlan_s2b_pdn_sm_init(stm_state_machine_t *sm,void *payload);
void ds_iwlan_s2b_pdn_sm_cleanup(stm_state_machine_t *sm,void *payload);


/* State entry/exit function prototypes */
void ds_iwlan_s2b_pdn_sm_enter_closed_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void ds_iwlan_s2b_pdn_sm_exit_any_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void ds_iwlan_s2b_pdn_sm_enter_tunnel_pre_config_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void ds_iwlan_s2b_pdn_sm_enter_tunnel_configuring_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void ds_iwlan_s2b_pdn_sm_enter_sio_configuring_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void ds_iwlan_s2b_pdn_sm_enter_rm_configuring_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void ds_iwlan_s2b_pdn_sm_enter_net_up_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void ds_iwlan_s2b_pdn_sm_enter_tunnel_resync_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void ds_iwlan_s2b_pdn_sm_enter_tunnel_teardown_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);


/* Transition function prototypes */
stm_state_t ds_iwlan_s2b_pdn_sm_bring_up_ev_in_closed_state(stm_state_machine_t *sm, void *payload);
stm_state_t ds_iwlan_s2b_pdn_sm_wlan_proxy_iface_up_ev_in_closed_state(stm_state_machine_t *sm, void *payload);
stm_state_t ds_iwlan_s2b_pdn_sm_teardown_ev_in_closed_state(stm_state_machine_t *sm, void *payload);
stm_state_t ds_iwlan_s2b_pdn_sm_teardown_ev_in_tunnel_pre_config_state(stm_state_machine_t *sm, void *payload);
stm_state_t ds_iwlan_s2b_pdn_sm_pre_config_complete_ev_in_tunnel_pre_config_state(stm_state_machine_t *sm, void *payload);
stm_state_t ds_iwlan_s2b_pdn_sm_tunnel_up_ev_in_tunnel_configuring_state(stm_state_machine_t *sm, void *payload);
stm_state_t ds_iwlan_s2b_pdn_sm_teardown_ev_in_tunnel_configuring_state(stm_state_machine_t *sm, void *payload);
stm_state_t ds_iwlan_s2b_pdn_sm_tunnel_down_ev_in_tunnel_configuring_state(stm_state_machine_t *sm, void *payload);
stm_state_t ds_iwlan_s2b_pdn_sm_sio_config_complete_ev_in_sio_configuring_state(stm_state_machine_t *sm, void *payload);
stm_state_t ds_iwlan_s2b_pdn_sm_teardown_ev_in_sio_configuring_state(stm_state_machine_t *sm, void *payload);
stm_state_t ds_iwlan_s2b_pdn_sm_tunnel_down_ev_in_sio_configuring_state(stm_state_machine_t *sm, void *payload);
stm_state_t ds_iwlan_s2b_pdn_sm_wlan_proxy_ip_addr_chg_ev_in_sio_config_state(stm_state_machine_t *sm, void *payload);
stm_state_t ds_iwlan_s2b_pdn_sm_rm_up_ev_in_rm_configuring_state(stm_state_machine_t *sm, void *payload);
stm_state_t ds_iwlan_s2b_pdn_sm_teardown_ev_in_rm_configuring_state(stm_state_machine_t *sm, void *payload);
stm_state_t ds_iwlan_s2b_pdn_sm_tunnel_down_ev_in_rm_configuring_state(stm_state_machine_t *sm, void *payload);
stm_state_t ds_iwlan_s2b_pdn_sm_wlan_proxy_ip_addr_chg_ev_in_rm_config_state(stm_state_machine_t *sm, void *payload);
stm_state_t ds_iwlan_s2b_pdn_sm_tunnel_up_ev_in_net_up_state(stm_state_machine_t *sm, void *payload);
stm_state_t ds_iwlan_s2b_pdn_sm_teardown_ev_in_net_up_state(stm_state_machine_t *sm, void *payload);
stm_state_t ds_iwlan_s2b_pdn_sm_tunnel_down_ev_in_net_up_state(stm_state_machine_t *sm, void *payload);
stm_state_t ds_iwlan_s2b_pdn_sm_resync_ev_in_net_up_state(stm_state_machine_t *sm, void *payload);
stm_state_t ds_iwlan_s2b_pdn_sm_wlan_proxy_iface_up_ev_in_net_up_state(stm_state_machine_t *sm, void *payload);
stm_state_t ds_iwlan_s2b_pdn_sm_wlan_proxy_iface_addr_chg_ev_in_net_up_state(stm_state_machine_t *sm, void *payload);
stm_state_t ds_iwlan_s2b_pdn_sm_tunnel_up_ev_in_tunnel_resync_state(stm_state_machine_t *sm, void *payload);
stm_state_t ds_iwlan_s2b_pdn_sm_teardown_ev_in_tunnel_resync_state(stm_state_machine_t *sm, void *payload);
stm_state_t ds_iwlan_s2b_pdn_sm_tunnel_down_ev_in_tunnel_resync_state(stm_state_machine_t *sm, void *payload);
stm_state_t ds_iwlan_s2b_pdn_sm_resync_ev_in_tunnel_resync_state(stm_state_machine_t *sm, void *payload);
stm_state_t ds_iwlan_s2b_pdn_sm_rm_up_ev_in_tunnel_resync_state(stm_state_machine_t *sm, void *payload);
stm_state_t ds_iwlan_s2b_pdn_sm_rm_down_ev_in_tunnel_resync_state(stm_state_machine_t *sm, void *payload);
stm_state_t ds_iwlan_s2b_pdn_sm_wlan_proxy_ip_addr_chg_ev_in_tunnel_resync_state(stm_state_machine_t *sm, void *payload);
stm_state_t ds_iwlan_s2b_pdn_sm_teardown_ev_in_tunnel_teardown_state(stm_state_machine_t *sm, void *payload);
stm_state_t ds_iwlan_s2b_pdn_sm_tunnel_down_ev_in_tunnel_teardown_state(stm_state_machine_t *sm, void *payload);


/* State enumeration */
enum
{
  DS_IWLAN_S2B_PDN_SM_CLOSED_STATE,
  DS_IWLAN_S2B_PDN_SM_TUNNEL_PRE_CONFIG_STATE,
  DS_IWLAN_S2B_PDN_SM_TUNNEL_CONFIGURING_STATE,
  DS_IWLAN_S2B_PDN_SM_SIO_CONFIGURING_STATE,
  DS_IWLAN_S2B_PDN_SM_RM_CONFIGURING_STATE,
  DS_IWLAN_S2B_PDN_SM_NET_UP_STATE,
  DS_IWLAN_S2B_PDN_SM_TUNNEL_RESYNC_STATE,
  DS_IWLAN_S2B_PDN_SM_TUNNEL_TEARDOWN_STATE,
};

#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated internal header for state machine array: ds_iwlan_s2b_pdn_sm[] */


#ifdef __cplusplus
} /* extern "C" {...} */
#endif

#endif /* ! __DS_IWLAN_S2B_PDN_SM_INT_H */
