/*=============================================================================

    __ds_iwlan_s2b_pdn_sm_int.c

Description:
  This file contains the machine generated source file for the state machine
  specified in the file:
  ds_iwlan_s2b_pdn_sm.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


/* Include STM compiler generated external and internal header files */
#include "__ds_iwlan_s2b_pdn_sm.h"
#include "__ds_iwlan_s2b_pdn_sm_int.h"

/* Include INPUT_DEF_FILE specified files */
#include <ds_iwlan_s2b_pdn_sm.h>

/* Begin machine generated internal source for state machine array: ds_iwlan_s2b_pdn_sm[] */

#ifndef STM_DATA_STRUCTURES_ONLY
/* Transition table */
static const stm_transition_fn_t
  ds_iwlan_s2b_pdn_sm_transitions[ ds_iwlan_s2b_pdn_sm_NUM_STATES * ds_iwlan_s2b_pdn_sm_NUM_INPUTS ] =
{
  /* Transition functions for state DS_IWLAN_S2B_PDN_SM_CLOSED_STATE */
  ds_iwlan_s2b_pdn_sm_bring_up_ev_in_closed_state,    /* DS_IWLAN_S2B_PDN_SM_BRING_UP_EV */
  ds_iwlan_s2b_pdn_sm_wlan_proxy_iface_up_ev_in_closed_state,    /* DS_IWLAN_S2B_PDN_SM_WLAN_PROXY_IFACE_UP_EV */
  ds_iwlan_s2b_pdn_sm_teardown_ev_in_closed_state,    /* DS_IWLAN_S2B_PDN_SM_TEARDOWN_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_TUNNEL_PRE_CONFIG_COMPLETE_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_TUNNEL_UP_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_TUNNEL_DOWN_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_SIO_CONFIG_COMPLETE_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_WLAN_PROXY_ADDR_CHANGE_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_RM_UP_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_RESYNC_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_RM_DOWN_EV */

  /* Transition functions for state DS_IWLAN_S2B_PDN_SM_TUNNEL_PRE_CONFIG_STATE */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_BRING_UP_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_WLAN_PROXY_IFACE_UP_EV */
  ds_iwlan_s2b_pdn_sm_teardown_ev_in_tunnel_pre_config_state,    /* DS_IWLAN_S2B_PDN_SM_TEARDOWN_EV */
  ds_iwlan_s2b_pdn_sm_pre_config_complete_ev_in_tunnel_pre_config_state,    /* DS_IWLAN_S2B_PDN_SM_TUNNEL_PRE_CONFIG_COMPLETE_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_TUNNEL_UP_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_TUNNEL_DOWN_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_SIO_CONFIG_COMPLETE_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_WLAN_PROXY_ADDR_CHANGE_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_RM_UP_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_RESYNC_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_RM_DOWN_EV */

  /* Transition functions for state DS_IWLAN_S2B_PDN_SM_TUNNEL_CONFIGURING_STATE */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_BRING_UP_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_WLAN_PROXY_IFACE_UP_EV */
  ds_iwlan_s2b_pdn_sm_teardown_ev_in_tunnel_configuring_state,    /* DS_IWLAN_S2B_PDN_SM_TEARDOWN_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_TUNNEL_PRE_CONFIG_COMPLETE_EV */
  ds_iwlan_s2b_pdn_sm_tunnel_up_ev_in_tunnel_configuring_state,    /* DS_IWLAN_S2B_PDN_SM_TUNNEL_UP_EV */
  ds_iwlan_s2b_pdn_sm_tunnel_down_ev_in_tunnel_configuring_state,    /* DS_IWLAN_S2B_PDN_SM_TUNNEL_DOWN_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_SIO_CONFIG_COMPLETE_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_WLAN_PROXY_ADDR_CHANGE_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_RM_UP_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_RESYNC_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_RM_DOWN_EV */

  /* Transition functions for state DS_IWLAN_S2B_PDN_SM_SIO_CONFIGURING_STATE */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_BRING_UP_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_WLAN_PROXY_IFACE_UP_EV */
  ds_iwlan_s2b_pdn_sm_teardown_ev_in_sio_configuring_state,    /* DS_IWLAN_S2B_PDN_SM_TEARDOWN_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_TUNNEL_PRE_CONFIG_COMPLETE_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_TUNNEL_UP_EV */
  ds_iwlan_s2b_pdn_sm_tunnel_down_ev_in_sio_configuring_state,    /* DS_IWLAN_S2B_PDN_SM_TUNNEL_DOWN_EV */
  ds_iwlan_s2b_pdn_sm_sio_config_complete_ev_in_sio_configuring_state,    /* DS_IWLAN_S2B_PDN_SM_SIO_CONFIG_COMPLETE_EV */
  ds_iwlan_s2b_pdn_sm_wlan_proxy_ip_addr_chg_ev_in_sio_config_state,    /* DS_IWLAN_S2B_PDN_SM_WLAN_PROXY_ADDR_CHANGE_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_RM_UP_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_RESYNC_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_RM_DOWN_EV */

  /* Transition functions for state DS_IWLAN_S2B_PDN_SM_RM_CONFIGURING_STATE */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_BRING_UP_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_WLAN_PROXY_IFACE_UP_EV */
  ds_iwlan_s2b_pdn_sm_teardown_ev_in_rm_configuring_state,    /* DS_IWLAN_S2B_PDN_SM_TEARDOWN_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_TUNNEL_PRE_CONFIG_COMPLETE_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_TUNNEL_UP_EV */
  ds_iwlan_s2b_pdn_sm_tunnel_down_ev_in_rm_configuring_state,    /* DS_IWLAN_S2B_PDN_SM_TUNNEL_DOWN_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_SIO_CONFIG_COMPLETE_EV */
  ds_iwlan_s2b_pdn_sm_wlan_proxy_ip_addr_chg_ev_in_rm_config_state,    /* DS_IWLAN_S2B_PDN_SM_WLAN_PROXY_ADDR_CHANGE_EV */
  ds_iwlan_s2b_pdn_sm_rm_up_ev_in_rm_configuring_state,    /* DS_IWLAN_S2B_PDN_SM_RM_UP_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_RESYNC_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_RM_DOWN_EV */

  /* Transition functions for state DS_IWLAN_S2B_PDN_SM_NET_UP_STATE */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_BRING_UP_EV */
  ds_iwlan_s2b_pdn_sm_wlan_proxy_iface_up_ev_in_net_up_state,    /* DS_IWLAN_S2B_PDN_SM_WLAN_PROXY_IFACE_UP_EV */
  ds_iwlan_s2b_pdn_sm_teardown_ev_in_net_up_state,    /* DS_IWLAN_S2B_PDN_SM_TEARDOWN_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_TUNNEL_PRE_CONFIG_COMPLETE_EV */
  ds_iwlan_s2b_pdn_sm_tunnel_up_ev_in_net_up_state,    /* DS_IWLAN_S2B_PDN_SM_TUNNEL_UP_EV */
  ds_iwlan_s2b_pdn_sm_tunnel_down_ev_in_net_up_state,    /* DS_IWLAN_S2B_PDN_SM_TUNNEL_DOWN_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_SIO_CONFIG_COMPLETE_EV */
  ds_iwlan_s2b_pdn_sm_wlan_proxy_iface_addr_chg_ev_in_net_up_state,    /* DS_IWLAN_S2B_PDN_SM_WLAN_PROXY_ADDR_CHANGE_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_RM_UP_EV */
  ds_iwlan_s2b_pdn_sm_resync_ev_in_net_up_state,    /* DS_IWLAN_S2B_PDN_SM_RESYNC_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_RM_DOWN_EV */

  /* Transition functions for state DS_IWLAN_S2B_PDN_SM_TUNNEL_RESYNC_STATE */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_BRING_UP_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_WLAN_PROXY_IFACE_UP_EV */
  ds_iwlan_s2b_pdn_sm_teardown_ev_in_tunnel_resync_state,    /* DS_IWLAN_S2B_PDN_SM_TEARDOWN_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_TUNNEL_PRE_CONFIG_COMPLETE_EV */
  ds_iwlan_s2b_pdn_sm_tunnel_up_ev_in_tunnel_resync_state,    /* DS_IWLAN_S2B_PDN_SM_TUNNEL_UP_EV */
  ds_iwlan_s2b_pdn_sm_tunnel_down_ev_in_tunnel_resync_state,    /* DS_IWLAN_S2B_PDN_SM_TUNNEL_DOWN_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_SIO_CONFIG_COMPLETE_EV */
  ds_iwlan_s2b_pdn_sm_wlan_proxy_ip_addr_chg_ev_in_tunnel_resync_state,    /* DS_IWLAN_S2B_PDN_SM_WLAN_PROXY_ADDR_CHANGE_EV */
  ds_iwlan_s2b_pdn_sm_rm_up_ev_in_tunnel_resync_state,    /* DS_IWLAN_S2B_PDN_SM_RM_UP_EV */
  ds_iwlan_s2b_pdn_sm_resync_ev_in_tunnel_resync_state,    /* DS_IWLAN_S2B_PDN_SM_RESYNC_EV */
  ds_iwlan_s2b_pdn_sm_rm_down_ev_in_tunnel_resync_state,    /* DS_IWLAN_S2B_PDN_SM_RM_DOWN_EV */

  /* Transition functions for state DS_IWLAN_S2B_PDN_SM_TUNNEL_TEARDOWN_STATE */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_BRING_UP_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_WLAN_PROXY_IFACE_UP_EV */
  ds_iwlan_s2b_pdn_sm_teardown_ev_in_tunnel_teardown_state,    /* DS_IWLAN_S2B_PDN_SM_TEARDOWN_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_TUNNEL_PRE_CONFIG_COMPLETE_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_TUNNEL_UP_EV */
  ds_iwlan_s2b_pdn_sm_tunnel_down_ev_in_tunnel_teardown_state,    /* DS_IWLAN_S2B_PDN_SM_TUNNEL_DOWN_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_SIO_CONFIG_COMPLETE_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_WLAN_PROXY_ADDR_CHANGE_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_RM_UP_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_RESYNC_EV */
  NULL,    /* DS_IWLAN_S2B_PDN_SM_RM_DOWN_EV */

};
#endif /* !STM_DATA_STRUCTURES_ONLY */

/* State { name, entry, exit, child SM } table */
static const stm_state_map_t
  ds_iwlan_s2b_pdn_sm_states[ ds_iwlan_s2b_pdn_sm_NUM_STATES ] =
{
  {"DS_IWLAN_S2B_PDN_SM_CLOSED_STATE",
#ifndef STM_DATA_STRUCTURES_ONLY
    ds_iwlan_s2b_pdn_sm_enter_closed_state, ds_iwlan_s2b_pdn_sm_exit_any_state,
#else /* STM_DATA_STRUCTURES_ONLY */
    NULL, NULL,
#endif /* STM_DATA_STRUCTURES_ONLY */
    NULL},
  {"DS_IWLAN_S2B_PDN_SM_TUNNEL_PRE_CONFIG_STATE",
#ifndef STM_DATA_STRUCTURES_ONLY
    ds_iwlan_s2b_pdn_sm_enter_tunnel_pre_config_state, ds_iwlan_s2b_pdn_sm_exit_any_state,
#else /* STM_DATA_STRUCTURES_ONLY */
    NULL, NULL,
#endif /* STM_DATA_STRUCTURES_ONLY */
    NULL},
  {"DS_IWLAN_S2B_PDN_SM_TUNNEL_CONFIGURING_STATE",
#ifndef STM_DATA_STRUCTURES_ONLY
    ds_iwlan_s2b_pdn_sm_enter_tunnel_configuring_state, ds_iwlan_s2b_pdn_sm_exit_any_state,
#else /* STM_DATA_STRUCTURES_ONLY */
    NULL, NULL,
#endif /* STM_DATA_STRUCTURES_ONLY */
    NULL},
  {"DS_IWLAN_S2B_PDN_SM_SIO_CONFIGURING_STATE",
#ifndef STM_DATA_STRUCTURES_ONLY
    ds_iwlan_s2b_pdn_sm_enter_sio_configuring_state, ds_iwlan_s2b_pdn_sm_exit_any_state,
#else /* STM_DATA_STRUCTURES_ONLY */
    NULL, NULL,
#endif /* STM_DATA_STRUCTURES_ONLY */
    NULL},
  {"DS_IWLAN_S2B_PDN_SM_RM_CONFIGURING_STATE",
#ifndef STM_DATA_STRUCTURES_ONLY
    ds_iwlan_s2b_pdn_sm_enter_rm_configuring_state, ds_iwlan_s2b_pdn_sm_exit_any_state,
#else /* STM_DATA_STRUCTURES_ONLY */
    NULL, NULL,
#endif /* STM_DATA_STRUCTURES_ONLY */
    NULL},
  {"DS_IWLAN_S2B_PDN_SM_NET_UP_STATE",
#ifndef STM_DATA_STRUCTURES_ONLY
    ds_iwlan_s2b_pdn_sm_enter_net_up_state, ds_iwlan_s2b_pdn_sm_exit_any_state,
#else /* STM_DATA_STRUCTURES_ONLY */
    NULL, NULL,
#endif /* STM_DATA_STRUCTURES_ONLY */
    NULL},
  {"DS_IWLAN_S2B_PDN_SM_TUNNEL_RESYNC_STATE",
#ifndef STM_DATA_STRUCTURES_ONLY
    ds_iwlan_s2b_pdn_sm_enter_tunnel_resync_state, ds_iwlan_s2b_pdn_sm_exit_any_state,
#else /* STM_DATA_STRUCTURES_ONLY */
    NULL, NULL,
#endif /* STM_DATA_STRUCTURES_ONLY */
    NULL},
  {"DS_IWLAN_S2B_PDN_SM_TUNNEL_TEARDOWN_STATE",
#ifndef STM_DATA_STRUCTURES_ONLY
    ds_iwlan_s2b_pdn_sm_enter_tunnel_teardown_state, ds_iwlan_s2b_pdn_sm_exit_any_state,
#else /* STM_DATA_STRUCTURES_ONLY */
    NULL, NULL,
#endif /* STM_DATA_STRUCTURES_ONLY */
    NULL},
};

/* Input { name, value } table */
static const stm_input_map_t
  ds_iwlan_s2b_pdn_sm_inputs[ ds_iwlan_s2b_pdn_sm_NUM_INPUTS ] =
{
  { "DS_IWLAN_S2B_PDN_SM_BRING_UP_EV" , (stm_input_t) DS_IWLAN_S2B_PDN_SM_BRING_UP_EV },
  { "DS_IWLAN_S2B_PDN_SM_WLAN_PROXY_IFACE_UP_EV" , (stm_input_t) DS_IWLAN_S2B_PDN_SM_WLAN_PROXY_IFACE_UP_EV },
  { "DS_IWLAN_S2B_PDN_SM_TEARDOWN_EV" , (stm_input_t) DS_IWLAN_S2B_PDN_SM_TEARDOWN_EV },
  { "DS_IWLAN_S2B_PDN_SM_TUNNEL_PRE_CONFIG_COMPLETE_EV" , (stm_input_t) DS_IWLAN_S2B_PDN_SM_TUNNEL_PRE_CONFIG_COMPLETE_EV },
  { "DS_IWLAN_S2B_PDN_SM_TUNNEL_UP_EV" , (stm_input_t) DS_IWLAN_S2B_PDN_SM_TUNNEL_UP_EV },
  { "DS_IWLAN_S2B_PDN_SM_TUNNEL_DOWN_EV" , (stm_input_t) DS_IWLAN_S2B_PDN_SM_TUNNEL_DOWN_EV },
  { "DS_IWLAN_S2B_PDN_SM_SIO_CONFIG_COMPLETE_EV" , (stm_input_t) DS_IWLAN_S2B_PDN_SM_SIO_CONFIG_COMPLETE_EV },
  { "DS_IWLAN_S2B_PDN_SM_WLAN_PROXY_ADDR_CHANGE_EV" , (stm_input_t) DS_IWLAN_S2B_PDN_SM_WLAN_PROXY_ADDR_CHANGE_EV },
  { "DS_IWLAN_S2B_PDN_SM_RM_UP_EV" , (stm_input_t) DS_IWLAN_S2B_PDN_SM_RM_UP_EV },
  { "DS_IWLAN_S2B_PDN_SM_RESYNC_EV" , (stm_input_t) DS_IWLAN_S2B_PDN_SM_RESYNC_EV },
  { "DS_IWLAN_S2B_PDN_SM_RM_DOWN_EV" , (stm_input_t) DS_IWLAN_S2B_PDN_SM_RM_DOWN_EV },
};


/* Constant all-instance state machine data */
static const stm_state_machine_constdata_t ds_iwlan_s2b_pdn_sm_constdata =
{
  ds_iwlan_s2b_pdn_sm_NUM_INSTANCES, /* number of state machine instances */
  ds_iwlan_s2b_pdn_sm_NUM_STATES, /* number of states */
  ds_iwlan_s2b_pdn_sm_states, /* array of state mappings */
  ds_iwlan_s2b_pdn_sm_NUM_INPUTS, /* number of inputs */
  ds_iwlan_s2b_pdn_sm_inputs, /* array of input mappings */
#ifndef STM_DATA_STRUCTURES_ONLY
  ds_iwlan_s2b_pdn_sm_transitions, /* array of transition function mappings */
  ds_iwlan_s2b_pdn_sm_init, /* state machine entry function */
  ds_iwlan_s2b_pdn_sm_cleanup, /* state machine exit function */
  ds_iwlan_s2b_pdn_sm_error_hook, /* state machine error hook function */
  NULL, /* state machine debug hook function */
  DS_IWLAN_S2B_PDN_SM_CLOSED_STATE /* state machine initial state */
#else /* STM_DATA_STRUCTURES_ONLY */
  NULL, /* array of transition function mappings */
  NULL, /* state machine entry function */
  NULL, /* state machine exit function */
  NULL, /* state machine error hook function */
  NULL, /* state machine debug hook function */
  0 /* state machine initial state */
#endif /* STM_DATA_STRUCTURES_ONLY */
};

/* Constant per-instance state machine data */
static const stm_state_machine_perinst_constdata_t
  ds_iwlan_s2b_pdn_sm_perinst_constdata[ ds_iwlan_s2b_pdn_sm_NUM_INSTANCES ] =
{
  {
    &ds_iwlan_s2b_pdn_sm_constdata, /* state machine constant data */
    "ds_iwlan_s2b_pdn_sm[0]", /* state machine name */
    0xd326367d, /* state machine unique ID (md5("ds_iwlan_s2b_pdn_sm[0]") & 0xFFFFFFFF) */
    0  /* this state machine instance */
  },
  {
    &ds_iwlan_s2b_pdn_sm_constdata, /* state machine constant data */
    "ds_iwlan_s2b_pdn_sm[1]", /* state machine name */
    0x10bfd967, /* state machine unique ID (md5("ds_iwlan_s2b_pdn_sm[1]") & 0xFFFFFFFF) */
    1  /* this state machine instance */
  },
  {
    &ds_iwlan_s2b_pdn_sm_constdata, /* state machine constant data */
    "ds_iwlan_s2b_pdn_sm[2]", /* state machine name */
    0x74d7bd69, /* state machine unique ID (md5("ds_iwlan_s2b_pdn_sm[2]") & 0xFFFFFFFF) */
    2  /* this state machine instance */
  },
  {
    &ds_iwlan_s2b_pdn_sm_constdata, /* state machine constant data */
    "ds_iwlan_s2b_pdn_sm[3]", /* state machine name */
    0x188826aa, /* state machine unique ID (md5("ds_iwlan_s2b_pdn_sm[3]") & 0xFFFFFFFF) */
    3  /* this state machine instance */
  },
  {
    &ds_iwlan_s2b_pdn_sm_constdata, /* state machine constant data */
    "ds_iwlan_s2b_pdn_sm[4]", /* state machine name */
    0x84a85444, /* state machine unique ID (md5("ds_iwlan_s2b_pdn_sm[4]") & 0xFFFFFFFF) */
    4  /* this state machine instance */
  },
  {
    &ds_iwlan_s2b_pdn_sm_constdata, /* state machine constant data */
    "ds_iwlan_s2b_pdn_sm[5]", /* state machine name */
    0x75846225, /* state machine unique ID (md5("ds_iwlan_s2b_pdn_sm[5]") & 0xFFFFFFFF) */
    5  /* this state machine instance */
  },
  {
    &ds_iwlan_s2b_pdn_sm_constdata, /* state machine constant data */
    "ds_iwlan_s2b_pdn_sm[6]", /* state machine name */
    0x0ffe535a, /* state machine unique ID (md5("ds_iwlan_s2b_pdn_sm[6]") & 0xFFFFFFFF) */
    6  /* this state machine instance */
  },
  {
    &ds_iwlan_s2b_pdn_sm_constdata, /* state machine constant data */
    "ds_iwlan_s2b_pdn_sm[7]", /* state machine name */
    0x71aff495, /* state machine unique ID (md5("ds_iwlan_s2b_pdn_sm[7]") & 0xFFFFFFFF) */
    7  /* this state machine instance */
  },
};

/* State machine instance array definition */
stm_state_machine_t
  ds_iwlan_s2b_pdn_sm[ ds_iwlan_s2b_pdn_sm_NUM_INSTANCES ] =
{
  {
    &ds_iwlan_s2b_pdn_sm_perinst_constdata[ 0 ], /* per instance constant data array */
    STM_DEACTIVATED_STATE, /* current state */
    -1, /* current input index */
    NULL, /* calling SM instance */
    FALSE, /* propagate input to parent */
    FALSE, /* locked flag */
    NULL, /* user defined per-instance data */
    0, /* user defined debug mask */
  },
  {
    &ds_iwlan_s2b_pdn_sm_perinst_constdata[ 1 ], /* per instance constant data array */
    STM_DEACTIVATED_STATE, /* current state */
    -1, /* current input index */
    NULL, /* calling SM instance */
    FALSE, /* propagate input to parent */
    FALSE, /* locked flag */
    NULL, /* user defined per-instance data */
    0, /* user defined debug mask */
  },
  {
    &ds_iwlan_s2b_pdn_sm_perinst_constdata[ 2 ], /* per instance constant data array */
    STM_DEACTIVATED_STATE, /* current state */
    -1, /* current input index */
    NULL, /* calling SM instance */
    FALSE, /* propagate input to parent */
    FALSE, /* locked flag */
    NULL, /* user defined per-instance data */
    0, /* user defined debug mask */
  },
  {
    &ds_iwlan_s2b_pdn_sm_perinst_constdata[ 3 ], /* per instance constant data array */
    STM_DEACTIVATED_STATE, /* current state */
    -1, /* current input index */
    NULL, /* calling SM instance */
    FALSE, /* propagate input to parent */
    FALSE, /* locked flag */
    NULL, /* user defined per-instance data */
    0, /* user defined debug mask */
  },
  {
    &ds_iwlan_s2b_pdn_sm_perinst_constdata[ 4 ], /* per instance constant data array */
    STM_DEACTIVATED_STATE, /* current state */
    -1, /* current input index */
    NULL, /* calling SM instance */
    FALSE, /* propagate input to parent */
    FALSE, /* locked flag */
    NULL, /* user defined per-instance data */
    0, /* user defined debug mask */
  },
  {
    &ds_iwlan_s2b_pdn_sm_perinst_constdata[ 5 ], /* per instance constant data array */
    STM_DEACTIVATED_STATE, /* current state */
    -1, /* current input index */
    NULL, /* calling SM instance */
    FALSE, /* propagate input to parent */
    FALSE, /* locked flag */
    NULL, /* user defined per-instance data */
    0, /* user defined debug mask */
  },
  {
    &ds_iwlan_s2b_pdn_sm_perinst_constdata[ 6 ], /* per instance constant data array */
    STM_DEACTIVATED_STATE, /* current state */
    -1, /* current input index */
    NULL, /* calling SM instance */
    FALSE, /* propagate input to parent */
    FALSE, /* locked flag */
    NULL, /* user defined per-instance data */
    0, /* user defined debug mask */
  },
  {
    &ds_iwlan_s2b_pdn_sm_perinst_constdata[ 7 ], /* per instance constant data array */
    STM_DEACTIVATED_STATE, /* current state */
    -1, /* current input index */
    NULL, /* calling SM instance */
    FALSE, /* propagate input to parent */
    FALSE, /* locked flag */
    NULL, /* user defined per-instance data */
    0, /* user defined debug mask */
  },
};

#ifndef STM_DATA_STRUCTURES_ONLY
/* User called 'reset' routine.  Should never be needed, but can be used to
   effect a complete reset of all a given state machine's instances. */
void ds_iwlan_s2b_pdn_sm_reset(void)
{
  uint32 idx;
  void **tricky;

  /* Reset all the child SMs (if any) */
  

  /* Reset the parent */
  for( idx = 0; idx < ds_iwlan_s2b_pdn_sm_NUM_INSTANCES; idx++)
  {
    tricky = (void **)&ds_iwlan_s2b_pdn_sm[ idx ].pi_const_data; /* sleight of hand to assign to const ptr below */
    *tricky = (void *)&ds_iwlan_s2b_pdn_sm_perinst_constdata[ idx ]; /* per instance constant data array */
    ds_iwlan_s2b_pdn_sm[ idx ].current_state = STM_DEACTIVATED_STATE; /* current state */
    ds_iwlan_s2b_pdn_sm[ idx ].curr_input_index = -1; /* current input index */
    ds_iwlan_s2b_pdn_sm[ idx ].propagate_input = FALSE; /* propagate input to parent */
    ds_iwlan_s2b_pdn_sm[ idx ].is_locked = FALSE; /* locked flag */
    ds_iwlan_s2b_pdn_sm[ idx ].user_data = NULL; /* user defined per-instance data */
    ds_iwlan_s2b_pdn_sm[ idx ].debug_mask = 0; /* user defined debug mask */
  }

}
#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated internal source for state machine array: ds_iwlan_s2b_pdn_sm[] */


