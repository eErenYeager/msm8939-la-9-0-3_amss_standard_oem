#ifndef DS_IWLAN_S2B_EXT_H
#define DS_IWLAN_S2B_EXT_H
/*===========================================================================
                           DS_IWLAN_S2B_EXT_H
===========================================================================*/

/*!
  @file
  ds_iwlan_s2b_ext.h

  @brief
  This module includes the function declarations used by other modules.

  @detail  
*/

/*===========================================================================

  Copyright (c) 2013-2014 QUALCOMM Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

===========================================================================*/


/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/wlan/iwlan_s2b/inc/ds_iwlan_s2b_ext.h#2 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/03/14   op      Added new API to get MMGSDI events
11/15/13   ab      Initial Version

===========================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/
#include "comdef.h"
#include "customer.h"

#ifdef FEATURE_DATA_IWLAN_S2B

/*===========================================================================
                         EXTERNAL FUNCTION DEFINTIONS
===========================================================================*/
typedef boolean (*ds_iwlan_s2b_call_allowed_hdlr_f_type)
(
  uint32                                pf_id,
  ps_sys_subscription_enum_type         subs_id
);

/*===========================================================================
FUNCTION      DS_IWLAN_S2B_PDN_CONTEXT_PROCESS_CMD

DESCRIPTION   Called by 3G DSMGR, to handle commands specific to this
              mode handler.

DEPENDENCIES  None.

RETURN VALUE  None

SIDE EFFECTS  None 
===========================================================================*/
void ds_iwlan_s2b_pdn_context_process_cmd
(
  ds_cmd_type   *ds_cmd_ptr
);


/*===========================================================================
FUNCTION  ds_iwlan_s2b_task_init()

DESCRIPTION  Run task init functions
  
PARAMETERS  
  None.

DEPENDENCIES 
  None.
  
RETURN VALUE 
  None. 
  
SIDE EFFECTS 
  None.
  
===========================================================================*/
void ds_iwlan_s2b_task_init
(
  void
);

/*===========================================================================
FUNCTION ds_iwlan_s2b_reg_call_allowed_hdlr

DESCRIPTION
  This function is used to register the call allowed handler with the
  EPDG Mode Handler. This API is called by EPC Mode Handler. 

PARAMETERS
  call_allowed_hdlr_f - Pointer to the call allowed handler provided by EPC MH

DEPENDENCIES
  None.

RETURN VALUE  
  None.

SIDE EFFECTS
  None.

===========================================================================*/
void ds_iwlan_s2b_pdn_context_reg_call_allowed_hdlr
(
  ds_iwlan_s2b_call_allowed_hdlr_f_type call_allowed_hdlr_f
);

/*===========================================================================
FUNCTION       DS_IWLAN_S2B_PROCESS_CM_CARD_REFRESH_EVENT

DESCRIPTION    This is the function called by DS3G when it receives Card 
               Refresh callback from CM.

PARAMETERS     dsd_subs_id  - DSD subscription id

DEPENDENCIES   NONE

RETURN VALUE   NONE

SIDE EFFECTS   NONE
===========================================================================*/
void ds_iwlan_s2b_process_cm_card_refresh_event
(
  ps_sys_subscription_enum_type dsd_subs_id
);

#endif /* FEATURE_DATA_IWLAN_S2B */
#endif /* DS_IWLAN_S2B_IFACE_EXT_H */