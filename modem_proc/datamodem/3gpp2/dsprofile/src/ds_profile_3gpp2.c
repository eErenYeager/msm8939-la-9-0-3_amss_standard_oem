/******************************************************************************
  @file    ds_profile_3gpp2.c
  @brief

  DESCRIPTION
  Tech specific implementation of 3GPP2 1x Profile Management

  INITIALIZATION AND SEQUENCING REQUIREMENTS
  N/A

  ---------------------------------------------------------------------------
  Copyright (C) 2009 - 2014 Qualcomm Technologies Incorporated.
  All Rights Reserved. QUALCOMM Proprietary and Confidential.
  ---------------------------------------------------------------------------
******************************************************************************/
/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/3gpp2/dsprofile/src/ds_profile_3gpp2.c#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
07/28/14    hr     Data+MMS - DSDA changes Phase 1.
07/07/14   sc      Fixed code to retrieve failure and disallow timer arrays
04/21/14   vm      Added check to prevent overflow while setting PDN inactivity
                   timeout 
06/26/13   sc      Replace all the memcpy and memmove with memscpy and 
                   memsmove as its safer.
12/02/12   jz      Support for Operator PCO  
04/09/12   msh     Change units for some PPP timers to seconds in EFS
12/12/11   msh     Fix for KW error 
12/08/11   ty      Added PDN throttle failure and disallow timers to parameter tables.  
12/05/11   vpk     Fixes security vulnerabilities.
11/10/11   jz      Add PDN Label 
10/27/11   op      Added support for PDN level authentication 
09/14/11   var     Convert pdn_inactivity_timer from sec to msec and vice-versa
08/04/11   var     Added check for profile param range
01/21/11   mg      Added support for APN class field
01/17/11   ss      Added support for Iface linger.
08/24/10   sa      Added support for zero length param.
04/30/10   ars     Made modem_mem_alloc/free and str fns platform independent
09/30/09   mg      Created the module. First version of the file.
===========================================================================*/

#include "ds_profile_3gpp2i.h"
#include "ds_profile_os.h"
#include <stringl/stringl.h>
#include "data_msg.h"

/*lint -save -e641*/
/*lint -save -e655*/

/*---------------------------------------------------------------------------
                       UTILITY MACROS
---------------------------------------------------------------------------*/

/* Macro to check info (for get_param function)
  (info->buf not NULL and validate info->len) */
#define GET_INFO_IS_VALID( info, max_len ) \
  ( (info != NULL) && (info->buf != NULL) && (info->len >= max_len) )

/* Macro to check info (for set_param function)
  (info->buf not NULL and validate info->len) */
#define SET_INFO_IS_VALID( info, max_len ) \
  ( (info->buf != NULL) && ( (info->len > 0) && (info->len <= max_len) ) )

/*===========================================================================
MACRO CONV_MIN_TO_MSEC()

DESCRIPTION
  This macro returns a msec value for the give value in min

PARAMETERS
  int16 - value in min

RETURN VALUE
  value in msec
===========================================================================*/
#define CONV_MIN_TO_MSEC(timer_val)                        \
  (timer_val = ((timer_val) * 60000))


/*===========================================================================
MACRO CONV_MSEC_TO_MIN()

DESCRIPTION
  This macro returns a min value for the give value in msec

PARAMETERS
  int16 - value in msec

RETURN VALUE
  value in min
===========================================================================*/
#define CONV_MSEC_TO_MIN(timer_val)                        \
  (timer_val = ((timer_val) / 60000))

/*---------------------------------------------------------------------------
              3GPP2 PARAMS ACCCESSOR/MUTATOR ROUTINES & UTILS
---------------------------------------------------------------------------*/
/*=========================================================================*/


/*-----------------------------------------------------------------------------
          3GPP2 PARAMS Table internal to DSI_PROFILE_3GPP2 module
-----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
  Table for parameter and its validity mask
-----------------------------------------------------------------------------*/
static dsi_profile_3gpp2_params_valid_mask ds_profile_3gpp2_params_valid_mask[] = {
  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_INVALID,
   DS_PROFILE_3GPP2_PROFILE_INVALID},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_NEGOTIATE_DNS_SERVER,
   DS_PROFILE_3GPP2_PROFILE_COMMON},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_SESSION_CLOSE_TIMER_DO,
   DS_PROFILE_3GPP2_PROFILE_COMMON},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_SESSION_CLOSE_TIMER_1X,
   DS_PROFILE_3GPP2_PROFILE_COMMON},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_ALLOW_LINGER,
   DS_PROFILE_3GPP2_PROFILE_COMMON},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_LCP_ACK_TIMEOUT,
   DS_PROFILE_3GPP2_PROFILE_COMMON},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_IPCP_ACK_TIMEOUT,
   DS_PROFILE_3GPP2_PROFILE_COMMON},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_AUTH_TIMEOUT,
   DS_PROFILE_3GPP2_PROFILE_COMMON},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_LCP_CREQ_RETRY_COUNT,
   DS_PROFILE_3GPP2_PROFILE_COMMON},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_IPCP_CREQ_RETRY_COUNT,
   DS_PROFILE_3GPP2_PROFILE_COMMON},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_AUTH_RETRY_COUNT,
   DS_PROFILE_3GPP2_PROFILE_COMMON},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_AUTH_PROTOCOL,
   DS_PROFILE_3GPP2_PROFILE_COMMON},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_USER_ID,
   DS_PROFILE_3GPP2_PROFILE_COMMON},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_AUTH_PASSWORD,
   DS_PROFILE_3GPP2_PROFILE_COMMON},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_DATA_RATE,
   DS_PROFILE_3GPP2_PROFILE_COMMON},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_DATA_MODE,
   DS_PROFILE_3GPP2_PROFILE_COMMON},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_APP_TYPE,
   DS_PROFILE_3GPP2_PROFILE_OMH},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_APP_PRIORITY,
   DS_PROFILE_3GPP2_PROFILE_OMH},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_APN_STRING,
   DS_PROFILE_3GPP2_PROFILE_EHRPD},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_TYPE,
   DS_PROFILE_3GPP2_PROFILE_EHRPD},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_IS_PCSCF_ADDR_NEEDED,
   DS_PROFILE_3GPP2_PROFILE_EHRPD},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_V4_DNS_ADDR_PRIMARY,
   DS_PROFILE_3GPP2_PROFILE_EHRPD},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_V4_DNS_ADDR_SECONDARY,
   DS_PROFILE_3GPP2_PROFILE_EHRPD},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_V6_DNS_ADDR_PRIMARY,
   DS_PROFILE_3GPP2_PROFILE_EHRPD},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_V6_DNS_ADDR_SECONDARY,
   DS_PROFILE_3GPP2_PROFILE_EHRPD},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_RAT_TYPE,
   DS_PROFILE_3GPP2_PROFILE_EHRPD},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_LINGER_PARAMS,
   DS_PROFILE_3GPP2_PROFILE_COMMON},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_APN_ENABLED,
   DS_PROFILE_3GPP2_PROFILE_EHRPD},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_INACTIVITY_TIMEOUT,
   DS_PROFILE_3GPP2_PROFILE_EHRPD},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_APN_CLASS,
   DS_PROFILE_3GPP2_PROFILE_EHRPD},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_LEVEL_AUTH_PROTOCOL,
   DS_PROFILE_3GPP2_PROFILE_EHRPD},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_LEVEL_USER_ID,
   DS_PROFILE_3GPP2_PROFILE_EHRPD},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_LEVEL_AUTH_PASSWORD,
   DS_PROFILE_3GPP2_PROFILE_EHRPD},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_LABEL, 
    (DS_PROFILE_3GPP2_PROFILE_EHRPD)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_FAILURE_TIMER_1,
   DS_PROFILE_3GPP2_PROFILE_EHRPD},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_FAILURE_TIMER_2,
   DS_PROFILE_3GPP2_PROFILE_EHRPD},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_FAILURE_TIMER_3,
   DS_PROFILE_3GPP2_PROFILE_EHRPD},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_FAILURE_TIMER_4,
   DS_PROFILE_3GPP2_PROFILE_EHRPD},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_FAILURE_TIMER_5,
   DS_PROFILE_3GPP2_PROFILE_EHRPD},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_FAILURE_TIMER_6,
   DS_PROFILE_3GPP2_PROFILE_EHRPD},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_DISALLOW_TIMER_1,
   DS_PROFILE_3GPP2_PROFILE_EHRPD},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_DISALLOW_TIMER_2,
   DS_PROFILE_3GPP2_PROFILE_EHRPD},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_DISALLOW_TIMER_3,
   DS_PROFILE_3GPP2_PROFILE_EHRPD},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_DISALLOW_TIMER_4,
   DS_PROFILE_3GPP2_PROFILE_EHRPD},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_DISALLOW_TIMER_5,
   DS_PROFILE_3GPP2_PROFILE_EHRPD},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_DISALLOW_TIMER_6,
   DS_PROFILE_3GPP2_PROFILE_EHRPD},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_OP_PCO_ID,
   DS_PROFILE_3GPP2_PROFILE_EHRPD},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_MCC,
   DS_PROFILE_3GPP2_PROFILE_EHRPD},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_MNC,
   DS_PROFILE_3GPP2_PROFILE_EHRPD},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_FAILURE_TIMERS,
   DS_PROFILE_3GPP2_PROFILE_EHRPD},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_DISALLOW_TIMERS,
   DS_PROFILE_3GPP2_PROFILE_EHRPD},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_USER_PROFILE_CLASS,
   DS_PROFILE_3GPP2_PROFILE_COMMON},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_SUBS_ID,
   DS_PROFILE_3GPP2_PROFILE_COMMON}
};

/*-----------------------------------------------------------------------------
  Table for parameter and the size of the parameter required in get/set
  functions
-----------------------------------------------------------------------------*/
static dsi_profile_params_desc_type ds_profile_3gpp2_profile_params_desc_tbl[] = {
  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_INVALID,
   0},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_NEGOTIATE_DNS_SERVER,
   sizeof(ds_profile_3gpp2_negotiate_dns_server_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_SESSION_CLOSE_TIMER_DO,
   sizeof(ds_profile_3gpp2_ppp_session_close_timer_DO_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_SESSION_CLOSE_TIMER_1X,
   sizeof(ds_profile_3gpp2_ppp_session_close_timer_1X_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_ALLOW_LINGER,
   sizeof(ds_profile_3gpp2_allow_linger_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_LCP_ACK_TIMEOUT,
   sizeof(ds_profile_3gpp2_lcp_ack_timeout_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_IPCP_ACK_TIMEOUT,
   sizeof(ds_profile_3gpp2_ipcp_ack_timeout_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_AUTH_TIMEOUT,
   sizeof(ds_profile_3gpp2_auth_timeout_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_LCP_CREQ_RETRY_COUNT,
   sizeof(ds_profile_3gpp2_lcp_creq_retry_count_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_IPCP_CREQ_RETRY_COUNT,
   sizeof(ds_profile_3gpp2_ipcp_creq_retry_count_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_AUTH_RETRY_COUNT,
   sizeof(ds_profile_3gpp2_auth_retry_count_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_AUTH_PROTOCOL,
   sizeof(ds_profile_3gpp2_auth_protocol_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_USER_ID,
   (DS_PROFILE_3GPP2_PPP_MAX_USER_ID_LEN)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_AUTH_PASSWORD,
   (DS_PROFILE_3GPP2_PPP_MAX_PASSWD_LEN)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_DATA_RATE,
   sizeof(ds_profile_3gpp2_data_rate_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_DATA_MODE,
   sizeof(ds_profile_3gpp2_data_mode_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_APP_TYPE,
   sizeof(ds_profile_3gpp2_app_type_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_APP_PRIORITY,
   sizeof(ds_profile_3gpp2_app_priority_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_APN_STRING,
   (DS_PROFILE_3GPP2_APN_MAX_VAL_LEN)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_TYPE,
   sizeof(ds_profile_3gpp2_pdn_type_enum_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_IS_PCSCF_ADDR_NEEDED,
   sizeof(ds_profile_3gpp2_is_pcscf_addr_needed_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_V4_DNS_ADDR_PRIMARY,
   sizeof(struct ds_profile_3gpp2_in_addr)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_V4_DNS_ADDR_SECONDARY,
   sizeof(struct ds_profile_3gpp2_in_addr)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_V6_DNS_ADDR_PRIMARY,
   sizeof(struct ds_profile_3gpp2_in6_addr)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_V6_DNS_ADDR_SECONDARY,
   sizeof(struct ds_profile_3gpp2_in6_addr)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_RAT_TYPE,
   sizeof(ds_profile_3gpp2_rat_type_enum_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_LINGER_PARAMS,
   sizeof(ds_profile_3gpp2_linger_params_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_APN_ENABLED,
   sizeof(ds_profile_3gpp2_apn_enabled)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_INACTIVITY_TIMEOUT,
   sizeof(ds_profile_3gpp2_pdn_inactivity_timeout)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_APN_CLASS,
   sizeof(ds_profile_3gpp2_apn_class)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_LEVEL_AUTH_PROTOCOL,
   sizeof(ds_profile_3gpp2_auth_protocol_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_LEVEL_USER_ID,
   (DS_PROFILE_3GPP2_PPP_MAX_USER_ID_LEN)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_LEVEL_AUTH_PASSWORD,
   (DS_PROFILE_3GPP2_PPP_MAX_PASSWD_LEN)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_LABEL, 
    (DS_PROFILE_3GPP2_APN_MAX_VAL_LEN)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_FAILURE_TIMER_1,
   sizeof(ds_profile_3gpp2_failure_timer_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_FAILURE_TIMER_2,
   sizeof(ds_profile_3gpp2_failure_timer_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_FAILURE_TIMER_3,
   sizeof(ds_profile_3gpp2_failure_timer_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_FAILURE_TIMER_4,
   sizeof(ds_profile_3gpp2_failure_timer_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_FAILURE_TIMER_5,
   sizeof(ds_profile_3gpp2_failure_timer_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_FAILURE_TIMER_6,
   sizeof(ds_profile_3gpp2_failure_timer_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_DISALLOW_TIMER_1,
   sizeof(ds_profile_3gpp2_disallow_timer_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_DISALLOW_TIMER_2,
   sizeof(ds_profile_3gpp2_disallow_timer_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_DISALLOW_TIMER_3,
   sizeof(ds_profile_3gpp2_disallow_timer_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_DISALLOW_TIMER_4,
   sizeof(ds_profile_3gpp2_disallow_timer_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_DISALLOW_TIMER_5,
   sizeof(ds_profile_3gpp2_disallow_timer_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_DISALLOW_TIMER_6,
   sizeof(ds_profile_3gpp2_disallow_timer_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_OP_PCO_ID,
      sizeof(ds_profile_3gpp2_op_pco_id_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_MCC,
   sizeof(ds_profile_3gpp2_mcc_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_MNC,
   sizeof(ds_profile_3gpp2_mnc_type)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_FAILURE_TIMERS,
   sizeof(ds_profile_3gpp2_failure_timer_type) * PDN_THROTTLE_MAX_TIMERS},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_DISALLOW_TIMERS,
   sizeof(ds_profile_3gpp2_disallow_timer_type) * PDN_THROTTLE_MAX_TIMERS},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_USER_PROFILE_CLASS,
   sizeof(ds_profile_3gpp2_user_profile_class)},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_SUBS_ID,
   sizeof(ds_profile_3gpp2_subs_id_enum_type)}
};

/*===========================================================================
FUNCTION dsi_profile_set_ group of functions

DESCRIPTION
  This set of accessor functions are used to set corresponding Profile
  parameter

PARAMETERS
  profile : ptr to 3GPP2 Profile blob
  mask    : mask to identify Profile parameter
  info    : ptr to mem containing data to be written to Profile blob

DEPENDENCIES

RETURN VALUE

SIDE EFFECTS
  none
===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_negotiate_dns_server(
  void                  *profile,
  uint64                 mask,
  const ds_profile_info_type  *info
)
{
  ds_profile_3gpp2_negotiate_dns_server_type negotiate_dns_server;
  uint8 index = 0;

  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_NEGOTIATE_DNS_SERVER);

  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_negotiate_dns_server: "
                     "Invalid len" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }

  negotiate_dns_server = (ds_profile_3gpp2_negotiate_dns_server_type)(*((ds_profile_3gpp2_negotiate_dns_server_type*)(info->buf)));
  if( !(negotiate_dns_server == FALSE || negotiate_dns_server == TRUE) )
  {
    DATA_IS707_MSG1( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_negotiate_dns_server: "
                     "Invalid param value %d", 
                      negotiate_dns_server);
      return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }
  memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->negotiate_dns_server,
  	   sizeof(ds_profile_3gpp2_negotiate_dns_server_type),
           info->buf, info->len );
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_ppp_session_close_timer_DO(
  void                  *profile,
  uint64               mask,
  const ds_profile_info_type  *info
)
{
  uint8 index = 0;

  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_SESSION_CLOSE_TIMER_DO );
  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_ppp_session_close_timer_DO: "
                     "Invalid len" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }
  
  memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->ppp_session_close_timer_DO,
  	   sizeof(ds_profile_3gpp2_ppp_session_close_timer_DO_type),
           info->buf, info->len );
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_ppp_session_close_timer_1X(
  void                  *profile,
  uint64               mask,
  const ds_profile_info_type  *info
)
{
  uint8 index = 0;
  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_SESSION_CLOSE_TIMER_1X );

  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR, 
                     "dsi_profile_3gpp2_set_ppp_session_close_timer_1X: "
                     "Invalid len" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }

  memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->ppp_session_close_timer_1X,
  	   sizeof(ds_profile_3gpp2_ppp_session_close_timer_1X_type),
           info->buf, info->len );
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_allow_linger(
  void                        *profile,
  uint64                      mask,
  const ds_profile_info_type  *info
)
{
  ds_profile_3gpp2_allow_linger_type allow_linger;

  allow_linger = (ds_profile_3gpp2_allow_linger_type)(*((ds_profile_3gpp2_allow_linger_type*)(info->buf)));
  if( !(allow_linger == FALSE || allow_linger == TRUE) )
  {
      DATA_IS707_MSG1( MSG_LEGACY_ERROR,
                       "dsi_profile_3gpp2_set_allow_linger: "
                       "Invalid param value %d", 
                      allow_linger );
      return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->allow_linger,
  	   sizeof(ds_profile_3gpp2_allow_linger_type),
           info->buf, info->len );
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_linger_timeout_val(
  void                        *profile,
  uint64                      mask,
  const ds_profile_info_type  *info
)
{
  memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->linger_timeout_val,
  	   sizeof(ds_profile_3gpp2_linger_timeout_val_type),
           info->buf, info->len );
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_linger_params(
  void                        *profile,
  uint64                       mask,
  const ds_profile_info_type  *info
)
{
  ds_profile_info_type  temp_info;
  uint8 index = 0;
  
  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_LINGER_PARAMS );
  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR, 
                     "dsi_profile_3gpp2_set_linger_params: Invalid len" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }

  /* Set Allow linger flag */
  temp_info.len = sizeof(ds_profile_3gpp2_allow_linger_type);
  temp_info.buf =
         (void *)&(((ds_profile_3gpp2_linger_params_type *)(info->buf))->allow_linger_flag);

  (void)dsi_profile_3gpp2_set_allow_linger(profile, mask, &temp_info);

  /* Set Linger timeout val */
  temp_info.len = sizeof(ds_profile_3gpp2_linger_timeout_val_type);
  temp_info.buf =
         (void *)&(((ds_profile_3gpp2_linger_params_type *)(info->buf))->linger_timeout_val);

  (void)dsi_profile_3gpp2_set_linger_timeout_val(profile, mask, &temp_info);

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_lcp_ack_timeout(
  void                  *profile,
  uint64               mask,
  const ds_profile_info_type  *info
)
{
  uint8 index = 0;
  
  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_LCP_ACK_TIMEOUT);
  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_lcp_ack_timeout: Invalid len" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  } 

  memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->lcp_ack_timeout,
  	   sizeof(ds_profile_3gpp2_lcp_ack_timeout_type),
           info->buf, info->len );
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_ipcp_ack_timeout(
  void                  *profile,
  uint64               mask,
  const ds_profile_info_type  *info
)
{
  uint8 index = 0;
  
  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_IPCP_ACK_TIMEOUT);
  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
               "dsi_profile_3gpp2_set_ipcp_ack_timeout: Invalid len" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  } 
  memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->ipcp_ack_timeout,
  	   sizeof(ds_profile_3gpp2_ipcp_ack_timeout_type),
           info->buf, info->len );
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_auth_timeout(
  void                  *profile,
  uint64               mask,
  const ds_profile_info_type  *info
)
{
  uint8 index = 0;
  
  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_AUTH_TIMEOUT);
  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_auth_timeout: Invalid len" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }  

  memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->auth_timeout,
  	   sizeof(ds_profile_3gpp2_auth_timeout_type),
           info->buf, info->len );
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_lcp_creq_retry_count(
  void                  *profile,
  uint64               mask,
  const ds_profile_info_type  *info
)
{
  uint8 index = 0;
  
  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_LCP_CREQ_RETRY_COUNT);
  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_lcp_creq_retry_count: "
                     "Invalid len" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }  

  memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->lcp_creq_retry_count,
  	   sizeof(ds_profile_3gpp2_lcp_creq_retry_count_type),
           info->buf, info->len );
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_ipcp_creq_retry_count(
  void                  *profile,
  uint64               mask,
  const ds_profile_info_type  *info
)
{
  uint8 index = 0;
  
  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_IPCP_CREQ_RETRY_COUNT);
  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_ipcp_creq_retry_count: "
                     "Invalid len" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  } 

  memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->ipcp_creq_retry_count,
  	   sizeof(ds_profile_3gpp2_ipcp_creq_retry_count_type),
           info->buf, info->len );
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_auth_retry_count(
  void                  *profile,
  uint64               mask,
  const ds_profile_info_type  *info
)
{
  uint8 index = 0;
  
  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_AUTH_RETRY_COUNT);
  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_auth_retry_count: Invalid len" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  } 

  memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->auth_retry_count,
  	   sizeof(ds_profile_3gpp2_auth_retry_count_type),
           info->buf, info->len );
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_auth_protocol(
  void                  *profile,
  uint64               mask,
  const ds_profile_info_type  *info
)
{
  ds_profile_3gpp2_auth_protocol_type auth_protocol;
  uint8 index = 0;
  
  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_AUTH_PROTOCOL);

  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_auth_protocol: Invalid len" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  } 

  auth_protocol = (ds_profile_3gpp2_auth_protocol_type)(*((ds_profile_3gpp2_auth_protocol_type*)(info->buf)));

  if(!((auth_protocol == DS_PROFILE_3GPP2_AUTH_PROTOCOL_PAP) ||
       (auth_protocol == DS_PROFILE_3GPP2_AUTH_PROTOCOL_CHAP)||
       (auth_protocol == DS_PROFILE_3GPP2_AUTH_PROTOCOL_PAP_CHAP) ||
       (auth_protocol == DS_PROFILE_3GPP2_AUTH_PROTOCOL_NONE)))
  {
      DATA_IS707_MSG1( MSG_LEGACY_ERROR,
                       "dsi_profile_3gpp2_set_auth_protocol: "
                       "Invalid param value %d", 
                      auth_protocol );
      return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->auth_protocol,
  	   sizeof(ds_profile_3gpp2_auth_protocol_type),
           info->buf, info->len );
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_user_id(
  void                  *profile,
  uint64               mask,
  const ds_profile_info_type  *info
)
{
  uint8 index = 0;

  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_USER_ID );

  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_user_id: Invalid len" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }

  /* clear the entire destination buffer before memcpy of requested data */
  memset((void*)((dsi_profile_3gpp2_type *)profile)->prf->user_id, 0,
         ds_profile_3gpp2_profile_params_desc_tbl[index].len );

  if( info->len != 0 )
  {
    memscpy( (void*)((dsi_profile_3gpp2_type *)profile)->prf->user_id,
    	     DS_PROFILE_3GPP2_PPP_MAX_USER_ID_LEN,
             info->buf, info->len );
  }

  ((dsi_profile_3gpp2_type *)profile)->prf->user_id_len = (uint8)info->len;
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_auth_password(
  void                  *profile,
  uint64               mask,
  const ds_profile_info_type  *info
)
{
  uint8 index =0;

  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_AUTH_PASSWORD );
  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_auth_password: Invalid len" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }

  /* clear the entire destination buffer before memcpy of requested data */
  memset((void*)((dsi_profile_3gpp2_type *)profile)->prf->auth_password, 0,
         ds_profile_3gpp2_profile_params_desc_tbl[index].len );

  if( info->len != 0 )
  {
    memscpy( (void*)((dsi_profile_3gpp2_type *)profile)->prf->auth_password,
             DS_PROFILE_3GPP2_PPP_MAX_PASSWD_LEN,
             info->buf, info->len );
  }

  ((dsi_profile_3gpp2_type *)profile)->prf->auth_password_len = (uint8)info->len;
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_data_rate(
  void                  *profile,
  uint64               mask,
  const ds_profile_info_type  *info
)
{
  ds_profile_3gpp2_data_rate_type data_rate;
  uint8 index = 0;
  
  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_DATA_RATE);
  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR, 
                     "dsi_profile_3gpp2_set_data_rate: Invalid len" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  } 

  data_rate = (ds_profile_3gpp2_data_rate_type)(*((ds_profile_3gpp2_data_rate_type*)(info->buf)));

  if( !( (data_rate == DS_PROFILE_3GPP2_DATARATE_LOW) ||
         (data_rate == DS_PROFILE_3GPP2_DATARATE_MED) ||
         (data_rate == DS_PROFILE_3GPP2_DATARATE_HIGH) ) )
  {
      DATA_IS707_MSG1( MSG_LEGACY_ERROR,
                       "dsi_profile_3gpp2_set_data_rate: "
                       "Invalid param value %d", 
                      data_rate );
      return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->data_rate,
  	   sizeof(ds_profile_3gpp2_data_rate_type),
           info->buf, info->len );
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_data_mode(
  void                  *profile,
  uint64               mask,
  const ds_profile_info_type  *info
)
{
  ds_profile_3gpp2_data_mode_type data_mode;
  uint8 index = 0;
  
  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_DATA_MODE);
  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_data_mode: Invalid len" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  } 

  data_mode = (ds_profile_3gpp2_data_mode_type)(*((ds_profile_3gpp2_data_mode_type*)(info->buf)));

  if( !( (data_mode == DS_PROFILE_3GPP2_DATAMODE_CDMA_HDR) ||
         (data_mode == DS_PROFILE_3GPP2_DATAMODE_CDMA_ONLY) ||
         (data_mode == DS_PROFILE_3GPP2_DATAMODE_HDR_ONLY) ) )
  {
      DATA_IS707_MSG1( MSG_LEGACY_ERROR,
                       "dsi_profile_3gpp2_set_data_mode: Invalid param value %d", 
                      data_mode );
      return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->data_mode,
  	   sizeof(ds_profile_3gpp2_data_mode_type),
           info->buf, info->len );
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_app_type(
  void                  *profile,
  uint64               mask,
  const ds_profile_info_type  *info
)
{
  DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                   "dsi_profile_3gpp2_set_app_type: set app_type not allowed");

  /*memcpy((void*)&((dsi_profile_3gpp2_type *)profile)->prf->app_type,
          info->buf, info->len );
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;*/
  return DS_PROFILE_REG_RESULT_ERR_INVAL_OP;
}

/*===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_app_priority(
  void                  *profile,
  uint64               mask,
  const ds_profile_info_type  *info
)
{
  DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                   "dsi_profile_3gpp2_set_app_priority: "
                   "set app_priority not allowed");

  /*memcpy((void*)&((dsi_profile_3gpp2_type *)profile)->prf->app_priority,
          info->buf, info->len );
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;*/
  return DS_PROFILE_REG_RESULT_ERR_INVAL_OP;
}

/*===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_apn_string(
  void                  *profile,
  uint64               mask,
  const ds_profile_info_type  *info
)
{
  uint8 index =0;

  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_APN_STRING );
  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_apn_string: Invalid buf" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }

  /* clear the entire destination buffer before memcpy of requested data */
  memset((void*)((dsi_profile_3gpp2_type *)profile)->prf->apn_string, 0,
         ds_profile_3gpp2_profile_params_desc_tbl[index].len );

  if (info->len != 0)
  {
    memscpy( (void*)((dsi_profile_3gpp2_type *)profile)->prf->apn_string,
    	     DS_PROFILE_3GPP2_APN_MAX_VAL_LEN,
             info->buf, info->len );
  }

  ((dsi_profile_3gpp2_type *)profile)->prf->apn_string_len = (uint8)info->len;
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_pdn_label(
  void                  *profile,
  uint64               mask,
  const ds_profile_info_type  *info
)
{
  uint8 index =0;

  index = dsi_profile_3gpp2_get_index_from_ident( 
                                DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_LABEL );
  if(( info->len != 0 ) &&  
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_pdn_label: Invalid buf" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }

  /* clear the entire destination buffer before memcpy of requested data */
  memset((void*)((dsi_profile_3gpp2_type *)profile)->prf->pdn_label, 0, 
         ds_profile_3gpp2_profile_params_desc_tbl[index].len ); 

  if (info->len != 0) 
  {
    memscpy( (void*)((dsi_profile_3gpp2_type *)profile)->prf->pdn_label,
    	     DS_PROFILE_3GPP2_APN_MAX_VAL_LEN,
             info->buf, info->len );
  }

  ((dsi_profile_3gpp2_type *)profile)->prf->pdn_label_len = (uint8)info->len;
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_mcc(
  void                  *profile,
  uint64               mask,
  const ds_profile_info_type  *info
)
{
  uint8 index =0;

  index = dsi_profile_3gpp2_get_index_from_ident( 
                                DS_PROFILE_3GPP2_PROFILE_PARAM_MCC );
  if(( info->len != 0 ) &&  
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_mcc: Invalid buf" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }

  memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->mcc, 
  	   sizeof(ds_profile_3gpp2_mcc_type),
           info->buf, info->len );
    ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}
/*===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_mnc(
  void                  *profile,
  uint64               mask,
  const ds_profile_info_type  *info
)
{
  uint8 index =0;

  index = dsi_profile_3gpp2_get_index_from_ident( 
                                DS_PROFILE_3GPP2_PROFILE_PARAM_MNC );
  if(( info->len != 0 ) &&  
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR, 
                     "dsi_profile_3gpp2_set_mnc: Invalid buf" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }

  memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->mnc,
   	   sizeof(ds_profile_3gpp2_mnc_type),
           info->buf, info->len );
    ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_apn_class(
  void                  *profile,
  uint64               mask,
  const ds_profile_info_type  *info
)
{
  uint8 index = 0;
  
  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_APN_CLASS);
  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_apn_class: Invalid len" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  } 
  memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->apn_class,
  	   sizeof(ds_profile_3gpp2_apn_class),
           info->buf, info->len );
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_apn_enabled(
  void                  *profile,
  uint64               mask,
  const ds_profile_info_type  *info
)
{
  ds_profile_3gpp2_apn_enabled apn_enabled;
  uint8 index = 0;
  
  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_APN_ENABLED);
  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_apn_enabled: Invalid len" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }

  apn_enabled = (ds_profile_3gpp2_apn_enabled)(*((ds_profile_3gpp2_apn_enabled*)(info->buf)));

  if( !( (apn_enabled == FALSE) ||
         (apn_enabled == TRUE) ) )
  {
      DATA_IS707_MSG1( MSG_LEGACY_ERROR,
                       "dsi_profile_3gpp2_set_apn_enabled: "
                       "Invalid param value %d", 
                      apn_enabled );
      return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->apn_enabled,
  	   sizeof(ds_profile_3gpp2_apn_enabled),
           info->buf, info->len );
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_pdn_type(
  void                  *profile,
  uint64               mask,
  const ds_profile_info_type  *info
)
{
  ds_profile_3gpp2_pdn_type_enum_type pdn_type;
  uint8 index = 0;
  
  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_TYPE );
  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_pdn_type: Invalid len" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  } 

  pdn_type = (ds_profile_3gpp2_pdn_type_enum_type)(*((ds_profile_3gpp2_pdn_type_enum_type*)(info->buf)));

  if( !( (pdn_type == DS_PROFILE_3GPP2_PDN_TYPE_V4) ||
         (pdn_type == DS_PROFILE_3GPP2_PDN_TYPE_V6) ||
         (pdn_type == DS_PROFILE_3GPP2_PDN_TYPE_V4_V6) ||
         (pdn_type == DS_PROFILE_3GPP2_PDN_TYPE_UNSPEC) ) )
  {
      DATA_IS707_MSG1( MSG_LEGACY_ERROR,
                       "dsi_profile_3gpp2_set_pdn_type: Invalid param value %d", 
                      pdn_type );
      return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->pdn_type,
  	   sizeof(ds_profile_3gpp2_pdn_type_enum_type),
           info->buf, info->len );
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_is_pcscf_addr_needed(
  void                  *profile,
  uint64               mask,
  const ds_profile_info_type  *info
)
{
  ds_profile_3gpp2_is_pcscf_addr_needed_type is_pcscf_addr_needed_flag;
  uint8 index = 0;
  
  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_IS_PCSCF_ADDR_NEEDED );
  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_is_pcscf_addr_needed: Invalid len" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  } 

  is_pcscf_addr_needed_flag = (ds_profile_3gpp2_is_pcscf_addr_needed_type)(*((ds_profile_3gpp2_is_pcscf_addr_needed_type*)(info->buf)));

  if( !( (is_pcscf_addr_needed_flag == FALSE) ||
         (is_pcscf_addr_needed_flag == TRUE) ) )
  {
    DATA_IS707_MSG1( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_is_pcscf_addr_needed: "
                     "Invalid param value %d", 
                    is_pcscf_addr_needed_flag );
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->is_pcscf_addr_needed,
  	   sizeof(ds_profile_3gpp2_is_pcscf_addr_needed_type),
           info->buf, info->len );
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_op_pco_id(
											  void                        *profile,
											  uint64                      mask,
											  const ds_profile_info_type  *info
											)
{
	 uint8 index =0;

	 index = dsi_profile_3gpp2_get_index_from_ident(
								  DS_PROFILE_3GPP2_PROFILE_PARAM_OP_PCO_ID );

  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_op_pco_id: Invalid len" );
	  return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }
  memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->op_pco_id, 
  	   sizeof(ds_profile_3gpp2_op_pco_id_type),
	   info->buf, info->len );
    ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

	return DS_PROFILE_REG_RESULT_SUCCESS;

}
/*===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_v4_dns_addr_primary(
  void                  *profile,
  uint64               mask,
  const ds_profile_info_type  *info
)
{
  uint8 index = 0;

  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_V4_DNS_ADDR_PRIMARY );
  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_v4_dns_addr_primary: Invalid len" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  } 
  
  memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->v4_dns_addr[DS_PROFILE_3GPP2_PRIMARY_DNS_ADDR],
  	   sizeof(ds_profile_3gpp2_in_addr_type),
           info->buf, info->len );
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_v4_dns_addr_secondary(
  void                  *profile,
  uint64               mask,
  const ds_profile_info_type  *info
)
{
  uint8 index = 0;

  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_V4_DNS_ADDR_SECONDARY );
  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_v4_dns_addr_secondary: "
                     "Invalid len" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  } 
  
  memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->v4_dns_addr[DS_PROFILE_3GPP2_SECONDARY_DNS_ADDR],
  	   sizeof(ds_profile_3gpp2_in_addr_type),
           info->buf, info->len );
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_v6_dns_addr_primary(
  void                  *profile,
  uint64               mask,
  const ds_profile_info_type  *info
)
{
  uint8 index = 0;

  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_V6_DNS_ADDR_PRIMARY );
  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_v6_dns_addr_primary: Invalid len" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }
  
  memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->v6_dns_addr[DS_PROFILE_3GPP2_PRIMARY_DNS_ADDR],
  	   sizeof(ds_profile_3gpp2_in6_addr_type),
           info->buf, info->len );
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_v6_dns_addr_secondary(
  void                  *profile,
  uint64               mask,
  const ds_profile_info_type  *info
)
{
  uint8 index = 0;

  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_V6_DNS_ADDR_SECONDARY );
  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_v6_dns_addr_secondary: "
                     "Invalid len" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }
  
  memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->v6_dns_addr[DS_PROFILE_3GPP2_SECONDARY_DNS_ADDR],
  	   sizeof(ds_profile_3gpp2_in6_addr_type),
           info->buf, info->len );
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_rat_type(
  void                  *profile,
  uint64               mask,
  const ds_profile_info_type  *info
)
{
  ds_profile_3gpp2_rat_type_enum_type rat_type;
  uint8 index = 0;

  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_RAT_TYPE );
  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_rat_type: Invalid len" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }
  rat_type = (ds_profile_3gpp2_rat_type_enum_type)(*((ds_profile_3gpp2_rat_type_enum_type*)(info->buf)));

  if( !( (rat_type == DS_PROFILE_3GPP2_RAT_TYPE_HRPD) ||
         (rat_type == DS_PROFILE_3GPP2_RAT_TYPE_EHRPD) ||
         (rat_type == DS_PROFILE_3GPP2_RAT_TYPE_HRPD_EHRPD) ) )
  {
    DATA_IS707_MSG1( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_rat_type: Invalid param value %d", 
                    rat_type );
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->rat_type,
  	   sizeof(ds_profile_3gpp2_rat_type_enum_type),
           info->buf, info->len );
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_param_invalid(
  void                  *profile,
  uint64               mask,
  const ds_profile_info_type  *info
)
{
  DATA_IS707_MSG0( MSG_LEGACY_ERROR, "_3gpp2_set: Invalid identifier" );
  return DS_PROFILE_REG_RESULT_ERR_INVAL_IDENT;
}

/*=========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp2_set_pdn_inactivity_timeout(
  void                  *profile,
  uint64               mask,
  const ds_profile_info_type  *info
)
{
  uint8 index = 0;
  ds_profile_3gpp2_pdn_inactivity_timeout pdn_inactivity_timeout=0;
  ds_profile_3gpp2_pdn_inactivity_timeout max_inactivity_timeout=0;

  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_INACTIVITY_TIMEOUT );
  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_pdn_inactivity_timeout: "
                     "Invalid len" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }
  /* --------------------------------------------------------------------------
   QMI sends in timer in min, needs to be converted from min to msec
  --------------------------------------------------------------------------*/
  memscpy( &pdn_inactivity_timeout, sizeof(pdn_inactivity_timeout),
           info->buf, sizeof(pdn_inactivity_timeout) );
  memset(&max_inactivity_timeout, 0xFF, sizeof(max_inactivity_timeout));
  
  if ( CONV_MSEC_TO_MIN(max_inactivity_timeout) < pdn_inactivity_timeout )
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_pdn_inactivity_timeout: "
                     "Value overflow after convertion to msec" );
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }
  
  CONV_MIN_TO_MSEC(pdn_inactivity_timeout);

  memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->pdn_inactivity_timeout,
  	   sizeof(ds_profile_3gpp2_pdn_inactivity_timeout),
           &pdn_inactivity_timeout, info->len );
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
} 

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_pdn_level_auth_protocol(
  void                  *profile,
  uint64               mask,
  const ds_profile_info_type  *info
)
{
  uint8 index = 0;
  ds_profile_3gpp2_auth_protocol_type pdn_level_auth_protocol;

  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_LEVEL_AUTH_PROTOCOL );
  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_pdn_level_auth_protocol: "
                     "Invalid len" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }
  pdn_level_auth_protocol = (ds_profile_3gpp2_auth_protocol_type)(*((ds_profile_3gpp2_auth_protocol_type*)(info->buf)));

  if(!((pdn_level_auth_protocol == DS_PROFILE_3GPP2_AUTH_PROTOCOL_PAP) ||
       (pdn_level_auth_protocol == DS_PROFILE_3GPP2_AUTH_PROTOCOL_CHAP)|| 
       (pdn_level_auth_protocol == DS_PROFILE_3GPP2_AUTH_PROTOCOL_PAP_CHAP) ||
       (pdn_level_auth_protocol == DS_PROFILE_3GPP2_AUTH_PROTOCOL_NONE)))
  {
      DATA_IS707_MSG1( MSG_LEGACY_ERROR,
                       "dsi_profile_3gpp2_set_pdn_level_auth_protocol: "
                       "Invalid param value %d", 
                      pdn_level_auth_protocol );
      return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->pdn_level_auth_protocol,
  	   sizeof(uint8), info->buf, info->len );
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_pdn_level_user_id(
  void                  *profile,
  uint64               mask,
  const ds_profile_info_type  *info
)
{
  uint8 index = 0;

  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_LEVEL_USER_ID );
  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_pdn_level_user_id: Invalid len" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }

  /* clear the entire destination buffer before memcpy of requested data */
  memset((void*)((dsi_profile_3gpp2_type *)profile)->prf->pdn_level_user_id, 0,
         ds_profile_3gpp2_profile_params_desc_tbl[index].len );

  if( info->len != 0 )
  {
    memscpy( (void*)((dsi_profile_3gpp2_type *)profile)->prf->pdn_level_user_id,
    	     DS_PROFILE_3GPP2_PPP_MAX_USER_ID_LEN,
             info->buf, info->len );
  }

  ((dsi_profile_3gpp2_type *)profile)->prf->pdn_level_user_id_len = (uint8)info->len;
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_pdn_level_auth_password(
  void                  *profile,
  uint64               mask,
  const ds_profile_info_type  *info
)
{
  uint8 index =0;

  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_LEVEL_AUTH_PASSWORD );
  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_pdn_level_auth_password: "
                     "Invalid len" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }

  /*clear the entire destination buffer before memcpy of requested data */
  memset((void*)((dsi_profile_3gpp2_type *)profile)->prf->pdn_level_auth_password, 0,
         ds_profile_3gpp2_profile_params_desc_tbl[index].len );

  if( info->len != 0 )
  {
    memscpy( (void*)((dsi_profile_3gpp2_type *)profile)->prf->pdn_level_auth_password,
    	     DS_PROFILE_3GPP2_PPP_MAX_PASSWD_LEN,
             info->buf, info->len );
  }

  ((dsi_profile_3gpp2_type *)profile)->prf->pdn_level_auth_password_len = (uint8)info->len;
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_failure_timer_1(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  uint8 index = 0;

  index = dsi_profile_3gpp2_get_index_from_ident(
                               DS_PROFILE_3GPP2_PROFILE_PARAM_FAILURE_TIMER_1 );
  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_failure_timer_1: Invalid buf" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }

  memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->failure_timer[0],
  	   sizeof(uint32), info->buf, info->len );
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_failure_timer_2(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  uint8 index = 0;

  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_FAILURE_TIMER_2 );

  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_failure_timer_2: Invalid buf" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }

  memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->failure_timer[1],
           sizeof(uint32), info->buf, info->len );
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_failure_timer_3(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  uint8 index = 0;

  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_FAILURE_TIMER_3 );
  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_failure_timer_3: Invalid buf" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }

  memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->failure_timer[2],
           sizeof(uint32), info->buf, info->len );
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_failure_timer_4(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  uint8 index = 0;

  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_FAILURE_TIMER_4 );
  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_failure_timer_4: Invalid buf" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }

  memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->failure_timer[3],
           sizeof(uint32), info->buf, info->len );
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_failure_timer_5(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  uint8 index = 0;

  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_FAILURE_TIMER_5 );
  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_failure_timer_5: Invalid buf" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }

  memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->failure_timer[4],
           sizeof(uint32), info->buf, info->len );
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_failure_timer_6(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  uint8 index = 0;

  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_FAILURE_TIMER_6 );
  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_failure_timer_6: Invalid buf" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }

  memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->failure_timer[5],
           sizeof(uint32), info->buf, info->len );
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp2_set_disallow_timer_1(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  uint8 index = 0;

  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_DISALLOW_TIMER_1 );
  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_disallow_timer_1: Invalid buf" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }

  memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->disallow_timer[0],
            sizeof(uint32), info->buf, info->len );
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_disallow_timer_2(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  uint8 index = 0;

  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_DISALLOW_TIMER_2 );
  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_disallow_timer_2: Invalid buf" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }

  memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->disallow_timer[1],
            sizeof(uint32), info->buf, info->len );
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_disallow_timer_3(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  uint8 index = 0;

  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_DISALLOW_TIMER_3 );
  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_disallow_timer_3: Invalid buf" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }

  memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->disallow_timer[2],
           sizeof(uint32), info->buf, info->len );
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_disallow_timer_4(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  uint8 index = 0;

  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_DISALLOW_TIMER_4 );
  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_disallow_timer_4: Invalid buf" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }

  memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->disallow_timer[3],
            sizeof(uint32), info->buf, info->len );
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_disallow_timer_5(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  uint8 index = 0;

  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_DISALLOW_TIMER_5 );
  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_disallow_timer_5: Invalid buf" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }

  memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->disallow_timer[4],
           sizeof(uint32), info->buf, info->len );
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_disallow_timer_6(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  uint8 index = 0;

  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_DISALLOW_TIMER_6 );
  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_disallow_timer_6: Invalid buf" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }

  memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->disallow_timer[5],
           sizeof(uint32), info->buf, info->len );
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_failure_timers(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  uint8 index = 0;
   uint8 idx = 0;
  uint32 *timer_arr = NULL;
  ds_profile_3gpp2_param_enum_type ident 
                            = DS_PROFILE_3GPP2_PROFILE_PARAM_FAILURE_TIMER_1;

  if((NULL == profile) || (NULL == info))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR, "profile or info ptr is NULL" );
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  timer_arr = (uint32 *)info->buf;
  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_FAILURE_TIMERS);
  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_failure_timer: Invalid buf" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }

  for(index = 0; index < PDN_THROTTLE_MAX_TIMERS; index++)
  {
    memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->failure_timer[index],
    	     sizeof(uint32),
             (void*)&(timer_arr[index]), 
             (info->len)/PDN_THROTTLE_MAX_TIMERS );

    /* Set the mask for each of the failure timers*/
    idx = dsi_profile_3gpp2_get_index_from_ident(ident++);
    /* get mask from identifier */
    CONVERT_IDENT_TO_MASK(mask, idx);
    ((dsi_profile_3gpp2_type *)profile)->mask |= mask;
  }

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_disallow_timers(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  uint8 index = 0;
  uint8 idx = 0;
  uint32 *timer_arr = NULL;
  ds_profile_3gpp2_param_enum_type ident 
                            = DS_PROFILE_3GPP2_PROFILE_PARAM_DISALLOW_TIMER_1;

  if((NULL == profile) || (NULL == info))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR, "profile or info ptr is NULL" );
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  timer_arr = (uint32 *)info->buf;
  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_DISALLOW_TIMERS);
  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_disallow_timer: Invalid buf" );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }

  for(index = 0; index < PDN_THROTTLE_MAX_TIMERS; index++)
  {
    memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->disallow_timer[index],
    	     sizeof(uint32),
             &(timer_arr[index]), 
             (info->len)/PDN_THROTTLE_MAX_TIMERS );

    /* Set the mask for each of the disallow timers*/
    idx = dsi_profile_3gpp2_get_index_from_ident(ident++);
    /* get mask from identifier */
    CONVERT_IDENT_TO_MASK(mask, idx);
    ((dsi_profile_3gpp2_type *)profile)->mask |= mask;
  }

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_user_profile_class(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  uint8 index = 0;

  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_USER_PROFILE_CLASS );
  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DS_PROFILE_LOGE( "dsi_profile_3gpp2_set_user_profile_class: Invalid buf", 0 );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }

  memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->user_profile_class,
           sizeof(uint32), info->buf, info->len );
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_set_subs_id(
  void                       *profile,
  uint64                      mask,
  const ds_profile_info_type *info
)
{
  uint8 index = 0;
  ds_profile_3gpp2_subs_id_enum_type subs_id;

  index = dsi_profile_3gpp2_get_index_from_ident(
                                DS_PROFILE_3GPP2_PROFILE_PARAM_SUBS_ID );
  if(( info->len != 0 ) &&
     !(SET_INFO_IS_VALID(info,ds_profile_3gpp2_profile_params_desc_tbl[index].len)))
  {
    DS_PROFILE_LOGE( "dsi_profile_3gpp2_set_subs_id: Invalid buf", 0 );
    return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
  }

  subs_id = (ds_profile_3gpp2_subs_id_enum_type)((*(ds_profile_3gpp2_subs_id_enum_type*)info->buf));

  if(subs_id != DS_PROFILE_3GPP2_ACTIVE_SUBSCRIPTION_1)
  {
    DATA_IS707_MSG1( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_subs_id: "
                     "Invalid param value %d", 
                     subs_id );
    return DS_PROFILE_REG_RESULT_ERR_INVAL_SUBS_ID;
  }

  memscpy( (void*)&((dsi_profile_3gpp2_type *)profile)->prf->subs_id,
           sizeof(ds_profile_3gpp2_subs_id_enum_type), info->buf, info->len );
  ((dsi_profile_3gpp2_type *)profile)->mask |= mask;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/

/*---------------------------------------------------------------------------
                   ACCESSOR FUNCTIONS: GET
---------------------------------------------------------------------------*/

/*===========================================================================
FUNCTION dsi_profile_get_ group of functions

DESCRIPTION
  This set of accessor functions are used to get Profile
  parameter identified by mask from Profile blob

PARAMETERS
  -> profile : ptr to 3GPP2 Profile blob
  -> info    : ptr to mem containing data to be written to Profile blob

DEPENDENCIES

RETURN VALUE

SIDE EFFECTS
  none
===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_negotiate_dns_server(
  const void             *profile,
  ds_profile_info_type   *info
)
{
  memscpy( info->buf, info->len,
           (void*)&((dsi_profile_3gpp2_type *)profile)->prf->negotiate_dns_server,
           sizeof(ds_profile_3gpp2_negotiate_dns_server_type) );

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_ppp_session_close_timer_DO(
  const void            *profile,
  ds_profile_info_type  *info
)
{
  memscpy( info->buf, info->len,
           (void*)&((dsi_profile_3gpp2_type *)profile)->prf->ppp_session_close_timer_DO,
           sizeof(ds_profile_3gpp2_ppp_session_close_timer_DO_type) );

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_ppp_session_close_timer_1X(
  const void            *profile,
  ds_profile_info_type  *info
)
{
  memscpy( info->buf, info->len,
           (void*)&((dsi_profile_3gpp2_type *)profile)->prf->ppp_session_close_timer_1X,
           sizeof(ds_profile_3gpp2_ppp_session_close_timer_1X_type) );

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_allow_linger(
  const void            *profile,
  ds_profile_info_type  *info
)
{
  memscpy( info->buf, info->len,
           (void*)&((dsi_profile_3gpp2_type *)profile)->prf->allow_linger,
           sizeof(ds_profile_3gpp2_allow_linger_type) );

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_linger_timeout_val(
  const void            *profile,
  ds_profile_info_type  *info
)
{
  memscpy( info->buf, info->len,
           (void*)&((dsi_profile_3gpp2_type *)profile)->prf->linger_timeout_val,
           sizeof(ds_profile_3gpp2_linger_timeout_val_type) );

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_linger_params(
  const void            *profile,
  ds_profile_info_type  *info
)
{
  ds_profile_info_type  temp_info;
  ds_profile_3gpp2_linger_params_type iface_linger_params;

  memset(&iface_linger_params, 0, sizeof(ds_profile_3gpp2_linger_params_type));

  /* Get Allow linger flag */
  temp_info.len = sizeof(ds_profile_3gpp2_allow_linger_type);
  temp_info.buf = (void *)&( ((ds_profile_3gpp2_linger_params_type *)(info->buf))\
                               ->allow_linger_flag);
  (void)dsi_profile_3gpp2_get_allow_linger(profile, &temp_info);

  /* Get Linger timeout val */
  temp_info.len = sizeof(ds_profile_3gpp2_linger_timeout_val_type);
  temp_info.buf = (void *)&( ((ds_profile_3gpp2_linger_params_type *)(info->buf))\
                               ->linger_timeout_val);
  (void)dsi_profile_3gpp2_get_linger_timeout_val(profile, &temp_info);

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_lcp_ack_timeout(
  const void            *profile,
  ds_profile_info_type  *info
)
{
  memscpy( info->buf, info->len, 
           (void*)&((dsi_profile_3gpp2_type *)profile)->prf->lcp_ack_timeout,
           sizeof(ds_profile_3gpp2_lcp_ack_timeout_type) );

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_ipcp_ack_timeout(
  const void            *profile,
  ds_profile_info_type  *info
)
{
  memscpy( info->buf, info->len, 
           (void*)&((dsi_profile_3gpp2_type *)profile)->prf->ipcp_ack_timeout,
           sizeof(ds_profile_3gpp2_ipcp_ack_timeout_type) );

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_auth_timeout(
  const void            *profile,
  ds_profile_info_type  *info
)
{
  memscpy( info->buf, info->len,
           (void*)&((dsi_profile_3gpp2_type *)profile)->prf->auth_timeout,
           sizeof(ds_profile_3gpp2_auth_timeout_type) );

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_lcp_creq_retry_count(
  const void            *profile,
  ds_profile_info_type  *info
)
{
  memscpy( info->buf, info->len,
           (void*)&((dsi_profile_3gpp2_type *)profile)->prf->lcp_creq_retry_count,
           sizeof(ds_profile_3gpp2_lcp_creq_retry_count_type));

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_ipcp_creq_retry_count(
  const void            *profile,
  ds_profile_info_type  *info
)
{
  memscpy( info->buf, info->len,
           (void*)&((dsi_profile_3gpp2_type *)profile)->prf->ipcp_creq_retry_count,
           sizeof(ds_profile_3gpp2_ipcp_creq_retry_count_type) );

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_auth_retry_count(
  const void            *profile,
  ds_profile_info_type  *info
)
{
  memscpy( info->buf, info->len,
           (void*)&((dsi_profile_3gpp2_type *)profile)->prf->auth_retry_count,
           sizeof(ds_profile_3gpp2_auth_retry_count_type) );

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_auth_protocol(
  const void            *profile,
  ds_profile_info_type  *info
)
{
  memscpy( info->buf, info->len,
           (void*)&((dsi_profile_3gpp2_type *)profile)->prf->auth_protocol,
           sizeof(ds_profile_3gpp2_auth_protocol_type) );

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_user_id(
  const void            *profile,
  ds_profile_info_type  *info
)
{
  memscpy( info->buf,
  	   ((dsi_profile_3gpp2_type *)profile)->prf->user_id_len,
           (void*)((dsi_profile_3gpp2_type *)profile)->prf->user_id,
           ((dsi_profile_3gpp2_type *)profile)->prf->user_id_len );
  info->len = ((dsi_profile_3gpp2_type *)profile)->prf->user_id_len;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_auth_password(
  const void            *profile,
  ds_profile_info_type  *info
)
{
  memscpy( info->buf,
  	   ((dsi_profile_3gpp2_type *)profile)->prf->auth_password_len,
           (void*)((dsi_profile_3gpp2_type *)profile)->prf->auth_password,
           ((dsi_profile_3gpp2_type *)profile)->prf->auth_password_len );
  info->len = ((dsi_profile_3gpp2_type *)profile)->prf->auth_password_len;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_data_rate(
  const void            *profile,
  ds_profile_info_type  *info
)
{
  memscpy( info->buf, info->len,
           (void*)&((dsi_profile_3gpp2_type *)profile)->prf->data_rate,
           sizeof(ds_profile_3gpp2_data_rate_type) );

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_data_mode(
  const void            *profile,
  ds_profile_info_type  *info
)
{
  memscpy( info->buf, info->len,
           (void*)&((dsi_profile_3gpp2_type *)profile)->prf->data_mode,
           sizeof(ds_profile_3gpp2_data_mode_type) );

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_app_type(
  const void            *profile,
  ds_profile_info_type  *info
)
{
  memscpy( info->buf, info->len,
           (void*)&((dsi_profile_3gpp2_type *)profile)->prf->app_type,
           sizeof(ds_profile_3gpp2_app_type_type) );

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_app_priority(
  const void            *profile,
  ds_profile_info_type  *info
)
{
  memscpy( info->buf, info->len,
           (void*)&((dsi_profile_3gpp2_type *)profile)->prf->app_priority,
           sizeof(ds_profile_3gpp2_app_priority_type) );

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_apn_string(
  const void             *profile,
  ds_profile_info_type  *info
)
{
  memscpy( info->buf,
  	   ((dsi_profile_3gpp2_type *)profile)->prf->apn_string_len,
           (void*)((dsi_profile_3gpp2_type *)profile)->prf->apn_string,
           ((dsi_profile_3gpp2_type *)profile)->prf->apn_string_len );
  info->len = ((dsi_profile_3gpp2_type *)profile)->prf->apn_string_len;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_pdn_label(
  const void             *profile,
  ds_profile_info_type  *info
)
{
  if(NULL==profile || NULL==info)
  {
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }
  memscpy( info->buf, 
  	   ((dsi_profile_3gpp2_type *)profile)->prf->pdn_label_len,
           (void*)((dsi_profile_3gpp2_type *)profile)->prf->pdn_label,
           ((dsi_profile_3gpp2_type *)profile)->prf->pdn_label_len );  
  info->len = ((dsi_profile_3gpp2_type *)profile)->prf->pdn_label_len;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}
/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_mcc(
  const void             *profile,
  ds_profile_info_type  *info
)
{
  if(NULL==profile || NULL==info)
  {
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  memscpy( info->buf, info->len,  
           (void*)&((dsi_profile_3gpp2_type *)profile)->prf->mcc,
           sizeof(ds_profile_3gpp2_mcc_type) );  

  return DS_PROFILE_REG_RESULT_SUCCESS;
}
/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_mnc(
  const void             *profile,
  ds_profile_info_type  *info
)
{
  if(NULL==profile || NULL==info)
  {
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }

  memscpy( info->buf, info->len,
           (void*)&((dsi_profile_3gpp2_type *)profile)->prf->mnc,
           sizeof(ds_profile_3gpp2_mnc_type) );  

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_apn_class(
  const void            *profile,
  ds_profile_info_type  *info
)
{
  memscpy( info->buf, info->len,
           (void*)&((dsi_profile_3gpp2_type *)profile)->prf->apn_class,
           sizeof(ds_profile_3gpp2_apn_class) );

  DATA_IS707_MSG1( MSG_LEGACY_MED,
                   "dsi_profile_3gpp2_get_apn_class: %d", 
                  ((dsi_profile_3gpp2_type *)profile)->prf->apn_class);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_apn_enabled(
  const void            *profile,
  ds_profile_info_type  *info
)
{
  memscpy( info->buf, info->len, 
           (void*)&((dsi_profile_3gpp2_type *)profile)->prf->apn_enabled,
           sizeof(ds_profile_3gpp2_apn_enabled) );

  DATA_IS707_MSG1( MSG_LEGACY_MED,
                   "dsi_profile_3gpp2_get_apn_enabled: %d", 
                  ((dsi_profile_3gpp2_type *)profile)->prf->apn_enabled);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_pdn_type(
  const void            *profile,
  ds_profile_info_type  *info
)
{
  memscpy( info->buf, info->len,
           (void*)&((dsi_profile_3gpp2_type *)profile)->prf->pdn_type,
           sizeof(ds_profile_3gpp2_pdn_type_enum_type) );

  DATA_IS707_MSG1( MSG_LEGACY_MED,
                   "dsi_profile_3gpp2_get_pdn_type: %d", 
                  ((dsi_profile_3gpp2_type *)profile)->prf->pdn_type);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_is_pcscf_addr_needed(
  const void            *profile,
  ds_profile_info_type  *info
)
{
  memscpy( info->buf, info->len,
           (void*)&((dsi_profile_3gpp2_type *)profile)->prf->is_pcscf_addr_needed,
           sizeof(ds_profile_3gpp2_is_pcscf_addr_needed_type) );

  return DS_PROFILE_REG_RESULT_SUCCESS;
}
/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_op_pco_id(
  const void            *profile,
  ds_profile_info_type  *info
)
{
  memscpy( info->buf, info->len,
           (void*)&((dsi_profile_3gpp2_type *)profile)->prf->op_pco_id,
           sizeof(ds_profile_3gpp2_op_pco_id_type) );

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_v4_dns_addr_primary(
  const void            *profile,
  ds_profile_info_type  *info
)
{
  memscpy( info->buf, info->len,
           (void*)&((dsi_profile_3gpp2_type *)profile)->prf->v4_dns_addr[DS_PROFILE_3GPP2_PRIMARY_DNS_ADDR],
           sizeof(ds_profile_3gpp2_in_addr_type) );

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_v4_dns_addr_secondary(
  const void            *profile,
  ds_profile_info_type  *info
)
{
  memscpy( info->buf, info->len,
           (void*)&((dsi_profile_3gpp2_type *)profile)->prf->v4_dns_addr[DS_PROFILE_3GPP2_SECONDARY_DNS_ADDR],
           sizeof(ds_profile_3gpp2_in_addr_type) );

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_v6_dns_addr_primary(
  const void            *profile,
  ds_profile_info_type  *info
)
{
  memscpy( info->buf, info->len,
           (void*)&((dsi_profile_3gpp2_type *)profile)->prf->v6_dns_addr[DS_PROFILE_3GPP2_PRIMARY_DNS_ADDR],
           sizeof(ds_profile_3gpp2_in6_addr_type) );

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_v6_dns_addr_secondary(
  const void            *profile,
  ds_profile_info_type  *info
)
{
  memscpy( info->buf, info->len,
           (void*)&((dsi_profile_3gpp2_type *)profile)->prf->v6_dns_addr[DS_PROFILE_3GPP2_SECONDARY_DNS_ADDR],
           sizeof(ds_profile_3gpp2_in6_addr_type) );

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_rat_type(
  const void            *profile,
  ds_profile_info_type  *info
)
{
  memscpy( info->buf, info->len,
           (void*)&((dsi_profile_3gpp2_type *)profile)->prf->rat_type,
           sizeof(ds_profile_3gpp2_rat_type_enum_type) );

  DATA_IS707_MSG1( MSG_LEGACY_MED,
                   "dsi_profile_3gpp2_get_rat_type: %d", 
                  ((dsi_profile_3gpp2_type *)profile)->prf->rat_type);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_pdn_inactivity_timeout(
  const void            *profile,
  ds_profile_info_type  *info
)
{
  ds_profile_3gpp2_pdn_inactivity_timeout pdn_inactivity_timeout=0;

  /*--------------------------------------------------------------------------
   QMI requires PDN inactivity timer in min, converted from msec to min
   --------------------------------------------------------------------------*/
  memscpy( &pdn_inactivity_timeout, sizeof(pdn_inactivity_timeout),
           (void*)&((dsi_profile_3gpp2_type *)profile)->prf->pdn_inactivity_timeout,
           sizeof(ds_profile_3gpp2_pdn_inactivity_timeout) );
  CONV_MSEC_TO_MIN(pdn_inactivity_timeout);
  memscpy( info->buf, info->len,
  	   &pdn_inactivity_timeout, 
  	   sizeof(pdn_inactivity_timeout) );

  DATA_IS707_MSG1( MSG_LEGACY_MED,
                   "dsi_profile_3gpp2_get_pdn_inactivity_timeout: %d", 
                  pdn_inactivity_timeout);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_param_invalid(
  const void            *profile,
  ds_profile_info_type  *info
)
{
  DATA_IS707_MSG0( MSG_LEGACY_ERROR,"_3gpp2_get: Invalid identifier" );
  return DS_PROFILE_REG_RESULT_ERR_INVAL_IDENT;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_pdn_level_auth_protocol(
  const void            *profile,
  ds_profile_info_type  *info
)
{
  memscpy( info->buf, info->len,
           (void*)&((dsi_profile_3gpp2_type *)profile)->prf->pdn_level_auth_protocol,
           sizeof(uint8) );

  DATA_IS707_MSG1( MSG_LEGACY_MED,
                   "dsi_profile_3gpp2_get_pdn_level_auth_protocol: %d", 
                  ((dsi_profile_3gpp2_type *)profile)->prf->pdn_level_auth_protocol);
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_pdn_level_user_id(
  const void            *profile,
  ds_profile_info_type  *info
)
{
  memscpy( info->buf,
  	   ((dsi_profile_3gpp2_type *)profile)->prf->pdn_level_user_id_len,
           (void*)((dsi_profile_3gpp2_type *)profile)->prf->pdn_level_user_id,
           ((dsi_profile_3gpp2_type *)profile)->prf->pdn_level_user_id_len );
  info->len = ((dsi_profile_3gpp2_type *)profile)->prf->pdn_level_user_id_len;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_pdn_level_auth_password(
  const void            *profile,
  ds_profile_info_type  *info
)
{
  memscpy( info->buf,
  	   ((dsi_profile_3gpp2_type *)profile)->prf->pdn_level_auth_password_len,
           (void*)((dsi_profile_3gpp2_type *)profile)->prf->pdn_level_auth_password,
           ((dsi_profile_3gpp2_type *)profile)->prf->pdn_level_auth_password_len );
  info->len = ((dsi_profile_3gpp2_type *)profile)->prf->pdn_level_auth_password_len;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_failure_timer_1(
  const void             *profile,
  ds_profile_info_type  *info
)
{
  info->len = sizeof(ds_profile_3gpp2_failure_timer_type);
  memscpy( info->buf, info->len,
           &(((dsi_profile_3gpp2_type *)profile)->prf->failure_timer[0]),
           sizeof(uint32) );

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_failure_timer_2(
  const void             *profile,
  ds_profile_info_type  *info
)
{
  info->len = sizeof(ds_profile_3gpp2_failure_timer_type);
  memscpy( info->buf, info->len,
           &(((dsi_profile_3gpp2_type *)profile)->prf->failure_timer[1]),
           sizeof(uint32) );

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_failure_timer_3(
  const void             *profile,
  ds_profile_info_type  *info
)
{
  info->len = sizeof(ds_profile_3gpp2_failure_timer_type);
  memscpy( info->buf, info->len,
           &(((dsi_profile_3gpp2_type *)profile)->prf->failure_timer[2]),
           sizeof(uint32) );

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_failure_timer_4(
  const void             *profile,
  ds_profile_info_type  *info
)
{
  info->len = sizeof(ds_profile_3gpp2_failure_timer_type);
  memscpy( info->buf, info->len,
           &(((dsi_profile_3gpp2_type *)profile)->prf->failure_timer[3]),
           sizeof(uint32) );

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_failure_timer_5(
  const void             *profile,
  ds_profile_info_type  *info
)
{
  info->len = sizeof(ds_profile_3gpp2_failure_timer_type);
  memscpy( info->buf, info->len,
           &(((dsi_profile_3gpp2_type *)profile)->prf->failure_timer[4]),
           sizeof(uint32) );

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_failure_timer_6(
  const void             *profile,
  ds_profile_info_type  *info
)
{
  info->len = sizeof(ds_profile_3gpp2_failure_timer_type);
  memscpy( info->buf, info->len,
           &(((dsi_profile_3gpp2_type *)profile)->prf->failure_timer[5]),
           sizeof(uint32) );

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_disallow_timer_1(
  const void             *profile,
  ds_profile_info_type  *info
)
{
  info->len = sizeof(ds_profile_3gpp2_disallow_timer_type);
  memscpy( info->buf, info->len,
           &(((dsi_profile_3gpp2_type *)profile)->prf->disallow_timer[0]),
           sizeof(uint32) );

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_disallow_timer_2(
  const void             *profile,
  ds_profile_info_type  *info
)
{
  info->len = sizeof(ds_profile_3gpp2_disallow_timer_type);
  memscpy( info->buf, info->len,
           &(((dsi_profile_3gpp2_type *)profile)->prf->disallow_timer[1]),
           sizeof(uint32) );

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_disallow_timer_3(
  const void             *profile,
  ds_profile_info_type  *info
)
{
  info->len = sizeof(ds_profile_3gpp2_disallow_timer_type);
  memscpy( info->buf, info->len,
           &(((dsi_profile_3gpp2_type *)profile)->prf->disallow_timer[2]),
           sizeof(uint32) );

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_disallow_timer_4(
  const void             *profile,
  ds_profile_info_type  *info
)
{
  info->len = sizeof(ds_profile_3gpp2_disallow_timer_type);
  memscpy( info->buf, info->len,
           &(((dsi_profile_3gpp2_type *)profile)->prf->disallow_timer[3]),
           sizeof(uint32) );

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_disallow_timer_5(
  const void             *profile,
  ds_profile_info_type  *info
)
{
  info->len = sizeof(ds_profile_3gpp2_disallow_timer_type);
  memscpy( info->buf, info->len,
           &(((dsi_profile_3gpp2_type *)profile)->prf->disallow_timer[4]),
           sizeof(uint32) );

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_disallow_timer_6(
  const void             *profile,
  ds_profile_info_type  *info
)
{
  info->len = sizeof(ds_profile_3gpp2_disallow_timer_type);
  memscpy( info->buf, info->len,
           &(((dsi_profile_3gpp2_type *)profile)->prf->disallow_timer[5]),
           sizeof(uint32) );

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_failure_timers(
  const void             *profile,
  ds_profile_info_type  *info
)
{
  info->len = sizeof(ds_profile_3gpp2_failure_timer_type) 
                                                   * PDN_THROTTLE_MAX_TIMERS;
  memscpy( info->buf, info->len,
           &(((dsi_profile_3gpp2_type *)profile)->prf->failure_timer[0]),
           (sizeof(uint32) * PDN_THROTTLE_MAX_TIMERS) );

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_disallow_timers(
  const void             *profile,
  ds_profile_info_type  *info
)
{
  info->len = sizeof(ds_profile_3gpp2_disallow_timer_type)
                                                   * PDN_THROTTLE_MAX_TIMERS;
  memscpy( info->buf, info->len,
           &(((dsi_profile_3gpp2_type *)profile)->prf->disallow_timer[0]),
           (sizeof(uint32) * PDN_THROTTLE_MAX_TIMERS) );

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_user_profile_class(
  const void             *profile,
  ds_profile_info_type  *info
)
{
  info->len = sizeof(ds_profile_3gpp2_user_profile_class);
  memscpy( info->buf, info->len,
           &(((dsi_profile_3gpp2_type *)profile)->prf->user_profile_class),
           sizeof(uint32) );

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*=========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_get_subs_id(
  const void             *profile,
  ds_profile_info_type  *info
)
{
  info->len = sizeof(ds_profile_3gpp2_subs_id_enum_type);
  memscpy( info->buf, info->len,
           &(((dsi_profile_3gpp2_type *)profile)->prf->subs_id),
           sizeof(ds_profile_3gpp2_subs_id_enum_type) );

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*-----------------------------------------------------------------------------
  Table for parameter and its get/set functions
-----------------------------------------------------------------------------*/
static dsi_profile_acc_mut_fn_type ds_profile_3gpp2_acc_mut_fn_tbl[] = {

   /* dummy function, ident not valid*/
  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_INVALID,
  dsi_profile_3gpp2_set_param_invalid,
  dsi_profile_3gpp2_get_param_invalid},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_NEGOTIATE_DNS_SERVER,
  dsi_profile_3gpp2_set_negotiate_dns_server,
  dsi_profile_3gpp2_get_negotiate_dns_server},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_SESSION_CLOSE_TIMER_DO,
  dsi_profile_3gpp2_set_ppp_session_close_timer_DO,
  dsi_profile_3gpp2_get_ppp_session_close_timer_DO},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_SESSION_CLOSE_TIMER_1X,
  dsi_profile_3gpp2_set_ppp_session_close_timer_1X,
  dsi_profile_3gpp2_get_ppp_session_close_timer_1X},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_ALLOW_LINGER,
  dsi_profile_3gpp2_set_allow_linger,
  dsi_profile_3gpp2_get_allow_linger},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_LCP_ACK_TIMEOUT,
  dsi_profile_3gpp2_set_lcp_ack_timeout,
  dsi_profile_3gpp2_get_lcp_ack_timeout},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_IPCP_ACK_TIMEOUT,
  dsi_profile_3gpp2_set_ipcp_ack_timeout,
  dsi_profile_3gpp2_get_ipcp_ack_timeout},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_AUTH_TIMEOUT,
  dsi_profile_3gpp2_set_auth_timeout,
  dsi_profile_3gpp2_get_auth_timeout},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_LCP_CREQ_RETRY_COUNT,
  dsi_profile_3gpp2_set_lcp_creq_retry_count,
  dsi_profile_3gpp2_get_lcp_creq_retry_count},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_IPCP_CREQ_RETRY_COUNT,
  dsi_profile_3gpp2_set_ipcp_creq_retry_count,
  dsi_profile_3gpp2_get_ipcp_creq_retry_count},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_AUTH_RETRY_COUNT,
  dsi_profile_3gpp2_set_auth_retry_count,
  dsi_profile_3gpp2_get_auth_retry_count},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_AUTH_PROTOCOL,
  dsi_profile_3gpp2_set_auth_protocol,
  dsi_profile_3gpp2_get_auth_protocol},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_USER_ID,
  dsi_profile_3gpp2_set_user_id,
  dsi_profile_3gpp2_get_user_id},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_AUTH_PASSWORD,
  dsi_profile_3gpp2_set_auth_password,
  dsi_profile_3gpp2_get_auth_password},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_DATA_RATE,
  dsi_profile_3gpp2_set_data_rate,
  dsi_profile_3gpp2_get_data_rate},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_DATA_MODE,
  dsi_profile_3gpp2_set_data_mode,
  dsi_profile_3gpp2_get_data_mode},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_APP_TYPE,
  dsi_profile_3gpp2_set_app_type,
  dsi_profile_3gpp2_get_app_type},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_APP_PRIORITY,
  dsi_profile_3gpp2_set_app_priority,
  dsi_profile_3gpp2_get_app_priority},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_APN_STRING,
  dsi_profile_3gpp2_set_apn_string,
  dsi_profile_3gpp2_get_apn_string},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_TYPE,
  dsi_profile_3gpp2_set_pdn_type,
  dsi_profile_3gpp2_get_pdn_type},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_IS_PCSCF_ADDR_NEEDED,
  dsi_profile_3gpp2_set_is_pcscf_addr_needed,
  dsi_profile_3gpp2_get_is_pcscf_addr_needed},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_V4_DNS_ADDR_PRIMARY,
  dsi_profile_3gpp2_set_v4_dns_addr_primary,
  dsi_profile_3gpp2_get_v4_dns_addr_primary},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_V4_DNS_ADDR_SECONDARY,
  dsi_profile_3gpp2_set_v4_dns_addr_secondary,
  dsi_profile_3gpp2_get_v4_dns_addr_secondary},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_V6_DNS_ADDR_PRIMARY,
  dsi_profile_3gpp2_set_v6_dns_addr_primary,
  dsi_profile_3gpp2_get_v6_dns_addr_primary},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_V6_DNS_ADDR_SECONDARY,
  dsi_profile_3gpp2_set_v6_dns_addr_secondary,
  dsi_profile_3gpp2_get_v6_dns_addr_secondary},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_RAT_TYPE,
  dsi_profile_3gpp2_set_rat_type,
  dsi_profile_3gpp2_get_rat_type},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_LINGER_PARAMS,
   dsi_profile_3gpp2_set_linger_params,
   dsi_profile_3gpp2_get_linger_params},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_APN_ENABLED,
  dsi_profile_3gpp2_set_apn_enabled,
  dsi_profile_3gpp2_get_apn_enabled},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_INACTIVITY_TIMEOUT,
  dsi_profile_3gpp2_set_pdn_inactivity_timeout,
  dsi_profile_3gpp2_get_pdn_inactivity_timeout},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_APN_CLASS,
  dsi_profile_3gpp2_set_apn_class,
  dsi_profile_3gpp2_get_apn_class},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_LEVEL_AUTH_PROTOCOL,
  dsi_profile_3gpp2_set_pdn_level_auth_protocol,
  dsi_profile_3gpp2_get_pdn_level_auth_protocol},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_LEVEL_USER_ID,
  dsi_profile_3gpp2_set_pdn_level_user_id,
  dsi_profile_3gpp2_get_pdn_level_user_id},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_LEVEL_AUTH_PASSWORD,
  dsi_profile_3gpp2_set_pdn_level_auth_password,
  dsi_profile_3gpp2_get_pdn_level_auth_password},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_LABEL,
  dsi_profile_3gpp2_set_pdn_label,
  dsi_profile_3gpp2_get_pdn_label},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_FAILURE_TIMER_1,
  dsi_profile_3gpp2_set_failure_timer_1,
  dsi_profile_3gpp2_get_failure_timer_1},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_FAILURE_TIMER_2,
  dsi_profile_3gpp2_set_failure_timer_2,
  dsi_profile_3gpp2_get_failure_timer_2},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_FAILURE_TIMER_3,
  dsi_profile_3gpp2_set_failure_timer_3,
  dsi_profile_3gpp2_get_failure_timer_3},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_FAILURE_TIMER_4,
  dsi_profile_3gpp2_set_failure_timer_4,
  dsi_profile_3gpp2_get_failure_timer_4},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_FAILURE_TIMER_5,
  dsi_profile_3gpp2_set_failure_timer_5,
  dsi_profile_3gpp2_get_failure_timer_5},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_FAILURE_TIMER_6,
  dsi_profile_3gpp2_set_failure_timer_6,
  dsi_profile_3gpp2_get_failure_timer_6},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_DISALLOW_TIMER_1,
  dsi_profile_3gpp2_set_disallow_timer_1,
  dsi_profile_3gpp2_get_disallow_timer_1},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_DISALLOW_TIMER_2,
  dsi_profile_3gpp2_set_disallow_timer_2,
  dsi_profile_3gpp2_get_disallow_timer_2},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_DISALLOW_TIMER_3,
  dsi_profile_3gpp2_set_disallow_timer_3,
  dsi_profile_3gpp2_get_disallow_timer_3},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_DISALLOW_TIMER_4,
  dsi_profile_3gpp2_set_disallow_timer_4,
  dsi_profile_3gpp2_get_disallow_timer_4},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_DISALLOW_TIMER_5,
  dsi_profile_3gpp2_set_disallow_timer_5,
  dsi_profile_3gpp2_get_disallow_timer_5},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_DISALLOW_TIMER_6,
  dsi_profile_3gpp2_set_disallow_timer_6,
  dsi_profile_3gpp2_get_disallow_timer_6},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_OP_PCO_ID,
  dsi_profile_3gpp2_set_op_pco_id,
  dsi_profile_3gpp2_get_op_pco_id},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_MCC,
  dsi_profile_3gpp2_set_mcc,
  dsi_profile_3gpp2_get_mcc},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_MNC,
  dsi_profile_3gpp2_set_mnc,
  dsi_profile_3gpp2_get_mnc},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_FAILURE_TIMERS,
  dsi_profile_3gpp2_set_failure_timers,
  dsi_profile_3gpp2_get_failure_timers},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_DISALLOW_TIMERS,
  dsi_profile_3gpp2_set_disallow_timers,
  dsi_profile_3gpp2_get_disallow_timers},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_USER_PROFILE_CLASS,
  dsi_profile_3gpp2_set_user_profile_class,
  dsi_profile_3gpp2_get_user_profile_class},

  {(ds_profile_identifier_type)
      DS_PROFILE_3GPP2_PROFILE_PARAM_SUBS_ID,
  dsi_profile_3gpp2_set_subs_id,
  dsi_profile_3gpp2_get_subs_id}
};

/*===========================================================================
FUNCTION GET_VALID_MASK_FROM_IDENT

DESCRIPTION
  This function returns the valid mask of the identifier.

PARAMETERS
  param_id : identifier for which mask is to be returned

DEPENDENCIES

RETURN VALUE
  returns the valid mask for the identifier
SIDE EFFECTS

===========================================================================*/
/*lint -save -e656*/

ds_profile_3gpp2_valid_profile_enum_type get_valid_mask_from_ident(
  ds_profile_identifier_type param_id
)
{
  uint8 index = 0;
  for (index = 0;
       index < (sizeof(ds_profile_3gpp2_params_valid_mask)/
                sizeof(dsi_profile_3gpp2_params_valid_mask));
       index++ )
  {
    if ( param_id == ds_profile_3gpp2_params_valid_mask[index].ident )
    {
      DATA_IS707_MSG1( MSG_LEGACY_MED,
                       "get_valid_mask_from_ident: mask %d",
                      ds_profile_3gpp2_params_valid_mask[index].valid_mask );
      return ds_profile_3gpp2_params_valid_mask[index].valid_mask;
    }
  }
  DATA_IS707_MSG0( MSG_LEGACY_ERROR, "get_valid_mask_from_ident: param invalid" );
  return DS_PROFILE_3GPP2_PROFILE_INVALID;
}

/*===========================================================================
FUNCTION DSI_PROFILE_3GPP2_GET_INDEX_FROM_IDENT

DESCRIPTION
  This function gets index for the identifier to index into the function table.

PARAMETERS
  ident : identifier for which index is to be returned

DEPENDENCIES

RETURN VALUE
  returns index
SIDE EFFECTS

===========================================================================*/

uint8 dsi_profile_3gpp2_get_index_from_ident(
  ds_profile_identifier_type ident
)
{
  uint8 index = 0;
  uint8 i     = 0;

  for (i = 0;
       i < (sizeof(ds_profile_3gpp2_profile_params_desc_tbl)/
            sizeof(dsi_profile_params_desc_type));
       i++ )
  {
    if (ident == ds_profile_3gpp2_profile_params_desc_tbl[i].uid)
    {
      index = i;
      break;
    }
  }
  DATA_IS707_MSG2( MSG_LEGACY_MED,
                   "dsi_profile_3gpp2_get_index_from_ident: param %d, index %d",
                   ident, index );
  return index;
}

/*lint -restore Restore lint error 656*/
/*===========================================================================
FUNCTION DSI_PROFILE_3GPP2_ALLOC_PROFILE

DESCRIPTION
  This function is used to allocate memory which will be used for the local copy
  of the profile

PARAMETERS

DEPENDENCIES

RETURN VALUE
  pointer to which memory is allocated
  NULL on failure

SIDE EFFECTS

===========================================================================*/

static void* dsi_profile_3gpp2_alloc_profile(
  void
)
{
  dsi_profile_3gpp2_type *tmp_prf = NULL;

  tmp_prf = (dsi_profile_3gpp2_type *)DS_PROFILE_MEM_ALLOC(sizeof(dsi_profile_3gpp2_type),
                                                     MODEM_MEM_CLIENT_DATA);

  if (tmp_prf == NULL)
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_alloc_profile: mem alloc failed" );
    return NULL;
  }

  tmp_prf->prf = (ds_profile_3gpp2_profile_info_type *)DS_PROFILE_MEM_ALLOC(
      sizeof(ds_profile_3gpp2_profile_info_type),
      MODEM_MEM_CLIENT_DATA);

  if (tmp_prf->prf == NULL)
  {
    DS_PROFILE_MEM_FREE( (void *)tmp_prf, MODEM_MEM_CLIENT_DATA );
    DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_alloc_profile: mem alloc failed" );
    return NULL;
  }
  tmp_prf->mask = 0;
  tmp_prf->self = tmp_prf;
  return tmp_prf;
}

/*===========================================================================
FUNCTION DSI_PROFILE_3GPP2_DEALLOC_PROFILE

DESCRIPTION
  This function is used to free memory which was allocated for the local copy
  of the profile

PARAMETERS
  ptr : pointer to local copy of profile

DEPENDENCIES

RETURN VALUE
  DSI_SUCCESS
  DSI_FAILURE

SIDE EFFECTS

===========================================================================*/

static int dsi_profile_3gpp2_dealloc_profile(
  void *ptr
)
{

  if ( ptr == NULL || ( ((dsi_profile_3gpp2_type *)ptr)->prf == NULL ) )
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR, 
                     "dsi_profile_3gpp2_dealloc_profile: ptr NULL" );
    return DSI_FAILURE;
  }

  DS_PROFILE_MEM_FREE( (void *)((dsi_profile_3gpp2_type *)ptr)->prf,
        MODEM_MEM_CLIENT_DATA );
  DS_PROFILE_MEM_FREE( (void *)ptr,
                  MODEM_MEM_CLIENT_DATA );

  return DSI_SUCCESS;
}

/*===========================================================================
FUNCTION DSI_PROFILE_3GPP2_SET_PARAM

DESCRIPTION
  This function is used to set 3GPP2 parameter value in the local copy
  of the profile

PARAMETERS
  blob : pointer to local copy of profile
  ident : identifier whose value is to be set
  info : pointer to store value of identifier to be modified

DEPENDENCIES

RETURN VALUE
  DS_PROFILE_REG_RESULT_ERR_INVAL_IDENT
  DS_PROFILE_REG_3GPP2_ERR_INVALID_IDENT_FOR_PROFILE
  DS_PROFILE_REG_RESULT_ERR_LEN_INVALID
  DS_PROFILE_REG_RESULT_SUCCESS

SIDE EFFECTS

===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp2_set_param(
  void                        *blob,
  ds_profile_identifier_type   ident,
  const ds_profile_info_type  *info
)
{
  uint8  index = 0;
  uint64 mask  = 0;
  ds_profile_3gpp2_valid_profile_enum_type valid_mask = DS_PROFILE_3GPP2_PROFILE_INVALID;
  DATA_IS707_MSG1( MSG_LEGACY_MED,
                   "dsi_profile_3gpp2_set_param: param %d", ident );

  /* Validate identifier */
  if ( !DS_PROFILE_3GPP2_IDENT_IS_VALID( ident ) )
  {
    return DS_PROFILE_REG_RESULT_ERR_INVAL_IDENT;
  }
  /* check if identifier is valid for this profile */
  valid_mask = get_valid_mask_from_ident( ident );
 
  if ( !(((dsi_profile_3gpp2_type *)blob)->prf->profile_type & valid_mask) )
  {
    DATA_IS707_MSG2( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_set_param: Invalid profile type %d for"
                     "param valid mask %d",
                     ((dsi_profile_3gpp2_type *)blob)->prf->profile_type, 
                     valid_mask );
    return DS_PROFILE_REG_3GPP2_ERR_INVALID_IDENT_FOR_PROFILE;
  }

  index = dsi_profile_3gpp2_get_index_from_ident( ident );

  if( !( ( ident == DS_PROFILE_3GPP2_PROFILE_PARAM_APN_STRING)||
         ( ident == DS_PROFILE_3GPP2_PROFILE_PARAM_USER_ID) ||
         ( ident == DS_PROFILE_3GPP2_PROFILE_PARAM_AUTH_PASSWORD) ||
         ( ident == DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_LEVEL_USER_ID) ||
         ( ident == DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_LEVEL_AUTH_PASSWORD) ) )
  {
    /* Validate info->buf and info->len  for all other except
      APN, userid and password . They can have len as zero
    */
    if ( !SET_INFO_IS_VALID( info, ds_profile_3gpp2_profile_params_desc_tbl[index].len ) )
    {
      DATA_IS707_MSG1( MSG_LEGACY_ERROR,
                       "dsi_profile_3gpp2_set_param: Buff size invalid %d", 
                       info->len );
      return DS_PROFILE_REG_RESULT_ERR_LEN_INVALID;
    }
  }

  /* get mask from identifier */
  CONVERT_IDENT_TO_MASK( mask, index );
  return ds_profile_3gpp2_acc_mut_fn_tbl[index].set_fn(blob, mask, info);
}

/*===========================================================================
FUNCTION DSI_PROFILE_3GPP2_GET_PARAM

DESCRIPTION
  This function is used to get 3GPP2 parameter value from the local copy
  of the profile

PARAMETERS
  blob : pointer to local copy of profile
  ident : identifier to get value
  info : pointer to store value of identifier fetched

DEPENDENCIES

RETURN VALUE
  DS_PROFILE_REG_RESULT_ERR_INVAL_IDENT
  DS_PROFILE_REG_3GPP2_ERR_INVALID_IDENT_FOR_PROFILE
  DS_PROFILE_REG_RESULT_ERR_LEN_INVALID
  DS_PROFILE_REG_RESULT_SUCCESS

SIDE EFFECTS

===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp2_get_param(
  void                        *blob,
  ds_profile_identifier_type   ident,
  ds_profile_info_type        *info
)
{
  uint8 index = 0;
  ds_profile_3gpp2_valid_profile_enum_type valid_mask = DS_PROFILE_3GPP2_PROFILE_INVALID;
  DATA_IS707_MSG1( MSG_LEGACY_MED, "dsi_profile_3gpp2_get_param: param %d", ident );
  /* Validate identifier */
  if ( !DS_PROFILE_3GPP2_IDENT_IS_VALID( ident ) )
  {
    return DS_PROFILE_REG_RESULT_ERR_INVAL_IDENT;
  }
  /* check if identifier is valid for this profile */
  valid_mask = get_valid_mask_from_ident( ident );

  if ( !(((dsi_profile_3gpp2_type *)blob)->prf->profile_type & valid_mask) )
  {
    DATA_IS707_MSG2( MSG_LEGACY_ERROR,
                     "dsi_profile_3gpp2_get_param: Invalid profile type %d for "
                     "param valid mask %d",
                     ((dsi_profile_3gpp2_type *)blob)->prf->profile_type ,
                     valid_mask );
    return DS_PROFILE_REG_3GPP2_ERR_INVALID_IDENT_FOR_PROFILE;
  }

  index = dsi_profile_3gpp2_get_index_from_ident( ident );
  /* Validate info->buf and info->len */
  if ( !GET_INFO_IS_VALID( info, ds_profile_3gpp2_profile_params_desc_tbl[index].len ) )
  {
    DATA_IS707_MSG0( MSG_LEGACY_ERROR, "dsi_profile_3gpp2_get_param: Buff invalid");
    return DS_PROFILE_REG_RESULT_ERR_INVAL;
  }
  info->len = ds_profile_3gpp2_profile_params_desc_tbl[index].len;
  return ds_profile_3gpp2_acc_mut_fn_tbl[index].get_fn(blob, info);
}

/*===========================================================================
FUNCTION DS_PROFILE_3GPP2_INIT

DESCRIPTION
  This function is called on the library init. It initializes the function
  pointers to valid functions for 3gpp2

PARAMETERS
  fntbl : pointer to table of function pointers

DEPENDENCIES

RETURN VALUE
  returns the mask for 3gpp2. (Used later as valid mask which is ORed value
  returned from all techs.
SIDE EFFECTS

===========================================================================*/

uint8 ds_profile_3gpp2_init ( tech_fntbl_type *fntbl )
{
  /* Init function pointers */
  fntbl->alloc     = dsi_profile_3gpp2_alloc_profile;
  fntbl->dealloc   = dsi_profile_3gpp2_dealloc_profile;
  fntbl->set_param = dsi_profile_3gpp2_set_param;
  fntbl->get_param = dsi_profile_3gpp2_get_param;

  return (0x01 << DS_PROFILE_TECH_3GPP2);
}

/*lint -restore Restore lint error 655*/
/*lint -restore Restore lint error 641*/
