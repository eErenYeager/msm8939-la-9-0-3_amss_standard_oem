/******************************************************************************
  @file    ds_profile_3gpp2_modem.c
  @brief

  DESCRIPTION
  Tech specific implementation of 3GPP2 1x Profile Management

  INITIALIZATION AND SEQUENCING REQUIREMENTS
  N/A

  ---------------------------------------------------------------------------
  Copyright (C) 2009-2013 Qualcomm Technologies Incorporated.
  All Rights Reserved. QUALCOMM Proprietary and Confidential.
  ---------------------------------------------------------------------------
******************************************************************************/
/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/3gpp2/dsprofile/src/ds_profile_3gpp2_modem.c#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
07/28/14    hr     Data+MMS - DSDA changes Phase 1.
10/28/13   sd      LLVM Compiler Warnings clean-up - 3GPP2
06/26/13   sc      Replace all the memcpy and memmove with memscpy and 
                   memsmove as its safer.
12/02/12   jz      Support for Operator PCO  
05/25/12   jee     To support resetting DS profiles to default in 3gpp2
02/27/12   op      Fix to copy pdn level auth password length properly
12/08/11   ty      Added support for PDN throttle failure and disallow timers.
11/10/11   jz      Add PDN Label
10/27/11   op      Added support for PDN level authentication
08/03/11   var     Modified get_list not to add default profile to the list 
                   for invalid profiles
06/03/11   mg      Added call to check mandatory APNs when profile's APN enabled
                   flag is changed.
05/25/11   ack     Adding profile persistence option
05/06/11   ack     Added create profile api support for 3GPP2
04/29/11   ack     Write profile changes to efs
03/15/11   mg      Global variable cleanup
02/16/11   ttv     Added DSDS changes for consolidated profile family.
01/21/11   mg      Added support for APN class field
01/17/11   ss      Added support for Iface linger.
10/20/10   op      Added support for PDN throttling timers in profiles
04/30/10   ars     Made modem_mem_alloc/free and str fns platform independent
09/30/09   mg      Created the module. First version of the file.
===========================================================================*/

#include "datamodem_variation.h"
#include "ds707_data_session_profile.h"
#include "ds_profile_3gpp2i.h"
#include "ds_profile_os.h"
#include "ds_util.h"
#include "data_msg.h"
#include <stringl/stringl.h>

/*lint -save -e641*/
/*lint -save -e655*/
/*===========================================================================
FUNCTION DSI_PROFILE_3GPP2_CHECK_PARAM_VAL

DESCRIPTION
  This function is used in list functions. This is used to check if the parameter
  value matches with the search value specified to form the list

PARAMETERS
  index : index into data session profile memory
  ident : identifier for parameter
  info : pointer to memory containing data to be matched with paramter
         value

DEPENDENCIES

RETURN VALUE
  DSI_SUCCESS
  DSI_FAILURE

SIDE EFFECTS

===========================================================================*/

static int dsi_profile_3gpp2_check_param_val(
  ds707_data_session_profile_info_type * profile_info_ptr,
  ds_profile_identifier_type ident,
  const ds_profile_info_type *info
)
{
  int return_status = DSI_FAILURE;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  if (profile_info_ptr == NULL)
  {
    return DSI_FAILURE;
  }
  DATA_IS707_MSG2(MSG_LEGACY_MED,
                  "dsi_profile_3gpp2_check_param_val: "
                  "checking for profile %d, param %d",
                  profile_info_ptr->data_session_profile_id, 
                  ident );

  switch( ident )
  {
    case DS_PROFILE_3GPP2_PROFILE_PARAM_NEGOTIATE_DNS_SERVER:
      if( 0 == memcmp( &(profile_info_ptr->negotiate_dns_server),
                       (ds_profile_3gpp2_negotiate_dns_server_type *)(info->buf),
                       sizeof(ds_profile_3gpp2_negotiate_dns_server_type) ) )
        return_status = DSI_SUCCESS;
    break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_SESSION_CLOSE_TIMER_DO:
      if( 0 == memcmp( &(profile_info_ptr->ppp_session_close_timer_DO),
                       (ds_profile_3gpp2_ppp_session_close_timer_DO_type *)(info->buf),
                       sizeof(ds_profile_3gpp2_ppp_session_close_timer_DO_type) ) )
        return_status = DSI_SUCCESS;
    break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_SESSION_CLOSE_TIMER_1X:
      if( 0 == memcmp( &(profile_info_ptr->ppp_session_close_timer_1X),
                       (ds_profile_3gpp2_ppp_session_close_timer_1X_type *)(info->buf),
                       sizeof(ds_profile_3gpp2_ppp_session_close_timer_1X_type) ) )
        return_status = DSI_SUCCESS;
    break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_ALLOW_LINGER:
      if( 0 == memcmp( &(profile_info_ptr->allow_linger),
                       (ds_profile_3gpp2_allow_linger_type *)(info->buf),
                       sizeof(ds_profile_3gpp2_allow_linger_type) ) )
        return_status = DSI_SUCCESS;
    break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_LINGER_PARAMS:
      if( 0 == memcmp( &(profile_info_ptr->allow_linger),
                       (ds_profile_3gpp2_allow_linger_type *)(info->buf),
                       sizeof(ds_profile_3gpp2_allow_linger_type) ) )

       if( 0 == memcmp( &(profile_info_ptr->linger_timeout_val),
                       (ds_profile_3gpp2_linger_timeout_val_type *)(info->buf),
                       sizeof(ds_profile_3gpp2_linger_timeout_val_type) ) )
        return_status = DSI_SUCCESS;
    break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_LCP_ACK_TIMEOUT:
      if( 0 == memcmp( &(profile_info_ptr->lcp_ack_timeout),
                       (ds_profile_3gpp2_lcp_ack_timeout_type *)(info->buf),
                       sizeof(ds_profile_3gpp2_lcp_ack_timeout_type) ) )
        return_status = DSI_SUCCESS;
    break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_IPCP_ACK_TIMEOUT:
      if( 0 == memcmp( &(profile_info_ptr->ipcp_ack_timeout),
                       (ds_profile_3gpp2_ipcp_ack_timeout_type *)(info->buf),
                       sizeof(ds_profile_3gpp2_ipcp_ack_timeout_type) ) )
        return_status = DSI_SUCCESS;
    break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_AUTH_TIMEOUT:
      if( 0 == memcmp( &(profile_info_ptr->auth_timeout),
                       (ds_profile_3gpp2_auth_timeout_type *)(info->buf),
                       sizeof(ds_profile_3gpp2_auth_timeout_type) ) )
        return_status = DSI_SUCCESS;
    break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_LCP_CREQ_RETRY_COUNT:
      if( 0 == memcmp( &(profile_info_ptr->lcp_creq_retry_count),
                       (ds_profile_3gpp2_lcp_creq_retry_count_type *)(info->buf),
                       sizeof(ds_profile_3gpp2_lcp_creq_retry_count_type) ) )
        return_status = DSI_SUCCESS;
    break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_IPCP_CREQ_RETRY_COUNT:
      if( 0 == memcmp( &(profile_info_ptr->ipcp_creq_retry_count),
                       (ds_profile_3gpp2_ipcp_creq_retry_count_type *)(info->buf),
                       sizeof(ds_profile_3gpp2_ipcp_creq_retry_count_type) ) )
        return_status = DSI_SUCCESS;
    break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_AUTH_RETRY_COUNT:
      if( 0 == memcmp( &(profile_info_ptr->auth_retry_count),
                       (ds_profile_3gpp2_auth_retry_count_type *)(info->buf),
                       sizeof(ds_profile_3gpp2_auth_retry_count_type) ) )
        return_status = DSI_SUCCESS;
    break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_AUTH_PROTOCOL:
      if( 0 == memcmp( &(profile_info_ptr->auth_protocol),
                       (ds_profile_3gpp2_auth_protocol_type *)(info->buf),
                       sizeof(ds_profile_3gpp2_auth_protocol_type) ) )
        return_status = DSI_SUCCESS;
    break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_USER_ID:
      if ( profile_info_ptr->user_id_len == info->len )
      {
        if ( 0 == DS_PROFILE_STR_CMP( profile_info_ptr->user_id,
                                      (char *)(info->buf),
                                      profile_info_ptr->user_id_len ) )
        {
          return_status = DSI_SUCCESS;
        }
      }
    break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_AUTH_PASSWORD:
      if ( profile_info_ptr->auth_password_len == info->len )
      {
        if ( 0 == DS_PROFILE_STR_CMP( profile_info_ptr->auth_password,
                                      (char *)(info->buf),
                                      profile_info_ptr->auth_password_len ) )
        {
          return_status = DSI_SUCCESS;
        }
      }
    break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_DATA_RATE:
      if( 0 == memcmp( &(profile_info_ptr->data_rate),
                       (ds_profile_3gpp2_data_rate_type *)(info->buf),
                       sizeof(ds_profile_3gpp2_data_rate_type) ) )
        return_status = DSI_SUCCESS;
    break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_DATA_MODE:
      if( 0 == memcmp( &(profile_info_ptr->data_mode),
                       (ds_profile_3gpp2_data_mode_type *)(info->buf),
                       sizeof(ds_profile_3gpp2_data_mode_type) ) )
        return_status = DSI_SUCCESS;
    break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_APP_TYPE:
      if( (profile_info_ptr->app_type) &
          (*((ds_profile_3gpp2_app_type_type *)(info->buf))) )
      {
        return_status = DSI_SUCCESS;
      }

      DATA_IS707_MSG3(MSG_LEGACY_MED,
                      "app_type in profile %d, app type passed %d, "
                      "return_status %d",
                      profile_info_ptr->app_type,
                      (*((ds_profile_3gpp2_app_type_type *)(info->buf))),
                      return_status);
    break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_APP_PRIORITY:
      if( 0 == memcmp( &(profile_info_ptr->app_priority),
                       (ds_profile_3gpp2_app_priority_type *)(info->buf),
                       sizeof(ds_profile_3gpp2_app_priority_type) ) )
        return_status = DSI_SUCCESS;
    break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_APN_STRING:
      if ( profile_info_ptr->apn_string_len == info->len )
      {
        if ( 0 == DS_PROFILE_STR_CASE_CMP( profile_info_ptr->apn_string,
                                      (char *)(info->buf),
                                      profile_info_ptr->apn_string_len ) )
        {
          return_status = DSI_SUCCESS;
        }
      }

      DATA_IS707_MSG3(MSG_LEGACY_MED,
                "APN string len in profile %d, length passed %d, "
                "return_status %d",
                profile_info_ptr->apn_string_len,
                info->len, return_status);
    break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_TYPE:
      if( 0 == memcmp( &(profile_info_ptr->pdn_type),
                       (ds_profile_3gpp2_pdn_type_enum_type *)(info->buf),
                       sizeof(ds_profile_3gpp2_pdn_type_enum_type) ) )
        return_status = DSI_SUCCESS;
    break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_IS_PCSCF_ADDR_NEEDED:
      if( 0 == memcmp( &(profile_info_ptr->is_pcscf_addr_needed),
                       (ds_profile_3gpp2_is_pcscf_addr_needed_type *)(info->buf),
                       sizeof(ds_profile_3gpp2_is_pcscf_addr_needed_type) ) )
        return_status = DSI_SUCCESS;
    break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_OP_PCO_ID:
      if( 0 == memcmp( &(profile_info_ptr->op_pco_id),
                       (uint16*)(info->buf),
                       sizeof(uint16) ) )
        return_status = DSI_SUCCESS;
    break;
     case DS_PROFILE_3GPP2_PROFILE_PARAM_MCC:
       if( 0 == memcmp( &(profile_info_ptr->mcc),
                        (uint16*)(info->buf),
                        sizeof(uint16) ) )
         return_status = DSI_SUCCESS;
     break;
     case DS_PROFILE_3GPP2_PROFILE_PARAM_MNC:
       if( 0 == memcmp( &(profile_info_ptr->mnc),
                        (uint16*)(info->buf),
                        sizeof(uint16) ) )
          return_status = DSI_SUCCESS;
    break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_V4_DNS_ADDR_PRIMARY:
      if( 0 == memcmp( &(profile_info_ptr->v4_dns_addr[DS_PROFILE_3GPP2_PRIMARY_DNS_ADDR]),
                       (ds_profile_3gpp2_in_addr_type *)(info->buf),
                       sizeof(ds_profile_3gpp2_in_addr_type) ) )
        return_status = DSI_SUCCESS;
    break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_V4_DNS_ADDR_SECONDARY:
      if( 0 == memcmp( &(profile_info_ptr->v4_dns_addr[DS_PROFILE_3GPP2_SECONDARY_DNS_ADDR]),
                       (ds_profile_3gpp2_in_addr_type *)(info->buf),
                       sizeof(ds_profile_3gpp2_in_addr_type) ) )
        return_status = DSI_SUCCESS;
    break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_V6_DNS_ADDR_PRIMARY:
      if( 0 == memcmp( &(profile_info_ptr->v6_dns_addr[DS_PROFILE_3GPP2_PRIMARY_DNS_ADDR]),
                       (ds_profile_3gpp2_in6_addr_type *)(info->buf),
                       sizeof(ds_profile_3gpp2_in6_addr_type) ) )
        return_status = DSI_SUCCESS;
    break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_V6_DNS_ADDR_SECONDARY:
      if( 0 == memcmp( &(profile_info_ptr->v6_dns_addr[DS_PROFILE_3GPP2_SECONDARY_DNS_ADDR]),
                       (ds_profile_3gpp2_in6_addr_type *)(info->buf),
                       sizeof(ds_profile_3gpp2_in6_addr_type) ) )
        return_status = DSI_SUCCESS;
    break;

   case DS_PROFILE_3GPP2_PROFILE_PARAM_RAT_TYPE:
      if( 0 == memcmp( &(profile_info_ptr->rat_type),
                       (ds_profile_3gpp2_rat_type_enum_type *)(info->buf),
                       sizeof(ds_profile_3gpp2_rat_type_enum_type) ) )
        return_status = DSI_SUCCESS;
    break;

   case DS_PROFILE_3GPP2_PROFILE_PARAM_APN_CLASS:
      if( 0 == memcmp( &(profile_info_ptr->apn_class),
                       (ds_profile_3gpp2_apn_class *)(info->buf),
                       sizeof(ds_profile_3gpp2_apn_class) ) )
      {
        return_status = DSI_SUCCESS;
      }

      DATA_IS707_MSG3(MSG_LEGACY_MED,
                      "APN class in profile %d, APN class passed %d, "
                      "return_status %d",
                      profile_info_ptr->apn_class,
                      (*((ds_profile_3gpp2_apn_class *)(info->buf))),
                      return_status);
    break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_LEVEL_AUTH_PROTOCOL:
      if( 0 == memcmp( &(profile_info_ptr->pdn_level_auth_protocol),
                       (ds_profile_3gpp2_auth_protocol_type *)(info->buf),
                       sizeof(ds_profile_3gpp2_auth_protocol_type) ) )
        return_status = DSI_SUCCESS;
    break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_LEVEL_USER_ID:
      if ( profile_info_ptr->pdn_level_user_id_len == info->len )
      {
        if ( 0 == DS_PROFILE_STR_CMP( profile_info_ptr->pdn_level_user_id,
                                      (char *)(info->buf),
                                      profile_info_ptr->pdn_level_user_id_len ) )
        {
          return_status = DSI_SUCCESS;
        }
      }
    break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_LEVEL_AUTH_PASSWORD:
      if ( profile_info_ptr->pdn_level_auth_password_len == info->len )
      {
        if ( 0 == DS_PROFILE_STR_CMP( profile_info_ptr->pdn_level_auth_password,
                                      (char *)(info->buf),
                                      profile_info_ptr->pdn_level_auth_password_len ) )
        {
          return_status = DSI_SUCCESS;
        }
      }
    break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_LABEL:
      if ( profile_info_ptr->pdn_label_len == info->len )
      {
        if ( 0 == DS_PROFILE_STR_CMP( profile_info_ptr->pdn_label, 
                                      (char *)(info->buf), 
                                      profile_info_ptr->pdn_label_len ) )
        {
          return_status = DSI_SUCCESS;
        }
      }
      DATA_IS707_MSG3(MSG_LEGACY_MED,
                      "pdn_label_len in profile %d, length passed %d, "
                      "return_status %d",
                      profile_info_ptr->pdn_label_len,
                      info->len, return_status);
    break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_FAILURE_TIMER_1:
      if( 0 == memcmp( &(profile_info_ptr->failure_timer[0]), 
                       (ds_profile_3gpp2_failure_timer_type *) (info->buf), 
                       info->len ))
      {
        return_status = DSI_SUCCESS;
      }
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_FAILURE_TIMER_2:
      if( 0 == memcmp( &(profile_info_ptr->failure_timer[1]), 
                       (ds_profile_3gpp2_failure_timer_type *) (info->buf), 
                       info->len ))
      {
        return_status = DSI_SUCCESS;
      }
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_FAILURE_TIMER_3:
      if( 0 == memcmp( &(profile_info_ptr->failure_timer[2]), 
                       (ds_profile_3gpp2_failure_timer_type *) (info->buf), 
                       info->len ))
      {
        return_status = DSI_SUCCESS;
      }
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_FAILURE_TIMER_4:
      if( 0 == memcmp( &(profile_info_ptr->failure_timer[3]), 
                       (ds_profile_3gpp2_failure_timer_type *) (info->buf), 
                       info->len ))
      {
        return_status = DSI_SUCCESS;
      }
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_FAILURE_TIMER_5:
      if( 0 == memcmp( &(profile_info_ptr->failure_timer[4]), 
                       (ds_profile_3gpp2_failure_timer_type *) (info->buf), 
                       info->len ))
      {
        return_status = DSI_SUCCESS;
      }
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_FAILURE_TIMER_6:
      if( 0 == memcmp( &(profile_info_ptr->failure_timer[5]), 
                       (ds_profile_3gpp2_failure_timer_type *) (info->buf), 
                       info->len ))
      {
        return_status = DSI_SUCCESS;
      }
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_DISALLOW_TIMER_1:
      if( 0 == memcmp( &(profile_info_ptr->disallow_timer[0]), 
                       (ds_profile_3gpp2_disallow_timer_type *) (info->buf), 
                       info->len ))
      {
        return_status = DSI_SUCCESS;
      }
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_DISALLOW_TIMER_2:
      if( 0 == memcmp( &(profile_info_ptr->disallow_timer[1]), 
                       (ds_profile_3gpp2_disallow_timer_type *) (info->buf), 
                       info->len ))
      {
        return_status = DSI_SUCCESS;
      }
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_DISALLOW_TIMER_3:
      if( 0 == memcmp( &(profile_info_ptr->disallow_timer[2]), 
                       (ds_profile_3gpp2_disallow_timer_type *) (info->buf), 
                       info->len ))
      {
        return_status = DSI_SUCCESS;
      }
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_DISALLOW_TIMER_4:
      if( 0 == memcmp( &(profile_info_ptr->disallow_timer[3]), 
                       (ds_profile_3gpp2_disallow_timer_type *) (info->buf), 
                       info->len ))
      {
        return_status = DSI_SUCCESS;
      }
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_DISALLOW_TIMER_5:
      if( 0 == memcmp( &(profile_info_ptr->disallow_timer[4]), 
                       (ds_profile_3gpp2_disallow_timer_type *) (info->buf), 
                       info->len ))
      {
        return_status = DSI_SUCCESS;
      }
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_DISALLOW_TIMER_6:
      if( 0 == memcmp( &(profile_info_ptr->disallow_timer[5]), 
                       (ds_profile_3gpp2_disallow_timer_type *) (info->buf), 
                       info->len ))
      {
        return_status = DSI_SUCCESS;
      }
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_USER_PROFILE_CLASS:
      if( 0 == memcmp( &(profile_info_ptr->user_profile_class), 
                       (ds_profile_3gpp2_user_profile_class *) (info->buf), 
                       info->len ))
      {
        return_status = DSI_SUCCESS;
      }
      break;


    case DS_PROFILE_3GPP2_PROFILE_PARAM_SUBS_ID:
      if( 0 == memcmp( &(profile_info_ptr->subs_id), 
                       (ds_profile_3gpp2_subs_id_enum_type *) (info->buf), 
                       info->len ))
      {
        return_status = DSI_SUCCESS;
      }
      break;

    default:
      return_status = DSI_FAILURE;
    break;
  }
  return return_status;
}

/*===========================================================================
FUNCTION DSI_PROFILE_3GPP2_MODEM_READ

DESCRIPTION
  This function is used to read a profile from modem to the local copy

PARAMETERS
  num : profile number
  ptr : pointer to profile blob

DEPENDENCIES

RETURN VALUE
  DS_PROFILE_REG_RESULT_SUCCESS
  DS_PROFILE_REG_RESULT_FAIL
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_NUM

SIDE EFFECTS

===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp2_modem_read(
  ds_profile_num_type   num,
  void                 *ptr,
  ds_profile_subs_etype subs
)
{
  ds707_data_session_profile_info_type * profile_info_ptr = NULL;

  if (subs != DS_PROFILE_ACTIVE_SUBSCRIPTION_1)
  {
    return DS_PROFILE_REG_RESULT_ERR_INVAL_SUBS_ID;
  }

  DATA_IS707_MSG1( MSG_LEGACY_MED,
                   "dsi_profile_3gpp2_modem_read: profile number %d", 
                   num );

  ds707_data_session_get_profile( num,
                                  &profile_info_ptr );

  ds_profile_mem_copy((void *)(((dsi_profile_3gpp2_type *)ptr)->prf),
                      (void *)profile_info_ptr);

  ((dsi_profile_3gpp2_type *)ptr)->mask = 0;

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================
FUNCTION DSI_PROFILE_UPDATE_CUSTOM_VAL_MASK

DESCRIPTION
  For a given ds_profile_identifier_type, updates the corresponding bit masks
  in ds707_data_session_profile_info_type custom_val_mask. We cannot directly
  copy over bits set in dsi_profile_3gpp2_type mask to
  ds707_data_session_profile_info_type custom_val_mask. These two masks
  though identical aren't the same. ds707_data_session_profile_info_type
  custom_val_mask contains bit mask for each and every profile parameter whereas
  the dsi_profile_3gpp2_type mask (that gets set by SET_PARAM api call) doesn't.
  E.g:
    When dsi_profile_3gpp2_set_linger_params() updates two profile params
    allow_linger_flag and linger_timeout_val, dsi_profile_3gpp2_type mask
    is set for ident: DS_PROFILE_3GPP2_PROFILE_PARAM_LINGER_PARAMS

    So in ds707_data_session_profile_info_type custom_val_mask, we need to set
    the masks that correspond to both ALLOW_LINGER and LINGER_TIMEOUT_VAL

PARAMETERS
  ident: profile param identifier

DEPENDENCIES

RETURN VALUE
  ds707_data_session_profile_info_type custom_val_mask corresponding to
  ds_profile_identifier_type

SIDE EFFECTS

===========================================================================*/
static uint64 dsi_profile_update_custom_val_mask(
  ds_profile_identifier_type ident
)
{
  uint64 custom_val_mask = 0;

  DATA_IS707_MSG1( MSG_LEGACY_MED,
                   "dsi_profile_update_custom_val_mask: param ident: %d",
                   ident );

  switch( ident )
  {
    case DS_PROFILE_3GPP2_PROFILE_PARAM_NEGOTIATE_DNS_SERVER:
      CONVERT_IDENT_TO_MASK(custom_val_mask, NEGOTIATE_DNS_SERVER);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_SESSION_CLOSE_TIMER_DO:
      CONVERT_IDENT_TO_MASK(custom_val_mask, PPP_SESSION_CLOSE_TIMER_DO);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_SESSION_CLOSE_TIMER_1X:
      CONVERT_IDENT_TO_MASK(custom_val_mask, PPP_SESSION_CLOSE_TIMER_1X);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_ALLOW_LINGER:
      CONVERT_IDENT_TO_MASK(custom_val_mask, ALLOW_LINGER);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_LCP_ACK_TIMEOUT:
      CONVERT_IDENT_TO_MASK(custom_val_mask, LCP_ACK_TIMEOUT_VAL);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_IPCP_ACK_TIMEOUT:
      CONVERT_IDENT_TO_MASK(custom_val_mask, IPCP_ACK_TIMEOUT_VAL);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_AUTH_TIMEOUT:
      CONVERT_IDENT_TO_MASK(custom_val_mask, AUTH_TIMEOUT_VAL);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_LCP_CREQ_RETRY_COUNT:
      CONVERT_IDENT_TO_MASK(custom_val_mask, LCP_CREQ_RETRY_COUNT);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_IPCP_CREQ_RETRY_COUNT:
      CONVERT_IDENT_TO_MASK(custom_val_mask, IPCP_CREQ_RETRY_COUNT);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_AUTH_RETRY_COUNT:
      CONVERT_IDENT_TO_MASK(custom_val_mask, AUTH_RETRY_COUNT);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_AUTH_PROTOCOL:
      CONVERT_IDENT_TO_MASK(custom_val_mask, AUTH_PROTOCOL);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_USER_ID:
      CONVERT_IDENT_TO_MASK(custom_val_mask, PPP_USER_ID);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_AUTH_PASSWORD:
      CONVERT_IDENT_TO_MASK(custom_val_mask, PPP_PASSWORD);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_DATA_RATE:
      CONVERT_IDENT_TO_MASK(custom_val_mask, DATA_RATE);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_DATA_MODE:
      CONVERT_IDENT_TO_MASK(custom_val_mask, DATA_MODE);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_APP_TYPE:
      CONVERT_IDENT_TO_MASK(custom_val_mask, APP_TYPE);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_APP_PRIORITY:
      CONVERT_IDENT_TO_MASK(custom_val_mask, APP_PRIORITY);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_APN_STRING:
      CONVERT_IDENT_TO_MASK(custom_val_mask, APN_STRING);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_TYPE:
      CONVERT_IDENT_TO_MASK(custom_val_mask, PDN_IP_VERSION_TYPE);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_IS_PCSCF_ADDR_NEEDED:
      CONVERT_IDENT_TO_MASK(custom_val_mask, PCSCF_ADDR_NEEDED);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_V4_DNS_ADDR_PRIMARY:
      CONVERT_IDENT_TO_MASK(custom_val_mask, PRIMARY_DNS_ADDR_V4);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_V4_DNS_ADDR_SECONDARY:
      CONVERT_IDENT_TO_MASK(custom_val_mask, SECONDARY_DNS_ADDR_V4);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_V6_DNS_ADDR_PRIMARY:
      CONVERT_IDENT_TO_MASK(custom_val_mask, PRIMARY_DNS_ADDR_V6);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_V6_DNS_ADDR_SECONDARY:
      CONVERT_IDENT_TO_MASK(custom_val_mask, SECONDARY_DNS_ADDR_V6);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_RAT_TYPE:
      CONVERT_IDENT_TO_MASK(custom_val_mask, RAN_TYPE);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_LINGER_PARAMS:
      /* Two profile params are set with one identifier*/
      CONVERT_IDENT_TO_MASK(custom_val_mask, ALLOW_LINGER);
      custom_val_mask |= (uint64)((uint64) 1 << LINGER_TIMEOUT_VAL);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_APN_ENABLED:
      CONVERT_IDENT_TO_MASK(custom_val_mask, APN_ENABLED);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_INACTIVITY_TIMEOUT:
      CONVERT_IDENT_TO_MASK(custom_val_mask, PDN_INACTIVITY_TIMEOUT);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_APN_CLASS:
      CONVERT_IDENT_TO_MASK(custom_val_mask, APN_CLASS);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_LEVEL_AUTH_PROTOCOL:
      CONVERT_IDENT_TO_MASK(custom_val_mask, PDN_LEVEL_AUTH_PROTOCOL);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_LEVEL_USER_ID:
      CONVERT_IDENT_TO_MASK(custom_val_mask, PDN_LEVEL_USER_ID);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_LEVEL_AUTH_PASSWORD:
      CONVERT_IDENT_TO_MASK(custom_val_mask, PDN_LEVEL_AUTH_PASSWORD);
      break;  
        
    case DS_PROFILE_3GPP2_PROFILE_PARAM_PDN_LABEL:
      CONVERT_IDENT_TO_MASK(custom_val_mask, PDN_LABEL);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_FAILURE_TIMER_1:
      CONVERT_IDENT_TO_MASK(custom_val_mask, FAILURE_TIMER_1);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_FAILURE_TIMER_2:
      CONVERT_IDENT_TO_MASK(custom_val_mask, FAILURE_TIMER_2);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_FAILURE_TIMER_3:
      CONVERT_IDENT_TO_MASK(custom_val_mask, FAILURE_TIMER_3);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_FAILURE_TIMER_4:
      CONVERT_IDENT_TO_MASK(custom_val_mask, FAILURE_TIMER_4);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_FAILURE_TIMER_5:
      CONVERT_IDENT_TO_MASK(custom_val_mask, FAILURE_TIMER_5);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_FAILURE_TIMER_6:
      CONVERT_IDENT_TO_MASK(custom_val_mask, FAILURE_TIMER_6);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_DISALLOW_TIMER_1:
      CONVERT_IDENT_TO_MASK(custom_val_mask, DISALLOW_TIMER_1);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_DISALLOW_TIMER_2:
      CONVERT_IDENT_TO_MASK(custom_val_mask, DISALLOW_TIMER_2);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_DISALLOW_TIMER_3:
      CONVERT_IDENT_TO_MASK(custom_val_mask, DISALLOW_TIMER_3);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_DISALLOW_TIMER_4:
      CONVERT_IDENT_TO_MASK(custom_val_mask, DISALLOW_TIMER_4);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_DISALLOW_TIMER_5:
      CONVERT_IDENT_TO_MASK(custom_val_mask, DISALLOW_TIMER_5);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_DISALLOW_TIMER_6:
      CONVERT_IDENT_TO_MASK(custom_val_mask, DISALLOW_TIMER_6);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_OP_PCO_ID:
      CONVERT_IDENT_TO_MASK(custom_val_mask, OP_PCO_ID);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_MCC:
      CONVERT_IDENT_TO_MASK(custom_val_mask, MCC);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_MNC:
      CONVERT_IDENT_TO_MASK(custom_val_mask, MNC);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_USER_PROFILE_CLASS:
      CONVERT_IDENT_TO_MASK(custom_val_mask, USER_PROFILE_CLASS);
      break;

    case DS_PROFILE_3GPP2_PROFILE_PARAM_SUBS_ID:
      CONVERT_IDENT_TO_MASK(custom_val_mask, SUBS_ID);
      break;

    default:
      break;
  }
  return custom_val_mask;
} /* dsi_profile_update_custom_val_mask */

/*===========================================================================
FUNCTION DSI_PROFILE_3GPP2_MODEM_WRITE

DESCRIPTION
  This function is used to write the profile blob back to modem efs.

PARAMETERS
  num : profile number
  ptr : pointer to profile blob

DEPENDENCIES

RETURN VALUE
  DS_PROFILE_REG_RESULT_SUCCESS
  DS_PROFILE_REG_RESULT_FAIL
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_NUM

SIDE EFFECTS

===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp2_modem_write(
  ds_profile_num_type   num,
  void                 *ptr
)
{
  ds707_data_session_profile_info_type  tmp_data_sess_profile;
  ds707_data_session_profile_info_type * profile_info_ptr = NULL;
  dsi_profile_3gpp2_type               *dsi_3gpp2_ptr     = NULL;
  ds_profile_identifier_type ident                        = 0;
  dsi_profile_3gpp2_mask_type mask                        = 0;
  uint8 index                                             = 0;
#ifdef FEATURE_EHRPD
  ds_cmd_type                          * cmd_ptr          = NULL;             
#endif

  if (ptr == NULL)
  {
    return DS_PROFILE_REG_RESULT_SUCCESS;
  }

  dsi_3gpp2_ptr = (dsi_profile_3gpp2_type *)ptr;
  if ( dsi_3gpp2_ptr->mask == 0 )
  {
    return DS_PROFILE_REG_RESULT_SUCCESS;
  }

  DATA_IS707_MSG2( MSG_LEGACY_MED,
                   "dsi_profile_3gpp2_modem_write: profile number %d with"
                   "persistency %d",
                   num, dsi_3gpp2_ptr->prf->is_persistent );

  /*
   dsi_3gpp2_ptr->mask has the bit masks set for those profile params updated
   in this transaction. Update the ds707_data_session_profile_info_type
   custom_val_mask based on dsi_3gpp2_ptr->mask
  */
  for( ident = DS_PROFILE_3GPP2_PROFILE_PARAM_MIN;
       ident <= DS_PROFILE_3GPP2_PROFILE_PARAM_MAX;
       ident++
     )
  {
    index = dsi_profile_3gpp2_get_index_from_ident( ident );
    CONVERT_IDENT_TO_MASK( mask, index );
    if( dsi_3gpp2_ptr->mask & mask )
    {
      dsi_3gpp2_ptr->prf->custom_val_mask |=
      dsi_profile_update_custom_val_mask(ident);
    }
  }

  /*Confirm write profile changes to efs is intended*/
  if( dsi_3gpp2_ptr->prf->is_persistent )
  {
    /*make a temp copy to ds707_data_session_profile_info_type*/
    ds_profile_mem_copy((void *)&tmp_data_sess_profile,
                        (void *)(dsi_3gpp2_ptr->prf));

    if (!ds707_write_profile_to_efs(num, &tmp_data_sess_profile))
    {
      return DS_PROFILE_REG_RESULT_FAIL;
    }
  }

  ds707_data_session_get_profile( num,
                                  &profile_info_ptr );
  ds_profile_mem_copy((void *)profile_info_ptr,
                      (void *)(dsi_3gpp2_ptr->prf));
                                      
#ifdef FEATURE_EHRPD
  /*---------------------------------------------------------------------
    check if there was a change to APN enabled flag, if yes run the
    mandatory APN enabled/disabled check
  ---------------------------------------------------------------------*/
  index = dsi_profile_3gpp2_get_index_from_ident( DS_PROFILE_3GPP2_PROFILE_PARAM_APN_ENABLED );
  CONVERT_IDENT_TO_MASK( mask, index );
  if( dsi_3gpp2_ptr->mask & mask )
  {
    DATA_IS707_MSG0( MSG_LEGACY_HIGH,
                     "dsi_profile_3gpp2_modem_write: APN enabled flag changed, "
                     "check mandatory APNs" );
    /*---------------------------------------------------------------------
      Send command to DS task to check if there is any change to mandatory APNs
    ---------------------------------------------------------------------*/
    cmd_ptr = ds_get_cmd_buf();
    if(NULL == cmd_ptr)
    {
      ASSERT(0);
      return DS_PROFILE_REG_RESULT_FAIL;
    }
    cmd_ptr->hdr.cmd_id = DS_707_MANDATORY_APN_CHECK_CMD;
    ds_put_cmd( cmd_ptr );
  }
#endif /* FEATURE_EHRPD */

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================
FUNCTION DSI_PROFILE_3GPP2_GET_LIST

DESCRIPTION
  This function is used to form a list of profile numbers depending on input
  value. (All profile numbers or search according to <key, value> pair)

PARAMETERS
  hndl : list handle
  lst : pointer to return list

DEPENDENCIES

RETURN VALUE
  DS_PROFILE_REG_RESULT_SUCCESS
  DS_PROFILE_REG_RESULT_FAIL
  DS_PROFILE_REG_RESULT_LIST_END : empty list is being returned

SIDE EFFECTS

===========================================================================*/

static ds_profile_status_etype dsi_profile_3gpp2_get_list(
  ds_util_list_hndl_type hndl,
  ds_profile_subs_etype subs,
  ds_profile_list_type  *lst
)
{
  int32 reg_index  = -1;
  uint16 min_num   = 0;
  uint16 max_num   = 0;
  uint32 info_size = 0;
  boolean list_is_empty = TRUE;
  ds_profile_3gpp2_list_info_type node;
  ds_profile_status_etype return_status = DS_PROFILE_REG_RESULT_FAIL;
  ds_profile_3gpp2_valid_profile_enum_type valid_mask = 
                                    DS_PROFILE_3GPP2_PROFILE_INVALID;
  ds707_data_session_profile_id profile_id = 0;
  ds707_data_session_profile_info_type * profile_info_ptr = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  if (lst == NULL)
  {
    return return_status;
  }

  if (subs != DS_PROFILE_ACTIVE_SUBSCRIPTION_1)
  {
    return DS_PROFILE_REG_RESULT_ERR_INVAL_SUBS_ID;
  }

  DATA_IS707_MSG2( MSG_LEGACY_MED, 
                   "dsi_profile_3gpp2_get_list, list_type: %d, search param %d",
                   lst->dfn, lst->ident);

  switch (lst->dfn)
  {
    /* List all profiles */
    case DS_PROFILE_LIST_ALL_PROFILES:
      return_status = DS_PROFILE_REG_RESULT_SUCCESS;
      dsi_profile_get_profile_num_range(DS_PROFILE_TECH_3GPP2,
                                        &min_num,
                                        &max_num);

      for( reg_index = min_num; reg_index < max_num; reg_index++ )
      {
        profile_id = ds707_data_session_get_profile_id_from_reg_index(reg_index);
        if ( profile_id != DATA_SESSION_PROFILE_ID_INVALID)
        {
          if(ds707_data_session_get_profile( profile_id,
                                             &profile_info_ptr ) == FALSE)
          {
            continue;
          }
          /* If profile type is invalid, do not add to list */
          if ( profile_info_ptr->profile_type != PROFILE_INVALID )
          {
            DATA_IS707_MSG1( MSG_LEGACY_HIGH,
                             "dsi_profile_3gpp2_get_list: "
                             "add profile id %d to list",
                             profile_info_ptr->data_session_profile_id );
            node.num = (ds_profile_num_type)(profile_info_ptr->data_session_profile_id);
            node.name[0] = '\0';
            info_size = sizeof(ds_profile_3gpp2_list_info_type);
            if ( ds_util_list_add( hndl, &node, info_size) != DS_SUCCESS)
            {
              DATA_IS707_MSG0( MSG_LEGACY_ERROR, "Unable to add node to list");
              return_status = DS_PROFILE_REG_RESULT_FAIL;
              break;
            }
            list_is_empty = FALSE;
          } /* profile type valid */
        } /* profile_id valid */
      } /* for loop */
    break;

    /* List profiles based on search condition */
    case DS_PROFILE_LIST_SEARCH_PROFILES:

      return_status = DS_PROFILE_REG_RESULT_SUCCESS;
      if ( !DS_PROFILE_3GPP2_IDENT_IS_VALID( lst->ident ) )
      {
        return DS_PROFILE_REG_RESULT_ERR_INVAL_IDENT;
      }
      valid_mask = get_valid_mask_from_ident( lst->ident );

      dsi_profile_get_profile_num_range(DS_PROFILE_TECH_3GPP2,
                                        &min_num,
                                        &max_num);
      for( reg_index = (int32)min_num; reg_index < (int32)max_num; reg_index++ )
      {
        profile_id = ds707_data_session_get_profile_id_from_reg_index(reg_index);
        if ( profile_id != DATA_SESSION_PROFILE_ID_INVALID)
        {
          if(ds707_data_session_get_profile( profile_id,
                                          &profile_info_ptr ) == FALSE)
          {
            continue;
          }

          /* Check validity mask and value of parameter before adding to list */
          if ( ((ds_profile_3gpp2_valid_profile_enum_type)
                  profile_info_ptr->profile_type) & valid_mask )
          {
            if ( dsi_profile_3gpp2_check_param_val( profile_info_ptr,
                                                   lst->ident,
                                                   &lst->info ) == DSI_SUCCESS )
            {
              DATA_IS707_MSG1( MSG_LEGACY_HIGH,
                               "dsi_profile_3gpp2_get_list: "
                               "add profile %d to list",
                               profile_info_ptr->data_session_profile_id );
              node.num = (ds_profile_num_type)(profile_info_ptr->data_session_profile_id);
              node.name[0] = '\0';
              info_size = sizeof(ds_profile_3gpp2_list_info_type);
              if ( ds_util_list_add( hndl, &node, info_size) != DS_SUCCESS)
              {
                DATA_IS707_MSG0( MSG_LEGACY_ERROR, "Unable to add node to list");
                return_status = DS_PROFILE_REG_RESULT_FAIL;
                break;
              }
              list_is_empty = FALSE;
            }
          } /* valid mask checking */
        } /* profile id valid */
      } /* for loop */
    break;

    default:
      return_status = DS_PROFILE_REG_RESULT_ERR_INVAL_OP;
      break;
  }

  if ( ( return_status == DS_PROFILE_REG_RESULT_SUCCESS ) &&
       ( list_is_empty == TRUE ) )
    return_status = DS_PROFILE_REG_RESULT_LIST_END;

  return return_status;
}

/*===========================================================================
FUNCTION DSI_PROFILE_3GPP2_GET_LIST_NODE

DESCRIPTION
  This function is used to get info from a particular node in the list

PARAMETERS
  hndl : iterator handle
  list_info : pointer to store information to be returned

DEPENDENCIES

RETURN VALUE
  DS_PROFILE_REG_RESULT_SUCCESS
  DS_PROFILE_REG_RESULT_FAIL

SIDE EFFECTS

===========================================================================*/

ds_profile_status_etype dsi_profile_3gpp2_get_list_node(
  ds_util_itr_hndl_type  hndl,
  ds_profile_list_info_type  *list_info
)
{
  size_t cpy_len = 0;
  ds_profile_3gpp2_list_info_type node;
  uint32 info_size = sizeof(ds_profile_3gpp2_list_info_type);
/*-------------------------------------------------------------------*/
  if (list_info == NULL)
  {
    return DS_PROFILE_REG_RESULT_FAIL;
  }

  if ( ds_util_itr_get_data(hndl, (void *)&node, &info_size) != DS_SUCCESS)
  {
    return DS_PROFILE_REG_RESULT_FAIL;
  }

  list_info->num = node.num;
  if ( list_info->name->buf != NULL )
  {
    if (DS_PROFILE_STR_LEN(node.name) != 0) 
    {
      cpy_len = strlcpy(list_info->name->buf, node.name, list_info->name->len);
      if ( cpy_len >= list_info->name->len )
      {
        /*--------------------------------------------------------------- 
          Profile name is not supported in 3GPP2 so set the length and
          return success for now. In future, when we support name, it
          might break and needs fix.
         ---------------------------------------------------------------*/
        list_info->name->len = (uint16)DS_PROFILE_STR_LEN( node.name );
        return DS_PROFILE_REG_RESULT_SUCCESS;
      }
      else
      {
        list_info->name->len = (uint16)DS_PROFILE_STR_LEN( node.name );
      }
    }
    else
    {
      ((char*)list_info->name->buf)[0] = '\0';
      list_info->name->len = (uint16)DS_PROFILE_STR_LEN( node.name );
    }
  }

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================
FUNCTION DSI_PROFILE_3GPP2_VALIDATE_PROFILE_NUM

DESCRIPTION
  This function is used to validate a profile number.

PARAMETERS
  num : profile number

DEPENDENCIES

RETURN VALUE
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_NUM
  DS_PROFILE_REG_RESULT_SUCCESS

SIDE EFFECTS

===========================================================================*/

ds_profile_status_etype dsi_profile_3gpp2_validate_profile_num(
  ds_profile_num_type   num,
  ds_profile_subs_etype subs
)
{
  ds707_data_session_profile_id reg_index = -1;

  if (subs != DS_PROFILE_ACTIVE_SUBSCRIPTION_1)
  {
    return DS_PROFILE_REG_RESULT_ERR_INVAL_SUBS_ID;
  }

  reg_index = ds707_data_session_is_valid_profile(num);
  if (reg_index == DATA_SESSION_PROFILE_ID_INVALID)
  {
    DATA_IS707_MSG1(MSG_LEGACY_ERROR,
                    "dsi_profile_3gpp2_validate_profile_num: "
                    "Invalid profile id %d",
                    num);
    return DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_NUM;
  }

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================
FUNCTION DSI_PROFILE_3GPP2_MODEM_RESET_PARAM

DESCRIPTION
  This function is used to reset a particular identifier value to default

PARAMETERS
  num : profile number
  ident : identifier for the paramter

DEPENDENCIES

RETURN VALUE

SIDE EFFECTS

===========================================================================*/
/*ARGUSED*/
ds_profile_status_etype dsi_profile_3gpp2_modem_reset_param (
  ds_profile_num_type         num,
  ds_profile_identifier_type  ident,
  ds_profile_subs_etype       subs
)
{
  /* reset param to default not supported */
  DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                   "dsi_profile_3gpp2_modem_reset_param: invalid operation" );
  return DS_PROFILE_REG_RESULT_ERR_INVAL_OP;
}

/*===========================================================================
FUNCTION DSI_PROFILE_3GPP2_MODEM_RESET_PROFILE_TO_DEFAULT

DESCRIPTION
  This function is used to reset profile to default values (No-op for 3GPP2)

PARAMETERS
  num : profile number

DEPENDENCIES

RETURN VALUE

SIDE EFFECTS

===========================================================================*/
/*ARGUSED*/
ds_profile_status_etype dsi_profile_3gpp2_modem_reset_profile_to_default(
  ds_profile_num_type    num,
  ds_profile_subs_etype  subs
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if (subs != DS_PROFILE_ACTIVE_SUBSCRIPTION_1)
  {
    return DS_PROFILE_REG_RESULT_ERR_INVAL_SUBS_ID;
  }

  /* reset profile to default */
  if( !ds707_data_session_reset_profile_to_default(num) )
  {
    return DS_PROFILE_REG_RESULT_FAIL;
  }

  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================
FUNCTION DSI_PROFILE_3GPP2_SET_DEFAULT_PROFILE

DESCRIPTION
  This function is used to set default profile number for a particular
  family (No-op for 3GPP2)

PARAMETERS
  family : type of profile
  num : profile number

DEPENDENCIES

RETURN VALUE

SIDE EFFECTS

===========================================================================*/
/*ARGUSED*/
ds_profile_status_etype dsi_profile_3gpp2_set_default_profile(
  uint32               family,
  ds_profile_num_type  num
)
{
  /* set default profile not supported */
  DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                   "dsi_profile_3gpp2_set_default_profile: invalid operation" );
  return DS_PROFILE_REG_RESULT_ERR_INVAL_OP;
}

/*===========================================================================
FUNCTION DSI_PROFILE_3GPP2_GET_DEFAULT_PROFILE

DESCRIPTION
  This function is used to get default profile number for a particular
  family (No-op for 3GPP2)

PARAMETERS
  family : type of profile
  num : pointer to store profile number

DEPENDENCIES

RETURN VALUE

SIDE EFFECTS

===========================================================================*/
/*ARGUSED*/
ds_profile_status_etype dsi_profile_3gpp2_get_default_profile(
  uint32                 family,
  ds_profile_num_type   *num
)
{
  *num = (ds_profile_num_type) ds707_data_session_get_default_profile();
  DATA_IS707_MSG1( MSG_LEGACY_MED,
                   "dsi_profile_3gpp2_get_default_profile: "
                   "default profile num %d", *num );
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================
FUNCTION DSI_PROFILE_3GPP2_SET_DEFAULT_PROFILE_PER_SUBS

DESCRIPTION
  This function is used to set default profile number for a particular
  family and subscription (No-op for 3GPP2).

PARAMETERS
  family  : type of family  (embedded or tethered)
  subs_id : subscription id (subs 1 or subs 2)
  num     : profile number

DEPENDENCIES
  None

RETURN VALUE
DS_PROFILE_REG_RESULT_SUCCESS   : On successful operation
DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE : Profile type is invalid
DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_NUM  : Invalid profile number
DS_PROFILE_REG_RESULT_ERR_INVAL_OP : Operation not supported for tech type
DS_PROFILE_REG_RESULT_FAIL      : On general errors. This return
                                  code provides blanket coverage
DS_PROFILE_REG_RESULT_ERR_INVAL_SUBS_ID : Invalid subscription id passed

SIDE EFFECTS
  None

===========================================================================*/
/*ARGUSED*/
ds_profile_status_etype dsi_profile_3gpp2_set_default_profile_per_subs(
  uint32                  family,
  ds_profile_subs_etype  subs_id,
  ds_profile_num_type     num
)
{
  /* set default profile not supported */
  DATA_IS707_MSG0( MSG_LEGACY_ERROR,
                   "dsi_profile_3gpp2_set_default_profile_per_subs: "
                   "invalid operation");
  return DS_PROFILE_REG_RESULT_ERR_INVAL_OP;
}

/*===========================================================================
FUNCTION DSI_PROFILE_3GPP2_GET_DEFAULT_PROFILE_PER_SUBS

DESCRIPTION
  This function is used to get default profile number for a particular
  family and subscription (No-op for 3GPP2).

PARAMETERS
  family  : type of family  (embedded or tethered)
  subs_id : subscription id (subs 1 or subs 2)
  num     : pointer to store profile number

DEPENDENCIES
  None

RETURN VALUE
DS_PROFILE_REG_RESULT_SUCCESS   : On successful operation
DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE : Profile type is invalid
DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_NUM  : Invalid profile number
DS_PROFILE_REG_RESULT_ERR_INVAL_OP : Operation not supported for tech type
DS_PROFILE_REG_RESULT_FAIL      : On general errors. This return
                                  code provides blanket coverage
DS_PROFILE_REG_RESULT_ERR_INVAL_SUBS_ID : Invalid subscription id passed

SIDE EFFECTS
  None

===========================================================================*/
/*ARGUSED*/
ds_profile_status_etype dsi_profile_3gpp2_get_default_profile_per_subs(
  uint32                  family,
  ds_profile_subs_etype  subs_id,
  ds_profile_num_type     *num
)
{
  ds_profile_status_etype  ret_val = DS_PROFILE_REG_RESULT_FAIL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
#ifdef FEATURE_DUAL_SIM
  if( subs_id == DS_PROFILE_ACTIVE_SUBSCRIPTION_1)
  {
    *num = DATA_SESSION_DEFAULT_PROFILE;
    ret_val = DS_PROFILE_REG_RESULT_SUCCESS;
  }
  else
  {
    ret_val = DS_PROFILE_REG_RESULT_ERR_INVAL_SUBS_ID;
  }
#else
  ret_val = DS_PROFILE_REG_RESULT_FAIL;
#endif /* FEATURE_DUAL_SIM */

  DATA_IS707_MSG2( MSG_LEGACY_HIGH,
                   "dsi_profile_3gpp2_get_default_profile_per_subs: "
                   "subs_id: %d, return status: %d",
                   subs_id, ret_val );

  return ret_val;
}

/*===========================================================================
FUNCTION DSI_PROFILE_3GPP2_CREATE

DESCRIPTION
  This function is used to create profile on modem

PARAMETERS
  num : profile number

DEPENDENCIES

RETURN VALUE

SIDE EFFECTS

===========================================================================*/
static ds_profile_status_etype dsi_profile_3gpp2_create(
  ds_profile_config_type *  config_ptr,
  ds_profile_num_type  *num
)
{
  ds707_data_session_profile_id profile_id = -1;

  /*By default all created profiles will be written to efs unless
  explicitly modified*/
  boolean is_persistent = TRUE;

  if( config_ptr != NULL )
  {
    if (config_ptr->config_mask &  DS_PROFILE_CONFIG_MASK_SUBS_ID)
    {
      if (config_ptr->subs_id != DS_PROFILE_ACTIVE_SUBSCRIPTION_1)
      {
        return DS_PROFILE_REG_RESULT_ERR_INVAL_SUBS_ID;
      }
    }

    if( config_ptr->config_mask &  DS_PROFILE_CONFIG_MASK_PERSISTENCE)
    {
      is_persistent = config_ptr->is_persistent;
    }
  }

  /*Note: We just pass the persistence param as a argument here.
    In future if we add more config params in config_ptr, consider
    passing the config_ptr to this function or use variadic
    functions*/
  profile_id = ds707_data_session_create_profile(is_persistent);

  DATA_IS707_MSG1( MSG_LEGACY_HIGH,
                   "dsi_profile_3gpp2_create: Created profile id: %d", 
                   profile_id );
  if( -1 == profile_id )
  {
    /* Ran out of Profiles */
    return DS_PROFILE_REG_3GPP2_ERR_OUT_OF_PROFILES;
  }
  else if( 0 <= profile_id && 255 >= profile_id )
  {
    /*ds707_data_session_create_profile() returns int32 and
    num is uint16. Hence the need to store in profile_id
    and then typecast
    Also QMI then typecasts it to uint8 variable. Hence the
    need to check for the range*/
    *num = (ds_profile_num_type)profile_id;
    return DS_PROFILE_REG_RESULT_SUCCESS;
  }
  else
  {
    /* Invalid profile_id */
    ASSERT(0);
    return DS_PROFILE_REG_RESULT_FAIL;
  }
}

/*===========================================================================
FUNCTION DSI_PROFILE_3GPP2_DEL

DESCRIPTION
  This function is used to delete profile on modem

PARAMETERS
  num : profile number

DEPENDENCIES

RETURN VALUE

SIDE EFFECTS

===========================================================================*/
/*ARGUSED*/
static ds_profile_status_etype dsi_profile_3gpp2_del(
  ds_profile_num_type   num,
  ds_profile_subs_etype subs 
)
{
  
  if (subs != DS_PROFILE_ACTIVE_SUBSCRIPTION_1)
  {
    return DS_PROFILE_REG_RESULT_ERR_INVAL_SUBS_ID;
  }

  if( !ds707_data_session_delete_profile(num) )
  {
    return DS_PROFILE_REG_RESULT_FAIL;
  }
  
  return DS_PROFILE_REG_RESULT_SUCCESS;
}

/*===========================================================================
FUNCTION DSI_PROFILE_3GPP2_GET_NUM_RANGE

DESCRIPTION
  This function is used to get the range of 3GPP2 profile numbers

PARAMETERS
  min_num, max_num : pointers to store range (min & max profile numbers)

DEPENDENCIES

RETURN VALUE

SIDE EFFECTS

===========================================================================*/
void dsi_profile_3gpp2_get_num_range(
  uint16 *min_num,
  uint16 *max_num
)
{
  *min_num = DATA_SESSION_PROFILE_ID_MIN;
  *max_num = (uint16)ds707_data_session_get_max_profiles();
  return;
}

/*===========================================================================
FUNCTION DS_PROFILE_3GPP2_MODEM_INIT

DESCRIPTION
  This function is called on the library init. It initializes the function
  pointers to valid functions for 3gpp2

PARAMETERS
  fntbl : pointer to table of function pointers

DEPENDENCIES

RETURN VALUE
  returns the mask for 3gpp2. (Used later as valid mask which is ORed value
  returned from all techs.
SIDE EFFECTS

===========================================================================*/

uint8 ds_profile_3gpp2_modem_init( tech_fntbl_type *fntbl )
{
  /* Init function pointers */
  fntbl->create         = dsi_profile_3gpp2_create;
  fntbl->del            = dsi_profile_3gpp2_del;
  fntbl->profile_read   = dsi_profile_3gpp2_modem_read;
  fntbl->profile_write  = dsi_profile_3gpp2_modem_write;
  fntbl->reset_param    = dsi_profile_3gpp2_modem_reset_param;
  fntbl->reset_profile_to_default = dsi_profile_3gpp2_modem_reset_profile_to_default;
  fntbl->set_default_profile      = dsi_profile_3gpp2_set_default_profile;
  fntbl->get_default_profile      = dsi_profile_3gpp2_get_default_profile;
  fntbl->set_default_profile_per_subs = dsi_profile_3gpp2_set_default_profile_per_subs;
  fntbl->get_default_profile_per_subs = dsi_profile_3gpp2_get_default_profile_per_subs;
  fntbl->get_list = dsi_profile_3gpp2_get_list;
  fntbl->get_list_node = dsi_profile_3gpp2_get_list_node;
  fntbl->validate_profile_num = dsi_profile_3gpp2_validate_profile_num;
  fntbl->get_num_range = dsi_profile_3gpp2_get_num_range;

  return (0x01 << DS_PROFILE_TECH_3GPP2);
}

/*===========================================================================
FUNCTION DS_PROFILE_MEM_COPY

DESCRIPTION
  This function is used to copy a data session profile blob member by member

PARAMETERS
  to : pointer to profile blob where the content is to be copied
  from : pointer to profile blob that contains the data to be copied

DEPENDENCIES

RETURN VALUE

SIDE EFFECTS

===========================================================================*/
void ds_profile_mem_copy(
  void *to,
  void *from
)
{
  ds707_data_session_profile_info_type *prf_to =
                     (ds707_data_session_profile_info_type *)to;
  ds707_data_session_profile_info_type *prf_from =
                     (ds707_data_session_profile_info_type *)from;
/****************************************************************************/

  memset( (void *)prf_to,
          0,
          sizeof(ds_profile_3gpp2_profile_info_type) );

  prf_to->data_session_profile_id       = prf_from->data_session_profile_id;
  prf_to->negotiate_dns_server          = prf_from->negotiate_dns_server;
  prf_to->ppp_session_close_timer_DO    = prf_from->ppp_session_close_timer_DO;
  prf_to->ppp_session_close_timer_1X    = prf_from->ppp_session_close_timer_1X;
  prf_to->allow_linger                  = prf_from->allow_linger;
  prf_to->linger_timeout_val            = prf_from->linger_timeout_val;
  prf_to->lcp_ack_timeout               = prf_from->lcp_ack_timeout;
  prf_to->ipcp_ack_timeout              = prf_from->ipcp_ack_timeout;
  prf_to->auth_timeout                  = prf_from->auth_timeout;
  prf_to->lcp_creq_retry_count          = prf_from->lcp_creq_retry_count;
  prf_to->ipcp_creq_retry_count         = prf_from->ipcp_creq_retry_count;
  prf_to->auth_retry_count              = prf_from->auth_retry_count;
  prf_to->auth_protocol                 = prf_from->auth_protocol;

  memscpy( prf_to->user_id,
  	   PPP_MAX_USER_ID_LEN,
           prf_from->user_id,
           prf_from->user_id_len);
  prf_to->user_id_len                   = prf_from->user_id_len;

  memscpy( prf_to->auth_password,
  	   PPP_MAX_PASSWD_LEN,
           prf_from->auth_password,
           prf_from->auth_password_len);
  prf_to->auth_password_len             = prf_from->auth_password_len;

  prf_to->data_rate                     = prf_from->data_rate;
  prf_to->data_mode                     = prf_from->data_mode;
  prf_to->app_type                      = prf_from->app_type;
  prf_to->app_priority                  = prf_from->app_priority;

  memscpy( prf_to->apn_string,
  	   DS_VSNCP_3GPP2_APN_MAX_VAL_LEN,
           prf_from->apn_string,
           prf_from->apn_string_len);
  prf_to->apn_string_len                = prf_from->apn_string_len;

  memscpy( prf_to->pdn_label,
           DS_VSNCP_3GPP2_APN_MAX_VAL_LEN,
           prf_from->pdn_label,
           prf_from->pdn_label_len);
  prf_to->pdn_label_len                = prf_from->pdn_label_len;
  prf_to->apn_enabled                   = prf_from->apn_enabled;
  prf_to->apn_class                     = prf_from->apn_class;
  prf_to->pdn_type                      = prf_from->pdn_type;
  prf_to->is_pcscf_addr_needed          = prf_from->is_pcscf_addr_needed;

  prf_to->op_pco_id = prf_from->op_pco_id;
  prf_to->mcc = prf_from->mcc;
  prf_to->mnc.mnc = prf_from->mnc.mnc;
  prf_to->mnc.mnc_includes_pcs_digit = prf_from->mnc.mnc_includes_pcs_digit;

  memscpy( prf_to->v4_dns_addr,
  	   sizeof(ds_profile_3gpp2_in_addr_type)*DS_PROFILE_3GPP2_MAX_NUM_DNS_ADDR,
           prf_from->v4_dns_addr,
           sizeof(ds_profile_3gpp2_in_addr_type)*DS_PROFILE_3GPP2_MAX_NUM_DNS_ADDR);
  memscpy( prf_to->v6_dns_addr,
           sizeof(ds_profile_3gpp2_in6_addr_type)*DS_PROFILE_3GPP2_MAX_NUM_DNS_ADDR,
           prf_from->v6_dns_addr,
           sizeof(ds_profile_3gpp2_in6_addr_type)*DS_PROFILE_3GPP2_MAX_NUM_DNS_ADDR);

  prf_to->rat_type                      = prf_from->rat_type;
  prf_to->pdn_inactivity_timeout        = prf_from->pdn_inactivity_timeout;

  memscpy( prf_to->failure_timer,
           sizeof(uint32)*PDN_THROTTLE_MAX_TIMERS,
           prf_from->failure_timer,
           sizeof(uint32)*PDN_THROTTLE_MAX_TIMERS);
  memscpy( prf_to->disallow_timer,
	   sizeof(uint32)*PDN_THROTTLE_MAX_TIMERS,
           prf_from->disallow_timer,
           sizeof(uint32)*PDN_THROTTLE_MAX_TIMERS);

  prf_to->profile_type                  = prf_from->profile_type;
  prf_to->custom_val_mask               = prf_from->custom_val_mask;
  prf_to->is_persistent                 = prf_from->is_persistent;

  prf_to->pdn_level_auth_protocol       = prf_from->pdn_level_auth_protocol;

  memscpy( prf_to->pdn_level_user_id,
  	   PPP_MAX_USER_ID_LEN,
           prf_from->pdn_level_user_id,
           prf_from->pdn_level_user_id_len);
  prf_to->pdn_level_user_id_len         = prf_from->pdn_level_user_id_len;

  memscpy( prf_to->pdn_level_auth_password,
           PPP_MAX_PASSWD_LEN,
           prf_from->pdn_level_auth_password,
           prf_from->pdn_level_auth_password_len);
  prf_to->pdn_level_auth_password_len   = prf_from->pdn_level_auth_password_len;

  prf_to->user_profile_class            = prf_from->user_profile_class;

  prf_to->subs_id                       = prf_from->subs_id;

}
/*lint -restore Restore lint error 655*/
/*lint -restore Restore lint error 641*/
