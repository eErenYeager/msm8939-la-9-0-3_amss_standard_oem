#ifndef DSBCDB_INT_H
#define DSBCDB_INT_H
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                           D S B C D B _ I N T . H

GENERAL DESCRIPTION
  This file contains the types and functions used internally by the BCDBAPP
  module.

EXTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS
  dsbcdb_task() must be called first from TMC to start the BCDBAPP task.

 Copyright (c) 2004, 2005 by Qualcomm Technologies Incorporated.  All Rights Reserved.
 Copyright (c) 2006 - 2008 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================
                        EDIT HISTORY FOR MODULE

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/3gpp2/bcdb/src/dsbcdb_int.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

-----------------------------------------------------------------------------
when        who    what, where, why
--------    ---    -------------------------------------------------------
09/03/08    ms     BCMCS 2.0 Phase3 Flow Priority related
                   enhancements
11/26/07    ms     Feature enhancement for BCMCS 2.0
04/04/06    kvd    Added a new field, age to dsbcdbi_db_entry_type.
10/03/05    as/kvd Added support for bcmcs db update ioctl
08/26/05    mpa    Use sys.h ip address type for CM cmds.
12/16/04    rsl    Added support for flow_format=1 based on changes in standard.
03/11/04    vr     Initial revision.
===========================================================================*/

/*===========================================================================
                      INCLUDE FILES
===========================================================================*/
#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"

#include "msg.h"
#include "err.h"
#include "nv.h"
#include "sys.h"
#include "rex.h"
#include "task.h"
#include "queue.h"
#include "fs_public.h"
#include "amssassert.h"
#include "dog.h"
#include "dsbcdb_api.h"


/*===========================================================================
                        TYPEDEFS AND VARIABLES
===========================================================================*/

/*---------------------------------------------------------------------------
  BCDBAPP Services Task Signal Definitions

  NOTE: Make sure not to reuse the common signals defined in task.h.
  All other signals can be used. The following signals are currently 
  reserved in task.h. There may be more in the future.

  TASK_START_SIG        0x8000
  TASK_STOP_SIG         0x4000
  TASK_OFFLINE_SIG      0x2000
  FS_OP_COMPLETE_SIG    0x40000000
  VS_OP_COMPLETE_SIG    0x20000000
  SEC_OP_COMPLETE_SIG   0x10000000
  GRP_ACK_SIG           0x08000000
  CAMERA_SIG            0x04000000
---------------------------------------------------------------------------*/
#define DSBCDB_TASK_START_SIG     TASK_START_SIG     /* 0x8000             */
#define DSBCDB_TASK_STOP_SIG      TASK_STOP_SIG      /* 0x4000             */
#define DSBCDB_TASK_OFFLINE_SIG   TASK_OFFLINE_SIG   /* 0x2000             */
#define DSBCDB_CMD_Q_SIG          0x1    /* Command Queue signal           */
#define DSBCDB_DOG_HB_REPORT_SIG  0x2    /* Dog HB Report signal           */

/*---------------------------------------------------------------------------
  The state of the entire database. This is for phase 1 and can change in 
  the future.
---------------------------------------------------------------------------*/
typedef enum {
  DSBCDBI_STATE_VALID          = 0,
  DSBCDBI_STATE_CONFIGURING    = 1,
  DSBCDBI_STATE_INVALID        = 2,

  DSBCDBI_STATE_MAX
} dsbcdbi_state_enum_type;

/*---------------------------------------------------------------------------
  Internal structure of the database maintained by BCDBAPP.

  For phase 1 this simply has the structure exported out to other modules.
  This struct would change in later phases, if we optimize how we traverse
  the database and add support for each database entry to expire
  independently.
---------------------------------------------------------------------------*/
typedef struct
{
  dsbcmcs_zone_type zone;
  ip_addr_type multicast_ip;
  uint32 program_id;
  uint8  program_id_len;
  uint8  flow_discrim_len;
  uint8  flow_discrim;
  uint16 port;

  /*-------------------------------------------------------------------------
    Whether the payload is framed using segment based or HDLC framing.
  -------------------------------------------------------------------------*/
  dsbcmcs_framing_enum_type framing;

  /*-------------------------------------------------------------------------
    The protocol field tells the deframer where to deliver the packet.

    If protocol field is BCMCS_PROTOCOL_PPP that means the
    packet is PPP encapsulated and the first one or two bytes tell 
    whether the rest of the packet is IPv4/v6 or some other protocol. The
    PPP encapsulation algorithm can unambiguously determine 
    whether one byte or two bytes have been used in the encapsulation.

    If protocol field is BCMCS_PROTOCOL_IPv4/v6 that means the 
    packet obtained after deframing is an IPv4/v6 packet and can be
    delivered directly to the IPv4/v6 stack.
  -------------------------------------------------------------------------*/
  dsbcmcs_protocol_enum_type protocol;

  /*-------------------------------------------------------------------------
    Whether the CRC is 0 or 2 bytes long
  -------------------------------------------------------------------------*/
  uint8 crc_len;

  uint8 flow_format;
  uint32 flow_id;

#ifdef FEATURE_HDR_BCMCS_2p0
  #error code not present
#endif /* FEATURE_HDR_BCMCS_2p0 */

  /* Additional fields, eg: BAK_key, will be added in the future*/

  boolean entry_valid;
  uint8   age;   /* age-used to decide which entry to replace on db full*/

} dsbcdbi_db_entry_type;

/*---------------------------------------------------------------------------
  Commands that may be sent to BCDBAPP task
---------------------------------------------------------------------------*/
typedef enum
{
  DSBCDBI_GET_FLOW_INFO_CMD  = 1,
  DSBCDBI_IOCTL_UPDATE_DB_CMD  = 2,

  DSBCDBI_MAX_CMDS
} dsbcdbi_cmd_enum_type;

/*---------------------------------------------------------------------------
  BCDBAPP Command Header type
---------------------------------------------------------------------------*/
typedef struct
{
  q_link_type               link;     /* Queue link type                   */
  dsbcdbi_cmd_enum_type     cmd_id;   /* Identifies the command            */
  dsbcdb_cmd_handle_type    handle;   /* The handle for the command that is
                                          returned to the calling module   */
} dsbcdbi_cmd_hdr_type;

/*---------------------------------------------------------------------------
  Input structure for GET_FLOW_INFO cmd 
---------------------------------------------------------------------------*/
typedef struct
{
  dsbcmcs_zone_type          zone;
  sys_ip_address_s_type      ipaddr;
  uint16                     port;
  dsbcdb_flow_info_cb_type   flow_info_cb;
} dsbcdbi_flow_info_cmd_input_type;

/*---------------------------------------------------------------------------
  BCDBAPP Command type.

  The cmd has a header which has a handle for the command, input parameters 
  and a callback function to be called when the command has been processed.
  It is possible that the callback function is NULL for some cmds 
  (eg: bcast reset cmd )
---------------------------------------------------------------------------*/
typedef struct
{
  dsbcdbi_cmd_hdr_type hdr;

  union
  {
    dsbcdbi_flow_info_cmd_input_type      flow_info_input;
    ps_iface_ioctl_bcmcs_db_update_type   db_update_rec;

    /* Add input types for other commands in the future */
  } cmd;

} dsbcdbi_cmd_type;


/*===========================================================================
                           FUNCTION PROTOTYPES 
===========================================================================*/
/*===========================================================================
FUNCTION      DSBCDBI_INIT

DESCRIPTION   This is the BCDBAPP task initialization function that should
              be called immediately after the task is started. For phase 1,
              it reads the database out of EFS.

DEPENDENCIES  This must only be called when the task starts.

RETURN VALUE  None.

SIDE EFFECTS  Populates the database in memory from EFS.
===========================================================================*/
void dsbcdbi_init(void);

/*===========================================================================
FUNCTION      DSBCDBI_GET_FLOW_LIST

DESCRIPTION   Internal function used by BCDBAPP to get the flow link and 
              security info.

              This is called by BCDBAPP task when handling the GET_FLOW_INFO
              command. The command handler will get the information from 
              this function and then call the callback function provided for
              that command. 

DEPENDENCIES  memory for flow_list and err_num must be allocated by the caller 
              before calling this function.

RETURN VALUE  Returns 0 and err_num is set to E_SUCCESS if successfully able
              to get the flow list, otherwise returns -1 and err_num will be
              set to the appropriate value.

              err_num is set to E_SUCCESS and flow_list is populated with the 
              list of flows matching the {zone, IP, port} specified in the
              command input if the flows are found.

              If port # in the flow_spec is zero and more than 
              BCDB_MAX_FLOWS_PER_IP are found for to the {zone, IP}, the
              first BCDB_MAX_FLOWS_PER_IP are returned in flow_list and
              err_num is set to E_OVERFLOW.

              If no flow is found, flow_list will be an entry list and err_num
              will be set to one of the following:
              E_DB_INVALID        The entire database is in an INVALID or
                                  CONFIGURING state.
              E_ZONE_NOT_IN_DB    The specified zone is not found anywhere
                                  in the database. (In the future, this may
                                  trigger information acquisition from the
                                  BCMCS controller.)
              E_FLOW_NOT_IN_ZONE  The specified zone is found in the
                                  database, but no entry for the requested 
                                  {IP, port} is found in that zone.
              E_FAILURE           An unknown error occurred. This should 
                                  ideally never happen and will only be 
                                  returned if ASSERT is turned off.

SIDE EFFECTS  None.
===========================================================================*/
int dsbcdbi_get_flow_list
(
  dsbcdbi_flow_info_cmd_input_type flow_key, 				  
  dsbcdb_flow_list_type *flow_list,
  sint15 *err_num
);

/*===========================================================================
FUNCTION      DSBCDBI_GET_CMD_BUF

DESCRIPTION   This allocates a cmd buffer for posting a command to BCBDAPP
              task.

DEPENDENCIES  BCDBAPP must be started prior to calling this function.

RETURN VALUE  Returns a cmd item from bcdbi_cmd_free_q. If no item is 
              available, it returns NULL. It is the callers responsibility
              to check for NULL and take appropriate action if needed.

SIDE EFFECTS  None.
===========================================================================*/
dsbcdbi_cmd_type* dsbcdbi_get_cmd_buf( void );

/*===========================================================================
FUNCTION      DSBCDBI_PUT_CMD

DESCRIPTION   This posts a command to BCDBAPP task and sets BCDB_CMD_Q_SIG.

DEPENDENCIES  The caller of this function should have previously allocated
              a command buffer by calling dsbcdbi_get_cmd_buf

RETURN VALUE  None.

SIDE EFFECTS  None.
===========================================================================*/
void dsbcdbi_put_cmd(dsbcdbi_cmd_type *cmd_ptr);

/*===========================================================================
FUNCTION      DSBCDBI_UPDATE_DB

DESCRIPTION   This adds update the bcmcs database with the passed in values.

DEPENDENCIES  None

RETURN VALUE  None.

SIDE EFFECTS  None.
===========================================================================*/
void dsbcdbi_update_db(ps_iface_ioctl_bcmcs_db_update_type *update_rec_ptr);


/*===========================================================================
FUNCTION      DSBCDB_ALLOW_DB_UPDATE

DESCRIPTION   This function checks to see if the adds update the bcmcs 
              database with the passed in values.

DEPENDENCIES  None

RETURN VALUE  -1 if invalid and 0 otherwise	- err_num	set	if invalid.

SIDE EFFECTS  None.
===========================================================================*/
int dsbcdb_allow_db_update(
    ps_iface_ioctl_bcmcs_db_update_type *update_rec_ptr,
    sint15 *err_num
);

#ifdef FEATURE_HDR_BCMCS_2p0 
#error code not present
#endif  /* FEATURE_HDR_BCMCS_2p0 */

#endif /* DSBCDB_INT_H */
