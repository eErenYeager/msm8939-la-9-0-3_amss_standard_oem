/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                           D S B C D B _ T A S K . C

GENERAL DESCRIPTION
  This file contains the entry point and main processing loop of the BCDBAPP 
  task. Functions to manipulate the BCDBAPP task command queue are 
  also in this file. 

EXTERNALIZED FUNCTIONS
  DSBCDB_TASK
    This is the entry point into the BCDBAPP task

INITIALIZATION AND SEQUENCING REQUIREMENTS
  dsbcdb_task() must be called first from TMC to start the BCDBAPP task.

Copyright (c) 2004-2013 Qualcomm Technologies Incorporated. 
All Rights Reserved.
Qualcomm Confidential and Proprietary
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================
                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/3gpp2/bcdb/src/dsbcdb_task.c#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

-----------------------------------------------------------------------------
when        who    what, where, why
--------    ---    -------------------------------------------------------
03/02/13    pkp    Dog heart beat implementation.
05/18/12    msh    Move RCINIT handshake out of rcinit phase 2 feature
03/21/12    msh    TCB changes due to Core changes
10/01/11    bvd    Adding featurization for rcinit framework
03/30/11    ms     Q6 free floating global variables cleanup.
01/31/11    op     Migrated to MSG 2.0 Macros
10/18/10    ms     Added debug messages for all Assert(0)s.
09/27/10    ss     DYNAMIC WDOG support for DATA tasks.
03/16/10    sy     Changed task_* api to tmc_task* api.
05/10/08    ms     BCMCS 2.0 Enhancements
01/13/06    as     Moved command handlers to dsbcdb_hdlr.c
05/31/06    mpa    Increased cmd queue size to 70
10/03/05    as/kvd Added cmd handlers for the bcmcs db update ioctl
08/23/04    vr     Included dsbcdb_task.h
03/11/04    vr     Initial revision.
===========================================================================*/

/*===========================================================================
                      INCLUDE FILES
===========================================================================*/
#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"
#include "data_msg.h"

#ifdef FEATURE_BCMCS

#include "dsbcdb_int.h"
#include "dsbcdb_hdlr.h"
#include "dsbcdb_task.h"

#include "dog_hb_rex.h"
#include "rex.h"

#include "rcinit_rex.h"

/*===========================================================================
                        TYPEDEFS AND VARIABLES
===========================================================================*/

/*---------------------------------------------------------------------------
  The maximum number of commands that can be in BCBDAPP command queue. This 
  should be a value less than or equal to 0xFE since 0xFF is reserved 
  for invalid handle (DSBCDB_INVALID_CMD_HANDLE)
---------------------------------------------------------------------------*/
#define BCDBI_CMD_BUF_CNT 100
static uint8 bcdbi_free_cmd_buf_cnt = 100;
#define BCDBI_MAX_CMD_HANDLE BCDBI_CMD_BUF_CNT 

/*---------------------------------------------------------------------------
  BCBDAPP command buffer that is used to post commands to BCDBAPP task.
---------------------------------------------------------------------------*/
dsbcdbi_cmd_type bcdbi_cmd_buf[BCDBI_CMD_BUF_CNT];

static q_type bcdbi_cmd_q;        /* BCDB_DB Task command queue             */
static q_type bcdbi_cmd_free_q;   /* Queue for storing free command buffers */

/*---------------------------------------------------------------------------
  TCB pointer for BCDB task
---------------------------------------------------------------------------*/
static rex_tcb_type* bcdb_tcb_ptr; 

/*===========================================================================
                  INTERNAL FUNCTION DEFINITIONS
===========================================================================*/
/*===========================================================================
FUNCTION      DSBCDBI_POWERUP_INIT

DESCRIPTION   This performs the powerup initialization needed in TMC task 
              context for starting the BCDBAPP task. It initializes the 
              required queues and timers.

DEPENDENCIES  Should only be called on powerup in TMC task context.

RETURN VALUE  None.

SIDE EFFECTS  None.
===========================================================================*/
void  dsbcdbi_powerup_init( void )
{
  uint8    i;                                               /* Loop index */

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------------
    Initialize the command queue and the free command queue, and link the
    command items onto the free command queue.
  -------------------------------------------------------------------------*/
  (void)q_init( &bcdbi_cmd_q );
  (void)q_init( &bcdbi_cmd_free_q );

  for( i = 0; i < BCDBI_CMD_BUF_CNT; i++ )
  {
    (void)q_link( &bcdbi_cmd_buf[i], &bcdbi_cmd_buf[i].hdr.link );
    q_put( &bcdbi_cmd_free_q, &bcdbi_cmd_buf[i].hdr.link );

  /*-------------------------------------------------------------------------
    It is possible that BCDBAPP may reply to commands out of sequence. 
    Assign a handle to each command that will be provided to the calling 
    task when the command is issued. The same handle will be provided in 
    the response to the command. The calling task needs to match the handle 
    it got for the query with the handle in the response to identify which 
    query the response is for. It is NOT guaranteed that responses will be 
    given in the same order that the queries were received.
  -------------------------------------------------------------------------*/
    bcdbi_cmd_buf[i].hdr.handle = i;
  }
} /* dsbcdbi_powerup_init() */


/*===========================================================================
FUNCTION      DSBCDBI_GET_CMD_BUF

DESCRIPTION   This allocates a cmd buffer for posting a command to BCBDAPP
              task.

DEPENDENCIES  BCDBAPP must be started prior to calling this function.

RETURN VALUE  Returns a cmd item from bcdbi_cmd_free_q. If no item is 
              available, it returns NULL. It is the callers responsibility
              to check for NULL and take appropriate action if needed.

SIDE EFFECTS  None.
===========================================================================*/
dsbcdbi_cmd_type * dsbcdbi_get_cmd_buf( void )
{
  dsbcdbi_cmd_type    *cmd_ptr;                     /* Pointer to command */

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------------
    Get a command buffer from the free command queue.
    Currently it will already have the handle. If this changes in the future,
    assign a handle to the cmd.
  -------------------------------------------------------------------------*/
  if( (cmd_ptr = q_get( &bcdbi_cmd_free_q )) == NULL )
  {
    /*-----------------------------------------------------------------------
      No free command buffers available, log an error.
    -----------------------------------------------------------------------*/
    DATA_BCMCS_MSG0(MSG_LEGACY_ERROR, "No items on BCDB_DB Task free cmd q");
  }
  bcdbi_free_cmd_buf_cnt --;
  DATA_BCMCS_MSG1(MSG_LEGACY_MED," Allocated bcdbi_cmd_buf. Remaning free count: %d",
                      bcdbi_free_cmd_buf_cnt);
  /*-------------------------------------------------------------------------
    Note that the cmd_ptr may be NULL if there were no free command buffers
    available. The calling task must either handle the NULL return value
    or ERR_FATAL.
  -------------------------------------------------------------------------*/
  return( cmd_ptr );
} /* dsbcdbi_get_cmd_buf */


/*===========================================================================
FUNCTION      DSBCDBI_PUT_CMD

DESCRIPTION   This posts a command to BCDBAPP task and sets BCDB_CMD_Q_SIG.

DEPENDENCIES  The caller of this function should have previously allocated
              a command buffer by calling dsbcdbi_get_cmd_buf

RETURN VALUE  Returns a cmd item from bcdbi_cmd_free_q. If no item is 
              available, it returns NULL. It is the callers responsibility
              to check for NULL and take appropriate action if needed.

SIDE EFFECTS  None.
===========================================================================*/
void dsbcdbi_put_cmd(dsbcdbi_cmd_type *cmd_ptr)
{
  if(cmd_ptr != NULL)
  {
    q_put(&bcdbi_cmd_q, &cmd_ptr->hdr.link);
    (void) rex_set_sigs( bcdb_tcb_ptr, DSBCDB_CMD_Q_SIG);
  }
  else
  {
    DATA_BCMCS_MSG0(MSG_LEGACY_ERROR, "NULL command passed to dsbcdbi_put_cmd");
  }
} /* dsbcdbi_put_cmd */


/*===========================================================================
FUNCTION      DSBCDBI_PROCESS_CMDS

DESCRIPTION   This is the main entry point into command processing for 
              BCBDAPP task. It calls the appropriate command handler based
              on the command.

DEPENDENCIES  This must be called in BCDBAPP task context.

RETURN VALUE  None.

SIDE EFFECTS  None.
===========================================================================*/
LOCAL void dsbcdbi_process_cmds( void )
{
  dsbcdbi_cmd_type *cmd_ptr;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------------
    Get commands from the command queue until the queue is empty. For each
    command received, dispatch the command to the appropriate sub-task.
  -------------------------------------------------------------------------*/
  while( (cmd_ptr = (dsbcdbi_cmd_type *)q_get( &bcdbi_cmd_q )) != NULL )
  {

    switch( cmd_ptr->hdr.cmd_id )
    {
      case DSBCDBI_GET_FLOW_INFO_CMD:
        DATA_BCMCS_MSG0(MSG_LEGACY_MED, "Processing BCDB_GET_FLOW_INFO_CMD");
        dsbcdb_get_flow_info_cmd_hdlr( cmd_ptr );
        break;
      case DSBCDBI_IOCTL_UPDATE_DB_CMD:
        DATA_BCMCS_MSG0(MSG_LEGACY_MED, "Processing DSBCDBI_IOCTL_UPDATE_DB_CMD");
        dsbcdb_update_db_cmd_hdlr( cmd_ptr );
        break;
      default:
        DATA_BCMCS_MSG2(MSG_LEGACY_HIGH,"Unknown command ID %d handle %d",
                 cmd_ptr->hdr.cmd_id,cmd_ptr->hdr.handle);
        /*lint -save -e506, -e774 */
        ASSERT(0);
        /*lint -restore */
        break;
    }

    /*-----------------------------------------------------------------------
      Free the command.

      In the future, if we respond to commands out of sequence, we may not 
      free them here but put them in a pending table and free them when we 
      respond to them.
    ----------------------------------------------------------------------*/
    bcdbi_free_cmd_buf_cnt++;
    DATA_BCMCS_MSG1(MSG_LEGACY_MED,"Processed bcdbi_cmd: Free cmd buff count :%d",bcdbi_free_cmd_buf_cnt);
    q_put(&bcdbi_cmd_free_q, &(cmd_ptr->hdr.link));

    /*-----------------------------------------------------------------------
      The assumption is that there won't be too many commands in the command
      queue at any given point in time and this loop won't take too long to
      execute. If this assumption turns out to be invalid, pet the watchdog
      or have other recovery mechanism here.
    -----------------------------------------------------------------------*/
  }
} /* dsbcdbi_process_cmds */


/*===========================================================================
                  EXTERNAL FUNCTION DEFINITIONS
===========================================================================*/
/*===========================================================================
FUNCTION      DSBCDB_TASK

DESCRIPTION  
  This is the entry point for the BCDBAPP Services Task. This function 
  contains the main processing loop that waits for events (signals or 
  commands) and dispatches each event to the appropriate entity for further 
  processing.

DEPENDENCIES  None.

RETURN VALUE  This function does not return.

SIDE EFFECTS  None.
===========================================================================*/
/*lint -esym(715,ignored)*/
void dsbcdb_task
(
  dword ignored
)
{
  /* This is the main loop for the information sharing task */ 

  rex_sigs_type   requested_sigs;   /* Signal mask to suspend on           */
  rex_sigs_type   set_sigs;         /* Signals set upon return from wait   */
  dog_report_type bcdb_dog_rpt_id = 0;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/


#ifdef FEATURE_MODEM_RCINIT_PHASE2
  RCEVT_SIGEX_SIGREX term_sig; /* Termination Signal to wait on*/
  term_sig.signal = bcdb_tcb_ptr = rex_self();
  term_sig.mask = DSBCDB_TASK_STOP_SIG; 

  /* Register for the STOP Signal */
  rcinit_register_term_group(RCINIT_GROUP_5, 
                             RCEVT_SIGEX_TYPE_SIGREX, 
                             &term_sig);

  rcinit_handshake_startup();
#else
  /*-------------------------------------------------------------------------
    Perform any initializations that are needed here that need to be done 
    in TMC task context and then start the task.

    This should include initializing any timers if needed (future phases).
  -------------------------------------------------------------------------*/
  bcdb_tcb_ptr = &bcdb_tcb;
  dsbcdbi_powerup_init();
#endif /*FEATURE_MODEM_RCINIT_PHASE2*/

  /*-------------------------------------------------------------------------
    Dog heart beat registration.
  -------------------------------------------------------------------------*/
  bcdb_dog_rpt_id = dog_hb_register_rex(
                     (rex_sigs_type)DSBCDB_DOG_HB_REPORT_SIG);
  
#ifndef FEATURE_MODEM_RCINIT_PHASE2
  /*-------------------------------------------------------------------------
    Wait for the task start signal from TMC.
   -------------------------------------------------------------------------*/
   tmc_task_start();  
#endif

  /*-------------------------------------------------------------------------
    For phase 1 there are no subtasks for BCDBAPP task. If there are any
    subtasks in the future, perform any subtask initializations needed in
    BCDBAPP task context here.
  -------------------------------------------------------------------------*/

  /*-------------------------------------------------------------------------
    Read the database from EFS and perform any other initializations that
    may be needed. 
  -------------------------------------------------------------------------*/
  dsbcdbi_init();

  /*-------------------------------------------------------------------------
    Signals that BCBDAPP task will wait on. Add other signals, including any
    subtask signals in the future.
  -------------------------------------------------------------------------*/
  requested_sigs = DSBCDB_CMD_Q_SIG        |
                   DSBCDB_TASK_STOP_SIG    |
                   DSBCDB_TASK_OFFLINE_SIG |
                   DSBCDB_DOG_HB_REPORT_SIG;

  /*-------------------------------------------------------------------------
    Enter main processing loop. This loop never exits.
  -------------------------------------------------------------------------*/
  for(;;)
  {
    set_sigs = rex_wait( requested_sigs );
    /*lint -save -e534*/
    rex_clr_sigs(bcdb_tcb_ptr, set_sigs);
    /*lint -restore */

    /*-----------------------------------------------------------------------
      Ack - dog heart beat, if set.
    -----------------------------------------------------------------------*/
    if( (set_sigs & DSBCDB_DOG_HB_REPORT_SIG) != 0 )
    {
      dog_hb_report( bcdb_dog_rpt_id ); /* Play ping <->pong */
      /*---------------------------------------------------------------------
        Clear the processed signal from set_sigs
      ---------------------------------------------------------------------*/
      set_sigs &= ~DSBCDB_DOG_HB_REPORT_SIG;
    }

    /*----------------------------------------------------------------------
      If any of the task signals set by REX are received, ACK the main REX
      task conroller and clear the signal from set_sigs.
    -----------------------------------------------------------------------*/
    if( (set_sigs & DSBCDB_TASK_STOP_SIG) != 0 )
    {
#ifdef FEATURE_MODEM_RCINIT_PHASE2
      dog_hb_dergister(bcdb_dog_rpt_id);
      rcinit_unregister_term_group(RCINIT_GROUP_5, RCEVT_SIGEX_TYPE_SIGREX, &term_sig);
      rcinit_handshake_term();
#else
      tmc_task_stop();
#endif 
      set_sigs &= ~DSBCDB_TASK_STOP_SIG;
    }

    if( (set_sigs & DSBCDB_TASK_OFFLINE_SIG) != 0 )
    {
      set_sigs &= ~DSBCDB_TASK_OFFLINE_SIG;
    }

    /*----------------------------------------------------------------------
      Process the received commands.
    -----------------------------------------------------------------------*/
    if( (set_sigs & DSBCDB_CMD_Q_SIG) != 0 )
    {
      dsbcdbi_process_cmds();

      /*---------------------------------------------------------------------
        Clear the processed signal from set_sigs
      ---------------------------------------------------------------------*/
      set_sigs &= ~DSBCDB_CMD_Q_SIG;
    }

    /*----------------------------------------------------------------------
      Since we do a synchronous EFS read, there is no need to do anything
      if FS_OP_COMPLETE signal is set.
    -----------------------------------------------------------------------*/
    if( (set_sigs & FS_OP_COMPLETE_SIG) != 0 )
    {
      DATA_BCMCS_MSG0(MSG_LEGACY_MED, "Ignoring FS_OP_COMPLETE_SIG");

      /*---------------------------------------------------------------------
        Clear the processed signal from set_sigs
      ---------------------------------------------------------------------*/
      set_sigs &= ~FS_OP_COMPLETE_SIG;
    }

    /*----------------------------------------------------------------------
      Sanity check - there should be no unprocessed signals at this point.
    -----------------------------------------------------------------------*/
    if(set_sigs != 0)
    {
      DATA_BCMCS_MSG1(MSG_LEGACY_ERROR, "Unprocessed sigs = 0x%x",set_sigs);
      /*lint -save -e506, -e774 */
      ASSERT(0);
      /*lint -restore */
    }
  }
} /* dsbcdb_task */


#endif /* FEATURE_BCMCS */
