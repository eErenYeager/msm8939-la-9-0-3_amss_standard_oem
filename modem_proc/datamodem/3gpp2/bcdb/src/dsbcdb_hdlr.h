
#ifndef DSBCDB_HDLR_H
#define DSBCDB_HDLR_H
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                           D S B C D B _ H D L R. H

GENERAL DESCRIPTION
  This file contains the task definition for the BCDB task.

EXTERNALIZED FUNCTIONS
DSBCDBI_PROCESS_CMDS
  This is the main entry point into command processing for 
  BCBDAPP task. It calls the appropriate command handler based
  on the command.
      
INITIALIZATION AND SEQUENCING REQUIREMENTS
  dsbcdb_task() must be called first from TMC to start the BCDBAPP task.

 Copyright (c) 2006 by Qualcomm Technologies Incorporated.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================
                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/3gpp2/bcdb/src/dsbcdb_hdlr.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

-----------------------------------------------------------------------------
when        who    what, where, why
--------    ---    -------------------------------------------------------
08/23/04    as     New File Command Handlers moved from dsbcdb_task.h#1
===========================================================================*/

/*===========================================================================
                      INCLUDE FILES
===========================================================================*/
#include "comdef.h"

/*===========================================================================
                        TYPEDEFS AND VARIABLES
===========================================================================*/


/*===========================================================================
                   EXTERNAL FUNCTION PROTOTYPES 
===========================================================================*/

/*===========================================================================
FUNCTION      DSBCDBI_GET_FLOW_INFO_CMD_HDLR

DESCRIPTION   Processes the GET_FLOW_INFO command. It gets the requested
              link and security layer information and calls the callback
              provided as part of the command.

DEPENDENCIES  This must be called in BCDBAPP task context.

RETURN VALUE  None.

SIDE EFFECTS  None.
===========================================================================*/
void dsbcdb_get_flow_info_cmd_hdlr
( 
  dsbcdbi_cmd_type *cmd_ptr
);

void dsbcdb_update_db_cmd_hdlr
( 
  dsbcdbi_cmd_type *cmd_ptr
);


#endif /* DSBCDB_HDLR_H */
