#ifndef PS_UW_FMC_IFACE_HDLR_H
#define PS_UW_FMC_IFACE_HDLR_H
/*===========================================================================
  @file ps_uw_fmc_iface_hdlr.h

  This file defines the external API for UW FMC module.

  Copyright (c) 2008-2011 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary
===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/3gpp2/uwfmc/inc/ps_uw_fmc_iface_hdlr.h#1 $
  $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "comdef.h"
#include "customer.h"
#include "target.h"
#include "pstimer.h"
#include "ps_crit_sect.h"


/*===========================================================================

                       LOCAL DATA DEFINITIONS

===========================================================================*/
/*---------------------------------------------------------------------------
TYPEDEF PS_UW_FMC_IFACE_HDLR_TUNNEL_HDR_TYPE

DESCRIPTION
  This structure defines tunnel header. This header is pushed on top of
  every packet that is sent by UW FMC IFACE. Fields are
    proto_id  : Protocol ID
    proto_vsn : Protocol Version
    reserved  : All 0's
    stream_id : Stream ID of the tunnel
---------------------------------------------------------------------------*/
typedef struct
{
  int8   proto_id;
  int8   proto_vsn;
  int16  reserved;
  int32  stream_id;
} ps_uw_fmc_iface_hdlr_tunnel_hdr_type;

/*---------------------------------------------------------------------------
TYPEDEF PS_UW_FMC_IFACE_HDLR_TUNNEL_INFO_TYPE

DESCRIPTION
  This structure defines various things associated with UW FMC tunnel. Fields
  are
    crit_section             : Critical section used to protect this structure
                               as it is accessed in multiple task contexts
    ps_flow_ptr              : Back ptr to ps_flow
    uw_fmc_tunnel_hdr        : UW FMC header for all packets transmitted over
                               this tunnel
    nat_refresh_timer_handle : PS timer managing NAT refresh interval
    is_nat_present           : Indicates if wPDIF if behind a NAT. NAT refresh
                               is performed only NAT is detected
    tunnel_info_srvr_addr    : Tunnel parameters are stored in this struct.
    tunnel_info_srvr_addr_len : Length of the tunnel IP addr is stored.
---------------------------------------------------------------------------*/
typedef struct
{
  ps_crit_sect_type                       crit_section;
  ps_flow_type                          * ps_flow_ptr;
  ps_uw_fmc_iface_hdlr_tunnel_hdr_type    uw_fmc_tunnel_hdr;
  ps_timer_handle_type                    nat_refresh_timer_handle;
  boolean                                 is_nat_present;
  struct ps_sockaddr                      tunnel_info_srvr_addr;
  int16                                   tunnel_info_srvr_addr_len;
} ps_uw_fmc_iface_hdlr_tunnel_info_type;
#ifdef FEATURE_UW_FMC
/*===========================================================================

                      EXTERNAL FUNCTION DEFINITIONS

===========================================================================*/

/*===========================================================================
FUNCTION PS_UW_FMC_GET_IS_FMC_ENABLED()

DESCRIPTION
  Accessor function to is_fmc_enabled
  
PARAMETERS
  None
  
RETURN VALUE
  Returns the value of is_fmc_enabled as provisioned by EFS NV

DEPENDENCIES
  None

SIDE EFFECTS
  None
===========================================================================*/
boolean ps_uw_fmc_get_is_fmc_enabled
(
  void
);
#endif /*FEATURE_UW_FMC*/
#endif /* PS_UW_FMC_IFACE_HDLR_H */
