#ifndef DS707_ROAMING_H
#define DS707_ROAMING_H

/*===========================================================================

                        D S 7 0 7 _ R O A M I N G 
GENERAL DESCRIPTION
  This file contains the roaming set up needed for the mobile when 
  the phone roams between the North America and Japan's KDDI network.
  
  
EXTERNALIZED FUNCTIONS
  
INITIALIZATION AND SEQUENCING REQUIREMENTS
  
 Copyright (c) 2007-2008 by Qualcomm Technologies Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================
                        EDIT HISTORY FOR MODULE

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/3gpp2/ds707/inc/ds707_roaming.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $
  
-----------------------------------------------------------------------------  
when        who    what, where, why
--------    ---    -------------------------------------------------------
12/06/07     sy    Initial revision.
===========================================================================*/


/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/

#include "comdef.h"
#include "customer.h"

/*===========================================================================

FUNCTION  DS707_ROAMING_INIT 

DESCRIPTION  This initialization function will take care of the data roaming
             between Japan and North America.

DEPENDENCIES
  

RETURN VALUE
  
SIDE EFFECTS
===========================================================================*/
void ds707_roaming_init( void );


/*===========================================================================
FUNCTION  DS707_ROAMING_IS_CURR_MODE_JCDMA

DESCRIPTION   Called to check if it is in JCDMA mode. 
              Return true for JCDMA mode is set otherwise 
              return false.

DEPENDENCIES   This parameter will be set with System Determination
               API.

RETURN VALUE
  
SIDE EFFECTS
===========================================================================*/
boolean ds707_roaming_is_curr_mode_jcdma(void);
/*===========================================================================
FUNCTION DS707_ROAMING_IS_SIMPLIFIED_JCDMA_MODE

DESCRIPTION
  If current is in simplified JCDMA mode

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  TRUE - simplified JCDMA mode
  FALSE - otherwise

SIDE EFFECTS
  None

===========================================================================*/
boolean ds707_roaming_is_simplified_jcdma_mode(void);
/*===========================================================================
FUNCTION  DS707_ROAMING_READ_JCDMA_NV

DESCRIPTION   Called to read the JCDMA mode NV value

DEPENDENCIES   

RETURN VALUE
  
SIDE EFFECTS
===========================================================================*/
void ds707_roaming_read_jcdma_nv (void);
/*===========================================================================
FUNCTION  DS707_ROAMING_READ_SIMPLIFIED_JCDMA_NV

DESCRIPTION   Called to read the simplified JCDMA mode NV value

DEPENDENCIES   

RETURN VALUE
  
SIDE EFFECTS
===========================================================================*/
void ds707_roaming_read_simplified_jcdma_nv (void);

#endif
