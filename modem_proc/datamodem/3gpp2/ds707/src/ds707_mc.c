/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
 
                           D S 7 0 7 _ M C 

GENERAL DESCRIPTION
  This file contains functions that are used to interface with the CDMA2000
  MC.
  
EXTERNALIZED FUNCTIONS
  Returns a pointer to an MC command buffer.

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None

 Copyright (c) 2002 by Qualcomm Technologies Incorporated.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

  $PVCSPath: L:/src/asw/MM_DATA/vcs/ds707_mc.c_v   1.1   18 Nov 2002 18:20:40   akhare  $
  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/3gpp2/ds707/src/ds707_mc.c#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when       who        what, where, why
--------   ---        -------------------------------------------------------
11/17/02   ak         Updated file header comments.
===========================================================================*/

/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"

#ifdef FEATURE_DATA_IS707
#include "ds707_mc.h"
#include "mc.h"


/*===========================================================================
                        TYPEDEFS AND VARIABLES
===========================================================================*/


/*===========================================================================
                   EXTERNAL FUNCTION DEFINITIONS
===========================================================================*/
#endif /* FEATURE_DATA_IS707 */
