#ifndef DSTASK_V_H
#define DSTASK_V_H
/*===========================================================================

                     D A T A   S E R V I C E S   T A S K

                            H E A D E R   F I L E

DESCRIPTION
  This is the external header file for the Data Services (DS) Task. This file
  contains all the functions, definitions and data types needed for other
  tasks to interface to the Data Services Task.

  Signal and command definitions for the Data Services Task, and functions
  that should be used by other tasks to send commands to the Data Services
  Task are defined here.


  Copyright (c) 2001-2014 by Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary.
===========================================================================*/


/*===========================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $PVCSPath: L:/src/asw/MM_DATA/vcs/dstask.h_v   1.17   28 Feb 2003 18:56:06   rchar  $
  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/3gpp2/dsmgr/inc/dstask_v.h#7 $ $DateTime: 2016/05/01 22:58:09 $ $Author: sanand $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
02/18/15   ab      Added a new cmd for wp addr change ev
7/27/14    pg      ATCoP changes for Data Plus Data.
07/08/14   sc      Fixed SGLTE+G issue on SUB2.
07/07/14   ab      Added a new API to put cmd in the internal DS queue    
04/15/14   sc      Fixed +COPS DSDS issue.
04/03/14   pg      Fixed subs feature mode updation.
03/15/14   scb     Added support for MPPM MMGSDI SUBSCRIPTION INFO.   
02/25/14   scb     Added support for MPPM EPC timers. 
02/15/14   pg      Added support for SGLTE+G
12/18/13   sc      Fixed +CSIM APDU response size issue.
11/21/13   scb     Add support for WLAN_MAX_CONN reached indication in MPPM
11/12/13   sc      Added support for $QCCSGCOPS command.
11/06/13   scb     Add support for PDN Database updates in MPPM
10/18/13   sc      Added support for $QCNSP, $QCSIMT, $QCRCIND commands and
                   REMOTE CALL END, REMOTE RING, REMOTE ANSWER
                   unsolicited result codes.
09/23/13   scb     Add support for throttling in MPPM.
09/23/13   ssb     Changing variable name from ds_3gpp_pdn_bearer_wm_type
                   to ds_3gpp_pdn_bearer_wm_info
09/16/13   scb     Add support for LTE_ATTACH_INIT status indication in MPPM
09/15/13   rs      Common build changes for dual sim and triple sim
07/02/13   pg      Migrated to MMGSDI API to find PLMN info in SE13 table
06/05/13   tk      ATCoP changes for SGLTE support on Dime Plus.
05/24/13   sd      TSTS changes for DS3G.
05/02/13   vs      NV item to check call origination is allowed before PS attach
03/17/13   tk      Fixed stack overflow issue with emergency number list.
02/22/13   op      Added support for the IWLAN_S2B module
02/22/13   tk      ATCoP - CM interface changes for Triton DSDA.
02/07/13   tk      ATCoP changes for DSDx Co-existence with TDS.
01/25/13   sd      Mainlining Modem Statistics Feature
01/23/13   sk      DS command buffer memory reduction.
11/21/12   sk      Fixed CSQ issue due to invalid updation of RSSI,BER values.
11/08/12   ssb     Dun Call Entitlement support using PS_SYS Interface
10/23/12   scb     Added support for MPPM in DS task. 
09/24/12   tk      Migrated to CM APIs for MRU table access.
06/19/12   nd      MT PDP pkt call.
08/06/12   tk      Added support for $QCDRX command.
07/31/12   sk      C-EONS Feature Support.
07/16/12   nc      New interface changes for Manual PLMN selection.
06/12/12   tk      Extended $QCSYSMODE command for 3GPP2 modes.
07/29/11   ua      Updated ds_at_mmgsdi_sim_info_type to have accessed rec num
                   and accessed offset to be used in mmgsdi read handlers.
04/19/12   sb      Feature Cleanup: Always On Flag FEATURE_UIM_SUPPORT_3GPD
03/07/12   sk      Made CLCC common for all modes.
03/12/12   ss      RPM: Fixed issue of oprt_mode and regd_serv_domain not 
                   being set after UE power-up.
03/09/12   msh     Removed FEATURE_DATA_NIKEL     
02/23/12   kv      Replaced FEATURE_DATA_TEST_LOOPBACK_HDLR with
                   FEATURE_DATA_TEST_LOOPBACK.
02/17/11   ua      Added support for $QCRMCALL. 
02/17/12   sk      Migrating Data related AT commands interface to unified MH.
02/08/12   vs      Added support for 3GPP IPv6 over PPP
02/08/12   sk      Feature cleanup.
02/01/12   ss      RPM: Fixed of sending ATTACH request again due to PS session
                   initiation after PS ATTACH is ignored by network.
02/06/12   ad      Added CSTF/CEN/CGPIAF/CVMOD command support.
01/31/12   sk      Removed featurization for call_direction.
01/19/12   sk      Feature cleanup.
01/16/12   ack     Fix to post a cmd for iface bringup/teardown
01/02/12   msh     Coan: Feature cleanup
12/26/11   ss      Added support for new command to process IPCP delayed NAK.
11/17/11   sa      Added support for V6 Loopback Mode B.
11/02/11   msh     Added DS_707_PARSE_DORM_TIMER_SOCM_CMD     
10/31/11   ssh     eHRPD Always On support
10/20/11   nc      Added support for CUAD/CEAP/CERP commands.
10/25/11   ua      Adding EFHPLMN to DSAT_MAX_NUM_REFRESH_FILES. 
10/24/11   ss      Fixed RPM mmgsdi refresh registration issue.   
10/17/11   ad      Added +CECALL support.
09/06/10   nc      Fixed the RSSI and RSCP display when 
                   FEATURE_WCDMA_DATA_FOR_UI_BAR_DISPLAY is enabled
09/15/11   vpk     Tear down all the ifaces when an UIM refresh happens.
09/08/11    ash    Added support for eHRPD to HRPD fallback.
09/05/11   ss      Changes to support RPM.
08/20/11   ttv     GSDI to MMGSDI_SESSION api migration changes.
07/05/11   nc      Added support for ^MODE,^CARDMODE,^SYSCONFIG,^SYSINFO,^DSCI.
06/06/11   ash     Added support for PPP Partial Context
06/20/11   ttv     Merged silent redial support changes for UMTS tethered calls.
04/11/11   ttv     Added support for Hot Swap feature.
03/24/11   sa      Added MT PDP Auto answer support.
03/23/11   sn      Globals cleanup for free floating of tasks.
03/14/11   sch     Added DS3GPP_DSM_MEM_CTRL_EV_CMD and related event type
03/02/11   ss      Added support for configurable RAB_REESTAB RETRY.
03/01/11   nc      Modified +CEMODE to be inline with CM Design.
02/11/11   vs      DUN changes for CSFB Fusion
02/02/11   ack     Added DS_RLP_INIT_CMD
02/04/11   mg      Added DS_707_PWROFF_LPM_MODE_CMD to process pwroff/lpm 
                   mode notification from CM
01/21/11   sa      Add support for CSFB call in service change SS evt from LTE to 1x.
01/17/11   ss      Added support for Iface linger.
01/14/11   ack     Added PHY_LINK_DOWN_EV reg/dereg to dssnet4/6 SM
01/04/11   op      Added MMGSDI event for ACL enable/disable check
12/15/10   ms      Handle Iface related events by posting command to DS task.
11/22/10   ttv     Added support for DSDS.
11/19/10   ars     Changed proc of setupBridgeInterfaceEvent to command based
10/25/10   ad      Init Changes for DSDS. 
10/23/10   pp      ds_send_cmd[_ext]APIs are introduced for sending commands 
                   to DS task. Cmd payloads are de-coupled from Task buffer.
10/12/10   mg      Changes for error code propogation to ps_iface  
09/08/10   kk      Including pbmlib.h for CMI compliance.
08/22/10   vk      Use a single signal for all forwarding ports
07/27/10   bs      Fixed MMGSDI commands queuing in RPC context.
07/09/10   asn     Support for Arbitration
07/08/10   ad      Added support for $ECALL command.
07/08/10   ssh     New command for PDN Inactivity timer expiry
07/07/10   ms      Data statistics support for Field Test Display.
06/25/10   vs      Support for Carrier Detect Signals for Fusion.
06/17/10   as      Fusion additions
06/04/10   bs      MMGSDI SESSION Migration.
05/17/10   ad      Added Support for +CEREG for LTE.
05/17/10   hs      Added APN disable and PDN inactivity feature support.
05/10/10   kk      Added support for $QCVOLT, $QCHWREV, $QCBOOTVER, $QCTEMP,
                   $QCAGC, $QCALLUP.
05/10/10   kk      Added support for ^DSDORMANT command.
05/10/10   kk      Added support for ^HDRCSQ command.
04/21/10   sa      Added support for Loopback Mode B.
04/14/10   sn      Mainlined GOBI changes.
03/15/10   ls      Silent Redial Feature
02/04/10   asn     Process DSM Mem events in DS Task context
01/16/10   ls      Add Message id when registering client id
01/15/10   ua      Added support for ^CPIN, CPIN, CLCK, CLCC for CDMA.
01/06/10   bs      Added +CGACT command support for LTE.
11/16/09   ca      Added support for the MMGSDI authentication
                   interface commands.
12/15/09   nc      Featurisation changes for LTE.
12/12/09   sa      Added support for ATCoP - Mode handler interface.
11/25/09   ls      Add support for retrieving slot info
11/22/09   vk      Added support for internal DS command queue
11/09/09   ls      Add support for retrieving IMSI info from USIM card
10/21/09   ls      Add app_type field into ds3g_mmgsdi_event_info
10/20/09   ss      Feature wrapping BCMCS header file inclusion and definitions.
09/22/09   vrk     Merged LTE changes.
08/22/08   ua      Added support for N/w initated Qos.
09/09/09   ua      Added support for EONS (spec 22.101).
08/10/09   ms      Changes related to CM AU level CMI changes.
08/10/09   ms      Remove private header file.
08/07/09   ls      Add union structure for CSIM feature
07/23/09   ls      Support FEATURE_CSIM
07/16/09   sch     Adding item srv_status to structure ds_ss_info_type
07/02/09   ss      WLAN decoupling from DS task.
06/19/09   ls      Support operation mode change
06/12/09   pp      DS Task de-coupling: Removed PS logical mode handler
                   references.
04/30/09   ls      Support LPM mode change.
04/20/09   bs/sa   Added support for Modem Bridge Architecture.
03/13/09   ls      Merge the changes from MFLO
03/09/09   sa      AU level CMI modifications.
03/06/09   ms      CMI Integration fixes.
02/15/09   ls      Added DS_CALL_TYPE_PKT_IS707B and epzid in ds707_pkt_call_info_type
1/13/09    nc      Fixing Featurisation issue.
12/29/08   nc      Added support for +VTS Command
12/17/08   ms      Added code to handle the pref_mode_change_event from CM
                   and to tear down the call (dormant and active both).
11/21/08   sa      Added synchronization of +CRSM command with MMGSDI.
10/23/08   bs      Added support for 1X AT Phonebook commands.
10/18/08   pp      Added support for SLIP interface.
10/17/08   bs      Fixed Klocwork issues.
09/30/09   yz      Increased DS command buffer size to 32 for UMTS/GSM, added
                   timestamp info in DS command.
06/23/08   ar      Added QMI proxy iface mode handler support.
09/03/08   ms      Feature enhancements for BCMCS 2.0 Phase2
08/05/08   ms      IS707B addendum Feature support.
07/21/08   bs      Added support for CDMA NAARefresh.
07/21/08   bs      Added support for +CREG=2 command.
07/16/08   ua      Added support for external client support.
                   Optimized received SIO signals usage. This is feature wrapped in
                   FEATURE_DSAT_EXT_CLIENT_SUPPORT.
07/08/08   ua      Added support for DSUMTSPS_PDP_IP_IRAT_TX_UM_CMD.
06/30/08   ssh     UICC support.
06/16/08   sn      Removed "snd.h" include as it is not required.
06/13/08   ua      Correcting reading of data from RUIM/NV items
                   for OMH 2.
04/15/08   ms      Feature BCMCS2.0 Enhancements
02/20/08   vd      MBMS Support.
02/14/08   vk      Graceful termination of PDP-PPP calls.
02/04/08   ssh     Changes to MIPv6 commands
01/29/08   asn     Serialize processing of FLOW IOCTL - QOS_RELEASE
                   by moving it to DS Task context
01/11/08   vk      Changes to propagate HSPA call status indication
01/10/08   spn     Added Commands for Memory Based Flow Control
12/12/07   as      Added support hybrid mode hdr serving system
11/30/07   asn     Added support for new feature MT-PDP terminating in TE
11/20/07   vp      Added support for MIPv6
11/15/07   sc      Added support to send PPP auth starting event to IFACE
10/08/07   ar      Added DS_SMD_DATA11_PORT_1ST_SIO_RX_SIG
09/21/07   ss      Added rtre_control in ds_at_ph_info_u_type.
09/11/07 sq/mga    Merged changes for FEATURE_RUIM_FUNC_INTERFACE
09/07/07   rt      Added support for IWLAN.
08/16/07   as      Added support for DS_707_PKT_SYS_CHG_CMD
06/19/07  ak/squ   Added a new member named 'call_inst' to the
                   structure: ds_call_info_type
06/11/07   as      Added DS_HDR_AN_PPP_START_CMD
06/06/07   ar      Add CM call ID to ds_ucsd_uus_data_cmd_type.
05/29/07  squ/mga  Added new field ln_status to union cmd
                   Added   DS_707_HDR_LN_STATUS_CMD.
05/01/07   sk      QoS Modify support
04/20/07   vk      Processing GSNDCP reg cnf cbs in DS context
03/28/07   sa      Added active_calls_list_s_type to ds_at_call_info_s_type.
03/08/07   ua      Added sim state in ds_at_ss_info_s_type
02/04/06   sr      Added support for STA UMTS mode handler commands
02/14/07   vd      Lint error fixes.
01/10/07   sq      Added support of DS_707_RETRY_DELAY_ORIG_CMD.
01/18/06   rr      Added dormant_reorig in dsumtsps_call_info.
12/07/06   as      Added DEREGED cmd used for MIP deregistration
11/16/06   sq      Add DS_707_PH_SYS_SEL_PREF_NORMAL_CMD.
11/09/06   vrk     Post command to DS to process tx high and low wmk
10/31/06   spn     Changes to consolidate a burst of commands in flow mgr
09/26/06   sa      Inter RAT handover support for $QCDGEN.
09/06/06   rsl     Added 2 new commands PHYS_LINK_FLOW_ENABLE and IFACE_FLOW_ENABLE.
09/06/06   ks      Featurized under FEATURE_MMGSDI.
09/05/06   ks      Added members to ds_qmi_cmd_type for QMI sim support.
08/23/06   ks      Removed RMNET_SIO_SIG.
07/14/06   ks      Added structs to hold CM SS and PH info in ds_qmi_cmd_type
07/11/06   ua      CACM/CAMM modifications according to the new CM API's
                   added cm_ph_aoc_info_s_type member in ds_at_ph_info_u_type
07/06/06   sk      QoS 2.0 Support
05/12/06   ua      Added mmgsdi_sim_info_type to provide MMGSDI support for CPOL command.
                   It is wrapped in FEATURE_MMGSDI.
05/01/06   vp      Defining sio_rx signals for shared memory data ports (for
                   multiple processor builds)
04/16/06   squ/gr  Added failed_orig and call_cmd_err fields to the ds_call_info_type.
03/06/06   sq      Added support to ds_hdr_an_ppp_ev_cmd
02/21/06   lyr     Added support for WLAN QoS-related cmds and types
01/19/06   sl      Serializer related changes.
11/14/05   gr      Changes to throttle the um link when the rm link is slower
                   than the um link (specifically the BT scenario)
10/12/05   mct     Added DS_DSSNET4_FLOW_DELETED_EV_CMD.
10/10/05   ks      Removed DS_RMNET_SIO_RDM_[OPEN/CLOSE]_CMD.
09/13/05   ks      Featurized cm_call_orig_fail_layer_e_type temporarily.
09/13/05  jd/ks    Added DS_RMNET_SM_CMD. Added ds_rmnet_sm_cmd_type to
                   ds_cmd_type.
09/09/05   vrk/gr  EPZID enhancements releated to L2/L3 ack
09/09/05   ks      Corrected featurization for nas member union
                   in ds_qmi_cmd_type.
09/08/05   ks      Added member for qmi_nas in ds_qmi_cmd_type.
09/02/05   rc      Added support for Mobile Initiated QOS Modification.
08/26/05   ar      Added support for CS Data in-call modification & fallback.
08/05/05   sk/vs   Merged the HDR Semi-Dynamic QOS changes from sandbox.
08/05/05   kvd     Featurized #include dsdctm.h nder DS707.
08/03/05   hap     Added csd_rate_adaption to ds_at_call_info_s_type
08/03/05   kvd     Featurized mip_ppp_status under DS707.
07/29/05   ar      Remove ds_at_cm_ss_event_type. Added DS_AT_CM_MSG_SIG.
07/22/05   kvd     Added ds_mip_ppp_status to cmd union in ds_cmd_type.
07/18/05   ar      Add support for asynchronous call setup response.
06/27/05   ks      Added QMI_CHARGER cmd
06/27/05   snb     Add field to ds_at_ps_data_gen_type supporting $QCDGEN
                   data send over multiple contexts simulataneously.
05/31/05   jd      Added QMI command.
06/09/05   aku     Added support for WLAN WPA
05/31/05   jd      Added QMUX receive signal and RDM cmd user data field.
05/25/05   atp     Added support for semi-dynamic QOS on DO (flow_mgr cmds).
05/20/05   ar      Added support for DS_CM_CALL_PROGRESS_CMD event.
05/11/05   ks      merged the change for ds_at_cm_ph_pref_type for CM mode
                   preference event from tip, as UMTS needs it.
04/21/05   mct     Removed include ps_bcmcs.h.
04/26/05   gr      Added the inc_call flag in the ds707_pkt_call_info_type
04/20/05   mct     Removed phys_link down references for dssnet/rmsm.
04/20/05   ar      Add ds_at_cm_ph_pref_type for CM mode preference event.
04/14/05   tkk     Added new values to ds_at_ui_cmd_e_type and removed
                   FEATURE_DSAT_SOUND portion.
04/08/05   ar      Add ds_at_cm_ph_pref_type for CM mode preference event.
04/06/05   gr      Moved the ds707_epzid.h include under 707 data feature
04/05/05   gr      Integrated EPZID functionality onto the main line
04/01/05   snb     Add channel_id field to ds_at_gsdi_apdu_resp_type for +CSIM
                   and +CRSM commands with USIM card.
03/15/05   ks      Removed Featurization of RMNET and QMI.
03/11/05   snb     Added DS_AT_UI_CMD event.
03/08/05   ks      Changed names of RM NET signals, cmds, functions.
03/02/05   snb     Add value to cmd for ds_cmd_type, associated data type and
                   nsapi info to ds_at_call_info_s_type in support of AT
                   command for generating PS data, merged ss_info and
                   call_direction additions to ds_at_call_info_s_type from
                   Sirius branch and removed timezone feature definition.
03/01/05   kvd     Added RDUD cmds.
02/28/05   rr      Added end_type to ds_call_info_type to distinguish between
                   CM's and Internal call end commands
02/21/05   sl      Serializer related changes.
02/15/05   jd      Added Rm Network interface rmsm event commands
02/10/05   sb      Accessories related changes.
01/19/05   ar      Merge in changes from ATCOP development branch.
01/12/05   aku     Added support for WLAN,
12/07/04   ar      Add available and preferred networks to PH info event.
                   Add sys_id to SS info event.
12/02/04   vas     Numbered all the DS commands
11/20/04   kvd     Added CAM module related DS commands.
11/30/04   dwp     Add FEATURE_UUS around relative types.
11/29/04   vr      Merged PZID hysteresis changes into main line
11/19/04   tkk     Updated "ds_at_ss_info_s_type" structure with
                   "sys_mm_information_s_type" element for supporting
                   +CTZR
11/18/04   dwp     Added DS_UCSD_USER_USER*_CMD, ds_ucsd_uus_data_cmd_type, &
                   ds_ucsd_uus_info_type to ds_ucsd_app_orig_call_cmd_type.
11/17/04   mv      Added DSUMTSPS_SEND_MT_REJECT_CMD.
11/14/04   mv      Added mt_flag to dsumtsps_call_info_type structure and
                   dsumtsps_mt_index to the ds_cmd_type structure.
11/09/04   ar      Generalize ATCOP GSDI SIM read to support write.
11/04/04   gr      Added EPZID_HYS_SDB_DATA_CMD to notify the hysteresis
                   engine whenever SDB packet data is exchanged
11/03/04   dwp     Added reject to ds_ucsd_app_answer_call_cmd_type.
10/12/04   vr      Added EPZID_HYS_DATA_READY_CMD to notify the hysteresis
                   engine whenever PPP data is exchanged
09/20/04   vas     QOS related changes
09/17/04   rc      Added DS_CM_SS_PS_DATA_FAIL_CMD.
09/10/04   sb      S-PDP changes
09/03/04   snb     Add support for AT command +CSIM under FEATURE_MMGSDI_
                   ATCSIM and structure additions for command +CHLD.
09/02/04   vas/jkomenda Added command for CM subscription changed event.
                   Added signal for RUIM command processing
07/16/04   ar      Add dial modifiers to ds_ucsd_app_orig_call_cmd_type.
07/06/04   snb     Add support for personalization PINs for ATCOP.
06/01/04   ar      Added DS_UCSD_GSDI_MSG_SIG definition.
05/25/04   vr      Fixed featurization bug for FEATURE_BCMCS
05/21/04   jd      Renamed FEATURE_AT_707_REINIT to FEATURE_UIM_SUPPORT_3GPD
05/17/04   vr/kvd  Added new BCMCS commands and command data types.
04/07/04   gr      Added force_cdma_only parameter into
                   ds707_pkt_call_info_type
03/22/04   gr      Added command to process the expiry of Go Null timer.
                   Go Null timer is the same as Abort Dormant PPP timer.
02/27/04   kvd     Added DS_707_RMSM_UM_MIP_IP_ADDR_CHANGED_CMD.
02/24/04   snb     Added support for handling SMSes via signals.
02/02/04   jd      Removed RMSM MIP_DOWN cmd, renamed MIP_UP as IFACE_UP
01/06/03   snb     Move registration for pin event with GSDI and handling
                   callback for pin events to common file and add protocol
                   stack re-initialization under feature define.
12/02/03   sb      Open ATCOP SMS events to both ETSI and CDMA
11/11/03   ar      Adjust ETSI wrapper for ATCOP events.
10/29/03   snb     Added support for ATCOP initiated asynchronous SIM read
                   and relocation of CM SS and PH cmd/events to common files.
10/14/03   ar      Add support for ATCOP network registration reporting.
10/13/03   vas     Removed Call Connected/End events for HDR AN module
10/13/03   kvd/vr  Added DS_707_INITIATE_ALN_CMD
10/01/03   ak      Added EPZID Delay timer command.
10/01/03   ar      Restructure ATCOP event types to minimize storage.
                   Remove FEATURE_DATA_ETSI_SUPSERV wrappers
10/01/03   sb      SMS Cell broadcast additions
09/10/03   ar      Added support for CSD handover event.
09/10/03   ak      Removed extern of ds_esn.  never used.
09/02/03   sy      Added epzid hysteresis timer expired command.
08/27/03   vr      Added new cmd for Idle Digital Mode changed and added
                   new_network field to ds707_pkt_call_info_type
08/24/03   ak      New cmds for IFACE_UP, _DOWN and _ROUTEABLE indications
                   for 1X packet.
07/25/03   ak      Added call_mode field to call_info struct.
06/20/03   dgy     Added UMTS CS Data API commands.
05/21/03   vas     Added HDR commands.
04/18/03   sb      Partition CM supserv event info to conserve storage.
04/18/03   vsk     updated to use new UMTS RMSM cmd and names.
                   added info_ptr to dsumtsps_call_info_type
04/11/03   ak      Updated to use new 707 RMSM CMD names.
02/26/03   rc      Added field ps_data_suspend to ds_ss_info_type.Renamed
                   DS_CM_SS_SYS_MODE_CHG_CMD as DS_CM_SS_SRV_CHG_CMD.
01/10/03   sb      Event handler declarations for +CGATT command
11/14/02   jd      Removed DS_RMSM_UM_PPP_UP_CMD (not used)
11/13/02   rc      Added multimode support for GPRS.
11/12/02   ar      Add support for supplementary services events
11/11/02   sb      Call Related CM events now apply in all modes
10/30/02   rsl     Made call_cmd and call_event common.
10/14/02   ak      Added client_id to call_info_type.  This helps support
                   JCDMA.
10/14/02   ak      Removed JCDMA commands - no longer needed.
10/14/02   sb      Merge from ETSI. PBM event declarations and state machine.
10/14/02   vsk     Added support to get statistics at end of call
10/10/02   ak      Updated for JCDMA 1X.
10/04/02   usb     Added Generic RM State Machine specific commands
10/03/02   rc      Added UMTS specific command handling.
08/28/02   usb     Added/renamed RMSM cmd names
08/14/02   atp     Added DS_707_ASYNC_ATZ_CMD.
08/13/02   dgy     Added return value to ds_get_call_information() and
                   ds_get_current_session_data_counters().
08/02/02   dgy     Added support for UI data call screen.
08/01/02   rc      Removed FEATURE_DS_SOCKETS wrap around socket commands.
                   Added DS_FLOW_CTRL_DSMGR_MASK to sockets flow control mask.
07/29/02   ak      Added use_hdown_timer field to ds707_pkt_call_info_type.
07/24/02   atp     Added DS_707_ASYNC_PHYS_LINK_TEAR_DOWN_CMD.
07/22/02   ak      Added DS_707_PKT_IFACE_DOWN_CBACK_CMD.
07/19/02   aku     Moved flow control masks to ds_flow_control.h
07/16/02   aku     Removed SOCKET featurization for dssnet flow mask.
07/13/02   atp     Added commands for 707 async.
07/01/02   ak      Added signal for re-origing a 707 pkt call.
06/18/02   tmr     Added DS_COMPLETE_LL_DISCONNECT_CMD, DS_GCSD_RX_DCD_ON_CMD and
                   DS_GCSD_TX_DATA_FLUSHED_CMD
05/21/02   smp     Added DS_CM_SS_SYS_MODE_CHG_CMD.
05/21/02   ak      #ifdef'd some RAB stuff since CM does it.  Should soon
                   go away.  But this helps compile.
05/20/02   usb     Added new command ids for RMSM (IS707 pkt call Rm handler)
05/16/02   smp     Updates for Multimode CM.
05/08/02   ak      Added end_status to call_info type.
05/08/02    wx     Remove FEATURE_DSAT_TIMER
04/19/02   vsk     Added PDP context preservation support
04/17/02   rc      Added DS_INITIATE_CALL_CMD, DS_ENTER_SOCKET_MODE_CMD,
                   DS_EXIT_SOCKET_MODE_CMD, DS_ORIG_SOCKET_NET_CMD,
                   DS_RELEASE_SOCKET_NET_CMD. Added ds_call_type_e_type and
                   ds_call_stack_e_type and ds_orig_call_type.
04/04/02   tmr     Modified definitions of DS_TASK_START_SIG,
                   DS_TASK_STOP_SIG and DS_TASK_OFFLINE_SIG
02/26/02   tmr     Added DS_COMPLETE_LL_CONNECT_CMD, DS_RELEASE_CALL_CMD
                   and DS_GCSD_XID_NEG_COMPL_CMD.
02/05/02   rlp     Added online-command mode support.
01/11/02   smp     Merged from packet data branch. Added call_type parameter
                   for commands received from CM.
11/21/01   tmr     Added DS_GCSD_DISCONNECT_CMD command ID
10/31/01   smp     Removed extra comma from ds_cmd_enum_type.
08/31/01   smp     Updates due to CM name changes.
05/24/01   smp     Initial version.

===========================================================================*/


/*===========================================================================

                      INCLUDE FILES

===========================================================================*/
#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"

#include "dstask.h"
#include "queue.h"
#include "sio.h"
#include "cm.h"

#include "cm_dualsim.h"
#include "rdevmap.h"
#include "sys.h"
#include "nv.h"
#include "task.h"
#include "cause.h"

#include "ps_iface.h"
#include "ps_ppp_defs.h"
#include "dsatclient.h"

#ifdef FEATURE_DATA_IS707
#include "ds707_epzid.h"
#endif /* FEATURE_DATA_IS707 */

#ifdef FEATURE_MMGSDI
#include "mmgsdisessionlib_v.h"
#include "mmgsdilib.h"
#endif /* FEATURE_MMGSDI */

#ifdef FEATURE_ECALL_APP
#include "ecall_app.h"
#endif /* FEATURE_ECALL_APP */

/* this file should be included only in GSM or WCDMA mode */
#if defined(FEATURE_ETSI_SMS) || defined(FEATURE_CDMA_SMS)
#include "wms.h"
#endif /* defined(FEATURE_ETSI_SMS) || defined(FEATURE_CDMA_SMS) */

#if defined(FEATURE_ETSI_PBM) || defined(FEATURE_DSAT_CDMA_PBM)
#include "pbmlib.h"
#endif /* defined(FEATURE_ETSI_PBM) || defined(FEATURE_DSAT_CDMA_PBM) */

#if ((defined(FEATURE_DATA_WCDMA_CS)) || (defined(FEATURE_DATA_GCSD)))
#include "dsucsdappif.h"
#endif /* FEATURE_DATA_WCDMA_CS || FEATURE_DATA_GCSD */

#include "sys.h"
#include "ps_in.h"
#ifdef FEATURE_DATA_PS_EAP 
#include "ps_eap.h"
#include "ps_eap_sim_aka.h"
#endif /* FEATURE_DATA_PS_EAP */

#ifdef FEATURE_HDR_QOS
#include "ds707.h"
#include "dsrlp_v.h"
#include "ds707_qosdefs.h"
#include "ps_qos_defs.h"
#include "ps_rsvp.h"
#include "timer.h"
#endif /* FEATURE_HDR_QOS */

#ifdef FEATURE_DATA_IS707
#include "dsdctm.h"
#endif /* FEATURE_DATA_IS707 */

#ifdef FEATURE_MBMS
#error code not present
#endif /* FEATURE_MBMS */

#ifdef FEATURE_HDR_BCMCS
#include "dsbcmcs_defs_v.h"
#endif /* FEATURE_HDR_BCMCS */

#ifdef FEATURE_DSAT_GPS_CMDS
#include "pdapi.h"
#endif /* defined(FEATURE_DSAT_GPS_CMDS) */

#ifdef FEATURE_DSAT_GOBI_MAINLINE 
#include "ftm.h"
#include "ftm_diagpkt.h"
#endif /* FEATURE_DSAT_GOBI_MAINLINE */

#include "time_svc.h"

#ifdef FEATURE_DATA_OPTHO
#include "ds707_s101_defs.h"
#endif /* FEATURE_DATA_OPTHO */
#include "ps_sys_ioctl.h"

/*===========================================================================

                      PUBLIC DATA DECLARATIONS

===========================================================================*/

/*---------------------------------------------------------------------------
  Data Services Task Signal Definitions
---------------------------------------------------------------------------*/

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Task top-level signals
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
#define DS_TASK_START_SIG       TASK_START_SIG         /* 0x8000           */
#define DS_TASK_STOP_SIG        TASK_STOP_SIG          /* 0x4000           */
#define DS_TASK_OFFLINE_SIG     TASK_OFFLINE_SIG       /* 0x2000           */
#define DS_CMD_Q_SIG            0x1000   /* Command Queue signal           */
#define DS_DOG_HB_REPORT_SIG    0x0800   /* Dog HB Report signal           */
#define DS_NV_CMD_SIG           0x0400   /* NV item retrieval signal       */
#define DS_TIMER_SIG            0x0200   /* Timer signal                   */

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  IS-707 Sub-task signals
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
#define DS_PKT_REORIG_SIG       0x00000100  /* called when want to reorig  */

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  QMI NAS client signals
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
#define DS_NOTIFY_NAS_SVC_SIG      0x00200000
#define DS_SEND_MSG_WAIT_SIG       0x00400000

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Serial Port signals
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
#define DS_DATA_PORT_1ST_SIO_RX_SIG      0x00100000
#define DS3G_SIOLIB_BLOCK_SIG          0x80000000

#define DS_SIO_RX_PKT_SIG       0x0004   /* Protocol packet rx'ed          */
#define DS_SIO_ESCAPE_SIG       0x0002   /* AT escape sequence detected    */

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  ATCOP Sub-task signals
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
#define DS_AT_MT_MSG_SIG        0x10000  /* MT message received            */
#define DS_AT_SMS_SIG           0x20000  /* Other, non-"MT msg" SMS event  */
#define DS_AT_CM_MSG_SIG        0x40000  /* CM message received            */

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  UCSD Sub-task signals
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
#define DS_UCSD_GSDI_MSG_SIG    0x00010  /* GSDI message received          */
#define DS_UIM_CMD_SIG          0x00020  /* UIM Command Signal             */
#define DS_RMSM_FC_SIG          0x80000  /* RMSM FC Command Q signal */

#ifdef FEATURE_DATA_ATP
#define DS_IMS_MSG_SIG          0x0080   /* signal sent by IMS libary*/
#endif
/*---------------------------------------------------------------------------
  Data call types: circuit-switched or packet. In each mode, circuit-switched
  and packet data calls may be handled by different call control modules.
---------------------------------------------------------------------------*/
typedef enum
{
  /*-----------------------------------------------------------------------
  Main call type is used by different call control modules
  -------------------------------------------------------------------------*/
  DS_CALL_TYPE_UNSPECIFIED = -1,
  DS_CALL_TYPE_CKT = 0,                      /* Circuit-switched data call */
  DS_CALL_TYPE_PKT,                                    /* Packet data call */
  DS_NUM_CALL_TYPES,            /* !! For internal use of 3G Dsmgr only !! */
  /*-----------------------------------------------------------------------
  Sub call type is the subset of main call type
  -------------------------------------------------------------------------*/
  DS_CALL_TYPE_PKT_IS707B                      /* IS707B data call type    */

} ds_call_type_e_type;


/*---------------------------------------------------------------------------
  Data Services Task Command Definitions
---------------------------------------------------------------------------*/

typedef enum
{
  DS_SUBS_NONE = -1,
  DS_FIRST_SUBS = 0,             /* Indicates the ds_cmd is for First Subs */
  DS_SECOND_SUBS = 1,            /* Indicates the ds_cmd is for Second Subs */
#ifdef FEATURE_TRIPLE_SIM
  DS_THIRD_SUBS,
#endif /* FEATURE_TRIPLE_SIM */
  DS_ALL_SUBS,
  DS_SUBS_MAX
}ds_subs_e_type;

#define CSIM_MAX_CMD_APDU_SIZE 260

#ifdef FEATURE_DUAL_SIM

typedef sys_modem_as_id_e_type ds_sys_modem_as_id_e_type;

#else

typedef uint8 ds_sys_modem_as_id_e_type;

#endif /* FEATURE_DUAL_SIM */

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Commands that may be sent to the Data Services Task.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
typedef enum
{
  DS_MIN_CMD = -1,

  /*====================== 3G DSMGR (values 0-199) ========================*/

  /* 3G Dsmgr sub-task commands  (values 0-199) */

  DS_RDM_OPEN_CMD = 0,           /* Cmd from RDM to open a serial port     */
  DS_RDM_CLOSE_CMD = 1,          /* Cmd from RDM to close a serial port    */
  DS_SIO_CLOSE_COMPLETE_CMD = 2, /* Cmd from SIOLIB when sio_close is done */
  DS_CM_CALL_ORIG_CMD = 3,       /* Notification of call origination       */
  DS_CM_CALL_END_CMD = 4,        /* Notification of call end               */
  DS_CM_CALL_INCOM_CMD = 5,      /* Notification of incoming call          */
  DS_CM_CALL_CONNECTED_CMD = 6,  /* Notification that call is connected    */
  DS_CM_CALL_SETUP_CMD = 7,      /* Notif'n of incoming setup indication   */
  DS_CM_CALL_PROGRESS_CMD = 8,   /* Notif'n of remote user being alerted   */
  DS_CM_CALL_CONF_CMD = 9,       /* Confirm that MO call is proceeding     */
  DS_CM_CALL_MODIFY_CMD = 10,    /* Confirm that MO call is proceeding     */
  DS_CM_SS_SRV_CHG_CMD = 11,     /* Notification of srv change             */
  DS_CM_SS_PS_DATA_FAIL_CMD = 12, /* Notif of failure to resume PS data    */
  DS_CM_RAB_REL_CMD = 13,         /* Indication that RAB has been released */
  DS_CM_RAB_REESTAB_IND_CMD = 14, /* RAB has been re-established           */
  DS_CM_RAB_REESTAB_REJ_CMD = 15, /*RAB reestablishment rejected byBS      */
  DS_CM_RAB_REESTAB_FAIL_CMD = 16,/* RAB reestablishment failed            */
  DS_CM_PDP_MODIFY_IND_CMD = 17,  /* Notif for PDP context modification    */
  DS_CM_PDP_PROMOTE_IND_CMD = 18, /* Notif for PDP context promotion       */
  DS_MSH_RAB_REESTAB_REQ_CMD = 19,/*req from mode hdlr to re-estab RAB     */
  DS_TIMER_EXPIRED_CMD = 20,      /* Notification of timer expiry          */

  DS_INITIATE_CALL_CMD = 21,      /* Command to Initiate a call            */
  DS_RELEASE_CALL_CMD = 22,       /* Command to Release a call             */
  DS_CM_PDP_MODIFY_CONF_CMD = 23, /* PDP context modification Success      */
  DS_CM_PDP_MODIFY_REJ_CMD = 24,  /* PDP context modification Failure      */
  DS_CM_CALL_EVENT_ACT_BEARER_IND = 25,
                                  /* LTE Activate Bearer Indication        */
  DS_DTR_EVENT_CMD = 26,          /* DTR changed event notification        */
#ifdef FEATURE_DSAT_MDM_FUSION
  DS_CD_EVENT_CMD = 27,          /* CD changed event notification        */
#endif
  DS_MSGR_RECV_CMD = 28,          /* Message received from MSGR            */
  DS_UT_SYNC_CMD = 29,            /* Sync up request from UT framework     */
  DS_CM_CALL_NW_MODIFY_REQ = 30,  /* NW initated MODIFY request */
  //DS_CNEDSD_PREF_SYS_CHG_CMD = 31,  /* Preferred system change from CNE*/
  DS_DSD_EXT_PREF_SYS_CHANGED_CMD = 31,

  /*-------------------------------------------------------------------------
    ATCoP online mode commands
  -------------------------------------------------------------------------*/
  DS_ENTER_ONLINE_CMD_TX_FLUSH_CMD = 32,    /* Goto online flush TX        */
  DS_ENTER_ONLINE_CMD_NO_TX_FLUSH_CMD = 33, /* Goto online,don't flush TX  */
  DS_ENTER_ONLINE_DATA_TX_FLUSH_CMD = 34,   /* Goto online-data, flush TX  */
  DS_ENTER_ONLINE_DATA_NO_TX_FLUSH_CMD = 35,/* Goto online-data, don't flush TX */
  DS_COMPLETE_ONLINE_CMD_SWITCH_CMD = 36,   /* OK to finish online-cmd switch   */
  DS_COMPLETE_ONLINE_DATA_SWITCH_CMD = 37,  /* OK to finish online-data switch  */
  DS_COMPLETE_LL_CONNECT_CMD = 38,     /* Complete low-level call connection    */
  DS_COMPLETE_LL_DISCONNECT_CMD = 39,  /* Complete low-level call disconnection */
  DS_CM_PS_SIG_REL_CMD = 40,           /* Status from CM of the TC release */
#ifdef FEATURE_DATA_S033_INCOMING_PAGE_HANDLER_SUPPORT
  DS_CM_CALL_INCOMING_PAGE_CMD = 41,  /* process incoming page event from CM */
#endif /* FEATURE_DATA_S033_INCOMING_PAGE_HANDLER_SUPPORT */
  DS_CM_PLMN_ID_CHG_CMD = 42,

  /*-------------------------------------------------------------------------
    CM Access Control commands
  -------------------------------------------------------------------------*/
  DS_CM_AC_CLEAR_1XPPP_STATE_CMD = 75, /* Current state of CLEAR_1XPPP CMD */
#ifdef FEATURE_DATA_CM_CENTRIC_EMERG_MODE
  DS_CM_AC_ENTER_EMERGENCY_MODE_CMD = 76, /*Enter emergency mode cmd*/
  DS_CM_AC_EXIT_EMERGENCY_MODE_CMD = 77, /*Exit emergency mode cmd*/
#endif /*FEATURE_DATA_CM_CENTRIC_EMERG_MODE*/

  /*================ AT COMMAND PROCESSOR (values 200-399) ================*/

  /*-------------------------------------------------------------------------
    ATCoP asynchronous events
  -------------------------------------------------------------------------*/
  DS_AT_ASYNC_EVENT_START = 200, /* Start of ATCoP asynchronous events     */

  DS_AT_TIMER_EXPIRED_CMD = 201, /* a ATCOP timer expire                   */
  DS_AT_CM_CALL_CMD = 202,       /* Call feedback from CM                  */
  DS_AT_CM_CALL_INFO_CMD = 203,  /* Call information from CM               */
  DS_AT_CM_SS_CMD = 204,         /* Serving System feedback from CM        */
  DS_AT_CM_SS_INFO_CMD = 205,    /* Serving System information from CM     */
  DS_AT_CM_PH_CMD = 206,         /* Phone command feedback from CM         */
  DS_AT_CM_PH_INFO_CMD = 207,    /* Phone information from CM              */
  DS_AT_ACC_INFO_CMD = 208,      /* Accessory command/response event       */


#if defined(FEATURE_DATA_ETSI_PIN) && defined(FEATURE_MMGSDI_PERSONALIZATION)
  DS_AT_MMGSDI_PERSO_CMD = 210,    /* Personalization PIN Event from GSDI    */
#endif /* FEATURE_DATA_ETSI_PIN && FEATURE_MMGSDI_PERSONALIZATION */
  DS_AT_UI_CMD = 211, /* UI-ATCOP interface */

  DS_AT_CM_INBAND_CMD = 212,     /* Inband cmd from CM                   */
  DS_AT_CM_INBAND_INFO_CMD = 213, /* Inband event from CM                   */

#ifdef FEATURE_DSAT_GOBI_MAINLINE
#ifdef FEATURE_DATA_IS707
  DS_AT_SIMST_CMD =214,
#endif /*FEATURE_DATA_IS707 */
#endif /* FEATURE_DSAT_GOBI_MAINLINE */

/*-------------------------------------------------------------------------
    CDMA and ETSI SMS AT asynchronous events
  -------------------------------------------------------------------------*/
#if defined(FEATURE_ETSI_SMS) || defined(FEATURE_CDMA_SMS)
  DS_AT_SMS_ERR_CMD = 220,              /* Error Event from UASMS           */
  DS_AT_SMS_ABT_CMD = 221,              /* Event to tell ATCOP to abort cmd */
#endif /* defined(FEATURE_ETSI_SMS) || defined(FEATURE_CDMA_SMS) */

#ifdef FEATURE_DSAT_MMGSDI_AUTH
  DS_AT_MD5_ABT_CMD = 222,   /* Event to tell ATCoP to abort MD5 command */
#endif /* FEATURE_DSAT_MMGSDI_AUTH */

  /*-------------------------------------------------------------------------
    WCDMA and GSM asynchronous AT events/commands
  -------------------------------------------------------------------------*/
  DS_AT_STATUS_CMD = 230,  /* used to report the excution status of an     */
                           /* async func call.  The callback function of   */
                           /* the  async command submit this cmd.          */

  DS_AT_GSDI_SIM_INFO = 231,     /* SIM read/write response from GSDI      */

#ifdef FEATURE_MMGSDI_ATCSIM
  DS_AT_MMGSDI_APDU_RESP = 232,    /* Response APDU sent from GSDI           */
#endif /* FEATURE_MMGSDI_ATCSIM */

  DS_AT_CM_SUPS_CMD = 233,       /* Supplemental Services status from CM   */
  DS_AT_CM_SUPS_INFO_CMD = 234,  /* Supplemental Services response from CM */

#if defined(FEATURE_ETSI_PBM) || defined(FEATURE_DSAT_CDMA_PBM)
  DS_AT_PBM_CB_CMD = 235,          /* Pbm async function's call back
                                      function submits this command to send
                                      the results to ATCOP */
#endif /* defined(FEATURE_ETSI_PBM) || defined(FEATURE_DSAT_CDMA_PBM) */
  DS_AT_TO_SIO_LOW_CMD = 236,              /* ds-to-sio watermark hits low */

#ifdef FEATURE_MMGSDI
  DS_AT_MMGSDI_SIM_INFO = 238, /* SIM read/write response from MMGSDI */
#endif /* FEATURE_MMGSDI */

#if defined(FEATURE_DATA_WCDMA_PS) || defined(FEATURE_GSM_GPRS)
  DS_AT_PS_DATA_GEN_CMD = 239,        /* PS Data generation command result */
#endif /* defined(FEATURE_DATA_WCDMA_PS) || defined(FEATURE_GSM_GPRS) */

#ifdef FEATURE_DSAT_EXT_CLIENT_SUPPORT
  DS_AT_SEND_AT_CMD = 240,
#endif /* FEATURE_DSAT_EXT_CLIENT_SUPPORT */

#ifdef FEATURE_MMGSDI
  DS_AT_MMGSDI_INFO_CMD = 241,     /* Response info sent from MMGSDI */
#endif /* FEATURE_MMGSDI */

#ifdef FEATURE_DSAT_MMGSDI_AUTH
  DS_AT_MMGSDI_AUTH_CMD = 242,     /* MMGSDI authentication commands result */
#endif /* FEATURE_DSAT_MMGSDI_AUTH */
#ifdef FEATURE_DATA_PS_EAP 
  DS_AT_EAP_SIM_AKA_TASK_SWITCH_CMD = 243,
  DS_AT_EAP_SIM_AKA_RESULT_IND_CMD  = 244,
  DS_AT_EAP_SIM_AKA_TRP_TX_CMD      = 245,
  DS_AT_EAP_SIM_AKA_SUPP_RESULT_IND_CMD = 246,
#endif /* FEATURE_DATA_PS_EAP */
#ifdef FEATURE_DSAT_GOBI_MAINLINE
  DS_AT_FTM_DIAGPKT_RSP_CMD = 247, /* Response sent from FTM diag request. */
#endif /* FEATURE_DSAT_GOBI_MAINLINE */

#ifdef FEATURE_DATA_GCSD_FAX
  /*-------------------------------------------------------------------------
    GSM FAX asynchronous AT events/commands
  -------------------------------------------------------------------------*/
  DS_AT_FPS_T31_CMD_STATUS = 250,    /* GSM Fax command status             */
#endif /* FEATURE_DATA_GCSD_FAX */

#ifdef FEATURE_MMGSDI
  DS_AT_COPS_AT_CMD = 252,
  DS_AT_CPOL_AT_CMD = 253,
#endif /* FEATURE_MMGSDI */
  DS_AT_FWD_AT_CMD = 301,
  DS_AT_FWD_AT_CMD_REG = 302,
  DS_AT_FWD_AT_CMD_DEREG = 303,
  DS_AT_EXT_CMD_RESP = 304,
  DS_AT_RESET_REQ_CMD     = 305,
  DS_AT_SHUTDOWN_REQ_CMD     = 306,
  DS_AT_EXT_CMD_URC  = 307,
#ifdef FEATURE_DSAT_GPS_CMDS
  DS_AT_PD_INFO_CMD =310,    /* GPS information from PDSM              */
#endif /* FEATURE_DSAT_GPS_CMDS */
#ifdef FEATURE_MMGSDI
    DS_AT_MMGSDI_INIT_INFO = 311,     /* Response info sent from MMGSDI */
#endif /* FEATURE_MMGSDI */

#ifdef FEATURE_MMGSDI
    DS_AT_MMGSDI_OPER_NAME_INFO = 312, /*  */
#endif /* FEATURE_MMGSDI */

  DS_AT_IFACE_EV_CMD     = 320,  /* Iface events related */
#ifdef FEATURE_DATA_MUX
  DS_AT_MUX_NOTIFY_CMD = 322,
  DS_AT_RDM_NOTIFY_CMD = 323,
#endif /* FEATURE_DATA_MUX */

#ifdef FEATURE_DSAT_LTE
#ifdef FEATURE_VOIP 
  DS_AT_VOIPM_AT_CMD = 325,
#endif /* FEATURE_VOIP */
#endif /* FEATURE_DSAT_LTE */

  DS_AT_ASYNC_EVENT_END = 399,  /* End of ATCoP asynchronous events        */

  /*====================== UMTS CS (values 400-599) =======================*/

  /*-------------------------------------------------------------------------
     GSM Circuit Switched sub-task commands
  -------------------------------------------------------------------------*/
  DS_GCSD_XID_NEG_COMPL_CMD = 400,         /* RLP Negotiation is completed */
  DS_GCSD_RX_DCD_ON_CMD = 401,             /* Received DCD ON from network */
  DS_GCSD_TX_DATA_FLUSHED_CMD = 402,       /* All TX data has been sent    */
  DS_GCSD_DISCONNECT_CMD = 403, /* Disconnect ind from GCSD protocol stack */

  /*-------------------------------------------------------------------------
    UMTS Circuit Switched Data sub-task commands
  -------------------------------------------------------------------------*/
  DS_UCSD_HANDOVER_CMD = 410,       /* ETSI Inter-RAT handover performed   */
  DS_UCSD_APP_ORIG_CALL_CMD = 411,  /* Originate a CS Data application call*/
  DS_UCSD_APP_ANSWER_CALL_CMD = 412,/* Answer a CS Data application call   */
  DS_UCSD_APP_CONFIRM_CALL_CMD = 413,/* Confirm a CS Data application call */
  DS_UCSD_APP_MODIFY_CALL_CMD = 414,   /* End a CS Data application call      */
  DS_UCSD_APP_END_CALL_CMD = 415,   /* End a CS Data application call      */
  DS_UCSD_USER_USER_DATA_IND_CMD = 416,/* User to User data arrived ind    */
  DS_UCSD_USER_USER_DATA_CONG_IND_CMD = 417, /* UUS congestion indication  */
  DS_UCSD_MMGSDI_CMD                  = 418, /* MMGSDI result process */

  /*====================== UMTS PS (values 600-799) =======================*/

  /*-------------------------------------------------------------------------
    UMTS Packet Data sub-task Commands
  -------------------------------------------------------------------------*/
  DSUMTSPS_PHYS_LINK_UP_CMD = 600,        /* bring up phys link            */
  DSUMTSPS_PHYS_LINK_DOWN_CMD = 601,      /* tear down phys link           */
  DSUMTSPS_IFACE_UP_CMD = 602,            /* bring up UMTS pkt iface       */
  DSUMTSPS_IFACE_DOWN_CMD = 603,          /* tear down UMTS pkt iface      */
  DSUMTSPS_REORIG_CMD = 604,
  DSUMTSPS_SEND_MT_REJECT_CMD = 605,      /*send a reject for a MT request */
  DSUMTSPS_PHYS_LINK_FLOW_ENABLE_CMD = 606,
  DSUMTSPS_IFACE_FLOW_ENABLE_CMD = 607,
  DSUMTSPS_PHYS_LINK_FLOW_DISABLE_CMD = 608,
  DSUMTSPS_IFACE_FLOW_DISABLE_CMD = 609,
  DSUMTSPS_IFACE_SET_TX_FUNC_CMD = 610,
#ifdef FEATURE_GSM_GPRS
  DSUMTSPS_GSNDCP_REG_CNF_CMD = 611,  /* Registration with GSNDCP complete */
  DSUMTSPS_GSNDCP_RES_CNF_CMD = 612,  /* Resumption with GSNDCP complete   */
#endif
#ifdef FEATURE_DATA_TE_MT_PDP
  DSUMTSPS_MT_TE_ANS_TIMEOUT_CMD = 613,
#endif /*FEATURE_DATA_TE_MT_PDP*/
  DSUMTSPS_SYS_CHG_SS_INFO_CMD = 614, /* system change received in UMTS    */
  DSUMTSPS_PHYS_LINK_NULL_CMD      = 615,
  DSUMTSPS_PDP_IP_IRAT_TX_UM_CMD = 616,
#ifdef FEATURE_GSM_GPRS
#endif /* FEATURE_GSM_GPRS */
  DSUMTSPS_MTPDP_DEACTIVATE_CMD = 620,
  DSUMTSPS_INITIATE_RAB_REESTAB = 621,

#if defined(FEATURE_DATA_WCDMA_PS) || defined(FEATURE_GSM_GPRS)
  DSUMTSPS_RLC_TO_PS_FLOW_DISABLE_CMD = 622,
  DSUMTSPS_RLC_TO_PS_FLOW_ENABLE_CMD = 623,
#endif
  DSUMTSPS_CM_SS_EVT_TIMER_CMD = 624,

  /*-------------------------------------------------------------------------
    UMTS RM State Machine sub-task Commands
  -------------------------------------------------------------------------*/
  DSUMTS_RMSM_RM_WANTS_PKT_CALL_CMD = 625,
  DSUMTS_RMSM_RM_BRING_UP_UM_IFACE_CMD = 626,
  DSUMTS_RMSM_RM_PPP_UP_CMD = 627,
  DSUMTS_RMSM_RM_PPP_DOWN_CMD = 628,
  DSUMTS_RMSM_UM_IFACE_UP_CMD = 629,
  DSUMTS_RMSM_UM_IFACE_PDP_PPP_UP_CMD = 630,
  DSUMTS_RMSM_UM_IFACE_DOWN_CMD = 631,
#ifdef FEATURE_DATA_TE_MT_PDP
  DSUMTS_RMSM_UM_WANTS_MT_PKT_CALL_CMD = 632,
  DSUMTS_RMSM_REJECT_MT_PKT_CALL_CMD = 633,
#endif /* FEATURE_DATA_TE_MT_PDP */
  DSUMTS_RMSM_UM_PPP_DOWN_CMD = 634,
  DS3GPP_IFACE_IN_USE_CMD = 635,
  DS3GPP_IFACE_OUTOF_USE_CMD = 636,
  DS3GPP_PHYS_LINK_UP_CMD = 637,        /* bring up phys link            */
  DS3GPP_PHYS_LINK_DOWN_CMD = 638,      /* tear down phys link           */
  DS3GPP_IFACE_UP_CMD = 639,            /* bring up UMTS pkt iface       */
  DS3GPP_IFACE_DOWN_CMD = 640,
  DS3GPP_PHYS_LINK_FLOW_ENABLE_CMD = 641,
  DS3GPP_IFACE_FLOW_ENABLE_CMD = 642,
  DS3GPP_PHYS_LINK_FLOW_DISABLE_CMD = 643,
  DS3GPP_IFACE_FLOW_DISABLE_CMD = 644,
  DS3GPP_REORIG_DORM_TMR_EXP_CMD = 645,
  DS3GPP_DORM_REORIG_CMD = 646,
  DS3GPP_IFACE_SET_TX_FUNC_CMD = 647,
  DS3GPP_PDP_IP_IRAT_TX_UM_CMD = 648,
  DS3GPP_PHYS_LINK_NULL_CMD = 649,
  DS3GPP_RESEND_PDN_CONN_REQ_CMD = 650,
  DS3GPP_FLOW_FLTR_ID_CONFLICT_CMD = 651,
  DS3GPP_FLOW_FLTR_PRECED_CONFLICT_CMD = 652,
  DS3GPP_FLOW_INVOKE_LOWER_LAYER_CMD = 653,
  DS3GPP_DSM_MEM_CTRL_EV_CMD = 654,
  DS_CM_CALL_EVENT_GET_PDN_CONN_IND = 655,
  DS_CM_CALL_EVENT_PDN_CONN_REJ_IND = 656,
  DS_CM_CALL_EVENT_PDN_CONN_FAIL_IND = 657,
  DS_CM_CALL_EVENT_BEARER_ALLOC_REJ_IND = 658,
  DS_CM_CALL_EVENT_BEARER_ALLOC_FAIL_IND = 659,
  DS_CM_CALL_EVENT_BEARER_MOD_REJ_IND = 660,
  DS_CM_CALL_EVENT_MOD_BEARER_IND = 661,
  DS3GPP_PDN_INACTIVITY_TMR_EXP_CMD = 662,
  DS3GPP_PDN_POLLING_TMR_EXP_CMD = 663,
  DS3GPP_SETUP_BRIDGE_INTERFACE_CMD = 664,

  /* Command to intiate dormancy immediately after iface moves to LINGERING */
  DSUMTSPS_IFACE_LINGER_CMD = 665,

#if  defined(FEATURE_DATA_LTE)
  DS_CM_SS_3GPP_CTXT_TRANSFER = 666,
#endif /* FEATURE_DATA_LTE && FEATURE_DS_UMTS_REARCH */

  /* Rm SM sub-task command */
  DSUMTS_RMSM_UM_IFACE_DOWN_ARB_CMD = 667,

  /* UMTS RMSM IPCP RSP TIMER CMD */
  DSUMTS_RMSM_IP_COMMON_IPCP_RSP_TIMER_CMD = 668,

  /* UMTS RMSM IPV6CP RSP TIMER CMD */
  DSUMTS_RMSM_IP_COMMON_IPV6CP_RSP_TIMER_CMD = 669,
  
/*-------------------------------------------------------------------------
    UMTS MBMS sub-task Commands
  -------------------------------------------------------------------------*/
#ifdef FEATURE_MBMS
  #error code not present
#endif /* FEATURE_MBMS */
  DSUMTS_RMSM_NO_REDIALS_REMAINING_CMD = 685,
  DSUMTS_RMSM_REDIAL_CHECK_COMPLETE_CMD = 686,
  DSUMTS_RMSM_ENTITLEMENT_CHECK_COMPLETE_CMD = 687,
  DSUMTS_RMSM_ADDR_CFG_FAILURE_CMD = 688,
  DSUMTS_RMSM_ADDR_CFG_COMPLETE_CMD = 689,
  DSUMTS_RMSM_ADDR_CFG_END_CMD = 690,
  DSUMTS_RMSM_ADDR_CFG_CHANGED_CMD = 691,
  DS_3GPP_RMSM_AUTOCONFIG_SM_CMD = 692,
  DSUMTS_RMSM_RM_PPP_CONFIGURED_CMD = 693,
  DSUMTS_RMSM_ENTITLEMENT_CHECK_FAILED_CMD = 694,
  DS3GPP_PURGE_CNF_CMD = 695,         
  DSUMTS_RMSM_DUN_CTRL_RSP = 696,
  DS_3GPP_RESET_PDN_THROTTLE = 697,
#if defined(FEATURE_STA_UMTSHDLR)
  DSUMTSPS_GET_CALL_STATE_FROM_CALL_INSTANCE_CMD = 701,
  DSUMTSPS_GET_CALL_STATE_FROM_IFACE_ID_CMD = 702,
#endif /* FEATURE_STA_UMTSHDLR */
  DS3GPP_TLB_MODE_B_HANDLER_CMD = 703,

  /* 3GPP Mode LPM CMD */
  DS_3GPP_MODE_LPM_CMD = 740,
#ifdef FEATURE_DATA_LTE
  DS_3GPP_CALL_END_MODE_CHG_LTE = 741,
#endif /* FEATURE_DATA_LTE */

  /* 3GPP RMSM AT interface commands */
  DS_3GPP_RMSM_AT_UM_IFACE_CMD = 750,
  DS_3GPP_RMSM_AT_SEND_DATA_CMD = 751,
  DS_3GPP_RMSM_AT_FLOW_EVENT_CMD = 752,

  /* Command to bring UP iface for auto answer */
  DS_3GPP_ANSWER_IFACE_BRING_UP_CMD = 753,
  DS_3GPP_SEND_MT_REJECT_CMD = 754,

#ifdef FEATURE_DATA_RPM
  /* RPM commands */
  DS_3GPP_RPM_LR3_TIMER_EXP_CMD         = 755,
  DS_3GPP_RPM_HOURLY_TIMER_EXP_CMD      = 756,
  DS_3GPP_RPM_MMGSDI_READ_CNF_CMD       = 757,
  DS_3GPP_RPM_MMGSDI_READ_FAIL_CMD      = 758,
#endif /* FEATURE_DATA_RPM */
  DS_3GPP_OPRT_MODE_SERV_DOMAIN_CMD     = 765,


#ifdef FEATURE_DATA_EMBMS
  DS_3GPP_EMBMS_IFACE_BRING_UP_CMD      = 766,
  DS_3GPP_EMBMS_SEND_TMGI_ACTIVATED_IND_CMD = 767,
  DS_3GPP_EMBMS_SEND_TMGI_DEACTIVATED_IND_CMD = 768,
#endif /* FEATURE_DATA_EMBMS */

 DS_3GPP_CLEAR_INFINITE_THROTTLING_CMD  = 769, /* request to clear Infinite Throttling */

 DS_3GPP_TEARDOWN_ON_INVALID_PCSCF = 770,  /* Teardown the Iface because the PCSCF    
                                             address is invalid. */
#ifdef FEATURE_DATA_TE_MT_PDP
  DSUMTS_RMSM_UM_LOCKED_CMD             = 771,
#endif /* FEATURE_DATA_TE_MT_PDP */
  DS_3GPP_PH_EVENT_OPRT_MODE_CHG_CMD    = 772,

  DS_3GPP_PDN_THROT_UNBLOCK_APN_TIMER_EXP_CMD = 773,

  DS_3GPP_PDN_THROT_TIMESTAMP_APN_THROT_TMR_CMD = 774,

  DS_3GPP_PDN_THROT_IMSI_INFO_AVAIL_CMD = 775,

  DS_3GPP_PDN_THROT_UNBLOCK_ALL_APN_CMD = 776,

  DS_3GPP_PDN_THROT_SAMPLING_TIMER_EXP_CMD = 777,

#if defined (FEATURE_DATA_A2_DL_BRIDGE) && defined (FEATURE_DATA_LTE)
  DS_3GPP_SWITCH_DATA_PATH_TO_SW_CMD = 778, /* Teardown and disable A2 bridging*/
  DS_3GPP_SWITCH_DATA_PATH_TO_HW_CMD = 779, /* Setup and enable A2 bridging */
#endif /* FEATURE_DATA_A2_DL_BRIDGE */

  DS_3GPP_RRC_PCH_STATE_CMD             = 780, /* RRC PCH STATE indication */

  DS3GPP_EMERGENCY_IFACE_UP_CMD         = 781,

#if defined (FEATURE_DATA_A2_DL_BRIDGE) && defined (FEATURE_DATA_LTE)
  DS3GPP_SIO_V4_GONE_EMPTY_CMD = 782,
  DS3GPP_SIO_V6_GONE_EMPTY_CMD = 783,
  DS3GPP_SIO_V4V6_GONE_EMPTY_CMD = 784,
  DS3GPP_UM_WM_GONE_EMPTY_CMD = 785,
  DS_3GPP_SWITCH_IFACE_DATA_PATH_TO_HW_CMD = 786,
  DS_3GPP_SWITCH_IFACE_DATA_PATH_TO_SW_CMD = 787,
#endif 
  DS_3GPP_EFS_READ_AFTER_INIT_CMD       = 788, /* Read EFS items soon after
                                                  device Init
                                                */
  DS_3GPP_PDN_THROT_CLEAR_APN_REJECT_T3402_TMR_CMD = 789,
  DS3GPP_RAB_COUNTER_FLIP_BIT_CMD  = 790,

  DS_3GPP_PDN_LIMIT_PDN_CONN_TMR_EXP_CMD = 791,
  DS_3GPP_PDN_LIMIT_PDN_REQ_THROT_TMR_EXP_CMD = 792,
  DS_3GPP_PDN_LIMIT_PDN_WAIT_TMR_EXP_CMD = 793,

  DS_3GPP_SYS_SEL_PREF_ERR_CMD = 794, /* Pref Sys Sel ERR */
  DS_3GPP_SYS_SEL_PREF_NO_ERR_CMD = 795,/* Pref Sys Sel NO_ERR */
  DS_3GPP_SYS_SEL_RETRY_TIMER_EXPIRY_CMD = 796,
 
  DS_3GPP_CONFIG_TD_ULRATE_CMD = 797,
  DS_3GPP_APN_PARAM_CHANGE_CMD = 798,
  /*====================== CDMA PS (values 800-1099) ======================*/

  /*-------------------------------------------------------------------------
    IS707 Packet Data sub-task Commands
  -------------------------------------------------------------------------*/
  DS_707_PKT_EPZID_CHANGE_CMD = 800,            /* EPZID has changed         */
  DS_707_PKT_EPZID_SOCM_CMD = 801,              /* EPZID has new SOCM        */

  DS_707_PKT_CTA_TIMER_EXPIRED_CMD = 802,   /* AT+CTA (idle timeout) timer */
  DS_707_PKT_HOLDDOWN_TIMER_EXPIRED_CMD = 803, /* holddown timer expired   */
  DS_707_PKT_EPZID_DELAY_TIMER_EXPIRED_CMD = 804,
                                               /* pzid delay timer expired */
  DS_707_PKT_EPZID_HYSTERESIS_TIMER_EXPIRED_CMD = 805,
                                          /* pzid hysteresis timer expired */
  DS_707_PKT_EPZID_HYS_DATA_READY_CMD = 806, /* to notify epzid hyst engine  */
  DS_707_PKT_EPZID_HYS_SDB_DATA_CMD = 807,  /* about packet data exchange   */

  DS_707_PKT_PHYS_LINK_UP_CMD = 808,      /* bring up phys link            */
  DS_707_PKT_PHYS_LINK_DOWN_CMD = 809,    /* tear down phys link           */

  DS_707_PKT_IDM_CHANGE_CMD = 810,        /* current network(1X/HDR) change*/

  DS_707_TOGGLE_QNC_ENABLE_CMD = 811,     /* toggle QNC setting - UI       */
  DS_707_INITIATE_ALN_CMD = 812,          /* Initiate PPP resync due to ALN*/
  DS_707_ABORT_DORMANT_PPP_TIMER_EXPIRED_CMD = 813,
                                          /*close dormant PPP timer expired*/
#ifdef FEATURE_DATA_IS707
  DS_707_EXIT_TC_CMD = 814, /* cmd when CDMA traffic channel torn down */
  DS_707_UM_TX_HI_WM_CMD = 815, /* cmd to process tx high watermark */
  DS_707_UM_TX_LO_WM_CMD = 816, /* cmd to process tx low watermark */
  DS_707_PH_SYS_SEL_PREF_NORMAL_CMD = 817,/*Notif'n of ph_sys_sel_pref change*/
  DS_707_RETRY_DELAY_ORIG_CMD = 818,           /* recvd retry delay from MC */
  DS_707_HDR_LN_STATUS_CMD = 819,         /* Command to update LN status */
  DS_707_PKT_SYS_CHG_CMD = 820,     /* to signify change is serving system */
  DS_707_UM_RX_HI_WM_CMD = 821, /* cmd to process rx high watermark */
  DS_707_UM_RX_LO_WM_CMD = 822, /* cmd to process rx low watermark */
#ifdef FEATURE_DEDICATED_C2K_MODE_DATA
  DS_707_PKT_AUTO_REDIAL_TIMER_EXPIRED_CMD= 823, /*Auto Redial timer expired*/
#endif
#ifdef FEATURE_IS707B_ADDENDUM
  DS_707_PKT_MGR_SET_HAT_TIMER_CMD = 824, /* Command to SET Hysteresis Activation Timer */
#endif /* FEATURE_IS707B_ADDENDUM */
  DS_707_PH_MODE_PREF_CHG_CMD = 825, /* Command TO notify PH Event from CM */
  DS_707_PKT_DELAY_CONNECTION_CLOSE_TIMER = 826, /* connection release
                                                          delay timer */
  DS_707_PH_EVENT_OPRT_MODE_CMD = 827, /* cmd to process operation mode change */
  DS_707_CM_NOTIFY_CMD = 828, /* Cmd to process notification from CM */
  DS_707_NEW_CALL_ATTEMPT_TIMER_CMD = 829,/*new call attempt timer expiration */

#ifdef FEATURE_EHRPD
  DS_707_PDN_INACTIVITY_TIMER_CMD = 830,/*PDN Inactivity timer expiration */
#endif /* FEATURE_EHRPD */
  DS_RLP_INIT_CMD = 831,
  DS_707_PARSE_DORM_TIMER_SOCM_CMD = 832, /*DORM_TIMER has new SOCM*/

  /*-------------------------------------------------------------------------
    Cmds used for 3GPP2 iface bringup and teardown
  -------------------------------------------------------------------------*/
  DS_707_RMSM_BRING_UP_CMD = 833,
  DS_707_SOCKETS_BRING_UP_CMD = 834, 
  DS_707_RMSM_TEAR_DOWN_CMD = 835,
  DS_707_SOCKETS_TEAR_DOWN_CMD = 836,
  DS_707_THROTTLE_INFO_UPDATE_CMD = 837,
#endif /* FEATURE_DATA_IS707 */

  /*-------------------------------------------------------------------------
    New cmds introduced for CAM module.
  -------------------------------------------------------------------------*/
  DS_707_COLOC_INFO_CHANGED_CMD = 840,
  DS_707_GET_COLOC_INFO_CMD = 841,

  /*-------------------------------------------------------------------------
    IS707 Packet Data Rm Iface Flow Control Specific commands
  -------------------------------------------------------------------------*/
  DS_707_RM_IFACE_FLOW_ENABLED_CMD = 842,
  DS_707_RM_IFACE_FLOW_DISABLED_CMD = 843,

#ifdef FEATURE_EHRPD
#ifdef FEATURE_DATA_EHRPD_DUAL_IP
  DS707_IFACE_IN_USE_CMD = 850,
  DS707_IFACE_OUT_USE_CMD = 851,
#endif /* FEATURE_DATA_EHRPD_DUAL_IP */
#endif /* FEATURE_EHRPD */

  /*-------------------------------------------------------------------------
    IS707 Packet Data Rm State Machine sub-task Commands
  -------------------------------------------------------------------------*/
  DS_707_RMSM_RM_WANTS_PKT_CALL_CMD = 860,
  DS_707_RMSM_RM_IFACE_DOWN_CMD = 861,
  DS_707_RMSM_UM_IFACE_DOWN_CMD = 862,
  DS_707_RMSM_UM_PHYS_LINK_UP_CMD = 863,
  DS_707_RMSM_UM_PHYS_LINK_DOWN_CMD = 864,
  DS_707_RMSM_UM_PPP_DOWN_CMD = 865,
  DS_707_RMSM_RM_PPP_UP_CMD = 866,
  DS_707_RMSM_UM_IFACE_UP_CMD = 867,
  DS_707_RMSM_UM_MIP_IP_ADDR_CHANGED_CMD = 868,
  DS_707_RMSM_UM_PHYS_LINK_GONE_CMD = 869,
  DS_707_RMSM_E2E_PPP_UP_CMD = 870,
  DS_707_RMSM_UM_RESYNCING_CMD = 871,
  DS_707_RMSM_RM_RESYNCING_CMD = 872,
  DS_707_RMSM_ENTITLEMENT_CHECK_COMPLETE_CMD = 873,

  /*-------------------------------------------------------------------------
    IS707 Async Data sub-task Commands
  -------------------------------------------------------------------------*/
  DS_707_ASYNC_IFACE_BRING_UP_CMD = 880,        /* bring up 1x async iface */
  DS_707_ASYNC_IFACE_TEAR_DOWN_CMD = 881,       /* tear down 1x async iface*/
  DS_707_ASYNC_PHYS_LINK_TEAR_DOWN_CMD = 882,
                                       /* tear down physlink 1x async iface*/
  DS_707_ASYNC_PTCL_OPENING_TIMER_EXPIRED_CMD = 883,
                                                /* Open protocols timer    */
  DS_707_ASYNC_PTCL_OPENED_CMD = 884,        /* PS informs protocols opened*/
  DS_707_ASYNC_PTCL_CLOSED_CMD = 885,        /* PS informs protocols closed*/
  DS_707_ASYNC_ATZ_CMD = 886,                /* ATZ reflected cmd          */
#ifdef FEATURE_EHRPD
  DS_707_MANDATORY_APN_CHECK_CMD = 887,     /*command to request for mandatory list check*/
#endif
  /*-------------------------------------------------------------------------
    HDR Commands
  -------------------------------------------------------------------------*/
  DS_HDR_EVENT_CMD = 890,


  /*--------------------------------------------------------------------------
    DS HDR Commands
  -------------------------------------------------------------------------*/
  DS_HDR_CDMA_SUBS_AVAIL_EVENT_CMD = 895,
  DS_HDR_AN_PPP_EV_CMD = 896,
  DS_HDR_AN_PPP_START_CMD = 897,

  /*-------------------------------------------------------------------------
    DSSNET4 commands
  -------------------------------------------------------------------------*/
#ifdef FEATURE_DATA_IS707
  DS_DSSNET4_PHY_LINK_UP_EV_CMD = 900,
  DS_DSSNET4_PHY_LINK_GONE_EV_CMD = 901,
  DS_DSSNET4_PHY_LINK_DOWN_EV_CMD = 902,
  DS_DSSNET4_LINK_UP_WITH_SIP_EV_CMD = 903,
  DS_DSSNET4_LINK_DOWN_EV_CMD = 904,
  DS_DSSNET4_LINK_RESYNC_EV_CMD = 905,
#ifdef FEATURE_DS_MOBILE_IP
  DS_DSSNET4_LINK_UP_WITH_MIP_EV_CMD = 906,
  DS_DSSNET4_MIP_UP_SUCCESS_EV_CMD = 907,
  DS_DSSNET4_MIP_FAILURE_EV_CMD = 908,
  DS_DSSNET4_MIP_DEREGED_EV_CMD = 909,
#endif /* FEATURE_DS_MOBILE_IP */
  DS_DSSNET4_PPP_CLOSE_EV_CMD = 910,
  DS_DSSNET4_FLOW_DELETED_EV_CMD = 911,
  DS_DSSNET4_DELAY_CONNECTION_CLOSE_TIMER  = 912,      /* Connection delay timer     */
#endif /* FEATURE_DATA_IS707 */

#ifdef FEATURE_DATA_PS_IPV6
  DS_DSSNET6_PHY_LINK_UP_EV_CMD = 913,
  DS_DSSNET6_PHY_LINK_GONE_EV_CMD = 914,
  DS_DSSNET6_PHY_LINK_DOWN_EV_CMD = 915,
  DS_DSSNET6_LINK_UP_EV_CMD = 916,
  DS_DSSNET6_LINK_DOWN_EV_CMD = 917,
  DS_DSSNET6_LINK_RESYNC_EV_CMD = 918,
  DS_DSSNET6_NET_UP_EV_CMD = 919,
  DS_DSSNET6_NET_DOWN_EV_CMD = 920,
  DS_DSSNET6_PPP_CLOSE_EV_CMD = 921,
  DS_DSSNET6_FLOW_DELETED_EV_CMD = 922,
#endif /* FEATURE_DATA_PS_IPV6 */
  DS_DSSNET4_AUTH_STARTED_EV_CMD = 923,

  /*-------------------------------------------------------------------------
    DS707 ANY iface related Commands
  -------------------------------------------------------------------------*/
  DS_DSSNETANY_IFACE_DOWN_EV_CMD = 930,
  DS_DSSNETANY_IFACE_UP_EV_CMD = 931,
  DS_DSSNETANY_CONFIGURING_EV_CMD = 932,
  DS_DSSNETANY_OPEN_EV_CMD = 933,
  DS_DSSNETANY_TEARDOWN_EV_CMD = 934,
  DS_DSSNETANY_CLOSE_EV_CMD = 935,


  /*-------------------------------------------------------------------------
    BCMCS Commands
  -------------------------------------------------------------------------*/

  DSBC_BCAST_SRVC_AVAILABLE_CMD = 940,/* bcast availabile ind rcvd from CM */
  DSBC_BCAST_SRVC_LOST_CMD = 941,     /* bcast lost ind rcvd from CM       */
  DSBC_FLOW_STATUS_CHANGED_CMD = 942, /* bcmcs flow status rcvd from CM    */
  DSBC_BCAST_RESET_CMD = 943,         /* bcast reset over-the-air or internal -
                                      ind that bcmcs db needs to be updated */
  DSBC_DBUPDATE_COMPLETED_CMD = 944,  /* BCMCS DB updated followed by RESET*/

  DSBC_1XHDR_REGISTER_FLOW_CMD = 945, /* BCMCS_JOIN ioctl on bcast iface   */
  DSBC_1XHDR_UNREGISTER_FLOW_CMD = 946,/* BCMCS_LEAVE ioctl on bcast iface */
  DSBC_1XHDR_IFACE_UP_CMD = 947,       /* request to bring up Bcast iface  */

  DSBCRM_1XHDR_RM_IFACE_DOWN_CMD = 948, /* Rm iface UP/ROUTEABLE cmd to BC-RmSm  */
  DSBCRM_1XHDR_RM_IFACE_UP_CMD = 949,   /* Rm iface DOWN cmd to BC-RmSm          */
#ifdef FEATURE_HDR_BCMCS_2p0
  #error code not present
#endif /* FEATURE_HDR_BCMCS_2p0 */

  /*-------------------------------------------------------------------------
    QOS Commands
  -------------------------------------------------------------------------*/

  DS_707_RESV_LABEL_BOUND_TO_RLP_CMD              = 960,
  DS_707_QOS_FLOW_MGR_QOS_GRANTED_NOTIFY_CMD      = 961,
  DS_707_QOS_FLOW_MGR_RESV_ON_NOTIFY_CMD          = 962,
  DS_707_QOS_FLOW_MGR_RESV_OFF_NOTIFY_CMD         = 963,
  DS_707_QOS_FLOW_MGR_RESV_ON_SENT_NOTIFY_CMD     = 964,
  DS_707_QOS_FLOW_MGR_CONSOLIDATED_CMD            = 965,

  DS_707_SEC_PKT_FLOW_CONFIGURE_CMD               = 966,
  DS_707_SEC_PKT_FLOW_ACTIVATE_CMD                = 967,
  DS_707_SEC_PKT_FLOW_RESUME_CMD                  = 968,
  DS_707_SEC_PKT_FLOW_SUSPEND_CMD                 = 969,
  DS_707_SEC_PKT_FLOW_GO_NULL_CMD                 = 970,
  DS_707_SEC_PKT_FLOW_MODIFY_CMD                  = 971,
  DS_707_SEC_PKT_SESSION_STATE_CHANGE_NOTIFY_CMD  = 972,
  DS_DQOS_MGR_PROC_RSVP_MSG_CMD                   = 973,      /* Process RSVP msg */
  DS_DQOS_MGR_REXMIT_TIMER_CMD                    = 974,      /* Rexmit timer is expired */
  DS_707_SEC_NETWORK_INITIATED_PKT_PPP_DOWN_CMD   = 975,      /* PPP is released by network */
  DS_707_SEC_PKT_GRANT_TIMER_EXPIRE_CMD           = 976,      /* QoS Grant timer expire     */
  DS_707_SEC_PKT_MGR_IFACE_CMD                    = 977,

  /*-------------------------------------------------------------------------
   Network initiated QOS command.
  -------------------------------------------------------------------------*/
  #ifdef FEATURE_NIQ_EHRPD
  DS707_NW_INIT_QOS_PPP_RESYNC_TIMER_EXP_CMD     =  980,
  DS707_NW_INIT_QOS_INACTIVITY_TIMER_EXP_CMD     =  981,
  #endif

  /*-------------------------------------------------------------------------
    SRLTE Enabled . 
  -------------------------------------------------------------------------*/
  DS_CAM_SRLTE_ENABLED_CMD                         = 982,

  /*-------------------------------------------------------------------------
   Following cmds added for CAM module that runs in DS context. Receives
   cmds from DS & CM tasks.  The purpose of this module is to force dormancy
   on 1X to move to EVDO if available, if the call ahs been UP on 1X for a
   while, by reducing  RLP IDLE timeout to 1 sec afetr 5 mnts into the call
  -------------------------------------------------------------------------*/
  DS_CAM_SO33_CALL_CONNECTED_CMD = 990,
  DS_CAM_TIMER_EXPIRED_CMD = 991,
  DS_CAM_CALL_GONE_NULL_CMD = 992,
  DS_CAM_CALL_DORMANT_CMD = 993,
  DS_CAM_EVDO_COLOCATED_CMD = 994,
  DS_CAM_NO_EVDO_COLOCATED_CMD = 995,
  DS_CAM_FLOW_CTRL_TIMER_EXPIRED_CMD = 996,
  DS_CAM_NON_SO33_CALL_CONNECTED_CMD = 997,

  /*-------------------------------------------------------------------------
   Following cmds added for RDUD module that runs in DS context.
   The purpose of this module is to enforce quick idle_timeout dormancy
   if TC came up due to an incoming call, to avoid keepign TC alive
   unncessarily long.
  -------------------------------------------------------------------------*/
  DS_RDUD_INC_CALL_ACCEPTED_CMD=998,
  DS_RDUD_PHYS_LINK_DOWN_CMD=999,
  DS_RDUD_TIMER_EXPIRED_CMD=1000,

   /* --------------------------------------------------------------------------
     Following DS CMD are added for Feature that runs in DS context.
     Also please refrain from using next 200 index from 1000 to 1200 as the
     code would be cleaner since most of it will be used for DS 3GPP
  ---------------------------------------------------------------------------*/
  DS_3GPP_REMOVE_UNUSED_PDN_CMD = 1001,
  DS_3GPP_THROT_TMR_EXP_CMD = 1002,
  DS_3GPP_ROAMING_DISALLOWED_FLAG_CHANGED_CMD = 1003,
  DS_3GPP_ROAMING_STATUS_CHG_CMD = 1004,
  DS_3GPP_APN_DISABLED_CMD = 1005,
  DS_3GPP_MMGSDI_UICC_3G_RESET_CMD = 1006,
#ifdef FEATURE_DATA_LTE
  DS_3GPP_PLMN_BLOCK_TMR_EXP_CMD = 1007,
  DS_3GPP_EPS_THROT_TMR_EXP_CMD = 1008,
  DS_3GPP_EPS_THROT_T3402_TMR_EXP_CMD = 1009,
#endif /*FEATURE_DATA_LTE*/
  DS_3GPP_RAU_COMPLTE_CMD = 1010,

  DS_3GPP_RESERVED = 1200,
  /*Reserved for 3GPP  */

  DS_RSVP_SOCKET_EVENT_CMD = 1300,

  /*-------------------------------------------------------------------------
    BCMCS FLOW cmds
  -------------------------------------------------------------------------*/
#if defined(FEATURE_MFLO) && defined(FEATURE_MFLO_FLOMCS)
  DSBC_FLO_BC_SRVC_AVAILABLE_CMD = 1320,      /* bcast availability ind rcvd from FLO stack */
  DSBC_FLO_BC_SRVC_LOST_CMD = 1321,           /* bcast lost ind rcvd from FLO stack         */
  DSBC_FLO_BC_FLOW_STATUS_CHANGED_CMD = 1322, /* bcmcs flow status rcvd from FLO stack      */
  DSBC_FLO_BC_REGISTER_FLOW_CMD = 1323,       /* MCAST_JOIN ioctl on FLO bcast iface        */
  DSBC_FLO_BC_UNREGISTER_FLOW_CMD = 1324,     /* MCAST_LEAVE ioctl on FLO bcast iface       */
  DSBC_FLO_BC_IFACE_UP_CMD = 1325,            /* request to bring up FLO bcast iface        */
  DSBC_FLO_BC_IFACE_DOWN_CMD = 1326,          /* request to bring down FLO bcast iface      */
#endif /* defined(FEATURE_MFLO) && defined(FEATURE_MFLO_FLOMCS) */

#ifdef FEATURE_MMGSDI_SESSION_LIB
  DS_MMGSDI_SIM_INFO_CMD = 1500,              /* MMGSDI SIM event cmds  */
#endif /* FEATURE_MMGSDI_SESSION_LIB */
  DS_AT_PDP_CMD = 1600,                   /* AT PDP cmd */
#ifdef FEATURE_DATA_TEST_LOOPBACK
  DS_LOOPBACK_HDLR_CMD             = 1657,  /* All events are demultiplexed
                                               by loopback cmd hdlr */
#endif /* FEATURE_DATA_TEST_LOOPBACK */

#ifdef FEATURE_ECALL_APP
  DS_AT_ECALL_CMD        = 1658,
#endif /* FEATURE_ECALL_APP */

/*-------------------------------------------------------------------------
  EPC HANDOFF cmds 
-------------------------------------------------------------------------*/

#ifdef FEATURE_EPC_HANDOFF
  //DS_EPC_HDLR_HANDOFF_INIT_CMD = 1659,
  //DS_EPC_HDLR_CONTEXT_RELEASE_CMD = 1660,
  //DS_EPC_HDLR_SRAT_CLEANUP_RELEASE_CMD      = 1661,  
  //DS_EPC_HDLR_IFACE_EVENT_PROCESS_CMD      = 1662,
  DS_EPC_LTE_ATTACH_INIT_PROC_CMD = 1659,
  DS_EPC_SS_BEARER_TECH_CHG_PROC_CMD = 1660,
  DS_EPC_eHRPD_S101_STATUS_CHG_PROC_CMD = 1661,
  DS_EPC_IFACE_EV_PROC_CMD = 1662,
  DS_EPC_IFACE_TEARDOWN_CMD = 1663,
  DS_EPC_THRTL_PDN_LIST_PROC_CMD = 1664,
  DS_EPC_eHRPD_PREREG_COMPLETE_RSP_CMD = 1665,
  DS_EPC_DATA_CTX_TIMER_EXP_PROC_CMD = 1666,
  DS_EPC_SRAT_TIMER_EXP_PROC_CMD = 1667,
  DS_EPC_RETRY_TIMER_EXP_PROC_CMD = 1668,
  DS_EPC_PREF_SYS_CHG_CMD = 1669,
  DS_EPC_REPOST_SYS_CMD = 1670,
  DS_EPC_CB_INIT_CMD = 1671,
#ifdef FEATURE_DATA_WLAN_MAPCON
  DS_EPC_HO_GIVEUP_TIMER_EXP_PROC_CMD = 1672,
  DS_EPC_HO_THROTTLE_TIMER_EXP_PROC_CMD = 1673,
#endif /*FEATURE_DATA_WLAN_MAPCON*/
#endif /*FEATURE_EPC_HANDOFF*/

  /*-------------------------------------------------------------------------
    DSD  cmds 
  -------------------------------------------------------------------------*/
  DS_DSD_TIMER_EXPIRED_CMD              = 1680,
  DS_DSD_SYS_CHG_CMD                    = 1681,
  DS_DSD_SYS_RECOMPUTE_CMD              = 1682,
  DS_DSD_EXT_NOTIFY_CMD                 = 1683,
  DS_DSD_MODE_PREF_CHG_CMD              = 1684,
  DS_DSD_RESET_CMD                      = 1685,
  DS_DSD_APM_APN_CHG_CMD                = 1686,

  DS_DSD_SS_EVENT_PROC_DONE_CMD         = 1687,
  DS_DSD_APM_REFRESH_ATTACH_PDN_LIST    = 1688,
  DS_DSD_APM_SET_APN_SWICH_STATUS_CMD   = 1689,

  /*-------------------------------------------------------------------------
    DS3GDSDIF cmds
  -------------------------------------------------------------------------*/
  DS3GDSDIF_WLAN_SYS_CHG_CMD            = 1690,
  DS3GDSDIF_VOICE_CALL_EVENT_CMD        = 1691,
  DS3GDSDIF_MT_CSFB_HYST_TIMER_EXP_CMD  = 1692,

  /*-------------------------------------------------------------------------
    DSC cmds
  -------------------------------------------------------------------------*/
  DS_DSC_LTE_ENABLE_CMD                 = 1695,
  DS_DSC_LTE_DISABLE_CMD                = 1696,

  /*-------------------------------------------------------------------------
    DSD APM cmds
  -------------------------------------------------------------------------*/
  DS_DSD_APM_APN_DISABLE_CMD            = 1697,

  /*-------------------------------------------------------------------------
    Command for DS SIO LIB
  -------------------------------------------------------------------------*/
  DS_SIO_EVENT_CMD       = 1700,

  DS_STATS_INIT_CMD = 1701,          /* DS STATISTICS Init command */

  DS_CM_STANDBY_INFO_CMD      = 1702,      /* Dual Standby event             */
  DS_CM_STANDBY_SUB_INFO_CMD  = 1703,  /* Dual Standby subscription info */
  DSUMTSPS_STOP_DATA_AVAIL_IND_CMD = 1704, /* Stop sending data avail ind to CM */

#ifdef FEATURE_EHRPD
#ifdef FEATURE_DATA_3GPP2_VS
  DS_707_MPIT_TIMEOUT_CMD = 1720, /* Max PPP Inactivity timer expiration */ 
#endif /* FEATURE_DATA_3GPP2_VS */

  /*-------------------------------------------------------------------------
    Device manager commands
  -------------------------------------------------------------------------*/
  DS707_DEVMGR_PCMT_TIMEOUT_CMD      = 1721,       /* PCMT timeout */
  DS707_DEVMGR_PPP_EV_CMD            = 1722,       /* LCP/AUTH event */

#ifdef FEATURE_DATA_3GPP2_ALWAYS_ON
  DS_707_LSD_TIMEOUT_CMD = 1723, /* Link Status Determination (LSD) expiry */
#endif /* FEATURE_DATA_3GPP2_ALWAYS_ON */

  DS707_PS_EVT_PROC_CMD = 1726,                  /* To process all PS events */
#ifdef FEATURE_EHRPD_HRPD_FALLBACK
  DS_707_ROAM_CHG_CMD = 1750,     /* to signify change is roaming status */
#endif /* FEATURE_EHRPD_HRPD_FALLBACK */
  DS_707_POST_GRACEFUL_PPP_TERM_CMD = 1751, /*Terminate all active PDNs and LCP*/
#endif /* FEATURE_EHRPD */
  
#ifdef FEATURE_DATA_OPTHO
  DS707_S101_COMPLETE_PREREG_HANDOFF_CMD = 1760,   /* Prereg Handoff handling*/
#endif /* FEATURE_DATA_OPTHO */
  DS_CM_CALL_EVENT_ORIG = 1761,                    /* CM call orig event */
  DS_707_WAIT_FOR_EMERG_CALL_TIMEOUT_CMD = 1762,   /* wait_for_emerg_call timeout */

#ifdef FEATURE_DATA_WLAN_MAPCON
  /*-------------------------------------------------------------------------
   DS WLAN PROXY commands
  -------------------------------------------------------------------------*/
  DS_WLAN_PROXY_IFACE_BRING_UP_CMD = 1763,
  DS_WLAN_PROXY_IFACE_TEAR_DOWN_CMD = 1764,
  DS_WLAN_PROXY_PROC_DSD_IND_CMD = 1765,
  DS_WLAN_PROXY_REV_IP_XPORT_SIO_CONFIG_COMPLETE_CMD = 1766,
  DS_WLAN_PROXY_REV_IP_XPORT_WDS_CONFIG_COMPLETE_CMD = 1767,
  DS_WLAN_PROXY_PROC_BRINGUP_RETRY_TIMER_EXPIRY_CMD  = 1768,
  DS_WLAN_PROXY_RM_DOWN_CMD                          = 1769,
  DS_WLAN_PROXY_WLAN_AVAILABLE_CMD                   = 1770,
#endif /*FEATURE_DATA_WLAN_MAPCON*/

#ifdef FEATURE_DATA_IWLAN_S2B
 /*-------------------------------------------------------------------------
  DS IWLAN S2B commands
  -------------------------------------------------------------------------*/
  DS_IWLAN_S2B_PDN_BRING_UP_CMD = 1780,
  DS_IWLAN_S2B_PDN_TEARDOWN_CMD = 1781,
  DS_IWLAN_S2B_REV_RM_CONFIG_COMPLETE_CMD = 1782,  
  DS_IWLAN_S2B_RM_DOWN_CMD = 1783,
  DS_IWLAN_S2B_WP_IFACE_UP_CMD = 1784,
  DS_IWLAN_S2B_WP_IFACE_DOWN_CMD = 1785,
  DS_IWLAN_S2B_WP_IFACE_ADDR_CHANGED_CMD = 1786,
  DS_IWLAN_S2B_REV_IP_XPORT_UP_CMD = 1787, 
  DS_IWLAN_S2B_IKEV2_EPDG_ADDR_CHANGED_CMD = 1788,
  DS_IWLAN_S2B_EPDG_ADDR_RESLVR_DNS_CALLBACK_CMD = 1789,
  DS_IWLAN_S2B_IKEV2_INITIATE_ESP_REKEY_CMD = 1790,
  DS_IWLAN_S2B_IKEV2_PROCESS_IKEV2_CB = 1791,
  DS_IWLAN_S2B_CM_CARD_REFRESH_EVENT_CMD = 1792,
  DS_IWLAN_S2B_EPDG_ADDR_RESLVR_IFACE_UP_CMD = 1793,
  DS_IWLAN_S2B_EPDG_ADDR_RESLVR_IFACE_DOWN_CMD = 1794,
  DS_IWLAN_S2B_EPDG_ADDR_RESLVR_TIMER_CALLBACK_CMD = 1795,
  DS_IWLAN_S2B_EPDG_ADDR_RESLVR_DSD_EVENT_CMD = 1796,
  DS_IWLAN_S2B_IFACE_IPSEC_IPFLTR_CHANGED_CMD = 1797,
  DS_IWLAN_S2B_WP_BEARER_TECH_CHANGED_CMD     = 1798,
  DS_IWLAN_S2B_EPDG_ADDR_RESLVR_TTL_TIMER_CALLBACK_CMD = 1799,
  /*-------------------------------------------------------------------------
    NOTE: Since from 1800 are already taken, temporarily using command
   id 2000. After ds_command re-architecture, proper id range will beP
   assigned to IWLAN and all the ids will be continuous.
  -------------------------------------------------------------------------*/
  DS_IWLAN_S2B_WP_ADDR_CHANGED_CMD  = 2000,
#endif /* FEATURE_DATA_IWLAN_S2B */

#ifdef FEATURE_DATA_WLAN_MAPCON 
  /*-------------------------------------------------------------------------
    MPPM commands
  -------------------------------------------------------------------------*/
  DS_MPPM_DSD_EVENT_CMD = 1800, /*DSD event processing command*/
  DS_MPPM_FLUSH_EXT_EVENT_Q_CMD = 1801,/*CMD to flush external events queue*/
  DS_MPPM_WLAN_LB_PREF_CHANGE_CMD = 1802,/*Handle WLAN LB preference change*/
  DS_MPPM_APN_PREF_SYS_CHANGE_CMD = 1803,/*Handle APN system preference change*/
  DS_MPPM_PDN_THROTTLE_INFO_CMD   = 1804,/*Handle PDN throttle info cmd*/
  DS_MPPM_DSDIF_LTE_ATTACH_INIT_STATUS_CMD = 1805,/*Handle LTE init status cmd*/
  DS_MPPM_WLAN_MAX_CONN_INFO_CMD = 1806,/*Handle WLAN MAX_CONN info cmd*/
  DS_MPPM_PDN_DB_UPDATE_CMD = 1807,/*Handle MPPM PDN DB updates cmd*/
  DS_MPPM_EPC_TIMER_CB_CMD  = 1808,/* EPC RAT timer callback cmd*/
  DS_MPPM_MMGSDI_SUBSCRIPTION_EVENT_CMD  = 1809,/* MMGSDI SUBS process cmd*/
  DS_MPPM_T_MINONWWAN_TIMER_CB_CMD = 1810, /*T_MINONWWAN timer hander cmd*/
  DS_MPPM_PDN_STATUS_INFO_CMD = 1850, /*PDN STATUS INFO hander cmd*/
  DS_MPPM_CM_CARD_REFRESH_EVENT_CMD = 1851, /* Command posted from DS3G when
                                               it receives card refresh event 
                                               from CM */
  /*-------------------------------------------------------------------------
    NOTE: Since 1811, 1812,1813 are already taken, temporarily using command
   id 1850. After ds_command re-architecture, proper id range will beP
   assigned to MPPM and all the ids will be continuous.
  -------------------------------------------------------------------------*/

 /*-------------------------------------------------------------------------
  DS EPS KAMGR Command
  -------------------------------------------------------------------------*/
  DS_EPS_KAMGR_SYS_INFO_CMD                           = 1820,
  DS_EPS_KAMGR_IFACE_DOWN_EV_CBACK_CMD                = 1821,
  DS_EPS_KAMGR_IFACE_UP_EV_CBACK_CMD                  = 1822,
  DS_EPS_KAMGR_TIMER_EXPIRY_CBACK_CMD                 = 1823,
  DS_EPS_KAMGR_APN_NAME_CHG_CMD                       = 1824,
  DS_EPS_KAMGR_ALL_IFACE_UP_EV_CBACK_CMD              = 1825,
  DS_EPS_KAMGR_TEAR_DOWN_TIMER_EXPIRY_CBACK_CMD       = 1826,
#endif /* FEATURE_DATA_WLAN_MAPCON */

#ifdef TEST_FRAMEWORK
  #error code not present
#endif /*TEST_FRAMEWORK*/
  DS_CM_MODEPREF_INFO_CMD = 1811,
  DS_3GPP_CLR_PENDING_CALLS_CMD = 1812,

#ifdef FEATURE_DATA_ATP
  DS_ATP_EXTERNAL_MSG = 1813,
#endif
  /*-------------------------------------------------------------------------
    DSMGR commands (1900 - 2000)
  -------------------------------------------------------------------------*/
  DS_DSMGR_INIT_COMPLETE_CMD = 1900,
  DS_CM_PRIORITY_INFO_CMD = 1901,
  DS_FLOW_MGR_TIMER_EXPIRED_CMD = 1902,
  DS_DSMGR_LOW_LATENCY_CMD=1903,

  DS_MAX_CMDS
} ds_cmd_enum_type;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Command Header type
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
typedef struct
{
  q_link_type       link;                               /* Queue link type */
  ds_cmd_enum_type  cmd_id;                      /* Identifies the command */
#ifdef FEATURE_DATA_DS_CMD_TIMESTAMP_DEBUG
  time_type         timestamp;             /* Timestamp of the command
                                           measured in ms after phone is powered on*/
#endif /*FEATURE_DATA_DS_CMD_TIMESTAMP_DEBUG*/
} ds_cmd_hdr_type;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Command Data types
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

typedef struct
{
  boolean result;
  uint32  cb_data;
} dsumts_rmsm_dun_ctrl_params_type;

/*---------------------------------------------------------------------------
  Struct is used to listen to the registered events for DS from CM.
---------------------------------------------------------------------------*/
typedef struct
{
  boolean hybrid_pref;
  boolean emerg_to_normal_mode;
  cm_mode_pref_e_type  mode_pref;
    /**< Indicate New mode preference */
  sys_oprt_mode_e_type oprt_mode;
  cm_ph_state_e_type ph_state;
  sys_modem_as_id_e_type asubs_id;
} ds707_cm_ph_event_type;

/*---------------------------------------------------------------------------
  Type for the Service Option Control Message for Packet Data Dormant Timer
  Control.
---------------------------------------------------------------------------*/
typedef PACKED struct PACKED_POST
{
  byte  cntl;          /* Dormant Timer Control & Field Type               */
  byte  dorm_time;     /* Packet Data Dormant Timer value (in 1/10 sec)    */
} ds707_dorm_timer_socm_msg_type;

/*---------------------------------------------------------------------------
  Structure is used when calling function in 3G DSMGR.  These structures
  are then passed right back to this interface.  Contains info that is
  important in configuring the response to orig or end a call.  Specific
  to IS-707 data
---------------------------------------------------------------------------*/
typedef struct
{
  boolean                drs_bit;          /* contains DRS bit value       */
  boolean                use_hdown_timer;  /* use the holddown timer?      */
  uint32                 pkt_instance;     /* pkt instance to work on      */
  sys_sys_mode_e_type    new_network;      /* new system (1X or HDR) the AT
                                              is on - needed for data
                                              session handoffs             */
  ps_iface_type         *iface_ptr;        /* iface this is                */
  boolean                force_cdma_only;  /* flag to force the mode to be
                                              CDMA specific                */
  boolean                inc_call;         /* is this an incoming call     */
  ps_phys_link_type     *phys_link_ptr;    /* Helps pass current physlink  */
  boolean                epzid;            /* is EPZID changed             */
} ds707_pkt_call_info_type;

/*---------------------------------------------------------------------------
  Struct is used for 707 data when receive a retry delay from BS
  for DS_707_RETRY_DELAY_ORIG_CMD.
---------------------------------------------------------------------------*/
typedef struct
{
  boolean delay_infinite;                  /*delay until cancel          */
  uint32 delay;                            /*delay in msec second         */
}ds707_retry_delay_orig_type;

#if defined(FEATURE_DATA_WCDMA_PS) || defined(FEATURE_GSM_GPRS) || defined(FEATURE_DATA_LTE)
/*---------------------------------------------------------------------------
  Structure is used when calling function in 3G DSMGR.  These structures
  are then passed right back to this interface.  Contains info that is
  important in configuring the response to orig or end a call.
  Specific to UMTS.
---------------------------------------------------------------------------*/
typedef struct
{
  uint32                 call_instance;     /* call instance to work on    */
  void                   *info_ptr;         /* Info pointer                */
  boolean                mt_flag;           /*indicates that the call orig
                                              is due to a MT request       */
  boolean                dormant_reorig;    /* Phys link dormant reorig or
                                            call bring up*/
  void                   *user_data;        /* This is used to hold any other
                                               info that needs to be passed.*/
  ps_iface_event_info_u_type  event_info;      /* Event Info passed by PS*/
  struct
  {
    int rm_dev_instance;
    int sm_instance;
  } rmsm_instance;

} dsumtsps_call_info_type;

typedef struct
{
  uint32 event;
  void* sm_ptr;  

}ds_3gpp_rmsm_autoconfig_sm_type ;

typedef struct 
{
  ds_umts_pdp_type_enum_type  ds_pdp_type;  /* Incoming PDP type            */
  cm_call_id_type        call_id;           /* incoming call Id             */
  apn_T                  apn_name;          /* Incoming APN                 */
  pdp_address_T          pdp_addr;          /* Incoming PDP addr            */
} dsPdn_incom_call_info_type;

typedef struct
{
  ps_iface_type       *iface_ptr;           /* PS Iface pointer      */
  uint32              pdn_instance;         /* PDN instance          */
  boolean             is_companion_iface_in_use; /*Companion iface's status*/
  boolean             ota_teardown;              /*OTA teardown status*/
  void                *info_ptr;            /* void pointer */
}dsPdn_call_info_type;

typedef struct
{
  int                        ds_3gpp_rmsm_at_instance; /* RMSM_AT instance */
  ps_iface_event_enum_type   event;                    /* PS Iface event   */
  ps_iface_event_info_u_type event_info;               /* Event info */
  ps_iface_type             *iface_ptr;                /* IFACE pointer    */
  boolean                    result;                   /* result of reg */
} ds_3gpp_rmsm_at_type;

typedef struct
{
  int                          ds_3gpp_rmsm_at_instance; /* RMSM_AT instance */
  ps_iface_event_enum_type     event;                    /* Flow     event   */
  ps_iface_event_info_u_type    event_info;               /* Event info */
  ps_flow_type                *flow_ptr;                 /* FLow pointer    */
} ds_3gpp_rmsm_at_flow_type;

typedef struct
{
  void *bearer_context_p;
  void *pdn_context_p;
  dsm_watermark_type *wm_pointer_p;   
} ds_3gpp_pdn_bearer_wm_type;

#ifdef FEATURE_DATA_LTE
/*---------------------------------------------------------------------------
  Structure used for Loopback Mode B command
---------------------------------------------------------------------------*/
typedef struct
{
  ps_iface_event_enum_type   event;
  ps_iface_type *     um_iface_ptr;     /* Um iface pointer  */
}ds_eps_tlb_iface_event_type;

typedef struct
{
  int                 rm_flow;          /* flow action */
  ps_iface_type *     rm_iface_ptr; /* Rm Iface pointer  */
}ds_eps_tlb_rm_flow_evt_type;

typedef  union
{
  ds_eps_tlb_iface_event_type    iface_evt_info;
  ds_eps_tlb_rm_flow_evt_type    rm_flow_evt_info;
}ds_eps_tlb_cmd_type;

typedef struct
{
  int                        event;             /* Event type  */
  ds_eps_tlb_cmd_type        event_info;        /* Event info  */
}ds_3gpp_tlb_cmd_info_type;

#endif /* FEATURE_DATA_LTE */

/*---------------------------------------------------------------------------
  Structure passed with DSUMTSPS_SYS_CHG_SS_INFO_CMD command.
---------------------------------------------------------------------------*/
typedef struct
{
  sys_modem_as_id_e_type  subs_id;
  sys_sys_mode_e_type     sys_mode;
  cm_cell_srv_ind_s_type  cell_srv_ind;
#ifdef FEATURE_DATA_LTE
  sys_div_duplex_e_type   div_duplex;
#endif /* FEATURE_DATA_LTE */
} dsumtsps_sys_chg_ss_info_type;

typedef struct
{
  ps_iface_type     *iface_ptr;    /* iface pointer */
  void              *pkt_instance; /* packet instance */
} dsumtsps_pdp_ip_irat_tx_um_type;
#endif /* FEATURE_DATA_WCDMA_PS || FEATURE_GSM_GPRS || FEATURE_DATA_LTE*/

#ifdef FEATURE_GSM_GPRS
/* Structure for GSNDCP Registration/Resumption Confirmation Events */
typedef struct
{
  uint32                 call_instance;    /* instance of the call */
  boolean                cnf_status;       /* confirmation status  */
} dsumtsps_gsndcp_reg_cnf_ev_type;

/* Structure for GSNDCP Registration/Resumption Confirmation Events */
typedef struct
{
  void               *bearer_context_p;    /* instance of the call */
  boolean            cnf_status;           /* confirmation status  */
} ds3gpp_gsndcp_reg_cnf_ev_type;


#endif /* FEATURE_GSM_GPRS */

/* Type for DS3G_INITIATE_CALL */

typedef struct
{
  ds_call_type_e_type    call_type;        /* Call type, circuit or packet */
  const byte            *dial_string;      /* Null-terminated dial string  */
  boolean                digit_mode;       /* 1 = dial str contains non-   */
                                           /*     digit chars              */
                                           /* 0 = dial str contains only   */
                                           /*     digits                   */
} ds_orig_call_type;

/* Type for DS_RDM_OPEN_CMD */

typedef struct
{
  sio_port_id_type  port_id;           /* Port id of the port to be opened */
  void *            user_data;          /* User data pointer                */
} ds_rdm_open_type;

/* Type for DS_RDM_CLOSE_CMD */

typedef struct
{
  sio_port_id_type  port_id;           /* Port id of the port to be closed */
} ds_rdm_close_type;

/* Type for DS_DTR_EVENT_CMD */
typedef struct
{
  int port_id;            /* Port on which the DTR has changed */
} ds_dtr_type;

#ifdef FEATURE_DSAT_MDM_FUSION
/* Type for DS_CD_EVENT_CMD */
typedef struct
{
  int port_id;            /* Port on which the CD has changed */
} ds_cd_type;
#endif


/* Tpye to distinguish if CALL END Command is from CM or Internal*/
typedef enum
{
  DS_CM_CALL_END_NONE = -1,
  DS_CM_CALL_END_EXTERNAL,
  DS_CM_CALL_END_INTERNAL,
  DS_CM_CALL_END_MAX
} ds_cm_call_end_e_type;


/* Type for: DS_CM_CALL_END_CMD,
             DS_CM_CALL_INCOM_CMD,
             DS_CM_CALL_CONNECTED_CMD,
             DS_CM_CALL_SETUP_CMD,
             DS_CM_CALL_PROGRESS_CMD,
             DS_CM_CALL_ORIG_CMD,
             DS_CM_CALL_MODIFY_CMD,
             DS_CM_CALL_CONF_CMD       */

typedef struct
{
  byte                       call_id;    /* Call id of the call            */
  uint32                     call_inst; /* Call instance of the call*/
  cm_client_id_type          client_id;  /* who issued this cmd            */
  cm_call_event_e_type       call_event; /* CM call event that occurred    */
  cm_call_type_e_type        call_type;    /* Data call type, circuit/packet */
  cm_call_type_e_type        cm_call_type; /* CM Call type unchanged         */
  sys_sys_mode_e_type        call_mode;  /* System call is on -GSM, 1X, etc*/
  sys_modem_as_id_e_type     subs_id;
  cm_call_mode_info_s_type   mode_info;  /* Call info, depends on the mode */
  cm_call_end_e_type         end_status; /* why call ended                 */
  ds_cm_call_end_e_type      end_type;   /* Call end is from CM or internal*/

  /*contains the highest fail reason for the orig attempt provided by CM*/
  cm_call_orig_fail_layer_e_type highest_fail_reason;
  boolean                  failed_orig;   /* TRUE = CM failed orig immediately  */
  cm_call_cmd_err_e_type   call_cmd_err;  /* The err code for cm cmd   */        
  uint8 				    seq_num;         
} ds_call_info_type;

/* Type for DS_STATS_INFO_TYPE */
typedef struct
{
  uint16 bit_mask;
} ds_stats_info_type;

/* Type for : DS_CALL_RAB_REESTAB_REQ */
typedef struct
{
  byte                          call_id;            /* Call id of the call */
  cm_rab_reestab_params_s_type  rab_params;    /* RAB to be re-established */
} ds_rab_reestab_info_type;

/* Type for DS_TIMER_EXPIRED_CMD */

typedef struct
{
  unsigned long  timer_id;           /* Timer id of the timer that expired */
  void*          user_data;
} ds_timer_type;

#ifdef FEATURE_DSAT_GPS_CMDS
typedef struct
{
  pdsm_pd_info_s_type     dsat_pd_info;
  pdsm_pd_event_type      dsat_pd_event;
}ds_at_pdsm_info;
#endif /* FEATURE_DSAT_GPS_CMDS */

/* DS_AT_PDP_CMD data type,
   report PDP context activation/de-activation status */
typedef struct
{
  dsat_result_enum_type     response;
  dsat_rmsm_reason_type reason_type;
  ps_iface_net_down_reason_type  iface_net_down;/* Primary PS call down reason */
  ps_extended_info_code_enum_type ext_info_code;/* Secondary PS call down reason */
  dsat_rmsm_cmd_type        dsat_rmsm_cmd;
} ds_at_pdp_cmd_type;

/* DS_AT_CM_CALL_CMD data type,
   report call command execution status */
typedef struct
{
  void                   *data_ptr;
  cm_call_cmd_e_type      cmd;
  cm_call_cmd_err_e_type  cmd_err;
  cm_sups_cmd_err_cause_e_type  err_cause; 
  cm_alpha_s_type               alpha;
} ds_at_cm_call_cmd_type;

/* DS_AT_CM_CALL_INFO_CMD data type,
   convery call event info to ATCOP */
typedef struct
{
  cm_client_type_e_type     call_client_id;
  cm_call_id_type           call_id;
  cm_call_type_e_type       call_type;
  cm_call_state_e_type      call_state;
  boolean                   success;

  cm_call_direction_e_type        call_direction;
#ifdef FEATURE_DSAT_ETSI_MODE
  cm_call_ss_info_s_type          ss_info;  /* call related SS information */
  byte                            csd_rate_adaption;
  active_calls_list_s_type        active_calls_list; /* CM_MANAGE_CALLS_CONF
                                                  event related info */
#endif /* defined(FEATURE_DSAT_ETSI_MODE) */

  cm_call_sups_type_e_type        sups_type;
  uint16                          profile_number;
#ifdef FEATURE_SECONDARY_PDP
  byte                            group_id;
#endif /* FEATURE_SECONDARY_PDP */
#ifdef FEATURE_DSAT_ETSI_DATA
  boolean                         nsapi_valid;
  byte                            nsapi;
#endif /* FEATURE_DSAT_ETSI_DATA */
  cm_num_s_type                   dsat_num;
  cm_call_mode_info_s_type        dsat_mode_info;
  cm_call_end_e_type              end_status;
  cm_call_state_info_list_s_type  dsat_cm_call_info_list;
  boolean                         dsat_info_list_avail;
  sys_sys_mode_e_type             sys_mode;
#ifdef FEATURE_DUAL_ACTIVE
  cm_call_hold_state_e_type       local_hold_state;
#endif /* FEATURE_DUAL_ACTIVE */
} ds_at_call_info_s_type;

typedef struct
{
  cm_call_event_e_type      event;
  ds_at_call_info_s_type    event_info;
} ds_at_cm_call_event_type;

#ifdef FEATURE_DSAT_ETSI_MODE
typedef struct
{
  cm_inband_cmd_e_type      cmd;
  cm_inband_cmd_err_e_type  cmd_err;
} ds_at_cm_inband_cmd_type;
typedef struct
{
  cm_inband_event_e_type      event;
  const cm_inband_info_s_type*    event_info;
} ds_at_cm_inband_event_type;
#endif /* FEATURE_DSAT_ETSI_MODE */

/* Type for DS_AT_STATUS_CMD */
typedef struct
{
  uint16                       cmd_id;  /* identify which cpb callback function
                             submitted this cmd */
  mmgsdi_return_enum_type      cmd_status; /* contains the data for the command */
  byte          cmd_data;   /* contains the data for the command */
  mmgsdi_session_id_type       session_id;
  boolean                      is_status_cmd_handling;
  mmgsdi_pin_info_type         pin1;
  mmgsdi_pin_info_type         pin2;
  mmgsdi_pin_info_type         universal_pin;
  uint8                        apps_id;
} ds_at_cmd_status_type;

/* DS_AT_CM_SS_CMD data type,
   report Serving System command execution status */
typedef struct
{
  void                 *data_ptr;
  cm_ss_cmd_e_type      cmd;
  cm_ss_cmd_err_e_type  cmd_err;
} ds_at_cm_ss_cmd_type;
/* Cache of RX signal data */
typedef struct
{
  uint16     rssi;   /* received signal strength indicator  */
  int16      rssi2;   /* Unsigned RSSI from RRC(CM is a pass through) */
  int16      rscp;  /* received signal code power */
  uint8      bit_err_rate;
  int16      ecio;
  int16      pathloss; 
  int16      sir;
  sys_sys_mode_e_type    sys_mode;
  boolean                hdr_hybrid;
  uint16                 hdr_rssi;
} dsatcmif_sigrep_s_type;

/* DS_AT_CM_SS_INFO_CMD data type,
   convey Serving System event info to ATCOP */
typedef struct
{
  uint64                    changed_fields;
  boolean                   is_operational;
  sys_roam_status_e_type    roam_status;
  sys_srv_status_e_type     srv_status;
  sys_srv_domain_e_type     srv_domain;
  uint16                    rssi;
  uint16                    hdr_rssi;
  sys_sys_id_s_type         sys_id;
  sys_csg_id_type           csg_id;
  sys_mm_information_s_type mm_info;
  sys_sim_state_e_type      sim_state;
  cm_cell_srv_ind_s_type    cell_srv_ind;
  sys_sys_mode_e_type       sys_mode;
  sys_srv_status_e_type     hdr_srv_status;
  sys_roam_status_e_type    hdr_roam_status;
  sys_active_prot_e_type    hdr_active_prot;
  boolean                   hdr_hybrid;
  boolean                   ps_data_suspend;
  boolean                   is_sys_forbidden;
  sys_cell_info_s_type      cell_info;
  dsatcmif_sigrep_s_type    dsatcmif_signal_reporting;
  cm_emerg_num_list_s_type  *emerg_num_list;
  cm_reg_reject_info_s_type reg_reject_info;
  sys_srv_domain_e_type     srv_capability;
#ifdef FEATURE_DSAT_ETSI_MODE
#ifdef FEATURE_WCDMA_L1_HS_CQI_STAT
cm_cqi_info_s_type          dsat_cqi_info;
#endif /* FEATURE_WCDMA_L1_HS_CQI_STAT  */
#endif /* FEATURE_DSAT_ETSI_MODE */
#ifdef FEATURE_DSAT_LTE
  sys_lac_type           lte_tac;
  byte                   lte_rac_or_mme_code;
#endif /* FEATURE_DSAT_LTE */
#ifdef FEATURE_LTE_TO_1X
  boolean                is_csfb_call_active;
#endif /* FEATURE_LTE_TO_1X */
  uint16                 cm_req_id;
} ds_at_ss_info_s_type;

/* DS_AT_CM_PH_CMD data type,           */
/* indicates in there is any error during */
/* ATTACH or DETACH of +CGATT command     */
typedef struct
{
  cm_ph_cmd_e_type        cmd;
  cm_ph_cmd_err_e_type    cmd_err;
  cm_mmode_mru_table_entry_u_type mru_table_entry;
} ds_at_cm_ph_cmd_type;

/* DS_AT_CM_PH_INFO_CMD data type,
   convey phone event info to ATCOP */
typedef struct
{
  cm_network_sel_mode_pref_e_type  network_sel_mode_pref;
  cm_mode_pref_e_type              network_rat_mode_pref;
  cm_roam_pref_e_type              network_roam_pref;
  cm_band_pref_e_type              network_band_pref;
  cm_srv_domain_pref_e_type        service_domain_pref;
  cm_gw_acq_order_pref_e_type      acq_order_pref;
  cm_acq_pri_order_pref_s_type     acq_pri_order_pref;
} ds_at_cm_ph_pref_type;

typedef struct
{
  sys_oprt_mode_e_type             oprt_mode;
  ds_at_cm_ph_pref_type            pref_info[MAX_SUBS];
  sys_detailed_plmn_list_s_type    available_networks[MAX_SUBS];
  sys_user_pref_plmn_list_s_type   user_pref_networks[MAX_SUBS];
#ifdef CM_API_AOC_INFO
  cm_ph_aoc_info_s_type            aoc_info[MAX_SUBS];
#endif /* CM_API_AOC_INFO */
#ifdef FEATURE_UIM_RUN_TIME_ENABLE
  cm_rtre_control_e_type           rtre_control;
#endif /* FEATURE_UIM_RUN_TIME_ENABLE */
#if defined(FEATURE_HS_USB_SER3_PORT)
  cm_packet_state_e_type           packet_state;
#endif /* defined(FEATURE_HS_USB_SER3_PORT) */
#ifdef FEATURE_DSAT_LTE
  cm_lte_ue_mode_pref_e_type       eps_mode;
#endif /* FEATURE_DSAT_LTE */
#ifdef FEATURE_DATA_IS707
uint16                            dsat_rtre_config;  /* CM RUIM CONFIG CHANGED event */
#endif /*FEATURE_DATA_IS707*/
#ifdef FEATURE_DSAT_ETSI_DATA
cm_srv_domain_pref_e_type  dsat_srv_domain_pref;
#endif /*FEATURE_DSAT_ETSI_DATA*/
#ifdef FEATURE_DUAL_SIM
sys_modem_as_id_e_type                 default_data_subs;
#endif /*FEATURE_DUAL_SIM*/
#ifdef FEATURE_DSAT_LTE
  sys_drx_cn_coefficient_s1_e_type drx_coefficient;
  boolean                          set_drx_result;
#endif /*FEATURE_DSAT_LTE*/
  sys_modem_device_mode_e_type     device_mode;
  boolean                          is_gw_subscription_available;
  boolean                          is_hybr_gw_subscription_available;
  boolean                          is_hybr_gw3_subscription_available;
} ds_at_ph_info_u_type;

/* DS_AT_CM_PH_EVENT data type. */
/* convey phone events from CM to ATCOP */
typedef struct
{
  cm_ph_event_e_type      event;
  ds_at_ph_info_u_type    event_info;
} ds_at_cm_ph_event_type;

#ifdef FEATURE_MMGSDI
typedef struct
{
  mmgsdi_client_id_type         client_id;     /**< ID of the client to be notified. */
  mmgsdi_session_id_type        session_id;    /**< Client session ID. */
  mmgsdi_session_type_enum_type session_type;  /**< Client session type. */
  mmgsdi_app_enum_type          app_type;         /**< Client apps ID. */
} ds_at_mmgsdi_init_info;

#endif /* FEATURE_MMGSDI */
#ifdef FEATURE_DATA_PS_EAP 
typedef struct
{
  eap_sim_aka_task_srv_req_cb_type req_cb;
  void *                           user_data;
}ds_at_eap_task_sw_info ;

#define DS_EAP_MAX_PRE_MASTER_KEY_SIZE  256

typedef struct
{
  uint16                cmd_id;
  uint8                 pmk[DS_EAP_MAX_PRE_MASTER_KEY_SIZE];
  uint16                pmk_len;
  eap_handle_type       handle;
  eap_result_enum_type  result;
  void *                user_data;
} ds_at_authi_eap_result_cmd_type;

typedef struct
{
  uint16                cmd_id;
  uint8                 supp_auth[DS_EAP_MAX_PRE_MASTER_KEY_SIZE];
  uint16                supp_auth_len;
  eap_sim_aka_supp_auth_mask_type
                        supp_auth_info_mask;
  eap_handle_type       handle;
  eap_result_enum_type  result;
  void *                user_data;
} ds_at_authi_supp_eap_result_cmd_type;
typedef struct
{
  void           * user_data; 
  dsm_item_type  * pkt;
}ds_at_eap_resp;
#endif /* FEATURE_DATA_PS_EAP */

#if defined(FEATURE_DATA_WCDMA_PS) || defined(FEATURE_GSM_GPRS)
/* DS_AT_PS_DATA_GEN_CMD data type for PS Data generation command
   registration result and async notification of WM gone empty and
   interRAT HO events */
typedef struct
{
  boolean success;
  uint8   cid;
} ds_at_ps_data_gen_type;
#endif /* defined(FEATURE_DATA_WCDMA_PS) || defined(FEATURE_GSM_GPRS) */

#ifdef FEATURE_DSAT_ETSI_MODE

/* DS_AT_CM_SUPS_CMD data type,
   report Supplementary Services command execution status */
typedef struct
{
  void                   *data_ptr;
  cm_sups_cmd_e_type      cmd;
  cm_sups_cmd_err_e_type  cmd_err;
  cm_sups_cmd_err_cause_e_type  err_cause; 
  cm_alpha_s_type               alpha;
} ds_at_cm_sups_cmd_type;

/* DS_AT_CM_SUPS_INFO_CMD data type,
   convey Supplementary Services event info to ATCOP */
typedef struct
{
  /* General */
  cm_client_id_type                   sups_client_id;  /* client */
  uint8                               invoke_id;       /* Invocation Id */
  boolean                             ss_success;
  uint8                               ss_code;         /* SS code */
  uint8                               ss_operation;    /* SS operation */
  ie_ss_status_s_type                 ss_status;       /* SS status */
  ie_cm_ss_error_s_type               ss_error;        /* SS error */
  cm_ss_conf_e_type                   conf_type;       /* SS confirmation type */
  ss_password_value                   guidance_info;   /* Password guidance information */
  uss_data_s_type                     uss_data;        /* Unstructured SS data */
  cm_uss_data_e_type                  uss_data_type;   /* Type of USS message */
  cli_restriction_info_s_type         cli_restriction;
  basic_service_s_type                basic_service;
  ie_forwarding_feature_list_s_type   fwd_feature_list;
  basic_service_group_list_s_type     bsg_list;
  ie_call_barring_info_s_type         call_barring_info;
} ds_at_sups_event_info_s_type;

typedef struct
{
  cm_sups_event_e_type      event;
  ds_at_sups_event_info_s_type  event_info;
} ds_at_cm_sups_event_type;

/* DS_AT_GSDI_SIM_INFO data type,
   convery SIM read response from GSDI to ATCOP */
#define DSAT_MAX_MMGSDI_BUFFER_LEN  255
#define DSAT_MMGSDI_ICC_IC_LEN  10

typedef struct
{
   mmgsdi_len_type              length;
   byte              data[DSAT_MAX_MMGSDI_BUFFER_LEN];
} ds_at_sim_info_type;

typedef byte ds_at_iccid_type[DSAT_MMGSDI_ICC_IC_LEN];

typedef struct
{
  uint32          client_ref; /* contains the status for the command */
  mmgsdi_return_enum_type  cmd_status;
  uint8           apps_id;
  union
  {
    ds_at_sim_info_type  info;
    ds_at_iccid_type     iccid;
  } sim_info;
} ds_at_gsdi_sim_info_type;
#endif /* defined FEATURE_DSAT_ETSI_MODE */

#ifdef FEATURE_MMGSDI
/* DS_AT_MMGSDI_SIM_INFO data type,
   convery SIM read response from MMGSDI to ATCOP */
#define MAX_MMGSDI_BUFFER_LEN  512

typedef struct
{
  mmgsdi_return_enum_type       cmd_status;  /* Status of the command */
  uint32                        client_ref;  /* Client requested */
  int32                         data_len;    /* Data length  */
  byte                          data[MAX_MMGSDI_BUFFER_LEN]; /* actual data */
  mmgsdi_file_attributes_type   file_attr;  /* Complete file attributes */
  mmgsdi_srv_available_cnf_type srv_cnf;    /* Service confirmation details */
  uint8                 apps_id;
  mmgsdi_sw_type                status_word;        /* Status word  */
  mmgsdi_rec_num_type         accessed_rec_num;   /* Accessed Rec Num for Read Record */
  mmgsdi_offset_type          accessed_offset;    /* Accessed Offset for Read Transparent */
  mmgsdi_session_id_type    session_id;    /**< Client session ID. */
}ds_at_mmgsdi_sim_info_type;
#endif /* FEATURE_MMGSDI */

#ifdef FEATURE_DSAT_MMGSDI_AUTH

#define DSAT_MAX_VPM_DATA 70
#define DSAT_MAX_MD5_RSP 20

typedef struct
{
  int32     data_len;
  uint8     data[DSAT_MAX_MD5_RSP];

}dsat_mmgsdi_md5_cnf_s_type;

/* MD5/ATCOP Abort command handler */
typedef struct
{
  int16         cmd_status;
  int           cme_error;
} ds_at_md5_abt_type;

typedef struct
{
  mmgsdi_key_type key_data;               /*copy smekey for VPM key generation*/
  int32           data_len;
  uint8           data[DSAT_MAX_VPM_DATA];/*copy VPM*/

}dsat_mmgsdi_vpm_cnf_s_type;

typedef struct
{
  uint32                          client_ref;
  mmgsdi_return_enum_type         mmgsdi_status;
  mmgsdi_session_id_type          session_id;
  mmgsdi_sw_type                  status_word;
  union
  {
    boolean                       ssd_update_ok;          /* SSD Update confirm
                                                                    rsp */
    dword                         randbs;                 /* BS Chalenge  */
    boolean                       srv_available;          /* Service
                                                             Availability */
    dsat_mmgsdi_vpm_cnf_s_type    vpm_cnf;                /* VPM Responese */
    dsat_mmgsdi_md5_cnf_s_type    md5_cnf;                /* MD5 Response */
    mmgsdi_cave_auth_resp_type    run_cave_response_data; /* Cave response */
    dword                         authu;                  /* Authentication
                                                           challenge response */
  }auth_cmd;
}dsat707_mmgsdi_auth_cmd_type;

#endif /* FEATURE_DSAT_MMGSDI_AUTH  */

#ifdef FEATURE_DSAT_EXT_CLIENT_SUPPORT

typedef struct
{
  char                        at_cmd_buff[MAX_LINE_SIZE];
  uint16                      cmd_len;
  int8                        client_id;
  dsat_client_cmd_rsp_cb_func cb_func;
  void                       *client_data;
}ds_at_send_at_cmd_s_type;
#endif /* FEATURE_DSAT_EXT_CLIENT_SUPPORT */
typedef struct
{
  ds_cmd_enum_type    type;
  ds_sys_modem_as_id_e_type subs;
  void* sub_cmd;
}ds_at_cm_cmd_type;
typedef struct dsat_fwd_cmd
{
  uint32                             *user_info_ptr;
  dsat_cmd_list_type                  at_cmd_list[MAX_CMD_LIST];
  dsatclient_cmd_status_cb_type       status_cb;
  dsatclient_ext_at_cmd_exec_cb       ext_at_cmd_exec_cb;
  dsatclient_ext_at_cmd_abort_cb      ext_at_abort_cb;
  int32                               client_id;
  uint8                               num_valid_cmds;
}dsat_fwd_at_cmd_s_type;

typedef struct
{
  dsm_item_type                             *resp_buff_ptr;
  int32                                      client_id;
  uint32                                     atcop_info;
  boolean                                    cmd_status;
  dsat_client_result_enum_type               result_code;
  dsat_fwd_resp_enum_type                    resp_type;
}ds_at_ext_client_resp_s_type;

typedef struct
{
  dsat_cmd_status_cb_fn_type   status_func_cb;
  void                        *user_info_ptr ;
}ds_at_reset_cmd_request_s_type;

typedef struct
{
  dsm_item_type                             *resp_buff_ptr;
  int32                                      client_id;
  uint8                                    cmd_status;
}ds_at_ext_client_urc_s_type;

#ifdef FEATURE_ECALL_APP
typedef struct
{
  ecall_session_status_type  session_status;
  ecall_type_of_call     type_of_call;
  ecall_activation_type  activation_type;
}ds_at_ecall_cmd_s_type;
#endif /* FEATURE_ECALL_APP */
#if defined(FEATURE_ETSI_PBM) || defined(FEATURE_DSAT_CDMA_PBM)
/* DS_AT_PBM_CB_CMD data type,
   return pbm function cb results from pbm to ATCOP */
typedef struct
{
  pbm_device_type pbm_device; /* the storage type */
  int used;                   /* number of used entries for
                                 the storage type */
  int total;                  /* number of total entries for
                                 the storage type */
  int text_len;               /* Length of text field. */
} ds_at_pbm_file_info_type;

typedef struct
{
  pbm_field_s_type *fields;
  uint16 Category;
  uint16 ItemCount;
} dsat_pbm_addrbk_rec_s_type;

typedef struct
{
  int8          cmd;          /* Pbm call back command identifier.
                                 It determines what type of cmd_info
                                 is contained in cmd_info union */
  pbm_return_T status;        /* Command status of the pbm cb function */
  union
  {
    ds_at_pbm_file_info_type file_info; /* used for pbm_file_info_cb
                                           function */
    pbm_record_s_type        record;    /* used for pbm read or find
                                           call back function */

    dsat_pbm_addrbk_rec_s_type  addrbk_rec; /* used to read address book
                                               info from PBM */

  } cmd_info;                 /* pbm call back command results */
} ds_at_pbm_info_type;
#endif /* defined(FEATURE_ETSI_PBM) || defined(FEATURE_DSAT_CDMA_PBM) */

#ifdef FEATURE_MMGSDI_ATCSIM
typedef struct
{
  mmgsdi_client_data_type           client_ref;
  mmgsdi_return_enum_type           cmd_status; /* contains the status for the command */
  mmgsdi_len_type                   apdu_len;
  uint16          implicit_get_rsp_sw1;
  uint16          implicit_get_rsp_sw2;
  int32           channel_id;
  uint8                             *csim_apdu_data_ptr;
} ds_at_mmgsdi_apdu_resp_type;
#endif /* FEATURE_MMGSDI_ATCSIM */


/* SMS Event handler data structures */
#if defined(FEATURE_ETSI_SMS) || defined(FEATURE_CDMA_SMS)
typedef struct
{
  wms_cmd_id_e_type cmd;
  wms_cmd_err_e_type cmd_info;
}ds_at_sms_cmd_type;

/* SMS/ATCOP Abort command handler */
typedef struct
{
  int16         cmd_status;
  int           cms_error;
} ds_at_sms_abt_type;
#endif /* defined(FEATURE_ETSI_SMS) || defined(FEATURE_CDMA_SMS) */

#ifdef FEATURE_DATA_GCSD_FAX
/* Fax command status report */
typedef struct
{
  int16         cmd_status;
} ds_at_fps_t31_status_type;
#endif  /* FEATURE_DATA_GCSD_FAX */

#ifdef FEATURE_MMGSDI
/* DS_AT_MMGSDI_INFO_CMD data type */

/* dsat_fcn_ind_type holds the FCN details returned
   by MMGSDI_REFRESH_FCN call back */

/* MAX number of refresh files ( UMTS EF's +CDMA EF's) */

  #define DSAT_MAX_NUM_REFRESH_FILES 6

typedef struct
{
  uint32                num_files; /* number of files in FCN ind */
  mmgsdi_file_enum_type  file_list[DSAT_MAX_NUM_REFRESH_FILES]; /* file list */
} dsat_fcn_ind_type;

typedef struct
{
  mmgsdi_events_enum_type mmgsdi_event;  /* client event */
  int16           cmd_status;  /* contains the status for the command */
  mmgsdi_pin_evt_info_type pin;
  uint8           apps_id;       /* Internal application specific id  */
  mmgsdi_perso_evt_info_type perso_info;
  mmgsdi_slot_id_enum_type slot_id;
  mmgsdi_session_id_type session_id;  /* session id obtained from mmgsdi */
  mmgsdi_card_err_evt_info_type    card_error; /*contain error information*/
  mmgsdi_app_enum_type app_type;
  mmgsdi_static_data_type aid;
  mmgsdi_refresh_stage_enum_type refresh_stage;
  mmgsdi_subscription_ready_evt_info_type         subscription_ready;
  boolean  activated;
  
  union
  {
    dsat_fcn_ind_type   fcn_ind;/* FCN details */
  }u;
} ds_at_mmgsdi_event_type;

#define DSAT_MAX_FULLNAME_STR_LEN 24
#define DSAT_MAX_SHORTNAME_STR_LEN 8


typedef struct
{
  boolean                         long_plmn_name_ci;/*Flag that indicates 
                                                           whether the country
                                                           initials should be 
                                                           added to plmn name*/
  boolean                         short_plmn_name_ci;
  byte                            long_name[DSAT_MAX_FULLNAME_STR_LEN*2];/*long name, USS2 character are 2 byte long */
  byte                            short_name[DSAT_MAX_SHORTNAME_STR_LEN*2];/*short name , USS2 character are 2 byte long */
  uint8                           long_plmn_name_spare_bits;/* Number of spare bits
                                                         in the end octet of the
                                                         plmn name */
  uint8                           short_plmn_name_spare_bits;  
  uint16                          lac;/*lac info*/
  int32                           long_name_len;/*long name lenght*/
  int32                           short_name_len;/*short name lenght*/
  mmgsdi_plmn_id_type             plmn_id;/* plmn identity*/
  mmgsdi_eons_encoding_enum_type  long_name_encoding; /*long name encoding type*/
  mmgsdi_eons_encoding_enum_type  short_name_encoding;/*short name encoding type*/
}ds_at_plmn_info;

typedef struct
{
  mmgsdi_cnf_enum_type	   cnf;
  mmgsdi_return_enum_type  mmgsdi_status;
  uint32                   client_ref;  /* Client requested */
  mmgsdi_session_id_type   session_id;
  mmgsdi_eons_name_type    spn;             /**< Service provider name. */
  uint32                   num_of_plmn_ids;  /**< Number of plmn ids. */
  ds_at_plmn_info          **plmn_info_ptr;    /**< Pointer to plmn info. */
}ds_at_mmgsdi_oper_name_type;

typedef struct
{
  mmgsdi_se13_plmn_info_type  plmn_info;    /*plmn info. */
  uint32                      client_ref;  /* Client requested */
  uint32                      num_of_plmn_ids;  /**< Number of plmn ids. */
  mmgsdi_cnf_enum_type        cnf;
  mmgsdi_return_enum_type     mmgsdi_status;
}ds_at_mmgsdi_plmn_info_type;

#endif /* FEATURE_MMGSDI */
#ifdef FEATURE_DSAT_LTE
#ifdef FEATURE_VOIP 
typedef struct
{
  void*     voip_parm;
}ds_at_voipm_config_info_type;
#endif /* FEATURE_VOIP */
#endif /* FEATURE_DSAT_LTE */

/* Type for DS_RELEASE_CALL_CMD */
typedef struct
{
   cm_end_params_s_type end_params;
} ds_release_call_ind_type;

#if ((defined(FEATURE_DATA_WCDMA_CS)) || (defined(FEATURE_DATA_GCSD)))
/* DS_UCSD_APP_ORIG_CALL_CMD command type */
typedef struct
{
  ds_ucsd_client_id_type  client_id;      /* UCSD client identifier        */
  uint8                inst_id;           /* CSData call instance ID       */
  ds_ucsd_call_type    call_type;         /* Type of call                  */
  uint8                call_mode;         /* Single/dual mode flag         */
  uint8                speed;             /* Equivalent to +CBST Speed     */
  uint8                name;              /* Synchronous or Asynchronous   */
  uint8                ce;                /* Transparent or Non-Transparent*/
  ds_ucsd_data_rate_T  ota_rate;          /* OTA data rate                 */
  uint8                waiur;             /* Equivalent to +CHSN WAIUR.    */
  const uint8         *dial_string;       /* Null-terminated dial string   */
  dsm_watermark_type  *ul_rlc_wm_ptr;     /* Pointer to UL RLC WM          */
  dsm_watermark_type  *dl_rlc_wm_ptr;     /* Pointer to DL RLC WM          */
  ds_ucsd_rlc_dl_rx_cb_type rlc_dl_rx_cb; /* Function called by RLC        **
                                          ** when RLC receives DL data.    **
                                          ** This callback can be NULL     */
  ds_ucsd_dial_modifier_info_type modifiers;/* SupSrv dial modifiers       */
  ds_ucsd_uus_info_type uus_info;          /* User to User IE              */
} ds_ucsd_app_orig_call_cmd_type;

/* DS_UCSD_APP_ANSWER_CALL_CMD command type */
typedef struct
{
  ds_ucsd_client_id_type    client_id;    /* UCSD client identifier        */
  uint8                inst_id;           /* CSData call instance ID       */
  ds_ucsd_call_type    call_type;         /* Type of call                  */
  dsm_watermark_type  *ul_rlc_wm_ptr;     /* Pointer to UL RLC WM          */
  dsm_watermark_type  *dl_rlc_wm_ptr;     /* Pointer to DL RLC WM          */
  ds_ucsd_rlc_dl_rx_cb_type rlc_dl_rx_cb; /* Function called by RLC        **
                                          ** when RLC receives DL data.    **
                                          ** This callback can be NULL     */
  ds_ucsd_answer_param_type answer_params; /* Incoming call answer info    */
} ds_ucsd_app_answer_call_cmd_type;

/* DS_UCSD_APP_CONFIRM_CALL_CMD command type */
typedef struct
{
  ds_ucsd_client_id_type    client_id;     /* UCSD client identifier       */
  uint8                     inst_id;       /* CSData call instance ID     */
  ds_ucsd_call_type         call_type;     /* Type of call                */
  ds_ucsd_answer_param_type result_params; /* Call setup result info      */
} ds_ucsd_app_confirm_call_cmd_type;

#ifdef FEATURE_MULTIMEDIA_FALLBACK
#error code not present
#endif /* FEATURE_MULTIMEDIA_FALLBACK */

/* DS_UCSD_APP_END_CALL_CMD command type */
typedef struct
{
  ds_ucsd_client_id_type    client_id;     /* UCSD client identifier       */
  uint8                inst_id;           /* CSData call instance ID       */
  ds_ucsd_call_type    call_type;         /* Type of call                  */
} ds_ucsd_app_end_call_cmd_type;

/* DS_UCSD_HANDOVER_CMD comamnd type */
typedef struct
{
  sync_enum_T    reason;                  /* Handover direction */
  uint32         rab_id;                  /* RAB identifier */
} ds_ucsd_handover_cmd_type;

#ifdef FEATURE_UUS
/* DS_UCSD_USER_USER_DATA_IND_CMD, DS_UCSD_USER_USER_DATA_CONG_IND_CMD */
typedef struct
{
  cm_call_id_type                 call_id;
  cm_call_event_user_data_s_type  uus_info;
} ds_ucsd_uus_data_cmd_type;
#endif /* FEATURE_UUS */

#endif /* FEATURE_DATA_WCDMA_CS || FEATURE_DATA_GCSD */

#ifdef FEATURE_HDR_BCMCS
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  struct used to pass flow status and other  BCMCS events info  from CM
  with CM commands
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
typedef struct
{
  sys_sys_mode_e_type             mode;
  uint8                           num_flows;
  sys_bcmcs_flow_status_element_s_type flow_list[DSBC_MAX_NUM_FLOWS];
  #ifdef FEATURE_HDR_BCMCS_2p0
  #error code not present
#endif /*FEATURE_HDR_BCMCS_2p0 */
} dsbcmcs_cm_cmd_type;

#endif /* FEATURE_HDR_BCMCS */

#ifdef FEATURE_MBMS
#error code not present
#endif /* FEATURE_MBMS */

#if defined(FEATURE_MFLO) && defined(FEATURE_MFLO_FLOMCS)
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  struct used to pass flow status and other  FLOMCS events info  from FLO
  stack
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
typedef struct
{
   uint16 evt;
   uint32 user_handle;
} dsflobcmcs_flo_stack_cmd_type;
#endif /* defined(FEATURE_MFLO) && defined(FEATURE_MFLO_FLOMCS) */

#ifdef FEATURE_HDR_QOS
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Struct used to pass RLP ID and associated Reservation Label to DS
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
typedef struct
{
  dsrlp_rlp_identity_type rlp;
  uint8 resv_label;
} ds_qos_resv_type;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Struct used to pass info about qos grant or modify notification to flow mgr
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
typedef struct
{
  uint8 resv_label;                      /* Reservation label              */
  ds707_direction_enum_type dirn;        /* Direction                      */
} ds707_qos_flow_mgr_qos_grant_notify_type;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Struct used to pass info about qos release notification to flow mgr
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
typedef struct
{
  uint8 resv_label;                      /* Reservation label              */
  ds707_direction_enum_type dirn;        /* Direction                      */
} ds707_qos_flow_mgr_qos_rel_notify_type;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Struct used to pass info about reservation on notification to flow mgr
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

typedef struct
{
  uint8 resv_label;                      /* Reservation label              */
  ds707_direction_enum_type dirn;        /* Direction                      */
  ds707_sec_pkt_flow_mgr_cb_enum_type cback_qualifier;  /* Call Back Reason               */
} ds707_qos_flow_mgr_resv_on_notify_type;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Struct used to pass info about reservation off notification to flow mgr
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
typedef struct
{
  uint8 resv_label;                    /* Reservation label                */
  ds707_direction_enum_type dirn;      /* Direction                        */
  ds707_sec_pkt_flow_mgr_cb_enum_type cback_qualifier;  /* Call Back Reason               */
} ds707_qos_flow_mgr_resv_off_notify_type;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Struct used to pass info about reservation off notification to flow mgr
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
typedef struct
{
  uint8 resv_label;                    /* Reservation label                */
  ds707_direction_enum_type dirn;      /* Direction                        */
} ds707_qos_flow_mgr_reservation_on_sent_notify_type;


/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Struct used to pass info about Secondary PS Flows
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
typedef struct
{
  ps_flow_type   *flow_ptr[MAX_QOS_OPERATIONS_PER_IOCTL]; /*PS flow array  */
  void           *client_data_ptr; /* Pointer to data associated with flow  */
  uint8           num_of_flows;    /* number of flows                       */
  ps_iface_type  * ps_iface_ptr;   /* iface ptr */
  boolean         is_nw_init_flow; /* If this is network initiated flow*/
  uint32          nw_init_resv_tid; /* NW initiated RESV msg transation ID*/
} ds707_sec_pkt_flow_cmd_type;


/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Struct used to notify network initiated PPP down to sec pkt mgr
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
typedef struct
{
  ps_iface_type              *ps_iface_ptr; /* iface on which PPP is gone */
} ds707_sec_pkt_ppp_cmd_type;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Struct used to pass info about Iface to Sec pkt mgr.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
typedef struct
{
  ps_iface_event_enum_type      event; /* Event name */
  ps_iface_event_info_u_type    event_info; /* Event Info */
  void                          *client_data_ptr; /* Pointer to data associated with flow  */
} ds707_sec_pkt_iface_event_type;

/*---------------------------------------------------------------------------
  Struct used by RSVP cmd callback function to notify DS of RESV response
---------------------------------------------------------------------------*/
typedef struct
{
  void                     * user_data_ptr;
  int32                      sid;
  uint32                     msg_id;
  ps_rsvp_event_enum_type    event;
} ds_dqos_mgr_resv_resp_notify_cmd_type;

#endif /* FEATURE_HDR_QOS */

/*---------------------------------------------------------------------------
  Struct used to pass Srvc Ref ID to DSRLP
---------------------------------------------------------------------------*/
typedef struct
{
  byte sr_id;                             /* Service reference identifier */
  byte rlp_blob_len;              /* RLP information block of bits length */
  byte rlp_blob[15];                                 /* RLP block of bits */
  boolean rlp_info_incl;            /* RLP information included indicator */
} dsrlp_blob_cmd_type;


typedef struct
{
  ppp_dev_enum_type   ppp_dev;
  ppp_protocol_e_type protocol;
  ppp_event_e_type    ppp_event;                 /* Down/Up/Resync         */
  void               *user_data;
  uint32              session_handle;
  ppp_fail_reason_e_type  fail_reason;
}ds_ppp_ev_cmd_type;


#ifdef FEATURE_MMGSDI_SESSION_LIB
/* The following events are set when receiving MMGSDI events */
typedef enum
{
  DS3G_MMGSDI_IDLE_EVENT = 0,
  /* Initilized value */
  DS3G_MMGSDI_SUBSCRIPTION_READY_EVENT = 1,
  /* Received MMGSDI_SUBSCRIPTION_READY_EVT */
  DS3G_MMGSDI_REFRESH_VOTING_EVENT  = 2,
  /* Received MMGSDI_REFRESH_EVT and stage is MMGSDI_REFRESH_STAGE_WAIT_FOR_OK_TO_INIT */
  /* ISIM voting feature is interested at this event */
  DS3G_MMGSDI_OK_REFRESH_EVENT = 3,
  /* Received MMGSDI_REFRESH_EVT and stage is MMGSDI_REFRESH_STAGE_WAIT_FOR_OK_TO_FCN */
  /* both CDMA session and UMTS session are interested at this event */
  DS3G_MMGSDI_REFRESH_START_EVENT = 4,
  /* this event is received when SIM card's refresh 
  is triggered */  
  DS3G_MMGSDI_REFRESH_COMPLETE_EVENT = 5,
  /* Received MMGSDI_REFRESH_EVT and stage is MMGSDI_REFRESH_STAGE_START */
  /* Right now, only 1x is interested at this event */
  DS3G_MMGSDI_SIM_READ_EVENT = 6,
  /* When receiving  MMGSDI_SUBSCRIPTION_READY_EVT, DS will send request to check
     CDMA service table. DS will set this event when receiving the call back of
     mmgsdi_session_read_transparent
     */
  DS3G_MMGSDI_SIM_WRITE_EVENT = 7,
  /* After reading info from CDMA service table. DS will set update
     CDMA operation table, this event is set when receiving the call back of
     mmgsdi_session_write_transparent
     */
  DS3G_MMGSDI_SRV_AVAIL_EVENT = 8,
  /* this event is received when receiving the callback of
  mmgsdi_session_is_service_available
     */
  DS3G_MMGSDI_IMSI_INFO_EVENT = 9,
  /* this event is received when receiving the callback of
  imsi info */
  DS3G_MMGSDI_MNC_INFO_EVENT = 10,
  /* this event is received when receiving the callback of
  mnc info */
  DS3G_MMGSDI_CLIENT_ID_EVENT = 11,
  /* this event is received when receiving the callback of
  mmgsdi_client_id_and_evt_reg */
  DS3G_MMGSDI_ACL_INFO_EVENT = 12,
  /* this event is received when receiving the callback of
  acl info */
  DS3G_MMGSDI_ACL_ENABLED_EVENT = 13,
  /* this event is received when receiving the callback of
  mmgsdi_session_get_app_capabilities */
  DS3G_MMGSDI_CARD_ERROR_EVENT = 14,
  /* this event is received when SIM card is removed */
  DS3G_MMGSDI_SESSION_CHANGED_EVENT = 15,
  /* this event is received when session is changed */
  DS3G_MMGSDI_ISIM_REFRESH_START_EVENT = 16,
  /* this event is received when ISIM refresh is started */
  DS3G_MMGSDI_ISIM_REFRESH_COMPLETE_EVENT = 17,
  /* this event is received when ISIM refresh is complete */
  DS3G_MMGSDI_ISIM_OK_REFRESH_EVENT = 19
  /* Received MMGSDI_REFRESH_EVT and stage is MMGSDI_REFRESH_STAGE_WAIT_FOR_OK_TO_FCN */
  /* ISIM session is interested at this event */
} ds3g_mmgsdi_event_enum_type;

typedef struct
{
  ds3g_mmgsdi_event_enum_type event_type;
  mmgsdi_session_id_type session_id;
  mmgsdi_app_enum_type   app_type;
  mmgsdi_slot_id_enum_type slot;
  mmgsdi_session_changed_evt_info_type  session_changed_info;
  mmgsdi_refresh_mode_enum_type refresh_mode;
  union
  {
    ds_at_mmgsdi_event_type   mmgsdi_event_info;
    ds_at_mmgsdi_sim_info_type  mmgsdi_sim_info;
  }cmd_info;
} ds3g_mmgsdi_event_info;

typedef struct
{
  mmgsdi_cnf_enum_type  cnf_type;
  mmgsdi_cnf_type       cnf_data;
  uint8                 read_cnf_data[MAX_MMGSDI_BUFFER_LEN];
} ds3g_mmgsdi_session_cnf_info;

#endif /* FEATURE_MMGSDI_SESSION_LIB */

#ifdef FEATURE_DATA_TEST_LOOPBACK
typedef struct
{
  int     id;
  void *  info_ptr;
  void *  user_data;
}ds_loopback_hdlr_cmd_info_type;
#endif /* FEATURE_DATA_TEST_LOOPBACK */

typedef struct
{
  void   * flow_context_p;
  void   * bearer_context_p;
  uint32   fltr_id_conflict;
  uint32   preced_conflict[8];
  boolean  prec_conf_on_same_bearer_modify;
}ds_3gpp_flow_manager_cmd_info_type;
typedef enum
{
  DS_3GPP_FLOW_BEARER_ALLOC = 0,
  DS_3GPP_FLOW_BEARER_ALLOC_ABORT = 1,
  DS_3GPP_FLOW_BEARER_MODIFY = 2
} ds_3gpp_flow_lower_layer_enum_type;

typedef struct
{
  qos_spec_type                       app_qos;
  tft_type_T                          nw_tft;
  void                                *flow_context_p;
  void                                *pdn_context_p;
  int                                 flow_state;
  ds_3gpp_flow_lower_layer_enum_type  event_type;
}ds_3gpp_flow_lower_layer_cmd_info_type;

typedef struct
{
  ps_iface_type   *this_iface_ptr;
  ps_iface_type   *target_iface_ptr;
  void            *client_data;
}ds_3gpp_setup_bridge_cmd_info_type;


typedef enum
{
  DS_SIO_EV_MIN          = 0,
  DS_SIO_EV_DSM_MEM_CTRL = 1,
  DS_SIO_EV_MAX
} ds_sio_event_enum_type;

typedef struct
{
  ds_sio_event_enum_type event_type;
  union
  {
    struct
    {
      dsm_mempool_id_enum_type    pool_id;
      dsm_mem_level_enum_type     event;
      dsm_mem_op_enum_type        op;
    } dsm_info;
  } cmd_info;
} ds_sio_event_type;

typedef enum
{
  DS3GPP_EV_MIN          = 0,
  DS3GPP_EV_DSM_MEM_CTRL = 1,
  DS3GPP_EV_MAX
} ds3gpp_event_enum_type;

typedef struct
{
  ds3gpp_event_enum_type event_type;
  union
  {
    struct
    {
      dsm_mempool_id_enum_type    pool_id;
      dsm_mem_level_enum_type     event;
      dsm_mem_op_enum_type        op;
    } dsm_info;
  } cmd_info;
} ds3gpp_event_type;

typedef struct
{
  sys_modem_dual_standby_pref_e_type     standby_pref;
      /* Standby preference of the Phone */

  sys_modem_as_id_e_type                 active_subs;
      /* The active subscription in Single Standby mode
      ** only valid when standby_pref is SYS_MODEM_DS_PREF_SINGLE_STANDBY
      */

  sys_modem_as_id_e_type                 default_voice_subs;
      /* default voice subscription */

  sys_modem_as_id_e_type                 default_data_subs;
      /* default data subscription */

  sys_modem_device_mode_e_type           device_mode;
      /* Device mode of operation */

} ds3g_cm_standby_info_type;

typedef struct
{
  sys_modem_as_id_e_type   subs_id;
  sys_data_priority_e_type priority_type;
} ds3g_cm_priority_info_type;

typedef struct
{
  sys_modem_as_id_e_type            subscription_id;
    /**< Active Subscription ID assigned to this subscription */

  mmgsdi_static_data_type           app_id_1x;
    /**< 1x Application ID associated with this subscription. */

  mmgsdi_session_type_enum_type     session_type_1x;
    /**< 1x UIM session type on which this subscription is currently provisioned. */
		
  mmgsdi_static_data_type           app_id_gw;
    /**< GW Application ID associated with this subscription. */

  mmgsdi_session_type_enum_type     session_type_gw;
    /**< GW UIM session type on which this subscription is currently provisioned. */	

  boolean                           is_operational;
    /**< Flag indicating if this subscription is in operation right now */

  boolean                           is_priority_subs;
    /**< Flag indicating if this subscription is the priority subscription */

  boolean                           is_default_voice;
    /**< Flag indicating if this subscription is the default voice subs */

  boolean                           is_default_data;
    /**< Flag indicating if this subscription is the default data subs */

  cm_pref_s_type                    pref_info;
    /**< Acquisition preferences for this subscription. */

  uint8                             uicc_id[CM_MMGSDI_MAX_UICC_LEN];
    /**< UICC ID of the card to which this subscription belongs. */

  sys_subs_feature_t                sub_feature_mode;
    /**< Enum indicating if this subscription is SGLTE */

  boolean                           is_available_gw;
  /**< Flag indicating if GW subscription is available */

} ds3g_cm_sub_info_type;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  structure to store the call status for a v6 call. This structure will be 
  used by mode handlers, to propogate the error code in a failure case. 
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
typedef enum
{
  DS_CALL_CATEGORY_PPP_TYPE = 0,
  DS_CALL_CATEGORY_NON_PPP_TYPE  = 1,
  DS_CALL_CATEGORY_MAX
} ds_call_category_enum_type;

typedef struct
{
  ds_call_category_enum_type call_type;
  union{
#ifdef FEATURE_DATA_IS707
    ds_v6_ppp_status   ppp_status;
#endif
    int32              call_status;
  }call_end_reason_info;
  void *dssnet_instance;
} ds_v6_call_status_type;

typedef struct
{
  int port_state;
  int port_operation_state;
} ds_at_mux_notify_info_type;

typedef struct
{
  rdm_assign_status_type status;
  rdm_service_enum_type service;
  rdm_device_enum_type device;
} ds_at_rdm_notify_info_type;

typedef PACKED struct PACKED_POST
{
  dword hdr_remaining_thr_time;  /* Remaining call throttling time in secs */
  dword cdma_remaining_thr_time; /* Remaining call throttling time in secs */
} ds707_call_throttle_info;

#ifdef FEATURE_DATA_RPM
/*---------------------------------------------------------------------------
  Structure to store info for RPM
---------------------------------------------------------------------------*/
typedef struct
{
  sys_sys_mode_e_type  sys_mode;
  sys_plmn_id_s_type	 plmn_id;
  uint8 			 apn_index;
  sys_modem_as_id_e_type subs_id;
} ds_3gpp_rpm_info_type;
#endif /* FEATURE_DATA_RPM */

/*---------------------------------------------------------------------------
TYPEDEF DS707_TEAR_DOWN_INFO_TYPE

DESCRIPTION
  Used to pass call end reason when a call is torn down
---------------------------------------------------------------------------*/
typedef struct
{
  ps_iface_net_down_reason_type call_end_reason;
} ds707_tear_down_info_type;

#ifdef FEATURE_DATA_IS707
typedef struct
{
  ps_iface_type *iface_ptr;
  boolean is_user_data_set;
  union
  {
    ds707_pkt_call_info_type pkt_call_info;
    network_params_info_type nw_params_info;
  }user_data;

} ds707_bring_up_cmd_info_type;

typedef struct
{
  ps_iface_type *iface_ptr;
  boolean lpm_mode;
  boolean is_user_data_set;
  union
  {
    ps_iface_tear_down_client_data_info_type tear_down_info;
  }user_data;

} ds707_tear_down_cmd_info_type;
#endif /* FEATURE_DATA_IS707 */

typedef struct
{
  ps_iface_type              *iface_ptr;
  ps_iface_event_enum_type   event;
  ps_iface_event_info_u_type event_info;
  void                       *user_data;
} dsat_iface_info_s_type;

#ifdef FEATURE_DATA_WCDMA_PS
typedef  struct
{
  void*                       bearer_context_p;
  boolean                    cnf_status;
} ds3gpp_purge_cnf_ev_type;
#endif /* FEATURE_DATA_WCDMA_PS */
typedef struct
{
  cm_ac_state_e_type      state;       /* Access Control command state*/
  cm_ac_end_reason_e_type end_reason;   /* Access Control state end reason */
} ds_cm_ac_cmd_state_info_s_type;

typedef struct
{
  void *ds_iface_p;
}ds_3gpp_iface_bridge_s_type;

typedef struct
{
  uint32 rab_id;
  uint32 ul_data_rate;
 sys_modem_as_id_e_type subs_id;
}ds_3gpp_ul_rab_info_type;

typedef struct
{
  int subs_id;
  void* throt_sm_ptr;
}ds_3gppi_throttle_sm_info_type;

typedef struct
{
  uint16                 profile_num;
  boolean                roaming_disallowed_flag;
  int                    subs_id;
}ds_3gpp_roam_info_type;

typedef struct
{
  uint16                     profile_num;
  uint64                     user_data;
  ds_umts_pdp_type_enum_type old_pdp_type;
}ds_3gpp_apn_param_chg_type;


typedef struct
{
  sys_modem_as_id_e_type subscription_id;
  sys_roam_status_e_type gw_roam_status;
}ds_3gpp_roaming_ind_s_type;

typedef struct
{
  uint16  profile_num;
  boolean apn_disable_flag;
}ds_3gpp_apn_disable_info;

typedef struct
{
  sys_modem_as_id_e_type                subs_id;
  uint32   in_order_delivery_requested;
}ds_3gpp_switch_data_path_type;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Command type: The command header identifies the command. If the command has
  any parameters, they are specified in the union.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
typedef struct
{
  ds_cmd_hdr_type    hdr;                                /* Command header */

  union
  {
    void*                          client_data_ptr;
    ds_3gpp_roam_info_type          roaming_info;
    ds_3gpp_apn_disable_info apn_disable_info;
    ds_3gppi_throttle_sm_info_type throt_info;
    ds_at_mux_notify_info_type mux_info;
    ds_at_rdm_notify_info_type rdm_info;
    ds_at_pdp_cmd_type        pdp_info;
#ifdef FEATURE_DSAT_GPS_CMDS
    ds_at_pdsm_info    dsat_pd_info;
#endif /* FEATURE_DSAT_GPS_CMDS */
    ds_rdm_open_type          rdm_open;
    ds_rdm_close_type         rdm_close;
    ds_dtr_type               dtr_info;       /* Pointer to DTR event info */
#ifdef FEATURE_DSAT_MDM_FUSION
    ds_cd_type                cd_info;       /* Pointer to CD event info */
#endif
    ds_call_info_type         call_info;
    ds_stats_info_type        stats_info;
    ds_timer_type             timer;
    ds_rab_reestab_info_type  rab_info;
    ds_at_cm_cmd_type         cm_cmd;
    ds_at_cmd_status_type     status_cmd;   /* used to report the excution */
                                            /* status of a async function  */
                                            /* call.  The callback function*/
                                            /* of the async command submit */
                                            /* this cmd.                   */

    unsigned int              dsat_timer_id;

#ifdef FEATURE_MMGSDI
    ds_at_mmgsdi_event_type                         mmgsdi_event_info;
    ds_at_mmgsdi_sim_info_type                      mmgsdi_sim_info;
    ds_at_mmgsdi_init_info                          mmgsdi_init_info;
    ds_at_mmgsdi_oper_name_type                     oper_name_cmd;
    ds_at_mmgsdi_plmn_info_type                     mmgsdi_plmn_info;
#endif /* FEATURE_MMGSDI */
#ifdef FEATURE_DATA_PS_EAP 
    ds_at_eap_task_sw_info      eap_task_sw;
    ds_at_authi_eap_result_cmd_type  eap_result;
    ds_at_authi_supp_eap_result_cmd_type supp_auth_info;
    ds_at_eap_resp                  eap_resp;
#endif /* FEATURE_DATA_PS_EAP */
#ifdef FEATURE_DSAT_LTE
#ifdef FEATURE_VOIP 
     ds_at_voipm_config_info_type  voip_rsp_info;
#endif /* FEATURE_VOIP */
#endif /* FEATURE_DSAT_LTE */
#ifdef FEATURE_DSAT_MMGSDI_AUTH
  dsat707_mmgsdi_auth_cmd_type  mmgsdi_auth_cmd;
  ds_at_md5_abt_type        md5_abt;
#endif /* FEATURE_DSAT_MMGSDI_AUTH */

#ifdef FEATURE_MMGSDI_SESSION_LIB
    ds3g_mmgsdi_event_info ds_mmgsdi_event_info;
    ds3g_mmgsdi_session_cnf_info  ds_mmgsdi_session_cnf_info;
#endif /* FEATURE_MMGSDI_SESSION_LIB */

    ds3g_cm_sub_info_type        sub_info;
    ds3g_cm_standby_info_type    standby_info;
    ds3g_cm_priority_info_type   priority_info;

    ds_sio_event_type      ds_sio_event_info;
    ds3gpp_event_type      ds3gpp_event_info;


#ifdef FEATURE_DSAT_ETSI_MODE
    ds_at_cm_inband_event_type  inband_event;
    ds_at_cm_inband_cmd_type    inband_cmd;
    ds_at_gsdi_sim_info_type  gsdi_sim_info;

#ifdef FEATURE_MMGSDI_ATCSIM
    ds_at_mmgsdi_apdu_resp_type mmgsdi_apdu_resp;
#endif /* FEATURE_MMGSDI_ATCSIM */

    ds_at_cm_sups_cmd_type      sups_cmd;
    ds_at_cm_sups_event_type    sups_event;
#endif /* FEATURE_DSAT_ETSI_MODE */
#if defined(FEATURE_ETSI_PBM) || defined(FEATURE_DSAT_CDMA_PBM)
    ds_at_pbm_info_type         pbm_info;
#endif /* defined(FEATURE_ETSI_PBM) || defined(FEATURE_DSAT_CDMA_PBM) */
#if defined(FEATURE_ETSI_SMS) || defined(FEATURE_CDMA_SMS)
    ds_at_sms_cmd_type        sms_cmd;
    ds_at_sms_abt_type        sms_abt;
#endif /* defined(FEATURE_ETSI_SMS) || defined(FEATURE_CDMA_SMS) */

#ifdef FEATURE_DATA_GCSD_FAX
    ds_at_fps_t31_status_type fps_t31_status;
#endif  /* FEATURE_DATA_GCSD_FAX */

#if defined(FEATURE_DATA_WCDMA_PS) || defined(FEATURE_GSM_GPRS)
    ds_at_ps_data_gen_type    ps_data_gen;
#endif /* defined(FEATURE_DATA_WCDMA_PS) || defined(FEATURE_GSM_GPRS) */

#ifdef FEATURE_DSAT_EXT_CLIENT_SUPPORT
    ds_at_send_at_cmd_s_type  send_at_cmd;
#endif /* FEATURE_DSAT_EXT_CLIENT_SUPPORT */

    dsat_fwd_at_cmd_s_type        forward_at_cmd;
    ds_at_ext_client_resp_s_type  ext_at_resp;
    ds_at_reset_cmd_request_s_type  reset_req_cmd;
    ds_at_ext_client_urc_s_type   ext_at_urc;
#ifdef FEATURE_ECALL_APP
    ds_at_ecall_cmd_s_type        ecall_cmd;
#endif /* FEATURE_ECALL_APP */
    dsat_iface_info_s_type        iface_info;
#ifdef FEATURE_DATA_IS707
    ds707_pkt_call_info_type      ds707_data;
    ds707_epzid_socm_msg_type         ds707_epzid_socm;
    ds707_dorm_timer_socm_msg_type    ds707_dorm_socm;
    ds707_epzid_type      ds707_epzid;
#ifdef FEATURE_IS707B_ADDENDUM
   ds707_epzid_hat_type                 update_hat;
   ds707_epzid_get_hat_status_type      get_hat;
#endif /* FEATURE_IS707B_ADDENDUM*/
    /*- - -  - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - - - -
    ds707_curr_tx_wmk_ptr holds the pointer to the structure maintained for rev
    (tx) watermark. Used for flow control when reaching high and low wmk by
    posting command to DS with id DS_707_UM_TX_LO_WM_CMD and DS_707_UM_TX_HI_WM_CMD
    - - -  - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - - - -*/
    void           *ds707_curr_tx_wmk_ptr; /* type is ds707_wmk_buf_type */
    ds707_retry_delay_orig_type ds707_retry_delay_orig;
    boolean                     ds_707_not_in_service;
    ds707_cm_ph_event_type       ds707_cm_ph_event_info;

    ds707_bring_up_cmd_info_type ds707_bring_up_cmd_info;
    ds707_tear_down_cmd_info_type ds707_tear_down_cmd_info;

#endif /* FEATURE_DATA_IS707 */

#if defined(FEATURE_DATA_WCDMA_PS) || defined(FEATURE_GSM_GPRS) || defined(FEATURE_DATA_LTE)
    dsumtsps_call_info_type  dsumtsps_call_info;
    dsPdn_call_info_type     dsPdn_call_info;
    ds_3gpp_rmsm_at_type     rmsm_at_event_info;
    ds_3gpp_rmsm_at_flow_type  ds_3gpp_rmsm_flow_info;
    ds_3gpp_pdn_bearer_wm_type ds_3gpp_pdn_bearer_wm_info; 
#endif /* (FEATURE_DATA_WCDMA_PS) || (FEATURE_GSM_GPRS) || (FEATURE_DATA_LTE) */

    dsrlp_blob_cmd_type                          dsrlp_blob;

#if defined(FEATURE_DATA_WCDMA_PS) || defined(FEATURE_GSM_GPRS)
    uint8 dsumtsps_mt_index;                /*Index to the MT timer table  */
    dsumtsps_sys_chg_ss_info_type   dsumtsps_sys_chg_ss_info;
    dsumtsps_pdp_ip_irat_tx_um_type dsumtsps_pdp_ip_irat_tx_um_info;
#endif /* FEATURE_DATA_WCDMA_PS || FEATURE_GSM_GPRS */

#ifdef FEATURE_GSM_GPRS
    dsumtsps_gsndcp_reg_cnf_ev_type dsumtsps_gsndcp_reg_cnf_ev;

    ds3gpp_gsndcp_reg_cnf_ev_type ds3gpp_gsndcp_reg_cnf_ev;

#endif /* FEATURE_GSM_GPRS */

    ds_release_call_ind_type  end_params;

    ds_orig_call_type         orig_params;

#if defined(FEATURE_DATA_WCDMA_PS) || defined(FEATURE_GSM_GPRS) || \
    defined(FEATURE_DATA_LTE)
    dsPdn_incom_call_info_type    dsPdn_incom_call_info;

    struct
    {
      int rm_dev_instance;
      int sm_instance;
    }dsumts_rmsm_instance;

    ds_3gpp_rmsm_autoconfig_sm_type autoconfig_info;

    dsumts_rmsm_dun_ctrl_params_type    dun_ctrl_params;
#endif /*(FEATURE_DATA_WCDMA_PS)||(FEATURE_GSM_GPRS) ||(FEATURE_DATA_LTE) */

#if ((defined(FEATURE_DATA_WCDMA_CS)) || (defined(FEATURE_DATA_GCSD)))
    ds_ucsd_app_orig_call_cmd_type   ucsd_app_orig_params;
                                               /* UMTS CS Data application **
                                               ** origination parameters   */
    ds_ucsd_app_answer_call_cmd_type ucsd_app_answer_params;
                                               /* UMTS CS Data application **
                                               ** answer parameters        */
    ds_ucsd_app_confirm_call_cmd_type ucsd_app_confirm_params;
                                               /* UMTS CS Data application **
                                               ** setup confirm parameters */
#ifdef FEATURE_MULTIMEDIA_FALLBACK
    #error code not present
#endif /* FEATURE_MULTIMEDIA_FALLBACK */

    ds_ucsd_app_end_call_cmd_type    ucsd_app_end_params;
                                               /* UMTS CS Data application **
                                               ** end parameters           */
    ds_ucsd_handover_cmd_type        ucsd_handover_params;
                                               /* UMTS CS Data inter-RAT   **
                                               ** handover parameters      */

#ifdef FEATURE_UUS
    ds_ucsd_uus_data_cmd_type         ucsd_uus_data_params; /* UUS1 data   */
#endif /* FEATURE_UUS */

#endif /* FEATURE_DATA_WCDMA_CS || FEATURE_DATA_GCSD */

    /*- - -  - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - - - -
     Command type used to send BCMCS commands from PS, BCRMSM
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    ps_iface_ioctl_mcast_join_type   bcmcs_flow_join;

    ps_iface_ioctl_mcast_leave_type   bcmcs_flow_leave;
#ifdef FEATURE_HDR_BCMCS_2p0
    #error code not present
#endif /* FEATURE_HDR_BCMCS_2p0*/

#ifdef FEATURE_HDR_BCMCS
    /*- - -  - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - - - -
     Command type used to send BCMCS commands from CM
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    dsbcmcs_cm_cmd_type                bcmcs_event_info;
#endif /* FEATURE_HDR_BCMCS */

#ifdef FEATURE_MBMS
    #error code not present
#endif /* FEATURE_MBMS */

#if defined(FEATURE_MFLO) && defined(FEATURE_MFLO_FLOMCS)

    dsflobcmcs_flo_stack_cmd_type      flo_event_info;
    ps_iface_ioctl_mcast_join_type     flo_flow_join;
    ps_iface_ioctl_mcast_leave_type    flo_flow_leave;

#endif /* defined(FEATURE_MFLO) && defined(FEATURE_MFLO_FLOMCS) */
#ifdef FEATURE_HDR_QOS

    /*- - -  - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - - - -
     Command types used to send Qos notifications to flow mgr.
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
   ds707_qos_flow_mgr_resv_on_notify_type         qos_resv_on_notify;
   ds707_qos_flow_mgr_resv_off_notify_type        qos_resv_off_notify;
   ds707_qos_flow_mgr_reservation_on_sent_notify_type  qos_resv_on_sent_notify;

   /*- - -  - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - - - -
    Command types used to send notifications regarding a sec flow status
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
   ds707_sec_pkt_flow_cmd_type                  sec_flow_cmd;

    /*- - -  - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - - - -
     Command type used to send RESV response notification to sec pkt mgr
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    ds_dqos_mgr_resv_resp_notify_cmd_type        resv_resp_notify;

    /*- - -  - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - - - -
     Command type used to tell dqos_mgr that rexmit timer is expired
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    timer_cb_data_type                           resv_rexmit_timer_handle;

   /*- - -  - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - - - -
    Command type used to PPP down notification to sec pkt mgr
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    ds707_sec_pkt_ppp_cmd_type                   ppp_down_cmd;

   /*- - -  - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - - - -
    Command type used to send Iface related events to Sec Pkt Mgr.
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    ds707_sec_pkt_iface_event_type               iface_event_info;

#endif /* FEATURE_HDR_QOS */

    ds_ppp_ev_cmd_type                 ppp_ev_info;
    boolean                            evdo_colocated;
                                       /* Evdo colocation info for CAM     */
    ds_v6_call_status_type             v6_call_status;
         /* PPP/3GPP status to propogate call failues to DCTM/ IFACE layer */

#ifdef FEATURE_DATA_IS707
    ds_mip_ppp_status                  mip_ppp_status;
                          /* MIP/PPP status to report call failues to DCTM */
#ifdef FEATURE_DS_LN_UPDATE
    sys_hdr_location_notification_status_e_type        ln_status;
              /* LN Status (Success or Failure)  */
#endif /*FEATURE_DS_LN_UPDATE*/

#endif /* FEATURE_DATA_IS707 */

#ifdef FEATURE_EPC_HANDOFF
    void*                           ds_epc_cmd_user_data_ptr; 
#endif /*FEATURE_EPC_HANDOFF*/

#ifdef FEATURE_DSAT_GOBI_MAINLINE
    ftm_pkt_type   ftm_diagpkt;
#endif /* FEATURE_DSAT_GOBI_MAINLINE */

    uint32               *user_data_ptr; /* Pointer to any user  data      */
    uint32                timer_val;

    cm_rtre_control_e_type    rtre_control; /* Field indicating to read from RUIM */

#ifdef FEATURE_DATA_TEST_LOOPBACK
    ds_loopback_hdlr_cmd_info_type     loopback_hdlr_cmd_info;
#endif /* FEATURE_DATA_TEST_LOOPBACK */

    ds_3gpp_flow_manager_cmd_info_type  ds_3gpp_flow_cmd_info;
    ds_3gpp_flow_lower_layer_cmd_info_type  ds_3gpp_flow_lower_layer_info;
    ds_3gpp_setup_bridge_cmd_info_type      ds_3gpp_setup_bridge_cmd_info;
#ifdef FEATURE_DATA_LTE
    ds_3gpp_tlb_cmd_info_type            ds_3gpp_tlb_cmd_info;
#endif /* FEATURE_DATA_LTE  */

#ifdef FEATURE_DATA_RPM
    ds_3gpp_rpm_info_type  rpm_info;
#endif /* FEATURE_DATA_RPM */
    sys_roam_status_e_type roam_status; /* Device's roam status */

#ifdef FEATURE_EHRPD
    uint32 pdn_context_cb; /* ref: DS_707_PDN_INACTIVITY_TIMER_CMD */
#endif /* FEATURE_EHRPD */

#ifdef FEATURE_DATA_EMBMS
    struct
    {
      ps_iface_embms_tmgi_activation_info_type   ind_info;
      uint8                                      index;
    } embms_tmgi_act_ind_info;

    ps_iface_embms_tmgi_deactivation_info_type embms_tmgi_deact_ind_info;
#endif

#ifdef FEATURE_DATA_OPTHO
    ds707_s101_process_cmd_info_type s101_cmd_info;
#endif /* FEATURE_DATA_OPTHO */

    ds707_call_throttle_info call_thr_info;
    ps_iface_net_down_reason_type call_end_reason;
    ps_sys_ioctl_dun_ctrl_response_type dun_ctrl_sr_response;

    ds_cm_ac_cmd_state_info_s_type cm_ac_cmd_state_info;
    
#ifdef FEATURE_DATA_WCDMA_PS
    ds3gpp_purge_cnf_ev_type    ds3gpp_purge_cnf_ev;
#endif /* FEATURE_DATA_WCDMA_PS */

#ifdef FEATURE_DATA_WLAN_MAPCON 
   void*                           ds_wlan_proxy_cmd_user_data_ptr; 
#endif /*FEATURE_DATA_WLAN_MAPCON*/

#ifdef FEATURE_DATA_IWLAN_S2B
    void*                          ds_iwlan_s2b_pdn_context_cb_ptr;
    void*                          ds_iwlan_s2b_ikev2_cback_info_ptr;
#endif /* FEATURE_DATA_IWLAN_S2B */
#ifdef FEATURE_DATA_LTE
    ps_sys_ioctl_lte_data_retry_type    lte_data_retry_status;
#endif /*FEATURE_DATA_LTE*/

#if defined (FEATURE_DATA_A2_DL_BRIDGE) && defined (FEATURE_DATA_LTE)
    ds_3gpp_iface_bridge_s_type  ds_3gpp_iface_bridge_s;
#endif
    ds_3gpp_ul_rab_info_type  ds_3gpp_ul_rab_info_s;
    ds_3gpp_roaming_ind_s_type roaming_ind_s;

#ifdef FEATURE_DATA_LTE
    ds_3gpp_apn_param_chg_type  apn_param_chg_s;
#endif

#if defined (FEATURE_DATA_A2_DL_BRIDGE) && defined (FEATURE_DATA_LTE)
    ds_3gpp_switch_data_path_type  switch_data_path_info;
#endif /* (FEATURE_DATA_A2_DL_BRIDGE) && defined (FEATURE_DATA_LTE) */

  } cmd;

} ds_cmd_type;


/*===========================================================================

                      PUBLIC FUNCTION DECLARATIONS

===========================================================================*/


/*===========================================================================

FUNCTION DS_GET_CMD_BUF

DESCRIPTION
  This function allocates a command buffer from the Data Services Task. The
  The caller of this function must check for a NULL return value, and either
  perform some error recovery or ERR_FATAL, as appropriate.

DEPENDENCIES
  None

RETURN VALUE
  Returns a pointer to a command buffer, if available, or NULL if no command
  buffers are available.

SIDE EFFECTS
  None

===========================================================================*/

extern ds_cmd_type  *ds_get_cmd_buf( void );


/*===========================================================================

FUNCTION DS_RELEASE_CMD_BUF

DESCRIPTION
  This function returns a command buffer to the Data Services Task's
  free queue.

DEPENDENCIES
  The caller of this function should have previously allocated a command
  buffer by calling ds_get_cmd_buf().

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/

extern void  ds_release_cmd_buf
(
  ds_cmd_type    **cmd_pptr           /* Pointer to command to be released */
);

/*===========================================================================
FUNCTION DS_GET_CMD_QUEUE_CNT

DESCRIPTION
  This function returns the count of outstanding commands that are in DS 
  command queue.

DEPENDENCIES
  None

RETURN VALUE
  Value of type uint8 - Indicates number of outstanding commands in DS command 
                        queue.

SIDE EFFECTS
  None
===========================================================================*/
uint8 ds_get_cmd_queue_cnt( void );

/*===========================================================================

FUNCTION DS_GET_CUR_FREE_CMD_CNT

DESCRIPTION
  This function returns the current count of free command buffers that are
  available in the DS task.

DEPENDENCIES
  None

RETURN VALUE
  Returns a uint8 with the number of outstanding commands in the dstask.

SIDE EFFECTS
  None

===========================================================================*/
uint8 ds_get_cur_free_cmd_cnt( void );


/*===========================================================================

FUNCTION DS_PUT_CMD_EXT2

DESCRIPTION
  This function puts a command on the Data Services Task's command queue, and
  sets the DS_CMD_Q_SIG. The command is put on the regular external command
  queue regardless of the calling task context, to preserve legacy behavior.

DEPENDENCIES
  The caller of this function should have previously allocated a command
  buffer by calling ds_get_cmd_buf().

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
void ds_put_cmd_ext2
(
  ds_cmd_type    *cmd_ptr                 /* Pointer to command to be sent */
);

/*===========================================================================

FUNCTION DS_PUT_CMD

DESCRIPTION
  This function puts a command on the Data Services Task's command queue, and
  sets the DS_CMD_Q_SIG. The command is put on the regular external command
  queue regardless of the calling task context, to preserve legacy behavior.

DEPENDENCIES
  The caller of this function should have previously allocated a command
  buffer by calling ds_get_cmd_buf().

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/

extern void  ds_put_cmd
(
  ds_cmd_type    *cmd_ptr                 /* Pointer to command to be sent */
);

/*===========================================================================

FUNCTION DS_SEND_CMD

DESCRIPTION
  This function puts a command on the Data Services Task's command queue, and
  sets the DS_CMD_Q_SIG. The command is put on the regular external command
  queue regardless of the calling task context, to preserve legacy behavior.

DEPENDENCIES
  A valid command need to be passed

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/

void  ds_send_cmd
(
  ds_cmd_enum_type  cmd_id,               /* Pointer to command to be sent */
  void             *user_data_ptr         /* User specific data            */
);

/*===========================================================================

FUNCTION DS_PUT_CMD_EXT

DESCRIPTION
  This function puts a command on the Data Services Task's command queue, and
  sets the DS_CMD_Q_SIG. If the calling task is DS task itself, then command
  is put on the internal (i.e. high priority) command queue, otherwise the
  command is put on the regular external command queue.

DEPENDENCIES
  The caller of this function should have previously allocated a command
  buffer by calling ds_get_cmd_buf().

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
extern void  ds_put_cmd_ext
(
  ds_cmd_type    *cmd_ptr                 /* Pointer to command to be sent */
);

/*===========================================================================

FUNCTION DS_PUT_CMD_INT

DESCRIPTION
  This function puts a command on the Data Services Task's internal command queue,
  and sets the DS_CMD_Q_SIG.

DEPENDENCIES
  The caller of this function should have previously allocated a command
  buffer by calling ds_get_cmd_buf().

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/

extern void  ds_put_cmd_int
(
  ds_cmd_type    *cmd_ptr                 /* Pointer to command to be sent */
);

/*===========================================================================

FUNCTION DS_SEND_CMD_EXT

DESCRIPTION
  This function puts a command on the Data Services Task's command queue, and
  sets the DS_CMD_Q_SIG. If the calling task is DS task itself, then command
  is put on the internal (i.e. high priority) command queue, otherwise the
  command is put on the regular external command queue.

DEPENDENCIES
  A valid command need to be passed

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
void  ds_send_cmd_ext
(
  ds_cmd_enum_type  cmd_id,               /* Pointer to command to be sent */
  void             *user_data_ptr         /* User specific data            */
);

#ifdef FEATURE_DATA_MUX_USB_TEST
/*===========================================================================

FUNCTION DS_IS_MUX_NV_ITEM_SET

DESCRIPTION
  This function sreturns true if NV item 6873 is set to MUX ports
DEPENDENCIES
  None

RETURN VALUE
  TRUE  - true if nv item has mux port
  FALSE - false if NV item doesnt have mux ports

SIDE EFFECTS
  None

===========================================================================*/
boolean ds_is_mux_nv_item_set(void);
#endif

#endif /* DSTASK_V_H */
