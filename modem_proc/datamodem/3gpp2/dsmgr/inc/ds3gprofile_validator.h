
#ifndef DS3PROFILE_VALIDATOR_H
#define DS3PROFILE_VALIDATOR_H
/*===========================================================================

               3 G   D A T A   S E R V I C E S   M A N A G E R

           M M G S D I   I N T E R F A C E   F U N C T I O N S

                            H E A D E R    F I L E

DESCRIPTION
  This file contains functions for interfacing to MMGSDI, including
  Data Services client initialization and processing of MMGSDI events. This
  header file is intended to be used internal to the 3G Dsmgr module only.


  Copyright (c) 2008-2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
===========================================================================*/


/*===========================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/3gpp2/dsmgr/inc/ds3gprofile_validator.h#1 $ $DateTime: 2015/10/05 22:49:46 $ $Author: teralak $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
12/05/2014 teralak      introduce this module to validate apn name for 3gpp spec

===========================================================================*/



/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"

/*===========================================================================

                     MACRO

===========================================================================*/
#define DS3G_PROFILE_OPERATOR_IDENTIFIER_TOTAL_LENGTH 19
#define DS3G_PROFILE_NETWORK_IDENTIFIER_MAX_LENGTH 62
#define DS3G_PROFILE_MCC_MNC_STR_LENGTH_WITH_DELIM 4
#define DS3G_PROFILE_GPRS_STR_LENGTH_WITH_DELIM 5
#define DS3G_PROFILE_MCC_MNC_STR_LENGTH 3
#define DS3G_PROFILE_GPRS_STR_LENGTH 4
#define DS3G_PROFILE_MCC_MNC_LABEL_LENGTH 6
#define DS3G_PROFILE_GPRS_LABEL_LENGTH 4
#define DS3G_PROFILE_RAC_LAC_RNC_STR_LENGTH 3
#define DS3G_PROFILE_SGSN_STR_LENGTH 4
#define DS3G_PROFILE_MCC_MNC_LENGTH_WITH_DELIM 7

#define DS3G_PROFILE_FQDN_OPERATOR_IDENTIFIER_TOTAL_LENGTH 38
#define DS3G_PROFILE_APN_STR_LENGTH 3
#define DS3G_PROFILE_APN_STR_LENGTH_WITH_DELIM 4
#define DS3G_PROFILE_EPC_STR_LENGTH 3
#define DS3G_PROFILE_EPC_STR_LENGTH_WITH_DELIM 4
#define DS3G_PROFILE_3GPPNETWORK_STR_LENGTH 11
#define DS3G_PROFILE_3GPPNETWORK_STR_LENGTH_WITH_DELIM 12
#define DS3G_PROFILE_ORG_STR_LENGTH 3
#define DS3G_PROFILE_ORG_STR_LENGTH_WITH_DELIM 4



/*===========================================================================

                     FUNCTION DELCARATION

===========================================================================*/


/*===========================================================================
FUNCTION DS3G_GET_NETWORK_IDENTIFER_FROM_APN_NAME

DESCRIPTION
  This function is used to get network identifier part from apn name stored in the profile. This
  function leaves out the operator identifier. It picks up only the labels
  that reflect the network identifier.

PARAMETERS
  IN
  input_apn - Pointer to the APN in the profile. 
  input_apn_length - length of the apn string

  OUT
  network_identifier - Pointer to the byte array in which the network identifier of APN is returned

DEPENDENCIES
  None

RETURN VALUE
  boolean - TRUE if extraction is successful
            FALSE if there is any error
   
SIDE EFFECTS
  None
===========================================================================*/
uint8 ds3g_get_network_identifer_from_apn_name
(
  char*  input_apn,
  byte   input_apn_length,
  char*  network_identifier
);


/*===========================================================================
FUNCTION DS3G_PROFILE_APN_IS_NETWORK_IDENTIFIER_VALID

DESCRIPTION
  This functions checks if network identifier is fully compliant with 3gpp spec.

PARAMETERS
  IN
  apn_ni - Pointer to the network identifier in the profile. 
  apn_ni_len - length of the network identifier string


DEPENDENCIES
  None

RETURN VALUE
  boolean - TRUE if network indentifier is valid
            FALSE  otherwise
   
SIDE EFFECTS
  None
===========================================================================*/

boolean ds3g_profile_apn_is_network_identifier_valid
(
   byte*  apn_ni,
   uint8 apn_ni_len
);

/*===========================================================================
FUNCTION DS3G_PROFILE_VALIDATE_APN_NAME

DESCRIPTION
  The function validates apn name according to 3gpp spec
    
PARAMETERS: 
  *apn_name  --  apn name char pointer
  *info     --  apn name lenght
   
 
DEPENDENCIES
  None.
  
RETURN VALUE 
  TRUE - if apn name is valid
  FALSE  - otherwise
  
SIDE EFFECTS 
  None 
  
===========================================================================*/

boolean ds3g_profile_validate_apn_name
(
  char *apn_name,
  uint8 apn_name_len
);


/*===========================================================================
FUNCTION DS3G_DECODE_APN_NAME

DESCRIPTION
  This function is used to decode the APN name returned by the network. This
  function leaves out the operator identified. It picks up only the labels
  that reflect the network identifier.

PARAMETERS
  IN
  encoded_apn - Pointer to the APN returned by the network. This is encoded
  encoded_apn_length - length of the encoded string

  OUT
  decoded_apn - Pointer to the byte array in which the decoded APN is returned

DEPENDENCIES
  None

RETURN VALUE
  boolean - TRUE if decoding is successful
            FALSE if there is any error
   
SIDE EFFECTS
  None
===========================================================================*/
boolean ds3g_decode_apn_name
(
  byte*  encoded_apn,
  uint32 encoded_apn_length,
  byte*  decoded_apn
);



#endif /* DS3PROFILE_VALIDATOR_H */