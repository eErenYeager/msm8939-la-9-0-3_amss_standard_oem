#ifndef DS3GUTIL_H
#define DS3GUTIL_H
/*===========================================================================

                      D S 3 G U T I L . H
 
GENERAL DESCRIPTION
  Utility functions for DS module

Copyright (c) 2004-2014 by QUALCOMM Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE
This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/3gpp2/dsmgr/inc/ds3gutil.h#2 $ $DateTime: 2015/11/25 21:19:52 $ $Author: c_vjhunj $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
10/15/13    sgd    Add ds3g_util_init to register callback functions

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

#include "comdef.h"
#include "customer.h"
#include "ps_sys.h"
#include "ds_dsd_ext_i.h"

/*-------------------------------------------------------------------------
  PAP and CHAP definitions used by 3GPP and 3GPP2 MH
-------------------------------------------------------------------------*/
#define DS3G_PDN_AUTH_PAP   0x0
#define DS3G_PDN_AUTH_CHAP  0x1

/*===========================================================================

FUNCTION DS3G_UTIL_INIT

DESCRIPTION
  This function is used to initialize the ds3g util module
  during power up 
  
DEPENDENCIES
  None

RETURN VALUE
  NONE

SIDE EFFECTS
  None

===========================================================================*/
void ds3g_util_init
(
  void
);

/*===========================================================================
FUNCTION       DS3G_CONVERT_DSD_RADIO_INFO

DESCRIPTION    
  This is the function to convert dsd radio info to ps status

PARAMETERS
  radio_idx - pref radio index
  *dsd_radio_info - dsd radio info
  subs_id - subscription id
  *dsd_sys_info - output pointer

DEPENDENCIES
  NONE

RETURN VALUE  
  NONE

SIDE EFFECTS   
===========================================================================*/
void ds3g_convert_dsd_radio_info
(
  dsd_radio_type                   radio_idx,
  ds_dsd_ext_radio_info_type      *dsd_radio_info,
  ps_sys_subscription_enum_type    ps_subs_id,
  ps_sys_system_status_info_type  *dsd_sys_info
);

/*===========================================================================
FUNCTION      DS3G_GET_CONFIG_PDN_LEVEL_AUTH

DESCRIPTION   Returns config_pdn_level_auth NV item

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
uint8 ds3g_get_config_pdn_level_auth
(
  sys_modem_as_id_e_type  subs_id
);

/*===========================================================================
FUNCTION      DS3G_GET_SUPPRESS_GSM_ON_SRVCC_CSFB

DESCRIPTION   Returns Suppress GSM on SRVCC/CSFB NV item

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
boolean ds3g_get_suppress_gsm_on_srvcc_csfb
(
  sys_modem_as_id_e_type  subs_id
);

/*===========================================================================
FUNCTION      DS3G_GET_CONFIG_LATENCY_INFO

DESCRIPTION   Returns config latency info

DEPENDENCIES  None

RETURN VALUE  TRUE/FALSE - return Latency Info nv value

SIDE EFFECTS  None
===========================================================================*/
boolean ds3g_get_config_latency_info(void);

/*===========================================================================
FUNCTION      ds3g_util_read_nv

DESCRIPTION   This function reads all the ds3g specific NV items
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds3g_util_read_nv( void );

#endif /* DS3GUTIL_H */

