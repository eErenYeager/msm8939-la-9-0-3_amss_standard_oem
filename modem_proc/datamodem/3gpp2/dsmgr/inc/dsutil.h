#ifndef DSUTIL_H
#define DSUTIL_H
/*===========================================================================

                      D S U T I L . H
 
GENERAL DESCRIPTION
  Utility functions for DS module

EXTERNALIZED FUNCTIONS
  DSUTIL_ATOI()
    Converts an ASCII string to an integer
  DS_EFS_FILE_INIT()
    Intializes the state machine and also opens the file
  DS_EFS_FILE_CLOSE()
    Closes file and releases the state machine 
  DS_EFS_TOKENIZER()
    Reads from the EFS file and returns a token
  DS_CREATE_EFS_CONFIG_FILE()
    Create the EFS config file which allows QPST to back up item files in QCNs.
    The config file will be empty until ds_create_efs_item_file() is called.
  DS_CREATE_EFS_ITEM_FILE()
    Places the path of an item file into the specified config file. This is
    used by QPST to backup the EFS item for QCN files. If the config file was
    not set up properly this function will exit with the appropriate efs_errno.
  DS_READ_EFS_NV()
    Reads the EFS item based on the item file path passed in.
  DS_WRITE_EFS_NV()
    Writes to the EFS item based on the item file path passed in.

INITIALIZATION AND SEQUENCING REQUIREMENTS
  USING EFS ITEMS
    1. DS_CREATE_EFS_CONFIG_FILE() should be called in a powerup function.
       Only need to call this once per task. Each task shall have a
       different config file. DS task calls this function in dstask.c
    2. DS_CREATE_EFS_ITEM_FILE() should be called in component specific
       init function. This function will append an efs item file path into
       the config file.
       For DS task, use "/nv/item_files/conf/data_config_info.conf" for config
       file location
    3. When the EFS item wants to be read, DS_READ_EFS_NV() is called.
       The item file path is used to locate the item file in the EFS.
       A data structure/data type (passed by reference) will be filled with
       the value of the EFS item.
    4. If the read call returns with a ENOENT (file does not exist), then
       user will need to call DS_WRITE_EFS_NV() with default values passed in.

Copyright (c) 2004-2014 by QUALCOMM Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE
This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/3gpp2/dsmgr/inc/dsutil.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
08/13/12    ss     Adding ds_init_efs_config_file() to populate values in 
                   data config file at bootup.
01/31/12    msh    Coan: Feature cleanup 
01/02/12    msh    Coan: Feature cleanup
09/05/11    ss     Changes to support RPM.
08/09/11    sha    Global Variable Clean Up.
05/05/11    ack    Moving ds_path_is_directory declaration to .h file
03/23/11    sn     Globals cleanup for free floating of tasks.
02/16/11    op     Klocwork fix and comments cleanup
01/17/11    ss     Added support for Iface linger.
09/07/10    op     Added functions to use EFS items.
07/18/10    mg     Added functions for reading EFS file for PDN throttling 
                   feature
04/16/10    ls     Merge efs functions into this file
05/16/04    vr     Initial revision.

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

#include "comdef.h"
#include "customer.h"
#include "fs_public.h"
#include "fs_sys_types.h"
#include "fs_fcntl.h"
#include "fs_errno.h"
#include "nv.h"
#include "ds3gmgr.h"
#include "rex.h"
/*-----------------------------------------------------------------------------
   
-----------------------------------------------------------------------------*/
#define DS_EFS_READ_BUFFER_SZ 128

/*---------------------------------------------------------------------------
  Converting a value in seconds to mili seconds
---------------------------------------------------------------------------*/
#define SEC_TO_MSEC(val_sec) ((val_sec) * 1000)

/*---------------------------------------------------------------------------
EFS txt file that contains the configurable keep alive APN name
---------------------------------------------------------------------------*/
#define                            DS_EPS_KAMGR_PDN     \
                                 "/data/ds_eps_kamgr_pdn.txt"

/*===========================================================================
MACRO MINUTE_TO_MSEC()

DESCRIPTION
  This macro returns a msec value for the give value in minutes

PARAMETERS
  int16 - value in minutes

RETURN VALUE
  value in msec
===========================================================================*/
#define MINUTE_TO_MSEC(val_minute)                        \
  ((val_minute) * 60000)

#define DSUTIL_BYTE_TO_BITS(byte)                       ((byte) << (3))
#define DSUTIL_BITS_TO_KBITS(total_bits)                ((total_bits) >> (10))
#define DSUTIL_BYTES_TO_KBITS(byte)                     ((byte) >> (7))
/* -----------------------------------------------------------------------
** Constants
** ----------------------------------------------------------------------- */

#ifndef MIN_INT8
#define MIN_INT8 -128
#endif
#ifndef MIN_INT16
#define MIN_INT16 -32768
#endif
#ifndef MIN_INT32
#define MIN_INT32 (~0x7fffffff)   /* -2147483648 is unsigned */
#endif
#ifndef MIN_INT64
#define MIN_INT64 (~0x7fffffffffffffffLL) /* -9223372036854775808 is unsigned */
#endif

#ifndef MAX_INT8
#define MAX_INT8 127
#endif
#ifndef MAX_INT16
#define MAX_INT16 32767
#endif
#ifndef MAX_INT32
#define MAX_INT32 2147483647
#endif
#ifndef MAX_INT64
#define MAX_INT64 9223372036854775807LL
#endif

#ifndef MAX_UINT8
#define MAX_UINT8 255
#endif
#ifndef MAX_UINT16
#define MAX_UINT16 65535
#endif
#ifndef MAX_UINT32
#define MAX_UINT32 4294967295u
#endif
#ifndef MAX_UINT64
#define MAX_UINT64 18446744073709551615uLL
#endif

/*===========================================================================
                                TYPEDEFS
===========================================================================*/
/*-----------------------------------------------------------------------------
  Structure used to parse the efs file
-----------------------------------------------------------------------------*/
typedef struct
{
    int read_buffer_size;           /*  For efs_read this value is DS_EFS_READ_BUFFER_SZ
                                    For efs_get this value is got from efs_stat */
    char seperator;                  /*  Seperator(;) to be parsed for       */
    char *buffer;                   /*  Buffer to read the file into   */
    char *curr;                     /*  pointer to the current location      */
    char *end_pos;                  /*  ponter to the end of the buffer      */
    char *efs_file_path;            /*  Use this when doing an efs_get              */
    boolean eof;                    /*  used to indicate the end of file     */
    boolean skip_line;              /*  identifies  comments in the file     */
    boolean eol;                    /*  used to indicate end of line         */
    boolean bol;                    /*  used to indicate begining of the line*/
    boolean efs_get_performed;       /*  Variable to know if an efs_get has been performed*/

}ds_efs_token_type ; 

/*-----------------------------------------------------------------------------
  Enum to specify the various return values 
  SUCCESS : Success
  EOL     : End of line is reached => record end
  EOF     : End of file is reached => file end => feof
  FAILURE : Failed 
-----------------------------------------------------------------------------*/
typedef enum 
{
  DS_EFS_TOKEN_PARSE_SUCCESS  = 0,
  DS_EFS_TOKEN_PARSE_EOL      = 1,
  DS_EFS_TOKEN_PARSE_EOF      = 2,
  DS_EFS_TOKEN_PARSE_FAILURE  = 3,

  DS_EFS_TOKEN_PARSE_MAX
}ds_efs_token_parse_status_enum_type;

#define DS_PDN_THROTTLE_MAX_FAILURE_TIMERS 10  /* max number of failure timers */
#define DS_PDN_THROTTLE_MAX_DISALLOW_TIMERS 6  /* max number of disallow timers */

/*-----------------------------------------------------------------------------
  Structure used to store the PDN config values from EFS
-----------------------------------------------------------------------------*/
typedef struct
{
  uint32 failure_timer[DS_PDN_THROTTLE_MAX_FAILURE_TIMERS];   /* Failure Timer  */
  uint32 disallow_timer[DS_PDN_THROTTLE_MAX_DISALLOW_TIMERS]; /* Disallow Timer */
}ds_pdn_throttle_config_type;

/*---------------------------------------------------------------------------
  Critical section macros for Data Services
---------------------------------------------------------------------------*/
#define DS_INIT_CRIT_SECTION( rex_crit_section )                            \
  { rex_init_crit_sect( rex_crit_section ); }

#define DS_ENTER_CRIT_SECTION( rex_crit_section )                           \
  { rex_enter_crit_sect( rex_crit_section ); }

#define DS_LEAVE_CRIT_SECTION( rex_crit_section )                           \
  { rex_leave_crit_sect( rex_crit_section ); }

/*===========================================================================

MACRO          DSUTIL_ATOI

DESCRIPTION    Converts the character input to integer type

               This takes a begin and end pointer to a string and converts
               a decimal number in string format starting at begin pointer
               to the corresponding integer value. 

               It stops converting when either a non-numeric value or the
               end pointer is encountered. On returning from this macro,
               begin pointer is set to where the conversion stopped.

               For eg: If the string passed is "123 45 a 7" then on returning
               from this macro, the output integer will be 123 and begin 
               pointer will be incremented by 3. If the macro is called
               again with "45 a 7", the output integer will be 45 and begin
               pointer will be incremented by 2. If the macro is now called
               once again with "a 7", the output integer will be 0 and begin
               pointer will be left unchanged.
                        
PARAMETERS     x : Begin pointer (char *)
               y : End pointer (char *)
               z : ouput integer

DEPENDENCIES   NONE

RETURN VALUE   NONE

SIDE EFFECTS   Begin pointer is incremented to point beyond the end of the
               first number in the string
===========================================================================*/
#define DSUTIL_ATOI(x, y, z)                  \
        z = 0;                                \
        while( x < y )                        \
        {                                     \
          if((*x) >= '0' && (*x) <= '9')      \
          {                                   \
            z = z *10 + (*x- '0');            \
            x++;                              \
          }                                   \
          else                                \
          {                                   \
            break;                            \
          }                                   \
        }

/*===========================================================================
FUNCTION      DS_EFS_FILE_INIT

DESCRIPTION   The function intializes the state machine and 
              also opens the file

DEPENDENCIES  None.

RETURN VALUE 
              0  : SUCCESS: The file is good, readable, 
                            State Machine Initialized.
              -1 : FAILURE: The file cannot be opened/ readable. 

SIDE EFFECTS  None.
===========================================================================*/
extern int ds_efs_file_init
(
  const char *file_path, 
  ds_efs_token_type *sm 
);

/*===========================================================================
FUNCTION      DS_EFS_FILE_CLOSE

DESCRIPTION   The function closes file and releases the state machine 

DEPENDENCIES  The file should have opened already.

RETURN VALUE  NONE

SIDE EFFECTS  None.
===========================================================================*/
extern void ds_efs_file_close
(
   ds_efs_token_type *sm 
);

/*===========================================================================
FUNCTION      DS_EFS_TOKENIZER

DESCRIPTION   The function reads from the EFS file and returns a token.
              *begin and *end point to the begin and end of the toekn when
              the function returns.

DEPENDENCIES  The file should have opened already.

RETURN VALUE  NONE

SIDE EFFECTS  None.
===========================================================================*/
ds_efs_token_parse_status_enum_type ds_efs_tokenizer
(
  ds_efs_token_type *sm,
  char **begin,
  char **end
);

#ifndef NUM_FIELDS
#define NUM_FIELDS 10
#endif

#ifndef MAX_FIELD_LEN
#define MAX_FIELD_LEN 64
#endif

/*===========================================================================
FUNCTION      DS_EFSIF_CTOI

DESCRIPTION   The function converts char to int

DEPENDENCIES  None

RETURN VALUE  Return a integer value

SIDE EFFECTS  None.
===========================================================================*/
__inline int ds_efsif_ctoi
( 
  char c
);

/*===========================================================================
FUNCTION      DS_EFSIF_HEXATOI

DESCRIPTION   The function converts a hexadecimal string to decimal equivalent

DEPENDENCIES  None

RETURN VALUE  Return decimal equivalent

SIDE EFFECTS  None.
===========================================================================*/
int ds_efsif_hexatoi
(
  const char *x
);

/*===========================================================================
FUNCTION      DS_EFSIF_READ_A_RECORD

DESCRIPTION   Read a record including number of fields

DEPENDENCIES  None.

RETURN VALUE  Status to indicate if failed or Success or others
             
SIDE EFFECTS  None.
===========================================================================*/
ds_efs_token_parse_status_enum_type ds_efsif_read_a_record
( 
  ds_efs_token_type *sm, 
  char rec[][MAX_FIELD_LEN],
  uint16 num_fields,
  uint16 *valid_fields 
);

/*===========================================================================
FUNCTION      DS_READ_PDN_THROTTLE_CONFIG_FROM_EFS

DESCRIPTION   This function will read from the efs file all the necessary 
              data & fill the PDN throttle config structure. Only valid data 
              is populated.
              EFS File Format - Param_Name:Param_Val;
              For example     - FAILURE_TIMER:12;             

PARAMETERS    None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds_read_pdn_throttle_config_from_efs (void);
/*===========================================================================
FUNCTION      DS_READ_CC_SP_THROTTLE_CONFIG_FROM_EFS

DESCRIPTION   This function will read from the efs file all the necessary 
              data & fill the PDN throttle config structure. Only valid data 
              is populated.
              EFS File Format - cause_code:max_fail_count:Param_Val;
              For example     - 29:2:12;             

PARAMETERS    None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds_read_cc_sp_throttle_config_from_efs(void);
/*===========================================================================
FUNCTION      DS_CC_SP_THROTTLE__IS_CAUSE_CODE_CONFIGURED

DESCRIPTION   This function return true if given cause code is configured in 
              cc_sp_throttle_timer.txt file

PARAMETERS    cause code - PDP activation reject cause

RETURN VALUE  true- if cause code configured, false otherwise

SIDE EFFECTS  None
===========================================================================*/
boolean ds_cc_sp_throttle_is_cause_code_configured(uint16 cause_code);

/*===========================================================================
FUNCTION      DS_GET_CC_SP_THROTTLE__TIMER

DESCRIPTION   This function provides the PDN throttling failure timer value

PARAMETERS    counter - Throttling counter used as an index

RETURN VALUE  PDN throttle Failure Timer value in msec

SIDE EFFECTS  None
===========================================================================*/
uint32 ds_get_cc_sp_throttle_timer (uint16 cause_code, uint8 counter);
/*===========================================================================
FUNCTION      ds_get_plmn_unblock_timer_val

DESCRIPTION   This function provides the PDN throttling failure timer value

PARAMETERS    counter - Throttling counter used as an index

RETURN VALUE  PDN throttle Failure Timer value in msec

SIDE EFFECTS  None
===========================================================================*/
uint32 ds_get_plmn_unblock_timer_val(void);

/*===========================================================================
FUNCTION      DS_GET_PDN_THROTTLE_FAILURE_TIMER

DESCRIPTION   This function provides the PDN throttling failure timer value

PARAMETERS    counter - Throttling counter used as an index

RETURN VALUE  PDN throttle Failure Timer value in msec

SIDE EFFECTS  None
===========================================================================*/
uint32 ds_get_pdn_throttle_failure_timer (uint8 counter);

/*===========================================================================
FUNCTION      DS_SET_PDN_THROTTLE_FAILURE_TIMER

DESCRIPTION   This function is to assign PDN throttling failure timer value
              manually

PARAMETERS    counter - Throttling counter used as an index

RETURN VALUE  none

SIDE EFFECTS  None
===========================================================================*/
void ds_set_pdn_throttle_failure_timer (uint8 counter,uint32 timerval);

/*===========================================================================
FUNCTION      DS_GET_PDN_THROTTLE_DISALLOW_TIMER

DESCRIPTION   This function provides the PDN throttling disallow timer value

PARAMETERS    counter - Throttling counter used as an index

RETURN VALUE  PDN throttle Failure Timer value in msec

SIDE EFFECTS  None
===========================================================================*/
uint32 ds_get_pdn_throttle_disallow_timer (uint8 counter);

/*===========================================================================
FUNCTION      DS_READ_FAST_DORMANCY_INFO_FROM_EFS

DESCRIPTION   This function will read the efs file all the necessary 
              data & fill the timer values in FD config structure.

              EFS File Format - Param_Name:Param_Val;
              For example     - TIMER_1:0;             

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
boolean ds_read_fast_dormancy_info_from_efs (void);

/*===========================================================================
FUNCTION      DS_READ_FAST_DORMANCY_ENABLED_FLAG_FROM_EFS

DESCRIPTION   This function will read the efs file and gets if Fast 
              Dormancy is enabled or not. If file doesnt exist, by 
              deafult cosiders as FD is disabled

              EFS File Format - Param_Val
              For example     - 1/0             

DEPENDENCIES  None

RETURN VALUE  
None

SIDE EFFECTS  None
===========================================================================*/
void ds_read_fast_dormancy_enabled_flag_from_efs (void);

/*===========================================================================
FUNCTION DS_CREATE_EFS_CONFIG_FILE

DESCRIPTION
  Create a config file in EFS which stores the path of EFS item files.

DEPENDENCIES
  None

PARAMETERS
  conf_file_path - File path of config file

RETURN VALUE
  0         - Success
 -1         - Non-EFS related Failures
  efs_errno - EFS related failures. Meaning of this value can be
              found in fs_errno.h

SIDE EFFECTS
  None
===========================================================================*/
int32 ds_create_efs_config_file
(
  const char *conf_file_path
);

/*===========================================================================
FUNCTION      DS_INIT_EFS_CONFIG_FILE

DESCRIPTION   Write all required item files to the config file.
              Any new item file must be added directly within this init 
              function.

DEPENDENCIES  None.

RETURN VALUE  0 for success.
              -1 for failure.

SIDE EFFECTS  None.
===========================================================================*/
int32 ds_init_efs_data_config_file(void);

/*===========================================================================
FUNCTION DS_READ_EFS_NV

DESCRIPTION
  This function reads the EFS item from the item file

DEPENDENCIES
  None

PARAMETERS
  item_file_path - File path to item in NV
  nv_info - Struct for NV item(s)
  nv_info_size - Size of NV item structure

RETURN VALUE
  NV_FAIL_S      - 
  NV_NOTACTIVE_S -
  NV_DONE_S      - 

SIDE EFFECTS
  None
===========================================================================*/
nv_stat_enum_type ds_read_efs_nv
(
  const char   *item_file_path, 
  void         *nv_info_ptr, 
  uint32       nv_info_size
);

/*===========================================================================
FUNCTION DS_WRITE_EFS_NV

DESCRIPTION
  This function writes the EFS item to the item file

DEPENDENCIES
  None

PARAMETERS
  item_file_path - File path to item in NV
  nv_info - Pointer to NV item
  nv_info_size - Size of NV item

RETURN VALUE
  0         - Success
 -1         - Non-EFS related Failures
  efs_errno - EFS related failures. Meaning of this value can be
              found in fs_errno.h

SIDE EFFECTS
  None
===========================================================================*/
int32 ds_write_efs_nv
(
  const char   *item_file_path, 
  void         *nv_info_ptr, 
  fs_size_t    nv_info_size
);

/*===========================================================================
FUNCTION DS_PATH_IS_DIRECTORY

DESCRIPTION
  To check if the EFS directory exists

DEPENDENCIES
  None

PARAMETERS
  dirname - Directory path

RETURN VALUE
   0         - success
   efs_errno - EFS error
   -1        - Other error

SIDE EFFECTS
  None
===========================================================================*/
int32 ds_path_is_directory
(
  const char *dirname
);

/*===========================================================================
FUNCTION DS_GET_TRAFFIC_CHANNEL_TEARDOWN_IN_LINGERING_VAL

DESCRIPTION
  Returns the current value of ds_traffic_channel_teardown_in_lingering.

DEPENDENCIES
  None

PARAMETERS
  None

RETURN VALUE
  boolean - TRUE or FALSE indicating whether to tear down traffic channel or 
            not during lingering.

SIDE EFFECTS
  None
===========================================================================*/
boolean ds_get_traffic_channel_teardown_in_lingering_val( void );

/*===========================================================================
FUNCTION DS_SET_TRAFFIC_CHANNEL_TEARDOWN_IN_LINGERING_VAL

DESCRIPTION
  Updates the current value of ds_traffic_channel_teardown_in_lingering.

DEPENDENCIES
  None

PARAMETERS
  boolean - TRUE or FALSE indicating whether to tear down traffic channel or 
            not during lingering.

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
void ds_set_traffic_channel_teardown_in_lingering_val
(
  boolean current_traffic_channel_teardown_in_lingering
);

/*===========================================================================
FUNCTION DS_MATH_CEIL

DESCRIPTION
  To return ceil value.

DEPENDENCIES
  None

PARAMETERS
  dividend - the dividend.
  divisor  - the divisor.

RETURN VALUE
  int32 - quotient.

SIDE EFFECTS
  None
===========================================================================*/
int32 ds_math_ceil
(
  /* dividend */
  int32 dividend,
  /* divisor */
  int32 divisor
);

/*===========================================================================
FUNCTION DS_MATH_FLOOR

DESCRIPTION
  To return floor value.

DEPENDENCIES
  None

PARAMETERS
  dividend - the dividend.
  divisor  - the divisor.

RETURN VALUE
  int32 - quotient.

SIDE EFFECTS
  None
===========================================================================*/
int32 ds_math_floor
(
  /* dividend */
  int32 dividend,
  /* divisor */
  int32 divisor
);

/*===========================================================================
FUNCTION DS_EFS_NV_INIT

DESCRIPTION
  This function gets all the EFS NV items for the Data task and stores them in
  the appropriate variables.

DEPENDENCIES
  The data config file must have been created

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
void ds_efs_nv_init
(
  void
);

/*==============================================================================
FUNCTION DS_UTIL_CALCULATE_MOVING_AVG

DESCRIPTION 
  This function calculates the exponential moving average of a given item
 
  CURRENT MATHEMATICAL MODEL USED TO CALCULATE MOVING AVG OF AN ITEM
  ========================================================================
  
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ==========================================================================
   ||                            - -                    - -                  ||
   ||                     W *   |A  |          (1-W) * |A  |                 ||
   ||ACTUAL THROUGHPUT =        | 1 |      +           | 0 |                 ||
   ||                            - -                    - -                  || 
    =========================================================================                             
    where
     A
      1    --->  Current Item value
   
     A
      0    ----> Average of values recorded from the previous observations
 
      W    -----> Weighting coefficient given to the previous average
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
DEPENDENCIES
    None

RETURN VALUE
  uint64 - updated moving average throughput

SIDE EFFECTS
  None
===========================================================================*/
uint64 ds_util_calculate_moving_avg
(
  uint64  prev_avg,
  uint64  new_item,
  int     weighting_coefficient
);

/*==============================================================================
FUNCTION DS_UTIL_CALCULATE_THROUGHPUT_MOVING_AVG

DESCRIPTION 
  This function calculates the moving average of the throughput values 
  given the sampling interval.
  This can be used to calculate both DL/UL throughput for the bearers/MAC ID's
 
PARAMETERS 
   uint32               prev_avg_throughput,
   uint64               prev_total_rx_bytes,
   uint64               current_total_rx_bytes,
   uint64               sampling_interval
 
DEPENDENCIES
    None
RETURN VALUE
  uint32 - updated moving average throughput

SIDE EFFECTS
  None
===========================================================================*/
uint32 ds_util_calculate_throughput_moving_avg
(
   uint32               prev_avg_throughput,
   uint64               prev_total_rx_bytes,
   uint64               current_total_rx_bytes,
   uint64               sampling_interval,
   uint8                moving_avg_coeff
);
/*===========================================================================
FUNCTION  DS_EFS_READ_KEEP_ALIVE_APN_DATA

DESCRIPTION
 Read the EFS file to get the Keep alive APN data.
  
PARAMETERS
 apn_name_ptr : The structure to be populated with apn_name
 
DEPENDENCIES
  None.

RETURN VALUE
  TRUE: If the read operation was successful
  FALSE: If the we could not read the efs file

SIDE EFFECTS
  None

===========================================================================*/
uint32 ds_efs_read_keep_alive_apn_data
(
   char                           **efs_data
);


/*===========================================================================
FUNCTION  DS_EFS_FREE_KAMGR_DATA_BUFFER

DESCRIPTION
 Free the EFS file data reserved for Keep alive manager.
  
PARAMETERS
 efs_data:               Pointer to the Memory to be released
 
DEPENDENCIES
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/

void ds_efs_free_kamgr_data_buffer
(
   char                             **efs_data
);

#endif /* DSUTIL_H */
