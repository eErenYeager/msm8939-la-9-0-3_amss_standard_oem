
/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

#include "ds3gprofile_validator.h"
#include "data_msg.h"
#include <stringl/stringl.h>

#define LABEL_DELIMITOR 0x2e



/*===========================================================================

                     FUNCTION DELCARATION

===========================================================================*/


/*===========================================================================
FUNCTION DS3G_GET_NETWORK_IDENTIFER_FROM_APN_NAME

DESCRIPTION
  This function is used to get network identifier part from apn name stored in the profile. This
  function leaves out the operator identifier. It picks up only the labels
  that reflect the network identifier.

PARAMETERS
  IN
  input_apn - Pointer to the APN in the profile. 
  input_apn_length - length of the apn string

  OUT
  network_identifier - Pointer to the byte array in which the network identifier of APN is returned

DEPENDENCIES
  None

RETURN VALUE
  boolean - TRUE if extraction is successful
            FALSE if there is any error
   
SIDE EFFECTS
  None
===========================================================================*/
uint8 ds3g_get_network_identifer_from_apn_name
(
  char*  input_apn,
  byte   input_apn_length,
  char*  network_identifier
)
{
  uint32  index = 0;
  boolean has_operator_identifier_part = FALSE;
  boolean has_fqdn_operator_identifier_part = FALSE;
  byte apn_length = input_apn_length; 
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  DATA_3GMGR_MSG1(MSG_LEGACY_HIGH, "APN Name passed with length: %d",input_apn_length);

  if (input_apn == NULL) 
  {
     DATA_3GMGR_MSG0(MSG_LEGACY_HIGH, "input_apn passed is null.");
     return 0;
  }

  apn_length = strnlen(input_apn, input_apn_length) ; 
  if (apn_length < input_apn_length) 
  {
     input_apn_length = apn_length;
  }

  if (input_apn_length == 0 || input_apn[0] == 0) 
  {
     DATA_3GMGR_MSG0(MSG_LEGACY_HIGH, "apn local_apn_length is 0.");
     return 0;
  }

  if(network_identifier == NULL)
  {
    DATA_3GMGR_MSG0(MSG_LEGACY_HIGH,"network identifier buffer is null.");
    return 0;
  }

  /*------------------------------------------------------------------------
    Determine if encoded_apn has operator identifier part. A valid operating
    would have the 3 labels in the following format 
    "mnc<MNC>.mcc<MCC>.gprs" 

   For example resolved apn name is (in byte format)
   {7,'a','p','n','n','a','m','e',6,'m','n','c','0','0','1',6,'m','c','c','0','1','1',4,'g','p','r','s'}
   "mnc001.mcc011.gprs" would be operator identifier (total 19 bytes)
   "apnname" would be network identifier
   
   The APN can also be in FQDN (Fully Qualified Domain Name) format. The
   network operator would be
   "apn.epc.mnc<MNC>.mcc<MCC>.3gppnetwork.org"
   
   For example resolved apn name is (in byte format)
   {3,'i','m','s',3,'a','p','n',3,'e','p','c',6,'m','n','c','0','0','1',6,'m','c','c','0','0','1',11,'3','g','p','p','n','e','t','w','o','r','k',3,'o','r','g'}
   "ims.apn.epc.mnc001.mcc001.3gppnetwork.org" would be the operator identifier (total 42 bytes)
   "ims" would be the network identifier
  
    For references check:

    Spec: 123 003 V 12.4.1 (2014-10)
    9 Definition of Access Point Name
    19.4.2.2 Access Point Name FQDN (APN-FQDN)

  ------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    Traverse the encoded APN to determine the number of labels in the string
  ------------------------------------------------------------------------*/

   /*-------------------------------------------------------------------------
    check for mnc<MNC>.mcc<MCC>.gprs format
   --------------------------------------------------------------------------*/
  if(input_apn_length > DS3G_PROFILE_OPERATOR_IDENTIFIER_TOTAL_LENGTH ) 
  {
    do
    {
      has_operator_identifier_part = TRUE;
      index = input_apn_length - DS3G_PROFILE_OPERATOR_IDENTIFIER_TOTAL_LENGTH;

    if(strncasecmp(input_apn+index,".mnc",DS3G_PROFILE_MCC_MNC_STR_LENGTH_WITH_DELIM) != 0)
    {
        has_operator_identifier_part = FALSE;
        break;
    }

       index += DS3G_PROFILE_MCC_MNC_LENGTH_WITH_DELIM;

       if(strncasecmp(input_apn+index,".mcc",DS3G_PROFILE_MCC_MNC_STR_LENGTH_WITH_DELIM) != 0)
       {
        has_operator_identifier_part = FALSE;
        break;
      }

      index += DS3G_PROFILE_MCC_MNC_LENGTH_WITH_DELIM;

      if(strncasecmp(input_apn+index,".gprs",DS3G_PROFILE_GPRS_STR_LENGTH_WITH_DELIM) != 0)
      {
        has_operator_identifier_part = FALSE;
        break;
      }
    }while (0); 
   }

  /*-------------------------------------------------------------------------
    check for FQDN format apn.epc.mnc<MNC>.mcc<MCC>.3gppnetwork.org.
    Skip check if format is already valid from non-FQDN check
   --------------------------------------------------------------------------*/
  if ((input_apn_length > DS3G_PROFILE_FQDN_OPERATOR_IDENTIFIER_TOTAL_LENGTH) && 
      (has_operator_identifier_part == FALSE))
  {
    do
    {
      has_fqdn_operator_identifier_part = TRUE;
      index = input_apn_length - DS3G_PROFILE_FQDN_OPERATOR_IDENTIFIER_TOTAL_LENGTH;

      if(strncasecmp(input_apn+index,".apn",DS3G_PROFILE_APN_STR_LENGTH_WITH_DELIM) != 0)
      {
        has_fqdn_operator_identifier_part = FALSE;
        break;
      }

      index += DS3G_PROFILE_APN_STR_LENGTH_WITH_DELIM;

      if(strncasecmp(input_apn+index,".epc",DS3G_PROFILE_EPC_STR_LENGTH_WITH_DELIM) != 0)
      {
        has_fqdn_operator_identifier_part = FALSE;
        break;
    }
    
      index += DS3G_PROFILE_EPC_STR_LENGTH_WITH_DELIM;

      if(strncasecmp(input_apn+index,".mnc",DS3G_PROFILE_MCC_MNC_STR_LENGTH_WITH_DELIM) != 0)
    {
        has_fqdn_operator_identifier_part = FALSE;
        break;
      }

       index += DS3G_PROFILE_MCC_MNC_LENGTH_WITH_DELIM;

      if(strncasecmp(input_apn+index,".mcc",DS3G_PROFILE_MCC_MNC_STR_LENGTH_WITH_DELIM) != 0)
       {
        has_fqdn_operator_identifier_part = FALSE;
        break;
       }

      index += DS3G_PROFILE_MCC_MNC_LENGTH_WITH_DELIM;

      if(strncasecmp(input_apn+index,".3gppnetwork",DS3G_PROFILE_3GPPNETWORK_STR_LENGTH_WITH_DELIM) != 0)
      {
        has_fqdn_operator_identifier_part = FALSE;
        break;
    }
    
      index += DS3G_PROFILE_3GPPNETWORK_STR_LENGTH_WITH_DELIM;

      if(strncasecmp(input_apn+index,".org",DS3G_PROFILE_ORG_STR_LENGTH_WITH_DELIM) != 0)
      {
        has_fqdn_operator_identifier_part = FALSE;
        break;
      }
    }while (0);
  }

  if(has_operator_identifier_part) 
  {
    input_apn_length -= DS3G_PROFILE_OPERATOR_IDENTIFIER_TOTAL_LENGTH;
  }
  else if (has_fqdn_operator_identifier_part)
  {
    input_apn_length -= DS3G_PROFILE_FQDN_OPERATOR_IDENTIFIER_TOTAL_LENGTH;
  }

  if (input_apn_length <= DS3G_PROFILE_NETWORK_IDENTIFIER_MAX_LENGTH) 
  {
    memscpy(network_identifier, input_apn_length,
                 input_apn,
                 input_apn_length);
    network_identifier[input_apn_length] = 0x0;
  }
 
  return input_apn_length;
} /* ds3g_get_network_identifer_from_apn_name */


/*===========================================================================
FUNCTION DS3G_PROFILE_APN_IS_NETWORK_IDENTIFIER_VALID

DESCRIPTION
  This functions checks if network identifier is fully compliant with 3gpp spec.

PARAMETERS
  IN
  apn_ni - Pointer to the network identifier in the profile. 
  apn_ni_len - length of the network identifier string


DEPENDENCIES
  None

RETURN VALUE
  boolean - TRUE if network indentifier is valid
            FALSE  otherwise
   
SIDE EFFECTS
  None
===========================================================================*/
boolean ds3g_profile_apn_is_network_identifier_valid
(
   byte*  apn_ni,
   uint8 apn_ni_len
)
{
   if (apn_ni_len == 0)
   {
      DATA_3GMGR_MSG0(MSG_LEGACY_HIGH,"apn_ni_len is 0");
      return TRUE;
   }

   if (apn_ni_len > DS3G_PROFILE_NETWORK_IDENTIFIER_MAX_LENGTH) 
   {
     DATA_3GMGR_MSG1(MSG_LEGACY_ERROR,"Network Indentifier is %d, greater"
                     "than  DS3G_PROFILE_NETWORK_IDENTIFIER_MAX_LENGTH encoding",
                     apn_ni_len);
     return FALSE;
   }

   if (apn_ni == NULL)
   {
      DATA_3GMGR_MSG0(MSG_LEGACY_HIGH,"Network identifier is NULL'.gprs'");
      return FALSE;
   }

   if(apn_ni_len >= DS3G_PROFILE_GPRS_STR_LENGTH_WITH_DELIM && 
       strncasecmp((char*)apn_ni + apn_ni_len-DS3G_PROFILE_GPRS_STR_LENGTH_WITH_DELIM ,
                   ".gprs",DS3G_PROFILE_GPRS_STR_LENGTH_WITH_DELIM) == 0)
    {
      DATA_3GMGR_MSG0(MSG_LEGACY_ERROR,"network identifier ends with '.gprs'");
      return FALSE;
    }
    else if(apn_ni_len >= DS3G_PROFILE_RAC_LAC_RNC_STR_LENGTH && 
            (strncasecmp((char*)apn_ni,"rac",DS3G_PROFILE_RAC_LAC_RNC_STR_LENGTH) == 0))
    {
      DATA_3GMGR_MSG0(MSG_LEGACY_ERROR,"network identifier starts with 'rac'");
      return FALSE;
    }
    else if(apn_ni_len >= DS3G_PROFILE_RAC_LAC_RNC_STR_LENGTH &&
            (strncasecmp((char*)apn_ni,"lac",DS3G_PROFILE_RAC_LAC_RNC_STR_LENGTH) == 0))
    {
      DATA_3GMGR_MSG0(MSG_LEGACY_ERROR,"network identifier starts with 'lac'");
      return FALSE;
    }
    else if(apn_ni_len >= DS3G_PROFILE_RAC_LAC_RNC_STR_LENGTH &&
            (strncasecmp((char*)apn_ni,"rnc",DS3G_PROFILE_RAC_LAC_RNC_STR_LENGTH) == 0))
    {
      DATA_3GMGR_MSG0(MSG_LEGACY_ERROR,"network identifier starts with 'rnc'");
      return FALSE;
    }
    else if(apn_ni_len >= DS3G_PROFILE_SGSN_STR_LENGTH &&
            (strncasecmp((char*)apn_ni,"sgsn",DS3G_PROFILE_SGSN_STR_LENGTH) == 0))
    {
      DATA_3GMGR_MSG0(MSG_LEGACY_ERROR,"network identifier starts with sgsn");
      return FALSE;
    }

    return TRUE;

}/*ds3g_profile_apn_is_network_identifier_valid*/

/*===========================================================================
FUNCTION DS3G_PROFILE_VALIDATE_APN_NAME

DESCRIPTION
  The function validates apn name according to 3gpp spec
    
PARAMETERS: 
  *apn_name  --  apn name char pointer
  *info     --  apn name lenght
   
 
DEPENDENCIES
  None.
  
RETURN VALUE 
  TRUE - if apn name is valid
  FALSE  - otherwise
  
SIDE EFFECTS 
  None 
  
===========================================================================*/

boolean ds3g_profile_validate_apn_name
(
  char *apn_name,
  uint8 apn_name_len
)
{
  uint8 network_ident_length = 0;
  byte apn_network_identifier[DS3G_PROFILE_NETWORK_IDENTIFIER_MAX_LENGTH+1];
 /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if(apn_name == NULL)
  {
    DATA_3GMGR_MSG0(MSG_LEGACY_HIGH,"apn_name is NULL");
    return TRUE;
  }

  //Validate operator identifier and extract the network identifier here
  network_ident_length = ds3g_get_network_identifer_from_apn_name(
     apn_name, apn_name_len, (char*)apn_network_identifier);

  if (!ds3g_profile_apn_is_network_identifier_valid(apn_network_identifier,
                                                      network_ident_length )) 
  {
     DATA_3GMGR_MSG0(MSG_LEGACY_ERROR,"Network Indentifier is not valid");
     return FALSE;
  }

  return TRUE;
} /*ds3g_profile_validate_apn_name*/


/*===========================================================================
FUNCTION DS3G_DECODE_APN_NAME

DESCRIPTION
  This function is used to decode the APN name returned by the network. This
  function leaves out the operator identified. It picks up only the labels
  that reflect the network identifier.

PARAMETERS
  IN
  encoded_apn - Pointer to the APN returned by the network. This is encoded
  encoded_apn_length - length of the encoded string

  OUT
  decoded_apn - Pointer to the byte array in which the decoded APN is returned

DEPENDENCIES
  None

RETURN VALUE
  boolean - TRUE if decoding is successful
            FALSE if there is any error
   
SIDE EFFECTS
  None
===========================================================================*/
boolean ds3g_decode_apn_name
(
  byte*  encoded_apn,
  uint32 encoded_apn_length,
  byte*  decoded_apn
)
{
  uint32  label_count = 0;
  uint32  label_length = 0;
  uint32  index = 0;
  boolean ret_val = TRUE;
  uint32  apn_string_offset = 0;
  boolean has_operator_identifier_part = FALSE;
  boolean has_fqdn_operator_identifier_part = FALSE;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  DATA_3GMGR_MSG1(MSG_LEGACY_MED,"Encoded APN length: %d",encoded_apn_length);
  DATA_3GMGR_MSG_SPRINTF_1(MSG_LEGACY_HIGH,"Encoded APN name: %s",encoded_apn);

  /*------------------------------------------------------------------------
    Determine if encoded_apn has operator identifier part. A valid operating
    would have the 3 labels in the following format 
    "mnc<MNC>.mcc<MCC>.gprs" 

   For example resolved apn name is (in byte format)
   {7,'a','p','n','n','a','m','e',6,'m','n','c','0','0','1',6,'m','c','c','0','1','1',4,'g','p','r','s'}
   "mnc001.mcc011.gprs" would be operator identifier (total 19 bytes)
   "apnname" would be network identifier
  
   The APN can also be in FQDN (Fully Qualified Domain Name) format. The
   network operator would be
   "apn.epc.mnc<MNC>.mcc<MCC>.3gppnetwork.org"
   
   For example resolved apn name is (in byte format)
   {3,'i','m','s',3,'a','p','n',3,'e','p','c',6,'m','n','c','0','0','1',6,'m','c','c','0','0','1',11,'3','g','p','p','n','e','t','w','o','r','k',3,'o','r','g'}
   "ims.apn.epc.mnc001.mcc001.3gppnetwork.org" would be the operator identifier (total 42 bytes)
   "ims" would be the network identifier
  
    For references check:

    Spec: 123 003 V 12.4.1 (2014-10)
    9 Definition of Access Point Name
    19.4.2.2 Access Point Name FQDN (APN-FQDN)

  ------------------------------------------------------------------------*/

  /*-------------------------------------------------------------------------
    check for mnc<MNC>.mcc<MCC>.gprs format
   --------------------------------------------------------------------------*/
  if(encoded_apn_length > DS3G_PROFILE_OPERATOR_IDENTIFIER_TOTAL_LENGTH) //Greater than 19 bytes
  {
    do
    {
      has_operator_identifier_part = TRUE;
      index = encoded_apn_length - DS3G_PROFILE_OPERATOR_IDENTIFIER_TOTAL_LENGTH;
      label_length = (uint32)(*(encoded_apn+index));
    /*-------------------------------------------------------------------------
      check length and make sure it has "mnc"
     --------------------------------------------------------------------------*/
    if((label_length != DS3G_PROFILE_MCC_MNC_LABEL_LENGTH) || 
       (strncasecmp((char*)encoded_apn+index+1,"mnc",DS3G_PROFILE_MCC_MNC_STR_LENGTH) != 0))
    {
        has_operator_identifier_part = FALSE;
        break;
    }

       index += label_length + 1;
       label_length = (uint32)(*(encoded_apn+index));
       /*-------------------------------------------------------------------------
        check length and make sure it has "mcc"
        --------------------------------------------------------------------------*/
       if((label_length != DS3G_PROFILE_MCC_MNC_LABEL_LENGTH) || 
         (strncasecmp((char*)encoded_apn+index+1,"mcc",DS3G_PROFILE_MCC_MNC_STR_LENGTH) != 0))
       {
        has_operator_identifier_part = FALSE;
        break;
    }
    
       index += label_length + 1;
       label_length = (uint32)(*(encoded_apn+index));
       /*-------------------------------------------------------------------------
        check length and make sure it has "gprs"
       --------------------------------------------------------------------------*/
       if((label_length != DS3G_PROFILE_GPRS_LABEL_LENGTH) || 
         (strncasecmp((char*)encoded_apn+index+1,"gprs",DS3G_PROFILE_GPRS_STR_LENGTH) != 0))
       {
        has_operator_identifier_part = FALSE;
        break;
      }
    }while (0); 
  }

  /*-------------------------------------------------------------------------
    check for FQDN format apn.epc.mnc<MNC>.mcc<MCC>.3gppnetwork.org.
    Skip check if format is already valid from non-FQDN check
   --------------------------------------------------------------------------*/
  if ((encoded_apn_length > DS3G_PROFILE_FQDN_OPERATOR_IDENTIFIER_TOTAL_LENGTH) && 
      (has_operator_identifier_part == FALSE))
  {
    do
    {
      has_fqdn_operator_identifier_part = TRUE;
      index = encoded_apn_length - DS3G_PROFILE_FQDN_OPERATOR_IDENTIFIER_TOTAL_LENGTH;
      label_length = (uint32)(*(encoded_apn+index));

      /*-------------------------------------------------------------------------
        check length and make sure it has "apn"
       --------------------------------------------------------------------------*/
      if((label_length != DS3G_PROFILE_APN_STR_LENGTH) || 
         (strncasecmp((char*)encoded_apn+index+1,"apn",DS3G_PROFILE_APN_STR_LENGTH) != 0))
      {
        has_fqdn_operator_identifier_part = FALSE;
        break;
       }

      index += label_length + 1;
      label_length = (uint32)(*(encoded_apn+index));

      /*-------------------------------------------------------------------------
        check length and make sure it has "epc"
       --------------------------------------------------------------------------*/
      if((label_length != DS3G_PROFILE_EPC_STR_LENGTH) || 
         (strncasecmp((char*)encoded_apn+index+1,"epc",DS3G_PROFILE_EPC_STR_LENGTH) != 0))
      {
        has_fqdn_operator_identifier_part = FALSE;
        break;
      }

      index += label_length + 1;
      label_length = (uint32)(*(encoded_apn+index));

      /*-------------------------------------------------------------------------
        check length and make sure it has "mnc"
       --------------------------------------------------------------------------*/
      if((label_length != DS3G_PROFILE_MCC_MNC_LABEL_LENGTH) || 
         (strncasecmp((char*)encoded_apn+index+1,"mnc",DS3G_PROFILE_MCC_MNC_STR_LENGTH) != 0))
      {
        has_fqdn_operator_identifier_part = FALSE;
        break;
      }

      index += label_length + 1;
      label_length = (uint32)(*(encoded_apn+index));

      /*-------------------------------------------------------------------------
       check length and make sure it has "mcc"
       --------------------------------------------------------------------------*/
      if((label_length != DS3G_PROFILE_MCC_MNC_LABEL_LENGTH) || 
        (strncasecmp((char*)encoded_apn+index+1,"mcc",DS3G_PROFILE_MCC_MNC_STR_LENGTH) != 0))
      {
        has_fqdn_operator_identifier_part = FALSE;
        break;
    }
    
      index += label_length + 1;
      label_length = (uint32)(*(encoded_apn+index));

      /*-------------------------------------------------------------------------
        check length and make sure it has "3gppnetwork"
       --------------------------------------------------------------------------*/
      if((label_length != DS3G_PROFILE_3GPPNETWORK_STR_LENGTH) || 
         (strncasecmp((char*)encoded_apn+index+1,"3gppnetwork",DS3G_PROFILE_3GPPNETWORK_STR_LENGTH) != 0))
      {
        has_fqdn_operator_identifier_part = FALSE;
        break;
      }

      index += label_length + 1;
      label_length = (uint32)(*(encoded_apn+index));

      /*-------------------------------------------------------------------------
        check length and make sure it has "org"
       --------------------------------------------------------------------------*/
      if((label_length != DS3G_PROFILE_ORG_STR_LENGTH) || 
         (strncasecmp((char*)encoded_apn+index+1,"org",DS3G_PROFILE_ORG_STR_LENGTH) != 0))
      {
        has_fqdn_operator_identifier_part = FALSE;
        break;
      }
    }while (0); 
  }

  if(has_operator_identifier_part) 
  {
    DATA_3GMGR_MSG0(MSG_LEGACY_HIGH, "Valid Operator Identifier Found");
     encoded_apn_length -= DS3G_PROFILE_OPERATOR_IDENTIFIER_TOTAL_LENGTH;
  }
  else if (has_fqdn_operator_identifier_part == TRUE)
  {
    DATA_3GMGR_MSG0(MSG_LEGACY_HIGH, "Valid FQDN Operator Identifier Found");
    encoded_apn_length -= DS3G_PROFILE_FQDN_OPERATOR_IDENTIFIER_TOTAL_LENGTH;
  }
  
  label_length = 0;
  index = 0;

  while(index < encoded_apn_length)
  {
    /*----------------------------------------------------------------------
      Get the length of the label
      Increment the label count
      Increase the offset into the string to point to the next label length
      indicator
    ----------------------------------------------------------------------*/
    label_length = (uint32)(*(encoded_apn+index));

    if(label_length > 0)
    {
      label_count++;
    }
    /*--------------------------------------------------------------------------
      Always increment by label_length+1. This is to ensure that we get to the
      next label_length indicator
    --------------------------------------------------------------------------*/
    index += (label_length+1);

  }// while loop to traverse the encoded APN string

  DATA_3GMGR_MSG1(MSG_LEGACY_MED,"Number of labels in APN Network identifier: %d",label_count);

  if(label_count < 1)
  {
    DATA_3GMGR_MSG0(MSG_LEGACY_MED,"Number of labels in APN Network identifier is less than 1");
    ret_val = FALSE;
  }
  else
  {
    DATA_3GMGR_MSG0(MSG_LEGACY_MED,"Extracting network identifier labels");

    /*--------------------------------------------------------------------------
      Reset the index to 1.
      Also reset the label_length to 0
    --------------------------------------------------------------------------*/
    index = 1;
    label_length = 0;

    /*------------------------------------------------------------------------
      Remember the following while copying from src to dest:

      Apn_string_offset is used to index into both buffers, but the offset
      in the src string is always one greater than dest
    ------------------------------------------------------------------------*/
    
    while(index <= label_count)
    {
      label_length = (uint32)(*(encoded_apn+apn_string_offset));

      if((apn_string_offset + label_length) < encoded_apn_length)
      {
        memscpy(decoded_apn+apn_string_offset, 
		 label_length,
                 encoded_apn+apn_string_offset+1,
                 label_length);

        /*----------------------------------------------------------------------
          Increment the apn_string_offset by label_length
        ----------------------------------------------------------------------*/
        apn_string_offset += label_length;

        if(index == label_count)
        {
          /*--------------------------------------------------------------------
            Terminate the string with NULL
          --------------------------------------------------------------------*/
          *(decoded_apn+apn_string_offset) = 0x0;
        }
        else
        {
          /*--------------------------------------------------------------------
            Write the label delimitor in the decoded string
          --------------------------------------------------------------------*/
          *(decoded_apn+apn_string_offset) = LABEL_DELIMITOR;

          /*--------------------------------------------------------------------
            Increment the apn_string_offset by one.
          --------------------------------------------------------------------*/
          apn_string_offset++;
        }
        index++;
      }
      else
      {
        DATA_3GMGR_MSG0(MSG_LEGACY_ERROR,"Error decoding. Avoiding buf overrun on Src Str");
        ret_val = FALSE;
        break;
      }

    } //while loop to extract labels corresponding to N/w Identifier
  } // label count greater than 3

  /*-----------------------------------------------------------------------------------------
   The APN Network Identifier shall contain at least one label and shall have, after encoding as 
   defined in subclause 9.1 above, a maximum length of 63 octets. An APN Network Identifier shall
   not start with any of the strings "rac", "lac", "sgsn" or "rnc", and it shall not end in ".gprs".
   ------------------------------------------------------------------------------------------*/

  if (ret_val && ds3g_profile_apn_is_network_identifier_valid(decoded_apn, apn_string_offset)) 
  {
    ret_val = TRUE; 
  }
  else
  {
    ret_val = FALSE;
  }
  
  return ret_val;
} /* ds3g_decode_apn_name */