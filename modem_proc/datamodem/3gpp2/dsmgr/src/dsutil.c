/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
               D S U T I L . C
           
GENERAL DESCRIPTION
  Utility functions for DS module

EXTERNALIZED FUNCTIONS
  DS_EFS_FILE_INIT()
    Intializes the state machine and also opens the file
  DS_EFS_FILE_CLOSE()
    Closes file and releases the state machine 
  DS_EFS_TOKENIZER()
    Reads from the EFS file and returns a token
  DS_CREATE_EFS_CONFIG_FILE()
    Create the EFS config file which allows QPST to back up item files in QCNs.
    The config file will be empty until ds_create_efs_item_file() is called.
  DS_CREATE_EFS_ITEM_FILE()
    Places the path of an item file into the specified config file. This is
    used by QPST to backup the EFS item for QCN files. If the config file was
    not set up properly this function will exit with the appropriate efs_errno.
  DS_READ_EFS_NV()
    Reads the EFS item based on the item file path passed in.
  DS_WRITE_EFS_NV()
    Writes to the EFS item based on the item file path passed in.

INITIALIZATION AND SEQUENCING REQUIREMENTS
  USING EFS ITEMS
    1. DS_CREATE_EFS_CONFIG_FILE() should be called in a powerup function.
       Only need to call this once per task. Each task shall have a
       different config file. DS task calls this function in dstask.c
    2. DS_CREATE_EFS_ITEM_FILE() should be called in component specific
       init function. This function will append an efs item file path into
       the config file.
       For DS task, use "/nv/item_files/conf/data_config_info.conf" for config
       file location
    3. When the EFS item wants to be read, DS_READ_EFS_NV() is called.
       The item file path is used to locate the item file in the EFS.
       A data structure/data type (passed by reference) will be filled with
       the value of the EFS item.
    4. If the read call returns with a ENOENT (file does not exist), then
       user will need to call DS_WRITE_EFS_NV() with default values passed in.

Copyright (c) 2004-2014 QUALCOMM Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/


/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/3gpp2/dsmgr/src/dsutil.c#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

===========================================================================
when       who        what, where, why
--------   ---       ------------------------------------------------------- 
06/03/13   sd        ATCoP decoupling changes
08/13/12   ss        EFS write optimizations.
09/05/11   ss        Changes to support RPM.
08/09/11   sha       Global variable clean up.
07/12/11   dvk       Fixed Klocwork errors
06/06/11   ash       Added support for PPP Partial Context
05/05/11   ack       Moving ds_path_is_directory declaration to .h file
03/23/11   sn        Globals cleanup for free floating of tasks.
02/16/11   op        Klocwork fix and comments cleanup
01/17/11   ss        Added support for Iface linger.
09/07/10   op        Added functions to use EFS items.
07/18/10   mg        Added functions for reading EFS file for PDN throttling 
                     feature for eHRPD
04/16/10   ls        Created this file. Merge efs functions from 
                     dshdr_efsif.c to dsutil.c

===========================================================================*/
/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

#include "comdef.h"
#include "customer.h"

#include "dsutil.h"
#include "msg.h"
#include "data_msg.h"

#include <stringl/stringl.h>
#include "data_err_suppr.h"
#include "ds_3gpp_hdlr.h"
#include "ds_3gpp_throttle_sm.h"
#include "ds_3gpp_pdn_throttle_sm.h"
#include "ds_3gppi_utils.h"
#include "ds707_pkt_mgr.h"
#include "ds_3gppi_utils.h"
#include "ds_system_heap.h"
#include "ds_wlan_util.h"
/*= = = = = = = =  = = = = = = = = = =  = = =  = = = = = = = = = = = = = = =
                        MACRO DEFINITIONS
= = = = = = = = = = = = = = =  = = = = =  = = = = = = = = = = = = = = = = =*/

/*===========================================================================
            LOCAL DEFINITIONS AND DECLARATIONS FOR MODULE
===========================================================================*/

/*---------------------------------------------------------------------------
  Specifies the no. & size of PDN throttle config params
  10 failure timer(for global throttling)+6 disallow timer
---------------------------------------------------------------------------*/
#define MAX_NUM_PDN_THROTTLE_PARAMS 16 
#define MAX_PDN_THROTTLE_PARAM_NAME_SIZE 30
#define MAX_NUM_THROTTLE_CAUSE_CODES 5
#define MAX_CAUSE_CODE_NAME_LENGTH  3

#define DS_SIGN(x)              (((x) < 0) ? -1 : 1)
/*---------------------------------------------------------------------------
  Weighting coefficient to calculate the exponential moving average of
  actual throughput
  100%   --> Ignore the history of moving averages calculated previously,
             and record the current value as the average
 
  0%     --> Discard the current value of the item and report only
                          the previous moving average 
  CHOOSE WEIGHTING COEFFICIENT BETWEEN 0 and 100% to moving average
  ---------------------------------------------------------------------------*/
#define DSUTIL_MOVING_AVG_COEFF          40

/*---------------------------------------------------------------------------
  An array of strings to map the PDN throttle name string (mentioned in EFS 
  file) to a token ID. This token ID will be used to modify the correct entry
  in the internal data structure. The token ID is the index of the string 
  entry in the array.
---------------------------------------------------------------------------*/
static char pdn_throttle_param_name
      [MAX_NUM_PDN_THROTTLE_PARAMS][MAX_PDN_THROTTLE_PARAM_NAME_SIZE] = 
                                                {"FAILURE_TIMER_1",
                                                 "FAILURE_TIMER_2",
                                                 "FAILURE_TIMER_3",
                                                 "FAILURE_TIMER_4",
                                                 "FAILURE_TIMER_5",
                                                 "FAILURE_TIMER_6",
                                                 "FAILURE_TIMER_7",
                                                 "FAILURE_TIMER_8",
                                                 "FAILURE_TIMER_9",
                                                 "FAILURE_TIMER_10",
                                                 "DISALLOW_TIMER_1",
                                                 "DISALLOW_TIMER_2",
                                                 "DISALLOW_TIMER_3",
                                                 "DISALLOW_TIMER_4",
                                                 "DISALLOW_TIMER_5",
                                                 "DISALLOW_TIMER_6"
                                                };

typedef struct 
{
  uint16 timerval;
  uint16 fail_count;
  uint16 cause_code;
}throttle_timer_type;

typedef enum
{
  CAUSE_CODE_NUMBER,
  MAX_FAILURE_COUNTER,
  TIMER_VALUE
}cc_sp_throttle_cfg_item_e_type;

 throttle_timer_type *cc_sp_throttle_timer_table[MAX_NUM_THROTTLE_CAUSE_CODES]; 

/*---------------------------------------------------------------------------
  Id's assigned to params specified in the PDN throttling config file in EFS
---------------------------------------------------------------------------*/
typedef enum
{
  FAILURE_TIMER_1,
  FAILURE_TIMER_2,
  FAILURE_TIMER_3,
  FAILURE_TIMER_4,
  FAILURE_TIMER_5,
  FAILURE_TIMER_6,
  FAILURE_TIMER_7,
  FAILURE_TIMER_8,
  FAILURE_TIMER_9,
  FAILURE_TIMER_10,
  DISALLOW_TIMER_1,
  DISALLOW_TIMER_2,
  DISALLOW_TIMER_3,
  DISALLOW_TIMER_4,
  DISALLOW_TIMER_5,
  DISALLOW_TIMER_6
}pdn_throttle_config;

/*---------------------------------------------------------------------------
  Structure to store PDN throttle config values
---------------------------------------------------------------------------*/
static ds_pdn_throttle_config_type pdn_throttle;

/*---------------------------------------------------------------------------
  Global variable to hold the value of 
  NV_TRAFFIC_CHANNEL_TEARDOWN_IN_LINGERING
---------------------------------------------------------------------------*/
static boolean ds_traffic_channel_teardown_in_lingering = FALSE;

static uint32 plmn_unblock_timer_val = 0;
/*===========================================================================
        Local functions.
===========================================================================*/
int16 ds_pdn_throttle_get_token_id
(
  char *from,
  char *to,
  uint8 *token_id
);

void ds_fill_pdn_throttle_config_with_token_contents
(
  uint8                        token_id,
  char                        *from,
  char                        *to
);

static void ds_set_plmn_unblock_timer_val(uint32 timer_val)
{
  if(timer_val>0)
  {
    plmn_unblock_timer_val = timer_val;
  }
}
/*===========================================================================

                      EXTERNAL FUNCTION DEFINITIONS

===========================================================================*/

/*===========================================================================
FUNCTION      DS_EFS_FILE_INIT

DESCRIPTION   The function intializes the state machine and 
              also opens the file

DEPENDENCIES  None.

RETURN VALUE 
              0  : SUCCESS: The file is good, readable, 
                            State Machine Initialized.
              -1 : FAILURE: The file cannot be opened/ readable. 

SIDE EFFECTS  None.
===========================================================================*/
int ds_efs_file_init( const char *file_path, ds_efs_token_type *sm )
{
  int ret_val = -1;
  struct fs_stat temp_buf;
  size_t efs_file_path_size;
 /*----------------------------------------------------------------------------------*/
  
  /*
  ** Initialize the structure variables and open the file in read mode.    
  ** Care must be taken to not initialize this structure variable before calling this 
  ** function 
  */
  if(sm == NULL)
  {
    DATA_3GMGR_MSG0( MSG_LEGACY_HIGH, "SM ptr is NULL");
    return ret_val;
  }

  memset(sm,0,sizeof(ds_efs_token_type));

  sm->seperator   = ';';
  sm->curr        = sm->buffer;
  sm->end_pos     = sm->buffer;
  sm->eof         = FALSE;
  sm->eol         = FALSE;
  sm->skip_line   = FALSE;
  sm->bol         = TRUE;
  sm->buffer                = NULL;
  sm->efs_file_path         = NULL;
  sm->efs_get_performed     = FALSE;

  if(efs_stat(file_path,&temp_buf) == -1)
  {
      DATA_3GMGR_MSG1(MSG_LEGACY_ERROR, "Error opening file: %s", file_path);
      return ret_val;
  }
  else
  {
   /*------------------------------------------------------------------------------------- 
    Allocate efs file path, If it succeeds try allocating buffer.
    If buffer allocation fails, free the file_path allocation   .                         
    Also check for ret_val = -1 to print an appropriate message.                                                            .
   -------------------------------------------------------------------------------------*/
    sm->read_buffer_size = temp_buf.st_size;

    if(sm->read_buffer_size <= 0)
    {

      DATA_3GMGR_MSG1(MSG_LEGACY_ERROR, "efs stat buffer size error,buffer_size:%d",
                        sm->read_buffer_size);
        return ret_val;
    }

    sm->buffer = (char*)ds_system_heap_mem_alloc(sizeof(char) * sm->read_buffer_size);
    if(sm->buffer != NULL)
    {
      efs_file_path_size = sizeof(char) * (strlen(file_path)+1);
      sm->efs_file_path = (char*)ds_system_heap_mem_alloc(efs_file_path_size);
      if(sm->efs_file_path != NULL)
      {
        (void)strlcpy(sm->efs_file_path,file_path,efs_file_path_size);
        ret_val = 0;
      }
      else 
      {
        ds_system_heap_mem_free(sm->buffer);
      }
    }
    /*----------------------------------------------------------------------------------- 
      The below check is to display message that memory alloc failed
     -----------------------------------------------------------------------------------*/
    if(ret_val == -1)
    {
      DATA_3GMGR_MSG0(MSG_LEGACY_ERROR, "Memory allocation failed");
    }
   }

  return ret_val;
}

/*===========================================================================
FUNCTION      DS_EFS_FILE_CLOSE

DESCRIPTION   The function closes file and releases the state machine, 
              This also cleans up any allocated heap memory. 

DEPENDENCIES  The file should have opened already.

RETURN VALUE  NONE

SIDE EFFECTS  None.
===========================================================================*/
void ds_efs_file_close( ds_efs_token_type *sm )
{
  if(sm == NULL)
  {
    DATA_3GMGR_MSG0( MSG_LEGACY_HIGH, "SM ptr is NULL,Close unsuccessful");
    return;
  }

  /*------------------------------------------------------------------------- 
    For an efs_get we will not have to close, However free all allocated
    dynamic memory
  --------------------------------------------------------------------------*/
  /* lint fix */
  DATA_MDM_ARG_NOT_CONST(sm);
  ds_system_heap_mem_free(sm->efs_file_path);
  sm->efs_file_path = NULL;
  ds_system_heap_mem_free(sm->buffer);
  sm->buffer = NULL;

  return;
}

/*==============================================================================
FUNCTION DS_UTIL_CALCULATE_MOVING_AVG

DESCRIPTION 
  This function calculates the exponential moving average of a given item
 
  CURRENT MATHEMATICAL MODEL USED TO CALCULATE MOVING AVG OF AN ITEM
  ========================================================================
  
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ==========================================================================
   ||                            - -                    - -                  ||
   ||                     W *   |A  |          (1-W) * |A  |                 ||
   ||ACTUAL THROUGHPUT =        | 1 |      +           | 0 |                 ||
   ||                            - -                    - -                  || 
    =========================================================================                             
    where
     A
      1    --->  Current Item value
   
     A
      0    ----> Average of values recorded from the previous observations
 
      W    -----> Weighting coefficient given to the previous average
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
DEPENDENCIES
    None

RETURN VALUE
  uint64 - updated moving average throughput

SIDE EFFECTS
  None
===========================================================================*/
uint64 ds_util_calculate_moving_avg
(
   uint64 prev_avg,
   uint64 new_item,
   int    coefficient
)
{
  uint64 new_avg              = 0;
  uint16  factor              = 0;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (coefficient < 0)
  {
    factor = 0;
  }
  else
  {
    factor = coefficient;
  }
  new_avg =  (((factor * new_item)+((100 - factor) * prev_avg)) / 100);
  return new_avg;
}
/*===========================================================================
FUNCTION      DS_EFS_TOKENIZER

DESCRIPTION   The is the function that reads data from the opened file.
              The data read is looked for tokens 
              1. Each token is seperated by ';'
              2. Successive ';' means empty token
              3. Line begining with '#' is comment
              4. '\n' is the end of token and record
              5. Empty line is ignored
              6. Insufficient tokens is a record is considered bad record
              
DEPENDENCIES  File should have already been opened.

RETURN VALUE   
              SUCCESS : Success => Found Token.
                        *begin points to the begining of token.
                        *end points to the end of token.
              EOL     : End of line is reached => record end 
                        => no token extracted
              END     : End of file is reached => file end => feof
                        => no token extracted
              FAILURE : Failed 

SIDE EFFECTS  None.
===========================================================================*/
ds_efs_token_parse_status_enum_type ds_efs_tokenizer
(
  ds_efs_token_type *sm,
  char **begin,
  char **end
)
{
  int bytes_read = 0;
  char *dummy;
  ds_efs_token_parse_status_enum_type retval;

  *begin = 0;
  *end   = 0;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
 
  
  /*------------------------------------------------------------x------------
     Traversed to end of file => return 
  ---------------------------------------------------------------------------*/
  if( sm->eof ) 
  {
    return DS_EFS_TOKEN_PARSE_EOF;
  }
   
  /*------------------------------------------------------------------------
     Have some bytes to read from the buffer
  ---------------------------------------------------------------------------*/
  while( sm->curr < sm->end_pos ) 
  {

    /*----------------------------------------------------------------------
      Skip over all carriage return characters (\r) added if file was
      editted using a windows machine
    -----------------------------------------------------------------------*/

    if (*sm->curr == '\r')         
    {
      sm->skip_line = FALSE;
      sm->curr++;
      continue;
    }

    /*-----------------------------------------------------------------------
       Lines begining the record with # are comments. Continue to read 
       until we reach the end of file.
    -----------------------------------------------------------------------*/
    if( sm->bol && *sm->curr ==  '#' ) 
    {
      sm->skip_line = TRUE;
      sm->bol = FALSE;
      sm->curr++;
      continue;
    } 

    if( sm->skip_line )                 /* reading a comment */
    {
      if( *sm->curr == '\n' )           /* End of comment */
      {  
        sm->skip_line = FALSE;
        sm->eol = TRUE;
        sm->bol = TRUE;
      }
      sm->curr++;
      continue;                         /*Continue to read until the end of line */
    }
    
    /*--------------------------------------------------------------------------
      Look for the token. If ';' found at the begining then it is 
      an empty token.
      There could be a  case where we hit '\n' while we are looking for a token
      so skip over all the new lines.
    ----------------------------------------------------------------------------*/
    if( *begin == 0 )                   /* Find the beginning of token */
    {                          
      if( *sm->curr == sm->seperator )  /* an empty token */
      {                             

        if( sm->bol == TRUE ) 
        {
          sm->bol = FALSE;
        }
        
        *begin = sm->curr;
        *end   = sm->curr;
        sm->curr++;
        return DS_EFS_TOKEN_PARSE_SUCCESS;
      }

      if( *sm->curr == '\n' )           /* Possibly an empty token */
      {    
        if( sm->eol )                   /* Skip over any successive new lines */
        {     
          sm->curr++;
          continue;
        }
        *begin  = sm->curr;
        *end    = sm->curr;
        sm->eol = TRUE;
        sm->bol = TRUE;
        sm->curr++;
        return DS_EFS_TOKEN_PARSE_SUCCESS;
      }

      /*-------------------------------------------------------------------------
       Before beginning a new token, return an end of record for previous record. 
      --------------------------------------------------------------------------*/
      if( sm->eol ) 
      {                             
        sm->eol = FALSE;
        return DS_EFS_TOKEN_PARSE_EOL;
      }

      *begin = sm->curr;                /* Initialize to beginning of token */
    }
    else if( *sm->curr == sm->seperator || *sm->curr == '\n' )
    {
      *end = sm->curr++;                /* Found end of token */
      
      /*--------------------------------------------------------------------------
         This is a end of line. Save the state and send 
         end of line event when a next token is requested .
      --------------------------------------------------------------------------*/
      if( **end == '\n' ) 
      {       
        sm->eol = TRUE;
        sm->bol = TRUE;
      }
      return DS_EFS_TOKEN_PARSE_SUCCESS;
    }
    
    sm->curr++;
  }/* while */

  /*-------------------------------------------------------------------------- 
    In the middle of token and we ran out characters in the buffer 
  --------------------------------------------------------------------------*/
  if( *begin ) 
  {      
    
    if( *begin != sm->buffer )
    {
      /*---------------------------------------------------------------------- 
        Move the partial token over to beginning of buffer 
      -----------------------------------------------------------------------*/
      /*lint -e732 */
      memscpy( sm->buffer,DS_EFS_READ_BUFFER_SZ, *begin, (sm->curr - *begin) );
      /*lint +e732 */
      sm->curr = sm->buffer + (sm->curr - *begin);
      *begin = sm->buffer;
    }
    else 
    {
      DATA_3GMGR_MSG0( MSG_LEGACY_HIGH, "Token is larger than DS_EFS_READ_BUFFER_SZ" );
      return DS_EFS_TOKEN_PARSE_FAILURE;
    }
  }
  else 
  {
    /*--------------------------------------------------------------------
      No token or data exists in the buffer 
    ---------------------------------------------------------------------*/
    sm->curr = sm->buffer;
  }
  
  /*----------------------------------------------------------------------
      Read data from the efs file.
  -----------------------------------------------------------------------*/
  {

    /*---------------------------------------------------------------- 
      efs_get has to be performed only once, If it has already been
      performed return EOF
    ------------------------------------------------------------------*/
    if(!sm->efs_get_performed)
    {
      DATA_3GMGR_MSG0( MSG_LEGACY_HIGH, "IN EFS GET");
      bytes_read = efs_get((const char*)sm->efs_file_path,(void *)
                           sm->curr,sm->read_buffer_size);
      if(bytes_read <= 0)
      {
        DATA_3GMGR_MSG0( MSG_LEGACY_HIGH, "Bytes incorrectly read");
        return DS_EFS_TOKEN_PARSE_FAILURE;
      }
      sm->efs_get_performed = TRUE;
    }
    else
    {
      sm->eof = TRUE;
      DATA_3GMGR_MSG0( MSG_LEGACY_HIGH, "END of EFS GET");
      return DS_EFS_TOKEN_PARSE_EOL;
    }

  }
    
    if( bytes_read > 0 ) 
    {
      sm->end_pos = sm->curr + bytes_read;
      sm->eof    = FALSE;
      
      if(*begin != 0)
      {
        retval= ds_efs_tokenizer( sm, &dummy, end ); /* Call the function 
                                              again because you could be in the
                                              middle of reading a token */
      }
      else
      {
        retval = ds_efs_tokenizer( sm, begin, end);
      }

      return retval;
    }
    else 
    {

      /*
        No bytes read => reached the end of file.
      */
      if(*begin == 0) 
      {
        sm->eof = 1;
        return DS_EFS_TOKEN_PARSE_EOL;
      }
      else
      {

        /*------------------------------------------------------------------
          If a token was found return the token and 
          when next token is requested send EOF 
        --------------------------------------------------------------------*/
        *end = sm->curr;
        if(bytes_read == 0)
        {
          /*---------------------------------------------------------------
           If the EOF character is missing in the file and the bytes read
           are zero all the time then we are trying to bail out of this.
           
           NOTE: We might have to revisit this later again if different 
           modules
           
          ----------------------------------------------------------------*/
          sm->eof = 1;
          return DS_EFS_TOKEN_PARSE_EOL;
        }
        return DS_EFS_TOKEN_PARSE_SUCCESS;
      }      
  }/* End of bytes read*/
}/* ds_efs_tokenizer() */

/*==============================================================================
FUNCTION DS_UTIL_CALCULATE_THROUGHPUT_MOVING_AVG

DESCRIPTION 
  This function calculates the moving average of the throughput values 
  given the sampling interval.
  This can be used to calculate both DL/UL throughput for the bearers/MAC ID's
 
PARAMETERS 
   uint32               prev_avg_throughput,
   uint64               prev_total_rx_bytes,
   uint64               current_total_rx_bytes,
   uint64               sampling_interval
 
DEPENDENCIES
    None
RETURN VALUE
  uint32 - updated moving average throughput

SIDE EFFECTS
  None
===========================================================================*/
uint32 ds_util_calculate_throughput_moving_avg
(
   uint32     prev_avg_throughput,
   uint64     prev_total_rx_bytes,
   uint64     current_total_rx_bytes,
   uint64     sampling_interval,
   uint8      moving_avg_coeff
)
{
  uint64 current_rate          =  0; 
  uint64 num_rx_bytes          =  0;
  uint64 num_rx_kbits          =  0;
  uint32 new_avg               =  0;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /*------------------------------------------------------------------------- 
    Calculate the total bytes received/transferred in the current sampling
    interval
    -------------------------------------------------------------------------*/
  if(current_total_rx_bytes >= prev_total_rx_bytes)
  {
    num_rx_bytes  = (current_total_rx_bytes - prev_total_rx_bytes);
  }
  else
  {
    /*---------------------------------------------------------------------- 
      (uint64) Integer overflow. This may happen if the bearer is active
      for a sufficiently longer time
      ----------------------------------------------------------------------*/
    num_rx_bytes =(MAX_UINT64 - prev_total_rx_bytes)+(current_total_rx_bytes);
  }
  /*------------------------------------------------------------------------- 
    From total bytes received, calculate total number of kilo bits rx'ed
    1 Byte = 8 bits
    1 kbit = 1024 bits
    and convert the result in kbits
    -------------------------------------------------------------------------*/
  num_rx_kbits = DSUTIL_BYTES_TO_KBITS(num_rx_bytes); 

  /*------------------------------------------------------------------------- 
    Calculate the throughput rate for the current sampling interval
    -------------------------------------------------------------------------*/
  if(sampling_interval > 0)
  {
    /*-----------------------------------------------------------------------
      From the total bits rx'ed and the current sampling interval,
      calculate the equivalent throughput per sec
     
      RATE PER SECOND (kbps)  =    (Total bits received * 1000 ( in ms))
                                   -------------------------------------
                                    Sampling Interval ( in ms)
      -----------------------------------------------------------------------*/
    current_rate  = ((num_rx_kbits * 1000) / (sampling_interval));


  }
  else
  {
    current_rate = 0;
  }
  /*-------------------------------------------------------------------------- 
    Compute the running average of the actual throughput
    --------------------------------------------------------------------------*/
  new_avg = (uint32)ds_util_calculate_moving_avg(prev_avg_throughput,
                                               current_rate,
                                               moving_avg_coeff);
  return new_avg;
}
/*===========================================================================
FUNCTION      DS_EFSIF_CTOI

DESCRIPTION   This function will convert character to interger.

DEPENDENCIES  None.

RETURN VALUE  Integer value
             
SIDE EFFECTS  None.
===========================================================================*/
__inline int ds_efsif_ctoi( char c )
{
  int n = 0;

  if( c >= 'A' && c <= 'F' ) 
  {
    n =  ( c - 'A' ) + 10;
  }
  else if ( c >= 'a' && c <= 'f' ) 
  {
    n =  ( c - 'a' ) + 10;
  }
  else if( c >= '0' && c <= '9' )
  {
    n = ( c - '0' ) + 0;
  }

  return n;
}
/*===========================================================================
FUNCTION      DS_EFSIF_HEXATOI

DESCRIPTION   Converts a hexadecimal string to decimal equivalent.

DEPENDENCIES  None.

RETURN VALUE  Return decimal equivalent
             
SIDE EFFECTS  None.
===========================================================================*/

int ds_efsif_hexatoi(const char *x)
{
  int nsystem = 10; /* Binary system.*/
  int z = 0;

  if( *x == '0' && *(x+1) && ( *(x+1) == 'x' || *(x+1) == 'X' ) )
  {
    /*
     Hexa decimal system. Always look for 0x before we read it
     as hexa decimal number.
    */
    nsystem = 16;
    x += 2;
  }

  while( *x )
  {         
    if(*x != '\r')
    {
      z = z * nsystem + ds_efsif_ctoi(*x);
    }
    (x)++;                    
  }
  
  return z;
}

/*===========================================================================
FUNCTION      DS_EFSIF_READ_A_RECORD

DESCRIPTION   Read a record including number of fields

DEPENDENCIES  None.

RETURN VALUE  Status to indicate if failed or Success or others
             
SIDE EFFECTS  None.
===========================================================================*/
ds_efs_token_parse_status_enum_type ds_efsif_read_a_record( 
  ds_efs_token_type *sm,
  char rec[][MAX_FIELD_LEN], uint16 num_fields,
  uint16 *valid_fields )
{
  ds_efs_token_parse_status_enum_type ret_val;
  char  *from, *to;
  uint16 current_field = 0;
  uint32 current_field_len;

  while( DS_EFS_TOKEN_PARSE_EOF != (ret_val=ds_efs_tokenizer(sm, &from, &to))
         && DS_EFS_TOKEN_PARSE_EOL != ret_val ) {

    if(( current_field < num_fields ) &&
       ( from != NULL))
      {
        current_field_len = (uint32) MIN((to-from), (MAX_FIELD_LEN-1));
        memscpy(rec[current_field],(MAX_FIELD_LEN-1), from, current_field_len);
        rec[current_field][current_field_len] = '\0';
        current_field++;
      }
    else {
      break;
    }
  }

  *valid_fields = current_field;

  return ret_val;
}

/*===========================================================================
FUNCTION      ds_cc_sp_throttling_get_token_id

DESCRIPTION   This function returns the token id associated with each pdn
              throttle config parameter. 

PARAMETERS    char *from - start of the token (param name)
              char *to   - end of the token (param name)
              *token_id  - Pointer in which Token ID is returned

DEPENDENCIES  None

RETURN VALUE  0  - success
              -1 - failure

SIDE EFFECTS  None
===========================================================================*/
int16 ds_cc_sp_throttling_get_token_id
(  
  char *from,
  char *to,
  uint8 *token_id
)
{
  uint8 i;		/* counter for number of params */
  uint8 length;		/* length of the token (param name) */
  int16 atoi_result;
  int16 first_free_index = -1;
  boolean found = FALSE;
  /*-----------------------------------------------------------------------*/
  DSUTIL_ATOI(from,to,atoi_result);
  
  DATA_3GMGR_MSG1(MSG_LEGACY_HIGH, "in ds_cc_sp_throttling_get_token_id %d", atoi_result);

  length =(uint8)(to-from);
  if(atoi_result == 0 || length > MAX_CAUSE_CODE_NAME_LENGTH)
  {
   return -1;
  }
  for(i=0;i<MAX_NUM_THROTTLE_CAUSE_CODES;i++)
  {
    if(cc_sp_throttle_timer_table[i] == NULL&& first_free_index == -1)
    {
     first_free_index = i;
    }
    else if((cc_sp_throttle_timer_table[i] != NULL) && 
          (cc_sp_throttle_timer_table[i]->cause_code == atoi_result))
    {
      found = TRUE;
      *token_id = i;
       DATA_3GMGR_MSG0(MSG_LEGACY_ERROR, "entry already present ");
       break;
    }
  }
  if(found)
  {
    return -1;
  }
  *token_id = first_free_index;
  return 0;
}

/*===========================================================================
FUNCTION      ds707_data_sess_get_token_id

DESCRIPTION   This function returns the token id associated with each pdn
              throttle config parameter. 

PARAMETERS    char *from - start of the token (param name)
              char *to   - end of the token (param name)
              *token_id  - Pointer in which Token ID is returned

DEPENDENCIES  None

RETURN VALUE  0  - success
              -1 - failure

SIDE EFFECTS  None
===========================================================================*/
int16 ds_pdn_throttle_get_token_id
(
  char *from,
  char *to,
  uint8 *token_id
)
{
  int16 ret_val = -1; /* return value */
  uint8 i;            /* counter for number of params */
  uint8 length;       /* length of the token (param name) */
  /*-----------------------------------------------------------------------*/

  /* Get the length of the string that is passed */
  if  ( from > to )  
  {
    DATA_3GMGR_MSG0(MSG_LEGACY_ERROR, "Invalid Parameters");
    return ret_val;
  }

  /* Assumption that the token length will not be greater than 255 */
  length =(uint8)(to-from);

  for (i=0; i<MAX_NUM_PDN_THROTTLE_PARAMS; i++)
  {
    if (length == strlen(pdn_throttle_param_name[i]))
    {
      if (0 == strncmp(from,pdn_throttle_param_name[i],length))
      {
        *token_id = i;
        ret_val = 0;
        break;
      }
    }
  }
  return ret_val;

} /* ds707_data_sess_get_token_id() */
/*===========================================================================
FUNCTION      ds_cc_sp_throttle_config_with_token_contents

DESCRIPTION   This function gets the data for each token and populates the 
              structure with the appropriate data that corresponds to the 
              token number.   

PARAMETERS    token_id -  describes the current token which is 
                          being populated into the EFS structure

              *from, *to - start and end of the character array which 
                           holds the data to populate the profile structure

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds_cc_sp_throttle_config_with_token_contents
(
  uint8                        token_id,
  char                        *from,
  char                        *to,
  cc_sp_throttle_cfg_item_e_type cfg_item
)
{
  int                 atoi_result;
  /*-----------------------------------------------------------------------*/
  DATA_3GMGR_MSG1(MSG_LEGACY_HIGH, "ds_cc_sp_throttle_config_with_token_contents: %d", cfg_item);

  switch(cfg_item)
  {
    case CAUSE_CODE_NUMBER:
    {
      DSUTIL_ATOI(from,to,atoi_result);
      if(atoi_result == 0)
      {
        break;
      }
      if((token_id < MAX_NUM_THROTTLE_CAUSE_CODES) &&
         (cc_sp_throttle_timer_table[token_id] == NULL ))
      {
        cc_sp_throttle_timer_table[token_id] = 
 	    (throttle_timer_type*)modem_mem_alloc(sizeof(throttle_timer_type), MODEM_MEM_CLIENT_DATA);
        cc_sp_throttle_timer_table[token_id]->cause_code = atoi_result;
      }
    }
    break;
    case MAX_FAILURE_COUNTER:
    {
  
      DSUTIL_ATOI(from,to,atoi_result);
      if(atoi_result == 0)
      {
        break;
      }
      if((token_id < MAX_NUM_THROTTLE_CAUSE_CODES) && 
         (cc_sp_throttle_timer_table[token_id] != NULL ))
      {
       cc_sp_throttle_timer_table[token_id]->fail_count = atoi_result;
      }
    }
    break;
    case TIMER_VALUE:
    {  	  
        DSUTIL_ATOI(from,to,atoi_result);
        if(atoi_result == 0)
        {
         break;
        }
      if((token_id < MAX_NUM_THROTTLE_CAUSE_CODES) && 
         (cc_sp_throttle_timer_table[token_id] != NULL ))
        {
          cc_sp_throttle_timer_table[token_id]->timerval= atoi_result;
        }
      
    }
  break;
  default:
    DATA_3GMGR_MSG0(MSG_LEGACY_ERROR, "invalid option");
  }
  return;
} /* ds_fill_pdn_throttle_config_with_token_contents() */

/*===========================================================================
FUNCTION      DS_FILL_PDN_THROTTLE_CONFIG_WITH_TOKEN_CONTENTS

DESCRIPTION   This function gets the data for each token and populates the 
              structure with the appropriate data that corresponds to the 
              token number.   

PARAMETERS    token_id -  describes the current token which is 
                          being populated into the EFS structure

              *from, *to - start and end of the character array which 
                           holds the data to populate the profile structure

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds_fill_pdn_throttle_config_with_token_contents
(
  uint8                        token_id,
  char                        *from,
  char                        *to
)
{
  int                 atoi_result;
  /*-----------------------------------------------------------------------*/
  if(/*FAILURE_TIMER_1 <= token_id &&*/ FAILURE_TIMER_10 >= token_id)
  {
    DSUTIL_ATOI(from,to,atoi_result);
    pdn_throttle.failure_timer[token_id] = (uint32)atoi_result;
  }

  else if((DISALLOW_TIMER_1 <= token_id) && (DISALLOW_TIMER_6 >= token_id))
  {
    DSUTIL_ATOI(from,to,atoi_result);
    pdn_throttle.disallow_timer[token_id - DS_PDN_THROTTLE_MAX_FAILURE_TIMERS]
                                                    = (uint32)atoi_result;
  }

  else
  {
    DATA_3GMGR_MSG0(MSG_LEGACY_ERROR, "Invalid token");
  }

  return;
} /* ds_fill_pdn_throttle_config_with_token_contents() */

/*===========================================================================
FUNCTION      DS_READ_CC_SP_THROTTLE_CONFIG_FROM_EFS

DESCRIPTION   This function will read from the efs file all the necessary 
              data & fill the PDN throttle config structure. Only valid data 
              is populated.

              EFS File Format - Param_Name:Param_Val;
              For example     - FAILURE_TIMER_1:0;             

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None

===========================================================================*/
void ds_read_cc_sp_throttle_config_from_efs(void)
{
  ds_efs_token_type  efs_db; /* structure used for tokenizing the file*/
  char               *from, *to; /* ptrs to start and end of the token */
  ds_efs_token_parse_status_enum_type  ret_val = DS_EFS_TOKEN_PARSE_SUCCESS;
  boolean param_name = TRUE; /* flag to keep track of the parser state 
			TRUE - parser expects Token Name in the EFS								   
                        FALSE - parser expects Token Val in the EFS*/
  uint8 token_id = 0;
  uint8 param_num = 0;
  uint16 length;
  int16 atoi_result;
  char file_name[] = "/nv/item_files/modem/data/epc/cc_sp_throttle_timer.txt";
/*-----------------------------------------------------------------------------------------*/
  DATA_3GMGR_MSG0(MSG_LEGACY_HIGH, "in ds_read_cc_sp_throttle_config_from_efs");

  memset((void*)&cc_sp_throttle_timer_table, 0, sizeof(cc_sp_throttle_timer_table));
  if(ds_efs_file_init(file_name, &efs_db) == -1)
  {
    DATA_3GMGR_MSG0(MSG_LEGACY_HIGH, "Failure reading PDN throttling cnfg");
    return ;
  }
  /*read NV value*/
  /* Set the seperator as : */
  efs_db.seperator = ':';
  while (DS_EFS_TOKEN_PARSE_EOF 
			!= (ret_val = ds_efs_tokenizer(&efs_db, &from, &to )))
  {
    /*------------------------------------------------------------------------
  	Token being read. 'from' points to the beginning of the token and 
  	'to' point to the end of the token.
  
  	The tokenizer automatically skips blank lines and comments (lines 
  	beginning with #, so no need to check for them here).
    ------------------------------------------------------------------------*/
    if((from == to) || (DS_EFS_TOKEN_PARSE_EOL == ret_val))
    {
  	/*----------------------------------------------------------------------
  	  Skip empty tokens.
  	----------------------------------------------------------------------*/
  	continue;
    } 	
    else if(DS_EFS_TOKEN_PARSE_SUCCESS == ret_val)
    {
      if( param_name == TRUE)
      {
        length =(uint8)(to-from);
        if((0 != strncmp(from,"enable",length)) && (0 != strncmp(from,"ENABLE",length)))
        {
	     DATA_3GMGR_MSG0(MSG_LEGACY_HIGH, "config file is not configured properly, enable flag not found");
	     return;
        }
	    efs_db.seperator = ';';
	    param_name = FALSE;
      }
      else
      {
        length =(uint8)(to-from);
        DSUTIL_ATOI(from,to,atoi_result);
        if(atoi_result == 1)
        {
         ds_3gpp_throt_sm_enable_cc_sp_throttling();
        }
  	break;
      }
    }
  }
    /* Set the seperator as : */
  param_name = TRUE;
  efs_db.seperator = ':';
  while (DS_EFS_TOKEN_PARSE_EOF 
		!= (ret_val = ds_efs_tokenizer(&efs_db, &from, &to )))
  {
	  /*------------------------------------------------------------------------
		Token being read. 'from' points to the beginning of the token and 
		'to' point to the end of the token.
  
		The tokenizer automatically skips blank lines and comments (lines 
		beginning with #, so no need to check for them here).
	  ------------------------------------------------------------------------*/
    if((from == to) || (DS_EFS_TOKEN_PARSE_EOL == ret_val))
    {
	/*----------------------------------------------------------------------
	  Skip empty tokens.
	----------------------------------------------------------------------*/
	 continue;
    } 	
    else if(DS_EFS_TOKEN_PARSE_SUCCESS == ret_val)
    {
      if( param_name == TRUE)
      {
        length =(uint8)(to-from);
        if((0 != strncmp(from,"unblocktimer",length)) && (0 != strncmp(from,"UNBLOCKTIMER",length)))
        {
	     DATA_3GMGR_MSG0(MSG_LEGACY_HIGH, "config file is not configured properly, enable flag not found");
	     break;
        }
	    efs_db.seperator = ';';
	    param_name = FALSE;
      }
      else
      {
        length =(uint8)(to-from);
        DSUTIL_ATOI(from,to,atoi_result);
        if(atoi_result >=0)
        {
         ds_set_plmn_unblock_timer_val(atoi_result);
        }
  	    break;
      }
    }
  }
  efs_db.seperator = ':';
  /*lint -save -esym(644,token_id) param_name boolean flag takes care of this */
  while ((param_name)||(DS_EFS_TOKEN_PARSE_EOF 
		!= (ret_val = ds_efs_tokenizer(&efs_db, &from, &to ))))
  {
   param_name = FALSE;
	  /*------------------------------------------------------------------------
		Token being read. 'from' points to the beginning of the token and 
		'to' point to the end of the token.
  
		The tokenizer automatically skips blank lines and comments (lines 
		beginning with #, so no need to check for them here).
	  ------------------------------------------------------------------------*/
    if((from == to) || (DS_EFS_TOKEN_PARSE_EOL == ret_val))
    {
	/*----------------------------------------------------------------------
	  Skip empty tokens.
	----------------------------------------------------------------------*/
	 continue;
    } 	
    else if(DS_EFS_TOKEN_PARSE_SUCCESS == ret_val)
    {
	/*--------------------------------------------------------------------- 
	check if we are looking for param name or param value 
	---------------------------------------------------------------------*/
      if (param_num < 2 )
      {
 	  /*------------------------------------------------------------------
 	  get the token identifier for this param name 
 	  ------------------------------------------------------------------*/
        if (param_num == 0)
        {
       	  if( ds_cc_sp_throttling_get_token_id(from,to,&token_id) < 0)
          {
     	    /* This is an error scenario, Skip till the end of the line? */
           DATA_3GMGR_MSG0( MSG_LEGACY_ERROR, "Incorrect Param Name" );
          }
     	  else
     	  {
     	    efs_db.seperator = ':';
    	    param_num++;
     	    ds_cc_sp_throttle_config_with_token_contents(token_id, from, to, CAUSE_CODE_NUMBER);
     	  }
        } 
        else if(param_num == 1)
        {
          efs_db.seperator = ';';
          param_num++;
         ds_cc_sp_throttle_config_with_token_contents(token_id, from, to, MAX_FAILURE_COUNTER);
        }
 	else
 	{
          DATA_3GMGR_MSG1( MSG_LEGACY_ERROR, "wrong input Token Id: %d", token_id );
         /*-----------------------------------------------------------------
          set param_name as FALSE This means the next token is a 
          param value
          -----------------------------------------------------------------*/
	  param_num++;
          efs_db.seperator = ';';
	  continue;
        }
 	  
      }
    /*---------------------------------------------------------------------
    This means that the token is a param value
     ---------------------------------------------------------------------*/
      else 
      {
        /*-------------------------------------------------------------------
          pass in the identifier and param value to fill the PDN throttle 
        config structure
         ------------------------------------------------------------------*/
        ds_cc_sp_throttle_config_with_token_contents( token_id, 													   
                                                     from, to,
                                                     TIMER_VALUE);
       /*-------------------------------------------------------------------
        set param_name as TRUE This means that next token is a param name
        -------------------------------------------------------------------*/
        param_num = 0;
	  /* Set the seperator as : */
        efs_db.seperator = ':';
      }
   }
  } /* end of while loop */

}

/*===========================================================================
FUNCTION      DS_READ_PDN_THROTTLE_CONFIG_FROM_EFS

DESCRIPTION   This function will read from the efs file all the necessary 
              data & fill the PDN throttle config structure. Only valid data 
              is populated.

              EFS File Format - Param_Name:Param_Val;
              For example     - FAILURE_TIMER_1:0;             

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds_read_pdn_throttle_config_from_efs (void)
{

  ds_efs_token_type  efs_db; /* structure used for tokenizing the file*/
  char *from, *to; /* ptrs to start and end of the token */
  ds_efs_token_parse_status_enum_type  ret_val 
                                              = DS_EFS_TOKEN_PARSE_SUCCESS;
  boolean param_name = TRUE; /* flag to keep track of the parser state 
                                TRUE - parser expects Token Name in the EFS
                                FALSE - parser expects Token Val in the EFS*/
  uint8 token_id = 0;
  uint32 idx;
  char file_name[] 
                = "/nv/item_files/modem/data/epc/pdn_throttling_config.txt";
  /*-----------------------------------------------------------------------*/

  /*-------------------------------------------------------------------------
    Init to default values
  -------------------------------------------------------------------------*/
  memset((void*)&pdn_throttle.failure_timer,0,
         (sizeof(uint32)*DS_PDN_THROTTLE_MAX_FAILURE_TIMERS));
  memset((void*)&pdn_throttle.disallow_timer,0,
          (sizeof(uint32)*DS_PDN_THROTTLE_MAX_DISALLOW_TIMERS));

  if(ds_efs_file_init(file_name, &efs_db) == -1)
  {
    DATA_3GMGR_MSG0(MSG_LEGACY_HIGH, "Failure reading PDN throttling cnfg");
    return;
  }

  /* Set the seperator as : */
  efs_db.seperator = ':';
/*lint -save -esym(644,token_id) param_name boolean flag takes care of this */
  while (DS_EFS_TOKEN_PARSE_EOF 
          != (ret_val = ds_efs_tokenizer(&efs_db, &from, &to )))
  {
    /*------------------------------------------------------------------------
      Token being read. 'from' points to the beginning of the token and 
      'to' point to the end of the token.

      The tokenizer automatically skips blank lines and comments (lines 
      beginning with #, so no need to check for them here).
    ------------------------------------------------------------------------*/
    if((from == to) || (DS_EFS_TOKEN_PARSE_EOL == ret_val))
    {
      /*----------------------------------------------------------------------
        Skip empty tokens.
      ----------------------------------------------------------------------*/
      continue;
    }     
    else if(DS_EFS_TOKEN_PARSE_SUCCESS == ret_val)
    {
      /*--------------------------------------------------------------------- 
      check if we are looking for param name or param value 
      ---------------------------------------------------------------------*/
      if (param_name == TRUE)
      {
        /*------------------------------------------------------------------
        get the token identifier for this param name 
        ------------------------------------------------------------------*/
        if (ds_pdn_throttle_get_token_id(from,to,&token_id) < 0)
        {
          /* This is an error scenario, Skip till the end of the line? */
          DATA_3GMGR_MSG0( MSG_LEGACY_ERROR, "Incorrect Param Name" );
        }
        else
        {
          DATA_3GMGR_MSG1( MSG_LEGACY_HIGH, "Token Id: %d", token_id );
          /*-----------------------------------------------------------------
          set param_name as FALSE This means the next token is a 
          param value
          -----------------------------------------------------------------*/
          param_name = FALSE;
          /* set the seperator as ; */
          efs_db.seperator = ';';
        }
        
      }
      /*---------------------------------------------------------------------
      This means that the token is a param value
      ---------------------------------------------------------------------*/
      else 
      {
        /*-------------------------------------------------------------------
         pass in the identifier and param value to fill the PDN throttle 
         config structure
         ------------------------------------------------------------------*/
        ds_fill_pdn_throttle_config_with_token_contents( token_id,
                                                         from, to                                                         
                                                       );
        /*-------------------------------------------------------------------
         set param_name as TRUE This means that next token is a param name
        -------------------------------------------------------------------*/
        param_name = TRUE;
        /* Set the seperator as : */
        efs_db.seperator = ':';
      }
    }
  } /* end of while loop */

  /*-------------------------------------------------------------------------
   If global throttling not enabled populate 7-10 indexes with 6th failure 
   timer value. This is required, because the failure_cnt will be incremented
   till DS_PDN_THROTTLE_MAX_FAILURE_TIMERS even if global throttling is not 
   enabled.
  -------------------------------------------------------------------------*/
  if( (!ds_3gpp_throt_sm_get_global_throttling_enabled()) )
  {
    for(idx = DS_PDN_THROTTLE_MAX_DISALLOW_TIMERS; 
        idx < DS_PDN_THROTTLE_MAX_FAILURE_TIMERS; idx++)
    {
      pdn_throttle.failure_timer[idx] = 
                     pdn_throttle.failure_timer[idx-1];
    }
  }
 
  for(idx = 0; idx < DS_PDN_THROTTLE_MAX_FAILURE_TIMERS; idx++)
  {
    DATA_3GMGR_MSG2( MSG_LEGACY_HIGH, "pdn_throttle.failure_timer[%d]: %d",
                     idx, pdn_throttle.failure_timer[idx] );
  }

/*lint -restore (644,token_id)*/
  ds_efs_file_close(&efs_db);
  return;

} /* ds_read_pdn_throttle_config_from_efs() */

/*===========================================================================
FUNCTION      DS_CC_SP_THROTTLE__IS_CAUSE_CODE_CONFIGURED

DESCRIPTION   This function return true if given cause code is configured in 
              cc_sp_throttle_timer.txt file

PARAMETERS    cause code - PDP activation reject cause

RETURN VALUE  true- if cause code configured, false otherwise

SIDE EFFECTS  None
SIDE EFFECTS  None
===========================================================================*/
boolean ds_cc_sp_throttle_is_cause_code_configured(uint16 cause_code)
{
  /*-----------------------------------------------------------------------*/
  uint16 i;		  /* counter for number of params */
  /*-----------------------------------------------------------------------*/
  for(i=0;i<MAX_NUM_THROTTLE_CAUSE_CODES;i++)
  {
    if((cc_sp_throttle_timer_table[i] != NULL) && 
  	          (cc_sp_throttle_timer_table[i]->cause_code == cause_code))
    {
      return TRUE;
    }
  }
 return FALSE;
}

/*===========================================================================
FUNCTION      DS_GET_CC_SP_THROTTLE__TIMER

DESCRIPTION   This function provides the PDN throttling failure timer value

PARAMETERS    counter - Throttling counter used as an index

RETURN VALUE  PDN throttle Failure Timer value in msec

SIDE EFFECTS  None
===========================================================================*/
uint32 ds_get_cc_sp_throttle_timer (uint16 cause_code, uint8 counter)
{
  /*-----------------------------------------------------------------------*/
  uint16 i;			/* counter for number of params */
  boolean found = FALSE;
  uint32 timer_val = 0;
  /*-----------------------------------------------------------------------*/
  
  for(i=0;i<MAX_NUM_THROTTLE_CAUSE_CODES;i++)
  {
    if((cc_sp_throttle_timer_table[i] != NULL) && 
  	          (cc_sp_throttle_timer_table[i]->cause_code == cause_code))
    {
      found = TRUE;
      DATA_3GMGR_MSG0(MSG_LEGACY_ERROR, "entry already present ");
      break;
    }
  }
  if(found && counter <= cc_sp_throttle_timer_table[i]->fail_count )
  {
    timer_val = cc_sp_throttle_timer_table[i]->timerval;
  }
  else if(found == FALSE)
  {
   return 0;
  }
  else
  {
   return DS_3GPP_PDN_THROT_INFINITE_THROT;
  }
    DATA_3GMGR_MSG2(MSG_LEGACY_HIGH,"returning timer value :%d for failure_count %d",
                  timer_val, counter);
  return SEC_TO_MSEC(timer_val);
} /* ds_get_pdn_throttle_failure_timer() */

uint32 ds_get_plmn_unblock_timer_val(void)
{
  DATA_3GMGR_MSG1(MSG_LEGACY_HIGH,"returning plmn_unblock_timer_val :%d" ,
		  plmn_unblock_timer_val);

  return plmn_unblock_timer_val*1000;
}
/*===========================================================================
FUNCTION      DS_GET_PDN_THROTTLE_FAILURE_TIMER

DESCRIPTION   This function provides the PDN throttling failure timer value

PARAMETERS    counter - Throttling counter used as an index

RETURN VALUE  PDN throttle Failure Timer value in msec

SIDE EFFECTS  None
===========================================================================*/
uint32 ds_get_pdn_throttle_failure_timer (uint8 counter)
{
  /*-----------------------------------------------------------------------*/
  /*-------------------------------------------------------------------------
    Decrement the counter by 1 so that it can be used as an index into timer
    array also check for its validity
  -------------------------------------------------------------------------*/
  counter -= 1;

  if((DS_PDN_THROTTLE_MAX_FAILURE_TIMERS <= counter)/* || (counter < 0) */)
  {
    DATA_3GMGR_MSG1(MSG_LEGACY_ERROR, "Invalid PDN throttle counter value: %d", 
                    counter);
    return 0;
  }

  DATA_3GMGR_MSG2(MSG_LEGACY_HIGH,"returning timer value :%d for failure_count %d",
                  pdn_throttle.failure_timer[counter], counter);

  return SEC_TO_MSEC(pdn_throttle.failure_timer[counter]);
} /* ds_get_pdn_throttle_failure_timer() */

/*===========================================================================
FUNCTION      DS_SET_PDN_THROTTLE_FAILURE_TIMER

DESCRIPTION   This function is to assign PDN throttling failure timer value
              manually

PARAMETERS    counter - Throttling counter used as an index

RETURN VALUE  none

SIDE EFFECTS  None
===========================================================================*/
void ds_set_pdn_throttle_failure_timer (uint8 counter,uint32 timerval)
{
  /*-----------------------------------------------------------------------*/
  /*-------------------------------------------------------------------------
    Decrement the counter by 1 so that it can be used as an index into timer
    array also check for its validity
  -------------------------------------------------------------------------*/
  counter -= 1;

  if((DS_PDN_THROTTLE_MAX_FAILURE_TIMERS <= counter)/* || (counter < 0) */)
  {
    DATA_3GMGR_MSG1(MSG_LEGACY_ERROR, "Invalid PDN throttle counter value: %d", 
                    counter);
    return;
  }
  pdn_throttle.failure_timer[counter] = timerval;

  DATA_3GMGR_MSG2(MSG_LEGACY_HIGH,"assigned timer value :%d for failure_count %d manually",
                  pdn_throttle.failure_timer[counter], counter);

} /* ds_get_pdn_throttle_failure_timer() */

/*===========================================================================
FUNCTION      DS_GET_PDN_THROTTLE_DISALLOW_TIMER

DESCRIPTION   This function provides the PDN throttling disallow timer value

PARAMETERS    counter - Throttling counter used as an index

RETURN VALUE  PDN throttle Disallow Timer value in msec

SIDE EFFECTS  None
===========================================================================*/
uint32 ds_get_pdn_throttle_disallow_timer (uint8 counter)
{
  /*-------------------------------------------------------------------------
    Decrement the counter by 1 so that it can be used as an index into timer
    array also check for its validity
  -------------------------------------------------------------------------*/
  counter -= 1;
  if(DS_PDN_THROTTLE_MAX_DISALLOW_TIMERS <= counter)
  {
    DATA_3GMGR_MSG0(MSG_LEGACY_ERROR, "Invalid PDN throttle counter value");
    return 0;
  }

  return SEC_TO_MSEC(pdn_throttle.disallow_timer[counter]);
} /* ds_get_pdn_throttle_disallow_timer() */

/*===========================================================================
FUNCTION      DS_READ_FAST_DORMANCY_INFO_FROM_EFS

DESCRIPTION   This function will read the efs file all the necessary 
              data & fill the timer values in FD config structure.

              EFS File Format - Param_Name:Param_Val;
              For example     - TIMER_1:0;             

DEPENDENCIES  None

RETURN VALUE  
TRUE- Success
FALSE- Failure

SIDE EFFECTS  None
===========================================================================*/
boolean ds_read_fast_dormancy_info_from_efs (void)
{
  ds_efs_token_type  efs_db; /* structure used for tokenizing the file*/
  char *from, *to; /* ptrs to start and end of the token */
  ds_efs_token_parse_status_enum_type  ret_val 
                                              = DS_EFS_TOKEN_PARSE_SUCCESS;
  boolean param_name = TRUE; /* flag to keep track of the parser state 
                                TRUE - parser expects Token Name in the EFS
                                FALSE - parser expects Token Val in the EFS*/
  boolean token_check = FALSE;
  int    atoi_result;
  char file_name[] 
                = "/nv/item_files/modem/data/3gpp/fast_dormancy_config.txt";
  /*-----------------------------------------------------------------------*/

  if(ds_efs_file_init(file_name, &efs_db) == -1)
  {
    DATA_3GMGR_MSG0(MSG_LEGACY_HIGH, "Failure reading Fast Dormancy cnfg");
    return FALSE;
  }

  /* Set the seperator as : */
  efs_db.seperator = ':';
/*lint -save -esym(644,token_id) param_name boolean flag takes care of this */
  while (DS_EFS_TOKEN_PARSE_EOF 
          != (ret_val = ds_efs_tokenizer(&efs_db, &from, &to )))
  {
    /*------------------------------------------------------------------------
      Token being read. 'from' points to the beginning of the token and 
      'to' point to the end of the token.

      The tokenizer automatically skips blank lines and comments (lines 
      beginning with #, so no need to check for them here).
    -------------------------------------------------------------------*/
    if((from == to) || (DS_EFS_TOKEN_PARSE_EOL == ret_val))
    {
      /*---------------------------------------------------------------
         Skip empty tokens.
      ------------------------------------------------------------------*/
      continue;
    }     
    else if(DS_EFS_TOKEN_PARSE_SUCCESS == ret_val)
    {
      /*-------------------------------------------------------------- 
        check if we are looking for param name or param value 
      -----------------------------------------------------------------*/
      if (param_name == TRUE)
      {
        /*-------------------------------------------------------------
          set param_name as FALSE This means the next token is a 
          param value
        ----------------------------------------------------------------*/
        param_name = FALSE;
        /* set the seperator as ; */
        efs_db.seperator = ';';
      }
      /*---------------------------------------------------------------
         This means that the token is a param value
        ----------------------------------------------------------------*/
      else 
      {
        /*--------------------------------------------------------------
          Read the parama Value from efs file and store in FD structure  
         --------------------------------------------------------------*/
        DSUTIL_ATOI(from,to,atoi_result);

        /* update the corresponding timer values based on token check */
        if(token_check == FALSE)
        {
          ds_3gpp_bearer_cntxt_set_modem_fd_timer_1_value((uint32)atoi_result);
          token_check = TRUE;
        }
        else
        {
          ds_3gpp_bearer_cntxt_set_modem_fd_timer_2_value((uint32)atoi_result);
        }
 
        /*---------------------------------------------------------------
          set param_name as TRUE This means that next token is a param name
          ----------------------------------------------------------------*/
        param_name = TRUE;
       /* Set the seperator as : */
        efs_db.seperator = ':';
      }
    }
  } /* end of while loop */

/*lint -restore (644,token_id)*/
  ds_efs_file_close(&efs_db);
  return TRUE;
}

/*===========================================================================
FUNCTION      DS_READ_FAST_DORMANCY_ENABLED_FLAG_FROM_EFS

DESCRIPTION   This function will read the efs file and gets if Fast 
              Dormancy is enabled or not. If file doesnt exist, by 
              deafult cosiders as FD is disabled

              EFS File Format - Param_Val
              For example     - 1/0             

DEPENDENCIES  None

RETURN VALUE  
None

SIDE EFFECTS  None
===========================================================================*/
void ds_read_fast_dormancy_enabled_flag_from_efs (void)
{
  char   data_conf_info_path[] = 
                         "/ds/qmi/fast_dormancy.txt";
  int   fd =0, efs_ret = 0;
  int result;
  boolean enabled_flag = FALSE;
  /* -------------------------------------------------------------*/
  /*Read FD flag from EFS and restrict FD filter feature based on EFS flag*/
  fd = efs_open(data_conf_info_path,O_RDONLY | O_TRUNC, S_IRUSR );
 
  if (fd < 0)
  {
    ds_3gpp_bearer_cntxt_set_modem_fd_enabled_flag(TRUE);
    DATA_3GMGR_MSG0(MSG_LEGACY_ERROR,"EFS item not configured for fast"
                                        "dormancy, Initializing defaults.");
    return;
  }
  else
  {
    /* reading the EFS Content into global variable */
    efs_ret = efs_read(fd,&(enabled_flag),sizeof(enabled_flag));
	
    if (efs_ret > 0)
    {
      DATA_3GMGR_MSG1(MSG_LEGACY_HIGH, "FD Enabled flag = %d"
                                                  ,enabled_flag);
    }
    ds_3gpp_bearer_cntxt_set_modem_fd_enabled_flag(enabled_flag);
  }
   /* EFS Close */
  result = efs_close (fd);
  if ( 0 != result )
  {
    DATA_3GMGR_MSG1(MSG_LEGACY_ERROR, "Error closing EFS file, "
                    "error %d", efs_errno);
  } 
  return;
}/*ds_read_fast_dormancy_enabled_flag_from_efs*/

/*===========================================================================
FUNCTION DS_CREATE_EFS_CONFIG_FILE

DESCRIPTION
  Create a config file in EFS which stores the path of EFS item files.

DEPENDENCIES
  None

PARAMETERS
  conf_file_path - File path of config file

RETURN VALUE
  0         - Success
 -1         - Non-EFS related Failures
  efs_errno - EFS related failures. Meaning of this value can be
              found in fs_errno.h

SIDE EFFECTS
  None
===========================================================================*/
int32 ds_create_efs_config_file
(
  const char *conf_file_path
)
{
  int32                 config_fd, result;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if ( NULL == conf_file_path )
  {
    DATA_3GMGR_MSG0(MSG_LEGACY_ERROR, "conf_file_path is NULL");
    return -1;
  }
  
  /*-------------------------------------------------------------------------
    Create common directories if needed.
  -------------------------------------------------------------------------*/
  DATA_3GMGR_MSG0(MSG_LEGACY_MED, "EFS: Creating conf file if necessary");
  result = ds_path_is_directory("/nv");
  if( 0 != result )
  {
    /* Directory doesn't exist yet */
    DATA_3GMGR_MSG0(MSG_LEGACY_HIGH, "Create /nv dir in EFS");
    result = efs_mkdir( "/nv", S_IREAD|S_IWRITE|S_IEXEC);
    if ( -1 == result )
    {
      DATA_3GMGR_MSG1(MSG_LEGACY_ERROR, "Create EFS Dir Failed: "
                      "error %d", efs_errno);
      return efs_errno;
    } 
  }

  result = ds_path_is_directory("/nv/item_files");
  if( 0 != result )
  {
    /* Directory doesn't exist yet */
    DATA_3GMGR_MSG0(MSG_LEGACY_HIGH, "Create /nv/item_files dir in EFS");
    result = efs_mkdir( "/nv/item_files", S_IREAD|S_IWRITE|S_IEXEC);
    if ( -1 == result )
    {
      DATA_3GMGR_MSG1(MSG_LEGACY_ERROR, "Create EFS Dir Failed: "
                      "error %d", efs_errno);
      return efs_errno;
    }
  }

  result = ds_path_is_directory("/nv/item_files/conf");
  if( 0 != result )
  {
    /* Directory doesn't exist yet */
    DATA_3GMGR_MSG0(MSG_LEGACY_HIGH, "Create /nv/item_file/conf dir in EFS");
    result = efs_mkdir( "/nv/item_files/conf", S_IREAD|S_IWRITE|S_IEXEC);
    if ( -1 == result )
    {
      DATA_3GMGR_MSG1(MSG_LEGACY_ERROR, "Create EFS Dir Failed: "
                      "error %d", efs_errno);
      return efs_errno;
    }
  }

  /*-------------------------------------------------------------------------
    Open conf file. Create conf file if does not exist.
    Resulting file is truncated to zero bytes.
  -------------------------------------------------------------------------*/  
  config_fd = efs_open (conf_file_path, O_WRONLY|O_CREAT|O_TRUNC, ALLPERMS);
  if ( 0 > config_fd )
  {
    DATA_3GMGR_MSG1(MSG_LEGACY_ERROR, "Error creating config file, "
                    "error %d", efs_errno);
    return efs_errno;
  }

  result = efs_close (config_fd);
  if ( 0 != result )
  {
    DATA_3GMGR_MSG1(MSG_LEGACY_ERROR, "Error closing config file, "
                    "error %d", efs_errno);
    return efs_errno;
  }
  return 0;

} /* ds_create_efs_config_file() */

/*===========================================================================
FUNCTION      DS_INIT_EFS_CONFIG_FILE

DESCRIPTION   Write all required item files to the config file.
              Any new item file must be added directly within this init 
              function.
              This function optimizes multiple calls for item file write from
              different modules into a single efs write call.

DEPENDENCIES  None.

RETURN VALUE  0 for success.
              -1 for failure.

SIDE EFFECTS  None.
===========================================================================*/
int32 ds_init_efs_data_config_file(void)
{
  char   data_conf_info_path[] = "/nv/item_files/conf/data_config_info.conf";
  int  config_fd, result;
  char *file_paths_buf_ptr = NULL;
  char *efs_read_buf_ptr = NULL;
  boolean conf_file_valid = TRUE;
  uint32 efs_conf_file_size;
  int bytes_read = 0, efs_ret = 0;
  struct fs_stat stat_buf;
  int ret_val = 0;
  uint32 next_file_paths_buf_write_pos = 0;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /*-------------------------------------------------------------------------
    Allocate memory to store the item file paths from 3gpp & 3gpp2
  -------------------------------------------------------------------------*/
  efs_conf_file_size =
#ifdef FEATURE_DATA_WLAN_MAPCON
   DS_WLAN_DATA_CONF_BUF_SIZE +
#endif  /* FEATURE_DATA_WLAN_MAPCON */
#ifdef FEATURE_DATA_IS707 
   ds707_pkt_mgr_3gpp2_efs_item_file_path_size() +
#endif  /* FEATURE_DATA_IS707 */
   DS_3GPP_DATA_CONF_BUF_SIZE + ds3g_get_efs_file_path_size(); 
  file_paths_buf_ptr = (char*)ds_system_heap_mem_alloc(efs_conf_file_size);
  if(NULL == file_paths_buf_ptr)
  {
    DATA_3GMGR_MSG0(MSG_LEGACY_ERROR, "Failed to allocate memory");
    return -1;
  }

  memset(file_paths_buf_ptr, 0, efs_conf_file_size);
  /* 3GPP specific items */
  /*-------------------------------------------------------------------------
    ds_3gpp_update_data_config_info function, will always NULL terminate
    file_paths_buf_ptr. In case of success return value is the size of string
    copied to buf_ptr.
  -------------------------------------------------------------------------*/
  result = ds_3gpp_update_data_config_info(file_paths_buf_ptr, efs_conf_file_size);
  if ( -1 == result || result > DS_3GPP_DATA_CONF_BUF_SIZE )
  {
    DATA_3GMGR_MSG0(MSG_LEGACY_ERROR, 
                    "Error storing efs item file paths in buffer");
    ds_system_heap_mem_free(file_paths_buf_ptr);
    return -1;
  }
  next_file_paths_buf_write_pos += result;

  /* DS3G common items */
  /*-------------------------------------------------------------------------
    ds3g_update_data_config_info function, will always NULL terminate
    file_paths_buf_ptr. In case of success return value is the size of string
    copied to buf_ptr.
  -------------------------------------------------------------------------*/
  DATA_3GMGR_MSG1(MSG_LEGACY_HIGH, "file_paths_buf_str length %d", result);
  result = ds3g_update_data_config_info(file_paths_buf_ptr 
                                         + next_file_paths_buf_write_pos,
                                        efs_conf_file_size);

  if ( (-1 == result) || 
       ( (uint32)result > ds3g_get_efs_file_path_size() 
        + DS_3GPP_DATA_CONF_BUF_SIZE))
  {
    DATA_3GMGR_MSG0(MSG_LEGACY_ERROR, 
                    "Error storing efs item file paths in buffer");
    ds_system_heap_mem_free(file_paths_buf_ptr);
    return -1;
  }
  next_file_paths_buf_write_pos += result;

  /*-------------------------------------------------------------------------
    Any additional items that need to be added to the buffer need to
    increment next_file_paths_buf_write_pos in order to keep track of a 
    running total of the string length that has been written to the buffer
  -------------------------------------------------------------------------*/

#ifdef FEATURE_DATA_WLAN_MAPCON  
  /* WLAN specific items */
  result = ds_wlan_update_data_config_info(file_paths_buf_ptr
                                            + next_file_paths_buf_write_pos);
  if ( -1 == result || result > DS_WLAN_DATA_CONF_BUF_SIZE )
  {
    DATA_3GMGR_MSG0(MSG_LEGACY_ERROR,
                    "Error storing WLAN efs item file paths in buffer");
    ds_system_heap_mem_free(file_paths_buf_ptr);
    return -1;
  }

  next_file_paths_buf_write_pos += result;
#endif  /* FEATURE_DATA_WLAN_MAPCON */

#ifdef FEATURE_DATA_IS707
  DATA_3GMGR_MSG1(MSG_LEGACY_HIGH, "file_paths_buf_str length %d", result);
  /* 3GPP2 specific items */
  /*-------------------------------------------------------------------------
    ds_3gpp2_update_data_config_info needs to take in an offset (result)
    that is the current length of the string written into the buffer and
    added it to file_paths_buf_ptr so it writes to the correct position
  -------------------------------------------------------------------------*/
  result = ds_3gpp2_update_data_config_info(file_paths_buf_ptr 
                                            + next_file_paths_buf_write_pos);
  if ( 0 != result )
  {
    DATA_3GMGR_MSG0(MSG_LEGACY_ERROR, 
                    "Error storing efs item file paths in buffer");
    ds_system_heap_mem_free(file_paths_buf_ptr);
    return -1;
  }
  next_file_paths_buf_write_pos += result;
#endif  /* FEATURE_DATA_IS707 */
  /*-------------------------------------------------------------------------
    Open conf file. If it doesn't exist create it and reopen it.
  -------------------------------------------------------------------------*/
  config_fd = efs_open (data_conf_info_path, O_RDWR, ALLPERMS);
  if ( 0 > config_fd )
  {
    if ( ENOENT == efs_errno ) /* Conf file does not exist */
    {
      DATA_3GMGR_MSG0(MSG_LEGACY_ERROR, "EFS: Config file not present");
      DATA_3GMGR_MSG0(MSG_LEGACY_HIGH, "EFS: Creating config file");
      conf_file_valid = FALSE;

      result = ds_create_efs_config_file(data_conf_info_path);
      if ( 0 != result )
      {
        DATA_3GMGR_MSG1(MSG_LEGACY_ERROR, "EFS: Error creating config file, "
                        "error %d", efs_errno);
        ds_system_heap_mem_free(file_paths_buf_ptr);
        return efs_errno;
      }
  
      config_fd = efs_open (data_conf_info_path, O_RDWR|O_APPEND);
      if ( 0 > config_fd )
      {
        DATA_3GMGR_MSG1(MSG_LEGACY_ERROR, "EFS: Error opening config file, "
                        "error %d", efs_errno);
        ds_system_heap_mem_free(file_paths_buf_ptr);
        return efs_errno;
      }
    }
    else /* Could not open conf file for some other reason */
    {
      DATA_3GMGR_MSG1(MSG_LEGACY_ERROR, "Error opening config file, "
                      "error %d", efs_errno);
      ds_system_heap_mem_free(file_paths_buf_ptr);
      return efs_errno;
    }
  }

  /*-------------------------------------------------------------------------
    Check the file size to decide whether writing the file is required or not
  -------------------------------------------------------------------------*/
  if(conf_file_valid)
  {
    if ((0 != efs_fstat(config_fd, &stat_buf)) 
        || (strlen(file_paths_buf_ptr) != stat_buf.st_size))
    {
      DATA_3GMGR_MSG2(MSG_LEGACY_ERROR, "Conf file invalid size %d, expected size %d", 
                      stat_buf.st_size, strlen(file_paths_buf_ptr));
      conf_file_valid = FALSE;
    }
  }

  /*-------------------------------------------------------------------------
    Read the efs config info file if it is valid & compare the contents read
    with the one stored in the dynamic buffer. If they don't match write the
    buffer info into the efs .conf file
  -------------------------------------------------------------------------*/
  if(conf_file_valid)
  {
    efs_read_buf_ptr = (char*)ds_system_heap_mem_alloc(efs_conf_file_size);
    if(NULL == efs_read_buf_ptr)
    {
      DATA_3GMGR_MSG0(MSG_LEGACY_ERROR , "Failed to allocate memory");
      (void)efs_close (config_fd);
      ds_system_heap_mem_free(file_paths_buf_ptr);
      return -1;
    }

    do
    {
      efs_ret = efs_read(config_fd, &efs_read_buf_ptr[bytes_read],
                         (strlen(file_paths_buf_ptr) - bytes_read));
    } while (efs_ret > 0 && (bytes_read += efs_ret) 
             < strlen(file_paths_buf_ptr));

    if((!(bytes_read == strlen(file_paths_buf_ptr)))
       || (0 != memcmp(file_paths_buf_ptr, efs_read_buf_ptr, strlen(file_paths_buf_ptr))))
    {
      DATA_3GMGR_MSG0(MSG_LEGACY_ERROR, "memcmp failed, write into conf file again");
      conf_file_valid = FALSE;
    }
  }

  /*-------------------------------------------------------------------------
    Write the efs item file paths into .conf file if the contents are not
    valid.
  -------------------------------------------------------------------------*/
  if(FALSE == conf_file_valid)
  {
    efs_ftruncate(config_fd, 0);
    efs_lseek(config_fd,0,SEEK_SET);

    DATA_3GMGR_MSG0(MSG_LEGACY_HIGH, "efs_write() called");
    if(efs_write(config_fd, file_paths_buf_ptr, (fs_size_t)strlen(file_paths_buf_ptr))
       != (fs_size_t)strlen(file_paths_buf_ptr))
    {
      DATA_3GMGR_MSG0(MSG_LEGACY_ERROR, 
                      "Failed to write efs file paths into conf file");
      ret_val = -1;
    }
  }

  ds_system_heap_mem_free(file_paths_buf_ptr);
  ds_system_heap_mem_free(efs_read_buf_ptr);

  /* EFS Close */
  result = efs_close (config_fd);
  if ( 0 != result )
  {
    DATA_3GMGR_MSG1(MSG_LEGACY_ERROR, "Error closing config file, "
                    "error %d", efs_errno);
    return efs_errno;
  }
  
  return ret_val;
}/* ds_init_efs_config_file() */

/*===========================================================================
FUNCTION DS_READ_EFS_NV

DESCRIPTION
  This function reads the EFS item from the item file

DEPENDENCIES
  None

PARAMETERS
  item_file_path - File path to item in NV
  nv_info - Struct for NV item(s)
  nv_info_size - Size of NV item structure

RETURN VALUE
  NV_FAIL_S      - 
  NV_NOTACTIVE_S -
  NV_DONE_S      - 
             
SIDE EFFECTS  None.
===========================================================================*/
nv_stat_enum_type ds_read_efs_nv
(
  const char         *item_path,
  void               *data_ptr,
  uint32             item_size
)
{
  int   return_size;
  nv_stat_enum_type read_status = NV_DONE_S;
  struct fs_stat temp_buf;

  if ( (item_path == NULL) || (data_ptr == NULL) )
  {
    DATA_3GMGR_MSG0(MSG_LEGACY_ERROR, "item_path or data_ptr is NULL");
    return NV_FAIL_S;
  }

  DATA_3GMGR_MSG_SPRINTF_2(MSG_LEGACY_HIGH, "Print f_name:%s item_size %d",
                           item_path, item_size);

  memset (data_ptr, 0, item_size);

  /* Check if the file is present or not. */
  if(efs_stat(item_path,&temp_buf) == -1)
  {
    DATA_3GMGR_MSG_SPRINTF_1(MSG_LEGACY_ERROR, "Error opening file: %s", item_path);
    return NV_NOTACTIVE_S;
  }
   
  if((temp_buf.st_size) != (item_size))
  {
    DATA_3GMGR_MSG_SPRINTF_3(MSG_LEGACY_HIGH, 
                    "ds_read_efs_nv: Print f_name:%s item_size %d temp buf"
                    " st_size %d . Performing efs_unlink",
                    item_path, item_size, temp_buf.st_size);
    (void)efs_unlink(item_path);
    return NV_NOTACTIVE_S;
  }
     
  /* Obtain the contents from EFS */
  return_size = efs_get(item_path, data_ptr, item_size);
  
  if (return_size == -1)
  {
    DATA_3GMGR_MSG_SPRINTF_2(MSG_LEGACY_ERROR, 
                            "efs_get: Error opening file: %s, error: %d", 
                             item_path, efs_errno);
    read_status = NV_FAIL_S;
  }
  else
  {
    read_status = NV_DONE_S;
    DATA_3GMGR_MSG0(MSG_LEGACY_HIGH, "DSI_GET_EFS_Item return NV_DONE_S");
  }
  
  return read_status;
}

/*===========================================================================
FUNCTION DS_WRITE_EFS_NV

DESCRIPTION
  This function writes the EFS item to the item file

DEPENDENCIES
  None

PARAMETERS
  item_file_path - File path to item in NV
  nv_info - Pointer to NV item
  nv_info_size - Size of NV item

RETURN VALUE
  0         - Success
 -1         - Non-EFS related Failures
  efs_errno - EFS related failures. Meaning of this value can be
              found in fs_errno.h

SIDE EFFECTS
  None
===========================================================================*/
int32 ds_write_efs_nv
(
  const char   *item_file_path, 
  void         *nv_info_ptr, 
  fs_size_t    nv_info_size
)
{
  int32                 result = 0;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  DATA_3GMGR_MSG0(MSG_LEGACY_MED, "Writing to EFS");

  if ( NULL == item_file_path || NULL == nv_info_ptr )
  {
    DATA_3GMGR_MSG0(MSG_LEGACY_ERROR, 
                    "item_file_path is NULL or nv_info_ptr is NULL");
    return -1;
  }
  /*-------------------------------------------------------------------------
    Write the item into the item file. If file or file path does not exist,
    create the file path and file
  -------------------------------------------------------------------------*/
  result = efs_put( item_file_path, 
                    (void *)nv_info_ptr, 
                    nv_info_size,
                    O_CREAT|O_AUTODIR, 
                    ALLPERMS );
  if( 0 != result )
  {
    DATA_3GMGR_MSG1(MSG_LEGACY_ERROR, "Unable to write EFS item, "
                    "error %d ", efs_errno);
    return efs_errno;
  }

  return 0;
} /* ds_write_efs_nv() */

/*===========================================================================
FUNCTION DS_PATH_IS_DIRECTORY

DESCRIPTION
  To check if the EFS directory exists

DEPENDENCIES
  None

PARAMETERS
  dirname - Directory path

RETURN VALUE
   0         - success
   efs_errno - EFS error
   -1        - Other error

SIDE EFFECTS
  None
===========================================================================*/
int32 ds_path_is_directory
(
  const char *dirname
)
{
  int                           rsp;
  struct fs_stat                stat_info;
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  rsp = efs_lstat( dirname, &stat_info);
  if( 0 != rsp )
  {
    rsp = efs_errno;
    if( ENOENT != rsp )
    {
      DATA_3GMGR_MSG1(MSG_LEGACY_ERROR, "efs_lstat error %d", rsp);
      return rsp;
    }
  }
  else if( S_ISDIR (stat_info.st_mode))
  {
    return 0;
  }

  return -1;
} /* ds_path_is_directory */

/*===========================================================================
FUNCTION DS_GET_TRAFFIC_CHANNEL_TEARDOWN_IN_LINGERING_VAL

DESCRIPTION
  Returns the current value of ds_traffic_channel_teardown_in_lingering.

DEPENDENCIES
  None

PARAMETERS
  None

RETURN VALUE
  boolean - TRUE or FALSE indicating whether to tear down traffic channel or 
            not during lingering.

SIDE EFFECTS
  None
===========================================================================*/
boolean ds_get_traffic_channel_teardown_in_lingering_val( void )
{
  return ds_traffic_channel_teardown_in_lingering;
} /* ds_get_traffic_channel_teardown_in_lingering_val() */

/*===========================================================================
FUNCTION DS_SET_TRAFFIC_CHANNEL_TEARDOWN_IN_LINGERING_VAL

DESCRIPTION
  Updates the current value of ds_traffic_channel_teardown_in_lingering.

DEPENDENCIES
  None

PARAMETERS
  boolean - TRUE or FALSE indicating whether to tear down traffic channel or 
            not during lingering.

RETURN VALUE
   None

SIDE EFFECTS
  None
===========================================================================*/
void ds_set_traffic_channel_teardown_in_lingering_val
(
  boolean current_traffic_chan_teardown_val
)
{
  ds_traffic_channel_teardown_in_lingering = 
                              current_traffic_chan_teardown_val;
} /* ds_set_traffic_channel_teardown_in_lingering_val() */

/*===========================================================================
FUNCTION DS_MATH_CEIL

DESCRIPTION
  To return ceil value.

DEPENDENCIES
  None

PARAMETERS
  dividend - the dividend.
  divisor  - the divisor.

RETURN VALUE
  int32 - quotient.

SIDE EFFECTS
  None
===========================================================================*/
int32 ds_math_ceil
(
  /* dividend */
  int32 dividend,
  /* divisor */
  int32 divisor
)
{
  int32 quotient;

  if (divisor == 0)
  {
    DATA_3GMGR_MSG0(MSG_LEGACY_ERROR, "Divide by zero");
    return 0;
  }

  /* Get exact/truncated value after integer division */
  quotient = dividend/divisor;

  /* Determine integer x such that x <= ceil(x) < x+1 */
  if ((quotient > 0) ||
      ((quotient == 0) && ((DS_SIGN(dividend) == DS_SIGN(divisor))))
     ) // leave ceil(x) truncated for -ve quotients
  {
    if ((divisor * quotient) != (dividend))
      quotient++;  //round towards +infinity
  }

  /* Note that -22/5 = -4, not -5 */
  return quotient;
} /* ds_math_ceil */

/*===========================================================================
FUNCTION DS_MATH_FLOOR

DESCRIPTION
  To return floor value.

DEPENDENCIES
  None

PARAMETERS
  dividend - the dividend.
  divisor  - the divisor.

RETURN VALUE
  int32 - quotient.

SIDE EFFECTS
  None
===========================================================================*/
int32 ds_math_floor
(
  /* dividend */
  int32 dividend,
  /* divisor */
  int32 divisor
)
{
  int32 quotient;

  if (divisor == 0)
  {
    DATA_3GMGR_MSG0(MSG_LEGACY_ERROR, "Divide by zero");
    return 0;
  }

  /* Get exact/truncated value after integer division */
  quotient = dividend/divisor;

  /* Determine integer x such that x-1 < floor(x) <= x */
  if ((quotient < 0) ||
      ((quotient == 0) && (DS_SIGN(dividend) != DS_SIGN(divisor))))  
     // leave floor(x) truncated for +ve quotients
  {
    if (abs(divisor * quotient) != abs(dividend))
      quotient--;  // round towards -infinity
  }

  /* Note that -22/5 = -5, not -4 */
  return quotient;
} /* ds_math_floor */

/*===========================================================================
FUNCTION DS_EFS_NV_INIT

DESCRIPTION
  This function gets all the EFS NV items for the Data task and stores them in
  the appropriate variables.

DEPENDENCIES
  The data config file must have been created

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
void ds_efs_nv_init
(
  void
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
#ifdef FEATURE_DATA_WLAN_MAPCON
  ds_wlan_powerup_init();
#endif /* FEATURE_DATA_WLAN_MAPCON */

#ifdef FEATURE_DATA_IS707
  ds707_pkt_mgr_nv_sync();
#endif /* FEATURE_DATA_IS707 */
} /* ds_efs_nv_init */

/*===========================================================================
FUNCTION  DS_EFS_READ_KEEP_ALIVE_APN_DATA

DESCRIPTION
 Read the EFS file to get the Keep alive APN data.
  
PARAMETERS
 efs_data:                Buffer to store the file contents
 
DEPENDENCIES
  None.

RETURN VALUE
  TRUE: If the read operation was successful
  FALSE: If the we could not read the efs file

SIDE EFFECTS
  None

===========================================================================*/
uint32 ds_efs_read_keep_alive_apn_data
(
   char                       **efs_data
)
{
   char                        item_file_path [] = DS_EPS_KAMGR_PDN;
   uint32                      size = 0;
   struct fs_stat              temp_buf;
   int                         get_result = -1;
/*------------------------------------------------------------------------*/
   memset(&temp_buf, 0, sizeof(struct fs_stat));

   if (efs_stat((const char*)item_file_path,&temp_buf) == -1)
   {
     DATA_MSG_SPRINTF_1(MSG_LEGACY_ERROR,"ds_efs_read_keep_alive_apn:"
                        "Error opening file %s",item_file_path);
   }
   else
   {
     *efs_data = (char*)ds_system_heap_mem_alloc(sizeof(char) * temp_buf.st_size);
     
     if(*efs_data == NULL)
     {
       DATA_MSG0_MED("ds_efs_read_keep_alive_apn_data: Memory allocation failed");
     }
     else
     {
       memset (*efs_data, 0, temp_buf.st_size);
       get_result = efs_get ((const char *)item_file_path, (void *)(*efs_data), 
                             temp_buf.st_size);
       if(get_result != -1 && get_result != 0)
       {
         size = temp_buf.st_size;
         DATA_MSG_SPRINTF_2(MSG_LEGACY_HIGH,"ds_efs_read_keep_alive_apn_data:"
                        "data read from EFS %s and size is %d",*efs_data,size);
       }
       else
       {
         DATA_MSG0_ERROR("EFS file for Keep Alive APN not read successfully");
         ds_system_heap_mem_free(*efs_data);
         *efs_data = NULL;
       }
     }
   }
   return size;
} /* ds_efs_read_keep_alive_apn */

/*===========================================================================
FUNCTION  DS_EFS_FREE_KAMGR_DATA_BUFFER

DESCRIPTION
 Free the EFS file data reserved for Keep alive manager.
  
PARAMETERS
 efs_data:               Pointer to the Memory to be released
 
DEPENDENCIES
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/

void ds_efs_free_kamgr_data_buffer
(
   char                             **efs_data
)
{
  if (*efs_data == NULL)
  {
    DATA_MSG0_ERROR("ds_efs_free_kamgr_data_buffer: Memory already released");
    return;
  }

  ds_system_heap_mem_free(*efs_data);
  *efs_data = NULL;

  DATA_MSG0_MED("ds_efs_free_kamgr_data_buffer: Memory released successfully");
}


