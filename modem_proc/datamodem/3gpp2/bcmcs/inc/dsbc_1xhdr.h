#ifndef DSBC_1X_HDR_H
#define DSBC_1X_HDR_H

/*===========================================================================

                        D S Bcast 1X HDR _ M G R
GENERAL DESCRIPTION
  This file is the controller for bcast data  calls. 

  This module  creates teh ps_iface correspondiign to broadcast channel and
  send the iface indications bsed on eth bcast service status 

  This module works in conjunscitonw ith Bcmgr

  This file mainly concerns itself with initializing DSBC_1XHDR and has the
  entry point for processing commands from CM & PS related to BCMCS. It is
  the external interface to the DS task.


EXTERNALIZED FUNCTIONS

  DSBC_1X_HDR_INIT
    Returns if the packet call is dormant.

  DSBC_1X_HDR_PROCESS_CMDS
    Processes commands from PS for bcast iface control and flow registration 
    called from dstask

  
INITIALIZATION AND SEQUENCING REQUIREMENTS
  dsbc_1xhdr_init() must be called at startup.

 Copyright (c) 2004 - 2011 by Qualcomm Technologies Incorporated.  All Rights Reserved.
 
===========================================================================*/

/*===========================================================================
                        EDIT HISTORY FOR MODULE

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/3gpp2/bcmcs/inc/dsbc_1xhdr.h#1 $

-----------------------------------------------------------------------------  
when        who    what, where, why
--------    ---    -------------------------------------------------------
03/30/11    ms     Q6 free floating global variables cleanup.
03/09/09    sa     AU level CMI modifications.
04/21/05    mct    Added some enums removed ps_bcmcs.h.
08/23/04    vr     Split header file into internal and external .h files
15/01/04    kvd    First version of file 
===========================================================================*/


/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/

#include "comdef.h"
#include "customer.h"

#include "dstask_v.h"

/*===========================================================================
                            TYPEDEFS
===========================================================================*/

/*---------------------------------------------------------------------------
  Ids for the different entities that can issue BCMgr ioctls
---------------------------------------------------------------------------*/
#define BCMCS_BCRM_APP_ID 1
#define BCMCS_BCSOCK_APP_ID 2


/*===========================================================================
                            VARIABLES
===========================================================================*/

/*===========================================================================
                       EXTERNAL FUNCTION DEFINITIONS
===========================================================================*/

/*===========================================================================
                        DSBC 1X HDR INITIALIZATION
===========================================================================*/

/*===========================================================================

FUNCTION      DSBC_1X_HDR_INIT

DESCRIPTION   Called once at mobile power-up. Creates bcast ps_iface and
              registers the appropriate functionality with BCMGR.

DEPENDENCIES  None.

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void dsbc_1xhdr_init(void);

/*===========================================================================

FUNCTION DSBC_1X_HDR_PROCESS_CMDS

DESCRIPTION
  This function processes BCMCS commands specific to 1x-hdr implementation.

  This function should be called by the top-level task dispatcher whenever
  any 1x-hdr specific BCMCS  command is received.

DEPENDENCIES
  BC_1xhdr modules should have been initialized prior to calling this function.

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/

void  dsbc_1xhdr_process_cmds
(
  ds_cmd_type  *cmd_ptr
);

/*===========================================================================
FUNCTION       DSBC_GET_PS_IFACE_PTR

DESCRIPTION   Given the bcast call instance, returns the ps iface_ptr.

DEPENDENCIES  None.

RETURN VALUE  See description.

SIDE EFFECTS  None.
===========================================================================*/
ps_iface_type *dsbc_get_ps_iface_ptr
( 
  void
);

#endif /* DSBC_1X_HDR_H */

