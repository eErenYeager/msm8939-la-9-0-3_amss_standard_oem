#ifndef DSBC_1XHDR_TX_H
#define DSBC_1XHDR_TX_H
/*===========================================================================

                      B C M C S    I N T E R F A C E

       D A T A   Q U E U E   M A N I P U L A T I O N   R O U T I N E S 
 
GENERAL DESCRIPTION
  The 1X-HDR BCMCS interface uses a separate queue to transmit data up the 
  stack or to the laptop. This module initializes and manages the queue
  and BCMCS data delivery to the IP layer/laptop.

EXTERNALIZED FUNCTIONS
  dsbc_1xhdr_tx_init( )
    Initialize the 1X-HDR BCMCS queue.

INITIALIZATION AND SEQUENCING REQUIREMENTS
  dsbc_1xhdr_tx_init must be called first.

Copyright (c) 2004-2009 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/3gpp2/bcmcs/inc/dsbc_1xhdr_tx.h#1 $                       

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/04/09   ss      CMI SU level modifications.
05/16/04   vr      Initial revision.

===========================================================================*/


/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "comdef.h"
#include "target.h"

#include "ps_meta_info.h"
#include "ps_iface.h"
#include "dsbc_1xhdr.h"
#include "dsbcdb_api.h"

/*===========================================================================

                     DATA TYPES AND DEFINITIONS

===========================================================================*/

/*===========================================================================

                        FUNCTION DEFINITIONS

===========================================================================*/
/*===========================================================================
FUNCTION DSBC_1XHDR_TX_INIT()

DESCRIPTION
  This function is called at powerup (PS initialization) to create
  and intialize the 1X-HDR BCMCS queue

DEPENDENCIES
  None

PARAMETERS
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
void dsbc_1xhdr_tx_init( void );

#endif /* DSBC_1XHDR_TX_H */
