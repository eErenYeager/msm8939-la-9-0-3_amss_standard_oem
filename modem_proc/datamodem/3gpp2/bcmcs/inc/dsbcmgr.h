#ifndef DSBCMGR_H
#define DSBCMGR_H

/*===========================================================================

                Broad Cast D A T A   S E R V I C E S   M A N A G E R

                           H E A D E R   F I L E

DESCRIPTION
  This file contains functions and definitions exported by Bcast Dsmgr to other
  Data Services software units, namely: the top-level task dispatcher and
  Mode-Specific Handlers.

  Copyright (c) 2004 - 2011 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/


/*===========================================================================

                      EDIT HISTORY FOR FILE

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/3gpp2/bcmcs/inc/dsbcmgr.h#1 $ 

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/30/11   ms      Q6 free floating global variables cleanup.
03/09/09   sa      AU level CMI modifications.
08/23/04   vr      Moved mode specific declarations to dsbcmshif.h
15/01/04   kvd     Initial version.

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

#include "comdef.h"
#include "dstask_v.h"
#include "dsbcmshif.h"

/*===========================================================================

                      PUBLIC DATA DECLARATIONS

===========================================================================*/

/*===========================================================================

                      PUBLIC FUNCTION DECLARATIONS

===========================================================================*/

/*===========================================================================

FUNCTION DSBCMGR_INIT

DESCRIPTION
  This function performs power-up initialization of BCMCS bcmgr. This includes
  initialization of some state variables and registration with Call Manager
  for handling of BCMCS specific events, commands.

  This function must be called once at data services task startup.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None 
===========================================================================*/

extern void  dsbc_init( void );

/*===========================================================================

FUNCTION DSBCMGR_PROCESS_CMDS

DESCRIPTION
  This function processes BCMCS commands. A command processing function is
  called based on the type of command received and the mode.

  This function should be called by the top-level task dispatcher whenever
  any BCMCS  bcmgr command is received.

DEPENDENCIES
  BCMGR should have been initialized prior to calling this function.

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/

extern void  dsbcmgr_process_cmds
(
  ds_cmd_type  *cmd_ptr
);

/*===========================================================================

FUNCTION DSBC_UPDATE_CLIENT_ID

DESCRIPTION
  This function sets the dscbi_cm_client_id global variable. 

DEPENDENCIES
  Updates the current dsbci_cm_client_id

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
void  dsbc_update_client_id
(
  cm_client_id_type  client_id
);

/*===========================================================================

FUNCTION DSBC_GET_CURRENT_CLIENT_ID

DESCRIPTION
  This function returns the dscbi_cm_client_id global variable. 

DEPENDENCIES
  Updates the current dsbci_cm_client_id

RETURN VALUE
  dsbci_cm_client_id

SIDE EFFECTS
  None

===========================================================================*/
cm_client_id_type dsbc_get_current_client_id
(
  void
);

/*===========================================================================

FUNCTION DSBC_GET_MODE_SP_HANDLER

DESCRIPTION
  This function returns a mode-specific handler function table for BCMCS
  commands, for the specified mode.

DEPENDENCIES
  None

RETURN VALUE
  Mode specific handler for the BCMCS commands of type  dsbc_hdlr_func_tbl_type

===========================================================================*/

dsbc_hdlr_func_tbl_type*  dsbc_get_mode_sp_handler
(
  sys_sys_mode_e_type           mode        /* Network mode             */
);
#endif /* DSBCMGR_H */
