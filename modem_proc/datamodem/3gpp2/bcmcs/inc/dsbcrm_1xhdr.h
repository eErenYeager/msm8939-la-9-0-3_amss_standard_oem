#ifndef DSBCRM_1XHDR_H
#define DSBCRM_1XHDR_H
/*===========================================================================

                          D S B C R M _ 1 X H D R . H

DESCRIPTION

  This file contains macros, definitions and function prototypes used by the 
  BCRm module. This is the external interface of the 1X-HDR BC-Rm module
  with other modules.

EXTERNALIZED FUNCTIONS
  dsbcrm_1xhdr_process_cmds()
    The command processor for BC-Rm commands, to be called from DS task
    context.

  dsbcrm_1xhdr_init()
    Performs DS task initialization needed for BC-Rm. Should only be called
    from dstask()

INITIALIZATION AND SEQUENCING REQUIREMENTS
    dsbcrm_1xhdr_init must be called first on startup, but after 707 RmSm
    init has been called.

Copyright (c) 2004 - 2011 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/


/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/3gpp2/bcmcs/inc/dsbcrm_1xhdr.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when        who    what, where, why
--------    ---    ----------------------------------------------------------
03/30/11     ms    Q6 free floating global variables cleanup.
03/09/09     sa    AU level CMI modifications.
04/06/04     vr    Initial revision.
===========================================================================*/


/*===========================================================================

                          INCLUDE FILES FOR MODULE

===========================================================================*/

#include "comdef.h"
#include "customer.h"

#include "dstask_v.h"

/*===========================================================================

                          EXTERNAL FUNCTION DEFINITIONS

==========================================================================*/
/*===========================================================================
  FUNCTION DSBCRM_1XHDR_PROCESS_CMDS()

  DESCRIPTION
    The command processor for BC-Rm commands. This will be called by
    ds_process_cmds from dstask
                                                                                        
  PARAMETERS
    the command for BC-Rm

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void dsbcrm_1xhdr_process_cmds( ds_cmd_type *cmd_ptr );

/*===========================================================================
  FUNCTION DSBCRM_1XHDR_INIT()

  DESCRIPTION
    Performs task initialization needed for BC-Rm
                                                                                        
  PARAMETERS
    None

  RETURN VALUE
    None

  DEPENDENCIES
    For phase 1, this must only be called AFTER 707 RmSm init has been 
    called and the 707 Rm ps_iface has been created.

  SIDE EFFECTS
    None
===========================================================================*/
void dsbcrm_1xhdr_init(void );

/*===========================================================================
  FUNCTION DSBCRM_1XHDR_SET_MCAST_LIST_IP_TYPE()

  DESCRIPTION
    This functions sets the IP addr of dsbcrm_info. to specified type
 
  PARAMETERS
   uint8 Index
   Ip address enum type

  RETURN VALUE
    None

  DEPENDENCIES
    Must only be called from DS task context

  SIDE EFFECTS
    None
===========================================================================*/
void dsbcrm_1xhdr_set_mcast_list_ip_type
( 
  uint8             index,
  ip_addr_enum_type type
);

/*===========================================================================
  FUNCTION DSBCRM_1XHDR_GET_MCAST_LIST_IP_TYPE()

  DESCRIPTION
    This functions Gets the IP addr type of dsbcrm_info of specified index i
 
  PARAMETERS
   Index

  RETURN VALUE
    Ip address enum type

  DEPENDENCIES
    Must only be called from DS task context

  SIDE EFFECTS
    None
===========================================================================*/
ip_addr_enum_type dsbcrm_1xhdr_get_mcast_list_ip_type
(
  uint8 index
);

/*===========================================================================
  FUNCTION DSBCRM_1XHDR_SET_MCAST_LIST_IP_INFO()

  DESCRIPTION
    This functions Sets the IP addr information of "dsbcrm_info"
    of specified index i to the value specified in arguments.
 
  PARAMETERS
   Index - Index to mcast_ip_list
   Ip addr  - Actual IP address to fill

  RETURN VALUE
   None

  DEPENDENCIES
    Must only be called from DS task context

  SIDE EFFECTS
    None
===========================================================================*/
void   dsbcrm_1xhdr_set_mcast_list_ip_info
(
  uint8        index,
  ip_addr_type ip_addr
);

/*===========================================================================
  FUNCTION DSBCRM_1XHDR_GET_MCAST_LIST_IP_INFO()

  DESCRIPTION
    This functions Gets the IP addr information of "dsbcrm_info" of specified index i
 
  PARAMETERS
   Index - Index to mcast_ip_list

  RETURN VALUE
    Ip address info for the given index

  DEPENDENCIES
    Must only be called from DS task context

  SIDE EFFECTS
    None
===========================================================================*/
void  dsbcrm_1xhdr_get_mcast_list_ip_info
(
  uint8        index,
  ip_addr_type *ip_addr
);
#endif /* DSBCRM_1XHDR_H */
