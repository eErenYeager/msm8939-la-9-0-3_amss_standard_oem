#ifndef DSBCMCS_DEFS_V_H
#define DSBCMCS_DEFS_V_H
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                           D S B C M C S _ D E F S . H

GENERAL DESCRIPTION
  This file contains the externalized structures and definitions needed to 
  access the BCMCS database module and should be included by all modules
  that need to access BCDBAPP. Structures and definitions used internally
  by BCDBAPP should not be in this file.

EXTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None.

 Copyright (c) 2004 - 2009 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/
/*===========================================================================
                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/3gpp2/bcmcs/inc/dsbcmcs_defs_v.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

-----------------------------------------------------------------------------
when        who    what, where, why
--------    ---    -------------------------------------------------------
09/04/09    ss     CMI SU level modifications.
08/10/09    ms     Changes related to CM AU level CMI changes.
03/09/09    sa     AU level CMI modifications.
04/04/06    kvd    Added new enum types DSBCMCS_FRAMING_NOT_SET &
                   DSBCMCS_PROTOCOL_NOT_SET used by db_update.
03/11/04    vr     Initial revision.
===========================================================================*/

/*===========================================================================
                      INCLUDE FILES
===========================================================================*/

#include "datamodem_variation.h"
#include "dsbcdb_api.h"

/*===========================================================================
                        TYPEDEFS AND VARIABLES
===========================================================================*/
#ifdef FEATURE_HDR_BCMCS_2p0
#error code not present
#elif defined (FEATURE_BCMCS)
#define DSBC_MAX_NUM_FLOWS SYS_BCMCS_10_MAX_FLOWS

#else
#define DSBC_MAX_NUM_FLOWS SYS_BCMCS_MIN_FLOWS
#endif /* FEATURE_HDR_BCMCS_2p0 */

#endif /* DSBCMCS_DEFS_V_H */
