
#ifndef DSBC_RT_ACL_H
#define DSBC_RT_ACL_H

/*===========================================================================

   B C M C S 	S P E C I F I C   M O D E   H A N D L E R

        A C C E S S   C O N T R O L   L I S T   D E F I N I T I O N S
 
       F O R   R O U T I N G   V I A   T H E   BCMCS   I N T E R F  A C E

GENERAL DESCRIPTION
  The BCMCS specific routing ACL is used to enable policy and address
  based routing across the bcmcs interface.

EXTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2004 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
Copyright (c) 2005 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/3gpp2/bcmcs/inc/dsbc_rt_acl.h#1 $ 

when       who     what, where, why
--------   ---     ----------------------------------------------------------
01/15/04   kvd     Created Module.

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "comdef.h"
#include "customer.h"

#include "ps_acl.h"

/*===========================================================================

                 ACCESS CONTROL LIST NAME DEFINITIONS

===========================================================================*/

ACL_DEF( dsbc_rt_acl );

#endif /* DSBC_RT_ACL_H */

