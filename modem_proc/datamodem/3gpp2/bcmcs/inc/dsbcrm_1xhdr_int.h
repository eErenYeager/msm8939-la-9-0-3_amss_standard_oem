#ifndef DSBCRM_1XHDR_INT_H
#define DSBCRM_1XHDR_INT_H
/*===========================================================================

                          D S B C R M _ 1 X H D R _ I N T . H

DESCRIPTION

  This file contains macros and definitions to be used internally by the 
  BCRm module and AT command processor for BCMCS related AT commands.

EXTERNALIZED FUNCTIONS

Copyright (c) 2004 - 2011 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/


/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/3gpp2/bcmcs/inc/dsbcrm_1xhdr_int.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when        who    what, where, why
--------    ---    ----------------------------------------------------------
03/30/11     ms    Q6 free floating global variables cleanup.
04/06/04     vr    Initial revision.
===========================================================================*/


/*===========================================================================

                          INCLUDE FILES FOR MODULE

===========================================================================*/


#include "comdef.h"
#include "customer.h"

#include "ps_iface.h"
#include "ps_ip_addr.h"

/*===========================================================================

                         REGIONAL DATA DECLARATIONS

===========================================================================*/
   
/*---------------------------------------------------------------------------
  Max number of BCMCS multicast IPs supported for laptop calls
---------------------------------------------------------------------------*/              
#define DSBCRM_1XHDR_MAX_IPS 3

/*---------------------------------------------------------------------------
  State information maintained by 1X-HDR specific BC-Rm
---------------------------------------------------------------------------*/              
typedef struct
{
  ps_iface_type    *rm_iface_ptr; /* pointer to 707 Rm iface               */
  ps_iface_type    *bc_iface_ptr; /* pointer to CDMA BCAST iface           */
  void             *rm_iface_up_buf_ptr; /* callback buffer registered with
                                        707 Rm PS_iface for IFACE_UP event */
  void             *rm_iface_routeable_buf_ptr; /* callback buffer 
                 registered with 707 Rm PS_iface for IFACE_ROUTEABLE event */
  void             *rm_iface_down_buf_ptr; /* callback buffer registered with
                                        707 Rm PS_iface for IFACE_UP event */
  ip_addr_type     mcast_ip_list[DSBCRM_1XHDR_MAX_IPS]; /* List of multicast IP 
              addresses that the laptop is interested in for BCMCS service */
  ps_iface_mcast_handle_type mcast_ip_handle_list[DSBCRM_1XHDR_MAX_IPS];
                             /* List of multicast IP  handles */
  boolean          is_inited;      /* Was powerup init okay                */
} dsbcrm_1xhdr_info_type;


#endif /* DSBCRM_1XHDR_INT_H */
