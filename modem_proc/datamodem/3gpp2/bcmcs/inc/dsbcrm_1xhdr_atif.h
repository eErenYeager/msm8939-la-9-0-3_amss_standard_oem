#ifndef DSBCRM_1XHDR_ATIF_H
#define DSBCRM_1XHDR_ATIF_H
/*===========================================================================

                    D S B C R M _ 1 X H D R _ A T I F . H

DESCRIPTION

  Interface between BCRm and ATCOP. This file contains functions to set 
  the multicast IP addresses that the laptop is interested in and to 
  enable/disable BCMCS service to the laptop. 
  
  This file is intended to be internal to the BCRm module and not meant
  to be used by other modules. 

EXTERNALIZED FUNCTIONS
  dsbcrm_1xhdr_atif_get_bcips_as_str()
    Gets the multicast IP addresses that the laptop is currently
    interested in listening to.
  dsbcrm_1xhdr_atif_set_bcips_from_str()
    Sets the multicast IP addresses that the laptop is interested
    in for BCMCS service. Also writes them to EFS.
  dsbcrm_1xhdr_atif_read_bcips_from_efs()
    Reads from EFS the multicast IP addresses that the laptop is
    interested in for BCMCS service.
  dsbcrm_1xhdr_atif_write_bcips_from_efs()
    Writes to EFS the multicast IP addresses that the laptop is
    interested in for BCMCS service.
  dsbcrm_1xhdr_atif_is_bc_to_laptop()
    Returns whether the laptop has indicated that it is interested in
    BCMCS service or not.

Copyright (c) 2004 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
Copyright (c) 2005 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/3gpp2/bcmcs/inc/dsbcrm_1xhdr_atif.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when        who    what, where, why
--------    ---    ----------------------------------------------------------
04/08/04     vr    Initial revision.
===========================================================================*/

/*===========================================================================

                          INCLUDE FILES FOR MODULE

===========================================================================*/

#include "comdef.h"
#include "customer.h"

#include "dsati.h"
#include "dsat707util.h"
#include "dsat_1xhdr_bc.h"
#include "dsat_1xhdr_bcctab.h"
#include "dsbcrm_1xhdr_int.h"


/*= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

                        PUBLIC FUNCTION DECLARATIONS

= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =*/

/*===========================================================================
  FUNCTION DSBCRM_1XHDR_ATIF_GET_BCIPS_AS_STR()

  DESCRIPTION
    This function gets the list of multicast IP addresses that the laptop
    is interested in listening to for BCMCS service, in a string format.
                                                                                        
  PARAMETERS
    Pointer to the string the IP addresses are to be returned in.

  RETURN VALUE
    Pointer to the end of the string after the IP addresses have been
    filled into it.

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
byte *dsbcrm_1xhdr_atif_get_bcips_as_str(byte *rb_ptr);

/*===========================================================================
  FUNCTION DSBCRM_1XHDR_ATIF_SET_BCIPS_FROM_STR()

  DESCRIPTION
    This function takes a string containing a list of IP addresses, 
    parses it, and, if the parse is successful, sets the list of multicast
    IP addresses that the laptop is interested in for BCMCS service.

    This function also writes the list to EFS.
                                                                                        
  PARAMETERS
    Pointer to the string containing the IP addresses.

  RETURN VALUE
    If parse is successful and the BC IP addresses are set, returns DSAT_OK.

    If parse is unsuccessful or if any of the IP addresses are incorrect or
    not in the muticast range, returns DSAT_ERROR and sets ALL the BC IPs
    to invalid.

  DEPENDENCIES
    All 3 IP addresses must be specified in the string and they must all
    be valid IP addresses or 0.0.0.0 If any of them is invalid, ALL the BC
    IPs will be set to invalid.

  SIDE EFFECTS
    Writes the BC IPs to EFS.
===========================================================================*/
dsat_result_enum_type dsbcrm_1xhdr_atif_set_bcips_from_str(char *str);

/*===========================================================================
  FUNCTION DSBCRM_1XHDR_ATIF_WRITE_BCIPS_TO_EFS()

  DESCRIPTION
    This function writes to EFS the IP addresses that the laptop is 
    interested in for BCMCS service.
                                                                                        
  PARAMETERS
    None

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void dsbcrm_1xhdr_atif_write_bcips_to_efs( void );

/*===========================================================================
  FUNCTION DSBCRM_1XHDR_ATIF_READ_BCIPS_FROM_EFS()

  DESCRIPTION
    This function reads from EFS the IP addresses that the laptop is 
    interested in for BCMCS service.
                                                                                        
  PARAMETERS
    None

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void dsbcrm_1xhdr_atif_read_bcips_from_efs( void );

/*===========================================================================
  FUNCTION DSBCRM_1XHDR_ATIF_SEND_BC_TO_LAPTOP()

  DESCRIPTION
    This function returns whether the laptop has requested for BCMCS service
    by issuing the AT$QCBCEN command
                                                                                        
  PARAMETERS
    None

  RETURN VALUE
    Returns TRUE is laptop has requested for BC service, FALSE otherwise.

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean dsbcrm_1xhdr_atif_is_bc_to_laptop( void );

#endif /* DSBCRM_1XHDR_ATIF_H */
