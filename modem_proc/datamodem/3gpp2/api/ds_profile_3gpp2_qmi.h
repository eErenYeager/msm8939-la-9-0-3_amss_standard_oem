/******************************************************************************
  @file    ds_profile_3gpp2_qmi.h
  @brief   

  DESCRIPTION
  Tech specific 3GPP2 Profile Management through QMI, header file

  INITIALIZATION AND SEQUENCING REQUIREMENTS
  N/A

  ---------------------------------------------------------------------------
  Copyright (C) 2012 Qualcomm Technologies Incorporated.
  All Rights Reserved. 
  QUALCOMM Proprietary and Confidential.
  ---------------------------------------------------------------------------
******************************************************************************/
/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/3gpp2/api/ds_profile_3gpp2_qmi.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
08/04/11   gs      First version of the header file.
===========================================================================*/
#ifndef DS_PROFILE_3GPP2_QMI_H
#define DS_PROFILE_3GPP2_QMI_H

#include "comdef.h"

#ifdef FEATURE_DS_PROFILE_ACCESS_QMI

#include "ds_profile_tech_common.h"

/*===========================================================================
FUNCTION DS_PROFILE_3GPP2_QMI_INIT

DESCRIPTION
  This function is called on the library init. It initializes the function
  pointers to valid functions for 3gpp2

PARAMETERS
  fntbl : pointer to table of function pointers

DEPENDENCIES

RETURN VALUE
  returns the mask for 3gpp2. (Used later as valid mask which is ORed value
  returned from all techs)
SIDE EFFECTS

===========================================================================*/
uint8 ds_profile_3gpp2_qmi_init(tech_fntbl_type *fntbl);

#endif /* FEATURE_DS_PROFILE_ACCESS_QMI */

#endif /* DS_PROFILE_3GPP2_QMI_H */
