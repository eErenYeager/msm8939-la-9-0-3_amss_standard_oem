/*===========================================================================
                        DSD APM EFS PARSER

GENERAL DESCRIPTION
  All the declarations and definitions necessary to support parsing
  of DSD APM related EFS file.

EXTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS

  Copyright (c) 2012-2014 by Qualcomm Technologies Incorporated.  All Rights Reserved.

===========================================================================*/

/*===========================================================================
                           EDIT HISTORY FOR MODULE

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/3gpp2/dsd/src/ds_dsd_apm_efs_parser.c#3 $
  $DateTime: 2016/03/17 07:26:49 $
  $Author: ssaha $


when        who    what, where, why
--------    ---    ----------------------------------------------------------
09/10/13    rs     Added support for NULL APN string
08/22/12    ss     Removing efs write call as that is added to dsutil now.
05/12/12    fjia   Create APM EFS parser module
===========================================================================*/

/*===========================================================================
                          INCLUDE FILES FOR MODULE
===========================================================================*/
#include "msg.h"
#include "data_msg.h"
#include "comdef.h"
#include "dsutil.h"
#include "ds_dsd_apm_efs_parser.h"
#include "modem_mem.h"
#include "lte.h"
#include "fs_public.h"
#include "fs_sys_types.h"
#include "fs_errno.h"
#include "dsumts_subs_mgr.h"
#include "ds3gsubsmgr.h"
#include <stringl/stringl.h>
#ifdef FEATURE_DATA_LTE
#include "ds_3gpp_pdn_context.h"
#endif /* FEATURE_DATA_LTE */
/*===========================================================================
                              GLOBAL VARIABLES
===========================================================================*/
#ifdef FEATURE_DATA_LTE
/*---------------------------------------------------------------------------
  An array of strings to map the Parameter Name string (mentioned in EFS
  file) to a token ID. This token ID will be used to modify
  the correct entry in the internal APM rule structure.
---------------------------------------------------------------------------*/
static char dsd_apm_rule_param_name_id[DSD_APM_RULE_MAX_PARAMS_NUM]
                                     [DSD_APM_RULE_MAX_PARAM_NAME_SIZE] = 
                        { "APN_String",
                          "ATTACH_TYPE_RULES",
                          "ON_DEMAND_TYPE_RULES",
                          "P-CSCFRequired",
                          "IPAddressRequired"};

static uint8 attach_prof_status;

/*===========================================================================
                      INTERNAL FUNCTION DEFINITIONS
===========================================================================*/

/*===========================================================================
FUNCTION      DSD_ATOI

DESCRIPTION    Converts the character input to integer type

PARAMETERS    x - Begin pointer (char *)
              y - End pointer (char *)

DEPENDENCIES  None

RETURN VALUE  ouput integer

SIDE EFFECTS  None
===========================================================================*/
int dsd_apm_atoi
(
  char* x, char* y
)
{
  int z = 0;
  boolean is_valid = TRUE;
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  while( x < y )
  {
    if((*x) >= '0' && (*x) <= '9')
    {
      z = z *10 + (*x- '0');
      x++;
    }
    else
    {
      is_valid = FALSE;
      break;
    }
  }

  if(!is_valid)
  {
    return -1;
  }
  return z;
} /* DSD_APM_ATOI() */

/*===========================================================================
FUNCTION      dsd_apm_pdn_rule_get_token_id

DESCRIPTION   This function returns the token id associated with each
              apm rule parameter.

PARAMETERS    char *from - start of the token (param name)
              char *to   - end of the token (param name)

DEPENDENCIES  None

RETURN VALUE  0 - success
              1 - failure

SIDE EFFECTS  None
===========================================================================*/
int16 dsd_apm_pdn_rule_get_token_id
(
  char    *from,
  char    *to,
  uint8   *token_id
)
{
  int16 ret_val = -1; /* return value */
  uint8 i;            /* counter for number of params */
  uint8 length;       /* length of the token (param name) */
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  /* Get the length of the string that is passed */
  if  ( from > to )
  {
    DATA_MSG0(MSG_LEGACY_ERROR,"Invalid Parameters");
    return ret_val;
  }

  /* Assumption that the token length will not be greater than 255 */
  length =(uint8)(to-from);

  for (i=0; i<DSD_APM_RULE_MAX_PARAMS_NUM; i++)
  {
    if (length == strlen(dsd_apm_rule_param_name_id[i]))
    {
      if (0 == strncasecmp(from,dsd_apm_rule_param_name_id[i],length))
      {
        *token_id = i;
        ret_val = 0;
        break;
     }
   }
  }
  return ret_val;

}/* dsd_apm_pdn_rule_get_token_id() */

/*===========================================================================
FUNCTION      DSD_APM_GET_APN_STRING_WITH_TOKEN_CONTENT

DESCRIPTION   This function gets apn string from EFS.

PARAMETERS    char *from, *to - start and end of the character array which
                          holds the data to populate the profile structure
              dsd_apm_pdn_name_type  *apn_name - structure to store apn string
 

DEPENDENCIES  None

RETURN VALUE  TRUE - get valid APN string 
              FALSE - APN string is not valid 

SIDE EFFECTS  None
===========================================================================*/
boolean dsd_apm_get_apn_string_with_token_content
(
  char   *from,
  char   *to,
  dsd_apm_apn_name_type   *apn_name
)
{
  uint8   length;
  boolean ret_val = TRUE;
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  /* Assumption that the token length will not be greater than 255 */
  /*Note: 'to' points to (end of token+1)*/
  length = (uint8)((to-from)/sizeof(char));

  if ( (from > to)  || (length > DSD_APM_MAX_APN_STRING_LEN))
  {
    apn_name->apn_string_len = 0;
    memset(apn_name->apn_string,'\0', DSD_APM_MAX_APN_STRING_LEN);
    DATA_MSG0(MSG_LEGACY_ERROR,"Error in APN String. Setting APN length to 0");
    ret_val = FALSE;
  }
  else
  {
    (void)memscpy(apn_name->apn_string,
                  DSD_APM_MAX_APN_STRING_LEN, from, length);
     apn_name->apn_string_len = length;
  }

  return ret_val;
} /* dsd_apm_get_apn_string_with_token_content() */

/*===========================================================================
FUNCTION      DSD_APM_VALIDATE_RULE_TYPE_WITH_TOKEN_CONTENT

DESCRIPTION   This function check if attach_rule_type/on_demand_rule_type 
              is valid or not 

PARAMETERS    char *from, *to - start and end of the character array which
                          holds the data to populate the profile structure
              boolen  *valid_rule - structure to store valid_rule_type

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void dsd_apm_validate_rule_type_with_token_content
(
  char       *from,
  char       *to,
  boolean    *valid_rule
)
{
  uint8   length;
  char    val_true[] = "TRUE";
  char    *start = from;
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  while ( *start == ' ' && start < to )
  {
    start++;
  }

  length =(uint8)(to-start);
  if ( (length == strlen(val_true)) && 
                        ( strncasecmp(start, val_true, length) == 0 ) )
  {
    *valid_rule = TRUE;
  }
  else
  {
    *valid_rule = FALSE;
  }
  return;
}/* dsd_apm_validate_rule_type_with_token_content() */

/*===========================================================================
FUNCTION      DSD_APM_FILL_ATTACH_RULE_WITH_TOKEN_CONTENT

DESCRIPTION   This function gets the data for each token and populates apn_info
              structure with the appropriate data that corresponds to the
              token number.

PARAMETERS    token_id -  describes the current token which is
                              being populated into the EFS structure

              char *from, *to - start and end of the character array which
                          holds the data to populate the profile structure
 
              pdn_id - PDN number to fill correspond content

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void dsd_apm_fill_attach_rule_with_token_content
(
  uint8  token_id,
  char   *from,
  char   *to,
  apn_info_type *apn_info
)
{
  ipRequiredType  value = 0;
  char    *start = from;
  boolean  token_valid = TRUE;
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  if ( apn_info == NULL )
  {
    DATA_MSG0(MSG_LEGACY_ERROR,"Invalid apn_info ptr passed");
    return;
  }

  while ( *start == ' ' && start < to )
  {
    start++;
  }

  switch(*start)
  {
    case '1':
      value = ipv4_mandatory_and_ipv6_donotcare;
      break;
    case '2':
      value = ipv6_mandatory_and_ipv4_donotcare;
      break;
    case '3':
      value = ipv4_mandatory_and_ipv6_mandatory;
      break;
    case '4':
      value = ipv4_dontcare_and_ipv6_donotcare;
      break;
    case '5':
      value = ipv4_mandatory_or_ipv6_mandatory;
      break;
    default:
      token_valid = FALSE;
      break;
  }

  if ( token_valid == TRUE )
  {
    switch (token_id)
    {
      case PCSCF_REQUIRED:
        apn_info->AttachPdn.config_rule.config_valid_flag |= CONFIG_PCSCF;
        apn_info->AttachPdn.config_rule.pcscf_address = value;
        DATA_MSG1(MSG_LEGACY_MED,"APM attach rule: P-CSCF required %d", 
                  value);
        break;

      case IPADDRESS_REQUIRED:
        apn_info->AttachPdn.config_rule.config_valid_flag |= CONFIG_IP_ADDRESS;
        apn_info->AttachPdn.config_rule.ip_address = value;
        DATA_MSG1(MSG_LEGACY_MED,"APM attach rule: IPAddressRequired %d", 
                  value);
        break;

      default:
        DATA_MSG0(MSG_LEGACY_ERROR,"Invalid rule param passed");
        break;
    }
  }
  return;
} /* dsd_apm_fill_attach_rule_with_token_content() */ 

/*===========================================================================
FUNCTION      DSD_APM_FILL_ON_DEMAND_RULE_WITH_TOKEN_CONTENT

DESCRIPTION   This function gets the data for each token and populates the
              apn_info structure with the appropriate data that corresponds
              to the token number.

PARAMETERS    token_id -  describes the current token which is
                              being populated into the EFS structure

              char *from, *to - start and end of the character array which
                          holds the data to populate the profile structure.
 
              pdn_id - PDN number to fill correspond content

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void dsd_apm_fill_on_demand_rule_with_token_content
(
  uint8  token_id,
  char   *from,
  char   *to,
  apn_info_type  *apn_info
)
{
  ipRequiredType  value = 0;
  boolean         token_valid = TRUE;
  char            *start = from;
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  if ( apn_info == NULL  )
  {
    DATA_MSG0(MSG_LEGACY_ERROR,"Invalid apn info ptr passed");
    return;
  }

  while ( *start == ' ' && start < to )
  {
    start++;
  }

  switch(*start)
  {
    case '1':
      value = ipv4_mandatory_and_ipv6_donotcare;
      break;
    case '2':
      value = ipv6_mandatory_and_ipv4_donotcare;
      break;
    case '3':
      value = ipv4_mandatory_and_ipv6_mandatory;
      break;
    case '4':
      value = ipv4_dontcare_and_ipv6_donotcare;
      break;
    case '5':
      value = ipv4_mandatory_or_ipv6_mandatory;
      break;
    default:
      token_valid = FALSE;
      break;
  }

  if ( token_valid == TRUE )
  {
    switch (token_id)
    {
      case PCSCF_REQUIRED:
        apn_info->OnDemandPdn.config_rule.config_valid_flag |= CONFIG_PCSCF;
        apn_info->OnDemandPdn.config_rule.pcscf_address = value;
        DATA_MSG1(MSG_LEGACY_MED,"APM on-demand rule: P-CSCF required %d", 
                  value);
        break;

      case IPADDRESS_REQUIRED:
        apn_info->OnDemandPdn.config_rule.config_valid_flag 
          |= CONFIG_IP_ADDRESS;
        apn_info->OnDemandPdn.config_rule.ip_address = value;
        DATA_MSG1(MSG_LEGACY_MED,"APM on-demand rule: IPAddressRequired %d", 
                  value);
        break;

      default:
        DATA_MSG0(MSG_LEGACY_ERROR,"Invalid rule param passed");
        break;
    }
  }
  return;
} /* dsd_apm_fill_on_demand_rule_with_token_content() */ 

/*===========================================================================
FUNCTION      DSD_APM_FILL_ATTACH_RULE_VALIDITY

DESCRIPTION   This function populates the apn_info structure on_demand 
              validity field with passed value

PARAMETERS    None

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void dsd_apm_fill_attach_rule_validity
(
  apn_info_type  *apn_info,
  boolean         is_valid
)
{
  apn_info->AttachPdn.attach_rule_valid = is_valid;
  return;
}/* dsd_apm_fill_attach_rule_validity() */

/*===========================================================================
FUNCTION      DSD_APM_FILL_ON_DEMAND_RULE_VALIDITY

DESCRIPTION   This function populates the apn_info structure on_demand 
              validity field with passed value

PARAMETERS    None

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void dsd_apm_fill_on_demand_rule_validity
(
  apn_info_type  *apn_info,
  boolean         is_valid
)
{
  apn_info->OnDemandPdn.on_demand_rule_valid = is_valid;
  return;
}/* dsd_apm_fill_on_demand_rule_validity() */


/*===========================================================================
FUNCTION      DSD_APM_EXTRACT_APN_NAME_FROM_PROFILE

DESCRIPTION   This function extracts the APN name from Profile if new attach 
              profile lists have been cached 

PARAMETERS    subs_id:                         Current active PS subscription 
              attach_prof_q:                   attach profile queue
              rule_index:                      Index to match corresponding
                                               prof in attach prof queue
              apn_name:                        Structure to hold APN name

DEPENDENCIES  None

RETURN VALUE  TRUE: IF read from profile was successful 
              FALSE: Otherwise 

SIDE EFFECTS  None
===========================================================================*/
boolean dsd_apm_extract_apn_name_from_profile
(
  sys_modem_as_id_e_type              subs_id,
  q_type                             *attach_prof_q,
  uint16                              rule_index,
  dsd_apm_apn_name_type              *apn_name
)
{
  attach_profile_data_block          *profile_block = NULL;
  ds_profile_info_type                prof_apn_name = {0, 0};
  boolean                             retval = FALSE;
  uint16                              prof_index = 0;
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  if(apn_name == NULL || attach_prof_q == NULL)
  {
    DATA_MSG0(MSG_LEGACY_ERROR, "Invalid Input Parameters");
    return retval;
  }

  profile_block = (attach_profile_data_block *)q_check(attach_prof_q);

  while(profile_block != NULL)
  {
    if(profile_block->attach_profile.subs_id == subs_id)
    {
      prof_index++;
      if(prof_index == rule_index)
      {
        /*---------------------------------------------------------------------
          Get the apn_name from profile database
        ---------------------------------------------------------------------*/
        memset(apn_name, '\0', sizeof(dsd_apm_apn_name_type));
        prof_apn_name.buf = apn_name->apn_string;
        prof_apn_name.len = DS_DSD_APM_MAX_APN_STRING_LEN;

        if ( dsd_apm_get_profile_param_info(
               profile_block->attach_profile.profile,
               DS_PROFILE_3GPP_PROFILE_PARAM_PDP_CONTEXT_APN,
               &prof_apn_name,
               subs_id) == TRUE )
        {
          /*---------------------------------------------------------------------
            update the apn_name in the rule block
          ---------------------------------------------------------------------*/
          retval = TRUE;
          apn_name->apn_string_len = (uint8)prof_apn_name.len;
        }
        else
        {
          DATA_MSG1(MSG_LEGACY_ERROR,"Cannot read APN name from profile %d",
                    profile_block->attach_profile.profile);
          retval = FALSE;
        }
        break;
      }
    }
    profile_block = (attach_profile_data_block *)
                                q_next(attach_prof_q, &(profile_block->link));
  }

  return retval;
}

/*===========================================================================
FUNCTION      DSD_APM_PARSE_PDN_RULE_LIST_FROM_EFS

DESCRIPTION   This function will read from the efs file all the necessary
              data that will populate the EFS structure.

              EFS File Format

              Param_Name:Param_Val;

              For example -
              APN_String:qcomIMS.com;

PARAMETERS    efs_db - structure that contains info about the EFS file

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
boolean dsd_apm_parse_pdn_rules_from_efs
(
  sys_modem_as_id_e_type     subs_id,
  ds_efs_token_type         *efs_db,
  q_type                    *attach_prof_q,
  q_type                    *rule_q
)
{
  char  *from, *to; /* ptrs to start and end of a token */
  ds_efs_token_parse_status_enum_type  ret_val
                                              = DS_EFS_TOKEN_PARSE_SUCCESS;
  boolean on_param_name = TRUE; /* flag to keep track of the parser state
                                TRUE - parser expects Token Name in the EFS
                                FALSE - parser expects Token Val in the EFS*/
  uint8 token_id = 0;
  boolean attach_pdn_rule = TRUE;
  boolean valid_pdn_rule = TRUE;
  boolean valid_pdn_string = FALSE;
  boolean result = FALSE;
  ds_dsd_apm_data_block     *apn_info_block = NULL;
  dsd_apm_apn_name_type     curr_pdn_name;
  uint16                    rule_index = 0;
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  curr_pdn_name.apn_string_len = 0;
  
  /* Set the seperator as : */
  efs_db->seperator = ':';

  while (DS_EFS_TOKEN_PARSE_EOF
          != (ret_val = ds_efs_tokenizer(efs_db, &from, &to )))
  {
     
    /*------------------------------------------------------------------------
      Token being read. from points to the beginning of the token and
      to points to the (end of the token+1).

      e.g: string= "token;" => from points to 't' and to points to ';'

      The tokenizer automatically skips blank lines and comments (lines
      beginning with #, so no need to check for them here).
     
      The additonal check of token_id ensures that NULL APN is accepted, 
      Token_Id: APN_STRING can accept Null parameter values, other tokens 
      cannot
    ------------------------------------------------------------------------*/
    if ((DS_EFS_TOKEN_PARSE_EOL == ret_val) ||
        (from == to && ((token_id == APN_STRING && on_param_name == TRUE) ||
                       (token_id != APN_STRING))) 
        )
    {
      /*----------------------------------------------------------------------
        Skip empty tokens.
      ----------------------------------------------------------------------*/
      if (on_param_name == FALSE)
      {
        on_param_name = TRUE;
        efs_db->seperator = ':'; 
      }
      continue;
    }

    else if ( DS_EFS_TOKEN_PARSE_SUCCESS == ret_val )
    {
      /*---------------------------------------------------------------------
      check if we are looking for param name or param value
      ---------------------------------------------------------------------*/
      if ( on_param_name == TRUE )
      {
        if ( dsd_apm_pdn_rule_get_token_id(from, to, &token_id) < 0 )
        {
          DATA_MSG0(MSG_LEGACY_ERROR,"Incorrect param name of apm PDN rule");
        }
        else
        {
          DATA_MSG1(MSG_LEGACY_MED,"Token ID: %d", token_id);
          /*-----------------------------------------------------------------
          set param_name as FALSE This means the next token is a
          param value
          -----------------------------------------------------------------*/
          on_param_name = FALSE;
          /* set the seperator as ; */
          efs_db->seperator = ';';
         }      
      }

      /*---------------------------------------------------------------------
      This means that the token is a param value
      ---------------------------------------------------------------------*/
      else
      {
        switch( token_id )
        {
          case APN_STRING:
          {
            if ( apn_info_block != NULL )
            {
              q_link(apn_info_block, &(apn_info_block->link));
              q_put(rule_q, &(apn_info_block->link));
              apn_info_block = NULL;
            }

            valid_pdn_string = FALSE;
            valid_pdn_rule = FALSE;
            rule_index++;

            memset(curr_pdn_name.apn_string,'\0', DS_DSD_APM_MAX_APN_STRING_LEN);
            if(attach_prof_status & ATTACH_PROF_CHANGED)
            {
              valid_pdn_string = dsd_apm_extract_apn_name_from_profile(subs_id,
                                 attach_prof_q, rule_index, &curr_pdn_name);
            }
            else
            {
              valid_pdn_string = dsd_apm_get_apn_string_with_token_content 
                                 (from, to, &curr_pdn_name);
            }
            if ( valid_pdn_string == TRUE )
            {
              apn_info_block = (ds_dsd_apm_data_block *)modem_mem_alloc(
                    sizeof(ds_dsd_apm_data_block), MODEM_MEM_CLIENT_DATA);
              if ( apn_info_block == NULL )
              {
                DATA_MSG0(MSG_LEGACY_HIGH,
                          "memory alloc for APM queue block failed");
                result = FALSE;
                break;
              }
              else
              {
                memset(apn_info_block, 0, sizeof(ds_dsd_apm_data_block));
                apn_info_block->apn_info.subs_id = subs_id;
                apn_info_block->apn_info.apn_name_len = 
                  curr_pdn_name.apn_string_len;
                strlcpy(apn_info_block->apn_info.apn_name,
                        curr_pdn_name.apn_string,
                        apn_info_block->apn_info.apn_name_len + 1);

                result = TRUE;
              }
            }
          }
          break;

          case ATTACH_TYPE_RULES:
          {
            if ( valid_pdn_string == TRUE )
            {
              attach_pdn_rule = TRUE;

              dsd_apm_validate_rule_type_with_token_content(from, to,
                                                         &valid_pdn_rule);
              dsd_apm_fill_attach_rule_validity(&(apn_info_block->apn_info),
                                                valid_pdn_rule);
            }
          }
          break;

          case ON_DEMAND_TYPE_RULES:
          {
            if ( valid_pdn_string == TRUE )
            {
              attach_pdn_rule = FALSE;
              dsd_apm_validate_rule_type_with_token_content(from, to,
                                                         &valid_pdn_rule);
              dsd_apm_fill_on_demand_rule_validity(&(apn_info_block->apn_info),
                                                   valid_pdn_rule);
            }
          }
          break;

          case PCSCF_REQUIRED:
          case IPADDRESS_REQUIRED:
          {
            if ( (valid_pdn_string == TRUE) && (valid_pdn_rule == TRUE) )
            {
              if ( attach_pdn_rule == TRUE )
              {
                if ( apn_info_block != NULL )
                {
                  dsd_apm_fill_attach_rule_with_token_content(
                     token_id,from,to,&(apn_info_block->apn_info));
                }
              }
              else
              {
                dsd_apm_fill_on_demand_rule_with_token_content(
                   token_id, from, to,&(apn_info_block->apn_info));
              }
            }
          }
          break;

          default:
            DATA_MSG1(MSG_LEGACY_ERROR,"Invalid APM rule param %d name", 
                      token_id);
            break;
        }
        
        if ( result == FALSE )
        {
          DATA_MSG0(MSG_LEGACY_ERROR,"Read APM rule EFS file fails");
          break;
        }
        /*-------------------------------------------------------------------
         set param_name as TRUE This means that next token is a param name
        -------------------------------------------------------------------*/
        on_param_name = TRUE;
        /* Set the seperator as : */
        efs_db->seperator = ':';
      }
    }
  }

  if ( apn_info_block != NULL )
  {
    q_link(apn_info_block, &(apn_info_block->link));
    q_put(rule_q, &(apn_info_block->link));
  }

  return result;
} /* dsd_apm_parse_pdn_rules_from_efs() */

/*===========================================================================
FUNCTION      DSD_APM_ATTACH_PROFILE_GET_TOKEN_ID

DESCRIPTION   This function returns the token id associated with each
              attach profiles parameter in EFS.

PARAMETERS    char *from - start of the token (param name)
              char *to   - end of the token (param name)

DEPENDENCIES  None

RETURN VALUE  0 - success
              1 - failure

SIDE EFFECTS  None
===========================================================================*/
int16 dsd_apm_attach_profile_get_token_id
(
  char      *from,
  char      *to,
  uint8     *token_id
)
{
  int16 ret_val = -1; /* return value */
  uint8 length;       /* length of the token (param name) */
  char para_name[] = "Attach_Profile_ID";
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  /* Get the length of the string that is passed */
  if  ( from > to )
  {
    DATA_MSG0(MSG_LEGACY_ERROR,"Invalid Parameters");
    return ret_val;
  }

  /* Assumption that the token length will not be greater than 255 */
  length =(uint8)(to-from);

  if ( (length == strlen(para_name)) &&
       ( 0 == strncasecmp(from, para_name, length)) )
  {
    *token_id = ATTACH_PROFILE_ID;
    ret_val = 0;
  }

  return ret_val;
} /*dsd_apm_attach_profile_get_token_id()*/

/*===========================================================================
FUNCTION      DSD_APM_FILL_ATTACH_PROFILE_WITH_TOKEN_CONTENT

DESCRIPTION   This function gets the data for each token and populates the
              structure with the appropriate data that corresponds to the
              token number.

PARAMETERS    token_id -  describes the current token which is
                              being populated into the EFS structure

              char *from, *to - start and end of the character array which
                          holds the data to populate the profile structure

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
boolean dsd_apm_fill_attach_profile_with_token_content
(
  uint8  token_id,
  char   *from,
  char   *to,
  apm_attach_profile  *attach_profile_info
)
{
  int profile_id;
  boolean result = TRUE;
  char    *start = from;
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  if ( attach_profile_info == NULL )
  {
    DATA_MSG0(MSG_LEGACY_ERROR,"NULL attach profile info passed");
    return FALSE;
  }

  /*----------------------------------------------------------------------
   Skip empty character
  ----------------------------------------------------------------------*/
  while ( *start == ' ' && start < to )
  {
    start++;
  }

  if ( token_id == ATTACH_PROFILE_ID )
  {
    profile_id = dsd_apm_atoi(start,to);
    if(profile_id < 0)  
    {
      result = FALSE;
      DATA_MSG1(MSG_LEGACY_ERROR,"Error parsing attach profile %d "
                "ID value in EFS file.", profile_id);
    }
    else
    {
      attach_profile_info->profile = profile_id;
      DATA_MSG1(MSG_LEGACY_HIGH,"Read attach profile %d in EFS", profile_id);
    }
  }
  else
  {
    result= FALSE;
    DATA_MSG0(MSG_LEGACY_ERROR,"Invalid attach profile param name");
  }
  return result;
}/* dsd_apm_fill_attach_profile_with_token_content() */
/*===========================================================================
FUNCTION      DSD_APM_PARSE_ATTACH_PROFILES_FROM_EFS

DESCRIPTION   This function read attach profile from EFS and save to passed 
              profile queue. 

              EFS File Format

              Param_Name:Param_Val;

              For example -
              Attach_Profile_ID:1;

PARAMETERS    efs_db - structure that contains info about the EFS file

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
boolean dsd_apm_parse_attach_profiles_from_efs
(
  sys_modem_as_id_e_type     subs_id,
  ds_efs_token_type         *efs_db,
  q_type                    *profile_q
)
{
  char  *from, *to; /* ptrs to start and end of a token */
  ds_efs_token_parse_status_enum_type  ret_val
                                              = DS_EFS_TOKEN_PARSE_SUCCESS;
  boolean param_name = TRUE; /* flag to keep track of the parser state
                                TRUE - parser expects Token Name in the EFS
                                FALSE - parser expects Token Val in the EFS*/
  uint8     token_id = 0;

  boolean   result = TRUE;
  attach_profile_data_block         *profile_block = NULL;
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  /* Set the seperator as : */
  efs_db->seperator = ':';
  while (DS_EFS_TOKEN_PARSE_EOF
          != (ret_val = ds_efs_tokenizer(efs_db, &from, &to )))
  {
    /*------------------------------------------------------------------------
      Token being read. from points to the beginning of the token and
      to points to the (end of the token+1).

      e.g: string= "token;" => from points to 't' and to points to ';'

      The tokenizer automatically skips blank lines and comments (lines
      beginning with #, so no need to check for them here).
    ------------------------------------------------------------------------*/
    if ((from == to) || (DS_EFS_TOKEN_PARSE_EOL == ret_val) )
    {
      /*----------------------------------------------------------------------
        Skip empty tokens.
      ----------------------------------------------------------------------*/
      if (param_name == FALSE)
      {
        param_name = TRUE;
        efs_db->seperator = ':'; 
      }
      continue;
    }

    else if ( DS_EFS_TOKEN_PARSE_SUCCESS == ret_val )
    {
      /*---------------------------------------------------------------------
      check if we are looking for param name or param value
      ---------------------------------------------------------------------*/
      if ( param_name == TRUE )
      {
        if ( dsd_apm_attach_profile_get_token_id(from, to, &token_id) < 0 )
        {
          DATA_MSG0(MSG_LEGACY_ERROR,
                    "Incorrect param name of attach profiles EFS");
        }
        else
        {
          DATA_MSG1(MSG_LEGACY_MED,"Token ID: %d", token_id);
          /*-----------------------------------------------------------------
          set param_name as FALSE This means the next token is a
          param value
          -----------------------------------------------------------------*/
          param_name = FALSE;
          /* set the seperator as ; */
          efs_db->seperator = ';';
        }
      }

      /*---------------------------------------------------------------------
      This means that the token is a param value
      ---------------------------------------------------------------------*/
      else
      {
        if ( (dsd_apm_attach_profile_param_name_type)token_id 
                                                     == ATTACH_PROFILE_ID)
        {
          if ( profile_block != NULL )
          {
            q_link(profile_block, &(profile_block->link));
            q_put(profile_q, &(profile_block->link));
            profile_block = NULL;
          }
          profile_block =(attach_profile_data_block*) modem_mem_alloc
                                           ( sizeof(attach_profile_data_block), 
                                             MODEM_MEM_CLIENT_DATA );
          if ( profile_block == NULL )
          {
            DATA_MSG0(MSG_LEGACY_ERROR,"Allocate memory failed");
            result = FALSE;
            break;
          }
          else
          {  
           memset(profile_block, 0, sizeof(attach_profile_data_block)); 
           profile_block->attach_profile.subs_id = subs_id;
           if ( (dsd_apm_fill_attach_profile_with_token_content( 
                   token_id, from, to, &(profile_block->attach_profile))) 
                == FALSE )
            {
             result = FALSE;
             modem_mem_free(profile_block, MODEM_MEM_CLIENT_DATA );
             profile_block = NULL;
            }
          }
        }

        if ( result == FALSE )
        {
          DATA_MSG0(MSG_LEGACY_ERROR,"Reading attach profile EFS file failed");
          break;
        }
        /*-------------------------------------------------------------------
         set param_name as TRUE This means that next token is a param name
        -------------------------------------------------------------------*/
        param_name = TRUE;
        /* Set the seperator as : */
        efs_db->seperator = ':';
      }
    }
  }

  /*-------------------------------------------------------------------
    Put the last profile ID info block to queue
  -------------------------------------------------------------------*/
  if ( profile_block != NULL )
  {
    q_link(profile_block, &(profile_block->link));
    q_put(profile_q, &(profile_block->link));
  }
  return result;
}/* dsd_apm_parse_attach_profiles_from_efs() */

/*===========================================================================
                      EXTERNAL FUNCTION DEFINITIONS
===========================================================================*/
/*===========================================================================
FUNCTION      DSD_APM_READ_PDN_RULE_LIST_FROM_EFS

DESCRIPTION   This function will read PDN rules from EFS

PARAMETERS    None

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
boolean dsd_apm_read_pdn_rule_list_from_efs
(
  sys_modem_as_id_e_type     subs_id,
  q_type                    *attach_prof_q,
  q_type                    *q
)
{
  ds_efs_token_type        efs_db;
  char                     efs_file_name[DSD_APM_RULE_MAX_PARAM_NAME_SIZE];
  boolean                  result = TRUE;
  char                    *apm_rules_string = NULL;
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  memset(efs_file_name,'\0',DSD_APM_RULE_MAX_PARAM_NAME_SIZE);

  switch (subs_id) 
  {
    case SYS_MODEM_AS_ID_1:
      strlcpy(efs_file_name, DSD_APM_RULES_FILE_SUBS_0, sizeof(efs_file_name));
      break;

    case SYS_MODEM_AS_ID_2:
      strlcpy(efs_file_name, DSD_APM_RULES_FILE_SUBS_1, sizeof(efs_file_name));
      break;

    default:
      DATA_MSG1(MSG_LEGACY_ERROR,
                "Error opening PDN rule EFS file. Invalid sub %d", subs_id);
      return FALSE;

  }


  DATA_MSG1(MSG_LEGACY_MED,"Read APM rules from efs on sub %d", subs_id);
  /* Open EFS file */
  if ( ds_efs_file_init(efs_file_name, &efs_db) == 0 )
  {
    result = dsd_apm_parse_pdn_rules_from_efs(subs_id, &efs_db, 
                                              attach_prof_q, q);
    /* Close EFS file */
    ds_efs_file_close(&efs_db);

    /*---------------------------------------------------------------------
      Update the rules in EFS if new attach profiles were read
    ---------------------------------------------------------------------*/
    if(attach_prof_status & ATTACH_PROF_CHANGED)
    {
      /*---------------------------------------------------------------------
        Create a new string from APM rules queue
      ---------------------------------------------------------------------*/
      apm_rules_string = (char*)modem_mem_alloc(DSD_APM_PROFILE_MAX_BUFF_SIZE,
                                                MODEM_MEM_CLIENT_DATA);
      if(apm_rules_string == NULL)
      {
        /*------------------------------------------------------------------
        This condition can not be recovered
        -------------------------------------------------------------------*/
        DATA_MSG0(MSG_LEGACY_ERROR,"APM couldn't get data_block_ptr memory " );
        result = FALSE;
        return result;
      }

      memset(apm_rules_string,0,DSD_APM_PROFILE_MAX_BUFF_SIZE);
      result = dsd_apm_gen_apm_rule_string(q, 
                                           apm_rules_string, 
					   DSD_APM_PROFILE_MAX_BUFF_SIZE,
                                           subs_id);

      if(result == FALSE)
      {
        DATA_MSG0(MSG_LEGACY_ERROR,
                  "Error generating apn rule string. Not updating apn rules");
        result = FALSE;
        return result;
      }

      /*---------------------------------------------------------------------
        Overwrite the existing EFS file with new string
      ---------------------------------------------------------------------*/
      dsd_apm_update_apn_rule_efs_file(subs_id, q, apm_rules_string);
    }

  }
  else
  {
    result = FALSE;
    DATA_MSG0(MSG_LEGACY_ERROR,"Error opening PDN rule EFS file");
  }

  if(apm_rules_string != NULL)
  {
    modem_mem_free(apm_rules_string, MODEM_MEM_CLIENT_DATA);
  }

  return result;
} /* dsd_apm_read_pdn_rule_list_from_efs() */

/*===========================================================================
FUNCTION      DSD_APM_READ_ATTACH_PROFILE_LIST_FROM_EFS

DESCRIPTION   This function will read attach profiles from EFS

PARAMETERS    None

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
boolean dsd_apm_read_attach_profile_list_from_efs
(
  sys_modem_as_id_e_type     subs_id,
  q_type                    *q
)
{
  ds_efs_token_type        efs_db;
  char                     efs_file_name
                           [DSD_APM_ATTACH_PROF_MAX_PARAM_NAME_SIZE];
  char                     tmp_file_name
                           [DSD_APM_ATTACH_PROF_MAX_PARAM_NAME_SIZE];
  struct fs_stat           fs_buf;
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  attach_prof_status = ATTACH_PROF_READ_FAIL;

  if(q == NULL)
  {
    DATA_MSG0(MSG_LEGACY_ERROR, "Invalid Input Params");
    return attach_prof_status;
  }

  memset(efs_file_name,'\0',DSD_APM_ATTACH_PROF_MAX_PARAM_NAME_SIZE);
  memset(&fs_buf, 0, sizeof(struct fs_stat));

  switch (subs_id) 
  {
    case SYS_MODEM_AS_ID_1:
      strlcpy(efs_file_name, DSD_APM_ATTACH_PROFILE_FILE_SUBS_0, 
              DSD_APM_ATTACH_PROF_MAX_PARAM_NAME_SIZE);
      strlcpy(tmp_file_name, DSD_APM_TMP_ATTACH_PROFILE_FILE_SUBS_0,
              DSD_APM_ATTACH_PROF_MAX_PARAM_NAME_SIZE);
      break;

    case SYS_MODEM_AS_ID_2:
      strlcpy(efs_file_name, DSD_APM_ATTACH_PROFILE_FILE_SUBS_1,
               DSD_APM_ATTACH_PROF_MAX_PARAM_NAME_SIZE);
      strlcpy(tmp_file_name, DSD_APM_TMP_ATTACH_PROFILE_FILE_SUBS_1,
              DSD_APM_ATTACH_PROF_MAX_PARAM_NAME_SIZE);
      break;

    default:
      DATA_MSG1(MSG_LEGACY_ERROR,
                "Error opening attach profile file. Invalid sub %d", subs_id);
      return attach_prof_status;

  }

  if(efs_stat(tmp_file_name,&fs_buf) == 0)
  {
    DATA_MSG0(MSG_LEGACY_MED, "Temp attach Profile List, exists copying ");
    if(efs_stat(efs_file_name, &fs_buf) == 0)
    {
      if(efs_unlink(efs_file_name) == -1)
      {
        DATA_MSG0(MSG_LEGACY_ERROR,"efs unlink original attach pdn list fails");
      }
    }

    if(efs_rename(tmp_file_name, efs_file_name) == -1)
    {
      DATA_MSG0(MSG_LEGACY_ERROR, 
                "Rename un-successful, stale contents will be read");
    }
    attach_prof_status |= ATTACH_PROF_CHANGED;
  }
  DATA_MSG0(MSG_LEGACY_MED,"Read attach profile list from EFS");
  /* Open EFS file */
  if ( ds_efs_file_init(efs_file_name, &efs_db) == 0 )
  {
    if(dsd_apm_parse_attach_profiles_from_efs(subs_id, &efs_db, q) == TRUE)
    {
      attach_prof_status |= ATTACH_PROF_READ_SUCCESS;
    }
    /* Close EFS file */
    ds_efs_file_close(&efs_db);
  }
  else
  {
    attach_prof_status = ATTACH_PROF_READ_FAIL;
    DATA_MSG0(MSG_LEGACY_ERROR,"Error opening attach profile file");
  }

  return attach_prof_status;
}/* dsd_apm_read_attach_profile_list_from_efs() */

/*===========================================================================
FUNCTION      DSD_APM_READ_DEFAULT_ATTACH_PROF_FILE

DESCRIPTION   This function will read attach profiles from the PDP registry

PARAMETERS    subs_id:                     Current active Data subs 
              attach_prof_q:               Queue that contains the attach
                                           profiles

DEPENDENCIES  None

RETURN VALUE  TRUE: If we were able to obtain default attach prof 
              FALSE: Otherwise 

SIDE EFFECTS  Modification of global attach profile queue of APM
===========================================================================*/
boolean dsd_apm_read_default_attach_prof 
(
  sys_modem_as_id_e_type                 subs_id,
  q_type                                *attach_prof_q
)
{
  ds_umts_pdp_profile_status_etype        profile_status = DS_UMTS_PDP_FAIL;
  byte                                    def_sock_prof = 0;
  boolean                                 ret_val = FALSE;
  attach_profile_data_block              *profile_block = NULL;
  ds_umts_pdp_subs_e_type                 umts_subs_id 
                                            = DS_UMTS_PDP_SUBSCRIPTION_NONE;
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  umts_subs_id = dsumts_subs_mgr_get_subs_id(subs_id);
  DS_UMTS_CONVERT_SUBS_ID_FOR_PDPREGINT(umts_subs_id);

  profile_status = ds_umts_get_default_profile_number_per_subs(
                                             DS_UMTS_EMBEDDED_PROFILE_FAMILY,
                                             umts_subs_id,
                                             &def_sock_prof);

  if(profile_status == DS_UMTS_PDP_SUCCESS)
  {
    DATA_MSG1_MED ("Default Socket Profile read from PDP registry is %d",
                   def_sock_prof);
    profile_block = (attach_profile_data_block *)modem_mem_alloc (
                     sizeof(attach_profile_data_block), MODEM_MEM_CLIENT_DATA);
    if(profile_block != NULL)
    {
      memset (profile_block, 0, sizeof(attach_profile_data_block));
      profile_block->attach_profile.profile = def_sock_prof;
      profile_block->attach_profile.subs_id = subs_id;

      q_link(profile_block, &(profile_block->link));
      q_put(attach_prof_q, &(profile_block->link));

      ret_val = TRUE;
    }
    else
    {
      DATA_MSG0_ERROR ("Unable to allocate memory, cannot insert default attach "
                       "profile block in Attach Profile Queue");
    }
  }
  else
  {
    DATA_MSG1_ERROR ("PDP Registry read not successful, Return Status: %d",
                     profile_status);
  }
  
  return ret_val;
}
/*===========================================================================
FUNCTION       DSD_APM_COPY_PARAM_TO_STRING

DESCRIPTION    Copies the param name to the string and add : seperator

DEPENDENCIES   NONE

RETURN VALUE   Length of characters copied to the string

SIDE EFFECTS   NONE
===========================================================================*/
uint16 dsd_apm_copy_param_to_string
(
  char                                  * str,
  dsd_apm_rule_param_name_type            param,
  char                                  * param_val,
  uint16                                  param_val_len,
  uint16                                  remaining_buffer_size
)
{
  uint16 length=0;
  uint16 tmp_len=0;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  DATA_MSG3(MSG_LEGACY_HIGH,
            "dsd_apm_copy_param_to_string: str:0x%xd, param:%d, "
            "param_val_len:%d",
            str,param,param_val_len);

  length = strlen(dsd_apm_rule_param_name_id[param])+param_val_len+4;
  DATA_MSG1(MSG_LEGACY_HIGH,"length: %d",length);

  if (length > remaining_buffer_size)
  {
    DATA_MSG0(MSG_LEGACY_ERROR,"dsd_apm_copy_param_to_string: Buffer Overflow"
                               " detected");
    return tmp_len;
  }
  /*---------------------------------------------------------------------
    if PCSCF_REQUIRED or IPADDRESS_REQUIRED, the param_val is a char.
    else, param_val is a string
  ---------------------------------------------------------------------*/
  if((param == PCSCF_REQUIRED) || (param == IPADDRESS_REQUIRED))
  {
    DATA_MSG_SPRINTF_1(MSG_LEGACY_HIGH,"param_val:%c",*param_val);

    tmp_len=snprintf(str,
                     length,
                     "%s:%c;\n",
                     dsd_apm_rule_param_name_id[param],
                     *param_val);
  }
  else
  {
    DATA_MSG_SPRINTF_1(MSG_LEGACY_HIGH,"param_val:%s",param_val);

    tmp_len=snprintf(str,
                     length,
                     "%s:%s;\n",
                     dsd_apm_rule_param_name_id[param],
                     param_val);
  }
  
  DATA_MSG1(MSG_LEGACY_HIGH,"tmp_len: %d",tmp_len);
  DATA_MSG_SPRINTF_1(MSG_LEGACY_HIGH,"str:%s",str);

  return tmp_len;
}/* dsd_apm_copy_param_to_string */

/*===========================================================================
FUNCTION       DSD_APM_GEN_APM_RULE_STRING

DESCRIPTION    Creates apm rule string from apm_rule_q

DEPENDENCIES   NONE

RETURN VALUE   NONE

SIDE EFFECTS   NONE
===========================================================================*/
boolean dsd_apm_gen_apm_rule_string
(
  q_type                  *rule_q,
  char                    *str,
  uint16                        buffer_size,
  sys_modem_as_id_e_type  subs_id
)
{
  char                          ipaddr_char;
  uint16                        start_pos=0;
  uint16                        length=0;
  ds_dsd_apm_data_block        *apn_data_block_temp = NULL;
  uint16                        remaining_buffer_size = buffer_size;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if(rule_q == NULL || buffer_size == 0)
  {
    DATA_MSG0(MSG_LEGACY_ERROR,"queue is empty");
    return FALSE;
  }

  /*---------------------------------------------------------------------
    Loop through the queue and create the string
  ---------------------------------------------------------------------*/
  /* grab first item in the queue */
  apn_data_block_temp = (ds_dsd_apm_data_block*)q_check(rule_q);

  while(apn_data_block_temp != NULL)
  {
    /* process only if subs id matches */
    if (apn_data_block_temp->apn_info.subs_id == subs_id)
    {
      // 1. copy - APN_STRING
      length = dsd_apm_copy_param_to_string(
                 str+start_pos,
                 APN_STRING,
                 apn_data_block_temp->apn_info.apn_name,
                 apn_data_block_temp->apn_info.apn_name_len,
                 remaining_buffer_size);
     
      if(length == 0)
      {
        DATA_MSG0 (MSG_LEGACY_ERROR, "dsd_apm_gen_apm_rule_string: Could not "
                   "copy parameter to string buffer");
        return FALSE;
      }

      start_pos+= length;
      remaining_buffer_size -= length;

      // 2. copy - ATTACH_TYPE_RULES
      if(apn_data_block_temp->apn_info.AttachPdn.attach_rule_valid == TRUE)
      {
        length = dsd_apm_copy_param_to_string(str+start_pos,
                                              ATTACH_TYPE_RULES,
                                              "TRUE",
                                              strlen("TRUE"),
                                              remaining_buffer_size);
      }
      else
      {
        length = dsd_apm_copy_param_to_string(str+start_pos,
                                              ATTACH_TYPE_RULES,
                                              "FALSE",
                                              strlen("FALSE"),
                                              remaining_buffer_size);
      }
      if(length == 0)
      {
        DATA_MSG0 (MSG_LEGACY_ERROR, "dsd_apm_gen_apm_rule_string: Could not "
                   "copy parameter to string buffer");
        return FALSE;
      }
      start_pos+= length;
      remaining_buffer_size -= length;

      // 3. copy - PCSCF_REQUIRED
      ipaddr_char = '0';
      if((apn_data_block_temp->apn_info.AttachPdn.config_rule.config_valid_flag|
         CONFIG_PCSCF) != 0)
      {
        ipaddr_char 
          = ds_dsd_apm_translate_ipreq_to_char(
             apn_data_block_temp->apn_info.AttachPdn.config_rule.pcscf_address);
      }
      length = dsd_apm_copy_param_to_string(str+start_pos,
                                         PCSCF_REQUIRED,
                                         &ipaddr_char,
                                          1,
                                          remaining_buffer_size);
      if(length == 0)
      {
        DATA_MSG0 (MSG_LEGACY_ERROR, "dsd_apm_gen_apm_rule_string: Could not "
                   "copy parameter to string buffer");
        return FALSE;
      }
      start_pos+= length;
      remaining_buffer_size -= length;

      // 4. copy - IPADDRESS_REQUIRED
      ipaddr_char = '0';
      if((apn_data_block_temp->apn_info.AttachPdn.config_rule.config_valid_flag|
         CONFIG_IP_ADDRESS) != 0)
      {
        ipaddr_char 
          = ds_dsd_apm_translate_ipreq_to_char(
             apn_data_block_temp->apn_info.AttachPdn.config_rule.ip_address);
      }
      length = dsd_apm_copy_param_to_string(str+start_pos,
                                         IPADDRESS_REQUIRED,
                                         &ipaddr_char,
                                          1, 
                                          remaining_buffer_size);
      if(length == 0)
      {
        DATA_MSG0 (MSG_LEGACY_ERROR, "dsd_apm_gen_apm_rule_string: Could not "
                   "copy parameter to string buffer");
        return FALSE;
      }
      start_pos+= length;
      remaining_buffer_size -= length;

      // 5. copy - ON_DEMAND_TYPE_RULES
      if(apn_data_block_temp->apn_info.OnDemandPdn.on_demand_rule_valid == TRUE)
      {
        length = dsd_apm_copy_param_to_string(str+start_pos,
                                              ON_DEMAND_TYPE_RULES,
                                              "TRUE",
                                              strlen("TRUE"),
                                              remaining_buffer_size);
      }
      else
      {
        length = dsd_apm_copy_param_to_string(str+start_pos,
                                              ON_DEMAND_TYPE_RULES,
                                              "FALSE",
                                              strlen("FALSE"),
                                              remaining_buffer_size);
      }
      if(length == 0)
      {
        DATA_MSG0 (MSG_LEGACY_ERROR, "dsd_apm_gen_apm_rule_string: Could not "
                   "copy parameter to string buffer");
        return FALSE;
      }
      start_pos+= length;
      remaining_buffer_size -= length;

      // 6. copy  - PCSCF_REQUIRED
      ipaddr_char = '0';
      if((apn_data_block_temp->apn_info.OnDemandPdn.config_rule.config_valid_flag|
         CONFIG_PCSCF) != 0)
      {
        ipaddr_char 
          = ds_dsd_apm_translate_ipreq_to_char(
             apn_data_block_temp->apn_info.OnDemandPdn.config_rule.pcscf_address);
      }
      length = dsd_apm_copy_param_to_string(str+start_pos,
                                         PCSCF_REQUIRED,
                                         &ipaddr_char,
                                          1, 
                                          remaining_buffer_size);
      if(length == 0)
      {
        DATA_MSG0 (MSG_LEGACY_ERROR, "dsd_apm_gen_apm_rule_string: Could not "
                   "copy parameter to string buffer");
        return FALSE;
      }
      start_pos+= length;
      remaining_buffer_size -= length;

      // 7. copy - IPADDRESS_REQUIRED
      ipaddr_char = '0';
      if((apn_data_block_temp->apn_info.OnDemandPdn.config_rule.config_valid_flag|
         CONFIG_IP_ADDRESS) != 0)
      {
        ipaddr_char 
          = ds_dsd_apm_translate_ipreq_to_char(
             apn_data_block_temp->apn_info.OnDemandPdn.config_rule.ip_address);
      }
      length = dsd_apm_copy_param_to_string(str+start_pos,
                                         IPADDRESS_REQUIRED,
                                         &ipaddr_char,
                                          1,
                                          remaining_buffer_size);
      if(length == 0)
      {
        DATA_MSG0 (MSG_LEGACY_ERROR, "dsd_apm_gen_apm_rule_string: Could not "
                   "copy parameter to string buffer");
        return FALSE;
      }
      start_pos+= length;
      remaining_buffer_size -= length;

      DATA_MSG1(MSG_LEGACY_HIGH,"total len = %d",start_pos);
    }

    /* get next element in the queue */
    apn_data_block_temp = q_next(rule_q,&(apn_data_block_temp->link));
  }// end while

  return TRUE;
}/* dsd_apm_gen_apm_rule_string() */

/*===========================================================================
FUNCTION      DSD_APM_UPDATE_APN_RULE_EFS_FILE

DESCRIPTION   This function will overwrite the EFS file with string passed

PARAMETERS    None

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
boolean dsd_apm_update_apn_rule_efs_file
(
  sys_modem_as_id_e_type     subs_id,
  q_type                    *q, 
  char*                     writebuff
)
{
  int     fd = -1;
  fs_ssize_t  efs_ret = -1;
  uint16  writebufflen = 0;
  char    efs_file_name[DSD_APM_RULE_MAX_PARAM_NAME_SIZE];
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  
  memset(efs_file_name,'\0',DSD_APM_RULE_MAX_PARAM_NAME_SIZE);

  switch (subs_id) 
  {
    case SYS_MODEM_AS_ID_1:
      strlcpy(efs_file_name, DSD_APM_RULES_FILE_SUBS_0, sizeof(efs_file_name));
      break;

    case SYS_MODEM_AS_ID_2:
      strlcpy(efs_file_name, DSD_APM_RULES_FILE_SUBS_1, sizeof(efs_file_name));
      break;

    default:
      DATA_MSG1(MSG_LEGACY_ERROR,
                "Error opening PDN rule EFS file. Invalid sub %d", subs_id);
      return FALSE;

  }

  DATA_MSG1(MSG_LEGACY_MED,"dsd_apm_update_apn_rule_efs_file for sub %d",
            subs_id);

  //open the APM rules file and truncate the file
  fd = efs_open(efs_file_name, O_WRONLY|O_TRUNC);

  writebufflen = strlen(writebuff);
  if (writebufflen)
  {
    DATA_MSG1(MSG_LEGACY_MED,"Writing %d bytes to EFS", writebufflen);
    efs_ret = efs_write(fd, writebuff, writebufflen);
    if((-1 == efs_ret) || (efs_ret != writebufflen))
    {
      DATA_MSG1(MSG_LEGACY_ERROR,"EFS Write failed. Efs_ret:%d", efs_ret);
      return FALSE;
    }
  }

  if (-1 == efs_close (fd))
  {
    DATA_MSG0(MSG_LEGACY_ERROR,"EFS close failed");
    return FALSE;
  }
  
  return TRUE;
}/* dsd_apm_update_apn_rule_efs_file() */

/*===========================================================================
FUNCTION      DS_DSD_APM_TRANSLATE_IPREQ_TO_CHAR

DESCRIPTION   This is a utility to translate IP Required enum type to char

DEPENDENCIES  None.

RETURN VALUE

SIDE EFFECTS
===========================================================================*/
char  ds_dsd_apm_translate_ipreq_to_char
(
  ipRequiredType ip_req
)
{
  char ch;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  switch (ip_req)
  {
    case ipv4_mandatory_and_ipv6_donotcare:
      ch = '1';
      break;
    case ipv6_mandatory_and_ipv4_donotcare:
      ch = '2';
      break;
    case ipv4_mandatory_and_ipv6_mandatory:
      ch = '3';
      break;
    case ipv4_dontcare_and_ipv6_donotcare:
      ch = '4';
      break;
    case ipv4_mandatory_or_ipv6_mandatory:
      ch = '5';
      break;
    default:
      ch = '0';
      break;
  }
  return ch;
}/* ds_dsd_apm_translate_ipreq_to_char() */

/*===========================================================================
FUNCTION      DS_DSD_APM_READ_APN_SWITCHING_NV

DESCRIPTION   This function get the NV configuration to know if feature 
              enable or not. By default, feature is enabled.

DEPENDENCIES  None.

RETURN VALUE

===========================================================================*/
boolean ds_dsd_apm_read_apn_switching_nv(void)
{
  char item_file_path[] = "/nv/item_files/data/dsd/ds_apn_switching";
  nv_stat_enum_type           read_efs_nv_retval;
  boolean                     nv_enable = TRUE;
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  read_efs_nv_retval = ds_read_efs_nv(item_file_path, 
                                     &nv_enable, 
                                     sizeof(boolean));
  
  if ( NV_DONE_S == read_efs_nv_retval )
  {
    /* This is for QTF testing */
    if ( nv_enable == '0' )
    {
      nv_enable = FALSE;
    }
    else if ( nv_enable == '1')
    {
      nv_enable = TRUE;
    }
    DATA_MSG1(MSG_LEGACY_MED,
              "Attach PDN switching enable/disable %d in NV item ", 
              nv_enable);
  }
  else
  {
    DATA_MSG1(MSG_LEGACY_MED,
              "NV item not configured %d, enable attach PDN switching", 
              nv_enable);
    nv_enable = TRUE;
  }

  return nv_enable;
}/*ds_dsd_apm_read_apn_switching_nv()*/

/*===========================================================================
FUNCTION      DS_DSD_APM_READ_LTE_REL_VER_NV

DESCRIPTION   This function get the NV configuration to know the LTE release 
              version. 

DEPENDENCIES  None.

RETURN VALUE

===========================================================================*/
boolean  ds_dsd_apm_read_lte_release10_nv
( 
  lte_3gpp_release_ver_e *buf
)
{
  if( buf == NULL ) 
  {
    DATA_MSG0(MSG_LEGACY_ERROR,"passed buf ptr is empty, return");
    return FALSE;
  }

  ds_pdn_cntxt_read_lte_rel_ver_from_nv(buf);
  return TRUE;
}/* ds_dsd_apm_read_lte_release10_nv() */

/*===========================================================================
FUNCTION      DS_DSD_APM_READ_LTE_REL_VER_NV

DESCRIPTION   This function get the NV configuration to know the LTE release 
              version. 

DEPENDENCIES  None.

RETURN VALUE

===========================================================================*/
boolean  ds_dsd_apm_read_release10_throttling
( 
  uint8 *buf
)
{

  char item_file_path[] = "/nv/item_files/modem/data/3gpp/ps/rel_10_throttling";
  nv_stat_enum_type          read_efs_nv_retval;
  boolean                    ret_val = FALSE;
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  DATA_MSG0(MSG_LEGACY_HIGH,"ds_dsd_apm_read_release10_throttling");
  if( buf == NULL ) 
  {
    DATA_MSG0(MSG_LEGACY_ERROR,"passed buf ptr is empty, return FALSE");
    return FALSE;
  }

  read_efs_nv_retval = ds_read_efs_nv(item_file_path,
                                      buf,
                                      sizeof(uint8));

  if( NV_DONE_S == read_efs_nv_retval ) 
  {
    DATA_MSG1(MSG_LEGACY_HIGH,"Release 10 Throttling Setting: %d",
              *buf);
    ret_val = TRUE;
  }
  else
  {
    DATA_MSG0(MSG_LEGACY_ERROR,"Read Release 10 Throttling Setting NV failed");
  }

  return ret_val;

}

/*===========================================================================
FUNCTION      DS_DSD_APM_EFS_OVERWRITE_ATTACH_PDN_LIST

DESCRIPTION   This function overwrite the attach PDN list in EFS with input 
              attach PDN profile ID array 

DEPENDENCIES  None.

RETURN VALUE  TRUE: Operation succeeds 
              FALSE: Operation fails 

===========================================================================*/
boolean ds_dsd_apm_efs_overwrite_attach_pdn_list
(
  sys_modem_as_id_e_type     subs_id,
  uint16                    *profile_id_arr,
  uint8                      num_of_pdn
)
{
  int                        fd = -1;
  uint8                      i = 0;
  char                       temp_buf[100] = {0};
  char                       prefix[] = "Attach_Profile_ID";
  fs_ssize_t                 efs_ret = -1;
  int32                      dir_result = 0;
  char                       tmp_file_name
                                  [DSD_APM_ATTACH_PROF_MAX_PARAM_NAME_SIZE];
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  if (NULL == profile_id_arr || 
      0 == num_of_pdn ||
      DS_SYS_LTE_ATTACH_PDN_PROFILE_LIST_MAX < num_of_pdn)
  {
    DATA_MSG0(MSG_LEGACY_ERROR, "Invalid Input arguments");
    return FALSE;
  }

  memset(tmp_file_name,'\0',DSD_APM_ATTACH_PROF_MAX_PARAM_NAME_SIZE);
  
  switch (subs_id) 
  {
    case SYS_MODEM_AS_ID_1:
      strlcpy(tmp_file_name, DSD_APM_TMP_ATTACH_PROFILE_FILE_SUBS_0,
              DSD_APM_ATTACH_PROF_MAX_PARAM_NAME_SIZE);
      break;

    case SYS_MODEM_AS_ID_2:
      strlcpy(tmp_file_name, DSD_APM_TMP_ATTACH_PROFILE_FILE_SUBS_1,
              DSD_APM_ATTACH_PROF_MAX_PARAM_NAME_SIZE);
      break;

    default:
      DATA_MSG1(MSG_LEGACY_ERROR,
                "Error opening attach profile file. Invalid sub %d", subs_id);
      return FALSE;

  }

  DATA_MSG1(MSG_LEGACY_MED, 
            "ds_dsd_apm_efs_overwrite_attach_pdn_list on sub %d", subs_id);

  dir_result = ds_path_is_directory(DSD_APM_ROOT_DIR);
  if( 0 != dir_result )
  {
    /* Directory doesn't exist yet */
    DATA_MSG0(MSG_LEGACY_HIGH,"data directory doesn't exist, creat one");
    if (0 != efs_mkdir( DSD_APM_ROOT_DIR,
                        S_IREAD|S_IWRITE|S_IEXEC))
    {
      DATA_MSG1(MSG_LEGACY_ERROR,"efs_mkdir Unable to create dir Err: %d",
                efs_errno);
      return FALSE;
    }
  }

  /* Create a tmp efs file*/
  fd = efs_open(tmp_file_name,
                O_CREAT | O_TRUNC | O_WRONLY, DEFFILEMODE);
  if (-1 == fd)
  {
    DATA_MSG1(MSG_LEGACY_ERROR,"create tmp attach pdn list Err: %d", efs_errno);
    return FALSE;
  }
  for (i=0; i<num_of_pdn; i++)
  {
    memset(temp_buf, 0, sizeof(temp_buf));
	snprintf(
             temp_buf,
             sizeof(temp_buf) - 1,
             "%s:%d;\r\n",
             prefix,profile_id_arr[i]
            );
    efs_ret = efs_write(fd, temp_buf, strlen(temp_buf));
    if (-1 == efs_ret || efs_ret != strlen(temp_buf))
    {
      DATA_MSG0(MSG_LEGACY_ERROR,"efs write attach pdn list fails");
      return FALSE;
    }
    DATA_MSG1(MSG_LEGACY_MED,"efs write attach pdn list, ID:%d", 
              profile_id_arr[i]);   
  }

  if (-1 == efs_close (fd))
  {
    DATA_MSG0(MSG_LEGACY_ERROR,"close attach pdn list fails");
    return FALSE;
  }

  return TRUE;
}

/*===========================================================================
FUNCTION      DSD_APM_GET_PROFILE_PARAM_INFO

DESCRIPTION   This function gets the profile param info from profile cache.

DEPENDENCIES  None

RETURN VALUE  TRUE: If read is successful 
              FALSE: Otherwise 

SIDE EFFECTS  None
===========================================================================*/
boolean dsd_apm_get_profile_param_info
(
  uint16                      profile_id,
  ds_profile_identifier_type  profile_param_id,
  ds_profile_info_type       *profile_param_info_ptr,
  sys_modem_as_id_e_type      subs_id
)
{
  ds_profile_status_etype           result;
  ds_profile_hndl_type              profile_handle = NULL;
  boolean                           ret_val = FALSE;
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  if( profile_param_info_ptr == NULL )
  {
    DATA_MSG0(MSG_LEGACY_ERROR,"NULL profile_param_info ptr");
    return FALSE;
  }

  do
  {
    result = ds_profile_begin_transaction_per_sub(
                                           DS_PROFILE_TRN_READ,
                                           DS_PROFILE_TECH_3GPP,
                                           (ds_profile_num_type)profile_id,
                                  ds3gsubsmgr_subs_id_cm_to_dsprofile(subs_id),
                                           &profile_handle );

    if( DS_PROFILE_REG_RESULT_SUCCESS != result ||
        profile_handle == NULL )
    {
      DATA_MSG2(MSG_LEGACY_ERROR,"Begin transaction failed %d %x", 
                result,profile_handle);
      break;
    }

    result = ds_profile_get_param( profile_handle,
                                   profile_param_id,
                                   profile_param_info_ptr);

    if( DS_PROFILE_REG_RESULT_SUCCESS != result )
    {
      DATA_MSG0(MSG_LEGACY_ERROR,"Read profile failed");
      break;
    }

    ret_val = TRUE;
  }while (0);

  if( NULL != profile_handle )
  {
    result = ds_profile_end_transaction (profile_handle,
                                         DS_PROFILE_ACTION_CANCEL);
  }

  return ret_val;
}/* dsd_apm_get_profile_param_info() */

#endif /* FEATURE_DATA_LTE */
