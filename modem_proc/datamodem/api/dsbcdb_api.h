#ifndef DSBCDB_API_H
#define DSBCDB_API_H
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                           D S B C D B _ A P I . H

GENERAL DESCRIPTION
  This file contains the types and functions needed to access 
  the BCMCS database module. This file is the interface between 
  BCDBAPP and the other modules (HDRMC) that need to use it. However, it
  is only meant for BINARY SHPPING and should only be included in source
  files that are shipped as binary. 

  Structures and definitions used internally by BCDBAPP should not be in 
  this file.

EXTERNALIZED FUNCTIONS
  DSBCDB_POST_FLOW_INFO_CMD
    Posts a command to BCDBAPP to get the information for a desired flow.

  DSBCDB_POST_UPDATE_DB_INFO_CMD
    Posts a UPDATE_DB_INFO command to BCDBAPP

  DSBC_1XHDR_PUT_PKT
    Discards any meta info and send the IP datagramm back to the stack.

INITIALIZATION AND SEQUENCING REQUIREMENTS
  dsbcdb_task() must be called first from TMC to start the BCDBAPP task.

 Copyright (c) 2004-2009 by Qualcomm Technologies Incorporated.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================
                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/api/dsbcdb_api.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

-----------------------------------------------------------------------------
when        who    what, where, why
--------    ---    -------------------------------------------------------
10/15/09    vs     SU Level Api Defeaturization
09/11/09    ss     SU level CMI modifications.
09/03/08    ms     BCMCS 2.0 Phase3 Flow Priority Related 
                   enhancements
11/26/07    ms     Feature enhancement for BCMCS 2.0
08/07/06    as     added param for errno to dsbcdb_post_update_db_info_cmd
05/31/06    mpa    Changed dsbcdb_cmd_handle_type to a uint32
04/05/06    mpa    Added flow format 1 specific info to flow info type.
08/26/05    mpa    Use sys.h ip address type for CM cmds
12/16/04    rsl    Added support for flow_format=1 based on changes in standard.
08/23/04    vr     Moved protoype for dsbcdb_task() to a separate header
                   file for binary shipping of code.
06/20/04    mct    Dependency on ip_addr_type, included dss_iface_ioctl.h.
06/08/04    vr     Modified comment
03/11/04    vr     Initial revision.
===========================================================================*/

/*===========================================================================
                      INCLUDE FILES
===========================================================================*/
#include "dsbcmcs_defs.h"
#include "dss_iface_ioctl.h"
#include "ps_iface_ioctl.h"
#include "ps_meta_info.h"
#include "sys.h"

/*===========================================================================
                        TYPEDEFS AND VARIABLES
===========================================================================*/

/*---------------------------------------------------------------------------
  Invalid command handle that will be returned to the calling module if 
  the API fails to post a cmd to BCDBAPP.

  If the size of dsbcdb_cmd_handle_type changes, this value should be
  modified.
---------------------------------------------------------------------------*/
#define DSBCDB_INVALID_CMD_HANDLE 0xFFFFFFFF

/*--------------------------------------------------------------------------- 
  The maxmimum number of ports that can be present in the database for a 
  given IP address. The reply to a "get flow info" query will return a list
  with a maximum of this many entries.
---------------------------------------------------------------------------*/
#define DSBCDB_MAX_FLOWS_PER_IP 5

/*---------------------------------------------------------------------------
  The BCMCS flow id type. This can be different for 1X and HDR
---------------------------------------------------------------------------*/
typedef uint32 dsbcmcs_hdr_flow_id_type;

/*---------------------------------------------------------------------------
  Define the BCMCS flow id to be an union so that it can be expandable in the 
  future 
---------------------------------------------------------------------------*/
typedef union
{
  dsbcmcs_hdr_flow_id_type hdr_flow_id;
  /*-------------------------------------------------------------------------
    Add more fields here later if 1X/UMTS have different flow_id types.
    Make this an union since this struct will be used by other modules,
    so if this is an union the other modules' code doesn't need to change
    later.
  -------------------------------------------------------------------------*/
} dsbcmcs_flow_id_type;

/*---------------------------------------------------------------------------
  dsbcdb_flow_info_type is the externalized type that will be provided to 
  the modules requesting flow info. This does not contain the zone/subnet
  and multicast IP address. That information needs to be provided and used
  in conjunction with this struct (see the struct dsbcdb_flow_list_type 
  below) in order to get the complete flow information.

  Internally, BCDBAPP may maintain this struct along with additional 
  information.
---------------------------------------------------------------------------*/
typedef struct
{
  dsbcmcs_flow_id_type flow_id;
  uint16 port;

  /*-------------------------------------------------------------------------
    Whether the payload is framed using segment based or HDLC framing.
  -------------------------------------------------------------------------*/
  dsbcmcs_framing_enum_type framing;

  /*-------------------------------------------------------------------------
    The protocol field tells the deframer where to deliver the packet.

    If protocol field is BCMCS_PROTOCOL_PPP that means the
    packet is PPP encapsulated and the first one or two bytes tell 
    whether the rest of the packet is IPv4/v6 or some other protocol. The
    PPP encapsulation algorithm can unambiguously determine 
    whether one byte or two bytes have been used in the encapsulation.

    If protocol field is BCMCS_PROTOCOL_IPv4/v6 that means the 
    packet obtained after deframing is an IPv4/v6 packet and can be
    delivered directly to the IPv4/v6 stack.
  -------------------------------------------------------------------------*/
  dsbcmcs_protocol_enum_type protocol;

  /*-------------------------------------------------------------------------
    Whether the CRC is 0 or 2 bytes long
  -------------------------------------------------------------------------*/
  uint8 crc_len;

  uint8 flow_format;
  /*-------------------------------------------------------------------------
   if flow_format is 1 - format 1 info 
  -------------------------------------------------------------------------*/
  uint32 program_id;
  uint8  program_id_len;
  uint8  flow_discrim_len;
  uint8  flow_discrim;
  uint8  flow_id_len; /* The length of the FlowID in octets */
  /* phase 3*/
  uint8  flow_priority;
  /* Additional fields (eg: security params) will be added in the future   */

} dsbcdb_flow_info_type;

/*---------------------------------------------------------------------------
  dsbcdb_flow_list_type is the type that will be returned to the modules 
  requesting the flow info. For each zone and multicast IP address, there
  may be a list of port numbers and corresponding flow information
  associated with them. The zone and IP address and provided only once since
  they are relatively big (HDR subnet = 32 bytes, IPv6 address = 16 bytes).

  num_flows gives how many flows associated with the given zone and IP
  address and provided in this structure. This may be different from the
  total number of flows in the database corresponding to the zone and IP,
  depending on which query is issued.

  This struct is provided as a response to GET_FLOW_INFO_CMD.
---------------------------------------------------------------------------*/
typedef struct
{
  dsbcmcs_zone_type zone;
  sys_ip_address_s_type multicast_ip;
  uint8 num_flows;
  dsbcdb_flow_info_type flow_info[DSBCDB_MAX_FLOWS_PER_IP]; 
} dsbcdb_flow_list_type;

/*---------------------------------------------------------------------------
  The handle returned for a command. The calling module must match the handle
  returned when making a request with the handle in the response to 
  unambigiously match the response to the request.
---------------------------------------------------------------------------*/
typedef uint32 dsbcdb_cmd_handle_type;

/*---------------------------------------------------------------------------
  Definition of callback function to be provided for GET_FLOW_INFO cmd 
---------------------------------------------------------------------------*/
typedef void (*dsbcdb_flow_info_cb_type) 
(
  dsbcdb_cmd_handle_type handle,
  const dsbcdb_flow_list_type *flow_list, /* The callback function should
                                              make a copy of this list. The
                                              passed list struct would no
                                              longer be valid after the
                                              callback function returns.
                                           */
  sint15 err_num
); /* dsbcdb_flow_info_cb_type */


/*===========================================================================
                   EXTERNAL FUNCTION PROTOTYPES 
===========================================================================*/
/*===========================================================================
FUNCTION      DSBCDB_POST_GET_FLOW_INFO_CMD

DESCRIPTION   API to post a GET_FLOW_INFO command to BCDBAPP. This API is
              to be used only to get the link and security layer information.
              A separate API will be provided to get the application layer
              information.

              ipaddr and port must be in network byte order.

DEPENDENCIES  None.

PARAMETERS
  handle             - Handle associated with the command.
  zone               - Subnet for HDR or PZID for 1x.
  ipaddr             - IP address.
  port               - Port number.
  callback_func      - Callback function pointer for flow info command.

RETURN VALUE  Returns the handle assigned to the request. If unable to post
              a command, returns DSBCDB_INVALID_CMD_HANDLE

SIDE EFFECTS  None.
===========================================================================*/
dsbcdb_cmd_handle_type dsbcdb_post_get_flow_info_cmd
(
  dsbcdb_cmd_handle_type handle,
  dsbcmcs_zone_type zone,
  sys_ip_address_s_type ipaddr,
  uint16 port,
  dsbcdb_flow_info_cb_type callback_func
);

/*===========================================================================
FUNCTION      DSBCDB_POST_UPDATE_DB_INFO_CMD

DESCRIPTION   API to post a UPDATE_DB_INFO command to BCDBAPP. 
              ipaddr and port must be in network byte order.
              
DEPENDENCIES  None.

PARAMETERS
  update_rec_ptr      - Pointer to bcmcs db update ioctl structure
  cmd_errno           - Pointer to the error number which stores the result of
                        db update.

RETURN VALUE  Returns whether the command was posted successfully

SIDE EFFECTS  If there is an error, the cmd_errno will return the enum
              that describes the error.
===========================================================================*/
int dsbcdb_post_update_db_info_cmd
(
  ps_iface_ioctl_bcmcs_db_update_type *update_rec_ptr,
  sint15                              *cmd_errno
);

/*===========================================================================
FUNCTION DSBC_1XHDR_PUT_PKT()

DESCRIPTION
  This function is used as transmit command for the 1X-HDR BCMCS interface.
  It discards any meta info and send the IP datagramm back to the stack.

DEPENDENCIES
  None

PARAMETERS
  pkt_chain_ptr      - Reference pointer to IP datagramm
  meta_info_ref_ptr  - Reference pointer to packet's meta info
  tx_cmd_info        - User data pointer (not used)

RETURN VALUE
  0 on success, -1 if BCMCS interface is down. 
  If BCMCS interface is down, the packet is not delivered up the stack.

SIDE EFFECTS
  None
===========================================================================*/
int dsbc_1xhdr_put_pkt
(
  dsm_item_type**      pkt_chain_ptr,
  ps_meta_info_type**  meta_info_ref_ptr,
  void*                tx_cmd_info
);

#endif /* DSBCDB_API_H */
