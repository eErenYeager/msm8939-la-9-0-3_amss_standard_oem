/**
@file ds_sys.h
@brief
This file defines function, variables and data structures common to all PS
System API module
*/

#ifndef DS_SYS_H
#define DS_SYS_H

/*===========================================================================

                          D S  _ S Y S . H

DESCRIPTION

  Data Services System module interface file. This contains variables and
  definitions used by the various modules to access interface definitions.

Copyright (c) 2011 Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary.

===========================================================================*/


/*===========================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  Copyright (c) 2011-2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary.


  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/api/public/ds_sys.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when        who    what, where, why
--------    ---    ----------------------------------------------------------
09/06/11    sy     Created module

===========================================================================*/

#include "comdef.h"
#include "dserrno.h"

#ifdef __cplusplus
extern "C" {
#endif
/*===========================================================================

                         EXTERNAL DATA DECLARATIONS

===========================================================================*/
/**
  RAT Mask for 3GPP
 */
#define DS_SYS_RAT_3GPP_WCDMA                0x01
#define DS_SYS_RAT_3GPP_GPRS                 0x02
#define DS_SYS_RAT_3GPP_HSDPA                0x04
#define DS_SYS_RAT_3GPP_HSUPA                0x08
#define DS_SYS_RAT_3GPP_EDGE                 0x10
#define DS_SYS_RAT_3GPP_LTE                  0x20
#define DS_SYS_RAT_3GPP_HSDPAPLUS            0x40
#define DS_SYS_RAT_3GPP_DC_HSDPAPLUS         0x80
#define DS_SYS_RAT_3GPP_64_QAM               0x100
#define DS_SYS_RAT_3GPP_TDSCDMA              0x200
#define DS_SYS_RAT_3GPP_NULL_BEARER          0x8000

/**
  RAT Mask for 3GPP2
*/
#define DS_SYS_RAT_3GPP2_1X               0x01
#define DS_SYS_RAT_3GPP2_EVDO_REV0        0x02
#define DS_SYS_RAT_3GPP2_EVDO_REVA        0x04
#define DS_SYS_RAT_3GPP2_EVDO_REVB        0x08
#define DS_SYS_RAT_3GPP2_EHRPD            0x10
#define DS_SYS_RAT_3GPP2_FMC              0x20
#define DS_SYS_RAT_3GPP2_NULL_BEARER      0x8000

/**
  SO Mask for 1x
*/
#define DS_SYS_SO_3GPP2_1X_IS95                0x01
#define DS_SYS_SO_3GPP2_1X_IS2000              0x02
#define DS_SYS_SO_3GPP2_1X_IS2000_REL_A        0x04

/**
  SO Mask for DO

  The following table gives which of the following so_masks are supported
  by which technology

  Rev0 supports @li DS_SYS_SO_3GPP2_EVDO_DPA

  RevA supports @li DS_SYS_SO_3GPP2_EVDO_DPA
                @li DS_SYS_SO_3GPP2_EVDO_MFPA
                @li DS_SYS_SO_3GPP2_EVDO_EMPA
                @li DS_SYS_SO_3GPP2_EVDO_EMPA_EHRPD

  RevB supports @li DS_SYS_SO_3GPP2_EVDO_DPA
                @li DS_SYS_SO_3GPP2_EVDO_MFPA
                @li DS_SYS_SO_3GPP2_EVDO_EMPA
                @li DS_SYS_SO_3GPP2_EVDO_EMPA_EHRPD
                @li DS_SYS_SO_3GPP2_EVDO_MMPA
                @li DS_SYS_SO_3GPP2_EVDO_MMPA_EHRPD

*/
#define DS_SYS_SO_3GPP2_EVDO_DPA           0x01
#define DS_SYS_SO_3GPP2_EVDO_MFPA          0x02
#define DS_SYS_SO_3GPP2_EVDO_EMPA          0x04
#define DS_SYS_SO_3GPP2_EVDO_EMPA_EHRPD    0x08
#define DS_SYS_SO_3GPP2_EVDO_MMPA          0x10
#define DS_SYS_SO_3GPP2_EVDO_MMPA_EHRPD    0x20

/**
   Defines the MAX length for PDN/APN name string
*/
#define DS_SYS_MAX_APN_LEN        101
#define DS_SYS_MAX_APNS           15 /**< Max number of APNs */
#define DS_SYS_LTE_ATTACH_PDN_PROFILE_LIST_MAX  56 
/**< Max number of LTE Attach PDN profile IDs */


#define DS_SYS_MAX_NUM_THROUGHPUT_INFO        18
/**< max_thrpt_info = num_pdn * num_ifaces_per_pdn 
     Currently we have 8PDNs + 1 emergency PDN,(total 9PDNs)
     and max ifaces per PDN = 2 */

#define DS_SYS_MAX_BEARER         8

#define DS_SYS_MAX_AVAIL_SYS      15

#define DS_SYS_MAC_ADDR_LEN       6

#define DS_SYS_MAX_SSID_LEN       33

/**
  @brief Technology type values
*/
typedef enum
{
  DS_SYS_TECH_MIN     = 0x00,
  DS_SYS_TECH_3GPP    = DS_SYS_TECH_MIN, /**< Tech type 3gpp */
  DS_SYS_TECH_3GPP2   = 0x01,            /**< Tech type 3gpp2 */
  DS_SYS_TECH_WLAN    = 0x02,            /**< Tech type WLAN */
  DS_SYS_TECH_ALL     = 0x03,            /**< All the tech types */
  DS_SYS_TECH_MAX     = 0x04,
  DS_SYS_TECH_INVALID = 0xFF
} ds_sys_tech_enum_type;

/**
  @brief This is the type that is used to different network types
*/
typedef enum
{
  DS_SYS_NETWORK_3GPP,     /**< Network type 3gpp */
  DS_SYS_NETWORK_3GPP2,    /**< Network type 3gpp2 */
  DS_SYS_NETWORK_WLAN,     /**< Network type WLAN */
  DS_SYS_NETWORK_MAX,
} ds_sys_network_enum_type;

/**
  @brief This is the type that is used to define core network details
*/
typedef struct
{
  uint32   rat_mask;  /**< RAT Mask */
  uint32   so_mask;   /**< SO Mask */
} ds_sys_network_info_type;

/**
  @brief This is the type that is used to define system status. It is
         common to both configuration DS_SYS_CONF_SYSTEM_STATUS and event
         DS_SYS_EVENT_SYSTEM_STATUS_CHANGE

  @see   DS_SYS_CONF_SYSTEM_STATUS
  @see   DS_SYS_EVENT_SYSTEM_STATUS_CHANGE
*/
typedef struct
{
  ds_sys_network_enum_type         preferred_network; /**< pref network value*/
  ds_sys_network_info_type         network_info[DS_SYS_NETWORK_MAX];
  /**< Array of rat and SO mask for all networks */
} ds_sys_system_status_type;


/**
  @brief This is the type that is used for 
         DS_SYS_CONF_GET_WLAN_MAC_ADDR configuration and
         DS_SYS_EVENT_WLAN_MAC_ADDR_CHANGE
   
  @see DS_SYS_CONF_GET_WLAN_MAC_ADDR
  @see DS_SYS_EVENT_WLAN_MAC_ADDR_CHANGE 
*/
typedef struct
{
  uint8            ap_mac_addr[DS_SYS_MAC_ADDR_LEN]; 
  /**< WLAN Access Point MAC addr*/
} ds_sys_wlan_mac_addr_type;

/**
  @brief This is the type that is used for 
         DS_SYS_CONF_GET_WLAN_SSID configuration and
         DS_SYS_EVENT_WLAN_SSID_CHANGE
   
  @see DS_SYS_CONF_GET_WLAN_SSID
  @see DS_SYS_EVENT_WLAN_SSID_CHANGE 
*/
typedef struct
{
  char            ssid[DS_SYS_MAX_SSID_LEN]; 
  /**< WLAN Service Set Identifier*/
} ds_sys_wlan_ssid_type;

/**
  @brief Data type used in ds_sys_3gpp2_page_monitor_type
  @see   ds_sys_3gpp2_page_monitor_type
*/
typedef uint8 ds_sys_3gpp2_page_monitor_period_type;

/**
  @brief Data type to be used with get operation for
         DS_SYS_CONF_3GPP2_PAGE_MONITOR_PERIOD and
         DS_SYS_EVENT_3GPP2_EVDO_PAGE_MONITOR_PERIOD_CHANGE event

  @see   DS_SYS_CONF_3GPP2_PAGE_MONITOR_PERIOD
  @see   DS_SYS_EVENT_3GPP2_EVDO_PAGE_MONITOR_PERIOD_CHANGE
*/
typedef struct
{
  ds_sys_3gpp2_page_monitor_period_type    page_monitor_period;
  boolean                                  force_long_sleep;
} ds_sys_3gpp2_page_monitor_type;


/**
  @brief This is the type that is used to  define different RATs types
*/
typedef enum
{
  DS_SYS_RAT_EX_NULL_BEARER           = 0x0,
  /* 3GPP RAT Values */
  DS_SYS_RAT_EX_3GPP_WCDMA             = 1,     
  DS_SYS_RAT_EX_3GPP_GERAN             = 2,
  DS_SYS_RAT_EX_3GPP_LTE               = 3,
  DS_SYS_RAT_EX_3GPP_TDSCDMA           = 4,
  DS_SYS_RAT_EX_3GPP_WLAN              = 5,
  DS_SYS_RAT_EX_3GPP_MAX               = 100,

  /* 3GPP2 RAT Values */
  DS_SYS_RAT_EX_3GPP2_1X               = 101,
  DS_SYS_RAT_EX_3GPP2_HRPD             = 102,
  DS_SYS_RAT_EX_3GPP2_EHRPD            = 103,
  DS_SYS_RAT_EX_3GPP2_WLAN             = 104,
  DS_SYS_RAT_EX_3GPP2_MAX              = 200, 

  /* WLAN RAT Values */
  DS_SYS_RAT_EX_WLAN                   = 201,
  DS_SYS_RAT_EX_WLAN_MAX               = 300,
 
  DS_SYS_RAT_MAX,
} ds_sys_rat_ex_enum_type;

/* SO Mask has a size of 8 bytes */

/* SO Mask not available or not specified */
#define DS_SYS_SO_EX_UNSPECIFIED                    0x0

/* 3GPP SO Mask, it occupies the first 3 bytes. ie. bytes 0, 1 and 2 */
#define DS_SYS_SO_EX_3GPP_WCDMA                     0x01
#define DS_SYS_SO_EX_3GPP_HSDPA                     0x02
#define DS_SYS_SO_EX_3GPP_HSUPA                     0x04
#define DS_SYS_SO_EX_3GPP_HSDPAPLUS                 0x08
#define DS_SYS_SO_EX_3GPP_DC_HSDPAPLUS              0x10
#define DS_SYS_SO_EX_3GPP_64_QAM                    0x20
#define DS_SYS_SO_EX_3GPP_HSPA                      0x40
#define DS_SYS_SO_EX_3GPP_GPRS                      0x80
#define DS_SYS_SO_EX_3GPP_EDGE                      0x100

/* GSM so mask is depreciated. Can only be GPRS or EDGE */
#define DS_SYS_SO_EX_3GPP_GSM                       0x200

#define DS_SYS_SO_EX_3GPP_S2B                       0x400
#define DS_SYS_SO_EX_3GPP_LTE_LIMITED_SRVC          0x800
#define DS_SYS_SO_EX_3GPP_LTE_FDD                   0x1000
#define DS_SYS_SO_EX_3GPP_LTE_TDD                   0x2000
#define DS_SYS_SO_EX_3GPP_TDSCDMA                   0x4000
#define DS_SYS_SO_EX_3GPP_DC_HSUPA                  0x8000

/* 3GPP2 SO Mask, it occupies bytes 3, 4 and 5 */
#define DS_SYS_SO_EX_3GPP2_1X_IS95                  (uint64)0x01000000ull
#define DS_SYS_SO_EX_3GPP2_1X_IS2000                (uint64)0x02000000ull
#define DS_SYS_SO_EX_3GPP2_1X_IS2000_REL_A          (uint64)0x04000000ull
#define DS_SYS_SO_EX_3GPP2_HDR_REV0_DPA             (uint64)0x08000000ull
#define DS_SYS_SO_EX_3GPP2_HDR_REVA_DPA             (uint64)0x10000000ull
#define DS_SYS_SO_EX_3GPP2_HDR_REVB_DPA             (uint64)0x20000000ull
#define DS_SYS_SO_EX_3GPP2_HDR_REVA_MPA             (uint64)0x40000000ull
#define DS_SYS_SO_EX_3GPP2_HDR_REVB_MPA             (uint64)0x80000000ull
#define DS_SYS_SO_EX_3GPP2_HDR_REVA_EMPA            (uint64)0x100000000ull
#define DS_SYS_SO_EX_3GPP2_HDR_REVB_EMPA            (uint64)0x200000000ull
#define DS_SYS_SO_EX_3GPP2_HDR_REVB_MMPA            (uint64)0x400000000ull
#define DS_SYS_SO_EX_3GPP2_EVDO_FMC                 (uint64)0x800000000ull

/* Bytes 6 and 7 of SO Mask are reserved for future use */


/**
  @brief This is the type that is used to  define different subscription types
         Unknown subs id is used when the client does not care/provide the subs id
*/
typedef enum 
{
  DS_SYS_DEFAULT_SUBS      = 0x0000, /** < Default Data Subscription */
  DS_SYS_PRIMARY_SUBS      = 0x0001, /**< Primary */
  DS_SYS_SECONDARY_SUBS    = 0x0002, /**< Secondary */
  DS_SYS_TERTIARY_SUBS     = 0x0003,  /**< Tertiary */
  DS_SYS_SUBS_MAX,
} ds_sys_subscription_enum_type;

/**
  @brief This is the tuple that defines network, RAT, SO and service status
*/
typedef struct 
{
  ds_sys_network_enum_type                 technology; /**< technology type*/
  ds_sys_rat_ex_enum_type                  rat_value;  /**< rat type*/
  uint64                                   so_mask;   /**< so type*/
} ds_sys_system_status_info_type;

/**
  @brief  This is the type that defines apn to preferred network relationship. 
          For every APN, there would be tuple that describes its preferred 
          system. 
*/
typedef struct 
{
   char                                apn_name[DS_SYS_MAX_APN_LEN]; /**< PDN name*/
   uint32                              num_avail_sys; 
  /**< Number of valid entries to available system status array */
   ds_sys_system_status_info_type      avail_sys[DS_SYS_MAX_AVAIL_SYS];
  /**<  Array of available systems for the APN. The first element of the 
        array will contain the preferred system for the APN.
        @see ds_sys_system_status_info_type 
        @see ds_sys_system_status_info_type */
} ds_sys_apn_pref_sys_info_type;

/**
  @brief This is the type that is used to define system status. It is
         common to both configuration DS_SYS_CONF_SYSTEM_STATUS_EX and event
         DS_SYS_EVENT_SYSTEM_STATUS_EX
*/
typedef struct 
{
  uint32   num_avail_sys; 
  /**< Number of valid entries to available system status array */

  ds_sys_system_status_info_type  avail_sys[DS_SYS_MAX_AVAIL_SYS]; 
  /**< Array of current system status, the first element in the array will 
       contain the preferred system */

  uint32   num_avail_apns;
  /**< Number of apn entries to pdn_info */

  ds_sys_apn_pref_sys_info_type apn_pref_sys_info[DS_SYS_MAX_APNS];    
  /**< Array of all apns available */
} ds_sys_system_status_ex_type;


/**
  @brief Enum type describing APN Preferred system.
  @see   ds_sys_conf_apn_pref_sys_type
*/
typedef enum
{
  DS_SYS_APN_PREF_SYS_WWAN  = 0,
  DS_SYS_APN_PREF_SYS_WLAN  = 1,
  DS_SYS_APN_PREF_SYS_IWLAN = 2
} ds_sys_apn_pref_sys_enum_type;

/**
  @brief Data type to be used with DS_SYS_CONF_APN_PREF_SYS and 
         DS_SYS_EVENT_APN_PREF_SYS_CHANGE. It describes preferred 
         system for apn.         
*/
typedef struct
{
  unsigned char                  apn[DS_SYS_MAX_APN_LEN];
  ds_sys_apn_pref_sys_enum_type  pref_sys;
} ds_sys_apn_pref_sys_type;

/**
  @brief Data type to be used with attach pdn list. 
   
  @see   DS_SYS_IOCTL_3GPP_SET_LTE_ATTACH_PDN_LIST 
  @see   DS_SYS_IOCTL_3GPP_GET_LTE_ATTACH_PDN_LIST 
  @see   DS_SYS_EVENT_3GPP_LTE_ATTACH_PDN_LIST_CHANGE  
*/
typedef struct
{
  uint8  attach_pdn_profile_list_num;   /**< Num of attach PDN in the array */
  uint16 attach_pdn_profile_list[DS_SYS_LTE_ATTACH_PDN_PROFILE_LIST_MAX]; 
    /**< Array of attach PDN profile IDs*/
} ds_sys_3gpp_lte_attach_pdn_list_type;

//Ip addr type enum
typedef enum
{
  DS_SYS_IP_TYPE_V4 = 0x00,
  DS_SYS_IP_TYPE_V6 = 0x01
} ds_sys_ip_addr_enum_type;

// Throughput quality enum
typedef enum {
  DS_SYS_HIGHER_THROUGHPUT_UNKNOWN,
  /**< Throughput quality is unknown \n */
  DS_SYS_HIGHER_THROUGHPUT_NOT_POSSIBLE,
  /**< Best throughput possible \n */
  DS_SYS_HIGHER_THROUGHPUT_POSSIBLE
  /**< Better throughput than current throughput is possible \n */
} ds_sys_throughput_quality_enum_type;

/**
  @brief Data type to be used to represent per bearer
         throughput info
*/
typedef struct
{
  uint32 bearer_rlp_mac_id;
  /**< Bearer ID representing the Bearer
       for which the throughput is being 
       reported in case of 3gpp tech
  */

  uint32 bearer_uplink_actual_rate;
  /**<  Uplink actual rate in bits per second
        corresponding to the bearer id 
  */

  uint32 bearer_uplink_queue_size;
  /**< Number of bytes pending in uplink queue
       corresponding to the bearer id 
  */
  
  boolean is_default_bearer;
  /**< Boolean to determine if the bearer
       is a default one 
  */ 
} ds_sys_bearer_throughput_info_type;

typedef struct
{
  uint8 num_bearers; 
  /**< Num of bearer trpt info in the array */

  ds_sys_bearer_throughput_info_type bearer_trpt_info[DS_SYS_MAX_BEARER];
  /**< Array of per bearer throughput info*/
} ds_sys_bearer_throughput_info_list_type;

/**
  @brief Data type to be used with throughput info timer 
   
  @see   DS_SYS_CONF_THROUGHPUT_INFO
  @see   DS_SYS_EVENT_THROUGHPUT_INFO  
*/
typedef struct
{
  char apn_string[DS_SYS_MAX_APN_LEN];
  /**< String representing the APN.
       Maximum length is 100 bytes.
  */

  ds_sys_ip_addr_enum_type ip_type;
  /**< Values: \n
       - 4 -- IPv4 \n
       - 6 -- IPv6 
  */

  ds_sys_tech_enum_type  tech_type;
  /**< Technology type */

  ds_sys_subscription_enum_type subscription;
  /**< Subscription that APN is bound to.
  */

  uint32 uplink_actual_rate;
  /**<  Uplink actual rate(kbps)
  */

  uint32 uplink_allowed_rate;
  /**<  Uplink allowed rate(kbps)
  */

  uint32 uplink_queue_size;
  /**< Number of bytes pending in uplink queue
  */

  ds_sys_throughput_quality_enum_type throughput_quality;
  /**< Indicates whether UE can have better throughput 
       rate than the current throughput reporting
  */  

  void *iface_ptr;
  /**< ptr to physical interface on which to 
       operate on.(Um interface representing the APN)
  */  

  ds_sys_bearer_throughput_info_list_type bearer_throughput_info_list;
  /**< list of per bearer throughput information
  */

} ds_sys_throughput_info_type;

typedef struct
{
   uint8 num_trpt_info; 
   /**< Num of trpt info in the list */

   ds_sys_throughput_info_type trpt_info[1];
   /**< Dynamic throughtput info array */
} ds_sys_throughput_info_list_type;

/**
  @brief Data type to be used with DS_SYS_CONF_3GPP_EMERGENCY_PDN_STATUS and 
         DS_SYS_EVENT_3GPP_EMERGENCY_PDN_STATUS. 
         TRUE : UE is attached to emergency PDN only.
         FALSE: UE is attached to other PDN(could be emergency pdn + 
                non-emergency pdn or non-emergency pdn)       
*/
typedef struct
{
  boolean emergency_pdn_only;
} ds_sys_is_pdn_only_emergency_type;

/**
  @brief Data type to store the apn name 
   
*/
typedef struct
{
  unsigned char     apn_name[DS_SYS_MAX_APN_LEN];
} ds_sys_apn_name_type;

/**
  @brief Data type to be used with roaming disallowed info
  is_roaming_disallowed : whether roaming is allowed or disallowed
                          on the apns
  num_apn_names         : No of apn names listed
  apn_list              : List of apn names 
   
  @see   DS_SYS_CONF_3GPP_ROAMING_DISALLOWED_INFO
  @see   DS_SYS_EVENT_3GPP_ROAMING_DISALLOWED_INFO  
*/
typedef struct
{
  boolean                 is_roaming_disallowed;
  uint8                   num_apn_names;
  ds_sys_apn_name_type    apn_list[DS_SYS_MAX_APNS];
} ds_sys_roaming_disallowed_info_type;

/**
  @brief Data type to be used with network aware status
   
  @see   DS_SYS_CONF_3GPP2_QOS_NETWORK_STATUS
  @see   DS_SYS_EVENT_3GPP2_QOS_NETWORK_STATUS_CHANGE  
*/
typedef boolean ds_sys_3gpp2_network_qos_aware_status;

/**
  @brief Data type to be used with network aware status
   
  @see   DS_SYS_IOCTL_3GPP2_GET_PAGE_MONITOR_PERIOD
  @see   DS_SYS_EVENT_3GPP2_PAGE_MONITOR_PERIOD_CHANGE
 */
typedef struct
{
  ds_sys_3gpp2_page_monitor_period_type       slotted_mode_option;
  boolean     long_sleep_enabled;
} ds_sys_3gpp2_page_monitor_period_info_type;

/*===========================================================================

                           EXTERNALIZED FUNCTIONS

===========================================================================*/
/**
  @brief   This function initializes the DS Sys Conf library.

  @param   None

  @return  DSS_SUCCESS          On Success.
  @return  DSS_ERROR            On failure.
*/
int16 ds_sys_init_lib
(
  void
);

#ifdef __cplusplus
}
#endif

#endif /* DS_SYS_H */
