#ifndef DS_PROFILE_H
#define DS_PROFILE_H

/** 
  @file ds_profile.h 
  @brief 
   Data Services Profile Registry API definitions 
*/
/* This file contains common, external header file definitions for Data 
   Services Profile Registry API. This library is thread-safe. 
*/

/*=============================================================================
NOTE: The @brief description and any detailed descriptions above do not appear 
      in the PDF. 

      The dms_mainpage.dox file contains all file/group descriptions 
      that are in the output PDF generated using Doxygen and Latex. To edit or 
      update any of the file/group text in the PDF, edit the 
      dms_mainpage.dox file or contact Tech Pubs.

      The above description for this file is part of the dsProfile group 
      description in the dms_mainpage.dox file. 
=============================================================================*/
/*=============================================================================
Copyright (c) 2009-2012 Qualcomm Technologies Incorporated.
All rights reserved.
Qualcomm Confidential and Proprietary
=============================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/datamodem/api/public/ds_profile.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
02/14/12   gs      Added declaration of ds_profile_close_lib()
08/28/11   sd     (Tech Pubs) Applied new Doxygen grouping markup.
05/23/11   ack     Added 3GPP2 Out of profiles error result 
03/02/11   mg      doxygen comments
02/11/11   ttv     Added changes for consolidated profile family.
01/14/11   ss      Added support for Get/Set Iface linger parameters.
09/30/09   mg      Created the module. First version of the file.
===========================================================================*/
/* group: dsprofile */
#include "comdef.h"
#include "dsumtspdpreg.h"
/** @addtogroup constants 
  @{
*/
/** Data services profile version. The first two digits indicate the major version. The 
  last two digits are the minor version.
*/
#define DS_PROFILE_VERSION      1101

/** Maximum number of handles supported.
*/
#define DS_PROFILE_MAX_NUM_HNDL 32

/** Data services embedded profile family.
*/
#define DS_PROFILE_EMBEDDED_PROFILE_FAMILY  0

/** Data services tethered profile family.
*/
#define DS_PROFILE_TETHERED_PROFILE_FAMILY  1

/** Unspecified profile number
*/
#define DS_PROFILE_UNSPECIFIED_NUM  ((ds_profile_num_type)(-1))

/** Always share profile number
*/
#define DS_PROFILE_ALWAYS_SHARE_PROFILE_NUM  ((ds_profile_num_type)(0xFE))
/** @} */ /* end_addtogroup constants */

/** @addtogroup datatypes
  @{
*/
/** Profile parameter identifier type.
*/
typedef uint32  ds_profile_identifier_type;    

/** Profile number.
*/
typedef uint16  ds_profile_num_type;           

/** Profile handle.
*/
typedef void*   ds_profile_hndl_type;          

/** Iterator used in list functions.
*/
typedef void*   ds_profile_itr_type; 

/** Status return values from functions.
*/ 
typedef enum   
{
  DS_PROFILE_REG_RESULT_SUCCESS    =       0,  
    /**< Successful operation. */
  DS_PROFILE_REG_RESULT_FAIL,                  
    /**< General failure in the library. */
  
  DS_PROFILE_REG_RESULT_ERR_INVAL_HNDL,        
    /**< Invalid profile handle. */ 
  DS_PROFILE_REG_RESULT_ERR_INVAL_OP,          
    /**< Operation not supported. */
                                                 
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE,
    /**< Invalid technology type. */
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_NUM, 
    /**< Invalid profile number. */
  DS_PROFILE_REG_RESULT_ERR_INVAL_IDENT,       
    /**< Invalid identifier. */
  DS_PROFILE_REG_RESULT_ERR_INVAL,             
    /**< Other invalid argument. */
                                                 
  DS_PROFILE_REG_RESULT_ERR_LIB_NOT_INITED,    
    /**< Library has not been initialized. */
                                                 
  DS_PROFILE_REG_RESULT_ERR_LEN_INVALID,       
    /**< Buffer size length is invalid.\n
         For get_param, the buffer size cannot be less than the maximum.\n
         For set_param, the buffer size cannot be greater than the maximum. */
  
  DS_PROFILE_REG_RESULT_LIST_END,              
    /**< End of the list has been reached. This status is the return value for 
         _itr_next. */
  
  DS_PROFILE_REG_RESULT_ERR_INVAL_SUBS_ID,     
    /**< Subscription ID is invalid. */
  DS_PROFILE_REG_INVAL_PROFILE_FAMILY,         
    /**< Invalid profile family. */
  DS_PROFILE_REG_PROFILE_VERSION_MISMATCH,
    /**< Profile Version Mismatch */
  DS_PROFILE_REG_NO_EMERGENCY_PDN_SUPPORT,
    /**< Emergency PDN Feature disabled */
  DS_PROFILE_REG_3GPP_SPEC_MIN = 0x1000,     
    /**< Offset for UMTS technology errors. */
  DS_PROFILE_REG_3GPP_INVAL_PROFILE_FAMILY,  
    /**< Invalid profile family. */
  DS_PROFILE_REG_3GPP_ACCESS_ERR,            
    /**< Error accessing the embedded file system. */
  DS_PROFILE_REG_3GPP_CONTEXT_NOT_DEFINED,   
    /**< Profile context is not defined. */
  DS_PROFILE_REG_3GPP_VALID_FLAG_NOT_SET,    
    /**< Profile valid flag is not set.  */
  DS_PROFILE_REG_3GPP_READ_ONLY_FLAG_SET,    
    /**< Read only flag is set in the profile. */
  DS_PROFILE_REG_3GPP_ERR_OUT_OF_PROFILES,   
    /**< No profiles are available while creating a new profile. */
  DS_PROFILE_REG_3GPP_SPEC_MAX = 0x10FF,     
    /**< Maximum for UMTS technology errors. */
  
  DS_PROFILE_REG_3GPP2_SPEC_MIN = 0x1100,    
    /**< Offset for CDMA technology errors. */
  DS_PROFILE_REG_3GPP2_ERR_INVALID_IDENT_FOR_PROFILE, 
    /**< Identifier is not valid for the profile. */
  DS_PROFILE_REG_3GPP2_ERR_OUT_OF_PROFILES,
    /**< Error indicating out of 3GPP2 profiles. */
  DS_PROFILE_REG_3GPP2_SPEC_MAX = 0x11FF,    
    /**< Maximum for CDMA technology errors. */
  /** @cond
  */
  DS_PROFILE_REG_RESULT_MAX    = 0xFFFF
    /**< Maximum value for return type (used internally). */
  /** @endcond */
} ds_profile_status_etype;

/** Profile technology type values.
*/
typedef enum
{
  /** @cond
  */
  DS_PROFILE_TECH_MIN     = 0x00,
    /**< Minimum value for the profile technology type (used internally). */
  /** @endcond */

  DS_PROFILE_TECH_3GPP    = DS_PROFILE_TECH_MIN,
    /**< 3GPP technology type. */
  DS_PROFILE_TECH_3GPP2   = 0x01,
    /**< 3GPP2 technology type. */
  /** @cond
  */
  DS_PROFILE_TECH_MAX     = 0x02,
    /**< Maximum value for the profile technology type (used internally). */
  /** @endcond */

  DS_PROFILE_TECH_INVALID = 0xFF
    /**< Invalid technology type. */
}ds_profile_tech_etype;

/** Transaction type values.
*/ 
typedef enum
{
  DS_PROFILE_TRN_READ   = 0x01,
    /**< Transaction type is read. */
  DS_PROFILE_TRN_RW     = 0x03,
    /**< Transaction type is write. */
  /** @cond
  */
  DS_PROFILE_TRN_VALID  = DS_PROFILE_TRN_READ | DS_PROFILE_TRN_RW
  /** @endcond */
}ds_profile_trn_etype;

/** Action values for ds_profile_end_transaction().
*/
typedef enum 
{
  /** @cond
  */
  DS_PROFILE_ACTION_MIN    = 0x0,
    /**< Minimum value for the action type (used internally). */
  /** @endcond */

  DS_PROFILE_ACTION_COMMIT = 0x1,
    /**< Commit action type. */
  DS_PROFILE_ACTION_CANCEL = 0x2,
    /**< Cancel action type. */
  /** @cond
  */
  DS_PROFILE_ACTION_MAX    = 0xFF
    /**< Maximum value for the action type (used internally). */
  /** @endcond */
}ds_profile_action_etype;

/** Profile list configuration.
*/
typedef enum
{
  /** @cond
  */
  DS_PROFILE_LIST_DFN_MIN           = 0,
    /**< Minimum value for the list definition type (used internally). */
  /** @endcond */

  DS_PROFILE_LIST_ALL_PROFILES      = 1,
    /**< Profile list includes all profiles. */
  DS_PROFILE_LIST_SEARCH_PROFILES   = 2,
    /**< Profile list is based on key words or values. */
  /** @cond
  */
  DS_PROFILE_LIST_DFN_MAX           = 0xFF
    /**< Maximum value for the list definition type (used internally). */
  /** @endcond */
} ds_profile_list_dfn_etype;

/** Dual SIM subscription information.
*/
typedef enum 
{
  DS_PROFILE_ACTIVE_SUBSCRIPTION_NONE = 0x00,
    /**< No active subscription. */
  DS_PROFILE_ACTIVE_SUBSCRIPTION_1,
    /**< Active subscription 1. */
  DS_PROFILE_ACTIVE_SUBSCRIPTION_2,
    /**< Active subscription 2. */
  DS_PROFILE_ACTIVE_SUBSCRIPTION_3,
    /**< Active subscription 3. */
  DS_PROFILE_ACTIVE_SUBSCRIPTION_MAX = DS_SUBSCRIPTION_MAX + 1
    /**< Maximum active subscription. */
} ds_profile_subs_etype;

/** @brief Stores the parameter value for get/set operations.
*/
typedef struct 
{
  void     *buf;  /**< Buffer to store the parameter value. */
  uint16   len;   /**< Buffer length. */
} ds_profile_info_type;

/** @brief Stores the node for the profile list. For name, this structure 
  allocates memory as specified by the profile_name_max_len parameter in the
  corresponding technology header.
*/
typedef struct
{
  ds_profile_num_type   num;
    /**< Profile number. */
  ds_profile_info_type *name;
    /**< Profile name. */
} ds_profile_list_info_type;

/** @brief Identifies the type of list to be returned, depending on the DFN.
*/
typedef struct
{
  ds_profile_list_dfn_etype  dfn;
    /**< List definition type. */
  ds_profile_identifier_type ident;
    /**< Profile parameter identifier. Not used for 
         #DS_PROFILE_LIST_ALL_PROFILES. */
  ds_profile_info_type       info;
    /**< Profile parameter value.  Not used for 
         #DS_PROFILE_LIST_ALL_PROFILES. */
} ds_profile_list_type;

/** @brief Linger parameters.
*/
typedef PACKED struct PACKED_POST
{
  boolean allow_linger_flag;   /**< Flag to indicate whether lingering is 
                                    supported for this profile. */
  uint16  linger_timeout_val;  /**< Linger timeout value in seconds. */
} ds_profile_linger_params_type;

/** Profile configuration definitions.
*/
typedef enum
{
  DS_PROFILE_CONFIG_MASK_NONE        = 0x00, /**< No mask. */
  DS_PROFILE_CONFIG_MASK_PERSISTENCE = 0x01, /**< Persistence mask. */
  DS_PROFILE_CONFIG_MASK_SUBS_ID     = 0x02  /**< Subscription ID mask. */
} ds_profile_config_mask_etype;

/** Profile configuration mask type.
*/
typedef uint32  ds_profile_config_mask_type;

/** @brief Profile configuration mask. */
typedef struct
{
  ds_profile_config_mask_type  config_mask;  
    /**< Configuration mask. */
  boolean                      is_persistent;
    /**< Boolean indicating the profile configuration is persistent. */
  ds_profile_subs_etype       subs_id;
    /**< Subscription ID for the profile. */
} ds_profile_config_type;
/** @} */ /* end_addtogroup datatypes */

/*---------------------------------------------------------------------------
                       PUBLIC ROUTINES
---------------------------------------------------------------------------*/
/*===========================================================================
FUNCTION DS_PROFILE_INIT_LIB 
============================================================================*/
/** @ingroup ds_profile_init_lib
  Initializes the DS profile library. On the modem, this function is 
  called only once at initialization. This function initializes the library for
  that process domain.

  @return
  DS_PROFILE_REG_RESULT_SUCCESS -- Operation successful.\n
  DS_PROFILE_REG_RESULT_FAIL    -- General error. 

  @dependencies 
  This function is called only once during startup.
  @newpage
*/
ds_profile_status_etype ds_profile_init_lib( void );

/*===========================================================================
FUNCTION DS_PROFILE_CLOSE_LIB 
============================================================================*/
/** @ingroup ds_profile_close_lib
  Closes the DS profile library and releases any associated resources. 

  @return
  DS_PROFILE_REG_RESULT_SUCCESS -- Operation successful.\n
  DS_PROFILE_REG_RESULT_FAIL    -- General error. 

  @dependencies 
  @newpage
*/
ds_profile_status_etype ds_profile_close_lib( void );

/*===========================================================================
FUNCTION DS_PROFILE_BEGIN_TRANSACTION 
============================================================================*/
/** @ingroup ds_profile_begin_transaction 
  Returns a handle that the clients of this software library can use for 
  subsequent profile operations. The handle returned is of the requested 
  transaction type.
 
  @datatypes
  #ds_profile_trn_etype\n
  #ds_profile_tech_etype\n
  #ds_profile_num_type\n
  #ds_profile_hndl_type

  @vertspace
  @param[in] trn     Requested transaction type.
  @param[in] tech    Technology type.
  @param[in] num     Profile number.
  @param[out] hndl   Pointer to the return requested handle.

  @return
  DS_PROFILE_REG_RESULT_SUCCESS                -- Operation succeeded.\n
  DS_PROFILE_REG_RESULT_ERR_LIB_NOT_INITED     -- Library is not initialized.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE -- Invalid technology type.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_NUM  -- Invalid profile number.\n 
  DS_PROFILE_REG_RESULT_FAIL                   -- General failure. 

  @dependencies 
  The software library must be initialized.\n
  All profile operations using this handle must call 
  ds_profile_end_transaction() at the end of the operation.
  @newpage
*/
ds_profile_status_etype ds_profile_begin_transaction (
  ds_profile_trn_etype   trn,
  ds_profile_tech_etype  tech,
  ds_profile_num_type    num,
  ds_profile_hndl_type  *hndl
);

/*===========================================================================
FUNCTION DS_PROFILE_BEGIN_TRANSACTION_PER_SUB 
============================================================================*/
/** @ingroup ds_profile_begin_transaction_per_sub 
  Returns a handle that the clients of this software library can use for 
  subsequent profile operations. The handle returned is of the requested 
  transaction type.
 
  @datatypes
  #ds_profile_trn_etype\n
  #ds_profile_tech_etype\n
  #ds_profile_num_type\n
  #ds_profile_hndl_type

  @vertspace
  @param[in] trn     Requested transaction type.
  @param[in] tech    Technology.
  @param[in] num     Profile number.
  @param[out] hndl   Pointer to the return requested handle.

  @return
  DS_PROFILE_REG_RESULT_SUCCESS                -- Operation succeeded.\n
  DS_PROFILE_REG_RESULT_ERR_LIB_NOT_INITED     -- Library is not initialized.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE -- Invalid technology type.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_NUM  -- Invalid profile number.\n 
  DS_PROFILE_REG_RESULT_FAIL                   -- General failure. 

  @dependencies 
  The software library must be initialized.\n
  All profile operations using this handle must call 
  ds_profile_end_transaction() at the end of the operation.
  @newpage
*/
ds_profile_status_etype ds_profile_begin_transaction_per_sub (
  ds_profile_trn_etype   trn,
  ds_profile_tech_etype  tech,
  ds_profile_num_type    num,
  ds_profile_subs_etype  subs_id,
  ds_profile_hndl_type  *hndl 
);

/*===========================================================================
FUNCTION DS_PROFILE_END_TRANSACTION 
============================================================================*/
/** @ingroup ds_profile_end_transaction
  Commits the prefetched, modified profile to the persistent storage on the 
  modem. This function also invokes cleanup routines for the profile
  handle specified. On return, the handle becomes unusable.
 
  @datatypes
  #ds_profile_hndl_type\n
  #ds_profile_action_etype

  @vertspace
  @param[in] hndl  Profile handle.
  @param[in] act   Commits the profile to persistent storage and invokes the 
                   clean up routines, or cancels the actions. 
 
  @return
  DS_PROFILE_REG_RESULT_SUCCESS             -- Operation succeeded.\n
  DS_PROFILE_REG_RESULT_ERR_LIB_NOT_INITED  -- Library is not initialized.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_HNDL      -- Invalid handle.\n
  DS_PROFILE_REG_RESULT_FAIL                -- General failure. 

  @dependencies 
  The software library must be initialized.
  @newpage
*/
ds_profile_status_etype ds_profile_end_transaction (
  ds_profile_hndl_type    hndl,
  ds_profile_action_etype act
);

/*===========================================================================
FUNCTION DS_PROFILE_CREATE 
============================================================================*/
/** @ingroup ds_profile_create
  Returns a profile number from a pool of free profiles. 

  @note1hang Not all technology types support this operation.
 
  @datatypes
  #ds_profile_tech_etype\n
  #ds_profile_num_type

  @vertspace
  @param[in] tech   Technology type for which a profile needs to be created.
  @param[out] num   Pointer to the profile number to be created.
 
  @return
  DS_PROFILE_REG_RESULT_SUCCESS                -- Operation succeeded.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE -- Invalid technology type.\n
  DS_PROFILE_REG_RESULT_ERR_LIB_NOT_INITED     -- Library is not initialized.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_OP           -- Operation is not supported  
                                                  for the technology type.\n
  DS_PROFILE_REG_RESULT_FAIL                   -- General failure. 

  @dependencies 
  The software library must be initialized.
  @newpage
*/
ds_profile_status_etype ds_profile_create (
  ds_profile_tech_etype  tech,
  ds_profile_num_type   *num
);

/*===========================================================================
FUNCTION DS_PROFILE_CREATE_EX
=============================================================================*/
/** @ingroup ds_profile_create_ex
  Returns a profile number from a pool of free profiles. 

  @note1hang Not all technology types support this operation.

  @datatypes
  #ds_profile_tech_etype\n
  #ds_profile_config_type\n
  #ds_profile_num_type

  @vertspace
  @param[in] tech       Technology type.
  @param[in] config_ptr Pointer to the configuration for creating profiles.
  @param[in] num        Pointer to the profile number of the profile created.

  @return
  DS_PROFILE_REG_RESULT_SUCCESS                -- Operation was successful.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE -- Invalid technology type.\n
  DS_PROFILE_REG_RESULT_ERR_LIB_NOT_INITED     -- Library has not been 
                                                  initialized.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_OP           -- Operation is not supported
                                                  for the technology type.\n
  DS_PROFILE_REG_RESULT_FAIL                   -- General error. 
  
  @dependencies
  None.
  @newpage
*/
ds_profile_status_etype ds_profile_create_ex
(
  ds_profile_tech_etype     tech,
  ds_profile_config_type *  config_ptr,
  ds_profile_num_type *     num
);

/*===========================================================================
FUNCTION DS_PROFILE_DELETE 
============================================================================*/
/** @ingroup ds_profile_delete
  Resets a profile to undefined and return to free pool. 

  @note1hang Not all technology types support this operation.
 
  @datatypes
  #ds_profile_tech_etype\n
  #ds_profile_num_type

  @vertspace
  @param[in] tech  Profile technology type.
  @param[in] num   Profile number to be deleted.
 
  @return
  DS_PROFILE_REG_RESULT_SUCCESS                -- Operation succeeded.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE -- Invalid technology type.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_NUM  -- Invalid profile number.\n
  DS_PROFILE_REG_RESULT_ERR_LIB_NOT_INITED     -- Library is not initialized.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_OP           -- Operation is not supported  
                                                  for the technology type.\n
  DS_PROFILE_REG_RESULT_FAIL                   -- General failure. 

  @dependencies 
  The software library must be initialized.
  @newpage
*/
ds_profile_status_etype ds_profile_delete ( 
  ds_profile_tech_etype  tech,
  ds_profile_num_type    num
);


/*===========================================================================
FUNCTION DS_PROFILE_DELETE_PER_SUB 
============================================================================*/
/** @ingroup ds_profile_delete_per_sub
  Resets a profile to undefined and return to free pool. 
  Not allowed for default profiles

  @note1hang Not all technology types support this operation.
 
  @datatypes
  #ds_profile_tech_etype\n
  #ds_profile_num_type

  @vertspace
  @param[in] tech  Technology.
  @param[in] num   Profile number to be deleted.
 
  @return
  DS_PROFILE_REG_RESULT_SUCCESS                -- Operation succeeded.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE -- Invalid technology type.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_NUM  -- Invalid profile number.\n
  DS_PROFILE_REG_RESULT_ERR_LIB_NOT_INITED     -- Library is not initialized.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_OP           -- Operation is not supported  
                                                  for the technology type.\n
  DS_PROFILE_REG_RESULT_FAIL                   -- General failure. 

  @dependencies 
  The software library must be initialized.
  @newpage
*/
ds_profile_status_etype ds_profile_delete_per_sub ( 
  ds_profile_tech_etype  tech,
  ds_profile_num_type    num,
  ds_profile_subs_etype  subs_id
);

/*===========================================================================
FUNCTION DS_PROFILE_GET_PARAM 
============================================================================*/
/** @ingroup ds_profile_get_param
  Gets the profile data element identified by the identifier. The identifiers 
  are specified in the corresponding technology header file. The data elements 
  are read from the prefetched profile, and information is returned with that 
  value and length.
 
  @datatypes
  #ds_profile_hndl_type\n
  #ds_profile_identifier_type\n
  ds_profile_info_type

  @vertspace
  @param[in] profile_hndl  Handle to the profile to get the profile data 
                           element.
  @param[in] identifier    Identifier of the profile data element.
  @param[out] info         Pointer to the stored value and length of the data 
                           element. The size of the buffer allocated is to be 
                           atleast be the maximum size of the parameter to be 
                           fetched.
 
  @return
  DS_PROFILE_REG_RESULT_SUCCESS                -- Operation succeeded.\n
  DS_PROFILE_REG_RESULT_ERR_LIB_NOT_INITED     -- Library is not initialized.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_HNDL         -- Invalid handle.\n 
  DS_PROFILE_REG_RESULT_ERR_INVAL_IDENT        -- Invalid identifier.\n
  DS_PROFILE_REG_RESULT_ERR_LEN_INVALID        -- Buffer size is less than 
                                                  required.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE -- Invalid technology type.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_NUM  -- Invalid profile number.\n
  DS_PROFILE_REG_RESULT_FAIL                   -- General failure. 

  @dependencies 
  The software library must be initialized.
  @newpage
*/
ds_profile_status_etype ds_profile_get_param (  
  ds_profile_hndl_type        profile_hndl,
  ds_profile_identifier_type  identifier,
  ds_profile_info_type       *info
);

/*===========================================================================
FUNCTION DS_PROFILE_SET_PARAM 
============================================================================*/
/** @ingroup ds_profile_set_param
  Sets the profile data element identified by the identifier. The identifiers 
  are specified in the corresponding technology header file. The prefetched 
  copy is modified. The ds_profile_end_transaction() function modifies the 
  profile on the modem. 
 
  @datatypes
  #ds_profile_hndl_type\n
  #ds_profile_identifier_type\n
  #ds_profile_info_type

  @vertspace
  @param[in] profile_hndl  Handle to profile to set profile data elements.
  @param[in] identifier    Identifies the profile data elements.
  @param[in] info          Pointer to the value to which the data element is to 
                           be set. The size of the buffer passed can be up to 
                           the maximum size of the parameter that needs to be 
                           set.
 
  @return
  DS_PROFILE_REG_RESULT_SUCCESS                -- Operation succeeded.\n
  DS_PROFILE_REG_RESULT_ERR_LIB_NOT_INITED     -- Library is not initialized.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_HNDL         -- Invalid handle.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_IDENT        -- Invalid identifier.\n
  DS_PROFILE_REG_RESULT_ERR_LEN_INVALID        -- Buffer size is less than 
                                                  required.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE -- Invalid technology type.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_NUM  -- Invalid profile number.\n
  DS_PROFILE_REG_RESULT_FAIL                   -- General failure. 

  @dependencies 
  The software library must be initialized.
  @newpage
*/
ds_profile_status_etype ds_profile_set_param (  
  ds_profile_hndl_type         profile_hndl,
  ds_profile_identifier_type   identifier,
  const ds_profile_info_type  *info
);

/*===========================================================================
FUNCTION DS_PROFILE_RESET_PARAM_TO_INVALID 
============================================================================*/
/** @ingroup ds_profile_reset_param_to_invalid
  Resets a parameter value in a profile to the default. This function
  directly changes the value on the modem, so a call to begin/end a transaction 
  is not required before/after this function.

  @note1hang Not all technology types support this operation.
 
  @datatypes
  #ds_profile_tech_etype\n
  #ds_profile_num_type\n
  #ds_profile_identifier_type

  @vertspace
  @param[in] tech    Technology type.
  @param[in] num     Profile number.
  @param[in] ident   Identifies the profile parameter to be set to the default.
 
  @return
  DS_PROFILE_REG_RESULT_SUCCESS                -- Operation succeeded.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE -- Invalid technology type.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_NUM  -- Invalid profile number.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_OP           -- Operation is not supported 
                                                  for the technology type.\n
  DS_PROFILE_REG_RESULT_FAIL                   -- General failure. 

  @dependencies 
  The software library must be initialized.
  @newpage
*/
ds_profile_status_etype ds_profile_reset_param_to_invalid (  
  ds_profile_tech_etype       tech,
  ds_profile_num_type         num,
  ds_profile_identifier_type  ident
);

/*===========================================================================
FUNCTION DS_PROFILE_RESET_PROFILE_TO_DEFAULT 
============================================================================*/
/** @ingroup ds_profile_reset_profile_to_default
  Resets all the parameters of the profile to the default. This function 
  directly changes the value on the modem, so a call to begin/end a transaction
  is not required before/after this function.

  @note1hang Not all technology types support this operation.
 
  @datatypes
  #ds_profile_tech_etype\n
  #ds_profile_num_type

  @vertspace
  @param[in] tech   Technology type.
  @param[in] num    Profile number.

  @return
  DS_PROFILE_REG_RESULT_SUCCESS                -- Operation succeeded.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE -- Invalid technology type.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_NUM  -- Invalid profile number.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_OP           -- Operation is not supported 
                                                  for the technology type.\n
  DS_PROFILE_REG_RESULT_FAIL                   -- General failure. 

  @dependencies 
  The software library must be initialized.
  @newpage
*/
ds_profile_status_etype ds_profile_reset_profile_to_default (  
  ds_profile_tech_etype  tech,
  ds_profile_num_type    num
);

/*===========================================================================
FUNCTION DS_PROFILE_RESET_PROFILE_TO_DEFAULT_PER_SUBS 
============================================================================*/
/** @ingroup ds_profile_reset_profile_to_default_per_subs
  Resets all the parameters of the profile to the default. This function 
  directly changes the value on the modem, so a call to begin/end a transaction
  is not required before/after this function.

  @note1hang Not all technology types support this operation.
 
  @datatypes
  #ds_profile_tech_etype\n
  #ds_profile_num_type

  @vertspace
  @param[in] tech   Technology.
  @param[in] num    Profile number.

  @return
  DS_PROFILE_REG_RESULT_SUCCESS                -- Operation succeeded.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE -- Invalid technology type.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_NUM  -- Invalid profile number.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_OP           -- Operation is not supported 
                                                  for the technology type.\n
  DS_PROFILE_REG_RESULT_FAIL                   -- General failure. 

  @dependencies 
  The software library must be initialized.
  @newpage
*/
ds_profile_status_etype ds_profile_reset_profile_to_default_per_sub (  
  ds_profile_tech_etype  tech,
  ds_profile_num_type    num,
  ds_profile_subs_etype  subs_id
);


/*===========================================================================
FUNCTION DS_PROFILE_SET_DEFAULT_PROFILE_NUM 
============================================================================*/
/** @ingroup ds_profile_set_default_profile_num
  Sets the given profile number as default profile for the family of the 
  specified technology.

  @note1hang Not all technology types support this operation.
 
  @datatypes
  #ds_profile_tech_etype\n
  #ds_profile_num_type

  @vertspace
  @param[in] tech    Technology type.
  @param[in] family  Profile family.
  @param[in] num     Profile number.

  @return
  DS_PROFILE_REG_RESULT_SUCCESS                -- Operation succeeded.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE -- Invalid technology type.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_NUM  -- Invalid profile number.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_OP           -- Operation is not supported 
                                                  for the technology type.\n
  DS_PROFILE_REG_RESULT_FAIL                   -- General failure. 

  @dependencies 
  The software library must be initialized.
  @newpage
*/
ds_profile_status_etype ds_profile_set_default_profile_num (  
  ds_profile_tech_etype  tech,
  uint32                 family, 
  ds_profile_num_type    num
);

/*===========================================================================
FUNCTION DS_PROFILE_SET_DEFAULT_PROFILE_NUM_PER_SUBS 
============================================================================*/
/** @ingroup ds_profile_set_default_profile_num_per_subs
  Sets the given profile number as default profile for the family of the 
  specified technology and subscription.

  @note1hang Not all technology types support this operation.
 
  @datatypes
  #ds_profile_tech_etype\n
  #ds_profile_subs_etype\n
  #ds_profile_num_type

  @vertspace
  @param[in] tech      Technology type. 
  @param[in] family    Profile family.  
  @param[in] subs_id   Subscription ID. Supported values: subs 1 or subs 2.
  @param[in] num       Profile number.

  @return
  DS_PROFILE_REG_RESULT_SUCCESS                -- Operation succeeded.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE -- Invalid profile type.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_NUM  -- Invalid profile number.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_OP           -- Operation is not supported 
                                                  for technology type.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_SUBS_ID      -- Invalid subscription ID.\n
  DS_PROFILE_REG_RESULT_FAIL                   -- General failure. 

  @dependencies 
  The software library must be initialized.
  @newpage
*/
ds_profile_status_etype ds_profile_set_default_profile_num_per_subs (
  ds_profile_tech_etype  tech,
  uint32                 family,
  ds_profile_subs_etype  subs_id,
  ds_profile_num_type    num
);

/*===========================================================================
FUNCTION DS_PROFILE_GET_DEFAULT_PROFILE_NUM 
============================================================================*/
/** @ingroup ds_profile_get_default_profile_num
  Gets the default profile number for the family of the specified technology.
 
  @datatypes
  #ds_profile_tech_etype\n
  #ds_profile_num_type

  @vertspace
  @param[in] tech     Technology type.
  @param[in] family   Profile family.
  @param[out] num     Pointer to store default profile number.

  @return
  DS_PROFILE_REG_RESULT_SUCCESS                -- Operation succeeded.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE -- Invalid profile type.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_OP           -- Operation is not supported 
                                                  for technology type.\n
  DS_PROFILE_REG_RESULT_FAIL                   -- General failure. 

  @dependencies 
  The software library must be initialized.
  @newpage
*/
ds_profile_status_etype ds_profile_get_default_profile_num (  
  ds_profile_tech_etype   tech,
  uint32                  family, 
  ds_profile_num_type    *num
);

/*===========================================================================
FUNCTION DS_PROFILE_GET_DEFAULT_PROFILE_NUM_PER_SUBS 
============================================================================*/
/** @ingroup ds_profile_get_default_profile_num_per_subs
  Gets the default profile number for the family of the specified technology 
  and subscription.

  @note1hang Not all technology types support this operation.
 
  @datatypes
  #ds_profile_tech_etype\n
  #ds_profile_subs_etype\n
  #ds_profile_num_type

  @vertspace
  @param[in] tech     Technology type. 
  @param[in] family   Profile family.  
  @param[in] subs_id  Subscription ID. Supported values: subs 1 or subs 2.
  @param[out] num     Profile number.

  @return
  DS_PROFILE_REG_RESULT_SUCCESS                -- Operation succeeded.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE -- Invalid profile type.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_OP           -- Operation is not supported 
                                                  for technology type.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_SUBS_ID      -- Invalid subscription ID.\n
  DS_PROFILE_REG_RESULT_FAIL                   -- General failure. 

  @dependencies 
  The software library must be initialized.
  @newpage
*/
ds_profile_status_etype ds_profile_get_default_profile_num_per_subs (  
  ds_profile_tech_etype   tech,
  uint32                  family,
  ds_profile_subs_etype   subs_id,
  ds_profile_num_type     *num
);

/*===========================================================================
FUNCTION DS_PROFILE_GET_MAX_NUM 
============================================================================*/
/** @ingroup ds_profile_get_max_num
  Returns the maximum number of profiles possible for a given technology type.
 
  @datatypes
  #ds_profile_tech_etype

  @vertspace
  @param[in] tech       Technology type.
  @param[out] max_num   Pointer to the value that indicates the maximum number 
                        of profiles possible to be stored.

  @return
  DS_PROFILE_REG_RESULT_SUCCESS                -- Operation succeeded.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE -- Invalid profile type.\n
  DS_PROFILE_REG_RESULT_FAIL                   -- General failure. 

  @dependencies 
  The software library must be initialized.
  @newpage
*/
ds_profile_status_etype ds_profile_get_max_num (  
  ds_profile_tech_etype   tech,
  uint32                 *max_num
);

/*===========================================================================
FUNCTION DS_PROFILE_GET_LIST_ITR 
============================================================================*/
/** @ingroup ds_profile_get_list_itr
  Gets the list of profiles of a particular technology type. This 
  function returns an iterator. The iterator is traversed using 
  ds_profile_itr_next(). After traversal is complete, the caller is 
  expected to call ds_profile_itr_destroy().
 
  @datatypes
  #ds_profile_tech_etype\n
  #ds_profile_list_type\n
  #ds_profile_itr_type

  @vertspace
  @param[in] tech   Technology type.
  @param[in] lst    Pointer to a list of profiles generated from a search.
  @param[out] itr   Pointer to an iterator to traverse through the search 
                    results.

  @return
  DS_PROFILE_REG_RESULT_SUCCESS                -- Operation succeeded.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE -- Invalid profile type.\n
  DS_PROFILE_REG_RESULT_FAIL                   -- General failure. 

  @dependencies 
  The software library must be initialized.
  @newpage
*/
ds_profile_status_etype ds_profile_get_list_itr ( 
  ds_profile_tech_etype  tech,
  ds_profile_list_type  *lst,
  ds_profile_itr_type   *itr
);

/*===========================================================================
FUNCTION DS_PROFILE_GET_LIST_ITR_PER_SUB 
============================================================================*/
/** @ingroup ds_profile_get_list_itr
  Gets the list of profiles of a particular technology type. This 
  function returns an iterator. The iterator is traversed using 
  ds_profile_itr_next(). After traversal is complete, the caller is 
  expected to call ds_profile_itr_destroy().
 
  @datatypes
  #ds_profile_tech_etype\n
  #ds_profile_list_type\n
  #ds_profile_itr_type

  @vertspace
  @param[in] tech   Technology type.
  @param[in] lst    Pointer to a list of profiles generated from a search.
  @param[out] itr   Pointer to an iterator to traverse through the search 
                    results.

  @return
  DS_PROFILE_REG_RESULT_SUCCESS                -- Operation succeeded.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE -- Invalid profile type.\n
  DS_PROFILE_REG_RESULT_FAIL                   -- General failure. 

  @dependencies 
  The software library must be initialized.
  @newpage
*/
ds_profile_status_etype ds_profile_get_list_itr_per_sub ( 
  ds_profile_tech_etype  tech,
  ds_profile_list_type  *lst,
  ds_profile_itr_type   *itr,
  ds_profile_subs_etype  subs_id
);

/*===========================================================================
FUNCTION DS_PROFILE_ITR_NEXT 
============================================================================*/
/** @ingroup ds_profile_itr_next
  Advances the iterator to the next element.

  @datatypes
  #ds_profile_itr_type

  @vertspace
  @param[in] itr   Iterator to traverse through the list.

  @return
  DS_PROFILE_REG_RESULT_SUCCESS    -- Operation succeeded.\n
  DS_PROFILE_REG_RESULT_FAIL       -- General failure.\n 
  DS_PROFILE_REG_RESULT_ERR_INVAL  -- Invalid argument (iterator).\n
  DS_PROFILE_REG_RESULT_LIST_END   -- End of the list. 

  @dependencies 
  The software library must be initialized.
  @newpage
*/
ds_profile_status_etype ds_profile_itr_next (  
  ds_profile_itr_type   itr
);

/*===========================================================================
FUNCTION DS_PROFILE_ITR_FIRST 
============================================================================*/
/** @ingroup ds_profile_itr_first
  Resets the iterator to the beginning of the list.

  @datatypes
  #ds_profile_itr_type

  @vertspace
  @param[in] itr Iterator to traverse through the list.

  @return
  DS_PROFILE_REG_RESULT_SUCCESS    -- Operation succeeded.\n
  DS_PROFILE_REG_RESULT_FAIL       -- General failure.\n 
  DS_PROFILE_REG_RESULT_ERR_INVAL  -- Invalid argument (iterator).

  @dependencies 
  The software library must be initialized.
  @newpage
*/
ds_profile_status_etype ds_profile_itr_first ( 
  ds_profile_itr_type   itr
);

/*===========================================================================
FUNCTION DS_PROFILE_GET_INFO_BY_ITR 
============================================================================*/
/** @ingroup ds_profile_get_info_by_itr
  Gets the information stored in the iterator node. 

  @datatypes
  #ds_profile_itr_type\n
  #ds_profile_list_info_type

  @vertspace
  @param[in] itr         Iterator to traverse through the list.
  @param[out] list_info  Pointer to the structure to return the profile 
                         information.

  @return
  DS_PROFILE_REG_RESULT_SUCCESS    -- Operation succeeded.\n
  DS_PROFILE_REG_RESULT_FAIL       -- General failure.\n 
  DS_PROFILE_REG_RESULT_ERR_INVAL  -- Invalid argument (iterator).

  @dependencies 
  The software library must be initialized.
  @newpage
*/
ds_profile_status_etype ds_profile_get_info_by_itr ( 
  ds_profile_itr_type         itr,
  ds_profile_list_info_type  *list_info
);

/*===========================================================================
FUNCTION DS_PROFILE_ITR_DESTROY 
============================================================================*/
/** @ingroup ds_profile_itr_destroy
  Destroys the iterator.

  @datatypes
  #ds_profile_itr_type

  @vertspace
  @param[in] itr   Iterator to traverse through the list.

  @return
  DS_PROFILE_REG_RESULT_SUCCESS    -- Operation succeeded.\n
  DS_PROFILE_REG_RESULT_FAIL       -- General failure.\n 
  DS_PROFILE_REG_RESULT_ERR_INVAL  -- Invalid argument (iterator).

  @dependencies 
  The software library must be initialized.
  @newpage
*/
ds_profile_status_etype ds_profile_itr_destroy ( 
  ds_profile_itr_type   itr
);

#ifdef FEATURE_DATA_LTE
/*===========================================================================
FUNCTION DS_PROFILE_UPDATE_LTE_ATTACH_PDN_LIST_PROFILES 
============================================================================*/
/** @ingroup ds_profile_update_lte_attach_pdn_list_profiles
  Updates the profile parameters in the lte attach pdn list of the 
  specified technology.

  @note1hang Not all technology types support this operation.
 
  @datatypes
  #ds_profile_tech_etype
  
  @vertspace
  @param[in] tech    Technology type.

  @return
  DS_PROFILE_REG_RESULT_SUCCESS                -- Operation succeeded.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE -- Invalid technology type.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_OP           -- Operation is not supported 
                                                  for the technology type.\n
  DS_PROFILE_REG_RESULT_FAIL                   -- General failure. 

  @dependencies 
  The software library must be initialized.
  @newpage
*/
ds_profile_status_etype ds_profile_update_lte_attach_pdn_list_profiles (  
  ds_profile_tech_etype  tech
);

/*===========================================================================
FUNCTION DS_PROFILE_UPDATE_LTE_ATTACH_PDN_LIST_PROFILES_PER_SUBS 
============================================================================*/
/** @ingroup ds_profile_update_lte_attach_pdn_list_profiles_per_subs
  Updates the profile parameters in the lte attach pdn list of the 
  specified technology.

  @note1hang Not all technology types support this operation.
 
  @datatypes
  #ds_profile_tech_etype
  
  @vertspace
  @param[in] tech    Technology type.

  @return
  DS_PROFILE_REG_RESULT_SUCCESS                -- Operation succeeded.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_PROFILE_TYPE -- Invalid technology type.\n
  DS_PROFILE_REG_RESULT_ERR_INVAL_OP           -- Operation is not supported 
                                                  for the technology type.\n
  DS_PROFILE_REG_RESULT_FAIL                   -- General failure. 

  @dependencies 
  The software library must be initialized.
  @newpage
*/
ds_profile_status_etype ds_profile_update_lte_attach_pdn_list_profiles_per_subs (  
  ds_profile_tech_etype  tech,
  ds_profile_subs_etype  subs_id
);
#endif /* FEATURE_DATA_LTE */

#endif /* DS_PROFILE_H */
