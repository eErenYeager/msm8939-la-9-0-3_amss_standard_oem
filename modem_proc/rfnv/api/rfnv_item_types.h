#ifndef RFNV_ITEM_TYPES_H
#define RFNV_ITEM_TYPES_H
/*===========================================================================

                             R F   N V  Data  Types

DESCRIPTION
  This header file contains the Data types and IDs  used for variant NVs

*/

#if defined(RFA_INTERNAL_NOTES)
#error code not present
#endif

/*

Copyright (c) 2012-2014 by Qualcomm Technologies, Inc.  All Rights Reserved.
===========================================================================*/


/*===========================================================================

                      EDIT HISTORY FOR FILE

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/rfnv/api/rfnv_item_types.h#1 $


when       who     what, where, why
--------   ---     ----------------------------------------------------------
01/21/15   kg      Added support for Rx RSB calibration
12/09/14   yzw     Added Rx static and Rx alt path MTPL data type
11/17/14   bmg     Added Rx RSB calibration data type
11/06/14   vb/ka   Added LTE FBRx droop cal
10/13/14   kab     Added support for 3DLCA BC Config
10/07/14   cri     Added support for LTE interband CA BW
09/26/16   kai     Added support for new WCDMA RX static and cal NVs
09/10/14   pl      Added support for C2K cal PM cal_type
05/09/14   pl      Added support for common RX CAL data
02/24/14   aa      Updated gain cal for predet
01/30/14   aa      Updated fbrx gain cal params
11/12/13   aa      Added data type for RFNV_DATA_TYPE_FBRX_GAIN_V2
08/15/13   dw      Update Rx static and Cal type
08/12/13   vs      Added data for Rx static and cal type
07/16/13   dbz     Added data type for FBRX_GAIN type
03/05/13   aka     Added data type ID for RFNV_DATA_TYPE_TX_PWR_ERR_LIST
10/25/12   aka     New file for Variant Nv strcutures and IDs
===========================================================================*/

/*===========================================================================

                                 INCLUDE FILES

===========================================================================*/
#include "comdef.h"

#ifdef __cplusplus
extern "C" {
#endif

#if defined(_MSC_VER)
/* 
  START of section to make visual studio happy in order for "PACK"
  to work properly for off-target testing.
  { 
*/
#pragma pack( push, 1 )
#endif

#define RFNV_LINEAR_MAX_RGI_INDEX_PER_GAIN_STATE   32

#define GSM_AMAM_TBL_SIZE      128

#define GSM_AMPM_TBL_SIZE      128

#define RFNV_DATA_TYPE_ID_SIZE 2

#define RFNV_NUMBER_OF_PCLS    16

#define RFNV_MAX_LNA_GAIN_STATES 8

#define WCDMA_NUM_LNA_STATES 5

/* MAX DL CA Combos need to increase with "RFNV_LTE_CA_BC_CONFIG_I" NV Item Capacity. */
#define LTE_MAX_DL_CA_COMBOS   128

/* MAX # of DL CA carriers Supported is 5 DLCA. */
#define LTE_DL_CA_MAX_NUM_CARRIERS    5

#define RFNV_RX_PATH_LNA_SWTPS   2*RFNV_MAX_LNA_GAIN_STATES

enum{
  /* 0-99 is reserved for legacy data types, will be added later */
  RFNV_DATA_TYPE_ID_CAL_CHANS                        = 100, 
  RFNV_DATA_TYPE_ID_FREQ_OFFSETS                     = 101,
  RFNV_DATA_TYPE_ID_LIN_TABLE_V3                     = 102,
  //RFNV_DATA_TYPE_ID_EPT_DPD_V1                     = 103, /* <-- Not used. Here only to acknowledge Nikel's EPT */
  RFNV_DATA_TYPE_ID_EPT_DPD_V2                       = 104,
  RFNV_DATA_TYPE_ID_ET_DPD                           = 105,
  RFNV_DATA_TYPE_ID_RGI_LIST                         = 106,   /* rfnv_data_rgi_list_type */
  RFNV_DATA_TYPE_ID_PMEAS_LIST                       = 107,   /* rfnv_data_pmeas_list_type */
  RFNV_DATA_TYPE_ID_AMAM_LIST                        = 108,   /* rfnv_data_amam_list_type */
  RFNV_DATA_TYPE_ID_AMPM_LIST                        = 109,   /* rfnv_data_ampm_list_type */
  RFNV_DATA_TYPE_TX_PWR_ERR_LIST                     = 110,   /* rfnv_data_pwr_err_list_type */
  RFNV_DATA_TYPE_REF_LIN_TEMP_ADC                    = 111,   /* rfnv_data_ref_lin_temp_adc_type */
  RFNV_DATA_TYPE_FBRX_GAIN                           = 112,   /* rfnv_data_fbrx_gain_type */
  RFNV_DATA_TYPE_ID_XPT_DPD_SCALING_V1               = 113,   /* rfnv_data_xpt_dpd_scaling_v1 */
  RFNV_DATA_TYPE_LNA_SWPTS_V1                        = 114,   /* rfnv_data_lna_swpts_type */
  RFNV_DATA_TYPE_RX_CAL_OFFSET_V1                    = 115,   /* rfnv_rx_cal_offset_type */
  RFNV_DATA_TYPE_RX_OFFSET_VS_FREQ_V1                = 116,   /* rfnv_rx_offset_vs_freq_type */
  RFNV_DATA_TYPE_FBRX_GAIN_V2                        = 117,   /* rfnv_data_fbrx_gain_type_v2 */
  RFNV_DATA_TYPE_RX_CAL_OFFSET_V2                    = 118,   /* rfnv_rx_cal_data_typeid_118_type in rflte_nv.h */
  RFNV_DATA_TYPE_ID_LIN_TABLE_V4                     = 119,   /* Multi Lin NV with Tx RSB and LOFT correction  */
  RFNV_DATA_TYPE_LNA_SWPTS_V2                        = 120,   /* rfnv_data_lna_swpts_type_v2 */
  RFNV_DATA_TYPE_LNA_SWPTS_V3                        = 122,   /* rfnv_data_lna_swpts_type_v3 */
  RFNV_DATA_TYPE_ID_LTE_FBRX_DROOP_FIR_FILTER_COEFF  = 123,   /* FBRx internal data structure: rfcommon_fbrx_nv_lte_bw_params_droop_type */
  RFNV_DATA_TYPE_RXRSB_V1                            = 124,   /* rfnv_data_type_rxrsb_v1 */
  RFNV_DATA_TYPE_LTE_RX_STATIC                       = 125,   /* rfnv_rx_static_data_type*/
  RFNV_DATA_TYPE_LTE_RX_ALT_PATH_MTPL                = 126,   /* rfnv_rx_alt_path_mtpl_data_type*/
};

enum 
{
  /* WCDMA RX NV_TYPE use 1-5000*/
  RFNV_RX_CAL_DATA_NV_TYPE_WCDMA_SC = 1,
  RFNV_RX_CAL_DATA_NV_TYPE_WCDMA_DC = 2,
  RFNV_RX_CAL_DATA_NV_TYPE_WCDMA_3C = 3,

  /* LTE RX NV_TYPE use 5001-10000*/
  RFNV_RX_CAL_DATA_NV_TYPE_LTE_BW_1p4 = 5001,
  RFNV_RX_CAL_DATA_NV_TYPE_LTE_BW_3p5 = 5002,
  RFNV_RX_CAL_DATA_NV_TYPE_LTE_BW_5p0 = 5003,
  RFNV_RX_CAL_DATA_NV_TYPE_LTE_BW_10p0 = 5004,
  RFNV_RX_CAL_DATA_NV_TYPE_LTE_BW_15p0 = 5005,
  RFNV_RX_CAL_DATA_NV_TYPE_LTE_BW_20p0 = 5006,
  RFNV_RX_CAL_DATA_NV_TYPE_LTE_BW_40p0 = 5007,
  RFNV_RX_CAL_DATA_NV_TYPE_LTE_INTRA_BW_1p4 = 5008,
  RFNV_RX_CAL_DATA_NV_TYPE_LTE_INTRA_BW_3p5 = 5009,
  RFNV_RX_CAL_DATA_NV_TYPE_LTE_INTRA_BW_5p0 = 5010,
  RFNV_RX_CAL_DATA_NV_TYPE_LTE_INTRA_BW_10p0 = 5011,
  RFNV_RX_CAL_DATA_NV_TYPE_LTE_INTRA_BW_15p0 = 5012,
  RFNV_RX_CAL_DATA_NV_TYPE_LTE_INTRA_BW_20p0 = 5013,
  RFNV_RX_CAL_DATA_NV_TYPE_LTE_INTRA_BW_40p0 = 5014,

  /* C2K RX NV_TYPE use 10001-15000*/
  RFNV_RX_CAL_NV_TYPE_CDMA_PM0         = 10001 ,
  RFNV_RX_CAL_NV_TYPE_CDMA_PM1         = 10002 ,
  RFNV_RX_CAL_NV_TYPE_CDMA_PM2         = 10003 ,

  /* TDS RX NV_TYPE use 15001-20000*/
  RFNV_RX_CAL_DATA_NV_TYPE_TDSCDMA_LL = 15001,
  RFNV_RX_CAL_DATA_NV_TYPE_TDSCDMA_HL = 15002,
};

/*----------------------------------------------------------------------------*/
/*! 
  @brief 
  Defines the variant marker type used in variable-size NVs
*/
typedef PACK(struct)
{
  /*! NV item version */
  uint16  version;

  /*! The number of elements contained in the NV item (not counting variant_marker element) */
  uint16  num_elements;
} rfnv_data_variant_marker_type;

typedef PACK (struct)
{
  uint8 mod;     /*0 = GMSK, 1 = 8PSK*/
  uint8 pa_state;
  uint8 valid_rgi_num;
  uint8 rgi_list[RFNV_LINEAR_MAX_RGI_INDEX_PER_GAIN_STATE];
}rfnv_data_rgi_list_type;

typedef PACK (struct)
{
  uint8 mod;     /*0 = GMSK, 1 = 8PSK*/
  uint8 channel_index;
  uint8 pa_state;
  int16 pmeas_list[RFNV_LINEAR_MAX_RGI_INDEX_PER_GAIN_STATE];
}rfnv_data_pmeas_list_type;

typedef PACK (struct)
{
  uint8 amam_ampm_identifier;  /* 0 = AMAM, 1= AMPM */
  uint8 channel_index;
  uint16 data_list[GSM_AMAM_TBL_SIZE];
}rfnv_data_amam_list_type;

typedef PACK (struct)
{
  uint8 amam_ampm_identifier;  /* 0 = AMAM, 1= AMPM */
  uint8 channel_index;
  int16 data_list[GSM_AMPM_TBL_SIZE];
}rfnv_data_ampm_list_type;

typedef PACK (struct)
{
  uint8 mod;     /*0 = GMSK, 1 = 8PSK*/
  uint8 channel_index;
  int16 pwr_err_list[RFNV_NUMBER_OF_PCLS];
}rfnv_data_pwr_err_list_type;

typedef PACK (struct)
{
  uint8 channel_index;
  uint8 gain_state_index;
  int16 fbrx_gain;
}rfnv_data_fbrx_gain_type;

typedef PACK (struct)
{
  uint8 channel_index;
  uint8 fbrx_mode_index;
  uint8 gain_state_index;
  int32 fbrx_gain;
  int16 tx_power;
  uint8 ubias;
  uint16 rx_scale;
  uint16 predet_index;
}rfnv_data_fbrx_gain_type_v2;

typedef PACK(struct)
{
  uint8 chain_id;
  uint8 carr_id;
  int16 lna_rise[WCDMA_NUM_LNA_STATES];
  int16 lna_fall[WCDMA_NUM_LNA_STATES];
  int16 lna_hs_rise[WCDMA_NUM_LNA_STATES];
  int16 lna_hs_fall[WCDMA_NUM_LNA_STATES];
}rfnv_data_lna_swpts_type;

typedef PACK(struct)
{
  uint8 chain_id;
  uint8 carr_id;
  int16 lna_offsets[WCDMA_NUM_LNA_STATES];
  int16 vga_offset;
}rfnv_rx_cal_offset_type;

typedef PACK(struct)
{
  uint8 chain_id;
  uint16 channel;
  uint8 chan_index;
  int8 vga_gain_offset_vs_freq;
  int8 lna_offset_vs_freq;
  int8 lna_offset_vs_freq_2;
  int8 lna_offset_vs_freq_3;
  int8 lna_offset_vs_freq_4;
}rfnv_rx_offset_vs_freq_type;


typedef PACK ( struct )
{
  uint16   path_idx;
  uint16   cal_type;
  uint16  channel_list[RFNV_NUMBER_OF_PCLS];
  int16   freq_comp_offset[RFNV_MAX_LNA_GAIN_STATES][RFNV_NUMBER_OF_PCLS];
} rfnv_rx_cal_data_typeid_118_type;

typedef PACK(struct)
{
  uint8    nv_active;
  int16    sin_theta;
  int16    cos_theta;
  int16    gain_inv;
} rsb_data_list_per_gain;

typedef PACK(struct)
{
  uint16                     path_idx;
  uint8                      rsb_type;
  uint16                     dl_channel;
  int32                      freq_offset_khz;
  uint8                      bw;
  rsb_data_list_per_gain     rsb_data[RFNV_MAX_LNA_GAIN_STATES];
} rfnv_rx_cal_data_typeid_124_type;

typedef PACK(struct)
{
  rfnv_data_variant_marker_type     variant_marker;
  rfnv_rx_cal_data_typeid_118_type  *cal_118_data;
  uint32                            num_of_cal_118_elements;
  uint32                            num_of_cal_124_elements;
  rfnv_rx_cal_data_typeid_124_type  *cal_124_data;
} rfnv_rx_cal_data_type;

typedef PACK(struct)
{
  uint8 rx_num_gain_states;
  int16 rx_gain_offsets[RFNV_MAX_LNA_GAIN_STATES];
  int16 rx_path_lna_swtps[RFNV_RX_PATH_LNA_SWTPS];
}rfnv_rx_static_data_type;

typedef PACK(struct)
{
  uint8 enable;
  int16 mtpl_value;
}rfnv_rx_alt_path_mtpl_data_type;

#if defined(_MSC_VER)
/* 
  } 
  END of section to make visual studio happy in order for "PACK"
  to work properly for off-target testing. 
*/
#pragma pack( pop )
#endif


#ifdef __cplusplus
}
#endif

#endif
