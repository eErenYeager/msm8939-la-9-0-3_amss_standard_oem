#ifndef QFE1520_HDET_LTE_H
#define QFE1520_HDET_LTE_H
/*!
   @file
   qfe1520_hdet_lte.h

   @brief
   QFE1520 HDET 
   lte driver header file

*/

/*===========================================================================

Copyright (c) 2013 by Qualcomm Technologies, Inc.  All Rights Reserved.

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: 

when       who   what, where, why
--------   ---   ------------------------------------------------------------------- 
06/16/14   sbm    HDET online brinup
04/04/13    vb    Use comm params from common HDET
03/20/13    vb    Merge with QFE1510 driver implementation
02/12/13   aca    Initial version

============================================================================*/

/*===========================================================================
                           INCLUDE FILES
===========================================================================*/
#include "rfdevice_hdet_lte.h"
#include "rfdevice_hdet_cmn.h"
#include "qfe1520_hdet_cmn.h"

#ifdef __cplusplus
extern "C" {
#endif

#include "qfe1520_cmn_hal_typedefs.h"
#include "rfcommon_locks.h"

#ifdef __cplusplus
}
#endif

class qfe1520_hdet_lte: public rfdevice_hdet_lte
{
  public:

  /* Constructor overloaded to get the RFFE device details */   
  qfe1520_hdet_lte( rfdevice_hdet_device_num_type device_id,
                    rf_path_enum_type rf_path,
                    qfe1520_comm_params_type comm_param_vals,
                    void *qfe1520_hdet_cmn_p
                   ); 
 
  // Destructor
  ~qfe1520_hdet_lte();   

  void init();
  
  boolean hdet_init
  (
    rf_buffer_intf *buff_obj_ptr, 
    rf_device_execution_type dev_action,
    uint32 tx_freq,
    rfm_device_enum_type device,
    rfcom_lte_band_type band
  );

  boolean enable_hdet
  (
    rf_buffer_intf *buff_obj_ptr, 
    rf_device_execution_type dev_action,
    rfdevice_lte_hdet_enable_type *hdet_data,
    rfm_device_enum_type device,
    rfcom_lte_band_type band
  );

  boolean disable_hdet
  (
    rf_buffer_intf *buff_obj_ptr, 
    rf_device_execution_type dev_action,
    rfm_device_enum_type device,
    rfcom_lte_band_type band
  );
  boolean do_full_hdet
  (
    rf_buffer_intf *buff_obj_ptr, 
    rf_device_execution_type dev_action,
    rfdevice_lte_hdet_settings_type *hdet_data,
    rfm_device_enum_type device,
    rfcom_lte_band_type band
  );

  boolean do_hdet_read
  (
    rf_buffer_intf *buff_obj_ptr, 
    rf_device_execution_type dev_action,
    rfdevice_lte_hdet_settings_type *hdet_data
  );

  boolean get_hdet_setting
  (
    rf_buffer_intf *buff_obj_ptr, 
    rf_device_execution_type dev_action,
    rfdevice_lte_hdet_therm_data_type* lte_hdet_therm_settings,
    rfdevice_lte_script_data_type* script_data_ptr,
    rfm_device_enum_type device,
    rfcom_lte_band_type band
  );
  boolean get_hdet_val
  (
    rf_buffer_intf *buff_obj_ptr, 
    rf_device_execution_type dev_action,
    rfdevice_lte_hdet_therm_data_type* lte_hdet_therm_settings,
    rfm_device_enum_type device,
    rfcom_lte_band_type band
  );

  protected:
 
  private:
  /*! Device communication prototype */
  rfdevice_hdet_device_num_type device_num;

  /*! The RF path this device is associated to 
      This is needed in order to retrive the correct bc_config */
  rf_path_enum_type device_rf_path;

  /*! Device communication prototype */
  qfe1520_comm_params_type comm_params;

  /*! Buffer to hold AG script */
  qfe1520_script_type qfe1520_script;

  /*! TX Frequency */
  uint32 tx_freq;

  /*! common hdet instance*/
  rfdevice_hdet_cmn *qfe1520_hdet_cmn_p;
};

#endif /* QFE1520_HDET_LTE_H */

