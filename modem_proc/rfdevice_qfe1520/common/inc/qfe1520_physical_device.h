#ifndef QFE1520_PHYSICAL_DEVICE_H
#define QFE1520_PHYSICAL_DEVICE_H
/*!
  @file
  qfe1520_physical_device.h 

  @brief
  Software abstraction of a QFE1520 physical device.
*/

/*===========================================================================

  Copyright (c) 2013,2014 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


/*===========================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/rfdevice_qfe1520/common/inc/qfe1520_physical_device.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------- 
05/28/14   vv     Physical device interface support for BOLT & DPM2.0
09/17/13   ndb    Added qfe1520_physical_device() Destructor 
04/26/13   adk    Initial version

===========================================================================*/

#include "rfdevice_physical_device.h"
#include "qfe1520_antenna_tuner.h"
#include "qfe1520_hdet_cmn.h"

typedef struct qfe1520_cal_data qfe1520_cal_data_type;


class qfe1520_physical_device : public rfdevice_physical_device
{
public:
  qfe1520_physical_device(rfc_device_cfg_info_type* cfg);

  ~qfe1520_physical_device();

  virtual bool validate_self_cal_efs(void);

  virtual bool load_self_cal(const char* str);

  
  virtual bool perform_self_cal(const char* str);
  
  virtual bool perform_k_sensor_cal(void);

  virtual bool load_k_sensor_cal(void);

  virtual bool perform_rc_tuner_cal(void);

  virtual bool load_rc_tuner_cal(void);
  

  virtual rfdevice_logical_component* get_component(rfdevice_type_enum_type type, int instance);



  /*Overloaded constructor for BOLT & DPM2.0 PLs*/
  qfe1520_physical_device(rfc_phy_device_info_type* cfg);

  /*Overloaded function for BOLT & DPM2.0 PLs*/
  virtual rfdevice_logical_component* get_component(rfc_logical_device_info_type *logical_device_cfg);

  /*Physical object pointer used by BOLT & DPM2.0 PLs*/
  qfe1520_physical_device* qfe1520_physical_device_p;

  rfc_phy_device_info_type* phy_device_cfg;

private:
  void create_tuner_object(void);
  void create_hdet_object(void);

 /*Overloaded API for physical device support in Bolt and DPM2.0*/
 void create_tuner_object( rfc_logical_device_info_type *logical_device_cfg);
 void create_hdet_object( rfc_logical_device_info_type *logical_device_cfg );

  rfc_device_cfg_info_type* cfg;
  qfe1520_antenna_tuner* tuner_ptr;
  qfe1520_hdet_cmn* hdet_ptr;
  
  /* Cal data */
  qfe1520_cal_data_type* calibrated_data;
};

#endif /* QFE1520_PHYSICAL_DEVICE_H */

