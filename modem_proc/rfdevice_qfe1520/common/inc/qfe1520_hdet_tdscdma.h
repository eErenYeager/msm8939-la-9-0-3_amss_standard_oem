#ifndef QFE1520_HDET_TDSCDMA_H
#define QFE1520_HDET_TDSCDMA_H
/*!
   @file
   qfe1520_hdet_tdscdma.h

   @brief
   QFE1520 HDET tdscdma driver header file

*/

/*===========================================================================

Copyright (c) 2013 by Qualcomm Technologies, Inc.  All Rights Reserved.

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: 

when       who   what, where, why
--------   ---   -------------------------------------------------------------------
04/26/13   adk   Initial version

============================================================================*/

/*===========================================================================
                           INCLUDE FILES
===========================================================================*/
#include "rfdevice_hdet_tdscdma.h"
#include "rfdevice_hdet_cmn.h"
#include "qfe1520_hdet_cmn.h"
#include "rfdevice_tdscdma_type_defs.h"

#ifdef __cplusplus
extern "C" {
#endif

#include "qfe1520_cmn_hal_typedefs.h"
#include "rfcommon_locks.h"

#ifdef __cplusplus
}
#endif

class qfe1520_hdet_tdscdma: public rfdevice_hdet_tdscdma
{
  public:

  /* Constructor overloaded to get the RFFE device details */   
  qfe1520_hdet_tdscdma( rfdevice_hdet_device_num_type device_id,
                        rf_path_enum_type rf_path,
                        qfe1520_comm_params_type comm_param_vals,
                        void *qfe1520_hdet_cmn_p
                     ); 
 
  // Destructor
  ~qfe1520_hdet_tdscdma();   

  void init();
   
  boolean hdet_init
  (
    rf_buffer_intf *buff_obj_ptr, 
	rf_device_execution_type dev_action,
    uint32 tx_freq
  );
 
  boolean get_mdsp_config_data
  (
    rfdevice_hdet_mdsp_config_data_type *data 
  );
 
  boolean read_mdsp_triggered_incident
  (
    rf_buffer_intf *buff_obj_ptr, 
    rf_device_execution_type dev_action,
    uint16 *hdet_val
  );
  
  boolean tx_hdet_read
  (
  rfm_device_enum_type rfm_dev,
  rfcom_tdscdma_band_type rf_band,
  boolean mdsp_triggered_read,
  uint16 *hdet_value,
  rf_buffer_intf* buff_obj_ptr,
  rf_device_execution_type dev_action
  );

  boolean tx_hdet_setup
  (   
  rfm_device_enum_type rfm_dev,
  rfcom_tdscdma_band_type rf_band,
  rf_device_execution_type dev_action,
  rf_buffer_intf* buff_obj_ptr,
  void *data
  );

  boolean qfe1520_hdet_cmn_gated_read_script_ag_tds
  (
    qfe1520_comm_params_type comm_params,
    qfe1520_script_type* script
  );

  boolean tx_hdet_script_read
  (   
  rfm_device_enum_type rfm_dev,
  rfcom_tdscdma_band_type rf_band,
  rf_device_execution_type dev_action,
  rf_buffer_intf* buff_obj_ptr,
  void* data
  );

  boolean calc_hdet_measurement 
 ( 
  rfm_device_enum_type rfm_dev,
  rfcom_tdscdma_band_type rf_band,
  rfdevice_tdscdma_calc_hdet_measurement_type*  calc_hdet_meas);

  protected:
 
  private:
  /*! Device communication prototype */
  rfdevice_hdet_device_num_type device_num;

  /*! The RF path this device is associated to 
      This is needed in order to retrive the correct bc_config */
  rf_path_enum_type device_rf_path;

  /*! Device communication prototype */
  qfe1520_comm_params_type comm_params;

  /*! QFE1520 global mutex - one per instance. Each technology should
  use this mutex once initialized. Should be initialized during device
  configuration before any other usage */
  rf_lock_data_type crit_sect_lock;

  /*! Buffer to hold AG script */
  qfe1520_script_type qfe1520_script;

  /*! common hdet instance*/
  rfdevice_hdet_cmn *qfe1520_hdet_cmn_p;
};

#endif /*QFE1520_HDET_TDSCDMA_H*/
