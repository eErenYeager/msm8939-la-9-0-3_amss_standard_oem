/*==============================================================================
Copyright (C) 2010 Qualcomm Technologies, Incorporated. All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.

File Name
   ims_settings_common.h

Description
   This file contains the definition of the public interfaces
   to update IMS settings configuration values

================================================================================
Date    |   Author's Name    |  BugID  |        Change Description
================================================================================
01-12-2012    Murali Anand      331135   FR#1510 - QMI Setting Changes
--------------------------------------------------------------------------------
14-03-2014    Prashanth M E     577990   FR#17768 - AT command Setting Changes
--------------------------------------------------------------------------------
09-05-2014    Prashanth M E     659885   IMS Settings Enhancements
==============================================================================*/

#ifndef __IMS_SETTINGS_COMMON_H
#define __IMS_SETTINGS_COMMON_H

#include "ims_variation.h"
#include "customer.h"

#include "qvp_rtp_payload.h"

#include "qpConfigNVItem.h"
#include "qpdpl_qmi_defs.h"

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

#define VOIP_AMR_MODE_STR_LEN_MAX 15
#define VOIP_AMRWB_MODE_STR_LEN_MAX 17
#define AMR_BUFFER_LEN_MAX 32

/************************************************************************
Function ims_settings_get_sip_read_only_config()

Description
Get SIP Read Only Configuration parameters from the current
configuration that is in memory

Parameters
pSipReadOnlyConfig (OUT)- Structure pointer for SIP Read Only configuration

Dependencies
None

Return Value
QPE_IO_ERROR

Side Effects
None
************************************************************************/
QPE_IO_ERROR ims_settings_get_sip_read_only_config(ims_settings_sip_read_only_configuration *pSipReadOnlyConfig);

/************************************************************************
Function ims_settings_get_network_read_only_config()

Description
Get Network Read Only Configuration parameters from the current
configuration that is in memory

Parameters
pNWReadOnlyConfig (OUT)- Structure pointer for Network Read Only configuration

Dependencies
None

Return Value
QPE_IO_ERROR

Side Effects
None
************************************************************************/
QPE_IO_ERROR ims_settings_get_network_read_only_config(ims_settings_network_read_only_configuration *pNWReadOnlyConfig);

/************************************************************************
Function ims_settings_get_voip_read_only_config()

Description
Get VOIP Read Only Configuration parameters from the current
configuration that is in memory

Parameters
pVoipReadOnlyConfig (OUT)- Structure pointer for VOIP Read Only configuration

Dependencies
None

Return Value
QPE_IO_ERROR

Side Effects
None
************************************************************************/
QPE_IO_ERROR ims_settings_get_voip_read_only_config(ims_settings_voip_read_only_configuration *pVoipReadOnlyConfig);

/************************************************************************
Function ims_settings_get_user_read_only_config()

Description
Get User Read Only Configuration parameters from the current
configuration that is in memory

Parameters
pUserReadOnlyConfig (OUT)- Structure pointer for User Read Only configuration

Dependencies
None

Return Value
QPE_IO_ERROR

Side Effects
None
************************************************************************/
QPE_IO_ERROR ims_settings_get_user_read_only_config(ims_settings_user_read_only_configuration *pUserReadOnlyConfig);

/************************************************************************
Function ims_settings_get_regmgr_read_only_config()

Description
Get REG MGR Read Only Configuration parameters from the current
configuration that is in memory

Parameters
pRegMgrReadOnlyConfig (OUT)- Structure pointer for REG MGR Read Only configuration

Dependencies
None

Return Value
QPE_IO_ERROR

Side Effects
None
************************************************************************/
QPE_IO_ERROR ims_settings_get_regmgr_read_only_config(ims_settings_reg_mgr_read_only_configuration *pRegMgrReadOnlyConfig);

/************************************************************************
Function ims_settings_get_rcs_auto_config_read_only_config()

Description
Get RCS Auto Config Read Only Configuration parameters from the current
configuration that is in memory

Parameters
pRcsAutoConfigReadOnlyConfig (OUT)- Structure pointer for RCS Auto Config Read Only configuration

Dependencies
None

Return Value
QPE_IO_ERROR

Side Effects
None
************************************************************************/
QPE_IO_ERROR ims_settings_get_rcs_auto_config_read_only_config(ims_settings_rcs_auto_config_read_only_configuration *pRcsAutoConfigReadOnlyConfig);


/************************************************************************
Function ims_settings_get_rcs_imscore_auto_config_read_only_config()

Description
Get RCS IMSCore Auto Config Read Only Configuration parameters from the current
configuration that is in memory

Parameters
pRcsImscoreAutoConfigReadOnlyConfig (OUT)- Structure pointer for RCS IMScore Auto Config Read Only configuration

Dependencies
None

Return Value
QPE_IO_ERROR

Side Effects
None
************************************************************************/
QPE_IO_ERROR ims_settings_get_rcs_imscore_auto_config_read_only_config(ims_settings_rcs_imscore_auto_config_read_only_configuration *pRcsImscoreAutoConfigReadOnlyConfig);

/************************************************************************
Function ims_settings_initialize()

Description
   Initialize the config session

Dependencies
   None

Return Value
   AEE_IMS_SUCCESS if the command has been posted successfully for
   update, otherwise AEE_IMS_EFAILED is returned

Side Effects
   None
************************************************************************/
QPINT ims_settings_initialize(QPVOID *task_globaldata_ptr);

/************************************************************************
Function ims_settings_uninitialize()

Description
   Un-Initialize the config session. Releases the App config memory.

Dependencies
   None

Return Value
   None

Side Effects
   None
************************************************************************/
QPVOID ims_settings_uninitialize(QPVOID);

/************************************************************************
Function ims_settings_switch_to_ims_task_context(QPVOID *callback_ptr, QPVOID *pUserdata)

Description
IMS Settings API to switch to IMS Task context when APIs are invoked in Task context.

Parameters
callbackPtr - Callback function pointer to be called in IMS Task context
pUserdata   - User data information to be passed in to the callback 

Dependencies
None

Return Value
AEE_IMS_SUCCESS - On success.
AEE_IMS_EFAILED - On failure.

Side Effects
None
************************************************************************/
QPINT ims_settings_switch_to_ims_task_context(QPVOID *callbackPtr, QPVOID *pUserdata);

/************************************************************************
Function ims_settings_set_nv_config_items()

Description
Sets the NV Configuration for the config indicated in 'eCfgType' and
uses config from 'pConfigItem' which should be a structure pointer for
corresponding to NV Configuration type

Parameters
eCfgType         (IN) - NV configuration Type 
pVoipConfigItem  (IN) - Structure pointer for corresponding to NV Configuration type

Dependencies
None

Return Value
QPE_IO_ERROR

Side Effects
None
************************************************************************/
QPE_IO_ERROR ims_settings_set_nv_config_items(QPE_IMS_CONFIG_ITEMS eCfgType, QPVOID *pConfigItem);

/************************************************************************
Function ims_settings_get_nv_config_items()

Description
Gets the NV Configuration for the config indicated in 'eCfgType' and fills 'pConfigItem' which should be a
structure pointer for corresponding to NV Configuration type.

Parameters
eCfgType         (IN) - NV configuration Type
sVoipConfigItem  (OUT)- Structure pointer for corresponding to NV Configuration type

Dependencies
None

Return Value
QPE_IO_ERROR

Side Effects
None
************************************************************************/
QPE_IO_ERROR ims_settings_get_nv_config_items(QPE_IMS_CONFIG_ITEMS eCfgType, QPVOID *pConfigItem);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // __IMS_SETTINGS_COMMON_H