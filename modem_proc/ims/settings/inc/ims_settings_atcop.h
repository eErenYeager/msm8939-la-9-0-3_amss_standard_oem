/*==============================================================================
Copyright (C) 2013 Qualcomm Technologies, Incorporated. All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.

File Name
   ims_settings_atcop.h

Description
   This file contains the definition of the public interfaces
   to update IMS settings configuration values using AT commands

   APPLICATION/API User                              IMS_AT_CMD_SETTIGNS
   ______                                                  _____
     |           AT_CMD_SET_CONFIG_REQ                       |
     |------------------------------------------------------>|
     |    (Ex: QC_IMS_SETTINGS_AT_CMD_SET_VOIP_CONFIG_REQ)   |
     |                                                       |
     :                                                       :
     |           AT_CMD_SET_CONFIG_RESP                      |
     |<------------------------------------------------------|
     |    (Ex: QC_IMS_SETTINGS_AT_CMD_SET_VOIP_CONFIG_RSP)   |
     |                                                       |
     :                                                       :
     :                                                       :
     |           AT_CMD_GET_CONFIG_REQ                       |
     |------------------------------------------------------>|
     |    (Ex: QC_IMS_SETTINGS_AT_CMD_GET_VOIP_CONFIG_REQ)   |
     |                                                       |
     :                                                       :
     |           AT_CMD_GET_CONFIG_RESP                      |
     |<------------------------------------------------------|
     |    (Ex: QC_IMS_SETTINGS_AT_CMD_GET_VOIP_CONFIG_RSP)   |


================================================================================
Date        |   Author's Name    |  BugID  |        Change Description
================================================================================
14-Mar-2014   Prashanth M E       577990       FR 17768  - AT command Setting Changes
---------------------------------------------------------------------------------
03-Apr-2014   Prashanth M E       642878       MPSS Build - MPSS.BO.1.0 Tiberium Strip/Pack Build Failed
==============================================================================*/

#ifndef __IMS_SETTINGS_ATCOP_H
#define __IMS_SETTINGS_ATCOP_H

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

#define QC_SETTINGS_AT_CMD_VOIP_AMR_MODE_STR_LEN     25 /**< String length Limitation */
#define QC_SETTINGS_AT_CMD_VOIP_AMR_WB_MODE_STR_LEN  25 /**< String length Limitation */

#ifndef AEE_IMS_SUCCESS
#define AEE_IMS_SUCCESS  0
#endif

#ifndef AEE_IMS_EFAILED
#define AEE_IMS_EFAILED  1
#endif

#ifndef QPVOID
typedef void QPVOID;
#endif

/**
 * \enum eImsSeetingsATCmdType
 * \brief AT Commands enum
 *
 * ALL AT Commmand Request, Response enumeration
 */
typedef enum eImsSeetingsATCmdType{
  QC_IMS_SETTINGS_AT_CMD_NONE,                  //Internal not to be used
  /*REQUESTs*/
  QC_IMS_SETTINGS_AT_CMD_SET_VOIP_CONFIG_REQ = 0x0001,  /**< */  
  QC_IMS_SETTINGS_AT_CMD_GET_VOIP_CONFIG_REQ,         /**< */
  QC_IMS_SETTINGS_AT_CMD_REQ_MAX,               //Internal not to be used
  /*RESPONSEs*/
  QC_IMS_SETTINGS_AT_CMD_SET_VOIP_CONFIG_RSP = 0x1000,  /**< */  
  QC_IMS_SETTINGS_AT_CMD_GET_VOIP_CONFIG_RSP,          /**< */
  QC_IMS_SETTINGS_AT_CMD_RSP_MAX,               //Internal not to be used
}QC_IMS_SETTINGS_AT_CMD_TYPE;

/**
 * \enum eImsSettingsATCmdResponseTypes
 *
 * IMS SETTINGS AT Command Response Types
 */
typedef enum eImsSettingsATCmdResponseTypes{
  QC_IMS_SETTINGS_AT_CMD_RSP_SUCCESS,  /**< */
  QC_IMS_SETTINGS_AT_CMD_RSP_FAILURE,  /**< */
} QC_IMS_SETTINGS_AT_CMD_RSP_TYPES;

/*
  Structure related to VoIP configuration
*/
/**
 * \struct ims_settings_at_voip_configuration
 *
 * Structure related to VoIP configuration
 * Each parameter is valid only if the corresponding
 * parameter Include fag is set to 1
 * ex: Session Expires value in 'iSessionExpires' parameter
 * will be valid only if, iIncludeSessionExpires == 1.
 * Otherwise, value in 'iSessionExpires' is Invalid and Ignored.
 *
 * NOTE: Added to Support AT+VZWVOIPM Command.
 * Current implementation sets 'iRtpRtcpInactivityTimer' TLV to
 *  NV 67282 -  qipcall_rtp_link_aliveness_timer.
 * TLVs 'cAmrModeStr' and 'cAmrWbModeStr' are valid only for 
 * QC_IMS_SETTINGS_AT_CMD_SET_VOIP_CONFIG_REQ command.
 *
 */
typedef struct
{
  uint8  iIncludeSessionExpires;         /**< */
  uint16 iSessionExpires;                /**< */
                                         /**< */
  uint8  iIncludeMinSessionExpiry;       /**< */
  uint16 iMinSessionExpiry;              /**< */
                                         /**< */
  uint8  iIncludeAmrWbEnable;            /**< */
  uint8  iAmrWbEnable;                   /**< */
                                         /**< */
  uint8  iIncludeScrAmrEnable;           /**< */
  uint8  iScrAmrEnable;                  /**< */
                                         /**< */
  uint8  iIncludeScrAmrWbEnable;         /**< */
  uint8  iScrAmrWbEnable;                /**< */
                                         /**< */
  uint8  iIncludeAmrMode;                /**< */
  uint8  iAmrMode;                       /**< */
                                         /**< */
  uint8  iIncludeAmrWbMode;              /**< */
  uint16 iAmrWbMode;                     /**< */
                                         /**< */
  uint8  iIncludeRingingTimerValid;      /**< */
  uint16 iRingingTimer;                  /**< */
                                         /**< */
  uint8  iIncludeRingbackTimerValid;     /**< */
  uint16 iRingbackTimer;                 /**< */
                                         /**< */
  uint8  iIncludeRtpRtcpInactivityTimer; /**< */
  uint32 iRtpRtcpInactivityTimer;        /**<  Current implementation sets for RTP NV - 67282 -  qipcall_rtp_link_aliveness_timer only*/
                                         /**< */
  uint8  iIncludeAmrModeStr;             /**< */
  char   cAmrModeStr[QC_SETTINGS_AT_CMD_VOIP_AMR_MODE_STR_LEN + 1];/**< valid only for SET CMD */
                                         /**< */
  uint8  iIncludeAmrWbModeStr;           /**< */
  char   cAmrWbModeStr[QC_SETTINGS_AT_CMD_VOIP_AMR_WB_MODE_STR_LEN + 1];/**< valid only for SET CMD */
} ims_settings_at_voip_configuration;


/**
 * \Union ims_settings_at_cmd_data
 * 
 *
 * Union for AT command related data
 */
typedef union
{
  ims_settings_at_voip_configuration voipConfig; /**< VOIP Configuration Parameters */

  /* Additional configurations */
} ims_settings_at_cmd_data;

/**
 * \struct ims_settings_at_cmd_rsp_info
 * 
 * Structure AT Command Response.
 * Used to send the AT command response through 'ims_settings_at_cmd_rsp_callback'.
 */
typedef struct
{
  QC_IMS_SETTINGS_AT_CMD_TYPE eCmd;          /**< cmd to get/set NV items */
  QC_IMS_SETTINGS_AT_CMD_RSP_TYPES rspType; /**< AT Command Response type : Success/Failure */
  ims_settings_at_cmd_data  rspData;        /**< Data info for the AT command resp. Valid only if rsp_type = QC_IMS_SETTINGS_AT_CMD_RSP_SUCCESS */
  QPVOID *pRspCbUserData;                   /**< User Data provided in AT command request */
} ims_settings_at_cmd_rsp_info;

/**
 * \typedef ims_settings_at_cmd_rsp_callback
 *
 * Callback Function pointer for getting AT command result notification.
 * This function pointer can be set using ims_settings_at_command_req() API.
 *
 * at_cmd_rsp_info - AT command response information parameter
 */
typedef QPVOID (*ims_settings_at_cmd_rsp_callback)(ims_settings_at_cmd_rsp_info at_cmd_rsp_info);

/**
 * \struct ims_settings_at_cmd_req_info
 * 
 * Structure AT Command Request.
 */
typedef struct
{
  QC_IMS_SETTINGS_AT_CMD_TYPE eCmd;              /**< AT cmd type to get/set NV items */
  ims_settings_at_cmd_data  reqData;             /**< Data information for the AT command */
  ims_settings_at_cmd_rsp_callback pRspCallback; /**< Callback function pointer for cmd result notification */
  QPVOID *pRspCbUserData;                        /**< User data pointer : sent back in response callback function */
} ims_settings_at_cmd_req_info;

/************************************************************************
Function ims_settings_at_command_req()

Description
   API for sending the AT commands to IMS Settings.
   Response to the command will be notified using response callback
   function pointer provided in 'at_cmd_req_info'.

Parameters
   at_cmd_req_info - AT command request information

Dependencies
   None

Return Value
   AEE_IMS_SUCCESS if the command has been posted successfully for
   update, otherwise AEE_IMS_EFAILED is returned

Side Effects
   None
************************************************************************/
int ims_settings_at_command_req(ims_settings_at_cmd_req_info at_cmd_req_info);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // __IMS_SETTINGS_ATCOP_H