#===============================================================================
#
# Modem MCS API wrapper script
#
# GENERAL DESCRIPTION
#    build script to load API's for modem/mcs
#
# Copyright (c) 2012 by Qualcomm Technologies, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/wcdma/build/wcdma.api#1 $
#  $DateTime: 2015/01/27 06:42:19 $
#
#                      EDIT HISTORY FOR FILE
#                      
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#  
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 05/24/13   geg     FR 3383: Segment Loading
# 05/03/12   jgr     Add moved public files
# 02/04/11   rmsd    Added online path also which was missing earlier.
# 12/22/10   stk     Published WCDMA headers as restricted APIs.
# 12/15/10   stk     Moved LoadAPIUnits outside of if/else clause.
# 12/09/10   stk     Published WCDMA\API as restricted.
# 07/10/10   enj     Initial revision
#
#===============================================================================
Import('env')

if env.PathExists('${BUILD_ROOT}/wcdma'):
    env.Replace(WCDMA_ROOT = '${INC_ROOT}/wcdma')
else:    
    env.Replace(WCDMA_ROOT = '${INC_ROOT}/modem/wcdma')


env.PublishPublicApi('WCDMA', ['${WCDMA_ROOT}/api', ])
env.PublishPublicApi('PUBLIC', [ '${WCDMA_ROOT}/api/public' ])



env.PublishRestrictedApi('VIOLATIONS',[
        '${INC_ROOT}/utils/osys',
        '${INC_ROOT}/mcs/hwio/inc',
        '${INC_ROOT}/geran/cust/inc',
        '${INC_ROOT}/mcs/cust/inc',
        '${INC_ROOT}/mcs/hwio/inc/${CHIPSET}',
        '${INC_ROOT}/mcs/variation/inc',
        '${INC_ROOT}/geran/variation/inc',
        '${INC_ROOT}/datamodem/cust/inc',
        '${INC_ROOT}/core/buses/api/icb',
        '${INC_ROOT}/datamodem/variation/inc',
        '${INC_ROOT}/mmcp/cust/inc',
        '${INC_ROOT}/utils/fc/inc',
        '${INC_ROOT}/mmcp/mmode/cm/src',
        '${INC_ROOT}/mmcp/variation/inc',
        ])




# ------------------------------------------------------------------------------
# WCDMA Subsystem API
#-------------------------------------------------------------------------------
env.PublishRestrictedApi('WCDMA', ['${INC_ROOT}/modem/wcdma/common/inc',
                                  '${INC_ROOT}/modem/wcdma/l2/inc',
                                  '${INC_ROOT}/modem/wcdma/mac/inc',
                                  '${INC_ROOT}/modem/wcdma/rlc/inc',
                                  '${INC_ROOT}/modem/wcdma/rrc/inc',
				  '${INC_ROOT}/modem/wcdma/test/wplt/tgt',
                                ])
env.PublishPublicApi('WCDMA', ['${INC_ROOT}/modem/wcdma/api',
			       '${INC_ROOT}/modem/wcdma/api/public'
			       ])

env.PublishRestrictedApi('WCDMA', ["${INC_ROOT}/modem/wcdma/api"])

if env.has_key('USES_QDSP6'):
    env.PublishRestrictedApi('WCDMA', ['${INC_ROOT}/modem/wcdma/l1/offline/inc'])
else:
    env.PublishRestrictedApi('WCDMA', ['${INC_ROOT}/modem/wcdma/l1/online/inc'])
env.LoadAPIUnits()

