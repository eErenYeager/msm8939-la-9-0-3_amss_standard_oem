#ifndef RLCDAR_H
#define RLCDAR_H
/*===========================================================================

            D U P L I C A T E  A V I O D A N C E  &  R E - O R D E RI N G

                    R L C :  H E A  D E R  F I L E


GENERAL DESCRIPTION

  This module contains the declaration of the functions and data types
  defined in rlcdar.c.

Copyright (c) 1992-2005 by Qualcomm Technologies Incorporated.  All Rights Reserved.
Copyright (c) 2006,2008-2009 by Qualcomm Technologies Incorporated.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/wcdma/rlc/src/rlcdar.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when      who     what, where, why
--------  ---     -----------------------------------------------------------
07/13/12  grk     MBMS feature cleanup at L2(MBMS_DEBUG_1, FEATURE_MODEM_MBMS)
05/11/09  ssg     Updated Copyright Information
02/01/08   pj     Changed FEATURE_MBMS flag to FEATURE_MODEM_MBMS.
11/28/06  ssg     Created file.

===========================================================================*/

/*===========================================================================
** Includes and Public Data Declarations
**=========================================================================*/

/*---------------------------------------------------------------------------
** Include Files
**-------------------------------------------------------------------------*/
#include "wcdma_variation.h"
#include "rlcdl.h"


#endif  /* RLCDAR_H */
