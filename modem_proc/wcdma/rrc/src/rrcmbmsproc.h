#ifndef RRCMBMSPROC_H
#define RRCMBMSPROC_H
/*========================================================================================

                                    R R C  M B M S  P R O C E D U R E

DESCRIPTION

  This file contains the declaration of the interface functions and
  data types used by RRC MBMS procedure

  Copyright (c) 2005, 2007-2009 by Qualcomm Technologies Incorporated.  All Rights Reserved.
========================================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/wcdma/rrc/src/rrcmbmsproc.h#1 $    $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $
  
 when       who     what, where, why
--------    ---     ----------------------------------------------------------
12/31/10   rl       Merged with VU_MODEM_WCDMA_RRC.01.86.50
05/08/09    ss      Updated Copyright Information
01/19/08    ps      Made changes for ASn1 migration 25.331v7.9  
                    i.e.May 2008 version  (2008-05)  
03/27/08    vr      Added function prototype for rrcmbms_service_terminated_ind()
03/06/08    da      Changed FEATURE_MBMS to FEATURE_MODEM_MBMS
12/21/07    vr      Created file

===========================================================================*/
#include "rrccmd_v.h"


#endif


