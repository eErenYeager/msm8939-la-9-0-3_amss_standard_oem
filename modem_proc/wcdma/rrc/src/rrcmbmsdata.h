#ifndef RRCMBMSDATA_H
#define RRCMBMSDATA_H
/*========================================================================================

                                    R R C M B M S D A T A 

DESCRIPTION
  This file contains the declaration of the interface functions and
  data types used by RRC MBMS Procedures

  Copyright (c) 2006-2008 by Qualcomm Technologies Incorporated.  All Rights Reserved.
========================================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.
  
$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/wcdma/rrc/src/rrcmbmsdata.h#1 $    $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $  
 when       who     what, where, why
--------   ---     ----------------------------------------------------------
01/28/11   rl       Merged with VU_MODEM_WCDMA_RRC.01.89.57
12/31/10   rl       Merged with VU_MODEM_WCDMA_RRC.01.86.50
05/08/09    ss      Updated Copyright Information
03/06/08    da      Added MBMS phase 2C changes.  MBMS support in CELL_DCH.  
                    Includes mobility and dynamic resource management.
03/06/08    da      Changed FEATURE_MBMS to FEATURE_MODEM_MBMS
12/21/07    vr      Created file              

===========================================================================*/
#include "rrccmd_v.h"

#endif /* RRCMBMSDATA_H */


