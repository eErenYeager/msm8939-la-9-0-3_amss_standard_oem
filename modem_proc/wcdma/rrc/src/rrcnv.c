/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*


           U S E R   I N T E R F A C E   N V   M O D U L E

GENERAL DESCRIPTION

  This source file provides the NV interface functions for RRC.

EXTERNALIZED FUNCTIONS
  rrc_get_nv
    Get an item from NV, handle non-active items

  rrc_put_nv
    Write an item to NV

  rrc_replace_nv
    Replaces an item in NV

  rrc_free_nv
    Free an item from NV

INITIALIZATION AND SEQUENCING REQUIREMENTS

  Copyright (c) 2001-2009 Qualcomm Technologies INCORPORATED.
  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================
                        EDIT HISTORY FOR MODULE

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/wcdma/rrc/src/rrcnv.c#2 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
10/28/14   vi      Made changes to write .conf file only when its newly created / after NAND erase
10/05/14   sg      Made changes to disable EDRX and HSRACH DS concurrency capabilities overriding the NV values set.
10/16/14   sn      Changes to enable FR21174 and FR21035
10/15/14   sp     Made changes to enable DCHSDPA by default with DSDA concurrency
09/04/14   vi      Made changes to add NV control over meas id extn feature.
08/26/14   sr      Made changes to enable CPC by default in DSDS and DSDA modes
08/07/14   sr      Made changes to enable DACC by default in MSIM modes
06/23/14   vi      Made changes to skip SIB7 reading if SIB7 expiration timer is stil running
08/04/14   sp      Made changes to mainline DCH-FACH transition optimization changes07/07/14   vi      Made changes to set the default value of TSTS concurrency NV to enable DCHSDPA
07/02/14   sp      Made changes to set the default value of DSDS concurrency NV(72581) to enable DCHSDPA
06/24/14   sp      Made changes to enable integrigty and ciphering by default if the user has not written to NV
05/02/14   sa      Made changes to disable the default config in CELL FACH feature by default
04/25/14   vi      Created new NV to prioritize 850 band
03/28/14   vi      Made changes to support MOB framework
03/26/14   sp      Made changes to enable/disable feature support in DSDS/DSDA/TSTS modes through NV
03/25/14   sn      Changes to control SIB sleep before receiving SBs using NV
03/05/14   sn      Changes to control ASF timer with a NV 
02/24/14   sr      Added NV support for uplink compression capability
02/25/14   geg     Feature name FEATURE_WCDMA_DC_HSUPA_FRAME replacement with FEATURE_WCDMA_DC_HSUPA
02/13/14   vi      Added new NV rrc_constant_320ms_drx_nv
01/27/14   sr      Added NV control for MFBI support
01/03/14   sa      Made changes to print unconditional wcdma_nv_hsupa_category per L1
12/24/13   db      Introduced new NV to allow CPC,HSFACH,HSRACH,DC-HSDPA in DSDS and DSDA mode
12/13/13   sn      Manual PLMN search optimizations
12/03/13   mn      Moved all declarations to top of the code.
12/03/13   sp      Made changes to add nv support and to save time ticks for 
                   rr to/from rrc mm to/from rrc and to add msg_type for crash debug ota changes
11/15/13   as      Made changes to set default EUL NV category correctly
10/15/13   sr      Made changes to fix compilation errors
09/24/13   vi      Introduced new NV for FE-FACH
08/28/13   vi      Introduced new NV for GANSS feature
08/21/13   gv      Enabling PPAC NV by default
08/13/13   vi      Fixed buffer overflow case while doing strlcpy
07/18/13   vi      Made changes to pass correct data type for rrc_fast_return_to_lte_after_csfb_timer_nv
06/19/13   vi      Made changes to pass correct data type for rrc_nv_freq_lock_item 
05/30/13   gv      Made changes to perform acq db only scan when requested so by higher layers
05/15/13   db      Made changes to enhance NV 67333 to control FDPCH and CPC DTX/DRX features independently
05/10/13   gv      Made changes to modify the default band priority
05/09/13   sn      Reduce fine/raw scan skip range for manual CSG search
05/07/03   vi      Removing feature FEATURE_WCDMA_RRC_SINGLE_CONF_FILE
05/01/13   vi      Fixed compiler warnings
04/30/13   vi      Made changes to use single wcdma_rrc_external.conf file for nv items
04/23/13   vi      Made default value of wcdma_rrc_wtol_tdd_ps_ho_support_nv to TRUE
04/19/13   vi      Made changes to remove IRAT bringup changes
02/21/13   sa      Made the changes to mainline the FEATURE_THIN_UI feature
12/07/12   sn      CSG feature changes
11/29/12   mp      Made chagnes to ignore meas reports for PSC lock under NV
11/02/12   gv      Corrected the changes under FEATURE_WCDMA_DIME_SW
11/02/12   sg      Made changes to remove the nv "rrc_nv_a2_power_opt" and remove
                   remove the code related to HW integrity protection.  
10/16/12   sr      Made changes to disable W2L measurements both in idle and connected mode through NV item
07/24/12   geg     Upmerge WCDMA.MPSS.1.0 to WCDMA.MPSS.2.0
03/28/12   zr      Adding feature definitions for Dime
07/03/12   sks     Made changes to support NV item for wtol_TDD ps_ho
09/05/12   md      Mainlined feature flags FEATURE_RRC_INTEGRITY_SW_ENGINE,
                   FEATURE_WCDMA_A2_POWER_COLLAPSE and FEATURE_WCDMA_NIKEL_SW
05/02/12   rd      Made changes to support NV item for wtol_ps_ho
04/02/12   db      Made changes to disable CPC and FDPCH through NV
04/10/12   pm      Added code to support the band priority nv
02/24/12   sn      Changes to periodically save ACQ DB in NV
03/09/12   gv      Provided NV support to enable/disable PPAC functionality
                   and also to use existing conf file to create new NV items
01/02/12   pm      Added rrc_wtol_cm_support_nv to put FEATURE_WCDMA_CM_LTE_SEARCH feature under nv check 
11/17/11   ad      Added featurization for A2 power optimization
11/15/11   sks     Made the changes for A2 power optimization.
10/31/11   sks     Added support for FEATURE_WCDMA_FAST_RETURN_TO_LTE_AFTER_CSFB.
09/16/11   rl      Made changes to remove the AEEstd library functions 
                   to CoreBsp secured function cal
07/05/11   rl      Merged with VU_MODEM_WCDMA_RRC.01.101.00
06/29/11   su      Added code for NV support to enable/disable
                   the FEATURE_RRC_DO_NOT_FORWARD_PAGE.
05/10/11   rl      Merged with VU_MODEM_WCDMA_RRC.01.97.50
05/05/11   vg      added a new NV support for channal locking feature 
05/03/11   su      Added code changes to provide NV support to enable/disable
	           FEATURE_3GPP_CSFB_SKIP_OPT_WCDMA_SIBS.
03/29/11   rl      Merged with VU_MODEM_WCDMA_RRC.01.94.50
03/28/11   su      Made changes to set SRB2 suspend_offset based on 
                   rrc_set_srb2_act_time_nv item.
03/16/11   su      Added code changes to NV item support to enable/disable support
                   for snow3g_security_algo.
03/14/11   rl      Merged with VU_MODEM_WCDMA_RRC.01.93.50
03/11/11   rl      Fixed the lint warning
03/09/11   su      Made changes to mainline code under FEATURE_WCDMA HSUPA 2MS TTI.
03/08/11   su      Added code to support enabling and disabling of 
                   FEATURE_UEUT_IGNORE_CELL_BAR_RESERVE_STATUS using NV item.
03/07/11   su      Added code to support device type NV.
05/10/09   kp      Added support for demand paging RRC code using pragma.
05/08/09   ss      Updated Copyright Information
04/29/09   ss      Made changes to compiler warnings on gcc compilers.
03/30/09   ps      Removed changes to enable/disable HE field usingthe NV flag
                   NV_WCDMA_TEST_NV_1_I
03/26/09   ps      Made changes to include hw.h instead of hw_api.h, as hw_api.h
                   will be removed for some of the targets  
02/09/09   ps      Made changes to disbale enable HE field on UL based on
                   NV flag NV_WCDMA_TEST_NV_1_I
10/23/08   ps      Made changes to support FEATURE_WCDMA_TRIM_CAPABILITY
02/17/08   sm      Added function rrc_get_hsupa_category() which is called by L1
                   to get the hsupa category
02/01/08   sm      Modified default value of nv_hsupa_category when feature 
                   FEATURE_WCDMA HSUPA 2MS TTI is defined
10/08/07   da      Added support for reading HSUPA+CM NV item
08/29/06   sm/da   Made changes required for R6 ASN.1 migration.  Also merged
                   FEATURE WCDMA HSUPA, FEATURE_NV WCDMA_OPTIONAL_FEATURE_LIST_SUPPORT,
                   FEATURE_NV_WCDMA HSUPA CATEGORY
04/17/06   vk      Added support for reading NV Item HSDPA_COMPRESSED_MODE_ENABLED
                   under FEATURE HSDPA
03/28/06   vk      Added support for reading hsdpa_cat NV Item
08/31/05   vm      Added support for reading two new NV items: wcdma_dl_freq and
                   wcdma_dl_freq_enabled.
12/23/04   vn      Fixed lint error related to initialization of read_index
08/27/02   kc      Initialized Security Related NV Items to FALSE if user has
                   not written to NV RAM.
08/02/02   kc      Added support to read Integrity, Ciphering, Fake Security
                   Enabled/Disabled flags from NV
05/10/02   bu      Removed rrc_wait_nv(). Now we call the rrc_event_wait().
02/22/02   bu      Added case to assign default value to NV_ACQ_LIST_4_I in
                   rrc_get_nv().
02/20/02   bu      Added cases to give default values to 3 NV_ACQ_LIST items
                   in rrc_get_nv().
01/23/02   bu      Created a new file with NV routines.

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

#include "wcdma_variation.h"
#include <stringl/stringl.h>

#include "err.h"
#include "nv.h"
#include "fs_async_put.h"
#include "rrcsigs.h"
#include "dog.h"
#include "rrccspdb.h"
#include "nv_items.h"
#include "rrcdispatcher.h"
#include "rrcnv.h"
#include "fs_fcntl.h"
#include "fs_public.h"
#include "fs_sys_types.h"
#include "string.h"
#include "fs_lib.h"
#include "rrcsibproc.h"



/*===========================================================================

            LOCAL DEFINITIONS AND DECLARATIONS FOR MODULE

This section contains local definitions for constants, macros, types,
variables and other items needed by this module.


===========================================================================*/
/* -----------------------------------------------------------------------
** Local Object Definitions
** ----------------------------------------------------------------------- */

static  nv_cmd_type nvi;           /* nv command buffer */


/* variable to indicate support of feature list supported.
 * This variable will be set using the value of NV item 'NV_WCDMA_OPTIONAL_FEATURE_LIST_I'
 * WCDMA_NV_OPTIONAL_FEATURE_HSDPA 0x1<<1 
 * WCDMA_NV_OPTIONAL_FEATURE_HSUPA 0x1<<2
 * WCDMA_NV_OPTIONAL_FEATURE_MBMS 0x1<<3
 * WCDMA_NV_OPTIONAL_FEATURE_RX_DIVERSITY 0x1<<4
 * WCDMA_NV_OPTIONAL_FEATURE_PS_DATA_HO 0x1<<5
 * all other bits are spare bits - for future use
 */
uint32 wcdma_nv_optional_feature_list;

/*
   Flag to find if NV .conf file is newly created
   If file is newly created then we have to write item file paths to this file otherwise we should not write.
   This is added to avoid writing duplicate entries to conf file when as part of MBN reload item files gets deleted  and phone is rebooted without NAND erase.
   When conf file is already present then we expect all the item file paths to be present in conf file.
*/
boolean rrc_is_conf_file_new = FALSE;

/* variable to store the hsupa category.
 * This variable will be set using the value of NV item 'NV_WCDMA_HSUPA_CATEGORY_I'
 */
uint16 wcdma_nv_hsupa_category = RRC_WCMDA_HSUPA_DEFAULT_CATEGORY;

/* Boolean to control HSUPA + CM feature enabling\disabling via NV item*/
boolean rrc_wcdma_hsupa_cm_enabled = FALSE;
/* Boolean to enable/disable DCH_FACH optimization. */
boolean feature_dch_fach_opt_enabled = TRUE;


#define RRC_NV_CONF_FILE_PERMISSIONS_COMMON 0777

char rrc_nv_device_type_conf_item[] = "/nv/item_files/Thin_UI/enable_thin_ui_cfg";
uint8 rrc_device_type_nv = 0;


char rrc_nv_ignore_bar_conf_item[] = "/nv/item_files/wcdma/rrc/wcdma_rrc_ignore_cell_bar_reserve_status";
uint8 rrc_ignore_cell_bar_nv = 0;

#ifdef FEATURE_WCDMA_SNOW3G_SECURITY
char rrc_nv_snow3g_algo_conf_item[] = "/nv/item_files/wcdma/rrc/wcdma_rrc_snow3g_enabled";
extern boolean snow3g_security_algo_supported;
#endif

#ifdef FEATURE_SMC_SRB2_SUSPENSION
char rrc_nv_set_srb2_act_conf_item[] = "/nv/item_files/wcdma/rrc/wcdma_rrc_set_srb2_act_time";
uint8 rrc_set_srb2_act_time_nv = 0;
#endif


char rrc_nv_csfb_skip_sib11_opt_item[] = "/nv/item_files/wcdma/rrc/wcdma_rrc_csfb_skip_sib11_opt";
uint8 rrc_csfb_skip_sib11_opt_nv = 0;

char rrc_nv_freq_lock_item[] = "/nv/item_files/wcdma/rrc/wcdma_rrc_freq_lock_item"; 
uint16 rrc_nv_channel_lock_item_value = 0; 

char rrc_nv_do_not_forward_page_item[] = "/nv/item_files/wcdma/rrc/wcdma_rrc_do_not_forward_page";
uint8 rrc_do_not_forward_page_nv = 0;

#ifdef FEATURE_WCDMA_FAST_RETURN_TO_LTE_AFTER_CSFB

char *rrc_nv_fast_return_to_lte_after_csfb_item = "/nv/item_files/wcdma/rrc/wcdma_rrc_fast_return_to_lte_after_csfb";
boolean rrc_fast_return_to_lte_after_csfb_nv = FALSE;

char *rrc_nv_fast_return_to_lte_after_csfb_timer_item = "/nv/item_files/wcdma/rrc/wcdma_rrc_fast_return_to_lte_after_csfb_timer";
uint16 rrc_fast_return_to_lte_after_csfb_timer_nv = 2000;

#endif

char rrc_nv_external_file[] = "/nv/item_files/conf/wcdma_rrc_external.conf";
char rrc_nv_wtol_cm_item[] = "/nv/item_files/wcdma/rrc/wcdma_rrc_wtol_cm_support"; 
boolean rrc_wtol_cm_support_nv = 1;

char rrc_nv_ppac_support_item[] = "/nv/item_files/wcdma/rrc/wcdma_ppac_support";
boolean rrc_ppac_support_nv = TRUE;

char rrc_save_acqdb_item[] = "/nv/item_files/wcdma/rrc/wcdma_rrc_save_acqdb";
boolean rrc_save_acqdb_nv = FALSE;

char rrc_disable_cpc_fdpch_item[] = "/nv/item_files/wcdma/rrc/wcdma_rrc_disable_cpc_fdpch";
uint8 rrc_disable_cpc_fdpch_nv = 0;

char rrc_disable_crash_debug_info_item[]="/nv/item_files/wcdma/rrc/wcdma_rrc_disable_crash_debug_info";
uint8 rrc_disable_crash_debug_info_nv = 0;

#ifdef FEATURE_WCDMA_DEBUG_ATTEMPT_FULL_SRCH
/* NV ID for PSC lock: 70241  */
char rrc_enable_psc_lock_item[] = "/nv/item_files/wcdma/rrc/wcdma_rrc_enable_psc_lock";
boolean rrc_enable_psc_lock_nv = FALSE;
#endif

/* If NV item not set yet, we default to Bands */
rrc_nv_band_priority_config_type  rrc_nv_band_priority_config =
{
   {0x0001, 0x0400, 0x0008, 0x0010, 0x0020, 0x0040, 0x0080, 0x0100, 0x0200, 0x0004}
};

/* EFS files to manage band priority configuration NV items. */
static char *rrc_band_priority_config_item = "/nv/item_files/wcdma/rrc/wcdma_rrc_band_priority_config";

char wcdma_rrc_wtol_ps_ho_support_item[] = "/nv/item_files/wcdma/rrc/wcdma_rrc_wtol_ps_ho_support";
boolean wcdma_rrc_wtol_ps_ho_support_nv = TRUE;

char wcdma_rrc_wtol_tdd_ps_ho_support_item[] = "/nv/item_files/wcdma/rrc/wcdma_rrc_wtol_tdd_ps_ho_support";
boolean wcdma_rrc_wtol_tdd_ps_ho_support_nv = TRUE;

#ifdef FEATURE_FEMTO_CSG
  char wcdma_rrc_csg_prune_count_item[] = "/nv/item_files/wcdma/rrc/wcdma_rrc_csg_prune_counter";
  uint32 wcdma_rrc_csg_max_prune_count_nv = 8640; /* Number of ASF searches in one month */

  /*If enabled manual CSG search considers minimum spacing between two carriers as 2.5MHz otherwise 4.4MHz*/
  /* NV# 70363 */
  char wcdma_rrc_csg_srch_carr_item[] = "/nv/item_files/wcdma/rrc/wcdma_csg_srch_carrier_space";
  boolean wcdma_csg_srch_carrier_space_nv = FALSE;


  /* 1 --> 1min,  2--> 2min ...  4 --> 4min 0 --> 5min*/
  /*NV#72574*/
  char wcdma_asf_timer_item[] = "/nv/item_files/wcdma/rrc/wcdma_asf_timer";
  uint8 wcdma_asf_timer_nv = 0;
#endif

  char wcdma_rrc_freq_scan_timer_item[] = "/nv/item_files/wcdma/rrc/wcdma_rrc_freq_scan_timer_in_ms";
  uint32 wcdma_rrc_freq_scan_timer_nv = 1800000; /* This defines the max time (default - 30 mins) for which Frequency scan should not be initiated */

  char wcdma_rrc_deep_sleep_no_svc_timer_item[] = "/nv/item_files/wcdma/rrc/wcdma_rrc_deep_sleep_no_svc_timer_in_ms";
/* This defines the max time for which service requests should not be entertained on finding no service
 * for a previous service request in deep sleep and empty available plmns.
 */
  uint32 wcdma_rrc_deep_sleep_no_svc_timer_nv = 10000;

  char wcdma_rrc_bplmn_freq_scan_timer_item[] = "/nv/item_files/wcdma/rrc/wcdma_rrc_bplmn_freq_scan_timer_in_ms";
/* This defines the max time (default - 15 mins) for which BPLMN Frequency scan should not be initiated */
  uint32 wcdma_rrc_bplmn_freq_scan_timer_nv = 900000;

#ifdef FEATURE_GANSS_SUPPORT
  char rrc_ganss_support_item[] = "/nv/item_files/wcdma/rrc/rrc_ganss_support_nv";
  boolean rrc_ganss_support_nv = FALSE;
#endif

char rrc_dormancy_support_item[] = "/nv/item_files/wcdma/rrc/wcdma_rrc_dormancy_support";

/*NV value  :  behavior 
              1  :  support both legacy and rel8 fast dormancy 
              2  :  support only rel8 fast dormancy
              3  :  Disable complete fast dormancy
*/
uint8 rrc_dormancy_support_nv = RRC_NV_DORMANCY_ENABLED;
  /*If enabled WRRC starts SIB sleep before receiving Scheduling blocks*/
  char wcdma_rrc_sib_sleep_sb_item[] = "/nv/item_files/wcdma/rrc/wcdma_rrc_sib_sleep_sb";
  boolean wcdma_rrc_sib_sleep_sb_nv = TRUE;

#ifdef FEATURE_WCDMA_ABSOLUTE_PRIORITY_FACH
  char rrc_fe_fach_support_item[] = "/nv/item_files/wcdma/rrc/rrc_fe_fach_support";
  uint8 rrc_fe_fach_support_nv = 0;
#endif

#ifdef FEATURE_DUAL_SIM

/*NV72581*/
char rrc_dsds_concurrency_nv_item[]="/nv/item_files/wcdma/rrc/rrc_dsds_concurrency_extended";
uint16 rrc_dsds_concurrency = 0;

/*NV72582*/
char rrc_dsda_concurrency_nv_item[]="/nv/item_files/wcdma/rrc/rrc_dsda_concurrency_extended";
uint16 rrc_dsda_concurrency = 0;

/*NV72583*/
#ifdef FEATURE_TRIPLE_SIM
char rrc_tsts_concurrency_nv_item[]="/nv/item_files/wcdma/rrc/rrc_tsts_concurrency_extended";
uint16 rrc_tsts_concurrency = 0;
#endif /*FEATURE_TRIPLE_SIM*/
#endif /*FEATURE_DUAL_SIM*/

#ifdef FEATURE_WCDMA_MULTI_FREQ_BAND_INDICATOR
  /* NV# 72548*/
  char rrc_mfbi_support_item[] = "/nv/item_files/wcdma/rrc/rrc_nv_enable_mfbi_support";
  boolean rrc_mfbi_support_nv = 0;
#endif

  char rrc_constant_320ms_drx_item[] = "/nv/item_files/wcdma/rrc/rrc_constant_320ms_drx";
  boolean rrc_constant_320ms_drx_nv = FALSE;

#ifdef FEATURE_WCDMA_UL_COMPR
  /* NV# 72576 */
  char rrc_ul_comp_support_item[] = "/nv/item_files/wcdma/rrc/rrc_ul_compr_cap_support";
  boolean rrc_ul_compr_cap_nv = 1;
#endif
  char rrc_prioritize_850_band_item[] = "/nv/item_files/wcdma/rrc/wcdma_rrc_prioritize_850_band";
  boolean rrc_prioritize_850_band_nv = FALSE;

#ifdef FEATURE_WCDMA_DEFAULT_CFG_CELLFACH
/*NV #72526*/
char rrc_default_cfg_in_cell_fach_nv_item[]="/nv/item_files/wcdma/rrc/rrc_dsds_concurrency";
/*Support HS-FACH and CPC by default*/
uint8 rrc_default_cfg_in_cell_fach_nv = 0 ;
#endif
  char rrc_sib7_time_item[] = "/nv/item_files/wcdma/rrc/rrc_sib7_exp_time";
  uint32 rrc_sib7_time_nv = 0;

  char rrc_enable_meas_id_extn_item[] = "/nv/item_files/wcdma/rrc/rrc_enable_measurement_id_extn";
  extern boolean rrc_meas_id_extn_support;

  /*Bit mask to for RRC idle features*/
  /*Refer to rrcnv.h for details*/
  uint32 wcdma_rrc_idle_features = 0x0C;
  char wcdma_rrc_idle_features_item[] = "/nv/item_files/wcdma/rrc/wcdma_rrc_idle_feature";
/* -----------------------------------------------------------------------
** Forward Declarations
** ----------------------------------------------------------------------- */



/*===========================================================================
** -----------------------------------------------------------------------------------
**-- ----------NON-Demand paging section Srart--------------------------------------------
** -----------------------------------------------------------------------------------
  This section will have code section that will not be demand paged. Function which should be in this section are
  -RRC functions that are called by L1/L2 which are higher priority then RRC
  -RRC functions that are called in ISR context or RTOS timer call back
  -RRC functions that indirectly gets called by L1/L2 /ISR/timer call back   
  For example: Fun1() gets called by L1. Fun2() is called by Fun1(). Now both  Fun1() & Fun2() should be NON demand paged, 
  so both should be added in this section  
  Usage of pragma 
  __WCDMA_RRC_CODE_SEGMENT_NON_DEMANDPAGED__
  void foo(void)
  {
    function body here
  }
        __WCDMA_RRC_CODE_SEGMENT_NON_DEMANDPAGED_END__
  If you have another function to be added in the same section then again it needs to be wrapped with these pragma 
  for example:- function bar() is just below function foo() then function bar() needs to be written like this �. 
  __WCDMA_RRC_CODE_SEGMENT_NON_DEMANDPAGED__
  void bar(void)
  {
    function body here
  }
  __WCDMA_RRC_CODE_SEGMENT_NON_DEMANDPAGED_END__
** ----------------------------------------------------------------------- */


/*=========================================================================

FUNCTION     : rrc_get_hsupa_category

DESCRIPTION  : RRC returns the HSUPA Category to L1. RRC reads NV to determine 
               UE CAT (valid NV value, invalid NV value, NV not present, 
               NV reading failure)  

DEPENDENCIES : None

RETURN VALUE : HSUPA Category value 1..6

SIDE EFFECTS : None

=========================================================================*/
/***/ __WCDMA_RRC_CODE_SEGMENT_NON_DEMANDPAGED__ /***/
uint16 rrc_get_hsupa_category (void)
{
  return(wcdma_nv_hsupa_category);
}
/***/ __WCDMA_RRC_CODE_SEGMENT_NON_DEMANDPAGED_END__ /***/

/*===========================================================================
FUNCTION RRC_GET_NV

DESCRIPTION
  Get an item from the nonvolatile memory.  Handles nonactive items by
  providing a default value.

RETURN VALUE
  The NV return code, except for NV_NOTACTIVE_S, which is handled
  internally.

DEPENDENCIES
  This routine is not reentrant.  Shouldn't be a problem, as it doesn't exit
  till we're done, and it's only called from the RRC task.
===========================================================================*/
nv_stat_enum_type rrc_get_nv(
  nv_items_enum_type item,        /* which item */
  nv_item_type *data_ptr          /* pointer to space for item */
)
{
  uint8 i;

  nvi.tcb_ptr = rex_self();          /* notify this task when done */
  nvi.sigs = RRC_NV_SIG;
  nvi.done_q_ptr = NULL;             /* command goes on no queue when done */

  nvi.item = item;                 /* item to read */
  nvi.cmd = NV_READ_F;

  /* Set up NV so that it will read the data into the correct location */
  nvi.data_ptr = data_ptr;


  /* Clear the return signal, call NV, and wait for a response */
  (void) rex_clr_sigs( rex_self(), RRC_NV_SIG );
  nv_cmd( &nvi );
  rrc_event_wait(RRC_NV_SIG);

/* Handle a NV_NOTACTIVE_S or NV_FAIL_S status internally by replacing
** the random data returned with a default value of our own.  Items that
** share the same structure are lumped together in the switch.
*/

  if( nvi.status == NV_NOTACTIVE_S || nvi.status == NV_FAIL_S)
  {  
    nvi.status = NV_DONE_S;

    switch( nvi.item )
    {           /* provide our own values */
      case NV_ACQ_DB_I:       /* stored ACQ database */
        WRRC_MSG1_HIGH("NV Item status was %d for NV_ACQ_DB_I", nvi.status); 

        /* Initialize Current Read Index to -1 */
        data_ptr->acq_db.curr_rd_index = ~0;

        /* Initialize Current Number of Pointers to 0 */
        data_ptr->acq_db.curr_wr_index = 0;

        data_ptr->acq_db.last_entry_invalid = TRUE;

        /* Initialize all values in ordered acquisition list in acq database to
        */
        for (i=0; i < MAX_ACQ_DB_ENTRIES; i++)
        {
          data_ptr->acq_db.acq_list_indices[i] = i;
        }
        break;

      case NV_ACQ_LIST_1_I:       /* ACQ List 1 item */
        WRRC_MSG1_HIGH("NV Item status was %d for NV_ACQ_LIST_1_I", nvi.status); 
        break;

      case NV_ACQ_LIST_2_I:       /* ACQ List 2 item */
        WRRC_MSG1_HIGH("NV Item status was %d for NV_ACQ_LIST_2_I", nvi.status); 
        break;

      case NV_ACQ_LIST_3_I:       /* ACQ List 3 item */
        WRRC_MSG1_HIGH("NV Item status was %d for NV_ACQ_LIST_3_I", nvi.status); 
        break;

      case NV_ACQ_LIST_4_I:       /* ACQ List 4 item */
        WRRC_MSG1_HIGH("NV Item status was %d for NV_ACQ_LIST_4_I", nvi.status); 
        break;

      case NV_RRC_INTEGRITY_ENABLED_I:       /* INTEGRITY_ENABLED item */
       /*set the values to TRUE to enable integrigty by default as the user has not written to NV*/
        data_ptr->rrc_integrity_enabled = TRUE;
        WRRC_MSG1_HIGH("NV Item status was %d for INTEGRITY", nvi.status); 
        break;

      case NV_RRC_CIPHERING_ENABLED_I:       /* CIPHERING_ENABLED item */
       /*set the values to TRUE to enable ciphering by defuault as the user has not written to NV*/
        data_ptr->rrc_ciphering_enabled = TRUE;
        WRRC_MSG1_HIGH("NV Item status was %d for CIPHERING ", nvi.status); 
        break;

      case NV_RRC_FAKE_SECURITY_ENABLED_I:   /* FAKE_SECURITY_ENABLED item */
       /*set the values to FALSE as the user has not written to NV*/
        data_ptr->rrc_fake_security_enabled = FALSE;
        WRRC_MSG1_HIGH("NV Item status was %d for FAKE_SECURITY", nvi.status); 
        break;

      case NV_WCDMA_DL_FREQ_ENABLED_I:       /*  WCDMA_DL_FREQ_ENABLED item */
        data_ptr->wcdma_dl_freq_enabled = FALSE;
        WRRC_MSG1_HIGH("NV Item status was %d for NV_WCDMA_DL_FREQ_ENABLED_I", nvi.status); 
        break;

      case NV_WCDMA_DL_FREQ_I:              /*  WCDMA_DL_FREQ item */
        WRRC_MSG1_HIGH("NV Item status was %d for NV_WCDMA_DL_FREQ_I", nvi.status); 
        break;

      case NV_HSDPA_CAT_I:
        WRRC_MSG1_HIGH("NV Item status was %d for NV_HSDPA_CAT_I", nvi.status); 
        break;
      case NV_HSDPA_COMPRESSED_MODE_ENABLED_I:
        WRRC_MSG1_HIGH("NV Item status was %d for NV_HSDPA_COMPRESSED_MODE_ENABLED_I", 
                 nvi.status); 
        break;

      default:
        WRRC_MSG1_HIGH( "Nonactive NV item 0x%x", nvi.item);
        nvi.status = NV_NOTACTIVE_S;
        break;
    }
  }
  else
  {
    if( nvi.status != NV_DONE_S )
    {
      WRRC_MSG2_ERROR( "NV Read Failed Item %d Code %d", nvi.item, nvi.status);
    }
  }

  return( nvi.status );
}


/*===========================================================================
FUNCTION RRC_PUT_NV

DESCRIPTION
  Write an item to NV memory.  Wait till write is completed.

RETURN VALUE
  The NV Return Code

DEPENDENCIES
  This routine is not reentrant.  Shouldn't be a problem, as it doesn't exit
  till we're done, and it's only called from the RRC task.
===========================================================================*/
nv_stat_enum_type rrc_put_nv(
  nv_items_enum_type item,        /* which item */
  nv_item_type *data_ptr          /* pointer to data for item */
)
{

  nvi.tcb_ptr = rex_self();        /* Notify this task when done */
  nvi.sigs = RRC_NV_SIG;
  nvi.done_q_ptr = NULL;           /* command goes to no queue when done */

  nvi.item = item;                 /* item to write */
  nvi.cmd = NV_WRITE_F;

  nvi.data_ptr =  data_ptr;        /* the data to write */


  /* Clear the signal, call NV, wait for it to finish */
  (void) rex_clr_sigs( rex_self(), RRC_NV_SIG );
  nv_cmd( &nvi );
  rrc_event_wait(RRC_NV_SIG);
  
  if( nvi.status != NV_DONE_S )
  {
    WRRC_MSG2_ERROR( "NV Write Failed Item %d Code %d", nvi.item, nvi.status);
  }

  return( nvi.status );

}

/*===========================================================================
FUNCTION RRC_QUEUE_NV_WRITE

DESCRIPTION
  Posts a NV write request to async_efs_put

RETURN VALUE
  Zero if request is successful otherwise -1

DEPENDENCIES
===========================================================================*/
uint32 rrc_queue_nv_write(
  nv_items_enum_type item,        /* which item */
  nv_item_type *data_ptr,          /* pointer to data for item */
  uint32 size
)
{

  char f_name[120];
  uint32 status = -1;

  (void)snprintf(f_name,sizeof(f_name),"/nvm/num/%d",item);
  
  status = efs_async_put(f_name, (void*)data_ptr, size,O_CREAT|O_AUTODIR, 0777);

  return status;
}

/*===========================================================================
FUNCTION RRC_REPLACE_NV

DESCRIPTION
  Replace an item to NV memory.  Wait till write is completed.

RETURN VALUE
  The NV Return Code

DEPENDENCIES
  This routine is not reentrant.  Shouldn't be a problem, as it doesn't exit
  till we're done, and it's only called from the RRC task.
===========================================================================*/
nv_stat_enum_type rrc_replace_nv(
  nv_items_enum_type item,        /* which item */
  nv_item_type *data_ptr          /* pointer to data for item */
)
{

  nvi.tcb_ptr = rex_self();        /* Notify this task when done */
  nvi.sigs = RRC_NV_SIG;
  nvi.done_q_ptr = NULL;           /* command goes to no queue when done */

  nvi.item = item;                 /* item to write */
  nvi.cmd = NV_REPLACE_F;

  nvi.data_ptr =  data_ptr;        /* the data to write */


  /* Clear the signal, call NV, wait for it to finish */
  (void) rex_clr_sigs( rex_self(), RRC_NV_SIG );
  nv_cmd( &nvi );
  rrc_event_wait(RRC_NV_SIG);
  
  if( nvi.status != NV_DONE_S )
  {
    WRRC_MSG2_ERROR( "NV Replace Failed Item %d Code %d", nvi.item, nvi.status);
  }

  return( nvi.status );
}

/*===========================================================================
FUNCTION RRC_FREE_NV

DESCRIPTION
  Free an item in NV memory.  Wait till free is completed.

RETURN VALUE
  The NV Return Code

DEPENDENCIES
  This routine is not reentrant.  Shouldn't be a problem, as it doesn't exit
  till we're done, and it's only called from the RRC task.
===========================================================================*/
nv_stat_enum_type rrc_free_nv(
  nv_items_enum_type item,        /* which item */
  nv_item_type *data_ptr          /* pointer to data for item */
)
{

  nvi.tcb_ptr = rex_self();        /* Notify this task when done */
  nvi.sigs = RRC_NV_SIG;
  nvi.done_q_ptr = NULL;           /* command goes to no queue when done */

  nvi.item = item;                 /* item to free */
  nvi.cmd = NV_FREE_F;

  nvi.data_ptr =  data_ptr;        /* the data to write */

  /* Clear the signal, call NV, wait for it to finish */
  (void) rex_clr_sigs( rex_self(), RRC_NV_SIG );
  nv_cmd( &nvi );
  rrc_event_wait(RRC_NV_SIG);
  
  if( nvi.status != NV_DONE_S )
  {
    WRRC_MSG2_ERROR( "NV Free Failed Item %d Code %d", nvi.item, nvi.status);
  }

  return( nvi.status );

}


/*===========================================================================

FUNCTION rrc_read_nv_wcdma_optional_feature_list

DESCRIPTION
  This  function gets the NV item NV_WCDMA_OPTIONAL_FEATURE_LIST_I and stores it in 
  rrc global variable 'wcdma_nv_optional_feature_list'

DEPENDENCIES
  None.

RETURN VALUE
  None.

===========================================================================*/
void rrc_read_nv_wcdma_optional_feature_list
(
  void
)
{

#ifdef FEATURE_NV_WCDMA_OPTIONAL_FEATURE_LIST_SUPPORT
  #error code not present
#else
  wcdma_nv_optional_feature_list = WCDMA_NV_OPTIONAL_FEATURE_ALL_SUPPORTED;
  WRRC_MSG0_HIGH("FEATURE_NV_WCDMA_OPTIONAL_FEATURE_LIST_SUPPORT not defined. ");
#endif /* FEATURE_NV_WCDMA_OPTIONAL_FEATURE_LIST_SUPPORT */
}  

/*===========================================================================

FUNCTION rrc_read_nv_wcdma_hsupa_category

DESCRIPTION
  This  function gets the NV item NV_WCDMA_HSUPA_CATEGORY_I and stores it in 
  rrc global variable 'wcdma_nv_hsupa_category'

DEPENDENCIES
  None.

RETURN VALUE
  None.

===========================================================================*/
void rrc_read_nv_wcdma_hsupa_category
(
  void
)
{
  uint8 local_hsupa_category = 0;

  /*read wcdma rrc release indicator from NV*/
  if ( NV_DONE_S ==  (rrc_get_nv( NV_WCDMA_HSUPA_CATEGORY_I, 
                                  (nv_item_type *) &local_hsupa_category)))
  {
    /* check for valid hsupa category in NV item - 
       this validity may differ from target to target. Need to take care of this check
       later by adding appropriate check for new target or 2ms support */
    if ((local_hsupa_category < 1) || (local_hsupa_category > 6))
    {
#ifdef FEATURE_WCDMA_DC_HSUPA
      if (local_hsupa_category > RRC_WCMDA_HSUPA_DEFAULT_CATEGORY)
      {
        local_hsupa_category = MAX_DC_HSUPA_CATEGORY;
      }
      else
#endif
      {     
        WRRC_MSG1_ERROR("RRCEUL: HSUPA category NV (nv item: 4210) contains invalid category(%d). set to default",
          local_hsupa_category);
        local_hsupa_category = RRC_WCMDA_HSUPA_DEFAULT_CATEGORY;
      }

    }
    WRRC_MSG1_HIGH("RRCEUL: NV_WCDMA_HSUPA_CATEGORY_I(nv item: 4210) is set to %d", 
      local_hsupa_category);
  }
  else
  {
    /* As per current assumption value 1 indicates REL5,
       and default value is for REL5 */    
    local_hsupa_category = RRC_WCMDA_HSUPA_DEFAULT_CATEGORY;
    WRRC_MSG0_ERROR("RRCEUL: Read failed for NV_WCDMA_HSUPA_CATEGORY_I(nv item: 4210), set default to 5");
  }
  
  /* use stored nv variable value to set rrc global variable */
  wcdma_nv_hsupa_category = local_hsupa_category;
}  


/*===========================================================================

FUNCTION rrc_get_wcdma_hsupa_cm_ctrl_nv

DESCRIPTION
  This  function gets the NV item wcdma_hsupa_cm_ctrl and stores it in 
  rrc global variable 'rrc_wcdma_hsupa_cm_enabled'

DEPENDENCIES
  None.

RETURN VALUE
  None.

===========================================================================*/
static void rrc_get_wcdma_hsupa_cm_ctrl_nv
(
void
)
{

  /*read wcdma rrc release indicator from NV*/
  if ( NV_DONE_S ==  (rrc_get_nv( NV_WCDMA_HSUPA_CM_CTRL_I, 
                                  (nv_item_type *) &rrc_wcdma_hsupa_cm_enabled)))
  {
    WRRC_MSG1_HIGH("wcdma_hsupa_cm_ctrl (nv item number 5090) is %d", rrc_wcdma_hsupa_cm_enabled);
  }
  else
  {
    WRRC_MSG0_ERROR("Could Not Read rrc_wcdma_hsupa_cm_enabled(nv item number 5090) - Enable by default");
    rrc_wcdma_hsupa_cm_enabled = TRUE;
  }
  
} /* rrc_get_wcdma_hsupa_cm_ctrl_nv() */




/*===========================================================================

FUNCTION rrc_read_nv_items

DESCRIPTION
  This  function should call all the functions which read NV items to be used in RRC

DEPENDENCIES
  None.

RETURN VALUE
  None.

===========================================================================*/
void rrc_read_nv_items(void)
{
  rrc_read_nv_wcdma_optional_feature_list();

  rrc_read_nv_wcdma_hsupa_category();

  rrc_get_wcdma_hsupa_cm_ctrl_nv();

}



/*===========================================================================

FUNCTION rrc_read_from_efs

DESCRIPTION
This function takes NV conf item, the nv item variable address and 
the size of nv item variable to be read. This uses efs_get() to read
the NV item value from the efs NV item.


DEPENDENCIES
  None.

RETURN VALUE
  None

===========================================================================*/

static void rrc_read_from_efs(char *nv_conf_item, void *value,fs_size_t size)
{
  (void)efs_get(nv_conf_item,value,size);
  WRRC_MSG1_HIGH(" NV item read is %d",value);

}


/*===========================================================================

FUNCTION rrc_write_to_efs

DESCRIPTION
This function takes conf file descriptorNV conf item, address of 
default value to be written and size of the NV item as arguments 
and creates the NV item. This function assigns the default value to 
the NV item

DEPENDENCIES
  None.

RETURN VALUE
  None.

===========================================================================*/
static void rrc_write_to_efs(
  int32 rrc_feature_conf_fd,
  char *nv_conf_item,
  void *default_value,
  fs_size_t default_size
)
{
  char buf[100];

  memset((void *)buf,'\0', sizeof(buf));
  strlcpy(buf, nv_conf_item, sizeof(buf));
  strlcat(buf, "\n",sizeof(buf));
  /*lint +e421 */
  /*Write to conf file only when conf file is newly created / NAND erase case*/
  if(rrc_is_conf_file_new)
  {
    (void)efs_write(rrc_feature_conf_fd, buf, strlen(buf));
  }
  efs_put(nv_conf_item,default_value,default_size,O_CREAT | O_AUTODIR,0777);

}

/*===========================================================================

FUNCTION rrc_check_and_write_to_efs

DESCRIPTION
This function takes file descriptor NV conf item, the nv item variable 
address and the size of nv item variable to be read. This uses efs_get() 
to read the NV item value from the efs NV item. If the NV conf item is not 
present this function will write the default value to the conf file

DEPENDENCIES
  None.

RETURN VALUE
  None.

===========================================================================*/
static void rrc_check_and_write_to_efs(
  int32 rrc_feature_conf_fd,
  char *nv_conf_item,
  void *default_value,
  fs_size_t default_size
)
{
  if(efs_get(nv_conf_item,default_value,default_size) < 0)
  {
    rrc_write_to_efs(rrc_feature_conf_fd,nv_conf_item,default_value,default_size);
  }
}

/*===========================================================================

FUNCTION rrc_create_external_nv_item

DESCRIPTION
This function creates the conf file and opens the file for 
writing NV items. If the conf file is already created this 
function does not perform any operation. All new external NV 
items to be created should call rrc_write_to_efs with appropriate
arguments accordingly

DEPENDENCIES
  None.

RETURN VALUE
  None.

===========================================================================*/
static void rrc_create_external_nv_item(void)
{
  int32 rrc_feature_conf_fd;
  struct fs_stat  rrc_feature_conf_stat;


  if (efs_stat(rrc_nv_external_file, &rrc_feature_conf_stat) < 0 )
  {
    WRRC_MSG0_HIGH("CONF file not present");
    (void)efs_mkdir("/nv/item_files/conf", RRC_NV_CONF_FILE_PERMISSIONS_COMMON);
    rrc_is_conf_file_new = TRUE;
  }
  rrc_feature_conf_fd = efs_open(rrc_nv_external_file,
                     O_RDWR | O_APPEND | O_CREAT,RRC_NV_CONF_FILE_PERMISSIONS_COMMON);

    /* Write into CONF File only if conf file created*/
  if(rrc_feature_conf_fd >=0)
  {
    WRRC_MSG0_HIGH(" Writing into CONF File only if the NV item doesn't exist");
    /* NV - 67256 */
    rrc_check_and_write_to_efs(rrc_feature_conf_fd,rrc_nv_wtol_cm_item,&rrc_wtol_cm_support_nv,sizeof(uint8));

    /* NV - 67305 */
    rrc_check_and_write_to_efs(rrc_feature_conf_fd,rrc_nv_ppac_support_item,&rrc_ppac_support_nv,sizeof(uint8));
    /* NV - 67309 */
    rrc_check_and_write_to_efs(rrc_feature_conf_fd,rrc_save_acqdb_item,&rrc_save_acqdb_nv,sizeof(uint8));
    /* NV - 67293 */
    rrc_check_and_write_to_efs(rrc_feature_conf_fd,rrc_band_priority_config_item,&rrc_nv_band_priority_config,sizeof(rrc_nv_band_priority_config_type));
    /* NV - 67333 */
    rrc_check_and_write_to_efs(rrc_feature_conf_fd,rrc_disable_cpc_fdpch_item,&rrc_disable_cpc_fdpch_nv ,sizeof(uint8));
    /* NV - 72515 (may change)*/
    rrc_check_and_write_to_efs(rrc_feature_conf_fd,rrc_disable_crash_debug_info_item,&rrc_disable_crash_debug_info_nv,sizeof(uint8));
    /* NV - 67347 */
    rrc_check_and_write_to_efs(rrc_feature_conf_fd,wcdma_rrc_wtol_ps_ho_support_item,&wcdma_rrc_wtol_ps_ho_support_nv,sizeof(uint8));
    /* NV - 69732 */
    rrc_check_and_write_to_efs(rrc_feature_conf_fd,wcdma_rrc_wtol_tdd_ps_ho_support_item,&wcdma_rrc_wtol_tdd_ps_ho_support_nv,sizeof(uint8));
#ifdef FEATURE_WCDMA_DEBUG_ATTEMPT_FULL_SRCH
    /* NV - 70241 */
    rrc_check_and_write_to_efs(rrc_feature_conf_fd,rrc_enable_psc_lock_item,&rrc_enable_psc_lock_nv ,sizeof(uint8));
#endif

#ifdef FEATURE_FEMTO_CSG
    /* NV - 70256 */
    rrc_check_and_write_to_efs(rrc_feature_conf_fd,wcdma_rrc_csg_prune_count_item,&wcdma_rrc_csg_max_prune_count_nv,sizeof(uint32));
    rrc_check_and_write_to_efs(rrc_feature_conf_fd,wcdma_rrc_csg_srch_carr_item,&wcdma_csg_srch_carrier_space_nv,sizeof(boolean));
    rrc_check_and_write_to_efs(rrc_feature_conf_fd,wcdma_asf_timer_item,&wcdma_asf_timer_nv,sizeof(uint8));
    
    /*Protection against EFS corruption*/
    if((wcdma_asf_timer_nv == 0)||(wcdma_asf_timer_nv > 4))
    {
      wcdma_asf_timer_nv = 5;
    }
#endif

   rrc_check_and_write_to_efs(rrc_feature_conf_fd,rrc_nv_ignore_bar_conf_item,&rrc_ignore_cell_bar_nv,sizeof(uint8));

  /*For snow3g_security_algo support */
#ifdef FEATURE_WCDMA_SNOW3G_SECURITY
   rrc_check_and_write_to_efs(rrc_feature_conf_fd,rrc_nv_snow3g_algo_conf_item,&snow3g_security_algo_supported,sizeof(uint8));
#endif

#ifdef FEATURE_SMC_SRB2_SUSPENSION
   rrc_check_and_write_to_efs(rrc_feature_conf_fd,rrc_nv_set_srb2_act_conf_item,&rrc_set_srb2_act_time_nv,sizeof(uint8));
#endif

   rrc_check_and_write_to_efs(rrc_feature_conf_fd,rrc_nv_csfb_skip_sib11_opt_item,&rrc_csfb_skip_sib11_opt_nv,sizeof(uint8));

   rrc_check_and_write_to_efs(rrc_feature_conf_fd,rrc_nv_freq_lock_item,&rrc_nv_channel_lock_item_value,sizeof(uint16));

   rrc_check_and_write_to_efs(rrc_feature_conf_fd,rrc_nv_do_not_forward_page_item,&rrc_do_not_forward_page_nv,sizeof(uint8));

#ifdef FEATURE_WCDMA_FAST_RETURN_TO_LTE_AFTER_CSFB

   rrc_check_and_write_to_efs(rrc_feature_conf_fd,rrc_nv_fast_return_to_lte_after_csfb_item,&rrc_fast_return_to_lte_after_csfb_nv,sizeof(uint8));

   rrc_check_and_write_to_efs(rrc_feature_conf_fd,rrc_nv_fast_return_to_lte_after_csfb_timer_item,&rrc_fast_return_to_lte_after_csfb_timer_nv,sizeof(uint16));
#endif

    /* NV - 70350 */
    rrc_check_and_write_to_efs(rrc_feature_conf_fd,wcdma_rrc_freq_scan_timer_item,&wcdma_rrc_freq_scan_timer_nv,sizeof(uint32));
    /* NV - 70351 */
    rrc_check_and_write_to_efs(rrc_feature_conf_fd,wcdma_rrc_deep_sleep_no_svc_timer_item,&wcdma_rrc_deep_sleep_no_svc_timer_nv,sizeof(uint32));
    /* NV - 70352 */
    rrc_check_and_write_to_efs(rrc_feature_conf_fd,wcdma_rrc_bplmn_freq_scan_timer_item,&wcdma_rrc_bplmn_freq_scan_timer_nv,sizeof(uint32));

#ifdef FEATURE_GANSS_SUPPORT
    rrc_check_and_write_to_efs(rrc_feature_conf_fd,rrc_ganss_support_item,&rrc_ganss_support_nv,sizeof(uint8));
#endif
    /* NV - 72599 */
    rrc_check_and_write_to_efs(rrc_feature_conf_fd,rrc_dormancy_support_item,&rrc_dormancy_support_nv,sizeof(uint8));
    if((rrc_dormancy_support_nv == 0)||(rrc_dormancy_support_nv > RRC_NV_DORMANCY_DISABLED))
    {
      rrc_dormancy_support_nv = RRC_NV_DORMANCY_ENABLED;
    }

    rrc_check_and_write_to_efs(rrc_feature_conf_fd,wcdma_rrc_sib_sleep_sb_item,&wcdma_rrc_sib_sleep_sb_nv,sizeof(boolean));
    sib_sleep_after_sb1_sb2 = !wcdma_rrc_sib_sleep_sb_nv;

#ifdef FEATURE_WCDMA_ABSOLUTE_PRIORITY_FACH
    rrc_check_and_write_to_efs(rrc_feature_conf_fd,rrc_fe_fach_support_item,&rrc_fe_fach_support_nv,sizeof(uint8));
#endif

#ifdef FEATURE_DUAL_SIM
    rrc_check_and_write_to_efs(rrc_feature_conf_fd,rrc_dsds_concurrency_nv_item,&rrc_dsds_concurrency,sizeof(uint16));
    rrc_check_and_write_to_efs(rrc_feature_conf_fd,rrc_dsda_concurrency_nv_item,&rrc_dsda_concurrency,sizeof(uint16));

  /* Disabling DSDS/DSDA/TSTS concurrency for E-DRX and HS-RACH overriding the values set in NV */
  rrc_dsds_concurrency &= ~(NV_DS_EDRX_CONCURRENCY|NV_DS_HSRACH_CONCURRENCY);
  rrc_dsda_concurrency &= ~(NV_DS_EDRX_CONCURRENCY|NV_DS_HSRACH_CONCURRENCY);

#ifdef FEATURE_TRIPLE_SIM
  rrc_check_and_write_to_efs(rrc_feature_conf_fd,rrc_tsts_concurrency_nv_item,&rrc_tsts_concurrency,sizeof(uint16));

    rrc_tsts_concurrency &= ~(NV_DS_EDRX_CONCURRENCY|NV_DS_HSRACH_CONCURRENCY);
#endif
#endif

#ifdef FEATURE_WCDMA_MULTI_FREQ_BAND_INDICATOR
    rrc_check_and_write_to_efs(rrc_feature_conf_fd,rrc_mfbi_support_item,&rrc_mfbi_support_nv,sizeof(boolean));
#endif

rrc_check_and_write_to_efs(rrc_feature_conf_fd,rrc_constant_320ms_drx_item,&rrc_constant_320ms_drx_nv,sizeof(boolean));

#ifdef FEATURE_WCDMA_UL_COMPR
    rrc_check_and_write_to_efs(rrc_feature_conf_fd,rrc_ul_comp_support_item,&rrc_ul_compr_cap_nv,sizeof(boolean));
    WRRC_MSG1_HIGH("UL compr cap NV %d", rrc_ul_compr_cap_nv);
#endif
    rrc_check_and_write_to_efs(rrc_feature_conf_fd,rrc_prioritize_850_band_item,&rrc_prioritize_850_band_nv,sizeof(boolean));
#ifdef FEATURE_WCDMA_DEFAULT_CFG_CELLFACH
    rrc_check_and_write_to_efs(rrc_feature_conf_fd,rrc_default_cfg_in_cell_fach_nv_item,&rrc_default_cfg_in_cell_fach_nv,sizeof(uint8));
#endif
    rrc_check_and_write_to_efs(rrc_feature_conf_fd,rrc_sib7_time_item,&rrc_sib7_time_nv,sizeof(uint32));
    rrc_check_and_write_to_efs(rrc_feature_conf_fd,rrc_enable_meas_id_extn_item,&rrc_meas_id_extn_support,sizeof(boolean));
    rrc_check_and_write_to_efs(rrc_feature_conf_fd,wcdma_rrc_idle_features_item,&wcdma_rrc_idle_features,sizeof(uint32));
    (void)efs_close(rrc_feature_conf_fd);
  }
}

/*===========================================================================

FUNCTION rrc_read_nv_item

DESCRIPTION
This function internally calls rrc_create_external_nv_item() 
in order to read or create(is not present) the NV items.This uses the globally defined NV conf file,
NV conf item, address of default value to be written and size of the NV item to call 
the above functions.

DEPENDENCIES
  None.

RETURN VALUE
  None.

===========================================================================*/
void rrc_read_nv_item(void)
{
  rrc_read_from_efs(rrc_nv_device_type_conf_item,(void*) &rrc_device_type_nv, sizeof(uint8));

  rrc_create_external_nv_item();
}

/*===========================================================================
FUNCTION RRC_PUT_EFS

DESCRIPTION
  Write data to given EFS file.

RETURN VALUE
  Boolean

DEPENDENCIES
  This routine is not reentrant.  Shouldn't be a problem, as it doesn't exit
  till we're done, and it's only called from the RRC task.
===========================================================================*/
boolean rrc_put_efs(char *efs_file, void *data,fs_size_t size)
{
  void *temp = data;
  int32 status;
  int32 rrc_file_fd;
  uint32 max_nbytes;
  uint32 num_writes = 1,rem_bytes = 0,i;
  struct fs_statvfs buf;
  memset((void *)&buf,0, sizeof(struct fs_statvfs));
  rrc_file_fd = efs_open(efs_file,
                     O_RDWR |O_CREAT,RRC_NV_CONF_FILE_PERMISSIONS_COMMON);
  status = efs_statvfs(efs_file,&buf); // Maximum number of bytes that can be written in one efs_write operation
  max_nbytes = buf.f_maxwrite;
  WRRC_MSG2_HIGH("CSG: Maximum number of bytes that are allowed to write %d, %d",max_nbytes,status);
  if(size > max_nbytes)
  {
    num_writes = size/max_nbytes;
    rem_bytes = size - (num_writes * max_nbytes);
    size = max_nbytes;
  }

  if (rrc_file_fd >= 0) 
  {
    for(i = 0; i < num_writes;i++)
    {
      WRRC_MSG1_HIGH("CSG: size of data to be written %d",size);
      status = efs_write(rrc_file_fd,temp,size);
      WRRC_MSG1_HIGH("CSG: Write status from efs %d",status);
      temp = (char *)temp + size;
    }
    WRRC_MSG1_HIGH("CSG: size of data to be written %d",rem_bytes);
    status = efs_write(rrc_file_fd,temp,rem_bytes);
    WRRC_MSG1_HIGH("CSG: Write status from efs %d",status);
    (void)efs_close(rrc_file_fd);
    return TRUE;
  }
  else
  {
    return FALSE;
  }

}

/*===========================================================================
FUNCTION rrc_get_from_efs

DESCRIPTION
  Reads the data from given EFS file

RETURN VALUE
  Boolean

DEPENDENCIES
  This routine is not reentrant.  Shouldn't be a problem, as it doesn't exit
  till we're done, and it's only called from the RRC task.
===========================================================================*/

boolean rrc_get_from_efs(char *efs_file, void *value,fs_size_t size)
{
  int32 rrc_file_fd;
  uint32 max_nbytes;
  uint32 num_reads = 1,rem_bytes = 0,i;
  struct fs_statvfs buf;
 
  void * temp = value;
  int32 status;
  memset((void *)&buf,0, sizeof(struct fs_statvfs));

  rrc_file_fd = efs_open(efs_file,O_RDONLY,RRC_NV_CONF_FILE_PERMISSIONS_COMMON);
  status = efs_statvfs(efs_file,&buf); // Maximum number of bytes that can be read in one efs_read operation
  max_nbytes = buf.f_maxwrite;
  WRRC_MSG2_HIGH("Maximum number of bytes that are allowed to read %d, %d",max_nbytes,status);
  if(max_nbytes == 0)
  {
    return FALSE;
  }
  if(size > max_nbytes)
  {
    num_reads = size/max_nbytes;
    rem_bytes = size - (num_reads * max_nbytes);
    size = max_nbytes;
  }
  if(rrc_file_fd >= 0)
  {
    for(i = 0; i < num_reads;i++)
    {
      WRRC_MSG1_HIGH("size of data to be read %d",size);
      status = efs_read(rrc_file_fd,temp,size);
      WRRC_MSG1_HIGH("Read status from efs %d",status);
      temp = (char *)temp + size;
      if(status < size)
      {
        (void)efs_close(rrc_file_fd);
        return FALSE;
      }
    }
    WRRC_MSG1_HIGH("size of data to be read %d",rem_bytes);
    status = efs_read(rrc_file_fd,temp,rem_bytes);
    WRRC_MSG1_HIGH("read status from efs %d",status);
    (void)efs_close(rrc_file_fd);
    if(status < rem_bytes) 
    {
      return FALSE;
    }
    return TRUE; 
  }
  else
  {
    return FALSE;
  }
}

#ifdef FEATURE_DUAL_SIM
/*===========================================================================
FUNCTION rrc_set_default_dsds_concurrency

DESCRIPTION
  Function to set the default values for rrc_dsds_concurrency NV

RETURN VALUE
  None

DEPENDENCIES
===========================================================================*/
void rrc_set_default_dsds_concurrency(void)
{
#ifdef FEATURE_WCDMA_DC_HSDPA
  rrc_dsds_concurrency |= NV_DS_DCHSDPA_CONCURRENCY;
#endif

#ifdef FEATURE_WCDMA_UL_COMPR
  rrc_dsds_concurrency |= NV_UL_COMPRESSION_CONCURRENCY;
#endif

#ifdef FEATURE_WCDMA_CPC_DTX
  rrc_dsds_concurrency |= NV_DS_CPC_CONCURRENCY;
#endif

}

/*===========================================================================
FUNCTION rrc_set_default_dsda_concurrency

DESCRIPTION
  Function to set the default values for rrc_dsda_concurrency NV

RETURN VALUE
  None

DEPENDENCIES
===========================================================================*/	
void rrc_set_default_dsda_concurrency(void)
{
#ifdef FEATURE_WCDMA_DC_HSDPA
    rrc_dsda_concurrency |= NV_DS_DCHSDPA_CONCURRENCY;
#endif

#ifdef FEATURE_WCDMA_UL_COMPR
  rrc_dsda_concurrency |= NV_UL_COMPRESSION_CONCURRENCY;
#endif

#ifdef FEATURE_WCDMA_CPC_DTX
  rrc_dsda_concurrency |= NV_DS_CPC_CONCURRENCY;
#endif
}

#ifdef FEATURE_TRIPLE_SIM
/*===========================================================================
FUNCTION rrc_set_default_tsts_concurrency

DESCRIPTION
  Function to set the default values for rrc_tsts_concurrency NV

RETURN VALUE
  None

DEPENDENCIES
===========================================================================*/

void rrc_set_default_tsts_concurrency(void)
{
#ifdef FEATURE_WCDMA_DC_HSDPA
  rrc_tsts_concurrency |= NV_DS_DCHSDPA_CONCURRENCY;
#endif

#ifdef FEATURE_WCDMA_UL_COMPR
  rrc_tsts_concurrency |= NV_UL_COMPRESSION_CONCURRENCY;
#endif
}
#endif
#endif
