/*===========================================================================

                                  R R C L O G

GENERAL DESCRIPTION
 The RRCLOG module consists of functions logging tyhe OTA messages in binary 
 format. These binary files can be used for post analysis.

EXTERNALIZED FUNCTIONS

  rrclog_create_log_file()
    This function creates a new data file for write only.

  rrclog_close_log_file()
    This function closes the file.

  rrclog_log_sig_msg()
    This function writes the SDU header first into binary file and then writes
    the SDU data into binary file.

  
Copyright (c) 2000-2001, 2003-2008 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.
===========================================================================*/


/*===========================================================================
                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$PVCSPath:  L:/src/asw/MSM5200/RRC/vcs/rrclog.c_v   1.7   07 Aug 2001 10:36:36   rjeenaga  $
$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/wcdma/rrc/src/rrclog.c#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/07/14   sg      Made changes to log events for inter freq HHO status
11/03/10   rl      Fixed compiler warnings
12/21/09   gkg     Corrected misleading F3s.
05/08/09   ss      Updated Copyright Information
04/11/06   vm      RVDS2.1 compilation related changes.
03/31/06   vm      Fixed lint error.
12/18/05   da      Added new function rrc_send_rb_rate_info_packet to log 
                   RB rate information.
08/30/04   sgk     Type cast fclose to void in rrclog_close_log_file to fix 
                   lint error Ignoring return value of function.
01/13/02   ram     Linted file and cleared all existing errors.
08/07/01   rj      Fixed a compilation warning for ARM.
03/03/01   js      Initialization of sdu_hdr in function rrclog_log_sig_msg
                   is modified to fix ARM Compiler warnings.
12/05/00   rj      The formal parameter for function rrclog_log_sig_msg is
                   changed from uint16 to rrclog_log_ch_e_type
11/30/00   rj      Added $Header to the EDIT HISTORY 
11/30/00   rj      Added more comments and editorial corrections
11/17/00   rj      deleted rrclog_read_from_file. 
                   Changed rrclog_write_to_file function name to 
                   rrclog_log_sig_msg.
                   Changed rrclog_close_file to rrclog_close_log_file.
                   Changed rrclog_create_file to rrclog_create_log_file.
                   File Pointer is removed as formal parameter for the above 
                   functions.
11/14/00   rj      Created file.

===========================================================================*/


/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

#include "rrclog.h"
#include "stdio.h"
#include "err.h"
#include "rrcllci.h"
#include "rrcscmgr.h"
#include "rrcllc.h"
#include "rrclogging.h"
/*===========================================================================

                DEFINITIONS AND DECLARATIONS FOR MODULE

===========================================================================*/
boolean rrc_log_inter_freq_hho_started = FALSE;

extern rrc_established_rabs_type rrc_est_rabs;

static FILE *fp;                            /* File pointer for log file */
rrclog_file_status_e_type file_status = RRCLOG_FILE_CLOSED;
                                            /* The file status is 
                                               initialized */


extern uint32 rrcllc_get_dl_ded_rate(rrc_state_e_type  rrc_state, rrc_RB_Identity  rb_id);
extern uint32 rrcllc_get_ul_ded_rate(rrc_state_e_type  rrc_state, rrc_RB_Identity  rb_id);

/*===========================================================================

FUNCTION rrclog_create_log_file

DESCRIPTION
  This function creates and opens a new data file for write only.

DEPENDENCIES
  None

RETURN VALUE
  rrclog_status_e_type

SIDE EFFECTS
  None
===========================================================================*/

rrclog_status_e_type rrclog_create_log_file
(    
  const char *filename  
)
{
  uint32 count;
  rrclog_file_hdr_type file_hdr = 
                   {{'R','R','C',' '},
                    FILE_FORMAT_VER,       /* File Format Version */
                    MAJOR_SPEC_RELEASE,    /* Major Specification release */
                    MINOR_SPEC_RELEASE,    /* Minor Specification release */
                    MINOR_MINOR_SPEC_RELEASE,
                                             /* Minor minor Spec release */
                    ' ',                   /* A byte reserved for future
                                               expansion */
                    {' ',' ',' ',' ',' ',' '}
                   };                        /* Reserved for future expansion */
  
  rrclog_status_e_type status = RRCLOG_FAILURE;
   /* Checks whether already log file is created or not.*/
  if(file_status != RRCLOG_FILE_CLOSED)
  {
    WRRC_MSG0_ERROR("Log File is already opened");
    return (status);                        /* If log file already created
                                               it returns the failure status*/
  }
  /* Creates a log file if not already created */
  if((fp = fopen(filename, "wb")) == NULL)
  {
     WRRC_MSG0_ERROR("Cannot open designated file");
    return (status);                        /* Returns failure status if it 
                                               is unable open a file */
  }
  count = fwrite(&file_hdr, 1, sizeof(rrclog_file_hdr_type), fp);   
                                            /* Writes Header for the file  
                                               into the log file */
  if (count != sizeof(rrclog_file_hdr_type))
  {
    WRRC_MSG0_ERROR("Failed to write into the File");
    return (status);  
  }

  file_status = RRCLOG_FILE_OPENED;         /* File status is updated */
  status = RRCLOG_SUCCESS;
  return (status);
} /* rrclog_create_log_file */

/*===========================================================================
FUNCTION rrclog_close_log_file

DESCRIPTION
  This function closes the file.

DEPENDENCIES
  None

RETURN VALUE
  rrclog_status_e_type

SIDE EFFECTS
  None
===========================================================================*/

rrclog_status_e_type rrclog_close_log_file
(    
  void
)
{ 
  rrclog_status_e_type status = RRCLOG_SUCCESS;
  if(file_status == RRCLOG_FILE_CLOSED)
  {
    WRRC_MSG0_ERROR("The log file is not created. Hence can not be closed");
    status = RRCLOG_FAILURE; 
    return (status);
  }
  if(fp != NULL)
  {
    (void)fclose (fp);
    file_status = RRCLOG_FILE_CLOSED;
  }
  else
  {
    WRRC_MSG0_ERROR("Invalid File Pointer. Can not close");
    status = RRCLOG_FAILURE;
    file_status = RRCLOG_FILE_CLOSED;
  }
  return (status);
} /* rrclog_close_log_file */

/*===========================================================================
FUNCTION rrclog_log_sig_msg

DESCRIPTION
  This function writes the SDU header first into binary file and then writes
  the SDU data into binary file.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

rrclog_status_e_type rrclog_log_sig_msg
(    
  void  *sdu,                                /* SDU data */
  uint32 sdu_size,                           /* SDU size in bytes */
  uint16 rb_id,                              /* Radio Bearer Id to which SDU
                                                belongs */
  rrclog_log_ch_e_type log_ch                /* Logical Channel type to which
                                                SDU belongs */           
)
{
  int i;                                     /* Loop counter */
  rrclog_status_e_type status;
  uint32 count;
  rrclog_sdu_hdr_type sdu_hdr;
   
  /* Initialize the SDU header */
  sdu_hdr.log_ch = (uint16)log_ch;         /* Logical Channel type - typecast
                                           to the right size since enum value is
                                           assumed to be int size. Keeps lint happy.
                                           Logical channel is between 0 and 4. */
  sdu_hdr.rb_id = rb_id;                   /* Radio Bearer Id */
  sdu_hdr.num_bytes = sdu_size;            /* Size of SDU in bytes */

  for (i = 0; i < TIME_STAMP_LENGTH; i++)
  {
    sdu_hdr.time_stamp[i] = 0;             /* Time stamp for future expansion */
  }

  for (i = 0; i < SDU_HDR_RES_LENGTH; i++)
  {
    sdu_hdr.reserved_bytes[i] = 0;         /* Reserved for future expansion */
  }

  status = RRCLOG_FAILURE;

  if(file_status == RRCLOG_FILE_CLOSED)     /* Checks whether log is opened */
  {
    WRRC_MSG0_ERROR("Log File is not opened. Hence Sig message can not be logged");
    return(status);
  }  

  count = fwrite(&sdu_hdr, 1, sizeof(rrclog_sdu_hdr_type), fp);   
                                            /* Writes Header for the SDU 
                                               into the file */
  if (count != sizeof(rrclog_sdu_hdr_type))
  {
    WRRC_MSG0_ERROR("Failed to write into the File");
    return (status);
  }
  
  count = fwrite(sdu, 1, sdu_hdr.num_bytes,fp);   
                                           /* Writes the SDU Data into
                                              the file */
  if (count != sdu_hdr.num_bytes)
  {
    WRRC_MSG0_ERROR("Failed to write into the File");
    return (status);
  }
  status = RRCLOG_SUCCESS;
  return (status);
} /* rrclog_log_sig_msg */


/*===========================================================================

FUNCTION RRC_SEND_RB_RATE_INFO_PACKET

DESCRIPTION

DEPENDENCIES
    None.

RETURN VALUE
    None.

SIDE EFFECTS
    None.

===========================================================================*/
void rrc_send_rb_rate_info_packet
(
void
)
{
    WCDMA_RB_RATE_INFO_LOG_PACKET_type   *log_rb_rate_ptr;
    uint8 num_rbs = 0;
    uint32 count, count1, count2;
    rrc_state_e_type rrc_state;
    rrc_proc_e_type                 proc_id;
    rrcllc_oc_process_state_e_type  process_state;  
    rrcllc_oc_set_status_e_type     oc_status;

    oc_status = rrcllc_get_ordered_config_state_and_proc(&proc_id, &process_state);

    switch (oc_status) 
    {
      case OC_SET_FOR_CELL_FACH:
      case OC_SET_FOR_DCH_FACH_TRANS:
        rrc_state = RRC_STATE_CELL_FACH;
        break;
      case OC_SET_FOR_CELL_DCH:
        rrc_state = RRC_STATE_CELL_DCH;
        break;
      case OC_SET_FOR_URA_PCH:
      case OC_SET_FOR_DCH_URA_PCH_TRANS:
      case OC_SET_FOR_FACH_URA_PCH_TRANS:
        rrc_state = RRC_STATE_URA_PCH;
        break;
      case OC_SET_FOR_CELL_PCH:
      case OC_SET_FOR_DCH_CELL_PCH_TRANS:
      case OC_SET_FOR_FACH_CELL_PCH_TRANS:
        rrc_state = RRC_STATE_CELL_PCH;
        break;
      default:
        rrc_state = rrc_get_state();
        break;
    }


    /* Get the total number of RBs */
    for (count = 0; count < MAX_RAB_TO_SETUP; count++) 
    {
      if( (rrc_est_rabs.rabs[count].rab_id != RRC_INVALID_RAB_ID) &&
          (rrc_est_rabs.rabs[count].num_rbs_for_rab > 0))
      {
        WRRC_MSG2_ERROR(" RAB_ID %d, num_rbs %d", rrc_est_rabs.rabs[count].rab_id, rrc_est_rabs.rabs[count].num_rbs_for_rab);
        num_rbs = num_rbs + (uint8)rrc_est_rabs.rabs[count].num_rbs_for_rab;
      }
    }
    WRRC_MSG1_ERROR("Total num rbs %d", num_rbs);
       /* Allocate memory here.*/
    log_rb_rate_ptr = (WCDMA_RB_RATE_INFO_LOG_PACKET_type   *)log_alloc(WCDMA_RB_RATE_INFO_LOG_PACKET,
        WCDMA_RB_RATE_INFO_PACKET_LEN(num_rbs));

    count2 = 0;
    if (log_rb_rate_ptr != NULL)
    {
      log_rb_rate_ptr->num_rbs = num_rbs;
      /* Get the total number of RBs */
      if (num_rbs > 0)
      {
        for (count = 0; count < MAX_RAB_TO_SETUP; count ++) 
        {
          if( (rrc_est_rabs.rabs[count].rab_id != RRC_INVALID_RAB_ID) &&
              (rrc_est_rabs.rabs[count].num_rbs_for_rab > 0))
          {
            for (count1 = 0; ((count1 < rrc_est_rabs.rabs[count].num_rbs_for_rab) &&
                              (count1 < MAX_RB_PER_RAB)) ; count1++) 
            {
              log_rb_rate_ptr->rb_rate[count2].rb_id = rrc_est_rabs.rabs[count].rb_for_rab[count1].rb_id;
              log_rb_rate_ptr->rb_rate[count2].dl_rb_rate = rrcllc_get_dl_ded_rate(
                rrc_state, rrc_est_rabs.rabs[count].rb_for_rab[count1].rb_id);
              log_rb_rate_ptr->rb_rate[count2].ul_rb_rate = rrcllc_get_ul_ded_rate(
                rrc_state, rrc_est_rabs.rabs[count].rb_for_rab[count1].rb_id); 
              count2++;
            }
          }
        }
      }
      /* Commit the buffer to log services.*/
      log_commit(log_rb_rate_ptr);
    }
    else
    {
        MSG_LOW("Dropped 0x%x, code enabled: %d\n", WCDMA_RB_RATE_INFO_LOG_PACKET,
            log_status(WCDMA_RB_RATE_INFO_LOG_PACKET), 0);
    }
}

/*====================================================================
FUNCTION: rrc_log_event_inter_freq_hho_update_status

DESCRIPTION:
  This function logs the status of inter frequency hard hand off.

DEPENDENCIES:
  None.

RETURN VALUE:
  None

SIDE EFFECTS:
  None.
====================================================================*/
void rrc_log_event_inter_freq_hho_update_status(rrc_inter_freq_hho_status_e_type status)
{
  rrc_log_inter_freq_hho_status_event_type log_status;
	MSG_HIGH("rrc_log_inter_freq_hho_started : %d",rrc_log_inter_freq_hho_started,0,0);

  log_status.status = status;
  if(rrc_log_inter_freq_hho_started == TRUE)
  {
    MSG_HIGH("LOG INTER FREQ HHO event: status %d rrc_log_inter_freq_hho_started: %d",status,rrc_log_inter_freq_hho_started,0);
    event_report_payload(EVENT_WCDMA_RRC_INTER_FREQ_HHO_STATUS, sizeof(log_status),(void *)&log_status);
  }
  if(status != RRC_LOG_INTER_FREQ_HHO_STARTED )
  {
    rrc_log_inter_freq_hho_started = FALSE;
  }
}

/*=========================================================================*/
