/**
 * This file was generated by the Objective Systems ASN1C Compiler
 * (http://www.obj-sys.com).  Version: 6.6.5, Date: 13-Nov-2013.
 * This file has been generated from 25.331V10.12.0(2013-06) ASN
 * $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/wcdma/rrc/inc/rrcasn1Enc.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $
 */
#ifndef RRCASN1ENC_H
#define RRCASN1ENC_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include "rrcasn1.h"
#include "rtpersrc/asn1per.h"

EXTERN int asn1PE_rrc_UL_DCCH_MessageType_ext (OSCTXT* pctxt, rrc_UL_DCCH_MessageType_ext* pvalue);

EXTERN int asn1PE_rrc_UL_DCCH_MessageType (OSCTXT* pctxt, rrc_UL_DCCH_MessageType* pvalue);

EXTERN int asn1PE_rrc_UL_DCCH_Message (OSCTXT* pctxt, rrc_UL_DCCH_Message* pvalue);

EXTERN int asn1PE_rrc_UL_CCCH_MessageType (OSCTXT* pctxt, rrc_UL_CCCH_MessageType* pvalue);

EXTERN int asn1PE_rrc_UL_CCCH_Message (OSCTXT* pctxt, rrc_UL_CCCH_Message* pvalue);

#ifdef __cplusplus
}
#endif

#endif
