#ifndef RRCMSMIF_H
#define RRCMSMIF_H
/*========================================================================================

                                    R R C  M S M  I N T E R F A C E

DESCRIPTION

  This file contains the interface commands and stuctures between RRC and MBMS Service Manager


  Copyright (c) 2005,2007,2008 by Qualcomm Technologies Incorporated.  All Rights Reserved.
========================================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/wcdma/api/rrcmsmif.h#1 $    $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $
  
 when       who     what, where, why
--------    ---     ----------------------------------------------------------
07/29/11   ad       Made changes for CMI Phase 4
02/10/11   rl       Merged with VU_MODEM_WCDMA_RRC.01.90.51
02/03/11    ad      Added changes to refeaturize the API files
01/28/11   rl       Merged with VU_MODEM_WCDMA_RRC.01.89.57
01/12/11    ad      Added changes for RRC SW decoupling
05/08/09    ss      Updated Copyright Information
03/06/08    da      Changed FEATURE_MBMS to FEATURE_MODEM_MBMS
12/21/07    vr      Created file

===========================================================================*/


#endif /* RRCMSMIF_H */
