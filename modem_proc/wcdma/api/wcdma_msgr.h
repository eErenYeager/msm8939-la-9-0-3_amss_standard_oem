#ifndef WCDMA_MSGR_H
#define WCDMA_MSGR_H
/*===========================================================================
                      WCDMA TECH MODULE DEFINITIONS

DESCRIPTION

  This file contains the WCDMA Tech module to support the MSGR

Copyright (c) 2000-2009 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.

===========================================================================*/


/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/wcdma/api/wcdma_msgr.h#1 $    $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
02/26/10   rm      Added new line at EOF
01/28/10   rm      Added WCDMA RRC tech module definition
===========================================================================*/


#include "msgr.h"


#define MSGR_WCDMA_RRC     MSGR_TECH_MODULE( MSGR_TECH_WCDMA, 0x01 )

#endif /* #ifndef WCDMA_MSGR_H */
