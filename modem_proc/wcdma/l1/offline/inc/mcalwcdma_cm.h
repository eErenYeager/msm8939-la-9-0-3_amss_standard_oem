#ifndef MCALWCDMA_CM_H
#define MCALWCDMA_CM_H

/*============================================================================*/
/** @file  
 * This module has definition and declaration related to MCAL UMTS compressed
 * mode module APIs
 */
/*============================================================================*/

/*============================================================================
Copyright (c) 2012 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.
============================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/wcdma/l1/offline/inc/mcalwcdma_cm.h#1 $
$DateTime: 2015/01/27 06:42:19 $
$Author: mplp4svc $

when        who     what, where, why
--------    ---     --------------------------------------------------------
08/08/14    mm      Changed start slot field for FMO gaps to int8
06/05/14    mk      WCDMA FED Feature for DPM_2_0, Flag used FEATURE_WCDMA_L1_DPM_FED_SUPPORT
04/29/14    mm      Update IRAT/CM driver initialization and RxLM buffer allocation call flow
02/07/14    jkb     Add check for Feature dual sim with feature qta
10/15/13    mm      Initial FE-FACH check-in
09/23/13    as      Porting Dual Sim from Triton to Dime.
08/12/13    cc      DBDC CM W2W feature check in
04/23/13    mm      Passing neighbor RxLM information for W2W inter-freq measurements every CM/FACH gap.
                    Additional RxLM buffer for back to back CM gaps.
01/09/13    dp      Changes to allow sending w2l cm gap-end time to WFW
01/30/12    vs      Feature cleanup.
10/26/11    dp      Added RxD reconfig capabilities after irat searches
10/26/11    yh      Fix calculation of available search window in a gap
08/03/11    dp      Re-worked dynamic tune time code for NikeL bringup.  
                    Also added fixes for previous XO changes.
05/26/11    dp      Added changes to make tuning time value dynamic
05/26/11    dp      Added support for W2W XO Parameter passing to FW
04/27/11    dp      Added RXLM support for IRAT and W2W searches
06/14/10    yh      Add init rf buf function
05/07/10    yh      Add support for IRAT RF buffer control
06/15/09    uk      Support for sending START_FACH_MEAS cmd to fw before 
                    starting a FMO gap for GSM meas
04/20/09    yh      Compiler warning fixes.
01/22/09    stk/yh  Added enum for TGMP
11/14/08    uk      Added FMO Support
11/13/08    yh      Initial revision.

===========================================================================*/

/* ==========================================================================
** Includes and Public Data Declarations
** ========================================================================*/

/* -----------------------------------------------------------------------
** Include Files
** ----------------------------------------------------------------------- */

#include "comdef.h"
#include "customer.h"
#include "wfw_misc_intf.h"

/* -----------------------------------------------------------------------
** Constant / Define Declarations
** ----------------------------------------------------------------------- */
/** Number of RF buffers for W2W */
#define MCALWCDMA_CM_MAX_RF_SETUP_IDX     3
/** Number of RF buffers for IRAT (W2G and W2L) */
#define MCALWCDMA_CM_IRAT_MAX_RF_BUF      2
/** Invalid GSM frame number */
#define MCALWCDMA_CM_INVALID_GSM_FN       0xFF

/* this is a general purpose value representing an invalid amount of time.  
   It should only be used with fields that store time values. */
#define MCALWCDMA_CM_TIME_INVALID         0

/* Use this to convert microseconds into (rounded) chipx8.  Given the wcdma 
   parametrs at the time of writing this, this effectively this takes usecs and
   multiplies by 30.72 chipx8/usec.  The shifting to the left 14 is to preserve
   precision when dividing by 10000 (microseconds per frame term).  To correct 
   this, at the end you would normally shift right by 14 at the end, but in 
   order to round, we shift right by 13 at the end, add one, and then shift to 
   the right one more time for a total of 14 right shifts.  A uint64 is 
   required to keep the dividend from overflowing. The result should end up as 
   a uint32 though after the corrective right shifting. */
#define MCALWCDMA_CM_USECS_TO_CHIPX8(usecs) \
((uint32)((((((((uint64)(usecs))*CHIP_PER_FRAME*8)<<14)/(MSEC_PER_FRAME*1000))>>13) + 1)>>1))

/* -----------------------------------------------------------------------
** Type Declarations
** ----------------------------------------------------------------------- */

/* Call back function type declaration */
/* ----------------------------------- */

/* Enum type declaration */
/* --------------------- */
/** Enum to be the same type as mDSP for CM method */
typedef enum {
  /** Compressed mode by HLS */
  MCALWCDMA_CM_METHOD_HLS,
  /** Compressed mode by SF reduction */
  MCALWCDMA_CM_METHOD_SF_RED,
} mcalwcdma_cm_method_enum_type;

/** Enum to be the same type as mDSP for TGMP */
typedef enum {
  /** CM No measurement (debug only) */
  MCALWCDMA_CM_TGMP_NO_MEAS,
  /** CM interfrequency measurements */
  MCALWCDMA_CM_TGMP_INTER_FREQ,
  /** For W2G GSM measurements */
  MCALWCDMA_CM_TGMP_W2G_GSM_MEAS,
  /** For W2L LTE measurements */
  MCALWCDMA_CM_TGMP_W2L_LTE_MEAS,
  #if defined(FEATURE_DUAL_SIM) && defined(FEATURE_QTA)
  /*Added two new enums to support QTA Start-Stop cmds*/
  /*! Open gap for W2G quick tune away */
  MCALWCDMA_CM_TGMP_W2G_QTA_START,
  /*! Close gap after W2G quick tune away */
  MCALWCDMA_CM_TGMP_W2G_QTA_STOP,
  #endif
  MCALWCDMA_CM_TGMP_MAX
} mcalwcdma_cm_tgmp_enum_type;

/* Structure type declaration */
/* -------------------------- */
/** Structure for IRAT RF buffer control */
/* Struct for the pending RF buffers used for GSM */
typedef struct
{
  /* GSM Frame that the buffer can be freed */
  uint8 gfn_to_free;
} mcalwcdma_cm_gsm_rf_buf_struct_type;

typedef struct
{
  /** Buffer in use, not free */
  boolean buffer_in_use;
  #ifdef FEATURE_WCDMA_L1_DPM_FED_SUPPORT
  /** Home RAT RxLM index */
  uint8 source_rxlm_index;
  #else
  /** Startup index */
  uint8 startup_index;
  /** Cleanup index */
  uint8 cleanup_index;
  #endif
  /** TGMP of the buffer */
  mcalwcdma_cm_tgmp_enum_type tgmp;
  union
  {
    mcalwcdma_cm_gsm_rf_buf_struct_type gsm_pending_rf_buf;
   /* To be included at a later stage */
   /* mcalwcdma_lte_rf_buf_struct_type lte_pending_rf_buf; */
  } irat_rf_buf;

  /* this structure also will contain the rxlm buffer index info passed to 
     other RAT's as part of searches */
  union
  {
    struct
    { 
      /* Interf neighbor RxLM buffer index */
      uint32 interf_buf_idx;
    } interf_info;

    /** (for GSM searches) buffer index for the WL1_XLM_RX_CHAIN0_IRAT_GSM_NBR
        client in addition to the WCDMA home client */ 
    struct
    {
      uint32 gsm_buf_idx;
    } gsm_info;

    /** (for LTE searches) buffer indicies for the 
        WL1_XLM_RX_CHAIN0_IRAT_LTE_NBR and WL1_XLM_RX_CHAIN1_IRAT_LTE_NBR
        clients */
    struct
    {
      uint32 lte_ch0_buf_idx;
      uint32 lte_ch1_buf_idx;
    } lte_info;

  } rxlm_buf_info;

} mcalwcdma_cm_irat_rf_buf_ctrl_struct_type;
/** Structure for CM init parameters */
typedef struct
{
  /** WTR index info **/
  uint8 wtrindex;
  /** RPP type */
  uint16 rpp;
  /** ITP bit */
  uint16 itp;
  /** DL Frame type */
  uint16 dl_frame_type;
  /** UL compressed mode method */
  mcalwcdma_cm_method_enum_type ul_cm_method;
  /** DL compressed mode method */
  mcalwcdma_cm_method_enum_type dl_cm_method;
  /** First compressed slot */
  uint16 first_cm_slot;
  /** transmission gap length */
  uint16 tgl;
  /** transmission gap measurement purpose */
  uint16 tgmp;
  #ifdef FEATURE_WCDMA_L1_DPM_FED_SUPPORT
  #else
  /** index into rf setup buf for frequency to be searched */
  uint16 rf_setup_buf_idx;
  /** index into rf setup buf for home frequency */
  uint16 rf_home_buf_idx;
  #endif
  /** the rxlm buffer index holding the nbr-frequency primary Rx chain info */
  uint32 rxlm_setup_buf_idx;
  /** the rxlm buffer index holding the home-frequency primary Rx chain info */
  uint32 rxlm_home_buf_idx;
  /** the rxlm buffer index holding the home-frequency diversity Rx chain info */
  uint32 rxlm_home_rxd_buf_idx;
  #ifdef FEATURE_WCDMA_L1_DPM_FED_SUPPORT
  #else
  #ifdef FEATURE_WCDMA_CM_LTE_SEARCH
  /** the frc frame number of the time when it's safe for WFW to start actions 
      for end of gap */
  uint32 tune_back_time_fr;
  /** the frc frame offset (cx8) of the time when it's safe for WFW to start 
      actions for end of gap */
  uint32 tune_back_time_offset;
  #endif
  #endif
} mcalwcdma_cm_init_params_struct_type;

/* Structure type declaration */
/* -------------------------- */
/** Structure for FACH init parameters */
typedef struct
{
  /** WTR index info **/
  uint8 wtrindex;

  /** Combiner channel number/HW channel index **/
  uint8 chan_num;

  /** Start CFN of the gap */
  uint8 start_cfn;

  /** start slot of the gap */
  int8 start_slot;

  /** measurement period length in slots */
  uint8 gap_len_slots;

  /** measurement gap purpose */
  uint16 tgmp;

  #ifdef FEATURE_WCDMA_L1_DPM_FED_SUPPORT
  #else
  /** RF setup buffer index (used for tuning away) */
  uint16 rf_setup_buf_idx;
  /** RF home buffer index (used for tuning back) */
  uint16 rf_home_buf_idx;
  #endif
  /** the rxlm buffer index describing the tune-away information */
  uint32 rxlm_setup_buf_idx;
  /** the rxlm buffer index holding the home-frequency primary Rx chain info */
  uint32 rxlm_home_buf_idx;
  /** the rxlm buffer index holding the home-frequency diversity Rx chain info */
  uint32 rxlm_home_rxd_buf_idx;

} mcalwcdma_fach_init_params_struct_type;


/** Structure for RF setup info to be written into mDSP  */
typedef struct{
  WfwCmRfRxfDeltaRegisterStruct deltaRxf;
  /** All the band information */
  WfwW2WRfBandInfoStruct bandInfo;  
}mcalwcdma_cm_rf_control_info_struct_type;

/* -----------------------------------------------------------------------
** Global Data Declarations
** ----------------------------------------------------------------------- */
extern mcalwcdma_cm_irat_rf_buf_ctrl_struct_type mcalwcdma_cm_irat_rf_buf_ctrl[MCALWCDMA_CM_IRAT_MAX_RF_BUF];

/* this is the amount of time (in chips x8) it takes for RF to tune away to a 
   neighboring WCDMA frequency */
extern uint32 mcalwcdma_cm_w2w_tune_away_time_cx8;
/* this is the amount of time (in chips x8) it takes for RF to tune back to the
   home frequency at the end of a neighbor search */
extern uint32 mcalwcdma_cm_w2w_tune_back_time_cx8;
/* this is the amount of time (in chips x8) it takes for WFW to collect samples   
   before start doing any search inside a gap */
extern uint32 mcalwcdma_cm_w2w_samples_collection_time_cx8;

/* =======================================================================
**                        Function Declarations
** ======================================================================= */

/*===========================================================================
**
 *  This function initializes all the variables used by mcalwcdma_cm, mostly
 *  assigning FW-SW pointers.
 */
/*============================================================================*/
extern void mcalwcdma_cm_init(void);

/*===========================================================================
**
 *  This function finds available RF buffer indices to be used for W2G and W2L 
 *  searches. 
 */
/*============================================================================*/
extern boolean mcalwcdma_cm_get_free_rf_buf_indices(
  /** Type of IRAT request */
  mcalwcdma_cm_tgmp_enum_type tgmp,
  /** Buffer to be assigned */
  mcalwcdma_cm_irat_rf_buf_ctrl_struct_type **rf_buf);

/*===========================================================================
**
 *  This function cleans up the rf_buf for future use.
 */
/*============================================================================*/
extern void mcalwcdma_cm_clean_rf_buf(
  /** Buffer to be assigned */
  mcalwcdma_cm_irat_rf_buf_ctrl_struct_type *rf_buf);

/*===========================================================================
**
 *  This function inits all the rf_buffers.
 */
/*============================================================================*/
extern void mcalwcdma_cm_init_rf_buffers(void);
/*===========================================================================
**
 *  This function writes the home frequency index into mDSP.
 */
/*===========================================================================*/
extern void mcalwcdma_cm_set_cm_home_freq_idx(
  /** mDSP index for the home frequency */
  uint16 idx);

/*===========================================================================
**
 *  This function  writes to mDSP all the RF setup data for interfrequency
 *  into a mDSP buffer location indicated by idx.
 */
/*===========================================================================*/
extern void mcalwcdma_cm_write_rf_buf_info(
  /** Buffer index in mDSP */
  uint16 idx, 
  /** mDSP RF setup buffer */
  mcalwcdma_cm_rf_control_info_struct_type *buffer);

/*============================================================================*/
/**
 * This function sends a command to mdsp to init cm.
 *
 */
/*============================================================================*/
extern void mcalwcdma_cm_init_cmd(
  /** cm init params */
  mcalwcdma_cm_init_params_struct_type *mcalwcdma_cm_init_params);

/*============================================================================*/
/**
 * This function sends a command to mdsp to init FACH interF meas.
 *
 */
/*============================================================================*/
extern void mcalwcdma_cm_fach_init_cmd(
  /** FACH cfg parameters */
  mcalwcdma_fach_init_params_struct_type *mcalwcdma_fach_init_params);

/*============================================================================*/
/**
 * This function sets the DL outer loop power control adjust value for
 * compressed mode. There are 2 types of power control gain to be adjusted
 * for compressed mode.
 * 
 * deltaP_compression:
 *  For SF/2 it is 3 dB
 *  For higher layer scheduling it is 0 dB
 * deltaP_coding:
 *  This is the deltaP_SIR or deltaP_SIR_After based on GAP position and
 *  active TGPS state for the GAP. The detail of this is mentioned below.
 *  Each deltaP_SIR_xxx is specified by UTRAN ranges from 0 to 3 dB in step of 0.1 dB
 * Here is the detail of how the deltaP coding is calculated.
 * DeltaP coding is the combination of 2 deltaP SIR
 * 1. Previous Frame. This reflect the frame after GAP
 * 2. Current GAP
 *
 * For previous GAP (This frame is one after the GAP <gap num> has ended)
 *  If this frame is the frame after the GAP then apply deltaP_SIR_After<gap num>
 *  Following are the exception for above:
 *    The deltaP_SIR_After should not applied if the GAP that ended in previous
 *    frame was multiframe GAP.
 *
 * For current frame (This frame has a GAP <gap num>)
 *  If this is the first frame in the GAP (Gap start in this frame), apply
 *  deltaP_SIR_<gap num>
 *  If this is the frame has a GAP but GAP didn't started in this frame,
 *  apply deltaP_SIR_After<gap num>
 *  
 * Here <gap num> can be 1 or 2.
 * To apply the delta_P SIRs apply the delta that is the sum of all the
 * active TGPS at a given time.
 * 
 *  cmFpcAdjust =
 *   1024 * 10^(0.1 x [deltaP_compression + deltaP_coding])
 *   = 1024 * 10^(0.1 deltaP_compression) * 10^(0.1 deltaP_coding))
 *   
 * The max dB value possible is 10 x log10(65536 / 1024) = 18.06
 */
/*============================================================================*/
extern void mcalwcdma_cm_set_ebnt_adj_val(
  /** Total delta SIR based on gap parameters */
  uint16 total_delta);

#ifndef FEATURE_WCDMA_L1_DPM_FED_SUPPORT
/*===========================================================================
FUNCTION mcalwcdma_cm_write_fw_w2w_xo_fields

DESCRIPTION     
  This function sets the parameters of FW's WfwCmXoParamsStruct, which should 
  be done for ever W2W CM gap.  These fields include calculated totalFreqError
  that will be present on the neighbor, as well as the invDLOFreq.

DEPENDENCIES    
  None

RETURN VALUE    
  None

SIDE EFFECTS    
  None
===========================================================================*/
void mcalwcdma_cm_write_fw_w2w_xo_fields(
  /* the index in WfwCmRFControlInfoAsyncWriteStruct where neighbor band 
     info is stored */
  uint8 idx,
  /* the home carrier frequency */
  uint32 h_carrier_khz, 
  /* the neighbor carrier frequency */  
  uint32 n_carrier_khz);

#endif

/*===========================================================================
FUNCTION  mcalwcdma_cm_calc_neighbor_tot_freq_error

DESCRIPTION
  This function calculates the expected neighbor total frequency error based on
  the home total frequency error, the home carrier frequency, and the neighbor
  carrier frequency.
  
DEPENDENCIES
  None
  
RETURN VALUE
  None
  
SIDE EFFECTS
  None
===========================================================================*/
int32 mcalwcdma_cm_calc_neighbor_tot_freq_error(
  /* total frequency error at home frequency in Hz Q6 format */
  int32 h_tot_freq_error,
  /* home carrier frequency in kHz */
  uint32 h_carrier_f,
  /* neighbor carrier frequency in kHz */
  uint32 n_carrier_f
  );

/*===========================================================================
FUNCTION  mcalwcdma_cm_calc_freq_inv

DESCRIPTION
  This function calculates an inverse frequency in the format FW wants.  The
  formula they want is 2^33/(frequency inverse in q19).
   
  
DEPENDENCIES
  None
  
RETURN VALUE
  uint32 value representing the frequency inverse.
  
SIDE EFFECTS
  None
===========================================================================*/
uint32 mcalwcdma_cm_calc_freq_inv(
  /* the frequency to calculate the inverse of (given in kHz)*/
  uint32 freq_to_inv
  );


/*============================================================================
FUNCTION mcalwcdma_cm_update_irat_tune_times

DESCRIPTION
  This function will call the correct rf interface in order to fill in IRAT's
  picture of how long tune times take from W2W, W2L, or W2G (both tuning away
  and back again).  Currently only does anything for W2W.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
============================================================================*/
void mcalwcdma_cm_update_irat_tune_times(void);

/*============================================================================
FUNCTION mcalwcdma_cm_set_pll_settling_time_cx8

DESCRIPTION
  This function stores the pll_settling time value in mcalwcdma_cm's global
  data fields.  This should be the only method of changing the value since FW
  must be notified each time the value changes.  This function takes care of 
  that.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  pll_settling_time is written to FW async params
============================================================================*/
void mcalwcdma_cm_set_pll_settling_time_cx8(
  /* the pll settling time in units of cx8 */
  uint32 settling_time);

/*===========================================================================
FUNCTION mcalwcdma_cm_deallocate_lte_rxlm_buffer

DESCRIPTION
  This function de-allocates the RxLM buffers for W2L searches

DEPENDENCIES
  None

RETURN VALUE

SIDE EFFECTS
  None
===========================================================================*/
extern void mcalwcdma_cm_deallocate_lte_rxlm_buffer(void);

/*===========================================================================
FUNCTION mcalwcdma_cm_deallocate_gsm_rxlm_buffer

DESCRIPTION
  This function de-allocates the RxLM buffers for W2G searches

DEPENDENCIES
  None

RETURN VALUE
  Allocated RxLM buffer index

SIDE EFFECTS
  None
===========================================================================*/
extern void mcalwcdma_cm_deallocate_gsm_rxlm_buffer(uint32 rxlm_buffer_to_deallocate);

/*===========================================================================
FUNCTION mcalwcdma_cm_deallocate_interf_rxlm_buffer

DESCRIPTION
  This function de-allocates the RxLM buffers for interf searches

DEPENDENCIES
  None

RETURN VALUE
  
SIDE EFFECTS
  None
===========================================================================*/
extern void mcalwcdma_cm_deallocate_interf_rxlm_buffer(uint32 rxlm_buffer_to_deallocate);

#endif /* MCALWCDMA_CM_H */
