#ifndef __MCALWCDMA_WENC_H__
#define __MCALWCDMA_WENC_H__
/*============================================================================
                        M C A L W C D M A _ W E N C. H
DESCRIPTION
  This module has definition and declaration related to MCAL (modem core
  abstraction layer) W Encoder APIs.
 
INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2013 QUALCOMM Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.

============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/wcdma/l1/offline/inc/mcalwcdma_wenc.h#1 $

when      who     what, where, why
--------  ---     --------------------------------------------------------------
02/19/14   ash    Backout SW workaround for Resetting TXC0 and TXR0
01/23/14   ash    SW workaround for Resetting TXC0 and TXR0
07/10/13   ar     Initial version : Modem core abstraction layer changes 

==============================================================================*/
/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "wcdma_variation.h"
#include "wenc.h"
#include "ulstates.h"
#include "l1rrcif.h"
#include "ul.h"
#include "l1macdata.h"
#include "wenci.h"
#include "l1utils.h"
#include "eulenc.h"

#include "comdef.h"
#include "msmhwio.h"
#include "msmhwioreg.h"

#ifdef FEATURE_WCDMA_L1_TM_CIPHER_SUPPORTED
#include "msm.h"
#endif

/*===========================================================================

                DEFINITIONS AND DECLARATIONS FOR MODULE

This section contains definitions for constants, macros, types, variables
and other items needed by this module.

===========================================================================*/
typedef struct {
    uint8 trch_idx;
    ul_rm_parms_struct_type ul_rm_parms;
    ul_trch_coding_struct_type ul_trch_coding_parms;
    ul_trch_turbo_ileav_parms_type ul_trch_turbo_ileav_parms;
    uint16 eramb_start_addr;
    uint8 tti_enc_val;
    l1_ul_coding_enum_type cctrch_coding_type;
}mcalwcdma_wenc_write_trch_cfg1_ram_struct_type;

/*===========================================================================
FUNCTION        mcalwcdma_wenc_dpch_state_setup

DESCRIPTION     This function writes the Scrambling code, channel type 
                and endianness during DPCH setup. 

DEPENDENCIES    Inputs : Scrambling code, scrambling code type, phy chan type

RETURN VALUE    None

SIDE EFFECTS    1) Will update scrambling code in HW 
                2) Will set the endianness for R99 Data and the Fall time
                3) Will update the physical channel type based on input
===========================================================================*/

void mcalwcdma_wenc_dpch_state_setup(
   uint32 scrambling_code,
   l1_ul_dpch_sc_enum_type  scrambling_code_type,
   uint8 chan
   );

/*===========================================================================
FUNCTION        mcalwcdma_wenc_process_phychan_cfg_hsdpcch

DESCRIPTION     This function writes HS-DPCCH OVSF code to HW

DEPENDENCIES    Inputs : Number of DPDCH active 

RETURN VALUE    None

SIDE EFFECTS    Will set the HS-DPCCH OVSF based on if DPDCH is active or not
===========================================================================*/
void mcalwcdma_wenc_process_phychan_cfg_hsdpcch(uint8 num_dpdch);

/*===========================================================================
FUNCTION        mcalwcdma_wenc_dpch_tx_init

DESCRIPTION     This function is used to initialize DPCH parameters. 

DEPENDENCIES    None

RETURN VALUE    None

SIDE EFFECTS    1) Will initialize the ERAMA write pointer so writes to ERAMA 
                   after this point will be from the beginning of ERAMA
                2) Will write Phychan type to HW as DPCH
                3) Will clear active Transport Channels information in HW
                
===========================================================================*/
void mcalwcdma_wenc_dpch_tx_init( void );

/*===========================================================================
FUNCTION        mcalwcdma_wenc_erama_write_init

DESCRIPTION     In this function WL1 will initialize the parameters for writing to 
                ERAMA every TTI.
                1. Choose the Memory mapping (Using HW Default values, by prog 0)
                2. Program the Phychan type
                3. Program number of Active trchs and ERAMA trchs
                4. Program filler polarity
                5. Program endianness of data when written to ERAMA (Little End)

DEPENDENCIES    Inputs : Physical channel type
                         Number of active transport channels 
                         Num of transport channels to be written to ERAMA this frame
                         Filler polarity based on filler val(0 or 2)

RETURN VALUE    FALSE if encoder is busy, TRUE otherwise

SIDE EFFECTS    1) Reset ERAMA write address 
                2) Write the physical channel type
                3) Set the number of active and erama transport channels
                4) Set the filler polarity
===========================================================================*/
void mcalwcdma_wenc_erama_write_init(
   l1_ul_phychan_enum_type chan,
   uint8 num_active_trchs,
   uint8 num_erama_trchs,
   l1_ul_filler_polarity_enum_type filler_val);

/*===========================================================================
FUNCTION        mcalwcdma_wenc_phychan_start_encoding

DESCRIPTION     This function is used to trigger R99 encoding. 
                It programs which frame in the TTI is being programmed
                and the last row and column of data in RMRAM, before trigerring
                encoding.

DEPENDENCIES    Inputs : Current uplink state, TTI for PRACH, next radio frame mod 8 
                         longest active TTI incase of DPCH 
  

RETURN VALUE    None

SIDE EFFECTS    1) Reset ERAMA Write pointer 
                2) Write the frame number within the TTI that the encoding is for
                3) Encoding is triggered in HW
===========================================================================*/
void mcalwcdma_wenc_phychan_start_encoding(
   ul_state_enum_type wl1_ul_state,
   l1_tti_enum_type prach_tti,
   uint8 nrf,
   l1_tti_enum_type longest_active_tti
   );

/*===========================================================================
FUNCTION        mcalwcdma_wenc_phychan_tx_init

DESCRIPTION     This function programs phychan parameters, namely 
                1. Enable DPDCH or not
                2. Spreading Factor
                3. OVSF
                4. Scrambling code and Scrambling code type

DEPENDENCIES    Input : OVSF, Spreading factor, scrambling code, 
                        scrambling code type, phychan type

RETURN VALUE    None

SIDE EFFECTS    1) OVSF code is written for DPDCH 
                2) Uplink Scrambling code is written for PRACH and DPCH
                3) Spreading factor is written for both PRACH and DPCH
===========================================================================*/
void mcalwcdma_wenc_phychan_tx_init(
   uint16 dpch_ovsf,
   l1_sf_enum_type sf,
   uint32 prach_sc,
   l1_ul_dpch_sc_enum_type  dpch_scrambling_code_type,
   uint32 dpch_scrambling_code,
   l1_ul_phychan_enum_type ul_phychan
   );

/*===========================================================================
FUNCTION        mcalwcdma_wenc_prach_signal_tx

DESCRIPTION     This function basically sets phychan type to PRACH and sets 
                endianness. 

DEPENDENCIES    None

RETURN VALUE    None

SIDE EFFECTS    1) Physical Channel type is updated to PRACH
===========================================================================*/
void mcalwcdma_wenc_prach_signal_tx( void );

/*===========================================================================
FUNCTION        mcalwcdma_wenc_set_endianness

DESCRIPTION     This function sets endianness for the read back format of data 
                in ERAM. This needs to be set to 1 when accessing other TxMEM
                regions.

DEPENDENCIES    None

RETURN VALUE    None

SIDE EFFECTS    1) Set the endianness of data 
                2) Fall time for Tx conditioning block is set 
===========================================================================*/
void mcalwcdma_wenc_set_endianness_fall_time(void);

/*===========================================================================
FUNCTION        mcalwcdma_wenc_tx_abort

DESCRIPTION     This function programs phychan type to '0' to abort any tx 
                operation. 

DEPENDENCIES    None

RETURN VALUE    None

SIDE EFFECTS    Phychan type is cleared in HW
===========================================================================*/
void mcalwcdma_wenc_tx_abort( void );

/*===========================================================================
FUNCTION        mcalwcdma_wenc_wl1_enc_init

DESCRIPTION     This function is called during wl1_enc_init. 
                It initializes ERAM parameters and HS-DPCCH OVSF. 

DEPENDENCIES    None

RETURN VALUE    None

SIDE EFFECTS    1) Number of active and erama transport channels are cleared 
                2) ERAMA write location is initialized
                3) HS-DPCCH OVSF code is written with one DPDCH present
===========================================================================*/
void mcalwcdma_wenc_wl1_enc_init(void);

/*===========================================================================
FUNCTION        mcalwcdma_wenc_write_eul_tti_bank_idx

DESCRIPTION     This function writes the EUL ERAM bank index and TTI value to 
                HW and reinitializes the EUL INCRACC write location in EUL ERAM

DEPENDENCIES    Inputs : Carrier Index, Harq bank index, EUL TTI

RETURN VALUE    returns the value written to TX_EUL_ERAM_MEMORY_BANK HW register

SIDE EFFECTS    1) Updates HW register for HARQ Memory Bank 
                2) Reinitializes EUL INCRACC write address
===========================================================================*/
uint32 mcalwcdma_wenc_write_eul_tti_bank_idx(wl1_ul_carr_id_enum_type carr_idx, uint32 eram_bank_idx, e_tti_enum_type tti);

/*===========================================================================
FUNCTION        mcalwcdma_wenc_init_eul_hw

DESCRIPTION     This function writes the coding and the turbo interleaving 
                parameters to HW for EUL 

DEPENDENCIES    Inputs :  Carrier Index, 
                          HARQ bank index, 
                          EUL Pre rate matching parameters,
                          Turbo Interleaving parameters

RETURN VALUE    None

SIDE EFFECTS    1) Encoding Parameters are written to HW 
                2) Turbo Interleaving parameters are written to HW
                
===========================================================================*/
void mcalwcdma_wenc_init_eul_hw(
   wl1_ul_carr_id_enum_type carr_idx,
   uint32 eram_bank_idx, 
   const eul_pre_rm_param_struct_type* hw_coding_param_ptr, 
   uint32 turbo_interlev_info, 
   uint32 turbo_interlev_prime_info);

/*===========================================================================
FUNCTION        mcalwcdma_wenc_hsrach_pmbl_hw_cfg

DESCRIPTION     This function initializes some HW parameters for HS-RACH, 
                such as phychan type, endianness and Tx Mode

DEPENDENCIES    None

RETURN VALUE    None

SIDE EFFECTS    1) Endianness of data is written to HW 
                2) Fall time for TX conditioning block is written to HW
                3) Phychan type is set to PRACH
===========================================================================*/
void mcalwcdma_wenc_hsrach_pmbl_hw_cfg(void);

/*===========================================================================
FUNCTION        mcalwcdma_wenc_hsrach_process_tx_data

DESCRIPTION     This function programs basic parameters while HS_RACH 
                is in progress, such as ERAMA write pointer reinitialization,
                endianness control and phychan type configuration

DEPENDENCIES    None

RETURN VALUE    None

SIDE EFFECTS    1) ERAMA Write location is initialized 
                2) Phychan type is set to PRACH in HW
===========================================================================*/
void mcalwcdma_wenc_hsrach_process_tx_data(void);

/*===========================================================================
FUNCTION        mcalwcdma_wenc_wl1ulhsdpchmgr_activate_init

DESCRIPTION     This function performs HW register writes during the activation 
                of DPCH channels during FACH state as part of HS RACH 

DEPENDENCIES    None

RETURN VALUE    None

SIDE EFFECTS    1) ERAMA Write location is initialized 
                2) Phychan type is set to DPCH in HW
                3) Clear number of Active and ERAMA Transport Channels in HW
===========================================================================*/
void mcalwcdma_wenc_wl1ulhsdpchmgr_activate_init(void);

/*===========================================================================
FUNCTION        mcalwcdma_wenc_write_erama_data_per_tb

DESCRIPTION     This function performs actual data writes to HW. 
                It first write Ciphering control words
                Second writes mac header data
                Finally writes the transport block data

DEPENDENCIES    Inputs : Transport channel index, transport channel data, 
                         transport block size, ciphering mode, total bits to be
                         ciphered

RETURN VALUE    FALSE if tb size is 0, TRUE otherwise

SIDE EFFECTS    1) Write Ciphering control parameters to HW 
                2) Write actual per transport block data to HW
                3) Update number of words written to HW for debugging (readback)
===========================================================================*/
boolean mcalwcdma_wenc_write_erama_data_per_tb(
   uint8 trch_idx,
   l1_ul_tb_data_type *trch_data,
   uint16 tb_size,
   uint32 ciphering_mode,
   uint16 total_bits_to_cipher
   );

/*===========================================================================
FUNCTION        mcalwcdma_wenc_write_scrambling_code

DESCRIPTION     This function writes scrambling code to HW by calling API 
                provided by HW team. 

DEPENDENCIES    Inputs : Scrambling code, scrambling code type

RETURN VALUE    None

SIDE EFFECTS    Scrambling code is updated in HW
===========================================================================*/
void mcalwcdma_wenc_write_scrambling_code(
   uint32 scrambling_code,
   l1_ul_dpch_sc_enum_type  scrambling_code_type);

/*===========================================================================
FUNCTION        mcalwcdma_wenc_write_trch_cfg1_ram

DESCRIPTION     This function writes rate matching, encoding and interleaving 
                parameters to config ram for DPDCH and PRACH message per trch. 

DEPENDENCIES    Inputs : 
                Transport Channel Index 
                Rate Matching Parameters.
                  EINI, EMINUS, EPLUS
                Encoding Parameters
                  PRE RM BITS, TTI, CODING TYPE, PUNCTURING FLAG, NUM CODE SEGMENTS,
                  NUMBER OF FILLER BITS, NUMBER OF BITS PER CODE SEGMENTS
                Turbo Interleaving Parameters
                  NUM COLUMNS, LAST COLUMN, NUM ROW INDEX, LAST ROW BIT EXCHANGE
                  LAST ROW, PRIME NUMBER, PRIME NUMBER INDEX
                Mux order configuration
                  ERAMB START ADDRESS

RETURN VALUE    None

SIDE EFFECTS    Update Config_1 RAM parameters per transport channel
===========================================================================*/
void mcalwcdma_wenc_write_trch_cfg1_ram
(
   mcalwcdma_wenc_write_trch_cfg1_ram_struct_type *mcalwcdma_wenc_write_trch_cfg1_ram_val
   );


/*===========================================================================

FUNCTION        mcalwcdma_wenc_trch_mux_order_cfg

DESCRIPTION     Transport channel order for R99 encoding per transport channel

DEPENDENCIES    Inputs : Transport channel index, ERAMB read start address

RETURN VALUE    None

SIDE EFFECTS    1) Mux order config in HW is updated with Transport channel index 
                   and ERAMB start address for this transport channel
===========================================================================*/

void mcalwcdma_wenc_trch_mux_order_cfg
     (
      uint8			trch_index,          //trch processing order
      uint16			eramb_rd_start_addr  //eramb read for the corresponding trch index
     );

/*===========================================================================

FUNCTION      mcalwcdma_wenc_trch_crc_params_cfg

DESCRIPTION   CRC parameters for R99 per transport channel
              If data is 1x0, then crc size is written to tb_size field.

DEPENDENCIES  Inputs : Transport channel index, 
                       transport block size,
                       crc size,
                       number of transport blocks,
                       eramb write start address

RETURN VALUE   None

SIDE EFFECTS   Updates CRC parameters in HW
===========================================================================*/
void mcalwcdma_wenc_trch_crc_params_cfg
(
   uint8   trch_idx,                  //transport ch index
   uint16  crc_trblk_size,
   uint8   crc_size,
   uint8   crc_num_trblks,
   uint16  crc_eramb_wr_start_addr
   );

/*===========================================================================

FUNCTION       mcalwcdma_wenc_trch_coding_params_cfg

DESCRIPTION    Transport channel coding parameters for R99 per transport channel

DEPENDENCIES   Inputs : Transport Channel index, 
                        number of pre rate matching bits,
                        transport channel TTI,
                        number of code segments,
                        coding type and rate,
                        puncturing flag,
                        number of filler bits, 
                        number of code segments

RETURN VALUE   None 

SIDE EFFECTS  Updates Encoding parameters in HW
===========================================================================*/
void mcalwcdma_wenc_trch_coding_params_cfg
     (
        uint8   trch_idx,
        uint32  enc_num_pre_rm_bits,   
        uint8     enc_trch_tti,          
        uint8     enc_num_code_segs,     
        l1_ul_coding_enum_type     enc_coding_rate_type,  
        boolean     enc_puncture_flag,     
        uint32     enc_num_filler_bits,   
        uint32     enc_num_bits_code_seg 
     );


/*===========================================================================

FUNCTION      mcalwcdma_wenc_rm_params_cfg

DESCRIPTION   Programming rate matching params for R99 per transport channel

DEPENDENCIES  Inputs : Transport channel index, Rate matching parameters 

RETURN VALUE  None

SIDE EFFECTS  Updates the Rate matching parameters in HW
===========================================================================*/
void mcalwcdma_wenc_rm_params_cfg
     (
       uint8   trch_idx,   //transport ch index
       uint32  einit2,  
       uint32  einit1,  
       uint32  eminus2, 
       uint32  eminus1, 
       uint32  eplus2, 
       uint32  eplus1
     );

/*===========================================================================

FUNCTION      mcalwcdma_wenc_tintlv_params_cfg

DESCRIPTION   Turbo interleaver parameters for R99 per transport channel

DEPENDENCIES  Inputs : Tranport channel index, 
                       Turbo interleaving parameters  

RETURN VALUE  None 

SIDE EFFECTS  Updates the Turbo interleaving parameters in HW
===========================================================================*/
void mcalwcdma_wenc_tintlv_params_cfg
     (
      uint8   trch_idx,                  //transport ch index
      uint16  enc_num_columns_minus1,  
      uint16  enc_last_column,         
      uint16  enc_num_row_index,       
      uint16  enc_last_row_bit_exch,   
      uint16  enc_last_row,            
      uint16  enc_prime_num_index, 
      uint16  enc_prime_number
     );

/*===========================================================================

FUNCTION      mcalwcdma_wenc_cipher_keys_cfg

DESCRIPTION   Programming a single ciphering key for R99

DEPENDENCIES  Inputs : Ciphering key index, 
                       128 Bit ciphering key,
                       boolean to specify whether keys should be printed to diag

RETURN VALUE  None

SIDE EFFECTS  Updates HW register corresponding to key index provided, 
              with the ciphering key given in input
===========================================================================*/
void mcalwcdma_wenc_cipher_keys_cfg
     (
       uint8   cipher_key_num,   //specifies the cipher key# from 0-5
       uint32  cipher_key[4],     //128 bit cipher key
       boolean print_ciph_msg
     );

#endif /* __MCALWCDMA_WENC_H__ */
