#ifndef STMR_H
#define STMR_H
/*===========================================================================

     S I M B A    L 1    S Y S T E M    T I M E R    H E A D E R    F I L E

DESCRIPTION
  The System Timer Driver module contains the data structure declarations
  and function prototypes to interface with the System Timer Block.

  The System Timer block generates all the frame related timing
  (frame/symbol) signals. It also maintains TX System Time within 80ms range
  in 1/8 chip resolution.
  
  The System Timer block provides registers to set and maintain System Time.
  
  The System Timer block provides three Programmable Interrupts, with the
  ability to schedule Events at programmable Firing Times.
  
  The System Timer Driver provides other interfaces for the units within L1
  to interact with the System Timer block.
  
EXTERNALIZED FUNCTIONS

  stmr_init
    This function sets up the interrupt handlers for the three Programmable
    Interrupts.

  stmr_set_isr
    This function allows the clients to set a handler to each
    of the Programmable Interrupts. The registered handler will be called
    whenever there is an Event on the particular interrupt. The Event Number
    will be passed to the handler.

  stmr_set_event_frame
    This function allows the user to set an Event Frame on a specific
    programmable interrupt. The Event Frames are periodic and has the
    following parameters:
    Intr                   - specifies the programmable interrupt
    Frame Ref Sel          - for STMR_PROG_INT0 and STMR_PROG_INT1, specifies
                             whether to use the Frame Reference Counter or
                             to use one of 4 Combiner Counters.
                             For STMR_PROG_INT2, specifies whether to use the
                             Frame Reference Counter or the TX System Time.
    Offset                 - specifies the Frame Offset relative to the
                             current reference counter in units of 256 chips.
                             The offset must be less than or equal to the Frame
                             Period. The time of all Events is advanced by the
                             amount of the offset.
    Event Mask             - specifies which Event is enabled. Bit N is set
                             true to enable event N on the specified STMR
                             interrupt.                      
  
    This function requires that the interrupt source is driven by one of 4
    combiner counters or the frame reference counter and further that the
    combiner counter is slaved to a physical channel. This should be done
    by a call to the function stmr_align_counter.

  stmr_align_counter
    This function aligns the specified interrupt source to the specified
    counter source.
    counter sources for int0 and int1
    STMR_COMBINER_COUNTER0
    STMR_COMBINER_COUNTER1
    STMR_COMBINER_COUNTER2
    STMR_COMBINER_COUNTER3
    counter source for int2 only
    STMR_TX_SYSTEM_TIME_COUNTER
    counter source for int0, int1, and int2
    STMR_FRAME_REF_COUNTER
  
  stmr_set_event_firing_times
    This function sets the Firing Times of Events on the specified interrupt
    source. The Events to set are specified in an Event Mask. The Firing Times
    of the Events are specified in an array. Element N of the array corresponds
    to Event N.
  
    The Event Firing Times are specified relative to the start of the offset
    Event Frame in units of 256 chips. The Firing Time must be less than the
    interrupt frame length (10 ms).

  stmr_add_event
    This function adds the specified Event on the specified interrupt source
    and sets the Firing Time of the Event to the specified value. If the
    Event was already present, it will have the value of the new Firing
    Time.

    The Event Mask specifies which Event should be added. Bit N of the mask
    corresponds to Event N. The Event Mask should only have one Event set.
    If more than one bit is set in the Event Mask, only the Event that
    corresponds to the first bit set from the LSB is added.

    The Event Firing Time is specified relative to the start of the offset
    Event Frame in units of 256 chips. The Firing Time must be less than the
    interrupt frame length (10 ms).

  stmr_disable_events
    This function disables the specified Event from the Programmable Interrupt
    specified. It reads the current Event Mask from the Frame Register and
    then unsets the Mask of the Events to be disabled.
  
    There is no need to reset the Firing Times in the Event Registers, since
    the Events will not fire if it is not programmed in the Frame Register.

  stmr_enable_events
    This function will enable the Events specified in the Event Mask on the
    specified interrupt source. Bit N of the mask corresponds to Event N.
    The Firing Times of the Events should already be programmed.

  stmr_modify_event_firing_time
    This function modifies the Firing Time of the specified Event
    on the specified Programmable Interrupt Source. The Event mask
    for this Event should already have been programmed in the Event Frame
    Register for this Programmable Interrupt.

  stmr_get_event_num
    This function returns the Event Number of an Event given a Event Mask.
    Bit N of the Event Mask corresponds to Event Number N (Event N).
    The parameter Event Mask is assumed to have only one bit set.
    If more than one bit in the Event Mask is set, only the
    Event Number corresponding to the first (least significant bit) bit
    set is returned.

  stmr_get_system_time
    This function gets the System Time from the System Time Status register.

  stmr_get_events_and_enable_only
    This function enables a set of events   
	for the specified interrupt. Additionally, it returns the mask of events 
	that were enabled on the interrupt just prior to calling this function.
    

INTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS
  stmr_init() should be called before any of the functions can be used.

REFERENCES
  CDMA Mobile Station ASIC Specification Sheet

Copyright (c) 2000 - 2010 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$PVCSPath:  L:/src/asw/MSM5200/l1/vcs/stmr.h_v   1.12   21 May 2002 19:06:44   cemani  $
$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/wcdma/l1/offline/inc/stmr.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when        who   what, where, why
--------    ---   --------------------------------------------------------
11/14/13    pj    Wrapped HWIO access with WL1_HWIO macro
05/09/13    pr    Featurized code under FEATURE_WCDMA_BOLT_RUMI_HWIO_DEMOD as part of Bolt Bringup.
02/14/12    pr    Remove Tx Timeline Check for STMR counter source.
07/24/12    geg   Upmerge WCDMA.MPSS.1.0 to WCDMA.MPSS.2.0
06/05/12    abs   STMR hw_sw counters mapping changes for Dime
05/22/12    pr    STMR changes for Dime
03/28/12    zr    Added feature definitions for Dime
04/17/12    pr    Removed inclusion of "clk.h"
04/16/11    sj    Added #include "rex.h" to resolve compilation errors
03/23/11    hk    Removed the clk_busy_wait of 1us from STMR_STATUS_DUMP
02/04/11    dm    Added mutex protection to stmr_status_dump.
02/26/10    rvs   Fixes for Mustang.
11/23/09    rvs   Added function stmr_debug_print_sfn_cfn_update_info().
11/20/09    rvs   Extern stmr_prog_int_num_to_intr and stmr_cdma_prog_intr_isr.
09/14/09    sv    Removed WCDMA_INTLOCK's.
12/24/08    vsr   Added support for maintaining event processing status
12/10/08    ks    Mainlining FEATURE_WCDMA_DL_ENHANCED
10/03/08    hk    Bringing in 7k Mailine fixes
11/05/07    mg    Taxis stmr changes
12/11/07    mg    Use HWIO_ADDRI to access stmr registers
11/27/07    mg    Fix STMR_FRAME_NUM_STATUS() macro
10/24/07    vsr   Mainlining MBMS and Enhanced DL changes 
09/06/07    gnk   Added support for combiner log packet when FEATURE_WCDMA_DL_ENHANCED
                  is not defined
01/31/07    vr    Added STMR_COMBINER_COUNT_STATUS macro
                  Added function stmr_get_combiner_cfn() to return cfn of a 
                  combiner from the corresponding STMR register 
12/22/06    rmak  Added STMR_FRAME_NUM_STATUS, STMR_FRAME_REF_COUNT_DYNAMIC and updated STMR_FRAME_NUM_DYNAMIC
11/16/06    rmak  Added STMR_FRAME_NUM_DYNAMIC macro
09/12/06    gr/sh Removed macros to convert stmr ints to/from tramp ints.
05/08/06    au    Replaced calls to MSM_IN and MSM_OUT with HWIO_IN and HWIO_OUT.
09/14/05    gs    Updates for 6280 register name changes
05/31/05    vp    Merging of fixes for Lint errors
11/11/03    yus   Replace FEATURE_6250_COMPILE with FEATURE_MAP_6250_DEFINES 
09/29/03    gw    Added STMR_WALL_TIME_LOAD_ST_EVENT to commands for
                  STMR_LOAD_FRAME_REF_COUNTER macro.
07/24/03    yus   Map tramp_int_num_to type tramp_isr_type for MSM6250.
06/10/03    yus     Add HWIO_XXXX style macros support.
01/13/02    src   Added a new macro STMR_STATUS_DUMP_NO_WAIT(), which differs
                  from the existing macro STMR_STATUS_DUMP() in that the former
                  does not automatically do a clock-wait.
12/06/02    gw    Added STMR_WALL_TIME_LOAD_ON_LINE_START to commands for
                  STMR_LOAD_FRAME_REF_COUNTER macro.
10/30/02    src   Added function declaration for stmr_get_event_mask, a new
                  function.
07/26/02    sh    Modified STMR_STATUS_DUMP to include 1us delay for hardware
                  to latch.
05/21/02    src   Re-instated the frame-reference bit-mask in place of the full
                  read-mask in the macro STMR_FRAME_REF_COUNT_STATUS().
05/19/02    src   In enum stmr_counter_source_enum_type, added a last element
                  to count the numre of counter sources.
02/11/02    asr   Added prototype for function stmr_get_events_and_enable_only()
10/10/01    sh    Modified macros using write-only registers to use MSM_OUT.
04/30/01    sh    Added STMR_MAX_EVENT_FIRING_TIME constant define.
01/23/01    ml    Added ability to register/de-register stmr isrs with
                  tramp.
11/16/00    sh    Added support to align counter sources to phy chan.
                  Modified TX system time macros.
                  Modified STMR_IN STMR_OUT macros to use OUTP/INP macros.
10/25/00    sh    Added TX System Time related macros.
09/27/00    sh    Created file.

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "wcdma_variation.h"
#include "comdef.h"
#include "msm.h"
#ifdef FEATURE_WCDMA_DAL_INT_CONTROLLER
#include "DALSys.h"
#include "DDIInterruptController.h"
#include "DalDevice.h"
#include "DALDeviceId.h"
#include "DALStdErr.h"
#else
#include "tramp.h"
#include "clk.h"
#endif /* FEATURE_WCDMA_DAL_INT_CONTROLLER */
#include "l1const.h"
#include "rex.h"
#include "l1msm.h"

#ifndef FEATURE_WCDMA_SYSTEM_TIMER_REV3
#define FEATURE_WCDMA_STMR_INT3_SUPPORT
#endif
/*===========================================================================

                        DATA DECLARATIONS

===========================================================================*/

/*-------------------------------------------------------------------------*/
/*             System Timer Programmable Interrupts Declarations           */
/*-------------------------------------------------------------------------*/
/*
 * System Timer provides three programmable interrupts to the microprocessor.
 * The following enumerations specify the interrupt sources.
 * Programmable Interrupt 0 (STMR0) has 16 Events associated with it.
 * Programmable Interrupt 1 (STMR1) has 08 Events associated with it.
 * Programmable Interrupt 2 (STMR2) has 08 Events associated with it.
 */
typedef enum {
  STMR_INT_0,  /* Programmable Interrupt 0 */
  STMR_INT_1,  /* Programmable Interrupt 1 */
  STMR_INT_2,  /* Programmable Interrupt 2 */
#ifdef FEATURE_WCDMA_STMR_INT3_SUPPORT
  STMR_INT_3,  /* Programmable Interrupt 3 */
#endif
  STMR_NUM_INT
} stmr_int_type;

/* this macro is used to convert a stmr_int_type enum, as defined above, to 
   it's corresponding actual STMR interrupt number as defined in tramp.h */

#ifdef FEATURE_WCDMA_DAL_INT_CONTROLLER
typedef void (*tramp_void_handler_type) (void);
typedef tramp_void_handler_type isr_ptr_type;
#define DALInterruptID tramp_isr_type
#else
#define tramp_int_num_type tramp_isr_type
#endif /* FEATURE_WCDMA_DAL_INT_CONTROLLER */

/* The following are register definitions depending on the System timer revision */

/* Currently these register definitions are in taxis modem system timer */

#define STMR_REF_COUNT_STATUS_REG     STMR_REF_COUNT_STATUS_PRI
#define STMR_REF_COUNT_REG            STMR_REF_COUNT
#define STMR_TX_TIME_STATUS_REG       STMR_TX_TIME_STATUS_PRI
#define STMR_STATUS_DUMP_CMD_REG      STMR_STATUS_DUMP_CMD_PRI
#define STMR_Cd_COUNT_STATUS_REG      STMR_Cd_COUNT_STATUS
#define STMR_TIME_CMD_REG             STMR_TIME_CMD_PRI

/*
 * The Events on the Programmable Interrupts are specified by a bit mask.
 * Bit N specifies Event N on the specified interrupt.
 * Following are the definitions of the bit mask for each of the Events.
 * STMR0 has 16 events and STMR1/STMR2 have 8 events.
 */
#define STMR_INT_EVENT_0  0x1
#define STMR_INT_EVENT_1  0x2
#define STMR_INT_EVENT_2  0x4
#define STMR_INT_EVENT_3  0x8
#define STMR_INT_EVENT_4  0x10
#define STMR_INT_EVENT_5  0x20
#define STMR_INT_EVENT_6  0x40
#define STMR_INT_EVENT_7  0x80
#define STMR_INT_EVENT_8  0x100
#define STMR_INT_EVENT_9  0x200
#define STMR_INT_EVENT_10 0x400
#define STMR_INT_EVENT_11 0x800
#define STMR_INT_EVENT_12 0x1000
#define STMR_INT_EVENT_13 0x2000
#define STMR_INT_EVENT_14 0x4000
#define STMR_INT_EVENT_15 0x8000
  
/* max number of events on each of the interrupt sources */
#define STMR_MAX_NUM_EVENTS 16
#define STMR_INT0_NUM_EVENTS  16
#define STMR_INT1_NUM_EVENTS  16
#define STMR_INT2_NUM_EVENTS  16
#define STMR_INT3_NUM_EVENTS  16

/* This Macro holds an invalid CFN value.*/
#define STMR_INVALID_CFN 0xFF

/* Max value of the Firing Time for any Event in units of 256 Chips */
#define STMR_MAX_EVENT_FIRING_TIME 149

#define STMR_MAX_EVENT_FIRING_TIME_IN_CHIPS (38400 -1)

/* This Macro holds the value that needs to be written to STMR_CFN_MODIFIER_SEL
 * field so that microprocessor can take control of the CFN field modifier.
*/
#define STMR_GET_MICRO_ACCESS_FOR_CFN_MODIFIER 0x1

/* counter source to slave the Programmable interrupts to;
 * These are the non-hsdpa counters; They are numbered according to the
 * mdsp convention
 */
typedef enum
{
  /* counter sources for int0 and int1 */
  STMR_COMBINER_COUNTER0=0,
  STMR_COMBINER_COUNTER1,
  STMR_COMBINER_COUNTER2,
  /* counter source for int2 only */
  STMR_TX_SYSTEM_TIME_COUNTER,

  /* counter source for int0, int1, and int2 */
  STMR_FRAME_REF_COUNTER,
  STMR_MAX_NUM_COUNTERS
} stmr_counter_source_enum_type;

#ifdef FEATURE_WCDMA_DAL_INT_CONTROLLER
extern uint32 stmr_prog_int_num_to_intr[STMR_NUM_INT];
#else
extern tramp_int_num_type stmr_prog_int_num_to_intr[STMR_NUM_INT];
#endif
extern isr_ptr_type stmr_cdma_prog_intr_isr[STMR_NUM_INT];

extern rex_crit_sect_type stmr_status_dump_mutex;

#define STMR_STATUS_DUMP_LOCK()  REX_ISR_LOCK(&stmr_status_dump_mutex)
#define STMR_STATUS_DUMP_UNLOCK()  REX_ISR_UNLOCK(&stmr_status_dump_mutex)


/*The following combiners are supported:
  Dime - 3
  Pre-Dime - 4*/
#ifdef FEATURE_WCDMA_SYSTEM_TIMER_REV3
#define STMR_SUPPORTED_COMBINERS 3
#else
#define STMR_SUPPORTED_COMBINERS 4
#endif

#ifndef FEATURE_WCDMA_BOLT_RUMI_HWIO_DEMOD
/* This macro reads the FRAME_REF_COUNTER_STATUS after a status dump The Frame
 * Reference Counter status carries the frame reference counter in chipx8
 * units.
 */
#define STMR_FRAME_REF_COUNT_STATUS( )                               \
  (                                                                  \
    WL1_HWIO_INM                                                         \
    (                                                                \
      STMR_REF_COUNT_STATUS_REG,                                     \
      WL1_HWIO_FMSK(STMR_REF_COUNT_STATUS_REG, FRAME_REF_COUNT)          \
    )                                                                \
  )


#define STMR_FRAME_NUM_STATUS( )                                     \
  (                                                                  \
    WL1_HWIO_INM                                                         \
    (                                                                \
      STMR_REF_COUNT_STATUS_REG,                                     \
      WL1_HWIO_FMSK(STMR_REF_COUNT_STATUS_REG, FRAME_NUM)                \
    )                                                                \
    >> WL1_HWIO_SHFT(STMR_REF_COUNT_STATUS_REG, FRAME_NUM )              \
  )

/* This macro reads the FRAME_REF_STATUS after a status dump The Frame
 * Reference status carries the frame_num, slot_num and frame reference 
 * counter in chipx8 units. The caller has to mask and extract the individual
 * values
 */
#define STMR_FRAME_REF_STATUS( )                                     \
  (                                                                  \
    WL1_HWIO_IN                                                          \
    (                                                                \
      STMR_REF_COUNT_STATUS_REG                                      \
    )                                                                \
  )

  #define STMR_COMBINER_COUNT_STATUS(combiner)                       \
  (                                                                  \
    WL1_HWIO_INMI                                                        \
    (                                                                \
     STMR_Cd_COUNT_STATUS_REG, combiner, WL1_HWIO_RMSK(STMR_Cd_COUNT_STATUS_REG)    \
    )                                                                \
  )

  #define STMR_TX_SYS_TIME_STATUS()                                  \
  (                                                                  \
    WL1_HWIO_IN                                                          \
    (                                                                \
      STMR_TX_TIME_STATUS_REG                                        \
    )                                                                \
  )

/* NOTE: This function will not provide correct results
 * The subframe is not counting and hw folks are notified.
 */
#define STMR_COMBINER_CFN_SUB_FRAME_DYNAMIC( counter  )              \
  (                                                                  \
    WL1_HWIO_INMI                                                        \
    (                                                                \
      STMR_COMBINER_COUNTn_PRI,counter,                              \
      WL1_HWIO_FMSK(STMR_COMBINER_COUNTn_PRI, C_CFN_SUBFRAME_NUM)        \
    )                                                                \
    >> WL1_HWIO_SHFT(STMR_COMBINER_COUNTn_PRI, C_CFN_SUBFRAME_NUM)       \
  )

/* The following macros return the dynamic value
 * of the Combiner counts in 256 chip resolution.
 */

#define STMR_COMBINER_COUNT_DYNAMIC( counter  )                      \
  (                                                                  \
    WL1_HWIO_INMI                                                        \
    (                                                                \
      STMR_COMBINER_COUNTn_PRI,counter,                              \
      WL1_HWIO_FMSK(STMR_COMBINER_COUNTn_PRI, C_COUNT)                   \
    )                                                                \
    >> WL1_HWIO_SHFT(STMR_COMBINER_COUNTn_PRI, C_COUNT)                  \
  )

#define STMR_COMBINER0_COUNT_DYNAMIC( ) STMR_COMBINER_COUNT_DYNAMIC(0)
#define STMR_COMBINER1_COUNT_DYNAMIC( ) STMR_COMBINER_COUNT_DYNAMIC(1)
#define STMR_COMBINER2_COUNT_DYNAMIC( ) STMR_COMBINER_COUNT_DYNAMIC(2)

#define STMR_SLOT_REF_COUNT_DYNAMIC()                                \
  (                                                                  \
    (                                                                \
      ( WL1_HWIO_INM( STMR_REF_COUNT_REG,                                \
           WL1_HWIO_FMSK(STMR_REF_COUNT_REG, FRAME_REF_COUNT) )         \
          >> WL1_HWIO_SHFT(STMR_REF_COUNT_REG, FRAME_REF_COUNT ) )       \
        >> 8                                                         \
    ) / STMR_TICKS_PER_SLOT                                          \
  )


/* The following macro return the dynamic values of Frame Reference Counter
 * in chipx1 units.
 */
#define STMR_FRAME_REF_COUNT_DYNAMIC( )                              \
  (                                                                  \
    WL1_HWIO_INM                                                         \
    (                                                                \
      STMR_REF_COUNT_REG,                                            \
      WL1_HWIO_FMSK(STMR_REF_COUNT_REG, FRAME_REF_COUNT)                 \
    )                                                                \
    >> WL1_HWIO_SHFT(STMR_REF_COUNT_REG, FRAME_REF_COUNT)                \
  )

#define STMR_FRAME_NUM_DYNAMIC( )                                    \
  (                                                                  \
    WL1_HWIO_INM                                                         \
    (                                                                \
      STMR_REF_COUNT_REG,                                            \
     WL1_HWIO_FMSK(STMR_REF_COUNT_REG, FRAME_NUM)                       \
    )                                                                \
    >> WL1_HWIO_SHFT(STMR_REF_COUNT_REG, FRAME_NUM)                      \
  )

#define STMR_REF_COUNT_DYNAMIC( )                                    \
  (                                                                  \
    WL1_HWIO_IN                                                          \
    (                                                                \
      STMR_REF_COUNT_REG                                             \
    )                                                                \
  )

#else

#define STMR_FRAME_REF_COUNT_STATUS( )                               \
  (                                                                  \
  0 \
  )


#define STMR_FRAME_NUM_STATUS( )                                     \
  (                                                                  \
  0 \
  )

/* This macro reads the FRAME_REF_STATUS after a status dump The Frame
 * Reference status carries the frame_num, slot_num and frame reference 
 * counter in chipx8 units. The caller has to mask and extract the individual
 * values
 */
#define STMR_FRAME_REF_STATUS( )                                     \
  (                                                                  \
  0 \
  )

  #define STMR_COMBINER_COUNT_STATUS(combiner)                       \
  (                                                                  \
  combiner \
  )

  #define STMR_TX_SYS_TIME_STATUS()                                  \
  (                                                                  \
  0 \
  )

/* NOTE: This function will not provide correct results
 * The subframe is not counting and hw folks are notified.
 */
#define STMR_COMBINER_CFN_SUB_FRAME_DYNAMIC( counter  )              \
  (                                                                  \
  counter \
  )

/* The following macros return the dynamic value
 * of the Combiner counts in 256 chip resolution.
 */

#define STMR_COMBINER_COUNT_DYNAMIC( counter  )                      \
  (                                                                  \
  counter \
  )

#define STMR_COMBINER0_COUNT_DYNAMIC( ) STMR_COMBINER_COUNT_DYNAMIC(0)
#define STMR_COMBINER1_COUNT_DYNAMIC( ) STMR_COMBINER_COUNT_DYNAMIC(1)
#define STMR_COMBINER2_COUNT_DYNAMIC( ) STMR_COMBINER_COUNT_DYNAMIC(2)

#define STMR_SLOT_REF_COUNT_DYNAMIC()                                \
  (                                                                  \
  MSG_ERROR("WCDMA_BOLT_DEMOD:Commented for Bringup", 0, 0, 0); \
  )


/* The following macro return the dynamic values of Frame Reference Counter
 * in chipx1 units.
 */
#define STMR_FRAME_REF_COUNT_DYNAMIC( )                              \
  (                                                                  \
  0 \
  )

#define STMR_FRAME_NUM_DYNAMIC( )                                    \
  (                                                                  \
  0 \
  )

#define STMR_REF_COUNT_DYNAMIC( )                                    \
  (                                                                  \
  0 \
  )

#endif /*FEATURE_WCDMA_BOLT_RUMI_HWIO_DEMOD*/

/* Macro to load the value of the Modifier register into the Frame
 * Reference Counters
 * If cmd is STMR_WALL_TIME_LOAD_IMMEDIATE, load the contents of
 * the Modifier register into the Frame Reference Counter immediately.
 */
#define STMR_WALL_TIME_LOAD_IMMEDIATE        0x4
#define STMR_WALL_TIME_LOAD_ON_LINE_START    0x5
#define STMR_WALL_TIME_LOAD_ST_EVENT         0x6

#ifndef FEATURE_WCDMA_BOLT_RUMI_HWIO_DEMOD

#ifdef FEATURE_WCDMA_SYSTEM_TIMER_REV2

#define STMR_LOAD_FRAME_REF_COUNTER( cmd ) \
  WL1_HWIO_OUT( STMR_TIME_CMD_REG, (WL1_HWIO_FMSK( STMR_TIME_CMD_REG, WALL_TIME_CMD) & cmd) )

#else
#error code not present
#endif /*FEATURE_WCDMA_SYSTEM_TIMER_REV2*/

#else

#ifdef FEATURE_WCDMA_SYSTEM_TIMER_REV2

#define STMR_LOAD_FRAME_REF_COUNTER( cmd ) \
  MSG_ERROR("WCDMA_BOLT_DEMOD:Commented for Bringup , counter %d ", cmd, 0, 0);

#else
#error code not present
#endif /*FEATURE_WCDMA_SYSTEM_TIMER_REV2*/

#endif /*FEATURE_WCDMA_BOLT_RUMI_HWIO_DEMOD*/

#ifndef FEATURE_WCDMA_BOLT_RUMI_HWIO_DEMOD
/* This macro sets the STMR_WALL_TIME_MODIFIER register with
 * Slot Counter and Frame Counter values. The mapping is
 * <21:19> Slot Counter
 * <18:0>  Frame Counter
 */
#define STMR_SET_WALL_TIME_MODIFIER( slot_counter, frame_counter )   \
  WL1_HWIO_OUT                                                           \
  (                                                                  \
    STMR_WALL_TIME_MODIFIER,                                         \
    ( (slot_counter << 19) | (frame_counter))                        \
  )

#else
#define STMR_SET_WALL_TIME_MODIFIER( slot_counter, frame_counter )   \
  MSG_ERROR("WCDMA_BOLT_DEMOD:Commented for Bringup ,slt ctr%d  fr ctr %d", slot_counter, frame_counter, 0);
#endif /*FEATURE_WCDMA_BOLT_RUMI_HWIO_DEMOD*/

#ifdef FEATURE_WCDMA_SYSTEM_TIMER_REV3
#define STMR_CNTR_MAX_COMB_CNTR (STMR_COMBINER_COUNTER2)
#else
#define STMR_CNTR_MAX_COMB_CNTR (STMR_COMBINER_COUNTER3)
#endif /* FEATURE_WCDMA_SYSTEM_TIMER_REV3 */

#define STMR_IS_CNTR_IN_COMB_CNTR_RANGE(cntr)                        \
    (cntr <= STMR_CNTR_MAX_COMB_CNTR)

#define STMR_IS_CNTR_EQ_TO_TX_CNTR(cntr)                             \
    (cntr == STMR_TX_SYSTEM_TIME_COUNTER)

#define STMR_IS_CNTR_EQ_TO_FRC(cntr)                                 \
    (cntr == STMR_FRAME_REF_COUNTER)

#define STMR_IS_INTR_RX_TL(intr)    (intr == MCALWCDMA_EVT_RX_TL_INTR)
#define STMR_IS_INTR_GEN_TL(intr)    (intr == MCALWCDMA_EVT_GEN_TL_INTR)

#define STMR_IS_INTR_TX_TL(intr)    (intr == MCALWCDMA_EVT_TX_TL_INTR)

#ifdef FEATURE_WCDMA_STMR_INT3_SUPPORT
#define STMR_IS_INTR_RX_EXT_TL(intr)    (intr == MCALWCDMA_EVT_RX_EXT_TL_INTR)

#define STMR_IS_CNTR_SRC_WRONG_FOR_RX_EXT_TL(intr, cntr_src)         \
    ((STMR_IS_INTR_RX_EXT_TL(intr)) && (STMR_IS_CNTR_EQ_TO_TX_CNTR(cntr_src)))
#endif

#define STMR_IS_CNTR_SRC_WRONG_FOR_RX_TL(intr, cntr_src)             \
    (STMR_IS_INTR_RX_TL(intr) && (STMR_IS_CNTR_EQ_TO_TX_CNTR(cntr_src)))

#define STMR_IS_CNTR_SRC_WRONG_FOR_GEN_TL(intr, cntr_src)            \
    (STMR_IS_INTR_GEN_TL(intr) && (STMR_IS_CNTR_EQ_TO_TX_CNTR(cntr_src)))

#define STMR_IS_CNTR_SRC_WRONG_FOR_TX_TL(intr, cntr_src)             \
    (STMR_IS_INTR_TX_TL(intr) && (STMR_IS_CNTR_IN_COMB_CNTR_RANGE(cntr_src)))

#ifdef FEATURE_WCDMA_STMR_INT3_SUPPORT
#define STMR_IS_CNTR_SRC_WRONG(intr, cntr_src)                       \
    (STMR_IS_CNTR_SRC_WRONG_FOR_RX_TL(intr, cntr_src) ||             \
    STMR_IS_CNTR_SRC_WRONG_FOR_GEN_TL(intr, cntr_src) ||             \
    STMR_IS_CNTR_SRC_WRONG_FOR_TX_TL(intr, cntr_src) ||              \
    STMR_IS_CNTR_SRC_WRONG_FOR_RX_EXT_TL(intr, cntr_src))
#else
#define STMR_IS_CNTR_SRC_WRONG(intr, cntr_src)                       \
    (STMR_IS_CNTR_SRC_WRONG_FOR_RX_TL(intr, cntr_src) ||             \
    STMR_IS_CNTR_SRC_WRONG_FOR_GEN_TL(intr, cntr_src))
#endif

#define STMR_GET_CNTR_SRC_VALUE_FOR_RX_GEN(cntr_src)                 \
    (stmr_ref_sel_for_rx_gen[cntr_src])

#ifdef FEATURE_WCDMA_STMR_INT3_SUPPORT
#define STMR_GET_CNTR_SRC_VALUE_FOR_RX_EXT_GEN(cntr_src)             \
    (stmr_ref_sel_for_rx_ext_gen[cntr_src])
#endif

/* This macro commands a status dump of the System Time values to be
 * captured into their respective status registers. The following
 * action times are supported:
 * STMR_STATUS_DUMP_IMMEDIATE: immediate operation
 */
#define STMR_STATUS_DUMP_CMD_IMMEDIATE 0x1

#ifndef FEATURE_WCDMA_BOLT_RUMI_HWIO_DEMOD

#define STMR_STATUS_DUMP( cmd )                                      \
  STMR_STATUS_DUMP_LOCK();                                           \
do {                                                                 \
  int32 frc_diff, frc_dynamic, frc;                                  \
  WL1_HWIO_OUT( STMR_STATUS_DUMP_CMD_REG, cmd  );                        \
  frc_dynamic = STMR_FRAME_REF_COUNT_DYNAMIC();                      \
  frc = STMR_FRAME_REF_COUNT_STATUS();                               \
  frc = frc >> CHIPX8_TO_CHIP_RSHIFT;                                \
  frc_diff = frc_dynamic - frc;                                      \
  if (frc_diff < 0)                                                  \
  {                                                                  \
    frc_diff = -frc_diff;                                            \
  }                                                                  \
  if (frc_diff > CHIP_PER_256_CHIP_UNITS)                            \
  {                                                                  \
    MSG_ERROR("STMR_STATUS_DUMP didn't take effect",  0, 0, 0);      \
  }                                                                  \
  STMR_STATUS_DUMP_UNLOCK();                                         \
} while ( 0 )

/* This macro is similar to STMR_STATUS_DUMP except that it does not do
 * an automatic clock-busy wait. To use this, the user must make sure to
 * allow the necessary time between the instant this macro is called and
 * the instant the dumped data are read out.
 */
#define STMR_STATUS_DUMP_NO_WAIT( cmd )                              \
do {                                                                 \
  WL1_HWIO_OUT( STMR_STATUS_DUMP_CMD_REG, cmd  );                        \
} while ( 0 )

#else
#define STMR_STATUS_DUMP( cmd )                                      \
  MSG_ERROR("WCDMA_BOLT_DEMOD:Commented for Bringup ,cmd %d", cmd, 0, 0);
#define STMR_STATUS_DUMP_NO_WAIT( cmd )                              \
  MSG_ERROR("WCDMA_BOLT_DEMOD:Commented for Bringup ,cmd %d", cmd, 0, 0);
#endif /*FEATURE_WCDMA_BOLT_RUMI_HWIO_DEMOD*/

/*============================================================================
    TX System Time Related Macros
============================================================================*/
/* This macro sets the TX 10ms Interrupt Offset to the specified value */
#ifndef FEATURE_WCDMA_BOLT_RUMI_HWIO_DEMOD
#define STMR_SET_TX_10MS_INT_OFFSET( offset )                        \
  WL1_HWIO_OUT                                                           \
  (                                                                  \
    STMR_TX_10MS_INT_OFFSET,                                         \
    offset                                                           \
  )

/* This macro returns the TX Slot Count that was captured by a status dump
 * The Frame Count returned by the STMR_TX_TIME_STATUS_MACRO is in units
 * of chipx8. Convert the value to STMR ticks and calculate the slot count.
 * Divide by 8 to get chipx1 and by 256 to get STMR ticks (right shift by 11).
 */
#define STMR_GET_TX_SLOT_COUNT_STATUS()                              \
  (                                                                  \
    (                                                                \
      ( WL1_HWIO_INM( STMR_TX_TIME_STATUS_REG,                           \
           WL1_HWIO_FMSK(STMR_TX_TIME_STATUS_REG, TX_FRAME_COUNT) )     \
          >> WL1_HWIO_SHFT(STMR_TX_TIME_STATUS_REG, TX_FRAME_COUNT ) )   \
        >> 11                                                        \
    ) / STMR_TICKS_PER_SLOT                                          \
  )

/* This macro returns the TX TTI Count that was captured by a status dump */
#define STMR_GET_TX_TTI_COUNT_STATUS()                               \
  (                                                                  \
    WL1_HWIO_INM                                                         \
    (                                                                \
      STMR_TX_TIME_STATUS_REG,                                       \
      WL1_HWIO_FMSK(STMR_TX_TIME_STATUS_REG,TX_TTI_COUNT)               \
    )                                                                \
    >> WL1_HWIO_SHFT(STMR_TX_TIME_STATUS_REG, TX_TTI_COUNT)              \
  )

/* This macro returns the TX Frame Count in units of Chipx8 that was captured
 * by a status dump
 */
#define STMR_GET_TX_FRAME_COUNT_STATUS()                             \
  (                                                                  \
    WL1_HWIO_INM                                                         \
    (                                                                \
      STMR_TX_TIME_STATUS_REG,                                       \
      WL1_HWIO_FMSK(STMR_TX_TIME_STATUS_REG, TX_FRAME_COUNT)             \
    )                                                                \
    >> WL1_HWIO_SHFT(STMR_TX_TIME_STATUS_REG, TX_FRAME_COUNT)            \
  )

/* This macro performs a status dump and captures the FRC status
 * in chipx8 units. The FRC status carries the frame_num, slot_num and 
 * frame reference counter. The caller has to mask and extract the
 * individual values
 */
#define STMR_GET_FRC_STATUS(frc_status)                              \
  do {                                                               \
  STMR_STATUS_DUMP( STMR_STATUS_DUMP_CMD_IMMEDIATE );                \
  frc_status = STMR_FRAME_REF_STATUS();                              \
  } while(0)

/* This macro performs a status dump and captures the combiner counter
 * in chipx8 units.
 */
#define STMR_GET_COMBINER_COUNT_STATUS(comb, comb_status)            \
  do {                                                               \
    STMR_STATUS_DUMP( STMR_STATUS_DUMP_CMD_IMMEDIATE );              \
    comb_status = STMR_COMBINER_COUNT_STATUS((stmr_counter_source_enum_type)comb);         \
  } while(0)

#else
#define STMR_SET_TX_10MS_INT_OFFSET( offset )                        \
  MSG_ERROR("WCDMA_BOLT_DEMOD:Commented for Bringup ,offset %d", offset, 0, 0);
/* This macro returns the TX Slot Count that was captured by a status dump
 * The Frame Count returned by the STMR_TX_TIME_STATUS_MACRO is in units
 * of chipx8. Convert the value to STMR ticks and calculate the slot count.
 * Divide by 8 to get chipx1 and by 256 to get STMR ticks (right shift by 11).
 */
#define STMR_GET_TX_SLOT_COUNT_STATUS()                              \
  MSG_ERROR("WCDMA_BOLT_DEMOD:Commented for Bringup", 0, 0, 0);

/* This macro returns the TX TTI Count that was captured by a status dump */
#define STMR_GET_TX_TTI_COUNT_STATUS()                               \
( \
  0 \
)

/* This macro returns the TX Frame Count in units of Chipx8 that was captured
 * by a status dump
 */
#define STMR_GET_TX_FRAME_COUNT_STATUS()                             \
( \
  0 \
)
/* This macro performs a status dump and captures the FRC status
 * in chipx8 units. The FRC status carries the frame_num, slot_num and 
 * frame reference counter. The caller has to mask and extract the
 * individual values
 */
#define STMR_GET_FRC_STATUS(frc_status)                              \
  MSG_ERROR("WCDMA_BOLT_DEMOD:Commented for Bringup ,frc_status %d", frc_status, 0, 0);

/* This macro performs a status dump and captures the combiner counter
 * in chipx8 units.
 */
#define STMR_GET_COMBINER_COUNT_STATUS(comb, comb_status)            \
  MSG_ERROR("WCDMA_BOLT_DEMOD:Commented for Bringup ,comb %d comb_status %d",comb, comb_status, 0);

#endif /*#ifndef FEATURE_WCDMA_BOLT_RUMI_HWIO_DEMOD*/

/* Specifies the event mask on the interrupt source. The Event
 * Number is specified as a bit mask. Event N has bit N set.
 */
typedef uint16 stmr_event_mask_type;

/*
 * This enum defines the grouping for combiners as defined for the cfn count registers
 * in the STMR Software Interface Document. 
 */
typedef enum {
  STMR_COMBINER_GROUP_COMBINERS_0_TO_3
} stmr_hw_combiner_grouping_enum_type;

/*
 * The following data structure carries the data for writing CFN to STMR
 * registers
 */
typedef struct {
  uint8 hwch;

  uint8 cfn;
}stmr_comb_cfn_info_struct_type;

/*
 * Type for handler registered by the units in L1 for the STMR
 * interrupts. This handler will be called by the STMR whenever an
 * Event occurs on the interrupt.
 */
typedef void stmr_int_isr_type
(
  stmr_event_mask_type event_mask
);

#ifdef FEATURE_WCDMA_DAL_INT_CONTROLLER
typedef struct {
  uint32 dal_intr_id;
  DalDeviceHandle* dal_device_handle;
}stmr_intr_mapping_tbl_struct_type;
#endif /* FEATURE_WCDMA_DAL_INT_CONTROLLER */


/*===========================================================================

                      FUNCTION DECLARATIONS

===========================================================================*/

void stmr_default_isr( stmr_event_mask_type event_mask );

/*===========================================================================

FUNCTION STMR_INIT

DESCRIPTION
  This function sets up the interrupt handlers for the three Programmable
  Interrupts.

DEPENDENCIES
  None

RETURN VALUE
  None
  
SIDE EFFECTS
  None
===========================================================================*/
extern void stmr_init( void );

/*===========================================================================

FUNCTION STMR_INT0_ISR

DESCRIPTION
  This function is called whenever a Event occurs on the Programmable
  Interrupt 0 (STMR0).
  This function
  - reads the Event Masks that caused the interrupt from the STMR0
    status register.
  - clears the corresponding mask by writing to the STMR0 command register.
  - calls the handler for this interrupt that is registered with the
    System Timer Driver and pass the Event Masks to the handler.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
extern void stmr_int0_isr( void );

/*===========================================================================

FUNCTION STMR_INT1_ISR

DESCRIPTION
  This function is called whenever a Event occurs on the Programmable
  Interrupt 1 (STMR1).
  This function
  - reads the Event Masks that caused the interrupt from the STMR1
    status register.
  - clears the corresponding mask by writing to the STMR1 command register.
  - calls the handler for this interrupt that is registered with the
    System Timer Driver and pass the Event Masks to the handler.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
extern void stmr_int1_isr( void );

/*===========================================================================

FUNCTION STMR_INT2_ISR

DESCRIPTION
  This function is called whenever a Event occurs on the Programmable
  Interrupt 2 (STMR2).
  This function
  - reads the Event Masks that caused the interrupt from the STMR2
    status register.
  - clears the corresponding mask by writing to the STMR2 command register.
  - calls the handler for this interrupt that is registered with the
    System Timer Driver and pass the Event Masks to the handler.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
extern void stmr_int2_isr( void );

#ifdef FEATURE_WCDMA_STMR_INT3_SUPPORT
/*===========================================================================

FUNCTION STMR_INT3_ISR

DESCRIPTION
  This function is called whenever a Event occurs on the Programmable
  Interrupt 3 (STMR3).
  This function
  - reads the Event Masks that caused the interrupt from the STMR3
    status register.
  - clears the corresponding mask by writing to the STMR3 command register.
  - calls the handler for this interrupt that is registered with the
    System Timer Driver and pass the Event Masks to the handler.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
extern void stmr_int3_isr( void );
#endif

/*===========================================================================

FUNCTION STMR_SET_ISR

DESCRIPTION
  This function allows the clients of L1 to set a handler to each
  of the Programmable Interrupts. The registered handler will be called
  whenever there is an Event(s) on the particular interrupt. The Event Mask
  of the occurring events will be passed to the handler.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
extern void stmr_set_isr
(
 stmr_int_type      intr,
 stmr_int_isr_type* isr_ptr
);

/*===========================================================================

FUNCTION STMR_INSTALL_ISRS

DESCRIPTION
  This function installs/registers STMR interrupt handlers with the tramp services.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
extern void stmr_install_isrs( void );

/*===========================================================================

FUNCTION STMR_UNINSTALL_ISRS

DESCRIPTION
  This function uninstalls/deregisters STMR interrupt handlers with the tramp services.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
extern void stmr_uninstall_isrs( void );

/*===========================================================================

FUNCTION STMR_DEACT_INIT

DESCRIPTION
  This function is called when WCDMA mode is deactivated. This function 
  disables all the events for the interrupt sources uninstalls/deregisters 
  STMR interrupt handlers with the tramp services.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
extern void stmr_deact_init( void );

/*===========================================================================

FUNCTION STMR_CLEAR_INTERRUPT

DESCRIPTION
  This function clears any pending STMR interrupts.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
extern void stmr_clear_interrupts( void );

/*===========================================================================

FUNCTION STMR_SET_EVENT_FRAME

DESCRIPTION
  This function allows the user to set an Event Frame on a specific
  programmable interrupt. The Event Frames are periodic and has the
  following parameters:
  Intr                   - specifies the programmable interrupt
  Frame Ref Sel          - for STMR_PROG_INT0 and STMR_PROG_INT1, specifies
                           whether to use the Frame Reference Counter or
                           to use one of 4 Combiner Counters.
                           For STMR_PROG_INT2, specifies whether to use the
                           Frame Reference Counter or the TX System Time.
  Offset                 - specifies the Frame Offset relative to the
                           current reference counter in units of 256 chips.
                           The offset must be less than or equal to the Frame
                           Period. The time of all Events is advanced by the
                           amount of the offset.
  Event Mask             - specifies which Event is enabled. Bit N is set
                           true to enable event N on the specified STMR
                           interrupt.                      

  This function requires that the interrupt source is driven by one of 4
  combiner counters or the frame reference counter and further that the
  combiner counter is slaved to a physical channel. This should be done
  by a call to the function stmr_align_counter.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
extern void stmr_set_event_frame
(
 stmr_int_type intr,
 uint8         offset,
 stmr_event_mask_type event_mask
);

/*===========================================================================

FUNCTION STMR_SET_EVENT_FIRING_TIMES

DESCRIPTION
  This function sets the Firing Times of Events on the specified interrupt
  source. The Firing Times of the Events are specified in an array. Element
  N of the array corresponds to Event N. The number of Events for which the
  Firing Times should be programmed is passed to this function.

  The Event Firing Times are specified relative to the start of the offset
  Event Frame in units of 256 chips. The Firing Time must be less than the
  interrupt frame length i.e. should take the value between 0 and 149.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
extern void stmr_set_event_firing_times
(
 stmr_int_type intr,
 const uint8*  event_firing_time_array_ptr,
 uint8         num_events
);

#ifdef FEATURE_WCDMA_SYSTEM_TIMER_REV2
/*===========================================================================

FUNCTION STMR_SET_EVENT_FIRING_TIMES_IN_CHIPS

DESCRIPTION
  This function sets the Firing Times of Events on the specified interrupt
  source. The Firing Times of the Events are specified in an array. Element
  N of the array corresponds to Event N. The number of Events for which the
  Firing Times should be programmed is passed to this function.

  The Event Firing Times are specified relative to the start of the offset
  Event Frame in units of chips. The Firing Time must be less than the
  interrupt frame length i.e. should take the value between 0 and 38400 - 1.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
void stmr_set_event_firing_times_in_chips
(
 stmr_int_type intr,
 const uint16* event_firing_time_array_ptr,
 uint8         num_events
);
#endif  /* FEATURE_WCDMA_SYSTEM_TIMER_REV2 */
/*===========================================================================

FUNCTION STMR_ENABLE_EVENTS

DESCRIPTION
  This function enables the Events specified in the Event Mask on the
  specified interrupt source. This function assumes that the Firing Times
  of these Events have already been programmed.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
extern void stmr_enable_events
(
 stmr_int_type intr,
 stmr_event_mask_type event_mask
);

/*===========================================================================

FUNCTION STMR_DISABLE_EVENTS

DESCRIPTION
  This function disables the specified Events from the Programmable
  Interrupt specified. It reads the current Event Mask from the Frame
  Register and then unsets the Mask of the Events to be disabled and programs
  the new Event Mask to the Frame Register.

  There is no need to reset the Firing Time in the Event Register, since the
  Event will not fire if it is not programmed in the Frame Register.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
extern void stmr_disable_events
(
 stmr_int_type intr,
 stmr_event_mask_type event_mask
);


#ifdef FEATURE_WCDMA_SYSTEM_TIMER_REV2
/*===========================================================================

FUNCTION STMR_MODIFY_EVENT_FIRING_TIME_IN_CHIPS

DESCRIPTION
  This function adds/modifies the firing time of the specified Event
  on the specified Programmable Interrupt Source. The Event mask
  for this Event should already have been programmed in the
  Event Frame Register for this Programmable Interrupt for the Event
  to occur.

  The Event Mask specifies which Event should be modified. Bit N of the mask
  corresponds to Event N. The Event Mask should specify only one Event.
  If more than one Event is specified in the Event Mask, only the Firing Time
  Event that corresponds to the first bit set from the LSB is modified.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
extern void stmr_modify_event_firing_time_in_chips
(
 stmr_int_type intr,
 uint16        event_mask,
 uint16        firing_time
);
#endif  /* FEATURE_WCDMA_SYSTEM_TIMER_REV2 */

/*===========================================================================

FUNCTION STMR_MODIFY_EVENT_FIRING_TIME

DESCRIPTION
  This function modifies the firing time of the specified Event
  on the specified Programmable Interrupt Source. The Event mask
  for this Event should already have been programmed in the
  Event Frame Register for this Programmable Interrupt.

  The Event Mask specifies which Event should be modified. Bit N of the mask
  corresponds to Event N. The Event Mask should specify only one Event.
  If more than one Event is specified in the Event Mask, only the Firing Time
  Event that corresponds to the first bit set from the LSB is modified.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
extern void stmr_modify_event_firing_time
(
 stmr_int_type intr,
 stmr_event_mask_type event_mask,
 uint16        firing_time
);

/*===========================================================================

FUNCTION STMR_GET_EVENT_NUM

DESCRIPTION
  Return the Event Number of an Event given a Event Mask.
  Bit N of the Event Mask corresponds to Event Number N (Event N).
  The parameter Event Mask is assumed to have only one bit set.
  
  If more than one bit in the Event Mask is set, only the
  Event Number corresponding to the first (least significant bit) bit
  set is returned.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
extern uint8 stmr_get_event_num
(
  stmr_event_mask_type event_mask
);

/*===========================================================================

FUNCTION STMR_ALIGN_COUNTER

DESCRIPTION
  This function aligns the specified interrupt source to the specified
  counter source.

  counter sources for int0 and int1
  STMR_COMBINER_COUNTER0
  STMR_COMBINER_COUNTER1
  STMR_COMBINER_COUNTER2
  STMR_COMBINER_COUNTER3
  
  counter source for int2 only
  STMR_TX_SYSTEM_TIME_COUNTER

  counter source for int0, int1, and int2
  STMR_FRAME_REF_COUNTER

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
extern void stmr_align_counter
(
  stmr_int_type intr,
  stmr_counter_source_enum_type cntr_src
);

/*===========================================================================

FUNCTION STMR_GET_CNTR_SRC

DESCRIPTION
  This function returns the enumerated value of the counter source that
  is currently driving the specified interrupt source.

DEPENDENCIES
  None

RETURN VALUE
  The enum value of the counter source that is currently driving the
  interrupt source.

SIDE EFFECTS
  None

===========================================================================*/
stmr_counter_source_enum_type stmr_get_cntr_src
(
  stmr_int_type intr
);


#ifdef FEATURE_WCDMA_SYSTEM_TIMER_REV2
/*===========================================================================

FUNCTION STMR_GET_FIRING_TIME_IN_CHIPS

DESCRIPTION
  This function gets the firing time of the specified event on the
  specified interrupt.

DEPENDENCIES
  None

RETURN VALUE
  Firing Time of Event in units of chips. (0 -38399)

SIDE EFFECTS
  None

===========================================================================*/
uint32 stmr_get_firing_time_in_chips
(
  stmr_int_type intr,
  uint8         event_num
);
#endif  /* FEATURE_WCDMA_SYSTEM_TIMER_REV2 */

/*===========================================================================

FUNCTION STMR_GET_FIRING_TIME

DESCRIPTION
  This function gets the firing time of the specified event on the
  specified interrupt.

DEPENDENCIES
  None

RETURN VALUE
  Firing Time of Event in units of 256 chips.

SIDE EFFECTS
  None

===========================================================================*/
uint32 stmr_get_firing_time
(
  stmr_int_type intr,
  uint8         event_num
);

/*===========================================================================

FUNCTION STMR_GET_CURRENT_TIME

DESCRIPTION
  This function gets the current time in units of 256 chips of the
  counter source that is currently driving the specified interrupt
  source.

DEPENDENCIES
  None

RETURN VALUE
  Firing Time of Event in units of 256 chips.

SIDE EFFECTS
  None

===========================================================================*/
uint32 stmr_get_current_time
(
  stmr_int_type intr
);

/*===========================================================================

FUNCTION STMR_GET_CURRENT_TIME

DESCRIPTION
   Gets the value in chipx8 of the counter driving the interrupt specified by
   the argument intr. It comands a status dump as part of the operation.

DEPENDENCIES
  None

RETURN VALUE
  Counter value in chipx8 for the specified counter.

SIDE EFFECTS
  Does a status dump as part of its operation.
===========================================================================*/
uint32 stmr_get_current_time_chipx8
(
  stmr_int_type intr
);

/*===========================================================================

FUNCTION STMR_GET_EVENTS_AND_ENABLE_ONLY

DESCRIPTION
  This function enables only the events specified
  by the enable_only_event_mask, for the specified
  interrupt, other events are effectively disabled. 
  Additionally, it returns the mask of
  events that were enabled on the interrupt 
  just prior to calling 
  this function.

DEPENDENCIES
  None

RETURN VALUE
  mask of events that were enabled on the interrupt 
  just prior to calling this function

SIDE EFFECTS

===========================================================================*/
extern uint16 stmr_get_events_and_enable_only
(
  stmr_int_type intr,
  uint16        enable_only_event_mask
);

extern uint16 stmr_get_event_mask
(
  stmr_int_type intr
);

/*===========================================================================

FUNCTION STMR_GET_COMBINER_CFN

DESCRIPTION
  This function reads the cfn of a combiner from the corresponding
  STMR register and returns it.

DEPENDENCIES
  None

RETURN VALUE
  CFN of the combiner

SIDE EFFECTS
  None

===========================================================================*/
extern uint8 stmr_get_combiner_cfn(stmr_counter_source_enum_type combiner);

/*===========================================================================

FUNCTION STMR_GET_COMBINER_CFN

DESCRIPTION
  This function reads the cfn of a combiner from the corresponding
  STMR register and returns it.

DEPENDENCIES
  None

RETURN VALUE
  CFN of the combiner

SIDE EFFECTS
  None

===========================================================================*/
extern int16 hw_sw_cfn_diff;

/*===========================================================================
FUNCTION STMR_GET_CNTR_TIME_CHIPX8

DESCRIPTION
  This function reads the chipx8 quantity of any STMR counter and returns it.

DEPENDENCIES
  None

RETURN VALUE
  counter value in cx8

SIDE EFFECTS
  None

===========================================================================*/
extern uint32 stmr_get_cntr_time_chipx8 (stmr_counter_source_enum_type cntr);

/*===========================================================================

FUNCTION STMR_IS_EVENT_PENDING_PROCESS

DESCRIPTION
  This function checks if the given evt has fired and is being processed

DEPENDENCIES
  None

RETURN VALUE
  Boolean

SIDE EFFECTS
  None

===========================================================================*/
boolean stmr_is_event_pending_process(stmr_int_type intr, uint8 evt);

/*===========================================================================

FUNCTION STMR_UPDATE_EVENT_PROCESSING_STATUS

DESCRIPTION
  This function updates the processed event status

DEPENDENCIES
  None

RETURN VALUE
  Boolean

SIDE EFFECTS
  None

===========================================================================*/
void stmr_update_event_processing_status(stmr_int_type intr, uint8 evt);


#ifdef FEATURE_WCDMA_SYSTEM_TIMER_REV2

#ifndef FEATURE_WCDMA_BOLT_RUMI_HWIO_DEMOD
/*===========================================================================

FUNCTION STMR_GET_COMBINER_SUB_FRAME_NUMBER

DESCRIPTION
  This function reads the cfn sub frame of a combiner from the corresponding
  STMR register and returns it.

DEPENDENCIES
  None

RETURN VALUE
  returns 2ms sub frame. Range 0-4

SIDE EFFECTS
  None

===========================================================================*/

extern uint8 stmr_get_combiner_sub_frame_number (stmr_counter_source_enum_type combiner);

/*===========================================================================

FUNCTION STMR_GET_COMBINER_CFN_AND_SUBFRAME

DESCRIPTION
  This function reads the cfn and sub frame of a combiner from the corresponding
  STMR register and returns the two values.

DEPENDENCIES
  None

RETURN VALUE
  cfn range (0-255) and subframe range (0-4)

SIDE EFFECTS
  None

===========================================================================*/

extern void stmr_get_combiner_cfn_and_subframe(stmr_counter_source_enum_type combiner, 
                                               uint8 *cfn, 
                                               uint8 *subframe );

#endif /*FEATURE_WCDMA_BOLT_RUMI_HWIO_DEMOD*/

/*===========================================================================

FUNCTION STMR_ADVANCE_COMBINER_COUNT

DESCRIPTION
  This function advances the combiner counter by one chipx8 count in the next 
  256 chip boundary.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/

extern void stmr_advance_combiner_count(stmr_counter_source_enum_type combiner);

/*===========================================================================

FUNCTION STMR_RETARD_COMBINER_COUNT

DESCRIPTION
  This function retards the combiner counter by one chipx8 count in the next 
  256 chip boundary.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
extern void stmr_retard_combiner_count(stmr_counter_source_enum_type combiner);

/*===========================================================================

FUNCTION STMR_SET_STMR_COUNTER

DESCRIPTION
  This function sets the combiner with the passed value
  - coutner   : The counter to set
  - slam_value: The value to programmed to the counter
  - frc_target: The value of frc when the counter value will be slammed

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
void stmr_set_stmr_counter(stmr_counter_source_enum_type coutner, 
                           uint32 slam_value, 
                           uint32 frc_target);


#endif

/*===========================================================================

FUNCTION STMR_UPD_COMBINER_CFN

DESCRIPTION
  This function writes the CFN of a combiner as provided in the input
  paramter in to the corresponding STMR register.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
extern void stmr_upd_combiner_cfn(const stmr_comb_cfn_info_struct_type *phy_data);

#ifndef FEATURE_WCDMA_BOLT_RUMI_HWIO_DEMOD
#ifdef FEATURE_DEBUG_STMR_TEST
/*===========================================================================

FUNCTION STMR_TEST_1

DESCRIPTION
  Performs STMR test 1.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
extern void stmr_test_1(void);

/*===========================================================================

FUNCTION STMR_TEST_2

DESCRIPTION
  Performs STMR test 2.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
extern void stmr_test_2(void);

/*===========================================================================

FUNCTION STMR_TEST_3

DESCRIPTION
  Performs STMR test 3.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
extern void stmr_test_3(void);

/*===========================================================================

FUNCTION STMR_TEST_4

DESCRIPTION
  Performs STMR test 4.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
extern void stmr_test_4(void);

/*===========================================================================

FUNCTION STMR_TEST_5

DESCRIPTION
  Performs STMR test 5.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
extern void stmr_test_5(void);


/*===========================================================================

FUNCTION STMR_TEST_6

DESCRIPTION
  Performs STMR test 6.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
extern void stmr_test_6(void);

#endif /* FEATURE_DEBUG_STMR_TEST */

/*===========================================================================

FUNCTION STMR_DEBUG_PRINT_SFN_CFN_UPDATE_INFO

DESCRIPTION
  Prints debug info pertaining to the SFN-CFN Update event.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
extern void stmr_debug_print_sfn_cfn_update_info(void);

extern void wcdma_stmr_test(uint32 stmr_test_type);
#endif /*FEATURE_WCDMA_BOLT_RUMI_HWIO_DEMOD*/

#endif /* STMR_H */

