#ifndef WL1TRM_H
#define WL1TRM_H
/*===========================================================================

                    L 1   T R M   A N D   T C X O   M A N A G E R

DESCRIPTION
  This file contains global declarations and external references
  for the L1 TRM and TCXO Manager.

EXTERNALIZED FUNCTIONS

  wl1_trm_request_primary_antenna
    This function requests TRM for the primary antenna before WCDMA can 
    attempt to acquire.

  wl1_trm_stop_cmd
    This function is called to send WL1 into a L1M_WAIT_FOR_TRM_STOP state.

  wl1_trm_debug_delay
    This function generates a delay in msec for debugging purposes

  wl1_tcxo_set_restriction
    Invoked due to either a restriction change from TCXO Mgr, or when error becomes
    large and UMTS is breaking the TCXO Mgr restriction or we are freezing the trk_lo
    for debugging purposes. This simply posts a local command which does all the work.
    Necessary because we cant call tcxomgr_report() from interrupt context.

  wl1_tcxo_set_restriction_cmd
    Generally invoked by the local command TCXO_SET_RESTRICTION_CMD to actually 
    perform the freezing or unfreezing of the TCXO. This function needs send 
    acks (when TCXO Mgr gives us a new restriction) and reports (when breaking 
    a restriction) to TCXO Mgr when necessary. Can also be invoked directly,
    if we cannot post a local command and wait for it at the moment.

  wl1_tcxo_resume_restriction
    Resumes current restriction after WL1 diverges from the restriction due to error
    in the VCO.

  wl1_tcxo_release
    Releases control of the TCXO.

  wl1_tcxo_get_vco
    This function returns the vco value from the mdsp.

  wl1_tcxo_frozen
    Returns if the tcxo is currently frozen or not.

  wl1_tcxo_rpush
    Performs the actual rotator push when using TCXO Mgr 3.0

  wl1_tcxo_request
    This function requests TCXO Mgr for control of the VCTCXO.

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None

Copyright (c) 2000-2014 by QUALCOMM Technologies, Incorporated. All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.
===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/wcdma/l1/offline/inc/wl1trm.h#1 $
$DateTime: 2015/01/27 06:42:19 $
$Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
01/23/15   ar      In CELL_FACH state, send resume failure immediately if TA duration
                   is larger than OOS duration.
12/4/14    pg      Added a new API to check whether QTA is on going.
10/20/14   pg      Fix for handling Empty QTA gaps by delaying qta stop to allow
                   preloading.
10/07/14   kr      KW errors
09/15/14   gm      mods to remove global variables which are never used.
09/10/14   gm      Mods to request for first QTA with force_reserve after moving to FACH state. 
08/11/14   skk     Request for lock for CELL_SELECTION_REQ in G2W TA mode as lock can be released as a part of SIB sleep.
07/28/14   skk     Handle pended ext cmd by setting ext_cmd_sig again.
07/17/14   pg      Handling the trm_lock_sig_pending_on_sleep variable if We are not 
                   sleeping to unblock the handling of ext. commands.
07/17/14   gm      added a function wl1_query_qta_start to be used by RxD module.
07/10/14   pg      RoTA (Receive Only Tune Away) Changes for WL1.
06/24/14   pg      FR 21174 : QTA allowed during re-config in DCH changes
06/06/14   bs      Added support to print multisim config fields in hs log packets
04/21/14   skk     Check proc mask rather than client status when L1 gets a grant callback in its PAGING_WAKEUP. 
04/10/14   bj      WCDMA Narrow Band Rejection for System Search.
04/09/14   sm      Internal Unlock API for Secondary
03/18/14   skk     DSDS: Added function headers for some G2W TA functions.
04/08/13   jd      Fix to return floored rxagc when trm is not reserved.
04/02/14   dm      DSDS: New API to make tuneaway duration read from RRC available to 
                   all L1 modules.
03/20/14   bj      Split ACQ code cleanup
02/28/14   pg      To avoid back to back reservation with FRM in case tuneaway timer 
                   expires in parallel in case FACH QTA
02/27/14   as      Changing wrm_unlock_trm_cb api to get additional data from trm.
02/26/14   rs      Externed a function to request lock for secondary chain
02/14/14   as      Delaying call to dl_cell_check_tddet_status to after closing the QTA gap.
02/14/14   qj      Changes for W diversity antenna sharing during PCH_SLEEP
02/12/14   sad     DB-3C API check in
02/06/14   jkb     Use WRM state instead of procedure mask to check lock status
02/07/14   sm      OMRD changes to make RxD trm complian
02/07/14   jkb     Add check for Feature dual sim with feature qta
01/27/14   as      Implemented Version 2 of the W2G tuneaway log packet WL1TRM_TUNEAWAY_LOG_PKT.
01/22/14   pkg     EUL optimization to avoid HICH result falling into QTA gap and improved RSN management for QTA.
01/22/14   gm      post a local command to handle unlock cancel event. 
01/21/14   abs     Cleaning up DSDA~CXM(FR:2334) changes
01/20/14   ar      Add TRM request support for W->L searches
01/17/14   jkb     Adding lock check to WL1_IN_DS_WITH_ANY_LOCK and 
                   WL1_IN_DS_WITH_NO_LOCK 
01/17/14   raj     Added changes for raising W UL Priority to CXM for SRB Data.
01/13/14   jkb     Changing WL1_IN_DS_WITH_ANY_LOCK and WL1_IN_DS_WITH_NO_LOCK
01/07/14   as      Added data structure and external funtions to support
                   WL1TRM_TUNEAWAY_LOG_PKT log packet.
12/16/13   as      Porting G2W TA from Triton to Dime.
12/06/13   ar      Modified definition wl1_ds_pre_meas_start_ind
11/15/13   mk      DSDS: Handling CPHY_DRX_REQ during sleep.
11/14/13   as      W2G TA: Adding code for RRC dependency.
10/29/13   dm      DSDS: Use WRM state to check if L1 has lock.
09/29/13   as      Continuing port of Dual Sim from Triton to Dime.
09/23/13   as      Porting Dual Sim from Triton to Dime.
09/12/13   jd      Searcher redesign
08/28/13   bs      Resolved KW issues
08/12/13   cc      DBDC CM W2W feature check in
07/24/13   jd      Changes for Dual band Dual carrier
01/11/13   scm     More partial FEATURE_DUAL_SIM support for DSDA.
12/10/12   scm     Partial FEATURE_DUAL_SIM support for DSDA.
12/07/12   scm     Pass trm_get_rf_device() return value into RFM APIs.
08/13/12   jd      Trm changes on dime.
01/30/12   vs      Feature cleanup.
06/30/10   scm     Reserve TCXO control after WFW download.
05/13/09   scm     Implement wl1_trm_set_starting_trk_lo().
10/03/08   hk      Bringing in 7k Mailine fixes
04/17/07   kps     Add wl1_tcxo_resume_restriction()
04/10/07   kps     Fully featurize file under the TRM/TCXOMGR30 features
03/09/07   kps     change the return type for wl1_tcxo_rpush()
01/19/06   kps     Added support for Rx Diversity
12/15/06   kps     Delete wl1_tcxo_freeze_request() and wl1_controls_tcxo().
11/21/06   kps     Add wl1_tcxo_freeze_request() and wl1_controls_tcxo()
11/13/06   kps     TCXO Rotator push support
                   Add wl1_tcxo_rpush()
11/06/06   kps     Add wl1_tcxo_release().
10/27/06   kps     Added support for setting restrictions in TCXOMGR 3.0
10/13/06   kps     Added basic support for TCXOMGR3.0, under FEATURE_WCDMA_TCXOMGR30
10/02/06   kps     Added declaration for wl1_trm_stop_cmd.
09/11/06   kps     Created file. Declaration of wl1_trm_request_primary_antenna.
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

#include "wcdma_variation.h"
#include "comdef.h"

#ifdef FEATURE_DUAL_SIM
#include "task.h"
#include "msg.h"
#include "trm.h"
#include "l1task_v.h"
#include "rrccmd_v.h"
#endif

#ifdef FEATURE_WCDMA_TCXOMGR30
#include "l1def.h"
#include "l1utils.h"
#include "rex.h"
#include "rfm.h"
#include "tcxomgr.h"
#include "trm.h"

#ifdef FEATURE_DUAL_SIM
#include "l1task.h"
#include "rrccmd.h"
#endif


typedef enum
{
  WL1_TCXOMGR_NONE,       /* Dont ack or report restriction change to TCXO Mgr */
  WL1_TCXOMGR_ACK,        /* Ack restriction change to TCXO Mgr */
  WL1_TCXOMGR_REPORT      /* Report restriction change to TCXO Mgr */
} wl1_tcxomgr_ack_type;

typedef struct
{
  l1_local_cmd_hdr_type        hdr;         /* generic command header */

  tcxomgr_restriction_type     restriction;       /* Indicates the restriction type */
  
  /* Indicates whether the restriction needs to be acknowledged or 
  ** reported to tcxomgr*/
  wl1_tcxomgr_ack_type             ack_tcxomgr; 

} tcxo_restriction_local_cmd_type;

#endif /* FEATURE_WCDMA_TCXOMGR30 */

#ifdef FEATURE_DUAL_SIM
/* WL1_WRM_GRANT_CALLBACK_CMD structure. */
typedef struct
{
  /* Need this to put command on l1_local_cmd_q. */
  l1_local_cmd_hdr_type   hdr;

  /* The client which needs the RF resource */
  trm_client_enum_t       client;

  /*The granted Chain*/
  trm_grant_event_enum_t  event;

  /*The tag that WRM passed to TRM in trm_request_and_notify(...)*/
  trm_request_tag_t       tag;

} wl1_wrm_grant_callback_cmd_type;
#endif

/*   Wl1 TRM Clients for DBDC */
typedef enum
{
  WCDMA_TRM,
  WCDMA_TRM_CA,
  WCDMA_TRM_NUM_CLIENTS
} wcdma_trm_client_type;

/* Multi-Sim Configuration of UE: SS/DSDA/DSDS/TSTS*/
typedef enum
{
  /* Single SIM */
  WL1_MULTI_SIM_SS,
    /* Dual Sim Dual active */
  WL1_MULTI_SIM_DSDA,
  /* Dual Sim Dual standby */
  WL1_MULTI_SIM_DSDS,
  /* Triple Sim Triple standby */
  WL1_MULTI_SIM_TSTS,
  /* Invalid Sim Config */
  INVALID_CONFIG
}wl1_trm_multi_sim_config_enum_type;


typedef struct
{
  l1_freq_scan_band_enum_type wl1trm_band_info;
  boolean trm_reserved;  
  /* Returned by trm_get_rf_device() after WL1 gets TRM lock.  Passed into all RFM
  ** functions, so RF knows device to use. */
  rfm_device_enum_type wl1_trm_primary_rf_device;
  rfm_device_enum_type wl1_trm_secondary_rf_device;
  uint16 channel;
} wl1_trm_db_type;


extern wl1_trm_db_type wl1_trm_db[WCDMA_TRM_NUM_CLIENTS];

/* -----------------------------------------------------------------------
** Global Data Declarations
** ----------------------------------------------------------------------- */

#ifdef FEATURE_DUAL_SIM
extern boolean wrm_determine_g2w_ta_mode_active(void);

extern boolean wl1idle_ta_mgr_lock_rel_offline_activity_pending(void);

#define WRM_G2W_TA_MODE_ACTIVE() wrm_determine_g2w_ta_mode_active()

#define WRM_WL1_RESERVATION_TIME_SCLK_INVALID 0xFFFFFFFF

extern boolean drx_bplmn_pch_sleep_transition;

extern uint32 wrm_g2w_ta_l1_trm_resrv_sclk;

/* Variable to keep track of consecutive reservation fails*/
extern uint8 wl1_fach_reservation_fail_count;

/*separate variables for the two g subs in TSTS mode for force reserve*/
#ifdef FEATURE_TRIPLE_SIM
extern uint8 wl1_fach_reservation_fail_count_gs2;
extern uint8 wl1_fach_reservation_fail_count_gs3;
#endif


#endif

#if defined(FEATURE_WCDMA_TCXOMGR30) || defined(FEATURE_WCDMA_TRM)
/* Used when waiting for confirmations from mDSP and TRM */
extern rex_timer_type  wl1_timeout_timer;
#endif /* FEATURE_WCDMA_TCXOMGR30 || FEATURE_WCDMA_TRM*/

#if defined(FEATURE_DUAL_SIM) && defined(FEATURE_QTA)

/* Different Quick Tuneaway states in WL1:
**   WL1_QTA_STATE_NOT_ACTIVE  - Default state, no QTA active.
**   WL1_QTA_STATE_CM_FROZEN   - QTA started by first freezing compressed mode
**                               state machine to block any CM activity.
**   WL1_QTA_STATE_WFW_START_QTA_SENT - Start QTA sent to WFW.  Waiting for response.
**   WL1_QTA_STATE_ACTIVE      - QTA in progress.  GSM is active.
**   WL1_QTA_STATE_WFW_STOP_QTA_SENT    -  QTA stopped, Informed to firmware and L1 will wait
                                           for the command Done Ind. from WFW 
**   WL1_QTA_STATE_STOP_DELAYED - Added to handle Empty QTA gap. This is to handle preload by GSM 
                                  in case of empty qta gaps. Consider QTA is not finish yet.
**   WL1_QTA_STATE_QTA_STOPPED  - Intermediate state for acting on Back to back Qta
                                  if present or restoring L1 in state (just before qta)
                                  like unfreezing CM etc. 
**  WL1_QTA_STATE_QTA_CLEANUP   - Intermediate State for handling back to back qta
                                  in case triggered or not (expiry of tuneaway timer) 
*/
typedef enum
{
  WL1_QTA_STATE_NOT_ACTIVE,
  WL1_QTA_STATE_CM_FROZEN,
  WL1_QTA_STATE_WFW_START_QTA_SENT,
  WL1_QTA_STATE_ACTIVE,
  WL1_QTA_STATE_WFW_STOP_QTA_SENT,
  WL1_QTA_STATE_STOP_DELAYED,
  WL1_QTA_STATE_QTA_STOPPED,
  WL1_QTA_STATE_QTA_CLEANUP,
  WL1_QTA_STATE_MAX
} wl1_qta_state_enum_type;


extern wl1_qta_state_enum_type  wl1_qta_state;

/*Variable to track ciphering key restoring status after QTA */
extern boolean restore_ul_ciph_keys_after_qta;

/* mutex for QTA module*/
extern rex_crit_sect_type wl1_qta_crit_sect;

#define WL1_QTA_MUTEXLOCK()   REX_ISR_LOCK(&wl1_qta_crit_sect)
#define WL1_QTA_MUTEXFREE()   REX_ISR_UNLOCK(&wl1_qta_crit_sect)

#define WL1_IS_QTA_CONFIGURED() (wl1_preparing_for_qta() || wl1_is_wfw_in_qta_gap())

#endif

#ifdef FEATURE_DUAL_SIM
typedef struct
{
 l1_local_cmd_hdr_type local_cmd_hdr;
}wrm_ext_cmd_proc_resume_type;

typedef enum
{
  WRM_G2W_TA_EXT_CMD_NOT_APPLICABLE,
  WRM_G2W_TA_EXT_CMD_LOCK_WITH_GPRS_LTA_G2W_TA_DRX_END, // DRX abort condition in PCH_SLEEP
  WRM_G2W_TA_EXT_CMD_LOCK_NOT_REQD_G2W_TA_CONT, // Do not abort in PCH_SLEEP condition
  WRM_G2W_TA_EXT_CMD_LOCK_WITH_GPRS_LTA,
  WRM_G2W_TA_EXT_CMD_LOCK,
  WRM_G2W_TA_EXT_CMD_LOCK_NOT_REQD,
  WRM_G2W_TA_EXT_CMD_LOCK_NOT_REQD_G2W_TA_DRX_END, // DRX abort condition in PCH_SLEEP
  WRM_G2W_TA_EXT_CMD_LOCK_NOT_REQD_G2W_TA_END,
  WRM_G2W_TA_EXT_CMD_PROC_DEFAULT,
  NUM_MAX_WRM_G2W_TA_EXT_CMD_CATEGORY
} wrm_g2w_ta_mode_ext_cmd_category_enum_type;

extern trm_time_t trm_reserve_time_sclk_val;

/* =========================================================== */
/* Log packet definitions that report the Tuneaway properties  */
/* =========================================================== */

/* Stores the types of Tune-Aways */
typedef enum
{
  /* Quick Tuneaway flag */
  WL1TRM_TUNEAWAY_LOG_QTA,

  /* Long Tuneaway flag */
  WL1TRM_TUNEAWAY_LOG_LTA
} wl1trm_tuneaway_log_type;

/* Flags to the wl1trm_tuneaway_log_set_time function to record
** specific timeticks for Tuneaways */
typedef enum
{
  /* Flag that records the timetick when CPHY_SUSPEND_WCDMA_MODE_REQ is received */
  SET_TUNEAWAY_SUSPEND_START_TIMETICK,

  /* Flag that clears the timetick of suspend, used in case the FACH QTA is cancelled. */
  CLEAR_TUNEAWAY_SUSPEND_START_TIMETICK,

  /* Flag that records the timetic when RRC_CPHY_SUSPEND_WCDMA_MODE_CNF is sent */
  SET_TUNEAWAY_SUSPEND_END_TIMETICK,

  /* Flag that records the timetick when RESUME_WCDMA_MODE_REQ is received */
  SET_TUNEAWAY_RESUME_START_TIMETICK,

  /* Flag that records the timetick when CPHY_SETUP_CNF is sent */
  SET_TUNEAWAY_SETUP_END_TIMETICK
}wl1trm_tuneaway_log_set_time_type;

/* Structure that contains the data for every timeslice type. Contains information regarding the starting
   time and the ending time of the timeslice. */
typedef PACKED struct PACKED_POST
{
  /* Timetick when the timeslice begins */
  timetick_type start_timetick;

  /* Timetick when the timeslice ends */
  timetick_type end_timetick;

  /* The duration of the timeslice in timeticks */
  timetick_type duration_timetick;

  /* The duration of the timeslice in milli-seconds */
  uint16 duration_msecs;

  /* The CFN when timeslice begins */
  uint16 start_cfn;

  /* The CFN when timeslice ends */
  uint16 end_cfn;

  /* The Slot when timeslice begins */
  uint16 start_slot;

  /* The Slot when timeslice ends */
  uint16 end_slot;
} wl1trm_tuneaway_log_timeslice_struct_type;

/* Structure that contains the data for the log packet WL1TRM_TUNEAWAY_LOG_PKT */
typedef PACKED struct PACKED_POST
{
  /* Contains the flag for the type of tuneaway (LTA or QTA) */
  uint8 tuneaway_type;

  /* State of L1 when the tuneaway begins */
  uint16 tuneaway_l1m_state;

  /* Is Harq flush required. Only valid for QTAs */
  boolean tuneaway_harq_flush;

  /* Data structure that contains data about the Tuneaway duration */
  wl1trm_tuneaway_log_timeslice_struct_type tuneaway_timeslice;

  /* Data structure that contains data about the Suspend duration */
  wl1trm_tuneaway_log_timeslice_struct_type suspend_timeslice;

  /* Data structure that contains data about the Resume and Setup duration */
  wl1trm_tuneaway_log_timeslice_struct_type resume_and_setup_timeslice;

  /* Flag that shows if FACH QTA is cancelled or not */
  boolean fach_qta_cancelled;

  /* Contains the flag is FACH QTA is force reserved */
  boolean force_reserve;

  /* This bitmask contains the type of search which was blocked to
     allow the QTA or due to which the QTA was cancelled */
  uint8 fach_qta_reason_bitmask;

} wl1trm_tuneaway_log_data_struct_type;

/* Define the WL1TRM_TUNEAWAY_LOG_PKT log packet */
LOG_RECORD_DEFINE(WL1TRM_TUNEAWAY_LOG_PKT)

  /* Version number of this log packet */
  uint8 version;

  /* The data of the log packet */
  wl1trm_tuneaway_log_data_struct_type log_data;
LOG_RECORD_END

#define WL1TRM_TUNEAWAY_LOG_PKT_SIZE() (sizeof(WL1TRM_TUNEAWAY_LOG_PKT_type))

/* WL1TRM_TUNEAWAY_LOG_SUBMIT command type that contains the WL1TRM_TUNEAWAY_LOG_PKT
** log packet data */
typedef struct
{
  /* Command header for l1m command queue */
  l1_local_cmd_hdr_type   hdr;

  /* Log packet data associated with the command */
  wl1trm_tuneaway_log_data_struct_type log_data;

} wl1trm_tuneaway_log_submit_cmd_type;

#endif /* FEATURE_DUAL_SIM */

#if defined(FEATURE_DUAL_SIM) && defined(FEATURE_QTA)
extern boolean wl1_back_to_back_qta;
#ifdef FEATURE_TRIPLE_SIM
extern trm_client_enum_t wl1_back_to_back_qta_client_id;
#endif
#endif

#ifdef FEATURE_DUAL_SIM
typedef void (* wrm_srch_lock_grant_cb_type) (boolean);
#endif

/* NV support for QTA ROTA */
#define WCDMA_QTA_ROTA_CONF_FILE "/nv/item_files/conf/wl1_qta_rota.conf"
/* QTA ROTA nv */
#define QTA_ROTA_CONF_FILE_PERMISSIONS 0777
/* Same NV is used for ROTA and QBTA. Bit 0 is for ROTA and Bit 2 is for
   QBTA. ROTA and QBTA are allowed if the respective bits are set. Default 
   value is 0x5 as both ROTA and QBTA are allowed by default */
#define QTA_ROTA_DEFAULT_VAL 0x5


/*===========================================================================

                     MACROS

===========================================================================*/
/*===========================================================================

                     FUNCTION PROTOTYPES

===========================================================================*/

#ifdef FEATURE_DEBUG_WCDMA_TRM
#error code not present
#endif /* FEATURE_DEBUG_WCDMA_TRM */

#ifdef FEATURE_WCDMA_TRM

#ifdef FEATURE_TRM_DIME

/*** maximum channels in a pirticular band ***/
#define MAX_CHANNELS_IN_BAND 352

/* Array to store the channels information  */
extern uint32 *channels_list;

/*structure for holding Band information ***/
typedef struct
{
  sys_band_class_e_type band;
  uint16 min_channel_number;
  uint16 max_channel_number;
} trm_band_map_struct_type;

/* Maps l1_freq_scan_band_enum_type to sys_band_class_e_type for TRM request*/
extern trm_band_map_struct_type trm_map_l1_band_type[L1_FREQ_WCDMA_BAND_NONE];

#ifdef FEATURE_WCDMA_DIME_DUAL_ACTIVE
/* Run-time debug flag.  TRUE means call trm_get_rf_device() for RF device.  FALSE
** means old way, hard-code to RFCOM_TRANSCEIVER_0. */
extern boolean wl1_trm_use_rf_device_from_trm;
#endif

#ifdef FEATURE_DUAL_SIM
extern rex_timer_type wrm_g2w_ta_priority_inversion_timer;
#endif

/*===========================================================================
FUNCTION     WL1_TRM_GET_PRIMARY_RF_DEVICE

DESCRIPTION
  This function is invoked everytime we make a call into RFM.  All RFM APIs
  take RF device as a parameter.

  When we got TRM lock for WCDMA primary operation earlier, we called
  trm_get_rf_device().  We stored that value away to be returned by this
  function now.

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  rfm_device_enum_type - RF device in use by primary WCDMA operation.

SIDE EFFECTS
  None.
===========================================================================*/
#ifdef __GNUC__
static inline 
#else 
INLINE 
#endif
rfm_device_enum_type wl1_trm_get_primary_rf_device(void)
{
#ifdef FEATURE_WCDMA_DIME_DUAL_ACTIVE
  rfm_device_enum_type  rfm_device = RFCOM_TRANSCEIVER_0;


  if (wl1_trm_use_rf_device_from_trm)
  {
    rfm_device = wl1_trm_db[WCDMA_TRM].wl1_trm_primary_rf_device;

    if (rfm_device >= RFM_WCDMA_MAX_DEVICE )
    {
      /* This should, in theory, never happen.  For some reason we are making
      ** a call to RFM, but we don't know the RF device in use for W primary
      ** operation.  This could happen if we end up calling RFM API before we
      ** have primary TRM lock.  Anyway, for now hard-code default. */
      WL1_MSG0(ERROR, "WL1_TRM: Primary RF device invalid! Use default");
    }
  }

  return rfm_device;
#else
  rfm_device_enum_type  rfm_device = wl1_trm_db[WCDMA_TRM].wl1_trm_primary_rf_device;
  if (rfm_device >= RFM_WCDMA_MAX_DEVICE )
  {
    /* This should, in theory, never happen.  For some reason we are making
     ** a call to RFM, but we don't know the RF device in use for W primary
     ** operation.  This could happen if we end up calling RFM API before we
     ** have primary TRM lock.  Anyway, for now hard-code default. */
     ERR_FATAL("WL1_TRM: Primary RF device invalid!", 0, 0, 0);
  }

  return rfm_device;


#endif  /* #ifdef FEATURE_WCDMA_DIME_DUAL_ACTIVE */
}


/*===========================================================================
FUNCTION     WL1_TRM_GET_PRIMARY_RF_DEVICE_FOR_CA

DESCRIPTION
  This function provides primary RF device ID for Carrier Aggregation (DBDC)

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  rfm_device_enum_type - RF device in use by primary WCDMA operation.

SIDE EFFECTS
  None.
===========================================================================*/
#ifdef __GNUC__
static inline 
#else 
INLINE 
#endif
rfm_device_enum_type  wl1_trm_get_primary_rf_device_for_ca(void)
{
  rfm_device_enum_type  rfm_device = wl1_trm_db[WCDMA_TRM_CA].wl1_trm_primary_rf_device;

    if (rfm_device >= RFM_INVALID_DEVICE)
    {
      /* This should, in theory, never happen.  For some reason we are making
      ** a call to RFM, but we don't know the RF device in use for W primary
      ** operation.  This could happen if we end up calling RFM API before we
      ** have primary TRM lock.  Anyway, for now hard-code default. */
    ERR_FATAL("WL1_TRM: Primary RF device invalid!", 0, 0, 0);
  }

  return rfm_device;
}


/*===========================================================================
FUNCTION     WL1_TRM_GET_SECONDARY_RF_DEVICE

DESCRIPTION
  This function is invoked everytime we make a call into RFM.  All RFM APIs
  take RF device as a parameter.

  When we got TRM lock for WCDMA secondary operation earlier, we called
  trm_get_rf_device().  We stored that value away to be returned by this
  function now.

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  rfm_device_enum_type - RF device in use by secondary WCDMA operation.

SIDE EFFECTS
  None.
===========================================================================*/
#ifdef __GNUC__
static inline 
#else 
INLINE 
#endif
rfm_device_enum_type wl1_trm_get_secondary_rf_device(void)
{
#ifdef FEATURE_WCDMA_DIME_DUAL_ACTIVE
  rfm_device_enum_type  rfm_device = RFCOM_RECEIVER_DIV;


  if (wl1_trm_use_rf_device_from_trm)
  {
    rfm_device = wl1_trm_db[WCDMA_TRM].wl1_trm_secondary_rf_device;

    if (rfm_device >= RFM_WCDMA_MAX_DEVICE)
    {
      /* This should, in theory, never happen.  For some reason we are making
      ** a call to RFM, but we don't know the RF device in use for W secondary
      ** operation.  This could happen if we end up calling RFM API before we
      ** have secondary TRM lock.  Anyway, for now hard-code default. */
      WL1_MSG0(ERROR, "WL1_TRM: Secondary RF device invalid! Use default");
    }
  }

  return rfm_device;
#else
  rfm_device_enum_type	rfm_device = wl1_trm_db[WCDMA_TRM].wl1_trm_secondary_rf_device;
  
  if (rfm_device >= RFM_WCDMA_MAX_DEVICE)
  {
		/* This should, in theory, never happen.  For some reason we are making
	  ** a call to RFM, but we don't know the RF device in use for W primary
		** operation.  This could happen if we end up calling RFM API before we
	  ** have primary TRM lock.  Anyway, for now hard-code default. */
	  MSG_ERROR("WL1_TRM: SECONDARY RF device invalid!", 0, 0, 0);
  }
  return rfm_device;

#endif  /* #ifdef FEATURE_WCDMA_DIME_DUAL_ACTIVE */
}

/*===========================================================================
FUNCTION     WL1_TRM_GET_SECONDARY_RF_DEVICE_FOR_CA

DESCRIPTION
  This function return secondary rf device for carrier aggregation.
DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  rfm_device_enum_type - RF device in use by secondary WCDMA operation.

SIDE EFFECTS
  None.
===========================================================================*/
#ifdef __GNUC__
static inline 
#else 
INLINE 
#endif
rfm_device_enum_type wl1_trm_get_secondary_rf_device_for_ca(void)
{
  rfm_device_enum_type  rfm_device = wl1_trm_db[WCDMA_TRM_CA].wl1_trm_secondary_rf_device;

    if (rfm_device == RFM_INVALID_DEVICE)
    {
      /* This should, in theory, never happen.  For some reason we are making
    ** a call to RFM, but we don't know the RF device in use for W primary
      ** operation.  This could happen if we end up calling RFM API before we
    ** have primary TRM lock.  Anyway, for now hard-code default. */
    MSG_ERROR("WL1_TRM: SECONDARY RF device invalid!", 0, 0, 0);
  }
  return rfm_device;
}

/*===========================================================================
FUNCTION     WL1_TRM_GET_RF_DEVICE_FOR_TARGET

DESCRIPTION
  This function return rf device for target.
DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  rfm_device_enum_type - RF device in use for target.

SIDE EFFECTS
  None.
===========================================================================*/
extern rfm_device_enum_type wl1_trm_get_rf_device_for_target(uint16 freq);

/*===========================================================================
FUNCTION     WL1_TRM_GET_BAND

DESCRIPTION
  This function returns the band channel belongs to 

DEPENDENCIES
  None.

PARAMETERS
  uint16 channel - The channel we are tuning to

RETURN VALUE
  l1_freq_scan_band_enum_type - Enum type of the band.

SIDE EFFECTS
  None.
===========================================================================*/

extern l1_freq_scan_band_enum_type wl1_trm_get_band(uint16 channel);

/*===========================================================================
FUNCTION     WL1_TRM_REQUEST_PRIMARY_ANTENNA

DESCRIPTION
  This function requests TRM for the primary antenna before WCDMA can 
  attempt to acquire.

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  boolean - FALSE if TRM request times out.

SIDE EFFECTS
  None.
===========================================================================*/
extern boolean wl1_trm_request_primary_antenna(uint16 channel, wcdma_trm_client_type client);

/*===========================================================================
FUNCTION     WL1_TRM_REQUEST_PRIMARY_ANTENNA

DESCRIPTION
  This function releases TRM for the primary antenna when de-activating stack.

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE

SIDE EFFECTS
  None.
===========================================================================*/

extern void wl1_trm_release_primary_antenna(wcdma_trm_client_type client);

/*===========================================================================
FUNCTION     WL1_TRM_CARRIER_AGGREGATION_ON

DESCRIPTION
  This function checks if it is DBDC call or normal DC call

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
boolean to indicate if DBDC call or not

SIDE EFFECTS
  None.
===========================================================================*/
extern boolean
wl1_trm_carrier_aggregation_on
(void);

/*===========================================================================
FUNCTION     WL1_TRM_RESERVED

DESCRIPTION
  This function returns if trm resource is acquired or not.

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
   boolean
   
SIDE EFFECTS
  None.
===========================================================================*/
extern boolean wl1_trm_reserved( void );

/*===========================================================================
FUNCTION     WL1_TRM_RESERVE_CARRIER_AGGREGATION_RESOURCES

DESCRIPTION
  This function reserves TRM for Carrier Aggregation

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
boolean to indicate if DBDC call or not

SIDE EFFECTS
  None.
===========================================================================*/
extern void 
wl1_trm_reserve_carrier_aggregation_resources
(uint16 uarfcn);

/*===========================================================================
FUNCTION     WL1_TRM_RELEASE_CARRIER_AGGREGATION_RESOURCES

DESCRIPTION
  This function reserves TRM for Carrier Aggregation

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
boolean to indicate if DBDC call or not

SIDE EFFECTS
  None.
===========================================================================*/
extern void 
wl1_trm_release_carrier_aggregation_resources
(void);

#else

/*===========================================================================
FUNCTION     WL1_TRM_REQUEST_PRIMARY_ANTENNA

DESCRIPTION
  This function requests TRM for the primary antenna before WCDMA can 
  attempt to acquire.

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  boolean - FALSE if TRM request times out.

SIDE EFFECTS
  None.
===========================================================================*/
extern boolean wl1_trm_request_primary_antenna(void);

#endif /*FEATURE_TRM_DIME */

/*===========================================================================
FUNCTION     WL1_TRM_STOP_CMD

DESCRIPTION
  This function is called to send WL1 into a L1M_WAIT_FOR_TRM_STOP state.

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1_trm_stop_cmd(void);


/*===========================================================================
FUNCTION     WL1_TRM_ENABLE_RXD_CMD

DESCRIPTION
  This function is called to process TRM_ENABLE_RXD_CMD and enable WL1 RxD.

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1_trm_enable_rxd_cmd(boolean ca_enabled);

#ifdef FEATURE_TRM_DIME

/*===========================================================================
FUNCTION     WL1_TRM_REQUEST_SECONDARY_ANTENNA

DESCRIPTION
  This function requests TRM for the secondary antenna before WCDMA can 
  attempt to acquire.

DEPENDENCIES
  None.

PARAMETERS
  uint16 channel - The channel tuned to.

RETURN VALUE

SIDE EFFECTS
  None.
===========================================================================*/

extern void wl1_trm_request_secondary_antenna(uint16 channel,wcdma_trm_client_type client);


#else
/*===========================================================================
FUNCTION     WL1_TRM_REQUEST_SECONDARY_ANTENNA

DESCRIPTION
  This function requests TRM for the secondary antenna before WCDMA can 
  attempt to acquire.

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  boolean - FALSE if TRM request times out.

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1_trm_request_secondary_antenna(void);

#endif /*  FEATURE_TRM_DIME */

/*===========================================================================
FUNCTION     WL1_TRM_RELEASE_SECONDARY_ANTENNA

DESCRIPTION
  This function is called once FOFF is set to TRUE while dropping a call to
  release the secondary antenna.

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1_trm_release_secondary_antenna(wcdma_trm_client_type client);

/*===========================================================================
FUNCTION     wl1_trm_reserve_secondary_antenna

DESCRIPTION
 This function is called by RxD to reserve secondary antenna for future time

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1_trm_reserve_secondary_antenna(uint16 channel,trm_time_t when,boolean ca_enabled);
#endif /* FEATURE_WCDMA_TRM */

#ifdef FEATURE_WCDMA_TCXOMGR30

/*===========================================================================
FUNCTION     WL1_TCXO_SET_RESTRICTION

DESCRIPTION
  Invoked due to either a restriction change from TCXO Mgr, or when error becomes
  large and UMTS is breaking the TCXO Mgr restriction or we are freezing the trk_lo
  for debugging purposes. This simply posts a local command which does all the work.
  Necessary because we cant call tcxomgr_report() from interrupt context.

DEPENDENCIES
  None.

PARAMETERS
   tcxomgr_restriction_type    restriction  - (New) restriction type.
   wl1_tcxomgr_ack_type        ack          - Indicates whether the change needs to be
                                              acked or reported.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1_tcxo_set_restriction(tcxomgr_restriction_type restriction,
                                     wl1_tcxomgr_ack_type ack);

/*===========================================================================
FUNCTION     WL1_TCXO_SET_RESTRICTION_CMD

DESCRIPTION
  Invoked by the local command TCXO_SET_RESTRICTION_CMD  to actually perform the
  freezing or unfreezing of the TCXO. This function needs send acks (when TCXO
  Mgr gives us a new restriction) and reports (when breaking a restriction) to
  TCXO Mgr when necessary.

DEPENDENCIES
  None.

PARAMETERS
   tcxomgr_restriction_type    restriction  - (New) restriction type.
   wl1_tcxomgr_ack_type        ack          - Indicates whether the change needs to be
                                              acked or reported.

RETURN VALUE
  None.

SIDE EFFECTS
    None.
===========================================================================*/
extern void wl1_tcxo_set_restriction_cmd(tcxomgr_restriction_type restriction,
                                         wl1_tcxomgr_ack_type ack);

/*===========================================================================
FUNCTION     WL1_TCXO_RESUME_RESTRICTION

DESCRIPTION
  Resumes current restriction after WL1 diverges from the restriction due to error
  in the VCO.

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1_tcxo_resume_restriction(void);

/*===========================================================================
FUNCTION     WL1_TCXO_RELEASE

DESCRIPTION
  Releases control of the TCXO.

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1_tcxo_release(void);

/*===========================================================================
FUNCTION     WL1_TCXO_GET_VCO

DESCRIPTION
  This function returns the vco value from the mdsp.

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  tcxomgr_vco_type - returns the PDM value.

SIDE EFFECTS
  None.
===========================================================================*/
extern tcxomgr_vco_type wl1_tcxo_get_vco(void);

#ifdef FEATURE_WCDMA_TCXOMGR30
/*===========================================================================
FUNCTION     WL1_TRM_SET_STARTING_TRK_LO

DESCRIPTION
  This function is called after WL1 has TCXO control from TCXO Manager, and
  after WCDMA firmware has been started.  These things happen when WCDMA is
  started up.  This function then sets the starting trk_lo value in firmware.
  WL1 received this value earlier from TCXO Manager.  It is the last known
  value of trk_lo, before WCDMA was activated.

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1_trm_set_starting_trk_lo(void);
#endif

/*===========================================================================
FUNCTION     WL1_TCXO_FROZEN

DESCRIPTION
  Returns if the tcxo is currently frozen or not.

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  boolean - TRUE if the TCXO is frozen.

SIDE EFFECTS
  None.
===========================================================================*/
extern boolean wl1_tcxo_frozen(void);

/*===========================================================================
FUNCTION     WL1_TCXO_DISABLE_PDM_FOR_SLEEP

DESCRIPTION
  Disable TRK_LO PDM via tcxomgr, if there is no restriction on the TCXO.

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1_tcxo_disable_pdm_for_sleep(void);

/*===========================================================================
FUNCTION     WL1_TCXO_ENABLE_PDM_FOR_WAKE

DESCRIPTION
  Enable TRK_LO PDM via tcxomgr.

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1_tcxo_enable_pdm_for_wake(void);

/*===========================================================================
FUNCTION     WL1_TCXO_RPUSH

DESCRIPTION
  Performs the actual rotator push when using TCXO Mgr 3.0

DEPENDENCIES
  None.

PARAMETERS
  tcxomgr_client_state_type state    - Indicates whether WL1 is in 
                                       Idle/Traffic/OOS.

RETURN VALUE
  boolean                            - Indicates whether the Rotator push
                                       succeeded. (It can fail if there are no
                                       samples in the accumulator)

SIDE EFFECTS
  None.
===========================================================================*/
extern boolean wl1_tcxo_rpush(tcxomgr_client_state_type state);

/*===========================================================================
FUNCTION     WL1_TCXO_RPUSH_INIT

DESCRIPTION
  Initialize RPush while L1 is entering BCH, FACH and DCH states.

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  None.
  
SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1_tcxo_rpush_init(void);

/*===========================================================================
FUNCTION     WL1_TCXO_RPUSH_CLEANUP

DESCRIPTION
  Clean up while L1 is leaving BCH, FACH and DCH states.

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  None.
  
SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1_tcxo_rpush_cleanup(void);

/*===========================================================================
FUNCTION     WL1_TCXO_REQUEST

DESCRIPTION
  This function requests TCXO Mgr for control of the VCTCXO.

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  boolean - FALSE if TCXO request times out, or TCXO Mgr denies control to WCDMA.

SIDE EFFECTS
  None.
===========================================================================*/
extern boolean wl1_tcxo_request(void);
#endif /* FEATURE_WCDMA_TCXOMGR30 */

#ifdef FEATURE_DUAL_SIM
#define WL1_IN_DS_MODE() (wl1_ds_db.wl1_in_ds_mode)
#define WL1_IN_DUAL_ACTIVE_MODE() \
        ( \
         ( (wl1_ds_db.wl1_device_mode == WL1_DUAL_ACTIVE) && (wl1_ds_db.wl1_standby_mode == WL1_DUAL_STANDBY) ) \
         ? TRUE : FALSE \
        )
#define WL1_NOT_ACTIVE() ((l1m_state == L1M_DEACTIVE) || (l1m_state == L1M_DEEP_SLEEP) || (l1m_state == L1M_STOPPED))
#else
#define WL1_IN_DS_MODE() (FALSE)
#define WL1_IN_DUAL_ACTIVE_MODE() (FALSE)
#endif  /*FEATURE_DUAL_SIM*/

#ifdef FEATURE_DUAL_SIM
/*Enum to identify L1 and RRC to WRM*/
typedef enum {
WRM_CLIENT_L1,
WRM_CLIENT_RRC,
WRM_CLIENT_MAX
} wrm_client_enum_type;

/*Enum to identify WRM state*/
typedef enum {
WRM_NO_LOCK_STATE,
WRM_LOCK_REQ_STATE,
WRM_LOCK_RES_STATE,
WRM_IN_LOCK_STATE,
WRM_STATE_MAX
} wrm_state_enum_type;

/*Enum to identify L1 and RRC state in WRM*/
typedef enum {
WRM_CLI_NO_LOCK_STATE,
WRM_CLI_LOCK_REQ_STATE,
WRM_CLI_LOCK_RES_STATE,
WRM_CLI_IN_LOCK_STATE,
WRM_CLI_STATE_MAX
} wrm_client_state_enum_type;

/* standby mode enums*/
typedef enum {
WL1_SINGLE_STANDBY = 0,
WL1_DUAL_STANDBY
#ifdef FEATURE_TRIPLE_SIM
, WL1_TRIPLE_STANDBY
#endif
} wl1_standby_enum_type;

/* device mode enums*/
typedef enum {
WL1_SINGLE_ACTIVE = 0,
WL1_DUAL_ACTIVE
} wl1_device_mode_enum_type;

/*Structure to hold L1 and RRC specific information in WRM*/
typedef struct {
wrm_client_state_enum_type cli_state;

trm_duration_t req_dur;

trm_time_t res_starts_at;

trm_duration_t res_dur;

trm_resource_enum_t req_resource;

trm_reason_enum_t req_reason;

trm_grant_callback_t grant_cb;

trm_request_tag_t tag;
} wrm_client_info_struct_type;

/*Structure to hold WRM information*/
typedef struct {
wrm_state_enum_type wrm_state;

wrm_client_info_struct_type wrm_client_info[WRM_CLIENT_MAX];
} wrm_db_struct_type;

extern wrm_db_struct_type wrm_db;

/*Initialization macros*/
#define WRM_INVALID_TIME_DURATION 0
#define WRM_INVALID_RES_AT_TIME 0
#define WRM_INVALID_TAG 0xFF

/*Identity of WRM/WCDMA with TRM*/
#if 0
#define WRM_TRM_CLIENT_IDENTITY TRM_UMTS
#else
#define WRM_TRM_CLIENT_IDENTITY TRM_WCDMA
#endif

/*Macros to check validity of WRM and WRM_TRM client identities*/
#define WRM_IS_CLIENT_ID_VALID(wrm_client_id) (wrm_client_id < WRM_CLIENT_MAX)
#define WRM_IS_TRM_CLIENT_ID_VALID(trm_client_id) (trm_client_id == WRM_TRM_CLIENT_IDENTITY)

/*Macros to check the state of WRM*/
#define WRM_STATE_IS_IN_LOCK() (wrm_db.wrm_state == WRM_IN_LOCK_STATE)
#define WRM_STATE_IS_LOCK_REQ() (wrm_db.wrm_state == WRM_LOCK_REQ_STATE)
#define WRM_STATE_IS_LOCK_RES() (wrm_db.wrm_state == WRM_LOCK_RES_STATE)
#define WRM_STATE_IS_NO_LOCK() (wrm_db.wrm_state == WRM_NO_LOCK_STATE)

/*Macros to check the state of WRM clients*/
#define WRM_CLI_STATE_IS_IN_LOCK(wrm_client_id) (wrm_db.wrm_client_info[wrm_client_id].cli_state == WRM_CLI_IN_LOCK_STATE)
#define WRM_CLI_STATE_IS_LOCK_REQ(wrm_client_id) (wrm_db.wrm_client_info[wrm_client_id].cli_state == WRM_CLI_LOCK_REQ_STATE)
#define WRM_CLI_STATE_IS_LOCK_RES(wrm_client_id) (wrm_db.wrm_client_info[wrm_client_id].cli_state == WRM_CLI_LOCK_RES_STATE)
#define WRM_CLI_STATE_IS_NO_LOCK(wrm_client_id) (wrm_db.wrm_client_info[wrm_client_id].cli_state == WRM_CLI_NO_LOCK_STATE)

/*Macros to get and set the state of WRM*/
#define WRM_SET_WRM_STATE(state_of_wrm) (wrm_db.wrm_state = state_of_wrm)
#define WRM_GET_WRM_STATE() (wrm_db.wrm_state)

/*Macros to get and set the state of WRM client*/
#define WRM_SET_WRM_CLI_STATE(wrm_client_id, wrm_cli_state) (wrm_db.wrm_client_info[wrm_client_id].cli_state = wrm_cli_state)
#define WRM_GET_WRM_CLI_STATE(wrm_client_id) (wrm_db.wrm_client_info[wrm_client_id].cli_state)

/*Macros to get alternate WRM client id*/
#define WRM_GET_ALTERNATE_CLIENT_ID(wrm_client_id) (wrm_client_enum_type)((wrm_client_id + (wrm_client_enum_type)1) % WRM_CLIENT_MAX)

#define WRM_GET_TAG_FROM_CLI_ID(wrm_client_id) (wrm_db.wrm_client_info[wrm_client_id].tag)

#define WL1_INTRA_F_SRCH_DUR (M_BY_N_MS_IN_SC(60,1))
#define WL1_INTER_F_SRCH_DUR (M_BY_N_MS_IN_SC(80,1))
#define WL1_INTER_RAT_SRCH_DUR (M_BY_N_MS_IN_SC(100,1))
#define WL1_LTE_SRCH_DUR (M_BY_N_MS_IN_SC(40,1))


#define WL1_DS_NUM_PEND_EXT_CMD 3

#define WL1_DS_INVALID_CMD_IDX 0xFF

/*Unit test definitions*/
#if 0
#define TRM_LAST_RESOURCE 1
#define TRM_CHANNEL_MAINTENANCE 1
#define TRM_NUM_REASONS 1
#endif

/*WL1 dual sim procedures*/
typedef enum {
  WL1_DS_PROC_START=0,
  WL1_DS_PROC_SUSPEND=1,
  WL1_DS_PROC_RESUME=2,
  WL1_DS_PROC_IDLE=3,
  WL1_DS_PROC_STOP=4,
  WL1_DS_PROC_DEACTIVATE=5,
  WL1_DS_PROC_FS=6,
  WL1_DS_PROC_ACQ=7,
  WL1_DS_PROC_SETUP_SBCH=8,
  WL1_DS_PROC_SETUP_NBCH=9,
  WL1_DS_PROC_SETUP_PCH=10,
  WL1_DS_PROC_SETUP_CTCH_FACH=11,
  WL1_DS_PROC_SETUP_DTCH_FACH=12,
  WL1_DS_PROC_SETUP_DCH=13,
  WL1_DS_PROC_PAGING=14,
  WL1_DS_PROC_CELL_RESEL_START=15,
  WL1_DS_PROC_CELL_TRANS_REQ=16,
  WL1_DS_PROC_DROP_CTCH=17,
  WL1_DS_PROC_BPLMN=18,
  WL1_DS_PROC_BPLMN_BCCH_DROP=19,
  WL1_DS_PROC_DRX=20,
  WL1_DS_PROC_GEN_CPHY_SETUP_REQ =21,
  WL1_DS_PROC_G2W_TA_IDLE=22,
  WL1_DS_PROC_G2W_TA_CELL_SELECTION_REQ=23,
  WL1_DS_PROC_MAX=24
}wl1_ds_proc_enum_type;


/* WRM lock release mode type for split acq in L1M_ACQ state */
typedef enum{
  WRM_LOCK_RELEASE_REGULAR_MODE = 0,
  WRM_LOCK_RELEASE_OFFLINE_NASTT_MODE,
  WRM_LOCK_RELEASE_MAX_NUM_MODES
}wrm_lock_release_type;


/*WL1 dual sim paging specific information structure*/
typedef struct {
  boolean lock_succ;

  uint16 declare_oos_thresh;

  uint32 trm_lock_consecutive_fail_cnt;

  boolean cell_trans_req_stat;

  boolean wl1_in_paging_wakeup;
} wl1_ds_paging_spec_info_struct_type;

/*Structure to hold WL1 dual sim information */
typedef struct {
  /*Whether WL1 is initializing RF and mdsp*/
  boolean wl1_in_ds_prep_mode;

  /*True: UE is in dual sim mode; False: UE is in single sim mode*/
  boolean wl1_in_ds_mode;

  /* standby mode: TRUE for Dual standby, FALSE for single standby */
  wl1_standby_enum_type wl1_standby_mode;

#ifdef FEATURE_TRIPLE_SIM
  /* Tells whether it is */
  rrc_multi_sim_mode_type wl1_multi_sim_mode;
#endif

  /* device mode: TRUE for dual active , FALSE for single active*/
  wl1_device_mode_enum_type wl1_device_mode;

  /*WL1 procedures that are holding TRM lock*/
  uint32 wl1_ds_proc_mask;

  /*Last WL1 procedure that requested for TRM lock*/
  wl1_ds_proc_enum_type ds_curr_proc;

  /*Last WL1 procedure that requested for TRM lock*/
  wl1_ds_proc_enum_type ds_pend_proc;

  /*Last cphy_setup_cmd procedure that requested for TRM lock*/
  wl1_ds_proc_enum_type last_cphy_setup_proc;

  /*Indicate whether cmd is pending waiting for the sleep proc to be completed*/
  boolean trm_lock_sig_pending_on_sleep;

  /* Used to store the TRM lock signal (success/failure) in
    case TRM signal is received in the middle of paging wakeup;
    only applicable if trm_lock_sig_pending_on_sleep = TRUE */
  rex_sigs_type trm_lock_sig;

  /* Flag to indicate Ext cmd processing with no TRM lock */
  boolean process_ext_cmd_without_lock;
}wl1_ds_db_struct_type;

/*Structure to hold WL1 dual sim statistics*/
typedef struct {
  uint32 lock_req_and_not_cnt;

  uint32 lock_req_and_not_succ_cnt;

  uint32 lock_req_and_not_fail_cnt;

  uint32 lock_req_cnt;

  uint32 lock_req_succ_cnt;

  uint32 lock_req_fail_cnt;

  uint32 lock_res_cnt;
} wl1_ds_overall_stat_struct_type;

typedef void WL1_DS_PROC_CB_PTR(void);

/*Structure to hold WL1 dual sim information on a per procedure basis*/
typedef struct {
  /*Process the RRC command even if lock failed,
  but without mdsp and RF*/
  boolean can_lock_fail;

  /* The RF resource which is being requested */
  trm_resource_enum_t resource_needed;

  /* How long the resource will be needed for (in milli secs) */
  uint32 lock_req_dur_in_ms;

  /* Why the resource is needed (used for priority decisions) */
  trm_reason_enum_t lock_reason;

  /* Callback to notify client when resource is granted */
  WL1_DS_PROC_CB_PTR *lock_grant_callback;

  uint32 lock_req_timeout_in_ms;

  uint32 trm_lock_proc_fail_cnt;

  uint32 trm_lock_proc_succ_cnt;

  l1_ext_cmd_type *ext_cmd_ptr;

  l1_cmd_enum_type rrc_to_l1_cmd;

  rrc_cmd_e_type l1_to_rrc_cnf;
}wl1_ds_proc_info_struct_type;

extern wl1_ds_db_struct_type wl1_ds_db;
extern wl1_ds_paging_spec_info_struct_type wl1_ds_paging_spec_info;
extern wl1_ds_proc_info_struct_type wl1_ds_proc_info[WL1_DS_PROC_MAX];

#ifdef FEATURE_DUAL_SIM
extern boolean wrm_split_acq_offline_nastt_in_progress;
#endif

/*If TRM lock request fails consecutively for this threshold count, then OOS
is declared to RRC*/
#define WL1_DS_PAG_TRM_LOCK_OOS_THRESH (8)

/*Macros to check whethe a procedure is valid and of particular type*/
#define WL1_DS_IS_PROC_VALID(ds_proc) (ds_proc < WL1_DS_PROC_MAX)

#define WL1_DS_IS_PROC_VALID_FOR_RESERVE(ds_proc) (ds_proc == WL1_DS_PROC_PAGING)

#define WL1_DS_IS_PROC_FOR_SETUP(ds_proc) ((ds_proc == WL1_DS_PROC_SETUP_SBCH) || \
  (ds_proc == WL1_DS_PROC_SETUP_NBCH) || (ds_proc == WL1_DS_PROC_SETUP_PCH) || \
  (ds_proc == WL1_DS_PROC_SETUP_CTCH_FACH) || (ds_proc == WL1_DS_PROC_SETUP_DTCH_FACH) || \
  (ds_proc == WL1_DS_PROC_SETUP_DCH))

/*Macros to handle statistics corresponding to a procedure*/
#define WL1_DS_INC_FAIL_CNT_FOR_PROC(ds_proc) (wl1_ds_proc_info[ds_proc].trm_lock_proc_fail_cnt++)

#define WL1_DS_INC_SUCC_CNT_FOR_PROC(ds_proc) (wl1_ds_proc_info[ds_proc].trm_lock_proc_succ_cnt++)

#define WL1_DS_INC_OVERALL_FAIL_CNT() (wl1_ds_overall_stat.lock_req_fail_cnt++)

#define WL1_DS_INC_OVERALL_SUCC_CNT() (wl1_ds_overall_stat.lock_req_succ_cnt++)

#define WL1_DS_INC_PAGING_CONSEC_FAIL_CNT() (wl1_ds_paging_spec_info.trm_lock_consecutive_fail_cnt++)

#define WL1_DS_IS_CMD_PTR_VALID(ds_proc) (wl1_ds_proc_info[ds_proc].ext_cmd_ptr != NULL)

/*Macros to get various fields of a procedure*/
#define WL1_DS_CAN_LOCK_FAIL_FOR_PROC(ds_proc) (wl1_ds_proc_info[ds_proc].can_lock_fail)

#define WL1_DS_GET_RESOURCE_FOR_PROC(ds_proc) (wl1_ds_proc_info[ds_proc].resource_needed)

#define WL1_DS_GET_DUR_IN_MS(ds_proc) (wl1_ds_proc_info[ds_proc].lock_req_dur_in_ms)

#define WL1_DS_GET_DUR_IN_SCLK(ds_proc) (M_BY_N_MS_IN_SC(WL1_DS_GET_DUR_IN_MS(ds_proc),1))

#define WL1_DS_GET_LOCK_RSN_FOR_PROC(ds_proc) (wl1_ds_proc_info[ds_proc].lock_reason)

#define WL1_DS_GET_CB_PTR_FOR_PROC(ds_proc) (wl1_ds_proc_info[ds_proc].lock_grant_callback)

#define WL1_DS_GET_LOCK_REQ_TIMEOUT_FOR_PROC(ds_proc) (wl1_ds_proc_info[ds_proc].lock_req_timeout_in_ms)

#define WL1_DEFAULT_GRANT_CB (wl1_ds_grant_cb)

/*Macros to handle WL1 dual sim procedure bit mask*/
#define WL1_DS_PROC_BMSK(ds_proc) ((uint32)(1 << ((uint32)ds_proc)))

#define WL1_DS_RESET_DS_PROC_BMASK() \
(wl1_ds_db.wl1_ds_proc_mask = 0)

#define WL1_DS_ADD_PROC_TO_DS_BMASK(ds_proc) \
  (wl1_ds_db.wl1_ds_proc_mask |= WL1_DS_PROC_BMSK(ds_proc))

#define WL1_DS_REM_PROC_FROM_DS_BMASK(ds_proc) \
  (wl1_ds_db.wl1_ds_proc_mask &= (~(WL1_DS_PROC_BMSK(ds_proc))))


/*Macros to check whether any dual sim procedure is active in WL1*/
#define WL1_IN_DS_WITH_ANY_LOCK() (WL1_IN_DS_MODE() && ((WRM_CLI_STATE_IS_IN_LOCK(WRM_CLIENT_L1)) || (wl1idle_ta_mgr_lock_rel_offline_activity_pending())))

#define WL1_IN_DS_WITH_NO_LOCK() (WL1_IN_DS_MODE() && ((!WRM_CLI_STATE_IS_IN_LOCK(WRM_CLIENT_L1)) && (!wl1idle_ta_mgr_lock_rel_offline_activity_pending())))

#define WL1_DS_IS_ANY_PROC_ACTIVE() (wl1_ds_db.wl1_ds_proc_mask != 0)

#define WL1_DS_IS_NO_PROC_ACTIVE() (wl1_ds_db.wl1_ds_proc_mask == 0)

#define WL1_DS_IS_PROC_ACTIVE(ds_proc) \
  ((wl1_ds_db.wl1_ds_proc_mask & WL1_DS_PROC_BMSK(ds_proc)) != 0)
 
#define WL1_IS_ANY_OTHER_PROC_ACTIVE(ds_proc) \
  ((wl1_ds_db.wl1_ds_proc_mask & (~(WL1_DS_PROC_BMSK(ds_proc)))) != 0)

#define WL1_IN_DS_WITH_PAG_LOCK() (WL1_IN_DS_MODE() && (wl1_ds_paging_spec_info.lock_succ))

#define WL1_IN_DS_WITH_NO_PAG_LOCK() (WL1_IN_DS_MODE() && (!wl1_ds_paging_spec_info.lock_succ))

#define WL1_IS_CELL_TRANS_REQ_SUCC() (wl1_ds_paging_spec_info.cell_trans_req_stat)

#define WL1_IS_IN_PCH_SLEEP_DS_NO_LOCK() ((WL1_IN_DS_MODE()) && \
  (l1m_state == L1M_PCH_SLEEP) && \
  (WL1_IN_DS_WITH_NO_LOCK()))

#define WL1_IS_IN_PCH_SLEEP_DS_LOCK() ((WL1_IN_DS_MODE()) && \
  (l1m_state == L1M_PCH_SLEEP) && \
  (WL1_IN_DS_WITH_ANY_LOCK()))

#define WL1_DS_IS_CMD_IDX_INVALID(cmd_idx) \
  (cmd_idx == WL1_DS_INVALID_CMD_IDX)

#define WL1_DS_IS_TRM_SIG_PENDING_ON_SLEEP() \
  (wl1_ds_db.trm_lock_sig_pending_on_sleep == TRUE)

#define WL1_DS_IS_WL1_IN_PAGING_WAKEUP() \
  (wl1_ds_paging_spec_info.wl1_in_paging_wakeup == TRUE)

/*===========================================================================
FUNCTION     WRM_REQUEST_AND_NOTIFY

DESCRIPTION
  This function makes an asynchronous lock request to TRM.If WRM already has
  TRM lock, it is provided immediately to the caller. WRM never times out in waiting
  for the TRM lock. The caller of wrm_request_and_notify() should should implement
  any timer for the call.If the caller decides to timeout the request, the caller should
  call wrm_cancel_reserve_notify to cancel the pending request.

DEPENDENCIES
  None.

PARAMETERS
  wrm_client_id: The client which needs the RF resource
  resource: The RF resource which is being requested
  duration: How long the resource will be needed for (in sclks) 
  reason: Why the resource is needed (used for priority decisions)
  grant_callback: Callback to notify client when resource is granted 
  tag: Anonymous payload to be echoed through grant_callback()

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
void wrm_request_and_notify(
  /* The client which needs the RF resource */
  wrm_client_enum_type wrm_client_id,

  /* The RF resource which is being requested */
  trm_resource_enum_t             resource,

  /* How long the resource will be needed for (in sclks) */
  trm_duration_t                  duration,

  /* Why the resource is needed (used for priority decisions) */
  trm_reason_enum_t               reason,

  /* Callback to notify client when resource is granted */
  trm_grant_callback_t            grant_callback,

  /* Anonymous payload to be echoed through grant_callback() */
  trm_request_tag_t               tag);

/*===========================================================================
FUNCTION     WRM_GRANT_CALLBACK

DESCRIPTION
  This function is called by TRM with lock status for WCDMA.  All it does is
  post a local command for processing later in WL1 task context.

DEPENDENCIES
  None.

PARAMETERS
  trm_client_enum_t: The client which needs the RF resource
  event: The RF resource which is being granted
  tag: Anonymous payload passed in wrm_request_and_notify()

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
extern void wrm_grant_callback(
  /* The client which needs the RF resource */
  trm_client_enum_t client, 

  /*The granted Chain*/
  trm_grant_event_enum_t event,

  /*The tag that WRM passed to TRM in trm_request_and_notify(...)*/
  trm_request_tag_t tag);

/*===========================================================================
FUNCTION     WRM_GRANT_CALLBACK_CMD_HANDLER

DESCRIPTION
  This function is the local command handler for TRM grant callback to WCDMA.
  It in turn calls the client(L1/RRC) callback.

DEPENDENCIES
  None.

PARAMETERS
  trm_client_enum_t: The client which needs the RF resource
  event: The RF resource which is being granted
  tag: Anonymous payload passed in wrm_request_and_notify()

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
extern void wrm_grant_callback_cmd_handler(
  /* The client which needs the RF resource */
  trm_client_enum_t client, 

  /*The granted Chain*/
  trm_grant_event_enum_t event,

  /*The tag that WRM passed to TRM in trm_request_and_notify(...)*/
  trm_request_tag_t tag);

/*===========================================================================
FUNCTION     WRM_UNLOCK_TRM_CB

DESCRIPTION
  This function is called by TRM when WCDMA needs to release the lock for
  the sake of higher priority activity in other clients.

DEPENDENCIES
  WCDMA must be holding an RF resource lock

PARAMETERS
  unlock_data : Data for unlock callback advanced.

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
void wrm_unlock_trm_cb(
  trm_unlock_cb_advanced_data_t* unlock_data
);

/*===========================================================================
FUNCTION     WRM_RESERVE_AT

DESCRIPTION
  Specifies the given client needs the given transceiver resource at the given
  time, for the given duration, for the supplied reason.

  This would be used with the "wrm_request()" function, below.
  
DEPENDENCIES
  None

PARAMETERS
  wrm_client_id: The client which needs the RF resource
  resource: The RF resource which is being requested
  duration: How long the resource will be needed for (in sclks) 
  reason: Why the resource is needed (used for priority decisions)
  when: When the resource will be needed (sclks timestamp)

RETURN VALUE
  None

SIDE EFFECTS
  If the client currently holds an RF chain, the chain is released.
===========================================================================*/
void wrm_reserve_at(
  /* The client which needs the RF resource */
  wrm_client_enum_type               wrm_client_id,

  /* The RF resource which is being requested */
  trm_resource_enum_t             resource,

  /* When the resource will be needed (sclks timestamp) */
  trm_time_t                      when,

  /* How long the resource will be needed for (in sclks) */
  trm_duration_t                  duration,

  /* Why the resource is needed (used for priority decisions) */
  trm_reason_enum_t               reason
);

/*============================================================================
FUNCTION WRM_REQUEST

DESCRIPTION
  Specifies the given client needs the given transciever resource, for the
  given duration, for the supplied reason. The resource request is immediately
  evaluated, and the result returned. This may be used in conjunction with
  wrm_reserve_at().

DEPENDENCIES
  None

PARAMETERS
  wrm_client_id: The client which needs the RF resource
  resource: The RF resource which is being requested
  duration: How long the resource will be needed for (in sclks) 
  reason: Why the resource is needed (used for priority decisions)

RETURN VALUE
  TRM_DENIED          - the client was denied the transceiver resources.
  TRM_GRANTED_CHAIN0  - the client can now use the primary RF chain.
  TRM_GRANTED_CHAIN1  - the client can now use the secondary RF chain.

SIDE EFFECTS
  If the client currently holds an RF chain, that chain is released before
  the request is evaluated.
============================================================================*/
trm_grant_event_enum_t wrm_request(
  /* The client which needs the RF resource */
  wrm_client_enum_type wrm_client_id,

  /* The transceiver resource which is being requested */
  trm_resource_enum_t             resource,

  /* How long the resource will be needed for (in sclks) */
  trm_duration_t                  duration,

  /* Why the resource is needed (used for priority decisions) */
  trm_reason_enum_t               reason);

/*============================================================================
FUNCTION WRM_CHANGE_PRIORITY

DESCRIPTION
  When a client changes what it is doing, it should change the advertised
  reason for holding the RF resource, so its priority will change.

  Eg) A client request the RF resource for listening for a PAGE.  If it
  receives one, it would change its priority to PAGE_RESPONSE and attempt
  to respond to the page, and eventually to TRAFFIC.

DEPENDENCIES
  The client must be holding an RF resource lock

PARAMETERS
  wrm_client_id: The client which needs to cancel
  reason: The new reason why the RF lock is held

RETURN VALUE
  None

SIDE EFFECTS
  None
============================================================================*/
void wrm_change_priority(
  /* The client whose priority is to be changed */
  wrm_client_enum_type               client_id,

  /* The new resource why the RF lock is held (used for priority decisions) */
  trm_reason_enum_t               reason
);

/*============================================================================
FUNCTION WRM_RETAIN_LOCK

DESCRIPTION
  Informs the Transceiver Resource Manager that the client wants to hold
  the resource indefinitely.  The TRM may inform the client that it must
  give up the lock through the supplied unlock callback.
  
DEPENDENCIES
  The client must be holding a transceiver resource lock

PARAMETERS
  wrm_client_id: The client which needs to cancel
  unlock_callback: Callback that may be called by TRM to release the lock

RETURN VALUE
  None

SIDE EFFECTS
  None
============================================================================*/
void wrm_retain_lock(
  /* The client which needs the RF resource */
  wrm_client_enum_type wrm_client_id,

  /* Function to call when lock must be given up. */
  trm_unlock_callback_advanced_t unlock_callback
);
/*===========================================================================
FUNCTION     WRM_REQUEST_AND_NOTIFY

DESCRIPTION
  This function makes an asynchronous lock request to TRM.If WRM already has
  TRM lock, it is provided immediately to the caller. WRM never times out in waiting
  for the TRM lock. The caller of wrm_request_and_notify() should should implement
  any timer for the call.If the caller decides to timeout the request, the caller should
  call wrm_cancel_reserve_notify to cancel the pending request.

DEPENDENCIES
  None.

PARAMETERS
  wrm_client_id: The client which needs the RF resource
  resource: The RF resource which is being requested
  duration: How long the resource will be needed for (in sclks) 
  reason: Why the resource is needed (used for priority decisions)
  grant_callback: Callback to notify client when resource is granted 
  tag: Anonymous payload to be echoed through grant_callback()

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
void wrm_request_and_notify(
  /* The client which needs the RF resource */
  wrm_client_enum_type wrm_client_id,

  /* The RF resource which is being requested */
  trm_resource_enum_t             resource,

  /* How long the resource will be needed for (in sclks) */
  trm_duration_t                  duration,

  /* Why the resource is needed (used for priority decisions) */
  trm_reason_enum_t               reason,

  /* Callback to notify client when resource is granted */
  trm_grant_callback_t            grant_callback,

  /* Anonymous payload to be echoed through grant_callback() */
  trm_request_tag_t               tag);

/*============================================================================
FUNCTION WRM_RELEASE

DESCRIPTION
  This function releases the lock previously acquired through wrm_request()
  or wrm_request_and_notify() .

DEPENDENCIES
  None

PARAMETERS
  wrm_client_id: The client which needs to cancel

RETURN VALUE
  None

SIDE EFFECTS
  None
============================================================================*/
void wrm_release(
  /* The client which needs the RF resource */
  wrm_client_enum_type wrm_client_id);

/*============================================================================
FUNCTION WRM_EXTEND_DURATION

DESCRIPTION
  Attempts to extend the duration an RF resource lock is held for.
  If the entire extension can be granted, it will be granted.
  If the entire extension cannot be granted, the lock duration remains
  unchanged, and the client should release the lock at the original lock
  expiry point.

  If the client had originally locked the resource for longer than the
  required extension, the lock will remain the original length
  and the extension will be INDICATED AS BEING GRANTED.

  The extension is all or nothing.  If a partial extension is desired, the
  trm_change_duration( ) function should be used.
  
DEPENDENCIES
  The client must be holding an RF resource lock

PARAMETERS
  wrm_client_id: The client which needs to cancel
  new_duration: The required extension, in sclks, from "now"

RETURN VALUE
  TRUE if the lock duration extends from "now" to "now + maximum".
  FALSE if the lock duration is unchanged.

SIDE EFFECTS
  None
============================================================================*/
extern boolean wrm_extend_duration
(
  /* The client which is attempting to extend the lock duration */
  wrm_client_enum_type wrm_client_id,

  /* The required extension, in sclks, from "now" */
  trm_duration_t                  new_duration
);

/*===========================================================================
FUNCTION     WRM_INIT

DESCRIPTION
  This function must be called during UE power-up to initialize the WRM globals.
  the sake of higher priority activity in other clients.

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
void wrm_init(void);

/*===========================================================================
FUNCTION     WL1_DS_INIT

DESCRIPTION
  This function initialized WL1 dual sim related globals. This function must be
  once after power-on.

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1_ds_init(void);

/*===========================================================================
FUNCTION     WL1_DS_REQ_AND_NOT_LOCK_LOCK_FOR_PROC

DESCRIPTION
  This function requests for lock from WRM and returns success/failure to the
  caller.

DEPENDENCIES
  None.

PARAMETERS
  ds_proc:.the procedure for which WL1 needs the TRM lock

RETURN VALUE
  TRUE: lock acquisition successful
  FALSE: lock acquisition failed

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1_ds_req_and_ntfy_lock_for_proc(wl1_ds_proc_enum_type ds_proc);

/*===========================================================================
FUNCTION     WL1_DS_REQ_LOCK_FOR_PROC

DESCRIPTION
  This function requests lock for WL1 paging.

DEPENDENCIES
  None.

PARAMETERS
  ds_proc:.the procedure for which WL1 needs the TRM lock

RETURN VALUE
  trm_grant_event_enum_t: the granted Chain

SIDE EFFECTS
  None.
===========================================================================*/
extern trm_grant_event_enum_t wl1_ds_req_lock_for_proc(wl1_ds_proc_enum_type ds_proc);

/*===========================================================================
FUNCTION     WL1_DS_GRANT_CB

DESCRIPTION
  This function is called by WRM with lock status for WL1.

DEPENDENCIES
  None.

PARAMETERS
  trm_client_enum_t: The client which needs the RF resource
  event: The RF resource which is being granted
  tag: Anonymous payload passed in wrm_request_and_notify()

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1_ds_grant_cb(trm_client_enum_t client,
  trm_grant_event_enum_t event, trm_request_tag_t tag);

/*===========================================================================
FUNCTION     WL1_DS_FIND_PROC_FROM_SETUP_CMD

DESCRIPTION
  This function is called with the cphy_setup_cmd pointeto find whether any dual
  sim procedure is associated with this command.

DEPENDENCIES
  This function does not acquire lock for the associated procedure. The caller 
  must call wl1_ds_req_and_not_lock_lock_for_proc() to acquire lock based
  on the procedure returned by this function.

PARAMETERS
  ext_cmd_ptr: cphy_setup_cmd pointer sent by RRC

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
extern wl1_ds_proc_enum_type wl1_ds_find_proc_from_setup_cmd(l1_ext_cmd_type *ext_cmd_ptr);

/*===========================================================================
FUNCTION     WL1_DS_FIND_PROC_FROM_EXT_CMD

DESCRIPTION
  This function is called with the command pointer associated with any
  command from RRC. It finds whether any dual sim procedure is associated
  with this command.

DEPENDENCIES
  This function does not acquire lock for the associated procedure. The caller 
  must call wl1_ds_req_and_not_lock_lock_for_proc() to acquire lock based
  on the procedure returned by this function.

PARAMETERS
  ext_cmd_ptr: Command pointer associated with the command from upper layers

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
extern wl1_ds_proc_enum_type wl1_ds_find_proc_from_ext_cmd(l1_ext_cmd_type *ext_cmd_ptr);

/*===========================================================================
FUNCTION     WL1_DS_FIND_PROC_FROM_SETUP_CNF

DESCRIPTION
  This function is called with the command pointer associated with any confirm
  sent from L1 to RRC. It finds whether any dual sim procedure is associated
  with this command.

DEPENDENCIES
  This function does not release/acquire lock for the associated procedure. The caller 
  must call wl1_ds_req_and_not_lock_lock_for_proc() or wrm_release() to
  acquire/release the lock.

PARAMETERS
  cnf_to_rrc_ptr: Confirm message sent by L1 to RRC.

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
extern wl1_ds_proc_enum_type wl1_ds_find_proc_from_ext_cnf(rrc_cmd_type *cnf_to_rrc_ptr);

/*===========================================================================
FUNCTION     WL1_DS_IS_LOCK_NEEDED_FOR_EXT_CMD

DESCRIPTION
  This function is called with the command pointer associated with any
  command from RRC. It finds whether any dual sim procedure is associated
  with this command.

DEPENDENCIES
  This function does not acquire lock for the associated procedure. The caller 
  must call wl1_ds_req_and_not_lock_lock_for_proc() to acquire lock based
  on the procedure returned by this function.

PARAMETERS
  ext_cmd_ptr: Command pointer associated with the command from upper layers

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
extern wl1_ds_proc_enum_type wl1_ds_is_lock_needed_for_ext_cmd(l1_ext_cmd_type *ext_cmd_ptr);

/*===========================================================================
FUNCTION     WL1_DS_HDLE_LOCK_ACQ_SUCC

DESCRIPTION
  This function handles TRM lock acquistion success for various procedures.
  It re-initializes mdsp/RF and increments any counters to track
  success.

DEPENDENCIES
  This function should not re-init mdsp/RF for paging procedure.

PARAMETERS
  ds_proc: Current dual sim procedure

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1_ds_hdle_lock_acq_succ(wl1_ds_proc_enum_type ds_proc);

/*===========================================================================
FUNCTION     WL1_DS_HDLE_CNF_SENT_TO_RRC

DESCRIPTION
  This function handles confirmations sent to RRC from dual_sim/trm_lock perspective.
  It releases the TRM lock if it is no longer needed and resets flags.

DEPENDENCIES
  None

PARAMETERS
  ds_proc: Current dual sim procedure

RETURN VALUE
  TRUE - send conf to RRC.
  FALSE - don't send the conf to RRC yet.

SIDE EFFECTS
  None.
===========================================================================*/
extern boolean wl1_ds_hdle_cnf_sent_to_rrc(rrc_cmd_type *cnf_to_rrc_ptr);

/*===========================================================================
FUNCTION     WL1_DS_HDLE_LOCK_FAILURE_FOR_PAGING

DESCRIPTION
  This function is called when lock acquisition has failed for paging procedure.
  This function sends OOS indication to RRC if the consecutive paging lock
  acquisition failure count is greater than the preset threshold.

DEPENDENCIES
  None

PARAMETERS
  None

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1_ds_hdle_lock_failure_for_paging(void);


/*===========================================================================
FUNCTION     WL1_DS_CHK_N_RESET_PCH_OOS_CRIT

DESCRIPTION
  This function checks if OOS ind is about to be sent by DSDS module to RRC,
  and in that case resets the PCH OOS timer to avoid sending a second OOS
  indication.

DEPENDENCIES
  None

PARAMETERS
  None

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1_ds_chk_n_reset_pch_oos_crit(void);

/*===========================================================================
FUNCTION     WL1_DS_CHECK_FOR_OOS

DESCRIPTION
  This function sends OOS indication to RRC if the consecutive paging lock
  acquisition failure count is greater than the preset threshold.

DEPENDENCIES
  None

PARAMETERS
  None

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1_ds_check_for_oos(void);

/*===========================================================================
FUNCTION     WL1_DS_POST_CELL_TRANS_CNF_CB

DESCRIPTION
  This function is called at the end of cell transition. It resets any paging
  related statistics.

DEPENDENCIES
  None

PARAMETERS
  status: the status of cell transition

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1_ds_post_cell_trans_cnf_cb(boolean status);

/*===========================================================================
FUNCTION     WL1_DS_PRE_CELL_TRANS_REQ_CB

DESCRIPTION
  This function is called upon receiving the cell transition command.
  It sets the status based on the cell reselection status from RRC

DEPENDENCIES
  None

PARAMETERS
  status: the status of cell transition
  Reselection type: L1_ASET_UPD_CELL_RESELECTION_FAILURE - Treated as Failure
  Any other reselection type is treared as Success

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1_ds_pre_cell_trans_req_cb(boolean status);

/*===========================================================================
FUNCTION     WL1_DS_HDLE_LOCK_ACQ_FAIL

DESCRIPTION
  This function handles TRM lock acquistion failure for various procedures.
  It sends failure confirmation to RRC and increments any counters to track
  failure.

DEPENDENCIES
  None.

PARAMETERS
  cmd_ptr: Command pointer associated with the command from upper layers
  ds_proc: Current dual sim procedure

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1_ds_hdle_lock_acq_fail(wl1_ds_proc_enum_type ds_proc);

/*===========================================================================
FUNCTION     WL1_DS_PREP_AFTER_LOCK_ACQ

DESCRIPTION
  This function is called when lock acquisition is successful for a procedure
  and WL1 needs to be prepare mdsp/rf. This does not handle lock acquisition
  case for Paging wakeups.

DEPENDENCIES
  None

PARAMETERS
  ds_proc: the procedure for which WL1 needs to be prepared

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1_ds_prep_after_lock_acq(wl1_ds_proc_enum_type ds_proc);

/*===========================================================================
FUNCTION     WL1_IS_DS_IN_PREP_MODE

DESCRIPTION
  This function returns whether WL1 is in the middle of preparing
  mdsp/RF.

DEPENDENCIES
  None

PARAMETERS
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
extern boolean wl1_is_ds_in_prep_mode(void);

/*===========================================================================
FUNCTION     WL1_DS_ENTER_SLEEP

DESCRIPTION
  This function must be called when WL1 is about to start sleeping. This function must be called
  from SLEEP_START_INT.

DEPENDENCIES
  None

PARAMETERS
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1_ds_enter_sleep(void);

/*===========================================================================
FUNCTION     WL1_DS_EXIT_SLEEP

DESCRIPTION
  This function conducts the dual sim procedure needed when WL1 exits sleep.
  This function must be called from micro wakeup ISR context.

DEPENDENCIES
  None

PARAMETERS
  None.

RETURN VALUE
  TRUE: Lock acquistion for paging is successful
  FALSE: Lock acquistion for paging failed

SIDE EFFECTS
  None.
===========================================================================*/
extern boolean wl1_ds_exit_sleep(void);

/*===========================================================================
FUNCTION     WL1_DS_HDLE_CELL_RESEL_START

DESCRIPTION
  This function must be called at the start of cell reselection. This function will ensure that WL1 holds
  the TRM lock during the entire duration of cell reselection.

DEPENDENCIES
  None

PARAMETERS
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1_ds_hdle_cell_resel_start(void);

/*===========================================================================
FUNCTION     WL1_DS_PRE_MEAS_START_IND

DESCRIPTION
 This function requests for TRM lock extension for a specified duration of
 trm_duration_t slpclk units

DEPENDENCIES
  This function must be called before srchcr starts any measurements in PCH_SLEEP state.

PARAMETERS
  duration - time in slpclks for which lock is requested.

RETURN VALUE
  True - if lock extension succeeded
  False - otherwise

SIDE EFFECTS
  None.
===========================================================================*/
extern boolean wl1_ds_pre_meas_start_ind(trm_duration_t duration);

/*===========================================================================
FUNCTION     WL1_DS_SET_PAGING_LOCK_STATUS

DESCRIPTION
  This function sets the paging lock status with the passed value.

DEPENDENCIES
  This function must be called at the earliest after wakeup.

PARAMETERS
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1_ds_set_paging_lock_status(boolean lock_status);

/*===========================================================================
FUNCTION     WL1_DS_HDLE_STATUS_CHG_IND

DESCRIPTION
  This function processes the Dual SIM status Indication from RRC. If UE is in
  dual sim mode, then WL1-RRC interface and sleep/wakeup procedures are
  impacted. This includes extra time needed for wakeup, mdsp/RF preparation
  for every wakeup and checking for TRM lock to process RRC request/indication.
  If UE is in single sim mode, then WL1 does not need TRM lock for every
  procedure and wakeup time can be reduced accordingly.

DEPENDENCIES
  WL1 can accept dual sim status indication only in do-nothing states.

PARAMETERS
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1_ds_hdle_status_chg_ind(l1_ds_status_change_ind_type *ds_cmd_ptr);

extern void wl1_ds_hdle_sig(rex_sigs_type ds_sig);

extern void wl1_ds_proc_pich_done(void);

/*===========================================================================
FUNCTION     WL1_DS_HDLE_BPLMN_START

DESCRIPTION
  This function handles the start of BPLMN search by changing the current
  DS procedure mask from Paging to BPLMN

DEPENDENCIES
  None

PARAMETERS
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1_ds_hdle_bplmn_start(void);

/*===========================================================================
FUNCTION     WL1_DS_HDLE_BPLMN_STOP

DESCRIPTION
  This function handles the stop of BPLMN search by changing the current
  DS procedure mask from BPLMN to Paging

DEPENDENCIES
  None

PARAMETERS
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1_ds_hdle_bplmn_stop(void);

/*===========================================================================
FUNCTION     WL1_DS_IND_DRX_CMD

DESCRIPTION
  This function handles the sets/resets any variable in wl1_ds upon the reception
  of a drx command from RRC.

DEPENDENCIES
  None

PARAMETERS
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1_ds_ind_drx_cmd(void);

/*===========================================================================
FUNCTION     WL1_DS_OK_TO_PROC_EXT_CMD_WITHOUT_LOCK

DESCRIPTION
  This function says whether an external command can be processed without
  TRM lock and mdsp/RF in pch_sleep state.

DEPENDENCIES
  None

PARAMETERS
  None.

RETURN VALUE
  TRUE: if external command can be processed without TRM lock
  FALSE: if external command cannot be processed without TRM lock

SIDE EFFECTS
  None.
===========================================================================*/
extern boolean wl1_ds_ok_to_proc_ext_cmd_in_sleep(l1_ext_cmd_type *ext_cmd_ptr);

/*===========================================================================
FUNCTION     WL1_DS_L1M_WOKEN_UP

DESCRIPTION
  This function clears the WL1 in paging wakeup flag as L1M is moving to PCH state.

DEPENDENCIES
  None

PARAMETERS
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1_ds_l1m_woken_up(void);

/*===========================================================================
FUNCTION enchs_override_cqi

DESCRIPTION
  This function sets a flag as well as the CQI override value to the MDSP. 
  mDSP reads the flag at the R99 frame boundary and if set, overrides the 
  CQI with the value written by L1.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
extern void enchs_override_cqi(boolean flag, uint8 val);

#if defined(FEATURE_DUAL_SIM) && defined(FEATURE_QTA)

/*===========================================================================
FUNCTION     WRM_IS_NEXT_TUNEAWAY_PAGE_DECODE

DESCRIPTION
  This function is called by RRC to determine whether the next W2G tuneaway
  is PAGE Decode by GSM Sub or not.  It calls into TRM to check GSM's intentions
  for the next tuneaway.  If GSM's intentions are long, like cell reselection, 
  then return FALSE.  If GSM's intentions are short, like only check paging 
  occasion, then do quick tuneaway.

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  TRUE  - Next Tuneaway is short Tuneaway.
  FALSE - Next Tuneaway could be long Tuneaway.
===========================================================================*/
extern boolean wrm_is_next_tuneaway_page_decode(void);

/*===========================================================================
FUNCTION     WRM_IS_NEXT_TUNEAWAY_QTA

DESCRIPTION
  This function is called by RRC to determine whether the next W2G tuneaway
  should be legacy variety or quick variety (QTA).  It calls into TRM to
  check GSM's intentions for the next tuneaway.  If GSM's intentions are long,
  like cell reselection, then do legacy tuneaway.  If GSM's intentions are
  short, like only check paging occasion, then do quick tuneaway.

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  TRUE  - Use QTA for next tuneaway.
  FALSE - Use legacy for next tuneaway.
===========================================================================*/
extern boolean wrm_is_next_tuneaway_qta(void);

/*===========================================================================
FUNCTION     WL1_QTA_TIMER_EXPIRED_CMD

DESCRIPTION
  wl1_tuneaway_timer expired during quick tuneaway procedure, which has
  different steps and different times between steps.  This is local command
  handler invoked for WL1_QTA_TIMER_EXPIRED_CMD.

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  None.
===========================================================================*/
extern void wl1_qta_timer_expired_cmd(void);

/*===========================================================================
FUNCTION     WL1_QTA_UNLOCK_CANCEL_CMD

DESCRIPTION
  This is local command handler is invoked for WL1_QTA_UNLOCK_CANCEL_CMD.


DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  None.
===========================================================================*/
extern void wl1_qta_unlock_cancel_cmd(void);

/*===========================================================================
FUNCTION     L1M_CPHY_START_QTA_REQ_CMD

DESCRIPTION
  This function is an indication from RRC that we are to start W2G quick
  tuneaway process.

DEPENDENCIES
  None.

PARAMETERS
  l1_start_qta_cmd_type *start_qta_cmd - Unused.

RETURN VALUE
  None.
===========================================================================*/
extern void l1m_cphy_start_qta_req_cmd(l1_start_qta_cmd_type *start_qta_cmd);

/*===========================================================================
FUNCTION     L1M_CPHY_STOP_QTA_REQ_CMD

DESCRIPTION
  This function is an indication from RRC that we are to end W2G quick
  tuneaway process, meaning resume WCDMA mode.

DEPENDENCIES
  None.

PARAMETERS
  l1_stop_qta_cmd_type *stop_qta_cmd - Unused.

RETURN VALUE
  None.
===========================================================================*/
extern void l1m_cphy_stop_qta_req_cmd(l1_stop_qta_cmd_type *stop_qta_cmd);

/*===========================================================================
FUNCTION     WL1_QTA_START_QTA

DESCRIPTION
  This function is invoked by QXDM debug command to simulate getting start
  QTA command from RRC.

  This is what QXDM command looks like to simulate QTA from RRC:

  send_data 75 4 47 0 0x1E 0x00

  The last 2 values define the QTA duration in msec.  For example, the above
  sets QTA duration of 30 msec = 0x001E.  Swap the bytes of the hex value to
  use and enter on the command line separated by spaces.

DEPENDENCIES
  None.

PARAMETERS
  uint16 qta_duration_msec - How long simulated QTA should last, i.e. when
                             to send stop QTA.
RETURN VALUE
  None.
===========================================================================*/
extern void wl1_qta_start_qta(uint16 qta_duration_msec);

/*===========================================================================
FUNCTION     WL1_QTA_COMMAND_DONE_ISR

DESCRIPTION
  This function handles the QTA START and STOP done indicatores. from WFW

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  None.
===========================================================================*/
extern void wl1_qta_command_done_isr(void);

/*=============================================================================

FUNCTION  eq_drop_eq_for_qta

DESCRIPTION:

 QTA manager calls this function 12ms before QTA enabled all the time without 
 knowledge of either EQ enabled or not If EQ has already enabled EQ controller 
 sends EQ disable command to FW,Deregister the call back with triage,demod and 
 CM.If EQ is not enabled, no need to do anything.
 
DEPENDENCIES:

None

PARAMETERS:

None

RETURN VALUES:

None
=============================================================================*/
extern void  eq_drop_eq_for_qta(void);

/*=============================================================================
FUNCTION void  eq_enable_eq_after_qta
DESCRIPTION:

 QTA manager calls this function after QTA gap completed.EQ controller validates
 weather CM and TD are on or not.If both of them are off  EQ controller sends 
 EQ enable command to FW and register the call back functions with triage,demod
 and CM.otherwise it is in EQ disable state only.

 DEPENDENCIES:
 
 CM and TD both should be off.
 
 PARAMETERS:
 
 None

 RETURN VALUES:
 
 None
=============================================================================*/
extern void  eq_enable_eq_after_qta(void);

/*===========================================================================

FUNCTION     WL1_IS_QTA_ACTIVE

DESCRIPTION  This function checks if QTA is active or not...

DEPENDENCIES
  None.

RETURN VALUE  Boolean.

SIDE EFFECTS
  None.
===========================================================================*/
extern boolean wl1_is_qta_active(void);

/*===========================================================================

FUNCTION  WL1_IN_QTA_GAP

DESCRIPTION
  This function checks if QTA is active or not...

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
extern boolean wl1_in_qta_gap(void);

/*===========================================================================

FUNCTION  WL1_IS_WFW_IN_QTA_GAP

DESCRIPTION
  This function checks if WFW is in QTA or  not...

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
extern boolean wl1_is_wfw_in_qta_gap(void);

/*===========================================================================

FUNCTION  WL1_IN_QTA_CLEANUP

DESCRIPTION
  This function checks if QTA just ended and CRCs reported are valid.

DEPENDENCIES
  None.

RETURN VALUE
  True - if QTA just ended and CRCs reported are not valid.
  else False.

SIDE EFFECTS
  None.
===========================================================================*/
extern boolean wl1_in_qta_cleanup(void);
/*===========================================================================

FUNCTION  WL1_IN_QTA_CLEANUP_BCH

DESCRIPTION
  This function checks if QTA just ended and BCH CRCs can be processed.

DEPENDENCIES
  None.

RETURN VALUE
  True - if QTA ended in the last 20 ms and BCH CRC reports are not to be 
  processed.
  else False.

SIDE EFFECTS
  None.
===========================================================================*/
extern boolean wl1_in_qta_cleanup_for_bch(void);

/*===========================================================================

FUNCTION  WL1_IN_FRAME_AFTER_QTA

DESCRIPTION
  This function checks if this is the first frame after QTA.

DEPENDENCIES
  None.

RETURN VALUE
  True - if QTA ended in the previous frame.
  else False.

SIDE EFFECTS
  None.
===========================================================================*/
extern boolean wl1_in_frame_after_qta(void);
/*===========================================================================

FUNCTION  WL1_PREPARING_FOR_QTA

DESCRIPTION
  This function checks if there is a QTA preparation going or not..

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
extern boolean wl1_preparing_for_qta(void);

/*===========================================================================

FUNCTION  WL1_QTA_CLEANUP

DESCRIPTION
  This function cleanup QTA related timers and variables if set.

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1_qta_cleanup(void);

/*===========================================================================

FUNCTION  l1m_gsm_rf_nv_init_wait_handler

DESCRIPTION
  This function is callbacek function used for GSM RF NV init to wait
  on the NV init done signal.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
extern void l1m_gsm_rf_nv_init_wait_handler(rex_sigs_type sig);

/*===========================================================================

FUNCTION SRCHDCH_FREEZE_SRCH

DESCRIPTION 
    This function is used to freeze the search HB event and make note of the 
    time stamp which is used later to evaluate the elapsed time while resuming.

DEPENDENCIES
  
RETURN VALUE
    None.

SIDE EFFECTS
    Ignore the HB event, so srch_ops_ctl_parms count will not be updated 
    until resumed.
===========================================================================*/
extern void srchdch_freeze_srch(void);

/*===========================================================================

FUNCTION SRCHDCH_UNFREEZE_SRCH

DESCRIPTION 
    This function is used to unfreeze the suspended HB event,calculate the elasped 
    time and increment the srch_ops_ctl_parms like RSSI,ASET SEARCH etc. 
    accordingly.

DEPENDENCIES
  
RETURN VALUE
    None.

SIDE EFFECTS
   The value of srch_ops_ctl_parms will be update according to the elapsed time.
===========================================================================*/
extern void srchdch_unfreeze_srch(void);

/*===========================================================================

FUNCTION     wl1_waitfor_mdsp_download_complete

DESCRIPTION
  This function will wait for mdsp download completion on a TIMEOUT sig.

DEPENDENCIES
  None.

RETURN VALUE
  Boolean.

SIDE EFFECTS
  None.

===========================================================================*/
extern void wl1_waitfor_mdsp_download_complete(void);

/*===========================================================================
FUNCTION     wl1_query_qta_activity

DESCRIPTION
  WL1 subsystems call this function to determine the state of Quick Tuneaway
  (QTA).  QTA is a mode where WCDMA has frozen its state for a brief period,
  ~30 msec, to allow GSM to check its paging occasion.  So during QTA, WL1
  subsystems should forgo significant activities, like mDSP accesses, RF
  accesses, etc., but continue whatever processing will allow them to start
  back up again quickly in the future.

  WL1 subsystems must call this function periodically in order to determine
  current and future QTA status.  And once QTA goes active, WL1 subsystems
  must call this function periodically to determine when QTA has ended.
  At that point, WL1 subsystems should resume normal processing.

DEPENDENCIES
  None.

PARAMETERS
  uint16 *next_qta_cfn  - Return CFN of future QTA gap to caller.  Set to
                          MAX_CFN_COUNT if no known QTA gap pending.
  uint8  *next_qta_slot - Return slot of future QTA gap.  Value undefined
                          if no known QTA gap pending.
RETURN VALUE
  TRUE  - WL1 is currently in the middle of a QTA gap.
  FALSE - WL1 is currently NOT in the middle of a QTA gap.
===========================================================================*/
extern boolean wl1_query_qta_activity(uint16 *, uint16 *);
                               
/*===========================================================================

FUNCTION  WL1_QTA_CLEANUP_INIT

DESCRIPTION
  This function initializes the variables needed for QTA cleanup.

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1_qta_cleanup_init(void);

/*===========================================================================

FUNCTION  WL1_QTA_CLEANUP_RESET

DESCRIPTION
  This function resets the variables used for QTA cleanup.

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1_qta_cleanup_reset(void);
/*===========================================================================
FUNCTION     WL1_QTA_SKIP_OLPC

DESCRIPTION

  This fucntion is called every frame to check if OLPC need to skipped in QTA gap

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  Boolean value
     TRUE: SKIP olpc in current frame
     FALSE: Don't skip olpc in current frame
===========================================================================*/
extern boolean wl1_qta_skip_olpc(void);

/*===========================================================================
FUNCTION     WL1_QTA_UPDATE_FRAME_CNT_FOR_CLEANUP

DESCRIPTION
  This function updates frame count variables after QTA stop 
  to know if we are in max TTI gap after QTA

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  None.
===========================================================================*/
extern void wl1_qta_update_frame_cnt_for_cleanup(void);

/*===========================================================================
FUNCTION     WL1_QTA_SET_GL1_RF_INFO

DESCRIPTION
  WL1_QTA_WFW_START_QTA
  Gets the free rf/rxlm buffer info from IRAT before start QTA and passes it GERAN

  WL1_QTA_WFW_STOP_QTA
  Call GERAN api to cleanup the buffer

DEPENDENCIES
  None.

PARAMETERS
  qta_flag

RETURN VALUE
  None.
===========================================================================*/

extern void wl1_qta_set_gl1_rf_info(uint16 qta_flag);

#endif  /* #ifdef FEATURE_QTA */

/*===========================================================================
FUNCTION     WL1_DS_TRM_RESERVE_OPS_GTS_IND

DESCRIPTION
  This function must be called when WL1 receives a GO TO SLEEP IND from RRC. 
  It must be called from l1m_process_ext_cmd() only.

DEPENDENCIES
  None

PARAMETERS
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1_ds_trm_reserve_ops_gts_ind(void);

/*===========================================================================
FUNCTION     WL1_QTA_SET_DL_TDDET_PENDING

DESCRIPTION
  This function sets the flag that indicates a call to dl_cell_check_tddet_status
  is pending, and should be completed after the QTA gap is closed.

DEPENDENCIES
  None

PARAMETERS
  boolean flag - Sets the internal flag to the value provided.

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1_qta_set_dl_tddet_pending(boolean flag);

/*===========================================================================
FUNCTION     WL1TRM_TUNEAWAY_LOG_PKT_SUBMIT

DESCRIPTION
  This function submits the wl1trm_tuneaway_log_pkt log packet after it is
  allocated memory.

DEPENDENCIES
  Sends the log packet only if it can successfully allocate memory.

PARAMETERS 
  wl1trm_tuneaway_log_data_struct_type *log_data - pointer to the log data
  that needs to be sent out.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1trm_tuneaway_log_pkt_submit(wl1trm_tuneaway_log_data_struct_type *log_data);

/*===========================================================================
FUNCTION     WL1TRM_TUNEAWAY_LOG_SET_TIME

DESCRIPTION
  This function provides a way to store the suspend start, suspend stop, resume
  start and the setup confirmation timeticks into the wl1trm_tuneaway_log_packet's
  data structure.

DEPENDENCIES
  None.
 
PARAMETERS 
  wl1trm_tuneaway_log_set_time_type time_type - Flag to indicate the event.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1trm_tuneaway_log_set_time(wl1trm_tuneaway_log_set_time_type time_type);

/*===========================================================================
FUNCTION     WL1TRM_TUNEAWAY_LOG_SET_L1M_STATE

DESCRIPTION
  This function stores the state of L1M in the wl1trm_tuneaway_log_packet's
  data structure.

DEPENDENCIES
  None.

PARAMETERS 
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1trm_tuneaway_log_set_l1m_state(void);

/*===========================================================================
FUNCTION     WL1TRM_TUNEAWAY_LOG_UPDATE_FACH_QTA_REASON_BITMASK

DESCRIPTION
  This function updates the search reason bitmask with the search that was cancelled
  due to impending QTA or search which cancels the QTA.

DEPENDENCIES
  None.

PARAMETERS 
  uint8 add_new_reason - The active or blocked client that needs to be added to the
  tuneaway log packet.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1trm_tuneaway_log_update_fach_qta_reason_bitmask(uint8 add_new_reason);

/*===========================================================================
FUNCTION     wl1_ds_hdle_cell_decfg_done_cb

DESCRIPTION
  This is  a callback function after the cell de registration is done.

DEPENDENCIES
  None

PARAMETERS
  None  
  
RETURN VALUE  
  None

SIDE EFFECTS
  None
===========================================================================*/
extern void wl1_ds_hdle_cell_decfg_done_cb(void);

#endif  /* #ifdef FEATURE_DUAL_SIM */

#if defined(FEATURE_DUAL_SIM) && defined(FEATURE_DUAL_SIM_WCDMA_TUNEAWAY)

/*===========================================================================
FUNCTION     wl1_read_w2g_tuneaway_config_from_nv

DESCRIPTION
  This function reads W2G tuneaway configuration items from EFS/NV one time
  at WCDMA start-up.

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  None.
===========================================================================*/
extern void wl1_read_w2g_tuneaway_config_from_nv(void);

/*===========================================================================
FUNCTION     wl1_cqi_zero_timeout_cmd

DESCRIPTION
  Local command handler for L1_CQI_ZERO_TIMEOUT_CMD.  If RRC never suspends
  WL1 for tuneaway for some reason, we want to reset CQI from zero back to
  normal.  In theory this timeout should never happen, but handle just in case.

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  None.
===========================================================================*/
extern void wl1_cqi_zero_timeout_cmd(void);

/*===========================================================================
FUNCTION     wl1_start_cqi_zero_cmd

DESCRIPTION
  Local command handler for L1_START_CQI_ZERO_CMD.  It is time to override
  CQI to zero in preparation for inter-RAT tuneaway in the near future.

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  None.
===========================================================================*/
extern void wl1_start_cqi_zero_cmd(void);

/*===========================================================================
FUNCTION     wl1_prepare_for_tuneaway

DESCRIPTION
  This function is called to tell WL1 to prepare for a future inter-RAT
  tuneaway, and to ask WL1 for permission to perform that future tuneaway.

DEPENDENCIES
  None.

PARAMETERS
  uint32 unlock_by_sclk - Future sleep clock timetick value when RRC intends
                          to SUSPEND WL1 and start tuneaway.
RETURN VALUE
  boolean - TRUE if tuneaway request approved.  FALSE if not.
===========================================================================*/
extern boolean wl1_prepare_for_tuneaway(uint32 unlock_by_sclk);

/*===========================================================================
FUNCTION     wl1_return_tuneaway_duration

DESCRIPTION
  This function saves the duration of tuneaway filled by RRC.
  RRC fills the duration into the global tuneaway_duration but might reset it if they start another 
  Suspend WCDMA procedure. So save the value into internal L1 variable and keep.

DEPENDENCIES
  None.

PARAMETERS

RETURN VALUE
  None.
===========================================================================*/
extern void wl1_read_tuneaway_duration_from_rrc(void);
/*===========================================================================
FUNCTION     wl1_return_tuneaway_duration

DESCRIPTION
  This function returns duration of tuneaway, in frames to the caller.

DEPENDENCIES
  None.

PARAMETERS

RETURN VALUE
Tuneaway duration, in frames.
===========================================================================*/
extern uint16 wl1_return_tuneaway_duration(void);

/*===========================================================================
FUNCTION     wl1_reset_tuneaway_duration

DESCRIPTION
  This function resets duration of tuneaway to zero.

DEPENDENCIES
  None.

PARAMETERS

RETURN VALUE
  None.
===========================================================================*/
extern void wl1_reset_tuneaway_duration(void);
/*===========================================================================
FUNCTION  RRC_IMMEDIATE_PREEMPTION_CB

DESCRIPTION

  This is a call back function to be called in case of pre-emption.
    
DEPENDENCIES

  None.
 
RETURN VALUE

  None.

SIDE EFFECTS

  None.
===========================================================================*/
extern boolean rrc_immediate_preemption_cb 
(
  /* The client which is being informed of an event */
  trm_client_enum_t               client,

  /* The event being sent to the client */
  trm_unlock_event_enum_t         event,

  /* Sclk timestamp for TRM_UNLOCK_BY */
  uint32                          unlock_by_sclk
);

#endif  /* #if defined(FEATURE_DUAL_SIM) && defined(FEATURE_DUAL_SIM_WCDMA_TUNEAWAY) */

#ifdef FEATURE_DUAL_SIM
/*===========================================================================
FUNCTION     wrm_determine_g2w_ta_mode_active

DESCRIPTION
  This function determines if a g2w tuneaway is active

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  boolean - FALSE if G2W TA is not active, or TRUE if active

SIDE EFFECTS
  None.
===========================================================================*/
extern boolean wrm_determine_g2w_ta_mode_active(void);

/*===========================================================================
FUNCTION  WRM_G2W_LTA_PROCESS_EXT_MCD

DESCRIPTION
 Performs categorization of external cmds that are sent to WL1

DEPENDENCIES
  To be used only in G2W TA mode.

RETURN VALUE
  Category type

SIDE EFFECTS
  None.
===========================================================================*/
extern wrm_g2w_ta_mode_ext_cmd_category_enum_type wrm_g2w_lta_process_ext_cmd(l1_ext_cmd_type *ext_cmd_ptr);

/*===========================================================================
FUNCTION     WRM_G2W_TA_RESERVE_TRM_LOCK

DESCRIPTION
  This function is used by WCDMA Idle scheduler to reserve TRM lock in G2W TA
  mode.

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
extern void wrm_g2w_ta_reserve_trm_lock(uint32 trm_reserve_sclk);

/*===========================================================================
FUNCTION     WRM_SWITCH_REG_MODE_G2W_TA_MODE_PROC

DESCRIPTION
  This function is used by WCDMA to switch WRM from regular DSDS mode to 
  G2W TA mode.
DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
extern void wrm_switch_reg_mode_g2w_ta_mode_proc(void);

/*===========================================================================
FUNCTION     WRM_SWITCH_G2W_TA_REG_MODE_PROC

DESCRIPTION
  This function is used by WCDMA to switch WRM from G2W TA mode to default
  DSDS mode

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
extern void wrm_switch_g2w_ta_reg_mode_proc(void);

/*===========================================================================
FUNCTION     WRM_G2W_TA_REQUEST_TRM_LOCK

DESCRIPTION
  This function is used by WCDMA Idle mode scheduler to request TRM lock in 
  G2W TA mode.

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
extern boolean wrm_g2w_ta_request_trm_lock(void);

/*===========================================================================
FUNCTION     WRM_CLEAR_G2W_TA_PROC

DESCRIPTION
  This function is used by WCDMA to clear G2W TA WRM PROC

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
extern void wrm_clear_g2w_ta_proc(void);

/*===========================================================================
FUNCTION     WRM_G2W_TA_PRIO_INV_TMR_EXPIRED_CB

DESCRIPTION 
  This will do a req_notify with actual ACQ priority after priority
  inversion timer expires. WRM modifies the priority with which TRM
  lock is requested in G2W TA mode. After priority inversion timer
  expires WRM will do a req_notify with actual TRM priority if we
  are in good state.

DEPENDENCIES
  None.

PARAMETERS 
  uint32 unused.

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
extern void wrm_g2w_ta_prio_inv_tmr_expired_cb(uint32 unused);

/*===========================================================================
FUNCTION     WRM_RELEASE_SPLIT_ACQ

DESCRIPTION
  This function is used by WCDMA to release lock in split acq mode.

DEPENDENCIES
  None.

PARAMETERS
  wrm_client_enum_type wrm_client_id     -- WRM client id,
  wrm_lock_release_type wrm_lock_release_rsn  -- release mode after which lock is getting released.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
extern void wrm_release_split_acq(wrm_client_enum_type wrm_client_id,wrm_lock_release_type wrm_lock_release_rsn);

/*===========================================================================
FUNCTION     WRM_REQUEST_AND_NOTIFY_FOR_SPLIT_ACQ

DESCRIPTION
  This function is used by WCDMA to request lock for split acq

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
extern void wrm_request_and_notify_for_split_acq(
  /* The client which needs the RF resource */
  wrm_client_enum_type wrm_client_id,

  /* How long the resource will be needed for (in sclks) */
  trm_duration_t                  duration, // change to milliseconds 

  /* Callback to notify client when resource is granted */
  wrm_srch_lock_grant_cb_type            grant_callback
);

void wrm_relinquish_alt_client_lock_for_split_acq
(
  /* The client which is attempting to gain complete control of TRM lock */
  wrm_client_enum_type wrm_client_id
);

/*===========================================================================
FUNCTION     WRM_G2W_TA_RELEASE

DESCRIPTION
  This function is used by WL1 idle mode scheduler to release lock for G2W TA

DEPENDENCIES
  None.

PARAMETERS
  wrm_client_enum_type wrm_client_id  -- WRM client whose lock should be released.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
extern void wrm_g2w_ta_release(wrm_client_enum_type wrm_client_id);

/*===========================================================================
FUNCTION     WRM_COMPLETE_SPLIT_ACQ_OFFLINE_NASTT_OPS

DESCRIPTION
  This function is used by WL1 to notify WRM that offline NASTT in split acq mode
  is complete.

DEPENDENCIES
  None.

PARAMETERS
  None.
 
RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
extern void wrm_complete_split_acq_offline_nastt_ops();

/*===========================================================================
FUNCTION     WRM_G2W_TA_GET_TRM_RESERVE_TIME

DESCRIPTION
  This function returns the duration after which TRM is reserved in sclk
  if W is in G2W TA mode else returns a invalid sclk value.

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  uint32 duration in sclks

SIDE EFFECTS
  None.
===========================================================================*/
extern uint32 wrm_g2w_ta_get_trm_reserve_time(void);
#endif

/*===========================================================================
FUNCTION     WL1_TRM_GET_CURRENT_ALLOCATED_BAND

DESCRIPTION
  The Function 

DEPENDENCIES
  None.

PARAMETERS
  trm_client_enum_t      client - The client which is being informed of an event

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/

extern rfcom_wcdma_band_type wl1_trm_get_current_allocated_band(void);


/*===========================================================================
FUNCTION     WL1_TRM_GET_CARRIER_AGGREGATION_FREQ

DESCRIPTION
  This function returns the frequency of 2nd carrier in DBDC call

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
boolean to indicate if DBDC call or not

SIDE EFFECTS
  None.
===========================================================================*/
extern uint16
wl1_trm_get_carrier_aggregation_freq(void);

/*===========================================================================
FUNCTION     WL1_TRM_INIT

DESCRIPTION
  This function initializes TRM database
  
DEPENDENCIES
  None.

PARAMETERS
  NONE

RETURN VALUE
  NONE

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1_trm_init( void );

/*===========================================================================
FUNCTION  wl1_trm_secondary_chain_lock_available

DESCRIPTION
  API can be used externally to figure out whether WCDMA has the div chain granted by TRM 

DEPENDENCIES
  None.

RETURN VALUE
  TRUE- W has the TRM lock for div chain
  FALSE- W doesn't have the TRM lock for div chain
SIDE EFFECTS
  None.
===========================================================================*/
extern boolean wl1_trm_secondary_chain_lock_available(void);

/*===========================================================================
FUNCTION wl1_trm_query_trm_multisim_config_info

DESCRIPTION
  This function is to query trm about the multi-sim configuration. To print UE is configured for SS/DSDS/TSTS/DSDA

DEPENDENCIES
  Assumed to be called after HS has indicated to start logging to DECHS

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

extern wl1_trm_multi_sim_config_enum_type wl1_trm_query_trm_multisim_config_info(void);


/*===========================================================================
/*===========================================================================
FUNCTION     WL1_TRM_SECONDARY_UNLOCK_CALLBACK

DESCRIPTION
  TRM calls this function when TRM requests WL1 to release the secondary antenna.

DEPENDENCIES
  None.

PARAMETERS
  trm_client_enum_t      client - The client which is being informed of an event

  trm_grant_event_enum_t event  - The event being sent to the client
  
  trm_request_tag_t      tag    - Anonymous payload echoed from trm_freq_request_and_notify()

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1_trm_secondary_unlock_callback(trm_client_enum_t client, 
                             trm_unlock_event_enum_t event,
                             uint32 unlock_by_sclk);

#ifdef FEATURE_DUAL_SIM
/*===========================================================================
FUNCTION     WL1_TRM_REQ_SEC_CHAIN

DESCRIPTION
  This is a wrapper function that requests for secondary antenna

DEPENDENCIES
  None.

PARAMETERS
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1_trm_req_sec_chain(void);
#endif

/*===========================================================================
FUNCTION     WL1_TRM_SECONDARY_UNLOCK_INTERNAL_CALLBACK

DESCRIPTION
  Wl1 calls this function to release the secondary antenna and disable RxD.
  It is needed to FOFF and release RxD without triggering OMRD callbacks.
  The function calls release and does not register a grant callback.
  This internal unlock function can be called with the same parameters as 
  the original TRM Unlock callback.

DEPENDENCIES
  None.

PARAMETERS
  trm_client_enum_t      client - The client which is being informed of an event

  trm_grant_event_enum_t event  - The event being sent to the client
  
  trm_request_tag_t      tag    - Anonymous payload echoed from trm_request_and_notify()

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
extern void wl1_trm_secondary_unlock_internal_callback(trm_client_enum_t client, 
                             trm_unlock_event_enum_t event,
                             uint32 unlock_by_sclk);



/*===========================================================================
FUNCTION wl1_qta_rota_create_nv

DESCRIPTION
  Creates nv to read the store whether TX ia allowed or not.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

extern void wl1_qta_rota_create_nv(void);

/*===========================================================================
FUNCTION wl1_qta_rota_read_nv

DESCRIPTION
  Reads nv to find whether TX ia allowed or not.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

extern void wl1_qta_rota_read_nv(void);

/*===========================================================================
FUNCTION wl1_qta_rota_nv_read

DESCRIPTION
  Creates nv and reads it to find whether TX ia allowed or not.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

extern void wl1_qta_rota_nv_read(void);

/*==========================================================================

FUNCTION     wl1_query_qta_start

DESCRIPTION
  This function is written to be used by RxD module to freeze RxD before the
  start of QTA. 

  WL1 subsystems call this function to determine the state of Quick Tuneaway
  (QTA).  QTA is a mode where WCDMA has frozen its state for a brief period,
  ~30 msec, to allow GSM to check its paging occasion.  So during QTA, WL1
  subsystems should forgo significant activities, like mDSP accesses, RF
  accesses, etc., but continue whatever processing will allow them to start
  back up again quickly in the future.

  WL1 subsystems must call this function periodically in order to determine
  current and future QTA status.  And once QTA goes active, WL1 subsystems
  must call this function periodically to determine when QTA has ended.
  At that point, WL1 subsystems should resume normal processing.

DEPENDENCIES
  None.

PARAMETERS
  uint16 *next_qta_cfn  - Return CFN of future QTA gap to caller.  Set to
                          MAX_CFN_COUNT if no known QTA gap pending.
  uint16 *next_qta_slot - Return slot of future QTA gap.  Value undefined
                          if no known QTA gap pending.
RETURN VALUE
 None.
===========================================================================*/
extern void wl1_query_qta_start(uint16 *next_qta_cfn, 
                                      uint16 *next_qta_slot);

/*===========================================================================
FUNCTION     WL1_DS_HANDLE_TRM_PENDING_SIG

DESCRIPTION
  This function must be called to handle the lock_acq_sig which was
  pended upon getting a TRM grant when WL1 is in paging wakeup.

DEPENDENCIES
  None

PARAMETERS
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
void wl1_ds_handle_trm_pending_sig(void);

/*=========================================================================
FUNCTION    WL1_DS_FAIL_RESUME_ON_LONG_TUNEAWAY

DESCRIPTION
    This function is called upon resume to check if the tuneaway duration is 
    1. Greater than OOS duration in FACH state
    2. DCH state is not handled currently here as RRC will trigger RLF after 
       ACQ during RESUME if T313+N313 out-of-sync's occured

    For now we support only CELL_FACH state and this function will indicate 
    to fail resume if #1 above is TRUE

PARAMETERS
 
DEPENDENCIES
  None

RETURN VALUE
  Boolean
  True -- Tuneaway duration > OOS duration (CELL_FACH state)
  False -- Otherwise
=========================================================================*/
extern boolean wl1_ds_fail_resume_on_long_tuneaway(void);

#endif /* WL1TRM_H */
