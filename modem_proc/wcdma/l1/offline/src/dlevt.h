#ifndef DL_EVENT_H
#define DL_EVENT_H

/*===========================================================================
                             D L _ E V T . H
                             
GENERAL DESCRIPTION
  This contains prototypes for downlink controller sequencer event handlers.
  Actually, it just #includes the two sequencer event handler header files for
  the decoder and demodulator modules.                             

EXTERNALIZED FUNCTIONS


INTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2000, 2001 by Qualcomm Technologies, Inc.  All Rights Reserved.

===========================================================================*/


/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$PVCSPath:  L:/src/asw/MSM5200/l1/vcs/dlevt.h_v   1.1   12 Jul 2001 13:36:10   mlevin  $
$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/wcdma/l1/offline/src/dlevt.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when        who     what, where, why
--------    ---     --------------------------------------------------------
                    
===========================================================================*/
#include "dldecevt.h"
#include "dldemevt.h"


#endif

