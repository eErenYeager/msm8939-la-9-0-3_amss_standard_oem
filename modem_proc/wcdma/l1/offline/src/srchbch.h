#ifndef SRCHBCH_H
#define SRCHBCH_H
/*===========================================================================

   S R C H    B C H    S T A T E   R E L A T E D   D E C L A R A T I O N S

DESCRIPTION

EXTERNALIZED FUNCTIONS


INTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS


REFERENCES
  CDMA Mobile Station ASIC Specification Sheet

Copyright (c) 2001 - 2013 by QUALCOMM Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$PVCSPath:  L:/src/asw/MSM5200/l1/vcs/srchbch.h_v   1.1   11 Jul 2002 21:28:06   halbhavi  $
$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/wcdma/l1/offline/src/srchbch.h#3 $ $DateTime: 2016/02/15 04:17:25 $ $Author: skkota $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
07/01/15   skk     Lock failure handling for cell selection. 
12/2/14    vn      Added macro checks whether cell selection req is received.
06/30/15   amj     Clean up old ASET update code.
09/12/13   jd      Searcher redesign
02/20/13   hk      Removed FEATURE_INTER_SIB_SLEEP
05/03/10   sv      Fixed compiler warnings.
02/06/10   sv      Mainlined feature FEATURE_WCDMA_OFFLINE_SEARCHER.
05/08/09   sup     Changing the includes for the renaming of header files  dec.h, enci.h, l1m.h, srch.h, srchi.h, 
                   srchlog.h to wdec.h, wenci.h, wl1m_v.h, wsrch.h, wsrchi.h, wsrchlog.h
04/29/09   sq      Removed compiler warnings
04/22/09   stk     Compiler warning fixes	
05/26/06   kps     Added function prototype get_rx_agc_cache() to return a cached 
                   value of the Rx AGC.
02/13/06   ub      Added function prototype for evaluating S criteria of target
                   inter ngbr cell - srchbch_cell_select_eval_inter()
11/24/05  nd/gv    Modified prototype of srchbch_cell_select_eval() to use it
                   for W to W BPLMN search
08/30/05   sh      Added proto srchbch_cell_select_eval().
04/05/05   gv      Mainlined the feature FEATURE_L1_CELL_SELECTION and 
                   FEATURE_L1_CELL_RESELECTION.Also added a macro for
                   Cell Selection time out.
07/11/02   sh      Added srchbch_declare_reacq_status().
04/29/02   sh      Added srchbch substates for cell reselection.
03/27/02   sh      Created file. Moved declarations from srchbch.c to here.
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "wcdma_variation.h"
#include "comdef.h"
#include "wsrchi.h"
#include "srchcmd.h"
#include "srchset.h"
#include "l1rrcif.h"
#include "wl1m.h"

/*===========================================================================

                        DATA DECLARATIONS

===========================================================================*/
/* Default minimum values for cell selection S values */
#define SRCH_SQUAL_DEFAULT_MIN (-32767)
#define SRCH_SRXLEV_DEFAULT_MIN (-32767)


#define CELL_SEL_TIMEOUT( )                                     \
  ( srchbch_cell_selection_list_search_cnt*SRCH_ASET_SRCH_PERIOD \
      >= SRCHBCH_CELL_SELECTION_TIMEOUT )

extern boolean srchbch_cphy_cell_selection_recvd;

/*Whether a cell selection request received or not */
#define WSRCH_IS_CELL_SELECTION_REQ_RECEIVED() (srchbch_cphy_cell_selection_recvd)

/*--------------------------------------------------------------------------
                          BCH SEARCH SUBSTATES

--------------------------------------------------------------------------*/
typedef enum
{
  SRCHBCH_INIT,           /* Initialization */
  SRCHBCH_INACTIVE,       /* No search activity */
  SRCHBCH_ASET,           /* ASET Search */
  SRCHBCH_ASET_CR,        /* ASET Search for cell reselection */
  SRCHBCH_NSET_STEP1,     /* NSET Step 1 search */
  SRCHBCH_NSET_NASTT,     /* NSET NASTT search */
  SRCHBCH_NSET_LIST,      /* NSET PN search   */
  SRCHBCH_STATE_MAX       /* Last item in enum. */
} srchbch_substate_enum_type;


extern srchbch_substate_enum_type  srchbch_substate;

extern boolean  srchbch_nset_info_rxed;

/*--------------------------------------------------------------------------
                         CELL SELECTION LIST STRUCT

 Cell list structure for Cell Selection evaluation.
--------------------------------------------------------------------------*/

typedef struct
{
  /* Cell Selection frequency requested by RRC */
  uint16   freq;

  /* Primary Scrambling Code */
  uint16   scr_code;

  /* Flag indicating if the cell is searched or not.
   * Currently, if the cell timing is unknown, the cell
   * will not be searched for cell selection evaluation
   */
  boolean  evaluated;

  /* Index (or pointer) to the corresponding cell table entry */
  srch_cell_struct_type *cell_ptr;

} srchbch_cell_select_list_type;

extern srchbch_cell_select_list_type  srchbch_cell_select_list;
/* Data structure to store cell list for cell selection */

/*===========================================================================

                      FUNCTION DECLARATIONS

===========================================================================*/

/*===========================================================================

FUNCTION SRCHBCH_PROC_HPQ_SRCH

DESCRIPTION
  This function processes search results from HPQ. 

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/
void srchbch_proc_hpq_srch( void );

/*===========================================================================

FUNCTION SRCHBCH_PROC_SYNC_NSET_SRCH

DESCRIPTION
  This function processes Sync NSET search results from LPQ. 

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/
void srchbch_proc_sync_nset_srch( void );

/*===========================================================================

FUNCTION SRCHBCH_NEXT_SRCH_CMD

DESCRIPTION
  This function first check if there is any step of neighbor search command
  to be issued following an ASET (HPQ) search done. if yes, the functions
  issues the search command to DSP. 

  The priority of neighbor searches is (from highest to lowest): Step1, 
  List, Step2/3. Search sequency will be something like: 
  
  ASET, Step1, ASET, STEP2, ASET, STEP3, ASET, NSET, ASET, STEP2, ASET,
  STEP3, ASET, .....

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/
void srchbch_next_srch_cmd (void);

/*===========================================================================

FUNCTION SRCHBCH_ASET_SRCH_DONE

DESCRIPTION
  This function processes HPQ list search Search dump during BCH. 

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/
void srchbch_aset_srch_done(srch_done_cmd_type *cmd_ptr);

/*===========================================================================

FUNCTION SRCHBCH_NSET_SRCH_DONE

DESCRIPTION
  This function processes LPQ neighbor list search Search dump during BCH. 

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/
void srchbch_nset_srch_done(srch_done_cmd_type *cmd_ptr);

/*===========================================================================

FUNCTION SRCHBCH_STEP1_DONE

DESCRIPTION
  This function processes step_1 Search dump during BCH. 

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/
void srchbch_step1_done(srchsched_step1_nastt_status_struct_type *step1_nastt_status);
void srchbch_nastt_done (srchsched_step1_nastt_status_struct_type* step1_nastt_status);

/*===========================================================================

FUNCTION SRCHBCH_STEP2_DONE

DESCRIPTION
  This function processes step_2 Search dump during BCH. 

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/
void srchbch_step2_done(srch_done_cmd_type *cmd_ptr);

/*===========================================================================

FUNCTION SRCHBCH_STEP3_DONE

DESCRIPTION
  This function processes step_3 Search dump during BCH. 

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/
void srchbch_step3_done (srch_done_cmd_type *cmd_ptr);

/*===========================================================================

FUNCTION SRCHBCH_CELL_SEL_RPT

DESCRIPTION
  This function sends a report CPHY_CELL_SELECTION_CNF to RRC on cell
  selection search results.
  
DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/
extern void srchbch_cell_sel_rpt (boolean lock_failure);

void srchbch_cell_select_next_srch (void);

/*===========================================================================

FUNCTION SRCHBCH_CELL_SELECT_SRCH_DONE

DESCRIPTION
  This function processes list search dump for cell selection in BCH state.
  This function retrieves the search results from the LPQ and computes
  the s_qual and s_rxlev Cell Selection criteria. It then calls a function
  to send the results to RRC.

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/
void srchbch_cell_select_srch_done(srch_done_cmd_type *cmd_ptr);

/*===========================================================================

FUNCTION SRCHBCH_CELL_SELECT_EVAL

DESCRIPTION
  This function evaluates S criteria of serving or target intra frequency 
  neighbor cell if reselection is in progress

DEPENDENCIES
  None.

RETURN VALUE
  TRUE, if S criteria passes, FALSE otherwise
  
SIDE EFFECTS
  None

===========================================================================*/
extern boolean srchbch_cell_select_eval( int16 q_qualmin, int16 q_rxlevmin, srch_cell_struct_type* cell_ptr, int16 max_tx_pwr);

/*===========================================================================

FUNCTION SRCHBCH_CELL_SELECT_EVAL_INTER

DESCRIPTION
  This function evaluates S criteria of target inter frequency neighbor cell

DEPENDENCIES
  None.

RETURN VALUE
  TRUE, if S criteria passes, FALSE otherwise
  
SIDE EFFECTS
  None

===========================================================================*/
extern boolean srchbch_cell_select_eval_inter( int16 q_qualmin, int16 q_rxlevmin, srch_interf_cell_struct_type* cell_ptr, int16 max_tx_pwr);
#endif /* SRCHBCH_H */
