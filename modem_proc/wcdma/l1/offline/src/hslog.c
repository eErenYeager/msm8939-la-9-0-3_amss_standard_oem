/*===========================================================================
                 WCDMA L1 HSPA Log

GENERAL DESCRIPTION
  This file contains the code for handling HSPA logging.

EXTERNALIZED FUNCTIONS

INTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS


Copyright (c) 2007 - 2014 by Qualcomm Technologies Incorporated.  All Rights Reserved.

*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/wcdma/l1/offline/src/hslog.c#1 $
$DateTime: 2015/01/27 06:42:19 $
$Author: mplp4svc $

when       who    what, where, why
--------   ---    ---------------------------------------------------------- 
09/03/14   hdk    F3 reduction
08/20/14   rs     Fixed compiler errors
07/30/14   jhl    Removed unused variable and fixed compiler warnings
07/26/14   rkmk   Reduce image size by using right macro depending upon number of parameters
07/21/14   hdk    Logging changes for HS ASAP start optimization
07/16/14   vs     Added support to print multisim config fields in hs log packets
07/10/14   hdk    Maintaining local ul hs call info to avoid dead lock due to polling 
                  of hs config during logging
06/27/14   hdk    Created hslog.c
===========================================================================*/

/* ==========================================================================
** Includes and Variable Definitions
** ========================================================================*/
/* -----------------------------------------------------------------------
** Include Files
** ----------------------------------------------------------------------- */

#include "wcdma_variation.h"
#include "comdef.h"
#include "customer.h"

#include "wl1m.h"
#include "hslog.h"
#include "mdspsync.h"
#include "tlm.h"
#include "mcalwcdma_evt.h" 
#include "l1msetup.h"
#include "dechs.h"
#include "enchs.h"
#include "hscfg.h"
#include "wl1multisim.h"

uint32 hs_decode_status_tb_blk_data[HS_INFO_TABLE_COUNT][HS_DECODE_DSCH_TB_DATA_BIT_BUF_LEN];

dsch_info_mac_ehs_st hs_decode_status_reord_pdu_info[HS_INFO_TABLE_COUNT]
                                 [HS_DECODE_DSCH_TB_DATA_BIT_BUF_LEN]
                                 [L1_DL_HS_DSCH_HARQ_MAX_PROC_MIMO];

uint8 debug_tsn_issue = 0;

extern boolean hs_cfg_log_pkt_pending;

/* Master controller database used during any request processing */
extern l1m_ctrl_db_type l1m_ctrl_db;


DEC_HS_CME_CME_CB_FUNC_TYPE *dec_hs_cme_cb = NULL;

/* -------------------------------------------------------- */
/* Following variable are for HS statistics and its logging */
/* -------------------------------------------------------- */
/*init flag for 0x4222*/
boolean hs_stat_refresh_info_flag= TRUE;

/* Last sub frame number at which stat generation and acumulation will stop */
volatile uint16 hs_stat_final_sub_fn = INVALID_STOP_GSFN;

/* This maintains difference between HS global sub frame number and CFNx5.
   at which stat generation is started or restarted */
int16   hs_stat_cfnx5_sub_fn_offset;
/* This is mDSP buffer index at which decode stats are being curently
   saved by mDSP */
uint8   hs_stat_info_table_idx[L1_HSDPA_MAX_CARRIERS];
/* HS stat are double buffered in order to submit it in task context using
   local command. This variable is index to current buffer in which stats
   are being accumulated */
uint8   hs_stat_info_buf_idx;

/* Following variables are used to initiate change in stat generation
   and accumulation activity                                          */
/* ------------------------------------------------------------------ */

/* DL DPCH CFN at which stat generation and accumulation need to start */
uint8 hs_stat_start_cfn;
/* First sub-frame number in starting DPCH frame at which stat accumulation
   and generation starts */
uint16 hs_stat_start_sub_fn;
/* mDSP buffer index to be used at which decode stats will be saved by mDSP */
uint8  hs_stat_start_info_table_idx[L1_HSDPA_MAX_CARRIERS];

/* HS decode status accumulation buffer */
HS_DECODE_STATUS_LOG_PKT_type hs_decode_status_log[HS_DECODE_STATUS_NUM_BUF];
/* HS decode status buffer availability status */
boolean hs_decode_status_log_buf_avail_status[HS_DECODE_STATUS_NUM_BUF];

/* indicate HS decode status need to be submitted */
boolean hs_decode_status_log_submit;
/* indicate buffer index that need to be submitted */
uint8 hs_decode_status_log_buf_idx_to_submit;
/* HS call information - USed for logging the header in decode status log packet */
dec_hs_call_info_struct_type dec_hs_call_info;

/* HS SCCH, HARQ, CQI stats log packets */
/* ------------------------------------ */

HS_SCCH_STAT_LOG_PKT_type hs_scch_stats_log;
HS_DSCH_HARQ_STAT_LOG_PKT_type hs_harq_stats_log;
HS_CQI_STAT_LOG_PKT_type hs_cqi_stats_log;

#ifdef FEATURE_WCDMA_MIMO
/* Flag to indicate if log accumulation is active */
boolean hs_mimo_demod_log_accum_active;
/* Flag for the status of the log code in QXDM */
boolean hs_mimo_demod_logging_enabled_qxdm;
/* Buffer index where samples are accumulated */
uint8 hs_mimo_demod_log_info_buf_idx;
/* Log buffer availability status database */
boolean hs_mimo_demod_log_buf_avail_status[HS_MIMO_LOG_NUM_BUF];
/* The log buffer where samples are accumulated */
HS_MIMO_DEMOD_LOG_PKT_type hs_mimo_demod_log_pkt_buf[HS_MIMO_LOG_NUM_BUF];
/* Variable to store the log action */
hs_stat_gen_action_enum_type hs_mimo_demod_log_action;
/* The start CFN from where logging has to start */
uint8 hs_mimo_demod_log_start_cfn;
/* The start subframe number for logging */
uint16 hs_mimo_demod_log_start_sub_fn;
/* The final subframe number at which logging for this packet has to end */
uint16 hs_mimo_demod_log_final_sub_fn;
/* The offset between CFNx5 and start subframe number */
int16 hs_mimo_demod_cfnx5_sub_fn_offset;
/* Flag indicating whether a packet has been submitted to diag */
boolean hs_mimo_demod_log_submit;
/* The log buffer index to be submitted */
uint8 hs_mimo_demod_log_buf_idx_to_submit;
/* offset value in the log buffer*/
uint16 hs_mimo_demod_sample_buf_offset;

/* Flag to indicate if log accumulation is active */
boolean hs_mimo_chan_log_accum_active;
/* Flag for the status of the log code in QXDM */
boolean hs_mimo_chan_logging_enabled_qxdm;
/* Buffer index where samples are accumulated */
uint8 hs_mimo_chan_log_info_buf_idx;
/* Log buffer availability status database */
boolean hs_mimo_chan_log_buf_avail_status[HS_MIMO_LOG_NUM_BUF];
/* The log buffer where samples are accumulated */
HS_MIMO_CH_ANALYSIS_LOG_PKT_type hs_mimo_chan_log_pkt_buf[HS_MIMO_LOG_NUM_BUF];
/* Variable to store the log action */
hs_stat_gen_action_enum_type hs_mimo_chan_log_action;
/* The start CFN from where logging has to start */
uint8 hs_mimo_chan_log_start_cfn;
/* The start subframe number for logging */
uint16 hs_mimo_chan_log_start_sub_fn;
/* The final subframe number at which logging for this packet has to end */
uint16 hs_mimo_chan_log_final_sub_fn;
/* The offset between CFNx5 and start subframe number */
int16 hs_mimo_chan_cfnx5_sub_fn_offset;
/* Flag indicating whether a packet has been submitted to diag */
boolean hs_mimo_chan_log_submit;
/* The log buffer index to be submitted */
uint8 hs_mimo_chan_log_buf_idx_to_submit;
/* offset value in the log buffer*/
uint16 hs_mimo_chan_sample_buf_offset;
#endif

/* Flag to indicate if log accumulation is active */
boolean hs_pess_cqi_log_accum_active;
/* Flag for the status of the log code in QXDM */
boolean hs_pess_cqi_logging_enabled_qxdm;
/* Buffer index where samples are accumulated */
uint8 hs_pess_cqi_log_info_buf_idx;
/* Log buffer availability status database */
boolean hs_pess_cqi_log_buf_avail_status[HS_PESS_CQI_LOG_NUM_BUF];
/* The log buffer where samples are accumulated */
HS_PESS_CQI_LOG_PKT_type hs_pess_cqi_log_pkt_buf[HS_PESS_CQI_LOG_NUM_BUF];
/* Variable to store the log action */
hs_stat_gen_action_enum_type hs_pess_cqi_log_action;
/* The start CFN from where logging has to start */
uint8 hs_pess_cqi_log_start_cfn;
/* The start subframe number for logging */
uint16 hs_pess_cqi_log_start_sub_fn;
/* The final subframe number at which logging for this packet has to end */
uint16 hs_pess_cqi_log_final_sub_fn;
/* The offset between CFNx5 and start subframe number */
int16 hs_pess_cqi_cfnx5_sub_fn_offset;
/* Flag indicating whether a packet has been submitted to diag */
boolean hs_pess_cqi_log_submit;
/* The log buffer index to be submitted */
uint8 hs_pess_cqi_log_buf_idx_to_submit;
/* offset value in the log buffer*/
uint16 hs_pess_cqi_sample_buf_offset;

/*-----------SCCH LOG Packet Variables--------*/
/*flag to init scch log after stop*/
boolean hs_scch_refresh_info_flag = TRUE;
/* Flag for the status of the log code in QXDM */
boolean hs_scch_logging_enabled_qxdm;
/* Buffer index where samples are accumulated */
uint8 hs_scch_log_info_buf_idx = 0;
/* Log buffer availability status database */
boolean hs_scch_log_buf_avail_status[HS_SCCH_LOG_NUM_BUF];
/* The log buffer where samples are accumulated */
HS_SCCH_LOG_PKT_type hs_scch_log_pkt_buf[HS_SCCH_LOG_NUM_BUF];
/* The final subframe number at which logging for this packet has to end */
volatile uint16 hs_scch_log_final_sub_fn = INVALID_STOP_GSFN;
/* Flag indicating whether a packet has been submitted to diag */
boolean hs_scch_log_submit;
/* The log buffer index to be submitted */
uint8 hs_scch_log_buf_idx_to_submit;
/* offset value in the log buffer*/
uint16 hs_scch_sample_buf_offset;

uint8 hs_harq_proc_id_info_idx[L1_DL_NUM_MAX_CARRIERS][L1_DL_HS_DSCH_HARQ_MAX_PROC_MIMO];
/* To keep track of the last valid transport block size for each HARQ process */
uint16 hs_last_valid_tbs[L1_DL_NUM_MAX_CARRIERS][L1_DL_HS_DSCH_HARQ_MAX_PROC_MIMO];
/* valid transport of the last valid transport block size for second stream*/
#ifdef FEATURE_WCDMA_MIMO
uint16 hs_last_valid_tbs_sec[L1_DL_HS_DSCH_HARQ_MAX_PROC_MIMO];
#endif
/* To keep track of the last received transport block size for each HARQ process */
#ifdef FEATURE_WCDMA_DYNAMIC_FING_GRAN
#error code not present
#endif /* FEATURE_WCDMA_DYNAMIC_FING_GRAN */

/* HARQ process statistics */
hs_dsch_harq_proc_stat_struct_type 
  hs_dsch_harq_proc_stat_info[L1_DL_NUM_MAX_CARRIERS][L1_DL_HS_DSCH_HARQ_MAX_PROC_MIMO];

/* other log packets */
/* ----------------- */

/* MAC HS hdr log packet buffer */
HS_MAC_HS_HDR_LOG_PKT_type dec_hs_mac_hs_hdr_log_pkt[DEC_HS_NUM_MAC_HS_HDR_LOG_PKT_BUF];
/* MAC-hs hdrlog packet buffer availability status */
boolean dec_hs_mac_hdr_log_buf_avail_status[DEC_HS_NUM_MAC_HS_HDR_LOG_PKT_BUF];

/* HS stat are double buffered in order to submit it in task context using
   local command. This variable is index to current buffer in which stats
   are being accumulated */
uint8 dec_hs_mac_hs_hdr_log_cur_buf_idx;
/* HS decode stats are flexible length based of SCCH decode status. This
   variable indicate the offset of HS decode status info used till current
   accumulation */
uint16 dec_hs_mac_hs_hdr_log_cur_buf_info_offset;

/* Flag to hold the status of log code in QXDM */
boolean dec_hs_mac_hs_log_code_enabled;

/* indicate HS decode status need to be submitted */
boolean dec_hs_mac_hs_hdr_log_submit;
/* indicate buffer index that need to be submitted */
uint8 dec_hs_mac_hs_hdr_log_buf_idx_to_submit;

/* Keeps track of the CFN count after the first log packet is sampled */
uint8 dec_hs_mac_hs_trk_cfn_log;

/*-----------HS Amplitude and Energy estimation log packet--------*/

/* HS Amplitude and Energy estimation log packet structures */
boolean hs_llr_refresh_info_flag = TRUE;
/* Flag for the status of the log code in QXDM */
boolean hs_llr_logging_enabled_qxdm;
/* Buffer index where samples are accumulated */
uint8   hs_llr_log_info_buf_idx;
/* Log buffer availability status database */
boolean hs_llr_log_buf_avail_status[HS_AMP_ENG_LOG_NUM_BUF];
/* The log buffer where samples are accumulated */
HS_AMP_ENG_EST_LOG_PKT_type hs_llr_log_pkt_buf[HS_AMP_ENG_LOG_NUM_BUF];
/* The final subframe number at which logging for this packet has to end */
volatile uint16  hs_llr_log_final_sub_fn = INVALID_STOP_GSFN;
/* Flag indicating whether a packet has been submitted to diag */
boolean hs_llr_log_submit;
/* The log buffer index to be submitted */
uint8   hs_llr_log_buf_idx_to_submit;
/* offset value in the AE log buffer*/
uint16 hs_llr_sample_buf_offset;


/*-----------HS + CM logging--------*/

/* Indicates whether HS+CM DL logging is active or not */
boolean hs_cm_log_dl_active;
/* index of the log buffer currently being used */
uint8 hs_cm_log_dl_buf_idx;

/* availability status of HS DL CM control table buffer */
boolean hs_cm_log_dl_buf_avail_status[HS_DEMOD_CTRL_NUM_BUF];

/* the HS+CM DL control table */
HS_DEMOD_CTRL_TABLE_LOG_PKT_type hs_cm_log_demod_ctrl[HS_DEMOD_CTRL_NUM_BUF];
/* flag indicating if the log packet has been submitted */
boolean hs_demod_ctrl_log_submit;
/* which index to submit */
uint8 hs_demod_ctrl_log_buf_idx_to_submit;
/* Holds the current state of HS demod log packet in QXDM */
boolean hs_cm_demod_logging_enabled;

/* stores the start CFN for the HS combiner */
uint8 hs_cm_log_dl_start_cfn;
/* Holds the DL HS stop subframe number */
uint16 hs_cm_log_dl_stop_sub_fr_num;
/* variable to hold the HS+CM operation type */
hs_stat_gen_action_enum_type hs_cm_log_dl_action;


/* the HS+CM UL control table */
HS_MOD_CTRL_TABLE_LOG_PKT_type hs_cm_log_mod_ctrl[HS_MOD_CTRL_NUM_BUF];
/* flag indicating if the log packet has been submitted */
boolean hs_mod_ctrl_log_submit;
/* which index to submit */
uint8 hs_mod_ctrl_log_buf_idx_to_submit;
/* Holds the current state of HS mod log packet in QXDM */
boolean hs_cm_mod_logging_enabled;

/* Indicates whether HS+CM UL logging is active or not */
boolean hs_cm_log_ul_active;
/* index of the log buffer currently being used */
uint8 hs_cm_log_ul_buf_idx;
/* stores the start CFN for the HS UL */
uint8 hs_cm_log_ul_start_cfn;
/* Holds the UL HS stop subframe number */
uint16 hs_cm_log_ul_stop_sub_fr_num;
/* variable to hold the HS+CM operation type */
enchs_log_action_enum_type hs_cm_log_ul_action;
/* availability status of HS UL CM control table buffer */
boolean hs_cm_log_ul_buf_avail_status[HS_MOD_CTRL_NUM_BUF];

/*-----------HS DPCCH log packet--------*/

/* CQI estimation over period of 30 seconds.
   Estimate is done every 2 seconds */

#define ENCHS_CQI_ESTIMATE_STAT_TOTAL_PERIOD_SEC 30
#define ENCHS_CQI_ESTIMATE_STAT_DUMP_SAMPLE_SEG 15


#define ENCHS_NUM_QUANTIZED_CQI_VALS 31

uint8 cqi_average_window_period_seconds = 2;

/* Number of samples in 2 seconds */
#define ENCHS_CQI_ESTIMATE_STAT_NUM_SAM_PER_SEG (500 * cqi_average_window_period_seconds) 

#define ENCHS_CQI_ESTIMATE_STAT_NUM_TOTAL_SAMPLES \
  (ENCHS_CQI_ESTIMATE_STAT_NUM_SAM_PER_SEG * ENCHS_CQI_ESTIMATE_STAT_DUMP_SAMPLE_SEG)

uint32 enchs_num_new_tx_rece_cqi_est_seg[ENCHS_CQI_ESTIMATE_STAT_DUMP_SAMPLE_SEG];
uint32 enchs_num_new_tx_err_cqi_est_seg[ENCHS_CQI_ESTIMATE_STAT_DUMP_SAMPLE_SEG];
uint16 enchs_cqi_estimate_stat_buffer
       [ENCHS_CQI_ESTIMATE_STAT_DUMP_SAMPLE_SEG][ENCHS_NUM_QUANTIZED_CQI_VALS];
uint8 enchs_cqi_estimate_stat_cur_segment;
uint16 enchs_cqi_estimate_stat_cur_segment_num_samples;
uint8 enchs_cqi_estimate_stat_num_seg_accum;


WfwTxAsyncReadStruct *wfw_intf_tx_hs_async_read_ptr;
uint32 enchs_avg_cqi;
uint16 enchs_avg_cqi_num_sample;



/* This indicate that HS UL DPCCH logging is active and is going on */
boolean enchs_log_accum_active;
/* This indicates if the log code in QXDM is enabled */
boolean enchs_dpcch_logging_on;
/* This maintains difference between HS global sub frame number and CFNx5.
   at which logging is started or restarted */
int16   enchs_log_cfnx5_sub_fn_offset;
/* This is mDSP buffer index at which UL log info are being curently
   saved by mDSP */
uint8   enchs_log_info_table_idx;
/* HS UL logging is double buffered in order to submit it in task context using
   local command. This variable is index to current buffer in which stats
   are being accumulated */
uint8   enchs_log_info_buf_idx;

/* Following variables are used to initiate change in logging and
   accumulation activity                                          */
/* -------------------------------------------------------------- */

/* HS UL DPCCH logging action type */
enchs_log_action_enum_type enchs_log_action;
/* UL DPCH CFN at which logging accumulation need to start */
uint8 enchs_log_start_cfn;
/* First sub-frame number in starting DPCH frame at which logging
   accumulation starts */
uint16 enchs_log_start_sub_fn;
/* mDSP buffer index to be used at which decode stats will be saved by mDSP */
uint8  enchs_log_start_info_table_idx;
/* Last sub frame number at which stat generation and acumulation will stop */
uint16 enchs_log_final_sub_fn;

/* Log packet buffers */
HS_UL_DPCCH_INFO_DBG_LOG_PKT_type enchs_dpcch_dbg_log_pkt[ENCHS_LOG_NUM_BUF];
boolean enchs_dpcch_log_buf_avail_status[ENCHS_LOG_NUM_BUF];

/* indicate UL HS DPCCH log need to be submitted */
boolean enchs_dpcch_log_submit;
/* indicate buffer index that need to be submitted */
uint8 enchs_dpcch_log_buf_idx_to_submit;

/*stores HS call info during enchs_do_logging_action 
used to avoid polling of HS config for every logging init*/
enc_hs_call_info_struct_type enc_hs_call_info;

/* -----------------------------------------------------------------------
** Local functions
** ----------------------------------------------------------------------- */
static HS_DECODE_STATUS_LOG_PKT_type* dec_hs_get_new_hs_stat_buff(void);
static HS_DECODE_STATUS_LOG_PKT_type* dec_hs_get_curr_hs_stat_buff(void);
static void dec_hs_logging_init(uint16 start_sub_fn);
static void dec_hs_log_populate(WfwDbackHsDecodeStatusLogEntryStruct *fw_hs_decode_status_per_sf_ptr);
static void dec_hs_logging_stop();

static void dec_hs_init_hs_log(HS_DECODE_STATUS_LOG_PKT_type *this_hs_decode_status_log,
                                    uint16 stat_start_sub_fn);

static void dec_hs_init_llr_log(HS_AMP_ENG_EST_LOG_PKT_type* this_hs_amp_eng_est_log_pkt);
static HS_AMP_ENG_EST_LOG_PKT_type* dec_hs_get_new_llr_log_buff(void);
static inline HS_AMP_ENG_EST_LOG_PKT_type* dec_hs_get_curr_llr_log(void);
static void dec_hs_llr_stop_logging(void);

/*Funciton to get new pess cqi buffer*/
LOCAL HS_PESS_CQI_LOG_PKT_type* dec_hs_get_new_pess_cqi_buff(void);
/*Function to init pess cqi log values*/
LOCAL void dec_hs_init_pess_cqi_log(HS_PESS_CQI_LOG_PKT_type *this_hs_pess_cqi_log_pkt,
                                    uint16 global_sub_fr_num);

/*Function to get current CQI log pointer*/
LOCAL inline HS_PESS_CQI_LOG_PKT_type* dec_hs_get_curr_pess_cqi_log(void);

/* Funtion to stop pess cqi logging*/
LOCAL void dec_hs_pess_cqi_stop_logging(void);
/* Funtion to send the log submit command*/
LOCAL void dec_hs_pess_cqi_send_log_submit_cmd(uint8 hs_mimo_log_idx);
/* funtion to update the logging samples for pess cqi packet*/
LOCAL void dec_hs_pess_cqi_log_update(void);
/* Funtion to stop hs-scch logging*/
LOCAL void dec_hs_scch_stop_logging(void);
/* Funtion to send the log submit command*/
LOCAL void dec_hs_scch_send_log_submit_cmd(uint8 hs_mimo_log_idx);

/*Function to initialize new SCCH log*/
static HS_SCCH_LOG_PKT_type* dec_hs_get_new_scch_log_buff(void);
/*Function to Get current SCCH log*/
static HS_SCCH_LOG_PKT_type* dec_hs_get_curr_scch_log(void);

static void dec_hs_init_scch_log(uint8 global_sub_fr_num);

/*function to read from FW buffer and populate scch log */
static void dec_hs_scch_log_populate(WfwDbackHsDecodeStatusLogEntryStruct *fw_hs_decode_status_per_sf_ptr,
                                      uint8 num_carr);

/* funtion to update the logging samples for hs-scch packet*/
LOCAL void dec_hs_scch_log_update(void);

/* Extern function to send HS_CFG_LOG_PKT */
extern void hs_send_cfg_log_pkt(void);

#ifdef FEATURE_WCDMA_MIMO
/* Funtion to stop mimo demod logging*/
LOCAL void dec_hs_mimo_demod_stop_logging(void);
/* Funtion to send the log submit command*/
LOCAL void dec_hs_mimo_demod_send_log_submit_cmd(uint8 hs_mimo_log_idx);
/* Funtion to initialize mimo demo log state*/
LOCAL void dec_hs_mimo_demod_log_init(void);
/* funtion to update the logging samples for mimo demod packet*/
LOCAL void dec_hs_mimo_demod_log_update(void);

/* Funtion to stop mimo chan logging*/
LOCAL void dec_hs_mimo_chan_stop_logging(void);
/* Funtion to send the log submit command*/
LOCAL void dec_hs_mimo_chan_send_log_submit_cmd(uint8 hs_mimo_log_idx);
/* Funtion to initialize mimo demo log state*/
LOCAL void dec_hs_mimo_chan_log_init(void);
/* funtion to update the logging samples for mimo demod packet*/
LOCAL void dec_hs_mimo_chan_log_update(void);
#endif

/*FUNCTION enchs_init_log_buffer*/
static HS_UL_DPCCH_INFO_DBG_LOG_PKT_type* enchs_init_log_buffer(void);
static void enchs_dpcch_stop_logging(void);


/* =======================================================================
**                            Function Definitions
** ======================================================================= */
/*===========================================================================
FUNCTION dec_hs_quantize_cqi

DESCRIPTION
  This function quantize CQI from lowest value of 0 to max value of 30.
  Unquantized value is 10 bit number in 6.4 format (Q4).
  
DEPENDENCIES
  None

RETURN VALUE
  6 Bit unquantized value form 0 to 30

SIDE EFFECTS
  None
===========================================================================*/

uint8 dec_hs_quantize_cqi(
  /* Unquantized CQI value */
  uint16 unquan_cqi)
{
  /* function local variables */
  /* ------------------------ */

  /* Quantized CQI value */
  uint8 quan_cqi;

  /*** function code starts here ***/

  /* get Upper 6 bits of 10 bit un-quantized value */
  quan_cqi = (uint8)(unquan_cqi >> 4);

  /* Check fraction part, and do rounding of quantized CQI value */
  if (unquan_cqi & 0x08)
  {
    quan_cqi += 1;
  }

  /* Do bounding in valid range */
  if (quan_cqi > 30)
  {
    quan_cqi = 30;
  }

  return quan_cqi;
}

/*===========================================================================
FUNCTION mcalwcdma_dec_hs_mac_hs_hdr_logging_init

DESCRIPTION
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
void mcalwcdma_dec_hs_mac_hs_hdr_logging_init()
{
  /* loop index */
  uint32 index_buf;
  
  /* iterate over all log buffer status to mark them available */
  for (index_buf = 0; index_buf < DEC_HS_NUM_MAC_HS_HDR_LOG_PKT_BUF; index_buf++)
  {
    dec_hs_mac_hdr_log_buf_avail_status[index_buf] = TRUE;
  }

  /* Set cur MAC-hs hdr log buffer index to invalid */
  dec_hs_mac_hs_hdr_log_cur_buf_idx = HS_INFO_TABLE_INVALID_VAL;

  /* Initialize the log code enabled flag to FALSE */
  dec_hs_mac_hs_log_code_enabled = FALSE;

  /* Init log submit request to FALSE */
  dec_hs_mac_hs_hdr_log_submit = FALSE;
}
/*===========================================================================
FUNCTION mcalwcdma_dec_hs_update_mac_hs_header_logging

DESCRIPTION
  This function accumulates MAC-hs header log samples and submits to DIAG once
  the packet is full. IT then creates a new packet for next accumulation.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

void mcalwcdma_dec_hs_update_mac_hs_header_logging(
  /* DOB status handle */
  mcalwcdma_dec_dob_status_handle_type handle,
  /* buffer for MAC-HS header data */
  uint32 *mac_hs_header_buf,
  uint16 offset)
{
  /* MAC-hs header log packet ptr */
  HS_MAC_HS_HDR_LOG_PKT_type *mac_hs_hdr_log_pkt_ptr;
  /* number of W32 in current MAC-hs hdr log buffer */
  uint16 num_w32_in_mac_hs_hdr_log_len;
  uint32 index_data;

  HS_LOG_INTLOCK();

  /* Save the data for 0x4222 logging */
  {
    /* Save first 32 bit of HS transport block in buffer for logging */
    uint8 buf_idx, hs_info_idx;

    buf_idx = mcalwcdma_dec_get_hs_subframe_num(handle) % HS_DECODE_DSCH_TB_DATA_BIT_BUF_LEN;
    hs_info_idx = mcalwcdma_dec_get_hs_info_id(handle);

    if ((buf_idx >= HS_DECODE_DSCH_TB_DATA_BIT_BUF_LEN) ||
        (hs_info_idx >= HS_INFO_TABLE_COUNT))
    {
      HS_LOG_INTFREE();
      WL1_MSG2(ERROR, "Invalid idx in mac_hs_header_logging buf_idx %d hs_info_idx %d",
                      buf_idx, hs_info_idx);
      return;
    }

    hs_decode_status_tb_blk_data[hs_info_idx][buf_idx] = mac_hs_header_buf[0];
  }
  
  /* MAC-hs header logging */
  /* --------------------- */

  /* Check for the current status of this log code if previously disabled */
  if (!dec_hs_mac_hs_log_code_enabled)
  {
    if (log_status(HS_MAC_HS_HDR_LOG_PKT))
    {
      /* Log code enabled now */
      dec_hs_mac_hs_log_code_enabled = TRUE;
      dec_hs_mac_hs_hdr_log_cur_buf_idx = HS_INFO_TABLE_INVALID_VAL;
    }
  }

  /* Mark MAC-hs buffer ptr to NULL */
  mac_hs_hdr_log_pkt_ptr = NULL;
  /* Calculate number of W32 required to log this header */
  num_w32_in_mac_hs_hdr_log_len = (uint8)((offset & 0x1F) ?
                                    ((offset >> 5) + 1) : (offset >> 5));

  /* Get log buffer ptr if it is valid and check if it is required to be logged now */
  if (dec_hs_mac_hs_hdr_log_cur_buf_idx < DEC_HS_NUM_MAC_HS_HDR_LOG_PKT_BUF)
  {
    mac_hs_hdr_log_pkt_ptr =
      &(dec_hs_mac_hs_hdr_log_pkt[dec_hs_mac_hs_hdr_log_cur_buf_idx]);

    /* Check if the buffer has available space to accomodate another MAC-hs header
       num_w32 * 4 = length of the new header in bytes
       dec_hs_mac_hs_hdr_log_cur_buf_info_offset = already filled up
       2 = length of the global subframe number of the new MAC-hs header */
    if (((num_w32_in_mac_hs_hdr_log_len * 4) + dec_hs_mac_hs_hdr_log_cur_buf_info_offset + 2) >
        (uint16)HS_MAC_HS_HDR_LOG_BUG_MAX_LEN) /*lint !e506 */
    {
      /* log packet header pointer */
      hsdpa_log_hdr_struct_type *mac_hs_hdr_log_pkt_hdr;

      /* Save length og log packet buffer */
      mac_hs_hdr_log_pkt_hdr = (hsdpa_log_hdr_struct_type*) mac_hs_hdr_log_pkt_ptr;
      mac_hs_hdr_log_pkt_hdr->len = (uint16) (sizeof(HS_MAC_HS_HDR_LOG_PKT_type) -
        (sizeof(mac_hs_hdr_log_pkt_ptr->mac_hdr_buffer) -
         dec_hs_mac_hs_hdr_log_cur_buf_info_offset ));

      /* submit log packet here before proceeding */
      dec_hs_mac_hs_hdr_send_log_submit_cmd(dec_hs_mac_hs_hdr_log_cur_buf_idx);

      dec_hs_mac_hs_hdr_log_cur_buf_idx = HS_INFO_TABLE_INVALID_VAL;
      mac_hs_hdr_log_pkt_ptr = NULL;
    }
  }

  /* Only if the log code is enabled, allocate a new packet and accumulate
     samples for this log */
  if (dec_hs_mac_hs_log_code_enabled)
  {
    if (mac_hs_hdr_log_pkt_ptr == NULL)
    {
      uint32 index_buf; /* loop index */

      /* iterate over all log buffer status to mark them available */
      for (index_buf = 0; index_buf < DEC_HS_NUM_MAC_HS_HDR_LOG_PKT_BUF; index_buf++)
      {
        if (dec_hs_mac_hdr_log_buf_avail_status[index_buf])
        {
          dec_hs_mac_hs_hdr_log_cur_buf_idx = index_buf;
          dec_hs_mac_hdr_log_buf_avail_status[index_buf] = FALSE;
          dec_hs_mac_hs_hdr_log_cur_buf_info_offset = 0;

          mac_hs_hdr_log_pkt_ptr =
            &(dec_hs_mac_hs_hdr_log_pkt[dec_hs_mac_hs_hdr_log_cur_buf_idx]);

          /* Do time stamp corresponding to first sample */
          log_set_timestamp(mac_hs_hdr_log_pkt_ptr);
          /* init number of samples accumulated to 0 */
          mac_hs_hdr_log_pkt_ptr->num_mac_hdr = 0;

          /* The first log packet is about to be sampled */
          dec_hs_mac_hs_trk_cfn_log = 0;
          break;
        }
      }

      /* This should never happen */
      if (mac_hs_hdr_log_pkt_ptr == NULL)
      {
        WL1_MSG0(ERROR, "Can't get any MAC-hs hdr log packet buf");

        /* iterate over all log buffer status to mark them available */
        for (index_buf = 0; index_buf < DEC_HS_NUM_MAC_HS_HDR_LOG_PKT_BUF; index_buf++)
        {
          dec_hs_mac_hdr_log_buf_avail_status[index_buf] = TRUE;
        }

        dec_hs_mac_hs_hdr_log_cur_buf_idx = 0;
        dec_hs_mac_hdr_log_buf_avail_status[dec_hs_mac_hs_hdr_log_cur_buf_idx] = FALSE;
        dec_hs_mac_hs_hdr_log_cur_buf_info_offset = 0;

        mac_hs_hdr_log_pkt_ptr =
          &(dec_hs_mac_hs_hdr_log_pkt[dec_hs_mac_hs_hdr_log_cur_buf_idx]);

        /* Do time stamp corresponding to first sample */
        log_set_timestamp(mac_hs_hdr_log_pkt_ptr);
        /* init number of samples accumulated to 0 */
        mac_hs_hdr_log_pkt_ptr->num_mac_hdr = 0;

        /* The first log packet is about to be sampled */
        dec_hs_mac_hs_trk_cfn_log = 0;
      }
    }

    /* Accumulate log sample */
    /* -----------------------------------------------------------------*/
    {
      /* Populate the global subframe number and the data for this header */
      hs_mac_hdr_log_info_struct_type *mac_hs_hdr_log_info;
      mac_hs_hdr_log_info = (hs_mac_hdr_log_info_struct_type *)
        &mac_hs_hdr_log_pkt_ptr->mac_hdr_buffer[dec_hs_mac_hs_hdr_log_cur_buf_info_offset];

      /* Save the global sub frame number for this MAC-hs header */
      mac_hs_hdr_log_info->global_sub_fn = mcalwcdma_dec_get_hs_subframe_num(handle);
      dec_hs_mac_hs_hdr_log_cur_buf_info_offset += 2;

      /* Save MAC-hs header data */
      for (index_data = 0;
           (index_data < num_w32_in_mac_hs_hdr_log_len) &&
           (index_data < DEC_HS_DOB_W32_READ_FOR_MAC_HS_HEADER_DECODE); /*lint !e506 */
           index_data++)
      {
        mac_hs_hdr_log_info->mac_hdr_buffer[index_data] =
          mac_hs_header_buf[index_data];

        dec_hs_mac_hs_hdr_log_cur_buf_info_offset += 4;
      }

      /* Update number of MAC-hs headers */
      mac_hs_hdr_log_pkt_ptr->num_mac_hdr++;
    } /* End accumulating log sample */
  }
  /* MAC-hs hdr logging end */

  HS_LOG_INTFREE();
}

#ifdef FEATURE_HSDPA_MAC_EHS
/*===========================================================================
FUNCTION mcalwcdma_dec_hs_update_mac_ehs_header_logging

DESCRIPTION
  This function accumulates MAC-ehs header log samples and submits to DIAG once
  the packet is full. It then creates a new packet for next accumulation.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

void mcalwcdma_dec_hs_update_mac_ehs_header_logging(
  /* DOB status handle */
  mcalwcdma_dec_dob_status_handle_type handle,
  /* TB set header information structure pointer */
  mcalwcdma_dec_tb_set_hdr_struct_type *tbset_hdr_info,
  uint32 num_reorder_sdu_in_reord_pdu[],
  uint8 si_info[],
  uint16 tsn_info[],
  uint8 q_info[]
  )
{
  uint32 index_reorder_pdu;
  uint32 hs_info_idx;
  uint8 gsfn_idx;
  uint8 harq_idx;
  uint16 temp_gsfn;
 
  HS_LOG_INTLOCK();

  /* Save first 32 bit of HS transport block in buffer for logging */
  hs_info_idx = mcalwcdma_dec_get_hs_info_id(handle);
  temp_gsfn = mcalwcdma_dec_get_hs_subframe_num(handle);
  gsfn_idx = mcalwcdma_dec_get_hs_subframe_num(handle) % HS_DECODE_DSCH_TB_DATA_BIT_BUF_LEN;
  harq_idx = mcalwcdma_dec_get_hs_harq_id(handle);

  if (harq_idx >= 16)
  {
    HS_LOG_INTFREE();
    WL1_MSG1(ERROR, "Invalid harq_idx %d in mac_ehs_header_logging", harq_idx);
    return;
  }

  if (hs_info_idx >= HS_INFO_TABLE_COUNT)
  {
    HS_LOG_INTFREE();
    WL1_MSG2(ERROR, "Invalid hs_info_idx %d, must be less than %d",
                    hs_info_idx, HS_INFO_TABLE_COUNT);
    return;
  }

   hs_decode_status_reord_pdu_info[hs_info_idx][gsfn_idx][harq_idx].num_reorder_sdu =
         tbset_hdr_info->info.ehs.num_reorder_sdu;

   if (tbset_hdr_info->info.ehs.num_reorder_sdu == 0)
   {
     memset(hs_decode_status_reord_pdu_info[hs_info_idx][gsfn_idx][harq_idx].reorder_pdu_info, 0xFF,
            sizeof(hs_decode_status_reord_pdu_info[hs_info_idx][gsfn_idx][harq_idx].reorder_pdu_info));
   }
   else
   {

    reord_pdu_info_st* reord_pdu_info_ptr = 
    hs_decode_status_reord_pdu_info[hs_info_idx][gsfn_idx][harq_idx].reorder_pdu_info;

     for (index_reorder_pdu = 0; index_reorder_pdu < MCALWCDMA_DEC_HS_REORDER_PDU_MAX_COUNT; index_reorder_pdu++)
     {

       reord_pdu_info_ptr[index_reorder_pdu].num_sdu = 
                                                num_reorder_sdu_in_reord_pdu[index_reorder_pdu];
       reord_pdu_info_ptr[index_reorder_pdu].si      = si_info[index_reorder_pdu];
       reord_pdu_info_ptr[index_reorder_pdu].tsn     = tsn_info[index_reorder_pdu];
       reord_pdu_info_ptr[index_reorder_pdu].qid     = q_info[index_reorder_pdu];
    }
  }

  if (debug_tsn_issue)
  {
    MSG_5(MSG_SSID_DFLT, MSG_LEGACY_HIGH, "ehs logging GSFN %d harq %d idx %d numSDU %d buf 0x%x",
          temp_gsfn,
          harq_idx,
          hs_info_idx,
          hs_decode_status_reord_pdu_info[hs_info_idx][gsfn_idx][harq_idx].num_reorder_sdu,
          *(uint32*)(&hs_decode_status_reord_pdu_info[hs_info_idx][gsfn_idx][harq_idx].reorder_pdu_info[0]));
  }

  HS_LOG_INTFREE();
}
#endif /* FEATURE_HSDPA_MAC_EHS */


/*===========================================================================
FUNCTION dec_hs_mac_hs_hdr_send_log_submit_cmd

DESCRIPTION
  This function is called to post MAC-hs header log submit command. There is
  only one HS log submit command. HS cfg module calls all accumlated logging
  entities. Each entity is reponsible for maintaining log submit pending
  information. It keeps this information for HS decode status log submit
  pending information
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

void dec_hs_mac_hs_hdr_send_log_submit_cmd(
  /* HS decode status log buffer index to submit */
  uint8 mac_hs_hdr_info_buf_idx)
{
  /* function local variables */
  /* ------------------------ */

  /* MAC-hs header log packet pointer that need to be submitted */
  HS_MAC_HS_HDR_LOG_PKT_type *this_mac_hs_hdr_log;
  /* MAC-hs header log packet header pointer */
  hsdpa_log_hdr_struct_type *mac_hs_hdr_log_pkt_hdr;

  /*** function code starts here ***/

  /* Check if there is a pending request to submit previous log packet */
  if (dec_hs_mac_hs_hdr_log_submit)
  {
    /* Print error message and mark this buffer as available one */

    /* get log packet pointer */
    this_mac_hs_hdr_log =
      &(dec_hs_mac_hs_hdr_log_pkt[mac_hs_hdr_info_buf_idx]);
    /* Get log packet header pointer and set log code to that */
    mac_hs_hdr_log_pkt_hdr = (hsdpa_log_hdr_struct_type*) this_mac_hs_hdr_log;

    WL1_MSG3(ERROR, "MAC_HS_HDR_LOG:Failed idx %d info sz %d idx %d pending",
                    mac_hs_hdr_info_buf_idx,
                    mac_hs_hdr_log_pkt_hdr->len,
              dec_hs_mac_hs_hdr_log_buf_idx_to_submit);

    /* Mark this log buffer to available */
    dec_hs_mac_hdr_log_buf_avail_status[mac_hs_hdr_info_buf_idx] = TRUE;
  }
  else
  {
    /* Save MAC-hs header log buffer index to submit later in task context */
    dec_hs_mac_hs_hdr_log_submit = TRUE;
    dec_hs_mac_hs_hdr_log_buf_idx_to_submit = mac_hs_hdr_info_buf_idx;

    /* Send local command to l1m */
    hs_send_submit_log_cmd();
  }
}

/*===========================================================================
FUNCTION dec_hs_mac_hs_hdr_submit_log_buffer

DESCRIPTION
  This function is called from hscfg module in response of HS log submit
  command. It checks for MAC-hs header log submit flag. It is set to TRUE,
  it submit log buffer pointed by log buffer index to submit.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

void dec_hs_mac_hs_hdr_submit_log_buffer(void)
{
  /* function local variables */
  /* ------------------------ */

  boolean log_code_disabled = FALSE;
  
  /* MAC-hs header log packet pointer that need to be submitted */
  HS_MAC_HS_HDR_LOG_PKT_type *this_mac_hs_hdr_log;
  /* MAC-hs header log packet header pointer */
  hsdpa_log_hdr_struct_type *mac_hs_hdr_log_pkt_hdr;
  /* MAC-hs header log buffer index to submit */
  uint8 mac_hs_hdr_info_buf_idx;

  /*** function code starts here ***/

  /* If MAC-hs header log need to be submitted then proceed otherwise
     simply return from here */
  if (dec_hs_mac_hs_hdr_log_submit)
  {
    /* save log buffer index to submit */
    mac_hs_hdr_info_buf_idx = dec_hs_mac_hs_hdr_log_buf_idx_to_submit;
  }
  else
  {
    return;
  }

  if (log_status(HS_MAC_HS_HDR_LOG_PKT))
  {
    /* get log packet pointer */
    this_mac_hs_hdr_log =
      &(dec_hs_mac_hs_hdr_log_pkt[mac_hs_hdr_info_buf_idx]);
    /* Get log packet header pointer and set log code to that */
    mac_hs_hdr_log_pkt_hdr = (hsdpa_log_hdr_struct_type*) this_mac_hs_hdr_log;
    mac_hs_hdr_log_pkt_hdr->code = HS_MAC_HS_HDR_LOG_PKT;
    /* submit the log packet */
    if (!log_submit((PACKED void *) this_mac_hs_hdr_log))
    {
      WL1_MSG1(ERROR, "MAC_HS_HDR_LOG:Failed info sz %d",
                      mac_hs_hdr_log_pkt_hdr->len);
    }
  }
  else
  {
    log_code_disabled = TRUE;
  }

  HS_LOG_INTLOCK();
  /* Mark MAC-hs hdr log buffer to available */
  dec_hs_mac_hdr_log_buf_avail_status[mac_hs_hdr_info_buf_idx] = TRUE;
  /* reset MAC-hs hdr log to submit flag */
  dec_hs_mac_hs_hdr_log_submit = FALSE;

  /* If the log code is disabled, clear all buffers and mark logging state
     as well as logging buffer index as invalid */
  if (log_code_disabled)
  {
    uint32 index_buf;
    
    /* Mark logging OFF by QXDM */
    dec_hs_mac_hs_log_code_enabled = FALSE;
    /* Mark the log buffer index as invalid */
    dec_hs_mac_hs_hdr_log_cur_buf_idx = HS_INFO_TABLE_INVALID_VAL;    
    /* iterate over all log buffer status to mark them available */
    for (index_buf = 0; index_buf < DEC_HS_NUM_MAC_HS_HDR_LOG_PKT_BUF; index_buf++)
    {
      dec_hs_mac_hdr_log_buf_avail_status[index_buf] = TRUE;
    }
  }
  
  HS_LOG_INTFREE();
}

/*===========================================================================
FUNCTION dec_hs_flush_mac_hs_log_pkt

DESCRIPTION
  This function is called from hscfg module to flush MAC hs header log packet
  if any accumulation is pending. It is called when there is switch in DL
  HS config at any reconfig or stop action.
  Note that variables accessed by this functions are shared with function
  that reads TB header read. However both functions are event on Rx timeline
  so there for they are mutually exclusive.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

void dec_hs_flush_mac_hs_log_pkt(void)
{
  /* function local variables */
  /* ------------------------ */

  /* MAC-hs header log packet ptr */
  HS_MAC_HS_HDR_LOG_PKT_type *mac_hs_hdr_log_pkt_ptr;

  /*** function code starts here ***/

  HS_LOG_INTLOCK();
  /* Get log buffer ptr if it is valid and check if it is required to be logged now */
  if (dec_hs_mac_hs_hdr_log_cur_buf_idx < DEC_HS_NUM_MAC_HS_HDR_LOG_PKT_BUF)
  {
    mac_hs_hdr_log_pkt_ptr =
      &(dec_hs_mac_hs_hdr_log_pkt[dec_hs_mac_hs_hdr_log_cur_buf_idx]);

    if (mac_hs_hdr_log_pkt_ptr->num_mac_hdr > 0)
    {
      /* log packet header pointer */
      hsdpa_log_hdr_struct_type *mac_hs_hdr_log_pkt_hdr;

      /* Save length og log packet buffer */
      mac_hs_hdr_log_pkt_hdr = (hsdpa_log_hdr_struct_type*) mac_hs_hdr_log_pkt_ptr;
      mac_hs_hdr_log_pkt_hdr->len = (uint16) (sizeof(HS_MAC_HS_HDR_LOG_PKT_type) -
        (sizeof(mac_hs_hdr_log_pkt_ptr->mac_hdr_buffer) -
         dec_hs_mac_hs_hdr_log_cur_buf_info_offset));

      /* submit log packet here before proceeding */
      dec_hs_mac_hs_hdr_send_log_submit_cmd(dec_hs_mac_hs_hdr_log_cur_buf_idx);

      dec_hs_mac_hs_hdr_log_cur_buf_idx = HS_INFO_TABLE_INVALID_VAL;
      dec_hs_mac_hs_trk_cfn_log = 0;

      WL1_MSG2(HIGH, "Flushed MAC-hs header log pkt #Samples %d  Length: %d",
                     mac_hs_hdr_log_pkt_ptr->num_mac_hdr,
                     mac_hs_hdr_log_pkt_hdr->len);
    }
    else
    {
      /* Mark MAC-hs hdr log buffer to available */
      dec_hs_mac_hdr_log_buf_avail_status[dec_hs_mac_hs_hdr_log_cur_buf_idx] = TRUE;
    }
  }

  HS_LOG_INTFREE();
}

/* -------------------------------------------------------- */
/* Following functions handle HS statistics and its logging */
/* -------------------------------------------------------- */

/*===========================================================================
FUNCTION dec_hs_invalid_logging_stop_gsfn

DESCRIPTION
  invalidates stop gsfn values for logging. used when HS is torn down.
DEPENDENCIES
  None
  
RETURN VALUE
  None

SIDE EFFECTS

===========================================================================*/

void dec_hs_invalid_logging_stop_gsfn()
{
  hs_stat_final_sub_fn = INVALID_STOP_GSFN;
  hs_stat_refresh_info_flag = TRUE;
  
  hs_llr_log_final_sub_fn = INVALID_STOP_GSFN;
  hs_llr_refresh_info_flag = TRUE;

  hs_scch_log_final_sub_fn = INVALID_STOP_GSFN;
  hs_scch_refresh_info_flag = TRUE;
  
}
/*===========================================================================
FUNCTION dec_hs_stat_init

DESCRIPTION
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  All above mentioned variables in descriptions are inited
===========================================================================*/

void dec_hs_stat_init(void)
{
  /* function local variables */
  /* ------------------------ */

  /* loop index */
  uint32 index_buf;
  
  /*** function code starts here ***/

  /*by default 1st time always read the hs config.*/
  hs_stat_refresh_info_flag = TRUE;
  
  /*invalidate stop SFN for hs decode stats*/
  hs_stat_final_sub_fn = INVALID_STOP_GSFN;
  
  WL1_MSG2(HIGH, "HS Log Debug:HS log init, flag init = %d, stop sfn = %d",
                 hs_stat_refresh_info_flag, hs_stat_final_sub_fn);

  /* iterate over all log buffer status to mark them available */
  for (index_buf = 0; index_buf < HS_DECODE_STATUS_NUM_BUF; index_buf++)
  {
    hs_decode_status_log_buf_avail_status[index_buf] = TRUE;
  }

  /* Set mDSP HS stat info table index to INVALID */

  for(index_buf = 0; index_buf <L1_HSDPA_MAX_CARRIERS; index_buf++)
  {
    hs_stat_info_table_idx[index_buf]       = HS_INFO_TABLE_INVALID_VAL;
    hs_stat_start_info_table_idx[index_buf] = HS_INFO_TABLE_INVALID_VAL;
  }

  
  /* Init log submit request to FALSE */
  hs_decode_status_log_submit = FALSE;
}

/*===========================================================================
FUNCTION dec_hs_stat_do_action

DESCRIPTION
  This function changes HSDPA decode status and stat generation mode. Before
  it commit action provided function argument, it validates if that action
  can be accepted. Current action should be always NOOP that is reset at
  start and after every action completion
  
  When stat generation is inactive, only action that is accepted is START.
  When stat generation is active it can be RESTARTED or STOPPED
  
  There are 2 buffer for accumulating HS decode status. These are used when
  a existing accumulation need to be submitted for logging. It happens when
  accumulation is done or there is reconfiguration and previous configuration
  need to be flushed.
  
DEPENDENCIES
  None

RETURN VALUE
  TRUE or FALSE based on action accept validation

SIDE EFFECTS
  None
===========================================================================*/
boolean dec_hs_stat_do_action(
  /* Action for HS decode statu accumulation and stat generation */
  hs_stat_gen_action_enum_type action,
  /* Sub frame number at which to start or restart */
  uint16 start_sub_fn,
  /* start CFN to start or restart accumulation/stat generation */
  uint16 start_cfn,
  /* mDSP pointer to info table idx array, from where to read the information */
  hs_dl_channel_per_carrier_struct_type  *info_table_ptr,
  /*number of carries, used to access idx array*/
  uint8  num_carr,
  /* Last sub frame number after which information is submitted for logging
     then either restarted ot stopped based on action */
  uint16 final_sub_fn)
{
  /* function local variables */
  /* ------------------------ */

  /* indicate that previous action was pending, need to convert input action
     to realign with pending action */
  boolean action_oride_happened = FALSE;

  /* indicates whether action was accepted or not */
  boolean log_action_status = TRUE;

  /*** function code starts here ***/

  /* Validate action if it is doable */
  /* ------------------------------- */

  if (num_carr > L1_HSDPA_MAX_CARRIERS)
  {
    WL1_MSG1(ERROR, "Incorrect number of Carriers = %d", num_carr);
    return TRUE;
  }

  /* Check if this function is called with action type NOOP. There is
     no use of calling this function with action NOOP. So print MSG_ERROR
     as warning */
  if (action == HS_STAT_GEN_NOOP)
  {
    WL1_MSG0(ERROR, "HS stat do action with NOOP, ignoring");
    return TRUE;
  }

  /* Check if current action is NOOP or not. When this function is called
     it is expected that current action be NOOP */

  HS_LOG_INTLOCK();

  if (hs_pess_cqi_log_action != HS_STAT_GEN_NOOP)
  {
    action_oride_happened = TRUE;

    /* Some other action already going on, need to realign  
       older action and update the next log action appropriately */
    WL1_MSG2(ERROR, "HS state action %d still pending, new action %d, realigning",
                    hs_pess_cqi_log_action, action);

    switch (hs_pess_cqi_log_action)
    {
    case HS_STAT_GEN_START:
      if (action == HS_STAT_GEN_RESTART)
      {
        /* Change the action to start so that logging will start at the 
           start of RESLAM start */
        action = HS_STAT_GEN_START;
      }
      else if (action == HS_STAT_GEN_STOP)
      {
        /* Received a stop action before the logging could start.
           Ignore the request to start logging and return */
        action = HS_STAT_GEN_NOOP;        
      }
      break;

    case HS_STAT_GEN_RESTART:
      if (action == HS_STAT_GEN_RESTART)
      {
        /* Override the stop of new reslam with stop of old reslam 
           Logging would stop at old stop GSFN and start at new start GSFN */
        final_sub_fn = hs_pess_cqi_log_final_sub_fn;
      }
      else if (action == HS_STAT_GEN_STOP)
      {
        /* Stop logging at the old STOP time. ignore RESTART action */
        /* Change the current action to STOP */        
        action = HS_STAT_GEN_STOP;
        /* Override the stop global subframe number */        
        final_sub_fn = hs_pess_cqi_log_final_sub_fn;
      }
      else if (action == HS_STAT_GEN_START)
      {
        action = HS_STAT_GEN_RESTART;
        final_sub_fn = hs_pess_cqi_log_final_sub_fn;
      }
      break;

    case HS_STAT_GEN_STOP:
      if (action == HS_STAT_GEN_STOP)
      {
        /* Stop at the old stop CFN, ignore the new stop CFN*/
        final_sub_fn = hs_pess_cqi_log_final_sub_fn;
      }
      else
      {
        /* Previous action is STOP and the new action is start */
        /* Action is changed to reslam */
        action = HS_STAT_GEN_RESTART;
        final_sub_fn = hs_pess_cqi_log_final_sub_fn;      
      }
      break;

    default:
      WL1_MSG1(ERROR , "Invalid HS STATS gen action %d",
                       hs_pess_cqi_log_action);
      hs_pess_cqi_log_action = HS_STAT_GEN_NOOP;
    }
  }
  /* When HS stat generation is active then only expected action is STOP
     or RESTART */
  if ((action == HS_STAT_GEN_START) && hs_pess_cqi_log_accum_active)
  {
    WL1_MSG0(ERROR, "HS state action START stat gen already active");
    action = HS_STAT_GEN_NOOP;
    log_action_status = FALSE;
  }

  /* When HS stat generation is active then action type START is not
     possible */
  if ((action != HS_STAT_GEN_START) && (!hs_pess_cqi_log_accum_active))
  {
    WL1_MSG1(ERROR, "HS state action %d stat gen not active", action);
    action = HS_STAT_GEN_NOOP;
    log_action_status = FALSE;
  }

  /* information validated. Save it in global variable so that it can be
     scanned at every frame to act on it                                 */
  /* ------------------------------------------------------------------- */

    hs_pess_cqi_log_action = action;
    hs_pess_cqi_log_start_cfn = (uint8) start_cfn;
    hs_pess_cqi_log_start_sub_fn = start_sub_fn;
    hs_pess_cqi_log_final_sub_fn = final_sub_fn;


   #ifdef FEATURE_WCDMA_MIMO
   
   if (action != HS_STAT_GEN_NOOP)
   {
    /* Sst the state of next mimo demod log action */
    hs_mimo_demod_log_action = action;
    hs_mimo_demod_log_start_cfn = (uint8) start_cfn;
    hs_mimo_demod_log_start_sub_fn = start_sub_fn;
    hs_mimo_demod_log_final_sub_fn = final_sub_fn;

    hs_mimo_chan_log_action = action;
    hs_mimo_chan_log_start_cfn = (uint8) start_cfn;
    hs_mimo_chan_log_start_sub_fn = start_sub_fn;
    hs_mimo_chan_log_final_sub_fn = final_sub_fn;
   }
   #endif


  HS_LOG_INTFREE();

  return log_action_status;
}

/*===========================================================================
FUNCTION dec_hs_prep_log_action

DESCRIPTION
  HS logging start: provide valid info table ptr and Final SFN = INVALID_STOP_GSFN
  HS logging stop: provide valid SFN to stop 
DEPENDENCIES
  Hs config tables.

RETURN VALUE
  void

SIDE EFFECTS
  Max one start and stop action in 1 Radio Frame (10msec)
===========================================================================*/
void dec_hs_prep_log_action(
  /* mDSP pointer to info table idx array, from where to read the information */
  hs_dl_channel_per_carrier_struct_type  *info_table_ptr,
  /*number of carries, used to access idx array*/
  uint8  num_carr,
  /* Last sub frame number after which information is submitted for logging
   then either restarted ot stopped based on action */
  uint16 final_sub_fn)
{
  /* function local variables */
  /* ------------------------ */
  uint8 carr_idx;
  
  /* Validate action if it is doable */
  /* ------------------------------- */
  /*Mutex lock*/
  HS_LOG_INTLOCK();
    /*only  stop fn only if valid .i.e stop action */
  if(final_sub_fn !=  INVALID_STOP_GSFN)
  {
  /*hs decode stats final sfn*/
  hs_stat_final_sub_fn = final_sub_fn;
  /*scch log final sfn*/
  hs_scch_log_final_sub_fn = final_sub_fn;
  /*llr log final sfn*/  
  hs_llr_log_final_sub_fn = final_sub_fn;
  }
  /*sanity check : num carriers = [1(pri)-max carriers] (KW)*/
  if(num_carr > L1_HSDPA_MAX_CARRIERS)
  {
    ERR_FATAL("incorrect num of carriers configured from hscfg #Carr = %d",num_carr,0,0);
  }
  /*Make a different copy of info_table_idx seperate 
   from the one currently used for logging, 
   so as to not overwrite when new action comes in*/
  for(carr_idx = 0; carr_idx < num_carr; carr_idx++)
  {
    hs_stat_start_info_table_idx[carr_idx] = 
                             info_table_ptr[carr_idx].hs_info_table_idx;
  }

  WL1_MSG2(HIGH, "HS Log Debug: HS prep log action called, Stop sfn = %d, num_carr = %d",
                 hs_stat_final_sub_fn, num_carr);

  /*Mutex unlock*/
  HS_LOG_INTFREE();

}

/*===========================================================================
FUNCTION dec_hs_get_new_hs_stat_buff

DESCRIPTION
  Returns pointer to empty slot of hs_decode_status_log 
  
DEPENDENCIES
  None

RETURN VALUE
  HS_DECODE_STATUS_LOG_PKT_type

SIDE EFFECTS
  None
===========================================================================*/

HS_DECODE_STATUS_LOG_PKT_type* dec_hs_get_new_hs_stat_buff(void)
{
  uint8 idx_buf;
  
  hs_stat_info_buf_idx = HS_INFO_TABLE_INVALID_VAL;

  for (idx_buf = 0;
       idx_buf < HS_DECODE_STATUS_NUM_BUF;
       idx_buf++)
  {
    if (hs_decode_status_log_buf_avail_status[idx_buf])
    {
      hs_decode_status_log_buf_avail_status[idx_buf]  = FALSE;
      hs_stat_info_buf_idx                            = idx_buf;
      break;
    }
  }
  /* If can't get buffer then there is no use of proceeding. It should
            never happen because of double buffering */
  if (hs_stat_info_buf_idx == HS_INFO_TABLE_INVALID_VAL)
  {
    WL1_MSG0(ERROR, "Can't get HS decode stat log");

    /* iterate over all log buffer status to mark them available */
    for (idx_buf = 0; idx_buf < HS_DECODE_STATUS_NUM_BUF; idx_buf++)
    {
      hs_decode_status_log_buf_avail_status[idx_buf] = TRUE;
    }

    hs_stat_info_buf_idx = 0;
    hs_decode_status_log_buf_avail_status[hs_stat_info_buf_idx] = FALSE;

    /* Init log submit request to FALSE */
    hs_decode_status_log_submit = FALSE;
  }

 #if (WCDMA_HSLOG_DEBUG)
  WL1_MSG1(HIGH, "HS Log Debug: Get new HS stats buffer Idx = %d",
                 hs_stat_info_buf_idx);
 #endif


return   &(hs_decode_status_log[hs_stat_info_buf_idx]);
}
/*===========================================================================
FUNCTION dec_hs_get_curr_hs_stat_buff

DESCRIPTION
  Returns pointer to empty slot of hs_decode_status_log 
  
DEPENDENCIES
  None

RETURN VALUE
  HS_DECODE_STATUS_LOG_PKT_type

SIDE EFFECTS
  None
===========================================================================*/
HS_DECODE_STATUS_LOG_PKT_type* dec_hs_get_curr_hs_stat_buff(void)
{
  if(hs_stat_info_buf_idx >= HS_DECODE_STATUS_NUM_BUF)
  {
    ERR_FATAL("Invalid dec hs current hs stat buffer idx = %d",
               hs_stat_info_buf_idx,0,0);
    return NULL;
  }
  else
  {
    return   &(hs_decode_status_log[hs_stat_info_buf_idx]);
  }

}


/*===========================================================================
FUNCTION dec_hs_init_hs_log

DESCRIPTION
  Initializes the HS decode status log for recording per subframe status. Used once after fetching a new HS Decod
  status buffer.
  
DEPENDENCIES
  None

RETURN VALUE
  void

SIDE EFFECTS
  None
===========================================================================*/
void dec_hs_init_hs_log(HS_DECODE_STATUS_LOG_PKT_type *this_hs_decode_status_log,
                             uint16 stat_start_sub_fn)
{
  uint8 num_carr;
  /* Do time stamp corresponding to first sample */
  log_set_timestamp(this_hs_decode_status_log);
  
  /* init number of samples accumulated to 0 */
  this_hs_decode_status_log->num_samp   = 0;

  /*0x4222 log packet Version */
  this_hs_decode_status_log->ver        = 12;

  /* Get Multi Sim Configuration(SS/DSDS/TSTS/DSDA) info from TRM and fill in the log buffer.
   * This info is required for every log packet, once in every 50ms
   */
  #if defined(FEATURE_DUAL_SIM) && defined(FEATURE_QTA)
  this_hs_decode_status_log->multi_sim_conf = wl1_ds_query_trm_multisim_config_info();
  #else
  this_hs_decode_status_log->multi_sim_conf = 0;
  #endif
  
  /*Save starting sub frame number */
  this_hs_decode_status_log->strt_sf    = stat_start_sub_fn;

  /* Num of carriers configured*/
  /* 0: 1C, 3: 4C*/
  this_hs_decode_status_log->num_car = (dec_hs_call_info.num_carr -1);          
  num_carr                           = dec_hs_call_info.num_carr;          

  if (dec_hs_call_info.num_carr > L1_HSDPA_MAX_CARRIERS)
  {
    WL1_MSG1(ERROR, "Incorrect number of Carriers = %d",
                    dec_hs_call_info.num_carr);
    return;
  }          

  if (MAC_HS == dec_hs_call_info.hs_or_ehs)
  {
    this_hs_decode_status_log->mac_hs_ehs = 0;
  }
  else if (MAC_EHS == dec_hs_call_info.hs_or_ehs)
  {
    this_hs_decode_status_log->mac_hs_ehs = 1;
  }

  if (hs_query_e_fach_state())
  {
    /*Set to 1: FACH state */
    this_hs_decode_status_log->wl1_state = HS_LOG_DECODE_STATUS_STATE_IND_VAL_FACH;
  }
  else
  {
    /*else set to 0: DCH state */
    this_hs_decode_status_log->wl1_state = HS_LOG_DECODE_STATUS_STATE_IND_VAL_DCH;
  }
  /* get ue cat*/
  this_hs_decode_status_log->cmn_info.ue_cat    = hs_get_ue_category();

  /* 0 - 1 HARQ process, 7 - 8 Harq process, hence subtract one from configured.*/
  this_hs_decode_status_log->cmn_info.num_harq  = dec_hs_call_info.num_harq_configured - 1;

  /*fall through till C0*/
  switch(num_carr)
  {
   #ifdef FEATURE_WCDMA_4C_HSDPA
    case 4: /*populating c3 - C0 carriers*/   
    /*TB size octet align*/
    this_hs_decode_status_log->cmn_info.c3_tb_size_octet_align =  
                                          dec_hs_call_info.hsdsch_tb_size_alignment_type[3];

    /*64QAM Config*/
    this_hs_decode_status_log->cmn_info.c3_64qam_config = 
                                                    dec_hs_call_info.hs_64qam_configured[3];
   #endif
   #ifdef FEATURE_WCDMA_3C_HSDPA
    case 3: /*populating c2 - C0 carriers*/      
    /*TB size octet align*/
    this_hs_decode_status_log->cmn_info.c2_tb_size_octet_align =  
                                          dec_hs_call_info.hsdsch_tb_size_alignment_type[2];

    /*64QAM Config*/
    this_hs_decode_status_log->cmn_info.c2_64qam_config = 
                                                    dec_hs_call_info.hs_64qam_configured[2];
   #endif
    case 2: /*populating c1 - C0 carriers*/      
    /*TB size octet align*/
    this_hs_decode_status_log->cmn_info.c1_tb_size_octet_align =  
                                          dec_hs_call_info.hsdsch_tb_size_alignment_type[1];

    /*64QAM Config*/
    this_hs_decode_status_log->cmn_info.c1_64qam_config = 
                                                    dec_hs_call_info.hs_64qam_configured[1];
    
    case 1:    /*populating c0 carrier*/   
    /*TB size octet align*/
    this_hs_decode_status_log->cmn_info.c0_tb_size_octet_align =  
                                          dec_hs_call_info.hsdsch_tb_size_alignment_type[0];

    /*64QAM Config*/
    this_hs_decode_status_log->cmn_info.c0_64qam_config = 
                                                    dec_hs_call_info.hs_64qam_configured[0];
    break;
    
    default:/*ERROR*/
      WL1_MSG1(ERROR, "Incorrect number of Carriers = %d", num_carr);
      
  }
    
     #if (WCDMA_HSLOG_DEBUG)
      WL1_MSG2(HIGH, "HS Log Debug: Get new HS stats buffer Init, start_SFN = %d, num_carr = %d",
                     stat_start_sub_fn, num_carr);
     #endif
}
/*===========================================================================
FUNCTION dec_hs_decode_update_stats

DESCRIPTION
  This function is called periodically every frame in Maintence Event handler 
  to read HS logs from FW buffer.
  
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  rd_idx != wr_idx blindly depending on FW. Wrong state of rd_idx & wr_idx 
  could lead to unexpected behaviour
===========================================================================*/


void dec_hs_decode_update_stats(boolean force_flush)
{
  /* function local variables */
  /* ------------------------ */
  WfwDbackHsDecodeStatusLogFifoStruct *fw_hs_decode_status_log_fifo_ptr;
  uint16 global_sub_fr_num = 0;
  uint8 wr_idx;
  uint8 rd_idx ; 

  /*** function code starts here ***/
  /*Mutex lock starting*/
  /*Mutex protection � as WL1 command processing and 
    DCH Maintenance Event ISR can run in parallel, make 
    sure that the logging update functions for all log 
    packets are covered under this mutex.*/
  HS_LOG_INTLOCK();
  
  /*Alias for FW-SW interface decode status log FIFO */
  fw_hs_decode_status_log_fifo_ptr = &(mcalwcdma_fwsw_intf_addr_ptr->dbackHsDecodeStatusLog);
  
  /* Read the write and read idx of hs decode status log from FW*/    
  wr_idx = fw_hs_decode_status_log_fifo_ptr->dbackHsDecodeStatusLogWrPtr;
  rd_idx = fw_hs_decode_status_log_fifo_ptr->dbackHsDecodeStatusLogRdPtr;
  
  /*sanity check - take care of KW errors*/
  if(wr_idx >= WFW_DEMOD_HS_DEC_STATUS_LOG_PACKET_BUF_SIZE
   || rd_idx >= WFW_DEMOD_HS_DEC_STATUS_LOG_PACKET_BUF_SIZE)
  {
    ERR_FATAL("FW Read and Write Idx greater than buffer size",0,0,0);
    return;
  }

  
#if (WCDMA_HSLOG_DEBUG)
  /* Sub frame number when log was captured*/
  global_sub_fr_num = fw_hs_decode_status_log_fifo_ptr\
                ->WfwDbackHsDecodeStatusLogEntry[rd_idx].gSubFn;

  WL1_MSG3(HIGH, "HS Log Debug:Populating log, Carr_loop, for Rd_idx = %d, wr_idx = %d, sfn = %d",
                            rd_idx,wr_idx,global_sub_fr_num);
   
#endif

/*For case when FW ISR to STOP comes after logging function is called 
      and Rd_idx has already caught up with wr_idx */
  rd_idx = rd_idx-1;
  if(rd_idx > WFW_DEMOD_HS_DEC_STATUS_LOG_PACKET_BUF_SIZE) 
  {
    rd_idx = WFW_DEMOD_HS_DEC_STATUS_LOG_PACKET_BUF_SIZE-1;
  }
  /*read the last read Buffer and compare the STOP SFN*/
  global_sub_fr_num = fw_hs_decode_status_log_fifo_ptr\
                ->WfwDbackHsDecodeStatusLogEntry[rd_idx].gSubFn;

  /*only enters here if the 1st stop hasn't been executed*/
  if(global_sub_fr_num == hs_stat_final_sub_fn)
  {
    MSG_HIGH("HS Log Debug: Stopping Hs logging",0,0,0);
    /*invalidate*/
    hs_stat_final_sub_fn = INVALID_STOP_GSFN; //INVALID SFN
    hs_stat_refresh_info_flag= TRUE;

    /*stop and flush hs log packet*/
    dec_hs_logging_stop();
	
    /*Force flush the LLR log packet since 
      the stop subframe was missed in the last run*/
    force_flush = TRUE;
  }
   
  /* Read the write and read idx of hs decode status log from FW*/    
  wr_idx = fw_hs_decode_status_log_fifo_ptr->dbackHsDecodeStatusLogWrPtr;
  rd_idx = fw_hs_decode_status_log_fifo_ptr->dbackHsDecodeStatusLogRdPtr;
  
  /*read till w_idx of the circular buffer*/
  while(rd_idx != wr_idx)
  {
    
    /* Sub frame number when log was captured*/
    global_sub_fr_num = fw_hs_decode_status_log_fifo_ptr\
                 ->WfwDbackHsDecodeStatusLogEntry[rd_idx].gSubFn;
    
    /*initialize hs log buffer, querying HS config once after stop */
    if(TRUE == hs_stat_refresh_info_flag)
    {
      dec_hs_logging_init(global_sub_fr_num);
      hs_stat_refresh_info_flag = FALSE;

    }
    
      
    /*Populate HS log packet*/
    dec_hs_log_populate(&(fw_hs_decode_status_log_fifo_ptr\
                          ->WfwDbackHsDecodeStatusLogEntry[rd_idx]));

    
    if(global_sub_fr_num == hs_stat_final_sub_fn)
    {
      WL1_MSG1(HIGH, "HS Log Debug: Stopping Hs logging SFN %d",
                     global_sub_fr_num);
      /*invalidate*/
      hs_stat_final_sub_fn = INVALID_STOP_GSFN; //INVALID SFN
      hs_stat_refresh_info_flag= TRUE;

      /*stop and flush hs log packet*/
      dec_hs_logging_stop();
      
    }

    /*~~increment and wrap around buffer size~~*/ 
    rd_idx = (rd_idx + 1)% WFW_DEMOD_HS_DEC_STATUS_LOG_PACKET_BUF_SIZE;
    
  }/*End of While (r_idx != w_idx)*/
  /* Do logging for HS-SCCH before updating  read index in FW-SW FIFO*/
  dec_hs_scch_log_update();

  /*~~~Update FW-SW decode Stats Log Fifo Read Idx ~~~*/
  /*This handles both **HS SCCH log** and **HS Decode stats** */
  fw_hs_decode_status_log_fifo_ptr->dbackHsDecodeStatusLogRdPtr = rd_idx;

  
  /* Do the HS amp and eng est logging  and pass force_flush flag*/
  dec_hs_llr_update_log(force_flush);

  /* Do logging for pessimistic CQI */
  dec_hs_pess_cqi_log_update();

  /*Mutex free*/
  /*Mutex protection � as WL1 command processing and 
    DCH Maintenance Event ISR can run in parallel, make 
    sure that the logging update functions for all log 
    packets are covered under this mutex.*/
  HS_LOG_INTFREE(); 
 return;  
}

/*===========================================================================
FUNCTION dec_hs_logging_init

DESCRIPTION Read Call info from hs config and upates a local call info structrue, 
                   for populating 0x4222 log packet. 
                   Only called when HS channel is stopped or reconfigured.
 
DEPENDENCIES 
 

RETURN VALUE
  void

SIDE EFFECTS
 
===========================================================================*/

void dec_hs_logging_init(uint16 start_sub_fn)
{
  uint8 carr_idx = 0,
        num_carr = 0,
        harq_id = 0;
  HS_DECODE_STATUS_LOG_PKT_type *this_hs_decode_status_log;
  
  for(carr_idx = 0; carr_idx < L1_HSDPA_MAX_CARRIERS; carr_idx++)
  {
    /*Make a another copy for logging so as not over write during dec_hs_prep_log_action*/
    hs_stat_info_table_idx[carr_idx] = hs_stat_start_info_table_idx[carr_idx];
  }
  /* Query call info from HS module this is used by all the other log packets
   Hence 0x4222 logging must be done before any other DL HS logging*/
  hs_get_call_info(&dec_hs_call_info, hs_stat_info_table_idx[0]); 
  /* Get available HS status accumulation buffer */
  this_hs_decode_status_log = dec_hs_get_new_hs_stat_buff();

  /* initialize the log for per sub frame logging, common info across sub-frames is populated here*/
  dec_hs_init_hs_log(this_hs_decode_status_log,start_sub_fn);

  /* Num of carriers configured
       0: 1 carrier, 3: 4 carriers */
  num_carr = this_hs_decode_status_log->num_car + 1;          

  /* init HS dowmlink stats generation */
  dec_hs_init_stats();
  mcalwcdma_dec_hs_reset_stats();

  dec_hs_num_new_tx_rece = 0;
  dec_hs_new_tx_in_err = 0;
  /* for each carrier */
  for (carr_idx = 0; carr_idx < num_carr; carr_idx++)
  {
    /* init HARQ process Id stats */
    for  (harq_id = 0; harq_id < L1_DL_HS_DSCH_HARQ_MAX_PROC; harq_id++)
    {
      hs_dsch_harq_proc_stat_info[carr_idx][harq_id].ack_received = TRUE;
    }
  }
  
  WL1_MSG3(HIGH, "HS Log Debug: Starting HS decode status, info tbl_idx = %d SubFn = %d , #Carr  = %d",
           hs_stat_info_table_idx[0],
           start_sub_fn,
           num_carr);

 return;
}

/*===========================================================================
FUNCTION dec_hs_log_populate

DESCRIPTION Populates SW hs decode stats log buffer from FW log buffer. Populates one Sub frame worth of stats.
  
DEPENDENCIES
  none

RETURN VALUE
  void

SIDE EFFECTS
  
===========================================================================*/
void dec_hs_log_populate(WfwDbackHsDecodeStatusLogEntryStruct *fw_hs_decode_status_per_sf_ptr)
{
  
  /* get previous frame starting sub frame number */
  uint16 global_sub_fr_num;
  
  uint8 num_carr,
        carr_idx,
        num_log_samp;
  
  uint8 gsfn_idx,
        harq_idx,
        temp_idx;
 
  /* Loop indices */
  uint32 index_reorder_pdu;
  /* variable used to stored temp val after extracting field from
            read mDSP buffer shadow */
  uint16 temp_val;
  /* Values of SCCH demod attempted, valid and DSCHstatus */
  uint16 scch_demod_attempted,
         scch_demod_valid    ,
         dsch_status = 0     ;
 /* Flag to indicate if detailed info has to be logged for a subframe */ 
  boolean detailed_info;

  /*Pointer to FW HS decode log structure for each carrier*/
  WfwDbackHsDecodeStatusLogStruct *hs_decode_status_mdsp_log_ptr  = NULL;
   HS_DECODE_STATUS_LOG_PKT_type  *this_hs_decode_status_log = NULL;
  /* Pointer to HS decode status log buffer */
  hs_info_per_sf_st * hs_info_per_subfr_log_ptr;
  dec_hs_decode_status_struct_type decode_status = {0};

  /* Sub frame number when log was captured*/
  global_sub_fr_num = fw_hs_decode_status_per_sf_ptr->gSubFn;
  
  this_hs_decode_status_log = dec_hs_get_curr_hs_stat_buff();
  
  /*sanity check (KW)*/
  if(NULL == this_hs_decode_status_log)
  {
    WL1_MSG0(FATAL, "current hs stat buff returned as NULL");
    return;
  }
  /*0: 1 carrier,... 3: 4 carriers*/
  num_carr = this_hs_decode_status_log->num_car + 1;  

  /*used in dec_hs_gen_stats( &decode_status ) */    
  decode_status.num_carr    = num_carr;
  
  /* If acumulation has reached max samples current log buffer must be submitted and new buffer to be used*/
  /* --------------------------------------------------------------------------------------- */
  if (this_hs_decode_status_log->num_samp == HS_DECODE_STATUS_LOG_MAX_SAMPLE)
  {
    
    /*Total size of the HS log buffer - #used HS info per sf buffer*/
    num_log_samp = this_hs_decode_status_log->num_samp * num_carr;

    /* Get header to current log buffer and update total size of
                information accumulated */
    this_hs_decode_status_log->hdr.len = sizeof(HS_DECODE_STATUS_LOG_PKT_type)
                                       - (HS_LOG_MAX_NUM_SAMPLES - num_log_samp)
                                       *  sizeof(hs_info_per_sf_st);                                    

#if(WCDMA_HSLOG_DEBUG)
    WL1_MSG3(HIGH, "HS Log Debug: Buffer idx = %d reached HS LOG MAX NUM SAMPLES,pkt len = %d, buff len =%d",
                                        hs_stat_info_buf_idx,
                                        this_hs_decode_status_log->hdr.len,
                                        sizeof(HS_DECODE_STATUS_LOG_PKT_type));
#endif        

    /* submit this log packet. */
    dec_hs_decode_status_send_log_submit_cmd(
      hs_stat_info_buf_idx);

    /* Get New HS status accumulation buffer */
    this_hs_decode_status_log = dec_hs_get_new_hs_stat_buff();

    /* Initalize log for per subframe logging*/
    dec_hs_init_hs_log(this_hs_decode_status_log,global_sub_fr_num);

  } /* end if, max decode status samples have been collected */
  
  for(carr_idx = 0; carr_idx < num_carr; carr_idx++)
  {
    
#if (WCDMA_HSLOG_DEBUG)
   WL1_MSG3(HIGH, "HS Log Debug: Populating log, Carr_loop, for SFN = %d, carr# = %d, num_carr = %d",
                           global_sub_fr_num,carr_idx,dec_hs_call_info.num_carr);

#endif
    /*Reset Detailed info flag*/
    detailed_info = FALSE;

    /* sample buffer idx to be used*/
    num_log_samp = (this_hs_decode_status_log->num_samp * num_carr) + carr_idx;

    if (num_log_samp >= HS_LOG_MAX_NUM_SAMPLES)
    {
       WL1_MSG1(ERROR, "Invalid number of samples: %d", num_log_samp);
       this_hs_decode_status_log->num_samp = HS_DECODE_STATUS_LOG_MAX_SAMPLE - 1;
       break;
    }

    /* pointer to HS Decode info per Carrier per Sub frame buffer*/
    hs_info_per_subfr_log_ptr = \
                 &(this_hs_decode_status_log->hs_info_per_sf[num_log_samp]);
    /* Reset log buff */
    memset(hs_info_per_subfr_log_ptr, 0x0, sizeof(hs_info_per_sf_st));

    /* initialize the mdsp log ptr from fw-sw interface address pointer*/
    hs_decode_status_mdsp_log_ptr= \
     &(fw_hs_decode_status_per_sf_ptr->WfwDbackHsDecodeStatusLogPerCarrier[carr_idx]);
    
    /* Save SCCH demodulation status for each configured carriers*/        
    scch_demod_attempted = hs_decode_status_mdsp_log_ptr->scchDemod;
    scch_demod_valid     = hs_decode_status_mdsp_log_ptr->scchValid;

    decode_status.scch_demod_attempted[carr_idx] = (boolean)scch_demod_attempted;
  
    if ((!scch_demod_attempted) && (!scch_demod_valid)) 
    {
      /* Neither attempted nor valid */
      dsch_status                = HS_LOG_DECODE_STATUS_DSCH_STATUS_DTX;
      decode_status.scch_valid[carr_idx]   = FALSE; 
      decode_status.dsch_status[carr_idx]  = FALSE;
      decode_status.new_tx[carr_idx]       = FALSE;
      detailed_info                        = FALSE;
    }
    else if(scch_demod_attempted)
    {
      if(scch_demod_valid)
      {
        /* SCCH passes CRC and is valid too 
                    Use the detailed info */
        detailed_info                      = TRUE;          
        decode_status.scch_valid[carr_idx] = TRUE;
        
        switch(hs_decode_status_mdsp_log_ptr->ackNack)
        {
          case HS_DECODE_STATUS_DSCH_STATUS_ACK: 
                  dsch_status = HS_LOG_DECODE_STATUS_DSCH_STATUS_CRC_PASS; // TRUE
                  decode_status.dsch_status[carr_idx] = TRUE;
                  break;

          case HS_DECODE_STATUS_DSCH_STATUS_AUTO_ACK:
                  /* APEX will automatically mark this as DUP */
                  dsch_status = HS_LOG_DECODE_STATUS_DSCH_STATUS_CRC_PASS;
                  decode_status.dsch_status[carr_idx] = TRUE;
                  break;

          case HS_DECODE_STATUS_DSCH_STATUS_NACK: 
                  dsch_status = HS_LOG_DECODE_STATUS_DSCH_STATUS_CRC_FAIL;
                  decode_status.dsch_status[carr_idx] = FALSE;
                  break;

          default: WL1_MSG0(ERROR, "Invalid value of AcKnack from FW");
                   break;
                  
        }
        
      }
      else
      {
        /* SCCH valid is 0, could be because of CRC fail or validity fail */
        temp_val    = hs_decode_status_mdsp_log_ptr->pdschTbs0;
        dsch_status= HS_LOG_DECODE_STATUS_DSCH_STATUS_DTX;
    
        decode_status.scch_valid[carr_idx]   = FALSE;
        decode_status.dsch_status[carr_idx]  = FALSE;

        /* F/W populates the following values in the TBS field when SCCH valid == 0
                  0xFFFF - SCCH CRC fails
                  0xF000 - SCCH part1 fails */
        
        if(0xFFFF == temp_val)
        {
          /* SCCH CRC check failed, no detailed info */
          detailed_info = FALSE;
        }
        else
        {
          /* SCCH CRC passed, validity failed. Do populate the detailed info */
          detailed_info = TRUE;
        }
      }
    }
    else
    {
      WL1_MSG0(ERROR, "Wrong combination of SCCH demod and valid");
      dsch_status= HS_LOG_DECODE_STATUS_DSCH_STATUS_DTX;
      decode_status.scch_valid[carr_idx] = FALSE;
      decode_status.dsch_status[carr_idx]= FALSE;
      detailed_info = FALSE;        
    }
    
    /* Save new transmission value */
    decode_status.new_tx[carr_idx]       = (boolean)hs_decode_status_mdsp_log_ptr->newTx0;
    /* HARQ ID for primary data stream */
    decode_status.harq_proc_id[carr_idx] = hs_decode_status_mdsp_log_ptr->scchPt2Xhap0;
    

    if(TRUE == detailed_info)
    {
      /* HARQ ID for primary data stream */
      decode_status.harq_proc_id[carr_idx] = hs_decode_status_mdsp_log_ptr->scchPt2Xhap0;

      /* Save HS transport block size */
      temp_val                   = hs_decode_status_mdsp_log_ptr->pdschTbs0;

    #ifdef FEATURE_WCDMA_DYNAMIC_FING_GRAN 
      #error code not present
#endif /* FEATURE_WCDMA_DYNAMIC_FING_GRAN */

      if (decode_status.new_tx[carr_idx])
      {
        /* Overwrite the last valid TB size at new Tx always.
                    This can be 0 also (should not be in valid case) */
        hs_last_valid_tbs[0][decode_status.harq_proc_id[carr_idx]] = temp_val;
      }
      else
      {
        /* If the TBS is 0, use the last valid saved TBS */
        if (temp_val)
        {
          hs_last_valid_tbs[0][decode_status.harq_proc_id[carr_idx]] = temp_val;
        }
        else
        {
          temp_val = hs_last_valid_tbs[0][decode_status.harq_proc_id[carr_idx]];
        }
      }

      decode_status.tb_size[carr_idx] = temp_val;

    }
  
    /* RxD enabled on Rake */
    hs_info_per_subfr_log_ptr->rxd_ena_on_rake = hs_decode_status_mdsp_log_ptr->rakeRxdEn;
    /* RxD enabled on QICE */
    hs_info_per_subfr_log_ptr->rxd_ena_on_qice = hs_decode_status_mdsp_log_ptr->qiceRxdEn;
    
    /*Rake or Q-ICE*/
    switch(hs_decode_status_mdsp_log_ptr->demodPath)
    {
      /*RAKE*/
      case 0 : 
        hs_info_per_subfr_log_ptr->demfnt = 0;
        break;

      /*EQ*/
      case 1 : 
      /*QICE*/  
      case 2 : 
        hs_info_per_subfr_log_ptr->demfnt = 1;
        break;

      /*error*/
      default: 
        WL1_MSG0(ERROR, "Incorrect DemodPath");
    }   

     /*CQI*/
    hs_info_per_subfr_log_ptr->rake_cqi  = hs_decode_status_mdsp_log_ptr->rakeCqiQ4;
    hs_info_per_subfr_log_ptr->qice_cqi  = hs_decode_status_mdsp_log_ptr->qIceCqiQ4;

    /*RAKE, Q-ICE, EQ enable flags*/
    hs_info_per_subfr_log_ptr->eq_en     = hs_decode_status_mdsp_log_ptr->eqEnb;
    hs_info_per_subfr_log_ptr->qice_en   = hs_decode_status_mdsp_log_ptr->qiceEnb;

    /*SET scch order flag*/
    hs_info_per_subfr_log_ptr->hs_scch_ord  = hs_decode_status_mdsp_log_ptr->scchOrder;

    /*SCCH attempt and valid*/
    hs_info_per_subfr_log_ptr->scch_attempt = hs_decode_status_mdsp_log_ptr->scchDemod;
    hs_info_per_subfr_log_ptr->scch_valid   = hs_decode_status_mdsp_log_ptr->scchValid;

    /*DSCH status*/
    hs_info_per_subfr_log_ptr->dsch_status  = dsch_status;


    /*Turbo Decode Early termination*/
    hs_info_per_subfr_log_ptr->td_early_term_en  = hs_decode_status_mdsp_log_ptr->tdEarlyTerminationEn;
    /* Log detailed info only if SCCH is valid or SCCH CRC passes but validity fails*/
    if(TRUE == detailed_info)
    {
        
      /* Populate SCCH part1 info fields */
      if (hs_decode_status_mdsp_log_ptr->scchOrder)
      {
        /*This is SCCH Order*/
        scch_info_ord_en_st *scch_info_ord_en = &hs_info_per_subfr_log_ptr->scch_info_ord_en;
        
        /* LOG SCCH order info*/
        scch_info_ord_en->scch_ord   = hs_decode_status_mdsp_log_ptr->order;
        scch_info_ord_en->scch_ord_t = hs_decode_status_mdsp_log_ptr->orderType;
        
      }
      else
      {
        /*This is not SCCH order*/
        scch_info_ord_dis_st *scch_info_ord_dis = &hs_info_per_subfr_log_ptr->scch_info_ord_dis;

        /*LOG SCCH info*/
        scch_info_ord_dis->code_offset  = hs_decode_status_mdsp_log_ptr->codeOffset;
        scch_info_ord_dis->num_code     = hs_decode_status_mdsp_log_ptr->numCodes;
        scch_info_ord_dis->mod_scheme   = hs_decode_status_mdsp_log_ptr->modScheme0;
        scch_info_ord_dis->harq_id      = hs_decode_status_mdsp_log_ptr->scchPt2Xhap0;
        scch_info_ord_dis->xrv          = hs_decode_status_mdsp_log_ptr->scchPt2Xrv0;
        scch_info_ord_dis->ndi          = hs_decode_status_mdsp_log_ptr->newTx0;
      }

      if (hs_decode_status_mdsp_log_ptr->tdEarlyTerminationEn)
      {
        /*Turbo Decoder Early termination enabled
                    Populate Turbo decoder info */
        
        hs_info_per_subfr_log_ptr->td_early_term_info.itr     = hs_decode_status_mdsp_log_ptr->numFullIteration;
        hs_info_per_subfr_log_ptr->td_early_term_info.min_llr = hs_decode_status_mdsp_log_ptr->minLlr;
        
      }

      /*Primary Transport Block Size (FW interpretation)*/
      hs_info_per_subfr_log_ptr->fw_tbsize = hs_decode_status_mdsp_log_ptr->pdschTbs0;
      
      /* Save winning SCCH channel index */
      hs_info_per_subfr_log_ptr->scchidx   = hs_decode_status_mdsp_log_ptr->winningScch;
    #ifdef FEATURE_WCDMA_64QAM
      /* If 64QAM is configured, then the SCCH indexing starts at 1 (1..4)
                 Firmware still reports like legacy HS (0..3). Hence add 1 to the 
                 value reported by firmware. */
      if (dec_hs_call_info.hs_64qam_configured[carr_idx])
      {
        hs_info_per_subfr_log_ptr->scchidx += 1;
      }
    #endif

      /*get Idx for pdu buffer from Global SF num*/
      gsfn_idx = global_sub_fr_num % HS_DECODE_DSCH_TB_DATA_BIT_BUF_LEN;
 
      if(MAC_EHS == dec_hs_call_info.hs_or_ehs )
      {
          
        /*assign pointer for easy readability*/
        dsch_info_mac_ehs_st *dsch_info_mac_ehs_ptr;   
        dsch_info_mac_ehs_st *hs_reord_pdu_info_prt;
        temp_idx = hs_stat_info_table_idx[carr_idx];
        
      #ifdef FEATURE_WCDMA_HS_FACH
        if(0 == carr_idx)/*BCCH only on Primary Carrier and for MAC ehs*/
        {
          hs_info_per_subfr_log_ptr->bcch_indic =  hs_decode_status_mdsp_log_ptr->hrntiType;
          WL1_MSG2(LOW, "bcch ind %d scch_info 0x%x",
                  hs_info_per_subfr_log_ptr->bcch_indic,
                        *((uint32*)(&(hs_info_per_subfr_log_ptr->scch_info_ord_dis))));
        }
      #endif
      
        harq_idx              = hs_decode_status_mdsp_log_ptr->scchPt2Xhap0;
        hs_reord_pdu_info_prt = &hs_decode_status_reord_pdu_info[temp_idx][gsfn_idx][harq_idx];
        dsch_info_mac_ehs_ptr = &hs_info_per_subfr_log_ptr->dsch_info_mac_ehs;

        /*Populate Number of reodered SDU*/
        dsch_info_mac_ehs_ptr->num_reorder_sdu = hs_reord_pdu_info_prt->num_reorder_sdu;

        if ((0 == dsch_info_mac_ehs_ptr->num_reorder_sdu) && 
             decode_status.dsch_status[carr_idx])
        {
           
            WL1_MSG3(ERROR, "num reorder SDU == 0 for primary GSFN %d harq %d idx %d",
                       global_sub_fr_num, harq_idx, temp_idx);
        }

        /*Populate each reoder PDU info*/
        for (index_reorder_pdu = 0; 
             index_reorder_pdu < MCALWCDMA_DEC_HS_REORDER_PDU_MAX_COUNT;
             index_reorder_pdu++)
        {

         (dsch_info_mac_ehs_ptr->reorder_pdu_info[index_reorder_pdu]) = 
                                   hs_reord_pdu_info_prt->reorder_pdu_info[index_reorder_pdu];
        }

      }
      else if(MAC_HS == dec_hs_call_info.hs_or_ehs)
      {

        temp_idx = hs_stat_info_table_idx[carr_idx];

        /*typecasting to strcuture*/
        hs_info_per_subfr_log_ptr->dsch_info_mac_hs = *(dsch_info_mac_hs_st *)\
                                              &(hs_decode_status_tb_blk_data[temp_idx][gsfn_idx]);
      }
    }
  
    /* DL Desense/QTA presence in 30bpgs of a subframe .*/
    hs_info_per_subfr_log_ptr->dl_desense_qta_bpgs = hs_decode_status_mdsp_log_ptr->rxBlankBmsk;
    /* DL Desense flag */
    hs_info_per_subfr_log_ptr->desense_flag = hs_decode_status_mdsp_log_ptr->dlDeneseflag;
  
    switch (hs_decode_status_mdsp_log_ptr->demodPath)
    {
      case 0:
      {
        decode_status.quan_cqi[carr_idx]= 
                                 dec_hs_quantize_cqi(hs_decode_status_mdsp_log_ptr->rakeCqiQ4);
        break;
      }
      case 1:
      {
        decode_status.quan_cqi[carr_idx] = 
                                 dec_hs_quantize_cqi(hs_decode_status_mdsp_log_ptr->eqCqiQ4);
        break;
      }
      case 2:
      {
        decode_status.quan_cqi[carr_idx] = 
                                 dec_hs_quantize_cqi(hs_decode_status_mdsp_log_ptr->qIceCqiQ4);
        break;
      }

      default: break;
        
    }

  }/*For each carrier*/  

  /* Increment number of samples accumulated */
  this_hs_decode_status_log->num_samp++;
  
  /* generate HS DL stats with this HS decode status sample */
  dec_hs_gen_stats(&decode_status);
  
  if (hs_scch_stats_log.num_sub_frames[0] >= HS_STAT_LOG_MAX_SAMPLE)
  {
    /* commit HS downlink stats to logging */
    dec_hs_submit_stats();
  } /* end if, max stat samples have been collected */

return;   
} /* end of while read idx and write idx are not equal*/ 

/*===========================================================================
FUNCTION dec_hs_logging_stop

DESCRIPTION
  Submit current log buffer to diag, by posting a local command.
  Called during HS stop or reconfig.
DEPENDENCIES
 
RETURN VALUE
  void

SIDE EFFECTS
 
===========================================================================*/

void dec_hs_logging_stop()
{
  uint8 num_carr;
  uint8 num_log_samp;
  uint8 carr_idx;
  HS_DECODE_STATUS_LOG_PKT_type *this_hs_decode_status_log= NULL;

  /*get current log buffer*/
  this_hs_decode_status_log = dec_hs_get_curr_hs_stat_buff();
  /*sanity check (KW)*/
  if(NULL == this_hs_decode_status_log)
  {
    WL1_MSG0(FATAL, "current hs stat buff returned as NULL");
    return;
  }
  /*0: 1 carrier,... 3: 4 carriers*/
  num_carr = this_hs_decode_status_log->num_car + 1;
  
  /* Get header to current log buffer and update total size of
               information accumulated */
  /*Total size of the HS log buffer - #unused HS info per sf buffer*/
  num_log_samp = this_hs_decode_status_log->num_samp * num_carr;

  this_hs_decode_status_log->hdr.len = sizeof(HS_DECODE_STATUS_LOG_PKT_type) 
                                       - (HS_LOG_MAX_NUM_SAMPLES - num_log_samp)
                                       * sizeof(hs_info_per_sf_st);

  /* send command to submit this decode status log packet */
  dec_hs_decode_status_send_log_submit_cmd( hs_stat_info_buf_idx );

  /* commit HS downlink stats to logging */
  dec_hs_submit_stats();

  /* if action was reconfig then change it to start. or
     if it was stop then it is al done, mark it NOOP */
  for(carr_idx = 0; carr_idx < L1_HSDPA_MAX_CARRIERS; carr_idx++)
  {
    hs_stat_info_table_idx[carr_idx] = HS_INFO_TABLE_INVALID_VAL;
  }

  return;
}

/*===========================================================================
FUNCTION dec_hs_init_stats

DESCRIPTION
  This function initialize stats information.
  
  It is called when ever HS stat generation is started at START or RESTART
  action OR
  when ever stats have been generated using max required number of samples
  
DEPENDENCIES
  None

RETURN VALUE
  5 Bit unquantized value form 1 to 30

SIDE EFFECTS
  None
===========================================================================*/

void dec_hs_init_stats(void)
{
  /* function local variables */
  /* ------------------------ */

  /* pointer to log packet header */
  uint32 index_proc;
  uint8  idx_carr;
  hsdpa_log_hdr_struct_type *hs_stats_log_pkt_hdr;
  /* log packet pointer that need to be submitted */
  HS_SCCH_STAT_LOG_PKT_type *this_hs_scch_stats_log;
  HS_DSCH_HARQ_STAT_LOG_PKT_type *this_hs_harq_stats_log;
  HS_CQI_STAT_LOG_PKT_type *this_hs_cqi_stats_log;

  /*** function code starts here ***/

  /* get log packet pointer */
  this_hs_scch_stats_log = &hs_scch_stats_log;
  this_hs_harq_stats_log = &hs_harq_stats_log;
  this_hs_cqi_stats_log = &hs_cqi_stats_log;

  /* init log code and number of frame used in all stat log buffers to 0 */
  memset((void*) &(hs_scch_stats_log), 0x00, sizeof(hs_scch_stats_log));
  hs_stats_log_pkt_hdr = (hsdpa_log_hdr_struct_type*) this_hs_scch_stats_log;
  hs_stats_log_pkt_hdr->code = HS_SCCH_STAT_LOG_PKT;
  log_set_timestamp(hs_stats_log_pkt_hdr);

  memset((void*) &(hs_harq_stats_log), 0x00, sizeof(hs_harq_stats_log));
  hs_stats_log_pkt_hdr = (hsdpa_log_hdr_struct_type*) this_hs_harq_stats_log;
  hs_stats_log_pkt_hdr->code = HS_DSCH_HARQ_STAT_LOG_PKT;
  log_set_timestamp(hs_stats_log_pkt_hdr);

   /* set the log packet version information*/ 
  hs_harq_stats_log.version = 5;

  for (idx_carr = 0; idx_carr < L1_HSDPA_MAX_CARRIERS; idx_carr++)
  {
  for (index_proc = 0;
       index_proc < L1_DL_HS_DSCH_HARQ_MAX_PROC;
       index_proc++)
  {
      hs_harq_proc_id_info_idx[idx_carr][index_proc] = HS_INFO_TABLE_INVALID_VAL;
      hs_last_valid_tbs[idx_carr][index_proc]        = 0;

    }
  }
  memset((void*) &(hs_cqi_stats_log), 0x00, sizeof(hs_cqi_stats_log));
  hs_stats_log_pkt_hdr = (hsdpa_log_hdr_struct_type*) this_hs_cqi_stats_log;
  hs_stats_log_pkt_hdr->code = HS_CQI_STAT_LOG_PKT;
  log_set_timestamp(hs_stats_log_pkt_hdr);  
  
 #if (WCDMA_HSLOG_DEBUG)
  WL1_MSG0(HIGH, "HS Log Debug: dec HS init stats");
 #endif

  
}

/*===========================================================================
FUNCTION dec_hs_gen_stats

DESCRIPTION
  This function generate various stats for HS downlink channels.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
void dec_hs_gen_stats(  /* HS decode status structure pointer */
                        dec_hs_decode_status_struct_type *decode_status)
{
  /* function local variables */
  /* ------------------------ */

  uint8 buf_idx_for_this_proc;
  uint8 carr_idx;
 
  
  /*** function code starts here ***/

 
  /* update SCCH stats */
  /* ----------------- */
 #if WCDMA_HSLOG_DEBUG
  WL1_MSG2(HIGH, "SCCH Stats log start, log size = %d, num carr = %d ",
                 sizeof(HS_SCCH_STAT_LOG_PKT_type), decode_status->num_carr);
 #endif

  /* increment the number of sub frames used to generate stats */
  hs_scch_stats_log.num_carr = decode_status->num_carr;

  if (decode_status->num_carr > L1_HSDPA_MAX_CARRIERS)
  {
    WL1_MSG1(ERROR, "Incorrect number of Carriers = %d",
                    decode_status->num_carr);
    return;
  }

  for(carr_idx = 0; carr_idx < decode_status->num_carr; carr_idx++)
  {
    hs_scch_stats_log.num_sub_frames[carr_idx]++;
    
    /* Update stats for SCCH CRC pass or fail */
    if(decode_status->scch_demod_attempted[carr_idx])
    {
      hs_scch_stats_log.num_scch_decode_attempted[carr_idx]++;
    }

    if (decode_status->scch_valid[carr_idx])
    {
      hs_scch_stats_log.num_scch_valid[carr_idx]++;
    }
  }

  if (dec_hs_cme_cb != NULL)
  {
      dec_hs_cme_cb(decode_status->num_carr,
              decode_status->scch_demod_attempted,
              decode_status->scch_valid);
  }
  
  rxd_rxdpm_meas_update(decode_status->scch_valid,
                        decode_status->scch_demod_attempted);


  /* update CQI stats */
  /* ---------------- */
 #if WCDMA_HSLOG_DEBUG
  WL1_MSG2(HIGH, "CQI Stats log Start, log size = %d, num carr = %d ",
                 sizeof(HS_CQI_STAT_LOG_PKT_type), decode_status->num_carr);
 #endif

  /* Update the version field*/
  hs_cqi_stats_log.version  = 2;
  /*num of carriers*/
  hs_cqi_stats_log.num_carr = decode_status->num_carr;
  /* Update the Num subframe  */
  hs_cqi_stats_log.num_sub_frames++;
  
  for(carr_idx = 0; carr_idx < decode_status->num_carr; carr_idx++)
  {
    uint8 quan_cqi = decode_status->quan_cqi[carr_idx];
   
    if (decode_status->scch_valid[carr_idx])
    {
      /* SCCH demod valid (CRC passed), Update DSCH ACK/NAACK stats for this CQI */
      if (decode_status->dsch_status[carr_idx])
      {
        /*ACK*/
        hs_cqi_stats_log.cqi_idx_stat[carr_idx][quan_cqi].num_ack++;
      }
      else
      {
        /*NACK*/
        hs_cqi_stats_log.cqi_idx_stat[carr_idx][quan_cqi].num_nack++;
      }
      
    }
    else
    {
      /* SCCH demod not valid (CRC failed), Update DTX stats for this CQI */
      hs_cqi_stats_log.cqi_idx_stat[carr_idx][quan_cqi].num_dtx++;
    }
  }

  /* Update DCSH HARQ stats */
  /* ---------------------- */
 #if WCDMA_HSLOG_DEBUG
  WL1_MSG2(HIGH, "HS HARQ log Start, log size = %d, num carr = %d ",
              sizeof(HS_DSCH_HARQ_STAT_LOG_PKT_type),
                 decode_status->num_carr);
 #endif

  /* increment the number of sub frames used to generate stats */
  hs_harq_stats_log.num_sub_frames++;

  /* Update the information presence mask */
  hs_harq_stats_log.num_carr = decode_status->num_carr;
  
  for(carr_idx = 0; carr_idx < decode_status->num_carr; carr_idx++)
  {
    if (decode_status->scch_valid[carr_idx])
    {
      /* Get HARQ proc stat information buffer index. Buffers are allocated at forst arrive
             first allocate basis */
      uint8 harq_proc_id = decode_status->harq_proc_id[carr_idx]; 
      
      if (harq_proc_id >= L1_DL_HS_DSCH_HARQ_MAX_PROC)
      {
        /* HARQ proc Id is invalid */
        WL1_MSG1(ERROR, "Invalid HARQ proc Id %d", harq_proc_id);
      }
      else
      {
        /* It is set to TRUE if at new Tx received ACK from previous block is not yet received */
        boolean prev_blk_in_error_at_new_tx = FALSE;
        hs_dsch_harq_proc_stat_struct_type *harq_proc_stat_info_ptr;
        hs_dsch_harq_proc_stat_log_struct_type *harq_stat_log_per_carr_ptr;
        harq_proc_stat_info_ptr = &hs_dsch_harq_proc_stat_info[carr_idx][harq_proc_id];
        
        /* ACK received flag is reset at new trasmission irrespective of the fact, if stats can
           be generated or not */
        if (decode_status->new_tx[carr_idx])
        {
          if (harq_proc_stat_info_ptr->ack_received == FALSE)
          {
            prev_blk_in_error_at_new_tx = TRUE;
          }
          /* Set ACK received to FALSE so that ACK/NACK stat generation can happen */
          harq_proc_stat_info_ptr->ack_received   = FALSE;
          harq_proc_stat_info_ptr->num_nack_rece  = 0;
        }
        /* Update NACK received count also, if DSCH failed CRC and ACK has not yet been received */
        if (harq_proc_stat_info_ptr->ack_received == FALSE)
        {
          if (decode_status->dsch_status[carr_idx] == FALSE)
          {
            if (harq_proc_stat_info_ptr->num_nack_rece <= 4)
            {
              harq_proc_stat_info_ptr->num_nack_rece++;
            }
          }
        }

        /* Get current buffer index, if it is invalid then get a new Id at buffer tail.
           Also check that number of HARQ proc must not have maxed out */
        buf_idx_for_this_proc = hs_harq_proc_id_info_idx[carr_idx][harq_proc_id];

        if (buf_idx_for_this_proc == HS_INFO_TABLE_INVALID_VAL)
        {
          /* This HARQ proc arrived for first time */

          if (hs_harq_stats_log.num_harq_proc_c0 > L1_DL_HS_DSCH_HARQ_MAX_PROC)
          {
            /* This is not a possibility */
            ERR_FATAL("Too many HARQ proc logged", 0, 0, 0);
          }
          else
          {
            
            switch ( carr_idx )
            {
              case 0: 
                /* Get next available buffer index */
                hs_harq_proc_id_info_idx[carr_idx][harq_proc_id] = hs_harq_stats_log.num_harq_proc_c0;
                buf_idx_for_this_proc =  hs_harq_stats_log.num_harq_proc_c0;
                hs_harq_stats_log.num_harq_proc_c0++;
                break;
                
              case 1: 
                /* Get next available buffer index */
                hs_harq_proc_id_info_idx[carr_idx][harq_proc_id] = hs_harq_stats_log.num_harq_proc_c1;
                buf_idx_for_this_proc =  hs_harq_stats_log.num_harq_proc_c1;
                hs_harq_stats_log.num_harq_proc_c1++;
                break;
                
              case 2: 
                /* Get next available buffer index */
                hs_harq_proc_id_info_idx[carr_idx][harq_proc_id] = hs_harq_stats_log.num_harq_proc_c2;
                buf_idx_for_this_proc =  hs_harq_stats_log.num_harq_proc_c2;
                hs_harq_stats_log.num_harq_proc_c2++;
                break;
              case 3: 
                /* Get next available buffer index */
                hs_harq_proc_id_info_idx[carr_idx][harq_proc_id] = hs_harq_stats_log.num_harq_proc_c3;
                buf_idx_for_this_proc =  hs_harq_stats_log.num_harq_proc_c3;
                hs_harq_stats_log.num_harq_proc_c3++;
                break;
                
              default: WL1_MSG0(ERROR, "incorrect num carr "); /*error*/
                    
            }
              
            /*alias the buffer*/
            harq_stat_log_per_carr_ptr = 
                            &hs_harq_stats_log.proc_stat[carr_idx][buf_idx_for_this_proc]; 
            /* initialize various counters in stat log packet */
            harq_stat_log_per_carr_ptr->proc_id = harq_proc_id;
            harq_stat_log_per_carr_ptr->num_rece_bits   = 0;
            harq_stat_log_per_carr_ptr->num_new_tx      = 0;
            harq_stat_log_per_carr_ptr->num_blk_errors  = 0;
            harq_stat_log_per_carr_ptr->num_1_nack      = 0;
            harq_stat_log_per_carr_ptr->num_2_nack      = 0;
            harq_stat_log_per_carr_ptr->num_3_nack      = 0;
            harq_stat_log_per_carr_ptr->num_4_nack      = 0;
          }
        }

        if (buf_idx_for_this_proc < L1_DL_HS_DSCH_HARQ_MAX_PROC)
        {
          harq_stat_log_per_carr_ptr = 
                            &hs_harq_stats_log.proc_stat[carr_idx][buf_idx_for_this_proc];
          if (harq_proc_stat_info_ptr->ack_received == FALSE)
          {
            /* increment new transmission count */
            if (decode_status->new_tx[carr_idx])
            {
              harq_stat_log_per_carr_ptr->num_new_tx++;
              if (prev_blk_in_error_at_new_tx)
              {
                harq_stat_log_per_carr_ptr->num_blk_errors++;
              }
            }

            if (decode_status->dsch_status[carr_idx])
            {
              /* Update number of bits received, if ACK is received now */
              harq_stat_log_per_carr_ptr->num_rece_bits += decode_status->tb_size[carr_idx];
            }
            else
            {
              /* NACK is receiced, update NACK receive count */
              switch(harq_proc_stat_info_ptr->num_nack_rece)
              {
                case 1: harq_stat_log_per_carr_ptr->num_1_nack++;
                        break;
                case 2: harq_stat_log_per_carr_ptr->num_2_nack++;
                        break;
                case 3: harq_stat_log_per_carr_ptr->num_3_nack++;
                        break;
                case 4: harq_stat_log_per_carr_ptr->num_4_nack++;
                        break;
                default:break;
              }
            } /* end if-else, ACK is received */
          } /* end if, ACK has not been received */
        } /* end if, got valid proc sta t buf index */
        else
        {
          /* This is not a possibility */
          ERR_FATAL("Invalid HARQ proc buf Id index %d", buf_idx_for_this_proc, 0, 0);
        }

        /* If ack is received then set ACK received flag to terminate this stat processing for this
           proc Id until next new tx is received again */
        if (decode_status->dsch_status[carr_idx])
        {
          harq_proc_stat_info_ptr->ack_received = TRUE;
        }
      } /* end if, proc Id is valid one */
    }
  }
}


/*===========================================================================
FUNCTION dec_hs_submit_stats

DESCRIPTION
  This function submits the HS downlink channel stats to logging. This is
  done when HS stat gen/accumulation action is STOP or RESTART or when stats
  have been generated using max number of samples.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

void dec_hs_submit_stats(void)
{
  /* function local variables */
  /* ------------------------ */

  /* loop indicies */
  uint32 index_proc;
  /* pointer to log packet header */
  hsdpa_log_hdr_struct_type *hs_stats_log_pkt_hdr;
   uint8 carr_idx;
  /*** function code starts here ***/

  /* Dump HS stats as F3 messages */
  
  if (hs_scch_stats_log.num_carr > L1_HSDPA_MAX_CARRIERS)
  {
    WL1_MSG1(ERROR, "Incorrect number of Carriers = %d",
                    hs_scch_stats_log.num_carr);
    return;
  }
   
 for (carr_idx = 0; 
       carr_idx <  hs_scch_stats_log.num_carr; 
       carr_idx++)
  {
    
    WL1_MSG4(HIGH, "SCCH stats carr %d :NumSubFN %d SCCH attempted %d valid %d",
             carr_idx,
             hs_scch_stats_log.num_sub_frames[carr_idx],
             hs_scch_stats_log.num_scch_decode_attempted[carr_idx],
             hs_scch_stats_log.num_scch_valid[carr_idx]);  
  }

  for (carr_idx = 0; 
       carr_idx <  hs_scch_stats_log.num_carr; 
       carr_idx++)
  {
    uint8 num_harq_proc = 0;
    
    switch(carr_idx)
    {
      case 0: num_harq_proc = hs_harq_stats_log.num_harq_proc_c0;
              break;
      case 1: num_harq_proc = hs_harq_stats_log.num_harq_proc_c1;
              break;
      case 2: num_harq_proc = hs_harq_stats_log.num_harq_proc_c2;
              break;
      case 3: num_harq_proc = hs_harq_stats_log.num_harq_proc_c3;
              break;
              
      default: WL1_MSG0(ERROR, "Incorrect num of carriers");
    }
    
    WL1_MSG1(HIGH, "HARQ stats carr = %d", carr_idx);
    for (index_proc = 0; index_proc < num_harq_proc; index_proc++)
    {
      WL1_MSG3(HIGH, "HARQprocId %d numNewTx %d blkErr %d",
               hs_harq_stats_log.proc_stat[carr_idx][index_proc].proc_id,
               hs_harq_stats_log.proc_stat[carr_idx][index_proc].num_new_tx,
               hs_harq_stats_log.proc_stat[carr_idx][index_proc].num_blk_errors);
    }
  }

  if (log_status(HS_SCCH_STAT_LOG_PKT))
  {
    /* log packet pointer that need to be submitted */
    HS_SCCH_STAT_LOG_PKT_type *this_hs_scch_stats_log;

    /* get log packet pointer */
    this_hs_scch_stats_log = &hs_scch_stats_log;

    /* Set log packet size */
    hs_stats_log_pkt_hdr = (hsdpa_log_hdr_struct_type*) this_hs_scch_stats_log;
    hs_stats_log_pkt_hdr->len = sizeof(HS_SCCH_STAT_LOG_PKT_type);

    hs_scch_stats_log.version = 2;

    /* submit the log packet */
    if (!log_submit((PACKED void *) &hs_scch_stats_log))
    {
      WL1_MSG1(ERROR, "HS_SCCH_STAT:Failed info sz %d",
                      hs_stats_log_pkt_hdr->len);
    }
  }

  if (log_status(HS_DSCH_HARQ_STAT_LOG_PKT))
  {
    /* log packet pointer that need to be submitted */
    HS_DSCH_HARQ_STAT_LOG_PKT_type *this_hs_harq_stats_log;

    /* get log packet pointer */
    this_hs_harq_stats_log = &hs_harq_stats_log;

    /* Set log packet size */
    hs_stats_log_pkt_hdr = (hsdpa_log_hdr_struct_type*) this_hs_harq_stats_log;
    hs_stats_log_pkt_hdr->len = sizeof(HS_DSCH_HARQ_STAT_LOG_PKT_type);

    /* submit the log packet */
    if (!log_submit((PACKED void *) &hs_harq_stats_log))
    {
      WL1_MSG1(ERROR, "HS_HARQ_STAT:Failed info sz %d",
                      hs_stats_log_pkt_hdr->len);
    }
  }

  if (log_status(HS_CQI_STAT_LOG_PKT))
  {
    HS_CQI_STAT_LOG_PKT_type *this_hs_cqi_stats_log;
    this_hs_cqi_stats_log = &hs_cqi_stats_log;

    /* Set log packet size */
    hs_stats_log_pkt_hdr = (hsdpa_log_hdr_struct_type*) this_hs_cqi_stats_log;
    hs_stats_log_pkt_hdr->len = sizeof(HS_CQI_STAT_LOG_PKT_type);

    /* submit the log packet */
    if (!log_submit((PACKED void *) &hs_cqi_stats_log))
    {
      WL1_MSG1(ERROR, "HS_CQI_STAT:Failed info sz %d",
                      hs_stats_log_pkt_hdr->len);
    }
  }
 
  mcalwcdma_dec_hs_dump_stats_from_dob_status_fifo();
 
  dec_hs_init_stats();
}

/*===========================================================================
FUNCTION dec_hs_register_hs_scch_stats_cb

DESCRIPTION
  Registers a function called when HS stats are generated.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
void dec_hs_register_hs_scch_stats_cb(DEC_HS_CME_CME_CB_FUNC_TYPE cme_cb)
{
  if (cme_cb == NULL)
  {
    WL1_MSG0(ERROR, "dec_hs_register_hs_scch_stats_cb: cme_cb == NULL");
  }

  dec_hs_cme_cb = cme_cb;

  return;
}

/*===========================================================================
FUNCTION dec_hs_deregister_hs_scch_stats_cb

DESCRIPTION
  Deregisters the function called when HS stats are generated.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
void dec_hs_deregister_hs_scch_stats_cb(void)
{
  dec_hs_cme_cb = NULL;

  return;
}

/*===========================================================================
FUNCTION dec_hs_scch_log_init
 
DESCRIPTION This function initializes the scch log init flag and final sub frame.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
void dec_hs_scch_log_init(void)
{
  /* function local variables */
  /* ------------------------ */

  /* loop index */
  uint8 index_buf;
  
  /*** function code starts here ***/

  /*RESET Init Flag*/  
  hs_scch_logging_enabled_qxdm = FALSE;
  
  hs_scch_refresh_info_flag = TRUE;
  
  /*scch log final sfn*/
  hs_scch_log_final_sub_fn = INVALID_STOP_GSFN;

  /* iterate over all(2 in this case..double buffering) log buffer status to mark them available */
  for (index_buf = 0; index_buf < HS_SCCH_LOG_NUM_BUF; index_buf++)
  {
    hs_scch_log_buf_avail_status[index_buf] = TRUE;
  }

  /* Set log buffer index to INVALID */
  hs_scch_log_info_buf_idx = HS_INFO_TABLE_INVALID_VAL;


  /* Init log submit request to FALSE */
  hs_scch_log_submit = FALSE;
  hs_scch_log_buf_idx_to_submit = HS_INFO_TABLE_INVALID_VAL;
}
/*===========================================================================
FUNCTION dec_hs_get_new_scch_log_buff

DESCRIPTION This function allocates and initializes the scch 
            log packet
  
DEPENDENCIES
  None

RETURN VALUE
  pointer to buffer of HS_SCCH_LOG_PKT_type

SIDE EFFECTS
  None
===========================================================================*/

HS_SCCH_LOG_PKT_type* dec_hs_get_new_scch_log_buff(void)
{
  /* Allocate a new buffer to accumulate log samples */
  
  uint8 index_buf; /* loop indicies */
  /* pointer to hs-scch log packets */
  HS_SCCH_LOG_PKT_type *this_hs_scch_log_pkt;

  /* Get available HS SCCH buffer */
  for (index_buf = 0;
       index_buf < HS_SCCH_LOG_NUM_BUF; /* GET ONE OF THE 2 BUFFERS */
       index_buf++)
  {
    if (hs_scch_log_buf_avail_status[index_buf]) /* if it is TRUE, proceed */
    {
      hs_scch_log_buf_avail_status[index_buf] = FALSE;
      hs_scch_log_info_buf_idx = index_buf;
      break;
    }
  }
  /* If can't get buffer then there is no use of proceeding. It should
     never happen because of double buffering */
  if (hs_scch_log_info_buf_idx == HS_INFO_TABLE_INVALID_VAL)
  {
    WL1_MSG0(ERROR, "Can't get hs scch log buf");

    /* iterate over all log buffer status to mark them available */
    for (index_buf = 0; index_buf < HS_SCCH_LOG_NUM_BUF; index_buf++)
    {
      hs_scch_log_buf_avail_status[index_buf] = TRUE;
    }

     hs_scch_log_info_buf_idx = 0;
     hs_scch_log_buf_avail_status[hs_scch_log_info_buf_idx] = FALSE;

     /* Init log submit request to FALSE */
     hs_scch_log_submit = FALSE;
  }
  this_hs_scch_log_pkt = &(hs_scch_log_pkt_buf[hs_scch_log_info_buf_idx]);

  /* Do time stamp corresponding to first sample */
  log_set_timestamp(this_hs_scch_log_pkt);
  /* init number of samples accumulated to 0 */
  this_hs_scch_log_pkt->num_samples = 0;
  /* set the verison info*/  
  this_hs_scch_log_pkt->version     = 2;
  /* initialize the buffer offest */
  hs_scch_sample_buf_offset = 0;

  /* get pointer to current HS scch log buffer */
 return this_hs_scch_log_pkt;
  
}
/*===========================================================================
FUNCTION: dec_hs_get_curr_scch_log

DESCRIPTION: This function gets current SCCH log packet
  
DEPENDENCIES
  None

RETURN VALUE
  pointer to buffer of HS_SCCH_LOG_PKT_type

SIDE EFFECTS
  None
===========================================================================*/

HS_SCCH_LOG_PKT_type* dec_hs_get_curr_scch_log(void)
{
  /*** function code starts here ***/
  if(hs_scch_log_info_buf_idx >= HS_SCCH_LOG_NUM_BUF)
  {
    ERR_FATAL("Invalid dec hs current scch log info buffer idx = %d",
               hs_scch_log_info_buf_idx,0,0);
    return NULL;
  }
  else
  {
    /* get the log buffer current location*/
    return &(hs_scch_log_pkt_buf[hs_scch_log_info_buf_idx]);
  }
}

/*===========================================================================
FUNCTION dec_hs_scch_log_populate

DESCRIPTION
  This function is used for populating the SW hs cch log buffer from FW buffer.
  It populates only one Subframe at a time.  
  
DEPENDENCIES
  None

RETURN VALUE
  none

SIDE EFFECTS
  None
===========================================================================*/
void dec_hs_scch_log_populate(WfwDbackHsDecodeStatusLogEntryStruct *fw_hs_decode_status_per_sf_ptr,
                              uint8 num_carr)
{
      /* function local variables */
      /* ------------------------ */
      /*num of samples logged*/
      uint8 num_samples;
      uint8 carr_idx;
      uint16 global_sub_fr_num;
      /* Index for Num scch per carrier*/
      uint8 scch_idx;
      HS_SCCH_LOG_PKT_type* this_hs_scch_log_pkt = NULL;
      hs_scch_info_struct_type* hs_scch_sample_ptr;
      
      /*** function code starts here ***/
      /* get pointer to current HS SCCH log buffer.
       Usefull when using an earlier unflushed buffer*/
      this_hs_scch_log_pkt = dec_hs_get_curr_scch_log();
      /*sanity check (KW)*/
      if(NULL == this_hs_scch_log_pkt)
      {
         WL1_MSG0(FATAL, "current hs scch log pkt returned as NULL");
	 return;
      }
      num_samples = this_hs_scch_log_pkt->num_samples;

      global_sub_fr_num = fw_hs_decode_status_per_sf_ptr->gSubFn;
  
      
      /* If acumulation has reached max samples then this must be submitted */
      /* ------------------------------------------------------------------ */

      if (this_hs_scch_log_pkt->num_samples >= HS_NUM_MAX_SCCH_SAMPLES)
      {
        /* HSDPA decode status log header pointer */
        hsdpa_log_hdr_struct_type *hs_scch_log_pkt_hdr;

        /* Get header to current log buffer and update total size of
           information accumulated */
        hs_scch_log_pkt_hdr = (hsdpa_log_hdr_struct_type*)(this_hs_scch_log_pkt);
        /* calculate size of log fields not part of scch info structure + no of samples recorded*/
        hs_scch_log_pkt_hdr->len = sizeof(HS_SCCH_LOG_PKT_type)
                                 - (MAX_SCCH_LOG_BUFF_SIZE) 
                                 +  hs_scch_sample_buf_offset;
        
 #if (WCDMA_HSLOG_DEBUG)
        WL1_MSG2(HIGH, "HS Log Debug: HS SCCH logging reached max size, Log Size = %d SubFn = %d",
                       hs_scch_log_pkt_hdr->len, global_sub_fr_num);
 #endif

       /* send command to submit this log packet */
       dec_hs_scch_send_log_submit_cmd(hs_scch_log_info_buf_idx);

       hs_scch_log_info_buf_idx = HS_INFO_TABLE_INVALID_VAL;

       /* Get available scch log accumulation buffer */
        this_hs_scch_log_pkt = dec_hs_get_new_scch_log_buff();

       /* Save starting sub frame number */
       this_hs_scch_log_pkt->starting_global_sub_fn = global_sub_fr_num;
       
       this_hs_scch_log_pkt->starting_global_sub_fn &= 0x7FFF;
             
      } /* end if, max decode status samples have been collected */
  
      if(hs_scch_sample_buf_offset >= MAX_SCCH_LOG_BUFF_SIZE)
      {
        WL1_MSG2(ERROR, "hs_scch_sample_buf_offset %d exceeded bounds %d",
                        hs_scch_sample_buf_offset, MAX_SCCH_LOG_BUFF_SIZE);
        /*report error and recover gracefully */
        hs_scch_sample_buf_offset= 0;
        this_hs_scch_log_pkt->num_samples = 0;
        
      }
      /* get the log buffer current location and typecast it to structure*/
      hs_scch_sample_ptr = (hs_scch_info_struct_type *)
                           &(this_hs_scch_log_pkt->scch_info[hs_scch_sample_buf_offset]);

      /* Set the carrier info.*/
      hs_scch_sample_ptr->num_carr = num_carr; 
      /* offset by one byte */
      hs_scch_sample_buf_offset += sizeof(uint8);

      for (carr_idx = 0; carr_idx < hs_scch_sample_ptr->num_carr; carr_idx++)
      {
        /* mDSP address from where to read the data */
        WfwDbackHsScchDecodeStruct* hs_scch_mdsp_log_ptr;
        hs_scch_struct_type *hs_scch_info;

       #if (WCDMA_HSLOG_DEBUG)
        WL1_MSG2(HIGH, "HS Log Debug: HS SCCH logging Carr = %d SubFn = %d",
                       carr_idx, global_sub_fr_num);
       #endif
        for (scch_idx = 0; scch_idx < HSDPA_NUM_SCCH_MONITERED; scch_idx++)
        {
          /* Source: get the SCCH FW sample location*/
          hs_scch_mdsp_log_ptr = &(fw_hs_decode_status_per_sf_ptr\
                                 ->WfwDbackHsDecodeStatusLogPerCarrier[carr_idx]\
                                 .hsScchDecode[scch_idx]);

          /*Destination: alias for SCCH info per SCCH and per Carrier*/
          hs_scch_info = &(hs_scch_sample_ptr->scch_info[carr_idx][scch_idx]);
      
          /*log the scch amplitude and scch error count*/ 
          hs_scch_info->amp_slot1   = hs_scch_mdsp_log_ptr->ampSlot1;
          hs_scch_info->amp_slot2   = hs_scch_mdsp_log_ptr->ampSlot2;
          hs_scch_info->amp_slot3   = hs_scch_mdsp_log_ptr->ampSlot3;
          hs_scch_info->err_cnt_pt1 = hs_scch_mdsp_log_ptr->errCntP1;
          hs_scch_info->err_cnt_pt2 = hs_scch_mdsp_log_ptr->errCntP2;
          /* offset the offset by 1 structure sample */
          hs_scch_sample_buf_offset += sizeof(hs_scch_struct_type);

       }
      }  
      /* Increment number of samples accumulated */
      this_hs_scch_log_pkt->num_samples++;


  return;
}

/*===========================================================================
FUNCTION dec_hs_init_scch_log

DESCRIPTION
  Get a new SCCH log buffer and initialize it.
  
DEPENDENCIES
  None

RETURN VALUE
  none

SIDE EFFECTS
  None
===========================================================================*/

void dec_hs_init_scch_log(uint8 global_sub_fr_num)
{
   /* function local variables */
  /* ------------------------ */
  HS_SCCH_LOG_PKT_type* this_hs_scch_log_pkt = NULL;
  
  /*** function code starts here ***/
  /*Assign new buffer and initialize it*/
  this_hs_scch_log_pkt = dec_hs_get_new_scch_log_buff();
  
  /* Save starting sub frame number */
  this_hs_scch_log_pkt->starting_global_sub_fn = global_sub_fr_num; 
  #if (WCDMA_HSLOG_DEBUG) 
  WL1_MSG2(HIGH, "HS Log Debug : Starting HS SCCH logging %d SubFn %d",
           hs_scch_log_info_buf_idx,
           this_hs_scch_log_pkt->starting_global_sub_fn);
  #endif
  return;
  }
/*===========================================================================
FUNCTION dec_hs_scch_log_update

DESCRIPTION
  This function is used for the  hs-scch logging, handles Start/Stop of logging.
  
DEPENDENCIES
  None

RETURN VALUE
  none

SIDE EFFECTS
  None
===========================================================================*/
void dec_hs_scch_log_update(void)
{
  /* function local variables */
  /* ------------------------ */
  /*num of conifgured carriers, initialized when SCCH logging action changes to START.*/
  static uint8 num_carr = 0;
  
  /* get previous frame starting sub frame number */
  uint16 global_sub_fr_num;

  uint8 wr_idx,
        rd_idx; 

  WfwDbackHsDecodeStatusLogFifoStruct *fw_hs_decode_status_log_fifo_ptr = NULL;
  
  /*** function code starts here ***/
  if (FALSE == hs_scch_logging_enabled_qxdm)
  {
    if (log_status(HS_SCCH_LOG_PKT))
    {
      hs_scch_logging_enabled_qxdm = TRUE;
    }
    else
    {
      /*log disabled from QXDM*/
      /*Logging could have been disabled from QXDM while HS is up and can be re-enabled 
        before HS is initialized, hence, need to reinitialize the logging flags here. CR 634673*/
            
      /*set init flag*/
       hs_scch_refresh_info_flag = TRUE;

       /*invalidate*/
       hs_scch_log_final_sub_fn = INVALID_STOP_GSFN;
       
      return;
    }
  }
  
  /*Alias */
  fw_hs_decode_status_log_fifo_ptr = &(mcalwcdma_fwsw_intf_addr_ptr->dbackHsDecodeStatusLog);

  /* Read the write and read idx of hs decode status log from FW*/    
  wr_idx = fw_hs_decode_status_log_fifo_ptr->dbackHsDecodeStatusLogWrPtr;
  rd_idx = fw_hs_decode_status_log_fifo_ptr->dbackHsDecodeStatusLogRdPtr;

  if(wr_idx >= WFW_DEMOD_HS_DEC_STATUS_LOG_PACKET_BUF_SIZE
     || rd_idx >= WFW_DEMOD_HS_DEC_STATUS_LOG_PACKET_BUF_SIZE)
  {
    ERR_FATAL("FW Read and Write Idx greater than buffer size",0,0,0);
    return;
  }
  /*For case when FW ISR to STOP comes after logging function is called, hence Stop SFN is not updated */
  rd_idx = (rd_idx - 1);  
  /*rd_idx wrap around*/
  if(rd_idx > WFW_DEMOD_HS_DEC_STATUS_LOG_PACKET_BUF_SIZE)
  {
    rd_idx = WFW_DEMOD_HS_DEC_STATUS_LOG_PACKET_BUF_SIZE-1;
  }
  /*read the last read Buffer and compare the STOP SFN*/
  global_sub_fr_num = fw_hs_decode_status_log_fifo_ptr\
                     ->WfwDbackHsDecodeStatusLogEntry[rd_idx].gSubFn;


  /* STOP this accumulation now */
  if(global_sub_fr_num == hs_scch_log_final_sub_fn)
  {
     
     MSG_HIGH("Stopping HS SCCH logging buf idx %d SubFn %d",
              hs_scch_log_info_buf_idx,
              global_sub_fr_num,0);
  
     /* Flush already accumulated samples */
     dec_hs_scch_stop_logging();
  
     /*set init flag*/
     hs_scch_refresh_info_flag = TRUE;
  
     /*invalidate*/
     hs_scch_log_final_sub_fn = INVALID_STOP_GSFN;
  
     /*set qxdm log to false*/
     hs_scch_logging_enabled_qxdm = FALSE;
    
  }

  /* Read the write and read idx of hs decode status log from FW*/    
  wr_idx = fw_hs_decode_status_log_fifo_ptr->dbackHsDecodeStatusLogWrPtr;
  rd_idx = fw_hs_decode_status_log_fifo_ptr->dbackHsDecodeStatusLogRdPtr;
  
  /*Read till W_idx */
  while(rd_idx != wr_idx)
  {
    /* Generate Sub frame number when log was captured*/
    global_sub_fr_num = fw_hs_decode_status_log_fifo_ptr\
                     ->WfwDbackHsDecodeStatusLogEntry[rd_idx].gSubFn;
  
    /*SCCH init*/
    if(TRUE == hs_scch_refresh_info_flag)
    {

      /*init scch log*/
      dec_hs_init_scch_log(global_sub_fr_num);
      hs_scch_refresh_info_flag = FALSE;

      /*query HS for number of carriers, 
            which are assumed to be constant till next start*/
      num_carr = dec_hs_call_info.num_carr;
      
      if(num_carr == 0)
      {
        WL1_MSG0(FATAL, "HS scch logging initalized with num carr = ZERO !!");
      }

      WL1_MSG2(HIGH, "Init HS SCCH logging SFN = %d, #carr = %d",
                     global_sub_fr_num, num_carr);
    }

    /*populate*/
    dec_hs_scch_log_populate(&(fw_hs_decode_status_log_fifo_ptr->WfwDbackHsDecodeStatusLogEntry[rd_idx]),
                             num_carr);
    
    /* STOP this accumulation now */
    if(global_sub_fr_num == hs_scch_log_final_sub_fn)
    {
       
       WL1_MSG2(HIGH, "Stopping HS SCCH logging buf idx %d SubFn %d",
                      hs_scch_log_info_buf_idx, global_sub_fr_num);

       /* Flush already accumulated samples */
       dec_hs_scch_stop_logging();

       /*set init flag*/
       hs_scch_refresh_info_flag = TRUE;

       /*invalidate*/
       hs_scch_log_final_sub_fn = INVALID_STOP_GSFN;

       /*set qxdm log to false*/
       hs_scch_logging_enabled_qxdm = FALSE;
      
    }

    /*~~increment and wrap around buffer size~~*/ 
    rd_idx = (rd_idx + 1) % WFW_DEMOD_HS_DEC_STATUS_LOG_PACKET_BUF_SIZE;  
  }

  return;
}

/*===========================================================================
FUNCTION dec_hs_scch_stop_logging

DESCRIPTION
  This function prepares the hs-scch buffer to be submitted to diag
  It calls the function that posts a hs-scch log submit local command and then
  clears the state of hs-scch logging.
  
DEPENDENCIES
  hs_scch_log_info_buf_idx should be valid while making
  a call to this function.

RETURN VALUE
  None

SIDE EFFECTS
  ===========================================================================*/
void dec_hs_scch_stop_logging(void)
{
  /* function local variables */
  /* ------------------------ */

  /* hs-scch log header pointer */
  hsdpa_log_hdr_struct_type *hs_scch_log_pkt_hdr;

  /*** function code starts here ***/

  /* Populate the header */
  hs_scch_log_pkt_hdr =
    (hsdpa_log_hdr_struct_type*) (&hs_scch_log_pkt_buf[hs_scch_log_info_buf_idx]);

  /*Buffer len = Size of total buffer - Size of info struct of unused samples*/
  hs_scch_log_pkt_hdr->len = sizeof(HS_SCCH_LOG_PKT_type)
                           - (sizeof(hs_scch_info_struct_type)
                           * (HS_NUM_MAX_SCCH_SAMPLES 
                           -  hs_scch_log_pkt_buf[hs_scch_log_info_buf_idx].num_samples));

  /* send command to submit this decode status log packet */
  dec_hs_scch_send_log_submit_cmd(
    hs_scch_log_info_buf_idx);

  /* mark HS inactive */
  hs_scch_logging_enabled_qxdm = FALSE;

}


/*===========================================================================
FUNCTION dec_hs_scch_send_log_submit_cmd

DESCRIPTION
  This function makes a request to post a local command to submit the 
  currently accumulated log packet to diag. Before it does so, it checks if 
  a previous log packet that was submitted to diag has been  serviced or not.
  If not, it drops the current packet and proceeds further.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

void dec_hs_scch_send_log_submit_cmd(
  /* HS scch log buffer index to submit */
  uint8 hs_scch_log_idx)
{
  /* function local variables */
  /* ------------------------ */

  /* log packet pointer that need to be submitted */
  HS_SCCH_LOG_PKT_type *this_hs_scch_log_pkt;

  /* log header pointer */
  hsdpa_log_hdr_struct_type *hs_scch_log_pkt_hdr;

  /*** function code starts here ***/

  if (hs_scch_log_idx >= HS_SCCH_LOG_NUM_BUF)
  {
    ERR_FATAL("Incorrect index for hs-scch log %d", hs_scch_log_idx, 0, 0);
  }

  /* Check if there is a pending request to submit previous log packet */
  if (hs_scch_log_submit)
  {
    /* Print error message and mark this buffer as available one */

      /* get log packet pointer */
      this_hs_scch_log_pkt = &(hs_scch_log_pkt_buf[hs_scch_log_idx]);
      /* Get log packet header pointer and set log code to that */
      hs_scch_log_pkt_hdr = (hsdpa_log_hdr_struct_type*) this_hs_scch_log_pkt;
      /* submit the log packet */
      WL1_MSG3(ERROR, "hs_scch_LOG:Failed idx %d info sz %d idx pending %d",
                      hs_scch_log_idx,
                      hs_scch_log_pkt_hdr->len,
                      hs_scch_log_buf_idx_to_submit);

      hs_scch_log_buf_avail_status[hs_scch_log_idx] = TRUE;
  }
  else
  {
    /* Save HS scch log buffer index to submit later in task context */
    hs_scch_log_submit = TRUE;
    hs_scch_log_buf_idx_to_submit = hs_scch_log_idx;

    /* Send local command to l1m */
    hs_send_submit_log_cmd();
  }
}


/*===========================================================================
FUNCTION dec_hs_scch_submit_log_buffer

DESCRIPTION
  This function is called to post HS SCCH submit command. If the 
  buffer is not ready to be submitted to diag the function returns without
  doing anything. This can so happen because multiple logs are instructed
  to be submitted to diag by the same L1 command.
  It checks for the log code in diag and submits the packet.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  Flag indicating the log buffer is submitted for flushing is reset.
===========================================================================*/
void dec_hs_scch_submit_log_buffer(void)
{
  /* function local variables */
  /* ------------------------ */

  /* flag to hold the state of the log code in QXDM */
  boolean log_code_disabled = FALSE;

  /* log packet pointer that need to be submitted */
  HS_SCCH_LOG_PKT_type *this_hs_scch_log_pkt;

  /* log header pointer */
  hsdpa_log_hdr_struct_type *hs_scch_log_pkt_hdr;
  /* buffer index to submit */
  uint8 log_info_buf_idx;

  /*** function code starts here ***/

  /* If log need to be submitted then proceed otherwise simply return from here */
  if (hs_scch_log_submit)
  {
    /* save log buffer index to submit */
    log_info_buf_idx = hs_scch_log_buf_idx_to_submit;
  }
  else
  {
    return;
  }

  if (log_info_buf_idx >= HS_SCCH_LOG_NUM_BUF)
  {
    ERR_FATAL("Incorrect index for HS-SCCH submit buffer %d", log_info_buf_idx, 0, 0);
  }

 #ifdef FEATURE_L1_LOGGING_ENABLED
  if (log_status(HS_SCCH_LOG_PKT))
    {

      /* get log packet pointer */
      this_hs_scch_log_pkt = &(hs_scch_log_pkt_buf[log_info_buf_idx]);
      /* Get log packet header pointer and set log code to that */
      hs_scch_log_pkt_hdr = (hsdpa_log_hdr_struct_type*) this_hs_scch_log_pkt;
      hs_scch_log_pkt_hdr->code = HS_SCCH_LOG_PKT;
        
      /* submit the log packet */
      if (!log_submit((PACKED void *) this_hs_scch_log_pkt))
      {
        WL1_MSG1(ERROR, "HS_SCCH_LOG:Failed info sz %d",
                        hs_scch_log_pkt_hdr->len);
      }
    }
  else
    {
      /* Mark the flag to indicate that the log code is disabled */
      log_code_disabled = TRUE;
    }
 #endif
  /*Mutex lock*/
  HS_LOG_INTLOCK();
  /* Mark this hs-scch log buffer to available */
  hs_scch_log_buf_avail_status[log_info_buf_idx] = TRUE;
  /* reset log to submit flag */
  hs_scch_log_submit = FALSE;

  /* If the log code is disabled, clear all buffers and mark logging state
     as well as logging buffer index as invalid */
  if (log_code_disabled)
  {
    uint8 index_buf;
    
    /* Mark logging OFF by QXDM */
    hs_scch_logging_enabled_qxdm = FALSE;
    /* Mark the log buffer index as invalid */
    hs_scch_log_info_buf_idx = HS_INFO_TABLE_INVALID_VAL;    

    /*set init flag since buffer have been de-allocated*/
    hs_scch_refresh_info_flag = TRUE;
	
    /* iterate over all log buffer status to mark them available */
    for (index_buf = 0; index_buf < 2; index_buf++)
    {
      hs_scch_log_buf_avail_status[index_buf] = TRUE;
    }
  }
  /*Mutex free*/
  HS_LOG_INTFREE();
}



/*===========================================================================
FUNCTION dec_hs_pess_cqi_log_init
 
DESCRIPTION This function initializes the log states for pess cqi 
            log packet
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
void dec_hs_pess_cqi_log_init(void)
{
  /* function local variables */
  /* ------------------------ */

  /* loop index */
  uint8 index_buf;
  
  /*** function code starts here ***/

  /* MIMO demod accumulation logging is inactive */
  hs_pess_cqi_log_accum_active = FALSE;
  hs_pess_cqi_logging_enabled_qxdm = FALSE;
  
  /* MIMO demod accumulation logging action is NO_OP */
  hs_pess_cqi_log_action = HS_STAT_GEN_NOOP;

  /* iterate over all log buffer status to mark them available */
  for (index_buf = 0; index_buf < HS_PESS_CQI_LOG_NUM_BUF; index_buf++)
  {
    hs_pess_cqi_log_buf_avail_status[index_buf] = TRUE;
  }

  /* Set log buffer index to INVALID */
  hs_pess_cqi_log_info_buf_idx = HS_INFO_TABLE_INVALID_VAL;

  /* Init log submit request to FALSE */
  hs_pess_cqi_log_submit = FALSE;
  hs_pess_cqi_log_buf_idx_to_submit = HS_INFO_TABLE_INVALID_VAL;
}
/*===========================================================================
FUNCTION dec_hs_get_pess_cqi_action

DESCRIPTION
  This function returns the action that HS Pess CQI state machine needs
  to perform next.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

hs_stat_gen_action_enum_type dec_hs_get_pess_cqi_action(void)
{
  /* function local variables */
  /* ------------------------ */

  /*** function code starts here ***/
  
  return hs_pess_cqi_log_action;
}

/*===========================================================================
FUNCTION dec_hs_set_pess_cqi_action

DESCRIPTION
  This function sets stat generation mode.
 
DEPENDENCIES
  None

RETURN VALUE
  TRUE or FALSE based on action accept validation

SIDE EFFECTS
  None
===========================================================================*/
boolean dec_hs_set_pess_cqi_action(
  /* Action for HS decode statu accumulation and stat generation */
  hs_stat_gen_action_enum_type action)
{
  hs_pess_cqi_log_action = action;
  WL1_MSG1(HIGH, "Setting the HS Pess CQI gen action to %d",
                 hs_pess_cqi_log_action);
  return TRUE;
}

/*===========================================================================
FUNCTION dec_hs_set_pess_cqi_accum_active

DESCRIPTION
  This function set Pess cqi accum flag to desired value which forces
  pess cqi logging to be enabled or disabled.
  
DEPENDENCIES
  None
  
RETURN VALUE
  None

SIDE EFFECTS
  Variables get inited to their initial values
===========================================================================*/

void dec_hs_set_pess_cqi_accum_active(boolean accum_status)
{
  uint8 index_buf;
  /*if disabling Pess CQI*/
  if(accum_status == FALSE)
  {
    /*check if current buffer has left over samples to be commited*/
    if (hs_pess_cqi_log_info_buf_idx != HS_INFO_TABLE_INVALID_VAL)
    {
      /*Get current buffer*/
      HS_PESS_CQI_LOG_PKT_type *this_hs_pess_cqi_log_pkt = dec_hs_get_curr_pess_cqi_log();
      
      if(this_hs_pess_cqi_log_pkt->num_samples > 0)
      {
        /* If some samples are already accumulated, flush them */
        dec_hs_pess_cqi_stop_logging ();
      }
      
      /* Reset after force stop: iterate over all log buffer status to mark them available */
      for (index_buf = 0; index_buf < HS_PESS_CQI_LOG_NUM_BUF; index_buf++)
      {
        hs_pess_cqi_log_buf_avail_status[index_buf] = TRUE;
      }
    }
  }
  hs_pess_cqi_log_accum_active = accum_status;
}

/*===========================================================================
FUNCTION dec_hs_get_new_pess_cqi_buff

DESCRIPTION
  gets an available new cqi buffer 
  
DEPENDENCIES
  None

RETURN VALUE
 
SIDE EFFECTS
  None
===========================================================================*/

HS_PESS_CQI_LOG_PKT_type* dec_hs_get_new_pess_cqi_buff(void)
{
  uint8 index_buf; /* loop indicies */
    
  /* Get available HS pess cqi buffer */
  for (index_buf = 0;
       index_buf < HS_PESS_CQI_LOG_NUM_BUF;
       index_buf++)
  {
    if (hs_pess_cqi_log_buf_avail_status[index_buf])
    {
      hs_pess_cqi_log_buf_avail_status[index_buf] = FALSE;
      hs_pess_cqi_log_info_buf_idx = index_buf;
      break;
    }
  }

  /* If can't get buffer then there is no use of proceeding. It should
     never happen because of double buffering */
  if (hs_pess_cqi_log_info_buf_idx >= HS_PESS_CQI_LOG_NUM_BUF)
  {
    WL1_MSG0(ERROR, "Can't get HS pess cqi log buf");

    /* iterate over all log buffer status to mark them available */
    for (index_buf = 0; index_buf < HS_PESS_CQI_LOG_NUM_BUF; index_buf++)
    {
      hs_pess_cqi_log_buf_avail_status[index_buf] = TRUE;
    }

    /* Set log buffer index to 0 */
    hs_pess_cqi_log_info_buf_idx = 0;
    hs_pess_cqi_log_buf_avail_status[hs_pess_cqi_log_info_buf_idx] = FALSE;

    /* Init log submit request to FALSE */
    hs_pess_cqi_log_submit = FALSE;
    
  }
    /*set buffer offset to ZERO*/
    hs_pess_cqi_sample_buf_offset = 0 ;
  /* get pointer to current HS pess cqi debug log buffer */
  return &(hs_pess_cqi_log_pkt_buf[hs_pess_cqi_log_info_buf_idx]);
  
}
/*===========================================================================
FUNCTION dec_hs_init_pess_cqi_log

DESCRIPTION
  Init pess cqi log fields
  
DEPENDENCIES
  None

RETURN VALUE

SIDE EFFECTS
  None
===========================================================================*/

void dec_hs_init_pess_cqi_log(HS_PESS_CQI_LOG_PKT_type *this_hs_pess_cqi_log_pkt,
                                    uint16 global_sub_fr_num)
{
  /* Do time stamp corresponding to first sample */
  log_set_timestamp(this_hs_pess_cqi_log_pkt);
  /* init number of samples accumulated to 0 */
  this_hs_pess_cqi_log_pkt->num_samples = 0;
  /* set the verison info*/  
  this_hs_pess_cqi_log_pkt->version = 4;
  /* Save starting sub frame number */
  /* If the DPCH to SCCH offset is >= 30, it means that the starting global subframe number
     is advanced by 1 to keep the separation between end of HS-PDSCH to start of HS-SCCH > 40bpgs */
  this_hs_pess_cqi_log_pkt->starting_global_sub_fn = global_sub_fr_num; 
 
}
/*===========================================================================
FUNCTION dec_hs_get_curr_pess_cqi_log

DESCRIPTION
return pointer to current pess cqi buffer in use
DEPENDENCIES

RETURN VALUE
  
SIDE EFFECTS
  None
===========================================================================*/

HS_PESS_CQI_LOG_PKT_type* dec_hs_get_curr_pess_cqi_log()
{
  if(hs_pess_cqi_log_info_buf_idx >= HS_PESS_CQI_LOG_NUM_BUF)
  {
    ERR_FATAL("Invalid dec hs current Pess CQI buffer idx = %d",
               hs_pess_cqi_log_info_buf_idx,0,0);
    return NULL;
  }
  else
  {
   return &(hs_pess_cqi_log_pkt_buf[hs_pess_cqi_log_info_buf_idx]);
  }
}
/*===========================================================================
FUNCTION dec_hs_pess_cqi_log_update

DESCRIPTION
  This function initialize mimo demod information.
  
  It is called when ever pess cqi log geneartion is started at START or RESTART
  action OR
  when ever samples generated using max required number of samples
  
DEPENDENCIES
  None

RETURN VALUE
  5 Bit unquantized value form 1 to 30

SIDE EFFECTS
  None
===========================================================================*/
void dec_hs_pess_cqi_log_update(void)
{

  /* function local variables */
  /* ------------------------ */
  /*num of conifgured carriers, initialized when CQI logging action changes to START.*/
  static uint8 num_carr = 0;
  
  /* CFN for which MIMO demod logging needs to be done */
  uint8 this_frame_cfn;

  /* pointer to amp_n_eng_est log packets */
  HS_PESS_CQI_LOG_PKT_type *this_hs_pess_cqi_log_pkt = NULL;

  /*** function code starts here ***/

  /* get CFN for the frame at which to accumulate MIMO logging information */
  this_frame_cfn = (uint8) normalize((stmr_get_combiner_cfn(TLM_GET_RXTL_SRC()) - 2), 
                                      MAX_CFN_COUNT);

  /* Handle any pending START action */
  /* ------------------------------- */

  /* Check if mimo channel analysis logging accumulation is active */
  if (!hs_pess_cqi_log_accum_active)
  {
    /* MIMO demod logging not active here */

    /* Check for LLR log action for start */
    if (hs_pess_cqi_log_action == HS_STAT_GEN_START)
    {
      /* If CFN generated above for accumulation matches start CFN */
      if (this_frame_cfn == hs_pess_cqi_log_start_cfn)
      {
        /* Start mimo logging */
        hs_pess_cqi_cfnx5_sub_fn_offset = (int16)
          normalize(hs_pess_cqi_log_start_sub_fn - (hs_pess_cqi_log_start_cfn * 5),
                    HS_MAX_GLOBAL_SUB_FRAME_COUNT);

        hs_pess_cqi_log_info_buf_idx = HS_INFO_TABLE_INVALID_VAL;              
        hs_pess_cqi_log_accum_active = TRUE;
        hs_pess_cqi_logging_enabled_qxdm = FALSE;

        /* get num of configured carriers at start*/
        num_carr = dec_hs_call_info.num_carr;

      } /* end if, start acummulation  */
    } /* end if, START action pending */
  } /* end if, stat generation not active */


  /* Check for HS Stat Stop action if QXDM is turned off */
  if ((!hs_pess_cqi_logging_enabled_qxdm) && (hs_pess_cqi_log_accum_active) &&
      ((hs_pess_cqi_log_action == HS_STAT_GEN_RESTART) ||
       (hs_pess_cqi_log_action == HS_STAT_GEN_STOP)))
  {
     /* If some samples are already accumulated, flush them */
    WL1_MSG1(HIGH , "Stopping HS pess cqi logging CFN %d", this_frame_cfn);    
    /* Time to disable mimo logging */
    if (hs_pess_cqi_log_info_buf_idx != HS_INFO_TABLE_INVALID_VAL)
    {
      dec_hs_pess_cqi_stop_logging ();
    }
    else
    {
      dec_hs_pess_cqi_log_init ();
    }
  }

  /* If the logging has been turned OFF, poll to check if it is enabled now */
  if ((!hs_pess_cqi_logging_enabled_qxdm) && (hs_pess_cqi_log_accum_active))
  {
    if (log_status(HS_PESS_CQI_LOG_PKT))
    {
      hs_pess_cqi_logging_enabled_qxdm = TRUE;
      hs_pess_cqi_log_info_buf_idx = HS_INFO_TABLE_INVALID_VAL;      
    }
  }
  
  /* Do accumulation and logging */
  /* --------------------------- */

  if ((hs_pess_cqi_log_accum_active) && (hs_pess_cqi_logging_enabled_qxdm))
  {
    /* loop index to get log info for 5 sub frames in this frame */
    uint8 index_sub_fr;
    /* get previous frame starting sub frame number */
    uint16 global_sub_fr_num;

     /* Generate global sub frame number from which accumulation will be done
       for 5 sub frames */
    global_sub_fr_num =
      (uint16)normalize((this_frame_cfn * 5) + hs_pess_cqi_cfnx5_sub_fn_offset,
                HS_MAX_GLOBAL_SUB_FRAME_COUNT);

    /* This is possible only when logging is turned OFF and then turned ON from
       within QXDM. Allocate a new buffer to start log accumulation */
    if (hs_pess_cqi_log_info_buf_idx == HS_INFO_TABLE_INVALID_VAL)
    {
      /* Allocate a new buffer to accumulate log samples */
      
      /*Get available buffer*/
      this_hs_pess_cqi_log_pkt = dec_hs_get_new_pess_cqi_buff();

      /*init fields of new pess_cqi_log*/
      dec_hs_init_pess_cqi_log(this_hs_pess_cqi_log_pkt,
                               global_sub_fr_num);

      /* Init the log buffer idx and reset action to NOOP */
      hs_pess_cqi_log_action = HS_STAT_GEN_NOOP;
      
      /*print start of logging*/
      WL1_MSG3(HIGH, "Starting HS pess cqi logging %d CFN %d SubFn %d",
                hs_pess_cqi_log_info_buf_idx,
                this_frame_cfn,
                this_hs_pess_cqi_log_pkt->starting_global_sub_fn);
    
    }
    
    /* Check if the log info buf idx is valid -- this is only to keep KW happy */
    if (hs_pess_cqi_log_info_buf_idx >= HS_PESS_CQI_LOG_NUM_BUF)
    {
      ERR_FATAL("Invalid hs_pess_cqi_log_info_buf_idx: %d", 
                hs_pess_cqi_log_info_buf_idx, 0, 0);
      return; /*lint !e527 */
    }

   

    /* Iterate over 5 sub frames to generate stats */
    for (index_sub_fr = 0;
         index_sub_fr < HS_NUM_SUB_FR_PER_RADIO_FR;
         index_sub_fr++)
    {
      uint8 carr_idx; /*carrier idx*/
      /* mDSP address from where to read the data */
      WfwHsPessCqiLogStruct* hs_pess_cqi_mdsp_log_ptr[L1_HSDPA_MAX_CARRIERS] = {NULL};
      /* this sub frame entry index into mDSP current buffer. It is indexed
              using last 4 bits of sub frame number */
      uint8 hs_decode_info_idx_mdsp;
      hs_pess_cqi_info_struct_type* hs_pess_cqi_sample_ptr = NULL;

      /* Get index of this sub frame entry into mDSP. This is the last 4
         bits of sub frame number (modulo 16 operation) */
      hs_decode_info_idx_mdsp = global_sub_fr_num & 0xF;

      /* get pointer to current HS amp and energy est debug log buffer */
      this_hs_pess_cqi_log_pkt = dec_hs_get_curr_pess_cqi_log();
      /*sanity check (KW)*/
      if(NULL == this_hs_pess_cqi_log_pkt)
      {
         WL1_MSG0(FATAL, "current pess CQI log pkt returned as NULL");
         return;
      }
      
      if(num_carr > L1_HSDPA_MAX_CARRIERS || num_carr == 0)
      {
        WL1_MSG1(ERROR, "Incorrect number of carriers# %d, configured in hs config db",
                        num_carr);
        /*num_carr is invalid (this is hit for FACH state),
          recover by using min carriers*/
        num_carr = L1_HSDPA_CARRIER_1;
      
      }
      
      for(carr_idx = 0; carr_idx < num_carr; carr_idx++)
      {
        hs_pess_cqi_mdsp_log_ptr[carr_idx] = 
               &(mcalwcdma_fwsw_intf_addr_ptr->pessCqi[hs_decode_info_idx_mdsp][carr_idx]);
      }

      /* get the log buffer current location*/
      if (hs_pess_cqi_sample_buf_offset < HS_PESS_CQI_LOG_MAX_BUF_SIZE)
      {
        hs_pess_cqi_sample_ptr = (hs_pess_cqi_info_struct_type*)
                &(this_hs_pess_cqi_log_pkt->pess_cqi_buff[hs_pess_cqi_sample_buf_offset]);
      }
      else
      {
        ERR_FATAL("Invalid hs_pess_cqi_sample_buf_offset: %d, must be less than: %d",
                   hs_pess_cqi_sample_buf_offset, HS_PESS_CQI_LOG_MAX_BUF_SIZE, 0);

        /*recover by overwritting the earlier values*/
        hs_pess_cqi_sample_buf_offset = 0;
        
        continue; /*lint !e527 */
      }


      /* Set the carrier info */
      /*0: 1 carrier, 1: 2 carriers*/
      hs_pess_cqi_sample_ptr->num_carr = num_carr-1;
 
      /* Increment the offset by 1 byte for carrier info */
      hs_pess_cqi_sample_buf_offset++;

      /*log the channel Matrix Estimation*/ 
      for(carr_idx = 0; carr_idx < num_carr; carr_idx++)
      {
        /*alias*/
        hs_pess_cqi_struct_type *pess_cqi_info_ptr;
        /*point to memory*/
        pess_cqi_info_ptr = &(hs_pess_cqi_sample_ptr->pess_cqi_info[carr_idx]);

       #if WCDMA_HSLOG_DEBUG
        WL1_MSG2(HIGH, "HS Log Debug: Pess CQI Populating log SFN = %d, Carr = %d",
                       index_sub_fr, carr_idx);
       #endif
        /*populate*/
        pess_cqi_info_ptr->cpich_snr_rake = hs_pess_cqi_mdsp_log_ptr[carr_idx]->cpichSnrRake;
        pess_cqi_info_ptr->cpich_snr_qice = hs_pess_cqi_mdsp_log_ptr[carr_idx]->cpichSnrQice;
        pess_cqi_info_ptr->cpich_snr_eq   = hs_pess_cqi_mdsp_log_ptr[carr_idx]->cpichSnrEq;
        pess_cqi_info_ptr->pnr_cnt_rake   = hs_pess_cqi_mdsp_log_ptr[carr_idx]->pnrCountRake; 
        pess_cqi_info_ptr->pnr_cnt_qice   = hs_pess_cqi_mdsp_log_ptr[carr_idx]->pnrCountQice;
        pess_cqi_info_ptr->pnr_cnt_eq     = hs_pess_cqi_mdsp_log_ptr[carr_idx]->pnrCountEq;

        pess_cqi_info_ptr->num_undo_agc_bpgs = hs_pess_cqi_mdsp_log_ptr[carr_idx]->numUndoAgcBpgs;
        pess_cqi_info_ptr->int_schd_rej_bmsk = 
                                    hs_pess_cqi_mdsp_log_ptr[carr_idx]->intermittentSchedGrpsBmsk;

        /*increment buff offset for 1 carrier cqi_struct_type size*/
        hs_pess_cqi_sample_buf_offset += sizeof(hs_pess_cqi_struct_type);
      }
      
      /* Increment number of samples accumulated */
      this_hs_pess_cqi_log_pkt->num_samples++;

      /* Handle RESTART and STOP actions */
      /* ------------------------------- */

      /* check if there action type RECONFIG and STOP set */
      if ((hs_pess_cqi_log_action == HS_STAT_GEN_RESTART) ||
          (hs_pess_cqi_log_action == HS_STAT_GEN_STOP))
      {
        /* Check if this is sub frame to STOP as the part of action STOP
           or RESTART */
        if (global_sub_fr_num == hs_pess_cqi_log_final_sub_fn)
        {
          /* STOP this accumulation now */

          WL1_MSG3(HIGH, "Stoping HS pess cqi logging %d CFN %d SubFn %d",
                   hs_pess_cqi_log_info_buf_idx,
                   this_frame_cfn,
                   global_sub_fr_num);

          /* Flush already accumulated samples */
          dec_hs_pess_cqi_stop_logging ();
          
          /* no need to further get sub frames */
          break;
        } /* End if, sub frame to stop accumulation has occured */
      } /* end if, action RESTART or STOP pending */

      /* Go to next sub frame */
      global_sub_fr_num = (uint16)normalize(global_sub_fr_num + 1, 
                                            HS_MAX_GLOBAL_SUB_FRAME_COUNT);
      
      /* If acumulation has reached max samples then this must be submitted */
      /* ------------------------------------------------------------------ */

      if (this_hs_pess_cqi_log_pkt->num_samples >= HS_NUM_MAX_PESS_CQI_SAMPLES)
      {
        /* HSDPA decode status log header pointer */
        hsdpa_log_hdr_struct_type *hs_pess_cqi_log_pkt_hdr = NULL;

        /*Get header to current log buffer and update total size of
                 information accumulated */
        hs_pess_cqi_log_pkt_hdr =(hsdpa_log_hdr_struct_type*) 
                                 (this_hs_pess_cqi_log_pkt);

        /*header of CQI_log packet + num bytes per sample per carrier*/
        hs_pess_cqi_log_pkt_hdr->len = sizeof(HS_PESS_CQI_LOG_PKT_type)
                                     - HS_PESS_CQI_LOG_MAX_BUF_SIZE
                                     + hs_pess_cqi_sample_buf_offset;

       #if WCDMA_HSLOG_DEBUG
        WL1_MSG3(HIGH, "HS Log Debug: Reached max flushing HS pess cqi log, len = %d, size %d, SubFn %d",
                           hs_pess_cqi_log_pkt_hdr->len,
                           sizeof(HS_PESS_CQI_LOG_PKT_type),
                           global_sub_fr_num);
       #endif 
        /* send command to submit this log packet */
        dec_hs_pess_cqi_send_log_submit_cmd(hs_pess_cqi_log_info_buf_idx);

        /* Get available pess cqi log accumulation buffer */
        hs_pess_cqi_log_info_buf_idx = HS_INFO_TABLE_INVALID_VAL;

        /* get pointer to current mimo debug log buffer */
        this_hs_pess_cqi_log_pkt = dec_hs_get_new_pess_cqi_buff();

        /*init cqi log fields*/
        dec_hs_init_pess_cqi_log(this_hs_pess_cqi_log_pkt,
                                  global_sub_fr_num);
        
      } /* end if, max decode status samples have been collected */
    }
  }
}

/*===========================================================================
FUNCTION dec_hs_pess_cqi_stop_logging

DESCRIPTION
  This function prepares the pess cqi buffer to be submitted to diag
  It calls the function that posts a pess cqi log submit local command and then
  clears the state of pess cqi logging.
  
DEPENDENCIES
  hs_pess_cqi_log_info_buf_idx should be valid while making
  a call to this function.

RETURN VALUE
  None

SIDE EFFECTS
  ===========================================================================*/
void dec_hs_pess_cqi_stop_logging(void)
{
  /* function local variables */
  /* ------------------------ */

  /* MIMO demod log header pointer */
  hsdpa_log_hdr_struct_type *hs_pess_cqi_log_pkt_hdr;

  /*** function code starts here ***/

  /* Populate the header */
  hs_pess_cqi_log_pkt_hdr = (hsdpa_log_hdr_struct_type*) dec_hs_get_curr_pess_cqi_log();
  /*sanity check (KW)*/
  if(NULL == hs_pess_cqi_log_pkt_hdr)
  {
    WL1_MSG0(FATAL, "current pess CQI log pkt returned as NULL");
    return;
  }
  /*header of CQI_log packet + num bytes per sample per carrier*/
  hs_pess_cqi_log_pkt_hdr->len = sizeof(HS_PESS_CQI_LOG_PKT_type)
                               - HS_PESS_CQI_LOG_MAX_BUF_SIZE
                               + hs_pess_cqi_sample_buf_offset;
   

  /* send command to submit this decode status log packet */
  dec_hs_pess_cqi_send_log_submit_cmd(
    hs_pess_cqi_log_info_buf_idx);

  /* mark HS inactive */
  hs_pess_cqi_log_accum_active = FALSE;
  hs_pess_cqi_logging_enabled_qxdm = FALSE;
 /*to avoid back to back submits when forceful flushing is done*/
  hs_pess_cqi_log_info_buf_idx = HS_INFO_TABLE_INVALID_VAL;
  /* if action was reconfig then change it to start. or
     if it was stop then it is al done, mark it NOOP */
  if (hs_pess_cqi_log_action == HS_STAT_GEN_RESTART)
  {
    hs_pess_cqi_log_action = HS_STAT_GEN_START;
  }
  else
  {
    hs_pess_cqi_log_action = HS_STAT_GEN_NOOP;
  }
}

/*===========================================================================
FUNCTION dec_hs_pess_cqi_send_log_submit_cmd

DESCRIPTION
  This function makes a request to post a local command to submit the 
  currently accumulated log packet to diag. Before it does so, it checks if 
  a previous log packet that was submitted to diag has been  serviced or not.
  If not, it drops the current packet and proceeds further.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

void dec_hs_pess_cqi_send_log_submit_cmd(
  /* HS decode status log buffer index to submit */
  uint8 hs_pess_cqi_log_idx)
{
  /* function local variables */
  /* ------------------------ */

  /* log packet pointer that need to be submitted */
  HS_PESS_CQI_LOG_PKT_type *this_hs_pess_cqi_log_pkt;

  /* log header pointer */
  hsdpa_log_hdr_struct_type *hs_pess_cqi_log_pkt_hdr;

  /*** function code starts here ***/

  if (hs_pess_cqi_log_idx >= HS_PESS_CQI_LOG_NUM_BUF)
  {
    ERR_FATAL("Incorrect index for pess cqi submit log %d", hs_pess_cqi_log_idx, 0, 0);
  }

  /* Check if there is a pending request to submit previous log packet */
  if (hs_pess_cqi_log_submit)
  {
    /* Print error message and mark this buffer as available one */

      /* get log packet pointer */
      this_hs_pess_cqi_log_pkt = &(hs_pess_cqi_log_pkt_buf[hs_pess_cqi_log_idx]);
      /* Get log packet header pointer and set log code to that */
      hs_pess_cqi_log_pkt_hdr = (hsdpa_log_hdr_struct_type*) this_hs_pess_cqi_log_pkt;
      /* submit the log packet */
      WL1_MSG3(ERROR, "hs_pess_cqi_LOG:Failed idx %d info sz %d idx pending %d",
                      hs_pess_cqi_log_idx,
                      hs_pess_cqi_log_pkt_hdr->len,
                      hs_pess_cqi_log_buf_idx_to_submit);

      hs_pess_cqi_log_buf_avail_status[hs_pess_cqi_log_idx] = TRUE;
  }
  else
  {
    /* Save HS decode status log buffer index to submit later in task context */
    hs_pess_cqi_log_submit = TRUE;
    hs_pess_cqi_log_buf_idx_to_submit = hs_pess_cqi_log_idx;

    /* Send local command to l1m */
    hs_send_submit_log_cmd();
  }
}

/*===========================================================================
FUNCTION dec_hs_pess_cqi_submit_log_buffer

DESCRIPTION
  This function is called to post HS pessimistic CQI submit command. If the 
  buffer is not ready to be submitted to diag the function returns without
  doing anything. This can so happen because multiple logs are instructed
  to be submitted to diag by the same L1 command.
  It checks for the log code in diag and submits the packet.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  Flag indicating the log buffer is submitted for flushing is reset.
===========================================================================*/
void dec_hs_pess_cqi_submit_log_buffer(void)
{
  /* function local variables */
  /* ------------------------ */

  /* flag to hold the state of the log code in QXDM */
  boolean log_code_disabled = FALSE;

  /* log packet pointer that need to be submitted */
  HS_PESS_CQI_LOG_PKT_type *this_hs_pess_cqi_log_pkt;

  /* log header pointer */
  hsdpa_log_hdr_struct_type *hs_pess_cqi_log_pkt_hdr;
  /* buffer index to submit */
  uint8 log_info_buf_idx;

  /*** function code starts here ***/

  /* If log need to be submitted then proceed otherwise simply return from here */
  if (hs_pess_cqi_log_submit)
  {
    /* save log buffer index to submit */
    log_info_buf_idx = hs_pess_cqi_log_buf_idx_to_submit;
  }
  else
  {
    return;
  }

  if (log_info_buf_idx >= HS_PESS_CQI_LOG_NUM_BUF)
  {
    ERR_FATAL("Incorrect index for pess cqi submit buffer %d", log_info_buf_idx, 0, 0);
  }

  if (log_status(HS_PESS_CQI_LOG_PKT))
    {
      /* get log packet pointer */
      this_hs_pess_cqi_log_pkt = &(hs_pess_cqi_log_pkt_buf[log_info_buf_idx]);
      /* Get log packet header pointer and set log code to that */
      hs_pess_cqi_log_pkt_hdr = (hsdpa_log_hdr_struct_type*) this_hs_pess_cqi_log_pkt;
      hs_pess_cqi_log_pkt_hdr->code = HS_PESS_CQI_LOG_PKT;
        
      /* submit the log packet */
      if (!log_submit((PACKED void *) this_hs_pess_cqi_log_pkt))
      {
        WL1_MSG1(ERROR, "HS_PESS_CQI_LOG:Failed info sz %d",
                        hs_pess_cqi_log_pkt_hdr->len);
      }
    }
  else
    {
      /* Mark the flag to indicate that the log code is disabled */
      log_code_disabled = TRUE;
    }

  HS_LOG_INTLOCK();
  /* Mark this LLR status log buffer to available */
  hs_pess_cqi_log_buf_avail_status[log_info_buf_idx] = TRUE;
  /* reset log to submit flag */
  hs_pess_cqi_log_submit = FALSE;

  /* If the log code is disabled, clear all buffers and mark logging state
     as well as logging buffer index as invalid */
  if (log_code_disabled)
  {
    uint8 index_buf;
    
    /* Mark logging OFF by QXDM */
    hs_pess_cqi_logging_enabled_qxdm = FALSE;
    /* Mark the log buffer index as invalid */
    hs_pess_cqi_log_info_buf_idx = HS_INFO_TABLE_INVALID_VAL;    
    /* iterate over all log buffer status to mark them available */
    for (index_buf = 0; index_buf < 2; index_buf++)
    {
      hs_pess_cqi_log_buf_avail_status[index_buf] = TRUE;
    }
  }

  HS_LOG_INTFREE();
}

/*===========================================================================
FUNCTION dec_hs_decode_status_send_log_submit_cmd

DESCRIPTION
  This function is called to post HS log submit command. There is only one HS
  log submit command. HS cfg module calls all accumlated logging entities. Each
  entity is reponsible for maintaining log submit pending information. It keeps
  this information for HS decode status log submit pending information
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

void dec_hs_decode_status_send_log_submit_cmd(
  /* HS decode status log buffer index to submit */
  uint8 decode_status_info_buf_idx)
{
  /* function local variables */
  /* ------------------------ */

  /* decode status log packet pointer that need to be submitted */
  HS_DECODE_STATUS_LOG_PKT_type *this_hs_decode_status_log;
  /* decode status log packet header pointer */
  hsdpa_log_hdr_struct_type *hs_decode_status_log_pkt_hdr;

  /*** function code starts here ***/

  /* Check if there is a pending request to submit previous log packet */
  if (hs_decode_status_log_submit)
  {
    /* Print error message and mark this buffer as available one */

    /* get log packet pointer */
    this_hs_decode_status_log =
      &(hs_decode_status_log[decode_status_info_buf_idx]);
    /* Get log packet header pointer and set log code to that */
    hs_decode_status_log_pkt_hdr = (hsdpa_log_hdr_struct_type*) this_hs_decode_status_log;

    WL1_MSG3(ERROR, "HS_DEC_STATUS_LOG:Failed idx %d info sz %d idx %d pending",
                    decode_status_info_buf_idx,
                    hs_decode_status_log_pkt_hdr->len,
              hs_decode_status_log_buf_idx_to_submit);

    /* Mark this decode status log buffer to available */
    hs_decode_status_log_buf_avail_status[decode_status_info_buf_idx] = TRUE;
  }
  else
  {
    /* Save HS decode status log buffer index to submit later in task context */
    hs_decode_status_log_submit = TRUE;
    hs_decode_status_log_buf_idx_to_submit = decode_status_info_buf_idx;

    /* Send local command to l1m */
    hs_send_submit_log_cmd();
  }
}

/*===========================================================================
FUNCTION dec_hs_decode_status_submit_log_buffer

DESCRIPTION
  This function is called from hscfg module in response of HS log submit
  command. It checks for hs decode status log submit flag. It is set to TRUE,
  it submit log buffer pointed by log buffer index to submit.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

void dec_hs_decode_status_submit_log_buffer(void)
{
  /* function local variables */
  /* ------------------------ */

  /* decode status log packet pointer that need to be submitted */
  HS_DECODE_STATUS_LOG_PKT_type *this_hs_decode_status_log;
  /* decode status log packet header pointer */
  hsdpa_log_hdr_struct_type *hs_decode_status_log_pkt_hdr;
  /* decode status log buffer index to submit */
  uint8 decode_status_info_buf_idx;

  /*** function code starts here ***/

  /* If HS decode status log status need to be submitted then proceed otherwise
     simply return from here */
  if (hs_decode_status_log_submit)
  {
    /* save decode status log buffer index to submit */
    decode_status_info_buf_idx = hs_decode_status_log_buf_idx_to_submit;
  }
  else
  {
    return;
  }

  if (log_status(HS_DECODE_STATUS_LOG_PKT))
  {
    /* Check if HS_CFG_LOG_PKT has been already been submitted */
    if (hs_cfg_log_pkt_pending == TRUE)
    {
      hs_send_cfg_log_pkt();
    }

    /* get log packet pointer */
    this_hs_decode_status_log =
      &(hs_decode_status_log[decode_status_info_buf_idx]);
    /* Get log packet header pointer and set log code to that */
    hs_decode_status_log_pkt_hdr = (hsdpa_log_hdr_struct_type*) this_hs_decode_status_log;
    hs_decode_status_log_pkt_hdr->code = HS_DECODE_STATUS_LOG_PKT;
    /* submit the log packet */
    if (!log_submit((PACKED void *) this_hs_decode_status_log))
    {
      WL1_MSG1(ERROR, "HS_DEC_STATUS_LOG:Failed info sz %d",
                      hs_decode_status_log_pkt_hdr->len);
    }
  }

  HS_LOG_INTLOCK();
  /* Mark this decode status log buffer to available */
  hs_decode_status_log_buf_avail_status[decode_status_info_buf_idx] = TRUE;
  /* reset HS decode status log to submit flag */
  hs_decode_status_log_submit = FALSE;
  HS_LOG_INTFREE();
}


/*===========================================================================
FUNCTION dec_hs_llr_log_init

DESCRIPTION
  This function initializes all the variables and states associated with HS
  Amplitude and energy Estimation log packet. 
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  All state variables initialized to the default values.
===========================================================================*/

void dec_hs_llr_log_init(void)
{
  /* function local variables */
  /* ------------------------ */

  /* loop index */
  uint32 index_buf;
  
  /*** function code starts here ***/

  HS_LOG_INTLOCK();

  hs_llr_logging_enabled_qxdm = FALSE;
  
  /*RESET Init Flag*/  
  hs_llr_refresh_info_flag = TRUE;

  /*llr log final sfn*/  
  hs_llr_log_final_sub_fn = INVALID_STOP_GSFN;

  /* iterate over all log buffer status to mark them available */
  for (index_buf = 0; index_buf < HS_AMP_ENG_LOG_NUM_BUF; index_buf++)
  {
    hs_llr_log_buf_avail_status[index_buf] = TRUE;
  }

  /* Set log buffer index to INVALID */
  hs_llr_log_info_buf_idx = HS_INFO_TABLE_INVALID_VAL;

  /* Init log submit request to FALSE */
  hs_llr_log_submit = FALSE;
  hs_llr_log_buf_idx_to_submit = HS_INFO_TABLE_INVALID_VAL;

  HS_LOG_INTFREE();
}

/*===========================================================================
FUNCTION dec_hs_llr_stop_logging

DESCRIPTION
  This function prepares the HS LLR logging buffer to be submitted to diag
  It calls the function that posts a LLR log submit local command and then
  clears the state of LLR logging.
  
DEPENDENCIES
  hs_llr_log_info_buf_idx should be valid while making
  a call to this function.

RETURN VALUE
  None

SIDE EFFECTS
  HS LLR logging states cleared
===========================================================================*/

void dec_hs_llr_stop_logging(void)
{
  /* function local variables */
  /* ------------------------ */
  /* pointer to amp_n_eng_est log packets */
  HS_AMP_ENG_EST_LOG_PKT_type *this_hs_amp_eng_est_log_pkt;

  /* HSDPA LLR log header pointer */
  hsdpa_log_hdr_struct_type *hs_llr_log_pkt_hdr;

  /*** function code starts here ***/

  /* get pointer to current HS amp and energy est debug log buffer */
  this_hs_amp_eng_est_log_pkt = dec_hs_get_curr_llr_log();
  /*sainty check (KW)*/
  if(NULL == this_hs_amp_eng_est_log_pkt)
  {
    WL1_MSG0(FATAL, "current HS A&E log packet returned as NULL");
	return;
  }
  /* Populate the header */
  hs_llr_log_pkt_hdr = (hsdpa_log_hdr_struct_type*) this_hs_amp_eng_est_log_pkt;

  hs_llr_log_pkt_hdr->len = sizeof(HS_AMP_ENG_EST_LOG_PKT_type) 
                          - MAX_LLR_BUFFER_SIZE 
                          + (hs_llr_sample_buf_offset);
          
  /* send command to submit this decode status log packet */
  dec_hs_llr_send_log_submit_cmd(hs_llr_log_info_buf_idx);

  hs_llr_logging_enabled_qxdm = FALSE;

}

/*===========================================================================
FUNCTION dec_hs_get_new_llr_log_buff

DESCRIPTION
finds and returns a free llr buffer 

DEPENDENCIES
  None

RETURN VALUE
  HS_AMP_ENG_EST_LOG_PKT_type pointer

SIDE EFFECTS
  None
===========================================================================*/

HS_AMP_ENG_EST_LOG_PKT_type* dec_hs_get_new_llr_log_buff(void)
{
  uint8 index_buf;
  /* Get available HS amp and energy est acumulation buffer */
  for (index_buf = 0; index_buf < HS_AMP_ENG_LOG_NUM_BUF; index_buf++)
  {
    if (hs_llr_log_buf_avail_status[index_buf])
    {
      hs_llr_log_buf_avail_status[index_buf] = FALSE;
      hs_llr_log_info_buf_idx = index_buf;
      break;
    }
  }

  /* If can't get buffer then there is no use of proceeding. It should
     never happen because of double buffering */
  if (hs_llr_log_info_buf_idx >= HS_AMP_ENG_LOG_NUM_BUF)
  {
    WL1_MSG0(ERROR, "Can't get HS LLR log buf");

    /* iterate over all log buffer status to mark them available */
    for (index_buf = 0; index_buf < HS_AMP_ENG_LOG_NUM_BUF; index_buf++)
    {
      hs_llr_log_buf_avail_status[index_buf] = TRUE;
    }

    /* Set log buffer index to 0 */
    hs_llr_log_info_buf_idx = 0;
    hs_llr_log_buf_avail_status[hs_llr_log_info_buf_idx] = FALSE;

    /* Init log submit request to FALSE */
    hs_llr_log_submit = FALSE;
    
  }

  /* get pointer to current HS amp and energy est debug log buffer */

  return (&(hs_llr_log_pkt_buf[hs_llr_log_info_buf_idx]));

}
/*===========================================================================
FUNCTION dec_hs_init_llr_log

DESCRIPTION
Initalize llr log fields

DEPENDENCIES
  None

RETURN VALUE
  void

SIDE EFFECTS
  None
===========================================================================*/

void dec_hs_init_llr_log(HS_AMP_ENG_EST_LOG_PKT_type* this_hs_amp_eng_est_log_pkt)
{
  /* Do time stamp corresponding to first sample */
  log_set_timestamp(this_hs_amp_eng_est_log_pkt);
  /* set the version*/
  this_hs_amp_eng_est_log_pkt->version        = 7;
  /* init number of samples accumulated to 0 */
  this_hs_amp_eng_est_log_pkt->num_samples    = 0;
  /* sample period */
  this_hs_amp_eng_est_log_pkt->sample_period  = 1;
  /* Number of samples for subframe */
  this_hs_amp_eng_est_log_pkt->num_samples_sf = MAX_AMP_ENG_MEAS_PER_SAMPLE;

  /* counter to denote the current location of the Buffer */
  hs_llr_sample_buf_offset = 0;

return;      
}
/*===========================================================================
FUNCTION dec_hs_init_llr_log

DESCRIPTION
Initalize llr log fields

DEPENDENCIES
  None

RETURN VALUE
  void

SIDE EFFECTS
  None
===========================================================================*/

HS_AMP_ENG_EST_LOG_PKT_type* dec_hs_get_curr_llr_log(void)
{
  if(hs_llr_log_info_buf_idx >= HS_AMP_ENG_LOG_NUM_BUF)
  {
    ERR_FATAL("Invalid dec hs get current llr log buffer idx = %d",
               hs_llr_log_info_buf_idx,0,0);

    return NULL;
  }
  else
  {
    return (&(hs_llr_log_pkt_buf[hs_llr_log_info_buf_idx]));
  }
}

/*===========================================================================
FUNCTION dec_hs_llr_log_populate

DESCRIPTION
Populates SW LLR buffer by reading FW buffer, populates 1 subframe worth of data.
Flush the SW log packet when reaches the max limit.

DEPENDENCIES
  None

RETURN VALUE
  void

SIDE EFFECTS
  None
===========================================================================*/
void dec_hs_llr_log_populate(WfwDbackHsAeLogAsyncReadStruct *hs_ae_fw_log_buf_ptr,
                                   uint8 hs_ae_log_rd_ptr,
                                   uint8 num_carr)
{

  /* mDSP address from where to read the data */
  WfwDbackHsAeLogStruct *hs_llr_log_mdsp_ptr    = NULL;
  amp_eng_info_type     *amp_eng_info_ptr       = NULL;
  demod_info_type       *amp_eng_demod_info_ptr = NULL;

  HS_AMP_ENG_EST_LOG_PKT_type *this_hs_amp_eng_est_log_pkt = NULL;
  /* sample count index*/
  uint8 sample_idx;
  uint8 carr_idx;

  /*** function code starts here ***/
  /*get current buffer in use*/
  this_hs_amp_eng_est_log_pkt = dec_hs_get_curr_llr_log();

  /*sanity check (KW)*/
  if(NULL == this_hs_amp_eng_est_log_pkt)
  {
    WL1_MSG0(FATAL, "current hs A&E log buff returned as NULL");
    return;
  }
  if (hs_ae_log_rd_ptr >= WFW_DEMOD_HS_AE_LOG_PACKET_BUF_SIZE)
  {
    WL1_MSG2(ERROR, "hs_ae_log_rd_ptr %d exceeds bounds %d",
                    hs_ae_log_rd_ptr, WFW_DEMOD_HS_AE_LOG_PACKET_BUF_SIZE);
  }
  
  /*ensure hs_llr_sample_buf_offset doesn't exceed max buffer size*/
  if (hs_llr_sample_buf_offset > MAX_LLR_BUFFER_SIZE)
  {
    WL1_MSG2(ERROR, "hs_llr_sample_buf_offset %d exceeded bounds %d",
                    hs_llr_sample_buf_offset, MAX_LLR_BUFFER_SIZE);
    hs_llr_sample_buf_offset = 0;
    this_hs_amp_eng_est_log_pkt->num_samples = 0;
    
  }

  /*typecast the log buffer to a structure*/
  amp_eng_info_ptr = (amp_eng_info_type*)
                     (&(this_hs_amp_eng_est_log_pkt->llr_buffer[hs_llr_sample_buf_offset]));

  /*0: 1C .... 3: 4C hence decrement by 1*/
  amp_eng_info_ptr->num_carr  = num_carr - 1;
  /*copy sub frame number from mdsp*/
  amp_eng_info_ptr->sf_number = hs_ae_fw_log_buf_ptr->dbackHsAeLog[hs_ae_log_rd_ptr][0].hsSubFrmNum;

  /*populate demodulation info per carrier*/
  for(carr_idx = 0; carr_idx < num_carr; carr_idx++)
  {
    amp_eng_demod_info_ptr = &(amp_eng_info_ptr->demod_info_per_carr[carr_idx]);
    /*alias of mdsp llr log per carrier*/
    hs_llr_log_mdsp_ptr = &(hs_ae_fw_log_buf_ptr->dbackHsAeLog[hs_ae_log_rd_ptr][carr_idx]);

    amp_eng_demod_info_ptr->hsdpa_modulation = hs_llr_log_mdsp_ptr->modScheme;
    amp_eng_demod_info_ptr->demfrnt_mode     = hs_llr_log_mdsp_ptr->demfrontMode;
    amp_eng_demod_info_ptr->new_trans        = hs_llr_log_mdsp_ptr->newTrans;
    amp_eng_demod_info_ptr->td_scale         = hs_llr_log_mdsp_ptr->tdScale;

    /*copy Amp and eng for 5 samples for each carrier*/
    for( sample_idx = 0; 
         sample_idx < MAX_AMP_ENG_MEAS_PER_SAMPLE;
         sample_idx++)
    {
      amp_eng_type *amp_eng_per_carr;
      amp_eng_per_carr = &(amp_eng_demod_info_ptr->amp_eng_per_carr[sample_idx]);

      amp_eng_per_carr->amp_est = hs_llr_log_mdsp_ptr->Ae[sample_idx].ampEst;
      amp_eng_per_carr->eng_est = hs_llr_log_mdsp_ptr->Ae[sample_idx].energyEst;
    }
         
  }
  
  /*increment the buff offset with number of bytes used*/
 #if WCDMA_HSLOG_DEBUG
  WL1_MSG3(HIGH, "LLR Buf offset = %d, num carr = %d, max size =%d",
                 hs_llr_sample_buf_offset, num_carr, MAX_LLR_BUFFER_SIZE);
 #endif

  hs_llr_sample_buf_offset += SIZE_OF_AMP_ENG_LOG_PER_SAMPLE(num_carr);

  /* Increment number of samples accumulated */
  this_hs_amp_eng_est_log_pkt->num_samples++;

  /* If acumulation has reached max samples then this must be submitted */
  /* ------------------------------------------------------------------ */

  if (HS_NUM_MAX_HS_AMP_ENG_SAMPLES == this_hs_amp_eng_est_log_pkt->num_samples)
  {
    /* HSDPA decode status log header pointer */
    hsdpa_log_hdr_struct_type *hs_llr_log_pkt_hdr;

    /* Get header to current log buffer and update total size of
       information accumulated */
    hs_llr_log_pkt_hdr =  (hsdpa_log_hdr_struct_type*) (this_hs_amp_eng_est_log_pkt);

    hs_llr_log_pkt_hdr->len = sizeof(HS_AMP_ENG_EST_LOG_PKT_type) 
                            - MAX_LLR_BUFFER_SIZE 
                            + (hs_llr_sample_buf_offset);
    #if WCDMA_HSLOG_DEBUG   
    WL1_MSG3(HIGH, "Reched max flushing HS LLR log  SubFn %d, Len = %d, size = %d",
             amp_eng_info_ptr->sf_number,
             hs_llr_log_pkt_hdr->len,
             sizeof(HS_AMP_ENG_EST_LOG_PKT_type));
    #endif
    /* send command to submit this decode status log packet */
    dec_hs_llr_send_log_submit_cmd(hs_llr_log_info_buf_idx);

    /* Get available HS status accumulation buffer */
    this_hs_amp_eng_est_log_pkt = dec_hs_get_new_llr_log_buff();

    /*Initalize the log values and reset "hs_llr_sample_buf_offset" to 0;*/
    dec_hs_init_llr_log(this_hs_amp_eng_est_log_pkt);
    
  } /* end if, max decode status samples have been collected */

}
/*===========================================================================
FUNCTION dec_hs_llr_update_log

DESCRIPTION
  This function is responsible for allocating and initializing the log buffer
  to start the log accumulation. It then accumulates samples into the buffer
  every frame until the buffer is full or the logging stops. In the case of 
  full buffer, it is submitted to diag and a new buffer is allocated. 
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
void dec_hs_llr_update_log(boolean force_flush)
{
  /* function local variables */
  /* ------------------------ */
  /*num of conifgured carriers, initialized when llr logging action changes to START.*/
  static uint8 num_carr = 0;
  uint16 global_sub_fr_num;
  uint32 rd_idx, wr_idx;

  /*** function code starts here ***/  
  wr_idx = mcalwcdma_fwsw_intf_addr_ptr->dbackHsAeLogAsyncReadBuf.wrPtr;
  rd_idx = mcalwcdma_fwsw_intf_addr_ptr->dbackHsAeLogRdPtr;
  
  global_sub_fr_num = mcalwcdma_fwsw_intf_addr_ptr->dbackHsAeLogAsyncReadBuf\
                                                  .dbackHsAeLog[rd_idx][0].hsSubFrmNum;
  
  /* If the logging has been turned OFF, poll to check if it is enabled now */
  if (FALSE == hs_llr_logging_enabled_qxdm)
  {
    if (log_status(HS_AMP_ENG_EST_LOG_PKT))
    {
      hs_llr_logging_enabled_qxdm = TRUE;
    }
    else
    {
      /*Log disabled from QXDM*/
      /*since logging could have been disabled from QXDM while HS is up 
             and can be re-enabled before hs is re-initialized*/
             
      /*set init flag*/
      hs_llr_refresh_info_flag = TRUE;
      
      /*invalidate*/
      hs_llr_log_final_sub_fn = INVALID_STOP_GSFN;
      
      #if (WCDMA_HSLOG_DEBUG)
      WL1_MSG3(HIGH, "llr_log disabled from QXDM rd_idx %d wr_idx %d sfn %d",
                     rd_idx, wr_idx, global_sub_fr_num);
      #endif
      
      /* If the logging has been turned OFF, then just set RdPtr
         equal to WrPtr and skip reading the log buffer contents to avoid Buffer overflow*/
      mcalwcdma_fwsw_intf_addr_ptr->dbackHsAeLogRdPtr =
                            mcalwcdma_fwsw_intf_addr_ptr->dbackHsAeLogAsyncReadBuf.wrPtr;

      return;
    }

  }
   /*Flush if force_flush is true, Fore_flush is true during HS tear down
   This is required since the STOP GSFN reproted by FW is not necessarily 
   the last SFN recorded by A&E log packet (only logged when PDSCH is active) */
  if ((TRUE == force_flush) && 
    /*flush only there is a log to flush*/
      (hs_llr_log_info_buf_idx != HS_INFO_TABLE_INVALID_VAL) &&
      /*and has not been stopped earlier*/
      (hs_llr_log_final_sub_fn != INVALID_STOP_GSFN))
  {
    /* STOP this accumulation now */
    /* Flush already accumulated samples */
    dec_hs_llr_stop_logging ();
      
    MSG_HIGH("Stopping dec hs llr logging %d SubFn %d",
             hs_llr_log_info_buf_idx,
             global_sub_fr_num,0);
      
    hs_llr_refresh_info_flag = TRUE;
    
    /*invalidate*/
    hs_llr_log_final_sub_fn = INVALID_STOP_GSFN;
  }

  #if(WCDMA_HSLOG_DEBUG)
  WL1_MSG3(HIGH, "LLR update rd_idx = %d, wr_idx = %d, sfn = %d",
                 rd_idx, wr_idx, global_sub_fr_num);
  #endif
  
  
  while(rd_idx != wr_idx)
  {
    global_sub_fr_num = mcalwcdma_fwsw_intf_addr_ptr->dbackHsAeLogAsyncReadBuf\
                                                  .dbackHsAeLog[rd_idx][0].hsSubFrmNum;
    if(TRUE == hs_llr_refresh_info_flag)
    {
      HS_AMP_ENG_EST_LOG_PKT_type* this_hs_amp_eng_est_log_pkt;

      /*get num of configured carriers only once during start
            and use this till LLR action (HS action) is reconfigured*/
      num_carr = dec_hs_call_info.num_carr;

      if(num_carr == 0 || num_carr > L1_HSDPA_MAX_CARRIERS)
      {
        WL1_MSG1(FATAL, "HS LLR logging initalized with #carr = %d", num_carr);
      }

      /* Get available HS status accumulation buffer */
      this_hs_amp_eng_est_log_pkt = dec_hs_get_new_llr_log_buff();

      /*Initalize the log values and reset "hs_llr_sample_buf_offset" to 0;*/
      dec_hs_init_llr_log(this_hs_amp_eng_est_log_pkt);

      hs_llr_refresh_info_flag = FALSE;

      WL1_MSG2(HIGH, "Init LLR logging SFN = %d, #carr = %d",
                     global_sub_fr_num, num_carr);

    }

    /*populate*/
    dec_hs_llr_log_populate(&(mcalwcdma_fwsw_intf_addr_ptr->dbackHsAeLogAsyncReadBuf),
                            rd_idx,
                            num_carr);
    
    if(global_sub_fr_num == hs_llr_log_final_sub_fn)
    {
      /* STOP this accumulation now */
      /* Flush already accumulated samples */
      dec_hs_llr_stop_logging ();
        
      WL1_MSG2(HIGH, "Stopping dec hs llr logging %d SubFn %d",
                     hs_llr_log_info_buf_idx, global_sub_fr_num);
        
      hs_llr_refresh_info_flag = TRUE;
      
      /*invalidate*/
      hs_llr_log_final_sub_fn = INVALID_STOP_GSFN;
    }
    
    rd_idx = (rd_idx + 1) % WFW_DEMOD_HS_AE_LOG_PACKET_BUF_SIZE;
  }
  /*update read ptr*/
  mcalwcdma_fwsw_intf_addr_ptr->dbackHsAeLogRdPtr = rd_idx;

  return;  
}

/*===========================================================================
FUNCTION dec_hs_llr_send_log_submit_cmd

DESCRIPTION
  This function makes a request to post a local command to submit the 
  currently accumulated log packet to diag. Before it does so, it checks if 
  a previous log packet that was submitted to diag has been  serviced or not.
  If not, it drops the current packet and proceeds further.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

void dec_hs_llr_send_log_submit_cmd(
  /* HS decode status log buffer index to submit */
  uint8 hs_llr_log_idx)
{
  /* function local variables */
  /* ------------------------ */

  /* log packet pointer that need to be submitted */
  HS_AMP_ENG_EST_LOG_PKT_type *this_hs_amp_eng_est_log_pkt;

  /* log header pointer */
  hsdpa_log_hdr_struct_type *hs_llr_log_pkt_hdr;

  /*** function code starts here ***/

  /* Check if there is a pending request to submit previous log packet */
  if (hs_llr_log_submit)
  {
    /* Print error message and mark this buffer as available one */

      /* get log packet pointer */
      this_hs_amp_eng_est_log_pkt = &(hs_llr_log_pkt_buf[hs_llr_log_idx]);
      /* Get log packet header pointer and set log code to that */
      hs_llr_log_pkt_hdr = (hsdpa_log_hdr_struct_type*) this_hs_amp_eng_est_log_pkt;
      /* submit the log packet */
      WL1_MSG3(ERROR, "HS_LLR_INFO_LOG:Failed idx %d info sz %d idx pending %d",
                      hs_llr_log_idx,
                      hs_llr_log_pkt_hdr->len,
                      hs_llr_log_buf_idx_to_submit);

      hs_llr_log_buf_avail_status[hs_llr_log_idx] = TRUE;
  }
  else
  {
    /* Save HS decode status log buffer index to submit later in task context */
    hs_llr_log_submit = TRUE;
    hs_llr_log_buf_idx_to_submit = hs_llr_log_idx;

    /* Send local command to l1m */
    hs_send_submit_log_cmd();
  }
}

/*===========================================================================
FUNCTION dec_hs_llr_submit_log_buffer

DESCRIPTION
  This function is called from hscfg module in response of HS log submit
  command. It checks for HS AMP and ENG estimation log submit flag. 
  If it is set to TRUE, it submit log buffer pointed by log buffer index 
  to submit.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

void dec_hs_llr_submit_log_buffer(void)
{
  /* function local variables */
  /* ------------------------ */

  /* flag to hold the state of the log code in QXDM */
  boolean log_code_disabled = FALSE;

  /* log packet pointer that need to be submitted */
  HS_AMP_ENG_EST_LOG_PKT_type *this_hs_amp_eng_est_log_pkt;

  /* log header pointer */
  hsdpa_log_hdr_struct_type *hs_llr_log_pkt_hdr;
  /* buffer index to submit */
  uint8 log_info_buf_idx;

  /*** function code starts here ***/

  /* If log need to be submitted then proceed otherwise simply return from here */
  if (hs_llr_log_submit)
  {
    /* save log buffer index to submit */
    log_info_buf_idx = hs_llr_log_buf_idx_to_submit;
  }
  else
  {
    return;
  }

  if (log_status(HS_AMP_ENG_EST_LOG_PKT))
    {
      /* get log packet pointer */
      this_hs_amp_eng_est_log_pkt = &(hs_llr_log_pkt_buf[log_info_buf_idx]);
      /* Get log packet header pointer and set log code to that */
      hs_llr_log_pkt_hdr = (hsdpa_log_hdr_struct_type*) this_hs_amp_eng_est_log_pkt;
      hs_llr_log_pkt_hdr->code = HS_AMP_ENG_EST_LOG_PKT;
      /* submit the log packet */
      if (!log_submit((PACKED void *) this_hs_amp_eng_est_log_pkt))
      {
        WL1_MSG1(ERROR, "HS_LLR_INFO_LOG:Failed info sz %d",
                        hs_llr_log_pkt_hdr->len);
      }
    }
  else
    {
      /* Mark the flag to indicate that the log code is disabled */
      log_code_disabled = TRUE;
    }

  HS_LOG_INTLOCK();
  /* Mark this LLR status log buffer to available */
  hs_llr_log_buf_avail_status[log_info_buf_idx] = TRUE;
  /* reset log to submit flag */
  hs_llr_log_submit = FALSE;

  /* If the log code is disabled, clear all buffers and mark logging state
     as well as logging buffer index as invalid */
  if (log_code_disabled)
  {
    uint32 index_buf;
    
    /* Mark logging OFF by QXDM */
    hs_llr_logging_enabled_qxdm = FALSE;
    /* Mark the log buffer index as invalid */
    hs_llr_log_info_buf_idx = HS_INFO_TABLE_INVALID_VAL;    
    
    /*RESET Init Flag need to init llr if bux idx is invalidated*/  
    hs_llr_refresh_info_flag = TRUE;
    /* iterate over all log buffer status to mark them available */
    for (index_buf = 0; index_buf < HS_AMP_ENG_LOG_NUM_BUF; index_buf++)
    {
      hs_llr_log_buf_avail_status[index_buf] = TRUE;
    }
  }

  HS_LOG_INTFREE();
}
/* ---------------------------------------------------------- */
/* Following functions handle UL HS DPCCH information logging */
/* ---------------------------------------------------------- */
/*===========================================================================
FUNCTION hslog_enchs_init

DESCRIPTION

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS

===========================================================================*/

void hslog_enchs_init()
{
  /* Initialize average CQI and num samples */
  enchs_avg_cqi = ENCHS_AVG_CQI_INVALID;
  enchs_avg_cqi_num_sample = ENCHS_INVALID_NUM_SAMPLE_FOR_AVG_CQI;

  wfw_intf_tx_hs_async_read_ptr = &(mcalwcdma_fwsw_intf_addr_ptr->txAsyncReadBuf);
}

/*===========================================================================
FUNCTION enchs_logging_init

DESCRIPTION
  This function initializes the variables required for accumulation
  and logging of HS UL logging. It initialize the following variables

  enchs_log_accum_active is set to FALSE to indicate inactive
  enchs_log_action is set to NOOP
  All HSDPA UL log buffers are set to available
  Log to submit request id set to FALSE

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  All above mentioned variables in descriptions are inited
===========================================================================*/

void enchs_logging_init(void)
{
  /* function local variables */
  /* ------------------------ */

  /* loop index */
  uint8 index_buf;

  /*** function code starts here ***/

  /* HSDPA UL log accumulation and logging is inactive */
  enchs_log_accum_active = FALSE;
  enchs_dpcch_logging_on = FALSE;

  /* Initialize average CQI and num samples */
  enchs_avg_cqi = ENCHS_AVG_CQI_INVALID;
  enchs_avg_cqi_num_sample = ENCHS_INVALID_NUM_SAMPLE_FOR_AVG_CQI;

  /* HS UL DPCCH accumulation and logging action is NOOP */
  enchs_log_action = ENCHS_LOG_NOOP;

  /* iterate over all log buffer status to mark them available */
  for (index_buf = 0; index_buf < ENCHS_LOG_NUM_BUF; index_buf++)
  {
    enchs_dpcch_log_buf_avail_status[index_buf] = TRUE;
  }

  /* Set mDSP UL log buffer table index to IONVALID */
  enchs_log_info_table_idx = HS_INFO_TABLE_INVALID_VAL;
  enchs_log_start_info_table_idx = HS_INFO_TABLE_INVALID_VAL;
  enchs_log_info_buf_idx = HS_INFO_TABLE_INVALID_VAL;

  /* Init log submit request to FALSE */
  enchs_dpcch_log_submit = FALSE;

  /* init CQI estimate stat variables */
  enchs_cqi_estimate_stat_cur_segment = 0;
  enchs_cqi_estimate_stat_cur_segment_num_samples = 0;
  enchs_cqi_estimate_stat_num_seg_accum = 0;
  memset(enchs_cqi_estimate_stat_buffer, 0x00, sizeof(enchs_cqi_estimate_stat_buffer));
  memset(enchs_num_new_tx_rece_cqi_est_seg, 0x00, sizeof(enchs_num_new_tx_rece_cqi_est_seg));
  memset(enchs_num_new_tx_err_cqi_est_seg, 0x00, sizeof(enchs_num_new_tx_err_cqi_est_seg));
  /*Initialize the eul dpch logpkt HS logging control variables*/
  enchs_eul_dpch_logpkt_hs_logging_init();
}

/*===========================================================================
FUNCTION enchs_log_do_action

DESCRIPTION
  This function changes HSDPA UL logging mode. Before it commit action
  provided function argument, it validates if that action can be accepted.
  Current action should be always NOOP that is reset at start and after every
  action completion

  When logging is inactive, only action that is accepted is START.
  When logging is active it can be RESTARTED or STOPPED

  There are 2 buffer for accumulating HS UL logging. These are used when
  a existing accumulation need to be submitted for logging. It happens when
  accumulation is done or there is reconfiguration and previous configuration
  need to be flushed.

DEPENDENCIES
  None

RETURN VALUE
  TRUE or FALSE based on action accept validation

SIDE EFFECTS
  None
===========================================================================*/

boolean enchs_log_do_action(
  /* Action for HS UL accumulation and logging */
  enchs_log_action_enum_type action,
  /* Sub frame number at which to start or restart */
  uint16 start_sub_fn,
  /* start CFN to start or restart accumulation/logging */
  uint16 start_cfn,
  /* mDSP info table index from where to read the information */
  uint8  info_table_index,
  /* Last sub frame number after which information is submitted for logging
     then either restarted ot stopped based on action */
  uint16 final_sub_fn,
   /*dpcch offset */
  uint8 dpcch_offset,
  /*num of configured carriers*/
  uint8 num_config_carriers
  )
{
  /* function local variables */
  /* ------------------------ */

  /* indicate that previous action was pending, need to convert input action
     to realign with pending action */
  boolean action_oride_happened = FALSE;

  /* indicates whether action was accepted or not */
  boolean log_action_status = TRUE;

  /*** function code starts here ***/

  /* Validate action if it is doable */
  /* ------------------------------- */

  /* Check if this function is called with action type NOOP. There is
     no use of calling this function with action NOOP. So print MSG_ERROR
     as warning */
  if (action == ENCHS_LOG_NOOP)
  {
    WL1_MSG0(ERROR, "ENCHS log do action with NOOP, ignoring");
    return TRUE;
  }

  HS_LOG_INTLOCK ();

  /* Check if current action is NOOP or not. When this function is called
     it is expected that current action be NOOP */

  if (enchs_log_action != ENCHS_LOG_NOOP)
  {
    action_oride_happened = TRUE;

    /* Some other action already going on, need to realign
       older action and update the next log action appropriately */
    WL1_MSG2(ERROR, "ENCHS state action %d still pending, new action %d, realigning",
              enchs_log_action, action);

    switch (enchs_log_action)
    {
    case ENCHS_LOG_START:
      if (action == ENCHS_LOG_RESTART)
      {
        /* Change the action to start so that logging will start at the
           start of RESLAM start */
        action = ENCHS_LOG_START;
      }
      else if (action == ENCHS_LOG_STOP)
      {
        /* Received a stop action before the logging could start.
           Ignore the request to start logging and return */
        action = ENCHS_LOG_NOOP;
      }
      break;

    case ENCHS_LOG_RESTART:
      if (action == ENCHS_LOG_RESTART)
      {
        /* Override the stop of new reslam with stop of old reslam
           Logging would stop at old stop GSFN and start at new start GSFN */
        final_sub_fn = enchs_log_final_sub_fn;
      }
      else if (action == ENCHS_LOG_STOP)
      {
        /* Stop logging at the old STOP time. ignore RESTART action */
        /* Change the current action to STOP */
        action = ENCHS_LOG_STOP;
        /* Override the stop global subframe number */
        final_sub_fn = enchs_log_final_sub_fn;
      }
      else if (action == ENCHS_LOG_START)
      {
        action = ENCHS_LOG_RESTART;
        final_sub_fn = enchs_log_final_sub_fn;
      }
      break;

    case ENCHS_LOG_STOP:
      if (action == ENCHS_LOG_STOP)
      {
        /* Stop at the old stop CFN, ignore the new stop CFN*/
        final_sub_fn = enchs_log_final_sub_fn;
      }
      else
      {
        /* Previous action is STOP and the new action is start/restart */
        /* Action is changed to reslam */
        action = ENCHS_LOG_RESTART;
        final_sub_fn = enchs_log_final_sub_fn;
      }
      break;

    default:
      WL1_MSG1(ERROR , "Invalid ENCHS log action %d", enchs_log_action);
      enchs_log_action = ENCHS_LOG_NOOP;
    }
  }

  if ((action_oride_happened) &&
      (action == ENCHS_LOG_NOOP))
  {
    WL1_MSG0(HIGH, "Action realigned to NOOP");
  }

  /* When HS UL logging is active then only expected action is STOP
     or RESTART */
  if ((action == ENCHS_LOG_START) && enchs_log_accum_active)
  {
    WL1_MSG0(ERROR, "ENCHS log action START logging already active");
    action = ENCHS_LOG_NOOP;
    log_action_status = FALSE;
  }

  /* When HS UL logging is active then action type START is not
     possible */
  if ((action != ENCHS_LOG_START) && (!enchs_log_accum_active))
  {
    WL1_MSG1(ERROR, "ENCHS log action %d logging not active", action);
    action = ENCHS_LOG_NOOP;
    log_action_status = FALSE;
  }

  /* information validated. Save it in global variable so that it can be
     scanned at every frame to act on it                                 */
  /* ------------------------------------------------------------------- */

  enchs_log_action = action;
  if (action != ENCHS_LOG_NOOP)
  {
    enchs_log_start_sub_fn = start_sub_fn;
    enchs_log_start_cfn = (uint8)start_cfn;
    enchs_log_start_info_table_idx = info_table_index;
    enchs_log_final_sub_fn = final_sub_fn;

    if (action != ENCHS_LOG_START)
    {
      WL1_MSG3(HIGH, "ENCHS log action %d final subFn %d logInfoTblIdx %d",
               action, enchs_log_final_sub_fn, enchs_log_info_table_idx);
    }

    if (action != ENCHS_LOG_STOP)
    {
      MSG_4(MSG_SSID_DFLT, MSG_LEGACY_HIGH, "ENCHS log action %d start subFn %d CFN %d logInfoTblIdx %d",
               action, enchs_log_start_sub_fn, start_cfn, enchs_log_start_info_table_idx);
    }
  }

  /*making a local copy at completion of hs config (ul switch config)*/
  enc_hs_call_info.dpcch_offset= dpcch_offset;
  enc_hs_call_info.num_config_carriers = num_config_carriers ;
  
  HS_LOG_INTFREE ();

  return log_action_status;
}

/*===========================================================================
FUNCTION enchs_dpcch_stop_logging

DESCRIPTION
  This function prepares the HS UL DPCCH logging buffer to be submitted to diag
  It calls the function that posts a UL DPCCH log submit local command and then
  clears the state of HS UL DPCCH logging.

DEPENDENCIES
  enchs_log_info_buf_idx should be valid while making
  a call to this function.

RETURN VALUE
  None

SIDE EFFECTS
  HS UL DPCCH logging states cleared
===========================================================================*/

void enchs_dpcch_stop_logging(void)
{
  /* function local variables */
  /* ------------------------ */

  /*** function code starts here ***/

  /* HSDPA decode status log header pointer */
  hsdpa_log_hdr_struct_type *hs_dpcch_log_pkt_hdr;

  /* Get header to current log buffer and update total size of
     information accumulated */
  hs_dpcch_log_pkt_hdr = (hsdpa_log_hdr_struct_type*)\
                         (&enchs_dpcch_dbg_log_pkt[enchs_log_info_buf_idx]);
  hs_dpcch_log_pkt_hdr->len = sizeof(HS_UL_DPCCH_INFO_DBG_LOG_PKT_type)
                            - ((HS_UL_DPCCH_INFO_LOG_MAX_SAMPLE
                            - enchs_dpcch_dbg_log_pkt[enchs_log_info_buf_idx].num_samples)
                            * HS_UL_DPCCH_DBG_LOG_INFO_SZ_PER_SAMPLE);

  /* send command to submit this decode status log packet */
  enchs_dpcch_send_log_submit_cmd(
    enchs_log_info_buf_idx);

  /* mark HS inactive */
  enchs_log_accum_active = FALSE;
  enchs_dpcch_logging_on = FALSE;

  /* Reset Average CQI and num samples */
  enchs_avg_cqi = ENCHS_AVG_CQI_INVALID;
  enchs_avg_cqi_num_sample = ENCHS_INVALID_NUM_SAMPLE_FOR_AVG_CQI;

  /* if action was reconfig then change it to start. or
     if it was stop then it is al done, mark it NOOP */
  if (enchs_log_action == ENCHS_LOG_RESTART)
  {
    enchs_log_action = ENCHS_LOG_START;
  }
  else
  {
    enchs_log_action = ENCHS_LOG_NOOP;
    enchs_log_info_table_idx = HS_INFO_TABLE_INVALID_VAL;
  }
}

/*===========================================================================
FUNCTION enchs_do_logging

DESCRIPTION
  This function is called periodically every frame to act on new action
  information, if set and do periodic log information accumulation.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

void enchs_do_logging(void)
{
  /* function local variables */
  /* ------------------------ */

  /* CFN for which to UL DPCCH logging needs to be done */
  uint8 this_frame_cfn;
  /* pointer to HS UL DPCCH log packets */
  HS_UL_DPCCH_INFO_DBG_LOG_PKT_type *this_enchs_dpcch_dbg_log_pkt;
  WfwTxHsLogStruct *curr_hslog_ptr;


  /*** function code starts here ***/

  HS_LOG_INTLOCK();

  /* get CFN for the frame at which to accumulate HS UL DPCCH logging
     information. Though it isbased on UL CFN, it is based on DL CFN. At
     instance this logging is done, UL/DL CFN is expected to be same */
  this_frame_cfn = (uint8) normalize((stmr_get_combiner_cfn(TLM_GET_RXTL_SRC()) - 1), MAX_CFN_COUNT);

  /* Handle any pending START action */
  /* ------------------------------- */

  /* Check if HS UL DPCCH logging accumulation is active */
  if (!enchs_log_accum_active)
  {
    /* HS UL DPCCH logging not active here */

    /* Check for HS UL DPCCH log action for start */
    if (enchs_log_action == ENCHS_LOG_START)
    {
      /* If CFN generated above for accumulation matches start CFN */
      if (this_frame_cfn == enchs_log_start_cfn)
      {
        /* Start HS UL DPCCH logging */
        enchs_log_accum_active = TRUE;
        enchs_dpcch_logging_on = FALSE;
        enchs_log_action = ENCHS_LOG_NOOP;

        /* Init CQI estimate stat variables */
        enchs_cqi_estimate_stat_cur_segment = 0;
        enchs_cqi_estimate_stat_cur_segment_num_samples = 0;
        enchs_cqi_estimate_stat_num_seg_accum = 0;
        memset(enchs_cqi_estimate_stat_buffer, 0x00, sizeof(enchs_cqi_estimate_stat_buffer));
        memset(enchs_num_new_tx_rece_cqi_est_seg, 0x00, sizeof(enchs_num_new_tx_rece_cqi_est_seg));
        memset(enchs_num_new_tx_err_cqi_est_seg, 0x00, sizeof(enchs_num_new_tx_err_cqi_est_seg));
      } /* end if, start acummulation  */
    } /* end if, START action pending */

  } /* end if, stat generation not active */

  /* Check for EncHS Stop action if QXDM is turned off */
  if ((!enchs_dpcch_logging_on) && (enchs_log_accum_active) &&
      ((enchs_log_action == ENCHS_LOG_RESTART) ||
       (enchs_log_action == ENCHS_LOG_STOP)))
  {
    /* If some samples are already accumulated, flush them */
    WL1_MSG1(HIGH , "Stopping HS UL DPCCH logging CFN %d", this_frame_cfn);
    /* Time to disable UL DPCCH logging */
    if (enchs_log_info_buf_idx != HS_INFO_TABLE_INVALID_VAL)
    {
      /* If some samples are already accumulated, flush them */
      enchs_dpcch_stop_logging ();
    }
    else
    {
      /* Reset HS UL DPCCH Logging state */
      enchs_logging_init ();
    }
  }

  /* If the logging has been turned OFF, poll to check if it is enabled now */
  if ((!enchs_dpcch_logging_on) && (enchs_log_accum_active))
  {
    if (log_status(HS_UL_DPCCH_INFO_DBG_LOG_PKT))
    {
      enchs_dpcch_logging_on = TRUE;
      enchs_log_info_buf_idx = HS_INFO_TABLE_INVALID_VAL;
    }
  }

  /* Do accumulation and logging */
  /* --------------------------- */

  if ((enchs_log_accum_active) && (enchs_dpcch_logging_on))
  {
    /* loop index to get log info for 5 sub frames in this frame */
    uint8 index_sub_fr;
    /* get previous frame starting sub frame number */
    uint16 global_sub_fr_num;

    /* This is possible only when logging is turned OFF and then turned ON from
       within QXDM. Allocate a new buffer to start log accumulation */
    if (enchs_log_info_buf_idx == HS_INFO_TABLE_INVALID_VAL)
    {
       /* get offset between sub frame number and CFNx5 */
      enchs_log_cfnx5_sub_fn_offset = (int16)normalize(enchs_log_start_sub_fn 
                                                   - (enchs_log_start_cfn * 5),
                                                   HS_MAX_GLOBAL_SUB_FRAME_COUNT);

      /* Allocate a new buffer to accumulate log samples */
      this_enchs_dpcch_dbg_log_pkt = enchs_init_log_buffer();

      /* Init the log buffer idx and reset action to NOOP */
      enchs_log_info_table_idx = enchs_log_start_info_table_idx;

      /*record the starting SFN*/
      this_enchs_dpcch_dbg_log_pkt->starting_global_sub_fn = (uint16)normalize((this_frame_cfn * 5) 
                                                                  + enchs_log_cfnx5_sub_fn_offset,
                                                                  HS_MAX_GLOBAL_SUB_FRAME_COUNT);
      
      WL1_MSG3(HIGH, "HS UL DPCCH log: Starting ENCHS logging %d CFN %d SubFn %d",
                enchs_log_info_table_idx,
                this_frame_cfn,
                this_enchs_dpcch_dbg_log_pkt->starting_global_sub_fn);
    }

    /* Generate global sub frame number from which accumulation will be done
       for 5 sub frames */
    global_sub_fr_num =
      (uint16)normalize((this_frame_cfn * 5) + enchs_log_cfnx5_sub_fn_offset,
                HS_MAX_GLOBAL_SUB_FRAME_COUNT);

    /* Iterate over 5 sub frames to generate stats */
    for (index_sub_fr = 0;
         index_sub_fr < HS_NUM_SUB_FR_PER_RADIO_FR;
         index_sub_fr++)
    {
      /* Info index in log packet where to log this info */
      uint8 log_buf_index;
      /* this sub frame entry index into mDSP current buffer. It is indexed
         using last 4 bits of sub frame number */
      uint8 hs_dpcch_info_idx_mdsp;
      /* UL HS DPCCH debug log info pointer */
        hs_ul_dpcch_debug_info_log_struct_type *debug_log_info_ptr = NULL;
      /* temporary variable to read value in */
      uint16 cqi_num;
      
      /* Flag to indicate if the iteration over sub frames is to be terminated */
      boolean stop_accumulation;
      /* Do not terminate subframe iteration initially */
      stop_accumulation = FALSE;

      /* Get index of this sub frame entry into mDSP. This is the last 4
         bits of sub frame number (modulo 16 operation) */
      hs_dpcch_info_idx_mdsp = global_sub_fr_num & 0xF;

       /* Get address in mDSP corresponding to above index in current table */
      if (enchs_log_info_table_idx == 0)
      {
        curr_hslog_ptr = &(wfw_intf_tx_hs_async_read_ptr->hs.hsLog[0][hs_dpcch_info_idx_mdsp]);
      }
      else
      {
        curr_hslog_ptr = &(wfw_intf_tx_hs_async_read_ptr->hs.hsLog[1][hs_dpcch_info_idx_mdsp]);
      }
      
      /*alias of current log buffer*/
      this_enchs_dpcch_dbg_log_pkt = &(enchs_dpcch_dbg_log_pkt[enchs_log_info_buf_idx]);

      
      /*populated dpcch slot format*/
      this_enchs_dpcch_dbg_log_pkt->hsdpcch_slot_format = curr_hslog_ptr->hsDpcchSlotFormat;

      /* Put a bounds check on the variable "enchs_log_info_buf_idx" to avoid KW errors.
       * keep the usage of the enchs_log_info_buf_idx within this if check. */
      
      if(enchs_log_info_buf_idx < ENCHS_LOG_NUM_BUF)
      {
        log_buf_index      = this_enchs_dpcch_dbg_log_pkt->num_samples;
        debug_log_info_ptr = &(this_enchs_dpcch_dbg_log_pkt->info[log_buf_index]);
      }
      else
      {
        WL1_MSG2(ERROR, "Invalid enchs log_info_buf_idx=%d cfn=%d; continue ",
                        enchs_log_info_buf_idx, seq_get_cfn());
        continue;
      }

#if WCDMA_HSLOG_DEBUG
     WL1_MSG1(HIGH, "HS UL DPCCH log: Inside loop %d", index_sub_fr);
#endif
     debug_log_info_ptr->hs_cqi_report     = curr_hslog_ptr->hsCqiReport;
     debug_log_info_ptr->cqi_type          = curr_hslog_ptr->cqiRcvrType;

     debug_log_info_ptr->beta_ack_slot     = curr_hslog_ptr->betaAckSlot;
     debug_log_info_ptr->beta_cqi_slot     = curr_hslog_ptr->betaCqiSlot;
     
     debug_log_info_ptr->hs_sec_carr_act_bmsk = curr_hslog_ptr->hsSecCarActiveBMask;
     /*ACK NACK for each carrier*/
     /*Primary*/
     debug_log_info_ptr->ack_nack_carr1 = curr_hslog_ptr->ackNackStatus[0];
     
     if (curr_hslog_ptr->hsSecCarActiveBMask)/*if secondary carriers are active*/
     {
      
#if WCDMA_HSLOG_DEBUG
       WL1_MSG2(HIGH, "HS UL DPCCH log: Active Sec Carr bmsk = %d, CQI valid bmsk = %d",
                     curr_hslog_ptr->hsSecCarActiveBMask,
                      curr_hslog_ptr->hsCqiValidBmsk);
#endif
       if(curr_hslog_ptr->hsSecCarActiveBMask & BIT_POS_2_NUM(0)) /*1st Secondary carrier*/
       {
         debug_log_info_ptr->ack_nack_carr2 = curr_hslog_ptr->ackNackStatus[1];
       }
      #ifdef FEATURE_WCDMA_3C_HSDPA
       if(curr_hslog_ptr->hsSecCarActiveBMask & BIT_POS_2_NUM(1)) /*2nd Secondary carrier*/
       {
         debug_log_info_ptr->ack_nack_carr3 = curr_hslog_ptr->ackNackStatus[2];
       }
      #endif
      #ifdef FEATURE_WCDMA_4C_HSDPA
       if(curr_hslog_ptr->hsSecCarActiveBMask & BIT_POS_2_NUM(2)) /*3rd Secondary carrier*/
       {
         debug_log_info_ptr->ack_nack_carr4 = curr_hslog_ptr->ackNackStatus[3];
       }
      #endif
     }
     /*CQI for each carrier*/
     debug_log_info_ptr->hs_cqi_valid_bmsk = curr_hslog_ptr->hsCqiValidBmsk;
    
     if(debug_log_info_ptr->hs_cqi_valid_bmsk & BIT_POS_2_NUM(0)) /*1st carrier*/
     {
       debug_log_info_ptr->cqi_carr1 = curr_hslog_ptr->cqiNum[0];
     }
     if(debug_log_info_ptr->hs_cqi_valid_bmsk & BIT_POS_2_NUM(1)) /*1st Secondary Carrier*/
     {
       debug_log_info_ptr->cqi_carr2 = curr_hslog_ptr->cqiNum[1];
     }
    #ifdef FEATURE_WCDMA_3C_HSDPA
     if(debug_log_info_ptr->hs_cqi_valid_bmsk & BIT_POS_2_NUM(2)) /*2nd Secondary Carrier*/
     {
       debug_log_info_ptr->cqi_carr3 = curr_hslog_ptr->cqiNum[2];
     }
    #endif
    #ifdef FEATURE_WCDMA_4C_HSDPA
     if(debug_log_info_ptr->hs_cqi_valid_bmsk & BIT_POS_2_NUM(3)) /*3rd Secondary Carrier*/
     {
       debug_log_info_ptr->cqi_carr4 = curr_hslog_ptr->cqiNum[3];
     }
    #endif
     cqi_num = curr_hslog_ptr->cqiNum[0]; 

            
      if (cqi_num <= 30)
      {
        enchs_cqi_estimate_stat_buffer[enchs_cqi_estimate_stat_cur_segment][cqi_num]++;
        enchs_cqi_estimate_stat_cur_segment_num_samples++;
        if (enchs_cqi_estimate_stat_cur_segment_num_samples >= ENCHS_CQI_ESTIMATE_STAT_NUM_SAM_PER_SEG)
        {
          uint32 sum_cqi, num_cqi, avg_cqi;
          uint8 index_cqi_val;

          sum_cqi = 0, num_cqi = 0, avg_cqi = 0;
          /* Do CQI Average calculation here */
          for (index_cqi_val = 0; index_cqi_val < ENCHS_NUM_QUANTIZED_CQI_VALS; index_cqi_val++)
          {
            if (enchs_cqi_estimate_stat_buffer[enchs_cqi_estimate_stat_cur_segment][index_cqi_val] != 0)
            {
              sum_cqi += (index_cqi_val * enchs_cqi_estimate_stat_buffer[enchs_cqi_estimate_stat_cur_segment][index_cqi_val]);
              num_cqi += (enchs_cqi_estimate_stat_buffer[enchs_cqi_estimate_stat_cur_segment][index_cqi_val]);
            }
          }

          if (num_cqi != 0)
          {
            avg_cqi = sum_cqi/num_cqi;
            enchs_avg_cqi = avg_cqi;
            enchs_avg_cqi_num_sample = ENCHS_CQI_ESTIMATE_STAT_NUM_SAM_PER_SEG;
          }

          WL1_MSG1(HIGH, "Avg CQI for 2 sec/1000 samples %d", avg_cqi);

          enchs_num_new_tx_rece_cqi_est_seg[enchs_cqi_estimate_stat_cur_segment] = dec_hs_num_new_tx_rece;
          enchs_num_new_tx_err_cqi_est_seg[enchs_cqi_estimate_stat_cur_segment] = dec_hs_new_tx_in_err;
          dec_hs_num_new_tx_rece = 0;
          dec_hs_new_tx_in_err = 0;

          enchs_cqi_estimate_stat_cur_segment = (uint8)
           normalize(enchs_cqi_estimate_stat_cur_segment + 1,
                     ENCHS_CQI_ESTIMATE_STAT_DUMP_SAMPLE_SEG);

          enchs_cqi_estimate_stat_cur_segment_num_samples = 0;

          /* reset stats of cur segment where stats will be populated */
          memset(enchs_cqi_estimate_stat_buffer[enchs_cqi_estimate_stat_cur_segment], 0x00,
                 (sizeof(uint16)* ENCHS_NUM_QUANTIZED_CQI_VALS));
          
          enchs_num_new_tx_rece_cqi_est_seg[enchs_cqi_estimate_stat_cur_segment] = 0;
          enchs_num_new_tx_err_cqi_est_seg[enchs_cqi_estimate_stat_cur_segment] = 0;

          if (enchs_cqi_estimate_stat_num_seg_accum < ENCHS_CQI_ESTIMATE_STAT_DUMP_SAMPLE_SEG)
          {
            enchs_cqi_estimate_stat_num_seg_accum++;
          }

          if (enchs_cqi_estimate_stat_num_seg_accum == ENCHS_CQI_ESTIMATE_STAT_DUMP_SAMPLE_SEG)
          {
            uint8 index_seg;
            uint16 num_samples_accum = 0;
            uint32 num_new_tx_rece = 0;
            uint32 num_new_tx_in_err = 0;

            for (index_seg = 0; index_seg < ENCHS_CQI_ESTIMATE_STAT_DUMP_SAMPLE_SEG; index_seg++)
            {
              num_new_tx_rece += enchs_num_new_tx_rece_cqi_est_seg[index_seg];
              num_new_tx_in_err += enchs_num_new_tx_err_cqi_est_seg[index_seg];
            }

            /* Do CQI median calculation here */
            for (index_cqi_val = 0; index_cqi_val < ENCHS_NUM_QUANTIZED_CQI_VALS; index_cqi_val++)
            {
              for (index_seg = 0; index_seg < ENCHS_CQI_ESTIMATE_STAT_DUMP_SAMPLE_SEG; index_seg++)
              {
                num_samples_accum += enchs_cqi_estimate_stat_buffer[index_seg][index_cqi_val];
              }

              if (num_samples_accum >= (ENCHS_CQI_ESTIMATE_STAT_NUM_TOTAL_SAMPLES / 2))
              {
                if (num_new_tx_rece == 0)
                {
                  WL1_MSG1(HIGH, "Median CQI value is %d BLER NA",
                                 index_cqi_val);
                }
                else
                {
                  uint8 bler_integer;
                  uint8 bler_fract;

                  if (num_new_tx_in_err > num_new_tx_rece)
                  {
                    num_new_tx_in_err = num_new_tx_rece;
                  }

                  bler_integer = (uint8)((num_new_tx_in_err * 100) / num_new_tx_rece);
                  bler_fract = (uint8)
                    ((((num_new_tx_in_err * 100) - (bler_integer * num_new_tx_rece)) * 100) /
                    num_new_tx_rece);

                  WL1_MSG3(HIGH, "Median CQI value is %d BLER %d.%02d",
                                 index_cqi_val, bler_integer, bler_fract);
                }

                break;
              }
            }

            /* reset stats of cur segment where stats will be populated */
            memset(enchs_cqi_estimate_stat_buffer[enchs_cqi_estimate_stat_cur_segment], 0x00,
                   (sizeof(uint16) * ENCHS_NUM_QUANTIZED_CQI_VALS));
            enchs_num_new_tx_rece_cqi_est_seg[enchs_cqi_estimate_stat_cur_segment] = 0;
            enchs_num_new_tx_err_cqi_est_seg[enchs_cqi_estimate_stat_cur_segment] = 0;
          }
        }
      }

      debug_log_info_ptr->tx_agc_ack  = curr_hslog_ptr->txAgcValueAck;
      debug_log_info_ptr->tx_pa_ack   = curr_hslog_ptr->paRangeAck;
      
      debug_log_info_ptr->tx_agc_cqi  = curr_hslog_ptr->txAgcValueCqi;
      debug_log_info_ptr->tx_pa_cqi   = curr_hslog_ptr->paRangeCqi;

      /* Tx Blanking */
      debug_log_info_ptr->tx_blanking = curr_hslog_ptr->txBlankBmsk;


      /* Increment number of samples accumulated */
      this_enchs_dpcch_dbg_log_pkt->num_samples++;

      /* Handle RESTART and STOP actions */
      /* ------------------------------- */

      /* check if there action type RECONFIG and STOP set */
      if ((enchs_log_action == ENCHS_LOG_RESTART) ||
          (enchs_log_action == ENCHS_LOG_STOP))
      {
        /*taking care of the logging offset in cal CFN */
        int16 curr_cfn = global_sub_fr_num / HS_NUM_SUB_FR_PER_RADIO_FR ;
        int16 stop_cfn = enchs_log_final_sub_fn / HS_NUM_SUB_FR_PER_RADIO_FR;
        int16 dist_curr_stop_cfn;
        int16 dist_curr_stop_gsfn;

        /*number of extra logged Sub frames*/  
        dist_curr_stop_gsfn = normalize(global_sub_fr_num - enchs_log_final_sub_fn,
                                         HS_MAX_GLOBAL_SUB_FRAME_COUNT);

        /*For cases when UL switch config (FW ISR) comes with STOP GSFN 
                after the that GSFN is already logged. 
                Here CFN is used for stopping criteria to accomodate error of 9 GSFN*/
         /* not normalized to have negative values and void stopping early*/
        dist_curr_stop_cfn  = curr_cfn - stop_cfn; 
        
        /*stop_cfn = 255 and curr_cfn = 0, only case when this is possible */
        if(dist_curr_stop_cfn == -255)
        {
          dist_curr_stop_cfn = 1;
        }
        /*putting greater than equal to here, 
                risks stopping early when Stop CFN is after roll-over*/
        if(1 == dist_curr_stop_cfn)
        { 
          WL1_MSG2(HIGH, "enchs_logging missed stop SFN, Curr gsfn = %d, Stop gsfn = %d ",
                         global_sub_fr_num, enchs_log_final_sub_fn);
          /*remove the extra logged subframes and flush the valid ones only*/
          this_enchs_dpcch_dbg_log_pkt->num_samples -= dist_curr_stop_gsfn;
        }
        /* Check if this is sub frame to STOP as the part of action STOP
           or RESTART */
        if ((global_sub_fr_num == enchs_log_final_sub_fn) ||
             /*case when curr cfn has gone beyond stop cfn + 1*/
             (dist_curr_stop_cfn == 1)||
            ((MODULE_CLEANUP_PENDING(L1_HS_CFG_MODULE)) &&
             (enchs_log_action == ENCHS_LOG_STOP)))

        {
          WL1_MSG3(HIGH, "HS UL DPCCH log: Stopping HS UL logging %d CFN %d SubFn %d",
                   enchs_log_info_table_idx,
                   this_frame_cfn,
                   global_sub_fr_num);

          /* STOP this accumulation now */
          enchs_dpcch_stop_logging ();

          /* no need to further get sub frames, set the termination flag here */
          /* After performing WCDMA_INTFREE, break out of this loop */
          stop_accumulation = TRUE;
        } /* End if, sub frame to stop accumulation has occured */
      } /* end if, action RESTART or STOP pending */

      /* Check if we were supposed to break out of this loop */
      if (stop_accumulation)
      {
        break;
      }

      /* Go to next sub frame */
      global_sub_fr_num = (uint16)
        normalize(global_sub_fr_num + 1, HS_MAX_GLOBAL_SUB_FRAME_COUNT);

      /* If acumulation has reached max samples then this must be submitted */
      /* ------------------------------------------------------------------ */
      if (this_enchs_dpcch_dbg_log_pkt->num_samples == HS_UL_DPCCH_INFO_LOG_MAX_SAMPLE)
      {
        /* HSDPA decode status log header pointer */
        hsdpa_log_hdr_struct_type *hs_dpcch_log_pkt_hdr;

        #if WCDMA_HSLOG_DEBUG
        WL1_MSG3(HIGH, "HS UL DPCCH log: Log max buff size idx = %d, GSFN = %d, Size = %d",
                  enchs_log_info_table_idx,
                  global_sub_fr_num,
                  sizeof(HS_UL_DPCCH_INFO_DBG_LOG_PKT_type));
        #endif

        /* Get header to current log buffer and update total size of
           information accumulated */
        hs_dpcch_log_pkt_hdr = (hsdpa_log_hdr_struct_type*)(this_enchs_dpcch_dbg_log_pkt);

        hs_dpcch_log_pkt_hdr->len  = sizeof(HS_UL_DPCCH_INFO_DBG_LOG_PKT_type)
                                   -((HS_UL_DPCCH_INFO_LOG_MAX_SAMPLE 
                                   - this_enchs_dpcch_dbg_log_pkt->num_samples)
                                   * HS_UL_DPCCH_DBG_LOG_INFO_SZ_PER_SAMPLE);

        /* send command to submit this decode status log packet */
        enchs_dpcch_send_log_submit_cmd(enchs_log_info_buf_idx);

        /* Get available HS status accumulation buffer */
        this_enchs_dpcch_dbg_log_pkt = enchs_init_log_buffer();
        this_enchs_dpcch_dbg_log_pkt->starting_global_sub_fn = global_sub_fr_num;
        
      } /* end if, max decode status samples have been collected */
    }
  }

  HS_LOG_INTFREE();
}

/*===========================================================================
FUNCTION enchs_init_log_buffer

DESCRIPTION
  This function is called at the start of HS channel bringup or at the start
  of logging. this functions allocates a buffer to accumulate the log samples
  and populates the log header.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  One of the log buffer is marked unavailable.
===========================================================================*/

HS_UL_DPCCH_INFO_DBG_LOG_PKT_type* enchs_init_log_buffer(void)
{
  /* Prepare for HS UL DPCCH logging */

  uint8 index_buf; /* loop indicies */
  HS_UL_DPCCH_INFO_DBG_LOG_PKT_type *this_enchs_dpcch_dbg_log_pkt;
  /* DPCH to HS-DPCCH offset */
  uint8 dpcch_offset;

  /* Get available HS UL DPCCH accumulation buffer */
  enchs_log_info_buf_idx = HS_INFO_TABLE_INVALID_VAL;
  for (index_buf = 0;
       index_buf < ENCHS_LOG_NUM_BUF;
       index_buf++)
  {
    if (enchs_dpcch_log_buf_avail_status[index_buf])
    {
      enchs_dpcch_log_buf_avail_status[index_buf] = FALSE;
      enchs_log_info_buf_idx = index_buf;
      break;
    }
  }
  /* If can't get buffer then there is no use of proceeding. It should
     never happen because of double buffering */
  if (enchs_log_info_buf_idx == HS_INFO_TABLE_INVALID_VAL)
  {
    ERR_FATAL("Can't get HS UL DPCCH log buf", 0, 0, 0);
    return NULL; /*lint !e527 */
  }

  /* get pointer to current HS UL DPCCH debug log buffer */
  this_enchs_dpcch_dbg_log_pkt = &(enchs_dpcch_dbg_log_pkt[enchs_log_info_buf_idx]);
  
  /* Do time stamp corresponding to first sample */
  log_set_timestamp(this_enchs_dpcch_dbg_log_pkt);
  
  /* Version number */
  this_enchs_dpcch_dbg_log_pkt->version_num = 3;
  /* Get Multi-Sim Configuration(SS/DSDS/DSDA/TSTS) info from TRM */
  #if defined(FEATURE_DUAL_SIM) && defined(FEATURE_QTA)
  this_enchs_dpcch_dbg_log_pkt->multi_sim_conf = wl1_ds_query_trm_multisim_config_info();
  #else
  this_enchs_dpcch_dbg_log_pkt->multi_sim_conf = 0;
  #endif

  /* init number of samples accumulated to 0 */
  this_enchs_dpcch_dbg_log_pkt->num_samples = 0;

  /* Prepare HS-DPCCH offset to be filled in log header */
  dpcch_offset = enc_hs_call_info.dpcch_offset;
  /* Keep offset within 30 bpg */
  while (dpcch_offset > HS_SUB_FRAME_LEN_UNIT_256_CHIP)
  {
    dpcch_offset -= HS_SUB_FRAME_LEN_UNIT_256_CHIP;
  }
 
  this_enchs_dpcch_dbg_log_pkt->dpcch_offset = dpcch_offset;
  /*0: 1 carrier, 1: 2 carriers ...*/
  this_enchs_dpcch_dbg_log_pkt->config_carr  = enc_hs_call_info.num_config_carriers - 1;
 
  return this_enchs_dpcch_dbg_log_pkt;
}

/*===========================================================================
FUNCTION enchs_dpcch_flush_log_pkt

DESCRIPTION
  This function flushes the DPCCH log packets if the UL timeline maintenance
  events are disabled but the HSDPA logging is still active. This can happen
  in the DCH to DCH transition. If even before the CFN is reached at which
  logging has to be stopped, the UL event timeline is suspended because of HHO
  measurements or any such conditions, this function will gracefully flush
  the packets and suspend logging.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

void enchs_dpcch_flush_log_pkt(void)
{
  /* function local variables */
  /* ------------------------ */

  /* HSDPA decode status log header pointer */
  hsdpa_log_hdr_struct_type *hs_dpcch_log_pkt_hdr;

  /*** function code starts here ***/

  HS_LOG_INTLOCK();

  if ((enchs_log_info_buf_idx != HS_INFO_TABLE_INVALID_VAL) &&
      (enchs_log_accum_active == TRUE))
  {
    #if WCDMA_HSLOG_DEBUG
    WL1_MSG1(HIGH, "Flushing DPCCH log packet, Num samples = %d",
                   enchs_dpcch_dbg_log_pkt[enchs_log_info_buf_idx].num_samples);
    #endif
    if (enchs_dpcch_dbg_log_pkt[enchs_log_info_buf_idx].num_samples > 0)
    {
      /* Get header to current log buffer and update total size of
         information accumulated */
      hs_dpcch_log_pkt_hdr =
        (hsdpa_log_hdr_struct_type*) (&enchs_dpcch_dbg_log_pkt[enchs_log_info_buf_idx]);
      hs_dpcch_log_pkt_hdr->len = sizeof(HS_UL_DPCCH_INFO_DBG_LOG_PKT_type) -
        ((HS_UL_DPCCH_INFO_LOG_MAX_SAMPLE -
          enchs_dpcch_dbg_log_pkt[enchs_log_info_buf_idx].num_samples) *
        HS_UL_DPCCH_DBG_LOG_INFO_SZ_PER_SAMPLE);

      /* send command to submit this decode status log packet */
      enchs_dpcch_send_log_submit_cmd(
      enchs_log_info_buf_idx);
    }
    else
    {
      /* Mark this decode status log buffer to available */
      enchs_dpcch_log_buf_avail_status[enchs_log_info_buf_idx] = TRUE;
    }

    /* mark HS inactive */
    enchs_log_accum_active = FALSE;
    enchs_dpcch_logging_on = FALSE;
    enchs_log_action = ENCHS_LOG_NOOP;
    enchs_log_info_table_idx = HS_INFO_TABLE_INVALID_VAL;
    enchs_log_info_buf_idx = HS_INFO_TABLE_INVALID_VAL;
  }

  HS_LOG_INTFREE();
}

/*===========================================================================
FUNCTION enchs_dpcch_send_log_submit_cmd

DESCRIPTION
  This function is called to post HS log submit command. There is only one HS
  log submit command. HS cfg module calls all accumlated logging entities. Each
  entity is reponsible for maintaining log submit pending information. It keeps
  this information for HS UL DPCCH log submit pending information

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

void enchs_dpcch_send_log_submit_cmd(
  /* HS decode status log buffer index to submit */
  uint8 ul_hs_info_buf_idx)
{
  /* function local variables */
  /* ------------------------ */

  /* log packet pointer that need to be submitted */
  HS_UL_DPCCH_INFO_DBG_LOG_PKT_type *hs_dpcch_dbg_log_pkt;

  /* log header pointer */
  hsdpa_log_hdr_struct_type *hs_dpcch_log_pkt_hdr;

  /*** function code starts here ***/

  /* Check if there is a pending request to submit previous log packet */
  if (enchs_dpcch_log_submit)
  {
    /* Print error message and mark this buffer as available one */

      /* get log packet pointer */
      hs_dpcch_dbg_log_pkt = &(enchs_dpcch_dbg_log_pkt[ul_hs_info_buf_idx]);
      /* Get log packet header pointer and set log code to that */
      hs_dpcch_log_pkt_hdr = (hsdpa_log_hdr_struct_type*) hs_dpcch_dbg_log_pkt;
      /* submit the log packet */
      WL1_MSG3(ERROR, "HS_UL_INFO_LOG:Failed idx %d info sz %d idx pending",
                      ul_hs_info_buf_idx,
                      hs_dpcch_log_pkt_hdr->len,
                      enchs_dpcch_log_buf_idx_to_submit);

    enchs_dpcch_log_buf_avail_status[ul_hs_info_buf_idx] = TRUE;
  }
  else
  {
    /* Save HS decode status log buffer index to submit later in task context */
    enchs_dpcch_log_submit = TRUE;
    enchs_dpcch_log_buf_idx_to_submit = ul_hs_info_buf_idx;
    /* Send local command to l1m */
    hs_send_submit_log_cmd();
  }
}

/*===========================================================================
FUNCTION enchs_submit_log_buffer

DESCRIPTION
  This function is called from hscfg module in response of HS log submit
  command. It checks for HS UL DPCCH log submit flag. It is set to TRUE,
  it submit log buffer pointed by log buffer index to submit.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

void enchs_submit_log_buffer(void)
{
  /* function local variables */
  /* ------------------------ */

  /* flag to hold the state of the log code in QXDM */
  boolean log_code_disabled = FALSE;

  /* log packet pointer that need to be submitted */
  HS_UL_DPCCH_INFO_DBG_LOG_PKT_type *hs_dpcch_dbg_log_pkt;

  /* log header pointer */
  hsdpa_log_hdr_struct_type *hs_dpcch_log_pkt_hdr;
  /* buffer index to submit */
  uint8 log_info_buf_idx;

  /*** function code starts here ***/

  /* If log need to be submitted then proceed otherwise simply return from here */
  if (enchs_dpcch_log_submit)
  {
    /* save log buffer index to submit */
    log_info_buf_idx = enchs_dpcch_log_buf_idx_to_submit;
  }
  else
  {
    return;
  }

  if (log_status(HS_UL_DPCCH_INFO_DBG_LOG_PKT))
  {
    /* get log packet pointer */
    hs_dpcch_dbg_log_pkt = &(enchs_dpcch_dbg_log_pkt[log_info_buf_idx]);
    /* Get log packet header pointer and set log code to that */
    hs_dpcch_log_pkt_hdr = (hsdpa_log_hdr_struct_type*) hs_dpcch_dbg_log_pkt;
    hs_dpcch_log_pkt_hdr->code = HS_UL_DPCCH_INFO_DBG_LOG_PKT;
    /* submit the log packet */
    if (!log_submit((PACKED void *) hs_dpcch_dbg_log_pkt))
    {
      WL1_MSG1(ERROR, "HS_UL_INFO_LOG:Failed info sz %d",
                      hs_dpcch_log_pkt_hdr->len);
    }
  }
  else
  {
    /* Mark the flag to indicate that the log code is disabled */
    log_code_disabled = TRUE;
  }

  HS_LOG_INTLOCK();
  /* Mark this decode status log buffer to available */
  enchs_dpcch_log_buf_avail_status[log_info_buf_idx] = TRUE;
  /* reset log to submit flag */
  enchs_dpcch_log_submit = FALSE;

  /* If the log code is disabled, clear all buffers and mark logging state
     as well as logging buffer index as invalid */
  if (log_code_disabled)
  {
    uint8 index_buf;

    /* Mark logging OFF by QXDM */
    enchs_dpcch_logging_on = FALSE;
    /* Mark the log buffer index as invalid */
    enchs_log_info_buf_idx = HS_INFO_TABLE_INVALID_VAL;
    /* iterate over all log buffer status to mark them available */
    for (index_buf = 0; index_buf < ENCHS_LOG_NUM_BUF; index_buf++)
    {
      enchs_dpcch_log_buf_avail_status[index_buf] = TRUE;
    }
  }

  HS_LOG_INTFREE();
}
/*===========================================================================
FUNCTION enchs_get_avg_cqi_info

DESCRIPTION
  This function returns avg CQI information

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
uint32 enchs_get_avg_cqi_info(void)
{
  return enchs_avg_cqi;
}

/*===========================================================================
FUNCTION enchs_get_avg_cqi_info_for_ui_display

DESCRIPTION
  This function populates avg CQI and total Num Sample information 
  for UI display

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
void enchs_get_avg_cqi_info_for_ui_display(
  /* structure pointer to get the avg CQI and Num sample */
  enchs_avg_cqi_info_struct_type *avg_cqi_info)
{
  /* function local variables */
  /* ------------------------ */

  /*** function code starts here ***/
  if(avg_cqi_info)
  {
    avg_cqi_info->enchs_avg_cqi_num_sample = enchs_avg_cqi_num_sample;
    avg_cqi_info->enchs_avg_cqi_val = enchs_avg_cqi;
  }
  else
  {
    WL1_MSG1(ERROR, "Invalid input param. avg_cqi_info:%0x", avg_cqi_info);
    return;
  }
}



/*===========================================================================
FUNCTION enchs_set_cqi_average_window_seconds

DESCRIPTION
  This function sets total Num Sample information 
  
DEPENDENCIES
 
RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
void enchs_set_cqi_average_window_seconds(
  /* Scaling factor for avg CQI calculation */
  uint8 cqi_averaging_window)
{
  /* function local variables */
  /* ------------------------ */

  /*** function code starts here ***/
  WL1_MSG2(HIGH,"cqi_averaging_window:%d seconds, MAX:%d seconds",
           cqi_averaging_window, 5);
  if ((cqi_averaging_window >=2)||(cqi_averaging_window <= 5))
  {
    
    cqi_average_window_period_seconds = cqi_averaging_window;
  }

}

#ifdef FEATURE_WCDMA_MIMO
/*===========================================================================
FUNCTION dec_hs_mimo_demod_log_init
 
DESCRIPTION This function initializes the log states for MIMO demod log packet
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
void dec_hs_mimo_demod_log_init(void)
{
  /* function local variables */
  /* ------------------------ */

  /* loop index */
  uint8 index_buf;
  
  /*** function code starts here ***/

  /* MIMO demod accumulation logging is inactive */
  hs_mimo_demod_log_accum_active = FALSE;
  hs_mimo_demod_logging_enabled_qxdm = FALSE;
  
  /* MIMO demod accumulation logging action is NO_OP */
  hs_mimo_demod_log_action = HS_STAT_GEN_NOOP;

  /* iterate over all log buffer status to mark them available */
  for (index_buf = 0; index_buf < HS_MIMO_LOG_NUM_BUF; index_buf++)
  {
    hs_mimo_demod_log_buf_avail_status[index_buf] = TRUE;
  }

  /* Set log buffer index to INVALID */
  hs_mimo_demod_log_info_buf_idx = HS_INFO_TABLE_INVALID_VAL;

  /* Init log submit request to FALSE */
  hs_mimo_demod_log_submit = FALSE;
  hs_mimo_demod_log_buf_idx_to_submit = HS_INFO_TABLE_INVALID_VAL;
}

/*===========================================================================
FUNCTION dec_hs_mimo_demod_log_update

DESCRIPTION
  This function initialize mimo demod information.
  
  It is called when ever MIMO demod log geneartion is started at START or RESTART
  action OR
  when ever samples generated using max required number of samples
  
DEPENDENCIES
  None

RETURN VALUE
  5 Bit unquantized value form 1 to 30

SIDE EFFECTS
  None
===========================================================================*/
void dec_hs_mimo_demod_log_update(void)
{

  /* function local variables */
  /* ------------------------ */
  
  /* CFN for which MIMO demod logging needs to be done */
  uint8 this_frame_cfn;
  /* S-CPICH Power imbalance value */
  uint8 s_cpich_po;

  /* pointer to amp_n_eng_est log packets */
  HS_MIMO_DEMOD_LOG_PKT_type *this_hs_mimo_demod_log_pkt;

  /*** function code starts here ***/

  /* Do logging only if MIMO is active */
  if (!hs_get_mimo_state(L1_HSDPA_CARRIER_0))
  {
    return;
  }

  WL1_MSG2(HIGH, "MIMO Demod Rd ptr %d MIMO Demod Wr ptr %d",
            mcalwcdma_fwsw_intf_addr_ptr->dbackHsMimoDemodRdPtr,
                 mcalwcdma_fwsw_intf_addr_ptr->dbackHsMimoDemodLogAsyncReadBuf.wrPtr);

  /* Query S-CPICH Power imbalance value */
  s_cpich_po = hs_get_s_cpich_po(L1_HSDPA_CARRIER_0);

  {
    WL1_MSG3(ERROR, "hs_mimo_demod_log_accum_active %d, hs_mimo_demod_log_action %d hs_mimo_demod_log_start_cfn %d",
                    hs_mimo_demod_log_accum_active,
                    hs_mimo_demod_log_action,
                    hs_mimo_demod_log_start_cfn);
  }

  HS_LOG_INTLOCK();

  /* get CFN for the frame at which to accumulate MIMO logging information */
  this_frame_cfn = (uint8) normalize((stmr_get_combiner_cfn(TLM_GET_RXTL_SRC()) - 2), MAX_CFN_COUNT);

  /* Handle any pending START action */
  /* ------------------------------- */

  /* Check if mimo channel analysis logging accumulation is active */
  if (!hs_mimo_demod_log_accum_active)
  {
    /* MIMO demod logging not active here */

    /* Check for LLR log action for start */
    if ((hs_mimo_demod_log_action == HS_STAT_GEN_START) ||
        (hs_mimo_demod_log_action == HS_STAT_GEN_RESTART))
    {
      /* If CFN generated above for accumulation matches start CFN */
      if (this_frame_cfn == hs_mimo_demod_log_start_cfn)
      {
        /* Start mimo logging */
        hs_mimo_demod_cfnx5_sub_fn_offset = (int16)
          normalize(hs_mimo_demod_log_start_sub_fn - (hs_mimo_demod_log_start_cfn * 5),
                    HS_MAX_GLOBAL_SUB_FRAME_COUNT);

        hs_mimo_demod_log_info_buf_idx = HS_INFO_TABLE_INVALID_VAL;              
        hs_mimo_demod_log_accum_active = TRUE;
        hs_mimo_demod_logging_enabled_qxdm = FALSE;
        {
          WL1_MSG3(ERROR, "hs_mimo_demod_log_accum_active %d, hs_mimo_demod_log_action %d hs_mimo_demod_log_start_cfn %d",
                          hs_mimo_demod_log_accum_active,
                          hs_mimo_demod_log_action,
                          hs_mimo_demod_log_start_cfn);
        }

        /* Reset action to NOOP */
        hs_mimo_demod_log_action = HS_STAT_GEN_NOOP;
      } /* end if, start acummulation  */
    } /* end if, START action pending */
  } /* end if, stat generation not active */


  /* Check for HS Stat Stop action if QXDM is turned off */
  if ((!hs_mimo_demod_logging_enabled_qxdm) && (hs_mimo_demod_log_accum_active) &&
      ((hs_mimo_demod_log_action == HS_STAT_GEN_RESTART) ||
       (hs_mimo_demod_log_action == HS_STAT_GEN_STOP)))
  {
    /* Time to disable mimo logging */
    if (hs_mimo_demod_log_info_buf_idx != HS_INFO_TABLE_INVALID_VAL)
    {
      /* If some samples are already accumulated, flush them */
      WL1_MSG1(HIGH , "Stopping HS mimo logging CFN %d", this_frame_cfn);
      dec_hs_mimo_demod_stop_logging ();
    }
    else
    {
      /* Reset HS mimo Logging state */
      dec_hs_mimo_demod_log_init ();
      WL1_MSG0(HIGH , "Stopped HS mimo logging");
    }
  }

  /* If the logging has been turned OFF, poll to check if it is enabled now */
  if ((!hs_mimo_demod_logging_enabled_qxdm) && (hs_mimo_demod_log_accum_active))
  {
    if (log_status(HS_MIMO_DEMOD_LOG_PKT))
    {
      hs_mimo_demod_logging_enabled_qxdm = TRUE;
      hs_mimo_demod_log_info_buf_idx = HS_INFO_TABLE_INVALID_VAL;      
    }
  }
  
  /* If the logging has been turned OFF, then just set RdPtr
     equal to WrPtr and skip reading the log buffer contents */
  if (!hs_mimo_demod_logging_enabled_qxdm)
  {
    mcalwcdma_fwsw_intf_addr_ptr->dbackHsMimoDemodRdPtr =
      mcalwcdma_fwsw_intf_addr_ptr->dbackHsMimoDemodLogAsyncReadBuf.wrPtr;
  }

  {
    WL1_MSG3(ERROR, "hs_mimo_demod_log_accum_active %d, hs_mimo_demod_logging_enabled_qxdm %d hs_mimo_demod_log_start_cfn %d",
                    hs_mimo_demod_log_accum_active,
                    hs_mimo_demod_logging_enabled_qxdm,
                    hs_mimo_demod_log_start_cfn);
  }

  /* Do accumulation and logging */
  /* --------------------------- */

  if ((hs_mimo_demod_log_accum_active) && (hs_mimo_demod_logging_enabled_qxdm))
  {
    /* get previous frame starting sub frame number */
    uint16 global_sub_fr_num;
    uint32 hs_mimo_demod_log_rd_ptr, hs_mimo_demod_log_wr_ptr;

    /* This is possible only when logging is turned OFF and then turned ON from
       within QXDM. Allocate a new buffer to start log accumulation */
    if (hs_mimo_demod_log_info_buf_idx == HS_INFO_TABLE_INVALID_VAL)
    {
      /* Allocate a new buffer to accumulate log samples */
      
      uint8 index_buf; /* loop indicies */
    
      /* Get available HS amp and energy est acumulation buffer */
      for (index_buf = 0;
           index_buf < HS_MIMO_LOG_NUM_BUF;
           index_buf++)
      {
        if (hs_mimo_demod_log_buf_avail_status[index_buf])
        {
          hs_mimo_demod_log_buf_avail_status[index_buf] = FALSE;
          hs_mimo_demod_log_info_buf_idx = index_buf;
          break;
        }
      }
      /* If can't get buffer then there is no use of proceeding. It should
         never happen because of double buffering */
      if (hs_mimo_demod_log_info_buf_idx == HS_INFO_TABLE_INVALID_VAL)
      {
        WL1_MSG0(ERROR, "Can't get HS mimo demod log buf");

        /* iterate over all log buffer status to mark them available */
        for (index_buf = 0; index_buf < HS_MIMO_LOG_NUM_BUF; index_buf++)
        {
          hs_mimo_demod_log_buf_avail_status[index_buf] = TRUE;
        }

        /* Set log buffer index to 0 */
        hs_mimo_demod_log_info_buf_idx = 0;
        hs_mimo_demod_log_buf_avail_status[hs_mimo_demod_log_info_buf_idx] = FALSE;

        /* Init log submit request to FALSE */
        hs_mimo_demod_log_submit = FALSE;
        hs_mimo_demod_log_buf_idx_to_submit = HS_INFO_TABLE_INVALID_VAL;
      }
    
      /* get pointer to current HS amp and energy est debug log buffer */
      this_hs_mimo_demod_log_pkt =
        &(hs_mimo_demod_log_pkt_buf[hs_mimo_demod_log_info_buf_idx]);

      /* Do time stamp corresponding to first sample */
      log_set_timestamp(this_hs_mimo_demod_log_pkt);
      /* init number of samples accumulated to 0 */
      this_hs_mimo_demod_log_pkt->num_samples = 0;
      /* set the verison info*/  
      this_hs_mimo_demod_log_pkt->version = 0x02;
      /* Save starting sub frame number */
      /* If the DPCH to SCCH offset is >= 30, it means that the starting global subframe number
         is advanced by 1 to keep the separation between end of HS-PDSCH to start of HS-SCCH > 40bpgs */
      this_hs_mimo_demod_log_pkt->starting_sub_fn_and_sp =
        (uint16)normalize((this_frame_cfn * 5) + hs_mimo_demod_cfnx5_sub_fn_offset, 
                           HS_MAX_GLOBAL_SUB_FRAME_COUNT);      

      /* set the Scpich-Pcpich power imbalance */
      this_hs_mimo_demod_log_pkt->Scpich_Pcpich_Power_Imbalance = s_cpich_po;

      /* counter used for log buffer optimization.incremented as per the usage */
      hs_mimo_demod_sample_buf_offset = 0 ;
      /* Init the log buffer idx and reset action to NOOP */
      hs_mimo_demod_log_action = HS_STAT_GEN_NOOP;
    
      WL1_MSG3(HIGH, "Starting HS mimo logging %d CFN %d SubFn %d",
               hs_mimo_demod_log_info_buf_idx,
               this_frame_cfn,
               this_hs_mimo_demod_log_pkt->starting_sub_fn_and_sp);
    }
    
    /* Generate global sub frame number from which accumulation will be done
       for 5 sub frames */
    global_sub_fr_num =
      (uint16)normalize((this_frame_cfn * 5) + hs_mimo_demod_cfnx5_sub_fn_offset,
                HS_MAX_GLOBAL_SUB_FRAME_COUNT);


    hs_mimo_demod_log_rd_ptr = mcalwcdma_fwsw_intf_addr_ptr->dbackHsMimoDemodRdPtr;
    hs_mimo_demod_log_wr_ptr = mcalwcdma_fwsw_intf_addr_ptr->dbackHsMimoDemodLogAsyncReadBuf.wrPtr;

    if (hs_mimo_demod_log_rd_ptr == hs_mimo_demod_log_wr_ptr)
    {
      /* check if there action type RECONFIG and STOP set */
      if ((hs_mimo_demod_log_action == HS_STAT_GEN_RESTART) ||
          (hs_mimo_demod_log_action == HS_STAT_GEN_STOP))
      {
        /* STOP this accumulation now */
        /* Flush already accumulated samples */
        dec_hs_mimo_demod_stop_logging ();
          
        WL1_MSG3(HIGH, "Stoping HS mimo logging %d CFN %d SubFn %d",
                 hs_mimo_demod_log_info_buf_idx,
                 this_frame_cfn,
                 global_sub_fr_num);
      } /* end if, action RESTART or STOP pending */
    }

    while(hs_mimo_demod_log_rd_ptr != hs_mimo_demod_log_wr_ptr)
    {
      uint8 index_buf; /* loop index */
      /* mDSP address from where to read the data */
      WfwDbackHsMimoDemodLogStruct* hs_mimo_mdsp_log_ptr;
      /* this sub frame entry index into mDSP current buffer. It is indexed
      using last 4 bits of sub frame number */
      uint8 hs_decode_info_idx_mdsp;
      uint32 temp_val;
      hs_mimo_demod_struct_type* hs_mimo_sample_ptr;

      global_sub_fr_num =
        mcalwcdma_fwsw_intf_addr_ptr->dbackHsMimoDemodLogAsyncReadBuf.dbackHsMimoDemodLog[hs_mimo_demod_log_rd_ptr].hsSubFrmNum;

      /* Get index of this sub frame entry into mDSP. This is the last 4
         bits of sub frame number (modulo 16 operation) */
      hs_decode_info_idx_mdsp = global_sub_fr_num & 0xF;

      /* get pointer to current HS amp and energy est debug log buffer */
      this_hs_mimo_demod_log_pkt =
        &(hs_mimo_demod_log_pkt_buf[hs_mimo_demod_log_info_buf_idx]);
      /* get the mdsp sample location*/

      if (hs_mimo_demod_log_rd_ptr >= WFW_DEMOD_HS_MIMO_DEMOD_LOG_PACKET_BUF_SIZE)
      {
        WL1_MSG2(ERROR, "hs_mimo_demod_log_rd_ptr %d exceeds bounds %d",
                        hs_mimo_demod_log_rd_ptr,
                        WFW_DEMOD_HS_MIMO_DEMOD_LOG_PACKET_BUF_SIZE);
        continue;
      }
      hs_mimo_mdsp_log_ptr = 
        &(mcalwcdma_fwsw_intf_addr_ptr->dbackHsMimoDemodLogAsyncReadBuf.dbackHsMimoDemodLog[hs_mimo_demod_log_rd_ptr]);

      if (hs_mimo_demod_sample_buf_offset > (HS_MIMO_DEMOD_LOG_MAX_BUF_SIZE-sizeof(hs_mimo_demod_struct_type)))
      {
        WL1_MSG2(ERROR, "hs_mimo_demod_sample_buf_offset %d exceeded bounds %d",
                  hs_mimo_demod_sample_buf_offset, 
                        (HS_MIMO_DEMOD_LOG_MAX_BUF_SIZE-sizeof(hs_mimo_demod_struct_type)));
        hs_mimo_demod_sample_buf_offset = 0;
        this_hs_mimo_demod_log_pkt->num_samples = 0;
        continue;
      }

      /* get the log buffer current location*/
      hs_mimo_sample_ptr =
         (hs_mimo_demod_struct_type*)&(this_hs_mimo_demod_log_pkt->info[hs_mimo_demod_sample_buf_offset]);

      /* initialize the mimo control info*/
      HS_LOG_MIMO_DEMOD_SET_VAL(hs_mimo_sample_ptr->mimo_control_info,
                       CTRL_PDSCH, hs_mimo_mdsp_log_ptr->pdschExists);
      HS_LOG_MIMO_DEMOD_SET_VAL(hs_mimo_sample_ptr->mimo_control_info,
                     CTRL_SIC_DONE_2ND_DECODE, hs_mimo_mdsp_log_ptr->sicDone2ndDecode);
      HS_LOG_MIMO_DEMOD_SET_VAL(hs_mimo_sample_ptr->mimo_control_info,
                     CTRL_SIC_DONE_3RD_DECODE, hs_mimo_mdsp_log_ptr->sicDone3rdDecode);
      HS_LOG_MIMO_DEMOD_SET_VAL(hs_mimo_sample_ptr->mimo_control_info,
                     CTRL_ID, hs_mimo_mdsp_log_ptr->firstDecodedStream);
      HS_LOG_MIMO_DEMOD_SET_VAL(hs_mimo_sample_ptr->mimo_control_info,
                     GENIE_CORRELATION, hs_mimo_mdsp_log_ptr->genieCorrelaton);
      /* increment the counter by 1 */
      hs_mimo_demod_sample_buf_offset++;

      /* proceed if pdsch exists*/ 
      if (hs_mimo_mdsp_log_ptr->pdschExists == 1)
      {
        /*log the channel Matrix Estimation*/ 
        hs_mimo_sample_ptr->H00_real = hs_mimo_mdsp_log_ptr->H00;
        temp_val = hs_mimo_mdsp_log_ptr->H00;
        hs_mimo_sample_ptr->H00_imag = temp_val >> 16;
  
        hs_mimo_sample_ptr->H01_real = hs_mimo_mdsp_log_ptr->H01;
        temp_val = hs_mimo_mdsp_log_ptr->H01;
        hs_mimo_sample_ptr->H01_imag = temp_val >> 16;
  
        hs_mimo_sample_ptr->H10_real = hs_mimo_mdsp_log_ptr->H10;
        temp_val = hs_mimo_mdsp_log_ptr->H10;
        hs_mimo_sample_ptr->H10_imag = temp_val >> 16;
  
        hs_mimo_sample_ptr->H11_real = hs_mimo_mdsp_log_ptr->H11;
        temp_val = hs_mimo_mdsp_log_ptr->H11;
        hs_mimo_sample_ptr->H11_imag = temp_val >> 16;

        /* increment the counter by 16 */
        hs_mimo_demod_sample_buf_offset = hs_mimo_demod_sample_buf_offset + 16;

        /*log the covariant info*/
        hs_mimo_sample_ptr->Ryy00_real = hs_mimo_mdsp_log_ptr->Ryy00;
        hs_mimo_sample_ptr->Ryy01_real = hs_mimo_mdsp_log_ptr->Ryy01Real;           
  
        hs_mimo_sample_ptr->Ryy01_imag = hs_mimo_mdsp_log_ptr->Ryy01Imag;
        hs_mimo_sample_ptr->Ryy11_real = hs_mimo_mdsp_log_ptr->Ryy11;

        /* increment the counter by 8 */
        hs_mimo_demod_sample_buf_offset = hs_mimo_demod_sample_buf_offset + 8;

        /* SEQ 1 */
        HS_LOG_MIMO_DEMOD_SET_VAL(hs_mimo_sample_ptr->seq0001_part1,
                 SEQ00_REAL, hs_mimo_mdsp_log_ptr->seq[0].seq00Real);
        HS_LOG_MIMO_DEMOD_SET_VAL(hs_mimo_sample_ptr->seq0001_part1,
                 SEQ00_IMAG, hs_mimo_mdsp_log_ptr->seq[0].seq00Imag);
        HS_LOG_MIMO_DEMOD_SET_VAL(hs_mimo_sample_ptr->seq0001_part1,
                 SEQ01_REAL_PART1, hs_mimo_mdsp_log_ptr->seq[0].seq01RealPt1);
  
        HS_LOG_MIMO_DEMOD_SET_VAL(hs_mimo_sample_ptr->seq0001_part2,
                 SEQ01_REAL_PART2, hs_mimo_mdsp_log_ptr->seq[0].seq01RealPt2);
        HS_LOG_MIMO_DEMOD_SET_VAL(hs_mimo_sample_ptr->seq0001_part2,
                 SEQ01_IMAG, hs_mimo_mdsp_log_ptr->seq[0].seq01Imag);

        /* increment the counter by 6 */
        hs_mimo_demod_sample_buf_offset = hs_mimo_demod_sample_buf_offset + 6;

        /* SEQ 2 */
        HS_LOG_MIMO_DEMOD_SET_VAL(hs_mimo_sample_ptr->seq1011_part1,
                 SEQ10_REAL, hs_mimo_mdsp_log_ptr->seq[1].seq00Real);
        HS_LOG_MIMO_DEMOD_SET_VAL(hs_mimo_sample_ptr->seq1011_part1,
                 SEQ10_IMAG, hs_mimo_mdsp_log_ptr->seq[1].seq00Imag);
        HS_LOG_MIMO_DEMOD_SET_VAL(hs_mimo_sample_ptr->seq1011_part1,
                 SEQ11_REAL_PART1, hs_mimo_mdsp_log_ptr->seq[1].seq01RealPt1);
        HS_LOG_MIMO_DEMOD_SET_VAL(hs_mimo_sample_ptr->seq1011_part2,
                 SEQ11_REAL_PART2, hs_mimo_mdsp_log_ptr->seq[1].seq01RealPt2);
        HS_LOG_MIMO_DEMOD_SET_VAL(hs_mimo_sample_ptr->seq1011_part2,
                 SEQ11_IMAG, hs_mimo_mdsp_log_ptr->seq[1].seq01Imag);

        /* increment the counter by 6 */
        hs_mimo_demod_sample_buf_offset = hs_mimo_demod_sample_buf_offset + 6;

        /* round bits */
        HS_LOG_MIMO_DEMOD_SET_VAL(hs_mimo_sample_ptr->seq_no_of_roundbits,
                 RB_ROW0, hs_mimo_mdsp_log_ptr->Rnd0);
        HS_LOG_MIMO_DEMOD_SET_VAL(hs_mimo_sample_ptr->seq_no_of_roundbits,
                 RB_ROW1, hs_mimo_mdsp_log_ptr->Rnd1);

        /* increment the counter by 1 */
        hs_mimo_demod_sample_buf_offset++;

      }

      /* proceed if SIC_done_second_decode and Genie_Correlation_used_for_DRM is enabled*/
      if (hs_mimo_mdsp_log_ptr->sicDone2ndDecode == 1 && hs_mimo_mdsp_log_ptr->genieCorrelaton == 1)
      {
        seq_x_type *seq_x_ptr;
        uint8 *seq_no_of_roundbits_rowx;

        seq_x_ptr =
          (seq_x_type *)&(this_hs_mimo_demod_log_pkt->info[hs_mimo_demod_sample_buf_offset]);

         HS_LOG_MIMO_DEMOD_SET_VAL(seq_x_ptr->seqx0_part1,
                 SEQx0_REAL, hs_mimo_mdsp_log_ptr->seq[2].seq00Real);
         HS_LOG_MIMO_DEMOD_SET_VAL(seq_x_ptr->seqx0_part1,
                 SEQx0_IMAG, hs_mimo_mdsp_log_ptr->seq[2].seq00Imag);
         HS_LOG_MIMO_DEMOD_SET_VAL(seq_x_ptr->seqx0_part1,
                 SEQx1_REAL_PART1, hs_mimo_mdsp_log_ptr->seq[2].seq01RealPt1);
         HS_LOG_MIMO_DEMOD_SET_VAL(seq_x_ptr->seqx0_part2,
                 SEQx1_REAL_PART2, hs_mimo_mdsp_log_ptr->seq[2].seq01RealPt2);
         HS_LOG_MIMO_DEMOD_SET_VAL(seq_x_ptr->seqx0_part2,
                 SEQx1_IMAG, hs_mimo_mdsp_log_ptr->seq[2].seq01Imag);     
         
         /* increment the counter by 6 */
         hs_mimo_demod_sample_buf_offset = hs_mimo_demod_sample_buf_offset + 6;

         /*round bits*/
         seq_no_of_roundbits_rowx = (uint8 *)&(this_hs_mimo_demod_log_pkt->info[hs_mimo_demod_sample_buf_offset]);

         HS_LOG_MIMO_DEMOD_SET_VAL(*seq_no_of_roundbits_rowx,  
                 RB_ROWx, hs_mimo_mdsp_log_ptr->Rnd2);
          
         /* increment the counter by 1 */
         hs_mimo_demod_sample_buf_offset = hs_mimo_demod_sample_buf_offset + 1;
      }


      /* proceed if SIC_done_third_decode and Genie_Correlation_used_for_DRM is enabled*/
      if (hs_mimo_mdsp_log_ptr->sicDone3rdDecode == 1 && hs_mimo_mdsp_log_ptr->genieCorrelaton == 1)
      {
        seq_y_type *seq_y_ptr;
        uint8 *seq_no_of_roundbits_rowy;

        seq_y_ptr =
          (seq_y_type *)&(this_hs_mimo_demod_log_pkt->info[hs_mimo_demod_sample_buf_offset]);

         HS_LOG_MIMO_DEMOD_SET_VAL(seq_y_ptr->seqy0_part1,
                 SEQy0_REAL, hs_mimo_mdsp_log_ptr->seq[3].seq00Real);
         HS_LOG_MIMO_DEMOD_SET_VAL(seq_y_ptr->seqy0_part1,
                 SEQy0_IMAG, hs_mimo_mdsp_log_ptr->seq[3].seq00Imag);
         HS_LOG_MIMO_DEMOD_SET_VAL(seq_y_ptr->seqy0_part1,
                 SEQy1_REAL_PART1, hs_mimo_mdsp_log_ptr->seq[3].seq01RealPt1);
         HS_LOG_MIMO_DEMOD_SET_VAL(seq_y_ptr->seqy0_part2,
                 SEQy1_REAL_PART2, hs_mimo_mdsp_log_ptr->seq[3].seq01RealPt2);
         HS_LOG_MIMO_DEMOD_SET_VAL(seq_y_ptr->seqy0_part2,
                 SEQy1_IMAG, hs_mimo_mdsp_log_ptr->seq[3].seq01Imag);     
         
         /* increment the counter by 6 */
        hs_mimo_demod_sample_buf_offset = hs_mimo_demod_sample_buf_offset + 6;

          /*round bits*/
        seq_no_of_roundbits_rowy = (uint8 *)&(this_hs_mimo_demod_log_pkt->info[hs_mimo_demod_sample_buf_offset]);

        HS_LOG_MIMO_DEMOD_SET_VAL(*seq_no_of_roundbits_rowy,  
                 RB_ROWy, hs_mimo_mdsp_log_ptr->Rnd3);
          
        /* increment the counter by 1 */
        hs_mimo_demod_sample_buf_offset = hs_mimo_demod_sample_buf_offset + 1;
      }

      /* Reconstruction Weights (RW), */
      /* proceed if SIC_done_second_decode and Genie_Correlation_used_for_DRM is enabled*/
      if (hs_mimo_mdsp_log_ptr->sicDone2ndDecode == 1 && hs_mimo_mdsp_log_ptr->genieCorrelaton == 1)
      {
        rw_type *rw_second_decode_genie;
       
        rw_second_decode_genie =
          (rw_type *)&(this_hs_mimo_demod_log_pkt->info[hs_mimo_demod_sample_buf_offset]);

         HS_LOG_MIMO_DEMOD_SET_VAL(rw_second_decode_genie->rw_parts_part1,
                 RW00_REAL, hs_mimo_mdsp_log_ptr->rw[1].rw00Real);
         HS_LOG_MIMO_DEMOD_SET_VAL(rw_second_decode_genie->rw_parts_part1,
                 RW00_IMAG, hs_mimo_mdsp_log_ptr->rw[1].rw00Imag);
         HS_LOG_MIMO_DEMOD_SET_VAL(rw_second_decode_genie->rw_parts_part1,
                 RW01REALPT1, hs_mimo_mdsp_log_ptr->rw[1].rw01RealPt1);
         HS_LOG_MIMO_DEMOD_SET_VAL(rw_second_decode_genie->rw_parts_part2,
                 RW01REALPT2, hs_mimo_mdsp_log_ptr->rw[1].rw01RealPt2);
         HS_LOG_MIMO_DEMOD_SET_VAL(rw_second_decode_genie->rw_parts_part2,
                 RW01IMAGPT1, hs_mimo_mdsp_log_ptr->rw[1].rw01Imag);
         HS_LOG_MIMO_DEMOD_SET_VAL(rw_second_decode_genie->rw_parts_part3,
                 RW01IMAGPT2, hs_mimo_mdsp_log_ptr->rw[1].rw01Imag >> 9);     
       
         /* increment the counter by 7 */
        hs_mimo_demod_sample_buf_offset = hs_mimo_demod_sample_buf_offset + 7;
       
      }

       /* proceed if only SIC_done_second_decode is enabled*/
      if (hs_mimo_mdsp_log_ptr->sicDone2ndDecode == 1)
      {
        rw_type *rw_second_decode;
       
        rw_second_decode =
          (rw_type *)&(this_hs_mimo_demod_log_pkt->info[hs_mimo_demod_sample_buf_offset]);

         HS_LOG_MIMO_DEMOD_SET_VAL(rw_second_decode->rw_parts_part1,
                 RW00_REAL, hs_mimo_mdsp_log_ptr->rw[0].rw00Real);
         HS_LOG_MIMO_DEMOD_SET_VAL(rw_second_decode->rw_parts_part1,
                 RW00_IMAG, hs_mimo_mdsp_log_ptr->rw[0].rw00Imag);
         HS_LOG_MIMO_DEMOD_SET_VAL(rw_second_decode->rw_parts_part1,
                 RW01REALPT1, hs_mimo_mdsp_log_ptr->rw[0].rw01RealPt1);
         HS_LOG_MIMO_DEMOD_SET_VAL(rw_second_decode->rw_parts_part2,
                 RW01REALPT2, hs_mimo_mdsp_log_ptr->rw[0].rw01RealPt2);
         HS_LOG_MIMO_DEMOD_SET_VAL(rw_second_decode->rw_parts_part2,
                 RW01IMAGPT1, hs_mimo_mdsp_log_ptr->rw[0].rw01Imag);
         HS_LOG_MIMO_DEMOD_SET_VAL(rw_second_decode->rw_parts_part3,
                 RW01IMAGPT2, hs_mimo_mdsp_log_ptr->rw[0].rw01Imag >> 9);      
       
         /* increment the counter by 7 */
        hs_mimo_demod_sample_buf_offset = hs_mimo_demod_sample_buf_offset + 7;
      }

      /* proceed if SIC_done_third_decode and Genie_Correlation_used_for_DRM is enabled*/
      if (hs_mimo_mdsp_log_ptr->sicDone3rdDecode == 1 && hs_mimo_mdsp_log_ptr->genieCorrelaton == 1)
      {
        rw_type *rw_third_decode_genie;
       
        rw_third_decode_genie =
          (rw_type *)&(this_hs_mimo_demod_log_pkt->info[hs_mimo_demod_sample_buf_offset]);

         HS_LOG_MIMO_DEMOD_SET_VAL(rw_third_decode_genie->rw_parts_part1,
                 RW00_REAL, hs_mimo_mdsp_log_ptr->rw[3].rw00Real);
         HS_LOG_MIMO_DEMOD_SET_VAL(rw_third_decode_genie->rw_parts_part1,
                 RW00_IMAG, hs_mimo_mdsp_log_ptr->rw[3].rw00Imag);
         HS_LOG_MIMO_DEMOD_SET_VAL(rw_third_decode_genie->rw_parts_part1,
                 RW01REALPT1, hs_mimo_mdsp_log_ptr->rw[3].rw01RealPt1);
         HS_LOG_MIMO_DEMOD_SET_VAL(rw_third_decode_genie->rw_parts_part2,
                 RW01REALPT2, hs_mimo_mdsp_log_ptr->rw[3].rw01RealPt2);
         HS_LOG_MIMO_DEMOD_SET_VAL(rw_third_decode_genie->rw_parts_part2,
                 RW01IMAGPT1, hs_mimo_mdsp_log_ptr->rw[3].rw01Imag); 
         HS_LOG_MIMO_DEMOD_SET_VAL(rw_third_decode_genie->rw_parts_part3,
                 RW01IMAGPT2, hs_mimo_mdsp_log_ptr->rw[3].rw01Imag >> 9);      
       
         /* increment the counter by 7 */
        hs_mimo_demod_sample_buf_offset = hs_mimo_demod_sample_buf_offset + 7;
       
      }

      /* proceed if SIC_done_third_decode and Genie_Correlation_used_for_DRM is enabled*/
      if (hs_mimo_mdsp_log_ptr->sicDone3rdDecode == 1)
      {
         rw_type *rw_third_decode;
       
        rw_third_decode =
          (rw_type *)&(this_hs_mimo_demod_log_pkt->info[hs_mimo_demod_sample_buf_offset]);

         HS_LOG_MIMO_DEMOD_SET_VAL(rw_third_decode->rw_parts_part1,
                 RW00_REAL, hs_mimo_mdsp_log_ptr->rw[2].rw00Real);
         HS_LOG_MIMO_DEMOD_SET_VAL(rw_third_decode->rw_parts_part1,
                 RW00_IMAG, hs_mimo_mdsp_log_ptr->rw[2].rw00Imag);
         HS_LOG_MIMO_DEMOD_SET_VAL(rw_third_decode->rw_parts_part1,
                 RW01REALPT1, hs_mimo_mdsp_log_ptr->rw[2].rw01RealPt1);
         HS_LOG_MIMO_DEMOD_SET_VAL(rw_third_decode->rw_parts_part2,
                 RW01REALPT2, hs_mimo_mdsp_log_ptr->rw[2].rw01RealPt2);
         HS_LOG_MIMO_DEMOD_SET_VAL(rw_third_decode->rw_parts_part2,
                 RW01IMAGPT1, hs_mimo_mdsp_log_ptr->rw[2].rw01Imag);  
         HS_LOG_MIMO_DEMOD_SET_VAL(rw_third_decode->rw_parts_part3,
                 RW01IMAGPT2, hs_mimo_mdsp_log_ptr->rw[2].rw01Imag >> 9);
       
         /* increment the counter by 7 */
        hs_mimo_demod_sample_buf_offset = hs_mimo_demod_sample_buf_offset + 7;
      }

      /* Increment number of samples accumulated */
      this_hs_mimo_demod_log_pkt->num_samples++;


      /* Handle RESTART and STOP actions */
      /* ------------------------------- */

      /* check if there action type RECONFIG and STOP set */
      if ((hs_mimo_demod_log_action == HS_STAT_GEN_RESTART) ||
          (hs_mimo_demod_log_action == HS_STAT_GEN_STOP))
      {
        /* Check if this is sub frame to STOP as the part of action STOP
           or RESTART */
        if (global_sub_fr_num == hs_mimo_demod_log_final_sub_fn)
        {
          /* STOP this accumulation now */

          /* Flush already accumulated samples */
          dec_hs_mimo_demod_stop_logging ();
          
          WL1_MSG3(HIGH, "Stoping HS mimo logging %d CFN %d SubFn %d",
                   hs_mimo_demod_log_info_buf_idx,
                   this_frame_cfn,
                   global_sub_fr_num);

          /* no need to further get sub frames */
          break;
        } /* End if, sub frame to stop accumulation has occured */
      } /* end if, action RESTART or STOP pending */

      /* Go to next sub frame */
      global_sub_fr_num = (uint16)
        normalize(global_sub_fr_num + 1, HS_MAX_GLOBAL_SUB_FRAME_COUNT);
      
      /* If acumulation has reached max samples then this must be submitted */
      /* ------------------------------------------------------------------ */

      if (hs_mimo_demod_log_pkt_buf[hs_mimo_demod_log_info_buf_idx].num_samples == HS_MIMO_LOG_MAX_SAMPLE)
      {
        /* HSDPA decode status log header pointer */
        hsdpa_log_hdr_struct_type *hs_mimo_log_pkt_hdr;

        /* Get header to current log buffer and update total size of
           information accumulated */
          hs_mimo_log_pkt_hdr =
            (hsdpa_log_hdr_struct_type*) (&hs_mimo_demod_log_pkt_buf[hs_mimo_demod_log_info_buf_idx]);
          hs_mimo_log_pkt_hdr->len = 5 + 12 + hs_mimo_demod_sample_buf_offset;

        /* send command to submit this log packet */
        dec_hs_mimo_demod_send_log_submit_cmd(hs_mimo_demod_log_info_buf_idx);

        /* Get available MIMO demod log accumulation buffer */
        hs_mimo_demod_log_info_buf_idx = HS_INFO_TABLE_INVALID_VAL;
        for (index_buf = 0;
             index_buf < HS_MIMO_LOG_NUM_BUF;
             index_buf++)
        {
          if (hs_mimo_demod_log_buf_avail_status[index_buf])
          {
            hs_mimo_demod_log_buf_avail_status[index_buf] = FALSE;
            hs_mimo_demod_log_info_buf_idx = index_buf;
            break;
          }
        }
        /* If can't get buffer then there is no use of proceeding. It should
           never happen because of double buffering */
        if (hs_mimo_demod_log_info_buf_idx == HS_INFO_TABLE_INVALID_VAL)
        {
          WL1_MSG0(ERROR, "Can't get HS mimo demod log buf");

          /* iterate over all log buffer status to mark them available */
          for (index_buf = 0; index_buf < HS_MIMO_LOG_NUM_BUF; index_buf++)
          {
            hs_mimo_demod_log_buf_avail_status[index_buf] = TRUE;
          }

          /* Set log buffer index to 0 */
          hs_mimo_demod_log_info_buf_idx = 0;
          hs_mimo_demod_log_buf_avail_status[hs_mimo_demod_log_info_buf_idx] = FALSE;

          /* Init log submit request to FALSE */
          hs_mimo_demod_log_submit = FALSE;
          hs_mimo_demod_log_buf_idx_to_submit = HS_INFO_TABLE_INVALID_VAL;
        }

        hs_mimo_demod_sample_buf_offset = 0;
        /* get pointer to current mimo debug log buffer */
        this_hs_mimo_demod_log_pkt =
          &(hs_mimo_demod_log_pkt_buf[hs_mimo_demod_log_info_buf_idx]);
        /* Do time stamp corresponding to first sample */
        log_set_timestamp(this_hs_mimo_demod_log_pkt);
        /* init number of samples accumulated to 0 */
        this_hs_mimo_demod_log_pkt->num_samples = 0;
         /* Set version info */
        this_hs_mimo_demod_log_pkt->version = 0x02;

        /* Save starting sub frame number */
        this_hs_mimo_demod_log_pkt->starting_sub_fn_and_sp = global_sub_fr_num;
        
        /* set the Scpich-Pcpich power imbalance */
        this_hs_mimo_demod_log_pkt->Scpich_Pcpich_Power_Imbalance = s_cpich_po;
      } /* end if, max decode status samples have been collected */

      hs_mimo_demod_log_rd_ptr++;
      hs_mimo_demod_log_rd_ptr %= WFW_DEMOD_HS_MIMO_DEMOD_LOG_PACKET_BUF_SIZE;
    }

    /* Update the RdPtr in the FW-SW interface */
    mcalwcdma_fwsw_intf_addr_ptr->dbackHsMimoDemodRdPtr = hs_mimo_demod_log_rd_ptr;
  }
  HS_LOG_INTFREE();
}


/*===========================================================================
FUNCTION dec_hs_mimo_demod_stop_logging

DESCRIPTION
  This function prepares the MIMO demod logging buffer to be submitted to diag
  It calls the function that posts a MIMO demod log submit local command and then
  clears the state of MIMO demod logging.
  
DEPENDENCIES
  hs_mimo_log_info_buf_idx should be valid while making
  a call to this function.

RETURN VALUE
  None

SIDE EFFECTS
  ===========================================================================*/
void dec_hs_mimo_demod_stop_logging(void)
{
  /* function local variables */
  /* ------------------------ */
  /* pointer to MIMO demod log packets */
  HS_MIMO_DEMOD_LOG_PKT_type *this_hs_mimo_demod_log_pkt;

  /* MIMO demod log header pointer */
  hsdpa_log_hdr_struct_type *hs_mimo_demod_log_pkt_hdr;

  /*** function code starts here ***/

  /* get pointer to current MIMO demod log buffer */
  this_hs_mimo_demod_log_pkt =
    &(hs_mimo_demod_log_pkt_buf[hs_mimo_demod_log_info_buf_idx]);

  /* Populate the header */
  hs_mimo_demod_log_pkt_hdr =
    (hsdpa_log_hdr_struct_type*) (&hs_mimo_demod_log_pkt_buf[hs_mimo_demod_log_info_buf_idx]);
  hs_mimo_demod_log_pkt_hdr->len = sizeof(HS_MIMO_DEMOD_LOG_PKT_type); 
          
  /* send command to submit this decode status log packet */
  dec_hs_mimo_demod_send_log_submit_cmd(
    hs_mimo_demod_log_info_buf_idx);

  /* mark HS inactive */
  hs_mimo_demod_log_accum_active = FALSE;
  hs_mimo_demod_logging_enabled_qxdm = FALSE;

  /* if action was reconfig then change it to start. or
     if it was stop then it is al done, mark it NOOP */
  if (hs_mimo_demod_log_action == HS_STAT_GEN_RESTART)
  {
    hs_mimo_demod_log_action = HS_STAT_GEN_START;
  }
  else
  {
    hs_mimo_demod_log_action = HS_STAT_GEN_NOOP;
  }
}

/*===========================================================================
FUNCTION dec_hs_mimo_demod_send_log_submit_cmd

DESCRIPTION
  This function makes a request to post a local command to submit the 
  currently accumulated log packet to diag. Before it does so, it checks if 
  a previous log packet that was submitted to diag has been  serviced or not.
  If not, it drops the current packet and proceeds further.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

void dec_hs_mimo_demod_send_log_submit_cmd(
  /* HS decode status log buffer index to submit */
  uint8 hs_mimo_log_idx)
{
  /* function local variables */
  /* ------------------------ */

  /* log packet pointer that need to be submitted */
  HS_MIMO_DEMOD_LOG_PKT_type *this_hs_mimo_demod_log_pkt;

  /* log header pointer */
  hsdpa_log_hdr_struct_type *hs_mimo_demod_log_pkt_hdr;

  /*** function code starts here ***/

  if (hs_mimo_log_idx >= HS_MIMO_LOG_NUM_BUF)
  {
    ERR_FATAL("Incorrect index for MIMO demod submit log %d", hs_mimo_log_idx, 0, 0);
  }

  /* Check if there is a pending request to submit previous log packet */
  if (hs_mimo_demod_log_submit)
  {
    /* Print error message and mark this buffer as available one */

      /* get log packet pointer */
      this_hs_mimo_demod_log_pkt = &(hs_mimo_demod_log_pkt_buf[hs_mimo_log_idx]);
      /* Get log packet header pointer and set log code to that */
      hs_mimo_demod_log_pkt_hdr = (hsdpa_log_hdr_struct_type*) this_hs_mimo_demod_log_pkt;
      /* submit the log packet */
      WL1_MSG3(ERROR, "HS_LLR_INFO_LOG:Failed idx %d info sz %d idx pending %d",
                      hs_mimo_log_idx,
                      hs_mimo_demod_log_pkt_hdr->len,
                      hs_mimo_demod_log_buf_idx_to_submit);

      hs_mimo_demod_log_buf_avail_status[hs_mimo_log_idx] = TRUE;
  }
  else
  {
    /* Save HS decode status log buffer index to submit later in task context */
    hs_mimo_demod_log_submit = TRUE;
    hs_mimo_demod_log_buf_idx_to_submit = hs_mimo_log_idx;

    /* Send local command to l1m */
    hs_send_submit_log_cmd();
  }
}

/*===========================================================================
FUNCTION dec_hs_mimo_demod_submit_log_buffer

DESCRIPTION
  This function is called from hscfg module in response of HS log submit
  command. It checks for MIMO demod log submit flag. 
  If it is set to TRUE, it submit log buffer pointed by log buffer index 
  to submit.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
void dec_hs_mimo_demod_submit_log_buffer(void)
{
  /* function local variables */
  /* ------------------------ */

  /* flag to hold the state of the log code in QXDM */
  boolean log_code_disabled = FALSE;

  /* log packet pointer that need to be submitted */
  HS_MIMO_DEMOD_LOG_PKT_type *this_hs_mimo_demod_log_pkt;

  /* log header pointer */
  hsdpa_log_hdr_struct_type *hs_mimo_demod_log_pkt_hdr;
  /* buffer index to submit */
  uint8 log_info_buf_idx;

  /*** function code starts here ***/

  /* If log need to be submitted then proceed otherwise simply return from here */
  if (hs_mimo_demod_log_submit)
  {
    /* save log buffer index to submit */
    log_info_buf_idx = hs_mimo_demod_log_buf_idx_to_submit;
  }
  else
  {
    return;
  }

  if (log_info_buf_idx >= HS_MIMO_LOG_NUM_BUF)
  {
    ERR_FATAL("Incorrect index for MIMO demod submit buffer %d", log_info_buf_idx, 0, 0);
  }

  if (log_status(HS_MIMO_DEMOD_LOG_PKT))
    {
      /* get log packet pointer */
      this_hs_mimo_demod_log_pkt = &(hs_mimo_demod_log_pkt_buf[log_info_buf_idx]);
      /* Get log packet header pointer and set log code to that */
      hs_mimo_demod_log_pkt_hdr = (hsdpa_log_hdr_struct_type*) this_hs_mimo_demod_log_pkt;
      hs_mimo_demod_log_pkt_hdr->code = HS_MIMO_DEMOD_LOG_PKT;
        
      /* submit the log packet */
      if (!log_submit((PACKED void *) this_hs_mimo_demod_log_pkt))
      {
        WL1_MSG1(ERROR, "HS_LLR_INFO_LOG:Failed info sz %d",
                        hs_mimo_demod_log_pkt_hdr->len);
      }
    }
  else
    {
      /* Mark the flag to indicate that the log code is disabled */
      log_code_disabled = TRUE;
    }

  HS_LOG_INTLOCK();
  /* Mark this LLR status log buffer to available */
  hs_mimo_demod_log_buf_avail_status[log_info_buf_idx] = TRUE;
  /* reset log to submit flag */
  hs_mimo_demod_log_submit = FALSE;

  /* If the log code is disabled, clear all buffers and mark logging state
     as well as logging buffer index as invalid */
  if (log_code_disabled)
  {
    uint8 index_buf;
    
    /* Mark logging OFF by QXDM */
    hs_mimo_demod_logging_enabled_qxdm = FALSE;
    /* Mark the log buffer index as invalid */
    hs_mimo_demod_log_info_buf_idx = HS_INFO_TABLE_INVALID_VAL;    
    /* iterate over all log buffer status to mark them available */
    for (index_buf = 0; index_buf < 2; index_buf++)
    {
      hs_mimo_demod_log_buf_avail_status[index_buf] = TRUE;
    }
  }

  HS_LOG_INTFREE();
}

/*===========================================================================
FUNCTION dec_hs_mimo_chan_log_init
 
DESCRIPTION This function initializes the log states for MIMO chan analysis
            log packet
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
void dec_hs_mimo_chan_log_init(void)
{
  /* function local variables */
  /* ------------------------ */

  /* loop index */
  uint8 index_buf;
  
  /*** function code starts here ***/

  /* MIMO demod accumulation logging is inactive */
  hs_mimo_chan_log_accum_active = FALSE;
  hs_mimo_chan_logging_enabled_qxdm = FALSE;
  
  /* MIMO demod accumulation logging action is NO_OP */
  hs_mimo_chan_log_action = HS_STAT_GEN_NOOP;

  /* iterate over all log buffer status to mark them available */
  for (index_buf = 0; index_buf < HS_MIMO_LOG_NUM_BUF; index_buf++)
  {
    hs_mimo_chan_log_buf_avail_status[index_buf] = TRUE;
  }

  /* Set log buffer index to INVALID */
  hs_mimo_chan_log_info_buf_idx = HS_INFO_TABLE_INVALID_VAL;

  /* Init log submit request to FALSE */
  hs_mimo_chan_log_submit = FALSE;
  hs_mimo_chan_log_buf_idx_to_submit = HS_INFO_TABLE_INVALID_VAL;
}

/*===========================================================================
FUNCTION dec_hs_mimo_chan_log_update

DESCRIPTION
  This function initialize mimo demod information.
  
  It is called when ever chan analysis log geneartion is started at START or RESTART
  action OR
  when ever samples generated using max required number of samples
  
DEPENDENCIES
  None

RETURN VALUE
  5 Bit unquantized value form 1 to 30

SIDE EFFECTS
  None
===========================================================================*/
void dec_hs_mimo_chan_log_update(void)
{
  /* function local variables */
  /* ------------------------ */
  
  /* CFN for which MIMO demod logging needs to be done */
  uint8 this_frame_cfn;

  /* pointer to amp_n_eng_est log packets */
  HS_MIMO_CH_ANALYSIS_LOG_PKT_type *this_hs_mimo_chan_log_pkt;

  /*** function code starts here ***/

  /* Do logging only if MIMO is active */
  if (!hs_get_mimo_state(L1_HSDPA_CARRIER_0))
  {
    return;
  }

  HS_LOG_INTLOCK();

  /* get CFN for the frame at which to accumulate MIMO logging information */
  this_frame_cfn = (uint8) normalize((stmr_get_combiner_cfn(TLM_GET_RXTL_SRC()) - 2), MAX_CFN_COUNT);

  /* Handle any pending START action */
  /* ------------------------------- */

  /* Check if mimo channel analysis logging accumulation is active */
  if (!hs_mimo_chan_log_accum_active)
  {
    /* MIMO demod logging not active here */

    /* Check for LLR log action for start */
    if ((hs_mimo_chan_log_action == HS_STAT_GEN_START) ||
        (hs_mimo_chan_log_action == HS_STAT_GEN_RESTART))
    {
      /* If CFN generated above for accumulation matches start CFN */
      if (this_frame_cfn == hs_mimo_chan_log_start_cfn)
      {
        /* Start mimo logging */
        hs_mimo_chan_cfnx5_sub_fn_offset = (int16)
          normalize(hs_mimo_chan_log_start_sub_fn - (hs_mimo_chan_log_start_cfn * 5),
                    HS_MAX_GLOBAL_SUB_FRAME_COUNT);

        hs_mimo_chan_log_action = HS_STAT_GEN_NOOP;
        hs_mimo_chan_log_info_buf_idx = HS_INFO_TABLE_INVALID_VAL;              
        hs_mimo_chan_log_accum_active = TRUE;
        hs_mimo_chan_logging_enabled_qxdm = FALSE;
      } /* end if, start acummulation  */
    } /* end if, START action pending */
  } /* end if, stat generation not active */

  /* Check for HS Stat Stop action if QXDM is turned off */
  if ((!hs_mimo_chan_logging_enabled_qxdm) && (hs_mimo_chan_log_accum_active) &&
      ((hs_mimo_chan_log_action == HS_STAT_GEN_RESTART) ||
       (hs_mimo_chan_log_action == HS_STAT_GEN_STOP)))
  {
    /* Time to disable mimo logging */
    if (hs_mimo_chan_log_info_buf_idx != HS_INFO_TABLE_INVALID_VAL)
    {
      /* If some samples are already accumulated, flush them */
      WL1_MSG1(HIGH , "Stopping HS mimo chan logging CFN %d", this_frame_cfn);
      dec_hs_mimo_chan_stop_logging ();
    }
    else
    {
      /* Reset HS mimo Logging state */
      WL1_MSG0(HIGH , "Stopped HS mimo logging");
      dec_hs_mimo_chan_log_init ();
    }
  }

  /* If the logging has been turned OFF, poll to check if it is enabled now */
  if ((!hs_mimo_chan_logging_enabled_qxdm) && (hs_mimo_chan_log_accum_active))
  {
    if (log_status(HS_MIMO_CH_ANALYSIS_LOG_PKT))
    {
      hs_mimo_chan_logging_enabled_qxdm = TRUE;
      hs_mimo_chan_log_info_buf_idx = HS_INFO_TABLE_INVALID_VAL;      
    }
  }
  
  /* Do accumulation and logging */
  /* --------------------------- */

  if ((hs_mimo_chan_log_accum_active) && (hs_mimo_chan_logging_enabled_qxdm))
  {
    /* loop index to get log info for 5 sub frames in this frame */
    uint8 index_sub_fr;
    /* get previous frame starting sub frame number */
    uint16 global_sub_fr_num;

    /* This is possible only when logging is turned OFF and then turned ON from
       within QXDM. Allocate a new buffer to start log accumulation */
    if (hs_mimo_chan_log_info_buf_idx == HS_INFO_TABLE_INVALID_VAL)
    {
      /* Allocate a new buffer to accumulate log samples */
      
      uint8 index_buf; /* loop indicies */
    
      /* Get available HS amp and energy est acumulation buffer */
      for (index_buf = 0;
           index_buf < HS_MIMO_LOG_NUM_BUF;
           index_buf++)
      {
        if (hs_mimo_chan_log_buf_avail_status[index_buf])
        {
          hs_mimo_chan_log_buf_avail_status[index_buf] = FALSE;
          hs_mimo_chan_log_info_buf_idx = index_buf;
          break;
        }
      }
      /* If can't get buffer then there is no use of proceeding. It should
         never happen because of double buffering */
      if (hs_mimo_chan_log_info_buf_idx == HS_INFO_TABLE_INVALID_VAL)
      {
        WL1_MSG0(ERROR, "Can't get HS mimo chan log buf");

        /* iterate over all log buffer status to mark them available */
        for (index_buf = 0; index_buf < HS_MIMO_LOG_NUM_BUF; index_buf++)
        {
          hs_mimo_chan_log_buf_avail_status[index_buf] = TRUE;
        }

        /* Set log buffer index to 0 */
        hs_mimo_chan_log_info_buf_idx = 0;
        hs_mimo_chan_log_buf_avail_status[hs_mimo_chan_log_info_buf_idx] = FALSE;

        /* Init log submit request to FALSE */
        hs_mimo_chan_log_submit = FALSE;
        hs_mimo_chan_log_buf_idx_to_submit = HS_INFO_TABLE_INVALID_VAL;
      }
    
      /* get pointer to current HS amp and energy est debug log buffer */
      this_hs_mimo_chan_log_pkt = 
         &(hs_mimo_chan_log_pkt_buf[hs_mimo_chan_log_info_buf_idx]);

      /* Do time stamp corresponding to first sample */
      log_set_timestamp(this_hs_mimo_chan_log_pkt);
      /* init number of samples accumulated to 0 */
      this_hs_mimo_chan_log_pkt->num_samples = 0;
      /* set the verison info*/  
      this_hs_mimo_chan_log_pkt->version = 0x02;
      /* Save starting sub frame number */
      /* If the DPCH to SCCH offset is >= 30, it means that the starting global subframe number
         is advanced by 1 to keep the separation between end of HS-PDSCH to start of HS-SCCH > 40bpgs */
      this_hs_mimo_chan_log_pkt->starting_sub_fn_and_sp =
        (uint16)normalize((this_frame_cfn * 5) + hs_mimo_chan_cfnx5_sub_fn_offset, 
                           HS_MAX_GLOBAL_SUB_FRAME_COUNT);      
      hs_mimo_chan_sample_buf_offset = 0 ;
      /* Init the log buffer idx and reset action to NOOP */
      hs_mimo_chan_log_action = HS_STAT_GEN_NOOP;
    
      WL1_MSG3(HIGH, "Starting HS mimo chan logging %d CFN %d SubFn %d",
               hs_mimo_chan_log_info_buf_idx,
               this_frame_cfn,
               this_hs_mimo_chan_log_pkt->starting_sub_fn_and_sp);
    }
    
    /* Generate global sub frame number from which accumulation will be done
       for 5 sub frames */
    global_sub_fr_num =
      (uint16)normalize((this_frame_cfn * 5) + hs_mimo_chan_cfnx5_sub_fn_offset,
                HS_MAX_GLOBAL_SUB_FRAME_COUNT);

    /* Iterate over 5 sub frames to generate stats */
    for (index_sub_fr = 0;
         index_sub_fr < HS_NUM_SUB_FR_PER_RADIO_FR;
         index_sub_fr++)
    {
      uint8 index_buf; /* loop index */      
      /* mDSP address from where to read the data */
      WfwChannelAnalysisStruct* hs_mimo_mdsp_log_ptr;
      /* this sub frame entry index into mDSP current buffer. It is indexed
      using last 4 bits of sub frame number */
      uint8 hs_decode_info_idx_mdsp;
      uint32 temp_val;
      hs_mimo_ch_analysis_struct_type* hs_mimo_sample_ptr;
      /* Get index of this sub frame entry into mDSP. This is the last 4
         bits of sub frame number (modulo 16 operation) */
      hs_decode_info_idx_mdsp = global_sub_fr_num & 0xF;

      /* get pointer to current HS amp and energy est debug log buffer */
      this_hs_mimo_chan_log_pkt =
        &(hs_mimo_chan_log_pkt_buf[hs_mimo_chan_log_info_buf_idx]);
      /* get the mdsp sample location*/
      hs_mimo_mdsp_log_ptr = &(mcalwcdma_fwsw_intf_addr_ptr->chanAnalysis[hs_decode_info_idx_mdsp]);
      /* get the log buffer current location*/
      hs_mimo_sample_ptr =
         (hs_mimo_ch_analysis_struct_type*)&(this_hs_mimo_chan_log_pkt->info[hs_mimo_chan_sample_buf_offset]);
      
      /*log the channel Matrix Estimation*/ 
      hs_mimo_sample_ptr->H00_real = hs_mimo_mdsp_log_ptr->H00;
      temp_val = hs_mimo_mdsp_log_ptr->H00;
      hs_mimo_sample_ptr->H00_imag = temp_val >> 16;

      hs_mimo_sample_ptr->H01_real = hs_mimo_mdsp_log_ptr->H01;
      temp_val = hs_mimo_mdsp_log_ptr->H01;
      hs_mimo_sample_ptr->H01_imag = temp_val >> 16;

      hs_mimo_sample_ptr->H10_real = hs_mimo_mdsp_log_ptr->H10;
      temp_val = hs_mimo_mdsp_log_ptr->H10;
      hs_mimo_sample_ptr->H10_imag = temp_val >> 16;

      hs_mimo_sample_ptr->H11_real = hs_mimo_mdsp_log_ptr->H11;
      temp_val = hs_mimo_mdsp_log_ptr->H11;
      hs_mimo_sample_ptr->H11_imag = temp_val >> 16;

      /*log the covariant info*/
      hs_mimo_sample_ptr->Rnn00_real = hs_mimo_mdsp_log_ptr->Rnn00_real;
      hs_mimo_sample_ptr->Rnn01_real = hs_mimo_mdsp_log_ptr->Rnn01_real;           

      hs_mimo_sample_ptr->Rnn01_imag = hs_mimo_mdsp_log_ptr->Rnn01_imag;
      hs_mimo_sample_ptr->Rnn11_real = hs_mimo_mdsp_log_ptr->Rnn11_real;
        
      hs_mimo_sample_ptr->supported_rate = 0;
      HS_MIMO_CHAN_SET_VAL(hs_mimo_sample_ptr->supported_rate, DS_RATE, hs_mimo_mdsp_log_ptr->dualStreamRate);
      HS_MIMO_CHAN_SET_VAL(hs_mimo_sample_ptr->supported_rate, SS_RATE, hs_mimo_mdsp_log_ptr->singleStreamRate);
      HS_MIMO_CHAN_SET_VAL(hs_mimo_sample_ptr->supported_rate, DS_SS_PREF, hs_mimo_mdsp_log_ptr->ssDsPreference);

      /* Increment number of samples accumulated */
      this_hs_mimo_chan_log_pkt->num_samples++;

      hs_mimo_chan_sample_buf_offset += HS_MIMO_CH_ANALYSIS_SAMPLE_SIZE;
      /* Handle RESTART and STOP actions */
      /* ------------------------------- */

      /* check if there action type RECONFIG and STOP set */
      if ((hs_mimo_chan_log_action == HS_STAT_GEN_RESTART) ||
          (hs_mimo_chan_log_action == HS_STAT_GEN_STOP))
      {
        /* Check if this is sub frame to STOP as the part of action STOP
           or RESTART */
        if (global_sub_fr_num == hs_mimo_chan_log_final_sub_fn)
        {
          /* STOP this accumulation now */

          WL1_MSG3(HIGH, "Stoping HS mimo chan logging %d CFN %d SubFn %d",
                   hs_mimo_chan_log_info_buf_idx,
                   this_frame_cfn,
                   global_sub_fr_num);

          /* Flush already accumulated samples */
          dec_hs_mimo_chan_stop_logging ();
          
          /* no need to further get sub frames */
          break;
        } /* End if, sub frame to stop accumulation has occured */
      } /* end if, action RESTART or STOP pending */

      /* Go to next sub frame */
      global_sub_fr_num = (uint16)
        normalize(global_sub_fr_num + 1, HS_MAX_GLOBAL_SUB_FRAME_COUNT);
      
      /* If acumulation has reached max samples then this must be submitted */
      /* ------------------------------------------------------------------ */

      if (hs_mimo_chan_log_pkt_buf[hs_mimo_chan_log_info_buf_idx].num_samples >= HS_MIMO_CHAN_LOG_MAX_SAMPLE)
      {
        /* HSDPA decode status log header pointer */
        hsdpa_log_hdr_struct_type *hs_mimo_log_pkt_hdr;

        /* Get header to current log buffer and update total size of
           information accumulated */
          hs_mimo_log_pkt_hdr =
            (hsdpa_log_hdr_struct_type*) (&hs_mimo_chan_log_pkt_buf[hs_mimo_chan_log_info_buf_idx]);
          hs_mimo_log_pkt_hdr->len = sizeof(HS_MIMO_CH_ANALYSIS_LOG_PKT_type) -
            ((HS_MIMO_CHAN_LOG_MAX_SAMPLE - this_hs_mimo_chan_log_pkt->num_samples) * 
                  HS_MIMO_CH_ANALYSIS_SAMPLE_SIZE);

        /* send command to submit this log packet */
        dec_hs_mimo_chan_send_log_submit_cmd(hs_mimo_chan_log_info_buf_idx);

        /* Get available MIMO demod log accumulation buffer */
        hs_mimo_chan_log_info_buf_idx = HS_INFO_TABLE_INVALID_VAL;
        for (index_buf = 0;
             index_buf < HS_MIMO_LOG_NUM_BUF;
             index_buf++)
        {
          if (hs_mimo_chan_log_buf_avail_status[index_buf])
          {
            hs_mimo_chan_log_buf_avail_status[index_buf] = FALSE;
            hs_mimo_chan_log_info_buf_idx = index_buf;
            break;
          }
        }

        /* If can't get buffer then there is no use of proceeding. It should
           never happen because of double buffering */
        if (hs_mimo_chan_log_info_buf_idx == HS_INFO_TABLE_INVALID_VAL)
        {
          WL1_MSG0(ERROR, "Can't get HS mimo chan log buf");

          /* iterate over all log buffer status to mark them available */
          for (index_buf = 0; index_buf < HS_MIMO_LOG_NUM_BUF; index_buf++)
          {
            hs_mimo_chan_log_buf_avail_status[index_buf] = TRUE;
          }

          /* Set log buffer index to 0 */
          hs_mimo_chan_log_info_buf_idx = 0;
          hs_mimo_chan_log_buf_avail_status[hs_mimo_chan_log_info_buf_idx] = FALSE;

          /* Init log submit request to FALSE */
          hs_mimo_chan_log_submit = FALSE;
          hs_mimo_chan_log_buf_idx_to_submit = HS_INFO_TABLE_INVALID_VAL;
        }

        hs_mimo_chan_sample_buf_offset = 0 ;
        /* get pointer to current mimo debug log buffer */
        this_hs_mimo_chan_log_pkt =
          &(hs_mimo_chan_log_pkt_buf[hs_mimo_chan_log_info_buf_idx]);
        /* Do time stamp corresponding to first sample */
        log_set_timestamp(this_hs_mimo_chan_log_pkt);
        /* init number of samples accumulated to 0 */
        this_hs_mimo_chan_log_pkt->num_samples = 0;
         /* Set version info */
        this_hs_mimo_chan_log_pkt->version = 0x02;

        /* Save starting sub frame number */
        this_hs_mimo_chan_log_pkt->starting_sub_fn_and_sp = global_sub_fr_num;
        
      } /* end if, max decode status samples have been collected */
    }
  }

  HS_LOG_INTFREE();
}


/*===========================================================================
FUNCTION dec_hs_mimo_chan_stop_logging

DESCRIPTION
  This function prepares the MIMO chanlogging buffer to be submitted to diag
  It calls the function that posts a MIMO chan log submit local command and then
  clears the state of MIMO chan logging.
  
DEPENDENCIES
  hs_mimo_log_info_buf_idx should be valid while making
  a call to this function.

RETURN VALUE
  None

SIDE EFFECTS
  ===========================================================================*/
void dec_hs_mimo_chan_stop_logging(void)
{
  /* function local variables */
  /* ------------------------ */
  /* pointer to MIMO demod log packets */
  HS_MIMO_CH_ANALYSIS_LOG_PKT_type *this_hs_mimo_chan_log_pkt;

  /* MIMO demod log header pointer */
  hsdpa_log_hdr_struct_type *hs_mimo_chan_log_pkt_hdr;

  /*** function code starts here ***/

  /* get pointer to current MIMO demod log buffer */
  this_hs_mimo_chan_log_pkt =
    &(hs_mimo_chan_log_pkt_buf[hs_mimo_chan_log_info_buf_idx]);

  /* Populate the header */
  hs_mimo_chan_log_pkt_hdr =
    (hsdpa_log_hdr_struct_type*) (&hs_mimo_chan_log_pkt_buf[hs_mimo_chan_log_info_buf_idx]);
  hs_mimo_chan_log_pkt_hdr->len = sizeof(HS_MIMO_CH_ANALYSIS_LOG_PKT_type); 
          
  /* send command to submit this decode status log packet */
  dec_hs_mimo_chan_send_log_submit_cmd(
    hs_mimo_chan_log_info_buf_idx);

  /* mark HS inactive */
  hs_mimo_chan_log_accum_active = FALSE;
  hs_mimo_chan_logging_enabled_qxdm = FALSE;

  /* if action was reconfig then change it to start. or
     if it was stop then it is al done, mark it NOOP */
  if (hs_mimo_chan_log_action == HS_STAT_GEN_RESTART)
  {
    hs_mimo_chan_log_action = HS_STAT_GEN_START;
  }
  else
  {
    hs_mimo_chan_log_action = HS_STAT_GEN_NOOP;
  }
}

/*===========================================================================
FUNCTION dec_hs_mimo_chan_send_log_submit_cmd

DESCRIPTION
  This function makes a request to post a local command to submit the 
  currently accumulated log packet to diag. Before it does so, it checks if 
  a previous log packet that was submitted to diag has been  serviced or not.
  If not, it drops the current packet and proceeds further.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

void dec_hs_mimo_chan_send_log_submit_cmd(
  /* HS decode status log buffer index to submit */
  uint8 hs_mimo_log_idx)
{
  /* function local variables */
  /* ------------------------ */

  /* log packet pointer that need to be submitted */
  HS_MIMO_CH_ANALYSIS_LOG_PKT_type *this_hs_mimo_chan_log_pkt;

  /* log header pointer */
  hsdpa_log_hdr_struct_type *hs_mimo_chan_log_pkt_hdr;

  /*** function code starts here ***/

  if (hs_mimo_log_idx >= HS_MIMO_LOG_NUM_BUF)
  {
    ERR_FATAL("Incorrect index for MIMO chan submit log %d", hs_mimo_log_idx, 0, 0);
  }

  /* Check if there is a pending request to submit previous log packet */
  if (hs_mimo_chan_log_submit)
  {
    /* Print error message and mark this buffer as available one */

      /* get log packet pointer */
      this_hs_mimo_chan_log_pkt = &(hs_mimo_chan_log_pkt_buf[hs_mimo_log_idx]);
      /* Get log packet header pointer and set log code to that */
      hs_mimo_chan_log_pkt_hdr = (hsdpa_log_hdr_struct_type*) this_hs_mimo_chan_log_pkt;
      /* submit the log packet */
      WL1_MSG3(ERROR, "HS_MIMO_CHAN_LOG:Failed idx %d info sz %d idx pending %d",
                      hs_mimo_log_idx,
                      hs_mimo_chan_log_pkt_hdr->len,
                      hs_mimo_chan_log_buf_idx_to_submit);

      hs_mimo_chan_log_buf_avail_status[hs_mimo_log_idx] = TRUE;
  }
  else
  {
    /* Save HS decode status log buffer index to submit later in task context */
    hs_mimo_chan_log_submit = TRUE;
    hs_mimo_chan_log_buf_idx_to_submit = hs_mimo_log_idx;

    /* Send local command to l1m */
    hs_send_submit_log_cmd();
  }
}

/*===========================================================================
FUNCTION dec_hs_mimo_chan_submit_log_buffer

DESCRIPTION
  This function is called from hscfg module in response of HS log submit
  command. It checks for MIMO chan log submit flag. 
  If it is set to TRUE, it submit log buffer pointed by log buffer index 
  to submit.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
void dec_hs_mimo_chan_submit_log_buffer(void)
{
  /* function local variables */
  /* ------------------------ */

  /* flag to hold the state of the log code in QXDM */
  boolean log_code_disabled = FALSE;

  /* log packet pointer that need to be submitted */
  HS_MIMO_CH_ANALYSIS_LOG_PKT_type *this_hs_mimo_chan_log_pkt;

  /* log header pointer */
  hsdpa_log_hdr_struct_type *hs_mimo_chan_log_pkt_hdr;
  /* buffer index to submit */
  uint8 log_info_buf_idx;

  /*** function code starts here ***/

  /* If log need to be submitted then proceed otherwise simply return from here */
  if (hs_mimo_chan_log_submit)
  {
    /* save log buffer index to submit */
    log_info_buf_idx = hs_mimo_chan_log_buf_idx_to_submit;
  }
  else
  {
    return;
  }

  if (log_info_buf_idx >= HS_MIMO_LOG_NUM_BUF)
  {
    ERR_FATAL("Incorrect index for MIMO chan submit buffer %d", log_info_buf_idx, 0, 0);
  }

  if (log_status(HS_MIMO_CH_ANALYSIS_LOG_PKT))
    {
      /* get log packet pointer */
      this_hs_mimo_chan_log_pkt = &(hs_mimo_chan_log_pkt_buf[log_info_buf_idx]);
      /* Get log packet header pointer and set log code to that */
      hs_mimo_chan_log_pkt_hdr = (hsdpa_log_hdr_struct_type*) this_hs_mimo_chan_log_pkt;
      hs_mimo_chan_log_pkt_hdr->code = HS_MIMO_CH_ANALYSIS_LOG_PKT;
        
      /* submit the log packet */
      if (!log_submit((PACKED void *) this_hs_mimo_chan_log_pkt))
      {
        WL1_MSG1(ERROR, "HS_MIMO_CAHN_LOG:Failed info sz %d",
                        hs_mimo_chan_log_pkt_hdr->len);
      }
    }
  else
    {
      /* Mark the flag to indicate that the log code is disabled */
      log_code_disabled = TRUE;
    }

  HS_LOG_INTLOCK();
  /* Mark this LLR status log buffer to available */
  hs_mimo_chan_log_buf_avail_status[log_info_buf_idx] = TRUE;
  /* reset log to submit flag */
  hs_mimo_chan_log_submit = FALSE;

  /* If the log code is disabled, clear all buffers and mark logging state
     as well as logging buffer index as invalid */
  if (log_code_disabled)
  {
    uint8 index_buf;
    
    /* Mark logging OFF by QXDM */
    hs_mimo_chan_logging_enabled_qxdm = FALSE;
    /* Mark the log buffer index as invalid */
    hs_mimo_chan_log_info_buf_idx = HS_INFO_TABLE_INVALID_VAL;    
    /* iterate over all log buffer status to mark them available */
    for (index_buf = 0; index_buf < 2; index_buf++)
    {
      hs_mimo_chan_log_buf_avail_status[index_buf] = TRUE;
    }
  }

  HS_LOG_INTFREE();
}

#endif



/*===========================================================================
FUNCTION hs_cm_dl_logging_init

DESCRIPTION
  This function initializes all the global variables that are needed for
  HS+CM Demod logging.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  All the global variables for HS+CM Demod logging are reset.
===========================================================================*/

void hs_cm_dl_logging_init(void)
{
  /* function local variables */
  /* ------------------------ */

  uint32 index;

  /*** function code starts here ***/

  /* For DL */
  hs_cm_log_dl_active = FALSE;

  /* Mark all the buffers as available */
  for(index = 0; index < HS_DEMOD_CTRL_NUM_BUF; index++)
  {
    hs_cm_log_dl_buf_avail_status[index] = TRUE;
  }
  hs_cm_log_dl_buf_idx = HS_INFO_TABLE_INVALID_VAL;
  hs_demod_ctrl_log_submit = FALSE;

  /* Mark the log status to FALSE */
  hs_cm_demod_logging_enabled = FALSE;
}

/*===========================================================================
FUNCTION hs_cm_ul_logging_init

DESCRIPTION
  This function initializes all the global variables that are needed for
  HS+CM Mod logging.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  All the global variables for HS+CM Mod logging are reset.
===========================================================================*/

void hs_cm_ul_logging_init(void)
{
  /* function local variables */
  /* ------------------------ */

  uint32 index;

  /*** function code starts here ***/

  /* For UL */
  hs_cm_log_ul_active = FALSE;

  /* Mark all the buffers as available */
  for(index = 0; index < HS_MOD_CTRL_NUM_BUF; index++)
  {
    hs_cm_log_ul_buf_avail_status[index] = TRUE;
  }
  hs_cm_log_ul_buf_idx = HS_INFO_TABLE_INVALID_VAL;
  hs_mod_ctrl_log_submit = FALSE;

  /* Mark the log status to FALSE */
  hs_cm_mod_logging_enabled = FALSE;
}

/*===========================================================================
FUNCTION hs_cm_dl_stop_logging

DESCRIPTION
  This function prepares the HS+CM Demod logging buffer to be submitted to diag
  It calls the function that posts a HS+CM Demod log submit local command and then
  clears the state of HS+CM Demod logging.
  
DEPENDENCIES
  hs_cm_log_dl_buf_idx should be valid while making a call to this
  function.

RETURN VALUE
  None

SIDE EFFECTS
  HS+CM Demod logging states cleared
===========================================================================*/

void hs_cm_dl_stop_logging(void)
{
  /* function local variables */
  /* ------------------------ */
  /* pointer to HS demof ctrl buffer to accumulate information */
  HS_DEMOD_CTRL_TABLE_LOG_PKT_type *this_hs_cm_demod_log;
  uint16 num_sub_frames;  /* variable storing num of sub frames already logged */

  /* HSDPA demod ctrl log header pointer */
  hsdpa_log_hdr_struct_type *hs_cm_log_demod_ctrl_pkt_hdr;

  /*** function code starts here ***/

  /* get pointer to current log buffer */
  this_hs_cm_demod_log =
    &(hs_cm_log_demod_ctrl[hs_cm_log_dl_buf_idx]);

  /* the num of subframe being logged now */
  num_sub_frames = this_hs_cm_demod_log->num_sub_frames;

  /* Get header to current log buffer and update total size of
     information accumulated */
  hs_cm_log_demod_ctrl_pkt_hdr =
    (hsdpa_log_hdr_struct_type*) this_hs_cm_demod_log;
  hs_cm_log_demod_ctrl_pkt_hdr->len = (uint16)(sizeof(HS_DEMOD_CTRL_TABLE_LOG_PKT_type) -
    ((HS_DEMOD_CTRL_TABLE_LOG_BUF_LEN_W32 - CEIL(num_sub_frames, 32)) * sizeof(uint32))); /*lint !e506 */

  /* send command to submit this demod ctrl log packet */
  hs_cm_dl_send_log_submit_cmd(hs_cm_log_dl_buf_idx);

  /* mark HS+CM Demod logging inactive */
  hs_cm_log_dl_active = FALSE;
  hs_cm_demod_logging_enabled = FALSE;

  /* if action was reconfig then change it to start. or
     if it was stop then it is al done, mark it NOOP */
  if (hs_cm_log_dl_action == HS_STAT_GEN_RESTART)
  {
    hs_cm_log_dl_action = HS_STAT_GEN_START;
  }
  else
  {
    hs_cm_log_dl_action = HS_STAT_GEN_NOOP;
  }
}

/*===========================================================================
FUNCTION hs_cm_dl_update_logging

DESCRIPTION
  This function is called periodically every frame. It logs the status of
  the subframes within the current frame with respect to whether the HS DL
  transmission has to be ignored or not. It maintains logging state of the
  log packet as well.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

void hs_cm_dl_update_logging(void)
{
  /* function local variables */
  /* ------------------------ */

  /* CFN for which to accumulate HS+CM DL information*/
  uint8 this_fr_cfn;
  /* pointer to HS demof ctrl buffer to accumulate information */
  HS_DEMOD_CTRL_TABLE_LOG_PKT_type *this_hs_cm_demod_log;
  /* Keeps the global subframe number of the subframe being logged */
  uint16 global_sub_fr_num;
  hs_dl_channel_cfg_db_struct_type const* hs_dl_channel_cfg_db_ptr = hs_get_dl_channel_cfg_db();
  /*** function code starts here ***/

  HS_LOG_INTLOCK();

  /* get CFN for the frame at which to accumulate HS+CM info */
  this_fr_cfn =  stmr_get_combiner_cfn(TLM_GET_RXTL_SRC()); 

  /* Get the starting global subframe number */
  global_sub_fr_num = (uint16)normalize((this_fr_cfn * 5) +
          hs_dl_channel_cfg_db_ptr->cfnx5_sub_fr_offset, HS_MAX_GLOBAL_SUB_FRAME_COUNT);

  /* Handle any pending START action */
  /* ------------------------------- */

  /* Check for HS+CM logging start */
  if ( /* HS+CM accumulation not already active */
       (!hs_cm_log_dl_active) &&
       /* Check for HS+CM action for start */
       (hs_cm_log_dl_action == HS_STAT_GEN_START) &&
       /* If CFN generated above for HS+CM matches start CFN */
       (this_fr_cfn == hs_cm_log_dl_start_cfn)
     )
  {
    /* Start HS demod control accumulation */
    hs_cm_log_dl_active = TRUE;
    hs_cm_demod_logging_enabled = FALSE;
  } /* end if, HS+CM logging not active */

  /* Check for HS Stat Stop action if QXDM is turned off */
  if ((!hs_cm_demod_logging_enabled) && (hs_cm_log_dl_active) &&
      ((hs_cm_log_dl_action == HS_STAT_GEN_RESTART) ||
       (hs_cm_log_dl_action == HS_STAT_GEN_STOP)))
  {
    /* Time to disable HS+CM Demod logging */
    if (hs_cm_log_dl_buf_idx != HS_INFO_TABLE_INVALID_VAL)
    {
      /* If some samples are already accumulated, flush them */
      hs_cm_dl_stop_logging ();
    }
    else
    {
      /* Reset HS+CM Demod Logging state */
      hs_cm_dl_logging_init();
    }
    WL1_MSG1(HIGH,"Stopped HS+CM Demod logging CFN %d", this_fr_cfn);
  }

  /* If the logging has been turned OFF, poll to check if it is enabled now */
  if ((!hs_cm_demod_logging_enabled) && (hs_cm_log_dl_active))
  {
    if (log_status(HS_DEMOD_CTRL_TABLE_LOG_PKT))
    {
      hs_cm_demod_logging_enabled = TRUE;
      hs_cm_log_dl_buf_idx = HS_INFO_TABLE_INVALID_VAL;
    }
  }

  /* Do logging */
  /* ----------------------------------- */

  if ((hs_cm_log_dl_active) && (hs_cm_demod_logging_enabled))
  {
    /* loop index to get stat for 5 sub frames in this frame */
    uint32 index_sub_fr;
    uint8 shadow_index = hs_dl_channel_cfg_db_ptr->carr_info[L1_HSDPA_CARRIER_0].hs_info_table_idx;

    /* This is possible only when logging is turned OFF and then turned ON from
       within QXDM. Allocate a new buffer to start log accumulation */
    if (hs_cm_log_dl_buf_idx == HS_INFO_TABLE_INVALID_VAL)
    {
      /* Allocate a new buffer to accumulate log samples */

      uint32 index_buf; /* loop indicies */

      /* Get available HS+CM status accumulation buffer */
      for (index_buf = 0;
           index_buf < HS_DEMOD_CTRL_NUM_BUF;
           index_buf++)
      {
        if (hs_cm_log_dl_buf_avail_status[index_buf])
        {
          hs_cm_log_dl_buf_avail_status[index_buf] = FALSE;
          hs_cm_log_dl_buf_idx = index_buf;
          break;
        }
      }
      /* If can't get buffer then there is no use of proceeding. It should
         never happen because of double buffering */
      if (hs_cm_log_dl_buf_idx >= HS_DEMOD_CTRL_NUM_BUF)
      {
        ERR_FATAL("Can't get HS CM DL status log", 0, 0, 0);
        return; /*lint !e527 */
      }

      /* get pointer to current log buffer */
      this_hs_cm_demod_log =
        &(hs_cm_log_demod_ctrl[hs_cm_log_dl_buf_idx]);
      /* Do time stamp corresponding to first sample */
      log_set_timestamp(this_hs_cm_demod_log);
      /* init number of samples accumulated to 0 */
      this_hs_cm_demod_log->num_sub_frames = 0;
      /* Save starting sub frame number */
      this_hs_cm_demod_log->starting_global_sub_fn = global_sub_fr_num;

      /* Mark HS+CM logging active to TRUE and action to NOOP */
      hs_cm_log_dl_active = TRUE;
      hs_cm_log_dl_action = HS_STAT_GEN_NOOP;

      WL1_MSG2(HIGH, "Starting HS demod ctrl CFN %d SubFn %d",
               hs_cm_log_dl_start_cfn,
                     hs_dl_channel_cfg_db_ptr->start_global_sub_fr_num);
    }

    /* Check if the buf idx is valid. */
    if (hs_cm_log_dl_buf_idx >= HS_DEMOD_CTRL_NUM_BUF)
    {
      ERR_FATAL("Invalid hs_cm_log_dl_buf_idx: %d", 
                hs_cm_log_dl_buf_idx, 0, 0);
      return; /*lint !e527 */
    }

    /* Iterate over 5 sub frames to generate info */
    for (index_sub_fr = 0;
         index_sub_fr < HS_NUM_SUB_FR_PER_RADIO_FR;
         index_sub_fr++)
    {
      uint16 num_sub_frames;  /* variable storing num of sub frames already logged */
      uint8 log_offset;       /* the offset into the log buffer */
      uint8 bit_position;     /* bit position at the offset buffer */
      uint8 shadow_offset;    /* offset into the shadow from where to read info */
      uint8 value;            /* value to be written */
      uint32 index_buf;        /* loop index */

      /* Log the data here */
      this_hs_cm_demod_log =
        &(hs_cm_log_demod_ctrl[hs_cm_log_dl_buf_idx]);

      /* the num of subframe being logged now */
      num_sub_frames = this_hs_cm_demod_log->num_sub_frames;
      /* offset into the log buffer */
      log_offset = (uint8)(num_sub_frames >> 5);
      /* bit position to write to in the buffer offset by log_offset */
      bit_position = (uint8)(num_sub_frames - ((num_sub_frames >> 5) << 5));

      /* The offset into the shadow */
      shadow_offset = global_sub_fr_num % HS_CM_CTRL_CFG_PARAM_DB_LEN_W16;
      /* value to be written */
      value = mcalwcdma_fwsw_intf_addr_ptr->hsCmCtrlTable[shadow_index].ctrl[shadow_offset].mask.demodEn;
      /* write the value into the log buffer */
      this_hs_cm_demod_log->demod_ctrl_table_info[log_offset] &= ~((uint32)1 << bit_position);
      this_hs_cm_demod_log->demod_ctrl_table_info[log_offset] |= (value << bit_position);

      /* Increment number of samples accumulated */
      this_hs_cm_demod_log->num_sub_frames++;

      /* Handle RESTART and STOP actions */
      /* ------------------------------- */

      /* check if there action type RECONFIG and STOP set */
      if ((hs_cm_log_dl_action == HS_STAT_GEN_RESTART) ||
          (hs_cm_log_dl_action == HS_STAT_GEN_STOP))
      {
        /* Check if this the sub frame to STOP as the part of action STOP
           or RESTART */
        if (global_sub_fr_num == hs_cm_log_dl_stop_sub_fr_num)
        {
          /* STOP this accumulation now */

          hs_cm_dl_stop_logging ();

          WL1_MSG2(HIGH, "Stopping HS demod ctrl CFN %d SubFn %d",
                         this_fr_cfn, global_sub_fr_num);

          /* no need to further get sub frames */
          break;
        } /* End if, sub frame to stop accumulation has occured */
      } /* end if, action RESTART or STOP pending */

      /* Go to next sub frame */
      global_sub_fr_num = (uint16)
        normalize(global_sub_fr_num + 1, HS_MAX_GLOBAL_SUB_FRAME_COUNT);

      /* If acumulation has reached max samples then this must be submitted */
      /* ------------------------------------------------------------------ */

      if (this_hs_cm_demod_log->num_sub_frames == HS_DEMOD_CTRL_TABLE_LOG_MAX_SUB_FR)
      {
        /* HSDPA demod ctrl log header pointer */
        hsdpa_log_hdr_struct_type *hs_cm_log_demod_ctrl_pkt_hdr;

        /* Get header to current log buffer and update total size of
           information accumulated */
        hs_cm_log_demod_ctrl_pkt_hdr = (hsdpa_log_hdr_struct_type*) this_hs_cm_demod_log;
        hs_cm_log_demod_ctrl_pkt_hdr->len = (uint16)(sizeof(HS_DEMOD_CTRL_TABLE_LOG_PKT_type) -
            ((HS_DEMOD_CTRL_TABLE_LOG_BUF_LEN_W32 - CEIL(num_sub_frames, 32)) * sizeof(uint32))); /*lint !e506 */

        /* submit this log packet. */
        hs_cm_dl_send_log_submit_cmd(hs_cm_log_dl_buf_idx);

        /* Get available HS status accumulation buffer */
        hs_cm_log_dl_buf_idx = HS_INFO_TABLE_INVALID_VAL;
        for (index_buf = 0;
             index_buf < HS_DEMOD_CTRL_NUM_BUF;
             index_buf++)
        {
          if (hs_cm_log_dl_buf_avail_status[index_buf])
          {
            hs_cm_log_dl_buf_avail_status[index_buf] = FALSE;
            hs_cm_log_dl_buf_idx = index_buf;
            break;
          }
        }
        /* If can't get buffer then there is no use of proceeding. It should
           never happen because of double buffering */
        if (hs_cm_log_dl_buf_idx == HS_INFO_TABLE_INVALID_VAL)
        {
          ERR_FATAL("Can't get HS CM DL log", 0, 0, 0);
          return; /*lint !e527 */
        }

        /* Get pointer to this log and init number of samples and starting
           sub-frame number */
        this_hs_cm_demod_log =
          &(hs_cm_log_demod_ctrl[hs_cm_log_dl_buf_idx]);

        this_hs_cm_demod_log->num_sub_frames = 0;
        this_hs_cm_demod_log->starting_global_sub_fn = global_sub_fr_num;

        /* Update time stamp corresponding to first sample */
        log_set_timestamp(this_hs_cm_demod_log);
      } /* end if, max demod ctrl samples have been collected */
    } /* end for, accumulate demod ctrl for 5 sub frames */
  } /* end if, HS+CM DL logging is active */

  HS_LOG_INTFREE();
}

/*===========================================================================
FUNCTION hs_cm_dl_send_log_submit_cmd

DESCRIPTION
  This function is called to post HS downlink CM control log submit command.
  This function checks if a previous log is pending or not. If pending, then
  the current buffer to log is discarded. If no DL CM control log is pending,
  then it calls function to issue a L1 local command to submit the log.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  If a log buffer pending to be submitted, the current buffer is discarded.
===========================================================================*/

void hs_cm_dl_send_log_submit_cmd(
  /* index of the log buffer to be submitted */
  uint8 buffer_idx)
{
  /* function local variables */
  /* ------------------------ */

  /* demod ctrl log packet pointer that need to be submitted */
  HS_DEMOD_CTRL_TABLE_LOG_PKT_type *this_hs_demod_ctrl_log;
  /* demod ctrl log packet header pointer */
  hsdpa_log_hdr_struct_type *hs_demod_ctrl_log_pkt_hdr;

  /*** function code starts here ***/

  /* Check if there is a pending request to submit previous log packet */
  if (hs_demod_ctrl_log_submit)
  {
    /* Print error message and mark this buffer as available one */

    /* get log packet pointer */
    this_hs_demod_ctrl_log =
      &(hs_cm_log_demod_ctrl[buffer_idx]);
    /* Get log packet header pointer and set log code to that */
    hs_demod_ctrl_log_pkt_hdr = (hsdpa_log_hdr_struct_type*) this_hs_demod_ctrl_log;

    WL1_MSG3(ERROR, "HS_DEMOD_CTRL_LOG:Failed idx %d info sz %d idx %d pending",
                    buffer_idx,
                    hs_demod_ctrl_log_pkt_hdr->len,
              hs_demod_ctrl_log_buf_idx_to_submit);

    /* Mark this demod ctrl log buffer to available */
    hs_cm_log_dl_buf_avail_status[buffer_idx] = TRUE;
  }
  else
  {
    /* Save HS+CM demod ctrl log buffer index to submit later in task context */
    hs_demod_ctrl_log_submit = TRUE;
    hs_demod_ctrl_log_buf_idx_to_submit = buffer_idx;

    /* Send local command to l1m */
    hs_send_submit_log_cmd();
  }
}

/*===========================================================================
FUNCTION hs_cm_demod_ctrl_submit_log_buffer

DESCRIPTION
  This function is submits the HS+CM DL control log buffer. After successful
  submission, it releases the resources held up by the log submit operation.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  The log buffer availability status as well as the submit pending flags are
  cleared upon successful log submit.
===========================================================================*/

void hs_cm_demod_ctrl_submit_log_buffer(void)
{
  /* function local variables */
  /* ------------------------ */

  /* flag to hold the state of the log code in QXDM */
  boolean log_code_disabled = FALSE;

  /* demod ctrl log packet pointer that need to be submitted */
  HS_DEMOD_CTRL_TABLE_LOG_PKT_type *this_hs_demod_ctrl_log;
  /* demod ctrl log packet header pointer */
  hsdpa_log_hdr_struct_type *hs_demod_ctrl_log_pkt_hdr;
  /* demod ctrl log buffer index to submit */
  uint8 demod_ctrl_buf_idx;

  /*** function code starts here ***/

  /* If HS demod ctrl log status need to be submitted then proceed otherwise
     simply return from here */
  if (hs_demod_ctrl_log_submit)
  {
    /* save demod ctrl log buffer index to submit */
    demod_ctrl_buf_idx = hs_demod_ctrl_log_buf_idx_to_submit;
  }
  else
  {
    return;
  }

  if (log_status(HS_DEMOD_CTRL_TABLE_LOG_PKT))
  {
    /* get log packet pointer */
    this_hs_demod_ctrl_log =
      &(hs_cm_log_demod_ctrl[demod_ctrl_buf_idx]);
    /* Get log packet header pointer and set log code to that */
    hs_demod_ctrl_log_pkt_hdr = (hsdpa_log_hdr_struct_type*) this_hs_demod_ctrl_log;
    hs_demod_ctrl_log_pkt_hdr->code = HS_DEMOD_CTRL_TABLE_LOG_PKT;
    /* submit the log packet */
    if (!log_submit((PACKED void *) this_hs_demod_ctrl_log))
    {
      WL1_MSG1(ERROR, "HS_DEMOD_CTRL_LOG:Failed info sz %d",
                      hs_demod_ctrl_log_pkt_hdr->len);
    }
  }
  else
  {
    /* Mark the flag to indicate that the log code is disabled */
    log_code_disabled = TRUE;
  }

  HS_LOG_INTLOCK();
  /* Mark this demod ctrl log buffer to available */
  hs_cm_log_dl_buf_avail_status[demod_ctrl_buf_idx] = TRUE;
  /* reset HS+CM demod ctrl log to submit flag */
  hs_demod_ctrl_log_submit = FALSE;

  /* If the log code is disabled, clear all buffers and mark logging state
     as well as logging buffer index as invalid */
  if (log_code_disabled)
  {
    uint32 index_buf;

    /* Mark logging OFF by QXDM */
    hs_cm_demod_logging_enabled = FALSE;
    /* Mark the log buffer index as invalid */
    hs_cm_log_dl_buf_idx = HS_INFO_TABLE_INVALID_VAL;
    /* iterate over all log buffer status to mark them available */
    for (index_buf = 0; index_buf < HS_DEMOD_CTRL_NUM_BUF; index_buf++)
    {
      hs_cm_log_dl_buf_avail_status[index_buf] = TRUE;
    }
  }
  HS_LOG_INTFREE();
}

/*===========================================================================
FUNCTION hs_cm_ul_stop_logging

DESCRIPTION
  This function prepares the HS+CM Mod logging buffer to be submitted to diag
  It calls the function that posts a HS+CM Mod log submit local command and then
  clears the state of HS+CM Mod logging.
  
DEPENDENCIES
  hs_cm_log_ul_buf_idx must be valid while making a call to this
  function

RETURN VALUE
  None

SIDE EFFECTS
  HS+CM Mod logging states cleared
===========================================================================*/

void hs_cm_ul_stop_logging(void)
{
  /* function local variables */
  /* ------------------------ */
  uint16 num_sub_frames;  /* variable storing num of sub frames already logged */
  uint8 log_offset;       /* the offset into the log buffer */

  /* pointer to HS mod ctrl buffer to accumulate information */
  HS_MOD_CTRL_TABLE_LOG_PKT_type *this_hs_cm_mod_log;

  /* HSDPA Mod ctrl log header pointer */
  hsdpa_log_hdr_struct_type *hs_cm_log_mod_ctrl_pkt_hdr;

  /*** function code starts here ***/

  /* Log the data here */
  this_hs_cm_mod_log =
    &(hs_cm_log_mod_ctrl[hs_cm_log_ul_buf_idx]);
  num_sub_frames = this_hs_cm_mod_log->num_sub_frames;
  /* which word to write to in the log packet for this subframe */
  log_offset = (uint8)(num_sub_frames / HS_MOD_CTRL_TABLE_LOG_NUM_SUB_FR_IN_W32);

  /* Get header to current log buffer and update total size of
     information accumulated */
  hs_cm_log_mod_ctrl_pkt_hdr =
    (hsdpa_log_hdr_struct_type*) this_hs_cm_mod_log;
  hs_cm_log_mod_ctrl_pkt_hdr->len = (uint16)(sizeof(HS_MOD_CTRL_TABLE_LOG_PKT_type) -
    ((HS_MOD_CTRL_TABLE_LOG_BUF_LEN_W32 - (log_offset + 1)) * sizeof(uint32))); /*lint !e506 */

  /* send command to submit this demod ctrl log packet */
  hs_cm_ul_send_log_submit_cmd(hs_cm_log_ul_buf_idx);

  /* mark HS+CM Mod logging inactive */
  hs_cm_log_ul_active = FALSE;
  hs_cm_mod_logging_enabled = FALSE;

  /* if action was reconfig then change it to start. or
     if it was stop then it is al done, mark it NOOP */
  if (hs_cm_log_ul_action == ENCHS_LOG_RESTART)
  {
    hs_cm_log_ul_action = ENCHS_LOG_START;
  }
  else
  {
    hs_cm_log_ul_action = ENCHS_LOG_NOOP;
  }
}

/*===========================================================================
FUNCTION hs_cm_ul_update_logging

DESCRIPTION
  This function is called periodically every frame. It logs the status of
  the subframes within the current frame with respect to whether the HS UL
  transmission has to be ignored or not. It maintains logging state of the
  log packet as well.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

void hs_cm_ul_update_logging(void)
{
  /* function local variables */
  /* ------------------------ */

  /* CFN for which to accumulate mod ctrl status */
  uint8 this_fr_cfn;
  /* pointer to HS mod ctrl buffer to accumulate information */
  HS_MOD_CTRL_TABLE_LOG_PKT_type *this_hs_cm_mod_log;
  /* Keeps the global subframe number of the subframe being logged */
  uint16 global_sub_fr_num;
  hs_ul_channel_cfg_db_struct_type const * hs_ul_cfg_db_ptr = hs_get_ul_channel_cfg_db();
  /*** function code starts here ***/

  HS_LOG_INTLOCK();

  /* get CFN for the frame at which to accumulate mod ctrl info */
  this_fr_cfn = stmr_get_combiner_cfn(TLM_GET_RXTL_SRC());

  /* get the start global subframe number */
  global_sub_fr_num = (uint16)normalize((this_fr_cfn * 5) +
          hs_ul_cfg_db_ptr->cfnx5_sub_fr_offset, HS_MAX_GLOBAL_SUB_FRAME_COUNT);

  /* Handle any pending START action */
  /* ------------------------------- */

  /* Check for HS+CM UL accumulation start condition */
  if ( /* HS+CM UL log generation not already active */
       (!hs_cm_log_ul_active) &&
       /* Check for HS+CM UL generation action for start */
       (hs_cm_log_ul_action == ENCHS_LOG_START) &&
       /* If CFN generated above for accumulation matches start CFN */
       (this_fr_cfn == hs_cm_log_ul_start_cfn)
     )
  {
    /* Start mod ctrl accumulation and stat generation */
    hs_cm_log_ul_active = TRUE;
    hs_cm_mod_logging_enabled = FALSE;
  } /* end if, log generation not active */

  /* Check for HS Stat Stop action if QXDM is turned off */
  if ((!hs_cm_mod_logging_enabled) && (hs_cm_log_ul_active) &&
      ((hs_cm_log_ul_action == ENCHS_LOG_RESTART) ||
       (hs_cm_log_ul_action == ENCHS_LOG_STOP)))
  {
    /* Time to disable HS+CM Mod logging */
    if (hs_cm_log_ul_buf_idx != HS_INFO_TABLE_INVALID_VAL)
    {
      /* If some samples are already accumulated, flush them */
      hs_cm_ul_stop_logging ();
    }
    else
    {
      /* Reset HS+CM Mod Logging state */
      hs_cm_ul_logging_init();
     
    }
    WL1_MSG1(HIGH , "Stopping HS+CM Mod logging CFN %d", this_fr_cfn);
  }

  /* If the logging has been turned OFF, poll to check if it is enabled now */
  if ((!hs_cm_mod_logging_enabled) && (hs_cm_log_ul_active))
  {
    if (log_status(HS_MOD_CTRL_TABLE_LOG_PKT))
    {
      hs_cm_mod_logging_enabled = TRUE;
      hs_cm_log_ul_buf_idx = HS_INFO_TABLE_INVALID_VAL;
    }
  }

  /* Do logging */
  /* ----------------------------------- */

  if ((hs_cm_log_ul_active) && (hs_cm_mod_logging_enabled))
  {
    /* loop index to get stat for 5 sub frames in this frame */
    uint32 index_sub_fr;
    uint8 shadow_index = hs_cur_ul_info_idx;

    if (hs_cm_log_ul_buf_idx == HS_INFO_TABLE_INVALID_VAL)
    {
      /* Allocate a new buffer to accumulate log samples */
      uint32 index_buf; /* loop indicies */

      /* Get available HS+CM UL status accumulation buffer */
      hs_cm_log_ul_buf_idx = HS_INFO_TABLE_INVALID_VAL;
      for (index_buf = 0;
           index_buf < HS_MOD_CTRL_NUM_BUF;
           index_buf++)
      {
        if (hs_cm_log_ul_buf_avail_status[index_buf])
        {
          hs_cm_log_ul_buf_avail_status[index_buf] = FALSE;
          hs_cm_log_ul_buf_idx = index_buf;
          break;
        }
      }
      /* If can't get buffer then there is no use of proceeding. It should
         never happen because of double buffering */
      if (hs_cm_log_ul_buf_idx >= HS_MOD_CTRL_NUM_BUF)
      {
        ERR_FATAL("Can't get HS CM UL status log", 0, 0, 0);
        return; /*lint !e527 */
      }

      /* get pointer to current log buffer */
      this_hs_cm_mod_log =
        &(hs_cm_log_mod_ctrl[hs_cm_log_ul_buf_idx]);
      /* Do time stamp corresponding to first sample */
      log_set_timestamp(this_hs_cm_mod_log);
      /* init number of samples accumulated to 0 */
      this_hs_cm_mod_log->num_sub_frames = 0;
      /* Save starting sub frame number */
      this_hs_cm_mod_log->starting_global_sub_fn = global_sub_fr_num;

      /* Mark mod ctrl accumulation to TRUE and reset action to NOOP */
      hs_cm_log_ul_active = TRUE;
      hs_cm_log_ul_action = ENCHS_LOG_NOOP;

      WL1_MSG2(HIGH, "Starting HS mod ctrl CFN %d SubFn %d",
               hs_cm_log_ul_start_cfn,
                     hs_ul_cfg_db_ptr->start_global_sub_fr_num);
    }

    /* Check if the buf idx is valid. */
    if (hs_cm_log_ul_buf_idx >= HS_MOD_CTRL_NUM_BUF)
    {
      ERR_FATAL("Invalid hs_cm_log_ul_buf_idx: %d", 
                hs_cm_log_ul_buf_idx, 0, 0);
      return; /*lint !e527 */
    }

    /* Iterate over 5 sub frames to generate stats */
    for (index_sub_fr = 0;
         index_sub_fr < HS_NUM_SUB_FR_PER_RADIO_FR;
         index_sub_fr++)
    {

      uint16 num_sub_frames;  /* variable storing num of sub frames already logged */
      uint8 log_offset;       /* the offset into the log buffer */
      uint8 bit_position;     /* bit position at the offset buffer */
      uint8 shadow_offset;    /* offset into the shadow from where to read info */
      uint8 value;            /* value to be written */
      uint32 index_buf;        /* loop index */

      /* Log the data here */
     this_hs_cm_mod_log =
        &(hs_cm_log_mod_ctrl[hs_cm_log_ul_buf_idx]);

      num_sub_frames = this_hs_cm_mod_log->num_sub_frames;
      /* which word to write to in the log packet for this subframe */
      log_offset = (uint8)(num_sub_frames / HS_MOD_CTRL_TABLE_LOG_NUM_SUB_FR_IN_W32);
      /* within the word, which bit position to write to */
      bit_position = (num_sub_frames % HS_MOD_CTRL_TABLE_LOG_NUM_SUB_FR_IN_W32) * 3;

      /* get the index in the shadow where info about this subframe is stored */
      shadow_offset = global_sub_fr_num % HS_CM_CTRL_CFG_PARAM_DB_LEN_W16;
      /* this value has to be written */
      value = (mcalwcdma_fwsw_intf_addr_ptr->hsCmCtrlTable[shadow_index].ctrl[shadow_offset].cmGapBmsk & 0xE) 
                >> HS_CM_CFG_DB_DL_BMSK;

      /* Write to appropriate position */
      this_hs_cm_mod_log->mod_ctrl_table_info[log_offset] &=
          ~((uint32)HS_CM_CFG_DB_UL_CQI_SHFTED_BMASK << bit_position);
      this_hs_cm_mod_log->mod_ctrl_table_info[log_offset] |= (value << bit_position);

      /* Increment number of samples accumulated */
      this_hs_cm_mod_log->num_sub_frames++;

      /* Handle RESTART and STOP actions */
      /* ------------------------------- */

      /* check if there action type RECONFIG and STOP set */
      if ((hs_cm_log_ul_action == ENCHS_LOG_RESTART) ||
          (hs_cm_log_ul_action == ENCHS_LOG_STOP))
      {
        /* Check if this the sub frame to STOP as the part of action STOP
           or RESTART */
        if (global_sub_fr_num == hs_cm_log_ul_stop_sub_fr_num)
        {
          /* STOP this accumulation now */

          hs_cm_ul_stop_logging ();

          WL1_MSG2(HIGH, "Stopping HS mod ctrl CFN %d SubFn %d",
                         this_fr_cfn, global_sub_fr_num);

          /* no need to further get sub frames */
          break;
        } /* End if, sub frame to stop accumulation has occured */
      } /* end if, action RESTART or STOP pending */

      /* Go to next sub frame */
      global_sub_fr_num = (uint16)
        normalize(global_sub_fr_num + 1, HS_MAX_GLOBAL_SUB_FRAME_COUNT);

      /* If acumulation has reached max samples then this must be submitted */
      /* ------------------------------------------------------------------ */

      if (this_hs_cm_mod_log->num_sub_frames == HS_MOD_CTRL_TABLE_LOG_MAX_SUB_FR)
      {
        /* HSDPA mod ctrl log header pointer */
        hsdpa_log_hdr_struct_type *hs_cm_log_mod_ctrl_pkt_hdr;

        /* Get header to current log buffer and update total size of
           information accumulated */
        hs_cm_log_mod_ctrl_pkt_hdr = (hsdpa_log_hdr_struct_type*) this_hs_cm_mod_log;
        hs_cm_log_mod_ctrl_pkt_hdr->len = (uint16)(sizeof(HS_MOD_CTRL_TABLE_LOG_PKT_type) -
            ((HS_MOD_CTRL_TABLE_LOG_BUF_LEN_W32 - (log_offset + 1)) * sizeof(uint32)));  /*lint !e506 */

        /* submit this log packet. */
        hs_cm_ul_send_log_submit_cmd(hs_cm_log_ul_buf_idx);

        /* Get available HS+CM UL log accumulation buffer */
        hs_cm_log_ul_buf_idx = HS_INFO_TABLE_INVALID_VAL;
        for (index_buf = 0;
             index_buf < HS_MOD_CTRL_NUM_BUF;
             index_buf++)
        {
          if (hs_cm_log_ul_buf_avail_status[index_buf])
          {
            hs_cm_log_ul_buf_avail_status[index_buf] = FALSE;
            hs_cm_log_ul_buf_idx = index_buf;
            break;
          }
        }
        /* If can't get buffer then there is no use of proceeding. It should
           never happen because of double buffering */
        if (hs_cm_log_ul_buf_idx == HS_INFO_TABLE_INVALID_VAL)
        {
          ERR_FATAL("Can't get HS CM UL log", 0, 0, 0);
          return; /*lint !e527 */
        }

        /* Get pointer to this log and init number of samples and starting
           sub-frame number */
        this_hs_cm_mod_log =
          &(hs_cm_log_mod_ctrl[hs_cm_log_ul_buf_idx]);

        this_hs_cm_mod_log->num_sub_frames = 0;
        this_hs_cm_mod_log->starting_global_sub_fn = global_sub_fr_num;

        /* Update time stamp corresponding to first sample */
        log_set_timestamp(this_hs_cm_mod_log);
      } /* end if, max mod ctrl samples have been collected */
    } /* end for, accumulate mod ctrl state for 5 sub frames */
  } /* end if, HS mod ctrl logging state is active */

  HS_LOG_INTFREE();
}

/*===========================================================================
FUNCTION hs_cm_ul_send_log_submit_cmd

DESCRIPTION
  This function is called to post HS uplink CM control log submit command.
  This function checks if a previous log is pending or not. If pending, then
  the current buffer to log is discarded. If no UL CM control log is pending,
  then it calls function to issue a L1 local command to submit the log.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  If a log buffer pending to be submitted, the current buffer is discarded.
===========================================================================*/

void hs_cm_ul_send_log_submit_cmd(
  /* index of the log buffer to be submitted */
  uint8 buffer_idx)
{
  /* function local variables */
  /* ------------------------ */

  /* mod ctrl log packet pointer that need to be submitted */
  HS_MOD_CTRL_TABLE_LOG_PKT_type *this_hs_mod_ctrl_log;
  /* mod ctrl log packet header pointer */
  hsdpa_log_hdr_struct_type *hs_mod_ctrl_log_pkt_hdr;

  /*** function code starts here ***/

  /* Check if there is a pending request to submit previous log packet */
  if (hs_mod_ctrl_log_submit)
  {
    /* Print error message and mark this buffer as available one */

    /* get log packet pointer */
    this_hs_mod_ctrl_log =
      &(hs_cm_log_mod_ctrl[buffer_idx]);
    /* Get log packet header pointer and set log code to that */
    hs_mod_ctrl_log_pkt_hdr = (hsdpa_log_hdr_struct_type*) this_hs_mod_ctrl_log;

    WL1_MSG3(ERROR, "HS_MOD_CTRL_LOG:Failed idx %d info sz %d idx %d pending",
                    buffer_idx,
                    hs_mod_ctrl_log_pkt_hdr->len,
              hs_mod_ctrl_log_buf_idx_to_submit);

    /* Mark this mod ctrl log buffer to available */
    hs_cm_log_ul_buf_avail_status[buffer_idx] = TRUE;
  }
  else
  {
    /* Save HS mod ctrl log buffer index to submit later in task context */
    hs_mod_ctrl_log_submit = TRUE;
    hs_mod_ctrl_log_buf_idx_to_submit = buffer_idx;

    /* Send local command to l1m */
    hs_send_submit_log_cmd();
  }
}

/*===========================================================================
FUNCTION hs_cm_mod_ctrl_submit_log_buffer

DESCRIPTION
  This function is submits the HS+CM UL control log buffer. After successful
  submission, it releases the resources held up by the log submit operation.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  The log buffer availability status as well as the submit pending flags are
  cleared upon successful log submit.
===========================================================================*/

void hs_cm_mod_ctrl_submit_log_buffer(void)
{
  /* function local variables */
  /* ------------------------ */

  /* flag to hold the state of the log code in QXDM */
  boolean log_code_disabled = FALSE;

  /* mod ctrl log packet pointer that need to be submitted */
  HS_MOD_CTRL_TABLE_LOG_PKT_type *this_hs_mod_ctrl_log;
  /* mod ctrl log packet header pointer */
  hsdpa_log_hdr_struct_type *hs_mod_ctrl_log_pkt_hdr;
  /* mod ctrl log buffer index to submit */
  uint8 mod_ctrl_buf_idx;

  /*** function code starts here ***/

  /* If HS mod ctrl log status need to be submitted then proceed otherwise
     simply return from here */
  if (hs_mod_ctrl_log_submit)
  {
    /* save mod ctrl log buffer index to submit */
    mod_ctrl_buf_idx = hs_mod_ctrl_log_buf_idx_to_submit;
  }
  else
  {
    return;
  }

  if (log_status(HS_MOD_CTRL_TABLE_LOG_PKT))
  {
    /* get log packet pointer */
    this_hs_mod_ctrl_log =
      &(hs_cm_log_mod_ctrl[mod_ctrl_buf_idx]);
    /* Get log packet header pointer and set log code to that */
    hs_mod_ctrl_log_pkt_hdr = (hsdpa_log_hdr_struct_type*) this_hs_mod_ctrl_log;
    hs_mod_ctrl_log_pkt_hdr->code = HS_MOD_CTRL_TABLE_LOG_PKT;
    /* submit the log packet */
    if (!log_submit((PACKED void *) this_hs_mod_ctrl_log))
    {
      WL1_MSG1(ERROR, "HS_DEMOD_CTRL_LOG:Failed info sz %d",
                      hs_mod_ctrl_log_pkt_hdr->len);
    }
  }
  else
  {
    /* Mark the flag to indicate that the log code is disabled */
    log_code_disabled = TRUE;
  }

  HS_LOG_INTLOCK();
  /* Mark this mod ctrl log buffer to available */
  hs_cm_log_ul_buf_avail_status[mod_ctrl_buf_idx] = TRUE;
  /* reset HS mod ctrl log to submit flag */
  hs_mod_ctrl_log_submit = FALSE;

  /* If the log code is disabled, clear all buffers and mark logging state
     as well as logging buffer index as invalid */
  if (log_code_disabled)
  {
    uint32 index_buf;

    /* Mark logging OFF by QXDM */
    hs_cm_mod_logging_enabled = FALSE;
    /* Mark the log buffer index as invalid */
    hs_cm_log_ul_buf_idx = HS_INFO_TABLE_INVALID_VAL;
    /* iterate over all log buffer status to mark them available */
    for (index_buf = 0; index_buf < HS_MOD_CTRL_NUM_BUF; index_buf++)
    {
      hs_cm_log_ul_buf_avail_status[index_buf] = TRUE;
    }
  }
  HS_LOG_INTFREE();
}


