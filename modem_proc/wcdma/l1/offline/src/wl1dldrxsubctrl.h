#ifndef WL1DLDRXSUBCTRL_H
#define WL1DLDRXSUBCTRL_H

/*===========================================================================
                 WCDMA L1 HS SUB CTRL

GENERAL DESCRIPTION
  This file contains the code for implementing the HS state machine under EDRX

EXTERNALIZED FUNCTIONS

INTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS


Copyright (c)  2011 by Qualcomm Technologies Incorporated.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/wcdma/l1/offline/src/wl1dldrxsubctrl.h#1 $
$DateTime: 2015/01/27 06:42:19 $
$Author: mplp4svc $


when       who    what, where, why
--------   ---    ---------------------------------------------------------
05/15/12   hk     Added API to get EDRX configured status
03/27/12   hk     Added support for FEATURE_WCDMA_HS_FACH_DRX
09/23/11   nd     wrapped inside FEATURE_WCDMA_HS_FACH_DRX.
04/11/11   nd     Initial revision.

===========================================================================*/

/* -----------------------------------------------------------------------
** Include Files
** ----------------------------------------------------------------------- */
#include "wcdma_variation.h"
#include "l1rrcif.h"

#ifdef FEATURE_WCDMA_HS_FACH_DRX
/**************************************************************/
/****  EDRX CHANNEL ENTITIES       **********************************/
/**************************************************************/

typedef enum
{
  
  WL1_EDRX_ENTITY_SRCH,
  WL1_EDRX_ENTITY_AICH,
  WL1_EDRX_ENTITY_HS,
  WL1_EDRX_ENTITY_PRACH,
  WL1_EDRX_ENTITY_PCCPCH_N,
  WL1_EDRX_ENTITY_MAX  
}wl1_edrx_entity_enum_type;


/* Logical entity (or channel) states.  These states define the logical entity
** state machine.  One state machine runs for each activated logical entity. */
typedef enum
{
  EDRX_ENTITY_INACTIVE,
  EDRX_ENTITY_ACTIVE_PENDING,
  EDRX_ENTITY_ACTIVE,
  EDRX_ENTITY_SLEEP_PENDING,
  EDRX_ENTITY_ASLEEP,
  EDRX_ENTITY_NUM_STATES
} wl1_edrx_entity_state_enum_type;


typedef enum
{
  WL1_EDRX_ACTION_NONE,
  WL1_EDRX_ACTION_REGISTER,
  WL1_EDRX_ACTION_DEREGISTER,
  WL1_EDRX_ACTION_WAKEUP,
  WL1_EDRX_ACTION_ENTITY_WAKEUP_RESPONSE,
  WL1_EDRX_ACTION_SLEEP,
  WL1_EDRX_ACTION_ENTITY_SLEEP_RESPONSE,
  WL1_EDRX_ACTION_SUSPEND,
  WL1_EDRX_ACTION_RESUME,
  WL1_EDRX_ACTION_MAX
}wl1_edrx_entity_action_enum_type;


/** function pointer */
typedef void (*WL1_EDRX_CONFIG_DONE_CB)(boolean);

/** function pointer */
typedef void (*WL1_EDRX_ENTITY_CHANNEL_HANDLER)(void);

/* EDRX SUB CTRL */
typedef struct
{
  uint16 drx_ctrl_id;
  wl1_edrx_entity_enum_type      type;  
  boolean          time_critical;
  WL1_EDRX_ENTITY_CHANNEL_HANDLER  resume_cb;
  WL1_EDRX_ENTITY_CHANNEL_HANDLER  suspend_cb;
  wl1_edrx_entity_state_enum_type     state;  
}wl1_edrx_entity_input_info_struct_type;

extern boolean wl1_edrx_pccpch_active;
extern boolean wl1_edrx_abort_ext_msg;

/*===========================================================================

FUNCTION       WL1_EDRX_INIT

DESCRIPTION     This function initialises EDRX state machine and other parameters

DEPENDENCIES    None

RETURN VALUE    None

SIDE EFFECTS    None

===========================================================================*/
extern void wl1_edrx_init(void);

/*===========================================================================

FUNCTION       WL1_EDRX_REGISTER_ENTITY

DESCRIPTION     This function initialises EDRX state machine and other parameters

DEPENDENCIES    None

RETURN VALUE    None

SIDE EFFECTS    None

===========================================================================*/
extern void wl1_edrx_register_entity
(
 wl1_edrx_entity_input_info_struct_type *input_info,
 WL1_EDRX_CONFIG_DONE_CB reg_done_client_cb
);

/*===========================================================================

FUNCTION       WL1_EDRX_DEREGISTER_ENTITY

DESCRIPTION     This function is used by the clients of EDRX to deregister itself

DEPENDENCIES    None

RETURN VALUE    None

SIDE EFFECTS    None

===========================================================================*/
extern void wl1_edrx_deregister_entity
(
  wl1_edrx_entity_enum_type entity_type,
  WL1_EDRX_CONFIG_DONE_CB dereg_done_client_cb
);

/*===========================================================================

FUNCTION        wl1_hsdrx_entity_init

DESCRIPTION     This function initialises the hs drx entity database

DEPENDENCIES    None

RETURN VALUE    None

SIDE EFFECTS    None

===========================================================================*/
extern boolean wl1_edrx_query_drx_active(void);

/*===========================================================================

FUNCTION        wl1_hsdrx_entity_init

DESCRIPTION     This function initialises the hs drx entity database

DEPENDENCIES    None

RETURN VALUE    None

SIDE EFFECTS    None

===========================================================================*/
extern boolean wl1_edrx_query_entity_active(uint8 drxctrl_id);

/*===========================================================================

FUNCTION       WL1_EDRX_WAKEUP

DESCRIPTION     This function initialises EDRX state machine and other parameters

DEPENDENCIES    None

RETURN VALUE    None

SIDE EFFECTS    None

===========================================================================*/

extern void wl1_edrx_wakeup(uint32 drxctrl_bmask, WL1_EDRX_CONFIG_DONE_CB wakeup_done_client_cb);

/*===========================================================================

FUNCTION       WL1_EDRX_SLEEP

DESCRIPTION     This function initialises EDRX state machine and other parameters

DEPENDENCIES    None

RETURN VALUE    None

SIDE EFFECTS    None

===========================================================================*/

extern void wl1_edrx_sleep(uint32 drxctrl_sleep_bmask,WL1_EDRX_CONFIG_DONE_CB sleep_done_client_cb);

/*===========================================================================

FUNCTION       WL1_EDRX_WAKEUP

DESCRIPTION     This function initialises EDRX state machine and other parameters

DEPENDENCIES    None

RETURN VALUE    None

SIDE EFFECTS    None

===========================================================================*/

extern void wl1_edrx_suspend( wl1_edrx_entity_enum_type entity_type, WL1_EDRX_CONFIG_DONE_CB suspend_done_client_cb);

/*===========================================================================

FUNCTION       WL1_EDRX_WAKEUP

DESCRIPTION     This function initialises EDRX state machine and other parameters

DEPENDENCIES    None

RETURN VALUE    None

SIDE EFFECTS    None

===========================================================================*/

extern void wl1_edrx_resume(wl1_edrx_entity_enum_type entity_type, WL1_EDRX_CONFIG_DONE_CB resume_done_client_cb);

/*===========================================================================

FUNCTION       WL1_EDRX_START

DESCRIPTION     This function will be called when the criteria to move into the EDRX
                       pattern has been satisfied.

DEPENDENCIES    None

RETURN VALUE    None

SIDE EFFECTS    None

===========================================================================*/

extern void wl1_edrx_start(void);

/*===========================================================================

FUNCTION       WL1_EDRX_STOP

DESCRIPTION     This function will be called when the criteria to move into the EDRX
                       pattern has been satisfied.

DEPENDENCIES    None

RETURN VALUE    None

SIDE EFFECTS    None

===========================================================================*/
extern void wl1_edrx_stop(void);

/*===========================================================================

FUNCTION        wl1_edrx_calculate_prewakeup_margin

DESCRIPTION     This function initialises the hs drx entity database

DEPENDENCIES    None

RETURN VALUE    None

SIDE EFFECTS    None

===========================================================================*/
extern uint16 wl1_edrx_calculate_prewakeup_margin
(
  uint16 drxctrl_entity_id
);

/*===========================================================================

FUNCTION        wl1_hsdrx_entity_init

DESCRIPTION     This function initialises the hs drx entity database

DEPENDENCIES    None

RETURN VALUE    None

SIDE EFFECTS    None

===========================================================================*/
extern void wl1_edrx_entity_cmd_handler(wl1_edrx_action_local_cmd_type *edrx_cmd_ptr);

/*===========================================================================

FUNCTION        wl1_edrx_get_edrx_configured_status

DESCRIPTION     This function can be used to find out the E-DRX configured status

DEPENDENCIES    None

RETURN VALUE    None

SIDE EFFECTS    None

===========================================================================*/
extern boolean wl1_edrx_get_edrx_configured_status(void);

#endif /* FEATURE_WCDMA_HS_FACH_DRX */

#endif

