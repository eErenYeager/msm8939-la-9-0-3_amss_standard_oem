#ifndef DECHS_H
#define DECHS_H

/*==========================================================================
             WCDMA L1 HSDPA configuration header file

DESCRIPTION
  This file contains definition, declaration required for HSDPA configuration
  and maintainance code.

  Copyright (c) 2004 - 2012 by Qualcomm Technologies Incorporated.
  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/wcdma/l1/offline/src/dechs.h#1 $
$DateTime: 2015/01/27 06:42:19 $
$Author: mplp4svc $

when       who    what, where, why
--------   ---    ----------------------------------------------------------
05/28/14   sm     API to support wait for agc tune done when RxD transitions on
03/22/14   hdk    Forcing Pess CQI to flush last log packet when HSDPA is stopped
03/17/14   hdk    Forcefully resetting Pess CQI logging state machine when HS channel is stopped.
03/11/14   hdk    Removing HS logging state machine for HS decode, A&E and SCCH stats log pkts
02/03/14   vs     0x4222 redesign to account for the new circular buffer logging.
11/18/13   rsr    Added QICE 3C support.
11/12/13   hdk    0x4222 HS Deocode log pkt support for 3C.
07/24/12   geg    Upmerge WCDMA.MPSS.1.0 to WCDMA.MPSS.2.0
05/9/12    vs     Added the support for the new log packet(0x423f).
01/28/12   vs     Nikel feature cleanup.
07/05/11   vsr    Mainlining Genesis CME features
03/14/11   ms     Mainlined FEATURE_WCDMA_MDSP_INTERFACE_REV_6280
10/06/10   ms     Klockwork fixes for LLR & MIMO Demod logging
10/05/10   mc     Enhanced QXDM overrides for misAsyncBuffer for Genesis
                  Use CLTD2 bitmask independent of CLTD1 bitmask
08/27/10   vsr    Changes for DC HS stats collection
05/14/10   rvs    Fix compile error in function declaration.
05/14/10   rvs    Add prototypes for function to set MIMO CQI margin for
                  each receiver for either PCPICH or SCPICH mode.
02/05/10   ms     Taxis/Genesis code merge
12/29/09   ms     Support to log secondary carrier HS stats in 0x4222.
11/20/09   ms     Updated UE category field in log pkt to support Cat24
11/13/09   ms     Added HS Decode Status Ver6 log packet support for DC.
09/09/09   rvs    Add callback to CME to notify of SCCH decode valid and
                  attempted statistics.
07/15/09   rvs    Added dec_hs_get_hs_pdsch_div_mode().
07/10/09   vc     Added support for 0x420F log packet and increased A&E
                  sample bitwidth for 0x421D.
05/20/09   vc     Added function prototype definitions for dec_hs_mimo_chan_submit_log_buffer
                  and void dec_hs_mimo_demod_submit_log_buffer  
05/08/09   sup       Changing the includes for the renaming of header files  dec.h, enci.h, l1m.h, srch.h, srchi.h, 
                    srchlog.h to wdec.h, wenci.h, wl1m.h, wsrch.h, wsrchi.h, wsrchlog.h
04/24/09   mr     Fixed Compiler Warnings
03/16/09   rc     Added a macro to update the RxD status in dec_hs_rl_status_mask
                  used in HS CQI BLER alignment.
03/12/09   rc     Added support for HS CQI BLER alignment.
03/03/09   ms     Removed unused non-MCAL code
12/02/08   ms     Removed feature dependency for FEATURE_PESSIMISTIC_CQI.
11/24/08   scm    Use RUMI_BRING_UP to fix CQI compilation problem.
10/28/08   mc     Added support for AUTO ACK.
09/24/08   vc     Added support for 0x4224 MIMO demod log packet.
09/12/08   vc     Added HARQ stat update for dual stream in 0x4221 log packet.
08/15/08   vc     Added mimo specific log support to 0x4222 log packet.
06/10/08   ms     Support for HS SCCH Debug log packet
01/25/08   mc     Added support for MCAL
05/14/07   mmr    Added code to get fing params update for Triage.
08/02/06   mc     HS decode status log packet is logged based on variable
                  exported from mDSP or EQ feature being defined.
05/18/06   mc     Added a function to query the HS stat next log action.
04/20/06   mc     Added support to log a subframe as MISSED if there has 
                  been a EQ->Rake switch.
04/12/06   mc     Added support for EQ log packets
04/11/06   bd     Mainlined the file from .../l1_msm6275/dechs.h#8
03/13/06   mc     Fixed lint errors
11/23/05   mc     Added support for HS amplitude and energy estimation 
                  log packet. 
08/25/05   mc     Added function definitions and variables for logging
                  pessimistic CQI samples.
07/20/05   mc     Added external variables for manual CQI update and also
                  prototype for the diag command callback function.
03/17/05   mc     Removed lint warnings and errors.
01/12/05   gs     Added macro for number of MAC-hs header log buffers
                  Externalized functions dec_hs_init and functions related
                  to MAC-hs header log support
12/27/04   gs     Added include file dec.h
                  Externalized vaiables to maintain overall BLER
10/11/04   gs     Created this module

===========================================================================*/

#include "wcdma_variation.h"
#include "comdef.h"
#include "customer.h"


/* ==========================================================================
** Includes and Public Data Declarations
** ========================================================================*/

/* -----------------------------------------------------------------------
** Include Files
** ----------------------------------------------------------------------- */

#include "wdec.h"

#include "l1rrcif.h"
#include "hscfg.h"

/* ---------------------------- */
/* Check for necessary features */
/* ---------------------------- */

/* -----------------------------------------------------------------------
** Constant / Define Declarations
** ----------------------------------------------------------------------- */

/* Number of HS SCCH debug log buffer */
#define HS_SCCH_DBG_LOG_NUM_BUF 2
  
/* HS decode status fields */
/* ----------------------- */

/* Number of HS decode status buffer */
#define HS_DECODE_STATUS_NUM_BUF 2
/* number of W16 in decode status entry for sub-frame in mDSP */
#define HS_DECODE_STATUS_NUM_W16_PER_ENTRY 3

/* Various fields in mDSP decode status buffer for a sub-frame */

#define HS_DECODE_STATUS_SCCH_DEMOD_ATTEMPTED_BMSK 0x8000
#define HS_DECODE_STATUS_SCCH_DEMOD_ATTEMPTED_REG  0
#define HS_DECODE_STATUS_SCCH_DEMOD_ATTEMPTED_SHFT 15

#define HS_DECODE_STATUS_SCCH_DEMOD_VALID_BMSK 0x4000
#define HS_DECODE_STATUS_SCCH_DEMOD_VALID_REG  0
#define HS_DECODE_STATUS_SCCH_DEMOD_VALID_SHFT 14

#define HS_DECODE_STATUS_SCCH_DATA_BMSK 0x3FFF
#define HS_DECODE_STATUS_SCCH_DATA_REG  0
#define HS_DECODE_STATUS_SCCH_DATA_SHFT 0

#define HS_DECODE_STATUS_HARQ_ID_BMSK 0x0038
#define HS_DECODE_STATUS_HARQ_ID_REG  0
#define HS_DECODE_STATUS_HARQ_ID_SHFT 3

#define HS_DECODE_STATUS_WINNING_SCCH_CH_IDX_BMSK 0xC000
#define HS_DECODE_STATUS_WINNING_SCCH_CH_IDX_REG  1
#define HS_DECODE_STATUS_WINNING_SCCH_CH_IDX_SHFT 14

#define HS_DECODE_STATUS_DSCH_TB_SZ_BMSK 0x3FFF
#define HS_DECODE_STATUS_DSCH_TB_SZ_REG  1
#define HS_DECODE_STATUS_DSCH_TB_SZ_SHFT 0

#define HS_DECODE_STATUS_UNQUAN_CQI_BMSK 0xFFF0
#define HS_DECODE_STATUS_UNQUAN_CQI_REG  2
#define HS_DECODE_STATUS_UNQUAN_CQI_SHFT 4

#define HS_DECODE_STATUS_NEW_TX_BMSK 0x0002
#define HS_DECODE_STATUS_NEW_TX_REG  2
#define HS_DECODE_STATUS_NEW_TX_SHFT 1

#define HS_DECODE_STATUS_DSCH_STATUS_BMSK 0x0001
#define HS_DECODE_STATUS_DSCH_STATUS_REG  2
#define HS_DECODE_STATUS_DSCH_STATUS_SHFT 0

#define HS_DECODE_STATUS_DSCH_STATUS_NACK 0
#define HS_DECODE_STATUS_DSCH_STATUS_ACK  1
#define HS_DECODE_STATUS_DSCH_STATUS_AUTO_ACK  2


/* DPCH_STTD bit field mask and shift in control register */
#define DEC_HS_DPCH_STTD_BMSK      0x01
#define DEC_HS_DPCH_STTD_SHFT      0

/* DPCH_CLTD bit field mask and shift in control register */
#define DEC_HS_DPCH_CLTD1_BMSK      0x02
#define DEC_HS_DPCH_CLTD1_SHFT      1

#define DEC_HS_DPCH_CLTD2_BMSK      0x04
#define DEC_HS_DPCH_CLTD2_SHFT      2

/* CPICH Diveristy mask and shift in the control register */
#define DEC_HS_DIV_PILOT_BMSK      0x08
#define DEC_HS_DIV_PILOT_SHFT      3

/* RxD transition bit field mask and shift in control register */
#define DEC_HS_RXD_TRANS_BMSK      0x10
#define DEC_HS_RXD_TRANS_SHFT      4

/* Macro to extract the bit field value from the HS RL status mask */
#define DEC_HS_GET_RL_STATUS_BF_PARAM(buffer, type) \
  ((buffer & DEC_HS_##type##_BMSK) >> DEC_HS_##type##_SHFT)


/* Macro to set the bit foeld value from the HS RL status mask */
#define DEC_HS_SET_RL_STATUS_BF_PARAM(type, value) \
  dec_hs_rl_status_mask = ((dec_hs_rl_status_mask & ~(DEC_HS_##type##_BMSK)) | ((value << DEC_HS_##type##_SHFT) & DEC_HS_##type##_BMSK))


#define DEC_HS_SET_RXD_STATUS_IN_HS_RL_MASK()   DEC_HS_SET_RL_STATUS_BF_PARAM(RXD_TRANS, rxd_is_active())

/* This macro gets the value form mDSP decode status buffer (W16 array)
   for the given field name */
#define HS_DECODE_STATUS_FIELD_VAL(buffer, field) \
  (((buffer)[HS_DECODE_STATUS_##field##_REG] & HS_DECODE_STATUS_##field##_BMSK) >> \
   HS_DECODE_STATUS_##field##_SHFT)
   
/* For HS decode status TB data bit logging, sub frame for frame N-2
   are logged at sub frame N, so there is 3 frame buffer, it will have
   3x5= 15 sub frames. Keep data buffered for 16 sub frames indexed with
   subframe mod 16 */
#define HS_DECODE_DSCH_TB_DATA_BIT_BUF_LEN 16
  
/* Number of log packet buffer for MAC hs header logging */
#define DEC_HS_NUM_MAC_HS_HDR_LOG_PKT_BUF 2

#ifdef FEATURE_WCDMA_MDSP_INTERFACE_REV_6280
/* Number of HS LLR log buffer */
#define HS_AMP_ENG_LOG_NUM_BUF 2
#define HS_MIMO_LOG_NUM_BUF 2
#define HS_PESS_CQI_LOG_NUM_BUF 2
/* Number of HS SCCH log buffer */
#define HS_SCCH_LOG_NUM_BUF 2
#endif /* FEATURE_WCDMA_MDSP_INTERFACE_REV_6280 */

#define DEC_HS_INCR_LLR_LOG_BUF_OFFSET() \
  ((hs_llr_sample_buf_offset < \
          ((HS_NUM_MAX_HS_AMP_ENG_SAMPLES * HS_AMP_ENG_INFO_NUM_W8_PER_ENTRY)-1)) ? \
          hs_llr_sample_buf_offset++ : 0)

#ifdef FEATURE_WCDMA_MIMO
#define DEC_HS_INCR_MIMO_DEMOD_BUF_OFFSET() \
  ((hs_mimo_demod_sample_buf_offset < \
          (HS_MIMO_DEMOD_LOG_MAX_BUF_SIZE-1)) ? \
          hs_mimo_demod_sample_buf_offset++ : 0)
#endif
/*Macro for invalid Stop/Final GSFN for logging*/
#define INVALID_STOP_GSFN (0xFFFF)
/* -----------------------------------------------------------------------
** Type Declarations
** ----------------------------------------------------------------------- */

/* Call back function type declaration */
/* ----------------------------------- */

/* Enum type declaration */
/* --------------------- */

/* This enum defines various actions for HS stat generation */
typedef enum 
{
  HS_STAT_GEN_NOOP,
  HS_STAT_GEN_START,
  HS_STAT_GEN_RESTART,
  HS_STAT_GEN_STOP
} hs_stat_gen_action_enum_type;

/* This enum defines the various actions for pessimistic CQI logging */
typedef enum
{
  DEC_HS_LOG_PESS_CQI,
  DEC_HS_CLEAR_PESS_CQI,
  DEC_HS_FLUSH_PESS_CQI
}dec_hs_pess_cqi_action_enum_type;

typedef enum
{
  DEMFRONT_RAKE = 1,
  DEMFRONT_QICE,
  DEMFRONT_INVALID = 0xFF
}dec_hs_demfront_enum_type;
/* Structure type declaration */
/* -------------------------- */

/* This structure defines HS status that is required for HS downlink channels
   stat generation */
typedef struct
{
  /* hs Decode status for each carrier*/
  /*Num of configured carriers*/
  uint8 num_carr; 
  /* Quantized CQI from n-3 frame where n is rest of info */
  uint8 quan_cqi[L1_HSDPA_MAX_CARRIERS]; 
  /* indicate if SCCH demod was even attempted or not */
  boolean scch_demod_attempted[L1_HSDPA_MAX_CARRIERS];
  /* TRUE indicate SCCH was demodulated and CRC pass FALSE otherwise */
  boolean scch_valid[L1_HSDPA_MAX_CARRIERS];
  /* DSCH CRC pass/fail status */
  boolean dsch_status[L1_HSDPA_MAX_CARRIERS];
  /* New transmission of HS DSCH block */
  boolean new_tx[L1_HSDPA_MAX_CARRIERS];
  /* HARQ process Id primary*/
  uint8 harq_proc_id[L1_HSDPA_MAX_CARRIERS];
  /* transport block size of this DSCH block */
  uint16 tb_size[L1_HSDPA_MAX_CARRIERS];

} dec_hs_decode_status_struct_type;


/* This structure defines HS-DSCH HARQ process stats */
typedef struct
{
  /* indicate that ACK has been received for this flag. If ACK is received then
     this process Id is supposed to be done for stat generation. It is not considered
     again untill new transmission is received.
     Set to TRUE for initialization */
  boolean ack_received;
  /* number of NACKs received. Init it to 0 when new transmission is received */
  uint8 num_nack_rece;
} hs_dsch_harq_proc_stat_struct_type;

/* -----------------------------------------------------------------------
** Global Data Declarations
** ----------------------------------------------------------------------- */

/* HS-DSCH MAC-d PDU deciphering related variables */

/* Deciphering start address of current MAC-d PDU */
extern uint16 dec_hs_tb_deciph_start_addr;
/* Deciphering start bit position at start address */
extern uint16 dec_hs_deciph_start_bit_pos;
/* Length with in MAC-d PDU size to decipher. This length is one less
   than actual length */
extern uint16 dec_hs_tb_deciph_len;
/* Offset of first bit to be deciphered of current MAC-d PDU from start of
   HS-DSCH block */
extern uint16 dec_hs_blk_deciph_first_bit_offset;

/* HS-DSCH MAC-d SDU data transfer related variables */

/* DOB start address to read data from */
extern uint16 dec_hs_tb_bg_xfer_start_addr;
/* Starting offset to read first bit at DOB start address */
extern uint8  dec_hs_bg_xfer_start_offset;
/* MAC-d SDU length */
extern uint16 dec_hs_tb_bg_xfer_len;

/* status mask for HS RL TxD status */
extern uint8 dec_hs_rl_status_mask;
/* Critical section mutex for hs_rl_status_mask */
extern rex_crit_sect_type hs_rl_status_mask_crit_sect;


/* This maintains running count of new transmissions received and
   that are in error */
extern uint32 dec_hs_num_new_tx_rece;
extern uint32 dec_hs_new_tx_in_err;



/* The next pessimistic CQI command */
extern dec_hs_pess_cqi_action_enum_type dec_hs_pess_cqi_log_next_sample;
/* =======================================================================
**                        Function Declarations
** ======================================================================= */

/*===========================================================================
FUNCTION dec_hs_set_stat_action

DESCRIPTION
  This function sets stat generation mode.
 
DEPENDENCIES
  None

RETURN VALUE
  TRUE or FALSE based on action accept validation

SIDE EFFECTS
  None
===========================================================================*/
extern boolean dec_hs_set_stat_action(hs_stat_gen_action_enum_type action);

/*===========================================================================
FUNCTION dec_hs_set_pess_cqi_accum_active

DESCRIPTION
  This function initialize the value of the global dec_hs_set_pess_cqi_accum_active for 
  HS pessimistic CQI logging
  
DEPENDENCIES
  None
  
RETURN VALUE
  None

SIDE EFFECTS
  Variables get inited to their initial values
===========================================================================*/

extern void dec_hs_set_pess_cqi_accum_active(boolean accum_status);
/*===========================================================================
FUNCTION dec_hs_init

DESCRIPTION
  This function initialize various variables in this module. It is intended
  to be called at L1 stack startup along with other module init.
  
DEPENDENCIES
  None
  
RETURN VALUE
  None

SIDE EFFECTS
  Variables get inited to their initial values
===========================================================================*/

extern void dec_hs_init(void);

#ifdef FEATURE_WCDMA_MIMO
/*===========================================================================
FUNCTION dec_hs_set_mimo_cqi_margin_rake_pcpich

DESCRIPTION
  Update the CQI Margin values in FW for Rake during P-CPICH MIMO.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
extern void dec_hs_set_mimo_cqi_margin_rake_pcpich(uint16 mimo_cqi_margin_ss,
                                                   uint16 mimo_cqi_margin_ds);

/*===========================================================================
FUNCTION dec_hs_set_mimo_cqi_margin_eq_pcpich

DESCRIPTION
  Update the CQI Margin values in FW for EQ during P-CPICH MIMO.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
extern void dec_hs_set_mimo_cqi_margin_eq_pcpich(uint16 mimo_cqi_margin_ss,
                                                 uint16 mimo_cqi_margin_ds);

/*===========================================================================
FUNCTION dec_hs_set_mimo_cqi_margin_qice_pcpich

DESCRIPTION
  Update the CQI Margin values in FW for QICE during P-CPICH MIMO.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
extern void dec_hs_set_mimo_cqi_margin_qice_pcpich(uint16 mimo_cqi_margin_ss,
                                                   uint16 mimo_cqi_margin_ds);

/*===========================================================================
FUNCTION dec_hs_set_mimo_cqi_margin_rake_scpich

DESCRIPTION
  Update the CQI Margin values in FW for Rake during S-CPICH MIMO.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
extern void dec_hs_set_mimo_cqi_margin_rake_scpich(uint16 mimo_cqi_margin_ss,
                                                   uint16 mimo_cqi_margin_ds);

/*===========================================================================
FUNCTION dec_hs_set_mimo_cqi_margin_eq_scpich

DESCRIPTION
  Update the CQI Margin values in FW for EQ during S-CPICH MIMO.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
extern void dec_hs_set_mimo_cqi_margin_eq_scpich(uint16 mimo_cqi_margin_ss,
                                                 uint16 mimo_cqi_margin_ds);

/*===========================================================================
FUNCTION dec_hs_set_mimo_cqi_margin_qice_scpich

DESCRIPTION
  Update the CQI Margin values in FW for QICE during S-CPICH MIMO.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
extern void dec_hs_set_mimo_cqi_margin_qice_scpich(uint16 mimo_cqi_margin_ss,
                                                   uint16 mimo_cqi_margin_ds);

/*===========================================================================
FUNCTION dec_hs_set_mimo_cqi_margin_rake_ds_sic

DESCRIPTION
  Update the CQI Margin_DS_SIC for Rake during MIMO call
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

extern void dec_hs_set_mimo_cqi_margin_rake_ds_sic(uint32 mimo_param_val);

/*===========================================================================
FUNCTION dec_hs_set_mimo_cqi_margin_eq_ds_sic

DESCRIPTION
  Update the CQI Margin_DS_SIC for EQ during MIMO call
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

extern void dec_hs_set_mimo_cqi_margin_eq_ds_sic(uint32 mimo_param_val);

/*===========================================================================
FUNCTION dec_hs_set_mimo_cqi_margin_qice_ds_sic

DESCRIPTION
  Update the CQI Margin_DS_SIC for QICE during MIMO call
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

extern void dec_hs_set_mimo_cqi_margin_qice_ds_sic(uint32 mimo_param_val);
#endif

/*===========================================================================
FUNCTION dec_hs_rxd_transition_callback

DESCRIPTION
  This is the callback function registered with RxD module. 
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

extern void dec_hs_rxd_transition_callback(
  /* State of the callback, TRUE if RxD transition has started */
  boolean transition);

/*===========================================================================
FUNCTION dec_hs_srch_td_update_callback

DESCRIPTION
  This is the callback function registered with SRCH module. 
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

extern void dec_hs_srch_td_update_callback(void);

/*===========================================================================
FUNCTION dec_hs_register_callbacks_for_cqi_bler_alignment

DESCRIPTION
  This function registers callbacks  
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
extern void dec_hs_register_callbacks_for_cqi_bler_alignment(void);


/*===========================================================================
FUNCTION dec_hs_deregister_callbacks_for_cqi_bler_alignment

DESCRIPTION
  This function deregisters callbacks    
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

extern void dec_hs_deregister_callbacks_for_cqi_bler_alignment(void);

/* -------------------------------------------------------- */
/* Following functions handle HS statistics and its logging */
/* -------------------------------------------------------- */

/*===========================================================================
FUNCTION dec_hs_stat_init

DESCRIPTION
  This function initializes the variables required for generation and
  accumulation of HS decode status and stats. It initialize the following
  variables
  
  hs_stat_accum_active is set to FALSE to indicate inactive
  hs_stats_gen_action is set to NOOP
  All HSDPA status log buffers are set to available
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  All above mentioned variables in descriptions are inited
===========================================================================*/

extern void dec_hs_stat_init(void);

/*===========================================================================
FUNCTION dec_hs_get_stat_action

DESCRIPTION
  This function returns the action that HS logging state machine needs
  to perform next.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

extern hs_stat_gen_action_enum_type dec_hs_get_stat_action(void);

/*===========================================================================
FUNCTION dec_hs_stat_do_action

DESCRIPTION
  This function changes HSDPA decode status and stat generation mode. Before
  it commit action provided function argument, it validates if that action
  can be accepted. Current action should be always NOOP that is reset at
  start and after every action completion
  
  When stat generation is inactive, only action that is accepted is START.
  When stat generation is active it can be RESTARTED or STOPPED
  
  There are 2 buffer for accumulating HS decode status. These are used when
  a existing accumulation need to be submitted for logging. It happens when
  accumulation is done or there is reconfiguration and previous configuration
  need to be flushed.
  
DEPENDENCIES
  None

RETURN VALUE
  TRUE or FALSE based on action accept validation

SIDE EFFECTS
  None
===========================================================================*/
extern boolean dec_hs_stat_do_action(
  /* Action for HS decode statu accumulation and stat generation */
  hs_stat_gen_action_enum_type action,
  /* Sub frame number at which to start or restart */
  uint16 start_sub_fn,
  /* start CFN to start or restart accumulation/stat generation */
  uint16 start_cfn,
  /* mDSP info table index from where to read the information */
  hs_dl_channel_per_carrier_struct_type  *info_table_ptr,
  /* number of carries, used to access idx array*/
  uint8  num_carr,
  /* Last sub frame number after which information is submitted for logging
     then either restarted ot stopped based on action */
  uint16 final_sub_fn);

/*===========================================================================
FUNCTION dec_hs_prep_log_action

DESCRIPTION
  HS logging start: provide valid info table ptr and Final SFN = 0xFFFF
  HS logging stop: provide valid SFN to stop 
DEPENDENCIES
  Hs config tables.

RETURN VALUE
  void

SIDE EFFECTS
  Max one start and stop action in 1 Radio Frame (10msec)
===========================================================================*/


extern void dec_hs_prep_log_action(
  
  /* mDSP pointer to info table idx array, from where to read the information */
  hs_dl_channel_per_carrier_struct_type  *info_table_ptr,
  /*number of carries, used to access idx array*/
  uint8  num_carr,
  /* Last sub frame number after which information is submitted for logging
   then either restarted ot stopped based on action */
  uint16 final_sub_fn);

/*===========================================================================
FUNCTION dec_hs_decode_update_stats

DESCRIPTION
  This function is called periodically every frame to act on new action
  information, if set and do periodic decode status accumulation and stat
  generation.
  Flush flag is used to force flush of LLR logging when HS channel is stopped.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

extern void dec_hs_decode_update_stats(boolean force_flush);

/*===========================================================================
FUNCTION dec_hs_log_pess_cqi

DESCRIPTION
  This function checks the log code in the diag server and if enabled then
  it collects the next sample and fills it in the log buffer. Once the log 
  buffer is full it flushes the log buffer. When the log buffer is empty and
  the first sample has to be logged, it initializes all the buffers and state
  variables.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  dec_hs_pess_cqi_first_sample is changed to FALSE if it was TRUE after 
  initializing the log buffers and state variables.
  If the command was to log next sample, num_samples is incremented.
===========================================================================*/

extern void dec_hs_log_pess_cqi(void);

/*===========================================================================
FUNCTION dec_hs_flush_pess_cqi_log_pkt

DESCRIPTION
  This function computes the length of the log buffer being submitted and 
  then posts a local command to L1 to this log packet. It does a 
  sanity testing to see that the log packet is not empty before submitting.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  Flag indicating log packet is submitted as well as that the next sample
  will be the first sample is made TRUE.
===========================================================================*/

extern void dec_hs_flush_pess_cqi_log_pkt(void);



/*===========================================================================
FUNCTION dec_hs_scch_submit_log_buffer

DESCRIPTION
  This function is called to post HS scch submit command. If the 
  buffer is not ready to be submitted to diag the function returns without
  doing anything. This can so happen because multiple logs are instructed
  to be submitted to diag by the same L1 command.
  It checks for the log code in diag and submits the packet.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  Flag indicating the log buffer is submitted for flushing is reset.
===========================================================================*/

extern void dec_hs_scch_submit_log_buffer(void);

/*===========================================================================
FUNCTION dec_hs_pess_cqi_submit_log_buffer

DESCRIPTION
  This function is called to post HS pessimistic CQI submit command. If the 
  buffer is not ready to be submitted to diag the function returns without
  doing anything. This can so happen because multiple logs are instructed
  to be submitted to diag by the same L1 command.
  It checks for the log code in diag and submits the packet.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  Flag indicating the log buffer is submitted for flushing is reset.
===========================================================================*/

extern void dec_hs_pess_cqi_submit_log_buffer(void);

/*===========================================================================
FUNCTION dec_hs_decode_status_send_log_submit_cmd

DESCRIPTION
  This function is called to post HS log submit command. There is only one HS
  log submit command. HS cfg module calls all accumlated logging entities. Each
  entity is reponsible for maintaining log submit pending information. It keeps
  this information for HS decode status log submit pending information
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

extern void dec_hs_decode_status_send_log_submit_cmd(
  /* HS decode status log buffer index to submit */
  uint8 decode_status_info_buf_idx);

/*===========================================================================
FUNCTION dec_hs_decode_status_submit_log_buffer

DESCRIPTION
  This function is called from hscfg module in response of HS log submit
  command. It checks for hs decode status log submit flag. It is set to TRUE,
  it submit log buffer pointed by lof buffer index to submit.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

extern void dec_hs_decode_status_submit_log_buffer(void);

/*===========================================================================
FUNCTION dec_hs_quantize_cqi

DESCRIPTION
  This function quantize CQI from lowest value of 1 to max value of 30.
  Unquantized value is 10 bit number in 6.4 format (10Q4).
  
DEPENDENCIES
  None

RETURN VALUE
  6 Bit unquantized value form 1 to 30

SIDE EFFECTS
  None
===========================================================================*/

extern uint8 dec_hs_quantize_cqi(
  /* Unquantized CQI value */
  uint16 unquan_cqi);

/*===========================================================================
FUNCTION dec_hs_init_stats

DESCRIPTION
  This function initialize stats information.
  
  It is called when ever HS stat generation is started at START or RESTART
  action OR
  when ever stats have been generated using max required number of samples
  
DEPENDENCIES
  None

RETURN VALUE
  5 Bit unquantized value form 1 to 30

SIDE EFFECTS
  None
===========================================================================*/

extern void dec_hs_init_stats(void);

/*===========================================================================
FUNCTION dec_hs_gen_stats

DESCRIPTION
  This function generate various stats for HS downlink channels.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

extern void dec_hs_gen_stats(
  /* HS decode status structure pointer */
  dec_hs_decode_status_struct_type *decode_status);

/*===========================================================================
FUNCTION dec_hs_submit_stats

DESCRIPTION
  This function submits the HS downlink channel stats to logging. This is
  done when HS stat gen/accumulation action is STOP or RESTART or when stats
  have been generated using max number of samples.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

extern void dec_hs_submit_stats(void);


/*===========================================================================
FUNCTION dec_hs_mac_hs_hdr_send_log_submit_cmd

DESCRIPTION
  This function is called to post MAC-hs header log submit command. There is
  only one HS log submit command. HS cfg module calls all accumlated logging
  entities. Each entity is reponsible for maintaining log submit pending
  information. It keeps this information for HS decode status log submit
  pending information
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

extern void dec_hs_mac_hs_hdr_send_log_submit_cmd(
  /* HS decode status log buffer index to submit */
  uint8 mac_hs_hdr_info_buf_idx);

/*===========================================================================
FUNCTION dec_hs_mac_hs_hdr_submit_log_buffer

DESCRIPTION
  This function is called from hscfg module in response of HS log submit
  command. It checks for MAC-hs header log submit flag. It is set to TRUE,
  it submit log buffer pointed by log buffer index to submit.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

extern void dec_hs_mac_hs_hdr_submit_log_buffer(void);

/*===========================================================================
FUNCTION dec_hs_flush_mac_hs_log_pkt

DESCRIPTION
  This function is called from hscfg module to flush MAC hs header log packet
  if any accumulation is pending. It is called when there is switch in DL
  HS config at any reconfig or stop action.
  Note that variables accessed by this functions are shared with function
  that reads TB header read. However both functions are event on Rx timeline
  so there for they are mutually exclusive.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

extern void dec_hs_flush_mac_hs_log_pkt(void);

#ifdef FEATURE_WCDMA_MDSP_INTERFACE_REV_6280
/*===========================================================================
FUNCTION dec_hs_llr_log_init

DESCRIPTION
  This function initializes all the variables and states associated with HS
  Amplitude and energy Estimation log packet. 
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  All state variables initialized to the default values.
===========================================================================*/

extern void dec_hs_llr_log_init(void);

/*===========================================================================
FUNCTION dec_hs_llr_update_log

DESCRIPTION
  This function is responsible reading A&E log data from FW and 
  populating SW log packet.
  
DEPENDENCIES
  force_flush flag is used for flush the log packet till the last 
  idx populated by FW and stopping.

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

extern void dec_hs_llr_update_log(boolean force_flush);

/*===========================================================================
FUNCTION dec_hs_llr_send_log_submit_cmd

DESCRIPTION
  This function makes a request to post a local command to submit the 
  currently accumulated log packet to diag. Before it does so, it checks if 
  a previous log packet that was submitted to diag has been  serviced or not.
  If not, it drops the current packet and proceeds further.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

extern void dec_hs_llr_send_log_submit_cmd(
  /* HS decode status log buffer index to submit */
  uint8 hs_llr_log_idx);

/*===========================================================================
FUNCTION dec_hs_llr_submit_log_buffer

DESCRIPTION
  This function is called from hscfg module in response of HS log submit
  command. It checks for HS AMP and ENG estimation log submit flag. 
  If it is set to TRUE, it submit log buffer pointed by log buffer index 
  to submit.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

extern void dec_hs_llr_submit_log_buffer(void);
#endif 

#ifdef FEATURE_WCDMA_MIMO
/*===========================================================================
FUNCTION dec_hs_mimo_demod_submit_log_buffer

DESCRIPTION
  This function is called from hscfg module in response of HS log submit
  command. It checks for MIMO demod log submit flag. 
  If it is set to TRUE, it submit log buffer pointed by log buffer index 
  to submit.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
extern void dec_hs_mimo_demod_submit_log_buffer(void);

/*===========================================================================
FUNCTION dec_hs_mimo_chan_submit_log_buffer

DESCRIPTION
  This function is called from hscfg module in response of HS log submit
  command. It checks for MIMO chan log submit flag. 
  If it is set to TRUE, it submit log buffer pointed by log buffer index 
  to submit.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
extern void dec_hs_mimo_chan_submit_log_buffer(void);

#endif

#ifdef FEATURE_WCDMA_DYNAMIC_FING_GRAN
#error code not present
#endif /* FEATURE_WCDMA_DYNAMIC_FING_GRAN */

typedef void DEC_HS_CME_CME_CB_FUNC_TYPE(uint8, boolean *, boolean *);

/*===========================================================================
FUNCTION dec_hs_register_hs_scch_stats_cb

DESCRIPTION
  Registers a function called when HS stats are generated.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
extern void dec_hs_register_hs_scch_stats_cb(DEC_HS_CME_CME_CB_FUNC_TYPE cme_cb);

/*===========================================================================
FUNCTION dec_hs_deregister_hs_scch_stats_cb

DESCRIPTION
  Deregisters the function called when HS stats are generated.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
extern void dec_hs_deregister_hs_scch_stats_cb(void);

/*===========================================================================
FUNCTION dec_hs_get_hs_pdsch_div_mode

DESCRIPTION
  Returns the diversity mode of the HS-PDSCH.
  
DEPENDENCIES
  None

RETURN VALUE
  0 - None, 1 - STTD, 2 - CLTD

SIDE EFFECTS
  None
===========================================================================*/
extern l1_tx_div_mode_enum_type dec_hs_get_hs_pdsch_div_mode(void);

/*===========================================================================
FUNCTION dec_hs_demfront_mode

DESCRIPTION
  Returns the Demfront Mode and Rake by default if QICE is not enabled
  
DEPENDENCIES
  HS module

RETURN VALUE
  dec_hs_demfront_enum_type 

SIDE EFFECTS
  None
===========================================================================*/

dec_hs_demfront_enum_type dec_hs_demfront_mode(void);


#endif /* DECHS_H */ 
