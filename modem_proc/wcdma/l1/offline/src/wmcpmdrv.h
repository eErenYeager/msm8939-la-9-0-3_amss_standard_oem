#ifndef WMCPMDRV_H
#define WMCPMDRV_H

/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                         WCDMA MCPM Driver interface

GENERAL DESCRIPTION

  This file contains the source code for the WCDMA Modem Power Abstraction
  layer implementation.


Copyright (c) 2000-2015 by QUALCOMM Technologies, Incorporated. All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.

*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/wcdma/l1/offline/src/wmcpmdrv.h#1 $
$DateTime: 2015/01/27 06:42:19 $
$Author: mplp4svc $

when           who      what, where, why
--------       ----     ----------------------------------------------------------
09/16/14       kr       MDSP Recovery boost changes.
07/31/14       kr       DPM.2.0 MCVS changes
06/20/14       skk      Added W2L client status api.
05/12/14       sks      Correct an error in logic check while sending the MCPM config modem. 
05/11/14       pr       MCPM Opts for unification of HSDPA/HSUA Clk requests.
04/24/14       kr       Dime PM check for Aset fingers instead of Total no. of fingers
04/18/14       kr       DC MCVS checkin for Dime PM
04/25/14       kr       Bump up VPE if Neighbor cells/fing are configured with TxD
04/11/14       kr       Track TxD status of Qset from CME
03/14/14       kr       Added client to Handle Offline BUMP up req when doing X2W resel HO
02/18/13       sr       Initial checkin for CPC Lite sleep
02/28/14       kr       DimePM changes
10/24/13       kr       Handle Triage and CME VPE request separately so as to update respective Params
11/22/13       kr       Need to bump up the clock to 384MHz with NBR cell enabled with QICE
11/04/13       stk      MCPM related changes for 3C
09/17/13       jkb      Porting Dual Sim from Triton to Dime
09/12/13       jd       Searcher redesign 
08/19/13       kr       Bump Q6 clk if DC is enable dand VPE is at NOM/Turbo
08/23/13       kr       MCVS Support for Pull in event
08/01/13       sks      Change finger counter for HS NOM. Change SVS PLUS to reduce fingers and counter total # fingers.
07/18/13       kr       MCVS: added support for SVS+ 
06/19/13       kr       Updated the Turbo switching condition and Number of fingers for NOM
06/10/13       kr       Added support to Bump up/down VPE witch PCCPCH add drop
04/15/13       kr       Protecting MCPM requests/release with Mutex lock.
04/12/13       stk      Changes to have NV support to bumpup Q6, VPE and Clkbus, and DIME MCVS changes.
04/04/13       stk      Preventing ISR/Task collision with offline clk change callback by having separate globals for request and relinquish
                        to indicate who called mcpm config modem.
04/02/13       stk      Reverting the change made to request_parms; making it global again since receiver cfg depends on previous params.
                        Example: EQ ON depends on previous QICE ON. 
03/28/13       stk      Preventing ISR/Task collision with WMCPMDRV drivers by making wmcpmdrv_request_parms a local variable.
03/22/13       kr       changed switching point logic for HS accrding to new uK.
03/14/13       stk      Changed the QICE query API for MCVS VPE update
03/09/13       kr       MCVS feature enhancement for DC
03/08/13       kr       MCVS feature enhancement
02/11/13       gsk/sr   EDRX Full Sleep initial check in.
01/18/13       kr       changing switching point criteria for HS and R99 calls
01/17/13       kr       Declaring a new function
01/16/13       kr       chnage srch period in frames instead of msec
01/10/13       kr       Featurizing FR2018,MCVS feature for Dime only.
12/11/12       kr       Added Support for MCVS 
07/24/12       geg      Upmerge WCDMA.MPSS.1.0 to WCDMA.MPSS.2.0
05/24/12       pr       Dime Bringup Changes
03/27/12       stk      New mechanism to track the offline clk speed and update FW.
02/21/12       pv       Support for MAX_OFFLINE_CLK_REQ client.
02/21/12       pv       Created API for registering with MCPM for any clock change.
11/11/11       pv       Support for Sleep timeline adjustment callback with MCPM.
10/25/11       pv       Added CM client.
08/09/11       stk      Added support for MCPM PARAMS UPDATE REQ.
08/02/11       pv       Added API to register MCPM callback for a HW block.
07/28/11       pv       APIs/macros to return W2G client status.
10/18/10       stk      First version of the WMCPM Driver interface for NIKEL.


===========================================================================*/

#include "wcdma_variation.h"
#ifdef FEATURE_WCDMA_MCPM_DRIVER

#include "mcpm_api.h"

#include "rex.h"

/* MACROs for voice and data call bit setting in call setup bmsk*/
#define WMCPMDRV_R99_VOICE_CALL_ACTIVE   0x01
#define WMCPMDRV_R99_DATA_CALL_ACTIVE    0x02
#define WMCPMDRV_HS_DATA_CALL_ACTIVE     0x04

#ifdef FEATURE_WCDMA_DIMEPM_MCVS 
#define DIMEPM_MARGIN 1
#endif

#define WMCPMDRV_QSET_TXD_STATUS_BIT_SHIFT      4

#ifdef FEATURE_WCDMA_DIME_SW
#define WMCPMDRV_DEFAULT_OFFLINE_CLK_MCPM_SPEED MCPM_CLK_72M
#else
#define WMCPMDRV_DEFAULT_OFFLINE_CLK_MCPM_SPEED MCPM_CLK_76_8M
#endif /* FEATURE_WCDMA_DIME_SW */

/* Enumeration detailing the different WCDMA PWR driver clients */
typedef enum
{
  WCDMA_MPWRDRV_CLIENT_STACK_ACTIVE,          /* 0 */
  WCDMA_MPWRDRV_CLIENT_DL_ACTIVE,             /* 1 */
  WCDMA_MPWRDRV_CLIENT_DRX,                   /* 2 */   
  WCDMA_MPWRDRV_CLIENT_W2G,                   /* 3 */
  WCDMA_MPWRDRV_CLIENT_W2L,                   /* 4 */
  WCDMA_MPWRDRV_CLIENT_R99_VOICE,             /* 5 */
  WCDMA_MPWRDRV_CLIENT_R99_DATA,              /* 6 */
  WCDMA_MPWRDRV_CLIENT_HSPA,                  /* 7 */
  WCDMA_MPWRDRV_CLIENT_RXD,                   /* 8 */
  WCDMA_MPWRDRV_CLIENT_EQ,                    /* 9 */
  WCDMA_MPWRDRV_CLIENT_QICE,                  /* 10 */
  WCDMA_MPWRDRV_CLIENT_MIMO,                  /* 11 */ 
  WCDMA_MPWRDRV_CLIENT_CM,                    /* 12 */
  WCDMA_MPWRDRV_PARMS_UPDATE_REQ,             /* 13 */
  WCDMA_WMCPM_MCVSDRV_VPE_PULLIN_UPDATE,      /* 14 */
  WCDMA_WMCPM_MCVSDRV_VPE_UPDATE,             /* 15 */
  WCDMA_WMCPM_MCVSDRV_Q6CLK_UPDATE,           /* 16 */
  WCDMA_WMCPM_MCVSDRV_OFFLINECLK_UPDATE,      /* 17 */
  WCDMA_WMCPM_MCVSDRV_VPE_UPDATE_CME,         /* 18 */
  WCDMA_WMCPM_MCVSDRV_OFFLINECLK_UPDATE_SRCH, /* 19 */
  #ifdef FEATURE_DUAL_SIM
  WCDMA_MPWRDRV_WAKEUP_UPDATE_REQ,            /* 20 */
  #endif
  WCDMA_CDRX_GO_TO_LIGHT_SLEEP_REQ,           /* 21 - Data->LightSleep */
  WCDMA_EDRX_GO_TO_LIGHT_SLEEP_REQ,           /* 22 - Data->LightSleep */
  WCDMA_EDRX_GO_TO_SLEEP_REQ,                 /* 23 - LightSleep->Sleep */
  WCDMA_EDRX_WAKEUP_REQ,                      /* 24 - Sleep->LightSleep */
  #ifdef FEATURE_WCDMA_MDSP_RECOVERY
  WCDMA_WMCPM_MCVSDRV_Q6CLK_UPDATE_MDSP_RECOV, /*25 - boost for MDSP recovery */
  #endif
  WCDMA_MPWRDRV_MAX_NUM_CLIENTS
}wmcpmdrv_client_type;

#ifdef FEATURE_WCDMA_MCVS
/*VPE speed for MCVS in Khz*/
#ifdef FEATURE_WCDMA_DIMEPM_MCVS

extern uint8 updated_num_sho_cells;
extern uint8 updated_num_aset_fingers;

typedef enum
{
  WMCVS_VPE_NULL=0, 
  WMCVS_VPE_288K=384000,/*SVS*/
  WMCVS_VPE_499_2K=576000,/*NOM*/
  WMCVS_VPE_576K= 691000,  /*TURBO*/
  WMCVS_VPE_384K = 0,
} wmcpmdrv_mcvs_vpe_speedType;

#else

typedef enum
{
  WMCVS_VPE_NULL=0, 
  WMCVS_VPE_288K=288000,/*SVS*/
  WMCVS_VPE_384K= 384000,  /*SVS+*/
  WMCVS_VPE_499_2K=499200,/*NOM*/
  WMCVS_VPE_576K= 576000,  /*TURBO*/
} wmcpmdrv_mcvs_vpe_speedType;

#endif

/*q6 speed for MCVS in Khz*/
typedef enum
{
  WMCVS_Q6CLK_NULL=0, 
  WMCVS_Q6CLK_144K= 144000,/*SUB-SVS*/
  WMCVS_Q6CLK_288K= 288000,/*SUB-SVS*/
  WMCVS_Q6CLK_384K= 384000,/*SVS*/
  WMCVS_Q6CLK_576K= 576000,/*NOM*/
  WMCVS_Q6CLK_691_2K= 691200,/*TURBO*/
} wmcpmdrv_mcvs_q6clk_speedType;

/*Values in Mhz as SRCH needs in MHz while sending to MCVS multiply by 1000*/
typedef enum
{
  WMCVS_OFFLINECLK_NULL=0, 
  WMCVS_OFFLINECLK_72M= 72,/*SVS*/
  WMCVS_OFFLINECLK_144M= 144,/*NOM*/
} wmcpmdrv_mcvs_offlineclk_speedType;

typedef struct 
{
   /*# SHO fingers */
   uint8 num_aset_fingers[2];
      /*# SHO cells */
   uint8 num_sho_cells[2];
   /*number of neighbors*/
   uint8 num_neighbor_cells[2];
   /*Total number of fingers*/
   uint8 tot_num_of_fingers[2];
   /* total number of cells will be used for PULL in*/
   uint8 total_num_cells;
   /* total number of  Neighbor cells[NBCH + QSET] */
   uint8 total_num_neighbor_cells[2];
   /*TxD is present or not*/	  
   boolean status_txd[2];
   /*PCCPCH  info in DCH */
   boolean pccpch_enabled ;
}wmcpmdrv_mcvs_vpe_update_struct_type;

//#define FEATURE_WCDMA_MCVS

extern uint8 wmcpmdrv_int_receiver_cfg_bmask ;

/* As per Systems, For internal testing we have made Fingers for R99 and HS as 1 
   Later this should be cahnged to 2 and 3 respectively as per SDD. Confirm with Younggeun. */ 
#define WMCVS_NUM_SHO_CELLS_HS    1
#define WMCVS_PERCENT_UTIL_HIGH   85
#define WMCVS_PERCENT_UTIL_LOW    80
#define WMCVS_SRCH_PERIOD360      36
#define WMCVS_SRCH_PERIOD180      18

#ifdef FEATURE_WCDMA_DIMEPM_MCVS
/* If no TxD
    Num of cell + Num of fing >(16 -1)

If TxD
    Num of cell + Num of fing >(13 -1)
*/
#define WMCVS_VPE_PARAM_STATUS_R99_NOM() ( ((!wmcpmdrv_mcvs_vpe_update_params.status_txd[0]) \
                                                                            &&((wmcpmdrv_mcvs_vpe_update_params.num_aset_fingers[0] \
                                                                                  + wmcpmdrv_mcvs_vpe_update_params.num_sho_cells[0]) > 16 -DIMEPM_MARGIN))\
                                                                            ||\
                                                                            ((wmcpmdrv_mcvs_vpe_update_params.status_txd[0])\
                                                                            &&((wmcpmdrv_mcvs_vpe_update_params.num_aset_fingers[0] \
                                                                                  + wmcpmdrv_mcvs_vpe_update_params.num_sho_cells[0]) > 13 -DIMEPM_MARGIN)))

/*NO HS MCVS algo for DPM, MCPM sheet has NOM configuration*/
#define WMCVS_VPE_PARAM_STATUS_PULLIN() 0
#define WMCVS_VPE_PARAM_STATUS_R99_SVS_PLUS()   0
#define WMCVS_VPE_PARAM_STATUS_HS_SVS_PLUS_CARR(carr_idx) 0
#define WMCVS_VPE_PARAM_STATUS_HS_SVS_PLUS() 0
/*

When the following conditions are all satisfied, MCVS should request SVS clock in both SC/DC data scenarios: 
NoTD is present across all cells
Total cell count sum across all carriers is less than or equal to 2
Total finger count sum across all carriers is less than or equal to 12
Total cell count in QSET is less than or equal to 2

*/
#define WMCVS_VPE_PARAM_STATUS_HS_NOM()  ((wmcpmdrv_mcvs_vpe_update_params.status_txd[0] || wmcpmdrv_mcvs_vpe_update_params.status_txd[1] )\
                                                                          ||((wmcpmdrv_mcvs_vpe_update_params.num_sho_cells[0] + wmcpmdrv_mcvs_vpe_update_params.num_sho_cells[1]\
                                                                               +wmcpmdrv_mcvs_vpe_update_params.total_num_neighbor_cells[0] + wmcpmdrv_mcvs_vpe_update_params.total_num_neighbor_cells[1]) >2)\
                                                                          ||((wmcpmdrv_mcvs_vpe_update_params.tot_num_of_fingers[0] + wmcpmdrv_mcvs_vpe_update_params.tot_num_of_fingers[1] ) > 12)\
                                                                          ||(wmcpmdrv_mcvs_cme_mask & 4))

//#define WMCVS_VPE_PARAM_STATUS_HS_NOM() ((WMCVS_VPE_PARAM_STATUS_HS_NOM_CARR(0)) || (WMCVS_VPE_PARAM_STATUS_HS_NOM_CARR(1)))

/*For DC we need to bump MCVS to TURBO based on QICE/RAKE/Triage params*/
/*
Bit 0 set from CME i.e Qset has >=4 set in c0 + c1
Bit 1set from CME i.e Qset has  >  4 set in c0 + c1

Rxd      TD          Qset
---     ---        -----
 0        0                X
 0        1             >=4
 1        1             >=4
 1        0             >=5

1a.  Txd is ON && Qset >=4 cells
1b   RxD is ON  && Qset >=5 cells
1c.TxD ON
    1c.1.  Sho cells =5 and  sho fing >=17
    1c.2. Sho cells =6 and  sho fing >=16
    1c.3. Sho cells =7 and  sho fing >=14
    1c.4. Sho cells =8 and  sho fing >=13
 1d. TxD OFF &&  ((sho cells + sho fing) >=25)
*/
#define WMCVS_VPE_PARAM_STATUS_TURBO()  ((IS_DC_ACTIVE()) && (((wmcpmdrv_mcvs_cme_mask & 1 )\
                                         && (wmcpmdrv_mcvs_vpe_update_params.status_txd[0] ||wmcpmdrv_mcvs_vpe_update_params.status_txd[1]))/*1a*/\
                                          ||( (wmcpmdrv_mcvs_cme_mask & 2)\
                                          &&(IS_RXD_ACTIVE()))/*1b*/\
                                          ||(((wmcpmdrv_mcvs_vpe_update_params.status_txd[0] ||wmcpmdrv_mcvs_vpe_update_params.status_txd[1]))/*1c*/\
                                          && (((updated_num_sho_cells==5)\
                                          &&(updated_num_aset_fingers>=17))/*1c.1*/\
                                           ||((updated_num_sho_cells ==6)\
                                         &&(updated_num_aset_fingers >=16))/*1c.2*/\
                                           ||((updated_num_sho_cells==7)\
                                           &&(updated_num_aset_fingers >=14))/*1c.3*/\
                                           ||((updated_num_sho_cells ==8)\
                                            &&(updated_num_aset_fingers >=13))))/*1c.4*/\
                                           ||((!(wmcpmdrv_mcvs_vpe_update_params.status_txd[0] ||wmcpmdrv_mcvs_vpe_update_params.status_txd[1]))/*1d*/\
                                           &&(updated_num_sho_cells + updated_num_aset_fingers >=25))))

#else
/*If (2*NO_ASET_CELL+NO_ASET_FINGER+2*I_TD >=12 ) OR (TOTAL_NO_FINGER+2*I_TD >= 11) OR (NO_ASET >= 4) OR (NO_neighbor cell >2), OR PCCPCH is ON*/
#define WMCVS_VPE_PARAM_STATUS_R99_NOM()      ((((wmcpmdrv_mcvs_vpe_update_params.num_sho_cells[0] <<1)\
                                           + (wmcpmdrv_mcvs_vpe_update_params.num_aset_fingers[0])\
                                           + (wmcpmdrv_mcvs_vpe_update_params.status_txd[0]<<1) ) >= (12))\
                                           ||\
                                           (((wmcpmdrv_mcvs_vpe_update_params.tot_num_of_fingers[0])\
                                           + (wmcpmdrv_mcvs_vpe_update_params.status_txd[0]<<1)) >= (11))\
                                           ||\
                                           (wmcpmdrv_mcvs_vpe_update_params.num_sho_cells[0] >=4)\
                                           ||(wmcpmdrv_mcvs_vpe_update_params.num_neighbor_cells[0] > 2)\
                                           ||( wmcpmdrv_mcvs_vpe_update_params.pccpch_enabled))


/*
SVS +  switching condition for R99
1.TxD OFF with 
        1a. number of SHO cells =1  AND  number of fingers <= 12
        1b. number of SHO cells =2  AND  number of fingers <= 12
        1c. number of SHO cells =3  AND  number of fingers <= 11
        1d. number of SHO cells =4  AND  number of fingers <= 9
        
2 TxD ON
       2a. number of SHO cells =1  AND  number of fingers <= 12
       2b. number of SHO cells =2  AND  number of fingers <= 11
       2c. number of SHO cells =3  AND  number of fingers <= 9
       2d. number of SHO cells =4  AND  number of fingers <= 7

In short  
If (2*NO_ASET_CELL+NO_ASET_FINGER+2*I_TD <18)
                SVS_PLUS
ELSE
                NOM
*/
#define WMCVS_VPE_PARAM_STATUS_R99_SVS_PLUS() (((wmcpmdrv_mcvs_vpe_update_params.num_sho_cells[0] <<1)\
                                               +(wmcpmdrv_mcvs_vpe_update_params.num_aset_fingers[0])\
                                               + (wmcpmdrv_mcvs_vpe_update_params.status_txd[0]<<1)) < 18)


/*
SVS +  switching condition for HS
1.TxD OFF with 
        1a. number of SHO cells + RxD <=2  AND  number of fingers <= 12
        1b.number of SHO cells <=3  AND  number of fingers <= 10 AND  QIce <=2 cells

2 TxD ON and NO RxD
       2a. number of SHO cells =1  AND  number of fingers <= 10 AND  QIce <=2 cells
       2b  number of SHO cells =2  AND  number of fingers <= 8 AND  QIce <=2 cells
       
//wl1_cme_is_qice_cfg_vpe_high  
"	0th bit -> Indicating CME VPE State.
o	 i.e. 1 for High State , 0 for Low State.

"	1st bit -> Indicating whether or not we have 2 or lower cells in C0.
o	 i.e. 1 if we have greater than 2 cells in QSET. 0 if we have 2 or less than 2 cells in QSET.

"	2nd bit -> Indicating whether or not we have 2 or lower cells in C1. 
o	i.e. 1 if we have greater than 2 cells in QSET. 0 if we have 2 or less than 2 cells in QSET.


*/
  #define   WMCVS_VPE_PARAM_STATUS_HS_SVS_PLUS_CARR(carr_idx) ((((!wmcpmdrv_mcvs_vpe_update_params.status_txd[carr_idx])\
                                                                  && ((wmcpmdrv_mcvs_cme_mask & (0x1<<(carr_idx + WMCPMDRV_QSET_TXD_STATUS_BIT_SHIFT)))!= ((1+carr_idx) << WMCPMDRV_QSET_TXD_STATUS_BIT_SHIFT)))/*1*/\
                                                               &&((((wmcpmdrv_mcvs_vpe_update_params.num_sho_cells[carr_idx] +(IS_RXD_ACTIVE())) <= 2)/*1a*/\
                                                                    &&(wmcpmdrv_mcvs_vpe_update_params.num_aset_fingers[carr_idx] <=12))\
                                                                   ||((wmcpmdrv_mcvs_vpe_update_params.num_sho_cells[carr_idx] <= 3)/*1b*/\
                                                                     &&(wmcpmdrv_mcvs_vpe_update_params.tot_num_of_fingers[carr_idx] <=9)\
                                                                     &&(!(wmcpmdrv_mcvs_cme_mask &( 2<<carr_idx)) ))))\
                                                               ||\
                                                               (((wmcpmdrv_mcvs_vpe_update_params.status_txd[carr_idx] || (wmcpmdrv_mcvs_cme_mask & (0x1<<(carr_idx + WMCPMDRV_QSET_TXD_STATUS_BIT_SHIFT)))) && (IS_RXD_ACTIVE() == 0))/*2*/\
                                                                  &&(((wmcpmdrv_mcvs_vpe_update_params.num_sho_cells[carr_idx] == 1)/*2a*/\
                                                                      &&(wmcpmdrv_mcvs_vpe_update_params.num_aset_fingers[carr_idx] <=10)\
                                                                      &&(!(wmcpmdrv_mcvs_cme_mask &( 2<<carr_idx))))\
                                                                    ||((wmcpmdrv_mcvs_vpe_update_params.num_sho_cells[carr_idx] == 2)/*2b*/\
                                                                      &&(wmcpmdrv_mcvs_vpe_update_params.num_aset_fingers[carr_idx] <=8)\
                                                                      &&(!(wmcpmdrv_mcvs_cme_mask &( 2<<carr_idx)))))))


#define   WMCVS_VPE_PARAM_STATUS_HS_SVS_PLUS() (WMCVS_VPE_PARAM_STATUS_HS_SVS_PLUS_CARR(0) && WMCVS_VPE_PARAM_STATUS_HS_SVS_PLUS_CARR(1))

/*
NOM switching condition
1.if number of SHO cells is greater than 1 OR number of neighbor cell>2 OR PCCPCH is ON
2.TxD OFF with(both CME AND TRIAGE off)
         2a.  NO neighbor cells 
                  2a1.NO RXD
                      tot  number of fingers >=8  OR QIce (with  iteration >3 or cells >=2)
                  2a2  RXD ON
                      tot number of fingers >=8 OR QIce (with  iteration >3 or cells >=2)
         2b.  neighbor cells  or Total no of fingers > num of aset fingers.

3.TxD ON with  (either of Triage OR CME)
         3a.RXD OFF
                   tot number of fingers >=7 
         3b.RXD ON
                  tot number of fingers >=6 
         3c.QICE is ON and  eq off
	 3d.  neighbor cells  or Total no of fingers > num of aset fingers.

*/
#define WMCVS_VPE_PARAM_STATUS_HS_NOM_CARR(carr_idx)   (((wmcpmdrv_mcvs_vpe_update_params.num_sho_cells[carr_idx] > WMCVS_NUM_SHO_CELLS_HS)/*1*/\
	                                                  ||(wmcpmdrv_mcvs_vpe_update_params.num_neighbor_cells[carr_idx] > 2) || wmcpmdrv_mcvs_vpe_update_params.pccpch_enabled)\
                                      ||\
                                      (((!wmcpmdrv_mcvs_vpe_update_params.status_txd[carr_idx])\
                                              && ((wmcpmdrv_mcvs_cme_mask & (0x1<<(carr_idx + WMCPMDRV_QSET_TXD_STATUS_BIT_SHIFT)))!= ((1+carr_idx) << WMCPMDRV_QSET_TXD_STATUS_BIT_SHIFT)))/*2*/\
                                        &&(((wmcpmdrv_mcvs_vpe_update_params.num_neighbor_cells[carr_idx] ==0)/*2a*/\
                                             &&(((IS_RXD_ACTIVE() == 0)/*2a1*/\
                                                  &&((wmcpmdrv_mcvs_vpe_update_params.tot_num_of_fingers[carr_idx] >= (8))\
                                                    ||(wmcpmdrv_mcvs_cme_mask & 1)))\
                                             ||((IS_RXD_ACTIVE())/*2a2*/\
                                                 &&((wmcpmdrv_mcvs_vpe_update_params.tot_num_of_fingers[carr_idx] >= (8))\
                                                   ||(wmcpmdrv_mcvs_cme_mask & 1)))))\
                                        ||((wmcpmdrv_mcvs_vpe_update_params.num_neighbor_cells[carr_idx] > 0)/*2b*/\
                                            ||(wmcpmdrv_mcvs_vpe_update_params.tot_num_of_fingers[carr_idx] > wmcpmdrv_mcvs_vpe_update_params.num_aset_fingers[carr_idx]))))/*TxD off ends*/\
                                      ||\
                                      (((wmcpmdrv_mcvs_vpe_update_params.status_txd[carr_idx] || (wmcpmdrv_mcvs_cme_mask & (0x1<<(carr_idx + WMCPMDRV_QSET_TXD_STATUS_BIT_SHIFT)))))/*3*/\
                                        &&(((IS_RXD_ACTIVE() == 0)/*3a*/\
                                              &&(wmcpmdrv_mcvs_vpe_update_params.tot_num_of_fingers[carr_idx] >= (7)))\
                                            ||((IS_RXD_ACTIVE())/*3b*/\
                                                &&(wmcpmdrv_mcvs_vpe_update_params.tot_num_of_fingers[carr_idx] >= (6)))\
                                            ||((IS_QICE_ACTIVE()) && (!IS_EQ_ACTIVE()))/*3c*/\
                                            ||((wmcpmdrv_mcvs_vpe_update_params.num_neighbor_cells[carr_idx] > 0)\
                                               ||(wmcpmdrv_mcvs_vpe_update_params.tot_num_of_fingers[carr_idx] > wmcpmdrv_mcvs_vpe_update_params.num_aset_fingers[carr_idx])))/*3d*/))

#define WMCVS_VPE_PARAM_STATUS_HS_NOM() ((WMCVS_VPE_PARAM_STATUS_HS_NOM_CARR(0)) || (WMCVS_VPE_PARAM_STATUS_HS_NOM_CARR(1)))

#define WMCVS_VPE_PARAM_STATUS_TURBO()((wmcpmdrv_mcvs_vpe_update_params.num_sho_cells[0] > 4) || (wmcpmdrv_mcvs_vpe_update_params.num_sho_cells[1] > 4))

#define WMCVS_VPE_PARAM_STATUS_PULLIN()  ((wmcpmdrv_mcvs_vpe_update_params.total_num_cells > 4) )

#endif

#endif /*FEATURE_WCDMA_MCVS*/

/* These are bitmasks corresponding to wcdma_mpwrdrv_client_type just defined above. */
#define WMCPMDRV_CLIENT_MASK_STACK_ACTIVE               (0x1 << (int32)WCDMA_MPWRDRV_CLIENT_STACK_ACTIVE)
#define WMCPMDRV_CLIENT_MASK_DL_ACTIVE                  (0x1 << (int32)WCDMA_MPWRDRV_CLIENT_DL_ACTIVE)
#define WMCPMDRV_CLIENT_MASK_DRX                        (0x1 << (int32)WCDMA_MPWRDRV_CLIENT_DRX)
#define WMCPMDRV_CLIENT_MASK_W2G                        (0x1 << (int32)WCDMA_MPWRDRV_CLIENT_W2G)
#define WMCPMDRV_CLIENT_MASK_W2L                        (0x1 << (int32)WCDMA_MPWRDRV_CLIENT_W2L)
#define WMCPMDRV_CLIENT_MASK_R99_VOICE                  (0x1 << (int32)WCDMA_MPWRDRV_CLIENT_R99_VOICE)
#define WMCPMDRV_CLIENT_MASK_R99_DATA                   (0x1 << (int32)WCDMA_MPWRDRV_CLIENT_R99_DATA)
#define WMCPMDRV_CLIENT_MASK_HSDPA                      (0x1 << (int32)WCDMA_MPWRDRV_CLIENT_HSDPA)
#define WMCPMDRV_CLIENT_MASK_HSUPA                      (0x1 << (int32)WCDMA_MPWRDRV_CLIENT_HSUPA)
#define WMCPMDRV_CLIENT_MASK_HSPA                       (0x1 << (int32)WCDMA_MPWRDRV_CLIENT_HSPA)
#define WMCPMDRV_CLIENT_MASK_RXD                        (0x1 << (int32)WCDMA_MPWRDRV_CLIENT_RXD)
#define WMCPMDRV_CLIENT_MASK_EQ                         (0x1 << (int32)WCDMA_MPWRDRV_CLIENT_EQ)
#define WMCPMDRV_CLIENT_MASK_QICE                       (0x1 << (int32)WCDMA_MPWRDRV_CLIENT_QICE)
#define WMCPMDRV_CLIENT_MASK_MIMO                       (0x1 << (int32)WCDMA_MPWRDRV_CLIENT_MIMO)
#define WMCPMDRV_CLIENT_MASK_CM                         (0x1 << (int32)WCDMA_MPWRDRV_CLIENT_CM)
#define WMCPMDRV_CLIENT_MASK_PARAMS_UPDATE              (0x1 << (int32)WCDMA_MPWRDRV_PARMS_UPDATE_REQ)
#define WMCPMDRV_CLIENT_MASK_MAX_OFFLINE_CLK            (0x1 << (int32)WCDMA_MPWRDRV_MAX_OFFLINE_CLK_REQ)
#define WMCPMDRV_CLIENT_MASK_CDRX_LIGHT_SLEEP           (0x1 << (int32)WCDMA_CDRX_GO_TO_LIGHT_SLEEP_REQ)


/* bitmask shift for receiver config request parameters. */
#define WMCPM_RECEIVER_CONFIG_RXD_SHIFT    0 
#define WMCPM_RECEIVER_CONFIG_EQ_SHIFT     1   
#define WMCPM_RECEIVER_CONFIG_QICE_SHIFT   2   
#define WMCPM_RECEIVER_CONFIG_W_MIMO_SHIFT 3   

/* bitmask shift for neighbor meas request parameters. */
#define WMCPM_NEIGHBOR_MEAS_W2G_SHIFT      0 
#define WMCPM_NEIGHBOR_MEAS_W2L_SHIFT      1   

/* bitmask shift for dl datarate request parameters. */
/*
#define WMCPM_DL_R99DATA_SHIFT           0
#define WMCPM_DL_3p6_MBPS_SHIFT          1
#define WMCPM_DL_7p2_MBPS_SHIFT          2
#define WMCPM_DL_14p4_MBPS_SHIFT         3
#define WMCPM_DL_21_MBPS_SHIFT           4 
#define WMCPM_DL_28_MBPS_SHIFT           5
#define WMCPM_DL_42_MBPS_SHIFT           6
*/

/* bitmask shift for ul datarate request parameters. */
/* 
#define WMCPM_UL_R99DATA_SHIFT           0
#define WMCPM_UL_1P46_MBPS_SHIFT         1
#define WMCPM_UL_2MBPS_SHIFT             2
#define WMCPM_UL_2P93_MBPS_SHIFT         3
#define WMCPM_UL_5P76_MBPS_SHIFT         4
#define WMCPM_UL_11P5_MBPS_SHIFT         5
*/

/* No Shift needed for RF BW. Since its just 1 bit. Bit 0: WCDMA and Bit 1: DC_WCDMA */
extern rex_crit_sect_type   wmcpmdrv_lock;

#define WMCPMDRV_MUTEXLOCK() REX_ISR_LOCK(&wmcpmdrv_lock)
#define WMCPMDRV_MUTEXFREE() REX_ISR_UNLOCK(&wmcpmdrv_lock)

/*=========================================================================== 
                                  EXTERN GLOBALS 
  =========================================================================== */
extern boolean wmcpmdrv_mcpm_modem_cfg_called_by_W_req;
extern boolean wmcpmdrv_mcpm_modem_cfg_called_by_W_rel;

/*===========================================================================
FUNCTION     WCDMA_MPWRDRV_REQUEST_CLIENT_POWER_CONFIG

DESCRIPTION
  Wrapper function for W MCPM clients to request for power resourcess.

PARAMETERS
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
extern void wcdma_mpwrdrv_request_client_power_config(wmcpmdrv_client_type client);

/*===========================================================================
FUNCTION     WCDMA_MPWRDRV_RELINQUISH_CLIENT_POWER_CONFIG

DESCRIPTION
  Wrapper function for W MCPM clients to relinquish power resources.

PARAMETERS
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
extern void wcdma_mpwrdrv_relinquish_client_power_config(wmcpmdrv_client_type client);

/*===========================================================================
FUNCTION     wmcpmdrv_get_voice_call_status

DESCRIPTION
  Returns the status of Voice Call from the internal client bit mask

PARAMETERS
  None.

RETURN VALUE
  boolean (TRUE - if voice call is active / FALSE - otherwise)

SIDE EFFECTS
  None.
===========================================================================*/
boolean wmcpmdrv_get_voice_call_status(void);

/*===========================================================================
FUNCTION     wmcpmdrv_get_data_call_status

DESCRIPTION
  Returns the status of data Call from the internal client bit mask

PARAMETERS
  None.

RETURN VALUE
  boolean (TRUE - if data call is active / FALSE - otherwise)

SIDE EFFECTS
  None.
===========================================================================*/
boolean wmcpmdrv_get_data_call_status(void);

#ifdef FEATURE_WCDMA_HS_FACH_DRX
/*===========================================================================
FUNCTION     wmcpmdrv_get_r99_data_status

DESCRIPTION
  Returns the status of R99 data call

PARAMETERS
  None.

RETURN VALUE
  boolean (TRUE - if data call is active / FALSE - otherwise)

SIDE EFFECTS
  None.
===========================================================================*/
boolean wmcpmdrv_get_hs_data_status(void);

/*===========================================================================
FUNCTION     wmcpmdrv_get_hs_data_status

DESCRIPTION
  Returns the status of HS data call

PARAMETERS
  None.

RETURN VALUE
  boolean (TRUE - if data call is active / FALSE - otherwise)

SIDE EFFECTS
  None.
===========================================================================*/
boolean wmcpmdrv_get_r99_data_status(void);
#endif /* FEATURE_WCDMA_HS_FACH_DRX */

/*===========================================================================
FUNCTION     wmcpmdrv_get_w2g_client_status

DESCRIPTION
  Returns the status of W2G client from the internal client bit mask

PARAMETERS
  None.

RETURN VALUE
  boolean (TRUE - if W2G client is active / FALSE - otherwise)

SIDE EFFECTS
  None.
===========================================================================*/
boolean wmcpmdrv_get_w2g_client_status(void);

/*===========================================================================
FUNCTION     wmcpmdrv_get_w2l_client_status

DESCRIPTION
  Returns the status of W2L client from the internal client bit mask

PARAMETERS
  None.

RETURN VALUE
  boolean (TRUE - if W2G client is active / FALSE - otherwise)

SIDE EFFECTS
  None.
===========================================================================*/
boolean wmcpmdrv_get_w2l_client_status(void);

/*===========================================================================
FUNCTION     wmcpmdrv_get_ue_asleep_status

DESCRIPTION
  Returns the UE asleep status. Checks both DRX client status in the internal 
  bit mask and also the UE wakeup status variable.

PARAMETERS
  None.

RETURN VALUE
  boolean (TRUE - if UE is asleep and FALSE otherwise).

SIDE EFFECTS
  None.
===========================================================================*/
boolean wmcpmdrv_get_ue_asleep_status(void);

/*===========================================================================
FUNCTION     WMCPMDRV_REGISTER_BLK_RESTORE_CB

DESCRIPTION
  Wrapper function for registering a MCPM callback for restoring the block after power collapse.
PARAMETERS
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
void wmcpmdrv_register_blk_restore_cb(mcpm_modem_block_type block,mcpm_block_restore_callback_type cb);

/*===========================================================================
FUNCTION     WMCPMDRV_REGISTER_SLP_TIMELINE_CB

DESCRIPTION
  Wrapper function for registering a MCPM callback that tells WL1 to use the optimized
  or extended timeline.
PARAMETERS
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
void wmcpmdrv_register_slp_timeline_cb(mcpm_npa_slp_tmln_callback_type cb);

/*===========================================================================
FUNCTION     WMCPMDRV_REGISTER_CLK_CHANGE_CB

DESCRIPTION
  Wrapper function for registering a MCPM callback for change in any clock.
PARAMETERS
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
void wmcpmdrv_register_clk_change_cb(mcpm_modem_clk_type clk,mcpm_clk_speed_change_callback_type cb);

/*===========================================================================
FUNCTION     WMCPMDRV_SEND_OFFLINE_CLK_SPEED_TO_FW

DESCRIPTION
  This function takes the MCPM speed and converts to corresponding WFW offline clock
  speed state and send it to FW.  

DEPENDENCIES
  None.

PARAMETERS
  One of enums corresponding to LOW,MED or HIGH state.

RETURN VALUE
  None.

SIDE EFFECTS

===========================================================================*/
void wmcpmdrv_send_offline_clk_speed_to_fw(MCPM_ClkSpeedType mcpm_offline_clk_speed);

/*===========================================================================
FUNCTION     WMCPMDRV_GET_OFFLINE_CLOCK_SPEED

DESCRIPTION
  Wrapper function for retrieving offline clock speed from MCPM.
PARAMETERS
  None.

RETURN VALUE
  Offline clock speed of the type MCPM_ClkSpeedType.

SIDE EFFECTS
  None.
===========================================================================*/
MCPM_ClkSpeedType wmcpmdrv_get_offline_clock_speed(void);

/*===========================================================================
FUNCTION     WMCPMDRV_RF_FTM_MODE_REQ

DESCRIPTION
Wrapper function for honoring voice requestfrom RF to mcpm when in FTM mode.

PARAMETERS
TRUE or FALSE

RETURN VALUE
None

SIDE EFFECTS
None.
===========================================================================*/
void wmcpmdrv_rf_ftm_mode_req(boolean request);

#ifdef FEATURE_WCDMA_MCVS
/*===========================================================================
FUNCTION     WMCPMDRV_MCVS_UPDATE_SRCH_PERIODICITY

DESCRIPTION
function to return the new/updated srch period.
 
PARAMETERS
  None.

RETURN VALUE
updated srch period

SIDE EFFECTS
  None.
===========================================================================*/
uint16 wmcpmdrv_mcvs_update_srch_periodicity(void);

/*===========================================================================
FUNCTION     WMCPMDRV_MCVS_UPDATE_OFFLINE_CLK_TO_SRCH

DESCRIPTION
function to return the offline clk freq in Mhz.
 
PARAMETERS
  None.

RETURN VALUE
offline clk freq in Mhz

SIDE EFFECTS
  None.
===========================================================================*/

uint32 wmcpmdrv_mcvs_update_offline_clk_to_srch(void);
/*===========================================================================
FUNCTION     wmcpmdrv_mcvs_get_triage_param_update

DESCRIPTION
  Wrapper function for retrieving TxD Status from WL1

PARAMETERS
  None.

RETURN VALUE
  TxD_OFF/TxD_ON/TxD_DC

SIDE EFFECTS
  None.
===========================================================================*/
void wmcpmdrv_mcvs_get_triage_param_update(wmcpmdrv_mcvs_vpe_update_struct_type* vpe_params );

/*===========================================================================
FUNCTION     wmcpmdrv_mcvs_get_Pullin_cell_cnt

DESCRIPTION
  Gets the cell cnt from demod during pull in procedure.
 
PARAMETERS
 global VPE struct Address

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
void wmcpmdrv_mcvs_get_pullin_cell_cnt(wmcpmdrv_mcvs_vpe_update_struct_type *vpe_params);

/*===========================================================================
FUNCTION     WMCPMDRV_MCVS__OFFLINECLK_UPDATE

DESCRIPTION
function to send MCVS request when n ot already running at desired offline frequency
 
PARAMETERS
Offline clk frequency wanted

RETURN VALUE
None

SIDE EFFECTS
Blocking call ranges between 100usec to 6msec
can have latency when  MCVS need RPM change.
===========================================================================*/
void wmcpmdrv_mcvs_offlineclk_update(uint32 period,wmcpmdrv_mcvs_offlineclk_speedType offlineclk);

/*===========================================================================
FUNCTION     WMCPMDRV_MCVS__VPE_UPDATE

DESCRIPTION
function to send MCVS request when n ot already running at desired VPE frequency

PARAMETERS
VPE frequencywanted

RETURN VALUE
None

SIDE EFFECTS
Blocking call at max will take 100usec
===========================================================================*/
void wmcpmdrv_vpe_update_in_mcvs_param(wmcpmdrv_mcvs_vpe_speedType vpe_speed,mcvs_request_type req_type);

/*===========================================================================
FUNCTION     WMCPMDRV_MCVS_UPDATE_OFFLINE_CLK_TO_SRCH

DESCRIPTION
function to return the offline clk freq in Mhz.
 
PARAMETERS
  None.

RETURN VALUE
offline clk freq in Mhz

SIDE EFFECTS
  None.
===========================================================================*/
uint32 wmcpmdrv_mcvs_update_offline_clk_to_srch(void);

/*===========================================================================
FUNCTION     wmcpmdrv_vpe_update_in_mcpm_param

DESCRIPTION
Update  MCVS params to send alongwith MCPM request
 
PARAMETERS
vpe_speed

RETURN VALUE
NONE

SIDE EFFECTS
  None.
===========================================================================*/
void wmcpmdrv_vpe_update_in_mcpm_param(wmcpmdrv_mcvs_vpe_speedType vpe_speed,mcvs_request_type req_type);

/*===========================================================================
FUNCTION     wmcpmdrv_q6clk_update_in_mcvs_param

DESCRIPTION
function to send MCVS request when not already running at desired Q6 frequency
pure MCVS req

PARAMETERS
Q6 frequency wanted

RETURN VALUE
None

SIDE EFFECTS
Blocking call for x usec
===========================================================================*/
void wmcpmdrv_q6clk_update_in_mcvs_param(wmcpmdrv_mcvs_q6clk_speedType q6_speed,mcvs_request_type req_type);
/*===========================================================================
FUNCTION     wmcpmdrv_q6clk_update_in_mcpm_param

DESCRIPTION
function to send MCVS request when not already running at desired Q6 frequency
MCVS embedded in mcpm req.

PARAMETERS
Q6 frequency wanted

RETURN VALUE
None

SIDE EFFECTS
Blocking call for x usec
===========================================================================*/
void wmcpmdrv_q6clk_update_in_mcpm_param(wmcpmdrv_mcvs_q6clk_speedType q6_speed,mcvs_request_type req_type);
/*===========================================================================
FUNCTION     wmcpmdrv_get_cme_status

DESCRIPTION
  This function gets CME mask for QIce and updates MCVS internal global variable.

PARAMETERS
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
void wmcpmdrv_get_cme_status(void);

#endif /*FEATURE_WCDMA_MCVS*/

#endif /* FEATURE_WCDMA_MCPM_DRIVER */
#endif /* ifndef WMCPMDRV_H */
