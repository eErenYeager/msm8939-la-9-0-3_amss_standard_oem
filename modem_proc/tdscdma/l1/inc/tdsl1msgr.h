#ifndef TDSL1_MSGR_H
#define TDSL1_MSGR_H

/*===========================================================================

            W2LTE Message Router R E L A T E D   D E C L A R A T I O N S

DESCRIPTION 

EXTERNALIZED FUNCTIONS

INTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS

REFERENCES

Copyright (c) 2001-2010 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/tdscdma/l1/inc/tdsl1msgr.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
08/18/11   asm     Added support for RF MSGR messages
01/25/11   cxd     Merge MSGR queues for L1 task
06/02/10   stk     Initial Version. Added support for W2LTE measurements and reselection 
                   under FEATURE_WCDMA_ABSOLUTE_PRIORITY & FEATURE_WCDMA_TO_LTE

===========================================================================*/
#include "tdscdma_variation.h"
#include "customer.h"
#include "rex.h"
#include "queue.h"
#include <msgr.h>
#include <msgr_umid.h>  /* UMID definitions */
#include "tfw_sw_intf_msg.h"

#ifdef FEATURE_TDSCDMA_RF_SUPPORT
#include "rf_tdscdma_msg.h"
#endif

#ifdef TEST_FRAMEWORK
#error code not present
#endif

#include "gl1_hw.h"

#if defined(FEATURE_TDSCDMA_TO_LTE) && defined(FEATURE_TDSCDMA_ABSOLUTE_PRIORITY)
#include "lte_cphy_irat_meas_msg.h"

#define MSGR_MODULE_L1                 0x01
#define MSGR_WCDMA_L1                  MSGR_TECH_MODULE( MSGR_TECH_WCDMA, MSGR_MODULE_L1 )

typedef lte_cphy_irat_meas_cleanup_cnf_s wl1_msgr_w2l_cleanup_cnf;
typedef lte_cphy_irat_meas_meas_cnf_s wl1_msgr_w2l_meas_cnf;

#endif /* (FEATURE_TDSCDMA_TO_LTE) && defined(FEATURE_TDSCDMA_ABSOLUTE_PRIORITY) */

#include "tdsirat.h"

#include "wwan_coex_mgr.h"
#include "wcn_coex_mgr.h"
typedef cxm_freqid_info_ind_s tdsl1_msgr_cxm_freqid_ind;


typedef union
{
  /* Message Router Header field */
  msgr_hdr_s             msgr_hdr;
#if defined(FEATURE_TDSCDMA_TO_LTE) && defined(FEATURE_TDSCDMA_ABSOLUTE_PRIORITY)
  lte_cphy_irat_meas_cleanup_cnf_s cleanup_cnf;
  lte_cphy_irat_meas_search_cnf_s search_cnf;
  lte_cphy_irat_meas_meas_cnf_s meas_cnf;
    #if defined(FEATURE_TDSCDMA_IDLEINTERVAL_INFO_SUPPORT) || defined(FEATURE_TDSCDMA_DMO_SUPPORT)
	lte_cphy_irat_meas_timed_srch_meas_cnf_s timed_meas_cnf;
	#endif
#endif /* (FEATURE_TDSCDMA_TO_LTE) && defined(FEATURE_TDSCDMA_ABSOLUTE_PRIORITY) */
  tfw_srch_gap_detect_rsp_t gap_detect_rsp;
  tfw_srch_syncdl_midamble_detect_rsp_t syncdl_midamble_detect_rsp;
  //tdscsrchfw_abort_rsp_msg_t abort_rsp;
  tfw_measure_rscp_rsp_t rscp_rsp;
  tfw_measure_iscp_rsp_t iscp_rsp;
  tfw_measure_all_rscp_rsp_t rscp_all_rsp;
  tfw_cell_info_ind_t cell_info_ind;
  tfw_baton_handover_rsp_t fw_bho_rsp;
  tfw_srch_reacq_rsp_t srch_reacq_rsp;
  #ifdef FEATURE_JDS_OPT
  tfw_ts0_cell_rsp_t ts0_cell_rsp;
  tfw_nonts0_cell_rsp_t non_ts0_cell_rsp;
  #endif
  
#if 0  
  tfw_measure_unscheduled_stats_ind_t unscheduled_stats_ind;
#endif
  tfw_srch_power_scan_rsp_t power_scan_rsp;
  
  gsm_irat_acquisition_rsp_t gsm_acq_rsp; 
  gsm_irat_sch_burst_rsp_t gsm_sch_rsp;
  gsm_irat_meas_search_rsp_t gsm_meas_rsp;

//#ifdef FEATURE_TDSCDMA_PLT  
  tfw_state_cfg_rsp_t state_rsp;
//#endif
  tdsirat_msg_union_type irat_msg;
  
  tfw_ard_ind_t ard_ind ;

  #ifdef FEATURE_TDSCDMA_DSDS_QTA  /*FEATURE_QTA*/
  tfw_dsds_cleanup_ind_t dsds_qta_cleanup_ind;
  #endif
  
#ifdef FEATURE_BOLT_MODEM
  tfw_rx_config_update_rsp_t rx_config_update_rsp;
#endif

  tdsl1_msgr_cxm_freqid_ind freqid_ind;
  
  #if defined (FEATURE_DIMEPM_MODEM) && defined (FEATURE_TDSCDMA_WLAN_COEX)
  cxm_coex_active_policy_ind_s wlan_coex_policy_ind;
  cxm_coex_metrics_req_s       wlan_coex_metrics_req;
  tfw_wlan_txfrmdnl_ind_t  tfw_txfrmdnl_ind;
  #endif
  
} tdsl1_msgr_cmd_code_type;

typedef struct
{
  q_link_type link;

  tdsl1_msgr_cmd_code_type cmd_code;

} tdsl1_msgr_cmd_type;



/* SRCH FW Received Search CMD Message types*/
typedef enum
{
  TDSCDMA_FW_SRCH_GAP_DETECT_CMD_MSG,       /* 0 */
  TDSCDMA_FW_SRCH_SYNCDL_MIDAMBLE_DETECT_CMD_MSG,       /* 1 */ 

  TDSCDMA_FW_MEASURE_RSCP_CMD_MSG,
  TDSCDMA_FW_MEASURE_ISCP_CMD_MSG,
  TDSCDMA_FW_CELL_SERVING_JDCS_CMD_MSG,
  TDSCDMA_FW_BATON_HANDOVER_CMD_MSG,
  TDSCDMA_FW_STATE_CFG_CMD_MSG,

  TDSCDMA_FW_DL_MIDAMBLE_CFG_CMD_MSG,      /* 6 */
  TDSCDMA_FW_PICH_CFG_CMD_MSG,         /* 7 */
  TDSCDMA_FW_PCCPCH_CFG_CMD_MSG,       /* 8 */ 
  TDSCDMA_FW_SCCPCH_CFG_CMD_MSG,       /* 9 */
  TDSCDMA_FW_DL_DPCH_CFG_CMD_MSG,      /* 10 */
  TDSCDMA_FW_CCTRCH_CFG_CMD_MSG,       /* 11 */
  TDSCDMA_FW_RX_TIME_SYNC_CMD_MSG,

  TDSCDMA_FW_INVALID_CMD_MSG           /* 12 */
} tds_fw_sim_srch_dl_msg_enum_type;

typedef union
{
  /* Message Router Header field */
  msgr_hdr_s             msgr_hdr;

  tfw_measure_rscp_cmd_t            rscp_req;
  tfw_measure_iscp_cmd_t            iscp_req;

  tfw_dl_midamble_config_table_cmd_t    dl_ma_config_req;
  tfw_dl_pich_config_cmd_t              dl_pich_config_req;
  tfw_dl_pccpch_config_cmd_t            dl_pccpch_config_req;
  tfw_dl_sccpch_config_cmd_t            dl_sccpch_config_req;
  tfw_dl_dpch_config_cmd_t              dl_dpch_config_req;
  tfw_dl_ccTrCh_config_cmd_t            dl_cctrch_config_req;

} tds_fw_sim_msgr_cmd_code_type;

typedef struct
{
  q_link_type link;

  tds_fw_sim_msgr_cmd_code_type cmd_code;

} wsrch_fw_sim_msgr_cmd_type;

#endif /* TDSL1_MSGR_H */
