#===============================================================================
#
# Modem HDR API wrapper script
#
# GENERAL DESCRIPTION
#    build script to load API's for modem/hdr
#
# Copyright (c) 2012 by Qualcomm Technologies, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/hdr/build/hdr.api#1 $
#  $DateTime: 2015/01/27 06:42:19 $
#
#                      EDIT HISTORY FOR FILE
#                      
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#  
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 11/13/12   cnx     Removed modem folder.
# 09/11/12   cnx     Removed modem folder.
# 05/03/12   jgr     Add moved public files
# 11/11/10   cnx     Published modem/hdr/api as restricted API 
# 08/17/10   cnx     Updated the comments
# 07/10/10   enj     Initial revision
#
#===============================================================================
Import('env')

if env.PathExists('${BUILD_ROOT}/hdr'):
    env.Replace(HDR_ROOT = '${INC_ROOT}/hdr')
else:    
    env.Replace(HDR_ROOT = '${INC_ROOT}/modem/hdr')

env.PublishRestrictedApi('VIOLATIONS',[
        '${INC_ROOT}/mmcp/cust/inc',
        '${INC_ROOT}/mcs/hwio/inc',
        '${INC_ROOT}/geran/cust/inc',
        '${INC_ROOT}/mcs/hwio/inc/${CHIPSET}',
        '${INC_ROOT}/mmcp/nas/mm/inc',
        '${INC_ROOT}/mmcp/variation/inc',
        '${INC_ROOT}/myps/api',
        '${INC_ROOT}/fw/api/common',
        '${INC_ROOT}/geran/variation/inc',
        '${INC_ROOT}/mcs/cust/inc',
        '${INC_ROOT}/core/buses/api/icb',
        '${INC_ROOT}/mcs/variation/inc',
        '${INC_ROOT}/1x/cust/inc',
        '${INC_ROOT}/1x/variation/inc',
        '${INC_ROOT}/datamodem/3gpp2/ds707/inc',
        '${INC_ROOT}/datamodem/variation/inc',
        '${INC_ROOT}/datamodem/cust/inc',
        '${INC_ROOT}/datamodem/3gpp2/1xrlp/inc',
        '${INC_ROOT}/datamodem/3gpp2/doqos/inc',
        '${INC_ROOT}/datamodem/3gpp2/hdrdata/inc',
        '${INC_ROOT}/datamodem/3gpp2/dsmgr/inc',
        '${INC_ROOT}/datamodem/protocols/api',
        '${INC_ROOT}/datamodem/interface/atcop/inc',
        '${INC_ROOT}/datamodem/protocols/mip/inc',
        '${INC_ROOT}/datamodem/3gpp2/bcmcs/inc',
        '${INC_ROOT}/datamodem/interface/sysapi/inc',
        '${INC_ROOT}/datamodem/3gpp2/api',
        '${INC_ROOT}/datamodem/interface/atcop/inc',
        '${INC_ROOT}/datamodem/3gpp2/jcdma/inc',
        '${INC_ROOT}/datamodem/interface/api',
        ])

env.PublishPublicApi('HDR',       ["${HDR_ROOT}/api"])
env.PublishPublicApi('PUBLIC',    ["${HDR_ROOT}/api/public"])

#next line for with modem folder only
env.PublishPublicApi('HDR',       ["${HDR_ROOT}/api/public"])

env.LoadAPIUnits()
