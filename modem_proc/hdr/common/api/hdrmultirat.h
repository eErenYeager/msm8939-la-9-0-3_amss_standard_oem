#ifndef HDRMULTIRAT_H
#define HDRMULTIRAT_H

/*===========================================================================

                          H D R  M U L T I R A T
                           D E F I N I T I O N S

DESCRIPTION
  This module contains utilities and interface components that HDR core modules
  can use to interact with external entities in implementing multi-RAT support.

Copyright (c) 2013 - 2015 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/hdr/common/api/hdrmultirat.h#2 $ $DateTime: 2016/04/17 23:20:55 $ $Author: c_venup $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/16/15   wsh     Reduced stack usage to avoid overflow
06/04/14   arm     Changes to support DO to G QTA
03/15/14   wsh     Change to support T/T and new RF concurrency concern
03/14/14   rmv     Enabled D-LNA in DSDA depending on band combinations 
02/27/14   sat     Sending updated frequency list information to FW after wakeup
02/18/14   bb      Changes to use proper carrier index while retrieving RF power
01/24/14   bb      Changes to add more reasons to hdrmultirat_priority_type
                   to match with COEX priorty table indexes
12/06/13   arm     Supported Div chain frequency and power reporting.
11/26/13   arm     Handle SSSS->DSDA transition in online mode.
10/14/13   arm     Send freq id list again to FW in Reacq state.
08/19/13   arm     Merged DSDS feature for triton.
07/20/13   arm     Enabled band specific DTX.
07/16/13   arm     Added support for  FW DSDA Rx/Tx log packet logging.
06/13/13   arm     Fixed coex power bias value.
04/22/13   arm     Created module for HDR interaction Multi RAT scenario.

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "hdr_variation.h"
#include "hdrmsg.h"
#include "hdrdebug.h"
#include "hdrsrchtypes.h"

#include "msgr.h"
#include "comdef.h" /* Interface to the common definitions */
#include "cmd.h"    /* Interface to the command types*/
#include "sys.h"
#include "sys_v.h"

#include "cm.h"

#ifdef FEATURE_HDR_MODEM_COEXISTENCE_SW
#include "cxm.h"
#include "wwan_coex_mgr.h"
#include "hdrfw_msg.h"
#endif /* FEATURE_HDR_MODEM_COEXISTENCE_SW */

/* <EJECT> */ 
/*===========================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

 This section contains local definitions for constants, macros, typesdefs,
 and other items needed by this module.


===========================================================================*/

#ifdef FEATURE_HDR_MODEM_COEXISTENCE_SW

#define HDRMULTIRAT_COEX_PWR_TIMER_MS                  20
  /* Interval to report coex rx tx power to MCS (every 20 ms)
     in Idle and traffic state */
#define HDRMULTIRAT_REVERSE_LINK                       CXM_LNK_DRCTN_UL
  /* Uplink -> reverse link */  
#define HDRMULTIRAT_FORWARD_LINK                       CXM_LNK_DRCTN_DL  
  /* Downlink -> forward link */  
#define HDRMULTIRAT_COEX_PWR_BIAS                      100
  /* Bias subtracted from rx tx coex power sampled before going to sleep
     but reported after wakeup in db10*/

#define HDRMULTIRAT_MAX_NUM_CARRIERS                   HDRSRCH_MAX_DEMOD_CARRIERS_CNT

#define HDRMULTIRAT_DEFAULT_FREQ_ID                    0xFFFFFFF0

#define hdrmultirat_cxm_freq_id_list_type              cxm_freqid_info_ind_s
  /* cxm_freqid_info_ind_s msg is used by MCS CXM to give
     us freq id for every band-chan we use */

cxm_activity_table_s  hdrmultirat_cxm_priority_table[MAX_ACTIVITY_TIERS];
  /* Create a local copy of priority table to save it from MCS */

/* Enum for Activity tier based on priority  */
typedef enum
{
  /*! Highest Priority Tier: Should be used for activities which needs to be 
    protected from the other tech
    For Example: PLL Tuning, AGC Acquisition, W PICH Decode, Any G Tx for 2
      slots etc */
  HDRMULTIRAT_HIGH_PRIORITY_ASDIV = ACTIVITY_TIER_3,
  HDRMULTIRAT_HIGH_PRIORITY_5,
  HDRMULTIRAT_HIGH_PRIORITY_10 = ACTIVITY_TIER_10,

  /*! Second lowest Tier: Second highest priority for activities that can be 
    blanked when it conflicts with other tech's higher priority tier.
    e.g. G PCH, G BCCH, Any W Tx*/
  
  HDRMULTIRAT_HIGH_PRIORITY_15 = ACTIVITY_TIER_15,
  HDRMULTIRAT_LOWER_PRIORITY_20 = ACTIVITY_TIER_20,
  HDRMULTIRAT_LOWER_PRIORITY_25 = ACTIVITY_TIER_25,

  /* End of the list indicator */
  HDRMULTIRAT_MAX_PRIORITIES
}hdrmultirat_priority_type;

/* Structure used to store frequency information received/sent from MCS */
typedef struct
{
   rfm_device_enum_type                rf_device;
     /* The logical chain ID associated with the request */

   sys_channel_type                    channel[HDRMULTIRAT_MAX_NUM_CARRIERS];
	      /* Channel to tune the RF to */

   hdrsrch_demod_idx_type              demod_idx[HDRMULTIRAT_MAX_NUM_CARRIERS];
             /* carrier ID of the channels */

   uint8                               channel_cnt;
     /* Number of channels active*/

   uint32                              freqid[HDRMULTIRAT_MAX_NUM_CARRIERS];
     /* frequency id for each band-chan received from coex manager */

   int16                               coex_power[HDRMULTIRAT_MAX_NUM_CARRIERS];
     /* Coex filtered power stored for each carrier */
  
}hdrmultirat_band_chan_info_type;

/* Structure to store all coex related info */
typedef struct
{
   hdrmultirat_band_chan_info_type     rx;
     /* All  Rx info for dsda coex */

   hdrmultirat_band_chan_info_type     tx;
     /* All  Tx info for dsda coex */

   hdrmultirat_band_chan_info_type     div;
     /* Store div chain info for coex */

}hdrmultirat_coex_info_type;

#endif /* FEATURE_HDR_MODEM_COEXISTENCE_SW */

/* Enumerate all device modes */
typedef enum
{
  HDRMULTIRAT_SINGLE_STANDBY,

  HDRMULTIRAT_DUAL_SIM_DUAL_STANDBY = HDR_DSDS_STATE_ACTIVE,

  HDRMULTIRAT_DUAL_SIM_DUAL_ACTIVE = HDR_DSDA_STATE_ACTIVE

}hdrmultirat_mode_pref_enum_type;

#ifdef FEATURE_HDR_QTA
/* Enumerate QTA states */
typedef enum
{
  HDRMULTIRAT_QTA_START = HDRFW_QTA_START_CMD,
  HDRMULTIRAT_QTA_END = HDRFW_QTA_END_CMD
}hdrmultirat_qta_state_enum_type;
#endif /* FEATURE_HDR_QTA */

/* Structure to store elements common to all Hdr Multi Rat */
typedef struct
{
  hdrmultirat_mode_pref_enum_type      mode_pref;
    /* mode (SS/DSDA/DSDS mode) we are currently in */

#ifdef FEATURE_HDR_MODEM_COEXISTENCE_SW
  hdrmultirat_coex_info_type           coex_info;
    /* Band Chan & Power info needed for coex manager */ 
#endif /* FEATURE_HDR_MODEM_COEXISTENCE_SW */

#ifdef FEATURE_HDR_MODEM_COEXISTENCE_FW
  boolean                              dsda_rx_log_pkt_is_logged;
    /* Flag to indicate if fw rx dsda log pkt is being logged */

  boolean                              dsda_tx_log_pkt_is_logged;
    /* Flag to indicate if fw tx dsda log pkt is being logged */

  boolean                              is_mode_changed;
    /* Did mode change when FW was inactive? */

#endif /* FEATURE_HDR_MODEM_COEXISTENCE_FW */

  boolean                              frequency_update_pending;
    /* Flag to check after wake up if frequency update was pending */

  /*-------------------------------------------
     Critical section
  -------------------------------------------*/

  rex_crit_sect_type                   crit_sect; 
    /* Critical Section */ 

}hdrmultirat_struct_type;

LOCAL hdrmultirat_struct_type          hdrmultirat;

/* <EJECT> */ 
/*===========================================================================

                        FUNCTION  DEFINITIONS

===========================================================================*/

/*=========================================================================

FUNCTION     : HDRMULTIRAT_POWERUP_INIT

DESCRIPTION  : This function inits frequnce info structure.
               

DEPENDENCIES : None.

INPUT        : None.

RETURN VALUE : None

SIDE EFFECTS : None

=========================================================================*/
void hdrmultirat_powerup_init( void );

#ifdef FEATURE_HDR_MODEM_COEXISTENCE_SW
/*===========================================================================

FUNCTION HDRMULTIRAT_PROCESS_FREQ_CHANGE

DESCRIPTION
  This function sends band-chan change msg to MCS whenever it changes.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
void hdrmultirat_process_freq_change
(
   rfm_device_enum_type                  rf_device,
    /* The logical chain ID */

   const sys_channel_type                *channel,
     /* Channel to tune the RF to */

   cxm_tech_link_direction               direction,
     /* Direction - reverse/forward link */

   uint8                                 channel_cnt
     /* Num channels */
);
/*===========================================================================

FUNCTION HDRMULTIRAT_PROCESS_DIV_CHANGE

DESCRIPTION
  This function informs MCS of any change in diversity mdoe.

DEPENDENCIES
  None
 
INPUT 
 
  boolean div_mode:  If diversity was turned on or off.
 
  rfm_device_enum_type:   The logical chain ID
 
RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
void hdrmultirat_process_div_change
(
  boolean                               div_mode,
    /* Div ON/OFF? */
  
  rfm_device_enum_type                  rf_device
    /* The logical chain ID */

);
/*===========================================================================

FUNCTION HDRMULTIRAT_REPORT_COEX_POWER

DESCRIPTION
  This function reports coex rx - tx power to MCS every 20 ms

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
void hdrmultirat_report_coex_power 
( 
   boolean               save_only
     /* Just save the power or send it to MCS? */ 
);

/*===========================================================================

FUNCTION HDRMULTIRAT_REPORT_COEX_POWER_WITH_BIAS

DESCRIPTION
  This function sends the coes power we saved before goind to sleep + bias
  to MCS.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
void hdrmultirat_report_coex_power_with_bias( void );

/*=========================================================================

FUNCTION     : HDRMULTIRAT_GET_MODE_PREF

DESCRIPTION  : This function returns devide mode pref.
               

DEPENDENCIES : None.

INPUT        : None.

RETURN VALUE : Device mode -> SS/DSDS/DSDA

SIDE EFFECTS : None

=========================================================================*/

hdrmultirat_mode_pref_enum_type hdrmultirat_get_mode_pref ( void );
/*=========================================================================

FUNCTION     : HDRMULTIRAT_GET_CXM_PRIORITY_TABLE_CB

DESCRIPTION  : This function gets coex manager priority table and saves it.
               

DEPENDENCIES : None.

INPUT        : None.

RETURN VALUE : None

SIDE EFFECTS : None

=========================================================================*/

void hdrmultirat_get_cxm_priority_table_cb
( 
   cxm_tech_type  tech_id,

   cxm_activity_table_s *activity_tbl
);
/*=========================================================================

FUNCTION     : HDRMULTIRAT_GET_CXM_PRIORITY

DESCRIPTION  : This function returns priority for given priority level from 
               the table. 
               
DEPENDENCIES : None.

INPUT        : None.

RETURN VALUE : Priority

SIDE EFFECTS : None

=========================================================================*/

uint32 hdrmultirat_get_cxm_priority
(
   hdrmultirat_priority_type pri_level
);

/*===========================================================================

FUNCTION HDRMULTIRAT_DSDA_MODE_CHANGE

DESCRIPTION
  This function processes mode pref change to DSDA.

DEPENDENCIES
  None

PARAMETERS
  None

RETURN VALUE
  None
 
SIDE EFFECTS
  None

===========================================================================*/

void hdrmultirat_dsda_mode_change( void );

/*===========================================================================

FUNCTION HDRMULTIRAT_PROCESS_FREQ_ID_LIST

DESCRIPTION
  Process the fre id list rxd from coex manager.

DEPENDENCIES
  None

PARAMETERS
  frequency id list

RETURN VALUE
  None
 
SIDE EFFECTS
  None

===========================================================================*/

void hdrmultirat_process_freq_id_list 
( 
  hdrmultirat_cxm_freq_id_list_type* freqid_list
);

/* ===========================================================================

FUNCTION HDRMULTIRAT_PRIORITY_SWITCH

DESCRIPTION
  This function gets the priority for the activity level and then sends it to
  FW.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
void hdrmultirat_priority_switch
( 
   hdrmultirat_priority_type  priority_level
);

/* ===========================================================================

FUNCTION HDRMULTIRAT_CXM_ALLOWS_DTX

DESCRIPTION
  This function queries coex manager if its OK to enable dtx depending on
  C+G band class/channel combinations with G.

DEPENDENCIES
  None

RETURN VALUE
  TRUE if DTX is allowed
  
SIDE EFFECTS
  None

===========================================================================*/
boolean hdrmultirat_cxm_allows_dtx( void );

/* ===========================================================================

FUNCTION HDRMULTIRAT_CXM_ALLOWS_DLNA

DESCRIPTION
  This function queries coex manager if its OK to enable D-LNA depending on
  C+G band class/channel combinations with G.

DEPENDENCIES
  None

RETURN VALUE
  TRUE if D-LNA is allowed
  
SIDE EFFECTS
  None

===========================================================================*/
boolean hdrmultirat_cxm_allows_dlna( void );
#endif /* FEATURE_HDR_MODEM_COEXISTENCE_SW */

#ifdef FEATURE_HDR_MODEM_COEXISTENCE_FW
/*===========================================================================

FUNCTION HDRMULTIRAT_SEND_DSDX_ENABLE_CFG_MSG

DESCRIPTION
  Send DSDX enable msg to FW

DEPENDENCIES
  None

PARAMETERS
  State to send to FW

RETURN VALUE
  None
 
SIDE EFFECTS
  None

===========================================================================*/

void hdrmultirat_send_dsdx_enable_cfg_msg 
( 
   hdrmultirat_mode_pref_enum_type state 
);

/*===========================================================================

FUNCTION HDRMULTIRAT_SEND_DSDA_PRIORITY_CFG_MSG

DESCRIPTION
  Send DSDA priority msg to FW

DEPENDENCIES
  None

PARAMETERS
  rx & tx priority

RETURN VALUE
  None
 
SIDE EFFECTS
  None

===========================================================================*/

void hdrmultirat_send_dsda_priority_cfg_msg 
( 
  uint32 rx_priority,

  uint32 tx_priority
);
/*===========================================================================

FUNCTION HDRMULTIRAT_SEND_DSDX_FREQUENCY_CFG_MSG

DESCRIPTION
  Send DSDA frequency ID list to FW

DEPENDENCIES
  None

PARAMETERS
  None

RETURN VALUE
  None
 
SIDE EFFECTS
  None

===========================================================================*/
void hdrmultirat_send_dsdx_frequency_cfg_msg ( void );

#endif /* FEATURE_HDR_MODEM_COEXISTENCE_FW */

/*===========================================================================

FUNCTION HDRMULTIRAT_ENABLE_RX_FW_LOGGING

DESCRIPTION
  Send enable/disable rx logging msg to FW

DEPENDENCIES
  None

PARAMETERS
  None
 
RETURN VALUE
  None
 
SIDE EFFECTS
  None

===========================================================================*/

void hdrmultirat_enable_rx_fw_logging ( void );

/*===========================================================================

FUNCTION HDRMULTIRAT_ENABLE_TX_FW_LOGGING

DESCRIPTION
  Send enable TX logging msg to FW

DEPENDENCIES
  None

PARAMETERS
  None

RETURN VALUE
  None
 
SIDE EFFECTS
  None

===========================================================================*/

void hdrmultirat_enable_tx_fw_logging ( void );

/*===========================================================================

FUNCTION HDRMULTIRAT_DO_ATOMIC_SEARCH

DESCRIPTION
  This function retruns whether to do atomic search or not

DEPENDENCIES
  None

RETURN VALUE
  TRUE: if to do atomic search
  FALSE: if we can reschedule searches.

SIDE EFFECTS
  None

===========================================================================*/
boolean hdrmultirat_do_atomic_search( void );

/*=========================================================================

FUNCTION     : HDRMULTIRAT_SEND_MSG

DESCRIPTION  : This function sends the message to MSGR.

DEPENDENCIES : None

INPUTS       : msg - The message pointer
               len - The message length 

RETURN VALUE : None

SIDE EFFECTS : None

=========================================================================*/

void hdrmultirat_send_msg
( 
  void                       *msg,
    /* Message to send */

  uint32                     len
    /* The message lenght */
);

#ifdef FEATURE_HDR_DUAL_SIM
/*===========================================================================
FUNCTION HDRMULTIRAT_STANDBY_PREF_CHGD

DESCRIPTION
  This function is called to inform HDRSRCH of standby preference
  has changed. 

DEPENDENCIES
  This function must be called when HDR is offline.
 
PARAMETERS 
  ds_pref - standby preference
 
RETURN VALUE
  void.

SIDE EFFECTS
  None.
===========================================================================*/
void hdrmultirat_standby_pref_chgd
(
  sys_modem_dual_standby_pref_e_type ds_pref /* New standby preferece */
);

/*===========================================================================
FUNCTION HDRMULTIRAT_IS_DSDS_ENABLED

DESCRIPTION
  This function returns if DSDS is enabled currently

DEPENDENCIES
  None
 
PARAMETERS 
  None
 
RETURN VALUE
  True - if DSDS is enabled
  False - if DSDS is disabled

SIDE EFFECTS
  None.
===========================================================================*/
boolean hdrmultirat_is_dsds_enabled( void );

#endif /* FEATURE_HDR_DUAL_SIM */

#endif /* hdrmultirat_H */

