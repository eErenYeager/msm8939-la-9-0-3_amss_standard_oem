/*=============================================================================

  hdrsrchcom_sm.smt

Description:
  This file contains the machine generated source file for the state machine
  and/or state machine group specified in the file:
  hdrsrchcom_sm.smf

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################

=============================================================================*/

/* Include STM framework header */
#include "stm.h"

/* Begin machine generated code for state machine group: hdrsrchcom_stm_group */

/* State machine group entry/exit function prototypes */



/* State machine group signal mapper function prototype */



/* Table of state machines in group */
static stm_state_machine_type *hdrsrchcom_stm_group_sm_list[ 4 ] =
{
  &HDRSRCHAFC_SM,
  &HDRSRCHRFDIV_SM,
  &HDRSRCHARD_SM,
  &HDRSRCHRFTXD_SM,
};


/* Table of signals handled by the group's signal mapper */
/* No signals handled */


/* State machine group definition */
stm_group_type hdrsrchcom_stm_group =
{
  "hdrsrchcom_stm_group", /* state machine group name */
  NULL, /* signal mapping table */
  0, /* number of signals in mapping table */
  NULL, /* signal mapper function */
  hdrsrchcom_stm_group_sm_list, /* state machine table */
  4, /* number of state machines in table */
  NULL, /* wait function for group's signals */
  NULL, /* TCB of task that processes group's signals */
  HDRSRCH_INT_CMD_SIG, /* internal command queue signal */
  &hdrsrch_int_cmd_q, /* internal command queue */
  NULL, /* delayed internal command queue */
  NULL, /* group entry function */
  NULL, /* group exit function */
  hdrsrch_stm_debug_hook, /* debug hook function */
  NULL, /* group heap constructor fn */
  hdrsrchbm_group_heap_clean, /* group heap destructor fn */
  hdrsrchbm_malloc, /* group malloc fn */
  hdrsrchbm_free, /* group free fn */
  NULL, /* global group */
  NULL, /* user signal retrieval fn */
  NULL /* user signal handler fn */
};

/* End machine generated code for state machine group: hdrsrchcom_stm_group */

