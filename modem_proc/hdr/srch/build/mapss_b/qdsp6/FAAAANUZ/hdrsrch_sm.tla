###############################################################################
#
#    hdrsrch_sm.tla
#
# Description:
#   This file contains the machine generated state machine TLA info from the
#   file:  hdrsrch_sm.smf
#
#
###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################


# Begin machine generated TLA code for state machine: HDRSRCH_SM
# State machine:current state:input:next state                      fgcolor bgcolor

HDRSRCH:HDRSRCH_STM:HDRSRCH_AFC_OPEN_LOOP:HDRSRCH_STM        8739 0 00 0    @blue @white

HDRSRCH:HDRSRCH_STM:HDRSRCH_AFC_ACQ_STARTED:HDRSRCH_STM      8739 0 01 0    @blue @white

HDRSRCH:HDRSRCH_STM:HDRSRCH_AFC_ACQ_SUCCEEDED:HDRSRCH_STM    8739 0 02 0    @blue @white

HDRSRCH:HDRSRCH_STM:HDRSRCH_AFC_ACQ_FAILED:HDRSRCH_STM       8739 0 03 0    @blue @white

HDRSRCH:HDRSRCH_STM:HDRSRCH_AFC_CLOSE_LOOP:HDRSRCH_STM       8739 0 04 0    @blue @white

HDRSRCH:HDRSRCH_STM:HDRSRCH_AFC_TCXO_RELEASED:HDRSRCH_STM    8739 0 05 0    @blue @white

HDRSRCH:HDRSRCH_STM:HDRSRCH_DIV_ENABLED:HDRSRCH_STM          8739 0 06 0    @blue @white

HDRSRCH:HDRSRCH_STM:HDRSRCH_DIV_ENABLE_TIMEOUT:HDRSRCH_STM   8739 0 07 0    @blue @white



# End machine generated TLA code for state machine: HDRSRCH_SM

