/*=============================================================================

  hdrsrchrftxd_sm.smt

Description:
  This file contains the machine generated source file for the state machine
  and/or state machine group specified in the file:
  hdrsrchrftxd_sm.smf

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################

=============================================================================*/

/* Include STM framework header */
#include "stm.h"

/* Begin machine generated code for state machine: HDRSRCHRFTXD_SM */

/* Transition function prototypes */
static stm_state_type hdrsrchrftxd_rx_tf(void *);
static stm_state_type hdrsrchrftxd_grant_mode_tf(void *);
static stm_state_type hdrsrchrftxd_timer_expired_tf(void *);
static stm_state_type hdrsrchrftxd_ant_switched_tf(void *);
static stm_state_type hdrsrchrftxd_deactivate_tf(void *);
static stm_state_type hdrsrchrftxd_grant_mode_config_tf(void *);
static stm_state_type hdrsrchrftxd_inact_tf(void *);
static stm_state_type hdrsrchrftxd_sleep_tf(void *);
static stm_state_type hdrsrchrftxd_suspend_tf(void *);
static stm_state_type hdrsrchrftxd_access_tf(void *);
static stm_state_type hdrsrchrftxd_traffic_tf(void *);
static stm_state_type hdrsrchrftxd_mdsp_info_ready_tf(void *);
static stm_state_type hdrsrchrftxd_end_tx_tf(void *);
static stm_state_type hdrsrchrftxd_resume_tf(void *);
static stm_state_type hdrsrchrftxd_set_config_tf(void *);
static stm_state_type hdrsrchrftxd_next_acc_seq_tf(void *);
static stm_state_type hdrsrchrftxd_div_granted_tf(void *);
static stm_state_type hdrsrchrftxd_div_released_tf(void *);


/* State Machine entry/exit function prototypes */
static void hdrsrchrftxd_init(stm_group_type *group);


/* State entry/exit function prototypes */
static void hdrsrchrftxd_enter_inactive(void *payload, stm_state_type previous_state);
static void hdrsrchrftxd_enter_sleep(void *payload, stm_state_type previous_state);
static void hdrsrchrftxd_enter_rx(void *payload, stm_state_type previous_state);
static void hdrsrchrftxd_exit_rx(void *payload, stm_state_type previous_state);
static void hdrsrchrftxd_enter_rx_susp(void *payload, stm_state_type previous_state);
static void hdrsrchrftxd_enter_access(void *payload, stm_state_type previous_state);
static void hdrsrchrftxd_enter_access_susp(void *payload, stm_state_type previous_state);
static void hdrsrchrftxd_enter_traffic(void *payload, stm_state_type previous_state);
static void hdrsrchrftxd_exit_traffic(void *payload, stm_state_type previous_state);
static void hdrsrchrftxd_enter_traffic_susp(void *payload, stm_state_type previous_state);
static void hdrsrchrftxd_exit_traffic_susp(void *payload, stm_state_type previous_state);
static void hdrsrchrftxd_enter_qta(void *payload, stm_state_type previous_state);


/* Total number of states and inputs */
#define HDRSRCHRFTXD_SM_NUMBER_OF_STATES 9
#define HDRSRCHRFTXD_SM_NUMBER_OF_INPUTS 18


/* State enumeration */
enum
{
  HDRSRCHRFTXD_INACTIVE_STATE,
  HDRSRCHRFTXD_SLEEP_STATE,
  HDRSRCHRFTXD_RX_STATE,
  HDRSRCHRFTXD_RX_SUSP_STATE,
  HDRSRCHRFTXD_ACCESS_STATE,
  HDRSRCHRFTXD_ACCESS_SUSP_STATE,
  HDRSRCHRFTXD_TRAFFIC_STATE,
  HDRSRCHRFTXD_TRAFFIC_SUSP_STATE,
  HDRSRCHRFTXD_QTA_STATE,
};


/* State name, entry, exit table */
static const stm_state_array_type
  HDRSRCHRFTXD_SM_states[ HDRSRCHRFTXD_SM_NUMBER_OF_STATES ] =
{
  {"HDRSRCHRFTXD_INACTIVE_STATE", hdrsrchrftxd_enter_inactive, NULL},
  {"HDRSRCHRFTXD_SLEEP_STATE", hdrsrchrftxd_enter_sleep, NULL},
  {"HDRSRCHRFTXD_RX_STATE", hdrsrchrftxd_enter_rx, hdrsrchrftxd_exit_rx},
  {"HDRSRCHRFTXD_RX_SUSP_STATE", hdrsrchrftxd_enter_rx_susp, NULL},
  {"HDRSRCHRFTXD_ACCESS_STATE", hdrsrchrftxd_enter_access, NULL},
  {"HDRSRCHRFTXD_ACCESS_SUSP_STATE", hdrsrchrftxd_enter_access_susp, NULL},
  {"HDRSRCHRFTXD_TRAFFIC_STATE", hdrsrchrftxd_enter_traffic, hdrsrchrftxd_exit_traffic},
  {"HDRSRCHRFTXD_TRAFFIC_SUSP_STATE", hdrsrchrftxd_enter_traffic_susp, hdrsrchrftxd_exit_traffic_susp},
  {"HDRSRCHRFTXD_QTA_STATE", hdrsrchrftxd_enter_qta, NULL},
};


/* Input value, name table */
static const stm_input_array_type
  HDRSRCHRFTXD_SM_inputs[ HDRSRCHRFTXD_SM_NUMBER_OF_INPUTS ] =
{
  {(stm_input_type)HDRSRCHRFTXD_RX_CMD, "HDRSRCHRFTXD_RX_CMD"},
  {(stm_input_type)HDRSRCHRFTXD_GRANT_MODE_CMD, "HDRSRCHRFTXD_GRANT_MODE_CMD"},
  {(stm_input_type)HDRSRCHRFTXD_TIMER_EXPIRED_CMD, "HDRSRCHRFTXD_TIMER_EXPIRED_CMD"},
  {(stm_input_type)HDRSRCHRFTXD_ANT_SWITCHED_CMD, "HDRSRCHRFTXD_ANT_SWITCHED_CMD"},
  {(stm_input_type)HDRSRCHRFTXD_DEACTIVATE_CMD, "HDRSRCHRFTXD_DEACTIVATE_CMD"},
  {(stm_input_type)HDRSRCHRFTXD_GRANT_MODE_CONFIG_CMD, "HDRSRCHRFTXD_GRANT_MODE_CONFIG_CMD"},
  {(stm_input_type)HDRSRCHRFTXD_INACT_CMD, "HDRSRCHRFTXD_INACT_CMD"},
  {(stm_input_type)HDRSRCHRFTXD_SLEEP_CMD, "HDRSRCHRFTXD_SLEEP_CMD"},
  {(stm_input_type)HDRSRCHRFTXD_SUSPEND_CMD, "HDRSRCHRFTXD_SUSPEND_CMD"},
  {(stm_input_type)HDRSRCHRFTXD_ACCESS_CMD, "HDRSRCHRFTXD_ACCESS_CMD"},
  {(stm_input_type)HDRSRCHRFTXD_TRAFFIC_CMD, "HDRSRCHRFTXD_TRAFFIC_CMD"},
  {(stm_input_type)HDRSRCHRFTXD_MDSP_INFO_READY_CMD, "HDRSRCHRFTXD_MDSP_INFO_READY_CMD"},
  {(stm_input_type)HDRSRCHRFTXD_END_TX_CMD, "HDRSRCHRFTXD_END_TX_CMD"},
  {(stm_input_type)HDRSRCHRFTXD_RESUME_CMD, "HDRSRCHRFTXD_RESUME_CMD"},
  {(stm_input_type)HDRSRCHRFTXD_SET_CONFIG_CMD, "HDRSRCHRFTXD_SET_CONFIG_CMD"},
  {(stm_input_type)HDRSRCHRFTXD_NEXT_ACC_SEQ_CMD, "HDRSRCHRFTXD_NEXT_ACC_SEQ_CMD"},
  {(stm_input_type)HDRSRCHRFTXD_DIV_GRANTED_CMD, "HDRSRCHRFTXD_DIV_GRANTED_CMD"},
  {(stm_input_type)HDRSRCHRFTXD_DIV_RELEASED_CMD, "HDRSRCHRFTXD_DIV_RELEASED_CMD"},
};


/* Transition table */
static const stm_transition_fn_type
  HDRSRCHRFTXD_SM_transitions[ HDRSRCHRFTXD_SM_NUMBER_OF_STATES *  HDRSRCHRFTXD_SM_NUMBER_OF_INPUTS ] =
{
  /* Transition functions for state HDRSRCHRFTXD_INACTIVE_STATE */
    hdrsrchrftxd_rx_tf,    /* HDRSRCHRFTXD_RX_CMD */
    hdrsrchrftxd_grant_mode_tf,    /* HDRSRCHRFTXD_GRANT_MODE_CMD */
    hdrsrchrftxd_timer_expired_tf,    /* HDRSRCHRFTXD_TIMER_EXPIRED_CMD */
    hdrsrchrftxd_ant_switched_tf,    /* HDRSRCHRFTXD_ANT_SWITCHED_CMD */
    NULL,    /* HDRSRCHRFTXD_DEACTIVATE_CMD */
    NULL,    /* HDRSRCHRFTXD_GRANT_MODE_CONFIG_CMD */
    NULL,    /* HDRSRCHRFTXD_INACT_CMD */
    NULL,    /* HDRSRCHRFTXD_SLEEP_CMD */
    NULL,    /* HDRSRCHRFTXD_SUSPEND_CMD */
    NULL,    /* HDRSRCHRFTXD_ACCESS_CMD */
    NULL,    /* HDRSRCHRFTXD_TRAFFIC_CMD */
    NULL,    /* HDRSRCHRFTXD_MDSP_INFO_READY_CMD */
    NULL,    /* HDRSRCHRFTXD_END_TX_CMD */
    NULL,    /* HDRSRCHRFTXD_RESUME_CMD */
    NULL,    /* HDRSRCHRFTXD_SET_CONFIG_CMD */
    NULL,    /* HDRSRCHRFTXD_NEXT_ACC_SEQ_CMD */
    NULL,    /* HDRSRCHRFTXD_DIV_GRANTED_CMD */
    NULL,    /* HDRSRCHRFTXD_DIV_RELEASED_CMD */

  /* Transition functions for state HDRSRCHRFTXD_SLEEP_STATE */
    hdrsrchrftxd_rx_tf,    /* HDRSRCHRFTXD_RX_CMD */
    NULL,    /* HDRSRCHRFTXD_GRANT_MODE_CMD */
    hdrsrchrftxd_timer_expired_tf,    /* HDRSRCHRFTXD_TIMER_EXPIRED_CMD */
    hdrsrchrftxd_ant_switched_tf,    /* HDRSRCHRFTXD_ANT_SWITCHED_CMD */
    hdrsrchrftxd_deactivate_tf,    /* HDRSRCHRFTXD_DEACTIVATE_CMD */
    hdrsrchrftxd_grant_mode_config_tf,    /* HDRSRCHRFTXD_GRANT_MODE_CONFIG_CMD */
    NULL,    /* HDRSRCHRFTXD_INACT_CMD */
    NULL,    /* HDRSRCHRFTXD_SLEEP_CMD */
    NULL,    /* HDRSRCHRFTXD_SUSPEND_CMD */
    NULL,    /* HDRSRCHRFTXD_ACCESS_CMD */
    NULL,    /* HDRSRCHRFTXD_TRAFFIC_CMD */
    NULL,    /* HDRSRCHRFTXD_MDSP_INFO_READY_CMD */
    NULL,    /* HDRSRCHRFTXD_END_TX_CMD */
    NULL,    /* HDRSRCHRFTXD_RESUME_CMD */
    NULL,    /* HDRSRCHRFTXD_SET_CONFIG_CMD */
    NULL,    /* HDRSRCHRFTXD_NEXT_ACC_SEQ_CMD */
    NULL,    /* HDRSRCHRFTXD_DIV_GRANTED_CMD */
    NULL,    /* HDRSRCHRFTXD_DIV_RELEASED_CMD */

  /* Transition functions for state HDRSRCHRFTXD_RX_STATE */
    NULL,    /* HDRSRCHRFTXD_RX_CMD */
    hdrsrchrftxd_grant_mode_tf,    /* HDRSRCHRFTXD_GRANT_MODE_CMD */
    hdrsrchrftxd_timer_expired_tf,    /* HDRSRCHRFTXD_TIMER_EXPIRED_CMD */
    hdrsrchrftxd_ant_switched_tf,    /* HDRSRCHRFTXD_ANT_SWITCHED_CMD */
    hdrsrchrftxd_deactivate_tf,    /* HDRSRCHRFTXD_DEACTIVATE_CMD */
    hdrsrchrftxd_grant_mode_config_tf,    /* HDRSRCHRFTXD_GRANT_MODE_CONFIG_CMD */
    hdrsrchrftxd_inact_tf,    /* HDRSRCHRFTXD_INACT_CMD */
    hdrsrchrftxd_sleep_tf,    /* HDRSRCHRFTXD_SLEEP_CMD */
    hdrsrchrftxd_suspend_tf,    /* HDRSRCHRFTXD_SUSPEND_CMD */
    hdrsrchrftxd_access_tf,    /* HDRSRCHRFTXD_ACCESS_CMD */
    hdrsrchrftxd_traffic_tf,    /* HDRSRCHRFTXD_TRAFFIC_CMD */
    hdrsrchrftxd_mdsp_info_ready_tf,    /* HDRSRCHRFTXD_MDSP_INFO_READY_CMD */
    NULL,    /* HDRSRCHRFTXD_END_TX_CMD */
    NULL,    /* HDRSRCHRFTXD_RESUME_CMD */
    NULL,    /* HDRSRCHRFTXD_SET_CONFIG_CMD */
    NULL,    /* HDRSRCHRFTXD_NEXT_ACC_SEQ_CMD */
    NULL,    /* HDRSRCHRFTXD_DIV_GRANTED_CMD */
    NULL,    /* HDRSRCHRFTXD_DIV_RELEASED_CMD */

  /* Transition functions for state HDRSRCHRFTXD_RX_SUSP_STATE */
    NULL,    /* HDRSRCHRFTXD_RX_CMD */
    hdrsrchrftxd_grant_mode_tf,    /* HDRSRCHRFTXD_GRANT_MODE_CMD */
    hdrsrchrftxd_timer_expired_tf,    /* HDRSRCHRFTXD_TIMER_EXPIRED_CMD */
    hdrsrchrftxd_ant_switched_tf,    /* HDRSRCHRFTXD_ANT_SWITCHED_CMD */
    hdrsrchrftxd_deactivate_tf,    /* HDRSRCHRFTXD_DEACTIVATE_CMD */
    NULL,    /* HDRSRCHRFTXD_GRANT_MODE_CONFIG_CMD */
    NULL,    /* HDRSRCHRFTXD_INACT_CMD */
    NULL,    /* HDRSRCHRFTXD_SLEEP_CMD */
    NULL,    /* HDRSRCHRFTXD_SUSPEND_CMD */
    NULL,    /* HDRSRCHRFTXD_ACCESS_CMD */
    NULL,    /* HDRSRCHRFTXD_TRAFFIC_CMD */
    hdrsrchrftxd_mdsp_info_ready_tf,    /* HDRSRCHRFTXD_MDSP_INFO_READY_CMD */
    hdrsrchrftxd_end_tx_tf,    /* HDRSRCHRFTXD_END_TX_CMD */
    hdrsrchrftxd_resume_tf,    /* HDRSRCHRFTXD_RESUME_CMD */
    hdrsrchrftxd_set_config_tf,    /* HDRSRCHRFTXD_SET_CONFIG_CMD */
    NULL,    /* HDRSRCHRFTXD_NEXT_ACC_SEQ_CMD */
    NULL,    /* HDRSRCHRFTXD_DIV_GRANTED_CMD */
    NULL,    /* HDRSRCHRFTXD_DIV_RELEASED_CMD */

  /* Transition functions for state HDRSRCHRFTXD_ACCESS_STATE */
    NULL,    /* HDRSRCHRFTXD_RX_CMD */
    hdrsrchrftxd_grant_mode_tf,    /* HDRSRCHRFTXD_GRANT_MODE_CMD */
    hdrsrchrftxd_timer_expired_tf,    /* HDRSRCHRFTXD_TIMER_EXPIRED_CMD */
    hdrsrchrftxd_ant_switched_tf,    /* HDRSRCHRFTXD_ANT_SWITCHED_CMD */
    hdrsrchrftxd_deactivate_tf,    /* HDRSRCHRFTXD_DEACTIVATE_CMD */
    NULL,    /* HDRSRCHRFTXD_GRANT_MODE_CONFIG_CMD */
    NULL,    /* HDRSRCHRFTXD_INACT_CMD */
    NULL,    /* HDRSRCHRFTXD_SLEEP_CMD */
    hdrsrchrftxd_suspend_tf,    /* HDRSRCHRFTXD_SUSPEND_CMD */
    NULL,    /* HDRSRCHRFTXD_ACCESS_CMD */
    hdrsrchrftxd_traffic_tf,    /* HDRSRCHRFTXD_TRAFFIC_CMD */
    NULL,    /* HDRSRCHRFTXD_MDSP_INFO_READY_CMD */
    hdrsrchrftxd_end_tx_tf,    /* HDRSRCHRFTXD_END_TX_CMD */
    NULL,    /* HDRSRCHRFTXD_RESUME_CMD */
    hdrsrchrftxd_set_config_tf,    /* HDRSRCHRFTXD_SET_CONFIG_CMD */
    hdrsrchrftxd_next_acc_seq_tf,    /* HDRSRCHRFTXD_NEXT_ACC_SEQ_CMD */
    NULL,    /* HDRSRCHRFTXD_DIV_GRANTED_CMD */
    NULL,    /* HDRSRCHRFTXD_DIV_RELEASED_CMD */

  /* Transition functions for state HDRSRCHRFTXD_ACCESS_SUSP_STATE */
    NULL,    /* HDRSRCHRFTXD_RX_CMD */
    hdrsrchrftxd_grant_mode_tf,    /* HDRSRCHRFTXD_GRANT_MODE_CMD */
    hdrsrchrftxd_timer_expired_tf,    /* HDRSRCHRFTXD_TIMER_EXPIRED_CMD */
    hdrsrchrftxd_ant_switched_tf,    /* HDRSRCHRFTXD_ANT_SWITCHED_CMD */
    hdrsrchrftxd_deactivate_tf,    /* HDRSRCHRFTXD_DEACTIVATE_CMD */
    NULL,    /* HDRSRCHRFTXD_GRANT_MODE_CONFIG_CMD */
    NULL,    /* HDRSRCHRFTXD_INACT_CMD */
    NULL,    /* HDRSRCHRFTXD_SLEEP_CMD */
    NULL,    /* HDRSRCHRFTXD_SUSPEND_CMD */
    NULL,    /* HDRSRCHRFTXD_ACCESS_CMD */
    NULL,    /* HDRSRCHRFTXD_TRAFFIC_CMD */
    NULL,    /* HDRSRCHRFTXD_MDSP_INFO_READY_CMD */
    hdrsrchrftxd_end_tx_tf,    /* HDRSRCHRFTXD_END_TX_CMD */
    hdrsrchrftxd_resume_tf,    /* HDRSRCHRFTXD_RESUME_CMD */
    hdrsrchrftxd_set_config_tf,    /* HDRSRCHRFTXD_SET_CONFIG_CMD */
    hdrsrchrftxd_next_acc_seq_tf,    /* HDRSRCHRFTXD_NEXT_ACC_SEQ_CMD */
    NULL,    /* HDRSRCHRFTXD_DIV_GRANTED_CMD */
    NULL,    /* HDRSRCHRFTXD_DIV_RELEASED_CMD */

  /* Transition functions for state HDRSRCHRFTXD_TRAFFIC_STATE */
    NULL,    /* HDRSRCHRFTXD_RX_CMD */
    hdrsrchrftxd_grant_mode_tf,    /* HDRSRCHRFTXD_GRANT_MODE_CMD */
    hdrsrchrftxd_timer_expired_tf,    /* HDRSRCHRFTXD_TIMER_EXPIRED_CMD */
    hdrsrchrftxd_ant_switched_tf,    /* HDRSRCHRFTXD_ANT_SWITCHED_CMD */
    hdrsrchrftxd_deactivate_tf,    /* HDRSRCHRFTXD_DEACTIVATE_CMD */
    NULL,    /* HDRSRCHRFTXD_GRANT_MODE_CONFIG_CMD */
    NULL,    /* HDRSRCHRFTXD_INACT_CMD */
    NULL,    /* HDRSRCHRFTXD_SLEEP_CMD */
    hdrsrchrftxd_suspend_tf,    /* HDRSRCHRFTXD_SUSPEND_CMD */
    NULL,    /* HDRSRCHRFTXD_ACCESS_CMD */
    hdrsrchrftxd_traffic_tf,    /* HDRSRCHRFTXD_TRAFFIC_CMD */
    hdrsrchrftxd_mdsp_info_ready_tf,    /* HDRSRCHRFTXD_MDSP_INFO_READY_CMD */
    hdrsrchrftxd_end_tx_tf,    /* HDRSRCHRFTXD_END_TX_CMD */
    NULL,    /* HDRSRCHRFTXD_RESUME_CMD */
    hdrsrchrftxd_set_config_tf,    /* HDRSRCHRFTXD_SET_CONFIG_CMD */
    NULL,    /* HDRSRCHRFTXD_NEXT_ACC_SEQ_CMD */
    hdrsrchrftxd_div_granted_tf,    /* HDRSRCHRFTXD_DIV_GRANTED_CMD */
    hdrsrchrftxd_div_released_tf,    /* HDRSRCHRFTXD_DIV_RELEASED_CMD */

  /* Transition functions for state HDRSRCHRFTXD_TRAFFIC_SUSP_STATE */
    NULL,    /* HDRSRCHRFTXD_RX_CMD */
    hdrsrchrftxd_grant_mode_tf,    /* HDRSRCHRFTXD_GRANT_MODE_CMD */
    hdrsrchrftxd_timer_expired_tf,    /* HDRSRCHRFTXD_TIMER_EXPIRED_CMD */
    hdrsrchrftxd_ant_switched_tf,    /* HDRSRCHRFTXD_ANT_SWITCHED_CMD */
    hdrsrchrftxd_deactivate_tf,    /* HDRSRCHRFTXD_DEACTIVATE_CMD */
    NULL,    /* HDRSRCHRFTXD_GRANT_MODE_CONFIG_CMD */
    NULL,    /* HDRSRCHRFTXD_INACT_CMD */
    NULL,    /* HDRSRCHRFTXD_SLEEP_CMD */
    NULL,    /* HDRSRCHRFTXD_SUSPEND_CMD */
    NULL,    /* HDRSRCHRFTXD_ACCESS_CMD */
    NULL,    /* HDRSRCHRFTXD_TRAFFIC_CMD */
    hdrsrchrftxd_mdsp_info_ready_tf,    /* HDRSRCHRFTXD_MDSP_INFO_READY_CMD */
    hdrsrchrftxd_end_tx_tf,    /* HDRSRCHRFTXD_END_TX_CMD */
    hdrsrchrftxd_resume_tf,    /* HDRSRCHRFTXD_RESUME_CMD */
    hdrsrchrftxd_set_config_tf,    /* HDRSRCHRFTXD_SET_CONFIG_CMD */
    NULL,    /* HDRSRCHRFTXD_NEXT_ACC_SEQ_CMD */
    NULL,    /* HDRSRCHRFTXD_DIV_GRANTED_CMD */
    NULL,    /* HDRSRCHRFTXD_DIV_RELEASED_CMD */

  /* Transition functions for state HDRSRCHRFTXD_QTA_STATE */
    NULL,    /* HDRSRCHRFTXD_RX_CMD */
    hdrsrchrftxd_grant_mode_tf,    /* HDRSRCHRFTXD_GRANT_MODE_CMD */
    hdrsrchrftxd_timer_expired_tf,    /* HDRSRCHRFTXD_TIMER_EXPIRED_CMD */
    hdrsrchrftxd_ant_switched_tf,    /* HDRSRCHRFTXD_ANT_SWITCHED_CMD */
    hdrsrchrftxd_deactivate_tf,    /* HDRSRCHRFTXD_DEACTIVATE_CMD */
    NULL,    /* HDRSRCHRFTXD_GRANT_MODE_CONFIG_CMD */
    NULL,    /* HDRSRCHRFTXD_INACT_CMD */
    NULL,    /* HDRSRCHRFTXD_SLEEP_CMD */
    NULL,    /* HDRSRCHRFTXD_SUSPEND_CMD */
    NULL,    /* HDRSRCHRFTXD_ACCESS_CMD */
    NULL,    /* HDRSRCHRFTXD_TRAFFIC_CMD */
    hdrsrchrftxd_mdsp_info_ready_tf,    /* HDRSRCHRFTXD_MDSP_INFO_READY_CMD */
    hdrsrchrftxd_end_tx_tf,    /* HDRSRCHRFTXD_END_TX_CMD */
    hdrsrchrftxd_resume_tf,    /* HDRSRCHRFTXD_RESUME_CMD */
    hdrsrchrftxd_set_config_tf,    /* HDRSRCHRFTXD_SET_CONFIG_CMD */
    NULL,    /* HDRSRCHRFTXD_NEXT_ACC_SEQ_CMD */
    NULL,    /* HDRSRCHRFTXD_DIV_GRANTED_CMD */
    NULL,    /* HDRSRCHRFTXD_DIV_RELEASED_CMD */

};


/* State machine definition */
stm_state_machine_type HDRSRCHRFTXD_SM =
{
  "HDRSRCHRFTXD_SM", /* state machine name */
  12567, /* unique SM id (hash of name) */
  hdrsrchrftxd_init, /* state machine entry function */
  NULL, /* state machine exit function */
  HDRSRCHRFTXD_INACTIVE_STATE, /* state machine initial state */
  FALSE, /* state machine starts active? */
  HDRSRCHRFTXD_SM_NUMBER_OF_STATES,
  HDRSRCHRFTXD_SM_NUMBER_OF_INPUTS,
  HDRSRCHRFTXD_SM_states,
  HDRSRCHRFTXD_SM_inputs,
  HDRSRCHRFTXD_SM_transitions,
};

/* End machine generated code for state machine: HDRSRCHRFTXD_SM */

