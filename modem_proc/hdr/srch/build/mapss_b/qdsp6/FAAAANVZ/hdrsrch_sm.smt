/*=============================================================================

  hdrsrch_sm.smt

Description:
  This file contains the machine generated source file for the state machine
  and/or state machine group specified in the file:
  hdrsrch_sm.smf

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################

=============================================================================*/

/* Include STM framework header */
#include "stm.h"

/* Begin machine generated code for state machine: HDRSRCH_SM */

/* Transition function prototypes */
static stm_state_type hdrsrch_process_int_cmd_tf(void *);


/* State Machine entry/exit function prototypes */


/* State entry/exit function prototypes */


/* Total number of states and inputs */
#define HDRSRCH_SM_NUMBER_OF_STATES 1
#define HDRSRCH_SM_NUMBER_OF_INPUTS 8


/* State enumeration */
enum
{
  HDRSRCH_STM_STATE,
};


/* State name, entry, exit table */
static const stm_state_array_type
  HDRSRCH_SM_states[ HDRSRCH_SM_NUMBER_OF_STATES ] =
{
  {"HDRSRCH_STM_STATE", NULL, NULL},
};


/* Input value, name table */
static const stm_input_array_type
  HDRSRCH_SM_inputs[ HDRSRCH_SM_NUMBER_OF_INPUTS ] =
{
  {(stm_input_type)HDRSRCH_AFC_OPEN_LOOP_CMD, "HDRSRCH_AFC_OPEN_LOOP_CMD"},
  {(stm_input_type)HDRSRCH_AFC_ACQ_STARTED_CMD, "HDRSRCH_AFC_ACQ_STARTED_CMD"},
  {(stm_input_type)HDRSRCH_AFC_ACQ_SUCCEEDED_CMD, "HDRSRCH_AFC_ACQ_SUCCEEDED_CMD"},
  {(stm_input_type)HDRSRCH_AFC_ACQ_FAILED_CMD, "HDRSRCH_AFC_ACQ_FAILED_CMD"},
  {(stm_input_type)HDRSRCH_AFC_CLOSE_LOOP_CMD, "HDRSRCH_AFC_CLOSE_LOOP_CMD"},
  {(stm_input_type)HDRSRCH_AFC_TCXO_RELEASED_CMD, "HDRSRCH_AFC_TCXO_RELEASED_CMD"},
  {(stm_input_type)HDRSRCH_DIV_ENABLED_CMD, "HDRSRCH_DIV_ENABLED_CMD"},
  {(stm_input_type)HDRSRCH_DIV_ENABLE_TIMEOUT_CMD, "HDRSRCH_DIV_ENABLE_TIMEOUT_CMD"},
};


/* Transition table */
static const stm_transition_fn_type
  HDRSRCH_SM_transitions[ HDRSRCH_SM_NUMBER_OF_STATES *  HDRSRCH_SM_NUMBER_OF_INPUTS ] =
{
  /* Transition functions for state HDRSRCH_STM_STATE */
    hdrsrch_process_int_cmd_tf,    /* HDRSRCH_AFC_OPEN_LOOP_CMD */
    hdrsrch_process_int_cmd_tf,    /* HDRSRCH_AFC_ACQ_STARTED_CMD */
    hdrsrch_process_int_cmd_tf,    /* HDRSRCH_AFC_ACQ_SUCCEEDED_CMD */
    hdrsrch_process_int_cmd_tf,    /* HDRSRCH_AFC_ACQ_FAILED_CMD */
    hdrsrch_process_int_cmd_tf,    /* HDRSRCH_AFC_CLOSE_LOOP_CMD */
    hdrsrch_process_int_cmd_tf,    /* HDRSRCH_AFC_TCXO_RELEASED_CMD */
    hdrsrch_process_int_cmd_tf,    /* HDRSRCH_DIV_ENABLED_CMD */
    hdrsrch_process_int_cmd_tf,    /* HDRSRCH_DIV_ENABLE_TIMEOUT_CMD */

};


/* State machine definition */
stm_state_machine_type HDRSRCH_SM =
{
  "HDRSRCH_SM", /* state machine name */
  34617, /* unique SM id (hash of name) */
  NULL, /* state machine entry function */
  NULL, /* state machine exit function */
  HDRSRCH_STM_STATE, /* state machine initial state */
  TRUE, /* state machine starts active? */
  HDRSRCH_SM_NUMBER_OF_STATES,
  HDRSRCH_SM_NUMBER_OF_INPUTS,
  HDRSRCH_SM_states,
  HDRSRCH_SM_inputs,
  HDRSRCH_SM_transitions,
};

/* End machine generated code for state machine: HDRSRCH_SM */
/* Begin machine generated code for state machine group: hdrsrch_stm_group */

/* State machine group entry/exit function prototypes */



/* State machine group signal mapper function prototype */



/* Table of state machines in group */
static stm_state_machine_type *hdrsrch_stm_group_sm_list[ 1 ] =
{
  &HDRSRCH_SM,
};


/* Table of signals handled by the group's signal mapper */
/* No signals handled */


/* State machine group definition */
stm_group_type hdrsrch_stm_group =
{
  "hdrsrch_stm_group", /* state machine group name */
  NULL, /* signal mapping table */
  0, /* number of signals in mapping table */
  NULL, /* signal mapper function */
  hdrsrch_stm_group_sm_list, /* state machine table */
  1, /* number of state machines in table */
  NULL, /* wait function for group's signals */
  NULL, /* TCB of task that processes group's signals */
  HDRSRCH_INT_CMD_SIG, /* internal command queue signal */
  &hdrsrch_int_cmd_q, /* internal command queue */
  NULL, /* delayed internal command queue */
  NULL, /* group entry function */
  NULL, /* group exit function */
  hdrsrch_stm_debug_hook, /* debug hook function */
  NULL, /* group heap constructor fn */
  hdrsrchbm_group_heap_clean, /* group heap destructor fn */
  hdrsrchbm_malloc, /* group malloc fn */
  hdrsrchbm_free, /* group free fn */
  &hdrsrchcom_stm_group, /* global group */
  NULL, /* user signal retrieval fn */
  NULL /* user signal handler fn */
};

/* End machine generated code for state machine group: hdrsrch_stm_group */

