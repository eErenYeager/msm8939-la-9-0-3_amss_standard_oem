/*=============================================================================

  hdrsrchafc_sm.smt

Description:
  This file contains the machine generated source file for the state machine
  and/or state machine group specified in the file:
  hdrsrchafc_sm.smf

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################

=============================================================================*/

/* Include STM framework header */
#include "stm.h"

/* Begin machine generated code for state machine: HDRSRCHAFC_SM */

/* Transition function prototypes */
static stm_state_type hdrsrchafc_cancel_pending_req_tf(void *);
static stm_state_type hdrsrchafc_request_ol_tf(void *);
static stm_state_type hdrsrchafc_st_fast_acq_tf(void *);
static stm_state_type hdrsrchafc_st_grant_tf(void *);
static stm_state_type hdrsrchafc_st_deny_tf(void *);
static stm_state_type hdrsrchafc_st_set_chan_tf(void *);
static stm_state_type hdrsrchafc_ol_deactivate_tf(void *);
static stm_state_type hdrsrchafc_ol_to_cl_tf(void *);
static stm_state_type hdrsrchafc_ol_start_acq_tf(void *);
static stm_state_type hdrsrchafc_change_restriction_tf(void *);
static stm_state_type hdrsrchafc_ol_set_chan_tf(void *);
static stm_state_type hdrsrchafc_force_rot_trk_tf(void *);
static stm_state_type hdrsrchafc_deactivate_fll_tf(void *);
static stm_state_type hdrsrchafc_acq_to_ol_tf(void *);
static stm_state_type hdrsrchafc_v_change_restriction_tf(void *);
static stm_state_type hdrsrchafc_v_acq_rsp_tf(void *);
static stm_state_type hdrsrchafc_acq_timer_expire_tf(void *);
static stm_state_type hdrsrchafc_r_change_restriction_tf(void *);
static stm_state_type hdrsrchafc_r_acq_rsp_tf(void *);
static stm_state_type hdrsrchafc_r_loop_timer_expire_tf(void *);
static stm_state_type hdrsrchafc_stop_fll_trk_tf(void *);
static stm_state_type hdrsrchafc_xo_trk_timer_expire_tf(void *);
static stm_state_type hdrsrchafc_trk_start_acq_tf(void *);
static stm_state_type hdrsrchafc_xo_trk_change_restriction_tf(void *);
static stm_state_type hdrsrchafc_cl_set_chan_tf(void *);
static stm_state_type hdrsrchafc_rpush_tf(void *);
static stm_state_type hdrsrchafc_update_rgs_tf(void *);
static stm_state_type hdrsrchafc_v_trk_timer_expire_tf(void *);
static stm_state_type hdrsrchafc_rv_trk_timer_expire_tf(void *);
static stm_state_type hdrsrchafc_rv_trk_change_restriction_tf(void *);
static stm_state_type hdrsrchafc_ofs_release_tcxo_tf(void *);
static stm_state_type hdrsrchafc_deactivate_tf(void *);
static stm_state_type hdrsrchafc_request_continue_tracking_tf(void *);
static stm_state_type hdrsrchafc_grant_tf(void *);
static stm_state_type hdrsrchafc_deny_tf(void *);


/* State Machine entry/exit function prototypes */


/* State entry/exit function prototypes */
static void hdrsrchafc_enter_inactive(void *payload, stm_state_type previous_state);
static void hdrsrchafc_enter_open_loop(void *payload, stm_state_type previous_state);
static void hdrsrchafc_exit_open_loop(void *payload, stm_state_type previous_state);
static void hdrsrchafc_enter_v_acq(void *payload, stm_state_type previous_state);
static void hdrsrchafc_enter_r_acq(void *payload, stm_state_type previous_state);
static void hdrsrchafc_enter_r_loop(void *payload, stm_state_type previous_state);
static void hdrsrchafc_enter_xo_trk(void *payload, stm_state_type previous_state);
static void hdrsrchafc_enter_v_trk(void *payload, stm_state_type previous_state);
static void hdrsrchafc_enter_rv_trk(void *payload, stm_state_type previous_state);
static void hdrsrchafc_enter_ofs(void *payload, stm_state_type previous_state);
static void hdrsrchafc_enter_stopped(void *payload, stm_state_type previous_state);


/* Total number of states and inputs */
#define HDRSRCHAFC_SM_NUMBER_OF_STATES 10
#define HDRSRCHAFC_SM_NUMBER_OF_INPUTS 17


/* State enumeration */
enum
{
  HDRSRCHAFC_INACTIVE_STATE,
  HDRSRCHAFC_OPEN_LOOP_STATE,
  HDRSRCHAFC_V_ACQ_STATE,
  HDRSRCHAFC_R_ACQ_STATE,
  HDRSRCHAFC_R_LOOP_STATE,
  HDRSRCHAFC_XO_TRK_STATE,
  HDRSRCHAFC_V_TRK_STATE,
  HDRSRCHAFC_RV_TRK_STATE,
  HDRSRCHAFC_OFS_STATE,
  HDRSRCHAFC_STOPPED_STATE,
};


/* State name, entry, exit table */
static const stm_state_array_type
  HDRSRCHAFC_SM_states[ HDRSRCHAFC_SM_NUMBER_OF_STATES ] =
{
  {"HDRSRCHAFC_INACTIVE_STATE", hdrsrchafc_enter_inactive, NULL},
  {"HDRSRCHAFC_OPEN_LOOP_STATE", hdrsrchafc_enter_open_loop, hdrsrchafc_exit_open_loop},
  {"HDRSRCHAFC_V_ACQ_STATE", hdrsrchafc_enter_v_acq, NULL},
  {"HDRSRCHAFC_R_ACQ_STATE", hdrsrchafc_enter_r_acq, NULL},
  {"HDRSRCHAFC_R_LOOP_STATE", hdrsrchafc_enter_r_loop, NULL},
  {"HDRSRCHAFC_XO_TRK_STATE", hdrsrchafc_enter_xo_trk, NULL},
  {"HDRSRCHAFC_V_TRK_STATE", hdrsrchafc_enter_v_trk, NULL},
  {"HDRSRCHAFC_RV_TRK_STATE", hdrsrchafc_enter_rv_trk, NULL},
  {"HDRSRCHAFC_OFS_STATE", hdrsrchafc_enter_ofs, NULL},
  {"HDRSRCHAFC_STOPPED_STATE", hdrsrchafc_enter_stopped, NULL},
};


/* Input value, name table */
static const stm_input_array_type
  HDRSRCHAFC_SM_inputs[ HDRSRCHAFC_SM_NUMBER_OF_INPUTS ] =
{
  {(stm_input_type)HDRSRCHAFC_DEACTIVATE_CMD, "HDRSRCHAFC_DEACTIVATE_CMD"},
  {(stm_input_type)HDRSRCHAFC_OPEN_LOOP_CMD, "HDRSRCHAFC_OPEN_LOOP_CMD"},
  {(stm_input_type)HDRSRCHAFC_FAST_ACQ_CMD, "HDRSRCHAFC_FAST_ACQ_CMD"},
  {(stm_input_type)HDRSRCHAFC_GRANT_CMD, "HDRSRCHAFC_GRANT_CMD"},
  {(stm_input_type)HDRSRCHAFC_DENY_CMD, "HDRSRCHAFC_DENY_CMD"},
  {(stm_input_type)HDRSRCHAFC_SET_CHAN_CMD, "HDRSRCHAFC_SET_CHAN_CMD"},
  {(stm_input_type)HDRSRCHAFC_CLOSE_LOOP_CMD, "HDRSRCHAFC_CLOSE_LOOP_CMD"},
  {(stm_input_type)HDRSRCHAFC_START_ACQ_CMD, "HDRSRCHAFC_START_ACQ_CMD"},
  {(stm_input_type)HDRSRCHAFC_CHANGE_RESTR_CMD, "HDRSRCHAFC_CHANGE_RESTR_CMD"},
  {(stm_input_type)HDRSRCHAFC_FORCE_R_TRK_CMD, "HDRSRCHAFC_FORCE_R_TRK_CMD"},
  {(stm_input_type)HDRSRCHAFC_ACQ_RSP_CMD, "HDRSRCHAFC_ACQ_RSP_CMD"},
  {(stm_input_type)HDRSRCHAFC_TIMER_CMD, "HDRSRCHAFC_TIMER_CMD"},
  {(stm_input_type)HDRSRCHAFC_STOP_CMD, "HDRSRCHAFC_STOP_CMD"},
  {(stm_input_type)HDRSRCHAFC_RPUSH_CMD, "HDRSRCHAFC_RPUSH_CMD"},
  {(stm_input_type)HDRSRCHAFC_UPDATE_RGS_CMD, "HDRSRCHAFC_UPDATE_RGS_CMD"},
  {(stm_input_type)HDRSRCHAFC_RELEASE_CMD, "HDRSRCHAFC_RELEASE_CMD"},
  {(stm_input_type)HDRSRCHAFC_CONTINUE_TRK_CMD, "HDRSRCHAFC_CONTINUE_TRK_CMD"},
};


/* Transition table */
static const stm_transition_fn_type
  HDRSRCHAFC_SM_transitions[ HDRSRCHAFC_SM_NUMBER_OF_STATES *  HDRSRCHAFC_SM_NUMBER_OF_INPUTS ] =
{
  /* Transition functions for state HDRSRCHAFC_INACTIVE_STATE */
    hdrsrchafc_cancel_pending_req_tf,    /* HDRSRCHAFC_DEACTIVATE_CMD */
    hdrsrchafc_request_ol_tf,    /* HDRSRCHAFC_OPEN_LOOP_CMD */
    hdrsrchafc_st_fast_acq_tf,    /* HDRSRCHAFC_FAST_ACQ_CMD */
    hdrsrchafc_st_grant_tf,    /* HDRSRCHAFC_GRANT_CMD */
    hdrsrchafc_st_deny_tf,    /* HDRSRCHAFC_DENY_CMD */
    hdrsrchafc_st_set_chan_tf,    /* HDRSRCHAFC_SET_CHAN_CMD */
    NULL,    /* HDRSRCHAFC_CLOSE_LOOP_CMD */
    NULL,    /* HDRSRCHAFC_START_ACQ_CMD */
    NULL,    /* HDRSRCHAFC_CHANGE_RESTR_CMD */
    NULL,    /* HDRSRCHAFC_FORCE_R_TRK_CMD */
    NULL,    /* HDRSRCHAFC_ACQ_RSP_CMD */
    NULL,    /* HDRSRCHAFC_TIMER_CMD */
    NULL,    /* HDRSRCHAFC_STOP_CMD */
    NULL,    /* HDRSRCHAFC_RPUSH_CMD */
    NULL,    /* HDRSRCHAFC_UPDATE_RGS_CMD */
    NULL,    /* HDRSRCHAFC_RELEASE_CMD */
    NULL,    /* HDRSRCHAFC_CONTINUE_TRK_CMD */

  /* Transition functions for state HDRSRCHAFC_OPEN_LOOP_STATE */
    hdrsrchafc_ol_deactivate_tf,    /* HDRSRCHAFC_DEACTIVATE_CMD */
    NULL,    /* HDRSRCHAFC_OPEN_LOOP_CMD */
    NULL,    /* HDRSRCHAFC_FAST_ACQ_CMD */
    NULL,    /* HDRSRCHAFC_GRANT_CMD */
    NULL,    /* HDRSRCHAFC_DENY_CMD */
    hdrsrchafc_ol_set_chan_tf,    /* HDRSRCHAFC_SET_CHAN_CMD */
    hdrsrchafc_ol_to_cl_tf,    /* HDRSRCHAFC_CLOSE_LOOP_CMD */
    hdrsrchafc_ol_start_acq_tf,    /* HDRSRCHAFC_START_ACQ_CMD */
    hdrsrchafc_change_restriction_tf,    /* HDRSRCHAFC_CHANGE_RESTR_CMD */
    hdrsrchafc_force_rot_trk_tf,    /* HDRSRCHAFC_FORCE_R_TRK_CMD */
    NULL,    /* HDRSRCHAFC_ACQ_RSP_CMD */
    NULL,    /* HDRSRCHAFC_TIMER_CMD */
    NULL,    /* HDRSRCHAFC_STOP_CMD */
    NULL,    /* HDRSRCHAFC_RPUSH_CMD */
    NULL,    /* HDRSRCHAFC_UPDATE_RGS_CMD */
    NULL,    /* HDRSRCHAFC_RELEASE_CMD */
    NULL,    /* HDRSRCHAFC_CONTINUE_TRK_CMD */

  /* Transition functions for state HDRSRCHAFC_V_ACQ_STATE */
    hdrsrchafc_deactivate_fll_tf,    /* HDRSRCHAFC_DEACTIVATE_CMD */
    hdrsrchafc_acq_to_ol_tf,    /* HDRSRCHAFC_OPEN_LOOP_CMD */
    NULL,    /* HDRSRCHAFC_FAST_ACQ_CMD */
    NULL,    /* HDRSRCHAFC_GRANT_CMD */
    NULL,    /* HDRSRCHAFC_DENY_CMD */
    NULL,    /* HDRSRCHAFC_SET_CHAN_CMD */
    NULL,    /* HDRSRCHAFC_CLOSE_LOOP_CMD */
    NULL,    /* HDRSRCHAFC_START_ACQ_CMD */
    hdrsrchafc_v_change_restriction_tf,    /* HDRSRCHAFC_CHANGE_RESTR_CMD */
    NULL,    /* HDRSRCHAFC_FORCE_R_TRK_CMD */
    hdrsrchafc_v_acq_rsp_tf,    /* HDRSRCHAFC_ACQ_RSP_CMD */
    hdrsrchafc_acq_timer_expire_tf,    /* HDRSRCHAFC_TIMER_CMD */
    NULL,    /* HDRSRCHAFC_STOP_CMD */
    NULL,    /* HDRSRCHAFC_RPUSH_CMD */
    NULL,    /* HDRSRCHAFC_UPDATE_RGS_CMD */
    NULL,    /* HDRSRCHAFC_RELEASE_CMD */
    NULL,    /* HDRSRCHAFC_CONTINUE_TRK_CMD */

  /* Transition functions for state HDRSRCHAFC_R_ACQ_STATE */
    hdrsrchafc_deactivate_fll_tf,    /* HDRSRCHAFC_DEACTIVATE_CMD */
    hdrsrchafc_acq_to_ol_tf,    /* HDRSRCHAFC_OPEN_LOOP_CMD */
    NULL,    /* HDRSRCHAFC_FAST_ACQ_CMD */
    NULL,    /* HDRSRCHAFC_GRANT_CMD */
    NULL,    /* HDRSRCHAFC_DENY_CMD */
    NULL,    /* HDRSRCHAFC_SET_CHAN_CMD */
    NULL,    /* HDRSRCHAFC_CLOSE_LOOP_CMD */
    NULL,    /* HDRSRCHAFC_START_ACQ_CMD */
    hdrsrchafc_r_change_restriction_tf,    /* HDRSRCHAFC_CHANGE_RESTR_CMD */
    NULL,    /* HDRSRCHAFC_FORCE_R_TRK_CMD */
    hdrsrchafc_r_acq_rsp_tf,    /* HDRSRCHAFC_ACQ_RSP_CMD */
    hdrsrchafc_acq_timer_expire_tf,    /* HDRSRCHAFC_TIMER_CMD */
    NULL,    /* HDRSRCHAFC_STOP_CMD */
    NULL,    /* HDRSRCHAFC_RPUSH_CMD */
    NULL,    /* HDRSRCHAFC_UPDATE_RGS_CMD */
    NULL,    /* HDRSRCHAFC_RELEASE_CMD */
    NULL,    /* HDRSRCHAFC_CONTINUE_TRK_CMD */

  /* Transition functions for state HDRSRCHAFC_R_LOOP_STATE */
    hdrsrchafc_deactivate_fll_tf,    /* HDRSRCHAFC_DEACTIVATE_CMD */
    hdrsrchafc_acq_to_ol_tf,    /* HDRSRCHAFC_OPEN_LOOP_CMD */
    NULL,    /* HDRSRCHAFC_FAST_ACQ_CMD */
    NULL,    /* HDRSRCHAFC_GRANT_CMD */
    NULL,    /* HDRSRCHAFC_DENY_CMD */
    NULL,    /* HDRSRCHAFC_SET_CHAN_CMD */
    NULL,    /* HDRSRCHAFC_CLOSE_LOOP_CMD */
    NULL,    /* HDRSRCHAFC_START_ACQ_CMD */
    hdrsrchafc_r_change_restriction_tf,    /* HDRSRCHAFC_CHANGE_RESTR_CMD */
    NULL,    /* HDRSRCHAFC_FORCE_R_TRK_CMD */
    NULL,    /* HDRSRCHAFC_ACQ_RSP_CMD */
    hdrsrchafc_r_loop_timer_expire_tf,    /* HDRSRCHAFC_TIMER_CMD */
    NULL,    /* HDRSRCHAFC_STOP_CMD */
    NULL,    /* HDRSRCHAFC_RPUSH_CMD */
    NULL,    /* HDRSRCHAFC_UPDATE_RGS_CMD */
    NULL,    /* HDRSRCHAFC_RELEASE_CMD */
    NULL,    /* HDRSRCHAFC_CONTINUE_TRK_CMD */

  /* Transition functions for state HDRSRCHAFC_XO_TRK_STATE */
    hdrsrchafc_deactivate_fll_tf,    /* HDRSRCHAFC_DEACTIVATE_CMD */
    NULL,    /* HDRSRCHAFC_OPEN_LOOP_CMD */
    NULL,    /* HDRSRCHAFC_FAST_ACQ_CMD */
    NULL,    /* HDRSRCHAFC_GRANT_CMD */
    NULL,    /* HDRSRCHAFC_DENY_CMD */
    hdrsrchafc_cl_set_chan_tf,    /* HDRSRCHAFC_SET_CHAN_CMD */
    NULL,    /* HDRSRCHAFC_CLOSE_LOOP_CMD */
    hdrsrchafc_trk_start_acq_tf,    /* HDRSRCHAFC_START_ACQ_CMD */
    hdrsrchafc_xo_trk_change_restriction_tf,    /* HDRSRCHAFC_CHANGE_RESTR_CMD */
    NULL,    /* HDRSRCHAFC_FORCE_R_TRK_CMD */
    NULL,    /* HDRSRCHAFC_ACQ_RSP_CMD */
    hdrsrchafc_xo_trk_timer_expire_tf,    /* HDRSRCHAFC_TIMER_CMD */
    hdrsrchafc_stop_fll_trk_tf,    /* HDRSRCHAFC_STOP_CMD */
    hdrsrchafc_rpush_tf,    /* HDRSRCHAFC_RPUSH_CMD */
    hdrsrchafc_update_rgs_tf,    /* HDRSRCHAFC_UPDATE_RGS_CMD */
    NULL,    /* HDRSRCHAFC_RELEASE_CMD */
    NULL,    /* HDRSRCHAFC_CONTINUE_TRK_CMD */

  /* Transition functions for state HDRSRCHAFC_V_TRK_STATE */
    hdrsrchafc_deactivate_fll_tf,    /* HDRSRCHAFC_DEACTIVATE_CMD */
    NULL,    /* HDRSRCHAFC_OPEN_LOOP_CMD */
    NULL,    /* HDRSRCHAFC_FAST_ACQ_CMD */
    NULL,    /* HDRSRCHAFC_GRANT_CMD */
    NULL,    /* HDRSRCHAFC_DENY_CMD */
    hdrsrchafc_cl_set_chan_tf,    /* HDRSRCHAFC_SET_CHAN_CMD */
    NULL,    /* HDRSRCHAFC_CLOSE_LOOP_CMD */
    hdrsrchafc_trk_start_acq_tf,    /* HDRSRCHAFC_START_ACQ_CMD */
    hdrsrchafc_v_change_restriction_tf,    /* HDRSRCHAFC_CHANGE_RESTR_CMD */
    NULL,    /* HDRSRCHAFC_FORCE_R_TRK_CMD */
    NULL,    /* HDRSRCHAFC_ACQ_RSP_CMD */
    hdrsrchafc_v_trk_timer_expire_tf,    /* HDRSRCHAFC_TIMER_CMD */
    hdrsrchafc_stop_fll_trk_tf,    /* HDRSRCHAFC_STOP_CMD */
    NULL,    /* HDRSRCHAFC_RPUSH_CMD */
    NULL,    /* HDRSRCHAFC_UPDATE_RGS_CMD */
    NULL,    /* HDRSRCHAFC_RELEASE_CMD */
    NULL,    /* HDRSRCHAFC_CONTINUE_TRK_CMD */

  /* Transition functions for state HDRSRCHAFC_RV_TRK_STATE */
    hdrsrchafc_deactivate_fll_tf,    /* HDRSRCHAFC_DEACTIVATE_CMD */
    NULL,    /* HDRSRCHAFC_OPEN_LOOP_CMD */
    NULL,    /* HDRSRCHAFC_FAST_ACQ_CMD */
    NULL,    /* HDRSRCHAFC_GRANT_CMD */
    NULL,    /* HDRSRCHAFC_DENY_CMD */
    hdrsrchafc_cl_set_chan_tf,    /* HDRSRCHAFC_SET_CHAN_CMD */
    NULL,    /* HDRSRCHAFC_CLOSE_LOOP_CMD */
    hdrsrchafc_trk_start_acq_tf,    /* HDRSRCHAFC_START_ACQ_CMD */
    hdrsrchafc_rv_trk_change_restriction_tf,    /* HDRSRCHAFC_CHANGE_RESTR_CMD */
    NULL,    /* HDRSRCHAFC_FORCE_R_TRK_CMD */
    NULL,    /* HDRSRCHAFC_ACQ_RSP_CMD */
    hdrsrchafc_rv_trk_timer_expire_tf,    /* HDRSRCHAFC_TIMER_CMD */
    hdrsrchafc_stop_fll_trk_tf,    /* HDRSRCHAFC_STOP_CMD */
    hdrsrchafc_rpush_tf,    /* HDRSRCHAFC_RPUSH_CMD */
    hdrsrchafc_update_rgs_tf,    /* HDRSRCHAFC_UPDATE_RGS_CMD */
    NULL,    /* HDRSRCHAFC_RELEASE_CMD */
    NULL,    /* HDRSRCHAFC_CONTINUE_TRK_CMD */

  /* Transition functions for state HDRSRCHAFC_OFS_STATE */
    hdrsrchafc_ol_deactivate_tf,    /* HDRSRCHAFC_DEACTIVATE_CMD */
    NULL,    /* HDRSRCHAFC_OPEN_LOOP_CMD */
    NULL,    /* HDRSRCHAFC_FAST_ACQ_CMD */
    NULL,    /* HDRSRCHAFC_GRANT_CMD */
    NULL,    /* HDRSRCHAFC_DENY_CMD */
    hdrsrchafc_ol_set_chan_tf,    /* HDRSRCHAFC_SET_CHAN_CMD */
    NULL,    /* HDRSRCHAFC_CLOSE_LOOP_CMD */
    NULL,    /* HDRSRCHAFC_START_ACQ_CMD */
    hdrsrchafc_change_restriction_tf,    /* HDRSRCHAFC_CHANGE_RESTR_CMD */
    NULL,    /* HDRSRCHAFC_FORCE_R_TRK_CMD */
    NULL,    /* HDRSRCHAFC_ACQ_RSP_CMD */
    NULL,    /* HDRSRCHAFC_TIMER_CMD */
    hdrsrchafc_ofs_release_tcxo_tf,    /* HDRSRCHAFC_STOP_CMD */
    NULL,    /* HDRSRCHAFC_RPUSH_CMD */
    NULL,    /* HDRSRCHAFC_UPDATE_RGS_CMD */
    hdrsrchafc_ofs_release_tcxo_tf,    /* HDRSRCHAFC_RELEASE_CMD */
    NULL,    /* HDRSRCHAFC_CONTINUE_TRK_CMD */

  /* Transition functions for state HDRSRCHAFC_STOPPED_STATE */
    hdrsrchafc_deactivate_tf,    /* HDRSRCHAFC_DEACTIVATE_CMD */
    hdrsrchafc_request_ol_tf,    /* HDRSRCHAFC_OPEN_LOOP_CMD */
    NULL,    /* HDRSRCHAFC_FAST_ACQ_CMD */
    hdrsrchafc_grant_tf,    /* HDRSRCHAFC_GRANT_CMD */
    hdrsrchafc_deny_tf,    /* HDRSRCHAFC_DENY_CMD */
    NULL,    /* HDRSRCHAFC_SET_CHAN_CMD */
    NULL,    /* HDRSRCHAFC_CLOSE_LOOP_CMD */
    NULL,    /* HDRSRCHAFC_START_ACQ_CMD */
    NULL,    /* HDRSRCHAFC_CHANGE_RESTR_CMD */
    NULL,    /* HDRSRCHAFC_FORCE_R_TRK_CMD */
    NULL,    /* HDRSRCHAFC_ACQ_RSP_CMD */
    NULL,    /* HDRSRCHAFC_TIMER_CMD */
    hdrsrchafc_cancel_pending_req_tf,    /* HDRSRCHAFC_STOP_CMD */
    NULL,    /* HDRSRCHAFC_RPUSH_CMD */
    NULL,    /* HDRSRCHAFC_UPDATE_RGS_CMD */
    NULL,    /* HDRSRCHAFC_RELEASE_CMD */
    hdrsrchafc_request_continue_tracking_tf,    /* HDRSRCHAFC_CONTINUE_TRK_CMD */

};


/* State machine definition */
stm_state_machine_type HDRSRCHAFC_SM =
{
  "HDRSRCHAFC_SM", /* state machine name */
  55729, /* unique SM id (hash of name) */
  NULL, /* state machine entry function */
  NULL, /* state machine exit function */
  HDRSRCHAFC_INACTIVE_STATE, /* state machine initial state */
  TRUE, /* state machine starts active? */
  HDRSRCHAFC_SM_NUMBER_OF_STATES,
  HDRSRCHAFC_SM_NUMBER_OF_INPUTS,
  HDRSRCHAFC_SM_states,
  HDRSRCHAFC_SM_inputs,
  HDRSRCHAFC_SM_transitions,
};

/* End machine generated code for state machine: HDRSRCHAFC_SM */

