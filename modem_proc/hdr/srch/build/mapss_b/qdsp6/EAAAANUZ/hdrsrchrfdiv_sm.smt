/*=============================================================================

  hdrsrchrfdiv_sm.smt

Description:
  This file contains the machine generated source file for the state machine
  and/or state machine group specified in the file:
  hdrsrchrfdiv_sm.smf

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################

=============================================================================*/

/* Include STM framework header */
#include "stm.h"

/* Begin machine generated code for state machine: HDRSRCHRFDIV_SM */

/* Transition function prototypes */
static stm_state_type hdrsrchrfdiv_request_div_tf(void *);
static stm_state_type hdrsrchrfdiv_set_div_pref_tf(void *);
static stm_state_type hdrsrchrfdiv_set_ramp_down_state_tf(void *);
static stm_state_type hdrsrchrfdiv_runtime_pref_ctrl_tf(void *);
static stm_state_type hdrsrchrfdiv_rf_granted_tf(void *);
static stm_state_type hdrsrchrfdiv_disable_div_tf(void *);
static stm_state_type hdrsrchrfdiv_warm_up_done_tf(void *);
static stm_state_type hdrsrchrfdiv_release_div_event_tf(void *);
static stm_state_type hdrsrchrfdiv_consider_div_switch_tf(void *);
static stm_state_type hdrsrchrfdiv_hard_ta_runtime_pref_ctrl_tf(void *);


/* State Machine entry/exit function prototypes */
static void hdrsrchrfdiv_init(stm_group_type *group);


/* State entry/exit function prototypes */
static void hdrsrchrfdiv_enter_inactive(void *payload, stm_state_type previous_state);
static void hdrsrchrfdiv_enter_acquiring_rf(void *payload, stm_state_type previous_state);
static void hdrsrchrfdiv_enter_warm_up(void *payload, stm_state_type previous_state);
static void hdrsrchrfdiv_enter_enabled(void *payload, stm_state_type previous_state);
static void hdrsrchrfdiv_exit_enabled(void *payload, stm_state_type previous_state);
static void hdrsrchrfdiv_enter_hard_div_tune_away(void *payload, stm_state_type previous_state);


/* Total number of states and inputs */
#define HDRSRCHRFDIV_SM_NUMBER_OF_STATES 5
#define HDRSRCHRFDIV_SM_NUMBER_OF_INPUTS 10


/* State enumeration */
enum
{
  HDRSRCHRFDIV_INACTIVE_STATE,
  HDRSRCHRFDIV_ACQUIRING_RF_STATE,
  HDRSRCHRFDIV_WARM_UP_STATE,
  HDRSRCHRFDIV_ENABLED_STATE,
  HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY_STATE,
};


/* State name, entry, exit table */
static const stm_state_array_type
  HDRSRCHRFDIV_SM_states[ HDRSRCHRFDIV_SM_NUMBER_OF_STATES ] =
{
  {"HDRSRCHRFDIV_INACTIVE_STATE", hdrsrchrfdiv_enter_inactive, NULL},
  {"HDRSRCHRFDIV_ACQUIRING_RF_STATE", hdrsrchrfdiv_enter_acquiring_rf, NULL},
  {"HDRSRCHRFDIV_WARM_UP_STATE", hdrsrchrfdiv_enter_warm_up, NULL},
  {"HDRSRCHRFDIV_ENABLED_STATE", hdrsrchrfdiv_enter_enabled, hdrsrchrfdiv_exit_enabled},
  {"HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY_STATE", hdrsrchrfdiv_enter_hard_div_tune_away, NULL},
};


/* Input value, name table */
static const stm_input_array_type
  HDRSRCHRFDIV_SM_inputs[ HDRSRCHRFDIV_SM_NUMBER_OF_INPUTS ] =
{
  {(stm_input_type)HDRSRCHRFDIV_REQUEST_DIV_CMD, "HDRSRCHRFDIV_REQUEST_DIV_CMD"},
  {(stm_input_type)HDRSRCHRFDIV_SET_DIV_PREF_CMD, "HDRSRCHRFDIV_SET_DIV_PREF_CMD"},
  {(stm_input_type)HDRSRCHRFDIV_SET_RAMP_DOWN_STATE_CMD, "HDRSRCHRFDIV_SET_RAMP_DOWN_STATE_CMD"},
  {(stm_input_type)HDRSRCHRFDIV_RUNTIME_PREF_CTRL_CMD, "HDRSRCHRFDIV_RUNTIME_PREF_CTRL_CMD"},
  {(stm_input_type)HDRSRCHRFDIV_RF_GRANTED_CMD, "HDRSRCHRFDIV_RF_GRANTED_CMD"},
  {(stm_input_type)HDRSRCHRFDIV_DISABLE_DIV_CMD, "HDRSRCHRFDIV_DISABLE_DIV_CMD"},
  {(stm_input_type)HDRSRCHRFDIV_WARMUP_DONE_CMD, "HDRSRCHRFDIV_WARMUP_DONE_CMD"},
  {(stm_input_type)HDRSRCHRFDIV_RELEASE_DIV_CMD, "HDRSRCHRFDIV_RELEASE_DIV_CMD"},
  {(stm_input_type)HDRSRCHRFDIV_CONSIDER_DIV_SWITCH_CMD, "HDRSRCHRFDIV_CONSIDER_DIV_SWITCH_CMD"},
  {(stm_input_type)HDRSRCHRFDIV_HARD_TA_REQUEST_DIV_CMD, "HDRSRCHRFDIV_HARD_TA_REQUEST_DIV_CMD"},
};


/* Transition table */
static const stm_transition_fn_type
  HDRSRCHRFDIV_SM_transitions[ HDRSRCHRFDIV_SM_NUMBER_OF_STATES *  HDRSRCHRFDIV_SM_NUMBER_OF_INPUTS ] =
{
  /* Transition functions for state HDRSRCHRFDIV_INACTIVE_STATE */
    hdrsrchrfdiv_request_div_tf,    /* HDRSRCHRFDIV_REQUEST_DIV_CMD */
    hdrsrchrfdiv_set_div_pref_tf,    /* HDRSRCHRFDIV_SET_DIV_PREF_CMD */
    hdrsrchrfdiv_set_ramp_down_state_tf,    /* HDRSRCHRFDIV_SET_RAMP_DOWN_STATE_CMD */
    hdrsrchrfdiv_runtime_pref_ctrl_tf,    /* HDRSRCHRFDIV_RUNTIME_PREF_CTRL_CMD */
    NULL,    /* HDRSRCHRFDIV_RF_GRANTED_CMD */
    NULL,    /* HDRSRCHRFDIV_DISABLE_DIV_CMD */
    NULL,    /* HDRSRCHRFDIV_WARMUP_DONE_CMD */
    NULL,    /* HDRSRCHRFDIV_RELEASE_DIV_CMD */
    NULL,    /* HDRSRCHRFDIV_CONSIDER_DIV_SWITCH_CMD */
    NULL,    /* HDRSRCHRFDIV_HARD_TA_REQUEST_DIV_CMD */

  /* Transition functions for state HDRSRCHRFDIV_ACQUIRING_RF_STATE */
    NULL,    /* HDRSRCHRFDIV_REQUEST_DIV_CMD */
    hdrsrchrfdiv_set_div_pref_tf,    /* HDRSRCHRFDIV_SET_DIV_PREF_CMD */
    hdrsrchrfdiv_set_ramp_down_state_tf,    /* HDRSRCHRFDIV_SET_RAMP_DOWN_STATE_CMD */
    hdrsrchrfdiv_runtime_pref_ctrl_tf,    /* HDRSRCHRFDIV_RUNTIME_PREF_CTRL_CMD */
    hdrsrchrfdiv_rf_granted_tf,    /* HDRSRCHRFDIV_RF_GRANTED_CMD */
    hdrsrchrfdiv_disable_div_tf,    /* HDRSRCHRFDIV_DISABLE_DIV_CMD */
    NULL,    /* HDRSRCHRFDIV_WARMUP_DONE_CMD */
    NULL,    /* HDRSRCHRFDIV_RELEASE_DIV_CMD */
    NULL,    /* HDRSRCHRFDIV_CONSIDER_DIV_SWITCH_CMD */
    NULL,    /* HDRSRCHRFDIV_HARD_TA_REQUEST_DIV_CMD */

  /* Transition functions for state HDRSRCHRFDIV_WARM_UP_STATE */
    NULL,    /* HDRSRCHRFDIV_REQUEST_DIV_CMD */
    hdrsrchrfdiv_set_div_pref_tf,    /* HDRSRCHRFDIV_SET_DIV_PREF_CMD */
    hdrsrchrfdiv_set_ramp_down_state_tf,    /* HDRSRCHRFDIV_SET_RAMP_DOWN_STATE_CMD */
    hdrsrchrfdiv_runtime_pref_ctrl_tf,    /* HDRSRCHRFDIV_RUNTIME_PREF_CTRL_CMD */
    NULL,    /* HDRSRCHRFDIV_RF_GRANTED_CMD */
    hdrsrchrfdiv_disable_div_tf,    /* HDRSRCHRFDIV_DISABLE_DIV_CMD */
    hdrsrchrfdiv_warm_up_done_tf,    /* HDRSRCHRFDIV_WARMUP_DONE_CMD */
    NULL,    /* HDRSRCHRFDIV_RELEASE_DIV_CMD */
    NULL,    /* HDRSRCHRFDIV_CONSIDER_DIV_SWITCH_CMD */
    NULL,    /* HDRSRCHRFDIV_HARD_TA_REQUEST_DIV_CMD */

  /* Transition functions for state HDRSRCHRFDIV_ENABLED_STATE */
    NULL,    /* HDRSRCHRFDIV_REQUEST_DIV_CMD */
    hdrsrchrfdiv_set_div_pref_tf,    /* HDRSRCHRFDIV_SET_DIV_PREF_CMD */
    hdrsrchrfdiv_set_ramp_down_state_tf,    /* HDRSRCHRFDIV_SET_RAMP_DOWN_STATE_CMD */
    hdrsrchrfdiv_runtime_pref_ctrl_tf,    /* HDRSRCHRFDIV_RUNTIME_PREF_CTRL_CMD */
    NULL,    /* HDRSRCHRFDIV_RF_GRANTED_CMD */
    hdrsrchrfdiv_disable_div_tf,    /* HDRSRCHRFDIV_DISABLE_DIV_CMD */
    NULL,    /* HDRSRCHRFDIV_WARMUP_DONE_CMD */
    hdrsrchrfdiv_release_div_event_tf,    /* HDRSRCHRFDIV_RELEASE_DIV_CMD */
    hdrsrchrfdiv_consider_div_switch_tf,    /* HDRSRCHRFDIV_CONSIDER_DIV_SWITCH_CMD */
    NULL,    /* HDRSRCHRFDIV_HARD_TA_REQUEST_DIV_CMD */

  /* Transition functions for state HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY_STATE */
    NULL,    /* HDRSRCHRFDIV_REQUEST_DIV_CMD */
    hdrsrchrfdiv_set_div_pref_tf,    /* HDRSRCHRFDIV_SET_DIV_PREF_CMD */
    hdrsrchrfdiv_set_ramp_down_state_tf,    /* HDRSRCHRFDIV_SET_RAMP_DOWN_STATE_CMD */
    hdrsrchrfdiv_hard_ta_runtime_pref_ctrl_tf,    /* HDRSRCHRFDIV_RUNTIME_PREF_CTRL_CMD */
    NULL,    /* HDRSRCHRFDIV_RF_GRANTED_CMD */
    hdrsrchrfdiv_disable_div_tf,    /* HDRSRCHRFDIV_DISABLE_DIV_CMD */
    NULL,    /* HDRSRCHRFDIV_WARMUP_DONE_CMD */
    NULL,    /* HDRSRCHRFDIV_RELEASE_DIV_CMD */
    NULL,    /* HDRSRCHRFDIV_CONSIDER_DIV_SWITCH_CMD */
    hdrsrchrfdiv_request_div_tf,    /* HDRSRCHRFDIV_HARD_TA_REQUEST_DIV_CMD */

};


/* State machine definition */
stm_state_machine_type HDRSRCHRFDIV_SM =
{
  "HDRSRCHRFDIV_SM", /* state machine name */
  8729, /* unique SM id (hash of name) */
  hdrsrchrfdiv_init, /* state machine entry function */
  NULL, /* state machine exit function */
  HDRSRCHRFDIV_INACTIVE_STATE, /* state machine initial state */
  FALSE, /* state machine starts active? */
  HDRSRCHRFDIV_SM_NUMBER_OF_STATES,
  HDRSRCHRFDIV_SM_NUMBER_OF_INPUTS,
  HDRSRCHRFDIV_SM_states,
  HDRSRCHRFDIV_SM_inputs,
  HDRSRCHRFDIV_SM_transitions,
};

/* End machine generated code for state machine: HDRSRCHRFDIV_SM */

