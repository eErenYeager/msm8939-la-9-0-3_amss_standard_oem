###############################################################################
#
#    hdrsrchrfdiv_sm.tla
#
# Description:
#   This file contains the machine generated state machine TLA info from the
#   file:  hdrsrchrfdiv_sm.smf
#
#
###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################


# Begin machine generated TLA code for state machine: HDRSRCHRFDIV_SM
# State machine:current state:input:next state                      fgcolor bgcolor

HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_REQUEST_DIV:HDRSRCHRFDIV_INACTIVE 2219 0 00 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_REQUEST_DIV:HDRSRCHRFDIV_ACQUIRING_RF 2219 0 00 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_REQUEST_DIV:HDRSRCHRFDIV_WARM_UP 2219 0 00 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_REQUEST_DIV:HDRSRCHRFDIV_ENABLED 2219 0 00 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_REQUEST_DIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 0 00 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_SET_DIV_PREF:HDRSRCHRFDIV_INACTIVE 2219 0 01 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_SET_DIV_PREF:HDRSRCHRFDIV_ACQUIRING_RF 2219 0 01 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_SET_DIV_PREF:HDRSRCHRFDIV_WARM_UP 2219 0 01 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_SET_DIV_PREF:HDRSRCHRFDIV_ENABLED 2219 0 01 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_SET_DIV_PREF:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 0 01 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_SET_RAMP_DOWN_STATE:HDRSRCHRFDIV_INACTIVE 2219 0 02 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_SET_RAMP_DOWN_STATE:HDRSRCHRFDIV_ACQUIRING_RF 2219 0 02 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_SET_RAMP_DOWN_STATE:HDRSRCHRFDIV_WARM_UP 2219 0 02 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_SET_RAMP_DOWN_STATE:HDRSRCHRFDIV_ENABLED 2219 0 02 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_SET_RAMP_DOWN_STATE:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 0 02 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_RUNTIME_PREF_CTRL:HDRSRCHRFDIV_INACTIVE 2219 0 03 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_RUNTIME_PREF_CTRL:HDRSRCHRFDIV_ACQUIRING_RF 2219 0 03 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_RUNTIME_PREF_CTRL:HDRSRCHRFDIV_WARM_UP 2219 0 03 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_RUNTIME_PREF_CTRL:HDRSRCHRFDIV_ENABLED 2219 0 03 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_RUNTIME_PREF_CTRL:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 0 03 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_RF_GRANTED:HDRSRCHRFDIV_INACTIVE 2219 0 04 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_RF_GRANTED:HDRSRCHRFDIV_ACQUIRING_RF 2219 0 04 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_RF_GRANTED:HDRSRCHRFDIV_WARM_UP 2219 0 04 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_RF_GRANTED:HDRSRCHRFDIV_ENABLED 2219 0 04 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_RF_GRANTED:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 0 04 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_DISABLE_DIV:HDRSRCHRFDIV_INACTIVE 2219 0 05 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_DISABLE_DIV:HDRSRCHRFDIV_ACQUIRING_RF 2219 0 05 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_DISABLE_DIV:HDRSRCHRFDIV_WARM_UP 2219 0 05 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_DISABLE_DIV:HDRSRCHRFDIV_ENABLED 2219 0 05 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_DISABLE_DIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 0 05 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_WARMUP_DONE:HDRSRCHRFDIV_INACTIVE 2219 0 06 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_WARMUP_DONE:HDRSRCHRFDIV_ACQUIRING_RF 2219 0 06 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_WARMUP_DONE:HDRSRCHRFDIV_WARM_UP 2219 0 06 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_WARMUP_DONE:HDRSRCHRFDIV_ENABLED 2219 0 06 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_WARMUP_DONE:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 0 06 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_RELEASE_DIV:HDRSRCHRFDIV_INACTIVE 2219 0 07 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_RELEASE_DIV:HDRSRCHRFDIV_ACQUIRING_RF 2219 0 07 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_RELEASE_DIV:HDRSRCHRFDIV_WARM_UP 2219 0 07 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_RELEASE_DIV:HDRSRCHRFDIV_ENABLED 2219 0 07 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_RELEASE_DIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 0 07 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_CONSIDER_DIV_SWITCH:HDRSRCHRFDIV_INACTIVE 2219 0 08 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_CONSIDER_DIV_SWITCH:HDRSRCHRFDIV_ACQUIRING_RF 2219 0 08 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_CONSIDER_DIV_SWITCH:HDRSRCHRFDIV_WARM_UP 2219 0 08 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_CONSIDER_DIV_SWITCH:HDRSRCHRFDIV_ENABLED 2219 0 08 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_CONSIDER_DIV_SWITCH:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 0 08 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_HARD_TA_REQUEST_DIV:HDRSRCHRFDIV_INACTIVE 2219 0 09 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_HARD_TA_REQUEST_DIV:HDRSRCHRFDIV_ACQUIRING_RF 2219 0 09 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_HARD_TA_REQUEST_DIV:HDRSRCHRFDIV_WARM_UP 2219 0 09 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_HARD_TA_REQUEST_DIV:HDRSRCHRFDIV_ENABLED 2219 0 09 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_INACTIVE:HDRSRCHRFDIV_HARD_TA_REQUEST_DIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 0 09 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_REQUEST_DIV:HDRSRCHRFDIV_INACTIVE 2219 1 00 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_REQUEST_DIV:HDRSRCHRFDIV_ACQUIRING_RF 2219 1 00 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_REQUEST_DIV:HDRSRCHRFDIV_WARM_UP 2219 1 00 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_REQUEST_DIV:HDRSRCHRFDIV_ENABLED 2219 1 00 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_REQUEST_DIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 1 00 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_SET_DIV_PREF:HDRSRCHRFDIV_INACTIVE 2219 1 01 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_SET_DIV_PREF:HDRSRCHRFDIV_ACQUIRING_RF 2219 1 01 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_SET_DIV_PREF:HDRSRCHRFDIV_WARM_UP 2219 1 01 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_SET_DIV_PREF:HDRSRCHRFDIV_ENABLED 2219 1 01 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_SET_DIV_PREF:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 1 01 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_SET_RAMP_DOWN_STATE:HDRSRCHRFDIV_INACTIVE 2219 1 02 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_SET_RAMP_DOWN_STATE:HDRSRCHRFDIV_ACQUIRING_RF 2219 1 02 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_SET_RAMP_DOWN_STATE:HDRSRCHRFDIV_WARM_UP 2219 1 02 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_SET_RAMP_DOWN_STATE:HDRSRCHRFDIV_ENABLED 2219 1 02 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_SET_RAMP_DOWN_STATE:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 1 02 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_RUNTIME_PREF_CTRL:HDRSRCHRFDIV_INACTIVE 2219 1 03 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_RUNTIME_PREF_CTRL:HDRSRCHRFDIV_ACQUIRING_RF 2219 1 03 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_RUNTIME_PREF_CTRL:HDRSRCHRFDIV_WARM_UP 2219 1 03 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_RUNTIME_PREF_CTRL:HDRSRCHRFDIV_ENABLED 2219 1 03 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_RUNTIME_PREF_CTRL:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 1 03 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_RF_GRANTED:HDRSRCHRFDIV_INACTIVE 2219 1 04 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_RF_GRANTED:HDRSRCHRFDIV_ACQUIRING_RF 2219 1 04 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_RF_GRANTED:HDRSRCHRFDIV_WARM_UP 2219 1 04 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_RF_GRANTED:HDRSRCHRFDIV_ENABLED 2219 1 04 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_RF_GRANTED:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 1 04 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_DISABLE_DIV:HDRSRCHRFDIV_INACTIVE 2219 1 05 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_DISABLE_DIV:HDRSRCHRFDIV_ACQUIRING_RF 2219 1 05 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_DISABLE_DIV:HDRSRCHRFDIV_WARM_UP 2219 1 05 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_DISABLE_DIV:HDRSRCHRFDIV_ENABLED 2219 1 05 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_DISABLE_DIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 1 05 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_WARMUP_DONE:HDRSRCHRFDIV_INACTIVE 2219 1 06 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_WARMUP_DONE:HDRSRCHRFDIV_ACQUIRING_RF 2219 1 06 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_WARMUP_DONE:HDRSRCHRFDIV_WARM_UP 2219 1 06 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_WARMUP_DONE:HDRSRCHRFDIV_ENABLED 2219 1 06 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_WARMUP_DONE:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 1 06 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_RELEASE_DIV:HDRSRCHRFDIV_INACTIVE 2219 1 07 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_RELEASE_DIV:HDRSRCHRFDIV_ACQUIRING_RF 2219 1 07 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_RELEASE_DIV:HDRSRCHRFDIV_WARM_UP 2219 1 07 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_RELEASE_DIV:HDRSRCHRFDIV_ENABLED 2219 1 07 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_RELEASE_DIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 1 07 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_CONSIDER_DIV_SWITCH:HDRSRCHRFDIV_INACTIVE 2219 1 08 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_CONSIDER_DIV_SWITCH:HDRSRCHRFDIV_ACQUIRING_RF 2219 1 08 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_CONSIDER_DIV_SWITCH:HDRSRCHRFDIV_WARM_UP 2219 1 08 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_CONSIDER_DIV_SWITCH:HDRSRCHRFDIV_ENABLED 2219 1 08 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_CONSIDER_DIV_SWITCH:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 1 08 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_HARD_TA_REQUEST_DIV:HDRSRCHRFDIV_INACTIVE 2219 1 09 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_HARD_TA_REQUEST_DIV:HDRSRCHRFDIV_ACQUIRING_RF 2219 1 09 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_HARD_TA_REQUEST_DIV:HDRSRCHRFDIV_WARM_UP 2219 1 09 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_HARD_TA_REQUEST_DIV:HDRSRCHRFDIV_ENABLED 2219 1 09 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ACQUIRING_RF:HDRSRCHRFDIV_HARD_TA_REQUEST_DIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 1 09 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_REQUEST_DIV:HDRSRCHRFDIV_INACTIVE 2219 2 00 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_REQUEST_DIV:HDRSRCHRFDIV_ACQUIRING_RF 2219 2 00 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_REQUEST_DIV:HDRSRCHRFDIV_WARM_UP 2219 2 00 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_REQUEST_DIV:HDRSRCHRFDIV_ENABLED 2219 2 00 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_REQUEST_DIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 2 00 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_SET_DIV_PREF:HDRSRCHRFDIV_INACTIVE 2219 2 01 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_SET_DIV_PREF:HDRSRCHRFDIV_ACQUIRING_RF 2219 2 01 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_SET_DIV_PREF:HDRSRCHRFDIV_WARM_UP 2219 2 01 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_SET_DIV_PREF:HDRSRCHRFDIV_ENABLED 2219 2 01 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_SET_DIV_PREF:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 2 01 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_SET_RAMP_DOWN_STATE:HDRSRCHRFDIV_INACTIVE 2219 2 02 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_SET_RAMP_DOWN_STATE:HDRSRCHRFDIV_ACQUIRING_RF 2219 2 02 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_SET_RAMP_DOWN_STATE:HDRSRCHRFDIV_WARM_UP 2219 2 02 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_SET_RAMP_DOWN_STATE:HDRSRCHRFDIV_ENABLED 2219 2 02 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_SET_RAMP_DOWN_STATE:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 2 02 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_RUNTIME_PREF_CTRL:HDRSRCHRFDIV_INACTIVE 2219 2 03 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_RUNTIME_PREF_CTRL:HDRSRCHRFDIV_ACQUIRING_RF 2219 2 03 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_RUNTIME_PREF_CTRL:HDRSRCHRFDIV_WARM_UP 2219 2 03 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_RUNTIME_PREF_CTRL:HDRSRCHRFDIV_ENABLED 2219 2 03 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_RUNTIME_PREF_CTRL:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 2 03 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_RF_GRANTED:HDRSRCHRFDIV_INACTIVE 2219 2 04 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_RF_GRANTED:HDRSRCHRFDIV_ACQUIRING_RF 2219 2 04 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_RF_GRANTED:HDRSRCHRFDIV_WARM_UP 2219 2 04 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_RF_GRANTED:HDRSRCHRFDIV_ENABLED 2219 2 04 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_RF_GRANTED:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 2 04 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_DISABLE_DIV:HDRSRCHRFDIV_INACTIVE 2219 2 05 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_DISABLE_DIV:HDRSRCHRFDIV_ACQUIRING_RF 2219 2 05 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_DISABLE_DIV:HDRSRCHRFDIV_WARM_UP 2219 2 05 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_DISABLE_DIV:HDRSRCHRFDIV_ENABLED 2219 2 05 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_DISABLE_DIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 2 05 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_WARMUP_DONE:HDRSRCHRFDIV_INACTIVE 2219 2 06 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_WARMUP_DONE:HDRSRCHRFDIV_ACQUIRING_RF 2219 2 06 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_WARMUP_DONE:HDRSRCHRFDIV_WARM_UP 2219 2 06 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_WARMUP_DONE:HDRSRCHRFDIV_ENABLED 2219 2 06 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_WARMUP_DONE:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 2 06 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_RELEASE_DIV:HDRSRCHRFDIV_INACTIVE 2219 2 07 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_RELEASE_DIV:HDRSRCHRFDIV_ACQUIRING_RF 2219 2 07 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_RELEASE_DIV:HDRSRCHRFDIV_WARM_UP 2219 2 07 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_RELEASE_DIV:HDRSRCHRFDIV_ENABLED 2219 2 07 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_RELEASE_DIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 2 07 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_CONSIDER_DIV_SWITCH:HDRSRCHRFDIV_INACTIVE 2219 2 08 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_CONSIDER_DIV_SWITCH:HDRSRCHRFDIV_ACQUIRING_RF 2219 2 08 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_CONSIDER_DIV_SWITCH:HDRSRCHRFDIV_WARM_UP 2219 2 08 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_CONSIDER_DIV_SWITCH:HDRSRCHRFDIV_ENABLED 2219 2 08 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_CONSIDER_DIV_SWITCH:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 2 08 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_HARD_TA_REQUEST_DIV:HDRSRCHRFDIV_INACTIVE 2219 2 09 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_HARD_TA_REQUEST_DIV:HDRSRCHRFDIV_ACQUIRING_RF 2219 2 09 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_HARD_TA_REQUEST_DIV:HDRSRCHRFDIV_WARM_UP 2219 2 09 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_HARD_TA_REQUEST_DIV:HDRSRCHRFDIV_ENABLED 2219 2 09 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_WARM_UP:HDRSRCHRFDIV_HARD_TA_REQUEST_DIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 2 09 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_REQUEST_DIV:HDRSRCHRFDIV_INACTIVE 2219 3 00 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_REQUEST_DIV:HDRSRCHRFDIV_ACQUIRING_RF 2219 3 00 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_REQUEST_DIV:HDRSRCHRFDIV_WARM_UP 2219 3 00 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_REQUEST_DIV:HDRSRCHRFDIV_ENABLED 2219 3 00 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_REQUEST_DIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 3 00 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_SET_DIV_PREF:HDRSRCHRFDIV_INACTIVE 2219 3 01 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_SET_DIV_PREF:HDRSRCHRFDIV_ACQUIRING_RF 2219 3 01 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_SET_DIV_PREF:HDRSRCHRFDIV_WARM_UP 2219 3 01 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_SET_DIV_PREF:HDRSRCHRFDIV_ENABLED 2219 3 01 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_SET_DIV_PREF:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 3 01 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_SET_RAMP_DOWN_STATE:HDRSRCHRFDIV_INACTIVE 2219 3 02 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_SET_RAMP_DOWN_STATE:HDRSRCHRFDIV_ACQUIRING_RF 2219 3 02 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_SET_RAMP_DOWN_STATE:HDRSRCHRFDIV_WARM_UP 2219 3 02 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_SET_RAMP_DOWN_STATE:HDRSRCHRFDIV_ENABLED 2219 3 02 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_SET_RAMP_DOWN_STATE:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 3 02 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_RUNTIME_PREF_CTRL:HDRSRCHRFDIV_INACTIVE 2219 3 03 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_RUNTIME_PREF_CTRL:HDRSRCHRFDIV_ACQUIRING_RF 2219 3 03 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_RUNTIME_PREF_CTRL:HDRSRCHRFDIV_WARM_UP 2219 3 03 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_RUNTIME_PREF_CTRL:HDRSRCHRFDIV_ENABLED 2219 3 03 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_RUNTIME_PREF_CTRL:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 3 03 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_RF_GRANTED:HDRSRCHRFDIV_INACTIVE 2219 3 04 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_RF_GRANTED:HDRSRCHRFDIV_ACQUIRING_RF 2219 3 04 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_RF_GRANTED:HDRSRCHRFDIV_WARM_UP 2219 3 04 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_RF_GRANTED:HDRSRCHRFDIV_ENABLED 2219 3 04 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_RF_GRANTED:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 3 04 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_DISABLE_DIV:HDRSRCHRFDIV_INACTIVE 2219 3 05 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_DISABLE_DIV:HDRSRCHRFDIV_ACQUIRING_RF 2219 3 05 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_DISABLE_DIV:HDRSRCHRFDIV_WARM_UP 2219 3 05 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_DISABLE_DIV:HDRSRCHRFDIV_ENABLED 2219 3 05 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_DISABLE_DIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 3 05 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_WARMUP_DONE:HDRSRCHRFDIV_INACTIVE 2219 3 06 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_WARMUP_DONE:HDRSRCHRFDIV_ACQUIRING_RF 2219 3 06 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_WARMUP_DONE:HDRSRCHRFDIV_WARM_UP 2219 3 06 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_WARMUP_DONE:HDRSRCHRFDIV_ENABLED 2219 3 06 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_WARMUP_DONE:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 3 06 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_RELEASE_DIV:HDRSRCHRFDIV_INACTIVE 2219 3 07 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_RELEASE_DIV:HDRSRCHRFDIV_ACQUIRING_RF 2219 3 07 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_RELEASE_DIV:HDRSRCHRFDIV_WARM_UP 2219 3 07 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_RELEASE_DIV:HDRSRCHRFDIV_ENABLED 2219 3 07 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_RELEASE_DIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 3 07 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_CONSIDER_DIV_SWITCH:HDRSRCHRFDIV_INACTIVE 2219 3 08 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_CONSIDER_DIV_SWITCH:HDRSRCHRFDIV_ACQUIRING_RF 2219 3 08 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_CONSIDER_DIV_SWITCH:HDRSRCHRFDIV_WARM_UP 2219 3 08 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_CONSIDER_DIV_SWITCH:HDRSRCHRFDIV_ENABLED 2219 3 08 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_CONSIDER_DIV_SWITCH:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 3 08 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_HARD_TA_REQUEST_DIV:HDRSRCHRFDIV_INACTIVE 2219 3 09 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_HARD_TA_REQUEST_DIV:HDRSRCHRFDIV_ACQUIRING_RF 2219 3 09 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_HARD_TA_REQUEST_DIV:HDRSRCHRFDIV_WARM_UP 2219 3 09 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_HARD_TA_REQUEST_DIV:HDRSRCHRFDIV_ENABLED 2219 3 09 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_ENABLED:HDRSRCHRFDIV_HARD_TA_REQUEST_DIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 3 09 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_REQUEST_DIV:HDRSRCHRFDIV_INACTIVE 2219 4 00 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_REQUEST_DIV:HDRSRCHRFDIV_ACQUIRING_RF 2219 4 00 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_REQUEST_DIV:HDRSRCHRFDIV_WARM_UP 2219 4 00 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_REQUEST_DIV:HDRSRCHRFDIV_ENABLED 2219 4 00 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_REQUEST_DIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 4 00 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_SET_DIV_PREF:HDRSRCHRFDIV_INACTIVE 2219 4 01 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_SET_DIV_PREF:HDRSRCHRFDIV_ACQUIRING_RF 2219 4 01 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_SET_DIV_PREF:HDRSRCHRFDIV_WARM_UP 2219 4 01 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_SET_DIV_PREF:HDRSRCHRFDIV_ENABLED 2219 4 01 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_SET_DIV_PREF:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 4 01 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_SET_RAMP_DOWN_STATE:HDRSRCHRFDIV_INACTIVE 2219 4 02 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_SET_RAMP_DOWN_STATE:HDRSRCHRFDIV_ACQUIRING_RF 2219 4 02 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_SET_RAMP_DOWN_STATE:HDRSRCHRFDIV_WARM_UP 2219 4 02 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_SET_RAMP_DOWN_STATE:HDRSRCHRFDIV_ENABLED 2219 4 02 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_SET_RAMP_DOWN_STATE:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 4 02 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_RUNTIME_PREF_CTRL:HDRSRCHRFDIV_INACTIVE 2219 4 03 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_RUNTIME_PREF_CTRL:HDRSRCHRFDIV_ACQUIRING_RF 2219 4 03 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_RUNTIME_PREF_CTRL:HDRSRCHRFDIV_WARM_UP 2219 4 03 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_RUNTIME_PREF_CTRL:HDRSRCHRFDIV_ENABLED 2219 4 03 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_RUNTIME_PREF_CTRL:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 4 03 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_RF_GRANTED:HDRSRCHRFDIV_INACTIVE 2219 4 04 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_RF_GRANTED:HDRSRCHRFDIV_ACQUIRING_RF 2219 4 04 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_RF_GRANTED:HDRSRCHRFDIV_WARM_UP 2219 4 04 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_RF_GRANTED:HDRSRCHRFDIV_ENABLED 2219 4 04 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_RF_GRANTED:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 4 04 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_DISABLE_DIV:HDRSRCHRFDIV_INACTIVE 2219 4 05 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_DISABLE_DIV:HDRSRCHRFDIV_ACQUIRING_RF 2219 4 05 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_DISABLE_DIV:HDRSRCHRFDIV_WARM_UP 2219 4 05 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_DISABLE_DIV:HDRSRCHRFDIV_ENABLED 2219 4 05 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_DISABLE_DIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 4 05 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_WARMUP_DONE:HDRSRCHRFDIV_INACTIVE 2219 4 06 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_WARMUP_DONE:HDRSRCHRFDIV_ACQUIRING_RF 2219 4 06 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_WARMUP_DONE:HDRSRCHRFDIV_WARM_UP 2219 4 06 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_WARMUP_DONE:HDRSRCHRFDIV_ENABLED 2219 4 06 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_WARMUP_DONE:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 4 06 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_RELEASE_DIV:HDRSRCHRFDIV_INACTIVE 2219 4 07 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_RELEASE_DIV:HDRSRCHRFDIV_ACQUIRING_RF 2219 4 07 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_RELEASE_DIV:HDRSRCHRFDIV_WARM_UP 2219 4 07 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_RELEASE_DIV:HDRSRCHRFDIV_ENABLED 2219 4 07 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_RELEASE_DIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 4 07 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_CONSIDER_DIV_SWITCH:HDRSRCHRFDIV_INACTIVE 2219 4 08 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_CONSIDER_DIV_SWITCH:HDRSRCHRFDIV_ACQUIRING_RF 2219 4 08 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_CONSIDER_DIV_SWITCH:HDRSRCHRFDIV_WARM_UP 2219 4 08 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_CONSIDER_DIV_SWITCH:HDRSRCHRFDIV_ENABLED 2219 4 08 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_CONSIDER_DIV_SWITCH:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 4 08 4    @blue @white

HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_HARD_TA_REQUEST_DIV:HDRSRCHRFDIV_INACTIVE 2219 4 09 0    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_HARD_TA_REQUEST_DIV:HDRSRCHRFDIV_ACQUIRING_RF 2219 4 09 1    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_HARD_TA_REQUEST_DIV:HDRSRCHRFDIV_WARM_UP 2219 4 09 2    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_HARD_TA_REQUEST_DIV:HDRSRCHRFDIV_ENABLED 2219 4 09 3    @blue @white
HDRSRCHRFDIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY:HDRSRCHRFDIV_HARD_TA_REQUEST_DIV:HDRSRCHRFDIV_HARD_DIV_TUNE_AWAY 2219 4 09 4    @blue @white



# End machine generated TLA code for state machine: HDRSRCHRFDIV_SM

