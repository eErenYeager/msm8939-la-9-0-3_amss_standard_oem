/*=============================================================================

  hdrsrchard_sm.smt

Description:
  This file contains the machine generated source file for the state machine
  and/or state machine group specified in the file:
  hdrsrchard_sm.smf

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################

=============================================================================*/

/* Include STM framework header */
#include "stm.h"

/* Begin machine generated code for state machine: HDRSRCHARD_SM */

/* Transition function prototypes */
static stm_state_type hdrsrchard_activate_tf(void *);
static stm_state_type hdrsrchard_deactivate_tf(void *);
static stm_state_type hdrsrchard_set_tap_tf(void *);
static stm_state_type hdrsrchard_suspend_tf(void *);
static stm_state_type hdrsrchard_timer_exp_tf(void *);
static stm_state_type hdrsrchard_fl_activity_tf(void *);
static stm_state_type hdrsrchard_drc_changed_tf(void *);
static stm_state_type hdrsrchard_resume_tf(void *);


/* State Machine entry/exit function prototypes */
static void hdrsrchard_init(stm_group_type *group);


/* State entry/exit function prototypes */
static void hdrsrchard_enter_inactive(void *payload, stm_state_type previous_state);
static void hdrsrchard_enter_fl_active(void *payload, stm_state_type previous_state);
static void hdrsrchard_enter_fl_inactive_drc_low(void *payload, stm_state_type previous_state);
static void hdrsrchard_enter_fl_inactive_drc_high(void *payload, stm_state_type previous_state);
static void hdrsrchard_enter_suspended(void *payload, stm_state_type previous_state);
static void hdrsrchard_enter_tap(void *payload, stm_state_type previous_state);


/* Total number of states and inputs */
#define HDRSRCHARD_SM_NUMBER_OF_STATES 6
#define HDRSRCHARD_SM_NUMBER_OF_INPUTS 8


/* State enumeration */
enum
{
  HDRSRCHARD_INACTIVE_STATE,
  HDRSRCHARD_FL_ACTIVE_STATE,
  HDRSRCHARD_FL_INACTIVE_DRC_LOW_STATE,
  HDRSRCHARD_FL_INACTIVE_DRC_HIGH_STATE,
  HDRSRCHARD_SUSPENDED_STATE,
  HDRSRCHARD_TAP_STATE,
};


/* State name, entry, exit table */
static const stm_state_array_type
  HDRSRCHARD_SM_states[ HDRSRCHARD_SM_NUMBER_OF_STATES ] =
{
  {"HDRSRCHARD_INACTIVE_STATE", hdrsrchard_enter_inactive, NULL},
  {"HDRSRCHARD_FL_ACTIVE_STATE", hdrsrchard_enter_fl_active, NULL},
  {"HDRSRCHARD_FL_INACTIVE_DRC_LOW_STATE", hdrsrchard_enter_fl_inactive_drc_low, NULL},
  {"HDRSRCHARD_FL_INACTIVE_DRC_HIGH_STATE", hdrsrchard_enter_fl_inactive_drc_high, NULL},
  {"HDRSRCHARD_SUSPENDED_STATE", hdrsrchard_enter_suspended, NULL},
  {"HDRSRCHARD_TAP_STATE", hdrsrchard_enter_tap, NULL},
};


/* Input value, name table */
static const stm_input_array_type
  HDRSRCHARD_SM_inputs[ HDRSRCHARD_SM_NUMBER_OF_INPUTS ] =
{
  {(stm_input_type)HDRSRCHARD_ACTIVATE_CMD, "HDRSRCHARD_ACTIVATE_CMD"},
  {(stm_input_type)HDRSRCHARD_DEACTIVATE_CMD, "HDRSRCHARD_DEACTIVATE_CMD"},
  {(stm_input_type)HDRSRCHARD_SET_TAP_CMD, "HDRSRCHARD_SET_TAP_CMD"},
  {(stm_input_type)HDRSRCHARD_SUSPEND_CMD, "HDRSRCHARD_SUSPEND_CMD"},
  {(stm_input_type)HDRSRCHARD_TIMER_EXP_CMD, "HDRSRCHARD_TIMER_EXP_CMD"},
  {(stm_input_type)HDRSRCHARD_FL_ACTIVITY_CMD, "HDRSRCHARD_FL_ACTIVITY_CMD"},
  {(stm_input_type)HDRSRCHARD_DRC_CHANGED_CMD, "HDRSRCHARD_DRC_CHANGED_CMD"},
  {(stm_input_type)HDRSRCHARD_RESUME_CMD, "HDRSRCHARD_RESUME_CMD"},
};


/* Transition table */
static const stm_transition_fn_type
  HDRSRCHARD_SM_transitions[ HDRSRCHARD_SM_NUMBER_OF_STATES *  HDRSRCHARD_SM_NUMBER_OF_INPUTS ] =
{
  /* Transition functions for state HDRSRCHARD_INACTIVE_STATE */
    hdrsrchard_activate_tf,    /* HDRSRCHARD_ACTIVATE_CMD */
    NULL,    /* HDRSRCHARD_DEACTIVATE_CMD */
    NULL,    /* HDRSRCHARD_SET_TAP_CMD */
    NULL,    /* HDRSRCHARD_SUSPEND_CMD */
    NULL,    /* HDRSRCHARD_TIMER_EXP_CMD */
    NULL,    /* HDRSRCHARD_FL_ACTIVITY_CMD */
    NULL,    /* HDRSRCHARD_DRC_CHANGED_CMD */
    NULL,    /* HDRSRCHARD_RESUME_CMD */

  /* Transition functions for state HDRSRCHARD_FL_ACTIVE_STATE */
    NULL,    /* HDRSRCHARD_ACTIVATE_CMD */
    hdrsrchard_deactivate_tf,    /* HDRSRCHARD_DEACTIVATE_CMD */
    hdrsrchard_set_tap_tf,    /* HDRSRCHARD_SET_TAP_CMD */
    hdrsrchard_suspend_tf,    /* HDRSRCHARD_SUSPEND_CMD */
    hdrsrchard_timer_exp_tf,    /* HDRSRCHARD_TIMER_EXP_CMD */
    NULL,    /* HDRSRCHARD_FL_ACTIVITY_CMD */
    NULL,    /* HDRSRCHARD_DRC_CHANGED_CMD */
    NULL,    /* HDRSRCHARD_RESUME_CMD */

  /* Transition functions for state HDRSRCHARD_FL_INACTIVE_DRC_LOW_STATE */
    NULL,    /* HDRSRCHARD_ACTIVATE_CMD */
    hdrsrchard_deactivate_tf,    /* HDRSRCHARD_DEACTIVATE_CMD */
    hdrsrchard_set_tap_tf,    /* HDRSRCHARD_SET_TAP_CMD */
    hdrsrchard_suspend_tf,    /* HDRSRCHARD_SUSPEND_CMD */
    NULL,    /* HDRSRCHARD_TIMER_EXP_CMD */
    hdrsrchard_fl_activity_tf,    /* HDRSRCHARD_FL_ACTIVITY_CMD */
    hdrsrchard_drc_changed_tf,    /* HDRSRCHARD_DRC_CHANGED_CMD */
    NULL,    /* HDRSRCHARD_RESUME_CMD */

  /* Transition functions for state HDRSRCHARD_FL_INACTIVE_DRC_HIGH_STATE */
    NULL,    /* HDRSRCHARD_ACTIVATE_CMD */
    hdrsrchard_deactivate_tf,    /* HDRSRCHARD_DEACTIVATE_CMD */
    hdrsrchard_set_tap_tf,    /* HDRSRCHARD_SET_TAP_CMD */
    hdrsrchard_suspend_tf,    /* HDRSRCHARD_SUSPEND_CMD */
    NULL,    /* HDRSRCHARD_TIMER_EXP_CMD */
    hdrsrchard_fl_activity_tf,    /* HDRSRCHARD_FL_ACTIVITY_CMD */
    hdrsrchard_drc_changed_tf,    /* HDRSRCHARD_DRC_CHANGED_CMD */
    NULL,    /* HDRSRCHARD_RESUME_CMD */

  /* Transition functions for state HDRSRCHARD_SUSPENDED_STATE */
    NULL,    /* HDRSRCHARD_ACTIVATE_CMD */
    hdrsrchard_deactivate_tf,    /* HDRSRCHARD_DEACTIVATE_CMD */
    hdrsrchard_set_tap_tf,    /* HDRSRCHARD_SET_TAP_CMD */
    NULL,    /* HDRSRCHARD_SUSPEND_CMD */
    NULL,    /* HDRSRCHARD_TIMER_EXP_CMD */
    NULL,    /* HDRSRCHARD_FL_ACTIVITY_CMD */
    NULL,    /* HDRSRCHARD_DRC_CHANGED_CMD */
    hdrsrchard_resume_tf,    /* HDRSRCHARD_RESUME_CMD */

  /* Transition functions for state HDRSRCHARD_TAP_STATE */
    NULL,    /* HDRSRCHARD_ACTIVATE_CMD */
    hdrsrchard_deactivate_tf,    /* HDRSRCHARD_DEACTIVATE_CMD */
    hdrsrchard_set_tap_tf,    /* HDRSRCHARD_SET_TAP_CMD */
    hdrsrchard_suspend_tf,    /* HDRSRCHARD_SUSPEND_CMD */
    NULL,    /* HDRSRCHARD_TIMER_EXP_CMD */
    NULL,    /* HDRSRCHARD_FL_ACTIVITY_CMD */
    NULL,    /* HDRSRCHARD_DRC_CHANGED_CMD */
    hdrsrchard_resume_tf,    /* HDRSRCHARD_RESUME_CMD */

};


/* State machine definition */
stm_state_machine_type HDRSRCHARD_SM =
{
  "HDRSRCHARD_SM", /* state machine name */
  58802, /* unique SM id (hash of name) */
  hdrsrchard_init, /* state machine entry function */
  NULL, /* state machine exit function */
  HDRSRCHARD_INACTIVE_STATE, /* state machine initial state */
  FALSE, /* state machine starts active? */
  HDRSRCHARD_SM_NUMBER_OF_STATES,
  HDRSRCHARD_SM_NUMBER_OF_INPUTS,
  HDRSRCHARD_SM_states,
  HDRSRCHARD_SM_inputs,
  HDRSRCHARD_SM_transitions,
};

/* End machine generated code for state machine: HDRSRCHARD_SM */

