#ifndef QFE1550_CMN_HAL_TYPEDEFS_H
#define QFE1550_CMN_HAL_TYPEDEFS_H
/*! 
  @file
  qfe1550_common_hal_typedef.h
 
  @brief
  Contains QFE1550 hal related typedefs

  @details

  @addtogroup QFE1550_COMMON
  @{
*/

/*==============================================================================

  Copyright (c) 2013 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===============================================================================*/

/*==============================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/rfdevice_qfe1550/common/inc/qfe1550_cmn_hal_typedefs.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------- 
03/21/13   sr    Initial version, leveraged from 1520

==============================================================================*/
#include "rf_buffer_intf.h"
#include "rfdevice_intf_cmd.h"
#include "rfdevice_type_defs.h"
#include "rf_hal_bus_types.h"

/*! Maximum number of SSBI writes possible/supported in a QFE script */
#define QFE1550_MAX_SCRIPT_SIZE         60

/*==============================================================================
   DEFINITONS
==============================================================================*/

/*----------------------------------------------------------------------------*/
/*!
  @brief
  Generic SSBI write type usable by all RF devices
*/
typedef struct
{
  /*! register address to write to */
  uint8 address;

  /*! Data to write to register address */
  uint8 data;

  /*! Delay in us to wait before sending this write to hardware */
  uint16 delay;
} qfe1550_script_data_type;


/*----------------------------------------------------------------------------*/
/*!
   @brief
   This structure defines the structure of a QFE device script
*/
typedef struct
{
  uint16 size;
  /*!< Actual number of SSBI entries in the script */

  qfe1550_script_data_type  buffer[ QFE1550_MAX_SCRIPT_SIZE ];
  /*!< Buffer for the device SSBI script */

} qfe1550_script_type;


/*----------------------------------------------------------------------------*/
/*!
   @brief
   This structure defines HAL communication interface parameters
   Note: rd_delay and new_time_ref are placed here as plaoe holders only
         Tere are no requirements yet to use these two fields
*/
typedef struct
{
  rfdevice_comm_proto_enum_type comm_proto; /*!< Communication prototype */
  uint8 slave_id;        /*!< SSBI/RFFE: Slave ID, also to be used as phys bus for SSBI*/
  uint8 channel;         /*!< RFFE: Channel number  */
  boolean extended_cmd;  /*!< RFFE Indicator to use extended cmd or not */
  uint8 half_rate;       /*!< RFFE half-rate configuration */
  uint8 rd_delay;        /*!< RFFE read delay -- PLACE HOLDER */
  rf_hal_bus_resource_script_settings_type settings;  /*!< RFFE script settings */
  boolean new_time_ref;  /*!< SSBI/RFFE: Indicator of new time ref -- PLACE HOLDER */
} qfe1550_comm_params_type;

#endif
