#ifndef QFE1550_PHYSICAL_DEVICE_H
#define QFE1550_PHYSICAL_DEVICE_H
/*!
  @file
  qfe1550_physical_device.h 

  @brief
  Software abstraction of a QFE1550 physical device.
*/

/*===========================================================================

  Copyright (c) 2013 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


/*===========================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/rfdevice_qfe1550/common/inc/qfe1550_physical_device.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------- 
05/22/14   vv      Overloaded APIs  create_tuner_object()  to invoke the new constructors
05/08/14   vv      Physical device interface support for DPM 2.0
10/02/13   adk     Enabled C-tuner calibration
09/17/13   ndb     Added qfe1550_physical_device() Destructor 
04/26/13   aca     RC Tuner cal
04/25/13   aca     Migration to seperate cal data file
04/23/13   aca     Self cal data load
03/28/13   bmg     Created

===========================================================================*/

#include "rfdevice_physical_device.h"
#include "qfe1550_antenna_tuner.h"

typedef struct qfe1550_cal_data qfe1550_cal_data_type;


class qfe1550_physical_device : public rfdevice_physical_device
{
public:
  qfe1550_physical_device(rfc_device_cfg_info_type* cfg);
 
  ~qfe1550_physical_device();

  virtual bool validate_self_cal_efs(void);

  virtual bool load_self_cal(const char* str);

  
  virtual bool perform_self_cal(const char* str);
  
  virtual bool perform_c_tuner_cal(void);

  virtual bool load_c_tuner_cal(void);
  

  virtual rfdevice_logical_component* get_component(rfdevice_type_enum_type type, int instance);



  /*Overloaded constructor for DPM 2.0*/
  qfe1550_physical_device(rfc_phy_device_info_type* cfg);

  /*Overloaded function for DPM 2.0*/
  virtual rfdevice_logical_component* get_component(rfc_logical_device_info_type *logical_device_cfg);

  /*Physical object pointer used by DPM 2.0*/
  qfe1550_physical_device* qfe1550_physical_device_p;

  rfc_phy_device_info_type* phy_device_cfg;

private:
  void create_tuner_object(void);

  /*Overloaded API for physical device support in Bolt and DPM2.0*/
  void create_tuner_object( rfc_logical_device_info_type *logical_device_cfg);

  rfc_device_cfg_info_type* cfg;
  qfe1550_antenna_tuner* tuner_ptr;
  
  /* Cal data */
  qfe1550_cal_data_type* calibrated_data;
};

#endif /* QFE1550_PHYSICAL_DEVICE_H */

