#ifndef QFE1550_ANTENNA_TUNER_H
#define QFE1550_ANTENNA_TUNER_H
/*!
   @file
   qfe1550_antenna_tuner.h

   @brief
   QFE1550 antenna tuner device driver header file

*/

/*===========================================================================

Copyright (c) 2013-2014 by Qualcomm Technologies, Inc.  All Rights Reserved.

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/rfdevice_qfe1550/common/inc/qfe1550_antenna_tuner.h#1 $

when       who   what, where, why
--------   ---   ------------------------------------------------------------------- 
05/22/14   vv    Overloaded constructor for physical device support for Bolt and DPm2.0 PLs
03/25/14   yb    Interface update for linearizer
03/25/14   ndb   Do the Detune Code programming only when NVG is enabled
03/17/14   vb    Added program_nvg_detune_tuner() API
01/02/14   yb    QFE1550 Optimization
11/27/13   yb    Added support for default tune code when tuner QCN is missing
                 Removed Low Power API
10/29/13   ndb   Added the support for dynamic tune code size
10/02/13   adk   Enabled C-tuner calibration
07/03/13   hm    Added tuner_low_power API
06/26/13   aca   Virtual function clean up- based on CL 3984018
06/26/13   aca   RL API update
06/10/13   aca   RL APIs
04/26/13   aca   RC Tuner cal
04/12/13   sr    removed legacy code which is not used.
03/27/13   vb    Added API for getting the comm. params of tuner
03/21/13   sr    Initial version, leveraged from 1520

============================================================================*/

/*===========================================================================
                           INCLUDE FILES
===========================================================================*/
#ifdef __cplusplus
extern "C" {
#endif

#include "rfdevice_antenna_tuner.h"
#include "rfcommon_locks.h"
#include "sys.h"
#include "qfe1550_cmn_hal_typedefs.h"
#include "rfc_common.h"
#include "rfdevice_physical_device.h"
#ifdef __cplusplus
}
#endif

// .dat file related definitions
#define QFE1550_ANTENNA_TUNER_RF_HW_STRING_SIZE 4
#define QFE1550_ANTENNA_TUNER_MAX_PATH_SIZE 100
#define QFE1550_ANTENNA_TUNER_MAX_DAT_SIZE 155
#define QFE1550_ANTENNA_TUNER_MAX_BINS 8
#define QFE1550_ANTENNA_TUNER_MAX_SCENARIOS_PER_BAND 9
#define QFE1550_ANTENNA_TUNER_MAX_SCENARIO_INDEX 64
#define QFE1550_ANTENNA_TUNER_DEFAULT_BIN_IDX 0
#define QFE1550_ANTENNA_TUNER_DEFAULT_SCENARIO_IDX 0
#define QFE1550_ANTENNA_TUNER_MAX_OEM_STRING_LEN 64
#define QFE1550_ANTENNA_TUNE_CODE_SIZE 2


typedef enum
{
  QFE1550_ANTENNA_TUNER_WCDMA_IDX,
  QFE1550_ANTENNA_TUNER_GSM_RX_IDX,
  QFE1550_ANTENNA_TUNER_GSM_TX_IDX,
  QFE1550_ANTENNA_TUNER_CDMA_IDX,
  QFE1550_ANTENNA_TUNER_LTE_IDX,
  QFE1550_ANTENNA_TUNER_IDX_NUM
} qfe1550_antenna_tuner_tech_idx;

typedef enum
{
  QFE1550_ANTENNA_TUNER_CHIP_V1,
  QFE1550_ANTENNA_TUNER_CHIP_V2,
  QFE1550_ANTENNA_TUNER_CHIP_V3,
  QFE1550_ANTENNA_TUNER_CHIP_V4,
  QFE1550_ANTENNA_TUNER_CHIP_VER_NUM
} qfe1550_antenna_tuner_chip_ver;

// Data structure for a C1 C2 pair
typedef struct
{
  uint8 c1;
  uint8 c2;
}qfe1550_antenna_tuner_c1_c2_pair;

// Data structure for a scenario data file
typedef struct
{
  uint8 num_of_bins;
  uint8 num_of_scenarios;
  uint8 mapping[QFE1550_ANTENNA_TUNER_MAX_SCENARIOS_PER_BAND];
  qfe1550_antenna_tuner_c1_c2_pair data[][QFE1550_ANTENNA_TUNER_MAX_SCENARIOS_PER_BAND];
}qfe1550_antenna_tuner_scenario_data;

// Data structure for a band info
typedef struct
{
  uint32 min_tx_freq;
  uint32 max_tx_freq;
  qfe1550_antenna_tuner_scenario_data* scenario_data;
}qfe1550_antenna_tuner_band_info;

// Data structure for info data file
// See RFRPE IDL definition radio_frequency_radiated_performance_enhancement_v01.h
typedef struct
{
  uint32 revision;
  uint32 oem_string_len;
  uint16 oem_string[];
}qfe1550_antenna_tuner_info_data;


/* Data structure to hold the tuner config .Currently support Version 0 format */ 
typedef struct
{
  uint8 version;
  PACK(union)
  {
    PACK(struct)
    {
      uint8 c1_tune_code;
      uint8 c2_tune_code;
      uint8 switch_config;
    }version_0;
  }tunecode_config;
}qfe1550_antenna_tuner_tunecode_config_data_type;


/* Data structure to hold the override tune code values .Currently support Version 0 format  */ 

typedef struct
{
 PACK(union)
  { 
   qfe1550_antenna_tuner_c1_c2_pair tuner_data;
  }antenna_tuner_data_v0;
 uint8 qfe1550_antenna_tuner_override_flag;
}qfe1550_antenna_tuner_tunecode_override_info_type;

class qfe1550_antenna_tuner: public rfdevice_antenna_tuner
{
  public:

  qfe1550_antenna_tuner( rfc_device_cfg_info_type* cfg,
                         rf_path_enum_type rf_path,
                         boolean extended_cmd,
                         uint8 half_rate
                       );

  /* Overloaded constructor for physical device support */
  qfe1550_antenna_tuner(
                          rfdevice_physical_device* phy_obj_ptr,
                          rfc_phy_device_info_type *phy_device_info, 
                          rfc_logical_device_info_type *logical_device_info,
                          rf_path_enum_type rf_path,
                          boolean extended_cmd,
                          uint8 half_rate
                        );
  /*To store the parent physical device object*/
  rfdevice_physical_device* rfdevice_phy_obj_ptr;

  ~qfe1550_antenna_tuner();

  boolean init( rf_buffer_intf *buff_obj_ptr, rf_device_execution_type dev_action, int16 script_timing = 0 );

  void init();

  boolean rx_mode( rf_buffer_intf *buff_obj_ptr,
                   rf_device_execution_type dev_action,
                   int16 script_timing = 0,
                   rfcom_mode_enum_type mode = RFCOM_PARKED_MODE,
                   uint32 rx_freq = 0 );

  boolean rx_init ( rf_buffer_intf *buff_obj_ptr,
	                rfcom_mode_enum_type mode,
	                rf_device_execution_type dev_action,
	                int16 script_timing = 0);

 
  boolean rx_disable ( rf_buffer_intf *buff_obj_ptr, rf_device_execution_type dev_action, int16 script_timing = 0 );

  boolean tx_mode_enable( rfcom_mode_enum_type mode, uint32 tx_freq,
                          rf_buffer_intf *buff_obj_ptr, 
                          rf_device_execution_type dev_action,
                          int16 script_timing = 0,
                          rfcom_band_type_u band = rfcom_band_type_u());

  boolean tx_mode_disable( rf_buffer_intf *buff_obj_ptr, 
                           rf_device_execution_type dev_action,
                           int16 script_timing = 0 );


  boolean program_scenario( rfcom_mode_enum_type mode, rfcom_band_type_u band,
                            uint16 chan_num,
                            boolean is_this_rx_operation,
                            uint8 current_scenario_number,
                            rf_buffer_intf *buff_obj_ptr, 
                            rf_device_execution_type dev_action,
                            int16 script_timing = 0);

  boolean disable ( rf_buffer_intf *buff_obj_ptr, rf_device_execution_type dev_action, int16 script_timing = 0 );

  uint16 get_script_size(void);

  /*! Read chip version of the antenna tuner */
  int16 read_chip_rev( void);

  boolean perform_c_tuner_cal(uint8* c1_idx, uint8* c2_idx);

  boolean load_c_tuner_cal(uint8 c1_idx, uint8 c2_idx);
	
  /* tune_code_override support */
  rfdevice_antenna_tuner_device_override_status_type tune_code_override(uint8 override_flag,
   	                                                                void *data,
 	                                                                uint8* tuner_nv_ptr);
  /* Get ptr to antenna tuner crit_sect_lock */
  rf_lock_data_type* get_antenna_tuner_crit_sect_lock( void);

    /*! Get the comm. params of the antenna tuner */
  qfe1550_comm_params_type get_antenna_tuner_comm_params( void );

  /*! Get the tuner instance value */
  rfdevice_antenna_tuner_device_num_type get_antenna_tuner_instance_val( void );

  /* Program tune codes */
  boolean program_tune_code
  (
   uint8 *tune_code_ptr,
   uint8 tune_code_size,
   rf_buffer_intf *buff_obj_ptr, 
   rf_device_execution_type dev_action,
   int16 script_timing
  );
 
  /* Program default tune code */
  boolean program_default_tune_code
  (
    uint8 *tune_code_ptr,
    uint8 *tune_code_size_ptr,
    rf_buffer_intf *buff_obj_ptr, 
    rf_device_execution_type dev_action,
    int16 script_timing
  );

  /* To program NVG and to detune*/
  boolean program_nvg_detune_tuner
  (
    boolean nvg_on_off,
    boolean is_program_nvg,
    uint8 tune_code_size,
    uint8 *tune_code_ptr,
    rf_buffer_intf *buff_obj_ptr, 
    rf_device_execution_type dev_action,
    int16 script_timing
  );

  /*! Indicator for whether tuner is initialized */
  boolean is_tuner_init;

  /*! Indicator for whether tuner is enabled */
  /*The below flag can be removed once the upper 
  level call is able to manage the enable/disable sequence
  of QFE1550*/
  boolean is_tuner_enabled;

  protected:
 
  private:
  /*! Device communication prototype */
  rfdevice_antenna_tuner_device_num_type device_num;

  /*! The RF path this device is associated to 
      This is needed in order to retrive the correct bc_config */
  rf_path_enum_type device_rf_path;

  /*! Device communication prototype */
  qfe1550_comm_params_type comm_params;

  /*! QFE1550 global mutex - one per instance. Each technology should
  use this mutex once initialized. Should be initialized during device
  configuration before any other usage */
  rf_lock_data_type crit_sect_lock;

  /*! Buffer to hold AG script */
  qfe1550_script_type qfe1550_script;

  /*! Flag that indicates if C-tuner cal has been done */
  boolean c_tuner_cal_done;

  /*! CPG frequency */
  uint32 cpg_freq;

  /*! Column indices of c1_comp and c2_comp arrays to get corrected values of c1 and c2 caps */
  uint8 c1_col_idx, c2_col_idx;

  /*! TX Frequency */
  uint32 tx_freq;

  /* Override tuner code values */
  qfe1550_antenna_tuner_tunecode_override_info_type tuner_tunecode_override_data;
	
  /* Func to compute the tune code values based on the tune code config and update nv values */
  rfdevice_antenna_tuner_device_override_status_type get_tuner_tune_codewords(void *data,
  	                                                                      uint8* tuner_nv_ptr );

  /*! Calculate cap values from C-tuner cal data */
  boolean get_ctuner_qcm_measure( uint32 *ctuner_meas_pF_x_10000, uint32 V_mVolt,
                                  uint32 I_uAmp_x_10, uint32 C_ideal_pF_x_10000,
                                  uint32 qcm_count_1pF, uint32 qcm_count_4pF);

  /*! Bin values of c1_comp and c2_comp arrays to get corrected values of c1 and c2 caps */
  void get_ctuner_bin_values( int* c1_bin, int* c2_bin,
                              uint32 ctuner_meas_pF_x_10000);

  /*! Convert bin values of c1_comp and c2_comp tables to their column indices */
  boolean map_ctuner_bin_values_to_comp_tbl_column_indices
  ( 
    int c1_bin, 
    int c2_bin,
    uint8* c1_col_idx_ptr,
    uint8* c2_col_idx_ptr
  );

  /*! Get corrected values of c1 and c2 capacitances */
  void get_ctuner_comp_settings( uint8 c1_in, uint8 c2_in, uint8* c1_out, uint8* c2_out);

  /*! Determine if the entries in c1_values[] and c2_values[] increase
      strictly monotonically */
  boolean do_monotonicity_check_on_cap_comp_tbls( void);

  /*! Search for the entry cap_val_in in the array c1_values[] or c2_values[] */
  boolean get_ctuner_comp_tbl_row_idx( uint8 *row_idx,
                                       uint8 (*fp_get_cap_val)(uint8 i),
                                       uint8 cap_val_in,
                                       uint8 cap_min_val,
                                       uint8 cap_max_val,
                                       int c_tbl_row_dim);
};

#endif /*QFE1550_ANTENNA_TUNER_H*/

