  /*========================================================================
    TCM layout:

    --------------------------------
    static TCM
    --------------------------------
    tcm region 1| tcm region 2
                |-------------
                | tcm region 3
                |-------------
                | tcm region 4
    --------------------------------
    dynamic TCM allocation
    --------------------------------

    Region 1: Overlayed WCDMA, GERAN, TD-SCDMA traffic
    Region 2,3: 1x, C2K common
    Region 4: Overlayed SV techs (MCDO, LTE)
    Dynamic TCM allocation: Allocated dynamically at runtime.

    ========================================================================*/


  /*========================================================================
    TCM region1. Starts after static TCM, size is max of overlays.   
    Set the offset, modulo max page size, of the end of the static TCM region.
    ========================================================================*/
  __pa_location__          = ALIGN(__tcm_static_pa_end__, __min_page_size__) ;
  __tcm_region1_pa_start__ = __pa_location__ ;
  __tcm_region1_pa_ofs__   = (__pa_location__) & (__max_page_size__ - 1);
  __tcm_region1_size__     = 0;


  /*------------------------------------------------------------------------
    WCDMA traffic mode TCM overlay.   
    Note that the appropriate sections need to be added to PHDRS above also.
   ------------------------------------------------------------------------*/
  __pa_location__ = __tcm_region1_pa_start__ ;

  __quant_load_location__ = (__load_location__ / __max_page_size__) * __max_page_size__ + __tcm_region1_pa_ofs__;
  __load_location__ = (__quant_load_location__ >= __load_location__ ? __quant_load_location__ : __quant_load_location__ + __max_page_size__);
  __va_location__ = __load_location__ ;

  __tcm_wfw_traffic_overlay_va_load__  = __load_location__ ; 
  __tcm_wfw_traffic_overlay_pa_run__   = __pa_location__ ;
  __tcm_wfw_traffic_overlay_va_start__ = __va_location__ ;  
  .  = __va_location__ ;

  __tcm_wfw_traffic_overlay_data_start__ = . ;
  .tcm_wfw_traffic_overlay_data : AT (__load_location__ + (ADDR(.tcm_wfw_traffic_overlay_data) - __va_location__) )
  {
    *(.tcm_wfw_traffic_overlay_data .tcm_wfw_traffic_overlay_data.*)
  } :TCM_WFW_TRAFFIC_OVERLAY =0

  .  = ALIGN(__cache_line_size__);
  __tcm_wfw_traffic_overlay_data_end__ = . ;

  __tcm_wfw_traffic_overlay_code_start__ = . ;
  .tcm_wfw_traffic_overlay_code : AT (__load_location__ + (ADDR(.tcm_wfw_traffic_overlay_code) - __va_location__) )
  {
    *(.tcm_wfw_traffic_overlay_code .tcm_wfw_traffic_overlay_code.*)
  } :TCM_WFW_TRAFFIC_OVERLAY =0x00c0007f

  .  = ALIGN(__cache_line_size__);  
  __tcm_wfw_traffic_overlay_code_end__ = . ;

  /* If data is present, then we need a partition within the code segment to switch
     from WB to WT.
     Pad up to a 4k multiple if start and end address of code are in same page. */
  . += ( __tcm_wfw_traffic_overlay_data_end__ > __tcm_wfw_traffic_overlay_data_start__ ? ( (. & ~(__min_page_size__ - 1)) == (__tcm_wfw_traffic_overlay_code_start__ & ~(__min_page_size__ - 1)) ? __min_page_size__ - (. & (__min_page_size__ - 1)) : 0 ) : 0 );  

  __tcm_wfw_traffic_overlay_dma_data_start__ = . ;
  .tcm_wfw_traffic_overlay_dma_data : AT (__load_location__ + (ADDR(.tcm_wfw_traffic_overlay_dma_data) - __va_location__) )
  {
    *(.tcm_wfw_traffic_overlay_dma_data .tcm_wfw_traffic_overlay_dma_data.*)
  } :TCM_WFW_TRAFFIC_OVERLAY =0

  .  = ALIGN(__cache_line_size__);  
  __tcm_wfw_traffic_overlay_dma_data_end__ = . ;

  __tcm_wfw_traffic_overlay_dma_bss_start__ = . ;
  .tcm_wfw_traffic_overlay_dma_bss (NOLOAD) : AT (__load_location__ + (ADDR(.tcm_wfw_traffic_overlay_dma_bss) - __va_location__) )
  {
    *(.tcm_wfw_traffic_overlay_dma_bss .tcm_wfw_traffic_overlay_dma_bss.*)
  } :TCM_WFW_TRAFFIC_OVERLAY =0

  .  = ALIGN(__cache_line_size__);  
  __tcm_wfw_traffic_overlay_dma_bss_end__ = . ;

  . = ALIGN(__min_page_size__);
  __tcm_wfw_traffic_overlay_va_end__ = . ;

  __section_len__    = . - __va_location__;
  __pa_location__   += __section_len__ ;
  __va_location__   += __section_len__ + (__section_len__ > 0 ? __min_page_size__ : 0) ; 
  __load_location__ += __section_len__ + (__section_len__ > 0 ? __min_page_size__ : 0) ;

  __tcm_region1_size__ = MAX( __tcm_region1_size__ , __section_len__ );

  /*------------------------------------------------------------------------
    End of WCDMA TCM traffic mode overlay.   
   ------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    GERAN traffic mode TCM overlay.   
    Note that the appropriate sections need to be added to PHDRS above also.
   ------------------------------------------------------------------------*/
  __pa_location__ = __tcm_region1_pa_start__ ;

  __quant_load_location__ = (__load_location__ / __max_page_size__) * __max_page_size__ + __tcm_region1_pa_ofs__;
  __load_location__ = (__quant_load_location__ >= __load_location__ ? __quant_load_location__ : __quant_load_location__ + __max_page_size__);
  __va_location__ = __load_location__ ;

  __tcm_gfw_traffic_overlay_va_load__  = __load_location__ ; 
  __tcm_gfw_traffic_overlay_pa_run__   = __pa_location__ ;
  __tcm_gfw_traffic_overlay_va_start__ = __va_location__ ;  
  .  = __va_location__ ;

  __tcm_gfw_traffic_overlay_data_start__ = . ;
  .tcm_gfw_traffic_overlay_data : AT (__load_location__ + (ADDR(.tcm_gfw_traffic_overlay_data) - __va_location__) )
  {
    *(.tcm_gfw_traffic_overlay_data .tcm_gfw_traffic_overlay_data.*)
  } :TCM_GFW_TRAFFIC_OVERLAY =0

  .  = ALIGN(__cache_line_size__);
  __tcm_gfw_traffic_overlay_data_end__ = . ;

  __tcm_gfw_traffic_overlay_code_start__ = . ;
  .tcm_gfw_traffic_overlay_code : AT (__load_location__ + (ADDR(.tcm_gfw_traffic_overlay_code) - __va_location__) )
  {
    *(.tcm_gfw_traffic_overlay_code .tcm_gfw_traffic_overlay_code.*)
  } :TCM_GFW_TRAFFIC_OVERLAY =0x00c0007f

  .  = ALIGN(__cache_line_size__);  
  __tcm_gfw_traffic_overlay_code_end__ = . ;

  /* If data is present, then we need a partition within the code segment to switch
     from WB to WT.
     Pad up to a 4k multiple if start and end address of code are in same page. */
  . += ( __tcm_gfw_traffic_overlay_data_end__ > __tcm_gfw_traffic_overlay_data_start__ ? ( (. & ~(__min_page_size__ - 1)) == (__tcm_gfw_traffic_overlay_code_start__ & ~(__min_page_size__ - 1)) ? __min_page_size__ - (. & (__min_page_size__ - 1)) : 0 ) : 0 );  

  __tcm_gfw_traffic_overlay_dma_data_start__ = . ;
  .tcm_gfw_traffic_overlay_dma_data : AT (__load_location__ + (ADDR(.tcm_gfw_traffic_overlay_dma_data) - __va_location__) )
  {
    *(.tcm_gfw_traffic_overlay_dma_data .tcm_gfw_traffic_overlay_dma_data.*)
  } :TCM_GFW_TRAFFIC_OVERLAY =0

  .  = ALIGN(__cache_line_size__);  
  __tcm_gfw_traffic_overlay_dma_data_end__ = . ;

  __tcm_gfw_traffic_overlay_dma_bss_start__ = . ;
  .tcm_gfw_traffic_overlay_dma_bss (NOLOAD) : AT (__load_location__ + (ADDR(.tcm_gfw_traffic_overlay_dma_bss) - __va_location__) )
  {
    *(.tcm_gfw_traffic_overlay_dma_bss .tcm_gfw_traffic_overlay_dma_bss.*)
  } :TCM_GFW_TRAFFIC_OVERLAY =0

  .  = ALIGN(__cache_line_size__);  
  __tcm_gfw_traffic_overlay_dma_bss_end__ = . ;

  . = ALIGN(__min_page_size__);
  __tcm_gfw_traffic_overlay_va_end__ = . ;

  __section_len__    = . - __va_location__;
  __pa_location__   += __section_len__ ;
  __va_location__   += __section_len__ + (__section_len__ > 0 ? __min_page_size__ : 0) ; 
  __load_location__ += __section_len__ + (__section_len__ > 0 ? __min_page_size__ : 0) ;

  __tcm_region1_size__ = MAX( __tcm_region1_size__ , __section_len__ );

  /*------------------------------------------------------------------------
    End of GERAN TCM traffic mode overlay.   
   ------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    TD-SCDMA traffic mode TCM overlay.   
    Note that the appropriate sections need to be added to PHDRS above also.
   ------------------------------------------------------------------------*/
  __pa_location__ = __tcm_region1_pa_start__ ;

  __quant_load_location__ = (__load_location__ / __max_page_size__) * __max_page_size__ + __tcm_region1_pa_ofs__;
  __load_location__ = (__quant_load_location__ >= __load_location__ ? __quant_load_location__ : __quant_load_location__ + __max_page_size__);
  __va_location__ = __load_location__ ;

  __tcm_tfw_traffic_overlay_va_load__  = __load_location__ ; 
  __tcm_tfw_traffic_overlay_pa_run__   = __pa_location__ ;
  __tcm_tfw_traffic_overlay_va_start__ = __va_location__ ;  
  .  = __va_location__ ;

  __tcm_tfw_traffic_overlay_data_start__ = . ;
  .tcm_tfw_traffic_overlay_data : AT (__load_location__ + (ADDR(.tcm_tfw_traffic_overlay_data) - __va_location__) )
  {
    *(.tcm_tfw_traffic_overlay_data .tcm_tfw_traffic_overlay_data.*)
  } :TCM_TFW_TRAFFIC_OVERLAY =0

  .  = ALIGN(__cache_line_size__);
  __tcm_tfw_traffic_overlay_data_end__ = . ;

  __tcm_tfw_traffic_overlay_code_start__ = . ;
  .tcm_tfw_traffic_overlay_code : AT (__load_location__ + (ADDR(.tcm_tfw_traffic_overlay_code) - __va_location__) )
  {
    *(.tcm_tfw_traffic_overlay_code .tcm_tfw_traffic_overlay_code.*)
  } :TCM_TFW_TRAFFIC_OVERLAY =0x00c0007f

  .  = ALIGN(__cache_line_size__);  
  __tcm_tfw_traffic_overlay_code_end__ = . ;

  /* If data is present, then we need a partition within the code segment to switch
     from WB to WT.
     Pad up to a 4k multiple if start and end address of code are in same page. */
  . += ( __tcm_tfw_traffic_overlay_data_end__ > __tcm_tfw_traffic_overlay_data_start__ ? ( (. & ~(__min_page_size__ - 1)) == (__tcm_tfw_traffic_overlay_code_start__ & ~(__min_page_size__ - 1)) ? __min_page_size__ - (. & (__min_page_size__ - 1)) : 0 ) : 0 );  

  __tcm_tfw_traffic_overlay_dma_data_start__ = . ;
  .tcm_tfw_traffic_overlay_dma_data : AT (__load_location__ + (ADDR(.tcm_tfw_traffic_overlay_dma_data) - __va_location__) )
  {
    *(.tcm_tfw_traffic_overlay_dma_data .tcm_tfw_traffic_overlay_dma_data.*)
  } :TCM_TFW_TRAFFIC_OVERLAY =0

  .  = ALIGN(__cache_line_size__);  
  __tcm_tfw_traffic_overlay_dma_data_end__ = . ;

  __tcm_tfw_traffic_overlay_dma_bss_start__ = . ;
  .tcm_tfw_traffic_overlay_dma_bss (NOLOAD) : AT (__load_location__ + (ADDR(.tcm_tfw_traffic_overlay_dma_bss) - __va_location__) )
  {
    *(.tcm_tfw_traffic_overlay_dma_bss .tcm_tfw_traffic_overlay_dma_bss.*)
  } :TCM_TFW_TRAFFIC_OVERLAY =0

  .  = ALIGN(__cache_line_size__);  
  __tcm_tfw_traffic_overlay_dma_bss_end__ = . ;

  . = ALIGN(__min_page_size__);
  __tcm_tfw_traffic_overlay_va_end__ = . ;

  __section_len__    = . - __va_location__;
  __pa_location__   += __section_len__ ;
  __va_location__   += __section_len__ + (__section_len__ > 0 ? __min_page_size__ : 0) ; 
  __load_location__ += __section_len__ + (__section_len__ > 0 ? __min_page_size__ : 0) ;

  __tcm_region1_size__ = MAX( __tcm_region1_size__ , __section_len__ );

  /*------------------------------------------------------------------------
    End of GERAN TCM traffic mode overlay.   
   ------------------------------------------------------------------------*/


  /*========================================================================
    TCM region2. Region2 and region3 and region4 are sequential and overlayed together 
    with region1.  
    ========================================================================*/
  __pa_location__          =  __tcm_region1_pa_start__ ;
  __tcm_region2_pa_start__ = __pa_location__ ;
  __tcm_region2_pa_ofs__   = (__pa_location__) & (__max_page_size__ - 1);
  __tcm_region2_size__     = 0;


  /*------------------------------------------------------------------------
    1x traffic mode TCM overlay.   
    Note that the appropriate sections need to be added to PHDRS above also.
   ------------------------------------------------------------------------*/
  __pa_location__ = __tcm_region2_pa_start__ ;

  __quant_load_location__ = (__load_location__ / __max_page_size__) * __max_page_size__ + __tcm_region2_pa_ofs__;
  __load_location__ = (__quant_load_location__ >= __load_location__ ? __quant_load_location__ : __quant_load_location__ + __max_page_size__);
  __va_location__ = __load_location__ ;

  __tcm_cdma1x_traffic_overlay_va_load__  = __load_location__ ; 
  __tcm_cdma1x_traffic_overlay_pa_run__   = __pa_location__ ;
  __tcm_cdma1x_traffic_overlay_va_start__ = __va_location__ ;  
  .  = __va_location__ ;

  __tcm_cdma1x_traffic_overlay_data_start__ = . ;
  .tcm_cdma1x_traffic_overlay_data : AT (__load_location__ + (ADDR(.tcm_cdma1x_traffic_overlay_data) - __va_location__) )
  {
    *(.tcm_cdma1x_traffic_overlay_data .tcm_cdma1x_traffic_overlay_data.*)
  } :TCM_CDMA1X_TRAFFIC_OVERLAY =0

  .  = ALIGN(__cache_line_size__);
  __tcm_cdma1x_traffic_overlay_data_end__ = . ;

  __tcm_cdma1x_traffic_overlay_code_start__ = . ;
  .tcm_cdma1x_traffic_overlay_code : AT (__load_location__ + (ADDR(.tcm_cdma1x_traffic_overlay_code) - __va_location__) )
  {
    *(.tcm_cdma1x_traffic_overlay_code .tcm_cdma1x_traffic_overlay_code.*)
  } :TCM_CDMA1X_TRAFFIC_OVERLAY =0x00c0007f

  .  = ALIGN(__cache_line_size__);  
  __tcm_cdma1x_traffic_overlay_code_end__ = . ;

  /* If data is present, then we need a partition within the code segment to switch
     from WB to WT.
     Pad up to a 4k multiple if start and end address of code are in same page. */
  . += ( __tcm_cdma1x_traffic_overlay_data_end__ > __tcm_cdma1x_traffic_overlay_data_start__ ? ( (. & ~(__min_page_size__ - 1)) == (__tcm_cdma1x_traffic_overlay_code_start__ & ~(__min_page_size__ - 1)) ? __min_page_size__ - (. & (__min_page_size__ - 1)) : 0 ) : 0 );  

  __tcm_cdma1x_traffic_overlay_dma_data_start__ = . ;
  .tcm_cdma1x_traffic_overlay_dma_data : AT (__load_location__ + (ADDR(.tcm_cdma1x_traffic_overlay_dma_data) - __va_location__) )
  {
    *(.tcm_cdma1x_traffic_overlay_dma_data .tcm_cdma1x_traffic_overlay_dma_data.*)
  } :TCM_CDMA1X_TRAFFIC_OVERLAY =0

  .  = ALIGN(__cache_line_size__);  
  __tcm_cdma1x_traffic_overlay_dma_data_end__ = . ;

  __tcm_cdma1x_traffic_overlay_dma_bss_start__ = . ;
  .tcm_cdma1x_traffic_overlay_dma_bss (NOLOAD) : AT (__load_location__ + (ADDR(.tcm_cdma1x_traffic_overlay_dma_bss) - __va_location__) )
  {
    *(.tcm_cdma1x_traffic_overlay_dma_bss .tcm_cdma1x_traffic_overlay_dma_bss.*)
  } :TCM_CDMA1X_TRAFFIC_OVERLAY =0

  .  = ALIGN(__cache_line_size__);  
  __tcm_cdma1x_traffic_overlay_dma_bss_end__ = . ;

  . = ALIGN(__min_page_size__);
  __tcm_cdma1x_traffic_overlay_va_end__ = . ;

  __section_len__    = . - __va_location__;
  __pa_location__   += __section_len__ ;
  __va_location__   += __section_len__ + (__section_len__ > 0 ? __min_page_size__ : 0) ; 
  __load_location__ += __section_len__ + (__section_len__ > 0 ? __min_page_size__ : 0) ;

  __tcm_region2_size__ = MAX( __tcm_region2_size__ , __section_len__ );

  /*------------------------------------------------------------------------
    End of 1x TCM traffic mode overlay.
   ------------------------------------------------------------------------*/

   /*------------------------------------------------------------------------
    XLTE TCM overlay.
    This is overlayed with the 1x memory when 1x is not present, otherwise
    enabled in external memory.
   ------------------------------------------------------------------------*/
  __pa_location__ = __tcm_region2_pa_start__ ;

  __quant_load_location__ = (__load_location__ / __max_page_size__) * __max_page_size__ + __tcm_region2_pa_ofs__;
  __load_location__ = (__quant_load_location__ >= __load_location__ ? __quant_load_location__ : __quant_load_location__ + __max_page_size__);
  __va_location__ = __load_location__ ;

  __tcm_xlte_overlay_va_load__  = __load_location__ ;
  __tcm_xlte_overlay_pa_run__   = __pa_location__ ;
  __tcm_xlte_overlay_va_start__ = __va_location__ ;
  .  = __va_location__ ;

  __tcm_xlte_overlay_code_start__ = . ;
  .tcm_xlte_overlay_code : AT (__load_location__ + (ADDR(.tcm_xlte_overlay_code) - __va_location__) )
  {
    *(.tcm_xlte_overlay_code .tcm_xlte_overlay_code.*)
  } :TCM_XLTE_OVERLAY =0x00c0007f

  .  = ALIGN(__cache_line_size__);
  __tcm_xlte_overlay_code_end__ = . ;

  . = ALIGN(__min_page_size__);
  __tcm_xlte_overlay_va_end__ = . ;

  __assert_sink__ = ASSERT( (__tcm_xlte_overlay_va_end__ - __tcm_xlte_overlay_va_start__) <= 48K , "Overflowed TCM - XLTE Overlay" );

  __section_len__    = . - __va_location__;
  __pa_location__   += __section_len__ ;
  __va_location__   += __section_len__ + (__section_len__ > 0 ? __min_page_size__ : 0) ;
  __load_location__ += __section_len__ + (__section_len__ > 0 ? __min_page_size__ : 0) ;

  __tcm_region2_size__ = MAX( __tcm_region2_size__ , __section_len__ );

  /*------------------------------------------------------------------------
    End of XLTE TCM overlay.
   ------------------------------------------------------------------------*/

  __tcm_region2_pa_end__ = __tcm_region2_pa_start__ + __tcm_region2_size__ ;

  /*========================================================================
    End of TCM region2
    ========================================================================*/


  /*========================================================================
    TCM region3. Concatenated with region2, together Region2 and region3 and 
    region 4 are overlayed with region1.  
    ========================================================================*/
  __pa_location__          =  __tcm_region2_pa_end__ ;
  __tcm_region3_pa_start__ = __pa_location__ ;
  __tcm_region3_pa_ofs__   = (__pa_location__) & (__max_page_size__ - 1);
  __tcm_region3_size__     = 0;


  /*------------------------------------------------------------------------
    C2K traffic mode TCM overlay.   
    Note that the appropriate sections need to be added to PHDRS above also.
   ------------------------------------------------------------------------*/
  __pa_location__ = __tcm_region3_pa_start__ ;

  __quant_load_location__ = (__load_location__ / __max_page_size__) * __max_page_size__ + __tcm_region3_pa_ofs__;
  __load_location__ = (__quant_load_location__ >= __load_location__ ? __quant_load_location__ : __quant_load_location__ + __max_page_size__);
  __va_location__ = __load_location__ ;

  __tcm_c2k_traffic_overlay_va_load__  = __load_location__ ; 
  __tcm_c2k_traffic_overlay_pa_run__   = __pa_location__ ;
  __tcm_c2k_traffic_overlay_va_start__ = __va_location__ ;  
  .  = __va_location__ ;

  __tcm_c2k_traffic_overlay_data_start__ = . ;
  .tcm_c2k_traffic_overlay_data : AT (__load_location__ + (ADDR(.tcm_c2k_traffic_overlay_data) - __va_location__) )
  {
    *(.tcm_c2k_traffic_overlay_data .tcm_c2k_traffic_overlay_data.*)
  } :TCM_C2K_TRAFFIC_OVERLAY =0

  .  = ALIGN(__cache_line_size__);
  __tcm_c2k_traffic_overlay_data_end__ = . ;

  __tcm_c2k_traffic_overlay_code_start__ = . ;
  .tcm_c2k_traffic_overlay_code : AT (__load_location__ + (ADDR(.tcm_c2k_traffic_overlay_code) - __va_location__) )
  {
    *(.tcm_c2k_traffic_overlay_code .tcm_c2k_traffic_overlay_code.*)
  } :TCM_C2K_TRAFFIC_OVERLAY =0x00c0007f

  .  = ALIGN(__cache_line_size__);  
  __tcm_c2k_traffic_overlay_code_end__ = . ;

  /* If data is present, then we need a partition within the code segment to switch
     from WB to WT.
     Pad up to a 4k multiple if start and end address of code are in same page. */
  . += ( __tcm_c2k_traffic_overlay_data_end__ > __tcm_c2k_traffic_overlay_data_start__ ? ( (. & ~(__min_page_size__ - 1)) == (__tcm_c2k_traffic_overlay_code_start__ & ~(__min_page_size__ - 1)) ? __min_page_size__ - (. & (__min_page_size__ - 1)) : 0 ) : 0 );  

  __tcm_c2k_traffic_overlay_dma_data_start__ = . ;
  .tcm_c2k_traffic_overlay_dma_data : AT (__load_location__ + (ADDR(.tcm_c2k_traffic_overlay_dma_data) - __va_location__) )
  {
    *(.tcm_c2k_traffic_overlay_dma_data .tcm_c2k_traffic_overlay_dma_data.*)
  } :TCM_C2K_TRAFFIC_OVERLAY =0

  .  = ALIGN(__cache_line_size__);  
  __tcm_c2k_traffic_overlay_dma_data_end__ = . ;

  __tcm_c2k_traffic_overlay_dma_bss_start__ = . ;
  .tcm_c2k_traffic_overlay_dma_bss (NOLOAD) : AT (__load_location__ + (ADDR(.tcm_c2k_traffic_overlay_dma_bss) - __va_location__) )
  {
    *(.tcm_c2k_traffic_overlay_dma_bss .tcm_c2k_traffic_overlay_dma_bss.*)
  } :TCM_C2K_TRAFFIC_OVERLAY =0

  .  = ALIGN(__cache_line_size__);  
  __tcm_c2k_traffic_overlay_dma_bss_end__ = . ;

  . = ALIGN(__min_page_size__);
  __tcm_c2k_traffic_overlay_va_end__ = . ;

  __section_len__    = . - __va_location__;
  __pa_location__   += __section_len__ ;
  __va_location__   += __section_len__ + (__section_len__ > 0 ? __min_page_size__ : 0) ; 
  __load_location__ += __section_len__ + (__section_len__ > 0 ? __min_page_size__ : 0) ;

  __tcm_region3_size__ = MAX( __tcm_region3_size__ , __section_len__ );

  /*------------------------------------------------------------------------
    End of C2K TCM traffic mode overlay.   
   ------------------------------------------------------------------------*/
  
  __tcm_region3_pa_end__ = __tcm_region3_pa_start__ + __tcm_region3_size__ ;
  
  /*========================================================================
    End of TCM region3
    ========================================================================*/


  /*========================================================================
    TCM region4. Concatenated with region3, together Region2 and region3 and 
    region 4 are overlayed with region1.  
    ========================================================================*/
  __pa_location__          =  __tcm_region3_pa_end__ ;
  __tcm_region4_pa_start__ = __pa_location__ ;
  __tcm_region4_pa_ofs__   = (__pa_location__) & (__max_page_size__ - 1);
  __tcm_region4_size__     = 0;


  /*------------------------------------------------------------------------
    MCDO traffic mode TCM overlay.   
    Note that the appropriate sections need to be added to PHDRS above also.
   ------------------------------------------------------------------------*/
  __pa_location__ = __tcm_region4_pa_start__ ;

  __quant_load_location__ = (__load_location__ / __max_page_size__) * __max_page_size__ + __tcm_region4_pa_ofs__;
  __load_location__ = (__quant_load_location__ >= __load_location__ ? __quant_load_location__ : __quant_load_location__ + __max_page_size__);
  __va_location__ = __load_location__ ;

  __tcm_mcdo_traffic_overlay_va_load__  = __load_location__ ; 
  __tcm_mcdo_traffic_overlay_pa_run__   = __pa_location__ ;
  __tcm_mcdo_traffic_overlay_va_start__ = __va_location__ ;  
  .  = __va_location__ ;

  __tcm_mcdo_traffic_overlay_data_start__ = . ;
  .tcm_mcdo_traffic_overlay_data : AT (__load_location__ + (ADDR(.tcm_mcdo_traffic_overlay_data) - __va_location__) )
  {
    *(.tcm_mcdo_traffic_overlay_data .tcm_mcdo_traffic_overlay_data.*)
  } :TCM_MCDO_TRAFFIC_OVERLAY =0

  .  = ALIGN(__cache_line_size__);
  __tcm_mcdo_traffic_overlay_data_end__ = . ;

  __tcm_mcdo_traffic_overlay_code_start__ = . ;
  .tcm_mcdo_traffic_overlay_code : AT (__load_location__ + (ADDR(.tcm_mcdo_traffic_overlay_code) - __va_location__) )
  {
    *(.tcm_mcdo_traffic_overlay_code .tcm_mcdo_traffic_overlay_code.*)
  } :TCM_MCDO_TRAFFIC_OVERLAY =0x00c0007f

  .  = ALIGN(__cache_line_size__);  
  __tcm_mcdo_traffic_overlay_code_end__ = . ;

  /* If data is present, then we need a partition within the code segment to switch
     from WB to WT.
     Pad up to a 4k multiple if start and end address of code are in same page. */
  . += ( __tcm_mcdo_traffic_overlay_data_end__ > __tcm_mcdo_traffic_overlay_data_start__ ? ( (. & ~(__min_page_size__ - 1)) == (__tcm_mcdo_traffic_overlay_code_start__ & ~(__min_page_size__ - 1)) ? __min_page_size__ - (. & (__min_page_size__ - 1)) : 0 ) : 0 );  

  __tcm_mcdo_traffic_overlay_dma_data_start__ = . ;
  .tcm_mcdo_traffic_overlay_dma_data : AT (__load_location__ + (ADDR(.tcm_mcdo_traffic_overlay_dma_data) - __va_location__) )
  {
    *(.tcm_mcdo_traffic_overlay_dma_data .tcm_mcdo_traffic_overlay_dma_data.*)
  } :TCM_MCDO_TRAFFIC_OVERLAY =0

  .  = ALIGN(__cache_line_size__);  
  __tcm_mcdo_traffic_overlay_dma_data_end__ = . ;

  __tcm_mcdo_traffic_overlay_dma_bss_start__ = . ;
  .tcm_mcdo_traffic_overlay_dma_bss (NOLOAD) : AT (__load_location__ + (ADDR(.tcm_mcdo_traffic_overlay_dma_bss) - __va_location__) )
  {
    *(.tcm_mcdo_traffic_overlay_dma_bss .tcm_mcdo_traffic_overlay_dma_bss.*)
  } :TCM_MCDO_TRAFFIC_OVERLAY =0

  .  = ALIGN(__cache_line_size__);  
  __tcm_mcdo_traffic_overlay_dma_bss_end__ = . ;

  . = ALIGN(__min_page_size__);
  __tcm_mcdo_traffic_overlay_va_end__ = . ;

  __section_len__    = . - __va_location__;
  __pa_location__   += __section_len__ ;
  __va_location__   += __section_len__ + (__section_len__ > 0 ? __min_page_size__ : 0) ; 
  __load_location__ += __section_len__ + (__section_len__ > 0 ? __min_page_size__ : 0) ;

  __tcm_region4_size__ = MAX( __tcm_region4_size__ , __section_len__ );

  /*------------------------------------------------------------------------
    End of MCDO TCM traffic mode overlay.   
   ------------------------------------------------------------------------*/



  /*------------------------------------------------------------------------
    LTE traffic mode TCM overlay.   
    Note that the appropriate sections need to be added to PHDRS above also.
   ------------------------------------------------------------------------*/
  __pa_location__ = __tcm_region4_pa_start__ ;

  __quant_load_location__ = (__load_location__ / __max_page_size__) * __max_page_size__ + __tcm_region4_pa_ofs__;
  __load_location__ = (__quant_load_location__ >= __load_location__ ? __quant_load_location__ : __quant_load_location__ + __max_page_size__);
  __va_location__ = __load_location__ ;

  __tcm_lte_traffic_overlay_va_load__  = __load_location__ ; 
  __tcm_lte_traffic_overlay_pa_run__   = __pa_location__ ;
  __tcm_lte_traffic_overlay_va_start__ = __va_location__ ;  
  .  = __va_location__ ;

  __tcm_lte_traffic_overlay_data_start__ = . ;
  .tcm_lte_traffic_overlay_data : AT (__load_location__ + (ADDR(.tcm_lte_traffic_overlay_data) - __va_location__) )
  {
    *(.tcm_lte_traffic_overlay_data .tcm_lte_traffic_overlay_data.*)
  } :TCM_LTE_TRAFFIC_OVERLAY =0

  .  = ALIGN(__cache_line_size__);
  __tcm_lte_traffic_overlay_data_end__ = . ;

  __tcm_lte_traffic_overlay_code_start__ = . ;
  .tcm_lte_traffic_overlay_code : AT (__load_location__ + (ADDR(.tcm_lte_traffic_overlay_code) - __va_location__) )
  {
    *(.tcm_lte_traffic_overlay_code .tcm_lte_traffic_overlay_code.*)
  } :TCM_LTE_TRAFFIC_OVERLAY =0x00c0007f

  .  = ALIGN(__cache_line_size__);  
  __tcm_lte_traffic_overlay_code_end__ = . ;

  /* If data is present, then we need a partition within the code segment to switch
     from WB to WT.
     Pad up to a 4k multiple if start and end address of code are in same page. */
  . += ( __tcm_lte_traffic_overlay_data_end__ > __tcm_lte_traffic_overlay_data_start__ ? ( (. & ~(__min_page_size__ - 1)) == (__tcm_lte_traffic_overlay_code_start__ & ~(__min_page_size__ - 1)) ? __min_page_size__ - (. & (__min_page_size__ - 1)) : 0 ) : 0 );  

  __tcm_lte_traffic_overlay_dma_data_start__ = . ;
  .tcm_lte_traffic_overlay_dma_data : AT (__load_location__ + (ADDR(.tcm_lte_traffic_overlay_dma_data) - __va_location__) )
  {
    *(.tcm_lte_traffic_overlay_dma_data .tcm_lte_traffic_overlay_dma_data.*)
  } :TCM_LTE_TRAFFIC_OVERLAY =0

  .  = ALIGN(__cache_line_size__);  
  __tcm_lte_traffic_overlay_dma_data_end__ = . ;

  __tcm_lte_traffic_overlay_dma_bss_start__ = . ;
  .tcm_lte_traffic_overlay_dma_bss (NOLOAD) : AT (__load_location__ + (ADDR(.tcm_lte_traffic_overlay_dma_bss) - __va_location__) )
  {
    *(.tcm_lte_traffic_overlay_dma_bss .tcm_lte_traffic_overlay_dma_bss.*)
  } :TCM_LTE_TRAFFIC_OVERLAY =0

  .  = ALIGN(__cache_line_size__);  
  __tcm_lte_traffic_overlay_dma_bss_end__ = . ;

  . = ALIGN(__min_page_size__);
  __tcm_lte_traffic_overlay_va_end__ = . ;

  __section_len__    = . - __va_location__;
  __pa_location__   += __section_len__ ;
  __va_location__   += __section_len__ + (__section_len__ > 0 ? __min_page_size__ : 0) ; 
  __load_location__ += __section_len__ + (__section_len__ > 0 ? __min_page_size__ : 0) ;

  __tcm_region4_size__ = MAX( __tcm_region4_size__ , __section_len__ );

  /*------------------------------------------------------------------------
    End of LTE TCM traffic mode overlay.   
   ------------------------------------------------------------------------*/

  __tcm_region4_pa_end__ = __tcm_region4_pa_start__ + __tcm_region4_size__ ;

  /*========================================================================
    End of TCM region4
    ========================================================================*/

  /* Region 1 overlayed with concatenation of region2 and region3 and region4*/
  __tcm_region1_size__ = MAX( __tcm_region1_size__ , __tcm_region4_pa_end__ - __tcm_region2_pa_start__ );
  __tcm_region1_pa_end__ = __tcm_region1_pa_start__ + __tcm_region1_size__ ;


  /*========================================================================
    End of TCM region1
    ========================================================================*/

  /* Verify that we haven't used more TCM than allocated */
  __pa_location__ = __tcm_region1_pa_end__ ;
  __tcm_size__    = __pa_location__ - __tcm_pa_base__ ;
  __assert_sink__ = ASSERT( (__tcm_size__ + __tcm_dynamic_size__ ) <= __tcm_max_size__ , "Image overflowed TCM - see linker script" );


