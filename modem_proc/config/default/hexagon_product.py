#==========================================================================
#
#  QC SCons overrides file for hexagon builders:
#
#  This file is used to temporarily override Hexagon compiler options 
#  during the transition to LLVM for mpss.dpm.2.0.
#
#  Name this file hexagon_oem.py and place it in config/default.  The
#  environment variable BUILD_SCRIPTS_OEM_ROOT must point to config/default.
#
#==========================================================================

import os

#--------------------------------------------------------------------------
# Hooks for SCons
#--------------------------------------------------------------------------

def exists(env):
   return env.Detect('hexagon_product')

def generate(env):

   ENDFLAGS = ' -Wno-enum-compare -Wno-tautological-constant-out-of-range-compare '

   env.Prepend(HEXAGON_COMPILE_CMD = ENDFLAGS)
