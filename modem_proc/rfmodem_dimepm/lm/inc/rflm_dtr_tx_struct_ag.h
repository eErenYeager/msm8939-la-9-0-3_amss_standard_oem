
#ifndef RFLM_DTR_TX_STRUCT_AG_H
#define RFLM_DTR_TX_STRUCT_AG_H


#ifdef __cplusplus
extern "C" {
#endif

/*
WARNING: This file is auto-generated.

Generated at:    Mon Oct  6 13:08:44 2014
Generated using: lm_autogen.pl
Generated from:  v5.6.4 of DimePM_TXLM_register_settings.xlsx
*/

/*=============================================================================

           T X    A U T O G E N    F I L E

GENERAL DESCRIPTION
  This file is auto-generated and it captures the modem register settings 
  configured by FW, provided by the rflm_dtr_tx.

Copyright (c) 2009, 2010, 2011, 2012, 2013, 2014 by Qualcomm Technologies, Inc.  All Rights Reserved.

$DateTime: 2015/01/27 06:42:19 $
$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/rfmodem_dimepm/lm/inc/rflm_dtr_tx_struct_ag.h#1 $

=============================================================================*/

/*=============================================================================
                           REVISION HISTORY
Version    Author   Date   
         Comments   IR Number   Other POC (Comma Delimited)   Release Validation Notes   Dependencies   AG File Location   Changes for   
5.6.4   nichunw   10/1/2014   
         1. Change REF_DWN_FACTOR to 1 for TDS_sAPT states in DAC rate 230.4MHz to lower down the DPD capture rate by half.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            TDS   
5.6.3   nichunw   9/4/2014   
         1. Updated DP_PEQ_SW_Dynamic tab to contain the complete register setting for TXC_DP_CFG_An.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            LTE   
5.6.2   nichunw   8/6/2014   
         1. Added chain dependency to the avg_error and overflow bit fix. The pass in value is now in the format of [overflow_1(5), avg_error_1(4), overflow_0(3), avg_error_0(2), foundry_ID(1:0)]  2. Added COMBODAC_CAL_CTRL_c::FSM_Cal_MODE.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            DAC   
5.6.1   nichunw   7/31/2014   
         1. Update IREF_LUT for Samsung. New settings are updated by DAC team. (CR 702890) 2. Added the fix for DAC avg_error and overflow bits by using the foundry column. This change will prevent those two bits being overwritten to 0 during txlm configuration. The second value in foundry column is the bit mask and it will AND with the pass in value [overflow(3), avg_error(2), foundry_ID(1:0)]. If the result matches with the first value in the foundry column, that row will be selected.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
5.6.0   nichunw   7/18/2014   
         1. Added a new txlm variable TXC_input_freq_hz.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
5.5.2   nichunw   7/16/2014   
         1. Revert REF_MODE and MODE_256 settings to unblock DPD IQ captures.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
5.5.1   nichunw   7/11/2014   
         1. Updated DP_PEQ_SW_Dynamic tab for filter settings for WTR4905.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
5.5.0   nichunw   6/19/2014   
         1. Added txlm var DAC_freq_khz. 2. Change state specific mask write to normal mask write for  COMBODAC_CLK_SRC_CFG and COMBODAC_DIGVAL_EN_c      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
5.4.6   nichunw   6/18/2014   
         1. Added foundry-dependent dac config on ComboDAC_cfg_SW_Dynamic tab. 2. Adding foundry-dependent IREF LUTs for TSMC, GF and SEC. 3. Changed REF_MODE to 1 and MODE_256 to 1 for all states. This is for ILPC and LOFT/RSB algorithms.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
5.4.5   nichunw   5/15/2014   
         1. Change PP_GAIN for GSM 76.8MHz to 44373.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            GSM   
5.4.4   nichunw   5/8/2014   
         1. Updated DAC config for TSMC and GF. 2. Added SEC dac config.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            DAC   
5.4.3   nichunw   5/5/2014   
         1. Remove COMBODAC_RTUNE_RW_CTRL_c since it is only needed during DAC powerup.       nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            DAC   
5.4.2   nichunw   5/4/2014   
         1. Added mask write for COMBODAC_CLK_SRC_CFG and COMBODAC_DIGVAL_EN_c.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            DAC   
5.4.1   nichunw   5/2/2014   
         1. Updated IREF table for the -18dB entry. 2. Disable EP_PEQ1/PEQ2_ENABLE for LTE, UMTS, 1x, DO. EP is not used by those techs. 3. Change TWO_SAMPLES_CYCLE to 1 for 1x and TDS 230.4MHz.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
5.4.0   nichunw   4/24/2014   
         1. Added TXFE_DELAYMATCH block for SW to dynamically changing the DM registers. This is for CS2.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
5.3.11   nichunw   4/15/2014   
         1. Make TXC subgroup SW_dynamic, this is mainly for EP_DM2_FINE_TAU and EP_DM3_FRAC_TAU.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
5.3.10   nichunw   4/9/2014   
         1. Add Foundry column to have foundry-dependent DAC cfg. The default setting is TSMC DAC cfg.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            DAC   
5.3.9   nichunw   3/19/2014   
         1. Revert back to the 5.3.7. The IREF changes in 5.3.8 are not needed anymore. The iref gain fix is totally on FW, during DAC reactivation.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            DAC   
5.3.8   nichunw   3/18/2014   
         1. Added IREF start/stop time and trigger values. This is to fix the 3dB power drop issue seen on GF parts. The fix applies to all states/all foundries.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            DAC   
5.3.7   nichunw   3/12/2014   
         1. Added IP2cal state for GSM (for WTR4905 only).      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            GSM   
5.3.6   nichunw   3/11/2014   
         1. Change the dac config for all non-LTE states (with WTR1605/2605) to LP mode.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            DAC   
5.3.5   nichunw   3/6/2014   
         1. Fixed the typo on ComboDAC_cfg_SW_Dynamic tab.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            DAC   
5.3.4   nichunw   3/3/2014   
         1. Changed COMBODAC_CFG settings for DAC calibration on DAC_cal_LM_settings tab 2. Changed COMBODAC_CFG settings for all CS2 techs states (1x/DO/UMTS at 230.4MHz and GSM at 76.8MHz) except LTE. 3. Reduced the states in ComboDAC_cfg_SW_Dynamic tab to LTE only. 4. Add two more columns in DAC_cal_LM_settings tab to include the CS2 COMBODAC_CFG settings. 5. Added DAC temp comp info on IREF_Dynamic tab.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
5.3.3   nichunw   2/13/2014   
         1. Removed entries of the state variable 'carrier_cfg' from HDR states. 2. Replaced state variable 'DAC_freq' by device to inidcate the dependency on WTR models.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
5.3.2   nichunw   2/2/2014   
         1. Reorder the register order in DAC_cal_LM_settings tab to follow the recommended sequence from DAC team.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
5.3.1   nichunw   1/15/2013   
         1. Filled in default RT_QFUSE_CODE with 128. 2. Filled in value for row TX_DIGITAL_DELAY_MICROSECS_Q16 for GSM. 3. Fixed index issue in DAC_Cal_control_registers tab. 4. Made TXC_DP_CFG subgroup SW_dynamic.       nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
5.3.0   nichunw   1/13/2014   
         1. Remove the redundant white space in DAC_CLK_EXT_XO_EN in DAC_cal_LM_settings tab. 2. Change modem version to DimePM. 3. Updated DAC_cal_LM_settings and DAC_Cal_control_registers tabs. 4. Added TXCR_RESET section at the bottom. 5. Filled in value for row TX_DIGITAL_DELAY_MICROSECS_Q16 except GSM.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
5.2.1   nichunw   12/20/2013   
         1. Updated the format and look-up table in IREF_Dynamic. 2. Added ComboDAC_cfg_SW_Dynamic tab for the different combodac_cfg settings in different WTRs.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
5.2.0   vikramr/nichunw   12/16/2013   
         1. Simplified the chain indices to only two sets. 2. Add COMBODAC_CLK_SRC_CFG::DAC_CLK_SEL=3 for all techs to complete the DAC clock settings. 3. DAC_CAL_LUT section is removed. 4. Fixed the typo in the name of register TXC_DP_PEQ1_Q_SET2_An. It was TXC_DP_PEQ1_SET2_An in the previous release. 5. Updated the DP_PEQ_SW_dynamic, DAC_cal_LM_settings, DAC_Cal_Control_registers tabs.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
5.1.0   nichunw   11/21/2013   
         1. Added DAC frequency parameters for all txlm states other than LTE. 2. Added COMBODAC_CFG2_c and updated the COMBODAC_CFG values. 3. Added IREF_UPDATE section to include iref-update related registers. 4. Merged MDM_CLK0 and MDM_CLK1 into single section. 5. Added DAC_POWER_DOWN and DAC_REACTIVATION sections. This change is made for common FW to update the DAC clock rate.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
5.0.1   nichunw   11/11/2013   
         Removing COMBODAC_MSBCAL and COMBODAC_DCCAL       nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com               
5.0.0   nichunw   11/8/2013   
         Removing deprecated registers.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com               
1.0.1   nichunw   11/8/2013   
         Remove COMBODAC_CTRL_c, MSS_DSM_PHASE_HIGHZ .      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com               
1.0.0   nichunw   10/30/2013   
         initial version      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com               
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              

=============================================================================*/
/*=============================================================================
                           INCLUDE FILES
=============================================================================*/

#include "rflm_dtr_tx_fw_intf_ag.h" 



/* This struct captures flags that control whether certain groups of registers are programmed for a given state. */
typedef struct
{
  boolean mtc_common_flag;/* This flag enables programming of the TXC registers to program both the MTC_TRIG blocks*/
  boolean mdm_clk1_flag;/* This flag enables programming of the MTC registers to program the CLK_DAC for Tx chain 1*/
  boolean mdm_clk0_flag;/* This flag enables programming of the MTC registers to program the CLK_DAC for Tx chain 1*/
  boolean tx_common_flag;/* This flag enables programming of the TXC registers to program both the Tx chains*/
  boolean iref_update_flag;/* This flag enables programming of the DAC IREF registers to program both the IREF gain value for both chains*/
  boolean tx_mod_cdma_flag;/* This flag enables programming of the TXC registers to program the Tx Mod (for CDMA)*/
  boolean txc_flag;/* This flag enables programming of the TXC registers to program the Tx chain 0*/
  boolean txr_flag;/* This flag enables programming of the TXR registers to program the Tx chain n*/
  boolean txcr_reset_flag;/* This flag enables programming of the TXC registers to program both the Tx chains*/
}rflm_dtr_tx_block_valid_flags_struct;



 /* Group MTC_TRIG: */ 
/* This block consist of the TXC registers to program both the MTC_TRIG blocks*/
typedef struct
{
  uint32 tx_cn_ctrl_sel;
  uint32 tx_cn_trig_cmd;
  uint32 tx_cn_trig_val;
}rflm_dtr_tx_mtc_trig_group_struct;



 /* Group DAC_DOWN: */ 
/* This block consist of the TXC registers to program both the MTC_TRIG blocks*/
typedef struct
{
  uint32 combodac_digval_en_c;
}rflm_dtr_tx_dac_down_group_struct;



 /* Group DAC_CLK: */ 
/* This block consist of the TXC registers to program both the MTC_TRIG blocks*/
typedef struct
{
  uint32 combodac_clk_src_cfg;
}rflm_dtr_tx_dac_clk_group_struct;



 /* Group MTC1: */ 
/* This block consist of the MTC registers to program the CLK_DAC for Tx chain 1*/
typedef struct
{
  uint32 combodac1_clk_cfg;
}rflm_dtr_tx_mtc1_group_struct;



 /* Group MTC: */ 
/* This block consist of the MTC registers to program the CLK_DAC for Tx chain 1*/
typedef struct
{
  uint32 combodac0_clk_cfg;
}rflm_dtr_tx_mtc_group_struct;



 /* Group TX_EN: */ 
/* This block consist of the TXC registers to program both the Tx chains*/
typedef struct
{
  uint32 tx_enable;
}rflm_dtr_tx_tx_en_group_struct;



 /* Group TX_TOP_GSM: */ 
/* This block consist of the TXC registers to program both the Tx chains*/
typedef struct
{
  uint32 tx_enable_gsm;
  uint32 tx_enable_gsm_mask;
  uint32 tx_enable_gsm_1;
  uint32 tx_enable_gsm_1_mask;
  uint32 tx_freq_out_sel;
  uint32 tx_freq_out_sel_mask;
}rflm_dtr_tx_tx_top_gsm_group_struct;



 /* Group TX_TOP: */ 
/* This block consist of the TXC registers to program both the Tx chains*/
typedef struct
{
  uint32 tx_mode_common;
  uint32 tx_mode_common_mask;
  uint32 tx_mode_ofdma;
  uint32 tx_mode_ofdma_mask;
  uint32 tx_mode_cdma;
  uint32 tx_mode_cdma_mask;
  uint32 tx_mode_umts;
  uint32 tx_mode_umts_mask;
  uint32 txc_mem_test;
  uint32 txr_ctrl;
  uint32 txr_ctrl_mask;
  uint32 tx_tds_ctrl;
  uint32 tx_tds_ctrl_mask;
  uint32 tx_tech_mux_ctrl;
  uint32 tx_tech_mux_ctrl_mask;
  uint32 tx_mem_access_cfg;
  uint32 tx_stmr_trig_frac_cnt_c;
  uint32 tx_mode_c;
}rflm_dtr_tx_tx_top_group_struct;



 /* Group TX_DAC_CFG: */ 
/* This block consist of the TXC registers to program both the Tx chains*/
typedef struct
{
  uint32 combodac_cfg0_c;
  uint32 combodac_cfg1_c;
  uint32 combodac_cfg2_c;
}rflm_dtr_tx_tx_dac_cfg_group_struct;



 /* Group TX_DAC_MODE: */ 
/* This block consist of the TXC registers to program both the Tx chains*/
typedef struct
{
  uint32 combodac_cal_ctrl_c;
}rflm_dtr_tx_tx_dac_mode_group_struct;



 /* Group MEMPOOLW: */ 
/* This block consist of the TXC registers to program both the Tx chains*/
typedef struct
{
  uint32 tx_mempool_wr_intf_ctl;
  uint32 tx_mempool_wr_intf_ctl_mask;
  uint32 tx_mempool_wr_intf_event;
  uint32 tx_mempool_wr_intf_event_mask;
}rflm_dtr_tx_mempoolw_group_struct;



 /* Group MEMPOOLR: */ 
/* This block consist of the TXC registers to program both the Tx chains*/
typedef struct
{
  uint32 tx_mempool_rd_intf_ctl;
  uint32 tx_mempool_rd_intf_ctl_mask;
  uint32 tx_mempool_rd_intf_event;
  uint32 tx_mempool_rd_intf_event_mask;
}rflm_dtr_tx_mempoolr_group_struct;



 /* Group IREF: */ 
/* This block consist of the DAC IREF registers to program both the IREF gain value for both chains*/
typedef struct
{
  uint32 tx_dacn_iref_gain;
  uint32 tx_dacn_update_strt_val;
  uint32 tx_dacn_update_strt_trig_cmd;
  uint32 tx_dacn_update_stop_val;
  uint32 tx_dacn_update_stop_trig_cmd;
}rflm_dtr_tx_iref_group_struct;



 /* Group TX_MOD: */ 
/* This block consist of the TXC registers to program the Tx Mod (for CDMA)*/
typedef struct
{
  uint32 mod_txc0_stb_sel;
  uint32 mod_txc0_stb_sel_mask;
  uint32 mod_txc1_stb_sel;
  uint32 mod_txc1_stb_sel_mask;
  uint32 mod_stall_sel;
  uint32 mod_stall_sel_mask;
  uint32 mod_offline_div;
}rflm_dtr_tx_tx_mod_group_struct;



 /* Group TIMESTAMP_TXC: */ 
/* This block consist of the TXC registers to program the Tx chain 0*/
typedef struct
{
  uint32 txc_timestamp_t1_an;
  uint32 txc_timestamp_t2_an;
}rflm_dtr_tx_timestamp_txc_group_struct;



 /* Group TXC_DP_CFG: */ 
/* This block consist of the TXC registers to program the Tx chain 0*/
typedef struct
{
  uint32 txc_tech_sel_an;
  uint32 txc_dp_cfg_2_an;
  uint32 txc_dp_cfg_an;
}rflm_dtr_tx_txc_dp_cfg_group_struct;



 /* Group TXC: */ 
/* This block consist of the TXC registers to program the Tx chain 0*/
typedef struct
{
  uint32 txc_dp_env_scale_val_an;
  uint32 txc_dp_env_scale_val_set2_an;
  uint32 txc_dp_phase_ovr_val_an;
  uint32 txc_pp_phase_ovr_val_an;
  uint32 txc_dp_env_ovr_val_an;
  uint32 txc_ep_env_ovr_val_an;
  uint32 txc_ep_env_ovr_val_set2_an;
  uint32 txc_ep_gain_an;
  uint32 txc_ep_gain_set2_an;
  uint32 txc_pp_gain_an;
  uint32 txc_pp_spill_an;
  uint32 txc_timestamp_reuse_an;
  uint32 txc_sample_count_ctl_an;
  uint32 txc_amam_pending_an;
  uint32 txc_ampm_pending_an;
  uint32 txc_amam_pending_set2_an;
  uint32 txc_ampm_pending_set2_an;
}rflm_dtr_tx_txc_group_struct;



 /* Group RAMP: */ 
/* This block consist of the TXC registers to program the Tx chain 0*/
typedef struct
{
  uint32 txc_dp_ramp_arm_an;
  uint32 txc_dp_ramp_start_time_an;
  uint32 txc_dp_ramp_slot_a_cfg_an;
  uint32 txc_dp_ramp_slot_b_cfg_an;
  uint32 txc_dp_ramp_gap_an;
}rflm_dtr_tx_ramp_group_struct;



 /* Group ROT: */ 
/* This block consist of the TXC registers to program the Tx chain 0*/
typedef struct
{
  uint32 txc_dp_rot_phase_inc_an;
  uint32 txc_dp_rot_phase_inc_set2_an;
  uint32 txc_dp_rot_phase_init_an;
  uint32 txc_dp_rot_phase_init_set2_an;
  uint32 txc_dp_rot_phase_shift_an;
  uint32 txc_dp_rot_phase_shift_set2_an;
}rflm_dtr_tx_rot_group_struct;



 /* Group IQMC: */ 
/* This block consist of the TXC registers to program the Tx chain 0*/
typedef struct
{
  uint32 txc_dp_iqmc_an;
  uint32 txc_dp_iqmc_set2_an;
}rflm_dtr_tx_iqmc_group_struct;



 /* Group PEQ: */ 
/* This block consist of the TXC registers to program the Tx chain 0*/
typedef struct
{
  uint32 txc_dp_peq1_an;
  uint32 txc_dp_peq1_set2_an;
  uint32 txc_dp_peq1_q_an;
  uint32 txc_dp_peq1_q_set2_an;
  uint32 txc_dp_peq2_an;
  uint32 txc_dp_peq2_set2_an;
}rflm_dtr_tx_peq_group_struct;



 /* Group DC: */ 
/* This block consist of the TXC registers to program the Tx chain 0*/
typedef struct
{
  uint32 txc_dp_dcoc_an;
  uint32 txc_dp_dcoc_set2_an;
  uint32 txc_ep_dcoc_an;
  uint32 txc_ep_dcoc_set2_an;
}rflm_dtr_tx_dc_group_struct;



 /* Group DM: */ 
/* This block consist of the TXC registers to program the Tx chain 0*/
typedef struct
{
  uint32 txc_dm1_cfg_an;
}rflm_dtr_tx_dm_group_struct;



 /* Group EP_PEQ: */ 
/* This block consist of the TXC registers to program the Tx chain 0*/
typedef struct
{
  uint32 txc_ep_peq1_an;
  uint32 txc_ep_peq1_set2_an;
  uint32 txc_ep_peq2_an;
}rflm_dtr_tx_ep_peq_group_struct;



 /* Group REF: */ 
/* This block consist of the TXC registers to program the Tx chain 0*/
typedef struct
{
  uint32 txc_ref_arm_an;
  uint32 txc_ref_dwn_factor_an;
  uint32 txc_ref_num_samples_an;
  uint32 txc_ref_start_time_an;
  uint32 txc_ref_mode_an;
  uint32 txc_ref_i_gain_val_an;
  uint32 txc_ref_q_gain_val_an;
  uint32 txc_ref_powercalc_or_log_an;
}rflm_dtr_tx_ref_group_struct;



 /* Group DBG: */ 
/* This block consist of the TXC registers to program the Tx chain 0*/
typedef struct
{
  uint32 txc_testbus_select_an;
}rflm_dtr_tx_dbg_group_struct;



 /* Group TXFE_DELAYMATCH: */ 
/* This block consist of the delay matching registers to program the Tx chain n*/
typedef struct
{
  uint32 txc_ep_dm2_fine_tau_an;
  uint32 txc_ep_dm3_frac_tau_an;
}rflm_dtr_tx_txfe_delaymatch_group_struct;



 /* Group DPIQGAIN_TXC: */ 
/* This block consist of the DP_IQ_GAIN registers to program the Tx chain n*/
typedef struct
{
  uint32 txc_dp_iq_gain_an;
  uint32 txc_dp_iq_gain_set2_an;
}rflm_dtr_tx_dpiqgain_txc_group_struct;



 /* Group TXR_IQ_DAC: */ 
/* This block consist of the TXR registers to program the Tx chain n*/
typedef struct
{
  uint32 txr_iq_dac_an;
}rflm_dtr_tx_txr_iq_dac_group_struct;



 /* Group TXR: */ 
/* This block consist of the TXR registers to program the Tx chain n*/
typedef struct
{
  uint32 txr_str_ctrl_an;
  uint32 txr_pda_ctrl_an;
  uint32 txr_pda_wd_update_ctrl_an;
  uint32 txr_pda_ctrl_wd_offset_an;
  uint32 txr_pda_offset_cmd_an;
  uint32 txr_fifo_cmd_an;
  uint32 txr_fifo_ctrl_an;
}rflm_dtr_tx_txr_group_struct;



 /* Group PPDSM: */ 
/* This block consist of the TXR registers to program the Tx chain n*/
typedef struct
{
  uint32 txr_ppdsm_ctl_an;
  uint32 txr_ppdsm_override_val_an;
  uint32 txr_ppdsm_override_rep_an;
}rflm_dtr_tx_ppdsm_group_struct;



 /* Group TXR_STREAM: */ 
/* This block consist of the TXR registers to program the Tx chain n*/
typedef struct
{
  uint32 txr_stream_if_ctrl_an;
  uint32 txr_fmt_conv_ctl_an;
}rflm_dtr_tx_txr_stream_group_struct;



 /* Group TXCR_RESET: */ 
/* This block consist of the TXC registers to program both the Tx chains*/
typedef struct
{
  uint32 tx_mode_v0;
  uint32 tx_mode;
}rflm_dtr_tx_txcr_reset_group_struct;



// SW FW Interface Buffer

typedef struct ALIGN(32)
{
  rflm_dtr_tx_xo_vars_group_struct xo_vars_params;
  rflm_dtr_tx_sw_xo_vars_group_struct sw_xo_vars_params;
  rflm_dtr_tx_dig_delay_vars_group_struct dig_delay_vars_params;
  rflm_dtr_tx_analog_delay_vars_group_struct analog_delay_vars_params;
  rflm_dtr_tx_txagc_dm_vars_group_struct txagc_dm_vars_params;
  rflm_dtr_tx_dac_vars_group_struct dac_vars_params;
  rflm_dtr_tx_mtc_trig_group_struct mtc_trig_params;
  rflm_dtr_tx_dac_down_group_struct dac_down_params;
  rflm_dtr_tx_dac_clk_group_struct dac_clk_params;
  rflm_dtr_tx_mtc1_group_struct mtc1_params;
  rflm_dtr_tx_mtc_group_struct mtc0_params;
  rflm_dtr_tx_tx_en_group_struct tx_en_params;
  rflm_dtr_tx_tx_top_gsm_group_struct tx_top_gsm_params;
  rflm_dtr_tx_tx_top_group_struct tx_top_params;
  rflm_dtr_tx_tx_dac_cfg_group_struct tx_dac_cfg_params;
  rflm_dtr_tx_tx_dac_mode_group_struct tx_dac_mode_params;
  rflm_dtr_tx_mempoolw_group_struct mempoolw_params;
  rflm_dtr_tx_mempoolr_group_struct mempoolr_params;
  rflm_dtr_tx_dac_down_group_struct dac_up_params;
  rflm_dtr_tx_iref_group_struct iref_params;
  rflm_dtr_tx_tx_mod_group_struct tx_mod_params;
  rflm_dtr_tx_timestamp_txc_group_struct timestamp_txc_params;
  rflm_dtr_tx_txc_dp_cfg_group_struct txc_dp_cfg_params;
  rflm_dtr_tx_txc_group_struct txc_params;
  rflm_dtr_tx_ramp_group_struct ramp_params;
  rflm_dtr_tx_rot_group_struct rot_params;
  rflm_dtr_tx_iqmc_group_struct iqmc_params;
  rflm_dtr_tx_peq_group_struct peq_params;
  rflm_dtr_tx_dc_group_struct dc_params;
  rflm_dtr_tx_dm_group_struct dm_params;
  rflm_dtr_tx_ep_peq_group_struct ep_peq_params;
  rflm_dtr_tx_ref_group_struct ref_params;
  rflm_dtr_tx_dbg_group_struct dbg_params;
  rflm_dtr_tx_txfe_delaymatch_group_struct txfe_delaymatch_params;
  rflm_dtr_tx_dpiqgain_txc_group_struct dpiqgain_txc_params;
  rflm_dtr_tx_timestamp_txc_group_struct timestamp_params;
  rflm_dtr_tx_txr_iq_dac_group_struct txr_iq_dac_params;
  rflm_dtr_tx_txr_group_struct txr_params;
  rflm_dtr_tx_ppdsm_group_struct ppdsm_params;
  rflm_dtr_tx_txr_stream_group_struct txr_stream_params;
  rflm_dtr_tx_txcr_reset_group_struct txcr_reset_params;
  rflm_dtr_tx_indices_struct rflm_dtr_tx_reg_indices;
  rflm_dtr_tx_block_valid_flags_struct rflm_dtr_tx_block_valid;
  rflm_dtr_tx_header_struct tx_header;
}rflm_dtr_tx_settings_type_ag;




#ifdef __cplusplus
}
#endif



#endif


