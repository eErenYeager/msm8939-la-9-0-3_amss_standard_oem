
#ifndef RFLM_DTR_TX_UMTS_STATIC_SETTINGS_AG_H
#define RFLM_DTR_TX_UMTS_STATIC_SETTINGS_AG_H


#ifdef __cplusplus
extern "C" {
#endif

/*
WARNING: This file is auto-generated.

Generated at:    Mon Oct  6 13:08:44 2014
Generated using: lm_autogen.pl
Generated from:  v5.6.4 of DimePM_TXLM_register_settings.xlsx
*/

/*=============================================================================

           T X    A U T O G E N    F I L E

GENERAL DESCRIPTION
  This file is auto-generated and it captures the modem register settings 
  configured by FW, provided by the rflm_dtr_tx.

Copyright (c) 2009, 2010, 2011, 2012, 2013, 2014 by Qualcomm Technologies, Inc.  All Rights Reserved.

$DateTime: 2015/01/27 06:42:19 $
$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/rfmodem_dimepm/lm/inc/rflm_dtr_tx_umts_static_settings_ag.h#1 $

=============================================================================*/

/*=============================================================================
                           REVISION HISTORY
Version    Author   Date   
         Comments   IR Number   Other POC (Comma Delimited)   Release Validation Notes   Dependencies   AG File Location   Changes for   
5.6.4   nichunw   10/1/2014   
         1. Change REF_DWN_FACTOR to 1 for TDS_sAPT states in DAC rate 230.4MHz to lower down the DPD capture rate by half.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            TDS   
5.6.3   nichunw   9/4/2014   
         1. Updated DP_PEQ_SW_Dynamic tab to contain the complete register setting for TXC_DP_CFG_An.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            LTE   
5.6.2   nichunw   8/6/2014   
         1. Added chain dependency to the avg_error and overflow bit fix. The pass in value is now in the format of [overflow_1(5), avg_error_1(4), overflow_0(3), avg_error_0(2), foundry_ID(1:0)]  2. Added COMBODAC_CAL_CTRL_c::FSM_Cal_MODE.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            DAC   
5.6.1   nichunw   7/31/2014   
         1. Update IREF_LUT for Samsung. New settings are updated by DAC team. (CR 702890) 2. Added the fix for DAC avg_error and overflow bits by using the foundry column. This change will prevent those two bits being overwritten to 0 during txlm configuration. The second value in foundry column is the bit mask and it will AND with the pass in value [overflow(3), avg_error(2), foundry_ID(1:0)]. If the result matches with the first value in the foundry column, that row will be selected.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
5.6.0   nichunw   7/18/2014   
         1. Added a new txlm variable TXC_input_freq_hz.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
5.5.2   nichunw   7/16/2014   
         1. Revert REF_MODE and MODE_256 settings to unblock DPD IQ captures.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
5.5.1   nichunw   7/11/2014   
         1. Updated DP_PEQ_SW_Dynamic tab for filter settings for WTR4905.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
5.5.0   nichunw   6/19/2014   
         1. Added txlm var DAC_freq_khz. 2. Change state specific mask write to normal mask write for  COMBODAC_CLK_SRC_CFG and COMBODAC_DIGVAL_EN_c      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
5.4.6   nichunw   6/18/2014   
         1. Added foundry-dependent dac config on ComboDAC_cfg_SW_Dynamic tab. 2. Adding foundry-dependent IREF LUTs for TSMC, GF and SEC. 3. Changed REF_MODE to 1 and MODE_256 to 1 for all states. This is for ILPC and LOFT/RSB algorithms.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
5.4.5   nichunw   5/15/2014   
         1. Change PP_GAIN for GSM 76.8MHz to 44373.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            GSM   
5.4.4   nichunw   5/8/2014   
         1. Updated DAC config for TSMC and GF. 2. Added SEC dac config.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            DAC   
5.4.3   nichunw   5/5/2014   
         1. Remove COMBODAC_RTUNE_RW_CTRL_c since it is only needed during DAC powerup.       nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            DAC   
5.4.2   nichunw   5/4/2014   
         1. Added mask write for COMBODAC_CLK_SRC_CFG and COMBODAC_DIGVAL_EN_c.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            DAC   
5.4.1   nichunw   5/2/2014   
         1. Updated IREF table for the -18dB entry. 2. Disable EP_PEQ1/PEQ2_ENABLE for LTE, UMTS, 1x, DO. EP is not used by those techs. 3. Change TWO_SAMPLES_CYCLE to 1 for 1x and TDS 230.4MHz.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
5.4.0   nichunw   4/24/2014   
         1. Added TXFE_DELAYMATCH block for SW to dynamically changing the DM registers. This is for CS2.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
5.3.11   nichunw   4/15/2014   
         1. Make TXC subgroup SW_dynamic, this is mainly for EP_DM2_FINE_TAU and EP_DM3_FRAC_TAU.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
5.3.10   nichunw   4/9/2014   
         1. Add Foundry column to have foundry-dependent DAC cfg. The default setting is TSMC DAC cfg.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            DAC   
5.3.9   nichunw   3/19/2014   
         1. Revert back to the 5.3.7. The IREF changes in 5.3.8 are not needed anymore. The iref gain fix is totally on FW, during DAC reactivation.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            DAC   
5.3.8   nichunw   3/18/2014   
         1. Added IREF start/stop time and trigger values. This is to fix the 3dB power drop issue seen on GF parts. The fix applies to all states/all foundries.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            DAC   
5.3.7   nichunw   3/12/2014   
         1. Added IP2cal state for GSM (for WTR4905 only).      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            GSM   
5.3.6   nichunw   3/11/2014   
         1. Change the dac config for all non-LTE states (with WTR1605/2605) to LP mode.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            DAC   
5.3.5   nichunw   3/6/2014   
         1. Fixed the typo on ComboDAC_cfg_SW_Dynamic tab.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            DAC   
5.3.4   nichunw   3/3/2014   
         1. Changed COMBODAC_CFG settings for DAC calibration on DAC_cal_LM_settings tab 2. Changed COMBODAC_CFG settings for all CS2 techs states (1x/DO/UMTS at 230.4MHz and GSM at 76.8MHz) except LTE. 3. Reduced the states in ComboDAC_cfg_SW_Dynamic tab to LTE only. 4. Add two more columns in DAC_cal_LM_settings tab to include the CS2 COMBODAC_CFG settings. 5. Added DAC temp comp info on IREF_Dynamic tab.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
5.3.3   nichunw   2/13/2014   
         1. Removed entries of the state variable 'carrier_cfg' from HDR states. 2. Replaced state variable 'DAC_freq' by device to inidcate the dependency on WTR models.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
5.3.2   nichunw   2/2/2014   
         1. Reorder the register order in DAC_cal_LM_settings tab to follow the recommended sequence from DAC team.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
5.3.1   nichunw   1/15/2013   
         1. Filled in default RT_QFUSE_CODE with 128. 2. Filled in value for row TX_DIGITAL_DELAY_MICROSECS_Q16 for GSM. 3. Fixed index issue in DAC_Cal_control_registers tab. 4. Made TXC_DP_CFG subgroup SW_dynamic.       nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
5.3.0   nichunw   1/13/2014   
         1. Remove the redundant white space in DAC_CLK_EXT_XO_EN in DAC_cal_LM_settings tab. 2. Change modem version to DimePM. 3. Updated DAC_cal_LM_settings and DAC_Cal_control_registers tabs. 4. Added TXCR_RESET section at the bottom. 5. Filled in value for row TX_DIGITAL_DELAY_MICROSECS_Q16 except GSM.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
5.2.1   nichunw   12/20/2013   
         1. Updated the format and look-up table in IREF_Dynamic. 2. Added ComboDAC_cfg_SW_Dynamic tab for the different combodac_cfg settings in different WTRs.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
5.2.0   vikramr/nichunw   12/16/2013   
         1. Simplified the chain indices to only two sets. 2. Add COMBODAC_CLK_SRC_CFG::DAC_CLK_SEL=3 for all techs to complete the DAC clock settings. 3. DAC_CAL_LUT section is removed. 4. Fixed the typo in the name of register TXC_DP_PEQ1_Q_SET2_An. It was TXC_DP_PEQ1_SET2_An in the previous release. 5. Updated the DP_PEQ_SW_dynamic, DAC_cal_LM_settings, DAC_Cal_Control_registers tabs.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
5.1.0   nichunw   11/21/2013   
         1. Added DAC frequency parameters for all txlm states other than LTE. 2. Added COMBODAC_CFG2_c and updated the COMBODAC_CFG values. 3. Added IREF_UPDATE section to include iref-update related registers. 4. Merged MDM_CLK0 and MDM_CLK1 into single section. 5. Added DAC_POWER_DOWN and DAC_REACTIVATION sections. This change is made for common FW to update the DAC clock rate.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
5.0.1   nichunw   11/11/2013   
         Removing COMBODAC_MSBCAL and COMBODAC_DCCAL       nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com               
5.0.0   nichunw   11/8/2013   
         Removing deprecated registers.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com               
1.0.1   nichunw   11/8/2013   
         Remove COMBODAC_CTRL_c, MSS_DSM_PHASE_HIGHZ .      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com               
1.0.0   nichunw   10/30/2013   
         initial version      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com               
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              

=============================================================================*/
/*=============================================================================
                           INCLUDE FILES
=============================================================================*/

#include "rflm_dtr_tx_struct_ag.h" 




extern rflm_dtr_tx_settings_type_ag rflm_dtr_tx_umts_sc_dac_115p2mhz_chain0_settings_ag;

extern rflm_dtr_tx_settings_type_ag rflm_dtr_tx_umts_sc_dac_115p2mhz_chain1_settings_ag;

extern rflm_dtr_tx_settings_type_ag rflm_dtr_tx_umts_dc_dac_115p2mhz_chain0_settings_ag;

extern rflm_dtr_tx_settings_type_ag rflm_dtr_tx_umts_dc_dac_115p2mhz_chain1_settings_ag;

extern rflm_dtr_tx_settings_type_ag rflm_dtr_tx_umts_sc_dac_230p4mhz_chain0_settings_ag;

extern rflm_dtr_tx_settings_type_ag rflm_dtr_tx_umts_sc_dac_230p4mhz_chain1_settings_ag;

extern rflm_dtr_tx_settings_type_ag rflm_dtr_tx_umts_dc_dac_230p4mhz_chain0_settings_ag;

extern rflm_dtr_tx_settings_type_ag rflm_dtr_tx_umts_dc_dac_230p4mhz_chain1_settings_ag;

extern rflm_dtr_tx_settings_type_ag rflm_dtr_tx_umts_sc_dac_115p2mhz_sapt_chain0_settings_ag;

extern rflm_dtr_tx_settings_type_ag rflm_dtr_tx_umts_sc_dac_115p2mhz_sapt_chain1_settings_ag;

extern rflm_dtr_tx_settings_type_ag rflm_dtr_tx_umts_sc_dac_230p4mhz_sapt_chain0_settings_ag;

extern rflm_dtr_tx_settings_type_ag rflm_dtr_tx_umts_sc_dac_230p4mhz_sapt_chain1_settings_ag;

extern rflm_dtr_tx_settings_type_ag rflm_dtr_tx_umts_dc_dac_115p2mhz_sapt_chain0_settings_ag;

extern rflm_dtr_tx_settings_type_ag rflm_dtr_tx_umts_dc_dac_115p2mhz_sapt_chain1_settings_ag;

extern rflm_dtr_tx_settings_type_ag rflm_dtr_tx_umts_dc_dac_230p4mhz_sapt_chain0_settings_ag;

extern rflm_dtr_tx_settings_type_ag rflm_dtr_tx_umts_dc_dac_230p4mhz_sapt_chain1_settings_ag;

#ifdef __cplusplus
}
#endif



#endif


