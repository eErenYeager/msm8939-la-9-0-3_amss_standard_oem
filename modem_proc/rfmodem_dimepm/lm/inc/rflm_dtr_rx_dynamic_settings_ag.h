
#ifndef RFLM_DTR_RX_DYNAMIC_SETTINGS_AG_H
#define RFLM_DTR_RX_DYNAMIC_SETTINGS_AG_H


#ifdef __cplusplus
extern "C" {
#endif

/*
WARNING: This file is auto-generated.

Generated at:    Mon Mar 23 23:21:23 2015
Generated using: lm_autogen.exe
Generated from:  v5.5.2 of DimePM_RxFE_Register_Settings.xlsx
*/

/*=============================================================================

           R X    A U T O G E N    F I L E

GENERAL DESCRIPTION
  This file is auto-generated and it captures the modem register settings 
  configured by FW, provided by the rflm_dtr_rx.

Copyright (c) 2009, 2010, 2011, 2012, 2013, 2014 by Qualcomm Technologies, Inc.  All Rights Reserved.

$DateTime: 2015/03/25 23:24:02 $
$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/rfmodem_dimepm/lm/inc/rflm_dtr_rx_dynamic_settings_ag.h#2 $

=============================================================================*/

/*=============================================================================
                           REVISION HISTORY
Version    Author   Date   
         Comments   IR Number   Other POC (Comma Delimited)   Release Validation Notes   Dependencies   AG File Location   
5.5.2   gatesj   3/23/2015   
         1. BBRx register settings updated for GSM, TDS, WCDMA, 1x/DO. Settings changed to improve sensitivity on affected devices.       wangyan@qti.qualcomm.com   1. Verified by RF test and BBRx team      https://sharepoint.qualcomm.com/qct/Modem-Tech/Projects/DimePM/Systems/RF%20Interfaces/RxFE%20AG%20Archive/5.5.2   
5.5.1   gatesj   10/21/2014   
         1.UMTS DSR state now has FET NB      ckwong@qti.qualcomm.com, zchoi@qti.qualcomm.com         https://sharepoint.qualcomm.com/qct/Modem-Tech/Projects/DimePM/Systems/RF%20Interfaces/RxFE%20AG%20Archive/5.5.1   
5.5.0   gatesj   10/7/2014   
         "1. nbpwr_SW_Dynamic tab now includes NBPWR estimates                  
2. LTE_K_NORM_FACTOR and LTE_K_NBEE_TO_DBV LTE scaling factors included in the RxLM Variables. All scale factors now int32                           
3. BBRx configuraiton registers SW dynamic to allow for BBRx Vcm calibraiton "      pengkaiz@qti.qualcomm.com, pphilip@qti.qualcomm.com   
         "1. Verified by LTE team                  
2. Verified by LTE team"      https://sharepoint.qualcomm.com/qct/Modem-Tech/Projects/DimePM/Systems/RF%20Interfaces/RxFE%20AG%20Archive/5.5.0   
                           
5.4.2   gatesj   9/17/2014   
         "1. LTE20 and LTE15 4905 states modified to increase VSRC output rate to 122.88MHz to avoid DC images falling in band                  
    a) Dec0 disabled                           
    b) Dec2 enabled                           
    c) Scaling factors updated                           
    d) Delays updated                           
    e) ICIFIR updated                           
    f) VSRC regulator bypassed"   IR-027726   pengkaiz@qti.qualcomm.com   
         1. Verified by LTE team   CR726173   https://sharepoint.qualcomm.com/qct/Modem-Tech/Projects/DimePM/Systems/RF%20Interfaces/RxFE%20AG%20Archive/5.4.2            
5.4.1   gatesj   9/8/2014   
         1. ADC insertion loss deltas populated for LTE states   IR-026955   pengkaiz@qti.qualcomm.com   1. Verified by LTE team      https://sharepoint.qualcomm.com/qct/Modem-Tech/Projects/DimePM/Systems/RF%20Interfaces/RxFE%20AG%20Archive/5.4.1   
5.4.0   gatesj   8/20/2014   
         "1. Updated WBDC scale factors for LTE states                  
2. Updated UMTS NBR state to include expected CSR values                           
3. ADC_CFG block now runtime dynamic - requires interface change                           
4. New RxLM Variable NOTCH_SAMPLING_RATE - requires interface change"   IR-023475   pengkaiz@qti.qualcomm.com, tungl@qti.qualcomm.com, aafzali@qti.qualcomm.com, alirezaa@qti.qualcomm.com   
         "1. Verified by LTE team                  
2. Verified by W team"      https://sharepoint.qualcomm.com/qct/Modem-Tech/Projects/DimePM/Systems/RF%20Interfaces/RxFE%20AG%20Archive/5.4.0   
                           
5.3.8   gatesj   8/11/2014   
         "1. CSR_IDEAL_VAL_C0 and CSR_IDEAL_VAL_C1 updated for UMTS2C state to now contain the 2.5MHz and -2.5MHz rotator values.                   
2. BBRx configuration registers updated. Includes an optmizaiton to LTE20M mode (bbr_cm_ctl and bbr_i5_ctl).                           
3. WBDC LSHIFT_UPDT_CMD set for all LTE modes.                            
4. GSM state now uses Dec7 in mode 1. ICIFIR udpated to reflect this. "   IR-021713   wangyan@qti.qualcomm.com, ashwina@qti.qualcomm.com, pengkaiz@qti.qualcomm.com, bcanpola@qti.qualcomm.com   
         "3. Verified by LTE team                  
4. Verified by GSM/RF Sys team"      https://sharepoint.qualcomm.com/qct/Modem-Tech/Projects/DimePM/Systems/RF%20Interfaces/RxFE%20AG%20Archive/5.3.8/   
                           
5.3.7   gatesj   7/24/2014   
         1. Modified the triggering of the FET NB, followup to change 2 in 5.3.5. Now uses imm_trig instead of enable/start_stop   IR-018794   apalla@qti.qualcomm.com         https://sharepoint.qualcomm.com/qct/Modem-Tech/Projects/DimePM/Systems/RF%20Interfaces/RxFE%20AG%20Archive/5.3.7/   
5.3.6   gatesj   7/22/2014   
         1. GSM IP2 cal state changes (rotator location/frequency, ICIFIR)   IR-018249   sbera@qti.qualcomm.com   1. Validated by the G team      https://sharepoint.qualcomm.com/qct/Modem-Tech/Projects/DimePM/Systems/RF%20Interfaces/RxFE%20AG%20Archive/5.3.6/   
5.3.5   gatesj   7/10/2014   
         "1. UMTS 2C 4905 state ICIFIR updated                   
2. UMTS SC state has the second FET NB disabled by default."   IR-016444   carstenp@qti.qualcomm.com, rparuchu@qti.qualcomm.com, msathyan@qti.qualcomm.com   
         "1. Validated by the W team                  
2. Validated by the W team"      https://sharepoint.qualcomm.com/qct/Modem-Tech/Projects/DimePM/Systems/RF%20Interfaces/RxFE%20AG%20Archive/5.3.5/   
                           
5.3.4   gatesj   7/8/2014   
         "1. GSM VSRC SCTR wrap around value doubled.                   
2. TDS TRK IRAT 4905 state's WBDC parameter K2 set to 7 (previously 6)."   IR-015980   bcanpola@qti.qualcomm.com, yclin@qti.qualcomm.com   
         "1. Validated on Bolt by G team                  
2. Validated by the TDS team"      https://sharepoint.qualcomm.com/qct/Modem-Tech/Projects/DimePM/Systems/RF%20Interfaces/RxFE%20AG%20Archive/5.3.4/   
                           
5.3.3   gatesj   6/27/2014   
         "1. Updated BBRx registers to new recommended values.                  
2. Included bbrx_fuse_SW_Dynamic tab for BBRx RSB compensation with 4905. "   IR-014275   wangyan@qti.qualcomm.com, cdeshmuk@qti.qualcomm.com, djoffe@qti.qualcomm.com   
               https://sharepoint.qualcomm.com/qct/Modem-Tech/Projects/DimePM/Systems/RF%20Interfaces/RxFE%20AG%20Archive/5.3.3/            
5.3.2   gatesj   6/11/2014   
         1. Corrected modes for UMTS ACQ states.    IR-011679   aojha@qti.qualcomm.com, kakash@qti.qualcomm.com         https://sharepoint.qualcomm.com/qct/Modem-Tech/Projects/DimePM/Systems/RF%20Interfaces/RxFE%20AG%20Archive/5.3.2/   
5.3.1   gatesj   6/5/2014   
         "1. UMTS_1C state now has 2 NB    to enable FET NB mirroring.                
2. New UMTS_ACQ state - copy of the 1C state for acquisition with only 1 NB."   IR-010911   apalla@qti.qualcomm.com, kakash@qti.qualcomm.com   
               https://sharepoint.qualcomm.com/qct/Modem-Tech/Projects/DimePM/Systems/RF%20Interfaces/RxFE%20AG%20Archive/5.3.1/            
5.3.0   gatesj   5/21/2014   
         "1. WBDC/WBPWR/NBPWR scale factors now have csim tags.                   
2. Included WBDC_OFFSET variable. Requires an interface change.                            
3. All WBPWR/WBDC/NBPWR offset/scale factors are now Q24 format.                           
4. BBRx register settings updated as per BBRx team's recomendation.                            
5. Separate states on 4905 for LTE1.4   3   5 to account for WTR pole differences and ICIFIR changes. No SW changes required.    
                           
6. Updated VSRC_WA to FW Export dynamicity    requies reorganizaiton of the VSRC subgroup and the addition of the new VSRC_WA_COUNT subgroup."   IR-009274   
         wangyan@qti.qualcomm.com, apalla@qti.qualcomm.com         https://sharepoint.qualcomm.com/qct/Modem-Tech/Projects/DimePM/Systems/RF%20Interfaces/RxFE%20AG%20Archive/5.3.0/         
5.2.8   gatesj   5/14/2014   
         1. UMTS SC and UMTS NBR 4905 states now also list B2 in the mode parameter.   IR-008524   ghuang@qti.qualcomm.com         https://sharepoint.qualcomm.com/qct/Modem-Tech/Projects/DimePM/Systems/RF%20Interfaces/RxFE%20AG%20Archive/5.2.8/   
5.2.7   gatesj   5/7/2014   
         "1. 1x/DO 4905 states now have both BC1 and non-BC1 bandwidth modes listed.                   
2. nbpwr_SW_Dynamic tab has had the columsn shifted appropriately. "   IR-007923   zhaow@qti.qualcomm.com   
               https://sharepoint.qualcomm.com/qct/Modem-Tech/Projects/DimePM/Systems/RF%20Interfaces/RxFE%20AG%20Archive/5.2.7/            
5.2.6   gatesj   4/17/2014   
         "1. Addition of the UMTS NBR state.                  
2. Addition of the nbpwr_SW_Dynamic tab containing UMTS NBPWR Cal information.                            
3. Modified the ADC_INSERTION_LOSS_DELTA values. Set to zero until insertion loss deltas are identified    but will use Q24 format in the future if needed. "   IR-005146   
         alirezaa@qti.qualcomm.com, pengkaiz@qti.qualcomm.com         https://sharepoint.qualcomm.com/qct/Modem-Tech/Projects/DimePM/Systems/RF%20Interfaces/RxFE%20AG%20Archive/5.2.6/         
5.2.5   gatesj   3/28/2014   
         "1. New TDS states - 1605 and 2605 states now split to account for WTR pole differences.                   
2. ""WTR1625"" string added to all TRX fields currently listing WTR2605 to allow for full compatibility.                            
3. GSM IP2 Cal state added.                            
4. UMTS2C 4905 state - Corrected ICIFIR trunc bits to calculate correctly    correcting the value from 4 to 3. "   IR-005098   
         djoffe@qti.qualcomm.com, hongzhen@qti.qualcomm.com, madhavis@qti.qualcomm.com, shardhas@qti.qualcomm.com         https://sharepoint.qualcomm.com/qct/Modem-Tech/Projects/DimePM/Systems/RF%20Interfaces/RxFE%20AG%20Archive/5.2.5/         
5.2.4   gatesj   3/18/2014   
         "1. Removed UMTS_Voice common state data as it is now unused.                  
2. Modified LTE LowBW states to use the BW parameter ""1.4; 3; 5"""   IR-005004   djoffe@qti.qualcomm.com, smkrishn@qti.qualcomm.com   
               https://sharepoint.qualcomm.com/qct/Modem-Tech/Projects/DimePM/Systems/RF%20Interfaces/RxFE%20AG%20Archive/5.2.4/            
5.2.3   gatesj   2/28/2014   
         "1. Removed duplicate DVGA_OFFSET_Cx rows -- No affect to RFSW or FW    AG script ignored duplicate versions.               
2. Corrected DVGA_OFFSET_Cx values for the 1x/HDR states.                           
3. Removed the 1x 2605 states    renamed 1x 1605 states to include support for 2605.                        
4. Removed the UMTS_Voice states    UMTS SC should now be used for Voice and Data.                        
5. Corrected the ADC register config settings for xPT 4905 states.                           
6. Restructured the register settings into 3 tabs:                           
         a. primary_state_reg_settings - contains each unique state definition    all pre-vsrc settings    each NB's ICIFIR settings   
          a pointers to a bbrx configuration column    and pointers to common_state_reg_settings column.                
         b. common_state_reg_settings - contains each common state defintion with all post-vsrc settings (excluding ICIFIR).                           
         c. bbrx_config - contains each BBRx config state including fuse-dependent setting placeholders.                           
         d. No interface changes required. With the exception of the ADC register bug fixes    the AG files are equal to version 5.2.1."   IR-004955   
                  https://sharepoint.qualcomm.com/qct/Modem-Tech/Projects/DimePM/Systems/RF%20Interfaces/RxFE%20AG%20Archive/5.2.3/         
5.2.2   gatesj   2/22/2014   
         1. Corrected the UMTS Voice state to have 9 tap ICIFIR.   IR-004940            https://sharepoint.qualcomm.com/qct/Modem-Tech/Projects/DimePM/Systems/RF%20Interfaces/RxFE%20AG%20Archive/5.2.2/   
5.2.1   gatesj   1/27/2014   
         1. Corrected the block valid flags for the debug states.                   
5.2.0   gatesj   1/24/2014   
         "1. ERROR_CORRECTION_ENABLE enabled for the UMTS/LTE states that were missing it. Scale factors updated to reflect this change.                  
2. Addition of DEBUG states for y1y2 capture.                           
3. GSM state WBDC now includes the LSHIFT_UPDATE_IMM_TRIGGER.                           
5. WBPWR dec factor corrected for UMTS states.                           
6. Corrected the UMTS2C states' ICIFIR filters - swapped NB0 and NB1.                           
7. ADC_FREQ row is now generated using the PLL MUX select registers. New PLL plan tab added to provide the PLL info.                           
8. Populated values for RXFE_DELAY_NBn_IN_SCTR (previously named VSRC_MEMPOOL_DELAYNBn_IN_SCTR) which contains a 32U24 value representing the full RxFE delay between the ADC and mempool in VSRC counts for each chain.                           
9. Correcting to version 5.2.0 due to the interface update from number 8 above. "                           
5.1.2   gatesj   1/8/2014   
         1. Modified the bandwidth parameter for the LTE_LowBW modes. Now set to                   
2. Modified the mode names for the LTE    UMTS    and 1x/DO cases with band specific states."   
                           
5.1.1   gatesj   1/3/2014   
         1. LTE Scale factors added                  
5.1.0   gatesj   12/16/2013   
         "1. WTR1605 and WTR4905 spreadsheets merged.                   
2. TRX state config parameter added to identify which RF pairings are allowed for each state.                            
3. Some states have been renamed to indicate additional RF pairing support.                            
4. COMMON_NOTCH_01 and COMMON_NOTCH_23 programming groups reorganized to follow Dime's subgroup naming.                            
5. Common and dedicated notches are now listed as SW DYNAMIC.                           
6. UMTS_1C_Data_nonET and UMTS_2C_Data_nonET modes both renamed to UMTS_Data_nonET and carrier numbers are indicated using the num_carr param.                           
7. Group and Sequence columns removed.                           
8. Included WBDC    WBPWR    NBPWR scale factor rows."   
                           
5.0.7   gatesj   12/4/2013   
         "1. Removed NB3 programming.                   
2. RXFE_NBn_WBMUX_CFG WB_SEL Subgroup name changed for NB1 and NB2 to match NB0's naming convention.                            
3. WB_NOTCH_FILT_THRESHOLD[2] changed to [3] to account for new dedicated notch.                            
4. WB IQMC changed to allow for runtime dynamicity.                            
5. Changed the MASTER_BLOCK name for the CSR frequency sections and the IQMC section.                            
6. Added dynamic SW dynamic to the CSR frequency fields."                           
5.0.6   gatesj   12/2/2013   
         Filled in the BBRx Registers RXFE_ADC_TEST1_REG, RXFE_ADC_TEST2_REG, RXFE_ADC_TEST3_REG , RXFE_ADC_CONFIG1_REG, and RXFE_ADC_CONFIG2_REG with appropriate Atlas_SE values for all techs.                  
5.0.5   gatesj   11/11/2013   
         Modified xPT mode for TDS. Corrected Dec1 filter mode. Modified the revision history page to follow Excelerator requirements. Modified version number to 5.0.x.                  
5.0.4   gatesj   11/7/2013   
         Added a blank line in row 9 to allow for autogeneration of rxlm files.                  
5.0.3   gatesj   11/6/2013   
         Change to xPT modes LTE10 and UMTS1C - disabled pack4.                  
5.0.2   gatesj   11/5/2013   
         Correction to LTE10 B7 state, removal of pack4 enabled. Adjustment of LTE15 state, addition of pack4 enabled.                  
5.0.1   gatesj   10/31/2013   
         Modifications to RxLM structure to allow the use of the LM autogen tool.                  
5.0.0   gatesj   10/30/2013   
         Initial release                  

=============================================================================*/
/*=============================================================================
                           INCLUDE FILES
=============================================================================*/

#include "rflm_dtr_rx_struct_ag.h" 
#include "rflm_dtr_rx_typedef_ag.h" 




extern rflm_dtr_rx_nb_pwr_nb_group_struct rx_nbpwr_nb_pwr_nb[RFLM_DTR_RX_NBPWR_SW_DYNAMIC_SETTINGS_NUM];

extern void update_rx_nbpwr_nb_pwr_nb (
 			rflm_dtr_rx_nb_pwr_nb_group_struct *target_struct ,
 			uint32 table_index 
			);
#ifdef __cplusplus
}
#endif



#endif


