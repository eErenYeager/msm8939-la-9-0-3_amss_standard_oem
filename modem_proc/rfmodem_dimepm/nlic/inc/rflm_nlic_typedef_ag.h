
#ifndef RFLM_NLIC_TYPEDEF_AG_H
#define RFLM_NLIC_TYPEDEF_AG_H


#ifdef __cplusplus
extern "C" {
#endif

/*
WARNING: This file is auto-generated.

Generated at:    Tue Apr  8 12:17:41 2014
Generated using: nlic_autogen.pl
Generated from:  v0.0.7 of Bolt_NLICLM_settings.xlsx
*/

/*=============================================================================

              A U T O G E N    F I L E

GENERAL DESCRIPTION
  This file is auto-generated and it captures the modem register settings 
  configured by FW, provided by the rflm_nlic.

Copyright (c) 2009, 2010, 2011, 2012, 2013 by Qualcomm Technologies, Inc.  All Rights Reserved.

$DateTime: 2015/01/27 06:42:19 $
$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/rfmodem_dimepm/nlic/inc/rflm_nlic_typedef_ag.h#1 $

=============================================================================*/

/*=============================================================================
                           REVISION HISTORY
Version   Author   Date   
         Comments   
0.0.7   shwang   4/1/2014   
         empty rows are filled for b3_b19 and later. nlic_mode added for wb+nb mode change   
0.0.6   shwang   3/31/2014   
         freature added: 1-reducing num_kernels. 2-selecting one antenna   
0.0.5   shwang   3/20/2014   
         FTL corrections. New equations to support PCC cancellation.   
0.0.4_B3B5   shwang   2/11/2014   
         B3-B3 FTL correction, NLIC enable always on for B3-B3, victim_carrier_id = 1, aggr_carrier_id = 0, f_offset_nom = 0   
0.0.4   shwang   12/9/2013   
         algorithm bug fix: kernel_term_extra_rxy_dly_acq = 2 for B8_B3 (was 1)   
0.0.3   shwang   11/25/2013   
         fix the algorithm: kernel_terms_cancel_mode_l0 = (1-k7_en)*kl0_on_off   
0.0.2   shwang   10/1/2013   
         added k7_en as static param   
0.0.1   shwang   9/27/2013   
         Initial Revision   

=============================================================================*/
/*=============================================================================
                           INCLUDE FILES
=============================================================================*/

#include "comdef.h" 



typedef enum
{
	USE_EQ,
	SET_TRUE,
	SET_FALSE
}rflm_compute_kl0_type;

typedef enum
{
	CONST_VAL,
	USE_EQ_A_B
}rflm_compute_ab_type;

typedef enum
{
	EQ1,
	EQ2,
	EQ3_EQ4
}rflm_compute_ftl_type ;

typedef enum
{
	SET_SCC,
	USE_EQ_PCC_SCC
}rflm_compute_victim_type  ;

typedef struct 
{
	uint8 kernel_term_sel_extra_rxy_dly_acq;
	uint8 kernel_terms_cancel_mode_l0_reduce;
	uint8 kernel_terms_cancel_mode_nl0;
	uint8 kernel_terms_cancel_mode_nl1_normal;
	uint8 kernel_terms_cancel_mode_nl1_reduce;
	boolean conj_linear;
	boolean rsb_enable;
	rflm_compute_kl0_type compute_mode_kl0;
	uint16 nlic_on_off_sinr_coeff_a;
	uint16 trk_on_off_th_1;
	uint16 trk_on_off_th_2;
	uint16 trk_on_off_coeff_a;
	uint8 trk_on_off_rb_1;
	uint8 trk_on_off_rb_2;
	boolean k7_en;
	uint8 aggr_carrier_idx;
	uint8 rx_enable_normal;
	uint8 rx_enable_reduce;
	rflm_compute_ab_type mode_select_a_b;
	rflm_compute_ftl_type compute_mode_f_offset_ftl;
	rflm_compute_victim_type mode_compute_victim_carrier_idx;
	int8 a_pcc0;
	int8 a_pcc1;
	int8 a_pcc2;
	int8 b_pcc0;
	int8 b_pcc1;
	int8 b_pcc2;
	int8 a_scc0;
	int8 a_scc1;
	int8 a_scc2;
	int8 b_scc0;
	int8 b_scc1;
	int8 b_scc2;
	int16 m;
	int16 q;
	uint8 nlic_mode;
	boolean nlic_enable_slot0;
	boolean nlic_enable_slot1;
	boolean nlic_enable_srs;
	uint8 rx_enable;
	boolean delay_tracking_enable;
	uint8 kernel_terms_cancel_mode_l0;
	uint8 kernel_terms_cancel_mode_nl1;
	uint8 kernel_terms_cancel_mode_nl2;
	int32 freq_offset;
	int16 tx_intf_power_db;
	uint8 ul_num_rb;
	uint32 ul_p_freq;
	uint32 dl_p_freq;
	uint32 dl_s_freq;
	boolean nlic_on_off_chan;
	boolean nlic_on_off_snir;
	boolean kl0_on_off;
	boolean trk_I1;
	boolean trk_I2;
	boolean trk_I3;
	int8 a0;
	int8 a1;
	int8 a2;
	int8 b0;
	int8 b1;
	int8 b2;
	int32 f_offset_nom;
	uint32 f_edge;
	uint32 f_edge_kl0;
	int32 f_offset_ftl;
	uint8 victim_carrier_idx;
	uint32 DL_SCC_earfcn;
	uint8 dl_bw;
	uint32 UL_PCC_earfcn;
	uint8 ul_bw;
	uint32 DL_PCC_earfcn;
}rflm_nlic_data_type;


typedef enum 
{
	NLIC_STATE_B17_B4_UL5_DL5,
	NLIC_STATE_B17_B4_UL5_DL10,
	NLIC_STATE_B17_B4_UL10_DL5,
	NLIC_STATE_B17_B4_UL10_DL10,
	NLIC_STATE_B12_B4_UL5_DL5,
	NLIC_STATE_B12_B4_UL5_DL10,
	NLIC_STATE_B12_B4_UL10_DL5,
	NLIC_STATE_B12_B4_UL10_DL10,
	NLIC_STATE_B8_B3_UL5_DL10,
	NLIC_STATE_B8_B3_UL5_DL15,
	NLIC_STATE_B8_B3_UL5_DL20,
	NLIC_STATE_B8_B3_UL10_DL10,
	NLIC_STATE_B8_B3_UL10_DL15,
	NLIC_STATE_B8_B3_UL10_DL20,
	NLIC_STATE_B3_B5_UL10_DL5,
	NLIC_STATE_B3_B5_UL10_DL10,
	NLIC_STATE_B3_B5_UL15_DL5,
	NLIC_STATE_B3_B5_UL15_DL10,
	NLIC_STATE_B3_B5_UL20_DL5,
	NLIC_STATE_B3_B5_UL20_DL10,
	NLIC_STATE_B3_B19_UL10_DL5,
	NLIC_STATE_B3_B19_UL10_DL10,
	NLIC_STATE_B3_B19_UL20_DL5,
	NLIC_STATE_B3_B19_UL20_DL10,
	NLIC_STATE_B3_B26_UL5_DL5,
	NLIC_STATE_B3_B26_UL5_DL10,
	NLIC_STATE_B3_B26_UL10_DL5,
	NLIC_STATE_B3_B26_UL10_DL10,
	NLIC_STATE_B3_B26_UL15_DL5,
	NLIC_STATE_B3_B26_UL15_DL10,
	NLIC_STATE_B3_B26_UL20_DL5,
	NLIC_STATE_B3_B26_UL20_DL10,
	NLIC_STATE_B4_B5_UL5_DL5,
	NLIC_STATE_B4_B5_UL5_DL10,
	NLIC_STATE_B4_B5_UL10_DL5,
	NLIC_STATE_B4_B5_UL10_DL10,
	NLIC_STATE_B4_B5_UL15_DL5,
	NLIC_STATE_B4_B5_UL15_DL10,
	NLIC_STATE_B4_B5_UL20_DL5,
	NLIC_STATE_B4_B5_UL20_DL10,
	NLIC_STATE_NUM,
	NLIC_STATE_INVALID
}nlic_state_enum_type;

#ifdef __cplusplus
}
#endif



#endif


