#ifndef RFLM_NLIC_CORE_H
#define RFLM_NLIC_CORE_H

/*!
  @file
  rflm_nlic_core.h

  @brief
  Export the API necessary for calling modules to get the NLIC dynamic settings
*/

/*==============================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/rfmodem_dimepm/nlic/inc/rflm_nlic_core.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------- 
05/01/14   bsh     NLIC status to ML1
03/14/14   bsh     NlIC changes: code clean up 
03/12/14   bsh     Initial check in

==============================================================================*/

#include "rflm_nlic_settings_ag.h"
#include "rfcom.h"
#include "rflte_mc.h"

#ifdef __cplusplus
extern "C" {
#endif

#define FEATURE_RFSW_LTE_NLIC_ENABLED 1

/*----------------------------------------------------------------------------*/
boolean rflm_nlic_get_dynamic_settings( uint32 tx_aggr_freq, 
                                        uint32 rx_vict_freq, 
                                        uint32 rx_aggr_greq,
                                        uint32 ul_bw,
                                        uint32 dl_bw, 
                                        rflm_nlic_data_type *nlic_data, 
                                        int32 rssi,
                                        nlic_input_params_per_sf *nlic_input_info );


void rflm_nlic_update_static_settings(rflte_mc_carrier_info_type carrier_info,
                                      rfcom_lte_band_type rx_band,
                                      rfcom_lte_bw_type rx_bw);

void rflm_nlic_update_dynamic_settings(nlic_input_params_per_sf *nlic_input_info, nlic_return_params_per_sf *nlic_return_info);

uint8 rflm_nlic_conv_bw_to_nlic_bw(rfcom_lte_bw_type input_bw);

boolean rflm_nlic_core_get_nlic_status(nlic_status_input_params *nlic_input_params, nlic_status_return_params *nlic_return_params);

#ifdef __cplusplus
}
#endif

#endif /*RFLM_NLIC_CORE_H*/

