
#ifndef RFLM_NLIC_AG_H
#define RFLM_NLIC_AG_H


#ifdef __cplusplus
extern "C" {
#endif

/*
WARNING: This file is auto-generated.

Generated at:    Tue Apr  8 12:17:41 2014
Generated using: nlic_autogen.pl
Generated from:  v0.0.7 of Bolt_NLICLM_settings.xlsx
*/

/*=============================================================================

              A U T O G E N    F I L E

GENERAL DESCRIPTION
  This file is auto-generated and it captures the modem register settings 
  configured by FW, provided by the rflm_nlic.

Copyright (c) 2009, 2010, 2011, 2012, 2013 by Qualcomm Technologies, Inc.  All Rights Reserved.

$DateTime: 2015/01/27 06:42:19 $
$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/rfmodem_dimepm/nlic/inc/rflm_nlic_ag.h#1 $

=============================================================================*/

/*=============================================================================
                           REVISION HISTORY
Version   Author   Date   
         Comments   
0.0.7   shwang   4/1/2014   
         empty rows are filled for b3_b19 and later. nlic_mode added for wb+nb mode change   
0.0.6   shwang   3/31/2014   
         freature added: 1-reducing num_kernels. 2-selecting one antenna   
0.0.5   shwang   3/20/2014   
         FTL corrections. New equations to support PCC cancellation.   
0.0.4_B3B5   shwang   2/11/2014   
         B3-B3 FTL correction, NLIC enable always on for B3-B3, victim_carrier_id = 1, aggr_carrier_id = 0, f_offset_nom = 0   
0.0.4   shwang   12/9/2013   
         algorithm bug fix: kernel_term_extra_rxy_dly_acq = 2 for B8_B3 (was 1)   
0.0.3   shwang   11/25/2013   
         fix the algorithm: kernel_terms_cancel_mode_l0 = (1-k7_en)*kl0_on_off   
0.0.2   shwang   10/1/2013   
         added k7_en as static param   
0.0.1   shwang   9/27/2013   
         Initial Revision   

=============================================================================*/
/*=============================================================================
                           INCLUDE FILES
=============================================================================*/

#include "comdef.h" 
#include "rflm_nlic_typedef_ag.h" 



boolean rflm_nlic_get_static_settings (uint8 tx_band, uint8 rx_band, uint16 tx_chan, uint8 ul_bw, uint8 dl_bw, rflm_nlic_data_type * nlic_data);

#ifdef __cplusplus
}
#endif



#endif


