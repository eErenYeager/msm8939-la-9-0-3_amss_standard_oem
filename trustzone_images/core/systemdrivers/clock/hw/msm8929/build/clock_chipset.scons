#=============================================================================
# TZ CLOCK DRIVER MSM8929 LIBRARY
#
# Target:    MSM8929
# Processor: APPS-TZ
#
#=============================================================================
# $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/systemdrivers/clock/hw/msm8929/build/clock_chipset.scons#1 $
#=============================================================================
#  Copyright 2012 Qualcomm Technologies Incorporated.
#  All Rights Reserved.
#  QUALCOMM Proprietary/GTDR
#=============================================================================

Import('env')
env = env.Clone()

#-----------------------------------------------------------------------------
# Define paths
#-----------------------------------------------------------------------------

SRCPATH = "../"
env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0) 

#-----------------------------------------------------------------------------
# Internal depends within CoreBSP
#-----------------------------------------------------------------------------

CBSP_API = [
   'DAL',
   'HAL',
   'SERVICES',
   'SYSTEMDRIVERS',
   'MINK'
]

env.RequirePublicApi(CBSP_API)
env.RequireRestrictedApi(CBSP_API)

env.PublishPrivateApi('SYSTEMDRIVERS_CLOCK', [
   "${BUILD_ROOT}/core/systemdrivers/clock/hw/${CHIPSET}/inc",
   "${BUILD_ROOT}/core/systemdrivers/busywait/inc"
])


#-----------------------------------------------------------------------------
# Image: TZ
#-----------------------------------------------------------------------------

env.AddBinaryLibrary(
  'TZOS_IMAGE',
  '${BUILDPATH}/Clock_msm8929_TZ',
  [ '${BUILDPATH}/src/ClockPIL.c' ])

ALL_SRC = env.FindFiles(['*.c'], "${BUILD_ROOT}/core/systemdrivers/clock/hw/")
TARGET_SRC = [ '${BUILDPATH}/src/ClockPIL.c' ]
CLEAN_SOURCES = list(set(ALL_SRC) - set(TARGET_SRC))
env.CleanPack(['TZOS_IMAGE'], CLEAN_SOURCES)

# ============================================================================
# HWIO
# ============================================================================

if env.has_key('HWIO_IMAGE'):
  env.AddHWIOFile('HWIO', [
    {
      'filename': '${INC_ROOT}/core/systemdrivers/clock/hw/msm8929/inc/ClockHWIO.h',
      'modules': ['GCC_CLK_CTL_REG',
                  'TCSR_TCSR_MUTEX', 'WCSS_A_PMU'],
      'filter-exclude': ['TSENS', 'ACR', 'RPU', 'RESERVED'],
      'output-offsets': True,
      'header':
        '/*\n'
        ' * HWIO base definitions\n'
        ' */\n'
    }
  ])

