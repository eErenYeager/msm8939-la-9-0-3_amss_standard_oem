#ifndef __CLOCKDRIVER_H__
#define __CLOCKDRIVER_H__
/*
===========================================================================
*/
/**
  @file ClockDriver.h

*/
/*
  ====================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ====================================================================
  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/systemdrivers/clock/src/ClockDriver.h#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $
  ====================================================================
*/

/*=========================================================================
      Include Files
==========================================================================*/

#include "ClockTZ.h"
#include "ClockBSP.h"


/*=========================================================================
      Type Definitions
==========================================================================*/

/*
 * Main clock driver data structure.
 */
typedef struct ClockDrvCtxt
{
  DALSYS_PROPERTY_HANDLE_DECLARE(hProp);
  ClockTZBSP *pBSP;
  boolean     bInitialized;
  boolean     bRPMAvailable;
} ClockDrvCtxtType;


/*=========================================================================
      Function Definitions
==========================================================================*/

/* =========================================================================
**  Function : Clock_ClearSSRestart
** =========================================================================*/
/**
  Clear subsystem restart bits.

  This function clears the subsystem restart bits. They are generllay cleared
  by default, but some may be left asserted after a warm reset. TZ needs to
  clear them so the subsystem XPUs can be programmed.
*/
void Clock_ClearSSRestart (void);


#endif /* __CLOCKDRIVER_H__ */

