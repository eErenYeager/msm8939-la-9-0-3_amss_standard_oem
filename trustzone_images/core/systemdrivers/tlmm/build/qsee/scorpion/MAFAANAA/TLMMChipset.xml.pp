# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/systemdrivers/tlmm/config/msm8994/TLMMChipset.xml"
# 1 "<built-in>" 1
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/systemdrivers/tlmm/config/msm8994/TLMMChipset.xml" 2
<driver name="NULL">

# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/systemdrivers/tlmm/config/TlmmPropDef.h" 1
# 53 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/systemdrivers/tlmm/config/TlmmPropDef.h"
typedef struct {
   uint32 nGpioNumber;
   uint32 nFunctionSelect;
}TLMMGpioIdType;
# 3 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/systemdrivers/tlmm/config/msm8994/TLMMChipset.xml" 2
 <device id="/tlmm/configs">
    <props name="blsp_spi_mosi[12]" type="TLMMGpioIdType">{85, 1}</props>
    <props name="blsp_spi_miso[12]" type="TLMMGpioIdType">{86, 1}</props>
    <props name="blsp_spi_cs_n[12]" type="TLMMGpioIdType">{87, 1}</props>
    <props name="blsp_spi_clk[12]" type="TLMMGpioIdType">{88, 1}</props>
    <props name="fp_int_n" type="TLMMGpioIdType">{90, 1}</props>

    <props name="tlmm_base" type=DALPROP_ATTR_TYPE_UINT32>
      0xFD500000
    </props>
    <props name="tlmm_offset" type=DALPROP_ATTR_TYPE_UINT32>
      0x00010000
    </props>
    <props name="tlmm_total_gpio" type=DALPROP_ATTR_TYPE_UINT32>
      146
    </props>
  </device>
</driver>
