<driver name="NULL">
typedef struct {
   uint32 nGpioNumber;
   uint32 nFunctionSelect;
}TLMMGpioIdType;
 <device id="/tlmm/configs">
    <props name="blsp_spi_mosi[12]" type="TLMMGpioIdType">{85, 1}</props>
    <props name="blsp_spi_miso[12]" type="TLMMGpioIdType">{86, 1}</props>
    <props name="blsp_spi_cs_n[12]" type="TLMMGpioIdType">{87, 1}</props>
    <props name="blsp_spi_clk[12]" type="TLMMGpioIdType">{88, 1}</props>
    <props name="fp_int_n" type="TLMMGpioIdType">{90, 1}</props>
    <props name="tlmm_base" type=DALPROP_ATTR_TYPE_UINT32>
      0xFD500000
    </props>
    <props name="tlmm_offset" type=DALPROP_ATTR_TYPE_UINT32>
      0x00010000
    </props>
    <props name="tlmm_total_gpio" type=DALPROP_ATTR_TYPE_UINT32>
      146
    </props>
  </device>
</driver>
