/*===========================================================================

             M A I N   T L M M   D R I V E R   F I L E

DESCRIPTION

  Contains the majority of functionality for the TLMM driver.  The
  API in this module MUST always remain target independent.


===========================================================================
             Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/systemdrivers/tlmm/src/Tlmm.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
07/16/14   dcf     Initial version. 
===========================================================================*/

/*==========================================================================

                     INCLUDE FILES FOR MODULE

==========================================================================*/

#include <DALSys.h>
#include <DALSysTypes.h>
#include <DALStdDef.h>
#include <HALtlmm.h>
#include "Tlmm.h"
#include "TlmmPropDef.h"

/*==========================================================================

                     LOCAL TYPE DEFINES FOR THIS MODULE

==========================================================================*/

typedef struct
{
  uint32            nGpioKey;
  TLMMGpioIdKeyType nGpioId;
}TLMMGpioSecAccessType;

uint16 gnGpioAdder = 0;

#define TLMM_GPIO_SET_KEY(nGpioNumber, nGpioId) \
           (nGpioNumber | (((0x0000FFFF & nGpioId) + ++gnGpioAdder) << 16))

#define TLMM_GPIO_GET_KEY(nGpioId) ((nGpioId & 0xFFFF0000) >> 16)

#define TLMM_GPIO_GET_PIN(nGpioId) (nGpioId & 0x0000FFFF)


/*==========================================================================

                     LOCAL DATA FOR THIS MODULE

==========================================================================*/


typedef struct
{
  DALDEVICEID                     DevId;
  DALSYS_PROPERTY_HANDLE_DECLARE(hProp);
  uint32                          Reserved[16];
  DALSYS_SYNC_OBJECT(hTlmmSyncObj);
  DALSYSSyncHandle                hTlmmSync;
}TLMMDevCtxt;

typedef struct
{
  boolean        bIsInitialized;
  TLMMDevCtxt    DevCtxt;
  char           *pszPlatform;
}TLMMClientCtxt;

TLMMClientCtxt TlmmLocalCtxt;

static uint32 gnTotalGPIO;

typedef uint32 TLMMGpioSignalType;


/*
 * ganGpioIdUsers array keeps track of which GPIOs are currently in use
 * ensures they are not overwritten during this time. When a GPIO is available 
 * its entry in this array will be marked by a '0'; otherwise, it will 
 * contain the GPIO ID value of the current users. 
 */
static TLMMGpioSecAccessType *ganGpioIdUsers;


/*==========================================================================

                     APPLICATION PROGRAMMING INTERFACE

==========================================================================*/

/*==========================================================================

  FUNCTION      Tlmm_DeviceInit

  DESCRIPTION   See DDITlmm.h

==========================================================================*/ 

DALResult Tlmm_DeviceInit(TLMMClientCtxt *pCtxt)
{
  DALSYSPropertyVar tPropVar;
  HAL_tlmm_InitType tHALInit;
  DALResult eResult = DAL_ERROR;

  /*
   * Check for shared memory.  If already initialized, get a pointer and 
   * we're done.
   */
  if( !pCtxt->bIsInitialized )
  {
    memset((void *)&tHALInit, 0x0, sizeof(HAL_tlmm_InitType));

    /* 
     * Attempt to retrieve a handle to the TLMM properties file.
     */
    eResult = DALSYS_GetDALPropertyHandleStr("/tlmm/configs", pCtxt->DevCtxt.hProp);

    if( DAL_SUCCESS != eResult )
    {
      return(DAL_ERROR);
    }

    /* 
     * Attempt to retrieve the platform configs name for this chip.
     */
//    DALSYS_GetDALPropertyHandleStr("tlmm/platformIO",  pCtxt->DevCtxt.hPlatIO);

    /* 
     * Attempt to retrieve the total supported GPIO for the current target.
     */
    eResult = DALSYS_GetPropertyValue( pCtxt->DevCtxt.hProp, "tlmm_total_gpio", 0, &tPropVar );

    if( DAL_SUCCESS != eResult )
    {
      return(DAL_ERROR);
    }

    tHALInit.nNumGpio = gnTotalGPIO = tPropVar.Val.dwVal;

    /* 
     * Attempt to retrieve the base address for TLMM.
     */
    eResult = DALSYS_GetPropertyValue( pCtxt->DevCtxt.hProp, "tlmm_base", 0, &tPropVar );

    if( DAL_SUCCESS != eResult )
    {
      return(DAL_ERROR);
    }

    tHALInit.nBaseAddress = (uint32)tPropVar.Val.dwVal;


    /* 
     * Get the TLMM Base offset for GPIO.
     */
    eResult = DALSYS_GetPropertyValue( pCtxt->DevCtxt.hProp, "tlmm_offset", 0, &tPropVar );

    if( DAL_SUCCESS != eResult )
    {
      return(DAL_ERROR);
    }

    tHALInit.nBaseAddress += (uint32)tPropVar.Val.dwVal;


    /* 
     * Initialize the HAL interface.
     */
    HAL_tlmm_Init(&tHALInit);
  }

  return(DAL_SUCCESS);

} /* Tlmm_DeviceInit */


/*==========================================================================

  FUNCTION      Tlmm_Init

  DESCRIPTION   See DDITlmm.h

==========================================================================*/ 

boolean Tlmm_Init(void)
{
  if(DAL_SUCCESS == Tlmm_DeviceInit(&TlmmLocalCtxt))
  {
    return(TRUE);
  }
  return(FALSE);

}


/*==========================================================================

  FUNCTION      Tlmm_ConfigGpioGroupInternal

  DESCRIPTION   Internal function to configure a group of GPIO.  This API 
                handles GPIO IDs to ensure incoming configurations do not 
                overwrite a current client's GPIO settings.

==========================================================================*/ 

static DALResult Tlmm_ConfigGpioGroupInternal
(
  TLMMClientCtxt*      pCtxt,
  TLMMGpioEnableType   eEnable,
  TLMMGpioSignalType*  eGpioGroup,
  uint32               nSize,
  TLMMGpioIdKeyType       nGpioId
)
{
  DALResult eResult = DAL_ERROR;
  uint32 nIdx;
  TLMMGpioSignalType nCfg;
  uint32 nGpioNumber;

  if( eGpioGroup == NULL )
  {
    return(DAL_ERROR);
  }

  /*
   * Validate input.  Ensure that there is no one stepping on a GPIO in use. 
   * nGpioId users get priority over the GPIOs.  If more than one nGpioId exists 
   * for the same GPIO, access is granted to the first to configure. 
   */
  for(nIdx = 0; nIdx < nSize; nIdx++)
  {
    nGpioNumber = HAL_GPIO_NUMBER(eGpioGroup[nIdx]);
    if( (nGpioNumber >= gnTotalGPIO) ||
        (nGpioId != ganGpioIdUsers[nGpioNumber].nGpioKey) )
    {
      return(DAL_ERROR);
    }
  }


  switch(eEnable)
  {
    case TLMM_GPIO_ENABLE:

      for( nIdx = 0; nIdx < nSize; nIdx++ )
      {
        /* 
         * Program the hardware.
         */
        nGpioNumber = HAL_GPIO_NUMBER(eGpioGroup[nIdx]);
        HAL_tlmm_ConfigGpio(eGpioGroup[nIdx]);  
      }
      eResult = DAL_SUCCESS;
      break;

    case TLMM_GPIO_DISABLE:

      for( nIdx = 0; nIdx < nSize; nIdx++ )
      {
        /*
         * Convert the configuration to macro format.
         */
        nGpioNumber = HAL_GPIO_NUMBER(eGpioGroup[nIdx]);


        /* 
         * Program the hardware.
         */

        HAL_tlmm_ConfigGpio(nCfg);
      }
      eResult = DAL_SUCCESS;
      break;

    default:
      eResult = DAL_ERROR;
      break;

  }

  return(eResult);

} /* Tlmm_ConfigGpioGroup */


/*==========================================================================

  FUNCTION      Tlmm_ConfigGpioGroup

  DESCRIPTION   See DDITlmm.h

==========================================================================*/ 

DALResult Tlmm_ConfigGpioGroup
(
  TLMMClientCtxt*     pCtxt,
  TLMMGpioEnableType   eEnable,
  TLMMGpioSignalType*  eGpioGroup,
  uint32              nSize
)
{
  return(Tlmm_ConfigGpioGroupInternal(pCtxt, eEnable, eGpioGroup, nSize, NULL));

} /* Tlmm_ConfigGpioGroup */


/*==========================================================================

  FUNCTION      Tlmm_ConfigGpioInternal

  DESCRIPTION   Internal function to configure a GPIO.  This API handles
                GPIO IDs to ensure incoming configurations do not overwrite
                a current client's setting.

==========================================================================*/ 

static DALResult Tlmm_ConfigGpioInternal
(
  TLMMClientCtxt*     pCtxt,   
  TLMMGpioSignalType   eGpioConfig,
  TLMMGpioEnableType   eEnable,
  TLMMGpioIdKeyType       nGpioId
)
{
  return(Tlmm_ConfigGpioGroupInternal(pCtxt, eEnable, &eGpioConfig, 1, nGpioId));

} /* Tlmm_ConfigGpioInternal */


/*==========================================================================

  FUNCTION      Tlmm_ConfigGpio

  DESCRIPTION   See DDITlmm.h

==========================================================================*/ 

DALResult Tlmm_ConfigGpio
(
  TLMMClientCtxt*     pCtxt,   
  TLMMGpioSignalType   eGpioConfig,
  TLMMGpioEnableType   eEnable
)
{
  return(Tlmm_ConfigGpioInternal(pCtxt, eGpioConfig, eEnable, NULL));

} /* Tlmm_ConfigGpio */


/*==========================================================================

  FUNCTION      Tlmm_GpioIn

  DESCRIPTION   See DDITlmm.h

==========================================================================*/ 

DALResult Tlmm_GpioIn
( 
  TLMMClientCtxt*    pCtxt,   
  TLMMGpioSignalType  eGpioConfig,  
  TLMMGpioValueType*  eValue
) 
{
  DALResult eResult = DAL_ERROR;
  boolean bHalRetVal = FALSE;
  uint32 nGpioNumber = HAL_GPIO_NUMBER(eGpioConfig);

  if( nGpioNumber < gnTotalGPIO )
  {
    bHalRetVal = HAL_tlmm_ReadGpio(eGpioConfig);
    eResult = DAL_SUCCESS;
  }
  
  if( !bHalRetVal  )
  {  
    *eValue = TLMM_GPIO_LOW_VALUE;
  }
  else
  {  
    *eValue = TLMM_GPIO_HIGH_VALUE;
  }

  return(eResult);

} /* Tlmm_GpioIn */


/*==========================================================================

  FUNCTION      Tlmm_GpioOutInternal

  DESCRIPTION   Internal API to output a value HIGH/LOW to a GPIO.  This
                API handles GPIO ID contention to ensure that the owner
                of a GPIO ID does not have it's GPIO(s) values changed
                by another user.

==========================================================================*/ 

static DALResult Tlmm_GpioOutGroupInternal
( 
  TLMMClientCtxt*      pCtxt,   
  TLMMGpioSignalType*  aeGpioConfigGroup,  
  uint32               nSize,
  TLMMGpioValueType    eValue,
  TLMMGpioIdKeyType    nGpioId
)
{
  DALResult eResult = DAL_ERROR;
  uint32 nIdx, nGpioNumber;

  if( aeGpioConfigGroup )
  {
    /*
     * Validate input.  Ensure that there is no one stepping on a GPIO in use. 
     * nGpioId users get priority over the GPIOs.  If more than one nGpioId exists 
     * for the same GPIO, access is granted to the first to configure. 
     */
    for(nIdx = 0; nIdx < nSize; nIdx++)
    {
      nGpioNumber = HAL_GPIO_NUMBER(aeGpioConfigGroup[nIdx]);
      if( (nGpioNumber >= gnTotalGPIO) || 
          (nGpioId != ganGpioIdUsers[nGpioNumber].nGpioKey) )
      {
        return(DAL_ERROR);
      }
    }

    HAL_tlmm_WriteGpioGroup((uint32*)aeGpioConfigGroup,
                            (uint16)nSize,
                           (boolean)eValue);

    eResult = DAL_SUCCESS;
  }
  return(eResult);

} /* Tlmm_GpioOutGroupInternal */


/*==========================================================================

  FUNCTION      Tlmm_GpioOutGroup

  DESCRIPTION   See DDITlmm.h

==========================================================================*/ 

DALResult Tlmm_GpioOutGroup
( 
  TLMMClientCtxt*    pCtxt,   
  TLMMGpioSignalType* aeGpioConfigGroup,  
  uint32             nSize,
  TLMMGpioValueType   eValue
)
{
  return(Tlmm_GpioOutGroupInternal(pCtxt, 
                                   aeGpioConfigGroup, 
                                   nSize, 
                                   eValue, 
                                   (TLMMGpioValueType)NULL));

} /* Tlmm_GpioOutGroup */


/*==========================================================================

  FUNCTION      Tlmm_GpioOutInternal

  DESCRIPTION   Internal API to output a value HIGH/LOW to a GPIO.  This
                API handles GPIO ID contention to ensure that the owner
                of a GPIO ID does not have it's GPIO(s) values changed
                by another user.

==========================================================================*/ 

static DALResult Tlmm_GpioOutInternal
( 
  TLMMClientCtxt*    pCtxt,   
  TLMMGpioSignalType  eGpioConfig,   
  TLMMGpioValueType   eValue,
  TLMMGpioIdKeyType      nGpioId
) 
{
  return(Tlmm_GpioOutGroupInternal(pCtxt, &eGpioConfig, 1, eValue, nGpioId));

} /* Tlmm_GpioOutInternal */


/*==========================================================================

  FUNCTION      Tlmm_GpioOut

  DESCRIPTION   See DDITlmm.h

==========================================================================*/ 

DALResult Tlmm_GpioOut
( 
  TLMMClientCtxt*    pCtxt,   
  TLMMGpioSignalType  eGpioConfig,   
  TLMMGpioValueType   eValue
) 
{
  return(Tlmm_GpioOutInternal(pCtxt, eGpioConfig, eValue, (TLMMGpioValueType)NULL));

} /* Tlmm_GpioOut */


/*==========================================================================

  FUNCTION      Tlmm_GetOutput

  DESCRIPTION   See DDITlmm.h

==========================================================================*/

DALResult Tlmm_GetOutput
( 
  TLMMClientCtxt*   pCtxt, 
  uint32            nGpioNumber, 
  TLMMGpioValueType* nValue
)
{
  if( nGpioNumber < gnTotalGPIO )
  {
    if(HAL_tlmm_GetOutput(nGpioNumber)) 
      *nValue = TLMM_GPIO_HIGH_VALUE;
    else
      *nValue = TLMM_GPIO_LOW_VALUE;
    return(DAL_SUCCESS);
  }
  return(DAL_ERROR);

} /* Tlmm_GetOutput */


/*==========================================================================

  FUNCTION      Tlmm_GetGpioId

  DESCRIPTION   See DDITlmm.h

==========================================================================*/

DALResult Tlmm_GetGpioId
( 
  const char*         pszGpioStr,
  TLMMGpioIdKeyType*  pnGpioId
)
{
  DALSYSPropertyVar tPropVar;
  uint32 nGpioNumber = 0;
  TLMMGpioIdKeyType nTempGpioId = 0;

  if(pszGpioStr == NULL ||
      pnGpioId == NULL)
  {
    return(DAL_ERROR);
  }

  (void)DALSYS_SyncEnter(TlmmLocalCtxt.DevCtxt.hTlmmSync);
#if 0 /* TODO remove - compiler warning */
  if( TlmmLocalCtxt.DevCtxt.hProp != NULL )
  {
#endif
    if(DAL_SUCCESS == 
       DALSYS_GetPropertyValue( TlmmLocalCtxt.DevCtxt.hProp, 
                                pszGpioStr, 
                                0, 
                                &tPropVar ))
    {
      nTempGpioId = (TLMMGpioIdKeyType)tPropVar.Val.pStruct;

      nGpioNumber = ((TLMMGpioIdType*)nTempGpioId)->nGpioNumber;

      /*
       * Only return the GPIO ID if it is not already in use by another entity.
       */
      if(ganGpioIdUsers[nGpioNumber].nGpioKey == 0)
      {
        ganGpioIdUsers[nGpioNumber].nGpioKey = TLMM_GPIO_SET_KEY(nGpioNumber, nTempGpioId);
        *pnGpioId = ganGpioIdUsers[nGpioNumber].nGpioKey;
        ganGpioIdUsers[nGpioNumber].nGpioId = nTempGpioId;

        (void)DALSYS_SyncLeave(TlmmLocalCtxt.DevCtxt.hTlmmSync);

        return(DAL_SUCCESS);
      }
      else
      {
        *pnGpioId = NULL;
        return(DAL_ERROR);
      }
    }
#if 0 /* TODO remove - compiler warning */
  }
#endif
  (void)DALSYS_SyncLeave(TlmmLocalCtxt.DevCtxt.hTlmmSync);

  return(DAL_ERROR);

} /* Tlmm_GetGpioId */


/*==========================================================================

  FUNCTION      Tlmm_ConfigGpioId

  DESCRIPTION   See DDITlmm.h

==========================================================================*/

DALResult Tlmm_ConfigGpioId
( 
  TLMMGpioIdKeyType    nGpioId,
  TLMMGpioConfigIdType *pUserSettings
)
{
  uint32 nGpioNumber = TLMM_GPIO_GET_PIN(nGpioId);
  TLMMGpioIdType* pGpioId = NULL;

  if(nGpioNumber < gnTotalGPIO)
  {
    if(nGpioId == ganGpioIdUsers[nGpioNumber].nGpioKey)
    {
      pGpioId = (TLMMGpioIdType*)ganGpioIdUsers[nGpioNumber].nGpioId;

      if( (pGpioId != NULL) && (pUserSettings != NULL) )
      {
        Tlmm_ConfigGpioInternal(&TlmmLocalCtxt, 
            TLMM_GPIO_CFG(pGpioId->nGpioNumber,
              pGpioId->nFunctionSelect,
              pUserSettings->eDirection,
              pUserSettings->ePull,
              pUserSettings->eDriveStrength),
            TLMM_GPIO_ENABLE,
            nGpioId);
        return(DAL_SUCCESS);
      }
    }
  }
  return(DAL_ERROR);

} /* Tlmm_ConfigGpioId */


/*==========================================================================

  FUNCTION      Tlmm_ConfigGpioIdInactive

  DESCRIPTION   See DDITlmm.h

==========================================================================*/

DALResult Tlmm_ConfigGpioIdInactive
( 
  TLMMGpioIdKeyType  nGpioId
)
{
  uint32 nGpioNumber = TLMM_GPIO_GET_PIN(nGpioId);
  TLMMGpioIdType* pGpioId = NULL;

  if(nGpioNumber < gnTotalGPIO)
  {
    if(nGpioId == ganGpioIdUsers[nGpioNumber].nGpioKey)
    {
      pGpioId = (TLMMGpioIdType*)ganGpioIdUsers[nGpioNumber].nGpioId;

      if( pGpioId != NULL )
      {
        /*
         * We only care about the GPIO number since all other 
         * parameters are extracted from the global BSP. 
         */
        Tlmm_ConfigGpioInternal(&TlmmLocalCtxt, 
            TLMM_GPIO_CFG(pGpioId->nGpioNumber,
              0,
              0,
              0,
              0),
            TLMM_GPIO_DISABLE,
            nGpioId);

        return(DAL_SUCCESS);
      }
    }
  }

  return(DAL_ERROR);

} /* Tlmm_ConfigGpioIdInactive */


/*==========================================================================

  FUNCTION      Tlmm_GpioIdOut

  DESCRIPTION   See DDITlmm.h

==========================================================================*/

DALResult Tlmm_GpioIdOut
(
  TLMMGpioIdKeyType         nGpioId,
  TLMMGpioValueType         eGpioValue
)
{
  uint32 nGpioNumber = TLMM_GPIO_GET_PIN(nGpioId);
  TLMMGpioIdType* pGpioId = NULL;

  if(nGpioNumber < gnTotalGPIO)
  {
    if(nGpioId == ganGpioIdUsers[nGpioNumber].nGpioKey)
    {
      pGpioId = (TLMMGpioIdType*)ganGpioIdUsers[nGpioNumber].nGpioId;
      if( pGpioId != NULL )
      {
        return Tlmm_GpioOutInternal(&TlmmLocalCtxt, TLMM_GPIO_CFG(pGpioId->nGpioNumber,
              pGpioId->nFunctionSelect,
              TLMM_GPIO_OUTPUT,
              TLMM_GPIO_NO_PULL,
              TLMM_GPIO_2MA),
            eGpioValue,
            nGpioId);
      }
    }
  }

  return DAL_ERROR;

} /* Tlmm_GpioIdOut */


/*==========================================================================

  FUNCTION      Tlmm_GpioIdIn

  DESCRIPTION   See DDITlmm.h

==========================================================================*/

DALResult Tlmm_GpioIdIn
(
  TLMMGpioIdKeyType         nGpioId,
  TLMMGpioValueType         *eGpioValue
)
{
  uint32 nGpioNumber = TLMM_GPIO_GET_PIN(nGpioId);
  TLMMGpioIdType* pGpioId = NULL;
  if(nGpioNumber < gnTotalGPIO)
  {
    if(nGpioId == ganGpioIdUsers[nGpioNumber].nGpioKey)
    {
      pGpioId = (TLMMGpioIdType*)ganGpioIdUsers[nGpioNumber].nGpioId;
      if( pGpioId != NULL )
      {
        return Tlmm_GpioIn(&TlmmLocalCtxt, 
            TLMM_GPIO_CFG(pGpioId->nGpioNumber,
              pGpioId->nFunctionSelect,
              TLMM_GPIO_INPUT,
              TLMM_GPIO_NO_PULL,
              TLMM_GPIO_2MA),
            eGpioValue);
      }
    }
  }

  return DAL_ERROR;

} /* Tlmm_GpioIdIn */


/*==========================================================================

  FUNCTION      Tlmm_ReleaseGpioId

  DESCRIPTION   See DDITlmm.h

==========================================================================*/

DALResult Tlmm_ReleaseGpioId
(
  TLMMGpioIdKeyType    nGpioId
)
{
  uint32 nGpioNumber = TLMM_GPIO_GET_PIN(nGpioId);

  if(nGpioNumber < gnTotalGPIO)
  {
    /*
     * Only the holder of the GPIO ID for this GPIO number can 
     * unlock access to it for others to use. 
     */
    if( ganGpioIdUsers[nGpioNumber].nGpioKey == nGpioId )
    {
      ganGpioIdUsers[nGpioNumber].nGpioKey = NULL;
      return(DAL_SUCCESS);
    }
  }

  return(DAL_ERROR);
 
} /* Tlmm_ReleaseGpioId */


/*==========================================================================

  FUNCTION      Tlmm_SelectGpioIdMode

  DESCRIPTION   See DDITlmm.h

==========================================================================*/

DALResult Tlmm_SelectGpioIdMode
(
  TLMMGpioIdKeyType          nGpioId,
  TLMMGpioModeType           eMode,
  TLMMGpioConfigIdType       *pUserSettings
)
{
  TLMMGpioIdType* pGpioId;
  uint32 nFunction;
  uint32 nGpioNumber = TLMM_GPIO_GET_PIN(nGpioId);

  /*
   * Only the holder of the GPIO ID for this GPIO number can 
   * unlock access to it for others to use. 
   */
  if(nGpioNumber < gnTotalGPIO)
  {
    if( ganGpioIdUsers[nGpioNumber].nGpioKey == nGpioId )
    {
      pGpioId = (TLMMGpioIdType*)ganGpioIdUsers[nGpioNumber].nGpioId;
      nGpioNumber = pGpioId->nGpioNumber;
      nFunction = pGpioId->nFunctionSelect;

      if( eMode == TLMM_GPIO_MODE_PRIMARY )
      {
        if( pUserSettings == NULL )
        {
          /*
           * Place the function select of this GPIO to its primary setting.
           */
          HAL_tlmm_SetFunction(nGpioNumber, nFunction);
        }
        else
        {
          /*
           * Program the entire configuration based on the user settings.
           */
          Tlmm_ConfigGpioInternal(&TlmmLocalCtxt, 
              TLMM_GPIO_CFG(nGpioNumber,
                nFunction,
                pUserSettings->eDirection,
                pUserSettings->ePull,
                pUserSettings->eDriveStrength),
              TLMM_GPIO_ENABLE,
              nGpioId);
        }

      }
      else
      {
        if( pUserSettings == NULL )
        {
          /*
           * Set to IO mode on this pin.
           */
          HAL_tlmm_SetFunction(nGpioNumber, 0);
        }
        else
        {
          /*
           * Program the user settings to use with IO mode.
           */
          Tlmm_ConfigGpioInternal(&TlmmLocalCtxt, 
              TLMM_GPIO_CFG(nGpioNumber,
                0,
                pUserSettings->eDirection,
                pUserSettings->ePull,
                pUserSettings->eDriveStrength),
              TLMM_GPIO_ENABLE,
              nGpioId);
        }
      }
      return(DAL_SUCCESS);
    }
  }

  return(DAL_ERROR);

} /* Tlmm_SelectGpioIdMode */


/*==========================================================================

  FUNCTION      Tlmm_GetGpioIdSettings

  DESCRIPTION   See DDITlmm.h

==========================================================================*/

DALResult Tlmm_GetGpioIdSettings
(
  TLMMGpioIdKeyType      nGpioId,
  TLMMGpioIdSettingsType *pGpioSettings
)
{
  uint32 nGpioNumber = TLMM_GPIO_GET_PIN(nGpioId);
  HAL_tlmm_GpioType GpioParams;

  if(pGpioSettings == NULL)
  {
    return(DAL_ERROR);
  }

  /*
   * If the GPIO number is valid we can retreive the current HW settings.
   */
  if(nGpioNumber < gnTotalGPIO)
  {
    pGpioSettings->nGpioNumber = nGpioNumber;

    /*
     * Call HAL to do a HW read and return the current settings.
     */
    HAL_tlmm_GetConfig(nGpioNumber, &GpioParams);

    pGpioSettings->nFunctionSelect = GpioParams.nFunc;
    pGpioSettings->Settings.eDirection = (TLMMGpioDirectionType)GpioParams.nDir;
    pGpioSettings->Settings.ePull = (TLMMGpioPullType)GpioParams.nPull;
    pGpioSettings->Settings.eDriveStrength = (TLMMGpioDriveType)GpioParams.nDrive;

    /*
     * Check for any output value being driven.
     */
    if(HAL_tlmm_GetOutput(nGpioNumber) == TRUE)
    {
      pGpioSettings->eOutputDrive = TLMM_GPIO_HIGH_VALUE;
    }
    else
    {
      pGpioSettings->eOutputDrive = TLMM_GPIO_LOW_VALUE;
    }

    return(DAL_SUCCESS);
  }
  else
  {
    return(DAL_ERROR);
  }

} /* Tlmm_GetGpioIdSettings */

