/*! \file pm_debug.c
*  \n
*  \brief This file contains the implementation of the PMIC debug APIs.
*  \n
*  \n &copy; Copyright 2014 QUALCOMM Technologies Incorporated, All Rights Reserved
*/
/* =======================================================================
Edit History
This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/systemdrivers/pmic/drivers/debug/src/pm_debug.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
06/30/14   vtw     Created
========================================================================== */

/*===========================================================================

INCLUDE FILES FOR MODULE

===========================================================================*/

#include "DALSys.h"
#include "pmapp_npa.h"
#include "sysdbg_mem_dump.h"
#include "pm_internal.h"
#include "pm_lib_err.h"
#include "pm_debug.h"
#include "spmi_1_0_lite.h"

#define SYSDEBUG_MAGIC_NUM     0x42445953

/* TZ PMIC handle property */
DALSYS_PROPERTY_HANDLE_DECLARE(hProp);

static pm_err_flag_type pm_dump_reg
(
  const char* rail_prop,
  PMDumpType* dump_buff      /* output buffer */
)
{
  uint8 reg_value;
  uint16 reg_addr;
  uint32 reg_count = 0;
  const uint16* reg_offset = NULL;

  pm_rail_reg_type* reg_info = NULL;

  PMRegDumpType* reg_table = dump_buff->PMRegs;

  uint16 periph_cnt;     /* index into peripheral array */
  uint16 reg_cnt;        /* index into register block of a peripheral */

  DALResult result = DAL_SUCCESS;
  DALSYSPropertyVar prop_var;

  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

  if (dump_buff == NULL)
  {
    return PM_ERR_FLAG__INVALID_POINTER;
  }

  /* get pehripheral info */
  if (DALSYS_GetPropertyValue(hProp, rail_prop, 0, &prop_var) != DAL_SUCCESS)
  {
    return PM_ERR_FLAG__DAL_SERVICE_FAILED;
  }

  reg_info =  (pm_rail_reg_type*)prop_var.Val.pStruct;

  for (periph_cnt = 0; reg_info[periph_cnt].base_addr != 0xffff; periph_cnt++)
  {
    if (reg_info[periph_cnt].rail_t == PM_RAIL_TYPE_SMPS)
    {
      reg_offset = pm_smps_regs_offset;
    }
    else
    {
      reg_offset = pm_ldo_regs_offset;
    }
    for ( reg_cnt = 0; reg_offset[reg_cnt] != 0xffff;  reg_cnt++)
    {

      reg_addr =  reg_info[periph_cnt].base_addr + reg_offset[reg_cnt];

      err_flag = pm_spmi_lite_read_byte(reg_info[periph_cnt].slave_id,  reg_addr, &reg_value, 0);
      if (err_flag == PM_ERR_FLAG__SUCCESS)
      {
        reg_table[reg_count].slaveid = reg_info[periph_cnt].slave_id;
        reg_table[reg_count].regoffs = reg_addr;
        reg_table[reg_count].val = reg_value;
        reg_count++;
      }
      else
      {
        return err_flag;
      }
    }
  }/* for each peripheral */

  /* Number of register being process. */
  dump_buff->count = reg_count;

  return err_flag;

}/* pm_dump_reg */

/**
 * @brief This function captures the content of PMIC registers and stores
 *        to the host memory buffer.
 *
 * @details
 *
 * @param[in/out] a pointer to a host buffer contains PMIC register infomation from
 *         DDR rails(Cx,MX,Px1 & Px3) on success otherwise a NULL pointer.
 *
 * @return
 */
pm_err_flag_type pm_register_dump( dump_data_type** reg_buffer_ptr )
{

  DALSYSPropertyVar prop;

  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;;
  PMDumpType* pmic_reg = NULL;  // pointer to PMIC register info buffer.
  dump_data_type*  host_buffer = NULL;

  /* Get the host buffer */
  host_buffer = sysdbg_get_pmdump();
  if (host_buffer == NULL)
  {
    return PM_ERR_FLAG__INVALID_POINTER;
  }

  if(DALSYS_GetDALPropertyHandleStr("/tz/pmic", hProp) != DAL_SUCCESS)
  {
    return  PM_ERR_FLAG__DAL_SERVICE_FAILED;
  }

  /* Get the data buffer address. */
  pmic_reg = (PMDumpType*)(uintptr_t)host_buffer->start_addr;

  /* Dump PMIC registers for DDR rails. */
  err_flag |= pm_dump_reg("debug_rail_reg" , pmic_reg);

  if (err_flag == PM_ERR_FLAG__SUCCESS)
  {
    /* set succeess flag */
    host_buffer->magic = SYSDEBUG_MAGIC_NUM;
    *reg_buffer_ptr   = host_buffer;
  }

  return err_flag;

} /* pm_get_reg_dump */

