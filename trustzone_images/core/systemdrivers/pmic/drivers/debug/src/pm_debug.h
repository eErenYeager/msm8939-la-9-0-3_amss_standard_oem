#ifndef PM_DEBUG_H
#define PM_DEBUG_H

/*! \file
 *  \n
 *  \brief  pm_debug.h
 *  \n
 *  \n This header file contains definitions for PMIC TZ debug.
 *  \n
 *  \n &copy; Copyright 2014 Qualcomm Technologies Incorporated, All Rights Reserved
 */
/* ======================================================================= */

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/systemdrivers/pmic/drivers/debug/src/pm_debug.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------

06/25/14   vtw     Created.
=============================================================================*/

#include "comdef.h"

/**
  * PMIC Peripheral Register Info.
  */

const uint16 pm_smps_regs_offset[] =
{
  0x0008, /* STATUS_1 */
  0x0009, /* STATUS_2 */
  0x000a, /* STATUS_3 */
  0x000b, /* STATUS_4 */
  0x000c, /* STATUS_5 */
  0x0010, /* INT_RT_STS */
  0x0014,  /* INT_LATCHED_CLR */
  0x0040, /* VOLTAGE_CTL1 */
  0x0041, /* VOLTAGE_CTL2 */
  0x0042, /* VSET_VALID */
  0x0045, /* MODE_CTL */
  0x0050, /* FREQ_CTL */
  0x0061,  /* VS_CTL */
  0xffff /* end array */
};


const uint16 pm_ldo_regs_offset[] =
{
  0x0008, /* STATUS_1 */
  0x0009, /* STATUS_2 */
  0x000a, /* STATUS_3 */
  0x0010, /* INT_RT_STS */
  0x0014,  /* INT_LATCHED_CLR */
  0x0040, /* VOLTAGE_CTL1 */
  0x0041, /* VOLTAGE_CTL2 */
  0x0045, /* MODE_CTL2 */
  0x0061,  /* VS_CTL */
  0xffff /* end array */
};


#endif /* PM_DEBUG_H */
