#===========================================================================
#
#  @file hwio_msm8996.py
#  @brief HWIO config file for the HWIO generation scripts for MSM8996.
#
#  This file can be invoked by calling:
#
#    HWIOGen.py --cfg=hwio_msm8996.py --flat=..\..\..\api\systemdrivers\hwio\msm8996\ARM_ADDRESS_FILE.FLAT
#
#  ===========================================================================
#
#  Copyright (c) 2011 Qualcomm Technologies Incorporated.  
#  All Rights Reserved.
#  QUALCOMM Proprietary and Confidential.
#
#  ===========================================================================
#
#  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/systemdrivers/hwio/build/hwio_msm8996.py#1 $
#  $DateTime: 2018/02/07 00:37:16 $
#  $Author: mplp4svc $
#
#  ===========================================================================

CHIPSET = 'msm8996'

# ============================================================================
# HWIO_BASE_FILES
# ============================================================================

bases = [
  '.*'  # We map everything for TZ for the moment.
]

base_resize = {

}

HWIO_BASE_FILES = [
  {
    'filename': '../../../api/systemdrivers/hwio/' + CHIPSET + '/phys/msmhwiobase.h',
    'bases': bases,
    'map-type': 'physical',
    'resize': base_resize,
    'check-sizes': True,
    'check-for-overlaps': True,
  },
]


# ============================================================================
# HWIO_REGISTER_FILES
# ============================================================================

# ============================================================================
# HWIO_T32VIEW_FILES
# ============================================================================

HWIO_T32VIEW_FILES = [
  {
    'symbol-filename': '../scripts/' + CHIPSET + '/hwio.cmm',
    'limit-array-size': [ 10, 4 ],
    'per-filename': '../scripts/' + CHIPSET + '/hwioreg',
    'filter-exclude': ['RESERVED', 'DUMMY']
  },
]


# ============================================================================
# Main
#
# Entry point when invoking this directly.
# ============================================================================

if __name__ == "__main__":
  from subprocess import Popen
  Popen(["\\\\ben\\corebsp_labdata_0001\\sysdrv\\hwio\\HWIOGen.py", "--cfg=hwio_" + CHIPSET + ".py", "--flat=../../../api/systemdrivers/hwio/" + CHIPSET + "/ARM_ADDRESS_FILE.FLAT"], shell=True)



