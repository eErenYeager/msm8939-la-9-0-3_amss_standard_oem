#===========================================================================
#
#  @file hwio_msm8994.py
#  @brief HWIO config file for the HWIO generation scripts for MSM8994.
#
#  This file can be invoked by calling:
#
#    HWIOGen.py --cfg=hwio_msm8994.py --flat=..\..\..\api\systemdrivers\hwio\msm8994\ARM_ADDRESS_FILE.FLAT
#
#  ===========================================================================
#
#  Copyright (c) 2011 Qualcomm Technologies Incorporated.  
#  All Rights Reserved.
#  QUALCOMM Proprietary and Confidential.
#
#  ===========================================================================
#
#  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/systemdrivers/hwio/build/hwio_msm8994.py#1 $
#  $DateTime: 2018/02/07 00:37:16 $
#  $Author: mplp4svc $
#
#  ===========================================================================

CHIPSET = 'msm8994'

# ============================================================================
# HWIO_BASE_FILES
# ============================================================================

bases = [
  '.*'  # We map everything for TZ for the moment.
]

base_resize = {
  'BOOT_ROM_END_ADDRESS':               0x0,
  'BOOT_ROM_SIZE':                      0x0,
  'BOOT_ROM_START_ADDRESS':             0x0,
  'EBI1_MEM':                           0x0,
  'EBI1_MEM_END':                       0x0,
  'EBI1_MEM_RPM_REGION0':               0x0,
  'EBI1_MEM_RPM_REGION0_END':           0x0,
  'EBI1_MEM_RPM_REGION0_SIZE':          0x0,
  'EBI1_MEM_RPM_REGION1':               0x0,
  'EBI1_MEM_RPM_REGION1_END':           0x0,
  'EBI1_MEM_RPM_REGION1_SIZE':          0x0,
  'EBI1_MEM_RPM_REGION2':               0x0,
  'EBI1_MEM_RPM_REGION2_END':           0x0,
  'EBI1_MEM_RPM_REGION2_SIZE':          0x0,
  'EBI1_MEM_RPM_REGION3':               0x0,
  'EBI1_MEM_RPM_REGION3_END':           0x0,
  'EBI1_MEM_RPM_REGION3_SIZE':          0x0,
  'EBI1_MEM_RPM_REGION4':               0x0,
  'EBI1_MEM_RPM_REGION4_END':           0x0,
  'EBI1_MEM_RPM_REGION4_SIZE':          0x0,
  'EBI1_MEM_RPM_REGION5':               0x0,
  'EBI1_MEM_RPM_REGION5_END':           0x0,
  'EBI1_MEM_RPM_REGION5_SIZE':          0x0,
  'EBI1_MEM_SIZE':                      0x0,
  'KRAIT_L2_MEM':                       0x0,
  'KRAIT_L2_MEM_END':                   0x0,
  'KRAIT_L2_MEM_SIZE':                  0x0,
  'LPASS_LPM':                          0x0,
  'LPASS_LPM_END':                      0x0,
  'LPASS_LPM_SIZE':                     0x0,
  'MMSS_OCMEM':                         0x0,
  'MMSS_OCMEM_END':                     0x0,
  'MMSS_OCMEM_SIZE':                    0x0,
  'MSS_TCM':                            0x0,
  'MSS_TCM_END':                        0x0,
  'MSS_TCM_SIZE':                       0x0,
  'PRONTO_CMEM':                        0x0,
  'PRONTO_CMEM_END':                    0x0,
  'PRONTO_CMEM_SIZE':                   0x0,
  'RPM_CODE_RAM_END_ADDRESS':           0x0,
  'RPM_CODE_RAM_SIZE':                  0x0,
  'RPM_CODE_RAM_START_ADDRESS':         0x0,
  'RPM_SS_MSG_RAM_END_ADDRESS':         0x0,
  'RPM_SS_MSG_RAM_SIZE':                0x0,
  'RPM_SS_MSG_RAM_START_ADDRESS':       0x0,
  'SYSTEM_IMEM':                        0x0,
  'SYSTEM_IMEM_END':                    0x0,
  'SYSTEM_IMEM_SIZE':                   0x0,
}

HWIO_BASE_FILES = [
  {
    'filename': '../../../api/systemdrivers/hwio/' + CHIPSET + '/phys/msmhwiobase.h',
    'bases': bases,
    'map-type': 'physical',
    'resize': base_resize,
    'check-sizes': True,
    'check-for-overlaps': True,
  },
]


# ============================================================================
# HWIO_REGISTER_FILES
# ============================================================================

# ============================================================================
# HWIO_T32VIEW_FILES
# ============================================================================

HWIO_T32VIEW_FILES = [
  {
    'symbol-filename': '../scripts/' + CHIPSET + '/hwio.cmm',
    'limit-array-size': [ 10, 4 ],
    'per-filename': '../scripts/' + CHIPSET + '/hwioreg',
    'filter-exclude': ['RESERVED', 'DUMMY']
  },
]


# ============================================================================
# Main
#
# Entry point when invoking this directly.
# ============================================================================

if __name__ == "__main__":
  from subprocess import Popen
  Popen(["\\\\ben\\corebsp_labdata_0001\\sysdrv\\hwio\\HWIOGen.py", "--cfg=hwio_" + CHIPSET + ".py", "--flat=../../../api/systemdrivers/hwio/" + CHIPSET + "/ARM_ADDRESS_FILE.FLAT"], shell=True)



