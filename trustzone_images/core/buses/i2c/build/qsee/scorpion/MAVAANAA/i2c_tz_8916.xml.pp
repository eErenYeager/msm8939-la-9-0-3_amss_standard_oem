# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/buses/i2c/config/i2c_tz_8916.xml"
# 1 "<built-in>" 1
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/buses/i2c/config/i2c_tz_8916.xml" 2
# 23 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/buses/i2c/config/i2c_tz_8916.xml"
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8916/src/tzbsp_xpu_def.h" 1
# 24 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8916/src/tzbsp_xpu_def.h"
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8916/cfg/mmu/tzbsp_hwio.h" 1
# 40 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8916/cfg/mmu/tzbsp_hwio.h"
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8916/cfg/mmu/../common/tzbsp_hwio_temp.h" 1
# 41 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8916/cfg/mmu/tzbsp_hwio.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8916/cfg/mmu/../common/tzbsp_hwio_prng.h" 1
# 42 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8916/cfg/mmu/tzbsp_hwio.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8916/cfg/mmu/../common/tzbsp_hwio.h" 1
# 53 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8916/cfg/mmu/../common/tzbsp_hwio.h"
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/api/systemdrivers/hwio/msm8916/phys/msmhwiobase.h" 1
# 54 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8916/cfg/mmu/../common/tzbsp_hwio.h" 2
# 43 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8916/cfg/mmu/tzbsp_hwio.h" 2
# 25 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8916/src/tzbsp_xpu_def.h" 2
# 24 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/buses/i2c/config/i2c_tz_8916.xml" 2


<driver name="I2C">
    <global_def>
       <var_seq name = "i2cqup1_gpio_number_arr" type=DALPROP_DATA_TYPE_UINT32_SEQ>
         2,3, end
       </var_seq>
       <var_seq name = "i2cqup2_gpio_number_arr" type=DALPROP_DATA_TYPE_UINT32_SEQ>
         6,7, end
       </var_seq>
       <var_seq name = "i2cqup3_gpio_number_arr" type=DALPROP_DATA_TYPE_UINT32_SEQ>
         10,11, end
       </var_seq>
       <var_seq name = "i2cqup4_gpio_number_arr" type=DALPROP_DATA_TYPE_UINT32_SEQ>
         14,15, end
       </var_seq>
       <var_seq name = "i2cqup5_gpio_number_arr" type=DALPROP_DATA_TYPE_UINT32_SEQ>
         18,19, end
       </var_seq>
       <var_seq name = "i2cqup6_gpio_number_arr" type=DALPROP_DATA_TYPE_UINT32_SEQ>
         22,23, end
       </var_seq>

    </global_def>

    <device id=DALDEVICEID_I2C_DEVICE_1>
        <props name="CHIP_BUS_INDEX" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
        <props name="CHIP_IRQ_NUM" type=DALPROP_ATTR_TYPE_UINT32> 127 </props>
        <props name="CHIP_QUP_BASE_ADDR" type=DALPROP_ATTR_TYPE_UINT32> 0x78B5000</props>
        <props name="CHIP_APPS_CLK_FREQ_KHZ" type=DALPROP_ATTR_TYPE_UINT32> 20000</props>
        <props name="CHIP_BLSP_NUM" type=DALPROP_ATTR_TYPE_UINT32> 1</props>
        <props name="CHIP_QUP_RG_NUM" type=DALPROP_ATTR_TYPE_UINT32> 9</props>
        <props name="GPIO_NUM_ARR" type=DALPROP_ATTR_TYPE_UINT32_SEQ_PTR> i2cqup1_gpio_number_arr </props>
        <props name="SW_ENABLE_INTERRUPTS" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>






        <props name="GPIO_MPU_PROTECTED" type=DALPROP_ATTR_TYPE_UINT32> 0x1 </props>
        <props name="GPIO_IN_SEQUENCE" type=DALPROP_ATTR_TYPE_UINT32> 0x1 </props>
        <props name="I2C_TZ_GPIO_MPU_INDEX_1" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
        <props name="I2C_TZ_GPIO_MPU_INDEX_2" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>

        <props name="I2C_GPIO_SDA_START_ADDR" type=DALPROP_ATTR_TYPE_UINT32> 0x1002000 </props>
        <props name="I2C_GPIO_SDA_END_ADDR" type=DALPROP_ATTR_TYPE_UINT32> 0x1003000 </props>
        <props name="I2C_GPIO_SCL_START_ADDR" type=DALPROP_ATTR_TYPE_UINT32> 0x1003000 </props>
        <props name="I2C_GPIO_SCL_END_ADDR" type=DALPROP_ATTR_TYPE_UINT32> 0x1004000 </props>


    </device>
    <device id=DALDEVICEID_I2C_DEVICE_2>
        <props name="CHIP_BUS_INDEX" type=DALPROP_ATTR_TYPE_UINT32> 1 </props>
 <props name="CHIP_IRQ_NUM" type=DALPROP_ATTR_TYPE_UINT32> 128 </props>
        <props name="CHIP_QUP_BASE_ADDR" type=DALPROP_ATTR_TYPE_UINT32> 0x78B6000</props>
        <props name="CHIP_APPS_CLK_FREQ_KHZ" type=DALPROP_ATTR_TYPE_UINT32> 20000</props>
        <props name="CHIP_BLSP_NUM" type=DALPROP_ATTR_TYPE_UINT32> 1</props>
        <props name="CHIP_QUP_RG_NUM" type=DALPROP_ATTR_TYPE_UINT32> 10</props>
        <props name="GPIO_NUM_ARR" type=DALPROP_ATTR_TYPE_UINT32_SEQ_PTR> i2cqup2_gpio_number_arr </props>
        <props name="SW_ENABLE_INTERRUPTS" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>


        <props name="GPIO_MPU_PROTECTED" type=DALPROP_ATTR_TYPE_UINT32> 0x1 </props>
        <props name="GPIO_IN_SEQUENCE" type=DALPROP_ATTR_TYPE_UINT32> 0x1 </props>
        <props name="I2C_TZ_GPIO_MPU_INDEX_1" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
        <props name="I2C_TZ_GPIO_MPU_INDEX_2" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
        <props name="I2C_GPIO_SDA_START_ADDR" type=DALPROP_ATTR_TYPE_UINT32> 0x1006000 </props>
        <props name="I2C_GPIO_SDA_END_ADDR" type=DALPROP_ATTR_TYPE_UINT32> 0x1007000 </props>
        <props name="I2C_GPIO_SCL_START_ADDR" type=DALPROP_ATTR_TYPE_UINT32> 0x1007000 </props>
        <props name="I2C_GPIO_SCL_END_ADDR" type=DALPROP_ATTR_TYPE_UINT32> 0x1008000 </props>
    </device>
    <device id=DALDEVICEID_I2C_DEVICE_3>
        <props name="CHIP_BUS_INDEX" type=DALPROP_ATTR_TYPE_UINT32> 2 </props>
 <props name="CHIP_IRQ_NUM" type=DALPROP_ATTR_TYPE_UINT32> 129 </props>
        <props name="CHIP_QUP_BASE_ADDR" type=DALPROP_ATTR_TYPE_UINT32> 0x78B7000</props>
        <props name="CHIP_APPS_CLK_FREQ_KHZ" type=DALPROP_ATTR_TYPE_UINT32> 20000</props>
 <props name="CHIP_BLSP_NUM" type=DALPROP_ATTR_TYPE_UINT32> 1</props>
        <props name="CHIP_QUP_RG_NUM" type=DALPROP_ATTR_TYPE_UINT32> 11</props>
        <props name="GPIO_NUM_ARR" type=DALPROP_ATTR_TYPE_UINT32_SEQ_PTR> i2cqup3_gpio_number_arr </props>
        <props name="SW_ENABLE_INTERRUPTS" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>


        <props name="GPIO_MPU_PROTECTED" type=DALPROP_ATTR_TYPE_UINT32> 0x1 </props>
        <props name="GPIO_IN_SEQUENCE" type=DALPROP_ATTR_TYPE_UINT32> 0x1 </props>
        <props name="I2C_TZ_GPIO_MPU_INDEX_1" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
        <props name="I2C_TZ_GPIO_MPU_INDEX_2" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
        <props name="I2C_GPIO_SDA_START_ADDR" type=DALPROP_ATTR_TYPE_UINT32> 0x100A000 </props>
        <props name="I2C_GPIO_SDA_END_ADDR" type=DALPROP_ATTR_TYPE_UINT32> 0x100B000 </props>
        <props name="I2C_GPIO_SCL_START_ADDR" type=DALPROP_ATTR_TYPE_UINT32> 0x100B000 </props>
        <props name="I2C_GPIO_SCL_END_ADDR" type=DALPROP_ATTR_TYPE_UINT32> 0x100C000 </props>
    </device>
    <device id=DALDEVICEID_I2C_DEVICE_4>
        <props name="CHIP_BUS_INDEX" type=DALPROP_ATTR_TYPE_UINT32> 3 </props>
 <props name="CHIP_IRQ_NUM" type=DALPROP_ATTR_TYPE_UINT32> 130 </props>
        <props name="CHIP_QUP_BASE_ADDR" type=DALPROP_ATTR_TYPE_UINT32> 0x78B8000</props>
        <props name="CHIP_APPS_CLK_FREQ_KHZ" type=DALPROP_ATTR_TYPE_UINT32> 20000</props>
 <props name="CHIP_BLSP_NUM" type=DALPROP_ATTR_TYPE_UINT32> 1</props>
        <props name="CHIP_QUP_RG_NUM" type=DALPROP_ATTR_TYPE_UINT32> 12</props>
        <props name="GPIO_NUM_ARR" type=DALPROP_ATTR_TYPE_UINT32_SEQ_PTR> i2cqup4_gpio_number_arr </props>
        <props name="SW_ENABLE_INTERRUPTS" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>


        <props name="GPIO_MPU_PROTECTED" type=DALPROP_ATTR_TYPE_UINT32> 0x1 </props>
        <props name="GPIO_IN_SEQUENCE" type=DALPROP_ATTR_TYPE_UINT32> 0x1 </props>
        <props name="I2C_TZ_GPIO_MPU_INDEX_1" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
        <props name="I2C_TZ_GPIO_MPU_INDEX_2" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
        <props name="I2C_GPIO_SDA_START_ADDR" type=DALPROP_ATTR_TYPE_UINT32> 0x100E000 </props>
        <props name="I2C_GPIO_SDA_END_ADDR" type=DALPROP_ATTR_TYPE_UINT32> 0x100F000 </props>
        <props name="I2C_GPIO_SCL_START_ADDR" type=DALPROP_ATTR_TYPE_UINT32> 0x100F000 </props>
        <props name="I2C_GPIO_SCL_END_ADDR" type=DALPROP_ATTR_TYPE_UINT32> 0x1010000 </props>
    </device>
    <device id=DALDEVICEID_I2C_DEVICE_5>
        <props name="CHIP_BUS_INDEX" type=DALPROP_ATTR_TYPE_UINT32> 4 </props>
 <props name="CHIP_IRQ_NUM" type=DALPROP_ATTR_TYPE_UINT32> 131 </props>
        <props name="CHIP_QUP_BASE_ADDR" type=DALPROP_ATTR_TYPE_UINT32> 0x78B9000</props>
        <props name="CHIP_APPS_CLK_FREQ_KHZ" type=DALPROP_ATTR_TYPE_UINT32> 20000</props>
 <props name="CHIP_BLSP_NUM" type=DALPROP_ATTR_TYPE_UINT32> 1</props>
        <props name="CHIP_QUP_RG_NUM" type=DALPROP_ATTR_TYPE_UINT32> 13</props>
        <props name="GPIO_NUM_ARR" type=DALPROP_ATTR_TYPE_UINT32_SEQ_PTR> i2cqup5_gpio_number_arr </props>
        <props name="SW_ENABLE_INTERRUPTS" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>


        <props name="GPIO_MPU_PROTECTED" type=DALPROP_ATTR_TYPE_UINT32> 0x1 </props>
        <props name="GPIO_IN_SEQUENCE" type=DALPROP_ATTR_TYPE_UINT32> 0x1 </props>
        <props name="I2C_TZ_GPIO_MPU_INDEX_1" type=DALPROP_ATTR_TYPE_UINT32> 15 </props>
        <props name="I2C_TZ_GPIO_MPU_INDEX_2" type=DALPROP_ATTR_TYPE_UINT32> 15 </props>
        <props name="I2C_GPIO_SDA_START_ADDR" type=DALPROP_ATTR_TYPE_UINT32> 0x01012000 </props>
        <props name="I2C_GPIO_SDA_END_ADDR" type=DALPROP_ATTR_TYPE_UINT32> 0x01013000 </props>
        <props name="I2C_GPIO_SCL_START_ADDR" type=DALPROP_ATTR_TYPE_UINT32> 0x01013000 </props>
        <props name="I2C_GPIO_SCL_END_ADDR" type=DALPROP_ATTR_TYPE_UINT32> 0x01014000 </props>
    </device>
    <device id=DALDEVICEID_I2C_DEVICE_6>
        <props name="CHIP_BUS_INDEX" type=DALPROP_ATTR_TYPE_UINT32> 5 </props>
 <props name="CHIP_IRQ_NUM" type=DALPROP_ATTR_TYPE_UINT32> 132 </props>
        <props name="CHIP_QUP_BASE_ADDR" type=DALPROP_ATTR_TYPE_UINT32> 0x78BA000</props>
        <props name="CHIP_APPS_CLK_FREQ_KHZ" type=DALPROP_ATTR_TYPE_UINT32> 20000</props>
 <props name="CHIP_BLSP_NUM" type=DALPROP_ATTR_TYPE_UINT32> 1</props>
        <props name="CHIP_QUP_RG_NUM" type=DALPROP_ATTR_TYPE_UINT32> 14</props>
        <props name="GPIO_NUM_ARR" type=DALPROP_ATTR_TYPE_UINT32_SEQ_PTR> i2cqup6_gpio_number_arr </props>
        <props name="SW_ENABLE_INTERRUPTS" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>


        <props name="GPIO_MPU_PROTECTED" type=DALPROP_ATTR_TYPE_UINT32> 0x1 </props>
        <props name="GPIO_IN_SEQUENCE" type=DALPROP_ATTR_TYPE_UINT32> 0x1 </props>
        <props name="I2C_TZ_GPIO_MPU_INDEX_1" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
        <props name="I2C_TZ_GPIO_MPU_INDEX_2" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
        <props name="I2C_GPIO_SDA_START_ADDR" type=DALPROP_ATTR_TYPE_UINT32> 0x1016000 </props>
        <props name="I2C_GPIO_SDA_END_ADDR" type=DALPROP_ATTR_TYPE_UINT32> 0x1017000 </props>
        <props name="I2C_GPIO_SCL_START_ADDR" type=DALPROP_ATTR_TYPE_UINT32> 0x1017000 </props>
        <props name="I2C_GPIO_SCL_END_ADDR" type=DALPROP_ATTR_TYPE_UINT32> 0x1018000 </props>
    </device>

</driver>
