#ifndef __ICBCFG_HWIO_H__
#define __ICBCFG_HWIO_H__
/*
===========================================================================
*/
/**
  @file icbcfg_hwio.h
  @brief Auto-generated HWIO interface include file.

*/
/*
  ===========================================================================

  Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/buses/icb/src/8916/icbcfg_hwio.h#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $

  ===========================================================================
*/

/* ICBCFG HWIO File for 8916 target (bagheera_v1.0_p3q2r15.2) */
#include "msmhwiobase.h"
/*----------------------------------------------------------------------------
 * BASE: BIMC
 *--------------------------------------------------------------------------*/
#undef  BIMC_BASE
#define BIMC_BASE                                               BIMC_BASE_PHYS

#endif /* __ICBCFG_HWIO_H__ */
