# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/buses/icb/config/icbcfg_tz_8936.xml"
# 1 "<built-in>" 1
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/buses/icb/config/icbcfg_tz_8936.xml" 2
# 32 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/buses/icb/config/icbcfg_tz_8936.xml"
<driver name="NULL">
   <global_def>

   </global_def>
   <device id="/dev/icbcfg/tz">
     <props name="icb_config_data" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
        icbcfg_boot_prop
     </props>
     <props name="num_channels" type=DALPROP_ATTR_TYPE_UINT32>
        1
     </props>
     <props name="num_segments" type=DALPROP_ATTR_TYPE_UINT32>
        3
     </props>
     <props name="map_ddr_region_count" type=DALPROP_ATTR_TYPE_UINT32>
        1
     </props>
     <props name="map_ddr_regions" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
        map_ddr_regions
     </props>
     <props name="bimc_hal_info" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
        bimc_hal_info
     </props>
     <props name="channel_map" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
        channel_map
     </props>
     <props name="safe_reset_seg" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
        safe_reset_seg
     </props>
   </device>
</driver>
