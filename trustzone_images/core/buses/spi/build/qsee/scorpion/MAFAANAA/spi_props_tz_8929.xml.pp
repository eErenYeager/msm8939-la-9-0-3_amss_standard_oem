# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/buses/spi/config/spi_props_tz_8929.xml"
# 1 "<built-in>" 1
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/buses/spi/config/spi_props_tz_8929.xml" 2
# 23 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/buses/spi/config/spi_props_tz_8929.xml"
<driver name="Spi">
    <device id=DALDEVICEID_SPI_DEVICE_1>
        <props name="QUP_CORE_NUM" type=DALPROP_ATTR_TYPE_UINT32>
                1
            </props>
        <props name="QUP_BASE_ADDR" type=DALPROP_ATTR_TYPE_UINT32>
                0x78B5000
            </props>
        <props name="SW_ENABLE_BAM" type=DALPROP_ATTR_TYPE_UINT32>
                1
            </props>
        <props name="BAM_BLSP_ID" type=DALPROP_ATTR_TYPE_UINT32>
                1
            </props>
        <props name="BAM_BASE_ADDR" type=DALPROP_ATTR_TYPE_UINT32>
                0x7884000
            </props>
        <props name="BAM_INTERRUPT_ID" type=DALPROP_ATTR_TYPE_UINT32>
                270
            </props>
        <props name="BAM_TX_PIPE_NUM" type=DALPROP_ATTR_TYPE_UINT32>
                4
            </props>
        <props name="BAM_RX_PIPE_NUM" type=DALPROP_ATTR_TYPE_UINT32>
                5
            </props>
    </device>

    <device id=DALDEVICEID_SPI_DEVICE_2>
        <props name="QUP_CORE_NUM" type=DALPROP_ATTR_TYPE_UINT32>
                2
            </props>
        <props name="QUP_BASE_ADDR" type=DALPROP_ATTR_TYPE_UINT32>
                0x78B6000
            </props>
        <props name="SW_ENABLE_BAM" type=DALPROP_ATTR_TYPE_UINT32>
                1
            </props>
        <props name="BAM_BLSP_ID" type=DALPROP_ATTR_TYPE_UINT32>
                1
            </props>
        <props name="BAM_BASE_ADDR" type=DALPROP_ATTR_TYPE_UINT32>
                0x7884000
            </props>
        <props name="BAM_INTERRUPT_ID" type=DALPROP_ATTR_TYPE_UINT32>
                270
            </props>
        <props name="BAM_TX_PIPE_NUM" type=DALPROP_ATTR_TYPE_UINT32>
                6
            </props>
        <props name="BAM_RX_PIPE_NUM" type=DALPROP_ATTR_TYPE_UINT32>
                7
            </props>
    </device>

    <device id=DALDEVICEID_SPI_DEVICE_3>
        <props name="QUP_CORE_NUM" type=DALPROP_ATTR_TYPE_UINT32>
                3
            </props>
        <props name="QUP_BASE_ADDR" type=DALPROP_ATTR_TYPE_UINT32>
                0x78B7000
            </props>
        <props name="SW_ENABLE_BAM" type=DALPROP_ATTR_TYPE_UINT32>
                1
            </props>
        <props name="BAM_BLSP_ID" type=DALPROP_ATTR_TYPE_UINT32>
                1
            </props>
        <props name="BAM_BASE_ADDR" type=DALPROP_ATTR_TYPE_UINT32>
                0x7884000
            </props>
        <props name="BAM_INTERRUPT_ID" type=DALPROP_ATTR_TYPE_UINT32>
                270
            </props>
        <props name="BAM_TX_PIPE_NUM" type=DALPROP_ATTR_TYPE_UINT32>
                8
            </props>
        <props name="BAM_RX_PIPE_NUM" type=DALPROP_ATTR_TYPE_UINT32>
                9
            </props>
    </device>

    <device id=DALDEVICEID_SPI_DEVICE_4>
        <props name="QUP_CORE_NUM" type=DALPROP_ATTR_TYPE_UINT32>
                4
            </props>
        <props name="QUP_BASE_ADDR" type=DALPROP_ATTR_TYPE_UINT32>
                0x78B8000
            </props>
        <props name="SW_ENABLE_BAM" type=DALPROP_ATTR_TYPE_UINT32>
                1
            </props>
        <props name="BAM_BLSP_ID" type=DALPROP_ATTR_TYPE_UINT32>
                1
            </props>
        <props name="BAM_BASE_ADDR" type=DALPROP_ATTR_TYPE_UINT32>
                0x7884000
            </props>
        <props name="BAM_INTERRUPT_ID" type=DALPROP_ATTR_TYPE_UINT32>
                270
            </props>
        <props name="BAM_TX_PIPE_NUM" type=DALPROP_ATTR_TYPE_UINT32>
                10
            </props>
        <props name="BAM_RX_PIPE_NUM" type=DALPROP_ATTR_TYPE_UINT32>
                11
            </props>
    </device>

    <device id=DALDEVICEID_SPI_DEVICE_5>
        <props name="QUP_CORE_NUM" type=DALPROP_ATTR_TYPE_UINT32>
                5
            </props>
        <props name="QUP_BASE_ADDR" type=DALPROP_ATTR_TYPE_UINT32>
                0x78B9000
            </props>
        <props name="SW_ENABLE_BAM" type=DALPROP_ATTR_TYPE_UINT32>
                1
            </props>
        <props name="BAM_BLSP_ID" type=DALPROP_ATTR_TYPE_UINT32>
                1
            </props>
        <props name="BAM_BASE_ADDR" type=DALPROP_ATTR_TYPE_UINT32>
                0x7884000
            </props>
        <props name="BAM_INTERRUPT_ID" type=DALPROP_ATTR_TYPE_UINT32>
                270
            </props>
        <props name="BAM_TX_PIPE_NUM" type=DALPROP_ATTR_TYPE_UINT32>
                12
            </props>
        <props name="BAM_RX_PIPE_NUM" type=DALPROP_ATTR_TYPE_UINT32>
                13
            </props>
    </device>

    <device id=DALDEVICEID_SPI_DEVICE_6>
        <props name="QUP_CORE_NUM" type=DALPROP_ATTR_TYPE_UINT32>
                6
            </props>
        <props name="QUP_BASE_ADDR" type=DALPROP_ATTR_TYPE_UINT32>
                0x78BA000
            </props>
        <props name="SW_ENABLE_BAM" type=DALPROP_ATTR_TYPE_UINT32>
                1
            </props>
        <props name="BAM_BLSP_ID" type=DALPROP_ATTR_TYPE_UINT32>
                1
            </props>
        <props name="BAM_BASE_ADDR" type=DALPROP_ATTR_TYPE_UINT32>
                0x7884000
            </props>
        <props name="BAM_INTERRUPT_ID" type=DALPROP_ATTR_TYPE_UINT32>
                270
            </props>
        <props name="BAM_TX_PIPE_NUM" type=DALPROP_ATTR_TYPE_UINT32>
                14
            </props>
        <props name="BAM_RX_PIPE_NUM" type=DALPROP_ATTR_TYPE_UINT32>
                15
            </props>
    </device>
</driver>
