/*
===========================================================================

FILE:   SpiDevicePlatSvc.c

DESCRIPTION:

===========================================================================

        Edit History

$Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/buses/spi/src/device/SpiDevicePlatSvc.c#1 $

When     Who    What, where, why
-------- ---    -----------------------------------------------------------
01/29/15 dpk    Added BAM De-Init functionality.
11/20/14  ms    Added changes for GPIO/QUP Protection
10/31/14  ms    Fixed an issue with reading GPIO_NUM_ARR Property.
09/10/13  vk    Use TLMM, bring up to date with mainline driver.
05/15/13  ag    Created 

===========================================================================
        Copyright (c) 2013-14 Qualcomm Technologies Incorporated.
            All Rights Reserved.
            Qualcomm Confidential & Proprietary
===========================================================================
*/

#include "SpiDevicePlatSvc.h"
#include "SpiDeviceOsSvc.h"
#include "SpiDeviceError.h"
#include "tzbsp_spi.h"

#include "stddef.h"
#include "stdio.h"

#include "DALStdDef.h"
#include "DALSys.h"
#include "DALDeviceId.h"
//#include "Tlmm.h"

#define MAX_NUM_QUP_HW_CORES 12
static DALDEVICEID DalSpi_DeviceId[] =
{
   DALDEVICEID_SPI_DEVICE_1,
   DALDEVICEID_SPI_DEVICE_2,
   DALDEVICEID_SPI_DEVICE_3,
   DALDEVICEID_SPI_DEVICE_4,
   DALDEVICEID_SPI_DEVICE_5,
   DALDEVICEID_SPI_DEVICE_6,
   DALDEVICEID_SPI_DEVICE_7,
   DALDEVICEID_SPI_DEVICE_8,
   DALDEVICEID_SPI_DEVICE_9,
   DALDEVICEID_SPI_DEVICE_10,
   DALDEVICEID_SPI_DEVICE_11,
   DALDEVICEID_SPI_DEVICE_12,
};
const DALREG_DriverInfo
   DALSpi_DriverInfo = { NULL,
   MAX_NUM_QUP_HW_CORES,
   DalSpi_DeviceId
};


#define QUP_FREQ_HZ              20000000

#define SPIDEVICEPLAT_CHK_RESULT_BREAK(res)\
   if (SPIDEVICE_RES_SUCCESS !=res) {\
      break;\
   }

typedef enum SpiDevicePlatCfg_Error
{
   SpiDevicePlatCfg_ERROR_BASE = SPIDEVICE_RES_ERROR_CLS_DEV_PLATFORM,

   SPIDEVICE_PLATCFG_RESULT_ERROR_DAL_GET_PROPERTY_HANDLE,
   SPIDEVICE_PLATCFG_RESULT_ERROR_DAL_GET_PROPERTY_VALUE,
   SPIDEVICE_PLATCFG_RESULT_ERROR_ATTACH_TO_CLOCKS,
   SPIDEVICE_PLATCFG_RESULT_ERROR_ATTACH_TO_DALHWIO,
   SPIDEVICE_PLATCFG_RESULT_ERROR_FAILED_TO_MAP_BLOCK_HWIO,
   SPIDEVICE_PLATCFG_RESULT_ERROR_GETTING_CLK_ID,
   SPIDEVICE_PLATCFG_RESULT_ERROR_CREATING_DISPATCH_WORKLOOP,
   SPIDEVICE_PLATCFG_RESULT_ERROR_CREATING_DISPATCH_EVENT,
   SPIDEVICE_PLATCFG_RESULT_ERROR_ASSOCIATING_EVENT_WITH_WORKLOOP,
   SPIDEVICE_PLATCFG_RESULT_ERROR_INVALID_POWER_STATE,
   SPIDEVICE_PLATCFG_RESULT_ERROR_FAILED_TO_SET_APPCLK_FREQ,
   SPIDEVICE_PLATCFG_RESULT_ERROR_FAILED_TO_GET_APPCLK_FREQ,
   SPIDEVICE_PLATCFG_RESULT_ERROR_FAILED_TO_ENABLE_APPCLK,
   SPIDEVICE_PLATCFG_RESULT_ERROR_FAILED_TO_DISABLE_APPCLK,
   SPIDEVICE_PLATCFG_RESULT_ERROR_FAILED_TO_ENABLE_HCLK,
   SPIDEVICE_PLATCFG_RESULT_ERROR_FAILED_TO_DISABLE_HCLK,
   SPIDEVICE_PLATCFG_RESULT_ERROR_TLMM_ATTACH,
   SPIDEVICE_PLATCFG_RESULT_ERROR_TLMM_OPEN,
   SPIDEVICE_PLATCFG_RESULT_ERROR_CONFIGURING_GPIOS,
   SPIDEVICE_PLATCFG_RESULT_ERROR_NULL_PTR,
   SPIDEVICE_PLATCFG_RESULT_ERROR_REMOVE_DEVCFG_NOT_FOUND,
   SPIDEVICE_PLATCFG_RESULT_ERROR_ENABLE_XPU_PROTECTION,
   SPIDEVICE_PLATCFG_RESULT_ERROR_DISABLE_XPU_PROTECTION,
} SpiDevicePlatCfg_Error;


typedef struct SpiDevicePlatCfg_DevTargetCfgType
{
   char *pQupHClkName;
   char *pQupAppClkName;
   uint32 uOsDevId;
   uint32 uQupCoreNum;
   uint32 qupVirtBlockAddr;
   uint32 qupPhysBlockAddr;
   uint32 tcsrVirtBlockAddr;
   boolean bInterruptBased;
   uint32 gpioConfigured;
   boolean bBamSupported;
   boolean bTcsrInterruptRequired;
   uint32 uTcsrInterruptBitMask;
   uint32 uTcsrInterruptBitShift;
   tzbsp_qup_protection_type qup_prot;
   SpiDeviceBamType *spiBam;
   SpiDeviceBamCfg_t spiBamCfg;
} SpiDevicePlatCfg_DevTargetCfgType;

typedef SpiDevicePlatCfg_DevTargetCfgType *SpiDevicePlatCfg_DevTargetCfgArrayType[12];

static SpiDevicePlatCfg_DevTargetCfgArrayType *platCfgArray = NULL;

static SpiDevicePlatCfg_DevTargetCfgType* SpiDevicePlatCfg_GetTargetConfig(uint32 uOsDevId)
{
   DALSYS_PROPERTY_HANDLE_DECLARE(hProp);
   DALSYSPropertyVar PropVar;
   uint32 uCoreNum = 0;
   SpiDevicePlatCfg_DevTargetCfgType *tgtCfg = NULL;

   if (NULL == platCfgArray)
   {
      if (SPIDEVICEOSSVC_RESULT_OK != SpiDeviceOsSvc_Malloc((void **)&platCfgArray,
                                                            MAX_NUM_QUP_HW_CORES*sizeof(SpiDevicePlatCfg_DevTargetCfgType *)))
      {
         return NULL;
      }
   }

   do
   {
      if (DAL_SUCCESS != DALSYS_GetDALPropertyHandle(uOsDevId, hProp))
      {
         break;
      }

      if (DAL_SUCCESS == DALSYS_GetPropertyValue
          (hProp, //Prop handle
           "QUP_CORE_NUM",     //Prop Name String
           0,                  //Not interested in Prop Id
           &PropVar))          //Output
      {
         uCoreNum = PropVar.Val.dwVal;
      }
      else
      {
         break;
      }

      if (NULL != (*(platCfgArray))[uCoreNum - 1])
      {
         tgtCfg = (*(platCfgArray))[uCoreNum - 1];
         break;
      }

      if (SPIDEVICEOSSVC_RESULT_OK != SpiDeviceOsSvc_Malloc((void **)&tgtCfg,
                                                            sizeof(SpiDevicePlatCfg_DevTargetCfgType)))
      {
         tgtCfg = NULL;
         break;
      }

      tgtCfg->uQupCoreNum = uCoreNum;
      tgtCfg->uOsDevId = uOsDevId;
      tgtCfg->qup_prot.qupId = (tzbsp_spi_device_id_t) (uCoreNum - 1);

      if (DAL_SUCCESS == DALSYS_GetPropertyValue
          (hProp, //Prop handle
           "QUP_BASE_ADDR",     //Prop Name String
           0,                  //Not interested in Prop Id
           &PropVar))          //Output
      {
         tgtCfg->qupVirtBlockAddr = PropVar.Val.dwVal;
         tgtCfg->qupPhysBlockAddr = PropVar.Val.dwVal;
      }
      else
      {
         SpiDeviceOsSvc_Free((void *)tgtCfg);
         tgtCfg = NULL;
         break;
      }

      if (DAL_SUCCESS == DALSYS_GetPropertyValue
          (hProp, //Prop handle
           "SW_ENABLE_BAM",     //Prop Name String
           0,                  //Not interested in Prop Id
           &PropVar))          //Output
      {
         tgtCfg->bBamSupported = PropVar.Val.dwVal;
      }
      else
      {
         SpiDeviceOsSvc_Free((void *)tgtCfg);
         tgtCfg = NULL;
         break;
      }

      if (DAL_SUCCESS == DALSYS_GetPropertyValue
          (hProp, //Prop handle
           "BAM_BLSP_ID",     //Prop Name String
           0,                  //Not interested in Prop Id
           &PropVar))          //Output
      {
         tgtCfg->spiBamCfg.uBamBlspId = PropVar.Val.dwVal;
         tgtCfg->qup_prot.blspId = (tzbsp_spi_blsp_t)(PropVar.Val.dwVal - 1);
      }
      else
      {
         SpiDeviceOsSvc_Free((void *)tgtCfg);
         tgtCfg = NULL;
         break;
      }

      if (DAL_SUCCESS == DALSYS_GetPropertyValue
          (hProp, //Prop handle
           "BAM_BASE_ADDR",     //Prop Name String
           0,                  //Not interested in Prop Id
           &PropVar))          //Output
      {
         tgtCfg->spiBamCfg.uBamBaseAddr = PropVar.Val.dwVal;
      }
      else
      {
         SpiDeviceOsSvc_Free((void *)tgtCfg);
         tgtCfg = NULL;
         break;
      }

      if (DAL_SUCCESS == DALSYS_GetPropertyValue
          (hProp, //Prop handle
           "BAM_INTERRUPT_ID",     //Prop Name String
           0,                  //Not interested in Prop Id
           &PropVar))          //Output
      {
         tgtCfg->spiBamCfg.uBamInterruptId = PropVar.Val.dwVal;
      }
      else
      {
         SpiDeviceOsSvc_Free((void *)tgtCfg);
         tgtCfg = NULL;
         break;
      }

      if (DAL_SUCCESS == DALSYS_GetPropertyValue
          (hProp, //Prop handle
           "BAM_TX_PIPE_NUM",     //Prop Name String
           0,                  //Not interested in Prop Id
           &PropVar))          //Output
      {
         tgtCfg->spiBamCfg.uBamTxPipeNum = PropVar.Val.dwVal;
         tgtCfg->qup_prot.uBamTxPipeNum  = PropVar.Val.dwVal;
      }
      else
      {
         SpiDeviceOsSvc_Free((void *)tgtCfg);
         tgtCfg = NULL;
         break;
      }

      if (DAL_SUCCESS == DALSYS_GetPropertyValue
          (hProp, //Prop handle
           "BAM_RX_PIPE_NUM",     //Prop Name String
           0,                  //Not interested in Prop Id
           &PropVar))          //Output
      {
         tgtCfg->spiBamCfg.uBamRxPipeNum = PropVar.Val.dwVal;
         tgtCfg->qup_prot.uBamRxPipeNum  = PropVar.Val.dwVal;
      }
      else
      {
         SpiDeviceOsSvc_Free((void *)tgtCfg);
         tgtCfg = NULL;
         break;
      }
      if(DAL_SUCCESS == DALSYS_GetPropertyValue
            (hProp, //Prop handle
             "GPIO_NUM_ARR",
             0,
             &PropVar))
      {
         tgtCfg->qup_prot.puGpioArr = PropVar.Val.pdwVal;
         tgtCfg->qup_prot.uNumGpios = PropVar.dwLen;
      }
      else
      {
         tgtCfg->qup_prot.puGpioArr = NULL;
         tgtCfg->qup_prot.uNumGpios = 0;   
      }

      (*(platCfgArray))[uCoreNum - 1] = tgtCfg;
   }
   while (0);

   return tgtCfg;
}

/*
static int32 SpiDevicePlatCfg_ConfigSpiGpio (char *gpio_str)
{
   TLMMGpioIdKeyType nGpioId;
   TLMMGpioConfigIdType UserSettings;
   int32 res = SPIDEVICE_PLATCFG_RESULT_ERROR_CONFIGURING_GPIOS;

   UserSettings.eDirection = TLMM_GPIO_INPUT; //don't care for function GPIOs
   UserSettings.ePull = TLMM_GPIO_PULL_DOWN;  
   UserSettings.eDriveStrength = TLMM_GPIO_8MA; 

   if (DAL_SUCCESS == Tlmm_GetGpioId(gpio_str, &nGpioId))
   {
      if (DAL_SUCCESS == Tlmm_ConfigGpioId(nGpioId, &UserSettings))
      {
         res = SPIDEVICE_RES_SUCCESS;
      }
   }
   return res;
}

static int32 SpiDevicePlatCfg_InitSpiGpios (uint32 uOsDevId)
{
   DALSYS_PROPERTY_HANDLE_DECLARE(hProp);
   DALSYSPropertyVar PropVar;
   int32 res = SPIDEVICE_RES_SUCCESS;
   
   do
   {
      if (DAL_SUCCESS != DALSYS_GetDALPropertyHandle(uOsDevId, hProp))
      {
         res = SPIDEVICE_PLATCFG_RESULT_ERROR_DAL_GET_PROPERTY_HANDLE;
		 break;
      }
   
      //Read CLK GPIO
      if (DAL_SUCCESS != DALSYS_GetPropertyValue (hProp, "gpio_spi_clk_str", 0, &PropVar))
      {
         res = SPIDEVICE_PLATCFG_RESULT_ERROR_DAL_GET_PROPERTY_VALUE;
         break;
      }
      res = SpiDevicePlatCfg_ConfigSpiGpio (PropVar.Val.pszVal);
      if (res != SPIDEVICE_RES_SUCCESS)
      {
         break;
      }
   
      //Read MOSI GPIO
      if (DAL_SUCCESS != DALSYS_GetPropertyValue (hProp, "gpio_spi_mosi_str", 0, &PropVar))
      {
         res = SPIDEVICE_PLATCFG_RESULT_ERROR_DAL_GET_PROPERTY_VALUE;
		 break;
      }
      res = SpiDevicePlatCfg_ConfigSpiGpio (PropVar.Val.pszVal);
      if (res != SPIDEVICE_RES_SUCCESS)
      {
         break;
      }

      //Read MISO GPIO
      if (DAL_SUCCESS != DALSYS_GetPropertyValue (hProp, "gpio_spi_miso_str", 0, &PropVar))
      {
         res = SPIDEVICE_PLATCFG_RESULT_ERROR_DAL_GET_PROPERTY_VALUE;
		 break;
      }
      res = SpiDevicePlatCfg_ConfigSpiGpio (PropVar.Val.pszVal);
      if (res != SPIDEVICE_RES_SUCCESS)
      {
         break;
      }

      //Read CS GPIO
      if (DAL_SUCCESS == DALSYS_GetPropertyValue (hProp, "gpio_spi_cs_str", 0, &PropVar))
      {
         res = SpiDevicePlatCfg_ConfigSpiGpio (PropVar.Val.pszVal);
         if (res != SPIDEVICE_RES_SUCCESS)
         {
            break;
         }
      }
      //else nothing, its OK to not have CS GPIO configured, e.g. for 3-wire SPI.
   }
   while(0);
   return res;
}*/

int32 SpiDevicePlatCfg_InitTarget(uint32 uOsDevId, SPIDEVICE_PLATCFG_HANDLE *phPlat)
{
   int32 res = -1;
   SpiDevicePlatCfg_DevTargetCfgType *tgtCfg = NULL;
   uint32 devId = (uint32)DalSpi_DeviceId[uOsDevId - 1];

   if (NULL !=
          (tgtCfg = (SPIDEVICE_PLATCFG_HANDLE)
           SpiDevicePlatCfg_GetTargetConfig(devId)))
   {
      *phPlat = (SPIDEVICE_PLATCFG_HANDLE)tgtCfg;

	  /*if (SPIDEVICE_RES_SUCCESS != SpiDevicePlatCfg_InitSpiGpios(devId))
	  {
	     return res;
	  }*/
      if (NULL == tgtCfg->spiBam)
      {
         res = SpiDeviceBamInit(&tgtCfg->spiBam,
                                (SPIDEVICE_PLATCFG_HANDLE)tgtCfg);
      }
      else
      {
         res = SPIDEVICE_RES_SUCCESS;
      }
   }

   return res;
}

int32 SpiDevicePlatCfg_DeInitTarget(SPIDEVICE_PLATCFG_HANDLE hPlat)
{
   /* SpiDevicePlatCfg_InitTarget, just does data structure allocation.
    * The allocated data structures will be available for the life-time 
    * of the driver. There is no point in allocating and de-allocating 
    * them.
    */
   int32 res = SPIDEVICE_RES_ERROR_CLS_DEV_PLATFORM ;
   SpiDevicePlatCfg_DevTargetCfgType *tgtCfg = (SpiDevicePlatCfg_DevTargetCfgType *)hPlat;
   if (tgtCfg->spiBam)
   {
	  res = SpiDeviceBamDeInit(&tgtCfg->spiBam);
   }
   else
   {
	  res = SPIDEVICE_RES_SUCCESS;
   }
   return res;
}


int32 SpiDevicePlatCfg_GetPlatformInfo
(
   SPIDEVICE_PLATCFG_HANDLE hPlat,
   SpiDevicePlatInfoType *platInfo
)
{
   SpiDevicePlatCfg_DevTargetCfgType *pDev;

   pDev = (SpiDevicePlatCfg_DevTargetCfgType *)hPlat;

   //*platInfo = pDev->devProps;
   platInfo->uQupCoreNum = pDev->uQupCoreNum;
   platInfo->qupVirtBlockAddr = pDev->qupVirtBlockAddr;
   platInfo->qupPhysBlockAddr = pDev->qupPhysBlockAddr;
   platInfo->tcsrVirtBlockAddr = pDev->tcsrVirtBlockAddr;
   platInfo->bInterruptBased = pDev->bInterruptBased;
   platInfo->bBamSupported = pDev->bBamSupported;
   platInfo->spiBam = pDev->spiBam;
   platInfo->bTcsrInterruptRequired = pDev->bTcsrInterruptRequired;
   platInfo->uTcsrInterruptBitMask = pDev->uTcsrInterruptBitMask;
   platInfo->uTcsrInterruptBitShift = pDev->uTcsrInterruptBitShift;

   return SPIDEVICE_RES_SUCCESS;
}

/** @brief Returns the physical address of the QUP device.

    @param[in] hPlat platform device handle.

    @return              int32.
  */
SpiDeviceBamCfg_t* SpiDevicePlatCfg_PlatInternal_GetBamCfg
(
   SPIDEVICE_PLATCFG_HANDLE hPlat
)
{
   SpiDevicePlatCfg_DevTargetCfgType *pDev;

   pDev = (SpiDevicePlatCfg_DevTargetCfgType *)hPlat;
   return&pDev->spiBamCfg;
}

/** @brief Sets the power state of the device.

    @param[in] hPlat platform device handle.

    @return          int32 .
  */
int32 SpiDevicePlatCfg_SetPowerState(SPIDEVICE_PLATCFG_HANDLE hPlat, uint32 state)
{
   SpiDevicePlatCfg_DevTargetCfgType *pDev;
   int32 res = SPIDEVICE_RES_SUCCESS;
   int32 xpu_res;
   
   
   pDev = (SpiDevicePlatCfg_DevTargetCfgType*)hPlat;
   
   switch (state)
   {
      case SPIDEVICE_POWER_STATE_2:
           pDev->qup_prot.enable =1;
           xpu_res = tzbsp_spi_configure_protection(&pDev->qup_prot);
           if (SPIDEVICE_RES_SUCCESS != xpu_res) {
              res = SPIDEVICE_PLATCFG_RESULT_ERROR_ENABLE_XPU_PROTECTION;        
           }
           break;
      case SPIDEVICE_POWER_STATE_1:
      case SPIDEVICE_POWER_STATE_0:
           //pDev->qup_prot.enable = 0;
           //xpu_res = tzbsp_xpu_disable_qup_protection(&pDev->qup_prot);
           //if (SPIDEVICE_RES_SUCCESS != xpu_res) {
           //   res = SPIDEVICE_PLATCFG_RESULT_ERROR_DISABLE_XPU_PROTECTION;        
           //}
           break;
      default:
           break;
   }

   return res;
}
