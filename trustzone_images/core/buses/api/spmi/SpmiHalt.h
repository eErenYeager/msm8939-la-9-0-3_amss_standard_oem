/**
 * @file: SpmiHalt.h
 * @brief: A standalone API for disabling the SPMI bus
 * 
 * Copyright (c) 2013 by Qualcomm Technologies Incorporated. All Rights Reserved.
 * 
 * $DateTime: 2018/02/07 00:37:16 $
 * $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/buses/api/spmi/SpmiHalt.h#1 $
 * $Change: 15409075 $
 * 
 *                              Edit History
 * Date     Description
 * -------  -------------------------------------------------------------------
 * 10/1/13  Initial Version
 */
#ifndef SPMIHALT_H
#define	SPMIHALT_H

#include "SpmiTypes.h"

/**
 * @brief Gracefully halts the SPMI Bus
 * 
 * This function disables the SPMI controller so no future bus
 * requests are processed.  If this function returns successfully,
 * the bus will have completed any in progress transaction and will be
 * disabled.
 * 
 * @return  SPMI_SUCCESS on success, error code on failure
*/
Spmi_Result SpmiHalt_DisableBus(void);

#endif
