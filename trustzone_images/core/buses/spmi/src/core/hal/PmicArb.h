/**
 * @file:  PmicArb.h
 * @brief: 
 * 
 * Copyright (c) 2013 by Qualcomm Technologies Incorporated. All Rights Reserved.
 * 
 * $DateTime: 2018/02/07 00:37:16 $
 * $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/buses/spmi/src/core/hal/PmicArb.h#1 $
 * $Change: 15409075 $ 
 * 
 *                              Edit History
 * Date     Description
 * -------  -------------------------------------------------------------------
 * 08/6/14  Log SPMI Geni cfg and HW vesrions in spmi init
 * 10/1/13  Initial Version
 */
#ifndef PMICARB_H
#define	PMICARB_H

#include "PmicArbConstants.h"
#include "SpmiTypes.h"
#include "SpmiUtils.h"
#include "SpmiHal.h"

typedef enum 
{
    PMIC_ARB_CMD_EXTENDED_REG_WRITE_LONG = 0,
    PMIC_ARB_CMD_EXTENDED_REG_READ_LONG = 1,
    PMIC_ARB_CMD_EXTENDED_REG_WRITE = 2,
    PMIC_ARB_CMD_RESET = 3,
    PMIC_ARB_CMD_SLEEP = 4,
    PMIC_ARB_CMD_SHUTDOWN = 5,
    PMIC_ARB_CMD_WAKEUP = 6,
    PMIC_ARB_CMD_AUTHENTICATE = 7,
    PMIC_ARB_CMD_MASTER_READ = 8,
    PMIC_ARB_CMD_MASTER_WRITE = 9,
    PMIC_ARB_CMD_TRANSFER_BUS_OWNERSHIP = 10,
    PMIC_ARB_CMD_DEVICE_DESC_BLOCK_MASTER = 11,
    PMIC_ARB_CMD_DEVICE_DESC_BLOCK_SLAVE = 12,
    PMIC_ARB_CMD_EXTENDED_REGISTER_READ = 13,
    PMIC_ARB_CMD_REGISTER_WRITE = 14,
    PMIC_ARB_CMD_REGISTER_READ = 15,
    PMIC_ARB_CMD_REGISTER_0_WRITE = 16,
    PMIC_ARB_CMD_MAX
} PmicArbCmd;

typedef struct 
{
    uint32 irqStatus;
    uint32 geniStatus;
    uint32 geniCtrl;
	uint32 SpmiCfg;
	uint32 hwVersion;
} PmicArbHwStatus;

typedef void (*PmicArbIsrCallback)(uint8 channel, uint8 mask);

Spmi_Result PmicArb_Init(PmicArbIsrCallback isrCb);
Spmi_Result PmicArb_ConfigHW(SpmiCfg_ConfigType cfgType);
Spmi_Result PmicArb_FindChannel(uint8 slaveId, uint8 periph, uint8* channel);
Spmi_Result PmicArb_SetInterruptEnabled(uint8 channel, boolean enabled);
Spmi_Result PmicArb_ExecuteCommand(PmicArbCmd cmd,
                                   Spmi_AccessPriority priority,
                                   uint8 slaveId,
                                   uint16 address,
                                   uint8* data,
                                   uint32 dataLen,
                                   uint32* bytesTransacted);

void PmicArb_GetHwStatus(PmicArbHwStatus* status);
boolean PmicArb_HasInterruptSupport(void);

#endif
