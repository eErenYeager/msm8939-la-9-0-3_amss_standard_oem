/**
 * @file:  PmicArbConstants.h
 * @brief: 
 * 
 * Copyright (c) 2013 by Qualcomm Technologies Incorporated. All Rights Reserved.
 * 
 * $DateTime: 2018/02/07 00:37:16 $
 * $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/buses/spmi/src/core/hal/bear/PmicArbConstants.h#1 $
 * $Change: 15409075 $
 * 
 *                              Edit History
 * Date     Description
 * -------  -------------------------------------------------------------------
 * 11/25/13 Initial Version
 */
#ifndef PMICARBCONSTANTS_H
#define	PMICARBCONSTANTS_H

#include "PmicArbCoreHwio.h"

#define PMIC_ARB_MAX_PERIPHERAL_SUPPORT (HWIO_PMIC_ARBq_CHNLn_CMD_MAXn + 1)

#endif

