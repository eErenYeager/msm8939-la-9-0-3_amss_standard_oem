/**
 * @file: SpmiOsLogs.h
 * 
 * @brief: This module implements logging functionality for the SPMI driver
 * 
 * Copyright (c) 2013 by Qualcomm Technologies Incorporated. All Rights Reserved.
 * 
 * $DateTime: 2018/02/07 00:37:16 $
 * $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/buses/spmi/src/platform/os/tz/SpmiOsLogs.h#1 $
 * $Change: 15409075 $ 
 * 
 *                              Edit History
 * Date     Description
 * -------  -------------------------------------------------------------------
 * 10/1/13  Initial Version
 */

#ifndef __SPMI_OS_LOGS_H_
#define __SPMI_OS_LOGS_H_

#include "tzbsp_log.h"

#define SPMI_OS_LOG_FATAL(msg, args...) \
    TZBSP_LOG(TZBSP_MSG_FATAL, msg, ##args)
    
#define SPMI_OS_LOG_ERROR(msg, args...) \
    TZBSP_LOG(TZBSP_MSG_ERROR, msg, ##args)
    
#define SPMI_OS_LOG_WARNING(msg, args...) \
    TZBSP_LOG(TZBSP_MSG_HIGH, msg, ##args)

#define SPMI_OS_LOG_INFO(msg, args...) \
    TZBSP_LOG(TZBSP_MSG_MED, msg, ##args)

#define SPMI_OS_LOG_VERBOSE(msg, args...) \
    TZBSP_LOG(TZBSP_MSG_LOW, msg, ##args)

#endif
