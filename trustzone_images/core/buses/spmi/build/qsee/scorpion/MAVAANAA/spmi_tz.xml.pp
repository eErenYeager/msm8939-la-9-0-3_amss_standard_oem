# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/buses/spmi/src/platform/config/bear/default/spmi_tz.xml"
# 1 "<built-in>" 1
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/buses/spmi/src/platform/config/bear/default/spmi_tz.xml" 2
<!--
 * @brief: Configuration file for the SPMI driver
 *
 * Copyright (c) 2013 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime:$
 * $Header:$
 * $Change:$
-->
<driver name="NULL">
    <device id="DALDEVICEID_SPMI_DEVICE">


# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/buses/spmi/src/platform/config/bear/default/spmi_sys.cfg" 1
<!-- System props -->
<props name="pmic_arb_base_addr" type=DALPROP_ATTR_TYPE_UINT32>
    0x02000000
</props>
# 14 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/buses/spmi/src/platform/config/bear/default/spmi_tz.xml" 2

        <!-- User configurable props -->
        <props name="owner" type=DALPROP_ATTR_TYPE_UINT32>
            1
        </props>

    </device>
</driver>
