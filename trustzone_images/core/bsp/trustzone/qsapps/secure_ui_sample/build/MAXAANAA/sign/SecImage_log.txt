Logging to /local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/bsp/trustzone/qsapps/secure_ui_sample/build/MAXAANAA/sign/SecImage_log.txt
Config path is set to: /local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/bsp/trustzone/qsapps/build/secimage.xml
WARNING: OEM ID is set to 0 for sign_id "secure_ui_sample"
Output dir is set to: /local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/bsp/trustzone/qsapps/secure_ui_sample/build/MAXAANAA/sign
------------------------------------------------------
Processing 1/1: /local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/ms/bin/MAXAANAA/unsigned/secure_ui_sample.mbn

Signing image: /local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/ms/bin/MAXAANAA/unsigned/secure_ui_sample.mbn
Generating new Root certificate and a random key
Generating new Attestation CA certificate and a random key
Generating new Attestation certificate and a random key

Attestation Certificate Properties:
| SW_ID     | 0x000000000000000C  |
| HW_ID     | 0x0000000000000000  |
| DEBUG     | 0x0000000000000002  |
| OEM_ID    | 0x0000              |
| SW_SIZE   | 168                 |
| MODEL_ID  | 0x0000              |
| SHA256    | True                |
| APP_ID    | 0x0000000000001110  |
| CRASH_DUMP| None                |
| ROT_EN    | None                |
| Exponent  | 3                   |
| TCG_MIN   | None                |
| TCG_MAX   | None                |
| FID_MIN   | None                |
| FID_MAX   | None                |

Signed image is stored at /local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/bsp/trustzone/qsapps/secure_ui_sample/build/MAXAANAA/sign/default/secure_ui_sample/secure_ui_sample.mbn

Base Properties: 
| Integrity Check             | True  |
| Signed                      | True  |
| Encrypted                   | False |
| Size of signature           | 256   |
| Size of one cert            | 2048  |
| Num of certs in cert chain  | 3     |
| Number of root certs        | 1     |
| Cert chain size             | 6144  |

ELF Properties: 
Elf Header: 
| Magic                      | ELF                           |
| Class                      | ELF32                          |
| Data                       | 2's complement, little endian  |
| Version                    | 1 (Current)                    |
| OS/ABI                     | No extensions or unspecified   |
| ABI Version                | 0                              |
| Type                       | EXEC (Executable file)         |
| Machine                    | Advanced RISC Machines ARM     |
| Version                    | 0x1                            |
| Entry address              | 0x00000000                     |
| Program headers offset     | 0x00000034                     |
| Section headers offset     | 0x00000000                     |
| Flags                      | 0x05000002                     |
| ELF header size            | 52                             |
| Program headers size       | 32                             |
| Number of program headers  | 2                              |
| Section headers size       | 40                             |
| Number of section headers  | 0                              |
| String table section index | 0                              |

Elf Program Headers: 
| S.No | Type | Offset | VirtAddr | PhysAddr | FileSize | MemSize |   Flags    | Align |
|------|------|--------|----------|----------|----------|---------|------------|-------|
|  1   | LOAD |0x03000 | 0x00000  | 0x00000  | 0x035c58 | 0x035c58| 0xa0000005 | 0x100 |
|  2   | LOAD |0x39fcc | 0x36000  | 0x36000  | 0x10e7a0 | 0x18a630| 0x30000006 | 0x1000|

Hash Segment Properties: 
| Header Size  | 40B  |

Header: 
| cert_chain_ptr  | 0x001c11a8  |
| cert_chain_size | 0x00001800  |
| code_size       | 0x00000080  |
| flash_parti_ver | 0x00000003  |
| image_dest_ptr  | 0x001c1028  |
| image_id        | 0x00000004  |
| image_size      | 0x00001980  |
| image_src       | 0x00000000  |
| sig_ptr         | 0x001c10a8  |
| sig_size        | 0x00000100  |

SecElf Properties: 
| image_type         | 0     |
| testsig_serialnum  | None  |

------------------------------------------------------

SUMMARY:
Following actions were performed: "sign"
Output is saved at: /local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/bsp/trustzone/qsapps/secure_ui_sample/build/MAXAANAA/sign

| Idx |      SignId      | Parse | Integrity | Sign | Encrypt |              Validate              |
|     |                  |       |           |      |         | Parse | Integrity | Sign | Encrypt |
|-----|------------------|-------|-----------|------|---------|-------|-----------|------|---------|
|  1. | secure_ui_sample |   T   |     NA    |  T   |    NA   |   NA  |     NA    |  NA  |    NA   |

