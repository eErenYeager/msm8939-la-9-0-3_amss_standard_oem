<?xml version="1.0" encoding="UTF-8"?>
<!--
===============================================================================

  Copyright (c) 2014 Qualcomm Technologies, Inc. All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.

===============================================================================
-->

<tns:secimage xmlns:tns="http://www.qualcomm.com/secimage"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.qualcomm.com/secimage ../xsd/secimage.xsd">

    <metadata>
        <chipset>default</chipset>
        <version>1.3</version>
    </metadata>
    <!--
        The default properties for all images. These properties maybe overriden
        per image in the 'images' section below as needed.

        max_cert_size: Maximum size (in bytes) of one certificate.
        key_size: Size (in bits) of the key used for signing.
        num_certs_in_certchain: Number of certificates in the certificate chain.
        num_root_certs: Number of root certificates in the certificate chain.
    -->
    <general_properties>
        <max_cert_size>2048</max_cert_size>
        <key_size>2048</key_size>
        <num_certs_in_certchain>3</num_certs_in_certchain>
        <num_root_certs>1</num_root_certs>
    </general_properties>

    <!-- Image configurations used by secimage to parse the image files. -->
    <parsegen>

        <!--
            Contains a list of 'image_type' blocks that associate an id to an
            image configuration. These blocks contain the information needed to
            correctly parse the corresponding file type.
        -->
        <image_types_list>

            <!--
                Contains the config data needed to correctly parse an image.
                This block contains configuration for a mbn file with a 80 byte
                header.

                id: ID for this particular block. This id should be unique.
            -->
            <image_type id='mbn_80b'>
                <!-- Type of file format -->
                <file_type>mbn</file_type>

                <!--
                    Configuration specific to the mbn format

                    header_size: Size of the mbn header (in bytes).
                -->
                <mbn_properties>
                    <header_size>80</header_size>
                </mbn_properties>
            </image_type>

            <!--
                This block contains configuration for a mbn file with a 80 byte
                header, 10k preamble, magic number and ota enabled.
            -->
            <image_type id='mbn_80b_pbl_ota'>
                <file_type>mbn_pmbl</file_type>
                <mbn_properties>
                    <header_size>80</header_size>
                </mbn_properties>
                <pmbl_properties>
                    <preamble_size>10</preamble_size>
                    <has_magic_num>true</has_magic_num>
                    <ota_enabled>false</ota_enabled>
                    <page_size>0</page_size>
                    <num_of_pages>0</num_of_pages>
                    <min_size_with_pad>256</min_size_with_pad>
                </pmbl_properties>
            </image_type>

            <!--
                This block contains configuration for a mbn file with a 40 byte
                header.
            -->
            <image_type id='mbn_40b'>
                <file_type>mbn</file_type>
                <mbn_properties>
                    <header_size>40</header_size>
                </mbn_properties>
            </image_type>

            <!--
                This block contains configuration for an elf file containing a
                hash table.
            -->
            <image_type id='elf_has_ht'>
                <!-- Type of file format -->
                <file_type>elf</file_type>

                <!--
                    Configuration specific to the elf format

                    has_hash_table: Flag whether the elf contains a hash table.
                    image_type: ID for the image type.
                    serial_num (optional): Serial number to be inserted in the
                        elf image. If the elf should not embed the serial
                        number, skip this value.
                -->
                <elf_properties>
                    <has_hash_table>true</has_hash_table>
                    <image_type>0</image_type>
                </elf_properties>
            </image_type>

            <!--
                This block contains configuration for an elf file containing a
                hash table.
            -->
            <image_type id='elf_as_bin'>
                <!-- Type of file format -->
                <file_type>elf_bin</file_type>

                <!--
                    Configuration specific to the elf format

                    has_hash_table: Flag whether the elf contains a hash table.
                    image_type: ID for the image type.
                    serial_num (optional): Serial number to be inserted in the
                        elf image. If the elf should not embed the serial
                        number, skip this value.
                -->
                <elf_properties>
                    <has_hash_table>true</has_hash_table>
                    <image_type>0</image_type>
                </elf_properties>
            </image_type>

            <!--
                This block contains configuration for an elf file not containing
                any hash table.
            -->
            <image_type id='elf_no_ht'>
                <file_type>elf</file_type>
                <elf_properties>
                    <has_hash_table>false</has_hash_table>
                    <image_type>0</image_type>
                </elf_properties>
            </image_type>

            <!--
                This block contains configuration for an elf file containing a
                hash table and a preamble.
            -->
            <image_type id='elf_preamble'>
                <!-- Type of file format -->
                <file_type>elf_pmbl</file_type>

                <elf_properties>
                    <has_hash_table>true</has_hash_table>
                    <image_type>0</image_type>
                </elf_properties>

                <pmbl_properties>
                    <preamble_size>10</preamble_size>
                    <has_magic_num>true</has_magic_num>
                    <ota_enabled>false</ota_enabled>
                    <page_size>0</page_size>
                    <num_of_pages>0</num_of_pages>
                    <min_size_with_pad>256</min_size_with_pad>
                </pmbl_properties>
            </image_type>

            <!--
                This block contains configuration for an elf wrapped mbn (ewm).
            -->
            <image_type id='elf_wrapped_mbn'>
                <!-- Type of file format -->
                <file_type>ewm</file_type>

                <!--
                Configuration specific to the ewm format

                image_entry: Entry for the image.
                relocatable: Allows PIL to relocate the segment in the heap instead
                    of loading the segment at the compiled-in physical address.
                header_size: Size of the mbn header (in bytes).
                -->
                <ewm_properties>
                    <image_entry>0x01200000</image_entry>
                    <relocatable>false</relocatable>
                </ewm_properties>

                <elf_properties>
                    <has_hash_table>false</has_hash_table>
                    <image_type>0</image_type>
                </elf_properties>

                <mbn_properties>
                    <header_size>80</header_size>
                </mbn_properties>
            </image_type>
        </image_types_list>
    </parsegen>

    <!-- Signing configurations used by secimage. -->
    <signing>
        <!--
            The selected signer for signing. Supported signer are:

            local: use local signer with test keys
            csms: generate tosign file for csms upload (first run)
                  package csms output zip to form signed image (second run)
            cass: use cass signer with production key
        -->
        <selected_signer>local</selected_signer>

        <!--
            The default signing attributes for all images. These properties
            maybe overriden per image in the 'images' section below as needed.

            msm_part: jtag_id for signing. (in hex)
            oem_id: oem_id for signing. (in hex)
            model_id: model_id for signing. (in hex)
            debug: debug_id for signing. (in hex)
            exponent: exponent value used in attestation key generation.
        -->
        <default_attributes>
            <msm_part>0x00000000</msm_part>
            <oem_id>0x0000</oem_id>
            <model_id>0x0000</model_id>
            <debug>0x0000000000000002</debug>
            <exponent>3</exponent>
        </default_attributes>

        <signer_attributes>

            <local_signer_attributes>

                <openssl_config_inputs>
                    <attestation_certificate_extensions_path>./../../../../../tools/build/scons/sectools/resources/openssl/v3_attest.ext</attestation_certificate_extensions_path>
                    <ca_certificate_extensions_path>./../../../../../tools/build/scons/sectools/resources/openssl/v3.ext</ca_certificate_extensions_path>
                    <openssl_configfile_path>./../../../../../tools/build/scons/sectools/resources/openssl/opensslroot.cfg</openssl_configfile_path>
                </openssl_config_inputs>

                <cert_configs_list>
                    <selected_cert_config>default</selected_cert_config>

                    <cert_config id='default'>
                        <root_cert>
                            <use_preexisting_cert>false</use_preexisting_cert>
                            <params_list>
                                <cert_param>C=US</cert_param>
                                <cert_param>ST=California</cert_param>
                                <cert_param>L=San Diego</cert_param>
                                <cert_param>OU=General Use Test Key (for testing only)</cert_param>
                                <cert_param>OU=CDMA Technologies</cert_param>
                                <cert_param>O=SecTools</cert_param>
                                <cert_param>CN=Generated Test Root CA</cert_param>
                            </params_list>
                        </root_cert>

                        <attest_ca_cert>
                            <use_preexisting_cert>false</use_preexisting_cert>
                            <params_list>
                                <cert_param>C   =   US        </cert_param>
                                <cert_param>ST  =   California</cert_param>
                                <cert_param>L   =   San Diego </cert_param>
                                <cert_param>O   =   SecTools  </cert_param>
                                <cert_param>CN  =   Generated Test Attestation CA</cert_param>
                            </params_list>
                        </attest_ca_cert>

                        <attest_cert>
                            <use_preexisting_cert>false</use_preexisting_cert>
                            <params_list>
                                <cert_param>C = US</cert_param>
                                <cert_param>ST= California</cert_param>
                                <cert_param>L = San Diego</cert_param>
                                <cert_param>O = SecTools</cert_param>
                                <cert_param>CN= SecTools Test User</cert_param>
                            </params_list>
                        </attest_cert>
                    </cert_config>
                </cert_configs_list>
            </local_signer_attributes>
        </signer_attributes>
    </signing>

    <encryption>
        <selected_encryptor>unified_encryption_2_0</selected_encryptor>
    </encryption>

    <data_provisioning>
        <base_path>./../../../../../tools/build/scons/sectools/resources/data_prov_assets/</base_path>
    </data_provisioning>

    <post_process>
        <pil_splitter>$(META_BUILD)/common/tools/misc/pil-splitter.py</pil_splitter>
    </post_process>

    <images_list>    
        <image sign_id="sampleap" name="sampleap.mbn" image_type="elf_has_ht">
            <general_properties_overrides></general_properties_overrides>
            <meta_build_location>dummy</meta_build_location>
            <signing_attributes_overrides>
                <sw_id>0x000000000000000C</sw_id>
                <app_id>0x0000000000000111</app_id>
            </signing_attributes_overrides>
        </image>
        <image sign_id="isdbtmm" name="isdbtmm.mbn" image_type="elf_has_ht">
            <general_properties_overrides></general_properties_overrides>
            <meta_build_location>dummy</meta_build_location>
            <signing_attributes_overrides>
                <sw_id>0x000000000000000C</sw_id>
                <app_id>0x0000000000000222</app_id>
            </signing_attributes_overrides>
        </image>
        <image sign_id="widevine" name="widevine.mbn" image_type="elf_has_ht">
            <general_properties_overrides></general_properties_overrides>
            <meta_build_location>dummy</meta_build_location>
            <signing_attributes_overrides>
                <sw_id>0x000000000000000C</sw_id>
                <app_id>0x0000000000000333</app_id>
            </signing_attributes_overrides>
        </image>
        <image sign_id="playread" name="playread.mbn" image_type="elf_has_ht">
            <general_properties_overrides></general_properties_overrides>
            <meta_build_location>dummy</meta_build_location>
            <signing_attributes_overrides>
                <sw_id>0x000000000000000C</sw_id>
                <app_id>0x0000000000000444</app_id>
            </signing_attributes_overrides>
        </image>
        <image sign_id="cmnlib" name="cmnlib.mbn" image_type="elf_has_ht">
            <general_properties_overrides></general_properties_overrides>
            <meta_build_location>dummy</meta_build_location>
            <signing_attributes_overrides>
                <sw_id>0x000000000000000C</sw_id>
                <app_id>0x0000000000000555</app_id>
            </signing_attributes_overrides>
        </image>
        <image sign_id="keymaster" name="keymaster.mbn" image_type="elf_has_ht">
            <general_properties_overrides></general_properties_overrides>
            <meta_build_location>dummy</meta_build_location>
            <signing_attributes_overrides>
                <sw_id>0x000000000000000C</sw_id>
                <app_id>0x0000000000000666</app_id>
            </signing_attributes_overrides>
        </image>
        <image sign_id="tztestexec" name="tztestexec.mbn" image_type="elf_has_ht">
            <general_properties_overrides></general_properties_overrides>
            <meta_build_location>dummy</meta_build_location>
            <signing_attributes_overrides>
                <sw_id>0x000000000000000C</sw_id>
                <app_id>0x0000000000000777</app_id>
            </signing_attributes_overrides>
        </image>
        <image sign_id="apttestapp" name="apttestapp.mbn" image_type="elf_has_ht">
            <general_properties_overrides></general_properties_overrides>
            <meta_build_location>dummy</meta_build_location>
            <signing_attributes_overrides>
                <sw_id>0x000000000000000C</sw_id>
                <app_id>0x0000000000000888</app_id>
            </signing_attributes_overrides>
        </image>
        <image sign_id="assurancetest" name="assurancetest.mbn" image_type="elf_has_ht">
            <general_properties_overrides></general_properties_overrides>
            <meta_build_location>dummy</meta_build_location>
            <signing_attributes_overrides>
                <sw_id>0x000000000000000C</sw_id>
                <app_id>0x0000000000000999</app_id>
            </signing_attributes_overrides>
        </image>    
        <image sign_id="dxhdcp2" name="dxhdcp2.mbn" image_type="elf_has_ht">
            <general_properties_overrides></general_properties_overrides>
            <meta_build_location>dummy</meta_build_location>
            <signing_attributes_overrides>
                <sw_id>0x000000000000000C</sw_id>
                <app_id>0x0000000000000AAA</app_id>
            </signing_attributes_overrides>
        </image>    
        <image sign_id="dxhdcp2dbg" name="dxhdcp2dbg.mbn" image_type="elf_has_ht">
            <general_properties_overrides></general_properties_overrides>
            <meta_build_location>dummy</meta_build_location>
            <signing_attributes_overrides>
                <sw_id>0x000000000000000C</sw_id>
                <app_id>0x0000000000000BBB</app_id>
            </signing_attributes_overrides>
        </image>    
        <image sign_id="hdcpsrm" name="hdcpsrm.mbn" image_type="elf_has_ht">
            <general_properties_overrides></general_properties_overrides>
            <meta_build_location>dummy</meta_build_location>
            <signing_attributes_overrides>
                <sw_id>0x000000000000000C</sw_id>
                <app_id>0x0000000000000CCC</app_id>
            </signing_attributes_overrides>
        </image>
        <image sign_id="tbase" name="tbase.mbn" image_type="elf_has_ht">
            <general_properties_overrides></general_properties_overrides>
            <meta_build_location>dummy</meta_build_location>
            <signing_attributes_overrides>
                <sw_id>0x000000000000000C</sw_id>
                <app_id>0x0000000000000DDD</app_id>
            </signing_attributes_overrides>
        </image>
        <image sign_id="qmpsecapp" name="qmpsecapp.mbn" image_type="elf_has_ht">
            <general_properties_overrides></general_properties_overrides>
            <meta_build_location>dummy</meta_build_location>
            <signing_attributes_overrides>
                <sw_id>0x000000000000000C</sw_id>
                <app_id>0x0000000000000EEE</app_id>
            </signing_attributes_overrides>
        </image>                          
        <image sign_id="securemm" name="securemm.mbn" image_type="elf_has_ht">
            <general_properties_overrides></general_properties_overrides>
            <meta_build_location>dummy</meta_build_location>
            <signing_attributes_overrides>
                <sw_id>0x000000000000000C</sw_id>
                <app_id>0x0000000000000FFF</app_id>
            </signing_attributes_overrides>
        </image>   
        <image sign_id="secure_ui_sample" name="secure_ui_sample.mbn" image_type="elf_has_ht">
            <general_properties_overrides></general_properties_overrides>
            <meta_build_location>dummy</meta_build_location>
            <signing_attributes_overrides>
                <sw_id>0x000000000000000C</sw_id>
                <app_id>0x0000000000001110</app_id>
            </signing_attributes_overrides>
        </image>   
        <image sign_id="securitytest" name="securitytest.mbn" image_type="elf_has_ht">
            <general_properties_overrides></general_properties_overrides>
            <meta_build_location>dummy</meta_build_location>
            <signing_attributes_overrides>
                <sw_id>0x000000000000000C</sw_id>
                <app_id>0x0000000000001221</app_id>
            </signing_attributes_overrides>
        </image>

        <image sign_id="tqs" name="tqs.mbn" image_type="elf_has_ht">
            <general_properties_overrides></general_properties_overrides>
            <meta_build_location>dummy</meta_build_location>
            <signing_attributes_overrides>
                <sw_id>0x000000000000000C</sw_id>
                <app_id>0x0000000000001234</app_id>
            </signing_attributes_overrides>
        </image>    		
        <image sign_id="ojtee" name="ojtee.mbn" image_type="elf_has_ht">
            <general_properties_overrides></general_properties_overrides>
            <meta_build_location>dummy</meta_build_location>
            <signing_attributes_overrides>
                <sw_id>0x000000000000000C</sw_id>
                <app_id>0x0000000000001554</app_id>
            </signing_attributes_overrides>
        </image>
        <image sign_id="pkcs11" name="pkcs11.mbn" image_type="elf_has_ht">
            <general_properties_overrides></general_properties_overrides>
            <meta_build_location>dummy</meta_build_location>
            <signing_attributes_overrides>
                <sw_id>0x000000000000000C</sw_id>
                <app_id>0x0000000000001665</app_id>
            </signing_attributes_overrides>
        </image>   
        <image sign_id="securepin" name="securepin.mbn" image_type="elf_has_ht">
            <general_properties_overrides></general_properties_overrides>
            <meta_build_location>dummy</meta_build_location>
            <signing_attributes_overrides>
                <sw_id>0x000000000000000C</sw_id>
                <app_id>0x0000000000001776</app_id>
            </signing_attributes_overrides>
        </image>
        <image sign_id="secureindicator" name="secureindicator.mbn" image_type="elf_has_ht">
            <general_properties_overrides></general_properties_overrides>
            <meta_build_location>dummy</meta_build_location>
            <signing_attributes_overrides>
                <sw_id>0x000000000000000C</sw_id>
                <app_id>0x0000000000001AA9</app_id>
            </signing_attributes_overrides>
        </image>
        <image sign_id="gptest" name="gptest.mbn" image_type="elf_has_ht">
            <general_properties_overrides></general_properties_overrides>
            <meta_build_location>dummy</meta_build_location>
            <signing_attributes_overrides>
                <sw_id>0x000000000000000C</sw_id>
                <app_id>0x0000000000001666</app_id>
            </signing_attributes_overrides>
        </image>
	
        <image sign_id="vfptest" name="vfptest.mbn" image_type="elf_has_ht" cert_config="default">
            <general_properties_overrides></general_properties_overrides>

            <post_process_commands></post_process_commands>

            <meta_build_location>dummy</meta_build_location>

            <signing_attributes_overrides>
                <sw_id>0x000000000000000C</sw_id>
                <app_id>0x00000000000019AC</app_id>

            </signing_attributes_overrides>
        </image>   

        <image sign_id="macchiato_sample" name="macchiato_sample.mbn" image_type="elf_has_ht">
            <general_properties_overrides></general_properties_overrides>
            <meta_build_location>dummy</meta_build_location>
            <signing_attributes_overrides>
                <sw_id>0x000000000000000C</sw_id>
                <app_id>0x0000000000001887</app_id>
            </signing_attributes_overrides>
        </image>
        
        <image sign_id="chamomile" name="chamomile.mbn" image_type="elf_has_ht">
            <general_properties_overrides></general_properties_overrides>
            <meta_build_location>dummy</meta_build_location>
            <signing_attributes_overrides>
                <sw_id>0x000000000000000C</sw_id>
                <app_id>0x0000000000003F44</app_id>
            </signing_attributes_overrides>
        </image>
        
		<image sign_id="fwlock" name="fwlock.mbn" image_type="elf_has_ht" cert_config="default">
            <general_properties_overrides></general_properties_overrides>

            <post_process_commands></post_process_commands>

            <meta_build_location>dummy</meta_build_location>

            <signing_attributes_overrides>
                <sw_id>0x000000000000000C</sw_id>
                <app_id>0x0000000000001555</app_id>
            </signing_attributes_overrides>
        </image>

        <image
             sign_id="sfsapp" 
             name="sfsapp.mbn" 
             image_type="elf_has_ht"  
             cert_config="default">
            
          <general_properties_overrides></general_properties_overrides>

          <post_process_commands></post_process_commands>

          <meta_build_location>dummy</meta_build_location>

          <signing_attributes_overrides>
            <sw_id>0x000000000000000C</sw_id>
            <app_id>0x0000000000009876</app_id>
          </signing_attributes_overrides>
        
        </image>  

        <image sign_id="fidocrypto" name="fidocrypto.mbn" image_type="elf_has_ht" cert_config="default">
          <general_properties_overrides></general_properties_overrides>
          <post_process_commands></post_process_commands>
          <meta_build_location>dummy</meta_build_location>
          <signing_attributes_overrides>
            <sw_id>0x000000000000000C</sw_id>
            <app_id>0x000000006669646F</app_id>
          </signing_attributes_overrides>
        </image>
    <image sign_id="sampleauth" name="sampleauth.mbn" image_type="elf_has_ht">
            <general_properties_overrides></general_properties_overrides>
            <meta_build_location>dummy</meta_build_location>
            <signing_attributes_overrides>
                <sw_id>0x000000000000000C</sw_id>
                <app_id>0x00000000709a0cf3</app_id>
            </signing_attributes_overrides>
    </image>
		
    </images_list>   

</tns:secimage>
