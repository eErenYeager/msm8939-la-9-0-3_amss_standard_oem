l1_file_name = /local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/tools/build/scons/sectools/resources/data_prov_assets/Encryption/Unified/default/l1_key.bin
l2_file_name = /local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/tools/build/scons/sectools/resources/data_prov_assets/Encryption/Unified/default/l2_key.bin
l3_file_name = /local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/tools/build/scons/sectools/resources/data_prov_assets/Encryption/Unified/default/l3_key.bin
Signing image: /local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/ms/bin/MAFAANAA/unsigned/chamomile.mbn
Generating new Root certificate and a random key
Generating new Attestation CA certificate and a random key
Generating new Attestation certificate and a random key

Attestation Certificate Properties:
| SW_ID     | 0x000000000000000C  |
| HW_ID     | 0x0000000000000000  |
| DEBUG     | 0x0000000000000002  |
| OEM_ID    | 0x0000              |
| SW_SIZE   | 168                 |
| MODEL_ID  | 0x0000              |
| SHA256    | True                |
| APP_ID    | 0x0000000000003F44  |
| CRASH_DUMP| None                |
| ROT_EN    | None                |
| Exponent  | 3                   |
| TCG_MIN   | None                |
| TCG_MAX   | None                |
| FID_MIN   | None                |
| FID_MAX   | None                |

Signed & Encrypted image is stored at /local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/bsp/trustzone/qsapps/chamomile/build/MAFAANAA/sign_and_encrypt/default/chamomile/chamomile.mbn
Image /local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/bsp/trustzone/qsapps/chamomile/build/MAFAANAA/sign_and_encrypt/default/chamomile/chamomile.mbn signature is valid
Image /local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/bsp/trustzone/qsapps/chamomile/build/MAFAANAA/sign_and_encrypt/default/chamomile/chamomile.mbn is encrypted

Base Properties: 
| Integrity Check             | True  |
| Signed                      | True  |
| Encrypted                   | True  |
| Size of signature           | 256   |
| Size of one cert            | 2048  |
| Num of certs in cert chain  | 3     |
| Number of root certs        | 1     |
| Cert chain size             | 6144  |

ELF Properties: 
Elf Header: 
| Magic                      | ELF                           |
| Class                      | ELF32                          |
| Data                       | 2's complement, little endian  |
| Version                    | 1 (Current)                    |
| OS/ABI                     | No extensions or unspecified   |
| ABI Version                | 0                              |
| Type                       | EXEC (Executable file)         |
| Machine                    | Advanced RISC Machines ARM     |
| Version                    | 0x1                            |
| Entry address              | 0x00000000                     |
| Program headers offset     | 0x00000034                     |
| Section headers offset     | 0x00000000                     |
| Flags                      | 0x05000002                     |
| ELF header size            | 52                             |
| Program headers size       | 32                             |
| Number of program headers  | 2                              |
| Section headers size       | 40                             |
| Number of section headers  | 0                              |
| String table section index | 0                              |

Elf Program Headers: 
| S.No | Type | Offset | VirtAddr | PhysAddr | FileSize | MemSize |   Flags    | Align |
|------|------|--------|----------|----------|----------|---------|------------|-------|
|  1   | LOAD | 0x3000 |  0x0000  |  0x0000  |  0x4448  | 0x04448 | 0xa0000005 | 0x100 |
|  2   | LOAD | 0x8fcc |  0x5000  |  0x5000  |  0x14a0  | 0x28a1c | 0x30000006 | 0x1000|

Hash Segment Properties: 
| Header Size  | 40B  |

Header: 
| cert_chain_ptr  | 0x0002e1a8  |
| cert_chain_size | 0x00001800  |
| code_size       | 0x00000080  |
| flash_parti_ver | 0x00000003  |
| image_dest_ptr  | 0x0002e028  |
| image_id        | 0x00000004  |
| image_size      | 0x00001980  |
| image_src       | 0x00000000  |
| sig_ptr         | 0x0002e0a8  |
| sig_size        | 0x00000100  |

SecElf Properties: 
| image_type         | 0     |
| testsig_serialnum  | None  |

