# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/monitor/msm8936/src/monitor.scl"
# 1 "<built-in>" 1
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/monitor/msm8936/src/monitor.scl" 2

MON_CODE_LR 0x86500000
{

  MON_RESET_ER +0x0 0x80
  {
    .ANY (CPU_RESET_ENTRY)
  }



  MON_ENTRY_ER +0x0 ALIGN 0x80
  {
    monitor.o (MON_COLD_ENTRY, +FIRST)
    .ANY (+RO-CODE)
    .ANY (+RO-DATA)
  }

  EL3_SP0_S_ER +0x0 ALIGN 0x800 0x80
  {
    monitor.o (EL3_SP0_S, +FIRST)
  }

  EL3_SP0_I_ER +0x8 0x80
  {
    monitor.o (EL3_SP0_I, +FIRST)
  }

  EL3_SP0_F_ER +0x8 0x80
  {
    monitor.o (EL3_SP0_F, +FIRST)
  }

  EL3_SP0_E_ER +0x8 0x80
  {
    monitor.o (EL3_SP0_E, +FIRST)
  }


  EL3_SP3_S_ER +0x8 0x80
  {
    monitor.o (EL3_SP3_S, +FIRST)
  }

  EL3_SP3_I_ER +0x8 0x80
  {
    monitor.o (EL3_SP3_I, +FIRST)
  }

  EL3_SP3_F_ER +0x8 0x80
  {
    monitor.o (EL3_SP3_F, +FIRST)
  }

  EL3_SP3_E_ER +0x8 0x80
  {
    monitor.o (EL3_SP3_E, +FIRST)
  }



  LEL_64_S_ER +0x8 0x80
  {
    monitor.o (LEL_64_S, +FIRST)
  }

  LEL_64_I_ER +0x8 0x80
  {
    monitor.o (LEL_64_I, +FIRST)
  }

  LEL_64_F_ER +0x8 0x80
  {
    monitor.o (LEL_64_F, +FIRST)
  }

  LEL_64_E_ER +0x8 0x80
  {
    monitor.o (LEL_64_E, +FIRST)
  }

  LEL_32_S_ER +0x8 0x80
  {
    monitor.o (LEL_32_S, +FIRST)
  }

  LEL_32_I_ER +0x8 0x80
  {
    monitor.o (LEL_32_I, +FIRST)
  }

  LEL_32_F_ER +0x8 0x80
  {
    monitor.o (LEL_32_F, +FIRST)
  }

  LEL_32_E_ER +0x8 0x80
  {
    monitor.o (LEL_32_E, +FIRST)
  }

  MON_CODE_ER_POST_VBAR +0x8 ALIGN 0x8
  {
    .ANY (+RO-CODE)
    .ANY (+RO-DATA)
  }
}

MON_DATA_LR +0x0 ALIGN 4096
{
  MON_DATA_ER +0x0
  {
    .ANY (+RW)
    .ANY (+ZI)
  }

  MON_L1_TT_MEM_ER +0x0
  {
    .ANY (MON_L1_TT_MEM)
  }

  MON_L2_TT_MEM_ER +0x0
  {
    .ANY (MON_L2_TT_MEM)
  }

  MON_L3_TT_MEM_ER +0x0
  {
    .ANY (MON_L3_TT_MEM)
  }

  QSEE_L1_TT_MEM_ER +0x0
  {
    .ANY (QSEE_L1_TT_MEM)
  }

  QSEE_L2_TT_MEM_ER +0x0
  {
    .ANY (QSEE_L2_TT_MEM)
  }

  QSEE_EXT_OS_DATA_ER +0x0
  {
    .ANY (QSEE_EXT_OS_DATA, +ZI)
  }

  MON_UNCACHED_DATA_ER +0x0 ALIGN 4096
  {
    .ANY (MONITOR_UNCACHED)
  }
}
