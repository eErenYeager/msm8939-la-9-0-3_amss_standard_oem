# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/kernel/hypervisor/build/hypervisor.scl"
# 1 "<built-in>" 1
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/kernel/hypervisor/build/hypervisor.scl" 2





# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/kernel/hypervisor/build/../inc/8929/hyp_target.h" 1
# 7 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/kernel/hypervisor/build/hypervisor.scl" 2


HYP_IMAGE 0x86400000

{
  HYP_CODE_1 +0
  {
    hyp_entry.o (HYPENTRYCODE, +FIRST)
    .ANY (+RO-CODE)
    .ANY (+RO-DATA)
  }
  EL2_SP0_S_ER +0x0 ALIGN 0x800 0x80
  {
    hyp_entry.o (EL2_SP0_S, +FIRST)
  }
  EL2_SP0_I_ER +0x8 0x80
  {
    hyp_entry.o (EL2_SP0_I, +FIRST)
  }
  EL2_SP0_F_ER +0x8 0x80
  {
    hyp_entry.o (EL2_SP0_F, +FIRST)
  }
  EL2_SP0_E_ER +0x8 0x80
  {
    hyp_entry.o (EL2_SP0_E, +FIRST)
  }
  EL2_SP2_S_ER +0x8 0x80
  {
    hyp_entry.o (EL2_SP2_S, +FIRST)
  }
  EL2_SP2_I_ER +0x8 0x80
  {
    hyp_entry.o (EL2_SP2_I, +FIRST)
  }
  EL2_SP2_F_ER +0x8 0x80
  {
    hyp_entry.o (EL2_SP2_F, +FIRST)
  }
  EL2_SP2_E_ER +0x8 0x80
  {
    hyp_entry.o (EL2_SP2_E, +FIRST)
  }
  LOW_EL_64_S_ER +0x8 0x80
  {
    hyp_entry.o (LOW_EL_64_S, +FIRST)
  }
  LOW_EL_64_I_ER +0x8 0x80
  {
    hyp_entry.o (LOW_EL_64_I, +FIRST)
  }
  LOW_EL_64_F_ER +0x8 0x80
  {
    hyp_entry.o (LOW_EL_64_F, +FIRST)
  }
  LOW_EL_64_E_ER +0x8 0x80
  {
    hyp_entry.o (LOW_EL_64_E, +FIRST)
  }
  LOW_EL_32_S_ER +0x8 0x80
  {
    hyp_entry.o (LOW_EL_32_S, +FIRST)
  }
  LOW_EL_32_I_ER +0x8 0x80
  {
    hyp_entry.o (LOW_EL_32_I, +FIRST)
  }
  LOW_EL_32_F_ER +0x8 0x80
  {
    hyp_entry.o (LOW_EL_32_F, +FIRST)
  }
  LOW_EL_32_E_ER +0x8 0x80
  {
    hyp_entry.o (LOW_EL_32_E, +FIRST)
  }

  ; non-code region needs to be mapped as RW for DEP and therefore
  ; this needs to be 4KB aligned
  HYP_RW +0 Align(0x1000)
  {
    * (+RW, +ZI)
  }


  ; the size below is 0 to prevent the loader from
  ; zero-initializing this region
  HYP_COPY (0x86400000 + 0x00016000) EMPTY 0
  {
  }
}
# 109 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/kernel/hypervisor/build/hypervisor.scl"
HYP_PT ((0x86400000 + 0x00016000) + 0x00016000)
{
  ; the size below is 0 to prevent the loader from
  ; zero-initializing this region
  PAGETABLE +0 EMPTY 0
  {
  }
}
