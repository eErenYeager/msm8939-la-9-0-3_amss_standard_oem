import os
import string
import re
import tempfile
Import('env')
env = env.Clone()

#------------------------------------------------------------------------------
# Check if we need to load this script or just bail-out
#------------------------------------------------------------------------------
# alias - First alias is always the target then the other possibles aliases
aliases = [
   'tz', 'all'
]

build_tags_list = ['CORE', 'APPS_PROC', 'IMAGE_TREE_VERSION_AUTO_GENERATE', 'IMAGE_TREE_UUID_AUTO_GENERATE']

env.InitImageVars(
   alias_list=aliases,           # aliases
   proc='scorpion',              # proc
   config='apps',                # config type, proc_name
   build_tags = build_tags_list,
   tools = ['${BUILD_ROOT}/core/bsp/build/scripts/elf64_builder.py',
			'${BUILD_ROOT}/core/bsp/build/scripts/mbn_builder.py',
			'${BUILD_ROOT}/tools/build/scons/sectools/sectools_builder.py']
)

if not env.CheckAlias(alias_list=aliases):
   Return()


#------------------------------------------------------------------------------
# Configure and load in uses and path variables,
#------------------------------------------------------------------------------
env.InitBuildConfig()

qsee_elf = '${COREBSP_ROOT}/bsp/qsee/build/${QC_SHORT_BUILDPATH}/qsee.elf'
mon_elf = '${COREBSP_ROOT}/bsp/monitor/build/${QC_SHORT_BUILDPATH}/mon.elf'

# The first arg is destination, the second is the QSEE ELF32 filename,
# the third is the ELF64 monitor filename. The order matters
fp = tempfile.NamedTemporaryFile()
comb_elf64 = env.ELF64Builder(fp.name, [qsee_elf, mon_elf])
fp.close()

env.Depends(comb_elf64, [qsee_elf, mon_elf])
env.Clean([qsee_elf, mon_elf], comb_elf64)

#----------------------------------------------------------------------------
# Generate MBN of combined image
#----------------------------------------------------------------------------
install_unsigned_root = env.SectoolGetUnsignedInstallPath(install_base_dir =env.subst('${MBN_ROOT}'))
env.Replace(MBN_FILE = os.path.join(install_unsigned_root, "tz"))
tz_mbn = env.MbnBuilder('${SHORT_BUILDPATH}/tz', comb_elf64,
   IMAGE_TYPE='tz', FLASH_TYPE='sdcc', ENABLE_ENCRYPT=True)

env.Depends(tz_mbn, comb_elf64)

#----------------------------------------------------------------------------
# Sectools signing
#----------------------------------------------------------------------------
sectools_signed_mbn = env.SectoolBuilder(
         target_base_dir = '${SHORT_BUILDPATH}', 
         source=tz_mbn, 
         sign_id="tz",
         msmid = env['MSM_ID'],
         sectools_install_base_dir = '${MBN_ROOT}',  
         install_file_name = "tz.mbn")  

#============================================================================
# Define units that will be built
#============================================================================
'''
tz_units = env.Alias ('arm11_tz_units', [
   comb_elf64,
   tz_mbn,
   sectools_signed_mbn,
])

# Add aliases
for alias in aliases:
   env.Alias(alias, tz_units)
'''
env.BindAliasesToTargets([comb_elf64, tz_mbn, 
   sectools_signed_mbn])