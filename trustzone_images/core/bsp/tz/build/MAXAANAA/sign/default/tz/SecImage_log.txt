Signing image: /local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/ms/bin/MAXAANAA/unsigned/tz.mbn
Generating new Root certificate and a random key
Generating new Attestation CA certificate and a random key
Generating new Attestation certificate and a random key

Attestation Certificate Properties:
| SW_ID     | 0x0000000000000007  |
| HW_ID     | 0x0000000000000000  |
| DEBUG     | 0x0000000000000002  |
| OEM_ID    | 0x0000              |
| SW_SIZE   | 424                 |
| MODEL_ID  | 0x0000              |
| SHA256    | True                |
| APP_ID    | None                |
| CRASH_DUMP| None                |
| ROT_EN    | None                |
| Exponent  | 3                   |
| TCG_MIN   | None                |
| TCG_MAX   | None                |
| FID_MIN   | None                |
| FID_MAX   | None                |

Signed image is stored at /local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/bsp/tz/build/MAXAANAA/sign/default/tz/tz.mbn

Base Properties: 
| Integrity Check             | True  |
| Signed                      | True  |
| Encrypted                   | False |
| Size of signature           | 256   |
| Size of one cert            | 2048  |
| Num of certs in cert chain  | 3     |
| Number of root certs        | 1     |
| Cert chain size             | 6144  |

ELF Properties: 
Elf Header: 
| Magic                      | ELF                           |
| Class                      | ELF64                          |
| Data                       | 2's complement, little endian  |
| Version                    | 1 (Current)                    |
| OS/ABI                     | No extensions or unspecified   |
| ABI Version                | 0                              |
| Type                       | EXEC (Executable file)         |
| Machine                    | Advanced RISC Machines ARM     |
| Version                    | 0x1                            |
| Entry address              | 0x86500000                     |
| Program headers offset     | 0x00000040                     |
| Section headers offset     | 0x00000000                     |
| Flags                      | 0x05000002                     |
| ELF header size            | 64                             |
| Program headers size       | 56                             |
| Number of program headers  | 10                             |
| Section headers size       | 64                             |
| Number of section headers  | 0                              |
| String table section index | 0                              |

Elf Program Headers: 
| S.No | Type | Offset | VirtAddr | PhysAddr | FileSize | MemSize |   Flags    | Align |
|------|------|--------|----------|----------|----------|---------|------------|-------|
|  1   | LOAD |0x03000 |0x86500000|0x86500000| 0x067c0  | 0x067c0 | 0x80000005 | 0x800 |
|  2   | LOAD |0x09800 |0x86508000|0x86508000| 0x15124  | 0x15124 | 0x00000006 | 0x4000|
|  3   | LOAD |0x22000 |0x86530000|0x86530000| 0x3d650  | 0x3d650 | 0x80000005 | 0x100 |
|  4   | LOAD |0x5f700 |0x8656e000|0x8656e000| 0x073b0  | 0x073b0 | 0x00000004 | 0x1000|
|  5   | LOAD |0x67000 |0x86576000|0x86576000| 0x065f0  | 0x13354 | 0x00000006 | 0x1000|
|  6   | LOAD |0x6e000 |0x8658f000|0x8658f000| 0x01000  | 0x01000 | 0x00000006 | 0x4   |
|  7   | LOAD |0x6f000 |0x86590000|0x86590000| 0x014d0  | 0x014d0 | 0x00000006 | 0x80  |
|  8   | LOAD |0x70500 |0x86592000|0x86592000| 0x0a000  | 0x0a000 | 0x00000006 | 0x1000|
|  9   | LOAD |0x7b000 |0x865b0000|0x865b0000| 0x0022c  | 0x0022c | 0x00000005 | 0x1000|
|  10  | LOAD |0x7c000 |0x865b1000|0x865b1000| 0x03438  | 0x03438 | 0x00000005 | 0x1000|

Hash Segment Properties: 
| Header Size  | 40B  |

Header: 
| cert_chain_ptr  | 0x865b52a8  |
| cert_chain_size | 0x00001800  |
| code_size       | 0x00000180  |
| flash_parti_ver | 0x00000003  |
| image_dest_ptr  | 0x865b5028  |
| image_id        | 0x00000019  |
| image_size      | 0x00001a80  |
| image_src       | 0x00000000  |
| sig_ptr         | 0x865b51a8  |
| sig_size        | 0x00000100  |

SecElf Properties: 
| image_type         | 0     |
| testsig_serialnum  | None  |

