#ifndef PM_REG_DUMP_H
#define PM_REG_DUMP_H

/*! \file
 *  \n
 *  \brief  pm_reg_dump.h
 *  \n
 *  \n This file contains definitions for PMIC register dump driver.
 *  \n
 *  \n &copy; Copyright 2014 Qualcomm Technologies Incorporated, All Rights Reserved
 */
/* ======================================================================= */

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/api/systemdrivers/pmic/pm_reg_dump.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
07/30/14   vtw     Created.
=============================================================================*/

#include "comdef.h"
#include "pm_lib_err.h"
#include "sysdbg_mem_dump.h"

/**
 * @brief This function captures the content of PMIC registers and stores
 *        to the host memory buffer.
 *
 * @details
 *
 * @param[in/out] a pointer to a host buffer contains PMIC register infomation from
 *         DDR rails(Cx,MX,Px1 & Px3) on success otherwise a NULL pointer.
 *
 * @return
 */
pm_err_flag_type pm_register_dump(dump_data_type** );

#endif /* PM_REG_DUMP_H */
