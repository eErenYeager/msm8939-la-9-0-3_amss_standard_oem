#ifndef TZBSP_HW_MUTEX_H
#define TZBSP_HW_MUTEX_H

/*===========================================================================
Copyright (c) 2012 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/api/boot/fuse_hw_mutex.h#1 $
$DateTime: 2018/02/07 00:37:16 $
$Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
12/05/12   rs      First version.
============================================================================*/

/** Set reserved HW Mutex ID for Security use **/
#define TZBSP_HW_MUTEX_ID               8

/** Set Processor ID to 1 to indicate reqest from Apps **/
#define TZBSP_HW_MUTEX_PID              1

/**
 * Grab Hardware Mutex for QFPROM Driver
 *
 */
void fuse_grab_mutex( void ) __attribute__((weak));

/**
 * Release Hardware Mutex for QFPROM Driver
 *
 */
void fuse_release_mutex( void ) __attribute__((weak));


#endif /* TZBSP_HW_MUTEX_H */
