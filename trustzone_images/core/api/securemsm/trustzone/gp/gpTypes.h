#ifndef GPTYPES_H
#define GPTYPES_H

/**
@file gpTypes.h
@brief Type definitions defined by GP standard.
*/

/*===========================================================================
   Copyright (c) 2013 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/api/securemsm/trustzone/gp/gpTypes.h#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $

when       who      what, where, why
--------   ---      ------------------------------------
10/15/13    dp      Initial version.

===========================================================================*/

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include <comdef.h>
#include <stdint.h>
#include "types.h"

/*----------------------------------------------------------------------------
 * Table of Contents
 * -------------------------------------------------------------------------*/
/*
 * 1. Basic type definitions
 *    a) Basic types
 * 2. TEE type definitions
 *    a) TEE Data types
 *        -TEE Core Framework data types (UUID, params, sessions, etc..)
 *        -TEE Trusted Storage types
 *        -TEE Crypto types
 *        -TEE Time types
 *        -TEE Arithmetical types
 * 3. TEE Constants
 *    a) TEE Core Framework constants
 *        -TEE Error codes
 *        -TEE Param types
 *        -TEE Login types
 *        -TEE Origin codes
 *        -TEE Property Set pseudo-handles
 *        -TEE Memory Access rights
 *        -TEE Handle values
 *    b) TEE Trusted Storage constants
 *        -TEE Object Storage constants
 *        -TEE Data Flag constants
 *        -TEE Usage constants
 *        -TEE Handle Flag constants
 *        -TEE Operation constants
 *        -TEE Miscellaneous constants
 *    c) TEE Crypto constants
 *        -TEE Crypto algorithms
 *        -TEE Transient Object types
 *        -TEE Object/Operation attributes
 *        -TEE attribute ID flags
 *
 */

/*----------------------------------------------------------------------------
 * Basic GP types
 * -------------------------------------------------------------------------*/
//typedef unsigned  char  uint8_t;
//typedef signed    char  int8_t;
//typedef uint16  uint16_t;
//typedef int16   int16_t;
//typedef uint32  uint32_t;
//typedef int32   int32_t;
//typedef uint64  uint64_t;
//typedef int64   int64_t;

//#define uint16_t uint16
//#define int16_t int16
//#define uint32_t uint32
//#define int32_t int32
//typedef unsigned short uint16_t;
//typedef signed short int16_t;
//typedef unsigned int uint32_t;
//typedef signed int int32_t;

/* For future revision to 64-bit cpu */
//typedef unsigned long uint64_t;
//typedef signed long int64_t;
//typedef unsigned long size_t;

/*----------------------------------------------------------------------------
 * Non-standard data types (custom to this TEE implementation only)
 * -------------------------------------------------------------------------*/

/* TEE phys addr types */

typedef struct TEE_physical_buf_entry_t {
  uint32_t phys_addr;
  uint32_t len;
} TEE_physical_buf_entry;

typedef struct TEE_physical_addr_t {
  TEE_physical_buf_entry fragmnt[512];
} TEE_physical_addr;


/*----------------------------------------------------------------------------
 * TEE data types
 * -------------------------------------------------------------------------*/

/* TEE core framework data types */

// Return type
typedef uint32_t TEE_Result;
typedef TEE_Result TEEC_Result;

// UUID
typedef struct
{
  uint32_t timeLow;
  uint16_t timeMid;
  uint16_t timeHiAndVersion;
  uint8_t  clockSeqAndNode[8];
} TEE_UUID;
typedef TEE_UUID TEEC_UUID;

// ID
typedef struct
{
  uint32_t login;
  TEE_UUID uuid;
} TEE_Identity;

// Params
typedef union
{
  struct
  {
    void* buffer;
    size_t size;
  } memref;
  struct
  {
    uint32_t a, b;
  } value;
} TEE_Param;

//Macros to retrieve Param Types
#define TEE_PARAM_TYPES(t0,t1,t2,t3) \
    ((t0) | ((t1) << 4) | ((t2) << 8) | ((t3) << 12))
#define TEE_PARAM_TYPE_GET(t, i) (((t) >> (i*4)) & 0xF)

// Sessions
struct __TEE_TASessionHandle;
typedef struct __TEE_TASessionHandle* TEE_TASessionHandle; /* Opaque handle */

#define TA_EXPORT //__declspec(dllexport)

// Property Set
struct __TEE_PropSetHandle;
typedef struct __TEE_PropSetHandle* TEE_PropSetHandle; /* Opaque handle */


/* TEE Trusted storage types */

// Attr
typedef struct {
  uint32_t attributeID;
  union
  {
    struct
    {
      void* buffer; /* [inbuf] */
      size_t length;
    } ref;
    struct
    {
      uint32_t a, b;
    } value;
  } content;
} TEE_Attribute;

// Obj info
typedef struct {
  uint32_t objectType;
  uint32_t objectSize;
  uint32_t maxObjectSize;
  uint32_t objectUsage;
  uint32_t dataSize;
  uint32_t dataPosition;
  uint32_t handleFlags;
  uint32_t mode;
} TEE_ObjectInfo;

// Whence (possible start offset when moving data streams)
typedef enum
{
  TEE_DATA_SEEK_SET = 0,
  TEE_DATA_SEEK_CUR,
  TEE_DATA_SEEK_END
} TEE_Whence;

// Obj
struct __TEE_ObjectHandle;
typedef struct __TEE_ObjectHandle* TEE_ObjectHandle; /* Opaque handle */

// Obj enum
struct __TEE_ObjectEnumHandle;
typedef struct __TEE_ObjectEnumHandle* TEE_ObjectEnumHandle; /* Opaque handle */


/* TEE Crypto types */

// Op mode
typedef enum {
  TEE_MODE_ENCRYPT,
  TEE_MODE_DECRYPT,
  TEE_MODE_SIGN,
  TEE_MODE_VERIFY,
  TEE_MODE_MAC,
  TEE_MODE_DIGEST,
  TEE_MODE_DERIVE
} TEE_OperationMode;

// Op info
typedef struct {
  uint32_t algorithm;
  uint32_t algorithm_class;
  uint32_t operationClass;
  uint32_t mode;
  uint32_t digestLength;
  uint32_t maxKeySize;
  uint32_t keySize;
  uint32_t requiredKeyUsage;
  uint32_t handleState;
  uint32_t xtsdusize;
} TEE_OperationInfo;

// Op
struct __TEE_OperationHandle;
typedef struct __TEE_OperationHandle* TEE_OperationHandle; /* Opaque handle */


/* TEE Time types */

typedef struct
{
  uint32_t seconds;
  uint32_t millis;
} TEE_Time;


/* TEE Arithmetical types */

typedef uint32_t TEE_BigInt; /* placeholder for large multi-precision int */
typedef uint32_t TEE_BigIntFMMContext; /* placeholder for context */
typedef uint32_t TEE_BigIntFMM; /* placeholder for mem structure of Fast modular mult */
#define TEE_BigIntSizeInU32(n) ((((n)+31)/32)+2)

/*----------------------------------------------------------------------------
 * TEE Core framework constants
 * -------------------------------------------------------------------------*/

/* TEE Error codes */

#define TEE_SUCCESS                           0x00000000    /* Operation success */
#define TEE_ERROR_GENERIC                     0xFFFF0000    /* Non-specific error */
#define TEE_ERROR_ACCESS_DENIED               0xFFFF0001    /* Access priveleges are not sufficient */
#define TEE_ERROR_CANCEL                      0xFFFF0002    /* Operation was cancelled */
#define TEE_ERROR_ACCESS_CONFLICT             0xFFFF0003    /* Concurrent accesses caused conflict */
#define TEE_ERROR_EXCESS_DATA                 0xFFFF0004    /* Too much data for the requested operation was provided */
#define TEE_ERROR_BAD_FORMAT                  0xFFFF0005    /* The input data was of invalid format */
#define TEE_ERROR_BAD_PARAMETERS              0xFFFF0006    /* The input parameters were invalid */
#define TEE_ERROR_BAD_STATE                   0xFFFF0007    /* The operation is not valid in current state */
#define TEE_ERROR_ITEM_NOT_FOUND              0xFFFF0008    /* The requested data could not be found */
#define TEE_ERROR_NOT_IMPLEMENTED             0xFFFF0009    /* The operation should be implemented, but does not currently exist */
#define TEE_ERROR_NOT_SUPPORTED               0xFFFF000A    /* The operation is valid, but not supported */
#define TEE_ERROR_NO_DATA                     0xFFFF000B    /* Expected data was missing */
#define TEE_ERROR_OUT_OF_MEMORY               0xFFFF000C    /* System ran out of resources */
#define TEE_ERROR_BUSY                        0xFFFF000D    /* System is busy with something else */
#define TEE_ERROR_COMMUNICATION               0xFFFF000E    /* Communication with a remote party failed */
#define TEE_ERROR_SECURITY                    0xFFFF000F    /* A security fault was detected */
#define TEE_ERROR_SHORT_BUFFER                0xFFFF0010    /* The buffer is NULL or is too small */
#define TEE_PENDING                           0xFFFF2000    /* The operation is pending action */
#define TEE_ERROR_TIMEOUT                     0xFFFF3001    /* The operation has timed out and could not complete */
#define TEE_ERROR_OVERFLOW                    0xFFFF300F    /* An integer overflow has occurred */
#define TEE_ERROR_TARGET_DEAD                 0xFFFF3024    /* TA has shut down */
#define TEE_ERROR_STORAGE_NO_SPACE            0xFFFF3041    /* Insufficient space available to create persistent object */
#define TEE_ERROR_MAC_INVALID                 0xFFFF3071    /* The computed MAC does not correspond with passed-in param */
#define TEE_ERROR_SIGNATURE_INVALID           0xFFFF3072    /* The signature is invalid */
#define TEE_ERROR_TIME_NOT_SET                0xFFFF5000    /* Persistent object is not currently set */
#define TEE_ERROR_TIME_NEEDS_RESET            0xFFFF5001    /* Persistent object is set, but may be corrupted and can no longer be trusted */
#define TEEC_SUCCESS                          TEE_SUCCESS
#define TEEC_ERROR_GENERIC                    TEE_ERROR_GENERIC
#define TEEC_ERROR_ACCESS_DENIED              TEE_ERROR_ACCESS_DENIED
#define TEEC_ERROR_CANCEL                     TEE_ERROR_CANCEL
#define TEEC_ERROR_ACCESS_CONFLICT            TEE_ERROR_ACCESS_CONFLICT
#define TEEC_ERROR_EXCESS_DATA                TEE_ERROR_EXCESS_DATA
#define TEEC_ERROR_BAD_FORMAT                 TEE_ERROR_BAD_FORMAT
#define TEEC_ERROR_BAD_PARAMETERS             TEE_ERROR_BAD_PARAMETERS
#define TEEC_ERROR_BAD_STATE                  TEE_ERROR_BAD_STATE
#define TEEC_ERROR_ITEM_NOT_FOUND             TEE_ERROR_ITEM_NOT_FOUND
#define TEEC_ERROR_NOT_IMPLEMENTED            TEE_ERROR_NOT_IMPLEMENTED
#define TEEC_ERROR_NOT_SUPPORTED              TEE_ERROR_NOT_SUPPORTED
#define TEEC_ERROR_NO_DATA                    TEE_ERROR_NO_DATA
#define TEEC_ERROR_OUT_OF_MEMORY              TEE_ERROR_OUT_OF_MEMORY
#define TEEC_ERROR_BUSY                       TEE_ERROR_BUSY
#define TEEC_ERROR_COMMUNICATION              TEE_ERROR_COMMUNICATION
#define TEEC_ERROR_SECURITY                   TEE_ERROR_SECURITY
#define TEEC_ERROR_SHORT_BUFFER               TEE_ERROR_SHORT_BUFFER
#define TEEC_ERROR_TARGET_DEAD                TEE_ERROR_TARGET_DEAD

/* Implementation defined errors */
#define TEE_ERROR_MAX_SESSIONS_EXCEEDED       0x0000FFF1

/* TEE Param types */

#define TEE_PARAM_TYPE_NONE                   0
#define TEE_PARAM_TYPE_VALUE_INPUT            1
#define TEE_PARAM_TYPE_VALUE_OUTPUT           2
#define TEE_PARAM_TYPE_VALUE_INOUT            3
#define TEE_PARAM_TYPE_MEMREF_INPUT           8       /* Changed to match TEEC values */
#define TEE_PARAM_TYPE_MEMREF_OUTPUT          9       /* Changed to match TEEC values */
#define TEE_PARAM_TYPE_MEMREF_INOUT           10      /* Changed to match TEEC values */

/* TEE Login types */

#define TEE_LOGIN_PUBLIC                      0x00000000
#define TEE_LOGIN_USER                        0x00000001
#define TEE_LOGIN_GROUP                       0x00000002
#define TEE_LOGIN_APPLICATION                 0x00000004
#define TEE_LOGIN_APPLICATION_USER            0x00000005
#define TEE_LOGIN_APPLICATION_GROUP           0x00000006
#define TEE_LOGIN_TRUSTED_APP                 0xF0000000


/* TEE Origin codes */

#define TEE_ORIGIN_API                        0x00000001
#define TEE_ORIGIN_COMMS                      0x00000002
#define TEE_ORIGIN_TEE                        0x00000003
#define TEE_ORIGIN_TRUSTED_APP                0x00000004

/* TEE Property Set pseudo-handles */

#define TEE_PROPSET_CURRENT_TA                (TEE_PropSetHandle)0xFFFFFFFF
#define TEE_PROPSET_CURRENT_CLIENT            (TEE_PropSetHandle)0xFFFFFFFE
#define TEE_PROPSET_TEE_IMPLEMENTATION        (TEE_PropSetHandle)0xFFFFFFFD

/* TEE Memory Access rights */

#define TEE_ACCESS_READ                       0x00000001
#define TEE_ACCESS_WRITE                      0x00000002
#define TEE_ACCESS_ANY_OWNER                  0x00000004

/* TEE Handle values */

#define TEE_HANDLE_NULL                       (NULL)

/*----------------------------------------------------------------------------
 * TEE Trusted storage constants
 * -------------------------------------------------------------------------*/

/* Object storage constants */

#define TEE_OBJECT_STORAGE_PRIVATE            0x00000001

/* Data Flag constants */

#define TEE_DATA_FLAG_ACCESS_READ             0x00000001
#define TEE_DATA_FLAG_ACCESS_WRITE            0x00000002
#define TEE_DATA_FLAG_ACCESS_WRITE_META       0x00000004
#define TEE_DATA_FLAG_SHARE_READ              0x00000010
#define TEE_DATA_FLAG_SHARE_WRITE             0x00000020
#define TEE_DATA_FLAG_CREATE                  0x00000200
#define TEE_DATA_FLAG_EXCLUSIVE               0x00000400

/* Usage constants */

#define TEE_USAGE_EXTRACTABLE                 0x00000001
#define TEE_USAGE_ENCRYPT                     0x00000002
#define TEE_USAGE_DECRYPT                     0x00000004
#define TEE_USAGE_MAC                         0x00000008
#define TEE_USAGE_SIGN                        0x00000010
#define TEE_USAGE_VERIFY                      0x00000020
#define TEE_USAGE_DERIVE                      0x00000040

/* Handle Flag constants */

#define TEE_HANDLE_FLAG_UNINITIALIZED         0x00000000
#define TEE_HANDLE_FLAG_PERSISTENT            0x00010000
#define TEE_HANDLE_FLAG_INITIALIZED           0x00020000
#define TEE_HANDLE_FLAG_KEY_SET               0x00040000
#define TEE_HANDLE_FLAG_EXPECT_TWO_KEYS       0x00080000

/* Operation constants */

#define TEE_OPERATION_CIPHER                  1
#define TEE_OPERATION_MAC                     3
#define TEE_OPERATION_AE                      4
#define TEE_OPERATION_DIGEST                  5
#define TEE_OPERATION_ASYMMETRIC_CIPHER       6
#define TEE_OPERATION_ASYMMETRIC_SIGNATURE    7
#define TEE_OPERATION_KEY_DERIVATION          8

/* Miscellaneous constants */

#define TEE_DATA_MAX_POSITION                 0xFFFFFFFF
#define TEE_OBJECT_ID_MAX_LEN                 64


/*----------------------------------------------------------------------------
 * TEE Crypto constants
 * -------------------------------------------------------------------------*/

/* TEE Crypto algorithms */

#define TEE_ALG_AES_ECB_NOPAD                 0x10000010
#define TEE_ALG_AES_CBC_NOPAD                 0x10000110
#define TEE_ALG_AES_CTR                       0x10000210
#define TEE_ALG_AES_CTS                       0x10000310
#define TEE_ALG_AES_XTS                       0x10000410
#define TEE_ALG_AES_CBC_MAC_NOPAD             0x30000110
#define TEE_ALG_AES_CBC_MAC_PKCS5             0x30000510
#define TEE_ALG_AES_CMAC                      0x30000610
#define TEE_ALG_AES_CCM                       0x40000710
#define TEE_ALG_AES_GCM                       0x40000810
#define TEE_ALG_AES_GCM_SW                    0x40000910
#define TEE_ALG_AES_CCM_SW                    0x40000A10
#define TEE_ALG_DES_ECB_NOPAD                 0x10000011
#define TEE_ALG_DES_CBC_NOPAD                 0x10000111
#define TEE_ALG_DES_CBC_MAC_NOPAD             0x30000111
#define TEE_ALG_DES_CBC_MAC_PKCS5             0x30000511
#define TEE_ALG_DES3_ECB_NOPAD                0x10000013
#define TEE_ALG_DES3_CBC_NOPAD                0x10000113
#define TEE_ALG_DES3_CBC_MAC_NOPAD            0x30000113
#define TEE_ALG_DES3_CBC_MAC_PKCS5            0x30000513
#define TEE_ALG_RSASSA_PKCS1_V1_5_MD5         0x70001830
#define TEE_ALG_RSASSA_PKCS1_V1_5_SHA1        0x70002830
#define TEE_ALG_RSASSA_PKCS1_V1_5_SHA224      0x70003830
#define TEE_ALG_RSASSA_PKCS1_V1_5_SHA256      0x70004830
#define TEE_ALG_RSASSA_PKCS1_V1_5_SHA384      0x70005830
#define TEE_ALG_RSASSA_PKCS1_V1_5_SHA512      0x70006830
#define TEE_ALG_RSASSA_PKCS1_PSS_MGF1_SHA1    0x70212930
#define TEE_ALG_RSASSA_PKCS1_PSS_MGF1_SHA224  0x70313930
#define TEE_ALG_RSASSA_PKCS1_PSS_MGF1_SHA256  0x70414930
#define TEE_ALG_RSASSA_PKCS1_PSS_MGF1_SHA384  0x70515930
#define TEE_ALG_RSASSA_PKCS1_PSS_MGF1_SHA512  0x70616930
#define TEE_ALG_RSAES_PKCS1_V1_5              0x60000130
#define TEE_ALG_RSAES_PKCS1_OAEP_MGF1_SHA1    0x60210230
#define TEE_ALG_RSAES_PKCS1_OAEP_MGF1_SHA224  0x60310230
#define TEE_ALG_RSAES_PKCS1_OAEP_MGF1_SHA256  0x60410230
#define TEE_ALG_RSAES_PKCS1_OAEP_MGF1_SHA384  0x60510230
#define TEE_ALG_RSAES_PKCS1_OAEP_MGF1_SHA512  0x60610230
#define TEE_ALG_RSA_NOPAD                     0x60000030
#define TEE_ALG_DSA_SHA1                      0x70002131
#define TEE_ALG_DH_DERIVE_SHARED_SECRET       0x80000032
#define TEE_ALG_MD5                           0x50000001
#define TEE_ALG_SHA1                          0x50000002
#define TEE_ALG_SHA224                        0x50000003
#define TEE_ALG_SHA256                        0x50000004
#define TEE_ALG_SHA384                        0x50000005
#define TEE_ALG_SHA512                        0x50000006
#define TEE_ALG_HMAC_MD5                      0x30000001
#define TEE_ALG_HMAC_SHA1                     0x30000002
#define TEE_ALG_HMAC_SHA224                   0x30000003
#define TEE_ALG_HMAC_SHA256                   0x30000004
#define TEE_ALG_HMAC_SHA384                   0x30000005
#define TEE_ALG_HMAC_SHA512                   0x30000006

/* TEE Transient Object types */

#define TEE_TYPE_AES                          0xA0000010  /* 128, 192, or 256 bits */
#define TEE_TYPE_DES                          0xA0000011  /* Always 56 bits */
#define TEE_TYPE_DES3                         0xA0000013  /* 112 or 168 bits */
#define TEE_TYPE_HMAC_MD5                     0xA0000001  /* Between 64 and 512 bits, multiple of 8 bits */
#define TEE_TYPE_HMAC_SHA1                    0xA0000002  /* Between 80 and 512 bits, multiple of 8 bits */
#define TEE_TYPE_HMAC_SHA224                  0xA0000003  /* Between 112 and 512 bits, multiple of 8 bits */
#define TEE_TYPE_HMAC_SHA256                  0xA0000004  /* Between 192 and 1024 bits, multiple of 8 bits */
#define TEE_TYPE_HMAC_SHA384                  0xA0000005  /* Between 256 and 1024 bits, multiple of 8 bits */
#define TEE_TYPE_HMAC_SHA512                  0xA0000006  /* Between 192 and 1024 bits, multiple of 8 bits */
#define TEE_TYPE_RSA_PUBLIC_KEY               0xA0000030  /* Object size is the number of bits in the modulus. Min key size 256 bits, must support up to 2048 bit key sizes */
#define TEE_TYPE_RSA_KEYPAIR                  0xA1000030  /* Same as RSA public key size */
#define TEE_TYPE_DSA_PUBLIC_KEY               0xA0000031  /* Between 512 and 1024 bits, multiple of 64 bits */
#define TEE_TYPE_DSA_KEYPAIR                  0xA1000031  /* Same as DSA public key size */
#define TEE_TYPE_DH_KEYPAIR                   0xA1000032  /* From 256 to 2048 bits */
#define TEE_TYPE_GENERIC_SECRET               0xA0000000  /* Multiple of 8 bits, up to 4096 bits */

/* TEE Object/Operation attributes */

#define TEE_ATTR_SECRET_VALUE                 0xC0000000
#define TEE_ATTR_LABEL                        0xC0000100
#define TEE_ATTR_CONTEXT                      0xC0000200
#define TEE_ATTR_INPUT_KEY                    0xC0000300
#define TEE_ATTR_RSA_MODULUS                  0xD0000130
#define TEE_ATTR_RSA_PUBLIC_EXPONENT          0xD0000230
#define TEE_ATTR_RSA_PRIVATE_EXPONENT         0xC0000330
#define TEE_ATTR_RSA_PRIME1                   0xC0000430
#define TEE_ATTR_RSA_PRIME2                   0xC0000530
#define TEE_ATTR_RSA_EXPONENT1                0xC0000630
#define TEE_ATTR_RSA_EXPONENT2                0xC0000730
#define TEE_ATTR_RSA_COEFFICIENT              0xC0000830
#define TEE_ATTR_DSA_PRIME                    0xD0001031
#define TEE_ATTR_DSA_SUBPRIME                 0xD0001131
#define TEE_ATTR_DSA_BASE                     0xD0001231
#define TEE_ATTR_DSA_PUBLIC_VALUE             0xD0000131
#define TEE_ATTR_DSA_PRIVATE_VALUE            0xC0000231
#define TEE_ATTR_DH_PRIME                     0xD0001032
#define TEE_ATTR_DH_SUBPRIME                  0xD0001132
#define TEE_ATTR_DH_BASE                      0xD0001232
#define TEE_ATTR_DH_X_BITS                    0xF0001332
#define TEE_ATTR_DH_PUBLIC_VALUE              0xD0000132
#define TEE_ATTR_DH_PRIVATE_VALUE             0xC0000232
#define TEE_ATTR_RSA_OAEP_LABEL               0xD0000930
#define TEE_ATTR_RSA_PSS_SALT_LENGTH          0xF0000A30
#define TEE_ATTR_RSA_KEY_CRT_DECRYPT          0xF0000B30

/* TEE attribute ID flags */

#define TEE_ATTR_FLAG_VALUE                   0x20000000
#define TEE_ATTR_FLAG_PUBLIC                  0x10000000

/* TEE crypto GP macros */

#define TEE_ALG_SHA1_DIGEST_LENGTH            20
#define TEE_ALG_SHA256_DIGEST_LENGTH          32

#define TEE_ALG_AES_BLK_SIZE                  16
#define TEE_ALG_AES128_LENGTH                 16*8
#define TEE_ALG_AES256_LENGTH                 32*8

#endif /* GPTYPES_H */
