﻿#ifndef _QSEE_SFS_H_
#define _QSEE_SFS_H_

/**
@file gpPersistentObjects.h
@brief Secure File System with Encryption and Integrity Protection.
This file contains the definitions of the constants, data 
structures, and interfaces that provide methods to 
encrypt/integrity-protect and decrypt/verify content to the EFS.
*/

/*=============================================================================
 Copyright (c) 2000-2013 Qualcomm Technologies Incorporated.
                      All Rights Reserved.
 Qualcomm Confidential and Proprietary.
=============================================================================*/

/*=============================================================================

                       EDIT HISTORY FOR MODULE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/api/securemsm/trustzone/gp/gpPersistentObjects.h#1 $ 
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
10/06/13   yh      Initial version
=============================================================================*/



/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#ifdef __cplusplus
extern "C"
{
#endif

/**
   @addtogroupqsee_sfs_apis 
   @{
*/

/**
The TEE_OpenPersistentObject function opens a handle on an existing persistent object. 
It returns a handle that can be used to access the object’s attributes and data stream.

@param[in]  storageID   The storage to use. It must be TEE_STORAGE_PRIVATE.
@param[in]  objectID    objectIDLen: The object identifier. Note that this buffer cannot reside in shared memory.
@param[in]  flags       The flags which determine the settings under which the object is opened.
@param[in]  object      A pointer to the handle, which contains the opened handle upon successful completion.
                        If this function fails for any reason, the value pointed to by object is set to TEE_HANDLE_NULL. 
                        When the object handle is no longer required, it must be closed using a call to the TEE_CloseObject function.

@return
TEE_SUCCESS: In case of success
TEE_ERROR_ITEM_NOT_FOUND: If the storage denoted by storageID does not exist or if the object identifier cannot be found in the storage
TEE_ERROR_ACCESS_CONFLICT: If an access right conflict was detected while opening the object
TEE_ERROR_OUT_OF_MEMORY: If there is not enough memory to complete the operation

*/
TEE_Result TEE_OpenPersistentObject
(
  uint32_t storageID,
  void* objectID, 
  size_t objectIDLen,
  uint32_t flags,
  TEE_ObjectHandle* object
);

/**
The TEE_CreatePersistentObject function creates a persistent object with initial attributes and an initial data stream content, 
and optionally returns a handle on the created object..

In the SFS, opening a file does not actually do anything in the file 
system. However, if the file (along with the associated file segments) already 
exists and the file is created with the O_TRUNC mode, the associated 
subfiles are deleted. The first segment is created only when new bytes to be 
written begin arriving.
Note:The base directory must exist; otherwise, a NULL is returned.

@param[in] storageID  The storage to use. It must be TEE_STORAGE_PRIVATE.
@param[in] objectID, objectIDLen  The object identifier. Note that this cannot reside in shared memory.
@param[in] flags      The flags which determine the settings under which the object is opened
@param[in] attributes A handle on a transient object from which to take the persistent object attributes. 
                      Can be TEE_HANDLE_NULL if the persistent object contains no attribute, for example if it is a pure data objec
@param[in]initialData, size_t initialDataLen  The initial data content of the persistent object
@param[out]           A pointer to the handle, which contains the opened handle upon successful completion. 
                      If this function fails for any reason, the value pointed to by object is set to TEE_HANDLE_NULL. 
                      When the object handle is no longer required, it must be closed using a call to the TEE_CloseObject function.

@return
TEE_SUCCESS: In case of success
TEE_ERROR_ITEM_NOT_FOUND: If the storage denoted by storageID does not exist or if the object identifier cannot be found in the storage
TEE_ERROR_ACCESS_CONFLICT: If an access right conflict was detected while opening the object
TEE_ERROR_OUT_OF_MEMORY: If there is not enough memory to complete the operation
TEE_ERROR_STORAGE_NO_SPACE: If insufficient space is available to create the persistent object

*/
TEE_Result TEE_CreatePersistentObject
(
  uint32_t storageID,
  void* objectID, size_t objectIDLen,
  uint32_t flags,
  TEE_ObjectHandle attributes,
  void* initialData, size_t initialDataLen,
  TEE_ObjectHandle* object
);

/**
The TEE_CloseAndDeletePersistentObject function marks an object for deletion and closes the object handle.
The object handle must have been opened with the write-meta access right, which means access to the object is exclusive.
Deleting an object is atomic; once this function returns, the object is definitely deleted and no more open handles for that object exist.
If object is TEE_HANDLE_NULL, the function does nothing.

@param[in] object   The object handle

@return

*/
void TEE_CloseAndDeletePersistentObject( TEE_ObjectHandle object );

/**
The function TEE_RenamePersistentObject changes the identifier of an object. 
The object handle must have been opened with the write-meta access right, which means access to the object is exclusive.
Renaming an object is an atomic operation; either the object is renamed or nothing happens.

@param[in] object   The object handle
@param[in] newObjectID, newObjectIDLen    A buffer containing the new object identifier. 
                    The identifier can contain arbitrary bytes, including the zero byte. 
                    The identifier length can be zero but must be less than or equal to TEE_OBJECT_ID_MAX_LEN. 
                    The buffer containing the new object identifier cannot reside in shared memory.

@return 
TEE_SUCCESS: In case of success
TEE_ERROR_ACCESS_CONFLICT: If an object with the same identifier already exists

*/
TEE_Result TEE_RenamePersistentObject
(
  TEE_ObjectHandle object,
  void* newObjectID, 
  size_t newObjectIDLen
);

/**
The TEE_ReadObjectData function attempts to read size bytes from the data stream associated with the object object into the buffer pointed to by buffer.
The object handle must have been opened with the read access right.

The bytes are read starting at the position in the data stream currently stored in the object handle. 
The handle’s position is incremented by the number of bytes actually read.
On completion TEE_ReadObjectData sets the number of bytes actually read in the uint32_t pointed to by count. 
The value written to *count may be less than size if the number of bytes until the end-of-stream is less than size. 
It is set to 0 if the position at the start of the read operation is at or beyond the end-of-stream. These are the only cases where *count may be less than size.
No data transfer can occur past the current end of stream. 
If an attempt is made to read past the end-of-stream, the TEE_ReadObjectData function stops reading data at the end-of-stream and returns the data read up to that point. 
This is still a success. The position indicator is then set at the end-of-stream. 
If the position is at, or past, the end of the data when this function is called, then no bytes are copied to *buffer and *count is set to 0.

@param[in]  object   The object handle
@param[out] buffer   A pointer to the memory which, upon successful completion, contains the bytes read
@param[in]  size     The number of bytes to read
@param[out] count    A pointer to the variable which upon successful completion contains the number of bytes read

@return
The only possible return value is TEE_SUCCESS. The presence of an error return value is for future versions of the specification.
*/
TEE_Result TEE_ReadObjectData
(
  TEE_ObjectHandle object,
  void* buffer,
  size_t size,
  uint32_t* count
);

/**
The TEE_WriteObjectData function writes size bytes from the buffer pointed to by buffer to the data stream associated with the open object handle object.
The object handle must have been opened with the write access permission.
If the current data position points before the end-of-stream, then size bytes are written to the data stream, overwriting bytes starting at the current data position. 
If the current data position points beyond the stream’s end, then the data stream is first extended with zero bytes until the length indicated by the data position indicator is reached, 
and then size bytes are written to the stream. Thus, the size of the data stream can be increased as a result of this operation.
The data position indicator is advanced by size. The data position indicators of other object handles opened on the same object are not changed.
Writing in a data stream is atomic; either the entire operation completes successfully or no write is done.

@param[in] object   The object handle
@param[in] buffer   The buffer containing the data to be written
@param[in] size     The number of bytes to write

@return
TEE_SUCCESS: In case of success
TEE_ERROR_STORAGE_NO_SPACE: If insufficient storage space is available

*/
TEE_Result TEE_WriteObjectData
(
  TEE_ObjectHandle object,
  void* buffer, 
  size_t size
);

/**
The function TEE_TruncateObjectData changes the size of a data stream. 
If size is less than the current size of the data stream then all bytes beyond size are removed. 
If size is greater than the current size of the data stream then the data stream is extended by adding zero bytes at the end of the stream.
The object handle must have been opened with the write access permission.
This operation does not change the data position of any handle opened on the object. 
Note that if the current data position of such a handle is beyond size, the data position will point beyond the object data’s end after truncation.
Truncating a data stream is atomic: Either the data stream is successfully truncated or nothing happens.

@param[in] object   The object handle
@param[in] size     The new size of the data stream

@return
Returns the current location if successful, or -1 if an error occurred. 

*/
TEE_Result TEE_TruncateObjectData
(
  TEE_ObjectHandle object,
  uint32_t size
);

/**
The TEE_SeekObjectData function sets the data position indicator associated with the object handle.
The parameter whence controls the meaning of offset:
• If whence is TEE_DATA_SEEK_SET, the data position is set to offset bytes from the beginning of the data stream.
• If whence is TEE_DATA_SEEK_CUR, the data position is set to its current position plus offset.
• If whence is TEE_DATA_SEEK_END, the data position is set to the size of the object data plus offset.
The TEE_SeekObjectData function may be used to set the data position beyond the end of stream; this does not constitute an error. 
However, the data position indicator does have a maximum value which is TEE_DATA_MAX_POSITION. 
If the value of the data position indicator resulting from this operation would be greater than TEE_DATA_MAX_POSITION, 
the error TEE_ERROR_OVERFLOW is returned.
If an attempt is made to move the data position before the beginning of the data stream, 
the data position is set at the beginning of the stream. This does not constitute an error.

@param[in] object   The object handle
@param[in] offset   The number of bytes to move the data position. 
                    A positive value moves the data position forward; a negative value moves the data position backward.
@param[in] whence   The position in the data stream from which to calculate the new position

@return
TEE_SUCCESS: In case of success
TEE_ERROR_OVERFLOW: If the value of the data position indicator resulting from this operation would be greater than TEE_DATA_MAX_POSITION
*/
TEE_Result TEE_SeekObjectData
(
  TEE_ObjectHandle object,
  int32_t offset,
  TEE_Whence whence
);

/** @} */   /* end_addtogroup qsee_sfs_apis */
#ifdef __cplusplus
}
#endif

#endif //_qsee_sfs_API_H_
