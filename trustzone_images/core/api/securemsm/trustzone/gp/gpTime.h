#ifndef GPTIME_H
#define GPTIME_H

/**
@file gpTime.h
@brief Get system time.
*/

/*===========================================================================
   Copyright (c) 2013 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/api/securemsm/trustzone/gp/gpTime.h#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $

when       who      what, where, why
--------   ---      ------------------------------------
10/15/13    dp      Initial version.

===========================================================================*/

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include "gpTypes.h"

/*----------------------------------------------------------------------------
 * Function Declarations and Documentation
 * -------------------------------------------------------------------------*/

#ifdef __cplusplus
  extern "C"
  {
#endif

/**
 * @brief Get secure uptime (not UTC based).
 *
 * The TEE_GetSystemTime function retrieves the current system time.
 * The system time has an arbitrary implementation-defined origin that can vary across TA instances.
 * The minimum guarantee is that the system time must be monotonous for a given TA instance.
 * Implementations are allowed to use the REE timers to implement this function but may also better
 * protect the system time. A TA can discover the level of protection implementation by querying the
 * implementation property gpd.tee.systemTime.protectionLevel. Possible values are listed in Table 7-1.
 *
 * @param[out] time     Time in seconds and milliseconds
 */
void
TEE_GetSystemTime (
  TEE_Time* time /* [out] */
);

#ifdef __cplusplus
  }
#endif

#endif /* GPTIME_H */
