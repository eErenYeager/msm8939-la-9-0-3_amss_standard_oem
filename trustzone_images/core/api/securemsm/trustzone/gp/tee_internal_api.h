#ifndef TEE_INTERNAL_API_H
#define TEE_INTERNAL_API_H

/**
@file tee_internal_api.h
@brief Includes GP all library.
*/

/*===========================================================================
   Copyright (c) 2013 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/api/securemsm/trustzone/gp/tee_internal_api.h#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $

when       who      what, where, why
--------   ---      ------------------------------------
10/15/13    dp      Initial version.

===========================================================================*/

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include "gpCrypto.h"
#include "gpGenericObjectFunctions.h"
#include "gpHeap.h"
#include "gpLog.h"
#include "gpPropertyAccessFunctions.h"
#include "gpPersistentObjects.h"
#include "gpServices.h"
#include "gpTime.h"
#include "gpTransientObjects.h"
#include "gpTypes.h"

#endif /* TEE_INTERNAL_API_H */
