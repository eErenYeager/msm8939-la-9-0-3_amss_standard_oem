#ifndef __TYPES_H
#define __TYPES_H

/**
@file types.h
@brief Basic types to supplement gpTypes.h.
*/

/*===========================================================================
   Copyright (c) 2013 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/api/securemsm/trustzone/gp/types.h#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $

when       who      what, where, why
--------   ---      ------------------------------------
10/15/13    dp      Initial version.

===========================================================================*/

/*----------------------------------------------------------------------------
 * Type Definitions
 * -------------------------------------------------------------------------*/

//#if !defined(bool)
//#define bool _Bool
typedef unsigned int bool;
//#endif
//#if !defined(true)
#define true 1;
//#endif
//#if !defined(false)
#define false 0;
//#endif
//#if !defined(size_t)
typedef unsigned int size_t;
//#endif

#endif /* __TYPES_H */
