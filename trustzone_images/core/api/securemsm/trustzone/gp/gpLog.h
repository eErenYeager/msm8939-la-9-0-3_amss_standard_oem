#ifndef GP_LOG_H
#define GP_LOG_H

/**
@file gpLog.h
@brief Provide logging API wrappers
*/

/*===========================================================================
Copyright (c) 2013  by Qualcomm Technologies Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                                 Edit History

$Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/api/securemsm/trustzone/gp/gpLog.h#1 $
$DateTime: 2018/02/07 00:37:16 $
$Author: mplp4svc $

when       who     what, where, why
--------   ---     --------------------------------------------------------
1/16/14    dp     Initial version.

===========================================================================*/

/* ------------------------------------------------------------------------
** Include files
** ------------------------------------------------------------------------ */
#include <comdef.h>
#include <stdarg.h>
#include <string.h>
#include "gpTypes.h"

/*----------------------------------------------------------------------------
 * Function Declarations and Documentation
 * -------------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C"
  {
#endif

/* Flags to control logging.  Enable the flag which will allow
 * the logs with corresponding log level.  For e.g., if we set
 * ENABLE_TEE_LOG_MSG_ERROR to 1 then we will get only error logging */
#define ENABLE_TEE_LOG_MSG_LOW      0
#define ENABLE_TEE_LOG_MSG_MED      0
#define ENABLE_TEE_LOG_MSG_HIGH     0
#define ENABLE_TEE_LOG_MSG_ERROR    1
#define ENABLE_TEE_LOG_MSG_FATAL    1
#define ENABLE_TEE_LOG_MSG_DEBUG    1


/* Priority of debug log messages */
#define TEE_LOG_MSG_LOW      0
#define TEE_LOG_MSG_MED      1
#define TEE_LOG_MSG_HIGH     2
#define TEE_LOG_MSG_ERROR    3
#define TEE_LOG_MSG_FATAL    4
#define TEE_LOG_MSG_DEBUG    5

typedef void (*LOG_FUNC)(uint8, const char*, ...);
extern LOG_FUNC TEE_Log;

/**
 * @brief Logs to the file in HLOS. Do check if this API is
 *        supported on your platform
 *
 * @param[in] None
 *
 * @return
 *
 */

#define TEE_LOG(xx_prio, xx_fmt, ...)                                             \
    do {                                                                          \
      if((xx_prio == TEE_LOG_MSG_LOW) && (ENABLE_TEE_LOG_MSG_LOW)) {              \
        TEE_Log(xx_prio, #xx_fmt, ##__VA_ARGS__); }                               \
      else if((xx_prio == TEE_LOG_MSG_MED) && (ENABLE_TEE_LOG_MSG_MED)) {         \
        TEE_Log(xx_prio, #xx_fmt, ##__VA_ARGS__);                    }            \
      else if((xx_prio == TEE_LOG_MSG_HIGH) && (ENABLE_TEE_LOG_MSG_HIGH)) {       \
        TEE_Log(xx_prio, #xx_fmt, ##__VA_ARGS__);                    }            \
      else if((xx_prio == TEE_LOG_MSG_ERROR) && (ENABLE_TEE_LOG_MSG_ERROR)) {     \
        TEE_Log(xx_prio, #xx_fmt, ##__VA_ARGS__);                    }            \
      else if((xx_prio == TEE_LOG_MSG_FATAL) && (ENABLE_TEE_LOG_MSG_FATAL)) {     \
        TEE_Log(xx_prio, #xx_fmt, ##__VA_ARGS__);                    }            \
      else if((xx_prio == TEE_LOG_MSG_DEBUG) && (ENABLE_TEE_LOG_MSG_DEBUG)) {     \
        TEE_Log(xx_prio, #xx_fmt, ##__VA_ARGS__);                    }            \
    } while (0)

#ifdef __cplusplus
  }
#endif

#endif /* GP_LOG_H */
