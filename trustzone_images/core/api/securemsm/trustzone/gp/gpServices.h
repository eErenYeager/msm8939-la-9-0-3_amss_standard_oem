#ifndef GP_SERVICES_H
#define GP_SERVICES_H

/**
@file gpServices.h
@brief Provide API wrappers for services
*/

/*============================================================================
Copyright (c) 2012 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
=========================================================================== */

/*=========================================================================
                              Edit History

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/api/securemsm/trustzone/gp/gpServices.h#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $


when       who     what, where, why
--------   ---     --------------------------------------------------------
01/015/14    dp     Initial Revision

=========================================================================== */

#include "gpTypes.h"

/**
 * @brief API for secure applications to register shared buffer with TEE.
 * @param[in] address
 * @param[in] size
 *
 * @return TEE_SUCCESS on success, TEE_ERROR_GENERIC on error.
 */
TEE_Result
TEE_RegisterSharedBuffer(
    void* address,
    size_t size
);


/**
 * API for secure applications to deregister the shared buffer
 * which has been previously registered with TEE.
 *
 * @param[in] address
 *
 * @return TEE_SUCCESS on success, TEE_ERROR_GENERIC on error.
 */
TEE_Result
TEE_DeregisterSharedBuffer(
    void* address
);


/**
 * @brief API for preparing the shared buffer sent by non-secure side before secure side reads it.
 * @param[in] address
 * @param[in] size
 *
 * @return TEE_SUCCESS on success, TEE_ERROR_GENERIC on error.
 */
TEE_Result
TEE_PrepareSharedBufferForSecureRead(
    void* address,
    size_t size
);


/**
 * API for prepaing shared buffer before sending it across to
 * non-secure side.
 *
 *
 * @param[in] address
 * @param[in] size
 *
 * @return TEE_SUCCESS on success, TEE_ERROR_GENERIC on error.
 */
TEE_Result
TEE_PrepareSharedBufferForNonsecureRead(
    void* address,
    size_t size
);

#endif /* GP_SERVICES_H */
