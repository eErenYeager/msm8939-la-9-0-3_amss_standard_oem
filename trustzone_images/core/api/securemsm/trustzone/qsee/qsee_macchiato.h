/**
 * Copyright 2014 QUALCOMM Technologies, Inc. 
 * All Rights Reserved.
 * QUALCOMM Confidential and Proprietary
 *
 * @file qsee_macchiato.h
 * @author	qxu
 * @version     1.0
 * @since       2014-05-16
 *
 * @brief header file for macchiato_key_service.c
 */

/*===========================================================================
   Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
  
   This file contains the definitions of the constants, data structures
   and interfaces that provide Macchiato RSA Key service support. It defines
   the external interfaces for Macchiato RSA key encrpytion/decryption, signature/
   verification, key generation.
  ===========================================================================*/
/*===========================================================================

                            EDIT HISTORY FOR FILE

  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $

  when       who      what, where, why
  --------   ---      ------------------------------------
  28/5/14     qxu      First version of qsee_macchiato.h 

===========================================================================*/

#ifndef QSEE_MACCHIATO_H
#define QSEE_MACCHIATO_H

#include "qsee_rsa.h"
#include "qsee_hash.h"
#include "qsee_cipher.h"
#include "qsee_log.h"

#define MACC_LOG QSEE_LOG
#define MACC_LOG_MSG_ERROR QSEE_LOG_MSG_ERROR
#define MACC_LOG_MSG_DEBUG QSEE_LOG_MSG_DEBUG

#define MAX_PUB_EXP_SIZE 5 // Array of public exponent e.g., {0x01, 0x00, 0x01}


/* Macchiato Return Error Codes */
#define QSEE_MACCHIATO_SUCCESS                     0  /* Success */
#define QSEE_MACCHIATO_KEY_EXIST                   1  /* Key exists already */
#define QSEE_MACCHIATO_ERROR                      -1  /* Generic Failure */
#define QSEE_MACCHIATO_INVALID_PARAM		      -2  /* Invalid arguments passed to the API */
#define QSEE_MACCHIATO_NOT_SUPPORTED              -3  /* Operation Not supported */
#define QSEE_MACCHIATO_BUFFER_TOO_SMALL           -4  /* memory buffer from caller is too small */
#define QSEE_MACCHIATO_OUT_OF_RESOURCES           -5  /* Out of memory/other resources*/
#define QSEE_MACCHIATO_INTERNAL_ERROR             -6  /* internal error such as key size is too big*/
#define QSEE_MACCHIATO_CIPHER_ERROR               -7  /* cipher operation error */
#define QSEE_MACCHIATO_SECMATH_ERROR              -8  /* secmath operation error */

#define QSEE_MACCHIATO_MSG_SIGN_PUBKEY_LEN      256 /*2048 bits*/
#define QSEE_MACCHIATO_MSG_ENC_PUBKEY_LEN       256 /*2048 bits*/
#define QSEE_MACCHIATO_MSG_SIGNATURE_LEN        256 /*2048 bits*/

#define QSEE_MACCHIATO_MSG_TA_HASH_LEN          32 /*256 bits*/
#define QSEE_MACCHIATO_MSG_NUM_SVCID_LEN        4  /*32 bits*/
#define QSEE_MACCHIATO_MSG_SVCID_LEN            4  /*32 bits*/

#define QSEE_MACCHIATO_KEYTYPE_SIGN				1
#define QSEE_MACCHIATO_KEYTYPE_ENC				2
/* 
#define QSEE_MACCHIATO_MSG_MAX_NUM_SVCID        4  / *maximum 4 service ids* / 
#define QSEE_MACCHIATO_MAX_MSG_HEADER_LEN       (QSEE_MACCHIATO_MSG_TA_HASH_LEN \
                                                 + QSEE_MACCHIATO_MSG_NUM_SVCID_LEN \
                                                 + QSEE_MACCHIATO_MSG_MAX_NUM_SVCID * QSEE_MACCHIATO_MSG_SVCID_LEN)
*/

/**
 * @brief 
 *	RSA private/public key generatio function. The step taken is as follows:
 *	1. generate random number usiung hw rng, size if 2048/1024 (mod size)
 *  2. check if it is prime - p
 *  3. if it is prime, then genrate another rng, check if it is prime - q
 *  4. multiply P.Q = N
 *  5. PHI = (p-1).(q-1)
 *  6. find d = invmod (e, PHI)
 *  7. take two same big numbers, x, y
 *  8. encrypt x with (e, n) using modexp = x1
 *  9. decrypt x1 with (d, n) using mod exp = x2
 *  10. see if x2 and x match, if not go back to (1)
 *  
 * @param[]  NULL
 *
 * @return 0 on success, QSEE_MACCHIATO_KEY_EXIST (1) if key already exists,
 * negative on failure 
*/ 
 
int qsee_macchiato_gen_key(void);

void qsee_macchiato_destroy_key(void);

/**
 * @brief  This function will pre-append the opaque data provided by TA with
 *  	   service ids list associated with the TA, hash the data blob with
 *  	   SHA-256 and sign signature with PKCS #1 padding.
 *
 * @param[in] opaque_data 		   the data buffer contained the data to be signed
 *  	 						         from TA
 * @param[in] opaque_data_len 	the length of the opaque data 
 * @param[out] signed_data 		the output data which has been signed, including
 *  							         Num SVC ID�s, SVC ID�s, Opaque Data. Data buffer
 * 								      provided by TA.
 * @param[out] signed_data_len	the size of the signed data 
 * @param[out] signature   		The signature
 * @param[in,out] siglen   		The max size and resulting size of the signature
 *
 * @return 0 on success, negative on failure
*/
int qsee_macchiato_rsa_sign
(
   const unsigned char       *opaque_data, 
   unsigned int               opaque_data_len,
   unsigned char             *signed_data,
   unsigned int              *signed_data_len,
   unsigned char             *signature,
   unsigned int              *signature_len
);

/**
 * @brief  This function will hash the signed data provided by TA with SHA-256, 
 *  	   calcuate the signature with PKCS #1 padding and verify it with
 *  	   provided signature.
 *
 * @param[in] signed_data  		The data buffer contained the signed data from 
 *  	 						         TA
 * @param[in] signed_data_len   The length of signed data 
 * @param[in] signature         The signature to be verified provided by TA
 * @param[in] signature_len     The size the signature 
 *
 * @return 0 on success, negative on failure
*/
int qsee_macchiato_rsa_verify
(
   unsigned char             *signed_data, 
   unsigned int               signed_data_len,
   unsigned char             *signature, 
   unsigned int               signature_len
);

/**
 * @brief This function will encrypt the plain message provided by TA with PKCS 
 *  	  #1 padding.
 *
 * @param[in] msg           The plaintext
 * @param[in] msglen        The length of the plaintext
 * @param[out] cipher       The ciphertext 
 * @param[in,out] cipherlen The max size and resulting size of the ciphertext 
 *
 * @return 0 on success, negative on failure
*/
int qsee_macchiato_rsa_encrypt
(
   const unsigned char     *msg, 
   unsigned int             msglen,
   unsigned char           *cipher,
   unsigned int            *cipherlen
);

/**
 * @brief  This function will decrpyed the cipher message provided by TA and 
 *  	   then v1.5 depad.
 *
 * @param[in] cipher        The ciphertext
 * @param[in] cipherlen     The length of the ciphertext(octets)
 * @param[out] msg          The plaintext 
 * @param[in,out] msglen    The max size and resulting size of the plaintext 
 *
 * @return 0 on success, negative on failure
*/
int qsee_macchiato_rsa_decrypt
(
   unsigned char           *cipher,
   unsigned int             cipherlen,
   unsigned char           *msg,
   unsigned int            *msglen
);

#endif //QSEE_MACCHIATO_H

