#ifndef QSEE_VERSION_H
#define QSEE_VERSION_H

/*============================================================================
Copyright (c) 2012 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
=========================================================================== */

/*=========================================================================
                              Edit History

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/api/securemsm/trustzone/qsee/qsee_version.h#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $


when       who     what, where, why
--------   ---     --------------------------------------------------------
08/04/11    rv     Initial Revision

=========================================================================== */

#define QSEE_VERSION_MAJOR 02
#define QSEE_VERSION_MINOR 01

#endif /*QSEE_VERSION_H*/

