#ifndef PROCESSOR_H
#define PROCESSOR_H
/*==========================================================================

                         Processor Selection Header File

DESCRIPTION
  Given a CPU target in target.h, this will include the appropriate CPU
  specific header file.
  
  User's should include "processor.h" instead of the CPU specific header.


Copyright (c) 1997-1999 by Qualcomm Technologies Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

$PVCSPath: L:/src/asw/COMMON/vcs/processor.h_v   1.0   Aug 21 2000 11:51:06   lpetrucc  $
$Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/api/services/processor.h#1 $ $DateTime: 2018/02/07 00:37:16 $ $Author: mplp4svc $
   
when       who     what, where, why
--------   ---     ----------------------------------------------------------
12/16/98   jct     Removed 80186 support
07/10/98   jct     Revised for coding standard, removed unused code
01/01/98   bwj     Created

===========================================================================*/

/*===========================================================================
 
                           INCLUDE FILES

===========================================================================*/

#include "arm.h"

#endif /* END PROCESSOR_H */
