/**
  @file bamconfig_props.c
  @brief
  This file contains implementation of the interfaces that provide
  target specific base addresses & irq vector data to the BAM driver.

*/
/*
===============================================================================
                            Edit History

$Header:

when       who     what, where, why
--------   ---     ------------------------------------------------------------
08/18/14   SS      Removed IRQ and base address query
21/02/14   SA      Added devcfg props support for tgtcfg and supported BAMs for 8916.

===============================================================================
                   Copyright (c) 2014 Qualcomm Technologies Incorporated.
                          All Rights Reserved.
                    Qualcomm Confidential and Proprietary.
===============================================================================
*/
#ifdef DAL_CONFIG_IMAGE_APPS
#ifndef BAM_APPS_CONFIG
#define BAM_APPS_CONFIG
#endif
#endif

#include "bamtgtcfg.h"
#ifdef BAM_XML_PROPS
#ifdef BAM_MODEM_CONFIG
#include "bamtgtcfgdata_mpss.h"
#elif BAM_TZOS_CONFIG
#include "bamtgtcfgdata.h"
#else
#error "Invalid header file included "
#endif
#endif

