#ifndef _SYSDBG_CLK_H
#define _SYSDBG_CLK_H

#include "comdef.h"

#define ARRAY_SIZE(array) (sizeof(array)/sizeof(array[0]))

#define HWIO_PLL_MODE_OFFS                 (HWIO_ADDR(GCC_GPLL0_MODE) - HWIO_ADDR(GCC_GPLL0_MODE))
#define HWIO_PLL_L_VAL_OFFS                (HWIO_ADDR(GCC_GPLL0_L_VAL) - HWIO_ADDR(GCC_GPLL0_MODE))
#define HWIO_PLL_M_VAL_OFFS                (HWIO_ADDR(GCC_GPLL0_M_VAL) - HWIO_ADDR(GCC_GPLL0_MODE))
#define HWIO_PLL_N_VAL_OFFS                (HWIO_ADDR(GCC_GPLL0_N_VAL) - HWIO_ADDR(GCC_GPLL0_MODE))
#define HWIO_PLL_USER_CTL_OFFS             (HWIO_ADDR(GCC_GPLL0_USER_CTL) - HWIO_ADDR(GCC_GPLL0_MODE))
#define HWIO_PLL_CONFIG_CTL_OFFS           (HWIO_ADDR(GCC_GPLL0_CONFIG_CTL) - HWIO_ADDR(GCC_GPLL0_MODE))
#define HWIO_PLL_TEST_CTL_OFFS             (HWIO_ADDR(GCC_GPLL0_TEST_CTL) - HWIO_ADDR(GCC_GPLL0_MODE))
#define HWIO_PLL_STATUS_OFFS               (HWIO_ADDR(GCC_GPLL0_STATUS) - HWIO_ADDR(GCC_GPLL0_MODE))

/* Type for statically defined clock branches */
typedef	uint32 cbcr_type;



/*===========================================================================
** 	Function: sysdbg_clock_init
** ==========================================================================
*/
/*!
* 
* @brief
* 	Initialize the clock branches for the debug image
*
*
* @param[in] 
*	None
*  
* @par Dependencies
*	None
* 
* @retval
*   None
* 
* @par Side Effects
*	None
*
*/
boolean sysdbg_clock_init(void);

#endif

