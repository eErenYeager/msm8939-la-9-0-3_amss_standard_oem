/*=============================================================================
                        SDI Main Init Function

GENERAL DESCRIPTION     
		  This module handles the main entry point of the debug functionalities within TZ.
  
	Copyright 2012- 2014 by Qualcomm Technologies, Inc.  All Rights Reserved.
=============================================================================*/

/*=============================================================================

                            EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

01/13/14	davidai					Ported for Bear
07/02/13    AJCheriyan  Updated for Gandalf
01/28/13	AJCheriyan	Added BOOT_PARTITION_SELECT write for v2
01/15/13	AJCheriyan	Moved reset apps core and debug feature enablement here
02/22/12	AJCheriyan	First cut for Aragorn. First ever SYSDBG
===========================================================================*/
#include <HALhwio.h>
#include "comdef.h"
#include "tzbsp_sys.h"
//#include "util.h"
#include "sysdbg_defs.h"
#include "sysdbg_hwio.h"
#include "tzbsp_xpu.h"
#include "busywait.h"

#define ARRAY_SIZE(array) (sizeof(array)/sizeof(array[0]))
#define CALL_IF_VALID(execute, fcnptr)	if (execute) \
											if (fcnptr) \
												{ /*SYSDBG_LOG_EVENT(SYSDBG_ENTER_FCN, fcnptr, get_current_cpu(), 0);*/ fcnptr();  /*SYSDBG_LOG_EVENT(SYSDBG_EXIT_FCN, fcnptr, get_current_cpu(), 0);*/} \
											else { break; }	\
										else				\
											{ continue; }	\

#define CALL_IF_VALID_AND_CHECK(rval, execute, fcnptr)	if (execute) \
															if (fcnptr) \
																{ /*SYSDBG_LOG_EVENT(SYSDBG_ENTER_FCN, fcnptr, get_current_cpu(), 0);*/ rval = fcnptr(); /*SYSDBG_LOG_EVENT(SYSDBG_EXIT_FCN, fcnptr, get_current_cpu(), 0);*/ } \
															else					    \
																{ break; }				\
														else							\
															{ continue; }



extern sysdbg_pass_data_type *sysdbg_pass;
extern sysdbg_pass_data_type sysdbg_pass_data;

static uint32 pass_count = 0;

extern boolean tzbsp_allow_unlock_xpu(void);

/*===========================================================================
**  Function :  SYSTEM_DEBUG_MAIN
** ==========================================================================
*/
/*!
* 
* @brief
* 	Main SYSDBG
* 	
* 
* @param[in] 
*       Pass count
*  
* @par Dependencies
*   None
* 
* @retval
*   None
* 
* @par Side Effects
*   None
* 
*
*/


void system_debug_main(void)
{
    /* Delay of 100ms in First pass reset path to avoid race condition between a core waking up 
       from power collapse and PS_HOLD de-assertion after Secure watchdog expiry - CR:879123*/
    if (1 == pass_count)
    {
        busywait_init();
        busywait(100000);
    }
		
	if(tzbsp_allow_unlock_xpu()) //Only allow sysdbg execution when enabled
	{
		sysdbg_pass=&sysdbg_pass_data;
		sysdbg_execute_pass(sysdbg_pass);
	}
}

/*===========================================================================
**  Function : SYSDBG_EXECUTE_PASS
** ==========================================================================
*/
/*!
* 
* @brief
*	This function is the workhorse for the image. It iteratively scans through 
*	the pass structure and executes every function in there. The functions are
*	classified as init, work and exit functions respectively. Init and exit functions
*	don't return values while work functions return a success / fail value. Every
*	function must be capable of being run on all cores. If you need to distinguish
*	between a particular core, handle it within the function.
*
* @param[in] 
*	The pointer to the SYSDBG pass data structure, pass_count j
*  
* @par Dependencies
*   None
* 
* @retval
*   None
* 
* @par Side Effects
*   Should be capable of handling multi-core situations as needed.
*
*
*/

static void sysdbg_execute_pass(sysdbg_pass_data_type *sysdbg_pass_ptr)
{
	boolean pass = TRUE;
	uint32 i = 0, current_cpu = tzbsp_cur_cpu_num();
	uint32 current_cluster = (current_cpu >= TARGET_CLUSTER_0_SIZE) ? 1 : 0;
	sysdbg_pass_sequence_type *sysdbg_seq_entry;
	//uint32 pass_count = sysdbg_reset_check();//pass 0 is cold boot; pass 1 is reset boot
	/* KW Fix */
	if ((current_cpu < (TARGET_CLUSTER_0_SIZE + TARGET_CLUSTER_1_SIZE)) && (current_cluster < TARGET_CLUSTERS))
	{
		if (sysdbg_pass_ptr)
		{
			// Log the entry
			//SYSDBG_LOG_EVENT(SYSDBG_PASS_ENTER, __current_pc(), __current_sp(), current_cpu);

			// We have our entry from the table. Now assign the pointer
			sysdbg_seq_entry = &sysdbg_pass_ptr[pass_count].pass_sequence;
			do 
			{
				/* Now call all the functions that actually get something
				 done if 1) it is intended for current cpu 2) valid */
				for (i=0; (i < ARRAY_SIZE(sysdbg_seq_entry->fcn_info) && (pass)); i++)
				{
					  //#define CALL_IF_VALID_AND_CHECK(rval, execute, fcnptr)
					  if (sysdbg_seq_entry->fcn_info[i].cpu_affinity[current_cluster][current_cpu])
					  {
						  if (sysdbg_seq_entry->fcn_info[i].sysdbg_fcn)
						  { 
							if (1 == pass_count) {
							    /* Warm boot function marker updated in warm boot */
						        SYSDBG_FUNC_START_MARKER(i,current_cpu);
							}

							//TZBSP_LOG_ERR(SDI_ENTER_FCN, (uint64)sdi_seq_entry->fcn_info[i].sdi_fcn, current_cluster, current_cpu); 
							pass = sysdbg_seq_entry->fcn_info[i].sysdbg_fcn(); 
							//TZBSP_LOG_ERR(SDI_EXIT_FCN, (uint64)sdi_seq_entry->fcn_info[i].sdi_fcn, current_cluster, current_cpu);
						  }
						  else
						  {
							break;
						  }
					  }
					  else
					  {
						continue;
					  }
				}
				/* Check the return value before proceeding further */
			
			}while(FALSE);
			/* pass_count incremented to 1 for warm-boot sequence */
            pass_count = 1;	
			
			// We are done with this pass. We are good. So mark it
			//sysdbg_pass_ptr[pass_count].complete[current_cpu] = TRUE;

			// Log the exit
			//SYSDBG_LOG_EVENT(SYSDBG_PASS_EXIT, __current_pc(), __current_sp(), current_cpu);
		}
	}
}

