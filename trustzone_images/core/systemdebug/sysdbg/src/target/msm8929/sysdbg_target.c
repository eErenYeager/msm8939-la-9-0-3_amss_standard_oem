/*=============================================================================
                        sysdbg Main Control Function

GENERAL DESCRIPTION
		  This module defines objects for the main control loop of the debug image.

	Copyright 2012- 2014 by Qualcomm Technologies, Inc.  All Rights Reserved.
=============================================================================*/

/*=============================================================================

                            EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/systemdebug/sysdbg/src/target/msm8929/sysdbg_target.c#1 $

when       who          what, where, why
--------   ---          --------------------------------------------------
07/02/13    AJCheriyan  Updated for Gandalf
01/28/13	AJCheriyan	Added BOOT_PARTITION_SELECT write for v2
01/15/13	AJCheriyan	Moved reset apps core and debug feature enablement here
02/22/12	AJCheriyan	First cut for Aragorn. First ever sysdbg

===========================================================================*/
#include "comdef.h"
#include "sysdbg_mem_dump.h"
#include "sysdbg_defs.h"
#include "aa64_ctx.h"
#include "sysdbg_pmic.h"
#include "sysdbg_target.h"
#include "tzbsp_sys.h"
#include "tzbsp.h"
#include "tz_mc.h"
#include "sysdbg_hwio.h"
#include "busywait.h"
#include "HALqgic.h"
#include <string.h>

#define SYSTEM_DEBUG_MARKER (SHARED_IMEM_BASE + 0xB14)
#define APPS_ALIAS_SIZE     0x10000

extern uint32 Image$$TZBSP_CODE$$Base;
extern void sysdbg_el1_entry_handler(void);

extern boolean tzbsp_allow_memory_dump(void);
extern uint8 tzbsp_get_cpu_count(void);
extern uint8 tzbsp_get_cluster_count(void);

extern boolean tzbsp_sysdbg_flat_mapped(void);

// "Lock" used by all 8 CPU cores
static uint64 global_lock = 0x0;

/* Debug structure to dump SPM status and control registers */
static volatile sysdbg_dump_spm_regs_type sysdbg_spm_regs;

// Boot cpu
uint8 boot_cpu = 0x0;
// Variable to store total cpu count
uint8 total_cpu_count = 0x0;
// Variable to get cluster count
uint8 total_cluster_count = 0x0;
static uint32 Secondary_cluster_online_cpu_count = 0x0;

// CTX saved from Monitor
static __attribute__ ((aligned (16))) aa_64_ctx_all_els_t mon_ctxt_dump;
//extern uint32 Image$$SYSDBG$$Base;

/*===========================================================================
** 	Function: GLOBAL LOCK SET/ GET Functions
** ==========================================================================
*/
/*!
* 
* @brief
*	The global lock is used to put current core in wait forever loop before 
*   bringing the next core out of reset. 
*/	
void set_global_lock(uint32 count)
{
	global_lock = count;
}

uint32 get_global_lock(void)
{
	return global_lock;
}

boolean sysdbg_update_core_info (void)
{
    static boolean init = FALSE;
	if (!init)
    {
        /* set current cpu number as Boot cpu to differentiaite */
    	/* between quadcore(8936) and octa core (8939) */
    	/* Update Current CPU number, which should be Boot cpu */
    	uint8 curr_cpu_num = tzbsp_cur_cpu_num();
    	boot_cpu = curr_cpu_num;

        /* Initialize the global lock with current cpu number */
        set_global_lock(curr_cpu_num);

        /* Set total apps cpu count */
        total_cpu_count = tzbsp_get_cpu_count();
        total_cluster_count = tzbsp_get_cluster_count();
        init = TRUE;
    }
    return TRUE;
}
/*===========================================================================
**  Function :  SYSDBG_DUMP_SPM_REGS
** ==========================================================================
*/
/*!
*
* @brief
* 	Dump SPm status and control registers for each core
* 	
*
* @param[in]
*  None
*
* @par Dependencies
*   None
*
* @retval
*   None
*
* @par Side Effects
*   None
*
*/
static void sysdbg_dump_spm_regs_flat_mapped(void);

void sysdbg_dump_spm_regs(void)
{
	int i;

	if (tzbsp_sysdbg_flat_mapped()) 
	{ 
		sysdbg_dump_spm_regs_flat_mapped();
		return;
	} 

	/* Log MPM2 Time */
	sysdbg_spm_regs.MPM2_MPM_SLEEP_TIMETICK_COUNT_VAL = *((uint32 *)0x4A3000);

	if (total_cluster_count > 1) 
	{
		/* Logging SPM status & SPM control for power cluster CPU's */  
		sysdbg_spm_regs.APCS_ALIASn_SAW2_SPM_STS[0] = HWIO_IN(APCS_ALIAS0_SAW2_SPM_STS);
		sysdbg_spm_regs.APCS_ALIASn_SAW2_SPM_STS[1] = HWIO_IN(APCS_ALIAS1_SAW2_SPM_STS);
		sysdbg_spm_regs.APCS_ALIASn_SAW2_SPM_STS[2] = HWIO_IN(APCS_ALIAS2_SAW2_SPM_STS);
		sysdbg_spm_regs.APCS_ALIASn_SAW2_SPM_STS[3] = HWIO_IN(APCS_ALIAS3_SAW2_SPM_STS);
		
		sysdbg_spm_regs.APCS_ALIASn_SAW2_SPM_CTL[0] = HWIO_IN(APCS_ALIAS0_SAW2_SPM_CTL);
		sysdbg_spm_regs.APCS_ALIASn_SAW2_SPM_CTL[1] = HWIO_IN(APCS_ALIAS1_SAW2_SPM_CTL);
		sysdbg_spm_regs.APCS_ALIASn_SAW2_SPM_CTL[2] = HWIO_IN(APCS_ALIAS2_SAW2_SPM_CTL);
		sysdbg_spm_regs.APCS_ALIASn_SAW2_SPM_CTL[3] = HWIO_IN(APCS_ALIAS3_SAW2_SPM_CTL);

		/* Logging Power status & Power gate status for power cluster CPU's */
		sysdbg_spm_regs.APCS_ALIASn_APC_PWR_STATUS[0] = HWIO_IN(APCS_ALIAS0_APC_PWR_STATUS);
		sysdbg_spm_regs.APCS_ALIASn_APC_PWR_STATUS[1] = HWIO_IN(APCS_ALIAS1_APC_PWR_STATUS);
		sysdbg_spm_regs.APCS_ALIASn_APC_PWR_STATUS[2] = HWIO_IN(APCS_ALIAS2_APC_PWR_STATUS);
		sysdbg_spm_regs.APCS_ALIASn_APC_PWR_STATUS[3] = HWIO_IN(APCS_ALIAS3_APC_PWR_STATUS);

		sysdbg_spm_regs.APCS_ALIASn_APC_PWR_GATE_STATUS[0] = HWIO_IN(APCS_ALIAS0_APC_PWR_GATE_STATUS);
		sysdbg_spm_regs.APCS_ALIASn_APC_PWR_GATE_STATUS[1] = HWIO_IN(APCS_ALIAS1_APC_PWR_GATE_STATUS);
		sysdbg_spm_regs.APCS_ALIASn_APC_PWR_GATE_STATUS[2] = HWIO_IN(APCS_ALIAS2_APC_PWR_GATE_STATUS);
		sysdbg_spm_regs.APCS_ALIASn_APC_PWR_GATE_STATUS[3] = HWIO_IN(APCS_ALIAS3_APC_PWR_GATE_STATUS);

		/* Logging SPM status & SPM control for power cluster L2 */
		sysdbg_spm_regs.APCLUS0_L2_SAW2_SPM_STS = HWIO_IN(APCLUS0_L2_SAW2_SPM_STS);
		sysdbg_spm_regs.APCLUS0_L2_SAW2_SPM_CTL = HWIO_IN(APCLUS0_L2_SAW2_SPM_CTL);
		
		/* Logging L2 Power status of power cluster */
		sysdbg_spm_regs.APCS_ALIASn_L2_PWR_STATUS[0] = HWIO_IN(APCS_ALIAS0_L2_PWR_STATUS);
		
	  }
	 
	  /* Logging SPM status & SPM control for performance cluster CPU's */
	  sysdbg_spm_regs.APCS_ALIASn_SAW2_SPM_STS[4] = HWIO_IN(APCS_ALIAS4_SAW2_SPM_STS);
	  sysdbg_spm_regs.APCS_ALIASn_SAW2_SPM_STS[5] = HWIO_IN(APCS_ALIAS5_SAW2_SPM_STS);
	  sysdbg_spm_regs.APCS_ALIASn_SAW2_SPM_STS[6] = HWIO_IN(APCS_ALIAS6_SAW2_SPM_STS);
	  sysdbg_spm_regs.APCS_ALIASn_SAW2_SPM_STS[7] = HWIO_IN(APCS_ALIAS7_SAW2_SPM_STS);
	  
	  sysdbg_spm_regs.APCS_ALIASn_SAW2_SPM_CTL[4] = HWIO_IN(APCS_ALIAS4_SAW2_SPM_CTL);
	  sysdbg_spm_regs.APCS_ALIASn_SAW2_SPM_CTL[5] = HWIO_IN(APCS_ALIAS5_SAW2_SPM_CTL);
	  sysdbg_spm_regs.APCS_ALIASn_SAW2_SPM_CTL[6] = HWIO_IN(APCS_ALIAS6_SAW2_SPM_CTL);
	  sysdbg_spm_regs.APCS_ALIASn_SAW2_SPM_CTL[7] = HWIO_IN(APCS_ALIAS7_SAW2_SPM_CTL);

	  /* Logging Power status & Power gate status for performance cluster CPU's */
	  sysdbg_spm_regs.APCS_ALIASn_APC_PWR_STATUS[4] = HWIO_IN(APCS_ALIAS4_APC_PWR_STATUS);
	  sysdbg_spm_regs.APCS_ALIASn_APC_PWR_STATUS[5] = HWIO_IN(APCS_ALIAS5_APC_PWR_STATUS);
	  sysdbg_spm_regs.APCS_ALIASn_APC_PWR_STATUS[6] = HWIO_IN(APCS_ALIAS6_APC_PWR_STATUS);
	  sysdbg_spm_regs.APCS_ALIASn_APC_PWR_STATUS[7] = HWIO_IN(APCS_ALIAS7_APC_PWR_STATUS);

	  sysdbg_spm_regs.APCS_ALIASn_APC_PWR_GATE_STATUS[4] = HWIO_IN(APCS_ALIAS4_APC_PWR_GATE_STATUS);
	  sysdbg_spm_regs.APCS_ALIASn_APC_PWR_GATE_STATUS[5] = HWIO_IN(APCS_ALIAS5_APC_PWR_GATE_STATUS);
	  sysdbg_spm_regs.APCS_ALIASn_APC_PWR_GATE_STATUS[6] = HWIO_IN(APCS_ALIAS6_APC_PWR_GATE_STATUS);
	  sysdbg_spm_regs.APCS_ALIASn_APC_PWR_GATE_STATUS[7] = HWIO_IN(APCS_ALIAS7_APC_PWR_GATE_STATUS);

	  /* Logging SPM status & SPM control for performance cluster L2 */
	  sysdbg_spm_regs.APCS_CLUS1_L2_SAW2_SPM_STS = HWIO_IN(APCS_CLUS1_L2_SAW2_SPM_STS);
	  sysdbg_spm_regs.APCS_CLUS1_L2_SAW2_SPM_CTL = HWIO_IN(APCS_CLUS1_L2_SAW2_SPM_CTL);
	  
	  /* Logging L2 Power status of performance cluster */
	  sysdbg_spm_regs.APCS_ALIASn_L2_PWR_STATUS[1] = HWIO_IN(APCS_ALIAS1_L2_PWR_STATUS);

	  /* Logging SPM status & SPM control for CCI */
	  sysdbg_spm_regs.CCI_SAW2_SPM_STS = HWIO_IN(CCI_SAW2_SPM_STS);
	  sysdbg_spm_regs.CCI_SAW2_SPM_CTL = HWIO_IN(CCI_SAW2_SPM_CTL);

}

/*===========================================================================
**  Function :  SYSDBG_DUMPS_STATS_INIT
** ==========================================================================
*/
/*!
*
* @brief
* 	API to get chip reset reason
* 	Watchdog reset - 0x3 - supported
*
* @param[in]
*  None
*
* @par Dependencies
*   None
*
* @retval
*   Boolean
*
* @par Side Effects
*   None
*
*/
static boolean sysdbg_dump_stats_init_flat_mapped(void);

boolean sysdbg_dump_stats_init(void)
{
	static boolean init = FALSE;
	static uint32 reset_reason[6] ={};

	uint32 temp = 0, i = 0 ;

	if (tzbsp_sysdbg_flat_mapped())
	{
		return sysdbg_dump_stats_init_flat_mapped();
	}

	if (!init)
	{
		// Set the debug timer after which controller can reset the system
		// 256 sclk cycles = ~ 7.8ms


		// Enable MPM timer to start the parent timer
		HWIO_OUTF(MPM2_MPM_CONTROL_CNTCR, EN, 0x1);

		// We are going to count at XO speed. Configure
		// for XO = 19.2 MHz
		HWIO_OUTF(APCS_QTMR0_QTMR_AC_CNTFRQ, CNTFRQ, 19200000);
		busywait_init();
		// We need to know the PMIC reset reason as well
		// To be able to talk to the PMIC, SPMI init needs to
		// happen and data needs to be saved. Hence, doing it here
		//SPMIFW_Init();

		/* MSM Reset Reasoninit	*/
		reset_reason[0] = HWIO_IN(GCC_RESET_STATUS);
		// Other reset reasons
		// PON Reason 1 - 0x08
		PM_ReadPerphData(PONA, 0x08, &temp);
		reset_reason[1] |= temp << 8;
		//Warm Reset Reason 1 - 0x0A
		PM_ReadPerphData(PONA, 0x0A, &temp);
		//Warm Reset Reason 2 - 0x0B
		PM_ReadPerphData(PONA, 0x0B, &reset_reason[2]);
		reset_reason[2] |= temp << 8;
		// POFF Reason 1 - 0x0C
		PM_ReadPerphData(PONA, 0xC, &temp);
		// POFF Reason 2 - 0x0D
		PM_ReadPerphData(PONA, 0xD, &reset_reason[3]);
		reset_reason[3] |= temp << 8;
		
		// PON PBL STATUS
		PM_ReadPerphData(PONA, 0x07, &temp);
		reset_reason[4] |= temp;
		// PS_HOLD_RESET_CTL
		PM_ReadPerphData(PONA, 0x5A, &temp);
		reset_reason[4] |= temp << 8;
		// PS_HOLD_RESET_CTL2
		PM_ReadPerphData(PONA, 0x5B, &temp);
		reset_reason[4] |= temp << 16;
		
		// Debug Enhancement to Log RTC time. 
		for(i=0; i<4; i++)
		{
			temp = 0;
			reset_reason[5] <<= 8;
			PM_ReadPerphData(RTCA_WR, 0x4B-i, &temp);
			reset_reason[5] |= temp;
		}
		

		// Now populate our dump structure
		sysdbgDumpStats(reset_reason);
		
		/* Call sysdbg_dump_spm_regs to dump SPM registers */
		sysdbg_dump_spm_regs();

		init = TRUE;
	}

	return init;
}



/*===========================================================================
**  Function :  sysdbg_GET_SHARED_IMEM
** ==========================================================================
*/
/*!
*
* @brief
* 	API to get the location of shared imem
*
* @param[in]
*  None
*
* @par Dependencies
*   None
*
* @retval
*   None
*
* @par Side Effects
*   None
*
*/

uint8 *sysdbg_get_shared_imem(void)
{
	return ((uint8 *)(SHARED_IMEM_BASE));
}

/*===========================================================================
**  Function :  SYSDBG_DEBUG_UI_EN
** ==========================================================================
*/
/*!
*
* @brief
* Initializes DEBUG UI hardware block
*
* @param[in]
*  None. Current CPU number is obtained from TZ API
*
* @par Dependencies
*   Nonvoide
*
* @retval
*   None
*
* @par Side Effects
*   None
*
*/
static boolean sysdbg_debug_ui_en_flat_mapped(void);

boolean sysdbg_debug_ui_en(void)
{
	if (tzbsp_sysdbg_flat_mapped())
	{
		return sysdbg_debug_ui_en_flat_mapped();
	}

    HWIO_OUTF(GCC_QDSS_DAP_AHB_CBCR, CLK_ENABLE, 0x1);
	HWIO_OUTF(GCC_QDSS_CFG_AHB_CBCR,CLK_ENABLE, 0x1);
	HWIO_OUTF(GCC_QDSS_DAP_CBCR, CLK_ENABLE, 0x1);

	HWIO_OUT(QDSS_WRAPPER_CS_DEBUG_UI_LOCKACCESS, 0xC5ACCE55);
	HWIO_OUTF(QDSS_WRAPPER_DEBUG_UI_SECURE, SEC_CTL, 0x1);
	HWIO_OUT(QDSS_WRAPPER_DEBUG_UI_CTL, 0x1);      // Clear all status
	HWIO_OUT(QDSS_WRAPPER_DEBUG_UI_CTL_MASK, 0x0);     // Clear SW_TRIG_MASK
	return TRUE;
}

/*===========================================================================
**  Function :  SYSDBG_ENABLE
** ==========================================================================
*/
/*!
*
* @brief
* The function that enables the debug through watchdog feature
*
* @param[in]
*   None
*
* @par Dependencies
*   None
*
* @retval
*   None
*
* @par Side Effects
*   None
*
*/
static boolean sysdbg_enable_flat_mapped(void);

boolean sysdbg_enable(void)
{
	if (tzbsp_sysdbg_flat_mapped())
	{
		return sysdbg_enable_flat_mapped();
	}

	// We need to enable PROC_HALT_EN so that MSM pulls up PS_HOLD after reset
	//HWIO_OUTF(GCC_RESET_DEBUG, PROC_HALT_EN, 0x1);

	// Set the debug timer after which controller can reset the system
	// 256 sclk cycles = ~ 7.8ms
	HWIO_OUTF(GCC_RESET_DEBUG, PRE_ARES_DEBUG_TIMER_VAL, 0x100);
	// Enable / Disable the debug through watchdog feature
	HWIO_OUTF(GCC_RESET_DEBUG, SECURE_WDOG_DEBUG_EN, 0x1);
	HWIO_OUTF(GCC_RESET_DEBUG, MSM_TSENSE_RESET_DEBUG_EN, 0x1);
	HWIO_OUTF(GCC_RESET_DEBUG, PMIC_ABNORMAL_RESET_DEBUG_EN, 0x1);

    // Set the ETR timer to flush data out
	HWIO_OUT(GCC_FLUSH_ETR_DEBUG_TIMER, 0x180);
	//Set STOP_CAPTURE_DEBUG_TIMER to 2ms
	HWIO_OUT(GCC_STOP_CAPTURE_DEBUG_TIMER, 0x40);
	// Flag to enable Debug
	HWIO_OUTM(TCSR_RESET_DEBUG_SW_ENTRY, 0x1, 0X1);
	
	sysdbg_debug_ui_en();
	return TRUE;
}

/*===========================================================================
**  Function :  SYSDBGREADCOREPC
** ==========================================================================
*/
/*!
* 
* @brief
* 	API to read the PC of a particular apps core from Debug UI
* 
* @param[in] 
*  sysdbg cpu 64 bit structure and cpu number
*  
* @par Dependencies
*   None
* 
* 
* @par Side Effects
*   None
* 
*/
/*static*/ void SYSDBGReadCorePCFlatMapped(sysdbg_cpu64_ctxt_regs_type *sysdbg_ctxt_regs, uint32 cpu);

void SYSDBGReadCorePC(sysdbg_cpu64_ctxt_regs_type *sysdbg_ctxt_regs, uint32 cpu)
{

	if (tzbsp_sysdbg_flat_mapped())
	{
		SYSDBGReadCorePCFlatMapped(sysdbg_ctxt_regs, cpu);
		return;
	}

	/* For 8936 quadcore, the core numbers are 0,1,2,3 but the registers to be read are from core 4,5,6,7 (performance cluster) */
	if (total_cluster_count == 1)
	{
		sysdbg_ctxt_regs->pc = HWIO_INI(QDSS_WRAPPER_DEBUG_UI_DATA_n, ((cpu+4)*0x4));	//reading PC for current core from DEBUG_UI
		sysdbg_ctxt_regs->pc = ((uint64)(HWIO_INI(QDSS_WRAPPER_DEBUG_UI_DATA_n, ((cpu+4)*0x4+3)))<<0x20)|sysdbg_ctxt_regs->pc;	//reading PC for current core from DEBUG_UI
	}
	/* For 8939 Octa core read the DEBUG UI directly */
	else
	{
		sysdbg_ctxt_regs->pc = HWIO_INI(QDSS_WRAPPER_DEBUG_UI_DATA_n, (cpu*0x4));	//reading PC for current core from DEBUG_UI
		sysdbg_ctxt_regs->pc = ((uint64)(HWIO_INI(QDSS_WRAPPER_DEBUG_UI_DATA_n, (cpu*0x4+3)))<<0x20)|sysdbg_ctxt_regs->pc;	//reading PC for current core from DEBUG_UI
	}
}

/*===========================================================================
**  Function :  SYSDBGGETCORESTATUS
** ==========================================================================
*/
/*!
* 
* @brief
* 	API to get the status of a particular apps core 
* 
* @param[in] 
*  Core whose status has to be detected
*  
* @par Dependencies
*   None
* 
* @retval
*   Boolean to indicate online status
* 
* @par Side Effects
*   None
* 
*/
static boolean SYSDBGGetCoreStatus_flat_mapped(uint32 corenum);

boolean SYSDBGGetCoreStatus(uint32 corenum)
{

    uint32 *reg = NULL;

    if (tzbsp_sysdbg_flat_mapped())
    {
	    return SYSDBGGetCoreStatus_flat_mapped(corenum);
    }

    /* Check the input */
    if (corenum > total_cpu_count)
        return FALSE;
	
	/* Changes to support 8936 Quad core */
    if (total_cluster_count == 1) 
	{
		/* Get the status */
		reg = (uint32 *)((HWIO_APCS_ALIAS4_APC_PWR_STATUS_ADDR + (APPS_ALIAS_SIZE * corenum )));
		return  ((*reg & (HWIO_APCS_ALIAS4_APC_PWR_STATUS_GDHS_STS_BMSK)) >> HWIO_APCS_ALIAS4_APC_PWR_STATUS_GDHS_STS_SHFT);
	}
	/* 8939 Octa core */ 
	else
	{	
		if (corenum >= 4)
		{	
			/* Get the status */
			reg = (uint32 *)((HWIO_APCS_ALIAS4_APC_PWR_STATUS_ADDR + (APPS_ALIAS_SIZE * (corenum-4) )));
			return  ((*reg & (HWIO_APCS_ALIAS4_APC_PWR_STATUS_GDHS_STS_BMSK)) >> HWIO_APCS_ALIAS4_APC_PWR_STATUS_GDHS_STS_SHFT);
		}
		else
		{
			reg = (uint32 *)(HWIO_APCS_ALIAS0_APC_PWR_STATUS_ADDR + (APPS_ALIAS_SIZE * corenum));
			return (*reg & (HWIO_APCS_ALIAS0_APC_PWR_STATUS_GDHS_STS_BMSK)) >> HWIO_APCS_ALIAS0_APC_PWR_STATUS_GDHS_STS_SHFT;
		}
	}
}

/*===========================================================================
**  Function :  SYSDBGGETCLUSTERL2VALIDSTATUS
** ==========================================================================
*/
/*!
* 
* @brief
* 	API to get the valid status of a particular cluster's L2 i.e. cache data is
* 	valid or not
* 
* @param[in] 
*  Cluster whose L2 status has to be detected
*  
* @par Dependencies
*   None
* 
* @retval
*   Boolean to indicate online status
* 
* @par Side Effects
*   None
* 
*/
#define L2_OFFSET      (2 * 0x1000)

static boolean SYSDBGGetClusterL2SValidStatus_flat_mapped(uint32 clusternum);

boolean SYSDBGGetClusterL2SValidStatus(uint32 clusternum)
{
    uint32 *reg = NULL;
    boolean l2_valid = FALSE;

    if (tzbsp_sysdbg_flat_mapped())
    {
	    return SYSDBGGetClusterL2SValidStatus_flat_mapped(clusternum);
    }

        /* Check the input */
    if (clusternum > total_cluster_count)
        return FALSE;
		
	/* Changes to support 8936 Quad core */
    if (total_cluster_count == 1) 
	{
		/* Check that for valid L2 cache using HS_STS. */
		reg  = (uint32 *)(HWIO_APCS_ALIAS1_L2_PWR_STATUS_ADDR);
		l2_valid = (*reg & (HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_HS_STS_BMSK)) >> HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_HS_STS_SHFT;
	}
	/* 8939 Octacore support */
	else
	{
		if (clusternum == 0)
		{
			/* Check that for valid L2 cache using HS_STS. */
			reg  = (uint32 *)(HWIO_APCS_ALIAS0_L2_PWR_STATUS_ADDR);
			l2_valid = (*reg & (HWIO_APCS_ALIAS0_L2_PWR_STATUS_L2_HS_STS_BMSK)) >> HWIO_APCS_ALIAS0_L2_PWR_STATUS_L2_HS_STS_SHFT ;
		}
		else
		{
			/* Check that for valid L2 cache using HS_STS. */
			reg  = (uint32 *)(HWIO_APCS_ALIAS1_L2_PWR_STATUS_ADDR);
			l2_valid = (*reg & (HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_HS_STS_BMSK)) >> HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_HS_STS_SHFT;
		}
	}
	
    return l2_valid;
}

/*===========================================================================
**  Function :  SYSDBGGETNXTACTCORE
** ==========================================================================
*/
/*!
* 
* @brief
* 	API to get the next active core in a cluster
* 
* @param[in] 
*  Core from which the search will start i.e. search all cores to core n to 
*  the max number of cores in cluster and return the first active one
*  
* @par Dependencies
*   Core whose number is provided as input is known to be alive. That will be 
*   the return value if no other active cores are found.
* 
* @retval
*   Boolean to indicate online status
* 
* @par Side Effects
*   None
* 
*/

uint32 SYSDBGGetNxtActCore(uint32 corenum)
{
    uint32 coreloop = corenum + 1, max_cores = total_cpu_count;
	boolean core_alive = 0 ;

    if (corenum >= max_cores)
        return FALSE;
	
	/* Specific to 8936 Quad core  */
	if (total_cluster_count == 1)
	{
		/* Find the first core which was alive*/
		for(; coreloop < max_cores; coreloop++)
		{
			core_alive = SYSDBGGetCoreStatus(coreloop);

			/* If alive, done*/
			if (core_alive)
            break;
		}

		if (coreloop == max_cores)
			return corenum ;
		else
			return coreloop;
	}
	/* specific to 8939 Octa core */
	else
    {
		if (coreloop == total_cpu_count)
			coreloop = 0;
		/* we never get a condition where next_core will be 4 */ 
        if (coreloop > 4)
        {
			/* Find the first core which was alive*/
			for(; coreloop < max_cores; coreloop++)
			{
				core_alive = SYSDBGGetCoreStatus(coreloop);

				/* If alive, done*/
				if (core_alive)
					break;
			}
		}
		if ((coreloop == total_cpu_count) && !core_alive)
            coreloop = 0;
		
		if (coreloop < 4)
		{
			/* Find the first core which was alive*/
			for(; coreloop < TARGET_CLUSTER_0_SIZE; coreloop++)
			{
				core_alive = SYSDBGGetCoreStatus(coreloop);

				/* If alive, done*/
				if (core_alive)
					break;
			}
		}
		if (coreloop == TARGET_CLUSTER_0_SIZE)
		{
		    if (Secondary_cluster_online_cpu_count == 0)
				return coreloop;
			else
				return corenum;
		}
		else
			return coreloop;
		
	}
}

/*===========================================================================
**  Function :  SYSDBGGETCLUSTERL2SPCSTATUS
** ==========================================================================
*/
/*!
* 
* @brief
* 	API to get the valid status of a particular cluster's L2 i.e. cache data is
* 	valid or not
* 
* @param[in] 
*  None
*  
* @par Dependencies
*   None
* 
* @retval
*   Boolean to indicate online status
* 
* @par Side Effects
*   None
* 
*/

static boolean SYSDBGGetClusterL2SPCStatus_flat_mapped(void);

boolean SYSDBGGetClusterL2SPCStatus(void)
{

    uint32 *reg = NULL;
    boolean l2online = FALSE, L2HSStatus = 0, L2ARRAYHSStatus = 0;

    if (tzbsp_sysdbg_flat_mapped())
    {
	    return SYSDBGGetClusterL2SPCStatus_flat_mapped();
    }

    /* Check that for valid L2 of secondary cluster using HS Status and HS Array status, if any one of these bits is set bring the L2 up */
	reg  = (uint32 *)(HWIO_APCS_ALIAS0_L2_PWR_STATUS_ADDR);
	L2HSStatus = (*reg & (HWIO_APCS_ALIAS0_L2_PWR_STATUS_L2_HS_STS_BMSK)) >> HWIO_APCS_ALIAS0_L2_PWR_STATUS_L2_HS_STS_SHFT;
	L2ARRAYHSStatus = (*reg & (HWIO_APCS_ALIAS0_L2_PWR_STATUS_L2_ARRAY_HS_STS_BMSK)) >> HWIO_APCS_ALIAS0_L2_PWR_STATUS_L2_ARRAY_HS_STS_SHFT;

    l2online = (L2HSStatus || L2ARRAYHSStatus);

    return l2online;
}

/*===========================================================================
**  Function :  SYSDBGGETCORESINCLUSTER
** ==========================================================================
*/
/*!
* 
* @brief
* 	API to get cores in a cluster
* 	 
* @param[in] 
*  Cluster for which info has to be returned
*  
* @par Dependencies
*   None
* 
* @retval
*   Unsigned integer to indicate the number of cores
* 
* @par Side Effects
*   None
* 
*/
uint32 SYSDBGGetCoresInCluster(uint32 clusternum)
{
    /* Check the input */
    if (clusternum >= TARGET_CLUSTERS)
        return FALSE;

    switch (clusternum)
    {
        case 0:
            return TARGET_CLUSTER_0_SIZE;
            break;
        case 1:
            return TARGET_CLUSTER_1_SIZE;
            break;
        default:
            return 0xFFFF;
            break;
    }
}


/*===========================================================================
**  Function :  SYSDBG_NORMAL_CORE_RESET
** ==========================================================================
*/
/*!
* 
* @brief
*   This function handles the normal reset of a core in debug path 
*     
* @param[in] 
*  CPU number of the core to be reset
*    
* @par Dependencies
*  None
* 
* @retval
*   None
* 
* @par Side Effects
*   None
* 
*/
static void SYSDBGNormalCoreReset_flat_mapped (uint32 core_num);

void SYSDBGNormalCoreReset (uint32 core_num)
{
    volatile uint32 *reg;
	uint32 offset = 0;
	 
	if (tzbsp_sysdbg_flat_mapped())
	{
		SYSDBGNormalCoreReset_flat_mapped(core_num);
		return;
	}
		 
    /* 8936 Quad core support */
    if (total_cluster_count == 1) 
    {
        offset = (APPS_ALIAS_SIZE * core_num);

		reg = (uint32 *)(HWIO_APCS_ALIAS4_CPU_PWR_CTL_ADDR + offset);
		*reg = 0x0000001D;

		reg = (uint32 *)(HWIO_APCS_ALIAS4_APC_PWR_GATE_CTL_ADDR + offset);
		*reg = 0x10000001;

		busywait(2);
		
		reg = (uint32 *)(HWIO_APCS_ALIAS4_CPU_PWR_CTL_ADDR + offset); 
		*reg = 0x0002001C;
		
		busywait(2);

		reg = (uint32 *)(HWIO_APCS_ALIAS4_CPU_PWR_CTL_ADDR + offset);
		*reg = 0x0002000C;

		reg = (uint32 *)(HWIO_APCS_ALIAS4_CPU_PWR_CTL_ADDR + offset);
		*reg = 0x0002008C;

    }/* End of changes pertaining to 8936 quadcore */
    /* 8939 Octa core specific */
    else 
    {
        if (core_num < 4) 
        {
			Secondary_cluster_online_cpu_count++;
            offset = APPS_ALIAS_SIZE * core_num;
            reg = (uint32 *)(HWIO_APCS_ALIAS0_CPU_PWR_CTL_ADDR + offset);
            *reg = 0x0000001D;

            reg = (uint32 *)(HWIO_APCS_ALIAS0_APC_PWR_GATE_CTL_ADDR + offset);
            *reg = 0x10000001;

            busywait(2);
            
            reg = (uint32 *)(HWIO_APCS_ALIAS0_CPU_PWR_CTL_ADDR + offset); 
            *reg = 0x0002001C;
            
            busywait(2);

            reg = (uint32 *)(HWIO_APCS_ALIAS0_CPU_PWR_CTL_ADDR + offset);
            *reg = 0x0002000C;

            reg = (uint32 *)(HWIO_APCS_ALIAS0_CPU_PWR_CTL_ADDR + offset);
            *reg = 0x0002008C;
            
        }
        else
        {
            offset = (APPS_ALIAS_SIZE * (core_num-4));

            reg = (uint32 *)(HWIO_APCS_ALIAS4_CPU_PWR_CTL_ADDR + offset);
            *reg = 0x0000001D;

            reg = (uint32 *)(HWIO_APCS_ALIAS4_APC_PWR_GATE_CTL_ADDR + offset);
            *reg = 0x10000001;

            busywait(2);
            
            reg = (uint32 *)(HWIO_APCS_ALIAS4_CPU_PWR_CTL_ADDR + offset); 
            *reg = 0x0002001C;
            
            busywait(2);

            reg = (uint32 *)(HWIO_APCS_ALIAS4_CPU_PWR_CTL_ADDR + offset);
            *reg = 0x0002000C;

            reg = (uint32 *)(HWIO_APCS_ALIAS4_CPU_PWR_CTL_ADDR + offset);
            *reg = 0x0002008C;
        }
    }
}

/*===========================================================================
**  Function :  SYSDBG_CORE_RESET_SPI
** ==========================================================================
*/
/*!
* 
* @brief
* The function does the core wake up if L2 of secondary cluster is in 
* dormant/standby mode and all cores in that cluster are in power collapse
* we need to wake up one core of secondary cluster to flush its L2 contents.
* 
* @param[in] 
*  Core number to be woken up
*  
* @par Dependencies
*   None
* 
* @retval
*   None
* 
* @par Side Effects
*   None
* 
*/
static void SYSDBGCoreResetSPI_flat_mapped(uint32 core_num);

void SYSDBGCoreResetSPI(uint32 core_num)
{
	volatile uint32 *reg;
	HAL_qgic_BaseAddressType base;

	if (tzbsp_sysdbg_flat_mapped())
	{
		SYSDBGCoreResetSPI_flat_mapped(core_num);
		return;
	}

	base.nGICDBaseAddress = 0xb000000;
	base.nGICCBaseAddress = 0xb002000;
	base.nGICHBaseAddress = 0xb001000;
	base.nGICVBaseAddress = 0xb004000;
	
	/* Update Secondary cluster OL core count, so that control will not come here again */
	Secondary_cluster_online_cpu_count++;
	
	/* Execute the core warm boot wake up sequence for core 0 */
	reg = (uint32 *)(HWIO_APCS_ALIAS0_CPU_PWR_CTL_ADDR);
	*reg = 0x0000001D;

	reg = (uint32 *)(HWIO_APCS_ALIAS0_APC_PWR_GATE_CTL_ADDR);
	*reg = 0x10000001;

	busywait(2);
				
	reg = (uint32 *)(HWIO_APCS_ALIAS0_CPU_PWR_CTL_ADDR); 
	*reg = 0x0002001C;
				
	busywait(2);

	reg = (uint32 *)(HWIO_APCS_ALIAS0_CPU_PWR_CTL_ADDR);
	*reg = 0x0002000C;

	reg = (uint32 *)(HWIO_APCS_ALIAS0_CPU_PWR_CTL_ADDR);
	*reg = 0x0002008C;
	
	/* Initialize QGIC */
	HAL_qgic_Init(&base);
	HAL_qgic_Reset();
	HAL_qgic_SetPriorityMask(HAL_QGIC_PRIORITY_LOWEST);
	HAL_qgic_EnableDistributor();
	HAL_qgic_EnableInterrupts();
	HAL_qgic_EnableDistributorNS();
	HAL_qgic_EnableInterruptsNS();
	HAL_qgic_SetSecureAckNS(TRUE);
	  
	/* Enable the QTIMER interrupt */
	HAL_qgic_SetSecurity(HAL_QGIC_QTMR_QGICFRM0PHYIRQ, HAL_QGIC_SECURE);
	HAL_qgic_SetPriority(HAL_QGIC_QTMR_QGICFRM0PHYIRQ, HAL_QGIC_PRIORITY_HIGHEST);
	//HAL_qgic_SetTrigger (HAL_QGIC_QTMR_QGICFRM0PHYIRQ, pInt_info->eTrigger);
	HAL_qgic_SetTargets (HAL_QGIC_QTMR_QGICFRM0PHYIRQ, (1 << core_num));
	HAL_qgic_Enable(HAL_QGIC_QTMR_QGICFRM0PHYIRQ);
		
	//Enable input to the counter
	HWIO_OUT(MPM2_MPM_CONTROL_CNTCR,0x1);

	//Set matching count
	HWIO_OUT(APCS_QTMR0_F0V1_QTMR_V1_CNTP_TVAL,0x50);

	//Enable the counter and interrupt                                         
	HWIO_OUT(APCS_QTMR0_F0V1_QTMR_V1_CNTP_CTL,0x01);
	
}

/*===========================================================================
**  Function :  SYSDBG_NORMAL_L2_RESET
** ==========================================================================
*/
/*!
* 
* @brief
*   This function handles the normal reset of an L2 block in debug path 
*     
* @param[in] 
*  CPU number of the core to be reset
*    
* @par Dependencies
*  None
* 
* @retval
*   None
* 
* @par Side Effects
*   None
* 
*/
static void SYSDBGNormalL2Reset_flat_mapped(void);

static void SYSDBGNormalL2Reset(void)
{
	if (tzbsp_sysdbg_flat_mapped())
	{
		SYSDBGNormalL2Reset_flat_mapped();
		return;
	}

    // Close L1/SCU logic GDHS(Powerup), Wakeup L2/SCU RAMs ; ie. deassert sleep signals
    // Write 0x00101707 to APCS_ALIAS0_L2_PWR_CTL (0x0B11_1014)
    // Wait 2ms (delay loop)
    HWIO_OUT(APCS_ALIAS0_L2_PWR_CTL, 0x00101707);
    busywait(2);
    
    // Enable Clocks using SW CLK EN
    // Write 0x00000001 to APCS_ALIAS0_CORE_CBCR (0x0B11_1058)
    HWIO_OUT(APCS_ALIAS0_CORE_CBCR, 0x00000001);
        
    // De-assert L2/SCU logic clamp
    // Write 0x00101607 to APCS_ALIAS0_L2_PWR_CTL (0x0B11_1014)
    // Wait 1.2ms (delay loop)
    HWIO_OUT(APCS_ALIAS0_L2_PWR_CTL, 0x00101607);
    busywait(2);
    
    // De-assert L2/SCU logic reset
    // Write 0x00100207 to APCS_ALIAS0_L2_PWR_CTL (0x0B11_1014)
    // Wait 2ms (delay loop)
    HWIO_OUT(APCS_ALIAS0_L2_PWR_CTL, 0x00100207);
    busywait(2);    
    
    // Set PMIC APC on
    // Write 0x10100207 to APCS_ALIAS0_L2_PWR_CTL (0x0B11_1014)
    HWIO_OUT(APCS_ALIAS0_L2_PWR_CTL, 0x10100207);
    
    // Set HW CLK CTL for the CBC block
    // Write 0x00000003 to APCS_ALIAS0_CORE_CBCR (0x0B11_1058)
    HWIO_OUT(APCS_ALIAS0_CORE_CBCR, 0x00000003);
}


/*===========================================================================
**  Function :  SYSDBG_RESET_NEXT_CORE
** ==========================================================================
*/
/*!
*
* @brief
* The function that pulls the next CPU core out of reset. This reset sequence
* is for CORTEX-A53
*
* @param[in]
*  None. Current CPU number is obtained from TZ API
*
* @par Dependencies
*   None
*
* @retval
*   None
*
* @par Side Effects
*   None
*
*/
static boolean sysdbg_reset_next_core_flat_mapped(void);

static void (*func_ptr)(uint32,uint64);

boolean sysdbg_reset_next_core(void)
{
	#define APPS_ALIAS_SIZE		0x10000
	uint32 next_act_core = 0, cluster = 0;
	volatile uint32 *reg;
	
	static boolean L2PowerCollapsed[] = { TRUE, FALSE };
	static boolean L2Init[] = { FALSE, FALSE };
    	
	static uint32 memcpy_to_rpm_coderam = 0x0; 
	
	if (tzbsp_sysdbg_flat_mapped()) 
	{ 
		return sysdbg_reset_next_core_flat_mapped();
	} 

 	/* Copy DDR self refrsh code to RPM code RAM and update the status */
	if(!memcpy_to_rpm_coderam)
	{
		//Using ENTRY point of SYSDBG in RPM CODERAM 
		func_ptr = (void (*)(uint32, uint64))(HWIO_IN(TCSR_RESET_DEBUG_SW_ENTRY)|0x1);//Using ENTRY point of SYSDBG in RPM CODERAM		 
		void *code_src = (void*)((uint32)ddr_enter_self_refresh_and_reset&0xFFFFFFFC); //current beta compiler doesn't support pragma arm or --arm_only compiler option
		void *code_dest = (void*)((uint32)func_ptr&0xFFFFFFFC); //temp solution is to force thumb code and manipulating the pointers directly
		memcpy(code_dest, code_src, 0x200);
	   	memcpy_to_rpm_coderam = 0x1;
  	}

	// Find the next core to be reset based on the status of the headswitch for the start
    next_act_core = SYSDBGGetNxtActCore(tzbsp_cur_cpu_num());
	//cluster = (next_act_core >= TARGET_CLUSTER_0_SIZE) ? 1 : 0;
	if(next_act_core >= TARGET_CLUSTER_0_SIZE)
	   cluster = 1;
	

	// Program the start address to the reset vector of TZ
	//HWIO_OUT(APCS_ALIAS1_BOOT_START_ADDR_SEC, (uint32)&Image$$TZBSP_CODE$$Base | 0x1);
     /* Need to write to 2 cluster registers for 8939 octa core */
    if (total_cluster_count > 1) 
	{
		HWIO_OUT(APCS_ALIAS0_CFG_SECURE, 0x1);
	}
	HWIO_OUT(APCS_ALIAS1_CFG_SECURE, 0x1);	
		  
/*	reg = (uint32 *)(HWIO_APCS_BOOT_START_ADDR_SEC_ADDR);
	*reg = (&Image$$TZBSP_CODE$$Base|0x1);

	reg = (uint32 *)(HWIO_APCS_CFG_SECURE_ADDR);
	*reg = 0x1;
*/

    /* Means this is the last active core */
    if (next_act_core == tzbsp_cur_cpu_num())
        return TRUE;
		
	// Check if we have reached the maximum number of cores to reset
    if (next_act_core < total_cpu_count)
    {
        /* Specific to 8936 Quad core */
		if (total_cluster_count == 1)
        {
            reg = (uint32 *)((uintptr_t)(HWIO_APCS_ALIAS1_AA64NAA32_REG_ADDR ));
			*reg = 0x1;		
		}
		/* Specific to 8939 Octa core */
		else
		{
            /* Configure the L2 for cluster 0 only if we are going to bring cluster 0 out of reset. L2 for cluster 1
             * will be brought out of PC even if it was in power collapse */
            if (((cluster == 0) && (!L2Init[cluster])) && (SYSDBGGetClusterL2SPCStatus()))
            {

                /* Bring the L2 out of reset and update state */
                 SYSDBGNormalL2Reset();
                 L2PowerCollapsed[cluster] = FALSE;
                 L2Init[cluster] = TRUE;

            }
			
            /* If the L2 for the cluster was power collapsed, skip the cluster completely. */
            if (!L2PowerCollapsed[cluster])
            {
				/* Configure it to boot up in Aarch64 */
				if (cluster == 0)
				{
					reg = (uint32 *)((uintptr_t)(HWIO_APCS_ALIAS0_AA64NAA32_REG_ADDR ));
					*reg = 0x1;
				}
				else
				{
					reg = (uint32 *)((uintptr_t)(HWIO_APCS_ALIAS1_AA64NAA32_REG_ADDR ));
					*reg = 0x1;
				}
				
            }
			/* Secondary cluster L2 needs to be flushed if it is not power collapsed */
			/* If all cores in secondary cluster (power cluster) are in power collapse */
			/* Force wake up the first core to flush L2.
               Here next_act_core is 4, because we are running on performance cluster and 
			   no other active cores in power cluster, so not to be confused by regular core4 */
			if (next_act_core == 4)
			{
				/* Check if L2 of secondary cluster(power cluster) is ON by checking power status register */
				uint32 L2PCstatus = SYSDBGGetClusterL2SValidStatus(0);
				/* If L2 is ON wake up core 0 to flush the contents */ 
				if(L2PCstatus == 0x1)
				{
					/* L2 is ONLINE, force wake up first core (core 0) in power cluster */
					next_act_core = 0;
					SYSDBGCoreResetSPI(next_act_core);
					/* Increment our lock to indicate that one more core is going to be up */
					set_global_lock(next_act_core);
				}
				return TRUE;
			}		
		}

		/* We need to check which reset sequence depending on whether core is in sleep or stuck in the winding down / wakeup path */
		/* Condition to check - clock gated (0)+ clamp enabled(4) + core in reset (3)*/
		/*reg = (uint32 *)((uintptr_t)(HWIO_APCS_ALIAS0_APC_PWR_STATUS_ADDR + (PAGE64K_OFFSET * core)));
		if (!BITGET(*reg, 27)) // | BITGET(*reg, 3) | BITGET(*reg, 4))
		SYSDBGForceCoreReset(core);
		else */
		SYSDBGNormalCoreReset(next_act_core);
     
    	 /* Increment our lock to indicate that one more core is going to be up */
		set_global_lock(next_act_core);
    }

   	return TRUE;
}

/*===========================================================================
**  Function :  SYSDBG_REGISTER_MONITOR
** ==========================================================================
*/
/*!
*
* @brief
* Register the entry handler of sysdbg in el1 for sysdbg to return to during wdog reset,
* also contains buffer for context saving to be done in el3
*
* @param[in]
*  None
*
* @par Dependencies
*   None
*
* @retval
*   None
*
* @par Side Effects
*   None
*
*/

boolean sysdbg_register_monitor(void)
{
	mon_register_el1_sysdbg_entry_and_buf((v_addr_t)sysdbg_el1_entry_handler, (v_addr_t)&mon_ctxt_dump);
	return TRUE;
}

/*===========================================================================
**  Function :  SYSDBG_GET_MON_CTXT
** ==========================================================================
*/
/*!
*
* @brief
* Gets the context buffer saved from EL3 sysdbg routine
*
* @param[in]
*  None
*
* @par Dependencies
*   None
*
* @retval
*   None
*
* @par Side Effects
*   None
*
*/

aa_64_ctx_all_els_t* sysdbg_get_mon_ctxt(void)
{
	return &mon_ctxt_dump;
}

/*===========================================================================
**  Function :  DDR_ENTER_SELF_REFRESH_AND_RESET
** ==========================================================================
*/
/*!
*
* @brief
* The function gets copied to a code region either in IMEM or RPM CODERAM for
* execution and is not called directly by sysdbg
*
* @param[in]
*  None
*
* @par Dependencies
*   None
*
* @retval
*   None
*
* @par Side Effects
*   None
*
*/
static void ddr_enter_self_refresh_and_reset_flat_mapped(uint32 cur_cpu, uint64 last_cpu);



void ddr_enter_self_refresh_and_reset(uint32 cur_cpu, uint64 last_cpu)
{
	 if (cur_cpu == last_cpu) //only reset if it is the last CPU
	{
             
        /* New sequence to be followed to put the DDR into self refresh for 8939 which has scaled code */
      	HWIO_BIMC_S_DDR0_SCMO_ADDR_MAP_CSn_OUTMI(0, HWIO_BIMC_S_DDR0_SCMO_ADDR_MAP_CSn_RANK_EN_BMSK, 0x0);
        HWIO_BIMC_S_DDR0_SCMO_ADDR_MAP_CSn_OUTMI(1, HWIO_BIMC_S_DDR0_SCMO_ADDR_MAP_CSn_RANK_EN_BMSK, 0x0);
        
        HWIO_OUTF(BIMC_S_DDR0_SHKE_SELF_REFRESH_CNTL, HW_SELF_RFSH_ENABLE_RANK0, 0x0);
        HWIO_OUTF(BIMC_S_DDR0_SHKE_SELF_REFRESH_CNTL, HW_SELF_RFSH_ENABLE_RANK1, 0x0);		
	    HWIO_OUTF(BIMC_S_DDR0_SHKE_SELF_REFRESH_CNTL, CONCURRENT_SELF_RFSH_EN, 0x0);
		
		/* Update IOC CTL CFG register which is in EBI_PHY_CFG domain */
		/* Please generate new hwio and replace the below code with hwio Macros */
		*((uint32 *)0x00480080) &= ~0x40000000;		
	   
	    HWIO_OUTF(BIMC_S_DDR0_SHKE_DRAM_MANUAL_0, ENTER_SELF_REFRESH_IDLE, 0x1);
	    while(HWIO_INF(BIMC_S_DDR0_SHKE_DRAM_MANUAL_0, ENTER_SELF_REFRESH_IDLE));
	    	
        /* Trigger for pshold reset */
        HWIO_OUTF(MPM2_MPM_PS_HOLD, PSHOLD, 0);
		/* Add dsb instruction to make sure the memory transactions are completed before pulling PS_HOLD low */
		tzbsp_dsb();
    }
    while(TRUE);
}


//#pragma arm section
/*===========================================================================
**  Function :  SYSDBG_EXIT
** ==========================================================================
*/
/*!
*
* @brief
* clean up function for sysdbg
* copies ddr_enter_self_refresh_reset into a non-ddr memory region,
* then finishes and reset
*
* @param[in]
*  None
*
* @par Dependencies
*   None
*
* @retval
*   None
*
* @par Side Effects
*   None
*
*/
static boolean sysdbg_exit_flat_mapped(void);

boolean sysdbg_exit(void)

{
	if (tzbsp_sysdbg_flat_mapped())
	{
		return sysdbg_exit_flat_mapped();
	}

	uint32 cur_cpu= tzbsp_cur_cpu_num();
	uint64 last_cpu= get_global_lock();

	if (cur_cpu == last_cpu)
	{
		HWIO_OUTF(GCC_RESET_DEBUG, SECURE_WDOG_DEBUG_EN, 0x0);
		HWIO_OUTF(GCC_RESET_DEBUG, MSM_TSENSE_RESET_DEBUG_EN, 0x0);
		HWIO_OUTF(GCC_RESET_DEBUG, PMIC_ABNORMAL_RESET_DEBUG_EN, 0x0);
		HWIO_OUT(TCSR_RESET_DEBUG_SW_ENTRY, 0x0);
		HWIO_OUTF(GCC_RESET_DEBUG, BLOCK_RESIN, 0x0);
		HWIO_OUT(GCC_RESET_STATUS, 0x2);
		(*(uint32*)SYSTEM_DEBUG_MARKER) = 0xDEADD00D;
	}
	tzbsp_isb();
	tzbsp_dsb();
	//should include a DSB & ISB before jumping to self-modified code
	func_ptr(cur_cpu, last_cpu); //never returns
	while(TRUE);
}

/*   ===========================X========== Flat Mapping when MMU disabled =========================== */
// QDSS_WRAPPER_CS_DEBUG_UI_LOCKACCESS
#undef  QDSS_WRAPPER_TOP_BASE
#define QDSS_WRAPPER_TOP_BASE   QDSS_WRAPPER_TOP_BASE_PHYS

/*static*/ void SYSDBGReadCorePCFlatMapped(sysdbg_cpu64_ctxt_regs_type *sysdbg_ctxt_regs, uint32 cpu)
{
	/* For 8936 quadcore, the core numbers are 0,1,2,3 but the registers to be read are from core 4,5,6,7 (performance cluster) */
	if (total_cluster_count == 1)
	{
		sysdbg_ctxt_regs->pc = HWIO_INI(QDSS_WRAPPER_DEBUG_UI_DATA_n, ((cpu+4)*0x4));	//reading PC for current core from DEBUG_UI
		sysdbg_ctxt_regs->pc = ((uint64)(HWIO_INI(QDSS_WRAPPER_DEBUG_UI_DATA_n, ((cpu+4)*0x4+3)))<<0x20)|sysdbg_ctxt_regs->pc;	//reading PC for current core from DEBUG_UI
	}
	/* For 8939 Octa core read the DEBUG UI directly */
	else
	{
		sysdbg_ctxt_regs->pc = HWIO_INI(QDSS_WRAPPER_DEBUG_UI_DATA_n, (cpu*0x4));	//reading PC for current core from DEBUG_UI
		sysdbg_ctxt_regs->pc = ((uint64)(HWIO_INI(QDSS_WRAPPER_DEBUG_UI_DATA_n, (cpu*0x4+3)))<<0x20)|sysdbg_ctxt_regs->pc;	//reading PC for current core from DEBUG_UI
	}
}

// APCS_ALIAS0_SAW2_SPM_STS , CCI_SAW2_REG_BASE
#undef  A53SS_BASE
#define A53SS_BASE              A53SS_BASE_PHYS

static void sysdbg_dump_spm_regs_flat_mapped(void)
{
	int i;

	/* Log MPM2 Time */
	sysdbg_spm_regs.MPM2_MPM_SLEEP_TIMETICK_COUNT_VAL = *((uint32 *)0x4A3000);

	if (total_cluster_count > 1) 
	{
		/* Logging SPM status & SPM control for power cluster CPU's */  
		sysdbg_spm_regs.APCS_ALIASn_SAW2_SPM_STS[0] = HWIO_IN(APCS_ALIAS0_SAW2_SPM_STS);
		sysdbg_spm_regs.APCS_ALIASn_SAW2_SPM_STS[1] = HWIO_IN(APCS_ALIAS1_SAW2_SPM_STS);
		sysdbg_spm_regs.APCS_ALIASn_SAW2_SPM_STS[2] = HWIO_IN(APCS_ALIAS2_SAW2_SPM_STS);
		sysdbg_spm_regs.APCS_ALIASn_SAW2_SPM_STS[3] = HWIO_IN(APCS_ALIAS3_SAW2_SPM_STS);
		
		sysdbg_spm_regs.APCS_ALIASn_SAW2_SPM_CTL[0] = HWIO_IN(APCS_ALIAS0_SAW2_SPM_CTL);
		sysdbg_spm_regs.APCS_ALIASn_SAW2_SPM_CTL[1] = HWIO_IN(APCS_ALIAS1_SAW2_SPM_CTL);
		sysdbg_spm_regs.APCS_ALIASn_SAW2_SPM_CTL[2] = HWIO_IN(APCS_ALIAS2_SAW2_SPM_CTL);
		sysdbg_spm_regs.APCS_ALIASn_SAW2_SPM_CTL[3] = HWIO_IN(APCS_ALIAS3_SAW2_SPM_CTL);

		/* Logging Power status & Power gate status for power cluster CPU's */
		sysdbg_spm_regs.APCS_ALIASn_APC_PWR_STATUS[0] = HWIO_IN(APCS_ALIAS0_APC_PWR_STATUS);
		sysdbg_spm_regs.APCS_ALIASn_APC_PWR_STATUS[1] = HWIO_IN(APCS_ALIAS1_APC_PWR_STATUS);
		sysdbg_spm_regs.APCS_ALIASn_APC_PWR_STATUS[2] = HWIO_IN(APCS_ALIAS2_APC_PWR_STATUS);
		sysdbg_spm_regs.APCS_ALIASn_APC_PWR_STATUS[3] = HWIO_IN(APCS_ALIAS3_APC_PWR_STATUS);

		sysdbg_spm_regs.APCS_ALIASn_APC_PWR_GATE_STATUS[0] = HWIO_IN(APCS_ALIAS0_APC_PWR_GATE_STATUS);
		sysdbg_spm_regs.APCS_ALIASn_APC_PWR_GATE_STATUS[1] = HWIO_IN(APCS_ALIAS1_APC_PWR_GATE_STATUS);
		sysdbg_spm_regs.APCS_ALIASn_APC_PWR_GATE_STATUS[2] = HWIO_IN(APCS_ALIAS2_APC_PWR_GATE_STATUS);
		sysdbg_spm_regs.APCS_ALIASn_APC_PWR_GATE_STATUS[3] = HWIO_IN(APCS_ALIAS3_APC_PWR_GATE_STATUS);

		/* Logging SPM status & SPM control for power cluster L2 */
		sysdbg_spm_regs.APCLUS0_L2_SAW2_SPM_STS = HWIO_IN(APCLUS0_L2_SAW2_SPM_STS);
		sysdbg_spm_regs.APCLUS0_L2_SAW2_SPM_CTL = HWIO_IN(APCLUS0_L2_SAW2_SPM_CTL);
		
		/* Logging L2 Power status of power cluster */
		sysdbg_spm_regs.APCS_ALIASn_L2_PWR_STATUS[0] = HWIO_IN(APCS_ALIAS0_L2_PWR_STATUS);
		
	  }
	 
	  /* Logging SPM status & SPM control for performance cluster CPU's */
	  sysdbg_spm_regs.APCS_ALIASn_SAW2_SPM_STS[4] = HWIO_IN(APCS_ALIAS4_SAW2_SPM_STS);
	  sysdbg_spm_regs.APCS_ALIASn_SAW2_SPM_STS[5] = HWIO_IN(APCS_ALIAS5_SAW2_SPM_STS);
	  sysdbg_spm_regs.APCS_ALIASn_SAW2_SPM_STS[6] = HWIO_IN(APCS_ALIAS6_SAW2_SPM_STS);
	  sysdbg_spm_regs.APCS_ALIASn_SAW2_SPM_STS[7] = HWIO_IN(APCS_ALIAS7_SAW2_SPM_STS);
	  
	  sysdbg_spm_regs.APCS_ALIASn_SAW2_SPM_CTL[4] = HWIO_IN(APCS_ALIAS4_SAW2_SPM_CTL);
	  sysdbg_spm_regs.APCS_ALIASn_SAW2_SPM_CTL[5] = HWIO_IN(APCS_ALIAS5_SAW2_SPM_CTL);
	  sysdbg_spm_regs.APCS_ALIASn_SAW2_SPM_CTL[6] = HWIO_IN(APCS_ALIAS6_SAW2_SPM_CTL);
	  sysdbg_spm_regs.APCS_ALIASn_SAW2_SPM_CTL[7] = HWIO_IN(APCS_ALIAS7_SAW2_SPM_CTL);

	  /* Logging Power status & Power gate status for performance cluster CPU's */
	  sysdbg_spm_regs.APCS_ALIASn_APC_PWR_STATUS[4] = HWIO_IN(APCS_ALIAS4_APC_PWR_STATUS);
	  sysdbg_spm_regs.APCS_ALIASn_APC_PWR_STATUS[5] = HWIO_IN(APCS_ALIAS5_APC_PWR_STATUS);
	  sysdbg_spm_regs.APCS_ALIASn_APC_PWR_STATUS[6] = HWIO_IN(APCS_ALIAS6_APC_PWR_STATUS);
	  sysdbg_spm_regs.APCS_ALIASn_APC_PWR_STATUS[7] = HWIO_IN(APCS_ALIAS7_APC_PWR_STATUS);

	  sysdbg_spm_regs.APCS_ALIASn_APC_PWR_GATE_STATUS[4] = HWIO_IN(APCS_ALIAS4_APC_PWR_GATE_STATUS);
	  sysdbg_spm_regs.APCS_ALIASn_APC_PWR_GATE_STATUS[5] = HWIO_IN(APCS_ALIAS5_APC_PWR_GATE_STATUS);
	  sysdbg_spm_regs.APCS_ALIASn_APC_PWR_GATE_STATUS[6] = HWIO_IN(APCS_ALIAS6_APC_PWR_GATE_STATUS);
	  sysdbg_spm_regs.APCS_ALIASn_APC_PWR_GATE_STATUS[7] = HWIO_IN(APCS_ALIAS7_APC_PWR_GATE_STATUS);

	  /* Logging SPM status & SPM control for performance cluster L2 */
	  sysdbg_spm_regs.APCS_CLUS1_L2_SAW2_SPM_STS = HWIO_IN(APCS_CLUS1_L2_SAW2_SPM_STS);
	  sysdbg_spm_regs.APCS_CLUS1_L2_SAW2_SPM_CTL = HWIO_IN(APCS_CLUS1_L2_SAW2_SPM_CTL);
	  
	  /* Logging L2 Power status of performance cluster */
	  sysdbg_spm_regs.APCS_ALIASn_L2_PWR_STATUS[1] = HWIO_IN(APCS_ALIAS1_L2_PWR_STATUS);

	  /* Logging SPM status & SPM control for CCI */
	  sysdbg_spm_regs.CCI_SAW2_SPM_STS = HWIO_IN(CCI_SAW2_SPM_STS);
	  sysdbg_spm_regs.CCI_SAW2_SPM_CTL = HWIO_IN(CCI_SAW2_SPM_CTL);

}

// GCC_RESET_DEBUG , GCC_QDSS_DAP_AHB_CBCR
#undef  CLK_CTL_BASE
#define CLK_CTL_BASE            CLK_CTL_BASE_PHYS

// QDSS_WRAPPER_CS_DEBUG_UI_LOCKACCESS
#undef  QDSS_WRAPPER_TOP_BASE
#define QDSS_WRAPPER_TOP_BASE   QDSS_WRAPPER_TOP_BASE_PHYS

static boolean sysdbg_debug_ui_en_flat_mapped(void)
{
	HWIO_OUTF(GCC_QDSS_DAP_AHB_CBCR, CLK_ENABLE, 0x1);
	HWIO_OUTF(GCC_QDSS_CFG_AHB_CBCR,CLK_ENABLE, 0x1);
	HWIO_OUTF(GCC_QDSS_DAP_CBCR, CLK_ENABLE, 0x1);

	HWIO_OUT(QDSS_WRAPPER_CS_DEBUG_UI_LOCKACCESS, 0xC5ACCE55);
	HWIO_OUTF(QDSS_WRAPPER_DEBUG_UI_SECURE, SEC_CTL, 0x1);
	HWIO_OUT(QDSS_WRAPPER_DEBUG_UI_CTL, 0x1);      // Clear all status
	HWIO_OUT(QDSS_WRAPPER_DEBUG_UI_CTL_MASK, 0x0);     // Clear SW_TRIG_MASK
	return TRUE;
}

// TCSR_RESET_DEBUG_SW_ENTRY
#undef  CORE_TOP_CSR_BASE
#define CORE_TOP_CSR_BASE    CORE_TOP_CSR_BASE_PHYS

static boolean sysdbg_enable_flat_mapped(void)
{
	// We need to enable PROC_HALT_EN so that MSM pulls up PS_HOLD after reset
	//HWIO_OUTF(GCC_RESET_DEBUG, PROC_HALT_EN, 0x1);

	// Set the debug timer after which controller can reset the system
	// 256 sclk cycles = ~ 7.8ms
	HWIO_OUTF(GCC_RESET_DEBUG, PRE_ARES_DEBUG_TIMER_VAL, 0x100);
	// Enable / Disable the debug through watchdog feature
	HWIO_OUTF(GCC_RESET_DEBUG, SECURE_WDOG_DEBUG_EN, 0x1);
	HWIO_OUTF(GCC_RESET_DEBUG, MSM_TSENSE_RESET_DEBUG_EN, 0x1);
	HWIO_OUTF(GCC_RESET_DEBUG, PMIC_ABNORMAL_RESET_DEBUG_EN, 0x1);

    // Set the ETR timer to flush data out
	HWIO_OUT(GCC_FLUSH_ETR_DEBUG_TIMER, 0x180);
	//Set STOP_CAPTURE_DEBUG_TIMER to 2ms
	HWIO_OUT(GCC_STOP_CAPTURE_DEBUG_TIMER, 0x40);
	// Flag to enable Debug
	HWIO_OUTM(TCSR_RESET_DEBUG_SW_ENTRY, 0x1, 0X1);
	
	sysdbg_debug_ui_en();
	return TRUE;
}

// MPM2_MPM_CONTROL_CNTCR
#undef  MPM2_MPM_BASE
#define MPM2_MPM_BASE    MPM2_MPM_BASE_PHYS

static boolean sysdbg_dump_stats_init_flat_mapped(void)
{
	static boolean init = FALSE;
	static uint32 reset_reason[6] ={};

	uint32 temp = 0, i = 0 ;

	if (!init)
	{
		// Set the debug timer after which controller can reset the system
		// 256 sclk cycles = ~ 7.8ms


		// Enable MPM timer to start the parent timer
		HWIO_OUTF(MPM2_MPM_CONTROL_CNTCR, EN, 0x1);

		// We are going to count at XO speed. Configure
		// for XO = 19.2 MHz
		HWIO_OUTF(APCS_QTMR0_QTMR_AC_CNTFRQ, CNTFRQ, 19200000);
		busywait_init();
		// We need to know the PMIC reset reason as well
		// To be able to talk to the PMIC, SPMI init needs to
		// happen and data needs to be saved. Hence, doing it here
		//SPMIFW_Init();

		/* MSM Reset Reasoninit	*/
		reset_reason[0] = HWIO_IN(GCC_RESET_STATUS);
		// Other reset reasons
		// PON Reason 1 - 0x08
		PM_ReadPerphData(PONA, 0x08, &temp);
		reset_reason[1] |= temp << 8;
		//Warm Reset Reason 1 - 0x0A
		PM_ReadPerphData(PONA, 0x0A, &temp);
		//Warm Reset Reason 2 - 0x0B
		PM_ReadPerphData(PONA, 0x0B, &reset_reason[2]);
		reset_reason[2] |= temp << 8;
		// POFF Reason 1 - 0x0C
		PM_ReadPerphData(PONA, 0xC, &temp);
		// POFF Reason 2 - 0x0D
		PM_ReadPerphData(PONA, 0xD, &reset_reason[3]);
		reset_reason[3] |= temp << 8;
		
		// PON PBL STATUS
		PM_ReadPerphData(PONA, 0x07, &temp);
		reset_reason[4] |= temp;
		// PS_HOLD_RESET_CTL
		PM_ReadPerphData(PONA, 0x5A, &temp);
		reset_reason[4] |= temp << 8;
		// PS_HOLD_RESET_CTL2
		PM_ReadPerphData(PONA, 0x5B, &temp);
		reset_reason[4] |= temp << 16;
		
		// Debug Enhancement to Log RTC time. 
		for(i=0; i<4; i++)
		{
			temp = 0;
			reset_reason[5] <<= 8;
			PM_ReadPerphData(RTCA_WR, 0x4B-i, &temp);
			reset_reason[5] |= temp;
		}
		

		// Now populate our dump structure
		sysdbgDumpStats(reset_reason);
		
		/* Call sysdbg_dump_spm_regs to dump SPM registers */
		sysdbg_dump_spm_regs();

		init = TRUE;
	}

	return init;
}

// GCC_RESET_DEBUG , GCC_QDSS_DAP_AHB_CBCR
#undef  CLK_CTL_BASE
#define CLK_CTL_BASE            CLK_CTL_BASE_PHYS

// TCSR_RESET_DEBUG_SW_ENTRY
#undef  CORE_TOP_CSR_BASE
#define CORE_TOP_CSR_BASE    CORE_TOP_CSR_BASE_PHYS

static boolean sysdbg_exit_flat_mapped(void)
{
	uint32 cur_cpu= tzbsp_cur_cpu_num();
	uint64 last_cpu= get_global_lock();

	if (cur_cpu == last_cpu)
	{
		HWIO_OUTF(GCC_RESET_DEBUG, SECURE_WDOG_DEBUG_EN, 0x0);
		HWIO_OUTF(GCC_RESET_DEBUG, MSM_TSENSE_RESET_DEBUG_EN, 0x0);
		HWIO_OUTF(GCC_RESET_DEBUG, PMIC_ABNORMAL_RESET_DEBUG_EN, 0x0);
		HWIO_OUT(TCSR_RESET_DEBUG_SW_ENTRY, 0x0);
		HWIO_OUTF(GCC_RESET_DEBUG, BLOCK_RESIN, 0x0);
		HWIO_OUT(GCC_RESET_STATUS, 0x2);

		(*(uint32*)SYSTEM_DEBUG_MARKER) = 0xDEADD00D;
	}
	tzbsp_isb();
	tzbsp_dsb();

	TZBSP_LOG_ERR("sysdbg_exit_flat_mapped() completed succesfully \n");

	//should include a DSB & ISB before jumping to self-modified code
	func_ptr(cur_cpu, last_cpu); //never returns
	while(TRUE);
}

static boolean sysdbg_reset_next_core_flat_mapped(void)
{
	#define APPS_ALIAS_SIZE		0x10000
	uint32 next_act_core = 0, cluster = 0;
	volatile uint32 *reg;
	
	static boolean L2PowerCollapsed[] = { TRUE, FALSE };
	static boolean L2Init[] = { FALSE, FALSE };
    	
	static uint32 memcpy_to_rpm_coderam = 0x0; 
	
 	/* Copy DDR self refrsh code to RPM code RAM and update the status */
	if(!memcpy_to_rpm_coderam)
	{
		//Using ENTRY point of SYSDBG in RPM CODERAM 
		func_ptr = (void (*)(uint32, uint64))(HWIO_IN(TCSR_RESET_DEBUG_SW_ENTRY)|0x1);//Using ENTRY point of SYSDBG in RPM CODERAM		 
		void *code_src = (void*)((uint32)ddr_enter_self_refresh_and_reset_flat_mapped &0xFFFFFFFC); //current beta compiler doesn't support pragma arm or --arm_only compiler option
		void *code_dest = (void*)((uint32)func_ptr&0xFFFFFFFC); //temp solution is to force thumb code and manipulating the pointers directly
		memscpy(code_dest, 0x200 , code_src, 0x200);
	   	memcpy_to_rpm_coderam = 0x1;
  	}

	// Find the next core to be reset based on the status of the headswitch for the start
    next_act_core = SYSDBGGetNxtActCore(tzbsp_cur_cpu_num());
	//cluster = (next_act_core >= TARGET_CLUSTER_0_SIZE) ? 1 : 0;
	if(next_act_core >= TARGET_CLUSTER_0_SIZE)
	   cluster = 1;
	

	// Program the start address to the reset vector of TZ
	//HWIO_OUT(APCS_ALIAS1_BOOT_START_ADDR_SEC, (uint32)&Image$$TZBSP_CODE$$Base | 0x1);
     /* Need to write to 2 cluster registers for 8939 octa core */
    if (total_cluster_count > 1) 
	{
		HWIO_OUT(APCS_ALIAS0_CFG_SECURE, 0x1);
	}
	HWIO_OUT(APCS_ALIAS1_CFG_SECURE, 0x1);	
		  
/*	reg = (uint32 *)(HWIO_APCS_BOOT_START_ADDR_SEC_ADDR);
	*reg = (&Image$$TZBSP_CODE$$Base|0x1);

	reg = (uint32 *)(HWIO_APCS_CFG_SECURE_ADDR);
	*reg = 0x1;
*/

    /* Means this is the last active core */
    if (next_act_core == tzbsp_cur_cpu_num())
        return TRUE;
		
	// Check if we have reached the maximum number of cores to reset
    if (next_act_core < total_cpu_count)
    {
        /* Specific to 8936 Quad core */
		if (total_cluster_count == 1)
        {
            reg = (uint32 *)((uintptr_t)(HWIO_APCS_ALIAS1_AA64NAA32_REG_ADDR ));
			*reg = 0x1;		
		}
		/* Specific to 8939 Octa core */
		else
		{
            /* Configure the L2 for cluster 0 only if we are going to bring cluster 0 out of reset. L2 for cluster 1
             * will be brought out of PC even if it was in power collapse */
            if (((cluster == 0) && (!L2Init[cluster])) && (SYSDBGGetClusterL2SPCStatus()))
            {

                /* Bring the L2 out of reset and update state */
                 SYSDBGNormalL2Reset();
                 L2PowerCollapsed[cluster] = FALSE;
                 L2Init[cluster] = TRUE;

            }
			
            /* If the L2 for the cluster was power collapsed, skip the cluster completely. */
            if (!L2PowerCollapsed[cluster])
            {
				/* Configure it to boot up in Aarch64 */
				if (cluster == 0)
				{
					reg = (uint32 *)((uintptr_t)(HWIO_APCS_ALIAS0_AA64NAA32_REG_ADDR ));
					*reg = 0x1;
				}
				else
				{
					reg = (uint32 *)((uintptr_t)(HWIO_APCS_ALIAS1_AA64NAA32_REG_ADDR ));
					*reg = 0x1;
				}
				
            }
			/* Secondary cluster L2 needs to be flushed if it is not power collapsed */
			/* If all cores in secondary cluster (power cluster) are in power collapse */
			/* Force wake up the first core to flush L2.
               Here next_act_core is 4, because we are running on performance cluster and 
			   no other active cores in power cluster, so not to be confused by regular core4 */
			if (next_act_core == 4)
			{
				/* Check if L2 of secondary cluster(power cluster) is ON by checking power status register */
				uint32 L2PCstatus = SYSDBGGetClusterL2SValidStatus(0);
				/* If L2 is ON wake up core 0 to flush the contents */ 
				if(L2PCstatus == 0x1)
				{
					/* L2 is ONLINE, force wake up first core (core 0) in power cluster */
					next_act_core = 0;
					SYSDBGCoreResetSPI(next_act_core);
					/* Increment our lock to indicate that one more core is going to be up */
					set_global_lock(next_act_core);
				}
				return TRUE;
			}		
		}

		/* We need to check which reset sequence depending on whether core is in sleep or stuck in the winding down / wakeup path */
		/* Condition to check - clock gated (0)+ clamp enabled(4) + core in reset (3)*/
		/*reg = (uint32 *)((uintptr_t)(HWIO_APCS_ALIAS0_APC_PWR_STATUS_ADDR + (PAGE64K_OFFSET * core)));
		if (!BITGET(*reg, 27)) // | BITGET(*reg, 3) | BITGET(*reg, 4))
		SYSDBGForceCoreReset(core);
		else */
		SYSDBGNormalCoreReset(next_act_core);
     
    	 /* Increment our lock to indicate that one more core is going to be up */
		set_global_lock(next_act_core);
    }

   	return TRUE;
}

// HWIO_APCS_ALIAS4_APC_PWR_STATUS_ADDR
#undef  A53SS_BASE
#define A53SS_BASE    A53SS_BASE_PHYS

static boolean SYSDBGGetCoreStatus_flat_mapped(uint32 corenum)
{

    uint32 *reg = NULL;

    /* Check the input */
    if (corenum > total_cpu_count)
        return FALSE;
	
	/* Changes to support 8936 Quad core */
    if (total_cluster_count == 1) 
	{
		/* Get the status */
		reg = (uint32 *)((HWIO_APCS_ALIAS4_APC_PWR_STATUS_ADDR + (APPS_ALIAS_SIZE * corenum )));
		return  ((*reg & (HWIO_APCS_ALIAS4_APC_PWR_STATUS_GDHS_STS_BMSK)) >> HWIO_APCS_ALIAS4_APC_PWR_STATUS_GDHS_STS_SHFT);
	}
	/* 8939 Octa core */ 
	else
	{	
		if (corenum >= 4)
		{	
			/* Get the status */
			reg = (uint32 *)((HWIO_APCS_ALIAS4_APC_PWR_STATUS_ADDR + (APPS_ALIAS_SIZE * (corenum-4) )));
			return  ((*reg & (HWIO_APCS_ALIAS4_APC_PWR_STATUS_GDHS_STS_BMSK)) >> HWIO_APCS_ALIAS4_APC_PWR_STATUS_GDHS_STS_SHFT);
		}
		else
		{
			reg = (uint32 *)(HWIO_APCS_ALIAS0_APC_PWR_STATUS_ADDR + (APPS_ALIAS_SIZE * corenum));
			return (*reg & (HWIO_APCS_ALIAS0_APC_PWR_STATUS_GDHS_STS_BMSK)) >> HWIO_APCS_ALIAS0_APC_PWR_STATUS_GDHS_STS_SHFT;
		}
	}
}


// HWIO_BIMC_S_DDR0_SCMO_ADDR_MAP_CSn_OUTMI
#undef  BIMC_BASE
#define BIMC_BASE    BIMC_BASE_PHYS

// HWIO_MPM2_MPM_PS_HOLD_ADDR
#undef  MPM2_MPM_BASE
#define MPM2_MPM_BASE    MPM2_MPM_BASE_PHYS


static void ddr_enter_self_refresh_and_reset_flat_mapped(uint32 cur_cpu, uint64 last_cpu)
{
	 if (cur_cpu == last_cpu) //only reset if it is the last CPU
	{
             
        /* New sequence to be followed to put the DDR into self refresh for 8939 which has scaled code */
      	HWIO_BIMC_S_DDR0_SCMO_ADDR_MAP_CSn_OUTMI(0, HWIO_BIMC_S_DDR0_SCMO_ADDR_MAP_CSn_RANK_EN_BMSK, 0x0);
        HWIO_BIMC_S_DDR0_SCMO_ADDR_MAP_CSn_OUTMI(1, HWIO_BIMC_S_DDR0_SCMO_ADDR_MAP_CSn_RANK_EN_BMSK, 0x0);
        
        HWIO_OUTF(BIMC_S_DDR0_SHKE_SELF_REFRESH_CNTL, HW_SELF_RFSH_ENABLE_RANK0, 0x0);
        HWIO_OUTF(BIMC_S_DDR0_SHKE_SELF_REFRESH_CNTL, HW_SELF_RFSH_ENABLE_RANK1, 0x0);		
	    HWIO_OUTF(BIMC_S_DDR0_SHKE_SELF_REFRESH_CNTL, CONCURRENT_SELF_RFSH_EN, 0x0);
		
		/* Update IOC CTL CFG register which is in EBI_PHY_CFG domain */
		/* Please generate new hwio and replace the below code with hwio Macros */
		*((uint32 *)0x00480080) &= ~0x40000000;		
	   
	    HWIO_OUTF(BIMC_S_DDR0_SHKE_DRAM_MANUAL_0, ENTER_SELF_REFRESH_IDLE, 0x1);
	    while(HWIO_INF(BIMC_S_DDR0_SHKE_DRAM_MANUAL_0, ENTER_SELF_REFRESH_IDLE));
	    	
        /* Trigger for pshold reset */
        HWIO_OUTF(MPM2_MPM_PS_HOLD, PSHOLD, 0);
		/* Add dsb instruction to make sure the memory transactions are completed before pulling PS_HOLD low */
		tzbsp_dsb();
    }
    while(TRUE);
}



// HWIO_APCS_ALIAS1_L2_PWR_STATUS_ADDR
#undef  A53SS_BASE
#define A53SS_BASE    A53SS_BASE_PHYS

static boolean SYSDBGGetClusterL2SValidStatus_flat_mapped(uint32 clusternum)
{
    uint32 *reg = NULL;
    boolean l2_valid = FALSE;

        /* Check the input */
    if (clusternum > total_cluster_count)
        return FALSE;
		
	/* Changes to support 8936 Quad core */
    if (total_cluster_count == 1) 
	{
		/* Check that for valid L2 cache using HS_STS. */
		reg  = (uint32 *)(HWIO_APCS_ALIAS1_L2_PWR_STATUS_ADDR);
		l2_valid = (*reg & (HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_HS_STS_BMSK)) >> HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_HS_STS_SHFT;
	}
	/* 8939 Octacore support */
	else
	{
		if (clusternum == 0)
		{
			/* Check that for valid L2 cache using HS_STS. */
			reg  = (uint32 *)(HWIO_APCS_ALIAS0_L2_PWR_STATUS_ADDR);
			l2_valid = (*reg & (HWIO_APCS_ALIAS0_L2_PWR_STATUS_L2_HS_STS_BMSK)) >> HWIO_APCS_ALIAS0_L2_PWR_STATUS_L2_HS_STS_SHFT ;
		}
		else
		{
			/* Check that for valid L2 cache using HS_STS. */
			reg  = (uint32 *)(HWIO_APCS_ALIAS1_L2_PWR_STATUS_ADDR);
			l2_valid = (*reg & (HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_HS_STS_BMSK)) >> HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_HS_STS_SHFT;
		}
	}
	
    return l2_valid;
}

// HWIO_APCS_ALIAS0_L2_PWR_STATUS_ADDR
#undef  A53SS_BASE
#define A53SS_BASE    A53SS_BASE_PHYS

static boolean SYSDBGGetClusterL2SPCStatus_flat_mapped(void)
{

    uint32 *reg = NULL;
    boolean l2online = FALSE, L2HSStatus = 0, L2ARRAYHSStatus = 0;

    /* Check that for valid L2 of secondary cluster using HS Status and HS Array status, if any one of these bits is set bring the L2 up */
	reg  = (uint32 *)(HWIO_APCS_ALIAS0_L2_PWR_STATUS_ADDR);
	L2HSStatus = (*reg & (HWIO_APCS_ALIAS0_L2_PWR_STATUS_L2_HS_STS_BMSK)) >> HWIO_APCS_ALIAS0_L2_PWR_STATUS_L2_HS_STS_SHFT;
	L2ARRAYHSStatus = (*reg & (HWIO_APCS_ALIAS0_L2_PWR_STATUS_L2_ARRAY_HS_STS_BMSK)) >> HWIO_APCS_ALIAS0_L2_PWR_STATUS_L2_ARRAY_HS_STS_SHFT;

    l2online = (L2HSStatus || L2ARRAYHSStatus);

    return l2online;
}
/*===========================================================================*/

static void SYSDBGNormalCoreReset_flat_mapped (uint32 core_num)
{
    volatile uint32 *reg;
	uint32 offset = 0;
	 
    /* 8936 Quad core support */
    if (total_cluster_count == 1) 
    {
        offset = (APPS_ALIAS_SIZE * core_num);

		reg = (uint32 *)(HWIO_APCS_ALIAS4_CPU_PWR_CTL_ADDR + offset);
		*reg = 0x0000001D;

		reg = (uint32 *)(HWIO_APCS_ALIAS4_APC_PWR_GATE_CTL_ADDR + offset);
		*reg = 0x10000001;

		busywait(2);
		
		reg = (uint32 *)(HWIO_APCS_ALIAS4_CPU_PWR_CTL_ADDR + offset); 
		*reg = 0x0002001C;
		
		busywait(2);

		reg = (uint32 *)(HWIO_APCS_ALIAS4_CPU_PWR_CTL_ADDR + offset);
		*reg = 0x0002000C;

		reg = (uint32 *)(HWIO_APCS_ALIAS4_CPU_PWR_CTL_ADDR + offset);
		*reg = 0x0002008C;

    }/* End of changes pertaining to 8936 quadcore */
    /* 8939 Octa core specific */
    else 
    {
        if (core_num < 4) 
        {
			Secondary_cluster_online_cpu_count++;
            offset = APPS_ALIAS_SIZE * core_num;
            reg = (uint32 *)(HWIO_APCS_ALIAS0_CPU_PWR_CTL_ADDR + offset);
            *reg = 0x0000001D;

            reg = (uint32 *)(HWIO_APCS_ALIAS0_APC_PWR_GATE_CTL_ADDR + offset);
            *reg = 0x10000001;

            busywait(2);
            
            reg = (uint32 *)(HWIO_APCS_ALIAS0_CPU_PWR_CTL_ADDR + offset); 
            *reg = 0x0002001C;
            
            busywait(2);

            reg = (uint32 *)(HWIO_APCS_ALIAS0_CPU_PWR_CTL_ADDR + offset);
            *reg = 0x0002000C;

            reg = (uint32 *)(HWIO_APCS_ALIAS0_CPU_PWR_CTL_ADDR + offset);
            *reg = 0x0002008C;
            
        }
        else
        {
            offset = (APPS_ALIAS_SIZE * (core_num-4));

            reg = (uint32 *)(HWIO_APCS_ALIAS4_CPU_PWR_CTL_ADDR + offset);
            *reg = 0x0000001D;

            reg = (uint32 *)(HWIO_APCS_ALIAS4_APC_PWR_GATE_CTL_ADDR + offset);
            *reg = 0x10000001;

            busywait(2);
            
            reg = (uint32 *)(HWIO_APCS_ALIAS4_CPU_PWR_CTL_ADDR + offset); 
            *reg = 0x0002001C;
            
            busywait(2);

            reg = (uint32 *)(HWIO_APCS_ALIAS4_CPU_PWR_CTL_ADDR + offset);
            *reg = 0x0002000C;

            reg = (uint32 *)(HWIO_APCS_ALIAS4_CPU_PWR_CTL_ADDR + offset);
            *reg = 0x0002008C;
        }
    }
}
/*===========================================================================*/

static void SYSDBGCoreResetSPI_flat_mapped(uint32 core_num)
{
	volatile uint32 *reg;
	HAL_qgic_BaseAddressType base;

	base.nGICDBaseAddress = 0xb000000;
	base.nGICCBaseAddress = 0xb002000;
	base.nGICHBaseAddress = 0xb001000;
	base.nGICVBaseAddress = 0xb004000;
	
	/* Update Secondary cluster OL core count, so that control will not come here again */
	Secondary_cluster_online_cpu_count++;
	
	/* Execute the core warm boot wake up sequence for core 0 */
	reg = (uint32 *)(HWIO_APCS_ALIAS0_CPU_PWR_CTL_ADDR);
	*reg = 0x0000001D;

	reg = (uint32 *)(HWIO_APCS_ALIAS0_APC_PWR_GATE_CTL_ADDR);
	*reg = 0x10000001;

	busywait(2);
				
	reg = (uint32 *)(HWIO_APCS_ALIAS0_CPU_PWR_CTL_ADDR); 
	*reg = 0x0002001C;
				
	busywait(2);

	reg = (uint32 *)(HWIO_APCS_ALIAS0_CPU_PWR_CTL_ADDR);
	*reg = 0x0002000C;

	reg = (uint32 *)(HWIO_APCS_ALIAS0_CPU_PWR_CTL_ADDR);
	*reg = 0x0002008C;
	
	/* Initialize QGIC */
	HAL_qgic_Init(&base);
	HAL_qgic_Reset();
	HAL_qgic_SetPriorityMask(HAL_QGIC_PRIORITY_LOWEST);
	HAL_qgic_EnableDistributor();
	HAL_qgic_EnableInterrupts();
	HAL_qgic_EnableDistributorNS();
	HAL_qgic_EnableInterruptsNS();
	HAL_qgic_SetSecureAckNS(TRUE);
	  
	/* Enable the QTIMER interrupt */
	HAL_qgic_SetSecurity(HAL_QGIC_QTMR_QGICFRM0PHYIRQ, HAL_QGIC_SECURE);
	HAL_qgic_SetPriority(HAL_QGIC_QTMR_QGICFRM0PHYIRQ, HAL_QGIC_PRIORITY_HIGHEST);
	//HAL_qgic_SetTrigger (HAL_QGIC_QTMR_QGICFRM0PHYIRQ, pInt_info->eTrigger);
	HAL_qgic_SetTargets (HAL_QGIC_QTMR_QGICFRM0PHYIRQ, (1 << core_num));
	HAL_qgic_Enable(HAL_QGIC_QTMR_QGICFRM0PHYIRQ);
		
	//Enable input to the counter
	HWIO_OUT(MPM2_MPM_CONTROL_CNTCR,0x1);

	//Set matching count
	HWIO_OUT(APCS_QTMR0_F0V1_QTMR_V1_CNTP_TVAL,0x50);

	//Enable the counter and interrupt                                         
	HWIO_OUT(APCS_QTMR0_F0V1_QTMR_V1_CNTP_CTL,0x01);
	
}

static void SYSDBGNormalL2Reset_flat_mapped(void)
{
    // Close L1/SCU logic GDHS(Powerup), Wakeup L2/SCU RAMs ; ie. deassert sleep signals
    // Write 0x00101707 to APCS_ALIAS0_L2_PWR_CTL (0x0B11_1014)
    // Wait 2ms (delay loop)
    HWIO_OUT(APCS_ALIAS0_L2_PWR_CTL, 0x00101707);
    busywait(2);
    
    // Enable Clocks using SW CLK EN
    // Write 0x00000001 to APCS_ALIAS0_CORE_CBCR (0x0B11_1058)
    HWIO_OUT(APCS_ALIAS0_CORE_CBCR, 0x00000001);
        
    // De-assert L2/SCU logic clamp
    // Write 0x00101607 to APCS_ALIAS0_L2_PWR_CTL (0x0B11_1014)
    // Wait 1.2ms (delay loop)
    HWIO_OUT(APCS_ALIAS0_L2_PWR_CTL, 0x00101607);
    busywait(2);
    
    // De-assert L2/SCU logic reset
    // Write 0x00100207 to APCS_ALIAS0_L2_PWR_CTL (0x0B11_1014)
    // Wait 2ms (delay loop)
    HWIO_OUT(APCS_ALIAS0_L2_PWR_CTL, 0x00100207);
    busywait(2);    
    
    // Set PMIC APC on
    // Write 0x10100207 to APCS_ALIAS0_L2_PWR_CTL (0x0B11_1014)
    HWIO_OUT(APCS_ALIAS0_L2_PWR_CTL, 0x10100207);
    
    // Set HW CLK CTL for the CBC block
    // Write 0x00000003 to APCS_ALIAS0_CORE_CBCR (0x0B11_1058)
    HWIO_OUT(APCS_ALIAS0_CORE_CBCR, 0x00000003);
}

