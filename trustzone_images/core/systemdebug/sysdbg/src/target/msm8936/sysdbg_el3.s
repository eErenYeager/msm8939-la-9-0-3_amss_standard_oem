;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;
;             SYSDBG INITIALIZATION ROUTINE
;
; GENERAL DESCRIPTION
;	This file contains initialization code for system debug. This performs
;	check on reset debug status and calls into tzbsp_monitor for saving ctxt.
;
;
; Copyright 2010-2012 by Qualcomm Technologies, Incorporated.All Rights Reserved.
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;
;                           EDIT HISTORY FOR FILE
;
; This section contains comments describing changes made to the module.
; Notice that changes are listed in reverse chronological order.
;
; when       who     what, where, why
; --------   ---     --------------------------------------------------------
; 10/26/2013 AJC    Add some more code markers
; 09/11/2013 AJC    Avoid clobbering mon mode r14 on 8x26 / 8x10
; 09/04/2013 AJC    Added change to avoid clobbering r0 and r1 on cores other than core 0
; 05/21/2013 AJC    Added change to verify CPU id before booting up after reset
; 01/29/2013 AJC	Added change to save information from bootloader
; 09/13/2012 AJC	Fixed missing sys mode registers
; 02/29/2012 AJC	Initial revision for DBI - 8974
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

#include "ctx_util.h"

;============================================================================
;
;                             MODULE DEFINES
;
;============================================================================
;
Mode_USR                EQU    0x10
Mode_FIQ                EQU    0x11
Mode_IRQ                EQU    0x12
Mode_SVC                EQU    0x13
Mode_MON				EQU	   0x16
Mode_ABT                EQU    0x17
Mode_UND                EQU    0x1B
Mode_SYS                EQU    0x1F

I_Bit                   EQU    0x80
F_Bit                   EQU    0x40

; Secure Configuration Register Bits
SCR_NS_BIT                  EQU     0x01    ; Non-Secure (NS) bit
SCR_IRQ_BIT                 EQU     0x02    ; IRQ bit
SCR_FIQ_BIT                 EQU     0x04    ; FIQ bit
SCR_FW_BIT                  EQU     0x10    ; F Bit writable (FW)
SCR_AW_BIT                  EQU     0x20    ; A Bit writable (AW)

; MASKs
DEBUG_RESET_CHECK_MASK	EQU    0x000E0000
GCC_RESET_STATUS        EQU    0x01815008
GCC_RESET_DEBUG         EQU    0x01814000
DEBUG_STATUS_CHECK_MASK	EQU    0x00000038

SYSDBG_EL1_ENTRY_MAGIC         EQU    0x5D1E1350
SYSDBG_EL1_EXIT_MAGIC          EQU    0x5D1E13A0
SYSDBG_WARMBOOT_MARKER_ADDR    EQU    0x8600B14

;============================================================================
;
;                             MODULE IMPORTS
;
;============================================================================
    ; Import the external symbols that are referenced in this module.

    IMPORT mon_get_sysdbg_buf_addr
    IMPORT mon_get_sysdbg_entry_addr
;============================================================================
;
;                             MODULE EXPORTS
;
;============================================================================

    ; Export the external symbols that are referenced in this module.
    EXPORT sysdbg_reset_check
    EXPORT sysdbg_entry_handler

;============================================================================
;
;                             MODULE DATA AREA
;
;============================================================================

    AREA SYSDBG_FUNC, CODE, READONLY
    PRESERVE8

;---------------------------------------------------------------------
;	Debug Reset Check
;======================================================================

sysdbg_reset_check FUNCTION
    movz    x0, #0x181, lsl #16
    add     x0, x0, #0x5000	     ;#0x5000 is not a valid #bimm32, orr cannot be used here
    add     x0, x0, #0x8	     ;add #0x5008 is not valid since exceeds #uimm12 that can be shifted
    ldr     x0, [x0]		     ;check for debug reset
    and     x0, x0, #DEBUG_STATUS_CHECK_MASK
    cmp     x0, #0
    b.ne    due_to_reset
    blr     x30 	     ;
    movz    x0, #0x181, lsl #16
    orr     x0, x0, #0x4000
    ldr     x0, [x0]		     ;
    and     x0, x0, #DEBUG_RESET_CHECK_MASK
    cmp     x0, #0
    b.ne    due_to_reset
    blr     x30			     ;return if not due to reset
due_to_reset
    mov     x0, #0x1
    blr     x30
    ENDFUNC

;============================================================================
; Handles the Ctxt saving and ensuring a clean EL1 entry for SYSDBG
;
;   Tasks include:
;   (1) Save all GP, register context
;   (2) Jump to sysdbg_el1 entry
;============================================================================
sysdbg_entry_handler
    ldr     x0, =sysdbg_remapper_reset
    br      x0
sysdbg_remapper_reset
	LDR     x0, =SYSDBG_WARMBOOT_MARKER_ADDR
	LDR     w30, =SYSDBG_EL1_ENTRY_MAGIC
	STR     w30, [x0]
	
    add     x0, sp, xzr
    msr     TPIDR_EL2, x0		;save sp to TPIDR_EL2
    bl      mon_get_sysdbg_buf_addr	;compiler only uses x0 in this function, no need to back up registers or setup stack
    add     sp, x0, xzr
    mrs     x0, TPIDR_EL0            	;restore X0
    mrs     x30, TPIDR_EL1    	     	;restore LR
    SaveCtx sp 			     	;call upon common SaveCtx routine
    mrs     x0, TPIDR_EL2            	;save previously backed up sp
    mrs     x1, SP_EL2
    PushTwoA sp, x0,  x1
    mrs     x0, SPSR_EL2
    mrs     x1, ELR_EL2
    PushTwoA sp, x0,  x1
    bl      mon_get_sysdbg_entry_addr

    ;-------------------------------------------------------------------
    ; Make sure EL1 CPSR is in good state
    ;-------------------------------------------------------------------
    mov     x1, #0xD3
    msr     SPSR_EL3, x1

    ;-------------------------------------------------------------------
    ; Set the ELR to the sysdbg entry addr in el1
    ;-------------------------------------------------------------------
    msr     ELR_EL3, x0

    LDR x0, =SYSDBG_WARMBOOT_MARKER_ADDR
    LDR w30, =SYSDBG_EL1_EXIT_MAGIC
    STR w30, [x0]	
    ;-------------------------------------------------------------------
    ; Ensure all system registers updates are observed
    ;-------------------------------------------------------------------
    dsb     sy

    ;-------------------------------------------------------------------
    ; Switch to secure EL1 to call the notifier
    ;-------------------------------------------------------------------
    eret


;=======================================================================
;
;		Data for the module
;
;=======================================================================
    AREA DBI_DATA , DATA, READWRITE

; Pointer to the CPU register save datastruct
; IMPORT dbi_cpu_regs

    END
