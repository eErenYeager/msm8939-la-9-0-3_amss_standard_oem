/*=============================================================================
                        SYSDBG Target Specific API Header

GENERAL DESCRIPTION     
		  This module defines objects for the main control loop of the debug image.
  
	Copyright 2012- 2014 by Qualcomm Technologies, Inc.  All Rights Reserved.
=============================================================================*/

/*=============================================================================

                            EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/systemdebug/sysdbg/src/target/msm8936/sysdbg_target.h#1 $

when       who          what, where, why
--------   ---          --------------------------------------------------
02/01/13	AJCheriyan	Added api for HW Revision ID check
02/22/12	AJCheriyan	First cut for Aragorn. First ever SYSDBG

===========================================================================*/


#ifndef _SYSDBG_TARGET_H
#define _SYSDBG_TARGET_H

#include "sysdbg_mem_dump.h"

#define HAL_QGIC_QTMR_QGICFRM0PHYIRQ 40

/*===========================================================================
**  Function :  SYSDBG_GET_SHARED_IMEM
** ==========================================================================
*/
/*!
* 
* @brief
* 	API to get the location of shared imem
* 
* @param[in] 
*  None
*  
* @par Dependencies
*   None
* 
* @retval
*   None
* 
* @par Side Effects
*   None
* 
*/

uint8 *sysdbg_get_shared_imem(void);

/*===========================================================================
**  Function :  SYSDBG_DUMP_SPM_REGS
** ==========================================================================
*/
/*!
*
* @brief
* 	Dump SPm status and control registers for each core
* 	
*
* @param[in]
*  None
*
* @par Dependencies
*   None
*
* @retval
*   None
*
* @par Side Effects
*   None
*
*/

void sysdbg_dump_spm_regs(void);

/*===========================================================================
*  Function :  SYSDBG_DUMP_STATS_INIT
** ==========================================================================
*/
/*!
* 
* @brief
* 	API to get chip reset reason
* 
* @param[in] 
*  None
*  
* @par Dependencies
*   None
* 
* @retval
*   Boolean
* 
* @par Side Effects
*   None
* 
*/

boolean sysdbg_dump_stats_init(void);

/*===========================================================================
**  Function :  SYSDBGREADCOREPC
** ==========================================================================
*/
/*!
* 
* @brief
* 	API to read the PC of a particular apps core from Debug UI
* 
* @param[in] 
*  sysdbg cpu 64 bit structure and cpu number
*  
* @par Dependencies
*   None
* 
* 
* @par Side Effects
*   None
* 
*/

void SYSDBGReadCorePC(sysdbg_cpu64_ctxt_regs_type *sysdbg_ctxt_regs, uint32 cpu);

/*===========================================================================
**  Function :  SYSDBGGETCORESTATUS
** ==========================================================================
*/
/*!
* 
* @brief
* 	API to get the status of a particular apps core 
* 
* @param[in] 
*  Core whose status has to be detected
*  
* @par Dependencies
*   None
* 
* @retval
*   Boolean to indicate online status
* 
* @par Side Effects
*   None
* 
*/

boolean SYSDBGGetCoreStatus(uint32);

/*===========================================================================
**  Function :  SYSDBGGETCLUSTERL2VALIDSTATUS
** ==========================================================================
*/
/*!
* 
* @brief
* 	API to get the valid status of a particular cluster's L2 i.e. cache data is
* 	valid or not
* 
* @param[in] 
*  Cluster whose L2 status has to be detected
*  
* @par Dependencies
*   None
* 
* @retval
*   Boolean to indicate online status
* 
* @par Side Effects
*   None
* 
*/

boolean SYSDBGGetClusterL2SValidStatus(uint32 clusternum);

/*===========================================================================
**  Function :  SYSDBGGETNXTACTCORE
** ==========================================================================
*/
/*!
* 
* @brief
* 	API to get the next active core in a cluster
* 
* @param[in] 
*  Core from which the search will start i.e. search all cores to core n to 
*  the max number of cores in cluster and return the first active one
*  
* @par Dependencies
*   None
* 
* @retval
*   ID for the next sequential core after input which is active
* 
* @par Side Effects
*   None
* 
*/

uint32 SYSDBGGetNxtActCore(uint32);

/*===========================================================================
**  Function :  SYSDBGGETCLUSTERL2PCSTATUS
** ==========================================================================
*/
/*!
* 
* @brief
* 	API to get the valid status of a particular cluster's L2 i.e. cache data is
* 	valid or not
* 
* @param[in] 
*  Cluster whose L2 status has to be detected
*  
* @par Dependencies
*   None
* 
* @retval
*   Boolean to indicate online status
* 
* @par Side Effects
*   None
* 
*/

boolean SYSDBGGetClusterL2SPCStatus(void);

/*===========================================================================
**  Function :  SDIGETCORESINCLUSTER
** ==========================================================================
*/
/*!
* 
* @brief
* 	API to get cores in a cluster
* 	 
* @param[in] 
*  Cluster for which info has to be returned
*  
* @par Dependencies
*   None
* 
* @retval
*   Unsigned integer to indicate the number of cores
* 
* @par Side Effects
*   None
* 
*/
uint32 SDIGetCoresInCluster(uint32);

/*===========================================================================
**  Function :  SYSDBG_REGISTER_MONITOR
** ==========================================================================
*/
/*!
* 
* @brief
* Register the entry handler of sysdbg in el1 for sysdbg to return to during wdog reset,
* also contains buffer for context saving to be done in el3
*
* @param[in] 
*  None
*  
* @par Dependencies
*   None
* 
* @retval
*   None
* 
* @par Side Effects
*   None
* 
*/

boolean sysdbg_register_monitor(void);

/*===========================================================================
**  Function :  SYSDBG_ENABLE
** ==========================================================================
*/
/*!
* 
* @brief
* The function that enables the debug through watchdog feature
* 
* @param[in] 
*   None
*
* @par Dependencies
*   None
* 
* @retval
*   None
* 
* @par Side Effects
*   None
* 
*/

boolean sysdbg_enable(void);
/*===========================================================================
**  Function :  SYSDBG_DEBUG_UI_EN
** ==========================================================================
*/
/*!
* 
* @brief
* The function that enables the debug through watchdog feature
* 
* @param[in] 
*   None
*
* @par Dependencies
*   None
* 
* @retval
*   None
* 
* @par Side Effects
*   None
* 
*/

boolean sysdbg_debug_ui_en(void);

/*===========================================================================
**  Function :  SYSDBG_GET_MON_CTXT
** ==========================================================================
*/
/*!
* 
* @brief
* Gets the context buffer saved from EL3 sysdbg routine
*
* @param[in] 
*  None
*  
* @par Dependencies
*   None
* 
* @retval
*   None
* 
* @par Side Effects
*   None
* 
*/

aa_64_ctx_all_els_t* sysdbg_get_mon_ctxt(void);


/*===========================================================================
**  Function :  SYSDBG_RESET_NEXT_CORE
** ==========================================================================
*/
/*!
* 
* @brief
* The function that pulls the next CPU core out of reset. This reset sequence
* is for CORTEX-A53
* 
* @param[in] 
*  None. Current CPU number is obtained from TZ API
*  
* @par Dependencies
*   None
* 
* @retval
*   None
* 
* @par Side Effects
*   None
* 
*/
boolean sysdbg_reset_next_core(void);

/*===========================================================================
**  Function :  DDR_ENTER_SELF_REFRESH_AND_RESET
** ==========================================================================
*/
/*!
* 
* @brief
* The function gets copied to a code region either in IMEM or RPM CODERAM for
* execution and is not called directly by sysdbg
* 
* @param[in] 
*  current cpu and Last active cpu
*  
* @par Dependencies
*   None
* 
* @retval
*   None
* 
* @par Side Effects
*   None
* 
*/

void ddr_enter_self_refresh_and_reset(uint32 cur_cpu, uint64 last_cpu);

/*===========================================================================
**  Function :  SYSDBG_CORE_RESET_SPI
** ==========================================================================
*/
/*!
* 
* @brief
* The function does the core wake up if L2 of secondary cluster is in 
* dormant/standby mode and all cores in that cluster are in power collapse
* we need to wake up one core of secondary cluster to flush its L2 contents.
* 
* @param[in] 
*  Core number to be woken up
*  
* @par Dependencies
*   None
* 
* @retval
*   None
* 
* @par Side Effects
*   None
* 
*/
void SYSDBGCoreResetSPI(uint32);

/*===========================================================================
**  Function :  SYSDBG_EXIT
** ==========================================================================
*/
/*!
* 
* @brief
* clean up function for sysdbg
* copies ddr_enter_self_refresh_reset into a non-ddr memory region,
* then finishes and reset
* 
* @param[in] 
*  None
*  
* @par Dependencies
*   None
* 
* @retval
*   None
* 
* @par Side Effects
*   None
* 
*/

boolean sysdbg_exit(void);

/*===========================================================================
** 	Function: sysdbg_update_core_info
** ==========================================================================
*/
/*!
* 
* @brief
*   Simple API to update total cpu count and cluster count in a multi-cluster
*   multi-core environment and also Boot cpu number.
* 	
* @param[in] 
*	None
*	  
* @par Dependencies
*	TZ API should return proper count info.
* 
* @retval
*   Success / Fail
* 
* @par Side Effects
*	None
*	
*/
boolean sysdbg_update_core_info(void);

#endif /* _SYSDBG_TARGET_H */

