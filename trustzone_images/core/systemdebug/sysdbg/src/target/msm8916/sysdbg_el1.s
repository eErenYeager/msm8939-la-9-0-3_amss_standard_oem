;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;
;             SYSDBG EL1 INITIALIZATION ROUTINE
;
; GENERAL DESCRIPTION
;	This file contains initialization code for system debug. This performs
;	check on reset debug status and calls into tzbsp_monitor for saving ctxt.
;
;
; Copyright 2010-2012 by Qualcomm Technologies, Incorporated.All Rights Reserved.
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;
;                           EDIT HISTORY FOR FILE
;
; This section contains comments describing changes made to the module.
; Notice that changes are listed in reverse chronological order.
;
; when       who     what, where, why
; --------   ---     --------------------------------------------------------
; 10/26/2013 AJC    Add some more code markers
; 09/11/2013 AJC    Avoid clobbering mon mode r14 on 8x26 / 8x10
; 09/04/2013 AJC    Added change to avoid clobbering r0 and r1 on cores other than core 0
; 05/21/2013 AJC    Added change to verify CPU id before booting up after reset	
; 01/29/2013 AJC	Added change to save information from bootloader
; 09/13/2012 AJC	Fixed missing sys mode registers	
; 02/29/2012 AJC	Initial revision for DBI - 8974
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*


;============================================================================
;
;                            MODULE INCLUDES
;
;============================================================================

;============================================================================
;
;                             MODULE DEFINES
;
;============================================================================
;
Mode_USR                EQU    0x10
Mode_FIQ                EQU    0x11
Mode_IRQ                EQU    0x12
Mode_SVC                EQU    0x13
Mode_MON				EQU	   0x16
Mode_ABT                EQU    0x17
Mode_UND                EQU    0x1B
Mode_SYS                EQU    0x1F

I_Bit                   EQU    0x80
F_Bit                   EQU    0x40

; Secure Configuration Register Bits
SCR_NS_BIT                  EQU     0x01    ; Non-Secure (NS) bit
SCR_IRQ_BIT                 EQU     0x02    ; IRQ bit
SCR_FIQ_BIT                 EQU     0x04    ; FIQ bit
SCR_FW_BIT                  EQU     0x10    ; F Bit writable (FW)
SCR_AW_BIT                  EQU     0x20    ; A Bit writable (AW)

; MASKs
DEBUG_RESET_CHECK_MASK	EQU    0x000E0000
GCC_RESET_STATUS        EQU    0x01815008
GCC_RESET_DEBUG         EQU    0x01814000
DEBUG_STATUS_CHECK_MASK	EQU    0x00000038

;============================================================================
;
;                             MODULE IMPORTS
;
;============================================================================
    ; Import the external symbols that are referenced in this module.
    IMPORT system_debug_main
    IMPORT tzbsp_cur_cpu_num
;============================================================================
;
;                             MODULE EXPORTS
;
;============================================================================

    ; Export the external symbols that are referenced in this module.
    EXPORT sysdbg_el1_entry_handler
   
;============================================================================
;
;                             MODULE DATA AREA
;
;============================================================================

    AREA SYSDBG_FUNC, CODE, READONLY
    PRESERVE8


;============================================================================
; Handles the init routine and main entry point for SYSDBG
;
;   Tasks include:
;   (1) Setup reset vectors as needed
;   (2) Setup Stack as needed
;   (3) Jump to system_debug_main for the reset debug case 
;============================================================================
sysdbg_el1_entry_handler
   ; Set the Stack Pointer address for EL1
    ldr     r13, =sysdbg_cpu3_stack
    ldr     r0, =tzbsp_cur_cpu_num
    blx     r0
    cmp     r0, #0
    beq     load_cpu0_stack
    cmp     r0, #1
    beq     load_cpu1_stack
    cmp     r0, #2
    beq     load_cpu2_stack
    cmp     r0, #3
    beq     load_stack_end
load_cpu0_stack     
    ldr     r13, =sysdbg_cpu0_stack
    b       load_stack_end
load_cpu1_stack     
    ldr     r13, =sysdbg_cpu1_stack
    b       load_stack_end
load_cpu2_stack     
    ldr     r13, =sysdbg_cpu2_stack
    b       load_stack_end
load_stack_end
    ldr     r3, =system_debug_main ; jump to system dbg main
    bx      r3 ;never return

;=======================================================================
;
;		Data and Stack for sysdbg;
;
;======================================================================= 
    AREA DBI_DATA , DATA, READWRITE
sysdbg_cpu0_stack_end 			SPACE  0x100
sysdbg_cpu0_stack
sysdbg_cpu1_stack_end 			SPACE  0x100
sysdbg_cpu1_stack
sysdbg_cpu2_stack_end 			SPACE  0x100
sysdbg_cpu2_stack
sysdbg_cpu3_stack_end 			SPACE  0x100
sysdbg_cpu3_stack

; Pointer to the CPU register save datastruct
; IMPORT dbi_cpu_regs

    END


