/*=============================================================================
                        sysdbg Main Control Function

GENERAL DESCRIPTION
		  This module defines objects for the main control loop of the debug image.

	Copyright 2012- 2014 by Qualcomm Technologies, Inc.  All Rights Reserved.
=============================================================================*/

/*=============================================================================

                            EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/systemdebug/sysdbg/src/target/msm8916/sysdbg_target.c#1 $

when       who          what, where, why
--------   ---          --------------------------------------------------
07/02/13    AJCheriyan  Updated for Gandalf
01/28/13	AJCheriyan	Added BOOT_PARTITION_SELECT write for v2
01/15/13	AJCheriyan	Moved reset apps core and debug feature enablement here
02/22/12	AJCheriyan	First cut for Aragorn. First ever sysdbg

===========================================================================*/
#include "comdef.h"
#include "sysdbg_mem_dump.h"
#include "sysdbg_defs.h"
#include "aa64_ctx.h"
#include "sysdbg_pmic.h"
#include "sysdbg_target.h"
#include "tzbsp_sys.h"
#include "tzbsp.h"
#include "tz_mc.h"
#include "sysdbg_hwio.h"
#include "busywait.h"
#include <string.h>

#define SYSTEM_DEBUG_MARKER (SHARED_IMEM_BASE + 0xB14)

extern uint32 Image$$TZBSP_CODE$$Base;
extern void sysdbg_el1_entry_handler(void);

/* Main struct that contains the sysdbg Stats dump */
extern sysdbgStatsDumpType *sysdbgStatsDump;

// CTX saved from Monitor
static __attribute__ ((aligned (16))) aa_64_ctx_all_els_t mon_ctxt_dump;
//extern uint32 Image$$SYSDBG$$Base;

/* Update boot cpu as 0 for 8916 */
uint8 boot_cpu = 0;

extern boolean tzbsp_sysdbg_flat_mapped(void);

/*===========================================================================
**  Function :  SYSDBG_VALID_RESET
** ==========================================================================
*/
/*!
*
* @brief
* 	API to get chip reset reason
* 	Watchdog reset - 0x3 - supported
*
* @param[in]
*  None
*
* @par Dependencies
*   None
*
* @retval
*   Boolean
*
* @par Side Effects
*   None
*
*/

boolean sysdbg_dump_stats_init_flat_mapped(void);
boolean sysdbg_dump_stats_init(void)
{
	static boolean init = FALSE;
	static uint32 reset_reason[6] ={};

	uint32 temp = 0, i = 0 ;

	if (tzbsp_sysdbg_flat_mapped())
	{
		sysdbg_dump_stats_init_flat_mapped();
	}
	else
	{
		if (!init)
		{
			// Set the debug timer after which controller can reset the system
			// 256 sclk cycles = ~ 7.8ms

			// Enable MPM timer to start the parent timer
			HWIO_OUTF(MPM2_MPM_CONTROL_CNTCR, EN, 0x1);

			// We are going to count at XO speed. Configure
			// for XO = 19.2 MHz
			HWIO_OUTF(APCS_QTMR_AC_CNTFRQ, CNTFRQ, 19200000);
			busywait_init();
			// We need to know the PMIC reset reason as well
			// To be able to talk to the PMIC, SPMI init needs to
			// happen and data needs to be saved. Hence, doing it here
			//SPMIFW_Init();

			/* MSM Reset Reasoninit	*/
			reset_reason[0] = HWIO_IN(GCC_RESET_STATUS);
			// Other reset reasons
			// PON Reason 1 - 0x08
			PM_ReadPerphData(PONA, 0x08, &temp);
			reset_reason[1] |= temp << 8;
			//Warm Reset Reason 1 - 0x0A
			PM_ReadPerphData(PONA, 0x0A, &temp);
			//Warm Reset Reason 2 - 0x0B
			PM_ReadPerphData(PONA, 0x0B, &reset_reason[2]);
			reset_reason[2] |= temp << 8;
			// POFF Reason 1 - 0x0C
			PM_ReadPerphData(PONA, 0xC, &temp);
			// POFF Reason 2 - 0x0D
			PM_ReadPerphData(PONA, 0xD, &reset_reason[3]);
			reset_reason[3] |= temp << 8;
			
			// PON PBL STATUS
			PM_ReadPerphData(PONA, 0x07, &temp);
			reset_reason[4] |= temp;
			// PS_HOLD_RESET_CTL
			PM_ReadPerphData(PONA, 0x5A, &temp);
			reset_reason[4] |= temp << 8;
			// PS_HOLD_RESET_CTL2
			PM_ReadPerphData(PONA, 0x5B, &temp);
			reset_reason[4] |= temp << 16;
			
			// Debug Enhancement to Log RTC time. 
			for(i=0; i<4; i++)
			{
				temp = 0;
				reset_reason[5] <<= 8;
				PM_ReadPerphData(RTCA_WR, 0x4B-i, &temp);
				reset_reason[5] |= temp;
			}
			

			// Now populate our dump structure
			sysdbgDumpStats(reset_reason);

			init = TRUE;
		}
	}
	return TRUE;
}



/*===========================================================================
**  Function :  sysdbg_GET_SHARED_IMEM
** ==========================================================================
*/
/*!
*
* @brief
* 	API to get the location of shared imem
*
* @param[in]
*  None
*
* @par Dependencies
*   None
*
* @retval
*   None
*
* @par Side Effects
*   None
*
*/

uint8 *sysdbg_get_shared_imem(void)
{
	return ((uint8 *)(SHARED_IMEM_BASE));
}

/*===========================================================================
**  Function :  SYSDBG_DEBUG_UI_EN
** ==========================================================================
*/
/*!
*
* @brief
* Initializes DEBUG UI hardware block
*
* @param[in]
*  None. Current CPU number is obtained from TZ API
*
* @par Dependencies
*   Nonvoide
*
* @retval
*   None
*
* @par Side Effects
*   None
*
*/
boolean sysdbg_debug_ui_en_flat_mapped(void);
boolean sysdbg_debug_ui_en(void)
{
	if (tzbsp_sysdbg_flat_mapped())
	{
		return sysdbg_debug_ui_en_flat_mapped();
	}
	else
	{
		HWIO_OUTF(GCC_QDSS_DAP_AHB_CBCR, CLK_ENABLE, 0x1);
		HWIO_OUTF(GCC_QDSS_CFG_AHB_CBCR,CLK_ENABLE, 0x1);
		HWIO_OUTF(GCC_QDSS_DAP_CBCR, CLK_ENABLE, 0x1);
		HWIO_OUT(QDSS_WRAPPER_CS_DEBUG_UI_LOCKACCESS, 0xC5ACCE55);
		HWIO_OUTF(QDSS_WRAPPER_DEBUG_UI_SECURE, SEC_CTL, 0x1);
		HWIO_OUT(QDSS_WRAPPER_DEBUG_UI_CTL, 0x1);      // Clear all status
		HWIO_OUT(QDSS_WRAPPER_DEBUG_UI_CTL_MASK, 0x0);     // Clear SW_TRIG_MASK

		return TRUE;
	}
}

/*===========================================================================
**  Function :  SYSDBG_ENABLE
** ==========================================================================
*/
/*!
*
* @brief
* The function that enables the debug through watchdog feature
*
* @param[in]
*   None
*
* @par Dependencies
*   None
*
* @retval
*   None
*
* @par Side Effects
*   None
*
*/
boolean sysdbg_enable_flat_mapped(void);
boolean sysdbg_enable(void)
{
	if (tzbsp_sysdbg_flat_mapped())
	{
		return sysdbg_enable_flat_mapped();
	}
	else
	{
		// We need to enable PROC_HALT_EN so that MSM pulls up PS_HOLD after reset
		//HWIO_OUTF(GCC_RESET_DEBUG, PROC_HALT_EN, 0x1);

		// Set the debug timer after which controller can reset the system
		// 256 sclk cycles = ~ 7.8ms
		HWIO_OUTF(GCC_RESET_DEBUG, PRE_ARES_DEBUG_TIMER_VAL, 0x100);
		// Enable / Disable the debug through watchdog feature
		HWIO_OUTF(GCC_RESET_DEBUG, SECURE_WDOG_DEBUG_EN, 0x1);
		HWIO_OUTF(GCC_RESET_DEBUG, MSM_TSENSE_RESET_DEBUG_EN, 0x1);
		HWIO_OUTF(GCC_RESET_DEBUG, PMIC_ABNORMAL_RESET_DEBUG_EN, 0x1);

			// Set the ETR timer to flush data out
		HWIO_OUT(GCC_FLUSH_ETR_DEBUG_TIMER, 0x180);
		//Set STOP_CAPTURE_DEBUG_TIMER to 2ms
		HWIO_OUT(GCC_STOP_CAPTURE_DEBUG_TIMER, 0x40);
		// Flag to enable Debug
		HWIO_OUTM(TCSR_RESET_DEBUG_SW_ENTRY, 0x1, 0X1);

		sysdbg_debug_ui_en();
		return TRUE;
	}
}

/*===========================================================================
**  Function :  SYSDBGREADCOREPC
** ==========================================================================
*/
/*!
* 
* @brief
* 	API to read the PC of a particular apps core from Debug UI
* 
* @param[in] 
*  sysdbg cpu 64 bit structure and cpu number
*  
* @par Dependencies
*   None
* 
* 
* @par Side Effects
*   None
* 
*/

void SYSDBGReadCorePC(sysdbg_cpu64_ctxt_regs_type *sysdbg_ctxt_regs, uint32 cpu)
{
   	sysdbg_ctxt_regs->pc = HWIO_INI(QDSS_WRAPPER_DEBUG_UI_DATA_n, (cpu*0x4));	//reading PC for current core from DEBUG_UI
	sysdbg_ctxt_regs->pc = ((uint64)(HWIO_INI(QDSS_WRAPPER_DEBUG_UI_DATA_n, (cpu*0x4+3)))<<0x20)|sysdbg_ctxt_regs->pc;	//reading PC for current core from DEBUG_UI
}

/*===========================================================================
**  Function :  SYSDBGGETCORESTATUS
** ==========================================================================
*/
/*!
* 
* @brief
* 	API to get the status of a particular apps core 
* 
* @param[in] 
*  Core whose status has to be detected
*  
* @par Dependencies
*   None
* 
* @retval
*   Boolean to indicate online status
* 
* @par Side Effects
*   None
* 
*/

boolean SYSDBGGetCoreStatus(uint32 corenum)
{
    if (sysdbgStatsDump) 
	if (sysdbgStatsDump->magic == STATS_MAGIC_NUMBER)
		return ((sysdbgStatsDump->l1_valid & (1 << corenum)) >> corenum);
		
	return TRUE;
}

/*===========================================================================
**  Function :  SYSDBGGETCLUSTERL2VALIDSTATUS
** ==========================================================================
*/
/*!
* 
* @brief
* 	API to get the valid status of a particular cluster's L2 i.e. cache data is
* 	valid or not 
* 
* @param[in] 
*  Cluster whose L2 status has to be detected (For 8916: cluster is always 0 )
*  
* @par Dependencies
*   None
* 
* @retval
*   Boolean to indicate online status
* 
* @par Side Effects
*   None
* 
*/

boolean SYSDBGGetClusterL2SValidStatus(uint32 clusternum)
{
   if (sysdbgStatsDump) 
	if (sysdbgStatsDump->magic == STATS_MAGIC_NUMBER)
		return sysdbgStatsDump->l2_valid;
	
	return TRUE;
}

/*===========================================================================
**  Function :  SYSDBG_RESET_NEXT_CORE
** ==========================================================================
*/
/*!
*
* @brief
* The function that pulls the next CPU core out of reset. This reset sequence
* is for CORTEX-A53
*
* @param[in]
*  None. Current CPU number is obtained from TZ API
*
* @par Dependencies
*   None
*
* @retval
*   None
*
* @par Side Effects
*   None
*
*/
boolean sysdbg_reset_next_core(void)
{
	#define APPS_ALIAS_SIZE		0x10000
	volatile uint32 *reg;

	// The next core to be reset is the current_cpu + 1
	uint32 next_core = tzbsp_cur_cpu_num() + 1;
//	if (next_core == 1)
//	{
		// Enable MPM timer to start the parent timer
//		HWIO_OUTF(MPM2_MPM_CONTROL_CNTCR, EN, 0x1);

		// We are going to count at XO speed. Configure
		// for XO = 19.2 MHz
//		HWIO_OUTF(APCS_QTMR_AC_CNTFRQ, CNTFRQ, 19200000);
//		busywait_init();
//	}

	// Program the start address to the reset vector of TZ
	HWIO_OUT(APCS_AA64NAA32_REG, 0x1);
	HWIO_OUT(APCS_CFG_SECURE, 0x1);
/*	reg = (uint32 *)(HWIO_APCS_BOOT_START_ADDR_SEC_ADDR);
	*reg = (&Image$$TZBSP_CODE$$Base|0x1);

	reg = (uint32 *)(HWIO_APCS_CFG_SECURE_ADDR);
	*reg = 0x1;
*/

	if (next_core < TARGET_APPS_CPUS)
	{

			reg = (uint32 *)(HWIO_APCS_ALIAS0_CPU_PWR_CTL_ADDR + (APPS_ALIAS_SIZE * next_core));
			*reg = 0x0000003D;

			reg = (uint32 *)(HWIO_APCS_ALIAS0_APC_PWR_GATE_CTL_ADDR + (APPS_ALIAS_SIZE * next_core));
			*reg = 0x10000001;

			busywait(2);

			reg = (uint32 *)(HWIO_APCS_ALIAS0_CPU_PWR_CTL_ADDR + (APPS_ALIAS_SIZE * next_core));
			*reg = 0x0002001C;

			busywait(2);

			reg = (uint32 *)(HWIO_APCS_ALIAS0_CPU_PWR_CTL_ADDR + (APPS_ALIAS_SIZE * next_core));
			*reg = 0x0002000C;

			reg = (uint32 *)(HWIO_APCS_ALIAS0_CPU_PWR_CTL_ADDR + (APPS_ALIAS_SIZE * next_core));
			*reg = 0x0002008C;

	}
	return TRUE;
}

/*===========================================================================
**  Function :  SYSDBG_REGISTER_MONITOR
** ==========================================================================
*/
/*!
*
* @brief
* Register the entry handler of sysdbg in el1 for sysdbg to return to during wdog reset,
* also contains buffer for context saving to be done in el3
*
* @param[in]
*  None
*
* @par Dependencies
*   None
*
* @retval
*   None
*
* @par Side Effects
*   None
*
*/

boolean sysdbg_register_monitor(void)
{
	mon_register_el1_sysdbg_entry_and_buf((v_addr_t)sysdbg_el1_entry_handler, (v_addr_t)&mon_ctxt_dump);
	return TRUE;
}

/*===========================================================================
**  Function :  SYSDBG_GET_MON_CTXT
** ==========================================================================
*/
/*!
*
* @brief
* Gets the context buffer saved from EL3 sysdbg routine
*
* @param[in]
*  None
*
* @par Dependencies
*   None
*
* @retval
*   None
*
* @par Side Effects
*   None
*
*/

aa_64_ctx_all_els_t* sysdbg_get_mon_ctxt(void)
{
	return &mon_ctxt_dump;
}

/*===========================================================================
**  Function :  DDR_ENTER_SELF_REFRESH_AND_RESET
** ==========================================================================
*/
/*!
*
* @brief
* The function gets copied to a code region either in IMEM or RPM CODERAM for
* execution and is not called directly by sysdbg
*
* @param[in]
*  None
*
* @par Dependencies
*   None
*
* @retval
*   None
*
* @par Side Effects
*   None
*
*/


void ddr_enter_self_refresh_and_reset(uint32 cpu_num)
{
	if(cpu_num == TARGET_APPS_CPUS-1) //only reset if it is the last CPU
	{
		HWIO_OUTF(BIMC_S_DDR0_SHKE_DRAM_MANUAL_0, ENTER_SELF_REFRESH_ASAP, 0x1); //Put DDR into self-refresh
		while(HWIO_INF(BIMC_S_DDR0_SHKE_DRAM_MANUAL_0, ENTER_SELF_REFRESH_ASAP)); //loop until bit is cleared to indicate cmd is executed
	//	HWIO_OUTF(BIMC_S_DDR1_SHKE_DRAM_MANUAL_0, ENTER_SELF_REFRESH_ASAP, 0x1); //Put DDR into self-refresh
	//	while(HWIO_INF(BIMC_S_DDR1_SHKE_DRAM_MANUAL_0, ENTER_SELF_REFRESH_ASAP)); //loop until bit is cleared to indicate cmd is executed

		/* Trigger for pshold reset */
		HWIO_OUTF(MPM2_MPM_PS_HOLD, PSHOLD, 0);
		/* Add dsb instruction to make sure the memory transactions are completed before pulling PS_HOLD low */
		tzbsp_dsb();
  	}
	while(TRUE);
}


/*===========================================================================
**  Function :  SYSDBG_EXIT
** ==========================================================================
*/
/*!
*
* @brief
* clean up function for sysdbg
* copies ddr_enter_self_refresh_reset into a non-ddr memory region,
* then finishes and reset
*
* @param[in]
*  None
*
* @par Dependencies
*   None
*
* @retval
*   None
*
* @par Side Effects
*   None
*
*/
boolean sysdbg_exit_flat_mapped(void);
boolean sysdbg_exit(void)
{
	if (tzbsp_sysdbg_flat_mapped())
	{
		return sysdbg_exit_flat_mapped();
	}
	else
	{
		void (*func_ptr)(uint32) = (void (*)(uint32))(HWIO_IN(TCSR_RESET_DEBUG_SW_ENTRY)|0x1);//Using ENTRY point of SYSDBG in RPM CODERAM
		uint32 cur_cpu  		 = tzbsp_cur_cpu_num();
		void *code_src  		 = (void*)((uint32)ddr_enter_self_refresh_and_reset&0xFFFFFFFC); //current beta compiler doesn't support pragma arm or --arm_only compiler option
		void *code_dest 		 = (void*)((uint32)func_ptr&0xFFFFFFFC); //temp solution is to force thumb code and manipulating the pointers directly

		if(cur_cpu==0)
			memcpy(code_dest, code_src, 0x100);
		if (cur_cpu==(TARGET_APPS_CPUS-1))
		{
			HWIO_OUTF(GCC_RESET_DEBUG, SECURE_WDOG_DEBUG_EN, 0x0);
			HWIO_OUTF(GCC_RESET_DEBUG, MSM_TSENSE_RESET_DEBUG_EN, 0x0);
			HWIO_OUTF(GCC_RESET_DEBUG, PMIC_ABNORMAL_RESET_DEBUG_EN, 0x0);
			HWIO_OUT(TCSR_RESET_DEBUG_SW_ENTRY, 0x0);
			HWIO_OUTF(GCC_RESET_DEBUG, BLOCK_RESIN, 0x0);
			HWIO_OUT(GCC_RESET_STATUS, 0x2);
			(*(uint32*)SYSTEM_DEBUG_MARKER) = 0xDEADD00D;
		}
		tzbsp_isb();
		tzbsp_dsb();
		//should include a DSB & ISB before jumping to self-modified code
		func_ptr(cur_cpu); //never returns
		while(TRUE);
	}
	
	return TRUE;
}

/* Use the C pre-processor to re-define the below macros based on
 * physical addresses. Note that the new defines are valid until
 * the end of the current translation unit */
#undef  GCC_CLK_CTL_REG_REG_BASE
#define GCC_CLK_CTL_REG_REG_BASE				GCC_CLK_CTL_REG_REG_BASE_PHYS
#undef  QDSS_WRAPPER_TOP_BASE
#define QDSS_WRAPPER_TOP_BASE   				QDSS_WRAPPER_TOP_BASE_PHYS
#undef  MPM2_MPM_BASE
#define MPM2_MPM_BASE   						MPM2_MPM_BASE_PHYS
#undef  CORE_TOP_CSR_BASE
#define CORE_TOP_CSR_BASE   					CORE_TOP_CSR_BASE_PHYS
#undef  BIMC_BASE
#define BIMC_BASE   							BIMC_BASE_PHYS
#undef  CORE_TOP_CSR_BASE
#define CORE_TOP_CSR_BASE   					CORE_TOP_CSR_BASE_PHYS

boolean sysdbg_dump_stats_init_flat_mapped(void)
{
	static boolean init = FALSE;
	static uint32 reset_reason[6] ={};

	uint32 temp = 0, i = 0 ;

	if (!init)
	{
		// Set the debug timer after which controller can reset the system
		// 256 sclk cycles = ~ 7.8ms

		// Enable MPM timer to start the parent timer
		HWIO_OUTF(MPM2_MPM_CONTROL_CNTCR, EN, 0x1);

		// We are going to count at XO speed. Configure
		// for XO = 19.2 MHz
		HWIO_OUTF(APCS_QTMR_AC_CNTFRQ, CNTFRQ, 19200000);
		busywait_init();
		// We need to know the PMIC reset reason as well
		// To be able to talk to the PMIC, SPMI init needs to
		// happen and data needs to be saved. Hence, doing it here
		//SPMIFW_Init();

		/* MSM Reset Reasoninit	*/
		reset_reason[0] = HWIO_IN(GCC_RESET_STATUS);
		// Other reset reasons
		// PON Reason 1 - 0x08
		PM_ReadPerphData(PONA, 0x08, &temp);
		reset_reason[1] |= temp << 8;
		//Warm Reset Reason 1 - 0x0A
		PM_ReadPerphData(PONA, 0x0A, &temp);
		//Warm Reset Reason 2 - 0x0B
		PM_ReadPerphData(PONA, 0x0B, &reset_reason[2]);
		reset_reason[2] |= temp << 8;
		// POFF Reason 1 - 0x0C
		PM_ReadPerphData(PONA, 0xC, &temp);
		// POFF Reason 2 - 0x0D
		PM_ReadPerphData(PONA, 0xD, &reset_reason[3]);
		reset_reason[3] |= temp << 8;
		
		// PON PBL STATUS
		PM_ReadPerphData(PONA, 0x07, &temp);
		reset_reason[4] |= temp;
		// PS_HOLD_RESET_CTL
		PM_ReadPerphData(PONA, 0x5A, &temp);
		reset_reason[4] |= temp << 8;
		// PS_HOLD_RESET_CTL2
		PM_ReadPerphData(PONA, 0x5B, &temp);
		reset_reason[4] |= temp << 16;
		
		// Debug Enhancement to Log RTC time. 
		for(i=0; i<4; i++)
		{
			temp = 0;
			reset_reason[5] <<= 8;
			PM_ReadPerphData(RTCA_WR, 0x4B-i, &temp);
			reset_reason[5] |= temp;
		}
		

		// Now populate our dump structure
		sysdbgDumpStats(reset_reason);

		init = TRUE;
	}
	return TRUE;
}

boolean sysdbg_debug_ui_en_flat_mapped(void)
{
	HWIO_OUTF(GCC_QDSS_DAP_AHB_CBCR, CLK_ENABLE, 0x1);
	HWIO_OUTF(GCC_QDSS_CFG_AHB_CBCR,CLK_ENABLE, 0x1);
	HWIO_OUTF(GCC_QDSS_DAP_CBCR, CLK_ENABLE, 0x1);
	HWIO_OUT(QDSS_WRAPPER_CS_DEBUG_UI_LOCKACCESS, 0xC5ACCE55);
	HWIO_OUTF(QDSS_WRAPPER_DEBUG_UI_SECURE, SEC_CTL, 0x1);
	HWIO_OUT(QDSS_WRAPPER_DEBUG_UI_CTL, 0x1);      // Clear all status
	HWIO_OUT(QDSS_WRAPPER_DEBUG_UI_CTL_MASK, 0x0);     // Clear SW_TRIG_MASK

	return TRUE;
}

boolean sysdbg_enable_flat_mapped(void)
{
	// Set the debug timer after which controller can reset the system
	// 256 sclk cycles = ~ 7.8ms
	HWIO_OUTF(GCC_RESET_DEBUG, PRE_ARES_DEBUG_TIMER_VAL, 0x100);
	// Enable / Disable the debug through watchdog feature
	HWIO_OUTF(GCC_RESET_DEBUG, SECURE_WDOG_DEBUG_EN, 0x1);
	HWIO_OUTF(GCC_RESET_DEBUG, MSM_TSENSE_RESET_DEBUG_EN, 0x1);
	HWIO_OUTF(GCC_RESET_DEBUG, PMIC_ABNORMAL_RESET_DEBUG_EN, 0x1);

		// Set the ETR timer to flush data out
	HWIO_OUT(GCC_FLUSH_ETR_DEBUG_TIMER, 0x180);
	//Set STOP_CAPTURE_DEBUG_TIMER to 2ms
	HWIO_OUT(GCC_STOP_CAPTURE_DEBUG_TIMER, 0x40);
	// Flag to enable Debug
	HWIO_OUTM(TCSR_RESET_DEBUG_SW_ENTRY, 0x1, 0X1);

	sysdbg_debug_ui_en_flat_mapped();
	return TRUE;
}


void ddr_enter_self_refresh_and_reset_flat_mapped(uint32 cpu_num)
{
	if(cpu_num == TARGET_APPS_CPUS-1) //only reset if it is the last CPU
	{
		HWIO_OUTF(BIMC_S_DDR0_SHKE_DRAM_MANUAL_0, ENTER_SELF_REFRESH_ASAP, 0x1); //Put DDR into self-refresh
		while(HWIO_INF(BIMC_S_DDR0_SHKE_DRAM_MANUAL_0, ENTER_SELF_REFRESH_ASAP)); //loop until bit is cleared to indicate cmd is executed
	//	HWIO_OUTF(BIMC_S_DDR1_SHKE_DRAM_MANUAL_0, ENTER_SELF_REFRESH_ASAP, 0x1); //Put DDR into self-refresh
	//	while(HWIO_INF(BIMC_S_DDR1_SHKE_DRAM_MANUAL_0, ENTER_SELF_REFRESH_ASAP)); //loop until bit is cleared to indicate cmd is executed

		/* Trigger for pshold reset */
		HWIO_OUTF(MPM2_MPM_PS_HOLD, PSHOLD, 0);
		/* Add dsb instruction to make sure the memory transactions are completed before pulling PS_HOLD low */
		tzbsp_dsb();
	}
	while(TRUE);
}

boolean sysdbg_exit_flat_mapped(void)
{
	void (*func_ptr)(uint32) = (void (*)(uint32))(HWIO_IN(TCSR_RESET_DEBUG_SW_ENTRY)|0x1);//Using ENTRY point of SYSDBG in RPM CODERAM
	uint32 cur_cpu   = tzbsp_cur_cpu_num();
	void  *code_src  = (void*)((uint32)ddr_enter_self_refresh_and_reset_flat_mapped&0xFFFFFFFC); //current beta compiler doesn't support pragma arm or --arm_only compiler option
	void  *code_dest = (void*)((uint32)func_ptr&0xFFFFFFFC); //temp solution is to force thumb code and manipulating the pointers directly

	if(cur_cpu==0)
		memcpy(code_dest, code_src, 0x100);
	if (cur_cpu==(TARGET_APPS_CPUS-1))
	{
		HWIO_OUTF(GCC_RESET_DEBUG, SECURE_WDOG_DEBUG_EN, 0x0);
		HWIO_OUTF(GCC_RESET_DEBUG, MSM_TSENSE_RESET_DEBUG_EN, 0x0);
		HWIO_OUTF(GCC_RESET_DEBUG, PMIC_ABNORMAL_RESET_DEBUG_EN, 0x0);
		HWIO_OUT(TCSR_RESET_DEBUG_SW_ENTRY, 0x0);
		HWIO_OUTF(GCC_RESET_DEBUG, BLOCK_RESIN, 0x0);
		HWIO_OUT(GCC_RESET_STATUS, 0x2);
		(*(uint32*)SYSTEM_DEBUG_MARKER) = 0xDEADD00D;
	}
	tzbsp_isb();
	tzbsp_dsb();
	//should include a DSB & ISB before jumping to self-modified code
	func_ptr(cur_cpu); //never returns
	while(TRUE);
	
	return TRUE;
}

void SYSDBGReadCorePCFlatMapped(sysdbg_cpu64_ctxt_regs_type *sysdbg_ctxt_regs, uint32 cpu)
{
	sysdbg_ctxt_regs->pc = HWIO_INI(QDSS_WRAPPER_DEBUG_UI_DATA_n, (cpu*0x4));	//reading PC for current core from DEBUG_UI
	sysdbg_ctxt_regs->pc = ((uint64)(HWIO_INI(QDSS_WRAPPER_DEBUG_UI_DATA_n, (cpu*0x4+3)))<<0x20)|sysdbg_ctxt_regs->pc;	//reading PC for current core from DEBUG_UI
}
