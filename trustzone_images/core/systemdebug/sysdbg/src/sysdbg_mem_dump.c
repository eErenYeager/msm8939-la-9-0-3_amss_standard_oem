/*=============================================================================
                        sysdbg_ Memory Dump objects 

GENERAL DESCRIPTION     
  This module defines objects for storing the CPU context saved by the debug image.
  
	Copyright 2012 by Qualcomm Technologies, Inc.  All Rights Reserved.
=============================================================================*/

/*=============================================================================

                            EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/systemdebug/sysdbg/src/sysdbg_mem_dump.c#1 $

when       who          what, where, why
--------   ---          --------------------------------------------------
10/02/13    AJCheriyan  Updates to CPU Dump for new format
09/11/13    AJCheriyan  More Robust entry to flush cache routine
09/03/13    AJCheriyan  Restore PBL / sysdbg Boot backed registers
07/16/13    AJCheriyan  Some API updates
07/08/13    AJCheriyan  Retain lower 16 bits of reset reason
05/16/13    AJCheriyan  Unlock ETM registers using OSLAR
06/26/12	AJCheriyan	Added L1 and L2 Save Code
02/26/12	AJCheriyan	First cut for Aragorn. 

===========================================================================*/

#include "sysdbg_mem_dump.h"
//#include "sysdbg_cache.h"
//#include "sysdbg_asm.h"
#include "sysdbg_target.h"
#include "sysdbg_hwio.h"
#include "tzbsp_sys.h"
#include "stddef.h"
#include <stdint.h>
#include "msmhwio.h"
#include "comdef.h"
#include "tzbsp_target.h"
#include "tzbsp_secboot.h"
#include "stringl/stringl.h"
//#include "tzbsp_context_util.h"
#include "aa64_ctx.h"
#include "sysdbg_defs.h"


/*Cold boot success cookie*/
#define SYSTEM_DEBUG_IMAGE_ID 0x5D1

//name used to populate dump_data_type
const char dump_sysdbg_name[] = "sysdbg";

// Main structure containing the register dump
sysdbg_cpu_ctxt_regs_type sysdbg_cpu_regs;

// Main struct containing all the cache dump pointer entries
sysdbgCacheDumpType sysdbgCacheDump;
//static dump_data_type sysdbgCacheDump;

// Main struct containing the information about the L0/L1 cache
//static sysdbgCacheLvlDumpType *sysdbgL1Dump = NULL;

// Main struct containing the information about the L2 cache
//static sysdbgL2DumpType ** const sysdbgL2Dump = (sysdbgL2DumpType **)sysdbg_CACHEDUMP_PTR;
//static sysdbgL2DumpType *sysdbgL2Dump = NULL;
//

// Main struct containing the information for the CPU register dump
//static dump_data_type sysdbgCPUDump;
sysdbgCPUDumpType sysdbgCPUDump;

/* Main struct containing the information for the ETB dump */
//static sysdbgETBDumpType *sysdbgETBDump = NULL;
static dump_data_type *sysdbgETBDump = NULL;

/* Main struct containing the information for ETM dump */
static sysdbgETMDumpType sysdbgETMDump;

/* Main struct containing the information for TMC dump */
static sysdbgTMCDumpType sysdbgTMCDump;

/* Main struct that contains the sysdbg Stats dump */
sysdbgStatsDumpType *sysdbgStatsDump;

// Main global for the CPU count
// Warning : This returns the total no of CPUs that have booted up. 
//static uint32 current_cpu = INVALID_CPU;

/* Main global for cluster count */
//static uint32 current_cluster = INVALID_CLUSTER;

// "Lock" used by all 4 CPU cores
//static uint64 global_lock;

// Pointer to the cpu regs dump structure
sysdbg_cpu_ctxt_regs_type *sysdbg_cpu_ctxt_regs_addr;

/* Overlay structure \c tzbsp_mon_cpu_ctx_t starting from monitor's label
 * \c Workspace_N_Begin to access monitor's saved non-secure context. */
//extern tzbsp_mon_cpu_ctx_t Sysdbg_Workspace_N_Begin;

extern void sysdbg_dcache_flush(uint32 l1_valid, uint32 l2_valid);
extern boolean tzbsp_sysdbg_flat_mapped(void);

extern uint8 boot_cpu;

/*===========================================================================
** 	Function: sysdbg_FlushCache
** ==========================================================================
*/
/*!
* 
* @brief
*	Wrapper function that checks whether cache needs to be flushed or not 
*
* @param[in] 
*	None
*  
* @par Dependencies
*	None
* 
* @retval
*   None
* 
* @par Side Effects
*	Must be called only after global struct is initialized
*
*/
boolean sysdbgFlushCache(void)
{
	uint32 l1_valid = 0, l2_valid = 0, cpu, cluster =0;

	cpu=tzbsp_cur_cpu_num();
	//uint32 cluster = (cpu >= TARGET_CLUSTER_0_SIZE) ? 1 : 0;
	if(cpu >=  TARGET_CLUSTER_0_SIZE)
	   cluster = 1;
		
	/* 8916: Check the TZ SW flags to get the validity of L1 and L2 */
	/*8939/36: Look at the GDHS status of the core to see if the L1 is valid. If closed, assume valid cache. This is bit 7 */
	l1_valid = SYSDBGGetCoreStatus(cpu); 
    l2_valid = SYSDBGGetClusterL2SValidStatus(cluster);
	
	if ((l1_valid) || (l2_valid))
		sysdbg_dcache_flush(l1_valid, l2_valid);
	
	return TRUE;
}

/*===========================================================================
** 	Function: sysdbgDumpStats
** ==========================================================================
*/
/*!
* 
* @brief
* 	Function that contains stats populated for and by sysdbg
* 	Includes 
* 		- State of caches populated by TZ for sysdbg
* 		- Reset reason as seen by sysdbg. Populated by sysdbg*
*
* @param[in] 
*	Pointer to data that contains stats populated by sysdbg
*  
* @par Dependencies
*	None
* 
* @retval
*   Success / Fail
* 
* @par Side Effects
*	Must be called only after global struct is initialized
*
*/
boolean sysdbgDumpStats(uint32 *data)
{
    sysdbgStatsDump = (sysdbgStatsDumpType *)((uint8 *)SYSDBG_IMEM_REGION1_ADDR); //set the table pointer again as it's not retained accross reset
	// Copy the data over
	const uint32 size = sizeof(sysdbgStatsDumpType) - offsetof(sysdbgStatsDumpType, msm_reset_reason);
	memscpy(&(sysdbgStatsDump->msm_reset_reason), size, data, size);
	
	return TRUE;
}	


/*===========================================================================
** 	Function: sysdbgMemoryReset
** ==========================================================================
*/
/*!
PU
* @brief
* 	Function that resets any memory locations that HLOS / TZ has to initialize.
*
*
* @param[in] 
*	None
*  
* @par Dependencies
*	None
* 
* @retval
*   None
* 
* @par Side Effects
*	Check for the "reset" value in the second pass of the debug image and then
*	proceed
*
*/
boolean sysdbgMemoryReset(void)
{
	// Locate the dump table pointer that HLOS will use to give us memory
    uint32 *dump_table_ptr = (uint32 *)((uint8 *)SYSDBG_DUMP_TABLE_ADDR);

	// Reset the value to our magic number
	*dump_table_ptr = SYSDBG_MAGIC_NUMBER;

	// Init our stats dump and set magic numbers
    sysdbgStatsDump = (sysdbgStatsDumpType *)((uint8 *)SYSDBG_IMEM_REGION1_ADDR);
	// Retain the lower 16 bits to contain the value from the previous reset
	// If the magic number is set
//	sysdbgStatsDump->msm_reset_reason = (sysdbg_MAGIC_NUMBER & 0xFFFF0000) | (sysdbgStatsDump->msm_reset_reason & 0xFFFF);

	/*Set cold boot successful cookie*/
	*((uint32 *)((uint8 *)SYSDBG_COLDBOOT_MARKER_ADDR)) = SYSTEM_DEBUG_IMAGE_ID;
	return TRUE;
}	

/*===========================================================================
** 	Function: sysdbgDumpTrace
** ==========================================================================
*/
/*!
* 
* @brief
*	Function that wraps all the trace related dumps. ETM dump is treated separately
*	and needs to happen on each CPU*
*
* @param[in] 
*	None
*  
* @par Dependencies
*	None
* 
* @retval
*   None
* 
* @par Side Effects
*	None
*
*/
boolean sysdbgDumpTrace(void)
{
	/* Dump TMC for ETB */
	sysdbgDumpTMC(TMC_ETB_TYPE);
	/* Dump TMC for ETR */
	sysdbgDumpTMC(TMC_ETR_TYPE);
	/* Dump ETB */
	sysdbgDumpETB();
	return TRUE;
}


/*===========================================================================
** 	Function: check_trace_enabled
** ==========================================================================
*/
/*!
* 
* @brief
*	Helper function that checks if a particular sink is enabled
*/
static boolean check_trace_enabled_flat_mapped(tmc_type tmc);
static boolean check_trace_enabled(tmc_type tmc)
{
    if (tzbsp_sysdbg_flat_mapped()) 
	{ 
		return check_trace_enabled_flat_mapped(tmc); 
	} 
	else 
	{ 
    	switch (tmc)
    	{
    		case TMC_ETB_TYPE:
    		/* Following are the values for mode
    		 * 0x0 - Circular buffer
    		 * 0x1 - SW Fifo
    		 * 0x2 - HW Fifo
    		 * 0x3 - Reserved
    		 * If enabled, it should be configured as a buffer */
    			if (HWIO_INF(QDSS_ETFETB_CTL, TRACECAPTEN) && (!HWIO_INF(QDSS_ETFETB_MODE, MODE)))
    				return TRUE;
    			else
    				return FALSE;
    			
    		
    		case TMC_ETR_TYPE:
    			if (HWIO_INF(QDSS_ETR_CTL, TRACECAPTEN) && (!HWIO_INF(QDSS_ETR_MODE, MODE)))
    				return TRUE;
    			else
    				return FALSE;
    			
    		default:
    			return FALSE;
    	}
    }
}



/*===========================================================================
** 	Function: sysdbgMemoryDumpInit
** ==========================================================================
*/
/*!
* 
* @brief
* 	Function that initalizes the dump structure for the various dump types
*
*
* @param[in] 
*	None
*  
* @par Dependencies
*	None
* 
* @retval
*   None
* 
* @par Side Effects
*	None
*
*/
boolean sysdbgMemoryDumpInit(void)
{
	static boolean initialized = FALSE;
	if (!initialized)
	{
	uint32 i, j, cpu = tzbsp_cur_cpu_num();
	uint32 cluster_num = 0;
	
	
	/* Check the cookie in OCIMEM that HLOS has setup for us */
	// Add the additional checks here including the one for the magic cookie
	dump_table_type ** DumpTable = (dump_table_type **)((uint8 *)sysdbg_get_shared_imem() + 0x10);

	// We will dig out CPU context and other useful jewels only if secure 
	// debug is enabled via fuses / certificate
//	if (!tzbsp_is_debug_enabled() && !tzbsp_is_retail_crash_dump_enable())

    /* KW error fix */
	if (cpu > ((TARGET_CLUSTER_0_SIZE + TARGET_CLUSTER_1_SIZE)-1))
		return FALSE;
		
	/* KW Fix */
	if(cpu >=  TARGET_CLUSTER_0_SIZE)
	   cluster_num = 1; 

	// Check if HLOS has populated the table after we "reset" the value
	if (*DumpTable != (dump_table_type *)SYSDBG_MAGIC_NUMBER)
	{
		for (i = 0; i < (*DumpTable)->num_entries; i++)
		{
			dump_entry_type top_entry = (**DumpTable).entries[i];

            /* Find the table populated for APPS*/
            if ((top_entry.type == MSM_DUMP_TYPE_TABLE) && (MSM_DUMP_CLIENT_APPS == top_entry.id))
            {
                /* We have the apps dump table now. */
                dump_table_type *apps_dump_table = (dump_table_type *)(uintptr_t)top_entry.start_addr;

                /* Parse all the entries within the apps dump table */
                for (j = 0; j < apps_dump_table->num_entries; j++)
                {
                    dump_entry_type entry = apps_dump_table->entries[j];

					/* Find the corresponding entry for each feature for every CPU */
					if ((entry.start_addr) && (entry.type == MSM_DUMP_TYPE_DATA))
					{
                        /* The LSN of the id field indicates which CPU / block when the feature involves
                         * multiple CPUs or blocks*/
                        uint32 count = entry.id & 0xFFFFFFF0;
//                        if (count > MSM_MAX_DUMP )
//                            sysdbg_ERR_FATAL("Invalid Dump Table entry id", entry.id, count, 0);

						switch (count)
						{
						  case CPU_L1_ICACHE_DUMP:
							  if (!sysdbgCacheDump.L1ICacheDump[cpu])
								  sysdbgCacheDump.L1ICacheDump[cpu] =(dump_data_type*)(uintptr_t)entry.start_addr;
							  break;

						  case CPU_L1_DCACHE_DUMP:
							  if (!sysdbgCacheDump.L1DCacheDump[cpu])
								  sysdbgCacheDump.L1DCacheDump[cpu] = (dump_data_type*)(uintptr_t)entry.start_addr;
							  break;

						  case CPU_L2_DCACHE_DUMP:
						        /* cluster_num is to fix KW */
						        if (!sysdbgCacheDump.L2CacheDump[cluster_num])
								  sysdbgCacheDump.L2CacheDump[cluster_num] = (dump_data_type*)(uintptr_t)entry.start_addr;
							  break;
						  
						  case MSM_CPU_REGS_DUMP:
						      /* KW Error Fix */
						      if((entry.id&0x0000000F) >= (TARGET_CLUSTER_0_SIZE+TARGET_CLUSTER_1_SIZE))
							     return FALSE;
                              if (!sysdbgCPUDump.CPUDump[entry.id&0x0000000F])
                                  sysdbgCPUDump.CPUDump[entry.id&0x0000000F] = (dump_data_type*)(uintptr_t)entry.start_addr;
                              break;

                          case MSM_ETB_DUMP:
                              if (!sysdbgETBDump)
                                  sysdbgETBDump = (dump_data_type*)(uintptr_t)entry.start_addr;
                              break;

                          case MSM_ETM_DUMP:
                              if (!sysdbgETMDump.ETMCPU[cpu])
                                  sysdbgETMDump.ETMCPU[cpu] = (dump_data_type*)(uintptr_t)entry.start_addr;
                              break;


						  case MSM_TMC_DUMP:
							   /* KW error fix */
								{
                                    int id = entry.id - MSM_TMC_DUMP;

									if (id < 0  ||  id > 1 )
							          return FALSE;
									if (!sysdbgTMCDump.TMCs[id])
								      sysdbgTMCDump.TMCs[id] = (dump_data_type*)(uintptr_t)entry.start_addr; 
                                }
							  break;

                          //default:
//							  sysdbg_ERR_FATAL(sysdbg_FATAL_ERROR, entry.id, 0, 0); 
						}
					}
				}
			}
		}
	}
	initialized = TRUE;
	}
	return initialized;
}


/*===========================================================================
** 	Function: sysdbgDumpETB
** ==========================================================================
*/
/*!
* 
* @brief
*	Function that dumps the ETB information
*
*
* @param[in] 
*	None
*  
* @par Dependencies
*	None
* 
* @retval
*   None
* 
* @par Side Effects
*	Must be called only after global pointer sysdbgETBDump is initialized
*
*/
boolean sysdbgDumpETBFlatMapped(void);
boolean sysdbgDumpETB(void)
{
	static uint32 timeout = 50;
	uint32 head, data, size, time = 0;

	if (tzbsp_sysdbg_flat_mapped()) 
	{ 
		return sysdbgDumpETBFlatMapped(); 
	} 
	else 
	{ 
		if ((sysdbgETBDump) && (sysdbgETBDump->magic != DUMP_MAGIC_NUMBER))
		{
			sysdbgETBDump->version = 0x11;
			sysdbgETBDumpType *ETBDump = (sysdbgETBDumpType *)(uintptr_t)sysdbgETBDump->start_addr;
			
			// We have do something only if Trace was enabled
			if (check_trace_enabled(TMC_ETB_TYPE))
			{
				// Find the size of the ETB
				size = HWIO_IN(QDSS_ETFETB_RSZ);
				// Unclock ETB
				HWIO_OUT(QDSS_ETFETB_LAR, 0xC5ACCE55);
	//			__DSB();

				// Flush the system and stop formatting when flush completes
				// HWIO_OUTF(QDSS_ETFETB_FFCR, STOPONFL, 0x1);
				// HWIO_OUTF(QDSS_ETFETB_FFCR, FLUSHMAN_W, 0x1);
				// Disable ETB. Should be disabled on a reset
				// HWIO_OUT(QDSS_ETFETB_CTL, 0x0);
				// Poll for the flush bit to go low
				while ((time++ < timeout) && (HWIO_INF(QDSS_ETFETB_FFCR, FLUSHMAN_W)));
				time = 0;
				while ((time++ < timeout) && (!HWIO_INF(QDSS_ETFETB_STS, TMCREADY)));

				// As per the new spec, we should be ready the data directly without keeping track 
				// of the size or current location of the read / write pointers. So keep reading until we 
				// get a 0xFFFFFFFF
				head = 0;
				while (head < size)
				{
					data = HWIO_IN(QDSS_ETFETB_RRD);
					if (data == 0xFFFFFFFF)
						break;
					ETBDump->entries[head] = data;
					head++;
				}

				// Lock ETB again. 
				HWIO_OUT(QDSS_ETFETB_LAR, 0xF00DD00D);

				// Write the magic number
				sysdbgETBDump->magic = DUMP_MAGIC_NUMBER;
				memcpy(sysdbgETBDump->name, "SYSDBG", 6);
			}
		}
	}
	return TRUE;
}

/*===========================================================================
** 	Function: sysdbgDumpETM
** ==========================================================================
*/
/*!
* 
* @brief
*	Function that dumps the ETM information
*
*
* @param[in] 
*	None
*  
* @par Dependencies
*	None
* 
* @retval
*   None
* 
* @par Side Effects
*	Must be called only after global pointer sysdbgETMDump is initialized
*
*/
boolean sysdbgDumpETM(void)
{
	uint32 etmcount, reg;
	uint32 *base;

	/* Make sure pointer is not null */
	etmcount = tzbsp_cur_cpu_num();
	
	/* KW Fix */
	if (etmcount > (TARGET_APPS_CPUS-1))
	   return FALSE;

	base = (uint32 *)((uintptr_t)(CPU0_ETM_BASE + (etmcount * ETM_BLOCK_SIZE)));

	if ((sysdbgETMDump.ETMCPU[etmcount]) && (sysdbgETMDump.ETMCPU[etmcount]->magic == DUMP_MAGIC_NUMBER))
	{

		sysdbgETMDump.ETMCPU[etmcount]->version = 0x11;
		sysdbgETMCPUDumpType *ETMCPU = 	(sysdbgETMCPUDumpType *)(uintptr_t)sysdbgETMDump.ETMCPU[etmcount]->start_addr;

		for (reg = 0; reg < 1024; reg +=1)
		{
			// We need to unlock access to the block
			if (!reg)
			{
				// Disable OS Lock using OSLAR
				*(base + 0x00C0) = 0;
				// Disable global lock
				*(base + 0x03EC) = 0xC5ACCE55;	
			}

			ETMCPU->entries[reg] = *(base + reg);
		}
			
		// Write the magic number to say that we have dumped ETB registers
		sysdbgETMDump.ETMCPU[etmcount]->magic = DUMP_MAGIC_NUMBER;
		memcpy(sysdbgETMDump.ETMCPU[etmcount]->name, "SYSDBG", 6);

	}

    return TRUE;

}

/*===========================================================================
** 	Function: sysdbgDumpTMC
** ==========================================================================
*/
/*!
* 
* @brief
*	Function that dumps the TMC information.
*
*
* @param[in] 
*	None
*  
* @par Dependencies
*	None
* 
* @retval
*   None
* 
* @par Side Effects
*	Must be called only after global pointer sysdbgTMCDump is initialized
*
*/

boolean sysdbgDumpTMC(tmc_type tmc)
{
#define TMC_RRD	0x4
#define TMC_RWD	0x9

	uint32 reg;
   	uint32 *base;

	/* Set the base depending on the TMC */
	switch(tmc)
	{
		case TMC_ETB_TYPE:
		  base = (uint32 *)(QDSS_ETFETB_BASE);
		  break;
		
		case TMC_ETR_TYPE:
		  base = (uint32 *)(QDSS_ETR_ETR_CXTMC_R64W32D_REG_BASE);
		  break;

		default:
		  return FALSE;
	}
	
	/* For flat mapped addressing adjust the base address to ensure that the physical
	 * address is used */
	if(tzbsp_sysdbg_flat_mapped()) 
	{
		base -= (TZBSP_SANDBOX_RELOCATE_OFFSET / sizeof(uint32));
	}

	/* Dump that TMC's registers */
	if ((sysdbgTMCDump.TMCs[tmc]) && (sysdbgTMCDump.TMCs[tmc]->magic!=DUMP_MAGIC_NUMBER))
	{
		sysdbgTMCDump.TMCs[tmc]->version = 0x11;
		sysdbgTMCPerType *TMCs = (sysdbgTMCPerType*)(uintptr_t)sysdbgTMCDump.TMCs[tmc]->start_addr ;
		for(reg = 0; reg < 1024; reg+= 1)
		{
			if (!reg)
				*(base + 0x03EC) = 0xC5ACCE55;

			// Do not touch the RRD and RWD registers as reading them modifies the pointers
			if ((reg == TMC_RRD) || (reg == TMC_RWD))
				continue;

			TMCs->entries[reg] = *(base + reg);
		}
		// Write the magic number to say that we have dumped that tmc's registers
		sysdbgTMCDump.TMCs[tmc]->magic = DUMP_MAGIC_NUMBER;
		memcpy(sysdbgTMCDump.TMCs[tmc]->name, "SYSDBG", 6);
	}
		
	return TRUE;
}


/*===========================================================================
** 	Function: sysdbgCPUDumpCopy
** ==========================================================================
*/
/*!
* 
* @brief
* 	Function that copies the data from internal IMEM structure to external
* 	DDR location
*
*
* @param[in] 
*	None
*  
* @par Dependencies
*	None
* 
* @retval
*   None
* 
* @par Side Effects
*	Must be called only after global pointer is initialized
*
*/
void	SYSDBGReadCorePCFlatMapped(sysdbg_cpu64_ctxt_regs_type *sysdbg_ctxt_regs, uint32 cpu);
boolean sysdbgCPUDumpCopy(void)
{
	volatile uint32 *reg=0;
	// Find the entry for this CPU
	uint32 count=0;
	uint64 *sbl_ctxt_reg=0;
	uint64 *sysdbg_ctxt_reg=0;
	
	uint32 cpu = tzbsp_cur_cpu_num();
	
	/* KW fix */
	if (cpu > ((TARGET_CLUSTER_0_SIZE+TARGET_CLUSTER_1_SIZE)-1))
	   return FALSE;
	   
	if(sysdbgCPUDump.CPUDump[cpu] && sysdbgCPUDump.CPUDump[cpu]->magic != DUMP_MAGIC_NUMBER)
	{
		sysdbgCPUDump.CPUDump[cpu]->version = 0x11; //update version before dump
		aa_64_ctx_all_els_t *mon_cpu_ctx = sysdbg_get_mon_ctxt();
	
		sysdbgCPUCtxtType *sysdbgCPUCtxt = (sysdbgCPUCtxtType*)(uintptr_t)sysdbgCPUDump.CPUDump[cpu]->start_addr;	
		sysdbg_cpu64_ctxt_regs_type *sysdbg_ctxt_regs = (sysdbg_cpu64_ctxt_regs_type*)&(sysdbgCPUCtxt->cpu_regs);
		sysdbgParseMonCtxt(mon_cpu_ctx, sysdbg_ctxt_regs);
		if(cpu==boot_cpu)
		{
			sbl_ctxt_reg=(uint64*)sysdbg_ctxt_regs->x0; //Location of ctxt saved buffer by sbl(Lower 32bit contexts)
			sysdbg_ctxt_reg=(uint64*)sysdbg_ctxt_regs;
			for(count=0; count<31; count++)
			{
				*(sysdbg_ctxt_reg+count) = *(sbl_ctxt_reg+count)|(*(sysdbg_ctxt_reg+count)&0xFFFFFFFF00000000);
			}	
			
		}

		/* Call target specific to read the PC from Debug UI registers */
#if TZBSP_SANDBOX_RELOCATE_OFFSET
		if(tzbsp_sysdbg_flat_mapped())
		{
			SYSDBGReadCorePCFlatMapped(sysdbg_ctxt_regs, cpu);
		}
		else
#endif
		{
			SYSDBGReadCorePC(sysdbg_ctxt_regs, cpu);
		}

		/*
		//reg = (uint32 *)(HWIO_QDSS_WRAPPER_DEBUG_UI_DATA_0_ADDR + (0x10 * cpu));
		sysdbg_ctxt_regs->pc = HWIO_INI(QDSS_WRAPPER_DEBUG_UI_DATA_n, (cpu*0x4));	//reading PC for current core from DEBUG_UI
		sysdbg_ctxt_regs->pc = ((uint64)(HWIO_INI(QDSS_WRAPPER_DEBUG_UI_DATA_n, (cpu*0x4+3)))<<0x20)|sysdbg_ctxt_regs->pc;	//reading PC for current core from DEBUG_UI
		*/
		sysdbgCPUDump.CPUDump[cpu]->magic = DUMP_MAGIC_NUMBER; //update magic after dumping
		memcpy(sysdbgCPUDump.CPUDump[cpu]->name, "SYSDBG", 6);
		sysdbgCPUCtxt -> status[0] = 1; //update status after dumping to indicate A53 dump 
	
	}
	return TRUE;
}

/*===========================================================================
** 	Function: sysdbgParseMonCtxt
** ==========================================================================
*/
/*!
* 
* @brief
*
*	Parses monitor saved context format into sysdbg dump format
*
* @param[in] 
*	None
*  
* @par Dependencies
*	None
* 
* @retval
*   None
* 
* @par Side Effects
*	Must be called only after global pointer is initialized
*
*/
void sysdbgParseMonCtxt(aa_64_ctx_all_els_t *mon_ctxt, sysdbg_cpu64_ctxt_regs_type *sysdbg_ctxt)
{
	aa_64_ctx_t *mon_ctxt_regs = &(mon_ctxt->aa_64_ctx);
	memcpy(sysdbg_ctxt, mon_ctxt_regs, (31*8)); // copy the initial 30 GP registers and X30 LR
	
	sysdbg_ctxt->sp_el2   = mon_ctxt->sp_el2;
	sysdbg_ctxt->elr_el2  = mon_ctxt->elr_el2;
	sysdbg_ctxt->spsr_el2 = mon_ctxt->spsr_el2;
	sysdbg_ctxt->sp_el3   = mon_ctxt->sp_el3;
	
	sysdbg_ctxt->spsr_el3 = mon_ctxt_regs->cpsr_el1;
	sysdbg_ctxt->elr_el3  = mon_ctxt_regs->elr_el3;
	sysdbg_ctxt->sp_el1   = mon_ctxt_regs->sp_el1;
	sysdbg_ctxt->elr_el1  = mon_ctxt_regs->elr_el1;
	sysdbg_ctxt->spsr_el1 = mon_ctxt_regs->spsr_el1;
	sysdbg_ctxt->sp_el0   = mon_ctxt_regs->sp_el0;
}

sysdbgCPUDumpType* sysdbg_get_cpudump()
{
	sysdbgMemoryDumpInit();
	return &sysdbgCPUDump;
}

/* Use the C pre-processor to re-define the below macros based on
 * physical addresses. Note that the new defines are valid until
 * the end of the current translation unit */
#undef  QDSS_QDSS_BASE
#define QDSS_QDSS_BASE  						  QDSS_QDSS_BASE_PHYS
#undef  QDSS_WRAPPER_TOP_BASE
#define QDSS_WRAPPER_TOP_BASE   				  QDSS_WRAPPER_TOP_BASE_PHYS

static boolean check_trace_enabled_flat_mapped(tmc_type tmc)
{
#ifndef FIXME_8952
	switch (tmc)
	{
		case TMC_ETB_TYPE:
		/* Following are the values for mode
		 * 0x0 - Circular buffer
		 * 0x1 - SW Fifo
		 * 0x2 - HW Fifo
		 * 0x3 - Reserved
		 * If enabled, it should be configured as a buffer */
			if (HWIO_INF(QDSS_ETFETB_CTL, TRACECAPTEN) && (!HWIO_INF(QDSS_ETFETB_MODE, MODE)))
				return TRUE;
			else
				return FALSE;
			
		
		case TMC_ETR_TYPE:
			if (HWIO_INF(QDSS_ETR_CTL, TRACECAPTEN) && (!HWIO_INF(QDSS_ETR_MODE, MODE)))
				return TRUE;
			else
				return FALSE;
			
		default:
			return FALSE;
	}
#endif
}

boolean sysdbgDumpETBFlatMapped(void)
{
#ifndef FIXME_8952
	static uint32 timeout = 50;
	uint32 head, data, size, time = 0;

	if ((sysdbgETBDump) && (sysdbgETBDump->magic != DUMP_MAGIC_NUMBER))
	{
		sysdbgETBDump->version = 0x11;
		sysdbgETBDumpType *ETBDump = (sysdbgETBDumpType *)(uintptr_t)sysdbgETBDump->start_addr;
		
		// We have do something only if Trace was enabled
		if (check_trace_enabled(TMC_ETB_TYPE))
		{
			// Find the size of the ETB
			size = HWIO_IN(QDSS_ETFETB_RSZ);
			// Unclock ETB
			HWIO_OUT(QDSS_ETFETB_LAR, 0xC5ACCE55);

			// Poll for the flush bit to go low
			while ((time++ < timeout) && (HWIO_INF(QDSS_ETFETB_FFCR, FLUSHMAN_W)));
			time = 0;
			while ((time++ < timeout) && (!HWIO_INF(QDSS_ETFETB_STS, TMCREADY)));

			// As per the new spec, we should be ready the data directly without keeping track 
			// of the size or current location of the read / write pointers. So keep reading until we 
			// get a 0xFFFFFFFF
			head = 0;
			while (head < size)
			{
				data = HWIO_IN(QDSS_ETFETB_RRD);
				if (data == 0xFFFFFFFF)
					break;
				ETBDump->entries[head] = data;
				head++;
			}

			// Lock ETB again. 
			HWIO_OUT(QDSS_ETFETB_LAR, 0xF00DD00D);

			// Write the magic number
			sysdbgETBDump->magic = DUMP_MAGIC_NUMBER;
			memcpy(sysdbgETBDump->name, "SYSDBG", 6);
		}
	}
#endif
	return TRUE;
}


