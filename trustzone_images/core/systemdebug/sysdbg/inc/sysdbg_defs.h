#ifndef _SYSDBG_MAIN_H
#define _SYSDBG_MAIN_H

/*=============================================================================
                        SYSDBG MAIN Header File 

GENERAL DESCRIPTION     
  This module defines functions for intializing SYSDBG functionalities.
  
	Copyright 2014 by Qualcomm Technologies, Inc.  All Rights Reserved.
=============================================================================*/
#include "msmhwio.h"
#include "comdef.h"
#include "sysdbg_hwio.h"
//#include "HALhwio.h"
/*=============================================================================

                            EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

when       who          what, where, why
--------   ---          -------------------------------------------------- 
01/14/2014 	davidai	Ported SDI to Bear

===========================================================================*/

#define SYSDBG_DUMP_TABLE_ADDR      	(SHARED_IMEM_BASE + 0x010)
#define SYSDBG_WARMBOOT_MARKER_ADDR 	(SHARED_IMEM_BASE + 0xB14)
#define SYSDBG_COLDBOOT_MARKER_ADDR 	(SHARED_IMEM_BASE + 0xB10)
#define SYSDBG_IMEM_REGION1_ADDR    	(SHARED_IMEM_BASE + 0x758)
#define SYSDBG_IMEM_REGION2_ADDR    	(SHARED_IMEM_BASE + 0xAF8)

#define SYSDBG_IMEM_REGION1_SIZE        32
#define SYSDBG_IMEM_REGION2_SIZE    	32

#define SYSDBG_PASS_SIZE   12

/* Pass Entry Functions */
typedef boolean (*sysdbg_pass_fcn_type)(void);
typedef struct
{
	boolean cpu_affinity[TARGET_CLUSTERS][TARGET_CLUSTER_0_SIZE];
	sysdbg_pass_fcn_type sysdbg_fcn;
}sysdbg_pass_entry_type;
	
/* Pass Type */
typedef struct
{
	sysdbg_pass_entry_type fcn_info[SYSDBG_PASS_SIZE];
}sysdbg_pass_sequence_type;

typedef struct
{
	boolean complete[TARGET_CLUSTERS][TARGET_CLUSTER_0_SIZE];
	sysdbg_pass_sequence_type pass_sequence;
}sysdbg_pass_data_type;



/*SDI debug marker structure.

31               7         0
+++++++++++++++++++++++++++
+ FUNC ID +  CURR CPU     +
+++++++++++++++++++++++++++
*/
#define SYSDBG_DEBUG_ENABLE_FLAG 1
#ifdef SYSDBG_DEBUG_ENABLE_FLAG									
#define SYSDBG_FUNC_START_MARKER(fnc, cpu) 					\
	do{														\
		*((uint32*)(SYSDBG_WARMBOOT_MARKER_ADDR)) = (fnc << 8)| cpu;\
	}while(0)
#else	
#define SYSDBG_FUNC_START_MARKER(fnc, cpu) do{}while(0)													
#endif			

/*===========================================================================
**  Function :  SYSTEM_DEBUG_MAIN
** ==========================================================================
*/
/*!
* 
* @brief
* 	Main SYSDBG
* 
* @param[in] 
*  	uint32 pass_count
*  
* @par Dependencies
*   None
* 
* @retval
*   None
* 
* @par Side Effects
*   None
* 
*
*/
void system_debug_main(void);

/*===========================================================================
**  Function :  DEBUG_RESET_BOOT
** ==========================================================================
*/
/*!
* 
* @brief
* The function checks for reset status
* 
* @param[in] 
*   None
*
* @par Dependencies
*   None
* 
* @retval
*   TRUE indicates reset due to crash; FALSE indicates cold boot
* 
* @par Side Effects
*   None
* 
*/

boolean sysdbg_reset_check(void);


/*===========================================================================
**  Function : SYSDBG_EXECUTE_PASS
** ==========================================================================
*/
/*!
* 
* @brief
*	This function is the workhorse for the image. It iteratively scans through 
*	the pass structure and executes every function in there. The functions are
*	classified as init, work and exit functions respectively. Init and exit functions
*	don't return values while work functions return a success / fail value. Every
*	function must be capable of being run on all cores. If you need to distinguish
*	between a particular core, handle it within the function.
*
* @param[in] 
*	The pointer to the SYSDBG pass data structure, pass_count j
*  
* @par Dependencies
*   None
* 
* @retval
*   None
* 
* @par Side Effects
*   Should be capable of handling multi-core situations as needed.
*
*
*/


static void sysdbg_execute_pass(sysdbg_pass_data_type *sysdbg_pass_ptr);

/*===========================================================================
**  Function : SYSDBG_RESET_CHECK
** ==========================================================================
*/
/*!
* 
* @brief
*   API used to check for whether we're in cold boot or WDT reset boot
*
* @param[in] 
*	
*  
* @par Dependencies
*   None
* 
* @retval
*   None
* 
* @par Side Effects
*   
*
*
*/


boolean sysdbg_reset_check(void);

#endif /* _SYSDBG_MAIN_H */
