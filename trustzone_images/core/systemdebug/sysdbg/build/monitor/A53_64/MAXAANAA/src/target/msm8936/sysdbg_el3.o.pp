# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/systemdebug/sysdbg/src/target/msm8936/sysdbg_el3.s"
# 1 "<built-in>" 1
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/systemdebug/sysdbg/src/target/msm8936/sysdbg_el3.s" 2
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;
; SYSDBG INITIALIZATION ROUTINE
;
; GENERAL DESCRIPTION
; This file contains initialization code for system debug. This performs
; check on reset debug status and calls into tzbsp_monitor for saving ctxt.
;
;
; Copyright 2010-2012 by Qualcomm Technologies, Incorporated.All Rights Reserved.
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;
; EDIT HISTORY FOR FILE
;
; This section contains comments describing changes made to the module.
; Notice that changes are listed in reverse chronological order.
;
; when who what, where, why
; -------- --- --------------------------------------------------------
; 10/26/2013 AJC Add some more code markers
; 09/11/2013 AJC Avoid clobbering mon mode r14 on 8x26 / 8x10
; 09/04/2013 AJC Added change to avoid clobbering r0 and r1 on cores other than core 0
; 05/21/2013 AJC Added change to verify CPU id before booting up after reset
; 01/29/2013 AJC Added change to save information from bootloader
; 09/13/2012 AJC Fixed missing sys mode registers
; 02/29/2012 AJC Initial revision for DBI - 8974
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*


# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/monitor/common/inc/ctx_util.h" 1
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/monitor/common/inc/aa64_ctx.h" 1
# 2 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/monitor/common/inc/ctx_util.h" 2

; -------------------
; MACRO: PushTwoA
; -------------------
; Like push, but memory addresses ascend instead of descend
    MACRO
    PushTwoA $x0, $x1, $x2
        stp $x1, $x2, [$x0], #0x10
    MEND

; -------------------
; MACRO: PopTwoA
; -------------------
    MACRO
    PopTwoA $x0, $x1, $x2
        ldp $x2, $x1, [$x0, #-0x10]!
    MEND
;
; TODO: CPACR_EL1 - for access to floating point and Advanced SIMD
; execution
;
; ----------------
; MACRO: SaveCtx
; ----------------
; $sp should be a pointer to the base address of a context structure.
; See aa_64_ctx_t in aa64_ctx.h
    MACRO
    SaveCtx $sp
        PushTwoA $sp, x0, x1
        PushTwoA $sp, x2, x3
        PushTwoA $sp, x4, x5
        PushTwoA $sp, x6, x7
        PushTwoA $sp, x8, x9
        PushTwoA $sp, x10, x11
        PushTwoA $sp, x12, x13
        PushTwoA $sp, x14, x15
        PushTwoA $sp, x16, x17
        PushTwoA $sp, x18, x19
        PushTwoA $sp, x20, x21
        PushTwoA $sp, x22, x23
        PushTwoA $sp, x24, x25
        PushTwoA $sp, x26, x27
        PushTwoA $sp, x28, x29

        mrs x0, SPSR_EL1
        PushTwoA $sp, x30, x0

        mrs x0, ELR_EL1
        mrs x1, SPSR_irq
        PushTwoA $sp, x0, x1

        mrs x0, SPSR_abt
        mrs x1, SPSR_und
        PushTwoA $sp, x0, x1

        mrs x0, SPSR_fiq
        mrs x1, SP_EL1
        PushTwoA $sp, x0, x1

        mrs x0, SP_EL0
        mrs x1, CNTP_TVAL_EL0
        PushTwoA $sp, x0, x1

        mrs x0, CNTP_CVAL_EL0
        mrs x1, CNTP_CTL_EL0
        PushTwoA $sp, x0, x1

        mrs x0, VBAR_EL1
        mrs x1, TTBR1_EL1
        PushTwoA $sp, x0, x1

        mrs x0, TTBR0_EL1
        mrs x1, TCR_EL1
        PushTwoA $sp, x0, x1

        mrs x0, TPIDR_EL0
        mrs x1, TPIDRRO_EL0
        PushTwoA $sp, x0, x1

        mrs x0, TPIDR_EL1
        mrs x1, SCTLR_EL1
        PushTwoA $sp, x0, x1

        mrs x0, PAR_EL1
        mrs x1, MAIR_EL1
        PushTwoA $sp, x0, x1

        mrs x0, ESR_EL1
        mrs x1, FAR_EL1
        PushTwoA $sp, x0, x1

        mrs x0, CSSELR_EL1
        mrs x1, CONTEXTIDR_EL1
        PushTwoA $sp, x0, x1

        mrs x0, AMAIR_EL1
        mrs x1, AFSR1_EL1
        PushTwoA $sp, x0, x1

        mrs x0, AFSR0_EL1
        mrs x1, ACTLR_EL1
        PushTwoA $sp, x0, x1

        mrs x0, CNTKCTL_EL1
        mrs x1, FPEXC32_EL2
        PushTwoA $sp, x0, x1

        mrs x0, DACR32_EL2
        mrs x1, IFSR32_EL2
        PushTwoA $sp, x0, x1

        mrs x0, CPACR_El1
        ldr x1, =0xAABBAABB
        PushTwoA $sp, x0, x1

        mrs x0, SPSR_EL3
        mrs x1, ELR_EL3
        PushTwoA $sp, x0, x1
    MEND

; ----------------
; MACRO: LoadCtx
; ----------------
; $sp should be a pointer to the base address of a context structure.
; See aa_64_ctx_t in aa64_ctx.h. The order of removing items from the
; structure is important to properly restore x0, x1.
    MACRO
    LoadCtx $sp
        add $sp, #((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8)

        PopTwoA $sp, x0, x1
        msr ELR_EL3, x0
        msr SPSR_EL3, x1

        PopTwoA $sp, x0, x1
        ; dummy val in x0
        msr CPACR_El1, x1

        PopTwoA $sp, x0, x1
        msr IFSR32_EL2, x0
        msr DACR32_EL2, x1

        PopTwoA $sp, x0, x1
        msr FPEXC32_EL2, x0
        msr CNTKCTL_EL1, x1

        PopTwoA $sp, x0, x1
        msr ACTLR_EL1, x0
        msr AFSR0_EL1, x1

        PopTwoA $sp, x0, x1
        msr AFSR1_EL1, x0
        msr AMAIR_EL1, x1

        PopTwoA $sp, x0, x1
        msr CONTEXTIDR_EL1, x0
        msr CSSELR_EL1, x1

        PopTwoA $sp, x0, x1
        msr FAR_EL1, x0
        msr ESR_EL1, x1

        PopTwoA $sp, x0, x1
        msr MAIR_EL1, x0
        msr PAR_EL1, x1

        PopTwoA $sp, x0, x1
        msr SCTLR_EL1, x0
        msr TPIDR_EL1, x1

        PopTwoA $sp, x0, x1
        msr TPIDRRO_EL0, x0
        msr TPIDR_EL0, x1

        PopTwoA $sp, x0, x1
        msr TCR_EL1, x0
        msr TTBR0_EL1, x1

        PopTwoA $sp, x0, x1
        msr TTBR1_EL1, x0
        msr VBAR_EL1, x1

        PopTwoA $sp, x0, x1
        msr CNTP_CTL_EL0, x0
        msr CNTP_CVAL_EL0, x1

        PopTwoA $sp, x0, x1
        msr CNTP_TVAL_EL0, x0
        msr SP_EL0, x1

        PopTwoA $sp, x0, x1
        msr SP_EL1, x0
        msr SPSR_fiq, x1

        PopTwoA $sp, x0, x1
        msr SPSR_und, x0
        msr SPSR_abt, x1

        PopTwoA $sp, x0, x1
        msr SPSR_irq, x0
        msr ELR_EL1, x1

        PopTwoA $sp, x1, x30
        msr SPSR_EL1, x1

        PopTwoA $sp, x29, x28
        PopTwoA $sp, x27, x26
        PopTwoA $sp, x25, x24
        PopTwoA $sp, x23, x22
        PopTwoA $sp, x21, x20
        PopTwoA $sp, x19, x18
        PopTwoA $sp, x17, x16
        PopTwoA $sp, x15, x14
        PopTwoA $sp, x13, x12
        PopTwoA $sp, x11, x10
        PopTwoA $sp, x9, x8
        PopTwoA $sp, x7, x6
        PopTwoA $sp, x5, x4
        PopTwoA $sp, x3, x2
        PopTwoA $sp, x1, x0

        add $sp, #((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8)
    MEND

; ---------------------
; MACRO: LoadSysRegsCtx
; ---------------------
; Loads registers from the context in a given register
; Note: we do not need to go in a specific order, since there is no
; concern for corrupting GP registers, but it's good to maintain the
; same order as savectx and loadctx so the operations can be compared,
; and those macros go in a specific order due to GP register
; corruption concern.
    MACRO
    LoadSysRegsCtx $x0, $x1, $x2
        add $x0, #((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) - (((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8))

        PopTwoA $x0, $x1, $x2
        msr SPSR_EL3, $x1
        ; dummy val in $x2

        PopTwoA $x0, $x1, $x2
        msr CPACR_El1, $x1
        msr IFSR32_EL2, $x2

        PopTwoA $x0, $x1, $x2
        msr DACR32_EL2, $x1
        msr FPEXC32_EL2, $x2

        PopTwoA $x0, $x1, $x2
        msr CNTKCTL_EL1, $x1
        msr ACTLR_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr AFSR0_EL1, $x1
        msr AFSR1_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr AMAIR_EL1, $x1
        msr CONTEXTIDR_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr CSSELR_EL1, $x1
        msr FAR_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr ESR_EL1, $x1
        msr MAIR_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr PAR_EL1, $x1
        msr SCTLR_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr TPIDR_EL1, $x1
        msr TPIDRRO_EL0, $x2

        PopTwoA $x0, $x1, $x2
        msr TPIDR_EL0, $x1
        msr TCR_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr TTBR0_EL1, $x1
        msr TTBR1_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr VBAR_EL1, $x1
        msr CNTP_CTL_EL0, $x2

        PopTwoA $x0, $x1, $x2
        msr CNTP_CVAL_EL0, $x1
        msr CNTP_TVAL_EL0, $x2

        PopTwoA $x0, $x1, $x2
        msr SP_EL0, $x1
        msr SP_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr SPSR_fiq, $x1
        msr SPSR_und, $x2

        PopTwoA $x0, $x1, $x2
        msr SPSR_abt, $x1
        msr SPSR_irq, $x2

        PopTwoA $x0, $x1, $x2
        msr ELR_EL1, $x1
        msr SPSR_EL1, $x2

        add $x0, #((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) - (((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8))
    MEND

; ---------------------
; MACRO: SaveSysRegsCtx
; ---------------------
; Saves registers from the context in a given register
    MACRO
    SaveSysRegsCtx $x0, $x1, $x2
        sub $x0, #((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) - (((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8))

        mrs $x1, SPSR_EL1
        mrs $x2, ELR_EL1
        PushTwoA $x0, $x1, $x2

        mrs $x1, SPSR_irq
        mrs $x2, SPSR_abt
        PushTwoA $x0, $x1, $x2

        mrs $x1, SPSR_und
        mrs $x2, SPSR_fiq
        PushTwoA $x0, $x1, $x2

        mrs $x1, SP_EL1
        mrs $x2, SP_EL0
        PushTwoA $x0, $x1, $x2

        mrs $x1, CNTP_TVAL_EL0
        mrs $x2, CNTP_CVAL_EL0
        PushTwoA $x0, $x1, $x2

        mrs $x1, CNTP_CTL_EL0
        mrs $x2, VBAR_EL1
        PushTwoA $x0, $x1, $x2

        mrs $x1, TTBR1_EL1
        mrs $x2, TTBR0_EL1
        PushTwoA $x0, $x1, $x2

        mrs $x1, TCR_EL1
        mrs $x2, TPIDR_EL0
        PushTwoA $x0, $x1, $x2

        mrs $x1, TPIDRRO_EL0
        mrs $x2, TPIDR_EL1
        PushTwoA $x0, $x1, $x2

        mrs $x1, SCTLR_EL1
        mrs $x2, PAR_EL1
        PushTwoA $x0, $x1, $x2

        mrs $x1, MAIR_EL1
        mrs $x2, ESR_EL1
        PushTwoA $x0, $x1, $x2

        mrs $x1, FAR_EL1
        mrs $x2, CSSELR_EL1
        PushTwoA $x0, $x1, $x2

        mrs $x1, CONTEXTIDR_EL1
        mrs $x2, AMAIR_EL1
        PushTwoA $x0, $x1, $x2

        mrs $x1, AFSR1_EL1
        mrs $x2, AFSR0_EL1
        PushTwoA $x0, $x1, $x2

        mrs $x1, ACTLR_EL1
        mrs $x2, CNTKCTL_EL1
        PushTwoA $x0, $x1, $x2

        mrs $x1, FPEXC32_EL2
        mrs $x2, DACR32_EL2
        PushTwoA $x0, $x1, $x2

        mrs $x1, IFSR32_EL2
        mrs $x2, CPACR_El1
        PushTwoA $x0, $x1, $x2

        ldr $x1, =0xAABBAABB
        mrs $x2, SPSR_EL3
        PushTwoA $x0, $x1, $x2

        sub $x0, #((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) - (((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8))
    MEND

; ----------------
; MACRO: MigrateSysregsFromEL3ToEL1
; ----------------
; Takes EL3 system registers and writes to EL1 counterparts
    MACRO
    MigrateSysregsFromEL3ToEL1 $x0, $x1
        mrs $x0, SPSR_EL3
        msr SPSR_EL1, $x0

        mrs $x0, ELR_EL3
        msr ELR_EL1, $x0

        mrs $x0, VBAR_EL3
        msr VBAR_EL1, $x0

        mrs $x0, TTBR0_EL3
        msr TTBR0_EL1, $x0

        mrs $x0, TCR_EL3
        ldr $x1, =(TCR_EL3_T0SZ_BMSK :OR: TCR_EL3_IRGN0_BMSK :OR: TCR_EL3_ORGN0_BMSK :OR: TCR_EL3_SH0_BMSK :OR: TCR_EL3_TG0_BMSK :OR: TCR_EL3_PS_BMSK :OR: TCR_EL3_TBI_BIT)



        and $x0, $x1 ; Mask DC vals

        ubfx $x1, $x0, #TCR_EL3_TBI_SHFT, #(TCR_EL3_TBI_BIT >> TCR_EL3_TBI_SHFT)

                                        ; Extract TBI and put in $x1
        lsl $x1, #TCR_EL1_TBI0_SHFT ; Move TCR_EL3[TBI] to TCR_EL1[TBI0]
        orr $x0, $x1 ; Apply TCR_EL3[TBI] to $x0
        ldr $x1, =TCR_EL3_TBI_BIT
        bic $x0, $x1 ; Clear TCR_EL3[TBI] from $x0

        ubfx $x1, $x0, #TCR_EL3_PS_SHFT, #(TCR_EL3_PS_BMSK >> TCR_EL3_PS_SHFT)

                                        ; Extract PS and put in $x1
        lsl $x1, #TCR_EL1_IPS_SHFT ; Move TCR_EL3[PS] to TCR_EL1[IPS]
        orr $x0, $x1 ; Apply TCR_EL3[PS] to $x0
        ldr $x1, =TCR_EL3_PS_BMSK
        bic $x0, $x1 ; Clear TCR_EL3[PS] from $x0

        msr TCR_EL1, $x0

        mrs $x0, TPIDR_EL3
        msr TPIDR_EL1, $x0

        mrs $x0, SCTLR_EL3
        ldr $x1, =(SCTLR_EL3_M_BIT :OR: SCTLR_EL3_A_BIT :OR: SCTLR_EL3_C_BIT :OR: SCTLR_EL3_SA_BIT :OR: SCTLR_EL3_I_BIT :OR: SCTLR_EL3_WXN_BIT :OR: SCTLR_EL3_EE_BIT)



        and $x0, $x1 ; Mask DC vals
        msr SCTLR_EL1, $x0

        ; no EL3 for PAR

        mrs $x0, MAIR_EL3
        msr MAIR_EL1, $x0

        mrs $x0, ESR_EL3
        msr ESR_EL1, $x0

        mrs $x0, FAR_EL3
        msr FAR_EL1, $x0

        ; no EL3 for CSSELR
        ; no EL3 for CONTEXTIDR

        mrs $x0, AMAIR_EL3
        msr AMAIR_EL1, $x0

        mrs $x0, AFSR1_EL3
        msr AFSR1_EL1, $x0

        mrs $x0, AFSR0_EL3
        msr AFSR0_EL1, $x0

        mrs $x0, ACTLR_EL3 ; TODO: implementation defined
        msr ACTLR_EL1, $x0

        mrs $x0, SPSel
        cmp $x0, #0 ; 0 indicates t, else h
        mov $x0, sp ; $x0 = SBL SP
        beq %f1
        msr SP_EL1, $x0 ; SBL uses sp_el1
        mov $x1, #SPSR_MODE_EL1h ; $x1 = SPSR[M] = SPh
        b %f2
1 msr SP_EL0, $x0 ; SBL uses sp_el0
        mov $x1, #SPSR_MODE_EL1t ; $x1 = SPSR[M] = SPt
2 mrs $x0, DAIF ; $x0 = SPSR[DAIF]
        orr $x0, $x1 ; Combine M with DAIF
        msr SPSR_EL3, $x0 ; set SPSR[MDAIF]
                                       ; TODO: other bits for AArch32 return

    MEND
# 32 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/systemdebug/sysdbg/src/target/msm8936/sysdbg_el3.s" 2

;============================================================================
;
; MODULE DEFINES
;
;============================================================================
;
Mode_USR EQU 0x10
Mode_FIQ EQU 0x11
Mode_IRQ EQU 0x12
Mode_SVC EQU 0x13
Mode_MON EQU 0x16
Mode_ABT EQU 0x17
Mode_UND EQU 0x1B
Mode_SYS EQU 0x1F

I_Bit EQU 0x80
F_Bit EQU 0x40

; Secure Configuration Register Bits
SCR_NS_BIT EQU 0x01 ; Non-Secure (NS) bit
SCR_IRQ_BIT EQU 0x02 ; IRQ bit
SCR_FIQ_BIT EQU 0x04 ; FIQ bit
SCR_FW_BIT EQU 0x10 ; F Bit writable (FW)
SCR_AW_BIT EQU 0x20 ; A Bit writable (AW)

; MASKs
DEBUG_RESET_CHECK_MASK EQU 0x000E0000
GCC_RESET_STATUS EQU 0x01815008
GCC_RESET_DEBUG EQU 0x01814000
DEBUG_STATUS_CHECK_MASK EQU 0x00000038

SYSDBG_EL1_ENTRY_MAGIC EQU 0x5D1E1350
SYSDBG_EL1_EXIT_MAGIC EQU 0x5D1E13A0
SYSDBG_WARMBOOT_MARKER_ADDR EQU 0x8600B14

;============================================================================
;
; MODULE IMPORTS
;
;============================================================================
    ; Import the external symbols that are referenced in this module.

    IMPORT mon_get_sysdbg_buf_addr
    IMPORT mon_get_sysdbg_entry_addr
;============================================================================
;
; MODULE EXPORTS
;
;============================================================================

    ; Export the external symbols that are referenced in this module.
    EXPORT sysdbg_reset_check
    EXPORT sysdbg_entry_handler

;============================================================================
;
; MODULE DATA AREA
;
;============================================================================

    AREA SYSDBG_FUNC, CODE, READONLY
    PRESERVE8

;---------------------------------------------------------------------
; Debug Reset Check
;======================================================================

sysdbg_reset_check FUNCTION
    movz x0, #0x181, lsl #16
    add x0, x0, #0x5000 ;#0x5000 is not a valid #bimm32, orr cannot be used here
    add x0, x0, #0x8 ;add #0x5008 is not valid since exceeds #uimm12 that can be shifted
    ldr x0, [x0] ;check for debug reset
    and x0, x0, #DEBUG_STATUS_CHECK_MASK
    cmp x0, #0
    b.ne due_to_reset
    blr x30 ;
    movz x0, #0x181, lsl #16
    orr x0, x0, #0x4000
    ldr x0, [x0] ;
    and x0, x0, #DEBUG_RESET_CHECK_MASK
    cmp x0, #0
    b.ne due_to_reset
    blr x30 ;return if not due to reset
due_to_reset
    mov x0, #0x1
    blr x30
    ENDFUNC

;============================================================================
; Handles the Ctxt saving and ensuring a clean EL1 entry for SYSDBG
;
; Tasks include:
; (1) Save all GP, register context
; (2) Jump to sysdbg_el1 entry
;============================================================================
sysdbg_entry_handler
    ldr x0, =sysdbg_remapper_reset
    br x0
sysdbg_remapper_reset
 LDR x0, =SYSDBG_WARMBOOT_MARKER_ADDR
 LDR w30, =SYSDBG_EL1_ENTRY_MAGIC
 STR w30, [x0]

    add x0, sp, xzr
    msr TPIDR_EL2, x0 ;save sp to TPIDR_EL2
    bl mon_get_sysdbg_buf_addr ;compiler only uses x0 in this function, no need to back up registers or setup stack
    add sp, x0, xzr
    mrs x0, TPIDR_EL0 ;restore X0
    mrs x30, TPIDR_EL1 ;restore LR
    SaveCtx sp ;call upon common SaveCtx routine
    mrs x0, TPIDR_EL2 ;save previously backed up sp
    mrs x1, SP_EL2
    PushTwoA sp, x0, x1
    mrs x0, SPSR_EL2
    mrs x1, ELR_EL2
    PushTwoA sp, x0, x1
    bl mon_get_sysdbg_entry_addr

    ;-------------------------------------------------------------------
    ; Make sure EL1 CPSR is in good state
    ;-------------------------------------------------------------------
    mov x1, #0xD3
    msr SPSR_EL3, x1

    ;-------------------------------------------------------------------
    ; Set the ELR to the sysdbg entry addr in el1
    ;-------------------------------------------------------------------
    msr ELR_EL3, x0

    LDR x0, =SYSDBG_WARMBOOT_MARKER_ADDR
    LDR w30, =SYSDBG_EL1_EXIT_MAGIC
    STR w30, [x0]
    ;-------------------------------------------------------------------
    ; Ensure all system registers updates are observed
    ;-------------------------------------------------------------------
    dsb sy

    ;-------------------------------------------------------------------
    ; Switch to secure EL1 to call the notifier
    ;-------------------------------------------------------------------
    eret


;=======================================================================
;
; Data for the module
;
;=======================================================================
    AREA DBI_DATA , DATA, READWRITE

; Pointer to the CPU register save datastruct
; IMPORT dbi_cpu_regs

    END
