#ifndef SMMUALLOCATOR_H
#define SMMUALLOCATOR_H
/*=============================================================================

                       S M M U  L I B R A R Y

FILE:      SMMUAllocator.h
   System MMU

GENERAL DESCRIPTION
  This file contains helper functions for memory allocation.

EXTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS
 
===============================================================================
Copyright (c) 2013
Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary
=============================================================================*/
/*=============================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/kernel/smmu/v2/SMMUAllocator.h#1 $
$DateTime: 2018/02/07 00:37:16 $
$Author: mplp4svc $
$Change: 15409075 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
06/21/13   sc      Added allocator functions from sasbe
=============================================================================*/
/* ----------------------------------------------------------------------------
** Include Files
** ------------------------------------------------------------------------- */
#include "com_dtypes.h"

/* ----------------------------------------------------------------------------
** Interfaces
** ------------------------------------------------------------------------- */
/** @name Allocation Interface */
/** @{ */

/**
@brief Allocate 4KB aligned pages from the memory region

   The purpose of this function is to find a free memory region from the memory
   chunk with the passed in \c len.  The allocated memory region bit mask would
   be marked.

@param[in] len - number of bytes

@return The start address value of the aligned pages if successful.
        Otherwise, 0 on allocation error.

@sa SMMU_FreePagesAligned()
*/
uint32 SMMU_AllocatePagesAligned( uint32 len );

/**
@brief Free the memory region specified

   The purpose of this function is free the aligned pages by unmarking the
   memory region bit mask.  The memory itself is not zero'ed out.

@param[in] addr - the start address value
@param[in] len  - number of bytes

@return 0 on success; -1 on error.

@sa SMMU_AllocatePagesAligned()
*/
int32 SMMU_FreePagesAligned( uint32 addr, uint32 len );

/** @} */

#endif // SMMUALLOCATOR_H
