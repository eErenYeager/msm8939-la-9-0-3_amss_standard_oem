#ifndef SMMUINTERNAL_H
#define SMMUINTERNAL_H
/*=============================================================================

                       SMMU Internal Header

FILE:      SMMUInternal.h
   System MMU

GENERAL DESCRIPTION
  This file contains internal SMMU prototypes.

EXTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS
 
===============================================================================
Copyright (c) 2013
Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary
=============================================================================*/
/*=============================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/kernel/smmu/v2/SMMUInternal.h#1 $
$DateTime: 2018/02/07 00:37:16 $
$Author: mplp4svc $
$Change: 15409075 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
10/04/13   sc      Cleaned up
=============================================================================*/
#include "SMMUTypes.h"
#include "SMMUConfig.h"

extern SMMUConfig_t smmuConfigInfo[];

// initialize global register space of the SMMU device
SMMUResult_t SMMU_InitDevice
  ( SMMUDeviceId_t smmuDeviceID,
    SMMUAddr_t pageTablePhysBase );

// validate the SMMU device initialization
SMMUResult_t SMMU_ValidateDeviceInitialized
  ( SMMUDeviceConfigEntry_t *pSMMUDeviceConfigEntry );

// inline helper functions
static __inline boolean SMMU_DEVICEID_VALID
(
  SMMUDeviceId_t smmuDeviceID
)
{
  return ((smmuDeviceID >= 0) && (smmuDeviceID < SMMU_NUM_MAX_DEVICES));
}

static __inline boolean SMMU_CB_VALID
(
  SMMUDeviceId_t smmuDeviceID,
  uint32 cb
)
{
  return (cb < smmuConfigInfo->SMMUDeviceConfig[smmuDeviceID].numNonSecureCBs);
}

static __inline boolean SMMU_DEVICE_CB_VALID
(
  SMMUDeviceId_t smmuDeviceID,
  uint32 cb
)
{
  return (SMMU_DEVICEID_VALID(smmuDeviceID) && SMMU_CB_VALID(smmuDeviceID, cb));
}

#endif /*SMMUINTERNAL_H*/
