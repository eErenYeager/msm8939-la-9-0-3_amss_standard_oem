#ifndef SMMUTRANSLATION_H
#define SMMUTRANSLATION_H
/*=========================================================================

                       S M M U  D A L  L I B R A R Y

FILE:      DALSMMU.h
   System MMU DAL

GENERAL DESCRIPTION
  This file contains data structures and exported API by DAL SMMU

EXTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS
 
Copyright (c) 2010 - 2014
Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary
==========================================================================*/
/*==========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/kernel/smmu/v2/SMMUTranslation.h#1 $
$DateTime: 2018/02/07 00:37:16 $
$Author: mplp4svc $
$Change: 15409075 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------
09/03/13   sc      Updated fault_regs_dump with global space 0 registers
08/21/13   sc      Updated fault_regs_dump
06/15/11   spa     Initial Version
==========================================================================*/

/*----------------------------------------------------------------------------
 * Include Files
 *--------------------------------------------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#include "SMMUTypes.h"
#include "SMMUConfig.h"
#include "HALSMMU.h"

/*Not supporting Large pages*/
typedef enum
{
  SECTION_1MB,
  PAGE_4KB,
  SUPPORTED_SIZE_MAX,
  UNSUPPORTED_SIZE = 0x7FFFFFFF
}mappingType;

/*Format is TEX[2:0]:C:B*/
typedef enum
{
  REGION_STRONGLY_ORDERED,
  REGION_SHARED_DEVICE,
  REGION_INNER_OUTER_WT_NWA,
  REGION_INNER_OUTER_WB_NWA,
  REGION_INNER_OUTER_NON_CACHE,
  REGION_RESERVED,
  REGION_IMP_DEFINED,
  REGION_INNER_OUTER_WB_WA,
  REGION_NON_SHARED_DEVICE,
  REGION_SUPPORTED_MAX,
  REGION_UNSUPPORTED = 0x7FFFFFFF
}regionType;

/*Bit mask of permissions*/
#define PERM_X 0x1
#define PERM_W 0x2
#define PERM_R 0x4

// Use as parameter passing into SMMU_DumpCBFaultRegs to dump
// the global space 0 fault registers
#define SMMU_DUMP_GLOBAL_FAULT 0xFFFFFFFF

/*all mappings are non-cacheable. need this to be configurable?*/
typedef struct map_attributes {
	uint32      virt_addr;
	uint32      phys_addr;
	uint32      size;
	mappingType type;
	uint32      permissions;
  regionType  rngtype;
}map_attributes;


typedef union SMMU_L1_PT_Entry_t {
    struct {
       unsigned TYPE:2;
       unsigned reserved1:1;
       unsigned NS:1;
       unsigned SBZ:1;
	   unsigned domain:4;
       unsigned reserved3:1;
	   unsigned l2pt_base:22;
    }l2pt;

    struct {
       unsigned TYPE:2;
       unsigned B:1;
       unsigned C:1;
       unsigned XN:1;
	   unsigned domain:4;
       unsigned reserved1:1;
       unsigned AP:2;
       unsigned TEX:3;
       unsigned APX:1;
       unsigned S:1;
       unsigned nG:1;
       unsigned SBZ:1;
       unsigned NS:1;
       unsigned PA:12;
    }section;
	uint32 raw;
}SMMU_L1_PT_Entry_t;

typedef union SMMU_L2_PT_Entry_t
{
    struct {
       unsigned XN:1;
       unsigned TYPE:1;
       unsigned B:1;
       unsigned C:1;
       unsigned AP:2;
       unsigned TEX:3;
       unsigned APX:1;
       unsigned S:1;
       unsigned nG:1;
       unsigned PA:20;
    }page;
	uint32 raw;
}SMMU_L2_PT_Entry_t;


/*Format used to dump CB Fault syndrome registers*/
typedef struct fault_regs_dump {
  uint32      dump_size;
  uint32      fsr_addr;
  SMMU_HAL_CBn_FSR_t      fsr;
  uint32      far0_addr;
  uint32      far0;
  uint32      far1_addr;
  uint32      far1;
  uint32      par0_addr;
  uint32      par0;
  uint32      par1_addr;
  uint32      par1;
  uint32      fsyn0_addr;
  uint32      fsyn0;
  uint32      fsyn1_addr;
  uint32      fsyn1;
  uint32      ttbr0_0_addr;
  uint32      ttbr0_0;
  uint32      ttbr0_1_addr;
  uint32      ttbr0_1;
  uint32      ttbr1_0_addr;
  uint32      ttbr1_0;
  uint32      ttbr1_1_addr;
  uint32      ttbr1_1;
  uint32      ttbcr_addr;
  SMMU_HAL_CBn_TTBCR_t      ttbcr;
  uint32      sctlr_addr;
  SMMU_HAL_CBn_SCTLR_t      sctlr;
  uint32      actlr_addr;
  SMMU_HAL_CBn_ACTLR_t      actlr;
  uint32      prrr_addr;
  uint32      prrr;
  uint32      nmrr_addr;
  uint32      nmrr;
  uint32      cbar_addr;
  SMMU_HAL_CBARn_t          cbar;
  uint32      cbfrsynra_addr;
  uint32      cbfrsynra;
}fault_regs_dump;

// The fault values from the global space 0 fault registers can be
// retrieved with SMMU_DUMP_GLOBAL_FAULT as the ctxBankNumber
// in SMMU_DumpCBFaultRegs().
typedef struct
{
  uint32      dump_size;
  // fault info from secure address space
  uint32      gfar0_addr;
  uint32      gfar0;
  uint32      gfar1_addr;
  uint32      gfar1;
  uint32      gfsr_addr;
  SMMU_HAL_GFSR_t      gfsr;
  uint32      gfsynr0_addr;
  uint32      gfsynr0;
  uint32      gfsynr1_addr;
  uint32      gfsynr1;
  uint32      gfsynr2_addr;
  uint32      gfsynr2;
  // fault info from non-secure address space
  uint32      nsgfar0_addr;
  uint32      nsgfar0;
  uint32      nsgfar1_addr;
  uint32      nsgfar1;
  uint32      nsgfsr_addr;
  SMMU_HAL_NSGFSR_t      nsgfsr;
  uint32      nsgfsynr0_addr;
  uint32      nsgfsynr0;
  uint32      nsgfsynr1_addr;
  uint32      nsgfsynr1;
  uint32      nsgfsynr2_addr;
  uint32      nsgfsynr2;
} global_fault_regs_dump;

/*----------------------------------------------------------------------------
* Externalized Function Definitions
* -------------------------------------------------------------------------*/

void
SMMU_PageTableInit(uint32 *PageTableBase, uint32 size);

SMMUResult_t
SMMU_MapAddressRange(SMMUDeviceId_t smmuDeviceID, uint32 ctxBankNumber, uint32 *L1PageTableBase, uint32 *L2PageTableBase, map_attributes *attr, boolean smmu_on);

SMMUResult_t
SMMU_MapAddress(SMMUDeviceId_t smmuDeviceID, uint32 ctxBankNumber, uint32 *L1PageTableBase, uint32 *L2PageTableBase, map_attributes *attr, boolean smmu_on);

//SMMUResult_t
//SMMU_ConfigureCBTranslation(SMMUDeviceConfigEntry_t *pSMMUDeviceConfigEntry, uint32 *L1PageTableBase, uint32 ctxBankNumber, boolean enable);

SMMUResult_t
SMMU_InstanceConfigureCBTranslation(SMMUDeviceId_t smmuDeviceID, uint32 *L1PageTableBase, uint32 ctxBankNumber, boolean enable);

SMMUResult_t
SMMU_UnmapAddressRange(SMMUDeviceId_t smmuDeviceID, uint32 ctxBankNumber, uint32 *L1PageTableBase, uint32 virt_addr, uint32 size , boolean smmu_on);

SMMUResult_t
SMMU_UnmapAddress(SMMUDeviceId_t smmuDeviceID, uint32 ctxBankNumber, uint32 *L1PageTableBase, uint32 virt_addr, uint32 size , boolean smmu_on);

SMMUResult_t   
SMMU_InvalidateCBTLBAll(SMMUDeviceId_t smmuDeviceID, uint32 ctxBankNumber);

/**
@brief Dump the context bank fault syndrome registers

   The purpose of this function is to dump the context bank fault syndrome
   registers.

@param[in] smmuDeviceID - SMMU device ID to be dumped
@param[in] ctxBankNumber - context bank number to be dumped
                           If ctxBankNumber is SMMU_DUMP_GLOBAL_FAULT,
                           values of global space 0 fault registers will be
                           retrieved.
@param[out] buffer - pointer to buffer to get the registers info
@param[in] len - length of the buffer

@return SMMU_ERR_HAL_SUCCESS if successful;
        otherwise non-zero SMMUResult_t enum value
*/
SMMUResult_t
SMMU_DumpCBFaultRegs(SMMUDeviceId_t  smmuDeviceID, uint32 ctxBankNumber, uint32 *buffer, int32 len);

void
SMMU_ClearFaultRegs(SMMUDeviceId_t  smmuDeviceID, uint32 ctxBankNumber);

SMMUResult_t
SMMU_IsPhysAddrMapped(SMMUDeviceId_t  smmuDeviceID, uint32 ctxBankNumber, uint32 *L1PageTableBase, uint32 physaddr, mappingType maptype, boolean smmu_on, uint32 *va);

#ifdef __cplusplus
}
#endif

#endif /* SMMUTRANSLATION_H */
