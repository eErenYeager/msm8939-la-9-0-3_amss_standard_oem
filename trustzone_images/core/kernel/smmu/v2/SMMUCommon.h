#ifndef SMMUCOMMON_H
#define SMMUCOMMON_H
/*=============================================================================

                       SMMU Common APIs

FILE:      SMMUCommon.h
   System MMU

GENERAL DESCRIPTION
  This file contains exported common SMMU APIs.

EXTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS
 
===============================================================================
Copyright (c) 2010 - 2013
Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary
=============================================================================*/
/*=============================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/kernel/smmu/v2/SMMUCommon.h#1 $
$DateTime: 2018/02/07 00:37:16 $
$Author: mplp4svc $
$Change: 15409075 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
10/04/13   sc      Cleaned up
=============================================================================*/
#include "SMMUTypes.h"
#include "SMMUConfig.h"

/**
@brief Initialize TZ SMMU driver

   This function initializes the TZ SMMU driver.

@return 0 if successful; otherwise non-zero value
*/
int SMMU_TZ_Init( void );

/**
@brief Initialize SMMU device

   This function initializes the global register space of the specified
   SMMU device.

@param[in] smmuDeviceID - SMMU device ID to be initialized

@return SMMU_ERR_OK if successful; otherwise non-zero SMMUResult_t enum value

@sa SMMU_ValidateInstanceInitialized()
*/
SMMUResult_t SMMU_InitInstance( SMMUDeviceId_t smmuDeviceID );

/**
@brief Validate initialization of the SMMU device 

   This function validates the initialization of the specified SMMU device.

@param[in] smmuDeviceID - SMMU device ID to be validated

@return SMMU_ERR_OK if successful; otherwise non-zero SMMUResult_t enum value

@sa SMMU_InitInstance()
*/
SMMUResult_t SMMU_ValidateInstanceInitialized( SMMUDeviceId_t smmuDeviceID );

#endif /*SMMUCOMMON_H*/
