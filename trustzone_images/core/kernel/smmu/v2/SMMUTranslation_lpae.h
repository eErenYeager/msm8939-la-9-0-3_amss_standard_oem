#ifndef SMMUTRANSLATION_LPAE_H
#define SMMUTRANSLATION_LPAE_H
/*=============================================================================

                       S M M U  L I B R A R Y

FILE:      SMMUTranslation_lpae.h
   System MMU

GENERAL DESCRIPTION
  This file contains data structures and exported API by SMMU LPAE translation.

EXTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS
 
===============================================================================
Copyright (c) 2013, 2014
Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary
=============================================================================*/
/*=============================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/kernel/smmu/v2/SMMUTranslation_lpae.h#1 $
$DateTime: 2018/02/07 00:37:16 $
$Author: mplp4svc $
$Change: 15409075 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
01/08/14   sc      Updated GetTotalTranslationPTMemorySize description
11/18/13   sc      Updated GetTotalTranslationPTMemorySize to take in pool size
10/03/13   sc      Added IsPhyAddrMapped API
09/03/13   sc      Updated fault_regs_dump with global space 0 registers
08/22/13   sc      Updated the mapping size from uint32 to uint64
08/21/13   sc      Added DumpCBFaultRegs prototype and updated fault_regs_dump
06/18/13   sc      Initial version
=============================================================================*/

#ifdef __cplusplus
extern "C" {
#endif

/* ----------------------------------------------------------------------------
** Includes
** ------------------------------------------------------------------------- */
#include "SMMUTypes.h"
#include "SMMUConfig.h"
#include "HALSMMU.h"

/* ----------------------------------------------------------------------------
** Definitions
** ------------------------------------------------------------------------- */
// permission bit mask
#define SMMU_LPAE_PERM_X 0x1
#define SMMU_LPAE_PERM_W 0x2
#define SMMU_LPAE_PERM_R 0x4

// Use as parameter passing into SMMU_LPAE_DumpCBFaultRegs to dump
// the global space 0 fault registers
#define SMMU_LPAE_DUMP_GLOBAL_FAULT 0xFFFFFFFF

/* ----------------------------------------------------------------------------
** Types
** ------------------------------------------------------------------------- */
// memory attribute indirection
typedef enum
{
                                            // MAIR bits[7:0] encoding
  LPAE_REGION_STRONGLY_ORDERED = 0,         // b00000000
  LPAE_REGION_DEVICE,                       // b00000100
  LPAE_REGION_INNER_OUTER_NON_CACHE,        // b01000100
  // normal, inner outer write-through, read/write allocate
  LPAE_REGION_INNER_OUTER_WT_RWA,           // b10111011
  // normal, inner outer write-through, read allocate, no write allocate
  LPAE_REGION_INNER_OUTER_WT_RA,            // b10101010
  // normal, inner outer write-back, read/write allocate
  LPAE_REGION_INNER_OUTER_WB_RWA,           // b11111111
  // normal, inner outer write-back, no read allocate, write allocate
  LPAE_REGION_INNER_OUTER_WB_WA,            // b11011101
  // normal, inner outer write-back, read allocate, no write allocate
  LPAE_REGION_INNER_OUTER_WB_RA,            // b11101110
  LPAE_REGION_SUPPORTED_MAX,
  LPAE_REGION_UNSUPPORTED = 0x7FFFFFFF, // force to 32-bit enum
} lpae_regionType;

// Refer to ARM Architectuer Reference Manual ARMv7-A and ARMv7-R edition
// memory shareability type for normal memory
// for block/page lower attr
typedef enum
{
  LPAE_NORMAL_NON_SHAREABLE,
  LPAE_NORMAL_OUTER_SHAREABLE = 2,
  LPAE_NORMAL_INNER_SHAREABLE,
  LPAE_MEMORY_SHAREABLE_SUPPORTED_MAX,
  LPAE_MEMORY_SHAREABLE_SIZE = 0x7FFFFFFF,  // force to 32-bit enum
} lpae_memory_shareableType;

// mapping attributes for populating the Block/Page descriptor
typedef struct
{
  uint64                        size; // in bytes
  uint64                        phys_addr;
  uint32                        virt_addr;
  uint32                        permissions;
  lpae_memory_shareableType     shareability;
  lpae_regionType               mem_region;
} lpae_map_attributesType;

// Format used to dump CB Fault syndrome registers
typedef struct lpae_fault_regs_dump
{
  uint32                        dump_size;
  uint32                        fsr_addr;
  SMMU_HAL_CBn_FSR_t            fsr;
  uint32                        far0_addr;
  uint32                        far0;
  uint32                        far1_addr;
  uint32                        far1;
  uint32                        par0_addr;
  uint32                        par0;
  uint32                        par1_addr;
  uint32                        par1;
  uint32                        fsyn0_addr;
  uint32                        fsyn0;
  uint32                        fsyn1_addr;
  uint32                        fsyn1;
  uint32                        ttbr0_0_addr;
  uint32                        ttbr0_0;
  uint32                        ttbr0_1_addr;
  uint32                        ttbr0_1;
  uint32                        ttbr1_0_addr;
  uint32                        ttbr1_0;
  uint32                        ttbr1_1_addr;
  uint32                        ttbr1_1;
  uint32                        ttbcr_addr;
  SMMU_HAL_CBn_TTBCR_t          ttbcr;
  uint32                        sctlr_addr;
  SMMU_HAL_CBn_SCTLR_t          sctlr;
  uint32                        actlr_addr;
  SMMU_HAL_CBn_ACTLR_t          actlr;
  uint32                        mair0_addr;
  uint32                        mair0;
  uint32                        mair1_addr;
  uint32                        mair1;
  // global space 1 registers
  uint32                        cbar_addr;
  SMMU_HAL_CBARn_t              cbar;
  uint32                        cbfrsynra_addr;
  uint32                        cbfrsynra;
} lpae_fault_regs_dump;

// The fault values from the global space 0 fault registers can be
// retrieved with SMMU_LPAE_DUMP_GLOBAL_FAULT as the ctxBankNumber
// in SMMU_LPAE_DumpCBFaultRegs().
typedef struct
{
  uint32                        dump_size;
  // fault info from secure address space
  uint32                        gfar0_addr;
  uint32                        gfar0;
  uint32                        gfar1_addr;
  uint32                        gfar1;
  uint32                        gfsr_addr;
  SMMU_HAL_GFSR_t               gfsr;
  uint32                        gfsynr0_addr;
  uint32                        gfsynr0;
  uint32                        gfsynr1_addr;
  uint32                        gfsynr1;
  uint32                        gfsynr2_addr;
  uint32                        gfsynr2;
  // fault info from non-secure address space
  uint32                        nsgfar0_addr;
  uint32                        nsgfar0;
  uint32                        nsgfar1_addr;
  uint32                        nsgfar1;
  uint32                        nsgfsr_addr;
  SMMU_HAL_NSGFSR_t             nsgfsr;
  uint32                        nsgfsynr0_addr;
  uint32                        nsgfsynr0;
  uint32                        nsgfsynr1_addr;
  uint32                        nsgfsynr1;
  uint32                        nsgfsynr2_addr;
  uint32                        nsgfsynr2;
} lpae_global_fault_regs_dump;

/* ----------------------------------------------------------------------------
** Interfaces
** ------------------------------------------------------------------------- */

/**
@brief Query for total memory size of translation page tables

   The purpose of this function is to query for the total memory size of the
   translation page tables.

@param[in] uMemPoolSize - pool size in bytes from HLOS.  If it is 0, default
                          total memory size 3MB will be returned.

@return Total memory size of translation page tables.

@sa SMMU_LPAE_NotifyTranslationPTMemoryRegion()
*/
uint32 SMMU_LPAE_GetTotalTranslationPTMemorySize
  ( uint32 uMemPoolSize );

/**
@brief Notify translation page table memory region

   The purpose of this function is to notify SMMU the memory region for the
   translation page tables.

@param[in] pMemRegion - pointer to the memory region
@param[in] uSize - total allocated size in bytes

@sa SMMU_LPAE_GetTotalTranslationPTMemorySize()
*/
void SMMU_LPAE_NotifyTranslationPTMemoryRegion
  ( const uint32 *pMemRegion,
    uint32 uSize );

/**
@brief Get the translation page table memory region info

   The purpose of this function is to get the memory region address and size
   for the translation page tables.

@param[out] pMemRegionAddr - pointer to the translation page table memory region
@param[out] pMemSizeInBytes - pointer to the total allocated size in bytes

@return SMMU_ERR_OK if successful; otherwise non-zero SMMUResult_t enum value

@pre SMMU_LPAE_NotifyTranslationPTMemoryRegion() must have been called.
     Otherwise, both out parameters would be 0.

@sa SMMU_LPAE_GetTotalTranslationPTMemorySize()
@sa SMMU_LPAE_NotifyTranslationPTMemoryRegion()
*/
SMMUResult_t SMMU_LPAE_GetTranslationPTMemoryInfo
  ( uint32 *pMemRegionAddr,
    uint32 *pMemSizeInBytes );

/**
@brief Check if tranlsation page table memory region is available

   The purpose of this function is to check if the memory region for the
   translation page tables is available.

@return TRUE if available; otherwise FALSE

@sa SMMU_LPAE_GetTotalTranslationPTMemorySize()
@sa SMMU_LPAE_NotifyTranslationPTMemoryRegion()
@sa SMMU_LPAE_GetTranslationPTMemoryInfo()
*/
boolean SMMU_LPAE_IsMemoryRegionAvail( void );

/**
@brief Configure context bank translation

   The purpose of this function is to configure the context bank translation
   info.

@param[in] smmuDeviceID - SMMU device ID to be configured
@param[in] ctxBankNumber - context bank number to be configured
@param[in] enable - whether MMU is enabled

@return SMMU_ERR_OK if successful; otherwise non-zero SMMUResult_t enum value
*/
SMMUResult_t SMMU_LPAE_InstanceConfigureCBTranslation
  ( SMMUDeviceId_t smmuDeviceID,
    uint32 ctxBankNumber,
    boolean enable );

/**
@brief Map address range

   The purpose of this function is to map the address range.

@param[in] smmuDeviceID - SMMU device ID to have the address mapped
@param[in] ctxBankNumber - context bank number to have the address mapped
@param[in] attr - pointer to the structure of the mapping attributes info
@param[in] smmu_on - indicates SMMU power and clock have been turned on

@return SMMU_ERR_OK if successful; otherwise non-zero SMMUResult_t enum value
*/
SMMUResult_t SMMU_LPAE_MapAddressRange
  ( SMMUDeviceId_t smmuDeviceID,
    uint32 ctxBankNumber,
    const lpae_map_attributesType *attr,
    boolean smmu_on );

/**
@brief Unmap address range

   The purpose of this function is to unmap the address range.

@param[in] smmuDeviceID - SMMU device ID to have the address unmapped
@param[in] ctxBankNumber - context bank number to have the address unmapped
@param[in] virt_addr - virtual address to be unmapped
@param[in] size - size to be unmapped
@param[in] smmu_on - indicates SMMU power and clock have been turned on

@return SMMU_ERR_OK if successful; otherwise non-zero SMMUResult_t enum value
*/
SMMUResult_t SMMU_LPAE_UnmapAddressRange
  ( SMMUDeviceId_t smmuDeviceID,
    uint32 ctxBankNumber,
    uint32 virt_addr,
    uint64 size,
    boolean smmu_on );

/**
@brief Invalidate all TBL entries of the context bank 

   The purpose of this function is to invalidate all TLB entries of the
   context bank.

@param[in] smmuDeviceID - SMMU device ID to be invalidated
@param[in] ctxBankNumber - context bank number to be invalidated

@return SMMU_ERR_OK if successful; otherwise non-zero SMMUResult_t enum value
*/
SMMUResult_t SMMU_LPAE_InvalidateCBTLBAll
  ( SMMUDeviceId_t smmuDeviceID,
    uint32 ctxBankNumber );

/**
@brief Dump the context bank fault syndrome registers

   The purpose of this function is to dump the context bank fault syndrome
   registers.

@param[in] smmuDeviceID - SMMU device ID to be dumped
@param[in] ctxBankNumber - context bank number to be dumped
                           If ctxBankNumber is SMMU_LPAE_DUMP_GLOBAL_FAULT,
                           values of global space 0 fault registers will be
                           retrieved.
@param[out] buffer - pointer to buffer to get the registers info
@param[in] len - length of the buffer

@return SMMU_ERR_HAL_SUCCESS if successful;
        otherwise non-zero SMMUResult_t enum value
*/
SMMUResult_t SMMU_LPAE_DumpCBFaultRegs
  ( SMMUDeviceId_t smmuDeviceID,
    uint32 ctxBankNumber,
    uint32 *buffer,
    int32 len );

/**
@brief Clear the fault status register

   The purpose of this function is to clear the fault status register.

@param[in] smmuDeviceID - SMMU device ID to have the FSR cleared
@param[in] ctxBankNumber - context bank number to have the FSR cleared
*/
void SMMU_LPAE_ClearFaultRegs
  ( SMMUDeviceId_t smmuDeviceID,
    uint32 ctxBankNumber );

/**
@brief Check if the inquired physical address is mapped

   The purpose of this function is to check if the inquired physical address
   is mapped.  If it is mapped, virtual address \c virtaddr is updated with
   SMMU_ERR_OK return status.  The \c size to be inquired is limit to 2MB.

@param[in] smmuDeviceID - SMMU device ID to be inquired
@param[in] ctxBankNumber - context bank number to be inquired
@param[in] physaddr - physical address to be inquired
@param[in] size - mapping size in bytes to be inquired
@param[out] virtaddr - pointer to virtual address to get the mapped virtual
                       address info

@return SMMU_ERR_OK if successful;
        otherwise non-zero SMMUResult_t enum value
*/
SMMUResult_t SMMU_LPAE_IsPhysAddrMapped
  ( SMMUDeviceId_t smmuDeviceID,
    uint32 ctxBankNumber,
    uint64 physaddr,
    uint64 size,
    uint32 *virtaddr );

#ifdef __cplusplus
}
#endif

#endif // SMMUTRANSLATION_LPAE_H
