#ifndef SMMUCONFIG_H
#define SMMUCONFIG_H
/*=========================================================================

                       S M M U  D A L  D R I V E R

FILE:      SMMUCONFIG.h

GENERAL DESCRIPTION
  This file contains data structures needed for driver configuration

EXTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS

==========================================================================
Copyright (c) 2014
Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary
==========================================================================*/
/*==========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/kernel/smmu/v2/8994/tz/SMMUConfig.h#1 $
$DateTime: 2018/02/07 00:37:16 $
$Author: mplp4svc $
$Change: 15409075 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------
01/21/14   sc      Initial Version
==========================================================================*/
#include "SMMUTypes.h"
#include "tzbsp_vmid_config.h"

#define SMMU_NO_ACCESS_VMID      TZBSP_VMID_NOACCESS
#define SMMU_HLOS_VMID           TZBSP_VMID_AP
#define SMMU_CP_VMID             TZBSP_VMID_CP
#define SMMU_VIDEO_VMID          TZBSP_VMID_VIDEO
#define SMMU_MDSS_VMID           TZBSP_VMID_MDSS

#define SMMU_NUM_MAX_DEVICES   3  //maximun number of SMMU instances in the system

#define SMMU_DEFAULT_ASID             0
#define SMMU_NUM_MAX_SIDS             15
#define SMMU_NUM_MAX_CTX_BANKS        5
#define SMMU_MAX_SECURE_MIDS          2

// Default total memory size required for all CBs per devices
// 3MB + 0x1000 is for L1 pt
#define SMMU_LPAE_TOTAL_MEMORY_SIZE_REQ (0x300000 + 0x1000)

/*------------------------------------------------------------------------------
Local typedef definitions
------------------------------------------------------------------------------*/
typedef enum SMMUDeviceId_t {
   SMMU_VENUS,
   SMMU_MDSS,
   SMMU_GPU, // OXILI
   //SMMU_CPP,
   //SMMU_FD,
   SMMU_DEVICE_ID_INVALID = -1
} SMMUDeviceId_t;

typedef struct SMMUCtxBankConfigEntry_t {
   uint8                           clientVMID;         // VMID  of the client master that is using this context bank
   uint8                           interruptIndex;
   uint8                           Valid;
   uint8                           secure;
  // actual number of SIDs configured
  uint16                          uNumCfgSIDs;
  // list of Stream IDs with SMR masked (i.e., exact SMR value) attached to this context bank
  uint32                          uMaskedSIDs[SMMU_NUM_MAX_SIDS];
} SMMUCtxBankConfigEntry_t;

typedef struct SMMUDeviceConfigEntry_t {
   SMMUAddr_t                 baseVirtAddr; // base address for context banks, For TZ, this is actually a physical Address
   uint8                      numNonSecureCBs;
   uint8                      numSecureCBs;
   uint16                     pad;
   uint16                     numSMRGroupOverride;
   uint8                      numCBGroupOverride;
   uint8                      numIRPTOverride;
   int16                      secureMastersList[SMMU_MAX_SECURE_MIDS];
   SMMUCtxBankConfigEntry_t   ctxBankConfigEntry[SMMU_NUM_MAX_CTX_BANKS];
} SMMUDeviceConfigEntry_t;

typedef struct SMMUConfig_t {
   uint32                            numDevices;
   SMMUDeviceConfigEntry_t           *SMMUDeviceConfig;
} SMMUConfig_t;

#endif /*SMMUCONFIG_H*/
