;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;
; T R U S T Z O N E N O N S E C U R E F I Q D E B U G
;
;
; GENERAL DESCRIPTION
; This file contains the Hypervisor init codes
;
; INITIALIZATION AND SEQUENCING REQUIREMENTS
;
; Copyright (c) 2013-2014 by Qualcomm Technologies, Incorporated.All Rights Reserved.
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;
; EDIT HISTORY FOR FILE
;
; This section contains comments describing changes made to the module.
; Notice that changes are listed in reverse chronological order.
;
;
; $Header:
; when who what, where, why
; -------- --- ---------------------------------------------------
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;============================================================================
;
        MACRO
        pusha $stack, $reg
        str $reg, [$stack, #-4]!
        MEND
        MACRO
        popa $stack, $reg
        ldr $reg, [$stack], #4
        MEND
        MACRO
        ENTRY_NODE $node_name
current_node_name SETS "$node_name"
        EXPORT $node_name
        ALIGN
        ROUT
$node_name
        MEND
        MACRO
        ENTRY_NODE_END
$current_node_name._exit
current_node_name SETS ""
        MEND
        MACRO
        LEAF_NODE $node_name
current_node_type SETS "Leaf"
        CODE16
        ENTRY_NODE $node_name
        bx pc
        ALIGN
        CODE32
        EXPORT $node_name._32
$node_name._32
        MEND
        MACRO
        LEAF_NODE_END $node_name
        ASSERT "$current_node_type" = "Leaf"
        ENTRY_NODE_END $node_name
        bx lr
current_node_type SETS ""
        MEND
        MACRO
        LEAF_NODE_16 $node_name
current_node_type SETS "Leaf16"
        CODE16
        ENTRY_NODE $node_name
        MEND
        MACRO
        LEAF_NODE_END_16 $node_name
        ASSERT "$current_node_type" = "Leaf16"
        ENTRY_NODE_END $node_name
        bx lr
current_node_type SETS ""
        MEND
        MACRO
        ALTERNATE_ENTRY_16 $node_name
        ASSERT "$current_node_type" = "Leaf16"
        EXPORT $node_name
$node_name
        MEND
        MACRO
        blatox $destreg
        ROUT

        tst $destreg, #0x01

        ldrne lr, =%1
        ldreq lr, =%2
        bx $destreg
1
        CODE16
        bx pc
        ALIGN
        CODE32
2
        MEND
; MODULE IMPORTS
;
;=======================================================================
        IMPORT |Image$$HYP_CODE_1$$Base|
        IMPORT |Image$$HYP_RW$$Base|
        IMPORT hyp_log_init
        IMPORT hyp_milestone
        IMPORT hyp_mm_cold_init
        IMPORT hyp_mm_warm_init
        IMPORT hyp_log_warmboot_entry
        IMPORT hyp_log_warmboot_exit
        IMPORT hyp_log_exception
        IMPORT hyp_get_forward_spsr
        IMPORT hyp_hvc_handler
        IMPORT hyp_config_download_mode_backup
        IMPORT hyp_log_lel_s
        IMPORT hyp_reg_dump
        EXPORT hyp_main
        EXPORT HYP_SetupMMU
        EXPORT hyp_setS2fatalflag
        EXPORT hyp_getCPUId
    EXPORT hyp_log_area
; -------------------------------
; MACRO: BringRPMOutOfReset
; -------------------------------
; Bring RPM Out of Reset
;
; Arguments:
; rx: Used as a work register, set to zero.
    MACRO
    BringRPMOutOfReset $reg0, $reg1
      LDR $reg0, =0x1860000
      LDR $reg1, [$reg0]
      AND $reg1, $reg1, #0x6
      STR $reg1, [$reg0]
    MEND
; 1
; -------------------
; MACRO: PushTwo
; -------------------
    MACRO
    PushTwo $x0, $x1
       STP $x1, $x0, [SP, #-16]!
    MEND
; -------------------
; MACRO: PopTwo
; -------------------
    MACRO
    PopTwo $x0, $x1
       LDP $x0, $x1, [SP], #16
    MEND
; ----------------
; MACRO: PushCorruptibleRegs
; ----------------
    MACRO
    PushCorruptibleRegs
        PushTwo x0, x1
        PushTwo x2, x3
        PushTwo x4, x5
        PushTwo x6, x7
        PushTwo x8, x9
        PushTwo x10, x11
        PushTwo x12, x13
        PushTwo x14, x15
        PushTwo x16, x17
      PushTwo x29, x30
    MEND
; ----------------
; MACRO: PopCorruptibleRegs
; ----------------
    MACRO
    PopCorruptibleRegs
  PopTwo x30, x29
        PopTwo x17, x16
        PopTwo x15, x14
        PopTwo x13, x12
        PopTwo x11, x10
        PopTwo x9, x8
        PopTwo x7, x6
        PopTwo x5, x4
        PopTwo x3, x2
        PopTwo x1, x0
    MEND
; ----------------
; MACRO: PushActualCorruptibleRegs
; ----------------
    MACRO
    PushActualCorruptibleRegs
        PushTwo x4, x5
        PushTwo x6, x7
        PushTwo x8, x9
        PushTwo x10, x11
        PushTwo x12, x13
        PushTwo x14, x15
        PushTwo x16, x17
        PushTwo x18, x19
        PushTwo x20, x21
        PushTwo x22, x23
        PushTwo x24, x25
        PushTwo x26, x27
        PushTwo x28, x29
        PushTwo x30, xzr
    MEND
; ----------------
; MACRO: PopActualCorruptibleRegs
; ----------------
    MACRO
    PopActualCorruptibleRegs
        PopTwo xzr, x30
        PopTwo x29, x28
        PopTwo x27, x26
        PopTwo x25, x24
        PopTwo x23, x22
        PopTwo x21, x20
        PopTwo x19, x18
        PopTwo x17, x16
        PopTwo x15, x14
        PopTwo x13, x12
        PopTwo x11, x10
        PopTwo x9, x8
        PopTwo x7, x6
        PopTwo x5, x4
        MEND
; -----------------------------------------------------------------------------
; MACRO: GetCPUUniqueID
; -----------------------------------------------------------------------------
; derrive an unique ID from cluster+core combo
  MACRO
  GetCPUUniqueID $x0, $x1
    MRS $x0, VMPIDR_EL2 ; Read virtuallizat multiprocessor ID
    AND $x1, $x0, #0xFF00 ; AFFL1 is the cluster number.
    AND $x0, $x0, #0xFF ; AFFL0 is the CPU number.
    ORR $x0, $x1, LSR #6
  MEND
; ----------------
; MACRO: BackUpRegsForHVC
; ----------------
    MACRO
    BackUpRegsForHVC
       SUB SP, SP, #0x100
       STP x0, x1, [SP]
       STP x2, x3, [SP, #0x10]
       STP x4, x5, [SP, #0x20]
       STP x6, x7, [SP, #0x30]
       STP x8, x9, [SP, #0x40]
       STP x10, x11, [SP, #0x50]
       STP x12, x13, [SP, #0x60]
       STP x14, x15, [SP, #0x70]
       STP x16, x17, [SP, #0x80]
       STP x18, x19, [SP, #0x90]
       STP x20, x21, [SP, #0xA0]
       STP x22, x23, [SP, #0xB0]
       STP x24, x25, [SP, #0xC0]
       STP x26, x27, [SP, #0xD0]
       STP x28, x29, [SP, #0xE0]
   STR x30, [SP, #0xF0]
    MEND
; ----------------
; MACRO: RestoreHVCSMCRegs
; ----------------
    MACRO
    RestoreRegsForHVC
       LDP x0, x1, [SP]
       LDP x2, x3, [SP, #0x10]
       LDP x4, x5, [SP, #0x20]
       LDP x6, x7, [SP, #0x30]
       LDP x8, x9, [SP, #0x40]
       LDP x10, x11, [SP, #0x50]
       LDP x12, x13, [SP, #0x60]
       LDP x14, x15, [SP, #0x70]
       LDP x16, x17, [SP, #0x80]
       LDP x18, x19, [SP, #0x90]
       LDP x20, x21, [SP, #0xA0]
       LDP x22, x23, [SP, #0xB0]
       LDP x24, x25, [SP, #0xC0]
       LDP x26, x27, [SP, #0xD0]
       LDP x28, x29, [SP, #0xE0]
   LDR x30, [SP, #0xF0]
       ADD SP, SP, #0x100
    MEND
; -------------------
; MACRO: BackUp4
; -------------------
  MACRO
    BackUp4 $dst, $x0, $x1, $x2, $x3
    STP $x0, $x1, [$dst], #16
    STP $x2, $x3, [$dst], #16
  MEND
  PRESERVE8
    AREA HYPENTRYCODE, CODE, READONLY, ALIGN=8
        ENTRY
hyp_main
  ; ------------------------------------------------------------
  ; Boot entry function for both warm and cold boot
  ; x0 contains the EL1 exit address
  ; x1 contains the ELF[e_ident[ei_class]] info
  ; x2 contains the hypervisor configuration bitmask
  ; x18-x30 must be preserved
  ; ------------------------------------------------------------
  ; ------------------------------------------------------------------
  ; Program EL2 vector table, no matter cold/warm boot
  ; ------------------------------------------------------------------
  LDR x4, =EL2_vector_table
  MSR VBAR_EL2, x4
  ; ------------------------------------------------------------
  ; Configure ELR_EL2, x0 is Return address passed by Monitor EL3
  ; ------------------------------------------------------------
  MSR ELR_EL2, x0
  ; ------------------------------------------------------------------
  ; Get current CPU
  ; ------------------------------------------------------------------
  GetCPUUniqueID x3, x4
  ; ------------------------------------------------------------------
  ; Setup Stack Pointer for this core
  ; The SP will be reset when every warm boot?
  ; ------------------------------------------------------------------
  LDR x4, =hyp_stack_bottom
  MOV x0, #0x1000
  MUL x0, x3
  SUB sp, x4, x0 ; shift stack to CPU specific stack
  PushTwo x1, x2
  GetCPUUniqueID x0, x2
  BL hyp_log_warmboot_entry
  PopTwo x2, x1
  PushTwo x19, x20
  MOV x19, x3 ; x19 is the current CPU#
  MOV x20, x2 ; x20 is the 1 config bmsk (on cold boot)
  ; ------------------------------------------------------------
  ; Intialize control registers
  ; ------------------------------------------------------------
  MOV x0, #0x02:OR:0x01
  MSR CNTHCTL_EL2, x0
  MOV x0, XZR
  MSR CNTVOFF_EL2, x0
  MSR HSTR_EL2, x0
  MOV x0, #0x33ff
  MSR CPTR_EL2, x0
  ; ------------------------------------------------------------
  ; Configure HCR
  ; ------------------------------------------------------------
  CMP x1, #1
  BEQ EL1_Is_AArch32
  CMP x1, #2
  BEQ EL1_Is_AArch64
Invalid_ELF_Class_Info
  B Invalid_ELF_Class_Info
  ; Configure HCR_EL2 for EL1
EL1_Is_AArch64
  MOV x4, #0x80000000
  B %f1
EL1_Is_AArch32
  MRS x0, SCTLR_EL1
  ORR x0, x0, #0x20
  MSR SCTLR_EL1, x0
  MOV x4, XZR
1 MSR HCR_EL2, x4
  ; ------------------------------------------------------------
  ; Configure SPSR_EL2 according to EL1 is AArch32 or AArch64
  ; ------------------------------------------------------------
  TST x4, #0x80000000
  BNE SPSR_EL1_Is_AArch64
  MRS x0, ELR_EL2
  TST x0, #1
  BNE SPSR_EL1_Is_AArch32_Thumb
SPSR_EL1_Is_AArch32_ARM32
  MOV x0, #0x00000080:OR:0x00000010:OR:0x00000003
  B %f1
SPSR_EL1_Is_AArch32_Thumb
  MOV x0, #0x00000080:OR:0x00000010:OR:0x00000003:OR:0x00000020
  B %f1
SPSR_EL1_Is_AArch64
  MOV x0, #0x00000080:OR:0x00000005
  B %f1
1 MSR SPSR_EL2, x0
  LDR x4, =hyp_is_init
  LDRB w5, [x4] ; Read flag to determine if hypervisor has been initialized
  CBNZ w5, hyp_warmboot ; skip cold boot code if hyp_is_init != 0
  ; ------------------------------------------------------------
  ; Cold boot INITIALIZATION
  ; ------------------------------------------------------------
  MOV w5, #1
  STRB w5, [x4] ; Write 1 to hyp_is_init
  LDR x4, =hyp_config_bmsk
  STR x20, [x4]
  LDR x0, =|Image$$HYP_CODE_1$$Base|
  BL hyp_config_download_mode_backup
  ; ------------------------------------------------------------
  ; Configure logging, in cold boot only
  ; ------------------------------------------------------------
  BL hyp_log_init
  GetCPUUniqueID x0, x1
  BL hyp_milestone
  CBNZ x20, %f1
  LDR x0, =|Image$$HYP_RW$$Base|
  BL hyp_mm_cold_init
1
  ; ------------------------------------------------------------
  ; Bring RPM out of reset before return to EL1
  ; ------------------------------------------------------------
  BringRPMOutOfReset x0, x1
  B %f1
hyp_warmboot
  LDR x4, =hyp_config_bmsk
  LDR x20, [x4]
1
  CBNZ x20, %f1
  BL hyp_mm_warm_init
1
  ; ------------------------------------------------
  ; Enable Hypervisor MMU
  ; ------------------------------------------------
  TLBI ALLE2
  DSB SY
  ISB
  MOV x0, #0x01004 ; Set SCTLR_EL2.I .C
  CBNZ x20, %f1
  ORR x0, #1 ; set SCTLR_EL2.M
1 MSR SCTLR_EL2, x0
  ISB
  TLBI ALLE2
  DSB SY
  ISB
  ; configure HCR
  CBNZ x20, %f1
  ; Invalidate all EL1&0 regime stage 1 and 2 TLB entries
  TLBI ALLE1
  DSB SY
  ISB
  ; Setup HCR_EL2
  MRS x0, HCR_EL2
  ORR x0, x0, #0x00000001
  ORR x0, x0, #0x00000004
  MSR HCR_EL2, x0
  ISB
  ; Invalidate all EL1&0 regime stage 1 and 2 TLB entries (again)
  TLBI ALLE1
  DSB SY
  ISB
1
  ; update CPU boot state (for debugging)
  LDR x4, =hyp_cpu_boot_state
  MOV w5, #1 ; TODO: use a defined constant
  STRB w5, [x4, x19] ; Indicate CPU booted
  GetCPUUniqueID x0, x1
  BL hyp_log_warmboot_exit
  ; ------------------------------------------------------------
  ; Clear out all work registers before jump to another
  ; exception level
  ; ------------------------------------------------------------
  MOV x0, XZR
  MOV x1, XZR
  MOV x2, XZR
  MOV x3, XZR
  MOV x4, XZR
  MOV x5, XZR
  MOV x6, XZR
  MOV x7, XZR
  MOV x8, XZR
  MOV x9, XZR
  MOV x10, XZR
  MOV x11, XZR
  MOV x12, XZR
  MOV x13, XZR
  MOV x14, XZR
  MOV x15, XZR
  MOV x16, XZR
  MOV x17, XZR
  PopTwo x20, x19
  ; ------------------------------------------------------------
  ; Return to NS EL1
  ; ------------------------------------------------------------
  ERET
; ------------------------------------------------------------
; Vector table
; ------------------------------------------------------------
;--------------------------------------------------------------------
; EL2 using SP from EL0 vectors (not used)
;--------------------------------------------------------------------
  AREA EL2_SP0_S, CODE, READONLY, ALIGN=7
EL2_vector_table
; Current Exception level with SP_EL0.
  B el2_sp_el0_synch
  AREA EL2_SP0_I, CODE, READONLY, ALIGN=7
  PushTwo x0, x30
  MOV x0, #9
  BL hyp_log_exception
  PopTwo x30, x0
el2_sp_el0_irq_loop
  B el2_sp_el0_irq_loop
  AREA EL2_SP0_F, CODE, READONLY, ALIGN=7
  PushTwo x0, x30
  MOV x0, #10
  BL hyp_log_exception
  PopTwo x30, x0
el2_sp_el0_fiq_loop
  B el2_sp_el0_fiq_loop
  AREA EL2_SP0_E, CODE, READONLY, ALIGN=7
  PushTwo x0, x30
  MOV x0, #11
  MRS x1, ELR_EL2
  BL hyp_log_exception
  PopTwo x30, x0
el2_sp_el0_error_loop
  B el2_sp_el0_error_loop
;--------------------------------------------------------------------
; EL2 using SP from EL2 vectors (IRQ, FIQ, etc...)
;--------------------------------------------------------------------
; Current Exception level with SP_ELx
  AREA EL2_SP2_S, CODE, READONLY, ALIGN=7
  PushTwo x0, x1
  MOV x0, #12
  MRS x1, ESR_EL2
  BL hyp_log_exception
  PopTwo x1, x0
  B el2_sp_el2_sync
  AREA EL2_SP2_I, CODE, READONLY, ALIGN=7
  PushTwo x0, x30
  MOV x0, #13
  BL hyp_log_exception
  PopTwo x30, x0
  B el2_sp_el2_irq
  AREA EL2_SP2_F, CODE, READONLY, ALIGN=7
  PushTwo x0, x30
  MOV x0, #14
  BL hyp_log_exception
  PopTwo x30, x0
  B el2_sp_el2_fiq
  AREA EL2_SP2_E, CODE, READONLY, ALIGN=7
  PushTwo x0, x1
  MOV x0, #15
  MRS x1, ESR_EL2
  BL hyp_log_exception
  PopTwo x1, x0
  B el2_sp_el2_error
;--------------------------------------------------------------------
; Lower Exception level, where the implemented
; level immediately lower than the target level is
; using AArch64.
;--------------------------------------------------------------------
  AREA LOW_EL_64_S, CODE, READONLY, ALIGN=7
  PushTwo x0, x30
  MRS x0, ESR_EL2
  LSR x0, #26
  CMP x0, #0x22
  PopTwo x30, x0
  BEQ HypForwardAArch64MisalignedPCException
  PushTwo x15, x16
  MRS x15, ESR_EL2
  LSR x15, x15, #26
  CMP x15, #0x16
  BEQ hvc64call
  CMP x15, #0x12
  BEQ hvc64call
  BNE low_el_sync
  AREA LOW_EL_64_I, CODE, READONLY, ALIGN=7
  PushTwo x0, x30
  MOV x0, #17
  BL hyp_log_exception
  PopTwo x30, x0
  B low_el_64_irq
  AREA LOW_EL_64_F, CODE, READONLY, ALIGN=7
  PushTwo x0, x30
  MOV x0, #18
  BL hyp_log_exception
  PopTwo x30, x0
  B low_el_64_fiq
  AREA LOW_EL_64_E, CODE, READONLY, ALIGN=7
  PushTwo x0, x30
  MOV x0, #19
  BL hyp_log_exception
  PopTwo x30, x0
  B low_el_64_error
;--------------------------------------------------------------------
; Lower Exception level, where the implemented
; level immediately lower than the target level is
; using AArch32.
;--------------------------------------------------------------------
  AREA LOW_EL_32_S, CODE, READONLY, ALIGN=7
  PushTwo x0, x30
  MRS x0, ESR_EL2
  LSR x0, #26
  CMP x0, #0x22
  PopTwo x30, x0
  BEQ HypForwardAArch64MisalignedPCException
  PushTwo x15, x16
  MRS x15, ESR_EL2
  LSR x15, x15, #26
  CMP x15, #0x12
  BEQ hvc32call
  CMP x15, #0x16
  BEQ hvc32call
  B low_el_sync
  AREA LOW_EL_32_I, CODE, READONLY, ALIGN=7
  PushTwo x0, x30
  MOV x0, #21
  BL hyp_log_exception
  PopTwo x30, x0
  B low_el_32_irq
  AREA LOW_EL_32_F, CODE, READONLY, ALIGN=7
  PushTwo x0, x30
  MOV x0, #22
  BL hyp_log_exception
  PopTwo x30, x0
  B low_el_32_fiq
  AREA LOW_EL_32_E, CODE, READONLY, ALIGN=7
  PushTwo x0, x30
  MOV x0, #23
  BL hyp_log_exception
  PopTwo x30, x0
  B low_el_32_error
  AREA EXCEPTION_IMPLEMENTATION, CODE, READONLY, ALIGN=7
; ------------------------------------------------------------
; Exception happened in AArch32 mis routed
; Implementation for ARM Cortex-A53 erratum 814270 -- begin
; ------------------------------------------------------------
HypForwardAArch32MisalignedPCException
  PushCorruptibleRegs
  MRS x0, SPSR_EL2
  MRS x1, SPSR_EL1
  MRS x2, ELR_EL2
  BL hyp_get_forward_spsr
  MRS x1, FAR_EL2
  MSR FAR_EL1, x1
  MRS x1, ESR_EL2
  MSR ESR_EL1, x1
  PopCorruptibleRegs
  ERET
; ------------------------------------------------------------
; Exception happened in AArch64 mis routed
; Implementation for ARM Cortex-A53 erratum 814270 -- end
; ------------------------------------------------------------
; ------------------------------------------------------------
; Exception happened in AArch64 mis routed
; Implementation for ARM Cortex-A53 erratum 814270 -- begin
; ------------------------------------------------------------
HypForwardAArch64MisalignedPCException
  PushCorruptibleRegs
  MRS x0, SPSR_EL2
  MRS x1, SPSR_EL1
  MRS x2, ELR_EL2
  BL hyp_get_forward_spsr
  MRS x1, FAR_EL2
  MSR FAR_EL1, x1
  MRS x1, ESR_EL2
  MSR ESR_EL1, x1
  PopCorruptibleRegs
  ERET
; ------------------------------------------------------------
; Exception happened in AArch64 mis routed
; Implementation for ARM Cortex-A53 erratum 814270 -- end
; ------------------------------------------------------------
hvc32call
  PopTwo x16, x15
  BackUpRegsForHVC
  MOV x0, SP
  MOV x1, #1
  BL hyp_hvc_handler
  RestoreRegsForHVC
  ERET
hvc64call
  PopTwo x16, x15
  BackUpRegsForHVC
  MOV x0, SP
  MOV x1, #2
  BL hyp_hvc_handler
  RestoreRegsForHVC
  ERET
; ------------------------------------------------------------
; Exception Handlers
; ------------------------------------------------------------
el2_sp_el0_synch
  PushCorruptibleRegs
  MOV x0, #8
  MRS x1, ELR_EL2
  BL hyp_log_exception
  PopCorruptibleRegs
el2_sp_el0_synch_loop
  B el2_sp_el0_synch_loop
el2_sp_el0_irq
el2_sp_el0_fiq
el2_sp_el0_error
el2_sp_el2_sync
el2_sp_el2_irq
el2_sp_el2_fiq
el2_sp_el2_error
  B el2_sp_el2_error
low_el_64_irq
low_el_64_fiq
low_el_64_error
low_el_64_loop
  B low_el_64_loop
  ; x15 carries EC of ESR_EL2
low_el_sync
  CMP x15, #0x20
  BEQ %f1
  CMP x15, #0x24
  BEQ %f1
  B hyp_backup_regs_before_loop
; Check S2 Fatal flag
1 LDR x15, =hyp_s2_fatal_flag
  LDRB w16, [x15]
  CBNZ w16, hyp_treat_s2_fault_as_nop
hyp_backup_regs_before_loop
  GetCPUUniqueID x15, x16
  MOV x16, #376
  MUL x15, x15, x16 ; 376 = sizeof(sdi_cpu64_ctxt_regs_type)
  LDR x16, =hyp_reg_dump
  ADD x16, x15, x16
  BackUp4 x16, x0, x1, x2, x3
  BackUp4 x16, x4, x5, x6, x7
  BackUp4 x16, x8, x9, x10, x11
  BackUp4 x16, x12, x13, x14, x15
  MOV x8, x16
  PopTwo x16, x15
  BackUp4 x8, x16, x17, x18, x19
  BackUp4 x8, x20, x21, x22, x23
  BackUp4 x8, x24, x25, x26, x27
  MOV x4, XZR
  BackUp4 x8, x28, x29, x30, x4 ; PC is not recordable
  MOV x0, #2 ; EL2
  ; SP_EL3, ELR_EL3, SPSR_EL3, SP_EL2 are not readable at EL2
  ; Cur EL, SP_EL3, ELR_EL3, SPSR_EL3
  BackUp4 x8, x0, x4, x4, x4
  ; SP_EL2, ELR_EL2, SPSR_EL2, SP_EL1
  MRS x0, ELR_EL2
  MRS x1, SPSR_EL2
  MRS x2, SP_EL1
  BackUp4 x8, x4, x0, x1, x2
  ; ELR_EL1, SPSR_EL1, SP_EL0, reserved
  MRS x0, ELR_EL1
  MRS x1, SPSR_EL1
  MRS x2, SP_EL0
  BackUp4 x8, x0, x1, x2, x4 ; ELR_EL1, SPSR_EL1, SP_EL0, rsv
  GetCPUUniqueID x0, x1
  MRS x1, ESR_EL2
  MRS x2, FAR_EL2
  MRS x3, HPFAR_EL2
  MRS x4, IFSR32_EL2
  MRS x5, MAIR_EL2
  MRS x6, VTTBR_EL2
  MRS x7, HCR_EL2
  BL hyp_log_lel_s
low_el_s_loop
  B low_el_s_loop
low_el_32_irq
low_el_32_fiq
low_el_32_error
exception_catch_all
  B exception_catch_all
;Unreachable loop
Unreachable
  B Unreachable
hyp_treat_s2_fault_as_nop
  PopTwo x16, x15
  PushCorruptibleRegs
  GetCPUUniqueID x0, x1
  MRS x1, ESR_EL2
  MRS x2, FAR_EL2
  MRS x3, HPFAR_EL2
  MRS x4, IFSR32_EL2
  MRS x5, MAIR_EL2
  MRS x6, VTTBR_EL2
  MRS x7, HCR_EL2
  BL hyp_log_lel_s
  MRS x0, ELR_EL2
  MRS x1, HCR_EL2
  TST x1, #0x80000000
  BEQ low_el32
  MOV x2, #4
  B %f2
low_el32
  MRS x1, SPSR_EL2
  TST x1, #0x00000020
  BNE low_el32_thumb
  MOV x2, #4
  B %f2
low_el32_thumb
  MOV x2, #2
2 ADD x0, x0, x2
  MSR ELR_EL2, x0
  PopCorruptibleRegs
  ERET
hyp_setS2fatalflag
  LDR x1, =hyp_s2_fatal_flag
  STRB w0, [x1]
  RET
hyp_getCPUId
  GetCPUUniqueID x0, x1
  RET
; ---------------------------------------------------------------
; 1 Export Function
; ---------------------------------------------------------------
HYP_SetupMMU FUNCTION
  ;
  ; Set up page table pointer (64-bit register)
  MSR TTBR0_EL2, x0 ; Write TTBR0_EL2
  ;
  ; Set up MAIR_EL2
  ; ---------------------------------------------------------------
  ; 63:56 - Attr7 - 0x00 - Strongly Ordered
  ; 55:48 - Attr6 - 0x00 - Strongly Ordered
  ; 47:40 - Attr5 - 0x00 - Strongly Ordered
  ; 39:32 - Attr4 - 0x00 - Strongly Ordered
  ; 31:24 - Attr3 - 0xBB - Normal inner/outer WT/RA/WA
  ; 23:16 - Attr2 - 0x04 - Device
  ; 15:8 - Attr1 - 0xFF - Normal inner/outer WB/RA/WA
  ; 7:0 - Attr0 - 0x44 - Normal inner/outer non-cachable.
  LDR x0, =0x00000000BB04FF44
  MSR MAIR_EL2, x0
  ;
  ; Configure Hyp Translation Control Register (HTCR)
  ; ------------------------------------------------
  ; HTCR format:
  ; 31 - RES1
  ; 30:24 - RES0
  ; 23 - RES1
  ; 22:21 - RES0
  ; 20 - TBI - 0x0
  ; 19 - RES0
  ; 18:16 - PS - 0x0 - 4GB
  ; 15:14 - TG0 - 0x0 - 4KB
  ; 13:12 - SH0 - 0x3 - inner shareable
  ; 11:10 - ORGN0 - 0x0 (NC) 0x1 (WB/WA) 0x02 (WT)
  ; 9:8 - IRGN0 - 0x0 (NC) 0x1 (WB/WA) 0x02 (WT)
  ; 7:6 - RES0
  ; 5:0 - T0SZ - 0x20 - 32-bit space
  ldr x0, =0x80803A20
  MSR TCR_EL2, x0
  RET
  ENDFUNC
    ;8-bytes aligned
    AREA HYPENTRYDATA, DATA, READWRITE, ALIGN=3
hyp_config_bmsk SPACE 8
hyp_cpu_boot_state SPACE (2 * 4)
hyp_is_init SPACE 1
hyp_s2_fatal_flag SPACE 1
    AREA |HYP_STACK|, DATA, READWRITE, ALIGN=12
hyp_stack_top SPACE (0x1000*(2 * 4))
hyp_stack_bottom
    AREA |HYP_DIAG|, DATA, READWRITE, ALIGN=12
hyp_log_area SPACE (0x2000)
    END
