;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;
; T R U S T Z O N E N O N S E C U R E F I Q D E B U G
;
;
; GENERAL DESCRIPTION
; This file contains the Hypervisor Mutex Assembly codes
;
; INITIALIZATION AND SEQUENCING REQUIREMENTS
;
; Copyright (c) 2013-2014 by Qualcomm Technologies, Incorporated.All Rights Reserved.
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;
; EDIT HISTORY FOR FILE
;
; This section contains comments describing changes made to the module.
; Notice that changes are listed in reverse chronological order.
;
;
; $Header:
; when who what, where, why
; -------- --- ---------------------------------------------------
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;============================================================================
;
; MODULE IMPORTS
;
;=======================================================================
    EXPORT hyp_mutex_init
    EXPORT hyp_mutex_lock
    EXPORT hyp_mutex_unlock
    EXPORT hyp_int_disable_all
    EXPORT hyp_int_restore
;=======================================================================
; MACRO mdmb
;
; ARGS
; NONE
;
; DESCRIPTION
; Performs a data memory barrier, either using the ARMv7 instruction o
; legacy coprocessor instruction.
;
; NOTES
; For reference see ARM DDI 0406A-03 section A3.8.3.
;=======================================================================
   MACRO
   MDMB
      DMB SY ; RVDS >= 3.0 supports ARMv7 instructio
   MEND
    AREA HYPUTILCODE, CODE, READONLY
; int hyp_mutex_initialize(mutex *m)
; uint64 hyp_mutex_init(hyp_mutex_t* mutex)
;
; r0 - Address of the mutex structure
hyp_mutex_initialize ; Alias for ARM C library
hyp_mutex_init FUNCTION
    MDMB
    MOV w1, #0
    STR w1, [x0]
    ; Return non-zero to tell the ARM C libraries that we are running
    ; multithreaded code.
    MOV x0, #1
    MDMB
    RET
    ENDFUNC
; void hyp_mutex_acquire(mutex *m)
; void hyp_mutex_lock(hyp_mutex_t* mutex)
;
; Waits until the provided mutex lock has been reclaimed.
;
; x0 - Address of the mutex structure
hyp_mutex_acquire ; Alias for ARM C library
hyp_mutex_lock FUNCTION
    MOV w1, #1
    PRFM PSTL1KEEP, [x0]
Lock
    LDAXR w2, [x0] ; read lock with acquire
    CBNZ w2, Lock ; check if 0
    STXR w2, w1, [x0] ; attemp to store new value
    CBNZ w2, Lock ; test if store succeeded, if fail,
                                ; retry until it scuceed
    MDMB ; Ensure other observers see lock claim.
    RET
    ENDFUNC
; void hyp_mutex_release(mutex *m)
; void hyp_mutex_unlock(hyp_mutex_t* mutex)
;
; x0 - Address of the mutex structure
hyp_mutex_release ; Alias for ARM C library
hyp_mutex_unlock FUNCTION
    MDMB ; Ensure other observers see lock claim.
    STLR WZR, [x0] ; clear the lock with release semantics
    MDMB ; Ensure other observers see lock claim.
    RET
    ENDFUNC
; uint64 hyp_int_disable_all(void)
;
; x0 - return the A/I/F of SPSR before new int mask
hyp_int_disable_all FUNCTION
    MRS x1, SPSR_EL2 ; Read the status registers
    AND x0, x1, #0x00000040:OR:0x00000080:OR:0x00000100 ; Record A/I/F bits only
    ORR x1, x1, #0x00000040:OR:0x00000080:OR:0x00000100 ; Set A/I/F bits?
    MSR SPSR_EL2, x1 ; Apply the new int mask
    ; x0 contains the A/I/F bits before masking
    RET
    ENDFUNC
; void hyp_int_restore(uint64 flags)
hyp_int_restore FUNCTION
    AND x0, x0, #0x00000040:OR:0x00000080:OR:0x00000100 ; Get A/I/F bits only
    MRS x1, SPSR_EL2 ; Read the status register
    MOV x2, #0x00000040:OR:0x00000080:OR:0x00000100
    BIC x1, x1, x2 ; Clear A/I/F bits
    ORR x1, x1, x0
    MSR SPSR_EL2, x1
    RET
    ENDFUNC
    END
