#ifndef HYP_LOG_H
#define HYP_LOG_H

/**
@file hyp_log.h
@brief Hypervisor Logging

Contains Macros for logging HYP debug messages. By default all 
logs go to the internal ring buffer.

*/
/*===========================================================================
   Copyright (c) 2013 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/kernel/hypervisor/inc/hyp_log.h#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $


when       who      what, where, why
--------   ---      ------------------------------------
12/16/12   mg       Support new log buffer management
04/29/10   ssm      Created

===========================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/
#include <comdef.h>
#include <stdarg.h> /* for va_list */

/*=========================================================================== 
    Preprocessor Definitions and Constants
============================================================================*/
/* HYP Diag Magic number */
#define HYP_DIAG_MAGIC_NUM   0x6879706D /* 'h', 'y', 'p', 'm' */

#define HYP_MSG_LOW      (0)
#define HYP_MSG_MED      (1)
#define HYP_MSG_HIGH     (2)
#define HYP_MSG_ERROR    (3)
#define HYP_MSG_FATAL    (4)
#define HYP_MSG_DEBUG    (5)

/* Error codes for logging only. */
#define HYP_LOG_CORE0_BRINGUP                 0
#define HYP_LOG_CORE1_BRINGUP                 1
#define HYP_LOG_CORE2_BRINGUP                 2
#define HYP_LOG_CORE3_BRINGUP                 3
#define HYP_LOG_CORE4_BRINGUP                 4
#define HYP_LOG_CORE5_BRINGUP                 5
#define HYP_LOG_CORE6_BRINGUP                 6
#define HYP_LOG_CORE7_BRINGUP                 7
#define HYP_LOG_EXCEPTION_CEL_SP_EL0_SYNC     8
#define HYP_LOG_EXCEPTION_CEL_SP_EL0_IRQ      9
#define HYP_LOG_EXCEPTION_CEL_SP_EL0_FIQ     10
#define HYP_LOG_EXCEPTION_CEL_SP_EL0_ERR     11
#define HYP_LOG_EXCEPTION_CEL_SP_EL2_SYNC    12
#define HYP_LOG_EXCEPTION_CEL_SP_EL2_IRQ     13
#define HYP_LOG_EXCEPTION_CEL_SP_EL2_FIQ     14
#define HYP_LOG_EXCEPTION_CEL_SP_EL2_ERR     15
#define HYP_LOG_EXCEPTION_LEL_AARCH64_SYNC   16
#define HYP_LOG_EXCEPTION_LEL_AARCH64_IRQ    17
#define HYP_LOG_EXCEPTION_LEL_AARCH64_FIQ    18
#define HYP_LOG_EXCEPTION_LEL_AARCH64_ERR    19
#define HYP_LOG_EXCEPTION_LEL_AARCH32_SYNC   20
#define HYP_LOG_EXCEPTION_LEL_AARCH32_IRQ    21
#define HYP_LOG_EXCEPTION_LEL_AARCH32_FIQ    22
#define HYP_LOG_EXCEPTION_LEL_AARCH32_ERR    23

/**
 * Hypervisor log macro to record debug messages to a ring buffer.
 *
 * @param [in] xx_prio - Priority of the message to be logge, one of the
 * @param [in] xx_fmt  - Format string for output message.
 * @param [in] ...     - Arguments to format string.
 */
#define HYP_LOG(xx_prio, xx_fmt, ...)                                   \
  do {                                                                  \
    hyp_log(xx_prio, xx_fmt, ##__VA_ARGS__);                            \
  } while (0)

#define HYP_LOG_ERR(xx_fmt, ...) \
  HYP_LOG(HYP_MSG_ERROR, xx_fmt, ##__VA_ARGS__)

#define HYP_LOG_DBG(xx_fmt, ...) \
  HYP_LOG(HYP_MSG_DEBUG, xx_fmt, ##__VA_ARGS__)

/* Logs a code. */
#define HYP_LOG_CODE(xx_prio, xx) \
  HYP_LOG(xx_prio, "(%u)", xx)

/**
 * Shortcut macro for conditinally logging an error, log is only printed if
 * error condition is present in \c pred.
 */
#define HYP_LOG_ERR_E(pred, xx_fmt, ...)                                 \
  if(E_SUCCESS != (pred))                                                \
  {                                                                      \
    HYP_LOG(HYP_MSG_ERROR, xx_fmt, ##__VA_ARGS__);                       \
  }

/*===========================================================================

          TYPES

============================================================================*/
/* External CPU Dump Structure - 64 bit EL */
typedef struct
{
  uint64 x0;
  uint64 x1;
  uint64 x2;
  uint64 x3;
  uint64 x4;
  uint64 x5;
  uint64 x6;
  uint64 x7;
  uint64 x8;
  uint64 x9;
  uint64 x10;
  uint64 x11;
  uint64 x12;
  uint64 x13;
  uint64 x14;
  uint64 x15;
  uint64 x16;
  uint64 x17;
  uint64 x18;
  uint64 x19;
  uint64 x20;
  uint64 x21;
  uint64 x22;
  uint64 x23;
  uint64 x24;
  uint64 x25;
  uint64 x26;
  uint64 x27;
  uint64 x28;
  uint64 x29;
  uint64 x30;
  uint64 pc;
  uint64 currentEL;
  uint64 sp_el3;
  uint64 elr_el3;
  uint64 spsr_el3;
  uint64 sp_el2;
  uint64 elr_el2;
  uint64 spsr_el2;
  uint64 sp_el1;
  uint64 elr_el1;
  uint64 spsr_el1;
  uint64 sp_el0;
  uint64 __reserved1;
  uint64 __reserved2;
  uint64 __reserved3;
  uint64 __reserved4;
} sdi_cpu64_ctxt_regs_type;

/*===========================================================================

          FUNCTIONS

============================================================================*/
void hyp_log_init(void);

void hyp_milestone(uint64 code);

void hyp_log(uint32 pri, const char* fmt, ...);

void hyp_log_warmboot_entry(uint32 cpu);
void hyp_log_warmboot_exit(uint32 cpu);
void hyp_log_exception(uint64 ex, uint64 code);
uint32 hyp_log_get_s2_fault_counter(uint64 cpu);

// log the start and end of a SMC or HVC call
void hyp_log_call_start(uint32 call_id);
void hyp_log_call_end(void);

#endif /* HYP_LOG_H */
