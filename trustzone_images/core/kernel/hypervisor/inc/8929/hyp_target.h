/**
@file hyp_target.h
@brief Hypervisor Target specific definitions

This file contains the definition of hypervisor target specific constants

*/
/*===========================================================================
   Copyright (c) 2013-2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/kernel/hypervisor/inc/8929/hyp_target.h#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $


when       who      what, where, why
--------   ---     ------------------------------------------------------------
=============================================================================*/
#ifndef HYP_TARGET_H
#define HYP_TARGET_H

#define NUM_CLUSTERS           2
#define NUM_CPUS_PER_CLUSTER   4
#define CLUSTER_RSHIFT         6 // equals to (8 - log(NUM_CPUS_PER_CLUSTER)
#define NUM_CPUS               (NUM_CLUSTERS * NUM_CPUS_PER_CLUSTER)

#define HYP_STACK_SIZE          0x1000

#define HYP_MEMORY_ADDR         0x86400000
#define HYP_MEMORY_SIZE         0x00100000
#define HYP_DIAG_SIZE           0x2000
#define HYP_IMAGE_SIZE          0x00016000

#define TZBSP_EBI1_TZ_BASE 0x86500000
#define TZBSP_EBI1_TZ_END  0x86680000

#define HYP_COPY_ADDR           (HYP_MEMORY_ADDR + HYP_IMAGE_SIZE)
#define PAGETABLE_MEMORY_ADDR   (HYP_COPY_ADDR + HYP_IMAGE_SIZE)
#define PAGETABLE_MEMORY_SIZE   (HYP_MEMORY_SIZE - (HYP_IMAGE_SIZE*2))

// L1PT addresses must be aligned to 32B
#define VIRT_L1PT_MEMORY_ADDR       PAGETABLE_MEMORY_ADDR
#define VIRT_L1PT_MEMORY_SIZE       0x00000200
#define SMMU_L1PT_MEMORY_ADDR       (VIRT_L1PT_MEMORY_ADDR + VIRT_L1PT_MEMORY_SIZE)
#define SMMU_L1PT_MEMORY_SIZE       0x00000E00

// pool memory must be 4KB-aligned
#define VIRT_PT_POOL_MEMORY_ADDR    (SMMU_L1PT_MEMORY_ADDR + SMMU_L1PT_MEMORY_SIZE)
#define VIRT_PT_POOL_MEMORY_SIZE    0x0007a000
#define SMMU_PT_POOL_MEMORY_ADDR    (VIRT_PT_POOL_MEMORY_ADDR + VIRT_PT_POOL_MEMORY_SIZE)
#define SMMU_PT_POOL_MEMORY_SIZE    (PAGETABLE_MEMORY_SIZE - VIRT_L1PT_MEMORY_SIZE - SMMU_L1PT_MEMORY_SIZE - VIRT_PT_POOL_MEMORY_SIZE) 

#define IMEM_BASE                   0x08600000
#define IMEM_MEM_DUMP               (IMEM_BASE + 0x10)
#define IMEM_HYP_OFFSET             0x00000B20
#define IMEM_BACKUP_ADDR            (IMEM_BASE + IMEM_HYP_OFFSET)
#define IMEM_HYP_LOG_ADDR           (IMEM_BASE + IMEM_HYP_OFFSET + 0x10)
#define IMEM_HYP_LOG_SIZE           (IMEM_BASE + IMEM_HYP_OFFSET + 0x14)
#define IMEM_BACKUP_MAGIC           0xC1F8DB42

#define HYP_BRING_RPM_OUT_OF_RESET  1
#define GCC_APSS_MISC_ADDR          0x1860000 
#define RPM_RESET_REMOVAL_BMSK      0x6

#endif
