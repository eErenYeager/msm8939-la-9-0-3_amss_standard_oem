/**
@file hyp_asm.h
@brief Hypervisor definitions

This file contains the definition of hypervisor register info 

*/
/*===========================================================================
   Copyright (c) 2013-2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/kernel/hypervisor/inc/hyp_asm.h#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $


when       who      what, where, why
--------   ---     ------------------------------------------------------------
=============================================================================*/

#ifndef HYP_ASM_H
#define HYP_ASM_H

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/


/*===========================================================================

                           DEFINITION OF EL1 Control registers

===========================================================================*/
#define SCTLR_EL1_CP15BEN_BIT    0x20


/*===========================================================================

                           DEFINITION OF EL2 Control registers AArch64

===========================================================================*/

/* PSR bits */
#define PSR_MODE_EL0t  0x00000000
#define PSR_MODE_EL1t  0x00000004
#define PSR_MODE_EL1h  0x00000005
#define PSR_MODE_EL2t  0x00000008
#define PSR_MODE_EL2h  0x00000009
#define PSR_MODE_EL3t  0x0000000C
#define PSR_MODE_EL3h  0x0000000D
#define PSR_MODE_E_32  0x00000010 /* if this bit is 0, the exception it taken from AArch64 */
#define PSR_MODE_MASK  0x0000000f

/* AArch64 SPSR bits */
#define PSR_F_BIT      0x00000040
#define PSR_I_BIT      0x00000080
#define PSR_A_BIT      0x00000100
#define PSR_D_BIT      0x00000200
#define PSR_Q_BIT      0x08000000
#define PSR_V_BIT      0x10000000
#define PSR_C_BIT      0x20000000
#define PSR_Z_BIT      0x40000000
#define PSR_N_BIT      0x80000000
#define CPSR_MODE_MASK 0x1F

#define HCR_VM_BIT                   0x00000001      /* VM bit in HCR        */
#define HCR_SWIO_BIT                 0x00000002      /* SWIO bit in HCR      */
#define HCR_PTW_BIT                  0x00000004      /* PTW bit in HCR   */
#define HCR_RW_BIT                   0x80000000      /* RW bit, 0 means AArch32, 1 means EL1 is AArch64 */
#define HCR_RW_BOOT_EL1_BIT          0x80000000      /* RW bit, 0 means AArch32, 1 means EL1 is AArch64 */

/* 
 * Defines for CNTHCTL_EL2
 */
#define CNTHCTL_COUNTER_ENABLE_1     0x02
#define CNTHCTL_COUNTER_ENABLE_0     0x01

/* 
 * Defines for VMPIDR_EL2
 */
#define VMPIDR_EL2_AFF0              0xFF

/* ESR_EL2 bits */
#define ESR_EL2_EC_UNKNOWN           0x00
#define ESR_EL2_EC_WFE_WFI           0x01
#define ESR_EL2_EC_CP15_ACCESS       0x03
#define ESR_EL2_EC_CP14_ACCESS       0x05
#define ESR_EL2_EC_CP14_ACCESS_2     0x06
#define ESR_EL2_EC_SIMD_FP           0x07
#define ESR_EL2_EC_CP14_ACCESS_3     0x0C
#define ESR_EL2_EC_IL                0x0E
#define ESR_EL2_EC_HVC_32            0x12
#define ESR_EL2_EC_SMC_32            0x13
#define ESR_EL2_EC_SVC_64            0x15
#define ESR_EL2_EC_HVC_64            0x16
#define ESR_EL2_EC_SMC_64            0x17
#define ESR_EL2_EC_SYSREG_64         0x18
#define ESR_EL2_EC_I_ABORT_LOWER_EL  0x20
#define ESR_EL2_EC_I_ABORT_THIS_EL   0x21
#define ESR_EL2_EC_PC_ALIGNMENT      0x22
#define ESR_EL2_EC_D_ABORT_LOWER_EL  0x24
#define ESR_EL2_EC_D_ABORT_THIS_EL   0x25
#define ESR_EL2_EC_SP_ALIGNMENT      0x26
#define ESR_EL2_EC_FP_EXCEPTION      0x2C
#define ESR_EL2_EC_SERROR_INT        0x2F
#define ESR_EL2_EC_BRK_64            0x3C
#define ESR_EL2_EC_SHFT              26
#define ESR_EL2_EC_BMSK              0x3F

/*===========================================================================

                           DEFINITION OF EL2 Control registers AArch32

===========================================================================*/
/* PSR_32 bits */
#define PSR_32_MODE_USR 0x00000000
#define PSR_32_MODE_FIQ 0x00000001
#define PSR_32_MODE_IRQ 0x00000002
#define PSR_32_MODE_SVC 0x00000003
#define PSR_32_MODE_MON 0x00000006
#define PSR_32_MODE_ABT 0x00000007
#define PSR_32_MODE_HYP 0x0000000A
#define PSR_32_MODE_UND 0x0000000B
#define PSR_32_MODE_SYS 0x0000000F
#define PSR_32_M_BIT 0x00000010   /* RW of exception */
#define PSR_32_T_BIT 0x00000020   /* Thumb execution state bit */
#define PSR_32_F_BIT 0x00000040   /* FIQ mask bit */
#define PSR_32_I_BIT 0x00000080   /* IRQ mask bit */
#define PSR_32_A_BIT 0x00000100   /* SError mask bit */
#define PSR_32_D_BIT 0x00000200   /* Debug exception mask bit */

#define PSR_f          0xff000000      /* Flags                */
#define PSR_s          0x00ff0000      /* Status               */
#define PSR_x          0x0000ff00      /* Extension            */
#define PSR_c          0x000000ff      /* Control              */

/*===========================================================================

                           DEFINITION OTHERS

===========================================================================*/

/*Defines from VM*/
/*
#define PC_OFFSET     0x48
#define USR_LR_OFFSET 0x58

#define SVC_SP_OFFSET 0x60

#define VBAR_OFFSET    0x0
#define SCTLR_OFFSET   0x4
#define TTBCR_OFFSET   0xC
#define TTBR0_0_OFFSET   0x10
#define TTBR0_1_OFFSET   0x14
#define TTBR1_0_OFFSET   0x18
#define TTBR1_1_OFFSET   0x1C
#define MAIR0_OFFSET   0x20
#define MAIR1_OFFSET   0x24
*/
#define LPAE_ENABLE_MASK 0x80000000

#define ELF_EI_CLASS_32   1
#define ELF_EI_CLASS_64   2


/*===========================================================================

                      HVC CALL

===========================================================================*/
#define HVC_VERSION_INFO          0x00000102 // 0x0100 : AArch64
                                             // 0x0000 : AArch32

#define HVC_REGISTER_WIDTH        8
#define HVC_RESULT_REGISTER_NUM   4          // x0-x3
#define HVC_RESULT_SIZE           (HVC_REGISTER_WIDTH*HVC_RESULT_REGISTER_NUM)


/*===========================================================================
                      TYPE
===========================================================================*/
typedef uint64 el2_reg_t;

/*===========================================================================

                      PUBLIC FUNCTION DECLARATIONS

===========================================================================*/
/**
 * The mutex structure used to provide simultaneous access to shared resources
 */
typedef struct
{
  volatile uint32 lock;
} hyp_mutex_t;


/**
 * Initialize the mutex structure. The mutex functions are aliased so that the
 * standard C libary will use the below implementation.
 *
 * @returns non-zero to tell the ARM C libraries that we are running
 *          multithreaded code.
 */
uint64 hyp_mutex_init(hyp_mutex_t *mutex);
/**
 * Perform a spinlock on the mutex
 */
void hyp_mutex_lock(hyp_mutex_t *mutex);
/**
 * Unlock the mutex
 */
void hyp_mutex_unlock(hyp_mutex_t *mutex);


/**
 * Disables all interrupts.
 *
 * @return The original interrupt bits from SPSR_EL2
 *
 * @see hyp_int_restore
 */
uint64 hyp_int_disable_all(void);

/**
 * Restores previously disabled interrupts.
 *
 * @param [in] flags The interrupt bits to restore to CPSR.
 *
 * @see hyp_int_disable_all
 */
void hyp_int_restore(uint64 flags);

void hyp_setS2fatalflag(uint8 flag);

/**
 * Get unique CPU Id
 *
 * @return  CPU ID
 *
 * @see hyp_int_disable_all
 */
uint64 hyp_getCPUId(void);

#endif 
