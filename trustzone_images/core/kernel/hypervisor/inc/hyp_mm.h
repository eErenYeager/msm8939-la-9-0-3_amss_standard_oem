#ifndef HYP_MM_H
#define HYP_MM_H

/**
@file hyp_mm.h
@brief Hypervisor Memory Management

Contains Macros for logging HYP debug messages. By default all 
logs go to the internal ring buffer.

*/
/*===========================================================================
   Copyright (c) 2013 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/kernel/hypervisor/inc/hyp_mm.h#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $


when       who      what, where, why
--------   ---      ------------------------------------
05/21/14   dc       Created

===========================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

/*=========================================================================== 
    Preprocessor Definitions and Constants
============================================================================*/


/*===========================================================================

          FUNCTIONS

============================================================================*/
int64 hyp_hvc64_grant_access(uint64_t ipa, uint64_t size, uint64_t perm, uint64_t cache, uint64_t share);
int64 hyp_hvc64_remove_access(uint64_t ipa, uint64_t size);

int32 hyp_hvc32_grant_access(uint32_t ipa_highword, uint32_t ipa_lowword, 
                             uint32_t size_highword, uint32_t size_lowword,
                             uint32_t x5);
int32 hyp_hvc32_remove_access(uint32_t ipa_highword, uint32_t ipa_lowword, 
										uint32_t size_highword, uint32_t size_lowword);

#endif /* HYP_LOG_H */
