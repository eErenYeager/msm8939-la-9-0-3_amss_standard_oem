/**
@file hyp_hvc.h
@brief Hypervisor definitions

This file contains the definition of hypervisor HVC call info

*/
/*===========================================================================
   Copyright (c) 2013-2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/kernel/hypervisor/inc/hyp_hvc.h#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $


when       who      what, where, why
--------   ---     ------------------------------------------------------------ 
06.20.14   dc       Created 
=============================================================================*/

#ifndef HYP_HVC_H
#define HYP_HVC_H

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/


/*===========================================================================
                      HVC CALL
===========================================================================*/
#define HVC64                     (1 << 30)
#define HVC_FUNCTION_ID_BASE      0x82000000

#define HVC_GET_VERSION           0x00000000
#define HVC_GRANT_ACCESS          0x00000001
#define HVC_REMOVE_ACCESS         0x00000002
#define HVC_SET_S2_S2FAULT_NOOP   0x00000003
#define HVC_GET_S2_FAULT_NUM      0x00000004

/*===========================================================================
                      HVC FUNCTION ID
===========================================================================*/
#define HVC64_GET_VERSION         (HVC_FUNCTION_ID_BASE | HVC64 | HVC_GET_VERSION) 
#define HVC64_GRANT_ACCESS        (HVC_FUNCTION_ID_BASE | HVC64 | HVC_GRANT_ACCESS) 
#define HVC64_REMOVE_ACCESS       (HVC_FUNCTION_ID_BASE | HVC64 | HVC_REMOVE_ACCESS) 
#define HVC64_SET_S2_S2FAULT_NOOP (HVC_FUNCTION_ID_BASE | HVC64 | HVC_SET_S2_S2FAULT_NOOP) 
#define HVC64_GET_S2_FAULT_NUM    (HVC_FUNCTION_ID_BASE | HVC64 | HVC_GET_S2_FAULT_NUM) 

#define HVC32_GET_VERSION         (HVC_FUNCTION_ID_BASE | HVC_GET_VERSION) 
#define HVC32_GRANT_ACCESS        (HVC_FUNCTION_ID_BASE | HVC_GRANT_ACCESS)
#define HVC32_REMOVE_ACCESS       (HVC_FUNCTION_ID_BASE | HVC_REMOVE_ACCESS)
#define HVC32_SET_S2_S2FAULT_NOOP (HVC_FUNCTION_ID_BASE | HVC_SET_S2_S2FAULT_NOOP) 
#define HVC32_GET_S2_FAULT_NUM    (HVC_FUNCTION_ID_BASE | HVC_GET_S2_FAULT_NUM) 

/*===========================================================================
                      HVC RESULTS
===========================================================================*/
#define HVC_CALL_SUCCESS                   0
#define HVC_CALL_GENERIC_ERROR            -1
#define HVC_CALL_NON_SUPPORT_ID           -2
#define HVC_CALL_HVC64_MADE_IN_AARCH32    -3
#define HVC_CALL_GRANT_FAIL               -4
#define HVC_CALL_REMOVE_FAIL              -5
#define HVC_CALL_NOT_ACCESSIBLE           -6


#endif 
