;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;
; GENERAL DESCRIPTION
; This file contains the Hypervisor MMU Set Up and TLB Invalid code
;
; INITIALIZATION AND SEQUENCING REQUIREMENTS
;
; Copyright (c) 2013-2014 by Qualcomm Technologies, Incorporated.All Rights Reserved.
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;
; EDIT HISTORY FOR FILE
;
; This section contains comments describing changes made to the module.
; Notice that changes are listed in reverse chronological order.
;
;
; $Header:
; when who what, where, why
; -------- --- ---------------------------------------------------
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
    EXPORT VMMM_SetupMMU
    EXPORT VMMM_Invalidate_TLB_BY_IPA
    AREA vm_mm_asm, CODE, READONLY
VMMM_SetupMMU FUNCTION
    ; setup the stage 2 translation registers
    MSR VTTBR_EL2, x0
    MSR VTCR_EL2, x1
    RET
    ENDFUNC
; X0: 64-bit IPA to be invalidated
VMMM_Invalidate_TLB_BY_IPA FUNCTION
   LSR x0, x0, #12
   ; TLBIIPAS2IS operation
   TLBI IPAS2E1IS, x0
   DMB SY
   TLBI VMALLE1IS
   DMB SY
   ISB
   RET
   ENDFUNC
   END
