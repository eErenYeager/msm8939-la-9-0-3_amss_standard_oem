/*
 * Copyright (c) 2014 TRUSTONIC LIMITED
 * All rights reserved
 *
 * The present software is the confidential and proprietary information of
 * TRUSTONIC LIMITED. You shall not disclose the present software and shall
 * use it only in accordance with the terms of the license agreement you
 * entered into with TRUSTONIC LIMITED. This software may be subject to
 * export or import laws in certain countries.
 */
/**
 * Architecture: ARM
 * Platform:     Qualcomm MSM8960
 */
#ifndef __MTK_API_TZBSP_API_WRAPPER_H__
#define __MTK_API_TZBSP_API_WRAPPER_H__

#include "MtkApiTzbsp_qc.h"

// TUI syscalls
#define PLATCTRLNO_TZBSP_TUI_STORE      1002      /* store some specific data in tbase */
#define TZBSP_TUI_STORE_SESSIONID       1         /* store the TUI session id */
#define TZBSP_TUI_STORE_BUFFER          2         /* store info about the TUI frame buffers */

/*
 * A TUI Secure Display (SD) session has a unique sessionID that should remain
 * unchanged during the session.
 * This function allows to send the sessionId to tbase so that it will be checked
 * every time we enter the SW from NW.
 *
 * @param sessionId Returned by tzbspApi_SdGetSession()
 *
 *@return DRAPI_OK if sessionId successfully stored
 *@return E_INVALID if sessionID is not the current id
 *
 */
static inline uint32_t tzbspPlat_storeSessionId(uint32_t sessionId){

	uint64_t ret;
	uint32_t retval;

	if (drApiPlatformControl(PLATCTRLNO_TZBSP_TUI_STORE, TZBSP_TUI_STORE_SESSIONID,
			sessionId, 0, 0, &ret) != DRAPI_OK)
	{
		// error while executing the sys call
		return E_INVALID;
	}

	retval = (uint32_t)ret;
    return retval;
}

/*
 * Before manipulating frame buffers, make sure they are secured.
 * This function allows to send physical address and size of frame buffers to t-base.
 * At every context switch between Nwd and Swd, buffers will be checked that are still secured.
 *
 *@param phys Physical address of frame buffer;
 *  	      it should be secure memory tagged with TZBSP_MEM_TAG_USECASE_SD.
 *@param size Size of frame buffer in bytes.
 *
 *@return E_OK if stored successfully.
 *@return E_INVALID if buffer to be stored is inncorect.
 *@return E_LIMIT if buffer not stored  - TODO this impl should maybe change to an unlimited number of buff?
 *
 */
static inline uint32_t  tzbspPlat_storeBuffer(uint64_t phys, uint32_t size){

	uint64_t ret;
	uint32_t retval;

	if ((retval = drApiPlatformControl(PLATCTRLNO_TZBSP_TUI_STORE, TZBSP_TUI_STORE_BUFFER,
			phys, size, 0, &ret)) != DRAPI_OK)
	   return retval;

    retval = (uint32_t)ret;
    return retval;
}


//------------------------------------------------------------------------------
uint32_t tzbspApi_core_isSwFuseBlown(
    tzbsp_sw_fuse_t fuse_num,
    bool_t*         is_blown,
    uint32_t        is_blown_sz);

bool_t tzbspApi_core_isSTagArea(
    uint32_t  tag,
    uint32_t  start,
    uint32_t  end);

int tzbspApi_hdmiStatusRead(
    uint32_t*  hdmiEnable,
    uint32_t*  hdmiSense,
    uint32_t*  hdcpAuth);


int tzbspApi_I2COpen(
    TZBSP_I2CPD_DEVICE_ET       i2cPdDeviceId);

int tzbspApi_I2CRead(
    TZBSP_I2CPD_DEVICE_ET       i2cPdDeviceId,
    const tzbspI2cConfig_t*     pConfig,
    tzbspI2cTransactionInfo_t*  pReadInfo);

int tzbspApi_I2CWrite(
    TZBSP_I2CPD_DEVICE_ET       i2cPdDeviceId,
    const tzbspI2cConfig_t*     pConfig,
    tzbspI2cTransactionInfo_t*  pWriteInfo);

int tzbspApi_I2CClose(
    TZBSP_I2CPD_DEVICE_ET       i2cPdDeviceId);

uint32_t tzbspApi_MemUsageCount(
    uint32_t usage);

uint32_t tzbspApi_SdGetSession(
    void);

uint32_t tzbspApi_TagMem(
    uint32_t  tag,
    uint32_t start,
    uint32_t end);


//------------------------------------------------------------------------------
uint32_t tzbspApi_crypto_hash(
    TZBSP_HASH_ALGO_ET  alg,
    const void         *msg,
    uint32_t            msgLen,
    void                *digest,
    uint32_t            digestLen);

uint32_t tzbspApi_crypto_hashInit(
    TZBSP_HASH_ALGO_ET  alg,
    tzbsp_hash_ctx      **hashCtx);

uint32_t tzbspApi_crypto_hashUpdate(
    const tzbsp_hash_ctx  *hashCtx,
    const void            *msg,
    uint32_t              msgLen);

uint32_t tzbspApi_crypto_hashFinal(
    const tzbsp_hash_ctx  *hashCtx,
    void                  *digest,
    uint32_t              digestLen);

uint32_t tzbspApi_crypto_hashFreeCtx(
    tzbsp_hash_ctx *hashCtx);

uint32_t tzbspApi_crypto_cipherInit(
    TZBSP_CIPHER_ALGO_ET  alg,
    tzbsp_cipher_ctx      **cipherCtx);

uint32_t tzbspApi_crypto_cipherFreeCtx(
    tzbsp_cipher_ctx *cipher_ctx);

uint32_t tzbspApi_crypto_cipherSetParam(
    tzbsp_cipher_ctx       *cipherCtx,
    TZBSP_CIPHER_PARAM_ET  paramId,
    const void             *param,
    uint32_t               paramLen);

uint32_t tzbspApi_crypto_cipherGetParam(
    const tzbsp_cipher_ctx  *cipherCtx,
    TZBSP_CIPHER_PARAM_ET   paramId,
    void                    *param,
    uint32_t                *paramLen);

uint32_t tzbspApi_crypto_cipherEncrypt(
    const tzbsp_cipher_ctx  *cipherCtx,
    const void              *pt,
    uint32_t                ptLen,
    void                    *ct,
    uint32_t                *ctLen);

uint32_t tzbspApi_crypto_cipherDecrypt(
    const tzbsp_cipher_ctx  *cipherCtx,
    const void              *ct,
    uint32_t                ctLen,
    void*                   pt,
    uint32_t                *ptLen);

uint32_t tzbspApi_crypto_hmac(
    TZBSP_HMAC_ALGO_ET  alg,
    const void          *msg,
    uint32_t            msgLen,
    const void          *key,
    uint32_t            keyLen,
    void                *msgDigest);

uint32_t tzbspApi_crypto_prng(
    const void  *out,
    uint32_t    outLen);

#endif // __MTK_API_TZBSP_API_WRAPPER_H__
