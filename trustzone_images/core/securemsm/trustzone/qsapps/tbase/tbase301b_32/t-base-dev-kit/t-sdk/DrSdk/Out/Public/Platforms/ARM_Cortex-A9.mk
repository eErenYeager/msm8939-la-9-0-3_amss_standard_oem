
#-------------------------------------------------------------------------
#
# ARM Cortex-A9
#
#-------------------------------------------------------------------------


#-------------------------------------------------------------------------
# add our supported targets 
ADD_PLATFORM_TARGETS := \
     ARM_VE_A9X4_STD \
     ARM_VE_A9X4_QEMU

#-------------------------------------------------------------------------
# specific platforms
VALID_PLATFORM = $(empty)

ifeq ($(PLATFORM),ARM_VE_A9X4_STD)
    VALID_PLATFORM = TRUE
    ARM_SHAPE := VE
    PLATFORM_CORE_COUNT := 4
    PLATFORM_CLUSTER_COUNT := 1
endif

ifeq ($(PLATFORM),ARM_VE_A9X4_QEMU)
    VALID_PLATFORM = TRUE
    ARM_SHAPE := VE
    ARMCC_COMPILATION_FLAGS += -DDR_ARM_VE_A9X4_QEMU
    PLATFORM_CORE_COUNT := 4
    PLATFORM_CLUSTER_COUNT := 1
endif

#-------------------------------------------------------------------------
# platform generic
ifeq ($(VALID_PLATFORM),TRUE)
    ARM_VENDOR := ARM
    ARM_CHIP := CORTEXA9
    ARM_ARCH := ARMv7
    CORTEXA9_SHAPE := VE

    ifeq ($(TOOLCHAIN),GNU)
       ARM_CPU := cortex-a9
       ARM_FPU := vfp
    else
       ARM_CPU := Cortex-A9.no_neon.no_vfp # for armcc --cpu=xxx
       ARM_FPU := SoftVFP # for armcc --fpu=xxx
    endif

    ARM_SUBDIR := CortexA9
endif

