/*
@file qsee_api_tests.c
@brief Contains test code for most of the QSEE APIs.

*/
/*===========================================================================
   Copyright (c) 2011 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE
  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsapps/sampleapp/src/qsee_api_tests.c#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $

# when       who     what, where, why
# --------   ---     ---------------------------------------------------------

===========================================================================*/
#include "qsee_log.h"
#include "qsee_heap.h"
#include "qsee_sfs.h"
#include "qsee_fs.h"
#include "qsee_i2c.h"
#include "qsee_comstr.h"
#include "qsee_spi.h"
#include "qsee_comstr.h"
#include "qsee_prng.h"
#include "qsee_core.h"

#define TOUCH_I2C_ADDRESS 0x4a                /* Address of the touch controller on the I2C bus */
#define TOUCH_I2C_DEVICE  QSEE_I2CPD_DEVICE_1 /* Bus the touch controller is on, BLSP 1 */

#define SPI_WRITE_ENABLE 1
#define SPI_READ_ENABLE 1
#define SPI_FULL_DUPLEX_ENABLE 1
#define SPI_LOOPBACK_ENABLED 0 //NOTE IF SPI_LOOPBACK_ENABLED is set to 1, then
							   // SPI_LOOPBACK mode should be enabled in 
							   // tzbsp_spi.c, other wise there will be data 
							   // mismatch
#define SPI_WRITE_BUF_SIZE 256
#define SPI_READ_BUF_SIZE  256
#define SPI_DEVICE_ID QSEE_SPI_DEVICE_1



/* Registers in the Atmel mXT Memory Map, Figure 2-1 of Protocol Guide */
#define REG_FAMILY_ID		  0x00 

static int run_security_state_test(void);

int run_core_test(void* cmd, uint32 cmdlen, void* rsp, uint32 rsplen, void* data, uint32 len)
{
  uint32 retval = 0;
  uint32 output_buffer = 0;
  
  QSEE_LOG(QSEE_LOG_MSG_ERROR, "-- CORE UNIT TESTS ---------------------------------");

  retval = qsee_prng_getdata((uint8 *)(&output_buffer), sizeof(uint32));
  if(retval != sizeof(uint32))
  {
    QSEE_LOG(QSEE_LOG_MSG_ERROR, "  qsee_prng_getdata() FAILED, returned wrong number of bytes written.");
    return -1;
  }
  else
  {
    QSEE_LOG(QSEE_LOG_MSG_ERROR, "  qsee_prng_getdata() PASSED, output: %x", output_buffer);
  }

  if( !qsee_is_ns_range(cmd, cmdlen)  && qsee_is_ns_range(data, len))
  {
    QSEE_LOG(QSEE_LOG_MSG_ERROR, "  qsee_is_ns_range PASSED");
  }   
  else
  {
    QSEE_LOG(QSEE_LOG_MSG_ERROR, "  qsee_is_ns_range FAILED"); 
    return -1;
  }

  QSEE_LOG(QSEE_LOG_MSG_ERROR, "  qsee_read_jtag_id() output: %x",qsee_read_jtag_id());
  QSEE_LOG(QSEE_LOG_MSG_ERROR, "  qsee_read_serial_num() output: %x",qsee_read_serial_num());

  /*Sanity Test*/
  qsee_enforce_hdmi_hdcp_encryption(1);
  qsee_enforce_hdmi_hdcp_encryption(0);
  qsee_hdmi_status_read( &output_buffer,  &output_buffer,  &output_buffer);

  QSEE_LOG(QSEE_LOG_MSG_ERROR, "  qsee_get_secure_stat() tests DONE: %x",run_security_state_test());

  return 0;
}
 
int run_securechannel_test()
{
 QSEE_LOG(QSEE_LOG_MSG_ERROR, "-- SECURE CHANNEL UNIT TESTS ---------------------------------");
 
	/*add tests here*/

 QSEE_LOG(QSEE_LOG_MSG_ERROR, "   Secure channel tests are not yet implemented.");
 return 0;
}


int run_misc_test()
{
  WCHAR* w_a = (WCHAR*)qsee_malloc(128);
  char * a = (char*)qsee_malloc(64);
  char * b = (char*)qsee_malloc(64);

  QSEE_LOG(QSEE_LOG_MSG_ERROR, "-- COMSTR TESTS begin---------------------------------");

  if(!a || !b || !w_a)
  {
    QSEE_LOG(QSEE_LOG_MSG_ERROR, "-------------- qsee_malloc() failed");
    return -1;
  }

  if(!qsee_strlcpy(a, "Test string", 32))  //Returns the number of bytes copied, if succssful.  Should be non-zero. 
  {
    QSEE_LOG(QSEE_LOG_MSG_ERROR, "-------------- qsee_strlcpy() failed");
    return -1; 
  }

  if(!qsee_strlcpy(b, "Test string", 32))  //Returns the number of bytes copied, if succssful.  Should be non-zero. 
  {
    QSEE_LOG(QSEE_LOG_MSG_ERROR, "-------------- qsee_strlcpy() failed");
    return -1; 
  }

  if(!qsee_strlcat(a, "-ending-", 32)) // (a's string length is now 19 chars, and then a NULL char at the end.)
  { 
    QSEE_LOG(QSEE_LOG_MSG_ERROR, "-------------- qsee_strlcat() failed");
    return -1; 
  }

  if(0 != qsee_strnicmp(a, b, 4)) //Compare only first 4 bytes.  Return should be 0.
  {
    QSEE_LOG(QSEE_LOG_MSG_ERROR, "-------------- qsee_strnicmp() failed");
    return -1;  
  }

  if(0 == qsee_strnicmp(a, b, 32)) //Compare all 32 bytes.  Return should NOT be 0.
  {
    QSEE_LOG(QSEE_LOG_MSG_ERROR, "-------------- qsee_strnicmp() failed");
    return -1;
  }

  qsee_strtowstr(a, w_a, 64);

  if(19 != qsee_wstrlen(w_a))
  {
    QSEE_LOG(QSEE_LOG_MSG_ERROR, "-------------- qsee_strtowstr() or qsee_wstrlen() failed");
    return -1;
  }
  
  if(0 != qsee_wstrcmp(w_a, w_a))
  {
    QSEE_LOG(QSEE_LOG_MSG_ERROR, "-------------- qsee_wstrcmp() failed");
    return -1;
  }

  if(0 == qsee_wstrtostr(w_a, b, 32))
  {
    QSEE_LOG(QSEE_LOG_MSG_ERROR, "-------------- qsee_wstrtostr() failed");
    return -1;
  }

  if(0 != qsee_strnicmp(a, b, 32))
  {
    QSEE_LOG(QSEE_LOG_MSG_ERROR, "-------------- qsee_strnicmp() failed");
    return -1;
  }

  if(0 == qsee_wstrlcat(w_a, w_a, 64))
  {
    QSEE_LOG(QSEE_LOG_MSG_ERROR, "-------------- qsee_wstrlcat() failed");
    return -1;
  }

  if(w_a != qsee_wstrchr(w_a, 'T'))
  {
    QSEE_LOG(QSEE_LOG_MSG_ERROR, "-------------- qsee_wstrchr() failed");
    return -1;
  }

  qsee_free(a);
  qsee_free(b);
  qsee_free(w_a);
  
  QSEE_LOG(QSEE_LOG_MSG_ERROR, "-- COMSTR TESTS passed---------------------------------");
  return 0;
}

int run_buses_spi_test()
{
	int retval = 0;

#if 0  //Clocks and GPIOs are not enabled in SPI driver, therefore these tests will hang (hence commented out).

	qsee_spi_config_t spi_config;
	qsee_spi_transaction_info_t write_info;
	static uint8 write_data[SPI_WRITE_BUF_SIZE] __attribute__((aligned(64)));
	qsee_spi_transaction_info_t read_info;
	static uint8 read_data[SPI_READ_BUF_SIZE] __attribute__((aligned(64)));
	int i;

	//START SPI TEST CODE
	spi_config.spi_bits_per_word = 8;
	spi_config.spi_clk_always_on = QSEE_SPI_CLK_NORMAL;
	spi_config.spi_clk_polarity = QSEE_SPI_CLK_IDLE_HIGH;
	spi_config.spi_cs_mode = QSEE_SPI_CS_KEEP_ASSERTED;
	spi_config.spi_cs_polarity = QSEE_SPI_CS_ACTIVE_HIGH;
	spi_config.spi_shift_mode = QSEE_SPI_OUTPUT_FIRST_MODE;


	for (i = 0; i < SPI_WRITE_BUF_SIZE; i++) {
		write_data[i] = i;
	}
	write_info.buf_addr = write_data;
	write_info.buf_len = SPI_WRITE_BUF_SIZE*sizeof(uint8);


	if (0 != (retval = qsee_spi_open(SPI_DEVICE_ID))) {
		QSEE_LOG(QSEE_LOG_MSG_DEBUG, "qsee_spi_open1: retval = %d\n", retval);
	}

	do{
		for (i = 0; i < SPI_READ_BUF_SIZE; i++) {
			read_data[i] = 0XFF;
		}
		read_info.buf_addr = read_data;
		read_info.buf_len = SPI_READ_BUF_SIZE*sizeof(uint8);
		//PERFORM WRITE
		if (0 != (retval = qsee_spi_write(SPI_DEVICE_ID,&spi_config, &write_info))){
			QSEE_LOG(QSEE_LOG_MSG_DEBUG, "qsee_spi_write: retval = %d\n", retval);
			break;
		}

		QSEE_LOG(QSEE_LOG_MSG_DEBUG, "qsee_spi_write: retval = %d\n", retval);

		//PEFORM READ

		if (0 != (retval = qsee_spi_read(SPI_DEVICE_ID ,&spi_config, &read_info))){
			QSEE_LOG(QSEE_LOG_MSG_DEBUG, "qsee_spi_read: retval = %d\n", retval);
			break;
		}


		QSEE_LOG(QSEE_LOG_MSG_DEBUG, "qsee_spi_read: retval = %d\n", retval);

		if (0 != (retval = qsee_spi_full_duplex(SPI_DEVICE_ID ,&spi_config, 
						&write_info, &read_info))){
			QSEE_LOG(QSEE_LOG_MSG_DEBUG, "qsee_spi_full_duplex: retval = %d\n",
					retval);
			break;
		}

		if (retval == -1) {
			break;
		}


		QSEE_LOG(QSEE_LOG_MSG_DEBUG, "qsee_spi_full_duplex: retval = %d\n", 
				retval);
	}while(0);


	if (0 != (retval = qsee_spi_close(SPI_DEVICE_ID ))) {
		QSEE_LOG(QSEE_LOG_MSG_DEBUG, "qsee_spi_close1: retval = %d\n", retval);
	}
#endif

	QSEE_LOG(QSEE_LOG_MSG_DEBUG, "BUSES SPI TEST COMPLETE, retval = %d\n", retval);
	return retval;

}

int run_buses_test()
{
  int ret = 0;
  int retClose = 0;
    
  QSEE_LOG(QSEE_LOG_MSG_ERROR, "-- BUSES UNIT TESTS ---------------------------------");
  
  /* Open the bus. This will lock the bus to TZ */
  ret = qsee_i2c_open(TOUCH_I2C_DEVICE);
  if( ret != 0 )
  {
    QSEE_LOG(QSEE_LOG_MSG_ERROR, "   qsee_i2c_open() FAILED! returned = %ld", ret);
    return ret;
  }
  QSEE_LOG(QSEE_LOG_MSG_DEBUG, "   qsee_i2c_open() PASSED");

  /* This call requires the QUP clocks to be on, or will cause an L2 error */
#if 0
  do {
    uint8 familyId;
    qsee_i2c_config_t i2c_config;
    qsee_i2c_transaction_info_t  readInfo;
    qsee_i2c_bus_config_t busConfig;
    qsee_i2c_slave_device_config_t deviceConfig;
    
    i2c_config.p_bus_config = &busConfig;
    i2c_config.p_slave_config = &deviceConfig;
    
    /* swap HIGH and LOW bytes for address , as requested by the Atmel mXT specs */
    readInfo.start_addr = ((REG_FAMILY_ID >> 8) & 0x00FF) | ((REG_FAMILY_ID & 0x00FF) << 8);
    readInfo.p_buf = &familyId;
    readInfo.buf_len = sizeof(familyId);
    readInfo.total_bytes = 0;
  
    ret = qsee_i2c_read(TOUCH_I2C_DEVICE, &i2c_config, &readInfo);
    if( ret != 0 )
    {
      QSEE_LOG(QSEE_LOG_MSG_ERROR, "   qsee_i2c_read() FAILED! returned = %ld", ret);
      break;
    }
    else
    {
      QSEE_LOG(QSEE_LOG_MSG_DEBUG, "   qsee_i2c_read() PASSED");
    }
  } while (0);
#endif
  retClose = qsee_i2c_close(TOUCH_I2C_DEVICE);
  if( retClose != 0 )
  {
    QSEE_LOG(QSEE_LOG_MSG_ERROR, "   qsee_i2c_close() FAILED! returned = %ld", ret);
  }
  else
  {
    QSEE_LOG(QSEE_LOG_MSG_DEBUG, "   qsee_i2c_close() PASSED");
  }
  
  if (ret)
    return ret;
  
  return retClose;
}


int security_state_test()
{
  int ret = 0;
  qsee_secctrl_secure_status_t status;
  memset(&status, 0, sizeof(status));

  ret = qsee_get_secure_state(&status);
  QSEE_LOG(QSEE_LOG_MSG_ERROR, "  qsee_get_secure_state() pass test: %x %x, ret %d",
               status.value[0], status.value[1], ret);
  if (0 != ret) {
    QSEE_LOG(QSEE_LOG_MSG_ERROR, "qsee_get_secure_state() call fail and returns %d\n", ret);
    return 1;
  }
  if (0 == (status.value[0] & 0x1f)) {
     QSEE_LOG(QSEE_LOG_MSG_DEBUG, "secure boot is setup");
     ret = 0x0;
  } else {
     QSEE_LOG(QSEE_LOG_MSG_DEBUG, "secure boot is NOT setup");
     ret = 0x02;
  }

  if (0 == (status.value[0] & 0x20)) {
     QSEE_LOG(QSEE_LOG_MSG_DEBUG, "RPMB is provisioned");
  } else {
     QSEE_LOG(QSEE_LOG_MSG_DEBUG, "RPMB boot is NOT provisioned");
     ret |= 0x4;
  }

  return ret;
}

static int run_security_state_test()
{
  int ret = 0;
  qsee_secctrl_secure_status_t status;
  memset(&status, 0, sizeof(status));

  ret = qsee_get_secure_state(&status);
  QSEE_LOG(QSEE_LOG_MSG_ERROR, "  qsee_get_secure_state() pass(=1): %d, val: %x %x",
               (ret == 0), status.value[0], status.value[1]);

  ret = qsee_get_secure_state(NULL);
  QSEE_LOG(QSEE_LOG_MSG_ERROR, "  qsee_get_secure_state() negative, NULL buffer, pass(=1): %d", (ret != 0));
 
  return ret;
}

