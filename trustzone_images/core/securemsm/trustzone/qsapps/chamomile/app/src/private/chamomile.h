/*
* Copyright (c) 2015 Qualcomm Technologies, Inc.  All Rights Reserved.
* Qualcomm Technologies Proprietary and Confidential.
*/

#pragma once

#define SEED_LEN 16

/**
 * Generates the provisioning key used to encrypt/decrypt
 * the input data for chamomile provisioning.
 * 
 * @param image_provisioning_seed 
 * @param oem_specific_param OEM specific parameter. Taken from 
 *                           bytes 12-15 of DPS
 * @param dku_param Device key update. Taken from bytes 1-11 of 
 *                  DPS
 * @param gen_num Generation number. Byte 0 of DPS is the 
 *                gen_num parameter
 * @param[out] provisioning_key Output provisioning key buffer
 * @return 0 on success. Negative on failure.
 */
int chamomile_generate_provisioning_key(
   unsigned char* image_provisioning_seed,
   unsigned char* oem_specific_param,
   unsigned char* dku_param,
   unsigned char gen_num,
   unsigned char* provisioning_key);

/**
 * Verifies the mac value over the input data.
 * 
 * @param ciphertext Message with MAC to be verifed
 * @param ciphertext_len Length of message
 * @param mac MAC value from input data 
 * @return 0 on sucess. Negative on failure. 
 */
int chamomile_verify_mac(
   unsigned char* ciphertext,
   uint32 ciphertext_len,
   unsigned char* mac);

/**
 * Decrypt the cipher text contained within input
 * data message.
 * 
 * @param ceiv IV supplied in data message 
 * @param cek Provisioning Key 
 * @param ciphertext Input ciphertext buffer
 * @param ciphertext_len Input ciphertext buffer length
 * @param plaintext[out] Output plaintext buffer
 * @param plaintext_len[in/out] Output plaintext buffer
 *                     length. Modified to the actual bytes
 *                     written.
 * @return 0 on sucess. Negative on failure.
 */
int chamomile_decrypt_prov_data(
   unsigned char* ceiv,
   unsigned char* cek,
   unsigned char* ciphertext,
   uint32 ciphertext_len,
   unsigned char* plaintext,
   uint32* plaintext_len);

/**
 * Get the image provisioning seed from the embedded
 * key.
 * 
 * @param[out] seedA Output image_provisiong_seed buffer
 * @return 0 on sucess. Negative on failure.
 */
int get_image_provisioning_seed(unsigned char* seedA);

