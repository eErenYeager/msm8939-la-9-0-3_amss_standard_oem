/*
* Copyright (c) 2015 Qualcomm Technologies, Inc.  All Rights Reserved.
* Qualcomm Technologies Proprietary and Confidential.
*/

#pragma once

#include "tci.h"
#include "paApiParams.h"

/**
* @cond
* Command ID's for communication HLOS Client App -> TZ App.
*/

#define PA_CMD_PROVISION_DATA           1

/*
* @endcond
*/

typedef union {
  cmdProvisionData_t              provisionData;
} paCommandPayload_t;

typedef union {
  rspProvisionData_t              provisionData;
} paResponsePayload_t;

typedef struct __attribute__ ((aligned (0x40))) {
  tciCommandHeader_t           header;
  paCommandPayload_t  payload;
} tciCommand_t;

typedef struct __attribute__ ((aligned (0x40))) {
  tciResponseHeader_t           header;
  paResponsePayload_t  payload;
} tciResponse_t;

typedef struct {
  tciCommand_t command;
  tciResponse_t response;
} patci_t;
