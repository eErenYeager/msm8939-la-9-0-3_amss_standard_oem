/*
* Copyright (c) 2015 Qualcomm Technologies, Inc.  All Rights Reserved.
* Qualcomm Technologies Proprietary and Confidential.
*/

#pragma once

#include <stdint.h>

#ifdef __RVCT__
#pragma anon_unions
#endif

#define QSEE_DPS_MAX_SIZE 16
#define MAX_SG_ENTRY 512

struct scattered_entries {
    struct {
       uint32_t pu8PhysAddr;
       uint32_t u32Len;
    } list[MAX_SG_ENTRY];
};

/******************************************************************************/
/** (1) ProvisionData */
/******************************************************************************/
typedef struct {
  char appName[32];
  uint32_t bufferLen;
  struct scattered_entries data;
  unsigned char dps[QSEE_DPS_MAX_SIZE];
} cmdProvisionData_t;
/** ProvisionData Response */
typedef struct {
  uint32_t messageLen;
} rspProvisionData_t;
