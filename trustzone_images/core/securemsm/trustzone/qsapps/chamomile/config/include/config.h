/*
* Copyright (c) 2015 Qualcomm Technologies, Inc.  All Rights Reserved.
* Qualcomm Technologies Proprietary and Confidential.
*/

#pragma once

#include <stdint.h>

#define QSAPP_CONFIG_SIZE 0x1400 /* 512 bytes */

#define QSAPP_MAGIC_0  0x41565250  // "PRVA"
#define QSAPP_MAGIC_1  0x50525641  // "AVRP"

#define QSAPP_CONFIG_VERSION_MAJOR 0
#define QSAPP_CONFIG_VERSION_MINOR 1

#define QSAPP_CLIENTS_MAX    5
#define QSAPP_IPS_MAX_SIZE   16
#define QSAPP_DPS_MAX_SIZE   16
#define QSAPP_DEVICE_KEY_LEN 16

#define MAGIC_NUMBER_LEN 32
#define PATCH_SIZE 4096

typedef struct key_struct {
	unsigned char magic_number1[MAGIC_NUMBER_LEN];
	unsigned char key_array[PATCH_SIZE];
	unsigned char magic_number2[MAGIC_NUMBER_LEN];
} key_struct;

typedef struct {
  uint16_t version_major;
  uint16_t version_minor;
  key_struct obfuscated_key; 
  char client[QSAPP_CLIENTS_MAX][32];  /* zero terminated */
} config_arg_t;

typedef struct {
  uint32_t       magic0;
  uint16_t       major;  /* integer portion of version number */
  uint16_t       minor;  /* 1/100ths portion of version number */
  config_arg_t arg;
  uint8_t reserved[QSAPP_CONFIG_SIZE - 12 - sizeof(config_arg_t)];
  uint32_t magic1;
} qsapp_config_t;
