/**
 * @QMISECAPP_main.h
 *===========================================================================
 *    Copyright (c) 2014-2016 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
 *===========================================================================
 */
#ifndef __SFSAPP_MAIN_H__
#define __SFSAPP_MAIN_H__

/** Major Version Number of the IDL used to generate this file */
#define SFS_V01_IDL_MAJOR_VERS 0x01
/** Revision Number of the IDL used to generate this file */
#define SFS_V01_IDL_MINOR_VERS 0x00
/** Major Version Number of the qmi_idl_compiler used to generate this file */
#define SFS_V01_IDL_TOOL_VERS 0x06
/** Maximum Defined Message ID */
#define SFS_V01_MAX_MESSAGE_ID 0x0021

#define QMI_SFS_TZ_APP_STATUS_SIZE_V01 4
#define QMI_SFS_TZ_APP_CMD_ID_SIZE_V01 4
#define QMI_SFS_TZ_APP_PARTITION_SIZE_V01 512
#define QMI_SFS_SEC_MSG_PAYLOAD_LEN_SIZE_V01 4
#define QMI_SFS_SEC_MSG_HEADER_SIZE_V01 100
#define QMI_SFS_TZ_APP_PAYLOAD_SIZE_V01 4
#define QMI_SFS_TZ_APP_WRITE_REQ_MSG_SIZE_V01 616
#define QMI_SFS_TZ_APP_WRITE_RSP_MSG_SIZE_V01 108
#define QMI_SFS_TZ_APP_READ_REQ_MSG_SIZE_V01 104
#define QMI_SFS_TZ_APP_READ_RSP_MSG_SIZE_V01 620

#endif //__SFSAPP_MAIN_H__

