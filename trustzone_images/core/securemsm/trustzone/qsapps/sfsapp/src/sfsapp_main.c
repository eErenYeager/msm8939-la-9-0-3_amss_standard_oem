/**
 * @app_main.c
 *===========================================================================
 *    Copyright (c) 2014-2016 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
 *===========================================================================
 */
#include <comdef.h>
#include <string.h>
#include "qsee_log.h"
#include "qsee_services.h"
#include "qsee_timer.h"
#include "qsee_heap.h"
#include <limits.h>
#include <float.h>
#include <stdio.h>
#include "sfsapp_main.h"
#include "qsee_stor.h"
#include "qsee_securechannel.h"
#include "IxErrno.h"
#include "stringl.h"

/* QSEECOM Communication */

char TZ_APP_NAME[] = { "sfsapp" };

#define QMI_CMD_0_READ_VERSION  0
#define QMI_CMD_1_WRITE_VERSION 1

#define SFS_RESULT_SUCCESS                 0
#define SFS_RESULT_FAILURE                 1
#define SFS_RESULT_UNSUPPORTED_CMD         2
#define SFS_RESULT_DEV_INIT_FAIL           3
#define SFS_RESULT_MAGIC_NUM_MISMATCH      4
#define SFS_RESULT_ADD_PARTITION_FAIL      5
#define SFS_RESULT_OPEN_PARTITION_FAIL     6
#define SFS_RESULT_READ_SECTORS_FAIL       7
#define SFS_RESULT_WRITE_SECTORS_FAIL      8
#define SFS_RESULT_MALLOC_FAIL             9
#define SFS_RESULT_NULL_ARGUMENTS          10
#define SFS_RESULT_INVALID_ARGUMENTS       11
#define SFS_RESULT_AUTH_FAIL               12

#define SFSAPP_VERSION_MAJOR 0x0
#define SFSAPP_VERSION_MINOR 0x1

typedef struct {
  uint32_t cmd_id;
  uint8_t  data[QMI_SFS_TZ_APP_PARTITION_SIZE_V01];
} __attribute__((packed)) write_req;

typedef struct {
  uint32_t status;
  uint32_t cmd_id;
} __attribute__((packed)) write_rsp;

typedef struct {
  uint32_t cmd_id;
} __attribute__((packed)) read_req;

typedef struct {
  uint32_t status;
  uint32_t cmd_id;
  uint8_t  data[QMI_SFS_TZ_APP_PARTITION_SIZE_V01];
} __attribute__((packed)) read_rsp;

/* RPMB Structures */

#define RPMB_PARTITION_MAGIC_NUM     (0x4D41524B)
#define RPMB_COUNTER_PARTITION_ID    (666)
#define RPMB_COUNTER_SECTOR          (0)

static  qsee_stor_device_handle_t   rpmb_device_handle = NULL;
static  qsee_stor_client_handle_t   rpmb_client_handle = NULL;

typedef struct {
  uint8* data[QMI_SFS_TZ_APP_PARTITION_SIZE_V01];
} rpmb_data_t;

rpmb_data_t  rpmb_counter_data;

unsigned int sfsapp_app_status = 0;

static int32 sfs_rpmb_counter_open_partition(void);
static int32 sfs_rpmb_counter_init(void);
static void sfs_rpmb_counter_read(read_rsp *rsp, 
                                  uint32 *rsplen);
static void sfs_rpmb_counter_write(write_req *req, 
                                   uint32 reqlen, 
                                   read_rsp *rsp, 
                                   uint32 *rsplen);

#define F __func__

/**
 @brief
 Add any app specific initialization code here
 QSEE will call this function after secure app is loaded and
 authenticated
 */
void tz_app_init(void) {
  /*  App specific initialization code*/
  QSEE_LOG(QSEE_LOG_MSG_DEBUG, "qmisecapp Init ");
  QSEE_LOG(QSEE_LOG_MSG_DEBUG, "Version: %x.%x", SFSAPP_VERSION_MAJOR,
           SFSAPP_VERSION_MINOR);
}

/**
 @brief
 QSEE will call when a user space entity sends a command 
 (request/response) to the secure application. 
 */
void tz_app_cmd_handler(void* req, uint32 reqlen, void* rsp, uint32 rsplen) 
{
  /* Request-response buffers are allocated by non-secure side*/
  /* Add code to process requests and set response (if any)*/
  void* decreq;
  uint32 decreqlen;
  void* decrsp;
  uint32 decrsplen;

  uint32 cipherlen;
  void *ciphertxt;

  /* Sanity Check */
  if ((!req) || (!rsp)) {
    QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s: Null Arg req_p=%d rsp_p=%d", F, req, rsp);
    return;
  }
  if (!reqlen || !rsplen) {
    QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s: Invalid Arg reqlen=%d rsplen=%d", F, reqlen, rsplen);
    return;
  }
  memset(rsp, 0, rsplen);

  /* Allocated buffers for decrypted request and response */
  if ((decreq = qsee_malloc(reqlen)) == NULL) 
  {
    QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s: Out of Memory", F);
    return;
  }
  decreqlen = reqlen;
  if ((decrsp = qsee_malloc(rsplen)) == NULL) 
  {
    QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s: Out of Memory", F);
    qsee_free(decreq);
    return;
  }
  decrsplen = rsplen;

  /* Sanity check for request/response length */
  if (reqlen <= sizeof(uint32) || rsplen <= sizeof(uint32))
  {
    QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s: Invalid Arg reqlen=%d rsplen=%d", F, reqlen, rsplen);
    return;
  }

  do
  {
    QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s: reqlen=%d rsplen=%d", F, reqlen, rsplen);

    /* Clear all allocated memory */
    memset(decreq, 0, decreqlen);
    memset(decrsp, 0, decrsplen);

    /* First 4 bytes of the request is ciphertxt payload size */
    cipherlen = *((uint32*)req);
    ciphertxt = ((uint32*)req) + 1;

    /* Sanity Check for payload length */
    if (cipherlen > reqlen - sizeof(uint32))
    {
      QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s: Invalid Arg cipherlen=%d", F, cipherlen);
      *((uint32*)decrsp) = SFS_RESULT_INVALID_ARGUMENTS;
      decrsplen = sizeof(uint32);
      break;
    }

    /* First we need to authenticate and retrieve info from MPSS SFS */
    if(E_SUCCESS != qsee_authenticate_decrypt_message(QSEE_SC_MPSS, 
                                                      QSEE_SC_CID_SSM,
                                                      (uint8*)ciphertxt,
                                                      cipherlen,
                                                      (uint8*)decreq,
                                                      &decreqlen))
    {
      QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s: Req authentication FAILED! Attack Scenario! clen=%d dlen=%d", 
               F, cipherlen, decreqlen);
      *((uint32*)decrsp) = SFS_RESULT_AUTH_FAIL;
      decrsplen = sizeof(uint32);
      break;
    }

    /* Depending whether its a read or write, we need to perform diff actions */

    QSEE_LOG(QSEE_LOG_MSG_DEBUG, "%s: cmd_id=%d", F, *((uint32_t*)decreq));

    switch (*((uint32_t*)decreq)) {

      case QMI_CMD_0_READ_VERSION:
        QSEE_LOG(QSEE_LOG_MSG_DEBUG, "%s: Read the version", F);

        /* Validate lenth of request */
        if (sizeof(read_req) != decreqlen) {
          QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s: read req payload size is incorrect! want=%d got=%d", 
               F, sizeof(read_req), decreqlen);
          break;
        }

        /* Read from the RPMB partition */
        sfs_rpmb_counter_read(decrsp, &decrsplen); 

        break;

      case QMI_CMD_1_WRITE_VERSION:
        QSEE_LOG(QSEE_LOG_MSG_DEBUG, "%s: Write the version", F);

        /* Validate lenth of request */
        if (sizeof(write_req) != decreqlen) {
          QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s: read req payload size is incorrect! want=%d got=%d", 
               F, sizeof(write_req), decreqlen);
          break;
        }

        /* Write to the RPMB partition */
        sfs_rpmb_counter_write(decreq, decreqlen, decrsp, &decrsplen);

        break;

      default:
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s: Unsupported cmd=%d", F, *((uint32_t*)decreq));
        break;
    }

  } while (0);

  cipherlen = rsplen - sizeof(uint32);
  ciphertxt = ((uint32*)rsp) + 1;

  QSEE_LOG(QSEE_LOG_MSG_DEBUG, "%s: qsee_secure_message clen=%d plen=%d", __func__, cipherlen, decrsplen);

  /* Package the secure payload to send back to MPSS */
  if(E_SUCCESS != qsee_secure_message(QSEE_SC_MPSS,
                                      QSEE_SC_CID_SSM,
                                      (uint8*)decrsp,
                                      decrsplen,
                                      ciphertxt,
                                      &cipherlen))
  {
    QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s: Encrypting message FAILED! dlen=%d, clen=%d", 
             F, decrsplen, *((uint32*)rsp));
    *((uint32*)rsp) = 0;
  }
  else
  {
    QSEE_LOG(QSEE_LOG_MSG_DEBUG, "%s: qsee_secure_message succeeded clen=%d", __func__, cipherlen);
    *((uint32*)rsp) = cipherlen;
  }

  /* Clean up resources */
  if (decreq)
    qsee_free(decreq);
  if (decrsp)
    qsee_free(decrsp);

  return;
}

/**
 @brief
 App specific shutdown
 App will be given a chance to shutdown gracefully
 */
void tz_app_shutdown(void) {
  /* app specific shutdown code*/
  QSEE_LOG(QSEE_LOG_MSG_DEBUG, "qmisecapp App shutdown");
  return;
}

static int32 sfs_rpmb_counter_open_partition(void)
{
  int32 ret = QSEE_STOR_SUCCESS;
  uint32 partition_id = RPMB_COUNTER_PARTITION_ID;

  do
  {
    /* try to initialize device */
    if(!rpmb_device_handle) 
    {

      ret = qsee_stor_device_init(QSEE_STOR_EMMC_RPMB, NULL, &rpmb_device_handle);
      if(QSEE_STOR_SUCCESS != ret)
      {
        QSEE_LOG(TZ_LOG_MSG_ERROR, "%s: qsee_stor_device_init failed {%x}", __func__, ret);
        QSEE_LOG( TZ_LOG_MSG_ERROR, "%s: rmpb key not provisioned", __func__);
        ret = SFS_RESULT_DEV_INIT_FAIL;
        break;
      }
    }

    if(!rpmb_client_handle) 
    {

      /* check if the init failed is because partition has not been created */
      ret = qsee_stor_open_partition(&rpmb_device_handle, partition_id,
                                  &rpmb_client_handle);

      if(QSEE_STOR_PARTI_NOT_FOUND_ERROR == ret) 
      {
        /* add a partition */
        ret = qsee_stor_add_partition(&rpmb_device_handle, partition_id, 4);
        if(QSEE_STOR_SUCCESS != ret)
        {
          QSEE_LOG(TZ_LOG_MSG_ERROR, "%s: qsee_stor_add_partition failed {%x}", __func__, ret);
          ret = SFS_RESULT_ADD_PARTITION_FAIL;
          break;
        }

        /* open a partition */
        ret = qsee_stor_open_partition(&rpmb_device_handle, partition_id,
                                       &rpmb_client_handle);

        if(QSEE_STOR_SUCCESS != ret) 
        {
          QSEE_LOG(TZ_LOG_MSG_ERROR, "%s: qsee_stor_open_partition failed {%x}", __func__, ret);
          ret = SFS_RESULT_OPEN_PARTITION_FAIL;
          break;;
        }

        memset((uint8 *)&rpmb_counter_data, 0, sizeof(rpmb_data_t));
        ret = qsee_stor_write_sectors(&rpmb_client_handle, RPMB_COUNTER_SECTOR,
                                      1, (uint8 *)&rpmb_counter_data);
        if (QSEE_STOR_SUCCESS != ret)
        {
          QSEE_LOG(TZ_LOG_MSG_ERROR, "%s: qsee_stor_write_sectors failed {%x}", __func__, ret);
          ret = SFS_RESULT_WRITE_SECTORS_FAIL;
          break;
        }

      } /* end of checking qsee_stor_open_partition */
      
    } /* end of checking g_rpmb_client_handle */
    
  } while(0);
 
  return ret;
}


static int32 sfs_rpmb_counter_init(void)
{
  int32 ret = QSEE_STOR_SUCCESS;

  do
  {

    if ( !rpmb_device_handle || !rpmb_client_handle ) 
    {
      /* try to open a rpmb partition */
      ret = sfs_rpmb_counter_open_partition();
      if ( ret )
      {
        QSEE_LOG(TZ_LOG_MSG_ERROR, "%s: sfs_rpmb_counter_open_partition failed {%x}", __func__, ret);
        break;
      }
      
      /* check magic number */
      memset((uint8 *)&rpmb_counter_data, 0, sizeof(rpmb_data_t));
      ret = qsee_stor_read_sectors(&rpmb_client_handle, RPMB_COUNTER_SECTOR,
                                1, (uint8 *)&rpmb_counter_data);
      if(QSEE_STOR_SUCCESS != ret) 
      {
        QSEE_LOG(TZ_LOG_MSG_ERROR, "%s: qsee_stor_read_sectors failed {%x}", __func__, ret);
        ret = SFS_RESULT_READ_SECTORS_FAIL;
        break;
      }

    } /* end of checking if it has been initialized */

  } while (0);

  return ret;
}

static void sfs_rpmb_counter_read(read_rsp *rsp, uint32 *rsplen)
{
  int32 ret = QSEE_STOR_SUCCESS;

  do
  {
    rsp->cmd_id = QMI_CMD_0_READ_VERSION;

    ret = sfs_rpmb_counter_init();
    if (ret) 
    {
      QSEE_LOG(TZ_LOG_MSG_ERROR, "%s: sfs_rpmb_counter_init failed {%x}", __func__, ret);
      rsp->status = ret;
      *rsplen = sizeof(rsp->status)+sizeof(rsp->cmd_id);
      break;
    }

    /* Copy RPMB data into response */
    memscpy(rsp->data, sizeof(rsp->data), &rpmb_counter_data, sizeof(rpmb_data_t));

    rsp->status = SFS_RESULT_SUCCESS;

    /* Set the size of the response */
    *rsplen = sizeof(read_rsp);

  } while (0);

}

static void sfs_rpmb_counter_write(write_req *req, uint32 reqlen, read_rsp *rsp, uint32 *rsplen)
{
  int32 ret = QSEE_STOR_SUCCESS;

  do
  {
    rsp->cmd_id = QMI_CMD_1_WRITE_VERSION;

    ret = sfs_rpmb_counter_init();
    if (ret)
    {
      QSEE_LOG(TZ_LOG_MSG_ERROR, "%s: sfs_rpmb_counter_init failed {%x}", __func__, ret);
      rsp->status = ret;
      *rsplen = sizeof(rsp->status)+sizeof(rsp->cmd_id);
      break;
    }

    /* Write Request Data into RPMB */
    ret = qsee_stor_write_sectors(&rpmb_client_handle, RPMB_COUNTER_SECTOR,
                                  1, (uint8 *)req->data);
    if (QSEE_STOR_SUCCESS != ret)
    {
      QSEE_LOG(TZ_LOG_MSG_ERROR, "%s: qsee_stor_write_sectors failed {%x}", __func__, ret);
      rsp->status = SFS_RESULT_WRITE_SECTORS_FAIL;
      *rsplen = sizeof(rsp->status)+sizeof(rsp->cmd_id);
      break;
    }

    QSEE_LOG(QSEE_LOG_MSG_DEBUG, "%s: qsee_stor_write_sectors succeeded", __func__);

    /* Copy data to local copy once written to RPMB */
    memscpy(&rpmb_counter_data, sizeof(rpmb_data_t), req->data, sizeof(req->data));

    rsp->status = SFS_RESULT_SUCCESS;

    /* Set the size of the response */
    *rsplen = sizeof(write_rsp);

  } while (0);

}

