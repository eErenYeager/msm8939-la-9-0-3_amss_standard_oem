/*
* Copyright (c) 2014 Qualcomm Technologies, Inc.
* All Rights Reserved.
* Confidential and Proprietary - Qualcomm Technologies, Inc.
*/

#include "config.h"

#define SECURE_PIN_VERSION 1

#define SFS_ROOT "/persist/data/securepin/"

#define SUI_ENROLL_TITLE              "Enrolling, enter PIN"
#define SUI_ENROLL_CONFIRM_TITLE      "Enrolling, confirm PIN"
#define SUI_CHANGEPIN_TITLE           "Change PIN. Old PIN"
#define SUI_CHANGEPIN_NEXT_TITLE      "Change PIN. Next PIN"
#define SUI_CHANGEPIN_CONFIRM_TITLE   "Change PIN. Confirm PIN"
#define SUI_MATCH_TITLE               "Enter PIN"
#define SUI_SHOWMESSAGE_TITLE         "Confirm transaction"
#define SUI_SHOWMESSAGE_OK_LABEL      "Ok"
#define SUI_SHOWMESSAGE_CANCEL_LABEL  "Cancel"


/*lint -esym(506,g_config) */
qsapp_config_t __attribute__((section("zconfig"))) g_config = {
  QSAPP_MAGIC_0,
  QSAPP_CONFIG_VERSION_MAJOR,
  QSAPP_CONFIG_VERSION_MINOR,
  {
    SECURE_PIN_VERSION,
    {
      "fidocrypto",
      {0},
      {0},
      {0},
      {0}
    },
    10, /**< maxUserNumber */
    SFS_ROOT,
    16, /**< ulMaxPinLen */
    4,  /**< ulMinPinLen */
    0,  /**< honeypot */
    10000, /**< unlockTimeStep in ms */
    3, /**< maxLoginAttempts */
    4096, /**< userPinIterations */
    0, /**< forceRandomizedKeypad */
    0, /**< forceSecureKeyboard */
    10000, /**< secureInputTimeout */
    SUI_ENROLL_TITLE,
    SUI_ENROLL_CONFIRM_TITLE,
    SUI_CHANGEPIN_TITLE,
    SUI_CHANGEPIN_NEXT_TITLE,
    SUI_CHANGEPIN_CONFIRM_TITLE,
    SUI_MATCH_TITLE,
    SUI_SHOWMESSAGE_TITLE,
    SUI_SHOWMESSAGE_OK_LABEL,
    SUI_SHOWMESSAGE_CANCEL_LABEL
  },
  {0},
  QSAPP_MAGIC_1
};


