/*
* Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
* Qualcomm Technologies Proprietary and Confidential.
*/

#pragma once

#include <stdint.h>
#include <auth.h>

#define QSAPP_CONFIG_SIZE 0x400 /* 1024 bytes */

#define QSAPP_MAGIC_0  0x4E504353  // "SCPN"
#define QSAPP_MAGIC_1  0x5343504E  // "PCDF"

#define QSAPP_CONFIG_VERSION_MAJOR 0
#define QSAPP_CONFIG_VERSION_MINOR 1

#define QSAPP_OPTIONAL_PARTNERTS_MAX  5

typedef struct {
  uint8_t version;
  char partners_app_name[QSAPP_OPTIONAL_PARTNERTS_MAX][32];  /* zero terminated */
  uint32_t maxUserNumber;
  char sfsRoot[64];
  /* PIN */
  uint32_t ulMaxPinLen;           /* in chars */
  uint32_t ulMinPinLen;           /* in chars */
  uint32_t honeypot;
  uint32_t unlockTimeStep; /**< in ms */
  uint32_t maxLoginAttempts;
  uint32_t userPinIterations;   /**< PBKDF2 iterations */
  /* Secure UI */
  uint32_t forceRandomizedKeypad;
  uint32_t forceSecureKeyboard;
  uint32_t secureInputTimeout;
  char sui_enroll_title[64];
  char sui_enroll_confirm_title[64];
  char sui_changePIN_currentTitle[64];
  char sui_changePIN_nextTitle[64];
  char sui_changePIN_confirmTitle[64];
  char sui_match_title[64];
  char sui_showMessage_title[64];
  char sui_showMessage_ok_label[16];
  char sui_showMessage_cancel_label[16];
} config_arg_t;

typedef struct {
  uint32_t       magic0;
  uint16_t       major;  /* integer portion of version number */
  uint16_t       minor;  /* 1/100ths portion of version number */
  config_arg_t arg;
  uint8_t reserved[QSAPP_CONFIG_SIZE - 12 - sizeof(config_arg_t)];
  uint32_t magic1;
} qsapp_config_t;
