#===============================================================================
#
# App Core
#
# GENERAL DESCRIPTION
#    build script
#
#
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsapps/securepin/app/build/SConscript#1 $
#  $DateTime: 2018/02/07 00:37:16 $
#  $Author: mplp4svc $
#  $Change: 15409075 $
#                      EDIT HISTORY FOR FILE
#
#  This section contains schedulerents describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#===============================================================================
Import('env')
env = env.Clone()

#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------
SRCPATH = "${BUILD_ROOT}/core/securemsm/trustzone/qsapps/securepin/app/src"

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

#-------------------------------------------------------------------------------
# Compiler, object, and linker definitions
#-------------------------------------------------------------------------------

# do not generate thumb code for inline assembler code
#env.Append(ARMCC_OPT = ' --arm')

env.PublishPrivateApi('SECUREMSM_SUI', [
   "${INC_ROOT}/core/securemsm/sse/qsee/SecureDisplay/libgd/include",
   "${INC_ROOT}/core/securemsm/sse/qsee/SecureDisplay/libpng/include",
   "${INC_ROOT}/core/securemsm/sse/qsee/SecureDisplay/zlib/include",
   "${INC_ROOT}/core/securemsm/sse/qsee/SecureUI/inc",
   "${INC_ROOT}/core/securemsm/sse/qsee/SecureUILib/include",
])

# Secure Touch includes
env.PublishPrivateApi('SSE_SECURE_TOUCH_API', [
   '${INC_ROOT}/core/securemsm/sse/SecureTouch/common/include',
   '${INC_ROOT}/core/securemsm/sse/SecureTouch/layout/include',
])
env.PublishPrivateApi('SSE_TOUCH_CONTROLLER_API', [
   '${INC_ROOT}/core/securemsm/sse/SecureTouch/controller/inc',
])
env.PublishPrivateApi('SSE_TOUCH_CONTROLLER_QSEE_API', [
   '${INC_ROOT}/core/securemsm/sse/qsee/SecureTouch/drTs/include',
])

env.Append(CPPPATH = "${COREBSP_ROOT}/securemsm/trustzone/qsapps/securepin/app/include")
env.Append(CPPPATH = "${COREBSP_ROOT}/securemsm/trustzone/qsapps/securepin/app/src/private")
env.Append(CPPPATH = "${COREBSP_ROOT}/securemsm/trustzone/qsee")
env.Append(CPPPATH = "${COREBSP_ROOT}/securemsm/trustzone/qsapps/securepin/config/include")

env.Append(CPPDEFINES = '-D_AEABI_PORTABILITY_LEVEL=1')
env.Append(CPPDEFINES = '-D_AEABI_LC_CTYPE=C')

INC_PATH_API = ["${INC_ROOT}/core/api/kernel/libstd/stringl"]
env.Append(CPPPATH = INC_PATH_API)

#----------------------------------------------------------------------------
# App core Objects
#----------------------------------------------------------------------------
APP_CORE_ENTRY_SOURCES = [
        '${BUILDPATH}/app_main.c',

        '${BUILDPATH}/functions/01_Initialize.c',
        '${BUILDPATH}/functions/02_GetCapabilities.c',
        '${BUILDPATH}/functions/03_GetInfo.c',
        '${BUILDPATH}/functions/04_EnrollSui.c',
        '${BUILDPATH}/functions/05_MatchSui.c',
        '${BUILDPATH}/functions/06_Disenroll.c',
        '${BUILDPATH}/functions/07_ChangePinSui.c',
        '${BUILDPATH}/functions/08_ShowMessageSui.c',
        '${BUILDPATH}/functions/09_GetToken.c',

        '${BUILDPATH}/functions.c',
        '${BUILDPATH}/spSecureUI.c',
]

UTILS_SOURCES = [
        '${BUILDPATH}/spStorage.c',
        '${BUILDPATH}/spUtils.c',
]


HEADERS = [
        '${BUILDPATH}/private/spSecureUI.h',
        '${BUILDPATH}/private/spStorage.h',
        '${BUILDPATH}/private/spUtils.h',
        '${BUILDPATH}/private/glob.h',
]

#-------------------------------------------------------------------------------
# Add Libraries to image
#-------------------------------------------------------------------------------
env.AddBinaryLibrary('SECUREPIN_IMAGE', '${BUILDPATH}/securepin_app', APP_CORE_ENTRY_SOURCES)
env.AddBinaryLibrary('SECUREPIN_IMAGE', '${BUILDPATH}/securepin_utils', UTILS_SOURCES)
env.AddBinaryLibrary('AUTHTEST_IMAGE', '${BUILDPATH}/securepin_utils', UTILS_SOURCES)

#-------------------------------------------------------------------------------
# Pack out files
#-------------------------------------------------------------------------------
env.CleanPack('SECUREPIN_IMAGE', HEADERS)

#-------------------------------------------------------------------------------
# Load sub scripts
#-------------------------------------------------------------------------------
env.LoadSoftwareUnits()
