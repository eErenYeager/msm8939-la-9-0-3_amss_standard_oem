/*
* Copyright (c) 2014 Qualcomm Technologies, Inc.
* All Rights Reserved.
* Qualcomm Technologies Confidential and Proprietary.
*/

#pragma once

#include <stddef.h>
#include <auth.h>
#include "spLengths.h"

#ifdef __RVCT__
#pragma anon_unions
#endif

#pragma pack(push, user, 1)

typedef struct {
  uint64_t releaseTime;
  uint32_t multiplier;
} sp_user_lock_t;

typedef struct {
  uint8_t salt[SALT_MAX_LEN];
  uint32_t iterations;
  uint8_t key[USER_KEY_LEN];
} user_key_t;

#define SO_USER_KEY_LEN QSO_SIZE(offsetof(user_key_t,key),sizeof(user_key_t)-offsetof(user_key_t,key))

typedef union {
  uint8_t value[SO_USER_KEY_LEN];
  user_key_t user_key;
  struct {
    QSoHeader_t header;
    user_key_t user_key;
  } wrappedUserKey;
} user_key_so_t;

typedef struct {
  char username[QC_USERNAME_MAX_LEN+1]; /** zero terminated */
  uint64_t userId;
  uint32_t index;
  uint32_t changed;
  char appId[QC_APP_ID_MAX_LEN+1]; /** zero terminated */
  char user_image_path[USER_IMAGE_PATH_LEN]; /** zero terminated */
  uint32_t randomizedKeypad;
  uint32_t secureKeyboard;
  uint32_t honeypot;
  sp_user_lock_t lock;
  uint32_t loginAttempts;
  user_key_so_t so_key;
} sp_user_reg_t;

/*
typedef struct {
  uint32_t changed;
  size_t len;
  union {
    uint8_t value[SO_SIZE(0, sizeof(sp_user_reg_t)];
    sp_user_reg_t user_reg;
    struct {
      QSoHeader_t header;
      sp_user_reg_t user_reg;
    } wrappedUserReg;
  };
} user_reg_so_t;
*/

#pragma pack(pop, user)


