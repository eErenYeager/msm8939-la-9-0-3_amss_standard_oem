/*
* Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
* Qualcomm Technologies Proprietary and Confidential.
*/

#pragma once

#include <stdint.h>
#include <auth.h>
#include "spLengths.h"

#ifdef __RVCT__
#pragma anon_unions
#endif

/******************************************************************************/
/** (1) Initialize */
/******************************************************************************/
typedef struct {
  uint32_t version;
} cmdInitialize_t;
typedef struct {
  uint32_t version;
} rspInitialize_t;
/******************************************************************************/
/** (2) GetCapabilities */
/******************************************************************************/
typedef struct {
  uint32_t reserved;
} cmdGetCapabilities_t;
/** GetCapabilities Response */
typedef struct {
  uint32_t reserved;
} rspGetCapabilities_t;
/******************************************************************************/
/** (3) GetInfo */
/******************************************************************************/
typedef struct {
  uint32_t reserved;
} cmdGetInfo_t;
/** GetInfo Response */
typedef struct {
  uint32_t reserved;
} rspGetInfo_t;
/******************************************************************************/
/** (4) Enroll */
/******************************************************************************/
typedef struct {
  uint32_t randomizedKeypad;
  uint32_t secureKeyboard;
  uint32_t honeypot;
  char username[QC_USERNAME_MAX_LEN+1];
  uint32_t imageLen;
  uint8_t image[USER_IMAGE_MAX_SIZE];
  char appId[QC_APP_ID_MAX_LEN+1];
} cmdEnroll_t;
/** Enroll Response */
typedef struct {
  qc_so_authentication_token_t so_token;
} rspEnroll_t;
/******************************************************************************/
/** (41) EnrollSuiStart */
/******************************************************************************/
typedef struct {
  uint32_t randomizedKeypad;
  uint32_t secureKeyboard;
  uint32_t honeypot;
  char username[QC_USERNAME_MAX_LEN+1];
  uint32_t imageLen;
  uint8_t image[USER_IMAGE_MAX_SIZE];
  char appId[QC_APP_ID_MAX_LEN+1];
} cmdEnrollSuiStart_t;
/** EnrollSuiStart Response */
typedef struct {
  uint32_t reserved;
} rspEnrollSuiStart_t;
/******************************************************************************/
/** (42) EnrollSuiConfirm */
/******************************************************************************/
typedef struct {
  uint32_t reserved;
} cmdEnrollSuiConfirm_t;
/** EnrollSuiConfirm Response */
typedef struct {
  uint32_t reserved;
} rspEnrollSuiConfirm_t;
/******************************************************************************/
/** (43) EnrollSuiFinish */
/******************************************************************************/
typedef struct {
  uint32_t reserved;
} cmdEnrollSuiFinish_t;
/** EnrollSuiFinish Response */
typedef struct {
  qc_so_authentication_token_t so_token;
} rspEnrollSuiFinish_t;
/******************************************************************************/
/** (5) Match */
/******************************************************************************/
typedef struct {
  char username[QC_USERNAME_MAX_LEN+1];
  char appId[QC_APP_ID_MAX_LEN+1];
} cmdMatch_t;
/** Match Response */
typedef struct {
  qc_so_authentication_token_t so_token;
} rspMatch_t;
/******************************************************************************/
/** (51) MatchSuiStart */
/******************************************************************************/
typedef struct {
  char username[QC_USERNAME_MAX_LEN+1];
  char appId[QC_APP_ID_MAX_LEN+1];
} cmdMatchSuiStart_t;
/** MatchSuiStart Response */
typedef struct {
  uint32_t reserved;
} rspMatchSuiStart_t;
/******************************************************************************/
/** (52) MatchSuiFinish */
/******************************************************************************/
typedef struct {
  uint32_t reserved;
} cmdMatchSuiFinish_t;
/** MatchSuiFinish Response */
typedef struct {
  qc_so_authentication_token_t so_token;
} rspMatchSuiFinish_t;
/******************************************************************************/
/** (6) Disenroll */
/******************************************************************************/
typedef struct {
  char username[QC_USERNAME_MAX_LEN+1];
} cmdDisenroll_t;
/** Disenroll Response */
typedef struct {
  uint32_t reserved;
} rspDisenroll_t;
/******************************************************************************/
/** (7) ChangePin */
/******************************************************************************/
typedef struct {
  uint32_t reserved;
} cmdChangePin_t;
/** ChangePin Response */
typedef struct {
  uint32_t reserved;
} rspChangePin_t;
/******************************************************************************/
/** (71) ChangePinSuiStart */
/******************************************************************************/
typedef struct {
  uint32_t reserved;
} cmdChangePinSuiStart_t;
/** ChangePinSuiStart Response */
typedef struct {
  uint32_t reserved;
} rspChangePinSuiStart_t;
/******************************************************************************/
/** (72) ChangePinSuiNewPin */
/******************************************************************************/
typedef struct {
  uint32_t reserved;
} cmdChangePinSuiNewPin_t;
/** ChangePinSuiNewPin Response */
typedef struct {
  uint32_t reserved;
} rspChangePinSuiNewPin_t;
/******************************************************************************/
/** (73) ChangePinSuiConfirm */
/******************************************************************************/
typedef struct {
  uint32_t reserved;
} cmdChangePinSuiConfirm_t;
/** ChangePinSuiConfirm Response */
typedef struct {
  uint32_t reserved;
} rspChangePinSuiConfirm_t;
/******************************************************************************/
/** (74) ChangePinSuiFinish */
/******************************************************************************/
typedef struct {
  uint32_t reserved;
} cmdChangePinSuiFinish_t;
/** EnrollSuiFinish Response */
typedef struct {
  uint32_t reserved;
} rspChangePinSuiFinish_t;
/******************************************************************************/
/** (8) ShowMessage */
/******************************************************************************/
typedef struct {
  uint32_t len;
  char content[QC_TRANSACTION_MAX_LEN];
  qc_so_authentication_token_t so_token;
  qc_user_verification_token_t enc_token;
  char secAppName[QC_SEC_APP_NAME_LEN]; /**< \0 terminated */
} cmdShowMessage_t;
/** ShowMessage Response */
typedef struct {
  qc_enc_transaction_t transaction;
} rspShowMessage_t;
/******************************************************************************/
/** (81) ShowMessageSuiStart */
/******************************************************************************/
typedef struct {
  char content[QC_TRANSACTION_MAX_LEN];
  qc_so_authentication_token_t so_token;
  qc_user_verification_token_t enc_token;
  char secAppName[QC_SEC_APP_NAME_LEN]; /**< \0 terminated */
} cmdShowMessageSuiStart_t;
/** ShowMessageSuiStart Response */
typedef struct {
  uint32_t reserved;
} rspShowMessageSuiStart_t;
/******************************************************************************/
/** (82) ShowMessageSuiFinish */
/******************************************************************************/
typedef struct {
  uint32_t reserved;
} cmdShowMessageSuiFinish_t;
/** ShowMessageSuiFinish Response */
typedef struct {
  qc_enc_transaction_t transaction;
} rspShowMessageSuiFinish_t;
/******************************************************************************/
/** (9) GetToken */
/******************************************************************************/
typedef struct {
  char secAppName[QC_SEC_APP_NAME_LEN]; /**< \0 terminated */
  qc_so_authentication_token_t so_token;
} cmdGetToken_t;
/** ShowMessage Response */
typedef struct {
  qc_user_verification_token_t token;
} rspGetToken_t;
