/*
* Copyright (c) 2013 Qualcomm Technologies, Inc.  All Rights Reserved.
* Qualcomm Technologies Proprietary and Confidential.
*/

#pragma once

#include "tci.h"
#include "securepinApiParams.h"

/**
* @cond
* Command ID's for communication HLOS Client App -> TZ App.
*/

#define SP_CMD_SUI_FLAG              0x08000000
#define SP_CMD_SUI_PROGRESS_FLAG     0x04000000
#define SP_CMD_SUI_FINAL_FLAG        0x02000000
#define SP_CMD_SUI_MASK              0x0FF00000

#define SP_CMD_INITIALIZE               1
#define SP_CMD_GET_CAPABILITIES         2
#define SP_CMD_GET_INFO                 3
#define SP_CMD_ENROLL                   4
#define SP_CMD_ENROLL_SUI_START         (SP_CMD_SUI_FLAG | 41)
#define SP_CMD_ENROLL_SUI_CONFIRM       (SP_CMD_SUI_FLAG | SP_CMD_SUI_PROGRESS_FLAG | 42)
#define SP_CMD_ENROLL_SUI_FINISH        (SP_CMD_SUI_FLAG | SP_CMD_SUI_FINAL_FLAG | 43)
#define SP_CMD_MATCH                    5
#define SP_CMD_MATCH_SUI_START          (SP_CMD_SUI_FLAG | 51)
#define SP_CMD_MATCH_SUI_FINISH         (SP_CMD_SUI_FLAG | SP_CMD_SUI_FINAL_FLAG | 52)
#define SP_CMD_DISENROLL                6
#define SP_CMD_CHANGE_PIN               7
#define SP_CMD_CHANGE_PIN_SUI_START     (SP_CMD_SUI_FLAG | 71)
#define SP_CMD_CHANGE_PIN_SUI_NEWPIN    (SP_CMD_SUI_FLAG | SP_CMD_SUI_PROGRESS_FLAG | 72)
#define SP_CMD_CHANGE_PIN_SUI_CONFIRM   (SP_CMD_SUI_FLAG | SP_CMD_SUI_PROGRESS_FLAG | 73)
#define SP_CMD_CHANGE_PIN_SUI_FINISH    (SP_CMD_SUI_FLAG | SP_CMD_SUI_FINAL_FLAG | 74)
#define SP_CMD_SHOW_MESSAGE             8
#define SP_CMD_SHOW_MESSAGE_SUI_START   (SP_CMD_SUI_FLAG | 81)
#define SP_CMD_SHOW_MESSAGE_SUI_FINISH  (SP_CMD_SUI_FLAG | SP_CMD_SUI_FINAL_FLAG | 82)
#define SP_CMD_GET_TOKEN                9


/*
* @endcond
*/

typedef union {
  cmdInitialize_t                 initialize;
  cmdGetCapabilities_t            getCapabilities;
  cmdGetInfo_t                    getInfo;
  cmdEnroll_t                     enroll;
  cmdEnrollSuiStart_t             enrollSuiStart;
  cmdEnrollSuiConfirm_t           enrollSuiConfirm;
  cmdEnrollSuiFinish_t            enrollSuiFinish;
  cmdMatch_t                      match;
  cmdMatchSuiStart_t              matchSuiStart;
  cmdMatchSuiFinish_t             matchSuiFinish;
  cmdDisenroll_t                  disenroll;
  cmdChangePin_t                  changePin;
  cmdChangePinSuiStart_t          changePinSuiStart;
  cmdChangePinSuiNewPin_t         changePinSuiNewPin;
  cmdChangePinSuiConfirm_t        changePinSuiConfirm;
  cmdChangePinSuiFinish_t         changePinSuiFinish;
  cmdShowMessage_t                showMessage;
  cmdShowMessageSuiStart_t        showMessageSuiStart;
  cmdShowMessageSuiFinish_t       showMessageSuiFinish;
  cmdGetToken_t                   getToken;
} spCommandPayload_t;

typedef union {
  rspInitialize_t                 initialize;
  rspGetCapabilities_t            getCapabilities;
  rspGetInfo_t                    getInfo;
  rspEnroll_t                     enroll;
  rspEnrollSuiStart_t             enrollSuiStart;
  rspEnrollSuiConfirm_t           enrollSuiConfirm;
  rspEnrollSuiFinish_t            enrollSuiFinish;
  rspMatch_t                      match;
  rspMatchSuiStart_t              matchSuiStart;
  rspMatchSuiFinish_t             matchSuiFinish;
  rspDisenroll_t                  disenroll;
  rspChangePin_t                  changePin;
  rspChangePinSuiStart_t          changePinSuiStart;
  rspChangePinSuiNewPin_t         changePinSuiNewPin;
  rspChangePinSuiConfirm_t        changePinSuiConfirm;
  rspChangePinSuiFinish_t         changePinSuiFinish;
  rspShowMessage_t                showMessage;
  rspShowMessageSuiStart_t        showMessageSuiStart;
  rspShowMessageSuiFinish_t       showMessageSuiFinish;
  rspGetToken_t                   getToken;
} spResponsePayload_t;

typedef struct __attribute__ ((aligned (0x40))) {
  tciCommandHeader_t           header;
  spCommandPayload_t  payload;
} tciCommand_t;

typedef struct __attribute__ ((aligned (0x40))) {
  tciResponseHeader_t           header;
  spResponsePayload_t  payload;
} tciResponse_t;

typedef struct {
  tciCommand_t command;
  tciResponse_t response;
} sptci_t;
