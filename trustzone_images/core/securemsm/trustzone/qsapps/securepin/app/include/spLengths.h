/*
 * Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
 * Qualcomm Technologies Proprietary and Confidential.
 */

#pragma once

#define USER_MIN_PIN_LEN 4
#define USER_PIN_LEN 64
#define USER_IMAGE_PATH_LEN    256
#define USER_IMAGE_MAX_SIZE    128*1024 /* 128 kB */
#define DESCRIPTION_LEN        256
#define TITLE_LEN               64
#define MAX_USER_NUMBER         32
#define USER_KEY_LEN            32
#define SALT_MAX_LEN            32



