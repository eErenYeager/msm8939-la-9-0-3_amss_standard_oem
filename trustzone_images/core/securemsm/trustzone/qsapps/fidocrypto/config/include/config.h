/*
* Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
* Qualcomm Technologies Proprietary and Confidential.
*/

#pragma once

#include <stdint.h>
#include "fcApiParams.h"
#include "ext_handler.h"

#pragma pack(push, guid, 1)

typedef struct {
  uint32_t data1;
  uint16_t data2;
  uint16_t data3;
  uint8_t  data4[8];
} guid_t;

#pragma pack(pop, guid)

typedef struct {
  uint16_t version_major;
  uint16_t version_minor;
  guid_t cookie;
  char secure_display_app_name[32];     /** zero terminated */
  uint32_t displayW;
  uint32_t displayH;
  char provisioning_agent[32];          /** zero terminated */
  uint32_t surrogateAttestation;
  uint16_t uvtVersion;
  uint16_t nonceLen;
  struct {
    char name[32];                      /** zero terminated */
    uint8_t version;
    uint16_t allowReprovisioning;
    uint64_t lifespan;
    char aaid[QC_AAID_MAX_LEN];
    uint16_t authType;
    uint16_t matcherProtection;
    uint32_t useBiolib;
    uint16_t secureDisplayAuthToken;
    uint16_t secureDisplayJointTokens;
    uint32_t userVerification;
    uint8_t contentType[QC_CONTENT_TYPE_MAX_LEN];
    getTlv_t getTlvFromExt;
    uint8_t supportedExtId[QC_MAX_SUPPORTED_EXTENSIONS][QC_SUPPORTED_EXTENSION_ID_MAX_LEN];
  } authenticators[QSAPP_OPTIONAL_PARTNERTS_MAX];
  /* SFS location */
  char sfsRoot[64];
} config_arg_t;

typedef struct {
  config_arg_t arg;
} qsapp_config_t;


