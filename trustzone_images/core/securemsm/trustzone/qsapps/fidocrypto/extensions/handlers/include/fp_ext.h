/*
* Copyright (c) 2015 Qualcomm Technologies, Inc.  All Rights Reserved.
* Qualcomm Technologies Proprietary and Confidential.
*/

#include "ext_handler.h"

DECLARE_EXT_GET_TLV_FUNC(fp_getTlv);
