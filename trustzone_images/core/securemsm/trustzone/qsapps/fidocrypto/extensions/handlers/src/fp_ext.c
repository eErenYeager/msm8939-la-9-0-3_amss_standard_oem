/*
* Copyright (c) 2015 Qualcomm Technologies, Inc.  All Rights Reserved.
* Qualcomm Technologies Proprietary and Confidential.
*/

#include "ext_handler.h"
#include <sselog.h>
#include "tlv.h"
#include <qsee_hash.h>
#include <qsee_heap.h>
#include <IxErrno.h>
#include "fidot_ext.h"
#include <stdbool.h>
#include "stringl.h"

static int checkExt(uint8_t const * const data, uint32_t const len, uint16_t **T, uint16_t **L, uint8_t **V)
{
  int rv = 0;
  do {
    if (!data || (len < 4)) {
      LOG_E("Invalid data");
      rv = -E_INVALID_ARG;
      break;
    }
    if (!T || !L || !V) {
      LOG_E("Invalid T/L/V pointers");
      rv = -E_INVALID_ARG;
      break;
    }
    *T = (uint16_t*)data;
    *L = *T + 1;
    if (len < 2 + 2 + **L) {
      LOG_E("Invalid length: %u vs %u min", len, 2+2+**L);
      rv = -E_INVALID_ARG;
      break;
    }
    if (**L != 0) {
      *V = (uint8_t*)(*L + 1);
    } else {
      *V = 0;
    }
  } while (0);
  return rv;
}

DECLARE_EXT_GET_TLV_FUNC(handle_fp_extension)
{
  int rv = 0;
  uint16_t *T = NULL;
  uint16_t *L = NULL;
  uint8_t *V = NULL;
  uint8_t *tlv = NULL;
  qsee_hash_ctx *hash_ctx = NULL;
  uint8_t const * next = ext;
  size_t nextLen = extLen;
  DEC_TLV();
  ENTER;
  do {

    if (!ext || (extLen == 0)  || !len) {
      LOG_E("Null/invalid input");
      rv = -E_INVALID_ARG;
      break;
    }
    DUMPHEX("ExtData", ext, extLen);

    // now we support only one, easy task
    rv = checkExt(next, nextLen, &T, &L, &V);
    if (rv) {
      LOG_E("Error getting TLV");
      break;
    }

    if ((*T == AFI_RAW_USER_VERIFICATION_INDEX) || (*T == AFI_RAW_USER_VERIFICATION_STATE)) {

      if ((*L > 32) || (*L == 0)) {
        LOG_E("Invalid tag length for tag %04X: %u", *T, *L);
        rv = -E_INVALID_ARG;
        break;
      }

      // we're good to go, proceed with composition
      rv = qsee_hash_init(QSEE_HASH_SHA256,&hash_ctx);
      if (rv) {
        LOG_E("Error in qsee_hash_init: %d", rv);
        break;
      }
      // feed the keyId
      rv = qsee_hash_update(hash_ctx, *keyId, sizeof(*keyId));
      if (rv) {
        LOG_E("Error in qsee_hash_update on keyId: %d", rv);
        break;
      }
      // feed the raw value
      rv = qsee_hash_update(hash_ctx, V, *L);
      if (rv) {
        LOG_E("Error in qsee_hash_update on raw value: %d", rv);
        break;
      }

      *len = 2 + 2 + QSEE_SHA256_HASH_SZ;
      tlv = qsee_malloc(*len);
      if (tlv == NULL) {
        LOG_E("Failed to allocate TLV (%u bytes)", *len);
        rv = -E_NO_MEMORY;
        break;
      }
      // moving pointer for TLV composition
      rv = -E_FAILURE;
      SET_TLV(tlv, *len);
      if (*T == AFI_RAW_USER_VERIFICATION_INDEX) {
        ADD_2(AFI_USER_VERIFICATION_INDEX);
      } else {
        ADD_2(AFI_USER_VERIFICATION_STATE);
      }
      ADD_2(QSEE_SHA256_HASH_SZ);
      // write the hash result
      CHECK_LEN(QSEE_SHA256_HASH_SZ);
      rv = qsee_hash_final(hash_ctx, TLV_PTR, QSEE_SHA256_HASH_SZ);
      if (rv) {
        LOG_E("Error in qsee_hash_final: %d", rv);
        break;
      }
      MOVE_PTR(QSEE_SHA256_HASH_SZ);

      // verify length was set appropriately... otherwise we might have just corrupted the heap! :)
      if (TLV_PTR - tlv != *len) {
        LOG_E("Error composing TLV, size mismatch: %u vs %u expected", TLV_PTR - tlv, *len);
        rv = -E_FAILURE;
        break;
      }
      rv = 0;
      if (*T == AFI_RAW_USER_VERIFICATION_INDEX) {
        DUMPHEX("AFI_USER_VERIFICATION_INDEX", tlv, *len);
      } else {
        DUMPHEX("AFI_USER_VERIFICATION_STATE", tlv, *len);
      }

    } else if (*T == TA_AUTHENTICATOR_VERSION) {
      if (*L != 1) {
        LOG_E("Invalid tag length for tag %04X: %u", *T, *L);
        rv = -E_INVALID_ARG;
        break;
      }
      if (version) {
        *version = *V;
        LOG_D("Version from TA: %u", *version);
      }
    }
  } while (0);
  if (rv && tlv) {
    qsee_free(tlv);
    tlv = NULL;
  }
  if (hash_ctx) {
    qsee_hash_free_ctx(hash_ctx);
  }
  EXITV(tlv);
}

uint8_t const * getNextTag(uint8_t const * const ext, uint32_t const extLen, uint32_t * const nextLen)
{
  int rv = 0;
  uint16_t *T = NULL;
  uint16_t *L = NULL;
  uint8_t *V = NULL;
  uint8_t *next = NULL;
  ENTER;
  do {
    if (!ext || (extLen == 0)  || !nextLen) {
      LOG_E("Null/invalid input");
      break;
    }
    //DUMPHEX("ExtData", ext, extLen);
    rv = checkExt(ext, extLen, &T, &L, &V);
    if (rv) {
      LOG_E("Error getting TLV");
      break;
    }
    // safe, since checkExt does check the length
    *nextLen = extLen - 4 - *L;
    if (*nextLen > 0) {
      next = (uint8_t*)T + 2 + 2 + *L;
    }
  } while (0);
  EXITV(next);
}

DECLARE_EXT_GET_TLV_FUNC(fp_getTlv)
{
  int rv = 0;
  uint8_t *tlv = NULL;
  uint8_t const * next = ext;
  uint32_t nextLen = 0;
  uint8_t *tag = NULL;
  uint32_t tagLen = 0;
  uint8_t *tlvTmp = NULL;
  ENTER;
  do {
    if (!ext || (extLen == 0)  || !len) {
      LOG_E("Null/invalid input");
      break;
    }

    *len = 0;
    nextLen = extLen;
    do {
      tag = handle_fp_extension(next, nextLen, keyId, &tagLen, version);
      if (tag) {
        if (*len + tagLen < *len) {
          // overflow, we cannot add this or any more tag
          LOG_E("Overflow while addind an extension TAG: %u + %u", *len, tagLen);
          rv = -E_FAILURE;
          break;
        }
        // append to tlv
        tlvTmp = tlv;
        tlv = qsee_malloc(*len + tagLen);
        if (!tlv) {
          rv = -E_NO_MEMORY;
          break;
        }
        if (tlvTmp) {
          memscpy(tlv, *len + tagLen, tlvTmp, *len);
          memscpy(tlv + *len, tagLen, tag, tagLen);
        } else {
          memscpy(tlv, tagLen, tag, tagLen);
        }
        *len += tagLen;
        qsee_free(tlvTmp);
        tlvTmp = NULL;
        qsee_free(tag);
        tag = NULL;
      }
      next = getNextTag(next, nextLen, &nextLen);
    } while (next != NULL);

  } while (0);
  if (rv) {
    if (tlvTmp) {
      qsee_free(tlvTmp);
    }
    if (tlv) {
      qsee_free(tlv);
    }
    if (tag) {
      qsee_free(tag);
    }
  }
  EXITV(tlv);
}
