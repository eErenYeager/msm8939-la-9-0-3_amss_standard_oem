/*
* Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
* Qualcomm Technologies Proprietary and Confidential.
*/

#include <stddef.h>
#include <stdint.h>

/** Macros to deal with TLV composition
 * */
#define DEC_TLV() uint8_t *_u8p_ = NULL; uint8_t *_ep_ = NULL
#define _PTR_ (_u8p_)
#define _EPTR_ (_ep_)
#define TLV_PTR (_PTR_)
#define CHECK_LEN(l) if ((_PTR_ > _EPTR_) || (_EPTR_ - _PTR_ < (l))) break
#define SET_TLV(x, len) _PTR_ = (x); _EPTR_ = (x) + len; CHECK_LEN(len)
#define ADD_1(x) CHECK_LEN(1); writeOne(&_PTR_, (x))
#define ADD_2(x) CHECK_LEN(2); writeTwo(&_PTR_, (x))
#define ADD_4(x) CHECK_LEN(4); writeFour(&_PTR_, (x))
#define ADD_B(x,l,b) CHECK_LEN(l); writeBin(&_PTR_, (x), (l), (b))
#define MOVE_PTR(l) CHECK_LEN(l); _PTR_ += (l)

uint8_t* writeBin(uint8_t** location, void* data, uint32_t len, uint32_t endianSwap);

uint8_t* writeOne(uint8_t** location, uint8_t value);
uint8_t* writeTwo(uint8_t** location, uint16_t value);
uint8_t* writeFour(uint8_t** location, uint32_t value);
