#===============================================================================
#
# App Core
#
# GENERAL DESCRIPTION
#    build script
#
#
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsapps/fidocrypto/app/build/SConscript#1 $
#  $DateTime: 2018/02/07 00:37:16 $
#  $Author: mplp4svc $
#  $Change: 15409075 $
#                      EDIT HISTORY FOR FILE
#
#  This section contains schedulerents describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#===============================================================================
Import('env')
env = env.Clone()

#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------
SRCPATH = "${BUILD_ROOT}/core/securemsm/trustzone/qsapps/fidocrypto/app/src"

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

#-------------------------------------------------------------------------------
# Compiler, object, and linker definitions
#-------------------------------------------------------------------------------

# do not generate thumb code for inline assembler code
#env.Append(ARMCC_OPT = ' --arm')

env.Append(CPPPATH = "${COREBSP_ROOT}/securemsm/trustzone/qsapps/fidocrypto/app/include")
env.Append(CPPPATH = "${COREBSP_ROOT}/securemsm/trustzone/qsapps/fidocrypto/app/fido")
env.Append(CPPPATH = "${COREBSP_ROOT}/securemsm/trustzone/qsapps/fidocrypto/authentication/include")
env.Append(CPPPATH = "${COREBSP_ROOT}/securemsm/trustzone/qsapps/fidocrypto/so/include")
env.Append(CPPPATH = "${COREBSP_ROOT}/securemsm/trustzone/qsapps/fidocrypto/tlv/include")
env.Append(CPPPATH = "${COREBSP_ROOT}/securemsm/trustzone/qsapps/fidocrypto/extensions/include")
env.Append(CPPPATH = "${COREBSP_ROOT}/securemsm/trustzone/qsapps/fidocrypto/app/src/private")
env.Append(CPPPATH = "${COREBSP_ROOT}/securemsm/trustzone/qsee")
env.Append(CPPPATH = "${COREBSP_ROOT}/securemsm/trustzone/qsapps/fidocrypto/config/include")

env.Append(CPPDEFINES = '-D_AEABI_PORTABILITY_LEVEL=1')
env.Append(CPPDEFINES = '-D_AEABI_LC_CTYPE=C')

INC_PATH_API = ["${INC_ROOT}/core/api/kernel/libstd/stringl"]
env.Append(CPPPATH = INC_PATH_API)

#----------------------------------------------------------------------------
# App core Objects
#----------------------------------------------------------------------------
APP_CORE_ENTRY_SOURCES = [
        '${BUILDPATH}/app_main.c',

        '${BUILDPATH}/functions/01_Initialize.c',
        '${BUILDPATH}/functions/02_GetInfo.c',
        '${BUILDPATH}/functions/03_Register.c',
        '${BUILDPATH}/functions/04_Sign.c',
        '${BUILDPATH}/functions/05_SetChallenge.c',
        '${BUILDPATH}/functions/99_Provision.c',

        '${BUILDPATH}/asn1.c',
        '${BUILDPATH}/asn1_fido.c',
        '${BUILDPATH}/functions.c',
        '${BUILDPATH}/fcUtils.c',
        '${BUILDPATH}/fcStorage.c',
        '${BUILDPATH}/tlv_fido.c',
]

HEADERS = [
        '${BUILDPATH}/private/asn1.h',
        '${BUILDPATH}/private/asn1_fido.h',
        '${BUILDPATH}/private/fcStorage.h',
        '${BUILDPATH}/private/fcUtils.h',
        '${BUILDPATH}/private/glob.h',
        '${BUILDPATH}/private/tlv_fido.h',
]

#-------------------------------------------------------------------------------
# Add Libraries to image
#-------------------------------------------------------------------------------
env.AddBinaryLibrary('FIDOCRYPTO_IMAGE', '${BUILDPATH}/fidocrypto_app', APP_CORE_ENTRY_SOURCES)

#-------------------------------------------------------------------------------
# Pack out files
#-------------------------------------------------------------------------------
env.CleanPack('FIDOCRYPTO_IMAGE', HEADERS)

#-------------------------------------------------------------------------------
# Load sub scripts
#-------------------------------------------------------------------------------
env.LoadSoftwareUnits()
