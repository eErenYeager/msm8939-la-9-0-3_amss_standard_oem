/*
 * Copyright (c) 2015 Qualcomm Technologies, Inc. All Rights Reserved.
 * Qualcomm Technologies Proprietary and Confidential.
*/

#pragma once

/** Definitions for FIDO extensions
 */

/** User Verification Index
 *
 * Extension included in UVT:
 * TAG    (2): AFI_RAW_USER_VERIFICATION_INDEX
 * LEN    (2): n <= 32
 * DATA   (n): raw user verification index
 *
 * Extension included in TLV payload (Register / Sign)
 * TAG    (2): AFI_USER_VERIFICATION_INDEX
 * LEN    (2): 32
 * DATA  (32): SHA256(keyID | raw index)
 * */
#define AFI_RAW_USER_VERIFICATION_INDEX   0x0103
#define AFI_USER_VERIFICATION_INDEX       0x0104

/** User Verification State
 *
 * Extension included in UVT:
 * TAG    (2): AFI_RAW_USER_VERIFICATION_STATE
 * LEN    (2): n <= 32
 * DATA   (n): raw user verification state
 *
 * Extension included in TLV payload (Register / Sign)
 * TAG    (2): AFI_USER_VERIFICATION_STATE
 * LEN    (2): 32
 * DATA  (32): SHA256(keyID | raw state)
 * */
#define AFI_RAW_USER_VERIFICATION_STATE   0x0105
#define AFI_USER_VERIFICATION_STATE       0x0106


/** Authenticator (TA) version
 *
 * Extension included in UVT:
 * TAG    (2): TA_AUTHENTICATOR_VERSION
 * LEN    (2): n = 1
 * DATA   (1): single byte version
 *
 * Data from the extension will be merged as the MSB
 * of the authenticator version in TAG_ASSERTION_INFO
 * */
#define TA_AUTHENTICATOR_VERSION          0x0181
