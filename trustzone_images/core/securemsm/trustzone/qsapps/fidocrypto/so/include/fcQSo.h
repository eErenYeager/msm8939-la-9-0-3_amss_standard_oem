/*
* Copyright (c) 2012-2013 Qualcomm Technologies, Inc.
* All Rights Reserved.
* Confidential and Proprietary - Qualcomm Technologies, Inc.
*/

#pragma once

#include <stdint.h>

int QWrapObject(
  void const * const src,
  uint32_t plainLen,
  uint32_t encryptedLen,
  void *dest,
  __packed size_t* destLen,
  uint8_t const * const key,
  size_t const keyLen);

int QUnwrapObject(
  void *src,
  size_t srcLen,
  void *dest,
  size_t *destLen,
  uint8_t const * const key,
  size_t const keyLen);
