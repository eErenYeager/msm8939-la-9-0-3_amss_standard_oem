/* =================================================================================
 *  Copyright (c) 2015 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
 * =================================================================================
 */

/*
 * Included Files
 */
#include "libstd/stringl/stringl.h"
#include <comdef.h>
int
km_query_rpmb_enablement(boolean* b);


/* Customize the fingerprint TA name. Default value is defined, if it needs to be changed then the 
 * default name assigned shall be modified as per the need. 
 */
int32_t get_fpta_name(char *fpta_name_str, uint32_t fpta_name_len)
{
   const char* default_fpta_name = "fpctzappfingerprint";

   if (fpta_name_str && (strlen(default_fpta_name) <= fpta_name_len))
   {
      strlcpy(fpta_name_str, default_fpta_name, fpta_name_len);
      return 0;
   }
   else
   {
	   return -1;
   }
}


/* For M-OTA WITHOUT Verified Boot/Partitions changes, this value shall be defined to 1 otherwise 0.
 * By default this value is defined to 1 here in this case. And this can be customized based on the needs. 
 *
 * This value is not allowed to change dynamically during runtime. 
 */
boolean get_disable_rot_status(void)
{
  const boolean customize_disable_rot = 1;
  return customize_disable_rot;
}

/* By default RPMB_STATUS_HARDCODE is defined to 0 to read the status dynamically from QSEE. 
 * OEM's can customize to set this macro to 1 to hardcode the status of RPMB to disable based on the need. 
*/
int get_rpmb_enablement(boolean *b)
{	
#define RPMB_STATUS_HARDCODE 0				
#if RPMB_STATUS_HARDCODE
   /* Sanity Check on argument */
  if(b == NULL)
  {
    /* Return error */
    return -1;
  }
  *b = 0;				
	return 0; //returning SUCCESS.
#else	
  return km_query_rpmb_enablement(b);
#endif	
}

/* OEM specific requirement for GK Retry Timeout, default is 0.
 */
uint32 get_gk_timeout_config(void)
{
  const uint32 gatekeeper_timeout_config = 0;
  return gatekeeper_timeout_config;
}
/* 
      0 - default internal partition ID, 1 - will switch to the old partition ID. 
*/
boolean get_rpmb_partition_id(void)
{
  const boolean customize_rpmb_partition_id = 0;
  return customize_rpmb_partition_id;
}


