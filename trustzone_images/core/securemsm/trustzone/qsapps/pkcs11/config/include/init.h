/*
* Copyright (c) 2013 Qualcomm Technologies, Inc.  All Rights Reserved.
* Qualcomm Technologies Proprietary and Confidential.
*/

#pragma once

#include <stdint.h>

#define TL_CONFIG_SIZE 0x400 /* 1024 bytes */

#define TL_MAGIC_0  0x43524351  // "QCRD"
#define TL_MAGIC_1  0x51435244  // "DRCQ"
#define TL_CKF_ENCRYPT            0x00000100
#define TL_CKF_DECRYPT            0x00000200
#define TL_CKF_DIGEST             0x00000400
#define TL_CKF_SIGN               0x00000800
#define TL_CKF_SIGN_RECOVER       0x00001000
#define TL_CKF_VERIFY             0x00002000
#define TL_CKF_VERIFY_RECOVER     0x00004000
#define TL_CKF_GENERATE           0x00008000
#define TL_CKF_GENERATE_KEY_PAIR  0x00010000
#define TL_CKF_WRAP               0x00020000
#define TL_CKF_UNWRAP             0x00040000
#define TL_CKF_DERIVE             0x00080000

#define TL_SE_STICKY_ATTRIBUTES   0x00000001
#define TL_SE_CONFLICTS_CHECK     0x00000002
#define TL_SE_SECURE_TEMPLATES    0x00000004
#define TL_SE_ITER_PBKDF2_SK      0x00000008
#define TL_SE_WEAKER_KEY          0x00000010
#define TL_SE_SECURE_MECH         0x00000020
#define TL_SE_SECURE_WRAP_ONLY    0x00000040
#define TL_SE_TRUSTED_WRAP_ONLY   0x00000080
#define TL_SE_ITER_PBKDF1_VPN     0x00000100

#define TL_CONFIG_VERSION_MAJOR 1
#define TL_CONFIG_VERSION_MINOR 2
#define TL_HW_VERSION_MAJOR 1
#define TL_HW_VERSION_MINOR 0
#define TL_FW_VERSION_MAJOR 1
#define TL_FW_VERSION_MINOR 0
#define TL_ENABLED_FUNCTIONS (\
  TL_CKF_ENCRYPT|TL_CKF_DECRYPT| \
  TL_CKF_DIGEST| \
  TL_CKF_SIGN|TL_CKF_SIGN_RECOVER| \
  TL_CKF_VERIFY|TL_CKF_VERIFY_RECOVER| \
  TL_CKF_GENERATE|TL_CKF_GENERATE_KEY_PAIR| \
  TL_CKF_WRAP|TL_CKF_UNWRAP| \
  TL_CKF_DERIVE)

#define TL_SECURITY_EXTENSIONS (TL_SE_STICKY_ATTRIBUTES| \
  TL_SE_CONFLICTS_CHECK| \
  TL_SE_SECURE_TEMPLATES| \
  TL_SE_ITER_PBKDF2_SK| \
  TL_SE_WEAKER_KEY| \
  TL_SE_SECURE_MECH| \
  TL_SE_SECURE_WRAP_ONLY| \
  TL_SE_TRUSTED_WRAP_ONLY)

#define TL_MIN_ITER_PBKDF2_SK  10000
#define TL_MIN_ITER_PBKDF1_VPN 1024
#define TL_MAX_ITER_PBKDF1     100000000
#define TL_MAX_ITER_PBKDF2     100000000
#define TL_TOKEN_PIN_ITER      1000

typedef struct {
  unsigned char   label[32];           /* blank padded */
  unsigned char   manufacturerID[32];  /* blank padded */
  unsigned char   model[16];           /* blank padded */
  unsigned char   serialNumber[16];    /* blank padded */
  uint32_t      flags;               /* see below */

  uint32_t      ulMaxSessionCount;     /* max open sessions */
  uint32_t      ulMaxRwSessionCount;   /* max R/W sessions */
  uint32_t      ulMaxPinLen;           /* in bytes */
  uint32_t      ulMinPinLen;           /* in bytes */

  uint8_t       hardwareVersion_major;  /* integer portion of version number */
  uint8_t       hardwareVersion_minor;  /* 1/100ths portion of version number */

  uint8_t       firmwareVersion_major;  /* integer portion of version number */
  uint8_t       firmwareVersion_minor;  /* 1/100ths portion of version number */

  uint32_t maxAuthSpan;
  uint32_t maxUsageNum;
  uint32_t maxLoginAttempts;
  uint32_t maxTokenNumber;
  uint32_t maxSessionGroupNumber;
  /* behaviour on lock */
  uint32_t unlockTimeStep; /* in ms */
  /* cryptographic behaviour */
  uint32_t enabledFunctions;
  uint32_t securityExtensions;
  uint32_t minIterationsPBKDF2_SK;
  uint32_t minIterationsPBKDF1_VPN;
  uint32_t maxIterationsPBKDF1;
  uint32_t maxIterationsPBKDF2;
  /* token keys pin key derivation */
  uint32_t tokenPinIterations;
  /* Secure UI */
  uint32_t forceRandomizedKeypad;
  uint32_t forceSecureKeyboard;
  uint32_t secureInputTimeout;
  unsigned char sui_initToken_title[64];
  unsigned char sui_initToken_confirm_title[64];
  unsigned char sui_setPIN_currentTitle[64];
  unsigned char sui_setPIN_nextTitle[64];
  unsigned char sui_setPIN_confirmTitle[64];
  unsigned char sui_login_title[64];
  unsigned char sui_generateKey_secretKey[64];
  unsigned char sui_generateKey_pbkd1[64];
  unsigned char sui_generateKey_pbkd2[64];
  /* SFS location */
  unsigned char sfsRoot[64];
} tl_config_arg_t;

typedef struct {
  uint32_t       magic0;
  uint16_t       major;  /* integer portion of version number */
  uint16_t       minor;  /* 1/100ths portion of version number */
  tl_config_arg_t arg;
  uint8_t reserved[TL_CONFIG_SIZE - 12 - sizeof(tl_config_arg_t)];
  uint32_t magic1;
} tl_config_t;
