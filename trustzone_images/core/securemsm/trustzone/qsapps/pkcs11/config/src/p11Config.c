/*
* Copyright (c) 2012 Qualcomm Technologies, Inc.
* All Rights Reserved.
* Confidential and Proprietary - Qualcomm Technologies, Inc.
*/

#include "init.h"
#include "p11Lengths.h"

#define TL_LABEL {'T','O',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
#define TL_MANUFACTURER {'Q','U','A','L','C','O','M','M',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
#define TL_MODEL {'S','S','E',0,0,0,0,0,0,0,0,0,0,0,0,0}
#define TL_SERIAL {'C','5','D','F',0,0,0,0,0,0,0,0,0,0,0,0}
#define TL_SFS_ROOT "/persist/data/pkcs11/"
#define TL_SUI_INIT_TOKEN_TITLE "INITIALIZING TOKEN"
#define TL_SUI_INIT_TOKEN_CONFIRM_TITLE "CONFIRM PIN FOR TOKEN"
#define TL_SUI_SET_PIN_CURRENT_TITLE "ENTER CURRENT PIN"
#define TL_SUI_SET_PIN_NEXT_TITLE "SET NEW PIN"
#define TL_SUI_SET_PIN_CONFIRM_TITLE "CONFIRM NEW PIN"
#define TL_SUI_LOGIN_TITLE "LOGIN TO"
#define TL_SUI_GENERATE_KEY_SECRET_KEY_TITLE "VPN Certificate Enrollment"
#define TL_SUI_GENERATE_KEY_PBKD1_TITLE "VPN Certificate Enrollment"
#define TL_SUI_GENERATE_KEY_PBKD2_TITLE "Generate Key from Password"

#define TL_FLAGS (0x00000001 | 0x00000004) // Secure UI disabled
//#define TL_FLAGS (0x00000001 | 0x00000004 | 0x00000100) // Secure UI enabled


/*lint -esym(506,g_config) */
tl_config_t __attribute__((section("zconfig"))) g_config = {
  TL_MAGIC_0,
  TL_CONFIG_VERSION_MAJOR,
  TL_CONFIG_VERSION_MINOR,
  {
    TL_LABEL,
      TL_MANUFACTURER,
      TL_MODEL,
      TL_SERIAL,
      TL_FLAGS,
      TOKEN_MAX_SESSION_COUNT,
      TOKEN_MAX_SESSION_COUNT,
      TOKEN_PIN_LEN,
      TOKEN_MIN_PIN_LEN,
      TL_HW_VERSION_MAJOR,
      TL_HW_VERSION_MINOR,
      TL_FW_VERSION_MAJOR,
      TL_FW_VERSION_MINOR,
      0, /* maxAuthSpan */
      0, /* maxUsageNum */
      0, /* maxLoginAttempts */
      20, /* maxTokenNumber */
      20, /* maxSessionGroupNumber */
      0, /* unlockTimeStep in ms */
      TL_ENABLED_FUNCTIONS,
      0, /* TL_SECURITY_EXTENSIONS, */
      TL_MIN_ITER_PBKDF2_SK,
      TL_MIN_ITER_PBKDF1_VPN,
      TL_MAX_ITER_PBKDF1,
      TL_MAX_ITER_PBKDF2,
      TL_TOKEN_PIN_ITER,
      /* SUI */
      1, /* forceRandomizedKaypad */
      0, /* forceSecureKeyboard */
      20000, /* secureInputTimeout */
      TL_SUI_INIT_TOKEN_TITLE,
      TL_SUI_INIT_TOKEN_CONFIRM_TITLE,
      TL_SUI_SET_PIN_CURRENT_TITLE,
      TL_SUI_SET_PIN_NEXT_TITLE,
      TL_SUI_SET_PIN_CONFIRM_TITLE,
      TL_SUI_LOGIN_TITLE,
      TL_SUI_GENERATE_KEY_SECRET_KEY_TITLE,
      TL_SUI_GENERATE_KEY_PBKD1_TITLE,
      TL_SUI_GENERATE_KEY_PBKD2_TITLE,
      TL_SFS_ROOT, /* sfsRoot */
  },
  {0}
};


