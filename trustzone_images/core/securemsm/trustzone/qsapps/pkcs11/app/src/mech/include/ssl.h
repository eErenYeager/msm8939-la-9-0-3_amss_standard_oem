/*
 * Copyright (c) 2012-2013 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#pragma once

#include "p11Mechanisms.h"

DECLARE_GENERATE_KEY_FUNC(generate_pre_master_key);
DECLARE_DERIVE_FUNC(derive_master_key);
