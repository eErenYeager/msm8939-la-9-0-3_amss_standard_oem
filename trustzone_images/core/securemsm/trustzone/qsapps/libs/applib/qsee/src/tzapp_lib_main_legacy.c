
/////////////////////////////////////////////////////////
// main image entry point
/////////////////////////////////////////////////////////

#include <stdlib.h>
#include "qsee_api.h"
#include "qsee_heap.h"
#include "qsee_version.h"
#include "qsee_log.h"
#include "syscall_wrapper.h"
#include "syscall.h"
#include "memheap_lite.h"
#include "qsee_message.h"
#include "mink_services.h"
#include "mink_object.h"
#include "server.h"

extern char Image$$TZ_APP_STACK$$ZI$$Base;
extern unsigned int Image$$TZ_APP_STACK$$ZI$$Limit;

extern char Image$$TZ_APP_HEAP$$ZI$$Base;
extern unsigned int Image$$TZ_APP_HEAP$$ZI$$Limit;

extern void __cpp_initialize__aeabi_(void);
extern char TZ_APP_NAME[];

extern void qsee_dcache_flush_region(void *addr, uint32  len);
extern void qsee_dcache_inval_region(void *addr, unsigned int length);

extern void tz_app_init(void);


/*Needed for stack protection.  Set initial value:*/
unsigned int __stack_chk_guard[] = {0xDEADDEAD};

void qsee_err_fatal(void)
{
  qsee_generic_syscall(SYSNUM(qsee_app_fatal_error), 0,0,0,0,0,0);
}

void platform_err_fatal(void)
{
  qsee_err_fatal();
}

uint32 platform_get_random(uint8* buf, uint32 size)
{
	return qsee_prng_getdata(buf, size);
}

void
__stack_chk_fail(void)
{
  qsee_err_fatal();
}

/*There may be a compiler bug because of which 
 *if there is no const string that needs to be
 *patched as part of __cpp_initialize__aeabi_ 
 *init array still have one entry which is invalid 
 *which causes issue. so force create at leave one 
 *valid entry with this dummy variable. 
 */
const char *dummy_str[] =
{
   "A",
};

apps_request_resp_info_t apps_request_resp;

void tzlib_app_entry(void);

/*Needs to be overridden. called by rvct*/
char *
_sys_command_string(char *cmd, int len)
{
  return "[stub]";
}

unsigned int get_stack_base(void)
{
  return (unsigned int)&Image$$TZ_APP_STACK$$ZI$$Base;
}
unsigned int get_stack_size(void)
{
  //return (unsigned int)&Image$$TZ_APP_STACK$$ZI$$Length;
  //return TZ_APP_STACK_SIZE;
  return (unsigned int)((unsigned int)&Image$$TZ_APP_STACK$$ZI$$Limit - (unsigned int)&Image$$TZ_APP_STACK$$ZI$$Base);
}

int qsee_app_initial_invoke(ObjectCxt ptr, ObjectOp op, ObjectArg *args, ObjectCounts k);

#define MAX_OBJS 0x5
#define ACCEPT_BUF_SIZE 0x100
char acceptBuf[0x100];
unsigned int get_app_maxObjs(void)
{
  return MAX_OBJS;
}

unsigned int get_app_acceptBufSize(void)
{
  return ACCEPT_BUF_SIZE;
}

char* get_app_acceptBuf(void)
{
  return acceptBuf;
}

const char* get_ta_app_name(void)
{
  return TZ_APP_NAME;
}

void* get_app_entry(void)
{
  return qsee_app_initial_invoke;
}

#if 1
int serverLoop(void *cxt);   
void* get_app_serverloop_entry(void)
{
  return serverLoop;
}


// Also defined in MINK code:
typedef struct AcceptHeader AcceptHeader;
struct AcceptHeader {
   Object server;
   Object obj;
   ObjectOp op;
   ObjectArg *args;
   ObjectCounts k;
   int result;
};

typedef struct KThreadSyscallResult KThreadSyscallResult;
struct KThreadSyscallResult {
   int32_t result;
   uint32_t aux;
};

extern KThreadSyscallResult mink_generic_syscall(uint32 num, intptr_t r0, intptr_t r1, intptr_t r2, intptr_t r3);
void server_exit(int ret)
{
  mink_generic_syscall(SYSNUM(mink_server_exit), ret, 0, 0, 0);
}

/* Accept and dispatch inbound invocations.
   This function is used as the entry point of user processes. */
int serverLoop(void *cxt)
{
   AcceptHeader *hdr = (AcceptHeader *) cxt;
   Object server = hdr->server;

   while (1) {
      int err = server_accept(server);
      if (! Object_isOK(err)) {
         break;
      }
      hdr->result = Object_invoke(hdr->obj, hdr->op, hdr->args, hdr->k);
   }
   
   server_exit(0);
   
   return 0;
}
#endif

unsigned int* get_app_req_resp(void)
{
  return (unsigned int *)&apps_request_resp;
}

extern void tz_app_cmd_handler(void* req, unsigned int reqlen, void* rsp, unsigned int rsplen);
extern void tz_app_shutdown(void);

extern void qsee_wait_syscall(void);
extern void qsee_wait_default_signal_syscall(void);

void wait_for_next_request(void)
{
  /*Wait for the default event (request from HLOS) */
  qsee_wait_default_signal_syscall();
    
  switch (apps_request_resp.command) {
    case QSEE_APP_SHUTDOWN_COMMAND:
      tz_app_shutdown();
      break;
    default:
      qsee_dcache_inval_region(apps_request_resp.req, apps_request_resp.reqlen);
      qsee_dcache_inval_region(apps_request_resp.rsp, apps_request_resp.rsplen);
      tz_app_cmd_handler(apps_request_resp.req, apps_request_resp.reqlen, apps_request_resp.rsp, apps_request_resp.rsplen);
      qsee_dcache_flush_region(apps_request_resp.req, apps_request_resp.reqlen );
      qsee_dcache_flush_region(apps_request_resp.rsp, apps_request_resp.rsplen );
      break;
  }
}

unsigned int qsee_read_register(unsigned int addr)
{
  return qsee_generic_syscall(SYSNUM(qsee_read_register), addr,0,0,0,0,0);
}

Object mink_api_service_obj;
Object mink_user_sscall_service_obj;

int qsee_request_service(unsigned int listener_id, void *req, unsigned int req_len, void *rsp, unsigned int rsplen)
{
  ObjectArg a[3];

  a[0].b.ptr = &listener_id;
  a[0].b.len = sizeof(uint32);
  a[1].b.ptr = req;
  a[1].b.len = req_len;
  a[2].b.ptr = rsp;
  a[2].b.len = rsplen;  
  return Object_invokeABCD(mink_api_service_obj, MINK_CMD_REQ_SERV, a, 2, 1, 0, 0);
}

void qsee_wait(void)
{
  qsee_generic_syscall(SYSNUM(qsee_wait_hlos), 0,0,0,0,0,0);
}

int qsee_register_shared_buffer(void *address, unsigned int size)
{
  return qsee_generic_syscall(SYSNUM(qsee_reg_sb), (unsigned int)address, size, 0,0,0,0);
}
int qsee_deregister_shared_buffer(void *address)
{
  return qsee_generic_syscall(SYSNUM(qsee_dereg_sb), (unsigned int)address, 0,0,0,0,0);
}
int qsee_prepare_shared_buf_for_nosecure_read(void * address, unsigned int size)
{
  return qsee_generic_syscall(SYSNUM(qsee_prep_sb_for_ns_read), (unsigned int)address, size, 0,0,0,0);
}
int qsee_prepare_shared_buf_for_secure_read(void *address, unsigned int size)
{
  return qsee_generic_syscall(SYSNUM(qsee_prep_sb_for_s_read), (unsigned int)address, size, 0, 0,0,0);
}

void qsee_set_global_flag(unsigned int flag)
{
  qsee_generic_syscall(SYSNUM(qsee_set_global_flag), flag, 0,0,0,0,0);
}
unsigned int qsee_get_global_flag(void)
{
  return qsee_generic_syscall(SYSNUM(qsee_get_global_flag), 0,0,0,0,0,0);
}
unsigned int qsee_oem_process_cmd(unsigned char *input, unsigned int input_len, unsigned char *output, unsigned int output_len)
{
  return qsee_generic_syscall(SYSNUM(qsee_oem_process_cmd), (unsigned int)input, input_len, (unsigned int)output, output_len, 0,0);
}

//QSEE message passing:
int qsee_encapsulate_inter_app_message(char* dest_app_name, uint8* msg_buf, uint32 msg_len, uint8* out_buf, uint32* out_len)
{
  return qsee_generic_syscall(SYSNUM(qsee_encapsulate_inter_app_message), (unsigned int)dest_app_name, (unsigned int)msg_buf, (unsigned int)msg_len, (unsigned int)out_buf, (unsigned int)out_len, 0);
}
int qsee_decapsulate_inter_app_message(char* source_app_name, uint8* msg_buf, uint32 msg_len, uint8* out_buf, uint32* out_len)
{
  return qsee_generic_syscall(SYSNUM(qsee_decapsulate_inter_app_message), (unsigned int)source_app_name, (unsigned int)msg_buf, (unsigned int)msg_len, (unsigned int)out_buf, (unsigned int)out_len, 0);
}

int qsee_enforce_hdmi_hdcp_encryption(unsigned int enforce)
{
  return qsee_generic_syscall(SYSNUM(qsee_enforce_hdmi_hdcp_encryption), (unsigned int)enforce, 0, 0, 0, 0, 0);
}

int qsee_get_secure_state(qsee_secctrl_secure_status_t* status)
{
  return qsee_generic_syscall(SYSNUM(qsee_get_secure_state), (unsigned int)status,0,0,0,0,0);
}

//DALHeap application_heap;
mem_heap_type application_heap;
/*Memheap magic number arrays which needs to be passed in to all the memheap functions*/
static uint32 magic_num[MAX_HEAP_INIT] = {(uint32)-1,(uint32)-1,(uint32)-1,(uint32)-1,(uint32)-1};
static uint16 magic_num_index_array[MAX_HEAP_INIT] = {0,1,2,3,4};
static mem_magic_number_struct mem_magic_number;

void* qsee_malloc(size_t size)
{
  return mem_malloc(&application_heap, &mem_magic_number,size);
}

void qsee_free(void *ptr)
{
  mem_free(&application_heap,&mem_magic_number, ptr);
}


static int app_cmd_invoke(ObjectCxt ptr, ObjectOp op, ObjectArg *args, ObjectCounts k)
{
  typedef struct mink_app_cmd_comm_s {
	void* req;
	uint32_t reqlen;
	void* rsp;
	uint32_t rsplen;
  } mink_app_cmd_comm_t;
  
  mink_app_cmd_comm_t *comm;
  
  if ((op == Object_OP_retain) || (op == Object_OP_release)) {
    return 0 ;
  }

  switch (op) {
    case QSEE_APP_SMC_CMD_DEFAULT:
      if (k != ObjectCounts_pack(1, 0, 0, 0)) {
        return -1;
      }	
      if ((args[0].b.ptr) && (args[0].b.len == sizeof(mink_app_cmd_comm_t))) {
        comm = args[0].b.ptr;
        qsee_dcache_inval_region(comm->req, comm->reqlen);
        qsee_dcache_inval_region(comm->rsp, comm->rsplen);
        tz_app_cmd_handler(comm->req, comm->reqlen, comm->rsp, comm->rsplen);
        qsee_dcache_flush_region(comm->req, comm->reqlen );
        qsee_dcache_flush_region(comm->rsp, comm->rsplen );		
	  } else {
	    return -1;
	  }
	  break;
    case QSEE_APP_SHUTDOWN_COMMAND:
      tz_app_shutdown();
	  break;
    default:
      return -1;
  }  
  return 0;
}  
  
Object app_cmd_invoker_obj =  { app_cmd_invoke, NULL };

Object mink_opener_obj;



static int qsee_app_initial_invoke(ObjectCxt ptr, ObjectOp op, ObjectArg *args, ObjectCounts counts)
{
  typedef struct mink_app_init_comm_s {
    uint32 arg1;
    uint32 arg2; 
  } mink_app_init_comm_t;
  uint32 serviceid;
  ObjectArg a[2];
  
  Object objnull =  { NULL, NULL };
  if ((op == Object_OP_retain) || (op == Object_OP_release)) {
    return 0 ;
  }
  
  if (counts != ObjectCounts_pack(1, 0, 1, 1)) {
    return -1;
  }
	mink_opener_obj = args[1].o;
	
	if (!((args[0].b.ptr) && (args[0].b.len == sizeof(mink_app_init_comm_t)))) {
	  args[2].o = objnull;
	  return -1;
	}
	
	Object_retain(mink_opener_obj);
	
	serviceid = MINK_SERVICE_KERNEL_API_ID;
    a[0].b.ptr = &serviceid;
    a[0].b.len = sizeof(uint32);
    Object_invokeABCD(mink_opener_obj, MINK_CMD_OPEN, a, 1, 0, 0, 1);
	mink_api_service_obj = a[1].o;
	
    serviceid = MINK_SERVICE_USER_SYSCALL_ID;
    a[0].b.ptr = &serviceid;
    a[0].b.len = sizeof(uint32);
    Object_invokeABCD(mink_opener_obj, MINK_CMD_OPEN, a, 1, 0, 0, 1);
	mink_user_sscall_service_obj = a[1].o;
	
	tzlib_app_entry();

    args[2].o = app_cmd_invoker_obj;
	
	return 0;
}


void tzlib_app_entry(void)
{
  __cpp_initialize__aeabi_();

  mem_magic_number.magic_num=&magic_num[0];
  mem_magic_number.magic_num_index_array=&magic_num_index_array[0];
  mem_magic_number.magic_num_index=0;
  //DALHeap_Init( &application_heap, (void*)&Image$$TZ_APP_HEAP$$ZI$$Base, (unsigned int)&Image$$TZ_APP_HEAP$$ZI$$Length);
  mem_init_heap( &application_heap, &mem_magic_number,(void*)&Image$$TZ_APP_HEAP$$ZI$$Base, (unsigned int)&Image$$TZ_APP_HEAP$$ZI$$Limit - (unsigned int)&Image$$TZ_APP_HEAP$$ZI$$Base);
  //DALHeap_Init( &application_heap, (void*)&Image$$TZ_APP_HEAP$$ZI$$Base, TZ_APP_HEAP_SIZE);
  //DALHeap_Init( &application_heap, TZ_APP_HEAP, TZ_APP_HEAP_SIZE);

  qsee_prng_getdata((uint8*)__stack_chk_guard, 4);
  
  tz_app_init();
}
