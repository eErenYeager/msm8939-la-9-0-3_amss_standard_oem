/**
@file biometric_serializer.h

@brief This file includes the interface of the biometric library message
	   serialization/de-serialization module. The purpose of this module
	   is to decouple the way the data is stored in the buffer from the
	   main module so that both can be enhanced independently.
*/
/*===========================================================================
   Copyright (c) 2015 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/
#ifndef BIOMETRIC_SERIALIZER_H
#define BIOMETRIC_SERIALIZER_H

/*===========================================================================

                            EDIT HISTORY FOR FILE

when         who      what, where, why
--------     ---      ------------------------------------
Jun/10/2015  el       API modification including bulletin_board usage.
Apr/14/2015  el       Initial version.
===========================================================================*/
/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include <stdbool.h>
#include "biometric_result.h"

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/

 
/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/

 
/*----------------------------------------------------------------------------
 * Function Declarations and Documentation
 * -------------------------------------------------------------------------*/
bool bio_serialize_auth_result(bio_buffer* buffer,
							   uint8_t bio_lib_major_version_num,
							   uint8_t bio_lib_minor_version_num,
							   bool auth_result,
							   uint64_t user_id,
							   uint64_t user_entity_id,
							   uint64_t uptime_counter,
							   uint64_t transaction_id,
							   const bio_buffer* extension_data);


bool bio_deserialize_auth_result(const bio_buffer* buffer,
							     uint8_t* bio_lib_major_version_num,
							     uint8_t* bio_lib_minor_version_num,
							     bool* auth_result,
							     uint64_t* user_id,
							     uint64_t* user_entity_id,
							     uint64_t* uptime_counter,
							     uint64_t* transaction_id,
							     bio_buffer* extension_data);


#endif // BIOMETRIC_SERIALIZER_H
