#ifndef SECUREMM_H
#define SECUREMM_H

/** @file securemm.h
 * @brief
 * This file contains shared definitions between securemm modules. 
 */

/*===========================================================================
 Copyright (c) 2015 Qualcomm Technologies, Inc.
 All Rights Reserved.
 Qualcomm Technologies Proprietary and Confidential.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/30/15   ab      Created the file.

===========================================================================*/
#include <comdef.h>

#define SECUREMM_MAX_PHYSICAL_MEM_CHUNKS (256)

#endif /* SECUREMM_H */
