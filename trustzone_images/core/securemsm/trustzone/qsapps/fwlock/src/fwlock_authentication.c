/*
 @file fwlock_authentication.c
 @brief FWLOCK PK and Certificates functionality implementation.

 */
/*===========================================================================
 Copyright (c) 2014 Qualcomm Technologies, Inc.
 All Rights Reserved. Qualcomm Technologies Proprietary and Confidential.
 ===========================================================================*/

/*===========================================================================

 EDIT HISTORY FOR FILE
 $Header:
 $DateTime:
 $Author:

 when         who     what, where, why
 ----------   ---     -------------------------------------------------------
 07/14/14     ablay   Initial version.
 ===========================================================================*/

#include "qsee_log.h"
#include "fwlock.h"

#include <comdef.h>
#include <stdlib.h>
#include <comdef.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>
#include <stringl.h>

#include "openssl/pkcs7.h"
#include "openssl/pem.h"

extern unsigned char fwlock_qcom_root_cert[];
extern unsigned int fwlock_qcom_root_cert_size;

/*-------------------------------------------------------------------------*/

/**
 * fwlock_auth_get_isv_crl
 *
 * Helper function for building X509_CRL from a provided buffer 
 * holding binary data.
 *
 * @return - negative value for an error, 0 for success.
 */
static int fwlock_auth_get_isv_crl(fwlock_crl_buf_t *isv_crl_buf, X509_CRL **x509_crl)
{
  BIO *crl_bio = NULL;
  int status;
  int ret = -1;

  do {
    /* Create a BIO to hold the ISV CRL, and fill it up with isv_crl_buf */
    crl_bio = BIO_new(BIO_f_buffer());
    if (crl_bio == NULL) {
      QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: fwlock_auth_get_isv_crl: crl_bio is null");
      break;
    }

    BIO_set_buffer_size(crl_bio, isv_crl_buf->len);

    status = (int) BIO_set_buffer_read_data(crl_bio, isv_crl_buf->data, isv_crl_buf->len);
    if (status != 1) {
      QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: fwlock_auth_get_isv_crl: crl_bio data read error");
      break;
    }

    /* Build X509 CRL from the BIO */
    *x509_crl = PEM_read_bio_X509_CRL(crl_bio, NULL, NULL, NULL);
    if (*x509_crl == NULL) {
      QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: fwlock_auth_get_isv_crl: error parsing CRL");
      break;
    }

    ret = 0;
  } while (0);

  if (crl_bio)
    BIO_free_all(crl_bio);

  return ret;
}

/**
 * fwlock_auth_verify_isv_msg
 *
 * Verify a signed PKCS7 message sent by an ISV and return the
 * signed payload.
 *
 * @return - negative value for an error, 0 for success.
 */
int fwlock_auth_verify_isv_msg(fwlock_pkcs7_buf_t *pkcs7_buf, fwlock_buffer_t *data_buf, fwlock_crl_buf_t *isv_crl_buf)
{ 
  int status;
  int ret = -1;
  X509 *qcom_ca_cert = NULL;
  BIO *qcom_ca_cert_bio = NULL;
  BIO *pkcs7_bio = NULL;
  BIO* out_bio = NULL;
  PKCS7 *pkcs7 = NULL;
  X509_STORE *store = NULL;
  ASN1_OCTET_STRING *os=NULL;
  X509_CRL *x509_crl = NULL;

  QSEE_LOG(QSEE_LOG_MSG_DEBUG, "fwlock: fwlock_auth_verify_isv_msg");

  do {
    if (isv_crl_buf->len != 0) {
      /* Build X509_CRL from provided buffer */
      status = fwlock_auth_get_isv_crl(isv_crl_buf, &x509_crl);
      if (status != 0) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: fwlock_auth_verify_isv_msg: fwlock_auth_get_isv_crl() error");
        break;
      }
    }

    pkcs7_bio = BIO_new(BIO_f_buffer());
    if (pkcs7_bio == NULL) {
      QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: fwlock_auth_verify_isv_msg: pkcs7_bio is null");
      break;
    }

    /* Build PKCS7 BIO from provided buffer */
    BIO_set_buffer_size(pkcs7_bio, pkcs7_buf->len);

    status = (int) BIO_set_buffer_read_data(pkcs7_bio, pkcs7_buf->data, pkcs7_buf->len);
    if (status != 1) {
      QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: fwlock_auth_verify_isv_msg: pkcs7_bio data read error");
      break;
    }

    /* Build PKCS7 structure from PKCS7 BIO */
    pkcs7 = d2i_PKCS7_bio(pkcs7_bio, NULL);

    /* Create QCOM CA Cert BIO and fill it with the fwlock_qcom_root_cert */
    qcom_ca_cert_bio = BIO_new(BIO_f_buffer());
    if (qcom_ca_cert_bio == NULL) {
      QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: fwlock_auth_verify_isv_msg: qcom_ca_cert_bio is null");
      break;
    }

    BIO_set_buffer_size(qcom_ca_cert_bio, fwlock_qcom_root_cert_size);

    status = (int) BIO_set_buffer_read_data(qcom_ca_cert_bio, fwlock_qcom_root_cert, fwlock_qcom_root_cert_size);
    if (status != 1) {
      QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: fwlock_auth_verify_isv_msg: qcom_ca_cert_bio data read failed");
      break;
    }

    /* Build X509 structure from the QCOM CA Cert BIO */
    qcom_ca_cert = PEM_read_bio_X509(qcom_ca_cert_bio, NULL, 0, NULL);
    if (qcom_ca_cert == NULL) {
      QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: fwlock_auth_verify_isv_msg: qcom_ca_cert is null");
      break;
    }

    /* Create a certificate store and add the QCOM CA certificate to it */
    store = X509_STORE_new();
    if (store == NULL) {
      QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: fwlock_auth_verify_isv_msg: store is null");
      break;
    }

    status = X509_STORE_add_cert(store, qcom_ca_cert);
    if (status != 1) {
      QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: fwlock_auth_verify_isv_msg: X509_STORE_add_cert() failed");
      break;
    }

    /* Add the CRL to the certificate store */
    if (x509_crl != NULL)
    {
      QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: fwlock_auth_verify_isv_msg: adding CRL");

      X509_STORE_set_flags(store, X509_V_FLAG_CRL_CHECK | X509_V_FLAG_CRL_CHECK_ALL);

      status = X509_STORE_add_crl(store, x509_crl);
      if (status != 1) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: fwlock_auth_verify_isv_msg: X509_STORE_add_crl() failed");
        break;
      }
    }

    out_bio = BIO_new(BIO_f_buffer());

    /* Verify the PKCS7 message with the given certificate store */
    status = PKCS7_verify(pkcs7, NULL, store, NULL, out_bio, 0);
    if (status != 1) {
      QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: fwlock_auth_verify_isv_msg: PKCS7_verify() failed");
      break;
    }

    QSEE_LOG(QSEE_LOG_MSG_DEBUG, "fwlock: fwlock_auth_verify_isv_msg: verified!");

    os = pkcs7->d.sign->contents->d.data;

    /* Return the payload of the signed message in data_buf */
    data_buf->data = os->data;
    data_buf->len = os->length;
    
    ret = 0;  
  } while (0);

  if (store)
    X509_STORE_free(store);
  if (out_bio)
    BIO_free_all(out_bio);
  if (pkcs7_bio)
    BIO_free_all(pkcs7_bio);
  if (qcom_ca_cert_bio)
    BIO_free_all(qcom_ca_cert_bio);
  if (x509_crl)
    X509_CRL_free(x509_crl);
  if (qcom_ca_cert)
    X509_free(qcom_ca_cert);

  return ret;
}
