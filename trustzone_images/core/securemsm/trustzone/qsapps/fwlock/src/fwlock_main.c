/*
 @file fwlock_main.c
 @brief Main FWLOCK handler functionality.

 */
/*===========================================================================
 Copyright (c) 2014 Qualcomm Technologies, Inc.
 All Rights Reserved. Qualcomm Technologies Proprietary and Confidential.
 ===========================================================================*/

/*===========================================================================

 EDIT HISTORY FOR FILE
 $Header:
 $DateTime:
 $Author:

 when         who     what, where, why
 ----------   ---     -------------------------------------------------------
 07/14/14     ablay   Add support for CRLs.
 04/05/14     ablay   Initial version.
 ===========================================================================*/

#include "qsee_cryptolib.h"
#include "openssl/evp.h"
#include "openssl/engine.h"

#include "qsee_log.h"
#include "qsee_core.h"
#include "qsee_time.h"
#include "fwlock.h"

char TZ_APP_NAME[] = { "fwlock" };

#ifdef ENABLE_QSEE_LOG_MSG_DEBUG
#undef ENABLE_QSEE_LOG_MSG_DEBUG
#endif
#define ENABLE_QSEE_LOG_MSG_DEBUG 1

#define FWLOCK_YEAR_OFFSET 1900

fwlock_function_t g_fwlock_function[] = {
    FWLOCK_FUNC(FWLOCK_GEN_TOKEN_CMD, fwlock_gen_token, fwlock_generic_req_t, fwlock_generic_rsp_t),
    FWLOCK_FUNC(FWLOCK_ACTIVATE_CMD, fwlock_activate, fwlock_activate_req_t, fwlock_activate_rsp_t),
    FWLOCK_FUNC(FWLOCK_COMPLETE_ACTIVATION_CMD, fwlock_complete_activation, fwlock_complete_activation_req_t, fwlock_complete_activation_rsp_t),
    FWLOCK_FUNC(FWLOCK_DEACTIVATE_REMOTE_CMD, fwlock_deactivate_remote, fwlock_deactivate_remote_req_t, fwlock_generic_rsp_t),
    FWLOCK_FUNC(FWLOCK_DEACTIVATE_LOCAL_CMD, fwlock_deactivate_local, fwlock_deactivate_local_req_t, fwlock_generic_rsp_t),
	FWLOCK_FUNC(FWLOCK_UPDATE_ISV_CRL_CMD, fwlock_update_isv_crl, fwlock_update_isv_crl_req_t, fwlock_generic_rsp_t),
	FWLOCK_FUNC(FWLOCK_PROVISION_CMD, fwlock_provision, fwlock_generic_req_t, fwlock_generic_rsp_t),
  };
	
extern time_t (*qsee_trusted_time)(time_t *timer);

/*-------------------------------------------------------------------------*/

static time_t fwlock_time(time_t *timer)
{
	int ret = 0;
	struct tztm tzTime;
	time_t time = {0};

	ret = time_getsystime(&tzTime);
	if (ret) {
		QSEE_LOG(TZ_LOG_MSG_DEBUG, "fwlock: unable to get system time: %d", ret);
		return time;
	}

	time = time_t_set(tzTime.tm_mday, tzTime.tm_mon, tzTime.tm_year + FWLOCK_YEAR_OFFSET);

	QSEE_LOG(TZ_LOG_MSG_ERROR, "fwlock: time: : tzTime.tm_mday: %d,tzTime.tm_mon: %d,tzTime.tm_year: %d", tzTime.tm_mday,tzTime.tm_mon,tzTime.tm_year);

	return time;
}
  	
/*-------------------------------------------------------------------------*/

static void fwlock_cryptolib_setup(void)
{
	QSEE_LOG(QSEE_LOG_MSG_DEBUG, "fwlock_cryptolib_setup");

	qsee_cryptolib_setup();
	OpenSSL_add_all_algorithms();
	ERR_load_crypto_strings();
}

static void fwlock_cryptolib_tear_down(void)
{
	QSEE_LOG(QSEE_LOG_MSG_DEBUG, "fwlock_cryptolib_tear_down");

	/* The following cleanups are needed in order to avoid OpenSSL memory leaks */

	ENGINE_cleanup();
	ERR_free_strings();
	EVP_cleanup();
	CRYPTO_cleanup_all_ex_data();
	qsee_cryptolib_tear_down();
}

/*-------------------------------------------------------------------------*/

void tz_app_cmd_handler(void* cmd, uint32 cmdlen, void* rsp, uint32 rsplen) {
  fwlock_generic_req_t* fwlock_cmd = (fwlock_generic_req_t*) cmd;
  fwlock_generic_rsp_t* fwlock_rsp = (fwlock_generic_rsp_t*) rsp;

  do {
    if ((uint32)(fwlock_cmd->cmd_id >= FWLOCK_CMD_LAST)) {
      QSEE_LOG(QSEE_LOG_MSG_ERROR, "Cmd failed: cmd id %d >= %d", fwlock_cmd->cmd_id,
        FWLOCK_CMD_LAST);
      break;
    }

    if (g_fwlock_function[fwlock_cmd->cmd_id].cmd_id != fwlock_cmd->cmd_id) {
      QSEE_LOG(QSEE_LOG_MSG_ERROR, "Cmd failed: cmd id %d != %d", fwlock_cmd->cmd_id,
        g_fwlock_function[fwlock_cmd->cmd_id].cmd_id);
      break;
    }

    if (g_fwlock_function[fwlock_cmd->cmd_id].func == NULL) {
      QSEE_LOG(QSEE_LOG_MSG_ERROR, "Cmd failed: NULL function");
      break;
    }

    if ((g_fwlock_function[fwlock_cmd->cmd_id].req_len > cmdlen) ||
      (g_fwlock_function[fwlock_cmd->cmd_id].rsp_len > rsplen)) {
      QSEE_LOG(QSEE_LOG_MSG_ERROR, "Cmd failed: req len %d buff len %d, rsp len %d buff len %d",
        g_fwlock_function[fwlock_cmd->cmd_id].req_len, cmdlen,
        g_fwlock_function[fwlock_cmd->cmd_id].rsp_len, rsplen);
      break;
    }

    QSEE_LOG(QSEE_LOG_MSG_DEBUG, "Cmd %d OK", fwlock_cmd->cmd_id);

	fwlock_cryptolib_setup();

	g_fwlock_function[fwlock_cmd->cmd_id].func(cmd, rsp);

	fwlock_cryptolib_tear_down();

    return;
  } while (0);

  fwlock_rsp->ret = FWLOCK_FAILURE;
  return;
}

/*-------------------------------------------------------------------------*/

void fwlock_gen_token(void *cmd, void *rsp)
{
	fwlock_gen_token_rsp_t *gen_token_rsp = (fwlock_gen_token_rsp_t*)rsp;
	fwlock_status_t ret;

	QSEE_LOG(QSEE_LOG_MSG_DEBUG, "fwlock_gen_token - enter");

	ret = (fwlock_gen_token_impl(&gen_token_rsp->token) == 0) ? FWLOCK_SUCCESS: FWLOCK_FAILURE;

	gen_token_rsp->ret = ret;

  QSEE_LOG(QSEE_LOG_MSG_DEBUG, "fwlock_gen_token - exit");
}

void fwlock_activate(void *cmd, void *rsp)
{
	fwlock_activate_req_t *activate_req = (fwlock_activate_req_t*)cmd;
	fwlock_activate_rsp_t *activate_rsp = (fwlock_activate_rsp_t*)rsp;
	fwlock_status_t ret;

	QSEE_LOG(QSEE_LOG_MSG_DEBUG, "fwlock_activate - enter");

	ret = (fwlock_activate_impl(&activate_req->pkcs7_buf, &activate_req->config, &activate_rsp->dip) == 0) ? FWLOCK_SUCCESS: FWLOCK_FAILURE;

	activate_rsp->ret = ret;

	QSEE_LOG(QSEE_LOG_MSG_DEBUG, "fwlock_activate - exit");
}

void fwlock_complete_activation(void *cmd, void *rsp)
{
	fwlock_complete_activation_req_t *complete_activation_req = (fwlock_complete_activation_req_t*)cmd;
	fwlock_complete_activation_rsp_t *complete_activation_rsp = (fwlock_complete_activation_rsp_t*)rsp;
	fwlock_status_t ret;

	QSEE_LOG(QSEE_LOG_MSG_DEBUG, "fwlock_complete_activation - enter");

	ret = (fwlock_complete_activation_impl(&complete_activation_req->lock_req, &complete_activation_req->lock_rsp) == 0) ? FWLOCK_SUCCESS: FWLOCK_FAILURE;

	complete_activation_rsp->ret = ret;

	QSEE_LOG(QSEE_LOG_MSG_DEBUG, "fwlock_complete_activation - exit");
}

void fwlock_deactivate_remote(void *cmd, void *rsp)
{
	fwlock_deactivate_remote_req_t *deactivate_remote_req = (fwlock_deactivate_remote_req_t*)cmd;
	fwlock_generic_rsp_t *deactivate_remote_rsp = (fwlock_generic_rsp_t*)rsp;
	fwlock_status_t ret;

	QSEE_LOG(QSEE_LOG_MSG_DEBUG, "fwlock_deactivate_remote - enter");

	ret = (fwlock_deactivate_remote_impl(&deactivate_remote_req->pkcs7_buf) == 0) ? FWLOCK_SUCCESS: FWLOCK_FAILURE;

	deactivate_remote_rsp->ret = ret;

	QSEE_LOG(QSEE_LOG_MSG_DEBUG, "fwlock_deactivate_remote - exit");
}

void fwlock_deactivate_local(void *cmd, void *rsp)
{
	fwlock_deactivate_local_req_t *deactivate_local_req = (fwlock_deactivate_local_req_t*)cmd;
	fwlock_generic_rsp_t *deactivate_local_rsp = (fwlock_generic_rsp_t*)rsp;
	fwlock_status_t ret;

	QSEE_LOG(QSEE_LOG_MSG_DEBUG, "fwlock_deactivate_local - enter");

	ret = (fwlock_deactivate_local_impl(&deactivate_local_req->user_auth) == 0) ? FWLOCK_SUCCESS: FWLOCK_FAILURE;

	deactivate_local_rsp->ret = ret;

	QSEE_LOG(QSEE_LOG_MSG_DEBUG, "fwlock_deactivate_local - exit");
}

void fwlock_update_isv_crl(void *cmd, void *rsp)
{
	fwlock_update_isv_crl_req_t *update_isv_crl_req = (fwlock_update_isv_crl_req_t*)cmd;
	fwlock_generic_rsp_t *update_isv_crl_rsp = (fwlock_generic_rsp_t*)rsp;
	fwlock_status_t ret;

	QSEE_LOG(QSEE_LOG_MSG_DEBUG, "fwlock_update_isv_crl - enter");
	
	ret = (fwlock_update_isv_crl_impl(&update_isv_crl_req->isv_crl) == 0) ? FWLOCK_SUCCESS: FWLOCK_FAILURE;

	update_isv_crl_rsp->ret = ret;

	QSEE_LOG(QSEE_LOG_MSG_DEBUG, "fwlock_update_isv_crl - exit");
}

void fwlock_provision(void *cmd, void *rsp)
{
	fwlock_generic_rsp_t *provision_rsp = (fwlock_generic_rsp_t*)rsp;
	fwlock_status_t ret;

	QSEE_LOG(QSEE_LOG_MSG_DEBUG, "fwlock_provision - enter");

	ret = (fwlock_provision_impl() == 0) ? FWLOCK_SUCCESS: FWLOCK_FAILURE;

	provision_rsp->ret = ret;

	QSEE_LOG(QSEE_LOG_MSG_DEBUG, "fwlock_provision - exit");
}

/*-------------------------------------------------------------------------*/

/**
 @brief
 Add any app specific initialization code here
 QSEE will call this function after secure app is loaded and
 authenticated
 */
void tz_app_init(void) {
  /* App specific initialization code */
  QSEE_LOG(QSEE_LOG_MSG_DEBUG, "FWLock App Init");

  if (qsee_cryptolib_create() != 0)
  {
	  QSEE_LOG(QSEE_LOG_MSG_DEBUG, "qsee_cryptolib_create() failed");
	  return;
  }

  qsee_trusted_time = fwlock_time;
}

/**
 @brief
 App specific shutdown
 App will be given a chance to shutdown gracefully
 */
void tz_app_shutdown(void) {
  /* app specific shutdown code*/
  QSEE_LOG(QSEE_LOG_MSG_DEBUG, "FWLock App shutdown");

  qsee_cryptolib_destroy();

  return;
}

