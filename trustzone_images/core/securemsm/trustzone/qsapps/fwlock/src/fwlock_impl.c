/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

 Firmware Lock QSEE Implementation

 GENERAL DESCRIPTION
 Contains the Firmware Lock QSEE application implementation.

 EXTERNALIZED FUNCTIONS
 None

 INITIALIZATION AND SEQUENCING REQUIREMENTS

 Copyright (c) 2014 Qualcomm Technologies, Inc.
 All Rights Reserved. Qualcomm Technologies Proprietary and Confidential.

 *====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

 EDIT HISTORY FOR MODULE


 when         who     what, where, why
 ----------   ---     -------------------------------------------------------
 07/14/14     ablay   Add support for CRLs.
 05/04/2014   ablay   Initial Version.
 ===========================================================================*/

#include <comdef.h>
#include <string.h>
#include "qsee_log.h"
#include "fwlock.h"
#include "qsee_heap.h"
#include "qsee_services.h"
#include "syscall_wrapper.h"
#include "syscall.h"
#include "qsee_fs.h"
#include "qsee_sfs.h"
#include "qsee_prng.h"
#include "qsee_core.h"
#include "IxErrno.h"

#define DIP_VERSION 0x0001

#ifdef ENABLE_QSEE_LOG_MSG_DEBUG
#undef ENABLE_QSEE_LOG_MSG_DEBUG
#endif
#define ENABLE_QSEE_LOG_MSG_DEBUG 1

#define FWLOCK_SVC_ID 0x6000
#define FWLOCK_PRODUCE_HMAC_DIP_CID 0
#define FWLOCK_PRODUCE_HMAC_PIN_CID 1

static int get_DIP_blob(DIP_t *dip);

/*****************************************************************************************************************/

/**
 * is_test_mode
 *
 * This function checks if we are in test mode.
 *
 * @return true if we are in test mode, false otherwise.
*/
static int is_test_mode(void)
{
    static int test_mode_set = 0;
    static int test_mode = 0;
    int ret = 0;
    qsee_secctrl_secure_status_t status;

#define SECBOOT_FUSE       0x01
#define SHK_FUSE           0x02
#define DEBUG_FUSE         0x04

    /* Make sure we only read the test mode once */
    if (test_mode_set)
        return test_mode;

    memset(&status,0,sizeof(qsee_secctrl_secure_status_t));
    ret = qsee_get_secure_state(&status);

    if(ret == 0)
    {
        /* (SECBOOT_FUSE | SHK_FUSE | DEBUG_FUSE) implies that none of the fuses are blown */
        if((status.value[0] & (SECBOOT_FUSE | SHK_FUSE | DEBUG_FUSE)) == (SECBOOT_FUSE | SHK_FUSE | DEBUG_FUSE))
            test_mode = 1;
    }
    else
    {
        QSEE_LOG(TZ_LOG_MSG_ERROR, "fwlock: qsee_get_secure_state returned error: %d, status.value[0]: %ld", ret, status.value[0]);
        test_mode = 0;
    }
    
    test_mode_set = 1;
    QSEE_LOG(TZ_LOG_MSG_DEBUG, "fwlock: test mode is set to %d", test_mode);

    return test_mode;
}

/**
 * write_sfs
 *
 * This function writes the fwlock configuration into SFS file.
 *
 * @return - negative value for an error, 0 for success.
*/
static int write_sfs(fwlock_sfs_entry_t *data)
{
    int fd;
    int ret = 0;

    do {
        fd = qsee_sfs_open(FWLOCK_DATA_FILE, O_CREAT | O_WRONLY | O_TRUNC);
        if (fd == NULL) {
          QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: qsee_sfs_open failed %s, error=%d", FWLOCK_DATA_FILE, qsee_sfs_error(fd));
          return -1;
        }

        if (qsee_sfs_write(fd, (char*)data, sizeof(fwlock_sfs_entry_t)) != sizeof(fwlock_sfs_entry_t)) {
          QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: qsee_sfs_write failed %s", FWLOCK_DATA_FILE);
          ret = -1;
          break;
        }
  } while (0);

  if (fd) {
    if (0 != qsee_sfs_close(fd)) {
      QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: qsee_sfs_close failed %s", FWLOCK_DATA_FILE);
      return -1;
    }
  }

  return ret;
}

/**
 * read_sfs
 *
 * This function reads the fwlock configuration from SFS file.
 *
 * @return - negative value for an error, 0 for success.
*/
static int read_sfs(fwlock_sfs_entry_t *data)
{
    int fd;
    uint32 file_size;
    int ret = 0;
    int last_err;

    do {
    
        fd = qsee_sfs_open(FWLOCK_DATA_FILE, O_RDONLY);
        if (fd == NULL) {
          last_err = qsee_sfs_error(NULL);
          if (last_err != SFS_NO_FILE) {
            /* Real error while opening the file */
            QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: qsee_sfs_open failed %s", FWLOCK_DATA_FILE);
            ret = -1;
            break;
          } else
          {
            /* File doesn't exist, handle this as an error, but produce different log */
            QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: file %s doesn't exist", FWLOCK_DATA_FILE);
            ret = -1;
            break;
          }
        }
    
        if ((0 != qsee_sfs_getSize(fd, &file_size)
            || (sizeof(fwlock_sfs_entry_t) != file_size))) {
          QSEE_LOG(QSEE_LOG_MSG_ERROR,
              "fwlock: qsee_sfs_getSize failed %s", FWLOCK_DATA_FILE);
          ret = -1;
          break;
        }
    
        if (qsee_sfs_read(fd, (char*)data, sizeof(fwlock_sfs_entry_t)) != sizeof(fwlock_sfs_entry_t)) {
          QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: qsee_sfs_read failed %s", FWLOCK_DATA_FILE);
          ret = -1;
          break;
        }
  } while (0);

  if (fd) {
    if (0 != qsee_sfs_close(fd)) {
      QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: qsee_sfs_close failed %s", FWLOCK_DATA_FILE);
      return -1;
    }
  }

  return ret;
}

/**
 * write_crl_sfs
 *
 * This function writes a given ISV CRL into SFS file. 
 *  
 * @return - negative value for an error, 0 for success.
*/
static int write_crl_sfs(fwlock_crl_buf_t *isv_crl_buf)
{
    int fd;
    int ret = 0;

    do {
        fd = qsee_sfs_open(FWLOCK_CRL_FILE, O_CREAT | O_WRONLY | O_TRUNC);
        if (fd == NULL) {
          QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: qsee_sfs_open failed %s, error=%d", FWLOCK_DATA_FILE, qsee_sfs_error(fd));
          return -1;
        }

        if (qsee_sfs_write(fd, (char*)isv_crl_buf->data, isv_crl_buf->len) != isv_crl_buf->len) {
          QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: qsee_sfs_write failed %s", FWLOCK_CRL_FILE);
          ret = -1;
          break;
        }
  } while (0);

  if (fd) {
    if (0 != qsee_sfs_close(fd)) {
      QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: qsee_sfs_close failed %s", FWLOCK_CRL_FILE);
      return -1;
    }
  }

  return ret;
}

/**
 * read_crl_sfs
 *
 * This function reads the ISV CRL from SFS file. 
 *  
 * @return - negative value for an error, 0 for success.
*/
static int read_crl_sfs(fwlock_crl_buf_t *isv_crl_buf)
{
    int fd;
    uint32 file_size;
    int ret = 0;
    int last_err;

    do {
    
        fd = qsee_sfs_open(FWLOCK_CRL_FILE, O_RDONLY);
        if (fd == NULL) {
          last_err = qsee_sfs_error(NULL);
          if (last_err != SFS_NO_FILE) {
            /* Real error while opening the file */
            QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: qsee_sfs_open failed %s", FWLOCK_CRL_FILE);
            ret = -1;
            break;
          } else
          {
            /* File doesn't exist, don't handle this as an error */
            QSEE_LOG(QSEE_LOG_MSG_DEBUG, "fwlock: file %s doesn't exist", FWLOCK_CRL_FILE);
            break;
          }
        }
    
        if ((0 != qsee_sfs_getSize(fd, &file_size))) {
          QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: qsee_sfs_getSize failed %s", FWLOCK_CRL_FILE);
          ret = -1;
          break;
        }
    
        if (qsee_sfs_read(fd, (char*)(isv_crl_buf->data), file_size) != file_size) {
          QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: qsee_sfs_read failed %s", FWLOCK_CRL_FILE);
          ret = -1;
          break;
        }
        isv_crl_buf->len = file_size;
  } while (0);

  if (fd) {
    if (0 != qsee_sfs_close(fd)) {
      QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: qsee_sfs_close failed %s", FWLOCK_CRL_FILE);
      return -1;
    }
  }

  return ret;
}

/**
 * authenticate_req
 *
 * This function authenticates a signed PKCS7 request received
 * from a ISV cloud service.
 *  
 * @return - negative value for an error, 0 for success.
*/
static int authenticate_req(fwlock_pkcs7_buf_t *pkcs7_buf, fwlock_buffer_t *data_buf)
{
    int status;
    fwlock_crl_buf_t isv_crl_buf;

    QSEE_LOG(QSEE_LOG_MSG_DEBUG, "fwlock: authenticate_req");

    isv_crl_buf.len = 0; /* The default is an empty CRL */

    status = read_crl_sfs(&isv_crl_buf);
    if (status) {
        /* This is in case of real error - not a case where the SFS file doesn't exist */
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: failure reading CRL SFS file");
        return -1;
    }

    status = fwlock_auth_verify_isv_msg(pkcs7_buf, data_buf, &isv_crl_buf);
    if (status) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: authenticate_req: failed verifying message");
        return -1;
    }

    return 0;
}

/**
 * validate_token
 *
 * This function validates a token against a token previously 
 * generated and stored in SFS.
 *  
 * @return - negative value for an error, 0 for success.
*/
static int validate_token(fwlock_pkcs7_buf_t *pkcs7_buf, fwlock_buffer_t *data_buf)
{
    int status;
    fwlock_sfs_entry_t sfs_entry;
    fwlock_activation_req_data_t *activation_req_data = (fwlock_activation_req_data_t*)data_buf->data;
     
    QSEE_LOG(QSEE_LOG_MSG_DEBUG, "fwlock: validate_token");

    status = read_sfs(&sfs_entry);
    if (status) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: validate_token: SFS file read error");
        return -1;
    }

    return memcmp(activation_req_data->token.data, sfs_entry.session_token.data, FWLOCK_TOKEN_LEN);
}

/**
 * validate_config
 *
 * This function validates a that a configuration passed on
 * fwlock activation is valid.
 *  
 * @return - negative value for an error, 0 for success.
*/
static int validate_config(fwlock_lock_cfg_t *config)
{
    int i;

    QSEE_LOG(QSEE_LOG_MSG_DEBUG, "fwlock: validate_config");

    if (config->num_partitions > FWLOCK_MAX_PARTITIONS) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: validate_config: Wrong num_partitions, %d", config->num_partitions);
        return -1;
    }

    if (config->enable_local_deactivation != 0 && config->enable_local_deactivation != 1) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: validate_config: Wrong enable_local_deactivation, %d", config->enable_local_deactivation);
        return -1;
    }

    if (config->user_auth.local_auth_mode != FWLOCK_AUTH_UI_PIN &&
        config->user_auth.local_auth_mode != FWLOCK_AUTH_TUI_PIN) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: validate_config: Wrong local_auth_mode, %d", config->user_auth.local_auth_mode);
        return -1;
    }

    for (i=0; i < config->num_partitions; i++) {
        if (config->partitions[i].hash_mode != FWLOCK_MODE_SINGLE &&
            config->partitions[i].hash_mode != FWLOCK_MODE_BLOCK) {
            QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: validate_config: Wrong hash_mode %d, for partition %d", config->partitions[i].hash_mode, i);
            return -1;
        }

        if (config->partitions[i].verify_ratio > 100) {
            QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: validate_config: Wrong verify_ratio %d, for partition %d", config->partitions[i].verify_ratio, i);
            return -1;
        }
    }

    return 0;
}

/**
 * hmac256
 *
 * This function generates a symmetric signature on a given
 * buffer using HMAC256 algorithm.
 *  
 * @return - negative value for an error, 0 for success.
*/
static int hmac256(unsigned char* inbuf, uint32 len, unsigned char* outbuf)
{
    QSEE_LOG(QSEE_LOG_MSG_DEBUG, "hmac256: calling TZBSP qsee_fwlock_produce_hmac");

    return qsee_generic_syscall(SYSNUM(qsee_fwlock_produce_hmac), (unsigned int)inbuf,
        len, (unsigned int)outbuf, HMAC_LEN, FWLOCK_PRODUCE_HMAC_DIP_CID, 0);
}

/*****************************************************************************************************************/

/* DIP helper functions */

/**
 * validate_dip
 *
 * This function validates the provided DIP status and its 
 * integrity.
 *  
 * @return - negative value for an error, 0 for success.
*/
static int validate_dip(DIP_t *dip, int activated)
{
    unsigned char hmac[HMAC_LEN];

    QSEE_LOG(QSEE_LOG_MSG_DEBUG, "fwlock: validate_dip: validate_dip for activate: %d", activated);

    if ((activated && dip->status != DIP_STATUS_ACTIVATED) ||
            (!activated && dip->status != DIP_STATUS_DEACTIVATED)) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: validate_dip: DIP status is incompatible (%d)", dip->status);
        return -1;
    }

    if (dip->version > DIP_VERSION) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: validate_dip: DIP version is incompatible (%d)", dip->version);
        return -1;
    }

    if (hmac256((unsigned char*)dip, sizeof(DIP_t) - HMAC_LEN, (unsigned char*)&hmac[0]))
    {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: validate_dip: HMAC failed");
        return -1;
    }

    if (memcmp(&hmac[0], &(dip->hmac[0]), HMAC_LEN))
    {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: validate_dip: HMAC is wrong");
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: validate_dip: DIP HMAC: 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x", 
                 *(unsigned int*)(&dip->hmac[0]), *(unsigned int*)(&dip->hmac[4]), *(unsigned int*)(&dip->hmac[8]), *(unsigned int*)(&dip->hmac[12]), 
                 *(unsigned int*)(&dip->hmac[16]), *(unsigned int*)(&dip->hmac[20]), *(unsigned int*)(&dip->hmac[24]), *(unsigned int*)(&dip->hmac[28]));
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: validate_dip: Calculated HMAC: 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x", 
                 *(unsigned int*)(&hmac[0]), *(unsigned int*)(&hmac[4]), *(unsigned int*)(&hmac[8]), *(unsigned int*)(&hmac[12]), 
                 *(unsigned int*)(&hmac[16]), *(unsigned int*)(&hmac[20]), *(unsigned int*)(&hmac[24]), *(unsigned int*)(&hmac[28]));

        return -1;
    }

    return 0;
}

/**
 * create_activated_dip
 *
 * This function creates a DIP in activated state, and generates
 * a symmetric signature to protect its integrity. 
 *  
 * @return - negative value for an error, 0 for success.
*/
static int create_activated_dip(DIP_t *dip, lock_req_t *lock_req, lock_rsp_t *lock_rsp)
{
    uint32 p, b;

    QSEE_LOG(QSEE_LOG_MSG_DEBUG, "fwlock: create_activated_dip");

    memset(dip, 0, sizeof(DIP_t));

    dip->version = DIP_VERSION;
    dip->status = DIP_STATUS_ACTIVATED;
    dip->enable_local_deactivation = lock_req->enable_local_deactivation;
    
    if (lock_req->enable_local_deactivation)
    {
        dip->user_auth = lock_req->user_auth;
    }

    for (p = 0; p < lock_req->num_partitions; p++) {
        memcpy(&(dip->partition_cfg[p].name), &(lock_req->partitions[p].name), FWLOCK_MAX_PARTITION_NAME_LEN);
        dip->partition_cfg[p].size = lock_rsp->partition_rsp[p].size;   
        dip->partition_cfg[p].lock_enabled = !(lock_rsp->partition_rsp[p].status);
        dip->partition_cfg[p].hash_mode = lock_req->partitions[p].hash_mode;
        dip->partition_cfg[p].total_num_blocks = lock_rsp->partition_rsp[p].hash_table_size;
        dip->partition_cfg[p].verify_ratio = lock_req->partitions[p].verify_ratio;

        for (b = 0; b < dip->partition_cfg[p].total_num_blocks; b++) {
            memcpy(&(dip->partition_cfg[p].hash_table[b].hash[0]), &(lock_rsp->partition_rsp[p].hash_table[b].hash[0]), HASH_LEN);
            dip->partition_cfg[p].hash_table[b].force_verify_block = lock_req->partitions[p].force_verify_block[b];
        }
    }

    if (hmac256((unsigned char*)dip, sizeof(DIP_t) - HMAC_LEN, (unsigned char*)&dip->hmac[0]))
    {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: create_activated_dip: HMAC failed");
        return -1;
    }

    return 0;
}

/**
 * create_deactivated_dip
 *
 * This function creates a DIP in deactivated state, and 
 * generates a symmetric signature to protect its integrity. 
 *  
 * @return - negative value for an error, 0 for success.
*/
static int create_deactivated_dip(DIP_t *dip)
{
    QSEE_LOG(QSEE_LOG_MSG_DEBUG, "fwlock: create_deactivated_dip");

    memset(dip, 0, sizeof(DIP_t));

    dip->version = DIP_VERSION;
    dip->status = DIP_STATUS_DEACTIVATED;

    if (hmac256((unsigned char*)dip, sizeof(DIP_t) - HMAC_LEN, (unsigned char*)&dip->hmac[0]))
    {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: create_deactivated_dip: HMAC failed");
        return -1;
    }

    return 0;
}

/**
 * validate_deactivation_pin
 *
 * This function validates the PIN provided upon manual 
 * deactivation, against the activation configuration stored in 
 * SFS.
 *  
 * @return - negative value for an error, 0 for success.
*/
static int validate_deactivation_pin(DIP_t *dip, fwlock_user_auth_t *user_auth)
{
    QSEE_LOG(QSEE_LOG_MSG_DEBUG, "fwlock: validate_deactivation_pin");

    if (user_auth->local_auth_mode != dip->user_auth.local_auth_mode) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: validate_deactivation_pin: user authenticator mismatch");
        return -1;
    }

    if (user_auth->local_auth_mode == FWLOCK_AUTH_UI_PIN) {
        if (memcmp(user_auth->local_auth_data.local_auth_ui_pin, dip->user_auth.local_auth_data.local_auth_ui_pin, FWLOCK_MAX_PIN_LEN)) {
            QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: validate_deactivation_pin: Wrong manual deactivation PIN");
            return -1;
        }
    }
    else /* Use TUI */
    {
        /* To be implemented */
    }

    return 0;
}


/*****************************************************************************************************************/

/* QSEE send requests handling */

/**
 * get_DIP_blob
 *
 * Send request to HLOS to read and return the DIP.
 *
 * @return - negative value for an error, 0 for success.
 */
static int get_DIP_blob(DIP_t *dip)
{
    fwlock_get_dip_req_t *req;
    fwlock_get_dip_rsp_t *rsp;
    int ret;

    QSEE_LOG(QSEE_LOG_MSG_DEBUG, "fwlock: get_DIP_blob");

    req = (fwlock_get_dip_req_t*)qsee_malloc(sizeof(fwlock_get_dip_req_t));
    if(req == NULL)
      return -1;
    memset(req, 0, sizeof(fwlock_get_dip_req_t));

    rsp = (fwlock_get_dip_rsp_t*)qsee_malloc(sizeof(fwlock_get_dip_rsp_t));
    if(rsp == NULL) {
      qsee_free(req);
      return -1;
    }
    memset(rsp, 0, sizeof(fwlock_get_dip_rsp_t));

    (*req).cmd_id = FWLOCK_HLOS_GET_DIP;

    ret = qsee_request_service(FWLOCK_SVC_ID, req, sizeof(fwlock_get_dip_req_t), rsp, sizeof(fwlock_get_dip_rsp_t));

    if (ret) 
    {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: get_DIP_blob: error requsting QSEE service");
    }
    else
    {
        if (!rsp->ret) {
            memcpy(dip, &rsp->dip, sizeof(DIP_t));
            memset(&rsp->dip, 0, sizeof(DIP_t));
        } else
        {
            QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: get_DIP_blob: error returned by QSEE service");
        }
        ret = rsp->ret;
    }

    qsee_free(req);
    qsee_free(rsp);

    return ret;
}

/**
 * set_DIP_blob
 *
 * Send request to HLOS to write the DIP.
 *
 * @return - negative value for an error, 0 for success.
 */
static int set_DIP_blob(DIP_t *dip)
{
    fwlock_set_dip_req_t *req;
    fwlock_set_dip_rsp_t *rsp;
    int ret;

    QSEE_LOG(QSEE_LOG_MSG_DEBUG, "fwlock: set_DIP_blob");

    req = (fwlock_set_dip_req_t*)qsee_malloc(sizeof(fwlock_set_dip_req_t));
    if(req == NULL)
      return -1;
    memset(req, 0, sizeof(fwlock_set_dip_req_t));

    rsp = (fwlock_set_dip_rsp_t*)qsee_malloc(sizeof(fwlock_set_dip_rsp_t));
    if(rsp == NULL) {
      qsee_free(req);
      return -1;
    }
    memset(rsp, 0, sizeof(fwlock_set_dip_rsp_t));

    (*req).cmd_id = FWLOCK_HLOS_SET_DIP;
    memcpy(&req->dip, dip, sizeof(DIP_t));

    ret = qsee_request_service(FWLOCK_SVC_ID, req, sizeof(fwlock_set_dip_req_t), rsp, sizeof(fwlock_set_dip_rsp_t));
    if (ret) 
    {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: set_DIP_blob: error requsting QSEE service");
    }
    else
    {
        if (!rsp->ret) {
            memset(&req->dip, 0, sizeof(DIP_t));
        } else
        {
            QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: set_DIP_blob: error returned by QSEE service");
        }
        ret = rsp->ret;
    }

    qsee_free(req);
    qsee_free(rsp);

    return ret;
}

/**
 * request_activation
 *
 * Send request to HLOS to enqueue an activation request.
 *
 * @return - negative value for an error, 0 for success.
 */
static int request_activation(lock_req_t *lock_req)
{
    fwlock_request_activation_req_t *req;
    fwlock_request_activation_rsp_t *rsp;
    int ret;

    QSEE_LOG(QSEE_LOG_MSG_DEBUG, "fwlock: request_activation");

    req = (fwlock_request_activation_req_t*)qsee_malloc(sizeof(fwlock_request_activation_req_t));
    if(req == NULL)
      return -1;
    memset(req, 0, sizeof(fwlock_request_activation_req_t));

    rsp = (fwlock_request_activation_rsp_t*)qsee_malloc(sizeof(fwlock_request_activation_rsp_t));
    if(rsp == NULL) {
        qsee_free(req);
        return -1;
    }
    memset(rsp, 0, sizeof(fwlock_request_activation_rsp_t));

    (*req).cmd_id = FWLOCK_HLOS_REQUEST_ACTIVATION;
    memcpy(&req->lock_req, lock_req, sizeof(lock_req_t));

    ret = qsee_request_service(FWLOCK_SVC_ID, req, sizeof(fwlock_request_activation_req_t), rsp, sizeof(fwlock_request_activation_rsp_t));
    if (ret) 
    {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: request_activation: error requsting QSEE service");
    }
    else
    {
        if (rsp->ret)
        {
            QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: request_activation: error returned by QSEE service");
        }
        ret = rsp->ret;
    }

    qsee_free(req);
    qsee_free(rsp);

    return ret;
}

/*****************************************************************************************************************/

/* QSEE commands handling */

/**
 * fwlock_QSEE_gen_token
 *
 * Generate a fresh activation token.
 *
 * @return - negative value for an error, 0 for success.
 */
int fwlock_gen_token_impl(fwlock_token_t *token)
{
    fwlock_sfs_entry_t sfs_entry;
    int status;

    QSEE_LOG(QSEE_LOG_MSG_DEBUG, "fwlock: fwlock_gen_token_impl");

    /* Generate token */
    memset(token->data, 0, FWLOCK_TOKEN_LEN);

    /* Generate random data for the token only on an operational device */
    if (is_test_mode()) {
        memcpy(token->data, "12345678", 8);
    } else {
        int bytes_generated;
    
        bytes_generated = qsee_prng_getdata(token->data, FWLOCK_TOKEN_LEN);
        if (bytes_generated != FWLOCK_TOKEN_LEN) {
            return -1;
        }
    }

    sfs_entry.session_token_generated = 1;
    sfs_entry.session_token = *token;

    /* Store new token in fwlock SFS file */
    status = write_sfs(&sfs_entry);
    if (status) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: fwlock_gen_token_impl: SFS write error");
        return -1;
    }

    return 0;
}

/**
 * fwlock_QSEE_activate
 *
 * Activate the firmware lock protection.
 *
 * @return - negative value for an error, 0 for success.
 */
int fwlock_activate_impl(
    fwlock_pkcs7_buf_t *pkcs7_buf,
    fwlock_lock_cfg_t *config,
    DIP_t *dip_blob)
{
    lock_req_t *lock_req = config;
    int status;
    fwlock_buffer_t data_buf;

    QSEE_LOG(QSEE_LOG_MSG_DEBUG, "fwlock: fwlock_activate_impl");

    status = validate_config(config);
    if (status) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: fwlock_activate_impl: Config is illegal");
        return -1;
    }

    if (authenticate_req(pkcs7_buf, &data_buf))
    {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: fwlock_activate_impl: authenticate_req failed");
        return -1;
    }

    if (validate_token(pkcs7_buf, &data_buf))
    {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: fwlock_activate_impl: validate_token failed");
        return -1;
    }

    /* Generate default DIP blob */
    status = create_deactivated_dip(dip_blob);
    if (status) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: fwlock_activate_impl: Failure creating deactivated DIP");
        return -1;
    }

    return request_activation(lock_req);
}

/**
 * fwlock_QSEE_complete_activation
 *
 * Complete activate of the firmware lock protection.
 *
 * @return - negative value for an error, 0 for success.
 */
int fwlock_complete_activation_impl(lock_req_t *lock_req, lock_rsp_t *lock_rsp)
{
    DIP_t dip;
    int status;

    QSEE_LOG(QSEE_LOG_MSG_DEBUG, "fwlock: fwlock_complete_activation_impl");

    status = get_DIP_blob(&dip);
    if (status) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: fwlock_complete_activation_impl: Failure reading DIP");
        return -1;
    }

    status = validate_dip(&dip, 0);
    if (status) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: fwlock_complete_activation_impl: DIP is not valid for activation");
        return -1;
    }

    status = create_activated_dip(&dip, lock_req, lock_rsp);
    if (status) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: fwlock_complete_activation_impl: Failure creating activated DIP");
        return -1;
    }

    status = set_DIP_blob(&dip);
    if (status) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: fwlock_complete_activation_impl: Failure writing DIP");
        return -1;
    }

    return 0;
}

/**
 * fwlock_deactivate_remote_impl
 *
 * Deactivate the firmware lock protection using a signed 
 * request. 
 *
 * @return - negative value for an error, 0 for success.
 */
int fwlock_deactivate_remote_impl(fwlock_pkcs7_buf_t *pkcs7_buf)
{
    DIP_t dip;
    int status;
    fwlock_buffer_t data_buf;

    QSEE_LOG(QSEE_LOG_MSG_DEBUG, "fwlock: fwlock_deactivate_remote_impl");

    if (authenticate_req(pkcs7_buf, &data_buf))
    {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: fwlock_deactivate_remote_impl: authenticate_req failed");
        return -1;
    }

    status = get_DIP_blob(&dip);
    if (status) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: fwlock_deactivate_remote_impl: Failure reading DIP");
        return -1;
    }

    if (validate_token(pkcs7_buf, &data_buf))
    {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: fwlock_deactivate_remote_impl: validate_token failed");
        return -1;
    }

    status = validate_dip(&dip, 1);
    if (status) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: fwlock_deactivate_remote_impl: DIP is not valid for deactivation");
        return -1;
    }

    status = create_deactivated_dip(&dip);
    if (status) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: fwlock_deactivate_remote_impl: Failure creating deactivated DIP");
        return -1;
    }

    status = set_DIP_blob(&dip);
    if (status) {
        QSEE_LOG(QSEE_LOG_MSG_DEBUG, "fwlock: fwlock_deactivate_remote_impl: Failure writing DIP");
        return -1;
    }

    return 0;
}

/**
 * fwlock_deactivate_local_impl
 *
 * Deactivate the firmware lock protection manually, using an
 * activation PIN.
 *
 * @return - negative value for an error, 0 for success.
 */
int fwlock_deactivate_local_impl(fwlock_user_auth_t *user_auth)
{
    DIP_t dip;
    int status;

    QSEE_LOG(QSEE_LOG_MSG_DEBUG, "fwlock: fwlock_deactivate_local_impl");

    status = get_DIP_blob(&dip);
    if (status) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: fwlock_deactivate_local_impl: Failure reading DIP");
        return -1;
    }

    status = validate_dip(&dip, 1);
    if (status) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: fwlock_deactivate_local_impl: DIP is not valid for deactivation");
        return -1;
    }

    if (dip.enable_local_deactivation) {
        status = validate_deactivation_pin(&dip, user_auth);
        if (status) {
            QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: fwlock_deactivate_local_impl: Failure validating deactivation PIN");
            return -1;
        }
    }

    status = create_deactivated_dip(&dip);
    if (status) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: fwlock_deactivate_local_impl: Failure creating deactivated DIP");
        return -1;
    }

    status = set_DIP_blob(&dip);
    if (status) {
        QSEE_LOG(QSEE_LOG_MSG_DEBUG, "fwlock: fwlock_deactivate_local_impl: Failure writing DIP");
        return -1;
    }

    return 0;
}

/**
 * fwlock_update_isv_crl_impl
 *
 * Update the FWLock with an ISV CRL.
 *
 * @return - negative value for an error, 0 for success.
 */
int fwlock_update_isv_crl_impl(fwlock_crl_buf_t *isv_crl_buf)
{
    int status;

    QSEE_LOG(QSEE_LOG_MSG_DEBUG, "fwlock: fwlock_update_isv_crl_impl");

    status = write_crl_sfs(isv_crl_buf);
    if (status) {
        QSEE_LOG(QSEE_LOG_MSG_DEBUG, "fwlock: fwlock_update_isv_crl_impl: failure writing CRL SFS file");
        return -1;
    }

    return 0;
}

#ifdef ENABLE_QSEE_LOG_MSG_DEBUG

/**
 * fwlock_QSEE_provision
 *
 * Provision the firmware lock.
 *
 * @return - negative value for an error, 0 for success.
 */
int fwlock_provision_impl()
{
    DIP_t dip;
    int status = 0;

    QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: fwlock_provision_impl");

    if (!is_test_mode()) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: fwlock_provision_impl: only allowed in test mode");
        return -1;
    }

    memset(&dip, 0, sizeof(DIP_t));

    status = create_deactivated_dip(&dip);
    if (status) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: fwlock_provision_impl: Failure creating deactivated DIP");
        return -1;
    }

    status = set_DIP_blob(&dip);
    if (status) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: fwlock_provision_impl: Failure writing DIP");
        return -1;
    }

    /* Clean old fwlock SFS files. Remove can fail if the files still haven't been created. */
    status = qsee_sfs_rm(FWLOCK_DATA_FILE);
    if(status != E_SUCCESS) {
       QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: fwlock_provision_impl: qsee_sfs_rm() returned = %ld", status);
       status = 0;
    }

    status =  qsee_sfs_rm(FWLOCK_CRL_FILE);
    if(status != E_SUCCESS) {
       QSEE_LOG(QSEE_LOG_MSG_ERROR, "fwlock: fwlock_provision_impl: qsee_sfs_rm() returned = %ld", status);
       status = 0;
    }

    return status;
}

#endif /* ENABLE_QSEE_LOG_MSG_DEBUG */
