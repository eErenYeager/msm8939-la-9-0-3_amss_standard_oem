#ifndef FWLOCK_H
#define FWLOCK_H

/** @file fwlock.h
 * @brief
 * This file contains the definitions of the Firmware Lock QSEE app.
 */

/*===========================================================================
  Copyright (c) 2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/05/14   ablay   Initial version.

===========================================================================*/

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/

/** Commands HLOS->TZ */
typedef enum {
  FWLOCK_GEN_TOKEN_CMD = 0,
  FWLOCK_ACTIVATE_CMD,
  FWLOCK_COMPLETE_ACTIVATION_CMD,
  FWLOCK_DEACTIVATE_REMOTE_CMD,
  FWLOCK_DEACTIVATE_LOCAL_CMD,
  FWLOCK_UPDATE_ISV_CRL_CMD,
  FWLOCK_PROVISION_CMD,
  FWLOCK_CMD_LAST,
  FWLOCK_CMD_SIZE = 0x7FFFFFFF
} fwlock_cmd_id_t;

/** Commands TZ->HLOS   */
typedef enum {
	FWLOCK_HLOS_GET_DIP,
	FWLOCK_HLOS_SET_DIP,
	FWLOCK_HLOS_REQUEST_ACTIVATION,
	FWLOCK_HLOS_CMD_LAST,
	FWLOCK_HLOS_CMD_SIZE = 0x7FFFFFFF
} fwlock_hlos_cmd_id_t;

/*----------------------------------------------------------------------------
 * Macro Declarations
 * -------------------------------------------------------------------------*/

#define FWLOCK_TOKEN_LEN 16
#define FWLOCK_MAX_PIN_LEN 8
#define FWLOCK_MAX_BLOCKS 1024
#define FWLOCK_MAX_PARTITIONS 3
#define FWLOCK_MAX_PARTITION_NAME_LEN 100
#define FWLOCK_BLOCK_SIZE 1024*1024*16
#define HASH_LEN 32
#define HMAC_LEN 32
#define FWLOCK_MAX_PKCS7_MSG_LEN 2048
#define FWLOCK_MAX_CRL_LEN 2048
#define FWLOCK_DATA_FILE "/fwlock-persist.dat"
#define FWLOCK_CRL_FILE "/fwlock-crl.dat"

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/

#pragma pack(push, fwlock, 1)

/** Error types. */
typedef enum {
	FWLOCK_SUCCESS = 0,
	FWLOCK_FAILURE,
	FWLOCK_ERR_SIZE = 0x7FFFFFFF
} fwlock_status_t;

/** PKCS7 message types. */
typedef enum {
	FWLOCK_MSG_ACTIVATION = 0,
	FWLOCK_MSG_DEACTIVATION,
	FWLOCK_MSG_SIZE = 0x7FFFFFFF
} fwlock_msg_type_t;

/** PKCS7 message types. */
typedef enum {
	FWLOCK_MODE_SINGLE = 0,
	FWLOCK_MODE_BLOCK,
	FWLOCK_MODE_SIZE = 0x7FFFFFFF
} fwlock_mode_t;

/** Lock partition configuration. */
typedef struct fwlock_partition_cfg {
	char name[FWLOCK_MAX_PARTITION_NAME_LEN];		/* Partition name. */
	fwlock_mode_t hash_mode;						/* Single hash or hash per block. */ 
	uint8 verify_ratio; 							/* Statistically verify only given partition ratio (1-100). */
	uint8 force_verify_block[FWLOCK_MAX_BLOCKS];	/* Verify only given block numbers. */
} fwlock_partition_cfg_t;

/** Firmware Lock user authenticator mode. */
typedef enum {
    FWLOCK_AUTH_UI_PIN = 0,         /* Locally authenticate user using PIN entered via UI. */
    FWLOCK_AUTH_TUI_PIN,            /* Locally authenticate user using PIN entered via Trusted UI. */
    FWLOCK_AUTH_SIZE = 0x7FFFFFFF
} fwlock_local_auth_mode_t;

/** Firmware Lock user authenticator for local deactivation. */
typedef struct fwlock_local_auth {
    fwlock_local_auth_mode_t local_auth_mode;       /* User authenticator mode for local deactivation. */
    union {
      char local_auth_ui_pin[FWLOCK_MAX_PIN_LEN+1]; /* PIN provided by user, for local auhenticator of type FWLOCK_AUTH_UI_PIN. */
    } local_auth_data;
} fwlock_user_auth_t;

/** Lock Activation configuration. */
typedef struct fwlock_lock_cfg {
	uint8 num_partitions;   									/* Number of partitions to protect. */
	fwlock_partition_cfg_t partitions[FWLOCK_MAX_PARTITIONS]; 	/* Partitions lock config. */
	uint8 enable_local_deactivation;        					/* Allow local lock deactivation using a user authenticator. */
	fwlock_user_auth_t user_auth;          						/* User authenticator such as PIN. */
} fwlock_lock_cfg_t;

/** Lock Activation configuration. */
typedef struct fwlock_lock_rsp {
	uint8 status;
	uint8 partition_status[FWLOCK_MAX_PARTITIONS]; /* Partitions lock status. */
} fwlock_lock_rsp_t;

/** Lock Activation BLOB. */
typedef struct fwlock_activation_blob {
	uint32 len; 			/* BLOB length in bytes. */
	unsigned char* data;	/* BLOB data. */
} fwlock_activation_blob_t;

/** Lock Activation token */
typedef struct fwlock_token {
	unsigned char data[FWLOCK_TOKEN_LEN];   /* 16 bytes of token. */
} fwlock_token_t;

/** Lock Activation and Deactivation messages. */
typedef struct fwlock_activation_req_data {
    fwlock_msg_type_t msg_type;         /* 0 for activation, 1 for deactivation. */
    fwlock_token_t token;               /* Activation token. */
} fwlock_activation_req_data_t;

/** Format of the pkcs7_msg containing signed message. */
typedef struct fwlock_pkcs7_buf {
	unsigned char data[FWLOCK_MAX_PKCS7_MSG_LEN];
	uint32 len;
} fwlock_pkcs7_buf_t;

/** X509 CRL Buffer */
typedef struct fwlock_crl_buf {
	unsigned char data[FWLOCK_MAX_CRL_LEN];
	uint32 len;
} fwlock_crl_buf_t;

typedef enum {
	DIP_STATUS_DEACTIVATED = 0,
	DIP_STATUS_ACTIVATED,
	DIP_STATUS_SIZE = 0x7FFFFFFF
} dip_status_t;

typedef struct DIP_hash_table_entry {
	unsigned char hash[HASH_LEN];   		/* Hash on block. */
	uint8 force_verify_block;   			/* Force verification of this block. */
} DIP_hash_table_entry_t;

typedef struct DIP_partition_cfg {
	uint32 size;								/* Partition size. */
	char name[FWLOCK_MAX_PARTITION_NAME_LEN];   /* Partition name. */
	uint8 lock_enabled; 						/* Partition locked? */
	fwlock_mode_t hash_mode;					/* Hash per IMAGE or BLOCK. */
	uint32 total_num_blocks;					/* Hash table size in num of blocks. */
	uint32 verify_ratio;   						/* Statistically verify num of blocks. */
	DIP_hash_table_entry_t hash_table[FWLOCK_MAX_BLOCKS];   /* Hash table. */
} DIP_partition_cfg_t;

typedef struct DIP {
	uint32 version; 											/* DIP version. */
	dip_status_t status;										/* Activated/Deactivated. */
	DIP_partition_cfg_t partition_cfg[FWLOCK_MAX_PARTITIONS];   /* Config for each partition. */
	fwlock_token_t token;   									/* Activation_Token. */
	uint8 enable_local_deactivation;        					/* Allow local lock deactivation using a user authenticator. */
	fwlock_user_auth_t user_auth;								/* User authenticator such as PIN. */
	unsigned char hmac[HMAC_LEN];   							/* DIP integrity. */
} DIP_t;

typedef fwlock_lock_cfg_t lock_req_t;

typedef struct partition_hash_table {
	unsigned char hash[HASH_LEN];
} partition_hash_table_t;

typedef struct partition_rsp {
	uint32 status;
	uint32 size;
	uint32 hash_table_size;
	partition_hash_table_t hash_table[FWLOCK_MAX_BLOCKS];
} partition_rsp_t;

typedef struct lock_rsp {
	uint32 status;
	partition_rsp_t partition_rsp[FWLOCK_MAX_PARTITIONS]; 
} lock_rsp_t;

/*-------------------------------------------------------------------------*/

typedef struct fwlock_generic_req_s {
 fwlock_cmd_id_t cmd_id;
} fwlock_generic_req_t;

typedef struct fwlock_generic_rsp_s {
  fwlock_status_t ret;
} fwlock_generic_rsp_t;

/*-------------------------------------------------------------------------*/

typedef struct fwlock_gen_token_rsp_s {
	fwlock_status_t ret;
	fwlock_token_t token;
} fwlock_gen_token_rsp_t;

/*-------------------------------------------------------------------------*/

typedef struct fwlock_activate_req_s {
	fwlock_cmd_id_t cmd_id;
	fwlock_pkcs7_buf_t pkcs7_buf;
	fwlock_lock_cfg_t config;
} fwlock_activate_req_t;

typedef struct fwlock_activate_rsp_s {
	fwlock_status_t ret;
	DIP_t dip;
} fwlock_activate_rsp_t;

/*-------------------------------------------------------------------------*/

typedef struct fwlock_complete_activation_req_s {
	fwlock_cmd_id_t cmd_id;
	lock_req_t lock_req;
	lock_rsp_t lock_rsp;
} fwlock_complete_activation_req_t;

typedef struct fwlock_complete_activation_rsp_s {
	fwlock_status_t ret;
} fwlock_complete_activation_rsp_t;

/*-------------------------------------------------------------------------*/

typedef struct fwlock_deactivate_remote_req_s {
	fwlock_cmd_id_t cmd_id;
	fwlock_pkcs7_buf_t pkcs7_buf;
} fwlock_deactivate_remote_req_t;

/*-------------------------------------------------------------------------*/

typedef struct fwlock_deactivate_local_req_s {
	fwlock_cmd_id_t cmd_id;
	fwlock_user_auth_t user_auth;
} fwlock_deactivate_local_req_t;

/*-------------------------------------------------------------------------*/

typedef struct fwlock_update_isv_crl_req_s {
	fwlock_cmd_id_t cmd_id;
	fwlock_crl_buf_t isv_crl;
} fwlock_update_isv_crl_req_t;

/*-------------------------------------------------------------------------*/

typedef struct fwlock_get_dip_req_s {
	fwlock_hlos_cmd_id_t cmd_id;
} fwlock_get_dip_req_t;

typedef struct fwlock_get_dip_rsp_s {
	fwlock_status_t ret;
	DIP_t dip;
} fwlock_get_dip_rsp_t;

/*-------------------------------------------------------------------------*/

typedef struct fwlock_set_dip_req_s {
	fwlock_hlos_cmd_id_t cmd_id;
	DIP_t dip;
} fwlock_set_dip_req_t;

typedef struct fwlock_set_dip_rsp_s {
	fwlock_status_t ret;
} fwlock_set_dip_rsp_t;

/*-------------------------------------------------------------------------*/

typedef struct fwlock_request_activation_req_s {
	fwlock_hlos_cmd_id_t cmd_id;
	lock_req_t lock_req;
} fwlock_request_activation_req_t;

typedef struct fwlock_request_activation_rsp_s {
	fwlock_status_t ret;
} fwlock_request_activation_rsp_t;

/*-------------------------------------------------------------------------*/

#pragma pack(pop, fwlock)

#define FWLOCK_FUNC(id, func, req_len, rsp_len) \
		{(id), (func), sizeof(req_len), sizeof(rsp_len)}

typedef void (*fwlock_func)(void* cmd, void* rsp);

typedef struct fwlock_function_s {
  fwlock_cmd_id_t cmd_id;
  fwlock_func func;
  uint32 req_len;
  uint32 rsp_len;
} fwlock_function_t;

typedef struct fwlock_sfs_entry {
	uint32 session_token_generated;
	fwlock_token_t session_token;
} fwlock_sfs_entry_t;

typedef struct fwlock_buffer {
    size_t len;
    unsigned char* data;
} fwlock_buffer_t;

void fwlock_gen_token(void *cmd, void *rsp);
void fwlock_activate(void *cmd, void *rsp);
void fwlock_complete_activation(void *cmd, void *rsp);
void fwlock_deactivate_remote(void *cmd, void *rsp);
void fwlock_deactivate_local(void *cmd, void *rsp);
void fwlock_update_isv_crl(void *cmd, void *rsp);
void fwlock_provision(void *cmd, void *rsp);

int fwlock_gen_token_impl(fwlock_token_t *token);
int fwlock_activate_impl(
	fwlock_pkcs7_buf_t *pkcs7_buf,
	fwlock_lock_cfg_t *config,
	DIP_t *dip);
int fwlock_complete_activation_impl(lock_req_t *lock_req, lock_rsp_t *lock_rsp);
int fwlock_deactivate_remote_impl(fwlock_pkcs7_buf_t *pkcs7_buf);
int fwlock_deactivate_local_impl(fwlock_user_auth_t *user_auth);
int fwlock_update_isv_crl_impl(fwlock_crl_buf_t *isv_crl_buf);
int fwlock_provision_impl(void);
int fwlock_auth_verify_isv_msg(fwlock_pkcs7_buf_t *pkcs7_buf, fwlock_buffer_t *data_buf, fwlock_crl_buf_t *isv_crl_buf);

#endif /* FWLOCK_H */
