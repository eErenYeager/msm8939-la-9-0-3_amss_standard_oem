#===============================================================================
#
# QSEE Macchiato lib
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
# Qualcomm Technologies Proprietary and Confidential.
#
#-------------------------------------------------------------------------------
#
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 5/28/14	 sn
#===============================================================================
Import('env')
env = env.Clone()

#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------
SRCPATH = "${BUILD_ROOT}/core/securemsm/trustzone/qsapps/keyprov/macchiato/src"
env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

#-------------------------------------------------------------------------------
# External depends within CoreBSP
#-------------------------------------------------------------------------------
CBSP_API = [
   'SERVICES',

   # needs to be last also contains wrong comdef.h
   'KERNEL',
]

env.RequirePublicApi(CBSP_API)

#-------------------------------------------------------------------------------
# Internal depends within CoreBSP
#-------------------------------------------------------------------------------
CBSP_API = [
   'SECUREMSM',

   # needs to be last also contains wrong comdef.h
   'KERNEL',
]
env.RequireRestrictedApi(CBSP_API)

#-------------------------------------------------------------------------------
# Publish Private APIs
#-------------------------------------------------------------------------------

env.PublishPrivateApi('SECUREMSM', [
   "${INC_ROOT}/core/securemsm/trustzone/qsapps/keyprov/macchiato/inc",
   "${INC_ROOT}/core/api/securemsm/trustzone/qsee",
   "${INC_ROOT}/core/api/kernel/libstd/stringl",
   "${INC_ROOT}/core/api/services",
   "${INC_ROOT}/core/api/boot/qfprom",
   "${INC_ROOT}/core/securemsm/unifiedcrypto/core/ecc/inc",
   "${INC_ROOT}/core/securemsm/unifiedcrypto/shared/ecc/inc",
   "${INC_ROOT}/core/securemsm/unifiedcrypto/shared/aes/inc",
   "${INC_ROOT}/core/securemsm/unifiedcrypto/core/aes/inc",
   "${INC_ROOT}/core/securemsm/unifiedcrypto/shared/sha/inc",
   "${INC_ROOT}/core/securemsm/unifiedcrypto/core/sha/inc",
   "${INC_ROOT}/core/securemsm/unifiedcrypto/core/ecc/inc",
   "${INC_ROOT}/core/securemsm/unifiedcrypto/shared/ecc/inc",
   "${INC_ROOT}/core/securemsm/unifiedcrypto/environment/inc",
])

#-------------------------------------------------------------------------------
# Sources, libraries
#-------------------------------------------------------------------------------

SOURCES = [
  '${BUILDPATH}/macchiato.c',
  '${BUILDPATH}/macchiato_ecc.c',
  '${BUILDPATH}/macchiato_service.c',
  '${BUILDPATH}/macchiato_test.c',
  '${BUILDPATH}/macchiato_eciesad.c',
]

#-------------------------------------------------------------------------------
# Add Libraries to image
#-------------------------------------------------------------------------------
env.AddBinaryLibrary([
 'MACCHIATO_SAMPLE_IMAGE', 'TQS_IMAGE',
 ],
 '${BUILDPATH}/macchiato',
 SOURCES)

#-------------------------------------------------------------------------------
# Pack out files
#-------------------------------------------------------------------------------
NOSHIP_SOURCES = env.FindFiles("*", SRCPATH)
env.CleanPack('MACCHIATO_SAMPLE_IMAGE', NOSHIP_SOURCES)

