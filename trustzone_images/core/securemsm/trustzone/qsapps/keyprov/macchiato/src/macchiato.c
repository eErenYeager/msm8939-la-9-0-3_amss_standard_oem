/** @file macchiato.c
 *    @brief
 * This file contains the implementations of the QSEE Macchiato key 
 * provisioning lib functions.
 */

/*===========================================================================
 Copyright (c) 2014 Qualcomm Technologies, Inc.
 All Rights Reserved.
 Qualcomm Technologies Proprietary and Confidential.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

when       who     what, where, why
--------   ---     ----------------------------------------------------------
01/06/15   pa      Added check for integer overflow for arithmatic expression (CR#739866)
09/24/14   cz      Do not check secondary HW key bit
08/14/14   qxu     Fixed return errors
05/27/14   sn      Initial Version.

===========================================================================*/


#include "qsee_core.h"
#include "qsee_log.h"
#include "qsee_heap.h"
#include "qsee_prng.h"
#include <stringl.h>
#include "macchiato.h"
#include "macchiato_service.h"
#include "qsee_fuse.h"
#include "qfprom.h"
#include <stdint.h>

#ifdef ENABLE_QSEE_LOG_MSG_DEBUG
#undef ENABLE_QSEE_LOG_MSG_DEBUG
#define ENABLE_QSEE_LOG_MSG_DEBUG 1
#endif

#define ENTER     QSEE_LOG(QSEE_LOG_MSG_DEBUG, "%s+", __func__)
#define EXIT      QSEE_LOG(QSEE_LOG_MSG_DEBUG, "%s-", __func__)
#define EXITV(x)  { QSEE_LOG(QSEE_LOG_MSG_DEBUG, "%s- (%d)", __func__, (x));return (x); }

#define MACCHIATO_RSA_SIG_LEN              64
#define MACCHIATO_PUB_KEY_LEN              64

#define MACCHIATO_PROTOCOL_VERSION      1
#define MACCHIATO_AUTHENTICATE_MSG_ID   1

#define MACCHIATO_ERROR_CODE_SUCCESS                       0x00
#define MACCHIATO_ERROR_CODE_APPS_SECURE_NOT_SET           0x01
#define MACCHIATO_ERROR_CODE_MSA_SECURE_NOT_SET            0x02
#define MACCHIATO_ERROR_CODE_UNSUPPORTED_PROTOCOL_VERSION  0x04
#define MACCHIATO_ERROR_CODE_UNSUPPORTED_MSG_ID            0x08
#define MACCHIATO_ERROR_CODE_APPS_MSA_SECURE_NOT_SET       0x10
#define MACCHIATO_ERROR_CODE_MACCHIATO_FUSE_NOT_SET        0x20

#define MACCHIATO_ENABLE_BIT_ADDR   (0xFC4B80A0)
#define MACCHIATO_ENABLE_BIT_MASK   (0x10000000)

/**
 *  We take the 3 least significant bits, in the security state for setting the APPS SECURE NOT SET flag in the error code:
 *  0. Secboot enabling check failed.
 *  1. Sec HW key is not programmed.
 *  2. Debug disable check failed.
 *
 **/
#define APPS_SECURE_STATE_MASK 0x05

static uint8_t response[MACCHIATO_CHALLENGE_RESPONSE_LEN] = {0};

#pragma pack (push, macchiato, 1)


struct macchiato_wrapped_key_msg_tail{
	uint8_t SigRSA[MACCHIATO_RSA_SIG_LEN];
	uint8_t response[MACCHIATO_CHALLENGE_RESPONSE_LEN];
	uint8_t encWrappingKey[MACCHIATO_PUB_KEY_LEN];
};


#pragma pack (pop, macchiato)

/**
 @brief this API will be deprecated. 
 macchiato_generate_keys
 Triggers Macchiato key pair generation.

*/
macchiato_err_t macchiato_generate_keys(){
	EXITV(MACCHIATO_SUCCESS);
}

/**
 * @brief  Check if Macchiato bit is set
 *         MACCHIATO_EN[0] � Fuse to denote that the Macchiato Feature
 *         has been enabled.
 *         Qfprom Raw: 20, Address: FC4B80A0, Bit: 28
 *
 * @param[in] NONE
 * @return TRUE if supported, FALSE if not supported
*/
static boolean macchiato_is_enabled(void)
{
  uint32 qfprom_status = QFPROM_NO_ERR;
  uint32 row_data_read = 0;

  /*Read the fuse value*/
  qsee_fuse_read(MACCHIATO_ENABLE_BIT_ADDR, QFPROM_ADDR_SPACE_RAW, (uint32*)&row_data_read, &qfprom_status);
  if(qfprom_status != QFPROM_NO_ERR)
  {
    MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_is_enabled() qsee_fuse_read returned error: %d", qfprom_status);
    return FALSE;
  }
  else
  {
    MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_is_enabled() qsee_fuse_read value: %x", (uint32)row_data_read);
  }

  if ( row_data_read & MACCHIATO_ENABLE_BIT_MASK )
  {
    return TRUE;
  }

  return FALSE;
}

/**
  @brief macchiato_get_msg_header_length
  Internal function to get the length of the Macchiato message, not including the service opaque data.
*/
macchiato_err_t macchiato_get_msg_header_length(uint32_t * pu32SMsgHeaderLen){


	uint32_t signature_len = 0;
	macchiato_err_t retVal;
	*pu32SMsgHeaderLen = 0;

	ENTER;

	/* we use the function for signing with zero length buffer to get the required length */
	retVal = (macchiato_err_t)macchiato_service_ecc_sign(NULL, 0, NULL, pu32SMsgHeaderLen, NULL, &signature_len);
	if(retVal != MACCHIATO_TOO_SHORT_OUT_BUFFER_ERROR){
		QSEE_LOG(QSEE_LOG_MSG_ERROR, "macchiato_service_rsa_sign returned unexpected value: %d instead of %d", retVal, MACCHIATO_TOO_SHORT_OUT_BUFFER_ERROR);
		EXITV(retVal);
	}

	if(signature_len != MACCHIATO_RSA_SIG_LEN){
		QSEE_LOG(QSEE_LOG_MSG_ERROR, "mismatch in signature length: expected: %d, got: %d (protocol version = %d)", MACCHIATO_RSA_SIG_LEN, signature_len, MACCHIATO_PROTOCOL_VERSION);
		EXITV(MACCHIATO_PROTOCOL_VERSION_MISMATCH);
	}

	/* add the fields that are added by this component */
	*pu32SMsgHeaderLen += sizeof(struct macchiato_msg_hdr_common_fields) + MACCHIATO_RSA_SIG_LEN + 2*MACCHIATO_CHALLENGE_RESPONSE_LEN;

	EXITV(MACCHIATO_SUCCESS);
}

/*****************************************************************************************************************/

/**
   @brief
   Internal function used to fill the Macchiato message buffer with the common fields:
     ------------------------------------------------------
	|      32 bits     | 32 bits |   32 bits  |   32 bits  |
	|------------------|---------|------------|------------|
	| Protocol Version | MSG ID  | MSG length | Error Code |
	 ------------------------------------------------------
*/
static macchiato_err_t write_macchiato_msg_hdr_common_fields(struct macchiato_msg_hdr_common_fields * common_fields, uint32_t msg_id, uint32_t protocol_version, uint8_t unexpected_msg_id){

	qsee_secctrl_secure_status_t security_status;
	int32_t retVal;

	ENTER;

	common_fields->u32ProtocolVersion = MACCHIATO_PROTOCOL_VERSION;
	common_fields->u32MsgId = msg_id;
	common_fields->u32MsgLength = sizeof(struct macchiato_msg_hdr_common_fields); /* initial value */

	/* Error code */
	common_fields->u32ErrorCode = MACCHIATO_ERROR_CODE_SUCCESS;

	/* get the security state */
	retVal = qsee_get_secure_state(&security_status);
	if(retVal < 0){
		QSEE_LOG(QSEE_LOG_MSG_ERROR, "qsee_get_secure_state returned %d", retVal);
		EXITV(MACCHIATO_FAILED_GET_SECURE_STATE);
	}

	/* taking only the bits that define the APPS SECURE SET flag in the error code */
	if(security_status.value[0] & APPS_SECURE_STATE_MASK){
		common_fields->u32ErrorCode |= MACCHIATO_ERROR_CODE_APPS_MSA_SECURE_NOT_SET;
	}

    /* check if protocol version is unsupported */
	if(protocol_version != MACCHIATO_PROTOCOL_VERSION){
		common_fields->u32ErrorCode |= MACCHIATO_ERROR_CODE_UNSUPPORTED_PROTOCOL_VERSION;
	}

    /* check if msg iD is unsupported */
	if(unexpected_msg_id){
		common_fields->u32ErrorCode |= MACCHIATO_ERROR_CODE_UNSUPPORTED_MSG_ID;
	}

    /* check if macchiato feature is enabled */
    if ( macchiato_is_enabled() == FALSE )
    {
		common_fields->u32ErrorCode |= MACCHIATO_ERROR_CODE_MACCHIATO_FUSE_NOT_SET;
    }

	if(common_fields->u32ErrorCode != MACCHIATO_ERROR_CODE_SUCCESS){
		QSEE_LOG(QSEE_LOG_MSG_ERROR, "got unsuccessful error code %d in Macchiato message", common_fields->u32ErrorCode);
		//EXITV(MACCHIATO_GOT_UNSUCCESSFUL_ERROR_CODE);
	}

	EXITV(MACCHIATO_SUCCESS);
}
/*****************************************************************************************************************/

/**
   @brief
   Internal function used to fill the Macchiato message buffer with some input buffer to be signed.

*/
static macchiato_err_t create_signed_macchiato_msg(uint32_t protocol_version, uint32_t msg_id, uint8_t unexpected_msg_id, buffer_t input, buffer_t msg, uint32_t* msg_len){
	uint32_t signed_data_len, sig_len = MACCHIATO_RSA_SIG_LEN;
	struct macchiato_msg_hdr_common_fields * common_fields;
	macchiato_err_t ret_val;

	ENTER;

	if(NULL == input.pu8Buffer || NULL == msg.pu8Buffer || NULL == msg_len){
		QSEE_LOG(QSEE_LOG_MSG_ERROR, "create_signed_macchiato_msg got null input, msg.pu8Buffer=%d, msg.pu8Buffer=%d, msg_len =%d ", input.pu8Buffer, msg.pu8Buffer, msg_len);
		EXITV(MACCHIATO_GOT_NULL_INPUT_ERROR);
	}

	/* check buffer size for the common part of the signed message */
	if(msg.u32Len < sizeof(struct macchiato_msg_hdr_common_fields)){
		QSEE_LOG(QSEE_LOG_MSG_ERROR, "create_signed_macchiato_msg reported too short buffer, msg.u32Len = %d, needed for the common fields: %d"
				", not including the signature output (for Macchiato protocol version %d)", msg.u32Len, sizeof(struct macchiato_msg_hdr_common_fields), MACCHIATO_PROTOCOL_VERSION);
		EXITV(MACCHIATO_TOO_SHORT_OUT_BUFFER_ERROR);
	}

	common_fields = (struct macchiato_msg_hdr_common_fields *) msg.pu8Buffer;

	/* write the common part of the message */
	ret_val = write_macchiato_msg_hdr_common_fields(common_fields, msg_id, protocol_version, unexpected_msg_id);
	if(MACCHIATO_SUCCESS != ret_val){
		QSEE_LOG(QSEE_LOG_MSG_ERROR, "write_macchiato_msg_hdr_common_fields returned %d", ret_val);
		EXITV(ret_val);
	}

	msg.pu8Buffer += sizeof(struct macchiato_msg_hdr_common_fields);
	msg.u32Len -= sizeof(struct macchiato_msg_hdr_common_fields);

	if(msg.u32Len < MACCHIATO_RSA_SIG_LEN){
		QSEE_LOG(QSEE_LOG_MSG_ERROR, "create_signed_macchiato_msg reported too short buffer, after common fields msg.u32Len = %d, needed for signature: %d"
				", not including the signed data (for Macchiato protocol version %d)", msg.u32Len, MACCHIATO_RSA_SIG_LEN, MACCHIATO_PROTOCOL_VERSION);
		EXITV(MACCHIATO_TOO_SHORT_OUT_BUFFER_ERROR);
	}

	/* add the signed data and the signature to the message */
	signed_data_len = msg.u32Len - MACCHIATO_RSA_SIG_LEN; /* maximum length of the signed data */

	ret_val = (macchiato_err_t)macchiato_service_ecc_sign(input.pu8Buffer, input.u32Len, msg.pu8Buffer + MACCHIATO_RSA_SIG_LEN, &signed_data_len, msg.pu8Buffer, &sig_len);
	if(MACCHIATO_SUCCESS != ret_val){
		if(MACCHIATO_TOO_SHORT_OUT_BUFFER_ERROR == ret_val){
			EXITV(ret_val);
		}
		QSEE_LOG(QSEE_LOG_MSG_ERROR, "macchiato_rsa_sign returned %d", ret_val);
		EXITV(ret_val);
	}

	if(sig_len != MACCHIATO_RSA_SIG_LEN){
		QSEE_LOG(QSEE_LOG_MSG_ERROR, "got signature length = %d, should be %d in Macchiato protocol version %d", sig_len, MACCHIATO_RSA_SIG_LEN, MACCHIATO_PROTOCOL_VERSION);
		EXITV(ret_val);
	}

	/* MSG length */
	common_fields->u32MsgLength += signed_data_len + sig_len;
	*msg_len = common_fields->u32MsgLength;

	QSEE_LOG(QSEE_LOG_MSG_DEBUG, "create_signed_macchiato_msg finished successfully for msg id %d, msg len = %d", msg_id, common_fields->u32MsgLength);
	EXITV(MACCHIATO_SUCCESS);
}
/*****************************************************************************************************************/

/**
@brief macchiato_authenticate_device
Use this function to authenticate the device using challenge response with Machhiato private key.

*/
macchiato_err_t macchiato_authenticate_device(buffer_t challenge, buffer_t opaqueData, buffer_t signedChallengeResponse, uint32_t* pu32SignedChallengeResponseLen){
	uint32_t input_idx = 0, response_len, header_len = 0;
	buffer_t input;
	macchiato_err_t ret_val = MACCHIATO_GENERAL_ERROR;
	struct macchiato_challenge_msg * challenge_msg = (struct macchiato_challenge_msg *) challenge.pu8Buffer;
	uint8_t unexpected_msg_id = FALSE;

	ENTER;

	if(NULL == challenge.pu8Buffer || NULL == opaqueData.pu8Buffer || NULL == signedChallengeResponse.pu8Buffer || NULL == pu32SignedChallengeResponseLen){
		QSEE_LOG(QSEE_LOG_MSG_ERROR, "macchiato_authenticate_device got null input, challenge.pu8Buffer=%d, opaqueData.pu8Buffer=%d, signedChallengeResponse.pu8Buffer =%d, pu32SignedChallengeResponseLen =%d ",
				challenge.pu8Buffer, opaqueData.pu8Buffer, signedChallengeResponse.pu8Buffer, pu32SignedChallengeResponseLen);
		EXITV(MACCHIATO_GOT_NULL_INPUT_ERROR);
	}

	ret_val = macchiato_get_msg_header_length(&header_len);
	if(ret_val != MACCHIATO_SUCCESS){
		QSEE_LOG(QSEE_LOG_MSG_ERROR, "macchiato_get_msg_header_length returned %d", ret_val);
		EXITV(ret_val);
	}
    if ((opaqueData.u32Len > 0) && (header_len > UINT32_MAX - opaqueData.u32Len))  /* `header_len + opaqueData.u32Len` would overflow */
	{
	    QSEE_LOG(QSEE_LOG_MSG_ERROR, "'header_len + opaqueData.u32Len' resulting in integer overflow");
		EXITV(MACCHIATO_INVALID_INPUT_ERROR);
	}

	/* check the outout buffer size */
	if(header_len + opaqueData.u32Len > signedChallengeResponse.u32Len)
	{
		*pu32SignedChallengeResponseLen = header_len + opaqueData.u32Len;
		EXITV(MACCHIATO_TOO_SHORT_OUT_BUFFER_ERROR);
	}

	if(challenge.u32Len != sizeof(struct macchiato_challenge_msg)){
		QSEE_LOG(QSEE_LOG_MSG_ERROR, "macchiato_authenticate_device got invalid challenge length = %d, should be %d for Macchiato protocol version %d",
				challenge.u32Len, sizeof(struct macchiato_challenge_msg), MACCHIATO_PROTOCOL_VERSION);
		EXITV(MACCHIATO_INVALID_INPUT_ERROR);
	}

    if ((opaqueData.u32Len > 0) && ((MACCHIATO_CHALLENGE_RESPONSE_LEN * 2) > UINT32_MAX - opaqueData.u32Len)) /* `(MACCHIATO_CHALLENGE_RESPONSE_LEN * 2) + opaqueData.u32Len` would overflow */
	{
	    QSEE_LOG(QSEE_LOG_MSG_ERROR, "'(MACCHIATO_CHALLENGE_RESPONSE_LEN * 2) + opaqueData.u32Len' resulting in integer overflow");
		EXITV(MACCHIATO_INVALID_INPUT_ERROR);
	}
	input.u32Len = MACCHIATO_CHALLENGE_RESPONSE_LEN * 2 + opaqueData.u32Len; /* challenge + response + opaque data */
	input.pu8Buffer = (uint8_t*) qsee_malloc(input.u32Len);
	if(NULL == input.pu8Buffer){
		QSEE_LOG(QSEE_LOG_MSG_ERROR, "qsee_malloc failed for input_buffer of length", input.u32Len);
		EXITV(MACCHIATO_ALLOCATION_ERROR);
	}

	do {

		/* Challenge */
		memscpy(input.pu8Buffer + input_idx, input.u32Len - input_idx, challenge_msg->challenge, MACCHIATO_CHALLENGE_RESPONSE_LEN);
		input_idx += MACCHIATO_CHALLENGE_RESPONSE_LEN;

		/* Response */
		response_len = qsee_prng_getdata(response, MACCHIATO_CHALLENGE_RESPONSE_LEN);
		if(response_len != MACCHIATO_CHALLENGE_RESPONSE_LEN){
			QSEE_LOG(QSEE_LOG_MSG_ERROR, "qsee_prng_getdata failed, returned %d, expected is %d", response_len, MACCHIATO_CHALLENGE_RESPONSE_LEN);
			ret_val = MACCHIATO_FAILED_GENERATE_RANDOM_RESPONSE;
			break;
		}
		memscpy(input.pu8Buffer + input_idx, input.u32Len - input_idx, response, response_len);

		input_idx += response_len;

		/* Opaque Data */
		memscpy(input.pu8Buffer + input_idx, input.u32Len - input_idx, opaqueData.pu8Buffer, opaqueData.u32Len);
		ret_val = create_signed_macchiato_msg(MACCHIATO_PROTOCOL_VERSION, MACCHIATO_AUTHENTICATE_MSG_ID, unexpected_msg_id, input, signedChallengeResponse, pu32SignedChallengeResponseLen);
	} while(0);

	qsee_free(input.pu8Buffer);
	EXITV(ret_val);

}
/*****************************************************************************************************************/

/**
@brief macchiato_sign_service_data
Use this function to sign service data with the device Machhiato private key.

*/
macchiato_err_t macchiato_sign_service_data(buffer_t serviceData, buffer_t signedServiceData, uint32_t* pu32SignedServiceDataLen){
	macchiato_err_t ret_val;
	buffer_t zero_challenge;
	uint8_t challenge_buffer[MACCHIATO_CHALLENGE_RESPONSE_LEN] = {0};

	ENTER;

	zero_challenge.pu8Buffer = &challenge_buffer[0];
	zero_challenge.u32Len = MACCHIATO_CHALLENGE_RESPONSE_LEN;

	ret_val = macchiato_authenticate_device(zero_challenge, serviceData, signedServiceData, pu32SignedServiceDataLen);
	EXITV(ret_val);

}
/*****************************************************************************************************************/

/**
@brief macchiato_provision_service_key
Use this function to get the unwrapped service key from the Macchiato provisioning message.

*/
macchiato_err_t macchiato_provision_service_key(buffer_t provisionMsg,
												buffer_t unwrappedKey,
												uint32_t* pu32UnwarppedKeyLen){

	/*struct macchiato_wrapped_key_msg_head* msg_head = (struct macchiato_wrapped_key_msg_head *)provisionMsg.pu8Buffer;
	struct macchiato_wrapped_key_msg_tail* msg_tail;

	ENTER;

	if(NULL == provisionMsg.pu8Buffer || NULL == unwrappedKey.pu8Buffer || NULL == pu32UnwarppedKeyLen){
		QSEE_LOG(QSEE_LOG_MSG_ERROR, "macchiato_provision_service_key got null input, "
										"provisionMsg.pu8Buffer=%d, unwrappedKey.pu8Buffer = %d, pu32UnwarppedKeyLen = %d",
										provisionMsg.pu8Buffer, unwrappedKey.pu8Buffer, pu32UnwarppedKeyLen);
		EXITV(MACCHIATO_GOT_NULL_INPUT_ERROR);
	}
	if(MACCHIATO_PROTOCOL_VERSION != msg_head->hdr.u32ProtocolVersion){
		return MACCHIATO_PROTOCOL_VERSION_MISMATCH;
	}

	if(msg_head->hdr.u32MsgId )*/



	EXITV(MACCHIATO_SUCCESS);
}
/*****************************************************************************************************************/
