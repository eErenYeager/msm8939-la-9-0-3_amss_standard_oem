/*===========================================================================
  @file secrsa_signature.c
  
   This file provides RSA PKCS #1 v1.5 and v2.0 PSS hash signature 
   generation/verification implementation.
  ===========================================================================

                           EDIT HISTORY FOR FILE
  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsapps/keyprov/macchiato/src/macchiato_test.c#1 $
  $DateTime: 2018/02/07 00:37:16 $ 
  $Author: mplp4svc $

  when       who     what, where, why
  --------   ---     ----------------------------------------------------------
  09/24/14   cz      Added hw/sw key AUTO detection
  05/27/14   qxu     initial version

  Copyright (c) 2011 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
  =============================================================================*/

/*===========================================================================

                             INCLUDE FILES

===========================================================================*/
#include "comdef.h"
#include "macchiato_service.h"
#include "macchiato.h"
#include "libstd/stringl/stringl.h"
#include "qsee_heap.h"
#include "qsee_timer.h"
#include "qsee_prng.h"
#include "macchiato_eciesad.h"
#include "ecc_core.h"
#include "qsee_ecc.h"

#define TEST_USE_PHK_SIGNING_KEY

extern unsigned int macchiato_get_svcid_num(void);
extern void bigval_to_binary(bigval_t const *src, void *tgt, size_t tgtlen);
extern void
macc_bigval_to_binary(MACC_KEY_TYPE const *src, void *tgt, size_t tgtlen);
extern void
macc_binary_to_bigval(const void* src, MACC_KEY_TYPE *tgt, size_t srclen);

int check_number_uint8_t(const uint8_t * result, const uint8_t * pre, size_t len)
{
	size_t i;

    for (i = 0; i < len; i ++)
    {
        if (result[i] != pre[i])
        return FALSE;
    }
    return TRUE;
}

void output_S_BYTE(unsigned char *data, int len)   
{
  int x;
  char str[10240];
  if (len == 0)
  {
    return;
  }
  memset(str, 0, sizeof(str));
  for (x = 0; x < len; x++) {
	  snprintf(str, sizeof(str), "%s%02x ", str, *((unsigned char *)(data + x)));
    if ((x % 32) == 31) {
	  MACC_LOG(MACC_LOG_MSG_ERROR, "%s", str);
	  str[0] = 0;
	}
  }        
  if ((x % 32) != 0 ) {
    MACC_LOG(MACC_LOG_MSG_ERROR, "%s", str);
  }
}

void output_S_UINT32(uint32 *data, int len)   
{
  int x;
  char str[10240];
  memset(str, 0, sizeof(str));
  for (x = 0; x < len; x++) {
	  snprintf(str, sizeof(str), "%s%08x ", str, *((uint32_t *)(data + x)));
    if ((x % 8) == 7) {
	  MACC_LOG(MACC_LOG_MSG_ERROR, "%s", str);
	  str[0] = 0;
	}
  }        
  if ((x % 8) != 0 ) {
    MACC_LOG(MACC_LOG_MSG_ERROR, "%s", str);
  }
}

/*===========================================================================

                            FUNCTION DEFINITIONS

===========================================================================*/
#if 0
int macchiato_perf_test(void)
{
  int ret_val = QSEE_MACCHIATO_SUCCESS;
  int i = 0;
  unsigned long long total_time;
 //  char *ptr = NULL;
 //unsigned char pub_exp[] = {0x00, 0x00, 0x01, 0x00, 0x01}; /* Default public exponent 65537 */

  MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_perf_test() performance testing start...\n");

#if 0
  if ( ce_util_init_s_bigint(&(Macc_Sign_Key.e)) ||
	ce_util_init_s_bigint(&(Macc_Sign_Key.d)) || 
	ce_util_init_s_bigint(&(Macc_Sign_Key.N)) ||
	ce_util_init_s_bigint(&(Macc_Sign_Key.p)) ||
	ce_util_init_s_bigint(&(Macc_Sign_Key.q)) ) {
    MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_perf_test() rsa key initialization failure and returns %d\n", ret_val);
    return QSEE_MACCHIATO_ERROR;
  }
  MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_perf_test() rsa key initialization sucessful\n");
#endif

  macc_key_seed = qsee_malloc(32);
	if (!macc_key_seed)
	{
		MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_perf_test() malloc failure and returns %d\n", ret_val);
		return QSEE_MACCHIATO_ERROR;
	}

  do {
    qsee_prng_getdata(macc_key_seed, 32);
//  	MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_perf_test() key seed %s\n", key256_rand_seed);  
    total_time = qsee_get_uptime();
	  /* calling macchiato_rsa_key_gen with a key != NULL initializes the PRNG automatically */
	  /* otherwise it reuses the old state */
	  ret_val = macchiato_service_gen_key ();
	  if (ret_val)
	  {
	    MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_perf_test() call fail for interation %u returns %d\n", i, ret_val);
		  break;
	  }
//	  MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_perf_test() call is successful and returns %d\n", ret_val);
    total_time = qsee_get_uptime() - total_time;
    MACC_LOG(MACC_LOG_MSG_ERROR, " %u ms ", total_time);
//	  MACC_LOG(MACC_LOG_MSG_ERROR, "N = ");
//    output_S_BIGINT(rsa_key.N);
//	  for (j=0;j<32;j++) key256_rand_seed[i]++;  
	}while (1);
	 
	macchiato_service_destroy_key();
	qsee_free(macc_key_seed);
  return ret_val;
}
#endif
int macchiato_ecc_test(void)
{
  int ret_val = MACCHIATO_SUCCESS;
  unsigned char opaque_data[] = 
	  "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do "
	  "eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim "
	  "ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut "
	  "aliquip ex ea commodo consequat. Duis aute irure dolor in "
	  "reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla "
	  "pariatur. Excepteur sint occaecat cupidatat non proident, sunt in "
	  "culpa qui officia deserunt mollit anim id est laborum.";
  unsigned int opaque_data_len = /*sizeof(opaque_data)*/strlen((const char*)&opaque_data[0]);
  unsigned char *signed_data = opaque_data;
  unsigned int signed_data_len = opaque_data_len;
  unsigned char *signature = NULL;
  unsigned int signature_len = 0;

  MACC_KEY_TYPE privkey_sign, privkey_enc;
  uint8 marshalled_key[BIGLEN*4];
  static const uint8_t signing_privkey_marshalled_1[32] = 
		{0x3a,0x3e,0x52,0x86, 0xfa,0x95,0x1e,0x4e,
		 0xa6,0x36,0x0f,0x41, 0xda,0x8b,0x4d,0x65,
		 0x3a,0x1c,0x97,0xb3, 0x4d,0xaf,0x58,0x49,
		 0x40,0xcf,0x34,0xea, 0x79,0x4c,0x0b,0x73};

  static const uint8_t encryption_privkey_marshalled_1[32] = 
		{0xb7,0x9d,0x21,0xe1, 0x33,0x36,0xbd,0x94,
		 0xfd,0x90,0x31,0xe0, 0x2c,0x02,0xcc,0x93,
		 0xfe,0xbc,0x7c,0x1a, 0x00,0x5e,0x0f,0x57,
		 0xb7,0xb7,0xa6,0x8a, 0x60,0xf0,0x1e,0x72};

  do
  {
  	/*generate macchiato signing key*/
  	ret_val = qsee_macc_gen_ecc_privkey(MACCHIATO_KEY_TYPE_ECC_AUTO_SIGNING, &privkey_sign, sizeof(MACC_KEY_TYPE));
  	if (ret_val)
  	{
  	  MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_ecc_test() macchiato signing key generation failure and returns %d\n", ret_val);
  	  break;
  	}
     
  	macc_bigval_to_binary(&privkey_sign, marshalled_key, 32);
  	MACC_LOG(MACC_LOG_MSG_ERROR, "sig key = ");
  	output_S_BYTE(marshalled_key, 32);
     
  	if (! check_number_uint8_t(marshalled_key,signing_privkey_marshalled_1,32))
  	{
  	  MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_ecc_test(): Macchiato signing private key wrong!\n");
  	  break;
  	}
     
  	/*generate macchiato signing key*/
  	ret_val = qsee_macc_gen_ecc_privkey(MACCHIATO_KEY_TYPE_ECC_AUTO_ENCRYPTION, &privkey_enc, sizeof(MACC_KEY_TYPE));
  	if (ret_val)
  	{
  	  MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_ecc_test(): macchiato signing key generation failure and returns %d\n", ret_val);
  	  break;
  	}
     
  	macc_bigval_to_binary(&privkey_enc, marshalled_key, 32);
  	MACC_LOG(MACC_LOG_MSG_ERROR, "enc key = ");
  	output_S_BYTE(marshalled_key, 32); 
     
  	if (! check_number_uint8_t(marshalled_key,encryption_privkey_marshalled_1,32))
    {
  	  MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_ecc_test(): Macchiato encryption private key wrong!\n");
  	  break;
    }
		 
  	signature = qsee_malloc(64);
  	signature_len = 64;
  	if (!signature)
  	{
  	  MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_ecc_test(): malloc failure\n");
  	  ret_val = MACCHIATO_ALLOCATION_ERROR;
  	  break;
  	}

  	ret_val = macchiato_ecc_sign(&privkey_sign, signed_data, signed_data_len, signature, &signature_len);
  	if (ret_val)
  	{
  	  MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_ecc_test(): ecc sign failure and returns %d\n", ret_val);
  	  break;
  	}
     
  	MACC_LOG(MACC_LOG_MSG_DEBUG, "macchiato_ecc_test(): signed data total len %d, signature len %d", 
  						 signed_data_len, signature_len);
     
  	MACC_LOG(MACC_LOG_MSG_DEBUG, "macchiato_ecc_test(): signature - length %d", signature_len);
  	output_S_BYTE(signature, signature_len);
     
    ret_val = macchiato_ecc_verify(&privkey_sign, signed_data, signed_data_len, signature);
    if(ret_val)
    {
  	  MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_ecc_test(): verify Signature failure and returns %d\n", ret_val);
  	  break;
    }
     
  	MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_ecc_test(): verify Signature result matches");  
  } while (0); 
     
  if (signature)
  {
  	qsee_free(signature);
    signature = NULL;
  }
     
  return ret_val;
}

int macchiato_service_test(void)
{
  int ret_val = MACCHIATO_SUCCESS;
  unsigned char opaque_data[] = 
	  "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do "
	  "eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim "
	  "ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut "
	  "aliquip ex ea commodo consequat. Duis aute irure dolor in "
	  "reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla "
	  "pariatur. Excepteur sint occaecat cupidatat non proident, sunt in "
	  "culpa qui officia deserunt mollit anim id est laborum.";
  unsigned int opaque_data_len = /*sizeof(opaque_data)*/strlen((const char*)&opaque_data[0]);
  unsigned char *signed_data = NULL;
  unsigned int signed_data_len = 0;
  unsigned char *signature = NULL;
  unsigned int signature_len = 0;
  unsigned char *field_ptr = NULL;
  unsigned int field_len = 0;

  do
  {
  	ret_val = macchiato_service_ecc_sign(opaque_data, opaque_data_len,
  						signed_data, &signed_data_len, signature, &signature_len);
  	if (ret_val && ret_val != MACCHIATO_TOO_SHORT_OUT_BUFFER_ERROR)
  	{
  	  MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_service_test(): rsa sign failure and returns %d\n", ret_val);
  	  break;
  	}
  	MACC_LOG(MACC_LOG_MSG_DEBUG, "macchiato_service_test(): signed data len %d, signature length %d", signed_data_len, signature_len);
     
  	if (MACCHIATO_TOO_SHORT_OUT_BUFFER_ERROR == ret_val)
  	{
  	  signed_data = qsee_malloc(signed_data_len);
  	  signature = qsee_malloc(signature_len);
  	}
     
  	if (!signed_data || !signature)
  	{
  	  MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_service_test(): malloc failure\n");
  	  ret_val = MACCHIATO_ALLOCATION_ERROR;
  	  break;
  	}
     
  	ret_val = macchiato_service_ecc_sign(opaque_data, opaque_data_len,
  						signed_data, &signed_data_len, signature, &signature_len);
  	if (ret_val)
  	{
  	  MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_service_test(): rsa sign failure and returns %d\n", ret_val);
  	  break;
  	}
     
  	MACC_LOG(MACC_LOG_MSG_DEBUG, "macchiato_service_test(): signed data total len %d, signature len %d", 
  						 signed_data_len, signature_len);
     
  	field_ptr = signed_data;
  	field_len = MACCHIATO_MSG_SIGN_PUBKEY_LEN;
  	MACC_LOG(MACC_LOG_MSG_DEBUG, "macchiato_service_test(): sgined pub key - leng %d", field_len);
  	output_S_BYTE(field_ptr, field_len);
  	field_ptr += field_len;
     
  	field_len = MACCHIATO_MSG_ENC_PUBKEY_LEN;		
  	MACC_LOG(MACC_LOG_MSG_DEBUG, "macchiato_service_test(): enc pub key - leng %d", field_len);
  	output_S_BYTE(field_ptr, field_len);
  	field_ptr += field_len;
     
  	field_len = MACCHIATO_MSG_TA_HASH_LEN;		
  	MACC_LOG(MACC_LOG_MSG_DEBUG, "macchiato_service_test(): TA Hash data - length %d", field_len);
  	output_S_BYTE(field_ptr, field_len);
  	field_ptr += field_len;
     
  	field_len = MACCHIATO_MSG_NUM_SVCID_LEN;		
    MACC_LOG(MACC_LOG_MSG_DEBUG, "macchiato_service_test(): Service ID length data - length %d", field_len);
    output_S_BYTE(field_ptr, field_len);
    field_ptr += field_len;
     
  	field_len = macchiato_get_svcid_num() * MACCHIATO_MSG_SVCID_LEN;		
    MACC_LOG(MACC_LOG_MSG_DEBUG, "macchiato_service_test(): Service ID data - length %d", field_len);
    output_S_BYTE(field_ptr, field_len);
    field_ptr += field_len;
     
  	field_len = MACCHIATO_MSG_HW_VERSION_LEN;		
  	MACC_LOG(MACC_LOG_MSG_DEBUG, "macchiato_service_test(): HW version length data - length %d", field_len);
  	output_S_BYTE(field_ptr, field_len);
  	field_ptr += field_len;
     
  	field_len = MACCHIATO_MSG_SERIAL_NUM_LEN;		
  	MACC_LOG(MACC_LOG_MSG_DEBUG, "macchiato_service_test(): Chip Serial Num length data - length %d", field_len);
  	output_S_BYTE(field_ptr, field_len);
  	field_ptr += field_len;
     
  	field_len = opaque_data_len;		
  	MACC_LOG(MACC_LOG_MSG_DEBUG, "macchiato_service_test(): opache data - length %d", field_len);
  	output_S_BYTE(field_ptr, field_len);
     
  	MACC_LOG(MACC_LOG_MSG_DEBUG, "macchiato_service_test(): signature - length %d", signature_len);
  	output_S_BYTE(signature, signature_len);
     
    ret_val = macchiato_service_ecc_verify(signed_data, signed_data_len, signature, signature_len);
    if(ret_val)
    {
  	  MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_service_test(): verify Signature failure and returns %d\n", ret_val);
  	  break;
    }
     
  	MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_service_test(): verify Signature result matches");
  } while (0); 
  
  if (signed_data)
  {
  	qsee_free(signed_data);
  	signed_data = NULL;
  }
  if (signature)
  {
  	qsee_free(signature);
    signature = NULL;
  }

  return ret_val;
}

static int macchiato_ecies_test_with_random_input(void)
{
    int            err = MACCHIATO_SUCCESS;
    int            i = 0;
    uint8_t      * cryptogram = NULL;
    uint32_t       cryptogram_len = 0;
    uint8_t      * deciphered = NULL;
    uint32_t       deciphered_len = 0;
    uint8_t      * outAD = NULL;
    uint32_t       outAD_len = 0;

    QSEE_affine_point_t  public_point;
    QSEE_bigval_t        private_key;

    uint8_t      * message = NULL;
    uint32_t       message_len = 0;
    uint8_t      * myAD = NULL;
    uint32_t       myAD_len = 0;
    
    uint32_t       MSG_LEN = 0;
    uint32_t       AD_LEN = 0;

#define NUM_ECIES_TESTS 4

    for (i=0;i<2*NUM_ECIES_TESTS;i++)
    {

        size_t random_val;

        MACC_LOG(MACC_LOG_MSG_ERROR,"<Loop %d> starts.", i);

        if ( sizeof(size_t) != qsee_prng_getdata((uint8*)&random_val,sizeof(size_t)) )
        {
            MACC_LOG(MACC_LOG_MSG_ERROR,"Error: qsee_prng_getdata failed. size_t");
            err = MACCHIATO_ERROR_PRNG_DATA;
            goto test_exit;
        }
        
        random_val %= (1 << (8+(i % NUM_ECIES_TESTS)*2));
        MSG_LEN = 512 + random_val;

        MACC_LOG(MACC_LOG_MSG_ERROR,"MSG_LEN = %d", MSG_LEN);

        message = qsee_malloc(MSG_LEN);
        if (message == NULL)
        {
            MACC_LOG(MACC_LOG_MSG_ERROR,"Error: message malloc failed.");
            err = MACCHIATO_ALLOCATION_ERROR;
            goto test_exit;
        }
        message_len = MSG_LEN;

        if ( 512 != qsee_prng_getdata(message,512) )
        {
            MACC_LOG(MACC_LOG_MSG_ERROR,"Error: qsee_prng_getdata failed. 512");
            err = MACCHIATO_ERROR_PRNG_DATA;
            goto test_exit;
        }

        if (i<NUM_ECIES_TESTS)
        {
            MACC_LOG(MACC_LOG_MSG_ERROR,"Testing ECIES Encryption/Decryption with empty metadata field.");
            myAD = NULL;
            myAD_len = 0;
        }
        else
        {
            MACC_LOG(MACC_LOG_MSG_ERROR,"Testing ECIES Encryption/Decryption with non-empty clear metadata field.\n");

            if ( sizeof(size_t) != qsee_prng_getdata((uint8*)&random_val,sizeof(size_t)) )
            {
                MACC_LOG(MACC_LOG_MSG_ERROR,"Error: qsee_prng_getdata failed. size_t");
                err = MACCHIATO_ERROR_PRNG_DATA;
                goto test_exit;
            }
            random_val %= (1 << (5+(i % NUM_ECIES_TESTS)*2));;
            AD_LEN  = 113 + random_val;

            MACC_LOG(MACC_LOG_MSG_ERROR,"AD_LEN = %d", AD_LEN);
            
            myAD = qsee_malloc(AD_LEN);
            if (myAD == NULL)
            {
                MACC_LOG(MACC_LOG_MSG_ERROR,"Error: myAD malloc failed.");
                err = MACCHIATO_ALLOCATION_ERROR;
                goto test_exit;
            }
            myAD_len = AD_LEN;

            if ( 113 != qsee_prng_getdata(myAD, 113) )
            {
                MACC_LOG(MACC_LOG_MSG_ERROR,"Error: qsee_prng_getdata failed. {%d}", myAD_len);
                err = MACCHIATO_ERROR_PRNG_DATA;
                goto test_exit;
            }
        }

        MACC_LOG(MACC_LOG_MSG_ERROR,"Plaintext length = %u, Metadata length = %u", message_len, myAD_len);
        
        // Generate key pair, this is dependent on local randomness and only for testing purposes

#ifdef TEST_USE_PHK_SIGNING_KEY
  	    err = qsee_macc_gen_ecc_privkey(MACCHIATO_KEY_TYPE_ECC_AUTO_SIGNING, &private_key, sizeof(MACC_KEY_TYPE));
  	    if (err)
  	    {
  	      MACC_LOG(MACC_LOG_MSG_ERROR, "gen ecc private key failed %d", err);
  	      goto test_exit;;
  	    }

        //generate public keys
        err = qsee_SW_ECDH_mult_basepoint((MACC_POINT_TYPE*)&public_point, &private_key);
  	    if (err)
  	    {
  	      MACC_LOG(MACC_LOG_MSG_ERROR, "gen ecc public key failed %d", err);
  	      goto test_exit;;
  	    }
#else
        err = qsee_SW_ECDH_generate(&public_point, &private_key);  /* variable */
        if (err)
        {
            MACC_LOG(MACC_LOG_MSG_ERROR,"Error: qsee_SW_ECDH_generate failed {%d}", err);
            goto test_exit;
        }
#endif

        //MACC_LOG(MACC_LOG_MSG_ERROR,"Original message =      '%s'",message);

        // qsee_malloc
        MACC_LOG(MACC_LOG_MSG_ERROR,"cryptogram_len = %d", cryptogram_len);
        cryptogram_len = 2 * 36 + 4 + myAD_len + 4 + message_len + (16 - (message_len % 16)) + 32;
        cryptogram = qsee_malloc(cryptogram_len);
        if (cryptogram == NULL)
        {
            MACC_LOG(MACC_LOG_MSG_ERROR,"Error: message malloc failed. {%d}", cryptogram_len);
            err = MACCHIATO_ALLOCATION_ERROR;
            goto test_exit;
        }

        // encrypt

        err = macchiato_ECIESAD_encrypt(
              & public_point,        // recipient's public key
              myAD,                  // associated data 
              myAD_len,              // associated data length
              message,               // message to be encrypted
              message_len,           // length of message
              cryptogram,            // pointer to place where to store the address of the cryptogram
                                     // if *cryptogram is NULL, then buffer will be allocated by routine
                                     // and its address stored here. If not, exiting will be used, but if
                                     // turns out to be too small, crash will occur.
              & cryptogram_len);     // ... and the (actual) resulting length

        if (err)
        {
            MACC_LOG(MACC_LOG_MSG_ERROR,"Error while encrypting!");
            goto test_exit;
        }
        else
        {
            MACC_LOG(MACC_LOG_MSG_ERROR,"Encryption returned succeeded!");
        }

        if (myAD)
        {
            // read out AD
            err = macchiato_ECIESAD_get_AD(
                  cryptogram,
                  cryptogram_len,
                  & outAD,
                  & outAD_len);

            if (err)
            {
                MACC_LOG(MACC_LOG_MSG_ERROR,"Error while extracting metadata!");
                goto test_exit;
            }
            else
            {
                MACC_LOG(MACC_LOG_MSG_ERROR,"Metadata extracted successfully!");
            }
        }

        // qsee_malloc
        MACC_LOG(MACC_LOG_MSG_ERROR,"deciphered_len = %d", deciphered_len);
        deciphered_len = cryptogram_len;
        deciphered = qsee_malloc(deciphered_len);
        if (deciphered == NULL)
        {
            MACC_LOG(MACC_LOG_MSG_ERROR,"Error: message malloc failed.{%d}", deciphered_len);
            err = MACCHIATO_ALLOCATION_ERROR;
            goto test_exit;
        }
        
        // decrypt

        err = macchiato_ECIESAD_decrypt(
              & private_key,        // recipient's private key
              cryptogram,           // pointer to received cryptogram
              cryptogram_len,       // length of cryptogram
              deciphered,         // pointer to place where to store the address of the deciphered data
                                    // if *deciphered is NULL, then buffer will be allocated by routine
                                    // and its address stored here. If not, exiting will be used, but if
                                    // turns out to be too small, crash will occur.
              & deciphered_len,     // actual resulting length (may differ from amount of allocate memory
              & outAD,              // address of pointer to the begin of associated data
              & outAD_len);         // address of location where associated data length will be stored

        if (err)
        {
            MACC_LOG(MACC_LOG_MSG_ERROR,"Error (%d) while decrypting!", err);
            goto test_exit;
        }
        else
        {
            if (deciphered_len != message_len)
            {
                MACC_LOG(MACC_LOG_MSG_ERROR,"Length of decryption does not match with that of plaintext!");
                err = MACCHIATO_ERROR_ECIES_COMPARISON_FAILED;
                goto test_exit;
            }

            if (myAD)
            {
                if (myAD_len != outAD_len)
                {
                    MACC_LOG(MACC_LOG_MSG_ERROR,"Length of associated data does not match with initial value!");
                    err = MACCHIATO_ERROR_ECIES_COMPARISON_FAILED;
                    goto test_exit;
                }
            }
            
            //MACC_LOG(MACC_LOG_MSG_ERROR,"Error: crashed here.");

            if ((err = memcmp(message, deciphered, message_len)) != 0)
            {
                MACC_LOG(MACC_LOG_MSG_ERROR,"Error: Decryption does not match with plaintext!");
                err = MACCHIATO_ERROR_ECIES_COMPARISON_FAILED;
                goto test_exit;
            }
            else
            {
                MACC_LOG(MACC_LOG_MSG_ERROR,"Success: Decryption matches!!!");
            }


            if (myAD)
            {
                if ((err = memcmp(myAD, outAD, outAD_len)) != 0)
                {
                    MACC_LOG(MACC_LOG_MSG_ERROR,"Error: Associated data does not match with original!");
                    err = MACCHIATO_ERROR_ECIES_COMPARISON_FAILED;
                    goto test_exit;
                }
                else
                {
                    MACC_LOG(MACC_LOG_MSG_ERROR,"Success: Associated data matches!!!");
                }
            }
        } // end of checking macchiato_ECIESAD_decrypt

        if (message)
        {
            qsee_free(message);
            message = NULL;
        }

        if (myAD)
        {
            qsee_free (myAD);
            myAD = NULL;
        }

        if (deciphered)
        {
            qsee_free (deciphered);
            deciphered = NULL;
        }

        if (cryptogram)
        {
            qsee_free (cryptogram);
            cryptogram = NULL;
        }

        MACC_LOG(MACC_LOG_MSG_ERROR,"<Loop %d> is done", i);
    } // end of for loop

test_exit:

    if (message)
    {
        qsee_free(message);
        message = NULL;
    }

    if (myAD)
    {
        qsee_free (myAD);
        myAD = NULL;
    }

    if (deciphered)
    {
        qsee_free (deciphered);
        deciphered = NULL;
    }

    if (cryptogram)
    {
        qsee_free (cryptogram);
        cryptogram = NULL;
    }

    if (err == MACCHIATO_SUCCESS)
    {
        MACC_LOG(MACC_LOG_MSG_ERROR,"All macchiato_ecies tests succeeded!");
    }
    else
    {
        MACC_LOG(MACC_LOG_MSG_ERROR,"Error: macchiato_ecies tests Failed. {%d}", err);
    }
    
    return err;
}

static int macchiato_ecies_test_with_given_input(void)
{
	int            err = MACCHIATO_SUCCESS;
	int            i = 0;
	uint8_t      * deciphered = NULL;
	uint32_t       deciphered_len = 0;
	uint8_t      * outAD = NULL;
	uint32_t       outAD_len = 0;

	// ///////////////////////////////////////////////////////////////////////////////////

	QSEE_bigval_t  private_key = {{100,0,0,0, 0,0,0,0, 0}};

	uint8_t        message[] = "What do you call an alligator in a vest? An investigator.";
	uint32_t       message_len = strlen((char*)&(message[0]));
	uint8_t        AD[] = "Knock knock!";
	uint32_t       AD_len = strlen((char*)&(AD[0]));

	uint8_t        cryptogram[180] =
				  {0xf7, 0xb6, 0xba, 0x60, 0xeb, 0x37, 0x28, 0xe3, 0x5a, 0x68, 0x75, 0x42, 
				   0xa8, 0x6a, 0x3c, 0x03, 0xde, 0xf4, 0x7c, 0x8e, 0xb4, 0xcd, 0xcd, 0x82, 
				   0xb3, 0x1f, 0x99, 0x60, 0xad, 0x52, 0xbe, 0xf2, 0x98, 0x28, 0xe5, 0x09, 
				   0xb3, 0x15, 0x08, 0x43, 0x72, 0x1c, 0x8b, 0x99, 0xd0, 0x08, 0x93, 0xf3, 
				   0x85, 0xd9, 0xc8, 0x6a, 0x8a, 0x63, 0x36, 0x73, 0x22, 0x09, 0x99, 0xf0, 
				   0xbc, 0xb9, 0x8a, 0x06, 0x00, 0x00, 0x00, 0x0c, 0x4b, 0x6e, 0x6f, 0x63, 
				   0x6b, 0x20, 0x6b, 0x6e, 0x6f, 0x63, 0x6b, 0x21, 0x00, 0x00, 0x00, 0x40, 
				   0x05, 0xc6, 0xc6, 0xeb, 0x41, 0xd0, 0xfd, 0x29, 0x1b, 0x02, 0xe0, 0xfe, 
				   0x97, 0x16, 0x6b, 0xf3, 0xf9, 0xb6, 0xba, 0xe7, 0x7e, 0x19, 0xcc, 0x76, 
				   0x28, 0xe7, 0xcb, 0xcf, 0x64, 0xe3, 0xcf, 0x7a, 0x2e, 0x05, 0x67, 0x7d, 
				   0x0e, 0x5b, 0x2e, 0xe6, 0x40, 0xd0, 0xad, 0x28, 0x00, 0x90, 0x18, 0xf5, 
				   0x67, 0x1f, 0x0e, 0xe8, 0xf9, 0x3f, 0x84, 0x71, 0xef, 0x0b, 0x29, 0xba, 
				   0x95, 0xa8, 0xa0, 0x51, 0x1b, 0xa8, 0x6f, 0x49, 0x1d, 0x08, 0xe4, 0xd9, 
				   0xf9, 0xa5, 0x94, 0xe3, 0x46, 0x2e, 0xf6, 0x88, 0xa7, 0x88, 0xa9, 0x29, 
				   0x8a, 0x1f, 0xc8, 0x70, 0x50, 0xda, 0x19, 0xf4, 0x71, 0x0b, 0x69, 0x44};
	uint32_t       cryptogram_len = 180;

	// ///////////////////////////////////////////////////////////////////////////////////
	
	MACC_LOG(MACC_LOG_MSG_ERROR,"Testing ECIESAD decryption with fixed inputs.");
	MACC_LOG(MACC_LOG_MSG_ERROR,"Plaintext length = %u, Metadata length = %u", message_len, AD_len);

    // qsee_malloc
    MACC_LOG(MACC_LOG_MSG_ERROR,"trying to malloc {%d}", cryptogram_len);
    deciphered = qsee_malloc(cryptogram_len);
    if (deciphered == NULL)
    {
        MACC_LOG(MACC_LOG_MSG_ERROR,"Error: message malloc failed.{%d}", cryptogram_len);
        err = MACCHIATO_ALLOCATION_ERROR;
        goto test_exit;
    }
    deciphered_len = cryptogram_len;

	// decrypt

	err = macchiato_ECIESAD_decrypt(
		  & private_key,        // recipient's private key
		  cryptogram,           // pointer to received cryptogram
		  cryptogram_len,       // length of cryptogram
		  deciphered,         // pointer to place where to store the adress of the deciphered data
								// if *deciphered is NULL, then buffer will be allocated by routine
								// and its address stored here. If not, exiting will be used, but if
								// turns out to be too small, crash will occur.
		  & deciphered_len,     // actual resulting length (may differ from amount of allocate memory
		  & outAD,              // address of pointer to the begin of associated data
		  & outAD_len);         // address of location where associated data length will be stored

	if (err)
	{
		MACC_LOG(MACC_LOG_MSG_ERROR,"Error (%d) while decrypting!\n", err);
		goto test_exit;
	}
	else
	{
		if (deciphered_len != message_len)
		{
			MACC_LOG(MACC_LOG_MSG_ERROR,"Length of decryption does not match with that of plaintext!\n");
			err = MACCHIATO_ERROR_ECIES_COMPARISON_FAILED;
			goto test_exit;
		}

		if (AD)
		{
			if (AD_len != outAD_len)
			{
				MACC_LOG(MACC_LOG_MSG_ERROR,"Length of associated data does not match with initial value!\n");
				err = MACCHIATO_ERROR_ECIES_COMPARISON_FAILED;
				goto test_exit;
			}
		}
	
		if ((err = memcmp(message, deciphered, message_len)) != 0)
		{
			MACC_LOG(MACC_LOG_MSG_ERROR,"Error: Decryption does not match with plaintext!\n");
			err = MACCHIATO_ERROR_ECIES_COMPARISON_FAILED;
			goto test_exit;
		}
		else
		{
			MACC_LOG(MACC_LOG_MSG_ERROR,"Success: Decryption matches!!!\n");
			MACC_LOG(MACC_LOG_MSG_ERROR,"Plain text = '");
			for (i=0;i<deciphered_len;i++)
			{
				MACC_LOG(MACC_LOG_MSG_ERROR,"%c",deciphered[i]);
			}
			MACC_LOG(MACC_LOG_MSG_ERROR," ");
		}

		if (AD)
		{
			if ((err = memcmp(AD, outAD, outAD_len)) != 0)
			{
				MACC_LOG(MACC_LOG_MSG_ERROR,"Error: Associated data does not match with original!\n");
				err = MACCHIATO_ERROR_ECIES_COMPARISON_FAILED;
				goto test_exit;
			}
			else
			{
				MACC_LOG(MACC_LOG_MSG_ERROR,"Success: Associated data matches!!!\n");
				MACC_LOG(MACC_LOG_MSG_ERROR,"Associated data = ");
				for (i=0;i<outAD_len;i++)
				{
					MACC_LOG(MACC_LOG_MSG_ERROR,"%c",outAD[i]);
				}
			}
		}
	}	

test_exit:

    if (deciphered)
    {
        qsee_free (deciphered);
        deciphered = NULL;
    }

    if (err == MACCHIATO_SUCCESS)
    {
        MACC_LOG(MACC_LOG_MSG_ERROR,"macchiato_ecies with a given input succeeded!");
    }
    else
    {
        MACC_LOG(MACC_LOG_MSG_ERROR,"macchiato_ecies with a given input Failed. {%d}", err);
    }

	return err;
}

int macchiato_ecies_test(void)
{
    int ret = 0;
    

    ret = macchiato_ecies_test_with_given_input();
    if (ret)
    {
        MACC_LOG(MACC_LOG_MSG_ERROR,"************ Test Case 1 Failed {%d} ************", ret);
    }
    else
    {
        MACC_LOG(MACC_LOG_MSG_ERROR,"************ Test Case 1 succeeded! ************");
    }
    

    ret = macchiato_ecies_test_with_random_input();
    if (ret)
    {
        MACC_LOG(MACC_LOG_MSG_ERROR,"************ Test Case 2 Failed {%d}************ ", ret);
    }
    else
    {
        MACC_LOG(MACC_LOG_MSG_ERROR,"************ Test Case 2 succeeded! ************");
    }


    if (ret)
    {
        MACC_LOG(MACC_LOG_MSG_ERROR, "**********************************************************************");
        MACC_LOG(MACC_LOG_MSG_ERROR, "****************** Macchiato ECIES TEST failed ***********************");
        MACC_LOG(MACC_LOG_MSG_ERROR, "**********************************************************************");
        MACC_LOG(MACC_LOG_MSG_ERROR, "");
    }
    else
    {
        MACC_LOG(MACC_LOG_MSG_ERROR, "**********************************************************************");
        MACC_LOG(MACC_LOG_MSG_ERROR, "****************** Macchiato ECIES TEST  Succeeded *******************");
        MACC_LOG(MACC_LOG_MSG_ERROR, "**********************************************************************");
        MACC_LOG(MACC_LOG_MSG_ERROR, "");
    }

    return ret;
}

