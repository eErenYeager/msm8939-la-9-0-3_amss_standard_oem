/*===========================================================================
  @file secrsa_signature.c
  
   This file provides RSA PKCS #1 v1.5 and v2.0 PSS hash signature 
   generation/verification implementation.
  ===========================================================================

                           EDIT HISTORY FOR FILE
  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsapps/keyprov/macchiato/src/macchiato_ecc.c#1 $
  $DateTime: 2018/02/07 00:37:16 $ 
  $Author: mplp4svc $

  when       who     what, where, why
  --------   ---     ----------------------------------------------------------
  05/27/14   qxu     initial version

  Copyright (c) 2011 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
  =============================================================================*/

/*===========================================================================

                             INCLUDE FILES

===========================================================================*/
#include "comdef.h"
#include "macchiato.h"
//#include "libstd/stringl/stringl.h"
#include "qsee_heap.h"
#include "qsee_timer.h"
#include "qsee_prng.h"
#include "qsee_core.h"

extern void output_S_BYTE(unsigned char *data, int len) ;
extern void output_S_UINT32(uint32 *data, int len);
int macchiato_hash(unsigned char *data, unsigned int data_len, MACC_KEY_TYPE *digest_val);

#define MSW (BIGLEN - 1)
#define big_is_negative(a) ((int32_t)(a)->data[MSW] < 0)
/* 
 * Converts bigval_t src to a network order (big-endian) binary (byte
 * vector) representation. The if tgtlen is longer that the bigval_t,
 * the value is written sign-extended.  If tgtlen is too small to hold
 * the value, high order bytes are silently dropped.
 */

void
macc_bigval_to_binary(MACC_KEY_TYPE const *src, void *tgt, size_t tgtlen)
{
   size_t i;
   uint8_t v;
   uint8_t highbytes = big_is_negative(src) ? 0xff : 0;

   /* LSbyte to MS_byte */
   for (i = 0; i < 4 * BIGLEN; ++i) {
      if (i < tgtlen) {
   v = src->data[i/4] >> (8 * (i % 4));
   ((uint8_t *)tgt)[tgtlen - 1 - i] = v;
      }
   }
   /* i is live */
   for (; i < tgtlen; ++i) {
      ((uint8_t *)tgt)[tgtlen - 1 - i] = highbytes;
   }
}

/* 
 * Converts a network-order (big-endian) binary value (byte array) at
 * *src of length srclen to a bigval_t at *bn.  If srclen is larger
 * then the length of a bigval_t, the high order bytes are silently
 * dropped.
 */
void
macc_binary_to_bigval(const void* src, MACC_KEY_TYPE *tgt, size_t srclen)
{
   size_t i;
   uint8_t v;

   /* zero the bigval_t */
   for (i = 0; i < BIGLEN; ++i) {
      tgt->data[i] = 0;
   }
   /* scan from LSbyte to MSbyte */
   for (i = 0; i < srclen && i < 4 * BIGLEN; ++i) {
      v = ((uint8_t *)src)[srclen - 1 - i];
      tgt->data[i/4] |= (uint32_t)v << (8 * (i % 4));
   }
} 

//#define MACCHIATO_TZBSP
#ifdef MACCHIATO_TZBSP
int macchiato_gen_ecc_privkey(int type, bigval_t *privkey)
{
  return 0;
}
#endif

/*===========================================================================

                            FUNCTION DEFINITIONS

===========================================================================*/
/**
 * @brief  This function will return Macchiato ECC public key. 
 *
 * @param[out] pubkey         The Macchiato ECC public key 
 *
 * @return 0 on success, negative on failure
*/
int macchiato_ecc_get_pubkey(MACC_KEY_TYPE *privkey, unsigned char *p_marshalled_key)
{
  MACC_POINT_TYPE public_point;

  //generate public keys
  qsee_SW_ECDH_mult_basepoint(&public_point, privkey);

  macc_bigval_to_binary(&public_point.x, p_marshalled_key, 32);
  macc_bigval_to_binary(&public_point.y, p_marshalled_key + 32, 32);
  return MACCHIATO_SUCCESS;
}

int macchiato_ecc_sign(MACC_KEY_TYPE *privkey,
             unsigned char *signed_data,
             unsigned int signed_data_len,
             unsigned char *signature,
             unsigned int  *signature_len)
{
  int ret_val = MACCHIATO_SUCCESS;
  MACC_KEY_TYPE digest_val;
  MACC_SIG_TYPE ecc_sig;
  uint8 marshalled_key[BIGLEN*4];
  do
  {
    /* generate signed_data hash and convert it to bigval*/
    ret_val = macchiato_hash(signed_data, signed_data_len, &digest_val);
    if( MACCHIATO_SUCCESS != ret_val ) 
    {
      MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_ecc_sign() macchiato_hash_bigval fail and returns %d\n", ret_val);
      break;
    }
    MACC_LOG(MACC_LOG_MSG_DEBUG, "macchiato_ecc_sign DigestNum:"); 
    output_S_UINT32(&(digest_val.data[0]), 8); 
     
    macc_bigval_to_binary(privkey, marshalled_key, 32);
    MACC_LOG(MACC_LOG_MSG_DEBUG, "ECDSA private key = ");
    output_S_BYTE(marshalled_key, 32);
     
    /* Compute the signature */
    ret_val = qsee_SW_ECDSA_Sign(&digest_val, privkey, &ecc_sig);
    if(ret_val < 0) 
    {
      MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_ecc_sign() ECDSA_sign fail and returns %d\n", ret_val);
      ret_val = MACCHIATO_HASH_ERROR;
      break;
    }
     
    if (*signature_len < 64)
    {
      MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_ecc_sign() signature len is too short %d\n", signature_len);
      ret_val = MACCHIATO_SIGNATURE_BUFFER_TOO_SMALL;
      break;
    }
       
    macc_bigval_to_binary(&(ecc_sig.r), signature, 32);
    MACC_LOG(MACC_LOG_MSG_DEBUG, "macchiato_ecc_sign() signature - r");
    output_S_BYTE(signature, 32);
       
    macc_bigval_to_binary(&(ecc_sig.s), signature + 32,32);
    MACC_LOG(MACC_LOG_MSG_DEBUG, "macchiato_ecc_sign() signature - s");
    output_S_BYTE(signature+32, 32);

  }  while (0); 

  return ret_val;
}

// Verification of the signature
int macchiato_ecc_verify(MACC_KEY_TYPE *privkey,
             unsigned char *signed_data,
             unsigned int signed_data_len,
             unsigned char *signature)
{
  int ret_val = MACCHIATO_SUCCESS;
  MACC_KEY_TYPE digest_val;
  MACC_POINT_TYPE public_point;
  MACC_SIG_TYPE  ecc_sig;

  uint8 marshalled_key[BIGLEN*4];
  do
  {
    /* generate signed_data hash and convert it to bigval*/
    ret_val = macchiato_hash(signed_data, signed_data_len, &digest_val);
    if(ret_val < 0) 
    {
      MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_ecc_verify() macchiato_hash_bigval fail and returns %d\n", ret_val);
      break;
    }
    
    MACC_LOG(MACC_LOG_MSG_DEBUG, "macchiato_ecc_verify DigestNum:"); 
    output_S_UINT32(&(digest_val.data[0]), 8); 
    
    macc_bigval_to_binary(privkey, marshalled_key, 32);
    MACC_LOG(MACC_LOG_MSG_DEBUG, "macchiato_ecc_verify ECDSA private key = ");
    output_S_BYTE(marshalled_key, 32);
    
    /* generate public keys */
    qsee_SW_ECDH_mult_basepoint(&public_point, privkey);
    
    /* convert the signature binary to bigval*/      
    macc_binary_to_bigval(signature, &ecc_sig.r, 32);
    macc_binary_to_bigval(signature + 32, &ecc_sig.s, 32);
 
          
    /* Verify the signature */
    if(!qsee_SW_ECDSA_Verify(&digest_val, &public_point, &ecc_sig))
    {
      MACC_LOG(MACC_LOG_MSG_DEBUG, "macchiato_ecc_verify ECDSA verify is sucessful"); 
      ret_val = MACCHIATO_SUCCESS;
    }
    else
    {
      MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_ecc_verify ECDSA signature doesn't match %d", ret_val);
      ret_val = MACCHIATO_SIGNATURE_NOMATCH;
    }  
  }  while (0); 

  return ret_val;
}

int macchiato_hash(unsigned char *data, unsigned int data_len, MACC_KEY_TYPE *digest_val)
{
  int ret_val = MACCHIATO_SUCCESS;
  unsigned char  digest[QSEE_SHA256_HASH_SZ]={0};
  
  do
  {
    /* Compute sha256 digest */
    ret_val = qsee_hash(QSEE_HASH_SHA256, data, data_len, digest, sizeof(digest));
    if(ret_val < 0) 
    {
      MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_ecc_sign() qsee_hash fail and returns %d\n", ret_val);
      ret_val = MACCHIATO_HASH_ERROR;
      break;
    }
    MACC_LOG(MACC_LOG_MSG_DEBUG, "macchiato_hash Digest:", ret_val);
    output_S_BYTE(&digest[0],32); 
    
    /* Convert the digest to a large integer */
    macc_binary_to_bigval(digest, digest_val, 32); 
    (digest_val->data)[8] = 0;
     
  }while(0);
     
  return ret_val;

}

