/*===========================================================================
  @file secrsa_signature.c
  
   This file provides RSA PKCS #1 v1.5 and v2.0 PSS hash signature 
   generation/verification implementation.
  ===========================================================================

                           EDIT HISTORY FOR FILE
  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsapps/keyprov/macchiato/src/macchiato_service.c#1 $
  $DateTime: 2018/02/07 00:37:16 $ 
  $Author: mplp4svc $

  when       who     what, where, why
  --------   ---     ----------------------------------------------------------
  09/24/14   cz      Added hw/sw key AUTO detection
  09/19/14   cz      Added qsee_read_hw_version() and macchiato_is_enabled()
  05/27/14   qxu     initial version

  Copyright (c) 2011 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
  =============================================================================*/

/*===========================================================================

                             INCLUDE FILES

===========================================================================*/
#include "comdef.h"
#include "macchiato.h"
#include "macchiato_service.h"
#include "libstd/stringl/stringl.h"
#include "qsee_heap.h"
#include "qsee_timer.h"
#include "qsee_prng.h"
#include "qsee_core.h"
#include "qsee_ecc.h"


//affine_point_t public_point;
extern void output_S_BYTE(unsigned char *data, int len) ;
extern void output_S_UINT32(uint32 *data, int len) ;
unsigned int macchiato_get_svcid_num(void);
void macchiato_destroy_key(MACC_KEY_TYPE *privkey);


/*===========================================================================

                            FUNCTION DEFINITIONS

===========================================================================*/
/**
 * @brief  This function will pre-append the opaque data provided by TA with
 *  	   service ids list associated with the TA, hash the data blob with
 *  	   SHA-256 and sign signature with PKCS #1 padding.
 *
 * @param[in] opaque_data 				the data buffer contained the data to be 
 *  											signed from TA
 * @param[in] opaque_data_len 		the length of the opaque data 
 * @param[out] signed_data 			the output data which has been signed, 
 *    										including Num SVC ID�s, SVC ID�s, Opaque
 *    										Data. Data buffer provided by TA.
 * @param[in, out] signed_data_len	the max and resulting size of the signed 
 * 											data
 * @param[out] signature   			The signature
 * @param[in,out] siglen   			The max size and resulting size of the 
 *  	 										signature
 *
 * @return 0 on success, negative on failure
*/
int macchiato_service_ecc_sign
(
   const unsigned char       *opaque_data, 
   unsigned int               opaque_data_len,
   unsigned char             *signed_data,
   unsigned int              *signed_data_len,
   unsigned char             *signature,
   unsigned int              *signature_len
)
{
  int ret_val = MACCHIATO_SUCCESS;
  MACC_KEY_TYPE privkey_sign, privkey_enc;
  unsigned char marshalled_key_sign[64] = {0};
  unsigned char marshalled_key_enc[64] = {0};
  unsigned char *data_ptr = signed_data;
  unsigned int header_len = 0, data_len = 0, key_len = 0;
  unsigned int num_svcid = macchiato_get_svcid_num();

  do
  {
    header_len += (MACCHIATO_MSG_ENC_PUBKEY_LEN  
				   + MACCHIATO_MSG_SIGN_PUBKEY_LEN 
				   + MACCHIATO_MSG_TA_HASH_LEN  
				   + MACCHIATO_MSG_NUM_SVCID_LEN
				   + num_svcid * MACCHIATO_MSG_SVCID_LEN 
				   + MACCHIATO_MSG_HW_VERSION_LEN 
				   + MACCHIATO_MSG_SERIAL_NUM_LEN );
    data_len = header_len + opaque_data_len;
    MACC_LOG(MACC_LOG_MSG_DEBUG, "macchiato_service_ecc_sign() header_len: %d, opaque_data_len %d, data_len %d", 
						 header_len, opaque_data_len, data_len);

    if((*signed_data_len < data_len) 
      || (*signature_len < MACCHIATO_MSG_SIGNATURE_LEN))
    {
      *signed_data_len = data_len;
      *signature_len = MACCHIATO_MSG_SIGNATURE_LEN;
      ret_val = MACCHIATO_TOO_SHORT_OUT_BUFFER_ERROR;
      MACC_LOG(MACC_LOG_MSG_DEBUG, "macchiato_service_ecc_sign() input buffer is too small\n");
      break;            
    }

    if ((opaque_data == NULL) || (signed_data == NULL) || (signature == NULL))
    {
      ret_val = MACCHIATO_INVALID_PARAM;
      break;
    }

    /*generate Macchiato signing and encryption keys*/
    ret_val = qsee_macc_gen_ecc_privkey(MACCHIATO_KEY_TYPE_ECC_AUTO_SIGNING, &privkey_sign, sizeof(MACC_KEY_TYPE));
    if (ret_val)
    {
      MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_service_ecc_sign() signing key gen fail and returns %d\n", ret_val);
      break;
    }

    ret_val = qsee_macc_gen_ecc_privkey(MACCHIATO_KEY_TYPE_ECC_AUTO_ENCRYPTION, &privkey_enc, sizeof(MACC_KEY_TYPE));
   	if (ret_val)
    {
      MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_service_ecc_sign() encryption key gen fail and returns %d\n", ret_val);
      break;
	}

    /*set up Macchiato data blob to be signed*/
    memset((uint8*)data_ptr, 0, data_len);

    /*Device Public Signing Key field*/
    macchiato_ecc_get_pubkey(&privkey_sign, &marshalled_key_sign[0]);

    key_len = MACCHIATO_MSG_SIGN_PUBKEY_LEN;
    memcpy(data_ptr, &marshalled_key_sign[0], key_len);
    data_ptr += key_len;

    /*Device Public Encryption Key field*/
    macchiato_ecc_get_pubkey(&privkey_enc, &marshalled_key_enc[0]);
    key_len = MACCHIATO_MSG_ENC_PUBKEY_LEN;
    memcpy(data_ptr, &marshalled_key_enc[0], key_len);
    data_ptr += key_len;

    /*TA Hash field - will be zero for current version */
    key_len = MACCHIATO_MSG_TA_HASH_LEN;
    data_ptr += key_len;

	/*Num Svc IDs field and Svc IDs field - will be zero for current version */
	key_len = MACCHIATO_MSG_NUM_SVCID_LEN
					  + num_svcid * MACCHIATO_MSG_SVCID_LEN;
	data_ptr += key_len;
	 
    /* HW version field */
	key_len = MACCHIATO_MSG_HW_VERSION_LEN;
	*((uint16 *)data_ptr) = (uint16)(qsee_read_hw_version()>>16);
        MACC_LOG(MACC_LOG_MSG_DEBUG, "qsee_read_hw_version() returns %x", *((uint16 *)data_ptr));
	data_ptr += key_len;

	/* Chipset Serial Num field */
	key_len = MACCHIATO_MSG_SERIAL_NUM_LEN;
	*((int *)data_ptr) = qsee_read_serial_num();
	data_ptr += key_len;

    /* Opaque Data field*/
    memcpy(data_ptr, opaque_data, opaque_data_len);

    ret_val = macchiato_ecc_sign(&privkey_sign, signed_data, data_len, signature, signature_len);
    if(ret_val)
    {
      MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_service_ecc_sign() ecc_sign fail and returns %d\n", ret_val);
      break;
    }
  }while (0);

  macchiato_destroy_key(&privkey_sign);
  macchiato_destroy_key(&privkey_enc);

  return ret_val;
}


/**
 * @brief  This function will hash the signed data provided by TA with SHA-256, 
 *  	   calcuate the signature with PKCS #1 padding and verify it with
 *  	   provided signature.
 *
 * @param[in] signed_data  		The data buffer contained the signed data from 
 *  	 						TA
 * @param[in] signed_data_len   The length of signed data 
 * @param[in] signature         The signature to be verified provided by TA
 * @param[in] signature_len     The size the signature 
 *
 * @return 0 on success, negative on failure
*/
int macchiato_service_ecc_verify
(
   unsigned char             *signed_data, 
   unsigned int               signed_data_len,
   unsigned char             *signature,
   unsigned int               signature_len
)
{
  int ret_val = MACCHIATO_SUCCESS;
  MACC_KEY_TYPE privkey_sign;

  do
  {
    /*generate Macchiato signing and encryption keys*/
    ret_val = qsee_macc_gen_ecc_privkey(MACCHIATO_KEY_TYPE_ECC_AUTO_SIGNING, &privkey_sign, sizeof(MACC_KEY_TYPE));
    if (ret_val)
    {
      MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_service_ecc_verify() signing key gen fail and returns %d\n", ret_val);
      break;
    }

    ret_val = macchiato_ecc_verify(&privkey_sign, signed_data, signed_data_len, signature);
    if(ret_val)
    {
      MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_service_ecc_verify(): verify Signature failure and returns %d\n", ret_val);
      break;
    }
  } while (0); 

  return ret_val;
}


/**
 * @brief This function will encrypt the plain message provided by TA with PKCS 
 *  	  #1 padding.
 *
 * @param[in] msg           The plaintext
 * @param[in] msglen        The length of the plaintext
 * @param[out] cipher       The ciphertext 
 * @param[in,out] cipherlen The max size and resulting size of the ciphertext 
 *
 * @return 0 on success, negative on failure
*/
int macchiato_service_ecc_encrypt
(
   const unsigned char     *msg, 
   unsigned int             msglen,
   unsigned char           *cipher,
   unsigned int            *cipherlen
)
{
  int ret_val = MACCHIATO_SUCCESS;

  return ret_val;
}


/**
 * @brief  This function will decrpyed the cipher message provided by TA and 
 *  	   then v1.5 depad.
 *
 * @param[in] cipher        The ciphertext
 * @param[in] cipherlen     The length of the ciphertext(octets)
 * @param[out] msg          The plaintext 
 * @param[in,out] msglen    The max size and resulting size of the plaintext 
 *
 * @return 0 on success, negative on failure
*/
int macchiato_service_ecc_decrypt
(
   unsigned char           *cipher,
   unsigned int             cipherlen,
   unsigned char           *msg,
   unsigned int            *msglen
)
{
  int ret_val = MACCHIATO_SUCCESS;

  return ret_val;
}

unsigned int macchiato_get_svcid_num(void)
{
  return 0;
}

void macchiato_destroy_key(MACC_KEY_TYPE *privkey)
{
  if (privkey)
  {
  	(void)memset(privkey, 0, sizeof(MACC_KEY_TYPE));   
  }
}

