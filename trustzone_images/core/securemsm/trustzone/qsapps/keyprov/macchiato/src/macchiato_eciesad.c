/*===========================================================================
 Copyright (c) 2014 Qualcomm Technologies, Inc.
 All Rights Reserved.
 Qualcomm Technologies Proprietary and Confidential.
===========================================================================*/

/* 
 * ECIES (Elliptic Curve Integrated Encryption Scheme)
 * with fixed options following INCITS/ISO/IEC 18033-2:2006[2008]
 * and support for clear Associated Data
 *
 * - Curve: NIST P256
 * - KDF:   KDF2 based on SHA-256
 * - ENC:   AES-128 in CBC mode with SC1 padding: needs 128 bit key
 * - MAC:   HMAC–SHA-256–256:                     needs 256 bit key
 * - Optional shared bitstrings: NONE
 * - Variant: we generate also an IV for the encryption/decryption
 *            (I.e. the IV is not zero) if GENERATE_IV == 1
 *
 * The parameters have been chosen to have matching security level
 * (a 256 bit ECC offers 2^128 security, as AES-128, SHA-256 and
 *  HMAC–SHA-256–256)
 * 
 * The file contains implementations of
 * 
 * - KDF2
 * - SC1 encryption/decryption using AES with any key lenght
 * - ECIES encryption/decryption with algorithms chosen as above.
 *
 * Author: Roberto Avanzi, Corporate Product Security
 */

 /*===========================================================================

                      EDIT HISTORY FOR FILE

when       who     what, where, why
--------   ---     ----------------------------------------------------------
10/01/14   cz      Ported the code to QSEE
09/30/14   ra      Author and created the initial Version.

===========================================================================*/

//#include <stdio.h>
//#include <stdint.h>
//#include <string.h>
#include "comdef.h"
#include "ecc_core.h"
#include "qsee_hash.h"
#include "qsee_log.h"
#include "qsee_core.h"
#include "qsee_heap.h"
#include "qsee_hmac.h"
#include "qsee_ecc.h"
#include "macchiato.h"
#include "macchiato_eciesad.h"

#ifndef OLD_LIB
#ifndef NEW_LIB
#define OLD_LIB
#endif
#endif

#ifdef MACC_LOG
#undef MACC_LOG
#define MACC_LOG(xx_prio, xx_fmt, ...) \
  qsee_log(xx_prio, "[%s:%d] " xx_fmt, __FUNCTION__,__LINE__, ##__VA_ARGS__)
#endif
  
#define MACC_LOG_MSG_ERROR QSEE_LOG_MSG_ERROR
#define MACC_LOG_MSG_DEBUG QSEE_LOG_MSG_DEBUG

#define   SW_SHA256_DIGEST_SIZE      QSEE_SHA256_HASH_SZ
#define   AES_BLOCKSIZE              QSEE_AES128_KEY_SIZE
#define   HMAC_SHA256_DIGEST_SIZE    QSEE_SHA256_HASH_SZ

#define   OUTPUT_DEBUG_VALUES  0

extern void macc_bigval_to_binary(MACC_KEY_TYPE const *src, void *tgt, size_t tgtlen);
extern void macc_binary_to_bigval(const void* src, MACC_KEY_TYPE *tgt, size_t srclen);

// ////////////////////////////////////////////////////////////////////

static
int macchiato_ECIES_KDF2(
    const uint8_t   * seed,
    const uint32_t    seed_len,
    uint8_t         * keys,
    const uint32_t    total_key_len);

static
int macchiato_SC1_encrypt(
    const uint8_t   * key, 
    const uint32_t    key_len,
    const uint8_t   * msg, 
    const uint32_t    msg_len,
    const uint8_t   * iv_orig,
    uint8_t         * out,
    uint32_t        * out_len);

static
int macchiato_SC1_decrypt(
    const uint8_t   * key,
    const uint32_t    key_len,
    const uint8_t   * msg,
    const uint32_t    msg_len,
    const uint8_t   * iv_orig,
    uint8_t         * out,
    uint32_t        * out_len,
    const uint32_t    max_len);

static
void write_length_field(uint8_t * field, uint32_t value);

static
uint32_t read_length_field(uint8_t * field);

// ////////////////////////////////////////////////////////////////////

// This is KDF2 from  ISO/IEC 18033-2, with SHA-2
//
// Lenghts are in BYTES,
//
// The INCITS/ISO/IEC 18033-2:2006[2008] KDF2 does NOT have an 
// optional string to be hashed in after the counter.

static
int macchiato_ECIES_KDF2(
    const uint8_t  * seed,
    const uint32_t   seed_len,
    uint8_t        * keys,
    const uint32_t   total_key_len)
{
    uint32_t         blocks;
    uint32_t         index,i;
    uint32_t         last_block_len;
    qsee_hash_ctx    *hash_ctx = NULL;
    uint8_t          digest[SW_SHA256_DIGEST_SIZE];
    uint8_t          counter[4] = {0,0,0,0};
    int              ret = MACCHIATO_SUCCESS;

    do 
    {        
        // Check parameters - not necessary since we use uint32_t so 2^56 cannot be represented
    
        //    if (total_key_len >= 1UL<<32)
        //        return MACCHIATO_ERROR_INVALID_ARG;
        //
        //    if (seed_len >= 1ULL<<56)
        //        return MACCHIATO_ERROR_INVALID_ARG;
    
        // The number of blocks is rounded to the next full block size
    
        blocks = (total_key_len+SW_SHA256_DIGEST_SIZE-1)/SW_SHA256_DIGEST_SIZE;
    
        // Compute effective size of the last block
    
        last_block_len = SW_SHA256_DIGEST_SIZE - (blocks*SW_SHA256_DIGEST_SIZE-total_key_len);
        if (last_block_len==0) 
        {
            last_block_len = SW_SHA256_DIGEST_SIZE;
        }
        
        // Now generate the key material block by block
    
        for(index=0;index<blocks;index++)
        {
            // first increase the counter, so that it starts with 1, ends with #blocks
    
            for (i = 0; i < 4; i ++)
            {
                counter[i] ++;
                if (counter[i] != 0)
                {
                    break;
                }
            }
    
            // compute HASH(seed || counter)
    
            if (qsee_hash_init(QSEE_HASH_SHA256, &hash_ctx)<0)
            {
                ret = MACCHIATO_ERROR_HASH_INIT;
                MACC_LOG(MACC_LOG_MSG_ERROR, "Error: qsee_hash_init failed");
                break;
            }
            if (qsee_hash_update(hash_ctx, seed, seed_len)<0)
            {
                ret = MACCHIATO_ERROR_HASH_UPDATE;
                MACC_LOG(MACC_LOG_MSG_ERROR, "Error: qsee_hash_update failed");
                break;
            }
            if (qsee_hash_update(hash_ctx, counter, 4)<0)
            {
                ret = MACCHIATO_ERROR_HASH_UPDATE;
                MACC_LOG(MACC_LOG_MSG_ERROR, "Error: qsee_hash_update 2 failed");
                break;
            }
            if (qsee_hash_final(hash_ctx, digest, sizeof(digest))<0)
            {
                ret = MACCHIATO_ERROR_HASH_FINAL;
                MACC_LOG(MACC_LOG_MSG_ERROR, "Error: qsee_hash_final failed");
                break;
            }
            
            //Free HASH context
            if (hash_ctx != NULL )
            {
                if ( qsee_hash_free_ctx(hash_ctx) < 0 )
                {
                    MACC_LOG(MACC_LOG_MSG_ERROR, "Error: hash free failed");
                    ret = MACCHIATO_ERROR_HASH_FREE;
                    break;
                }
                hash_ctx = NULL;
            }
  
            // copy result to keys[] array
            memcpy(keys,digest, (index<blocks-1) ? SW_SHA256_DIGEST_SIZE : last_block_len);
            keys += SW_SHA256_DIGEST_SIZE;
        }
    
    } while(0);
    
    
    //Failure case, Free HASH context
    if (hash_ctx != NULL )
    {
        if ( qsee_hash_free_ctx(hash_ctx) < 0 )
        {
            MACC_LOG(MACC_LOG_MSG_ERROR, "Error: hash free failed");
            ret = MACCHIATO_ERROR_HASH_FREE;
        }
        hash_ctx = NULL;
    }
            
    if (ret)
    {
        MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_ECIES_KDF2 failed {%d}", ret);
    }
    else
    {
        MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_ECIES_KDF2 succeeded.");
    }
    
    return ret;
}

// ////////////////////////////////////////////////////////////////////

// The SC1 encryption based on AES in CBC mode.
// Supports all admissible AES key lenghts (16,24,32 bytes)

static
int macchiato_SC1_encrypt(
    const uint8_t   * key, 
    const uint32_t    key_len,
    const uint8_t   * msg, 
    const uint32_t    msg_len,
    const uint8_t   * iv_orig,
    uint8_t         * out,
    uint32_t        * out_len)
{
    uint32_t          intlen, pad_len, total_len;
    uint32_t          outlen;
    uint32_t          i;
    uint8_t           pad_len8;
    uint8_t           lastblock[AES_BLOCKSIZE];
    uint8_t           iv[AES_BLOCKSIZE];
    //aes_context_t     context;
    qsee_cipher_ctx  *cipher_ctx = 0;
    int               ret = MACCHIATO_SUCCESS;
    QSEE_CIPHER_MODE_ET mode = QSEE_CIPHER_MODE_CBC;

    do
    {
        /* sanity check */
        if (!key || !msg || !out || !out_len )
        {
            MACC_LOG(MACC_LOG_MSG_ERROR, " invalid input = NULL");
            ret = MACCHIATO_INVALID_INPUT_ERROR;
            break;
        }

        /* determine padding lenght, this is in [1..AES_BLOCKSIZE] */
        pad_len = AES_BLOCKSIZE - (msg_len % AES_BLOCKSIZE);
        pad_len8 = (uint8_t) pad_len; 

        /* total length of ciphertext equal to message length plus padding */
        total_len = msg_len + pad_len;

        /* lenght of the "integral" part of the message */
        /* (the padding is "fractional part" + padding */
        intlen = total_len - AES_BLOCKSIZE;
        
        /* copy fractional block to special buffer ... */
        for(i=0; i<AES_BLOCKSIZE-pad_len; i++)
        {
            lastblock[i] = msg[i+intlen];
        }

        /* ... and complete that buffer it with padding */
        for(i=AES_BLOCKSIZE-pad_len; i<AES_BLOCKSIZE; i++)
        {
            lastblock[i] = pad_len8;
        }

        /* init cipher context */
        if (qsee_cipher_init(QSEE_CIPHER_ALGO_AES_128, &cipher_ctx) < 0)
        {
            ret = MACCHIATO_ERROR_CIPHER_INIT;
            MACC_LOG(MACC_LOG_MSG_ERROR, "Error: init failed {%d}", ret);
            break;
        }

        /* set key */
        //aes_set_key(key, key_len, AES_ENCRYPT_KEY, &context);
        if (qsee_cipher_set_param(cipher_ctx, QSEE_CIPHER_PARAM_KEY, key, key_len) < 0)
        {
            ret = MACCHIATO_ERROR_CIPHER_SET_KEY;
            MACC_LOG(MACC_LOG_MSG_ERROR, "Error: set key failed {%d}", ret);
            break;
        }

        /* set block CBC mode */
        if (qsee_cipher_set_param(cipher_ctx, QSEE_CIPHER_PARAM_MODE, &mode, sizeof(mode)) < 0)
        {
            ret = MACCHIATO_ERROR_CIPHER_SET_MODE;
            MACC_LOG(MACC_LOG_MSG_ERROR, "Error: set mode failed {%d}", ret);
            break;
        }

#if GENERATE_IV
        memcpy(iv,iv_orig,AES_BLOCKSIZE);
#else
        memset(iv,0,AES_BLOCKSIZE);
#endif
        /* set IV */
        if(qsee_cipher_set_param(cipher_ctx, QSEE_CIPHER_PARAM_IV, iv, AES_BLOCKSIZE) < 0)
        {
            ret = MACCHIATO_ERROR_CIPHER_SET_IV;
            MACC_LOG(MACC_LOG_MSG_ERROR, "Error: set iv failed {%d}", ret);
            break;
        }

        /* encrypt integral part of message, then fractional part + padding */
        outlen = intlen;
        if (qsee_cipher_encrypt(cipher_ctx, msg, intlen, out, &outlen) < 0 || 
            intlen != outlen)
        {
            ret = MACCHIATO_ERROR_CIPHER_ENCRYPT;
            MACC_LOG(MACC_LOG_MSG_ERROR, "Error: qsee_cipher_encrypt failed {%d}", ret);
            break;
        }
        outlen = AES_BLOCKSIZE;
        if (qsee_cipher_encrypt(cipher_ctx, lastblock, AES_BLOCKSIZE, out+intlen, &outlen) < 0 ||
            outlen != AES_BLOCKSIZE)
        {
            ret = MACCHIATO_ERROR_CIPHER_ENCRYPT;
            MACC_LOG(MACC_LOG_MSG_ERROR, "Error: qsee_cipher_encrypt 2 failed {%d}", ret);
            break;
        }
        *out_len = total_len;
    }while(0);

    if (cipher_ctx)
    {
        if (qsee_cipher_free_ctx(cipher_ctx) < 0)
        {
          MACC_LOG(MACC_LOG_MSG_ERROR, "Error: Cipher DeInit failed!");
          ret = MACCHIATO_ERROR_CIPHER_DEINT;
        }
        cipher_ctx = NULL;
    }
  
    MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_SC1_encrypt returns {%d}", ret);
    return ret;
}

// ////////////////////////////////////////////////////////////////////

// The corresponding decryption

static
int macchiato_SC1_decrypt(
    const uint8_t   * key,
    const uint32_t    key_len,
    const uint8_t   * msg,
    const uint32_t    msg_len,
    const uint8_t   * iv_orig,
    uint8_t         * out,
    uint32_t        * out_len,
    const uint32_t    max_len)
{
    uint32_t              pad_len=0, total_len=0;
    uint32_t              i=0;
    uint8_t               pad_len8_t=0;
    uint8_t               iv[AES_BLOCKSIZE]={0};
    qsee_cipher_ctx      *cipher_ctx = NULL;
    int                   err = MACCHIATO_SUCCESS;
    uint32_t              outlen = 0;
    QSEE_CIPHER_MODE_ET   mode = QSEE_CIPHER_MODE_CBC;

    do
    {    
    
        /* sanity check */
        if (!key || !msg || !out || !out_len )
        {
            MACC_LOG(MACC_LOG_MSG_ERROR, "invalid input = NULL");
            err = MACCHIATO_INVALID_INPUT_ERROR;
            break;
        }

        if (msg_len % AES_BLOCKSIZE)
        {
            MACC_LOG(MACC_LOG_MSG_ERROR, "msg_len % AES_BLOCKSIZE = 0");
            err = MACCHIATO_ERROR_NOT_ALIGNMENT;
            break;
        }

        total_len = msg_len;

        /* init cipher context */
        if (qsee_cipher_init(QSEE_CIPHER_ALGO_AES_128, &cipher_ctx) < 0)
        {
            err = MACCHIATO_ERROR_CIPHER_INIT;
            MACC_LOG(MACC_LOG_MSG_ERROR, "Error: set algo failed  {%d}", err);
            break;
        }

        /* set key and iv */
        if (qsee_cipher_set_param(cipher_ctx, QSEE_CIPHER_PARAM_KEY, key, key_len) < 0)
        {
            err = MACCHIATO_ERROR_CIPHER_SET_KEY;
            MACC_LOG(MACC_LOG_MSG_ERROR, "Error: set key failed  {%d}", err);
            break;
        }

        /* set block CBC mode */
        if (qsee_cipher_set_param(cipher_ctx, QSEE_CIPHER_PARAM_MODE, &mode, sizeof(mode)) < 0)
        {
            err = MACCHIATO_ERROR_CIPHER_SET_MODE;
            MACC_LOG(MACC_LOG_MSG_ERROR, "Error: set mode failed  {%d}", err);
            break;
        }
  
#if GENERATE_IV
    memcpy(iv,iv_orig,AES_BLOCKSIZE);
#else
    memset(iv,0,AES_BLOCKSIZE);
#endif

        /* set IV */
        if(qsee_cipher_set_param(cipher_ctx, QSEE_CIPHER_PARAM_IV, iv, AES_BLOCKSIZE) < 0)
        {
            err = MACCHIATO_ERROR_CIPHER_SET_IV;
            MACC_LOG(MACC_LOG_MSG_ERROR, "Error: set iv failed  {%d}", err);
            break;
        }
        
        //AES_CBC_decrypt(msg, out, iv, msg_len, &cipher_ctx);
        outlen = msg_len;
        if(qsee_cipher_decrypt(cipher_ctx, msg, msg_len, out, &outlen) < 0)
        {
            err = MACCHIATO_ERROR_CIPHER_DECRYPT;
            MACC_LOG(MACC_LOG_MSG_ERROR, "Error: cipher decryption failed  {%d}", err);
            break;
        }

        /* determine padding lenght, this is in [1..AES_BLOCKSIZE] */
        pad_len8_t = out[msg_len-1];
        pad_len = pad_len8_t;
    
        /* verify padding */
        for (i=2;i<=pad_len;i++)
        {
            if (out[msg_len-i] != pad_len8_t)
            {
                err = MACCHIATO_ERROR_BAD_DATA;
                MACC_LOG(MACC_LOG_MSG_ERROR, "Error: bad data  {%d}", err);
                break;
            }
        }
        if (err)
        {
            break;
        }  
    
        memset(iv,0,AES_BLOCKSIZE);
    
        *out_len = total_len - pad_len;
    
        if (max_len != 0)
        {
            if (*out_len > max_len)
            {
                err = MACCHIATO_ERROR_BAD_DATA;
                MACC_LOG(MACC_LOG_MSG_ERROR, "Error: bad data 2 {%d}", err);
                break;
            }
        }
    
        MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_SC1_decrypt succeeded.");

    } while(0);
    
    if (cipher_ctx)
    {
        if (qsee_cipher_free_ctx(cipher_ctx) < 0)
        {
          MACC_LOG(MACC_LOG_MSG_ERROR, "Error: Cipher DeInit failed.");
          err = MACCHIATO_ERROR_CIPHER_DEINT;
        }
        cipher_ctx = NULL;
    }


    if (err)
    {
        memset(iv,0,AES_BLOCKSIZE);
        if ( (out) )
        {
            memset(out,0,total_len);
        }
    }

    if (err)
    {
        MACC_LOG(MACC_LOG_MSG_ERROR, "Error: macchiato_SC1_decrypt failed {%d}", err);
    }
    else
    {
        MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_SC1_decrypt succeeded");
    }
    
    return err;
}

// ////////////////////////////////////////////////////////////////////

// Elliptic Curve Integrated Encryption Scheme

// Cryptogram Format:
//
// the first 64 bytes are the elliptic curve point
// the last 32 are the tag
// everything in the middle is the ciphertext

#define CURVE_POINT_COORDINATE_SIZE 32

#define ENC_DERIVED_KEY_SIZE 16
#define MAC_DERIVED_KEY_SIZE 32

#if GENERATE_IV
#define ENC_DERIVED_IV_SIZE  16
#else
#define ENC_DERIVED_IV_SIZE  0
#endif

#define DERIVED_KEY_MATERIAL_SIZE (ENC_DERIVED_KEY_SIZE+ENC_DERIVED_IV_SIZE+MAC_DERIVED_KEY_SIZE)

// ////////////////////////////////////////////////////////////////////

// Elliptic Curve Integrated Encryption Scheme with Associated Data

// Cryptogram Format:
//
// - 64 bytes are the elliptic curve point
// -  4 bytes in big endian format, length of the associated data in bytes
// - the associated data (nothing it length is zero)
// -  4 bytes in big endian format, length of the ciphertext in bytes
//    (the ciphertext length is larges than the plaintext length)
// - the ciphertext
// - last 32 bytes are the tag.
//

// Big-endian systems store the most significant byte of a word in the
// smallest address and the least significant byte is stored in the
// largest address


static
void write_length_field(uint8_t * field, uint32_t value)
{
    field[0] = (uint8_t)(value >> 24);
    field[1] = (uint8_t)(value >> 16);
    field[2] = (uint8_t)(value >>  8);
    field[3] = (uint8_t) value;
}

static
uint32_t read_length_field(uint8_t * field)
{
    return (field[3] + (field[2] << 8) + (field[1] << 16) + (field[0] << 24));
}

int macchiato_ECIESAD_encrypt(
    const QSEE_affine_point_t  * recipient_public_key,
    const uint8_t         * AD,
    const uint32_t          AD_len,
    const uint8_t         * msg,
    const uint32_t          msg_len,
    uint8_t               * out,
    uint32_t              * out_len)
{
    int                     err = MACCHIATO_SUCCESS;
    QSEE_affine_point_t     local_public_key;
    QSEE_bigval_t           r;
    QSEE_affine_point_t     R;
    QSEE_bigval_t           xS;
    uint8_t                 xS_bitstring[CURVE_POINT_COORDINATE_SIZE];
    uint8_t                 derived_keys[DERIVED_KEY_MATERIAL_SIZE];
    uint8_t               * point_start;
    uint8_t               * AD_len_start;
    uint8_t               * AD_start;
    uint8_t               * ciphertext_len_start;
    uint8_t               * ciphertext_start;
    uint8_t               * hmac_start;
    uint32_t                ciphertext_len;
    uint32_t                total_len = 0;
    uint32_t                check_ciphertext_len = 0;

#define enc_key  derived_keys
#if GENERATE_IV
#define enc_iv  (derived_keys+ENC_DERIVED_KEY_SIZE)
#else
#define enc_iv  NULL
#endif
#define mac_key (derived_keys+ENC_DERIVED_KEY_SIZE+ENC_DERIVED_IV_SIZE)

    do
    {

        // Simple sanity check.
        if (!recipient_public_key || !msg || !msg_len || !out || !out_len
            || (msg_len > MAX_DATA_LEN) || (AD_len > MAX_DATA_LEN) )
        {
            MACC_LOG(MACC_LOG_MSG_ERROR, "Error: invalid arguments");
            MACC_LOG(MACC_LOG_MSG_ERROR, "recipient_public_key = 0x%x", recipient_public_key);
            MACC_LOG(MACC_LOG_MSG_ERROR, "msg = 0x%x, out = 0x%x", msg, out);
            MACC_LOG(MACC_LOG_MSG_ERROR, "msg_len = %d, AD_len = %d", msg_len, AD_len);
            err = MACCHIATO_ERROR_INVALID_ARG;
            break;
        }

        if ( !AD )  /* if AD == NULL then AD_len must be 0 */
        {
            if ( AD_len )
            {
                MACC_LOG(MACC_LOG_MSG_ERROR, "Error: invalid argument");
                err = MACCHIATO_ERROR_INVALID_ARG;
                break;
            }
        }

        // The recipient's public key must be a valid point on the curve.
        if (!qsee_in_curveP(recipient_public_key))
        {
            MACC_LOG(MACC_LOG_MSG_ERROR, "Error: Point not in curve");
            err = MACCHIATO_ERROR_POINT_NOT_IN_CURVE;
            break;
        }

        // Total length of encrypted part of the cryptogram equal to message length plus padding
        // Then add size of the ECC point and of the SHA256 tag    
        ciphertext_len = msg_len + (AES_BLOCKSIZE - (msg_len % AES_BLOCKSIZE));
        total_len = 2 * CURVE_POINT_COORDINATE_SIZE + 4 + AD_len + 4 + ciphertext_len + HMAC_SHA256_DIGEST_SIZE;

        // check the output buffer length 
        if (*out_len < total_len)
        {
            MACC_LOG(MACC_LOG_MSG_ERROR, "Error: output buffer size too small {%d}<{%d}", 
                     *out_len,  total_len);
            *out_len = total_len;
            err = MACCHIATO_SERVICE_BUFFER_TOO_SMALL;
            break;
        }

        // Determine the start of the various fields in the cryptogram
        // since we are at it, let us write also the lengths of the fields
        // and copy the AD into the cryptogram
        
        point_start = out;
        
        AD_len_start = out + 2 * CURVE_POINT_COORDINATE_SIZE;
        write_length_field(AD_len_start, AD_len);
        AD_start = AD_len_start + 4;
        if (AD && AD_len)
        {
            memcpy(AD_start, AD, AD_len);
        }

        ciphertext_len_start = AD_start + AD_len;
        write_length_field(ciphertext_len_start, ciphertext_len);
        ciphertext_start = ciphertext_len_start + 4;

        hmac_start = ciphertext_start + ciphertext_len;

        // Generate ephemeral public/private keys
        err = qsee_SW_ECDH_generate(&R, &r);
        if ( err != 0 )
        {
            err = MACCHIATO_ERROR_SW_ECDH_GENERATE;
            MACC_LOG(MACC_LOG_MSG_ERROR, "ECDH generate failed {%d}", err);
            break;
        }

        macc_bigval_to_binary((MACC_KEY_TYPE *)&R.x,point_start, CURVE_POINT_COORDINATE_SIZE);
        macc_bigval_to_binary((MACC_KEY_TYPE *)&R.y,point_start + CURVE_POINT_COORDINATE_SIZE,CURVE_POINT_COORDINATE_SIZE);

        // Generate temporary DH key and
        // derive the envelope key material (shared secret)
        memcpy((uint8*)&local_public_key, (uint8*)recipient_public_key, sizeof(local_public_key));

        if( qsee_SW_ECDH_derive(&xS, &r, &local_public_key) == FALSE )
        {
            err = MACCHIATO_ERROR_SW_ECDH_DERIVE;
            MACC_LOG(MACC_LOG_MSG_ERROR, "ECDH derive failed");
            break;
        }

        macc_bigval_to_binary((MACC_KEY_TYPE *)&xS, xS_bitstring, CURVE_POINT_COORDINATE_SIZE);

        // Now derive the encryption and MAC keys ...

        err = macchiato_ECIES_KDF2(xS_bitstring, CURVE_POINT_COORDINATE_SIZE,
                                derived_keys, DERIVED_KEY_MATERIAL_SIZE);

        if (err != MACCHIATO_SUCCESS)
        {
            MACC_LOG(MACC_LOG_MSG_ERROR, "Error: macchiato_ECIES_KDF2 failed {%d}", err);
            break;
        }

        // ... and (if ok) finally Encrypt-then-MAC, with error checking

        // Encrypt ...
        err = macchiato_SC1_encrypt(enc_key, ENC_DERIVED_KEY_SIZE,
                                    msg, msg_len,
                                    enc_iv,
                                    ciphertext_start,
                                    &check_ciphertext_len);
        if (err != MACCHIATO_SUCCESS)
        {
            MACC_LOG(MACC_LOG_MSG_ERROR, "Error: macchiato_SC1_encrypt failed {%d}", err);
            break;
        }

        if (check_ciphertext_len != ciphertext_len)
        {
            err = MACCHIATO_ERROR_ECIES_COMPARISON_FAILED;
            MACC_LOG(MACC_LOG_MSG_ERROR, "Error: ciphertext len incorrect {%d}", err);
            break;
        }

        // ... then MAC
        if (qsee_hmac(QSEE_HMAC_SHA256, AD_len_start, 4 + AD_len + 4 + ciphertext_len,
                      mac_key, MAC_DERIVED_KEY_SIZE, hmac_start) < 0)
        {
            err = MACCHIATO_ERROR_HMAC_FAILED;
            MACC_LOG(MACC_LOG_MSG_ERROR, "Error: qsee hmac failed {%d}", err);
            break;
        }

        * out_len = total_len;

    } while(0);

    if (err != MACCHIATO_SUCCESS)
    {
        if (out != NULL)
        {
            memset(out,0,total_len);
        }
    }

    if (err)
    {
        MACC_LOG(MACC_LOG_MSG_ERROR, "Error: macchiato_ECIESAD_encrypt returns {%d}", err);
    }
    else
    {
        MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_ECIESAD_encrypt succeeded");
    }
    return err; 
}

// ////////////////////////////////////////////////////////////////////

// AD_start is the address of a variable holding the pointer to the AD start
// AD_len is the address of a variable holding the length of the AD

int macchiato_ECIESAD_get_AD(
    const uint8_t   * cryptogram,
    const uint32_t    cryptogram_len,
    uint8_t        ** AD_start_p,
    uint32_t        * AD_len_p)
{
    uint8_t         * start;

    if (AD_start_p && AD_len_p)
    {
        start = (uint8_t *) cryptogram + 2 * CURVE_POINT_COORDINATE_SIZE;

        *AD_len_p = read_length_field(start);
        *AD_start_p = start + 4;
    
        MACC_LOG(MACC_LOG_MSG_ERROR, "succeed!");
        return MACCHIATO_SUCCESS;
    }
    else
    {
        MACC_LOG(MACC_LOG_MSG_ERROR, "failed!");
        return MACCHIATO_ERROR_INVALID_ARG;
    }
}

// This routine does not extract and store the associated data
// separately, as it is in the clear in the cryptogram.
// It does, however, fill *AD_start_p and *AD_len_p as the previous
// routine in case these pointers are not NULL (if NULL is passed,
// the pointer is ignored)

// *out will be allocated and replaced if the variable is NULL

int macchiato_ECIESAD_decrypt(
    const QSEE_bigval_t  * private_key,
    const uint8_t        * cryptogram,
    const uint32_t         cryptogram_len,
    uint8_t              * out,
    uint32_t             * out_len,
    uint8_t             ** AD_start_p,
    uint32_t             * AD_len_p)
{
    int                    err = MACCHIATO_SUCCESS;
    QSEE_bigval_t          local_private_key;
    QSEE_affine_point_t    R;
    QSEE_bigval_t          xS;
    uint8_t                xS_bitstring[CURVE_POINT_COORDINATE_SIZE] = {0};
    uint8_t                derived_keys[DERIVED_KEY_MATERIAL_SIZE] = {0};
    uint8_t                hmac_local[HMAC_SHA256_DIGEST_SIZE] = {0};
    uint8_t              * point_start = NULL;
    uint8_t              * AD_len_start = NULL;
    uint8_t              * AD_start = NULL;
    uint8_t              * ciphertext_len_start = NULL;
    uint8_t              * ciphertext_start = NULL;
    uint8_t              * hmac_start = NULL;
    uint32_t               AD_len = 0;
    uint32_t               ciphertext_len = 0;

    do
    {
        // Simple sanity check.
        if (!out || !private_key || !cryptogram || !out_len
            || cryptogram_len<(2*CURVE_POINT_COORDINATE_SIZE+HMAC_SHA256_DIGEST_SIZE))
        {
            MACC_LOG(MACC_LOG_MSG_ERROR, "Error: invalid arguments!");
            err = MACCHIATO_ERROR_INVALID_ARG;
            break;
        }

        // Determine the start of the various fields in the cryptogram
        point_start = (uint8_t *) cryptogram;
        AD_len_start = point_start + 2 * CURVE_POINT_COORDINATE_SIZE;
        AD_len =  read_length_field(AD_len_start);

        if (AD_len > MAX_DATA_LEN)
        {
            MACC_LOG(MACC_LOG_MSG_ERROR, "Error: bad AD data len");
            err = MACCHIATO_ERROR_BAD_DATA;
            break;
        }

        AD_start = AD_len_start + 4;

        if (AD_len_p)
        {
            *AD_len_p = AD_len;
        }

        if (AD_start_p)
        {
            *AD_start_p = AD_start;
        }

        ciphertext_len_start = AD_start + AD_len;
        ciphertext_len =  read_length_field(ciphertext_len_start);

        if (ciphertext_len > MAX_DATA_LEN)
        {
            err = MACCHIATO_ERROR_BAD_DATA;
            MACC_LOG(MACC_LOG_MSG_ERROR, "Error: bad AD data %d", ciphertext_len);
            break;
        }
        
        // check the output buffer length 
        if (*out_len < ciphertext_len)
        {
            MACC_LOG(MACC_LOG_MSG_ERROR, "Error: output buffer size too small {%d}<{%d}", 
                     *out_len,  ciphertext_len);
            *out_len = ciphertext_len;
            err = MACCHIATO_SERVICE_BUFFER_TOO_SMALL;
            break;
        }

        ciphertext_start = ciphertext_len_start + 4;
        hmac_start = ciphertext_start + ciphertext_len;

        // Internal consistency check - golden rule: if there is a possibility of
        // an internal consistency check, perform it!
        if (hmac_start != (ciphertext_start+ciphertext_len))
        {
            err =  MACCHIATO_ERROR_BAD_DATA;
            MACC_LOG(MACC_LOG_MSG_ERROR, "Error: bad hmac_start");
            break;
        }

        // Read in point R -- this part is dependent on internals of the ecc library...
        macc_binary_to_bigval(point_start, (MACC_KEY_TYPE *)&R.x, CURVE_POINT_COORDINATE_SIZE);
        macc_binary_to_bigval(point_start+CURVE_POINT_COORDINATE_SIZE, (MACC_KEY_TYPE *)&R.y, CURVE_POINT_COORDINATE_SIZE);

#ifdef OLD_LIB
    R.infinity = B_FALSE;
#endif
#ifdef NEW_LIB
    R.flags = QRLBN_POINT_AFFINE;
#endif

        // Verify if point in header is on curve
        if (!qsee_in_curveP(&R))
        {
            err = MACCHIATO_ERROR_POINT_NOT_IN_CURVE;
            MACC_LOG(MACC_LOG_MSG_ERROR, "Error: point is not in curve");
            break;
        }

        memcpy((uint8*)&local_private_key,(uint8*)private_key, sizeof(local_private_key));
        // Generate temporary DH key and
        // derive the envelope key material (shared secret)
        
        if ( qsee_SW_ECDH_derive(&xS, &local_private_key, &R) == FALSE )
        {
            err = MACCHIATO_ERROR_SW_ECDH_DERIVE;
            MACC_LOG(MACC_LOG_MSG_ERROR, "ECDH derive failed");
            break;
        }

        macc_bigval_to_binary((MACC_KEY_TYPE *)&xS, xS_bitstring, CURVE_POINT_COORDINATE_SIZE);

        // Now derive the encryption and MAC keys ...
        err = macchiato_ECIES_KDF2(xS_bitstring, CURVE_POINT_COORDINATE_SIZE,
                               derived_keys, DERIVED_KEY_MATERIAL_SIZE);
        if (err != MACCHIATO_SUCCESS)
        {
            MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_ECIES_KDF2 failed {%d}", err);
            break;
        }

        // ... and (if ok) finally MAC-then-decrypt, with error checking
        if (qsee_hmac(QSEE_HMAC_SHA256, AD_len_start, 4 + AD_len + 4 + ciphertext_len,
                    mac_key, MAC_DERIVED_KEY_SIZE, hmac_local) < 0)
        {
            MACC_LOG(MACC_LOG_MSG_ERROR, "qsee_hmac256 failed!");
            err = MACCHIATO_ERROR_HMAC_FAILED;
            break;
        }

        if (memcmp(hmac_local, hmac_start, HMAC_SHA256_DIGEST_SIZE) != 0)
        {
            memset(hmac_local,0,HMAC_SHA256_DIGEST_SIZE);
            MACC_LOG(MACC_LOG_MSG_ERROR, "memcmp failed: hmac digest incorrect");
            err = MACCHIATO_ERROR_ECIES_COMPARISON_FAILED;
            break;
        }

        err = macchiato_SC1_decrypt(enc_key, ENC_DERIVED_KEY_SIZE,
                                    ciphertext_start, ciphertext_len,
                                    enc_iv,
                                    out,
                                    out_len,
                                    MAX_DATA_LEN);
        if (err)
        {
            MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_SC1_decrypt failed");
            break;
        }

    } while(0);

    memset(&local_private_key, 0, sizeof(local_private_key));

    if (err)
    {
        MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_ECIESAD_decrypt failed {%d}", err);
    }
    else
    {
        MACC_LOG(MACC_LOG_MSG_ERROR, "macchiato_ECIESAD_decrypt succeeded.");
    }
    
    return err;
}

#undef enc_key
#undef enc_iv 
#undef mac_key

// ////////////////////////////////////////////////////////////////////


