#ifndef QSEE_MACCHIATO_H_
#define QSEE_MACCHIATO_H_

/** @file macchiato.h
 * @brief
 * This file contains the definitions of the constants, data structures
 * and interfaces to the QSEE Macchiato key provisioning lib.
 */

/*===========================================================================
 Copyright (c) 2014 Qualcomm Technologies, Inc.
 All Rights Reserved.
 Qualcomm Technologies Proprietary and Confidential.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/22/14   cz      Added an error code: MACCHIATO_ERROR_FEATURE_NOT_SUPPORTED
08/14/14   qxu     Added return errors
05/26/14   sn      Initial Version.

===========================================================================*/
#include <stdint.h>
#include "macchiato_service.h"

#define MACCHIATO_CHALLENGE_RESPONSE_LEN   16

#define MACCHIATO_ERROR_BASE QSEE_MACC_KEYGEN_MAX_ERROR
/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/

/* Error codes */
 typedef enum {
    MACCHIATO_SUCCESS                                = 0,
    MACCHIATO_GENERAL_ERROR                          = MACCHIATO_ERROR_BASE + 1, 

    /*macchiato_ecc.c*/	                            
    MACCHIATO_SIGNATURE_BUFFER_TOO_SMALL             = MACCHIATO_ERROR_BASE + 2,   /* memory buffer from caller is too small */
    MACCHIATO_HASH_ERROR                             = MACCHIATO_ERROR_BASE + 3,   /* hash operation error */
    MACCHIATO_SIGNATURE_NOMATCH                      = MACCHIATO_ERROR_BASE + 4,   /* signature doesn't match */
    MACCHIATO_ECDSA_ERROR                            = MACCHIATO_ERROR_BASE + 5,   /* ecdsa operation error */

    /*macchiato_service.c*/	    
    MACCHIATO_INVALID_PARAM                          = MACCHIATO_ERROR_BASE + 6,   /* Invalid arguments passed to the API */
    MACCHIATO_NOT_SUPPORTED                          = MACCHIATO_ERROR_BASE + 7,   /* Operation Not supported */
    MACCHIATO_SERVICE_BUFFER_TOO_SMALL	             = MACCHIATO_ERROR_BASE + 8,   /* Macchiato service buffer is too small*/


    /*macchiato.c*/	    
    MACCHIATO_FAILED_GET_SECURE_STATE                = MACCHIATO_ERROR_BASE + 9,   /* failed to get MSA/APP secure state */
    MACCHIATO_UNSUCCESSFUL_ERROR_CODE                = MACCHIATO_ERROR_BASE + 10,  /* unsuccessful error code*/
    MACCHIATO_FAILED_SIGN_DATA                       = MACCHIATO_ERROR_BASE + 11,  /* signing service data failed */
    MACCHIATO_TOO_SHORT_OUT_BUFFER_ERROR             = MACCHIATO_ERROR_BASE + 12, 
    MACCHIATO_ALLOCATION_ERROR                       = MACCHIATO_ERROR_BASE + 13,
    MACCHIATO_GOT_NULL_INPUT_ERROR                   = MACCHIATO_ERROR_BASE + 14,
    MACCHIATO_FAILED_GENERATE_RANDOM_RESPONSE        = MACCHIATO_ERROR_BASE + 15, 
    MACCHIATO_FAILED_GENERATE_KEYS                   = MACCHIATO_ERROR_BASE + 16,
    MACCHIATO_INVALID_INPUT_ERROR                    = MACCHIATO_ERROR_BASE + 17,
    MACCHIATO_FAILED_STORING_SRVICE_KEY              = MACCHIATO_ERROR_BASE + 18,
    MACCHIATO_PROTOCOL_VERSION_MISMATCH              = MACCHIATO_ERROR_BASE + 19,
    MACCHIATO_ERROR_UNEXPECTED_MSG_ID                = MACCHIATO_ERROR_BASE + 20,
    MACCHIATO_ERROR_FEATURE_NOT_SUPPORTED            = MACCHIATO_ERROR_BASE + 21, 
    MACCHIATO_ERROR_HASH_INIT                        = MACCHIATO_ERROR_BASE + 22,
    MACCHIATO_ERROR_HASH_UPDATE                      = MACCHIATO_ERROR_BASE + 23,
    MACCHIATO_ERROR_HASH_FINAL                       = MACCHIATO_ERROR_BASE + 24,
    MACCHIATO_ERROR_HASH_FREE                        = MACCHIATO_ERROR_BASE + 25,
    MACCHIATO_ERROR_CIPHER_INIT                      = MACCHIATO_ERROR_BASE + 26,
    MACCHIATO_ERROR_CIPHER_SET_KEY                   = MACCHIATO_ERROR_BASE + 27,
    MACCHIATO_ERROR_CIPHER_SET_MODE                  = MACCHIATO_ERROR_BASE + 28,
    MACCHIATO_ERROR_CIPHER_SET_IV                    = MACCHIATO_ERROR_BASE + 29,
    MACCHIATO_ERROR_CIPHER_ENCRYPT                   = MACCHIATO_ERROR_BASE + 30,
    MACCHIATO_ERROR_CIPHER_DECRYPT                   = MACCHIATO_ERROR_BASE + 31,
    MACCHIATO_ERROR_CIPHER_DEINT                     = MACCHIATO_ERROR_BASE + 32,
    MACCHIATO_ERROR_BAD_DATA                         = MACCHIATO_ERROR_BASE + 33,
    MACCHIATO_ERROR_INVALID_ARG                      = MACCHIATO_ERROR_BASE + 34,
    MACCHIATO_ERROR_HMAC_FAILED                      = MACCHIATO_ERROR_BASE + 35,
    MACCHIATO_ERROR_ECIES_COMPARISON_FAILED          = MACCHIATO_ERROR_BASE + 36,
    MACCHIATO_ERROR_SW_ECDH_GENERATE                 = MACCHIATO_ERROR_BASE + 37,
    MACCHIATO_ERROR_SW_ECDH_DERIVE                   = MACCHIATO_ERROR_BASE + 38,
    MACCHIATO_ERROR_PRNG_DATA                        = MACCHIATO_ERROR_BASE + 39,
    MACCHIATO_ERROR_POINT_NOT_IN_CURVE               = MACCHIATO_ERROR_BASE + 40,
    MACCHIATO_ERROR_NOT_ALIGNMENT                    = MACCHIATO_ERROR_BASE + 41,

    
    MACCHIATO_ERR_SIZE                  = 0x7FFFFFFF
 } macchiato_err_t;



 /* generic buffer parameter */
 typedef struct{
	uint8_t* pu8Buffer;
	uint32_t u32Len;
} buffer_t;

#pragma pack (push, macchiato, 1)

/* This struct represents the common header fields in every Macchiato message. */
struct macchiato_msg_hdr_common_fields{
	uint32_t u32ProtocolVersion;
	uint32_t u32MsgId;
	uint32_t u32MsgLength;
	uint32_t u32ErrorCode;
};


/* This struct represents the Macchiato challenge message. */
struct macchiato_challenge_msg{
	uint8_t challenge[MACCHIATO_CHALLENGE_RESPONSE_LEN];
};

#pragma pack (pop, macchiato)

/*----------------------------------------------------------------------------
 * Function Declarations and Documentation
 * -------------------------------------------------------------------------*/
 


 
  /**
  @brief macchiato_sign_service_data
  Use this function to sign service data with the device Machhiato private key.
  
  @param[in]    serviceData               - buffer that contains the service data to be signed.
  @param[in/out]   signedServiceData         - buffer (also allocated by the calling app) to fill the signature output in the following format:
	                                                                         <----------------------------------------------------------------- ECC signature covers this area -------------------------------------------------------------------->
	 -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	|      32 bits     | 32 bits |   32 bits  |   32 bits  |   512 bits    |          512 bits         |           512 bits           | 256 bits |   32 bits   | 32 bits each |  16 bits   |    32 bits    |  128 bits | 128 bits | variable length |
	|------------------|---------|------------|------------|---------------|---------------------------|------------------------------|----------|-------------|--------------|------------|---------------|-----------|----------|-----------------|
	| Protocol Version | MSG ID  | MSG length | Error Code | ECC Signature | Device Public Signing Key | Device Public Encryption Key | TA Hash  | Num Svc IDs |    Svc IDs   | HW Version | Serial Number | Challenge | Response |   Opaque Data   |
	 -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
												  
	Note: The byte order is LITTLE ENDIAN !!! (except the keys and the signature)
	Svc IDs are the identifiers of the calling QSEE application.
	All the buffers should be allocated by the caller.
	The challenge field is zeroed.
												  
  @param[out]  pu32SignedServiceDataLen  - the length of the signature output in bytes.
  
  @return
  MACCHIATO_SUCCESS � success
  MACCHIATO_GOT_NULL_INPUT_ERROR � received null pointer
  MACCHIATO_TOO_SHORT_OUT_BUFFER_ERR - the signature output overflows the given buffer (signedServiceData), the required buffer size is returned in pu32SignedServiceDataLen
  MACCHIATO_FAILED_GET_SECURE_STATE - failed to get the error code for the message
  MACCHIATO_FAILED_SIGN_DATA - failed signing the data using Macchiato private key
  

  @dependencies
  None.

  @sideeffects
  None.
*/
 macchiato_err_t macchiato_sign_service_data(buffer_t serviceData, 
											buffer_t signedServiceData, 
											uint32_t* pu32SignedServiceDataLen);
											
  /**
  @brief macchiato_authenticate_device
  Use this function to authenticate the device using challenge response with Machhiato private key.

  This function also outputs the device id and Macchiato public key to enable signature verification.
  
  @param[in]   challenge                    - buffer that contains the authentication challenge.

  @param[in]   opaqueData                   - buffer that contains additional opaque data to be signed (optional).
  @param[in/out]  signedChallengeResponse   - buffer (also allocated by the calling app) to fill the challenge response output in the following format:
                                                                            <----------------------------------------------------------------- ECC signature covers this area -------------------------------------------------------------------->
	 -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	|      32 bits     | 32 bits |   32 bits  |   32 bits  |   2048 bits   |          2048 bits        |           2048 bits          | 256 bits |   32 bits   | 32 bits each |  16 bits   |    32 bits    |  128 bits | 128 bits | variable length |
	|------------------|---------|------------|------------|---------------|---------------------------|------------------------------|----------|-------------|--------------|------------|---------------|-----------|----------|-----------------|
	| Protocol Version | MSG ID  | MSG length | Error Code | RSA Signature | Device Public Signing Key | Device Public Encryption Key | TA Hash  | Num Svc IDs |    Svc IDs   | HW Version | Serial Number | Challenge | Response |   Opaque Data   |
	 -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
										  
	Note: The byte order is LITTLE ENDIAN !!!(except the keys and the signature)
	Svc IDs are the identifiers of the calling QSEE application.
	All the buffers should be allocated by the caller.
  
  @param[out]  pu32SignedChallengeResponseLen  - the length of the challenge response output in bytes.
  
  @return
  MACCHIATO_SUCCESS � success
  MACCHIATO_GOT_NULL_INPUT_ERROR � received null pointer
  MACCHIATO_TOO_SHORT_OUT_BUFFER_ERR - the signature output overflows the given buffer (signedChallengeResponse), the required buffer size is returned in pu32SignedChallengeResponseLen
  MACCHIATO_FAILED_GET_SECURE_STATE - failed to get the error code for the message
  MACCHIATO_FAILED_SIGN_DATA - failed signing the data using Macchiato private key
  MACCHIATO_ALLOCATION_ERROR - failed to allocate helper buffer
  MACCHIATO_FAILED_GENERATE_RANDOM_RESPONSE - failed to generate random response
  @dependencies
  None.

  @sideeffects
  None.
*/
 
 macchiato_err_t macchiato_authenticate_device(buffer_t challenge, 
												buffer_t opaqueData, 
												buffer_t signedChallengeResponse, 
												uint32_t* pu32SignedChallengeResponseLen);

 /**
 @brief macchiato_provision_service_key
 Use this function to get the unwrapped service key from the Macchiato provisioning message.

 @param[in]   provisionMsg          - buffer that contains the provisioning message of the following format:
  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	   <------------------------ RSA signature covers this area ------------------------>
 	 --------------------------------------------------------------------------------------------------------------------------------------------------------
	|      32 bits     | 32 bits |   32 bits  |   32 bits  |   2048 bits   |  variable length | 128 bits |          2048 bits         |    variable length   |
	|------------------|---------|------------|------------|---------------|------------------|----------|----------------------------|----------------------|
	| Protocol Version | MSG ID  | MSG length | Error Code | RSA Signature | KPSP Certificate | Response | ECC Encrypted Wrapping Key | Wrapped Service Key  |
	 --------------------------------------------------------------------------------------------------------------------------------------------------------


 @param[in/out]   unwrappedKey - A buffer to contain the unwrapped service key, the maximum size should be known to the calling application.
 @param[out]      pu32UnwarppedKeyLen

 @return
 MACCHIATO_SUCCESS � success
 MACCHIATO_GOT_NULL_INPUT_ERROR � received null pointer
 @dependencies
 None.

 @sideeffects
 None.
*/

macchiato_err_t macchiato_provision_service_key(buffer_t provisionMsg,
												buffer_t unwrappedKey,
												uint32_t* pu32UnwarppedKeyLen);


#endif /* QSEE_MACCHIATO_H_ */
