/**
 * Copyright 2014 QUALCOMM Technologies, Inc. 
 * All Rights Reserved.
 * QUALCOMM Confidential and Proprietary
 *
 * @file qsee_macchiato.h
 * @author	qxu
 * @version     1.0
 * @since       2014-05-16
 *
 * @brief header file for macchiato_key_service.c
 */

/*===========================================================================
   Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
  
   This file contains the definitions of the constants, data structures
   and interfaces that provide Macchiato RSA Key service support. It defines
   the external interfaces for Macchiato RSA key encrpytion/decryption, signature/
   verification, key generation.
  ===========================================================================*/
/*===========================================================================

                            EDIT HISTORY FOR FILE

  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $

  when       who      what, where, why
  --------   ---      ------------------------------------
  28/5/14    qxu      First version of qsee_macchiato.h 

===========================================================================*/

#ifndef MACCHIATO_SERVICE_H
#define MACCHIATO_SERVICE_H
#include <stddef.h>
#include "qsee_keyprov.h"
#include "ecc_core.h"
#include "qsee_hash.h"
#include "qsee_cipher.h"
#include "qsee_ecc.h"
#include "qsee_log.h"

#define MACC_LOG QSEE_LOG
#define MACC_LOG_MSG_ERROR QSEE_LOG_MSG_ERROR
#define MACC_LOG_MSG_DEBUG QSEE_LOG_MSG_DEBUG

#define MAX_PUB_EXP_SIZE 5 // Array of public exponent e.g., {0x01, 0x00, 0x01}

#define MACCHIATO_MSG_SIGN_PUBKEY_LEN      64 /*512 bits*/
#define MACCHIATO_MSG_ENC_PUBKEY_LEN       64 /*512 bits*/

#define MACCHIATO_MSG_TA_HASH_LEN          32 /*256 bits*/
#define MACCHIATO_MSG_NUM_SVCID_LEN        4  /*32 bits*/
#define MACCHIATO_MSG_SVCID_LEN            4  /*32 bits*/

#define MACCHIATO_MSG_HW_VERSION_LEN       2  /*16 bits*/
#define MACCHIATO_MSG_SERIAL_NUM_LEN       4  /*32 bits*/

#define MACCHIATO_MSG_SIGNATURE_LEN        64 /*512 bits*/

#define MACCHIATO_KEYTYPE_SIGN				1
#define MACCHIATO_KEYTYPE_ENC				2
/* 
#define MACCHIATO_MSG_MAX_NUM_SVCID        4  / *maximum 4 service ids* / 
#define MACCHIATO_MAX_MSG_HEADER_LEN       (MACCHIATO_MSG_TA_HASH_LEN \
                                                 + MACCHIATO_MSG_NUM_SVCID_LEN \
                                                 + MACCHIATO_MSG_MAX_NUM_SVCID * MACCHIATO_MSG_SVCID_LEN)
*/

typedef QSEE_bigval_t MACC_KEY_TYPE;
typedef QSEE_ECDSA_sig_t MACC_SIG_TYPE;
typedef QSEE_affine_point_t MACC_POINT_TYPE;

/**
 * @brief  This function will pre-append the opaque data provided by TA with
 *  	   service ids list associated with the TA, hash the data blob with
 *  	   SHA-256 and sign signature with PKCS #1 padding.
 *
 * @param[in] opaque_data 		   the data buffer contained the data to be signed
 *  	 						         from TA
 * @param[in] opaque_data_len 	the length of the opaque data 
 * @param[out] signed_data 		the output data which has been signed, including
 *  							         Num SVC ID�s, SVC ID�s, Opaque Data. Data buffer
 * 								      provided by TA.
 * @param[out] signed_data_len	the size of the signed data 
 * @param[out] signature   		The signature
 * @param[in,out] siglen   		The max size and resulting size of the signature
 *
 * @return 0 on success, negative on failure
*/
int macchiato_service_ecc_sign
(
   const unsigned char       *opaque_data, 
   unsigned int               opaque_data_len,
   unsigned char             *signed_data,
   unsigned int              *signed_data_len,
   unsigned char             *signature,
   unsigned int              *signature_len
);

/**
 * @brief  This function will hash the signed data provided by TA with SHA-256, 
 *  	   calcuate the signature with PKCS #1 padding and verify it with
 *  	   provided signature.
 *
 * @param[in] signed_data  		The data buffer contained the signed data from 
 *  	 						         TA
 * @param[in] signed_data_len   The length of signed data 
 * @param[in] signature         The signature to be verified provided by TA
 * @param[in] signature_len     The size the signature 
 *
 * @return 0 on success, negative on failure
*/
int macchiato_service_ecc_verify
(
   unsigned char             *signed_data, 
   unsigned int               signed_data_len,
   unsigned char             *signature, 
   unsigned int               signature_len
);

/**
 * @brief This function will encrypt the plain message provided by TA with PKCS 
 *  	  #1 padding.
 *
 * @param[in] msg           The plaintext
 * @param[in] msglen        The length of the plaintext
 * @param[out] cipher       The ciphertext 
 * @param[in,out] cipherlen The max size and resulting size of the ciphertext 
 *
 * @return 0 on success, negative on failure
*/
int macchiato_service_ecc_encrypt
(
   const unsigned char     *msg, 
   unsigned int             msglen,
   unsigned char           *cipher,
   unsigned int            *cipherlen
);

/**
 * @brief  This function will decrpyed the cipher message provided by TA and 
 *  	   then v1.5 depad.
 *
 * @param[in] cipher        The ciphertext
 * @param[in] cipherlen     The length of the ciphertext(octets)
 * @param[out] msg          The plaintext 
 * @param[in,out] msglen    The max size and resulting size of the plaintext 
 *
 * @return 0 on success, negative on failure
*/
int macchiato_service_ecc_decrypt
(
   unsigned char           *cipher,
   unsigned int             cipherlen,
   unsigned char           *msg,
   unsigned int            *msglen
);

int macchiato_ecc_get_pubkey
(
   MACC_KEY_TYPE *privkey, 
   unsigned char *p_marshalled_key
);

int macchiato_ecc_sign
(
   MACC_KEY_TYPE *privkey,
   unsigned char *signed_data,
   unsigned int signed_data_len,
   unsigned char *signature,
   unsigned int  *signature_len
);

int macchiato_ecc_verify
(
   MACC_KEY_TYPE *privkey,
   unsigned char *signed_data,
   unsigned int signed_data_len,
   unsigned char *signature
);

#endif //MACCHIATO_SERVICE_H

