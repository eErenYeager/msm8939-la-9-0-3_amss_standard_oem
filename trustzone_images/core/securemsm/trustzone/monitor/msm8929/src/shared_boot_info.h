#ifndef SHARED_BOOT_INFO_H
#define SHARED_BOOT_INFO_H

#include <comdef.h>
#include <secboot.h>

#define BOOT_IMAGES_NUM_ENTRY 5

typedef struct boot_images_entry
{
  uint32 image_id;
  uint32 e_ident;
  uint64 entry_point;
  secboot_verified_info_type image_verified_info;
  uint32 reserved_1;
  uint32 reserved_2;
  uint32 reserved_3;
  uint32 reserved_4;
}boot_images_entry_type;

typedef struct pbl_secboot_shared_info_type
{
  uint32 magic1;
  uint32 magic2;
  uint32 version;
  uint32 num_images;
  uint32 reserved1;
  boot_images_entry_type boot_image_entry[BOOT_IMAGES_NUM_ENTRY];
} pbl_secboot_shared_info_type;

void mon_secboot_set_shared_info_arch ();
#endif /* SHARED_BOOT_INFO_H */
