#ifndef TCR_H
#define TCR_H

/*===========================================================================

                    T r u s t z o n e   T a r g e t
                          H e a d e r  F i l e

DESCRIPTION
  Describe MMU-specific register setting that outlines memory types

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None

Copyright (c) 2011-2012 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/monitor/msm8929/src/tcr.h#1 $
$DateTime: 2018/02/07 00:37:16 $
$Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/01/13   pre     Initial Revision
============================================================================*/

#include "tt_ld.h"

/*****************************************************************************
 *                             !!! CAUTION !!!                               *
 * TCR value must match memory attributes for translation table memory or    *
 * else really weird hard to debug crashes will happen.                      *
 *****************************************************************************/

#define TCR  (TCR_PS_4_GB | TCR_TG0_4KB | TCR_INNR_SH | \
              TCR_ORGN0_WB_WA | TCR_IRGN0_WB_WA |       \
              TCR_T0SZ_PWR2(32))

#endif /* TCR_H */
