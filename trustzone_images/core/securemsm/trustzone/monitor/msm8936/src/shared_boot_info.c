#include <comdef.h>
#include <string.h>
#include <monitor.h>
#include "shared_boot_info.h"
#include "mon_init_and_pc.h"

pbl_secboot_shared_info_type secboot_shared_info = {NULL};

/**
 * @brief Copies the secboot shared information locally to Monitor
 *
 * @param shared_info [in]  Pointer to info, provided by SBL
 */
void mon_secboot_set_shared_info_arch ()
{
  /* 8916A64 SBL does not send sbl_info */
  //shared_boot_info_params[0] param_0, param_1 was set by monitor
  memcpy(&secboot_shared_info, (void *)shared_boot_info_params[0], sizeof(pbl_secboot_shared_info_type));

  //shared_boot_info_params[0] should now point to monitor's copy of SBL Shared Info.
  shared_boot_info_params[0] = (uint32)&secboot_shared_info;
  shared_boot_info_params[1] = (uint32)NULL;
}//mon_secboot_set_shared_info_arch()
