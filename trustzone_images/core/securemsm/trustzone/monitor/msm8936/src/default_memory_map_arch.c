#include <comdef.h>
#include "memory_defs.h"
#include "default_memory_map_arch.h"
#include "util.h"

static p_addr_t os_rgn_base[1] = {SCL_TZ_CODE_BASE};
static size_t os_rgn_len[1] = {SCL_TZ_TOTAL_SIZE};

p_addr_t get_secure_os_entry_addr_arch(void)
{
  return os_rgn_base[0];
}

p_addr_t* get_secure_os_mem_rgns_arch(void)
{
  return &os_rgn_base[0];
}

size_t* get_secure_os_rgn_lens_arch(void)
{
  return &os_rgn_len[0];
}

uint32 get_num_secure_os_rgns_arch(void)
{
  return ARRAY_SIZE(os_rgn_base);
}

v_addr_t get_tz_image_imem_base_addr_arch(void)
{
  return get_secure_os_entry_addr_arch();
}

size_t get_tz_image_len_arch(void)
{
  return (size_t) SCL_TZ_TOTAL_SIZE;
}
