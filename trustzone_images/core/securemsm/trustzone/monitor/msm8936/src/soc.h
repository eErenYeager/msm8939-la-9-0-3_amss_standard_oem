#ifndef SOC_H
#define SOC_H

#define CPU_COUNT            8
#define NUM_CPUS_PER_CLUSTER 4
#define CLUSTER_COUNT        2

#ifndef _ARM_ASM_

#include "sys_regs.h"

extern uint32 l2_gdhs_state[CLUSTER_COUNT];
extern uint32 boot_cpu_idx;

__inline static el3_reg_t cur_cpu_num(void)
{
  return (((get_mpidr_el1() & 0xFF00) >> 8) * NUM_CPUS_PER_CLUSTER +
          (get_mpidr_el1() & 0xFF));
}

__inline static boolean get_cluster_num(void)
{
  return ((get_mpidr_el1() & 0xFF00) >> 8);
}

__inline static uint32 get_num_cpus_in_cluster(uint32 cluster_num)
{
  return NUM_CPUS_PER_CLUSTER;
}

__inline static boolean is_perf_cluster(void)
{
  return (cur_cpu_num() >= boot_cpu_idx);
}

__inline static boolean is_power_cluster_present(void)
{
  return (boot_cpu_idx == 4);
}

void soc_pc_entry(void);
void soc_pc_fallthrough(void);

#else
#include "sys_fields.h"

/* ----------------------------------------------------------------------
   MACRO: CurCPUNum
   ---------------------------------------------------------------------- */
   MACRO
   CurCPUNum $rx, $ry
      mrs     $rx, MPIDR_EL1
      and     $ry, $rx, #MPIDR_EL1_AFF1_MSK
      lsr     $ry, #MPIDR_EL1_AFF1_SHFT
      mov     $rx, #NUM_CPUS_PER_CLUSTER
      mul     $ry, $rx
      mrs     $rx, MPIDR_EL1
      and     $rx, #MPIDR_EL1_AFF0_MSK
      add     $rx, $ry
   MEND
/* ----------------------------------------------------------------------
   MACRO: check L2 GDHS flag for each cluster
   Pass cluster number in $rx and return the state also in $rx
   ---------------------------------------------------------------------- */
   MACRO
   PerClusterGdhsState $rx, $ry, $rz
     ldr     $rz, =l2_gdhs_state;
     add     $rz, $rz, $rx, lsl #0x2
     ldr     $rx, [$ry]
     MEND

/* ----------------------------------------------------------------------
   MACRO: clear L2 GDHS flag for each cluster
   Pass cluster number in $rx
   ---------------------------------------------------------------------- */
   MACRO
   ClearPerClusterGdhs $rx, $ry, $rz
     ldr     $rz, =l2_gdhs_state;
     add     $rz, $rz, $rx, lsl #0x2
     mov     $rx, #0x0
     str     $rx, [$ry]
     MEND 

#endif /* _ARM_ASM_ */
#endif /* SOC_H */
