/**
   @file mon_pc.c
   @brief EL3 soc-specific power collapse
*/
/*===========================================================================
   Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/monitor/msm8936/src/mon_init_and_pc_arch.c#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $


when       who      what, where, why
--------   ---     ------------------------------------------------------------
=============================================================================*/

#include <comdef.h>
#include "soc.h"
#include "sys_regs.h"
#include "a53_pc.h"
#include "memory_defs.h"
#include "mapper.h"
#include "tt_tz.h"
#include "default_memory_map_arch.h"
#include "mon_init_and_pc_arch.h"
#include "todo.h"
#include <HALhwio.h>
#include "mon_hwio.h"
#include "a53_sys_regs.h"

#define UNCACHED __attribute__((section("MONITOR_UNCACHED")))
#define SOC_ID_8939_R3   0x20010300
/* Global so it is shared with arch-specific assembly file:
   mon_cpu_reset_arch.s */
uint32 UNCACHED boot_lock_addr;
uint32 UNCACHED sdi_wa_flag;

extern uint32 cci_wa_done;
extern uint32 l2_spm_state_dbg[2];

extern void a53_aarch64_sysini(uint32 tcsr_hw_version);

void sysini_arch()
{
  /* TODO: request this in sysini */
  /* TODO: Make sure EL2 does not give access to NS EL1 or switch
     this register for every context switch. */
  a53_grant_el1_cpuectlr_access();

}

/* Use physical address before MMU is enabled */

// HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_ADDR
#undef  SECURITY_CONTROL_BASE
#define SECURITY_CONTROL_BASE    SECURITY_CONTROL_BASE_PHYS

// HWIO_TCSR_MEM_ACC_SEL_VDDAPC_ADDR
#undef  CORE_TOP_CSR_BASE
#define CORE_TOP_CSR_BASE    CORE_TOP_CSR_BASE_PHYS

void mon_disable_l2_tcm_arch()
{
    
  uint32 soc_id = 0;   
  soc_id = HWIO_IN(TCSR_SOC_HW_VERSION );
  
  HWIO_OUTF(APCS_ALIAS1_TCM_START_ADDR,TCM_REDIRECT_EN_0,0);
  if(is_power_cluster_present())
  {
    HWIO_OUTF(APCS_ALIAS0_TCM_START_ADDR,TCM_REDIRECT_EN_0,0);
  }
  __asm__ ("dmb sy" : : :);


  if (SOC_ID_8939_R3 == soc_id) 
  {
    HWIO_OUT(TCSR_CUSTOM_VDDAPC_ACC_0, 0xC0000);
    HWIO_OUT(TCSR_CUSTOM_VDDAPC_ACC_1, 0x3000); 
   /* SW Workaround for missing bootrom patch for APSS L1 & L2 ACC settings
   * only for 8939 */   
    if (0x40000000 & (HWIO_INI(QFPROM_CORR_MEM_CONFIG_ROWn_LSB, 0x0)))  /* if ROW0[30] == 1 */
    {
      HWIO_OUT(TCSR_MEM_ACC_SEL_VDDAPC , 0x1);
      
    }
    else
    {
      HWIO_OUT(TCSR_MEM_ACC_SEL_VDDAPC , 0x0);    
    }
  }
  else
  {
  /* SW Workaround for missing bootrom patch for APSS L1 & L2 ACC settings
   * only for 8939 */
  if (0x40000000 & (HWIO_INI(QFPROM_CORR_MEM_CONFIG_ROWn_LSB, 0x0)))  /* if ROW0[30] == 1 */
  {
    HWIO_OUT(TCSR_MEM_ACC_SEL_VDDAPC , 0x1);
    HWIO_OUT(TCSR_CUSTOM_VDDAPC_ACC_1, 0x3000);
  }
  else
  {
    HWIO_OUT(TCSR_MEM_ACC_SEL_VDDAPC , 0x0);
    HWIO_OUT(TCSR_CUSTOM_VDDAPC_ACC_1, 0x0);
  }
 }

  __asm__ ("dmb sy" : : :);
}

void mon_set_boot_lock_arch(uint32 lock_addr)
{
  /* TODO: some validation this memory is in EL1 */
  boot_lock_addr = lock_addr;
}

void mon_set_sdi_wa(boolean  sdi_flag)
{
  sdi_wa_flag = sdi_flag;
}

void mon_enable_cluster_coherency_arch(void)
{
  if(is_perf_cluster())
  {
    HWIO_OUTF(APCS_ALIAS1_L2_FLUSH_CTL, ACINACTM, 0);
    while(HWIO_INF(APCS_ALIAS1_L2_FLUSH_CTL, ACINACTM) != 0);

    HWIO_OUT(APCS_CCI_SNOOPCONTROL4, 3);
    while(0 != HWIO_IN(APCS_CCI_STATUS));
  }
  else /* Power Cluster */
  {
    HWIO_OUTF(APCS_ALIAS0_L2_FLUSH_CTL, ACINACTM, 0);
    while(HWIO_INF(APCS_ALIAS0_L2_FLUSH_CTL, ACINACTM) != 0);

    HWIO_OUT(APCS_CCI_SNOOPCONTROL3, 3);
    while(0 != HWIO_IN(APCS_CCI_STATUS));
  }
}

/**
 * Checks if the chip is shere v1 OR v2
 *
 * @return \c TRUE if chip is shere v1 or v2, FALSE otherwise
 */
boolean mon_chip_is_shere_v1_v2(void)
{
  return (HWIO_INF(TCSR_SOC_HW_VERSION, MAJOR_VERSION) < 0x3);
}

void mon_cci_reset_wa_arch(uint32* cpu_pc_state_ptr)
{
  uint64 i;
  uint32 val = SCL_MON_CODE_BASE;

  /* CCI reset & SYSBARDISABLE=1 not needed starting from rev V3 */
  if(mon_chip_is_shere_v1_v2())
  {
    /* Setting SYSBARDISABLE = 1 to be able to disabled Barrier in CPU.
     * WARNING!!!! - This change require a reset, hence applying this
     * change as part of 8936 CCI Reset Work around.
     * Disabling CCI work around need special attention. */
   
    HWIO_OUTF(APCS_ALIAS1_A53_CFG_STS, SYSBARDISABLE, 0x1);
    if(is_power_cluster_present())
    {
      HWIO_OUTF(APCS_ALIAS0_A53_CFG_STS, SYSBARDISABLE, 0x1);
    }

    /* Reset the CPU PC state for the current CPU so the state value
       stays consistent after doing a second reset. */
    *cpu_pc_state_ptr = 0;

    /* CCI reset WA Impl */

    ////////////////////////////////////////////////////////////////////
    // Configure CCI SPM sequence
    ////////////////////////////////////////////////////////////////////

    HWIO_OUT(CCI_SAW2_SECURE, 0x7);
    HWIO_OUT(CCI_SAW2_CFG, 0x14);
    HWIO_OUTF(CCI_SAW2_SPM_CTL, SPM_SYS_PC_MODE, 0x0);
    HWIO_OUTF(CCI_SAW2_SPM_CTL, SPM_START_ADR, 0x00);
    HWIO_OUTF(CCI_SAW2_SPM_CTL, ISAR, 0x01);
    HWIO_OUTF(CCI_SAW2_SPM_CTL, WAKEUP_CONFIG, 0x03);
    HWIO_OUT(CCI_SAW2_SPM_DLY, 0x3C102800);

    HWIO_OUTI(CCI_SAW2_SPM_SLP_SEQ_ENTRY_n,0,0x60700360);
    HWIO_OUTI(CCI_SAW2_SPM_SLP_SEQ_ENTRY_n,1,0x00000F70);

    //HWIO_OUTI(CCI_SAW2_SPM_SLP_SEQ_ENTRY_n,0,0x0F720370);

    HWIO_OUTF(CCI_SAW2_SPM_CTL, SPM_EN, 0x01);

    ////////////////////////////////////////////////////////////////////
    // Configure CORE SPM sequence
    ////////////////////////////////////////////////////////////////////

    //=================  APCS SPM SEQ  ===========================//
    HWIO_OUT(APCS_BANKED_SAW2_SECURE, 0x00000007); 	// Enable Non Secure Read for RPM
    HWIO_OUT(APCS_BANKED_SAW2_CFG, 0x01);
    HWIO_OUTF(APCS_BANKED_SAW2_SPM_CTL, SPM_SYS_PC_MODE, 0x0);
    HWIO_OUTF(APCS_BANKED_SAW2_SPM_CTL, SPM_START_ADR, 0x0);
    HWIO_OUTF(APCS_BANKED_SAW2_SPM_CTL, ISAR, 0x01);
    HWIO_OUTF(APCS_BANKED_SAW2_SPM_CTL, WAKEUP_CONFIG, 0x03);	//sys_spm_wakeup or rising edge of sys_spm_dbg_nopwrdwn
    HWIO_OUT(APCS_BANKED_SAW2_SPM_DLY, 0x3C102800);

    HWIO_OUTI(APCS_BANKED_SAW2_SPM_SLP_SEQ_ENTRY_n,0,0x03603020);
    HWIO_OUTI(APCS_BANKED_SAW2_SPM_SLP_SEQ_ENTRY_n,1,0x30200B60);
    HWIO_OUTI(APCS_BANKED_SAW2_SPM_SLP_SEQ_ENTRY_n,2,0x0000000F);

    //HWIO_OUTI(APCS_BANKED_SAW2_SPM_SLP_SEQ_ENTRY_n,0,0x60036030);
    //HWIO_OUTI(APCS_BANKED_SAW2_SPM_SLP_SEQ_ENTRY_n,1,0x00000F30);

    HWIO_OUTF(APCS_BANKED_SAW2_SPM_CTL, SPM_EN, 0x01);


    ////////////////////////////////////////////////////////////////////
    // Configure L2 SPM sequence
    ////////////////////////////////////////////////////////////////////
    //=================  L2 SPM SEQ  ===========================//
    HWIO_OUT(APCS_CLUS1_L2_SAW2_SECURE, 0x7);
    HWIO_OUT(APCS_CLUS1_L2_SAW2_CFG, 0x14);
    HWIO_OUTF(APCS_CLUS1_L2_SAW2_SPM_CTL, SPM_SYS_PC_MODE, 0x00);
    HWIO_OUTF(APCS_CLUS1_L2_SAW2_SPM_CTL, SPM_START_ADR, 0x0);
    HWIO_OUTF(APCS_CLUS1_L2_SAW2_SPM_CTL, ISAR, 0x01);
    HWIO_OUTF(APCS_CLUS1_L2_SAW2_SPM_CTL, WAKEUP_CONFIG, 0x03);
    HWIO_OUT(APCS_CLUS1_L2_SAW2_SPM_DLY, 0x3C102800);


    HWIO_OUTI(APCS_CLUS1_L2_SAW2_SPM_SLP_SEQ_ENTRY_n,0,0x501B0330);
    HWIO_OUTI(APCS_CLUS1_L2_SAW2_SPM_SLP_SEQ_ENTRY_n,1,0x000F5030);

    //HWIO_OUTI(APCS_CLUS1_L2_SAW2_SPM_SLP_SEQ_ENTRY_n,0,0x0F520350);

    HWIO_OUTF(APCS_CLUS1_L2_SAW2_SPM_CTL, SPM_EN, 0x01);


    //Configure Boot-remapper to TZ location
    /* monitor CPU entry point. */
    val |= (HWIO_APCS_ALIAS1_BOOT_START_ADDR_SEC_REMAP_EN_BMSK);
    HWIO_OUT(APCS_ALIAS1_BOOT_START_ADDR_SEC, val);

    /* Future chip resets should be in AArch64 */
    HWIO_OUT(APCS_ALIAS1_AA64NAA32_REG, 1);

    ///////////////////////////////////////////////////////////////////////////
    // Configure QTimer for delay for around 100 us to generate the interrupt
    // Configure QTimer-0 Physical timer frame-0 interrupt
    ///////////////////////////////////////////////////////////////////////////

    //QGIC init
    HWIO_OUT(APCS_GICD_CTLR, 0x1);
    HWIO_OUT(APCS_GICC_PMR, 0xF8);
    HWIO_OUT(APCS_GICD_CGCR, 0xFFFFFFFF);

    //disable all interrupts
    for(i=0; i<=HWIO_APCS_GICD_ICENABLERn_MAXn; i++) {
      HWIO_OUTI(APCS_GICD_ICENABLERn, i, 0xFFFFFFFF);
    };

    //Configure and enable timer interrupt in QGIC
    HWIO_OUTI(APCS_GICD_ICFGRn, 18, 0x3<<6);
    HWIO_OUTI(APCS_GICD_ITARGETSRn, 0x48, 0x10000000);
    HWIO_OUTI(APCS_GICD_ISENABLERn, 9, 0x8);

    //Set the timer frequency to 19.2 MHz
    HWIO_OUT(APCS_QTMR1_QTMR_AC_CNTFRQ,0x124F800);

    //Set permissions for CVAL
    HWIO_OUTI(APCS_QTMR1_QTMR_AC_CNTACR_n,0,0x3F);

    //Set matching count to around 100us
    HWIO_OUT(APCS_QTMR1_F2V1_QTMR_V1_CNTP_TVAL,0x800);

    //Enable Timer && Interrupt
    HWIO_OUT(APCS_QTMR1_F2V1_QTMR_V1_CNTP_CTL,0x01);

    //Disable QGIC CPU interface
    HWIO_OUT(APCS_GICC_CTLR, 0x00000000);

    //Set ACINATCM bit for cluster-0 to disable snoop
    HWIO_OUT(APCS_CCI_SNOOPCONTROL4, 0);
    HWIO_OUTF(APCS_ALIAS1_L2_FLUSH_CTL, ACINACTM, 1);
    while(0 != HWIO_IN(APCS_CCI_STATUS));

    //Enter into WFI state to trigger the SPM signals
    asm("WFI");
  }
  else
  {
    /* CCI reset not need starting from 8939 V3.
       Set CCI reset done flag to 0x1 to avoid next time
       CPU rseume at post CCI work arround path */
    cci_wa_done = 0x1;
  }
}

void mon_disable_spm_post_cci_reset(void)
{
  //=================  DISABLE L2, CCI & A53 SPM ======================//
  HWIO_OUTF(CCI_SAW2_SPM_CTL, SPM_EN, 0x0);
  HWIO_OUTF(APCS_BANKED_SAW2_SPM_CTL, SPM_EN, 0x0);
  HWIO_OUTF(APCS_CLUS1_L2_SAW2_SPM_CTL, SPM_EN, 0x0);
}

/* TODO: Move to HLOS */
void mon_pc_cpu_spm_disable_arch(void)
{
  /*check if sdi work around needed or not*/
  if(sdi_wa_flag)
  {
    /* Disable SPM at PC Entry. QCTDD01625131:: Unable to save
     CPU state on WDOG reset in case the bite happened
     during core Power collapse */

    /* Assert if L2 SPM is on */
    if(is_perf_cluster())
    {
      if(1 == HWIO_INF(APCS_CLUS1_L2_SAW2_SPM_CTL, SPM_EN))
      {
        l2_spm_state_dbg[1] = HWIO_IN(APCS_CLUS1_L2_SAW2_SPM_CTL);
        while(1);
      }
    }
    else
    {
      if(1 == HWIO_INF(APCLUS0_L2_SAW2_SPM_CTL, SPM_EN))
      {
        l2_spm_state_dbg[0] = HWIO_IN(APCLUS0_L2_SAW2_SPM_CTL);
        while(1);
      }
    }

    /* Disable CPU SPM */
    HWIO_OUTF(APCS_BANKED_SAW2_SPM_CTL, SPM_EN, 0x0);
    while(HWIO_INF(APCS_BANKED_SAW2_SPM_CTL, SPM_EN) != 0);
 }
}