;============================================================================
;
;                  Monitor CPU Reset Entry Point
;
; Copyright 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved
;============================================================================

;============================================================================
;
;                       EDIT HISTORY FOR MODULE
;
; $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/monitor/msm8936/src/mon_cpu_reset_arch.s#1 $
; $DateTime: 2018/02/07 00:37:16 $
; $Author: mplp4svc $
;
; This section contains comments describing changes made to the module.
; Notice that changes are listed in reverse chronological order.
;
;
; when      who     what, where, why
; --------  ---     ---------------------------------------------------------
; 06/05/14  pre     Initial revision.
;============================================================================
#include "soc.h"
#include "util.h"
#include "mon_hwio.h"
#include "ctx_util.h"

; Use the pre-processor to re-define the below macro based on a
; physical address. Note that the new define is valid until
; the end of the current translation unit

#undef  CORE_TOP_CSR_BASE
#define CORE_TOP_CSR_BASE    CORE_TOP_CSR_BASE_PHYS

#undef  A53SS_BASE
#define A53SS_BASE    A53SS_BASE_PHYS

    IMPORT  cpu_init_asm
    IMPORT  sysdbg_reset_check
    IMPORT  sysdbg_entry_handler
    IMPORT  |Image$$MON_ENTRY_ER$$Base|
    IMPORT  a53_aarch64_sysini
    IMPORT  resume_cold_boot_post_cci_wa
    IMPORT  boot_lock_addr
    IMPORT  mon_disable_spm_post_cci_reset
    IMPORT  l2_gdhs_state
    IMPORT  sdi_wa_flag

    EXPORT  mon_post_cci_reset_wa_arch
    EXPORT  CPU_RESET_ENTRY
    EXPORT  boot_cpu_cold_booted
    EXPORT  cci_wa_done
    EXPORT  boot_cpu_idx
    EXPORT  l2_spm_state_dbg
    ; ------------------------------------------------------------------
    ; 8x36 - cold and warm boot entry point
    ; ------------------------------------------------------------------
    PRESERVE8
    AREA CPU_RESET_ENTRY, CODE, READONLY, ALIGN=3
    ENTRY

    ; ------------------------------------------------------------------
    ; Use TPIDR to back up x0 and x30 and call sysdbg APIs to
    ; determine if this is a reset due to failure.
    ; ------------------------------------------------------------------
    msr     TPIDR_EL0, x0
    msr     TPIDR_EL1, x30
    bl      sysdbg_reset_check      ; check to see if reset due to failure
    cmp     x0, #0
    bne     sysdbg_entry_handler
    mrs     x0, TPIDR_EL0           ; restore x0
    mrs     x30, TPIDR_EL1          ; restore LR

    ; ------------------------------------------------------------------
    ; Until SBL context is saved, we may only use x3-x19 (Boot interface)
    ; ------------------------------------------------------------------
    ; ------------------------------------------------------------------
    ; Need to align the PC with the physical memory address of the
    ; code to work around the remapper.
    ; ------------------------------------------------------------------
    ldr     x3, =cold_boot_remapper_reset
    br      x3

    ;-------------------------------------------------------------------
    ; Memory section above may only be 0x80 bytes
    ;-------------------------------------------------------------------

    PRESERVE8
    AREA CPU_RESET_CONTINUED, CODE, READONLY, ALIGN=3

cold_boot_remapper_reset

    ; ------------------------------------------------------------------
    ; Go to either Boot CPU cold boot or generic CPU reset branch
    ; Since Boot Cpu is different for this target CPU 0 for 8936 & 
    ; CPU 4 for 8939, Shifting to a common variable for 8x36/8x39 target 
    ; only. Other target uses has_cpu_cold_booted with cpu index 0
    ; ------------------------------------------------------------------

    ldr     w3, boot_cpu_cold_booted
    cmp     w3, #0
    bne     cpu_reset_branch
    mov     w4, #1                  ; set boot_cpu_cold_booted = TRUE 
                                    ; to restrict to only one time cold boot 
    ldr     x3, =boot_cpu_cold_booted
    str     w4, [x3]

    ; ------------------------------------------------------------------
    ; 8936/8939 - Primary / Boot CPU cold boot contd.
    ; ------------------------------------------------------------------
boot_cpu_cold_boot

    ; ------------------------------------------------------------------
    ; Do sysini.  Save x0, x1, x2 and restore when done.  This is
    ; needed to preserve boot arguments for cold boot
    ; ------------------------------------------------------------------
    mov     x29, x0
    mov     x28, x1
    mov     x27, x2
    ldr     x3, =HWIO_TCSR_SOC_HW_VERSION_ADDR
    ldr     w0, [x3]
    bl      a53_aarch64_sysini
    mov     x0, x29
    mov     x1, x28
    mov     x2, x27

    ; ------------------------------------------------------------------
    ; Populate primary/boot CPU idx here. Will use this to identify 8936/8939.
    ; ------------------------------------------------------------------
    CurCPUNum  x3, x4               ; Return current CPU No at x3
    ldr     x4, =boot_cpu_idx
    str     w3, [x4]                ; Populate boot cpu idx with current CPU No.

    ldr     x3, =|Image$$MON_ENTRY_ER$$Base|
    br      x3
    ; END
    ; ------------------------------------------------------------------
    ; continues in monitor.s (common). Jump to cold_boot_asm 
    ; ------------------------------------------------------------------


    ; ------------------------------------------------------------------
    ; 8936/8939 - Secondary CPU cold boot and warm boot
    ; ------------------------------------------------------------------
cpu_reset_branch
    ;-------------------------------------------------------------------
    ; Check if its booting just after CCI reset work around, then boot
    ; CPU reset due to CCI reset work around and QSEE cold boot is
    ; still pending. Do NOT aquire boot lock before QSEE cold boot.
    ; Lock address will be populated at QSEE Cold boot.
    ;-------------------------------------------------------------------
    ldr   w0, cci_wa_done
    cmp   w0, #0
    beq   cpu_reset_branch_without_lock

    ;-------------------------------------------------------------------
    ; Boot lock must be taken immediately and held until EL1 is done
    ; with CPU init stuff
    ;-------------------------------------------------------------------
    ldr     w0, boot_lock_addr
    SpinlockObtain x0, x1, w30
    
    ldr     w0, sdi_wa_flag
    cbz     w0, cpu_reset_branch_without_lock
    ;-------------------------------------------------------------------
    ; Disable CPU & L2 SPM.
    ; QCTDD01625131:: Unable to save CPU state on WDOG reset in case
    ; the bite happened during core Power collapse.
    ; Keeping SPM_EN to preserve CPU & L2 state post secure bite reset
    ;-------------------------------------------------------------------
    ; Disable L2 SPM.
    CurCPUNum x0, x30               ; Get the current CPU No.
    ldr     w1, boot_cpu_idx        ; Get boot cpu idx. CPU Idx = 4/0 for 8939/8936
    cmp     x0, x1                  ; Compare with boot CPU No.
    bge     perf_cluster_l2_spm     ; If its equal or greater, its a perf cluster,
    
    ; Disable Power Cluster L2 SPM.
power_cluster_l2_spm
    ldr     x0, =HWIO_APCLUS0_L2_SAW2_SPM_CTL_ADDR
    ldr     w30, [x0]
    ; Only 1st CPU waking up in the cluster can see L2 SPM enable. 
    ; Disable only if it was enable.
    tst     w30, #HWIO_APCLUS0_L2_SAW2_SPM_CTL_SPM_EN_BMSK
    beq     cpu_spm                 ; SPM_EN bit is already set to 0x0.
    bfi     w30, wzr, #HWIO_APCLUS0_L2_SAW2_SPM_CTL_SPM_EN_SHFT, #1 ; Clear SPM_EN bit / Bit 0
    str     w30, [x0]

2   ldr   w30, [x0]
    tst   w30, #HWIO_APCLUS0_L2_SAW2_SPM_CTL_SPM_EN_BMSK
    bne   %b2                       ; Wait for write success for spm disable.

   ; Pass info to EL1 about L2 SPM_EN was enable to double check.
    ldr     w0, boot_lock_addr
    ldr     w30, [x0]
    orr     w30, #0xF000
    str     w30, [x0]

    b       cpu_spm

    ; Disable Perf Cluster L2 SPM.
perf_cluster_l2_spm
    ldr     x0, =HWIO_APCS_CLUS1_L2_SAW2_SPM_CTL_ADDR
    ldr     w30, [x0]
    ; Only 1st CPU waking up in the cluster can see L2 SPM enable. 
    ; Disable only if it was enable.
    tst     w30, #HWIO_APCS_CLUS1_L2_SAW2_SPM_CTL_SPM_EN_BMSK
    beq     cpu_spm                 ; SPM_EN bit is already set to 0x0.
    bfi     w30, wzr, #HWIO_APCS_CLUS1_L2_SAW2_SPM_CTL_SPM_EN_SHFT, #1 ; Clear SPM_EN bit / Bit 0
    str     w30, [x0]

3   ldr   w30, [x0]
    tst   w30, #HWIO_APCS_CLUS1_L2_SAW2_SPM_CTL_SPM_EN_BMSK
    bne   %b3                       ; Wait for write success for spm disable.

   ; Pass info to EL1 about L2 SPM_EN was enable to double check.
    ldr     w0, boot_lock_addr
    ldr     w30, [x0]
    orr     w30, #0xF000
    str     w30, [x0]

    ; Disable APCS CPU SPM
cpu_spm
    ldr     x0, =HWIO_APCS_BANKED_SAW2_SPM_CTL_ADDR
    ldr     w30, [x0]
    bfi     w30, wzr, #HWIO_APCS_BANKED_SAW2_SPM_CTL_SPM_EN_SHFT, #1 ; Clear SPM_EN bit / Bit 0
    str     w30, [x0]

4   ldr   w30, [x0]
    tst   w30, #HWIO_APCS_BANKED_SAW2_SPM_CTL_SPM_EN_BMSK
    bne   %b4                       ; Wait for write success for spm disable.

cpu_reset_branch_without_lock
    ;-------------------------------------------------------------------
    ; APCS_ALIAS[0/1]_L2_FLUSH_CTRL[ACINACTM] must be set to zero
    ; before any DVM transactions are sent to the bus, which means it
    ; must come first or else a DSB will cause a system hang.
    ;-------------------------------------------------------------------
    CurCPUNum x0, x30               ; Get the current CPU No.
    ldr     w1, boot_cpu_idx        ; Get boot cpu idx. CPU Idx = 4 for 8939
    cmp     x0, x1                  ; Compare with boot CPU No.
    bge     cluster1            ; If its equal or greater, its a perf cluster 

    ;-----------------------------------------------------------------------
    ;check L2 state for the other cluster and bring it online first if its
    ;in GDHS state and enable current clusters L2 
    ;-----------------------------------------------------------------------
cluster0
    mov x0, #0
    ClearPerClusterGdhs w0, x1, w1           ;clear GDHS flag for the current cluster
    CurClusterNum x0
    eor     w0, w0, #0x1                     ;check GDHS flag for the other cluster
    PerClusterGdhsState  w0, x1, w1
    cmp     w0, #1
    bne     %f0
    bl      cluster1_acinactm_snoop_enable   ;if GDHS flag is set bring its L2 online
    
0   bl      cluster0_acinactm_snoop_enable   ;L2 should be online for current cluster
    b       cluster_coherency_enable_done

cluster1
    mov x0, #1
    ClearPerClusterGdhs w0, x1, w1           ;clear GDHS flag for the current cluster
    CurClusterNum x0
    eor     w0, w0, #0x1                     ;check GDHS flag for the other cluster
    PerClusterGdhsState  w0, x1, w1
    cmp     w0, #1
    bne     %f1
    bl      cluster0_acinactm_snoop_enable   ;if GDHS flag is set bring its L2 online
    
1   bl      cluster1_acinactm_snoop_enable   ;L2 should be online for current cluster
    b       cluster_coherency_enable_done

    ;-------------------------------------------------------------------
    ; Enable power cluster coherency. 
    ; Power cluster ACINACTM = 0 & CCI_SNOOPCONTROL3 = 3 configured together.
    ; Wait for L2 to come online & do not corrupt x30
    ;-------------------------------------------------------------------
cluster0_acinactm_snoop_enable
    ; Read APCS_ALIAS0_L2_FLUSH_CTRL
    ldr     x0, =HWIO_APCS_ALIAS0_L2_FLUSH_CTL_ADDR
    ldr     w3, [x0]               ; Skip if ACINACTM field is zero.
    tst     w3, #HWIO_APCS_ALIAS0_L2_FLUSH_CTL_ACINACTM_BMSK
    beq     skip_cluster0_acinactm
    ; Clear ACINACTM field
    bfi     w3, wzr, #HWIO_APCS_ALIAS0_L2_FLUSH_CTL_ACINACTM_SHFT, #1
    str     w3, [x0]               ; Write APCS_ALIAS0_L2_FLUSH_CTRL
5   ldr     w3, [x0]
    tst     w3, #HWIO_APCS_ALIAS0_L2_FLUSH_CTL_ACINACTM_BMSK
    bne     %b5                     ; busy wait until ACINACTM is zero
    ;wait for L2 to come online
    ldr     x0, =HWIO_APCS_ALIAS0_L2_PWR_STATUS_ADDR
6   ldr     w3, [x0]
    tst     w3, #HWIO_APCS_ALIAS0_L2_PWR_STATUS_L2_HS_STS_BMSK
    beq     %b6

skip_cluster0_acinactm
    ; Snoop control
    ldr     x0, =HWIO_APCS_CCI_SNOOPCONTROL3_ADDR
    mov     x1, #0x3
    str     w1, [x0]
    ldr     x0, =HWIO_APCS_CCI_STATUS_ADDR
7   ldr     w1, [x0]
    tst     w1, #0x1
    bne     %b7                     ; busy wait until CCI_STATUS is zero
    br      x30                      ;return to function from where it was called

    ;-------------------------------------------------------------------
    ; Enable perf cluster coherency. 
    ; Perf cluster ACINACTM = 0 & CCI_SNOOPCONTROL4 = 3 configured together.
    ; Wait for L2 to come online & do not corrupt x30.
    ;-------------------------------------------------------------------
cluster1_acinactm_snoop_enable
    ; Read APCS_ALIAS1_L2_FLUSH_CTRL
    ldr     x0, =HWIO_APCS_ALIAS1_L2_FLUSH_CTL_ADDR
    ldr     w3, [x0]               ; Skip if ACINACTM field is zero. 
    tst     w3, #HWIO_APCS_ALIAS1_L2_FLUSH_CTL_ACINACTM_BMSK
    beq     skip_cluster1_acinactm
    ; Clear ACINACTM field
    bfi     w3, wzr, #HWIO_APCS_ALIAS1_L2_FLUSH_CTL_ACINACTM_SHFT, #1
    str     w3, [x0]               ; Write APCS_ALIAS1_L2_FLUSH_CTRL
8   ldr     w3, [x0]
    tst     w3, #HWIO_APCS_ALIAS1_L2_FLUSH_CTL_ACINACTM_BMSK
    bne     %b8                     ; busy wait until ACINACTM is zero
    ;Wait for L2 to come online
    ldr     x0, =HWIO_APCS_ALIAS1_L2_PWR_STATUS_ADDR
9   ldr     w3, [x0]
    tst     w3, #HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_HS_STS_BMSK
    beq     %b9

skip_cluster1_acinactm
    ; Snoop control
    ldr     x0, =HWIO_APCS_CCI_SNOOPCONTROL4_ADDR
    mov     x1, #0x3
    str     w1, [x0]
    ldr     x0, =HWIO_APCS_CCI_STATUS_ADDR
10   ldr     w1, [x0]
    tst     w1, #0x1
    bne     %b10
    br      x30                       ;return to function from where it was called

    ; cluster coherency configuration complete.
cluster_coherency_enable_done

    ; sysini
a53_sysini
    ldr     x0, =HWIO_TCSR_SOC_HW_VERSION_ADDR
    ldr     w0, [x0]
    bl      a53_aarch64_sysini

    ; ------------------------------------------------------------------
    ; Continue normal CPU init stuff
    ; ------------------------------------------------------------------
    b       cpu_init_asm
    ; END
    ; ------------------------------------------------------------------
    ; continues in mon_cpu_reset.s (common)
    ; ------------------------------------------------------------------


    ; ------------------------------------------------------------------
    ; 8936/8939 - Post CCI reset work around boot up. Consider 1st warm
    ; boot is due to CCI reset work around. Execution need to resume for
    ; QSEE cold boot path, since CCI/CPU reseted at the end of monitor
    ; mode cold boot.
    ; Warning!!.. Do NOT use stack here. SP will be manipulated in this API.
    ; ------------------------------------------------------------------
mon_post_cci_reset_wa_arch
    ; ------------------------------------------------------------------
    ; Mark CCI Reset Completed.
    ; ------------------------------------------------------------------

    ldr   w0, cci_wa_done
    cmp   w0, #0
    bne   mon_post_cci_reset_wa_arch_end
    mov   w0, #1
    ldr   x2, =cci_wa_done
    str   w0, [x2]

    ; ------------------------------------------------------------------
    ; Disable SPM, CCI Reset Completed.
    ; ------------------------------------------------------------------
    bl    mon_disable_spm_post_cci_reset

    ; ------------------------------------------------------------------
    ; Reserve Stack for Non Sec Context before jumping to QSEE Cold Boot.
    ; This is done here since post CCI Reset we resume cold boot path
    ; @ resume_cold_boot_post_cci_wa. Stack reservation is done before
    ; this label during normal cold boot.path.
    ; SaveCtx will allow saving EL3 context when jump to EL1
    ; ------------------------------------------------------------------
    sub     sp, #CTX_SZ
    SaveCtx sp
    sub     sp, #CTX_SZ

    b       resume_cold_boot_post_cci_wa
    ; ------------------------------------------------------------------
    ; continues in monitor.s (common)
    ; ------------------------------------------------------------------

mon_post_cci_reset_wa_arch_end
    ret

    ; END
    ; ------------------------------------------------------------------
    ; continues in mon_cpu_reset.s (common). 
    ; Its a normal warm boot / secondary CPU cold boot.
    ; ------------------------------------------------------------------

    ; ------------------------------------------------------------------
    ; Variable Declaration.
    ; ------------------------------------------------------------------
    AREA      MONITOR_UNCACHED, DATA, READWRITE
boot_cpu_cold_booted         SPACE  4  ; Since 8936/8939 Boot CPU is
                                       ; different,defined a common variable
                                       ; to identify TZ cold booted or Not

cci_wa_done                  SPACE  4  ; Hold the CCI Reset Status.
                                       ; CCI reset done after L2 TCM Mode disable

boot_cpu_idx                 SPACE  4  ; Hold the boot CPU Idx
                                       ; ( Boot CPU is 0/4 for 8936/8939)

l2_spm_state_dbg             SPACE  8  ; Hold L2 SPM State at PC Entry

    END
