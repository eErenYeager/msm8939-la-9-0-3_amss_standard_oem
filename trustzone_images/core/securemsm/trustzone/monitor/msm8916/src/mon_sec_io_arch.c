#include "mon_sec_io.h"
#include "todo.h"
#include "mon_hwio.h"

/* Remove any sandboxing offset added in HWIO based macro */
/*----------------------------------------------------------------------------
 * BASE: CORE_TOP_CSR
 *--------------------------------------------------------------------------*/
#undef  CORE_TOP_CSR_BASE
#define CORE_TOP_CSR_BASE                                CORE_TOP_CSR_BASE_PHYS

int secure_io_write(p_addr_t addr, unsigned int value)
{
  if (HWIO_TCSR_BOOT_MISC_DETECT_ADDR == addr)
  {
    /* These are 32 bit registers */
    *(volatile uint32*) addr = (uint32) value;
    return E_SUCCESS;
  }

  return -E_OUT_OF_RANGE;
}

unsigned int secure_io_read(p_addr_t addr)
{
  if (HWIO_TCSR_BOOT_MISC_DETECT_ADDR == addr)
  {
    /* These are 32 bit registers */
    return *(volatile uint32*) addr;
  }

  return (unsigned int) -E_OUT_OF_RANGE;
}
