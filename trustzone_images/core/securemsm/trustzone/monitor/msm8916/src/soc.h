#ifndef SOC_H
#define SOC_H

#define CPU_COUNT      4
#define CLUSTER_COUNT  1

#ifndef _ARM_ASM_

#include "sys_regs.h"

__inline static el3_reg_t cur_cpu_num(void)
{
  return (get_mpidr_el1() & 0xFF);
}

__inline static boolean get_cluster_num(void)
{
  return 0;
}

__inline static uint32 get_num_cpus_in_cluster(uint32 cluster_num)
{
  return CPU_COUNT;
}

void soc_pc_entry(void);
void soc_pc_fallthrough(void);

#else
/* ----------------------------------------------------------------------
   MACRO: CurCPUNum
   ---------------------------------------------------------------------- */
/*   This uses two registers to keep the macro definition constant
     across targets */
    MACRO
    CurCPUNum $rx, $ry
        mrs     $rx, MPIDR_EL1
        and     $rx, #MPIDR_EL1_AFF0_MSK
    MEND
#endif /* _ARM_ASM_ */

#endif /* SOC_H */
