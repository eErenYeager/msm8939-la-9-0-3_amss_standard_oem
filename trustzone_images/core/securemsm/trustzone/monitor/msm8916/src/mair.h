#ifndef MAIR_H
#define MAIR_H

/*===========================================================================

                    T r u s t z o n e   T a r g e t
                          H e a d e r  F i l e

DESCRIPTION
  Describe MMU-specific register setting that outlines memory types

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None

Copyright (c) 2011-2012 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/monitor/msm8916/src/mair.h#1 $
$DateTime: 2018/02/07 00:37:16 $
$Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/01/13   pre     Initial Revision
============================================================================*/

#include "tt_ld.h"

/*****************************************************************************
 *                             !!! CAUTION !!!                               *
 * Indices must match tt_tz.h and tt_ld.h or else memory type abstraction    *
 * will not work.                                                            *
 *****************************************************************************
 * TCR value must match memory attributes for translation table memory or    *
 * else really weird hard to debug crashes will happen.                      *
 *****************************************************************************/
#define MAIR_INDEX_0  MAIR_STRONGLY_ORDERED
#define MAIR_INDEX_1  MAIR_DEVICE
#define MAIR_INDEX_2  MAIR_NORMAL
#define MAIR_INDEX_3 (MAIR_OUTER_WT_RWA_NTR    | MAIR_INNER_WT_RWA_NTR)
#define MAIR_INDEX_4 (MAIR_OUTER_WT_NWA_RA_NTR | MAIR_INNER_WT_NWA_RA_NTR)
#define MAIR_INDEX_5 (MAIR_OUTER_WB_RWA_NTR    | MAIR_INNER_WB_RWA_NTR)
#define MAIR_INDEX_6 (MAIR_OUTER_WB_NWA_RA_NTR | MAIR_INNER_WB_NWA_RA_NTR)
#define MAIR_INDEX_7  MAIR_STRONGLY_ORDERED


#define MAIR  ((MAIR_INDEX_0)       | (MAIR_INDEX_1 << 8)  |  \
               (MAIR_INDEX_2 << 16) | (MAIR_INDEX_3 << 24) |  \
               (MAIR_INDEX_4 << 32) | (MAIR_INDEX_5 << 40) |  \
               (MAIR_INDEX_6 << 48) | (MAIR_INDEX_7 << 56))

#endif
