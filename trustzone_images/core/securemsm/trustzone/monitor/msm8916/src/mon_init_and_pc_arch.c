/**
   @file mon_pc.c
   @brief EL3 soc-specific power collapse
*/
/*===========================================================================
   Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/monitor/msm8916/src/mon_init_and_pc_arch.c#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $


when       who      what, where, why
--------   ---     ------------------------------------------------------------
=============================================================================*/

#include <comdef.h>
#include "soc.h"
#include "sys_regs.h"
#include "a53_pc.h"
#include "memory_defs.h"
#include "mapper.h"
#include "tt_tz.h"
#include "default_memory_map_arch.h"
#include "mon_init_and_pc_arch.h"
#include "todo.h"
#include <HALhwio.h>
#include "mon_hwio.h"
#include <a53_sys_regs.h>

#define UNCACHED __attribute__((section("MONITOR_UNCACHED")))

/* Remove any sandboxing offset added in HWIO based macro */
/*----------------------------------------------------------------------------
 * BASE: CORE_TOP_CSR
 *--------------------------------------------------------------------------*/
#undef  CORE_TOP_CSR_BASE
#define CORE_TOP_CSR_BASE                                CORE_TOP_CSR_BASE_PHYS

/* Global so it is shared with arch-specific assembly file:
   mon_cpu_reset_arch.s */
uint32 UNCACHED boot_lock_addr;

extern void a53_aarch64_sysini(uint32 tcsr_hw_version);

void sysini_arch()
{
  /* TODO: request this in sysini */
  /* TODO: Make sure EL2 does not give access to NS EL1 or switch
     this register for every context switch. */
  a53_grant_el1_cpuectlr_access();
}

void mon_disable_l2_tcm_arch()
{
  HWIO_OUTF(APCS_TCM_START_ADDR,TCM_REDIRECT_EN_0,0);
  __asm__ ("dmb sy" : : :);
  HWIO_OUT(TCSR_MEM_ACC_SEL_APC, 0x0100);
  __asm__ ("dmb sy" : : :);
}

/* STUB function as cci reset work around not applicable for 8916,
   this function calls from cold_boot_asm for 8936, which is not 
   needed for 8916. */
void mon_post_cci_reset_wa_arch(void)
{
}

void mon_pc_cpu_spm_disable_arch(void)
{
  /* Disable CPU & L2 SPM at PC Entry.
   * Not implemented since QCTDD01625131 not applicable */
}

