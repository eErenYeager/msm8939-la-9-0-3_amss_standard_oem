# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/monitor/msm8936/src/mon_cpu_reset_arch.s"
# 1 "<built-in>" 1
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/monitor/msm8936/src/mon_cpu_reset_arch.s" 2
;============================================================================
;
; Monitor CPU Reset Entry Point
;
; Copyright 2014 by Qualcomm Technologies, Incorporated. All Rights Reserved
;============================================================================

;============================================================================
;
; EDIT HISTORY FOR MODULE
;
; $Header:
; $DateTime: 2018/02/07 00:37:16 $
; $Author: mplp4svc $
;
; This section contains comments describing changes made to the module.
; Notice that changes are listed in reverse chronological order.
;
;
; when who what, where, why
; -------- --- ---------------------------------------------------------
; 06/05/14 pre Initial revision.
;============================================================================

# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/monitor/msm8936/src/soc.h" 1
# 45 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/monitor/msm8936/src/soc.h"
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/armlib/v8/inc/sys_fields.h" 1
# 46 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/monitor/msm8936/src/soc.h" 2




   MACRO
   CurCPUNum $rx, $ry
      mrs $rx, MPIDR_EL1
      and $ry, $rx, #0x0000FF00
      lsr $ry, #0x8
      mov $rx, #4
      mul $ry, $rx
      mrs $rx, MPIDR_EL1
      and $rx, #0x000000FF
      add $rx, $ry
   MEND




   MACRO
   PerClusterGdhsState $rx, $ry, $rz
     ldr $rz, =l2_gdhs_state;
     add $rz, $rz, $rx, lsl #0x2
     ldr $rx, [$ry]
     MEND





   MACRO
   ClearPerClusterGdhs $rx, $ry, $rz
     ldr $rz, =l2_gdhs_state;
     add $rz, $rz, $rx, lsl #0x2
     mov $rx, #0x0
     str $rx, [$ry]
     MEND
# 25 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/monitor/msm8936/src/mon_cpu_reset_arch.s" 2
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/armlib/v8/inc/util.h" 1
# 42 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/armlib/v8/inc/util.h"
    MACRO
    SetSecure $rx





        mov $rx, xzr
        msr SCR_EL3, $rx
        isb
    MEND





    MACRO
    SetNonSecure $rx





        mov $rx, #0x020:OR:0x001:OR:0x004:OR:0x400:OR:0x100
        msr SCR_EL3, $rx
        isb
    MEND





    MACRO
    PushTwo $x0, $x1
        stp $x1, $x0, [sp, #-0x10]!
    MEND




    MACRO
    PopTwo $x0, $x1
        ldp $x0, $x1, [sp], #0x10
    MEND




    MACRO
    PushAArch32NPRegs
        PushTwo x5, x6
        PushTwo x7, x8
        PushTwo x9, x10
        PushTwo x11, x12
        PushTwo x13, x14
        PushTwo x15, x16
        PushTwo x17, x18
        PushTwo x19, x20
        PushTwo x21, x22
        PushTwo x23, x24
        PushTwo x25, x26
        PushTwo x27, x28
        PushTwo x29, x30
    MEND




    MACRO
    PopAArch32NPRegs
        PopTwo x30, x29
        PopTwo x28, x27
        PopTwo x26, x25
        PopTwo x24, x23
        PopTwo x22, x21
        PopTwo x20, x19
        PopTwo x18, x17
        PopTwo x16, x15
        PopTwo x14, x13
        PopTwo x12, x11
        PopTwo x10, x9
        PopTwo x8, x7
        PopTwo x6, x5
    MEND





    MACRO
    PushAArch64CSRegs
        PushTwo x19, x20
        PushTwo x21, x22
        PushTwo x23, x24
        PushTwo x25, x26
        PushTwo x27, x28
        PushTwo x29, x30
    MEND





    MACRO
    PopAArch64CSRegs
        PopTwo x30, x29
        PopTwo x28, x27
        PopTwo x26, x25
        PopTwo x24, x23
        PopTwo x22, x21
        PopTwo x20, x19
    MEND







    MACRO
    PushAArch64PSRegs
        PushTwo x19, x18
        PushTwo x17, x16
        PushTwo x15, x14
        PushTwo x13, x12
        PushTwo x11, x10
        PushTwo x9, x8
        PushTwo x7, x6
        PushTwo x5, x4
        PushTwo x3, x2
        PushTwo x1, x0
    MEND




    MACRO
    PopAArch64PSRegs
        PopTwo x0, x1
        PopTwo x2, x3
        PopTwo x4, x5
        PopTwo x6, x7
        PopTwo x8, x9
        PopTwo x10, x11
        PopTwo x12, x13
        PopTwo x14, x15
        PopTwo x16, x17
        PopTwo x18, x19
    MEND





    MACRO
    WipeParamRegs
        mov x0, #0
        mov x1, #0
        mov x2, #0
        mov x3, #0
        mov x4, #0
        mov x5, #0
        mov x6, #0
        mov x7, #0
        mov x8, #0
    MEND





    MACRO
    WipeScratchRegs
        mov x9, #0
        mov x10, #0
        mov x11, #0
        mov x12, #0
        mov x13, #0
        mov x14, #0
        mov x15, #0
        mov x16, #0
        mov x17, #0
        mov x18, #0
    MEND





    MACRO
    WipeAllGPRegs
        WipeParamRegs
        WipeScratchRegs
        mov x19, #0
        mov x20, #0
        mov x21, #0
        mov x22, #0
        mov x23, #0
        mov x24, #0
        mov x25, #0
        mov x26, #0
        mov x27, #0
        mov x28, #0
        mov x29, #0
        mov x30, #0
    MEND





    MACRO
    CorruptMonRegs
        mov x0, #0xA
        mov x1, #0xB
        mov x2, #0xC
        mov x3, #0xD
        mov x4, #0xE
        mov x5, #0xF
        mov x6, #0xA
        mov x7, #0xB
        mov x8, #0xC
        mov x9, #0xD
        mov x10, #0xE
        mov x11, #0xF
        mov x12, #0xA
        mov x13, #0xB
        mov x14, #0xC
        mov x15, #0xD
        mov x16, #0xE
        mov x17, #0xF
        mov x18, #0xA
        mov x19, #0xB
        mov x20, #0xC
        mov x21, #0xD
        mov x22, #0xE
        mov x23, #0xF
        mov x24, #0xA
        mov x25, #0xB
        mov x26, #0xC
        mov x27, #0xD
        mov x28, #0xE
        mov x29, #0xF
        mov x30, #0xA
    MEND






  MACRO
  SpinlockObtain $lock_addr, $cpu, $work
    mrs $cpu, MPIDR_EL1
    and $cpu, $cpu, #0xFF
    add $cpu, #0x1
42 ldaxr $work, [$lock_addr]
    cbnz $work, %b42
    stlxr $work, $cpu, [$lock_addr]
    cbnz $work, %b42
    dmb sy
  MEND





  MACRO
  SpinlockRelease $lock_addr
    ; Ensure all reads/writes are visible to other observers before
    ; releasing the lock.
    dmb sy
    str xzr, [$lock_addr] ; Clear the lock
    dmb sy
  MEND




    MACRO
    CurCPUNumber $rx, $ry, $cpus_per_cluster
       mrs $rx, MPIDR_EL1
       and $ry, $rx, #0x0000FF00
       lsr $ry, #0x8
       mov $rx, #$cpus_per_cluster
       mul $ry, $rx
       mrs $rx, MPIDR_EL1
       and $rx, #0x000000FF
       add $rx, $ry
    MEND





    MACRO
    CurClusterNum $rx
       mrs $rx, MPIDR_EL1
       and $rx, #0x0000FF00
       lsr $rx, #0x8
    MEND
# 26 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/monitor/msm8936/src/mon_cpu_reset_arch.s" 2
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/monitor/msm8936/src/mon_hwio.h" 1
# 36 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/monitor/msm8936/src/mon_hwio.h"
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/api/systemdrivers/hwio/msm8936/phys/msmhwiobase.h" 1
# 37 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/monitor/msm8936/src/mon_hwio.h" 2
# 27 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/monitor/msm8936/src/mon_cpu_reset_arch.s" 2
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/monitor/common/inc/ctx_util.h" 1
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/monitor/common/inc/aa64_ctx.h" 1
# 2 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/monitor/common/inc/ctx_util.h" 2

; -------------------
; MACRO: PushTwoA
; -------------------
; Like push, but memory addresses ascend instead of descend
    MACRO
    PushTwoA $x0, $x1, $x2
        stp $x1, $x2, [$x0], #0x10
    MEND

; -------------------
; MACRO: PopTwoA
; -------------------
    MACRO
    PopTwoA $x0, $x1, $x2
        ldp $x2, $x1, [$x0, #-0x10]!
    MEND
;
; TODO: CPACR_EL1 - for access to floating point and Advanced SIMD
; execution
;
; ----------------
; MACRO: SaveCtx
; ----------------
; $sp should be a pointer to the base address of a context structure.
; See aa_64_ctx_t in aa64_ctx.h
    MACRO
    SaveCtx $sp
        PushTwoA $sp, x0, x1
        PushTwoA $sp, x2, x3
        PushTwoA $sp, x4, x5
        PushTwoA $sp, x6, x7
        PushTwoA $sp, x8, x9
        PushTwoA $sp, x10, x11
        PushTwoA $sp, x12, x13
        PushTwoA $sp, x14, x15
        PushTwoA $sp, x16, x17
        PushTwoA $sp, x18, x19
        PushTwoA $sp, x20, x21
        PushTwoA $sp, x22, x23
        PushTwoA $sp, x24, x25
        PushTwoA $sp, x26, x27
        PushTwoA $sp, x28, x29

        mrs x0, SPSR_EL1
        PushTwoA $sp, x30, x0

        mrs x0, ELR_EL1
        mrs x1, SPSR_irq
        PushTwoA $sp, x0, x1

        mrs x0, SPSR_abt
        mrs x1, SPSR_und
        PushTwoA $sp, x0, x1

        mrs x0, SPSR_fiq
        mrs x1, SP_EL1
        PushTwoA $sp, x0, x1

        mrs x0, SP_EL0
        mrs x1, CNTP_TVAL_EL0
        PushTwoA $sp, x0, x1

        mrs x0, CNTP_CVAL_EL0
        mrs x1, CNTP_CTL_EL0
        PushTwoA $sp, x0, x1

        mrs x0, VBAR_EL1
        mrs x1, TTBR1_EL1
        PushTwoA $sp, x0, x1

        mrs x0, TTBR0_EL1
        mrs x1, TCR_EL1
        PushTwoA $sp, x0, x1

        mrs x0, TPIDR_EL0
        mrs x1, TPIDRRO_EL0
        PushTwoA $sp, x0, x1

        mrs x0, TPIDR_EL1
        mrs x1, SCTLR_EL1
        PushTwoA $sp, x0, x1

        mrs x0, PAR_EL1
        mrs x1, MAIR_EL1
        PushTwoA $sp, x0, x1

        mrs x0, ESR_EL1
        mrs x1, FAR_EL1
        PushTwoA $sp, x0, x1

        mrs x0, CSSELR_EL1
        mrs x1, CONTEXTIDR_EL1
        PushTwoA $sp, x0, x1

        mrs x0, AMAIR_EL1
        mrs x1, AFSR1_EL1
        PushTwoA $sp, x0, x1

        mrs x0, AFSR0_EL1
        mrs x1, ACTLR_EL1
        PushTwoA $sp, x0, x1

        mrs x0, CNTKCTL_EL1
        mrs x1, FPEXC32_EL2
        PushTwoA $sp, x0, x1

        mrs x0, DACR32_EL2
        mrs x1, IFSR32_EL2
        PushTwoA $sp, x0, x1

        mrs x0, CPACR_El1
        ldr x1, =0xAABBAABB
        PushTwoA $sp, x0, x1

        mrs x0, SPSR_EL3
        mrs x1, ELR_EL3
        PushTwoA $sp, x0, x1
    MEND

; ----------------
; MACRO: LoadCtx
; ----------------
; $sp should be a pointer to the base address of a context structure.
; See aa_64_ctx_t in aa64_ctx.h. The order of removing items from the
; structure is important to properly restore x0, x1.
    MACRO
    LoadCtx $sp
        add $sp, #((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8)

        PopTwoA $sp, x0, x1
        msr ELR_EL3, x0
        msr SPSR_EL3, x1

        PopTwoA $sp, x0, x1
        ; dummy val in x0
        msr CPACR_El1, x1

        PopTwoA $sp, x0, x1
        msr IFSR32_EL2, x0
        msr DACR32_EL2, x1

        PopTwoA $sp, x0, x1
        msr FPEXC32_EL2, x0
        msr CNTKCTL_EL1, x1

        PopTwoA $sp, x0, x1
        msr ACTLR_EL1, x0
        msr AFSR0_EL1, x1

        PopTwoA $sp, x0, x1
        msr AFSR1_EL1, x0
        msr AMAIR_EL1, x1

        PopTwoA $sp, x0, x1
        msr CONTEXTIDR_EL1, x0
        msr CSSELR_EL1, x1

        PopTwoA $sp, x0, x1
        msr FAR_EL1, x0
        msr ESR_EL1, x1

        PopTwoA $sp, x0, x1
        msr MAIR_EL1, x0
        msr PAR_EL1, x1

        PopTwoA $sp, x0, x1
        msr SCTLR_EL1, x0
        msr TPIDR_EL1, x1

        PopTwoA $sp, x0, x1
        msr TPIDRRO_EL0, x0
        msr TPIDR_EL0, x1

        PopTwoA $sp, x0, x1
        msr TCR_EL1, x0
        msr TTBR0_EL1, x1

        PopTwoA $sp, x0, x1
        msr TTBR1_EL1, x0
        msr VBAR_EL1, x1

        PopTwoA $sp, x0, x1
        msr CNTP_CTL_EL0, x0
        msr CNTP_CVAL_EL0, x1

        PopTwoA $sp, x0, x1
        msr CNTP_TVAL_EL0, x0
        msr SP_EL0, x1

        PopTwoA $sp, x0, x1
        msr SP_EL1, x0
        msr SPSR_fiq, x1

        PopTwoA $sp, x0, x1
        msr SPSR_und, x0
        msr SPSR_abt, x1

        PopTwoA $sp, x0, x1
        msr SPSR_irq, x0
        msr ELR_EL1, x1

        PopTwoA $sp, x1, x30
        msr SPSR_EL1, x1

        PopTwoA $sp, x29, x28
        PopTwoA $sp, x27, x26
        PopTwoA $sp, x25, x24
        PopTwoA $sp, x23, x22
        PopTwoA $sp, x21, x20
        PopTwoA $sp, x19, x18
        PopTwoA $sp, x17, x16
        PopTwoA $sp, x15, x14
        PopTwoA $sp, x13, x12
        PopTwoA $sp, x11, x10
        PopTwoA $sp, x9, x8
        PopTwoA $sp, x7, x6
        PopTwoA $sp, x5, x4
        PopTwoA $sp, x3, x2
        PopTwoA $sp, x1, x0

        add $sp, #((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8)
    MEND

; ---------------------
; MACRO: LoadSysRegsCtx
; ---------------------
; Loads registers from the context in a given register
; Note: we do not need to go in a specific order, since there is no
; concern for corrupting GP registers, but it's good to maintain the
; same order as savectx and loadctx so the operations can be compared,
; and those macros go in a specific order due to GP register
; corruption concern.
    MACRO
    LoadSysRegsCtx $x0, $x1, $x2
        add $x0, #((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) - (((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8))

        PopTwoA $x0, $x1, $x2
        msr SPSR_EL3, $x1
        ; dummy val in $x2

        PopTwoA $x0, $x1, $x2
        msr CPACR_El1, $x1
        msr IFSR32_EL2, $x2

        PopTwoA $x0, $x1, $x2
        msr DACR32_EL2, $x1
        msr FPEXC32_EL2, $x2

        PopTwoA $x0, $x1, $x2
        msr CNTKCTL_EL1, $x1
        msr ACTLR_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr AFSR0_EL1, $x1
        msr AFSR1_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr AMAIR_EL1, $x1
        msr CONTEXTIDR_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr CSSELR_EL1, $x1
        msr FAR_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr ESR_EL1, $x1
        msr MAIR_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr PAR_EL1, $x1
        msr SCTLR_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr TPIDR_EL1, $x1
        msr TPIDRRO_EL0, $x2

        PopTwoA $x0, $x1, $x2
        msr TPIDR_EL0, $x1
        msr TCR_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr TTBR0_EL1, $x1
        msr TTBR1_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr VBAR_EL1, $x1
        msr CNTP_CTL_EL0, $x2

        PopTwoA $x0, $x1, $x2
        msr CNTP_CVAL_EL0, $x1
        msr CNTP_TVAL_EL0, $x2

        PopTwoA $x0, $x1, $x2
        msr SP_EL0, $x1
        msr SP_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr SPSR_fiq, $x1
        msr SPSR_und, $x2

        PopTwoA $x0, $x1, $x2
        msr SPSR_abt, $x1
        msr SPSR_irq, $x2

        PopTwoA $x0, $x1, $x2
        msr ELR_EL1, $x1
        msr SPSR_EL1, $x2

        add $x0, #((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) - (((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8))
    MEND

; ---------------------
; MACRO: SaveSysRegsCtx
; ---------------------
; Saves registers from the context in a given register
    MACRO
    SaveSysRegsCtx $x0, $x1, $x2
        sub $x0, #((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) - (((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8))

        mrs $x1, SPSR_EL1
        mrs $x2, ELR_EL1
        PushTwoA $x0, $x1, $x2

        mrs $x1, SPSR_irq
        mrs $x2, SPSR_abt
        PushTwoA $x0, $x1, $x2

        mrs $x1, SPSR_und
        mrs $x2, SPSR_fiq
        PushTwoA $x0, $x1, $x2

        mrs $x1, SP_EL1
        mrs $x2, SP_EL0
        PushTwoA $x0, $x1, $x2

        mrs $x1, CNTP_TVAL_EL0
        mrs $x2, CNTP_CVAL_EL0
        PushTwoA $x0, $x1, $x2

        mrs $x1, CNTP_CTL_EL0
        mrs $x2, VBAR_EL1
        PushTwoA $x0, $x1, $x2

        mrs $x1, TTBR1_EL1
        mrs $x2, TTBR0_EL1
        PushTwoA $x0, $x1, $x2

        mrs $x1, TCR_EL1
        mrs $x2, TPIDR_EL0
        PushTwoA $x0, $x1, $x2

        mrs $x1, TPIDRRO_EL0
        mrs $x2, TPIDR_EL1
        PushTwoA $x0, $x1, $x2

        mrs $x1, SCTLR_EL1
        mrs $x2, PAR_EL1
        PushTwoA $x0, $x1, $x2

        mrs $x1, MAIR_EL1
        mrs $x2, ESR_EL1
        PushTwoA $x0, $x1, $x2

        mrs $x1, FAR_EL1
        mrs $x2, CSSELR_EL1
        PushTwoA $x0, $x1, $x2

        mrs $x1, CONTEXTIDR_EL1
        mrs $x2, AMAIR_EL1
        PushTwoA $x0, $x1, $x2

        mrs $x1, AFSR1_EL1
        mrs $x2, AFSR0_EL1
        PushTwoA $x0, $x1, $x2

        mrs $x1, ACTLR_EL1
        mrs $x2, CNTKCTL_EL1
        PushTwoA $x0, $x1, $x2

        mrs $x1, FPEXC32_EL2
        mrs $x2, DACR32_EL2
        PushTwoA $x0, $x1, $x2

        mrs $x1, IFSR32_EL2
        mrs $x2, CPACR_El1
        PushTwoA $x0, $x1, $x2

        ldr $x1, =0xAABBAABB
        mrs $x2, SPSR_EL3
        PushTwoA $x0, $x1, $x2

        sub $x0, #((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) - (((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8))
    MEND

; ----------------
; MACRO: MigrateSysregsFromEL3ToEL1
; ----------------
; Takes EL3 system registers and writes to EL1 counterparts
    MACRO
    MigrateSysregsFromEL3ToEL1 $x0, $x1
        mrs $x0, SPSR_EL3
        msr SPSR_EL1, $x0

        mrs $x0, ELR_EL3
        msr ELR_EL1, $x0

        mrs $x0, VBAR_EL3
        msr VBAR_EL1, $x0

        mrs $x0, TTBR0_EL3
        msr TTBR0_EL1, $x0

        mrs $x0, TCR_EL3
        ldr $x1, =(0x0000003F :OR: 0x00000300 :OR: 0x00000C00 :OR: 0x00003000 :OR: 0x0000C000 :OR: 0x00070000 :OR: 0x00100000)



        and $x0, $x1 ; Mask DC vals

        ubfx $x1, $x0, #20, #(0x00100000 >> 20)

                                        ; Extract TBI and put in $x1
        lsl $x1, #37 ; Move TCR_EL3[TBI] to TCR_EL1[TBI0]
        orr $x0, $x1 ; Apply TCR_EL3[TBI] to $x0
        ldr $x1, =0x00100000
        bic $x0, $x1 ; Clear TCR_EL3[TBI] from $x0

        ubfx $x1, $x0, #16, #(0x00070000 >> 16)

                                        ; Extract PS and put in $x1
        lsl $x1, #32 ; Move TCR_EL3[PS] to TCR_EL1[IPS]
        orr $x0, $x1 ; Apply TCR_EL3[PS] to $x0
        ldr $x1, =0x00070000
        bic $x0, $x1 ; Clear TCR_EL3[PS] from $x0

        msr TCR_EL1, $x0

        mrs $x0, TPIDR_EL3
        msr TPIDR_EL1, $x0

        mrs $x0, SCTLR_EL3
        ldr $x1, =(0x00000001 :OR: 0x00000002 :OR: 0x00000004 :OR: 0x00000008 :OR: 0x00001000 :OR: 0x00080000 :OR: 0x02000000)



        and $x0, $x1 ; Mask DC vals
        msr SCTLR_EL1, $x0

        ; no EL3 for PAR

        mrs $x0, MAIR_EL3
        msr MAIR_EL1, $x0

        mrs $x0, ESR_EL3
        msr ESR_EL1, $x0

        mrs $x0, FAR_EL3
        msr FAR_EL1, $x0

        ; no EL3 for CSSELR
        ; no EL3 for CONTEXTIDR

        mrs $x0, AMAIR_EL3
        msr AMAIR_EL1, $x0

        mrs $x0, AFSR1_EL3
        msr AFSR1_EL1, $x0

        mrs $x0, AFSR0_EL3
        msr AFSR0_EL1, $x0

        mrs $x0, ACTLR_EL3 ; TODO: implementation defined
        msr ACTLR_EL1, $x0

        mrs $x0, SPSel
        cmp $x0, #0 ; 0 indicates t, else h
        mov $x0, sp ; $x0 = SBL SP
        beq %f1
        msr SP_EL1, $x0 ; SBL uses sp_el1
        mov $x1, #0x05 ; $x1 = SPSR[M] = SPh
        b %f2
1 msr SP_EL0, $x0 ; SBL uses sp_el0
        mov $x1, #0x04 ; $x1 = SPSR[M] = SPt
2 mrs $x0, DAIF ; $x0 = SPSR[DAIF]
        orr $x0, $x1 ; Combine M with DAIF
        msr SPSR_EL3, $x0 ; set SPSR[MDAIF]
                                       ; TODO: other bits for AArch32 return

    MEND
# 28 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/monitor/msm8936/src/mon_cpu_reset_arch.s" 2

; Use the pre-processor to re-define the below macro based on a
; physical address. Note that the new define is valid until
; the end of the current translation unit







    IMPORT cpu_init_asm
    IMPORT sysdbg_reset_check
    IMPORT sysdbg_entry_handler
    IMPORT |Image$$MON_ENTRY_ER$$Base|
    IMPORT a53_aarch64_sysini
    IMPORT resume_cold_boot_post_cci_wa
    IMPORT boot_lock_addr
    IMPORT mon_disable_spm_post_cci_reset
    IMPORT l2_gdhs_state
    IMPORT sdi_wa_flag

    EXPORT mon_post_cci_reset_wa_arch
    EXPORT CPU_RESET_ENTRY
    EXPORT boot_cpu_cold_booted
    EXPORT cci_wa_done
    EXPORT boot_cpu_idx
    EXPORT l2_spm_state_dbg
    ; ------------------------------------------------------------------
    ; 8x36 - cold and warm boot entry point
    ; ------------------------------------------------------------------
    PRESERVE8
    AREA CPU_RESET_ENTRY, CODE, READONLY, ALIGN=3
    ENTRY

    ; ------------------------------------------------------------------
    ; Use TPIDR to back up x0 and x30 and call sysdbg APIs to
    ; determine if this is a reset due to failure.
    ; ------------------------------------------------------------------
    msr TPIDR_EL0, x0
    msr TPIDR_EL1, x30
    bl sysdbg_reset_check ; check to see if reset due to failure
    cmp x0, #0
    bne sysdbg_entry_handler
    mrs x0, TPIDR_EL0 ; restore x0
    mrs x30, TPIDR_EL1 ; restore LR

    ; ------------------------------------------------------------------
    ; Until SBL context is saved, we may only use x3-x19 (Boot interface)
    ; ------------------------------------------------------------------
    ; ------------------------------------------------------------------
    ; Need to align the PC with the physical memory address of the
    ; code to work around the remapper.
    ; ------------------------------------------------------------------
    ldr x3, =cold_boot_remapper_reset
    br x3

    ;-------------------------------------------------------------------
    ; Memory section above may only be 0x80 bytes
    ;-------------------------------------------------------------------

    PRESERVE8
    AREA CPU_RESET_CONTINUED, CODE, READONLY, ALIGN=3

cold_boot_remapper_reset

    ; ------------------------------------------------------------------
    ; Go to either Boot CPU cold boot or generic CPU reset branch
    ; Since Boot Cpu is different for this target CPU 0 for 8936 &
    ; CPU 4 for 8939, Shifting to a common variable for 8x36/8x39 target
    ; only. Other target uses has_cpu_cold_booted with cpu index 0
    ; ------------------------------------------------------------------

    ldr w3, boot_cpu_cold_booted
    cmp w3, #0
    bne cpu_reset_branch
    mov w4, #1 ; set boot_cpu_cold_booted = TRUE
                                    ; to restrict to only one time cold boot
    ldr x3, =boot_cpu_cold_booted
    str w4, [x3]

    ; ------------------------------------------------------------------
    ; 8936/8939 - Primary / Boot CPU cold boot contd.
    ; ------------------------------------------------------------------
boot_cpu_cold_boot

    ; ------------------------------------------------------------------
    ; Do sysini. Save x0, x1, x2 and restore when done. This is
    ; needed to preserve boot arguments for cold boot
    ; ------------------------------------------------------------------
    mov x29, x0
    mov x28, x1
    mov x27, x2
    ldr x3, =((0x01900000 + 0x00037000) + 0x00016000)
    ldr w0, [x3]
    bl a53_aarch64_sysini
    mov x0, x29
    mov x1, x28
    mov x2, x27

    ; ------------------------------------------------------------------
    ; Populate primary/boot CPU idx here. Will use this to identify 8936/8939.
    ; ------------------------------------------------------------------
    CurCPUNum x3, x4 ; Return current CPU No at x3
    ldr x4, =boot_cpu_idx
    str w3, [x4] ; Populate boot cpu idx with current CPU No.

    ldr x3, =|Image$$MON_ENTRY_ER$$Base|
    br x3
    ; END
    ; ------------------------------------------------------------------
    ; continues in monitor.s (common). Jump to cold_boot_asm
    ; ------------------------------------------------------------------


    ; ------------------------------------------------------------------
    ; 8936/8939 - Secondary CPU cold boot and warm boot
    ; ------------------------------------------------------------------
cpu_reset_branch
    ;-------------------------------------------------------------------
    ; Check if its booting just after CCI reset work around, then boot
    ; CPU reset due to CCI reset work around and QSEE cold boot is
    ; still pending. Do NOT aquire boot lock before QSEE cold boot.
    ; Lock address will be populated at QSEE Cold boot.
    ;-------------------------------------------------------------------
    ldr w0, cci_wa_done
    cmp w0, #0
    beq cpu_reset_branch_without_lock

    ;-------------------------------------------------------------------
    ; Boot lock must be taken immediately and held until EL1 is done
    ; with CPU init stuff
    ;-------------------------------------------------------------------
    ldr w0, boot_lock_addr
    SpinlockObtain x0, x1, w30

    ldr w0, sdi_wa_flag
    cbz w0, cpu_reset_branch_without_lock
    ;-------------------------------------------------------------------
    ; Disable CPU & L2 SPM.
    ; QCTDD01625131:: Unable to save CPU state on WDOG reset in case
    ; the bite happened during core Power collapse.
    ; Keeping SPM_EN to preserve CPU & L2 state post secure bite reset
    ;-------------------------------------------------------------------
    ; Disable L2 SPM.
    CurCPUNum x0, x30 ; Get the current CPU No.
    ldr w1, boot_cpu_idx ; Get boot cpu idx. CPU Idx = 4/0 for 8939/8936
    cmp x0, x1 ; Compare with boot CPU No.
    bge perf_cluster_l2_spm ; If its equal or greater, its a perf cluster,

    ; Disable Power Cluster L2 SPM.
power_cluster_l2_spm
    ldr x0, =((0x0b000000 + 0x00112000) + 0x00000030)
    ldr w30, [x0]
    ; Only 1st CPU waking up in the cluster can see L2 SPM enable.
    ; Disable only if it was enable.
    tst w30, #0x1
    beq cpu_spm ; SPM_EN bit is already set to 0x0.
    bfi w30, wzr, #0x0, #1 ; Clear SPM_EN bit / Bit 0
    str w30, [x0]

2 ldr w30, [x0]
    tst w30, #0x1
    bne %b2 ; Wait for write success for spm disable.

   ; Pass info to EL1 about L2 SPM_EN was enable to double check.
    ldr w0, boot_lock_addr
    ldr w30, [x0]
    orr w30, #0xF000
    str w30, [x0]

    b cpu_spm

    ; Disable Perf Cluster L2 SPM.
perf_cluster_l2_spm
    ldr x0, =((0x0b000000 + 0x00012000) + 0x00000030)
    ldr w30, [x0]
    ; Only 1st CPU waking up in the cluster can see L2 SPM enable.
    ; Disable only if it was enable.
    tst w30, #0x1
    beq cpu_spm ; SPM_EN bit is already set to 0x0.
    bfi w30, wzr, #0x0, #1 ; Clear SPM_EN bit / Bit 0
    str w30, [x0]

3 ldr w30, [x0]
    tst w30, #0x1
    bne %b3 ; Wait for write success for spm disable.

   ; Pass info to EL1 about L2 SPM_EN was enable to double check.
    ldr w0, boot_lock_addr
    ldr w30, [x0]
    orr w30, #0xF000
    str w30, [x0]

    ; Disable APCS CPU SPM
cpu_spm
    ldr x0, =((0x0b000000 + 0x00009000) + 0x00000030)
    ldr w30, [x0]
    bfi w30, wzr, #0x0, #1 ; Clear SPM_EN bit / Bit 0
    str w30, [x0]

4 ldr w30, [x0]
    tst w30, #0x1
    bne %b4 ; Wait for write success for spm disable.

cpu_reset_branch_without_lock
    ;-------------------------------------------------------------------
    ; APCS_ALIAS[0/1]_L2_FLUSH_CTRL[ACINACTM] must be set to zero
    ; before any DVM transactions are sent to the bus, which means it
    ; must come first or else a DSB will cause a system hang.
    ;-------------------------------------------------------------------
    CurCPUNum x0, x30 ; Get the current CPU No.
    ldr w1, boot_cpu_idx ; Get boot cpu idx. CPU Idx = 4 for 8939
    cmp x0, x1 ; Compare with boot CPU No.
    bge cluster1 ; If its equal or greater, its a perf cluster

    ;-----------------------------------------------------------------------
    ;check L2 state for the other cluster and bring it online first if its
    ;in GDHS state and enable current clusters L2
    ;-----------------------------------------------------------------------
cluster0
    mov x0, #0
    ClearPerClusterGdhs w0, x1, w1 ;clear GDHS flag for the current cluster
    CurClusterNum x0
    eor w0, w0, #0x1 ;check GDHS flag for the other cluster
    PerClusterGdhsState w0, x1, w1
    cmp w0, #1
    bne %f0
    bl cluster1_acinactm_snoop_enable ;if GDHS flag is set bring its L2 online

0 bl cluster0_acinactm_snoop_enable ;L2 should be online for current cluster
    b cluster_coherency_enable_done

cluster1
    mov x0, #1
    ClearPerClusterGdhs w0, x1, w1 ;clear GDHS flag for the current cluster
    CurClusterNum x0
    eor w0, w0, #0x1 ;check GDHS flag for the other cluster
    PerClusterGdhsState w0, x1, w1
    cmp w0, #1
    bne %f1
    bl cluster0_acinactm_snoop_enable ;if GDHS flag is set bring its L2 online

1 bl cluster1_acinactm_snoop_enable ;L2 should be online for current cluster
    b cluster_coherency_enable_done

    ;-------------------------------------------------------------------
    ; Enable power cluster coherency.
    ; Power cluster ACINACTM = 0 & CCI_SNOOPCONTROL3 = 3 configured together.
    ; Wait for L2 to come online & do not corrupt x30
    ;-------------------------------------------------------------------
cluster0_acinactm_snoop_enable
    ; Read APCS_ALIAS0_L2_FLUSH_CTRL
    ldr x0, =((0x0b000000 + 0x00111000) + 0x00000218)
    ldr w3, [x0] ; Skip if ACINACTM field is zero.
    tst w3, #0x10
    beq skip_cluster0_acinactm
    ; Clear ACINACTM field
    bfi w3, wzr, #0x4, #1
    str w3, [x0] ; Write APCS_ALIAS0_L2_FLUSH_CTRL
5 ldr w3, [x0]
    tst w3, #0x10
    bne %b5 ; busy wait until ACINACTM is zero
    ;wait for L2 to come online
    ldr x0, =((0x0b000000 + 0x00111000) + 0x00000018)
6 ldr w3, [x0]
    tst w3, #0x200
    beq %b6

skip_cluster0_acinactm
    ; Snoop control
    ldr x0, =((0x0b000000 + 0x001c0000) + 0x00004000)
    mov x1, #0x3
    str w1, [x0]
    ldr x0, =((0x0b000000 + 0x001c0000) + 0x0000000c)
7 ldr w1, [x0]
    tst w1, #0x1
    bne %b7 ; busy wait until CCI_STATUS is zero
    br x30 ;return to function from where it was called

    ;-------------------------------------------------------------------
    ; Enable perf cluster coherency.
    ; Perf cluster ACINACTM = 0 & CCI_SNOOPCONTROL4 = 3 configured together.
    ; Wait for L2 to come online & do not corrupt x30.
    ;-------------------------------------------------------------------
cluster1_acinactm_snoop_enable
    ; Read APCS_ALIAS1_L2_FLUSH_CTRL
    ldr x0, =((0x0b000000 + 0x00011000) + 0x00000218)
    ldr w3, [x0] ; Skip if ACINACTM field is zero.
    tst w3, #0x10
    beq skip_cluster1_acinactm
    ; Clear ACINACTM field
    bfi w3, wzr, #0x4, #1
    str w3, [x0] ; Write APCS_ALIAS1_L2_FLUSH_CTRL
8 ldr w3, [x0]
    tst w3, #0x10
    bne %b8 ; busy wait until ACINACTM is zero
    ;Wait for L2 to come online
    ldr x0, =((0x0b000000 + 0x00011000) + 0x00000018)
9 ldr w3, [x0]
    tst w3, #0x200
    beq %b9

skip_cluster1_acinactm
    ; Snoop control
    ldr x0, =((0x0b000000 + 0x001c0000) + 0x00005000)
    mov x1, #0x3
    str w1, [x0]
    ldr x0, =((0x0b000000 + 0x001c0000) + 0x0000000c)
10 ldr w1, [x0]
    tst w1, #0x1
    bne %b10
    br x30 ;return to function from where it was called

    ; cluster coherency configuration complete.
cluster_coherency_enable_done

    ; sysini
a53_sysini
    ldr x0, =((0x01900000 + 0x00037000) + 0x00016000)
    ldr w0, [x0]
    bl a53_aarch64_sysini

    ; ------------------------------------------------------------------
    ; Continue normal CPU init stuff
    ; ------------------------------------------------------------------
    b cpu_init_asm
    ; END
    ; ------------------------------------------------------------------
    ; continues in mon_cpu_reset.s (common)
    ; ------------------------------------------------------------------


    ; ------------------------------------------------------------------
    ; 8936/8939 - Post CCI reset work around boot up. Consider 1st warm
    ; boot is due to CCI reset work around. Execution need to resume for
    ; QSEE cold boot path, since CCI/CPU reseted at the end of monitor
    ; mode cold boot.
    ; Warning!!.. Do NOT use stack here. SP will be manipulated in this API.
    ; ------------------------------------------------------------------
mon_post_cci_reset_wa_arch
    ; ------------------------------------------------------------------
    ; Mark CCI Reset Completed.
    ; ------------------------------------------------------------------

    ldr w0, cci_wa_done
    cmp w0, #0
    bne mon_post_cci_reset_wa_arch_end
    mov w0, #1
    ldr x2, =cci_wa_done
    str w0, [x2]

    ; ------------------------------------------------------------------
    ; Disable SPM, CCI Reset Completed.
    ; ------------------------------------------------------------------
    bl mon_disable_spm_post_cci_reset

    ; ------------------------------------------------------------------
    ; Reserve Stack for Non Sec Context before jumping to QSEE Cold Boot.
    ; This is done here since post CCI Reset we resume cold boot path
    ; @ resume_cold_boot_post_cci_wa. Stack reservation is done before
    ; this label during normal cold boot.path.
    ; SaveCtx will allow saving EL3 context when jump to EL1
    ; ------------------------------------------------------------------
    sub sp, #((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8)
    SaveCtx sp
    sub sp, #((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8)

    b resume_cold_boot_post_cci_wa
    ; ------------------------------------------------------------------
    ; continues in monitor.s (common)
    ; ------------------------------------------------------------------

mon_post_cci_reset_wa_arch_end
    ret

    ; END
    ; ------------------------------------------------------------------
    ; continues in mon_cpu_reset.s (common).
    ; Its a normal warm boot / secondary CPU cold boot.
    ; ------------------------------------------------------------------

    ; ------------------------------------------------------------------
    ; Variable Declaration.
    ; ------------------------------------------------------------------
    AREA MONITOR_UNCACHED, DATA, READWRITE
boot_cpu_cold_booted SPACE 4 ; Since 8936/8939 Boot CPU is
                                       ; different,defined a common variable
                                       ; to identify TZ cold booted or Not

cci_wa_done SPACE 4 ; Hold the CCI Reset Status.
                                       ; CCI reset done after L2 TCM Mode disable

boot_cpu_idx SPACE 4 ; Hold the boot CPU Idx
                                       ; ( Boot CPU is 0/4 for 8936/8939)

l2_spm_state_dbg SPACE 8 ; Hold L2 SPM State at PC Entry

    END
