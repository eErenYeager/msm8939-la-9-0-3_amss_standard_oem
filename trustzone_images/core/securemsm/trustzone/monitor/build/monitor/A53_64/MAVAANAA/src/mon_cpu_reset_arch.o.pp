# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/monitor/msm8916/src/mon_cpu_reset_arch.s"
# 1 "<built-in>" 1
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/monitor/msm8916/src/mon_cpu_reset_arch.s" 2
;============================================================================
;
; Monitor CPU Reset Entry Point
;
; Copyright 2014 by Qualcomm Technologies, Incorporated. All Rights Reserved
;============================================================================

;============================================================================
;
; EDIT HISTORY FOR MODULE
;
; $Header:
; $DateTime: 2018/02/07 00:37:16 $
; $Author: mplp4svc $
;
; This section contains comments describing changes made to the module.
; Notice that changes are listed in reverse chronological order.
;
;
; when who what, where, why
; -------- --- ---------------------------------------------------------
; 06/05/14 pre Initial revision.
;============================================================================

# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/api/systemdrivers/hwio/msm8916/phys/msmhwiobase.h" 1
# 25 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/monitor/msm8916/src/mon_cpu_reset_arch.s" 2

; Use the pre-processor to re-define the below macro based on a
; physical address. Note that the new define is valid until
; the end of the current translation unit





# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/monitor/msm8916/src/mon_hwio.h" 1
# 34 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/monitor/msm8916/src/mon_cpu_reset_arch.s" 2

    IMPORT cpu_init_asm
    IMPORT sysdbg_reset_check
    IMPORT sysdbg_entry_handler
    IMPORT |Image$$MON_ENTRY_ER$$Base|
    IMPORT has_cpu_cold_booted
    IMPORT a53_aarch64_sysini

    EXPORT CPU_RESET_ENTRY

    ; ------------------------------------------------------------------
    ; 8x16 - cold and warm boot entry point
    ; ------------------------------------------------------------------
    PRESERVE8
    AREA CPU_RESET_ENTRY, CODE, READONLY, ALIGN=3
    ENTRY

    ; ------------------------------------------------------------------
    ; Use TPIDR to back up x0 and x30 and call sysdbg APIs to
    ; determine if this is a reset due to failure.
    ; ------------------------------------------------------------------
    msr TPIDR_EL0, x0
    msr TPIDR_EL1, x30
    bl sysdbg_reset_check ; check to see if reset due to failure
    cmp x0, #0
    bne sysdbg_entry_handler
    mrs x0, TPIDR_EL0 ; restore x0
    mrs x30, TPIDR_EL1 ; restore LR

    ; ------------------------------------------------------------------
    ; Until SBL context is saved, we may only use x3-x19 (Boot interface)
    ; ------------------------------------------------------------------
    ; ------------------------------------------------------------------
    ; Need to align the PC with the physical memory address of the
    ; code to work around the remapper.
    ; ------------------------------------------------------------------
    ldr x3, =cold_boot_remapper_reset
    br x3
cold_boot_remapper_reset

    ; ------------------------------------------------------------------
    ; Do sysini. Save x0, x1, x2 and restore when done. This is
    ; needed to preserve boot arguments for cold boot, but isn't
    ; needed for cpu reset.
    ; ------------------------------------------------------------------
    mov x29, x0
    mov x28, x1
    mov x27, x2
    ldr x3, =((0x01900000 + 0x00037000) + 0x00016000)
    ldr w0, [x3]
    bl a53_aarch64_sysini
    mov x0, x29
    mov x1, x28
    mov x2, x27

    ; ------------------------------------------------------------------
    ; Go to either CPU0 cold boot or generic CPU reset branch
    ; ------------------------------------------------------------------
    ldr w3, has_cpu_cold_booted
    cmp w3, #0
    bne cpu_init_asm
    ldr x3, =|Image$$MON_ENTRY_ER$$Base|
    br x3
    ; END

    ; ------------------------------------------------------------------
    ; continues in mon_cpu_reset.s (common)
    ; ------------------------------------------------------------------

    END
