#ifndef DEFAULT_MEMORY_MAP_ARCH_H
#define DEFAULT_MEMORY_MAP_ARCH_H

#include <comdef.h>
#include "memory_defs.h"
#include "mapper.h"

#define PAS_DEVICE_BASE_ARCH 0x00000000UL
#define PAS_DEVICE_SIZE_ARCH 0x20000000UL

p_addr_t  get_secure_os_entry_addr_arch(void);
v_addr_t  get_tz_image_imem_base_addr_arch(void);
size_t    get_tz_image_len_arch(void);
p_addr_t  get_secure_os_entry_addr_arch(void);
p_addr_t* get_secure_os_mem_rgns_arch(void);
size_t*   get_secure_os_rgn_lens_arch(void);
uint32    get_num_secure_os_rgns_arch(void);
p_addr_t  get_tz_image_ddr_base_addr_arch(void) __attribute__((weak));
size_t    get_tz_image_ddr_len_arch(void) __attribute__((weak));


#endif	/* DEFAULT_MEMORY_MAP_ARCH_H */
