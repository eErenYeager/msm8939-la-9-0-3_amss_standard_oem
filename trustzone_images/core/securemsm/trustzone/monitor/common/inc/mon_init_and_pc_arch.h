#ifndef MON_COLD_BOOT_ARCH_H
#define MON_COLD_BOOT_ARCH_H

#include <comdef.h>

void sysini_arch(void);
void remove_mappings_to_tz_from_el1_arch(
  p_addr_t ns_el1_ttbr0) __attribute__((weak));
void mon_set_boot_lock_arch(uint32 lock_addr) __attribute__((weak));
void mon_set_sdi_wa(boolean sdi_flag) __attribute__((weak));
void mon_disable_l2_tcm_arch(void) __attribute__((weak));
void mon_enable_cluster_coherency_arch(void) __attribute__((weak));
void mon_secboot_set_shared_info_arch() __attribute__((weak));
void mon_cci_reset_wa_arch(uint32* cpu_pc_state_ptr) __attribute__((weak));

#endif /* MON_COLD_BOOT_ARCH_H */
