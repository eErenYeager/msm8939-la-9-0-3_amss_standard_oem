#ifndef DEFAULT_MEMORY_MAP_H
#define DEFAULT_MEMORY_MAP_H

#include <string.h> /* size_t */
#include "memory_defs.h"
#include "default_memory_map_arch.h"

#define VAS_KERNEL_BASE 0x0
#define VAS_KERNEL_END  0x100000000ULL

#define PAS_DEVICE_BASE PAS_DEVICE_BASE_ARCH
#define PAS_DEVICE_SIZE PAS_DEVICE_SIZE_ARCH

int add_to_default_memory_map
(
  v_addr_t v_addr,
  p_addr_t p_addr,
  size_t   sz_in_kb,
  tz_cfg_t tz_cfg
);

int apply_default_memory_map(void);

v_addr_t get_mon_tt_base_addr(void);
size_t   get_mon_tt_len(void);
v_addr_t get_mon_data_base_addr(void);
size_t   get_mon_data_len(void);
v_addr_t get_mon_uc_data_base_addr(void);
size_t   get_mon_uc_data_len(void);
v_addr_t get_mon_code_base_addr(void);
size_t   get_mon_code_len(void);

p_addr_t get_secure_os_entry_addr(void);

v_addr_t get_tz_image_imem_base_addr(void);
size_t   get_tz_image_len(void);

p_addr_t get_el1_tt_base_addr(void);
size_t   get_el1_tt_len(void);

void set_tzbsp_is_ext_os_config(void);

#endif /* DEFAULT_MEMORY_MAP_H */
