;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;
; GENERAL DESCRIPTION
;   This file contains the macros for TZ monitor assembly files
;
; INITIALIZATION AND SEQUENCING REQUIREMENTS
;
; Copyright (c) 2013 by QUALCOMM, Incorporated.All Rights Reserved.
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;
;                           EDIT HISTORY FOR FILE
;
; This section contains comments describing changes made to the module.
; Notice that changes are listed in reverse chronological order.
;
;
; $Header: //depot/asic/sandbox/users/ianhuang/tz/monitor/common/src/tzbsp_monitor.s#1 $
; when       who     what, where, why
; --------   ---     --------------------------------------------------------
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

; ----------------
; MACRO: ClearNSState
; ----------------
    MACRO
    ClearNSState $a, $b
        CurCPUNum x$a, x$b
        ldr     x$b, =ns_state
        strb    wzr, [x$b, x$a]
    MEND

; ----------------
; MACRO: SaveNSState
; ----------------
    MACRO
    SaveNSState $a, $b, $c
        CurCPUNum x$a, x$b
        ldr     x$b, =ns_state
        mov     x$c, #1            ; SCR_NS_BIT
        strb    w$c, [x$b, x$a]
    MEND


; -----------------
; MACRO: ReadNSState
; -----------------
    MACRO
    ReadNSState $a, $b
        CurCPUNum x$b, x$a
        ldr     x$a, =ns_state
        ldrb    w$a, [x$a, x$b]
    MEND

; -------------------------------
; MACRO: MonCallReturnToNonSecure
; -------------------------------
; All monitor calls return to non-secure mode.
;
; Arguments:
; rx: Used as a work register, set to zero.
    MACRO
    MonCallReturnToNonSecure $rx
        SetNonSecure    $rx
        mov             $rx, #0     ; Don't leak anything in this reg.
        eret                        ; Return to the normal world.
    MEND


  END
