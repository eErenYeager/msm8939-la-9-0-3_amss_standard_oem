#ifndef AA64_CTX_H
#define AA64_CTX_H

#ifndef _ARM_ASM_

#include <stddef.h>
#include "memory_defs.h"

typedef struct
{
  el3_reg_t x0;            /* r0 */
  el3_reg_t x1;            /* r1 */
  el3_reg_t x2;            /* r2 */
  el3_reg_t x3;            /* r3 */
  el3_reg_t x4;            /* r4 */
  el3_reg_t x5;            /* r5 */
  el3_reg_t x6;            /* r6 */
  el3_reg_t x7;            /* r7 */
  el3_reg_t x8;            /* r8_usr */
  el3_reg_t x9;            /* r9_usr */
  el3_reg_t x10;           /* r10_usr */
  el3_reg_t x11;           /* r11_usr */
  el3_reg_t x12;           /* r12_usr */
  el3_reg_t x13;           /* SP_usr */
  el3_reg_t x14;           /* LR_usr */
  el3_reg_t x15;           /* SP_hyp */
  el3_reg_t x16;           /* LR_irq */
  el3_reg_t x17;           /* SP_irq */
  el3_reg_t x18;           /* LR_svc */
  el3_reg_t x19;           /* SP_svc */
  el3_reg_t x20;           /* LR_abt */
  el3_reg_t x21;           /* SP_abt */
  el3_reg_t x22;           /* LR_und */
  el3_reg_t x23;           /* SP_und */
  el3_reg_t x24;           /* r8_fiq */
  el3_reg_t x25;           /* r9_fiq */
  el3_reg_t x26;           /* r10_fiq */
  el3_reg_t x27;           /* r11_fiq */
  el3_reg_t x28;           /* r12_fiq */
  el3_reg_t x29;           /* SP_fiq */
  el3_reg_t x30;           /* LR_fiq */
  el3_reg_t spsr_el1;      /* EL1 NS SPSR */
  el3_reg_t elr_el1;       /* EL1 exception link register */
  el3_reg_t spsr_irq;      /* EL1 IRQ SPSR when EL1 is AArch32 */
  el3_reg_t spsr_abt;      /* EL1 ABT SPSR when EL1 is AArch32 */
  el3_reg_t spsr_und;      /* EL1 UND SPSR when EL1 is AArch32 */
  el3_reg_t spsr_fiq;      /* EL1 FIQ SPSR when EL1 is AArch32 */
  el3_reg_t sp_el1;        /* EL1 stack pointer */
  el3_reg_t sp_el0;        /* EL0 stack pointer */
  el3_reg_t cntp_tval_el0; /* CNTP_TVAL */
  el3_reg_t cntp_cval_el0; /* CNTP_CVAL */
  el3_reg_t cntp_ctl_el0;  /* CNTP_CTL */
  el3_reg_t vbar_el1;      /* VBAR */
  el3_reg_t ttbr1_el1;     /* TTBR1 */
  el3_reg_t ttbr0_el1;     /* TTBR0 */
  el3_reg_t tcr_el1;       /* TTBCR */
  el3_reg_t tpidr_el0;     /* TPIDRURW */
  el3_reg_t tpidrro_el0;   /* TPIDRURO */
  el3_reg_t tpidr_el1;     /* TPIDRPRW */
  el3_reg_t sctlr_el1;     /* SCTLR */
  el3_reg_t par_el1;       /* PAR */
  el3_reg_t mair_el1;      /* NMRR/MAIR1 PRRR/MAIR0 */
  el3_reg_t esr_el1;       /* DFSR */
  el3_reg_t far_el1;       /* DFAR, IFAR */
  el3_reg_t csselr_el1;    /* CSSELR */
  el3_reg_t contextidr_el1;/* CONTEXTIDR */
  el3_reg_t amair_el1;     /* AMAIR0 AMAIR1 */
  el3_reg_t afsr1_el1;     /* AIFSR */
  el3_reg_t afsr0_el1;     /* ADFSR */
  el3_reg_t actlr_el1;     /* ACTLR */
  el3_reg_t cntkctl_el1;   /* CNTKCTL */
  el3_reg_t fpexc32_el2;   /* FPEXC */
  el3_reg_t dacr32_el2;    /* DACR */
  el3_reg_t ifsr32_el2;    /* IFSR */
  el3_reg_t cpacr_el1;     /* CPACR */
  el3_reg_t dummy;         /* Needed for alignment */
  el3_reg_t cpsr_el1;      /* CPSR */
  el3_reg_t elr_el3;       /* ELR_EL3, return point when exiting EL3 */
} aa_64_ctx_t;

typedef struct
{
  el3_reg_t SavedStatusReg;
  el3_reg_t d0;             /* d0 */
  el3_reg_t d1;             /* d1 */
  el3_reg_t d2;             /* d2 */
  el3_reg_t d3;             /* d3 */
  el3_reg_t d4;             /* d4 */
  el3_reg_t d5;             /* d5 */
  el3_reg_t d6;             /* d6 */
  el3_reg_t d7;             /* d7 */
  el3_reg_t d8;             /* d8 */
  el3_reg_t d9;             /* d9 */
  el3_reg_t d10;            /* d10 */
  el3_reg_t d11;            /* d11 */
  el3_reg_t d12;            /* d12 */
  el3_reg_t d13;            /* d13 */
  el3_reg_t d14;            /* d14 */
  el3_reg_t d15;            /* d15 */
  el3_reg_t d16;            /* d16 */
  el3_reg_t d17;            /* d17 */
  el3_reg_t d18;            /* d18 */
  el3_reg_t d19;            /* d19 */
  el3_reg_t d20;            /* d20 */
  el3_reg_t d21;            /* d21 */
  el3_reg_t d22;            /* d22 */
  el3_reg_t d23;            /* d23 */
  el3_reg_t d24;            /* d24 */
  el3_reg_t d25;            /* d25 */
  el3_reg_t d26;            /* d26 */
  el3_reg_t d27;            /* d27 */
  el3_reg_t d28;            /* d28 */
  el3_reg_t d29;            /* d29 */
  el3_reg_t d30;            /* d30 */
  el3_reg_t d31;            /* d31 */
  el3_reg_t fpsr;          /* fpsr */  
}aa_64_vfp_ctx_t;

//todo-sg: Not the best place to have this
//need to find a better place
typedef struct
{
  uint32 r0;
  uint32 r1;
  uint32 r2;
  uint32 r3;
  uint32 r4;
  uint32 r5;
  uint32 r6;
  uint32 r7;
  uint32 r8;
  uint32 r9;
  uint32 r10;
  uint32 r11;
  uint32 r12;
  uint32 r13;
  uint32 lr;
}el1_context_t;

typedef struct {
  aa_64_ctx_t aa_64_ctx;

  el3_reg_t sp_el3;          //Dummy place holder for SGI to store context
  el3_reg_t sp_el2;
  el3_reg_t spsr_el2;
  el3_reg_t elr_el2;
}aa_64_ctx_all_els_t;

typedef struct
{
  uint32 has_cpu_cold_booted;
  uint32 cpu_pc_state;
  uint32 cpus_in_lpm;
}cpu_power_ctx_t;

typedef struct
{
  aa_64_ctx_all_els_t aa_64_ctx_all_els;

  el1_context_t el1_context;

  //Tracking of SGI states during dumping
  uint32 sgi_state;

  //Not necessary to store the following on per core basis
  //However at times we don't get dump from all cores.
  cpu_power_ctx_t cpu_power_info;

  uint32 wdt_sts;
  uint32 sdi_hint[3];
  uint32 sc_status;

  uint32 magic;
  uint32 version;
}cpu_dbg_info_t;

#define CTX_X0_OFF offsetof(aa_64_ctx_t, x0);
#define CTX_X1_OFF offsetof(aa_64_ctx_t, x1);
#define CTX_X2_OFF offsetof(aa_64_ctx_t, x2);
#define CTX_X3_OFF offsetof(aa_64_ctx_t, x3);
#define CTX_X4_OFF offsetof(aa_64_ctx_t, x4);
#define CTX_X5_OFF offsetof(aa_64_ctx_t, x5);
#define CTX_X6_OFF offsetof(aa_64_ctx_t, x6);
#define CTX_X7_OFF offsetof(aa_64_ctx_t, x7);
#define CTX_X8_OFF offsetof(aa_64_ctx_t, x8);
#define CTX_X9_OFF offsetof(aa_64_ctx_t, x9);
#define CTX_X10_OFF offsetof(aa_64_ctx_t, x10);
#define CTX_X11_OFF offsetof(aa_64_ctx_t, x11);
#define CTX_X12_OFF offsetof(aa_64_ctx_t, x12);
#define CTX_X13_OFF offsetof(aa_64_ctx_t, x13);
#define CTX_X14_OFF offsetof(aa_64_ctx_t, x14);
#define CTX_X15_OFF offsetof(aa_64_ctx_t, x15);
#define CTX_X16_OFF offsetof(aa_64_ctx_t, x16);
#define CTX_X17_OFF offsetof(aa_64_ctx_t, x17);
#define CTX_X18_OFF offsetof(aa_64_ctx_t, x18);
#define CTX_X19_OFF offsetof(aa_64_ctx_t, x19);
#define CTX_X20_OFF offsetof(aa_64_ctx_t, x20);
#define CTX_X21_OFF offsetof(aa_64_ctx_t, x21);
#define CTX_X22_OFF offsetof(aa_64_ctx_t, x22);
#define CTX_X23_OFF offsetof(aa_64_ctx_t, x23);
#define CTX_X24_OFF offsetof(aa_64_ctx_t, x24);
#define CTX_X25_OFF offsetof(aa_64_ctx_t, x25);
#define CTX_X26_OFF offsetof(aa_64_ctx_t, x26);
#define CTX_X27_OFF offsetof(aa_64_ctx_t, x27);
#define CTX_X28_OFF offsetof(aa_64_ctx_t, x28);
#define CTX_X29_OFF offsetof(aa_64_ctx_t, x29);
#define CTX_X30_OFF offsetof(aa_64_ctx_t, x30);
#define CTX_SPSR_EL1_OFF offsetof(aa_64_ctx_t, spsr_el1);
#define CTX_ELR_EL1_OFF offsetof(aa_64_ctx_t, elr_el1);
#define CTX_SPSR_IRQ_OFF offsetof(aa_64_ctx_t, spsr_irq);
#define CTX_SPSR_ABT_OFF offsetof(aa_64_ctx_t, spsr_abt);
#define CTX_SPSR_UND_OFF offsetof(aa_64_ctx_t, spsr_und);
#define CTX_SPSR_FIQ_OFF offsetof(aa_64_ctx_t, spsr_fiq);
#define CTX_SP_EL1_OFF offsetof(aa_64_ctx_t, sp_el1);
#define CTX_SP_EL0_OFF offsetof(aa_64_ctx_t, sp_el0);
#define CTX_CNTP_TVAL_OFF offsetof(aa_64_ctx_t, cntp_tval);
#define CTX_CNTP_CVAL_OFF offsetof(aa_64_ctx_t, cntp_cval);
#define CTX_CNTP_CTL_EL0_OFF offsetof(aa_64_ctx_t, cntp_ctl_el0);
#define CTX_VBAR_EL1_OFF offsetof(aa_64_ctx_t, vbar_el1);
#define CTX_TTBR1_EL1_OFF offsetof(aa_64_ctx_t, ttbr1_el1);
#define CTX_TTBR0_EL1_OFF offsetof(aa_64_ctx_t, ttbr0_el1);
#define CTX_TCR_OFF offsetof(aa_64_ctx_t, tcr);
#define CTX_TPIDR_EL0_OFF offsetof(aa_64_ctx_t, tpidr_el0);
#define CTX_TPIDRRO_OFF offsetof(aa_64_ctx_t, tpidrro);
#define CTX_TPIDR_EL1_OFF offsetof(aa_64_ctx_t, tpidr_el1);
#define CTX_SCTLR_EL1_OFF offsetof(aa_64_ctx_t, sctlr_el1);
#define CTX_PAR_EL1_OFF offsetof(aa_64_ctx_t, par_el1);
#define CTX_MAIR_EL1_OFF offsetof(aa_64_ctx_t, mair_el1);
#define CTX_ESR_EL1_OFF offsetof(aa_64_ctx_t, esr_el1);
#define CTX_FAR_EL1_OFF offsetof(aa_64_ctx_t, far_el1);
#define CTX_CSSELR_EL1_OFF offsetof(aa_64_ctx_t, csselr_el1);
#define CTX_CONTEXTIDR_EL1_OFF offsetof(aa_64_ctx_t, contextidr_el1);
#define CTX_AMAIR_EL1_OFF offsetof(aa_64_ctx_t, amair_el1);
#define CTX_AFSR1_EL1_OFF offsetof(aa_64_ctx_t, afsr1_el1);
#define CTX_AFSR0_EL1_OFF offsetof(aa_64_ctx_t, afsr0_el1);
#define CTX_ACTLR_EL1_OFF offsetof(aa_64_ctx_t, actlr_el1);
#define CTX_CNTKCTL_EL1_OFF offsetof(aa_64_ctx_t, cntkctl_el1);
#define CTX_FPEXC32_EL2_OFF offsetof(aa_64_ctx_t, fpexc32_el2);
#define CTX_DACR32_EL2_OFF offsetof(aa_64_ctx_t, dacr32_el2);
#define CTX_IFSR32_EL2_OFF offsetof(aa_64_ctx_t, ifsr32_el2);
#define CTX_CPACR_EL1_OFF offsetof(aa_64_ctx_t, cpacr_el1);
#define CTX_DUMMY_OFF offsetof(aa_64_ctx_t, dummy);
#define CTX_CPSR_EL1_OFF offsetof(aa_64_ctx_t, cpsr_el1);
#define CTX_ELR_EL3_OFF offsetof(aa_64_ctx_t, elr_el3);
#define CTX_SZ          sizeof(aa_64_ctx_t)

#else

#define CTX_X0_OFF             0
#define CTX_X1_OFF             (CTX_X0_OFF + 0x8)
#define CTX_X2_OFF             (CTX_X1_OFF + 0x8)
#define CTX_X3_OFF             (CTX_X2_OFF + 0x8)
#define CTX_X4_OFF             (CTX_X3_OFF + 0x8)
#define CTX_X5_OFF             (CTX_X4_OFF + 0x8)
#define CTX_X6_OFF             (CTX_X5_OFF + 0x8)
#define CTX_X7_OFF             (CTX_X6_OFF + 0x8)
#define CTX_X8_OFF             (CTX_X7_OFF + 0x8)
#define CTX_X9_OFF             (CTX_X8_OFF + 0x8)
#define CTX_X10_OFF            (CTX_X9_OFF + 0x8)
#define CTX_X11_OFF            (CTX_X10_OFF + 0x8)
#define CTX_X12_OFF            (CTX_X11_OFF + 0x8)
#define CTX_X13_OFF            (CTX_X12_OFF + 0x8)
#define CTX_X14_OFF            (CTX_X13_OFF + 0x8)
#define CTX_X15_OFF            (CTX_X14_OFF + 0x8)
#define CTX_X16_OFF            (CTX_X15_OFF + 0x8)
#define CTX_X17_OFF            (CTX_X16_OFF + 0x8)
#define CTX_X18_OFF            (CTX_X17_OFF + 0x8)
#define CTX_X19_OFF            (CTX_X18_OFF + 0x8)
#define CTX_X20_OFF            (CTX_X19_OFF + 0x8)
#define CTX_X21_OFF            (CTX_X20_OFF + 0x8)
#define CTX_X22_OFF            (CTX_X21_OFF + 0x8)
#define CTX_X23_OFF            (CTX_X22_OFF + 0x8)
#define CTX_X24_OFF            (CTX_X23_OFF + 0x8)
#define CTX_X25_OFF            (CTX_X24_OFF + 0x8)
#define CTX_X26_OFF            (CTX_X25_OFF + 0x8)
#define CTX_X27_OFF            (CTX_X26_OFF + 0x8)
#define CTX_X28_OFF            (CTX_X27_OFF + 0x8)
#define CTX_X29_OFF            (CTX_X28_OFF + 0x8)
#define CTX_X30_OFF            (CTX_X29_OFF + 0x8)
/* SPSR_EL1 must be the first system register */
#define CTX_SPSR_EL1_OFF       (CTX_X30_OFF + 0x8)
#define CTX_ELR_EL1_OFF        (CTX_SPSR_EL1_OFF + 0x8)
#define CTX_SPSR_IRQ_OFF       (CTX_ELR_EL1_OFF + 0x8)
#define CTX_SPSR_ABT_OFF       (CTX_SPSR_IRQ_OFF + 0x8)
#define CTX_SPSR_UND_OFF       (CTX_SPSR_ABT_OFF + 0x8)
#define CTX_SPSR_FIQ_OFF       (CTX_SPSR_UND_OFF + 0x8)
#define CTX_SP_EL1_OFF         (CTX_SPSR_FIQ_OFF + 0x8)
#define CTX_SP_EL0_OFF         (CTX_SP_EL1_OFF + 0x8)
#define CTX_CNTP_TVAL_OFF      (CTX_SP_EL0_OFF + 0x8)
#define CTX_CNTP_CVAL_OFF      (CTX_CNTP_TVAL_OFF + 0x8)
#define CTX_CNTP_CTL_EL0_OFF   (CTX_CNTP_CVAL_OFF + 0x8)
#define CTX_VBAR_EL1_OFF       (CTX_CNTP_CTL_EL0_OFF + 0x8)
#define CTX_TTBR1_EL1_OFF      (CTX_VBAR_EL1_OFF + 0x8)
#define CTX_TTBR0_EL1_OFF      (CTX_TTBR1_EL1_OFF + 0x8)
#define CTX_TCR_OFF            (CTX_TTBR0_EL1_OFF + 0x8)
#define CTX_TPIDR_EL0_OFF      (CTX_TCR_OFF + 0x8)
#define CTX_TPIDRRO_OFF        (CTX_TPIDR_EL0_OFF + 0x8)
#define CTX_TPIDR_EL1_OFF      (CTX_TPIDRRO_OFF + 0x8)
#define CTX_SCTLR_EL1_OFF      (CTX_TPIDR_EL1_OFF + 0x8)
#define CTX_PAR_EL1_OFF        (CTX_SCTLR_EL1_OFF + 0x8)
#define CTX_MAIR_EL1_OFF       (CTX_PAR_EL1_OFF + 0x8)
#define CTX_ESR_EL1_OFF        (CTX_MAIR_EL1_OFF + 0x8)
#define CTX_FAR_EL1_OFF        (CTX_ESR_EL1_OFF + 0x8)
#define CTX_CSSELR_EL1_OFF     (CTX_FAR_EL1_OFF + 0x8)
#define CTX_CONTEXTIDR_EL1_OFF (CTX_CSSELR_EL1_OFF + 0x8)
#define CTX_AMAIR_EL1_OFF      (CTX_CONTEXTIDR_EL1_OFF + 0x8)
#define CTX_AFSR1_EL1_OFF      (CTX_AMAIR_EL1_OFF + 0x8)
#define CTX_AFSR0_EL1_OFF      (CTX_AFSR1_EL1_OFF + 0x8)
#define CTX_ACTLR_EL1_OFF      (CTX_AFSR0_EL1_OFF + 0x8)
#define CTX_CNTKCTL_EL1_OFF    (CTX_ACTLR_EL1_OFF + 0x8)
#define CTX_FPEXC32_EL2_OFF    (CTX_CNTKCTL_EL1_OFF + 0x8)
#define CTX_DACR32_EL2_OFF     (CTX_FPEXC32_EL2_OFF + 0x8)
#define CTX_IFSR32_EL2_OFF     (CTX_DACR32_EL2_OFF + 0x8)
#define CTX_CPACR_EL1_OFF      (CTX_IFSR32_EL2_OFF + 0x8)
#define CTX_DUMMY_OFF          (CTX_CPACR_EL1_OFF + 0x8)
/* DUMMY must be the last system register */
#define CTX_CPSR_EL1_OFF       (CTX_DUMMY_OFF + 0x8)
#define CTX_ELR_EL3_OFF        (CTX_CPSR_EL1_OFF + 0x8)
#define CTX_SZ                 (CTX_ELR_EL3_OFF + 0x8)

#endif

#endif /* AA64_CTX_H */
