#ifndef MONITOR_H
#define MONITOR_H
/**
   @file monitor.h
   @brief EL3 implementation
 */
/*===========================================================================
   Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/monitor/common/inc/monitor.h#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $

when       who      what, where, why
--------   ---      ------------------------------------
05/29/14   pre      Initial revision
===========================================================================*/

/* TODO: test stack usage of EL3 */
#define STACK_LEN 0x800

#ifndef _ARM_ASM_

#include "memory_defs.h"

#define L2_GDHS_BMSK    3

typedef struct s_el1_reg_s
{
  uint32 v;  /* value */
  uint32 dc; /* don't care */
} s_el1_regs_t;

typedef struct
{
  el3_reg_t x0;            /* r0 */
  el3_reg_t x1;            /* r1 */
  el3_reg_t x2;            /* r2 */
  el3_reg_t x3;            /* r3 */
  el3_reg_t x4;            /* r4 */
  el3_reg_t x5;            /* r5 */
  el3_reg_t x6;            /* r6 */
  el3_reg_t x7;            /* r7 */
  el3_reg_t x8;            /* r8_usr */
  el3_reg_t x9;            /* r9_usr */
  el3_reg_t x10;           /* r10_usr */
  el3_reg_t x11;           /* r11_usr */
  el3_reg_t x12;           /* r12_usr */
  el3_reg_t x13;           /* SP_usr */
  el3_reg_t x14;           /* LR_usr */
  el3_reg_t x15;           /* SP_hyp */
  el3_reg_t x16;           /* LR_irq */
  el3_reg_t x17;           /* SP_irq */
  el3_reg_t x18;           /* LR_svc */
  el3_reg_t x19;           /* SP_svc */
  el3_reg_t x29;           /* SP_fiq */
  el3_reg_t x30;           /* LR_fiq */
} ns_smc_ctx_t;

typedef struct
{
  s_el1_regs_t x0;         /* r0 */
  s_el1_regs_t x1;         /* r1 */
  s_el1_regs_t x2;         /* r2 */
  s_el1_regs_t x3;         /* r3 */
  s_el1_regs_t x4;         /* r4 */
  s_el1_regs_t x5;         /* r5 */
  s_el1_regs_t x6;         /* r6 */
  s_el1_regs_t x7;         /* r7 */
  s_el1_regs_t x8;         /* r8_usr */
  s_el1_regs_t x9;         /* r9_usr */
  s_el1_regs_t x10;        /* r10_usr */
  s_el1_regs_t x11;        /* r11_usr */
  s_el1_regs_t x12;        /* r12_usr */
  s_el1_regs_t x13;        /* SP_usr */
  s_el1_regs_t x14;        /* LR_usr */
  s_el1_regs_t x15;        /* SP_hyp */
  s_el1_regs_t x16;        /* LR_irq */
  s_el1_regs_t x17;        /* SP_irq */
  s_el1_regs_t x18;        /* LR_svc */
  s_el1_regs_t x19;        /* SP_svc */
  s_el1_regs_t x29;        /* SP_fiq */
  s_el1_regs_t x30;        /* LR_fiq */
} s_smc_ctx_t;

/* TODO: should move to a common area between qsee/monitor */
typedef enum {
  SMP_NOTIFIER_CMD_PC    = 0,
  SMP_NOTIFIER_CMD_SPMI  = 1
} el1_smp_notifier_cmd_t;

typedef enum {
  CPU_PC           = 0,
  CPU_AND_L2_PC    = 1,
  PC_FALLTHROUGH   = 2,
  CPU_AND_L2_GDHS  = 3
} el1_pc_notifier_t;

typedef enum {
  SPMI_SUCCESS     = 0,
  SPMI_FAILURE     = 1
} el1_spmi_notifier_t;

typedef union {
  el1_pc_notifier_t el1_pc_notifier;
  el1_spmi_notifier_t el1_spmi_notifier;
} el1_smp_notifier_t;

//Declared in
extern uint32 shared_boot_info_addr;

p_addr_t  get_ns_el1_ttbr0(void);
el3_reg_t get_ns_el1_x30(void);
el3_reg_t get_el3_sp(void);

/**
   Called before returning to NS EL1 in order to pass return values.
   ARM SMC calling convention indicates a maximum of four return
   values.
 */
void set_ns_return_registers(el3_reg_t x0,
                             el3_reg_t x1,
                             el3_reg_t x2,
                             el3_reg_t x3);

/**
   Set ELR EL3 to be used when returning to NS
 */
void set_ns_el3_elr(el3_reg_t addr);

void set_ns_cpsr(el3_reg_t ns_cpsr);
v_addr_t get_ns_context_addr();
el1_smp_notifier_t el1_smp_notifier_asm(p_addr_t addr, el1_smp_notifier_cmd_t cmd, uint32 arg0, uint32 arg1);

void clear_el1_ns_ctx(void);

/* Below functions are in mon_init_and_pc.c */
el3_reg_t enter_pc(el3_reg_t arg);

void set_sysdbg_entry_and_buf(p_addr_t sysdbg_entry_addr, p_addr_t sysdbg_ctxt_buf);

uint32 mon_get_sysdbg_entry_addr(void);

uint32 mon_get_sysdbg_buf_addr(void);

el3_reg_t get_ns_state(void);

void vfp_handler_func(void *ptr, int VFPStatusFlag);

/* Below functions are in mon_smp_notifier.c */
el1_smp_notifier_t el1_smp_notifier(el1_smp_notifier_cmd_t cmd, uint32 arg0, uint32 arg1);
void set_secure_os_smp_notifier(p_addr_t smp_notifier_addr);

el3_reg_t spmi_bus_disable(el3_reg_t spare);

#endif /* _ARM_ASM_ */

#endif /* MONITOR_H */
