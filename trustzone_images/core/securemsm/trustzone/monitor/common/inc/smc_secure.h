#ifndef SMC_SECURE_H
#define SMC_SECURE_H
/**
 * @file smc_secure.h
 * @brief API for SMC from secure world
 */

/*===========================================================================
   Copyright (c) 2013 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/monitor/common/inc/smc_secure.h#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $

when       who      what, where, why
--------   ---      ------------------------------------
===========================================================================*/

typedef enum
{
  /* Return to NS. Secure context saved. */
  SMC_CMD_RETURN_TO_NS                  =  0,

  /* Enter NS OS or boot-loader. Secure context discarded. */
  SMC_CMD_ENTER_NS_OS                   =  1,

  /* FIQ Done. If FIQ originated in NS, secure context is discarded
   * and NS is restored. Otherwise, returns to secure world.
   */
  SMC_CMD_FIQ_DONE                      =  2,

  /* Disable NS MMU and set NS return address. Returns to secure. */
  SMC_CMD_MILESTONE_SET                 =  3,

  /* Passes the address for a boot lock shared between EL3 and the
     secure kernel. */
  SMC_CMD_SET_BOOT_LOCK_ADDR            =  4,

  /* Secure EL1 memory mapping routines */
  SMC_CMD_MMU_MAP                       =  5,
  SMC_CMD_MMU_MAP_BLOCK                 =  6,
  SMC_CMD_GET_EL1_TT_MEM_INFO           =  7,
  SMC_CMD_GET_EL3_TT_MEM_INFO           =  8,

  /* Perform Mode switch for EL1 to A64 */
  SMC_CMD_DO_MODE_SWITCH                =  9,

  /* Configure Entry Address for Hypervisor */
  SMC_CONFIG_HYP_ENTRY                  = 10,

  /* Used during boot in badger-family boot flow where cold boot is
     initialized as a function call. This is the exit point for the TZ
     image for the initial boot flow. */
  SMC_CMD_GET_NS_W30                    = 11,

  /* Used when EL1 has modified the NS context directly */
  SMC_CMD_RETURN_TO_NS_NO_REGS_MODIFIED = 12,

  /* Set a generic EL1 notifier to call when an SMP execution
     environment is requested. */
  SMC_CMD_SET_SMP_NOTIFIER               = 13,

  /* Called when EL1 PC notifier completes */
  SMC_CMD_RETURN_FROM_PC_NOTIFIER       = 14,

  /* Called by EL1 when an IRQ happens.  This is to leverage the
     monitor to save the interrupted state of secure EL1 and return to
     NS. */
  SMC_CMD_IRQ                           = 15,

  /* Instruct Monitor i.e. EL3 to dump context */
  SMC_CMD_DUMP_CONTEXT                  = 16,

  SMC_CMD_SET_SYSDBG_ENTRY_AND_BUF      = 17,

  /* Only EL3 can set the timer frequency */
  SMC_CMD_SET_TIMER_FREQ                = 18,

  /* EL1 parses the boot info structure and informs EL3 of whether
     debugging is enabled in the cert and EL3 updates system registers
     accordingly. */
  SMC_CMD_SET_DEBUG                     = 19,

  /* transfer secure timer to EL3 */
  SMC_CMD_ONESHOT_START_CPU_TIMER       = 20,
  SMC_CMD_SET_CPU_TIMER                 = 21,
  SMC_CMD_GET_CPU_TIMER_COUNT           = 22,

  /* Dump calls */
  SMC_CMD_GET_A53_TLB_DUMP_SZ           = 23,
  SMC_CMD_DUMP_A53_TLB                  = 24,
  SMC_CMD_GET_A53_ICACHE_DUMP_SZ        = 25,
  SMC_CMD_DUMP_A53_ICACHE               = 26,
  SMC_CMD_GET_A53_DCACHE_DUMP_SZ        = 27,
  SMC_CMD_DUMP_A53_DCACHE               = 28,

  /* Instruct Monitor i.e. EL3 to dump fvp context */
  SMC_CMD_DUMP_VFP_CONTEXT              = 29,

  /* set the is_ext_os_config to true */
  SMC_CMD_SET_EXT_OS_CONFIG             = 30,
  
   /* set this flag to have SDI work around */
  SMC_CMD_SET_SDI_WA_FLAG        = 31,

  /* Get Monitor Code Base Address and Length */
  SMC_CMD_GET_MON_CODE_INFO             = 34,

} mon_call_id_t;



#define SMC_EVENT_SYSCALL               0
#define SMC_EVENT_POWER_COLLAPSE_ENTRY  1
#define SMC_EVENT_POWER_COLLAPSE_EXIT   2

#define SMC_FIQ_RETURN_TO_SECURE        0
#define SMC_FIQ_RETURN_TO_NS            1

#define SMC_RETURN_TO_SECURE            0
#define SMC_RETURN_TO_NS                1

#define SMC_INVALID_VALUE               99

/* Perform Mode switch for EL1 to A64 */
typedef struct hlos_boot_params_s
{
  uint64 el1_x0;
  uint64 el1_x1;
  uint64 el1_x2;
  uint64 el1_x3;
  uint64 el1_x4;
  uint64 el1_x5;
  uint64 el1_x6;
  uint64 el1_x7;
  uint64 el1_x8;
  uint64 el1_elr;
} hlos_boot_params_t;
#endif
