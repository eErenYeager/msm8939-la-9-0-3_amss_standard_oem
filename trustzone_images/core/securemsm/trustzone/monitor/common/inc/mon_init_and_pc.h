#ifndef MON_INIT_AND_PC_H
#define MON_INIT_AND_PC_H

#include <comdef.h>

extern const int32 NUM_PARAMS_FROM_SBL;
extern uint64 shared_boot_info_params[];

uint32 mon_get_has_cpu_cold_booted(uint32 cpu);
uint32 mon_get_num_cpus_in_lpm(void);
uint32 mon_get_cpu_pc_state(uint32 cpu);

#endif /* MON_INIT_AND_PC_H */
