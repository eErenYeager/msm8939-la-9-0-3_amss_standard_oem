#include "aa64_ctx.h"

; -------------------
; MACRO: PushTwoA
; -------------------
; Like push, but memory addresses ascend instead of descend
    MACRO
    PushTwoA $x0, $x1, $x2
        stp     $x1, $x2, [$x0], #0x10
    MEND

; -------------------
; MACRO: PopTwoA
; -------------------
    MACRO
    PopTwoA $x0, $x1, $x2
        ldp     $x2, $x1, [$x0, #-0x10]!
    MEND
;
; TODO: CPACR_EL1 - for access to floating point and Advanced SIMD
;                   execution
;
; ----------------
; MACRO: SaveCtx
; ----------------
; $sp should be a pointer to the base address of a context structure.
; See aa_64_ctx_t in aa64_ctx.h
    MACRO
    SaveCtx $sp
        PushTwoA $sp, x0,  x1
        PushTwoA $sp, x2,  x3
        PushTwoA $sp, x4,  x5
        PushTwoA $sp, x6,  x7
        PushTwoA $sp, x8,  x9
        PushTwoA $sp, x10, x11
        PushTwoA $sp, x12, x13
        PushTwoA $sp, x14, x15
        PushTwoA $sp, x16, x17
        PushTwoA $sp, x18, x19
        PushTwoA $sp, x20, x21
        PushTwoA $sp, x22, x23
        PushTwoA $sp, x24, x25
        PushTwoA $sp, x26, x27
        PushTwoA $sp, x28, x29

        mrs     x0, SPSR_EL1
        PushTwoA $sp, x30, x0

        mrs     x0, ELR_EL1
        mrs     x1, SPSR_irq
        PushTwoA $sp, x0, x1

        mrs     x0, SPSR_abt
        mrs     x1, SPSR_und
        PushTwoA $sp, x0, x1

        mrs     x0, SPSR_fiq
        mrs     x1, SP_EL1
        PushTwoA $sp, x0, x1

        mrs     x0, SP_EL0
        mrs     x1, CNTP_TVAL_EL0
        PushTwoA $sp, x0, x1

        mrs     x0, CNTP_CVAL_EL0
        mrs     x1, CNTP_CTL_EL0
        PushTwoA $sp, x0, x1

        mrs     x0, VBAR_EL1
        mrs     x1, TTBR1_EL1
        PushTwoA $sp, x0, x1

        mrs     x0, TTBR0_EL1
        mrs     x1, TCR_EL1
        PushTwoA $sp, x0, x1

        mrs     x0, TPIDR_EL0
        mrs     x1, TPIDRRO_EL0
        PushTwoA $sp, x0, x1

        mrs     x0, TPIDR_EL1
        mrs     x1, SCTLR_EL1
        PushTwoA $sp, x0, x1

        mrs     x0, PAR_EL1
        mrs     x1, MAIR_EL1
        PushTwoA $sp, x0, x1

        mrs     x0, ESR_EL1
        mrs     x1, FAR_EL1
        PushTwoA $sp, x0, x1

        mrs     x0, CSSELR_EL1
        mrs     x1, CONTEXTIDR_EL1
        PushTwoA $sp, x0, x1

        mrs     x0, AMAIR_EL1
        mrs     x1, AFSR1_EL1
        PushTwoA $sp, x0, x1

        mrs     x0, AFSR0_EL1
        mrs     x1, ACTLR_EL1
        PushTwoA $sp, x0, x1

        mrs     x0, CNTKCTL_EL1
        mrs     x1, FPEXC32_EL2
        PushTwoA $sp, x0, x1

        mrs     x0, DACR32_EL2
        mrs     x1, IFSR32_EL2
        PushTwoA $sp, x0, x1

        mrs     x0, CPACR_El1
        ldr     x1, =0xAABBAABB
        PushTwoA $sp, x0, x1

        mrs     x0, SPSR_EL3
        mrs     x1, ELR_EL3
        PushTwoA $sp, x0, x1
    MEND

; ----------------
; MACRO: LoadCtx
; ----------------
; $sp should be a pointer to the base address of a context structure.
; See aa_64_ctx_t in aa64_ctx.h.  The order of removing items from the
; structure is important to properly restore x0, x1.
    MACRO
    LoadCtx $sp
        add $sp, #CTX_SZ

        PopTwoA $sp, x0, x1
        msr     ELR_EL3, x0
        msr     SPSR_EL3, x1

        PopTwoA $sp, x0, x1
        ; dummy val in x0
        msr     CPACR_El1, x1

        PopTwoA $sp, x0, x1
        msr     IFSR32_EL2, x0
        msr     DACR32_EL2, x1

        PopTwoA $sp, x0, x1
        msr     FPEXC32_EL2, x0
        msr     CNTKCTL_EL1, x1

        PopTwoA $sp, x0, x1
        msr     ACTLR_EL1, x0
        msr     AFSR0_EL1, x1

        PopTwoA $sp, x0, x1
        msr     AFSR1_EL1, x0
        msr     AMAIR_EL1, x1

        PopTwoA $sp, x0, x1
        msr     CONTEXTIDR_EL1, x0
        msr     CSSELR_EL1, x1

        PopTwoA $sp, x0, x1
        msr     FAR_EL1, x0
        msr     ESR_EL1, x1

        PopTwoA $sp, x0, x1
        msr     MAIR_EL1, x0
        msr     PAR_EL1, x1

        PopTwoA $sp, x0, x1
        msr     SCTLR_EL1, x0
        msr     TPIDR_EL1, x1

        PopTwoA $sp, x0, x1
        msr     TPIDRRO_EL0, x0
        msr     TPIDR_EL0, x1

        PopTwoA $sp, x0, x1
        msr     TCR_EL1, x0
        msr     TTBR0_EL1, x1

        PopTwoA $sp, x0, x1
        msr     TTBR1_EL1, x0
        msr     VBAR_EL1, x1

        PopTwoA $sp, x0, x1
        msr     CNTP_CTL_EL0, x0
        msr     CNTP_CVAL_EL0, x1

        PopTwoA $sp, x0, x1
        msr     CNTP_TVAL_EL0, x0
        msr     SP_EL0, x1

        PopTwoA $sp, x0, x1
        msr     SP_EL1, x0
        msr     SPSR_fiq, x1

        PopTwoA $sp, x0, x1
        msr     SPSR_und, x0
        msr     SPSR_abt, x1

        PopTwoA $sp, x0, x1
        msr     SPSR_irq, x0
        msr     ELR_EL1, x1

        PopTwoA $sp, x1, x30
        msr     SPSR_EL1, x1

        PopTwoA $sp,  x29, x28
        PopTwoA $sp,  x27, x26
        PopTwoA $sp,  x25, x24
        PopTwoA $sp,  x23, x22
        PopTwoA $sp,  x21, x20
        PopTwoA $sp,  x19, x18
        PopTwoA $sp,  x17, x16
        PopTwoA $sp,  x15, x14
        PopTwoA $sp,  x13, x12
        PopTwoA $sp,  x11, x10
        PopTwoA $sp,  x9,  x8
        PopTwoA $sp,  x7,  x6
        PopTwoA $sp,  x5,  x4
        PopTwoA $sp,  x3,  x2
        PopTwoA $sp,  x1,  x0

        add $sp, #CTX_SZ
    MEND

; ---------------------
; MACRO: LoadSysRegsCtx
; ---------------------
; Loads registers from the context in a given register
; Note: we do not need to go in a specific order, since there is no
; concern for corrupting GP registers, but it's good to maintain the
; same order as savectx and loadctx so the operations can be compared,
; and those macros go in a specific order due to GP register
; corruption concern.
    MACRO
    LoadSysRegsCtx $x0, $x1, $x2
        add     $x0, #(CTX_ELR_EL3_OFF - CTX_SPSR_EL1_OFF)

        PopTwoA $x0, $x1, $x2
        msr     SPSR_EL3, $x1
        ; dummy val in $x2

        PopTwoA $x0, $x1, $x2
        msr     CPACR_El1, $x1
        msr     IFSR32_EL2, $x2

        PopTwoA $x0, $x1, $x2
        msr     DACR32_EL2, $x1
        msr     FPEXC32_EL2, $x2

        PopTwoA $x0, $x1, $x2
        msr     CNTKCTL_EL1, $x1
        msr     ACTLR_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr     AFSR0_EL1, $x1
        msr     AFSR1_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr     AMAIR_EL1, $x1
        msr     CONTEXTIDR_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr     CSSELR_EL1, $x1
        msr     FAR_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr     ESR_EL1, $x1
        msr     MAIR_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr     PAR_EL1, $x1
        msr     SCTLR_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr     TPIDR_EL1, $x1
        msr     TPIDRRO_EL0, $x2

        PopTwoA $x0, $x1, $x2
        msr     TPIDR_EL0, $x1
        msr     TCR_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr     TTBR0_EL1, $x1
        msr     TTBR1_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr     VBAR_EL1, $x1
        msr     CNTP_CTL_EL0, $x2

        PopTwoA $x0, $x1, $x2
        msr     CNTP_CVAL_EL0, $x1
        msr     CNTP_TVAL_EL0, $x2

        PopTwoA $x0, $x1, $x2
        msr     SP_EL0, $x1
        msr     SP_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr     SPSR_fiq, $x1
        msr     SPSR_und, $x2

        PopTwoA $x0, $x1, $x2
        msr     SPSR_abt, $x1
        msr     SPSR_irq, $x2

        PopTwoA $x0, $x1, $x2
        msr     ELR_EL1, $x1
        msr     SPSR_EL1, $x2

        add     $x0, #(CTX_ELR_EL3_OFF - CTX_SPSR_EL1_OFF)
    MEND

; ---------------------
; MACRO: SaveSysRegsCtx
; ---------------------
; Saves registers from the context in a given register
    MACRO
    SaveSysRegsCtx $x0, $x1, $x2
        sub     $x0, #(CTX_ELR_EL3_OFF - CTX_SPSR_EL1_OFF)

        mrs     $x1, SPSR_EL1
        mrs     $x2, ELR_EL1
        PushTwoA $x0, $x1, $x2

        mrs     $x1, SPSR_irq
        mrs     $x2, SPSR_abt
        PushTwoA $x0, $x1, $x2

        mrs     $x1, SPSR_und
        mrs     $x2, SPSR_fiq
        PushTwoA $x0, $x1, $x2

        mrs     $x1, SP_EL1
        mrs     $x2, SP_EL0
        PushTwoA $x0, $x1, $x2

        mrs     $x1, CNTP_TVAL_EL0
        mrs     $x2, CNTP_CVAL_EL0
        PushTwoA $x0, $x1, $x2

        mrs     $x1, CNTP_CTL_EL0
        mrs     $x2, VBAR_EL1
        PushTwoA $x0, $x1, $x2

        mrs     $x1, TTBR1_EL1
        mrs     $x2, TTBR0_EL1
        PushTwoA $x0, $x1, $x2

        mrs     $x1, TCR_EL1
        mrs     $x2, TPIDR_EL0
        PushTwoA $x0, $x1, $x2

        mrs     $x1, TPIDRRO_EL0
        mrs     $x2, TPIDR_EL1
        PushTwoA $x0, $x1, $x2

        mrs     $x1, SCTLR_EL1
        mrs     $x2, PAR_EL1
        PushTwoA $x0, $x1, $x2

        mrs     $x1, MAIR_EL1
        mrs     $x2, ESR_EL1
        PushTwoA $x0, $x1, $x2

        mrs     $x1, FAR_EL1
        mrs     $x2, CSSELR_EL1
        PushTwoA $x0, $x1, $x2

        mrs     $x1, CONTEXTIDR_EL1
        mrs     $x2, AMAIR_EL1
        PushTwoA $x0, $x1, $x2

        mrs     $x1, AFSR1_EL1
        mrs     $x2, AFSR0_EL1
        PushTwoA $x0, $x1, $x2

        mrs     $x1, ACTLR_EL1
        mrs     $x2, CNTKCTL_EL1
        PushTwoA $x0, $x1, $x2

        mrs     $x1, FPEXC32_EL2
        mrs     $x2, DACR32_EL2
        PushTwoA $x0, $x1, $x2

        mrs     $x1, IFSR32_EL2
        mrs     $x2, CPACR_El1
        PushTwoA $x0, $x1, $x2

        ldr     $x1, =0xAABBAABB
        mrs     $x2, SPSR_EL3
        PushTwoA $x0, $x1, $x2

        sub     $x0, #(CTX_ELR_EL3_OFF - CTX_SPSR_EL1_OFF)
    MEND

; ----------------
; MACRO: MigrateSysregsFromEL3ToEL1
; ----------------
; Takes EL3 system registers and writes to EL1 counterparts
    MACRO
    MigrateSysregsFromEL3ToEL1 $x0, $x1
        mrs     $x0, SPSR_EL3
        msr     SPSR_EL1, $x0

        mrs     $x0, ELR_EL3
        msr     ELR_EL1, $x0

        mrs     $x0, VBAR_EL3
        msr     VBAR_EL1, $x0

        mrs     $x0, TTBR0_EL3
        msr     TTBR0_EL1, $x0

        mrs     $x0, TCR_EL3
        ldr     $x1, =(TCR_EL3_T0SZ_BMSK :OR: TCR_EL3_IRGN0_BMSK :OR:       \
                       TCR_EL3_ORGN0_BMSK :OR: TCR_EL3_SH0_BMSK :OR:        \
                       TCR_EL3_TG0_BMSK :OR: TCR_EL3_PS_BMSK :OR:           \
                       TCR_EL3_TBI_BIT)
        and     $x0, $x1                ; Mask DC vals

        ubfx    $x1, $x0, #TCR_EL3_TBI_SHFT, #(TCR_EL3_TBI_BIT >> \
                                               TCR_EL3_TBI_SHFT)
                                        ; Extract TBI and put in $x1
        lsl     $x1, #TCR_EL1_TBI0_SHFT ; Move TCR_EL3[TBI] to TCR_EL1[TBI0]
        orr     $x0, $x1                ; Apply TCR_EL3[TBI] to $x0
        ldr     $x1, =TCR_EL3_TBI_BIT
        bic     $x0, $x1                ; Clear TCR_EL3[TBI] from $x0

        ubfx    $x1, $x0, #TCR_EL3_PS_SHFT, #(TCR_EL3_PS_BMSK >> \
                                              TCR_EL3_PS_SHFT)
                                        ; Extract PS and put in $x1
        lsl     $x1, #TCR_EL1_IPS_SHFT  ; Move TCR_EL3[PS] to TCR_EL1[IPS]
        orr     $x0, $x1                ; Apply TCR_EL3[PS] to $x0
        ldr     $x1, =TCR_EL3_PS_BMSK
        bic     $x0, $x1                ; Clear TCR_EL3[PS] from $x0

        msr     TCR_EL1, $x0

        mrs     $x0, TPIDR_EL3
        msr     TPIDR_EL1, $x0

        mrs     $x0, SCTLR_EL3
        ldr     $x1, =(SCTLR_EL3_M_BIT :OR: SCTLR_EL3_A_BIT :OR:   \
                       SCTLR_EL3_C_BIT :OR: SCTLR_EL3_SA_BIT :OR:  \
                       SCTLR_EL3_I_BIT :OR: SCTLR_EL3_WXN_BIT :OR: \
                       SCTLR_EL3_EE_BIT)
        and     $x0, $x1                ; Mask DC vals
        msr     SCTLR_EL1, $x0

        ; no EL3 for PAR

        mrs     $x0, MAIR_EL3
        msr     MAIR_EL1, $x0

        mrs     $x0, ESR_EL3
        msr     ESR_EL1, $x0

        mrs     $x0, FAR_EL3
        msr     FAR_EL1, $x0

        ; no EL3 for CSSELR
        ; no EL3 for CONTEXTIDR

        mrs     $x0, AMAIR_EL3
        msr     AMAIR_EL1, $x0

        mrs     $x0, AFSR1_EL3
        msr     AFSR1_EL1, $x0

        mrs     $x0, AFSR0_EL3
        msr     AFSR0_EL1, $x0

        mrs     $x0, ACTLR_EL3         ; TODO: implementation defined
        msr     ACTLR_EL1, $x0

        mrs     $x0, SPSel
        cmp     $x0, #0                ; 0 indicates t, else h
        mov     $x0, sp                ; $x0 = SBL SP
        beq     %f1
        msr     SP_EL1, $x0            ; SBL uses sp_el1
        mov     $x1, #SPSR_MODE_EL1h   ; $x1 = SPSR[M] = SPh
        b       %f2
1       msr     SP_EL0, $x0            ; SBL uses sp_el0
        mov     $x1, #SPSR_MODE_EL1t   ; $x1 = SPSR[M] = SPt
2       mrs     $x0, DAIF              ; $x0 = SPSR[DAIF]
        orr     $x0, $x1               ; Combine M with DAIF
        msr     SPSR_EL3, $x0          ; set SPSR[MDAIF]
                                       ; TODO: other bits for AArch32 return

    MEND
