#ifndef MON_SEC_IO_H
#define MON_SEC_IO_H

#include <comdef.h>
#include "memory_defs.h"

/* Implemented in ARCH folder */
int secure_io_write(p_addr_t addr, unsigned int value);
/* Implemented in ARCH folder */
unsigned int secure_io_read(p_addr_t addr);

#endif /* MON_SEC_IO_H */
