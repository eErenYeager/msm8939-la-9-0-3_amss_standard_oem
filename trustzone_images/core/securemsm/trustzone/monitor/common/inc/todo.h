#ifndef TODO_H
#define TODO_H

#include <comdef.h>

#define E_SUCCESS 0
#define E_FAILURE 1
#define E_NO_MEMORY 2
#define E_BAD_ADDRESS 3
#define E_OUT_OF_RANGE 4
#define E_NOT_SUPPORTED 5
#define E_NOT_FOUND     6

/****************************************************************************
   TODO: Monitor should have its own logging/debug stuff maybe?
 ****************************************************************************/
#define ERROR_CODE(xx)
#define LOG_DBG(xx, yy)

/****************************************************************************
   Set by kernel.  Need monitor call to set.
 ****************************************************************************/
__inline boolean is_ns_debug_fiq_allowed(void) {return TRUE;}
__inline boolean is_debug_int_allowed(uint32 intnum) {return TRUE;}
__inline boolean is_cache_dumping_allowed(void) {return TRUE;}


/****************************************************************************
   TODO: Does this refer to Monitor's cache settings or kernel's cache
         settings?
 ****************************************************************************/
__inline boolean is_icache_enabled(void) {return TRUE;}
__inline boolean is_dcache_enabled(void) {return TRUE;}
__inline void disable_dcache(void) {}
__inline void enable_dcache(void) {}
__inline void disable_icache(void) {}
__inline void enable_icache(void) {}


/****************************************************************************
   TODO: Where will the blacklist stuff live?
 ****************************************************************************/
__inline boolean is_ns_range(const void* addr, uint32 len) {return TRUE;}

/****************************************************************************
   TODO: The debug FIQ handling may need to get mostly pushed to the
         kernel and the context would need to be passed from the
         kernel back to the monitor.
 ****************************************************************************/
__inline int register_isr(uint32 intnum,
                          const char *intdesc,
                          void* (*fn)(void*),
                          void* ctx,
                          uint32 flags,
                          boolean enable) {return 0;}

__inline int unregister_isr(uint32 intnum, uint32 flags) {return 0;}

/****************************************************************************
   This will exist entirely in monitor eventually, due to being shared
   between warmboot and power collapse.
 ****************************************************************************/
__inline boolean is_warm_boot_in_process(uint32 cpu) {return FALSE;}

/* Monitor logging */
__inline void tzbsp_log(uint32 pri, const char* fmt, ...) {}

__inline static void loop_here(void)
{
  volatile int aa = 1; while (aa);
}

/****************************************************************************
   Below described in .c file.
 ****************************************************************************/
int mm_map(int va, int pa, int cfg);
void mask_wdt_bark(boolean mask);
void ns_wdt_isr(int arg);


#endif /* TODO_H */
