/**
 * @file tzbsp_mon_call.c
 * @brief Handler for syscalls terminating in the monitor
 */

/*===========================================================================
   Copyright (c) 2013 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/monitor/common/src/mon_call.c#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $

when       who      what, where, why
--------   ---      ------------------------------------
===========================================================================*/
/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include <comdef.h>
#include <string.h>
#include "IxErrno.h"
#include "monitor.h"
#include "smc_secure.h"
#include "default_memory_map.h"
#include "sys_regs.h"
#include "cache.h"
#include "aa64_ctx.h" /* Only here for debugging */
#include "soc.h"
#include "sys_fields.h"
#include "mon_sec_io.h"
#include "mon_init_and_pc_arch.h"
#include "mon_init_and_pc.h"
#include "a53_tlb.h"
#include "a53_cache.h"

typedef struct el1_regs_s
{
  uint32 pc;
  uint32 cpsr;
  uint32 reg[14];
} el1_regs_t;

aa_64_vfp_ctx_t ns_vfp_cxt;
aa_64_vfp_ctx_t s_vfp_cxt;

/* ELF header types */
typedef enum ELF_HEADER_TYPE
{
  ELF_HEADER_UNKNOWN  = 0x00000000,        //Force 32bit
  ELF_HEADER_32_BIT   = 1,
  ELF_HEADER_64_BIT   = 2,
} ELF_HEADER_TYPE;

extern int tzbsp_mmu_map(v_addr_t, p_addr_t, size_t, uint32);
extern int tzbsp_mmu_map_block(const mem_block_t*, unsigned int);

/* This is for debugging only */
static __attribute__((used)) size_t context_size = sizeof(aa_64_ctx_t);
static __attribute__((used)) aa_64_ctx_t* aa_64_ctx_ptr;

/*----------------------------------------------------------------------------
 * Global variables
 * -------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 * Function Declarations and Definitions
 * -------------------------------------------------------------------------*/

static uint32 us_to_ticks_qtimer(uint64 us)
{
  /* QTimers run on 19.2 MHz, which is 19.2 times per us. Use of x10
   * in multiplier/divider allows more fixed point precision. */
  return (192ULL * us) / 10ULL;
}

/**
   Disables caching on this CPU for EL3 until CPU reset
 */
static void disable_mon_caches(void)
{
  disable_el3_icache();
  disable_el3_dcache();
  clean_and_inval_dcache_region(get_el3_sp() - STACK_LEN, STACK_LEN);
}

/**
  @brief Handle monitor call from NS
  @param[in] regs  Pointer to register list x0-x6, lr
  @return 0 if call not handled, non-zero otherwise
 */
el3_reg_t mon_call_from_ns(ns_smc_ctx_t* p_ns_ctx)
{
  uint32 id = p_ns_ctx->x0 & 0x3f00ffff; /* owner ID, svc ID, and func ID */

  /* Return status in x0, results in x1-3 */
  switch(id)
  {
    case 0x02000102:
      /* Power Collapse */
      p_ns_ctx->x0 = enter_pc(p_ns_ctx->x2);
      break;
    case 0x02000305:
      /* L0/L1/L2 cache dump */
      /* r2: 0, handle L1 dump request similar to WDT bark, resulting in L0/L1
       *        cache dump, CPU context dumps and device reset.
       *     1, only dump L1 cache and return to HLOS after the dump is done.
       *     2, only dump L2 cache and return to HLOS after the dump is done.
       *     Any other value for r2 will return -4 (-E_NOT_SUPPORTED).
       */
      switch(p_ns_ctx->x2)
      {
        case 0:
          /* TODO: temporarily removed for 8994 */
          /* tzbsp_mon_cache_dump_wdt_bark(); */
          while(1); /* never return */
        case 1:
          /* TODO: temporarily removed for 8994 */
          /* tzbsp_l1_dump(); */
          p_ns_ctx->x0 = 0;
          break;
        case 2:
          /* TODO: temporarily removed for 8994 */
          /* tzbsp_l2_dump(); */
          p_ns_ctx->x0 = 0;
          break;
        default:
          p_ns_ctx->x0 = (uint32)-1; /* SMC_ERR_SYSCALL_NOT_SUPPORTED */
          break;
      }
      break;
    case 0x02000104:
      /* Krait errata #21 workaround enable - not needed */
      p_ns_ctx->x0 = 0;
      break;
    case 0x02000501:
      /* I/O read cmd */
      p_ns_ctx->x1 = secure_io_read(p_ns_ctx->x2);
      if (p_ns_ctx->x1 == (uint32)-4)
        p_ns_ctx->x0 = (uint32)-2; /* -SMC_ERR_SYSCALL_FAILED */
      else
        p_ns_ctx->x0 = 0; /* SMC_SUCCESS */
      break;
    case 0x02000502:
      /* I/O write cmd */
      p_ns_ctx->x0 = secure_io_write(p_ns_ctx->x2, p_ns_ctx->x3);
      break;
    case 0x02000902:
      /* SPMI bus disable */
      p_ns_ctx->x0 = spmi_bus_disable(p_ns_ctx->x2);
      break;
    default:
      /* Return not handled unless target file handled the call */
      return 0;
  }
  return 1;
}

/* Generates a 64 Bit address */
static uint64 generate_entry_addr(uint32 e_entry_l, uint32 e_entry_h)
{
  return ((uint64)e_entry_h) << 32 | (e_entry_l);
}//generate_entry_addr()

/*
   Parameter names come from ELF header standard
 */
static void mon_handle_milestone(uint32 e_entry_l, uint32 e_entry_h,
                                 el3_reg_t ei_class)
{
  uint64 e_entry = generate_entry_addr(e_entry_l, e_entry_h);

  /* Clearing the NS system registers will set the NS SCTLR[M] to
     zero, disabling the MMU. */
  clear_el1_ns_ctx();

  /* Check for thumb entry  */
  if (ELF_HEADER_32_BIT == ei_class && (e_entry & 1))
  {
    set_ns_cpsr(SPSR_32_MODE_SVC | SPSR_32_M_BIT |
                SPSR_32_I_BIT | SPSR_32_T_BIT);
    set_hcr_el2(get_hcr_el2() & ~(HCR_RW_BIT));
  }
  /* Check for ARM32 entry */
  else if (ELF_HEADER_32_BIT == ei_class)
  {
    set_ns_cpsr(SPSR_32_MODE_SVC | SPSR_32_M_BIT | SPSR_32_I_BIT);
    set_hcr_el2(get_hcr_el2() & ~(HCR_RW_BIT));
  }
  /* ELF64 */
  else if (ELF_HEADER_64_BIT == ei_class)
  {
    set_ns_cpsr(SPSR_MODE_EL1h | SPSR_I_BIT);
    set_hcr_el2(get_hcr_el2() | HCR_RW_BIT);
  }
  else
  {
    while (1);
  }

  set_ns_el3_elr(e_entry);
} /* mon_handle_milestone() */

#define MAX_NS_PARAM_REGS        9
static void set_ns_register_bank(el3_reg_t* p_x0, uint32 num_regs)
{
  el3_reg_t* ns_reg_ctx = (el3_reg_t *) get_ns_context_addr();
  el3_reg_t* input_ctx = p_x0;
  int32 cnt = 0;

  if (num_regs > MAX_NS_PARAM_REGS)
  {
    while (1);
  }

  while (num_regs != 0)
  {
    *ns_reg_ctx++ = *input_ctx++;
    num_regs--;
  }
} /* set_ns_register_bank() */

/**
  @brief Do mode switch for EL1 to A64
  @param[in] regs  Pointer to register list x0-x8, el1_elr
 */
void mon_do_mode_switch(hlos_boot_params_t* hlos_boot_info, el3_reg_t ei_class)
{
  /* ELF64 */
  if (ELF_HEADER_64_BIT != ei_class)
  {
    while (1);
  }

  /* Clearing the NS system registers will set the NS SCTLR[M] to
     zero, disabling the MMU. */
  clear_el1_ns_ctx();

  set_ns_cpsr(SPSR_D_BIT | SPSR_I_BIT | SPSR_MODE_EL1h);
  set_hcr_el2(get_hcr_el2() | HCR_RW_BIT);

  set_ns_el3_elr(hlos_boot_info->el1_elr);
  set_ns_register_bank(&(hlos_boot_info->el1_x0), 9);
} /* mon_do_mode_switch() */


static void mon_config_hyp_entry(uint32 e_entry_l,
                                 uint32 e_entry_h,
                                 el3_reg_t ei_class)
{
  uint64 e_entry = generate_entry_addr(e_entry_l, e_entry_h);

  /* ELF64 */
  if (ELF_HEADER_64_BIT != ei_class)
  {
    while (1);
  }

  set_ns_cpsr(SPSR_MODE_EL2h | SPSR_I_BIT);
  set_hcr_el2(get_hcr_el2() | HCR_RW_BIT);

  set_ns_el3_elr(e_entry);
}//config_hyp_entry()

static void set_debug_sys_regs(uint32 debug)
{
  /* Default is to boot up with the assumption that debug is disabled,
     so there doesn't need to be an else statement. */
  if (debug)
  {
    /* SDER32[SUIDEN] = 1, SDER32[SUNIDEN] = 1 */
    set_sder32_el3(SDER32_EL3_SUIDEN_BIT | SDER32_EL3_SUNIDEN_BIT);

    /* MDCR[TPM]   = 0 MDCR[TDA]   = 0 MDCR[TDOSA] = 0 MDCR[SPD32] = 3
       MDCR[SDD]   = 0 MDCR[SPME]  = 1 MDCR[EDAD]  = 0 MDCR[EPMAD] = 0
    */
    set_mdcr_el3((MDCR_EL3_SPD32_SPDE << MDCR_EL3_SPD32_SHFT) |
                 MDCR_EL3_SPME_BIT);
  }
}


void tzbsp_set_cpu_timer(uint64 val)
{
  set_cntps_ctl_el1(val);
}


uint64 tzbsp_get_cpu_timer_count(void)
{
  return get_cntpct_el0();
}


void tzbsp_oneshot_start_cpu_timer(uint64 timeout)
{
  uint64 val;

  set_cntps_ctl_el1(0);
  set_cntps_cval_el1(get_cntpct_el0() + us_to_ticks_qtimer(timeout));
  set_cntps_ctl_el1(1);
}

void mon_dump_context(uint32 ctx_buf)
{
  uint32 cpu = cur_cpu_num();
  cpu_dbg_info_t *pbuffer = (cpu_dbg_info_t *)((uint64)ctx_buf);
  byte *src  = NULL;
  byte *dest = NULL;

  /* TODO: */
  /* Cache dumping only happens in error conditions.  Disable all
     caching to help prevent losing state. */
  /* disable_mon_caches(); */

  dest = (byte *)(&(pbuffer->aa_64_ctx_all_els.aa_64_ctx));

  /* get_el3_sp() returns the base of CURRENT CORE'S SP which is where
     context is stored. -CTX_SZ is required since the context is
     stored like the struct define. */
  src = ((byte *) get_el3_sp()) - CTX_SZ;
  memcpy(dest, src, CTX_SZ);

  //Store EL2 registers
  pbuffer->aa_64_ctx_all_els.sp_el2 = get_sp_el2();
  pbuffer->aa_64_ctx_all_els.elr_el2 = get_elr_el2();
  pbuffer->aa_64_ctx_all_els.spsr_el2 = get_spsr_el2();

  pbuffer->cpu_power_info.cpu_pc_state = mon_get_cpu_pc_state(cpu);
  pbuffer->cpu_power_info.has_cpu_cold_booted =
    mon_get_has_cpu_cold_booted(cpu);

  /* EL3 must fill in the NS state for the dump, since EL1 does not
     know if a watchdog FIQ interrupted secure or non-secure.  */
  if (SCR_NS_BIT == get_ns_state())
  {
    /* Magic number defined in tzbsp_memory_dump.h */
    pbuffer->sc_status |= 1;
  }
}//mon_dump_context()


void mon_save_ns_vfp_context(int VfpStatusFlag)
{
   vfp_handler_func((void *)&ns_vfp_cxt, VfpStatusFlag);
   ns_vfp_cxt.SavedStatusReg=1;
}//mon_save_ns_vfp_context()

/**
  @brief Handle monitor call from Secure World
  @param[in] regs  Pointer to register list R0-R12
  @return 0 if returning to Secure, else NS
 */
el3_reg_t mon_call_from_secure(s_smc_ctx_t* p_s_ctx)
{
  uint32 id = p_s_ctx->x0.v;
  el1_regs_t* el1_regs_ptr;
  uint32 returnId = SMC_RETURN_TO_SECURE;

  switch(id)
  {
    case SMC_CMD_RETURN_TO_NS:
    {
      /* TODO: secure EL1 doesn't get all the registers implied by
               this type. */
      el1_regs_ptr = (el1_regs_t*) ((uint64) p_s_ctx->x1.v);
      set_ns_return_registers(el1_regs_ptr->reg[0],
                              el1_regs_ptr->reg[1],
                              el1_regs_ptr->reg[2],
                              el1_regs_ptr->reg[3]);

      returnId =  SMC_RETURN_TO_NS;
      break;
    }

    case SMC_CMD_RETURN_TO_NS_NO_REGS_MODIFIED:
    {
      returnId =  SMC_RETURN_TO_NS;
      break;
    }

    case SMC_CMD_MMU_MAP:
    {
      p_s_ctx->x0.v = tzbsp_mmu_map((v_addr_t) p_s_ctx->x1.v,
                                    (p_addr_t) p_s_ctx->x2.v,
                                    (size_t) p_s_ctx->x3.v,
                                    (uint32) p_s_ctx->x4.v);
      break;
    }

    case SMC_CMD_MMU_MAP_BLOCK:
    {
      p_s_ctx->x0.v = tzbsp_mmu_map_block((void*) (uint64) p_s_ctx->x1.v, p_s_ctx->x2.v);
      break;
    }

    case SMC_CMD_GET_EL1_TT_MEM_INFO:
    {
      p_s_ctx->x0.v = E_SUCCESS;
      p_s_ctx->x1.v = (uint32) get_el1_tt_base_addr();
      p_s_ctx->x2.v = (uint32) get_el1_tt_len();
      break;
    }

    case SMC_CMD_GET_EL3_TT_MEM_INFO:
    {
      p_s_ctx->x0.v = E_SUCCESS;
      p_s_ctx->x1.v = (uint32) get_mon_tt_base_addr();
      p_s_ctx->x2.v = (uint32) get_mon_tt_len();
      break;
    }

    case SMC_CONFIG_HYP_ENTRY:
    {
      mon_config_hyp_entry(p_s_ctx->x1.v, p_s_ctx->x2.v, p_s_ctx->x3.v);
      p_s_ctx->x0.v = E_SUCCESS;
      break;
    }

    case SMC_CMD_MILESTONE_SET:
    {
      mon_handle_milestone(p_s_ctx->x1.v, p_s_ctx->x2.v, p_s_ctx->x3.v);
      p_s_ctx->x0.v = E_SUCCESS;
      break;
    }

    case SMC_CMD_SET_BOOT_LOCK_ADDR:
    {
      mon_set_boot_lock_arch(p_s_ctx->x1.v);
      p_s_ctx->x0.v = E_SUCCESS;
      break;
    }

    case SMC_CMD_DO_MODE_SWITCH:
    {
      mon_do_mode_switch((hlos_boot_params_t*)((uint64)p_s_ctx->x1.v),
                         ELF_HEADER_64_BIT);
      p_s_ctx->x0.v = E_SUCCESS;
      break;
    }

    case SMC_CMD_GET_NS_W30:
    {
      /* Explicitly reduce precision of return to 32 bits, since the
         kernel image is AArch32. */
      p_s_ctx->x0.v = (uint32) get_ns_el1_x30();
      break;
    }

    case SMC_CMD_SET_SMP_NOTIFIER:
    {
      set_secure_os_smp_notifier(p_s_ctx->x1.v);
      break;
    }

    case SMC_CMD_DUMP_CONTEXT:
    {
      mon_dump_context(p_s_ctx->x1.v);
      break;
    }

    case SMC_CMD_DUMP_VFP_CONTEXT:
    {
      mon_save_ns_vfp_context(p_s_ctx->x1.v);
      break;
    }

    case SMC_CMD_SET_SYSDBG_ENTRY_AND_BUF:
    {
      set_sysdbg_entry_and_buf(p_s_ctx->x1.v, p_s_ctx->x2.v);
      break;
    }

    case SMC_CMD_SET_TIMER_FREQ:
    {
      set_cntfrq_el0(p_s_ctx->x1.v);
      p_s_ctx->x0.v = E_SUCCESS;
      break;
    }

    case SMC_CMD_SET_DEBUG:
    {
      set_debug_sys_regs(p_s_ctx->x1.v);
      break;
    }

	case SMC_CMD_ONESHOT_START_CPU_TIMER:
	{
	  tzbsp_oneshot_start_cpu_timer(p_s_ctx->x1.v);
	  break;
	}

	case SMC_CMD_SET_CPU_TIMER:
	{
	  tzbsp_set_cpu_timer(p_s_ctx->x1.v);
	  break;
	}

	case SMC_CMD_GET_CPU_TIMER_COUNT:
	{
	  p_s_ctx->x0.v = tzbsp_get_cpu_timer_count();
	  break;
	}

  case SMC_CMD_GET_A53_TLB_DUMP_SZ:
  {
    /* TODO: Cache dumping only happens in error conditions.  Disable
       all caching to help prevent losing state. */
    /* disable_mon_caches(); */
    p_s_ctx->x0.v = a53_get_tlb_dump_len();
    break;
  }

  case SMC_CMD_DUMP_A53_TLB:
  {
    /* TODO: Cache dumping only happens in error conditions.  Disable
       all caching to help prevent losing state. */
    /* disable_mon_caches(); */
    a53_dump_tlb((void*) (uint64) p_s_ctx->x1.v);
    break;
  }

  case SMC_CMD_GET_A53_ICACHE_DUMP_SZ:
  {
    /* TODO: Cache dumping only happens in error conditions.  Disable all
       caching to help prevent losing state. */
    /* disable_mon_caches(); */
    p_s_ctx->x0.v = a53_get_cache_dump_len(A53_L1_I_CACHE);
    break;
  }

  case SMC_CMD_DUMP_A53_ICACHE:
  {
    /* TODO: Cache dumping only happens in error conditions.  Disable
       all caching to help prevent losing state. */
    /* disable_mon_caches(); */
    a53_dump_cache(A53_L1_I_CACHE, (void*) (uint64) p_s_ctx->x1.v);
    break;
  }

  case SMC_CMD_GET_A53_DCACHE_DUMP_SZ:
  {
    /* TODO: Cache dumping only happens in error conditions.  Disable
       all caching to help prevent losing state. */
    /* disable_mon_caches(); */
    p_s_ctx->x0.v = a53_get_cache_dump_len(A53_L1_D_CACHE);
    break;
  }

  case SMC_CMD_DUMP_A53_DCACHE:
  {
    /* TODO: Cache dumping only happens in error conditions.  Disable
       all caching to help prevent losing state. */
    /* disable_mon_caches(); */
    a53_dump_cache(A53_L1_D_CACHE, (void*) (uint64) p_s_ctx->x1.v);
    break;
  }

  case SMC_CMD_SET_EXT_OS_CONFIG:
  {
    /* Set the is_ext_os_config variable to a non-zero value (true) 
     * This is to be used when ext os is configured in QSEE */
    set_tzbsp_is_ext_os_config();
    break;
  }

  case SMC_CMD_GET_MON_CODE_INFO:
  {
  	p_s_ctx->x0.v = E_SUCCESS;
    p_s_ctx->x1.v = (uint32) get_mon_code_base_addr();
    p_s_ctx->x2.v = (uint32) get_mon_code_len();
    break;
  }

  case SMC_CMD_SET_SDI_WA_FLAG:
  {  
    mon_set_sdi_wa(p_s_ctx->x1.v);
    p_s_ctx->x0.v = E_SUCCESS;
    break;
  }
  
  default:
  {
    p_s_ctx->x0.v = (uint32) -1;
    while(1);
  }
  }

  return returnId;
}//mon_call_from_secure()
