;============================================================================
;
; Monitor CPU Reset Entry Point
;
; Copyright 2014 by Qualcomm Technologies, Incorporated. All Rights Reserved
;============================================================================
;============================================================================
;
; EDIT HISTORY FOR MODULE
;
; $Header:
; $DateTime: 2018/02/07 00:37:16 $
; $Author: mplp4svc $
;
; This section contains comments describing changes made to the module.
; Notice that changes are listed in reverse chronological order.
;
;
; when who what, where, why
; -------- --- ---------------------------------------------------------
; 03/11/14 pre Initial revision.
;============================================================================
   MACRO
   CurCPUNum $rx, $ry
      mrs $rx, MPIDR_EL1
      and $ry, $rx, #0x0000FF00
      lsr $ry, #0x8
      mov $rx, #4
      mul $ry, $rx
      mrs $rx, MPIDR_EL1
      and $rx, #0x000000FF
      add $rx, $ry
   MEND
   MACRO
   PerClusterGdhsState $rx, $ry, $rz
     ldr $rz, =l2_gdhs_state;
     add $rz, $rz, $rx, lsl #0x2
     ldr $rx, [$ry]
     MEND
   MACRO
   ClearPerClusterGdhs $rx, $ry, $rz
     ldr $rz, =l2_gdhs_state;
     add $rz, $rz, $rx, lsl #0x2
     mov $rx, #0x0
     str $rx, [$ry]
     MEND
    MACRO
    SetSecure $rx





        mov $rx, xzr
        msr SCR_EL3, $rx
        isb
    MEND
    MACRO
    SetNonSecure $rx





        mov $rx, #0x020:OR:0x001:OR:0x004:OR:0x400:OR:0x100
        msr SCR_EL3, $rx
        isb
    MEND
    MACRO
    PushTwo $x0, $x1
        stp $x1, $x0, [sp, #-0x10]!
    MEND
    MACRO
    PopTwo $x0, $x1
        ldp $x0, $x1, [sp], #0x10
    MEND
    MACRO
    PushAArch32NPRegs
        PushTwo x5, x6
        PushTwo x7, x8
        PushTwo x9, x10
        PushTwo x11, x12
        PushTwo x13, x14
        PushTwo x15, x16
        PushTwo x17, x18
        PushTwo x19, x20
        PushTwo x21, x22
        PushTwo x23, x24
        PushTwo x25, x26
        PushTwo x27, x28
        PushTwo x29, x30
    MEND
    MACRO
    PopAArch32NPRegs
        PopTwo x30, x29
        PopTwo x28, x27
        PopTwo x26, x25
        PopTwo x24, x23
        PopTwo x22, x21
        PopTwo x20, x19
        PopTwo x18, x17
        PopTwo x16, x15
        PopTwo x14, x13
        PopTwo x12, x11
        PopTwo x10, x9
        PopTwo x8, x7
        PopTwo x6, x5
    MEND
    MACRO
    PushAArch64CSRegs
        PushTwo x19, x20
        PushTwo x21, x22
        PushTwo x23, x24
        PushTwo x25, x26
        PushTwo x27, x28
        PushTwo x29, x30
    MEND
    MACRO
    PopAArch64CSRegs
        PopTwo x30, x29
        PopTwo x28, x27
        PopTwo x26, x25
        PopTwo x24, x23
        PopTwo x22, x21
        PopTwo x20, x19
    MEND
    MACRO
    PushAArch64PSRegs
        PushTwo x19, x18
        PushTwo x17, x16
        PushTwo x15, x14
        PushTwo x13, x12
        PushTwo x11, x10
        PushTwo x9, x8
        PushTwo x7, x6
        PushTwo x5, x4
        PushTwo x3, x2
        PushTwo x1, x0
    MEND
    MACRO
    PopAArch64PSRegs
        PopTwo x0, x1
        PopTwo x2, x3
        PopTwo x4, x5
        PopTwo x6, x7
        PopTwo x8, x9
        PopTwo x10, x11
        PopTwo x12, x13
        PopTwo x14, x15
        PopTwo x16, x17
        PopTwo x18, x19
    MEND
    MACRO
    WipeParamRegs
        mov x0, #0
        mov x1, #0
        mov x2, #0
        mov x3, #0
        mov x4, #0
        mov x5, #0
        mov x6, #0
        mov x7, #0
        mov x8, #0
    MEND
    MACRO
    WipeScratchRegs
        mov x9, #0
        mov x10, #0
        mov x11, #0
        mov x12, #0
        mov x13, #0
        mov x14, #0
        mov x15, #0
        mov x16, #0
        mov x17, #0
        mov x18, #0
    MEND
    MACRO
    WipeAllGPRegs
        WipeParamRegs
        WipeScratchRegs
        mov x19, #0
        mov x20, #0
        mov x21, #0
        mov x22, #0
        mov x23, #0
        mov x24, #0
        mov x25, #0
        mov x26, #0
        mov x27, #0
        mov x28, #0
        mov x29, #0
        mov x30, #0
    MEND
    MACRO
    CorruptMonRegs
        mov x0, #0xA
        mov x1, #0xB
        mov x2, #0xC
        mov x3, #0xD
        mov x4, #0xE
        mov x5, #0xF
        mov x6, #0xA
        mov x7, #0xB
        mov x8, #0xC
        mov x9, #0xD
        mov x10, #0xE
        mov x11, #0xF
        mov x12, #0xA
        mov x13, #0xB
        mov x14, #0xC
        mov x15, #0xD
        mov x16, #0xE
        mov x17, #0xF
        mov x18, #0xA
        mov x19, #0xB
        mov x20, #0xC
        mov x21, #0xD
        mov x22, #0xE
        mov x23, #0xF
        mov x24, #0xA
        mov x25, #0xB
        mov x26, #0xC
        mov x27, #0xD
        mov x28, #0xE
        mov x29, #0xF
        mov x30, #0xA
    MEND
  MACRO
  SpinlockObtain $lock_addr, $cpu, $work
    mrs $cpu, MPIDR_EL1
    and $cpu, $cpu, #0xFF
    add $cpu, #0x1
42 ldaxr $work, [$lock_addr]
    cbnz $work, %b42
    stlxr $work, $cpu, [$lock_addr]
    cbnz $work, %b42
    dmb sy
  MEND
  MACRO
  SpinlockRelease $lock_addr
    ; Ensure all reads/writes are visible to other observers before
    ; releasing the lock.
    dmb sy
    str xzr, [$lock_addr] ; Clear the lock
    dmb sy
  MEND
    MACRO
    CurCPUNumber $rx, $ry, $cpus_per_cluster
       mrs $rx, MPIDR_EL1
       and $ry, $rx, #0x0000FF00
       lsr $ry, #0x8
       mov $rx, #$cpus_per_cluster
       mul $ry, $rx
       mrs $rx, MPIDR_EL1
       and $rx, #0x000000FF
       add $rx, $ry
    MEND
    MACRO
    CurClusterNum $rx
       mrs $rx, MPIDR_EL1
       and $rx, #0x0000FF00
       lsr $rx, #0x8
    MEND
; -------------------
; MACRO: PushTwoA
; -------------------
; Like push, but memory addresses ascend instead of descend
    MACRO
    PushTwoA $x0, $x1, $x2
        stp $x1, $x2, [$x0], #0x10
    MEND
; -------------------
; MACRO: PopTwoA
; -------------------
    MACRO
    PopTwoA $x0, $x1, $x2
        ldp $x2, $x1, [$x0, #-0x10]!
    MEND
;
; TODO: CPACR_EL1 - for access to floating point and Advanced SIMD
; execution
;
; ----------------
; MACRO: SaveCtx
; ----------------
; $sp should be a pointer to the base address of a context structure.
; See aa_64_ctx_t in aa64_ctx.h
    MACRO
    SaveCtx $sp
        PushTwoA $sp, x0, x1
        PushTwoA $sp, x2, x3
        PushTwoA $sp, x4, x5
        PushTwoA $sp, x6, x7
        PushTwoA $sp, x8, x9
        PushTwoA $sp, x10, x11
        PushTwoA $sp, x12, x13
        PushTwoA $sp, x14, x15
        PushTwoA $sp, x16, x17
        PushTwoA $sp, x18, x19
        PushTwoA $sp, x20, x21
        PushTwoA $sp, x22, x23
        PushTwoA $sp, x24, x25
        PushTwoA $sp, x26, x27
        PushTwoA $sp, x28, x29

        mrs x0, SPSR_EL1
        PushTwoA $sp, x30, x0

        mrs x0, ELR_EL1
        mrs x1, SPSR_irq
        PushTwoA $sp, x0, x1

        mrs x0, SPSR_abt
        mrs x1, SPSR_und
        PushTwoA $sp, x0, x1

        mrs x0, SPSR_fiq
        mrs x1, SP_EL1
        PushTwoA $sp, x0, x1

        mrs x0, SP_EL0
        mrs x1, CNTP_TVAL_EL0
        PushTwoA $sp, x0, x1

        mrs x0, CNTP_CVAL_EL0
        mrs x1, CNTP_CTL_EL0
        PushTwoA $sp, x0, x1

        mrs x0, VBAR_EL1
        mrs x1, TTBR1_EL1
        PushTwoA $sp, x0, x1

        mrs x0, TTBR0_EL1
        mrs x1, TCR_EL1
        PushTwoA $sp, x0, x1

        mrs x0, TPIDR_EL0
        mrs x1, TPIDRRO_EL0
        PushTwoA $sp, x0, x1

        mrs x0, TPIDR_EL1
        mrs x1, SCTLR_EL1
        PushTwoA $sp, x0, x1

        mrs x0, PAR_EL1
        mrs x1, MAIR_EL1
        PushTwoA $sp, x0, x1

        mrs x0, ESR_EL1
        mrs x1, FAR_EL1
        PushTwoA $sp, x0, x1

        mrs x0, CSSELR_EL1
        mrs x1, CONTEXTIDR_EL1
        PushTwoA $sp, x0, x1

        mrs x0, AMAIR_EL1
        mrs x1, AFSR1_EL1
        PushTwoA $sp, x0, x1

        mrs x0, AFSR0_EL1
        mrs x1, ACTLR_EL1
        PushTwoA $sp, x0, x1

        mrs x0, CNTKCTL_EL1
        mrs x1, FPEXC32_EL2
        PushTwoA $sp, x0, x1

        mrs x0, DACR32_EL2
        mrs x1, IFSR32_EL2
        PushTwoA $sp, x0, x1

        mrs x0, CPACR_El1
        ldr x1, =0xAABBAABB
        PushTwoA $sp, x0, x1

        mrs x0, SPSR_EL3
        mrs x1, ELR_EL3
        PushTwoA $sp, x0, x1
    MEND
; ----------------
; MACRO: LoadCtx
; ----------------
; $sp should be a pointer to the base address of a context structure.
; See aa_64_ctx_t in aa64_ctx.h. The order of removing items from the
; structure is important to properly restore x0, x1.
    MACRO
    LoadCtx $sp
        add $sp, #((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8)

        PopTwoA $sp, x0, x1
        msr ELR_EL3, x0
        msr SPSR_EL3, x1

        PopTwoA $sp, x0, x1
        ; dummy val in x0
        msr CPACR_El1, x1

        PopTwoA $sp, x0, x1
        msr IFSR32_EL2, x0
        msr DACR32_EL2, x1

        PopTwoA $sp, x0, x1
        msr FPEXC32_EL2, x0
        msr CNTKCTL_EL1, x1

        PopTwoA $sp, x0, x1
        msr ACTLR_EL1, x0
        msr AFSR0_EL1, x1

        PopTwoA $sp, x0, x1
        msr AFSR1_EL1, x0
        msr AMAIR_EL1, x1

        PopTwoA $sp, x0, x1
        msr CONTEXTIDR_EL1, x0
        msr CSSELR_EL1, x1

        PopTwoA $sp, x0, x1
        msr FAR_EL1, x0
        msr ESR_EL1, x1

        PopTwoA $sp, x0, x1
        msr MAIR_EL1, x0
        msr PAR_EL1, x1

        PopTwoA $sp, x0, x1
        msr SCTLR_EL1, x0
        msr TPIDR_EL1, x1

        PopTwoA $sp, x0, x1
        msr TPIDRRO_EL0, x0
        msr TPIDR_EL0, x1

        PopTwoA $sp, x0, x1
        msr TCR_EL1, x0
        msr TTBR0_EL1, x1

        PopTwoA $sp, x0, x1
        msr TTBR1_EL1, x0
        msr VBAR_EL1, x1

        PopTwoA $sp, x0, x1
        msr CNTP_CTL_EL0, x0
        msr CNTP_CVAL_EL0, x1

        PopTwoA $sp, x0, x1
        msr CNTP_TVAL_EL0, x0
        msr SP_EL0, x1

        PopTwoA $sp, x0, x1
        msr SP_EL1, x0
        msr SPSR_fiq, x1

        PopTwoA $sp, x0, x1
        msr SPSR_und, x0
        msr SPSR_abt, x1

        PopTwoA $sp, x0, x1
        msr SPSR_irq, x0
        msr ELR_EL1, x1

        PopTwoA $sp, x1, x30
        msr SPSR_EL1, x1

        PopTwoA $sp, x29, x28
        PopTwoA $sp, x27, x26
        PopTwoA $sp, x25, x24
        PopTwoA $sp, x23, x22
        PopTwoA $sp, x21, x20
        PopTwoA $sp, x19, x18
        PopTwoA $sp, x17, x16
        PopTwoA $sp, x15, x14
        PopTwoA $sp, x13, x12
        PopTwoA $sp, x11, x10
        PopTwoA $sp, x9, x8
        PopTwoA $sp, x7, x6
        PopTwoA $sp, x5, x4
        PopTwoA $sp, x3, x2
        PopTwoA $sp, x1, x0

        add $sp, #((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8)
    MEND
; ---------------------
; MACRO: LoadSysRegsCtx
; ---------------------
; Loads registers from the context in a given register
; Note: we do not need to go in a specific order, since there is no
; concern for corrupting GP registers, but it's good to maintain the
; same order as savectx and loadctx so the operations can be compared,
; and those macros go in a specific order due to GP register
; corruption concern.
    MACRO
    LoadSysRegsCtx $x0, $x1, $x2
        add $x0, #((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) - (((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8))

        PopTwoA $x0, $x1, $x2
        msr SPSR_EL3, $x1
        ; dummy val in $x2

        PopTwoA $x0, $x1, $x2
        msr CPACR_El1, $x1
        msr IFSR32_EL2, $x2

        PopTwoA $x0, $x1, $x2
        msr DACR32_EL2, $x1
        msr FPEXC32_EL2, $x2

        PopTwoA $x0, $x1, $x2
        msr CNTKCTL_EL1, $x1
        msr ACTLR_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr AFSR0_EL1, $x1
        msr AFSR1_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr AMAIR_EL1, $x1
        msr CONTEXTIDR_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr CSSELR_EL1, $x1
        msr FAR_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr ESR_EL1, $x1
        msr MAIR_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr PAR_EL1, $x1
        msr SCTLR_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr TPIDR_EL1, $x1
        msr TPIDRRO_EL0, $x2

        PopTwoA $x0, $x1, $x2
        msr TPIDR_EL0, $x1
        msr TCR_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr TTBR0_EL1, $x1
        msr TTBR1_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr VBAR_EL1, $x1
        msr CNTP_CTL_EL0, $x2

        PopTwoA $x0, $x1, $x2
        msr CNTP_CVAL_EL0, $x1
        msr CNTP_TVAL_EL0, $x2

        PopTwoA $x0, $x1, $x2
        msr SP_EL0, $x1
        msr SP_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr SPSR_fiq, $x1
        msr SPSR_und, $x2

        PopTwoA $x0, $x1, $x2
        msr SPSR_abt, $x1
        msr SPSR_irq, $x2

        PopTwoA $x0, $x1, $x2
        msr ELR_EL1, $x1
        msr SPSR_EL1, $x2

        add $x0, #((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) - (((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8))
    MEND
; ---------------------
; MACRO: SaveSysRegsCtx
; ---------------------
; Saves registers from the context in a given register
    MACRO
    SaveSysRegsCtx $x0, $x1, $x2
        sub $x0, #((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) - (((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8))

        mrs $x1, SPSR_EL1
        mrs $x2, ELR_EL1
        PushTwoA $x0, $x1, $x2

        mrs $x1, SPSR_irq
        mrs $x2, SPSR_abt
        PushTwoA $x0, $x1, $x2

        mrs $x1, SPSR_und
        mrs $x2, SPSR_fiq
        PushTwoA $x0, $x1, $x2

        mrs $x1, SP_EL1
        mrs $x2, SP_EL0
        PushTwoA $x0, $x1, $x2

        mrs $x1, CNTP_TVAL_EL0
        mrs $x2, CNTP_CVAL_EL0
        PushTwoA $x0, $x1, $x2

        mrs $x1, CNTP_CTL_EL0
        mrs $x2, VBAR_EL1
        PushTwoA $x0, $x1, $x2

        mrs $x1, TTBR1_EL1
        mrs $x2, TTBR0_EL1
        PushTwoA $x0, $x1, $x2

        mrs $x1, TCR_EL1
        mrs $x2, TPIDR_EL0
        PushTwoA $x0, $x1, $x2

        mrs $x1, TPIDRRO_EL0
        mrs $x2, TPIDR_EL1
        PushTwoA $x0, $x1, $x2

        mrs $x1, SCTLR_EL1
        mrs $x2, PAR_EL1
        PushTwoA $x0, $x1, $x2

        mrs $x1, MAIR_EL1
        mrs $x2, ESR_EL1
        PushTwoA $x0, $x1, $x2

        mrs $x1, FAR_EL1
        mrs $x2, CSSELR_EL1
        PushTwoA $x0, $x1, $x2

        mrs $x1, CONTEXTIDR_EL1
        mrs $x2, AMAIR_EL1
        PushTwoA $x0, $x1, $x2

        mrs $x1, AFSR1_EL1
        mrs $x2, AFSR0_EL1
        PushTwoA $x0, $x1, $x2

        mrs $x1, ACTLR_EL1
        mrs $x2, CNTKCTL_EL1
        PushTwoA $x0, $x1, $x2

        mrs $x1, FPEXC32_EL2
        mrs $x2, DACR32_EL2
        PushTwoA $x0, $x1, $x2

        mrs $x1, IFSR32_EL2
        mrs $x2, CPACR_El1
        PushTwoA $x0, $x1, $x2

        ldr $x1, =0xAABBAABB
        mrs $x2, SPSR_EL3
        PushTwoA $x0, $x1, $x2

        sub $x0, #((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) - (((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8))
    MEND
; ----------------
; MACRO: MigrateSysregsFromEL3ToEL1
; ----------------
; Takes EL3 system registers and writes to EL1 counterparts
    MACRO
    MigrateSysregsFromEL3ToEL1 $x0, $x1
        mrs $x0, SPSR_EL3
        msr SPSR_EL1, $x0

        mrs $x0, ELR_EL3
        msr ELR_EL1, $x0

        mrs $x0, VBAR_EL3
        msr VBAR_EL1, $x0

        mrs $x0, TTBR0_EL3
        msr TTBR0_EL1, $x0

        mrs $x0, TCR_EL3
        ldr $x1, =(0x0000003F :OR: 0x00000300 :OR: 0x00000C00 :OR: 0x00003000 :OR: 0x0000C000 :OR: 0x00070000 :OR: 0x00100000)



        and $x0, $x1 ; Mask DC vals

        ubfx $x1, $x0, #20, #(0x00100000 >> 20)

                                        ; Extract TBI and put in $x1
        lsl $x1, #37 ; Move TCR_EL3[TBI] to TCR_EL1[TBI0]
        orr $x0, $x1 ; Apply TCR_EL3[TBI] to $x0
        ldr $x1, =0x00100000
        bic $x0, $x1 ; Clear TCR_EL3[TBI] from $x0

        ubfx $x1, $x0, #16, #(0x00070000 >> 16)

                                        ; Extract PS and put in $x1
        lsl $x1, #32 ; Move TCR_EL3[PS] to TCR_EL1[IPS]
        orr $x0, $x1 ; Apply TCR_EL3[PS] to $x0
        ldr $x1, =0x00070000
        bic $x0, $x1 ; Clear TCR_EL3[PS] from $x0

        msr TCR_EL1, $x0

        mrs $x0, TPIDR_EL3
        msr TPIDR_EL1, $x0

        mrs $x0, SCTLR_EL3
        ldr $x1, =(0x00000001 :OR: 0x00000002 :OR: 0x00000004 :OR: 0x00000008 :OR: 0x00001000 :OR: 0x00080000 :OR: 0x02000000)



        and $x0, $x1 ; Mask DC vals
        msr SCTLR_EL1, $x0

        ; no EL3 for PAR

        mrs $x0, MAIR_EL3
        msr MAIR_EL1, $x0

        mrs $x0, ESR_EL3
        msr ESR_EL1, $x0

        mrs $x0, FAR_EL3
        msr FAR_EL1, $x0

        ; no EL3 for CSSELR
        ; no EL3 for CONTEXTIDR

        mrs $x0, AMAIR_EL3
        msr AMAIR_EL1, $x0

        mrs $x0, AFSR1_EL3
        msr AFSR1_EL1, $x0

        mrs $x0, AFSR0_EL3
        msr AFSR0_EL1, $x0

        mrs $x0, ACTLR_EL3 ; TODO: implementation defined
        msr ACTLR_EL1, $x0

        mrs $x0, SPSel
        cmp $x0, #0 ; 0 indicates t, else h
        mov $x0, sp ; $x0 = SBL SP
        beq %f1
        msr SP_EL1, $x0 ; SBL uses sp_el1
        mov $x1, #0x05 ; $x1 = SPSR[M] = SPh
        b %f2
1 msr SP_EL0, $x0 ; SBL uses sp_el0
        mov $x1, #0x04 ; $x1 = SPSR[M] = SPt
2 mrs $x0, DAIF ; $x0 = SPSR[DAIF]
        orr $x0, $x1 ; Combine M with DAIF
        msr SPSR_EL3, $x0 ; set SPSR[MDAIF]
                                       ; TODO: other bits for AArch32 return

    MEND
    GET monitor_macros.inc
    IMPORT get_el3_vector_addr
    IMPORT get_el3_sp
    IMPORT cpu_init
    IMPORT mon_l1
    IMPORT mair
    IMPORT tcr
    IMPORT warm_init_and_enable_el3_mmu
    IMPORT get_secure_os_entry_addr_arch
    IMPORT memset
    IMPORT ns_state
    IMPORT mon_post_cci_reset_wa_arch
    EXPORT cpu_init_asm
; TODO: this is defined in two places
DEFAULT_AARCH32_S_EL1_SPSR EQU 0x003:OR:0x080:OR:0x040:OR:0x010
    ; ------------------------------------------------------------------
    ; Initial CPU reset code is arch-specific. Check
    ; mon_cpu_reset_arch.s for details.
    ; ------------------------------------------------------------------
    PRESERVE8
    AREA CPU_RESET_COMMON, CODE, READONLY, ALIGN=3
cpu_init_asm
    ; ------------------------------------------------------------------
    ; Program EL3 vector table. Should happen as early as corruption
    ; of vbar is possible in order to catch any oopsies.
    ; ------------------------------------------------------------------
    bl get_el3_vector_addr
    msr VBAR_EL3, x0
    ; ------------------------------------------------------------------
    ; Mask FIQ and IRQs. Monitor should never be interruptible.
    ; ------------------------------------------------------------------
    mov x0, #0x080:OR:0x040
    msr DAIF, x0
    ;-------------------------------------------------------------------
    ; Set SPSR to return to correct state for NS.
    ;
    ; TODO: This isnt needed unless SBL comes to monitor without using
    ; an exception
    ;-------------------------------------------------------------------
    mov x0, #0x05:OR:0x80
    msr SPSR_EL3, x0
    ; ------------------------------------------------------------------
    ; Enable EL3 MMU
    ;
    ; No stack may be used before enabling the MMU. Consider yourself
    ; warned.
    ; ------------------------------------------------------------------
    ldr x0, =mon_l1
    ldr x1, mair
    ldr x2, tcr
    bl warm_init_and_enable_el3_mmu
    ClearNSState 0, 1
    ; ------------------------------------------------------------------
    ; Set up stack.
    ; ------------------------------------------------------------------
    bl get_el3_sp
    add sp, x0, xzr
    ; ------------------------------------------------------------------
    ; Jump to C code init routines. Return is the address for QSEE
    ; kernel entry.
    ; ------------------------------------------------------------------
    bl cpu_init
    ; ------------------------------------------------------------------
    ; Set ELR to QSEE kernel entry point
    ; ------------------------------------------------------------------
    msr ELR_EL3, x0
    ;-------------------------------------------------------------------
    ; Jump to arch specific post CCI reset WA implementation.
    ; Required only for 8936/8939 now. Other target simply return from this API.
    ;-------------------------------------------------------------------
    bl mon_post_cci_reset_wa_arch
    ;-------------------------------------------------------------------
    ; Set SPSR to return to correct state for secure EL1
    ;-------------------------------------------------------------------
    mov x0, #DEFAULT_AARCH32_S_EL1_SPSR
    msr SPSR_EL3, x0
    ;-------------------------------------------------------------------
    ; Make sure no upper bits are set in registers before going to
    ; AArch32 secure EL1.
    ;-------------------------------------------------------------------
    WipeAllGPRegs
    eret
    END
