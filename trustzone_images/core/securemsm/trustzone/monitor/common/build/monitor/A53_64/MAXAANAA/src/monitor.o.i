;============================================================================
;
; Monitor Context Switching and Cold Boot
;
; Copyright 2014 by Qualcomm Technologies, Incorporated. All Rights Reserved
;============================================================================
;============================================================================
;
; EDIT HISTORY FOR MODULE
;
; $Header:
; $DateTime: 2018/02/07 00:37:16 $
; $Author: mplp4svc $
;
; This section contains comments describing changes made to the module.
; Notice that changes are listed in reverse chronological order.
;
;
; when who what, where, why
; -------- --- ---------------------------------------------------------
; 01/16/14 pre Initial revision.
;============================================================================
   MACRO
   CurCPUNum $rx, $ry
      mrs $rx, MPIDR_EL1
      and $ry, $rx, #0x0000FF00
      lsr $ry, #0x8
      mov $rx, #4
      mul $ry, $rx
      mrs $rx, MPIDR_EL1
      and $rx, #0x000000FF
      add $rx, $ry
   MEND
   MACRO
   PerClusterGdhsState $rx, $ry, $rz
     ldr $rz, =l2_gdhs_state;
     add $rz, $rz, $rx, lsl #0x2
     ldr $rx, [$ry]
     MEND
   MACRO
   ClearPerClusterGdhs $rx, $ry, $rz
     ldr $rz, =l2_gdhs_state;
     add $rz, $rz, $rx, lsl #0x2
     mov $rx, #0x0
     str $rx, [$ry]
     MEND
    MACRO
    SetSecure $rx





        mov $rx, xzr
        msr SCR_EL3, $rx
        isb
    MEND
    MACRO
    SetNonSecure $rx





        mov $rx, #0x020:OR:0x001:OR:0x004:OR:0x400:OR:0x100
        msr SCR_EL3, $rx
        isb
    MEND
    MACRO
    PushTwo $x0, $x1
        stp $x1, $x0, [sp, #-0x10]!
    MEND
    MACRO
    PopTwo $x0, $x1
        ldp $x0, $x1, [sp], #0x10
    MEND
    MACRO
    PushAArch32NPRegs
        PushTwo x5, x6
        PushTwo x7, x8
        PushTwo x9, x10
        PushTwo x11, x12
        PushTwo x13, x14
        PushTwo x15, x16
        PushTwo x17, x18
        PushTwo x19, x20
        PushTwo x21, x22
        PushTwo x23, x24
        PushTwo x25, x26
        PushTwo x27, x28
        PushTwo x29, x30
    MEND
    MACRO
    PopAArch32NPRegs
        PopTwo x30, x29
        PopTwo x28, x27
        PopTwo x26, x25
        PopTwo x24, x23
        PopTwo x22, x21
        PopTwo x20, x19
        PopTwo x18, x17
        PopTwo x16, x15
        PopTwo x14, x13
        PopTwo x12, x11
        PopTwo x10, x9
        PopTwo x8, x7
        PopTwo x6, x5
    MEND
    MACRO
    PushAArch64CSRegs
        PushTwo x19, x20
        PushTwo x21, x22
        PushTwo x23, x24
        PushTwo x25, x26
        PushTwo x27, x28
        PushTwo x29, x30
    MEND
    MACRO
    PopAArch64CSRegs
        PopTwo x30, x29
        PopTwo x28, x27
        PopTwo x26, x25
        PopTwo x24, x23
        PopTwo x22, x21
        PopTwo x20, x19
    MEND
    MACRO
    PushAArch64PSRegs
        PushTwo x19, x18
        PushTwo x17, x16
        PushTwo x15, x14
        PushTwo x13, x12
        PushTwo x11, x10
        PushTwo x9, x8
        PushTwo x7, x6
        PushTwo x5, x4
        PushTwo x3, x2
        PushTwo x1, x0
    MEND
    MACRO
    PopAArch64PSRegs
        PopTwo x0, x1
        PopTwo x2, x3
        PopTwo x4, x5
        PopTwo x6, x7
        PopTwo x8, x9
        PopTwo x10, x11
        PopTwo x12, x13
        PopTwo x14, x15
        PopTwo x16, x17
        PopTwo x18, x19
    MEND
    MACRO
    WipeParamRegs
        mov x0, #0
        mov x1, #0
        mov x2, #0
        mov x3, #0
        mov x4, #0
        mov x5, #0
        mov x6, #0
        mov x7, #0
        mov x8, #0
    MEND
    MACRO
    WipeScratchRegs
        mov x9, #0
        mov x10, #0
        mov x11, #0
        mov x12, #0
        mov x13, #0
        mov x14, #0
        mov x15, #0
        mov x16, #0
        mov x17, #0
        mov x18, #0
    MEND
    MACRO
    WipeAllGPRegs
        WipeParamRegs
        WipeScratchRegs
        mov x19, #0
        mov x20, #0
        mov x21, #0
        mov x22, #0
        mov x23, #0
        mov x24, #0
        mov x25, #0
        mov x26, #0
        mov x27, #0
        mov x28, #0
        mov x29, #0
        mov x30, #0
    MEND
    MACRO
    CorruptMonRegs
        mov x0, #0xA
        mov x1, #0xB
        mov x2, #0xC
        mov x3, #0xD
        mov x4, #0xE
        mov x5, #0xF
        mov x6, #0xA
        mov x7, #0xB
        mov x8, #0xC
        mov x9, #0xD
        mov x10, #0xE
        mov x11, #0xF
        mov x12, #0xA
        mov x13, #0xB
        mov x14, #0xC
        mov x15, #0xD
        mov x16, #0xE
        mov x17, #0xF
        mov x18, #0xA
        mov x19, #0xB
        mov x20, #0xC
        mov x21, #0xD
        mov x22, #0xE
        mov x23, #0xF
        mov x24, #0xA
        mov x25, #0xB
        mov x26, #0xC
        mov x27, #0xD
        mov x28, #0xE
        mov x29, #0xF
        mov x30, #0xA
    MEND
  MACRO
  SpinlockObtain $lock_addr, $cpu, $work
    mrs $cpu, MPIDR_EL1
    and $cpu, $cpu, #0xFF
    add $cpu, #0x1
42 ldaxr $work, [$lock_addr]
    cbnz $work, %b42
    stlxr $work, $cpu, [$lock_addr]
    cbnz $work, %b42
    dmb sy
  MEND
  MACRO
  SpinlockRelease $lock_addr
    ; Ensure all reads/writes are visible to other observers before
    ; releasing the lock.
    dmb sy
    str xzr, [$lock_addr] ; Clear the lock
    dmb sy
  MEND
    MACRO
    CurCPUNumber $rx, $ry, $cpus_per_cluster
       mrs $rx, MPIDR_EL1
       and $ry, $rx, #0x0000FF00
       lsr $ry, #0x8
       mov $rx, #$cpus_per_cluster
       mul $ry, $rx
       mrs $rx, MPIDR_EL1
       and $rx, #0x000000FF
       add $rx, $ry
    MEND
    MACRO
    CurClusterNum $rx
       mrs $rx, MPIDR_EL1
       and $rx, #0x0000FF00
       lsr $rx, #0x8
    MEND
; -------------------
; MACRO: PushTwoA
; -------------------
; Like push, but memory addresses ascend instead of descend
    MACRO
    PushTwoA $x0, $x1, $x2
        stp $x1, $x2, [$x0], #0x10
    MEND
; -------------------
; MACRO: PopTwoA
; -------------------
    MACRO
    PopTwoA $x0, $x1, $x2
        ldp $x2, $x1, [$x0, #-0x10]!
    MEND
;
; TODO: CPACR_EL1 - for access to floating point and Advanced SIMD
; execution
;
; ----------------
; MACRO: SaveCtx
; ----------------
; $sp should be a pointer to the base address of a context structure.
; See aa_64_ctx_t in aa64_ctx.h
    MACRO
    SaveCtx $sp
        PushTwoA $sp, x0, x1
        PushTwoA $sp, x2, x3
        PushTwoA $sp, x4, x5
        PushTwoA $sp, x6, x7
        PushTwoA $sp, x8, x9
        PushTwoA $sp, x10, x11
        PushTwoA $sp, x12, x13
        PushTwoA $sp, x14, x15
        PushTwoA $sp, x16, x17
        PushTwoA $sp, x18, x19
        PushTwoA $sp, x20, x21
        PushTwoA $sp, x22, x23
        PushTwoA $sp, x24, x25
        PushTwoA $sp, x26, x27
        PushTwoA $sp, x28, x29

        mrs x0, SPSR_EL1
        PushTwoA $sp, x30, x0

        mrs x0, ELR_EL1
        mrs x1, SPSR_irq
        PushTwoA $sp, x0, x1

        mrs x0, SPSR_abt
        mrs x1, SPSR_und
        PushTwoA $sp, x0, x1

        mrs x0, SPSR_fiq
        mrs x1, SP_EL1
        PushTwoA $sp, x0, x1

        mrs x0, SP_EL0
        mrs x1, CNTP_TVAL_EL0
        PushTwoA $sp, x0, x1

        mrs x0, CNTP_CVAL_EL0
        mrs x1, CNTP_CTL_EL0
        PushTwoA $sp, x0, x1

        mrs x0, VBAR_EL1
        mrs x1, TTBR1_EL1
        PushTwoA $sp, x0, x1

        mrs x0, TTBR0_EL1
        mrs x1, TCR_EL1
        PushTwoA $sp, x0, x1

        mrs x0, TPIDR_EL0
        mrs x1, TPIDRRO_EL0
        PushTwoA $sp, x0, x1

        mrs x0, TPIDR_EL1
        mrs x1, SCTLR_EL1
        PushTwoA $sp, x0, x1

        mrs x0, PAR_EL1
        mrs x1, MAIR_EL1
        PushTwoA $sp, x0, x1

        mrs x0, ESR_EL1
        mrs x1, FAR_EL1
        PushTwoA $sp, x0, x1

        mrs x0, CSSELR_EL1
        mrs x1, CONTEXTIDR_EL1
        PushTwoA $sp, x0, x1

        mrs x0, AMAIR_EL1
        mrs x1, AFSR1_EL1
        PushTwoA $sp, x0, x1

        mrs x0, AFSR0_EL1
        mrs x1, ACTLR_EL1
        PushTwoA $sp, x0, x1

        mrs x0, CNTKCTL_EL1
        mrs x1, FPEXC32_EL2
        PushTwoA $sp, x0, x1

        mrs x0, DACR32_EL2
        mrs x1, IFSR32_EL2
        PushTwoA $sp, x0, x1

        mrs x0, CPACR_El1
        ldr x1, =0xAABBAABB
        PushTwoA $sp, x0, x1

        mrs x0, SPSR_EL3
        mrs x1, ELR_EL3
        PushTwoA $sp, x0, x1
    MEND
; ----------------
; MACRO: LoadCtx
; ----------------
; $sp should be a pointer to the base address of a context structure.
; See aa_64_ctx_t in aa64_ctx.h. The order of removing items from the
; structure is important to properly restore x0, x1.
    MACRO
    LoadCtx $sp
        add $sp, #((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8)

        PopTwoA $sp, x0, x1
        msr ELR_EL3, x0
        msr SPSR_EL3, x1

        PopTwoA $sp, x0, x1
        ; dummy val in x0
        msr CPACR_El1, x1

        PopTwoA $sp, x0, x1
        msr IFSR32_EL2, x0
        msr DACR32_EL2, x1

        PopTwoA $sp, x0, x1
        msr FPEXC32_EL2, x0
        msr CNTKCTL_EL1, x1

        PopTwoA $sp, x0, x1
        msr ACTLR_EL1, x0
        msr AFSR0_EL1, x1

        PopTwoA $sp, x0, x1
        msr AFSR1_EL1, x0
        msr AMAIR_EL1, x1

        PopTwoA $sp, x0, x1
        msr CONTEXTIDR_EL1, x0
        msr CSSELR_EL1, x1

        PopTwoA $sp, x0, x1
        msr FAR_EL1, x0
        msr ESR_EL1, x1

        PopTwoA $sp, x0, x1
        msr MAIR_EL1, x0
        msr PAR_EL1, x1

        PopTwoA $sp, x0, x1
        msr SCTLR_EL1, x0
        msr TPIDR_EL1, x1

        PopTwoA $sp, x0, x1
        msr TPIDRRO_EL0, x0
        msr TPIDR_EL0, x1

        PopTwoA $sp, x0, x1
        msr TCR_EL1, x0
        msr TTBR0_EL1, x1

        PopTwoA $sp, x0, x1
        msr TTBR1_EL1, x0
        msr VBAR_EL1, x1

        PopTwoA $sp, x0, x1
        msr CNTP_CTL_EL0, x0
        msr CNTP_CVAL_EL0, x1

        PopTwoA $sp, x0, x1
        msr CNTP_TVAL_EL0, x0
        msr SP_EL0, x1

        PopTwoA $sp, x0, x1
        msr SP_EL1, x0
        msr SPSR_fiq, x1

        PopTwoA $sp, x0, x1
        msr SPSR_und, x0
        msr SPSR_abt, x1

        PopTwoA $sp, x0, x1
        msr SPSR_irq, x0
        msr ELR_EL1, x1

        PopTwoA $sp, x1, x30
        msr SPSR_EL1, x1

        PopTwoA $sp, x29, x28
        PopTwoA $sp, x27, x26
        PopTwoA $sp, x25, x24
        PopTwoA $sp, x23, x22
        PopTwoA $sp, x21, x20
        PopTwoA $sp, x19, x18
        PopTwoA $sp, x17, x16
        PopTwoA $sp, x15, x14
        PopTwoA $sp, x13, x12
        PopTwoA $sp, x11, x10
        PopTwoA $sp, x9, x8
        PopTwoA $sp, x7, x6
        PopTwoA $sp, x5, x4
        PopTwoA $sp, x3, x2
        PopTwoA $sp, x1, x0

        add $sp, #((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8)
    MEND
; ---------------------
; MACRO: LoadSysRegsCtx
; ---------------------
; Loads registers from the context in a given register
; Note: we do not need to go in a specific order, since there is no
; concern for corrupting GP registers, but it's good to maintain the
; same order as savectx and loadctx so the operations can be compared,
; and those macros go in a specific order due to GP register
; corruption concern.
    MACRO
    LoadSysRegsCtx $x0, $x1, $x2
        add $x0, #((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) - (((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8))

        PopTwoA $x0, $x1, $x2
        msr SPSR_EL3, $x1
        ; dummy val in $x2

        PopTwoA $x0, $x1, $x2
        msr CPACR_El1, $x1
        msr IFSR32_EL2, $x2

        PopTwoA $x0, $x1, $x2
        msr DACR32_EL2, $x1
        msr FPEXC32_EL2, $x2

        PopTwoA $x0, $x1, $x2
        msr CNTKCTL_EL1, $x1
        msr ACTLR_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr AFSR0_EL1, $x1
        msr AFSR1_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr AMAIR_EL1, $x1
        msr CONTEXTIDR_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr CSSELR_EL1, $x1
        msr FAR_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr ESR_EL1, $x1
        msr MAIR_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr PAR_EL1, $x1
        msr SCTLR_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr TPIDR_EL1, $x1
        msr TPIDRRO_EL0, $x2

        PopTwoA $x0, $x1, $x2
        msr TPIDR_EL0, $x1
        msr TCR_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr TTBR0_EL1, $x1
        msr TTBR1_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr VBAR_EL1, $x1
        msr CNTP_CTL_EL0, $x2

        PopTwoA $x0, $x1, $x2
        msr CNTP_CVAL_EL0, $x1
        msr CNTP_TVAL_EL0, $x2

        PopTwoA $x0, $x1, $x2
        msr SP_EL0, $x1
        msr SP_EL1, $x2

        PopTwoA $x0, $x1, $x2
        msr SPSR_fiq, $x1
        msr SPSR_und, $x2

        PopTwoA $x0, $x1, $x2
        msr SPSR_abt, $x1
        msr SPSR_irq, $x2

        PopTwoA $x0, $x1, $x2
        msr ELR_EL1, $x1
        msr SPSR_EL1, $x2

        add $x0, #((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) - (((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8))
    MEND
; ---------------------
; MACRO: SaveSysRegsCtx
; ---------------------
; Saves registers from the context in a given register
    MACRO
    SaveSysRegsCtx $x0, $x1, $x2
        sub $x0, #((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) - (((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8))

        mrs $x1, SPSR_EL1
        mrs $x2, ELR_EL1
        PushTwoA $x0, $x1, $x2

        mrs $x1, SPSR_irq
        mrs $x2, SPSR_abt
        PushTwoA $x0, $x1, $x2

        mrs $x1, SPSR_und
        mrs $x2, SPSR_fiq
        PushTwoA $x0, $x1, $x2

        mrs $x1, SP_EL1
        mrs $x2, SP_EL0
        PushTwoA $x0, $x1, $x2

        mrs $x1, CNTP_TVAL_EL0
        mrs $x2, CNTP_CVAL_EL0
        PushTwoA $x0, $x1, $x2

        mrs $x1, CNTP_CTL_EL0
        mrs $x2, VBAR_EL1
        PushTwoA $x0, $x1, $x2

        mrs $x1, TTBR1_EL1
        mrs $x2, TTBR0_EL1
        PushTwoA $x0, $x1, $x2

        mrs $x1, TCR_EL1
        mrs $x2, TPIDR_EL0
        PushTwoA $x0, $x1, $x2

        mrs $x1, TPIDRRO_EL0
        mrs $x2, TPIDR_EL1
        PushTwoA $x0, $x1, $x2

        mrs $x1, SCTLR_EL1
        mrs $x2, PAR_EL1
        PushTwoA $x0, $x1, $x2

        mrs $x1, MAIR_EL1
        mrs $x2, ESR_EL1
        PushTwoA $x0, $x1, $x2

        mrs $x1, FAR_EL1
        mrs $x2, CSSELR_EL1
        PushTwoA $x0, $x1, $x2

        mrs $x1, CONTEXTIDR_EL1
        mrs $x2, AMAIR_EL1
        PushTwoA $x0, $x1, $x2

        mrs $x1, AFSR1_EL1
        mrs $x2, AFSR0_EL1
        PushTwoA $x0, $x1, $x2

        mrs $x1, ACTLR_EL1
        mrs $x2, CNTKCTL_EL1
        PushTwoA $x0, $x1, $x2

        mrs $x1, FPEXC32_EL2
        mrs $x2, DACR32_EL2
        PushTwoA $x0, $x1, $x2

        mrs $x1, IFSR32_EL2
        mrs $x2, CPACR_El1
        PushTwoA $x0, $x1, $x2

        ldr $x1, =0xAABBAABB
        mrs $x2, SPSR_EL3
        PushTwoA $x0, $x1, $x2

        sub $x0, #((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) - (((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8))
    MEND
; ----------------
; MACRO: MigrateSysregsFromEL3ToEL1
; ----------------
; Takes EL3 system registers and writes to EL1 counterparts
    MACRO
    MigrateSysregsFromEL3ToEL1 $x0, $x1
        mrs $x0, SPSR_EL3
        msr SPSR_EL1, $x0

        mrs $x0, ELR_EL3
        msr ELR_EL1, $x0

        mrs $x0, VBAR_EL3
        msr VBAR_EL1, $x0

        mrs $x0, TTBR0_EL3
        msr TTBR0_EL1, $x0

        mrs $x0, TCR_EL3
        ldr $x1, =(0x0000003F :OR: 0x00000300 :OR: 0x00000C00 :OR: 0x00003000 :OR: 0x0000C000 :OR: 0x00070000 :OR: 0x00100000)



        and $x0, $x1 ; Mask DC vals

        ubfx $x1, $x0, #20, #(0x00100000 >> 20)

                                        ; Extract TBI and put in $x1
        lsl $x1, #37 ; Move TCR_EL3[TBI] to TCR_EL1[TBI0]
        orr $x0, $x1 ; Apply TCR_EL3[TBI] to $x0
        ldr $x1, =0x00100000
        bic $x0, $x1 ; Clear TCR_EL3[TBI] from $x0

        ubfx $x1, $x0, #16, #(0x00070000 >> 16)

                                        ; Extract PS and put in $x1
        lsl $x1, #32 ; Move TCR_EL3[PS] to TCR_EL1[IPS]
        orr $x0, $x1 ; Apply TCR_EL3[PS] to $x0
        ldr $x1, =0x00070000
        bic $x0, $x1 ; Clear TCR_EL3[PS] from $x0

        msr TCR_EL1, $x0

        mrs $x0, TPIDR_EL3
        msr TPIDR_EL1, $x0

        mrs $x0, SCTLR_EL3
        ldr $x1, =(0x00000001 :OR: 0x00000002 :OR: 0x00000004 :OR: 0x00000008 :OR: 0x00001000 :OR: 0x00080000 :OR: 0x02000000)



        and $x0, $x1 ; Mask DC vals
        msr SCTLR_EL1, $x0

        ; no EL3 for PAR

        mrs $x0, MAIR_EL3
        msr MAIR_EL1, $x0

        mrs $x0, ESR_EL3
        msr ESR_EL1, $x0

        mrs $x0, FAR_EL3
        msr FAR_EL1, $x0

        ; no EL3 for CSSELR
        ; no EL3 for CONTEXTIDR

        mrs $x0, AMAIR_EL3
        msr AMAIR_EL1, $x0

        mrs $x0, AFSR1_EL3
        msr AFSR1_EL1, $x0

        mrs $x0, AFSR0_EL3
        msr AFSR0_EL1, $x0

        mrs $x0, ACTLR_EL3 ; TODO: implementation defined
        msr ACTLR_EL1, $x0

        mrs $x0, SPSel
        cmp $x0, #0 ; 0 indicates t, else h
        mov $x0, sp ; $x0 = SBL SP
        beq %f1
        msr SP_EL1, $x0 ; SBL uses sp_el1
        mov $x1, #0x05 ; $x1 = SPSR[M] = SPh
        b %f2
1 msr SP_EL0, $x0 ; SBL uses sp_el0
        mov $x1, #0x04 ; $x1 = SPSR[M] = SPt
2 mrs $x0, DAIF ; $x0 = SPSR[DAIF]
        orr $x0, $x1 ; Combine M with DAIF
        msr SPSR_EL3, $x0 ; set SPSR[MDAIF]
                                       ; TODO: other bits for AArch32 return

    MEND
;============================================================================
;
    GET monitor_macros.inc
    IMPORT mon_call_from_ns
    IMPORT mon_call_from_secure
    IMPORT cold_boot
    IMPORT shared_boot_info_params
    IMPORT memset
    IMPORT ns_vfp_cxt
    EXPORT ns_state
    EXPORT resume_cold_boot_post_cci_wa
    EXPORT MON_COLD_ENTRY
    EXPORT vfp_handler_func
    EXPORT load_vfp_regs
NS_RETURN_VAL_IRQ_EXIT EQU 0x1
TRUSTED_OS_SESSION_ID EQU 0x42 ; Hardcoded session ID because only
                                          ; one session is supported.
SEC_MON_CALL_RETURN_TO_S_RET EQU 0x0 ; Return from monitor call to secure
SEC_MON_CALL_RETURN_TO_NS_RET EQU 0x1 ; return from monitor call to ns
SEC_MON_CALL_RETURN_TO_OS_ID EQU 0x1 ; Return to HLOS bootloader
SEC_MON_CALL_FIQ_COMPLETE_ID EQU 0x2 ; Return after FIQ
SEC_MON_CALL_SMP_NOTIFIER_COMPLETE_ID EQU 0xE
SEC_MON_CALL_IRQ_ID EQU 0xF
SMC_HANDLED_IN_SECURE_EL1 EQU 0x0
SMC_HANDLED_IN_MONITOR EQU 0x1
NS_VFP_SAVED EQU 0x1
NS_VFP_NOT_SAVED EQU 0x0
DEFAULT_AARCH32_S_EL1_SPSR EQU 0x003:OR:0x080:OR:0x040:OR:0x010
DEFAULT_AARCH32_S_EL1_FIQ_SPSR EQU 0x001:OR:0x080:OR:0x040:OR:0x010:OR:0x100
; ----------------
; MACRO: PushSMC
; ----------------
; Preserve all the input registers in a way that they can be passed
; on the stack. Also save x30 so that it may be restored after
; function calls. Fewer parameters may be pushed on the stack when
; the NS clients don't expect TZ to only modify r0-r3.
; ----------------
    MACRO
    PushSMC
        PushTwo x30, x29
        PushAArch64PSRegs
    MEND
; ----------------
; MACRO: PopSMC
; ----------------
    MACRO
    PopSMC
        PopAArch64PSRegs
        PopTwo x29, x30
    MEND
; ----------------
; MACRO: GetEL3SP
; ----------------
; When you need to set the stack without messing with the LR, inline!
    MACRO
    get_el3_sp_inline $x0, $x1, $x2
        ldr $x0, =mon_stack
        CurCPUNum $x1, $x2
        ldr $x2, =0x800
        mul $x1, $x2
        sub $x0, $x1
    MEND
    PRESERVE8
    AREA MON_COLD_ENTRY, CODE, READONLY, ALIGN=3
    ENTRY
cold_boot_asm
    ; ------------------------------------------------------------------
    ; Until SBL context is saved, we may only use x0-x19 (From PCS doc)
    ; ------------------------------------------------------------------
    ldr x2, =shared_boot_info_params
    stp w0, w1, [x2]
    ldr x1, =valid_ns_ctx
    mov x0, #NS_VFP_NOT_SAVED
    str x0, [x1]
    ; ------------------------------------------------------------------
    ; If the relative address of a symbol doesnt match the absolute
    ; address given by the linker, the load location of the monitor
    ; image is wrong.
    ; ------------------------------------------------------------------
    adr x0, cold_boot_asm
    ldr x1, =cold_boot_asm
    cmp x0, x1
    bne load_mismatch_error
    ; ------------------------------------------------------------------
    ; Set SCR to secure world
    ; ------------------------------------------------------------------
    SetSecure x0
    ; ------------------------------------------------------------------
    ; TODO: skip this if coming from EL1 in SBL
    ;
    ; Move EL3 system registers to EL1 system registers before saving
    ; context so that the same context saving macro thats used for NS
    ; world switches can be used for boot.
    ; ------------------------------------------------------------------
    MigrateSysregsFromEL3ToEL1 x0, x1
    ; ------------------------------------------------------------------
    ; May not corrupt any system regs until now, including stack
    ; ------------------------------------------------------------------
    ; ------------------------------------------------------------------
    ; Program EL3 vector table. Should happen as early as corruption
    ; of vbar is possible in order to catch any oopsies.
    ; ------------------------------------------------------------------
    ldr x0, =vector_table
    msr VBAR_EL3, x0
    ; ------------------------------------------------------------------
    ; Mask FIQ, IRQs and external aborts. Monitor should never be
    ; interruptible.
    ; ------------------------------------------------------------------
    mov x0, #0x080:OR:0x040:OR:0x100
    msr DAIF, x0
    ;-------------------------------------------------------------------
    ; Set SPSR to return to correct state for NS.
    ;
    ; TODO: This isnt needed unless SBL comes to monitor without using
    ; an exception
    ;-------------------------------------------------------------------
    mov x0, #0x09:OR:0x80
    msr SPSR_EL3, x0
    ; ------------------------------------------------------------------
    ; Set up stack.
    ; ------------------------------------------------------------------
    get_el3_sp_inline x0, x1, x2
    add sp, x0, xzr
    ; ------------------------------------------------------------------
    ; Save NS (SBLs) context.
    ; ------------------------------------------------------------------
    sub sp, #((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8)
    SaveCtx sp
    sub sp, #((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8)
    ; ------------------------------------------------------------------
    ; Assume control of EL3 address space by first disabling the MMU.
    ; Will re-enable later from cold_boot_c code.
    ; ------------------------------------------------------------------
    mrs x0, SCTLR_EL3
    mov x1, #0x00000001
    bic x0, x1
    msr SCTLR_EL3, x0
    ; ------------------------------------------------------------------
    ; Jump to C code init routines. Return is the address for QSEE
    ; kernel entry.
    ; ------------------------------------------------------------------
    bl cold_boot
    ; ------------------------------------------------------------------
    ; Set ELR to QSEE kernel entry point
    ; ------------------------------------------------------------------
    msr ELR_EL3, x0
    ; ------------------------------------------------------------------
    ; This label used only for 8939 target from mon_cpu_reset_arch.s file.
    ; 8939 resume cold boot flow here when it warm boot's due to CCI reset.
    ; ------------------------------------------------------------------
resume_cold_boot_post_cci_wa
    ; ------------------------------------------------------------------
    ; Set register width for remainder of boot images. Register width
    ; shouldnt be modified in the smc handling path, so it must be
    ; configured during the cold boot path once.
    ;
    ; TODO: The register width should depend on the boot parameters
    ; passed by SBL
    ; TODO: EL2 RW depends on EL3_SCR, which is hardcoded
    ; ------------------------------------------------------------------
    mrs x0, HCR_EL2
    mov x1, #0x80000000 ; NS EL1, EL0 = 64 bit
    orr x0, x1
    msr HCR_EL2, x0
    ;-------------------------------------------------------------------
    ; Set SPSR to return to correct state for secure EL1
    ;-------------------------------------------------------------------
    mov x0, #DEFAULT_AARCH32_S_EL1_SPSR
    msr SPSR_EL3, x0
    ; ------------------------------------------------------------------
    ; EL1 system registers are set to SBLs settings from migration
    ; above. Put them in default state so QSEE kernel can set them at
    ; will.
    ; ------------------------------------------------------------------
    bl zero_el1_el0_sys_regs_noapcs
    ; ------------------------------------------------------------------
    ; Clear all GP regs before going to secure side for the first time
    ; so that the upper 32 bits of all secure EL1 registers are zero.
    ; ------------------------------------------------------------------
    WipeAllGPRegs
    ; ------------------------------------------------------------------
    ; Configure cold boot parameters for QSEE cold boot call
    ; ------------------------------------------------------------------
    ldr x2, =shared_boot_info_params
    ldp w0, w1, [x2]
    mov x2, xzr
    ;-------------------------------------------------------------------
    ; Ensure all system registers updates are observed
    ;-------------------------------------------------------------------
    dsb sy
    eret
    ; END
;===================================================================
; void vfp_handler_func(aa_64_vfp_ctx_t* p_vfp_ctx)
;
; Save NS context and enable VFP
;===================================================================
vfp_handler_func
    ;-------------------------------------------------------------------
    ; Enable VFP and Neon in EL1
    ;-------------------------------------------------------------------
    mrs x1, FPEXC32_EL2
    orr x1, #0x40000000
    msr FPEXC32_EL2, x1
    ;-------------------------------------------------------------------
    ; Make sure VFP is enabled before saving the NS VFP context
    ;-------------------------------------------------------------------
    isb
    ; check if we already have the valid NS context
    ldr x1, valid_ns_ctx
    cmp x1, #NS_VFP_SAVED
    beq valid_ns_vfp_cxt
    ;-------------------------------------------------------------------
    ; Skip p_vfp_ctx->SavedStatusReg
    ;-------------------------------------------------------------------
    add x0, x0, #8
    ;------------------------------------------------------------------
    ; Save the NS VFP context
    ;-------------------------------------------------------------------
     str q0, [x0, #0]
     str q1, [x0, #16]
     str q2, [x0, #32]
     str q3, [x0, #48]
     str q4, [x0, #64]
     str q5, [x0, #80]
     str q6, [x0, #96]
     str q7, [x0, #112]
     str q8, [x0, #128]
     str q9, [x0, #144]
     str q10, [x0, #160]
     str q11, [x0, #176]
     str q12, [x0, #192]
     str q13, [x0, #208]
     str q14, [x0, #224]
     str q15, [x0, #240]
     mrs x1, fpsr
     str x1, [x0, #256]
    ; Enable the flag to context switch
    ldr x4, =valid_ns_ctx
    mov x1, #NS_VFP_SAVED
    str x1, [x4]
valid_ns_vfp_cxt
     ret
     ; END
;===================================================================
; void store_vfp_regs(aa_64_vfp_ctx_t* p_vfp_ctx)
;
; Loads NS context and enables VFP
;===================================================================
store_vfp_regs
    ;-------------------------------------------------------------------
    ; Enable VFP and Neon in EL1
    ;-------------------------------------------------------------------
    ;-------------------------------------------------------------------
    ; Skip p_vfp_ctx->SavedStatusReg
    ;-------------------------------------------------------------------
    add x0, x0, #8
    ;------------------------------------------------------------------
    ; Save the NS VFP context
    ;-------------------------------------------------------------------
    str q0, [x0, #0]
    str q1, [x0, #16]
    str q2, [x0, #32]
    str q3, [x0, #48]
    str q4, [x0, #64]
    str q5, [x0, #80]
    str q6, [x0, #96]
    str q7, [x0, #112]
    str q8, [x0, #128]
    str q9, [x0, #144]
    str q10, [x0, #160]
    str q11, [x0, #176]
    str q12, [x0, #192]
    str q13, [x0, #208]
    str q14, [x0, #224]
    str q15, [x0, #240]
    mrs x1, fpsr
    str x1, [x0, #256]
     ; do we need to disable the exception?
    ret
     ; END
;===================================================================
; void load_vfp_regs(aa_64_vfp_ctx_t* p_vfp_ctx)
;
; Loads NS context and enables VFP
;===================================================================
load_vfp_regs
    ;-------------------------------------------------------------------
    ; NULL pointer argument means no VFP needs to be loaded
    ;-------------------------------------------------------------------
    cmp x0, #0
    beq vfp_not_saved
    ;-------------------------------------------------------------------
    ; Enable VFP and Neon in EL1
    ;-------------------------------------------------------------------
    add x0, x0, #8
    ;-------------------------------------------------------------------
    ; Load the NS FP context
    ;-------------------------------------------------------------------
    ldr q0, [x0, #0]
    ldr q1, [x0, #16]
    ldr q2, [x0, #32]
    ldr q3, [x0, #48]
    ldr q4, [x0, #64]
    ldr q5, [x0, #80]
    ldr q6, [x0, #96]
    ldr q7, [x0, #112]
    ldr q8, [x0, #128]
    ldr q9, [x0, #144]
    ldr q10, [x0, #160]
    ldr q11, [x0, #176]
    ldr q12, [x0, #192]
    ldr q13, [x0, #208]
    ldr q14, [x0, #224]
    ldr q15, [x0, #240]
    ldr x1, [x0, #256]
    msr fpsr, x1
  ; do we need to disable the exception?
vfp_not_saved
   ret
   ; END
;===================================================================
; void zero_el1_el0_sys_regs_noapcs(void)
;
; Zeros out all system regs before initially entering into EL1
; image.
;
; WARNING: This call is not APCS compliant and may not modify any GP
; registers.
;===================================================================
zero_el1_el0_sys_regs_noapcs
    msr ACTLR_EL1, xzr
    msr AFSR1_EL1, xzr
    msr AFSR0_EL1, xzr
    msr CONTEXTIDR_EL1, xzr
    msr AMAIR_EL1, xzr
    msr FAR_EL1, xzr
    msr CSSELR_EL1, xzr
    msr MAIR_EL1, xzr
    msr ESR_EL1, xzr
    msr SCTLR_EL1, xzr
    msr PAR_EL1, xzr
    msr TPIDRRO_EL0, xzr
    msr TPIDR_EL1, xzr
    msr TCR_EL1, xzr
    msr TPIDR_EL0, xzr
    msr TTBR1_EL1, xzr
    msr TTBR0_EL1, xzr
    msr CNTP_CTL_EL0, xzr
    msr VBAR_EL1, xzr
    msr CNTP_TVAL_EL0, xzr
    msr CNTP_CVAL_EL0, xzr
    msr ELR_EL1, xzr
    msr SP_EL1, xzr
    msr SP_EL0, xzr
    msr SPSR_EL1, xzr
    ret
load_mismatch_error
    b load_mismatch_error
;===================================================================
; smc_hander: SMC exception handler. Entry point from NS or S.
;===================================================================
    EXPORT smc_handler
smc_handler
    ;-------------------------------------------------------------------
    ; Coming from secure or NS? Do not corrupt x0: Used below.
    ;-------------------------------------------------------------------
    mrs x4, SCR_EL3
    tst x4, #0x001
    bne smc_from_ns
    ;-------------------------------------------------------------------
    ; Coming from secure
    ;-------------------------------------------------------------------
    EXPORT smc_from_s
smc_from_s
    ;-------------------------------------------------------------------
    ; Handle SMP notifier completion
    ;-------------------------------------------------------------------
    cmp x0, #SEC_MON_CALL_SMP_NOTIFIER_COMPLETE_ID
    beq el1_smp_notifier_end
    ;-------------------------------------------------------------------
    ; Handle completion of CPU init after PC or secondary CPU
    ; init. The monitor does not support multiple secure contexts yet,
    ; so need to intercept the entry to NS call, and call
    ; mon_call_complete_cpu_init. When multiple secure contexts are
    ; supported, the tzbsp_mon_call_from_secure() function can
    ; directly modify the return address in C
    ;-------------------------------------------------------------------
    cmp x0, #SEC_MON_CALL_RETURN_TO_OS_ID
    beq mon_call_complete_cpu_init
    ;-------------------------------------------------------------------
    ; Handle FIQ completion
    ;-------------------------------------------------------------------
    cmp x0, #SEC_MON_CALL_FIQ_COMPLETE_ID
    beq fiq_complete
    ;-------------------------------------------------------------------
    ; Handle IRQ happened while in secure EL1
    ;-------------------------------------------------------------------
    cmp x0, #SEC_MON_CALL_IRQ_ID
    beq irq_handler
    ;-------------------------------------------------------------------
    ; Call monitors secure EL1 SMC handler.
    ;-------------------------------------------------------------------
    mov x0, sp
    bl mon_call_from_secure
    ;-------------------------------------------------------------------
    ; If SEC_MON_CALL_RETURN_TO_S_RET (0) is returned, go back to
    ; secure.
    ;-------------------------------------------------------------------
    cmp x0, #SEC_MON_CALL_RETURN_TO_S_RET
    ;-------------------------------------------------------------------
    ; The mon_call C code indicated to return to secure
    ;-------------------------------------------------------------------
    beq exit_to_secure
    ;-------------------------------------------------------------------
    ; The mon_call C code indicated to return to non-secure.
    ;-------------------------------------------------------------------
smc_exit_to_ns
    ;-------------------------------------------------------------------
    ; The below is done for both SMC exits to NS and IRQ exits to NS.
    ;-------------------------------------------------------------------
    EXPORT common_monitor_exit
irq_exit_to_ns
common_monitor_exit
    ;-------------------------------------------------------------------
    ; The secure kernel traps FIQs to FIQ mode directly, but the
    ; secure kernel doesn't know whether a FIQ interrupted secure
    ; execution or NS execution, so it calls into EL3 and EL3 decides
    ; based on this variable what world to return to.
    ;-------------------------------------------------------------------
    SaveNSState 0, 1, 2
    ;-------------------------------------------------------------------
    ; Secure side calls are supposed to made only by non-secure side
    ; Secure Channel Manager. The call to secure side returns x0 and
    ; x1 from non-secure perspective. This means we can corrupt
    ; non-secure registers x2 and x3.
    ;
    ; TODO: the SCM should follow the SMC calling convetions and x0-x3
    ; may all be return values, but there are other corruptable
    ; registers.
    ;-------------------------------------------------------------------
    PopSMC
    ;-------------------------------------------------------------------
    ; If we exited secure state via IRQ, then set ns_x0 to a special
    ; return value to indicate as such. Also, set ns_x6 to the 32-bit
    ; Trusted OS Session ID to represent the interrupted EL1 context.
    ;-------------------------------------------------------------------
    ldr x0, did_irq_exit
    cmp x0, #0
    beq %f1 ; Didnt IRQ exit, skip ns_x0 assignment
    mov x0, #NS_RETURN_VAL_IRQ_EXIT
    str x0, [sp] ; SP is currently ns_x0
    mov x0, #TRUSTED_OS_SESSION_ID
    str x0, [sp, #((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8)]
1
    ;-------------------------------------------------------------------
    ; Do context switch
    ;-------------------------------------------------------------------
    add x0, sp, xzr
    msr TPIDR_EL3, x0
    ldr x0, =s_ctx
    add sp, x0, xzr
    SaveCtx sp
    mrs x0, TPIDR_EL3
    add sp, x0, xzr
    ;-------------------------------------------------------------------
    ; Check if last VFP enabled app shutdown, if so, restore the NS
    ;-------------------------------------------------------------------
    ldr x1, valid_ns_ctx
    cmp x1, #NS_VFP_NOT_SAVED
    beq load_only_gp_ctx
    ;-------------------------------------------------------------------
    ; Load the NS VFP context
    ;-------------------------------------------------------------------
    ldr x0, =ns_vfp_cxt
    bl load_vfp_regs
    ldr x4, =valid_ns_ctx
    mov x1, #NS_VFP_NOT_SAVED
    str x1, [x4]
load_only_gp_ctx
    LoadCtx sp
    ;-------------------------------------------------------------------
    ; Clear out sensitive information from parameter registers and
    ; scratch registers.
    ;
    ; TODO: Can't do this until NS uses ARM SMC conventions
    ;-------------------------------------------------------------------
; WipeParamRegs
; WipeScratchRegs
    ;-------------------------------------------------------------------
    ; Ensure all system registers updates are observed
    ;-------------------------------------------------------------------
    dsb sy
    ;-------------------------------------------------------------------
    ; Clear the QSEE entry spinlock and clear temporary registers.
    ; After this point any other core can enter the QSEE.
    ; Saving X12 as no GP rgister may be modified.
    ;-------------------------------------------------------------------
    msr TPIDR_EL3, x12
    ldr x12, =smc_lock
    SpinlockRelease x12
    ;-------------------------------------------------------------------
    ; restoring X12 and return to Non-secure world.
    ;-------------------------------------------------------------------
    SetNonSecure x12
    mrs x12, TPIDR_EL3
    eret
exit_to_secure
    ;-------------------------------------------------------------------
    ; Restore all registers from monitor caller. (Unwind stack)
    ;-------------------------------------------------------------------
    PopSMC
    ;-------------------------------------------------------------------
    ; Ensure all system registers updates are observed
    ;-------------------------------------------------------------------
    dsb sy
    eret
    ; END
fiq_complete
    ;-------------------------------------------------------------------
    ; Read the saved SCR and determine which world to return to
    ;-------------------------------------------------------------------
    ReadNSState 0, 1
    tst x0, #0x001
    ;-------------------------------------------------------------------
    ; Unconditionally unwind the stack
    ;-------------------------------------------------------------------
    PopSMC
    ;-------------------------------------------------------------------
    ; Return to proper environment according to above check for NS bit
    ;-------------------------------------------------------------------
    beq fiq_return_to_s
fiq_return_to_ns
    ; ------------------------------------------------------------------
    ; Return to Non Secure
    ; ------------------------------------------------------------------
    SetNonSecure x0
    ;-------------------------------------------------------------------
    ; FIQ originated in NS state, return to NS world. Pop off
    ; registers from stack that were saved by the monitor FIQ handler
    ; Secure OS FIQ handler SMCs back even if the FIQ fired while
    ; Secure World is running because only the monitor knows security
    ; state at the time when the FIQ fired.
    ;-------------------------------------------------------------------
    LoadCtx sp
    ;-------------------------------------------------------------------
    ; Ensure all system registers updates are observed
    ;-------------------------------------------------------------------
    dsb sy
    eret
    ; END
    ;-------------------------------------------------------------------
    ; FIQ originated in secure state. Return to Secure World.
    ;-------------------------------------------------------------------
fiq_return_to_s
    eret
    ; END
    ;-------------------------------------------------------------------
    ; Called during CPU reset when exiting to HLOS.
    ; x0 = call id from SMC entry
    ; x1 = NS EL1 entry physical address, lower 32 bits
    ; x2 = NS EL1 entry physical address, upper 32 bits
    ; x3 = hyp entry physical address, lower 32 bits
    ; x4 = hyp entry physical address, upper 32 bits
    ; x5 = NS EL1 ELF EI class
    ; x6 = NS EL2 ELF EI class
mon_call_complete_cpu_init
    ; ------------------------------------------------------------------
    ; Clear out secure settings from EL1 and EL0 system registers
    ; before handing off to NS. This call is not APCS compliant and
    ; may not modify any GP registers.
    ; ------------------------------------------------------------------
    bl zero_el1_el0_sys_regs_noapcs
    mov x30, xzr ; Clear the LR
    ;-------------------------------------------------------------------
    ; The secure kernel traps FIQs to FIQ mode directly, but the
    ; secure kernel doesn't know whether a FIQ interrupted secure
    ; execution or NS execution, so it calls into EL3 and EL3 decides
    ; based on this variable what world to return to.
    ;-------------------------------------------------------------------
    SaveNSState 0, 1, 2
    ;-------------------------------------------------------------------
    ; Unwind the stack
    ;-------------------------------------------------------------------
    PopSMC
    ;-------------------------------------------------------------------
    ; Combine x1 and x2 and put into x0
    ; WARNING: May not modify x0 for remainder of routine
    ;-------------------------------------------------------------------
    and x0, x1, #0xFFFFFFFF
    orr x0, x2, lsl #32
    ;-------------------------------------------------------------------
    ; Combine x3 and x4 and put into x3
    ;-------------------------------------------------------------------
    and x3, #0xFFFFFFFF
    orr x3, x4, lsl #32
    ; ------------------------------------------------------------------
    ; Set ELR to hypervisor entry
    ; ------------------------------------------------------------------
    msr ELR_EL3, x3
    ; ------------------------------------------------------------------
    ; Configure HCR, so NS is at the right register width
    ;
    ; TODO: use ELF_EI class #define
    ; ------------------------------------------------------------------
    mrs x1, HCR_EL2
    mov x3, #0x80000000
    tst x6, #2
    beq %f1
    ; ------------------------------------------------------------------
    ; EL2 is AArch64
    ; ------------------------------------------------------------------
    mov x2, #0x09:OR:0x80
    orr x1, x3
    b %f2
    ; ------------------------------------------------------------------
    ; hyp is AArch32
    ; ------------------------------------------------------------------
1 mov x2, #0x00A:OR:0x80
    bic x1, x3
2 msr HCR_EL2, x1
    msr SPSR_EL3, x2
    ; ------------------------------------------------------------------
    ; Move NS SVC/EL1 class to x1 parameter
    ; ------------------------------------------------------------------
    mov x1, x5
    ;-------------------------------------------------------------------
    ; Clear all GP regs for end of cold boot
    ; EL1 entry address and EL1 ELF EI class returned in x0 and x1
    ;-------------------------------------------------------------------
    PushTwo x0, x1
    WipeAllGPRegs
    PopTwo x1, x0
    ;-------------------------------------------------------------------
    ; Return to NS
    ; EL1 entry address and EL1 ELF EI class returned in x0 and x1
    ;-------------------------------------------------------------------
    MonCallReturnToNonSecure x2
    ; END
    ;-------------------------------------------------------------------
    ; Handle SMC calls from NS world
    ;-------------------------------------------------------------------
    EXPORT smc_from_ns
smc_from_ns
    ;-------------------------------------------------------------------
    ; Switch to secure mode immediately
    ;-------------------------------------------------------------------
    SetSecure x0
    ;-------------------------------------------------------------------
    ; Ready parameter to stacked register context for monitor system
    ; call handler
    ;-------------------------------------------------------------------
    mov x0, sp
    ;-------------------------------------------------------------------
    ; Call monitor syscall handler
    ;-------------------------------------------------------------------
    bl mon_call_from_ns
    ;-------------------------------------------------------------------
    ; Check return value from mon_call
    ;-------------------------------------------------------------------
    cmp x0, #SMC_HANDLED_IN_MONITOR
    ;-------------------------------------------------------------------
    ; If the monitor call was handled in the monitor, just return
    ;-------------------------------------------------------------------
    beq ns_mon_call_return_to_ns
    EXPORT secure_svc_entry
secure_svc_entry
    ;-------------------------------------------------------------------
    ; Check if EL1 was previously interrupted by an IRQ.
    ;-------------------------------------------------------------------
    ldr x0, did_irq_exit
    cmp x0, #1
    ;-------------------------------------------------------------------
    ; If EL1 was not previously interrupted, continue entry into EL1.
    ;-------------------------------------------------------------------
    bne secure_svc_entry_continued
    ;-------------------------------------------------------------------
    ; Get 32 bit Trusted OS Session ID passed in W6. The session ID
    ; represents an interrupted EL1 context and must match in order to
    ; resume that context.
    ;
    ; Note: Since EL1 can only have one interrupted context at a time
    ; (single-threaded model), there is only one hardcoded session ID.
    ;-------------------------------------------------------------------
    ldr w0, [sp, #((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8)]
    cmp w0, #TRUSTED_OS_SESSION_ID
    beq secure_svc_entry_continued
    ;-------------------------------------------------------------------
    ; No session ID match, return error.
    ;-------------------------------------------------------------------
    mov x0, #-13 ; SMC_ERR_SESSION_ID_MISMATCH
    str x0, [sp, #0]
    bne ns_mon_call_return_to_ns
secure_svc_entry_continued
    ;-------------------------------------------------------------------
    ; The secure kernel traps FIQs to FIQ mode directly, but the
    ; secure kernel doesn't know whether a FIQ interrupted secure
    ; execution or NS execution, so it calls into EL3 and EL3 decides
    ; based on this variable what world to return to.
    ;-------------------------------------------------------------------
    ClearNSState 0, 1
    ;-------------------------------------------------------------------
    ; Grab SMC lock
    ;-------------------------------------------------------------------
    ldr x0, =smc_lock
    SpinlockObtain x0, x1, w2
    ;-------------------------------------------------------------------
    ; Always clear IRQ exit status when entering EL1.
    ;-------------------------------------------------------------------
    ldr x0, =did_irq_exit
    str xzr, [x0]
    ;-------------------------------------------------------------------
    ; Unwind stack
    ;-------------------------------------------------------------------
    PopSMC
    ;-------------------------------------------------------------------
    ; Save NS context
    ;-------------------------------------------------------------------
    sub sp, #((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8)
    SaveCtx sp
    sub sp, #((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8)
    ;-------------------------------------------------------------------
    ; Place copy of ns_context on secure el1 stack, pointed to by
    ; secure x1.
    ;
    ; TODO: This assumes AArch32 secure EL1
    ;-------------------------------------------------------------------
    ldr x0, s_ctx
    ldr w0, [x0, #(0 + 0x8)] ; s_x1
    stp w2, w3, [x0, #0x10]
    stp w4, w5, [x0, #0x18]
    stp w6, w7, [x0, #0x20]
    stp w8, w9, [x0, #0x28]
    stp w10, w11, [x0, #0x30]
    stp w12, w13, [x0, #0x38]
    mrs x1, SPSR_EL3
    stp w30, w1, [x0] ; lr and ns_cpsr
    ldp x1, x2, [sp] ; ns x0, x1 were both clobbered
    stp w1, w2, [x0, #0x08] ; by SaveCtx, so restore them
    ;-------------------------------------------------------------------
    ; Save SP EL3
    ;-------------------------------------------------------------------
    add x0, sp, xzr
    msr TPIDR_EL3, x0
    ;-------------------------------------------------------------------
    ; Disable VFP exception
    ;-------------------------------------------------------------------
; ldr x0, =s_ctx
; ldr w1, [x0, #((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8)]
; mov w2, #0xff5fffff
; and w1, w2
; str w1, [x0, #((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8)]
    ;-------------------------------------------------------------------
    ; Load secure GP context, finishing the context switch
    ;-------------------------------------------------------------------
    ldr x0, =s_ctx
    add sp, x0, xzr
    LoadCtx sp
    ;-------------------------------------------------------------------
    ; Restore SP EL3
    ;-------------------------------------------------------------------
    mrs x0, TPIDR_EL3
    add sp, x0, xzr
    ;-------------------------------------------------------------------
    ; Communicate to secure EL1 to process SMC call from NS
    ;
    ; TODO: This should be #defined somewhere in a generic interface
    ; location between monitor and secure el1
    ;-------------------------------------------------------------------
    mov x0, #SMC_HANDLED_IN_SECURE_EL1
    ;-------------------------------------------------------------------
    ; Ensure all system registers updates are observed
    ;-------------------------------------------------------------------
    dsb sy
    ; ------------------------------------------------------------------
    ; Jump to secure EL1
    ; ------------------------------------------------------------------
    eret
    ; END
fiq_switch_ctx
    ;-------------------------------------------------------------------
    ; Save NS context, restore secure context
    ;-------------------------------------------------------------------
    sub sp, #((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8)
    SaveCtx sp
    sub sp, #((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8)
    add x0, sp, xzr ; save el3_sp
    msr TPIDR_EL3, x0
    ldr x0, =s_ctx
    add sp, x0, xzr
    LoadCtx sp
    mrs x0, TPIDR_EL3 ; restore el3_sp
    add sp, x0, xzr
    ;-------------------------------------------------------------------
    ; Populate FIQ r14 with the instruction that would have been
    ; executed in SVC mode.
    ;
    ; TODO: this only works for EL1 in AArch32
    ;-------------------------------------------------------------------
    mrs x30, ELR_EL3
    ;-------------------------------------------------------------------
    ; Route the FIQ to secure EL1 for processing. Update ELR and SPSR
    ; to return to secure EL1s FIQ vector.
    ;
    ; TODO: this only works for EL1 in AArch32
    ;-------------------------------------------------------------------
    mrs x0, VBAR_EL1
    add x0, #0x1C
    msr ELR_EL3, x0
    mov x0, #DEFAULT_AARCH32_S_EL1_FIQ_SPSR
    msr SPSR_EL3, x0
    ;-------------------------------------------------------------------
    ; Ensure all system registers updates are observed
    ;-------------------------------------------------------------------
    dsb sy
    ;-------------------------------------------------------------------
    ; Return to secure EL1
    ;-------------------------------------------------------------------
    eret
;--------------------------------------------------------------------
; void clear_el1_ns_ctx(void)
;
; Clears all the GP and system registers for the NS side, but doesnt
; clear the return address or return mode.
;--------------------------------------------------------------------
    EXPORT clear_el1_ns_ctx
clear_el1_ns_ctx FUNCTION
    PushTwo x0, x30
    bl get_el3_sp
    sub x0, #(((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) - 0)
    mov x1, #0
    mov x2, #(((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) - 0)
    bl memset
    PopTwo x30, x0
    ret
    ENDFUNC
;--------------------------------------------------------------------
; EL3 using SP from EL0 vectors (not used)
;--------------------------------------------------------------------
    AREA EL3_SP0_S, CODE, READONLY, ALIGN=8
vector_table
el3_sp_el0_synch ; Not using EL0 stack in EL3
    b el3_sp_el0_synch
    AREA EL3_SP0_I, CODE, READONLY, ALIGN=7
el3_sp_el0_irq ; Not using EL0 stack in EL3
    b el3_sp_el0_irq
    AREA EL3_SP0_F, CODE, READONLY, ALIGN=7
el3_sp_el0_fiq ; Not using EL0 stack in EL3
    b el3_sp_el0_fiq
    AREA EL3_SP0_E, CODE, READONLY, ALIGN=7
el3_sp_el0_error ; Not using EL0 stack in EL3
    b el3_sp_el0_error
;--------------------------------------------------------------------
; EL3 using SP from EL3 vectors (monitor data aborts, etc...)
;--------------------------------------------------------------------
    AREA EL3_SP3_S, CODE, READONLY, ALIGN=7
el3_sp_el3_synch ; This can happen due to an illegal
    mrs x3, ESR_EL3
    lsr x4, x3, #26
    and x4, #0x3F
1 b %b1 ; exception return, which is fatal
    AREA EL3_SP3_I, CODE, READONLY, ALIGN=7
el3_sp_el3_irq ; IRQs in EL3 disabled
    b el3_sp_el3_irq
    AREA EL3_SP3_F, CODE, READONLY, ALIGN=7
el3_sp_el3_fiq ; FIQs in EL3 disabled
    b el3_sp_el3_fiq
    AREA EL3_SP3_E, CODE, READONLY, ALIGN=7
el3_sp_el3_error
    b el3_sp_el3_error
;--------------------------------------------------------------------
; Lower EL AArch64 Synchronous
;--------------------------------------------------------------------
    AREA LEL_64_S, CODE, READONLY, ALIGN=7
    ;-------------------------------------------------------------------
    ; Push the parameter and scratch registers to stack.
    ;-------------------------------------------------------------------
    PushSMC
    ;-------------------------------------------------------------------
    ; Confirm call is due to lower EL making a SMC instruction. Do not
    ; corrupt x0, it is used in the caller.
    ;-------------------------------------------------------------------
    mrs x4, ESR_EL3
    lsr x4, #26
    and x4, #0x3F
    cmp x4, #0x17
    beq smc_handler
    cmp x4, #0x13
    beq smc_handler
1 b %b1
;--------------------------------------------------------------------
; Lower EL AArch64 IRQ (Wont be used until secure EL1 is AArch64)
;--------------------------------------------------------------------
    AREA LEL_64_I, CODE, READONLY, ALIGN=7
lower_el_64_irq ; IRQ from AArch64 EL[012]
    b lower_el_64_irq
;--------------------------------------------------------------------
; Updates the saved non-secure context
;
; void set_el3_elr(el3_reg_t addr)
;--------------------------------------------------------------------
    EXPORT set_ns_el3_elr
set_ns_el3_elr FUNCTION
    PushTwo x29, x30
    mov x29, x0
    bl get_el3_sp
    str x29, [x0, #((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) - ((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8))] ; ns_elr_el3
    PopTwo x30, x29
    ret
    ENDFUNC
;--------------------------------------------------------------------
; Updates the saved non-secure context
;
; void set_ns_cpsr(el3_reg_t addr)
;--------------------------------------------------------------------
    EXPORT set_ns_cpsr
set_ns_cpsr FUNCTION
    PushTwo x29, x30
    mov x29, x0
    bl get_el3_sp
    str x29, [x0, #(((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) - ((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8))] ; ns_cpsr
    PopTwo x30, x29
    ret
    ENDFUNC
    ;-------------------------------------------------------------------
    ; If the monitor call was handled in the monitor, unwind the
    ; stack, clear any parameter or scratch registers and return.
    ;-------------------------------------------------------------------
ns_mon_call_return_to_ns
    ;-------------------------------------------------------------------
    ; Unwind stack: this gets the return value from monitor call
    ;-------------------------------------------------------------------
    PopSMC
    ;-------------------------------------------------------------------
    ; Saving and restoring x12 as GP register should not be modified when returning to NS.
    ; Return to NS world
    ;-------------------------------------------------------------------
    msr TPIDR_EL3, x12
    SetNonSecure x12
    mrs x12, TPIDR_EL3
    eret
    ; END
;--------------------------------------------------------------------
; Lower EL AArch64 FIQ (common fiq handler)
;--------------------------------------------------------------------
    AREA LEL_64_F, CODE, READONLY, ALIGN=7
    EXPORT fiq_handler
fiq_handler
    ;-------------------------------------------------------------------
    ; Clear the exclusive monitors
    ;-------------------------------------------------------------------
    clrex
    ;-------------------------------------------------------------------
    ; Make some work registers
    ;-------------------------------------------------------------------
    PushTwo x0, x1
    ;-------------------------------------------------------------------
    ; Set SCR to secure world
    ;-------------------------------------------------------------------
    SetSecure x0
    ;-------------------------------------------------------------------
    ; Did FIQ interrupt secure or non-secure?
    ;-------------------------------------------------------------------
    ReadNSState 0, 1
    ;-------------------------------------------------------------------
    ; FIQ should never interrupt secure because they should be handled
    ; in secure EL1 directly
    ;-------------------------------------------------------------------
    tst x0, #0x001
1 beq %b1
    ;-------------------------------------------------------------------
    ; Unwind stack
    ;-------------------------------------------------------------------
    PopTwo x1, x0
    ;-------------------------------------------------------------------
    ; Context switch is too large to stuff here
    ;-------------------------------------------------------------------
    b fiq_switch_ctx
;--------------------------------------------------------------------
; Lower EL AArch64 Error
;--------------------------------------------------------------------
    AREA LEL_64_E, CODE, READONLY, ALIGN=7
lower_el_64_error ; TODO: what can do this?
    b lower_el_64_error
;--------------------------------------------------------------------
; Lower EL AArch32 Synchronous
;--------------------------------------------------------------------
    AREA LEL_32_S, CODE, READONLY, ALIGN=7
lower_el_32_sync
    ;-------------------------------------------------------------------
    ; Push the parameter registers to stack. See procedure call
    ; standard.
    ;-------------------------------------------------------------------
    PushSMC
    ;-------------------------------------------------------------------
    ; Confirm call is due to lower EL making a SMC instruction. Do not
    ; corrupt x0, it is used in the caller.
    ;-------------------------------------------------------------------
    mrs x4, ESR_EL3
    lsr x4, #26
    and x4, #0x3F
    cmp x4, #0x13
    beq smc_handler
    ;-------------------------------------------------------------------
    ; TODO: Other aborts coming to this vector arent currently handled
    ;-------------------------------------------------------------------
1 b %b1
;--------------------------------------------------------------------
; Lower EL AArch32 IRQ
;--------------------------------------------------------------------
    AREA LEL_32_I, CODE, READONLY, ALIGN=7
    ;-------------------------------------------------------------------
    ; EL3 will not route IRQs to EL3 and will always mask IRQs.
    ;-------------------------------------------------------------------
lower_el_32_irq
    b lower_el_32_irq
    ;-------------------------------------------------------------------
    ; Called from secure EL1 when IRQ happens
    ;-------------------------------------------------------------------
irq_handler
    ;-------------------------------------------------------------------
    ; Clear the exclusive monitors
    ;-------------------------------------------------------------------
    clrex
    ;-------------------------------------------------------------------
    ; Save IRQ exit status
    ;-------------------------------------------------------------------
    ldr x0, =did_irq_exit
    mov x1, #1
    str x1, [x0]
    ;-------------------------------------------------------------------
    ; Exit to NS EL1
    ;-------------------------------------------------------------------
    b irq_exit_to_ns
    ; END
;--------------------------------------------------------------------
; Lower EL AArch32 FIQ
;--------------------------------------------------------------------
    AREA LEL_32_F, CODE, READONLY, ALIGN=7
lower_el_32_fiq
    b lower_el_32_fiq
;--------------------------------------------------------------------
; Lower EL AArch32 Error
;--------------------------------------------------------------------
    AREA LEL_32_E, CODE, READONLY, ALIGN=7
lower_el_32_error ; TODO: what can do this?
    b lower_el_32_error
;--------------------------------------------------------------------
; Return the value of TTBR0 saved in NS context
; p_addr_t get_ns_el1_ttbr0(void)
;--------------------------------------------------------------------
    EXPORT get_ns_el1_ttbr0
get_ns_el1_ttbr0 FUNCTION
    PushTwo x1, x30
    bl get_el3_sp
    ldr x0, [x0, #(((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) - ((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8))] ; ns_ttbr0
    PopTwo x30, x1
    ret
    ENDFUNC
;--------------------------------------------------------------------
; Return the value of x30 saved in NS context
; el3_reg_t get_ns_el1_x30(void)
;--------------------------------------------------------------------
    EXPORT get_ns_el1_x30
get_ns_el1_x30 FUNCTION
    PushTwo x1, x30
    bl get_el3_sp
    sub x0, #((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) ; can't do in one op due to limitations
                                    ; on offsets
    ldr x0, [x0, #((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8)] ; ns_x30
    PopTwo x30, x1
    ret
    ENDFUNC
;--------------------------------------------------------------------
; Return the monitor vector address
; el3_reg_t get_el3_vector_addr(void)
;--------------------------------------------------------------------
    EXPORT get_el3_vector_addr
get_el3_vector_addr FUNCTION
    ldr x0, =vector_table
    ret
    ENDFUNC
    AREA MON_CODE_ER_POST_VBAR, CODE, READONLY, ALIGN=3
;--------------------------------------------------------------------
; Configure the EL3 stack. Called during cold and warm boot.
; void get_el3_sp(void)
;--------------------------------------------------------------------
    EXPORT get_el3_sp
get_el3_sp FUNCTION
    get_el3_sp_inline x0, x1, x2
    ret
    ENDFUNC
;--------------------------------------------------------------------
; el1_smp_notifier_t el1_smp_notifier_asm(p_addr_t addr,
; el1_smp_notifier_cmd_t cmd, uint32 arg0, uint32 arg1)
;--------------------------------------------------------------------
    EXPORT el1_smp_notifier_asm
el1_smp_notifier_asm FUNCTION
    ;-------------------------------------------------------------------
    ; Save the APCS callee-saved registers on stack before making a
    ; switch to EL1. EL1 doesnt know about its responsibility to
    ; save AArch64 callee-saved registers, since it is AArch32.
    ;-------------------------------------------------------------------
    PushAArch64CSRegs
    ;-------------------------------------------------------------------
    ; Save the ELR outside the save macro. Macro does not modify
    ; elr_el3 because s_elr_el3 can't be left to point to the notifier
    ; handler and the save/load should be symmetric.
    ;-------------------------------------------------------------------
    mrs x19, ELR_EL3
    mov x20, #0xABAB
    PushTwo x19, x20
    ;-------------------------------------------------------------------
    ; Save NS system register context. As long as there is a PC
    ; notifier in secure EL1 the system registers will need to be
    ; swapped from NS to S and back to NS if PC falls through.
    ;-------------------------------------------------------------------
    SaveSysRegsCtx sp, x19, x20
    ;-------------------------------------------------------------------
    ; Load secure system registers from secure context
    ;-------------------------------------------------------------------
    ldr x19, =s_ctx
    add x19, #(((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8)
    LoadSysRegsCtx x19, x20, x21
    ;-------------------------------------------------------------------
    ; Make sure secure EL1 masks the F, A and I bits during the PC
    ; notifier call
    ;-------------------------------------------------------------------
    mrs x19, SPSR_EL3
    mov x20, #0x040:OR:0x080:OR:0x100
    orr x19, x20
    msr SPSR_EL3, x19
    ;-------------------------------------------------------------------
    ; Set the ELR to the notifier address
    ;-------------------------------------------------------------------
    msr ELR_EL3, x0
    ;-------------------------------------------------------------------
    ; Move the params to x0 through x2 and wipe x3
    ;-------------------------------------------------------------------
    mov x0, x1
    mov x1, x2
    mov x2, x3
    mov x3, xzr
    ;-------------------------------------------------------------------
    ; Save x0 through x3
    ;-------------------------------------------------------------------
    PushTwo x0, x1
    PushTwo x2, x3
    ;-------------------------------------------------------------------
    ; Clear all registers before calling secure EL1 notifier
    ;-------------------------------------------------------------------
    WipeAllGPRegs
    ;-------------------------------------------------------------------
    ; Restore x0 through x3
    ;-------------------------------------------------------------------
    PopTwo x3, x2
    PopTwo x1, x0
    ;-------------------------------------------------------------------
    ; Ensure all system registers updates are observed
    ;-------------------------------------------------------------------
    dsb sy
    ;-------------------------------------------------------------------
    ; Switch to secure EL1 to call the notifier
    ;-------------------------------------------------------------------
    eret
;-------------------------------------------------------------------
; Conclusion of el1_smp_notifier after returning from secure EL1
;-------------------------------------------------------------------
el1_smp_notifier_end
    ;-------------------------------------------------------------------
    ; Unconditionally unwind the SMC stack
    ;-------------------------------------------------------------------
    PopSMC
    ;-------------------------------------------------------------------
    ; Load NS system registers from secure context
    ;-------------------------------------------------------------------
    LoadSysRegsCtx sp, x19, x20
    ;-------------------------------------------------------------------
    ; Load the ELR outside the load macro. Macros do not modify
    ; elr_el3 because s_elr_el3 can't be clobbered.
    ;-------------------------------------------------------------------
    PopTwo x20, x19
    msr ELR_EL3, x19
    ;-------------------------------------------------------------------
    ; Pop remaining callee-saved registers from stack. These were
    ; placed on the stack in el1_pc_notifier.
    ;-------------------------------------------------------------------
    PopAArch64CSRegs
    ;-------------------------------------------------------------------
    ; Return value comes in x1. Move to x0 and clear x1.
    ;-------------------------------------------------------------------
    mov x0, x1
    mov x1, xzr
    ;-------------------------------------------------------------------
    ; Ensure all system registers updates are observed
    ;-------------------------------------------------------------------
    dsb sy
    ret
    ENDFUNC
;--------------------------------------------------------------------
; Gets the address of the NS context
; phys_addr_t get_ns_context_addr(void)
;--------------------------------------------------------------------
    EXPORT get_ns_context_addr
get_ns_context_addr FUNCTION
    PushTwo x1, x30
    bl get_el3_sp
    sub x0, #((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8)
    PopTwo x30, x1
    ret
    ENDFUNC
;--------------------------------------------------------------------
; Gets the NS state
; el3_reg_t get_ns_state(void)
;--------------------------------------------------------------------
    EXPORT get_ns_state
get_ns_state FUNCTION
    ReadNSState 0, 1
    ret
    ENDFUNC
;--------------------------------------------------------------------
; Sets values to be returned to NS side
; void set_ns_return_registers(el3_reg_t reg0,
; el3_reg_t reg1,
; el3_reg_t reg2,
; el3_reg_t reg3)
;--------------------------------------------------------------------
    EXPORT set_ns_return_registers
set_ns_return_registers FUNCTION
    ;-------------------------------------------------------------------
    ; Save LR and x0, x1, x2 and x3 params
    ;-------------------------------------------------------------------
    PushTwo x30, x29
    PushTwo x3, x2
    PushTwo x1, x0
    ;-------------------------------------------------------------------
    ; Get ns_x0 address in x4
    ;-------------------------------------------------------------------
    bl get_el3_sp
    sub x4, x0, #(((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) - 0)
    ;-------------------------------------------------------------------
    ; Restore params from stack
    ;-------------------------------------------------------------------
    PopTwo x0, x1
    PopTwo x2, x3
    ;-------------------------------------------------------------------
    ; Update ns_x0, ns_x1, ns_x2, ns_x3 using params
    ;-------------------------------------------------------------------
    PushTwoA x4, x0, x1
    PushTwoA x4, x2, x3
    ;-------------------------------------------------------------------
    ; Restore LR
    ;-------------------------------------------------------------------
    PopTwo x29, x30
    ret
    ENDFUNC
;===================================================================
; Secure Context
;===================================================================
;
; Memory area for storing the secure processor context. Only one call
; context is supported, hence only one CPU at any given time can
; execute secure side code. That is, the secure side is not
; re-entrant.
;
;--------------------------------------------------------------------
; See Table D1-61 for mappings from AArch64 to AArch32
;--------------------------------------------------------------------
    AREA |S_CONTEXT|, DATA, READWRITE, ALIGN=2
s_ctx SPACE ((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8) + 0x8)
;--------------------------------------------------------------------
; The monitor stack must be at least cache-line aligned and the size
; must be a multiple of the cache line size due to cache maintenance
; requirements when calling functions that enable/disable the MMU and
; data caching. Aligning on page boundary should be a good hack for
; the forseeable future, but the right solution is to define this in a
; target-specific manner.
; TODO: define monitor alignment in target-specific manner.
;--------------------------------------------------------------------
    AREA |MONITOR_STACK|, DATA, READWRITE, ALIGN=12
mon_stack_end SPACE (0x800 * 8)
mon_stack
    AREA |MONITOR_DATA|, DATA, READWRITE, ALIGN=4
ns_state SPACE 8
    ALIGN 8 ; variables are aligned on all targets
did_irq_exit SPACE 0x8
valid_ns_ctx SPACE 0x8
smc_lock SPACE (0x8)
    END
