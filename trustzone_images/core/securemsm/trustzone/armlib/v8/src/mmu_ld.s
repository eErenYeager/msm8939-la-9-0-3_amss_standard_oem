;============================================================================
;
;                  Long Descriptor MMU Functions
;
; Copyright 2011 by Qualcomm Technologies, Incorporated.  All Rights Reserved
;============================================================================

;============================================================================
;
;                       EDIT HISTORY FOR MODULE
;
; $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/armlib/v8/src/mmu_ld.s#1 $
;
; This section contains comments describing changes made to the module.
; Notice that changes are listed in reverse chronological order.
;
;
; when      who     what, where, why
; --------  ---     ---------------------------------------------------------
; 08/13/13   pre     Initial revision.
;============================================================================

;============================================================================
;                             MODULE IMPORTS
;============================================================================
    IMPORT  mair0
    IMPORT  mair1

;============================================================================
;                             MODULE EXPORTS
;============================================================================
    EXPORT  mmu_set_ttbrs
    EXPORT  mmu_enable_tex_remap
    EXPORT  tzbsp_vtop

    PRESERVE8
    AREA    ARM_MMU, CODE, READONLY, ALIGN=3


;============================================================================
; mmu_set_ttbrs
;
; ARGS
;   r0 low 32 bits of base
;   r1 high 32 bits of base
;
; DESCRIPTION
;   Sets a value that defines the Translation Table Base boundary that will
;   be used by TTB Registers, and loads Translation Table Base Register 0
;   with the base address of page table.
;   Default value of N after power up is 0 for backward compatible. Any
;   change to the Translation Table Base Register 0 needs to be paired with
;   write to Translation Table Base Control Register.
;============================================================================
mmu_set_ttbrs  FUNCTION
    ; TODO: make memory attributes cacheable
    ; TTBCR - TTB Control Register
    ;   EAE IMP SH1 ORGN1 IRGN1 EPD1 A1 U/S T1SZ
    ; 0b  1   0  11    01    10    0  0 000  000
    ;   U/S SH0 ORGN0 IRGN0 EPD0  U/S T0SZ
    ;    00  11    01    10    0 0000  000 = 0xB6003600
    mov     r2, #0x3600
    movt    r2, #0xB600
    mcr     p15, 0, r2, c2, c0, 2

    ; TTBR0 - Translation Table Base Register 0
    ;         U/S     ASID      U/S BADDR[39:x] U/S[x:0]
    ; 0b 00000000 00000000 00000000        addr
    mov     r1, #0x0
    mcrr    p15, 0, r0, r1, c2

    bx      lr
    ENDFUNC

;============================================================================
; Enable TeX Remap
;   tex remap scheme described in tzbsp_mmu_config.h
;============================================================================
mmu_enable_tex_remap FUNCTION
    ldr     r0, =mair0
    ldr     r0, [r0]
    WCP15_MAIR0 r0

    ldr     r0, =mair1
    ldr     r0, [r0]
    WCP15_MAIR1 r0

    bx      lr
    ENDFUNC

    END
