#ifndef SYS_REGS_H
#define SYS_REGS_H

/**
   @file sys_regs.h
   @brief Defines ARM architected system register accessor functions for TZ
*/
/*===========================================================================
   Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/armlib/v8/inc/sys_regs.h#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $


when       who      what, where, why
--------   ---     ------------------------------------------------------------
=============================================================================*/
#include "memory_defs.h"

el3_reg_t get_hcr_el2(void);
void      set_hcr_el2(el3_reg_t hcr_el2);

el3_reg_t get_sctlr_el1(void);
void      set_sctlr_el1(el3_reg_t sctlr_el1);

void      el3_set_scr_s(void);
void      el3_set_scr_ns(void);

void      set_cntfrq_el0(el3_reg_t cntfrq_el0);

uint32    get_cptr_el3(void);
void      set_cptr_el3(uint32 cptr_el3);

el3_reg_t get_sp_el2(void);
el3_reg_t get_elr_el2(void);
el3_reg_t get_spsr_el2(void);

el3_reg_t get_mpidr_el1(void);

void      set_mdcr_el3(el3_reg_t mdcr_el3);
void      set_sder32_el3(el3_reg_t sder32_el3);

void      set_cntp_ctl_el0(el3_reg_t cntp_ctl_el0);
void      set_cntp_cval_el0(el3_reg_t cntp_cval_el0);
el3_reg_t get_cntpct_el0(void);
void      set_cntps_cval_el1(el3_reg_t);
void      set_cntps_ctl_el1(el3_reg_t);

#endif /* SYS_REGS_H */
