#ifndef CACHE_H
#define CACHE_H

/**
   @file sys_regs.h
   @brief Defines ARM architected system register accessor functions for TZ
*/
/*===========================================================================
   Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/armlib/v8/inc/cache.h#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $


when       who      what, where, why
--------   ---     ------------------------------------------------------------
=============================================================================*/

#include "memory_defs.h"

void clean_and_inval_all_dcache(void);
void clean_all_dcache(void);
void inval_all_dcache(void);

void clean_and_inval_all_dcache_louis(void);

void clean_and_inval_dcache_region(v_addr_t vaddr, size_t len);
void clean_dcache_region(v_addr_t vaddr, size_t len);
void inval_dcache_region(v_addr_t vaddr, size_t len);

void enable_el3_icache(void);
void enable_el3_dcache(void);

void disable_el3_icache(void);
void disable_el3_dcache(void);

void disable_el1_dcache(void);

boolean is_el3_icache_enabled(void);
boolean is_el3_dcache_enabled(void);

#endif /* CACHE_H */
