#ifndef _SYS_FIELDS_H_
#define _SYS_FIELDS_H_

/* MPIDR */
#define MPIDR_EL1_AFF0_MSK  0x000000FF
#define MPIDR_EL1_AFF0_SHFT 0x0
#define MPIDR_EL1_AFF1_MSK  0x0000FF00
#define MPIDR_EL1_AFF1_SHFT 0x8
#define MPIDR_EL1_AFF2_MSK  0x00FF0000
#define MPIDR_EL1_AFF2_SHFT 0x10
#define MPIDR_EL1_MT_MSK    0x01000000
#define MPIDR_EL1_MT_SHFT   0x18

/* SPSR */
/* CPSR Processing Modes M[4:0] (CPSR[M]) bits */
#define SPSR_MODE_EL0t 0x00
#define SPSR_MODE_EL1t 0x04 /* SP_EL0 */
#define SPSR_MODE_EL1h 0x05 /* SP_EL1 */
#define SPSR_MODE_EL2t 0x08 /* SP_EL0 */
#define SPSR_MODE_EL2h 0x09 /* SP_EL2 */
#define SPSR_MODE_EL3t 0x0C /* SP_EL0 */
#define SPSR_MODE_EL3h 0x0D /* SP_EL3 */
#define SPSR_MODE_MSK  0x0F
#define SPSR_MODE_SHFT 0x00
#define SPSR_F_BIT     0x40
#define SPSR_I_BIT     0x80
#define SPSR_D_BIT     0x200

#define SPSR_32_MODE_USR 0x000
#define SPSR_32_MODE_FIQ 0x001
#define SPSR_32_MODE_IRQ 0x002
#define SPSR_32_MODE_SVC 0x003
#define SPSR_32_MODE_MON 0x006
#define SPSR_32_MODE_ABT 0x007
#define SPSR_32_MODE_HYP 0x00A
#define SPSR_32_MODE_UND 0x00B
#define SPSR_32_MODE_SYS 0x00F
#define SPSR_32_M_BIT    0x010   /* RW of exception */
#define SPSR_32_T_BIT    0x020   /* Thumb execution state bit */
#define SPSR_32_F_BIT    0x040   /* FIQ mask bit */
#define SPSR_32_I_BIT    0x080   /* IRQ mask bit */
#define SPSR_32_A_BIT    0x100   /* SError mask bit */
#define SPSR_32_D_BIT    0x200   /* Debug exception mask bit */



/* DAIF bits */
#define DAIF_D_BIT 0x200   /* Debug exception mask bit */
#define DAIF_A_BIT 0x100   /* SError mask bit */
#define DAIF_I_BIT 0x080   /* IRQ mask bit */
#define DAIF_F_BIT 0x040   /* FIQ mask bit */

/* SCR (Secure Configuration Register) bits */
#define SCR_NS_BIT  0x001   /* Non-Secure (NS) bit */
#define SCR_IRQ_BIT 0x002   /* IRQ bit */
#define SCR_FIQ_BIT 0x004   /* FIQ bit */
#define SCR_EA_BIT  0x008   /* EA bit */
#define SCR_AW_BIT  0x020   /* A Bit writable (AW) bit */
#define SCR_SMD_BIT 0x040   /* Disables SMC */
#define SCR_HCE_BIT 0x100   /* Enables HVC */
#define SCR_SIF_BIT 0x200   /* Enforce secure instruction fetch */
#define SCR_RW_BIT  0x400   /* Register width for lower EL */

/* HCR (Hypervisor Configuration Register) bits */
#define HCR_RW_BIT 0x80000000  /* Register width for lower EL */

/* SCTLR_EL1 bits */
#define SCTLR_EL1_M_BIT       0x00000001
#define SCTLR_EL1_A_BIT       0x00000002
#define SCTLR_EL1_C_BIT       0x00000004
#define SCTLR_EL1_CP15BEN_BIT 0x00000020
#define SCTLR_EL1_THEE_BIT    0x00000040
#define SCTLR_EL1_ITD_BIT     0x00000080
#define SCTLR_EL1_SED_BIT     0x00000100
#define SCTLR_EL1_I_BIT       0x00001000
#define SCTLR_EL1_V_BIT       0x00002000
#define SCTLR_EL1_NTWI_BIT    0x00010000
#define SCTLR_EL1_NTWE_BIT    0x00040000
#define SCTLR_EL1_WXN_BIT     0x00080000
#define SCTLR_EL1_UWXN_BIT    0x00100000
#define SCTLR_EL1_EE_BIT      0x02000000
#define SCTLR_EL1_TRE_BIT     0x10000000
#define SCTRL_EL1_AFE_BIT     0x20000000
#define SCTLR_EL1_TE_BIT      0x40000000

/* SCTLR_EL3 bits */
#define SCTLR_EL3_M_BIT    0x00000001
#define SCTLR_EL3_A_BIT    0x00000002
#define SCTLR_EL3_A_SHFT   0x1
#define SCTLR_EL3_C_BIT    0x00000004
#define SCTLR_EL3_C_SHFT   0x2
#define SCTLR_EL3_SA_BIT   0x00000008
#define SCTLR_EL3_SA_SHFT  0x3
#define SCTLR_EL3_I_BIT    0x00001000
#define SCTLR_EL3_I_SHFT   0xC
#define SCTLR_EL3_WXN_BIT  0x00080000
#define SCTLR_EL3_WXN_SHFT 0x13
#define SCTLR_EL3_EE_BIT   0x02000000
#define SCTLR_EL3_EE_SHFT  0x19
#define SCTLR_EL3_DEFAULT  (SCTLR_EL3_M_BIT:OR:SCTLR_EL3_C_BIT:OR:SCTLR_EL3_I_BIT:OR:SCTLR_EL3_WXN_BIT)

/* CTR_EL0 bits */

/* CLIDR_EL1 bits */
#define CLIDR_LOUIS_MSK  0x00E00000
#define CLIDR_LOUIS_SHFT 21
#define CLIDR_LOC_MSK    0x07000000
#define CLIDR_LOC_SHFT   24
#define CLIDR_CTYPE_MSK  0x00000007

#define CLIDR_CTYPE_NO_CACHE 0
#define CLIDR_CTYPE_I_ONLY   1
#define CLIDR_CTYPE_D_ONLY   2
#define CLIDR_CTYPE_I_AND_D  3
#define CLIDR_CTYPE_UNIFIED  4

/* CSSELR_EL1 bits */
#define CSSELR_IND_BIT  1
#define CSSELR_LVL_BMSK 0x7
#define CSSELR_LVL_SHFT 1

/* CCSIDR_EL1 bits */
#define CCSIDR_LINE_SZ_BMSK   0x7
#define CCSIDR_LINE_SZ_OFFST  0x4 /* offset to get bytes, not words */
#define CCSIDR_NUM_WAYS_BITS  10
#define CCSIDR_NUM_WAYS_SHFT  3
#define CCSIDR_NUM_WAYS_OFFST 1
#define CCSIDR_NUM_SETS_BITS  15
#define CCSIDR_NUM_SETS_SHFT  13
#define CCSIDR_NUM_SETS_OFFST 1

/* ESR_EL3 bits */
#define ESR_EL3_EC_UNKNOWN          0x00
#define ESR_EL3_EC_WFE_WFI          0x01
#define ESR_EL3_EC_CP15_ACCESS      0x03
#define ESR_EL3_EC_CP14_ACCESS      0x05
#define ESR_EL3_EC_CP14_ACCESS_2    0x06
#define ESR_EL3_EC_SIMD_FP          0x07
#define ESR_EL3_EC_CP14_ACCESS_3    0x0C
#define ESR_EL3_EC_IL               0x0E
#define ESR_EL3_EC_SMC_32           0x13
#define ESR_EL3_EC_SVC_64           0x15
#define ESR_EL3_EC_HVC_64           0x16
#define ESR_EL3_EC_SMC_64           0x17
#define ESR_EL3_EC_SYSREG_64        0x18
#define ESR_EL3_EC_I_ABORT_LOWER_EL 0x20
#define ESR_EL3_EC_I_ABORT_THIS_EL  0x21
#define ESR_EL3_EC_PC_ALIGNMENT     0x22
#define ESR_EL3_EC_D_ABORT_LOWER_EL 0x24
#define ESR_EL3_EC_D_ABORT_THIS_EL  0x25
#define ESR_EL3_EC_SP_ALIGNMENT     0x26
#define ESR_EL3_EC_FP_EXCEPTION     0x2C
#define ESR_EL3_EC_SERROR_INT       0x2F
#define ESR_EL3_EC_BRK_64           0x3C
#define ESR_EL3_EC_SHFT             26
#define ESR_EL3_EC_BMSK             0x3F

/* SDER32_EL3 bits */
#define SDER32_EL3_SUIDEN_BIT       1
#define SDER32_EL3_SUNIDEN_BIT      2

/* MDCR_EL3 bits */
#define MDCR_EL3_TPM_BIT            0x00000020
#define MDCR_EL3_TDA_BIT            0x00000200
#define MDCR_EL3_TDOSA_BIT          0x00000400
#define MDCR_EL3_SPD32_BMSK         0xC
#define MDCR_EL3_SPD32_SHFT         14
#define MDCR_EL3_SDD_BIT            0x00010000
#define MDCR_EL3_SPME_BIT           0x00020000
#define MDCR_EL3_EDAD_BIT           0x00100000
#define MDCR_EL3_EPMAD_BIT          0x00200000

#define MDCR_EL3_SPD32_LEGACY       0x0
#define MDCR_EL3_SPD32_SPDD         0x2
#define MDCR_EL3_SPD32_SPDE         0x3

/* TCR_EL3 bits */
#define TCR_EL3_T0SZ_BMSK           0x0000003F
#define TCR_EL3_T0SZ_SHFT           0
#define TCR_EL3_IRGN0_BMSK          0x00000300
#define TCR_EL3_IRGN0_SHFT          8
#define TCR_EL3_ORGN0_BMSK          0x00000C00
#define TCR_EL3_ORGN0_SHFT          10
#define TCR_EL3_SH0_BMSK            0x00003000
#define TCR_EL3_SH0_SHFT            12
#define TCR_EL3_TG0_BMSK            0x0000C000
#define TCR_EL3_TG0_SHFT            14
#define TCR_EL3_PS_BMSK             0x00070000
#define TCR_EL3_PS_SHFT             16
#define TCR_EL3_TBI_BIT             0x00100000
#define TCR_EL3_TBI_SHFT            20

#define TCR_EL3_SET_VAS(T0SZ)       (64 - T0SZ)

#define TCR_EL3_RGN_NC              0x0
#define TCR_EL3_RGN_WB_WA           0x1
#define TCR_EL3_RGN_WT              0x2
#define TCR_EL3_RGN_WB_NW           0x3

#define TCR_EL3_SH_NS               0x0
#define TCR_EL3_SH_OS               0x2
#define TCR_EL3_SH_IS               0x3

#define TCR_EL3_PS_32_BIT           0x0
#define TCR_EL3_PS_36_BIT           0x1
#define TCR_EL3_PS_40_BIT           0x2
#define TCR_EL3_PS_42_BIT           0x3
#define TCR_EL3_PS_44_BIT           0x4
#define TCR_EL3_PS_48_BIT           0x5

#define TCR_EL1_IPS_SHFT            32
#define TCR_EL1_TBI0_SHFT           37

/* FPEXC32_EL2 bits */
#define FPEXC32_EL2_EN_BIT              0x40000000
#define FPEXC32_EL2_DEX_BIT             0x20000000
#define FPEXC32_EL2_TFV_BIT             0x04000000
#define FPEXC32_EL2_IDF_BIT             0x00000080
#define FPEXC32_EL2_IXF_BIT             0x00000010
#define FPEXC32_EL2_UFF_BIT             0x00000008
#define FPEXC32_EL2_OFF_BIT             0x00000004
#define FPEXC32_EL2_DZF_BIT             0x00000002
#define FPEXC32_EL2_IOF_BIT             0x00000001


/* FPEXC bits */
#define FPEXC_EX_BIT              0x80000000
#define FPEXC_EN_BIT              0x40000000

#endif
