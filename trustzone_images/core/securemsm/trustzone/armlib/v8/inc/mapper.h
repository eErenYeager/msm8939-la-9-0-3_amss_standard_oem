#include <comdef.h>
#include "tt_ld.h"

int init_tt_entries
(
  mem_block_t* default_memory_map
);

int mmu_map
(
  v_addr_t v_addr,
  p_addr_t p_addr,
  size_t   sz_in_kb,
  tz_cfg_t msk
);

int mmu_map_block
(
  const mem_block_t* map
);

void mmu_remove_tz_mapping
(
  uint32 *sbl_l1_pt
);

void mmu_map_init(l1_tt_t*     l1_tt_ptr,
                  l2_tt_t*     l2_tt_pool_ptr,
                  unsigned int l2_pool_len,
                  l3_tt_t*     l3_tt_pool_ptr,
                  unsigned int l3_pool_len);

el3_reg_t get_el3_ttbr0(void);
el3_reg_t get_el3_mair(void);
