/**
   @file mmu.h
   @brief EL3 MMU Control Routines
 */
/*===========================================================================
   Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/armlib/v8/inc/mmu.h#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $

when       who      what, where, why
--------   ---      ------------------------------------
05/29/14   pre      Initial revision
===========================================================================*/

#include "memory_defs.h"

void warm_init_and_enable_el3_mmu(p_addr_t ttbr_addr,
                                  el3_reg_t mair_el3,
                                  el3_reg_t tcr_el3);
void cold_init_and_enable_el3_mmu(p_addr_t ttbr_addr,
                                  el3_reg_t mair_el3,
                                  el3_reg_t tcr_el3);
void disable_el3_mmu(void);
void enable_el3_mmu(void);
