#ifndef UTIL_H
#define UTIL_H

#ifndef _ARM_ASM_
#include "comdef.h"
#include "proc_types.h"
#include "memory_defs.h"

/* TODO: move these elsewhere */
#define TZ_VAS_LAST_ADDRESS 0xFFFFFFFF
#define TZBSP_CODE_BASE 0xFE801000
#define TZBSP_MAX_IMAGE_SIZE 0x40000

#define ARRAY_SIZE(array) (sizeof(array) / sizeof(array[0]))
#define RETCHK(xx) if ((xx) != E_SUCCESS) {return (xx);}
#define FLOOR_4K(xx) ((xx) & ~0xFFF)
#define ROUNDUP(value, round) (0 == value % round ?			\
                               value :                  \
                               value + (round - (value % round)))

typedef struct
{
  volatile uint32 lock __attribute__((aligned(ERG_ALIGNMENT)));
} spinlock_t;


/* uint32 read_l2_data(uint32 offset); */

el3_reg_t int_disable_all(void);
void      int_restore(el3_reg_t flags);

void spinlock_obtain(spinlock_t* lock);
void spinlock_release(spinlock_t* lock);

#else /* #ifndef _ARM_ASM_ */

#include "sys_fields.h"

/* -----------------------------------------------------------------------------
 * MACRO: SetSecure
 * ---------------------------------------------------------------------------*/
    MACRO
    SetSecure $rx
        /* Set up SCR for Secure world.
         * IRQ taken by the OS directly in the secure world
         * FIQ taken by the OS directly in the secure world
         * SError taken by the OS directly in the secure world
         */
        mov     $rx, xzr
        msr     SCR_EL3, $rx
        isb
    MEND

/* -----------------------------------------------------------------------------
 * MACRO: SetNonSecure
 * ---------------------------------------------------------------------------*/
/* TODO: must change for targets where EL2/EL1 are not AArch64 */
    MACRO
    SetNonSecure $rx
      /* Set up SCR for Normal world.
       * FIQ - monitor, not maskable by NS.
       * IRQ - NS, maskable by NS.
       * EA  - NS, maskable by NS.
       */
        mov     $rx, #SCR_AW_BIT:OR:SCR_NS_BIT:OR:SCR_FIQ_BIT:OR:SCR_RW_BIT:OR:SCR_HCE_BIT
        msr     SCR_EL3, $rx
        isb
    MEND


/* -----------------------------------------------------------------------------
 * MACRO: PushTwo
 * ---------------------------------------------------------------------------*/
    MACRO
    PushTwo $x0, $x1
        stp     $x1, $x0, [sp, #-0x10]!
    MEND

/* -----------------------------------------------------------------------------
 * MACRO: PopTwo
 * ---------------------------------------------------------------------------*/
    MACRO
    PopTwo $x0, $x1
        ldp     $x0, $x1, [sp], #0x10
    MEND

/* -----------------------------------------------------------------------------
 * MACRO: PushAArch32NPRegs
 * ---------------------------------------------------------------------------*/
    MACRO
    PushAArch32NPRegs
        PushTwo x5,  x6
        PushTwo x7,  x8
        PushTwo x9,  x10
        PushTwo x11, x12
        PushTwo x13, x14
        PushTwo x15, x16
        PushTwo x17, x18
        PushTwo x19, x20
        PushTwo x21, x22
        PushTwo x23, x24
        PushTwo x25, x26
        PushTwo x27, x28
        PushTwo x29, x30
    MEND

/* -----------------------------------------------------------------------------
 * MACRO: PopAArch32NPRegs
 * ---------------------------------------------------------------------------*/
    MACRO
    PopAArch32NPRegs
        PopTwo x30, x29
        PopTwo x28, x27
        PopTwo x26, x25
        PopTwo x24, x23
        PopTwo x22, x21
        PopTwo x20, x19
        PopTwo x18, x17
        PopTwo x16, x15
        PopTwo x14, x13
        PopTwo x12, x11
        PopTwo x10, x9
        PopTwo x8,  x7
        PopTwo x6,  x5
    MEND

/* -----------------------------------------------------------------------------
 * MACRO: PushAArch64CSRegs
 * ---------------------------------------------------------------------------*/
/* Push AArch64 Callee-saved registers */
    MACRO
    PushAArch64CSRegs
        PushTwo x19, x20
        PushTwo x21, x22
        PushTwo x23, x24
        PushTwo x25, x26
        PushTwo x27, x28
        PushTwo x29, x30
    MEND

/* -----------------------------------------------------------------------------
 * MACRO: PopAArch64CSRegs
 * ---------------------------------------------------------------------------*/
/* Pop AArch64 Callee-saved registers */
    MACRO
    PopAArch64CSRegs
        PopTwo x30, x29
        PopTwo x28, x27
        PopTwo x26, x25
        PopTwo x24, x23
        PopTwo x22, x21
        PopTwo x20, x19
    MEND

/* -----------------------------------------------------------------------------
 * MACRO: PushAArch64PSRegs
 * ---------------------------------------------------------------------------*/
/* Push AArch64 Param and Scratch Registers.  The order of registers
 * is important due to assumptions of context structure on the stack.
 */
    MACRO
    PushAArch64PSRegs
        PushTwo x19, x18
        PushTwo x17, x16
        PushTwo x15, x14
        PushTwo x13, x12
        PushTwo x11, x10
        PushTwo x9,  x8
        PushTwo x7,  x6
        PushTwo x5,  x4
        PushTwo x3,  x2
        PushTwo x1,  x0
    MEND

/* -----------------------------------------------------------------------------
 * MACRO: PopAArch64PSRegs
 * ---------------------------------------------------------------------------*/
    MACRO
    PopAArch64PSRegs
        PopTwo  x0,  x1
        PopTwo  x2,  x3
        PopTwo  x4,  x5
        PopTwo  x6,  x7
        PopTwo  x8,  x9
        PopTwo  x10, x11
        PopTwo  x12, x13
        PopTwo  x14, x15
        PopTwo  x16, x17
        PopTwo  x18, x19
    MEND

/* -----------------------------------------------------------------------------
 * MACRO: WipeParamRegs
 * ---------------------------------------------------------------------------*/
/* Wipes the parameter registers for AArch64 */
    MACRO
    WipeParamRegs
        mov     x0,  #0
        mov     x1,  #0
        mov     x2,  #0
        mov     x3,  #0
        mov     x4,  #0
        mov     x5,  #0
        mov     x6,  #0
        mov     x7,  #0
        mov     x8,  #0
    MEND

/* -----------------------------------------------------------------------------
 * MACRO: WipeScratchRegs
 * ---------------------------------------------------------------------------*/
/* Wipes the scratch registers for AArch64 */
    MACRO
    WipeScratchRegs
        mov     x9,  #0
        mov     x10, #0
        mov     x11, #0
        mov     x12, #0
        mov     x13, #0
        mov     x14, #0
        mov     x15, #0
        mov     x16, #0
        mov     x17, #0
        mov     x18, #0
    MEND

/* -----------------------------------------------------------------------------
 * MACRO: WipeAllGPRegs
 * ---------------------------------------------------------------------------*/
/* Wipes all AArch64 GP registers */
    MACRO
    WipeAllGPRegs
        WipeParamRegs
        WipeScratchRegs
        mov     x19, #0
        mov     x20, #0
        mov     x21, #0
        mov     x22, #0
        mov     x23, #0
        mov     x24, #0
        mov     x25, #0
        mov     x26, #0
        mov     x27, #0
        mov     x28, #0
        mov     x29, #0
        mov     x30, #0
    MEND

/* -----------------------------------------------------------------------------
 * MACRO: CorruptMonRegs
 * ---------------------------------------------------------------------------*/
/* Use in testing to set all the registers to something monitor-specific */
    MACRO
    CorruptMonRegs
        mov     x0,  #0xA
        mov     x1,  #0xB
        mov     x2,  #0xC
        mov     x3,  #0xD
        mov     x4,  #0xE
        mov     x5,  #0xF
        mov     x6,  #0xA
        mov     x7,  #0xB
        mov     x8,  #0xC
        mov     x9,  #0xD
        mov     x10, #0xE
        mov     x11, #0xF
        mov     x12, #0xA
        mov     x13, #0xB
        mov     x14, #0xC
        mov     x15, #0xD
        mov     x16, #0xE
        mov     x17, #0xF
        mov     x18, #0xA
        mov     x19, #0xB
        mov     x20, #0xC
        mov     x21, #0xD
        mov     x22, #0xE
        mov     x23, #0xF
        mov     x24, #0xA
        mov     x25, #0xB
        mov     x26, #0xC
        mov     x27, #0xD
        mov     x28, #0xE
        mov     x29, #0xF
        mov     x30, #0xA
    MEND

/* -----------------------------------------------------------------------------
 * MACRO: SpinlockObtain
 * ---------------------------------------------------------------------------*/
/* Multicore safe spinlock implementation. Waits until the monitor lock
   is cleared and the lock is successfully claimed. */
  MACRO
  SpinlockObtain $lock_addr, $cpu, $work
    mrs     $cpu, MPIDR_EL1      /* Current MPIDR. */
    and     $cpu, $cpu, #0xFF    /* MPIDR[AFFL0] is the CPU number. */
    add     $cpu,  #0x1          /* (cpu num + 1) = TZ is locked */
42  ldaxr   $work, [$lock_addr]  /* Load the current lock status. */
    cbnz    $work, %b42          /* Is TZ locked? */
    stlxr   $work, $cpu, [$lock_addr] /* Not locked: try to claim the lock. */
    cbnz    $work, %b42          /* Not locked: Did we get the lock? */
    dmb     sy                   /* Ensure other observers see lock claim. */
  MEND

/* -----------------------------------------------------------------------------
 * MACRO: SpinlockRelease
 * ---------------------------------------------------------------------------*/
/* Releases a lock acquired by WaitForAccess */
  MACRO
  SpinlockRelease $lock_addr
    ; Ensure all reads/writes are visible to other observers before
    ; releasing the lock.
    dmb     sy
    str     xzr, [$lock_addr]  ; Clear the lock
    dmb     sy
  MEND

/* ----------------------------------------------------------------------
 * MACRO: CurCPUNumber
 * ---------------------------------------------------------------------- */
    MACRO
    CurCPUNumber $rx, $ry, $cpus_per_cluster
       mrs     $rx, MPIDR_EL1
       and     $ry, $rx, #MPIDR_EL1_AFF1_MSK
       lsr     $ry, #MPIDR_EL1_AFF1_SHFT
       mov     $rx, #$cpus_per_cluster
       mul     $ry, $rx
       mrs     $rx, MPIDR_EL1
       and     $rx, #MPIDR_EL1_AFF0_MSK
       add     $rx, $ry
    MEND

/* ----------------------------------------------------------------------
 * MACRO: CurClusterNum
 * TODO: move this into armlib
 * ---------------------------------------------------------------------- */
    MACRO
    CurClusterNum $rx
       mrs     $rx, MPIDR_EL1
       and     $rx, #MPIDR_EL1_AFF1_MSK
       lsr     $rx, #MPIDR_EL1_AFF1_SHFT
    MEND

#endif /* #ifndef _ARM_ASM_ */

#endif /* UTIL_H */
