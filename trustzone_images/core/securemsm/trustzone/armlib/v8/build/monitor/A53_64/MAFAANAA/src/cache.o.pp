# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/armlib/v8/src/cache.s"
# 1 "<built-in>" 1
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/armlib/v8/src/cache.s" 2
;============================================================================
;
; Cache Maintenance Routines
;
; Copyright 2014 by Qualcomm Technologies, Incorporated. All Rights Reserved
;============================================================================

;============================================================================
;
; EDIT HISTORY FOR MODULE
;
; $Header:
; $DateTime: 2018/02/07 00:37:16 $
; $Author: mplp4svc $
;
; This section contains comments describing changes made to the module.
; Notice that changes are listed in reverse chronological order.
;
;
; when who what, where, why
; -------- --- ---------------------------------------------------------
; 03/11/14 pre Initial revision.
;============================================================================


# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/armlib/v8/inc/sys_fields.h" 1
# 26 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/armlib/v8/src/cache.s" 2
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/armlib/v8/inc/util.h" 1
# 42 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/armlib/v8/inc/util.h"
    MACRO
    SetSecure $rx





        mov $rx, xzr
        msr SCR_EL3, $rx
        isb
    MEND





    MACRO
    SetNonSecure $rx





        mov $rx, #0x020:OR:0x001:OR:0x004:OR:0x400:OR:0x100
        msr SCR_EL3, $rx
        isb
    MEND





    MACRO
    PushTwo $x0, $x1
        stp $x1, $x0, [sp, #-0x10]!
    MEND




    MACRO
    PopTwo $x0, $x1
        ldp $x0, $x1, [sp], #0x10
    MEND




    MACRO
    PushAArch32NPRegs
        PushTwo x5, x6
        PushTwo x7, x8
        PushTwo x9, x10
        PushTwo x11, x12
        PushTwo x13, x14
        PushTwo x15, x16
        PushTwo x17, x18
        PushTwo x19, x20
        PushTwo x21, x22
        PushTwo x23, x24
        PushTwo x25, x26
        PushTwo x27, x28
        PushTwo x29, x30
    MEND




    MACRO
    PopAArch32NPRegs
        PopTwo x30, x29
        PopTwo x28, x27
        PopTwo x26, x25
        PopTwo x24, x23
        PopTwo x22, x21
        PopTwo x20, x19
        PopTwo x18, x17
        PopTwo x16, x15
        PopTwo x14, x13
        PopTwo x12, x11
        PopTwo x10, x9
        PopTwo x8, x7
        PopTwo x6, x5
    MEND





    MACRO
    PushAArch64CSRegs
        PushTwo x19, x20
        PushTwo x21, x22
        PushTwo x23, x24
        PushTwo x25, x26
        PushTwo x27, x28
        PushTwo x29, x30
    MEND





    MACRO
    PopAArch64CSRegs
        PopTwo x30, x29
        PopTwo x28, x27
        PopTwo x26, x25
        PopTwo x24, x23
        PopTwo x22, x21
        PopTwo x20, x19
    MEND







    MACRO
    PushAArch64PSRegs
        PushTwo x19, x18
        PushTwo x17, x16
        PushTwo x15, x14
        PushTwo x13, x12
        PushTwo x11, x10
        PushTwo x9, x8
        PushTwo x7, x6
        PushTwo x5, x4
        PushTwo x3, x2
        PushTwo x1, x0
    MEND




    MACRO
    PopAArch64PSRegs
        PopTwo x0, x1
        PopTwo x2, x3
        PopTwo x4, x5
        PopTwo x6, x7
        PopTwo x8, x9
        PopTwo x10, x11
        PopTwo x12, x13
        PopTwo x14, x15
        PopTwo x16, x17
        PopTwo x18, x19
    MEND





    MACRO
    WipeParamRegs
        mov x0, #0
        mov x1, #0
        mov x2, #0
        mov x3, #0
        mov x4, #0
        mov x5, #0
        mov x6, #0
        mov x7, #0
        mov x8, #0
    MEND





    MACRO
    WipeScratchRegs
        mov x9, #0
        mov x10, #0
        mov x11, #0
        mov x12, #0
        mov x13, #0
        mov x14, #0
        mov x15, #0
        mov x16, #0
        mov x17, #0
        mov x18, #0
    MEND





    MACRO
    WipeAllGPRegs
        WipeParamRegs
        WipeScratchRegs
        mov x19, #0
        mov x20, #0
        mov x21, #0
        mov x22, #0
        mov x23, #0
        mov x24, #0
        mov x25, #0
        mov x26, #0
        mov x27, #0
        mov x28, #0
        mov x29, #0
        mov x30, #0
    MEND





    MACRO
    CorruptMonRegs
        mov x0, #0xA
        mov x1, #0xB
        mov x2, #0xC
        mov x3, #0xD
        mov x4, #0xE
        mov x5, #0xF
        mov x6, #0xA
        mov x7, #0xB
        mov x8, #0xC
        mov x9, #0xD
        mov x10, #0xE
        mov x11, #0xF
        mov x12, #0xA
        mov x13, #0xB
        mov x14, #0xC
        mov x15, #0xD
        mov x16, #0xE
        mov x17, #0xF
        mov x18, #0xA
        mov x19, #0xB
        mov x20, #0xC
        mov x21, #0xD
        mov x22, #0xE
        mov x23, #0xF
        mov x24, #0xA
        mov x25, #0xB
        mov x26, #0xC
        mov x27, #0xD
        mov x28, #0xE
        mov x29, #0xF
        mov x30, #0xA
    MEND






  MACRO
  SpinlockObtain $lock_addr, $cpu, $work
    mrs $cpu, MPIDR_EL1
    and $cpu, $cpu, #0xFF
    add $cpu, #0x1
42 ldaxr $work, [$lock_addr]
    cbnz $work, %b42
    stlxr $work, $cpu, [$lock_addr]
    cbnz $work, %b42
    dmb sy
  MEND





  MACRO
  SpinlockRelease $lock_addr
    ; Ensure all reads/writes are visible to other observers before
    ; releasing the lock.
    dmb sy
    str xzr, [$lock_addr] ; Clear the lock
    dmb sy
  MEND




    MACRO
    CurCPUNumber $rx, $ry, $cpus_per_cluster
       mrs $rx, MPIDR_EL1
       and $ry, $rx, #0x0000FF00
       lsr $ry, #0x8
       mov $rx, #$cpus_per_cluster
       mul $ry, $rx
       mrs $rx, MPIDR_EL1
       and $rx, #0x000000FF
       add $rx, $ry
    MEND





    MACRO
    CurClusterNum $rx
       mrs $rx, MPIDR_EL1
       and $rx, #0x0000FF00
       lsr $rx, #0x8
    MEND
# 27 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/armlib/v8/src/cache.s" 2

;============================================================================
;
; MACROS AND FUNCTIONS
;
;============================================================================
    PRESERVE8
    AREA CACHE, CODE, READONLY, ALIGN=3

;============================================================================
; MACRO dcache_granule_size
;
; ARGS
; $reg - cache granule size in bytes
; $tmp - cache granule size in words used for calculation
; of the size in bytes
;
; DESCRIPTION
; cache_granule_size - get the size of the smallest dcache/unified
; cache (available on ARMv7+). Note that the value of $reg and $tmp
; will be modified.
;
; TODO: CTR register not architecturally guaranteed
;============================================================================
  MACRO
    dcache_granule_size $reg, $tmp
    mrs $tmp, CTR_EL0
    lsr $tmp, #16
    and $tmp, #0xF ; $tmp = CTR[DminLine]
    mov $reg, #4 ; CTR[DminLine] = log2 num words
    lsl $reg, $reg, $tmp ; cache writeback granule size in bytes
  MEND

;============================================================================
; void iterate_set_way_louis(void (*fn)(uint64 set_and_way_info))
; Note: iterate_set_way may return
;============================================================================
iterate_set_way_louis
    mrs x1, CLIDR_EL1 ; read clidr
    mov x2, #0x00E00000
    ands x2, x1 ; extract louis from clidr
    beq %f1 ; if louis is 0, then no need to clean
    lsr x2, #(21 -1) ; x2 = (LoUIS * 2)
    b iterate_set_way
1
    ret

;============================================================================
; void iterate_set_way_loc(void (*fn)(uint64 set_and_way_info))
; Note: iterate_set_way may return
; WARNING: may not corrupt x19. See mmu.s.
;============================================================================
iterate_set_way_loc
    mrs x1, CLIDR_EL1 ; read clidr
    mov x2, #0x07000000
    ands x2, x1 ; extract loc from clidr
    beq %f1 ; if (LoC == 0), then no need to iterate
    lsr x2, #(24 -1) ; x2 = (LoC * 2)
    b iterate_set_way
1
    ret

;============================================================================
; static void iterate_set_way(void (*fn)(uint64 set_and_way_info),
; el3_reg_t CLIDR_EL1,
; el3_reg_t last_level_times_two)
; WARNING: may not corrupt x19. See mmu.s.
;============================================================================
iterate_set_way FUNCTION
    dsb sy ; ensure ordering with previous
                                    ; memory accesses
    mov x6, x0 ; x6 = function pointer arg
    mov x12, x30 ; Save LR
    mov x10, #0 ; start at L1 cache
level_loop
    add x8, x10, x10, lsr #1 ; x8 = (3 * level)
    lsr x3, x1, x8 ; extract cache type bits from clidr
    and x3, #0x00000007 ; for current cache level
    cmp x3, #2 ; see what cache we have at this level
    blt next_level ; values below have no d_cache
    msr CSSELR_EL1, x10 ; select current cache level in csselr
    isb ; isb to sych cssrelr & ccsidr
    mrs x3, CCSIDR_EL1 ; x3 = ccsidr
    and x8, x3, #0x7
                                    ; x8 = log2(line length) - 4
    add x8, #0x4
                                    ; x8 = log2(line length), offset of set
    ubfx x4, x3, #3, #10
                                    ; x4 = number of ways from ccsidr
    clz w5, w4 ; find bit position of way size increment
    ubfx x7, x3, #13, #15
                                    ; x7 = set index (from ccsidr)
set_loop
    mov x9, x4 ; x9 = way index
way_loop
    lsl x0, x9, x5 ; x0 = way index
    orr x0, x10 ; x0 |= cache level
    lsl x3, x7, x8 ; x3 = shifted set index
    orr x0, x3 ; x0 |= shifted set index
    blr x6 ; Jump to function for given cache line
    subs x9, #1 ; decrement the way index
    bge way_loop
    subs x7, #1 ; decrement the set index
    bge set_loop
next_level
    add x10, #2 ; cache level += 2
    cmp x2, x10 ; (last_level * 2) (x2) >=
                                    ; (cache level * 2) (x10)
    dsb sy
    bgt level_loop
iterate_finished
    mov x10, #0
    msr CSSELR_EL1, x10 ; select level 0 in csselr
    isb
    mov x30, x12 ; Restore LR
    ret ; return to original caller

    ENDFUNC


;============================================================================
; static void clean_dcache_line(uint64 set_and_way_info)
;
; DESCRIPTION -
; Flush a single cache line by set/way
;============================================================================
clean_dcache_line FUNCTION
    dc csw, x0
    ret
    ENDFUNC

;============================================================================
; static void clean_and_inval_dcache_line(uint64 set_and_way_info)
;============================================================================
clean_and_inval_dcache_line FUNCTION
    dc cisw, x0
    ret
    ENDFUNC

;============================================================================
; static void inval_dcache_line(uint64 set_and_way_info)
;============================================================================
inval_dcache_line FUNCTION
    dc isw, x0
    ret
    ENDFUNC

;============================================================================
; void clean_all_dcache(void)
; Note: iterate_set_way_loc returns
;============================================================================
    EXPORT clean_all_dcache
clean_all_dcache
    ldr x0, =clean_dcache_line
    b iterate_set_way_loc

;============================================================================
; void clean_and_inval_all_dcache(void)
; Note: iterate_set_way_loc returns
; WARNING: may not corrupt x19. See mmu.s.
;============================================================================
    EXPORT clean_and_inval_all_dcache
clean_and_inval_all_dcache
    ldr x0, =clean_and_inval_dcache_line
    b iterate_set_way_loc

;============================================================================
; void clean_and_inval_all_dcache_louis(void)
; Note: iterate_set_way_louis returns
;============================================================================
    EXPORT clean_and_inval_all_dcache_louis
clean_and_inval_all_dcache_louis
    ldr x0, =clean_and_inval_dcache_line
    b iterate_set_way_louis

;============================================================================
; void inval_all_dcache(void)
; Note: iterate_set_way_loc returns
;============================================================================
    EXPORT inval_all_dcache
inval_all_dcache
    ldr x0, =inval_dcache_line
    b iterate_set_way_loc

;============================================================================
; void clean_and_inval_dcache_region(uint64 vaddr, uint64 len)
;============================================================================
    EXPORT clean_and_inval_dcache_region
clean_and_inval_dcache_region FUNCTION
    dsb sy ; data barrier before flushing

    add x1, x0 ; get the end address
    dcache_granule_size x2, x3 ; x2 = cache granule in bytes
    sub x3, x2, #1 ; x3 = (cache granule size - 1)
    bic x0, x3 ; x0 = start address with cache granule
                                    ; size bits removed
flush_loop
    dc civac, x0
    add x0, x2 ; x0 = (x0 + cache granule size)
    cmp x0, x1 ; x0 > "end of region" ?
    blt flush_loop
    dsb sy ; ensure all memory operations are complete
    ret
    ENDFUNC

;============================================================================
; void inval_dcache_region(uint64 vaddr, uint64 len)
; Note: see clean_and_inval_dcache_region for comments
;============================================================================
    EXPORT inval_dcache_region
inval_dcache_region FUNCTION
    dsb sy
    add x1, x0
    dcache_granule_size x2, x3
    sub x3, x2, #1
    bic x0, x3
inval_loop
    dc ivac, x0
    add x0, x2
    cmp x0, x1
    blt inval_loop
    dsb sy
    ret
    ENDFUNC

;============================================================================
; void clean_dcache_region(uint64 vaddr, uint64 len)
; Note: see clean_and_inval_dcache_region for comments
;============================================================================
    EXPORT clean_dcache_region
clean_dcache_region FUNCTION
    dsb sy
    add x1, x0
    dcache_granule_size x2, x3
    sub x3, x2, #1
    bic x0, x3
clean_loop
    dc cvac, x0
    add x0, x2
    cmp x0, x1
    blt clean_loop
    dsb sy
    ret
    ENDFUNC

;============================================================================
; void enable_el3_icache(void)
;============================================================================
    EXPORT enable_el3_icache
enable_el3_icache FUNCTION
    dsb sy
    isb
    mrs x0, SCTLR_EL3
    orr x0, #0x00001000
    msr SCTLR_EL3, x0
    isb
    ret
    ENDFUNC

;============================================================================
; void enable_el3_dcache(void)
;============================================================================
    EXPORT enable_el3_dcache
enable_el3_dcache FUNCTION
    dsb sy
    isb
    mrs x0, SCTLR_EL3
    orr x0, #0x00000004
    msr SCTLR_EL3, x0
    isb
    ret
    ENDFUNC

;============================================================================
; void disable_el3_icache(void)
;============================================================================
    EXPORT disable_el3_icache
disable_el3_icache FUNCTION
    dsb sy
    isb
    mrs x0, SCTLR_EL3
    mov x1, #0x00001000
    bic x0, x1
    msr SCTLR_EL3, x0
    isb
    ret
    ENDFUNC

;============================================================================
; void disable_el3_dcache(void)
;============================================================================
    EXPORT disable_el3_dcache
disable_el3_dcache FUNCTION
    dsb sy
    isb
    mrs x0, SCTLR_EL3
    mov x1, #0x00000004
    bic x0, x1
    msr SCTLR_EL3, x0
    isb
    ret
    ENDFUNC

;============================================================================
; void disable_el1_dcache(void)
;============================================================================
    EXPORT disable_el1_dcache
disable_el1_dcache FUNCTION
    dsb sy
    isb
    mrs x0, SCTLR_EL1
    mov x1, #0x00000004
    bic x0, x1
    msr SCTLR_EL1, x0
    isb
    ret
    ENDFUNC

;============================================================================
; boolean is_el3_icache_enabled(void)
;============================================================================
    EXPORT is_el3_icache_enabled
is_el3_icache_enabled FUNCTION
    isb
    mrs x0, SCTLR_EL3
    ands x0, #0x00001000
    lsr x0, #0xC
    ret
    ENDFUNC

;============================================================================
; boolean is_el3_dcache_enabled(void)
;============================================================================
    EXPORT is_el3_dcache_enabled
is_el3_dcache_enabled FUNCTION
    isb
    mrs x0, SCTLR_EL3
    ands x0, #0x00000004
    lsr x0, #0x2
    ret
    ENDFUNC

    END
