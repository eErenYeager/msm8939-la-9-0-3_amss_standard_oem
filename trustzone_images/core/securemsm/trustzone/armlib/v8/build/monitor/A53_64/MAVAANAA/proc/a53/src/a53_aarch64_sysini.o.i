;-------------------------------------------------------------------------------
; SYSINI
; Version: 3.13
; CPU: Cortex-A53
; Mode: AArch64
;-------------------------------------------------------------------------------
; Bit 15 of the value at this address is used to distinguish between
; 8939(octa-core) and 8936(quad-core)
;-------------------------------------------------------------------------------
   AREA RO, CODE, READONLY, ALIGN=3
   ; Export functions and global variables
   EXPORT a53_aarch64_sysini
   PRESERVE8
;-------------------------------------------------------------------------------
;-------------------------------------------------------------------------------
; a53_aarch64_sysini
; Inputs- None
; Description - This function initializes several implementation defined
; CPU registers. Fixes for several ARM errata are also applied here.
;-------------------------------------------------------------------------------
a53_aarch64_sysini
   LDR X1, =tcsr_hw_version
   STR X0, [X1]
   ; ---------------------------------------------------------------------------
   ; *** RMR Setting ***
   ; Set RMR to 0 for Core0 in Cortex-A53 cluster (Cluster 0) in 8992
   ; Set RMR to 0 for Core0 in Cortex-A53 cluster (Cluster 0) in 8994_R1
   ; Set RMR to 0 for Core0 in Cortex-A53 cluster (Cluster 0) in 8994_R2*
   ; Set RMR to 0 for Core0 in Cortex-A53 cluster (Cluster 0) in 8956
   ; Set RMR to 0 for Core0 in Cortex-A53 cluster 1 in 8952 (not Cluster 0)
   ; Set RMR to 0 for Core0 in Cortex-A53 cluster 1 in 8939 (not Cluster 0)
   ; Set RMR to 0 for Core0 in Cortex-A53 cluster 1 in 8939_R3 (not Cluster 0)
   ; Set RMR to 0 for Core0 in Cortex-A53 cluster 0 in 8939 quad core ...
   ; (no Cluster 1)
   ; Set RMR to 0 for Core0 in Cortex-A53 cluster 0 in 8916 quad core ...
   ; (no Cluster 1)
   ;
   ; Prism CR: 716767
   ;-----------------------------------------------------------------------------
   LDR X1, tcsr_hw_version; Read Soc_ID into X1
   LDR X0, =0x000E0100
   CMP X0, X1
   B.EQ l_RMR_Block_0
   LDR X0, =0x000B0100
   CMP X0, X1
   B.EQ l_RMR_Block_0
   LDR X0, =0x000B0200
   CMP X0, X1
   B.EQ l_RMR_Block_0
   LDR X0, =0x000B0201
   CMP X0, X1
   B.EQ l_RMR_Block_0
   LDR X0, =0x000B0202
   CMP X0, X1
   B.EQ l_RMR_Block_0
   LDR X0, =0x20040100
   CMP X0, X1
   B.EQ l_RMR_Block_0
   LDR X0, =0x20070100
   CMP X0, X1
   B.EQ l_RMR_check_cluster
   LDR X0, =0x20010300
   CMP X0, X1
   B.EQ l_RMR_check_cluster
   LDR X0, =0x20010100
   CMP X0, X1
   B.NE l_check_8936_8939_exit
l_check_8936_8939_start
   LDR X2, =0x00058054
   LDR W0, [X2]
   UBFX X2, X0, #15, #1
   CMP X2, XZR
   B.EQ l_RMR_check_cluster
   B l_RMR_Block_0
l_check_8936_8939_exit
   LDR X0, =0x20000100
   CMP X0, X1
   B.EQ l_RMR_Block_0
   B l_RMR_exit
l_RMR_check_cluster
   MRS X0, MPIDR_EL1
   UBFX X1, X0, #8, #8 ; X1 <- Cluster number
   CMP X1, XZR
   B.EQ l_RMR_Block_1
l_RMR_Block_0
   MRS X0, MPIDR_EL1
   AND X0, X0, #0xFF ; X0 <- CPU number
   CMP X0, XZR
   CSET X1, NE ; Conditional Set if Not Equal
   MSR RMR_EL3, X1 ; Write X1 to RMR_EL3
   B l_RMR_exit
l_RMR_Block_1
   MOV X1, #1
   MSR RMR_EL3, X1 ; Write X1 to RMR_EL3
l_RMR_exit
   ISB SY
   DSB SY
   ;----------------------------------------------------------------------------
   ; Set SMP Bit - must be set before caches and MMU are enabled
   ;----------------------------------------------------------------------------
   MRS X0, S3_1_C15_C2_1 ; Read EL1 CPU Extended Control Register
   ORR X0, X0, #(0x1 << 6) ; Set SMP bit
   MSR S3_1_C15_C2_1, X0 ; Write EL1 CPU Extended Control Register
   ISB SY
   DSB SY
   ; ---------------------------------------------------------------------------
   ; Unset TFP bit in CPTR_EL3
   ; The default value of this is expected to be 0 in A53 and 1 in A57
   ; Instructions that access registers associated with floating-point and
   ; Advanced SIMD execution are not trapped
   ; ---------------------------------------------------------------------------
   MRS X0, CPTR_EL3; Read CPTR_EL3
   MOV X1, #0x400
   BIC X0, X0, X1
   MSR CPTR_EL3, X0; Write EL3 Architectural Feature Trap Register
   ISB SY
   ; ---------------------------------------------------------------------------
   ; Disable VFP/SIMD functionality, will be enabled when accessed first.
   ; Exception will be trapped into EL1 but not to EL3
   ; ---------------------------------------------------------------------------
   MRS X0, CPACR_EL1
   MOV X1, #0x300000
   BIC X0, X0, X1
   MSR CPACR_EL1, X0
   MRS X0, FPEXC32_EL2
   MOV X1, #0x40000000
   BIC X0, X0, X1
   MSR FPEXC32_EL2, X0
   ISB SY
   ; ---------------------------------------------------------------------------
   ; Disable Secure Invasive and Non-Invasive Debug
   ; through SDER32_EL3 - Secure Debug Enable Register
   ; This register is specific to AArch32 EL1 Secure
   ; This should ideally be set by Secure AArch32 monitor code
   ; ---------------------------------------------------------------------------
   MRS X0, SDER32_EL3 ; Read SDER32_EL3 into X0
   MOV X1, #3
   BIC X0, X0, X1 ; Disable bits [0] and [1]
   MSR SDER32_EL3, X0 ; Write X0 to SDER32_EL3
   ISB SY
   ; ---------------------------------------------------------------------------
   ; Set debugging features in MDCR_EL3
   ; Some of the debug features are set to permitted
   ; and some others are set to trap to EL3
   ; Please refer to ARM ARM v8 for details
   ; ---------------------------------------------------------------------------
   MRS X0, MDCR_EL3 ; Read MDCR_EL3 into X0
   MOV X1, #1<<6
   BIC X0, X0, X1 ; Clear bit 6
   MOV X1, #3<<9
   BIC X0, X0, X1 ; Clear bits 9 and 10
   MOV X1, #3<<14
   BIC X0, X0, X1; Clear bits 14 and 15
   ORR X0, X0, #1<<16 ; Set bit 16
   MOV X1, #1<<17
   BIC X0, X0, X1; Clear bit 17
   ORR X0, X0, #3<<20 ; Set bits 20 and 21
   MSR MDCR_EL3, X0 ; Write X0 to MDCR_EL3
   ISB SY
   ; ---------------------------------------------------------------------------
   ; Enable CPU Dynamic Retention
   ; 8994_R2 supports C2d mode and this requires programming of the CPUECTLR_EL1
   ; Bits [2:0] need to be set to 0b001 to enable retention entry after 2
   ; generic arch timer ticks
   ;
   ; Prism CR: 726838, 760460, 767940
   ; Applicable to: 8994_R1-NO, 8994_R2*-YES, 8992-YES, 8956-NO
   ; ---------------------------------------------------------------------------
   LDR X1, tcsr_hw_version; Read SoC_ID into X1
   LDR X0, =0x000B0200
   CMP X0, X1
   B.EQ l_C2d_EN_start
   LDR X0, =0x000B0201
   CMP X0, X1
   B.EQ l_C2d_EN_start
   LDR X0, =0x000B0202
   CMP X0, X1
   B.EQ l_C2d_EN_start
   LDR X0, =0x000E0100
   CMP X0, X1
   B.EQ l_C2d_EN_start
   B l_C2d_EN_exit
l_C2d_EN_start
   MRS X0, S3_1_c15_c2_1; Read EL1 CPU Extended Control Register
   MOV X1, #1
   BFI X0, X1, #0, #3
   MSR S3_1_c15_c2_1, X0; Write EL1 CPU Extended Control Register
   ISB SY
l_C2d_EN_exit
   ; ---------------------------------------------------------------------------
   ; Enable L2 Dynamic Retention
   ; 8994_R2 supports D2d mode and this requires programming of the L2ECTLR_EL1
   ; Bits [2:0] need to be set to 0b111 to enable retention entry after 512
   ; generic arch timer ticks
   ;
   ; Prism CR: 726838, 760460, 767940
   ; Applicable to: 8994_R1-NO, 8994_R2*-YES, 8992-YES, 8956-YES
   ; ---------------------------------------------------------------------------
   LDR X1, tcsr_hw_version; Read SoC_ID into X1
   LDR X0, =0x000B0200
   CMP X0, X1
   B.EQ l_D2d_EN_start
   LDR X0, =0x000B0201
   CMP X0, X1
   B.EQ l_D2d_EN_start
   LDR X0, =0x000B0202
   CMP X0, X1
   B.EQ l_D2d_EN_start
   LDR X0, =0x000E0100
   CMP X0, X1
   B.EQ l_D2d_EN_start
   LDR X0, =0x20040100
   CMP X0, X1
   B.EQ l_D2d_EN_start
   B l_D2d_EN_exit
l_D2d_EN_start
   MRS X0, S3_1_c11_c0_3; Read L2 Extended Control Register
   MOV X1, #7
   BFI X0, X1, #0, #3
   MSR S3_1_c11_c0_3, X0; Write L2 Extended Control Register
   ISB SY
l_D2d_EN_exit
   ; ---------------------------------------------------------------------------
   ; From Cortex-A53 MPCore Software Developers Errata Notice
   ; Erratum #823819
   ; A snoop request to a core coincident with retention entry might cause
   ; deadlock Status
   ;
   ; Workaround:
   ; This erratum can be avoided by disabling CPU retention, by setting
   ; CPUECTLR.CPURETCTL to zero
   ;
   ; Prism CR: -------
   ; ClearQuest CR: QCTDD01140838
   ;
   ; Fix Applicable to: 8994_R1
   ; ---------------------------------------------------------------------------
   LDR X1, tcsr_hw_version; Read SoC_ID into X1
   LDR X0, =0x000B0100
   CMP X0, X1
   B.EQ l_823819_start
   B l_823819_exit
l_823819_start
   MRS X0, S3_1_C15_C2_1; Read EL1 CPU Extended Control Register
   MOV X1, #0x7
   BIC X0, X0, X1
   MSR S3_1_C15_C2_1, X0; Write EL1 CPU Extended Control Register
   ISB SY
l_823819_exit
   ; ---------------------------------------------------------------------------
   ; From Cortex-A53 MPCore Software Developers Errata Notice
   ; Erratum #819472
   ; Store exclusive instructions might cause data corruption
   ;
   ; Workaround:
   ; Enable external HW which converts DCCMVAC and DCCMVAU instructions
   ; into DCCIMVAC instructions
   ;
   ; Prism CR: 610928
   ; ClearQuest CR: QCTDD01066297
   ;
   ; Fix Applicable to: 8936, 8994_R1(*NOT IN THIS FILE)
   ; In case of 8939, we enable the chicket bit to do this
   ; In case of 8994_R1, this fix is applied by modifying the system
   ; register of Cortex-A57, so need to apply the following patch for
   ; 8939 only
   ; ---------------------------------------------------------------------------
   LDR X1, tcsr_hw_version; Read Soc_ID into X1
   LDR X0, =0x20010100
   CMP X0, X1
   B.EQ l_819472_start
   B l_819472_exit
l_819472_start
   LDR X1, =0x0B1D1400 ; Address of memory-mapped register to enable
                       ; translation of cache clean to clean and invalidate
   LDR X0, [X1]
   ORR X0, X0, #1
   STR X0, [X1]
   LDR X0, [X1]
   ISB SY
l_819472_exit
   ; ---------------------------------------------------------------------------
   ; From Cortex-A53 MPCore Software Developers Errata Notice
   ; Erratum #824069
   ; Cache line might not be marked as clean after a CleanShared snoop
   ;
   ; Workaround:
   ; Enable external HW which converts DCCMVAC and DCCMVAU instructions
   ; into DCCIMVAC instructions
   ;
   ; Prism CR: -----
   ; ClearQuest CR: QCTDD01251383
   ;
   ; Fix same as that for Erratum #819472
   ; ---------------------------------------------------------------------------
   ; *UPDATE*
   ; Clarification from ARM
   ; This erratum will not cause any failure with CCI-400 or CCN-504.
   ; ---------------------------------------------------------------------------
   ; From Cortex-A53 MPCore Software Developers Errata Notice
   ; Erratum #826319
   ; System might deadlock if a write cannot complete until read data is accepted
   ;
   ; Workaround:
   ; For systems without a snoop filter in the interconnect, and without an L3
   ; cache, this erratum can be worked around by disabling Evict and WriteEvict
   ; transactions. This can be done by clearing bit 14 and setting bit 3 in the
   ; L2ACTLR.
   ;
   ; Prism CR: 652298
   ; ClearQuest CR: QCTDD01258361
   ;
   ; Fix Applicable to: 8916, 8939, 8994_R1, 8994_R2
   ; ---------------------------------------------------------------------------
   LDR X1, tcsr_hw_version; Read Soc_ID into X1
   LDR X0, =0x20000100
   CMP X0, X1
   B.EQ l_826319_start
   LDR X0, =0x20010100
   CMP X0, X1
   B.EQ l_826319_start
   LDR X0, =0x000B0100
   CMP X0, X1
   B.EQ l_826319_start
   LDR X0, =0x000B0200
   CMP X0, X1
   B.EQ l_826319_start
   LDR X0, =0x000B0201
   CMP X0, X1
   B.EQ l_826319_start
   LDR X0, =0x000B0202
   CMP X0, X1
   B.EQ l_826319_start
   B l_826319_exit
l_826319_start
   MRS X0, S3_1_C15_C0_0; Read L2ACTLR_EL1 into X0
   MOV X1, #1<<14
   BIC X0, X0, X1 ;Clear bit 14
   ORR X0, X0, #1<<3 ;Set bit 3
   MSR S3_1_C15_C0_0, X0; Write X0 to L2ACTLR_EL1
   ISB SY
l_826319_exit
   ; ---------------------------------------------------------------------------
   ; From Cortex-A53 MPCore Software Developers Errata Notice
   ; Erratum #822569
   ; RAMs in PDL2 power domain can be clocked while still in retention
   ;
   ; Workaround:
   ; Software can workaround this erratum by disabling L2 retention entry.
   ; This can be done by writing 0b000 to bits [2:0] of the L2ECTLR register.
   ;
   ; Prism CR:
   ; ClearQuest CR: QCTDD01073254
   ;
   ; Fix Applicable to: 8994_R1 - YES
   ; ----------------------------------------------------------
   LDR X1, tcsr_hw_version; Read Soc_ID into X1
   LDR X0, =0x000B0100
   CMP X0, X1
   B.EQ l_822569_start
   B l_822569_exit
l_822569_start
   MRS X0, S3_1_c11_c0_3; Read L2 Extended Control Register
   MOV X1, #7
   BIC X0, X0, X1
   MSR S3_1_c11_c0_3, X0; Write L2 Extended Control Register
   ISB SY
l_822569_exit
   ; ---------------------------------------------------------------------------
   ; In APCS_*_L1_L2_MEM_CGC_OVERRIDE register(s), clear out desired bits to
   ; enable L1/L2 clock gating(CGC) based on CS(chip select)
   ;
   ; Fix Applicable to:
   ; - 8939_R3 : clear bits [9:0] of register APCS_COMMON_L1_L2_CGC_DISABLE
   ; - 8952 : clear bits [4:0] of register APCS_ALIAS0_L1_L2_CGC_DISABLE
   ; clear bits [4:0] of register APCS_ALIAS0_L1_L2_CGC_DISABLE
   ; - 8956 : clear bits [4:0] of register APCS_COMMON_L1_L2_MEM_CGC_OVERRIDE
   ;
   ; These fixes need to be made for non-boot CPUs.
   ; There is a bug discovered in this feature due to which the CPU for which
   ; CS based clockgating is being enabled has to in quiescent state
   ; (preferably clock gated). This is due to a missing sychronizer for L1/L2
   ; clock gating enable
   ; ---------------------------------------------------------------------------
   LDR X1, tcsr_hw_version; Read SoC_ID into X1
   LDR X0, =0x20010300
   CMP X0, X1
   B.EQ cgc_override_start_8939_R3
   LDR X0, =0x20070100
   CMP X0, X1
   B.EQ cgc_override_start_8952
   LDR X0, =0x20040100
   CMP X0, X1
   B.EQ cgc_override_start_8956
   B cgc_override_exit
cgc_override_start_8939_R3
   LDR X0, =0x0B1D1500; APCS_COMMON_L1_L2_CGC_DISABLE
   MOV W2, #0x1EF; Bits to be cleared; would have been 0x3FF in absence of bug
   LDR W1, [X0]; Read register into X1
   BIC W1, W1, W2
   STR W1, [X0]
   B cgc_override_exit
cgc_override_start_8952
   LDR X0, =0x0B111088; APCS_ALIAS0_L1_L2_CGC_OVERIDE
   MOV W2, #0x1F; Bits to be cleared
   LDR W1, [X0]; Read register into X1
   BIC W1, W1, W2
   STR W1, [X0]
   LDR X0, =0x0B011088; APCS_ALIAS1_L1_L2_CGC_OVERIDE
   MOV W2, #0x0E; Bits to be cleared; would have been 0x1F in absence of bug
   LDR W1, [X0]; Read register into X1
   BIC W1, W1, W2
   STR W1, [X0]
   B cgc_override_exit
cgc_override_start_8956
   LDR X0, =0x0B1D1210; APCS_COMMON_L1_L2_CGC_DISABLE
   MOV W2, #0x0E; Bits to be cleared; would have been 0x1F in absence of bug
   LDR W1, [X0]; Read register into X1
   BIC W1, W1, W2
   STR W1, [X0]
   B cgc_override_exit
cgc_override_exit
   ISB
   DSB SY
   ; ---------------------------------------------------------------------------
   ; Enable CCI Hierarchical clock gating
   ; This is a one time setting
   ;
   ; Fix Applicable to:
   ; - 8952 : Write a 1 to APCS_COMMON_CCI_HIER_CG[0] (Enable bit)
   ; - 8956 : Write a 1 to APCS_COMMON_CCI_HIER_CG[0] (Enable bit)
   ; Program APCS_COMMON_CCI_HIER_CG[13:4] to 0 (Timer count to
   ; wait after CCI blocks are idle)
   ; ---------------------------------------------------------------------------
   LDR X1, tcsr_hw_version; Read SoC_ID into X1
   LDR X0, =0x20070100
   CMP X0, X1
   B.EQ CCI_HIER_CG_start_8952
   LDR X0, =0x20040100
   CMP X0, X1
   B.EQ CCI_HIER_CG_start_8956
   B CCI_HIER_CG_exit
CCI_HIER_CG_start_8952
   LDR X0, =0x0B1D105C; APCS_COMMON_CCI_HIER_CG
   LDR W1, [X0]; Read register into X1
   ORR W1, W1, #1 ; Write a 1 to bit [0]
   STR W1, [X0]
   B CCI_HIER_CG_exit
CCI_HIER_CG_start_8956
   LDR X0, =0x0B1D105C; APCS_COMMON_CCI_HIER_CG
   LDR W1, [X0]; Read register into X1
   ORR W1, W1, #1 ; Write a 1 to bit [0]
   MOV W2, #0x3FF<<4 ; 0x3FF0 i.e. bits [13:4] are 1, rest 0
   BIC W1, W1, W2 ; Clear out bits [13:4]
   STR W1, [X0]
   B CCI_HIER_CG_exit
CCI_HIER_CG_exit
   ISB
   DSB SY
   DSB SY
   ISB SY ;Make sure all changes are propagated before returning from sysini
   RET
   AREA A53_SYSINI_DATA, READWRITE, ALIGN=3
tcsr_hw_version SPACE 0x8
   END ; End of sysini
