# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/armlib/v8/proc/a53/src/a53_sys_regs.s"
# 1 "<built-in>" 1
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/armlib/v8/proc/a53/src/a53_sys_regs.s" 2
;============================================================================
;
; Accessor Functions for Processor Specific System Registers
;
; Copyright 2014 by Qualcomm Technologies, Incorporated. All Rights Reserved
;============================================================================

;============================================================================
;
; EDIT HISTORY FOR MODULE
;
; $Header:
; $DateTime: 2018/02/07 00:37:16 $
; $Author: mplp4svc $
;
; This section contains comments describing changes made to the module.
; Notice that changes are listed in reverse chronological order.
;
;
; when who what, where, why
; -------- --- ---------------------------------------------------------
; 03/11/14 pre Initial revision.
;============================================================================


# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/armlib/v8/proc/a53/inc/a53_sys_fields.h" 1
# 26 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/armlib/v8/proc/a53/src/a53_sys_regs.s" 2

    PRESERVE8
    AREA A53_SYS_REG, CODE, READONLY, ALIGN=3

;--------------------------------------------------------------------
; Updates ACTLR_EL3[CPUECTLR_EL1]
;
; void a53_grant_el1_cpuectlr_access(void)
;--------------------------------------------------------------------
    EXPORT a53_grant_el1_cpuectlr_access
a53_grant_el1_cpuectlr_access FUNCTION
    mrs x0, ACTLR_EL3
    orr x0, #0x02
    msr ACTLR_EL3, x0
    ret
    ENDFUNC

    END
