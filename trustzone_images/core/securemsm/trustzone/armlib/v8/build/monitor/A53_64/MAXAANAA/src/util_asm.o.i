;============================================================================
;
; Monitor Specific General Purpose Functions
;
; Copyright 2014 by Qualcomm Technologies, Incorporated. All Rights Reserved
;============================================================================
;============================================================================
;
; EDIT HISTORY FOR MODULE
;
; $Header:
; $DateTime: 2018/02/07 00:37:16 $
; $Author: mplp4svc $
;
; This section contains comments describing changes made to the module.
; Notice that changes are listed in reverse chronological order.
;
;
; when who what, where, why
; -------- --- ---------------------------------------------------------
; 02/26/14 pre Initial Version
;============================================================================
    MACRO
    SetSecure $rx





        mov $rx, xzr
        msr SCR_EL3, $rx
        isb
    MEND
    MACRO
    SetNonSecure $rx





        mov $rx, #0x020:OR:0x001:OR:0x004:OR:0x400:OR:0x100
        msr SCR_EL3, $rx
        isb
    MEND
    MACRO
    PushTwo $x0, $x1
        stp $x1, $x0, [sp, #-0x10]!
    MEND
    MACRO
    PopTwo $x0, $x1
        ldp $x0, $x1, [sp], #0x10
    MEND
    MACRO
    PushAArch32NPRegs
        PushTwo x5, x6
        PushTwo x7, x8
        PushTwo x9, x10
        PushTwo x11, x12
        PushTwo x13, x14
        PushTwo x15, x16
        PushTwo x17, x18
        PushTwo x19, x20
        PushTwo x21, x22
        PushTwo x23, x24
        PushTwo x25, x26
        PushTwo x27, x28
        PushTwo x29, x30
    MEND
    MACRO
    PopAArch32NPRegs
        PopTwo x30, x29
        PopTwo x28, x27
        PopTwo x26, x25
        PopTwo x24, x23
        PopTwo x22, x21
        PopTwo x20, x19
        PopTwo x18, x17
        PopTwo x16, x15
        PopTwo x14, x13
        PopTwo x12, x11
        PopTwo x10, x9
        PopTwo x8, x7
        PopTwo x6, x5
    MEND
    MACRO
    PushAArch64CSRegs
        PushTwo x19, x20
        PushTwo x21, x22
        PushTwo x23, x24
        PushTwo x25, x26
        PushTwo x27, x28
        PushTwo x29, x30
    MEND
    MACRO
    PopAArch64CSRegs
        PopTwo x30, x29
        PopTwo x28, x27
        PopTwo x26, x25
        PopTwo x24, x23
        PopTwo x22, x21
        PopTwo x20, x19
    MEND
    MACRO
    PushAArch64PSRegs
        PushTwo x19, x18
        PushTwo x17, x16
        PushTwo x15, x14
        PushTwo x13, x12
        PushTwo x11, x10
        PushTwo x9, x8
        PushTwo x7, x6
        PushTwo x5, x4
        PushTwo x3, x2
        PushTwo x1, x0
    MEND
    MACRO
    PopAArch64PSRegs
        PopTwo x0, x1
        PopTwo x2, x3
        PopTwo x4, x5
        PopTwo x6, x7
        PopTwo x8, x9
        PopTwo x10, x11
        PopTwo x12, x13
        PopTwo x14, x15
        PopTwo x16, x17
        PopTwo x18, x19
    MEND
    MACRO
    WipeParamRegs
        mov x0, #0
        mov x1, #0
        mov x2, #0
        mov x3, #0
        mov x4, #0
        mov x5, #0
        mov x6, #0
        mov x7, #0
        mov x8, #0
    MEND
    MACRO
    WipeScratchRegs
        mov x9, #0
        mov x10, #0
        mov x11, #0
        mov x12, #0
        mov x13, #0
        mov x14, #0
        mov x15, #0
        mov x16, #0
        mov x17, #0
        mov x18, #0
    MEND
    MACRO
    WipeAllGPRegs
        WipeParamRegs
        WipeScratchRegs
        mov x19, #0
        mov x20, #0
        mov x21, #0
        mov x22, #0
        mov x23, #0
        mov x24, #0
        mov x25, #0
        mov x26, #0
        mov x27, #0
        mov x28, #0
        mov x29, #0
        mov x30, #0
    MEND
    MACRO
    CorruptMonRegs
        mov x0, #0xA
        mov x1, #0xB
        mov x2, #0xC
        mov x3, #0xD
        mov x4, #0xE
        mov x5, #0xF
        mov x6, #0xA
        mov x7, #0xB
        mov x8, #0xC
        mov x9, #0xD
        mov x10, #0xE
        mov x11, #0xF
        mov x12, #0xA
        mov x13, #0xB
        mov x14, #0xC
        mov x15, #0xD
        mov x16, #0xE
        mov x17, #0xF
        mov x18, #0xA
        mov x19, #0xB
        mov x20, #0xC
        mov x21, #0xD
        mov x22, #0xE
        mov x23, #0xF
        mov x24, #0xA
        mov x25, #0xB
        mov x26, #0xC
        mov x27, #0xD
        mov x28, #0xE
        mov x29, #0xF
        mov x30, #0xA
    MEND
  MACRO
  SpinlockObtain $lock_addr, $cpu, $work
    mrs $cpu, MPIDR_EL1
    and $cpu, $cpu, #0xFF
    add $cpu, #0x1
42 ldaxr $work, [$lock_addr]
    cbnz $work, %b42
    stlxr $work, $cpu, [$lock_addr]
    cbnz $work, %b42
    dmb sy
  MEND
  MACRO
  SpinlockRelease $lock_addr
    ; Ensure all reads/writes are visible to other observers before
    ; releasing the lock.
    dmb sy
    str xzr, [$lock_addr] ; Clear the lock
    dmb sy
  MEND
    MACRO
    CurCPUNumber $rx, $ry, $cpus_per_cluster
       mrs $rx, MPIDR_EL1
       and $ry, $rx, #0x0000FF00
       lsr $ry, #0x8
       mov $rx, #$cpus_per_cluster
       mul $ry, $rx
       mrs $rx, MPIDR_EL1
       and $rx, #0x000000FF
       add $rx, $ry
    MEND
    MACRO
    CurClusterNum $rx
       mrs $rx, MPIDR_EL1
       and $rx, #0x0000FF00
       lsr $rx, #0x8
    MEND
;============================================================================
;
; MACROS AND FUNCTIONS
;
;============================================================================
    PRESERVE8
    AREA UTIL_ASM, CODE, READONLY, ALIGN=3
;============================================================================
; void spinlock_obtain(spinlock_t* lock)
;============================================================================
    EXPORT spinlock_obtain
spinlock_obtain FUNCTION
    SpinlockObtain x0, x1, w2
    ret
    ENDFUNC
;============================================================================
; void spinlock_release(spinlock_t* lock)
;============================================================================
    EXPORT spinlock_release
spinlock_release FUNCTION
    SpinlockRelease x0
    ret
    ENDFUNC
;============================================================================
; el3_reg_t int_restore(void)
;============================================================================
    EXPORT int_disable_all
int_disable_all FUNCTION
    mrs x1, DAIF
    and x0, x1, #0x080:OR:0x040:OR:0x100
    orr x1, #0x080:OR:0x040:OR:0x100
    msr DAIF, x1 ; Apply the new int mask.
    ; x0 contains the A/I/F bits before masking
    ret
    ENDFUNC
;============================================================================
; void int_restore(el3_reg_t flags)
;============================================================================
    EXPORT int_restore
int_restore FUNCTION
    mov x2, #0x080:OR:0x040:OR:0x100
    and x0, x2
    mrs x1, DAIF
    bic x1, x2
    orr x1, x0 ; Set requested A/I/F bits
    msr DAIF, x1
    ret
    ENDFUNC
    END
