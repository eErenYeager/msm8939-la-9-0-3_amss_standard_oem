/**
   @file a53_pc.c
   @brief CPU specific power collapse routines
*/
/*===========================================================================
   Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/armlib/v8/proc/a53/src/a53_pc.c#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $


when       who      what, where, why
--------   ---     ------------------------------------------------------------
03/07/14   pre     Initial revision
=============================================================================*/

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include <comdef.h>
#include "memory_defs.h"
#include "cache.h"
#include "a53_el1_regs.h"
#include "sys_regs.h"
#include "a53_sys_fields.h"

/**
   @copydoc a53_pc_entry
 */
void a53_pc_entry(v_addr_t el3_sp_base, size_t stack_len)
{
  /* Disable EL3 data cache */
  disable_el3_dcache();

  /* clean and inval all l1 dcache */
  clean_and_inval_all_dcache_louis();

  /* Clean stack to PoC, since stack is cached and data cache was just
     disabled. */
  clean_dcache_region(el3_sp_base, stack_len);

  /* Clear CPUECTLR[SMPEN] */
  set_cpuectlr_el1(get_cpuectlr_el1() & ~CPUECTLR_EL1_SMP);

  __asm__ ("dsb sy" : : :);
  __asm__ ("isb" : : :);
}

/**
   CPU specific handling of power collapse

   @warning May only be called from EL3
 */
void a53_pc_fallthrough(v_addr_t el3_sp_base, size_t stack_len)
{
  /* Set CPUECTLR[SMPEN] */
  set_cpuectlr_el1(get_cpuectlr_el1() | CPUECTLR_EL1_SMP);
  __asm__ ("dsb sy" : : :);
  __asm__ ("isb" : : :);

  /* Invalidate stack to PoC, since stack is cached and data cache is
     about to be enabled. */
  inval_dcache_region(el3_sp_base, stack_len);

  /* Enable EL3 data cache */
  enable_el3_dcache();

  el3_set_scr_ns();
  __asm__ ("dsb sy" : : :);

  /* have to do TLB invalidations with SCR[NS=1] */
  /* TODO: check to see if TLB invalidation is necessary.  It is not
     mentioned in the TRM. */
  /* TODO: EL2 TLB invalidation causing some weird crash. */
  /*  __asm__ ("tlbi alle2is" : : :); */
  __asm__ ("tlbi alle1is" : : :);
  __asm__ ("dsb sy" : : :);

  el3_set_scr_s();
  __asm__ ("dsb sy" : : :);
  __asm__ ("isb" : : :);

  __asm__ ("tlbi alle3is" : : :);
  __asm__ ("tlbi alle1is" : : :);
  __asm__ ("dsb sy" : : :);
  __asm__ ("isb" : : :);
}
