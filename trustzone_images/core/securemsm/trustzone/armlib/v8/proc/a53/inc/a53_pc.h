/**
   @file a53_pc.h
   @brief CPU specific power collapse routines
*/
/*===========================================================================
   Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/armlib/v8/proc/a53/inc/a53_pc.h#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $


when       who      what, where, why
--------   ---     ------------------------------------------------------------
03/11/14   pre     Initial revision
=============================================================================*/
/**
   CPU specific handling of power collapse

   @warning May only be called from EL3
 */
void a53_pc_entry(v_addr_t el3_sp_base, size_t stack_len);

/**
   CPU specific handling of power collapse

   @warning May only be called from EL3
 */
void a53_pc_fallthrough(v_addr_t el3_sp_base, size_t stack_len);
