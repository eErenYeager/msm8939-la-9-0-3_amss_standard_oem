#ifndef A53_SYS_REGS_H
#define A53_SYS_REGS_H

/*============================================================================

                         A53 System Registers

GENERAL DESCRIPTION
 Define macros for reading and writing to the cp registers specific
 for the A53 processor.

Copyright 2014 by Qualcomm Technologies, Inc.  All Rights Reserved.
============================================================================*/


/*============================================================================

                            EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/armlib/v8/proc/a53/inc/a53_sys_regs.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
06/16/14   pre     Intial revision.
============================================================================*/

void a53_grant_el1_cpuectlr_access(void);

#endif /* A53_SYS_REGS_H */
