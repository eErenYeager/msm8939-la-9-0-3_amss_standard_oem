#ifndef A53_TLB_H
#define A53_TLB_H

/*============================================================================

                           A53 TLB Dumping

GENERAL DESCRIPTION
 APIs for dumping A53 TLBs

Copyright 2014 by Qualcomm Technologies, Inc.  All Rights Reserved.
============================================================================*/


/*============================================================================

                            EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/armlib/v8/proc/a53/inc/a53_tlb.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
07/25/14   pre     Intial revision.
============================================================================*/

#include <comdef.h>

#define A53_TLB_NUM_WAYS         3
#define A53_TLB_NUM_II          (1 << 8)
#define A53_TLB_NUM_WPE          4
#define A53_TLB_NUM_W           (A53_TLB_NUM_WAYS * \
                                 A53_TLB_NUM_II *   \
                                 A53_TLB_NUM_WPE)

typedef struct a53_tlb_s {
  uint32 data[A53_TLB_NUM_W];
} a53_tlb_t;

void a53_dump_tlb(void* dump_ptr);
uint32 a53_get_tlb_dump_len(void);

#endif /* A53_TLB_H */
