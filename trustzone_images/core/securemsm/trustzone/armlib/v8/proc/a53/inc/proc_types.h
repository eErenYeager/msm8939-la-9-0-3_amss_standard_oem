#ifndef PROC_TYPES_H
#define PROC_TYPES_H
/*=============================================================================

               Processor Specific Preprocessor Definitions


Copyright 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
=============================================================================*/


/*===========================================================================
                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/armlib/v8/proc/a53/inc/proc_types.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
02/24/14   pre     Initial Version
===========================================================================*/


/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/


/* Exclusive reservations granule */
#define ERG_WORDS        16
#define ERG_ALIGNMENT   (ERG_WORDS * 4)

#endif /* PROC_TYPES_H */
