#ifndef A53_CACHE_H
#define A53_CACHE_H

/*============================================================================

                           A53 CACHE Dumping

GENERAL DESCRIPTION
 APIs for dumping A53 caches

Copyright 2014 by Qualcomm Technologies, Inc.  All Rights Reserved.
============================================================================*/


/*============================================================================

                            EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/armlib/v8/proc/a53/inc/a53_cache.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
07/25/14   pre     Intial revision.
============================================================================*/

#include <comdef.h>

#define A53_L1_CACHE_LINESIZE_W         16

/* i-cache reads 20 bits per data read, but does two reads per
   iteration since it only iterates 16 bits at a time for some
   really weird reason. */
#define A53_L1_I_CACHE_NUM_WPR          1
#define A53_L1_I_CACHE_TAGSIZE_W        1
#define A53_L1_I_CACHE_NUM_WAYS         2
#define A53_L1_I_CACHE_NUM_SETS        (1 << 10)
#define A53_L1_I_CACHE_NUM_W           (A53_L1_I_CACHE_NUM_WAYS * \
                                        A53_L1_I_CACHE_NUM_SETS * \
                                        A53_L1_CACHE_LINESIZE_W)

/* d-cache reads 32 bits per data read and does two reads per
   iteration. */
#define A53_L1_D_CACHE_NUM_WPR          2
#define A53_L1_D_CACHE_TAGSIZE_W        2
#define A53_L1_D_CACHE_NUM_WAYS         4
#define A53_L1_D_CACHE_NUM_SETS        (1 << 9)
#define A53_L1_D_CACHE_NUM_W           (A53_L1_D_CACHE_NUM_WAYS * \
                                        A53_L1_D_CACHE_NUM_SETS * \
                                        A53_L1_CACHE_LINESIZE_W)

typedef enum
{
  A53_L1_I_CACHE,
  A53_L1_D_CACHE,
} a53_cache_t;

typedef struct a53_l1_i_cache_tag_s
{
  uint32 tag_addr : 28;
  uint32 ns       : 1;
  uint32 vmode    : 2;
  uint32 unused   : 1;
} __attribute__((packed)) a53_l1_i_cache_tag_t;

typedef struct a53_l1_i_cache_data_s
{
  uint32 inst     : 20;
  uint32 unused   : 12;
} __attribute__((packed)) a53_l1_i_cache_data_t;

typedef struct a53_l1_d_cache_tag_s
{
  uint32 moesi_s_0 : 2;
  uint32 os        : 1;
  uint32 oah       : 1;
  uint32 dcb       : 1;
  uint32 pb_0      : 1;
  uint32 reserved  : 25;
  uint32 tag_addr  : 29;
  uint32 ns        : 1;
  uint32 moesi_s_1 : 2;
  uint32 pb_1      : 1;
} __attribute__((packed)) a53_l1_d_cache_tag_t;

#if 0
typedef struct a53_l1_d_cache_tag_s
{
  uint32 pb_1      : 1;
  uint32 moesi_s_1 : 2;
  uint32 ns        : 1;
  uint32 tag_addr  : 29;
  uint32 reserved  : 25;
  uint32 pb_0      : 1;
  uint32 dcb       : 1;
  uint32 oah       : 1;
  uint32 os        : 1;
  uint32 moesi_s_0 : 2;
} __attribute__((packed)) a53_l1_d_cache_tag_t;
#endif

typedef struct a53_l1_i_cache_line_s
{
  a53_l1_i_cache_tag_t tag[A53_L1_I_CACHE_TAGSIZE_W];
  uint32 data[A53_L1_CACHE_LINESIZE_W];
} a53_l1_i_cache_line_t;

typedef struct a53_l1_i_cache_s
{
  a53_l1_i_cache_line_t line[A53_L1_I_CACHE_NUM_WAYS * A53_L1_I_CACHE_NUM_SETS];
} a53_l1_i_cache_t;

typedef struct a53_l1_d_cache_line_s
{
  uint32 tag[A53_L1_D_CACHE_TAGSIZE_W];
  uint32 data[A53_L1_CACHE_LINESIZE_W];
} a53_l1_d_cache_line_t;

typedef struct a53_l1_d_cache_s {
  a53_l1_d_cache_line_t line[A53_L1_D_CACHE_NUM_WAYS * A53_L1_D_CACHE_NUM_SETS];
} a53_l1_d_cache_t;

void a53_dump_cache(a53_cache_t cache, void* dump_ptr);
uint32 a53_get_cache_dump_len(a53_cache_t cache);

#endif /* A53_CACHE_H */
