#ifndef A53_SYS_FIELDS
#define A53_SYS_FIELDS

#define CPUECTLR_EL1_SMP_BIT               (1ULL << 6)

#define ACTLR_EL3_CPUACTLR_BIT             0x01
#define ACTLR_EL3_CPUECTLR_BIT             0x02
#define ACTLR_EL3_L2CTLR_BIT               0x10
#define ACTLR_EL3_L2ECTLR_BIT              0x20
#define ACTLR_EL3_L2ACTLR_BIT              0x40

#define DCTROR_EL3_WAY_BMSK                0x3
#define DCTROR_EL3_WAY_SHFT                29
#define DCTROR_EL3_SET_BMSK                0xFF
#define DCTROR_EL3_SET_SHFT                6

#define DCDROR_EL3_WAY_BMSK                DCTROR_EL3_WAY_BMSK
#define DCDROR_EL3_WAY_SHFT                DCTROR_EL3_WAY_SHFT
#define DCDROR_EL3_SET_BMSK                DCTROR_EL3_SET_BMSK
#define DCDROR_EL3_SET_SHFT                DCTROR_EL3_SET_SHFT
#define DCDROR_EL3_DWDO_BMSK               0x7
#define DCDROR_EL3_DWDO_SHFT               3

#define ICTROR_EL3_WAY_BMSK                0x1
#define ICTROR_EL3_WAY_SHFT                30
#define ICTROR_EL3_SET_BMSK                0x1FF
#define ICTROR_EL3_SET_SHFT                6

#define ICDROR_EL3_WAY_BMSK                ICTROR_EL3_WAY_BMSK
#define ICDROR_EL3_WAY_SHFT                ICTROR_EL3_WAY_SHFT
#define ICDROR_EL3_SET_BMSK                ICTROR_EL3_SET_BMSK
#define ICDROR_EL3_SET_SHFT                ICTROR_EL3_SET_SHFT
#define ICDROR_EL3_DWDO_BMSK               0xF
#define ICDROR_EL3_DWDO_SHFT               2

#define TDROR_EL3_WAY_BMSK                 0x3
#define TDROR_EL3_WAY_SHFT                 30
#define TDROR_EL3_II_BMSK                  0xFF
#define TDROR_EL3_II_SHFT                  0

#endif /* A53_SYS_FIELDS */
