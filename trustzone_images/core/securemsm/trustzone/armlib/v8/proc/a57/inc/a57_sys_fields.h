#ifndef A57_SYS_FIELDS
#define A57_SYS_FIELDS

#define CPUECTLR_EL1_SMP_BIT               (1ULL << 6)
#define CPUECTLR_EL1_DIS_TWDA_PREFETCH_BIT (1ULL << 38)
#define CPUECTLR_EL1_L2_IPF_DIS_SHFT       35
#define CPUECTLR_EL1_L2_IPF_DIS_BMSK       0x3ULL
#define CPUECTLR_EL1_L2_LSPF_DIS_SHFT      32
#define CPUECTLR_EL1_L2_LSPF_DIS_BMSK      0x3ULL

#define ACTLR_EL3_CPUACTLR_BIT             0x01
#define ACTLR_EL3_CPUECTLR_BIT             0x02
#define ACTLR_EL3_L2CTLR_BIT               0x10
#define ACTLR_EL3_L2ECTLR_BIT              0x20
#define ACTLR_EL3_L2ACTLR_BIT              0x40

#endif /* A57_SYS_FIELDS */
