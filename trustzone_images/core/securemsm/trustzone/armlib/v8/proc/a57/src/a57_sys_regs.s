;============================================================================
;
;         Accessor Functions for Processor Specific System Registers
;
; Copyright 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved
;============================================================================

;============================================================================
;
;                       EDIT HISTORY FOR MODULE
;
; $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/armlib/v8/proc/a57/src/a57_sys_regs.s#1 $
; $DateTime: 2018/02/07 00:37:16 $
; $Author: mplp4svc $
;
; This section contains comments describing changes made to the module.
; Notice that changes are listed in reverse chronological order.
;
;
; when      who     what, where, why
; --------  ---     ---------------------------------------------------------
; 03/11/14  pre     Initial revision.
;============================================================================

#include "a57_sys_fields.h"

    PRESERVE8
    AREA    A57_SYS_REG, CODE, READONLY, ALIGN=3

;--------------------------------------------------------------------
; Updates ACTLR_EL3[CPUECTLR_EL1]
;
; void a57_grant_el1_cpuectlr_access(void)
;--------------------------------------------------------------------
    EXPORT a57_grant_el1_cpuectlr_access
a57_grant_el1_cpuectlr_access FUNCTION
    mrs     x0, ACTLR_EL3
    orr     x0, #ACTLR_EL3_CPUECTLR_BIT
    msr     ACTLR_EL3, x0
    ret
    ENDFUNC


    END