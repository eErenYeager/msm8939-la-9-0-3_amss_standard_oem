;-----------------------------------------------------------------------
; SYSINI
; Version: 3.7
; CPU:     Cortex-A57
; Mode:    AArch64
;-----------------------------------------------------------------------

#define SOC_ID_8916    0x20000100
#define SOC_ID_8939    0x20010100
#define SOC_ID_8994_R1 0x000B0100
#define SOC_ID_8994_R2 0x000B0200
#define SOC_ID_8992    0x000E0100
#define CPTR_EL3_TFP   0x400
#define CPACR_FPEN     0x300000
#define FPEXC_EL2_EN   0x40000000


#ifndef SYSINI_EN_828024_WA
#define SYSINI_EN_828024_WA  0
#endif

#ifndef SYSINI_EN_829519_WA
#define SYSINI_EN_829519_WA  0
#endif

#ifndef SYSINI_EN_829520_WA
#define SYSINI_EN_829520_WA  0
#endif

#ifndef SYSINI_EN_832569_WA
#define SYSINI_EN_832569_WA  0
#endif

#ifndef SYSINI_EN_VMSR_WA
#define SYSINI_EN_VMSR_WA  0
#endif

;-----------------------------------------------------------------------
   AREA RO, CODE, READONLY, ALIGN=3
   ;ARM64

   ; Export functions and global variables
   EXPORT a57_aarch64_sysini
   PRESERVE8
;-----------------------------------------------------------------------

;-----------------------------------------------------------------------
; a57_aarch64_sysini
; Inputs- None
; Description - This function initializes several implementation defined
; CPU registers. Fixes for several ARM errata are also applied here.
;-----------------------------------------------------------------------

a57_aarch64_sysini
   LDR X1, =tcsr_hw_version
   STR X0, [X1]

   ; ----------------------------------------------------------
   ; Set SMP Bit - must be set before caches and MMU are enabled
   ; CPUECTLR[6]
   ; ----------------------------------------------------------
   MRS X0, S3_1_C15_C2_1 ; Read EL1 CPU Extended Control Register
   ORR X0, X0, #(0x1 << 6) ; Set SMP bit
   MSR S3_1_C15_C2_1, X0 ; Write EL1 CPU Extended Control Register
   ISB SY
   DSB SY

   ; ----------------------------------------------------------
   ; Unset TFP bit in CPTR_EL3
   ; The default value of this is expected to be 0 in A53 and 1 in A57
   ; Instructions that access registers associated with floating-point and
   ; Advanced SIMD execution are not trapped
   ; ----------------------------------------------------------
   MRS X0, CPTR_EL3; Read CPTR_EL3
   MOV X1, #CPTR_EL3_TFP
   BIC X0, X0, X1
   MSR CPTR_EL3, X0; Write EL3 Architectural Feature Trap Register
   ISB SY

   ;------------------------------------------------------------
   ; Disable VFP/SIMD functionality, will be enabled when accessed first.
   ; Exception will be trapped into EL1 but not to EL3
   ;------------------------------------------------------------
   MRS X0, CPACR_EL1
   MOV X1, #CPACR_FPEN
   BIC X0, X0, X1
   MSR CPACR_EL1, X0

   MRS X0, FPEXC32_EL2
   MOV X1, #FPEXC_EL2_EN
   BIC X0, X0, X1
   MSR FPEXC32_EL2, X0
   ISB SY

   ; ----------------------------------------------------------
   ; Disable Secure Invasive and Non-Invasive Debug
   ; through SDER32_EL3 - Secure Debug Enable Register
   ; This register is specific to AArch32 EL1 Secure
   ; This should ideally be set by Secure AArch32 monitor code
   ; ----------------------------------------------------------
   MRS X0, SDER32_EL3 ; Read SDER32_EL3 into X0
   MOV X1, #3
   BIC X0, X0, X1 ; Disable bits [0] and [1]
   MSR SDER32_EL3, X0 ; Write X0 to SDER32_EL3
   ISB SY

   ; ----------------------------------------------------------
   ; Enable L1/L2 ECC and parity
   ; ----------------------------------------------------------
   MRS X0, S3_1_c11_c0_2; Read L2 Control Register
   ORR X0, X0, #1<<21
   MSR S3_1_c11_c0_2, X0; Write L2 Control Register
   ISB SY

   ; ----------------------------------------------------------
   ; Set debugging features in MDCR_EL3
   ; Some of the debug features are set to permitted
   ; and some others are set to trap to EL3
   ; Please refer to ARM ARM v8 for details
   ; ----------------------------------------------------------
   MRS X0, MDCR_EL3 ; Read MDCR_EL3 into X0
   MOV X1, #1<<6
   BIC X0, X0, X1 ; Clear bit 6
   MOV X1, #3<<9
   BIC X0, X0, X1 ; Clear bits 9 and 10
   MOV X1, #3<<14
   BIC X0, X0, X1; Clear bits 14 and 15
   ORR X0, X0, #1<<16 ; Set bit 16
   MOV X1, #1<<17
   BIC X0, X0, X1; Clear bit 17
   ORR X0, X0, #3<<20 ; Set bits 20 and 21
   MSR MDCR_EL3, X0 ; Write X0 to MDCR_EL3
   ISB SY

   ; ----------------------------------------------------------
   ; From Cortex-A57 TRM Section 7.7.11
   ; When the Cortex-A57 MPCore processor is used with the ARM CCI-400 in an
   ; ACE-based system, ARM recommends that you set L2ACTLR_EL1[3] to 1 to
   ; disable Evict transactions. The reset value of L2ACTLR[3] is 0 in
   ; Cortex-A57 ACE configurations.
   ; ----------------------------------------------------------
   MRS X0, S3_1_C15_C0_0; Read L2ACTLR_EL1 into X0
   ORR X0, X0, #1<<3 ;Set bit 3
   MSR S3_1_C15_C0_0, X0; Write X0 to L2ACTLR_EL1
   ISB SY

   ; ----------------------------------------------------------
   ; Enable CPU Dynamic Retention
   ; 8994_R2 supports C2d mode and this requires programming of the CPUECTLR_EL1
   ; Bits [2:0] need to be set to 0b111 to enable retention entry after 512
   ; generic arch timer ticks
   ;
   ; Prism CR: 726838
   ; Applicable to: 8994_R1-NO, 8994_R2-YES
   ; ----------------------------------------------------------
   LDR X1, tcsr_hw_version; Read SoC_ID into X1
   LDR X0, =SOC_ID_8994_R2
   CMP X0, X1
   B.EQ l_C2d_EN_start
   B l_C2d_EN_exit
l_C2d_EN_start
   MRS X0, S3_1_c15_c2_1; Read EL1 CPU Extended Control Register
   MOV X1, #7
   BFI X0, X1, #0, #3
   MSR S3_1_c15_c2_1, X0; Write EL1 CPU Extended Control Register
   ISB SY
l_C2d_EN_exit

   ; ----------------------------------------------------------
   ; Enable L2 Dynamic Retention
   ; 8994_R2 supports D2d mode and this requires programming of the L2ECTLR_EL1
   ; Bits [2:0] need to be set to 0b111 to enable retention entry after 512
   ; generic arch timer ticks
   ;
   ; Prism CR: 726838
   ; Applicable to: 8994_R1-NO, 8994_R2-YES
   ; ----------------------------------------------------------
   LDR X1, tcsr_hw_version; Read SoC_ID into X1
   LDR X0, =SOC_ID_8994_R2
   CMP X0, X1
   B.EQ l_D2d_EN_start
   B l_D2d_EN_exit
l_D2d_EN_start
   MRS X0, S3_1_c11_c0_3; Read L2 Extended Control Register
   MOV X1, #7
   BFI X0, X1, #0, #3
   MSR S3_1_c11_c0_3, X0; Write L2 Extended Control Register
   ISB SY
l_D2d_EN_exit

   ; ----------------------------------------------------------
   ; From Cortex-A57 MPCore Software Developers Errata Notice
   ; Erratum #815569
   ; Store-Exclusive to GRE memory type might always fail exclusivity check
   ;
   ; Workaround:
   ; Set CPUACTLR[54] bit. This will force the A57 to treat all GRE and
   ; nGRE memory accesses as nGnRE memory accesses
   ; Side effect: Disables optimization for GRE and nGRE load/store
   ; Fault Status: Present in: r0p0, r0p1 Fixed in r1p0
   ;
   ; Prism CR: 614378
   ; ClearQuest CR: QCTDD01066814
   ;
   ; Applicable to: 8994_R1
   ; ----------------------------------------------------------
   LDR X1, tcsr_hw_version; Read SoC_ID into X1
   LDR X0, =SOC_ID_8994_R1
   CMP X0, X1
   B.EQ l_815569_start
   B l_815569_exit
l_815569_start
   MRS X0, S3_1_c15_c2_0; Read EL1 CPU Auxiliary Control Register
   ORR X0, X0, #1<<54 ;bit 54 is set to 1
   MSR S3_1_c15_c2_0, X0; Write EL1 CPU Auxiliary Control Register
   ISB SY
l_815569_exit

   ; ----------------------------------------------------------
   ; From Cortex-A53 MPCore Software Developers Errata Notice
   ; Erratum #819472
   ; Store exclusive instructions might cause data corruption
   ;
   ; Workaround:
   ; On a Cortex-A57 processor it is possible to set bit 44 in the CPUACTLR_EL1
   ; register to automatically convert all DCCMVAC and DCCMVAU instructions
   ; into DCCIMVAC instructions
   ;
   ; Prism CR: 610928
   ; ClearQuest CR: QCTDD01066297
   ;
   ; Fix Applicable to: 8994_R1
   ; Although this erratum is for Cortex-A53 processor, the workaround requires
   ; modifications of Cortex-A57 system registers that is the master which will
   ; generate a snoop into the Cortex-A53 cluster
   ; ----------------------------------------------------------
   LDR X1, tcsr_hw_version; Read SoC_ID into X1
   LDR X0, =SOC_ID_8994_R1
   CMP X0, X1
   B.EQ l_819472_start
   B l_819472_exit
l_819472_start
   MRS X0, S3_1_c15_c2_0; Read EL1 CPU Auxiliary Control Register
   ORR X0, X0, #1<<44 ;bit 44 is set to 1
   MSR S3_1_c15_c2_0, X0; Write EL1 CPU Auxiliary Control Register
   ISB SY
l_819472_exit


   ; ----------------------------------------------------------
   ; From Cortex-A53 MPCore Software Developers Errata Notice
   ; Erratum #824069
   ; Cache line might not be marked as clean after a CleanShared snoop
   ;
   ; Workaround:
   ; On a Cortex-A57 processor it is possible to set bit 44 in the CPUACTLR_EL1
   ; register to automatically convert all DCCMVAC and DCCMVAU instructions
   ; into DCCIMVAC instructions
   ;
   ; Prism CR: -----
   ; ClearQuest CR: QCTDD01251383
   ;
   ; Fix Applicable to: 8994_R2-YES
   ; ----------------------------------------------------------
   ; *UPDATE*
   ; Clarification from ARM
   ; This erratum will not cause any failure with CCI-400 or CCN-504.


   ; ----------------------------------------------------------
   ; From Cortex-A57 MPCore Software Developers Errata Notice
   ; Erratum #823222
   ; L2 Dirty RAM corruption might occur because of a replayed eviction
   ; following a partial write to L2 cache
   ;
   ; Workaround:
   ; Set the Tag RAM setup cycle L2CTLR[9]=1 or L2CTLR[8:6]>1, which will
   ; increase the overall latency to be greater than 2 cycles and eliminate
   ; the necessary conditions for this erratum
   ; We are setting L2CTLR[8:6] 0b010 which sets the L2 tag RAM latency as
   ; 3 cycles
   ;
   ; Prism CR: ----
   ; ClearQuest CR: QCTDD01140739
   ;
   ; Fix Applicable to: 8994_R1-YES, 8994_R2-NO
   ; ----------------------------------------------------------
   LDR X1, tcsr_hw_version; Read SoC_ID into X1
   LDR X0, =SOC_ID_8994_R1
   CMP X0, X1
   B.EQ l_823222_start
   B l_823222_exit
l_823222_start
   MRS X0, S3_1_c11_c0_2; Read L2 Control Register
   MOV X1, #2
   BFI X0, X1, #6, #3
   MSR S3_1_c11_c0_2, X0; Write L2 Control Register
   ISB SY
l_823222_exit

   ; ----------------------------------------------------------
   ; From Cortex-A57 MPCore Software Developers Errata Notice
   ; Erratum #822226
   ; A spin loop of exclusive accesses might starve other CPUs with
   ; overlapping exclusive accesses
   ;
   ; Workaround:
   ; Set CPUACTLR_EL1[51]. This will disable speculative ReadUnique
   ; transactions for LDREX instructions.
   ;
   ; Prism CR: 657900
   ; ClearQuest CR: QCTDD01140714
   ;
   ; Fix Applicable to: 8994_R1-YES, 8994_R2-YES
   ; ----------------------------------------------------------
   LDR X1, tcsr_hw_version; Read SoC_ID into X1
   LDR X0, =SOC_ID_8994_R1
   CMP X0, X1
   B.EQ l_822226_start
   LDR X0, =SOC_ID_8994_R2
   CMP X0, X1
   B.EQ l_822226_start
   B l_822226_exit
l_822226_start
   MRS X0, S3_1_c15_c2_0; Read EL1 CPU Auxiliary Control Register
   ORR X0, X0, #1<<51
   MSR S3_1_c15_c2_0, X0; Write EL1 CPU Auxiliary Control Register
   ISB SY
l_822226_exit

   ; ----------------------------------------------------------
   ; From Cortex-A57 MPCore Software Developers Errata Notice
   ; Erratum #823970
   ; New L2 Dynamic Retention errata
   ;
   ; Workaround:
   ; Disable A57 L2 dynamic retention.
   ;
   ; Prism CR: ----
   ; ClearQuest CR: QCTDD01240663
   ;
   ; Fix Applicable to: 8994_R1-YES, 8994_R2-NO
   ; ----------------------------------------------------------
   LDR X1, tcsr_hw_version; Read SoC_ID into X1
   LDR X0, =SOC_ID_8994_R1
   CMP X0, X1
   B.EQ l_823970_start
   B l_823970_exit
l_823970_start
   MRS X0, S3_1_c11_c0_3; Read L2 Extended Control Register
   MOV X1, #7
   BIC X0, X0, X1
   MSR S3_1_c11_c0_3, X0; Write L2 Extended Control Register
   ISB SY
l_823970_exit

   ; ----------------------------------------------------------
   ; From Cortex-A57 MPCore Software Developers Errata Notice
   ; Erratum #826974
   ; A57 might violate the memory ordering between an older store and a
   ; younger load when executing a DMB ALL/ALL instruction
   ;
   ; Workaround:
   ; Set CPUACTLR[59] bit. This will disable speculative load execution
   ; ahead of a DMB
   ;
   ; Prism CR: 660485
   ; ClearQuest CR: QCTDD01282616
   ;
   ; Applicable to: 8994_R1-YES, 8994_R2-YES
   ; ----------------------------------------------------------
   LDR X1, tcsr_hw_version; Read SoC_ID into X1
   LDR X0, =SOC_ID_8994_R1
   CMP X0, X1
   B.EQ l_826974_start
   LDR X0, =SOC_ID_8994_R2
   CMP X0, X1
   B.EQ l_826974_start
   B l_826974_exit
l_826974_start
   MRS X0, S3_1_c15_c2_0; Read EL1 CPU Auxiliary Control Register
   ORR X0, X0, #1<<59 ;bit 59 is set to 1
   MSR S3_1_c15_c2_0, X0; Write EL1 CPU Auxiliary Control Register
   ISB SY
l_826974_exit

   ; ----------------------------------------------------------
   ; From Cortex-A57 MPCore Software Developers Errata Notice
   ; Erratum #826977
   ; L2 Data RAM corruption might occur because of a streaming write to GRE
   ; memory which detects a hazard
   ; Workaround:
   ; Set the Tag RAM setup cycle L2CTLR[9]=1 or L2CTLR[8:6]>1, which will
   ; increase the overall latency to be greater than 2 cycles and eliminate
   ; the necessary conditions for this erratum
   ;
   ; Prism CR: 655790
   ; ClearQuest CR: QCTDD01282610
   ;
   ; Applicable to: 8994_R1-YES, 8994_R2-TBD(HW Fix expected)
   ; ----------------------------------------------------------
   ; No action needs to be taken due to the fix provided for Erratum #823222

#if SYSINI_EN_828024_WA
   ; ----------------------------------------------------------
   ; From Cortex-A57 MPCore Software Developers Errata Notice
   ; Erratum #828024
   ; TLB maintenance operations might not be synchronized by DSB instruction
   ; Workaround:
   ; By following the workaround below, this erratum will not affect cacheable
   ; operations, therefore it becomes a Category C erratum that only affects
   ; store transactions with write-through, non-cacheable, device, or strongly-
   ; ordered memory attributes. Note that disabling write streaming will have
   ; an adverse effect on the performance of memset and memcpy operations.
   ;  1) Set CPUACTLR_EL1[49] = 1 // disable non-allocate hint of write-back-no-allocate memory type
   ;  2) Set CPUACTLR_EL1[26:25] = 2b11 // disable write streaming no L1-allocate threshold
   ;  3) Set CPUACTLR_EL1[28:27] = 2b11 // disable write streaming no-allocate threshold
   ;
   ; Prism CR: TBD
   ; ClearQuest CR: QCTDD01342547
   ;
   ; Applicable to: 8994_R1-YES, 8994_R2-TBD(HW Fix expected)
   ; ----------------------------------------------------------
   LDR X1, tcsr_hw_version; Read SoC_ID into X1
   LDR X0, =SOC_ID_8994_R1
   CMP X0, X1
   B.EQ l_828024_start
   B l_828024_exit
l_828024_start
   MRS X0, S3_1_c15_c2_0; Read EL1 CPU Auxiliary Control Register
   ORR X0, X0, #1<<49 ;Set bit 49=1
   ORR X0, X0, #0xF<<25 ;Set bits 25,26,27,28 to 1
   MSR S3_1_c15_c2_0, X0; Write EL1 CPU Auxiliary Control Register
   ISB SY
l_828024_exit
#endif

   ; ----------------------------------------------------------
   ; For Cortex-A57 the following settings are done:
   ; - Set L2 data RAM latency as 3 cycles i.e. L2CTLR[2:0]=0b010
   ; - Set L2 tag  RAM latency as 3 cycles i.e. L2CTLR[8:6]=0b010
   ; - Set L2 data RAM setup as 1 cycle i.e. L2CTLR[5]=1
   ; - Set L2 tag  RAM setup as 1 cycle i.e. L2CTLR[9]=1
   ;
   ; The above values are from Design Team and are as per 8994_R1 and 8994_R2
   ; POR. They are required to achieve target Fmax on A57.
   ; The settings are applicable to 8994_R2 as well, but may revisit the same
   ; after 8994_R2 Si is back.
   ;
   ; Applicable to: 8994_R1, 8994_R2, 8992
   ; ----------------------------------------------------------
   LDR X1, tcsr_hw_version; Read SoC_ID into X1
   LDR X0, =SOC_ID_8994_R1
   CMP X0, X1
   B.EQ l_L2RAM_latency_setup_start
   LDR X0, =SOC_ID_8994_R2
   CMP X0, X1
   B.EQ l_L2RAM_latency_setup_start
   LDR X0, =SOC_ID_8992
   CMP X0, X1
   B.EQ l_L2RAM_latency_setup_start
   B l_L2RAM_latency_setup_exit
l_L2RAM_latency_setup_start
   MRS X0, S3_1_c11_c0_2; Read L2 Control Register
   MOV X1, #2
   BFI X0, X1, #0, #3 ; Set bits [2:0] to 0b010
   BFI X0, X1, #6, #3 ; Set bits [8:6] to 0b010
   ORR X0, X0, #1<<5 ; Set bit [5] to 1
   ORR X0, X0, #1<<9 ; Set bit [9] to 1
   MSR S3_1_c11_c0_2, X0; Write L2 Control Register
   ISB SY
l_L2RAM_latency_setup_exit

#if SYSINI_EN_829519_WA
   ; ----------------------------------------------------------
   ; From Cortex-A57 MPCore Software Developers Errata Notice
   ; Erratum #829519
   ; Cortex-A57 might starve other masters when executing continuous stores
   ; to the same bytes
   ;
   ; Workaround:
   ; Set CPUACTLR_EL1[28:27]=2'b11 and CPUACTLR_EL1[26:25]=2'b11 to turn off
   ; store streaming.
   ; However, this has performance degradation on some memory store streaming
   ; workloads.
   ;
   ; Prism CR:
   ; ClearQuest CR: QCTDD01420171
   ;
   ; Applicable to: 8994_R1, 8994_R2, 8992
   ; ----------------------------------------------------------
   LDR X1, tcsr_hw_version; Read SoC_ID into X1
   LDR X0, =SOC_ID_8994_R1
   CMP X0, X1
   B.EQ l_829519_start
   LDR X0, =SOC_ID_8994_R2
   CMP X0, X1
   B.EQ l_829519_start
   LDR X0, =SOC_ID_8992
   CMP X0, X1
   B.EQ l_829519_start
   B l_829519_exit
l_829519_start
   MRS X0, S3_1_c15_c2_0; Read EL1 CPU Auxiliary Control Register
   ORR X0, X0, #0xF<<25 ; bits 28:25 are set to 1
   MSR S3_1_c15_c2_0, X0; Write EL1 CPU Auxiliary Control Register
   ISB SY
l_829519_exit
#endif

#if SYSINI_EN_829520_WA
   ; ----------------------------------------------------------
   ; From Cortex-A57 MPCore Software Developers Errata Notice
   ; Erratum #829520
   ; Code bounded by indirect conditional branch might corrupt instruction
   ; stream
   ;
   ; Workaround:
   ; The CPUACTLR_EL1 Disable Indirect Predictor bit will prevent this erratum
   ; from occurring. Set CPUACTLR_EL1[4] = 1'b1 to disable the Indirect
   ; Predictor. Note that disabling the Indirect Predictor might cause some
   ; performance degradation depending on the application.
   ;
   ; Prism CR:
   ; ClearQuest CR: QCTDD01420616
   ;
   ; Applicable to: 8994_R1-YES, 8994_R2-NO (HW fix expected)
   ;                8992-NO(A57 REVIDR[9]=1)
   ; ----------------------------------------------------------
   LDR X1, tcsr_hw_version; Read SoC_ID into X1
   LDR X0, =SOC_ID_8994_R1
   CMP X0, X1
   B.EQ l_829520_start
   B l_829520_exit
l_829520_start
   MRS X0, S3_1_c15_c2_0; Read EL1 CPU Auxiliary Control Register
   ORR X0, X0, #1<<4 ; bit 4 is set to 1
   MSR S3_1_c15_c2_0, X0; Write EL1 CPU Auxiliary Control Register
   ISB SY
l_829520_exit
#endif

#if SYSINI_EN_832569_WA
   ; ----------------------------------------------------------
   ; From Cortex-A57 MPCore Software Developers Errata Notice
   ; Erratum #832569
   ; Cortex-A57 L1 prefetch works inefficiently for a load stream whose address
   ; is mapped to a page size of 64K or larger
   ;
   ; Workaround:
   ; Set bit[47] of CPUACTLR_EL1 to make the L1 hardware prefetcher treat all
   ; page sizes of the loads as 4KB pages
   ; This Workaround can have performance impacts
   ;
   ; Prism CR: 712354
   ; ClearQuest CR: QCTDD01539101
   ;
   ; Applicable to: 8994_R1-YES, 8994_R2-YES, 8992-YES
   ;
   ; ----------------------------------------------------------
   LDR X1, tcsr_hw_version; Read SoC_ID into X1
   LDR X0, =SOC_ID_8994_R1
   CMP X0, X1
   B.EQ l_832569_start
   LDR X0, =SOC_ID_8994_R2
   CMP X0, X1
   B.EQ l_832569_start
   LDR X0, =SOC_ID_8992
   CMP X0, X1
   B.EQ l_832569_start
   B l_832569_exit
l_832569_start
   MRS X0, S3_1_c15_c2_0; Read EL1 CPU Auxiliary Control Register
   ORR X0, X0, #1<<47 ; bit 47 is set to 1
   MSR S3_1_c15_c2_0, X0; Write EL1 CPU Auxiliary Control Register
   ISB SY
l_832569_exit
#endif

#if SYSINI_EN_VMSR_WA
   ; ----------------------------------------------------------
   ; From Cortex-A57 MPCore Software Developers Errata Notice
   ; Erratum #TBD
   ; VMSR FPSCR functional failure or deadlock
   ;
   ; Workaround:
   ; Set CPUACTLR[38] to 1, which forces FPSCR write flush. Note that in some
   ; cases where a flush is unnecessary this could impact performance.
   ;
   ; Prism CR: 715889
   ; ClearQuest CR: QCTDD01575143
   ;
   ; Applicable to: 8994_R1-YES, 8994_R2-YES, 8992-YES
   ;
   ; ----------------------------------------------------------
   LDR X1, tcsr_hw_version; Read SoC_ID into X1
   LDR X0, =SOC_ID_8994_R1
   CMP X0, X1
   B.EQ l_VMSR_WA_start
   LDR X0, =SOC_ID_8994_R2
   CMP X0, X1
   B.EQ l_VMSR_WA_start
   LDR X0, =SOC_ID_8992
   CMP X0, X1
   B.EQ l_VMSR_WA_start
   B l_VMSR_WA_exit
l_VMSR_WA_start
   MRS X0, S3_1_c15_c2_0; Read EL1 CPU Auxiliary Control Register
   ORR X0, X0, #1<<38 ; bit 38 is set to 1
   MSR S3_1_c15_c2_0, X0; Write EL1 CPU Auxiliary Control Register
   ISB SY
l_VMSR_WA_exit
#endif

   DSB SY ;
   ISB SY ;Make sure all changes are propagated before returning from sysini

   RET

   AREA A57_SYSINI_DATA, READWRITE, ALIGN=3
tcsr_hw_version SPACE 0x8

   END ; End of sysini
