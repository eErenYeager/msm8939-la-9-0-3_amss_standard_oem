/**
   @file a57_pc.h
   @brief CPU specific power collapse routines
*/
/*===========================================================================
   Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/armlib/v8/proc/a57/inc/a57_pc.h#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $


when       who      what, where, why
--------   ---     ------------------------------------------------------------
04/10/14   pre     Initial revision
=============================================================================*/
/**
   CPU specific handling of power collapse

   @warning May only be called from EL3
 */
void a57_pc_entry(el3_reg_t* cpuectlr_el1,
                  v_addr_t stack_base,
                  size_t stack_len);

/**
   CPU specific handling of power collapse

   @warning May only be called from EL3
 */
void a57_pc_fallthrough(el3_reg_t* cpuectlr_el1,
                        v_addr_t stack_base,
                        size_t stack_len);
