/**
   @file a57_pc.c
   @brief CPU specific power collapse routines
*/
/*===========================================================================
   Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/armlib/v8/proc/a57/src/a57_pc.c#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $


when       who      what, where, why
--------   ---     ------------------------------------------------------------
04/10/14   pre     Initial revision
=============================================================================*/

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include <comdef.h>
#include "memory_defs.h"
#include "cache.h"
#include "a57_el1_regs.h"
#include "sys_regs.h"
#include "a57_sys_fields.h"

/**
   @copydoc a53_pc_entry
 */
void a57_pc_entry(el3_reg_t* cpuectlr_el1,
                  v_addr_t stack_base,
                  size_t stack_len)
{
  /* Disable EL3 data cache */
  disable_el3_dcache();

  /* TODO: doesn't make sense to -clear- the disable table walk
           descriptor access prefetch.  Manual may be wrong? */
  /* Clear CPUECTLR[38,36:35,33:32] */
  *cpuectlr_el1 = get_cpuectlr_el1();

  set_cpuectlr_el1(*cpuectlr_el1 &
                   ~((CPUECTLR_EL1_L2_IPF_DIS_BMSK <<
                      CPUECTLR_EL1_L2_IPF_DIS_SHFT) |
                     (CPUECTLR_EL1_L2_LSPF_DIS_BMSK <<
                      CPUECTLR_EL1_L2_LSPF_DIS_SHFT) |
                     CPUECTLR_EL1_DIS_TWDA_PREFETCH_BIT));

  __asm__ ("isb" : : :);
  __asm__ ("dsb sy" : : :);

  /* clean and inval all l1 dcache */
  clean_and_inval_all_dcache_louis();

  /* Clean stack to PoC, since stack is cached and data cache was just
     disabled. */
  clean_dcache_region(stack_base, stack_len);

  /* Clear CPUECTLR[SMPEN] */
  set_cpuectlr_el1(get_cpuectlr_el1() & ~CPUECTLR_EL1_SMP_BIT);

  __asm__ ("isb" : : :);
  __asm__ ("dsb sy" : : :);

  /* TODO: Set DBGOSDLR[DLK].  This is done by HLOS I think? */
}

/**
   CPU specific handling of power collapse

   @warning May only be called from EL3
 */
void a57_pc_fallthrough(el3_reg_t* cpuectlr_el1,
                        v_addr_t stack_base,
                        size_t stack_len)
{
  /* Set CPUECTLR[SMPEN] */
  set_cpuectlr_el1(get_cpuectlr_el1() | CPUECTLR_EL1_SMP_BIT);
  __asm__ ("isb" : : :);
  __asm__ ("dsb sy" : : :);

  /* Set bits cleared in CPUECTLR during power collapse back to
     pre-WFI settings */
  set_cpuectlr_el1((get_cpuectlr_el1() & ~((CPUECTLR_EL1_L2_IPF_DIS_BMSK <<
                                            CPUECTLR_EL1_L2_IPF_DIS_SHFT) |
                                           (CPUECTLR_EL1_L2_LSPF_DIS_BMSK <<
                                            CPUECTLR_EL1_L2_LSPF_DIS_SHFT) |
                                           CPUECTLR_EL1_DIS_TWDA_PREFETCH_BIT))|
                   (*cpuectlr_el1 & ((CPUECTLR_EL1_L2_IPF_DIS_BMSK <<
                                      CPUECTLR_EL1_L2_IPF_DIS_SHFT) |
                                     (CPUECTLR_EL1_L2_LSPF_DIS_BMSK <<
                                      CPUECTLR_EL1_L2_LSPF_DIS_SHFT) |
                                     CPUECTLR_EL1_DIS_TWDA_PREFETCH_BIT)));
  __asm__ ("isb" : : :);
  __asm__ ("dsb sy" : : :);

  /* Invalidate stack to PoC, since stack is cached and data cache is
     about to be enabled. */
  inval_dcache_region(stack_base, stack_len);

  /* Enable EL3 data cache */
  enable_el3_dcache();

  el3_set_scr_ns();
  __asm__ ("dsb sy" : : :);

  /* have to do TLB invalidations with SCR[NS=1] */
  /* TODO: check to see if TLB invalidation is necessary.  It is not
     mentioned in the TRM. */
  /* TODO: EL2 TLB invalidation causing some weird crash. */
  /*  __asm__ ("tlbi alle2is" : : :); */
  __asm__ ("tlbi alle1is" : : :);
  __asm__ ("dsb sy" : : :);

  el3_set_scr_s();
  __asm__ ("dsb sy" : : :);
  __asm__ ("isb" : : :);

  __asm__ ("tlbi alle3is" : : :);
  __asm__ ("tlbi alle1is" : : :);
  __asm__ ("dsb sy" : : :);
  __asm__ ("isb" : : :);
}
