#ifndef A53_PC_H
#define A53_PC_H
/**
   @file a53_pc.c
   @brief Trustzone A53 specific collapse related routines

   This file contains the routines related to power collapse
   functionality specific to the ARM A53 processor.

*/
/*===========================================================================
   Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/armlib/v7/proc/a53/inc/a53_pc.h#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $


when       who      what, where, why
--------   ---     ------------------------------------------------------------
06/16/14   pre     Initial revision
=============================================================================*/

void a53_clear_cpuectlr_smpen(void);

#endif /* A53_PC_H */
