/**
   @file a57_pc.c
   @brief Trustzone A57 specific collapse related routines

   This file contains the routines related to power collapse
   functionality specific to the ARM A57 processor.

*/
/*===========================================================================
   Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/armlib/v7/proc/a57/src/a57_pc.c#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $


when       who      what, where, why
--------   ---     ------------------------------------------------------------
06/16/14   pre     Initial revision
=============================================================================*/

#include <comdef.h>
#include "a57_sys_regs.h"

#define CPUECTLR_EL1_SMP_BIT               (1ULL << 6)
#define CPUECTLR_EL1_DIS_TWDA_PREFETCH_BIT (1ULL << 38)
#define CPUECTLR_EL1_L2_IPF_DIS_SHFT       35
#define CPUECTLR_EL1_L2_IPF_DIS_BMSK       0x3ULL
#define CPUECTLR_EL1_L2_LSPF_DIS_SHFT      32
#define CPUECTLR_EL1_L2_LSPF_DIS_BMSK      0x3ULL

void a57_disable_l2_prefetch(void)
{
  uint32 cpuectlr_l, cpuectlr_h;
  uint64 cpuectlr;

  /* Clear CPUECTLR[38,36:35,33:32] */
  RCP15_CPUECTLR(cpuectlr_l, cpuectlr_h);
  cpuectlr = cpuectlr_l | ((uint64) cpuectlr_h << 32);
  cpuectlr &= ~((CPUECTLR_EL1_L2_IPF_DIS_BMSK <<
                 CPUECTLR_EL1_L2_IPF_DIS_SHFT) |
                (CPUECTLR_EL1_L2_LSPF_DIS_BMSK <<
                 CPUECTLR_EL1_L2_LSPF_DIS_SHFT) |
                CPUECTLR_EL1_DIS_TWDA_PREFETCH_BIT);
  cpuectlr_l = cpuectlr & 0xFFFFFFFF;
  cpuectlr_h = (cpuectlr >> 32) & 0xFFFFFFFF;
  WCP15_CPUECTLR(cpuectlr_l, cpuectlr_h);

  __asm__ ("isb" : : :);
  __asm__ ("dsb sy" : : :);
}

void a57_clear_cpuectlr_smpen(void)
{
  uint32 cpuectlr_l, cpuectlr_h;

  RCP15_CPUECTLR(cpuectlr_l, cpuectlr_h);
  cpuectlr_l &= ~CPUECTLR_EL1_SMP_BIT;
  WCP15_CPUECTLR(cpuectlr_l, cpuectlr_h);

  __asm__ ("isb" : : :);

  /* Erratum for CPU issue */
  MCR15(&cpuectlr_l, 0, c8, c7, 1);

  __asm__ ("dsb sy" : : :);
}
