#ifndef A57_TLB_H
#define A57_TLB_H

/*============================================================================

                           A57 TLB Dumping

GENERAL DESCRIPTION
 APIs for dumping A57 TLBs

Copyright 2014 by Qualcomm Technologies, Inc.  All Rights Reserved.
============================================================================*/


/*============================================================================

                            EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/armlib/v7/proc/a57/inc/a57_tlb.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
07/25/14   pre     Intial revision.
============================================================================*/

#include <comdef.h>

#define A57_NUM_L1_I_TLB_ENTRIES         48
#define A57_NUM_L1_D_TLB_ENTRIES         32
#define A57_NUM_L2_TLB_ENTRIES       (256*4) /* 4 ways, 256 entries per way */

typedef enum
{
  A57_L1_I_TLB,
  A57_L1_D_TLB,
  A57_L2_TLB
} a57_tlb_t;

typedef struct a57_tlb_data_s
{
  uint32 data[4];
} a57_tlb_data_t;

typedef struct a57_l1_i_tlb_s
{
  a57_tlb_data_t entry[A57_NUM_L1_I_TLB_ENTRIES];
} a57_l1_i_tlb_t;

typedef struct a57_l1_d_tlb_s
{
  a57_tlb_data_t entry[A57_NUM_L1_D_TLB_ENTRIES];
} a57_l1_d_tlb_t;

typedef struct a57_l2_tlb_s
{
  a57_tlb_data_t entry[A57_NUM_L2_TLB_ENTRIES];
} a57_l2_tlb_t;

void a57_dump_tlb(a57_tlb_t tlb, void* dump_ptr);

#endif /* A57_TLB_H */
