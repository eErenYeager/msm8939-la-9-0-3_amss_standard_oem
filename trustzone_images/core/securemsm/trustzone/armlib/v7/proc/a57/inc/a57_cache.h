#ifndef A57_CACHE_H
#define A57_CACHE_H

/*============================================================================

                           A57 CACHE Dumping

GENERAL DESCRIPTION
 APIs for dumping A57 caches

Copyright 2014 by Qualcomm Technologies, Inc.  All Rights Reserved.
============================================================================*/


/*============================================================================

                            EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/armlib/v7/proc/a57/inc/a57_cache.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
07/25/14   pre     Intial revision.
============================================================================*/

#include <comdef.h>

#define A57_L1_I_CACHE_NUM_WAYS         3
#define A57_L1_I_CACHE_NUM_SETS        (1 << 9)
/* Number of words per read */
#define A57_L1_I_CACHE_NUM_WPR          2
/* Reads per cache line */
#define A57_L1_I_CACHE_NUM_RPL         (1 << 3)
/* Number of words */
#define A57_L1_I_CACHE_NUM_W           (A57_L1_I_CACHE_NUM_WAYS * \
                                        A57_L1_I_CACHE_NUM_SETS * \
                                        A57_L1_I_CACHE_NUM_RPL * \
                                        A57_L1_I_CACHE_NUM_WPR)

#define A57_L1_D_CACHE_NUM_WAYS         2
#define A57_L1_D_CACHE_NUM_SETS        (1 << 9)
#define A57_L1_D_CACHE_NUM_WPR          2
#define A57_L1_D_CACHE_NUM_RPL         (1 << 3)
#define A57_L1_D_CACHE_NUM_W           (A57_L1_D_CACHE_NUM_WAYS * \
                                        A57_L1_D_CACHE_NUM_SETS * \
                                        A57_L1_D_CACHE_NUM_RPL * \
                                        A57_L1_D_CACHE_NUM_WPR)

#define A57_L2_CACHE_NUM_WAYS          (1 << 5)
#define A57_L2_CACHE_NUM_SETS          (1 << 14)
#define A57_L2_CACHE_NUM_WPR            4
#define A57_L2_CACHE_NUM_RPL           (1 << 2)
#define A57_L2_CACHE_NUM_W             (A57_L2_CACHE_NUM_WAYS * \
                                        A57_L2_CACHE_NUM_SETS * \
                                        A57_L2_CACHE_NUM_RPL * \
                                        A57_L2_CACHE_NUM_WPR)

typedef enum
{
  A57_L1_I_CACHE,
  A57_L1_D_CACHE,
  A57_L2_CACHE
} a57_cache_t;

typedef struct a57_l1_i_cache_line_s
{
  uint32 tag[A57_L1_I_CACHE_NUM_WPR];
  uint32 data[A57_L1_I_CACHE_NUM_WPR * A57_L1_I_CACHE_NUM_RPL];
} a57_l1_i_cache_line_t;

typedef struct a57_l1_d_cache_line_s
{
  uint32 tag[A57_L1_D_CACHE_NUM_WPR];
  uint32 data[A57_L1_D_CACHE_NUM_WPR * A57_L1_D_CACHE_NUM_RPL];
} a57_l1_d_cache_line_t;

typedef struct a57_l2_cache_line_s
{
  uint32 tag[A57_L2_CACHE_NUM_WPR];
  uint32 data[A57_L2_CACHE_NUM_WPR * A57_L2_CACHE_NUM_RPL];
} a57_l2_cache_line_t;

typedef struct a57_l1_i_cache_s
{
  a57_l1_i_cache_line_t line[A57_L1_I_CACHE_NUM_SETS * A57_L1_I_CACHE_NUM_WAYS];
} a57_l1_i_cache_t;

typedef struct a57_l1_d_cache_s {
  a57_l1_d_cache_line_t line[A57_L1_D_CACHE_NUM_SETS * A57_L1_D_CACHE_NUM_WAYS];
} a57_l1_d_cache_t;

typedef struct a57_l2_cache_s {
  a57_l2_cache_line_t line[A57_L2_CACHE_NUM_SETS * A57_L2_CACHE_NUM_WAYS];
} a57_l2_cache_t;

void a57_dump_cache(a57_cache_t cache, void* dump_ptr);

#endif /* A57_CACHE_H */
