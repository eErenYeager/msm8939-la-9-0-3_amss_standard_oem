#ifndef A57_SYS_REGS_H
#define A57_SYS_REGS_H

/*============================================================================

                      A57 System (CP15) Registers

GENERAL DESCRIPTION
 Define macros for reading and writing to the cp registers specific
 for the A57 processor.

Copyright 2014 by Qualcomm Technologies, Inc.  All Rights Reserved.
============================================================================*/


/*============================================================================

                            EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/armlib/v7/proc/a57/inc/a57_sys_regs.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
06/16/14   pre     Intial revision.
============================================================================*/

#include <comdef.h>
#include "tzbsp_mcrmrc.h"

#define WCP15_CPUECTLR(reg0, reg1) MCRR15(reg0, reg1, 1, c15)
#define RCP15_CPUECTLR(reg0, reg1) MRRC15(reg0, reg1, 1, c15)

#endif /* A57_SYS_REGS_H */
