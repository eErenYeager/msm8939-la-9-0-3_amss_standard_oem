#ifndef A57_SYS_FIELDS
#define A57_SYS_FIELDS

/**
   @file a57_sys_fields.h
   @brief Trustzone A57 specific system register fields
*/
/*===========================================================================
   Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/armlib/v7/proc/a57/inc/a57_sys_fields.h#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $


when       who      what, where, why
--------   ---     ------------------------------------------------------------
08/14/14   pre     Initial revision
=============================================================================*/

#define RAMINDEX_RAMID_BMSK                0xFF
#define RAMINDEX_RAMID_SHFT                24
#define RAMINDEX_WAY_BMSK                  0xF
#define RAMINDEX_WAY_SHFT                  18
#define RAMINDEX_II_BMSK                   0x3FFFF
#define RAMINDEX_II_SHFT                   0

#define RAMINDEX_RAMID_L1_I_TAG            0x00
#define RAMINDEX_RAMID_L1_I_DATA           0x01
#define RAMINDEX_RAMID_L1_I_BTB            0x02
#define RAMINDEX_RAMID_L1_I_GHB            0x03
#define RAMINDEX_RAMID_L1_I_TLB            0x04
#define RAMINDEX_RAMID_L1_I_IP             0x05

#define RAMINDEX_RAMID_L1_D_TAG            0x08
#define RAMINDEX_RAMID_L1_D_DATA           0x09
#define RAMINDEX_RAMID_L1_D_TLB            0x0A

#define RAMINDEX_RAMID_L2_TAG              0x10
#define RAMINDEX_RAMID_L2_DATA             0x11
#define RAMINDEX_RAMID_L2_SNOOP_TAG        0x12
#define RAMINDEX_RAMID_L2_DATA_ECC         0x13
#define RAMINDEX_RAMID_L2_DIRTY            0x14
#define RAMINDEX_RAMID_L2_TLB              0x18

#endif /* A57_SYS_FIELDS */
