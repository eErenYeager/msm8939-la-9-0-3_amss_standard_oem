/**
   @file a57_tlb.c
   @brief Trustzone A57 specific TLB dumping routines
*/
/*===========================================================================
   Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/armlib/v7/proc/a57/src/a57_tlb.c#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $


when       who      what, where, why
--------   ---     ------------------------------------------------------------
07/25/14   pre     Initial revision
=============================================================================*/

#include "a57_tlb.h"
#include "a57_sys_regs.h"
#include "a57_sys_fields.h"

/* AArch64: SYS #0, c15, c4, #0, x0 */
#define WCP15_RAMINDEX(xx) MCR15(xx, 0, c15, c4, 0);

/* AArch64: MRS x0, S3_0_c15_c0_ii */
#define RCP15_READRAMDATA_II(xx, ii) MRC15(xx, 0, c15, c0, ii);

/* l1_i, l1_d, l2 */
#define TLB_SET_FN(TLB_T) TLB_SET_FN_IMPL(TLB_T)
#define TLB_SET_FN_IMPL(TLB_T) \
  static void set_##TLB_T##_tlb_data(void* tlb_ptr,               \
                                     uint32 ii,                   \
                                     uint32 data0,                \
                                     uint32 data1,                \
                                     uint32 data2,                \
                                     uint32 data3)                \
  {                                                               \
    ((a57_##TLB_T##_tlb_t*) tlb_ptr)->entry[ii].data[0] = data0;  \
    ((a57_##TLB_T##_tlb_t*) tlb_ptr)->entry[ii].data[1] = data1;  \
    ((a57_##TLB_T##_tlb_t*) tlb_ptr)->entry[ii].data[2] = data2;  \
    ((a57_##TLB_T##_tlb_t*) tlb_ptr)->entry[ii].data[3] = data3;  \
  }

static uint32 get_l1_i_tlb_array_ramindex(uint32 ii)
{
  return ((RAMINDEX_RAMID_L1_I_TLB << RAMINDEX_RAMID_SHFT) | (ii & 0x3F));
}

static uint32 get_l1_d_tlb_array_ramindex(uint32 ii)
{
  return ((RAMINDEX_RAMID_L1_D_TLB << RAMINDEX_RAMID_SHFT) | (ii & 0x1F));
}

static uint32 get_l2_tlb_array_ramindex(uint32 ii)
{
  return ((RAMINDEX_RAMID_L2_TLB << RAMINDEX_RAMID_SHFT) |
          (((ii / 256) & 3) << RAMINDEX_WAY_SHFT) |
          ((ii % 256) & 0xFF));
}

TLB_SET_FN(l1_i)
TLB_SET_FN(l1_d)
TLB_SET_FN(l2)

void a57_dump_tlb(a57_tlb_t tlb, void* dump_ptr)
{
  uint32 ii, limit;
  uint32 (*ramindex_fn)(uint32);
  void (*set_data_fn)(void*, uint32, uint32, uint32, uint32, uint32);

  switch (tlb)
  {
  case A57_L1_I_TLB:
    limit = A57_NUM_L1_I_TLB_ENTRIES;
    ramindex_fn = get_l1_i_tlb_array_ramindex;
    set_data_fn = set_l1_i_tlb_data;
    break;
  case A57_L1_D_TLB:
    limit = A57_NUM_L1_D_TLB_ENTRIES;
    ramindex_fn = get_l1_d_tlb_array_ramindex;
    set_data_fn = set_l1_d_tlb_data;
    break;
  case A57_L2_TLB:
    limit = A57_NUM_L2_TLB_ENTRIES;
    ramindex_fn = get_l2_tlb_array_ramindex;
    set_data_fn = set_l2_tlb_data;
    break;
  default:
    /* TODO: print error code */
    return;
  }

  for (ii = 0; ii < limit; ii++)
  {
    uint32 ramindex = ramindex_fn(ii);
    uint32 data0, data1, data2, data3;
    WCP15_RAMINDEX(ramindex);

    __asm__ ("dsb sy \n isb" ::);
    RCP15_READRAMDATA_II(data0, 0);
    RCP15_READRAMDATA_II(data1, 1);
    RCP15_READRAMDATA_II(data2, 2);
    RCP15_READRAMDATA_II(data3, 3);
    set_data_fn(dump_ptr, ii, data0, data1, data2, data3);

    __asm__ ("dsb sy" ::);
  }
}
