#ifndef A57_PC_H
#define A57_PC_H
/**
   @file a57_pc.h
   @brief Trustzone A57 specific collapse related routines

   This file contains the routines related to power collapse
   functionality specific to the ARM A57 processor.

*/
/*===========================================================================
   Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/armlib/v7/proc/a57/inc/a57_pc.h#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $


when       who      what, where, why
--------   ---     ------------------------------------------------------------
06/16/14   pre     Initial revision
=============================================================================*/


void a57_disable_l2_prefetch(void);
void a57_clear_cpuectlr_smpen(void);

#endif /* A57_PC_H */
