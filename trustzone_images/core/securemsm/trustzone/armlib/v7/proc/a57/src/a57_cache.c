/**
   @file a57_cache.c
   @brief Trustzone A57 specific cache dumping routines
*/
/*===========================================================================
   Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/armlib/v7/proc/a57/src/a57_cache.c#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $


when       who      what, where, why
--------   ---     ------------------------------------------------------------
07/25/14   pre     Initial revision
=============================================================================*/

#include "a57_cache.h"
#include "a57_sys_regs.h"
#include "a57_sys_fields.h"

/* TODO: figure out how to tell toolchain that we want these types
   included in the debug info without having to define dummy
   variables. */
a57_l1_i_cache_t* a57_l1_i_cache_t_debug;
a57_l1_d_cache_t* a57_l1_d_cache_t_debug;
a57_l2_cache_t* a57_l2_cache_t_debug;

/* AArch64: SYS #0, c15, c4, #0, x0 */
#define WCP15_RAMINDEX(xx) MCR15(xx, 0, c15, c4, 0);

/* AArch64: MRS x0, S3_0_c15_c0_ii */
#define RCP15_READRAMDATA_II(xx, ii) MRC15(xx, 0, c15, c0, ii);

/* Inputs: [l1_i, l1_d], [tag, data] */
#define CACHE_SET_FN(CT, TOD) CACHE_SET_FN_IMPL(CT, TOD)
#define CACHE_SET_FN_IMPL(CT, TOD)                                      \
  static void set_##CT##_cache_##TOD(void* cache_ptr,                   \
                                     uint32 ii,                         \
                                     uint32 jj,                         \
                                     uint32 data0,                      \
                                     uint32 data1,                      \
                                     uint32 data2,                      \
                                     uint32 data3)                      \
  {                                                                     \
    ((a57_##CT##_cache_t*) cache_ptr)->line[ii]./**/TOD/**/[jj + 0] = data0; \
    ((a57_##CT##_cache_t*) cache_ptr)->line[ii]./**/TOD/**/[jj + 1] = data1; \
  }

/* Inputs: [tag, data] */
#define L2_CACHE_SET_FN(TOD) L2_CACHE_SET_FN_IMPL(TOD)
#define L2_CACHE_SET_FN_IMPL(TOD)                                       \
  static void set_l2_cache_##TOD(void* cache_ptr,                       \
                                   uint32 ii,                           \
                                 uint32 jj,                             \
                                 uint32 data0,                          \
                                 uint32 data1,                          \
                                 uint32 data2,                          \
                                 uint32 data3)                          \
  {                                                                     \
   ((a57_l2_cache_t*) cache_ptr)->line[ii]./**/TOD/**/[jj + 0] = data0; \
   ((a57_l2_cache_t*) cache_ptr)->line[ii]./**/TOD/**/[jj + 1] = data1; \
   ((a57_l2_cache_t*) cache_ptr)->line[ii]./**/TOD/**/[jj + 2] = data2; \
   ((a57_l2_cache_t*) cache_ptr)->line[ii]./**/TOD/**/[jj + 3] = data3; \
  }


/* Inputs: [l1_i, l1_d, l2], [L1_I, L1_D, L2] */
#define CFG_PARAMS(CT, ct)                                      \
  limit = A57_##CT##_CACHE_NUM_W / A57_##CT##_CACHE_NUM_WPR;    \
  tag_ramindex_fn = get_##ct##_cache_tag_ramindex;              \
  data_ramindex_fn = get_##ct##_cache_data_ramindex;            \
  set_data_fn = set_##ct##_cache_data;                          \
  set_tag_fn = set_##ct##_cache_tag;                            \
  tag_ignore_ii = A57_##CT##_CACHE_NUM_RPL

CACHE_SET_FN(l1_i, tag)
CACHE_SET_FN(l1_d, tag)
L2_CACHE_SET_FN(tag);

CACHE_SET_FN(l1_i, data)
CACHE_SET_FN(l1_d, data)
L2_CACHE_SET_FN(data);


/* TODO: use shifts and masks from sys_fields.h */
static uint32 get_l1_i_cache_data_ramindex(uint32 ii)
{
  return ((RAMINDEX_RAMID_L1_I_DATA << RAMINDEX_RAMID_SHFT) |
          (((ii >> 11) & 0x3) << RAMINDEX_WAY_SHFT) |
          ((ii & 0x7FF) << 3));
}

/* TODO: use shifts and masks from sys_fields.h */
static uint32 get_l1_i_cache_tag_ramindex(uint32 ii)
{
  return ((RAMINDEX_RAMID_L1_I_TAG << RAMINDEX_RAMID_SHFT) |
          (((ii >> 8) & 0x3) << RAMINDEX_WAY_SHFT) |
          ((ii & 0xFF) << 6));
}

/* TODO: use shifts and masks from sys_fields.h */
static uint32 get_l1_d_cache_data_ramindex(uint32 ii)
{
  return ((RAMINDEX_RAMID_L1_D_DATA << RAMINDEX_RAMID_SHFT) |
          (((ii >> 11) & 0x1) << RAMINDEX_WAY_SHFT) |
          ((ii & 0x7FF) << 3));
}

/* TODO: use shifts and masks from sys_fields.h */
static uint32 get_l1_d_cache_tag_ramindex(uint32 ii)
{
  return ((RAMINDEX_RAMID_L1_D_TAG << RAMINDEX_RAMID_SHFT) |
          (((ii >> 8) & 0x1) << RAMINDEX_WAY_SHFT) |
          ((ii & 0xFF) << 6));
}

/* TODO: use shifts and masks from sys_fields.h */
static uint32 get_l2_cache_data_ramindex(uint32 ii)
{
  return ((RAMINDEX_RAMID_L2_DATA << RAMINDEX_RAMID_SHFT) |
          (((ii >> 13) & 0xF) << RAMINDEX_WAY_SHFT) |
          ((ii & 0x1FFF) << 4));
}

/* TODO: use shifts and masks from sys_fields.h */
static uint32 get_l2_cache_tag_ramindex(uint32 ii)
{
  return ((RAMINDEX_RAMID_L2_TAG << RAMINDEX_RAMID_SHFT) |
          (((ii >> 11) & 0xF) << RAMINDEX_WAY_SHFT) |
          ((ii & 0x7FF) << 6));
}

void a57_dump_cache(a57_cache_t cache, void* dump_ptr)
{
  uint32 ii, limit, tag_ignore_ii;
  uint32 (*data_ramindex_fn)(uint32);
  uint32 (*tag_ramindex_fn)(uint32);
  void (*set_data_fn)(void*, uint32, uint32, uint32, uint32, uint32, uint32);
  void (*set_tag_fn)(void*, uint32, uint32, uint32, uint32, uint32, uint32);

  switch (cache)
  {
  case A57_L1_I_CACHE:
    CFG_PARAMS(L1_I, l1_i);
    break;
  case A57_L1_D_CACHE:
    CFG_PARAMS(L1_D, l1_d);
    break;
  case A57_L2_CACHE:
    CFG_PARAMS(L2, l2);
    break;
  default:
    /* TODO: print error code */
    return;
  }

  for (ii = 0; ii < limit; ii++)
  {
    uint32 data0, data1, data2, data3;

    if (0 == (ii % tag_ignore_ii))
    {
      /* tag */
      WCP15_RAMINDEX(tag_ramindex_fn(ii));
      __asm__ __volatile__ ("dsb sy \n isb"::);
      RCP15_READRAMDATA_II(data0, 0);
      RCP15_READRAMDATA_II(data1, 1);
      RCP15_READRAMDATA_II(data2, 2);
      RCP15_READRAMDATA_II(data3, 3);
      __asm__ __volatile__ ("dsb sy"::);
      set_tag_fn(dump_ptr, ii / tag_ignore_ii, 0, data0, data1, data2, data3);
    }

    /* data */
    WCP15_RAMINDEX(data_ramindex_fn(ii));
    __asm__ __volatile__ ("dsb sy \n isb"::);
    RCP15_READRAMDATA_II(data0, 0);
    RCP15_READRAMDATA_II(data1, 1);
    RCP15_READRAMDATA_II(data2, 2);
    RCP15_READRAMDATA_II(data3, 3);
    __asm__ __volatile__ ("dsb sy"::);
    set_data_fn(dump_ptr,
                ii / tag_ignore_ii,
                ii % tag_ignore_ii,
                data0, data1, data2, data3);
  }
}
