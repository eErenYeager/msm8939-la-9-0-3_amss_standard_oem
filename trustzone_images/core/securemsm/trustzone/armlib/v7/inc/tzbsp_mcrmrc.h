/***************************************************************************

 MODULE: mrcmcr

 DESCRIPTION: Convience macros for access the cp registers in the arm.

 AUTHOR: KAS

 REV/DATE: Fri Mar 18 16:34:44 EST 2005

 Copyright (c) 2005 by Qualcomm Technologies Incorporated.  All Rights Reserved.
*****************************************************************************/

#ifndef __mrcmcr__h_
#define __mrcmcr__h_

/*
 * Define some convience macros to acccess the cp registers from c code
 *
 * Takes the same format as the asm instructions and unfortunatly you cannot
 * use variables to select the crn, crn or op fields...
 *
 * When compiled these compile to single asm instructions (fast)
 * without all the hassel of __asm__ __volatile__ (...)
 *
 * Format is:
 *
 *    register unsigned long reg;   // destination variable
 *    MRC(reg, p15, 0, c1, c0, 0 );
 *
 *   MRC read control register
 *   MCR control register write
 */

/*
 * Some assembly macros so we can use the same macros as in the C version.
 * Turns the ASM code a little C-ish but keeps the code consistant and in
 * one location...
 */
#ifdef _ARM_ASM_

#define MRC(reg, processor, op1, crn, crm, op2) \
      mrc      processor, op1, reg,  crn, crm, op2

#define MCR(reg, processor, op1, crn, crm, op2) \
      mcr      processor, op1, reg,  crn, crm, op2

#define MRRC(reg0, reg1, processor, op1, crn)    \
      mrrc     processor, op1, reg0,  reg1, crn

#define MCRR(reg0, reg1, processor, op1, crn)   \
      mcrr     processor, op1, reg0,  reg1, crn


/*
 * C version of the macros.
 */
#else

#define W2DW(lsw, msw) ((uint64)(((uint64) msw << 32) + (uint32) lsw))
#define DW2LSW(dw) ((uint32) (dw & 0xFFFFFFFF))
#define DW2MSW(dw) ((uint32) (dw >> 32))


#define MRC(reg, processor, op1, crn, crm, op2) \
   __asm__ __volatile__ ( \
      "mrc " #processor ", " #op1 ", %0 ," #crn "," #crm "," #op2 " \n" \
      :"=r"(reg) \
   )

#define MRRC(reg0, reg1, processor, op1, crn)    \
   __asm__ __volatile__ ( \
     "mrrc " #processor ", " #op1 ", %0, %1, " #crn " \n" \
     :"=r"(reg0), "=r"(reg1)                                            \
   )


#define MCR(reg, processor, op1, crn, crm, op2) \
   __asm__ __volatile__ ( \
      "mcr " #processor "," #op1 ", %0 ," #crn "," #crm "," #op2 " \n" \
      ::"r"(reg) \
   )

#define MCRR(reg0, reg1, processor, op1, crn)    \
   __asm__ __volatile__ ( \
     "mcrr " #processor ", " #op1 ", %0, %1, " #crn " \n" \
     ::"r"(reg0), "r"(reg1)                             \
   )


#endif

/*
 * Easy access convience function to read CP15 registers from c code
 */
#define MRC15(reg, op1, crn, crm, op2) MRC(reg, p15, op1, crn, crm, op2)
#define MCR15(reg, op1, crn, crm, op2) MCR(reg, p15, op1, crn, crm, op2)
#define MRRC15(reg0, reg1, op1, crn) MRRC(reg0, reg1, p15, op1, crn)
#define MCRR15(reg0, reg1, op1, crn) MCRR(reg0, reg1, p15, op1, crn)


#endif
