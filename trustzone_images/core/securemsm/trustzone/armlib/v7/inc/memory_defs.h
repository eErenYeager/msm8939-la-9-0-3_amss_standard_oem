#ifndef MEMORY_DEFS_H
#define MEMORY_DEFS_H

#include <comdef.h> /* uint* */
#include <string.h> /* size_t */

#define TZ_512KB 0x80000
#define TZ_1GB 0x40000000
#define TZ_2MB 0x00200000
#define TZ_1MB 0x00100000
#define TZ_4KB 0x00001000
#define TZ_1KB 0x00000400

#define KB2MB(xx)    ((xx) >> 10)
#define B2KB(xx)     ((xx) >> 10)
#define KB2B(xx)     ((xx) << 10)
#define MB2KB(xx)    ((xx) << 10)

typedef uint32 v_addr_t;
typedef uint32 p_addr_t;
typedef uint32 tz_cfg_t;
typedef uint64 el3_reg_t;

typedef struct
{
  v_addr_t v_addr;
  p_addr_t p_addr;
  uint32   sz_in_kb; /* Since size_t changes between 64 and 32, can't
                        use that type here.  */
  tz_cfg_t tz_cfg;
} mem_block_t;

#endif /* MEMORY_DEFS_H */
