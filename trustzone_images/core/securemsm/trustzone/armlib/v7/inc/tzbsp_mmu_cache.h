#ifndef TZBSP_MMU_CACHE_H
#define TZBSP_MMU_CACHE_H

/*=============================================================================

                    Boot Cache MMU Header File

GENERAL DESCRIPTION
  This header file contains the public interface for the cache
  and mmu control functions.  Originally from here:
  //source/qcom/qct/core/pkg/modem/mp/arm11/rel/1.0/modem_proc/core/boot/secboot3/common/cache_mmu.h#3

Copyright 2011 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
=============================================================================*/


/*===========================================================================
                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/armlib/v7/inc/tzbsp_mmu_cache.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
01/25/11   pre     Initial Version
===========================================================================*/


/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

#include <comdef.h>
#include "memory_defs.h"

/*===========================================================================

                       FUNCTION PROTOTYPES

===========================================================================*/

/*===========================================================================
 *  Function :  tzbsp_mmu_cache_init
 * ==========================================================================
 */
/**
 * Initialize and enable the MMU, and enable the I-cache and D-cache.
 */
int tzbsp_mmu_cache_init(void) __attribute__((weak)) ;

/*===========================================================================
 *  Function :  tzbsp_mmu_cache_disable
 * ==========================================================================
 */
/**
 * Disable the MMU and caches.
 */
void tzbsp_mmu_cache_disable(void);

/*===========================================================================
 *  Function :  tzbsp_mmu_cache_re_enable
 * ==========================================================================
 */
/**
 * Re-enable the MMU and caches.
 */
void tzbsp_mmu_cache_re_enable(void) __attribute__((weak)) ;

/*===========================================================================
 *  Function :  tzbsp_tlb_inval_all
 * ==========================================================================
 */
/**
 * Invalidate the entire unified TLB.
 */
void tzbsp_tlb_inval_all(void) __attribute__((weak)) ;

/*===========================================================================
 *  Function :  tzbsp_itlb_inval_all
 * ==========================================================================
 */
/**
 * Invalidate the entire instruction TLB.
 */
void tzbsp_itlb_inval_all(void) __attribute__((weak)) ;

/*===========================================================================
 *  Function :  tzbsp_tlb_inval_all_is
 * ==========================================================================
 */
/**
 * Invalidate the unified TLB inner shared
 */
void tzbsp_tlb_inval_all_is(void) __attribute__((weak)) ;

/*===========================================================================
 *  Function :  tzbsp_tlb_inval_by_asid_is
 * ==========================================================================
 */
/**
 * Invalidate the unified TLB by ASID inner shared
 */
void tzbsp_tlb_inval_by_asid_is(uint32 asid) __attribute__((weak)) ;

/*===========================================================================
 *  Function :  tzbsp_tlb_inval_by_mva_is
 * ==========================================================================
 */
/**
 * Invalidate the unified TLB by MVA inner shared
 */
void tzbsp_tlb_inval_by_mva_is(uint32 mva) __attribute__((weak)) ;

/*===========================================================================
 *  Function :  tzbsp_dtlb_inval_all
 * ==========================================================================
 */
/**
 * Invalidate the entire data TLB.
 */
void tzbsp_dtlb_inval_all(void) __attribute__((weak)) ;

/*===========================================================================
 *  Function :  tzbsp_cache_flush
 * ==========================================================================
 */
/**
 * Flushes the entire cache system.  The data cache flush is achieved
 * using atomic clean / invalidates working outwards from L1
 * cache. This is done using Set/Way based cache maintainance
 * instructions.  The instruction cache can still be invalidated back
 * to the point of unification in a single instruction.  This also
 * flushes the branch target cache.
 */
void tzbsp_cache_flush(void) __attribute__((weak)) ;

/*===========================================================================
 *  Function :  tzbsp_dcache_clean_all
 * ==========================================================================
 */
/**
 * Clean the data cache system.  The clean is achieved using clean
 * operations working outwards from L1 cache. This is done using
 * Set/Way based cache maintainance instructions.
 */
void tzbsp_dcache_clean_all(void);

/*===========================================================================
 *  Function :  tzbsp_dcache_inval_all
 * ==========================================================================
 */
/**
 * Inval the data cache system.  The invalidate is achieved using
 * invalidate operations working outwards from L1 cache. This is done
 * using Set/Way based cache maintainance instructions.
 */
void tzbsp_dcache_inval_all(void);

/*===========================================================================
 *  Function :  tzbsp_dcache_flush_all
 * ==========================================================================
 */
/**
 * Flush the data cache system.  The flush is achieved using atomic
 * clean / invalidates working outwards from L1 cache. This is done
 * using Set/Way based cache maintainance instructions.
 */
void tzbsp_dcache_flush_all(void);

/*===========================================================================
 *  Function :  tzbsp_dcache_clean_region
 * ==========================================================================
 */
/**
 * Cleans a memory region in the cache. Please note that this will
 * write back any data that is dirty but it will not invalidate the
 * cache region.  So any further accesses to data in this region would
 * result in a cache-hit.
 *
 * @param addr Start address of memory region
 * @param length Length of memory region
 */
void tzbsp_dcache_clean_region(void *addr, unsigned int length);

/*===========================================================================
 *  Function :  tzbsp_dcache_inval_region
 * ==========================================================================
 */
/**
 * Invalidate a memory region in the cache. Please note that the data
 * in the cache will not be written back to the main memory. However,
 * any further accesses to data in this region would result in a
 * cache-miss.
 *
 * @param addr Start address of memory region
 * @param length Length of memory region
 */
void tzbsp_dcache_inval_region(void *addr, unsigned int length);

/*===========================================================================
 *  Function :  tzbsp_dcache_flush_region
 * ==========================================================================
 */
/**
 * Cleans and invalidates a memory region in the cache. Please note
 * that the data in the cache would be written back to main memory if
 * dirty and the region invalidated. So any further access to the data
 * would result in a cache-miss.
 *
 * @param addr Start address of memory region
 * @param length Length of memory region
 */
void tzbsp_dcache_flush_region(void *addr, unsigned int length);

/*===========================================================================
 *  Function :  tzbsp_icache_inv
 * ==========================================================================
 */
/**
 * Instuction cache invalidate, inner shared
 */
void tzbsp_icache_inv(void) __attribute__((weak)) ;

/*===========================================================================
 *  Function :  tzbsp_mmu_disable
 * ==========================================================================
 */
/**
 * Disable the ARM MMU. This function does not perform any cache maintenance
 */
void tzbsp_mmu_disable(void);

/*===========================================================================
 *  Function :  tzbsp_mmu_enable
 * ==========================================================================
 */
/**
 * Enable the ARM MMU. This function does not perform any cache maintenance
 */
void tzbsp_mmu_enable(void);

/*===========================================================================
 *  Function :  tzbsp_mmu_is_enabled
 * ==========================================================================
 */
/**
 * Return the current state of the MMU. TRUE if the MMU is currently enabled
 */
uint32 tzbsp_mmu_is_enabled(void);

/*===========================================================================
 *  Function :  tzbsp_vtop
 * ==========================================================================
 */
/**
 * Converts virtual memory pointer to physical address
 *
 * @param v_addr virtual address
 * @param p_addr physical address associated with the virtual address
 *
 * @returns  E_SUCCESS on success
 *          -E_FAILURE on failure
 */
uint32 tzbsp_vtop(uint32 v_addr, uint32 *p_addr);

/*===========================================================================
 *  Function :  tzbsp_mmu_disable_and_configure_ns
 * ==========================================================================
 */
/**
 * Invalidates L1 and TLB before disabling the MMU.  Configures the NS
 * translation table registers, and enables the caches and MMU for NS-world.
 */
void tzbsp_mmu_disable_and_configure_ns(void);


/*===========================================================================
 *  Function :  cold_init_and_enable_el1_mmu
 * ==========================================================================
 */
/**
 * Called during cold init to program MMU related system registers and
 * enable the MMU
 *
 * @param ttbr0 Translation table bas registeer
 * @param nmrr  Memory attribute register
 * @param prrr  Memory attribute register
 */
void cold_init_and_enable_el1_mmu(uint32 ttbr0, uint32 nmrr, uint32 prrr);


/*===========================================================================
 *  Function :  warm_init_and_enable_el1_mmu
 * ==========================================================================
 */
/**
 * Called during cpu initialization to program MMU related system
 * registers and enable the MMU
 *
 * @param ttbr0 Translation table bas registeer
 * @param nmrr  Memory attribute register
 * @param prrr  Memory attribute register
 */
void warm_init_and_enable_el1_mmu(uint32 ttbr0, uint32 nmrr, uint32 prrr);

#endif /* TZBSP_MMU_CACHE_H */
