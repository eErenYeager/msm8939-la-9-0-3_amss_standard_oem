#ifndef MAPPER_H
#define MAPPER_H

/*=============================================================================

                      MMU Mapping Functions

GENERAL DESCRIPTION
  Contains APIs for changing the memory mappings

Copyright 2013 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
=============================================================================*/


/*===========================================================================
                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/armlib/v7/inc/mapper.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
01/21/13   pre     Initial Version
===========================================================================*/


/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

#include <comdef.h>
#include "memory_defs.h"

/*===========================================================================

                       FUNCTION PROTOTYPES

===========================================================================*/

/**
   Maps a given memory block in the TZ secure translation table(s)

   @param map Structure that contains information relevant for mapping
              memory in the MMU

   @returns  E_SUCCESS on success
            -E_FAILURE on failed
            -E_NO_MEMORY when memory couldn't be alloced for an L2 page

   @pre interrupt locks may be required depending on memory regions
        being mapped

   @post TLB maintenance may be required after calling this function
 */
int tzbsp_mmu_map_block(const mem_block_t* map, unsigned int map_len);

/**
   Maps a given memory block in the TZ secure translation table(s)

   @param p_addr   physical address start
   @param v_addr   virtual address start
   @param sz_in_kb size of region to map in KB
   @param msk      memory attributes

   @returns  E_SUCCESS on success
            -E_FAILURE on failed
            -E_NO_MEMORY when memory couldn't be alloced for an L2 page

   @pre interrupt locks may be required depending on memory regions
        being mapped

   @post TLB maintenance may be required after calling this function
 */
int tzbsp_mmu_map(v_addr_t v_addr, p_addr_t p_addr,
                  size_t sz_in_kb, uint32 msk);


#endif /* MAPPER_H */
