#ifndef UTIL_H
#define UTIL_H
/**
@file util.h
@brief Header file for ARM library generic architectural routines
*/
/*===========================================================================
   Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/armlib/v7/inc/util.h#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $

when       who      what, where, why
--------   ---      ------------------------------------
06/08/14   pre      Initial version.
===========================================================================*/

#include "comdef.h"

void save_context(uint8* buf);

#endif /* UTIL_H */
