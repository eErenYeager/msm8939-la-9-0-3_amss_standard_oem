;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;
; GENERAL DESCRIPTION
; System register manipulation for v7
;
; Copyright (c) 2010-2014 by Qualcomm Technologies, Incorporated.All Rights Reserved.
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;
; EDIT HISTORY FOR FILE
;
; This section contains comments describing changes made to the module.
; Notice that changes are listed in reverse chronological order.
;
; $Header:
;
; when who what, where, why
; -------- --- ---------------------------------------------------
; 04/03/14 pre Initial version.
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
    EXPORT tzbsp_enable_branch_prediction
    EXPORT tzbsp_bp_invalidate_all
    EXPORT tzbsp_dmb
    EXPORT memory_barrier ; Alias for dsb, needed by DMOV.
    EXPORT tzbsp_dsb
    EXPORT tzbsp_isb
    EXPORT wcp15_csselr
    EXPORT rcp15_ccsidr
    CODE32
    PRESERVE8
    AREA TZBSP_SYS_REGS, align=4, CODE, READONLY
; void tzbsp_dmb(void)
tzbsp_dmb FUNCTION
    dmb
    bx lr
    ENDFUNC
memory_barrier ; Alias for tzbsp_dsb
; void tzbsp_dsb(void)
tzbsp_dsb FUNCTION
    dsb
    bx lr
    ENDFUNC
; void tzbsp_isb(void)
tzbsp_isb FUNCTION
    isb
    bx lr
    ENDFUNC
; void tzbsp_bp_invalidate_all(void)
tzbsp_bp_invalidate_all FUNCTION
    mcr p15, 0, r0, c7, c5, 6
    dsb
    isb
    bx lr
    ENDFUNC
; void wcp15_csselr(uint32 val)
wcp15_csselr FUNCTION
    mcr p15, 2, r0, c0, c0, 0 ; Write CSSELR
    bx lr
    ENDFUNC
; uint32 rcp15_ccsidr(void)
rcp15_ccsidr FUNCTION
    mrc p15, 1, r0, c0, c0, 0 ; Read CCSIDR
    bx lr
    ENDFUNC
; void tzbsp_enable_branch_prediction
tzbsp_enable_branch_prediction
   ;SCTLR
   mrc p15, 0, r0, c1, c0, 0 ; Read from CP15 Control Register
   orr r0, r0, # (1 << 11 ) ; Enable branch prediction
   ;SCTLR
   mcr p15, 0, r0, c1, c0, 0 ; Write back to CP15 Control Register
   mov r0, #0 ; Return success
   bx lr
    END
