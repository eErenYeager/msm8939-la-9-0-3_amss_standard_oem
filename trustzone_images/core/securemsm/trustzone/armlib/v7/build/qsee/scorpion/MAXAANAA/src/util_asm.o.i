;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;
; v8 Architecture Utilities
;
;
; GENERAL DESCRIPTION
; Routines that are generic for the v7 architecture
;
; Copyright (c) 2014 by Qualcomm Technologies, Incorporated.All Rights Reserved.
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;
; EDIT HISTORY FOR FILE
;
; This section contains comments describing changes made to the module.
; Notice that changes are listed in reverse chronological order.
;
; $Header:
;
; when who what, where, why
; -------- --- ---------------------------------------------------
; 06/08/14 pre Initial version.
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
    EXPORT tzbsp_int_enable
    EXPORT tzbsp_int_disable
    EXPORT tzbsp_int_read
    EXPORT tzbsp_int_disable_all
    EXPORT tzbsp_int_restore
    CODE32
    PRESERVE8
    ; Just lump these to the libarm area
    AREA TZBSP_UTIL_ASM, align=4, CODE, READONLY
; void tzbsp_int_enable(uint32 flags)
tzbsp_int_enable FUNCTION
    and r0, r0, #(1<<6):OR:(1<<7):OR:(1<<8) ; Only care about A/I/F bits.
    mrs r1, cpsr ; Read the status register.
    bic r1, r1, r0 ; Clear requested A/I/F bits
    msr cpsr_cx, r1 ; Write control & extension field
    bx lr
    ENDFUNC
; void tzbsp_int_disable(uint32 flags)
tzbsp_int_disable FUNCTION
    and r0, r0, #(1<<6):OR:(1<<7):OR:(1<<8) ; Only care about A/I/F bits.
    mrs r1, cpsr ; Read the status register.
    orr r1, r1, r0 ; Set requested A/I/F bits
    msr cpsr_cx, r1 ; Write control & extension field
    bx lr
    ENDFUNC
; int tzbsp_int_read(void)
tzbsp_int_read FUNCTION
  mrs r0, cpsr
  and r0, r0, #(1<<6):OR:(1<<7):OR:(1<<8)
  bx lr
  ENDFUNC
; uint32 tzbsp_int_disable_all(void)
tzbsp_int_disable_all FUNCTION
    mrs r1, cpsr ; Read the status register.
    and r0, r1, #(1<<6):OR:(1<<7):OR:(1<<8) ; Only care about A/I/F bits.
    orr r1, r1, #(1<<6):OR:(1<<7):OR:(1<<8) ; Set A/I/F bits.
    msr cpsr_cx, r1 ; Apply the new int mask.
    ; R0 contains the A/I/F bits before masking
    bx lr
    ENDFUNC
; void tzbsp_int_restore(uint32 flags)
tzbsp_int_restore FUNCTION
    and r0, r0, #(1<<6):OR:(1<<7):OR:(1<<8) ; Only care about A/I/F bits.
    mrs r1, cpsr ; Read the status register.
    bic r1, r1, #(1<<6):OR:(1<<7):OR:(1<<8) ; Clear A/I/F bits.
    orr r1, r1, r0 ; Set requested A/I/F bits.
    msr cpsr_cx, r1 ; Write control & extension field
    bx lr
    ENDFUNC
    END
