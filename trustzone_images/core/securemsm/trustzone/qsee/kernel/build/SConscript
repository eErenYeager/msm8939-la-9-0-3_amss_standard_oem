#===============================================================================
#
#                             Edit History
# $Header: //source/qcom/qct/core/kernel/lk/rel/1h10/build/SConscript#3 $
#
# when         who     what, where, why
# ----------   ---     ---------------------------------------------------------
# 2011/07/12   spa     Create initial version
#
#===============================================================================
#                    Copyright (c) 2011 QUALCOMM Incorporated.
#                           All Rights Reserved.
#                         QUALCOMM Proprietary/GTDR
#===============================================================================
# QSEE Lib
#-------------------------------------------------------------------------------
Import('env')
vars = Variables()


#----------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------
SRCPATH = "${BUILD_ROOT}/core/securemsm/trustzone/qsee/kernel"

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

#-------------------------------------------------------------------------------
# Internal depends within CoreBSP
#-------------------------------------------------------------------------------
env.RequirePublicApi([
   'SERVICES',                    #< comdef.h
   'DAL',                         #< HALcomdef.h
   'SYSTEMDRIVERS',               #< HALhwio.h
])
  
env.RequireRestrictedApi([
   'KERNEL',                 #< HALxpu2.h
   'SYSTEMDRIVERS',          #< HALqgic.h
   'SECUREMSM',              #< secboot_hw.h
   'TZCHIPSET',              #< tzbsp_target.h
   'TZLIBARMV7',             #< mmu stuff
   'MON_CALL',               #< for interrupt handling
   'SECAPP',                 #< 
   'MINK_LK',
   'TZMRM', 
])

QSEEKERNEL_INC = [
      '${INC_ROOT}/core/securemsm/trustzone/qsee/kernel/inc',
      '${INC_ROOT}/core/securemsm/trustzone/qsee/services/elfloader/inc',
      '${INC_ROOT}/core/securemsm/trustzone/qsee/services/external',
]

env.PublishPrivateApi('QSEEKERNEL', QSEEKERNEL_INC)

#env.Append(CCFLAGS = " --diag_suppress=1786,2523 ")
#env.Append(ASFLAGS = " --diag_suppress=1786,2523 ")

#-------------------------------------------------------------------------------
# Compiler options
#-------------------------------------------------------------------------------
#env.Append(CCFLAGS = "--gnu")

#Comment out the following line to disable debug module
env.Append(CPPDEFINES=['TZBSP_DBG_DEMAND_PAGING'])

#-------------------------------------------------------------------------------
# Sources, libraries
#-------------------------------------------------------------------------------

QSEE_CORE_SOURCES_LIB = [
   '${BUILDPATH}/src/tzos_boot_shim.c',
   '${BUILDPATH}/src/tzos_int_shim.c',
   '${BUILDPATH}/src/tzos_timer_shim.c',
   '${BUILDPATH}/src/tzos_cache_shim.c',
   '${BUILDPATH}/src/tzos_core_shim.c',
   '${BUILDPATH}/src/tzos_crypto_shim.c',
   '${BUILDPATH}/src/tzos_securechannel_shim.c',
   '${BUILDPATH}/src/interrupts.c',
   '${BUILDPATH}/src/pager.c',
   '${BUILDPATH}/src/poly665.s',
#   '${BUILDPATH}/src/mink_bsp_module_def.c',
]

QSEE_CORE_SOURCES = [
   '${BUILDPATH}/src/tzbsp_dload_mode.c',
   '${BUILDPATH}/src/tzbsp_isr_array.c',
   '${BUILDPATH}/src/tzbsp_mem.c',
   '${BUILDPATH}/src/tzbsp_asm.s',
]

#-------------------------------------------------------------------------------
# Add Libraries to image
#-------------------------------------------------------------------------------
env.AddBinaryLibrary(['TZOS_IMAGE'], '${BUILDPATH}/QSEECORE', QSEE_CORE_SOURCES_LIB)
env.AddObject(['TZOS_IMAGE'], QSEE_CORE_SOURCES)

env.LoadSoftwareUnits()
