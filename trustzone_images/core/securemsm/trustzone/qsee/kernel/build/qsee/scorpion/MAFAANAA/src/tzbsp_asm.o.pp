# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/kernel/src/tzbsp_asm.s"
# 1 "<built-in>" 1
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/kernel/src/tzbsp_asm.s" 2

;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;
; T R U S T Z O N E A S M
;
; GENERAL DESCRIPTION
; Assembly functions
;
; Copyright (c) 2011 by Qualcomm Technologies, Incorporated.All Rights Reserved.
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;
; EDIT HISTORY FOR FILE
;
; This section contains comments describing changes made to the module.
; Notice that changes are listed in reverse chronological order.
;
; $Header:
;
; when who what, where, why
; -------- --- --------------------------------------------------------
; 03/28/11 tk Initial version
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*


;============================================================================
;
; MODULE INCLUDES
;
;============================================================================

# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8929/cfg/mmu/tzbsp_target.h" 1
# 43 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8929/cfg/mmu/tzbsp_target.h"
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/customer.h" 1
# 80 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/customer.h"
# 1 "./custmafaanaaa.h" 1
# 10 "./custmafaanaaa.h"
# 1 "./targmafaanaaa.h" 1
# 11 "./custmafaanaaa.h" 2
# 144 "./custmafaanaaa.h"
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/custremoteapis.h" 1
# 145 "./custmafaanaaa.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/custtarget.h" 1
# 146 "./custmafaanaaa.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/custsdcc.h" 1
# 147 "./custmafaanaaa.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/custsurf.h" 1
# 148 "./custmafaanaaa.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/custdiag.h" 1
# 149 "./custmafaanaaa.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/custefs.h" 1
# 150 "./custmafaanaaa.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/custpmic.h" 1
# 151 "./custmafaanaaa.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/custsio_8660.h" 1
# 152 "./custmafaanaaa.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/custsec.h" 1
# 121 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/custsec.h"
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/custsfs.h" 1
# 122 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/custsec.h" 2
# 153 "./custmafaanaaa.h" 2
# 81 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/customer.h" 2
# 44 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8929/cfg/mmu/tzbsp_target.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8929/cfg/mmu/../common/tzbsp_target.h" 1
# 45 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8929/cfg/mmu/tzbsp_target.h" 2
# 33 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/kernel/src/tzbsp_asm.s" 2

    GET tzbsp_libarm_asm.inc
    GET tzbsp_asm.inc

;============================================================================
;
; MODULE DEFINES
;
;============================================================================

;============================================================================
;
; MODULE IMPORTS
;
;============================================================================

    ; Import the external symbols that are referenced in this module.
    IMPORT tzbsp_syscall
    IMPORT |Image$$TZBSP_STACK$$ZI$$Limit|
    IMPORT |Image$$TZBSP_STACK$$ZI$$Base|
    IMPORT tzbsp_dcache_flush_all

;============================================================================
;
; MODULE EXPORTS
;
;============================================================================

    EXPORT tzbsp_loop_here
    EXPORT tzbsp_switch_execute_smc

;============================================================================
;
; MODULE DATA AREA
;
;============================================================================

    PRESERVE8
    CODE32
    AREA TZBSP_ASM, CODE, READONLY, align=2

;======================================================================
; Called by tzbsp_error_hanlder only. We perform
; Clean up the registers and loop here until JTAG is connected.
;======================================================================
tzbsp_loop_here FUNCTION
    mov r0,#0
    mov r1,#0
    mov r2,#0
    mov r3,#0
    mov r4,#0
    mov r5,#0
    mov r6,#0
    mov r7,#0
    mov r8,#0
    mov r9,#0
    mov r10,#0
    mov r11,#0
    mov r12,#0
1 b %b1
    ENDFUNC

;======================================================================
; The main message loop. Each incoming syscall will be handled by this
; loop.
;======================================================================
tzbsp_switch_execute_smc FUNCTION
    mov r0, #0
    smc 0x0 ; After init just go to non-secure side.
1 blx tzbsp_syscall ; Every subsequent call is to msg processing.
    mov r0, #0
    smc 0x0 ; Msg handled, return to non-secure side.
    b %b1 ; Back to secure side, loop to msg processing.
    ENDFUNC

; This area is used currently by DAL for heap. It is defined here
; rather than the DAL layer so we can use the base address in our SCL
; file and MMU pagetable configurations
    AREA TZBSP_HEAP_AREA, DATA, READWRITE, align=12
tzbsp_heap % 0xA000

    END
