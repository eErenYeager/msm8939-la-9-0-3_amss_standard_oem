
/**
* @file tzos_syscall_def.c
* @brief All library SYSCALL need to defined in one place
*
*/

/*===========================================================================
   Copyright (c) 2010 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsee/mink/app_mgr/src/tzos_syscall_defs.c#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $


when       who      what, where, why
--------   ---      ------------------------------------
05/08/2014 dp      Initial
===========================================================================*/

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include "tzbsp.h"
#include "tzbsp_syscall.h"
#include "mink_services.h"
/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/
#define TZOS_FLAGS   TZ_SC_FLAG_ENABLE_INT   | \
                     TZ_SC_FLAG_O_RSP_ON_ERR | \
                     TZ_SC_FLAG_RSP

TZBSP_DEFINE_SYSCALL(MINK_SERVICE_SYSCALL_ID,
                     TZ_OS_APP_START_ID,
                     TZ_OS_APP_START_ID_PARAM_ID,
                     TZOS_FLAGS | TZ_SC_FLAG_RSP,
                     tzos_app_start);

TZBSP_DEFINE_SYSCALL(MINK_SERVICE_SYSCALL_ID,
                     TZ_OS_APP_SHUTDOWN_ID,
                     TZ_OS_APP_SHUTDOWN_ID_PARAM_ID,
                     TZOS_FLAGS | TZ_SC_FLAG_RSP,
                     tzos_app_shutdown);

TZBSP_DEFINE_SYSCALL(MINK_SERVICE_SYSCALL_ID,
                     TZ_OS_APP_LOOKUP_ID,
                     TZ_OS_APP_LOOKUP_ID_PARAM_ID,
                     TZOS_FLAGS | TZ_SC_FLAG_RSP,
                     tzos_app_lookup);

TZBSP_DEFINE_SYSCALL(MINK_SERVICE_SYSCALL_ID,
                     TZ_OS_APP_GET_STATE_ID,
                     TZ_OS_APP_GET_STATE_ID_PARAM_ID,
                     TZOS_FLAGS | TZ_SC_FLAG_RSP,
                     tzos_app_get_state);

TZBSP_DEFINE_SYSCALL(MINK_SERVICE_SYSCALL_ID,
                     TZ_APP_QSAPP_SEND_DATA_ID,
                     TZ_APP_QSAPP_SEND_DATA_ID_PARAM_ID,
                     TZOS_FLAGS | TZ_SC_FLAG_RSP,
                     tzapp_qsapp_send_data);

TZBSP_DEFINE_SYSCALL(MINK_SERVICE_SYSCALL_ID,
                     TZ_APP_QSAPP_SEND_DATA_WLIST_ID,
                     TZ_APP_QSAPP_SEND_DATA_WLIST_ID_PARAM_ID,
                     TZOS_FLAGS | TZ_SC_FLAG_RSP,
                     tzapp_qsapp_send_data_wlist);
					 
TZBSP_DEFINE_SYSCALL(MINK_SERVICE_SYSCALL_ID,
                     TZ_OS_APP_REGION_NOTIFICATION_ID,
                     TZ_OS_APP_REGION_NOTIFICATION_ID_PARAM_ID,
                     TZOS_FLAGS,
                     tzos_app_region_notification);

TZBSP_DEFINE_SYSCALL(MINK_SERVICE_SYSCALL_ID,
                     TZ_OS_REGISTER_LISTENER_ID,
                     TZ_OS_REGISTER_LISTENER_ID_PARAM_ID,
                     TZOS_FLAGS,
                     tzos_register_listener);

TZBSP_DEFINE_SYSCALL(MINK_SERVICE_SYSCALL_ID,
                     TZ_OS_DEREGISTER_LISTENER_ID,
                     TZ_OS_DEREGISTER_LISTENER_ID_PARAM_ID,
                     TZOS_FLAGS,
                     tzos_deregister_listener);

TZBSP_DEFINE_SYSCALL(MINK_SERVICE_SYSCALL_ID,
                     TZ_OS_LISTENER_RESPONSE_HANDLER_ID,
                     TZ_OS_LISTENER_RESPONSE_HANDLER_ID_PARAM_ID,
                     TZOS_FLAGS | TZ_SC_FLAG_RSP,
                     tzos_listener_response_handler);

TZBSP_DEFINE_SYSCALL(MINK_SERVICE_SYSCALL_ID,
                     TZ_OS_LISTENER_RESPONSE_WLIST_HANDLER_ID,
                     TZ_OS_LISTENER_RESPONSE_WLIST_HANDLER_ID_PARAM_ID,
                     TZOS_FLAGS,
                     tzos_listener_response_wlist_handler);
					 
TZBSP_DEFINE_SYSCALL(MINK_SERVICE_SYSCALL_ID,
                     TZ_OS_LOAD_SERVICES_IMAGE_ID,
                     TZ_OS_LOAD_SERVICES_IMAGE_ID_PARAM_ID,
                     TZOS_FLAGS,
                     tzos_load_services_image);

TZBSP_DEFINE_SYSCALL(MINK_SERVICE_SYSCALL_ID,
                     TZ_OS_UNLOAD_SERVICES_IMAGE_ID,
                     TZ_OS_UNLOAD_SERVICES_IMAGE_ID_PARAM_ID,
                     TZOS_FLAGS,
                     tzos_unload_services_image);

TZBSP_DEFINE_SYSCALL(MINK_SERVICE_SYSCALL_ID,
                     TZ_OS_REGISTER_LOG_BUFFER_ID,
                     TZ_OS_REGISTER_LOG_BUFFER_ID_PARAM_ID,
                     TZOS_FLAGS,
                     tzos_register_log_buffer);

TZBSP_DEFINE_SYSCALL(MINK_SERVICE_SYSCALL_ID,
                     TZ_OS_RPMB_PROVISION_KEY_ID,
                     TZ_OS_RPMB_PROVISION_KEY_ID_PARAM_ID,
                     TZOS_FLAGS | TZ_SC_FLAG_RSP,
                     tzos_rpmb_provision_key);

TZBSP_DEFINE_SYSCALL(MINK_SERVICE_SYSCALL_ID,
                     TZ_OS_RPMB_ERASE_ID,
                     TZ_OS_RPMB_ERASE_ID_PARAM_ID,
                     TZOS_FLAGS | TZ_SC_FLAG_RSP,
                     tzos_rpmb_erase);

TZBSP_DEFINE_SYSCALL(MINK_SERVICE_SYSCALL_ID,
                     TZ_OS_LOAD_EXTERNAL_IMAGE_ID,
                     TZ_OS_LOAD_EXTERNAL_IMAGE_ID_PARAM_ID,
                     TZOS_FLAGS,
                     tzos_load_external_image);

TZBSP_DEFINE_SYSCALL(MINK_SERVICE_SYSCALL_ID,
                     TZ_OS_UNLOAD_EXTERNAL_IMAGE_ID,
                     TZ_OS_UNLOAD_EXTERNAL_IMAGE_ID_PARAM_ID,
                     TZOS_FLAGS,
                     tzos_unload_external_image);

TZBSP_DEFINE_SYSCALL(MINK_SERVICE_SYSCALL_ID,
                     TZ_OS_KS_GEN_KEY_ID,
                     TZ_OS_KS_GEN_KEY_ID_PARAM_ID,
                     TZOS_FLAGS | TZ_SC_FLAG_RSP,
                     tzos_ks_gen_key);

TZBSP_DEFINE_SYSCALL(MINK_SERVICE_SYSCALL_ID,
                     TZ_OS_KS_DEL_KEY_ID,
                     TZ_OS_KS_DEL_KEY_ID_PARAM_ID,
                     TZOS_FLAGS | TZ_SC_FLAG_RSP,
                     tzos_ks_del_key);

TZBSP_DEFINE_SYSCALL(MINK_SERVICE_SYSCALL_ID,
                     TZ_OS_KS_GET_MAX_KEYS_ID,
                     TZ_OS_KS_GET_MAX_KEYS_ID_PARAM_ID,
                     TZOS_FLAGS | TZ_SC_FLAG_RSP,
                     tzos_ks_get_max_keys);

TZBSP_DEFINE_SYSCALL(MINK_SERVICE_SYSCALL_ID,
                     TZ_OS_KS_SET_PIPE_KEY_ID,
                     TZ_OS_KS_SET_PIPE_KEY_ID_PARAM_ID,
                     TZOS_FLAGS | TZ_SC_FLAG_RSP,
                     tzos_ks_set_pipe_key);

TZBSP_DEFINE_SYSCALL(MINK_SERVICE_SYSCALL_ID,
                     TZ_OS_KS_UPDATE_KEY_ID,
                     TZ_OS_KS_UPDATE_KEY_ID_PARAM_ID,
                     TZOS_FLAGS | TZ_SC_FLAG_RSP,
                     tzos_ks_update_key);

TZBSP_DEFINE_SYSCALL(MINK_SERVICE_SYSCALL_ID,
                     TZ_APP_GPAPP_OPEN_SESSION_ID,
                     TZ_APP_GPAPP_OPEN_SESSION_ID_PARAM_ID,
                     TZOS_FLAGS | TZ_SC_FLAG_RSP,
                     tzapp_gpapp_open_session);

TZBSP_DEFINE_SYSCALL(MINK_SERVICE_SYSCALL_ID,
                     TZ_APP_GPAPP_CLOSE_SESSION_ID,
                     TZ_APP_GPAPP_CLOSE_SESSION_ID_PARAM_ID,
                     TZOS_FLAGS | TZ_SC_FLAG_RSP,
                     tzapp_gpapp_close_session);

TZBSP_DEFINE_SYSCALL(MINK_SERVICE_SYSCALL_ID,
                     TZ_APP_GPAPP_INVOKE_COMMAND_ID,
                     TZ_APP_GPAPP_INVOKE_COMMAND_ID_PARAM_ID,
                     TZOS_FLAGS | TZ_SC_FLAG_RSP,
                     tzapp_gpapp_invoke_command);

extern const void *tz_syscall_core_start;
extern const void *tz_syscall_core_end;

//extern const void *tz_syscall_os_start;
//extern const void *tz_syscall_os_end;

