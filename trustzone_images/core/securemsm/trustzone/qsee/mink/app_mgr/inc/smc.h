#ifndef __QSEE_SMC_H
#define __QSEE_SMC_H

#include "lktypes.h"
#include "lkthread.h"
#include "lkmutex.h"
#include "lktimer.h"
#include "tzos_exec.h"
#include "tzbsp_log.h"
#include "qsee_errno.h"
#include "map.h"

typedef enum
{
  QSEE_APP_START_COMMAND = 0x01,
  QSEE_APP_SHUTDOWN_COMMAND,
  QSEE_APP_LOOKUP_COMMAND,
  QSEE_REGISTER_LISTENER,
  QSEE_DEREGISTER_LISTENER,
  QSEE_CLIENT_SEND_DATA_COMMAND,
  QSEE_LISTENER_DATA_RSP_COMMAND,
  QSEE_LOAD_EXTERNAL_ELF_COMMAND,
  QSEE_UNLOAD_EXTERNAL_ELF_COMMAND,
  QSEE_GET_APP_STATE_COMMAND,
  QSEE_LOAD_SERV_IMAGE_COMMAND,
  QSEE_UNLOAD_SERV_IMAGE_COMMAND,
  QSEE_APP_REGION_NOTIFICATION,
  QSEE_REGISTER_LOG_BUF_COMMAND,
  QSEE_RPMB_PROVISION_KEY_COMMAND,
  QSEE_RPMB_ERASE_COMMAND,
  QSEE_KS_GEN_KEY_COMMAND,
  QSEE_KS_DEL_KEY_COMMAND,
  QSEE_KS_GET_MAX_KEYS_COMMAND,
  QSEE_KS_SET_PIPE_KEY_COMMAND,
  QSEE_KS_UPDATE_KEY_COMMAND,
  QSEE_GP_OPEN_SESSION,
  QSEE_GP_INVOKE_COMMAND,
  QSEE_GP_CLOSE_SESSION,
}qsee_smc_command_type;

typedef enum
{
  QSEE_APP_STATE_SUSPENDED = 0x01,
  QSEE_APP_STATE_ACTIVE  //"Active" includes all other states than "suspended"
}qsee_app_state;


/*mdt must start with elf and program headers.*/
typedef struct img_info_type{
  uint32_t mdt_len;
  uint32_t img_len;
  uint32_t pa;
  char    app_name[32];
}img_info_t;

typedef struct qsee_common_request_type{
  uint32_t command_id;
} qsee_common_request_t;

typedef struct qsee_apps_region_notificaton_type{
  uint32_t applications_region_addr;
  uint32_t applications_region_size;
}qsee_apps_region_notification_t;

typedef struct qsee_rpmb_provision_key_req_type{
  uint32_t key_type;
}qsee_rpmb_provision_key_req_t;

typedef struct qsee_client_send_data_req_type {
  uint32_t app_id;
  void * req_ptr;
  uint32_t req_len;
  void * rsp_ptr;   /* First 4 bytes should always be the return status */
  uint32_t rsp_len;
}qsee_client_send_data_req_t;

typedef struct qsee_register_listener_type {
  uint32_t listener_id;
  void * req_ptr;
  uint32_t req_len;
}qsee_register_listener_t;

typedef struct qsee_deregister_listener_type {
  uint32_t listener_id;
}qsee_deregister_listener_t;

typedef struct qsee_get_app_state_type {
  uint32_t app_id;
}qsee_get_app_state_t;

typedef struct qsee_listener_rsp_type
{
  uint32_t listener_id;
  uint32_t status;
}qsee_listener_rsp_t;

/* register log buffer */
typedef struct qsee_reg_log_buf_req_type {
   void *      phy_addr;    /* physical address of log buffer area */
   uint32_t    len;         /* Length of buffer */
}qsee_reg_log_buf_req_t;
typedef struct qsee_load_serv_image_type
{
  uint32_t mdt_len;
  uint32_t img_len;
  uint32_t pa;
}qsee_load_serv_image_t;


typedef struct qsee_command_resp_info_type {
  uint32_t result;
  qsee_command_resp_type resp_type;
  unsigned int data;
} qsee_command_resp_info_t;

int qsee_process_app_start_smc(void* req, uint32_t reqlen, void* rsp, uint32_t rsplen);

int qsee_smc_handler(void* req, uint32_t reqlen, void* rsp, uint32_t rsplen);

thread_t* qsee_get_thread_blocked_on_listener(uint32_t listener_id);

//int qsee_validate_image_seg_range(uint32_t addr, uint32_t size);

int qsee_load_services_image(void* req, uint32_t reqlen, void* rsp, uint32_t rsplen);

int qsee_unload_services_image(void);

int qsee_remap_SD_MGR_error_codes(int input);
#endif
