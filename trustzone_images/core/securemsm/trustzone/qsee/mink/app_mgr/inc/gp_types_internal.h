#ifndef GPTYPESINTERNAL_H
#define GPTYPESINTERNAL_H

/* This file is code duplication from \core\api\trustzone\gp\gpTypes.h
 * The above file is located in TZ app lib api, which is not in the scope
 * of QSEE (secure OS).
 */

typedef uint32 TEE_Result;
#define TEE_ORIGIN_TEE                        0x00000003
#define TEE_ERROR_BAD_STATE                   0xFFFF0007    /* The operation is not valid in current state */
typedef union
{
  struct
  {
    void* buffer;
    uint32 size;
  } memref;
  struct
  {
    uint32 a, b;
  } value;
} TEE_Param;
typedef struct
{
  uint32 timeLow;
  uint16 timeMid;
  uint16 timeHiAndVersion;
  uint8  clockSeqAndNode[8];
} TEE_UUID;

/* Non-Standard data types */

/* TEE phys addr types */

typedef struct TEE_physical_buf_entry_t {
  uint32 phys_addr;
  uint32 len;
} TEE_physical_buf_entry;

typedef struct TEE_physical_addr_t {
  TEE_physical_buf_entry fragmnt[512];
} TEE_physical_addr;

#endif /* GPTYPESINTERNAL_H */
