#ifndef __QSEE_CORE_APPS_H
#define __QSEE_CORE_APPS_H

#include "lktypes.h"
#include "lkthread.h"
#include "lkmutex.h"
#include "lktimer.h"
#include "tzos_exec.h"
#include "map.h"
#include "gp_types_internal.h"
#include "mink_services.h"
#include "mink_object.h"
#include "kprocess.h"
#include "kthread.h"

#define QSEE_VERION_MAJOR 01
#define QSEE_VERION_MINOR 02

#define QSEE_RETURN 1
#define LEGACY_RETURN 2

#define MAX_UINT32 0xffffffff

extern unsigned int syscall_return_path;
extern unsigned int syscall_command_id;
extern unsigned int global_app_id;

//GP Request types

typedef struct qsee_gp_opensession_req_type_t {
  TEE_UUID    uuid;
  uint32_t    connectionMethod;
  uint32_t    connectionData;
  uint32_t    paramTypes;
  TEE_Param   params[4];
} qsee_gp_opensession_req_type;

typedef struct qsee_gp_invokecommand_req_type_t {
  uint32_t    sessionId;
  uint32_t    appCommandId;
  uint32_t    paramTypes;
  TEE_Param   params[4];
} qsee_gp_invokecommand_req_type;

typedef struct qsee_gp_closesession_req_type_t {
  uint32_t    sessionId;
} qsee_gp_closesession_req_type;

//GP Response Type
typedef struct gp_command_response_type_t {
  uint32_t    app_id;
  uint32_t    sessionId;
  TEE_Result  teeResult;
  uint32_t    returnOrigin;
} gp_command_response_type;

typedef struct apps_request_resp_info
{
  uint32_t command;
  const void* req;
  uint32_t reqlen;
  const void* rsp;
  uint32_t rsplen;
}apps_request_resp_info_t;

typedef struct mink_apps_invoke_info
{
  uint32_t app_id;
  Object   invokeObj;
}AppState;

#define MAX_NUM_APPS 0x10

typedef struct apps_listener_resp_info
{
  void* rsp;
  unsigned int rsplen;
}apps_listener_resp_info_t;

/**
   Does buf0 completely contain/envelope buf1?
*/
static bool contains(uintptr_t buf0, size_t buf0_len,
                     uintptr_t buf1, size_t buf1_len)
{
  return (buf0 <= buf1 && buf1 + buf1_len <= buf0 + buf0_len);
}

int qsee_process_app_smc(uint32_t app_id,
                         uint32_t command_id,
                         void* req, uint32_t reqlen,
                         void* rsp, uint32_t rsplen,
                         const void* sg_info_ptr, uint32_t sg_info_len);
/**
 * Called to notify QSEE client that warmboot has occurred
 */
void qsee_warmboot_notifier_func(void);

/**
 * Called to notify app manager that power collapse has occurred
 */
void qsee_power_collapse_notifier_func(void);

void qsee_app_entry_return(unsigned int init_result, void *entry, unsigned int stack_size, void *stack_base, char *name, uint32_t maxObjs, void *acceptBuf, uint32_t acceptBufSize, void *serverloop_entry);
void qsee_app_init_return(unsigned int sp);
unsigned int qsee_create_app_thread(unsigned int init_result, void *entry, unsigned int stack_size, void *stack_base, char *app_name, uint32_t maxObjs, void *acceptBuf, uint32_t acceptBufSize, void *serverloop_entry);
int qsee_app_entry(unsigned int entry);
int qsee_app_init(uint8_t major, uint8_t minor, unsigned int entry, unsigned int rw_base, unsigned int *sp);
int qsee_app_kill(int app_id);

void qsee_int_handler(uint32_t);


uint32 qsee_get_listener_buffer_size(unsigned int listener_id);
void* qsee_get_and_lock_listener_buffer(unsigned int listener_id);
void qsee_unlock_listener_buffer(unsigned int listener_id);

int qsee_request_service(unsigned int listener_id, void *req, unsigned int req_len, void *rsp, unsigned int rsplen);

int qsee_register_shared_buffer(void *address, unsigned int size);
int qsee_deregister_shared_buffer(void *address);
int qsee_prepare_shared_buf_for_nosecure_read(void * address, unsigned int size);
int qsee_prepare_shared_buf_for_secure_read(void *address, unsigned int size);

void qsee_app_shutdown(thread_t *pthread);

void qsee_set_global_flag(unsigned int flag);
unsigned int qsee_get_global_flag(void);

/*Structure of the command passed to QSEE. It is used for
 *1. App start
 *2. App shutdown
 *3. GP sessions
 */
typedef enum
{
  QSEE_APP_SMC_CMD_DEFAULT = 0,
  QSEE_START_CMD_TO_APP = 0xFF01,
  QSEE_SHUTDOWN_CMD_TO_APP,
  QSEE_FLUSH_CMD_TO_APP,
  QSEE_INVAL_CMD_TO_APP,
  GP_OPEN_SESSION_CMD_TO_APP,
  GP_CLOSE_SESSION_CMD_TO_APP,
  GP_INVOKE_COMMAND_CMD_TO_APP
}qsee_command_type;

typedef struct qsee_app_start_command_info_type {
  uint32_t elf_header_ptr; /*Elf header pointer*/
  uint32_t image_base_addr; /*Image base pointer*/
} qsee_app_start_command_info_t;

typedef struct qsee_app_shutdown_command_info_type {
  uint32_t app_id; /*App ID*/
} qsee_app_shutdown_command_info_t;

typedef struct qsee_app_lookup_command_info_type {
  char app_name[THREAD_NAME_LEN]; /*App Name*/
} qsee_app_lookup_command_info_t;

typedef struct qsee_command_info_type {
  qsee_command_type command;
  void *data;
} qsee_command_info_t;

/*Structure of the command response from QSEE. It is used for
 *1. App start
 *1. App shutdown
 */
typedef enum
{
  QSEE_APP_ID      = 0x0000EE01, /**< QSEE response type when the response
                                  data is the application ID. */
  QSEE_LISTENER_ID = 0x0000EE02, /**< QSEE response type when the response
                                  data is the listener ID. */
  QSEE_APP_STATE   = 0x0000EE03, /**< QSEE response type when the response
                                  data is the app state. */
  QSEE_NONE   = 0x0000EE04,
  QSEE_GENERAL     = 0x0000EE05, /**< QSEE response type when the response
                                      data is specific to the request */
  QSEE_RESERVED    = INT_MAX /**< Required to make the enum 4 bytes. */
}qsee_command_resp_type;

typedef struct listener_info_type{
  uint32_t listener_id;
  void*    req_ptr;
  uint32_t req_len;
  thread_t* thread;
  uint32_t status;
}listener_info_t;

void qsee_initialize_listener_table(void);
int qsee_register_listener(uint32_t listener_id, void * req_ptr, uint32_t req_len);
int qsee_deregister_listener(uint32_t listener_id);
//int qsee_process_listener_response(uint32_t listener_id, void * rsp_ptr, uint32_t rsp_len);
int32_t qsee_process_listener_response(uint32_t listener_id,
                                       int32_t status,
                                       const void* sg_info_ptr,
                                       size_t sg_info_len);

typedef struct qsee_app_reqrsp_protection_type {
  uint32_t app_id;
  uint32_t addr;
  uint32_t  len;
}qsee_app_reqrsp_protection_t;

int qsee_protect_reqrsp_region(uint32_t app_id, uint32_t addr, uint32_t len);

void qsee_unprotect_reqrsp_region(void);

void qsee_app_faulted(void);
void qsee_set_app_faulted(void);

void qsee_return_smc_handler(void);

int qsee_validate_ptrs_belong_to_app(uint32 address, uint32 size, thread_t *t,
                                     boolean commonlib, uint32* bytes_left,
                                     uint32_t memory_type);
int qsee_is_range_in_app_segments(uint32 address, uint32 size, segments *s,
                                  uint32* bytes_left);

/*Checks if [addr,size-1] is mapped by ANY application*/
int qsee_is_mapped_by_application(uint32_t address, uint32_t size);


//GP QSEE Functions
void qsee_gp_opensession (
    unsigned int app_id,
    void* request,  uint32_t reqlen,
    void* response, uint32_t rsplen);

void qsee_gp_invokecommand (
    unsigned int app_id,
    void* request,  uint32_t reqlen,
    void* response, uint32_t rsplen);

void qsee_gp_closesession (
    unsigned int app_id,
    void* request,  uint32_t reqlen,
    void* response, uint32_t rsplen);

/**
 * Adds to an existing validation buffer to be used by QSEE to
 * validate calls by a TA to share memory.
 *
 * @param[in] thread The thread that requested the listener being
 *                   responded to.
 *
 * @param[in] buf The listener buffer that contains whitelist
 *                information.
 *
 * @param[in] buf_len Length of buf parameter in bytes.
 *
 * @param[in] sg_info_ptr Non-secure buffer describing where to find
 *                        lists of address/length pairs within the
 *                        request buffer.  This parameter is optional.
 *                        If it is not passed a whitelist allowing all
 *                        addresses will be created.
 *
 * @param[in] sg_info_len Length in bytes of the memory pointed to by
 *                        sg_info_ptr.
 *
 * @return 0 for success, any other value for failure
 */
int32_t qsee_amend_whitelist(const thread_t* pthread,
                             const void* buf,
                             size_t buf_len,
                             const void* sg_info_ptr,
                             size_t sg_info_len);

#endif
