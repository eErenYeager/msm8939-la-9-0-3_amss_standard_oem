/*
 * Copyright (c) 2016  Qualcomm Technologies, Inc.  All Rights Reserved.
 * Qualcomm Technologies Proprietary and Confidential.
 */

#ifndef __WHITELIST_H__
#define __WHITELIST_H__

#include <stdint.h>
#include <stddef.h>
#include "tz_syscall_pub.h"
#include "heap.h"

/**
   A consolidated list of address/length pairs sent by a client to a
   TA that is saved for each call into a TA and used for validations
   against TA requests to share memory.
 */
typedef struct {
  ScatterListEntryAA32* data;
  size_t                numEntries;
  bool                  isLax;
} Whitelist;

/**
   Populates a validation buffer to be used by QSEE to validate calls
   by a TA to share memory.

   @param[in] reqBuf Request buffer information is used for input
                     and validation buffer is populated as output.

   @param[in] reqBufLen Length of reqBuf parameter in bytes.

   @param[in] nsQWLBuf Non-secure buffer describing where to find
                       lists of address/length pairs within the
                       request buffer.  This parameter is optional.
                       If it is not passed a whitelist allowing all
                       addresses will be created.

   @param[in] nsQWLBufLen Length in bytes of the memory pointed to by
                          nsQWLBuf.

   @param[out] me Whitelist object that may be used for validations of
                  buffers passed by qseecom to TAs on success.

   @return QSEE_RESULT_SUCCESS on success
*/
int32_t Whitelist_construct(const void* reqBuf, size_t reqBufLen,
                            const void* nsQWLBuf, size_t nsQWLBufLen,
                            Whitelist* newWL);

/**
   Creates a new validation buffer that extends the one that is
   passed as input.

   @param[inout] me Whitelist object that may be used for validations
                    of buffers passed by qseecom to TAs.

   @param[in] reqBuf Request buffer information is used for input
                     and validation buffer is populated as output.

   @param[in] reqBufLen Length of reqBuf parameter in bytes.

   @param[in] nsQWLBuf Non-secure buffer describing where to find
                       lists of address/length pairs within the
                       request buffer.  This parameter is optional.
                       If it is not passed a whitelist allowing all
                       addresses will be created.

   @param[in] nsQWLBufLen Length in bytes of the memory pointed to by
                          nsQWLBuf.

   @param[out] newWL Whitelist created based on the input parameters.

   @return QSEE_RESULT_SUCCESS on success
*/
int32_t Whitelist_constructAmended(const Whitelist *me, const void* reqBuf,
                                   size_t reqBufLen, const void* nsQWLBuf,
								   size_t nsQWLBufLen, Whitelist* newWL);

/**
 * Validates buffer is entirely within the scatter list associated
 * with the service request.
 *
 * @param[in] me
 * @param[in] buf
 * @param[in] bufLen
 *
 * @return Boolean value indicating whether the buffer described by
 *         the input parameters falls entirely within the scatter
 *         gather list provided by the caller.
 */
bool Whitelist_contains(const Whitelist* me, const void* buf, size_t bufLen);


/**
 * Frees resources associated with a call to Whitelist_construct().
 *
 * @param[in] me
 */
void Whitelist_destruct(Whitelist *me);

#endif /* #define __WHITELIST_H__ */
