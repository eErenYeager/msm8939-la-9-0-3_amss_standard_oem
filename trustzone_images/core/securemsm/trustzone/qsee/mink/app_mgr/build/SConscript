#===============================================================================
#
#                             Edit History
# $Header: //source/qcom/qct/core/kernel/lk/rel/1h10/build/SConscript#3 $
#
# when         who     what, where, why
# ----------   ---     ---------------------------------------------------------
# 2014/08/13   cz      Added keyprov path
# 2012/12/16   mg      Add qsee_log.c to QSEE_SEC_APP_SOURCES
# 2011/07/12   spa     Create initial version
#
#===============================================================================
#                    Copyright (c) 2013 QUALCOMM Incorporated.
#                           All Rights Reserved.
#                         QUALCOMM Proprietary/GTDR
#===============================================================================
# QSEE Lib
#-------------------------------------------------------------------------------
Import('env')
env = env.Clone()

#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------
#SRCPATH = ".."
SRCPATH = "${BUILD_ROOT}/core/securemsm/trustzone/qsee/mink/app_mgr"

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0) 

#-------------------------------------------------------------------------------
# Internal depends within CoreBSP
#-------------------------------------------------------------------------------
CBSP_APIS = [
   'BOOT',
   'BUSES',
   'DAL',
   'DEBUGTOOLS',
   'HAL',
   'SERVICES',
   'SYSTEMDRIVERS',
   'KERNEL',
   'SECUREMSM',   
   'TZKS',   
   'SECAPP',
   'MINK_LK',
   'MINK',
]

env.RequirePublicApi(CBSP_APIS)
env.RequirePrivateApi(CBSP_APIS)

CBSP_APIS.extend(['TZLIBARMV7',
                  'TZCHIPSET',
                  'TZCRYPTO',
                  'TZIMGAUTH',])               

env.RequireRestrictedApi(CBSP_APIS)

INC_PATH_API = ["${INC_ROOT}/core/api/securemsm/trustzone/qsee",
               "${INC_ROOT}/core/api/kernel/libstd/stringl",
               "${BUILD_ROOT}/core/securemsm/trustzone/qsee/drivers/sec_ctrl/src"]
               
env.Append(CPPPATH = INC_PATH_API)

INC_PATH_QSEE = [
      "${INC_ROOT}/core/securemsm/trustzone/qsee/kernel/inc",
      "${INC_ROOT}/core/securemsm/trustzone/qsee/services/elfloader/inc",
      "${INC_ROOT}/core/securemsm/trustzone/qsee/services/external",
      "${INC_ROOT}/core/securemsm/trustzone/qsee/services/fuseprov/inc",
      "${INC_ROOT}/core/securemsm/trustzone/qsee/services/keyprov/inc",
      "${INC_ROOT}/core/securemsm/unifiedcrypto/core/ecc/inc",
]
env.Append(CPPPATH = INC_PATH_QSEE)


INC_PATH_STORAGE = ["${INC_ROOT}/core/storage/sd_mgr/inc"]
env.Append(CPPPATH = INC_PATH_STORAGE)

env.Append(CPPPATH = "${INC_ROOT}/core/securemsm/trustzone/qsee/services/external/inc")
env.Append(CPPPATH = "${INC_ROOT}/core/storage/sd_mgr/inc/")


#env.Append(CCFLAGS = " --diag_suppress=1786,2523 ")
#env.Append(ASFLAGS = " --diag_suppress=1786,2523 ")

#-------------------------------------------------------------------------------
# Compiler options
#-------------------------------------------------------------------------------
#env.Append(CCFLAGS = "--gnu")

#-------------------------------------------------------------------------------
# Sources, libraries
#-------------------------------------------------------------------------------

QSEE_SEC_APP_SOURCES = [
   '${BUILDPATH}/src/apps.c',
   '${BUILDPATH}/src/apps_unpaged.c',
   '${BUILDPATH}/src/message.c',
   '${BUILDPATH}/src/qsee_handle.c',
   '${BUILDPATH}/src/map.c',
   '${BUILDPATH}/src/qsee_log.c',
   '${BUILDPATH}/src/smc.c',
   '${BUILDPATH}/src/tzos_legacy.c',
   '${BUILDPATH}/src/syscall_handler.c',
   '${BUILDPATH}/src/tzos_init_thread.s',
   '${BUILDPATH}/src/whitelist.c',
]

QSEE_SYSCALL_DEF_SOURCES = [
   '${BUILDPATH}/src/tzos_syscall_defs.c',
]

CLEAN_SOURCES = [
    '${BUILDPATH}/src/fuse_hw_mutex.c',
]

#-------------------------------------------------------------------------------
# Add Libraries to image
#-------------------------------------------------------------------------------
env.AddBinaryLibrary(['TZOS_IMAGE'], '${BUILDPATH}/QSEESECAPP', QSEE_SEC_APP_SOURCES)

# These are ARMv8 SMC syscall functions exposed to HLOS
env.AddObject('TZOS_IMAGE', QSEE_SYSCALL_DEF_SOURCES)

env.CleanPack('TZOS_IMAGE', CLEAN_SOURCES)

env.LoadAPIUnits()
