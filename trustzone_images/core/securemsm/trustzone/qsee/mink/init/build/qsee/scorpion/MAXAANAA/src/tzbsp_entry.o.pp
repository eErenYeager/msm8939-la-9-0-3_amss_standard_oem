# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/mink/init/src/tzbsp_entry.s"
# 1 "<built-in>" 1
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/mink/init/src/tzbsp_entry.s" 2
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;
; T R U S T Z O N E E N T R Y
;
; GENERAL DESCRIPTION
; This file contains the TZBSP main entry
;
; EXTERNALIZED SYMBOLS
; __main
; _main
;
; INITIALIZATION AND SEQUENCING REQUIREMENTS
;
;
; Copyright (c) 2011-2012 by Qualcomm Technologies, Incorporated.All Rights Reserved.
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;
; EDIT HISTORY FOR FILE
;
; This section contains comments describing changes made to the module.
; Notice that changes are listed in reverse chronological order.
;
; $Header:
;
; when who what, where, why
; -------- --- --------------------------------------------------------
; 03/28/11 tk Initial version
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*


;============================================================================
;
; MODULE INCLUDES
;
;============================================================================

# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8936/cfg/mmu/tzbsp_target.h" 1
# 43 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8936/cfg/mmu/tzbsp_target.h"
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/customer.h" 1
# 80 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/customer.h"
# 1 "./custmaxaanaaa.h" 1
# 10 "./custmaxaanaaa.h"
# 1 "./targmaxaanaaa.h" 1
# 11 "./custmaxaanaaa.h" 2
# 144 "./custmaxaanaaa.h"
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/custremoteapis.h" 1
# 145 "./custmaxaanaaa.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/custtarget.h" 1
# 146 "./custmaxaanaaa.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/custsdcc.h" 1
# 147 "./custmaxaanaaa.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/custsurf.h" 1
# 148 "./custmaxaanaaa.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/custdiag.h" 1
# 149 "./custmaxaanaaa.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/custefs.h" 1
# 150 "./custmaxaanaaa.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/custpmic.h" 1
# 151 "./custmaxaanaaa.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/custsio_8660.h" 1
# 152 "./custmaxaanaaa.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/custsec.h" 1
# 121 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/custsec.h"
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/custsfs.h" 1
# 122 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/custsec.h" 2
# 153 "./custmaxaanaaa.h" 2
# 81 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/customer.h" 2
# 44 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8936/cfg/mmu/tzbsp_target.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8936/cfg/mmu/../common/tzbsp_target.h" 1
# 45 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8936/cfg/mmu/tzbsp_target.h" 2
# 39 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/mink/init/src/tzbsp_entry.s" 2
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/include/tzbsp_shared_imem.h" 1
# 27 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/include/tzbsp_shared_imem.h"
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/api/services/target.h" 1
# 28 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/include/tzbsp_shared_imem.h" 2
# 40 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/mink/init/src/tzbsp_entry.s" 2
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/include/tzbsp_asm.h" 1
# 35 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/include/tzbsp_asm.h"
; ---------------------
; MACRO: Mov32
; ---------------------
; Moves a 32 bit value into a register
  MACRO
  Mov32 $r, $val_l
    movw $r, #($val_l & 0xFFFF)
    movt $r, #(($val_l >> 16) & 0xFFFF)
  MEND

; ---------------------
; MACRO: Read32R
; ---------------------
; Reads a given word where the address is in a register
  MACRO
  Read32R $val_r, $addr_r, $work_r
    Mov32 $work_r, 0x02000000
    cmp $addr_r, $work_r ; compare address to remap area
    mov $work_r, #0
    bhs %f10 ; addr >= remap, no need to offset
    Mov32 $work_r, 0x04800000

10 ldr $val_r, [$addr_r, $work_r]
  MEND

; ---------------------
; MACRO: Read32L
; ---------------------
; Reads a given word where the address is a literal
  MACRO
  Read32L $val_r, $addr_l, $work_r
    Mov32 $val_r, $addr_l
    Read32R $val_r, $val_r, $work_r
  MEND

; ---------------------
; MACRO: Write32R
; ---------------------
; Writes a given word where the address is in a register
  MACRO
  Write32R $val_r, $addr_r, $work_r
    Mov32 $work_r, 0x02000000
    cmp $addr_r, $work_r ; compare address to remap area
    mov $work_r, #0
    bhs %f10 ; addr >= remap, no need to offset
    Mov32 $work_r, 0x04800000

10 str $val_r, [$addr_r, $work_r]
  MEND

; ---------------------
; MACRO: Write32L
; ---------------------
; Writes a given word where the address is a literal
  MACRO
  Write32L $val_r, $addr_l, $work_r, $addr_r
    Mov32 $addr_r, $addr_l
    Write32R $val_r, $addr_r, $work_r
  MEND
# 41 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/mink/init/src/tzbsp_entry.s" 2

    GET tzbsp_libarm_asm.inc
    GET tzbsp_asm.inc
    GET qctps_common_macros.inc
    GET tzbsp_sys_arch.inc

;============================================================================
;
; MODULE DEFINES
;
;============================================================================

;============================================================================
;
; MODULE IMPORTS
;
;============================================================================

    IMPORT tzbsp_cold_init
    IMPORT tzbsp_cpu_init
    IMPORT tzbsp_loop_here
    IMPORT tzbsp_switch_execute_smc
    IMPORT |Image$$TZBSP_STACK$$ZI$$Limit|
    IMPORT |Image$$TZBSP_STACK$$ZI$$Base|
    IMPORT |TZBSP_UNCACHED_ZI_BOOT_STACK$$Limit|

    IMPORT qsee_undef_handler
    IMPORT qsee_swi_handler
    IMPORT qsee_pabort_handler
    IMPORT qsee_dabort_handler

    IMPORT tzbsp_fiq_handler
    IMPORT tzbsp_irq_handler
    IMPORT tzbsp_smp_notifier
;============================================================================
;
; MODULE EXPORTS
;
;============================================================================

    ; Export the symbols __main and _main to prevent the linker from
    ; including the standard runtime library and startup routine.
    EXPORT __main
    EXPORT _main

    EXPORT tzbsp_has_cpu_cold_booted

    EXPORT tzbsp_was_l2_pc
    EXPORT tzbsp_was_gdhs_pc
    EXPORT tzbsp_reset_vector
    EXPORT tzbsp_set_cntfrq
    EXPORT tzbsp_smp_notifier_asm
    EXPORT tzbsp_get_pre_fiq_ctx



;============================================================================
;
; MODULE DATA AREA
;
;============================================================================

    ;========================================================================
    ; Section TZBSP_VECTOR_TABLE must be defined first in the scatter-load
    ;========================================================================
    PRESERVE8
    CODE32
    AREA TZBSP_VECTOR_TABLE, CODE, READONLY, ALIGN=5
    ENTRY
__main
_main

tzbsp_reset_vector
    ldr pc, =tzbsp_entry_chooser ; Reset
    ldr pc, =qsee_undef_handler ; Undefined instruction
    ldr pc, =qsee_swi_handler ; SVC
    ldr pc, =qsee_pabort_handler ; Prefetch abort
    ldr pc, =qsee_dabort_handler ; Data abort
    ldr pc, =tzbsp_loop_here ; Not used / reserved
    ldr pc, =irq_handler ; IRQ
    ldr pc, =fiq_handler ; FIQ

tzbsp_entry_chooser
    ldr sp, =tzbsp_boot_cpu_cold_booted ; check if TZBSP Cold boot is done / boot cpu booted once
    ldr sp, [sp]
    cmp sp, #0
    beq tzbsp_entry_handler ; Entry from SPBL
    b tzbsp_reset_handler ; CPU1 cold boot, or any warm boot entry

;======================================================================
; When the reset handler is invoked we are either doing a secondary CPU cold
; boot or warm booting any of the CPUs. CPU0 cold boot is a special case
; handled in tzbsp_entry_handler. Because this is a real reset handler, we can
; use all available registers.
; TODO: Give each processor its own stack so calls can be handled concurrently
; by the idle thread.
;======================================================================
tzbsp_reset_handler

    ; ------------------------------------------------------------------
    ; Verify our load address.
    ; ------------------------------------------------------------------
    adr r0, tzbsp_reset_handler
    ldr r1, =tzbsp_reset_handler
    cmp r0, r1
0 bne %0

    ; -----------------------------------------------------------------------
    ; Set up a stack pointer in Supervisor,Undefined,Abort mode.
    ; -----------------------------------------------------------------------
    ; Set the Stack Pointer address for supervisor mode
    ; Use CPU specific stack so we booting can be concurrent
    msr CPSR_c,#TZBSP_Mode_SVC:OR:TZBSP_I_Bit:OR:TZBSP_F_Bit
    ldr sp, =tzbsp_boot_stack_start ; load base address for stack
    CurCPUNum r0, r1 ; Determine the current CPU

    mov r1, #0x200
    mul r1, r0 ; obtain stack offset for CPU
    sub sp, r1 ; shift stack to CPU specific stack

    ; Set the Stack Pointer address for undefined mode
    ldr r0, =tzbsp_abt_stack
    add r0, #0x400 ; Size is defined below.
    sub r0, r0, #0x8
    msr CPSR_c, #TZBSP_Mode_UND:OR:TZBSP_I_Bit:OR:TZBSP_F_Bit
    mov r13, r0 ; set Undefined SP

    ; Set the Stack Pointer address for abort mode
    msr CPSR_c, #TZBSP_Mode_ABT:OR:TZBSP_I_Bit:OR:TZBSP_F_Bit
    mov r13, r0 ; set Abort SP

    ; Set the Stack Pointer address for irq mode
    msr CPSR_c, #TZBSP_Mode_IRQ:OR:TZBSP_I_Bit:OR:TZBSP_F_Bit
    ldr sp, =tzbsp_irq_stack_start ; load base address for stack


    ; Switch back to Supervisor mode
    msr CPSR_c, #TZBSP_Mode_SVC:OR:TZBSP_I_Bit:OR:TZBSP_F_Bit

    ; ------------------------------------------------------------------
    ; Setup the Vector Base Address Register (VBAR) to our vectors
    ; ------------------------------------------------------------------
    ldr r0, =tzbsp_reset_vector
    mcr p15, 0, r0, c12, c0, 0

    ; Force flush/refetch of prefetch pipeline
    misb

    ; Make space on the stack for HLOS jump address return
    ; Make space on the stack for Hypervisor entry address
    ; Make space on the stack for HLOS and hyp class (is AArch64?)
    push {r0, r1, r2, r3, r4, r5}

    ; Set up parameters to tzbsp_cpu_init
    add r0, sp, #0x0
    add r1, sp, #0x8
    add r2, sp, #0x10
    add r3, sp, #0x14

    ; Call initializer in C
    blx tzbsp_cpu_init

    ; TODO: use #define for monitor command ID
    ; Return values from tzbsp_cpu_init are still on
    ; stack. Fill them into registers as follows:
    ; x0 = monitor command ID
    ; x1 = NS entry physical address
    ; x2 = NS entry physical address
    ; x3 = hyp entry lower 32 bits
    ; x4 = hyp entry upppre 32 bits
    ; x5 = NS EL1 ELF EI class
    ; x6 = NS EL2 ELF EI class
    pop {r1, r2, r3, r4, r5, r6}
    mov r0, #1
    smc #0

;============================================================================
; Handles the init routine and main entry point for TZBSP
; SMC lock is held by the monitor at this point
; Secondary cores should be in held in reset
;
; Tasks include:
; (1) Save shared boot info passed in r0
; (2) Jump to the Trustzone Init handler
;============================================================================
tzbsp_entry_handler
    ; Preserve secboot shared info
    mov r4, r0

    ; ------------------------------------------------------------------
    ; Start by disabling IRQ and FIQ interrupts, when the boot flows
    ; from SPBL we cannot be sure of IRQ and FIQ masks.
    ; ------------------------------------------------------------------
    msr CPSR_c,#TZBSP_Mode_SVC:OR:TZBSP_I_Bit:OR:TZBSP_F_Bit

    ; ------------------------------------------------------------------
    ; Verify our load address.
    ; ------------------------------------------------------------------
    adr r0, tzbsp_entry_handler
    ldr r1, =tzbsp_entry_handler
    cmp r0, r1
invalid_addr_trap
    bne invalid_addr_trap

    ; Update Boot CPU boot status. TZBSP Cold boot is done.
    ldr r0, =tzbsp_boot_cpu_cold_booted
    mov r1, #1 ; Cold boot done
    str r1, [r0] ; Save boot status

    ; -----------------------------------------------------------------------
    ; Set up a stack pointer in Supervisor, Undefined, Abort mode.
    ; -----------------------------------------------------------------------
    ; Set the Stack Pointer address for supervisor mode
    ldr r0, =|Image$$TZBSP_STACK$$ZI$$Limit|
    mov r13, r0

    ; Set the Stack Pointer address for undefined mode
    ldr r0, =tzbsp_abt_stack
    add r0, #0x400 ; Size is defined below.
    sub r0, r0, #0x8
    msr CPSR_c, #TZBSP_Mode_UND:OR:TZBSP_I_Bit:OR:TZBSP_F_Bit
    mov r13, r0 ; set Undefined SP

    ; Set the Stack Pointer address for abort mode
    msr CPSR_c, #TZBSP_Mode_ABT:OR:TZBSP_I_Bit:OR:TZBSP_F_Bit
    mov r13, r0 ; set Abort SP

    ; Set the Stack Pointer address for irq mode
    msr CPSR_c, #TZBSP_Mode_IRQ:OR:TZBSP_I_Bit:OR:TZBSP_F_Bit
    ldr sp, =tzbsp_irq_stack_start ; load base address for stack

    ; Switch back to Supervisor mode
    msr CPSR_c, #TZBSP_Mode_SVC:OR:TZBSP_I_Bit:OR:TZBSP_F_Bit

    ; ------------------------------------------------------------------
    ; Setup the Vector Base Address Register (VBAR) to our vectors
    ; ------------------------------------------------------------------
    ldr r0, =tzbsp_reset_vector
    mcr p15, 0, r0, c12, c0, 0

    ; Force flush/refetch of prefetch pipeline
    misb

    ; ------------------------------------------------------------------
    ; Pass locations of secboot shared info, passed by SBL as a pointer,
    ; currently in r4.
    ; ------------------------------------------------------------------
    mov r0, r4

    ; Jump to TZBSP C initializer. Never return.
    blx tzbsp_cold_init

;======================================================================
; IRQ handler for the kernel
;
; Call directly into the monitor to handle the IRQ context switching.
; The r0 and r1 values are passed to the monitor and restored when the
; context is restored. This way we don't need a separate IRQ stack
;======================================================================
irq_handler

    clrex ; Clear exclusive monitors
    sub lr, #4 ; Shift return address by IRQ offset


    ; Save pre-IRQ context and call secure IRQ ISR
    ; r0 and r1 must persist across NS return, due to SMC call to EL3
    ; for IRQ exit to NS
    push {r0-r1}
    push {r2-r12, lr}
    blx tzbsp_irq_handler
    pop {r2-r12, lr}

     ldr r0, =(16*4) ; SP must point to the stack. EL3 will
    sub r1, sp, r0 ; treat R1 as a SP and populate it with
                                    ; NS arguments.

    mov r0, #0xF ; SMC_CMD_IRQ

    smc #0 ; Returns to NS


    pop {r0-r1} ; Restore r0 and r1 from original interrupt

    movs pc, lr ; return from interrupt

;======================================================================
; FIQ handler for the kernel
;
; The FIQ stack must be configured each time because the call to the C
; code FIQ handler will not return if the FIQ interrupted NS
; execution, so the stack must be reset for each call. FIQ handling
; uses the same stack as IRQ handling, so nested interrupts are not
; supported. R8 and R9 are banked for FIQ mode, so those registers
; may be used to set the stack.
;======================================================================
fiq_handler
    ldr sp, =tzbsp_fiq_stack_start ; load base address for stack
    CurCPUNum r8, r9 ; Determine the current CPU
    mov r9, #0x400
    mul r8, r9 ; obtain stack offset for CPU
    sub sp, r8 ; shift stack to CPU specific stack


    push {r0-r7, lr}
    blx tzbsp_fiq_handler ; Call the FIQ handler in C
    pop {r0-r7, lr}
    sub lr, lr, #4
    movs pc, lr

;======================================================================
; Get the secure EL1 context when the last FIQ interrupted EL1
; execution
;======================================================================
tzbsp_get_pre_fiq_ctx FUNCTION
    push {r4-r8, lr}
    ldr r1, =tzbsp_fiq_stack_start ; load base address for stack
    CurCPUNum r2, r3 ; Determine the current CPU
    mov r3, #0x400
    mul r2, r3 ; obtain stack offset for CPU
    sub r1, r2 ; shift stack to CPU specific stack
    mov r2, #(9*4) ; Constant offset for registers
    sub r1, r2 ; + context offset
    ldmia r1, {r1-r8,r14} ; loading r0-r7, shifted by 1 reg
    stmia r0!, {r1-r8} ; storing r0-r7, shifted by 1 reg
    msr CPSR_c, #TZBSP_Mode_SVC:OR:TZBSP_I_Bit:OR:TZBSP_F_Bit
    stmia r0!, {r8-r13} ; storing r8-r13
    msr CPSR_c, #TZBSP_Mode_FIQ:OR:TZBSP_I_Bit:OR:TZBSP_F_Bit
    str r14, [r0] ; storing r14
    pop {r4-r8, pc}
    ENDFUNC

;======================================================================
; Symmetric Multiprocessing notifier from EL3
; Per-CPU stack needs to be configured each time
; on entry.
; CMD 0x0 - Power collapse:
; r0 = PC state
; r1 = PC argument
; CMD 0x1 - SPMI Bus disable:
; r0 = spare
; r1 = (unused)
;
; YOU ARE FORBIDDEN FROM ADDING ANY OTHER USE CASE TO THE SMP NOTIFIER!
;======================================================================
tzbsp_smp_notifier_asm FUNCTION
    ; Set up the CPU specific stack using boot-stack since PC and boot should
    ; not occur at the same time.
    ldr sp, =tzbsp_boot_stack_start ; load base address for stack
    CurCPUNum r3, r4 ; Determine the current CPU
    mov r4, #0x200
    mul r3, r4 ; obtain stack offset for CPU
    sub sp, r3 ; shift stack to CPU specific stack

    blx tzbsp_smp_notifier

    mov r1, r0 ; Move return info to r1
    mov r0, #0xE
    smc #0
    ENDFUNC

;======================================================================
; Sets CNTFRQ, so as to be readable by PL0 and PL1
;
; r0 contains the frequency
;======================================================================
tzbsp_set_cntfrq FUNCTION
    mcr p15, 0, r0, c14, c0, 0
    bx lr
    ENDFUNC

;======================================================================
; TZBSP data area
;======================================================================
    AREA TZBSP_DATA, DATA, READWRITE
tzbsp_abt_stack SPACE 0x400
tzbsp_shared_imem_slp_ticks_0 DCD ((0x08600000 + 0x734) + (0x04))

; Uncached data is for items that are read/written both when the MMU
; is on and off. This area is enforced ZI by memset in init.
    AREA TZBSP_UNCACHED_ZI_PC_DATA, DATA, READWRITE, ALIGN=2
; Array of values, nonzero indicating CPU has not booted. This needs to be
; 4-Byte Aligned
tzbsp_has_cpu_cold_booted SPACE (4 * 8)
tzbsp_was_l2_pc SPACE (4 * 2); L2 power collapse status copy.
tzbsp_was_gdhs_pc SPACE (4 * 2); L2 GDHS collapse status copy.
tzbsp_boot_cpu_cold_booted SPACE (4) ; Keep Track if TZ Cold Init done. 8936/8939 Boot cpu is 0/4.
                                                                  ; Boot CPU is CPU 0 not a valid assumption.


; Boot stack is place do uncached memory as it is being used before and after
; MMU enablement.
    AREA TZBSP_UNCACHED_ZI_BOOT_STACK, DATA, READWRITE, ALIGN=3
tzbsp_boot_stack_end SPACE 0x200 * 8
tzbsp_boot_stack_start DCD 0xdeadbeef

    AREA TZBSP_FIQ_STACK, DATA, READWRITE, ALIGN=3
tzbsp_fiq_stack_end SPACE 0x400 * 8
tzbsp_fiq_stack_start DCD 0xdeadbeef

    AREA TZBSP_IRQ_STACK, DATA, READWRITE, ALIGN=3
tzbsp_irq_stack_end SPACE 0x400
tzbsp_irq_stack_start DCD 0xdeadbeef
    END
