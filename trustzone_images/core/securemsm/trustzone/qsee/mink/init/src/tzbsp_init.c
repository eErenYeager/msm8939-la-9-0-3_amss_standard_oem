/**
@file tzbsp_init.c
@brief Trustzone Init Handler

This file contains the routines for initializing TZBSP.

*/
/*===========================================================================
   Copyright (c) 2010-2012 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsee/mink/init/src/tzbsp_init.c#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $


when       who      what, where, why
--------   ---     ------------------------------------------------------------
=============================================================================*/

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include <comdef.h>
#include <string.h>
#include "HALhwio.h"

#include "tzbsp_errno.h"
#include "tzbsp.h"
#include "tzbsp_chipset.h"
#include "tzbsp_sys.h"
#include "tzbsp_config.h"
#include "tzbsp_xpu.h"
#include "tzbsp_log.h"
#include "tzbsp_target.h"
#include "tzbsp_diag.h"
#include "tzbsp_exec.h"
#include "crypto/tzbsp_cipher.h"
#include "tzbsp_secboot.h"
#include "tzbsp_mem.h"
#include "tzbsp_syscall.h"
#include "tz_mc.h"
#include "tt_tz.h"
#include "mapper.h"
#include "nmrr_prrr.h"
#include "qsee_mrm.h"
#include "memory_defs.h"
#include "tzbsp_init_arch.h"
#include "tzbsp_secboot_arch.h"
#include "tzbsp_mmu_cache.h"
#include "tzbsp_shared_imem.h"
#include "tzbsp_hwio.h"

#include "qfprom_test.h"
#include "sysdbg_main.h"
#include "kthread.h"
#include "tzbsp_arch.h"
#include "HALqgic.h"

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/
extern void tzbsp_zi_init(void) __attribute__((weak));
extern void tzbsp_secboot_set_shared_info_arch(void *info);
extern void tzbsp_smp_notifier_asm(void);
extern void tzbsp_sec_interrupts_modify_target(uint32 cpu, boolean add);

extern uint64 tzbsp_boot_get_hlos_cold_start_addr(uint32 cpu_ii);
extern uint64 tzbsp_boot_get_hlos_warm_start_addr(uint32 cpu_ii);
extern uint32 tzbsp_boot_get_hlos_ei_class(uint32 cpu_ii);

extern void tzbsp_cpu_init_lock_arch(tzbsp_mutex_t* boot_lock);

extern void oem_config_gpu_dcvs_para(void);
extern boolean tzbsp_oem_allow_sdi_wa(void);

/** ----------------------------------------------------------------------------
Stub out ARM routines included with standard C library
         routines that are not used in TZBSP
* ---------------------------------------------------------------------------*/

void   __rt_raise              (int signal, int type) {}
void   __default_signal_handler(int signal, int type) {}
void   _sys_exit               (int return_code)      {}

/*----------------------------------------------------------------------------
 * Static Variable Definitions
 * -------------------------------------------------------------------------*/

extern uint32 tzbsp_has_cpu_cold_booted[TZBSP_CPU_COUNT];

static uint32 s_enable_dbg = 0;

/* The initial value for stack canary. */
unsigned int __stack_chk_guard[] = {0xDEADDEAD};

#define UNCACHED __attribute__((section("TZBSP_UNCACHED_ZI_BOOT")))

/* Needed to determine if L2 maintenance is needed before collapse. */
uint32 UNCACHED cpus_per_cluster_in_lpm[TZBSP_CLUSTER_COUNT] = {0};

uint32 UNCACHED tzbsp_l2_gdhs_state[2];
el1_pc_notifier_t UNCACHED last_pc[TZBSP_CLUSTER_COUNT] = {0};

/* Mutex for synchronizing global variables shared between boot up and
   power collapse.  This variable must be uncached because of how
   cache maintenance is done during L2 power collapse.  The L2 power
   collapse cleans and invalidates the L2 cache prior to unlocking. It
   seems that the L2 PC sequence does not keep cached variables
   coherent throughout the PC. */
tzbsp_mutex_t UNCACHED tzbsp_boot_lock;

/*----------------------------------------------------------------------------
 * Static Function Declarations and Definitions
 * -------------------------------------------------------------------------*/

/** Defining tzos_entry as weak reference TZOS (QSEE or MobiCore) if present
 * are expected to implement tzos_entry */
extern int tzos_entry(const tzbsp_memblock_t* a0,
                      const tzbsp_memblock_t* a1,
                      uint32                  a2) __attribute__((weak));

/** TODO: This is temporary and should be removed when support for old chip
 *        revisions is dropped */
extern void tzbsp_do_chip_ver_cfg(void) __attribute__((weak)) ;
/** TODO: FIXME_8936 This is temporary and should be done only for 8939/8936
 *        Other option could be dummy definition for all chipset */
extern void tzbsp_init_cluster_cpu_var_arch(void) __attribute__((weak)) ;

extern int tzbsp_pil_init(void);
extern uint32 tzbsp_prng_getdata(uint8*, uint32);
extern int tzbsp_chipset_init(uint32 debug);
extern void tzbsp_read_ddr_size(void);
extern int tzbsp_secure_channel_key_init(void);
extern void tzbsp_memzi(uint32* addr, uint32 size);
extern int qsee_demand_paging_init(void);
extern int qsee_demand_paging_init_key(void);
extern int tzbsp_chipset_reinit(void);
extern void tzbsp_release_hw_lock(void);
extern void tzbsp_wdt_mask_bark(boolean mask);
extern int tzbsp_pc_check_votes_contention(int do_l2_collapse);
extern int tzbsp_check_pend_disable_gicc(void);
extern int tzbsp_pc_vote_against_pc(void);
extern void tzbsp_exec_power_collapse_notifier(void);
extern void tzbsp_pc_perform_l2_collapse_preconditions(void);
extern int tzbsp_secboot_update_rollback_fuse_version();
extern void tzbsp_milestone_set_arch(void);
extern void tzbsp_oem_fuse_config(void);
extern boolean tzbsp_allow_memory_dump(void);
extern void __attribute__((noreturn)) tzbsp_spmi_bus_disable_notifier(uint32 arg0);

extern void tzbsp_power_collapse_notifier_arch(el1_pc_notifier_t);
extern int mink_initialize_modules(void);
void get_hyp_exit_point_details_arch (uint64* e_entry, uint32* ei_class);


#define RETCHK(xx) if (E_SUCCESS != xx) {return xx;}

/*----------------------------------------------------------------------------
 * Function Declarations and Documentation
 * -------------------------------------------------------------------------*/

/* Handle messages from NS in an infinite loop */
static void __attribute__((noreturn)) qsee_ns_msg_handling_loop(void)
{
  tzbsp_regs_t regs;
  int sysCallRetVal = SMC_INFO_GENERIC_TYPE;
  int smc_info = SMC_INVALID_VALUE;
  uint64 addr_64;
  uint32 elf_ei_class;
  uint32 hyp_config_bmsk;

  memset(&regs, 0, sizeof(regs));

  get_hyp_entry_point_details_arch(&addr_64, &elf_ei_class);

  tzbsp_config_hyp_entry(addr_64, elf_ei_class);

  get_hyp_exit_point_details_arch(&addr_64, &elf_ei_class);

  #ifdef FEATURE_NO_XPU
  hyp_config_bmsk = 1;
  #else
  hyp_config_bmsk = tzbsp_allow_memory_dump()? 1: 0;
  #endif

  /* TODO: Until the secure kernel can return > 32 bit arguments, we
           can't support a hypervisor image beyond 4 GB. */
  regs.reg[0] = (uint32) addr_64;
  regs.reg[1] = elf_ei_class;
  regs.reg[2] = hyp_config_bmsk;

  while(1)
  {
    if (SMC_INFO_MODE_SWITCH_TYPE != sysCallRetVal)
    {
      smc_info = tzbsp_smc_return_to_ns(&regs);
    }
    else
    {
      smc_info = tzbsp_smc_return_to_ns_no_regs_modified(&regs);
    }

    switch(smc_info)
    {
      case SMC_EVENT_SYSCALL:
      {
        sysCallRetVal = tzbsp_syscall(&regs);
        break;
      }
      case SMC_EVENT_POWER_COLLAPSE_ENTRY:
      {
        break;
      }
      case SMC_EVENT_POWER_COLLAPSE_EXIT:
      {
        break;
      }
      case SMC_INVALID_VALUE:
      default:
      {
        /* Can intercept NS calls currently when multiple secure contexts are
         * supported
         */
        continue;
      }//case default
    }//switch
  }//while
}//qsee_ns_msg_handling_loop()

static int setup_initial_memory_mappings(void)
{
  int retval;
  size_t temp_sz;
  p_addr_t* paddrs = NULL;
  size_t* lens = NULL;
  p_addr_t mon_code_base;
  uint32 mon_code_len;

  for (paddrs = qsee_mrm_get_code_base_addrs(),
         lens = qsee_mrm_get_code_lens();
       (size_t) TZ_VAS_LAST_ADDRESS != *paddrs;
       lens++, paddrs++)
  {
    /* QSEE executable region not mapped by monitor */
    retval =
      qsee_mrm_add_to_cold_init_map(
        *paddrs, *paddrs,
        B2KB(TZBSP_ROUNDUP(uint32, *lens, MIN_MAP_SIZE)),
        (TZ_READ_ONLY_PL1 | TZ_MEM_WRITE_BACK_WA |
         TZ_INR_SHAREABLE | TZ_EXECUTABLE));

    RETCHK(retval);
  }

  /* Mark the Monitor Code area as read only */
  tzbsp_get_mon_code_info(&mon_code_base, &mon_code_len);
  retval =
    qsee_mrm_add_to_cold_init_map(
      mon_code_base, mon_code_base,
      (size_t)B2KB(TZBSP_ROUNDUP(uint32, mon_code_len, MIN_MAP_SIZE)),
      (TZ_READ_ONLY_PL1 | TZ_MEM_WRITE_BACK_WA |
       TZ_INR_SHAREABLE | TZ_EXECUTABLE));

  RETCHK(retval);

  /* Add the shared code region for user space modules.
     Should be USR mode, read only, executable.  */
  retval =
    qsee_mrm_add_to_cold_init_map(
      qsee_mrm_get_mink_shared_usr_code_base(), qsee_mrm_get_mink_shared_usr_code_base(),
      B2KB(TZBSP_ROUNDUP(uint32, qsee_mrm_get_mink_shared_usr_code_len(), MIN_MAP_SIZE)),
      (TZ_READ_ONLY_PL0 | TZ_MEM_WRITE_BACK_WA |
       TZ_INR_SHAREABLE | TZ_EXECUTABLE));

  RETCHK(retval);

  return tzbsp_mmu_map_block(qsee_mrm_get_cold_init_map(),
                             qsee_mrm_get_cold_init_map_len());
}

static void tzbsp_wfi_fell_through(void)
{
  volatile uint32 *l1_validity = (uint32 *) TZBSP_SHARED_IMEM_L1_VALIDITY;
  volatile uint32 *l2_validity = (uint32 *) TZBSP_SHARED_IMEM_L2_VALIDITY;
  uint32 cpu_ii = tzbsp_cur_cpu_num();
  uint32 cluster_ii = tzbsp_get_cluster_num();

  tzbsp_diag_inc_term_exit();

  tzbsp_wdt_mask_bark(FALSE);
  tzbsp_pc_vote_against_pc();

  cpus_per_cluster_in_lpm[cluster_ii]--;
  tzbsp_l2_gdhs_state[cluster_ii] = 0;

  tzbsp_sec_interrupts_modify_target(cpu_ii, TRUE);

  /* mark L1 as valid for SDI */
  *l1_validity |= 1 << cpu_ii;

  /* Mark L2 for this cluster as valid for SDI */
  *l2_validity |= 1 << cluster_ii;

  /* Track the last PC status for this cluster. */
  last_pc[cluster_ii] = PC_FALLTHROUGH | (cpu_ii << 16);
}

/**
   @param state Fallthrough or CPU_PC
   @param arg   arg[0]   L2 PC
                arg[1]   GDHS PC
                arg[2-3] Reserved
                arg[4-7] Do not wake HLOS up on this core
   @returns Fallthrough, CPU_PC or CPU_and_L2_PC
 */
el1_pc_notifier_t tzbsp_power_collapse_notifier(el1_pc_notifier_t state,
                                                uint32 arg)
{
  volatile uint32 *l1_validity = (uint32 *) TZBSP_SHARED_IMEM_L1_VALIDITY;
  volatile uint32 *l2_validity = (uint32 *) TZBSP_SHARED_IMEM_L2_VALIDITY;
  uint32 cluster_ii = tzbsp_get_cluster_num();
  uint32 cpu_ii = tzbsp_cur_cpu_num();

  tzbsp_diag_inc_term_entry();

  /* Take boot lock */
  tzbsp_mutex_lock(&tzbsp_boot_lock);

  if (PC_FALLTHROUGH == state)
  {
    tzbsp_wfi_fell_through();

    /* Unblock boot-flow of the other CPUs */
    tzbsp_mutex_unlock(&tzbsp_boot_lock);
  }
  else
  {
    /* Treat all unhandled state arguments as though they are
       CPU_PC.  This makes error handling simpler. */
    state = CPU_PC;

    /* Release HLOS lock */
    tzbsp_release_hw_lock();

    /* Disable secure WDT for this core.  This is needed in addition
       to disabling all the interrupts in the GIC controller below. */
    tzbsp_wdt_mask_bark(TRUE);

    /* Increment the number of CPUs in low power mode. */
    cpus_per_cluster_in_lpm[cluster_ii]++;

    /* ASSERT if there is an issue with the boot lock */
    if (cpus_per_cluster_in_lpm[cluster_ii] >
        tzbsp_get_num_cpus_in_cluster(cluster_ii))
    {
      while (1);
    }

    /* mark L1 as invalid for SDI */
    *l1_validity &= (uint32)~(1 << cpu_ii);

    /* Bit 0 indicates L2 PC.  Check to make sure bit 1 is not set. */
    if ((arg & 0x3) == 1)
    {
      /* Only allow L2 PC if the current CPU is the last CPU in the
         cluster to go to sleep. */
      if (cpus_per_cluster_in_lpm[cluster_ii] ==
          tzbsp_get_num_cpus_in_cluster(cluster_ii))
      {
        state = CPU_AND_L2_PC;

        /* Mark L2 for this cluster as invalid for SDI */
        *l2_validity &= (uint32)~(1 << cluster_ii);
      }
    }
    /*bit 0 and 1 need to be high for L2 GDHS*/
    else if ((arg & L2_GDHS_BMSK) == L2_GDHS_BMSK)
    {
      /* Set GDHS flag for curret cluster if the last CPU going down in the cluster */
       if (cpus_per_cluster_in_lpm[cluster_ii] == tzbsp_get_num_cpus_in_cluster(cluster_ii))
       {
         state = CPU_AND_L2_GDHS;
         tzbsp_l2_gdhs_state[cluster_ii] = 1;
       }
    }

    /* Any bits set in bits 4-7 indicates a request to make the
       current core NON interruptible. */
    if ((arg & 0xF0) != 0)
    {
      tzbsp_sec_interrupts_modify_target(cpu_ii, FALSE);
    }

    /* Check for any contending PC votes */
    if (E_SUCCESS != tzbsp_pc_check_votes_contention(
                     (CPU_AND_L2_PC == state) ||
                     (CPU_AND_L2_GDHS == state)))
    {
      tzbsp_wfi_fell_through();

      /* Unblock boot-flow of the other CPUs */
      tzbsp_mutex_unlock(&tzbsp_boot_lock);

      return PC_FALLTHROUGH;
    }

    /* Check if there are currently any pending interrupts.  If not,
       disable interrupts going to the CPU from the GIC distributer.
       This eliminates a race condition between starting the SPM
       sequence when the CPU executes the WFI instruction and an
       interrupt coming into the CPU, causing the WFI to fall-through
       before the SPM sequence completes.  The side-effect is that the
       WFI instruction will never fall through and interrupts coming
       in during that time must wait to wake up the CPU until after
       the SPM sequence is complete. */
    if (E_SUCCESS != tzbsp_check_pend_disable_gicc())
    {
      tzbsp_wfi_fell_through();

      /* Unblock boot-flow of the other CPUs */
      tzbsp_mutex_unlock(&tzbsp_boot_lock);

      return PC_FALLTHROUGH;
    }

    /* TODO: need to inform TPOS vendors that QSEE will do all cache
       maintenance. *Sigh* */
    if (CPU_AND_L2_PC == state)
    {
      tzbsp_exec_power_collapse_notifier();
    }

    last_pc[cluster_ii] = state | (cpu_ii << 16);

    /* Remaining sequence is chip and processor dependent. */
    tzbsp_power_collapse_notifier_arch(state);

    /* Unblock boot-flow of the other CPUs */
    tzbsp_mutex_unlock(&tzbsp_boot_lock);

    __asm__ ("wfi" : : :);
  }

  return state;
}

extern void JustLoop(void) __attribute__((weak));

/**
 * Cold boot flow init. This function is called when TZBSP is initialized for
 * the first time.
 */
void __attribute__((noreturn)) tzbsp_cold_init (void *info)
{
  volatile int err = E_SUCCESS;
  uint32 ii;

  JustLoop();
  /* This should be called first in case pointers/memory locations are
   * dependant on the chip version */
  tzbsp_do_chip_ver_cfg();
  /* Initialize Cluster & Cpu count variables Do it ASAP before MMU Enable &
   * before any usage of Cluster or Cpu count/index variable */
  tzbsp_init_cluster_cpu_var_arch();

  /* Cold boot for this CPU is done */
  tzbsp_has_cpu_cold_booted[tzbsp_cur_cpu_num()] = TRUE;

  /* Initialize the Diag region.  This must be early to enable debug
     messages */
  tzbsp_diag_init();

  /* Adds DDR regions to default memory map and maps them */
  err = tzbsp_ddr_init();
  if (err)
  {
    TZBSP_ERROR_CODE(INIT_DDR_INIT_FAILED);
    while(1);
  }

  //todo-sg
  // err = qsee_demand_paging_init();
  // if (err)
  // {
    // TZBSP_ERROR_CODE(INIT_DEMAND_PAGE_FAILED);
    // tzbsp_loop_here();
  // }

  /* Must come after DDR init */
  /* TODO: QSEE RO region, QSEE RW region */
  err = setup_initial_memory_mappings();
  if (err)
  {
    /* TODO: new error return */
    /* TZBSP_ERROR_CODE(); */
    while(1);
  }

  KThread_init(NULL, NULL);

  /* May only enable MMU after all default regions are
     established. */
  cold_init_and_enable_el1_mmu((qsee_mrm_get_el1_tt_base_addr() |
                                get_ttbr_mem_attrs()),
                               get_el1_nmrr(), get_el1_prrr());

  /* Initialize cpus_per_cluster when the first CPU comes up */
  for (ii = 0; ii < TZBSP_CLUSTER_COUNT; ii++)
  {
    cpus_per_cluster_in_lpm[ii] = tzbsp_get_num_cpus_in_cluster(ii);

    /* The current cluster has one CPU (this CPU) online */
    if (tzbsp_get_cluster_num() == ii)
    {
      cpus_per_cluster_in_lpm[ii]--;
    }
  }

  //Verify if info passed is correct, if not loop
  tzbsp_verify_sbl_info_arch(info);

  tzbsp_secboot_set_shared_info_arch(info);

  /* Unmap any dynamically mapped peripherals */
  err = tzbsp_mem_unmap_all_devices();
  if (err)
  {
    TZBSP_ERROR_CODE(INIT_DEVICE_UNMAP_FAILED);
    while(1);
  }

  /* Making this call during cold boot marks this CPU as
     interruptible, if/when interrupts are registered.  By default,
     each CPU should be interruptible when it cold-boots. */
  tzbsp_sec_interrupts_modify_target(tzbsp_cur_cpu_num(), TRUE);

  s_enable_dbg = tzbsp_is_debug_enabled();

  err = tzbsp_configure_hw(s_enable_dbg);
  if (err)
  {
    TZBSP_ERROR_CODE(INIT_HW_INIT_FAILED);
    while(1);
  }

  tzbsp_heap_init();

  err = mink_initialize_modules();
  if (err)
  {
    TZBSP_ERROR_CODE(INIT_BOOT_TAMPER_CHECK_FAILED);
    while(1);
  }

  //Do Not move position of tzbsp_chipset_init()
  //It has code which is order dependent, like disabling TCM
  err = tzbsp_chipset_init(s_enable_dbg);
  if(err)
  {
    TZBSP_ERROR_CODE(INIT_CHIPSET_INIT_FAILED);
    while(1);
  }

  /* init OEM specific configuration for GPU DCVS */
  oem_config_gpu_dcvs_para();

  /* update anti-rollback fuse for sbl1-loaded images */
  err = tzbsp_secboot_update_rollback_fuse_version();
  if(err)
  {
    TZBSP_ERROR_CODE(INIT_HW_INIT_FAILED);
    while(1);
  }

  tzbsp_oem_fuse_config();

  system_debug_main();

//todo-sg:Remove flag Virtio flag once prgn in.
#ifndef TZBSP_VIRTIO
#ifndef FIXME_8916

  /* Populate the stack protection canary with random data. */
  tzbsp_prng_getdata((uint8*)__stack_chk_guard, 4);

  /* Initialize demand paging hash key for faster operation */
  err = qsee_demand_paging_init_key();
  if (err)
  {
    TZBSP_ERROR_CODE(INIT_DEMAND_PAGE_FAILED);
    while(1);
    }
#endif //FIXME_8916
#endif
  err = tzbsp_pil_init();
  if (err)
  {
    TZBSP_ERROR_CODE(INIT_PIL_INIT_FAILED);
    while(1);
  }

  /* Bear target need to finish milestone call here.
     No separate milestone syscall for Bear Target */
  tzbsp_milestone_set_arch();

  /* Initializes TZOS (LK or MobiCore)*/
  err = tzbsp_reset_exec((uint32) tzos_entry);
  if (err)
  {
    TZBSP_ERROR_CODE(INIT_RESET_EXEC_FAILED);
    while(1);
  }

#ifdef TZBSP_WITH_TEST_SVC
  /*Call to qfprom test framework*/
  boot_qfprom_test();
#endif // TZBSP_WITH_TEST_SVC

  /* Register a notification function to call when a call that requires
     symmetric multiprocessing comes into the monitor. (e.g. power collapse) */
  mon_register_el1_symmetric_multiprocessing_notifier((v_addr_t) tzbsp_smp_notifier_asm);

  /* Share the address of the boot lock with EL3. */
  mon_set_boot_lock_addr((v_addr_t) &tzbsp_boot_lock);

  /* Enable interrupts. IRQs are not enabled at this point, we will first
   * have to do a function style return to boot loaders. IRQs on secure side
   * are enabled when the first system call is executed. */
  tzbsp_int_enable(TZBSP_EA_BIT | TZBSP_FIQ_BIT);

  /* Drop into the message handling loop and never return */
  qsee_ns_msg_handling_loop();
}//tzbsp_cold_init()

/**
 * CPU init, called when cold booting secondary CPU(s) and warm booting any of
 * the CPUs. When this function is called TZBSP has been initialized once
 * already (@see tzbsp_cold_init).
 */
void tzbsp_cpu_init (uint64* hlos_addr, uint64* hyp_addr,
                     uint32* hlos_ei_class, uint32* hyp_ei_class)
{
  int err = E_SUCCESS;
  uint32 cpu = tzbsp_cur_cpu_num();
  uint32 cluster_ii = tzbsp_get_cluster_num();
  volatile uint32 *l1_validity = (uint32 *) TZBSP_SHARED_IMEM_L1_VALIDITY;
  volatile uint32 *l2_validity = (uint32 *) TZBSP_SHARED_IMEM_L2_VALIDITY;

  tzbsp_diag_inc_warm_entry();

  do
  {
    if (cpu >= TZBSP_CPU_COUNT)
    {
      err = -E_NOT_SUPPORTED;
      break;
    }

    /* The jump address is a physical memory address given by the
       HLOS. Read jump address before the MMU has been enabled to
       ensure that the instruction that exists at the address location
       can be accessed for debug purposes.
    */
    if (tzbsp_has_cpu_cold_booted[cpu])
    {
      *hlos_addr = tzbsp_boot_get_hlos_warm_start_addr(cpu);
    }
    else
    {
      tzbsp_has_cpu_cold_booted[cpu] = TRUE;
      *hlos_addr = tzbsp_boot_get_hlos_cold_start_addr(cpu);
    }

    /* Call into the monitor to enable MMU */
    warm_init_and_enable_el1_mmu(qsee_mrm_get_el1_tt_base_addr(),
                                 get_el1_nmrr(), get_el1_prrr());

    /* Block boot-flow for other CPUs while manipulating shared
       resources.  Shared resources with known contentions:
       * Global variables for CPUs in LPM, L1 and L2 state
       * QGIC target registers
       * Clock voting inside tzbsp_configure_hw
       * PC votes inside tzbsp_chipset_reinit
       * The executive warmboot notifier is a known unknown
     */
    tzbsp_cpu_init_lock_arch(&tzbsp_boot_lock);

    TZBSP_DIAG_LOG_WAKEUP_INFO((HWIO_IN(APCS_GICC_HPPIR)),\
          (HWIO_IN(APCS_GICC_AHPPIR)));

    if (tzbsp_oem_allow_sdi_wa())
    {
        /* Assert if L2 SPM is enable when this is NOT a 1st CPU wake up in the Cluster */
        /* Debug Code to catch if HLOS SPM Config miss behave */
        if(0xF000 == (tzbsp_boot_lock.lock & 0xF000))
        {
            if (cpus_per_cluster_in_lpm[cluster_ii] !=      tzbsp_get_num_cpus_in_cluster(cluster_ii))
            {
                TZBSP_LOG_ERR("L2 SPM is BAD for CPU %x, No of CPU On in CL %x", cpu, cpus_per_cluster_in_lpm[cluster_ii]);
                err = -E_NOT_SUPPORTED;
                break;
            }
         }
     }

    /* Update PC globals */
    cpus_per_cluster_in_lpm[tzbsp_get_cluster_num()]--;

    tzbsp_l2_gdhs_state[tzbsp_get_cluster_num()] = 0;

    /* mark L1 as valid for SDI */
    *l1_validity |= 1 << cpu;

    /* Mark L2 for this cluster as valid for SDI */
    *l2_validity |= 1 << tzbsp_get_cluster_num();

    last_pc[tzbsp_get_cluster_num()] = (el1_pc_notifier_t) 3 | (cpu << 16);

    /* Enable all secure interrupts for this CPU and mark this CPU as
       interruptible.
       TODO: This does not make sense with interrupt registration API
       that allows for registering interrupts with a CPU affinity. */
    tzbsp_sec_interrupts_modify_target(cpu, TRUE);

    if (tzbsp_has_cpu_cold_booted[cpu])
    {
      tzbsp_exec_warmboot_notifier();
    }

    err = tzbsp_configure_hw(s_enable_dbg);
    if (err)
    {
      TZBSP_ERROR_CODE(INIT_HW_INIT_FAILED);
      break;
    }

    err = tzbsp_chipset_reinit();
    if (err)
    {
      TZBSP_ERROR_CODE(INIT_CHIPSET_REINIT_FAILED);
      break;
    }

    /* Unblock boot-flow of the other CPUs */
    tzbsp_mutex_unlock(&tzbsp_boot_lock);

    tzbsp_int_enable(TZBSP_EA_BIT | TZBSP_FIQ_BIT);

  } while(0);

  if(err)
  {
    tzbsp_loop_here();
  }

  /* Log the both the jump address and the instruction at the jump address to
   * detect DDR memory problems. */
  if (*hlos_addr)
  {
    TZBSP_DIAG_WARM_JMP_ADDR((uint32) *hlos_addr);
    TZBSP_DIAG_WARM_JMP_INSTR(*((uint32*) *hlos_addr));
  }

  *hlos_ei_class = tzbsp_boot_get_hlos_ei_class(cpu);
  get_hyp_entry_point_details_arch(hyp_addr, hyp_ei_class);

  tzbsp_diag_inc_warm_exit();
}

uint32 tzbsp_get_num_cpus_in_lpm(void)
{
  uint32 ii, cnt;

  for (ii = 0, cnt = 0; ii < TZBSP_CLUSTER_COUNT; ii++)
  {
    cnt += cpus_per_cluster_in_lpm[ii];
  }

  return cnt;
}

el1_smp_notifier_t tzbsp_smp_notifier(el1_smp_notifier_cmd_t cmd, uint32 arg0, uint32 arg1)
{
  el1_smp_notifier_t el1_notification = {0};

  switch(cmd)
  {
    case SMP_NOTIFIER_CMD_PC:
    {
      el1_notification.el1_pc_notifier = tzbsp_power_collapse_notifier(arg0, arg1);
      break;
    }
    case SMP_NOTIFIER_CMD_SPMI:
    {
      /* Does not return */
      /* el1_notification.el1_spmi_notifier = */ tzbsp_spmi_bus_disable_notifier(arg0);
      break;
    }
    default:
    {
      break;
    }
  }

  return el1_notification;
}
