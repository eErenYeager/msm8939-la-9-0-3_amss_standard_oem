    EXPORT arm_switch_to
    EXPORT arm_context_switch
    EXPORT arm_context_switch_to_usr
    EXPORT arm_get_cpsr
    EXPORT arm_vfp_context_save
    EXPORT arm_vfp_context_restore
    EXPORT arm_vfp_disable
    EXPORT arm_context_switch_async
    AREA contex_switch_asm, CODE, ALIGN=2
    CODE32
arm_switch_to
    mov sp, r0
    mov r0, #0x0
    ldmdb sp, {r0 - r14}^
    rfeia sp
arm_get_cpsr
    mrs r0, cpsr
    bx r14
; ------------------------------------------------------------------
; Save the VFP context of the old thread. Always disable VFP so that
; first VFP causes the undefined exception. This is needed as we are
; no more disabling in the monitor
; Note: This function temporarily enables the VFP to store the context
; ------------------------------------------------------------------
arm_vfp_context_save
    ; R0-R3 are scratch regs, saved by caller
    cmp r0, #0
    beq arm_vfp_context_save_end
    FSTMIAD r0!, {D0-D15}
    FSTMIAD r0!, {D16-D31}
    vmrs r1, fpscr
    str r1, [r0]
arm_vfp_context_save_end
    bx lr
arm_vfp_context_restore
    ; R0-R3 are scratch regs, saved by caller
    cmp r0, #0
    beq arm_vfp_context_restore_end
    FLDMIAD r0!, {D0-D15}
    FLDMIAD r0!, {D16-D31}
    ldr r1, [r0]
    vmsr fpscr, r1
arm_vfp_context_restore_end
    bx lr
arm_vfp_disable
    ; R0-R3 are scratch regs, saved by caller
    ; enable trapping to EL1
    mrc p15,0,r0,c1,c0,2 ; Read CPACR into Rt
    mov32 r1, #0xff5fffff
    and r0, r1
    mcr p15,0,r0,c1,c0,2 ; Write Rt to CPACR
    isb SY
    bx lr
arm_context_switch_to_usr
    sub r3, sp, #(11*4)
    mov r12, r14
    stmia r3, { r4-r11, r12, r13, r14 }^
    str r3, [r0]
    mov sp, r1
    mov r0, #0x0
    ldmdb sp, {r0 - r14}^
    rfeia sp
arm_context_switch_async
    sub r3, sp, #(11*4)
    mov r12, lr
    stmia r3, { r4-r11, r12, r13, r14 }^
    str r3, [r0]
    mov r13, r1
    blx r2
arm_context_switch
    sub r3, sp, #(11*4)
    mov r12, lr
    stmia r3, { r4-r11, r12, r13, r14 }^
    str r3, [r0]
    ldmia r1, { r4-r11, r12, r13, r14 }^
    mov lr, r12
    add sp, r1, #(11*4)
    bx lr
    END
