/*
 * Copyright (c) 2008 Travis Geiselbrecht
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef __ARM_ARCH_THREAD_H
#define __ARM_ARCH_THREAD_H

#define CPSR_I_MASK  0x80
#define CPSR_F_MASK  0x40

struct arch_thread {
  unsigned int regs[16];
  unsigned int cpsr;
  unsigned int sp;
};

// data type for the VFP context in TCB
struct arch_vfp_thread {
  unsigned int vfp_regs[64];
  unsigned int fpscr;
};

// To Save the vfp context from the kernel
void arm_vfp_context_save(struct arch_vfp_thread *arch_vfp);
// To restore the vfp context from the kernel
void arm_vfp_context_restore(struct arch_vfp_thread *arch_vfp);
// To read the current program and control register from kernel
void arm_read_CPACR(unsigned int reg);
// To disable VFP at EL1 from kernel
void arm_vfp_disable(void);

#endif

