/*
@file tzbsp_boot.c
@brief Warm and cold boot SYSCALL support

*/
/*===========================================================================
   Copyright (c) 2010 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsee/mink/services/boot/src/tzbsp_boot.c#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $


when       who      what, where, why
--------   ---      ------------------------------------
05/17/10   tk       Initial version.

===========================================================================*/


/*===========================================================================

          INCLUDE FILES

============================================================================*/
#include "tzbsp.h"
#include "tzbsp_errno.h"
#include "tzbsp_log.h"
#include "tzbsp_mem.h"
#include "tzbsp_syscall.h"
#include "tzbsp_chipset.h"
#include "tzbsp_target.h"
#include "tzbsp_xpu.h"
#include "tzbsp_mmu_cache.h"
#include "tzbsp_image_version.h"
#include "ClockTZ.h"
#include "tz_mc.h"
#include "tzbsp_fuseprov.h"
#include "qsee_mrm_arch.h"
#include "tz_mc.h"
#include "smem.h"


/** CPU1 initial boot. */
#define TZ_BOOT_ADDR_CPU1_COLD             0x00000001
/** CPU1 subsequent boots/resets. */
#define TZ_BOOT_ADDR_CPU1_WARM             0x00000002
/** CPU0 subsequent boots/resets. */
#define TZ_BOOT_ADDR_CPU0_WARM             0x00000004
/** CPU2 initial boot. */
#define TZ_BOOT_ADDR_CPU2_COLD             0x00000008
/** CPU2 subsequent boots/resets. */
#define TZ_BOOT_ADDR_CPU2_WARM             0x00000010
/** CPU3 initial boot. */
#define TZ_BOOT_ADDR_CPU3_COLD             0x00000020
/** CPU3 subsequent boots/resets. */
#define TZ_BOOT_ADDR_CPU3_WARM             0x00000040

#define WEAK __attribute__((weak))

extern int qsee_demand_paging_post_milestone_init(void);
extern boolean tzbsp_allow_memory_dump(void);
extern int affl_to_cpu_num(uint32 al0, uint32 al1, uint32 al2, uint32 al3);
extern void WEAK qsee_chipset_post_milestone_init(void);

/** HLOS start addresses. */
uint32 tzbsp_cpu_hlos_cold_start_addr[TZBSP_CPU_COUNT] = {0};
uint32 tzbsp_cpu_hlos_warm_start_addr[TZBSP_CPU_COUNT] = {0};
uint32 tzbsp_cpu_hlos_in_aarch64[TZBSP_CPU_COUNT] = {0};

boolean tzbsp_boot_milestone_status = FALSE;
boolean tzbsp_mode_switch_status = FALSE;

static void tzbsp_set_boot_addr_loop(uint32 boot_addr,
                                     const uint32 affl0_bmsk,
                                     const uint32 affl1_bmsk,
                                     const uint32 affl2_bmsk,
                                     const uint32 affl3_bmsk,
                                     uint32 flags)

{
  uint32 a0_ii, a1_ii, a2_ii, a3_ii;
  uint32 a0_bm, a1_bm, a2_bm, a3_bm;

  for (a3_ii = 0, a3_bm = affl3_bmsk; a3_bm; a3_bm &= ~(1 << a3_ii++))
  {
    if (a3_bm & (1 << a3_ii))
    {
      for (a2_ii = 0, a2_bm = affl2_bmsk; a2_bm; a2_bm &= ~(1 << a2_ii++))
      {
        if (a2_bm & (1 << a2_ii))
        {
          for (a1_ii = 0, a1_bm = affl1_bmsk; a1_bm; a1_bm &= ~(1 << a1_ii++))
          {
            if (a1_bm & (1 << a1_ii))
            {
              for (a0_ii = 0, a0_bm = affl0_bmsk;
                   a0_bm;
                   a0_bm &= ~(1 << a0_ii++))
              {
                if (a0_bm & (1 << a0_ii))
                {
                  int cpu_ii = affl_to_cpu_num(a0_ii, a1_ii, a2_ii, a3_ii);

                  if (cpu_ii >= 0 && cpu_ii < TZBSP_CPU_COUNT)
                  {
                    if (flags & TZ_BOOT_ADDR_COLD)
                    {
                      tzbsp_cpu_hlos_cold_start_addr[cpu_ii] = boot_addr;
                    }
                    if (flags & TZ_BOOT_ADDR_WARM)
                    {
                      tzbsp_cpu_hlos_warm_start_addr[cpu_ii] = boot_addr;
                    }
                    if (flags & TZ_BOOT_ADDR_AARCH64)
                    {
                      tzbsp_cpu_hlos_in_aarch64[cpu_ii] = TRUE;
                    }
                  }
                } /* affl0 check */
              } /* affl0 loop */
            } /* affl1 check */
          } /* affl1 loop */
        } /* affl2 check */
      } /* affl2 loop */
    } /* affl3 check */
  } /* affl3 loop */
}


/* Subroutine for handling the HLOS start address request.
 *
 * @param req Pointer to the request structure.
 *
 * @return \c E_SUCCESS on success, an error code otherwise.
 */
int tzbsp_set_boot_addr(uint32 boot_addr, uint32 affl0_bmsk, uint32 affl1_bmsk,
                        uint32 affl2_bmsk, uint32 affl3_bmsk, uint32 flags)
{
  /* TODO: is_ns_range needs to be able to take 64 bit address
     eventually... */
  /* HLOS start address must be non-secure address space. */
  if (!tzbsp_is_ns_range((void*) boot_addr, sizeof(uint32)))
  {
    return -E_BAD_ADDRESS;
  }

  /* One of these flags must be set */
  if (!(flags & TZ_BOOT_ADDR_COLD) && !(flags & TZ_BOOT_ADDR_WARM))
  {
    return -E_FAILURE;
  }

  /* Something must be set in each of the bitmasks, otherwise the loop
     makes no sense. */
  if (!affl0_bmsk || !affl1_bmsk || !affl2_bmsk || !affl3_bmsk)
  {
    return -E_FAILURE;
  }

  /* Loop through each affl bitmask and save the relevant info. */
  tzbsp_set_boot_addr_loop(boot_addr, affl0_bmsk, affl1_bmsk, affl2_bmsk,
                           affl3_bmsk, flags);

  /* Flush the cold start and warm start structures to ensure they are
   * available with the MMU off */
  tzbsp_dcache_flush_region(tzbsp_cpu_hlos_cold_start_addr,
                            sizeof(tzbsp_cpu_hlos_cold_start_addr));
  tzbsp_dcache_flush_region(tzbsp_cpu_hlos_warm_start_addr,
                            sizeof(tzbsp_cpu_hlos_warm_start_addr));
  tzbsp_dcache_flush_region(tzbsp_cpu_hlos_in_aarch64,
                            sizeof(tzbsp_cpu_hlos_in_aarch64));

  return E_SUCCESS;
}

/* Subroutine for handling the HLOS start address request.
 *
 * @param req Pointer to the request structure.
 *
 * @return \c E_SUCCESS on success, an error code otherwise.
 */
int tzbsp_set_boot_addr_old(int flags, uint32 addr)
{
  /* HLOS start address must be non-secure address space. */
  if(!tzbsp_is_ns_range((void*)addr, sizeof(uint32)))
  {
    return -E_BAD_ADDRESS;
  }

  if (flags & TZ_BOOT_ADDR_CPU0_WARM)
  {
    tzbsp_cpu_hlos_warm_start_addr[0] = addr;
    tzbsp_cpu_hlos_in_aarch64[0] = TRUE;
  }

#if (TZBSP_CPU_COUNT > 1)
  if (flags & TZ_BOOT_ADDR_CPU1_COLD)
  {
    tzbsp_cpu_hlos_cold_start_addr[1] = addr;
    tzbsp_cpu_hlos_in_aarch64[1] = TRUE;
  }

  if (flags & TZ_BOOT_ADDR_CPU1_WARM)
  {
    tzbsp_cpu_hlos_warm_start_addr[1] = addr;
    tzbsp_cpu_hlos_in_aarch64[1] = TRUE;
  }
#endif

#if (TZBSP_CPU_COUNT > 2)
  if (flags & TZ_BOOT_ADDR_CPU2_COLD)
  {
    tzbsp_cpu_hlos_cold_start_addr[2] = addr;
    tzbsp_cpu_hlos_in_aarch64[2] = TRUE;
  }

  if (flags & TZ_BOOT_ADDR_CPU2_WARM)
  {
    tzbsp_cpu_hlos_warm_start_addr[2] = addr;
    tzbsp_cpu_hlos_in_aarch64[2] = TRUE;
  }
#endif

#if (TZBSP_CPU_COUNT > 3)
  if (flags & TZ_BOOT_ADDR_CPU3_COLD)
  {
    tzbsp_cpu_hlos_cold_start_addr[3] = addr;
    tzbsp_cpu_hlos_in_aarch64[3] = TRUE;
  }

  if (flags & TZ_BOOT_ADDR_CPU3_WARM)
  {
    tzbsp_cpu_hlos_warm_start_addr[3] = addr;
    tzbsp_cpu_hlos_in_aarch64[3] = TRUE;
  }
#endif

  /* Flush the cold start and warm start structures to ensure they are
   * available with the MMU off */
  tzbsp_dcache_flush_region(tzbsp_cpu_hlos_cold_start_addr,
                            sizeof(tzbsp_cpu_hlos_cold_start_addr));
  tzbsp_dcache_flush_region(tzbsp_cpu_hlos_warm_start_addr,
                            sizeof(tzbsp_cpu_hlos_warm_start_addr));
  tzbsp_dcache_flush_region(tzbsp_cpu_hlos_in_aarch64,
                            sizeof(tzbsp_cpu_hlos_in_aarch64));
  return E_SUCCESS;
}

int tzbsp_config_hyp_entry(uint64 e_entry, uint32 ei_class)
{
  //todo-sg: fix tzbsp_is_ns_range() to take 64bit address
  if (!tzbsp_is_ns_range((void*)((uint32)e_entry), sizeof(uint32)))
  {
    while(1); /*if just return error code, hypervisor entry isn't initialized*/
  }

  mon_config_hyp_entry(e_entry, ei_class);

  return E_SUCCESS;
}//tzbsp_config_hyp_entry()

/* Subroutine used by external bootlaoders to indicate end
 * of bootloading stage (begin HLOS). This call will handle
 * disabling the non-secure MMU and setting a new non-secure
 * return address
 *
 * @param jump_addr Address to return to after completing the Milestone call
 *
 * @return \c E_SUCCESS on success, an error code otherwise.
 */
int tzbsp_milestone_set(uint32 e_entry, uint32 mode, uint32 ei_class)
{

  /* TODO: fix tzbsp_is_ns_range() to take 64bit address */
  if (!tzbsp_is_ns_range((void*) e_entry, sizeof(e_entry)))
  {
    return -E_BAD_ADDRESS;
  }

  if (tzbsp_boot_milestone_status)
  {
    return -E_ALREADY_DONE;
  }

  tzbsp_boot_milestone_status = TRUE;

  /* allow the fuse provisioning service to check for sec.dat and blow fuses if needed */
  /* device will reset if any fuse is blown, in order for fuse to take affect */
  tzbsp_blow_fuses_and_reset();

  /* TODO: fix for 8994.  Causes crash in virtio. */
  /* populate boot image version in the SMEM_IMAGE_VERSION_TABLE */
#ifndef TZBSP_VIRTIO
  if (E_SUCCESS != tzbsp_image_version_populate_version(TZBSP_IMAGE_INDEX_TZ))
  {
    /* Log the error, but continue on */
    TZBSP_ERROR_CODE(IMAGE_VERSION_POPULATE_FAILED);
  }
#endif

  /* monitor milestone API takes a 64 bit entry address, like it
     should. */
  mon_handle_milestone(e_entry, 0, ei_class);

  if(E_SUCCESS != qsee_demand_paging_post_milestone_init())
  {
    tzbsp_loop_here();
  }

  /* In dload mode, don't config smem prtns since static config is disabled */
  /* If device is in dload mode at this point, then customer is having dump feature in LK rather than SBL1*/
  if(FALSE == tzbsp_allow_memory_dump())
  {
    smem_tz_set_partitions();
  }

  /* In case HLOS boots in DLOAD mode, skip configuring XPUs here as
   * well (device security allowing) */

  if(FALSE == tzbsp_allow_memory_dump())
  {
    if(E_SUCCESS != tzbsp_xpu_config_post_milestone(mode))
    {
      tzbsp_loop_here();
    }
  }

  qsee_chipset_post_milestone_init();

#ifdef TZBSP_VIRTIO
  Clock_EnableProcessor(CLOCK_PROCESSOR_RPM);
#endif

  /* set sbl region to 0*/
  memset((void *)qsee_mrm_get_sbl_base_addr_arch(), 0, qsee_mrm_get_sbl_base_len_arch());

  return E_SUCCESS;
}

int tzbsp_mode_switch(hlos_boot_params_t* hlos_boot_info, uint32 hlos_boot_info_sz)
{
  if (hlos_boot_info_sz != sizeof(hlos_boot_params_t))
  {
    return -E_INVALID_ARG;
  }

  //todo: fix tzbsp_is_ns_range() to take 64bit address
  if (!tzbsp_is_ns_range((void*)hlos_boot_info, sizeof(hlos_boot_params_t)))
  {
    return -E_BAD_ADDRESS;
  }

  if (tzbsp_mode_switch_status)
  {
    return -E_ALREADY_DONE;
  }

  tzbsp_mode_switch_status = TRUE;

  mon_handle_mode_switch(hlos_boot_info);

  return E_SUCCESS;
}


uint64 tzbsp_boot_get_hlos_cold_start_addr(uint32 cpu_ii)
{
  return tzbsp_cpu_hlos_cold_start_addr[cpu_ii];
}

uint64 tzbsp_boot_get_hlos_warm_start_addr(uint32 cpu_ii)
{
  return tzbsp_cpu_hlos_warm_start_addr[cpu_ii];
}

uint32 tzbsp_boot_get_hlos_ei_class(uint32 cpu_ii)
{
  /* TODO: use #define for EI_ELF64 and EI_ELF32 */
  return (tzbsp_cpu_hlos_in_aarch64[cpu_ii] == TRUE) ? 2 : 1;
}
