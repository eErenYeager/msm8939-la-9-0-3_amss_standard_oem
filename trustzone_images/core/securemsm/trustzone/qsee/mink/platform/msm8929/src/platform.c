#include <stdlib.h>
#include <string.h>
#include "lktypes.h"
#include "lkcompiler.h"
#include "lkplatform.h"
#include "lkthread.h"
#include "lktimer.h"
#include "lkdpc.h"
#include "tzbsp.h"
#include "tzbsp_mem.h"
#include "tzbsp_shared_imem.h"
#include "tzbsp_diag.h"
#include "tzbsp_target.h"
#include "crypto/tzbsp_crypto.h"
#include "apps.h"
#include "smc.h"
#include "tzos_exec.h"
#include "tzos_legacy.h"
#include "mink_threads.h"
#include "sd_mgr_rpmb.h"
#include "tzbsp_kdf.h"
#include "external.h"
#include "tzbsp_rollback_version.h"
#include "tzbsp_cp.h"
#include "tzbsp_mpu.h"
#include "mink_services.h"
#include "mink_object.h"
#include "memheap_lite.h"
#include "mink_scm_errno.h"
#include "kthread.h"
#include "tzbsp_jtag.h"
#include "platform.h"
#include "crypto/tzbsp_hash.h"
#include "tzbsp_hwio.h"
#include "HALhwio.h"


extern size_t  memscpy(
          void        *dst,
          size_t      dst_size,
          const void  *src,
          size_t      src_size
          );




/*The main mink thread is busy, check if we can service this syscall on the secondary thread*/
boolean platform_check_invoke_syscall(void *arg1, void *arg2, unsigned int arg3, uint32 arg4, int syscall_id)
{
  tzbsp_regs_t* regs;
  qsee_common_request_t* qsee_request;

  /* If the DPC thread is blocked, we will only allow one syscall to be executed:  
   * The listener response command.  This limitation is imposed because the idle thread doesn't
   * have enough stack space to handle any arbitrary syscall.  Also there are some concurrency 
   * concerns about what can take place in TZ while the DPC thread is blocked.  
   * Thus, if the DPC thread is blocked, we only allow the syscall that will unblock it.*/

  /*Additionally we allow the OCMEM syscalls since they use little stack and have no concurrency concerns */

  /*If the syscall is a listener response, handle it*/
  if (syscall_id == TZ_OS_LISTENER_RESPONSE_HANDLER_ID ||
      syscall_id == TZ_OS_LISTENER_RESPONSE_WLIST_HANDLER_ID)
  {
    return TRUE;
  }

  /*If the syscall is for OCMEM, handle it*/
  else if(syscall_id == TZ_OCMEM_LOCK_REGION || syscall_id == TZ_OCMEM_UNLOCK_REGION)
  {
    /*We can service the OCMEM syscalls*/
    return TRUE; 
  }

  /*If the main service thread is not blocked on a listener itself, and the incoming syscall is not a QSEE layer syscall, handle it. 
    The idea here is that main service thread is blocked on an invocation (either on a module or an app), 
    and that is somewhere down the line blocked on a listener. 
    We don't allow a QSEE syscall here because they might not be re-entrant safe.*/

  /*This is done under the assumption that the secondary service thread has sufficient stack size*/
  else if( !mink_thread_blocked_on_listener()  &&  
            !IS_OWNER_TRUSTED_OS( TZ_SYSCALL_OWNER_ID(syscall_id))  && 
            !IS_OWNER_TZ_APPS( TZ_SYSCALL_OWNER_ID(syscall_id)))  
  {
    return TRUE;
  }  
  
  /*Caller should retry the syscall*/
  return FALSE;  
}


/*init related*/

tzexec_memblock_t* imem_mem_layout;
tzexec_memblock_t* ddr_mem_layout;
tzexecFuncGroups_t*   tzbsp_fns;

/** Defining tzos_entry as weak reference TZOS (QSEE or MobiCore) if present
 * are expected to implement tzos_entry */

/* Initialize shim layers */
extern void qsee_crypto_shim_init(void* fntablegrp);
extern void qsee_core_shim_init(void* fntablegrp);
extern void qsee_boot_shim_init(void* fntablegrp);
extern void qsee_timer_shim_init(void* fntablegrp);
extern void qsee_int_shim_init(void* fntablegrp);
extern void qsee_cache_shim_init(void* fntablegrp);
extern void qsee_securechannel_shim_init(void* fntablegrp);

static tzexec_monitor_handler_table_t mon_handlers =
{
  qsee_smc_handler,
  qsee_int_handler,
  qsee_legacy_app_smc_handler,
  qsee_warmboot_notifier_func,
  qsee_power_collapse_notifier_func
};

void qsee_set_func_table_group(void *fntablegrp)
{
  /* Initialize shim layers */
  qsee_crypto_shim_init(fntablegrp);
  qsee_core_shim_init(fntablegrp);
  qsee_boot_shim_init(fntablegrp);
  qsee_timer_shim_init(fntablegrp);
  qsee_int_shim_init(fntablegrp);
  qsee_cache_shim_init(fntablegrp);
  qsee_securechannel_shim_init(fntablegrp);
}

#define LK_CFN(x, grp, off)    \
  (((tzexecFuncTable_t *) x[grp].group) + off)->fntbl_group
  
int exec_init(const tzexec_memblock_t* a0,
                             const tzexec_memblock_t* a1,
                             uint32                  a2)
{
  imem_mem_layout = (tzexec_memblock_t*) a0;
  ddr_mem_layout  = (tzexec_memblock_t*) a1;
  tzbsp_fns       = (tzexecFuncGroups_t*) a2;

  /* Call to set LK function pointers. This sets the smc handler 
   * for executive syscall
   */
  ((void (*)(tzexec_monitor_handler_table_t*))
  LK_CFN(tzbsp_fns, 0, 0))(&mon_handlers);

  qsee_set_func_table_group((void *)a2);

  return 0;
}

extern int tzbsp_reset_exec(uint32 addr);
void platform_kernel_init(void)
{
  tzbsp_reset_exec((uint32) exec_init);
}



extern int qsee_enforce_hdmi_hdcp_encryption(boolean enforce, boolean *current_vote);

void platform_app_cleanup(void)
{
	/*If this app has voted for hdcp encryption enforcement, then we need to lower the vote count when shutting down the app.  
	  We do this by voting for no enforcment on the app's behalf. */
	qsee_enforce_hdmi_hdcp_encryption(0, &(current_thread->hdcp_enforce));

}


void platform_deregister_shared_buffer(uint32 address, uint32 size)
{
/*   Secure camera only on 8084 at this time. 
  //If data is tagged for secure camera, we need to clear the data before deregistering:
  if(tzbsp_exec_is_s_tag_area(TZBSP_MEM_TAG_USECASE_SC, address, address + size)) 
  {
    memset((void*)address, 0, size);
    tzbsp_dcache_flush_region((void*)address, size);
  }
*/
}


static const char label[] = "core trustzone application key label securemsm";

int gen_key_for_tz_app(uint8 *key_out, uint8 *tz_app_id, uint8 app_id_len)
{
   int   ret = 0;

   if(!key_out || !tz_app_id || !app_id_len)
      return -1;

   memset(key_out, 0, TZBSP_AES256_KEY_SIZE);

   ret = tzbsp_kdf(NULL, TZBSP_AES256_KEY_SIZE,
                  (const void  *)label, strlen(label),
                  (const void  *)tz_app_id, app_id_len,
                  (void *)key_out,TZBSP_AES256_KEY_SIZE);


   return ret;
}

int gen_tz_app_id(const char *app_name, uint8 *tz_app_id, uint8 app_id_len)
{
  TZBSP_HASH_ALGO_ET alg = TZBSP_HASH_SHA256;
  uint8              digest[TZBSP_SHA256_HASH_SZ];
  uint32             ret;
  uint32             len = 0;
  int                i;

  if (!tz_app_id)
  {
    TZBSP_LOG_DBG("{%x}",QSEE_RESULT_FAIL_BAD_APP_NAME);
    return -1;
  }

  if (TZBSP_APP_ID_LEN != app_id_len)
  {
    TZBSP_LOG_DBG("{%x %x}", QSEE_RESULT_FAIL_BAD_APP_NAME, app_id_len);
    return -1;
  }

  len = strlen(app_name);
  if(0 == len)
  {
    TZBSP_LOG_DBG("{%x %x %x}", QSEE_RESULT_FAIL_BAD_APP_NAME, app_id_len, len);
    return -1;
  }

  memset( digest, 0, TZBSP_SHA256_HASH_SZ);
  ret = tzbsp_hash(alg, (const uint8 *)app_name, len, digest, sizeof(digest));
  if(ret)
  {
    TZBSP_LOG_DBG("{%x %s %x}",QSEE_RESULT_FAIL_BAD_APP_NAME, app_name,ret);
    return ret;
  }

  for(i=0; i<app_id_len; i++)
  {
    if(digest[i])
      break;
  }
  if(app_id_len == i)
  {
    TZBSP_LOG_DBG("{%x %x %s}",QSEE_RESULT_FAIL_BAD_APP_NAME, app_id_len, app_name);
    return -1;
  }

  memcpy(tz_app_id, digest, app_id_len);

  return 0;

}

int platform_get_app_key_id(char *app_name, uint8 *key, uint8 *id, boolean *key_created)
{

 int i;

 for(i=0; i<TZBSP_APP_ID_LEN; i++)
 {
   if(global_tz_app_id[i])
	 break;
 }
 if(i<TZBSP_APP_ID_LEN)
 {
   /* Generate application key when we get unique application ID from certificate */
   *key_created = TRUE;
   (void)gen_key_for_tz_app(key, global_tz_app_id, TZBSP_APP_ID_LEN);
   //(void)gen_tz_app_id(app_name, global_tz_app_id, TZBSP_APP_ID_LEN);
 }
 else
 {
   *key_created = FALSE;
 }
 if(gen_tz_app_id(app_name, global_tz_app_id, TZBSP_APP_ID_LEN))
 {
	return QSEE_RESULT_FAIL_BAD_APP_NAME;
 }

 memcpy(id, global_tz_app_id, TZBSP_APP_ID_LEN);
 return 0;
}






int platform_handle_syscall(ObjectCxt ptr, ObjectOp op, ObjectArg *args, ObjectCounts k)
{
  if ((op == Object_OP_retain) || (op == Object_OP_release)) {
    return 0 ;
  }
  switch (op) {
    default:
      return -1;
  } 
}


uint32 tzbsp_read_hw_version(void)
{
  uint32 hwver;
  hwver = HWIO_IN(TCSR_SOC_HW_VERSION);
  return hwver;
}

