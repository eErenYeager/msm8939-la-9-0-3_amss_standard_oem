#ifndef INC_PLATFORM_API
#define INC_PLATFORM_API

//#include "elf.h"
//#include "smc.h"


/*----------------------------------
 *  MINK INTERFACE TO PLATFORM 
 -----------------------------------*/


/*Core, syscall and mem related  ----------------------------------------------------  */
/*
uint32 platform_get_random(uint8* buf, uint32 size);
boolean platform_secure_memory_checks_enabled(void);
boolean platform_is_ns_range(const void* start, uint32 len);
boolean platform_validate_ddr_checks(void);
boolean platform_ddr_contains_area(const void* start_ptr, uint32 len);
int platform_protect_memory(
  uint32 start,
  uint32 size,
  qsee_protect_memory_usage_hint_type usage_hint,
  boolean lock,
  uint32 flags
);
*/
  
boolean platform_check_invoke_syscall(void *arg1, void *arg2, unsigned int arg3, uint32 arg4, int syscall_id);

/*Secure app related  -----------------------------------------------------------------*/

void platform_deregister_shared_buffer(uint32 address, uint32 size);
int platform_get_app_key_id(char *app_name, uint8 *key, uint8 *id, boolean *key_created);
void platform_app_cleanup(void);
/*
boolean platform_allow_shared_buffer(uint32 address, uint32 size);
boolean platform_handle_appmgr_command(
  uint32_t command_id, 
  void* req, 
  uint32_t reqlen, 
  void* rsp, 
  uint32_t rsplen, 
  int *retval, 
  boolean secure
);
*/

/*Heap related ------------------------------------------------------------------------*/ 
/*
void* platform_heap_mem_malloc(unsigned int size);
int platform_heap_init(void *heap_mem_ptr, unsigned long heap_mem_size);
int platform_heap_mem_free(void *ptr);
*/

/*Elfloader related  ------------------------------------------------------------------*/
/*
int platform_authenticate_img_headers(Elf32_Ehdr *elf_hdr);
int platform_authenticate_image(void);
int platform_set_img_reloc_range(uint32 start, uint32 end);
void platform_reinit_subsystem_info(qsee_subsystem_image_type proc, uint32 start, uint32 size);
*/

/*Diag related --------------------------------------------------------------------*/
/*
boolean platform_clear_diag_region(void);
boolean platform_get_shared_imem_addr(uint32 ***addr);
boolean platform_allow_jtag_logging(void);
void platform_jtag_put(const char ch);
*/

/*Interrupts and configure_hw.c ---------------------------------------------------*/
/*
int platform_sec_processor_target(uint32 debug);
int platform_configure_qgic(void);
int platform_configure_qgic_interrupts(void);
void platform_set_clk_vote(boolean val);
*/

#endif
