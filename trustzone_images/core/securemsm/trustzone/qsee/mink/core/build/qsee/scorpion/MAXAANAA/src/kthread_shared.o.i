 PRESERVE8
    EXPORT mink_generic_syscall
    AREA kthread_shared, CODE, ALIGN=8
    CODE32
mink_generic_syscall
    push {r0-r12, r14}
 mov r0, r1 ; SWI num
    mov r4, r2 ; arg 0
    mov r5, r3 ; arg 1
    ldr r6, [sp,#0x38] ; arg 2
    ldr r7, [sp,#0x3C] ; arg 3
    swi 0x1400
 ldr r2, [sp] ; ret addr
 strd r0, r1, [r2] ; store return vals
 add r13, r13, #0x10 ;adjust pc
    pop {r4-r12, pc}
    END
