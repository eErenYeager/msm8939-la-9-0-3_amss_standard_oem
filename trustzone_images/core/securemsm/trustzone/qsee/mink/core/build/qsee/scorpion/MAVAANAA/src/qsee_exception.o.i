    PRESERVE8
    EXPORT qsee_swi_handler
    EXPORT qsee_undef_handler
    EXPORT qsee_dabort_handler
    EXPORT qsee_pabort_handler
    IMPORT qsee_data_fault_status
    IMPORT qsee_data_fault_addr
    IMPORT qsee_instr_fault_status
    IMPORT qsee_instr_fault_addr
    IMPORT qsee_handle_page_fault
    EXPORT qsee_get_request_info
    EXPORT qsee_save_thread_regs
    IMPORT qsee_handle_app_request
    IMPORT entry_cpsr
    EXPORT qsee_apps_svc_stack_end
    EXPORT qsee_apps_svc_stack_guard
    IMPORT thread_get_curr_exception_sp
    IMPORT thread_current_vfp_enable
    IMPORT mon_save_ns_vfp_context
    AREA qsee_exception_asm, CODE, ALIGN=8
    CODE32
qsee_swi_handler
    mrs r2, spsr
    ldr r1, =exception_cpsr
    str r2, [r1]
    sub r1, r1, #4
    str r14, [r1]
    stmdb r1, {r4 - r14}^
    ldr r1, =qsee_apps_svc_stack_start
    sub r1, r1, #8
    mov r13, r1
    ldr r2, =app_init_swi_num
    ldr r2, [r2]
    cmp r2, r0
    beq done_set_exception_sp
    mov r4, r0 ; backup r0
    blx thread_get_curr_exception_sp
    mov r13, r0
    mov r0, r4 ; restore r0
done_set_exception_sp
    mrs r2, cpsr
    ldr r1, =entry_cpsr
    ldr r1, [r1]
    and r1, #0xC0
    and r2, #(~0xC0)
    orr r2, r1
    msr cpsr_c, r2
    ldr r1, =qsee_handle_app_request
    blx r1
qsee_get_request_info
    push {r4-r8, r14}
    ldr r2, =exception_regs
    add r2, r2, #0x10
copy_loop
    ldmia r2!, {r3}
    stmia r0!, {r3}
    sub r1, r1, #1
    cmp r1, #0
    bne copy_loop
    pop {r4-r8, pc}
qsee_save_thread_regs
    ldr r1, =exception_regs
    mov r2, #17
save_loop
    ldmia r1!, {r3}
    stmia r0!, {r3}
    sub r2, r2, #1
    cmp r2, #0
    bne save_loop
    bx r14
qsee_undef_handler
    b qsee_undefined_handler
qsee_dabort_handler
    str r14, [r13]
    mrs r14, spsr
    ands r14, r14, #0xF
    beq qsee_exception_handler
    mrc p15, 0, r14, c5, c0, 0
    str r14, [r13, #0x4]
    mrc p15, 0, r14, c6, c0, 0
1 b %b1
qsee_pabort_handler
    str r14, [r13]
    mrs r14, spsr
    ands r14, r14, #0xF
    beq qsee_exception_handler
    mrc p15, 0, r14, c5, c0, 1
    str r14, [r13, #0x4]
    mrc p15, 0, r14, c6, c0, 2
1 b %b1
; Remove pager entry for TZ.BF.3.0, since no demand paging is used.
; The above functions will be friendlier for debugging.
qsee_undefined_handler
    sub r13, #60
    stm r13, {r0 - r14}^ ; dump user mode registers on the stack
    sub r13, r13, #4
    mrs r0, spsr
    push {r0}
    and r0, r0, #0x20
    cmp r0, #0
    bne from_t32
    sub r14, r14, #2
from_t32
    sub r14, r14, #2 ; point to instruction causing the exception
    sub r13, r13, #4
    push {r14}
    ; read the instruction that caused the exception
    ldr r1, [r14]
    ; check if instruction is T32 VFP or SIMD
    cmp r0, #0
    bne t32_instruction
    mov32 r0, #0xF2000000
    and r0, r0, r1
    mov32 r1, #0xF2000000
    cmp r0, r1
    beq valid_vfp_simd
    ldr r1, [r14]
    mov32 r0, #0xE000A00
    and r0, r0, r1
    mov32 r1, #0xE000A00
    cmp r0, r1
    beq valid_vfp_simd
    ldr r1, [r14]
    mov32 r0, #0xC000A00
    and r0, r0, r1
    mov32 r1, #0xC000A00
    cmp r0, r1
    beq valid_vfp_simd
    ldr r1, [r14]
    mov32 r0, #0xF4000000
    and r0, r0, r1
    mov32 r1, #0xF4000000
    cmp r0, r1
    beq valid_vfp_simd
    ldr r1, [r14]
    mov32 r0, #0xE000A00
    and r0, r0, r1
    mov32 r1, #0xE000A00
    cmp r0, r1
    beq valid_vfp_simd
    ldr r1, [r14]
    mov32 r0, #0xC400A00
    and r0, r0, r1
    mov32 r1, #0xC400A00
    cmp r0, r1
    beq valid_vfp_simd
    ldr r1, [r14]
    mov32 r0, #0xEC000A00
    and r0, r0, r1
    mov32 r1, #0xEC000A00
    cmp r0, r1
    beq valid_vfp_simd
    ldr r1, [r14]
    mov32 r0, #0xFC000A00
    and r0, r0, r1
    mov32 r1, #0xFC000A00
    cmp r0, r1
    beq valid_vfp_simd
t32_instruction
    ldr r1, [r14]
    ldr r0, [r14]
    lsr r1, #16
    lsl r0, #16
    orr r2, r1, r0
    mov32 r0, #0xEF000000
    and r0, r0, r2
    mov32 r1, #0xEF000000
    cmp r0, r1
    beq valid_vfp_simd
    mov32 r0, #0xE000A00
    and r0, r0, r2
    mov32 r1, #0xE000A00
    cmp r0, r1
    beq valid_vfp_simd
    mov32 r0, #0xC000A00
    and r0, r0, r2
    mov32 r1, #0xC000A00
    cmp r0, r1
    beq valid_vfp_simd
    mov32 r0, #0xF9000000
    and r0, r0, r2
    mov32 r1, #0xF9000000
    cmp r0, r1
    beq valid_vfp_simd
    mov32 r0, #0xE000A00
    and r0, r0, r2
    mov32 r1, #0xE000A00
    cmp r0, r1
    beq valid_vfp_simd
    mov32 r0, #0xC400A00
    and r0, r0, r2
    mov32 r1, #0xC400A00
    cmp r0, r1
    beq valid_vfp_simd
    mov32 r0, #0xEC000A00
    and r0, r0, r2
    mov32 r1, #0xEC000A00
    cmp r0, r1
    beq valid_vfp_simd
    mov32 r0, #0xFC000A00
    and r0, r0, r2
    mov32 r1, #0xFC000A00
    cmp r0, r1
    beq valid_vfp_simd
undef_loop ; If it is not VFP and SIMD exception, loop indefinitely
    b qsee_exception_handler
valid_vfp_simd
; ------------------------------------------------------------------
; Enable VFP and Neon this should cause an exception into EL3
;-------------------------------------------------------------------
    ; disable trapping to EL1
    mrc p15,0,r0,c1,c0,2 ; Read CPACR into Rt
    mov r1, #0xF<<20
    orr r0, r0, r1
    mcr p15,0,r0,c1,c0,2 ; Write Rt to CPACR
    isb SY
; ------------------------------------------------------------------
; Check if anyone othet than the user app caused the VFP exception.
; ------------------------------------------------------------------
    mrs r1, spsr
    and r1, r1, #0x1F
    cmp r1, #0x1<<4
    beq no_loop
InvalidModeLoop
    b qsee_exception_handler ; Someone other than the user caused the VFP exception
no_loop
    ; Enable VFP/SIMD
    vmrs r1, FPEXC
    orr r1, r1, #0x1<<30;
    vmsr FPEXC, r1 ; enable VFP
    isb ; make sure the VFP is enabled before saving the NS VFP context
    bl mon_save_ns_vfp_context
    bl thread_current_vfp_enable
    cmp r0, #0x0
    bne vfp_mem_allocated
loop_vfp_malloc_fail ; If it is not VFP and SIMD exception, loop indefinitely
    b qsee_exception_handler
vfp_mem_allocated
    pop {r14}
    add r13, r13, #4
    pop {r0}
    msr spsr_und, r0 ; restore the spsr
    add r13, r13, #4
    ldm r13, {r0 - r14}^ ; restore user registers
    add r13, r13, #60 ; Adjust the stack pointer to the original position
    eret ; return to the instruction causing the exception
;END
qsee_exception_handler
    ldr r14, =exception_cpsr
    sub r14, r14, #4
    stmdb r14, {r0 - r14}^
    ldr r0, [r13]
    str r0, [r14]
    mrs r0, spsr
    str r0, [r14, #4]
    ldr r1, =qsee_data_fault_status
    mrc p15, 0, r0, c5, c0, 0
    str r0, [r1]
    ldr r1, =qsee_data_fault_addr
    mrc p15, 0, r0, c6, c0, 0
    str r0, [r1]
    ldr r1, =qsee_instr_fault_status
    mrc p15, 0, r0, c5, c0, 1
    str r0, [r1]
    ldr r1, =qsee_instr_fault_addr
    mrc p15, 0, r0, c6, c0, 2
    str r0, [r1]
    mrs r14, spsr
    ands r14, r14, #0xF
    subnes r14, r14, #0xF
    bne handle_non_usr_mode_fault
    msr CPSR_c, #0xD3
    ldr r1, =qsee_apps_svc_stack_start
    sub r1, r1, #8
    mov r13, r1
    ldr r1, =qsee_handle_app_request
    mov r0, #0
    blx r1
handle_non_usr_mode_fault
    mrs r14, spsr
    and r14, r14, #0x1F
    cmp r14, #0x13
    bne exception_loop_here
    mrs r14, cpsr
    and r14, r14, #0x1F
    cmp r14, #0x17
    bne exception_loop_here
    ldr r14, =exception_regs
    stm r14, {r0 - r3,r12}
    ldr r0, [r13]
    ldr r1, [r13, #0x4]
    cmp r1, #0x0
    bne pref_abt_mode
    sub r0, r0, #0x4
pref_abt_mode
    sub r0, r0, #0x4
    mrs r1, spsr
    ldr r14, =exception_cpsr
    str r1, [r14]
    sub r14, r14, #4
    str r0, [r14]
    ldr r1, [r13, #0x4]
    cmp r1, #0x0
    bne invoke_pager
    mrc p15, 0, r0, c6, c0, 0
invoke_pager
    ldr r2, =qsee_handle_page_fault
    blx r2
    cmp r0, #0x0
    bne pager_error
    ldr r14, =exception_regs
    ldm r14, {r0 - r3,r12}
    ldr r14, =exception_cpsr
    sub r14, r14, #4
    rfe r14
pager_error
    b pager_error
exception_loop_here
    b exception_loop_here
    AREA |QSEE_SWI_Area|, DATA, READWRITE
exception_regs SPACE 0x40
exception_cpsr SPACE 0x4
qsee_apps_svc_stack_guard DCD 0xdeadbeef
qsee_apps_svc_stack_end SPACE 0x100
qsee_apps_svc_stack_start DCD 0xdeadbeef
app_init_swi_num DCD (0xffffff00 + 0x0)
    END
