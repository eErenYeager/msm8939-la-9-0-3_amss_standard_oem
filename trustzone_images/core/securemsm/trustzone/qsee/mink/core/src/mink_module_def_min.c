
/**
* @file mink_pil_module.c
* @brief define PIL module and 
*
*/

/*===========================================================================
   Copyright (c) 2010 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsee/mink/core/src/mink_module_def_min.c#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $


when       who      what, where, why
--------   ---      ------------------------------------
12/16/2010 cap      Initial
===========================================================================*/

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include <string.h>
#include <limits.h>
#include "mink_modules.h"


#if 0

extern uint8 subsys_config_acceptBuf[];
extern uint32 subsys_config_acceptBufSize;

MINK_DEFINE_MODULE(
    bsp,
    bsp_module_entry,
    MINK_PRIV_LEVEL_KERNEL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
	NULL,
	NULL,
	NULL,
    3,
    {MINK_SERVICE_SYSCALL_ID, MINK_SERVICE_IMG_AUTH_ID, MINK_SERVICE_SUBSYS_RESET_ID});

MINK_DEFINE_MODULE(
    subsys_config,
    subsys_config_module_entry,
    MINK_PRIV_LEVEL_USER,
    subsys_config_stack,
    SUBSYS_STACK_SIZE,
    &Image$$MINK_MODULE_SUBSYS_CONFIG_RO$$Base,
    &Image$$MINK_MODULE_SUBSYS_CONFIG_RO$$Length,
    &Image$$MINK_MODULE_SUBSYS_CONFIG_RW$$Base,
    &Image$$MINK_MODULE_SUBSYS_CONFIG_RW$$Length,
	4,
	subsys_config_acceptBuf,
	subsys_config_acceptBufSize,
    1,
    {MINK_SERVICE_SUBSYS_CONFIG_ID});
#endif

const void *mink_modules_start = &(_mink_module$$Base);
const void *mink_modules_end = &(_mink_module$$Limit);
