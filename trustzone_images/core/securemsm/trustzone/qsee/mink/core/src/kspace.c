	
#include "kspace.h"
#include "heap.h"
#include "mink_object.h"
#include "lkthread.h"
#include <stdint.h>

extern int qsee_validate_ptrs_belong_to_app(uint32_t address, uint32_t size,
                                            thread_t *t, boolean commonlib,
                                            uint32_t* bytes_left, uint32_t memory_type);

void KSpace_construct(KSpace *me)
{
   me->numLocks = 0;
}


void KSpace_destruct(KSpace *me)
{
   (void) me;
}

static int KMemLock_validate_range(void *ptr, size_t len)
{
   uint32_t bytes_left = 0;
   int ret = Object_ERROR;

   // The validate function returns 1 if successful in which case we need to
   // return good Object status (even though it has nothing to do with Objects)
   // otherwise we return an object error. The object status coincides with the
   // expectation of the kspace callers.
   // NOTE: We return false for including commonlib as the only syscall that
   // calls in here is qsee_request_service which should not include it.
   if (qsee_validate_ptrs_belong_to_app((uint32_t)ptr, (uint32_t)len,
                                        current_thread, false, &bytes_left,
                                        KSPACE_ALL)) {
     ret = Object_OK;
   }

   return ret;
}

// KMemLock is an object that locks a set of regions in the memory space.
//
// Calls to `lock[R/W/RW]` atomically validate that a memory range is
// accessible in the memory space *and* locks it (preserves the mapping of
// that range and the access rights).
//
// `destruct` releases all of the ranges that have been locked.


int KMemLock_lockR(KMemLock *me, void *ptr, size_t len)
{
   (void) me;

   return KMemLock_validate_range(ptr, len);
}


int KMemLock_lockW(KMemLock *me, void *ptr, size_t len)
{
   (void) me;

   return KMemLock_validate_range(ptr, len);
}


int KMemLock_lockRW(KMemLock *me, void *ptr, size_t len)
{
   (void) me;

   return KMemLock_validate_range(ptr, len);
}


void KMemLock_destruct(KMemLock *me)
{
   (void) me;
}


void KMemLock_construct(KMemLock *me, KSpace *space)
{
   me->space = space;
}
