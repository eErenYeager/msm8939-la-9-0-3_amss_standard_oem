
/**
* @file mink_pil_module.c
* @brief define PIL module and 
*
*/

/*===========================================================================
   Copyright (c) 2010 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsee/mink/core/src/mink_module_def.c#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $


when       who      what, where, why
--------   ---      ------------------------------------
12/16/2010 cap      Initial
===========================================================================*/

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include <string.h>
#include <limits.h>
#include "mink_modules.h"
#include "mink_services.h"


//extern Object bsp_module_entry(Object outgoing);
extern int bsp_module_entry(ObjectCxt h, ObjectOp op, ObjectArg *args, ObjectCounts counts);

MINK_DEFINE_MODULE(
    bsp,
    bsp_module_entry,
    MINK_PRIV_LEVEL_KERNEL,
    NULL,
    0,
    NULL,
    NULL,
    NULL,
    NULL,
    0,
    NULL,
    0,
    2,
    {MINK_SERVICE_SYSCALL_ID, MINK_SERVICE_IMG_AUTH_ID});

const void *mink_modules_start = &(_mink_module$$Base);
const void *mink_modules_end = &(_mink_module$$Limit);
