	
#include <stddef.h>
#include "lktypes.h"
#include <string.h>
#include <stringl.h> //memscpy
#include "qlist.h"
#include "mink_object.h"
#include "heap.h"
#include "kprocess_priv.h"
#include "kthread.h"
#include "server.h"
#include "minkipc.h"
#include "kdefs.h"


#define isOK(r)     Object_isOK(r)

#define Object_setNull(o)   ((o).invoke = NULL)

#define FOR_ARGS(ndxvar, counts, section)                  \
   for (ndxvar = ObjectCounts_index##section(counts);      \
        ndxvar < (ObjectCounts_index##section(counts)      \
                  + ObjectCounts_num##section(counts));    \
        ++ndxvar)


typedef struct QueuedInvoke QueuedInvoke;
typedef struct Forwarder Forwarder;


//------------------------------------------------------------------------
// An `Invoker` describes an inbound invocation while it awaits dispatch.
// Queued Invoker instances are either:
//
//  A) `BlockingInvoker.inv` when a thread is blocked. In this case the
//     thread is non-NULL and must be signaled when the invocation
//     completes.
//
//  B) `Forwarder.post` in the special case of an asynchronous `release`
//     operation. In this case the forwarder must be deleted when the
//     invocation completes.
//
//------------------------------------------------------------------------


typedef struct Invoker Invoker;
struct Invoker {
   QNode qn;               // ...on list of all pending invocations
   KThread *thread;        // the thread that is blocked
};


typedef struct BlockingInvoker BlockingInvoker;
struct BlockingInvoker {
   Invoker invoker;

   // params to invoke()
   Forwarder *forwarder;
   int op;
   ObjectArg *args;
   ObjectCounts k;

   // response from invoke
   int result;
};


//------------------------------------------------------------------------
// Forwarders: proxies that perform "inbound" (into a process) invocations
//------------------------------------------------------------------------

// A "forwarder" is a kernel object that forwards its invocations to an
// object that lives inside a process.
//
// When it is created, the forwarder takes ownership of the reference to the
// user object.  When the forwarder is destroyed, it delivers an invocation
// that will release the user object.  Forwarders guarantee that this final
// release (as well as other calls to release) will be non-blocking. (This
// avoids some deadlock scenarios.)
//
// Due to this requirement for non-blocking release, forwarders have unusual
// lifetime management.  When their reference count reaches zero, they queue
// an Invoker to release the user object.  Since release cannot block, the
// Invoker cannot be stack-allocated as in ordinary invocations.  Instead,
// this Invoker is pre-allocated as part of the forwarder itself, and the
// forwarder gets freed after that Invoker has been dequeued by the server
// (not synchronously during its final release).


struct Forwarder {
   int refs;
   KProcess *proc;        // process that hosts the object
   Object objUser;        // target object; valid in host process (not in kernel)
   QNode qnForwarders;    // ... in list of all forwarders
   Invoker post;          // for asynchronous release of objUser
   bool owns;             // true => release objUser when forwarder is deleted
};


static void Forwarder_own(Forwarder *me, bool owns)
{
   me->owns = owns;
}


// Make forwarder defunct.
//
// Future invocations will not enqueue on the server queue.
//
// Currently pending invocations will remain queued until the server queue is flushed.
//
static void Forwarder_detach(Forwarder *me)
{
   KProcess_release(me->proc);
   QNode_dequeue(&me->qnForwarders);
   me->proc = NULL;
}


static void Forwarder_delete(Forwarder *me)
{
   if (me->proc) {
      Forwarder_detach(me);
   }
   HEAP_FREE_PTR(me);
}


static void Invoker_onCompletion(Invoker *pi)
{
   KThread *thread = pi->thread;
   if (thread) {
      KThread_signal(pi->thread);
   } else {
      Forwarder *fwd = containerOf(pi, Forwarder, post);
      Forwarder_delete(fwd);
   }
}


static void Forwarder_postRelease(Forwarder *me)
{
   if (me->owns && me->proc && me->proc->thread) {
      QList_appendNode(&me->proc->qlInvokers, &me->post.qn);
      KThread_signal(me->proc->thread);
   } else {
      Forwarder_delete(me);
   }
}


static void Forwarder_release(Forwarder *me)
{
   if (--me->refs == 0) {
      Forwarder_postRelease(me);
   }
}


static void Forwarder_retain(Forwarder *me)
{
   ++me->refs;
}


static int Forwarder_invoke(ObjectCxt p, ObjectOp op, ObjectArg *args, ObjectCounts k)
{
   Forwarder *me = (Forwarder*)p;

   if (ObjectOp_isLocal(op)) {
      if (op == Object_OP_release) {
         Forwarder_release(me);
         return Object_OK;
      } else if (op == Object_OP_retain) {
         Forwarder_retain(me);
         return Object_OK;
      }
      return Object_ERROR_REMOTE;
   }

   KProcess *proc = me->proc;
   if (!proc) {
      return Object_ERROR_DEFUNCT;
   }

   struct BlockingInvoker bi;
   bi.invoker.thread = KThread_getCurrent();
   bi.forwarder = me;
   bi.op = op;
   bi.args = args;
   bi.k = k;
   bi.result = 0;
   QNode_construct(&bi.invoker.qn);

   KThread *thread = proc->thread;
   if (!thread) {
      return Object_ERROR_UNAVAIL;
   }

   QList_appendNode(&proc->qlInvokers, &bi.invoker.qn);
   KThread_signal(thread);

#if 0   
   if (!KThread_IsFaulted(thread)) {
     do {
        if (KThread_wait()) {
           return Object_ERROR_ABORT;
        }   
     }while (QNode_isQueued(&bi.invoker.qn));
   } else {
     return Object_ERROR_ABORT;
   }
#else
   do {
     if (KThread_wait()) {
       return Object_ERROR_ABORT;
     }   
   }while (QNode_isQueued(&bi.invoker.qn));   
#endif
   return bi.result;
}


//  objUser = object valid inside the process
//  consume => take ownership of the user object, releasing the object
//             after the forwarder is deleted.
//
static Object Forwarder_newObject(KProcess *proc, Object objUser, bool consume)
{
   Forwarder *me = HEAP_ZALLOC_REC(Forwarder);
   Object o = { Forwarder_invoke, me };
   if (!me) {
      Object_setNull(o);
      return o;
   }

   me->refs = 1;
   me->proc = proc;
   KProcess_retain(proc);
   me->objUser = objUser;
   QList_appendNode(&proc->qlForwarders, &me->qnForwarders);
   me->owns = consume;

   return o;
}


//------------------------------------------------------------------------
// Object Marshaling
//------------------------------------------------------------------------
//
// Terminology
// -----------
//
// Kernel object: an object valid in the kernel. Its `context` member
//     typically points to memory in the kernel heap.
//
// User object: an object valid in a user domain (a process).
//
// Outbound object: an object that forwards invocations to a kernel object.
//     The `invoke` member is `minkIPC` and the `context` member is an index
//     into the object table.  Outbound objects are only valid within a
//     process.
//
// Hosted object: a user object that is not an outbound object.  Its
//     `context` member typically points to memory owned by that process.
//
// Inbound object: a kernel object that forwards invocations to a hosted
//     object. This is also called a "Forwarder".
//
//
// Object Marshaling Across a Process Boundary
// -------------------------------------------
//
// To marshal an object across a domain boundary we create or find an object
// on the other side of the boundary that responds to `invoke` in the same
// way.  There are two possibilities:
//
//   1. Adding a layer of indirection by creating an object that forwards
//      across the boundary (an outbound or inbound object).
//
//   2. Removing a layer of indirection by extracting the target object from
//      an existing outbound or inbound object.
//
// Complications arise from reference counting across the process boundary,
// and in particular when reference ownership is transferred across the
// boundary. The following sections elaborate on reference management.
// The `Object_NULL` case is trivial and not covered below.
//
// copyObjectToUser
// -----------------------------------------------------------------
//
// This occurs when input objects are passed to an inbound invoke (via
// server_accept), and when output objects are received from an outbound
// invoke (via minkIPC). Consider two cases that differ by the kind of user
// object that results:
//
//   1. Hosted object: When the kernel object is an inbound object that
//      forwards into the same process to which we are marshaling, we
//      retrieve the target object from the forwarder.
//
//   2. Outbound object: For any other kernel object, add it to the object
//      table of the process and construct an outbound object.
//
// Reference counting in the hosted object case differs between:
//
//    A) Objects returned from outbound invocations
//
//       When a hosted object is returned from the kernel, minkIPC will
//       `retain` it before returning to the caller.
//
//    B) Objects passed to an inbound invocation
//
//       Since ownership of a reference is not transferred, there is no
//       special handling required.
//
// copyObjectFromUser
// -----------------------------------------------------------------
//
// This occurs when input objects are passed to an outbound invoke (via
// minkIPC), and when output objects are received from an inbound invoke
// (via server_accept).  Consider two cases named by the kind of user object
// that we are marshaling *from*:
//
//   1. Outbound object: In this case we find the target kernel object in
//      the object table of the process.
//
//   2. Hosted object: we create a forwarder that routes into the process.
//
// Reference counting in the user object case differs between:
//
//   A) Inputs to outbound invocations
//
//      If the invoked object retains a pointer to the forwarder we have
//      created, then the user object must be retained (to be released later
//      when the forwarder is released).  This is handled in minkIPC after
//      the return to user mode.
//
//      If the invoked object does not retain a pointer to the forwarder,
//      we avoid retaining the user object and prevent the forwarder from
//      releasing the user object when it is deleted.
//
//   B) Outputs from inbound invocations
//
//      When objects are returned, ownership of the reference is transferred
//      to the caller.  Ordinarily, this works out neatly because the new
//      forwarder takes ownership of the user object.  However, if forwarder
//      creation fails (due to lack of memory) we release the user object by
//      manufacturing the corresponding invocation when returning from
//      server_accept().
//


#define ObjectCxt_fromInt(n)  ((ObjectCxt) (intptr_t) n)

#define ObjectCxt_toInt(p)    ((int) (unsigned long) (p))

#define ObjectCxt_BAD         ObjectCxt_fromInt(-1)


#define makeOutboundObject(context) \
   ((Object) {minkIPC, ObjectCxt_fromInt(context) })

#define isOutboundObject(objUser)   ((objUser).invoke == minkIPC)

#define isHostedObject(objUser) \
   (!Object_isNull(objUser) && !isOutboundObject(objUser))

#define isForwarder(obj)     ((obj).invoke == Forwarder_invoke)

#define asForwarder(obj)     ((Forwarder *) (obj).context)



// Create and return a new forwarder for the specified inbound object.
//
// `objUser` is an object valid only inside the process. The resulting
// object is valid in the kernel domain.
//
// On failure, it returns an invalid object.
//
Object KProcess_newInboundObject(KProcess *proc, Object objUser)
{
   return Forwarder_newObject(proc, objUser, false);
}


// This accepts a kernel object and returns an object valid only inside the
// server process.
//
// On failure, it returns Object_NULL.
// On success, `obj` is retained.
//
Object KProcess_newOutboundObject(KProcess *me, Object obj)
{
   int n;
   unsigned int *refs = me->objectRefs;
   for (n = 0; n < me->objectsLen; ++n) {
      if (refs[n] == 0) {
         ++refs[n];
         me->objects[n] = obj;
         Object_retain(obj);
         return makeOutboundObject(n);
      }
   }
   return Object_NULL;
}


// Return the kernel object to which an outbound object forwards invokes.
//
static Object KProcess_getOutboundTarget(KProcess *me, ObjectCxt h)
{
   size_t ndx = ObjectCxt_toInt(h);
   if (ndx < me->objectsLen) {
      Object o = me->objects[ndx];
      if (!Object_isNull(o)) {
         Object_retain(o);
         return o;
      }
   }
   return Object_NULL;
}


// Release an outbound object, given its context value.
//
static void KProcess_releaseOutboundHandle(KProcess *me, ObjectCxt h)
{
   size_t ndx = (size_t) h;
   if (ndx < me->objectsLen) {
      int ref = --me->objectRefs[ndx];
      if (ref == 0) {
         Object_release(me->objects[ndx]);
         Object_setNull(me->objects[ndx]);
      }
   }
}

// Release an outbound object, given its context value.
//
static void KProcess_retainOutboundHandle(KProcess *me, ObjectCxt h)
{
   size_t ndx = (size_t) h;
   if (ndx < me->objectsLen) {
      ++me->objectRefs[ndx];
   }
}


static void KProcess_releaseIfOutboundObject(KProcess *me, Object objUser)
{
   if (isOutboundObject(objUser)) {
      KProcess_releaseOutboundHandle(me, objUser.context);
   }
}


// Marshal a user object to the kernel
//
// objUser           *objOut         return value
// ----------------  --------------  ----------------
// Object_NULL       Object_NULL     Object_OK
// outbound          kernel object   Object_OK
//                   Object_NULL     Object_ERROR_BADOBJ
// hosted            kernel object   Object_OK
//                   Object_NULL     Object_ERROR_KMEM
//
// When `consume` is true, this function consumes the reference to objUser
// (if valid) ... EXCEPT in the Object_ERROR_KMEM case, when a valid user
// object is left unconsumed.
//
// When `consume` is false, the user object is not consumed, but a
// "non-owning" forwarder may be returned.
//
static int KProcess_copyObjectFromUser(KProcess *me,
                                       Object objUser,
                                       bool consume,
                                       Object *objOut)
{
   if (Object_isNull(objUser)) {
      *objOut = Object_NULL;
   } else if (isOutboundObject(objUser)) {
      *objOut = KProcess_getOutboundTarget(me, objUser.context);
      if (Object_isNull(*objOut)) {
         return Object_ERROR_BADOBJ;
      } else if (consume) {
         KProcess_releaseOutboundHandle(me, objUser.context);
      }
   } else {
      *objOut = Forwarder_newObject(me, objUser, consume);
      if (Object_isNull(*objOut)) {
         return Object_ERROR_KMEM;
      }
   }
   return Object_OK;
}


// Marshal one object from kernel to user. See KProcess_copyObjectsToUser, below.
//
static int KProcess_copyObjectToUser(KProcess *me, Object obj, Object *objUserOut)
{
   if (Object_isNull(obj)) {
      *objUserOut = Object_NULL;
   } else if (isForwarder(obj) && asForwarder(obj)->proc == me) {
      // object NOT retained
      *objUserOut = asForwarder(obj)->objUser;
   } else {
      // object retained
      Object objUser = KProcess_newOutboundObject(me, obj);
      *objUserOut = objUser;
      if (Object_isNull(objUser)) {
         return Object_ERROR_NOSLOTS;
      }
   }

   return Object_OK;
}


// Marshal objects from kernel to user
//
// If failure occurs in marshaling any object, Object_NULL will be written
// and the function will return an error code. All objects will be processes
// in any case.
//
// When the result object is non-null, there are two cases:
//
//     Hosted object      NOT retained
//     Outbound object    new, retained
//
static int KProcess_copyObjectsToUser(KProcess *me,
                                      ObjectArg *args,
                                      ObjectArg *argsUserOut,
                                      int ndxStart,
                                      int num)
{
   int ndx, err;
   int result = Object_OK;
   int ndxEnd = ndxStart + num;
   for (ndx = ndxStart; ndx < ndxEnd; ++ndx) {
      err = KProcess_copyObjectToUser(me, args[ndx].o, &argsUserOut[ndx].o);
      if (err) {
         result = err;
      }
   }
   return result;
}


//------------------------------------------------------------------------
// accept
//------------------------------------------------------------------------


struct AcceptHeader {
   Object server;
   Object obj;
   ObjectOp op;
   ObjectArg *args;
   ObjectCounts k;
   int result;
};


// Write a description of the invocation into user space, including input
// buffers and objects.
//
// Returns:
//    Object_OK:  nominal condition
//    other:      failure to allocate buffers or objects
//
static int KProcess_marshalIn(KProcess *me, Invoker *pi)
{
   ObjectCounts k;
   ObjectOp op;
   ObjectArg *args;
   Forwarder *fwd;

   if (pi->thread) {
      BlockingInvoker *pbi = containerOf(pi, BlockingInvoker, invoker);
      k = pbi->k;
      args = pbi->args;
      fwd = pbi->forwarder;
      op = pbi->op;
   } else {
      fwd = containerOf(pi, Forwarder, post);
      k = 0;
      args = NULL;
      op = Object_OP_release;
   }

   AcceptHeader *hdrUser = me->acceptHeader;
   BBuf bbUser = me->acceptBuffer;
   ObjectArg *argsUser = BBuf_ALLOC_ARRAY(&bbUser, ObjectArg, ObjectCounts_total(k));

   if (!argsUser) {
      return Object_ERROR_MAXDATA;
   }

   // initialize header

   hdrUser->obj = fwd->objUser;
   hdrUser->op = op;
   hdrUser->args = argsUser;
   hdrUser->k = k;

   // buffers: allocate room for all buffers; copy input buffers

   int ndx = 0;
   int ndxEndBI = ndx + (int) ObjectCounts_numBI(k);
   int ndxEnd = ndxEndBI + (int) ObjectCounts_numBO(k);
   for (; ndx < ndxEnd; ++ndx) {
      size_t len = args[ndx].b.len;
      void *ptrUser = BBuf_alloc(&bbUser, len);
      if (!ptrUser) {
         return Object_ERROR_MAXDATA;
      }
      argsUser[ndx].b = (ObjectBuf) {ptrUser, len};
      if (ndx < ndxEndBI) {
         memscpy(ptrUser,len, args[ndx].b.ptr, len);
      }
   }

   // input objects
   //
   // cleanupArgs/cleanupNum schedule cleanup of retained outbound objects
   // that are created here

   int ndxStart = (int) ObjectCounts_indexOI(k);
   int numOI = ObjectCounts_numOI(k);
   me->cleanupArgs = argsUser + ndxStart;
   me->cleanupNum = numOI;

   if (KProcess_copyObjectsToUser(me, args, argsUser, ndxStart, numOI)) {
      return Object_ERROR_UNAVAIL;
   }
   return Object_OK;
}


// Release any outbound objects that were allocated for input objects
// (as in case 3 in marshalIn).
//
// Here we read from user memory (!) but the data only affects state that is
// under control of the process itself.  If the process were to somehow
// corrupt argsUser[], it can only harm itself.
//
static void KProcess_marshalInCleanup(KProcess *me)
{
   while (me->cleanupNum) {
      KProcess_releaseIfOutboundObject(me, me->cleanupArgs[--me->cleanupNum].o);
   }
}


// Copy output buffers to the caller's destination buffers, and marshal output objects
// objects.
//
// Here we calculate the positions of the arguments in the same manner that
// as marshalIn().  Note that we trust `pi`, not the user buffer.
//
// Since the buffer sizes were checked during marshalIn(), a MAXDATA failure
// indicates a serious problem (e.g. the Invoker was dequeued or or memory
// has been corrupted.)
//
static void KProcess_marshalOut(KProcess *me, Invoker *pi, int result)
{
   BlockingInvoker *pbi;
   ObjectArg *args;
   ObjectCounts k;
   BBuf bbUser;
   ObjectArg *argsUser;
   int ndx, ndxEndBI, ndxEnd;
   size_t len;
   void *ptrUser;
   int ndxStart, err;
   if (!pi->thread) {
      // posted `release` => no marshaling to do
      return;
   }
   pbi = containerOf(pi, BlockingInvoker, invoker);

   if (! Object_isOK(result)) {
      goto done;
   }

   k = pbi->k;
   args = pbi->args;

   bbUser = me->acceptBuffer;
   argsUser = BBuf_ALLOC_ARRAY(&bbUser, ObjectArg, ObjectCounts_total(k));

   if (!argsUser) {
      // This should not happen (should have been caught in marshalIn).
      // Perhaps this BlockingInvoker is not the one we expect to be
      // responding to, which could be the case if accept were re-entered,
      // but that should not happen, either. In any event, avoid corrupting
      // kernel memory.
      result = Object_ERROR;
      goto done;
   }

   // buffers: skip over input buffers; copy output buffer contents from "inside"

   ndx = 0;
   ndxEndBI = ndx + (int) ObjectCounts_numBI(k);
   ndxEnd = ndxEndBI + (int) ObjectCounts_numBO(k);
   for (; ndx < ndxEnd; ++ndx) {
      len = args[ndx].b.len;
      ptrUser = BBuf_alloc(&bbUser, len);
      if (!ptrUser) {
         result = Object_ERROR_MAXDATA;
         goto done;
      }
      if (ndx >= ndxEndBI) {
         memscpy(args[ndx].b.ptr, len, ptrUser, len);
      }
   }

   // objects

   ndxStart = (int) ObjectCounts_indexOO(k);
   ndxEnd = ndxStart + (int) ObjectCounts_numOO(k);
   for (ndx = ndxStart; ndx < ndxEnd; ++ndx) {
      err = KProcess_copyObjectFromUser(me, argsUser[ndx].o, true, &args[ndx].o);
      if (err) {
         // Queue user-side cleanup of remaining output objects
         me->cleanupArgs = argsUser + ndx;
         me->cleanupNum = ndxEnd - ndx;
         result = (err == Object_ERROR_KMEM ? err : Object_ERROR_UNAVAIL);

         // Release any kernel-side objects we have created so far
         while (--ndx >= ndxStart) {
            Object_RELEASE_IF(args[ndx].o);
         }

         // NULLing object args in error cases is not required by the invoke
         // contract, but it will help catch invalid dereferences of objects
         // (but maybe mask other programming errors, like depending on NULL
         // in this case).
         ZERO_RANGE(args+ndxStart, ndxEnd - ndxStart);
         break;
      }
   }

 done:
   pbi->result = result;
   return;
}


// If any user-side release operations are required for cleanup (after
// object marshaling in marshalOut) prepare the AcceptHeader and return
// true.
//
static bool KProcess_marshalOutCleanup(KProcess *me)
{
   if (me->cleanupNum) {
      AcceptHeader *hdr = me->acceptHeader;
      hdr->obj = me->cleanupArgs[--me->cleanupNum].o;
      hdr->op = Object_OP_release;
      hdr->k = 0;
      return true;
   }
   return false;
}

static KThreadSyscallResult KProcess_reaccept(void *arg); 


// Accept an incoming invocation and return pertinent information to the
// caller.  buf[] is a range of memory that will hold the resulting data.
//
// Terminology:
//
//   "Inside" = the environment into which the invocation is going.
//   "Outside" = the environment from which the invocation is coming.
//
// NOTE: we place pointers in the result buffer, assuming that `accept`
// will only be called in a simple U->K case (or in-process). It should
// be apparent to any client of `accept` that it cannot safely be called
// in a U->U scenario.
//
static int KProcess_accept(KProcess *me)
{
   QNode *pqn;
   int result;

   result = me->acceptHeader->result;

   do {
      // send response to previous invocation

      if (me->isResponsePending) {
         // release outbound slots referenced created during marshalIn()
         KProcess_marshalInCleanup(me);

         me->isResponsePending = false;

         pqn = QList_getFirst(&me->qlInvokers);
         kassert(pqn);
         if (pqn) {
            Invoker *pi = containerOf(pqn, Invoker, qn);
            KProcess_marshalOut(me, pi, result);

            // unblock the caller
            (void) QList_pop(&me->qlInvokers);
            Invoker_onCompletion(pi);
         }
      }

      // Any cleanup after marshalOut?
      if (KProcess_marshalOutCleanup(me)) {
         return Object_OK;
      }

      // wait for request

      while (NULL == (pqn = QList_getFirst(&me->qlInvokers))) {
         if (me->isShutdown) {
            return Object_ERROR_ABORT;
         }
		 KThread_asynchWait(KProcess_reaccept, me);
		 return Object_OK;
      }

      me->isResponsePending = true;
      Invoker *pi = containerOf(pqn, Invoker, qn);
      result = KProcess_marshalIn(me, pi);

      // Marshaling errors cause the incoming `invoke()` to fail; they don't
      // cause `accept()` to fail.

   } while (result);

   return Object_OK;
}

static KThreadSyscallResult KProcess_reaccept(void *arg) 
{ 
  KProcess *me = arg; 
  int result = KProcess_accept(me); 
  return (KThreadSyscallResult) {result, 0}; 
}


DEFINE_SERVER_INVOKE(KProcess_server_invoke, KProcess_, KProcess *)

// Handle a fault. This is called on the kernel side of the thread that
// faulted in user mode.
//
// Here we only issue a notification callback and then suspend the thread.
// It is up to the owner of the process to decide whether to kill the
// process, debug it, etc...
//
int KThread_handleFault(void *arg)
{
   KProcess *me = (KProcess*) arg;

   if (me->faultCallback) {
      me->faultCallback(me->faultArg);
   }
   return 0;
}


void KProcess_setFaultCallback(KProcess *me, FaultCB callback, FaultCBCxt *arg)
{
   me->faultCallback = callback;
   me->faultArg = arg;

   // suspend until thread is killed
   while (KThread_wait() == 0);
}


#define RESULT_BAIL(c)  do { if (!Object_isOK((result = (c)))) goto bail; } while (0)

int KProcess_unblockInvoker(KProcess *me)
{
  QNode *pqn;

  pqn = QList_getFirst(&me->qlInvokers);
  kassert(pqn);
  if (pqn) {
    Invoker *pi = containerOf(pqn, Invoker, qn);
    // unblock the caller
    (void) QList_pop(&me->qlInvokers);
    Invoker_onCompletion(pi);
  }
  return 0;
}

// Handler for software interrupts. r1..4 are args 1..4 of minkIPC.
//
KThreadSyscallResult
KThread_handleSyscall(void *arg, intptr_t r1, intptr_t r2, intptr_t r3, intptr_t r4)
{
   KProcess * const me = (KProcess *) arg;
   const ObjectCxt h = ObjectCxt_fromInt(r1);
   const ObjectOp op = (ObjectOp) r2;
   ObjectArg * const argsUser = (ObjectArg *) (void*) r3;
   const ObjectCounts k = (ObjectCounts) r4;
   uint32_t rorMask, rorBit;
   int result;
   int ndx;
   ObjectArg args[KPROCESS_MAX_ARGS];

   // atLog("h=%p op=%d abcd=%d,%d;%d,%d", h, op, (int) ObjectCounts_numBI(k), (int) ObjectCounts_numBO(k), (int) ObjectCounts_numOI(k), (int) ObjectCounts_numOO(k));

   // Handle retain/release within the transport

   if (ObjectOp_isLocal(op)) {
      result = Object_OK;
      if (op == Object_OP_release) {
         KProcess_releaseOutboundHandle(me, h);
      } else if (op == Object_OP_retain) {
         KProcess_retainOutboundHandle(me, h);
      } else {
         result = Object_ERROR_REMOTE;
      }
      return (KThreadSyscallResult) { result, 0 };
   }

   const int argsLen = ObjectCounts_total(k);
   if (argsLen > ARRAY_LEN(args)) {
      return (KThreadSyscallResult) { Object_ERROR_MAXARGS, 0 };
   }

   // The invoked object could conceivably be removed from the object table
   // during the invocation, perhaps via KProcess_kill(). Conventional
   // reference counting conventions protect us here.

   const Object obj = KProcess_getOutboundTarget(me, h);
   if (Object_isNull(obj)) {
      return (KThreadSyscallResult) { Object_ERROR_BADOBJ, 0 };
   }

   // Marshal arguments: construct args[] from argsUser[]

   KMemLock lock;
   KMemLock_construct(&lock, &me->space);
   ZERO_RANGE(args, argsLen);

   // Now that args[] is initialized, `goto bail` is safe.  No bails above here...

   // Validate that the specified pointer to the argsUser array is valid
   if (argsLen != 0) {
      RESULT_BAIL(KMemLock_lockRW(&lock, argsUser, argsLen * sizeof(*argsUser)));
   }

   FOR_ARGS(ndx, k, BI) {
      ObjectBuf b = argsUser[ndx].b;
      RESULT_BAIL(KMemLock_lockR(&lock, b.ptr, b.len));
      args[ndx].b = b;
   }

   FOR_ARGS(ndx, k, BO) {
      ObjectBuf b = argsUser[ndx].b;
      RESULT_BAIL(KMemLock_lockW(&lock, b.ptr, b.len));
      args[ndx].b = b;
   }

   FOR_ARGS(ndx, k, OI) {
      RESULT_BAIL(KProcess_copyObjectFromUser(me, argsUser[ndx].o, false, &args[ndx].o));
   }

   result = Object_invoke(obj, op, args, k);

   // create/recover user object and release kernel object

   if (Object_isOK(result)) {
      int ndxOO = ObjectCounts_indexOO(k);
      int numOO = ObjectCounts_numOO(k);
      int err = KProcess_copyObjectsToUser(me, args, argsUser, ndxOO, numOO);

      FOR_ARGS(ndx, k, OO) {
         Object_RELEASE_IF(args[ndx].o);
      }

      if (!Object_isOK(err)) {
         // release any user objects we allocated with K2U
         FOR_ARGS(ndx, k, OO) {
            KProcess_releaseIfOutboundObject(me, argsUser[ndx].o);
         }
         result = err;
      }
   }

 bail:

   // bit mask of input objects to be retained on return
   rorMask = 0;
   rorBit = 1;

   // Release objects we have allocated, and set input objects to NULL
   // unless we take ownership.
   FOR_ARGS(ndx, k, OI) {
      bool retainOnReturn = false;
      Object o = args[ndx].o;
      if (isForwarder(o) && asForwarder(o)->proc == me) {
         // Forwarder is in non-owning state. If someone else how holds a
         // reference, make it owning and `retain` the user object on return
         // to user mode.
         retainOnReturn = (asForwarder(o)->refs > 1);
         Forwarder_own(asForwarder(o), retainOnReturn);
         if (retainOnReturn) {
            rorMask |= rorBit;
         }
      }
      rorBit <<= 1;
      Object_RELEASE_IF(o);
   }

   KMemLock_destruct(&lock);

   Object_release(obj);

   return (KThreadSyscallResult) { result, rorMask };
}


#define MAXSIZE   (SIZE_T_MAX / 4)


// Returns:  K_OK on success
//           K_ERROR on failure
//
int KProcess_start(KProcess *me)
{
   if (!me->thread) {
      return K_ERROR;
   }

   KThread_setArg(me->thread, me->acceptHeader);
   
   KThread_start(me->thread);
   return K_OK;
}


// "killing" a process terminates its execution and releases resources held
// by the process.
//
int KProcess_kill(KProcess *me, bool isGraceful)
{
   int ndx;
   if (!me->thread) {
      return K_ERROR;
   }

   // prevent new invocations
   QNode *pqn, *pqnNext;
   QLIST_NEXTSAFE_FOR_ALL(&me->qlForwarders, pqn, pqnNext) {
      Forwarder_detach( containerOf(pqn, Forwarder, qnForwarders) );
   }

   // prevent the server thread from being blocked on inbound connections
   me->isShutdown = true;
   if (isGraceful) {
      // give process a chance to successfully process pending invocations
      KThread_signal(me->thread);
      (void) KThread_join(me->thread);
   } else {
      // fail all pending invocations
      (void) KThread_kill(me->thread);
   }

   // ensure all pending invokers are processed
   me->acceptHeader->result = Object_ERROR_DEFUNCT;
   while (KProcess_accept(me) == Object_OK) {
      // nothing
   }

   KThread_delete(me->thread);
   me->thread = 0;

   // clean object table
   for (ndx = 0; ndx < me->objectsLen; ++ndx) {
      Object o = me->objects[ndx];
      if (!Object_isNull(o)) {
         Object_release(o);
         Object_setNull(me->objects[ndx]);
      }
   }

   return K_OK;
}


// deinit() always "succeeds".
//
static void KProcess_delete(KProcess *me)
{
   KProcess_kill(me, true);

   HEAP_FREE_PTR(me->objects);
   HEAP_FREE_PTR(me->objectRefs);
   me->objectsLen = 0;

   KSpace_destruct(&me->space);

   HEAP_FREE_PTR(me);
}


void KProcess_release(KProcess *me)
{
   if (--me->refs == 0) {
      KProcess_delete(me);
   }
}


void KProcess_retain(KProcess *me)
{
   ++me->refs;
}


#if 0
static int KProcess_invoke(ObjectCxt h, ObjectOp op, ObjectArg *a, ObjectCounts counts)
{
   KProcess *me = (KProcess *) h;

   switch (op) {
   case Object_OP_release:
      KProcess_release(me);
      break;

   case Object_OP_retain:
      KProcess_retain(me);
      break;

   default:
      return Object_ERROR_INVALID;
   }
   return Object_OK;
}
#endif

#define USERSTACK_DEFAULT  8192


KProcess *KProcess_new(int maxObjects, void *acceptBuf, size_t acceptBufSize)
{
   KProcess *me;
   BBuf bb;
   Object oServer;
   Object oUser = Object_NULL;
   
   oServer = Object_NULL;
   
   me = HEAP_ZALLOC_REC(KProcess);
   if (!me) {
      return NULL;
   }


   BBuf_construct(&bb, acceptBuf, acceptBufSize);
   me->acceptHeader = BBuf_ALLOC_REC(&bb, AcceptHeader);
   if (!me->acceptHeader) {
      goto bail;
   }
   me->acceptBuffer = bb;



   me->refs = 1;
   QList_construct(&me->qlInvokers);
   QList_construct(&me->qlForwarders);
   me->userStackSize = USERSTACK_DEFAULT;

   // operations that can fail

   me->objects = HEAP_ZALLOC_ARRAY(Object, maxObjects);
   if (!me->objects) {
      goto bail;
   }
   me->objectsLen = maxObjects;

   me->objectRefs = HEAP_ZALLOC_ARRAY(unsigned, maxObjects);
   if (!me->objectRefs) {
      goto bail;
   }

   // Note: failure of newOutboundObject() will cause server not to
   // function but not corrupt kernel memory.
   oServer.invoke = KProcess_server_invoke;
   oServer.context = me;
   
   oUser = KProcess_newOutboundObject(me, oServer);
   me->acceptHeader->server = oUser;
   
   KSpace_construct(&me->space);

   return me;

 bail:
   KProcess_releaseIfOutboundObject(me, oUser);
   KProcess_delete(me);
   return NULL;
}

void KProcess_attachThread(KProcess *meProcess, KThread *meThread)
{
   meProcess->thread = meThread;  // the process now holds the reference on the thread
}
