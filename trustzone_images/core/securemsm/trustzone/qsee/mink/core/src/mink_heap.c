/*
@file tzbsp_sys.c
@brief Trustzone System Services

Functions for system/processor level functionality.

*/
/*===========================================================================
   Copyright (c) 2010 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsee/mink/core/src/mink_heap.c#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $


when       who      what, where, why
--------   ---     ------------------------------------------------------------
06/01/12   sg      Add tzbsp_mem_is_tagged_area stub
05/19/10   tk      Initial version.
=============================================================================*/

/*=============================================================================

                            INCLUDE FILES FOR MODULE

=============================================================================*/
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <comdef.h>
#include "mink_scm_errno.h"     
#include "platform.h"                       



/*=============================================================================

            LOCAL DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/

/*=============================================================================

                              FUNCTION DEFINITIONS

=============================================================================*/

#if 0
/* @copydoc tzbsp_abort */
void tzbsp_abort(const char* fmt, ...)
{
  volatile int wait = 1;
  va_list ap;

  va_start(ap, fmt);
  tzbsp_log_with_ap(TZBSP_MSG_FATAL, fmt, ap);
  va_end(ap);

  /* Start waiting in an infinite loop. */
  while(wait);
}
#endif


extern uint32  Image$$TZBSP_DAL_HEAP$$Base;
extern uint32  Image$$TZBSP_DAL_HEAP$$Length;


/* @copydoc tzbsp_heap_init */
void tzbsp_heap_init(void)
{
	if(MINK_E_SUCCESS !=platform_heap_init((void *) &Image$$TZBSP_DAL_HEAP$$Base, (uint32) &Image$$TZBSP_DAL_HEAP$$Length))
	{
		//handle it; TODO
	}
}

/* @copydoc tzbsp_malloc */
void* tzbsp_malloc(uint32 size)
{
	return platform_heap_mem_malloc(size);
}

/* @copydoc tzbsp_free */
void tzbsp_free(void* ptr)
{
	platform_heap_mem_free(ptr);
}


