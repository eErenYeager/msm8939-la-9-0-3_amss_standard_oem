// A place for mink code.
// TODO relocate code to correct files (if needed)

#include "mink_threads.h"
//#include "mink_target.h"
#include "tzbsp_target.h"

service_thread_t threadpool[NUM_THREADS_IN_KERNEL_THREAD_POOL];
uint32 service_thread_stack_size[NUM_THREADS_IN_KERNEL_THREAD_POOL] = {PRIMARY_SERVICE_THREAD_STACK_SIZE, SECONDARY_SERVICE_THREADS_STACK_SIZE};
uint8 thread1_stack[PRIMARY_SERVICE_THREAD_STACK_SIZE];
uint8 thread2_stack[SECONDARY_SERVICE_THREADS_STACK_SIZE];
uint8* thread_stacks[NUM_THREADS_IN_KERNEL_THREAD_POOL] = {thread1_stack, thread2_stack};

static struct list_node thread_list = LIST_INITIAL_VALUE(thread_list);

static int thread_entry(void *arg)
{
  int i = 0;
  service_thread_t *pservice_thread = NULL;

  while(1)
  {
    event_wait_on_default_event();

    pservice_thread = NULL;
    for (i = 0; i < NUM_THREADS_IN_KERNEL_THREAD_POOL; i++)
    {
      if (&threadpool[i].thread == current_thread) {
        pservice_thread = &threadpool[i];
        break;
      }
    }

    if (pservice_thread) {
      pservice_thread->thread.busy_flag = 1;
      pservice_thread->thread.retcode = 0;
      pservice_thread->thread.retcode = pservice_thread->cb(
          (void*)pservice_thread->args[0], (void*)pservice_thread->args[1],
          pservice_thread->args[2], pservice_thread->args[3]);
      pservice_thread->thread.busy_flag = 0;
    }
  }
}

void mink_init_threads()
{
  int i = 0;
  thread_t* pthread = NULL;

  for (i = 0; i < NUM_THREADS_IN_KERNEL_THREAD_POOL; i++) {
    pthread = thread_create("service", &thread_entry, NULL, DEFAULT_PRIORITY,
                            &threadpool[i].thread, service_thread_stack_size[i],
                            (void*)thread_stacks[i], 0, NULL, 1, 1, NULL, NULL);
    if(pthread){
      thread_resume(pthread);
    }
  }
}

int mink_queue_thread(thread_callback cb, void *arg1, void *arg2, uint32_t arg3, uint32_t arg4)
{
  uint32 i = 0;
  service_thread_t *pservice_thread = NULL;

  enter_critical_section();
  pservice_thread = NULL;
  for (i = 0; i < NUM_THREADS_IN_KERNEL_THREAD_POOL; i++)
  {
    if (!threadpool[i].thread.busy_flag) {
      pservice_thread = &threadpool[i];
      break;
    }
  }
  exit_critical_section();

  if (pservice_thread) {
    pservice_thread->cb = cb;
    pservice_thread->args[0] = (uint32)arg1;
    pservice_thread->args[1] = (uint32)arg2;
    pservice_thread->args[2] = arg3;
    pservice_thread->args[3] = arg4;

    event_signal_default_event(&(pservice_thread->thread), true);
    return pservice_thread->thread.retcode;
  } else {
    return ~0;
  }
}


boolean mink_thread_busy()
{
  return threadpool[0].thread.busy_flag;
}

boolean mink_thread_blocked_on_listener()
{
  return (threadpool[0].thread.blocked_on_listner_id != 0xFFFFFFFF);
}


