/*
 * Copyright (c) 2008 Travis Geiselbrecht
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#include "lktypes.h"
#include "lkcompiler.h"
#include <string.h>
#include "lkplatform.h"
#include "lkthread.h"
#include "lktimer.h"
#include "lkdpc.h"
#include "mink_threads.h"
#include "mink_errno.h"
#include "mink_scm_errno.h"
#include "platform.h"
#include "tzbsp_target.h"

unsigned int qsee_data_fault_status;
unsigned int qsee_data_fault_addr;
unsigned int qsee_instr_fault_status;
unsigned int qsee_instr_fault_addr;

extern void oem_init(void);

extern void platform_kernel_init(void);

extern void qsee_initialize_listener_table(void);

/* Called when stack protection fails, detecting a corrupt stack*/
void __stack_chk_fail(void)
{
#ifdef TZBSP_WITH_TEST_SVC
  TZBSP_ERROR_CODE(STACK_CHECK_FAILED);
#else
  TZBSP_ERROR_CODE(STACK_CHECK_FAILED);
  tzbsp_loop_here();
#endif
}

int kernel_init(void)
{

  oem_init();

  qsee_initialize_listener_table();

  platform_kernel_init();

  // get us into some sort of thread context
  thread_init_early();

  mink_init_threads();

  timer_init();

  // enable interrupts
  //exit_critical_section();

  //qsee_app_init_global();

  // become the idle thread
  thread_become_idle();

  //TZBSP_LOG_DBG("QSEE version major=%d, minor=%d", QSEE_VERION_MAJOR, QSEE_VERION_MINOR);
  TZBSP_LOG_DBG("Mink kernel inited");

  return 0;
}


int qsee_signal_thread(thread_callback cb, void *arg1, void *arg2, unsigned int arg3, uint32 arg4)
{
  return mink_queue_thread(cb, arg1, arg2, arg3, arg4);
}

/*
 * qsee_do_syscall
 *
 * There is a pool of kernel threads kept by mink.  The primary thread (first thread) in the pool has
 * sufficient stack size to handle any syscall (either by executing fully within mink in its own thread,
 * or by invoking a module).   The secondary threads in the pool may or may not be able to service
 * any arbitrary syscall.
 *
 * When a syscall comes in and the primary thread is available, we can service it on the primary thread.
 *
 * If the primary thead is busy, we call into the platform to determine if the syscall can be handled on
 * a secondary mink thread.  This decision is based on stack size and concurrency limitations.
 *
 */
int qsee_do_syscall(thread_callback cb, void *arg1, void *arg2, unsigned int arg3, uint32 arg4, int syscall_id)
{
  if (!mink_thread_busy())
  {
    /*Normal case - The main mink thread is ready to service a call*/
    return qsee_signal_thread(cb, arg1, arg2, arg3, arg4);
  }
  else
  {
    /*Main mink thread is busy, let platform decide if we should service the call on a secondary thread*/
    if(platform_check_invoke_syscall(arg1, arg2, arg3, arg4, syscall_id))
    {
      /*Service the call on a secondary thread*/
      return qsee_signal_thread(cb, arg1, arg2, arg3, arg4);
    }
    else
    {
      /* Cannot service the call now, caller should retry operation*/
      TZBSP_LOG_ERR("{%d: %x}", TZBSP_EC_SYSCALL_DENIED_DPC_BLOCKED, syscall_id);
      return -MINK_E_BUSY;
    }
  }
}

/*uint64 for stacks for 8-byte alignment*/
uint64_t kprocess_kernel_stack[PROCESS_NUM_KERNEL_STACKS][PROCESS_KERNEL_STACK_SIZE/sizeof(uint64_t)];
uint32_t kprocess_kernel_stack_in_use[PROCESS_NUM_KERNEL_STACKS];

uint32_t* mink_allocate_kprocess_kernel_stack(void)
{
  uint32_t i;
  for (i=0; i < PROCESS_NUM_KERNEL_STACKS; i++)
  {
    if (!kprocess_kernel_stack_in_use[i]) 
	{
	  kprocess_kernel_stack_in_use[i] = 0x1;
	  break;
	}
  }
  
  if (i >=  PROCESS_NUM_KERNEL_STACKS)
  {
    return NULL;
  }
  
  return (uint32_t *)&kprocess_kernel_stack[i][PROCESS_KERNEL_STACK_SIZE/sizeof(uint64_t)];
  
}

void mink_release_kprocess_kernel_stack(uint64_t *sp)
{
  uint32_t i;
  for (i=0; i < PROCESS_NUM_KERNEL_STACKS; i++)
  {
    if (kprocess_kernel_stack_in_use[i]) 
	{
	  if (sp == &kprocess_kernel_stack[i][PROCESS_KERNEL_STACK_SIZE/sizeof(uint64_t)]) 
	  {
	    kprocess_kernel_stack_in_use[i] = 0;
		break;
	  }
	}
  }
}

