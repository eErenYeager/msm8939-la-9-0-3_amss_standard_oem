	
#define _POSIX_C_SOURCE 999901L
#include <stdio.h>
#include "lktypes.h"
#include "heap.h"
#include "kthread.h"
#include "kdefs.h"
#include "lkthread.h"
#include "tzbsp_target.h"

//------------------------------------------------------------------------


KThread *KThread_new(const char *name,
                     KThreadStartRoutine start,
                     void *startArg,
                     int priority,
                     size_t stackSize,
                     void *stack,
                     uint32 app_id,
                     bool startInUserMode)
{
   KThread *me = HEAP_ZALLOC_REC(KThread);
   if (!me) {
      return NULL;
   }
   
   if (thread_create(name, start, startArg, priority, me, stackSize, stack,
                     app_id, NULL, startInUserMode?0:1, 0, NULL, NULL) != me) {
     HEAP_FREE_PTR(me);
     return NULL;
   } else  {
     return me;
   }
}

// __start and __exit are called from "outside" the thread
//
extern void mink_release_kprocess_kernel_stack(uint64_t *sp);

void KThread_delete(KThread *me)
{
   //thread_cleanup(me);
   uint8 *stack;
   thread_cleanup(me);
   if (me->exception_sp) {
     mink_release_kprocess_kernel_stack(me->exception_sp);
   }
   HEAP_FREE_PTR(me);
}

#if 0
static void acquireLock(KThread *me)
{
#if 0
   pthread_mutex_lock(&g_excl);
   g_currentThread = me;
   KLOG("----------------------------------------------------------------: %d\n", me->id);
#endif   
}

static KThread *releaseLock(void)
{
#if 0
   KThread *me = g_currentThread;
   g_currentThread = NULL;
   qt_assert(me);
   KLOG("<---- %d\n", me->id);
   pthread_mutex_unlock(&g_excl);
   return me;
#endif   
}
#endif

void KThread_start(KThread *me)
{
  thread_resume(me);
}

int KThread_join(KThread *me)
{
  return 0;
}


int KThread_kill(KThread *me)
{
  return 0;
}


void KThread_signal(KThread *me)
{
   event_signal_default_event(me, TRUE);
}


KThread *KThread_getCurrent(void)
{
  return get_current_thread();
}

void KThread_asynchWait(KThreadSyscallResult (*pfn)(void*), void *arg) 
{ 
  KThread *me = KThread_getCurrent(); 
  me->asynchFunc = pfn; 
  me->asynchArg = arg;
}

int KThread_wait(void)
{
#if 0
   KThread *me = g_currentThread;
   qt_assert(me);

   KLOG("T%d wait\n", me->id);

   while (! (me->bit || me->isAborting)) {
      KLOG("T%d wait: bit=%d isAborting=%d\n",
             me->id, me->bit, me->isAborting);
      qt_assert(me == g_currentThread);
      kyield();
      qt_assert(me == g_currentThread);
   }
   g_yieldCount = 0;

   KLOG("T%d wait DONE\n", me->id);
   qt_assert(me == g_currentThread);
   me->bit = 0;
   return me->isAborting;
#endif
  return event_wait_on_default_event();
 
}


void KThread_exit(void)
{
  thread_exit(0);
}


void
KThread_fault(void)
{
#if 0
   KLOG("KThread_fault\n");

   KThread *me = g_currentThread;
   qt_assert(me);
   qt_assert(me->isInUserMode);

   me->isInUserMode = false;

   KThread_handleFault(me->handlerArg);

   me->isInUserMode = true;
 #endif
}

bool KThread_IsFaulted(KThread *me)
{
  return thread_is_suspended(me);
}

void *KThread_getCurrentProcess(void)
{
   return ((get_current_thread())->process);
}

KThreadSyscallResult
KThread_RouteSyscall(intptr_t r1, intptr_t r2, intptr_t r3, intptr_t r4)
{
   return KThread_handleSyscall(KThread_getCurrentProcess(), r1, r2, r3, r4);
}

extern void kernel_init(void);
void KThread_init(void (*start)(void*), void *startArg)
{
   kernel_init();
}

extern uint32_t* mink_allocate_kprocess_kernel_stack(void);

void KThread_attachHandler(void *handler, KThread *meThread)
{
   uint8 *stack;
   meThread->process = handler;
   stack = (uint8_t *)mink_allocate_kprocess_kernel_stack();
   /*TODO: need to handle failure case*/
   thread_set_exception_stack_ptr(meThread, stack);
}

void KThread_setArg(KThread *meThread, void *arg)
{
   meThread->arg = arg;
}
