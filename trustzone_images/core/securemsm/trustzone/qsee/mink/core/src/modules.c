/*============================================================================
Modules

ABSTRACT
   modules management in Mink

DESCRIPTION
   


Copyright (c) 2011 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
=========================================================================== */

/*=========================================================================
                              Edit History

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsee/mink/core/src/modules.c#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $


when       who     what, where, why
--------   ---     --------------------------------------------------------
03/10/12   spa     Initial Revision

=========================================================================== */

#include "mink_modules.h"
#include "mink_services.h"
#include "mmu.h"
#include "kprocess.h"
#include "kthread.h"
#include "lkthread.h"

extern const void *mink_modules_start;
extern const void *mink_modules_end;

Object mink_opener_obj_table[3];
extern mink_service_table_entry_t mink_service_table[];

extern Object mink_service_obj_lookup_for_module(uint32 service_id);

extern Object mink_api_service_obj;

extern Object mink_service_syscall_obj;

static int mink_opener_invoke(ObjectCxt ptr, ObjectOp op, ObjectArg *args, ObjectCounts k)
{
  uint32 service_id;
  Object objnull =  { NULL, NULL };

  if ((op == Object_OP_retain) || (op == Object_OP_release)) {
    return 0 ;
  }

  if (k != ObjectCounts_pack(1, 0, 0, 1)) {
    return -1;
  }
  
  if (op != MINK_CMD_OPEN) {
    args[1].o = objnull;
    return -1;
  }

  if ((args[0].b.ptr) && (args[0].b.len == 4)) {
    service_id = *((uint32 *)args[0].b.ptr);

    switch (service_id) {
      case MINK_SERVICE_SYSCALL_ID:
        /*If this service is being opened from user app (only use apps have process) then do not allow*/
        if (KThread_getCurrentProcess()) {
          args[1].o = objnull;
        } else {
          args[1].o = mink_service_syscall_obj;
        }
        break;
      case MINK_SERVICE_KERNEL_API_ID:
        args[1].o = mink_api_service_obj;
        break;
      case MINK_SERVICE_IMG_AUTH_ID:
        args[1].o = objnull;
        break;
       case MINK_SERVICE_KERNEL_MEM_MGMT_ID:
         args[1].o = objnull;
        break;
      default:
        args[1].o = objnull;
        break;
    }
  }
    return 0;
}

Object mink_opener = {mink_opener_invoke, NULL};

const static struct
{
  const  void **start;
  const  void **end;
} mink_sections[] =
              { {&mink_modules_start,    &mink_modules_end },
                {NULL,    NULL },
              };


extern int serverLoop(void *cxt);
int mink_initialize_modules(void)
{
  ObjectArg a[2];
  KProcess *pProc = NULL;
  KThread *pThread = NULL;

  struct mink_module *cur = NULL;
  struct mink_module *start = NULL;
  struct mink_module *end = NULL;

  Object ObjNull = {NULL, NULL};
  Object ObjInit = {NULL, NULL};
  Object ObjUserInit = {NULL, NULL};
  uint32 j = 0, table_idx = 0;
  uint32 i = 0;
  uint32 module_opener_object_table_index = 0;


  /*Map the shared code region*/
  //This is done in default mappings now. 
//  qsee_map_region((unsigned int)&Image$$MINK_SHARED_CODE_RGN$$Base, (unsigned int)&Image$$MINK_SHARED_CODE_RGN$$Base, 
//    (unsigned int)&Image$$MINK_SHARED_CODE_RGN$$Length, PERM_R | PERM_X, TZ_MEM_WRITE_BACK_CACHE_S, 1);

  while (mink_sections[i].start != NULL &&
         mink_sections[i].end != NULL)
  {
    start = *(struct mink_module **)mink_sections[i].start;
    cur = start;
    end = *(struct mink_module **)mink_sections[i].end;

    if ((uint32)end - (uint32)start < sizeof(struct mink_module))
    {
      i++;
      continue;
    }

    while ((uint32)cur < (uint32)end)
    {
      if (cur->priv_level == MINK_PRIV_LEVEL_KERNEL)
      {
        if( (unsigned int)cur->code_size)
        {
          /* Map the code and data regions for this module*/
          qsee_map_region((unsigned int)cur->code_base, (unsigned int)cur->code_base, 
            (unsigned int)cur->code_size, PERM_R | PERM_X, TZ_MEM_WRITE_BACK_CACHE_S, cur->priv_level);
          }
        if( (unsigned int)cur->data_size)
        {     
          qsee_map_region((unsigned int)cur->data_base, (unsigned int)cur->data_base, 
            (unsigned int)cur->data_size, PERM_R | PERM_W, TZ_MEM_WRITE_BACK_CACHE_S, cur->priv_level);
        }
      }

      for (j = 0; j < cur->num_services; j++) {
        mink_service_table[table_idx].service_id = cur->services[j];
        mink_service_table[table_idx].module_idx = module_opener_object_table_index;
        mink_service_table[table_idx++].obj = ObjNull;
      }

      if (cur->priv_level == MINK_PRIV_LEVEL_USER)
      {
        pProc = KProcess_new(cur->maxObjs, cur->acceptBuf,  cur->acceptBufSize);
        if (!pProc) {
          return -1;
        }
      
        pThread = KThread_new(cur->name, serverLoop,  NULL, HIGHEST_PRIORITY,
                              cur->stack_size, cur->stack_base, 0, TRUE);
        if (!pThread) {
          return -1;
        }
        
        pThread->segs[0].addr = (unsigned int)cur->code_base;
        pThread->segs[0].vaddr = (unsigned int)cur->code_base;
        pThread->segs[0].size = (unsigned int)cur->code_size;
        pThread->segs[0].flags = PERM_R | PERM_X;
        
        pThread->segs[1].addr = (unsigned int)cur->data_base;
        pThread->segs[1].vaddr = (unsigned int)cur->data_base;
        pThread->segs[1].size = (unsigned int)cur->data_size;
        pThread->segs[1].flags = PERM_R | PERM_W;       
        
        KProcess_attachThread(pProc, pThread);
        KThread_attachHandler(pProc, pThread);
        
        KProcess_start(pProc);
      }
      
      if (cur->priv_level == MINK_PRIV_LEVEL_USER) {
        ObjUserInit.invoke = cur->init_invoke;
        ObjUserInit.context = NULL;
        ObjInit = KProcess_newInboundObject(pProc, ObjUserInit);
      } else {
        ObjInit.invoke = cur->init_invoke;
        ObjInit.context = NULL;
      }
      a[0].o = mink_opener;   
      Object_invokeABCD(ObjInit, MINK_CMD_INIT, a, 0, 0, 1, 1);
      mink_opener_obj_table[module_opener_object_table_index++] = a[1].o;     
      
      //release the initial invoke object
      Object_release(ObjInit);

      cur = (struct mink_module*)((uint8*)cur +
          (sizeof(struct mink_module) + (sizeof(uint32) * cur->num_services)));
    }
    ++i;
  }
  
  return 0;

}

