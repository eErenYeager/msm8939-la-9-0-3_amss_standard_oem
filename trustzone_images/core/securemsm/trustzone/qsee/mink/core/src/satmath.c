/*
 * Copyright (c) 2016  Qualcomm Technologies, Inc.  All Rights Reserved.
 * Qualcomm Technologies Proprietary and Confidential.
 */

#include "satmath.h"
#include <stdint.h>

size_t satMul(size_t a, size_t b)
{
  if (b > 0 && a > SIZE_MAX / b) {
    return SIZE_MAX;
  }
  return a * b;
}

size_t satAdd(size_t a, size_t b)
{
  if (a >= SIZE_MAX - b) {
    return SIZE_MAX;
  }
  return a + b;
}

uint32_t u32SatAdd(uint32_t a, uint32_t b)
{
  if (a >= UINT32_MAX - b) {
    return UINT32_MAX;
  }
  return a + b;
}
