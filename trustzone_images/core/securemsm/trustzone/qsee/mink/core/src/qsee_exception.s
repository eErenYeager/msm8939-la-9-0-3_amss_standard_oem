#include "syscall.h"
    
    PRESERVE8

    EXPORT  qsee_swi_handler
    EXPORT  qsee_undef_handler
    EXPORT  qsee_dabort_handler
    EXPORT  qsee_pabort_handler
    IMPORT  qsee_data_fault_status
    IMPORT  qsee_data_fault_addr
    IMPORT  qsee_instr_fault_status
    IMPORT  qsee_instr_fault_addr
    IMPORT  qsee_handle_page_fault
    EXPORT  qsee_get_request_info
    EXPORT  qsee_save_thread_regs
    IMPORT  qsee_handle_app_request
    IMPORT  entry_cpsr
    EXPORT  qsee_apps_svc_stack_end
    EXPORT  qsee_apps_svc_stack_guard
    IMPORT  thread_get_curr_exception_sp
    IMPORT  thread_current_vfp_enable	
    IMPORT  mon_save_ns_vfp_context

    AREA    qsee_exception_asm, CODE, ALIGN=8
    CODE32

#define QSEE_SVC_STACK_SIZE   0x100

#define T_ADV_SIMD_DATA_PROCESSING 0xEF000000
#define A_ADV_SIMD_DATA_PROCESSING 0xF2000000

#define TA_VFP_DATA_PROCESSING 0xE000A00

#define TA_VFP_LOAD_STORE 0xC000A00

#define T_SIMD_LOAD_STORE 0xF9000000
#define A_SIMD_LOAD_STORE 0xF4000000

#define TA_32_TRANSFER 0xE000A00

#define TA_64_TRANSFER 0xC400A00

#define TA_CP_INST_1 0xEC000A00
#define TA_CP_INST_2 0xFC000A00

#define ARM_T32_MODE_BIT 0x20

qsee_swi_handler
    /* r0 contains the syscall number. Do not modify.
     * rest of the parameters are in r4-r12
     */
    mrs     r2, spsr
    ldr     r1, =exception_cpsr
    str     r2, [r1]        /*Save exception cpsr*/
    sub     r1, r1, #4      /*Make r1 point to use pc*/
    str     r14, [r1]        /*Save exception pc*/
    stmdb   r1, {r4 - r14}^  /*Do not save r0-r3*/

    ldr     r1, =qsee_apps_svc_stack_start
    sub     r1, r1, #8
    mov     r13, r1

    ldr     r2, =app_init_swi_num
    ldr     r2, [r2]
    cmp     r2, r0
    beq     done_set_exception_sp
    mov     r4,  r0   ; backup r0
    blx     thread_get_curr_exception_sp
    mov     r13, r0
    mov     r0,  r4   ; restore r0
done_set_exception_sp	
    mrs     r2, cpsr        /* Get current cpsr and adjust I & F */
    ldr     r1, =entry_cpsr  /* based on entry cpsr*/
    ldr     r1, [r1]
    and     r1, #0xC0       /* Mask all bits except I, F*/
    and     r2, #(~0xC0)    /* Mask I, F*/
    orr     r2, r1
    msr     cpsr_c, r2        /*Update CPSR*/
    ldr     r1, =qsee_handle_app_request
    blx     r1

qsee_get_request_info
    push    {r4-r8, r14}
    ldr     r2, =exception_regs
    add     r2, r2, #0x10  /*Make it point to r4 of saved exc regs */
copy_loop
    ldmia     r2!, {r3}
    stmia     r0!, {r3}
    sub     r1, r1, #1
    cmp     r1, #0
    bne     copy_loop
    /*stmia   lr, {r0 - r3}^*/
    pop     {r4-r8, pc}

/*r0 points to thread->arch-regs*/
qsee_save_thread_regs
    ldr     r1, =exception_regs
    mov     r2, #17   /*Need to save 17 regs*/
save_loop
    ldmia   r1!, {r3}
    stmia   r0!, {r3}
    sub     r2, r2, #1
    cmp     r2, #0
    bne     save_loop
    bx      r14

qsee_undef_handler
    b       qsee_undefined_handler
qsee_dabort_handler
    str     r14, [r13]   /*Save LR onto stack*/
    mrs     r14, spsr                 /* Check if fault is from user mode*/
    ands    r14, r14, #0xF            /* check for mode bits 3:0 == 0x0 user mode */
    beq     qsee_exception_handler
    mrc     p15, 0, r14, c5, c0, 0 /*DFSR*/
    str     r14, [r13, #0x4]
    mrc     p15, 0, r14, c6, c0, 0 /*DFAR*/
1   b       %b1
qsee_pabort_handler
    str     r14, [r13]   /*Save LR onto stack*/
    mrs     r14, spsr                 /* Check if fault is from user mode*/
    ands    r14, r14, #0xF            /* check for mode bits 3:0 == 0x0 user mode*/
    beq     qsee_exception_handler
    mrc     p15, 0, r14, c5, c0, 1 /*IFSR*/
    str     r14, [r13, #0x4]
    mrc     p15, 0, r14, c6, c0, 2 /*IFAR*/
1   b       %b1

; Remove pager entry for TZ.BF.3.0, since no demand paging is used.
; The above functions will be friendlier for debugging.
#if 0
qsee_undef_handler
    str     r14, [r13]    /*Save L4 onto stack*/
    b       qsee_exception_handler
qsee_dabort_handler
    str     r14, [r13]   /*Save L4 onto stack*/
    mov     r14, #0x0   /*Indicate data abort*/
    str     r14, [r13, #0x4]
    b       qsee_exception_handler
qsee_pabort_handler
    str     r14, [r13]   /*Save L4 onto stack*/
    mov     r14, #0x1   /*Indicate prefetch abort*/
    str     r14, [r13, #0x4]
    b       qsee_exception_handler
#endif


qsee_undefined_handler

/* Undefined exception as a result of VFP/SIMD instruction first usage. Enable the VFP/SIMD now*/

    sub    r13, #60
    stm    r13, {r0 - r14}^ ; dump user mode registers on the stack

    sub    r13, r13, #4
    mrs    r0, spsr
    push   {r0}

    and    r0, r0, #ARM_T32_MODE_BIT
    cmp    r0, #0
    bne    from_t32
    sub    r14, r14, #2
from_t32
    sub    r14, r14, #2 ; point to instruction causing the exception
    sub    r13, r13, #4
    push   {r14}

    ; read the instruction that caused the exception
    ldr    r1, [r14]

    ; check if instruction is T32 VFP or SIMD
    cmp    r0, #0
    bne    t32_instruction

    mov32  r0, #A_ADV_SIMD_DATA_PROCESSING
    and    r0, r0, r1
    mov32  r1, #A_ADV_SIMD_DATA_PROCESSING
    cmp    r0, r1
    beq    valid_vfp_simd

    ldr    r1, [r14]
    mov32  r0, #TA_VFP_DATA_PROCESSING
    and    r0, r0, r1
    mov32  r1, #TA_VFP_DATA_PROCESSING
    cmp    r0, r1
    beq    valid_vfp_simd

    ldr    r1, [r14]
    mov32  r0, #TA_VFP_LOAD_STORE
    and    r0, r0, r1
    mov32  r1, #TA_VFP_LOAD_STORE
    cmp    r0, r1
    beq    valid_vfp_simd

    ldr    r1, [r14]
    mov32  r0, #A_SIMD_LOAD_STORE
    and    r0, r0, r1
    mov32  r1, #A_SIMD_LOAD_STORE
    cmp    r0, r1
    beq    valid_vfp_simd

    ldr    r1, [r14]
    mov32  r0, #TA_32_TRANSFER
    and    r0, r0, r1
    mov32  r1, #TA_32_TRANSFER
    cmp    r0, r1
    beq    valid_vfp_simd

    ldr    r1, [r14]
    mov32  r0, #TA_64_TRANSFER
    and    r0, r0, r1
    mov32  r1, #TA_64_TRANSFER
    cmp    r0, r1
    beq    valid_vfp_simd

    ldr    r1, [r14]
    mov32  r0, #TA_CP_INST_1
    and    r0, r0, r1
    mov32  r1, #TA_CP_INST_1
    cmp    r0, r1
    beq    valid_vfp_simd

    ldr    r1, [r14]
    mov32  r0, #TA_CP_INST_2
    and    r0, r0, r1
    mov32  r1, #TA_CP_INST_2
    cmp    r0, r1
    beq    valid_vfp_simd


t32_instruction


    ldr    r1, [r14]
    ldr    r0, [r14]
    lsr    r1, #16    
    lsl    r0, #16    
    orr    r2, r1, r0
    mov32  r0, #T_ADV_SIMD_DATA_PROCESSING
    and    r0, r0, r2
    mov32  r1, #T_ADV_SIMD_DATA_PROCESSING
    cmp    r0, r1
    beq    valid_vfp_simd


    mov32  r0, #TA_VFP_DATA_PROCESSING
    and    r0, r0, r2
    mov32  r1, #TA_VFP_DATA_PROCESSING
    cmp    r0, r1
    beq    valid_vfp_simd

    mov32  r0, #TA_VFP_LOAD_STORE
    and    r0, r0, r2
    mov32  r1, #TA_VFP_LOAD_STORE
    cmp    r0, r1
    beq    valid_vfp_simd

    mov32  r0, #T_SIMD_LOAD_STORE
    and    r0, r0, r2
    mov32  r1, #T_SIMD_LOAD_STORE
    cmp    r0, r1
    beq    valid_vfp_simd

    mov32  r0, #TA_32_TRANSFER
    and    r0, r0, r2
    mov32  r1, #TA_32_TRANSFER
    cmp    r0, r1
    beq    valid_vfp_simd

    mov32  r0, #TA_64_TRANSFER
    and    r0, r0, r2
    mov32  r1, #TA_64_TRANSFER
    cmp    r0, r1
    beq    valid_vfp_simd

    mov32  r0, #TA_CP_INST_1
    and    r0, r0, r2
    mov32  r1, #TA_CP_INST_1
    cmp    r0, r1
    beq    valid_vfp_simd

    mov32  r0, #TA_CP_INST_2
    and    r0, r0, r2
    mov32  r1, #TA_CP_INST_2
    cmp    r0, r1
    beq    valid_vfp_simd

undef_loop                  ; If it is not VFP and SIMD exception, loop indefinitely
    b      qsee_exception_handler

valid_vfp_simd

; ------------------------------------------------------------------
; Enable VFP and Neon this should cause an exception into EL3
;-------------------------------------------------------------------
  
    ; disable trapping to EL1
    mrc    p15,0,r0,c1,c0,2 ; Read CPACR into Rt
    mov    r1, #0xF<<20
    orr    r0, r0, r1
    mcr    p15,0,r0,c1,c0,2 ; Write Rt to CPACR
    isb    SY

; ------------------------------------------------------------------
; Check if anyone othet than the user app caused the VFP exception.
; ------------------------------------------------------------------

    mrs    r1, spsr
    and    r1, r1, #0x1F
    cmp    r1, #0x1<<4
    beq    no_loop

InvalidModeLoop
    b      qsee_exception_handler	; Someone other than the user caused the VFP exception

no_loop

    ; Enable VFP/SIMD
    vmrs   r1, FPEXC
    orr    r1, r1, #0x1<<30;
    vmsr   FPEXC, r1   ; enable VFP
    isb    ; make sure the VFP is enabled before saving the NS VFP context        

    bl     mon_save_ns_vfp_context
    bl thread_current_vfp_enable
    cmp    r0, #0x0
    bne    vfp_mem_allocated

/* Ran out of Heap while allocating for VFP context */	
loop_vfp_malloc_fail	; If it is not VFP and SIMD exception, loop indefinitely
    b      qsee_exception_handler	

vfp_mem_allocated	
    pop    {r14}
    add    r13, r13, #4
    pop    {r0}
    msr    spsr_und, r0 ; restore the spsr

    add    r13, r13, #4
    ldm    r13, {r0 - r14}^	; restore user registers

    add    r13, r13, #60 ; Adjust the stack pointer to the original position

    eret   ; return to the instruction causing the exception
   
;END

qsee_exception_handler
    ldr    r14, =exception_cpsr
    sub    r14, r14, #4      /*Make r14 point to use pc*/
    stmdb  r14, {r0 - r14}^
    ldr    r0, [r13]
    str    r0, [r14]         /*Save exception faulted pc*/
    mrs    r0, spsr
    str    r0, [r14, #4]     /*Save exception cpsr*/

    ldr    r1, =qsee_data_fault_status
    mrc    p15, 0, r0, c5, c0, 0   /*DFSR*/
    str    r0, [r1]
    ldr    r1, =qsee_data_fault_addr
    mrc    p15, 0, r0, c6, c0, 0   /*DFAR*/
    str    r0, [r1]
    ldr    r1, =qsee_instr_fault_status
    mrc    p15, 0, r0, c5, c0, 1   /*IFSR*/
    str    r0, [r1]
    ldr    r1, =qsee_instr_fault_addr
    mrc    p15, 0, r0, c6, c0, 2   /*IFAR*/
    str    r0, [r1]

    mrs    r14, spsr
    /* Check if fault is from user */
    ands   r14, r14, #0xF            /* check for bits 3:0 == 0x0 */
    subnes r14, r14, #0xF            /* check for bits 3:0 == 0xf */
    bne    handle_non_usr_mode_fault

    /*enter svc mode */
    msr     CPSR_c, #0xD3
    ldr     r1, =qsee_apps_svc_stack_start
    sub     r1, r1, #8
    mov     r13, r1
    ldr     r1, =qsee_handle_app_request
    mov     r0, #0
    blx     r1

/*Handle non-user mode fault. only treat svc mode faults.*/
handle_non_usr_mode_fault
    mrs     r14, spsr
    and     r14, r14, #0x1F            /* keep Mode bits 4:0 */
    cmp     r14, #0x13
    bne     exception_loop_here  /*Fault not from svc, do not handle*/

    /*Handle faults from SVC mode*/
    mrs     r14, cpsr
    and     r14, r14, #0x1F            /* keep Mode bits 4:0 */
    cmp     r14, #0x17              /*check for ABT mode*/
    bne     exception_loop_here  /*In UND mode, do not handle*/

    /*Proceed in abt mode*/
    ldr     r14, =exception_regs
    stm     r14, {r0 - r3,r12}
    ldr     r0, [r13]    /*Read preferred return address*/
    ldr     r1, [r13, #0x4]    /*read data or prefetch fault mode*/
    cmp     r1, #0x0     /*Check for data abort*/
    bne     pref_abt_mode
    sub     r0, r0, #0x4  /*data abt ret addr is LR - 8*/
pref_abt_mode
    sub     r0, r0, #0x4  /*pref abt ret addr is LR - 4*/

    /*Save R14 and SPSR*/
    mrs     r1, spsr
    ldr     r14, =exception_cpsr
    str     r1, [r14]
    sub     r14, r14, #4      /*Make r14 point to use pc*/
    str     r0, [r14]         /*Save exception faulted pc*/

    ldr     r1, [r13, #0x4]    /*read data or prefetch fault mode*/
    cmp     r1, #0x0       /*Check for data abort*/
    bne     invoke_pager   /*pref abt:r0 already has addr to be paged*/
    mrc p15, 0, r0, c6, c0, 0   /* Read DFAR*/
                           /* data abt: get addr to be paged in r0*/

invoke_pager
    /*r0=fault address, r1=data/prefetch abt info*/
    ldr     r2, =qsee_handle_page_fault
    blx     r2
    cmp     r0, #0x0
    bne     pager_error   /*Could not page in*/
    ldr     r14, =exception_regs
    ldm     r14, {r0 - r3,r12}
    ldr     r14, =exception_cpsr
    sub     r14, r14, #4      /*Make r14 point to use pc*/
    rfe     r14

pager_error
    b      pager_error
exception_loop_here
    b       exception_loop_here

    AREA |QSEE_SWI_Area|, DATA, READWRITE
exception_regs    SPACE 0x40
exception_cpsr    SPACE 0x4

qsee_apps_svc_stack_guard DCD   0xdeadbeef
qsee_apps_svc_stack_end   SPACE QSEE_SVC_STACK_SIZE
qsee_apps_svc_stack_start DCD   0xdeadbeef

app_init_swi_num         DCD    SYSNUM(export_init_info)

    END

