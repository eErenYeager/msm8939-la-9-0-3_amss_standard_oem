/*============================================================================
Mink Services

ABSTRACT
   Services in Mink

DESCRIPTION



Copyright (c) 2011 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
=========================================================================== */

/*=========================================================================
                              Edit History

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsee/mink/core/src/mink_services.c#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $


when       who     what, where, why
--------   ---     --------------------------------------------------------
03/27/12   dp     Initial Revision

=========================================================================== */
#include "mink_services.h"
#include "apps.h"

mink_service_table_entry_t mink_service_table[MINK_NUM_TOTAL_SERVICES];

extern Object mink_opener_obj_table[]; // TODO: need private header file

Object mink_service_obj_lookup(uint32 service_id)
{
  Object ObjNull = {NULL, NULL};
  Object OpenerObj = {NULL, NULL};
  ObjectArg a[2];

  int i = 0;
  for (i = 0; i < MINK_NUM_TOTAL_SERVICES; i++)
  {
    if (mink_service_table[i].service_id == service_id)
    {
      if (Object_isNull(mink_service_table[i].obj))
      {
        // Find Opener Object and invoke on open and get service object and add it to the table
        OpenerObj = mink_opener_obj_table[mink_service_table[i].module_idx];
        a[0].b.ptr = &service_id;
        a[0].b.len = sizeof(service_id);
        Object_invokeABCD(OpenerObj, MINK_CMD_OPEN, a, 1, 0, 0, 1);

        mink_service_table[i].obj = a[1].o;
      }
      /*service object now present*/
      return mink_service_table[i].obj;
    }
  }
  return ObjNull;
}


Object mink_service_obj_lookup_for_module(uint32 service_id)
{
  Object ObjNull = {NULL, NULL};
  Object OpenerObj = {NULL, NULL};
  ObjectArg a[2];

  int i = 0;
  for (i = 0; i < MINK_NUM_TOTAL_SERVICES; i++)
  {
    if (mink_service_table[i].service_id == service_id)
    {
        // Find Opener Object and invoke on open and get service object and add it to the table
        OpenerObj = mink_opener_obj_table[mink_service_table[i].module_idx];
        a[0].b.ptr = &service_id;
        a[0].b.len = sizeof(service_id);
        Object_invokeABCD(OpenerObj, MINK_CMD_OPEN, a, 1, 0, 0, 1);
        return a[1].o;
    }
  }
  return ObjNull;
}

static int mink_api_invoke(ObjectCxt ptr, ObjectOp op, ObjectArg *args, ObjectCounts k)
{

  if ((op == Object_OP_retain) || (op == Object_OP_release)) {
    return 0 ;
  }
  switch (op) {
    case MINK_CMD_REQ_SERV:
	  if (k != ObjectCounts_pack(2, 1, 0, 0)) {
        return -1;
      }
	  
	  if ((args[0].b.ptr) && (args[0].b.len == sizeof(uint32))) {
        return qsee_request_service(*((uint32 *)args[0].b.ptr), args[1].b.ptr, args[1].b.len, args[2].b.ptr, args[2].b.len);
	  } else {
	    return -1;
	  }
    default:
      return -1;
  }  
}
extern int platform_handle_syscall(ObjectCxt ptr, ObjectOp op, ObjectArg *args, ObjectCounts k);

static int mink_syscall_invoke(ObjectCxt ptr, ObjectOp op, ObjectArg *args, ObjectCounts k)
{
  return platform_handle_syscall(ptr, op, args, k);
}


Object mink_api_service_obj = {mink_api_invoke, NULL};


Object mink_service_syscall_obj = {mink_syscall_invoke, NULL};
