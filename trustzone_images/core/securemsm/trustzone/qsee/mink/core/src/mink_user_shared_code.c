
#include <stddef.h>
#include <string.h>
#include "lktypes.h"
#include "kprocess_priv.h"
#include "kthread.h"
#include "server.h"
#include "minkipc.h"
#include "kdefs.h"
#include "syscall.h"


#define isOK(r)     Object_isOK(r)

#define Object_setNull(o)   ((o).invoke = NULL)

#define FOR_ARGS(ndxvar, counts, section)                  \
   for (ndxvar = ObjectCounts_index##section(counts);      \
        ndxvar < (ObjectCounts_index##section(counts)      \
                  + ObjectCounts_num##section(counts));    \
        ++ndxvar)


#define isOutboundObject(objUser)   ((objUser).invoke == minkIPC)

#define isHostedObject(objUser) \
   (!Object_isNull(objUser) && !isOutboundObject(objUser))		
		
struct AcceptHeader {
   Object server;
   Object obj;
   ObjectOp op;
   ObjectArg *args;
   ObjectCounts k;
   int result;
};

extern KThreadSyscallResult mink_generic_syscall(uint32 num, intptr_t r0, intptr_t r1, intptr_t r2, intptr_t r3);

KThreadSyscallResult KThread_syscall(intptr_t r0, intptr_t r1, intptr_t r2, intptr_t r3)
{
   return mink_generic_syscall(SYSNUM(mink_syscall), r0, r1, r2, r3);
}

int minkIPC(ObjectCxt cxt,
            ObjectOp op,
            ObjectArg *args,
            ObjectCounts k)
{
   KThreadSyscallResult ksr;

   ksr = KThread_syscall((intptr_t) cxt,
                         (intptr_t) op,
                         (intptr_t) args,
                         (intptr_t) k);

   // Retain local objects for whom forwarders have been created.
   int ndx;
   uint32_t mask = ksr.aux;
   for (ndx = 0; mask != 0; mask >>= 1, ++ndx) {
      if (mask & 1) {
         Object_retain(args[ndx + ObjectCounts_indexOI(k)].o);
      }
   }

   // retain user objects returned from the kernel
   if (Object_isOK(ksr.result)) {
      FOR_ARGS(ndx, k, OO) {
         Object o = args[ndx].o;
         if (isHostedObject(o)) {
            //atLog("OUT arg[%d].o = {%p,%p}", ndx, o.invoke, o.context);
            Object_retain(o);
         }
      }
   }

   return (int) ksr.result;
}

void server_exit(int ret)
{
  mink_generic_syscall(SYSNUM(mink_server_exit), ret, 0, 0, 0);
}


// Accept and dispatch inbound invocations.
//
// This function is used as the entry point of user processes.
//
int serverLoop(void *cxt)
{
   AcceptHeader *hdr = (AcceptHeader *) cxt;
   Object server = hdr->server;

   while (1) {
      int err = server_accept(server);
      if (! Object_isOK(err)) {
         break;
      }
      hdr->result = Object_invoke(hdr->obj, hdr->op, hdr->args, hdr->k);
   }
   
   server_exit(0);
   
   return 0;
}
