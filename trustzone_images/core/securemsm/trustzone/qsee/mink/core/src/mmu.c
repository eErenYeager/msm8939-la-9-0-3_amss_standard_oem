/*============================================================================
mmu

ABSTRACT
   memory management in QSEE

DESCRIPTION



Copyright (c) 2011 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
=========================================================================== */

/*=========================================================================
                              Edit History

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsee/mink/core/src/mmu.c#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $


when       who     what, where, why
--------   ---     --------------------------------------------------------
03/10/12   spa     Initial Revision

=========================================================================== */

#include "lktypes.h"
#include "mmu.h"
#include "mink_log.h"
#include "tzbsp_mmu_cache.h"

void qsee_map_region(unsigned int vaddr, unsigned int paddr, unsigned int size, unsigned int perms, unsigned int attr, unsigned int user)
{
  unsigned int aligned_vaddr;
  unsigned int aligned_paddr;
  unsigned int end_addr;
  unsigned int aligned_end_addr;
  unsigned int size_in_kb;
  int retval;

  /*attr is assumed to have correct cache attributes*/

  aligned_vaddr = (vaddr >> 12)<<12;
  aligned_paddr = (paddr >> 12)<<12;
  end_addr = vaddr + size;

  aligned_end_addr = end_addr;
  if (end_addr & (0x1000-1)) {
    aligned_end_addr = ((end_addr >> 12)<<12) + 0x1000;
  }

  size_in_kb = (aligned_end_addr - aligned_vaddr) / 1024;

  if (user) {
    switch (perms) {
      case PERM_R:
        attr |= TZ_READ_ONLY_PL0 | TZ_NON_EXECUTABLE;
        break;
	  case PERM_R | PERM_X:
        attr |= TZ_READ_ONLY_PL0 | TZ_EXECUTABLE;
        break;
	  case PERM_R | PERM_W:
        attr |= TZ_READ_WRITE_PL0 | TZ_NON_EXECUTABLE;
        break;
      case PERM_R | PERM_W | PERM_X:
        attr |= TZ_READ_WRITE_PL0 | TZ_EXECUTABLE;
        break;
    }
  } else {
    switch (perms) {
      case PERM_R:
        attr |= TZ_READ_ONLY_PL1 | TZ_NON_EXECUTABLE;
        break;
	  case PERM_R | PERM_X:
        attr |= TZ_READ_ONLY_PL1 | TZ_EXECUTABLE;
        break;
	  case PERM_R | PERM_W:
        attr |= TZ_READ_WRITE_PL1 | TZ_NON_EXECUTABLE;
        break;
      case PERM_R | PERM_W | PERM_X:
        attr |= TZ_READ_WRITE_PL1 | TZ_EXECUTABLE;
        break;
    }
  }

  retval = tzbsp_mmu_map(aligned_vaddr, aligned_paddr, size_in_kb, attr);
  if (retval) {
    TZBSP_LOG_DBG("QSEE Map Region Failed");
  }

  tzbsp_tlb_inval_all();
}
