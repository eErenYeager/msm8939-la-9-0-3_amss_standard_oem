/*
 * Copyright (c) 2016  Qualcomm Technologies, Inc.  All Rights Reserved.
 * Qualcomm Technologies Proprietary and Confidential.
 */

#ifndef SATMATH_H
#define SATMATH_H

#include <stddef.h>
#include <stdint.h>

/*
 * Saturating add & mul simplify the task of ensuring safety.  Since any
 * allocation of SIZE_MAX bytes is guaranteed to fail, we can chain the
 * results of adds and muls and then check for allocation failure.
 */

size_t satMul(size_t a, size_t b);

size_t satAdd(size_t a, size_t b);

/*
 * UINT32_MAX is returned on overflow
 */
 
uint32_t u32SatAdd(uint32_t a, uint32_t b);

#endif /* SATMATH_H */
