#ifndef MINK_THREADS_H
#define MINK_THREADS_H

/**
@file mink_threads.h
@brief Mink pool of threads.
*/

/*===========================================================================
   Copyright (c) 2010 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsee/mink/core/inc/mink_threads.h#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $

when       who      what, where, why
--------   ---      ------------------------------------
4/1/14     dp       Initial version
===========================================================================*/

//#include <comdef.h>
#include "lkthread.h"

typedef int (*thread_callback)(void *arg, void *arg2, uint32_t arg3, uint32_t arg4);

typedef struct service_thread_s {
  thread_t thread;
  thread_callback cb;
  uint32_t status;
  uint32_t args[4];
} service_thread_t;

void mink_init_threads(void);
int mink_queue_thread(thread_callback, void *arg1, void * arg2, uint32_t arg3, uint32_t arg4);
boolean mink_thread_busy(void);
boolean mink_thread_blocked_on_listener();
#endif /* MINK_THREADS_H */
