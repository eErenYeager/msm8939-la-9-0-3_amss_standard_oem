	
// kdefs: definitions for the kernel environment

#ifndef __KDEFS_H
#define __KDEFS_H


#include <stdint.h>
#include <stdlib.h>
#include <limits.h>

#if defined(__cplusplus)
# define KINLINE static __inline
#else
# define KINLINE static __inline
#endif


#define K_OK      0
#define K_ERROR  -1

// zero out contents of a C value
#define ZERO_VALUE(o)          (memset(&(o), 0, sizeof(o)))

// zero out contents of a range of memory (given typed pointer and size)
#define ZERO_RANGE(ptr, len)   (memset((ptr), 0, (len) * sizeof (ptr)[0]))

#define ARRAY_LEN(a)  (sizeof((a)) / sizeof((a)[0]))

#define containerOf(ptr, type, member) \
   ( (type*) (((uintptr_t)(void*)ptr) - offsetof(type, member)))


#include <stdio.h>

#ifdef _DEBUG

#  define kassert(x)                                                    \
   do {                                                                 \
      if (!(x)) {                                                       \
         printf("%s:%d: assertion failed: %s\n", __FILE__, __LINE__, #x); \
         exit(1);                                                       \
      }                                                                 \
   } while (0)


#  define kassert__eq(a, b, ty, fmt)                            \
   do {                                                         \
      ty _a = (ty) (a), _b = (ty) (b);                          \
      if (_a != _b) {                                           \
         printf("%s:%d: eq assertion failed\n"                  \
                "A: " fmt "\n"                                  \
                "B: " fmt "\n", __FILE__, __LINE__, _a, _b);    \
         exit(1);                                               \
      }                                                         \
   } while (0)

#else

#  define kassert(x)
#  define kassert__eq(a,b,c,d)

#endif


#define kassert_eqi(a, b)   kassert__eq(a, b, int, "%d")

#define kassert_equ(a, b)   kassert__eq(a, b, unsigned int, "%u")

typedef void *kvoidptr;
#define kassert_eqp(a, b)   kassert__eq(a, b, kvoidptr, "%p")


//#define atLog(args...)   (printf("%s:%d: ", __FILE__, __LINE__), printf(args), printf("\n"))
#define atLog(fmt, args...)   (printf("%s:%d: " fmt "\n", __FILE__, __LINE__, ##args))
#define at               atLog("")

#ifdef VERBOSE
# define KLOG(a...)  printf(a)
#else
# define KLOG(a...)
#endif


#endif /* __KDEFS_H */
