#ifndef MINK_SERVICES_H
#define MINK_SERVICES_H

/**
@file mink_services.h
@brief Provides service definitions
*/

/*===========================================================================
   Copyright (c) 2010 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsee/mink/include/mink_services.h#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $

when       who      what, where, why
--------   ---      ------------------------------------

===========================================================================*/

//#include <comdef.h>
#include "com_dtypes.h"
//#include "tzbsp_syscall.h"
#include "mink_object.h"

/*----------------------------------------------------------------------------
 * Mink helper macros
 * -------------------------------------------------------------------------*/
#define MINK_SERVICE_ID(s, id) \
  ((uint32)(((s & 0x3F)<< 26) | (id & 0x3FFFFFF)))

#define MINK_KERNEL_SERVICE     0x0
#define MINK_SIP_SERVICE        0x1
#define MINK_OEM_SERVICE        0x2
#define MINK_TEST_SERVICE       0x3
#define MINK_OTHER_SERVICE      0x4


/*----------------------------------------------------------------------------
 * Mink service IDs
 * -------------------------------------------------------------------------*/
#define MINK_NUM_TOTAL_SERVICES         7

/**
 * Kernel services
 */
#define MINK_SERVICE_KERNEL_MEM_MGMT_ID           MINK_SERVICE_ID(MINK_KERNEL_SERVICE, 0x0)
#define MINK_SERVICE_KERNEL_SYSCALL_ID            MINK_SERVICE_ID(MINK_KERNEL_SERVICE, 0x1)
#define MINK_SERVICE_KERNEL_API_ID            MINK_SERVICE_ID(MINK_KERNEL_SERVICE, 0x2)

#define MINK_SERVICE_USER_SYSCALL_ID      MINK_SERVICE_ID(MINK_SIP_SERVICE, 0x4)

/**
 * SIP services
 */
#define MINK_SERVICE_SYSCALL_ID            MINK_SERVICE_ID(MINK_SIP_SERVICE, 0x0)
#define MINK_SERVICE_IMG_AUTH_ID           MINK_SERVICE_ID(MINK_SIP_SERVICE, 0x1)

/**
 * Other (ie. OEM, Test) services
 */
#define MINK_SERVICE_TEST_ID               MINK_SERVICE_ID(MINK_TEST_SERVICE, 0x0)

/*----------------------------------------------------------------------------
 * Mink commands
 * -------------------------------------------------------------------------*/
#define MINK_CMD_SYSCALL      1
#define MINK_CMD_OPEN         2
#define MINK_CMD_INIT         3
#define MINK_CMD_REQ_SERV         4
#define MINK_CMD_REG_SHARED_BUF         5
/*----------------------------------------------------------------------------
 * Subsys CONFIG and Subsys Reset commands <-- have to be moved to correct header file
 * -------------------------------------------------------------------------*/
#define SUBSYS_CONFIG_CMD_BRINGUP       1
#define SUBSYS_CONFIG_CMD_TEARDOWN      2

#define BSP_SUBSYS_RESET_CMD_BRINGUP       1
#define BSP_SUBSYS_RESET_CMD_TEARDOWN      2


/*----------------------------------------------------------------------------
 * Type definitions
 * -------------------------------------------------------------------------*/

//TODO this def is redundant
#ifndef TZBSP_SYSCALL_FUNC_T
#define TZBSP_SYSCALL_FUNC_T
typedef int (*tzbsp_syscall_func_t) ();
#endif 

typedef struct mink_service_table_entry_s {
  uint32 service_id;
  uint32 module_idx;
  Object obj; //TODO should it be object or object *
} mink_service_table_entry_t;

typedef struct mink_syscall_info_s {
  tzbsp_syscall_func_t func;
  uint32 nargs;
  uint32 args[11];  //11 to allow for 10 args and 1 arg for the rsp pointer 
} mink_syscall_info_t;

/*----------------------------------------------------------------------------
 * Function definitions
 * -------------------------------------------------------------------------*/

Object mink_service_obj_lookup(uint32 service_id);

#endif /*MINK_SERVICES_H*/
