	
#ifndef __HEAP_H
#define __HEAP_H

#include <stddef.h>
#include <string.h>
#include "lktypes.h"


void* tzbsp_malloc(uint32_t size);
void tzbsp_free(void* ptr);


static inline void* tzbsp_malloc_zero_init(uint32_t size)
{
  void* ptr;
  ptr = tzbsp_malloc(size);
  
  if(ptr){
    memset(ptr, 0, size);
  }
  
  return ptr;
}

static inline void *heap_memdup(const void* ptr, size_t size)
{
  void* p = tzbsp_malloc(size);
  if (p != NULL) {
    memcpy(p, ptr, size);
  }
  return p;
}

// free memory and zero pointer
#define HEAP_FREE_PTR(var)       ((void) ((var) && (tzbsp_free(var), (var) = 0)))

// wrappers for type safety

#define HEAP_ZALLOC_REC(ty)       ((ty *) tzbsp_malloc_zero_init(sizeof(ty)))

//#define HEAP_ZALLOC_ARRAY(ty, k)  ((ty *) heap_zalloc( heap_umul(sizeof(ty), (k)))) <-- need multiply

#define HEAP_ZALLOC_ARRAY(ty, k)  ((ty *) tzbsp_malloc_zero_init(sizeof(ty) * (k)))

#endif /* __HEAP_COMMON_H */
