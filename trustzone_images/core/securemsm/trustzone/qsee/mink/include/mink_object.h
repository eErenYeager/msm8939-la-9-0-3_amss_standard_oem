// object.h : invocable objects
//
// Object an invocable object (capability)
// ObjectBuf a range of bytes in memory
// ObjectArg an object or buffer
// ObjectCxt context data to be passed to object()
// Object_* methods & constants for object invocation
//
// An object consist of two pointers: `invoke` and `context`.
//
// For objects reachable via syscall (from a process):
// invoke == minkIPC
// context == descriptor
//
// For objects resident in the caller's domain:
// invoke == the object's invoke function
// context == the object's data structure
//

#ifndef __OBJECT_H
#define __OBJECT_H

#include <stddef.h> // size_t
#include <inttypes.h> // other_t


typedef struct Object Object;
typedef struct ObjectBuf ObjectBuf;
typedef union ObjectArg ObjectArg;
typedef void *ObjectCxt;
typedef uint32_t ObjectCounts;
typedef uint32_t ObjectOp;

typedef int (*invoke_func_t)(ObjectCxt h,
                 ObjectOp op,
                 ObjectArg *args,
                 ObjectCounts counts);
				 

struct Object {
   int (*invoke)(ObjectCxt h,
                 ObjectOp op,
                 ObjectArg *args,
                 ObjectCounts counts);
   ObjectCxt context; // context data to pass to the invoke function
};


struct ObjectBuf {
   void *ptr;
   size_t len;
};


union ObjectArg {
   ObjectBuf b;
   Object o;
};


// The counts of different types of items in args[] is encoded in bit fields
// in a 32-bit quantity `ObjectCounts`.

#define ObjectCounts_pack(nBuffersIn, nBuffersOut, nObjectsIn, nObjectsOut) \
((ObjectCounts) ((nBuffersIn) | \
((nBuffersOut) << 8) | \
((nObjectsIn) << 16) | \
((nObjectsOut) << 24)))


// unpack counts
#define ObjectCounts_numBI(k) ( (size_t) ( ((k) >> 0) & 0xFF) )
#define ObjectCounts_numBO(k) ( (size_t) ( ((k) >> 8) & 0xFF) )
#define ObjectCounts_numOI(k) ( (size_t) ( ((k) >> 16) & 0xFF) )
#define ObjectCounts_numOO(k) ( (size_t) ( ((k) >> 24) & 0xFF) )

#define ObjectCounts_hasObjects(k) ( (k) && 0xFFFF0000 )

// Indices into args[]
#define ObjectCounts_indexBI(k) 0
#define ObjectCounts_indexBO(k) (ObjectCounts_indexBI(k) + ObjectCounts_numBI(k))
#define ObjectCounts_indexOI(k) (ObjectCounts_indexBO(k) + ObjectCounts_numBO(k))
#define ObjectCounts_indexOO(k) (ObjectCounts_indexOI(k) + ObjectCounts_numOI(k))
#define ObjectCounts_total(k) (ObjectCounts_indexOO(k) + ObjectCounts_numOO(k))


#if 0

#define Object_invoke(o, op, a, k) \
(o).invoke((o).context, (op), (a), (k))

#define Object_invokeABCD(o, op, args, a, b, c, d) \
(o).invoke((o).context, (op), (args), ObjectCounts_pack((a), (b), (c), (d)))

#else

static inline int Object_invoke(Object o, ObjectOp op, ObjectArg *args, ObjectCounts k)
{
   return (o).invoke((o).context, (op), (args), (k));
}

static inline int Object_invokeABCD(Object o, ObjectOp op, ObjectArg *args, int a, int b, int c, int d)
{
   return Object_invoke(o, op, args, ObjectCounts_pack((a), (b), (c), (d)));
}

#endif


#define Object_isNull(o) \
( (o).invoke == NULL )


#define Object_NULL ( (Object) { NULL, NULL } )

// Generic error codes

#define Object_OK 0 // non-specific success code
#define Object_ERROR -1 // non-specific error
#define Object_ERROR_INVALID -3 // unsupported/unrecognized request

// Transport-level error codes

#define Object_ERROR_DEFUNCT -10 // object no longer exists
#define Object_ERROR_ABORT -11 // calling thread must exit
#define Object_ERROR_BADOBJ -12 // invalid object context (handle) passed as target or input
#define Object_ERROR_NOSLOTS -13 // caller's object table full [dynamic]
#define Object_ERROR_MAXARGS -14 // too many args in the invoke request [propery of the req]
#define Object_ERROR_MAXDATA -15 // buffers too large [property of the route]
#define Object_ERROR_UNAVAIL -16 // the request could not be processed [resources/state in dest]
#define Object_ERROR_KMEM -17 // kernel out of memory
#define Object_ERROR_REMOTE -18 // object is remote; cannot receive local ops

//#define Object_ERROR_PARAM -8 // invalid argument


#define Object_isOK(err) ((err) >= 0)


#define ObjectOp_LOCAL ((ObjectOp)0x80000000)
#define ObjectOp_isLocal(op) ( ((op) & ObjectOp_LOCAL) != 0)


// "Object" also serves as the base interface for all objects


#define Object_OP_release ((ObjectOp) -1)
#define Object_OP_retain ((ObjectOp) -2)

#define Object_release(o) Object_invoke((o), Object_OP_release, 0, 0)
#define Object_retain(o) Object_invoke((o), Object_OP_retain, 0, 0)


#define DEFINE_OBJECT_INVOKE(func, prefix, type) \
int func(ObjectCxt h, ObjectOp op, ObjectArg *args, ObjectCounts k) \
{ \
type me = (typ) h; \
switch (op) { \
case Object_OP_release: \
prefix##release(me); \
return Object_OK; \
case Object_OP_retain: \
prefix##retain(me); \
return Object_OK; \
} \
return Object_ERROR_INVALID; \
}



#define Object_RELEASE_IF(o) \
do { Object o_ = (o); if (!Object_isNull(o_)) Object_release(o_); } while (0)


#endif /* __OBJECT_H */
