	
#ifndef __BBUF_H
#define __BBUF_H

#include <stddef.h>

typedef struct {
   char *ptr;
   size_t len;
} BBuf;


void BBuf_construct(BBuf *me, char *ptr, size_t len);

// Sub-allocate a range of memory from the buffer.  Returned value is
// aligned to the maximum alignment required by C data structures.
//
void *BBuf_alloc(BBuf *me, size_t a_size);

#define BBuf_ALLOC_REC(pbb, typ)                \
   ((typ*) BBuf_alloc((pbb), sizeof(typ)))

#define BBuf_ALLOC_ARRAY(pbb, typ, count) \
   ((typ*) BBuf_alloc((pbb), sizeof(typ) * (count)))


#endif /* __BBUF_H */
