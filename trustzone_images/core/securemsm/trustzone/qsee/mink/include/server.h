#ifndef __SERVER_H
#define __SERVER_H

//#include "object.h"

#define server_OP_accept  0


// caller proxies

static __inline int server_accept(Object o)
{
   return Object_invoke(o, server_OP_accept, 0, 0);
}


// callee proxies

#define DEFINE_SERVER_INVOKE(func, prefix, type)                        \
   int func(ObjectCxt h, ObjectOp op, ObjectArg *args, ObjectCounts k)  \
   {                                                                    \
      type me = (type) h;                                               \
      switch (op) {                                                     \
      case server_OP_accept:                                            \
         return prefix##accept(me);                                     \
                                                                        \
      case Object_OP_release:                                           \
         prefix##release(me);                                           \
         return Object_OK;                                              \
                                                                        \
      case Object_OP_retain:                                            \
         prefix##retain(me);                                            \
         return Object_OK;                                              \
      }                                                                 \
      return Object_ERROR_INVALID;                                      \
   }

#endif
