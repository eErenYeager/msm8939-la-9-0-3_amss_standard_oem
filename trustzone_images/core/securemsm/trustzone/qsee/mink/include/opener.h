	
#ifndef __OPENER_H
#define __OPENER_H

#include <inttypes.h>
#include "object.h"
#include "idlrt.h"

#define opener_OP_open    0


static __inline int opener_open(Object o, uint32_t id, Object *objOut)
{
   ObjectArg a[2];

   a[0].b.ptr = &id;
   a[0].b.len = sizeof(id);
   int result = Object_invokeABCD(o, opener_OP_open, a, 1, 0, 0, 1);
   *objOut = a[1].o;
   return result;
}


// callee proxies

#define DEFINE_OPENER_INVOKE(func, prefix, type)                        \
   int func(ObjectCxt h, ObjectOp op, ObjectArg *args, ObjectCounts k)  \
   {                                                                    \
      type me = (type) h;                                               \
      switch (op) {                                                     \
      case opener_OP_open:                                              \
         if (k != ObjectCounts_pack(1, 0, 0, 1)) {                      \
            break;                                                      \
         }                                                              \
         uint32_t id;                                                   \
         return                                                         \
            IDLRT_READ(id, args+0) ||                                   \
            prefix##open(me, id, &args[1].o);                           \
                                                                        \
      case Object_OP_release:                                           \
         prefix##release(me);                                           \
         return Object_OK;                                              \
                                                                        \
      case Object_OP_retain:                                            \
         prefix##retain(me);                                            \
         return Object_OK;                                              \
                                                                        \
      }                                                                 \
      return Object_ERROR_INVALID;                                      \
   }


#endif /* __OPENER_H */
