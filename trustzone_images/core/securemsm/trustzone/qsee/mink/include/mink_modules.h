#ifndef MINK_MODULES_H
#define MINK_MODULES_H

/**
@file mink_modules.h
@brief Provides module definitions
*/

/*===========================================================================
   Copyright (c) 2010 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsee/mink/include/mink_modules.h#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $

when       who      what, where, why
--------   ---      ------------------------------------
08/20/10   cap      Initial version.

===========================================================================*/

//#include <comdef.h>
#include "com_dtypes.h"
#include "mink_object.h"


#define MINK_PRIV_LEVEL_KERNEL 0
#define MINK_PRIV_LEVEL_USER   1


struct mink_module {
  const char *name;
  int (*init_invoke)(ObjectCxt h, ObjectOp op, ObjectArg *args, ObjectCounts counts);
  uint32 priv_level;
  uint8* stack_base;
  uint32 stack_size;
  uint8* code_base;
  uint8* code_size;
  uint8* data_base;
  uint8* data_size;
  uint32 maxObjs;
  uint8* acceptBuf;
  uint32 acceptBufSize;
  uint32 num_services;
  uint32 services[];
};

/* The ARM C99 compiler does not support variable length arrays yet
   so we have to do this via macros and trick the compiler */
#define MINK_MODULE(num_args)   \
  struct {                      \
	  const char *name;           \
	int (*init_invoke)(ObjectCxt h, ObjectOp op, ObjectArg *args, ObjectCounts counts); \
		uint32 priv_level;          \
    uint8* stack_base;          \
		uint32 stack_size;          \
		uint8* code_base;           \
		uint8* code_size;           \
		uint8* data_base;           \
		uint8* data_size;           \
        uint32 maxObjs;             \
        uint8* acceptBuf;           \
		uint32 acceptBufSize;       \
		uint32 num_services;        \
		uint32 services[num_args];  \
  }

#define MINK_DEFINE_MODULE(name, init_invoke, priv_level, stack_base, stack_size, code_base, code_size, data_base, data_size, maxObjs, acceptBuf, acceptBufSize, num_services, ...) \
   static MINK_MODULE(num_services) mink_module_ ##name \
   __attribute__((section("_mink_module"),used)) = \
  { #name, init_invoke, priv_level, stack_base, stack_size, code_base, code_size, data_base, data_size, maxObjs, acceptBuf, acceptBufSize, num_services, __VA_ARGS__}

extern const void * _mink_module$$Base;
extern const void * _mink_module$$Limit;

#endif /*MINK_MODULES_H*/
