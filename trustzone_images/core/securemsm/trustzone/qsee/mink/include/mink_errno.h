#ifndef MINK_ERRNO_H
#define MINK_ERRNO_H
/**
@file mink_errno.h
@brief Provide mink error codes
*/

/*===========================================================================
   Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsee/mink/include/mink_errno.h#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $

when       who      what, where, why
--------   ---      ------------------------------------
06/20/14   nnl      initial version: splitting tzbsp_errno.h
===========================================================================*/


/*===========================================================================

                              DEFINES

===========================================================================*/

/* Base error numbers */
#include "mink_scm_errno.h"


/* Start of TZ Specific error codes */
//#define E_NEXT_ERROR_CODE 256
//#if 0
/* Logs an error code. */
//#define TZBSP_ERROR_CODE(xx) \
//  TZBSP_LOG_ERR("(%u)", TZBSP_EC_##xx)

/* Conditionally logs an error code. Error code is logged only if error is set
 * in \c pred. */
//#define TZBSP_ERROR_CODE_E(pred, xx) \
  TZBSP_LOG_ERR_E((pred),  "(%u)", TZBSP_EC_##xx)
//#endif  
#define MINK_ERROR_CODE(xx) \
  MINK_LOG_ERR("(%u)", MINK_EC_##xx)

/* Conditionally logs an error code. Error code is logged only if error is set
 * in \c pred. */
#define MINK_ERROR_CODE_E(pred, xx) \
  MINK_LOG_ERR_E((pred),  "(%u)", MINK_EC_##xx)

#define TZBSP_EC_INIT_DEVICE_UNMAP_FAILED                              118
#define TZBSP_EC_INIT_HW_INIT_FAILED                                   120
#define TZBSP_EC_INIT_CHIPSET_INIT_FAILED                              121
#define TZBSP_EC_INIT_PIL_INIT_FAILED                                  123
#define TZBSP_EC_INIT_BOOT_TAMPER_CHECK_FAILED                         124
#define TZBSP_EC_STACK_CHECK_FAILED                                    136
#define TZBSP_EC_INIT_CHIPSET_REINIT_FAILED                            139
#define TZBSP_EC_INIT_DEMAND_PAGE_FAILED                               140
#define TZBSP_EC_SYSCALL_DENIED_DPC_BLOCKED                            166
#define TZBSP_EC_INIT_DDR_INIT_FAILED_2                                223
#define TZBSP_EC_MEM_ALLOCATE_ALIGNED_BAD_INPUT                        232
#define TZBSP_EC_MEM_REGION_NOT_DEFINED                                233
#define TZBSP_EC_MEM_SET_MASK_BAD_INPUT                                234
#define TZBSP_EC_MEM_FREE_AND_UNMAP_VTOP                               235
#define TZBSP_EC_MEM_FREE_ALIGNED_BAD_INPUT                            236
#define TZBSP_EC_MEM_COUNTER_TOO_HIGH                                  237
#define TZBSP_EC_MEM_PRNG_FAIL                                         238
#define TZBSP_EC_MEM_RANDOM_MALLOC_FAIL                                239
#define TZBSP_EC_MEM_RANDOM_BAD_INPUT                                  240
#define TZBSP_EC_MEM_BESTFIT_BAD_INPUT                                 241
#define TZBSP_EC_MEM_ALLOCATE_NO_SPACE                                 242







#endif
