#ifndef MINK_SCM_ERRNO_H
#define MINK_SCM_ERRNO_H
/*===========================================================================

               E R R O R   N U M B E R   D E F I N I T I O N S

DESCRIPTION
  This contains the definition of the return codes (error numbers).
  Functions using this definition either return an error code, or set
  a global variable to the appropriate value.
 

Copyright (c) 2000-2009 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

$Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsee/mink/include/mink_scm_errno.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
10/28/09   chm     Add support for more informational return values from 
                   WMDRM.
02/20/06   ssm     Deleted IxErrno in Interfaces dir and copied to services
                   folder
01/03/06   ssm     Cloned from errno.h

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

/*===========================================================================

              DEFINITIONS AND CONSTANTS FOR ERROR CODES

===========================================================================*/

typedef enum
{
  /*** Generic outcomes of operations ***/
  MINK_E_SUCCESS            =  0,    /* Operation successful                  */
  MINK_E_FAILURE            =  1,    /* Operation failed due to unknown err   */
  MINK_E_NOT_AVAILABLE      =  3,    /* Operation currently not available     */
  MINK_E_NOT_SUPPORTED      =  4,    /* Operation not yet implemented         */

  /*** Memory management related error conditions ***/
  MINK_E_NO_MEMORY          = 15,    /* Allocation from a memory pool failed  */

  /*** Parameter or data parsing related error conditions ***/
  MINK_E_INVALID_ARG        = 16,    /* Arg is not recognized                 */
  MINK_E_OUT_OF_RANGE       = 17,    /* Arg value is out of range             */
  MINK_E_BAD_ADDRESS        = 18,    /* Ptr arg is bad address                */

  /*** File related error conditions ***/
  MINK_E_BUSY               = 55,    /* Device/Item is busy                   */

  MINK_E_RESERVED           = 0x7FFFFFFF
  
} Mink_SCM_ErrnoType;

//} IxErrnoType;

/* Backwards compatibility */
//typedef IxErrnoType errno_enum_type;          //nnl ?

#endif /* MINK_SCM_ERRNO_H */
