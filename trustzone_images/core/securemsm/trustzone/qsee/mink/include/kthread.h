	
// kthread.h : Kernel-level threading interface.

#ifndef __KTHREAD_H
#define __KTHREAD_H

#include "lktypes.h"
#include <stdint.h>

#include "lkthread.h"

// KThread: kernel-level thread API
//
// Threads may execute in user mode or kernel mode, transitioning back and
// forth. When running in user mode, a software interrupt or other exception
// will transition control to the appropriate handler (named
// KThread_handleXXX).  Handlers execute on a stack in the kernel in the
// context of the same KThread. When handlers return, execution resumes in
// user mode at the instruction after the one causing the fault.
//
// Threads may begin execution in user mode or kernel mode. Threads that
// begin execution in kernel mode will never transition to user mode.


// The following handlers are defined outside of the KThread module; the
// naming is purely for namespace concerns.


typedef struct KThreadSyscallResult KThreadSyscallResult;
struct KThreadSyscallResult {
   int32_t result;
   uint32_t aux;
};

// Handle a syscall that was triggered by KThread_syscall().
//
// `arg` is the `handlerArg` value associated with the running thread.
//
// `r1`...`r4` contain the corresponding values that were passed to
// KThread_syscall().
//
// On return from the handler, execution shall resume in user space following
// the software interrupt instruction, the value returned by the handler
// shall be available in R0.
//
extern KThreadSyscallResult
KThread_handleSyscall(void *arg, intptr_t r1, intptr_t r2, intptr_t r3, intptr_t r4);

KThreadSyscallResult
KThread_RouteSyscall(intptr_t r1, intptr_t r2, intptr_t r3, intptr_t r4);

// Handle a fault.
//
extern int KThread_handleFault(void *arg);


typedef thread_t KThread;

typedef int (*KThreadStartRoutine)(void *arg);

void *KThread_getCurrentProcess(void);

// Initialize the KThread library and begin execution within a thread at
// `start`. On some platforms (e.g. 'sim'), this function returns when all
// threads exit. On others, it never returns.
//
void KThread_init(void (*start)(void*), void *startArg);


// Create a new thread, returning a pointer to it.
//
// The thread is created in a non-running state.  Call `start` to begin
// execution.
//
// `stackTop` points to the memory location AFTER the range of memory
// allocated for the stack.  (This will be equivalent to the intial stack
// pointer if the architecture uses a pre-decrement/post-increment stack
// convention.)
//
// When `startInUserMode` is true, `start` will begin execution in user mode.
//
// `handlerArg` is a value that will be passed to the KThread_handleXXX functions.
//
KThread *KThread_new(const char *name,
                     KThreadStartRoutine start,
                     void *startArg,
                     int priority,
                     size_t stackSize,
                     void *stack,
                     uint32 app_id,
                     bool startInUserMode);


// Delete a thread. If the thread has not exited, it is exited.
//
void KThread_delete(KThread *me);


// Move a thread from non-running state to a running state.
//
void KThread_start(KThread *me);

bool KThread_IsFaulted(KThread *me);

// Wait for thread to exit.
//
int KThread_join(KThread *me);

void KThread_setArg(KThread *meThread, void *arg);

// Terminate execution of a thread.
//
// If its execution state is currently in user mode, the thread will cease
// execution immediately.
//
// If the thread is executing in kernel mode, this function will put the
// thread in an "aborting" state and block until the thread exits.  While in
// an aborting state, the thread's calls to wait() will not block and
// instead will return a non-zero value.  Also, while in an aborting state,
// return from an exception handlers will not return control to user mode --
// instead the thread will be exited at that point.
//
int KThread_kill(KThread *me);


// Signal the given thread.
//
// Signaling a thread sets its "signal bit" to true.
//
void KThread_signal(KThread *me);


// Wait until the current thread's signal bit is true, then clear the bit
// and return 0.
//
// If the thread is in a "aborting state" immediately return 1.
//
int KThread_wait(void);

typedef KThreadSyscallResult (*KThreadResumeFunc)(void *); 

void KThread_asynchWait(KThreadResumeFunc pfn, void *arg);

// Terminate the calling thread.
//
void KThread_exit(void);


// Return the pointer to the currently-running KThread.
//
KThread *KThread_getCurrent(void);

void KThread_attachHandler(void *handler, KThread *meThread);

// Issue a software interrupt instruction.
//
// This function is callable only from user mode. Results are undefined
// otherwise.
//
KThreadSyscallResult
KThread_syscall(intptr_t r1, intptr_t r2, intptr_t r3, intptr_t r4);


#endif /* __KTHREAD_H */
