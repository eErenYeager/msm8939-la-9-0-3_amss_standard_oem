	
#ifndef __KPROCESS_H
#define __KPROCESS_H

#include <stddef.h>
#include "kthread.h"
#include "lktypes.h"
//#include "object.h"
#include "mink_object.h"

typedef struct KProcess KProcess;

// Process creation
// ----------------
//    A: passed to new()
//    B: after new() abd before start()
//    C: passed to start()
//    D: before or after start()
//
// Fixed Assumptions:
//    process has one thread
//    process == server
//      entry point == serverLoop, or alternate impl
//    kernel stack size(s)
//
// Options in sim AND non-sim platforms:
//    object limit              [A]
//    intial object             [D]
//    payload limit             [C]
//    privileges (opener caps)  [?]
//
// Options only in non-sim (actual memory protection) platforms:
//    user stack size           [B]
//    map data                  [B]
//    map code                  [B]
//    alloc & map heap & stack  [B]
//
// TODO:
//    memory allocation limits (separate alloc object?)


KProcess *KProcess_new(int maxObjects, void *acceptBuf, size_t acceptBufSize);

void      KProcess_retain(KProcess *me);

void      KProcess_release(KProcess *me);

int KProcess_start(KProcess *me);

int       KProcess_kill(KProcess *me, bool isGraceful);

Object    KProcess_newInboundObject(KProcess *proc, Object objInside);

Object KProcess_newOutboundObject(KProcess *me, Object obj);

void KProcess_attachThread(KProcess *meProcess, KThread *meThread);

// opaque data type to be defined by each callback implementation
typedef struct FaultCBCxt FaultCBCxt;
typedef void (*FaultCB)(FaultCBCxt *arg);

void KProcess_setFaultCallback(KProcess *me, FaultCB callback, FaultCBCxt *arg);


#endif /* __KPROCESS_H */
