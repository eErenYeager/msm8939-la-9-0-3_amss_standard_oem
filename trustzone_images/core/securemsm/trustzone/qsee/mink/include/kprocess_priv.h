	
#ifndef __KPROCESS_PRIV_H
#define __KPROCESS_PRIV_H

#include "lktypes.h"
#include "qlist.h"
//#include "object.h"
#include "kthread.h"
#include "kspace.h"
#include "bbuf.h"
#include "kprocess.h"


typedef struct AcceptHeader AcceptHeader;


// Maximum number of invoke arguments when crossing process boundary
#define KPROCESS_MAX_ARGS  8


struct KProcess {
   unsigned refs;

   // debugging-related items

   QNode qProcesses;            // all processes

   // capabilites & memory access

   int objectsLen;
   Object *objects;             // invocable objects [objectsLen]
   unsigned *objectRefs;        // reference counts [objectsLen]
   KSpace space;               // address space
   size_t userStackSize;

   FaultCB faultCallback;
   FaultCBCxt *faultArg;

   // server state

   KThread *thread;             // "main" thread
   QList qlInvokers;
   QList qlForwarders;
   bool isResponsePending;
   bool isShutdown;
   AcceptHeader *acceptHeader;  // (user memory)
   BBuf acceptBuffer;           // (user memory)
   ObjectArg *cleanupArgs;      // (user memory)
   int cleanupNum;              // size of cleanupArgs[]

   // resource grants & budgets

};


#define USERSTACK_DEFAULT   8192

#endif /* __KPROCESS_PRIV_H */
