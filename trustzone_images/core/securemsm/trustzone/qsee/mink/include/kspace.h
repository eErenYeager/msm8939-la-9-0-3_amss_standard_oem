	
// KSpace_common: KSpace functions common across all platforms.
//
// This file is typically included via <platform>/kspace.h.
//
// KSpace exposes a construct/destruct contract, so the caller must know the
// sizes of the underlying structures. The sizes may change from platform to
// platform, so those are defined in files names <platform>/kspace.h.


#ifndef __KSPACE_COMMON_H
#define __KSPACE_COMMON_H

#include <stddef.h>

// TODO: use public "MemRegion" header for these defs.
#define MemRegion_PERM_READ     1
#define MemRegion_PERM_WRITE    2
#define MemRegion_PERM_EXEC     4
#define MemRegion_PERM_DATA     ( MemRegion_PERM_READ | MemRegion_PERM_WRITE )
#define MemRegion_PERM_CODE     ( MemRegion_PERM_READ | MemRegion_PERM_EXEC )

//--------------------------------
// KSpace
//--------------------------------

#define KSPACE_SECURE    0x0001
#define KSPACE_NONSECURE 0x0002
#define KSPACE_ALL       (KSPACE_SECURE | KSPACE_NONSECURE)

struct KSpace {
   int numLocks;
};

typedef struct KSpace KSpace;

struct KMemLock {
   KSpace *space;
};

void KSpace_construct(KSpace *me);
void KSpace_destruct(KSpace *me);

// KSpace_createWindow(paddr, size, READ|EXEC, &ulCode);
// memrgnCode = createWindowRegion(paddr, size, READ | EXEC);
// KSpace_mapRegion(me->space, memrgnCode, &va, &len);


//--------------------------------
// KMemLock
//--------------------------------

typedef struct KMemLock KMemLock;

void KMemLock_construct(KMemLock *me, KSpace *space);
void KMemLock_destruct(KMemLock *me);
int KMemLock_lockR(KMemLock *me, void *ptr, size_t len);
int KMemLock_lockW(KMemLock *me, void *ptr, size_t len);
int KMemLock_lockRW(KMemLock *me, void *ptr, size_t len);


#endif /* __KSPACE_COMMON_H */
