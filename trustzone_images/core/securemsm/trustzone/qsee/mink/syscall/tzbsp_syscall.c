/**
@file tzbsp_syscall.c
@brief Support functions for system call API

*/

/*===========================================================================
   Copyright (c) 2010-2013 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsee/mink/syscall/tzbsp_syscall.c#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $

when       who      what, where, why
--------   ---      ------------------------------------
08/19/10   cap      Created

===========================================================================*/

/*===========================================================================

          INCLUDE FILES

=============================================================================*/
#include <stdarg.h>
#include <string.h>

#include "tzbsp.h"
#include "tzbsp_errno.h"
#include "tzbsp_syscall.h"
#include "tzbsp_mem.h"
#include "tzbsp_sys.h"
#include "tzbsp_config.h"
#include "tzbsp_exec.h"
#include "crypto/tzbsp_crypto.h"
#include "tzbsp_mmu_cache.h"
#include "tzbsp_smc_errno.h"
#include "tz_mc.h"
#include "mink_errno.h"
#include "mink_scm_errno.h"
#include "mink_services.h"
#include "tt_sd.h"

/* The starting offset of the syscall params in the register array */
#define TZBSP_PARAM_OFFSET 2

/* Can't modify tzbsp_regs_t without modifying Monitor image,
 * so redefine here with rsp struct. This effort is to align
 * with future SMP TZ images (symmetric multiprocessing).
 */
typedef struct tzbsp_regs_with_rsp_s
{
   uint32   pc;
   uint32   cpsr;
   uint32   reg[14];
   tzbsp_smc_rsp_t *rsp;
} tzbsp_regs_with_rsp_t;

tzbsp_smc_rsp_t *smc_rsp = NULL;

/* The global nonsecure syscall ID documents the syscall request from HLOS
 * that is being handled. It can be used to print out syscall ID in
 * error scenarios, such as malloc failures.
 */
static uint32 ns_syscall_id = 0;

extern const void *tz_syscall_core_start;
extern const void *tz_syscall_core_end;
extern const void *tz_syscall_chipset_start;
extern const void *tz_syscall_chipset_end;
extern const void *tz_syscall_oem_start;
extern const void *tz_syscall_oem_end;
#if (defined TZBSP_WITH_TEST_SVC)
static const void *tz_syscall_test_start          = NULL;
static const void *tz_syscall_test_end            = NULL;
static const void *tz_syscall_test_chipset_start  = NULL;
static const void *tz_syscall_test_chipset_end    = NULL;
#endif

extern boolean mink_thread_busy(void);
extern const uint32 N_CPSR;
extern uint32 entry_cpsr;
extern int qsee_do_syscall(void *cb, void *arg1, void *arg2, unsigned int arg3, uint32 arg4, int syscall_id);
extern void qsee_reset_secure_syscall_id(void);

const static struct
{
  const  void **start;
  const  void **end;
} sections[] =
              { {&tz_syscall_core_start,    &tz_syscall_core_end },
                {&tz_syscall_chipset_start, &tz_syscall_chipset_end },
                {&tz_syscall_oem_start,     &tz_syscall_oem_end },
#if (defined TZBSP_WITH_TEST_SVC)
                {&tz_syscall_test_start,    &tz_syscall_test_end },
                {&tz_syscall_test_chipset_start, &tz_syscall_test_chipset_end },
#endif
                    { NULL, NULL } };

#if (defined TZBSP_WITH_TEST_SVC)
/**
   Called by the TZBSP Test Executive image to set the location of the test syscalls
 */
void tzbsp_set_test_syscall_ranges(const void* core_start, const void* core_end,
                                   const void* chipset_start, const void* chipset_end)
{
  tz_syscall_test_start          = core_start;
  tz_syscall_test_end            = core_end;
  tz_syscall_test_chipset_start  = chipset_start;
  tz_syscall_test_chipset_end    = chipset_end;
}
#endif


uint32 tzbsp_get_ns_syscall_id(void)
{
  return ns_syscall_id;
}

void tzbsp_set_ns_syscall_id(uint32 smc_id)
{
  ns_syscall_id = smc_id;
}

void tzbsp_reset_ns_syscall_id(void)
{
  ns_syscall_id = 0;
}


/*
 * @brief Get the syscall table entry for the specified smc_id
 *
 * @return tzbsp_syscall_entry_t* on success
 *         NULL on failure
 */
tzbsp_syscall_entry_t* tzbsp_syscall_lookup(uint32 smc_id)
{
  tzbsp_syscall_entry_t *cur = NULL;
  tzbsp_syscall_entry_t *start = NULL;
  tzbsp_syscall_entry_t *end = NULL;
  uint32 i = 0;
  uint32 owner_id_mask = TZ_MASK_BITS(29,24);
  uint32 func_id_mask = TZ_MASK_BITS(15,0);
  uint32 app_id_mask = TZ_MASK_BITS(15,8);

  /* We only want Owner ID and Function ID */
  smc_id &= owner_id_mask | func_id_mask;

  /* Clear bits for app ID when looking up TZ App syscall */
  if (IS_OWNER_TZ_APPS(TZ_SYSCALL_OWNER_ID(smc_id)))
  {
    smc_id &= ~app_id_mask;
  }

  while (sections[i].start != NULL &&
         sections[i].end != NULL)
  {
    start = *(tzbsp_syscall_entry_t**)sections[i].start;
    cur = start;
    end = *(tzbsp_syscall_entry_t**)sections[i].end;

    /* Skip empty sections */
    if ((uint32)end - (uint32)start < sizeof(tzbsp_syscall_entry_t))
    {
      i++;
      continue;
    }

    while ((uint32)cur < (uint32)end)
    {
      if (smc_id == cur->smc_id)
      {
        return cur;
      }

      cur = (tzbsp_syscall_entry_t*)((uint8*)cur + sizeof(tzbsp_syscall_entry_t));
    }
    ++i;
  }
  return NULL;
}

/*
 * @brief Get the syscall count for a specific Owning entity.
 *
 * @return syscall_count
 */
uint32 tzbsp_get_syscall_count(uint32 owner_id)
{
  tzbsp_syscall_entry_t *cur = NULL;
  tzbsp_syscall_entry_t *start = NULL;
  tzbsp_syscall_entry_t *end = NULL;
  uint32 i = 0;
  uint32 syscall_count = 0;

  while (sections[i].start != NULL &&
         sections[i].end != NULL)
  {
    start = *(tzbsp_syscall_entry_t**)sections[i].start;
    cur = start;
    end = *(tzbsp_syscall_entry_t**)sections[i].end;

    /* Skip empty sections */
    if ((uint32)end - (uint32)start < sizeof(tzbsp_syscall_entry_t))
    {
      i++;
      continue;
    }

    while ((uint32)cur < (uint32)end)
    {
      if (owner_id == TZ_SYSCALL_OWNER_ID(cur->smc_id))
      {
        ++syscall_count;
      }

      cur = (tzbsp_syscall_entry_t*)((uint8*)cur + sizeof(tzbsp_syscall_entry_t));
    }
    ++i;
  }
  return syscall_count;
}

/**
 * Query for monitor calls, which are not present in the standard
 * system call registration.
 *
 * @param [in] id  Monitor call service/command ID.
 *
 * @return \c TRUE if the monitor call is supported, \c FALSE otherwise.
 */
boolean tzbsp_is_moncall_available(uint32 id)
{
  uint32 owner_id_mask = TZ_MASK_BITS(29,24);
  uint32 func_id_mask = TZ_MASK_BITS(15,0);

  /* We only want Owner ID and Function ID */
  id &= owner_id_mask | func_id_mask;

  if(TZ_POWER_COLLAPSE_ID         == id ||
     TZ_POWER_SPMI_DISABLE_BUS_ID == id ||
     TZ_IO_ACCESS_READ_ID         == id ||
     TZ_IO_ACCESS_WRITE_ID        == id)
  {
    return TRUE;
  }

  return FALSE;
}

/**
 * Checks if a system call is available.
 *
 * @param [in]  id      The searched system call ID.
 *
 * @return TRUE if found, FALSE otherwise.
 */
int tzbsp_is_service_available(uint32 smc_id, tzbsp_smc_rsp_t *rsp)
{
  boolean found = FALSE;

  if(tzbsp_syscall_lookup(smc_id))
  {
    found = TRUE;
  }
  else if(tzbsp_is_moncall_available(smc_id))
  {
    found = TRUE;
  }

  TZBSP_SET_RSP1(rsp, found);

  return E_SUCCESS;
}

/**
  @brief Do some direct validation of the request. The returned
         copy of the secure memory is validated.

  @param[in]       sc_req         - Pointer to the HLOS shared memory
  @param[in,out]   sc_req_copy    - Pointer to a secure memory area

  @return One of the following error codes
          <ul>
             <li> 0 on success, negative on failure.
          </ul>
*/
int tzbsp_copy_ns_mem(void * sc_req_copy,
                      void * sc_req,
                      uint32 size)
{
  if (NULL == sc_req || NULL == sc_req_copy || 0 == size)
    return -E_INVALID_ARG;

  if (!tzbsp_is_ns_range((uint8 *) sc_req, size))
    return -E_BAD_ADDRESS;

  if (tzbsp_is_ns_range((uint8 *) sc_req_copy, size))
    return -E_BAD_ADDRESS;

  /* Invalidate the header to make sure we read fresh DDR */
  tzbsp_dcache_inval_region(sc_req, size);

  memcpy(sc_req_copy, sc_req, size);

  /* Order of checking does matter so the HLOS cannot change parameters
     between IRQ's. */
  if (!tzbsp_is_ns_range((uint8 *) sc_req, size))
    return -E_BAD_ADDRESS;

  return E_SUCCESS;
}

/**
  @brief Perform a direct system call into TZBSP. Up to 10 arguments are
         supported.

  @param[in]      regs  - Pointer to non-secure register state
  @param[in]      func  - The function to be executed
  @param[in]      args  - The number of arguments

  @return One of the following error codes
          <ul>
             <li> 0 on success, negative on failure.
          </ul>
*/
int tzbsp_do_syscall(tzbsp_regs_with_rsp_t * regs, tzbsp_syscall_func_t func,
                     uint32 args, uint32 mink_service_id)
{
  int ret = 0;

  mink_syscall_info_t mink_syscall_info;
  ObjectArg obj_arg;

  Object obj = {NULL, NULL};

  if (mink_service_id != MINK_SERVICE_KERNEL_SYSCALL_ID && mink_service_id != MINK_SERVICE_TEST_ID)
  {

    obj = mink_service_obj_lookup(mink_service_id);

    if (Object_isNull(obj))
    {
      //TODO add logging
      return -1; /*did not get object back */
    }
    if (args > 10)
    {
      return -1;
    }

    mink_syscall_info.func = func;
    mink_syscall_info.nargs = args + 1;  /*Plus one to allow for the rsp pointer*/
    memcpy(&mink_syscall_info.args, &regs->reg[2], sizeof(uint32) * args);

    /*Copy in the response poitner as the last argument*/
    mink_syscall_info.args[args] = (uint32)regs->rsp;

    obj_arg.b.ptr = &mink_syscall_info;
    obj_arg.b.len = sizeof(mink_syscall_info_t);

    ret = Object_invokeABCD(obj, MINK_CMD_SYSCALL, &obj_arg, 1, 0, 0, 0);
  }
  else
  {
    /*Do this syscall without an invoke (just a function call) */
    /* TZBSP SYSCALL supports up to 10 arguments */
    switch (args)
    {
    case 0:
      ret = func(regs->rsp);
      break;

    case 1:
      ret = func(regs->reg[2], regs->rsp);
      break;

    case 2:
      ret = func(regs->reg[2], regs->reg[3], regs->rsp);
      break;

    case 3:
      ret = func(regs->reg[2], regs->reg[3], regs->reg[4], regs->rsp);
      break;

    case 4:
      ret = func(regs->reg[2], regs->reg[3], regs->reg[4], regs->reg[5], regs->rsp);
      break;

    case 5:
      ret = func(regs->reg[2], regs->reg[3], regs->reg[4],
                 regs->reg[5], regs->reg[6], regs->rsp);
      break;

    case 6:
      ret = func(regs->reg[2], regs->reg[3], regs->reg[4],
                 regs->reg[5], regs->reg[6], regs->reg[7], regs->rsp);
      break;

    case 7:
      ret = func(regs->reg[2], regs->reg[3], regs->reg[4],
                 regs->reg[5], regs->reg[6], regs->reg[7], regs->reg[8], regs->rsp);
      break;

    case 8:
      ret = func(regs->reg[2], regs->reg[3], regs->reg[4],
                 regs->reg[5], regs->reg[6], regs->reg[7],
                 regs->reg[8], regs->reg[9], regs->rsp);
      break;

    case 9:
      ret = func(regs->reg[2], regs->reg[3], regs->reg[4],
                 regs->reg[5], regs->reg[6], regs->reg[7],
                 regs->reg[8], regs->reg[9], regs->reg[10], regs->rsp);
      break;

    case 10:
      ret = func(regs->reg[2], regs->reg[3], regs->reg[4],
                 regs->reg[5], regs->reg[6], regs->reg[7],
                 regs->reg[8], regs->reg[9], regs->reg[10],
                 regs->reg[11], regs->rsp);
      break;

    default:
      ret = -1;
    }
  }
  return ret;
}

/**
   @brief Perform checks on AARCH and Reserved fields that must
          be consistent for all ARMv8 SMC calls.
 */
int tzbsp_validate_smc_id(uint32 smc_id)
{
  /* AARCH64 not supported in EL1 for 8994 */
  if (TZBSP_SYSCALL_AARCH64 == TZ_SYSCALL_AARCH(smc_id))
  {
    return SMC_ERR_AARCH64_NOT_SUPPORTED;
  }

  /* SMC ID reserved field must be zero for fast calls (IRQs disabled) */
  if (TZBSP_SYSCALL_IRQS_DISABLED == TZ_SYSCALL_IRQ(smc_id))
  {
    if (0 != TZ_SYSCALL_RESERVED(smc_id))
    {
      /* If Owner ID is a Trusted OS, check reserved field for all bits set.
       * Some legacy ARMv7 Trusted OS fast calls have reserved field set to 0xFF */
      if (IS_OWNER_TRUSTED_OS(TZ_SYSCALL_OWNER_ID(smc_id)) &&
          0xFF == TZ_SYSCALL_RESERVED(smc_id))
      {
        ;
      }
      else
      {
        return SMC_ERR_RESERVED_FIELD_MBZ;
      }
    }
  }

  return SMC_SUCCESS;
}

/**
   @brief Perform checks on all parameters
 */
int tzbsp_validate_syscall_parameters(uint32 param_id, tzbsp_regs_t *regs)
{
  uint32 i;
  uint32 reg_index;
  uint32 param_type = 0;
  uint32 nargs = TZ_SYSCALL_NUM_ARGS(param_id);

  if (NULL == regs)
  {
    return SMC_ERR_FAILURE;
  }

  for (i = 0; i < nargs; i++)
  {
    param_type = TZ_SYSCALL_ARG_TYPE(param_id, i);
	reg_index = i + TZBSP_PARAM_OFFSET; /* Parameter list starts at reg[2] */

    switch (param_type)
    {
      case TZBSP_SYSCALL_PARAM_TYPE_VAL:
      {
        break;
      }
      case TZBSP_SYSCALL_PARAM_TYPE_BUF_RO:
      case TZBSP_SYSCALL_PARAM_TYPE_BUF_RW:
      {
        if ((i + 1) >= nargs)
        {
          /* No buflen after buffer */
          return SMC_ERR_BUF_LEN_NOT_FOUND;
        }
        if (TZBSP_SYSCALL_PARAM_TYPE_VAL != TZ_SYSCALL_ARG_TYPE(param_id, (i + 1)))
        {
          /* No buflen after buffer */
          return SMC_ERR_BUF_LEN_NOT_FOUND;
        }

        if (!tzbsp_is_ns_range((uint8 *)regs->reg[reg_index],
                                        regs->reg[reg_index + 1]))
        {
          return SMC_ERR_BUF_PTR_NOT_NS;
        }
		
		/* The first page is mapped as invalid. Deny any buffers within this range. */
        if ((void*)regs->reg[reg_index] >= (void*)0x0 && 
            (void*)regs->reg[reg_index] <= (void*)MIN_MAP_SIZE)
        {
          return SMC_ERR_BUF_PTR_INVALID;
        }

        /* Invalidate the buffer to make sure we read fresh DDR */
        tzbsp_dcache_inval_region((uint8 *)regs->reg[reg_index],
          regs->reg[reg_index + 1]);

        ++i; /* Increment index to skip validation of buflen */
        break;
      }
      case TZBSP_SYSCALL_PARAM_TYPE_BUF_VALIDATION:
      {
        break;
      }
      default:
      {
        return SMC_ERR_FAILURE;
      }
    }//switch()
  }//for()

  return SMC_SUCCESS;
}

/*
 * @brief Parse, validate, and execute the SMC request.
 */
int tzbsp_execute_syscall(tzbsp_regs_t * regs, uint32* no_copy_modify)
{
  uint32 nargs = 0, int_flags = 0;
  uint32 smc_id = 0, param_id = 0;
  uint32 i = 0;
  int ret = 0;
  tzbsp_regs_with_rsp_t tmp_regs = {0};
  //tzbsp_smc_rsp_t smc_rsp = {0};
  tzbsp_syscall_entry_t *sc_entry = NULL;
  void* indirect_params_ptr = NULL;
  uint32 indirect_params_size = 0;
  uint32 retval = 0;

  if (NULL == regs) {
    return SMC_ERR_FAILURE;
  }

  if (NULL == smc_rsp)
  {
    smc_rsp = (tzbsp_smc_rsp_t *) tzbsp_malloc(sizeof(tzbsp_smc_rsp_t));
    if (NULL == smc_rsp)
    {
      return SMC_ERR_NO_MEMORY;
    }
  }

  memcpy(&tmp_regs, regs, sizeof(*regs));
  memset(smc_rsp, 0, sizeof(tzbsp_smc_rsp_t));
  tmp_regs.rsp = smc_rsp;

  /* SMC function ID is in r0 */
  smc_id = tmp_regs.reg[0];
  /* Param ID is in r1 */
  param_id = tmp_regs.reg[1];

  retval = tzbsp_validate_smc_id(smc_id);
  if (SMC_SUCCESS != retval)
  {
    return retval;
  }

  if ((sc_entry = tzbsp_syscall_lookup(smc_id)) == NULL)
    return SMC_ERR_SYSCALL_NOT_SUPPORTED;

  if (sc_entry->func == NULL)
    return SMC_ERR_SYSCALL_NOT_AVAILABLE;

  nargs = TZ_SYSCALL_NUM_ARGS(sc_entry->param_id);

  /* Some syscall registered for more args then they can handle */
  if (nargs > TZBSP_SYSCALL_MAX_ARG)
    return SMC_ERR_SYSCALL_NOT_AVAILABLE;

  if (TZ_SYSCALL_NUM_ARGS(param_id) != nargs)
    return SMC_ERR_NUM_ARGS_MISMATCH;

  if (param_id != sc_entry->param_id)
    return SMC_ERR_PARAM_ID_MISMATCH;

  /* Check for indirect parameter list when there are more than 4 function args
   * (we only have 4 regs for parameters in ARMv8 smc interface) */
  if (nargs > 4)
  {
    /* Last parameter register contains indirect parameter buffer.
     * Extract indirect regs from buffer and place in tmp_regs */
    indirect_params_ptr = (void *) tmp_regs.reg[5];
    indirect_params_size = nargs - 3;
    retval = tzbsp_copy_ns_mem(&tmp_regs.reg[5], indirect_params_ptr, sizeof(uint32) * indirect_params_size);
    if (E_SUCCESS != retval)
    {
      return SMC_ERR_INDIRECT_PARAM_PTR_NOT_NS;
    }
  }

  /* Must maintain support for pre-existing syscalls by skipping parameter validation */
  if (!(sc_entry->flags & TZ_SC_FLAG_LEGACY_API))
  {
    retval = tzbsp_validate_syscall_parameters(param_id, (tzbsp_regs_t *)&tmp_regs);
    if (SMC_SUCCESS != retval)
    {
      return retval;
    }
  }

  /* Set global syscall ID, to be used as debug info in certain error scenarios (i.e. malloc failures) */
  tzbsp_set_ns_syscall_id(sc_entry->smc_id);

  int_flags = tzbsp_int_read();
  if ((sc_entry->flags & TZ_SC_FLAG_ENABLE_INT) && (TZBSP_SYSCALL_IRQS_ENABLED == TZ_SYSCALL_IRQ(smc_id)) &&
      /* Do not enable IRQs when NS has IRQs disabled */
      !(regs->cpsr & TZBSP_IRQ_BIT))
  {
    tzbsp_int_enable(int_flags | TZBSP_IRQ_BIT);
  }

  entry_cpsr = tzbsp_int_read();
  /* Now we have what we need */
  ret = qsee_do_syscall((void *)tzbsp_do_syscall, (void *)&tmp_regs, (void *)(sc_entry->func), nargs, sc_entry->mink_service_id, sc_entry->smc_id);

  tzbsp_int_disable(TZBSP_IRQ_BIT);

  /* Reset global S/NS syscall IDs on exit */
  tzbsp_reset_ns_syscall_id();
  qsee_reset_secure_syscall_id();

  *no_copy_modify = FALSE;
  if (sc_entry->flags & TZ_SC_FLAG_NON_COPY_RETURN)
  {
    if (ret == E_SUCCESS)
    {
      *no_copy_modify = TRUE;
    }
  }

  /* Fill response */
  if (FALSE == *no_copy_modify)
  {
    /* Return BUSY when blocked on listener */
    if (mink_thread_busy())
    {
      if (-MINK_E_BUSY == ret)
      {
        return SMC_ERR_BUSY;
      }
    }

    /* Syscall elected to populated the response registers itself */
    if (sc_entry->flags & TZ_SC_FLAG_RSP)
    {
      if ((sc_entry->flags & TZ_SC_FLAG_O_RSP_ON_ERR) &&
          ret < 0)
      {
        /* Overwrite response with syscall return status on error */
        tmp_regs.rsp->rsp[0] = ret;
      }
    }
    else
    {
      /* Otherwise, populate with syscall return status */
      tmp_regs.rsp->rsp[0] = ret;
    }

    /* Fill registers with rsp */
    memcpy(&regs->reg[1], tmp_regs.rsp, sizeof(*tmp_regs.rsp));

    /* If syscall returned negative status value, return error */
    if (ret < 0)
    {
      TZBSP_LOG_ERR("syscall err! id = {0x%x}", smc_id);
      TZBSP_LOG_ERR("ret = {0x%x}, rsp = {0x%x, 0x%x, 0x%x}", ret,
          regs->reg[1], regs->reg[2], regs->reg[3]);
      return SMC_ERR_SYSCALL_FAILED;
    }
  }

  return SMC_SUCCESS;
}//tzbsp_execute_syscall()

/**
 * @brief Handle info syscalls defined in ARMv8 SMC spec
 *
 */
void tzbsp_execute_info_call(tzbsp_regs_t *regs)
{
  int32 retval;
  uint32 r0 = regs->reg[0];
  uint32 cmd = r0 & 0xFF;
  uint32 owner_id = TZ_SYSCALL_OWNER_ID(r0);

  /* Clear the optional return regs to prevent leaking data */
  memset(&regs->reg[1], 0, sizeof(tzbsp_smc_rsp_t));

  retval = tzbsp_validate_smc_id(r0);
  if (retval)
  {
    /* return error in r0 */
    regs->reg[0] = retval;
    return;
  }

  /* Must use SMC32 interface for all info calls */
  switch (cmd)
  {
    case 0x0: /* Call count */
    {
      /* Return syscall count */
      regs->reg[0] = tzbsp_get_syscall_count(owner_id);
      break;
    }
    case 0x1: /* UID */
    {
      /* Return 16 byte UUID as defined by RFC 4122 */
      regs->reg[0] = 1; /* TODO */
      regs->reg[1] = 2;
      regs->reg[2] = 3;
      regs->reg[3] = 4;
      break;
    }
    case 0x3: /* Revision details */
    {
      /* Return major version in r0, minor version in r1 */
      regs->reg[0] = TZBSP_MAJOR_VERSION; /* TODO */
      regs->reg[1] = TZBSP_MINOR_VERSION;
      break;
    }
    default:
    {
      /* return error in r0 */
      regs->reg[0] = SMC_ERR_SYSCALL_NOT_SUPPORTED;
      TZBSP_LOG_ERR("smc err! id = {0x%x}", cmd);
      TZBSP_LOG_ERR("ret = {%d}", (int32)regs->reg[0]);
      break;
    }
  }
}

/*
 * Syscall called from secure or monitor context
 *
 * @param[in] regs    The non-secure saved registers
 */
int tzbsp_syscall(tzbsp_regs_t * regs)
{
  int32 ret = 0;
  uint32 no_copy_modify = FALSE;
  int32 retVal = SMC_INFO_GENERIC_TYPE;
  uint32 r0 = regs->reg[0];

  tzbsp_int_enable(TZBSP_EA_BIT | TZBSP_FIQ_BIT);

  if (TZ_SVC_INFO_CALL_ID == TZ_SYSCALL_SVC_ID(r0) &&
      !IS_OWNER_TZ_APPS(TZ_SYSCALL_OWNER_ID(r0)))
  {
    /* ARMv8 Info */
    tzbsp_execute_info_call(regs);
  }
  else
  {
    /* ARMv8 SMC interface */
    ret = tzbsp_execute_syscall(regs, &no_copy_modify);

    /* Do no modify return registers for mode switch call */
    if (no_copy_modify == TRUE)
    {
      retVal = SMC_INFO_MODE_SWITCH_TYPE;
    }
    else
    {
      /* Return SMC result */
      regs->reg[0] = ret;

      /* Check if SMC parsing failed */
      if (SMC_SUCCESS != ret && SMC_ERR_SYSCALL_FAILED != ret)
      {
        /* Clear optional rsp regs to prevent leaking data */
        memset(&regs->reg[1], 0, sizeof(tzbsp_smc_rsp_t));

        /* Log the SMC ID and result */
        if (-SMC_ERR_BUSY == ret)
          TZBSP_LOG_ERR("smc busy! id = {0x%x}", r0);
        else
          TZBSP_LOG_ERR("smc err! id = {0x%x}", r0);

        TZBSP_LOG_ERR("ret = {%d}", (int32)regs->reg[0]);
      }
    }
  }

  return retVal;
}//tzbsp_syscall()

int tzbsp_exec_smc(const void *req, const void *rsp, uint32 rsp_len)
{
  return -E_NOT_SUPPORTED;
}

int tzbsp_exec_smc_ext(const void *req, uint32 req_len, 
                    const void *rsp, uint32 rsp_len)
{
  int32 ret = -E_NOT_SUPPORTED;

  ret = tzbsp_exec_smc_handler_ext(req,
                                   req_len,
                                   rsp,
                                   rsp_len);
  return ret;
}

int tzbsp_tzos_smc(uint32 app_id, const void *req, const void *rsp, uint32 rsp_len)
{
  int32 ret = -E_NOT_SUPPORTED;

  if ((uint32)rsp < (uint32)req)
    return -E_INVALID_ARG;

  ret = tzbsp_tzos_smc_handler(app_id, req, (uint32)rsp - (uint32)req, rsp, rsp_len);
  return ret;
}


