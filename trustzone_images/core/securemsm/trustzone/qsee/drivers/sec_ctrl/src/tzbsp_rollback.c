/*===========================================================================
 *    Copyright (c) 2010-2012 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
 ===========================================================================*/

/*===========================================================================
 *
 *                          EDIT HISTORY FOR FILE
 *
 *  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsee/drivers/sec_ctrl/src/tzbsp_rollback.c#1 $
 *  $DateTime: 2018/02/07 00:37:16 $
 *  $Author: mplp4svc $
 *
 *  when       who      what, where, why
 *  --------   ---      ------------------------------------
 *  09/21/11   nk      Created
 *
 *===========================================================================*/

/*===========================================================================
 *
 *           INCLUDE FILES
 *
 *=============================================================================*/
#include "tzbsp_sys.h"
#include "tzbsp_errno.h"
#include "qfprom.h"
#include "tzbsp_mem.h"
#include "tzbsp_mmu_cache.h"
#include "tzbsp_peripheral_info.h"

extern boolean tzbsp_boot_milestone_status;
int tzbsp_qfprom_rollback_write_row
(
  uint32   raw_row_address,
  uint32*  row_data,
  uint32   bus_clk_khz,
  uint32*  qfprom_api_status
)
{
  if ((NULL == qfprom_api_status)||
      (NULL == row_data))
  {
    return -E_INVALID_ARG;
  }

  if(!tzbsp_is_ns_range(row_data, sizeof(uint64)) ||
     !tzbsp_is_ns_range(qfprom_api_status, sizeof(uint32)))
  {
      return -E_BAD_ADDRESS;
  }

  tzbsp_dcache_inval_region(row_data, sizeof(uint64));

  *qfprom_api_status = QFPROM_ERR_UNKNOWN;

  if (!tzbsp_boot_milestone_status)
  {
    tzbsp_mem_map_device(TZBSP_MMP_SEC_CTRL_QFPROM_RAW);
    tzbsp_mem_map_device(TZBSP_MMP_SEC_CTRL_QFPROM_CORR);
    tzbsp_mem_map_device(TZBSP_MMP_SEC_CTRL_SEC_JTAG);

    *qfprom_api_status = qfprom_write_row(raw_row_address,
                             row_data,
                             bus_clk_khz);

    tzbsp_mem_unmap_device(TZBSP_MMP_SEC_CTRL_QFPROM_RAW);
    tzbsp_mem_unmap_device(TZBSP_MMP_SEC_CTRL_QFPROM_CORR);
    tzbsp_mem_unmap_device(TZBSP_MMP_SEC_CTRL_SEC_JTAG);

    tzbsp_dcache_flush_region(qfprom_api_status, sizeof(*qfprom_api_status));

    return E_SUCCESS;
  }
  else
  {
    tzbsp_dcache_flush_region(qfprom_api_status, sizeof(*qfprom_api_status));
    return -E_NOT_ALLOWED;
  }
}

