#ifndef TZBSP_SECBOOT_H
#define TZBSP_SECBOOT_H

/**
@file tzbsp_secboot.h
@brief TZ secboot interface
*/

/*===========================================================================
   Copyright (c) 2012 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsee/include/tzbsp_secboot.h#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $

when       who      what, where, why
--------   ---      ------------------------------------
07/30/12   jl       Initial version.

===========================================================================*/

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/

#include "secboot.h"
#include "secboot_hw.h"

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/

/*===========================================================================

                      PUBLIC FUNCTION DECLARATIONS

===========================================================================*/

/* SW Types for each of the subsystems; used during image authentication */
#define SECBOOT_SBL_SW_TYPE         0x0
#define SECBOOT_ADSP_SW_TYPE        0x4
#define SECBOOT_TZ_KERNEL_SW_TYPE   0x7
#define SECBOOT_APPSBL_SW_TYPE      0x9
#define SECBOOT_RPM_FW_SW_TYPE      0xA
#define SECBOOT_TZ_EXEC_SW_TYPE     0xC
#define SECBOOT_WCNSS_SW_TYPE       0xD
#define SECBOOT_VIDEO_SW_TYPE       0xE
#define SECBOOT_VPU_SW_TYPE         0xF
#define SECBOOT_BCSS_SW_TYPE        0x10
#define SECBOOT_QHEE_SW_TYPE        0x15
  
#define SECBOOT_INVALID_SW_TYPE     (-1)

/**
*
*  Structure to incorporate multiple ROT's for TZ APPS Requirement 
*
*/
typedef struct {
  uint8 root_of_trust[CEML_HASH_DIGEST_SIZE_SHA256];
} secboot_root_of_trust_type;

/**
*
*  Structure for holding the mulitple rot's and secboot fuse information.   
*
*/

typedef struct secboot_multiple_fuse_info_type
{
  secboot_root_of_trust_type  *multiple_root_of_trust;
  uint64       msm_hw_id;
  uint32       auth_use_serial_num;
  uint32       serial_num;
  boolean      use_root_of_trust_only; /**< Caller sets this variable to TRUE if
                                            secboot needs to use only root of trust from the
                                            supplied fuses */
  secboot_root_cert_fuse_info_type    root_sel_info;
  uint8        num_of_rots;           /** storing the number of rots **/      
} secboot_multiple_fuse_info_type;

/**
 * @brief Returns the debug state (enabled or disabled) as specified by PBL
 *
 * @return  TRUE if debug is enabled, FALSE otherwise
 */
boolean tzbsp_is_debug_enabled(void);

/**
 * @brief Returns the retail crash dump state (enabled or 
 *        disabled) as specified by PBL
 *
 * @return  TRUE if retail crash dump is enabled, FALSE 
 *          otherwise
 */
boolean tzbsp_is_retail_crash_dump_enable(void);

/**
 * @brief This function checks if the code needs to be authenticated. If
 *        authentication is enabled, callers MUST authenticate the
 *        code successfully before it is allowed to execute.
 *
 * @param code_segment   [in]  Segment of SECURE_BOOTn register holding
 *                             authentication information for the code
 *
 * @return \c FALSE if authentication is not required
 *         \c TRUE if authentication IS required
 *
 */
boolean tzbsp_secboot_hw_is_auth_enabled(uint32 code_segment);

/**
 * Authenticates an image using the secboot routines provided by PBL
 *
 * @param image_info[in]        Struct containing information about the image
 *                              to be authenticated
 * @param verified_info[in,out] Struct populated with info about the image
 *                              if authentication is successful.  Memory
 *                              allocated by the caller.
 *
 * @return  \c E_SUCCESS on success, \c -E_FAILURE otherwise
 */
int tzbsp_secboot_authenticate_image(secboot_image_info_type* image_info,
                                     secboot_verified_info_type* verified_info);

#endif /* TZBSP_SECBOOT_H */
