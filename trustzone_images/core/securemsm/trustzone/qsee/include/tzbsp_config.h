#ifndef TZBSP_CONFIG_H
#define TZBSP_CONFIG_H

/**
 * @brief All Trustzone features flags are provided here. The use of these
 * features is to aif in bringup/debugging. All features are enabled in release
 * builds.
*/

/*===========================================================================
   Copyright (c) 2010 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsee/include/tzbsp_config.h#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $


when       who      what, where, why
--------   ---      ------------------------------------
04/19/11   rv       Enable SSD for 8660
07/28/10   tk       Initial version
============================================================================*/

/*===========================================================================

                              DEFINES

===========================================================================*/

/* By default XPU interrupt handler doesn't dump the errored XPU syndrome
 * registers. Turning this flag to 1 will enable XPU syndrome dumping. */
#define TZBSP_XPU_SYNDROME_DUMPING              1

#endif  /* TZBSP_CONFIG_H */
