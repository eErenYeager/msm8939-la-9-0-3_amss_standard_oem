#ifndef TZBSP_MPU_H
#define TZBSP_MPU_H

/**
@file tzbsp_mpu.h
@brief Provides API for MPU functionality
*/

/*===========================================================================
   Copyright (c) 2012 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsee/include/tzbsp_mpu.h#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $

when       who      what, where, why
--------   ---      ------------------------------------
2012/06/01 sg       Add CP related new APIs
2011/12/19 qazib    Initial version.

===========================================================================*/

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/

/* Flags for tzbsp_mpu_protect_memory API */

/*
 * Specify if memory should not be cleared while unprotecting.
 * Effective only in debug environments
 */
#define TZBSP_MPU_PROT_MEM_FLAG_DONT_CLEAR_MEM 0x1


/** Deprecated */
typedef enum
{
  TZBSP_MEM_TAG_MM_CARVEOUT        = 0x00,
  TZBSP_MEM_TAG_MFC_SHAREDMEM      = 0x01,
  TZBSP_MEM_TAG_MDP_WRITEBACK      = 0x02,
  TZBSP_MEM_TAG_SA_REGION          = 0x03,
  TZBSP_MEM_TAG_QSEE_REQRSP_REGION = 0x04,
  TZBSP_MEM_TAG_SSD_REGION         = 0x05,
  TZBSP_MEM_TAG_SMMU_PT_MEM        = 0x06,
  TZBSP_MEM_TAG_MAX = 0x7FFFFFFF
} tzbsp_mem_tag_id_t;

/** Enumerates memory usage hints */
typedef enum
{
  /** Memory used for video SMMU page tables */
  TZBSP_MEM_MPU_USAGE_CP_PTBL             = 0x1,

  /** Memory used for QSEE Req/Rsp region  */
  TZBSP_MEM_MPU_USAGE_QSEE_REQRSP_REGION  = 0x2,

  /** Memory used for QSEE Secure Area region  */
  TZBSP_MEM_MPU_USAGE_QSEE_SA_REGION      = 0x3,

  /** Memory used for a variety of secure uses. */
  TZBSP_MEM_MPU_USAGE_SDDR_REGION         = 0x4,

  /** Memory used for QSEE Shared Buffer region  */
  TZBSP_MEM_MPU_USAGE_QSEE_SB_REGION      = 0x5,

  /** Max value */
  TZBSP_MEM_MPU_USAGE_MAX,

  TZBSP_MEM_MPU_USAGE_UNKNOWN  = 0x7fffffff
}tzbsp_mem_mpu_usage_e_type;

typedef struct tz_share_settings_s
{
  uint32 xpu;
  uint32 mask;
  uint32 rvmid;
  uint32 wvmid;
} tz_share_settings_t;

/*----------------------------------------------------------------------------
 * Function Declarations and Documentation
 * -------------------------------------------------------------------------*/

/**
 * Dynamically allocate and deallocate an MPU RG and set R/W permissions
 *
 * @param [in] start      start address
 * @param [in] size       size of the region (in bytes)
 * @param [in] usage_hint A tag that specifies this region's usage
 * @param [in] lock       lock or unlock
 * @param [in] flags      Optional Flags
 *
 * Currently supported flags:
 * TZBSP_MPU_PROT_MEM_FLAG_DONT_CLEAR_MEM Don't clear memory on unlock
 *                                        (1->dont clear, 0->clear). Effective
 *                                        only in debug environments.
 *
 * @return \c E_SUCCESS if successful, error code otherwise.
 */

int tzbsp_mpu_protect_memory ( uint32 start, uint32 size,
                               tzbsp_mem_mpu_usage_e_type usage_hint,
                               boolean lock, uint32 flags);
/**
  * Changes read/write permissions to unmapped memory for specified vmids
  * @param xpu   - The xpu which controls the memory that will be locked
  * @param mask  - A mask to control which vmid's will be affected, only vmids 
  *                 whose bit enabled in the mask will be changed
  * @param rvmid - Read vmid's which should have access to the protected memory region
  * @param wvmid - Write vmid's which...
  * @return E_SUCCESS on success, negative value on failure.
  */
int tzbsp_mpu_set_umr_vmid(uint32 xpu, uint32 mask, uint32 rvmids, uint32 wvmids);

/**
  * Finds a free resource group in xpu and assigns it to given memory range
  *   with given read and write permissions for vmids.
  * @param xpu   - The xpu which controls the memory that will be locked
  * @param start - Start of memory region to protect
  * @param size  - The size of the memory region to protect
  * @param rvmid - Read vmid's which should have access to the protected memory region
  * @param wvmid - Write vmid's which...
  * @return E_SUCCESS on success, negative value on failure.
  */
int tzbsp_mpu_pil_lock_memory ( uint32 xpu, uint32 start, 
                                uint32 size, uint32 rvmid, uint32 wvmid);

/**
  * If memory region from start to start+size is currently mapped
  *   for xpu, then unmap that memory region (remove the rg it is in)
  * @param xpu   - The xpu which controls the region of memory to unlock
  * @param start - Start of memory region to release
  * @param size  - The size of the memory region to protect
  * @return E_SUCCESS on success, negative value on failure.
  */
int tzbsp_mpu_pil_unlock_memory (uint32 xpu, uint32 start, uint32 size);

#endif /* TZBSP_MPU_H */

