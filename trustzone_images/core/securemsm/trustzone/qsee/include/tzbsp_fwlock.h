#ifndef TZBSP_FWLOCK_H
#define TZBSP_FWLOCK_H

/**
@file tzbsp_fwlock.h
@brief Provides API for the fwlock functionaility.
*/

/*===========================================================================
   Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
   Qualcomm Technologies Proprietary and Confidential.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

when       who      what, where, why
--------   ---      ------------------------------------
04/05/14   ablay    Initial version. 

===========================================================================*/

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Function Declarations and Documentation
 * -------------------------------------------------------------------------*/

#define FWLOCK_PRODUCE_HMAC_DIP_CID 0
#define FWLOCK_PRODUCE_HMAC_PIN_CID 1
#define FWLOCK_MAX_HMAC_DIP_USES    2	/* For DIP verification + DIP recovery */
#define FWLOCK_MAX_HMAC_PIN_USES    5	/* For PIN entry attempts */

/**
 * Calculate HMAC on a given buffer and write it to the provided
 * buffer. 
 * This API is called from HLOS via QSEE. 
 *
 * @param in_buf[in]
 * @param in_buf_size[in]
 * @param out_buf[out]
 * @param out_buf_size[in]
 * @param context[in]
 *
 * @return     0 on success, negative on failure
 */
int tzbsp_fwlock_produce_hmac(uint8* in_buf,
                              uint32 in_buf_size,
                              uint8* out_buf,
                              uint32 out_buf_size,
                              uint32 context);

/**
 * Calculate HMAC on a given buffer and write it to the provided
 * ns buffer. 
 * This API is called via SCM from LK. 
 *
 * @param in_buf[in]
 * @param in_buf_size[in]
 * @param out_buf[in]
 * @param out_buf_size[out]
 *
 * @return     0 on success, negative on failure
 */
int tzbsp_fwlock_produce_hmac_ns(uint8* in_buf,
                                 uint32 in_buf_size,
                                 uint8* out_buf,
                                 uint32 out_buf_size,
                                 uint32 context);

#endif /* TZBSP_FWLOCK_H */

