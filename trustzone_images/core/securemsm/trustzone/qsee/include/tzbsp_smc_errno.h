#ifndef TZBSP_SMC_ERRNO_H
#define TZBSP_SMC_ERRNO_H

/*===========================================================================

            S M C  E R R O R   N U M B E R   D E F I N I T I O N S

DESCRIPTION
  This contains the definition of the ARMv8 SMC framework return codes (error
  numbers).

Copyright (c) 2014-2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

$Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsee/include/tzbsp_smc_errno.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
07/17/2014 dp      Initial version.

===========================================================================*/

typedef enum
{
  SMC_INTERRUPTED                   =  1,  /* SMC call was interrupted                */
  SMC_SUCCESS                       =  0,  /* Success, requested syscall was called   */
  SMC_ERR_UNKNOWN_SMC_ID            = -1,  /* ARM-defined error of 0xFFFFFFFF         */
  SMC_ERR_SYSCALL_FAILED            = -2,  /* Syscall function returned failure       */
  SMC_ERR_SYSCALL_NOT_AVAILABLE     = -3,  /* No function associated with syscall def */
  SMC_ERR_RESERVED_FIELD_MBZ        = -4,  /* SMC ID err: reserved field must be zero */
  SMC_ERR_NUM_ARGS_MISMATCH         = -5,  /* Num args does not match func definition */
  SMC_ERR_INDIRECT_PARAM_PTR_NOT_NS = -6,  /* Ind. param ptr doesnt point to ns mem   */
  SMC_ERR_BUF_LEN_NOT_FOUND         = -7,  /* No buffer len following buffer ptr      */
  SMC_ERR_BUF_PTR_NOT_NS            = -8,  /* Buffer ptr does not point to ns mem     */
  SMC_ERR_NO_MEMORY                 = -9,  /* Heap is out of space                    */
  SMC_ERR_PARAM_ID_MISMATCH         = -10, /* Incorrect Param ID from NS world        */
  SMC_ERR_BUF_PTR_INVALID           = -11, /* Buf ptr is in first 4k (invalid) page   */
  SMC_ERR_BUSY                      = -12, /* TZ is busy waiting for listener rsp     */
  SMC_ERR_SESSION_ID_MISMATCH       = -13, /* Trusted OS Session ID mismatch error    */

  /* ARMv8 SMC spec: Must return 0xFFFFFFFF for following errors */
  SMC_ERR_SYSCALL_NOT_SUPPORTED     = SMC_ERR_UNKNOWN_SMC_ID,
  SMC_ERR_AARCH64_NOT_SUPPORTED     = SMC_ERR_UNKNOWN_SMC_ID,

  SMC_ERR_FAILURE                   = -INT32_MAX   /* Internal failure                */
} SmcErrnoType;

#endif /* TZBSP_SMC_ERRNO_H */
