#ifndef TZBSP_BLSP_H
#define TZBSP_BLSP_H

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include <comdef.h>
#include "tz_syscall_pub.h"


#define TZBSP_BLSP_GPIOS_SUPPORTED    6
#define TZBSP_BLSP_INDICES_SUPPORTED  (TZBSP_BLSP_GPIOS_SUPPORTED / 2)

#define TZBSP_BLSP_QUP_INFO(QUP_PROTOCOL, PERSISTENCE, GPIO_PROTECTION_TYPE, QUP_ID, QUP_RG_INDEX, BLSP_XPU_ID, GPIO_1, GPIO_2, GPIO_3, GPIO_4, GPIO_5, GPIO_6, NUM_GPIOS, GPIO_INDEX_1, GPIO_INDEX_2, GPIO_INDEX_3, GPIO_ADDR_1, GPIO_ADDR_2, GPIO_ADDR_3, GPIO_ADDR_4, GPIO_ADDR_5, GPIO_ADDR_6, GPIO_VALUE_1, GPIO_VALUE_2, GPIO_VALUE_3, GPIO_VALUE_4, GPIO_VALUE_5, GPIO_VALUE_6) \
{                       \
QUP_PROTOCOL,           \
PERSISTENCE,            \
GPIO_PROTECTION_TYPE,   \
QUP_ID,                 \
QUP_RG_INDEX,           \
BLSP_XPU_ID,            \
{GPIO_1, GPIO_2, GPIO_3, GPIO_4, GPIO_5, GPIO_6},  \
NUM_GPIOS,              \
{GPIO_INDEX_1, GPIO_INDEX_2, GPIO_INDEX_3},  \
{GPIO_ADDR_1, GPIO_ADDR_2, GPIO_ADDR_3, GPIO_ADDR_4, GPIO_ADDR_5, GPIO_ADDR_6},  \
{GPIO_VALUE_1, GPIO_VALUE_2, GPIO_VALUE_3, GPIO_VALUE_4, GPIO_VALUE_5, GPIO_VALUE_6}  \
}                       \

#ifndef TZBSP_BLSP_UNREFERENCED_PARAMETER
#define TZBSP_BLSP_UNREFERENCED_PARAMETER(p)   (p)
#endif


typedef enum TzBspBlspGpioProtType TzBspBlspGpioProtType;
enum TzBspBlspGpioProtType
{
   PROTECTION_TYPE_MPU,
   PROTECTION_TYPE_RPU
};

typedef enum TzBspBlspQupProtocol TzBspBlspQupProtocol;
enum TzBspBlspQupProtocol
{
   QUP_PROTOCOL_SPI,
   QUP_PROTOCOL_I2C
};

typedef enum TzBspBlspIsPersistence TzBspBlspIsPersistence;
enum TzBspBlspIsPersistence
{
   PERSISTENCE,
   NON_PERSISTENCE
};

typedef enum TzBspBlspQupId TzBspBlspQupId;
enum TzBspBlspQupId
{
   QUP_DEVICE_ID_1 = 1,
   QUP_DEVICE_ID_2,
   QUP_DEVICE_ID_3,
   QUP_DEVICE_ID_4,
   QUP_DEVICE_ID_5,
   QUP_DEVICE_ID_6,
   QUP_DEVICE_ID_7,
   QUP_DEVICE_ID_8,
   QUP_DEVICE_ID_9,
   QUP_DEVICE_ID_10,
   QUP_DEVICE_ID_11,
   QUP_DEVICE_ID_12,   
};

  
typedef struct tzbsp_blsp_qup_info_s
{
   TzBspBlspQupProtocol  protocol; /* Whether this QUP is used for SPI/I2C */
   TzBspBlspIsPersistence  isPersistence; /* Whether the protection should remain for ever starting from TZ Cold Boot and 
                                            * no dynamic switching of QUP b/w TZ and other sub-system. To achieve dynamic 
                                            * owner switching this field should be set to FALSE irrespective of whether cold
                                            * boot protection is enabled or not*/
   TzBspBlspGpioProtType  protectionType; /* Whether the MPU/RPU protection is used of GPIO's. For Bear it is MPU and for Badger it is RPU */   
   TzBspBlspQupId  qupId; /* QUP ID starting from 1 */
   uint32  uQupRgIndex; /* QUP protection settings. For both Bear & Badger targets */
   uint32  uBlspXpuId; /* XPU Index used for QUP protection */
   uint32  uGpios[TZBSP_BLSP_GPIOS_SUPPORTED]; /* This member will be used for GPIO numbers for Badger targets and 
                                                * for GPIO address range for Bear targets, 
                                                * NOTE:: Must set unused entries to Zero.
                                                *
                                                * Ex: If all 4 GPIO's are contiguous
                                                *	    uGpios[0] = 0x01014000 (Start Address)
                                                *     uGpios[1] = 0x01018000 (End Address)
                                                *   
                                                *     For first 2 contiguous GPIO's
                                                *     uGpios[0] = 0x01014000 (Start Address)
                                                *     uGpios[1] = 0x01016000 (End Address)
                                                *     For next 2 contiguous GPIO's
                                                *     uGpios[2] = 0x01017000 (Start Address)
                                                *     uGpios[3] = 0x01019000 (End Address)
                                                */
   uint32  uNumGpios; /* Total number of GPIO's to be protected in Badger targets and 
                       * also tells the number of GPIO's to be configured for both SPI/I2C in all targets */
   uint32  uGpioIndex[TZBSP_BLSP_INDICES_SUPPORTED]; /* Index used for Bear targets. 
                                                      * Number of indices used should be equal to no.of 
                                                      * sets of address ranges mentioned in uGPIOs member
                                                      * NOTE:: Must contact Qualcomm for index numbers.
                                                      *        Must set unused entries to Zero.
                                                      */
   uint32  uGpioConfigAddr[TZBSP_BLSP_GPIOS_SUPPORTED]; /* GPIO Register Addresses */
   uint32  uGpioConfigVal[TZBSP_BLSP_GPIOS_SUPPORTED]; /* GPIO Register values */
} TzBsp_Blsp_Qup_Info;

/**
 * System call to change BLSP ownership.
 *
 * @param[in] blsp_periph_num	Blsp Qup Id.
 * @param[in] owner_ss          Subsystem Owner ID.
 *
 * @return \c E_SUCCESS on success, an error code otherwise.
 */
int tzbsp_blsp_modify_ownership(uint32 blsp_periph_num, tzbsp_blsp_owner_ss_type owner_ss);

/**
 * @brief Protects QUP and GPIO and configures GPIO functionality.
 *
 * @return E_SUCCESS on success, an error code otherwise.
 */
int tzbsp_blsp_init(void);
#endif /* TZBSP_BLSP_H */


