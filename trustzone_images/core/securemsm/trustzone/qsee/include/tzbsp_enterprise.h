#ifndef TZBSP_ENTERPRISE_H
#define TZBSP_ENTERPRISE_H

/**
@file tzbsp_enterprise.h
@brief Provides API for teh enterprise security BSP functionality
*/

/*===========================================================================
   Copyright (c) 2013-2014 Qualcomm Technologies, Inc.  All Rights Reserved.
   Qualcomm Technologies Proprietary and Confidential.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

when       who      what, where, why
--------   ---      ------------------------------------
04/11/13   el       Initial version.

===========================================================================*/

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Function Declarations and Documentation
 * -------------------------------------------------------------------------*/

/**
 * Save Kernel/System partition hash.
 * This API is called via SCM from HLOS.
 *
 * @param partition_id[in]
 * @param hash[in]
 *
 * @return     0 on success, negative on failure
 */
int tzbsp_es_save_partition_hash(uint32 partition_id, uint32* hash);

/**
 * Get Kernel/System partition hash.
 * This API is called via QSEE from TZ App.
 *
 * @param partition_id[in]
 * @param hash[out]
 *
 * @return     0 on success, negative on failure
 */
int tzbsp_es_get_partition_hash(uint32 partition_id, uint32* hash);


#endif /* TZBSP_ENTERPRISE_H */

