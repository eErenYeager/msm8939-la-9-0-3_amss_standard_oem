#ifndef TZBSP_CLK_H
#define TZBSP_CLK_H

/**
@file tzbsp_clk.h
@brief Provides API for Clock functionality
*/

/*===========================================================================
   Copyright (c) 2012 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsee/include/tzbsp_clk.h#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $

when       who      what, where, why
--------   ---      ------------------------------------
2012/05/19 amen       Initial version

===========================================================================*/


/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/

/*
 * Level of clock frequence
*/

typedef enum
{
  INACTIVE      = 0x0,
  LOW           = 0x1,
  MEDIUM        = 0x2,
  HIGH          = 0x3,
  COUNT_MAX     = 0x4,
  MAX_ENTRY     = 0x7FFFFFFF
}clklev;

/*
 * Voteable clock resource, will 
 * ensure to vote for all dependencies 
*/

typedef enum
{
  CE1           = 0x1,
  CE2           = 0x2,
  CE3           = 0x3,
  COUNT         = 0x4,       
  MAX           = 0x7FFFFFFF
}resource;


/*----------------------------------------------------------------------------
 * Function Declarations and Documentation
 * -------------------------------------------------------------------------*/


/**
 * Clear the amt locked regions after verifying the current hash
 * of the amt table equals the stored hash value
 *  
 * @param [in] reqClient    Client requesting the clock
 * @param [in] reqClientlen length of reqClient name in bytes
 * @param [in] res_req      resource for which to vote clocks, all associated clocks
 *                          will be turned on and voted for, defined by resource enum
 * @param [in] level        set as per clklev enum above
 * @param [in] flags        flags (for future use)

 *  
 * @return \c E_SUCCESS if success,
 *            E_FAILURE otherwise
 *  
 */
uint32 tzbsp_set_bandwidth(void *reqClient, uint32 reqClientlen,
                           uint32 res_req, uint32 level, 
                           uint32 flags);

#endif
