/**
@file tzbsp_bulletin_board.h
@brief Provides simple message posting mechanism between QSEE applications
*/
/*===========================================================================
   Copyright (c) 2015 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

when         who      what, where, why
--------     ---      ------------------------------------
Jun/01/2015  el       Initial version.
===========================================================================*/
#ifndef TZBSP_BULLETIN_BOARD_H
#define TZBSP_BULLETIN_BOARD_H


/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include <stdint.h>
 
/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Function Declarations and Documentation
 * -------------------------------------------------------------------------*/

/**
 * @brief Post a note to QSEE bulletin board, under a specific category.
 *
 *        QSEE bulletin board contains several categories, each category only
 *        holds the recent note that was posted to that category, overwriting
 *        the previous one.
 *        Notes, cannot be deleted from the bulletin board and are available
 *        until they're overwritten by newer note or the device reboots.
 *        
 *        No permission model is applied on the bulletin board.
 *        Every QSEE application can post messages to all categories.
 *
 * @param[in] note_message         pointer to a buffer that holds the message to post.
 * @param[in] note_message_size    the size of the message in 'note_message' buffer.
 * @param[in] category_name        NULL terminated string that holds the name of the category
 *                                 this note will get posted under.
 * @param[in] category_name_size   the overall length of the category name buffer including the null termination byte.
 *
 * @return 0 on success, negative on failure (values are taken from IxErrno.h)
*/
int tzbsp_bulletin_board_post(const uint8_t* note_message,
                              uint32_t       note_message_size,
							  const char*    category_name,
							  uint32_t       category_name_size);


/**
 * @brief Read a note from QSEE bulletin board.
 *
 *        This function reads a note (if available) from a specific category.
 *        
 *        No permission model is applied on the bulletin board.
 *        Every QSEE application can read all available notes from all categories.
 *
 * @param[out]   note_message       a pointer to a pre-allocated user buffer that will hold the read message
 * @param[inout] note_message_size  a pointer to an integer that holds the size of the 'note_message' buffer.
 *                                  before this function returns, it alters that integer to hold the number
 *                                  of bytes that were actually copied into the buffer.
 * @param[in]    category_name      NULL terminated string that holds the name of the category we want to read a note from.
 * @param[in]    category_name_size the overall length of the category name buffer including the null termination byte.
 * @param[out]   sender_name        a pointer to a pre-allocated user buffer that will hold the name of the TA that sent the message.
 *                                  the string that will get copied will be NULL terminated.
 * @param[in]    sender_name_size   the size of the 'sender_name' buffer.
 *
 * @return 0 on success, negative on failure (values are taken from IxErrno.h)
*/
int tzbsp_bulletin_board_read(uint8_t*    note_message,
                              uint32_t*   note_message_size,
							  const char* category_name,
							  uint32_t    category_name_size,
							  char*       sender_name,
							  uint32_t    sender_name_size);

#endif // TZBSP_BULLETIN_BOARD_H
