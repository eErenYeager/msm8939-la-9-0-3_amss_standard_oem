#include "comdef.h"

/* For OEM to set. If it returns TRUE, then the Secure File System will enable its version 
 * anti-rollback feature which uses RPMB. Else, if FALSE is returned this feature will be 
 * disabled and there will be no dependency on RPMB be SFS, and consequently there will be
 * no version anti-rollback protection on the Secure File System Counter File. 
 * WARNING: This opens up SFS to rollack attacks causing security degradation. This feature
 * was implemented to stop rollback attacks and by disabling you are making the decision to
 * decrease security and open vulnerabilities.*/
boolean tzbsp_counter_enable_rpmb(void)
{
  return TRUE;
}

/* For OEM to set. If it returns TRUE, then the Keysotre feature will enable its version 
 * anti-rollback feature which uses RPMB. Else, if FALSE is returned this feature will be 
 * disabled and there will be no dependency on RPMB for keystore, and consequently there will be
 * no version anti-rollback protection on the Keystore Feature.
 * WARNING: This opens up Keystore to rollack attacks causing security degradation. This feature
 * was implemented to stop rollback attacks and by disabling you are making the decision to
 * decrease security and open vulnerabilities.*/
boolean tzbsp_keystore_enable_rpmb(void)
{
  return TRUE;
}

/* Note !!! This funciton is used for the below condition only: !!!
  A customer's refurbish device's eMMC is replaced, since RPMB key provision fuse is blown,
  RPMB auto production key provision won't happen for new eMMC.
  The return TRUE value can override RPMB key provision fuse and let RPMB be provsioned with
  new eMMC.
  This functions MUST always return FALSE for production devices. 
  Otherwise the security of the RPMB will be compromised */
boolean oem_allow_rpmb_key_provision(void)
{
  return FALSE;
}

/* For OEM to set.
 * OEM can set different threshold values for GPU DCVS algorithm through this function.
 * WARNING: Its highly discouraged to change the default value for GPU DCVS
 * algorithm. Default values are optimized for Power and Performance, OEM should
 * not change it. */
extern int tzbsp_oem_config_gpu_dcvs_penalty_para(uint32 pwr_level, uint32 penalty, uint32 down_penalty);
void oem_config_gpu_dcvs_para(void)
{
	// call to tzbsp_oem_config_gpu_dcvs_penalty_para()
}

/* For OEM to set.
 * OEM can choose to use the derived key from KDF for FDE. 
 * OEM_use_derived_key_for_fde: TRUE enables KDF usage for FDE. 
 *                              FALSE disables KDF usage for FDE. 
 * WARNING: This parameter controls whether the actual Master key or a key derived out of Master 
 * key will be used for FDE. Latter is more secure. Set to zero only for configurations 
 * that require backward compatibility with existing devices */ 
int tz_ce_use_derived_key(uint32 *OEM_use_derived_key_for_fde)
{
  *OEM_use_derived_key_for_fde = TRUE;
  return 0;
}
