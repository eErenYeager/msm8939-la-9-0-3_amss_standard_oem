/*===========================================================================
   Copyright (c) 2010 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsee/oem/msm8916/src/tzbsp_oem_syscall.c#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $


when       who     what, where, why
--------   ---     ------------------------------------------------------------
12/15/10   cap     Initial version.
=============================================================================*/

#include <comdef.h>
#include "tzbsp_syscall.h"

int tzbsp_oem_do_something(int a, void *b, void *c, int d)
{
  return 0;
}
