#ifndef TZBSP_CE_PIPE_CHIP_H
#define TZBSP_CE_PIPE_CHIP_H
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

TrustZone Crypto Pipe Configuaration Manager

General Description
Manages chipset specific configuaration for Keystore

Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/25/14   mr      Initial creation

============================================================================*/
/*===========================================================================
 
                           INCLUDE FILES

===========================================================================*/

#include "tzbsp_ce_pipe.h"

/*===========================================================================

                      PUBLIC DATA DECLARATIONS

===========================================================================*/

/**
* Enable the crypto clock for the crypto engine specified.
*
* @param [in] ce    Crypto engine number who's clock need to be enabled.
*
* @return E_SUCCESS If successfully voted for CE clock enablement.
*         E_FAILURE Failed to vote for CE clock enablement.
*/
int tz_ce_pipe_enable_clk(uint32 ce);

/**
* Disable the crypto clock for the crypto engine specified.
*
* @param [in] ce    Crypto engine number who's clock need to be disabled.
*
* @return E_SUCCESS If successfully voted for CE clock disablement.
*         E_FAILURE Failed to vote for CE clock disablement.
*/
int tz_ce_pipe_disable_clk(uint32 ce);

/**
* Sets the pipe key length for the current target.
*
* @param [in]  pipe_desc    Pipe description provided by QSEECOM.
* @param [out] pipe_key_len Length of device pipe key.
*
* @return E_SUCCESS If successfully set device specific pipe key length.
*         E_FAILURE Failed to set device specific pipe key length.
*/
int tz_ce_get_pipe_key_len(tz_pipe_desc_t pipe_desc, uint32* pipe_key_len);

/**
* Sets the crypto engine pipe base address.
*
* @param [in]  ce          Crypto engine number who's base to return.
* @param [in]  pipe_desc   Pipe description provided by QSEECOM.
* @param [out] base        Crypto engine pipe base address.
*
* @return E_SUCCESS If successfully set crypto engine pipe base address.
*         E_FAILURE Failed to set crypto engine pipe base address.
*/
int tz_ce_get_pipe_base(uint32 ce, tz_pipe_desc_t pipe_desc, uint32* base);

/**
* Returns number of crypto engines supported on current device.
*
* @return Number of crypto engines.
*/
uint32 tz_ce_get_num_pipes(void);

/**
* Sets the crypto engine specific data for current device, including number of 
* crypto engines and pipes.
*
* @param [out] num_pipes      Number of crypto pipes supported on device.
* @param [out] num_ces        Number of crypto engines supported on device.
*
* @return E_SUCCESS If successfully set crypto engine attributes.
*         E_FAILURE Failed to set crypto engine attributes.
*/
int tz_ce_get_pipe_validate_info(uint32* num_pipes, uint32* num_ces);

/**
* Returns true if the provided crypto engine and description is valid for the 
* use of a non-secure key.
*
* @param [in]  ce              Crypto engine number.
* @param [in]  pipe            Crypto engine pipe number.
* @param [in]  pipe_desc_msk   Pipe description provided by QSEECOM.
*
* @return TRUE  If crypto engine attributes are valid for non-secure key.
*         FALSE If crypto engine attributes are invalid for non-secure key.
*/
boolean tz_ce_pipe_is_valid_for_ns_key(uint32 ce, uint32 pipe, tz_pipe_desc_msk_t pipe_desc_msk);

#endif /* TZBSP_CE_PIPE_CHIP_H */

