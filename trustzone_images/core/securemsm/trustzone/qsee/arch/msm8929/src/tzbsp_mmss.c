/**
@file tzbsp_mmss.c
@brief Trustzone MultiMedia Subsystem related functions.

*/
/*===========================================================================
Copyright (c) 2012 Qualcomm Technologies, Inc.  All Rights Reserved.
Qualcomm Technologies Proprietary and Confidential.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8929/src/tzbsp_mmss.c#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $


when       who      what, where, why
--------   ---     ------------------------------------------------------------
11/10/12   cb      Initial version
=============================================================================*/

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include <comdef.h>
#include <HALhwio.h>
#include "tzbsp.h"
#include "tzbsp_config.h"
#include "tzbsp_errno.h"

/*----------------------------------------------------------------------------
 * Function Declarations and Documentation
 * -------------------------------------------------------------------------*/

/**
 * Reads the status of the HDMI link and hardware HDCP
 *  
 * @param [out] hdmi_enable HDMI output enabled
 *  
 * @param [out] hdmi_sense A panel is connected on the Hot Plug Detect
 *
 * @param [out] hdcp_auth HDCP authentication success.
 *
 * @return 0 on success
 */
int tzbsp_mmss_hdmi_status_read(uint32* hdmi_enable, uint32* hdmi_sense,
                                uint32* hdcp_auth)
{
  return -E_NOT_SUPPORTED;  
}


int qsee_enforce_hdmi_hdcp_encryption(boolean enforce, boolean *current_vote)
{
  /* HDCP Enhancement 1.X Feature is not supported on msm8916.  This is just a stub. */
  return 0;
}
