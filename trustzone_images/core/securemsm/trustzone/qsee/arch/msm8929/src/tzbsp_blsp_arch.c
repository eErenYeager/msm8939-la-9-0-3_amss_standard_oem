/*===========================================================================
  Copyright (c) 2016 by QUALCOMM Technologies, Incorporated.  All Rights Reserved.
  ===========================================================================*/

/*===========================================================================

  when       who     what, where, why
  --------   ---     ------------------------------------------------------------
  07/15/16   dpk     Created for 8936
  =============================================================================*/
/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/

#include "tzbsp_blsp.h"
#include "tzbsp_blsp_arch.h"
#include <HALhwio.h>
#include "tzbsp_hwio.h"
#include "tzbsp_xpu.h"
#include "tzbsp_errno.h"
#include "tzbsp_vmid_config.h"


extern int tzbsp_mpu_lock_area(uint32 mpu, uint32 index, uint32 start,
							   uint32 end, uint32 rvmids, uint32 wvmids);
extern int tzbsp_mpu_unlock_area(uint32 mpu, uint32 index);


int tzbsp_blsp_set_clock(uint32 qupId, boolean flag)
{
   uint32 blspClkMask = 0;
   uint32 regReadVal = 0;
   uint32 regWriteVal = 0;
   uint32 timeOut = 0;
   int res = E_SUCCESS;
   
   TZBSP_BLSP_UNREFERENCED_PARAMETER(qupId);
   
   /* Set BLSP1 AHB clock bit mask */
   blspClkMask = (1 << HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_BLSP1_AHB_CLK_ENA_SHFT);
   
   regReadVal = HWIO_IN(GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE) & blspClkMask;
   if (flag)
   {
      regWriteVal = (regReadVal | blspClkMask);      
   }
   else
   {
      regWriteVal = (regReadVal & ~blspClkMask);      
   }
   HWIO_OUT(GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE, regWriteVal);
   
   /* Read back the register to check whether the value is properly written 
    * to the register or not 
	*/
   regReadVal = HWIO_IN(GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE);

   if (regReadVal != regWriteVal)
   {
      TZBSP_LOG(TZBSP_MSG_ERROR,"tzbsp_blsp_set_clock(): Invalid clock status");
      res = E_FAILURE;
   }
   
   return res;
}

boolean tzbsp_blsp_is_clock_enabled(uint32 qupId)
{
   uint32 blspClkMask;

   TZBSP_BLSP_UNREFERENCED_PARAMETER(qupId);
   
   /* Set BLSP1 AHB clock bit mask */
   blspClkMask = (1 << HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_BLSP1_AHB_CLK_ENA_SHFT);

   if (HWIO_IN(GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE) & blspClkMask)
   {
      return TRUE;
   }
   else
   {
      return FALSE;
   }
}

int tzbsp_blsp_lock_gpio(const TzBsp_Blsp_Qup_Info *pTzBspQupInfo)
{  
   int32 i, res = E_SUCCESS;
   HAL_xpu2_XPU2Type xpuTlmm = HAL_XPU2_TLMM;
   uint8 gpioStartAddrIdx = 0, gpioEndAddrIdx = 1;
   uint32 mpuIndex = 0;      
      
   
   /* Configure GPIO MPU protection for Bear Targets */
   if(PROTECTION_TYPE_MPU == pTzBspQupInfo->protectionType) 
   {
      i = 0;
      while (i < TZBSP_BLSP_GPIOS_SUPPORTED)
	  {
         if ((pTzBspQupInfo->uGpios[gpioStartAddrIdx]) && (pTzBspQupInfo->uGpios[gpioEndAddrIdx]))
         {
	        res = tzbsp_mpu_lock_area(xpuTlmm,
                                      pTzBspQupInfo->uGpioIndex[mpuIndex],
                                      pTzBspQupInfo->uGpios[gpioStartAddrIdx],
                                      pTzBspQupInfo->uGpios[gpioEndAddrIdx],
                                      TZBSP_VMID_TZ_BIT, TZBSP_VMID_TZ_BIT);
            if (res != E_SUCCESS)
            {
               TZBSP_LOG(TZBSP_MSG_ERROR,"tzbsp_blsp_lock_gpio(): GPIO Lock Failed. res: %08x \n", res);
               break;
            }
		 
            /* Increment the start and end address index to check and configure next address range. */
            gpioStartAddrIdx += 2;
            gpioEndAddrIdx += 2;
            mpuIndex += 1;
            i += 2;
         }
         else
         {
            break;		 
         }

         if (E_SUCCESS != res) 
         {
            tzbsp_blsp_unlock_gpio(pTzBspQupInfo);
         }
      }
   }		
   else 
   {
      TZBSP_LOG(TZBSP_MSG_ERROR,"tzbsp_blsp_lock_gpio(): GPIO Protection type not supported \n");   
      res = -E_NOT_SUPPORTED;		   
   }   
  
   return res;
}

int tzbsp_blsp_unlock_gpio(const TzBsp_Blsp_Qup_Info *pTzBspQupInfo)
{  
   int32 i, res = E_SUCCESS;   
   HAL_xpu2_XPU2Type xpuTlmm = HAL_XPU2_TLMM;
   uint8 gpioStartAddrIdx = 0, gpioEndAddrIdx = 1;
   uint32 mpuIndex = 0;
   
  
   /* De-configure GPIO MPU protection for Bear Targets */
   if (PROTECTION_TYPE_MPU == pTzBspQupInfo->protectionType) 
   {      
      i = 0;
      while (i < TZBSP_BLSP_GPIOS_SUPPORTED)
      {
         if ((pTzBspQupInfo->uGpios[gpioStartAddrIdx]) && (pTzBspQupInfo->uGpios[gpioEndAddrIdx]))	  
         {
	        res = tzbsp_mpu_unlock_area(xpuTlmm,
                                        pTzBspQupInfo->uGpioIndex[mpuIndex]);		  
            if (res != E_SUCCESS)
            {
               TZBSP_LOG(TZBSP_MSG_ERROR,"tzbsp_blsp_unlock_gpio(): GPIO Un-Lock Failed. res: %08x \n", res);
               break;
            }
		 
            gpioStartAddrIdx += 2;
            gpioEndAddrIdx += 2;
            mpuIndex += 1;
            i += 2;
         }
         else 
         {
            break;
         }
      }         
   }
   else
   {
      TZBSP_LOG(TZBSP_MSG_ERROR,"tzbsp_blsp_unlock_gpio(): GPIO Protection type not supported \n");
      res = -E_NOT_SUPPORTED;	  
   }
     
   return res;
}

int tzbsp_blsp_gpio_configuration(const TzBsp_Blsp_Qup_Info *pTzBspQupInfo)
{
   int i;

   
   for (i = 0; i < pTzBspQupInfo->uNumGpios; i++)
   {
      out_dword(pTzBspQupInfo->uGpioConfigAddr[i], pTzBspQupInfo->uGpioConfigVal[i]);
   }
   
   return E_SUCCESS;
}