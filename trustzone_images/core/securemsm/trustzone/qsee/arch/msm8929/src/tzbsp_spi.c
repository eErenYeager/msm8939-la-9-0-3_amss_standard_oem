/**
@file tzbsp_spi.c
@brief Wrapper functions for the SPIPD interface in TZBSP


*/
/*===========================================================================
Copyright (c) 2013-2016 Qualcomm Technologies, Inc.  All Rights Reserved.
Qualcomm Technologies Proprietary and Confidential.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8929/src/tzbsp_spi.c#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $


when       who      what, where, why
--------   ---      ------------------------------------
09/20/16   dpk      Added support for exclusivity b/w TA's
11/20/14   ms       Added changes for GPIO Protection
09/10/14   vk       Updates from mainline SPI driver.
08/13/14   ms       Added support for all QUP devices
07/19/13   lk       Added calls to SpiDevice_SetPowerState.
11/10/12   th       Created

===========================================================================*/


/*===========================================================================

          INCLUDE FILES

============================================================================*/
#include "lkstring.h"
#include "kthread.h"
#include "tzbsp_target.h"
#include "tzbsp_errno.h"
#include "tzbsp_isr.h"
#include "tzbsp_sys.h"
#include "tzbsp_spi.h"
#include "tzbsp_xpu.h"
#include "tzbsp_xpu_def.h"
#include "tzbsp_vmid_config.h"
#include "tzbsp_mmu_cache.h"
#include "HALxpu2.h"

#include "SpiDevice.h"
#include "DALDeviceId.h"

#include <stringl/stringl.h>

#define SPI_MAX_NUM_DEVICES 12

typedef struct
{
  SPIDEVICE_HANDLE  ahSpi;
  uint32            dal_id;
  uint8_t           tzAppId[TZBSP_APP_ID_LEN];
  uint8_t           tzAppIdLen;

} spiDevice;

#define TZBSP_SPI_BLSP_MAX   1
#define TZBSP_SPI_QUP_MAX    6


/* Structure containing a reference to all the DAL SPI devices supported by this
 * interface and the SPI interrupt these devices are attached to.
 *
 * The values in this table need to be updated depending on the platform
 * configuration.
 */
static spiDevice spiDevices[TZBSP_SPIPD_DEVICE_COUNT] = {
  /* TS Controller - QUP5 */
  { NULL, 1, {0}, 0},
  { NULL, 2, {0}, 0},
  { NULL, 3, {0}, 0},
  { NULL, 4, {0}, 0},
  { NULL, 5, {0}, 0},
  { NULL, 6, {0}, 0},
};
static int tzbsp_map_id_2_arr_idx(tzbsp_spi_device_id_t device_id)
{
  int array_index = -1;
  switch (device_id)
  {
  case TZBSP_SPI_DEVICE_1:
    array_index = 0;
    break;
  case TZBSP_SPI_DEVICE_2:
    array_index = 1;
    break;
  case TZBSP_SPI_DEVICE_3:
    array_index = 2;
    break;
  case TZBSP_SPI_DEVICE_4:
    array_index = 3;
    break;
  case TZBSP_SPI_DEVICE_5:
    array_index = 4;
    break;
  case TZBSP_SPI_DEVICE_6:
    array_index = 5;
    break;
	
  default:
    break;
  }
  return array_index;
}

static void tzbsp_get_spi_info(const tzbsp_spi_config_t* p_config,
                              SpiDeviceInfoType* p_spi_info)
{
   p_spi_info->deviceParameters.eClockMode = (SpiClockModeType)p_config->spi_clk_always_on;
   p_spi_info->deviceParameters.eClockPolarity =(SpiClockPolarityType)p_config->spi_clk_polarity;
   p_spi_info->deviceParameters.eHSMode = (SpiHSModeType)p_config->hs_mode;
   p_spi_info->transferParameters.eLoopbackMode = (SpiLoopbackModeType)p_config->loopback;
   p_spi_info->deviceParameters.eShiftMode = (SpiShiftModeType)p_config->spi_shift_mode;
   p_spi_info->deviceParameters.u32DeassertionTime = 0;
   p_spi_info->deviceParameters.u32MinSlaveFrequencyHz = 0;
   p_spi_info->deviceParameters.u32MaxSlaveFrequencyHz = p_config->max_freq;
   p_spi_info->deviceParameters.eCSPolarity = (SpiCSPolarityType)p_config->spi_cs_polarity;
   p_spi_info->deviceParameters.eCSMode = (SpiCSModeType )p_config->spi_cs_mode;
   p_spi_info->deviceBoardInfo.nSlaveNumber = 0;
   p_spi_info->deviceBoardInfo.eCoreMode = SPI_CORE_MODE_MASTER;
   p_spi_info->transferParameters.nNumBits = p_config->spi_bits_per_word;
   p_spi_info->transferParameters.eTransferMode = SPI_TRANSFER_MODE_DEFAULT;
   p_spi_info->transferParameters.eInputPacking = SPI_INPUT_PACKING_DISABLED;
   p_spi_info->transferParameters.eOutputUnpacking = SPI_OUTPUT_UNPACKING_DISABLED;
   p_spi_info->transferParameters.slaveTimeoutUs = 0;
}

/* @copydoc tzbsp_spi_open */
int tzbsp_spi_open(tzbsp_spi_device_id_t device_id)
{
  thread_tz_app_id_t app_id    = {0};
  int                result    = E_SUCCESS;
  int32              spiResult;
  int                device_idx;

  if (device_id >= SPI_MAX_NUM_DEVICES )
  {
    TZBSP_LOG(TZBSP_MSG_ERROR,
              "tzbsp_spi_open() invalid device_id: %08x", device_id);
    return -E_FAILURE;
  }

  if ((device_idx = tzbsp_map_id_2_arr_idx(device_id)) < 0)
  {
    TZBSP_LOG(TZBSP_MSG_ERROR,
              "tzbsp_spi_open() invalid device_id: %08x", device_id);
    return -E_FAILURE;
  }

  /* Check if there is already an open client. This wrapper supports
   * only one open client per device */
  if (spiDevices[device_idx].tzAppIdLen)
  {
    TZBSP_LOG(TZBSP_MSG_ERROR,
              "tzbsp_spi_open() device_id already open: %08x", device_id);
    return -E_FAILURE;
  }

  do {
    /* Configure the SPI device if this has not been done before */
    if (NULL == spiDevices[device_idx].ahSpi)
    {
      spiResult = SpiDevice_Init(spiDevices[device_idx].dal_id,
                                &spiDevices[device_idx].ahSpi);

      if (spiResult != SPIDEVICE_RES_SUCCESS )
      {
        TZBSP_LOG(TZBSP_MSG_ERROR,
                  "tzbsp_spi_open() init error: %08x", spiResult);
        result = -E_FAILURE;
        break;
      }
    }

    /* Fetch the app id information */
    if ((E_SUCCESS        != thread_get_current_tz_app_id(&app_id)) || 
        (TZBSP_APP_ID_LEN != app_id.tz_app_id_len))
    {
      TZBSP_LOG(TZBSP_MSG_ERROR, "tzbsp_spi_open() failed to get app_id");
      result = -E_FAILURE;
      break;
    }
    
    spiResult = SpiDevice_SetPowerState(spiDevices[device_idx].ahSpi,
                                        SPIDEVICE_POWER_STATE_1);
    if (spiResult != SPIDEVICE_RES_SUCCESS )
    {
       TZBSP_LOG(TZBSP_MSG_ERROR,
                  "tzbsp_spi_open() init error: %08x", spiResult);
       result = -E_FAILURE;
       break;
    }

    /* Device is now open */
    (void) memscpy(spiDevices[device_idx].tzAppId, TZBSP_APP_ID_LEN, 
                   app_id.tz_app_id,               app_id.tz_app_id_len);
    spiDevices[device_idx].tzAppIdLen = app_id.tz_app_id_len;

    /* Lock the resources used by the SPI QUP */
    //result = tzbsp_spi_lock_resources(device_id);
    if (E_SUCCESS != result)
    {
      break;
    }

    /* Return results to caller */
  } while (0);

  if (E_SUCCESS != result) {
    /* Clean up everything that we can. Ignore the return value from
     * tzbsp_spi_close() as we want to return the reason the tzbsp_spi_open()
     * failed */
    (void) tzbsp_spi_close(device_id);
  }

  return result;
}

/* @copydoc tzbsp_spi_read */
int tzbsp_spi_read(tzbsp_spi_device_id_t        device_id,
                   const tzbsp_spi_config_t*      p_config,
                   tzbsp_spi_transaction_info_t*  p_read_info)
{
  SPIDEVICE_HANDLE   hSpi;
  int32              spiResult;
  thread_tz_app_id_t app_id     = {0};
  int                result     = E_SUCCESS;
  int                device_idx = -1;
  SpiDeviceInfoType  spi_info;
  SpiDataAddrType    readBuf;
  
  do {
    /* Check for valid input parameters */
    if (((uint32)device_id        >= SPI_MAX_NUM_DEVICES ) ||
        (p_config                 == NULL) ||
        (p_read_info              == NULL))
    {
      TZBSP_LOG(TZBSP_MSG_ERROR, "tzbsp_spi_read() invalid input parameters");
      result = -E_FAILURE;
      break;
    }

    if ((device_idx = tzbsp_map_id_2_arr_idx(device_id)) < 0)
    {
      TZBSP_LOG(TZBSP_MSG_ERROR,
                "tzbsp_spi_read() invalid device_id: %08x", device_id);
      return -E_FAILURE;
    }

    /* Fetch the current app id information */
    if ((E_SUCCESS        != thread_get_current_tz_app_id(&app_id)) || 
        (TZBSP_APP_ID_LEN != app_id.tz_app_id_len))
    {
      TZBSP_LOG(TZBSP_MSG_ERROR, "tzbsp_spi_read() failed to get app_id");
      result = -E_FAILURE;
      break;
    }
    
    /* Check the device is open for reading */
    if (memcmp(spiDevices[device_idx].tzAppId, app_id.tz_app_id, TZBSP_APP_ID_LEN))
    {
      TZBSP_LOG(TZBSP_MSG_ERROR, "tzbsp_spi_read() SPI device not owned/open");
      result = -E_FAILURE;
      break;
    }
    
    /*get SPI config*/
    tzbsp_get_spi_info(p_config, &spi_info);

    /*Set up SPI buffers*/
    readBuf.virtualAddr = p_read_info->buf_addr;
    {
      uint32 phy_addr;
      if (tzbsp_vtop((uint32)p_read_info->buf_addr, &phy_addr))
      {
        result = -E_FAILURE;
        break;
      }
      readBuf.physicalAddr = (void *)phy_addr;
    }

    hSpi = spiDevices[device_idx].ahSpi;

    /* If the read buffer isn't cache granule aligned/multiple flush it
     * so the invalidate after the SPI read isn't destructive.  If aligned
     * only need to invalidate now, since any dirty data will get
     * overwritten anyways
     */
    if((uint32)p_read_info->buf_addr & (KRAIT_CACHE_GRANULE_SZ-1) || 
       p_read_info->buf_len % KRAIT_CACHE_GRANULE_SZ != 0)
    {
      tzbsp_dcache_flush_region(p_read_info->buf_addr, p_read_info->buf_len);
    }
    else
    {
      tzbsp_dcache_inval_region(p_read_info->buf_addr, p_read_info->buf_len);
    }

    spiResult = SpiDevice_Read(hSpi, &spi_info, &readBuf, p_read_info->buf_len);

    tzbsp_dcache_inval_region(p_read_info->buf_addr, p_read_info->buf_len);

    if (spiResult != SPIDEVICE_RES_SUCCESS )
    {
      p_read_info->total_bytes = 0;
      TZBSP_LOG(TZBSP_MSG_ERROR, "tzbsp_spi_read() error: %08x", spiResult);
      result = -E_FAILURE;
      break;
    }
     p_read_info->total_bytes = p_read_info->buf_len;

  } while (0);

  return result;
}

/* @copydoc tzbsp_spi_write */
int tzbsp_spi_write(tzbsp_spi_device_id_t       device_id,
                    const tzbsp_spi_config_t*     p_config,
                    tzbsp_spi_transaction_info_t* p_write_info)
{
  SPIDEVICE_HANDLE   hSpi;
  thread_tz_app_id_t app_id     = {0};
  int32              spiResult  =  0;
  int                result     =  E_SUCCESS;
  int                device_idx = -1;
  SpiDeviceInfoType  spi_info;
  SpiDataAddrType    writeBuf;
  

  do {
    /* Check for valid input parameters */
    if (((uint32)device_id        >= SPI_MAX_NUM_DEVICES ) ||
        (p_config                 == NULL) ||
        (p_write_info             == NULL))
    {
      TZBSP_LOG(TZBSP_MSG_ERROR, "tzbsp_spi_write() invalid input parameters");
      result = -E_FAILURE;
      break;
    }

    if((device_idx = tzbsp_map_id_2_arr_idx(device_id)) < 0)
    {
      TZBSP_LOG(TZBSP_MSG_ERROR,
                "tzbsp_spi_write() invalid device_id: %08x", device_id);
      return -E_FAILURE;
    }
    
    /* Fetch the app id information */
    if ((E_SUCCESS        != thread_get_current_tz_app_id(&app_id)) || 
        (TZBSP_APP_ID_LEN != app_id.tz_app_id_len))
    {
      TZBSP_LOG(TZBSP_MSG_ERROR, "tzbsp_spi_write() failed to get app_id");
      result = -E_FAILURE;
      break;
    }

    /* Check the device is open for writing */
    if (memcmp(spiDevices[device_idx].tzAppId, app_id.tz_app_id, TZBSP_APP_ID_LEN))
    {
      TZBSP_LOG(TZBSP_MSG_ERROR, "tzbsp_spi_write() SPI device not owned/open");
      result = -E_FAILURE;
      break;
    }
    
    /*get SPI config*/
    tzbsp_get_spi_info(p_config,&spi_info);

    /*Set up SPI buffers*/
    writeBuf.virtualAddr = p_write_info->buf_addr;
    {
      uint32 phy_addr;
      if (tzbsp_vtop((uint32)p_write_info->buf_addr,&phy_addr))
      {
        result = -E_FAILURE;
        break;
      }
      writeBuf.physicalAddr = (void *)phy_addr;
    }

    hSpi = spiDevices[device_idx].ahSpi;

    tzbsp_dcache_flush_region(p_write_info->buf_addr, p_write_info->buf_len);

    spiResult = SpiDevice_Write(hSpi, &spi_info, &writeBuf, 
                                p_write_info->buf_len);
   
    if (spiResult != SPIDEVICE_RES_SUCCESS )
    {
      p_write_info->total_bytes = 0;
      TZBSP_LOG(TZBSP_MSG_ERROR, "tzbsp_spi_write() error: %08x", spiResult);
      result = -E_FAILURE;
      break;
    }
    p_write_info->total_bytes = p_write_info->buf_len;
  
  } while (0);

  return result;
}

int tzbsp_spi_full_duplex(tzbsp_spi_device_id_t device_id,
                          const tzbsp_spi_config_t* p_config,
                          tzbsp_spi_transaction_info_t* p_write_info,
                          tzbsp_spi_transaction_info_t* p_read_info)
{
  SPIDEVICE_HANDLE   hSpi;
  thread_tz_app_id_t app_id     = {0};
  int32              spiResult  =  0;
  int                result     =  E_SUCCESS;
  int                device_idx = -1;
  SpiDeviceInfoType  spi_info;
  SpiDataAddrType    writeBuf;
  SpiDataAddrType    readBuf;
  

  do {
    /* Check for valid input parameters */
    if (((uint32)device_id        >= SPI_MAX_NUM_DEVICES ) ||
        (p_config                 == NULL) ||
        (p_write_info             == NULL) ||
        (p_read_info              == NULL))
    {
      TZBSP_LOG(TZBSP_MSG_ERROR, "tzbsp_spi_full_duplex() invalid input parameters");
      result = -E_FAILURE;
      break;
    }

    if((device_idx = tzbsp_map_id_2_arr_idx(device_id))< 0)
    {
      TZBSP_LOG(TZBSP_MSG_ERROR,
                "tzbsp_spi_full_duplex() invalid device_id: %08x", device_id);
      return -E_FAILURE;
    }

    /* Fetch the app id information */
    if ((E_SUCCESS        != thread_get_current_tz_app_id(&app_id)) || 
        (TZBSP_APP_ID_LEN != app_id.tz_app_id_len))
    {
      TZBSP_LOG(TZBSP_MSG_ERROR, "tzbsp_spi_full_duplex() failed to get app_id");
      result = -E_FAILURE;
      break;
    }
    
    /* Check the device is open */
    if (memcmp(spiDevices[device_idx].tzAppId, app_id.tz_app_id, TZBSP_APP_ID_LEN))
    {
      TZBSP_LOG(TZBSP_MSG_ERROR, "tzbsp_spi_full_duplex() SPI device not open");
      result = -E_FAILURE;
      break;
    }
    
    /*get SPI config*/
    tzbsp_get_spi_info(p_config,&spi_info);

    /*Set up SPI buffers*/
    writeBuf.virtualAddr = p_write_info->buf_addr;
    
    {
      uint32 phy_addr;
      if (tzbsp_vtop((uint32)p_write_info->buf_addr,&phy_addr))
      {
        result = -E_FAILURE;
        break;
      }
      writeBuf.physicalAddr = (void *)phy_addr;
    }
    /*Set up SPI buffers*/
    readBuf.virtualAddr = p_read_info->buf_addr;
    {
      uint32 phy_addr;
      if (tzbsp_vtop((uint32)p_read_info->buf_addr,&phy_addr))
      {
        result = -E_FAILURE;
        break;
      }
      readBuf.physicalAddr = (void *)phy_addr;
    }

    hSpi = spiDevices[device_idx].ahSpi;

    /* If the read buffer isn't cache granule aligned/multiple flush it
     * so the invalidate after the SPI read isn't destructive.  If aligned
     * only need to invalidate now, since any dirty data will get
     * overwritten anyways
     */
    if((uint32)p_read_info->buf_addr & (KRAIT_CACHE_GRANULE_SZ-1) || 
       p_read_info->buf_len % KRAIT_CACHE_GRANULE_SZ != 0)
    {
      tzbsp_dcache_flush_region(p_read_info->buf_addr, p_read_info->buf_len);
    }
    else
    {
      tzbsp_dcache_inval_region(p_read_info->buf_addr, p_read_info->buf_len);
    }

    tzbsp_dcache_flush_region(p_write_info->buf_addr, p_write_info->buf_len);

    spiResult = SpiDevice_WriteRead(hSpi, &spi_info, &readBuf, 
                                    p_read_info->buf_len, &writeBuf, 
                                    p_write_info->buf_len);

    tzbsp_dcache_inval_region(p_read_info->buf_addr, p_read_info->buf_len);
    

    if (spiResult != SPIDEVICE_RES_SUCCESS )
    {
      p_read_info->total_bytes = 0;
      p_write_info->total_bytes = 0;
      TZBSP_LOG(TZBSP_MSG_ERROR, "tzbsp_spi_fullduplex() error: %08x", spiResult);
      result = -E_FAILURE;
      break;
    }
    p_read_info->total_bytes = p_read_info->buf_len;
    p_write_info->total_bytes = p_write_info->buf_len;
    
  } while (0);

  return result;
}
/* @copydoc tzbsp_spi_close */
int tzbsp_spi_close(tzbsp_spi_device_id_t device_id)
{
  /* Close will attempt to clean up as much as possible. A failure in
   * one step will be recorded, but the other steps will also be
   * executed */
  SPIDEVICE_HANDLE   hSpi;
  thread_tz_app_id_t app_id    = {0};
  int                result    =  E_SUCCESS;
  int32              spiResult =  0;
  int                device_idx;

  if (device_id >= SPI_MAX_NUM_DEVICES)
  {
    TZBSP_LOG(TZBSP_MSG_ERROR,
              "tzbsp_spi_close() invalid device_id: %08x", device_id);
    return -E_FAILURE;
  }

  /* Fetch the app id information */
  if ((E_SUCCESS        != thread_get_current_tz_app_id(&app_id)) || 
      (TZBSP_APP_ID_LEN != app_id.tz_app_id_len))
  {
    TZBSP_LOG(TZBSP_MSG_ERROR, "tzbsp_spi_close() failed to get app_id");
    return -E_FAILURE;
  }
  
  if ((device_idx = tzbsp_map_id_2_arr_idx(device_id)) < 0)
  {
    TZBSP_LOG(TZBSP_MSG_ERROR,
              "tzbsp_spi_close() invalid device_id: %08x", device_id);
    return -E_FAILURE;
  }
  
  /* Check if the caller is the open client */
  if (memcmp(spiDevices[device_idx].tzAppId, app_id.tz_app_id, TZBSP_APP_ID_LEN))
  {
    TZBSP_LOG(TZBSP_MSG_ERROR,
              "tzbsp_spi_close() device_id not opened/owned: %08x", device_id);
    return -E_FAILURE;
  }
  
  hSpi      = spiDevices[device_idx].ahSpi;
  spiResult = SpiDevice_SetPowerState(spiDevices[device_idx].ahSpi,
                                      SPIDEVICE_POWER_STATE_0);
                                      
  if (spiResult != SPIDEVICE_RES_SUCCESS)
  {
     TZBSP_LOG_ERR("tzbsp_spi_close() init error: %08x", spiResult);
     return -E_FAILURE;
  }
    
  if ((spiResult = SpiDevice_DeInit(hSpi)) != SPIDEVICE_RES_SUCCESS)
  {
    TZBSP_LOG(TZBSP_MSG_ERROR,
              "tzbsp_spi_close() de-init error: %08x", spiResult);
    result = -E_FAILURE;
  }
  
  memset(spiDevices[device_idx].tzAppId, 0, TZBSP_APP_ID_LEN);
  spiDevices[device_idx].ahSpi      = NULL;
  spiDevices[device_idx].tzAppIdLen = 0;
  
  return result;
}

/* @copydoc tzbsp_spi_configure_protection 
 *
 * The current configuration in BAM is hard coded, so
 * we don't do it here.
*/
int tzbsp_spi_configure_protection(const tzbsp_qup_protection_type *prot)
{
  (void) prot;
  /* Functionality not tested on 8936*/
  return E_SUCCESS;
}
