#ifndef QSEE_MRM_ARCH_H
#define QSEE_MRM_ARCH_H

/**
@file qsee_mrm_arch.h
@brief Memory Region Manager Architecture Specific Extensions/Constructs
*/

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include <comdef.h>
#include "memory_defs.h"

p_addr_t* qsee_mrm_get_code_base_addrs_arch(void);
size_t*   qsee_mrm_get_code_lens_len_arch(void);

p_addr_t  qsee_mrm_get_demand_paging_code_base_addr_arch(void);
size_t    qsee_mrm_get_demand_paging_code_len_arch(void);

p_addr_t qsee_mrm_get_sbl_base_addr_arch(void);
size_t qsee_mrm_get_sbl_base_len_arch(void);

mem_block_t* qsee_mrm_get_venus_xpu_init_map_arch(void);
unsigned int qsee_mrm_get_venus_xpu_init_map_len_arch(void);

#endif /* #define QSEE_MRM_ARCH_H */

