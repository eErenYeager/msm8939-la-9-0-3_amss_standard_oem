/**
 * @file tzbsp_sec_core.c
 * @brief Control all target hardware NS aware register settings
 *
 */

/*===========================================================================
 *    Copyright (c) 2010-2012 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
 ===========================================================================*/

/*===========================================================================
 *
 *                          EDIT HISTORY FOR FILE
 *
 *  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8929/src/tzbsp_sec_core.c#1 $
 *  $DateTime: 2018/02/07 00:37:16 $
 *  $Author: mplp4svc $
 *
 *  when       who      what, where, why
 *  --------   ---      ------------------------------------
 *  10/06/10   cap      Created
 *
 *===========================================================================*/

/*===========================================================================
 *
 *           INCLUDE FILES
 *
 *=============================================================================*/
#include <HALhwio.h>

#include "tzbsp_cp15.h"
#include "tzbsp_target.h"
#include "tzbsp.h"
#include "tzbsp_errno.h"
#include "tzbsp_log.h"
#include "tzbsp_xpu_def.h"
#include "tzbsp_xpu_config.h"
#include "tzbsp_config.h"
#include "tzbsp_chipset.h"
#include "tzbsp_sys.h"
#include "tzbsp_hwio.h"
#include "tzbsp_secboot.h"
#include "tzbsp_peripheral_info.h"
#include "tzbsp_mem.h"
#include "tz_mc.h"
#include "tzbsp_ext_os.h"

#define RCP15_HACR(reg) MRC15(reg, 4, c1, c1, 7)
#define WCP15_HACR(reg) MCR15(reg, 4, c1, c1, 7)

#define ANSACR_L2REMOTE  (1 << 24)
#define ANSACR_L2VR      (1 << 23)
#define ANSACR_L2PWR     (1 << 21)
#define ANSACR_L2DB      (1 << 19)
#define ANSACR_L2EH      (1 << 18)
#define ANSACR_L2LK      (1 << 17)
#define ANSACR_L2CR      (1 << 16)
#define ANSACR_CPUVR     (1 << 9)
#define ANSACR_CPUPWR    (1 << 8)
#define ANSACR_CPUDB     (1 << 5)
#define ANSACR_CPUEH     (1 << 4)
#define ANSACR_CPULKTLB  (1 << 3)
#define ANSACR_CPULKIC   (1 << 2)
#define ANSACR_CPULKDC   (1 << 1)
#define ANSACR_CPUCR     (1 << 0)

#define APCS_APC_SECURE_TST_NS      (1 << 2)

#define APCS_WDOG_SECURE_NS_ALL        (HWIO_APCS_WDT_TMR1_WDOG_SECURE_RMSK)
#define APCS_APC_SECURE_NS_ALL         (HWIO_APCS_ALIAS0_APC_SECURE_RMSK)
#define APCS_GLB_SECURE_NS_ALL         (HWIO_APCS_ALIAS0_GLB_SECURE_RMSK)
#define APCS_L2_SAW2_SECURE_NS_ALL     (HWIO_APCLUS0_L2_SAW2_SECURE_RMSK)
#define APCS_C0_PLL_SECURE_NS_ALL      (HWIO_APCS_C0_PLL_SECURE_RMSK)
#define APCS_C1_PLL_SECURE_NS_ALL      (HWIO_APCS_C1_PLL_SECURE_RMSK)
#define APCS_CCI_PLL_SECURE_NS_ALL     (HWIO_APCS_CCI_PLL_SECURE_RMSK)
#define APCS_SAW2_SECURE_NS_ALL        (HWIO_APCS_ALIAS0_SAW2_SECURE_RMSK)
#define CCI_SAW2_SECURE_NS_ALL         (HWIO_CCI_SAW2_SECURE_RMSK)
#define APCS_BANKED_SAW2_SECURE_NS_ALL (HWIO_APCS_BANKED_SAW2_SECURE_RMSK)
#define APCS_PLL_SECURE_NS_ALL         (HWIO_APCS_ALIAS0_PLL_SECURE_RMSK)

#define HWIO_MERGE(var, name, xx) \
  (var) |= ((xx) << HWIO_##name##_SHFT) & (HWIO_##name##_BMSK)

extern uint8 tzbsp_milestone_complete;
extern uint32 tzbsp_is_power_cluster_present(void);

/*----------------------------------------------------------------------------
 * Static Variable Definitions
 * -------------------------------------------------------------------------*/

/* Debug flag is stored in non-cached memory as L0/L1/L2 dumping features use
 * function \c tzbsp_allow_cache_dumping. */
__attribute__((section ("TZBSP_UNCACHED_ZI_VARS"))) uint32 debug_flag;

static boolean tzbsp_allow_jtag_log       = FALSE;
static boolean tzbsp_allow_jtag_log_init  = FALSE;

static uint32  tzbsp_spiden_disable      = FALSE;
static boolean tzbsp_spiden_disable_init = FALSE;

/** Sets the one-time writable debug OVERRIDE fuses */
static void tzbsp_dbg_fuse_override(uint32 debug)
{
  uint32 override_2 = 0;
  uint32 override_3 = 0;

  if (debug)
  {
    override_2 |= 1 << (HWIO_SHFT(OVERRIDE_2, OVRID_DAP_DEVICEEN_DISABLE));
    override_2 |= 1 << (HWIO_SHFT(OVERRIDE_2, OVRID_DAP_NIDEN_DISABLE));
    override_2 |= 1 << (HWIO_SHFT(OVERRIDE_2, OVRID_DAP_DBGEN_DISABLE));
    override_2 |= 1 << (HWIO_SHFT(OVERRIDE_2, OVRID_APPS_NIDEN_DISABLE));
    override_2 |= 1 << (HWIO_SHFT(OVERRIDE_2, OVRID_APPS_DBGEN_DISABLE));
    override_2 |= 1 << (HWIO_SHFT(OVERRIDE_2, OVRID_SPARE1_DISABLE));
    override_2 |= 1 << (HWIO_SHFT(OVERRIDE_2, OVRID_SPARE0_DISABLE));
    override_2 |= 1 << (HWIO_SHFT(OVERRIDE_2, OVRID_VENUS_0_DBGEN_DISABLE));
    override_2 |= 1 << (HWIO_SHFT(OVERRIDE_2, OVRID_RPM_DAPEN_DISABLE));
    override_2 |= 1 << (HWIO_SHFT(OVERRIDE_2, OVRID_RPM_WCSS_NIDEN_DISABLE));
    override_2 |= 1 << (HWIO_SHFT(OVERRIDE_2, OVRID_RPM_DBGEN_DISABLE));
    override_2 |= 1 << (HWIO_SHFT(OVERRIDE_2, OVRID_WCSS_DBGEN_DISABLE));

    override_3 |= 1 << (HWIO_SHFT(OVERRIDE_3, OVRID_DAP_SPNIDEN_DISABLE));
    override_3 |= 1 << (HWIO_SHFT(OVERRIDE_3, OVRID_DAP_SPIDEN_DISABLE));
    override_3 |= 1 << (HWIO_SHFT(OVERRIDE_3, OVRID_APPS_SPNIDEN_DISABLE));
    override_3 |= 1 << (HWIO_SHFT(OVERRIDE_3, OVRID_APPS_SPIDEN_DISABLE));
    override_3 |= 1 << (HWIO_SHFT(OVERRIDE_3, OVRID_SPDM_SECURE_MODE));

  }

  tzbsp_mem_map_device(TZBSP_MMP_SEC_CTRL_SECURITY_CONTROL);

  HWIO_OUT(OVERRIDE_2, override_2);
  HWIO_OUT(OVERRIDE_3, override_3);

  tzbsp_mem_unmap_device(TZBSP_MMP_SEC_CTRL_SECURITY_CONTROL);
}

/* Security Control contains the ACC, QFPROM and SMC (Security Mode Control).
 * SMC contains the SW configuration registers.
 *
 * In SMC FEATURE_CONFIG* contain all of debug disables.
 */
void tzbsp_security_control_config(uint32 debug)
{
  volatile uint32 val;  /* Volatile so compiler doesn't optimize out HWIO_IN/OUT below */

  /* The following writable Security Control registers are currently ignored by
   * TZ:
   *   - OVERRIDE_1
   *   - CAPT_SEC_GPIO
   *   - APP_PROC_CFG, the expectation is that this will come outside TZ and is
   *     hard-coded (i.e. earlier in the boot chain).
   *   - MSS_PROC_CFG TODO How should this be configured?
   *   - QFPROM_CLKCTL, Security Control clock is enabled (reset value). There is
   *     now HW clock gating for Security Control and the clock gating here is
   *     purely SW controlled. As multiple masters in the system need to
   *     independently read fuse values, this clock gating cannot really be used.
   *   - TEST_BUS_SEL, this is a Surf related debug feature.
   *   - BOOT_PARTITION_SELECT
   */

  tzbsp_mem_map_device(TZBSP_MMP_SEC_CTRL_SECURITY_CONTROL);

  /* Full chip power collapse isn't supported. Read and write \c CAPT_SEC_GPIO
   * to lock down its one-time writable value. */
  val = HWIO_IN(CAPT_SEC_GPIO);
  HWIO_OUT(CAPT_SEC_GPIO, val);

  /* OVERRIDE_0, all overrides disabled. */
  HWIO_OUT(OVERRIDE_0, 0);

  tzbsp_mem_unmap_device(TZBSP_MMP_SEC_CTRL_SECURITY_CONTROL);

  tzbsp_dbg_fuse_override(debug);

  tzbsp_mem_map_device(TZBSP_MMP_SEC_CTRL_SECURITY_CONTROL);

  /* SPDM_DYN_SECURE_MODE, full monitoring allowed only during debug. */
  if(debug)
  {
    /* SECURE_MODE=0, disabled, full monitoring allowed. */
    HWIO_OUTF(SPDM_DYN_SECURE_MODE, SECURE_MODE, 0);
  }
  else
  {
    /* SECURE_MODE=1, enabled, reduced monitoring. */
    HWIO_OUTF(SPDM_DYN_SECURE_MODE, SECURE_MODE, 1);
  }

  tzbsp_mem_unmap_device(TZBSP_MMP_SEC_CTRL_SECURITY_CONTROL);
}

static void tzbsp_set_boot_remap( void )
{
  /* monitor CPU entry point. */
  uint32 val = SCL_MON_CODE_BASE;
  val |= (HWIO_APCS_ALIAS1_BOOT_START_ADDR_SEC_REMAP_EN_BMSK);
  HWIO_OUT(APCS_ALIAS1_BOOT_START_ADDR_SEC, val);
  /* Future chip resets should be in AArch64 */
  HWIO_OUT(APCS_ALIAS1_AA64NAA32_REG, 1);

  if(tzbsp_is_power_cluster_present())
  {

    val = SCL_MON_CODE_BASE;
    val |= (HWIO_APCS_ALIAS0_BOOT_START_ADDR_SEC_REMAP_EN_BMSK);
    HWIO_OUT(APCS_ALIAS0_BOOT_START_ADDR_SEC, val);
    /* Future chip resets should be in AArch64 */
    HWIO_OUT(APCS_ALIAS0_AA64NAA32_REG, 1);
  }

}

/*
 * Configure the security of the Krait Subsystem (ACPS)
 *
 * The KPSS is responsible for:
 *  - CPU Clock control/distribution (acc).
 *  - Global CPU Clock control/distribution (gcc)
 *  - General Purpose Timer(s) (tmr configured as general purpose)
 *  - Debug Timer (tmr configured for debug)
 *  - Watch Dog timer(s) (wdt)
 *  - System Interrupt Controller (SIC or QGIC or External)
 *  - Test Interface Controller (tic).
 *  - AHB Bus Interface and decoder (ahb).
 *  - Per CPU voltage control (avs)
 *  - Per CPU power control (spm)
 */
static void tzbsp_set_scss(uint32 debug)
{
  register uint32 val = APCS_APC_SECURE_NS_ALL;

  /* Configuration that need to do only once. This registers are Cx Domain & retain state */
  if(!tzbsp_milestone_complete)
  {
    HWIO_OUT(APCS_WDT_TMR1_WDOG_SECURE, APCS_WDOG_SECURE_NS_ALL);

    if (!debug)
    {
      val &= ~APCS_APC_SECURE_TST_NS;
    }

    /* Configuration for Power cluster. Single cluster MSM's will not have Power Cluster */
    if(tzbsp_is_power_cluster_present())
    {
      HWIO_OUT(APCS_ALIAS0_APC_SECURE, val);
      HWIO_OUT(APCS_ALIAS1_APC_SECURE, val);
      HWIO_OUT(APCS_ALIAS2_APC_SECURE, val);
      HWIO_OUT(APCS_ALIAS3_APC_SECURE, val);

      HWIO_OUT(APCS_ALIAS0_GLB_SECURE, APCS_GLB_SECURE_NS_ALL);

      HWIO_OUT(APCLUS0_L2_SAW2_SECURE, APCS_L2_SAW2_SECURE_NS_ALL);

      HWIO_OUT(APCS_ALIAS0_SAW2_SECURE, APCS_SAW2_SECURE_NS_ALL);
      HWIO_OUT(APCS_ALIAS1_SAW2_SECURE, APCS_SAW2_SECURE_NS_ALL);
      HWIO_OUT(APCS_ALIAS2_SAW2_SECURE, APCS_SAW2_SECURE_NS_ALL);
      HWIO_OUT(APCS_ALIAS3_SAW2_SECURE, APCS_SAW2_SECURE_NS_ALL);

      /* Krait start address can only be changed by secure side. */
      HWIO_OUTF(APCS_ALIAS0_CFG_SECURE, GLB_CTL, 0x0);
    }

    HWIO_OUT(APCS_ALIAS4_APC_SECURE, val);
    HWIO_OUT(APCS_ALIAS5_APC_SECURE, val);
    HWIO_OUT(APCS_ALIAS6_APC_SECURE, val);
    HWIO_OUT(APCS_ALIAS7_APC_SECURE, val);

    HWIO_OUT(APCS_ALIAS1_GLB_SECURE, APCS_GLB_SECURE_NS_ALL);

    HWIO_OUT(APCS_CLUS1_L2_SAW2_SECURE, APCS_L2_SAW2_SECURE_NS_ALL);

    HWIO_OUT(APCS_ALIAS4_SAW2_SECURE, APCS_SAW2_SECURE_NS_ALL);
    HWIO_OUT(APCS_ALIAS5_SAW2_SECURE, APCS_SAW2_SECURE_NS_ALL);
    HWIO_OUT(APCS_ALIAS6_SAW2_SECURE, APCS_SAW2_SECURE_NS_ALL);
    HWIO_OUT(APCS_ALIAS7_SAW2_SECURE, APCS_SAW2_SECURE_NS_ALL);

    /* Krait start address can only be changed by secure side. */
    HWIO_OUTF(APCS_ALIAS1_CFG_SECURE, GLB_CTL, 0x0);

    HWIO_OUT(APCS_C1_PLL_SECURE, APCS_C1_PLL_SECURE_NS_ALL);
    
    /* Configuration for Power cluster. Single cluster MSM's will not have Power Cluster */
    if(tzbsp_is_power_cluster_present())
    {
      HWIO_OUT(APCS_C0_PLL_SECURE, APCS_C0_PLL_SECURE_NS_ALL);
    }
    HWIO_OUT(APCS_CCI_PLL_SECURE, APCS_CCI_PLL_SECURE_NS_ALL);

    HWIO_OUT(CCI_SAW2_SECURE, CCI_SAW2_SECURE_NS_ALL);

    /* Set the start address to boot from trustzone */
    tzbsp_set_boot_remap();
  }
}

/* Configure access to, and set-up, the CP15 counter module */
static void tzbsp_timer_security_config( void )
{
  /* CNTFRQ can only be written by EL3; set it here */
  mon_set_timer_freq(HWIO_IN(APCS_QTMR1_QTMR_AC_CNTFRQ));

  /* Configure Qtimer Counter Secure Reg to allow QTMR access
     Need to do only once. This registers are Cx Domain & retain state */
  if(!tzbsp_milestone_complete)
  {
    HWIO_OUT(APCS_QTMR0_QTMR_AC_CNTSR, 0x7B);

    /* Set Qtimer CNTVOFF Reg to 0 for all 7 frames */
    HWIO_OUTI(APCS_QTMR0_QTMR_AC_CNTACR_n, 0, 0x3F);
    HWIO_OUTI(APCS_QTMR0_QTMR_AC_CNTACR_n, 1, 0x3F);
    HWIO_OUTI(APCS_QTMR0_QTMR_AC_CNTACR_n, 3, 0x3F);
    HWIO_OUTI(APCS_QTMR0_QTMR_AC_CNTACR_n, 4, 0x3F);
    HWIO_OUTI(APCS_QTMR0_QTMR_AC_CNTACR_n, 5, 0x3F);
    HWIO_OUTI(APCS_QTMR0_QTMR_AC_CNTACR_n, 6, 0x3F);

    /* Set Qtimer CNTVOFF Reg to 0 for all 7 frames */
    HWIO_OUTI(APCS_QTMR0_QTMR_AC_CNTVOFF_LO_n, 0, 0x0);
    HWIO_OUTI(APCS_QTMR0_QTMR_AC_CNTVOFF_HI_n, 0, 0x0);
    HWIO_OUTI(APCS_QTMR0_QTMR_AC_CNTVOFF_LO_n, 1, 0x0);
    HWIO_OUTI(APCS_QTMR0_QTMR_AC_CNTVOFF_HI_n, 1, 0x0);
    HWIO_OUTI(APCS_QTMR0_QTMR_AC_CNTVOFF_LO_n, 2, 0x0);
    HWIO_OUTI(APCS_QTMR0_QTMR_AC_CNTVOFF_HI_n, 2, 0x0);
    HWIO_OUTI(APCS_QTMR0_QTMR_AC_CNTVOFF_LO_n, 3, 0x0);
    HWIO_OUTI(APCS_QTMR0_QTMR_AC_CNTVOFF_HI_n, 3, 0x0);
    HWIO_OUTI(APCS_QTMR0_QTMR_AC_CNTVOFF_LO_n, 4, 0x0);
    HWIO_OUTI(APCS_QTMR0_QTMR_AC_CNTVOFF_HI_n, 4, 0x0);
    HWIO_OUTI(APCS_QTMR0_QTMR_AC_CNTVOFF_LO_n, 5, 0x0);
    HWIO_OUTI(APCS_QTMR0_QTMR_AC_CNTVOFF_HI_n, 5, 0x0);
    HWIO_OUTI(APCS_QTMR0_QTMR_AC_CNTVOFF_LO_n, 6, 0x0);
    HWIO_OUTI(APCS_QTMR0_QTMR_AC_CNTVOFF_HI_n, 6, 0x0);

    /* Configure Qtimer Counter Secure Reg to allow QTMR access*/
    HWIO_OUT(APCS_QTMR1_QTMR_AC_CNTSR, 0x7B);

    /* Set Qtimer CNTVOFF Reg to 0 for all 7 frames */
    HWIO_OUTI(APCS_QTMR1_QTMR_AC_CNTACR_n, 0, 0x3F);
    HWIO_OUTI(APCS_QTMR1_QTMR_AC_CNTACR_n, 1, 0x3F);
    HWIO_OUTI(APCS_QTMR1_QTMR_AC_CNTACR_n, 3, 0x3F);
    HWIO_OUTI(APCS_QTMR1_QTMR_AC_CNTACR_n, 4, 0x3F);
    HWIO_OUTI(APCS_QTMR1_QTMR_AC_CNTACR_n, 5, 0x3F);
    HWIO_OUTI(APCS_QTMR1_QTMR_AC_CNTACR_n, 6, 0x3F);

    /* Set Qtimer CNTVOFF Reg to 0 for all 7 frames */
    HWIO_OUTI(APCS_QTMR1_QTMR_AC_CNTVOFF_LO_n, 0, 0x0);
    HWIO_OUTI(APCS_QTMR1_QTMR_AC_CNTVOFF_HI_n, 0, 0x0);
    HWIO_OUTI(APCS_QTMR1_QTMR_AC_CNTVOFF_LO_n, 1, 0x0);
    HWIO_OUTI(APCS_QTMR1_QTMR_AC_CNTVOFF_HI_n, 1, 0x0);
    HWIO_OUTI(APCS_QTMR1_QTMR_AC_CNTVOFF_LO_n, 2, 0x0);
    HWIO_OUTI(APCS_QTMR1_QTMR_AC_CNTVOFF_HI_n, 2, 0x0);
    HWIO_OUTI(APCS_QTMR1_QTMR_AC_CNTVOFF_LO_n, 3, 0x0);
    HWIO_OUTI(APCS_QTMR1_QTMR_AC_CNTVOFF_HI_n, 3, 0x0);
    HWIO_OUTI(APCS_QTMR1_QTMR_AC_CNTVOFF_LO_n, 4, 0x0);
    HWIO_OUTI(APCS_QTMR1_QTMR_AC_CNTVOFF_HI_n, 4, 0x0);
    HWIO_OUTI(APCS_QTMR1_QTMR_AC_CNTVOFF_LO_n, 5, 0x0);
    HWIO_OUTI(APCS_QTMR1_QTMR_AC_CNTVOFF_HI_n, 5, 0x0);
    HWIO_OUTI(APCS_QTMR1_QTMR_AC_CNTVOFF_LO_n, 6, 0x0);
    HWIO_OUTI(APCS_QTMR1_QTMR_AC_CNTVOFF_HI_n, 6, 0x0);
  }
}

int tzbsp_sec_processor_target(uint32 debug)
{
  debug_flag = debug;

  tzbsp_timer_security_config();

  tzbsp_set_scss(debug);

  return 0;
}


/*
 * Checking if memory can be dumped in case of Modem SSR
 * Allowed if:
 * if JTAG enable is ON in the cert
 * if secboot is disabled
 * if DEBUG FUSE (SPIDEN) is not blown
 *
 * @note This function is called in contexts where cached global variables are
 *       not allowed. Variable \debug_flag is linked into non-cached area.
 */
boolean tzbsp_allow_unlock_xpu(void)
{
  boolean retval = FALSE;

  if (!tzbsp_spiden_disable_init)
  {
    tzbsp_spiden_disable_init = TRUE;

    tzbsp_mem_map_device(TZBSP_MMP_SEC_CTRL_QFPROM_RAW);
    tzbsp_spiden_disable =
      HWIO_INF(QFPROM_RAW_OEM_CONFIG_ROW0_MSB, APPS_SPIDEN_DISABLE);
    tzbsp_mem_unmap_device(TZBSP_MMP_SEC_CTRL_QFPROM_RAW);
  }

  /* By default memory dumping is denied. */
  retval = (debug_flag ||
            (!tzbsp_secboot_hw_is_auth_enabled(1) && !tzbsp_spiden_disable));

  return retval;
}

/**
 * Checks whether cache dumping should be allowed. Currently the rules for this
 * are the same as for XPU unlocking.
 *
 * @note This function is called in contexts where cached global variables are
 *       not allowed.
 */
boolean tzbsp_allow_cache_dumping(void)
{
  return tzbsp_allow_unlock_xpu();
}

/**
 * Checks whether non-secure debug FIQ should be allowed. Currently the rules
 * for this are the same as for XPU unlocking.
 */
boolean tzbsp_allow_ns_debug_fiq(void)
{
  return tzbsp_allow_unlock_xpu();
}

/**
 * Checks if HLOS can affect the behaviour of the secure watchdog; currently
 * the rules for this are the same as XPU unlocking.
 */
boolean tzbsp_allow_sec_wdog_debug(void)
{
  return tzbsp_allow_unlock_xpu();
}

/* @return TRUE if JTAG logging is allowed, FALSE otherwise. */
boolean tzbsp_allow_jtag_logging(void)
{
  if(!tzbsp_allow_jtag_log_init)
  {
    tzbsp_allow_jtag_log_init = TRUE;

    tzbsp_mem_map_device(TZBSP_MMP_SEC_CTRL_SECURITY_CONTROL);

    /* If debug disable fuses have been blown, JTAG logging makes no sense. */
    if(!HWIO_INF(OEM_CONFIG1, ALL_DEBUG_DISABLE) &&
       !HWIO_INF(FEATURE_CONFIG2, QC_APPS_DBGEN_DISABLE) &&
       !HWIO_INF(OEM_CONFIG1, APPS_DBGEN_DISABLE) &&
       !HWIO_INF(FEATURE_CONFIG2, QC_APPS_SPIDEN_DISABLE) &&
       !HWIO_INF(OEM_CONFIG1, APPS_SPIDEN_DISABLE))
    {
      tzbsp_allow_jtag_log = TRUE;
    }

    tzbsp_mem_unmap_device(TZBSP_MMP_SEC_CTRL_SECURITY_CONTROL);
  }

  return tzbsp_allow_jtag_log;
}

/**
 * Wrapper function for tzbsp_dbg_fuse_override() for TZ kernel
 */
int tzbsp_jtag_override(uint32 debug)
{
  tzbsp_dbg_fuse_override(debug);
  return E_SUCCESS;
}

/**
 * Wrapper function to release HLOS HW lock taken by the calling core
 * before making monitor power collapse call. HW Mutex #7 is used to
 * provide synchronization between TZ and HLOS.
 */

static void tzbsp_release_hw_lock_flat_mapped(void);

void tzbsp_release_hw_lock(void)
{
  if (tzbsp_sysdbg_flat_mapped()) 
  { 
    tzbsp_release_hw_lock_flat_mapped(); 
  } 
  else 
  { 
    /* TODO: HWIO doesn't currently work */
    /* HWIO_OUTI(TCSR_MUTEX_REG_n, 7, 0); */
    *((volatile uint32*)(0x0190C000 + TZBSP_SANDBOX_RELOCATE_OFFSET)) = 0;
  }
}

/* Use the C pre-processor to re-define the below macros based on 
 * physical addresses. Note that the new defines are valid until 
 * the end of the current translation unit */ 
#undef  CORE_TOP_CSR_BASE 
#define CORE_TOP_CSR_BASE                  CORE_TOP_CSR_BASE_PHYS 

static void tzbsp_release_hw_lock_flat_mapped( void ) 
{ 
  *((volatile uint32*) 0x0190C000) = 0;
}
