/*===========================================================================
   Copyright (c) 2011-2013 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8929/src/tzbsp_mpu.c#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $


when       who     what, where, why
--------   ---     ------------------------------------------------------------
06/21/12   sg      Implement CP related APIs
06/01/12   sg      Add CP related new APIs
03/23/12   rkul    Added changes for CP 2.0 with fallback to CP 1.0 for backward compatibility
03/23/12   amen    added function to free RG if error return
03/12/12   sg      Floor addresses to 4k boundary before unlocking
07/13/11   tk      Initial version.
=============================================================================*/


/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include <string.h>
#include <comdef.h>
#include <HALxpu2.h>

#include "tzbsp.h"
#include "tzbsp_config.h"
#include "tzbsp_errno.h"
#include "tzbsp_mem.h"
#include "tzbsp_target.h"
#include "tzbsp_xpu.h"
#include "tzbsp_xpu_def.h"
#include "tzbsp_xpu_config.h"
#include "tzbsp_vmid_config.h"
#include "tzbsp_exec.h"
#include "tzbsp_mpu.h"
#include "tzbsp_mmu_cache.h"

/* Partition allocation information is stored in data structure. Because of
 * EBI needs to support interleaved and non-interleaved modes, it is not
 * practical to determine the allocated partitions from HW directly.
 *
 * When EBI is interleaved, both channels address the same memory and the MPU
 * configuration is identical. That is, when EBI is interleaved there are only
 * 16 unique partitions. Whe EBI isn't interleaved and both channels address
 * different memory ranges, there is total of 32 partitions, 16 per each
 * channel.
 *
 * The EBI partition allocation only supports four partitions.
 *
 * The assumption is that the content protected memory area is physically
 * contiguous. That is, there is no need to support a content area that would
 * overlap channels in non-interleaved mode.
 */

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/

#define TZBSP_EBI_MAX_ALLOCS          5

typedef struct tzbsp_ebi_alloc_partition_s
{
  /** 1 if used, 0 if free. */
  uint8   used;
  /** MPU resource group index. */
  uint8   rg;
  /** Blacklist ID. */
  uint8   blist;
  /** Memory Tag */
  tzbsp_mem_mpu_usage_e_type usage_hint;
  /** Start address of the allocated partition, inclusive of the actual
   * protected memory range. */
  uint32  start;
  /** End address of the allocated partition, exclusive of the actual protected
   * memory range. */
  uint32  end;
} tzbsp_ebi_alloc_partition_t;

/*----------------------------------------------------------------------------
 * Static Variable Definitions
 * -------------------------------------------------------------------------*/

tzbsp_ebi_alloc_partition_t g_tzbsp_ebi_alloc[TZBSP_EBI_MAX_ALLOCS];

/*----------------------------------------------------------------------------
 * Static Function Declarations and Definitions
 * -------------------------------------------------------------------------*/
extern boolean tzbsp_is_ebi_interleaved(void);
extern uint8 tzbsp_ebi_channel_for_addr(uint32 start, uint32 end);
extern int tzbsp_mpu_lock_area(uint32 mpu, uint32 index, uint32 start,
                               uint32 end, uint32 rvmids, uint32 wvmids);
extern int tzbsp_mpu_unlock_area(uint32 mpu, uint32 index);
extern boolean tzbsp_mpu_amt_is_overlap_amt(uint32 start, uint32 len);

/**
 * @return The index of the first found free and allocatable MPU partition, -1
 *         if no free resource groups are not available.
 */
static int tzbsp_find_free_ebi_rg_index(void)
{
  int i;
  for(i=0; i < TZBSP_EBI_MAX_ALLOCS; i++)
  {
    if(!g_tzbsp_ebi_alloc[i].used)
      return i;
  }
  return -1;
}

/**
 * Finds an allocated partition by matching to its start and end address.
 *
 * @param[in] start   The start address of the searched partition.
 * @param[in] end     The end address of the searched partition.
 *
 * @return A pointer to the found partition, \c NULL if not found.
 */
static tzbsp_ebi_alloc_partition_t* tzbsp_find_used_ebi_rg(uint32 start,
                                                           uint32 end)
{
  int i;
  for(i=0; i < TZBSP_EBI_MAX_ALLOCS; i++)
  {
    if(g_tzbsp_ebi_alloc[i].used && start == g_tzbsp_ebi_alloc[i].start &&
       end == g_tzbsp_ebi_alloc[i].end)
      return &g_tzbsp_ebi_alloc[i];
  }
  return NULL;
}

/**
 * Lock a memory region
 *
 * @param [in] start      start address
 * @param [in] end        End of the region (endpoint exclusive)
 * @param [in] usage_hint A tag that specifies this region's usage
 * @param [in] flags      Optional Flags
 *
 * Currently supported flags:
 * TZBSP_MPU_PROT_MEM_FLAG_DONT_CLEAR_MEM Don't clear memory on unlock
 *                                        (1->dont clear, 0->clear). Effective
 *                                        only in debug environments.
 *
 * @return \c E_SUCCESS if successful, error code otherwise.
 */
static int tzbsp_mpu_lock_memory ( uint32 start, uint32 end,
                                   tzbsp_mem_mpu_usage_e_type usage_hint,
                                   uint32 flags)
{
  tzbsp_ebi_alloc_partition_t* rg;
  int id;
  int ret = E_SUCCESS;

  /*
   * Check whether the address range is in non-secure region or not.
   * This is to make sure that someone cannot lock an already secure
   * area with their own VMID and then gets un-intended access to the
   * area.
   */
  if((!tzbsp_is_ns_range((const void *)start, end - start)) ||
     (tzbsp_mpu_amt_is_overlap_amt(start, end - start)))
  {
    return -E_FAILURE;
  }

  id = tzbsp_find_free_ebi_rg_index();

  if(-1 == id)
  {
    return -E_NOT_FOUND;
  }

  rg = &g_tzbsp_ebi_alloc[id];

  rg->used        = 1;
  rg->rg          = TZBSP_EBI1_RG_ALLOC_0 + id;
  rg->blist       = TZBSP_BLIST_ALLOC_0 + id;
  rg->start       = start;
  rg->end         = end;
  rg->usage_hint  = usage_hint;

  switch(usage_hint)
  {
    /* Listener and app command buffers are only write-protected to 
    prevent speculative fetches in HLOS */
    case TZBSP_MEM_MPU_USAGE_QSEE_SB_REGION:
    /* Intentional fallthrough */
    case TZBSP_MEM_MPU_USAGE_QSEE_REQRSP_REGION:
      ret = tzbsp_mpu_lock_area(HAL_XPU2_BIMC_MPU0, rg->rg, rg->start,
                                rg->end, TZBSP_VMID_TZ_BIT | TZBSP_VMID_AP_BIT,
                                TZBSP_VMID_TZ_BIT);
      break;

    case TZBSP_MEM_MPU_USAGE_QSEE_SA_REGION:
      /* Intentional fallthrough */
    case TZBSP_MEM_MPU_USAGE_SDDR_REGION:
      ret = tzbsp_mpu_lock_area(HAL_XPU2_BIMC_MPU0, rg->rg, rg->start,
                                rg->end, TZBSP_VMID_TZ_BIT, TZBSP_VMID_TZ_BIT);
      break;

    case TZBSP_MEM_MPU_USAGE_CP_PTBL:
      ret = tzbsp_mpu_lock_area(HAL_XPU2_BIMC_MPU0, rg->rg, rg->start,
                                rg->end, 0xFFFFFFFF, TZBSP_VMID_TZ_BIT);
      break;

    default:
      memset(rg, 0, sizeof(tzbsp_ebi_alloc_partition_t));
      return -E_FAILURE;

  }

  if(E_SUCCESS == ret)
  {
    if(tzbsp_config_s_area(rg->blist, rg->start, rg->end) < 0)
    {
      TZBSP_ERROR_CODE(TZBSP_CONFIG_S_AREA_FAIL);
      memset(rg, 0, sizeof(tzbsp_ebi_alloc_partition_t));
      return -E_FAILURE;
    }

    if(tzbsp_enable_s_area(rg->blist) < 0)
    {
      TZBSP_ERROR_CODE(TZBSP_ENABLE_S_AREA_FAIL);
      memset(rg, 0, sizeof(tzbsp_ebi_alloc_partition_t));
      return -E_FAILURE;
    }
  }
  else
  {
    memset(rg, 0, sizeof(tzbsp_ebi_alloc_partition_t));
  }

  return ret;
}

/**
 * Unlock a memory region
 *
 * @param [in] start      start address
 * @param [in] end        End of the region (endpoint exclusive)
 * @param [in] usage_hint A tag that specifies this region's usage
 * @param [in] flags      Optional Flags
 *
 * Currently supported flags:
 * TZBSP_MPU_PROT_MEM_FLAG_DONT_CLEAR_MEM Don't clear memory on unlock
 *                                        (1->dont clear, 0->clear). Effective
 *                                        only in debug environments.
 *
 * @return \c E_SUCCESS if successful, error code otherwise.
 */
static int tzbsp_mpu_unlock_memory ( uint32 start, uint32 end,
                                     tzbsp_mem_mpu_usage_e_type usage_hint,
                                     uint32 flags)
{
  tzbsp_ebi_alloc_partition_t* rg;
  int ret = E_SUCCESS;

  /*
   * Check whether the address range is in secure region or not.
   * No unlocking is needed if the area is already non-secure.
   */
  if(!tzbsp_is_s_range((const void *)start, end - start))
  {
    return -E_FAILURE;
  }

  rg = tzbsp_find_used_ebi_rg(start, end);

  if(!rg)
  {
    return -E_NOT_FOUND;
  }

  /* QSEE request/response region should not be cleared when it is
   * unlocked
   */
  if(TZBSP_MEM_MPU_USAGE_QSEE_REQRSP_REGION != rg->usage_hint && TZBSP_MEM_MPU_USAGE_QSEE_SB_REGION != rg->usage_hint)
  {
    /* Clear the memory if memory dumping is not allowed */
    if((FALSE == tzbsp_allow_unlock_xpu()) ||
       (1 != (flags & TZBSP_MPU_PROT_MEM_FLAG_DONT_CLEAR_MEM)))
    {
      /* memset to zero before unlocking */
      #if !defined(TZBSP_VIRTIO)
      memset((void*)(rg->start), 0, rg->end - rg->start);
      #endif
      tzbsp_dcache_flush_region((void*) rg->start, rg->end - rg->start);
    }
  }

  ret = tzbsp_mpu_unlock_area(HAL_XPU2_BIMC_MPU0, rg->rg);

  if(E_SUCCESS == ret)
  {
    if (tzbsp_disable_s_area(rg->blist) < 0)
    {
      TZBSP_ERROR_CODE(TZBSP_DISABLE_S_AREA_FAIL);
      ret = -E_FAILURE;
    }

    memset(rg, 0, sizeof(tzbsp_ebi_alloc_partition_t));
  }
  return ret;
}

/**
 * Dynamically allocate and deallocate an MPU RG and set R/W permissions
 *
 * @param [in] start      start address
 * @param [in] size       size of the region (in bytes)
 * @param [in] usage_hint A tag that specifies this region's usage
 * @param [in] lock       lock or unlock
 * @param [in] flags      Optional Flags
 *
 * Currently supported flags:
 * TZBSP_MPU_PROT_MEM_FLAG_DONT_CLEAR_MEM Don't clear memory on unlock
 *                                        (1->dont clear, 0->clear). Effective
 *                                        only in debug environments.
 *
 * @return \c E_SUCCESS if successful, error code otherwise.
 */

int tzbsp_mpu_protect_memory ( uint32 start, uint32 size,
                               tzbsp_mem_mpu_usage_e_type usage_hint,
                               boolean lock, uint32 flags)
{
  uint32 end = 0;
  int ret = E_SUCCESS;

  if(size == 0)
  {
    return -E_BAD_ADDRESS;
  }
  /* The base address of the image is floored to the nearest 4K boundary. */
  end = TZBSP_FLOOR_4K((uint32)(start  + size) + TZBSP_PAGE_4K - 1);
  start = TZBSP_FLOOR_4K((uint32)start);

  /* Check for overflow */
  if(start >= end)
  {
    return -E_INVALID_ARG;
  }

  if(lock)
  {
    ret = tzbsp_mpu_lock_memory(start, end, usage_hint, flags);
  }
  else
  {
    ret = tzbsp_mpu_unlock_memory(start, end, usage_hint, flags);
  }

  return ret;
}

/**
  * Changes read/write permissions to unmapped memory for specified vmids
  * @param xpu   - The xpu which controls the memory that will be locked
  * @param mask  - A mask to control which vmid's will be affected, only vmids 
  *                 whose bit enabled in the mask will be changed
  * @param rvmid - Read vmid's which should have access to the protected memory region
  * @param wvmid - Write vmid's which...
  * @return E_SUCCESS on success, negative value on failure.
  */
int tzbsp_mpu_set_umr_vmid(uint32 xpu, uint32 mask, uint32 rvmids, uint32 wvmids)
{
  return -E_NOT_SUPPORTED;
}

/**
  * Finds a free resource group in xpu and assigns it to given memory range
  *   with given read and write permissions for vmids.
  * @param xpu   - The xpu which controls the memory that will be locked
  * @param start - Start of memory region to protect
  * @param size  - The size of the memory region to protect
  * @param rvmid - Read vmid's which should have access to the protected memory region
  * @param wvmid - Write vmid's which...
  * @return E_SUCCESS on success, negative value on failure.
  */
int tzbsp_mpu_pil_lock_memory (uint32 xpu, uint32 start, uint32 size, uint32 rvmid, uint32 wvmid)
{
  return -E_NOT_SUPPORTED;
}

/**
  * If memory region from start to start+size is currently mapped
  *   for xpu, then unmap that memory region (remove the rg it is in)
  * @param xpu   - The xpu which controls the region of memory to unlock
  * @param start - Start of memory region to release
  * @param size  - The size of the memory region to protect
  * @return E_SUCCESS on success, negative value on failure.
  */
int tzbsp_mpu_pil_unlock_memory (uint32 xpu, uint32 start, uint32 size)
{
  return -E_NOT_SUPPORTED;
}
