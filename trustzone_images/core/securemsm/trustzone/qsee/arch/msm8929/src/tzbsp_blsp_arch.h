#ifndef TZBSP_BLSP_ARCH_H
#define TZBSP_BLSP_ARCH_H

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/

#include "tzbsp_blsp.h"
#include "tzbsp_xpu_def.h"
#include "tzbsp_xpu.h"


#define TZBSP_BLSP_COUNT  1 /**< Number of BLSP's supported in this target*/
#define TZBSP_QUPS_PER_BLSP  6 /**< Number of QUP instances per each BLSP*/
#define TZBSP_TOTAL_QUPS  (TZBSP_BLSP_COUNT * TZBSP_QUPS_PER_BLSP) /**< Total Number of QUP combining all BLSP's*/


/** @brief Check whether clock is enabled or not
  
    @param[in] qupId   Qup id for which BLSP the clock status to read.
        
    @return E_SUCCESS if success otherwise error code.
*/
boolean tzbsp_blsp_is_clock_enabled(uint32 qupId);

/** @brief Set BLSAP AHB clock ON/OFF

    @param[in] qupId   Qup id.
    @param[in] flag    TRUE  - Clock ON
                       FALSE - Clock OFF
  
    @return E_SUCCESS if success otherwise error code.
*/
int tzbsp_blsp_set_clock(uint32 qupId, boolean flag);

/** @brief Locks GPIO
  
    @param[in] pTzBspQupInfo   QUP Information.
        
    @return E_SUCCESS if success otherwise error code.
*/
int tzbsp_blsp_lock_gpio(const TzBsp_Blsp_Qup_Info *pTzBspQupInfo);

/** @brief UnLocks GPIO
  
    @param[in] pTzBspQupInfo   QUP Information.
        
    @return E_SUCCESS if success otherwise error code.
*/
int tzbsp_blsp_unlock_gpio(const TzBsp_Blsp_Qup_Info *pTzBspQupInfo);

/** @brief Configure GPIO Functionality.
  
    @param[in] pTzBspQupInfo   QUP Information.
        
    @return E_SUCCESS if success otherwise error code.
*/
int tzbsp_blsp_gpio_configuration(const TzBsp_Blsp_Qup_Info *pTzBspQupInfo);

#endif /* TZBSP_BLSP_ARCH_H */