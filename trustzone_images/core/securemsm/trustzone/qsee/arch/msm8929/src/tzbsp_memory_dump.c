/**
 * @file tzbsp_memory_dump.c
 * @brief Provides memory dumping related functionality.

*/

/*===========================================================================
   Copyright (c) 2011-2013 by QUALCOMM Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8929/src/tzbsp_memory_dump.c#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $


when       who      what, where, why
--------   ---      ------------------------------------
10/04/11   mm       Added logs for Reset
02/25/11   tk       Initial version
===========================================================================*/

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include <HALhwio.h>
#include <HALqgic.h>
#include <string.h>
#include <busywait.h>
#include <stddef.h>

#include "tzbsp.h"
#include "tzbsp_chipset.h"
#include "tzbsp_syscall.h"
#include "tz_syscall_pub.h"
#include "tzbsp_errno.h"
#include "tzbsp_sys.h"
#include "tzbsp_mem.h"
#include "tzbsp_target.h"
#include "tzbsp_xpu.h"
#include "tzbsp_timer.h"
#include "tzbsp_log.h"
#include "tzbsp_mmu_cache.h"
#include "tzbsp_diag.h"
#include "tzbsp_hwio.h"
#include "tzbsp_shared_imem.h"
#include "tz_mc.h"
#include "aa64_ctx.h"
#include "tzbsp_memory_dump.h"
#include "sysdbg_mem_dump.h"
#include "util.h"
#include "tzbsp_secboot.h"
#include "tzbsp_memory_map.h"

extern uint32 tzbsp_get_num_cpus_in_lpm(void);
extern uint8 tzbsp_get_cluster_num(void);
extern uint32 tzbsp_is_perf_cluster(void);
extern uint32 tzbsp_is_power_cluster_present(void);
extern tzbsp_mutex_t tzbsp_boot_lock;
static void tzbsp_dump_gicd_spm_reg(void);
extern boolean tzbsp_allow_memory_dump(void);

/*----------------------------------------------------------------------------
 * Static Variable Definitions
 * -------------------------------------------------------------------------*/
/* EL1's Copy of Entire Debug Context */
cpu_dbg_info_t cpu_dbg_info[TZBSP_CPU_COUNT] = {0};

static volatile tzbsp_dump_gicd_spm_reg_t g_tzbsp_dump_gicd_spm_reg;

/*----------------------------------------------------------------------------
 * Function Declarations and Documentation
 * -------------------------------------------------------------------------*/
static void tzbsp_copy_dump_ctx()
{
  uint32 cpu = tzbsp_cur_cpu_num();
  sysdbgCPUDumpType* cpu_dump = sysdbg_get_cpudump();
  cpu_dbg_info_t *cpu_debug_info = &cpu_dbg_info[cpu];

  if(cpu_dump && cpu_dump->CPUDump[cpu])
  {
    /* skip if the dump struct is already occupied */
    if (cpu_dump->CPUDump[cpu]->magic == DUMP_MAGIC_NUMBER)
    {
      return;
    }

    /* Get contexts */
    aa_64_ctx_all_els_t *mon_cpu_ctx = &(cpu_debug_info->aa_64_ctx_all_els);
    sysdbgCPUCtxtType *sysdbgCPUCtxt =
      (sysdbgCPUCtxtType*) ((uint32*)cpu_dump->CPUDump[cpu]->start_addr);
    sysdbg_cpu64_ctxt_regs_type *sysdbg_ctxt_regs =
      (sysdbg_cpu64_ctxt_regs_type*)&(sysdbgCPUCtxt->cpu_regs);
    sysdbg_cpu32_ctxt_regs_type *sysdbg_ctxt_regs_32 =
      (sysdbg_cpu32_ctxt_regs_type*)&(sysdbgCPUCtxt->__reserved3);
    aa_64_ctx_t *mon_ctxt_regs = &(mon_cpu_ctx->aa_64_ctx);

    /* This magic number tells SDI that TZ populated the struct */
    cpu_dump->CPUDump[cpu]->magic = cpu_debug_info->magic;
    /* SDI version of context dump */
    cpu_dump->CPUDump[cpu]->version = cpu_debug_info->version;

    /* status[0] is 1 for Cluster0, 2 for Cluster1 */
    sysdbgCPUCtxt->status[0] = (tzbsp_get_cluster_num() + 1);

    sysdbgCPUCtxt->status[1] = cpu_dbg_info[cpu].sc_status;

    /* TODO: different PC for WDT/SGI while in warm boot? */
    if (cpu_dbg_info[cpu].sc_status & TZ_SC_STATUS_NS_BIT)
    {
      /* For NS WDT, NS EL1 PC will be EL3 ELR */
      sysdbg_ctxt_regs->pc = mon_ctxt_regs->elr_el3;
    }
    else
    {
      /* For S WDT, S SVC PC will be FIQ LR */
      sysdbg_ctxt_regs->pc = (uint64)cpu_debug_info->el1_context.lr;
    }

    /* Copy monitor general purpose register context */
    memcpy(&sysdbg_ctxt_regs->x0, &mon_ctxt_regs->x0,
           (offsetof(sysdbg_cpu64_ctxt_regs_type, x30) +
            sizeof(mon_ctxt_regs->x30)) -
           offsetof(sysdbg_cpu64_ctxt_regs_type, x0));

    sysdbg_ctxt_regs->sp_el2   = mon_cpu_ctx->sp_el2;
    sysdbg_ctxt_regs->elr_el2  = mon_cpu_ctx->elr_el2;
    sysdbg_ctxt_regs->spsr_el2 = mon_cpu_ctx->spsr_el2;
    sysdbg_ctxt_regs->sp_el3   = mon_cpu_ctx->sp_el3;
    sysdbg_ctxt_regs->spsr_el3 = mon_ctxt_regs->cpsr_el1;
    sysdbg_ctxt_regs->elr_el3  = mon_ctxt_regs->elr_el3;
    sysdbg_ctxt_regs->sp_el1   = mon_ctxt_regs->sp_el1;
    sysdbg_ctxt_regs->elr_el1  = mon_ctxt_regs->elr_el1;
    sysdbg_ctxt_regs->spsr_el1 = mon_ctxt_regs->spsr_el1;
    sysdbg_ctxt_regs->sp_el0   = mon_ctxt_regs->sp_el0;

    /* Copy regs to hlos */
    sysdbg_ctxt_regs_32->r0 = (uint64)cpu_debug_info->el1_context.r0;
    sysdbg_ctxt_regs_32->r1 = (uint64)cpu_debug_info->el1_context.r1;
    sysdbg_ctxt_regs_32->r2 = (uint64)cpu_debug_info->el1_context.r2;
    sysdbg_ctxt_regs_32->r3 = (uint64)cpu_debug_info->el1_context.r3;
    sysdbg_ctxt_regs_32->r4 = (uint64)cpu_debug_info->el1_context.r4;
    sysdbg_ctxt_regs_32->r5 = (uint64)cpu_debug_info->el1_context.r5;
    sysdbg_ctxt_regs_32->r6 = (uint64)cpu_debug_info->el1_context.r6;
    sysdbg_ctxt_regs_32->r7 = (uint64)cpu_debug_info->el1_context.r7;
    sysdbg_ctxt_regs_32->r8 = (uint64)cpu_debug_info->el1_context.r8;
    sysdbg_ctxt_regs_32->r9 = (uint64)cpu_debug_info->el1_context.r9;
    sysdbg_ctxt_regs_32->r10 = (uint64)cpu_debug_info->el1_context.r10;
    sysdbg_ctxt_regs_32->r11 = (uint64)cpu_debug_info->el1_context.r11;
    sysdbg_ctxt_regs_32->r12 = (uint64)cpu_debug_info->el1_context.r12;
    sysdbg_ctxt_regs_32->r13_usr = (uint64)cpu_debug_info->el1_context.r13;
    sysdbg_ctxt_regs_32->r14_usr = (uint64)cpu_debug_info->el1_context.lr;
  }
}//tzbsp_copy_dump_ctx()

/* Resets the chip using secure watchdog. This function will never return. */
void tzbsp_wdt_reset(void)
{
  uint32 cpu = tzbsp_cur_cpu_num();

  /* Program the WDT to bite after a delay, a delay is needed to allow other
   * CPUs time to dump their context. Bite timeout < bark timeout, hence the
   * WDT will reset when it expires. */
  tzbsp_wdt_start(TZBSP_WDT_DELAY_MS * 2, TZBSP_WDT_DELAY_MS);
  cpu_dbg_info[cpu].sgi_state |= 0x4;

  /* Notify Q6 subsystems. WDT is started before the NMI writes to ensure
   * that a hanging NMI write will still do a WDT bite. */
  tzbsp_write_q6_nmis();
  cpu_dbg_info[cpu].sgi_state |= 0x8;

  /* Wait for WDT bite to take effect. */
  while(1)
  {
    cpu_dbg_info[cpu].sgi_state |= 0x10;
    /* Send a SGI to all other CPUs to stop them and make them dump their
     * current context. */
    if (!cpu_dbg_info[((cpu+1)%TZBSP_CPU_COUNT)].sgi_state ||
        !cpu_dbg_info[((cpu+2)%TZBSP_CPU_COUNT)].sgi_state ||
        !cpu_dbg_info[((cpu+3)%TZBSP_CPU_COUNT)].sgi_state ||
        !cpu_dbg_info[((cpu+4)%TZBSP_CPU_COUNT)].sgi_state ||
        !cpu_dbg_info[((cpu+5)%TZBSP_CPU_COUNT)].sgi_state ||
        !cpu_dbg_info[((cpu+6)%TZBSP_CPU_COUNT)].sgi_state ||
        !cpu_dbg_info[((cpu+7)%TZBSP_CPU_COUNT)].sgi_state)
    {
      uint32 i;
      cpu_dbg_info[cpu].sgi_state |= 0x40;
      /* Send a SGI to all online CPUs to stop them and make them dump their
       * current context. */
      for (i=0; i<TZBSP_CPU_COUNT; i++)
      {
        if (i == tzbsp_cur_cpu_num())
        {
          continue;
        }

        if (TRUE == tzbsp_diag_is_core_online(i))
        {
          /* qGIC consider cluster 0 as Cluster 1 for 8936. CPU ID need to offset by 4 */
          if(TZBSP_MSM_8x36 == tzbsp_get_chip_id())
          {
            HAL_qgic_Generate(TZBSP_INT_SGI_RESET, HAL_QGIC_SECURE,
                              HAL_QGIC_TARGET_CPU((i % 4) + 4));  
          }
          else
          {
            HAL_qgic_Generate(TZBSP_INT_SGI_RESET, HAL_QGIC_SECURE,
                              HAL_QGIC_TARGET_CPU(i));          
          }
        }
      }
      cpu_dbg_info[cpu].sgi_state += 0x100;

    }
    cpu_dbg_info[cpu].sgi_state |= 0x20;
  }

}

/* Populates the current CPU's context in the context dump buffer.
 *
 * @param [in] reason The cause of the reset (WDT bark or SGI).
 */
static void tzbsp_populate_dump_ctx(uint32 reason)
{
  uint32 counter = 0;
  uint32 cpu = tzbsp_cur_cpu_num();

  cpu_dbg_info[cpu].sdi_hint[0] = *(uint32 *)TZBSP_SHARED_IMEM_CACHE_MAGIC;
  cpu_dbg_info[cpu].sdi_hint[1] = *(uint32 *)TZBSP_SHARED_IMEM_L1_VALIDITY;
  cpu_dbg_info[cpu].sdi_hint[2] = *(uint32 *)TZBSP_SHARED_IMEM_L2_VALIDITY;

  /* Collect the WDT counter value for debug analysis. */
  for (counter = 0; counter < TZBSP_CPU_COUNT; counter ++)
  {
    cpu_dbg_info[counter].wdt_sts = HWIO_IN(APCS_WDT_TMR1_WDOG_STATUS);
  }

  /* Logging banked GICD registers for each CPU */
  g_tzbsp_dump_gicd_spm_reg.APCS_GICD_ISENABLER0_PRIV[cpu] = HWIO_INI(APCS_GICD_ISENABLERn, 0x0);
  g_tzbsp_dump_gicd_spm_reg.APCS_GICD_ISPENDR0_PRIV[cpu] = HWIO_INI(APCS_GICD_ISPENDRn, 0x0);

  cpu_dbg_info[cpu].cpu_power_info.cpus_in_lpm = tzbsp_get_num_cpus_in_lpm();

  /* Initialize status to zero */
  cpu_dbg_info[cpu].sc_status = 0;

  /* Check if Memory dumping is allowed */
  if (tzbsp_allow_unlock_xpu())
  {
    mon_dump_context(&cpu_dbg_info[cpu]);
    tzbsp_get_pre_fiq_ctx((uint32 *)&(cpu_dbg_info[cpu].el1_context));
  }

  /* |= because some status info may be populated in
     mon_dump_context() call above. */
  cpu_dbg_info[cpu].sc_status |= reason;

  /* Check to see if the core was coming out of PC when interrupt
     fired. */
  if (g_tzbsp_diag->boot_info[cpu].warm_entry_cnt >
      g_tzbsp_diag->boot_info[cpu].warm_exit_cnt)
  {
    cpu_dbg_info[cpu].sc_status |= TZ_SC_STATUS_WARM_BOOT;
  }

  /* Finally, to signify content completion, write the magic number and
   * and version defined by sysdebug API */
  cpu_dbg_info[cpu].magic     = DUMP_MAGIC_NUMBER;
  cpu_dbg_info[cpu].version   = CPU_FORMAT_VERSION;

  /* Call tzbsp_copy_dump_ctx() to populate HLOS memory with dump */
  tzbsp_copy_dump_ctx();

  tzbsp_dcache_flush_all();
}

/* Interrupt handler for non-secure WDT bark. When this handler is hit, the
 * non-secure side is in dysfunctional state and WDT timed out and took a bite.
 * As a result TZ starts a sequence to dump all CPU contexts to memory and
 * reset the device to download mode.
 *
 * @param [in] ctx    ISR context.
 *
 * @return The ISR context is passed through.
 */
void* tzbsp_ns_wdt_isr(void* ctx)
{
  uint32 cpu = tzbsp_cur_cpu_num();

  cpu_dbg_info[cpu].sgi_state = 0;

  /* Disable the WDT. */
  tzbsp_wdt_stop();

  cpu_dbg_info[cpu].sgi_state |= 0x1;

  /* Does not return; Signals other CPUs with SGIs and triggers a
     secure watchdog reset. */
  tzbsp_err_fatal(TZBSP_ERR_FATAL_NON_SECURE_WDT);

  return ctx;
}

/* Some other CPU got the WDT bark and sent out a SGI to other CPUs.
 *
 * @param [in] ctx    ISR context.
 *
 * @return The ISR context is passed through.
 */
__attribute__((noreturn)) void* tzbsp_sgi15_isr(void* ctx)
{
  /* Collect the WDT counter value for debug analysis. */
  uint32 cpu = tzbsp_cur_cpu_num();

  cpu_dbg_info[cpu].sgi_state = 0x0;

  /* TODO: code duplication with tzbsp_memory_dump */
  if (tzbsp_allow_memory_dump())
  {
    /* Dump L1 caches. */
    /* TODO: integrate L1 dumping from 8994 */
    /* tzbsp_l1_dump(); */
    cpu_dbg_info[cpu].sgi_state |= 0x1;

    /* Fills in the CPU dump context for this CPU and flushes cache */
    tzbsp_populate_dump_ctx(TZ_SC_STATUS_SGI);
    cpu_dbg_info[cpu].sgi_state |= 0x2;
  }


  /* Release the lock tzbsp_boot_lock to allow other CPUs to enter the
     warm boot path (SGI is pending on other CPUs as well). */
  tzbsp_dsb();
  tzbsp_mutex_unlock(&tzbsp_boot_lock);
  tzbsp_dsb();

  cpu_dbg_info[cpu].sgi_state |= 0x4;

  /* Start waiting for the device to be reset by the CPU that got the WDT
   * originally. */
  while(1);
}

void tzbsp_memory_dump(tzbsp_err_fatal_e err)
{
  if (tzbsp_allow_memory_dump())
  {
    uint32 cpu = tzbsp_cur_cpu_num();
    uint32 sc_status = ((err == TZBSP_ERR_FATAL_NON_SECURE_WDT) ?
                        TZ_SC_STATUS_WDT : 0);

    /* Logging QGIC, CPU & L2 SPM Registers */
    tzbsp_dump_gicd_spm_reg();

    tzbsp_populate_dump_ctx(sc_status);

    /* TODO: integrate cache dumping from 8994. */
    /* tzbsp_l1_dump(); */
  }
}

/* Function to stop and flush the ETB; used in the dead-on-arrival FIQs */
void tzbsp_stop_and_flush_etb(void)
{
  tzbsp_enable_qdss_clocks();

  HWIO_OUT(QDSS_ETFETB_LAR, 0xC5ACCE55);
  tzbsp_dsb();
  if(HWIO_INF(QDSS_ETFETB_CTL, TRACECAPTEN) && (!HWIO_INF(QDSS_ETFETB_MODE, MODE)))
  {
    HWIO_OUTF(QDSS_ETFETB_FFCR, STOPONFL, 0x1);
    HWIO_OUTF(QDSS_ETFETB_FFCR, FLUSHMAN_W, 0x1);
    TZBSP_QDSS_TMC_WAIT(HWIO_INF(QDSS_ETFETB_FFCR, FLUSHMAN_R));
    TZBSP_QDSS_TMC_WAIT(!HWIO_INF(QDSS_ETFETB_STS, TMCREADY));
    tzbsp_dsb();
  }
  HWIO_OUT(QDSS_ETFETB_LAR, 0x0);

  HWIO_OUT(QDSS_ETR_LAR, 0xC5ACCE55);
  tzbsp_dsb();
  if(HWIO_INF(QDSS_ETR_CTL, TRACECAPTEN) && (!HWIO_INF(QDSS_ETR_MODE, MODE)))
  {
    HWIO_OUTF(QDSS_ETR_FFCR, STOPONFL, 0x1);
    HWIO_OUTF(QDSS_ETR_FFCR, FLUSHMAN_W, 0x1);
    TZBSP_QDSS_TMC_WAIT(HWIO_INF(QDSS_ETR_FFCR, FLUSHMAN_R));
    TZBSP_QDSS_TMC_WAIT(!HWIO_INF(QDSS_ETR_STS, TMCREADY));
    tzbsp_dsb();
  }
  HWIO_OUT(QDSS_ETR_LAR, 0x0);
}

/**
 * System call for non-secure side to set the WDT reset context dump buffer
 * address.
 *
 * @param [in] addr   Address of the buffer, must be in non-secure memory.
 * @param [in] size   Size of the buffer in bytes.
 *
 * @return E_SUCCESS if the call succeeded, error code otherwise.
 */
int tzbsp_set_cpu_ctx_buf(uint32 addr, uint32 size)
{
  int err = -E_BAD_ADDRESS;

  //Not required on 8936 A64

  return err;
}

int tzbsp_set_l1_dump_buf(uint32 addr, uint32 size)
{
  /* Not supported on 8936 */
  return -E_NOT_SUPPORTED;
}

int tzbsp_set_l2_dump_buf(uint32 addr, uint32 size)
{
  /* Not supported on 8936 */
  return -E_NOT_SUPPORTED;
}

int tzbsp_query_l1_dump_buf_size(uint32 addr, uint32* rsp, uint32* rsplen)
{
  /* Not supported on 8936 */
  return -E_NOT_SUPPORTED;
}

int tzbsp_query_l2_dump_buf_size(uint32 addr, uint32* rsp, uint32* rsplen)
{
  /* Not supported on 8936 */
  return -E_NOT_SUPPORTED;
}

/**
 * Initialization routine for the Memory Dump/Debug framework
 */
void tzbsp_mem_dump_init(void)
{

  /* Set-up the TZ dump / TZ back up information in shared IMEM.
     Monitor & TZ region both need to back up */
  *((volatile uint32*)TZBSP_SHARED_IMEM_DUMP_IMEM_ADDR) = SCL_TZ_DDR_START_ADDR;
  *((volatile uint32*)TZBSP_SHARED_IMEM_DUMP_DDR_ADDR)  = TZBSP_DEBUG_AREA_BASE;
  *((volatile uint32*)TZBSP_SHARED_IMEM_DUMP_SIZE)      = TZBSP_TZ_BKUP_REGION_SIZE;
  *((volatile uint32*)TZBSP_SHARED_IMEM_DUMP_MAGIC)     = TZBSP_IMEM_DUMP_MAGIC;

  /* Set up the variables which indicate if the L1 and L2 caches can
     be dumped */
  *((volatile uint32*) TZBSP_SHARED_IMEM_L1_VALIDITY) =
    (1 << tzbsp_cur_cpu_num());
  *((volatile uint32*) TZBSP_SHARED_IMEM_L2_VALIDITY) =
    (1 << tzbsp_get_cluster_num());
  *((volatile uint32*) TZBSP_SHARED_IMEM_CACHE_MAGIC) = TZBSP_CACHE_DUMP_MAGIC;

  //cpu_dbg_info is pre-initialized to 0
}

/**
 * Logging spm status, spm control and qGIC registers.
 * Banked qGIC registers dumped by each CPU separately.
 */
static void tzbsp_dump_gicd_spm_reg(void)
{
  int i;

  /* Log MPM2 Time */
  g_tzbsp_dump_gicd_spm_reg.MPM2_MPM_SLEEP_TIMETICK_COUNT_VAL = HWIO_IN(MPM2_MPM_SLEEP_TIMETICK_COUNT_VAL);

  if (tzbsp_is_power_cluster_present()) 
  {
    /* Logging SPM status & SPM control for power cluster CPU */  
    g_tzbsp_dump_gicd_spm_reg.APCS_ALIASn_SAW2_SPM_STS[0] = HWIO_IN(APCS_ALIAS0_SAW2_SPM_STS);
    g_tzbsp_dump_gicd_spm_reg.APCS_ALIASn_SAW2_SPM_STS[1] = HWIO_IN(APCS_ALIAS1_SAW2_SPM_STS);
    g_tzbsp_dump_gicd_spm_reg.APCS_ALIASn_SAW2_SPM_STS[2] = HWIO_IN(APCS_ALIAS2_SAW2_SPM_STS);
    g_tzbsp_dump_gicd_spm_reg.APCS_ALIASn_SAW2_SPM_STS[3] = HWIO_IN(APCS_ALIAS3_SAW2_SPM_STS);
    
    g_tzbsp_dump_gicd_spm_reg.APCS_ALIASn_SAW2_SPM_CTL[0] = HWIO_IN(APCS_ALIAS0_SAW2_SPM_CTL);
    g_tzbsp_dump_gicd_spm_reg.APCS_ALIASn_SAW2_SPM_CTL[1] = HWIO_IN(APCS_ALIAS1_SAW2_SPM_CTL);
    g_tzbsp_dump_gicd_spm_reg.APCS_ALIASn_SAW2_SPM_CTL[2] = HWIO_IN(APCS_ALIAS2_SAW2_SPM_CTL);
    g_tzbsp_dump_gicd_spm_reg.APCS_ALIASn_SAW2_SPM_CTL[3] = HWIO_IN(APCS_ALIAS3_SAW2_SPM_CTL);

        /* Logging Power status & Power gate status for performance cluster CPU */
    g_tzbsp_dump_gicd_spm_reg.APCS_ALIASn_APC_PWR_STATUS[0] = HWIO_IN(APCS_ALIAS0_APC_PWR_STATUS);
    g_tzbsp_dump_gicd_spm_reg.APCS_ALIASn_APC_PWR_STATUS[1] = HWIO_IN(APCS_ALIAS1_APC_PWR_STATUS);
    g_tzbsp_dump_gicd_spm_reg.APCS_ALIASn_APC_PWR_STATUS[2] = HWIO_IN(APCS_ALIAS2_APC_PWR_STATUS);
    g_tzbsp_dump_gicd_spm_reg.APCS_ALIASn_APC_PWR_STATUS[3] = HWIO_IN(APCS_ALIAS3_APC_PWR_STATUS);

    g_tzbsp_dump_gicd_spm_reg.APCS_ALIASn_APC_PWR_GATE_STATUS[0] = HWIO_IN(APCS_ALIAS0_APC_PWR_GATE_STATUS);
    g_tzbsp_dump_gicd_spm_reg.APCS_ALIASn_APC_PWR_GATE_STATUS[1] = HWIO_IN(APCS_ALIAS1_APC_PWR_GATE_STATUS);
    g_tzbsp_dump_gicd_spm_reg.APCS_ALIASn_APC_PWR_GATE_STATUS[2] = HWIO_IN(APCS_ALIAS2_APC_PWR_GATE_STATUS);
    g_tzbsp_dump_gicd_spm_reg.APCS_ALIASn_APC_PWR_GATE_STATUS[3] = HWIO_IN(APCS_ALIAS3_APC_PWR_GATE_STATUS);

    /* Logging SPM status & SPM control for performance cluster L2 */
    g_tzbsp_dump_gicd_spm_reg.APCLUS0_L2_SAW2_SPM_STS = HWIO_IN(APCLUS0_L2_SAW2_SPM_STS);
    g_tzbsp_dump_gicd_spm_reg.APCLUS0_L2_SAW2_SPM_CTL = HWIO_IN(APCLUS0_L2_SAW2_SPM_CTL);
    
    /* Logging L2 Power status */
    g_tzbsp_dump_gicd_spm_reg.APCS_ALIASn_L2_PWR_STATUS[0] = HWIO_IN(APCS_ALIAS0_L2_PWR_STATUS);
    
  }
 
  /* Logging SPM status & SPM control for performance cluster CPU */
  g_tzbsp_dump_gicd_spm_reg.APCS_ALIASn_SAW2_SPM_STS[4] = HWIO_IN(APCS_ALIAS4_SAW2_SPM_STS);
  g_tzbsp_dump_gicd_spm_reg.APCS_ALIASn_SAW2_SPM_STS[5] = HWIO_IN(APCS_ALIAS5_SAW2_SPM_STS);
  g_tzbsp_dump_gicd_spm_reg.APCS_ALIASn_SAW2_SPM_STS[6] = HWIO_IN(APCS_ALIAS6_SAW2_SPM_STS);
  g_tzbsp_dump_gicd_spm_reg.APCS_ALIASn_SAW2_SPM_STS[7] = HWIO_IN(APCS_ALIAS7_SAW2_SPM_STS);
  
  g_tzbsp_dump_gicd_spm_reg.APCS_ALIASn_SAW2_SPM_CTL[4] = HWIO_IN(APCS_ALIAS4_SAW2_SPM_CTL);
  g_tzbsp_dump_gicd_spm_reg.APCS_ALIASn_SAW2_SPM_CTL[5] = HWIO_IN(APCS_ALIAS5_SAW2_SPM_CTL);
  g_tzbsp_dump_gicd_spm_reg.APCS_ALIASn_SAW2_SPM_CTL[6] = HWIO_IN(APCS_ALIAS6_SAW2_SPM_CTL);
  g_tzbsp_dump_gicd_spm_reg.APCS_ALIASn_SAW2_SPM_CTL[7] = HWIO_IN(APCS_ALIAS7_SAW2_SPM_CTL);

  /* Logging Power status & Power gate status for performance cluster CPU */
  g_tzbsp_dump_gicd_spm_reg.APCS_ALIASn_APC_PWR_STATUS[4] = HWIO_IN(APCS_ALIAS4_APC_PWR_STATUS);
  g_tzbsp_dump_gicd_spm_reg.APCS_ALIASn_APC_PWR_STATUS[5] = HWIO_IN(APCS_ALIAS5_APC_PWR_STATUS);
  g_tzbsp_dump_gicd_spm_reg.APCS_ALIASn_APC_PWR_STATUS[6] = HWIO_IN(APCS_ALIAS6_APC_PWR_STATUS);
  g_tzbsp_dump_gicd_spm_reg.APCS_ALIASn_APC_PWR_STATUS[7] = HWIO_IN(APCS_ALIAS7_APC_PWR_STATUS);

  g_tzbsp_dump_gicd_spm_reg.APCS_ALIASn_APC_PWR_GATE_STATUS[4] = HWIO_IN(APCS_ALIAS4_APC_PWR_GATE_STATUS);
  g_tzbsp_dump_gicd_spm_reg.APCS_ALIASn_APC_PWR_GATE_STATUS[5] = HWIO_IN(APCS_ALIAS5_APC_PWR_GATE_STATUS);
  g_tzbsp_dump_gicd_spm_reg.APCS_ALIASn_APC_PWR_GATE_STATUS[6] = HWIO_IN(APCS_ALIAS6_APC_PWR_GATE_STATUS);
  g_tzbsp_dump_gicd_spm_reg.APCS_ALIASn_APC_PWR_GATE_STATUS[7] = HWIO_IN(APCS_ALIAS7_APC_PWR_GATE_STATUS);

  /* Logging SPM status & SPM control for performance cluster L2 */
  g_tzbsp_dump_gicd_spm_reg.APCS_CLUS1_L2_SAW2_SPM_STS = HWIO_IN(APCS_CLUS1_L2_SAW2_SPM_STS);
  g_tzbsp_dump_gicd_spm_reg.APCS_CLUS1_L2_SAW2_SPM_CTL = HWIO_IN(APCS_CLUS1_L2_SAW2_SPM_CTL);
  
  /* Logging L2 Power status */
  g_tzbsp_dump_gicd_spm_reg.APCS_ALIASn_L2_PWR_STATUS[1] = HWIO_IN(APCS_ALIAS1_L2_PWR_STATUS);

  /* Logging SPM status & SPM control for CCI */
  g_tzbsp_dump_gicd_spm_reg.CCI_SAW2_SPM_STS = HWIO_IN(CCI_SAW2_SPM_STS);
  g_tzbsp_dump_gicd_spm_reg.CCI_SAW2_SPM_CTL = HWIO_IN(CCI_SAW2_SPM_CTL);

  /* Dumping non banked GICD registers */
  for(i=1; i < 11; i++)
  {
    g_tzbsp_dump_gicd_spm_reg.APCS_GICD_ISENABLERn_PUB[i] = HWIO_INI(APCS_GICD_ISENABLERn, i);
    g_tzbsp_dump_gicd_spm_reg.APCS_GICD_ISPENDRn_PUB[i] = HWIO_INI(APCS_GICD_ISPENDRn, i);
  }
}

