# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8936/src/tzbsp_mutex_asm.s"
# 1 "<built-in>" 1
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8936/src/tzbsp_mutex_asm.s" 2
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;
; T R U S T Z O N E M U T E X
;
;
; GENERAL DESCRIPTION
; Target specific spinlock implementation
;
; Copyright (c) 2010-2014 by Qualcomm Technologies, Incorporated.All Rights Reserved.
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;
; EDIT HISTORY FOR FILE
;
; This section contains comments describing changes made to the module.
; Notice that changes are listed in reverse chronological order.
;
; $Header:
;
; when who what, where, why
; -------- --- ---------------------------------------------------
; 05/19/10 tk Initial version.
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8936/cfg/mmu/tzbsp_target.h" 1
# 43 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8936/cfg/mmu/tzbsp_target.h"
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/customer.h" 1
# 80 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/customer.h"
# 1 "./custmaxaanaaa.h" 1
# 10 "./custmaxaanaaa.h"
# 1 "./targmaxaanaaa.h" 1
# 11 "./custmaxaanaaa.h" 2
# 144 "./custmaxaanaaa.h"
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/custremoteapis.h" 1
# 145 "./custmaxaanaaa.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/custtarget.h" 1
# 146 "./custmaxaanaaa.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/custsdcc.h" 1
# 147 "./custmaxaanaaa.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/custsurf.h" 1
# 148 "./custmaxaanaaa.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/custdiag.h" 1
# 149 "./custmaxaanaaa.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/custefs.h" 1
# 150 "./custmaxaanaaa.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/custpmic.h" 1
# 151 "./custmaxaanaaa.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/custsio_8660.h" 1
# 152 "./custmaxaanaaa.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/custsec.h" 1
# 121 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/custsec.h"
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/custsfs.h" 1
# 122 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/custsec.h" 2
# 153 "./custmaxaanaaa.h" 2
# 81 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/build/cust/customer.h" 2
# 44 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8936/cfg/mmu/tzbsp_target.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8936/cfg/mmu/../common/tzbsp_target.h" 1
# 45 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8936/cfg/mmu/tzbsp_target.h" 2
# 26 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8936/src/tzbsp_mutex_asm.s" 2
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/include/tzbsp_asm.h" 1
# 35 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/include/tzbsp_asm.h"
; ---------------------
; MACRO: Mov32
; ---------------------
; Moves a 32 bit value into a register
  MACRO
  Mov32 $r, $val_l
    movw $r, #($val_l & 0xFFFF)
    movt $r, #(($val_l >> 16) & 0xFFFF)
  MEND

; ---------------------
; MACRO: Read32R
; ---------------------
; Reads a given word where the address is in a register
  MACRO
  Read32R $val_r, $addr_r, $work_r
    Mov32 $work_r, 0x02000000
    cmp $addr_r, $work_r ; compare address to remap area
    mov $work_r, #0
    bhs %f10 ; addr >= remap, no need to offset
    Mov32 $work_r, 0x04800000

10 ldr $val_r, [$addr_r, $work_r]
  MEND

; ---------------------
; MACRO: Read32L
; ---------------------
; Reads a given word where the address is a literal
  MACRO
  Read32L $val_r, $addr_l, $work_r
    Mov32 $val_r, $addr_l
    Read32R $val_r, $val_r, $work_r
  MEND

; ---------------------
; MACRO: Write32R
; ---------------------
; Writes a given word where the address is in a register
  MACRO
  Write32R $val_r, $addr_r, $work_r
    Mov32 $work_r, 0x02000000
    cmp $addr_r, $work_r ; compare address to remap area
    mov $work_r, #0
    bhs %f10 ; addr >= remap, no need to offset
    Mov32 $work_r, 0x04800000

10 str $val_r, [$addr_r, $work_r]
  MEND

; ---------------------
; MACRO: Write32L
; ---------------------
; Writes a given word where the address is a literal
  MACRO
  Write32L $val_r, $addr_l, $work_r, $addr_r
    Mov32 $addr_r, $addr_l
    Write32R $val_r, $addr_r, $work_r
  MEND
# 27 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8936/src/tzbsp_mutex_asm.s" 2

    GET qctps_common_macros.inc
    GET tzbsp_libarm_asm.inc
    GET tzbsp_asm.inc
    GET tzbsp_sys_arch.inc

    EXPORT tzbsp_mutex_init
    EXPORT tzbsp_mutex_trylock
    EXPORT tzbsp_mutex_lock
    EXPORT tzbsp_mutex_unlock

    CODE32
    PRESERVE8

    AREA TZBSP_MUTEX_ASM, align=4, CODE, READONLY

; int _mutex_initialize(mutex *m)
; uint32 tzbsp_mutex_init(tzbsp_mutex_t* mutex)
;
; r0 - Address of the mutex structure
_mutex_initialize ; Alias for ARM C library
tzbsp_mutex_init FUNCTION
    mdmb
    mov r1, #0
    str r1, [r0]
    ; Return non-zero to tell the ARM C libraries that we are running
    ; multithreaded code.
    mov r0, #1
    bx lr
    ENDFUNC

; void _mutex_acquire(mutex *m)
; void tzbsp_mutex_lock(tzbsp_mutex_t* mutex)
;
; Waits until the provided mutex lock has been reclaimed.
;
; r0 - Address of the mutex structure
_mutex_acquire ; Alias for ARM C library
tzbsp_mutex_lock FUNCTION
    CurCPUNum r1, r2
    add r1, #1
42 ldrex r2, [r0] ; Load the current lock status.
    cmp r2, #0 ; Is locked?
    strexeq r2, r1, [r0] ; Not locked: try to claim the lock.
    cmpeq r2, #0 ; Not locked: Did we get the lock?
    bne %b42 ; Locked or claiming lock failed, retry.
    mdmb ; Ensure other observers see lock claim.
    bx lr
    ENDFUNC

; void _mutex_acquire(mutex *m)
; void tzbsp_mutex_trylock(tzbsp_mutex_t* mutex)
;
; Waits until the provided mutex lock has been reclaimed.
;
; r0 - Address of the mutex structure
tzbsp_mutex_trylock FUNCTION
    CurCPUNum r1, r2
    add r1, #1
    ldrex r2, [r0] ; Load the current lock status.
    cmp r2, #0 ; Is locked?
    strexeq r2, r1, [r0] ; Not locked: try to claim the lock.
    cmpeq r2, #0 ; Not locked: Did we get the lock?
    bne %f42 ; Locked or claiming lock failed, retry.
    mdmb ; Ensure other observers see lock claim.
    mov r0, #1
    bx lr
42 mov r0, #0
    bx lr
    ENDFUNC

; void _mutex_release(mutex *m)
; void tzbsp_mutex_unlock(tzbsp_mutex_t* mutex)
;
; r0 - Address of the mutex structure
_mutex_release ; Alias for ARM C library
tzbsp_mutex_unlock FUNCTION
    ; Make sure other CPU's observe any writes before releasing the lock.
    mdmb
    mov r1, #0 ; Zero equals unlocked.
    str r1, [r0] ; Clear the lock
    ldr r1, [r0] ; Perform Read-after-Write otherwise the write
                                    ; is not guaranteed to be released.
    ; Make sure other CPU's observe any writes after releasing the lock.
    mdmb
    bx lr
    ENDFUNC

    END
