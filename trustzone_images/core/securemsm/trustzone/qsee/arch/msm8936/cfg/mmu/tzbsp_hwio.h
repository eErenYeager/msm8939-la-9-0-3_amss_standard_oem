#ifndef TZBSP_HWIO_H
#define TZBSP_HWIO_H

/*===========================================================================

                    T r u s t z o n e   T a r g e t
                          H e a d e r  F i l e

DESCRIPTION
  This header file contains declarations and definitions for the TZBSP
  that is target-specific

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None

Copyright (c) 2011-2012 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8936/cfg/mmu/tzbsp_hwio.h#1 $
$DateTime: 2018/02/07 00:37:16 $
$Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
05/16/12   jl      Initial version
============================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/
#include "../common/tzbsp_hwio_temp.h"
#include "../common/tzbsp_hwio_prng.h"
#include "../common/tzbsp_hwio.h"

/*===========================================================================

                      PUBLIC DATA DECLARATIONS

===========================================================================*/

#endif /* TZBSP_HWIO_H */
