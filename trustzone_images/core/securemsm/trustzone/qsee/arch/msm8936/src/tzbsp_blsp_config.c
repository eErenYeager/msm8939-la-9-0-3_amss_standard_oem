/*===========================================================================
  Copyright (c) 2015 by QUALCOMM Technologies, Incorporated.  All Rights Reserved.
  ===========================================================================*/

/*===========================================================================

  when       who     what, where, why
  --------   ---     ------------------------------------------------------------
  05/01/15   dpk     Created for 8936
  =============================================================================*/
/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/


#include "tzbsp_blsp.h"
#include "tzbsp_xpu_def.h"
#include "tzbsp_xpu.h"


#define TZ_BSP_BLSP_QUPS_PROTECTED 0 /*Number of QUP instances to be protected.*/

const uint32 TzBspBlspNumQups = TZ_BSP_BLSP_QUPS_PROTECTED;


/* This array contains the QUP device id's details for which the QUP and GPIO protection has to be enabled in TZ Cold Boot stage.
 * Set the value of macro to the number of QUP instances for which the protection required at cold boot stage. The macro value and
 * the number of valid QUP instances in the array must be equal.
 */
const TzBspBlspQupId TzBspBlspQupsProtected[TZ_BSP_BLSP_QUPS_PROTECTED] = {};

/* This structure contains QUP information required for protection. Uncomment only required QUP information.
 * Set the value of the macro "TZ_BSP_BLSP_QUPS_PROTECTED" to the number of configurations used.
 * Note: For GPIO index value, the customer must contact Buses team before using. The unused indices must be set to zero.
 */
/*
Information Format::
TZBSP_BLSP_QUP_INFO(QUP_PROTOCOL, PERSISTENCE, GPIO_PROTECTION_TYPE, 
                    QUP_ID, QUP_RG_INDEX, BLSP_XPU_ID, 
                    GPIO_1, GPIO_2, GPIO_3, GPIO_4, GPIO_5, GPIO_6, NUM_GPIOS, GPIO_INDEX_1, GPIO_INDEX_2, GPIO_INDEX_3, 
                    GPIO_ADDR_1, GPIO_ADDR_2, GPIO_ADDR_3, GPIO_ADDR_4, GPIO_ADDR_5, GPIO_ADDR_6, 
                    GPIO_VALUE_1, GPIO_VALUE_2, GPIO_VALUE_3, GPIO_VALUE_4, GPIO_VALUE_5, GPIO_VALUE_6)
*/ 
const TzBsp_Blsp_Qup_Info  TzBspBlspQupInfo[TZ_BSP_BLSP_QUPS_PROTECTED] = 
{
   /* QUP 1 */
   /* TZBSP_BLSP_QUP_INFO(QUP_PROTOCOL_SPI, NON_PERSISTENCE, PROTECTION_TYPE_MPU, 
                       QUP_DEVICE_ID_1, TZBSP_BAM_BLSP1_RG_QUP0, HAL_XPU2_BAM_BLSP1_DMA,
                       0x01000000,0x01004000,0x0,0x0,0x0,0x0,4,14,0,0,
                       0x01000000,0x01001000,0x01002000,0x01003000,0x0,0x0,
                       0x000002C5,0x000002C5,0x000002C5,0x000002C5,0x0,0x0), */
   /* TZBSP_BLSP_QUP_INFO(QUP_PROTOCOL_I2C, NON_PERSISTENCE, PROTECTION_TYPE_MPU,
                       QUP_DEVICE_ID_1, TZBSP_BAM_BLSP1_RG_QUP0, HAL_XPU2_BAM_BLSP1_DMA,
                       0x01002000,0x01004000,0x0,0x0,0x0,0x0,2,15,15,15,
                       0x01002000,0x01003000,0x0,0x0,0x0,0x0,
                       0x000002CF,0x000002CF,0,0,0,0), */
   
   /* QUP 2 */
   /* TZBSP_BLSP_QUP_INFO(QUP_PROTOCOL_SPI, NON_PERSISTENCE, PROTECTION_TYPE_MPU, 
                       QUP_DEVICE_ID_2, TZBSP_BAM_BLSP1_RG_QUP1, HAL_XPU2_BAM_BLSP1_DMA,
                       0x01004000,0x01008000,0x0,0x0,0x0,0x0,4,15,15,15,
                       0x01004000,0x01005000,0x01006000,0x01007000,0x0,0x0,
                       0x000002C5,0x000002C5,0x000002C5,0x000002C5,0x0,0x0), */
   /* TZBSP_BLSP_QUP_INFO(QUP_PROTOCOL_I2C, NON_PERSISTENCE, PROTECTION_TYPE_MPU, 
                       QUP_DEVICE_ID_2, TZBSP_BAM_BLSP1_RG_QUP1, HAL_XPU2_BAM_BLSP1_DMA,
                       0x01006000,0x01008000,0x0,0x0,0x0,0x0,2,15,15,15,
                       0x01006000,0x01007000,0x0,0x0,0x0,0x0,
                       0x000002CF,0x000002CF,0,0,0,0), */
   
   /* QUP 3 */
   /* TZBSP_BLSP_QUP_INFO(QUP_PROTOCOL_SPI, NON_PERSISTENCE, PROTECTION_TYPE_MPU, 
                       QUP_DEVICE_ID_3, TZBSP_BAM_BLSP1_RG_QUP2, HAL_XPU2_BAM_BLSP1_DMA,
                       0x01008000,0x0100C000,0x0,0x0,0x0,0x0,4,15,0,0,
                       0x01008000,0x01009000,0x0100A000,0x0100B000,0x0,0x0,
                       0x000002C5,0x000002C5,0x000002C5,0x000002C5,0x0,0x0), */
   /* TZBSP_BLSP_QUP_INFO(QUP_PROTOCOL_I2C, NON_PERSISTENCE, PROTECTION_TYPE_MPU, 
                       QUP_DEVICE_ID_3, TZBSP_BAM_BLSP1_RG_QUP2, HAL_XPU2_BAM_BLSP1_DMA,
                       0x0100A000,0x0100C000,0x0,0x0,0x0,0x0,2,15,15,15,
                       0x0100A000,0x0100B000,0x0,0x0,0x0,0x0,
                       0x000002CB,0x000002CB,0,0,0,0), */
   
   /* QUP 4 */
   /* TZBSP_BLSP_QUP_INFO(QUP_PROTOCOL_SPI, NON_PERSISTENCE, PROTECTION_TYPE_MPU, 
                       QUP_DEVICE_ID_4, TZBSP_BAM_BLSP1_RG_QUP3, HAL_XPU2_BAM_BLSP1_DMA,
                       0x0100C000,0x01010000,0x0,0x0,0x0,0x0,4,15,15,15,
                       0x0100C000,0x0100D000,0x0100E000,0x0100F000,0x0,0x0,
                       0x000002C5,0x000002C5,0x000002C5,0x000002C5,0x0,0x0), */
   /* TZBSP_BLSP_QUP_INFO(QUP_PROTOCOL_I2C, NON_PERSISTENCE, PROTECTION_TYPE_MPU, 
                       QUP_DEVICE_ID_4, TZBSP_BAM_BLSP1_RG_QUP3, HAL_XPU2_BAM_BLSP1_DMA,
                       0x0100E000,0x01010000,0x0,0x0,0x0,0x0,2,15,15,15,
                       0x0100E000,0x0100F000,0x0,0x0,0x0,0x0,
                       0x000002CB,0x000002CB,0,0,0,0), */
   
   /* QUP 5 */
   /* TZBSP_BLSP_QUP_INFO(QUP_PROTOCOL_SPI, NON_PERSISTENCE, PROTECTION_TYPE_MPU, 
                       QUP_DEVICE_ID_5, TZBSP_BAM_BLSP1_RG_QUP4, HAL_XPU2_BAM_BLSP1_DMA,
                       0x01010000,0x01014000,0x0,0x0,0x0,0x0,4,15,15,15,
                       0x01010000,0x01011000,0x01012000,0x01013000,0x0,0x0,
                       0x000002C5,0x000002C5,0x000002C5,0x000002C5,0x0,0x0), */
   /* TZBSP_BLSP_QUP_INFO(QUP_PROTOCOL_I2C, NON_PERSISTENCE, PROTECTION_TYPE_MPU, 
                       QUP_DEVICE_ID_5, TZBSP_BAM_BLSP1_RG_QUP4, HAL_XPU2_BAM_BLSP1_DMA,
                       0x01012000,0x01014000,0x0,0x0,0x0,0x0,2,15,15,15,
                       0x01012000,0x01013000,0x0,0x0,0x0,0x0,
                       0x000002CB,0x000002CB,0,0,0,0), */
   
   /* QUP 6 */
   /* TZBSP_BLSP_QUP_INFO(QUP_PROTOCOL_SPI, NON_PERSISTENCE, PROTECTION_TYPE_MPU, 
                       QUP_DEVICE_ID_6, TZBSP_BAM_BLSP1_RG_QUP5, HAL_XPU2_BAM_BLSP1_DMA,
                       0x01014000,0x01018000,0x0,0x0,0x0,0x0,4,15,15,15,
                       0x01014000,0x01015000,0x01016000,0x01017000,0x0,0x0,
                       0x000002C5,0x000002C5,0x000002C5,0x000002C5,0x0,0x0) */
   /* TZBSP_BLSP_QUP_INFO(QUP_PROTOCOL_I2C, NON_PERSISTENCE, PROTECTION_TYPE_MPU, 
                       QUP_DEVICE_ID_6, TZBSP_BAM_BLSP1_RG_QUP5, HAL_XPU2_BAM_BLSP1_DMA,
                       0x01016000,0x01018000,0x0,0x0,0x0,0x0,2,15,15,15,
                       0x01016000,0x01017000,0x0,0x0,0x0,0x0,
                       0x000002CB,0x000002CB,0,0,0,0) */
};



