#ifndef TZBSP_CP_H
#define TZBSP_CP_H

/**
@file tzbsp_cp.h
@brief Header file for Content Protection related functions.
*/
/*===========================================================================
   Copyright (c) 2012-2015 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8936/src/tzbsp_cp.h#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $

when       who      what, where, why
--------   ---      ------------------------------------
16/02/15   c_sduc   Application buffer support.
07/25/12   amen     added usecase enum (prev defined in mpu.h)
06/12/12   rkk      Initial version.

===========================================================================*/

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include <comdef.h>
#include "tzbsp_syscall.h"
#include <stdint.h>

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/
//dynamic buffer version
#define CP_MAJ  1
#define CP_MIN  1
#define CP_STEP 0

/* -----------------------------------------------------------------------
 * Typedefs
 * ----------------------------------------------------------------------- */

typedef enum
{
  TZBSP_MEM_TAG_USECASE_CP       = 0x01,
  TZBSP_MEM_TAG_USECASE_AUDIO_CP = 0x02,
  TZBSP_MEM_TAG_USECASE_VIDEO_PIXEL_CP = 0x03,
  TZBSP_MEM_TAG_USECASE_SD       = 0x04,
  TZBSP_MEM_TAG_USECASE_SC       = 0x05,
  TZBSP_MEM_TAG_USECASE_APP      = 0x06,
  TZBSP_MEM_TAG_USECASE_MAX      = 0x7FFFFFFF
}tzbsp_mem_usecase_tag_id_t;

/*----------------------------------------------------------------------------
 * Function Declarations and Documentation
 * -------------------------------------------------------------------------*/

/**
 * Protects(locks/unlocks) a list of memory chunks with an MPU.
 *
 * @param [in] chunks    Memory chunk list
 * @param [in] mem_usage Hint on the memory usage.
 * @param [in] lock      TRUE for lock, FALSE for unlock
 * @param [in] flags     Future use
 * @param [out] rsp      Pointer to return response to caller
 *
 * @return \c E_SUCCESS if the lock was successfully done,
 *         error code otherwise.
 */
int tzbsp_memprot_lock2(tz_memprot_chunks_info_t* chunks,
                        tz_mem_usage_e_type       mem_usage,
                        uint32                    lock,
                        uint32                    flags,
                        tzbsp_smc_rsp_t*          rsp);

/**
 * Maps a list of protected memory chunks.
 *
 * @param [in] chunks    Memory chunk list
 * @param [in] map       Map-to information
 * @param [in] flags     Currently supported flags:
 *                         a) TZ_MEMPROT_FLAG_TLBINVALIDATE
 * @param [out] rsp      Pointer to return response to caller
 *
 * @return \c E_SUCCESS if the map was successfully done,
 *         error code otherwise.
                                                           */
int tzbsp_memprot_map2(tz_memprot_chunks_info_t* chunks,
                       tz_memprot_map_info_t*    map,
                       uint32                    flags,
                       tzbsp_smc_rsp_t*          rsp);

/**
 * Unmaps (removes) Virtual Address to Physical Address mapping
 * for given range.
 *
 * @param [in] map       Mapped-at information
 * @param [in] flags     Currently supported flags:
 *                         a) TZ_MEMPROT_FLAG_TLBINVALIDATE
 * @param [out] rsp      Pointer to return response to caller
 *
 * @return \c E_SUCCESS if the unmap was successfully done,
 *         error code otherwise.
 */
int tzbsp_memprot_unmap2(tz_memprot_map_info_t* map,
                         uint32                 flags,
                         tzbsp_smc_rsp_t*       rsp);

/**
 * Invalidates the context bank of the SMMU associated with the specified
 * pagetable
 *
 * @param [in] device_id     Id of Device which needs TLB
 *                           invalidated (tz_device_e_type)
 * @param [in] ctxt_bank_id  Context bank id which needs TLB
 *                           invalidated. These correspond to
 *                           context bank numbers and are unique
 *                           only for the device. Value start
 *                           from 0 which corresponds to CB0
 * @param[in] flags          Future use
 * @param[in] spare          Future use
 * @param [out] rsp          Pointer to return response to caller
 *
 * @returns \c E_SUCCESS on success, negative values on failure
 */

int tzbsp_memprot_tlbinval(uint32           device_id,
                           uint32           ctxt_bank_id,
                           uint32           flags,
                           uint32           spare,
                           tzbsp_smc_rsp_t* rsp);

/**
 * Tests whether all of the range [\c start, \c end] is in secure
 * memory and also tagged for a particular use case.
 *
 * @param [in] tag   Tag ID of the memory it should be tagged with
 * @param [in] start Start of the memory range, physical address,
 *                   included in the range.
 * @param [in] end   End of the memory range, physical address,
 *                   included  in the range.
 *
 * @return \c TRUE if the entire area is in secure memory.
 *         \c FALSE if the area contains non-secure memory.
 */

boolean tzbsp_memprot_is_s_tag_area(tzbsp_mem_usecase_tag_id_t tag,
                                    uint32                     start,
                                    uint32                     end);

/**
 * Counts how many chunks are protected for a specific memory usage.
 *
 * @param [in] usage  Hint on the memory usage.
 *
 * @return Number of chunks protected for this usage
 */
uint32 tzbsp_memprot_mem_usage_count(tzbsp_mem_usecase_tag_id_t usage);

/**
 * Called to enable or disable the Secure Display.
 *
 * When enabled, the MDP SMMU is configured so that no
 * content from CB0 can be displayed by the MDP HW.
 * When enabled, the secure display must be disabled
 * first by QSEE, and only afterwards it can be disabled
 * via this syscall by HLOS.
 *
 * @param[in]  enable   1 to enable it, 0 to disable
 * @param[out] rsp      Result of this syscall
 *
 * @returns \c E_SUCCESS on success, negative error code otherwise
 *          \c -E_INVALID_ARG if the size or address are
 *                            misaligned, or size is too small, or
 *                            if argument is neither 1 nor 0
 *          \c -E_NOT_ALLOWED if requesting to disable while
 *                            QSEE lock is still in place
 *          \c -E_BAD_ADDRESS if the memory is secure
 *          \c -E_FAILURE     if memory could not be MPU protected
 */
int tzbsp_memprot_sd_ctrl(uint32 enable, tzbsp_smc_rsp_t *rsp);

/**
 * Get the session identifier for the current Secure Display session.
 *
 * @return 0 if no session is in progress, the identifier otherwise.
 */
uint32 tzbsp_memprot_sd_get_session(void);

/**
 * Tag all memory in the range [\c start, \c end] with the specified
 * tag ID.
 *
 * @param [in] tag   Tag ID of the memory it should be tagged with
 * @param [in] start Start of the memory range, physical address,
 *                   included in the range.
 * @param [in] end   End of the memory range, physical address,
 *                   included  in the range.
 *
 * @return \c E_SUCCESS on success, negative error code otherwise
 *         \c -E_NOT_ALLOWED if any memory in the range is already
 *                           tagged with a different tag
 */
int tzbsp_memprot_tag_mem(tzbsp_mem_usecase_tag_id_t tag,
                          uintptr_t                  start,
                          uintptr_t                  end);

#endif
