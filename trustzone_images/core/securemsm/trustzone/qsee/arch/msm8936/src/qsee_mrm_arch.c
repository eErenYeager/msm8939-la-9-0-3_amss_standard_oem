
#include <comdef.h>
#include "memory_defs.h"
#include "tzbsp_target.h"
#include "qsee_mrm_arch.h"
#include "tzbsp_hwio.h"
#include "tt_tz.h"

//extern int Image$$TZBSP_CODE_POST_MON$$Base; //__attribute__((weak))
//extern int Image$$TZBSP_CODE_POST_MON$$Length; //__attribute__((weak))

//extern int Image$$TZBSP_DP_DDR$$Base; //__attribute__((weak))
//extern int Image$$TZBSP_DP_DDR$$Length; //__attribute__((weak))


#define VENUS_XPU_START_ADDR_PHYS       0x01DF0000
#define VENUS_XPU_END_ADDR_PHYS         0x01E00000

#define VENUS_XPU_SIZE                  (VENUS_XPU_END_ADDR_PHYS   - VENUS_XPU_START_ADDR_PHYS)
#define VENUS_XPU_START_ADDR            (VENUS_XPU_START_ADDR_PHYS + TZBSP_SANDBOX_RELOCATE_OFFSET)

#define ARRAY_SIZE(xx) (sizeof(xx) / sizeof(xx[0]))

extern int Image$$TZBSP_CODE$$Base;
extern int Image$$TZBSP_CODE$$Length;

extern int Image$$MINK_MODULE_IMG_AUTH_RO$$Base;
extern int Image$$MINK_MODULE_IMG_AUTH_RO$$Length;

static mem_block_t venus_init_map[] =
{
/*
 * There is a HW bug which causes back to back writes to venus xpu space to
 * get corrupted or return a noc error. The workaroudn is to make venus
 * xpu space as strongly ordered
 */
 { VENUS_XPU_START_ADDR,      // Virtual address
   VENUS_XPU_START_ADDR_PHYS, // Physical address
   (VENUS_XPU_SIZE)>>10,
   (TZ_READ_WRITE_PL1 | TZ_MEM_STRONGLY_ORDERED | TZ_INR_SHAREABLE | TZ_NON_EXECUTABLE)
 }
};
static p_addr_t code_base_addrs[] =
{
  (p_addr_t) &Image$$TZBSP_CODE$$Base,
  // (p_addr_t) &Image$$TZBSP_CODE_POST_MON$$Base,
  /* TODO: map demand paged region for virtio */
  // (p_addr_t) &Image$$TZBSP_DP_DDR$$Base,
  (p_addr_t) &Image$$MINK_MODULE_IMG_AUTH_RO$$Base,
  (p_addr_t) TZ_VAS_LAST_ADDRESS
};

static size_t code_lens[] =
{
  (size_t) &Image$$TZBSP_CODE$$Length,
  // (size_t) &Image$$TZBSP_CODE_POST_MON$$Length,
  /* TODO: map demand paged region as executable for virtio */
  // (size_t) &Image$$TZBSP_DP_DDR$$Base,
  (size_t) &Image$$MINK_MODULE_IMG_AUTH_RO$$Length,
  (size_t) TZ_VAS_LAST_ADDRESS
};

p_addr_t* qsee_mrm_get_code_base_addrs_arch(void)
{
  return code_base_addrs;
}

size_t* qsee_mrm_get_code_lens_len_arch(void)
{
  return code_lens;
}

p_addr_t qsee_mrm_get_demand_paging_code_base_addr_arch(void)
{
  return (p_addr_t) 0x0;  //&Image$$TZBSP_DP_DDR$$Base;
}

size_t qsee_mrm_get_demand_paging_code_len_arch(void)
{
  return (size_t) 0x0;  //&Image$$TZBSP_DP_DDR$$Length;
}

p_addr_t qsee_mrm_get_sbl_base_addr_arch(void)
{
  /* TODO: figure out the sbl base address for 8916*/
  return (p_addr_t) 0x0;
}

size_t qsee_mrm_get_sbl_base_len_arch(void)
{
  return (size_t) 0x0;
}

mem_block_t* qsee_mrm_get_venus_xpu_init_map_arch(void)
{
  return venus_init_map;
}

unsigned int qsee_mrm_get_venus_xpu_init_map_len_arch(void)
{
  return ARRAY_SIZE(venus_init_map);
}
