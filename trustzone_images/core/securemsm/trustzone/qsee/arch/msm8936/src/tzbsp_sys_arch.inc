;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;
; GENERAL DESCRIPTION
;   This file contains the macros for TZ EL1 assembly files
;
; INITIALIZATION AND SEQUENCING REQUIREMENTS
;
; Copyright (c) 2014 by QUALCOMM, Incorporated.All Rights Reserved.
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;
;                           EDIT HISTORY FOR FILE
;
; This section contains comments describing changes made to the module.
; Notice that changes are listed in reverse chronological order.
;
;
; $Header: $
; when       who     what, where, why
; --------   ---     --------------------------------------------------------
; 04/04/14   pre     Initial Revision
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

NUM_CPUS_PER_CLUSTER EQU 0x4

; ----------------
; MACRO: CurCPUNum
; ----------------
; Keep two arguments so the macro definition stays consistent between targets

    MACRO
    CurCPUNum $rx, $ry
        RCP15_MPIDR $rx
        and     $ry, $rx, #0xFF00
        lsr     $ry, #8
        mov     $rx, #NUM_CPUS_PER_CLUSTER
        mul     $ry, $rx
        RCP15_MPIDR $rx
        and     $rx, #0xFF
        add     $rx, $ry
    MEND

    END
