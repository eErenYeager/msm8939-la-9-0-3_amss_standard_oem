/*===========================================================================
  Copyright (c) 2015 by QUALCOMM Technologies, Incorporated.  All Rights Reserved.
  ===========================================================================*/

/*===========================================================================

  when       who     what, where, why
  --------   ---     ------------------------------------------------------------
  05/01/15   dpk     Created for 8936
  =============================================================================*/
/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/

#include "tzbsp_blsp.h"
#include "tzbsp_blsp_arch.h"
#include "tzbsp_errno.h"
#include "tzbsp_vmid_config.h"


extern const uint32 TzBspBlspNumQups;
extern const TzBspBlspQupId TzBspBlspQupsProtected[];
extern const TzBsp_Blsp_Qup_Info  TzBspBlspQupInfo[];

static const TzBsp_Blsp_Qup_Info *tzbsp_blsp_get_qup_info(uint32 qupId);

static int tzbsp_blsp_qup_lock(const TzBsp_Blsp_Qup_Info *pTzBspQupInfo, uint32 vmid_access_bitmap, boolean lock);

static int tzbsp_blsp_gpio_protection(const TzBsp_Blsp_Qup_Info *pTzBspQupInfo, boolean lockGpio);




int tzbsp_blsp_modify_ownership(uint32 blsp_periph_num, tzbsp_blsp_owner_ss_type owner_ss)
{
  return -E_NOT_SUPPORTED;
}

int tzbsp_blsp_init(void)
{
   int res = E_SUCCESS;
   boolean clkStatus;
   uint32 qupId;
   int i;
   const TzBsp_Blsp_Qup_Info *pTzBspQupInfo;
   
      
   for (i = 0; i < TzBspBlspNumQups; i++)
   {
      qupId = 0;
      pTzBspQupInfo = NULL;
      clkStatus = FALSE;  
	  
      qupId = TzBspBlspQupsProtected[i];
      if ((qupId == 0) || (qupId > TZBSP_TOTAL_QUPS))
      {
         TZBSP_LOG(TZBSP_MSG_ERROR, "tzbsp_blsp_init(), QUP ID %d Not supported", qupId);         
      }
      else
      {         
         pTzBspQupInfo = tzbsp_blsp_get_qup_info(qupId);
         if (NULL == pTzBspQupInfo)
         {
            TZBSP_LOG(TZBSP_MSG_ERROR, "tzbsp_blsp_init(): QUP information not found for QUP ID %d", qupId);
            break;
         }
         else 
         {
            clkStatus = tzbsp_blsp_is_clock_enabled(qupId);
	  
            if (!clkStatus)
            {
               res = tzbsp_blsp_set_clock(qupId, TRUE);
               if (E_SUCCESS != res)
               {
                  TZBSP_LOG(TZBSP_MSG_ERROR, "tzbsp_blsp_init(): Enable AHB clock failed for QUP ID %d, res: %08x", qupId, res);
                  break;	  
               }
            }
	  
            res = tzbsp_blsp_qup_lock(pTzBspQupInfo, TZBSP_VMID_TZ_BIT, TRUE);
            if (E_SUCCESS != res)
            {
               TZBSP_LOG(TZBSP_MSG_ERROR, "tzbsp_blsp_init(): Qup Lock failed for QUP ID %d, res: %08x", qupId, res);
               break;	  
            }
	
            res = tzbsp_blsp_gpio_protection(pTzBspQupInfo, TRUE);
            if (E_SUCCESS != res)
            {
               TZBSP_LOG(TZBSP_MSG_ERROR, "tzbsp_blsp_init(): GPIO Protection failed for QUP ID %d, res: %08x", qupId, res);
               break;	  
            }
			
            res = tzbsp_blsp_gpio_configuration(pTzBspQupInfo);
            if (E_SUCCESS != res)
            {
               TZBSP_LOG(TZBSP_MSG_ERROR, "tzbsp_blsp_init(): GPIO Configuration failed for QUP ID %d, res: %08x", qupId, res);
               break;
            }
     
            if (!clkStatus)
            {
               res = tzbsp_blsp_set_clock(qupId, FALSE);
               if (E_SUCCESS != res)
               {
                  TZBSP_LOG(TZBSP_MSG_ERROR, "tzbsp_blsp_init(): Disable AHB clock failed for QUP ID %d, res: %08x", qupId, res);
                  break;	  
               }
            }
         }         
      }
   }
   
   return res;   
}

/** @brief Get QUP information from the Qup ID
  
    @param[in] qupId   Qup id 
        
    @return Nothing
*/
static const TzBsp_Blsp_Qup_Info *tzbsp_blsp_get_qup_info(uint32 qupId)
{
   uint32 i;
   
   
   for (i = 0; i < TzBspBlspNumQups; i++)
   {
      if (qupId == TzBspBlspQupInfo[i].qupId)
      {
         return &TzBspBlspQupInfo[i]; 
      }
   }
   
   return NULL;      
}

/** @brief Locks QUP for a sub-system

    @param[in] pTzBspQupInfo         QUP Information.
    @param[in] vmid_access_bitmap    VMID Bit map specifying the subsystem.
    @param[in] lock                  Whether to protect the QUP or not.
                                     TRUE - Protect QUP
                                     FALSE - Un-Protect QUP
    
    @return E_SUCCESS if success otherwise error code.
*/
static int tzbsp_blsp_qup_lock (const TzBsp_Blsp_Qup_Info *pTzBspQupInfo, uint32 vmid_access_bitmap, boolean lock)
{
   int i, res = E_SUCCESS;
   tzbsp_rpu_rg_t rg;
   uint32 xpu;
   

   rg.index = pTzBspQupInfo->uQupRgIndex;

   if (lock)
   {
      rg.flags = TZBSP_XPU_SEC;
      rg.read_vmid =  vmid_access_bitmap;
      rg.write_vmid = vmid_access_bitmap;
   }
   else
   { 
      rg.flags = TZBSP_RWE | TZBSP_XPU_NON_SEC ;
      rg.read_vmid = ~(TZBSP_VMID_NOACCESS_BIT);
      rg.write_vmid = ~(TZBSP_VMID_NOACCESS_BIT);
   }

   xpu = pTzBspQupInfo->uBlspXpuId;

   if (TZBSP_VMID_TZ_BIT == vmid_access_bitmap)
      res = tzbsp_rpu_reconfigure(xpu, &rg, TRUE); //Only TZ needs secure lock
   else
      res = tzbsp_rpu_reconfigure(xpu, &rg, FALSE);

   if (res != E_SUCCESS)
   {
      TZBSP_LOG(TZBSP_MSG_ERROR, "tzbsp_blsp_qup_lock(), res: %08x", res);
   }

   return res;
}

/** @brief Protects and De-protects GPIO's
  
    @param[in] pTzBspQupInfo   QUP Information.
    @param[in] lockGpio        Whether to protect the GPIO or not.
                               TRUE - Protect GPIO
                               FALSE - Un-Protect GPIO
    
    @return E_SUCCESS if success otherwise error code.
*/
static int tzbsp_blsp_gpio_protection(const TzBsp_Blsp_Qup_Info *pTzBspQupInfo, boolean lockGpio)
{
   int res = E_SUCCESS;
   
   
   if (lockGpio)
   {
      res = tzbsp_blsp_lock_gpio(pTzBspQupInfo);
   }
   else 
   {
      res = tzbsp_blsp_unlock_gpio(pTzBspQupInfo);   
   }
   
   return res;
}



