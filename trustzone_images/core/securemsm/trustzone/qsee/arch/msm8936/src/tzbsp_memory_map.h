#ifndef TZBSP_MEMORY_MAP_H
#define TZBSP_MEMORY_MAP_H

/*===========================================================================
Copyright (c) 2012 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8936/src/tzbsp_memory_map.h#1 $
$DateTime: 2018/02/07 00:37:16 $
$Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
08/11/12   tk      First version.
============================================================================*/

#include "tzbsp_target.h"

/*TZ non-relocatable partition range in DDR */
#define TZBSP_EBI1_TZ_BASE             SCL_TZ_DDR_START_ADDR
#define TZBSP_EBI1_TZ_END              (SCL_TZ_DDR_START_ADDR + 0x180000)
#define TZBSP_EBI1_TZ_SIZE             (TZBSP_EBI1_TZ_END - \
                                         (TZBSP_EBI1_TZ_BASE))

/*HYP non-relocatable partition range in DDR */
#define TZBSP_EBI1_HYP_BASE             0x86400000
#define TZBSP_EBI1_HYP_END              0x86500000
#define TZBSP_EBI1_HYP_SIZE             (TZBSP_EBI1_HYP_END - \
                                         (TZBSP_EBI1_HYP_BASE))

/*TZ relocatable APP partition range in DDR */
#define TZBSP_EBI1_TZ_APP_BASE             0x86000000
#define TZBSP_EBI1_TZ_APP_END              0x86300000
#define TZBSP_EBI1_TZ_APP_SIZE             (TZBSP_EBI1_TZ_APP_END - \
                                         (TZBSP_EBI1_TZ_APP_BASE))

/*RFSA Buffer  partition range in DDR */
#define TZBSP_EBI1_RFSA_BASE             0x867E0000
#define TZBSP_EBI1_RFSA_END              0x86800000
#define TZBSP_EBI1_RFSA_SIZE             (TZBSP_EBI1_RFSA_END - \
                                         (TZBSP_EBI1_RFSA_BASE))

/* EBI1 shared memory. */
#define TZBSP_EBI1_SHARED_MEM_BASE      0x86300000
#define TZBSP_EBI1_SHARED_MEM_END       0x86346000  //ToDo-sg: Fix to 0x86400000
#define TZBSP_EBI1_SHARED_MEM_SIZE      (TZBSP_EBI1_SHARED_MEM_END - \
                                         (TZBSP_EBI1_SHARED_MEM_BASE))

/* BOOT ROM MPU CONFIG ADDR bits as 8 bits. configuring address as below, but
 * actual address is different. */
#define TZBSP_BOOT_ROM_MSS_BASE        0x00018000
#define TZBSP_BOOT_ROM_MSS_END         0x00024000
#define TZBSP_BOOT_ROM_MSS_SIZE       (TZBSP_BOOT_ROM_MSS_END - \
                                      (TZBSP_BOOT_ROM_MSS_BASE))

#define TZBSP_BOOT_ROM_APPS_BASE        0x00100000
#define TZBSP_BOOT_ROM_APPS_END         0x00118000
#define TZBSP_BOOT_ROM_APPS_SIZE       (TZBSP_BOOT_ROM_APPS_END - \
                                       (TZBSP_BOOT_ROM_APPS_BASE))

/* Used for xpu config only, can't use above _BASE, due to a
   bug,  xpu requires the 21st bit to be 0 */
#define TZBSP_BOOT_ROM_APPS_XPU_BASE    0x00000000
#define TZBSP_BOOT_ROM_APPS_XPU_END     0x00018000

//Used to Blacklist TZ/Mon TZ_NON_RELOC_REGION - Secure channels.
//End is TZBSP_EBI1_TZ_END - 3 x Secure Channel Size
#define TZBSP_FIXED_SECURE_DDR_BASE     SCL_TZ_DDR_START_ADDR
#define TZBSP_FIXED_SECURE_DDR_SIZE     (3 * 0x1000)
#define TZBSP_FIXED_SECURE_DDR_END      (TZBSP_EBI1_TZ_END - TZBSP_FIXED_SECURE_DDR_SIZE)

#define TZBSP_EBI1_SLIMBUS_XFR_BASE       0x86680000
#define TZBSP_EBI1_SLIMBUS_XFR_END        0x86681000
#define TZBSP_EBI1_SLIMBUS_XFR_SIZE       (TZBSP_EBI1_SLIMBUS_XFR_END - \
                                           TZBSP_EBI1_SLIMBUS_XFR_BASE)

#define TZBSP_SBL1_REGION_BASE            0x86700000
#define TZBSP_SBL1_REGION_END             0x867E0000
#define TZBSP_SBL1_REGION_SIZE           (TZBSP_SBL1_REGION_END - \
                                           TZBSP_SBL1_REGION_BASE)

/* IMEM */
#define TZBSP_IMEM_USB_TRACE_PIPE_BASE    0x08603800
#define TZBSP_IMEM_USB_TRACE_PIPE_END     0x08604000

#define TZBSP_IMEM_TZ_BASE                0x08601000
#define TZBSP_IMEM_TZ_END                 0x08603800

#define TZBSP_IMEM_SHARED_BASE            0x08600000
#define TZBSP_IMEM_SHARED_END             0x08601000

/* SSD */
#define TZBSP_SSD_REGION_BASE             (SCL_TZ_DDR_START_ADDR + 0xA0000)
#define TZBSP_SSD_REGION_SIZE             0x2000      /* 8K Size */
#define TZBSP_SSD_REGION_END              (TZBSP_SSD_REGION_BASE + TZBSP_SSD_REGION_SIZE)

#if ((TZBSP_EBI1_TZ_BASE > TZBSP_SSD_REGION_BASE) || \
     (TZBSP_EBI1_TZ_END  < TZBSP_SSD_REGION_END))
  #error "SSD region not within secure DDR region"
#endif

/* Subsystem Load Region Info.*/
#define TZBSP_SUBSYS_INFO_ADDRESS         (SCL_TZ_DDR_START_ADDR + 0xA2000)

/* Region in TZ DDR to store copy of amt table and its hash value */
#define TZBSP_AMT_DDR_BASE                (SCL_TZ_DDR_START_ADDR + 0xA3000)
#define TZBSP_AMT_DDR_SIZE                0x210

/* MobiCore */
#define TZBSP_EXT_ELF_TT_BASE             (TZBSP_ROUNDUP(uint32, \
                                          (SCL_TZ_DDR_START_ADDR + 0xA4000), \
                                          (1 << TZBSP_L1_PT_ALIGN_LOOKUP(TZ_VAS_SIZE))))

/* Region in TZ DDR to keep mink module code and data */
/* Defined in tzbsp_target.h since it get use in linker File
#define TZBSP_MINK_MODULES_DDR_BASE       (SCL_TZ_DDR_START_ADDR + 0xB0000)
#define TZBSP_MINK_MODULES_DDR_SIZE       0x10000 */

/* Area in DDR reserved for SEC.DAT. SBL will read SEC.DAT image
 * in DDR @0x82000 offset from this base address.
 */
#define TZBSP_SECDAT_BUFFER_BASE_ADDR     (SCL_TZ_DDR_START_ADDR + 0x40000)  /* actual address will be + 0x82000 & Size 2KB */

/* Area in DDR reserved for debugging information. SBL will dump TZ
 * here when entering DLOAD mode.
 */
#define TZBSP_DEBUG_AREA_BASE             (SCL_TZ_DDR_START_ADDR + 0xCB000)
#define TZBSP_TZ_BKUP_REGION_SIZE         0xB0000  /* 704 KB */

/* TZ Diagnostics area, within the TZ DDR non-relocatable region;
 * defined in tzbsp_target.h, but included here for completeness
 */
#define TZBSP_DIAG_BASE                   (SCL_TZ_DDR_START_ADDR + 0x17B000)
#define TZBSP_DIAG_END                    (TZBSP_DIAG_BASE + TZBSP_DIAG_SIZE)

#define TZBSP_EBI1_SECCHANNEL_MSS_BASE    (SCL_TZ_DDR_START_ADDR + 0x17D000)
#define TZBSP_EBI1_SECCHANNEL_MSS_SIZE    0x1000      /* 4K Size */
#define TZBSP_EBI1_SECCHANNEL_MSS_END     (TZBSP_EBI1_SECCHANNEL_MSS_BASE + TZBSP_EBI1_SECCHANNEL_MSS_SIZE)

#define TZBSP_EBI1_SECCHANNEL_LPASS_BASE  (SCL_TZ_DDR_START_ADDR + 0x17E000)
#define TZBSP_EBI1_SECCHANNEL_LPASS_SIZE  0x1000        /* 4K */
#define TZBSP_EBI1_SECCHANNEL_LPASS_END   (TZBSP_EBI1_SECCHANNEL_LPASS_BASE + TZBSP_EBI1_SECCHANNEL_LPASS_SIZE)

#define TZBSP_EBI1_SECCHANNEL_WCNSS_BASE  (SCL_TZ_DDR_START_ADDR + 0x17F000)
#define TZBSP_EBI1_SECCHANNEL_WCNSS_SIZE  0x1000        /* 4K */
#define TZBSP_EBI1_SECCHANNEL_WCNSS_END   (TZBSP_EBI1_SECCHANNEL_WCNSS_BASE + TZBSP_EBI1_SECCHANNEL_WCNSS_SIZE)

#endif /* TZBSP_MEMORY_MAP_H */
