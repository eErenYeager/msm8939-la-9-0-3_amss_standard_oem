/**
@file tzbsp_crypto.c
@brief Lock the hardware so that only TZ can use it
       The crypto hardware is shared by the HLOS. This means that
       there are all kinds of race conditions if the hardware access
       is not properly managed. The HLOS must ask TZ to use the hardware
       before it can be used. TZ must also lock access to this resource
       if the hardware is used.
*/

/*===========================================================================
 Copyright (c) 2010-2011 by Qualcomm Technologies, Incorporated. All Rights Reserved.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

 $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8936/src/tzbsp_crypto.c#1 $
 $DateTime: 2018/02/07 00:37:16 $
 $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
1/04/11    cap     initial version

===========================================================================*/

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include "tzbsp_errno.h"
#include "tzbsp_xpu.h"
#include "tzbsp_xpu_config.h"
#include "tzbsp_sys.h"
#include "tz_syscall_pub.h"
#include "crypto/tzbsp_crypto.h"


/*===========================================================================
                       GLOBAL VARIABLES
===========================================================================*/

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Function Declarations and Definitions
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Function Declarations and Documentation
 * -------------------------------------------------------------------------*/

/**
 * @brief Intialize the HW crypto engine
 *
 * @return 0 on success, negative on failure
 */
int tzbsp_hw_crypto_init(void)
{
  return E_SUCCESS;
}

boolean tzbsp_crypto_lock_engine(boolean lock)
{
  /* Crypto sharing is achieved by BAM pipes at HW level. */
  return TRUE;
}
