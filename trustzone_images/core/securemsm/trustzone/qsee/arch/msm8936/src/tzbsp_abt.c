/*===========================================================================
   Copyright (c) 2012 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8936/src/tzbsp_abt.c#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $


when       who     what, where, why
--------   ---     ------------------------------------------------------------
08/22/12   jl      Initial version.
=============================================================================*/
#include <comdef.h>
#include <HALhwio.h>
#include <ABTimeout.h>

#include "tzbsp.h"
#include "tzbsp_hwio.h"
#include "tzbsp_secboot.h"

void tzbsp_abt_init()
{
#if !defined(TZBSP_NO_ABT)
  /* Enabling the AHB Timeout debug depends on whether secboot is enabled */
  if(!tzbsp_secboot_hw_is_auth_enabled(1))
  {
    HWIO_OUTF(TCSR_TIMEOUT_SLAVE_GLB_EN, TIMEOUT_SLAVE_GLB_EN, 0x1);
    ABT_Init();
  }
#endif
}

