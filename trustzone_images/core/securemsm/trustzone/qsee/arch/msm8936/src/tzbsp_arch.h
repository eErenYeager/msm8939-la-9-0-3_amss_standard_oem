#ifndef TZBSP_ARCH_H
#define TZBSP_ARCH_H

/*===========================================================================
Copyright (c) 2010 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8936/src/tzbsp_arch.h#1 $
$DateTime: 2018/02/07 00:37:16 $
$Author: mplp4svc $

============================================================================*/
uint32 tzbsp_is_a53_cluster(void);
uint8 tzbsp_get_cluster_num(void);
uint32 tzbsp_get_num_cpus_in_cluster(uint32 cluster_num);

#endif
