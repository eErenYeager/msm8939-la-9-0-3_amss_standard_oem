/*===========================================================================
   Copyright (c) 2010 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8916/src/tzbsp_blacklist.c#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $


when       who     what, where, why
--------   ---     ------------------------------------------------------------
03/22/12   amen    Video subsystem
10/01/10   tk      Initial version.
=============================================================================*/

#include <comdef.h>
#include "tzbsp.h"
#include "tzbsp_mem.h"
#include "tzbsp_xpu.h"
#include "tzbsp_xpu_config.h"
#include "tzbsp_memory_map.h"
#include "tzbsp_target.h"
#include "tz_securechannel.h"

/* Array of the secure memory areas. All secure memory areas should be
 * appended to this list. */
static tzbsp_blacklist_area_t g_8x26_blist[] =
{
  /* Everything outside of DDR is considered blacklisted; we don't expect
   * to recieve any addresses within this range from the NS-world. */
  {
    TZBSP_BLIST_DEVICE_MEMORY,
    TZBSP_BLACKLIST_ITEM_STATIC | TZBSP_BLACKLIST_ITEM_ENABLED,
    0, 0xFFFFFFFF   /* Configured during cold boot. */
  },

  /* ADSP/LPASS item. Dynamically configured by PIL, disabled by default. */
  {
    TZBSP_BLIST_LPASS,
    TZBSP_BLACKLIST_ITEM_DYNAMIC | TZBSP_BLACKLIST_ITEM_DISABLED, 0, 0
  },

  /* WCNSS core item.  Dynamically configured by PIL, disabled by default. */
  {
    TZBSP_BLIST_WLAN,
    TZBSP_BLACKLIST_ITEM_DYNAMIC | TZBSP_BLACKLIST_ITEM_DISABLED, 0, 0
  },

  /* Video core item.  Dynamically configured by PIL, disabled by default. */
  {
    TZBSP_BLIST_VIDEO,
    TZBSP_BLACKLIST_ITEM_DYNAMIC | TZBSP_BLACKLIST_ITEM_DISABLED, 0, 0
  },

  /* MSS Secure Channel is blacklisted */
  {
    TZBSP_BLIST_SECCHANNEL_MSS,
    TZBSP_BLACKLIST_ITEM_STATIC | TZBSP_BLACKLIST_ITEM_ENABLED,
    TZBSP_EBI1_SECCHANNEL_MSS_BASE, TZBSP_EBI1_SECCHANNEL_MSS_END
  },

  /* Fixed secure DDR area is blacklisted */
  {
    TZBSP_BLIST_FIXED_SEC_DDR,
    TZBSP_BLACKLIST_ITEM_STATIC | TZBSP_BLACKLIST_ITEM_ENABLED,
    TZBSP_FIXED_SECURE_DDR_BASE, TZBSP_FIXED_SECURE_DDR_END
  },

  /* 4 areas are supported for dynamically allocated EBI partitions.
   * 1 for Page Table memory used by Video core
   * 1 for QSEE Req/Rsp region
   * 1 for QSEE Secure App region 
   * 1 for cp audio 
   */
  {
    TZBSP_BLIST_ALLOC_0,
    TZBSP_BLACKLIST_ITEM_DYNAMIC | TZBSP_BLACKLIST_ITEM_DISABLED, 0, 0
  },
  {
    TZBSP_BLIST_ALLOC_1,
    TZBSP_BLACKLIST_ITEM_DYNAMIC | TZBSP_BLACKLIST_ITEM_DISABLED, 0, 0
  },
  {
    TZBSP_BLIST_ALLOC_2,
    TZBSP_BLACKLIST_ITEM_DYNAMIC | TZBSP_BLACKLIST_ITEM_DISABLED, 0, 0
  },
  {
    TZBSP_BLIST_ALLOC_3,
    TZBSP_BLACKLIST_ITEM_DYNAMIC | TZBSP_BLACKLIST_ITEM_DISABLED, 0, 0
  },
  /* TZ Region in DDR */
  {
    TZBSP_BLIST_TZ_DDR,
    TZBSP_BLACKLIST_ITEM_STATIC | TZBSP_BLACKLIST_ITEM_ENABLED,
    TZBSP_EBI1_TZ_BASE, TZBSP_EBI1_TZ_END
  },
  /* Stopper item to mark end of blacklist */
  {
    TZBSP_BLACKLIST_ID_NONE,
    0, 0, 0
  },
  /* EBI1 memory space goes all the way up to 0xffff_ffff. */
};

/* Bind the 8974 blacklist as the default one. */
tzbsp_blacklist_area_t* g_tzbsp_blist = g_8x26_blist;

/* Get the blacklist id based on SubSystem ID */
int tzbsp_ssid2blistid(tz_sc_ss_e_type ssid, uint32* blist_id_ptr)
{
  int status = E_SUCCESS;

  if (blist_id_ptr == NULL)
  {
    return -E_INVALID_ARG;
  }

  switch(ssid)
  {
    case TZ_SC_LPASS:
      *blist_id_ptr = TZBSP_BLIST_LPASS;
      break;

    case TZ_SC_WCNSS:
      *blist_id_ptr = TZBSP_BLIST_WLAN;
      break;

    default:
      status = -E_INVALID_ARG;
      break;
  }

  return status;
}

/* In Bear family, device address range is [0, 0x80000000]*/
/* TODO:  for 1GB devices, the last 1GB of the address range (32-bit)
 *        remains open (not black listed). We have to add one more region
 *        in blacklisted list to cover the last 1GB
 */
void tzbsp_reconfig_black_list(uint32 ddr_start, uint32 ddr_end)
{
  g_tzbsp_blist[TZBSP_BLIST_DEVICE_MEMORY].start = 0;
  g_tzbsp_blist[TZBSP_BLIST_DEVICE_MEMORY].end = ddr_start;
}
