#ifndef TZBSP_OCMEM_H
#define TZBSP_OCMEM_H

/*===========================================================================
Copyright (c) 2012 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8916/src/tzbsp_ocmem.h#1 $
$DateTime: 2018/02/07 00:37:16 $
$Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
05/23/12   rv      First version.
============================================================================*/
#include <comdef.h>
#include "tz_syscall_pub.h"

/* Number of Resource Groups available in the various OCMEM MPUs */
#define TZBSP_OCMEM_NUM_MPU_RGS       (8)

/* At this point we only support 10 concurrent OCMEM allocations */
#define TZBSP_OCMEM_MAX_ALLOCS        (10)

/* Number of banks in 8974 */
#define TZBSP_OCMEM_REGION_CNT        (3)

/* Size of each bank in bytes */
#define TZBSP_OCMEM_REGION_SIZE       (0x80000)

/* Base address for OCMEM */
#define TZBSP_OCMEM_BASE_ADDR         (0xFEC00000)

#define TZBSP_OCMEM_REGION_0          (0)
#define TZBSP_OCMEM_REGION_1          (TZBSP_OCMEM_REGION_0 + TZBSP_OCMEM_REGION_SIZE)
#define TZBSP_OCMEM_REGION_2          (TZBSP_OCMEM_REGION_1 + TZBSP_OCMEM_REGION_SIZE)

#define TZBSP_OCMEM_SIZE              (TZBSP_OCMEM_REGION_CNT * TZBSP_OCMEM_REGION_SIZE)

typedef enum
{
  TZBSP_OCMEM_XPU_GROUP_CORE = 0, /* OCMEM Core */
  TZBSP_OCMEM_XPU_GROUP_SNOC,     /* OCMEM access via system NOC */
  TZBSP_OCMEM_XPU_GROUP_ONOC,     /* OCMEM access via MMSS OCMEM NOC */
  TZBSP_OCMEM_XPU_GROUP_DM,       /* OCMEM DM */
  TZBSP_NUM_OCMEM_XPU_GROUPS
} tzbsp_ocmem_xpu_groups_e;

/**
 * Lock OCMEM region:
 * Validates the request against internal TZ state &
 * reprogram MPU for access to requested client
 *
 * @param  client_id: OCMEM client requesting the lock
 * @param  offset   : Offset from the OCMEM base address
 * @param  size     : Size of memory requested
 * @param  mode     : Wide mode or narrow mode.
 *
 * @return int:
 *     0 if successful.
 *    -E_FAILURE: Failed to lock memory region.
 */
int tzbsp_ocmem_lock_region
(
  tz_ocmem_client_e_type          client_id,
  uint32                          offset,
  uint32                          size,
  tz_ocmem_op_mode_e_type         mode
);

/**
 * Unlock OCMEM region:
 * Validates the request against internal TZ state & reprogram
 * MPU for access to TZ only.
 * Zero out the OCMEM region
 *
 * @param  client_id: OCMEM client requesting the lock
 * @param  offset   : Offset from the OCMEM base address
 * @param  size     : Size of memory requested
 *
 * @return int:
 *     0 if successful.
 *    -E_FAILURE: Failed to unlock memory region.
 */
int tzbsp_ocmem_unlock_region
(
  tz_ocmem_client_e_type          client_id,
  uint32                          offset,
  uint32                          size
);

/**
 * Enable dumping of OCMEM region by HLOS
 * Validates the request against internal TZ state & reprogram
 * MPU to enable access to HLOS
 *
 * NOTE:
 * 1) This function is for debug purpose only
 * 2) This function should always be followed by
 * tzbsp_ocmem_disable_mem_dump
 * 3) This function is disabled when secboot fuse is blown
 *
 * @param  client_id: OCMEM client that currently owns the
 *                    region
 * @param  offset   : Offset from the OCMEM base address
 * @param  size     : Size of memory that needs to be dumped
 *
 * @return int:
 *     0 if successful.
 *    -E_FAILURE: Failed to lock memory region.
 */
int tzbsp_ocmem_enable_mem_dump
(
  tz_ocmem_client_e_type          client_id,
  uint32                          offset,
  uint32                          size
);

/**
 * Disable memory dump of OCMEM region:
 * Validates the request against internal TZ state & reprogram
 * MPU to disable access to HLOS
 *
 * @param  client_id: OCMEM client that currently owns the
 *                    region
 * @param  offset   : Offset from the OCMEM base address
 * @param  size     : Size of memory that needs to be dumped
 *
 * @return int:
 *     0 if successful.
 *    -E_FAILURE: Failed to lock memory region.
 */
int tzbsp_ocmem_disable_mem_dump
(
  tz_ocmem_client_e_type          client_id,
  uint32                          offset,
  uint32                          size
);

#endif /* TZBSP_OCMEM_H */

